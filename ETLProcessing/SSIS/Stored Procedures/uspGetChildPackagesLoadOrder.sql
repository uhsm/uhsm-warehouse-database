﻿



/************************************************************************************
Name: SSIS.uspGetChildPackagesLoadOrder
Type: Stored Procedure
Purpose: To work out the process queue of packages and which thread the package will
	 process under. This is dependent on the maximum number of parallel tasks 
	 and the remainder after a modulo using this value.

	 Sequence order is determined by the SequenceNumber within a phase for a particular
	 parent package.
Inputs: ParentPackageId, MaxParallelTasks
Used in package: ParentPackage - "Get Child Package Order"
Created by: Mandy Parker
Created on: 19/09/2014
Last Update by: 
Last Update on: 

Update History:


*************************************************************************************/

CREATE PROCEDURE [SSIS].[uspGetChildPackagesLoadOrder] @ParentPackageID int, @MaxParallelTasks int
AS
BEGIN
	SELECT 
		 Phase
		,SequenceOrder
		,CASE 
			WHEN SequenceOrder % @MaxParallelTasks = 0 THEN '1' --no remainder from modulo to thread1
			WHEN SequenceOrder % @MaxParallelTasks = 1 THEN '2' --remainder 1 from modulo to thread2
			WHEN SequenceOrder % @MaxParallelTasks = 2 THEN '3' --remainder 2 from modulo to thread3
			WHEN SequenceOrder % @MaxParallelTasks = 3 THEN '4' --remainder 3 from modulo to thread4
			WHEN SequenceOrder % @MaxParallelTasks = 4 THEN '5' --remainder 4 from modulo to thread5
			WHEN SequenceOrder % @MaxParallelTasks = 5 THEN '6' --remainder 5 from modulo to thread6
			WHEN SequenceOrder % @MaxParallelTasks = 6 THEN '7' --remainder 6 from modulo to thread7
			WHEN SequenceOrder % @MaxParallelTasks = 7 THEN '8' --remainder 7 from modulo to thread8
			ELSE '1'
		 END AS 'Thread'
		,PK_ChildPackageID
		FROM
	(
	SELECT
		 PK_ChildPackageID
		,Phase
		,CASE WHEN pp.ParentPackageName in ('LoadInquire','LoadDatix','LoadPPMLive_Standby','LoadCRIS') THEN SequenceNumber ELSE ROW_NUMBER() OVER(PARTITION BY Phase ORDER BY SequenceNumber) END AS 'SequenceOrder'
	FROM SSIS.ChildPackages cp
	LEFT JOIN SSIS.ParentPackages pp
	ON cp.FK_ParentPackageID = pp.PK_ParentPackage
	WHERE FK_ParentPackageID =  @ParentPackageID
	AND cp.isPackageEnabled = 'True'
	) Sequence
	ORDER BY Phase,SequenceOrder
END