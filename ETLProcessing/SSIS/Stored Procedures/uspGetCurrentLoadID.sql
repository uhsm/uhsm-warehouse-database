﻿

/************************************************************************************
Name: SSIS.GuspetCurrentLoadID
Type: Stored Procedure
Purpose: To determine the current load ID
Inputs: ParentPackageID
Used in package: ParentPackage - "Get Current Load ID"
Created by: Mandy Parker
Created on: 19/09/2014
Last Update by: 
Last Update on: 

Update History:


*************************************************************************************/

CREATE PROCEDURE [SSIS].[uspGetCurrentLoadID] @ParentPackageID int
AS
BEGIN
	SELECT
		 PK_LoadID
	FROM SSIS.Control ctrl,
	(SELECT 
			 MAX(PK_LoadID) AS 'maxLoadId'
			,FK_ParentPackageID
	 FROM SSIS.Control
	 WHERE [FK_ParentPackageID] = @ParentPackageID
	 GROUP BY FK_ParentPackageID) maxLoad
	WHERE ctrl.PK_LoadID = maxLoad.maxLoadId
	AND ctrl.FK_ParentPackageID = @ParentPackageID
	AND IsRunning = 'True'
END