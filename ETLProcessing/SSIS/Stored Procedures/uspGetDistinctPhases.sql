﻿


/************************************************************************************
Name: SSIS.uspGetDistinctPhases
Type: Stored Procedure
Purpose: Returns the distinct phases of child packages associated with a particular
	 parent package. The full result set is loaded to a system.object variable.
Inputs: ParentPackageID
Used in package: ParentPackage - "Get Distinct Phases"
Created by: Mandy Parker
Created on: 19/09/2014
Last Update by: 
Last Update on: 

Update History:


*************************************************************************************/

CREATE PROCEDURE [SSIS].[uspGetDistinctPhases] @ParentPackageID int
AS
SELECT
	 DISTINCT Phase AS ProcessPhase
FROM SSIS.ChildPackages
WHERE [FK_ParentPackageID] = @ParentPackageID