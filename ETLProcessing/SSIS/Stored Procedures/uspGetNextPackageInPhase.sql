﻿

/************************************************************************************
Name: SSIS.uspGetNextPackageInPhase
Type: Stored Procedure
Purpose: Returns the next package within in particular phase and within a particular
	 thread within that phase.
Inputs: CurrentPhase, LoadID, ParentPackageID, Thread
Outputs: NextPackageID, NextPackageName
Used in package: Worker - "Get First Package" & "Get Next Package"
Created by: Mandy Parker
Created on: 19/09/2014
Last Update by: 
Last Update on: 

Update History:

*************************************************************************************/

CREATE PROCEDURE [SSIS].[uspGetNextPackageInPhase] @CurrentPhase int, @LoadID int, @ParentPackageID int ,@Thread int, @NextPackageID int OUTPUT, @NextPackageName varchar(100) OUTPUT
AS
SELECT TOP 1 
	 @NextPackageName = CP.ChildPackageName
	,@NextPackageID = CP.PK_ChildPackageID
FROM SSIS.ChildPackages CP
INNER JOIN SSIS.ProcessQueue PQ
	ON CP.PK_ChildPackageID = PQ.FK_ChildPackageID
WHERE PQ.HasProcessed = 'False'
	AND PQ.IsProcessing = 'False'
	AND PQ.Phase = @CurrentPhase
	AND PQ.FK_LoadID = @LoadID
	AND PQ.FK_ParentPackageID = @ParentPackageID
	AND PQ.Thread = @Thread
ORDER BY PQ.SequenceOrder