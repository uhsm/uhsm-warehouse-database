﻿


/************************************************************************************
Name: SSIS.uspGetNumberOfPackagesInPhase
Type: Stored Procedure
Purpose: Counts the number of packages within a particular phase
Inputs: CurrentPhase, LoadID, ParentPackage
Used in package: <notUsed>
Created by: Mandy Parker
Created on: 19/09/2014
Last Update by: 
Last Update on: 

Update History:


*************************************************************************************/

CREATE PROCEDURE [SSIS].[uspGetNumberOfPackagesInPhase] @CurrentPhase int, @LoadID int, @ParentPackageID int
AS
SELECT COUNT(*)
FROM SSIS.ProcessQueue
WHERE HasProcessed = 'False'
AND Phase = @CurrentPhase
AND FK_LoadID = @LoadID
AND FK_ParentPackageID = @ParentPackageID