﻿


/************************************************************************************
Name: SSIS.uspGetNumberOfPhases
Type: Stored Procedure
Purpose: Returns the max phase number from a set of child packages associated with a
	 particular parent package.
Inputs: ParentPackageID
Used in package: <notUsed>
Created by: Mandy Parker
Created on: 19/09/2014
Last Update by: 
Last Update on: 

Update History:


*************************************************************************************/

CREATE PROCEDURE [SSIS].[uspGetNumberOfPhases] @ParentPackageID int
AS
SELECT
	 MAX(Phase) AS 'NumberOfPhases'
FROM SSIS.ChildPackages
WHERE [FK_ParentPackageID] = @ParentPackageID