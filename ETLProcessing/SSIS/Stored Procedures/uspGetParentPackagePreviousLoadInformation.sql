﻿


/************************************************************************************
Name: SSIS.uspGetParentPackagePreviousLoadInformation
Type: Stored Procedure
Purpose: Returns the LoadID, IsRunning, HasCompleted, UpdatedNumber, and ExtractStart-
	 and ExtractEnd-Time of the previous load of particular parent package.

	 The outputs are used to determine whether the previous load completed
	 successfully, and reset variable information if not, as well as determing the
	 start of the extract window for the next new load.
Inputs: ParentPackageID
Used in package: ParentPackage - "Get Prior Load Info", "Set Previous Load Information"
		 		 "Get Previous Load's Extract End Time"
Created by: Mandy Parker	
Created on: 09/09/2014
Last Update by: 
Last Update on: 

Update History:


*************************************************************************************/

CREATE PROCEDURE [SSIS].[uspGetParentPackagePreviousLoadInformation] @ParentPackageID int
AS
SELECT
	 ctrl.[PK_LoadID]
	,[IsRunning]
	,[HasCompleted]
	,CONVERT(VARCHAR(23),ExtractStartTime,121) AS ExtractStartTime
	,CONVERT(VARCHAR(23),ExtractEndTime,121) AS ExtractEndTime
FROM SSIS.Control ctrl,
	(SELECT 
			 MAX(sctrl.PK_LoadID) AS 'maxLoadId'
			,FK_ParentPackageID
	 FROM SSIS.Control sctrl
	 WHERE [FK_ParentPackageID] = @ParentPackageID
	 GROUP BY FK_ParentPackageID) maxLoad
WHERE ctrl.PK_LoadID = maxLoad.maxLoadId
AND ctrl.[FK_ParentPackageID] = @ParentPackageID