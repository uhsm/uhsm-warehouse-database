﻿


/************************************************************************************
Name: SSIS.uspGetParentPackageVariables
Type: Stored Procedure
Purpose: Returns the variables used throughout the solution - ParentPackageID, 
	 SSISFolder (where packages are located), sourceServer, sourceDatabase,
	 targetServer, targetDatabase, importsFolder, exportsFolder, msdbServer,
	 msdbDatabase, maximumParallelTasks, configServer,configDatabase,reliantPackage
	,SourceSchema, and TargetSchema
Inputs: ParentPackageName
Used in package: ParentPackage - "Get Package Variables"
Created by: Mandy Parker
Created on: 19/09/2014
Last Update by: 
Last Update on: 

Update History:


*************************************************************************************/

CREATE PROCEDURE [SSIS].[uspGetParentPackageVariables] @ParentPackageName varchar(50)
AS
SELECT
	 [PK_ParentPackage]
	,[ReliantPackage]
FROM SSIS.ParentPackages
WHERE [ParentPackageName] = @ParentPackageName