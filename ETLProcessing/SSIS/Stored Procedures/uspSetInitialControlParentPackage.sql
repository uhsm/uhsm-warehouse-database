﻿


/************************************************************************************
Name: SSIS.uspSetInitialControlParentPackage
Type: Stored Procedure
Purpose: Before a package can be run, a previous dummy load needs to be created in
	 the SSIS.Control table (to provide an ExtractStartDate).

	 Post the update, the ExtractEndTime can be manually altered to suit the needs
	 of the initial load.
Inputs: ParentPackageID
Used in package: None
Created by: Mandy Parker	
Created on: 19/09/2014
Last Update by: 
Last Update on: 

Update History:


*************************************************************************************/

CREATE PROCEDURE [SSIS].[uspSetInitialControlParentPackage] @ParentPackageID int
AS
BEGIN
	INSERT INTO SSIS.Control
	(FK_ParentPackageID,IsRunning,HasCompleted,CurrentPhase,ExtractStartTime,ExtractEndTime,ETLStartTime,ETLEndTime,isDailyUpdate,isMonthEnd,isYearEnd,MaxParallelTasks)
	VALUES
	(@ParentPackageID,'False','True',NULL,'2010-01-01 00:00:00.000','2010-01-01 00:00:00.000','2010-01-01 00:00:00.000','2010-01-01 00:00:00.000',NULL,NULL,NULL,NULL)
END