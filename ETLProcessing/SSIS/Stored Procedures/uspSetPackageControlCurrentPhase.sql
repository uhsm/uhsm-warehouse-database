﻿


/************************************************************************************
Name: SSIS.uspSetPackageControlCurrentPhase
Type: Stored Procedure
Purpose: Used to update the SSIS.Control table to indicate which phase is currently
 	 processing.
Inputs: CurrentPhase, LoadID, ParentPackage
Used in package: ParentPackage
Created by: Mandy Parker
Created on: 19/09/2014
Last Update by: 
Last Update on: 

Update History:


*************************************************************************************/

CREATE PROCEDURE [SSIS].[uspSetPackageControlCurrentPhase] @CurrentPhase int, @LoadID int, @ParentPackageID int
AS
UPDATE SSIS.Control
SET CurrentPhase = @CurrentPhase
WHERE PK_LoadID = @LoadID AND FK_ParentPackageID = @ParentPackageID