﻿CREATE TABLE [SSIS].[ChildPackageRunDetails] (
    [FK_LoadID]          INT           NOT NULL,
    [FK_ParentPackageID] INT           NOT NULL,
    [FK_ChildPackageID]  INT           NOT NULL,
    [PackageStartTime]   DATETIME      NOT NULL,
    [PackageEndTime]     DATETIME      NULL,
    [Status]             CHAR (1)      NOT NULL,
    [Message]            VARCHAR (MAX) NOT NULL,
    [TotalLoadRows]      INT           NULL,
    [TotalLoadErrors]    INT           NULL,
    [TotalUpdateRows]    INT           NULL,
    [TotalUpdateErrors]  INT           NULL,
    [TotalExportedRows]  INT           NULL,
    [TotalImportRows]    INT           NULL,
    CONSTRAINT [PK_ChildPackageRunDetails] PRIMARY KEY CLUSTERED ([FK_LoadID] ASC, [FK_ParentPackageID] ASC, [FK_ChildPackageID] ASC),
    CONSTRAINT [FK_ChildPackageRunDetails_ChildPackages] FOREIGN KEY ([FK_ChildPackageID]) REFERENCES [SSIS].[ChildPackages] ([PK_ChildPackageID]),
    CONSTRAINT [FK_ChildPackageRunDetails_Control] FOREIGN KEY ([FK_LoadID]) REFERENCES [SSIS].[Control] ([PK_LoadID]),
    CONSTRAINT [FK_ChildPackageRunDetails_ParentPackages] FOREIGN KEY ([FK_ParentPackageID]) REFERENCES [SSIS].[ParentPackages] ([PK_ParentPackage])
);




GO
