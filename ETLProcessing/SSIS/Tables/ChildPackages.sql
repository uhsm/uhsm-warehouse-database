﻿CREATE TABLE [SSIS].[ChildPackages] (
    [PK_ChildPackageID]  INT           IDENTITY (1, 1) NOT NULL,
    [FK_ParentPackageID] INT           NOT NULL,
    [ChildPackageName]   VARCHAR (100) NOT NULL,
    [Phase]              INT           CONSTRAINT [DF_ChildPackages_Phase] DEFAULT ((1)) NOT NULL,
    [SequenceNumber]     INT           NOT NULL,
    [isPackageEnabled]   BIT           CONSTRAINT [DF_ChildPackages_isPackageEnabled] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ChildPackages] PRIMARY KEY CLUSTERED ([PK_ChildPackageID] ASC),
    CONSTRAINT [FK_ChildPackages_ParentPackages] FOREIGN KEY ([FK_ParentPackageID]) REFERENCES [SSIS].[ParentPackages] ([PK_ParentPackage])
);




GO
