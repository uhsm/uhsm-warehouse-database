﻿CREATE TABLE [SSIS].[Control] (
    [PK_LoadID]          INT      IDENTITY (1, 1) NOT NULL,
    [FK_ParentPackageID] INT      NOT NULL,
    [IsRunning]          BIT      CONSTRAINT [DF_Control_IsRunning] DEFAULT ((1)) NOT NULL,
    [HasCompleted]       BIT      CONSTRAINT [DF_Control_HasCompleted] DEFAULT ((0)) NOT NULL,
    [CurrentPhase]       INT      NULL,
    [ExtractStartTime]   DATETIME NULL,
    [ExtractEndTime]     DATETIME NULL,
    [ETLStartTime]       DATETIME NULL,
    [ETLEndTime]         DATETIME NULL,
    [isDailyUpdate]      BIT      NULL,
    [isMonthEnd]         BIT      NULL,
    [isYearEnd]          BIT      NULL,
    [MaxParallelTasks]   INT      NULL,
    CONSTRAINT [PK_Control] PRIMARY KEY CLUSTERED ([PK_LoadID] ASC)
);




GO
