﻿CREATE TABLE [SSIS].[ParentPackages] (
    [PK_ParentPackage]  INT           IDENTITY (1, 1) NOT NULL,
    [ParentPackageName] VARCHAR (100) NOT NULL,
    [ReliantPackage]    VARCHAR (50)  CONSTRAINT [DF_ParentPackages_ReliantPackage] DEFAULT ('N/A') NULL,
    CONSTRAINT [PK_ParentPackages] PRIMARY KEY CLUSTERED ([PK_ParentPackage] ASC)
);




GO
