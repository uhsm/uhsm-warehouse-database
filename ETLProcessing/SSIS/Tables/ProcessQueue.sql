﻿CREATE TABLE [SSIS].[ProcessQueue] (
    [FK_LoadID]          INT NOT NULL,
    [FK_ParentPackageID] INT NOT NULL,
    [FK_ChildPackageID]  INT NOT NULL,
    [Phase]              INT NOT NULL,
    [SequenceOrder]      INT NOT NULL,
    [HasProcessed]       BIT CONSTRAINT [DF_ProcessQueue_HasProcessed] DEFAULT ((0)) NOT NULL,
    [IsProcessing]       BIT CONSTRAINT [DF_ProcessQueue_IsProcessing] DEFAULT ((0)) NOT NULL,
    [Thread]             INT NOT NULL,
    CONSTRAINT [PK_ProcessQueue] PRIMARY KEY CLUSTERED ([FK_LoadID] ASC, [FK_ParentPackageID] ASC, [FK_ChildPackageID] ASC),
    CONSTRAINT [FK_ProcessQueue_ChildPackages] FOREIGN KEY ([FK_ChildPackageID]) REFERENCES [SSIS].[ChildPackages] ([PK_ChildPackageID]),
    CONSTRAINT [FK_ProcessQueue_Control] FOREIGN KEY ([FK_LoadID]) REFERENCES [SSIS].[Control] ([PK_LoadID]),
    CONSTRAINT [FK_ProcessQueue_ParentPackages] FOREIGN KEY ([FK_ParentPackageID]) REFERENCES [SSIS].[ParentPackages] ([PK_ParentPackage])
);




GO
