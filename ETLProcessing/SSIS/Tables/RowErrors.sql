﻿CREATE TABLE [SSIS].[RowErrors] (
    [FK_LoadID]          INT           NOT NULL,
    [FK_ParentPackageID] INT           NOT NULL,
    [FK_ChildPackageID]  INT           NOT NULL,
    [Message]            VARCHAR (MAX) NULL,
    [UniqueRecordID]     VARCHAR (MAX) NULL,
    [ImportTime]         DATETIME      NULL,
    [SSISErrorCode]      INT           NULL
);




GO
