﻿

CREATE VIEW [SSIS].[ETLChildPackageRunDetails] AS

SELECT 
	FK_LoadID
	,PP.ParentPackageName
	,CP.ChildPackageName
	,PackageStartTime
	,PackageEndTime
	,DATEDIFF(SECOND,PackageStartTime,PackageEndTime) RunTimeSecs
	,DATEDIFF(MINUTE,PackageStartTime,PackageEndTime) RunTimeMins
	,Status
	,Message
	,TotalLoadRows
	,TotalLoadErrors
	,TotalUpdateRows
	,TotalUpdateErrors
FROM SSIS.ChildPackageRunDetails CPR
LEFT JOIN SSIS.ParentPackages PP
ON CPR.FK_ParentPackageID = PP.PK_ParentPackage
LEFT JOIN SSIS.ChildPackages CP
ON CPR.FK_ChildPackageID = CP.PK_ChildPackageID