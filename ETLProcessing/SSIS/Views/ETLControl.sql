﻿

CREATE VIEW [SSIS].[ETLControl] AS

SELECT 
	PK_LoadID 
	,PP.ParentPackageName
	,IsRunning
	,HasCompleted
	,ETLStartTime
	,ETLEndTime
	,DATEDIFF(SECOND,ETLStartTime,ETLEndTime) RunTimeSecs
	,DATEDIFF(MINUTE,ETLStartTime,ETLEndTime) RunTimeMins
	,isDailyUpdate
	,isMonthEnd
	,isYearEnd
FROM SSIS.Control C
LEFT JOIN SSIS.ParentPackages PP
ON C.FK_ParentPackageID = PP.PK_ParentPackage