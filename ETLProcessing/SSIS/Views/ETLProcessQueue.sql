﻿

CREATE VIEW [SSIS].[ETLProcessQueue] AS

SELECT 
	FK_LoadID
	,PP.ParentPackageName
	,CP.ChildPackageName
	,PQ.Phase
	,SequenceOrder
	,HasProcessed
	,IsProcessing
	,Thread
FROM SSIS.ProcessQueue PQ
LEFT JOIN SSIS.ParentPackages PP
ON PQ.FK_ParentPackageID = PP.PK_ParentPackage
LEFT JOIN SSIS.ChildPackages CP
ON PQ.FK_ChildPackageID = CP.PK_ChildPackageID