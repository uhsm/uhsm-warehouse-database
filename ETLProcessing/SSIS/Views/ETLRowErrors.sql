﻿

CREATE VIEW [SSIS].[ETLRowErrors] AS

SELECT 
	FK_LoadID
	,PP.ParentPackageName
	,CP.ChildPackageName
	,Message
	,UniqueRecordID
	,ImportTime
	,SSISErrorCode
FROM SSIS.RowErrors RE
LEFT JOIN SSIS.ParentPackages PP
ON RE.FK_ParentPackageID = PP.PK_ParentPackage
LEFT JOIN SSIS.ChildPackages CP
ON RE.FK_ChildPackageID = CP.PK_ChildPackageID