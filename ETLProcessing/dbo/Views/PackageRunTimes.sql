﻿



CREATE view [dbo].[PackageRunTimes] as

select
        ChildPackageRunDetails.FK_LoadID
       ,ChildPackages.FK_ParentPackageID
       ,ChildPackages.PK_ChildPackageID
       ,ParentPackages.ParentPackageName
       ,ChildPackages.ChildPackageName
       ,ChildPackageRunDetails.PackageStartTime
       ,ChildPackageRunDetails.PackageEndTime
       ,DurationSeconds = isnull(datediff(second, ChildPackageRunDetails.PackageStartTime, ChildPackageRunDetails.PackageEndTime) +1, 0)
from
       SSIS.ChildPackages

left join
    SSIS.ParentPackages
on
    ChildPackages.FK_ParentPackageID = ParentPackages.PK_ParentPackage

inner join SSIS.ChildPackageRunDetails
on     ChildPackageRunDetails.FK_ChildPackageID = ChildPackages.PK_ChildPackageID
and    ChildPackageRunDetails.Status = 'C'