﻿CREATE   PROCEDURE Audit.WriteLogEvent
	 @ProcessCode varchar(255) = null
	,@Event varchar(255) = null
	,@StartTime datetime = null
	,@PrintAudit bit = 1
as

select
	 @ProcessCode = coalesce(@ProcessCode, 'Process missing')
	,@Event = coalesce(@Event, 'Event information missing')


if (@PrintAudit = 1)
begin

	declare @Audit varchar(max) =
		convert(varchar, getdate(), 111) + ' ' + convert(varchar, getdate(), 114) + ' : ' + @ProcessCode + ' - ' + @Event

	print @Audit
end


insert into
	Audit.Log 
	(
	 EventTime
	,UserId
	,ProcessCode
	,Event
	,StartTime
	)
values 
	(
	 getdate()
	,Suser_SName()
	,@ProcessCode
	,@Event
	,@StartTime
	)