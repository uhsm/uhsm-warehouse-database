﻿CREATE procedure ETL.LoadReferenceData
as

/********************************************************************************************************************************************************************
Author: Phil Orrell
Date: 14 Oct 2014
Aim: generate Local and National metadata if missing

Change History
20141014 Created
*********************************************************************************************************************************************************************/

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@deleted int
	,@inserted int
	,@updated int

declare
	@ProcessList table
	(
	 Action nvarchar(10)
	)


insert
into
	Map.AttributeContext
(
	 AttributeID
	,ContextID
)
select distinct
	 Attribute.AttributeID
	,Context.ContextID
from
	ETL.TImportReferenceData

inner join Map.Attribute
on	Attribute.AttributeCode = TImportReferenceData.AttributeCode

inner join Map.Context
on	Context.ContextCode = TImportReferenceData.ContextCode

where
	not exists
	(
	select
		1
	from
		Map.AttributeContext
	where
		AttributeContext.AttributeID = Attribute.AttributeID
	and	AttributeContext.ContextID = Context.ContextID
	)



merge
	Map.Value target
using
	(
	select
		 AttributeContextID
		,ValueCode
		,Value
	from
		ETL.TImportReferenceData

	inner join Map.Attribute
	on	Attribute.AttributeCode = TImportReferenceData.AttributeCode

	inner join Map.Context
	on	Context.ContextCode = TImportReferenceData.ContextCode

	inner join Map.AttributeContext
	on	AttributeContext.AttributeID = Attribute.AttributeID
	and	AttributeContext.ContextID = Context.ContextID
	) source
on	source.AttributeContextID = target.AttributeContextID
and	source.ValueCode = target.ValueCode

when not matched
then
	insert
		(
		 AttributeContextID
		,ValueCode
		,Value
		,Created
		,ByWhom
		)
	values
		(
		 source.AttributeContextID
		,source.ValueCode
		,source.Value
		,getdate()
		,suser_name()
		)

when matched
and	source.Value <> target.Value
then
	update
	set
		 target.Value = source.Value
		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	 $action
into
	@ProcessList

;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


exec Map.BuildMissingXrefs

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + cast(@Elapsed as varchar) + ' minutes'

print @Stats

exec Audit.WriteLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime