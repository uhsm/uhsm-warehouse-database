﻿CREATE TABLE [ETL].[TImportReferenceData] (
    [AttributeCode] VARCHAR (20)  NOT NULL,
    [ContextCode]   VARCHAR (20)  NOT NULL,
    [ValueCode]     VARCHAR (255) NOT NULL,
    [Value]         VARCHAR (512) NULL,
    CONSTRAINT [PK_TImportReferenceData] PRIMARY KEY CLUSTERED ([AttributeCode] ASC, [ContextCode] ASC, [ValueCode] ASC)
);

