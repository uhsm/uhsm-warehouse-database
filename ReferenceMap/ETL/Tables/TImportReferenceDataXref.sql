﻿CREATE TABLE [ETL].[TImportReferenceDataXref] (
    [AttributeCode] VARCHAR (20)  NOT NULL,
    [ContextCode]   VARCHAR (20)  NOT NULL,
    [ValueCode]     VARCHAR (255) NOT NULL,
    [ContextXrefCode]   VARCHAR (20)  NOT NULL,
    [ValueXrefCode]     VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_TImportReferenceDataXref] PRIMARY KEY CLUSTERED ([AttributeCode] ASC, [ContextCode] ASC, [ValueCode] ASC, [ContextXrefCode] ASC, [ValueXrefCode] ASC)
);

