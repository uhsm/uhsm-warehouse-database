﻿CREATE function [Map].[GetAttributeValue]
(
	 @AttributeCode varchar(10)
	,@ContextCode varchar(10)
	,@ValueCode varchar(max)
)
RETURNS TABLE 
AS
RETURN 
(
select
	Value.ValueID
from
	Map.Value

inner join Map.AttributeContext
on	AttributeContext.AttributeContextID = Value.AttributeContextID

inner join Map.Context
on	Context.ContextID = AttributeContext.ContextID

inner join Map.Attribute
on	Attribute.AttributeID = AttributeContext.AttributeID

where
	Attribute.AttributeCode = @AttributeCode
and	Context.ContextCode = @ContextCode
and	Value.ValueCode = @ValueCode
)