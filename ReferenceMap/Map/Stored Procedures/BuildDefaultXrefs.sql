﻿CREATE procedure [Map].[BuildDefaultXrefs]
	@debug bit = 0
as

--prevent parallel running and consequent duplication of values
set transaction isolation level serializable

begin transaction

	declare @referenceTable varchar(max)
	declare @referenceCodeColumn varchar(max)
	declare @referenceNationalCodeColumn varchar(max)
	declare @XrefValueAttributeContextID int
	declare @SourceAttributeContextID int


	declare @sql varchar(max)

	declare @sqlTemplate varchar(max) = 
		'
		update
			Map.ValueXref
		set
			 ValueXrefID = NationalValue.ValueID
			,Updated = getdate()
			,ByWhom = SUSER_NAME()
		from
			Map.Value

		inner join Map.AttributeContext
		on	AttributeContext.AttributeContextID = Value.AttributeContextID

		inner join Map.ValueXref
		on	ValueXref.ValueXrefID = Value.ValueID

		inner join Map.Attribute
		on	Attribute.AttributeID = AttributeContext.AttributeID

		inner join Map.Value SourceValue
		on	SourceValue.ValueID = ValueXref.ValueID

		inner join <referenceTable> ReferenceTable
		on	cast(ReferenceTable.<referenceCodeColumn> as varchar(MAX)) = SourceValue.ValueCode

		--does the national code exist in the Value table for this context
		inner join  Map.Value NationalValue
		on	NationalValue.ValueCode = cast(ReferenceTable.<referenceNationalCodeColumn> as varchar(MAX))
		and	NationalValue.AttributeContextID = Value.AttributeContextID

		where
			Value.ValueCode = ''N||'' + Attribute.AttributeCode
		and	Value.AttributeContextID = <XrefValueAttributeContextID>
		and	SourceValue.AttributeContextID = <SourceAttributeContextID>
		'

	--get distinct Attribute/Context from the activity table

	--loop through each context

	declare contextCursor cursor for
	select distinct
		 XrefValueAttributeContextID = Value.AttributeContextID
		,SourceAttributeContextID = SourceValue.AttributeContextID
	from
		Map.Value

	inner join Map.AttributeContext
	on	AttributeContext.AttributeContextID = Value.AttributeContextID

	inner join Map.ValueXref
	on	ValueXref.ValueXrefID = Value.ValueID

	inner join Map.Attribute
	on	Attribute.AttributeID = AttributeContext.AttributeID

	inner join Map.Value SourceValue
	on	SourceValue.ValueID = ValueXref.ValueID

	where
		Value.ValueCode = 'N||' + Attribute.AttributeCode

	OPEN contextCursor
  
	FETCH NEXT FROM contextCursor
	INTO
		 @XrefValueAttributeContextID
		,@SourceAttributeContextID

	WHILE @@FETCH_STATUS = 0

	BEGIN

		select
			 @referenceTable = null
			,@referenceCodeColumn = null
			,@referenceNationalCodeColumn = null


		--get the reference table code column name and reference table name
		select
			@referenceTable = 
				TableBase.[Table]

			,@referenceCodeColumn =
				'[' + ColumnBase.[Column] + ']'

		from
			Map.ColumnBase

		inner join Map.TableBase
		on	TableBase.TableID = ColumnBase.TableID

		where
			ColumnBase.ColumnTypeID = 2	--Reference Code
		and	ColumnBase.AttributeContextID = @SourceAttributeContextID

		--get the reference value (national) column name
		select
			@referenceNationalCodeColumn =
				'[' + ColumnBase.[Column] + ']'
		from
			Map.ColumnBase
		where
			ColumnBase.AttributeContextID = @SourceAttributeContextID
		and	ColumnBase.ColumnTypeID = 4	--National Reference

		if @referenceNationalCodeColumn is not null
		begin

			select @sql = 
				REPLACE(
				REPLACE(
				REPLACE(
				REPLACE(
				REPLACE(
					 @sqlTemplate
					,'<referenceTable>'
					,@referenceTable
				)
					,'<referenceCodeColumn>'
					,@referenceCodeColumn
				)
					,'<referenceNationalCodeColumn>'
					,@referenceNationalCodeColumn
				)
					,'<XrefValueAttributeContextID>'
					,@XrefValueAttributeContextID
				)
					,'<SourceAttributeContextID>'
					,@SourceAttributeContextID
				)

			if @sql is not null
				if @debug = 1
					print @sql
				else
				begin
					execute(@sql)
				end
		end


		FETCH NEXT FROM contextCursor
		INTO
			 @XrefValueAttributeContextID
			,@SourceAttributeContextID
	END
  
	CLOSE contextCursor
	DEALLOCATE contextCursor

commit
