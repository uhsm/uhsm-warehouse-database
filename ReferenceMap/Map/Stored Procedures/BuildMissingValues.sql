﻿CREATE procedure [Map].[BuildMissingValues]
	 @activityTableID int
	,@NewRows int = 0 output
	,@debug bit = 0
	,@ActivityTableOverride varchar(max) = null
as

--prevent parallel running and consequent duplication of values
set transaction isolation level serializable

begin transaction

	declare @activityTable varchar(max)
	declare @activityCodeColumn varchar(max)
	declare @referenceTable varchar(max)
	declare @referenceCodeColumn varchar(max)
	declare @referenceColumn varchar(max)
	declare @attributeContextID int
	declare @contextCode varchar(max)
	declare @sql varchar(max)
	declare @columnContext varchar(max)

	declare @sqlTemplate varchar(max) = 
		'
IF OBJECT_ID(N''tempdb.dbo.#DistinctValues'', ''U'') IS NOT NULL
BEGIN
  DROP TABLE #DistinctValues;
END;

select distinct
	ValueCode = coalesce(cast(ActivityTable.<activityCodeColumn> as varchar(MAX)), ''-1'')
	,ActivityTable.ContextCode
into
	#DistinctValues
from
	<activityTable> ActivityTable
;

insert
into
	Map.Value
	(
	AttributeContextID
	,ValueCode
	,Value
	,Created
	,ByWhom
	)
select
	AttributeContextID
	,ValueCode
	,Value
	,Created
	,ByWhom
from
	(
	select
		AttributeContextID = <attributeContextID>
		,DistinctValues.ValueCode

		,Value =
			isnull(
				cast(ReferenceTable.<referenceColumn> as varchar(MAX))
				,''No Description''
			)

		,Created = GETDATE()
		,ByWhom = SUSER_NAME()
	from
		#DistinctValues DistinctValues

	left join <referenceTable> ReferenceTable
	on	cast(ReferenceTable.<referenceCodeColumn> as varchar(max)) = DistinctValues.ValueCode

	where
		DistinctValues.ContextCode = ''<contextCode>''
	) ActivityTable
where
	not exists
	(
	select
		1
	from
		Map.Value
	where
		Value.ValueCode = ActivityTable.ValueCode
	and	Value.AttributeContextID = <attributeContextID>
	)

;

		'

	declare @sqlTemplateNoReferenceTable varchar(max) = 
		'
		insert
		into
			Map.Value
			(
			 AttributeContextID
			,ValueCode
			,Value
			,Created
			,ByWhom
			)
		select
			 AttributeContextID
			,ValueCode
			,Value
			,Created
			,ByWhom
		from
			(
			select distinct
				 AttributeContextID = <attributeContextID>
				,ValueCode = coalesce(Cast(ActivityTable.<activityCodeColumn> as varchar(MAX)), ''-1'')

				,Value =
					coalesce(
						 cast(ActivityTable.<activityCodeColumn> as varchar(MAX)) + '' - No Description''
						,''Not Specified''
					)

				,Created = GETDATE()
				,ByWhom = SUSER_NAME()
			from
				<activityTable> ActivityTable
			where
				ActivityTable.ContextCode = ''<contextCode>''

			union

			select
				 AttributeContextID = <attributeContextID>
				,ValueCode = ''-1''
				,Value = ''Not Specified''
				,Created = GETDATE()
				,ByWhom = SUSER_NAME()

			) ActivityTable

		where
			not exists
			(
			select
				1
			from
				Map.Value
			where
				Value.ValueCode = ActivityTable.ValueCode
			and	Value.AttributeContextID = <attributeContextID>
			)
		'

	select
		@activityTable =
			coalesce(
				 @ActivityTableOverride
				,TableBase.[Table]
			)
	from
		Map.TableBase
	where
		TableBase.TableID = @activityTableID

	create table #AttributeContext
		(
		AttributeContextID int
		,ContextCode varchar(max)
		)

	--get distinct Attribute/Context from the activity table

	select
		@sql =
			REPLACE(
				'
				select distinct
					ActivityTable.ContextCode
				into
					#ActivityTable
				from
					<activityTable> ActivityTable


				insert into #AttributeContext
				select
					AttributeContext.AttributeContextID
					,ActivityTable.ContextCode
				from
					#ActivityTable ActivityTable

				cross join	Map.Attribute

				inner join Map.Context
				on	Context.BaseContext = 1
				and	Context.ContextCode = ActivityTable.ContextCode

				inner join Map.AttributeContext
				on	AttributeContext.ContextID = Context.ContextID
				and	AttributeContext.AttributeID = Attribute.AttributeID

				'
				,'<activityTable>'
				,@activityTable
			)

	if @debug = 1
		print @sql


	execute(@sql)

	set @NewRows = 0

	--loop through each context

	declare contextCursor cursor for
	select
		 AttributeContextID
		,ContextCode
	from
		#AttributeContext
	where
		AttributeContextID is not null

	OPEN contextCursor
  
	FETCH NEXT FROM contextCursor
	INTO @attributeContextID, @contextCode

	WHILE @@FETCH_STATUS = 0

	BEGIN

		select
			 @activityCodeColumn = null
			,@referenceTable = null
			,@referenceCodeColumn = null
			,@referenceColumn = null


		--get the reference table code column name and reference table name
		select
			@referenceTable = 
				TableBase.[Table]

			,@referenceCodeColumn =
				'[' + ColumnBase.[Column] + ']'

		from
			Map.ColumnBase

		inner join Map.TableBase
		on	TableBase.TableID = ColumnBase.TableID

		where
			ColumnBase.ColumnTypeID = 2	--Reference Code
		and	ColumnBase.AttributeContextID = @attributeContextID

		--get the reference value (description) column name
		select
			@referenceColumn =
				'[' + ColumnBase.[Column] + ']'
		from
			Map.ColumnBase
		where
			ColumnBase.AttributeContextID = @attributeContextID
		and	ColumnBase.ColumnTypeID = 3	--Reference


	--loop through each activity column code

		declare activityCodeColumnCursor cursor for

		--get the activity table column name
		select
			ActivityCodeColumn = '[' + ColumnBase.[Column] + ']'
		from
			Map.ColumnBase
		where
			ColumnBase.TableID = @activityTableID
		and	ColumnBase.AttributeContextID = @attributeContextID
		and	ColumnBase.ColumnTypeID = 1	--Activity Code

		OPEN activityCodeColumnCursor
	  
		FETCH NEXT FROM activityCodeColumnCursor
		INTO @activityCodeColumn

		WHILE @@FETCH_STATUS = 0

		BEGIN


			select @sql = 
				REPLACE(
				REPLACE(
				REPLACE(
				REPLACE(
				REPLACE(
				REPLACE(
				REPLACE(
					 @sqlTemplate
					,'<activityTable>'
					,@activityTable
				)
					,'<activityCodeColumn>'
					,@activityCodeColumn
				)	
					,'<attributeContextID>'
					,@attributeContextID
				)
					,'<referenceColumn>'
					,@referenceColumn
				)
					,'<referenceTable>'
					,@referenceTable
				)
					,'<referenceCodeColumn>'
					,@referenceCodeColumn
				)
					,'<contextCode>'
					,@contextCode
				)


			if @sql is null
				select @sql = 
					REPLACE(
					REPLACE(
					REPLACE(
					REPLACE(
						 @sqlTemplateNoReferenceTable
						,'<activityTable>'
						,@activityTable
					)
						,'<activityCodeColumn>'
						,@activityCodeColumn
					)	
						,'<attributeContextID>'
						,@attributeContextID
					)
						,'<contextCode>'
						,@contextCode
					)


			if @sql is not null
				if @debug = 1
					print @sql
				else

				begin try

					set @columnContext = 'Column: ' + @activityCodeColumn + ', Context: ' + @contextCode

					print @columnContext

					execute(@sql)

					set @NewRows = @NewRows + @@ROWCOUNT

				end try

				BEGIN
					CATCH
	
					DECLARE @ErrorSeverity INT,
							@ErrorNumber   INT,
							@ErrorMessage varchar(255),
							@ErrorState INT,
							@ErrorLine  INT,
							@ErrorProc varchar(200)

					-- Grab error information from SQL functions
					SET @ErrorSeverity = ERROR_SEVERITY()
					SET @ErrorNumber   = ERROR_NUMBER()
					SET @ErrorMessage  = ERROR_MESSAGE()
					SET @ErrorState    = ERROR_STATE()
					SET @ErrorLine     = ERROR_LINE()
					SET @ErrorProc     = ERROR_PROCEDURE()
					SET @ErrorMessage  = 'An error occurred. SQL Server Error Message is: ' + @ErrorMessage
				
					-- Not all errors generate an error state, to set to 1 if it's zero
					IF @ErrorState  = 0
						SET @ErrorState = 1

					exec Audit.WriteLogEvent @columnContext, @ErrorMessage

					RAISERROR (@ErrorMessage , @ErrorSeverity, @ErrorState, @ErrorNumber)
				END CATCH


			FETCH NEXT FROM activityCodeColumnCursor
			INTO @activityCodeColumn
		END
	  
		CLOSE activityCodeColumnCursor
		DEALLOCATE activityCodeColumnCursor


		FETCH NEXT FROM contextCursor
		INTO @attributeContextID, @contextCode
	END
  
	CLOSE contextCursor
	DEALLOCATE contextCursor

commit
