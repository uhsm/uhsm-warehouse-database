﻿CREATE procedure [Map].[DeleteDuplicateXrefs] as

--Do not run unless manually repairing duplicates

begin transaction

delete
from
	Map.ValueXref
from
	Map.ValueXref Member

inner join
	(
	select
		 ValueID
		,ValueXrefID
	from
		Map.ValueXref
	group by
		 ValueID
		,ValueXrefID
	having	
		count(*) > 1
	) Dupes
on	Dupes.ValueID = Member.ValueID
and	Dupes.ValueXrefID = Member.ValueXrefID

where
	not
		(
		not exists
			(
			select
				1
			from
				Map.ValueXref Xref
			where
				Xref.ValueID = Member.ValueID
			and	Xref.ValueXrefID = Member.ValueXrefID
			and	Xref.AttributeXrefID < Member.AttributeXrefID
			)
		)

--debug
	--select
	--	 ValueID
	--	,ValueXrefID
	--from
	--	Map.ValueXref
	--group by
	--	 ValueID
	--	,ValueXrefID
	--having	
	--	count(*) > 1

commit