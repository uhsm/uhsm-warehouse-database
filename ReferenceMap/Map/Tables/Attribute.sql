﻿CREATE TABLE [Map].[Attribute] (
    [AttributeID]   INT           IDENTITY (1, 1) NOT NULL,
    [AttributeCode] VARCHAR (20)  NOT NULL,
    [Attribute]     VARCHAR (512) NULL,
	[UseProperCase] BIT NOT NULL DEFAULT 1,
    CONSTRAINT [PK_AttributeType] PRIMARY KEY CLUSTERED ([AttributeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Attribute]
    ON [Map].[Attribute]([AttributeCode] ASC);

