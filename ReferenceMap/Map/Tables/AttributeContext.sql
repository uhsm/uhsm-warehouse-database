﻿CREATE TABLE [Map].[AttributeContext] (
    [AttributeContextID] INT IDENTITY (1, 1) NOT NULL,
    [AttributeID]        INT NOT NULL,
    [ContextID]          INT NOT NULL,
    PRIMARY KEY CLUSTERED ([AttributeContextID] ASC),
    CONSTRAINT [FK_AttributeContext_Attribute] FOREIGN KEY ([AttributeID]) REFERENCES [Map].[Attribute] ([AttributeID]),
    CONSTRAINT [FK_AttributeContext_Context] FOREIGN KEY ([ContextID]) REFERENCES [Map].[Context] ([ContextID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_AttributeContext]
    ON [Map].[AttributeContext]([AttributeID] ASC, [ContextID] ASC);

