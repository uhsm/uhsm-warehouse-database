﻿CREATE TABLE [Map].[ColumnBase] (
    [ColumnID]           INT           IDENTITY (1, 1) NOT NULL,
    [AttributeContextID] INT           NOT NULL,
    [ColumnTypeID]       INT           NOT NULL,
    [Column]             VARCHAR (128) NOT NULL,
    [TableID]            SMALLINT      NOT NULL,
    CONSTRAINT [PK__ColumnBa__1AA1422F2145C81B] PRIMARY KEY CLUSTERED ([ColumnID] ASC),
    CONSTRAINT [FK_ColumnBase_AttributeContext] FOREIGN KEY ([AttributeContextID]) REFERENCES [Map].[AttributeContext] ([AttributeContextID]),
    CONSTRAINT [FK_ColumnBase_ColumnBase] FOREIGN KEY ([ColumnTypeID]) REFERENCES [Map].[ColumnType] ([ColumnTypeID]),
    CONSTRAINT [FK_ColumnBase_TableBase] FOREIGN KEY ([TableID]) REFERENCES [Map].[TableBase] ([TableID])
);

