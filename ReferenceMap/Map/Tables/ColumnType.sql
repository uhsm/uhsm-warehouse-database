﻿CREATE TABLE [Map].[ColumnType] (
    [ColumnTypeID] INT          IDENTITY (1, 1) NOT NULL,
    [ColumnType]   VARCHAR (50) NOT NULL,
    CONSTRAINT [PK__ColumnTy__DC45A5D130C33EC3] PRIMARY KEY CLUSTERED ([ColumnTypeID] ASC)
);

