﻿CREATE TABLE [Map].[Context] (
    [ContextID]   INT           IDENTITY (1, 1) NOT NULL,
    [ContextCode] VARCHAR (20)  NOT NULL,
    [Context]     VARCHAR (255) NOT NULL,
    [BaseContext] BIT           NOT NULL,
    CONSTRAINT [PK_AttributeContext] PRIMARY KEY CLUSTERED ([ContextID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Context]
    ON [Map].[Context]([ContextCode] ASC);

