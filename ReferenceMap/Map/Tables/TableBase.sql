﻿CREATE TABLE [Map].[TableBase] (
    [TableID]     SMALLINT      IDENTITY (1, 1) NOT NULL,
    [Table]       VARCHAR (512) NOT NULL,
    [TableTypeID] TINYINT       NOT NULL,
    CONSTRAINT [PK__TableBas__7D5F018E498EEC8D] PRIMARY KEY CLUSTERED ([TableID] ASC),
    CONSTRAINT [FK_TableBase_TableType] FOREIGN KEY ([TableTypeID]) REFERENCES [Map].[TableType] ([TableTypeID])
);

