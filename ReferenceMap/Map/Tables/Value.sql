﻿CREATE TABLE [Map].[Value] (
    [ValueID]            INT           IDENTITY (1, 1) NOT NULL,
    [AttributeContextID] INT           NOT NULL,
    [ValueCode]          VARCHAR (255) NOT NULL,
    [Value]              VARCHAR (512) NOT NULL,
    [Created]            DATETIME      CONSTRAINT [DF_Value_Created] DEFAULT (getdate()) NOT NULL,
    [Updated]            DATETIME      NULL,
    [ByWhom]             VARCHAR (255) CONSTRAINT [DF_Value_ByWhom] DEFAULT (suser_name()) NOT NULL,
    CONSTRAINT [PK_AttributeValue] PRIMARY KEY CLUSTERED ([ValueID] ASC),
    CONSTRAINT [FK_AttributeValue_AttributeContext] FOREIGN KEY ([AttributeContextID]) REFERENCES [Map].[AttributeContext] ([AttributeContextID])
);


GO
CREATE NONCLUSTERED INDEX [Idx_TIBAPC_AttributeContextID]
    ON [Map].[Value]([AttributeContextID] ASC)
    INCLUDE([ValueID], [ValueCode]);


GO
CREATE NONCLUSTERED INDEX [IX_Value]
    ON [Map].[Value]([AttributeContextID] ASC, [Value] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_Value_1] ON [Map].[Value]
(
	[AttributeContextID] ASC,
	[ValueCode] ASC
);

