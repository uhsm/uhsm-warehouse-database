﻿CREATE TABLE [Map].[ValueXref] (
    [AttributeXrefID] INT           IDENTITY (1, 1) NOT NULL,
    [ValueID]         INT           NOT NULL,
    [ValueXrefID]     INT           NOT NULL,
    [Created]         DATETIME      CONSTRAINT [DF_ValueXref_Created] DEFAULT (getdate()) NOT NULL,
    [Updated]         DATETIME      NULL,
    [ByWhom]          VARCHAR (255) CONSTRAINT [DF_ValueXref_ByWhom] DEFAULT (suser_name()) NOT NULL,
    CONSTRAINT [PK__Attribut__39B97B120C4AAB35] PRIMARY KEY CLUSTERED ([ValueID] ASC, [ValueXrefID] ASC),
    CONSTRAINT [FK_AttributeXref_AttributeValue] FOREIGN KEY ([ValueID]) REFERENCES [Map].[Value] ([ValueID]),
    CONSTRAINT [FK_AttributeXref_AttributeValue1] FOREIGN KEY ([ValueXrefID]) REFERENCES [Map].[Value] ([ValueID])
);
GO

CREATE NONCLUSTERED INDEX [IX_Map_ValueXref_01]
ON [Map].[ValueXref] ([ValueXrefID])
INCLUDE ([AttributeXrefID],[ValueID],[Created])
GO
