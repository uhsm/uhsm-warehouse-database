﻿CREATE VIEW [Map].[TableTree] AS
SELECT
	TableID,
	AddressString,
	[Server] = CASE 
		WHEN LEFT([Server],1) = '[' THEN SUBSTRING([Server],2,LEN([Server])-2)
		ELSE [Server]
	END,
	[Database] =
	CASE 
		WHEN LEFT([Database],1) = '[' THEN SUBSTRING([Database],2,LEN([Database])-2)
		ELSE [Database]
	END
	,
	[Schema] = CASE 
		WHEN LEFT([Schema],1) = '[' THEN SUBSTRING([Schema],2,LEN([Schema])-2)
		ELSE [Schema]
	END,
	[Table] = CASE 
		WHEN LEFT([Table],1) = '[' THEN SUBSTRING([Table],2,LEN([Table])-2)
		ELSE [Table]
	END,
	TableTypeID,
	TableType
FROM
(
SELECT
	T.TableID,
	AddressString = T.[Table],
	[Server] = SUBSTRING(T.[Table],
		1,
		CHARINDEX('.',T.[Table],1)-1),
	[Database] = SUBSTRING(T.[Table],
		CHARINDEX('.',T.[Table],1)+1,
		CHARINDEX('.',T.[Table],CHARINDEX('.',T.[Table],1)+1) - 
		CHARINDEX('.',T.[Table],1)-1),
	[Schema] = SUBSTRING(T.[Table],
		CHARINDEX('.',T.[Table],CHARINDEX('.',T.[Table],1)+1)+1,
		CHARINDEX('.',T.[Table],CHARINDEX('.',T.[Table],CHARINDEX('.',T.[Table],1)+1)+1) - 
		CHARINDEX('.',T.[Table],CHARINDEX('.',T.[Table],1)+1)-1),
	[Table] = SUBSTRING(T.[Table],
		CHARINDEX('.',T.[Table],CHARINDEX('.',T.[Table],CHARINDEX('.',T.[Table],1)+1)+1)+1,
		999),
	T.TableTypeID,
	TT.TableType
FROM Map.TableBase T
JOIN Map.TableType TT ON TT.TableTypeID = T.TableTypeID
) AS X