﻿--Mappings relating to a given attribute may be copied into the target ETL.TImportReferenceDataXref table using an amended version of this utility script.

insert
into
	INFOLIVEDBITS.ReferenceMap.ETL.TImportReferenceDataXref
	(
	AttributeCode
	,ContextCode
	,ValueCode
	,ContextXrefCode
	,ValueXrefCode
	)
select
	Attribute.AttributeCode
	,sourceContext.ContextCode
	,sourceValue.ValueCode
	,targetContext.ContextCode
	,targetValue.ValueCode
from
	Map.ValueXref

inner join Map.Value sourceValue
on	sourceValue.ValueID = ValueXref.ValueID

inner join Map.AttributeContext AttributeContext
on	AttributeContext.AttributeContextID = sourceValue.AttributeContextID

inner join Map.Attribute
on	Attribute.AttributeID = AttributeContext.AttributeID

inner join Map.Context sourceContext
on	sourceContext.ContextID = AttributeContext.ContextID

inner join Map.Value targetValue
on	targetValue.ValueID = ValueXref.ValueXrefID

inner join Map.AttributeContext targetAttributeContext
on	targetAttributeContext.AttributeContextID = targetValue.AttributeContextID

inner join Map.Context targetContext
on	targetContext.ContextID = targetAttributeContext.ContextID

where
	Attribute.AttributeCode = 'EDANSTATUS'

and	not exists
	(
	select
		1
	from
		INFOLIVEDBITS.ReferenceMap.ETL.TImportReferenceDataXref
	where
		TImportReferenceDataXref.AttributeCode = Attribute.AttributeCode
	and	TImportReferenceDataXref.ContextCode = sourceContext.ContextCode
	and	TImportReferenceDataXref.ValueCode = sourceValue.ValueCode
	and	TImportReferenceDataXref.ContextXrefCode = targetContext.ContextCode		
	and	TImportReferenceDataXref.ValueXrefCode = targetValue.ValueCode
	)
