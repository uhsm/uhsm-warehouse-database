﻿--Values relating to a given attribute may be copied into the target ETL.TImportReferenceData table using an amended version of this utility script.

insert
into
	INFOLIVEDBITS.ReferenceMap.ETL.TImportReferenceData
	(
	AttributeCode
	,ContextCode
	,ValueCode
	,Value
	)
select
	Attribute.AttributeCode
	,Context.ContextCode
	,Value.ValueCode
	,Value.Value
from
	Map.Value

inner join Map.AttributeContext
on	AttributeContext.AttributeContextID = Value.AttributeContextID

inner join Map.Attribute
on	Attribute.AttributeID = AttributeContext.AttributeID

inner join Map.Context
on	Context.ContextID = AttributeContext.ContextID

where
	Attribute.AttributeCode = 'EDANSTATUS'

and	not exists
	(
	select
		1
	from
		INFOLIVEDBITS.ReferenceMap.ETL.TImportReferenceData
	where
		TImportReferenceData.AttributeCode = Attribute.AttributeCode
	and	TImportReferenceData.ContextCode = Context.ContextCode
	and	TImportReferenceData.ValueCode = Value.ValueCode
	and	TImportReferenceData.Value = Value.Value		
	)
