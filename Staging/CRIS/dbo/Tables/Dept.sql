﻿CREATE TABLE [dbo].[dept] (
    [code]      NVARCHAR (16) NULL,
    [sitecode]  NVARCHAR (24) NULL,
    [dept_name] NVARCHAR (80) NULL
);
