﻿CREATE TABLE [dbo].[actioncd] (
    [code]            NVARCHAR (24)  NULL,
    [description]     NVARCHAR (255) NULL,
    [actioncd_type]   NVARCHAR (2)   NULL,
    [requires_action] NVARCHAR (2)   NULL,
    [end_date]        datetime2      NULL
);

