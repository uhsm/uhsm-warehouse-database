﻿CREATE TABLE [dbo].[actioncomments] (
    [actioncomment_key] INT           NULL,
    [action_key]        INT           NULL,
    [session_key]       INT           NULL,
    [event_key]         INT           NULL,
    [entered_by]        NVARCHAR (20)  NULL,
    [date_entered]      datetime2      NULL,
    [date_modified]     datetime2      NULL,
    [freetxt]           NVARCHAR (MAX) NULL,
    [time_entered]      NVARCHAR (8)   NULL,
    [time_modified]     NVARCHAR (8)   NULL
);

