﻿CREATE TABLE [dbo].[actions] (
    [action_key]       INT          NULL,
    [session_key]      INT          NULL,
    [event_key]        INT          NULL,
    [action_code]      NVARCHAR (24) NULL,
    [assigned_to]      NVARCHAR (24) NULL,
    [completed_date]   datetime2     NULL,
    [completed_time]   NVARCHAR (8)  NULL,
    [actions_type]     NVARCHAR (2)  NULL,
    [required_by_date] datetime2     NULL
);

