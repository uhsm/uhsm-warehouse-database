﻿CREATE TABLE [dbo].[alarm] (
    [computer_number] INT            NULL,
    [recnum]          INT            NULL,
    [alarm_code]      NVARCHAR (2)    NULL,
    [severity]        NUMERIC (4, 2) NULL,
    [text_1]          NVARCHAR (60)   NULL,
    [text_2]          NVARCHAR (200)  NULL,
    [code]            NVARCHAR (16)   NULL,
    [uniqueid]        NVARCHAR (MAX)  NULL
);

