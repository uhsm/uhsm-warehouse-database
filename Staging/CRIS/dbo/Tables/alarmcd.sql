﻿CREATE TABLE [dbo].[alarmcd] (
    [code]         NVARCHAR (255)  NULL,
    [alarmcd_name] NVARCHAR (60)   NULL,
    [alarmcd_type] NVARCHAR (2)    NULL,
    [severity]     NUMERIC (4, 2) NULL,
    [end_date]     datetime2       NULL
);

