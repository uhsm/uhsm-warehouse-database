﻿CREATE TABLE [dbo].[autocheckinlog] (
    [date_retrieved] datetime2      NULL,
    [terminalid]     NVARCHAR (200) NULL,
    [zipped_log]     BINARY (255)  NULL
);

