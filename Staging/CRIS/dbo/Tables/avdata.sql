﻿CREATE TABLE [dbo].[avdata] (
    [avdata_key]      INT           NULL,
    [deleted]         NVARCHAR (2)   NULL,
    [avdata_type]     NVARCHAR (20)  NULL,
    [foreign_table]   NVARCHAR (20)  NULL,
    [foreign_key]     INT           NULL,
    [avdata_sequence] INT           NULL,
    [avdata_store]    NVARCHAR (255) NULL,
    [avdata_encoding] NVARCHAR (20)  NULL,
    [acquired_date]   datetime2      NULL,
    [acquired_time]   NVARCHAR (12)  NULL,
    [acquired_by]     NVARCHAR (20)  NULL,
    [last_access]     datetime2      NULL,
    [filename]        NVARCHAR (MAX) NULL
);

