﻿CREATE TABLE [dbo].[batbin] (
    [batch_key]     INT          NULL,
    [bin_key]       INT          NULL,
    [serial_no]     NVARCHAR (40) NULL,
    [qty_allocated] INT          NULL,
    [qty_remaining] INT          NULL
);

