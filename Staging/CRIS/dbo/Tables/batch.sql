﻿CREATE TABLE [dbo].[batch] (
    [batch_id]        NVARCHAR (12) NULL,
    [batch_name]      NVARCHAR (60) NULL,
    [no_patients]     INT          NULL,
    [date_last_added] datetime2     NULL,
    [patient_latest]  NVARCHAR (2)  NULL,
    [event_action]    NVARCHAR (2)  NULL,
    [creation_days]   INT          NULL
);

