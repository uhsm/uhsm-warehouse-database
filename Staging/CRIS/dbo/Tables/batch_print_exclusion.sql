﻿CREATE TABLE [dbo].[batch_print_exclusion] (
    [bpe_key]             INT          NULL,
    [bpe_trust]           NVARCHAR (24) NULL,
    [bpe_referral_source] NVARCHAR (48) NULL,
    [bpe_referrer]        NVARCHAR (48) NULL,
    [bpe_ward]            NVARCHAR (30) NULL,
    [bpe_patient_type]    NVARCHAR (2)  NULL,
    [bpe_modality]        NVARCHAR (2)  NULL,
    [bpe_reason]          NVARCHAR (16) NULL,
    [bpe_user_id]         NVARCHAR (20) NULL,
    [bpe_updated_date]    datetime2     NULL
);

