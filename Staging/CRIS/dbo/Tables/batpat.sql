﻿CREATE TABLE [dbo].[batpat] (
    [batch_id]         NVARCHAR (12) NULL,
    [computer_number]  INT          NULL,
    [batpat_temporary] NVARCHAR (2)  NULL,
    [event_key]        INT          NULL,
    [site]             NVARCHAR (24) NULL,
    [date_added]       datetime2     NULL,
    [time_added]       NVARCHAR (12) NULL,
    [added_by]         NVARCHAR (20) NULL,
    [report_key]       INT          NULL,
    [exam_key]         INT          NULL
);

