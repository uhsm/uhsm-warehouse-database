﻿CREATE TABLE [dbo].[column_config] (
    [table_type]   NVARCHAR (40)  NULL,
    [column_name]  NVARCHAR (60)  NULL,
    [script]       NVARCHAR (MAX) NULL,
    [column_width] INT           NULL
);

