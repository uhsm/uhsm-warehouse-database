﻿CREATE TABLE [dbo].[config] (
    [config_use]         NVARCHAR (60)  NULL,
    [key1]               NVARCHAR (20)  NULL,
    [key2]               NVARCHAR (20)  NULL,
    [config_order]       INT           NULL,
    [key3]               NVARCHAR (20)  NULL,
    [config_value]       NVARCHAR (MAX) NULL,
    [param1]             NVARCHAR (40)  NULL,
    [param2]             NVARCHAR (40)  NULL,
    [param3]             NVARCHAR (40)  NULL,
    [config_description] NVARCHAR (90)  NULL
);

