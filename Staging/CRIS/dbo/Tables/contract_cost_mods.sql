﻿CREATE TABLE [dbo].[contract_cost_mods] (
    [contract_cost_key]     INT             NULL,
    [contract_cost_mod_key] INT             NULL,
    [break_point]           INT             NULL,
    [break_point_type]      NVARCHAR (2)     NULL,
    [repeat_type]           NVARCHAR (2)     NULL,
    [modification_amount]   NUMERIC (10, 2) NULL,
    [modification_type]     NVARCHAR (2)     NULL,
    [apply_modification_to] NVARCHAR (2)     NULL,
    [start_date]            datetime2        NULL,
    [end_date]              datetime2        NULL
);

