﻿CREATE TABLE [dbo].[contract_link] (
    [contract_link_key]   INT NULL,
    [master_contract_key] INT NULL,
    [precedence]          INT NULL,
    [slave_contract_key]  INT NULL
);

