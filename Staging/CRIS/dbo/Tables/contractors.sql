﻿CREATE TABLE [dbo].[contractors] (
    [code]            NVARCHAR (16)  NULL,
    [external_id]     NVARCHAR (6)   NULL,
    [contractor_name] NVARCHAR (80)  NULL,
    [contact_name]    NVARCHAR (80)  NULL,
    [address_1]       NVARCHAR (200) NULL,
    [address_2]       NVARCHAR (200) NULL,
    [address_5]       NVARCHAR (60)  NULL,
    [postcode_1]      NVARCHAR (8)   NULL,
    [postcode_2]      NVARCHAR (8)   NULL,
    [telephone]       NVARCHAR (50)  NULL,
    [end_date]        datetime2      NULL,
    [address_3]       NVARCHAR (70)  NULL,
    [address_4]       NVARCHAR (60)  NULL,
    [fax]             NVARCHAR (50)  NULL
);

