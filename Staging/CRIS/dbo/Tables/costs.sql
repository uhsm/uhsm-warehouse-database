﻿CREATE TABLE [dbo].[costs] (
    [code]          NVARCHAR (16)   NULL,
    [site]          NVARCHAR (24)   NULL,
    [costs_year]    INT            NULL,
    [costs_name]    NVARCHAR (255)  NULL,
    [insurer]       NVARCHAR (48)   NULL,
    [request_cat]   NVARCHAR (2)    NULL,
    [policy_excess] NUMERIC (8, 2) NULL,
    [end_date]      datetime2       NULL
);

