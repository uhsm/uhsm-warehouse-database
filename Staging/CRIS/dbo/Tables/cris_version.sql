﻿CREATE TABLE [dbo].[cris_version] (
    [software_version] NVARCHAR (255) NULL,
    [software_date]    datetime2      NULL,
    [software_number]  INT           NULL
);

