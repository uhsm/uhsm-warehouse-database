﻿CREATE TABLE [dbo].[criserr] (
    [terminal_id] NVARCHAR (128) NULL,
    [user_id]     NVARCHAR (20)  NULL,
    [err_date]    datetime2      NULL,
    [err_time]    NVARCHAR (12)  NULL,
    [err_title]   NVARCHAR (60)  NULL,
    [build_date]  NVARCHAR (50)  NULL,
    [err_details] NVARCHAR (MAX) NULL
);

