﻿CREATE TABLE [dbo].[crisstt] (
    [code]             NVARCHAR (12)  NULL,
    [crisstt_type]     NVARCHAR (64)  NULL,
    [description]      NVARCHAR (MAX) NULL,
    [crisstt_category] NVARCHAR (4)   NULL
);

