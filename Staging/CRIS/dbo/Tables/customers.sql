﻿CREATE TABLE [dbo].[customers] (
    [code]                  NVARCHAR (16)  NULL,
    [customer_name]         NVARCHAR (80)  NULL,
    [customer_type]         NVARCHAR (20)  NULL,
    [contact_name]          NVARCHAR (80)  NULL,
    [address_1]             NVARCHAR (200) NULL,
    [address_2]             NVARCHAR (200) NULL,
    [address_5]             NVARCHAR (60)  NULL,
    [postcode_1]            NVARCHAR (8)   NULL,
    [postcode_2]            NVARCHAR (8)   NULL,
    [telephone]             NVARCHAR (50)  NULL,
    [end_date]              datetime2      NULL,
    [address_3]             NVARCHAR (70)  NULL,
    [address_4]             NVARCHAR (60)  NULL,
    [external_debtor_id]    NVARCHAR (64)  NULL,
    [external_debtor_class] NVARCHAR (64)  NULL,
    [payment_terms]         NVARCHAR (4)   NULL,
    [description]           NVARCHAR (60)  NULL
);

