﻿CREATE TABLE [dbo].[destination] (
    [source_trust]     NVARCHAR (24) NULL,
    [dest_trust]       NVARCHAR (24) NULL,
    [dest_site]        NVARCHAR (24) NULL,
    [max_ref_events]   INT          NULL,
    [types_to_send]    NVARCHAR (16) NULL,
    [max_days_old]     INT          NULL,
    [send_images]      NVARCHAR (2)  NULL,
    [transport_method] NVARCHAR (96) NULL
);

