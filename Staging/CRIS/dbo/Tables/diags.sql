﻿CREATE TABLE [dbo].[diags] (
    [diags_type] NVARCHAR (12)  NULL,
    [code]       NVARCHAR (16)  NULL,
    [diags_desc] NVARCHAR (MAX) NULL
);

