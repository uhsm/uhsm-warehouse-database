﻿CREATE TABLE [dbo].[dicomequipment] (
    [code]                         NVARCHAR (32)  NULL,
    [address]                      NVARCHAR (128) NULL,
    [ae_title]                     NVARCHAR (128) NULL,
    [port]                         INT           NULL,
    [dicomequipment_type]          NVARCHAR (24)  NULL,
    [dicomequipment_configuration] NVARCHAR (MAX) NULL,
    [equipment_code]               NVARCHAR (32)  NULL
);

