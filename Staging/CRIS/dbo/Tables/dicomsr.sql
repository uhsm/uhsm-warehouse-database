﻿CREATE TABLE [dbo].[dicomsr] (
    [accession_number]  NVARCHAR (48) NULL,
    [measurement]       NVARCHAR (48) NULL,
    [measurement_value] NVARCHAR (48) NULL,
    [fetus_number]      INT          NULL
);

