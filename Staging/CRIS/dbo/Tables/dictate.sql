﻿CREATE TABLE [dbo].[dictate] (
    [dictation_key]  INT            NULL,
    [exam_key]       INT            NULL,
    [report_type]    NVARCHAR (2)    NULL,
    [priority]       NVARCHAR (2)    NULL,
    [event_key]      INT            NULL,
    [dictate_by]     NVARCHAR (20)   NULL,
    [dictate_date]   datetime2       NULL,
    [dictate_time]   NVARCHAR (8)    NULL,
    [sec]            NVARCHAR (20)   NULL,
    [dictate_type]   NVARCHAR (12)   NULL,
    [dictate_length] NUMERIC (6, 2) NULL,
    [status]         NVARCHAR (12)   NULL,
    [started_date]   datetime2       NULL,
    [started_time]   NVARCHAR (8)    NULL,
    [started_by]     NVARCHAR (20)   NULL,
    [reported_by2]   NVARCHAR (20)   NULL,
    [keep]           NVARCHAR (2)    NULL,
    [keep_until]     datetime2       NULL
);

