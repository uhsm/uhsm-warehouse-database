﻿CREATE TABLE [dbo].[equipment] (
    [code]        NVARCHAR (32)  NULL,
    [site]        NVARCHAR (24)  NULL,
    [description] NVARCHAR (128) NULL,
    [room]        NVARCHAR (12)  NULL
);

