﻿CREATE TABLE [dbo].[events] (
    [event_key]          INT            NOT NULL,
    [deleted]            NVARCHAR (8)   NULL,
    [computer_number]    INT            NULL,
    [events_date]        datetime2  NULL,
    [events_time]        NVARCHAR (32)  NULL,
    [ward]               NVARCHAR (120) NULL,
    [site]               NVARCHAR (96)  NULL,
    [referal_source]     NVARCHAR (192) NULL,
    [patient_type]       NVARCHAR (8)   NULL,
    [request_cat]        NVARCHAR (8)   NULL,
    [referrer]           NVARCHAR (120) NULL,
    [date_booked]        datetime2  NULL,
    [urgent]             NVARCHAR (8)   NULL,
    [request_date]       datetime2  NULL,
    [intended_radiol]    NVARCHAR (80)  NULL,
    [speciality]         NVARCHAR (64)  NULL,
    [booking_type]       NVARCHAR (8)   NULL,
    [creation_date]      datetime2  NULL,
    [required_clinician] NVARCHAR (255) NULL,
    CONSTRAINT [PK_Events] PRIMARY KEY CLUSTERED ([event_key] ASC)
);





