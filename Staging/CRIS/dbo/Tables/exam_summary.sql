﻿CREATE TABLE [dbo].[exam_summary] (
    [practitioner]          NVARCHAR (20) NULL,
    [exam_key]              INT          NULL,
    [event_key]             INT          NULL,
    [exam_code]             NVARCHAR (16) NULL,
    [dictation_status]      NVARCHAR (12) NULL,
    [avdata]                NVARCHAR (2)  NULL,
    [summary_report_status] NVARCHAR (2)  NULL,
    [report_status]         NVARCHAR (2)  NULL,
    [intended_clinician]    NVARCHAR (20) NULL,
    [some_reported]         NVARCHAR (2)  NULL,
    [room]                  NVARCHAR (12) NULL,
    [exam_status]           NVARCHAR (2)  NULL,
    [accession]             NVARCHAR (40) NULL,
    [processed]             NVARCHAR (2)  NULL,
    [booked_time]           NVARCHAR (8)  NULL
);

