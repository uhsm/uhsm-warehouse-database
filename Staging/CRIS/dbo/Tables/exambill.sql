﻿CREATE TABLE [dbo].[exambill] (
    [exam_key]       INT            NULL,
    [exambill_cost]  NUMERIC (8, 2) NULL,
    [cost_reason]    NVARCHAR (48)   NULL,
    [bill]           NVARCHAR (2)    NULL,
    [creation_date]  datetime2       NULL,
    [user_id]        NVARCHAR (20)   NULL,
    [no_bill_reason] NVARCHAR (16)   NULL,
    [bill_comment]   NVARCHAR (MAX)  NULL,
    [referrer]       NVARCHAR (48)   NULL,
    [invoice_number] NVARCHAR (128)  NULL
);

