﻿CREATE TABLE [dbo].[examcd] (
	[code] NVARCHAR(16),
    [real_exam] NVARCHAR(2),
    [dwt_exam] NVARCHAR(2),
    [examcd_name] NVARCHAR(80),
    [modality] NVARCHAR(2),
    [interventional] NVARCHAR(2),
    [korner_value] NVARCHAR(4),
    [korner_code] NVARCHAR(8),
    [radiol_units] int,
    [radiog_units] int,
    [radiol_class] NVARCHAR(6),
    [radiog_class] NVARCHAR(6),
    [group_1] NVARCHAR(4),
    [group_2] NVARCHAR(4),
    [real_time] int,
    [lmp] NVARCHAR(4),
    [radiol_reqd] NVARCHAR(2),
    [anaesthetist] NVARCHAR(2),
    [nurse] NVARCHAR(2),
    [no_exams] int,
    [end_date] datetime2,
    [max_wait] int,
    [korn_wght] numeric(6,2),
    [dose_male] int,
    [dose_female] int,
    [auto_track] NVARCHAR(2),
    [scan_reason] NVARCHAR(16),
    [justify_level] NVARCHAR(2),
    [area] NVARCHAR(4),
    [korner_band] NVARCHAR(8),
    [arsac_limit] numeric(7,2),
    [waste] numeric(6,2),
    [letter_name] NVARCHAR(80),
    [snomedct] NVARCHAR(100),
);




