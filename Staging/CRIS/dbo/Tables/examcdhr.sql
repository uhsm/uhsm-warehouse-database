﻿CREATE TABLE [dbo].[examcdhr] (
    [hospital]     NVARCHAR (24) NULL,
    [examination]  NVARCHAR (16) NULL,
    [room]         NVARCHAR (12) NULL,
    [room_type]    NVARCHAR (2)  NULL,
    [examcdhr_key] INT          NULL
);

