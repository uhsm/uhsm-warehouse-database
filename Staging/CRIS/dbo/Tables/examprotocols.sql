﻿CREATE TABLE [dbo].[examprotocols] (
    [protocol_key] INT           NULL,
    [examination]  NVARCHAR (16)  NULL,
    [modality]     NVARCHAR (2)   NULL,
    [area]         NVARCHAR (4)   NULL,
    [uniqueid]     NVARCHAR (MAX) NULL
);

