﻿CREATE TABLE [dbo].[exams] (
    [exam_key]       INT           NULL,
    [event_key]      INT           NULL,
    [examination]    NVARCHAR (16) NULL,
    [deleted]        NVARCHAR (2)  NULL,
    [room]           NVARCHAR (12) NULL,
    [radiologist]    NVARCHAR (20) NULL,
    [exams_date]     datetime2 NULL,
    [start_time]     NVARCHAR (8)  NULL,
    [radiographer_1] NVARCHAR (20) NULL,
    [radiographer_2] NVARCHAR (20) NULL,
    [radiographer_3] NVARCHAR (20) NULL,
    [booked_time]    NVARCHAR (8)  NULL,
    [booked_date]    datetime2 NULL,
    [creation_date]  datetime2 NULL,
    [accession]      NVARCHAR (40) NULL
);
