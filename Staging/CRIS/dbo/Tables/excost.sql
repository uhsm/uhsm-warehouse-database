﻿CREATE TABLE [dbo].[excost] (
    [code]        NVARCHAR (16)   NULL,
    [examination] NVARCHAR (16)   NULL,
    [total_in]    NUMERIC (8, 2) NULL,
    [total_out]   NUMERIC (8, 2) NULL,
    [cost_out]    NUMERIC (8, 2) NULL,
    [cost1]       NUMERIC (8, 2) NULL,
    [cost2]       NUMERIC (8, 2) NULL,
    [cost3]       NUMERIC (8, 2) NULL,
    [cost4]       NUMERIC (8, 2) NULL,
    [cost5]       NUMERIC (8, 2) NULL,
    [cost6]       NUMERIC (8, 2) NULL,
    [costband]    NVARCHAR (16)   NULL,
    [uniqueid]    NVARCHAR (32)   NULL
);

