﻿CREATE TABLE [dbo].[excostadd] (
    [excostadd_policy] NVARCHAR (16)   NULL,
    [examination]      NVARCHAR (16)   NULL,
    [cost_reason]      NVARCHAR (48)   NULL,
    [excostadd_cost]   NUMERIC (8, 2) NULL,
    [uniqueid]         NVARCHAR (80)   NULL
);

