﻿CREATE TABLE [dbo].[excostband] (
    [examcostcode]    NVARCHAR (48)   NULL,
    [band]            NVARCHAR (16)   NULL,
    [excostband_cost] NUMERIC (8, 2) NULL,
    [uniqueid]        NVARCHAR (64)   NULL
);

