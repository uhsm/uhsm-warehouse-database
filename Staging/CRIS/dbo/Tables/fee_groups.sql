﻿CREATE TABLE [dbo].[fee_groups] (
    [code]        NVARCHAR (48)  NULL,
    [description] NVARCHAR (128) NULL,
    [group_type]  NVARCHAR (2)   NULL,
    [end_date]    datetime2      NULL
);

