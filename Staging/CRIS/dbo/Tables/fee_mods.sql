﻿CREATE TABLE [dbo].[fee_mods] (
    [fee_mod_key]           INT             NULL,
    [fee_key]               INT             NULL,
    [end_date]              datetime2        NULL,
    [break_point]           INT             NULL,
    [repeat_type]           NVARCHAR (2)     NULL,
    [modification_type]     NVARCHAR (2)     NULL,
    [apply_modification_to] NVARCHAR (2)     NULL,
    [modification_amount]   NUMERIC (10, 2) NULL,
    [start_date]            datetime2        NULL
);

