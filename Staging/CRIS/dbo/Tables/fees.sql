﻿CREATE TABLE [dbo].[fees] (
    [fee_key]                 INT             NULL,
    [party_type]              NVARCHAR (2)     NULL,
    [counter_reset_frequency] NVARCHAR (2)     NULL,
    [break_point_type]        NVARCHAR (2)     NULL,
    [contract_cost_key]       INT             NULL,
    [fee_type]                NVARCHAR (2)     NULL,
    [party]                   NVARCHAR (48)    NULL,
    [description]             NVARCHAR (255)   NULL,
    [charge_type]             NVARCHAR (2)     NULL,
    [amount]                  NUMERIC (10, 2) NULL,
    [start_date]              datetime2        NULL,
    [end_date]                datetime2        NULL,
    [contract_key]            INT             NULL,
    [fee_level]               NVARCHAR (16)    NULL,
    [external_code]           NVARCHAR (16)    NULL,
    [sites]                   NVARCHAR (255)   NULL,
    [item_type]               NVARCHAR (2)     NULL,
    [item_key]                NVARCHAR (32)    NULL
);

