﻿CREATE TABLE [dbo].[filing] (
    [cris_number]    INT          NULL,
    [volume_key]     INT          NULL,
    [site]           NVARCHAR (24) NULL,
    [date_allocated] datetime2     NULL,
    [time_allocated] NVARCHAR (8)  NULL,
    [filing_info]    NVARCHAR (30) NULL,
    [filing_current] NVARCHAR (2)  NULL
);

