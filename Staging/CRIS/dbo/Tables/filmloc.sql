﻿CREATE TABLE [dbo].[filmloc] (
    [code]         NVARCHAR (32) NULL,
    [filmloc_name] NVARCHAR (80) NULL,
    [site]         NVARCHAR (24) NULL,
    [filmloc_type] NVARCHAR (2)  NULL,
    [end_date]     datetime2     NULL
);

