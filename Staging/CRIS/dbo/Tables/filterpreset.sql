﻿CREATE TABLE [dbo].[filterpreset] (
    [fp_key]   INT           NULL,
    [fp_list]  NVARCHAR (20)  NULL,
    [fp_name]  NVARCHAR (100) NULL,
    [fp_props] NVARCHAR (MAX) NULL,
    [fp_site]  NVARCHAR (24)  NULL,
    [fp_trust] NVARCHAR (20)  NULL,
    [fp_user]  NVARCHAR (20)  NULL,
    [fp_vis]   NVARCHAR (12)  NULL
);

