﻿CREATE TABLE [dbo].[flexdat] (
    [exam_key]      INT           NULL,
    [field]         NVARCHAR (16)  NULL,
    [flexdat_value] NVARCHAR (32)  NULL,
    [flexdat_order] INT           NULL,
    [uniqueid]      NVARCHAR (100) NULL
);

