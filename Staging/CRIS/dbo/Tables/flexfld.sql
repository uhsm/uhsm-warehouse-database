﻿CREATE TABLE [dbo].[flexfld] (
    [code]               NVARCHAR (16)  NULL,
    [flexfld_name]       NVARCHAR (140) NULL,
    [brief]              NVARCHAR (64)  NULL,
    [help]               NVARCHAR (140) NULL,
    [prompt]             NVARCHAR (20)  NULL,
    [flexfld_validation] NVARCHAR (140) NULL,
    [calc]               NVARCHAR (140) NULL,
    [round]              NVARCHAR (2)   NULL,
    [flexfld_type]       NVARCHAR (2)   NULL,
    [mandatory]          NVARCHAR (2)   NULL,
    [flexfld_group]      NVARCHAR (8)   NULL,
    [suffix]             NVARCHAR (20)  NULL,
    [prompt_type]        NVARCHAR (2)   NULL,
    [fld_type]           NVARCHAR (2)   NULL
);

