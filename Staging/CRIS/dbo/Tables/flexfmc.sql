﻿CREATE TABLE [dbo].[flexfmc] (
    [code]          NVARCHAR (16) NULL,
    [field]         NVARCHAR (16) NULL,
    [flexfmc_order] INT          NULL,
    [end_date]      datetime2     NULL,
    [x_pos]         INT          NULL,
    [y_pos]         INT          NULL,
    [disp_suffix]   NVARCHAR (2)  NULL,
    [disp_name]     NVARCHAR (2)  NULL,
    [value_pos]     NVARCHAR (2)  NULL,
    [in_comments]   NVARCHAR (2)  NULL,
    [x_width]       INT          NULL,
    [uniqueid]      NVARCHAR (32) NULL
);

