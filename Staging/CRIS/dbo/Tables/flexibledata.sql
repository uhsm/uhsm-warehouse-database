﻿CREATE TABLE [dbo].[flexibledata] (
    [root]                NVARCHAR (200) NULL,
    [code]                NVARCHAR (200) NULL,
    [flexibledata_parent] NVARCHAR (200) NULL,
    [flexibledata_key]    NVARCHAR (200) NULL,
    [flexibledata_value]  NVARCHAR (MAX) NULL,
    [path]                NVARCHAR (MAX) NULL,
    [item]                NUMERIC (22)  NULL
);

