﻿CREATE TABLE [dbo].[fmeas] (
    [code]     NVARCHAR (12)   NULL,
    [measure]  NUMERIC (6, 2) NULL,
    [days]     INT            NULL,
    [years]    INT            NULL,
    [weeks]    INT            NULL,
    [sd]       NUMERIC (8, 4) NULL,
    [sdtype]   NVARCHAR (2)    NULL,
    [uniqueid] NVARCHAR (40)   NULL
);

