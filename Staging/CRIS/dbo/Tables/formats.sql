﻿CREATE TABLE [dbo].[formats] (
    [formats_name]  NVARCHAR (100) NULL,
    [fmt_protected] NVARCHAR (2)   NULL,
    [format]        NVARCHAR (MAX) NULL,
    [fmt_version]   NVARCHAR (16)  NULL
);

