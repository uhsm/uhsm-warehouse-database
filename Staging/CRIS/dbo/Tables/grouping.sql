﻿CREATE TABLE [dbo].[grouping] (
    [code]        NVARCHAR (48)  NULL,
    [description] NVARCHAR (128) NULL,
    [end_date]    datetime2      NULL,
    [grouping_id] INT           NULL
);

