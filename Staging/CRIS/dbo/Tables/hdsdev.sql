﻿CREATE TABLE [dbo].[hdsdev] (
    [device_id]      NVARCHAR (20)  NULL,
    [hdsdev_name]    NVARCHAR (80)  NULL,
    [print_mode]     NVARCHAR (2)   NULL,
    [print_command]  NVARCHAR (120) NULL,
    [command_mode]   NVARCHAR (2)   NULL,
    [pre_print_os]   NVARCHAR (120) NULL,
    [post_print_os]  NVARCHAR (120) NULL,
    [pre_print_str]  NVARCHAR (40)  NULL,
    [post_print_str] NVARCHAR (40)  NULL,
    [os_device]      NVARCHAR (40)  NULL,
    [printer_type]   NVARCHAR (20)  NULL
);

