﻿CREATE TABLE [dbo].[hdslmt] (
    [tablecode]     NVARCHAR (16) NULL,
    [description]   NVARCHAR (80) NULL,
    [valtxt_1]      NVARCHAR (30) NULL,
    [valtxt_2]      NVARCHAR (30) NULL,
    [valtxt_3]      NVARCHAR (30) NULL,
    [valtxt_4]      NVARCHAR (30) NULL,
    [valtxt_5]      NVARCHAR (30) NULL,
    [valtxt_6]      NVARCHAR (30) NULL,
    [valtxt_7]      NVARCHAR (30) NULL,
    [valtxt_8]      NVARCHAR (30) NULL,
    [valtxt_9]      NVARCHAR (30) NULL,
    [valchar_1]     NVARCHAR (2)  NULL,
    [valchar_2]     NVARCHAR (2)  NULL,
    [valchar_3]     NVARCHAR (2)  NULL,
    [valchar_4]     NVARCHAR (2)  NULL,
    [valchar_5]     NVARCHAR (2)  NULL,
    [valchar_6]     NVARCHAR (2)  NULL,
    [valchar_7]     NVARCHAR (2)  NULL,
    [valchar_8]     NVARCHAR (2)  NULL,
    [valchar_9]     NVARCHAR (2)  NULL,
    [val_sel_mode]  NVARCHAR (2)  NULL,
    [val_default]   INT          NULL,
    [deleted]       NVARCHAR (2)  NULL,
    [creation_date] datetime2     NULL,
    [creation_time] INT          NULL,
    [mutation_date] datetime2     NULL,
    [mutation_time] INT          NULL
);

