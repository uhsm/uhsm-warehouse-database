﻿CREATE TABLE [dbo].[hdstbc] (
    [table_number]  NVARCHAR (16)  NULL,
    [entry_number]  INT           NULL,
    [description]   NVARCHAR (255) NULL,
    [return_value]  NVARCHAR (48)  NULL,
    [value_1]       NVARCHAR (48)  NULL,
    [value_2]       NVARCHAR (48)  NULL,
    [value_3]       NVARCHAR (48)  NULL,
    [value_4]       NVARCHAR (48)  NULL,
    [value_5]       NVARCHAR (48)  NULL,
    [value_6]       NVARCHAR (48)  NULL,
    [value_7]       NVARCHAR (48)  NULL,
    [value_8]       NVARCHAR (48)  NULL,
    [value_9]       NVARCHAR (48)  NULL,
    [value_10]      NVARCHAR (48)  NULL,
    [deleted]       NVARCHAR (2)   NULL,
    [creation_date] datetime2      NULL,
    [creation_time] INT           NULL,
    [mutation_date] datetime2      NULL,
    [mutation_time] INT           NULL
);

