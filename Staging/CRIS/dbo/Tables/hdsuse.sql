﻿CREATE TABLE [dbo].[hdsuse] (
	[user_id] [nvarchar](10) NOT NULL,
	[hdsuse_name] [nvarchar](40) NOT NULL,
	[deleted] [nvarchar](1) NOT NULL,
	[login_id] [nvarchar](30) NOT NULL,
	[hdsuse_type] [nvarchar](1) NULL,
	[user_group] [nvarchar](1) NULL,
	[hdsuse_level] [int] NULL,
	[ini_file] [nvarchar](40) NULL,
	[creation_date] [datetime2](7) NULL,
	[creation_time] [int] NULL,
	[mutation_date] [datetime2](7) NULL,
	[mutation_time] [int] NULL,
	[password_expiry] [datetime2](7) NULL,
	[user_access] [nvarchar](2048) NULL,
	[mod_date] [datetime2](7) NULL,
	[national_id] [nvarchar](12) NULL,
	[spine_roles] [nvarchar](4000) NULL,
	[spine_roles_date] [datetime2](7) NULL,
	[locking_user] [nvarchar](10) NULL,
	[locked_date] [datetime2](7) NULL,
	[site_code] [nvarchar](100) NULL,
	[valid_from] [datetime2](7) NULL,
	[valid_to] [datetime2](7) NULL
);

