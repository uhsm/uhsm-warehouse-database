﻿CREATE TABLE [dbo].[hiscontacts] (
    [paslink_key]      INT           NULL,
    [date_changed]     datetime2      NULL,
    [hiscontacts_type] NVARCHAR (4)   NULL,
    [surname]          NVARCHAR (128) NULL,
    [forenames]        NVARCHAR (128) NULL,
    [dob]              datetime2      NULL,
    [address_1]        NVARCHAR (128) NULL,
    [address_2]        NVARCHAR (128) NULL,
    [address_3]        NVARCHAR (128) NULL,
    [address_4]        NVARCHAR (128) NULL,
    [post_1]           NVARCHAR (8)   NULL,
    [post_2]           NVARCHAR (6)   NULL,
    [telephone]        NVARCHAR (50)  NULL,
    [tel_mobile]       NVARCHAR (50)  NULL,
    [tel_work]         NVARCHAR (50)  NULL,
    [relationship]     NVARCHAR (50)  NULL,
    [uniqueid]         NVARCHAR (60)  NULL
);

