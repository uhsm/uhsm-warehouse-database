﻿CREATE TABLE [dbo].[hisepi] (
    [episode_number] NVARCHAR (30) NULL,
    [unit_number]    NVARCHAR (30) NULL,
    [paslink_key]    INT          NULL,
    [hospital]       NVARCHAR (24) NULL,
    [ward_code]      NVARCHAR (30) NULL,
    [admission_date] datetime2     NULL,
    [consultant]     NVARCHAR (20) NULL,
    [referrer]       NVARCHAR (48) NULL
);

