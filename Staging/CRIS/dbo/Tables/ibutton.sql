﻿CREATE TABLE [dbo].[ibutton] (
    [buttonid]        NVARCHAR (32) NULL,
    [ibutton_mapping] NVARCHAR (20) NULL,
    [ibutton_type]    NVARCHAR (2)  NULL,
    [allocated]       datetime2     NULL,
    [lost]            datetime2     NULL,
    [lastlogin]       datetime2     NULL,
    [logouttime]      NVARCHAR (8)  NULL
);

