﻿CREATE TABLE [dbo].[insurers] (
    [code]          NVARCHAR (48)  NULL,
    [insurers_name] NVARCHAR (255) NULL,
    [address_1]     NVARCHAR (128) NULL,
    [address_2]     NVARCHAR (128) NULL,
    [address_3]     NVARCHAR (128) NULL,
    [address_4]     NVARCHAR (128) NULL,
    [address_5]     NVARCHAR (128) NULL,
    [postcode_1]    NVARCHAR (8)   NULL,
    [postcode_2]    NVARCHAR (6)   NULL,
    [telephone]     NVARCHAR (48)  NULL,
    [fax]           NVARCHAR (48)  NULL,
    [email]         NVARCHAR (255) NULL,
    [end_date]      datetime2      NULL
);

