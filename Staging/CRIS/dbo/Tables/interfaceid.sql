﻿CREATE TABLE [dbo].[interfaceid] (
    [id_system_type]   NVARCHAR (128) NULL,
    [cris_system_id]   INT           NULL,
    [remote_system_id] NVARCHAR (255) NULL,
    [date_sent]        datetime2      NULL,
    [uniqueid]         NVARCHAR (200) NULL
);

