﻿CREATE TABLE [dbo].[intlog] (
    [intlog_interface] NVARCHAR (48)  NULL,
    [direction]        NVARCHAR (2)   NULL,
    [message_id]       NVARCHAR (255) NULL,
    [transmit_date]    datetime2      NULL,
    [transmit_time]    NVARCHAR (12)  NULL,
    [rcv_date]         datetime2      NULL,
    [rcv_time]         NVARCHAR (12)  NULL,
    [detail]           NVARCHAR (255) NULL
);

