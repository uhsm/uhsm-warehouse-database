﻿CREATE TABLE [dbo].[intproc] (
    [post_date]     datetime2      NULL,
    [post_time]     NVARCHAR (12)  NULL,
    [process_id]    BIGINT        NULL,
    [trigger_id]    BIGINT        NULL,
    [intproc_queue] NVARCHAR (60)  NULL,
    [process_date]  datetime2      NULL,
    [process_time]  NVARCHAR (12)  NULL,
    [intproc_error] NVARCHAR (2)   NULL,
    [detail]        NVARCHAR (255) NULL,
    [transmit_date] datetime2      NULL,
    [transmit_time] NVARCHAR (12)  NULL,
    [transmit_ok]   NVARCHAR (2)   NULL,
    [ackmsg]        NVARCHAR (MAX) NULL,
    [ebmsgid]       NVARCHAR (255) NULL,
    [rcvack_date]   datetime2      NULL,
    [rcvack_time]   NVARCHAR (12)  NULL,
    [hl7msgid]      NVARCHAR (255) NULL
);

