﻿CREATE TABLE [dbo].[inttodo] (
    [inttodo_key]    BIGINT        NULL,
    [date_due]       datetime2      NULL,
    [time_due]       NVARCHAR (18)  NULL,
    [event_type]     NVARCHAR (40)  NULL,
    [inttodo_source] NVARCHAR (20)  NULL,
    [message_body]   NVARCHAR (MAX) NULL,
    [destination]    NVARCHAR (40)  NULL,
    [processed]      NVARCHAR (2)   NULL,
    [userid]         NVARCHAR (80)  NULL,
    [workstation]    NVARCHAR (80)  NULL,
    [event_date]     datetime2      NULL,
    [event_time]     NVARCHAR (20)  NULL,
    [cris_number]    INT           NULL,
    [recnum]         INT           NULL,
    [exam_key]       INT           NULL,
    [event_key]      INT           NULL
);

