﻿CREATE TABLE [dbo].[invoices] (
    [invoice_key]        INT             NULL,
    [invoice_id]         NVARCHAR (128)   NULL,
    [contract_key]       INT             NULL,
    [invoice_date]       datetime2        NULL,
    [invoice_time]       NVARCHAR (8)     NULL,
    [authorised_by]      NVARCHAR (20)    NULL,
    [last_printed_date]  datetime2        NULL,
    [last_printed_by]    NVARCHAR (20)    NULL,
    [transfer_exception] NVARCHAR (MAX)   NULL,
    [exchange_rate]      NUMERIC (12, 4) NULL,
    [creation_date]      datetime2        NULL,
    [creation_time]      NVARCHAR (8)     NULL,
    [customer_reference] NVARCHAR (40)    NULL
);

