﻿CREATE TABLE [dbo].[isotope] (
    [code]          NVARCHAR (20) NULL,
    [isotope_name]  NVARCHAR (60) NULL,
    [isotope_group] NVARCHAR (16) NULL,
    [half_life]     BIGINT       NULL
);

