﻿CREATE TABLE [dbo].[jms_messages] (
    [destination] NVARCHAR (255) NULL,
    [messageid]   BIGINT        NULL,
    [messageblob] BINARY (255)  NULL,
    [txop]        NVARCHAR (2)   NULL,
    [txid]        BIGINT        NULL,
    [uniqueid]    NVARCHAR (MAX) NULL
);

