﻿CREATE TABLE [dbo].[locking] (
    [computer_number] NUMERIC (22)  NULL,
    [lock_type]       NVARCHAR (16)  NULL,
    [user_id]         NVARCHAR (20)  NULL,
    [lock_datetime2]   datetime2      NULL,
    [alternate_key]   NUMERIC (22)  NULL,
    [event_key]       INT           NULL,
    [terminal_id]     NVARCHAR (128) NULL,
    [uniqueid]        NVARCHAR (200) NULL
);

