﻿CREATE TABLE [dbo].[log] (
    [log_user]     NVARCHAR (20)  NULL,
    [log_category] NVARCHAR (255) NULL,
    [message]      NVARCHAR (MAX) NULL,
    [workstation]  NVARCHAR (96)  NULL,
    [cris_number]  INT           NULL,
    [paslink_key]  INT           NULL,
    [log_level]    NVARCHAR (24)  NULL,
    [entry_date]   datetime2      NULL,
    [entry_time]   NVARCHAR (12)  NULL,
    [log_type]     NVARCHAR (255) NULL,
    [key1]         INT           NULL,
    [key2]         INT           NULL,
    [key3]         INT           NULL,
    [key4]         INT           NULL,
    [trust]        NVARCHAR (24)  NULL,
    [sub_category] NVARCHAR (48)  NULL,
    [details]      NVARCHAR (MAX) NULL,
    [details_type] NVARCHAR (24)  NULL,
    [uniqueid]     NVARCHAR (100) NULL
);

