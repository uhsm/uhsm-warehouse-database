﻿CREATE TABLE [dbo].[lrstatus] (
    [lr_key]               NUMERIC (22) NULL,
    [nhsnumber]            NVARCHAR (24) NULL,
    [lrid]                 NVARCHAR (96) NULL,
    [workgroup]            NVARCHAR (64) NULL,
    [lrstatus]             NVARCHAR (96) NULL,
    [event_key]            NVARCHAR (48) NULL,
    [claimed]              datetime2     NULL,
    [statusupdated]        datetime2     NULL,
    [confirmed]            datetime2     NULL,
    [user_id]              NVARCHAR (20) NULL,
    [user_role_profile_id] NVARCHAR (64) NULL,
    [order_key]            INT          NULL
);

