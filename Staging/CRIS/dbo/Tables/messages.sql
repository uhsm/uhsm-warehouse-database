﻿CREATE TABLE [dbo].[messages] (
    [messages_key] NUMERIC (22)  NULL,
    [message_date] datetime2      NULL,
    [popup]        NVARCHAR (2)   NULL,
    [isread]       NVARCHAR (2)   NULL,
    [to_user]      NVARCHAR (28)  NULL,
    [from_system]  NVARCHAR (255) NULL,
    [from_user]    NVARCHAR (255) NULL,
    [msg_subject]  NVARCHAR (MAX) NULL,
    [msg_content]  NVARCHAR (MAX) NULL,
    [event_key]    INT           NULL
);

