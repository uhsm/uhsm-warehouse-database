﻿CREATE TABLE [dbo].[museum] (
    [code]         NVARCHAR (8)  NULL,
    [museum_name]  NVARCHAR (80) NULL,
    [museum_group] NVARCHAR (4)  NULL,
    [end_date]     datetime2     NULL
);

