﻿CREATE TABLE [dbo].[nmcont] (
    [nmcont_date]  datetime2     NULL,
    [code]         NVARCHAR (20) NULL,
    [nmcont_type]  NVARCHAR (20) NULL,
    [contaminated] NVARCHAR (2)  NULL,
    [reading]      INT          NULL,
    [nmcont_user]  NVARCHAR (20) NULL
);

