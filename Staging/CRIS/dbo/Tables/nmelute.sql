﻿CREATE TABLE [dbo].[nmelute] (
    [elution_key]     INT             NULL,
    [generator_key]   INT             NULL,
    [eluted_by]       NVARCHAR (20)    NULL,
    [volume]          NUMERIC (6, 2)  NULL,
    [elution_date]    datetime2        NULL,
    [expiry_date]     datetime2        NULL,
    [activity_per_ml] NUMERIC (10, 2) NULL,
    [minimum_temp]    INT             NULL,
    [maximum_temp]    INT             NULL,
    [mbc]             INT             NULL
);

