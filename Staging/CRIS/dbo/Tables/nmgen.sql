﻿CREATE TABLE [dbo].[nmgen] (
    [generator_key] INT            NULL,
    [serial_no]     NVARCHAR (60)   NULL,
    [isotope]       NVARCHAR (20)   NULL,
    [delivery_date] datetime2       NULL,
    [return_date]   datetime2       NULL,
    [ref_activity]  NUMERIC (6, 2) NULL,
    [ref_date]      datetime2       NULL
);

