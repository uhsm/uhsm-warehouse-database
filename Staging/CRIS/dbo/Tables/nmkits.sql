﻿CREATE TABLE [dbo].[nmkits] (
    [kit_key]     INT          NULL,
    [chemical]    NVARCHAR (20) NULL,
    [lot_number]  NVARCHAR (40) NULL,
    [license]     NVARCHAR (40) NULL,
    [expiry_date] datetime2     NULL
);

