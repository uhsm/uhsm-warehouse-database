﻿CREATE TABLE [dbo].[nmphial] (
    [disposed]       NVARCHAR (2)     NULL,
    [phial_key]      INT             NULL,
    [elution_key]    INT             NULL,
    [kit_key]        INT             NULL,
    [phial_serial]   NVARCHAR (60)    NULL,
    [site]           NVARCHAR (24)    NULL,
    [isotope]        NVARCHAR (20)    NULL,
    [chemical]       NVARCHAR (20)    NULL,
    [current_volume] NUMERIC (6, 2)  NULL,
    [init_volume]    NUMERIC (6, 2)  NULL,
    [init_activity]  NUMERIC (10, 2) NULL,
    [activity]       NUMERIC (10, 2) NULL,
    [activity_date]  datetime2        NULL,
    [volume]         NUMERIC (6, 2)  NULL,
    [creation_date]  datetime2        NULL,
    [creation_time]  NVARCHAR (8)     NULL,
    [act_disposed]   INT             NULL,
    [dispose_date]   datetime2        NULL,
    [dispose_time]   NVARCHAR (8)     NULL,
    [disposed_by]    NVARCHAR (20)    NULL
);

