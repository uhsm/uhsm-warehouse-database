﻿CREATE TABLE [dbo].[obsconfig] (
    [database_field] NVARCHAR (64)  NULL,
    [display_label]  NVARCHAR (255) NULL,
    [scanreason]     NVARCHAR (16)  NULL,
    [field]          NVARCHAR (248) NULL,
    [field_type]     NVARCHAR (2)   NULL,
    [lookuptable]    NVARCHAR (16)  NULL,
    [lookuptype]     NVARCHAR (2)   NULL,
    [label]          NVARCHAR (255) NULL,
    [uniqueid]       NVARCHAR (MAX) NULL
);

