﻿CREATE TABLE [dbo].[outmsg] (
    [destination]     NVARCHAR (20)    NULL,
    [message_name]    NVARCHAR (40)    NULL,
    [creation_date]   datetime2        NULL,
    [creation_time]   NVARCHAR (16)    NULL,
    [status]          NVARCHAR (8)     NULL,
    [computer_number] INT             NULL,
    [event_key]       INT             NULL,
    [exam_key]        INT             NULL,
    [report_key]      INT             NULL,
    [aux_string_1]    NVARCHAR (64)    NULL,
    [aux_date_1]      datetime2        NULL,
    [aux_num_1]       NUMERIC (10, 2) NULL,
    [aux_string_2]    NVARCHAR (60)    NULL,
    [aux_date_2]      datetime2        NULL,
    [aux_num_2]       NUMERIC (10, 2) NULL,
    [paslink_key]     INT             NULL,
    [priority]        INT             NULL,
    [unqiueid]        NVARCHAR (200)   NULL
);

