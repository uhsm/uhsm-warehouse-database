﻿CREATE TABLE [dbo].[paper] (
    [paper_name]      NVARCHAR (80) NULL,
    [paper_width]     NUMERIC (22) NULL,
    [paper_height]    NUMERIC (22) NULL,
    [paper_x]         NUMERIC (22) NULL,
    [paper_y]         NUMERIC (22) NULL,
    [paper_im_width]  NUMERIC (22) NULL,
    [paper_im_height] NUMERIC (22) NULL
);

