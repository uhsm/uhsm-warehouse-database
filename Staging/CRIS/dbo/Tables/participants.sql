﻿CREATE TABLE [dbo].[participants] (
    [participant_key] INT          NULL,
    [session_key]     INT          NULL,
    [participant_id]  NVARCHAR (24) NULL,
    [attended]        NVARCHAR (2)  NULL
);

