﻿CREATE TABLE [dbo].[patientpolicies] (
    [computer_number] INT           NULL,
    [billing_type]    NVARCHAR (16)  NULL,
    [policy_number]   NVARCHAR (128) NULL,
    [policy_holder]   NVARCHAR (255) NULL,
    [isdefault]       NVARCHAR (2)   NULL,
    [insurer]         NVARCHAR (48)  NULL
);

