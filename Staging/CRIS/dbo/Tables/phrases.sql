﻿CREATE TABLE [dbo].[phrases] (
    [code]         NVARCHAR (24)  NULL,
    [module]       NVARCHAR (2)   NULL,
    [diagnosis]    NVARCHAR (16)  NULL,
    [phrase]       NVARCHAR (MAX) NULL,
    [phrases_user] NVARCHAR (20)  NULL,
    [phrases_name] NVARCHAR (60)  NULL,
    [verify]       NVARCHAR (2)   NULL,
    [trust]        NVARCHAR (24)  NULL
);

