﻿CREATE TABLE [dbo].[porter] (
    [porter_key]      INT           NULL,
    [status]          INT           NULL,
    [collection_date] datetime2      NULL,
    [collection_time] NVARCHAR (8)   NULL,
    [collection_ampm] NVARCHAR (2)   NULL,
    [tba_by]          NVARCHAR (2)   NULL,
    [porter]          NVARCHAR (20)  NULL,
    [requested_by]    NVARCHAR (20)  NULL,
    [computer_number] INT           NULL,
    [event_key]       INT           NULL,
    [porter_location] NVARCHAR (36)  NULL,
    [priority]        INT           NULL,
    [destination]     NVARCHAR (36)  NULL,
    [notes]           NVARCHAR (100) NULL,
    [start_time]      NVARCHAR (8)   NULL,
    [completion_time] NVARCHAR (8)   NULL,
    [mobility]        NVARCHAR (2)   NULL,
    [direction]       NVARCHAR (2)   NULL,
    [site]            NVARCHAR (24)  NULL
);

