﻿CREATE TABLE [dbo].[postcde] (
    [postcode_1] NVARCHAR (8)  NULL,
    [postcode_2] NVARCHAR (6)  NULL,
    [address_1]  NVARCHAR (70) NULL,
    [address_2]  NVARCHAR (70) NULL,
    [address_3]  NVARCHAR (70) NULL,
    [address_4]  NVARCHAR (70) NULL,
    [dha_code]   NVARCHAR (6)  NULL,
    [grid_north] NVARCHAR (10) NULL,
    [grid_east]  NVARCHAR (10) NULL,
    [ward_code]  NVARCHAR (16) NULL,
    [pct]        NVARCHAR (12) NULL,
    [uniqueid]   NVARCHAR (20) NULL
);

