﻿CREATE TABLE [dbo].[preg] (
    [preg_key]        INT          NULL,
    [computer_number] INT          NULL,
    [end_date]        datetime2     NULL,
    [start_date]      datetime2     NULL,
    [num_foetus]      INT          NULL,
    [update_date]     datetime2     NULL,
    [edd_source]      NVARCHAR (12) NULL,
    [edd_user]        NVARCHAR (20) NULL,
    [edd_setdate]     datetime2     NULL,
    [edd_validated]   NVARCHAR (2)  NULL,
    [outcome_event]   INT          NULL,
    [outcome_wks]     INT          NULL,
    [outcome_days]    INT          NULL,
    [outcome_date]    datetime2     NULL,
    [outcome_loc]     NVARCHAR (8)  NULL,
    [surname]         NVARCHAR (40) NULL,
    [lmp]             datetime2     NULL
);

