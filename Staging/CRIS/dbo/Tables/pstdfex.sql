﻿CREATE TABLE [dbo].[pstdfex] (
    [site]          NVARCHAR (24)   NULL,
    [room]          NVARCHAR (12)   NULL,
    [examination]   NVARCHAR (16)   NULL,
    [contrast_code] NVARCHAR (16)   NULL,
    [quantity_used] INT            NULL,
    [concentration] NUMERIC (6, 2) NULL
);

