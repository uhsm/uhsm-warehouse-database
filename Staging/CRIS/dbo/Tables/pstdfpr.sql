﻿CREATE TABLE [dbo].[pstdfpr] (
    [site]            NVARCHAR (24)    NULL,
    [room]            NVARCHAR (12)    NULL,
    [examination]     NVARCHAR (16)    NULL,
    [projection]      NVARCHAR (8)     NULL,
    [kvp]             NUMERIC (6, 2)  NULL,
    [ma]              NUMERIC (6, 2)  NULL,
    [seconds]         NUMERIC (4, 2)  NULL,
    [mas]             NUMERIC (6, 2)  NULL,
    [dosage]          NUMERIC (10, 2) NULL,
    [focus_film_dist] INT             NULL,
    [stock_code]      NVARCHAR (12)    NULL,
    [pstdfpr_store]   NVARCHAR (24)    NULL,
    [no_used]         INT             NULL,
    [dosagetype]      NVARCHAR (20)    NULL
);

