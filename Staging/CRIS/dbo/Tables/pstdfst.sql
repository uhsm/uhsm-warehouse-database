﻿CREATE TABLE [dbo].[pstdfst] (
    [site]          NVARCHAR (24) NULL,
    [room]          NVARCHAR (12) NULL,
    [examination]   NVARCHAR (16) NULL,
    [projection]    NVARCHAR (8)  NULL,
    [stock_code]    NVARCHAR (12) NULL,
    [no_used]       INT          NULL,
    [pstdfst_store] NVARCHAR (24) NULL
);

