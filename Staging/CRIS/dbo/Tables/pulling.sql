﻿CREATE TABLE [dbo].[pulling] (
    [listcode]        NVARCHAR (16)  NULL,
    [listdate]        datetime2      NULL,
    [compno]          INT           NULL,
    [hospno]          NVARCHAR (20)  NULL,
    [pulling_comment] NVARCHAR (120) NULL
);

