﻿CREATE TABLE [dbo].[radiol] (
    [code]              NVARCHAR (20) NULL,
    [radiol_name]       NVARCHAR (80) NULL,
    [radiol_password]   NVARCHAR (32) NULL,
    [user_id]           NVARCHAR (20) NULL,
    [radiol_type]       NVARCHAR (12) NULL,
    [unverified]        INT          NULL,
    [suspended]         INT          NULL,
    [unprinted]         INT          NULL,
    [end_date]          datetime2     NULL,
    [qa_percent]        INT          NULL,
    [dblreport_percent] INT          NULL
);

