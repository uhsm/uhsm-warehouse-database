﻿CREATE TABLE [dbo].[referer] (
    [start_date]    datetime2  NULL,
    [code]          NVARCHAR (48)  NULL,
    [referer_name]  NVARCHAR (158) NULL,
    [referer_type]  NVARCHAR (2)   NULL,
    [end_date]      datetime2  NULL,
    [speciality]    NVARCHAR (60)  NULL,
    [referer_group] NVARCHAR (30)  NULL,
    [national_code] NVARCHAR (30)  NULL,
    [send_edi]      NVARCHAR (4)   NULL
);
