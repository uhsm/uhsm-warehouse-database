﻿CREATE TABLE [dbo].[reflink] (
    [referrer] NVARCHAR (48) NULL,
    [refsrc]   NVARCHAR (48) NULL,
    [end_date] datetime2 NULL
);
