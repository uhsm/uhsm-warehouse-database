﻿CREATE TABLE [dbo].[refsrc] (
    [refsrc_name]   NVARCHAR (200) NULL,
    [code]          NVARCHAR (48)  NULL,
    [refsrc_group]  NVARCHAR (4)   NULL,
    [end_date]      datetime2   NULL,
    [national_code] NVARCHAR (18)  NULL,
    [address_1]     NVARCHAR (90)  NULL,
    [address_2]     NVARCHAR (140) NULL,
    [address_3]     NVARCHAR (70)  NULL,
    [address_4]     NVARCHAR (70)  NULL,
    [address_5]     NVARCHAR (70)  NULL,
    [postcode_1]    NVARCHAR (8)   NULL,
    [postcode_2]    NVARCHAR (8)   NULL,
    [send_edi]      NVARCHAR (4)   NULL,
    [pct]           NVARCHAR (10)  NULL,
    [refsrc_type]   NVARCHAR (2)   NULL
);
