﻿CREATE TABLE [dbo].[res] (
    [res_code]        NVARCHAR (32)  NULL,
    [res_name]        NVARCHAR (96)  NULL,
    [res_description] NVARCHAR (255) NULL,
    [res_trust]       NVARCHAR (24)  NULL,
    [res_site]        NVARCHAR (24)  NULL,
    [res_groups]      NVARCHAR (MAX) NULL,
    [res_clinician]   NVARCHAR (20)  NULL,
    [res_end_date]    datetime2      NULL,
    [res_always_av]   NVARCHAR (2)   NULL
);

