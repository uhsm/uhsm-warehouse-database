﻿CREATE TABLE [dbo].[res_av] (
    [av_key]               INT           NULL,
    [av_resource]          NVARCHAR (32)  NULL,
    [av_date]              datetime2      NULL,
    [av_duration]          INT           NULL,
    [av_reason]            NVARCHAR (16)  NULL,
    [av_protect]           NVARCHAR (2)   NULL,
    [av_created_by]        NVARCHAR (20)  NULL,
    [av_creation_date]     datetime2      NULL,
    [av_contract_key]      INT           NULL,
    [av_contract_cost_key] INT           NULL,
    [av_trust]             NVARCHAR (24)  NULL,
    [av_site]              NVARCHAR (24)  NULL,
    [av_groups]            NVARCHAR (MAX) NULL,
    [av_description]       NVARCHAR (255) NULL,
    [av_changed_by]        NVARCHAR (20)  NULL,
    [av_changed_date]      datetime2      NULL
);

