﻿CREATE TABLE [dbo].[res_group] (
    [rg_modality]    NVARCHAR (2)     NULL,
    [rg_code]        NVARCHAR (32)    NULL,
    [rg_cost]        NUMERIC (10, 2) NULL,
    [rg_name]        NVARCHAR (96)    NULL,
    [rg_description] NVARCHAR (255)   NULL,
    [rg_trust]       NVARCHAR (24)    NULL,
    [rg_site]        NVARCHAR (24)    NULL,
    [rg_end_date]    datetime2        NULL
);

