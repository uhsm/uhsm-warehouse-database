﻿CREATE TABLE [dbo].[res_use] (
    [use_key]                INT           NULL,
    [use_iteration]          INT           NULL,
    [use_deleted]            NVARCHAR (2)   NULL,
    [use_ignored]            NVARCHAR (2)   NULL,
    [use_exam_key]           INT           NULL,
    [use_duration]           INT           NULL,
    [use_group]              NVARCHAR (32)  NULL,
    [use_resource]           NVARCHAR (32)  NULL,
    [use_date]               datetime2      NULL,
    [use_description]        NVARCHAR (255) NULL,
    [use_allocated_resource] NVARCHAR (32)  NULL
);

