﻿CREATE TABLE [dbo].[roompge] (
    [hospital]        NVARCHAR (24)  NULL,
    [room]            NVARCHAR (12)  NULL,
    [roompge_date]    datetime2      NULL,
    [roompge_comment] NVARCHAR (MAX) NULL,
    [room_status]     NVARCHAR (16)  NULL,
    [free_slots]      INT           NULL,
    [uniqueid]        NVARCHAR (100) NULL
);

