﻿CREATE TABLE [dbo].[rooms] (
    [code]       NVARCHAR (12) NOT NULL,
    [hospital]   NVARCHAR (24) NULL,
    [rooms_name] NVARCHAR (80) NULL,
    [end_date]   datetime2      NULL,
    [dept]       NVARCHAR (16) NULL
);
