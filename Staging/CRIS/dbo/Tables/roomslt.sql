﻿CREATE TABLE [dbo].[roomslt] (
    [hospital]        NVARCHAR (24)  NULL,
    [room]            NVARCHAR (12)  NULL,
    [roomslt_date]    datetime2      NULL,
    [roomslt_time]    NVARCHAR (8)   NULL,
    [examination]     NVARCHAR (16)  NULL,
    [exam_group]      NVARCHAR (4)   NULL,
    [modality]        NVARCHAR (2)   NULL,
    [urgency]         NVARCHAR (2)   NULL,
    [patient_type]    NVARCHAR (8)   NULL,
    [radiologist]     NVARCHAR (20)  NULL,
    [max_no_patients] INT           NULL,
    [patients_booked] INT           NULL,
    [closed]          NVARCHAR (2)   NULL,
    [roomslt_comment] NVARCHAR (255) NULL,
    [uniqueid]        NVARCHAR (100) NULL
);

