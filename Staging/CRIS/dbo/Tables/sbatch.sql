﻿CREATE TABLE [dbo].[sbatch] (
    [batch_key]       INT          NULL,
    [delivery_key]    INT          NULL,
    [sbatch_name]     NVARCHAR (40) NULL,
    [stock_code]      NVARCHAR (12) NULL,
    [manufac_date]    datetime2     NULL,
    [delivery_date]   datetime2     NULL,
    [expiry_date]     datetime2     NULL,
    [no_items]        INT          NULL,
    [allocated_items] INT          NULL
);

