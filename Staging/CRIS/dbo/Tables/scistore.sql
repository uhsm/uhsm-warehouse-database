﻿CREATE TABLE [dbo].[scistore] (
    [sci_id]        NVARCHAR (20) NULL,
    [cris_number]   INT          NULL,
    [last_sci_hash] NVARCHAR (64) NULL,
    [last_merged]   datetime2     NULL,
    [uniqueid]      NVARCHAR (60) NULL
);

