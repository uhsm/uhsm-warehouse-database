﻿CREATE TABLE [dbo].[sessions] (
    [sessions_complete] NVARCHAR (2)   NULL,
    [session_key]       INT           NULL,
    [site]              NVARCHAR (24)  NULL,
    [session_type]      NVARCHAR (6)   NULL,
    [venue]             NVARCHAR (24)  NULL,
    [session_date]      datetime2      NULL,
    [start_time]        NVARCHAR (8)   NULL,
    [end_time]          NVARCHAR (8)   NULL,
    [title]             NVARCHAR (MAX) NULL,
    [sessions_owner]    NVARCHAR (20)  NULL,
    [sessions_subtype]  NVARCHAR (12)  NULL,
    [stat_key]          INT           NULL,
    [session_public]    NVARCHAR (2)   NULL,
    [comments]          NVARCHAR (MAX) NULL
);

