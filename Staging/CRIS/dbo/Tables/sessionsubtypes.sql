﻿CREATE TABLE [dbo].[sessionsubtypes] (
    [code]                 NVARCHAR (12)  NULL,
    [sessionsubtypes_type] NVARCHAR (48)  NULL,
    [site]                 NVARCHAR (24)  NULL,
    [description]          NVARCHAR (MAX) NULL,
    [end_date]             datetime2      NULL
);

