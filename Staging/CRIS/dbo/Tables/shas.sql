﻿CREATE TABLE [dbo].[shas] (
    [code]         NVARCHAR (24)  NULL,
    [shas_name]    NVARCHAR (200) NULL,
    [cluster_code] NVARCHAR (6)   NULL,
    [address_1]    NVARCHAR (70)  NULL,
    [address_2]    NVARCHAR (70)  NULL,
    [address_3]    NVARCHAR (70)  NULL,
    [address_4]    NVARCHAR (70)  NULL,
    [address_5]    NVARCHAR (70)  NULL,
    [postcode_1]   NVARCHAR (8)   NULL,
    [postcode_2]   NVARCHAR (8)   NULL,
    [end_date]     datetime2      NULL
);

