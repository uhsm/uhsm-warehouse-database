﻿CREATE TABLE [dbo].[sitecode] (
    [sitecode_table] NVARCHAR (16)  NULL,
    [hide]           NVARCHAR (2)   NULL,
    [site]           NVARCHAR (24)  NULL,
    [code]           NVARCHAR (48)  NULL,
    [uniqueid]       NVARCHAR (100) NULL
);

