﻿CREATE TABLE [dbo].[sites] (
    [invoice_suffix] NVARCHAR (10) NULL,
    [trust]          NVARCHAR (24) NULL,
    [code]           NVARCHAR (24) NULL,
    [sites_name]     NVARCHAR (80) NULL,
    [image_number]   INT           NULL,
    [image_date]     datetime2      NULL,
    [restart_year]   NVARCHAR (2)  NULL,
    [hosp_numbers]   NVARCHAR (24) NULL,
    [image_numbers]  NVARCHAR (24) NULL,
    [film_store]     NVARCHAR (24) NULL,
    [end_date]       datetime2      NULL,
    [location_i_hrs] NVARCHAR (30) NULL,
    [location_o_hrs] NVARCHAR (30) NULL,
    [dflt_film_loc]  NVARCHAR (32) NULL,
    [filing_system]  NVARCHAR (60) NULL,
    [filing_site]    NVARCHAR (24) NULL,
    [refsrc]         NVARCHAR (48) NULL,
    [workgroup]      NVARCHAR (64) NULL,
    [invoice_number] INT          NULL,
    [invoice_site]   NVARCHAR (24) NULL
);

