﻿CREATE TABLE [dbo].[smonitor_tbl] (
    [dttm]           datetime2      NULL,
    [trust]          NVARCHAR (MAX) NULL,
    [site]           NVARCHAR (MAX) NULL,
    [terminal]       NVARCHAR (MAX) NULL,
    [username]       NVARCHAR (MAX) NULL,
    [client_version] NVARCHAR (MAX) NULL,
    [uniqueid]       NVARCHAR (255) NULL
);

