﻿CREATE TABLE [dbo].[special] (
    [code]          NVARCHAR (16) NULL,
    [special_name]  NVARCHAR (90) NULL,
    [special_group] NVARCHAR (6)  NULL,
    [end_date]      datetime2      NULL
);
