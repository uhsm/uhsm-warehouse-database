﻿CREATE TABLE [dbo].[spinecodes] (
    [code]              NVARCHAR (16)  NULL,
    [spinecodes_system] NVARCHAR (24)  NULL,
    [spinecodes_type]   NVARCHAR (12)  NULL,
    [description]       NVARCHAR (MAX) NULL,
    [uniqueid]          NVARCHAR (50)  NULL
);

