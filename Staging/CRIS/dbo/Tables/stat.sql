﻿CREATE TABLE [dbo].[stat] (
    [priority]            INT           NULL,
    [stat_name]           NVARCHAR (255) NULL,
    [stat_owner]          NVARCHAR (20)  NULL,
    [date_to_run]         datetime2      NULL,
    [time_to_run]         NVARCHAR (8)   NULL,
    [repeat_type]         NVARCHAR (2)   NULL,
    [status]              NVARCHAR (2)   NULL,
    [schedule_start]      datetime2      NULL,
    [schedule_end]        datetime2      NULL,
    [printer]             NVARCHAR (60)  NULL,
    [last_run_by]         NVARCHAR (20)  NULL,
    [terminal]            NVARCHAR (128) NULL,
    [range_type]          NVARCHAR (12)  NULL,
    [date_last_run]       datetime2      NULL,
    [time_last_run]       NVARCHAR (8)   NULL,
    [status_changed_by]   NVARCHAR (20)  NULL,
    [fail_trace]          NVARCHAR (MAX) NULL,
    [stat_query]          NVARCHAR (MAX) NULL,
    [stat_keep]           NVARCHAR (2)   NULL,
    [properties]          NVARCHAR (MAX) NULL,
    [stat_key]            BIGINT        NULL,
    [no_of_copies]        BIGINT        NULL,
    [stats_start_version] NVARCHAR (24)  NULL,
    [stats_end_version]   NVARCHAR (24)  NULL
);

