﻿CREATE TABLE [dbo].[statlinks] (
    [table1]              NVARCHAR (48)  NULL,
    [table2]              NVARCHAR (48)  NULL,
    [statlinks_link]      NVARCHAR (MAX) NULL,
    [link_type]           NVARCHAR (2)   NULL,
    [default_join]        NVARCHAR (4)   NULL,
    [deleted]             NVARCHAR (2)   NULL,
    [stats_start_version] NVARCHAR (24)  NULL,
    [stats_end_version]   NVARCHAR (24)  NULL,
    [uniqueid]            NVARCHAR (100) NULL
);

