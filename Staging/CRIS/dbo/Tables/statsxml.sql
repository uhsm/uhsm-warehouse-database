﻿CREATE TABLE [dbo].[statsxml] (
    [statsxml_type]  NVARCHAR (2)   NULL,
    [statsxml_owner] NVARCHAR (20)  NULL,
    [filename]       NVARCHAR (40)  NULL,
    [statsxml_xml]   NVARCHAR (MAX) NULL,
    [statsxml_name]  NVARCHAR (100) NULL,
    [xml_key]        BIGINT        NULL,
    [stat_key]       BIGINT        NULL
);

