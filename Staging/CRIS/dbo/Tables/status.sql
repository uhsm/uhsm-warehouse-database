﻿CREATE TABLE [dbo].[status] (
    [status_key]      INT           NULL,
    [computer_number] INT           NULL,
    [event_key]       INT           NULL,
    [exam_key]        INT           NULL,
    [status_category] NVARCHAR (4)   NULL,
    [status_current]  NVARCHAR (2)   NULL,
    [deleted]         NVARCHAR (2)   NULL,
    [status_code]     NVARCHAR (12)  NULL,
    [status_type]     NVARCHAR (12)  NULL,
    [status_date]     datetime2      NULL,
    [status_time]     NVARCHAR (8)   NULL,
    [user_id]         NVARCHAR (20)  NULL,
    [start_date]      datetime2      NULL,
    [start_time]      NVARCHAR (8)   NULL,
    [end_date]        datetime2      NULL,
    [end_time]        NVARCHAR (8)   NULL,
    [examination]     NVARCHAR (16)  NULL,
    [site]            NVARCHAR (24)  NULL,
    [other_code]      NVARCHAR (24)  NULL,
    [description]     NVARCHAR (MAX) NULL,
    [freetext]        NVARCHAR (2)   NULL
);

