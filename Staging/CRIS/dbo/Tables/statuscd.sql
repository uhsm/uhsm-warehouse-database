﻿CREATE TABLE [dbo].[statuscd] (
    [CODE]              NVARCHAR (12) NULL,
    [STATUSCD_CATEGORY] NVARCHAR (4)  NULL,
    [STATUSCD_TYPE]     NVARCHAR (6)  NULL,
    [STATUSCD_DEFAULT]  NVARCHAR (1)  NULL,
    [SHORT_DESC]        NVARCHAR (60) NULL,
    [LONG_DESC]         NVARCHAR (60) NULL,
    [STATUSCD_ORDER]    NUMERIC (2)   NULL,
    [END_DATE]          datetime2      NULL
);
