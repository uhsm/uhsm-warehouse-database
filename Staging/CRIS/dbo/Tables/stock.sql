﻿CREATE TABLE [dbo].[stock] (
    [code]           NVARCHAR (12)   NULL,
    [stock_store]    NVARCHAR (24)   NULL,
    [stock_type]     NVARCHAR (8)    NULL,
    [stock_name]     NVARCHAR (80)   NULL,
    [man_code]       NVARCHAR (40)   NULL,
    [stock_level]    INT            NULL,
    [minimum_level]  INT            NULL,
    [re_order_level] INT            NULL,
    [unit_cost]      NUMERIC (8, 2) NULL
);

