﻿CREATE TABLE [dbo].[stock_pack_contents] (
    [stock_pack]       NVARCHAR (16)  NULL,
    [stockcd_category] NVARCHAR (20)  NULL,
    [stockcd_code]     NVARCHAR (20)  NULL,
    [trust]            NVARCHAR (24)  NULL,
    [quantity]         INT           NULL,
    [end_date]         datetime2      NULL,
    [uniqueid]         NVARCHAR (100) NULL
);

