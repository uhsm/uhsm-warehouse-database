﻿CREATE TABLE [dbo].[stock_packs] (
    [code]            NVARCHAR (16)    NULL,
    [trust]           NVARCHAR (24)    NULL,
    [description]     NVARCHAR (80)    NULL,
    [stock_pack_cost] NUMERIC (10, 2) NULL,
    [end_date]        datetime2        NULL,
    [uniqueid]        NVARCHAR (40)    NULL
);

