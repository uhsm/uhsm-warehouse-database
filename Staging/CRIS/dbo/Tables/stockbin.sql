﻿CREATE TABLE [dbo].[stockbin] (
    [bin_key]        INT          NULL,
    [stockbin_name]  NVARCHAR (40) NULL,
    [stockbin_store] NVARCHAR (24) NULL,
    [stock_code]     NVARCHAR (12) NULL,
    [qty_in_stock]   INT          NULL,
    [reorder_level]  INT          NULL,
    [qty_on_order]   INT          NULL,
    [end_date]       datetime2     NULL
);

