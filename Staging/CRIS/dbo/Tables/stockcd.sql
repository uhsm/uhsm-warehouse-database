﻿CREATE TABLE [dbo].[stockcd] (
    [trust]            NVARCHAR (24)    NULL,
    [stockcd_category] NVARCHAR (20)    NULL,
    [code]             NVARCHAR (20)    NULL,
    [rate_required]    NVARCHAR (2)     NULL,
    [stockcd_cost]     NUMERIC (10, 2) NULL,
    [units]            NVARCHAR (20)    NULL,
    [accuracy]         INT             NULL,
    [description]      NVARCHAR (MAX)   NULL,
    [end_date]         datetime2        NULL,
    [int_quantity]     NVARCHAR (2)     NULL,
    [uniqueid]         NVARCHAR (70)    NULL
);

