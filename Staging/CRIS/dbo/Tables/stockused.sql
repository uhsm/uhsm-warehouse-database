﻿CREATE TABLE [dbo].[stockused] (
    [deleted]               NVARCHAR (2)    NULL,
    [stock_key]             INT            NULL,
    [event_key]             INT            NULL,
    [trust]                 NVARCHAR (24)   NULL,
    [stockused_category]    NVARCHAR (20)   NULL,
    [code]                  NVARCHAR (20)   NULL,
    [stock_pack]            NVARCHAR (16)   NULL,
    [used]                  NVARCHAR (2)    NULL,
    [pack_count]            INT            NULL,
    [batch]                 NVARCHAR (MAX)  NULL,
    [serial]                NVARCHAR (MAX)  NULL,
    [quantity]              NUMERIC (8, 4) NULL,
    [units]                 NVARCHAR (20)   NULL,
    [administration_method] NVARCHAR (48)   NULL,
    [administered_by]       NVARCHAR (20)   NULL,
    [checked_by]            NVARCHAR (20)   NULL,
    [rate]                  NUMERIC (5, 2) NULL,
    [expiry_date]           datetime2       NULL
);

