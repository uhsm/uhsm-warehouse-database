﻿CREATE TABLE [dbo].[stype] (
    [code]       NVARCHAR (16) NULL,
    [stype_name] NVARCHAR (60) NULL,
    [batched]    NVARCHAR (2)  NULL,
    [traced]     NVARCHAR (2)  NULL
);

