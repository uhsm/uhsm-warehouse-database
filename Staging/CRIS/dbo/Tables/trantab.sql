﻿CREATE TABLE [dbo].[trantab] (
    [trantab_system]   NVARCHAR (32) NULL,
    [cris_code]        NVARCHAR (40) NULL,
    [foreign_code]     NVARCHAR (60) NULL,
    [additional_info]  NVARCHAR (20) NULL,
    [trantab_name]     NVARCHAR (80) NULL,
    [trantab_table]    NVARCHAR (60) NULL,
    [trantab_outbound] NVARCHAR (2)  NULL
);

