﻿CREATE TABLE [dbo].[trusts] (
    [code]        NVARCHAR (24)  NULL,
    [trusts_name] NVARCHAR (255) NULL,
    [org_type]    NVARCHAR (2)   NULL,
    [end_date]    datetime2      NULL
);

