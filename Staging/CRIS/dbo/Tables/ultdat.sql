﻿CREATE TABLE [dbo].[ultdat] (
    [ult_key]         INT            NULL,
    [report_key]      INT            NULL,
    [deleted]         NVARCHAR (2)    NULL,
    [ult_type]        NVARCHAR (16)   NULL,
    [ultdat_position] NVARCHAR (24)   NULL,
    [ultdat_category] NVARCHAR (16)   NULL,
    [ultdat_value]    NVARCHAR (16)   NULL,
    [val1]            NUMERIC (8, 2) NULL,
    [val2]            NUMERIC (8, 2) NULL,
    [val3]            NUMERIC (8, 2) NULL,
    [ultdat_location] NVARCHAR (48)   NULL
);

