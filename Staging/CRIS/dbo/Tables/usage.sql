﻿CREATE TABLE [dbo].[usage] (
    [usage_key]     INT             NULL,
    [exam_key]      INT             NULL,
    [stock_code]    NVARCHAR (12)    NULL,
    [bin_key]       INT             NULL,
    [batch_key]     INT             NULL,
    [serial_number] NVARCHAR (40)    NULL,
    [date_used]     datetime2        NULL,
    [time_used]     NVARCHAR (8)     NULL,
    [used_by]       NVARCHAR (20)    NULL,
    [quantity_used] NUMERIC (16, 8) NULL,
    [wasted]        NVARCHAR (8)     NULL,
    [concentration] NUMERIC (16, 8) NULL,
    [amount_given]  NUMERIC (16, 8) NULL
);

