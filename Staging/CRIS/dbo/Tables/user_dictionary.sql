﻿CREATE TABLE [dbo].[user_dictionary] (
    [word]                NVARCHAR (255) NULL,
    [user_dictionary_key] INT           NULL,
    [user_id]             NVARCHAR (20)  NULL,
    [date_changed]        datetime2      NULL
);

