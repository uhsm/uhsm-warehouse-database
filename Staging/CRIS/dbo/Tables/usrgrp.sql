﻿CREATE TABLE [dbo].[usrgrp] (
    [usrgrp_group]           NVARCHAR (48)  NULL,
    [end_date]               datetime2      NULL,
    [usrgrp_name]            NVARCHAR (80)  NULL,
    [parent_group]           NVARCHAR (48)  NULL,
    [trust]                  NVARCHAR (24)  NULL,
    [group_type]             NVARCHAR (2)   NULL,
    [activities]             NVARCHAR (MAX) NULL,
    [recommended_activities] NVARCHAR (MAX) NULL
);

