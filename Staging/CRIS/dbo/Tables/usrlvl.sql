﻿CREATE TABLE [dbo].[usrlvl] (
    [usrlvl_group]  NVARCHAR (30)  NULL,
    [section]       NVARCHAR (30)  NULL,
    [item]          NVARCHAR (30)  NULL,
    [usrlvl_access] NVARCHAR (2)   NULL,
    [uniqueid]      NVARCHAR (100) NULL
);

