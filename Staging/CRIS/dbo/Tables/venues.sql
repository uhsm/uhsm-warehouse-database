﻿CREATE TABLE [dbo].[venues] (
    [code]        NVARCHAR (24)  NULL,
    [site]        NVARCHAR (24)  NULL,
    [venues_name] NVARCHAR (255) NULL,
    [end_date]    datetime2      NULL
);

