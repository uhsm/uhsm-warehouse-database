﻿CREATE TABLE [dbo].[vetting] (
    [vetting_key]        INT           NULL,
    [event_key]          INT           NULL,
    [preparation]        NVARCHAR (MAX) NULL,
    [contrast]           NVARCHAR (255) NULL,
    [resources]          NVARCHAR (MAX) NULL,
    [booking_notes]      NVARCHAR (MAX) NULL,
    [patient_condition]  NVARCHAR (MAX) NULL,
    [current_medication] NVARCHAR (MAX) NULL,
    [room]               NVARCHAR (128) NULL,
    [date_required]      datetime2      NULL,
    [selected_protocols] NVARCHAR (MAX) NULL
);

