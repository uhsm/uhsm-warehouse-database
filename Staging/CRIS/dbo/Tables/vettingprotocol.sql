﻿CREATE TABLE [dbo].[vettingprotocol] (
    [trust]                    NVARCHAR (24)  NULL,
    [protocol_key]             INT           NULL,
    [vettingprotocol_name]     NVARCHAR (255) NULL,
    [parent_key]               INT           NULL,
    [vettingprotocol_document] NVARCHAR (MAX) NULL,
    [preparation]              NVARCHAR (MAX) NULL,
    [contrast]                 NVARCHAR (255) NULL,
    [resources]                NVARCHAR (MAX) NULL,
    [booking_notes]            NVARCHAR (MAX) NULL,
    [patient_condition]        NVARCHAR (MAX) NULL,
    [current_medication]       NVARCHAR (MAX) NULL,
    [room]                     NVARCHAR (128) NULL,
    [intended_clinician]       NVARCHAR (20)  NULL,
    [required_days]            INT           NULL
);

