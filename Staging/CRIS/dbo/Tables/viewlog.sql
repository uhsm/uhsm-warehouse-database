﻿CREATE TABLE [dbo].[viewlog] (
    [userid]      NVARCHAR (20)  NULL,
    [crisnumber]  INT           NULL,
    [pasid]       INT           NULL,
    [timeviewed]  datetime2      NULL,
    [lrreason]    NVARCHAR (6)   NULL,
    [site]        NVARCHAR (24)  NULL,
    [terminal_id] NVARCHAR (128) NULL,
    [uniqueid]    NVARCHAR (100) NULL
);

