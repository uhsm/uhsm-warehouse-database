﻿CREATE TABLE [dbo].[volcont] (
    [volume_key]   INT          NULL,
    [event_key]    INT          NULL,
    [from_volume]  INT          NULL,
    [volcont_date] datetime2     NULL,
    [volcont_time] NVARCHAR (8)  NULL,
    [user_id]      NVARCHAR (20) NULL
);

