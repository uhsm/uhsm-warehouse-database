﻿CREATE TABLE [dbo].[volmove] (
    [volume_key]       INT           NULL,
    [volmove_location] NVARCHAR (30)  NULL,
    [description]      NVARCHAR (60)  NULL,
    [text_key]         INT           NULL,
    [volmove_date]     datetime2      NULL,
    [volmove_time]     NVARCHAR (12)  NULL,
    [user_id]          NVARCHAR (20)  NULL,
    [volmove_text]     NVARCHAR (MAX) NULL
);

