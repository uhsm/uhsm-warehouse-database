﻿CREATE TABLE [dbo].[volres] (
    [reservation_key] INT           NULL,
    [computer_number] INT           NULL,
    [volume_key]      INT           NULL,
    [date_requested]  datetime2      NULL,
    [time_requested]  NVARCHAR (8)   NULL,
    [entered_by]      NVARCHAR (20)  NULL,
    [volres_location] NVARCHAR (30)  NULL,
    [description]     NVARCHAR (60)  NULL,
    [text_key]        INT           NULL,
    [request_for]     datetime2      NULL,
    [cancelled]       NVARCHAR (2)   NULL,
    [filled]          NVARCHAR (2)   NULL,
    [volres_text]     NVARCHAR (MAX) NULL
);

