﻿CREATE TABLE [dbo].[volumes] (
    [volume_key]       INT          NULL,
    [computer_number]  INT          NULL,
    [first_comp_no]    INT          NULL,
    [volume_type]      NVARCHAR (16) NULL,
    [main_packet]      NVARCHAR (2)  NULL,
    [site]             NVARCHAR (24) NULL,
    [volumes_location] NVARCHAR (32) NULL,
    [description]      NVARCHAR (60) NULL,
    [text_key]         INT          NULL,
    [active]           NVARCHAR (2)  NULL,
    [move_date]        datetime2     NULL,
    [move_time]        NVARCHAR (12) NULL,
    [move_user]        NVARCHAR (20) NULL,
    [creation_date]    datetime2     NULL,
    [creation_time]    NVARCHAR (8)  NULL,
    [creation_user]    NVARCHAR (20) NULL,
    [merge_date]       datetime2     NULL,
    [merge_time]       NVARCHAR (8)  NULL,
    [merge_user]       NVARCHAR (20) NULL,
    [merged_to]        INT          NULL
);

