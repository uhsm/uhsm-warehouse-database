﻿CREATE TABLE [dbo].[wards] (
    [wards_name] NVARCHAR (80) NULL,
    [code]       NVARCHAR (30) NULL,
    [hospital]   NVARCHAR (24) NULL,
    [end_date]   datetime2      NULL
);
