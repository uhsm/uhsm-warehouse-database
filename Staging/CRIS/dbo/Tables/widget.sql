﻿CREATE TABLE [dbo].[widget] (
    [widget_name] NVARCHAR (128) NULL,
    [widget_type] NVARCHAR (48)  NULL,
    [menu_name]   NVARCHAR (128) NULL,
    [script]      NVARCHAR (MAX) NULL
);

