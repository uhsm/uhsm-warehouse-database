﻿CREATE TABLE [dbo].[workgroup] (
    [code]     NVARCHAR (24) NULL,
    [trust]    NVARCHAR (24) NULL,
    [site]     NVARCHAR (24) NULL,
    [end_date] datetime2     NULL
);

