﻿CREATE TABLE [dbo].[worklist] (
    [computer_number] INT           NULL,
    [event_key]       INT           NULL,
    [worklist_key]    INT           NULL,
    [completed]       NVARCHAR (2)   NULL,
    [worklist_locked] NVARCHAR (2)   NULL,
    [priority]        NVARCHAR (2)   NULL,
    [date_added]      datetime2      NULL,
    [time_added]      NVARCHAR (8)   NULL,
    [exam_key]        INT           NULL,
    [report_key]      INT           NULL,
    [expiry_date]     datetime2      NULL,
    [expiry_time]     NVARCHAR (8)   NULL,
    [properties]      NVARCHAR (MAX) NULL,
    [session_key]     INT           NULL,
    [session_order]   INT           NULL,
    [worklist_source] NVARCHAR (2)   NULL,
    [list_type]       NVARCHAR (16)  NULL,
    [worklist_owner]  NVARCHAR (20)  NULL,
    [work_group]      NVARCHAR (16)  NULL
);

