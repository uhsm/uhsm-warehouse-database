﻿CREATE TABLE [dbo].[xdsassociation] (
    [association_id]        NVARCHAR (255) NULL,
    [xdsassociation_type]   NVARCHAR (255) NULL,
    [xdsassociation_source] NVARCHAR (255) NULL,
    [target]                NVARCHAR (255) NULL,
    [computer_number]       INT           NULL
);

