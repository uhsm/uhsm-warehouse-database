﻿CREATE TABLE [dbo].[xdsdocument] (
    [document_id]        NVARCHAR (255) NULL,
    [computer_number]    INT           NULL,
    [date_cached]        datetime2      NULL,
    [document_uid]       NVARCHAR (255) NULL,
    [folder_id]          NVARCHAR (255) NULL,
    [document_path]      NVARCHAR (255) NULL,
    [xdsdocument_object] NVARCHAR (MAX) NULL
);

