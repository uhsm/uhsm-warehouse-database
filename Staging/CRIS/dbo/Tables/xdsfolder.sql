﻿CREATE TABLE [dbo].[xdsfolder] (
    [folder_id]        NVARCHAR (255) NULL,
    [computer_number]  INT           NULL,
    [date_cached]      datetime2      NULL,
    [folder_uid]       NVARCHAR (255) NULL,
    [event_key]        INT           NULL,
    [xdsfolder_object] NVARCHAR (MAX) NULL
);

