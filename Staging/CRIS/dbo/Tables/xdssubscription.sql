﻿CREATE TABLE [dbo].[xdssubscription] (
    [computer_number]   INT           NULL,
    [subscription_url]  NVARCHAR (255) NULL,
    [subscription_date] datetime2      NULL,
    [expiry_date]       datetime2      NULL
);

