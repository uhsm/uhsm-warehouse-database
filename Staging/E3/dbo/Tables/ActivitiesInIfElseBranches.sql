﻿CREATE TABLE [dbo].[ActivitiesInIfElseBranches](
	[QuestionnaireID] [int] NULL,
	[IfElseName] [nvarchar](255) NULL,
	[IfElseBranchName] [nvarchar](255) NULL,
	[ParentIfElseBranches] [nvarchar](max) NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[ActivitiesInIfElseBranchesID] [int] NOT NULL
)
