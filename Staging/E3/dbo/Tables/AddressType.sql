﻿CREATE TABLE [dbo].[AddressType](
	[AddressTypeID] [int] NOT NULL,
	[AddressType] [nvarchar](20) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
