﻿CREATE TABLE [dbo].[Alert](
	[AlertID] [int] NOT NULL,
	[Alert] [nvarchar](300) NULL,
	[PregnancyID] [bigint] NULL,
	[BabyID] [bigint] NULL,
	[CarePlanID] [int] NULL,
	[AlertDefinitionID] [int] NULL,
	[PODID] [int] NULL,
	[Deleted] [bit] NOT NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[LastUpdated] [datetime] NULL,
	[UserID] [int] NULL,
	[ExtraDetails] [nvarchar](500) NULL,
	[IsCopiedAcrossFromMother] [bit] NULL,
	[ToBeCopiedToAllBabies] [bit] NULL
)
