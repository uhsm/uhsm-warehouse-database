﻿CREATE TABLE [dbo].[AlertDefinition](
	[AlertDefinitionID] [int] NOT NULL,
	[AlertDefinition] [nvarchar](300) NULL,
	[Deleted] [bit] NOT NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[LastUpdated] [datetime] NULL,
	[UserID] [int] NULL,
	[IsAlertForPregnancy] [bit] NULL,
	[IsAlertForBaby] [bit] NULL,
	[AlertLabelToUseWhenCopyingToBaby] [nvarchar](255) NULL
)
