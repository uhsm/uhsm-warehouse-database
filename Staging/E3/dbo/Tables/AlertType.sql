﻿CREATE TABLE [dbo].[AlertType](
	[AlertTypeID] [int] NOT NULL,
	[AlertType] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
