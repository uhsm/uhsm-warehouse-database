﻿CREATE TABLE [dbo].[AnswerDependencyRule](
	[AnswerDependencyName] [varchar](50) NOT NULL,
	[AnswerDependencyId] [int] NOT NULL,
	[AnswerDependnecySetId] [int] NOT NULL,
	[ValidationTypeId] [int] NOT NULL,
	[ValidationIndex] [int] NOT NULL,
	[CareplanId] [int] NOT NULL,
	[TableId] [int] NULL,
	[FieldName] [varchar](255) NULL,
	[SQLTableName] [varchar](255) NULL,
	[SQLFieldName] [varchar](255) NULL,
	[ValidationLogicId] [int] NOT NULL,
	[EqualToAValueOf] [varchar](255) NULL
)
