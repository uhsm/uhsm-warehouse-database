﻿CREATE TABLE [dbo].[AnswerExcludeAnswer](
	[AnswerExcludeAnswerId] [int] NOT NULL,
	[AnswerID] [int] NULL,
	[ExcludeAnswer] [nvarchar](255) NULL,
	[ExcludeAnswerID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UserID] [int] NULL,
	[GUID] [uniqueidentifier] NULL,
	[EK_Record] [bit] NULL
)
