﻿CREATE TABLE [dbo].[AnswerType](
	[AnswerTypeID] [tinyint] NOT NULL,
	[AnswerType] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
