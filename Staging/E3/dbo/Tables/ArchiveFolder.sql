﻿CREATE TABLE [dbo].[ArchiveFolder](
	[ArchiveFolderId] [int] NOT NULL,
	[ArchiveFolder] [nvarchar](max) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
