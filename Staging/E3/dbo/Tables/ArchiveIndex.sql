﻿CREATE TABLE [dbo].[ArchiveIndex](
	[ArchiveIndexID] [int] NOT NULL,
	[ArchiveRecordNo] [int] NOT NULL,
	[LegacyHNO] [nvarchar](20) NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
