﻿CREATE TABLE [dbo].[Area](
	[AreaID] [tinyint] NOT NULL,
	[SystemListID] [int] NULL,
	[Area] [varchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[UpdatedBy] [int] NULL,
	[LastUpdated] [datetime] NULL
)
