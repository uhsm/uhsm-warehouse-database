﻿CREATE TABLE [dbo].[Audit](
	[AuditID] [bigint] NOT NULL,
	[TableName] [nvarchar](50) NOT NULL,
	[RowId] [bigint] NOT NULL,
	[Operation] [nvarchar](10) NOT NULL,
	[OccurredAt] [datetime] NOT NULL,
	[PerformedBy] [varchar](50) NOT NULL,
	[FieldName] [nvarchar](50) NULL,
	[OldValue] [nvarchar](1000) NULL,
	[NewValue] [nvarchar](1000) NULL,
	[AuditAction] [nvarchar](20) NULL,
	[MigratedData] [bit] NULL
)
