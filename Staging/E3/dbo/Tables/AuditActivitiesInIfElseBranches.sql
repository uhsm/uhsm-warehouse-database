﻿CREATE TABLE [dbo].[AuditActivitiesInIfElseBranches](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[QuestionnaireID] [int] NULL,
	[IfElseName] [nvarchar](255) NULL,
	[IfElseBranchName] [nvarchar](255) NULL,
	[ParentIfElseBranches] [nvarchar](max) NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[ActivitiesInIfElseBranchesID] [int] NULL,
	[MigratedData] [bit] NULL
)
