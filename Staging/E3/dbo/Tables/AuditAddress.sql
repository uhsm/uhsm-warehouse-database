﻿CREATE TABLE [dbo].[AuditAddress](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[AddressID] [bigint] NULL,
	[Line1] [nvarchar](50) NULL,
	[Line2] [nvarchar](50) NULL,
	[Line3] [nvarchar](50) NULL,
	[Line4] [nvarchar](50) NULL,
	[PostCode] [nchar](10) NULL,
	[HomeTelephone] [nvarchar](50) NULL,
	[MobileTelephone] [nvarchar](50) NULL,
	[WorkTelephone] [nvarchar](50) NULL,
	[EMail] [nvarchar](100) NULL,
	[Fax] [nvarchar](50) NULL,
	[AreaOfResidence] [varchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[MigratedData] [bit] NULL,
	[OtherTelephone] [nvarchar](50) NULL
)
GO
