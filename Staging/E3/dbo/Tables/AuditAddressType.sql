﻿CREATE TABLE [dbo].[AuditAddressType](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[AddressTypeID] [int] NULL,
	[AddressType] [nvarchar](20) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
