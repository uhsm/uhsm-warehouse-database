﻿CREATE TABLE [dbo].[AuditAlertDefinition](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[AlertDefinitionID] [int] NULL,
	[AlertDefinition] [nvarchar](300) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[LastUpdated] [datetime] NULL,
	[UserID] [int] NULL,
	[MigratedData] [bit] NULL,
	[AlertLabelToUseWhenCopyingToBaby] [nvarchar](1000) NULL,
	[IsAlertForBaby] [bit] NULL,
	[IsAlertForPregnancy] [bit] NULL
)
