﻿CREATE TABLE [dbo].[AuditAnswerExcludeAnswer](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[AnswerExcludeAnswerId] [int] NULL,
	[AnswerID] [int] NULL,
	[ExcludeAnswer] [nvarchar](255) NULL,
	[ExcludeAnswerID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UserID] [int] NULL,
	[GUID] [uniqueidentifier] NULL,
	[EK_Record] [bit] NULL,
	[MigratedData] [bit] NULL
)
