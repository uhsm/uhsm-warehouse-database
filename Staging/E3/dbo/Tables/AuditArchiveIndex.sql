﻿CREATE TABLE [dbo].[AuditArchiveIndex](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[ArchiveIndexID] [int] NULL,
	[ArchiveRecordNo] [int] NULL,
	[LegacyHNO] [nvarchar](20) NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[MigratedData] [bit] NULL
)
