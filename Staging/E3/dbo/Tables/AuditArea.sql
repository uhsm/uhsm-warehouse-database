﻿CREATE TABLE [dbo].[AuditArea](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[AreaID] [tinyint] NULL,
	[SystemListID] [int] NULL,
	[Area] [varchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[UpdatedBy] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[MigratedData] [bit] NULL
)
