﻿CREATE TABLE [dbo].[AuditBaby](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[BabyID] [bigint] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[CarePlan] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[RequestedNN4B] [bit] NULL,
	[RegisteredInPAS] [bit] NULL,
	[BabyNotesMemoID] [bigint] NULL,
	[Birthday] [datetime] NULL,
	[GestationBirth] [nvarchar](30) NULL,
	[MergedData] [bit] NULL,
	[MigratedData] [bit] NULL
)
GO
