﻿CREATE TABLE [dbo].[AuditBabyAdmission](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[BabyAdmissionID] [bigint] NULL,
	[BabyID] [bigint] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MergedData] [bit] NULL,
	[MigratedData] [bit] NULL
)
