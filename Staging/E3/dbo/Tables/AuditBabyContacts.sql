﻿CREATE TABLE [dbo].[AuditBabyContacts](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[BabyID] [bigint] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[BabyContactsID] [bigint] NULL,
	[MergedData] [bit] NULL,
	[MigratedData] [bit] NULL
)
