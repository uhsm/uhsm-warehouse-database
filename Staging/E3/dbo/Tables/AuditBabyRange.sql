﻿CREATE TABLE [dbo].[AuditBabyRange](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[BabyRangeID] [bigint] NULL,
	[Finish] [int] NULL,
	[InUse] [bit] NULL,
	[Prefix] [nvarchar](50) NULL,
	[RangeType] [nvarchar](50) NULL,
	[Start] [int] NULL,
	[Suffix] [nvarchar](50) NULL,
	[Warning] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[SystemNumberPrefix] [nvarchar](10) NULL,
	[HospitalNumberPrefix] [nvarchar](10) NULL,
	[TotalWidth] [int] NULL,
	[MigratedData] [bit] NULL,
	[Disabled] [bit] NULL
)
