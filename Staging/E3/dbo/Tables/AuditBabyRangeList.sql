﻿CREATE TABLE [dbo].[AuditBabyRangeList](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[BabyRangeListID] [bigint] NULL,
	[BabyRangeList] [nvarchar](50) NULL,
	[BabyRangeID] [bigint] NULL,
	[PregnancyId] [bigint] NULL,
	[ReservedByPregnancyId] [bigint] NULL,
	[Used] [bit] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
