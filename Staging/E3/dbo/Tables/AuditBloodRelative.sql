﻿CREATE TABLE [dbo].[AuditBloodRelative](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[BloodRelativeID] [int] NULL,
	[BloodRelative] [nvarchar](20) NULL,
	[EK_Record] [bit] NULL,
	[Deleted] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[LastUpdated] [datetime] NULL,
	[UserID] [int] NULL,
	[MigratedData] [bit] NULL,
	[UpdatedBy] [int] NULL
)
