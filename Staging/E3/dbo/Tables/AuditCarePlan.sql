﻿CREATE TABLE [dbo].[AuditCarePlan](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[CarePlan] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[CarePlanID] [int] NULL,
	[MigratedData] [bit] NULL
)
