﻿CREATE TABLE [dbo].[AuditChildrenCentre](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[ChildrenCentreID] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[Code] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UserID] [int] NULL,
	[MigratedData] [bit] NULL
)
