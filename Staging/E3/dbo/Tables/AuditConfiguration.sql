﻿CREATE TABLE [dbo].[AuditConfiguration](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[ConfigurationId] [int] NULL,
	[Configuration] [nvarchar](255) NULL,
	[FieldType] [nvarchar](25) NULL,
	[Options] [nvarchar](1000) NULL,
	[ConfigurationValue] [nvarchar](500) NULL,
	[GroupName] [nvarchar](255) NULL,
	[Configuration_Description] [nvarchar](500) NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserId] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[deleted] [bit] NULL
)
