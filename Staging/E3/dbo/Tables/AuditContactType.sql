﻿CREATE TABLE [dbo].[AuditContactType](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[ContactTypeID] [smallint] NULL,
	[ContactType] [varchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
