﻿CREATE TABLE [dbo].[AuditCountry](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[CountryID] [int] NULL,
	[Country] [nvarchar](100) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
