﻿CREATE TABLE [dbo].[AuditDeathLocation](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[DeathLocationId] [bigint] NULL,
	[DeathLocation] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[sequence] [int] NULL,
	[MigratedData] [bit] NULL
)
