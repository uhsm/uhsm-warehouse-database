﻿CREATE TABLE [dbo].[AuditDefinition](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[DefinitionID] [bigint] NULL,
	[AllowAddPatient] [bit] NULL,
	[ApachiServerName] [nvarchar](50) NULL,
	[ArchiveApplicationName] [nvarchar](50) NULL,
	[ArchiveEngineTimeout] [int] NULL,
	[ArchiveParameters] [nvarchar](50) NULL,
	[ArchivePath] [nvarchar](50) NULL,
	[ArchiveProgramName] [nvarchar](50) NULL,
	[Auditing] [bit] NULL,
	[CacheRootDir] [nvarchar](200) NULL,
	[CalendarType] [nvarchar](50) NULL,
	[ChildHealthCode] [nvarchar](50) NULL,
	[ClinicalDev] [bit] NULL,
	[CopyMotherGPToBaby] [bit] NULL,
	[Core] [bit] NULL,
	[CreateReports] [bit] NULL,
	[CustomAbout] [bit] NULL,
	[CustomEuroKingLogo] [nvarchar](50) NULL,
	[CustomInfo1] [nvarchar](50) NULL,
	[CustomInfo2] [nvarchar](50) NULL,
	[CustomInfo3] [nvarchar](50) NULL,
	[CustomInfo4] [nvarchar](50) NULL,
	[CustomTitle1] [nvarchar](50) NULL,
	[CustomTitle2] [nvarchar](50) NULL,
	[CustomTitle3] [nvarchar](50) NULL,
	[CustomTitle4] [nvarchar](50) NULL,
	[CustomTrustLogo] [nvarchar](50) NULL,
	[DemoMode] [bit] NULL,
	[DocMan] [bit] NULL,
	[E3Version] [nvarchar](30) NULL,
	[EKit] [bit] NULL,
	[EUR] [bit] NULL,
	[ExcelDir] [nvarchar](200) NULL,
	[FinancialYearDay] [int] NULL,
	[FinancialYearMonth] [int] NULL,
	[FirstDayOfWeek] [nvarchar](10) NULL,
	[HL7EOB] [int] NULL,
	[HL7EOM] [int] NULL,
	[HL7EOS] [int] NULL,
	[HL7SOB] [int] NULL,
	[IsArchiveSite] [bit] NULL,
	[Maternity] [bit] NULL,
	[NN4B] [bit] NULL,
	[NN4BCA] [nvarchar](50) NULL,
	[NN4BCL] [nvarchar](500) NULL,
	[NN4BLog] [nvarchar](50) NULL,
	[NN4BPass] [nvarchar](50) NULL,
	[NN4BS1] [nvarchar](50) NULL,
	[NN4BS1P] [nvarchar](50) NULL,
	[NN4BS2] [nvarchar](50) NULL,
	[NN4BS2P] [nvarchar](50) NULL,
	[NN4BTime] [nvarchar](50) NULL,
	[Neonatal] [bit] NULL,
	[NeonatalModule] [bit] NULL,
	[ObservationChartSite] [bit] NULL,
	[Organization] [nvarchar](50) NULL,
	[PAS] [bit] NULL,
	[PASAdd] [bit] NULL,
	[PASRevise] [bit] NULL,
	[PCExcelDrv] [nvarchar](2) NULL,
	[PCReportDrv] [nvarchar](2) NULL,
	[PCTemplateDrv] [nvarchar](2) NULL,
	[PartogramSaveTO] [int] NULL,
	[PartogramSite] [bit] NULL,
	[PostBooking] [int] NULL,
	[PostDelivery] [int] NULL,
	[PrintReports] [bit] NULL,
	[QnDefault] [bit] NULL,
	[RecoverySite] [bit] NULL,
	[ReportDir] [nvarchar](200) NULL,
	[ServerExcelDrv] [nvarchar](2) NULL,
	[ServerReportDrv] [nvarchar](2) NULL,
	[ServerTemplateDrv] [nvarchar](2) NULL,
	[SystemCharting] [bit] NULL,
	[SystemManager] [int] NULL,
	[TPRChartSite] [bit] NULL,
	[TemplateDir] [nvarchar](200) NULL,
	[TriumMode] [bit] NULL,
	[TrustName] [nvarchar](200) NULL,
	[ViewPoint] [bit] NULL,
	[WebPath] [nvarchar](50) NULL,
	[WelcomeBan1] [nvarchar](200) NULL,
	[WelcomeBan2] [nvarchar](200) NULL,
	[WelcomeBan3] [nvarchar](200) NULL,
	[WelcomeBan4] [nvarchar](200) NULL,
	[WelcomeBan5] [nvarchar](200) NULL,
	[ExpiryDays] [int] NULL,
	[ExpiryWarn] [int] NULL,
	[HistoryType] [int] NULL,
	[HistoryValue] [int] NULL,
	[LengthMax] [int] NULL,
	[LengthMin] [int] NULL,
	[LoginAttempts] [int] NULL,
	[MatchName] [int] NULL,
	[Repeat] [int] NULL,
	[Trivial] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[DocumentsURL] [nvarchar](50) NULL,
	[MigratedData] [bit] NULL
)
