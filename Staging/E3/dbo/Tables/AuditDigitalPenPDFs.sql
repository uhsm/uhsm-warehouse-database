﻿CREATE TABLE [dbo].[AuditDigitalPenPDFs](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[DigitalPenPDFsID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[Formunique] [nvarchar](250) NULL,
	[PDFDatetime] [datetime] NULL,
	[PDFPath] [nvarchar](500) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[UserID] [int] NULL,
	[GuID] [uniqueidentifier] NULL,
	[LastUpdated] [datetime] NULL
)
