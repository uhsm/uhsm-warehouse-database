﻿CREATE TABLE [dbo].[AuditDoctorSpecialty](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[DoctorSpecialtyID] [int] NULL,
	[PersonID] [int] NULL,
	[SpecialtyID] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
