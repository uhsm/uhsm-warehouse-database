﻿CREATE TABLE [dbo].[AuditEthnicOrigin](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[EthnicOriginID] [int] NULL,
	[EthnicOrigin] [varchar](50) NULL,
	[PASCode] [varchar](30) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
GO

