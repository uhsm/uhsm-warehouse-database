﻿CREATE TABLE [dbo].[AuditEvent](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[EventID] [bigint] NULL,
	[PregnancyContactID] [bigint] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[MigratedData] [bit] NULL
)
