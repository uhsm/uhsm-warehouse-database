﻿CREATE TABLE [dbo].[AuditFetus](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[FetusID] [bigint] NULL,
	[PregnancyContactID] [bigint] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
