﻿CREATE TABLE [dbo].[AuditFieldName](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[FieldNameID] [int] NULL,
	[TableNameID] [int] NULL,
	[FieldName] [nvarchar](50) NULL,
	[OldTableName] [nvarchar](50) NULL,
	[FieldType] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
