﻿CREATE TABLE [dbo].[AuditForm](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[FormID] [int] NULL,
	[Form] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
