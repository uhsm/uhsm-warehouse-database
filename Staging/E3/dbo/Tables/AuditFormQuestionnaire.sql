﻿CREATE TABLE [dbo].[AuditFormQuestionnaire](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[FormQuestionnaireID] [int] NULL,
	[QuestionnaireId] [int] NULL,
	[ValidFrom] [datetime] NULL,
	[ValidTo] [datetime] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[EK_Record] [bit] NULL,
	[Deleted] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[FormName] [nvarchar](255) NULL
)
