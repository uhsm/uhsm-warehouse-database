﻿CREATE TABLE [dbo].[AuditGPIndex](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[GPIndexID] [int] NULL,
	[PASCode] [nvarchar](50) NULL,
	[GPID] [int] NULL,
	[GPPracticeID] [int] NULL,
	[PracticeAddressID] [int] NULL,
	[Deleted] [bit] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
