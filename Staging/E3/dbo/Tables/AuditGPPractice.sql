﻿CREATE TABLE [dbo].[AuditGPPractice](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[GPPracticeID] [int] NULL,
	[PracticeCode] [nvarchar](30) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[PCTID] [int] NULL,
	[MigratedData] [bit] NULL
)
GO

