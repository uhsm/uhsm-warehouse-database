﻿CREATE TABLE [dbo].[AuditGender](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[GenderID] [int] NULL,
	[Gender] [nvarchar](50) NULL,
	[PASCode] [varchar](30) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
GO

