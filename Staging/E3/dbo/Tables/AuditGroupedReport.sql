﻿CREATE TABLE [dbo].[AuditGroupedReport](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[GroupedReportID] [int] NULL,
	[ReportGroupID] [int] NULL,
	[ReportID] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
