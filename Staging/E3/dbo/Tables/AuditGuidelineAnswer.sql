﻿CREATE TABLE [dbo].[AuditGuidelineAnswer](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[GuidelineAnswerId] [int] NULL,
	[GuidelineDetailID] [int] NULL,
	[AnswerId] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
