﻿CREATE TABLE [dbo].[AuditGuidelineDetail](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[GuidelineDetailId] [int] NULL,
	[GuidelineDetail] [nvarchar](255) NULL,
	[GuidelineGroupId] [int] NULL,
	[Description] [nvarchar](500) NULL,
	[LocationURL] [nvarchar](1000) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
