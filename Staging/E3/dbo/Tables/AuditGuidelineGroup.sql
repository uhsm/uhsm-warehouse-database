﻿CREATE TABLE [dbo].[AuditGuidelineGroup](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[GuidelineGroupId] [int] NULL,
	[GuidelineGroup] [nvarchar](255) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
