﻿CREATE TABLE [dbo].[AuditGuidelinesForPatient](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[GuidelineUsed] [int] NULL,
	[CareplanId] [int] NULL,
	[PregnancyId] [bigint] NULL,
	[BabyId] [bigint] NULL,
	[QuestionId] [int] NULL,
	[AnswerId] [int] NULL,
	[GuidelineDetailId] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
