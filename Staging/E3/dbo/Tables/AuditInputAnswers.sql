﻿CREATE TABLE [dbo].[AuditInputAnswers](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[FieldNameID] [int] NULL,
	[InstanceID] [int] NULL,
	[AnswerText] [nvarchar](1000) NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[MigratedData] [bit] NULL
)
