﻿CREATE TABLE [dbo].[AuditInputDetails](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[InputDetailsID] [bigint] NULL,
	[InputID] [bigint] NULL,
	[IsProblem] [bit] NULL,
	[IsCurrent] [bit] NULL,
	[StartDate] [datetime] NULL,
	[PersonID] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
