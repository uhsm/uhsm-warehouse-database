﻿CREATE TABLE [dbo].[AuditIrishSettings](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[IrishSettingsID] [int] NULL,
	[ConsecutiveBabyNumber] [int] NULL,
	[Year] [int] NULL,
	[MigratedData] [bit] NULL
)
