﻿CREATE TABLE [dbo].[AuditLSOA](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[LSOAID] [int] NULL,
	[LSOA] [nvarchar](50) NULL,
	[LastUpdated] [smalldatetime] NULL,
	[Deleted] [bit] NULL,
	[UserID] [int] NULL,
	[MigratedData] [bit] NULL
)
