﻿CREATE TABLE [dbo].[AuditLocalAuthority](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[LocalAuthorityID] [int] NULL,
	[LocalAuthority] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
