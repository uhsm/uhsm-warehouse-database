﻿CREATE TABLE [dbo].[AuditMemo](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[MemoID] [bigint] NULL,
	[Memo] [nvarchar](1000) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
