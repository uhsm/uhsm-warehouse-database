﻿CREATE TABLE [dbo].[AuditMessage](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[MessageID] [int] NULL,
	[Message] [nvarchar](500) NULL,
	[Important] [nchar](10) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
