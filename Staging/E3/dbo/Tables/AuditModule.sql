﻿CREATE TABLE [dbo].[AuditModule](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[ModuleID] [bigint] NULL,
	[Module] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[UserID] [int] NULL,
	[GUID] [uniqueidentifier] NULL,
	[LastUpdated] [datetime] NULL
)
