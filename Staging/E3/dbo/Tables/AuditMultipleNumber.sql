﻿CREATE TABLE [dbo].[AuditMultipleNumber](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[MultipleNumberID] [bigint] NULL,
	[MultipleNumber] [nvarchar](50) NULL,
	[PatientID] [bigint] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[MigratedData] [bit] NULL,
	[IsSystemNumber] [bit] NULL
)
