﻿CREATE TABLE [dbo].[AuditNHSVerification](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[NHSVerificationID] [int] NULL,
	[VerificationCode] [nvarchar](20) NULL,
	[TracingStatus] [nvarchar](150) NULL,
	[CMDSStatusID] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[LastUpdated] [datetime] NULL,
	[Verified] [bit] NULL,
	[MigratedData] [bit] NULL,
	[TracingStatusCode] [nvarchar](20) NULL,
	[VerificationID] [int] NULL,
	[UserID] [int] NULL
)
