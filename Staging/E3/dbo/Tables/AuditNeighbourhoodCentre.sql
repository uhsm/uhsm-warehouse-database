﻿CREATE TABLE [dbo].[AuditNeighbourhoodCentre](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[NeighbourhoodCentreID] [int] NULL,
	[NeighbourhoodCentre] [nvarchar](20) NULL,
	[Deleted] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[UserID] [int] NULL
)
