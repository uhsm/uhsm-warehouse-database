﻿CREATE TABLE [dbo].[AuditNextofKin](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[NextofKinID] [int] NULL,
	[NextofKin] [varchar](50) NULL,
	[PASCode] [varchar](30) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
