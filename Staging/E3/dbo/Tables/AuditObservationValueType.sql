﻿CREATE TABLE [dbo].[AuditObservationValueType](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[ObservationValueTypeID] [int] NULL,
	[ObservationValueType] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
