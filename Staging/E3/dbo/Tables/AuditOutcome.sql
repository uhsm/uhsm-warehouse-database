﻿CREATE TABLE [dbo].[AuditOutcome](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[OutcomeID] [int] NULL,
	[Outcome] [varchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
