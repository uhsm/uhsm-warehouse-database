﻿CREATE TABLE [dbo].[AuditParentalContact](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[ParentalContactID] [bigint] NULL,
	[ParentalContact] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[MigratedData] [bit] NULL
)
