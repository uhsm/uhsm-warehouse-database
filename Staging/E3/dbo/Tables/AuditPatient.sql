﻿CREATE TABLE [dbo].[AuditPatient](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[PatientID] [bigint] NULL,
	[TitleID] [int] NULL,
	[FullName] [nvarchar](50) NULL,
	[Forenames] [nvarchar](50) NULL,
	[Surname] [nvarchar](50) NULL,
	[Adopted] [bit] NULL,
	[ArchiveID] [int] NULL,
	[BabyBornOutsideTrust] [bit] NULL,
	[CountryBirthID] [int] NULL,
	[DoB] [datetime] NULL,
	[EthnicOriginID] [int] NULL,
	[GPUsualCare] [int] NULL,
	[GenderID] [int] NULL,
	[HasArchiveRecord] [bit] NULL,
	[HospitalNumber] [nvarchar](20) NULL,
	[Infant] [bit] NULL,
	[LocalAuthorityID] [int] NULL,
	[LanguageID] [int] NULL,
	[MaritalStatusID] [int] NULL,
	[NHSNumber] [nvarchar](15) NULL,
	[NHSNumberRequestedBy] [int] NULL,
	[NN4BChildHealthOrg] [nvarchar](50) NULL,
	[NN4BMessage] [nvarchar](255) NULL,
	[NN4BOrganisationCode] [nvarchar](50) NULL,
	[NN4BRequestedBy] [nvarchar](50) NULL,
	[NN4BTime] [nvarchar](50) NULL,
	[Occupation] [nvarchar](50) NULL,
	[OverseasVisitorID] [int] NULL,
	[PASUnitReferenceNumber] [nvarchar](50) NULL,
	[PPS] [nvarchar](10) NULL,
	[PartnerOccupation] [nvarchar](50) NULL,
	[PCTID] [int] NULL,
	[ReligionID] [int] NULL,
	[BabyID] [bigint] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[GPUsualPersonID] [int] NULL,
	[SystemNumber] [nvarchar](20) NULL,
	[BirthSurname] [nvarchar](50) NULL,
	[PatientNotesMemoID] [bigint] NULL,
	[GPUsualGPAddressID] [int] NULL,
	[GPUsualPracticeAddressID] [int] NULL,
	[PASInfoID] [int] NULL,
	[ChildrenCentreID] [int] NULL,
	[DeathCauseMain] [nvarchar](255) NULL,
	[DeathCauseOther] [nvarchar](255) NULL,
	[DeathContact] [nvarchar](255) NULL,
	[DeathCoroner] [nvarchar](255) NULL,
	[DeathCoronerPM] [nvarchar](255) NULL,
	[DeathDate] [datetime] NULL,
	[DeathDiseaseMaternal] [nvarchar](255) NULL,
	[DeathOtherMaternal] [nvarchar](255) NULL,
	[DeathOtherRelevant] [nvarchar](255) NULL,
	[DeathPlace] [nvarchar](255) NULL,
	[DeathPM] [nvarchar](255) NULL,
	[DeathPMConsent] [nvarchar](255) NULL,
	[DeathPMDT] [datetime] NULL,
	[DeathPMPlace] [nvarchar](255) NULL,
	[PostMortem] [nvarchar](255) NULL,
	[Age] [int] NULL,
	[BoardNumber] [nvarchar](10) NULL,
	[NHSVerificationID] [int] NULL,
	[MergedData] [bit] NULL,
	[DeathLocationDescription] [nvarchar](255) NULL,
	[DeathPMConsentRequired] [bit] NULL,
	[BloodGroup] [nvarchar](25) NULL,
	[MigratedData] [bit] NULL,
	[PatientNotesConfidential] [bit] NULL
)
GO

