﻿CREATE TABLE [dbo].[AuditPatientAddress](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[PatientAddressID] [bigint] NULL,
	[AddressTypeID] [int] NULL,
	[PatientID] [bigint] NULL,
	[BabyID] [bigint] NULL,
	[AddressID] [bigint] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
GO

