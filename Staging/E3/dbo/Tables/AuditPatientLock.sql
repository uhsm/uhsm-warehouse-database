﻿CREATE TABLE [dbo].[AuditPatientLock](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[PatientLockID] [int] NULL,
	[PatientID] [int] NULL,
	[LockDate] [datetime] NULL,
	[LockedByUserID] [int] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[LockedByMachine] [nvarchar](50) NULL,
	[MigratedData] [bit] NULL
)
