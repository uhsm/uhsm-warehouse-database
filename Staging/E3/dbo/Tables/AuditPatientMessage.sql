﻿CREATE TABLE [dbo].[AuditPatientMessage](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[PatientMessageID] [bigint] NULL,
	[PatientID] [bigint] NULL,
	[Message] [text] NULL,
	[Falg] [bit] NULL,
	[PatientMessageTypeID] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
