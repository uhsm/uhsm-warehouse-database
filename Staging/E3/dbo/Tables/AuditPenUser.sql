﻿CREATE TABLE [dbo].[AuditPenUser](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[PenUserId] [int] NULL,
	[PenId] [nvarchar](255) NULL,
	[E3UserId] [int] NULL,
	[E3Username] [nvarchar](255) NULL,
	[ValidFrom] [datetime] NULL,
	[ValidTo] [datetime] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[EK_Record] [bit] NULL,
	[Deleted] [bit] NULL,
	[GUID] [uniqueidentifier] NULL
)
