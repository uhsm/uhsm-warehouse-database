﻿CREATE TABLE [dbo].[AuditPersonContact](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[PersonContactID] [int] NULL,
	[PersonID] [int] NULL,
	[EMail] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
	[HomeTelephone] [nvarchar](50) NULL,
	[MobileTelephone] [nvarchar](50) NULL,
	[WorkTelephone] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
