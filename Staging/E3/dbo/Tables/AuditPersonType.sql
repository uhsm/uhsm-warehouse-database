﻿CREATE TABLE [dbo].[AuditPersonType](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[PersonTypeID] [int] NULL,
	[PersonType] [varchar](50) NULL,
	[IsEndUser] [bit] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
