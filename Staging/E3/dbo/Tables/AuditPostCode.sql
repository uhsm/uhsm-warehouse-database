﻿CREATE TABLE [dbo].[AuditPostCode](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[PostCodeID] [int] NULL,
	[PostCode] [nvarchar](10) NULL,
	[ChildrenCentreID] [int] NULL,
	[PCTID] [int] NULL,
	[Deleted] [bit] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[LSOAID] [int] NULL,
	[UserID] [int] NULL,
	[MigratedData] [bit] NULL,
	[NeighbourhoodCentreID] [int] NULL
)
