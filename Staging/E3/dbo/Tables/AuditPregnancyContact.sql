﻿CREATE TABLE [dbo].[AuditPregnancyContact](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[PregnancyContactID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[ContactTypeId] [int] NULL,
	[ParentPregnancyContactID] [bigint] NULL,
	[MergedData] [bit] NULL,
	[MigratedData] [bit] NULL
)
