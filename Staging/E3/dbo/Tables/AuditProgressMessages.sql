﻿CREATE TABLE [dbo].[AuditProgressMessages](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[ProgressID] [int] NULL,
	[ProgressMessage] [nvarchar](max) NULL,
	[BabyID] [int] NULL,
	[PregnancyID] [int] NULL,
	[MessageFor] [nvarchar](255) NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[MigratedData] [bit] NULL
)
