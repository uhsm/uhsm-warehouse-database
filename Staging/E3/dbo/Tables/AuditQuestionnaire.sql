﻿CREATE TABLE [dbo].[AuditQuestionnaire](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[QuestionnaireID] [int] NULL,
	[Questionnaire] [nvarchar](50) NULL,
	[PostProcess] [nvarchar](50) NULL,
	[ReportGroupID] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
