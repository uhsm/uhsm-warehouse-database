﻿CREATE TABLE [dbo].[AuditReadOnlyControl](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[ReadOnlyControlID] [int] NULL,
	[FormName] [nvarchar](255) NULL,
	[ControlName] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[Enabled] [bit] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[MigratedData] [bit] NULL
)
