﻿CREATE TABLE [dbo].[AuditReport](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[ReportID] [int] NULL,
	[FileName] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[PopulateSQL] [nvarchar](2000) NULL,
	[PrintCopies] [nvarchar](2000) NULL,
	[Requestable] [bit] NULL,
	[WordDocument] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
