﻿CREATE TABLE [dbo].[AuditReportRule](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[ReportRuleID] [int] NULL,
	[ReportID] [int] NULL,
	[RulesID] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
