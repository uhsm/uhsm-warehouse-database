﻿CREATE TABLE [dbo].[AuditReportToHideOnAddionalReports](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[ReportToHideOnAddionalReportsID] [bigint] NULL,
	[ReportPath] [nvarchar](500) NULL,
	[Careplan] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[LastUpdated] [datetime] NULL,
	[UserID] [int] NULL
)
