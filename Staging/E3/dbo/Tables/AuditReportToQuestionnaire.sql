﻿CREATE TABLE [dbo].[AuditReportToQuestionnaire](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[ReportName] [nvarchar](200) NULL,
	[CarePlanId] [int] NULL,
	[ElementId] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
