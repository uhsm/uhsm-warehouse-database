﻿CREATE TABLE [dbo].[AuditRiskType](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[RiskTypeID] [int] NULL,
	[RiskType] [nvarchar](50) NULL,
	[EK_Record] [bit] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[Deleted] [bit] NULL,
	[MigratedData] [bit] NULL
)
