﻿CREATE TABLE [dbo].[AuditRoleTree](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[RoleTreeID] [int] NULL,
	[RoleTree] [nvarchar](255) NULL,
	[RoleColumnName] [nvarchar](255) NULL,
	[ParentRoleTreeID] [int] NULL,
	[Sequence] [int] NULL,
	[Deleted] [bit] NULL
)
