﻿CREATE TABLE [dbo].[AuditStaffGrade](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[StaffGradeID] [int] NULL,
	[StaffGrade] [varchar](50) NULL,
	[PersonTypeID] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
