﻿CREATE TABLE [dbo].[AuditStatsBuilderTemplates](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[StatsBuilderTemplatesID] [int] NULL,
	[Name] [nvarchar](255) NULL,
	[CustomTablename] [nvarchar](255) NULL,
	[StatsTemplateXML] [nvarchar](max) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[MainTreeNode] [bit] NULL,
	[QuestionnaireID] [int] NULL
)
