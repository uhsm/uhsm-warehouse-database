﻿CREATE TABLE [dbo].[AuditStatsGroup](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[StatsGroupID] [bigint] NULL,
	[Description] [nvarchar](50) NULL,
	[StatsMainID] [int] NULL,
	[Nr] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
