﻿CREATE TABLE [dbo].[AuditStatsLayoutBuilder](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[StatsLayoutBuilderId] [int] NULL,
	[LayoutXML] [nvarchar](max) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
