﻿CREATE TABLE [dbo].[AuditStatsMain](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[StatsMainID] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[XMLFilename] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
