﻿CREATE TABLE [dbo].[AuditStatsReport](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[StatsReportID] [bigint] NULL,
	[DetailParam1] [nvarchar](50) NULL,
	[DetailParam2] [nvarchar](50) NULL,
	[DetailParam3] [nvarchar](50) NULL,
	[DetailParam4] [nvarchar](50) NULL,
	[LinkDetailParam1] [nvarchar](50) NULL,
	[LinkDetailParam2] [nvarchar](50) NULL,
	[LinkDetailParam3] [nvarchar](50) NULL,
	[LinkDetailParam4] [nvarchar](50) NULL,
	[ReturnField] [nvarchar](50) NULL,
	[Stats] [nvarchar](50) NULL,
	[StatsDetail] [nvarchar](50) NULL,
	[StatsField] [nvarchar](50) NULL,
	[StatsParam1] [nvarchar](50) NULL,
	[StatsParam2] [nvarchar](50) NULL,
	[StatsParam3] [nvarchar](50) NULL,
	[StatsParam4] [nvarchar](50) NULL,
	[StatsTotal] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
