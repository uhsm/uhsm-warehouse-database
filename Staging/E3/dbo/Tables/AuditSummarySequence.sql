﻿CREATE TABLE [dbo].[AuditSummarySequence](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[SummarySequenceID] [int] NULL,
	[QuestionnaireID] [bigint] NULL,
	[ActivityName] [nvarchar](200) NULL,
	[QuestionID] [nvarchar](50) NULL,
	[PositionID] [int] NULL,
	[IsRootQuestion] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[MigratedData] [bit] NULL,
	[UniqueNumber] [int] NULL
)
