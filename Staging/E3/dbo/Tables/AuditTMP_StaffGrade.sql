﻿CREATE TABLE [dbo].[AuditTMP_StaffGrade](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[StaffGradeID] [int] NULL,
	[StaffGrade] [varchar](50) NULL,
	[PersonTypeID] [tinyint] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
