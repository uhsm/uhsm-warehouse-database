﻿CREATE TABLE [dbo].[AuditTRIGGERINPUT](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[TriggerInputID] [int] NULL,
	[ConcAnswer] [varchar](8000) NULL,
	[ReadRecord] [bit] NULL,
	[MigratedData] [bit] NULL
)
