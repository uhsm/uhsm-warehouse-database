﻿CREATE TABLE [dbo].[AuditTableSizeHistory](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[id] [int] NULL,
	[historyDate] [datetime] NULL,
	[schemaName] [nvarchar](128) NULL,
	[tableName] [nvarchar](128) NULL,
	[rows] [int] NULL,
	[reservedKB] [int] NULL,
	[dataKB] [int] NULL,
	[indexSizeKB] [int] NULL,
	[unusedKB] [int] NULL,
	[MigratedData] [bit] NULL
)
