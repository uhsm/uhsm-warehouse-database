﻿CREATE TABLE [dbo].[AuditTempFunctions](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[name] [nvarchar](128) NULL,
	[MigratedData] [bit] NULL
)
