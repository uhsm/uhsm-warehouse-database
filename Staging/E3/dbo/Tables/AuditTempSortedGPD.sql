﻿CREATE TABLE [dbo].[AuditTempSortedGPD](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[GMCCode] [varchar](250) NULL,
	[GPSurnameInitial] [varchar](250) NULL,
	[AddressLine1] [varchar](250) NULL,
	[AddressLine2] [varchar](250) NULL,
	[AddressLine3] [varchar](250) NULL,
	[AddressLine4] [varchar](250) NULL,
	[PostCode] [varchar](250) NULL,
	[PracticeCode] [varchar](250) NULL,
	[TelephoneNumber] [varchar](250) NULL,
	[MigratedData] [bit] NULL
)
