﻿CREATE TABLE [dbo].[AuditTempSystemListCodes](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[SystemListCodesID] [varchar](250) NULL,
	[Code] [varchar](250) NULL,
	[Deleted] [varchar](250) NULL,
	[Title] [varchar](250) NULL,
	[MigratedData] [bit] NULL
)
