﻿CREATE TABLE [dbo].[AuditTempView](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[name] [nvarchar](128) NULL,
	[MigratedData] [bit] NULL
)
