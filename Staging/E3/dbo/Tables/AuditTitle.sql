﻿CREATE TABLE [dbo].[AuditTitle](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[TitleID] [int] NULL,
	[Title] [varchar](50) NULL,
	[PASCode] [varchar](30) NULL,
	[Mother] [bit] NULL,
	[Partner] [bit] NULL,
	[NOK] [bit] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
