﻿CREATE TABLE [dbo].[AuditUserPassword](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[UserPasswordID] [bigint] NULL,
	[Password] [nvarchar](50) NULL,
	[Expiry] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
