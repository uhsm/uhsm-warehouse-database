﻿CREATE TABLE [dbo].[AuditUserPatient](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[UserPatientID] [int] NULL,
	[UsersID] [int] NULL,
	[PatientID] [bigint] NULL,
	[UpdatedBy] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
