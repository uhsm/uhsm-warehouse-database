﻿CREATE TABLE [dbo].[AuditUserRole](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[UserRoleID] [int] NULL,
	[UsersID] [int] NULL,
	[RolesID] [int] NULL,
	[IsDefault] [bit] NULL,
	[UpdatedBy] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
