﻿CREATE TABLE [dbo].[AuditValidationType](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[ValidationTypeID] [int] NULL,
	[ValidationType] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
