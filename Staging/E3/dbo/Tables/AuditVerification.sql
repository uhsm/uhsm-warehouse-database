﻿CREATE TABLE [dbo].[AuditVerification](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[VerificationID] [int] NULL,
	[Verification] [nvarchar](100) NULL,
	[VerificationStatusCode] [nvarchar](5) NULL,
	[Deleted] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[UserID] [int] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[MigratedData] [bit] NULL
)
