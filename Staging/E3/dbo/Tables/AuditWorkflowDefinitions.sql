﻿CREATE TABLE [dbo].[AuditWorkflowDefinitions](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[WorkFlowDefinitionsID] [int] NULL,
	[NodeName] [nvarchar](255) NULL,
	[QuestionnaireID] [int] NULL,
	[QuestionID] [int] NULL,
	[QuestionActivityName] [nvarchar](255) NULL,
	[IfElseName] [nvarchar](255) NULL,
	[IfElseBranchName] [nvarchar](255) NULL,
	[ParentNames] [nvarchar](max) NULL,
	[Parent] [nvarchar](255) NULL,
	[NodeID] [int] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[MigratedData] [bit] NULL
)
