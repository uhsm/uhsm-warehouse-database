﻿CREATE TABLE [dbo].[AuditXOMLQuestionnaire](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[XOMLQuestionnaireID] [int] NULL,
	[QuestionnaireID] [int] NULL,
	[XOMLFile] [xml] NULL,
	[DLLValue] [binary](1) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
