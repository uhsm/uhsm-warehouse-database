﻿CREATE TABLE [dbo].[Auditauthor](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[id] [int] NULL,
	[name] [varchar](10) NULL,
	[active] [bit] NULL,
	[MigratedData] [bit] NULL
)
