﻿CREATE TABLE [dbo].[Audithistory](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[id] [int] NULL,
	[number] [char](20) NULL,
	[name] [varchar](100) NULL,
	[notes] [varchar](255) NULL,
	[authorId] [int] NULL,
	[executed] [datetime] NULL,
	[MigratedData] [bit] NULL
)
