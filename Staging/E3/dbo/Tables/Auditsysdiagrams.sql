﻿CREATE TABLE [dbo].[Auditsysdiagrams](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[name] [nvarchar](128) NULL,
	[principal_id] [int] NULL,
	[diagram_id] [int] NULL,
	[version] [int] NULL,
	[definition] [varbinary](1) NULL,
	[MigratedData] [bit] NULL
)
