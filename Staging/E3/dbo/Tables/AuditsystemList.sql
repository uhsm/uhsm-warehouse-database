﻿CREATE TABLE [dbo].[AuditsystemList](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[SystemListID] [int] NULL,
	[Code] [varchar](25) NULL,
	[Name] [varchar](50) NULL,
	[Sequence] [int] NULL,
	[SystemListCode] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
