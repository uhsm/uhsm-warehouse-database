﻿CREATE TABLE [dbo].[AuditsystemListCodes](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[SystemListCodesID] [int] NULL,
	[Code] [varchar](25) NULL,
	[Title] [varchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[MigratedData] [bit] NULL
)
