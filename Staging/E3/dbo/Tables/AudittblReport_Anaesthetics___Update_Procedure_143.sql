﻿CREATE TABLE [dbo].[AudittblReport_Anaesthetics___Update_Procedure_143](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[UserID] [int] NULL,
	[PregnancyID] [bigint] NULL,
	[PregnancycontactID] [bigint] NULL,
	[AnaesUpdateDT] [nvarchar](1000) NULL,
	[RegionalProblem] [nvarchar](1000) NULL,
	[AnaesLipidRescue] [nvarchar](1000) NULL,
	[RegionalProblem_Value] [bigint] NULL,
	[AnaesLipidRescue_Value] [bigint] NULL,
	[AnaesUpdateComments] [nvarchar](1000) NULL
)
