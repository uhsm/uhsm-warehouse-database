﻿CREATE TABLE [dbo].[AudittblReport_Asymptomatic_Bacteriuria_Screen_133](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](1000) NULL,
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[ASBTestDate] [nvarchar](1000) NULL,
	[ASBTestDate_Value] [bigint] NULL,
	[ASBResult] [nvarchar](1000) NULL
)
