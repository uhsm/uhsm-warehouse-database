﻿CREATE TABLE [dbo].[AudittblReport_Blood_Group_131](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](1000) NULL,
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[BloodGroupTestDate] [nvarchar](1000) NULL,
	[BloodGroupTestDate_Value] [bigint] NULL,
	[BloodGroup] [nvarchar](1000) NULL,
	[Antibodies] [nvarchar](1000) NULL,
	[BloodGroup_Value] [bigint] NULL,
	[Antibodies_Value] [bigint] NULL,
	[AntibodyTestPartner] [nvarchar](1000) NULL
)
