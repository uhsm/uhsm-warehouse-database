﻿CREATE TABLE [dbo].[AudittblReport_Chlamydia_Screen_132](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](1000) NULL,
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[ChlamydiaTestDate] [nvarchar](1000) NULL,
	[ChlamydiaScreenResult] [nvarchar](1000) NULL,
	[ChlamydiaTestDate_Value] [bigint] NULL,
	[ChlamydiaScreenResult_Value] [bigint] NULL,
	[ChlamydiaActionTaken] [nvarchar](1000) NULL
)
