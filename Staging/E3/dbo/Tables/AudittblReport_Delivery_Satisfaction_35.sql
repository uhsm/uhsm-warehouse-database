﻿CREATE TABLE [dbo].[AudittblReport_Delivery_Satisfaction_35](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](1000) NULL,
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[MothersExperience] [nvarchar](1000) NULL,
	[SatisfactionDelivery] [nvarchar](1000) NULL,
	[SatisfactionLabour] [nvarchar](1000) NULL,
	[MigratedData] [bit] NULL
)
