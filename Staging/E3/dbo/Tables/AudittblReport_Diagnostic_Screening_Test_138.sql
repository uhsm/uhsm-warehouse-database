﻿CREATE TABLE [dbo].[AudittblReport_Diagnostic_Screening_Test_138](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[ResultofAmniotesis_Value] [bigint] NULL,
	[ResultofCVS_Value] [bigint] NULL,
	[ResultofOtherDiagnostic_Value] [bigint] NULL,
	[ResultofAmniotesis] [nvarchar](255) NULL,
	[ResultofCVS] [nvarchar](255) NULL,
	[ResultofOtherDiagnostic] [nvarchar](255) NULL
)
