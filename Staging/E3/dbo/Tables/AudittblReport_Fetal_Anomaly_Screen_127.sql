﻿CREATE TABLE [dbo].[AudittblReport_Fetal_Anomaly_Screen_127](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](1000) NULL,
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[USSFetalAnomalyDate] [nvarchar](1000) NULL,
	[USSRepeatScan] [nvarchar](1000) NULL,
	[USSFetalAnomalyDate_Value] [bigint] NULL,
	[USSRepeatScan_Value] [bigint] NULL,
	[USSRepeatScanDate] [nvarchar](1000) NULL,
	[EDDScan] [nvarchar](1000) NULL
)
