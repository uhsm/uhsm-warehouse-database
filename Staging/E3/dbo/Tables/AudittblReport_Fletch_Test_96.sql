﻿CREATE TABLE [dbo].[AudittblReport_Fletch_Test_96](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](1000) NULL,
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[SmokerBeforePregnancy_Value] [bigint] NULL,
	[SmokerBeforePregnancy] [nvarchar](1000) NULL,
	[MigratedData] [bit] NULL
)
