﻿CREATE TABLE [dbo].[AudittblReport_Missed_Contact_PN_20](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](1000) NULL,
	[UserID] [int] NULL,
	[PregnancycontactID] [bigint] NULL,
	[FetusID] [bigint] NULL,
	[CTGAbnormality] [nvarchar](1000) NULL,
	[CTGNormal] [nvarchar](1000) NULL,
	[CTGPerformed] [nvarchar](1000) NULL,
	[FetPosition] [nvarchar](1000) NULL,
	[HeartHeard] [nvarchar](1000) NULL,
	[Lie] [nvarchar](1000) NULL,
	[Movements] [nvarchar](1000) NULL,
	[Presentation] [nvarchar](1000) NULL
)
