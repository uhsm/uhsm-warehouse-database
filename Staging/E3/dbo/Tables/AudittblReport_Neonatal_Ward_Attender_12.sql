﻿CREATE TABLE [dbo].[AudittblReport_Neonatal_Ward_Attender_12](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](1000) NULL,
	[UserID] [int] NULL,
	[BabyID] [bigint] NULL,
	[BabyadmissionID] [bigint] NULL,
	[AdditionalComments] [nvarchar](1000) NULL,
	[AdmitDT] [nvarchar](1000) NULL,
	[AdmitFrom] [nvarchar](1000) NULL,
	[AdmitReason] [nvarchar](1000) NULL,
	[Destination] [nvarchar](1000) NULL,
	[DischargeDT] [nvarchar](1000) NULL,
	[MigratedData] [bit] NULL
)
