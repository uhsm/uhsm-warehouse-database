﻿CREATE TABLE [dbo].[AudittblReport_Not_Used_Delivery_Caesarean_Pre_Assessment_29](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](1000) NULL,
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[AnaestheticHistory_Value] [bigint] NULL,
	[AnaestheticHistory] [nvarchar](1000) NULL,
	[ConsentCS] [nvarchar](1000) NULL,
	[CSCurrentMedication] [nvarchar](1000) NULL,
	[DentalChkPreCS] [nvarchar](1000) NULL,
	[ReasonCSOther] [nvarchar](1000) NULL,
	[ReasonCSPrimary] [nvarchar](1000) NULL,
	[ReasonCSSecondary] [nvarchar](1000) NULL,
	[ThromboembolicRisk] [nvarchar](1000) NULL,
	[MigratedData] [bit] NULL
)
