﻿CREATE TABLE [dbo].[AudittblReport_Not_Used_Delivery_Induction_Membrane_Sweep_37](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](1000) NULL,
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[InductionMembranes] [nvarchar](1000) NULL,
	[InductionSweepBy] [nvarchar](1000) NULL,
	[InductionSweepDT] [nvarchar](1000) NULL,
	[InductionSweepOffer] [nvarchar](1000) NULL,
	[InductionSweepPerf] [nvarchar](1000) NULL,
	[MigratedData] [bit] NULL
)
