﻿CREATE TABLE [dbo].[AudittblReport_Not_Used_Delivery_Unbooked_Pre_Labour_36](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](1000) NULL,
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[ProblemsMedical_Value] [bigint] NULL,
	[Smokes_Value] [bigint] NULL,
	[AlcoholInPregnancy] [nvarchar](1000) NULL,
	[BirthPlaceIntended] [nvarchar](1000) NULL,
	[BleedingEpisodes] [nvarchar](1000) NULL,
	[DrugAbusePregnancy] [nvarchar](1000) NULL,
	[ProblemsBP] [nvarchar](1000) NULL,
	[ProblemsFetal] [nvarchar](1000) NULL,
	[ProblemsMedical] [nvarchar](1000) NULL,
	[Smokes] [nvarchar](1000) NULL,
	[MigratedData] [bit] NULL
)
