﻿CREATE TABLE [dbo].[AudittblReport_Not_Used_Pre_Labour_1](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](1000) NULL,
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[CaesareanSections_Value] [bigint] NULL,
	[DeathsAfter28Days_Value] [bigint] NULL,
	[Ectopics_Value] [bigint] NULL,
	[Gravida_Value] [bigint] NULL,
	[Livebirths_Value] [bigint] NULL,
	[Miscarriages_Value] [bigint] NULL,
	[NeonatalDeaths_Value] [bigint] NULL,
	[NumberPrevMoles_Value] [bigint] NULL,
	[Stillbirths_Value] [bigint] NULL,
	[Terminations_Value] [bigint] NULL,
	[CaesareanSections] [nvarchar](1000) NULL,
	[Category] [nvarchar](1000) NULL,
	[DeathsAfter28Days] [nvarchar](1000) NULL,
	[Ectopics] [nvarchar](1000) NULL,
	[Gravida] [nvarchar](1000) NULL,
	[Livebirths] [nvarchar](1000) NULL,
	[Miscarriages] [nvarchar](1000) NULL,
	[NeonatalDeaths] [nvarchar](1000) NULL,
	[NumberPrevMoles] [nvarchar](1000) NULL,
	[Stillbirths] [nvarchar](1000) NULL,
	[Terminations] [nvarchar](1000) NULL,
	[DrugAbusePregnancy_Value] [bigint] NULL,
	[AdmissionAN] [nvarchar](1000) NULL,
	[AFPNormal] [nvarchar](1000) NULL,
	[AlcoholInPregnancy] [nvarchar](1000) NULL,
	[AntenatalCare] [nvarchar](1000) NULL,
	[Antibodies] [nvarchar](1000) NULL,
	[BirthPlaceIntended] [nvarchar](1000) NULL,
	[BleedingEpisodes] [nvarchar](1000) NULL,
	[BMI] [nvarchar](1000) NULL,
	[CareType] [nvarchar](1000) NULL,
	[DrugAbusePregnancy] [nvarchar](1000) NULL,
	[EDDCalculatedBy] [nvarchar](1000) NULL,
	[EDDScan] [nvarchar](1000) NULL,
	[HbElect] [nvarchar](1000) NULL,
	[HbLast] [nvarchar](1000) NULL,
	[HepatitisScreen] [nvarchar](1000) NULL,
	[HIVTest] [nvarchar](1000) NULL,
	[NumberClinicContacts] [nvarchar](1000) NULL,
	[NumberScansPerformed] [nvarchar](1000) NULL,
	[ProblemsBP] [nvarchar](1000) NULL,
	[ProblemsFetal] [nvarchar](1000) NULL,
	[ProblemsMedical] [nvarchar](1000) NULL,
	[ReasonForChange] [nvarchar](1000) NULL,
	[RubellaStatus] [nvarchar](1000) NULL,
	[ScanFindings] [nvarchar](1000) NULL,
	[SmearLast] [nvarchar](1000) NULL,
	[Smokes] [nvarchar](1000) NULL,
	[Syphylis] [nvarchar](1000) NULL,
	[Treatment] [nvarchar](1000) NULL,
	[MigratedData] [bit] NULL
)
