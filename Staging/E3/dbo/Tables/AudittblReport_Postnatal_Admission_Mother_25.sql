﻿CREATE TABLE [dbo].[AudittblReport_Postnatal_Admission_Mother_25](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](1000) NULL,
	[UserID] [int] NULL,
	[PregnancyID] [bigint] NULL,
	[PregnancyadmissionID] [bigint] NULL,
	[AdmissionTo_Value] [bigint] NULL,
	[AdmittedBy_Value] [bigint] NULL,
	[Mode_Value] [bigint] NULL,
	[Reason_Value] [bigint] NULL,
	[AdmissionDT] [nvarchar](1000) NULL,
	[AdmissionTo] [nvarchar](1000) NULL,
	[AdmittedBy] [nvarchar](1000) NULL,
	[Mode] [nvarchar](1000) NULL,
	[Reason] [nvarchar](1000) NULL,
	[MigratedData] [bit] NULL
)
