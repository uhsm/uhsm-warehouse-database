﻿CREATE TABLE [dbo].[AudittblReport_Results_at_28_weeks_142](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[Hb28Weeks] [nvarchar](1000) NULL,
	[Antibodies] [nvarchar](1000) NULL,
	[Antibodies_Value] [bigint] NULL,
	[ResultRandomBloodSugar] [nvarchar](1000) NULL,
	[ResultRandomBloodSugar_Value] [bigint] NULL,
	[Results28WeeksAddedDate] [nvarchar](1000) NULL
)
