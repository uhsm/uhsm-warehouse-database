﻿CREATE TABLE [dbo].[AudittblReport_Rubella_Screen_129](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](1000) NULL,
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[RubellaTestDate] [nvarchar](1000) NULL,
	[RubellaTestDate_Value] [bigint] NULL,
	[RubellaStatus] [nvarchar](1000) NULL
)
