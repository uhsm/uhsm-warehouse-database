﻿CREATE TABLE [dbo].[AudittblReport_Syphilis_Screen_130](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](1000) NULL,
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[SyphilisTestDate] [nvarchar](1000) NULL,
	[SyphilisTestDate_Value] [bigint] NULL,
	[SyphilisTestResult] [nvarchar](1000) NULL,
	[SyphilisTestResult_Value] [bigint] NULL,
	[SyphilisActionTaken] [nvarchar](1000) NULL,
	[SyphilisClassification] [nvarchar](1000) NULL,
	[SyphilisActionTaken_Value] [bigint] NULL
)
