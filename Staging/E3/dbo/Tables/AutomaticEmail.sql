﻿CREATE TABLE [dbo].[AutomaticEmail](
	[AutomaticEmailId] [bigint] NOT NULL,
	[DateSend] [datetime] NULL,
	[HospitalNumber] [nvarchar](30) NULL,
	[SystemNumber] [nvarchar](30) NULL,
	[DocumentType] [nvarchar](255) NULL,
	[EmailCode] [nvarchar](max) NULL,
	[Deleted] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[UserID] [int] NULL,
	[EK_Record] [bit] NULL
)
