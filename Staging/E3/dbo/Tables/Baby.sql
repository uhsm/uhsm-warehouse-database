﻿CREATE TABLE [dbo].[Baby](
	[BabyID] [bigint] NOT NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[CarePlan] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[RequestedNN4B] [bit] NULL,
	[RegisteredInPAS] [bit] NULL,
	[BabyNotesMemoID] [bigint] NULL,
	[Birthday] [datetime] NULL,
	[GestationBirth] nvarchar(30) null
)
