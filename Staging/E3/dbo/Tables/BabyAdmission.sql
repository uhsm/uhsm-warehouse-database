﻿CREATE TABLE [dbo].[BabyAdmission](
	[BabyAdmissionID] [bigint] NOT NULL,
	[BabyID] [bigint] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
