﻿CREATE TABLE [dbo].[BabyContacts](
	[BabyID] [bigint] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[BabyContactsID] [bigint] NOT NULL
)
