﻿CREATE TABLE [dbo].[BabyDaily](
	[BabyDailyID] [bigint] NOT NULL,
	[BabyAdmissionID] [bigint] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
