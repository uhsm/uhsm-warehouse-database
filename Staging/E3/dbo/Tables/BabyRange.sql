﻿CREATE TABLE [dbo].[BabyRange](
	[BabyRangeID] [bigint] NOT NULL,
	[Finish] [int] NULL,
	[InUse] [bit] NOT NULL,
	[Prefix] [nvarchar](50) NULL,
	[RangeType] [nvarchar](50) NULL,
	[Start] [int] NULL,
	[Suffix] [nvarchar](50) NULL,
	[Warning] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[SystemNumberPrefix] [nvarchar](10) NULL,
	[HospitalNumberPrefix] [nvarchar](10) NULL,
	[TotalWidth] [int] NULL,
	[Disabled] [bit] NULL
)
