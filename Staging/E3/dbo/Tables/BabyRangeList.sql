﻿CREATE TABLE [dbo].[BabyRangeList](
	[BabyRangeListID] [bigint] NOT NULL,
	[BabyRangeList] [nvarchar](50) NULL,
	[BabyRangeID] [bigint] NULL,
	[PregnancyId] [bigint] NULL,
	[ReservedByPregnancyId] [bigint] NULL,
	[Used] [bit] NULL,
	[Deleted] [bit] NOT NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
