﻿CREATE TABLE [dbo].[BadPractice_Address](
	[GMCCode] [varchar](250) NULL,
	[GPForename] [varchar](250) NULL,
	[GPSurname] [varchar](250) NULL,
	[AddressLine1] [varchar](250) NULL,
	[AddressLine2] [varchar](250) NULL,
	[AddressLine3] [varchar](250) NULL,
	[AddressLine4] [varchar](250) NULL,
	[PostCode] [varchar](250) NULL,
	[PracticeCode] [varchar](250) NULL,
	[TelephoneNumber] [varchar](250) NULL
)
