﻿CREATE TABLE [dbo].[BloodRelative](
	[BloodRelativeID] [int] NOT NULL,
	[BloodRelative] [nvarchar](20) NULL,
	[EK_Record] [bit] NULL,
	[Deleted] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[LastUpdated] [datetime] NULL,
	[UserID] [int] NULL
)
