﻿CREATE TABLE [dbo].[CIRRAuditDuplicates](
	[CIRRAuditDuplicatesID] [bigint] NOT NULL,
	[CIRRAuditID] [bigint] NOT NULL,
	[MotherNHSNumber] [nchar](15) NULL,
	[MotherSurname] [nvarchar](35) NULL,
	[MotherForename] [nvarchar](50) NULL,
	[MotherDOB] [smalldatetime] NULL,
	[AddressLine1] [nvarchar](35) NULL,
	[AddressLine2] [nvarchar](35) NULL,
	[AddressLine3] [nvarchar](35) NULL,
	[AddressLine4] [nvarchar](35) NULL,
	[Postcode] [nvarchar](15) NULL,
	[BabyForename] [nvarchar](35) NULL,
	[BabySurname] [nvarchar](35) NULL,
	[BabyDOB] [smalldatetime] NULL,
	[BabyGender] [nchar](10) NULL,
	[BabyBirthOrder] [nchar](1) NULL,
	[BabyNHSNumber] [nchar](15) NULL
)
