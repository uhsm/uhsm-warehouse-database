﻿CREATE TABLE [dbo].[ChildrenCentre](
	[ChildrenCentreID] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Code] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UserID] [int] NULL
)
