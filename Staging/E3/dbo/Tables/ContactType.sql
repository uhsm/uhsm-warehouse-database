﻿CREATE TABLE [dbo].[ContactType](
	[ContactTypeID] [smallint] NOT NULL,
	[ContactType] [varchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
