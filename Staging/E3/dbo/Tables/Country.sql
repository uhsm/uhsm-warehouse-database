﻿CREATE TABLE [dbo].[Country](
	[CountryID] [int] NOT NULL,
	[Country] [nvarchar](100) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
