﻿CREATE TABLE [dbo].[Death](
	[DeathID] [bigint] NOT NULL,
	[PatientID] [bigint] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
