﻿CREATE TABLE [dbo].[DeathLocation](
	[DeathLocationId] [bigint] NOT NULL,
	[DeathLocation] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[sequence] [int] NULL
)
