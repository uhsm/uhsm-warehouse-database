﻿CREATE TABLE [dbo].[DigitalPenException](
	[PK_ExceptionID] [bigint] NOT NULL,
	[DateTimeSubmitted] [datetime] NOT NULL,
	[Forename] [nvarchar](50) NOT NULL,
	[Surname] [nvarchar](50) NOT NULL,
	[Dob] [datetime] NOT NULL,
	[NHSNumber] [nvarchar](50) NOT NULL,
	[HospitalNumber] [nvarchar](50) NOT NULL,
	[Resolved] [bit] NOT NULL,
	[FormUnique] [nvarchar](100) NOT NULL,
	[PrgPatientId] [bigint] NOT NULL,
	[PregnancyId] [bigint] NOT NULL
)
