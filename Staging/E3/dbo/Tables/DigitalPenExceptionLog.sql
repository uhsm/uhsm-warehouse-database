﻿CREATE TABLE [dbo].[DigitalPenExceptionLog](
	[ID] [bigint] NOT NULL,
	[FK_ExceptionId] [bigint] NOT NULL,
	[QuestionId] [bigint] NOT NULL,
	[QuestionText] [varchar](max) NULL,
	[AnswerIDList] [nvarchar](150) NOT NULL,
	[AnswerTextList] [varchar](max) NOT NULL,
	[ErrorText] [nvarchar](150) NOT NULL,
	[ErrorMessage] [nvarchar](250) NOT NULL
)
