﻿CREATE TABLE [dbo].[DigitalPenMapping](
	[MAPPINGID] [bigint] NOT NULL,
	[FORMNAME] [varchar](150) NULL,
	[QUESTIONNAIREID] [bigint] NULL,
	[QUESTIONID] [bigint] NULL,
	[ANSWERID] [bigint] NULL,
	[MAPPINGTYPE] [varchar](150) NULL,
	[INPUTQUESTIONREFERENCE] [varchar](150) NULL,
	[INPUTANSWERREFERENCE] [varchar](150) NULL,
	[COMBINEDWITHINPUTQUESTIONREFERENCE] [varchar](150) NULL,
	[COMBINEDWITHSEPERATOR] [varchar](150) NULL,
	[MEMOQUESTIONREFERENCE] [varchar](150) NULL
)
