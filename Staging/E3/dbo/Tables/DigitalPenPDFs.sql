﻿CREATE TABLE [dbo].[DigitalPenPDFs](
	[DigitalPenPDFsID] [bigint] NOT NULL,
	[PregnancyID] [bigint] NULL,
	[Formunique] [nvarchar](250) NULL,
	[PDFDatetime] [datetime] NULL,
	[PDFPath] [nvarchar](500) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[UserID] [int] NULL,
	[GuID] [uniqueidentifier] NULL,
	[LastUpdated] [datetime] NULL
)
