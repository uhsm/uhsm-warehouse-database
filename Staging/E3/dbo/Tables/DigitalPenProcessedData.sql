﻿CREATE TABLE [dbo].[DigitalPenProcessedData](
	[DigitalPenProcessedDataId] [int] NOT NULL,
	[PregnancyID] [bigint] NULL,
	[BabyID] [bigint] NULL,
	[DSOLevel] [nvarchar](20) NULL,
	[DSOLevelID] [bigint] NULL,
	[QuestionnaireID] [int] NULL,
	[HospitalNumber] [nvarchar](20) NULL,
	[NHSNumber] [nvarchar](20) NULL,
	[UserID] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[LastUpdated] [datetime] NULL,
	[FileData] [varbinary](max) NULL,
	[QuestionnairePrimaryDateValue] [nvarchar](50) NULL
)
