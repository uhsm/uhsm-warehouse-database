﻿CREATE TABLE [dbo].[DigitalPenTransactionLog](
	[GUID] [uniqueidentifier] NOT NULL,
	[LOGDATETIME] [datetime] NULL,
	[FORENAME] [varchar](50) NULL,
	[SURNAME] [varchar](50) NULL,
	[NHSNUMBER] [varchar](50) NULL,
	[HOSPITALNUMBER] [varchar](50) NULL,
	[LOGDATA] [nvarchar](max) NOT NULL
)
