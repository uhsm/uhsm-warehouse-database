﻿CREATE TABLE [dbo].[DigitalPenUnmatchedPatient](
	[ID] [bigint] NOT NULL,
	[DATETIMESUBMITTED] [datetime] NOT NULL,
	[FORENAME] [nvarchar](50) NULL,
	[SURNAME] [nvarchar](50) NULL,
	[DOB] [datetime] NULL,
	[NHSNUMBER] [nvarchar](50) NULL,
	[HOSPITALNUMBER] [nvarchar](50) NULL,
	[XMLDATA] [xml] NULL,
	[RESOLVED] [bit] NOT NULL,
	[XMLNAMEFORHOSPITALNUMBER] [nvarchar](50) NULL,
	[XMLNAMEFORNHSNUMBER] [nvarchar](50) NULL,
	[XMLNAMEFORDOB] [nvarchar](50) NULL,
	[XMLNAMEFORFORENAME] [nvarchar](50) NULL,
	[XMLNAMEFORSURNAME] [nvarchar](50) NULL,
	[FORMUNIQUE] [nvarchar](100) NULL
)
