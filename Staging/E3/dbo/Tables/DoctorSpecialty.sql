﻿CREATE TABLE [dbo].[DoctorSpecialty](
	[DoctorSpecialtyID] [int] NOT NULL,
	[PersonID] [int] NULL,
	[SpecialtyID] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
