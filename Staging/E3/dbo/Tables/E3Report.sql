﻿CREATE TABLE [dbo].[E3Report](
	[E3ReportId] [bigint] NOT NULL,
	[PregnancyId] [bigint] NULL,
	[BabyId] [bigint] NULL,
	[PregnancyAdmissionId] [bigint] NULL,
	[BabyAdmissionId] [bigint] NULL,
	[PregnancyContactId] [bigint] NULL,
	[BabyContactsId] [bigint] NULL,
	[Version] [int] NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[ConnectedTo] [nvarchar](200) NULL,
	[PDF] [varbinary](max) NOT NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[PatientID] [bigint] NULL
)
