﻿CREATE TABLE [dbo].[EROL](
	[ErolID] [bigint] NOT NULL,
	[PregnancyID] [bigint] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[Deleted] [bit] NOT NULL,
	[EK_Record] [bigint] NOT NULL,
	[GUID] [uniqueidentifier] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[LastUpdated] [datetime] NOT NULL
)
