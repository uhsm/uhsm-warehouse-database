﻿CREATE TABLE [dbo].[EROL_FluidDirection](
	[EROL_FluidDirectionID] [bigint] NOT NULL,
	[Direction] [varchar](50) NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[EK_Record] [bit] NULL,
	[Deleted] [bit] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
