﻿CREATE TABLE [dbo].[EROL_FluidIntakeOutput](
	[EROL_FluidIntakeOutputID] [bigint] NOT NULL,
	[FluidDirectionID] [bigint] NOT NULL,
	[FluidTypeID] [bigint] NOT NULL,
	[MedicationRouteID] [bigint] NOT NULL,
	[PerformedByID] [bigint] NOT NULL,
	[NoteID] [bigint] NOT NULL,
	[TimeGiven] [datetime] NOT NULL,
	[Volume] [varchar](20) NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[EK_Record] [bit] NULL,
	[Deleted] [bit] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[ErolID] [bigint] NULL
)
