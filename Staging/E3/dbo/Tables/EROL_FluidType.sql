﻿CREATE TABLE [dbo].[EROL_FluidType](
	[EROL_FluidTypeID] [bigint] NOT NULL,
	[FluidTypeName] [varchar](50) NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[EK_Record] [bit] NULL,
	[Deleted] [bit] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
