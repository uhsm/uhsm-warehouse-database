﻿CREATE TABLE [dbo].[EROL_Measurement](
	[EROL_MeasurementID] [bigint] NOT NULL,
	[ErolID] [bigint] NOT NULL,
	[ValueTypeID] [bigint] NOT NULL,
	[PerformedById] [bigint] NOT NULL,
	[NoteID] [bigint] NULL,
	[DateTime] [datetime] NOT NULL,
	[Value] [varchar](50) NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[EK_Record] [bit] NULL,
	[Deleted] [bit] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
