﻿CREATE TABLE [dbo].[EROL_Medication](
	[EROL_MedicationID] [bigint] NOT NULL,
	[RouteID] [bigint] NOT NULL,
	[NoteID] [bigint] NULL,
	[PerformedByID] [bigint] NOT NULL,
	[TimeGiven] [datetime] NOT NULL,
	[Drug] [varchar](250) NOT NULL,
	[Dose] [varchar](50) NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[EK_Record] [bit] NULL,
	[Deleted] [bit] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[ErolID] [bigint] NULL,
	[PainRelief] [bit] NOT NULL
)
