﻿CREATE TABLE [dbo].[EROL_MedicationRoute](
	[EROL_MedicationRouteID] [bigint] NOT NULL,
	[RouteName] [nchar](50) NOT NULL,
	[Description] [nchar](250) NOT NULL,
	[Deleted] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[EK_Record] [bit] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
