﻿CREATE TABLE [dbo].[EROL_MedicationTextual](
	[EROL_MedicationTextualID] [bigint] NOT NULL,
	[PerformedByID] [bigint] NOT NULL,
	[TimeGiven] [datetime] NOT NULL,
	[Subject] [varchar](250) NOT NULL,
	[NoteID] [bigint] NOT NULL,
	[Urgent] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[EK_Record] [bit] NULL,
	[Deleted] [bit] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[ErolID] [bigint] NULL
)
