﻿CREATE TABLE [dbo].[EROL_Note](
	[EROL_NoteID] [bigint] NOT NULL,
	[Note] [varchar](500) NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[EK_Record] [bit] NULL,
	[Deleted] [bit] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
