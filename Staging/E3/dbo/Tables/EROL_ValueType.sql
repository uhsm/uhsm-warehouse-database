﻿CREATE TABLE [dbo].[EROL_ValueType](
	[EROL_ValueTypeID] [bigint] NOT NULL,
	[MeasurementName] [varchar](150) NOT NULL,
	[MeasurementLabel] [varchar](150) NOT NULL,
	[InternalName] [varchar](50) NULL,
	[ValidationMin] [varchar](50) NOT NULL,
	[ValidationMax] [varchar](50) NOT NULL,
	[MeowYellowMin] [varchar](50) NOT NULL,
	[MeowYellowMax] [varchar](50) NOT NULL,
	[MeowRedMin] [varchar](50) NOT NULL,
	[MeowRedMax] [varchar](50) NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[EK_Record] [bit] NULL,
	[Deleted] [bit] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
