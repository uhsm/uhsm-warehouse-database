﻿CREATE TABLE [dbo].[Element](
	[ElementID] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[DisplayName] [nvarchar](50) NULL,
	[Description] [nvarchar](50) NULL,
	[UpdateName] [nvarchar](50) NULL,
	[Type] [nvarchar](50) NULL,
	[DSOLevel] [nvarchar](50) NULL,
	[IconFile] [nvarchar](255) NULL,
	[IconFileCompleted] [nvarchar](255) NULL,
	[IconFileProgress] [nvarchar](255) NULL,
	[ParentElementID] [int] NULL,
	[CarePlanId] [int] NULL,
	[QuestionnaireID] [int] NULL,
	[TableName] [nvarchar](50) NULL,
	[ReportGroupID] [int] NULL,
	[FormName] [nvarchar](50) NULL,
	[NeonatalModule] [bit] NULL,
	[Sequence] [int] NULL,
	[HideOnStartup] [bit] NULL,
	[LoadWithParent] [bit] NULL,
	[IsFolder] [bit] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[IsRootNode] [bit] NULL,
	[CreateNewRecord] [bit] NULL,
	[AllowMultipleInstances] [bit] NULL,
	[MultipleInstanceUpperLimit] [int] NULL,
	[ElementType] [nvarchar](30) NULL
)
