﻿CREATE TABLE [dbo].[Event](
	[EventID] [bigint] NOT NULL,
	[PregnancyContactID] [bigint] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
