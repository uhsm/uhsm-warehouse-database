﻿CREATE TABLE [dbo].[FormLevel](
	[FormLevelID] [int] NOT NULL,
	[FormLevel] [nvarchar](20) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
