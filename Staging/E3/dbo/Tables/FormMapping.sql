﻿CREATE TABLE [dbo].[FormMapping](
	[FormMappingID] [int] NOT NULL,
	[UserControl] [nvarchar](50) NULL,
	[ControlID] [nvarchar](255) NULL,
	[ControlName] [nvarchar](50) NULL,
	[QuestionnaireId] [int] NULL,
	[QuestionID] [int] NULL,
	[UseNumerics] [bit] NULL,
	[StartNumber] [int] NULL,
	[EndNumber] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[BaseTable] [nvarchar](50) NULL
)
