﻿CREATE TABLE [dbo].[GPIndex](
	[GPIndexID] [int] NOT NULL,
	[PASCode] [nvarchar](50) NULL,
	[GPID] [int] NOT NULL,
	[GPPracticeID] [int] NOT NULL,
	[PracticeAddressID] [int] NOT NULL,
	[Deleted] [bit] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
