﻿CREATE TABLE [dbo].[GPPractice](
	[GPPracticeID] [int] NOT NULL,
	[PracticeCode] [nvarchar](30) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[PCTID] [int] NULL
)
