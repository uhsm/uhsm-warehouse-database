﻿CREATE TABLE [dbo].[Gender](
	[GenderID] [int] NOT NULL,
	[Gender] [nvarchar](50) NULL,
	[PASCode] [varchar](30) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
