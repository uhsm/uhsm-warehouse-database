﻿CREATE TABLE [dbo].[GuidelineAnswer](
	[GuidelineAnswerId] [int] NOT NULL,
	[GuidelineDetailID] [int] NULL,
	[AnswerId] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
