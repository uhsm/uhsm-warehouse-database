﻿CREATE TABLE [dbo].[GuidelineGroup](
	[GuidelineGroupId] [int] NOT NULL,
	[GuidelineGroup] [nvarchar](255) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
