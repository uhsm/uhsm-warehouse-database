﻿CREATE TABLE [dbo].[GuidelinesForPatient](
	[GuidelineUsed] [int] NOT NULL,
	[CareplanId] [int] NULL,
	[PregnancyId] [bigint] NULL,
	[BabyId] [bigint] NULL,
	[QuestionId] [int] NULL,
	[AnswerId] [int] NULL,
	[GuidelineDetailId] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
