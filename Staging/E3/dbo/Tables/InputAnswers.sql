﻿CREATE TABLE [dbo].[InputAnswers](
	[FieldNameID] [int] NOT NULL,
	[InstanceID] [int] NOT NULL,
	[AnswerText] [nvarchar](1000) NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
