﻿CREATE TABLE [dbo].[InputDetails](
	[InputDetailsID] [bigint] NOT NULL,
	[InputID] [bigint] NULL,
	[IsProblem] [bit] NULL,
	[IsCurrent] [bit] NULL,
	[StartDate] [datetime] NULL,
	[PersonID] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
