﻿CREATE TABLE [dbo].[InterfaceLog](
	[InterfaceLogID] [bigint] NOT NULL,
	[MessageReceived] [datetime] NULL,
	[MessageType] [nvarchar](10) NULL,
	[ErrorMessage] [nvarchar](max) NULL,
	[XMLMessage] [nvarchar](max) NULL,
	[NHSNumber] [nvarchar](20) NULL,
	[HospitalNumber] [nvarchar](20) NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[Action] [nvarchar](50) NULL
)
