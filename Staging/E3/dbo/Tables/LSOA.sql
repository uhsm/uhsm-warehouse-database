﻿CREATE TABLE [dbo].[LSOA](
	[LSOAID] [int] NOT NULL,
	[LSOA] [nvarchar](50) NULL,
	[LastUpdated] [smalldatetime] NULL,
	[Deleted] [bit] NULL,
	[UserID] [int] NULL
)
