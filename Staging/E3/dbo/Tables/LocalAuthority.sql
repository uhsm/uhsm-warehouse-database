﻿CREATE TABLE [dbo].[LocalAuthority](
	[LocalAuthorityID] [int] NOT NULL,
	[LocalAuthority] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
