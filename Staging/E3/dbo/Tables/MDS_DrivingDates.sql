﻿CREATE TABLE [dbo].[MDS_DrivingDates](
	[MDSSegmentCode] [varchar](6) NULL,
	[MDSSegmentFieldBatch] [int] NULL,
	[DrivenBy_E3QuestionnaireID] [int] NULL,
	[DrivenBy_E3Field] [varchar](100) NULL
)
