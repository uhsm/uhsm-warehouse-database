﻿CREATE TABLE [dbo].[MDS_FileValidationResult](
	[ReportIdentifier] [int] NULL,
	[Success] [bit] NULL,
	[ResultMessage] [varchar](max) NULL,
	[FileName] [varchar](max) NULL
)
