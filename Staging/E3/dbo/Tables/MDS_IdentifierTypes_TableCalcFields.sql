﻿CREATE TABLE [dbo].[MDS_IdentifierTypes_TableCalcFields](
	[E3SourceType] [varchar](50) NULL,
	[E3SourceName] [varchar](50) NULL,
	[E3QuestionnaireID] [int] NULL,
	[E3Field] [varchar](50) NULL,
	[CalculationDescription] [varchar](255) NULL,
	[IdentifierType] [varchar](50) NULL
)
