﻿CREATE TABLE [dbo].[MDS_Segments](
	[ID] [int] NOT NULL,
	[MDSSegmentCode] [varchar](6) NULL,
	[MDSSegmentFieldBatch] [int] NULL,
	[MDSSegmentXMLLabel] [varchar](50) NULL,
	[MDSSegmentDescription] [varchar](255) NULL,
	[MDSSegmentSort] [int] NULL,
	[MDSSegmentNesting] [int] NULL,
	[GroupSectionLabel] [varchar](255) NULL,
	[GroupSectionSort] [int] NULL,
	[MDSSegmentCanRepeat] [bit] NULL,
	[GroupingIdentifier] [varchar](50) NULL
)
