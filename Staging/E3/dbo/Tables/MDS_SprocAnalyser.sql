﻿CREATE TABLE [dbo].[MDS_SprocAnalyser](
	[ReportIdentifier] [int] NULL,
	[DateTimeStamp] [datetime] NULL,
	[SprocName] [varchar](255) NULL,
	[Element] [varchar](255) NULL,
	[CompletedDT] [datetime] NULL,
	[Duration] [int] NULL
)
