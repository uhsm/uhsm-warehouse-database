﻿CREATE TABLE [dbo].[MaritalStatus](
	[MaritalStatusID] [int] NOT NULL,
	[MaritalStatus] [varchar](50) NULL,
	[PASCode] [varchar](30) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
