﻿CREATE TABLE [dbo].[Memo](
	[MemoID] [bigint] NOT NULL,
	[Memo] [nvarchar](1000) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
