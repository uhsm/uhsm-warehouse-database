﻿CREATE TABLE [dbo].[Message](
	[MessageID] [int] NOT NULL,
	[Message] [nvarchar](500) NULL,
	[Important] [nchar](10) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
