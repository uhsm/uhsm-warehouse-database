﻿CREATE TABLE [dbo].[MessageSend](
	[MessageID] [bigint] NOT NULL,
	[DateSent] [datetime] NULL,
	[Deleted] [bit] NULL,
	[Important] [bit] NULL,
	[Messageread] [nvarchar](500) NULL,
	[Receiver] [nvarchar](50) NULL,
	[Sender] [nvarchar](50) NULL,
	[Text] [nvarchar](2000) NULL,
	[Title] [nvarchar](200) NULL,
	[EK_Record] [bit] NULL,
	[ReadStatus] [bit] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[GUID] [uniqueidentifier] NULL
)
