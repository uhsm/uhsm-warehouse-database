﻿CREATE TABLE [dbo].[Module](
	[ModuleID] [bigint] NOT NULL,
	[Module] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[UserID] [int] NULL,
	[GUID] [uniqueidentifier] NULL,
	[LastUpdated] [datetime] NULL
)
