﻿CREATE TABLE [dbo].[MultipleNumber](
	[MultipleNumberID] [bigint] NOT NULL,
	[MultipleNumber] [nvarchar](50) NULL,
	[PatientID] [bigint] NULL,
	[Deleted] [bit] NOT NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[IsSystemNumber] [bit] NULL
)
