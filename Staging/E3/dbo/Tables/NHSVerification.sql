﻿CREATE TABLE [dbo].[NHSVerification](
	[NHSVerificationID] [int] NOT NULL,
	[TracingStatusCode] [nvarchar](20) NULL,
	[TracingStatus] [nvarchar](150) NULL,
	[VerificationID] [int] NULL,
	[Verified] [bit] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[LastUpdated] [datetime] NULL,
	[UserID] [int] NULL
)
