﻿CREATE TABLE [dbo].[NeighbourhoodCentre](
	[NeighbourhoodCentreID] [int] NOT NULL,
	[NeighbourhoodCentre] [nvarchar](20) NULL,
	[Deleted] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[UserID] [int] NULL
)
