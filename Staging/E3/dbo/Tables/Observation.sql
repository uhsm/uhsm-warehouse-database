﻿CREATE TABLE [dbo].[Observation](
	[ObservationID] [int] NOT NULL,
	[PODID] [int] NULL,
	[ObservationTypeID] [int] NULL,
	[SendForResult] [bit] NULL,
	[ObservationValueTypeID] [int] NULL,
	[Code] [nvarchar](50) NULL,
	[MaxValue] [float] NULL,
	[MinValue] [float] NULL,
	[WarnHigh] [int] NULL,
	[WarnLow] [int] NULL,
	[Conversion] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
