﻿CREATE TABLE [dbo].[Outcome](
	[OutcomeID] [int] NOT NULL,
	[Outcome] [varchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
