﻿CREATE TABLE [dbo].[PASLoadError](
	[HospitalNumber] [varchar](250) NULL,
	[System] [varchar](250) NULL,
	[Title] [varchar](250) NULL,
	[Ethnicgroup] [varchar](250) NULL,
	[MaritalStatus] [varchar](250) NULL,
	[Religion] [varchar](250) NULL,
	[GPNational] [varchar](250) NULL,
	[GPLocal] [varchar](250) NULL,
	[DOB_Duff] [varchar](250) NULL,
	[AddLine1Size] [varchar](250) NULL,
	[AddLine2Size] [varchar](250) NULL,
	[AddLine3Size] [varchar](250) NULL,
	[AddLine4Size] [varchar](250) NULL
)
