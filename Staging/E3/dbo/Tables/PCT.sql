﻿CREATE TABLE [dbo].[PCT](
	[PCTID] [int] NOT NULL,
	[PCT] [varchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
