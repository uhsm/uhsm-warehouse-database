﻿CREATE TABLE [dbo].[PODGroup](
	[PODGroupID] [int] NOT NULL,
	[PODGroup] [nvarchar](50) NULL,
	[PODTypeID] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
