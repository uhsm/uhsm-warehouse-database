﻿CREATE TABLE [dbo].[PODMap](
	[PODMapID] [int] NOT NULL,
	[PODGroupID] [int] NULL,
	[PODID] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
