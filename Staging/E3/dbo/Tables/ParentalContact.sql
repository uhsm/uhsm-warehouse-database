﻿CREATE TABLE [dbo].[ParentalContact](
	[ParentalContactID] [bigint] NOT NULL,
	[ParentalContact] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
