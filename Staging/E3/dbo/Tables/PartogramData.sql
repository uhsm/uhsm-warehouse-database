﻿CREATE TABLE [dbo].[PartogramData](
	[DataId] [bigint] NOT NULL,
	[PartogramId] [bigint] NULL,
	[DateTime] [datetime] NULL,
	[ClientSourceGUID] [uniqueidentifier] NULL,
	[DataSource] [nvarchar](max) NULL,
	[PanelName] [nvarchar](50) NULL,
	[Value] [nchar](10) NULL,
	[Value2] [nchar](10) NULL,
	[Freehand] [varbinary](max) NULL,
	[BabyNum] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserId] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[Alerted] [bit] NULL
)
