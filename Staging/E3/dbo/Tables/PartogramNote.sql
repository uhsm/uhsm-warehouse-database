﻿CREATE TABLE [dbo].[PartogramNote](
	[NoteID] [bigint] NOT NULL,
	[PartogramID] [bigint] NULL,
	[NoteValue] [nvarchar](50) NULL,
	[NoteValue2] [nvarchar](50) NULL,
	[NoteDateTime] [datetime] NULL,
	[clientSourceID] [uniqueidentifier] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[dataGUID] [uniqueidentifier] NULL
)
