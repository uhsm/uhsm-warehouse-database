﻿CREATE TABLE [dbo].[PartogramValidation](
	[FieldName] [varchar](50) NULL,
	[MaxWarn] [float] NULL,
	[MaxLimit] [float] NULL,
	[MinWarn] [float] NULL,
	[MinLimit] [float] NULL
)
