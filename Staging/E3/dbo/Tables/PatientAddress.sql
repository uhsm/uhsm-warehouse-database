﻿CREATE TABLE [dbo].[PatientAddress](
	[PatientAddressID] [bigint] NOT NULL,
	[AddressTypeID] [int] NULL,
	[PatientID] [bigint] NULL,
	[BabyID] [bigint] NULL,
	[AddressID] [bigint] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
