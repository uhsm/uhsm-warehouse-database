﻿CREATE TABLE [dbo].[PatientLock](
	[PatientLockID] [int] NOT NULL,
	[PatientID] [int] NULL,
	[LockDate] [datetime] NULL,
	[LockedByUserID] [int] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[LockedByMachine] [nvarchar](50) NULL
)
