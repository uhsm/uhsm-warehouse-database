﻿CREATE TABLE [dbo].[PatientMessage](
	[PatientMessageID] [bigint] NOT NULL,
	[PatientID] [bigint] NULL,
	[Message] [text] NULL,
	[Falg] [bit] NULL,
	[PatientMessageTypeID] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
