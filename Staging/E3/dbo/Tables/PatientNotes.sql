﻿CREATE TABLE [dbo].[PatientNotes](
	[PatientNotesId] [int] NOT NULL,
	[PatientId] [bigint] NULL,
	[MemoId] [bigint] NULL,
	[IsConfidential] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[UserID] [int] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[Deleted] [bit] NULL,
	[PatientNotes] [nvarchar](max) NULL,
	[DeletedByUserId] [int] NULL,
	[DeletedOnDate] [datetime] NULL
)
