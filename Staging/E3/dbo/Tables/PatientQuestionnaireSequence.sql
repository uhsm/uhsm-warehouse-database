﻿CREATE TABLE [dbo].[PatientQuestionnaireSequence](
	[PatientQuestionnaireSequenceID] [int] NOT NULL,
	[QuestionID] [int] NULL,
	[PregnancyID] [bigint] NULL,
	[BabyId] [bigint] NULL,
	[Saved] [bit] NULL,
	[Cleared] [bit] NULL,
	[IfElseBranchName] [nvarchar](50) NULL,
	[DSOLevel] [nvarchar](50) NULL,
	[DSOLevelID] [bigint] NULL,
	[Deleted] [bit] NOT NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
