﻿CREATE TABLE [dbo].[PbRConfig](
	[ID] [uniqueidentifier] NOT NULL,
	[Category] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Answer] [nvarchar](max) NOT NULL,
	[Filter] [nchar](10) NOT NULL,
	[Fieldname] [nvarchar](max) NOT NULL
)
