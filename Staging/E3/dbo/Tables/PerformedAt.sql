﻿CREATE TABLE [dbo].[PerformedAt](
	[PerformedAtID] [int] NOT NULL,
	[PerformedAt] [varchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
