﻿CREATE TABLE [dbo].[PersonContact](
	[PersonContactID] [int] NOT NULL,
	[PersonID] [int] NULL,
	[EMail] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
	[HomeTelephone] [nvarchar](50) NULL,
	[MobileTelephone] [nvarchar](50) NULL,
	[WorkTelephone] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
