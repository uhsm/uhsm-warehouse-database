﻿CREATE TABLE [dbo].[PersonType](
	[PersonTypeID] [int] NOT NULL,
	[PersonType] [varchar](50) NULL,
	[IsEndUser] [bit] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
