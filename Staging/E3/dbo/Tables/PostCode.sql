﻿CREATE TABLE [dbo].[PostCode](
	[PostCodeID] [int] NOT NULL,
	[PostCode] [nvarchar](10) NOT NULL,
	[ChildrenCentreID] [int] NULL,
	[PCTID] [int] NULL,
	[Deleted] [bit] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[LSOAID] [int] NULL,
	[UserID] [int] NULL,
	[NeighbourhoodCentreID] [int] NULL
)
