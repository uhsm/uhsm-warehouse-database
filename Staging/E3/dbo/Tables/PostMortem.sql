﻿CREATE TABLE [dbo].[PostMortem](
	[PostMortemID] [bigint] NOT NULL,
	[PostMortem] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
