﻿CREATE TABLE [dbo].[PregnancyAdmission](
	[PregnancyAdmissionID] [bigint] NOT NULL,
	[PregnancyID] [bigint] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
