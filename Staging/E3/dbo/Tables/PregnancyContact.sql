﻿CREATE TABLE [dbo].[PregnancyContact](
	[PregnancyContactID] [bigint] NOT NULL,
	[PregnancyID] [bigint] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[ContactTypeId] [int] NULL,
	[ParentPregnancyContactID] [bigint] NULL
)
