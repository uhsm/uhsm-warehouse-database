﻿CREATE TABLE [dbo].[ProgressMessages](
	[ProgressID] [int] NOT NULL,
	[ProgressMessage] [nvarchar](max) NULL,
	[BabyID] [int] NULL,
	[PregnancyID] [int] NULL,
	[MessageFor] [nvarchar](255) NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
