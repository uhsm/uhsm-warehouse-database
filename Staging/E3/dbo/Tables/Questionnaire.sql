﻿CREATE TABLE [dbo].[Questionnaire](
	[QuestionnaireID] [int] NOT NULL,
	[Questionnaire] [nvarchar](50) NULL,
	[PostProcess] [nvarchar](50) NULL,
	[ReportGroupID] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
