﻿CREATE TABLE [dbo].[QuestionnaireNote](
	[QuestionnaireNoteId] [bigint] NOT NULL,
	[PregnancyId] [bigint] NULL,
	[BabyId] [bigint] NULL,
	[PregnancyAdmissionId] [bigint] NULL,
	[BabyAdmissionId] [bigint] NULL,
	[PregnancyContactId] [bigint] NULL,
	[BabyContactsId] [int] NULL,
	[Name] [nvarchar](200) NOT NULL,
	[ConnectedTo] [nvarchar](200) NULL,
	[Notes] [nvarchar](max) NOT NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
