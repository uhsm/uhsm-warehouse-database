﻿CREATE TABLE [dbo].[QuestionnaireSequence](
	[QuestionnaireSequenceID] [int] NOT NULL,
	[QuestionnaireID] [int] NULL,
	[QuestionID] [int] NULL,
	[Sequence] [int] NULL,
	[IfName] [nvarchar](255) NULL,
	[ElseName] [nvarchar](255) NULL,
	[IfElseName] [nvarchar](255) NULL,
	[IfElseBranchName] [nvarchar](255) NULL,
	[ParentBranches] [nvarchar](1000) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
