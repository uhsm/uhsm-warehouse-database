﻿CREATE TABLE [dbo].[QuestionnaireSummary](
	[QuestionnaireSummaryID] [int] NOT NULL,
	[PregnancyId] [bigint] NULL,
	[BabyID] [bigint] NULL,
	[QuestionnaireID] [int] NULL,
	[QuestionID] [bigint] NULL,
	[InputID] [bigint] NULL,
	[IfElseBranchName] [nvarchar](255) NULL,
	[IfElseName] [nvarchar](255) NULL,
	[Visible] [bit] NULL,
	[Name] [nvarchar](255) NULL,
	[Sequence] [int] NULL,
	[InstanceID] [int] NULL,
	[DSOLEVEL] [nvarchar](50) NULL,
	[DSOLEVELID] [int] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
