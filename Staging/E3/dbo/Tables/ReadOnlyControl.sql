﻿CREATE TABLE [dbo].[ReadOnlyControl](
	[ReadOnlyControlID] [int] NOT NULL,
	[FormName] [nvarchar](255) NULL,
	[ControlName] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[Enabled] [bit] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
