﻿CREATE TABLE [dbo].[Report](
	[ReportID] [int] NOT NULL,
	[FileName] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[PopulateSQL] [nvarchar](2000) NULL,
	[PrintCopies] [nvarchar](2000) NULL,
	[Requestable] [bit] NULL,
	[WordDocument] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
