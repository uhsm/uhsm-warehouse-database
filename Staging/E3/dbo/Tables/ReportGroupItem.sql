﻿CREATE TABLE [dbo].[ReportGroupItem](
	[ReportGroupItemID] [int] NOT NULL,
	[ReportGroupID] [int] NULL,
	[ReportID] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
