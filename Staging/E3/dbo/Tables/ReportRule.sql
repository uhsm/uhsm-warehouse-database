﻿CREATE TABLE [dbo].[ReportRule](
	[ReportRuleID] [int] NOT NULL,
	[ReportID] [int] NULL,
	[RulesID] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
