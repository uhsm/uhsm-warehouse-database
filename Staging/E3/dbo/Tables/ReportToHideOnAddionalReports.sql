﻿CREATE TABLE [dbo].[ReportToHideOnAddionalReports](
	[ReportToHideOnAddionalReportsID] [bigint] NOT NULL,
	[ReportPath] [nvarchar](500) NULL,
	[Careplan] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[LastUpdated] [datetime] NULL,
	[UserID] [int] NULL
)
