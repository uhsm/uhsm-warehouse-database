﻿CREATE TABLE [dbo].[ReportToQuestionnaire](
	[ReportName] [nvarchar](200) NOT NULL,
	[CarePlanId] [int] NOT NULL,
	[ElementId] [int] NOT NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
