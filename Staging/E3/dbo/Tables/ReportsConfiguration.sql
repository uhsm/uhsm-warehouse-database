﻿CREATE TABLE [dbo].[ReportsConfiguration](
	[TitleStyle] [nvarchar](50) NULL,
	[TitleFont] [nvarchar](50) NULL,
	[TitleSize] [nvarchar](50) NULL,
	[TitleWeight] [nvarchar](50) NULL,
	[TitleAlign] [nvarchar](50) NULL,
	[ConfigurationID] [bigint] NOT NULL,
	[ConfigurationName] [char](10) NULL,
	[HeaderRowStyle] [nvarchar](50) NULL,
	[HeaderRowFont] [varchar](50) NULL,
	[HeaderRowSize] [nvarchar](50) NULL,
	[HeaderRowWeight] [nvarchar](50) NULL,
	[HeaderRowAlign] [nvarchar](50) NULL,
	[FooterRowStyle] [nvarchar](50) NULL,
	[FooterRowFont] [nvarchar](50) NULL,
	[FooterRowSize] [nvarchar](50) NULL,
	[FooterRowWeight] [nvarchar](50) NULL,
	[FooterRowAlign] [nvarchar](50) NULL,
	[GroupRowStyle] [nvarchar](50) NULL,
	[GroupRowFont] [nvarchar](50) NULL,
	[GroupRowSize] [nvarchar](50) NULL,
	[GroupRowWeight] [nvarchar](50) NULL,
	[GroupRowAlign] [nvarchar](50) NULL,
	[AlternateRows] [tinyint] NULL,
	[AlternateRowColour] [nvarchar](50) NULL,
	[FooterStyle] [nvarchar](50) NULL,
	[FooterFont] [nvarchar](50) NULL,
	[FooterSize] [nvarchar](50) NULL,
	[FooterWeight] [nvarchar](50) NULL,
	[FooterAlign] [nvarchar](50) NULL,
	[TableBorder] [tinyint] NULL,
	[HeaderRowColour] [nvarchar](50) NULL,
	[FooterRowColour] [nvarchar](50) NULL,
	[GroupRowColour] [nvarchar](50) NULL
)
