﻿CREATE TABLE [dbo].[ResetQuestions](
	[ResetQuestonsId] [int] NOT NULL,
	[QuestionId] [int] NULL,
	[dsolevel] [nvarchar](100) NULL,
	[dsolevelid] [nvarchar](20) NULL,
	[deleted] [bit] NULL,
	[LastUpdated] [datetime] NULL
)
