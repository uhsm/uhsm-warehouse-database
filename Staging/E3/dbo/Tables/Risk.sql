﻿CREATE TABLE [dbo].[Risk](
	[RiskID] [int] NOT NULL,
	[Risk] [nvarchar](300) NULL,
	[PregnancyID] [bigint] NULL,
	[BabyID] [bigint] NULL,
	[CarePlanID] [int] NULL,
	[PODID] [int] NULL,
	[RiskTypeID] [int] NULL,
	[Deleted] [bit] NOT NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[LastUpdated] [datetime] NULL,
	[UserID] [int] NULL,
	[ExtraDetails] [nvarchar](500) NULL,
	[QuestionId] [int] NULL,
	[IsCopiedAcrossFromMother] [bit] NULL
)
