﻿CREATE TABLE [dbo].[RiskType](
	[RiskTypeID] [int] NOT NULL,
	[RiskType] [nvarchar](50) NULL,
	[EK_Record] [bit] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[Deleted] [bit] NULL
)
