﻿CREATE TABLE [dbo].[RoleTree](
	[RoleTreeID] [int] NOT NULL,
	[RoleTree] [nvarchar](255) NULL,
	[RoleColumnName] [nvarchar](255) NULL,
	[ParentRoleTreeID] [int] NULL,
	[Sequence] [int] NULL,
	[Deleted] [bit] NULL
)
