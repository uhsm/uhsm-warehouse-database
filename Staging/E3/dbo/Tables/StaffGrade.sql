﻿CREATE TABLE [dbo].[StaffGrade](
	[StaffGradeID] [int] NOT NULL,
	[StaffGrade] [varchar](50) NULL,
	[PersonTypeID] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
