﻿CREATE TABLE [dbo].[Statistics](
	[StatisticsId] [int] NOT NULL,
	[Statistics] [nvarchar](500) NULL,
	[GroupBy] [nvarchar](500) NULL,
	[Deleted] [bit] NULL,
	[ParentStatisticsId] [int] NULL,
	[IsUser] [bit] NULL,
	[IsEnquiry] [bit] NULL,
	[IsStatistic] [bit] NULL,
	[EK_Record] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[UserID] [int] NULL,
	[NoShow] [bit] NULL,
	[SQLQuery] [nvarchar](max) NULL
)
