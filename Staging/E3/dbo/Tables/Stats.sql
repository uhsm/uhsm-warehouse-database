﻿CREATE TABLE [dbo].[Stats](
	[StatsID] [bigint] NOT NULL,
	[CacheClass] [nvarchar](50) NULL,
	[Description] [nvarchar](50) NULL,
	[StatsGroupID] [int] NULL,
	[Nr] [int] NULL,
	[StatsDetailClass] [nvarchar](50) NULL,
	[StatsDetailTitle] [nvarchar](50) NULL,
	[StatsDetailTotal] [nvarchar](50) NULL,
	[onChartClickReturnField] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
