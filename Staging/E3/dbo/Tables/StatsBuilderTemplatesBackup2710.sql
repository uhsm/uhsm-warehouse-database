﻿CREATE TABLE [dbo].[StatsBuilderTemplatesBackup2710](
	[StatsBuilderTemplatesID] [int] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[CustomTablename] [nvarchar](255) NULL,
	[StatsTemplateXML] [nvarchar](max) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[MainTreeNode] [bit] NULL,
	[QuestionnaireID] [int] NULL
)
