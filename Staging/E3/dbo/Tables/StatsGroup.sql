﻿CREATE TABLE [dbo].[StatsGroup](
	[StatsGroupID] [bigint] NOT NULL,
	[Description] [nvarchar](50) NULL,
	[StatsMainID] [int] NULL,
	[Nr] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
