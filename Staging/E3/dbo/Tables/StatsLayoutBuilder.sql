﻿CREATE TABLE [dbo].[StatsLayoutBuilder](
	[StatsLayoutBuilderId] [int] NOT NULL,
	[LayoutXML] [nvarchar](max) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL
)
