﻿CREATE TABLE [dbo].[StatsMain](
	[StatsMainID] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[XMLFilename] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
