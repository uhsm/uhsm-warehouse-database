﻿CREATE TABLE [dbo].[SummarySequence](
	[SummarySequenceID] [int] NULL,
	[QuestionnaireID] [bigint] NULL,
	[ActivityName] [nvarchar](200) NULL,
	[QuestionID] [nvarchar](50) NULL,
	[PositionID] [int] NULL,
	[IsRootQuestion] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[UniqueNumber] [int] NOT NULL
)
