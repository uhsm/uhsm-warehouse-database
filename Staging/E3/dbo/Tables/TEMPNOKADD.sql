﻿CREATE TABLE [dbo].[TEMPNOKADD](
	[NOKID] [bigint] NOT NULL,
	[PASID] [int] NULL,
	[NOKAddressLine1] [varchar](250) NULL,
	[NOKAddressLine2] [varchar](250) NULL,
	[NOKAddressLine3] [varchar](250) NULL,
	[NOKAddressLine4] [varchar](250) NULL,
	[NOKPostCode] [varchar](250) NULL,
	[NOKTelephoneNumber] [varchar](250) NULL
)
