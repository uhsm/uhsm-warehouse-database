﻿CREATE TABLE [dbo].[TempSystemList](
	[SystemListID] [varchar](250) NULL,
	[Code] [varchar](250) NULL,
	[Deleted] [varchar](250) NULL,
	[Name] [varchar](250) NULL,
	[Sequence] [varchar](250) NULL,
	[SystemListCode] [varchar](250) NULL
)
