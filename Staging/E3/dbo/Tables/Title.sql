﻿CREATE TABLE [dbo].[Title](
	[TitleID] [int] NOT NULL,
	[Title] [varchar](50) NULL,
	[PASCode] [varchar](30) NULL,
	[Mother] [bit] NULL,
	[Partner] [bit] NULL,
	[NOK] [bit] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
