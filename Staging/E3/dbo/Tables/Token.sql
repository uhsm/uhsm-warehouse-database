﻿CREATE TABLE [dbo].[Token](
	[TokenID] [bigint] NOT NULL,
	[Token] [nvarchar](50) NULL,
	[field] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
