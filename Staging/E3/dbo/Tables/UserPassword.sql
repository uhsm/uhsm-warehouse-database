﻿CREATE TABLE [dbo].[UserPassword](
	[UserPasswordID] [bigint] NOT NULL,
	[Password] [nvarchar](50) NULL,
	[Expiry] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
