﻿CREATE TABLE [dbo].[UserPatient](
	[UserPatientID] [int] NOT NULL,
	[UsersID] [int] NULL,
	[PatientID] [bigint] NULL,
	[UpdatedBy] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
