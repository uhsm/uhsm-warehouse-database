﻿CREATE TABLE [dbo].[UserRole](
	[UserRoleID] [int] NOT NULL,
	[UsersID] [int] NULL,
	[RolesID] [int] NULL,
	[IsDefault] [bit] NULL,
	[UpdatedBy] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
