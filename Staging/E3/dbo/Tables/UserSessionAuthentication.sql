﻿CREATE TABLE [dbo].[UserSessionAuthentication](
	[UserSessionAuthenticationID] [bigint] NOT NULL,
	[UserID] [int] NOT NULL,
	[WindowsIdentity] [nvarchar](50) NOT NULL,
	[Authenticated] [char](10) NOT NULL,
	[SessionKey] [nvarchar](50) NULL,
	[RightsMask] [nvarchar](300) NULL,
	[LicenseMask] [nvarchar](50) NULL,
	[LoginDateTime] [datetime] NULL,
	[LastActivityDateTime] [datetime] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[Machinename] [nvarchar](50) NULL
)
