﻿CREATE TABLE [dbo].[ValidationType](
	[ValidationTypeID] [int] NOT NULL,
	[ValidationType] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
