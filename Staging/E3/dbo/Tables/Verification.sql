﻿CREATE TABLE [dbo].[Verification](
	[VerificationID] [int] NOT NULL,
	[Verification] [nvarchar](100) NULL,
	[VerificationStatusCode] [nvarchar](5) NULL,
	[Deleted] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[UserID] [int] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL
)
