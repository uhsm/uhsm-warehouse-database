﻿CREATE TABLE [dbo].[Workflow](
	[WorkflowID] [int] NOT NULL,
	[WorkflowName] [varchar](500) NOT NULL,
	[QuestionnaireID] [int] NULL,
	[XOMLFile] [nvarchar](max) NULL,
	[XOMLFileLastUpdated] [datetime] NULL,
	[RulesFile] [nvarchar](max) NULL,
	[RulesFileLastUpdated] [datetime] NULL,
	[AssemblyFile] [varbinary](max) NULL,
	[AssemblyFileLastUpdated] [datetime] NULL,
	[Version] [int] NOT NULL,
	[CheckedOutBy] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
