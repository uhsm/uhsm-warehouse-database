﻿CREATE TABLE [dbo].[XOMLQuestionnaire](
	[XOMLQuestionnaireID] [int] NOT NULL,
	[QuestionnaireID] [int] NULL,
	[XOMLFile] [xml] NULL,
	[DLLValue] [binary](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
