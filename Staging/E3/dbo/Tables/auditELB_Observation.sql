﻿CREATE TABLE [dbo].[auditELB_Observation](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](20) NULL,
	[ELB_ObservationID] [bigint] NULL,
	[PartogramID] [bigint] NULL,
	[ObservationDateTime] [datetime] NULL,
	[ObservationPodID] [int] NULL,
	[ObservationValue] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
