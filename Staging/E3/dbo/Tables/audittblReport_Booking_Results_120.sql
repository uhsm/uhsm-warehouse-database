﻿CREATE TABLE [dbo].[audittblReport_Booking_Results_120](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](1000) NULL,
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[BloodGroup_Value] [bigint] NULL,
	[BloodGroup] [nvarchar](1000) NULL,
	[AntenatalDiagnosis_Value] [bigint] NULL,
	[Antibodies_Value] [bigint] NULL,
	[ASBResult_Value] [bigint] NULL,
	[ChlamydiaScreenResult_Value] [bigint] NULL,
	[HaemoglobinopathyPartnerTest_Value] [bigint] NULL,
	[HaemoglobinopathyStatus_Value] [bigint] NULL,
	[HepatitisBstatus_Value] [bigint] NULL,
	[HepatitisBTreatment_Value] [bigint] NULL,
	[HepatitisBVaccine_Value] [bigint] NULL,
	[HIVStatus_Value] [bigint] NULL,
	[HPAInformedHepB_Value] [bigint] NULL,
	[PartnerTestAntibodies_Value] [bigint] NULL,
	[PostnatalDiagnosis_Value] [bigint] NULL,
	[RubellaStatus_Value] [bigint] NULL,
	[SexualHealthClinicReferral_Value] [bigint] NULL,
	[Syphylis_Value] [bigint] NULL,
	[AntenatalDiagnosis] [nvarchar](1000) NULL,
	[Antibodies] [nvarchar](1000) NULL,
	[ASBResult] [nvarchar](1000) NULL,
	[ChlamydiaScreenResult] [nvarchar](1000) NULL,
	[HaemoglobinopathyPartnerTest] [nvarchar](1000) NULL,
	[HaemoglobinopathyStatus] [nvarchar](1000) NULL,
	[HepatitisBstatus] [nvarchar](1000) NULL,
	[HepatitisBTreatment] [nvarchar](1000) NULL,
	[HepatitisBVaccine] [nvarchar](1000) NULL,
	[HIVStatus] [nvarchar](1000) NULL,
	[HPAInformedHepB] [nvarchar](1000) NULL,
	[PartnerTestAntibodies] [nvarchar](1000) NULL,
	[PostnatalDiagnosis] [nvarchar](1000) NULL,
	[ResultsAddedDT] [nvarchar](1000) NULL,
	[RubellaStatus] [nvarchar](1000) NULL,
	[SexualHealthClinicReferral] [nvarchar](1000) NULL,
	[Syphylis] [nvarchar](1000) NULL,
	[ResultsGiven] [nvarchar](1000) NULL,
	[ResultsGiven_Value] [bigint] NULL,
	[NeonatalHepatitisVaccination] [nvarchar](1000) NULL,
	[NeonatalHepatitisVaccination_Value] [bigint] NULL,
	[ScreeningResultsGiven] [nvarchar](1000) NULL,
	[HbBooking] [nvarchar](1000) NULL,
	[DownsScreenResult] [nvarchar](1000) NULL,
	[HbBooking_Value] [bigint] NULL,
	[DownsTestType] [nvarchar](1000) NULL,
	[DownsScreenResult_Value] [bigint] NULL,
	[DownsTestType_Value] [bigint] NULL,
	[HaemoglobinopathyPartnerResult] [nvarchar](1000) NULL,
	[HaemoglobinopathyPartnerResult_Value] [bigint] NULL,
	[DownsTestDate] [nvarchar](1000) NULL,
	[DownsScreeningRiskRatioResult] [nvarchar](1000) NULL,
	[DatingScanPerformedDate] [nvarchar](1000) NULL,
	[DatingScanAnomalyDiagnosed] [nvarchar](1000) NULL,
	[AnomalyScanResult] [nvarchar](1000) NULL,
	[DatingScanAnomalyDiagnosed_Value] [bigint] NULL,
	[AnomalyScanResult_Value] [bigint] NULL,
	[BabiesOnScan] [nvarchar](1000) NULL,
	[AnomalyScanbabyAffected] [nvarchar](1000) NULL,
	[BabiesOnScan_Value] [bigint] NULL,
	[AnomalyScanbabyAffected_Value] [bigint] NULL,
	[AnomalyScanResultFetus2] [nvarchar](1000) NULL,
	[AnomalyScanResultFetus3] [nvarchar](1000) NULL,
	[AnomalyScanResultFetus4] [nvarchar](1000) NULL,
	[AnomalyScanResultFetus5] [nvarchar](1000) NULL,
	[AnomalyScanResultFetus6] [nvarchar](1000) NULL,
	[AnomalyScanResultFetus2_Value] [bigint] NULL,
	[AnomalyScanResultFetus3_Value] [bigint] NULL,
	[AnomalyScanResultFetus4_Value] [bigint] NULL,
	[AnomalyScanResultFetus5_Value] [bigint] NULL,
	[AnomalyScanResultFetus6_Value] [bigint] NULL,
	[AnomalyScanDT] [nvarchar](1000) NULL,
	[PAPPAResult] [nvarchar](1000) NULL,
	[DownsTestResultDate] [nvarchar](1000) NULL,
	[PAPPAResult_Value] [bigint] NULL,
	[DownsTestWomanInformedDate] [nvarchar](1000) NULL,
	[DownsDateWomanInformed] [nvarchar](1000) NULL,
	[JointScreeningTestResult] [nvarchar](1000) NULL,
	[JointScreeningTestResult_Value] [bigint] NULL,
	[JointScreeningTestRiskRatio] [nvarchar](1000) NULL
)
