﻿CREATE TABLE [dbo].[audittblReport_Missed_Contact_Baby_118](
	[AuditDate] [datetime] NULL,
	[AuditAction] [nvarchar](1000) NULL,
	[UserID] [int] NULL,
	[BabyID] [bigint] NULL,
	[BabycontactsID] [bigint] NULL,
	[DNAAction_Value] [bigint] NULL,
	[DNAComments_Value] [bigint] NULL,
	[DNAReportedBy_Value] [bigint] NULL,
	[DNASeenAt_Value] [bigint] NULL,
	[DNAAction] [nvarchar](1000) NULL,
	[DNAComments] [nvarchar](1000) NULL,
	[DNADate] [nvarchar](1000) NULL,
	[DNAReportedBy] [nvarchar](1000) NULL,
	[DNASeenAt] [nvarchar](1000) NULL
)
