﻿CREATE TABLE [dbo].[debugRichard](
	[id] [int] NOT NULL,
	[date] [datetime] NULL,
	[instanceid] [int] NULL,
	[fieldnameid] [int] NULL,
	[tablenameid] [int] NULL,
	[memolist] [nvarchar](max) NULL,
	[answertextlist] [nvarchar](max) NULL,
	[answertypelist] [nvarchar](max) NULL,
	[questionid] [int] NULL,
	[podgrouplist] [nvarchar](max) NULL,
	[podlist] [nvarchar](max) NULL,
	[userid] [int] NULL,
	[answeridlist] [nvarchar](max) NULL
)
