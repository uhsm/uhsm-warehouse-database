﻿CREATE TABLE [dbo].[savedstatstablebackup0401](
	[StatsBuilderId] [int] NOT NULL,
	[StatsXML] [nvarchar](max) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[StatsSQL] [varchar](max) NULL
)
