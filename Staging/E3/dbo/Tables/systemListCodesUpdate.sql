﻿CREATE TABLE [dbo].[systemListCodesUpdate](
	[SystemListCodesID] [int] NOT NULL,
	[Code] [varchar](25) NULL,
	[Title] [varchar](50) NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
