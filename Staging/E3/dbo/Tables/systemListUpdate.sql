﻿CREATE TABLE [dbo].[systemListUpdate](
	[SystemListID] [int] NOT NULL,
	[Code] [varchar](25) NULL,
	[Name] [varchar](50) NULL,
	[Sequence] [int] NULL,
	[SystemListCode] [int] NULL,
	[Deleted] [bit] NULL,
	[EK_Record] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL
)
