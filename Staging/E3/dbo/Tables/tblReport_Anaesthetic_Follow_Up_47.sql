﻿CREATE TABLE [dbo].[tblReport_Anaesthetic_Follow_Up_47](
	[UserID] [int] NULL,
	[PregnancyID] [bigint] NULL,
	[PregnancycontactID] [bigint] NULL,
	[AnaesApptNext_Value] [bigint] NULL,
	[AnaesPNAppt_Value] [bigint] NULL,
	[FollowUpAnaesthetist_Value] [bigint] NULL,
	[FollowUpProcedure_Value] [bigint] NULL,
	[FurtherFollowUp_Value] [bigint] NULL,
	[PostOpSatisfaction_Value] [bigint] NULL,
	[AdditionalComments] [nvarchar](1000) NULL,
	[AnaesANApptReason] [nvarchar](1000) NULL,
	[AnaesApptNext] [nvarchar](1000) NULL,
	[AnaesPNAppt] [nvarchar](1000) NULL,
	[AnaesthesiaSatisfaction] [nvarchar](1000) NULL,
	[AnalgesiaSatisfaction] [nvarchar](1000) NULL,
	[BloodPatchEffective] [nvarchar](1000) NULL,
	[FollowUpAnaesthetist] [nvarchar](1000) NULL,
	[FollowUpComments] [nvarchar](1000) NULL,
	[FollowUpDT] [nvarchar](1000) NULL,
	[FollowUpProcedure] [nvarchar](1000) NULL,
	[FollowUpReason] [nvarchar](1000) NULL,
	[FollowUpType] [nvarchar](1000) NULL,
	[FurtherFollowUp] [nvarchar](1000) NULL,
	[GAProblem] [nvarchar](1000) NULL,
	[Headache] [nvarchar](1000) NULL,
	[HeadacheAction] [nvarchar](1000) NULL,
	[HeadacheLink] [nvarchar](1000) NULL,
	[HeadacheSeverity] [nvarchar](1000) NULL,
	[NeuroProblems] [nvarchar](1000) NULL,
	[NonMobilisationReason] [nvarchar](1000) NULL,
	[PostOpSatisfaction] [nvarchar](1000) NULL,
	[Retention] [nvarchar](1000) NULL,
	[SymptomLink] [nvarchar](1000) NULL,
	[Walked] [nvarchar](1000) NULL,
	[BloodPatchEffective_Value] [bigint] NULL,
	[NonMobilisationReason_Value] [bigint] NULL,
	[AnaesFollowUpOtherProblems] [nvarchar](1000) NULL,
	[AnaesFollowUpOtherProblems_Value] [bigint] NULL,
	[HeadacheLink_Value] [bigint] NULL,
	[FollowUpAdverseIncidents] [nvarchar](1000) NULL
)
