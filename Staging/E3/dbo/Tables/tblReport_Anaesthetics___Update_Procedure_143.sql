﻿CREATE TABLE [dbo].[tblReport_Anaesthetics___Update_Procedure_143](
	[UserID] [int] NULL,
	[PregnancyID] [bigint] NULL,
	[PregnancycontactID] [bigint] NULL,
	[AnaesUpdateDT] [nvarchar](1000) NULL,
	[RegionalProblem] [nvarchar](1000) NULL,
	[RegionalProblem_Value] [bigint] NULL,
	[AnaesLipidRescue] [nvarchar](1000) NULL,
	[AnaesLipidRescue_Value] [bigint] NULL,
	[AnaesUpdateComments] [nvarchar](1000) NULL
)
