﻿CREATE TABLE [dbo].[tblReport_Asymptomatic_Bacteriuria_Screen_133](
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[ASBTestDate] [nvarchar](1000) NULL,
	[ASBTestDate_Value] [bigint] NULL,
	[ASBResult] [nvarchar](1000) NULL
)
