﻿CREATE TABLE [dbo].[tblReport_Baby_Born_Outside_Trust_137](
	[UserID] [int] NULL,
	[PregnancyID] [bigint] NULL,
	[BabyID] [bigint] NULL,
	[BOTPlaceOfBirth] [nvarchar](255) NULL,
	[BBEGestation] [nvarchar](1000) NULL,
	[BBEPlaceOfBirth] [nvarchar](1000) NULL,
	[BBEDeliveryType] [nvarchar](1000) NULL,
	[BEEPrimaryIndicationCS] [nvarchar](1000) NULL,
	[BBEInstrumentalPrimary] [nvarchar](1000) NULL,
	[BBEApgarScore1Minute] [nvarchar](1000) NULL,
	[BBEApgarScore5Minutes] [nvarchar](1000) NULL,
	[BBEHeadCircumference] [nvarchar](1000) NULL,
	[BBEInitialFeedingType] [nvarchar](1000) NULL,
	[BBEVitaminK] [nvarchar](1000) NULL,
	[BBEOtherIssues] [nvarchar](1000) NULL,
	[BBEDeliveryType_Value] [bigint] NULL,
	[BBEInitialFeedingType_Value] [bigint] NULL,
	[BBEInstrumentalPrimary_Value] [bigint] NULL,
	[BBEOtherIssues_Value] [bigint] NULL,
	[BBEPlaceOfBirth_Value] [bigint] NULL,
	[BBEVitaminK_Value] [bigint] NULL,
	[BEEPrimaryIndicationCS_Value] [bigint] NULL,
	[BBEFurtherComments] [nvarchar](1000) NULL,
	[BBEFurtherComments_Value] [bigint] NULL,
	[BBEGender] [nvarchar](1000) NULL,
	[BBEGender_Value] [bigint] NULL,
	[BBEBirthDate] [nvarchar](1000) NULL,
	[BBEBirthweight] [nvarchar](1000) NULL
)
