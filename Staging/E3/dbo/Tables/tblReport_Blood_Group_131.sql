﻿CREATE TABLE [dbo].[tblReport_Blood_Group_131](
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[BloodGroupTestDate] [nvarchar](1000) NULL,
	[BloodGroup] [nvarchar](1000) NULL,
	[BloodGroupTestDate_Value] [bigint] NULL,
	[BloodGroup_Value] [bigint] NULL,
	[Antibodies] [nvarchar](1000) NULL,
	[Antibodies_Value] [bigint] NULL,
	[AntibodyTestPartner] [nvarchar](1000) NULL
)
