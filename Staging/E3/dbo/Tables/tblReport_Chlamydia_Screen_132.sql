﻿CREATE TABLE [dbo].[tblReport_Chlamydia_Screen_132](
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[ChlamydiaTestDate] [nvarchar](1000) NULL,
	[ChlamydiaTestDate_Value] [bigint] NULL,
	[ChlamydiaScreenResult] [nvarchar](1000) NULL,
	[ChlamydiaScreenResult_Value] [bigint] NULL,
	[ChlamydiaActionTaken] [nvarchar](1000) NULL
)
