﻿CREATE TABLE [dbo].[tblReport_Delivery_Satisfaction_35](
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[MothersExperience] [nvarchar](1000) NULL,
	[SatisfactionDelivery] [nvarchar](1000) NULL,
	[SatisfactionLabour] [nvarchar](1000) NULL
)
