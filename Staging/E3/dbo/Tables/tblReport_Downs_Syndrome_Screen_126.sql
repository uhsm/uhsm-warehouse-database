﻿CREATE TABLE [dbo].[tblReport_Downs_Syndrome_Screen_126](
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[DownsTestDate] [nvarchar](1000) NULL,
	[DownsTestDate_Value] [bigint] NULL,
	[DownsFurtherInformationRequired] [nvarchar](1000) NULL,
	[DownsFurtherInformationRequired_Value] [bigint] NULL,
	[DownsScreenResult] [nvarchar](1000) NULL,
	[DownsScreenResult_Value] [bigint] NULL,
	[DownsTestType] [nvarchar](1000) NULL,
	[DownsTestType_Value] [bigint] NULL,
	[DownsDiagnosticTest] [nvarchar](1000) NULL,
	[DownsDiagnosticTest_Value] [bigint] NULL,
	[DownsLaboratoryDate] [nvarchar](1000) NULL,
	[DownsDiagnosticTestConsented] [nvarchar](1000) NULL,
	[DownsDiagnosticTestConsented_Value] [bigint] NULL,
	[DownsTestResultDate] [nvarchar](1000) NULL
)
