﻿CREATE TABLE [dbo].[tblReport_Drs_Theatre_Procedure_136](
	[UserID] [int] NULL,
	[PregnancyID] [bigint] NULL,
	[PregnancyadmissionID] [bigint] NULL,
	[TheatreProcedure] [nvarchar](1000) NULL,
	[TheatreProcedure_Value] [bigint] NULL,
	[TPAnaesthesia] [nvarchar](1000) NULL,
	[TPCommencedDT] [nvarchar](1000) NULL,
	[TPArriveInTheatreDT] [nvarchar](1000) NULL,
	[TPIndicationsSuture] [nvarchar](1000) NULL,
	[TPIndicationsSuture_Value] [bigint] NULL,
	[TPTransVaginalUSS] [nvarchar](1000) NULL,
	[TPTransVaginalUSS_Value] [bigint] NULL,
	[TPComplications] [nvarchar](1000) NULL,
	[TPBloodLoss] [nvarchar](1000) NULL,
	[TPComplications_Value] [bigint] NULL,
	[TPProcedureCompleteDT] [nvarchar](1000) NULL,
	[TPSurgeon] [nvarchar](1000) NULL,
	[TPSurgeon_Value] [bigint] NULL,
	[TPAssistant] [nvarchar](1000) NULL,
	[TPAnaesthesia_Value] [bigint] NULL,
	[TPAssistant_Value] [bigint] NULL
)
