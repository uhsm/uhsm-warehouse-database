﻿CREATE TABLE [dbo].[tblReport_Fetal_Anomaly_Screen_127](
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[USSFetalAnomalyDate] [nvarchar](1000) NULL,
	[USSFetalAnomalyDate_Value] [bigint] NULL,
	[USSRepeatScan] [nvarchar](1000) NULL,
	[USSRepeatScan_Value] [bigint] NULL,
	[USSRepeatScanDate] [nvarchar](1000) NULL,
	[EDDScan] [nvarchar](1000) NULL
)
