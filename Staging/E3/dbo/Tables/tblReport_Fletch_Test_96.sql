﻿CREATE TABLE [dbo].[tblReport_Fletch_Test_96](
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[SmokerBeforePregnancy_Value] [bigint] NULL,
	[SmokerBeforePregnancy] [nvarchar](1000) NULL
)
