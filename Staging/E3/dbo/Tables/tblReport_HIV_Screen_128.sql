﻿CREATE TABLE [dbo].[tblReport_HIV_Screen_128](
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[HIVTestDate] [nvarchar](1000) NULL,
	[HIVResultDate] [nvarchar](1000) NULL,
	[HIVInformedDate] [nvarchar](1000) NULL,
	[HIVTestDate_Value] [bigint] NULL,
	[HIVPlanOfCare] [nvarchar](1000) NULL,
	[HIVPlanOfCare_Value] [bigint] NULL,
	[HIVConsentToShare] [nvarchar](1000) NULL,
	[HIVConsentToShare_Value] [bigint] NULL,
	[HIVReferrals] [nvarchar](1000) NULL,
	[HIVReferrals_Value] [bigint] NULL,
	[HIVSexualHealthDate] [nvarchar](1000) NULL,
	[HIVSexualHealthDate_Value] [bigint] NULL
)
