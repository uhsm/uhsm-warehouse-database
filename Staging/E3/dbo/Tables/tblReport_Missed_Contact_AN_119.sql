﻿CREATE TABLE [dbo].[tblReport_Missed_Contact_AN_119](
	[UserID] [int] NULL,
	[PregnancyID] [bigint] NULL,
	[PregnancycontactID] [bigint] NULL,
	[DNAContactAction_Value] [bigint] NULL,
	[DNAContactAT_Value] [bigint] NULL,
	[DNAContactDetails_Value] [bigint] NULL,
	[DNAContactReportBy_Value] [bigint] NULL,
	[DNAContactAction] [nvarchar](1000) NULL,
	[DNAContactAT] [nvarchar](1000) NULL,
	[DNAContactDetails] [nvarchar](1000) NULL,
	[DNAContactDT] [nvarchar](1000) NULL,
	[DNAContactReportBy] [nvarchar](1000) NULL,
	[DNAContactReasonAN] [nvarchar](1000) NULL
)
