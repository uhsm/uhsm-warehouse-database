﻿CREATE TABLE [dbo].[tblReport_Missed_Contact_Baby_16](
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[AlternativePainRelief] [nvarchar](1000) NULL,
	[Analgesia] [nvarchar](1000) NULL,
	[LabourDrugs] [nvarchar](1000) NULL,
	[LabourOnset] [nvarchar](1000) NULL
)
