﻿CREATE TABLE [dbo].[tblReport_Missed_Contact_PN_117](
	[UserID] [int] NULL,
	[PregnancyID] [bigint] NULL,
	[PregnancycontactID] [bigint] NULL,
	[PNDNAAction_Value] [bigint] NULL,
	[PNDNAComments_Value] [bigint] NULL,
	[PNDNAPlace_Value] [bigint] NULL,
	[PNMissedContactBy_Value] [bigint] NULL,
	[PNDNAAction] [nvarchar](1000) NULL,
	[PNDNAComments] [nvarchar](1000) NULL,
	[PNDNADateMissed] [nvarchar](1000) NULL,
	[PNDNAPlace] [nvarchar](1000) NULL,
	[PNMissedContactBy] [nvarchar](1000) NULL
)
