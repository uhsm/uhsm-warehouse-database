﻿CREATE TABLE [dbo].[tblReport_Not_Used_Baby_Details__BOT__16](
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[AlternativePainRelief] [nvarchar](1000) NULL,
	[Analgesia] [nvarchar](1000) NULL,
	[LabourDrugs] [nvarchar](1000) NULL,
	[LabourOnset] [nvarchar](1000) NULL
)
