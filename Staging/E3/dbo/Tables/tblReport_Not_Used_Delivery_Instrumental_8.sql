﻿CREATE TABLE [dbo].[tblReport_Not_Used_Delivery_Instrumental_8](
	[UserID] [int] NULL,
	[PregnancyID] [bigint] NULL,
	[BabyID] [bigint] NULL,
	[ForcepsFailedReason] [nvarchar](1000) NULL,
	[ForcepsType] [nvarchar](1000) NULL,
	[InstrumentalReasonOther] [nvarchar](1000) NULL,
	[InstrumentalReasonPrimary] [nvarchar](1000) NULL,
	[InstrumentalReasonSecondary] [nvarchar](1000) NULL,
	[VentouseCupType] [nvarchar](1000) NULL,
	[VentousePurpose] [nvarchar](1000) NULL
)
