﻿CREATE TABLE [dbo].[tblReport_Not_Used_Mother_Details__BOT__15](
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[DeathsAfter28Days_Value] [bigint] NULL,
	[Gravida_Value] [bigint] NULL,
	[Livebirths_Value] [bigint] NULL,
	[Miscarriages_Value] [bigint] NULL,
	[NeonatalDeaths_Value] [bigint] NULL,
	[Stillbirths_Value] [bigint] NULL,
	[Terminations_Value] [bigint] NULL,
	[DeathsAfter28Days] [nvarchar](1000) NULL,
	[Gravida] [nvarchar](1000) NULL,
	[Livebirths] [nvarchar](1000) NULL,
	[Miscarriages] [nvarchar](1000) NULL,
	[NeonatalDeaths] [nvarchar](1000) NULL,
	[PartnerEthnic] [nvarchar](1000) NULL,
	[Stillbirths] [nvarchar](1000) NULL,
	[Terminations] [nvarchar](1000) NULL,
	[ProblemsMedical_Value] [bigint] NULL,
	[ReasonForChangeAN_Value] [bigint] NULL,
	[Antibodies] [nvarchar](1000) NULL,
	[BetaDexamethasone] [nvarchar](1000) NULL,
	[BirthPlaceIntended] [nvarchar](1000) NULL,
	[BleedingEpisodes] [nvarchar](1000) NULL,
	[BOTCareChange] [nvarchar](1000) NULL,
	[EDD] [nvarchar](1000) NULL,
	[EDDScan] [nvarchar](1000) NULL,
	[LMP] [nvarchar](1000) NULL,
	[ProblemsBP] [nvarchar](1000) NULL,
	[ProblemsFetal] [nvarchar](1000) NULL,
	[ProblemsMedical] [nvarchar](1000) NULL,
	[ReasonForChangeAN] [nvarchar](1000) NULL,
	[RubellaStatus] [nvarchar](1000) NULL,
	[ScanFindings] [nvarchar](1000) NULL
)
