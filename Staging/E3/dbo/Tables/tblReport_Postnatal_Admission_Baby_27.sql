﻿CREATE TABLE [dbo].[tblReport_Postnatal_Admission_Baby_27](
	[UserID] [int] NULL,
	[BabyID] [bigint] NULL,
	[BabyadmissionID] [bigint] NULL,
	[AdmitBy_Value] [bigint] NULL,
	[AdmitReason_Value] [bigint] NULL,
	[AdmitTo_Value] [bigint] NULL,
	[ReferralMode_Value] [bigint] NULL,
	[AdmitBy] [nvarchar](1000) NULL,
	[AdmitDT] [nvarchar](1000) NULL,
	[AdmitReason] [nvarchar](1000) NULL,
	[AdmitTo] [nvarchar](1000) NULL,
	[ReferralMode] [nvarchar](1000) NULL
)
