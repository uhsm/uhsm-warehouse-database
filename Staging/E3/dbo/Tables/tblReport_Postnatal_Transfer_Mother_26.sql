﻿CREATE TABLE [dbo].[tblReport_Postnatal_Transfer_Mother_26](
	[UserID] [int] NULL,
	[PregnancyID] [bigint] NULL,
	[PregnancyadmissionID] [bigint] NULL,
	[TransfusionProblems_Value] [bigint] NULL,
	[TransfusionProblems] [nvarchar](1000) NULL,
	[ActionFollowingCounselling_Value] [bigint] NULL,
	[Agencies_Value] [bigint] NULL,
	[AntiD_Value] [bigint] NULL,
	[AntiDGivenBy_Value] [bigint] NULL,
	[Appointments_Value] [bigint] NULL,
	[BloodTransfusionRQD_Value] [bigint] NULL,
	[BowelProblems_Value] [bigint] NULL,
	[BP_Value] [bigint] NULL,
	[BreastExam_Value] [bigint] NULL,
	[ContactTelephone_Value] [bigint] NULL,
	[CounselledBy_Value] [bigint] NULL,
	[CounsellingOrDebriefing_Value] [bigint] NULL,
	[Destination_Value] [bigint] NULL,
	[DischargedBy_Value] [bigint] NULL,
	[EmotionalState_Value] [bigint] NULL,
	[FeedingMethod_Value] [bigint] NULL,
	[FinalDischarge_Value] [bigint] NULL,
	[Hb_Value] [bigint] NULL,
	[InformationGiven_Value] [bigint] NULL,
	[LegExam_Value] [bigint] NULL,
	[Lochia_Value] [bigint] NULL,
	[Mode_Value] [bigint] NULL,
	[OtherPNProblems_Value] [bigint] NULL,
	[Perineum_Value] [bigint] NULL,
	[ProceduresDone_Value] [bigint] NULL,
	[Pulse_Value] [bigint] NULL,
	[Reason_Value] [bigint] NULL,
	[ReferralsMade_Value] [bigint] NULL,
	[RoutinePNCare_Value] [bigint] NULL,
	[Rubella_Value] [bigint] NULL,
	[RubellaSerologyTest_Value] [bigint] NULL,
	[SmokerDischarge_Value] [bigint] NULL,
	[SupervisedBy_Value] [bigint] NULL,
	[Temperature_Value] [bigint] NULL,
	[TransferredFrom_Value] [bigint] NULL,
	[Uterus_Value] [bigint] NULL,
	[ActionFollowingCounselling] [nvarchar](1000) NULL,
	[AdditionalComments] [nvarchar](1000) NULL,
	[AdmissionDT] [nvarchar](1000) NULL,
	[Agencies] [nvarchar](1000) NULL,
	[AntiD] [nvarchar](1000) NULL,
	[AntiDBatchNo] [nvarchar](1000) NULL,
	[AntiDDate] [nvarchar](1000) NULL,
	[AntiDGivenBy] [nvarchar](1000) NULL,
	[Appointments] [nvarchar](1000) NULL,
	[BloodsTaken] [nvarchar](1000) NULL,
	[BloodTransfusionRQD] [nvarchar](1000) NULL,
	[BowelProblems] [nvarchar](1000) NULL,
	[BP] [nvarchar](1000) NULL,
	[BPProblems] [nvarchar](1000) NULL,
	[BreastExam] [nvarchar](1000) NULL,
	[BreastFeedSupport] [nvarchar](1000) NULL,
	[CMRingWard] [nvarchar](1000) NULL,
	[ContactTelephone] [nvarchar](1000) NULL,
	[CounselledBy] [nvarchar](1000) NULL,
	[CounsellingOrDebriefing] [nvarchar](1000) NULL,
	[Debriefing] [nvarchar](1000) NULL,
	[Destination] [nvarchar](1000) NULL,
	[DischargedBy] [nvarchar](1000) NULL,
	[DischargedDT] [nvarchar](1000) NULL,
	[EmotionalState] [nvarchar](1000) NULL,
	[FeedingMethod] [nvarchar](1000) NULL,
	[FinalDischarge] [nvarchar](1000) NULL,
	[Hb] [nvarchar](1000) NULL,
	[InformationGiven] [nvarchar](1000) NULL,
	[IntravenousFluids] [nvarchar](1000) NULL,
	[LegExam] [nvarchar](1000) NULL,
	[Lochia] [nvarchar](1000) NULL,
	[Medication] [nvarchar](1000) NULL,
	[MicturitionProblems] [nvarchar](1000) NULL,
	[Mode] [nvarchar](1000) NULL,
	[ObsReviewReq] [nvarchar](1000) NULL,
	[ObstetricDecision] [nvarchar](1000) NULL,
	[OtherPNProblems] [nvarchar](1000) NULL,
	[OtherTests] [nvarchar](1000) NULL,
	[ParentEducation] [nvarchar](1000) NULL,
	[Perineum] [nvarchar](1000) NULL,
	[PerineumManagement] [nvarchar](1000) NULL,
	[ProceduresDone] [nvarchar](1000) NULL,
	[Pulse] [nvarchar](1000) NULL,
	[PulseRateProblems] [nvarchar](1000) NULL,
	[Reason] [nvarchar](1000) NULL,
	[ReferralsMade] [nvarchar](1000) NULL,
	[RoutinePNCare] [nvarchar](1000) NULL,
	[Rubella] [nvarchar](1000) NULL,
	[RubellaSerologyTest] [nvarchar](1000) NULL,
	[SmearRequired] [nvarchar](1000) NULL,
	[SmokerDischarge] [nvarchar](1000) NULL,
	[SupervisedBy] [nvarchar](1000) NULL,
	[SupportGroups] [nvarchar](1000) NULL,
	[Temperature] [nvarchar](1000) NULL,
	[TemperatureProblems] [nvarchar](1000) NULL,
	[TransferredFrom] [nvarchar](1000) NULL,
	[Urinalysis] [nvarchar](1000) NULL,
	[Uterus] [nvarchar](1000) NULL,
	[Wound] [nvarchar](1000) NULL,
	[AdditionalComments_Value] [bigint] NULL,
	[PerinealFollowUP] [nvarchar](1000) NULL,
	[PerinealFollowUP_Value] [bigint] NULL,
	[UrinePostDelivery] [nvarchar](1000) NULL,
	[UrineDT] [nvarchar](1000) NULL,
	[UrineVolume] [nvarchar](1000) NULL,
	[HbResult] [nvarchar](1000) NULL,
	[HbResult_Value] [bigint] NULL,
	[AntiDDosePostNatal] [nvarchar](1000) NULL,
	[AntiDDosePostNatal_Value] [bigint] NULL,
	[DischargeMethod_Value] [bigint] NULL,
	[DischargeMethod] [nvarchar](1000) NULL,
	[PNInvestigations] [nvarchar](1000) NULL,
	[PNInvestigations_Value] [bigint] NULL,
	[PNBloodTests] [nvarchar](1000) NULL,
	[PNBloodTests_Value] [bigint] NULL,
	[RespirationRate] [nvarchar](1000) NULL,
	[RespirationRate_Value] [bigint] NULL,
	[MicturitionProblems_Value] [bigint] NULL,
	[Wound_Value] [bigint] NULL,
	[Medication_Value] [bigint] NULL,
	[ThromboembolicRiskPN] [nvarchar](1000) NULL,
	[TeamPN] [nvarchar](1000) NULL,
	[ThromboembolicRiskPN_Value] [bigint] NULL,
	[TeamPN_Value] [bigint] NULL,
	[TransferredWithBaby] [nvarchar](1000) NULL,
	[TransferredWithBaby_Value] [bigint] NULL,
	[DiscussionsDuringPNAdmission] [nvarchar](1000) NULL,
	[DiscussionsDuringPNAdmission_Value] [bigint] NULL,
	[FGMInformationImplications] [nvarchar](1000) NULL,
	[FGMInformationImplications_Value] [bigint] NULL,
	[FGMInformationIllegality] [nvarchar](1000) NULL
)
