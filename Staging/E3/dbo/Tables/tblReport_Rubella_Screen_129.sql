﻿CREATE TABLE [dbo].[tblReport_Rubella_Screen_129](
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[RubellaTestDate] [nvarchar](1000) NULL,
	[RubellaTestDate_Value] [bigint] NULL,
	[RubellaStatus] [nvarchar](1000) NULL
)
