﻿CREATE TABLE [dbo].[tblReport_Syphilis_Screen_130](
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[SyphilisTestDate] [nvarchar](1000) NULL,
	[SyphilisTestDate_Value] [bigint] NULL,
	[SyphilisTestResult] [nvarchar](1000) NULL,
	[SyphilisTestResult_Value] [bigint] NULL,
	[SyphilisActionTaken] [nvarchar](1000) NULL,
	[SyphilisActionTaken_Value] [bigint] NULL,
	[SyphilisClassification] [nvarchar](1000) NULL
)
