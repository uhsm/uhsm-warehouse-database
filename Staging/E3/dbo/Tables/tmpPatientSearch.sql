﻿CREATE TABLE [dbo].[tmpPatientSearch](
	[gId] [uniqueidentifier] NULL,
	[row] [bigint] NULL,
	[patientid] [bigint] NULL,
	[deliverydate] [nvarchar](20) NULL,
	[LastUpdated] [datetime] NULL,
	[rownumber] [int] NOT NULL
)
