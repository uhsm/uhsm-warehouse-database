﻿CREATE TABLE [meta].[TableSizeHistory](
	[id] [int] NOT NULL,
	[historyDate] [datetime] NOT NULL,
	[schemaName] [sysname] NOT NULL,
	[tableName] [sysname] NOT NULL,
	[rows] [int] NULL,
	[reservedKB] [int] NULL,
	[dataKB] [int] NULL,
	[indexSizeKB] [int] NULL,
	[unusedKB] [int] NULL
)
