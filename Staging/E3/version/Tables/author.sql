﻿CREATE TABLE [version].[author](
	[id] [int] NOT NULL,
	[name] [varchar](10) NOT NULL,
	[active] [bit] NOT NULL
)
