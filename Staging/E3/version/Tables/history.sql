﻿CREATE TABLE [version].[history](
	[id] [int] NOT NULL,
	[number] [char](20) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[notes] [varchar](255) NOT NULL,
	[authorId] [int] NOT NULL,
	[executed] [datetime] NOT NULL
)
