﻿CREATE TABLE [dbo].[AssignmentCompetency]
(
	[Competence Name] [varchar](255) NULL,
	[Expiry Date] [varchar](9) NULL,
	[Competence Match] [varchar](255) NULL,
	[Assignment] [varchar](30) NULL
)
