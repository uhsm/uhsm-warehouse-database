﻿CREATE TABLE [dbo].[AM_Merge_Ready2](
	[groupid] [int] NULL,
	[pid] [int] NULL,
	[master] [int] NULL,
	[reason] [tinyint] NULL,
	[type] [tinyint] NULL
)