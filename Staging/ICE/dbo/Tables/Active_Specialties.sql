﻿CREATE TABLE [dbo].[Active_Specialties](
	[ORG_Nat_Code] [varchar](6) NULL,
	[Specialty_Code] [int] NULL,
	[Requesting_Active] [bit] NOT NULL,
	[Reporting_Active] [bit] NOT NULL
)