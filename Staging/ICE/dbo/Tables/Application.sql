﻿CREATE TABLE [dbo].[Application](
	[ApplicationID] [int] NOT NULL,
	[ApplicationName] [varchar](50) NOT NULL,
	[BriefName] [varchar](10) NULL,
	[Notes] [text] NULL,
	[Installed] [bit] NOT NULL,
	[Colour] [int] NULL,
	[Mobile_Installed] [bit] NULL
)