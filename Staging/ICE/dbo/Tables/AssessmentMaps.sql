﻿CREATE TABLE [dbo].[AssessmentMaps](
	[MapId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[HelpText] [varchar](255) NULL,
	[MasterLevel] [bit] NOT NULL,
	[Active] [bit] NOT NULL,
	[Default] [bit] NOT NULL,
	[MinAge] [smallint] NULL,
	[MaxAge] [smallint] NULL,
	[ExcludeIfMale] [bit] NOT NULL,
	[ExcludeIfFemale] [bit] NOT NULL,
	[MapImage] [image] NULL
)