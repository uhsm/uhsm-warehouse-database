﻿CREATE TABLE [dbo].[Audit_Filed_Reports](
	[Report_Index] [int] NOT NULL,
	[User_Index] [int] NOT NULL,
	[Filed_Date] [datetime] NOT NULL,
	[Reason_Index] [int] NOT NULL,
	[Reason_Text] [varchar](50) NULL
)