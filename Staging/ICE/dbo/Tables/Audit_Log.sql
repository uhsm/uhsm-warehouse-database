﻿CREATE TABLE [dbo].[Audit_Log](
	[id] [int] NOT NULL,
	[application_id] [int] NOT NULL,
	[action_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[event_idx] [int] NULL,
	[create_dt] [datetime] NOT NULL,
	[data] [varchar](2000) NULL
)