﻿CREATE TABLE [dbo].[Audit_Viewed_Reports](
	[Report_Index] [int] NOT NULL,
	[Date_Read] [datetime] NULL,
	[Username] [varchar](25) NULL
)