﻿CREATE TABLE [dbo].[BDU_Upload_Headers](
	[BDU_ID] [int] NOT NULL,
	[UserIndex] [int] NOT NULL,
	[UploadType] [varchar](30) NOT NULL,
	[UploadVersion] [varchar](4) NOT NULL,
	[Completed] [bit] NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateCompleted] [datetime] NULL
)