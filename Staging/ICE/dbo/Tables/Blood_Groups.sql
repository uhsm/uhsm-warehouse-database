﻿CREATE TABLE [dbo].[Blood_Groups](
	[Blood_Group_Index] [smallint] NOT NULL,
	[Code] [varchar](10) NULL,
	[Name] [varchar](50) NOT NULL,
	[Equivalent_To] [smallint] NOT NULL
)