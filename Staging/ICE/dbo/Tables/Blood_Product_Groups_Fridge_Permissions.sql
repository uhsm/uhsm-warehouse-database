﻿CREATE TABLE [dbo].[Blood_Product_Groups_Fridge_Permissions](
	[Permission_Index] [int] NOT NULL,
	[Group_Index] [int] NOT NULL,
	[Fridge_Type_Index] [int] NOT NULL,
	[Permitted] [bit] NOT NULL
)