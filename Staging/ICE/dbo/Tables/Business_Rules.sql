﻿CREATE TABLE [dbo].[Business_Rules](
	[Id] [int] NOT NULL,
	[Entity_Name] [varchar](100) NOT NULL,
	[Property_Name] [varchar](100) NOT NULL,
	[Rule_Type] [int] NOT NULL,
	[Rule_Value] [varchar](255) NULL,
	[Read_Only] [bit] NOT NULL
)