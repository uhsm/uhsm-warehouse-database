﻿CREATE TABLE [dbo].[CPT_Codes](
	[Code] [varchar](5) NOT NULL,
	[Modifier] [varchar](2) NULL,
	[Short_Description] [varchar](35) NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[Ftot] [decimal](18, 0) NULL,
	[Ntot] [decimal](18, 0) NULL,
	[Billing_Status] [char](1) NULL,
	[Male] [bit] NOT NULL,
	[Female] [bit] NOT NULL,
	[Comp] [bit] NOT NULL,
	[Medical_Necessity] [bit] NOT NULL,
	[Active_Date] [smalldatetime] NULL,
	[Inactive_Date] [smalldatetime] NULL
)