﻿CREATE TABLE [dbo].[Cached_Data](
	[Group] [varchar](20) NOT NULL,
	[Property] [varchar](50) NOT NULL,
	[Value] [text] NOT NULL
)