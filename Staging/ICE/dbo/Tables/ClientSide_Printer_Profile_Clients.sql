﻿CREATE TABLE [dbo].[ClientSide_Printer_Profile_Clients](
	[Client_Index] [int] NOT NULL,
	[Identity_GUID] [uniqueidentifier] NOT NULL,
	[IP_Address] [varchar](15) NOT NULL,
	[Last_Accessed] [datetime] NOT NULL,
	[Last_Machine_Name] [varchar](100) NULL,
	[Last_ICE_User] [int] NULL
)