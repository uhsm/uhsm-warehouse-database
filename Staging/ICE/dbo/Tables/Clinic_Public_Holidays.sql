﻿CREATE TABLE [dbo].[Clinic_Public_Holidays](
	[Holiday_Index] [int] NOT NULL,
	[Year] [smallint] NOT NULL,
	[JulianDate] [smallint] NOT NULL,
	[HolidayName] [varchar](50) NOT NULL
)