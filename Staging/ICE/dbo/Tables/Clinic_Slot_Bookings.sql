﻿CREATE TABLE [dbo].[Clinic_Slot_Bookings](
	[Booking_Index] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[CancelReason_Index] [smallint] NULL,
	[DNAReason_Index] [smallint] NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateLastEdited] [datetime] NOT NULL,
	[Urgency_Index] [tinyint] NOT NULL,
	[ReferringUser] [int] NULL,
	[ReferringClinician] [int] NULL,
	[ReferringLocation] [int] NULL,
	[Patient_Index] [int] NOT NULL,
	[Service_Request_Index] [int] NULL,
	[Status] [tinyint] NOT NULL,
	[SummaryPrinted] [bit] NULL
)