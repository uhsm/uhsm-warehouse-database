﻿CREATE TABLE [dbo].[Clinic_Slot_Pattern_Weeks](
	[Clinic_Index] [int] NOT NULL,
	[Pattern_Index] [int] NOT NULL,
	[Year] [smallint] NOT NULL,
	[WeekOfYear] [tinyint] NOT NULL,
	[Published] [bit] NOT NULL
)