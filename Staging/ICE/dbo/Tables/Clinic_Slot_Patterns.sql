﻿CREATE TABLE [dbo].[Clinic_Slot_Patterns](
	[Pattern_Index] [int] NOT NULL,
	[Clinic_Index] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[Active] [bit] NOT NULL
)