﻿CREATE TABLE [dbo].[Clinic_Slots](
	[Slot_Index] [int] NOT NULL,
	[Clinic_Index] [int] NOT NULL,
	[Year] [smallint] NOT NULL,
	[JulianDate] [smallint] NOT NULL,
	[StartTime] [smalldatetime] NOT NULL,
	[Duration] [smallint] NOT NULL,
	[CutoffMinsBefore] [smallint] NULL,
	[Sequence] [smallint] NOT NULL,
	[Urgency_Index] [tinyint] NOT NULL,
	[Clinician] [int] NULL,
	[Specialty] [int] NULL,
	[Active] [bit] NOT NULL,
	[CancelReason_Index] [smallint] NULL,
	[Booking_Index] [int] NULL,
	[infinite_slot] [bit] NOT NULL
)