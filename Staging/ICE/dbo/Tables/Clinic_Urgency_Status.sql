﻿CREATE TABLE [dbo].[Clinic_Urgency_Status](
	[Urgency_Index] [int] NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Urgent] [bit] NOT NULL,
	[TwoWeekWait] [bit] NOT NULL,
	[Active] [bit] NOT NULL
)