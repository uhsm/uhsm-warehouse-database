﻿CREATE TABLE [dbo].[ClinicalFormAnnotatableImage](
	[ImageId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[ImageBinary] [image] NULL,
	[WarningWatermark] [tinyint] NOT NULL,
	[WarningWatermarkColour] [int] NULL
)