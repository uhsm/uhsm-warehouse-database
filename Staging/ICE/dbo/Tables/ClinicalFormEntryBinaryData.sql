﻿CREATE TABLE [dbo].[ClinicalFormEntryBinaryData](
	[DataId] [int] NOT NULL,
	[ImageBinary] [image] NOT NULL
)