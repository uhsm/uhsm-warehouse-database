﻿CREATE TABLE [dbo].[ClinicalFormEntryData](
	[DataId] [int] NOT NULL,
	[EntryId] [int] NOT NULL,
	[FactorId] [int] NOT NULL,
	[Score] [smallint] NULL,
	[OutOfRange] [bit] NOT NULL,
	[Value] [varchar](2000) NULL
)