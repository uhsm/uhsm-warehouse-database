﻿CREATE TABLE [dbo].[ClinicalFormFactorPicklistItem](
	[PicklistId] [int] NOT NULL,
	[FactorId] [int] NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[Order] [int] NOT NULL,
	[ScoreVal] [tinyint] NULL
)