﻿CREATE TABLE [dbo].[ClinicalFormGroup](
	[GroupId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL
)