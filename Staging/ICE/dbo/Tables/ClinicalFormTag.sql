﻿CREATE TABLE [dbo].[ClinicalFormTag](
	[TagId] [int] NOT NULL,
	[TagName] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL
)