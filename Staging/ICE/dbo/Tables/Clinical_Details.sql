﻿CREATE TABLE [dbo].[Clinical_Details](
	[CLIN_DETAIL_KEY] [varchar](61) NOT NULL,
	[DISCHARGE_KEY] [varchar](51) NULL,
	[DETAIL_TYPE] [varchar](1) NULL,
	[READ_CODE] [varchar](5) NULL,
	[FREE_TEXT] [varchar](132) NULL,
	[USER_TEXT] [text] NULL,
	[DETAIL_TITLE] [varchar](40) NULL,
	[LINE_TYPE] [varchar](1) NULL,
	[DateTime_of_Detail] [datetime] NULL,
	[COMMENTS_MARKER] [bit] NOT NULL,
	[DATE_ADDED] [datetime] NULL
)