﻿CREATE TABLE [dbo].[Clinical_Letter_Non_Formulary_Items](
	[Drug_Index] [int] NOT NULL,
	[Drug_Name] [varchar](50) NOT NULL,
	[User_Index] [int] NOT NULL,
	[Active] [bit] NOT NULL
)