﻿CREATE TABLE [dbo].[Clinical_Letter_Paragraph_Templates_Dropdown_Items](
	[Dropdown_Index] [int] NOT NULL,
	[Paragraph_Template_Index] [int] NOT NULL,
	[Item_Text] [varchar](255) NOT NULL,
	[Order] [tinyint] NOT NULL,
	[Specialty] [smallint] NULL
)