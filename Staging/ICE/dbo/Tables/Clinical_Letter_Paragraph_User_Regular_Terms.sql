﻿CREATE TABLE [dbo].[Clinical_Letter_Paragraph_User_Regular_Terms](
	[Term_Index] [int] NOT NULL,
	[Paragraph_Template_Index] [int] NOT NULL,
	[User_Index] [int] NOT NULL,
	[Item_Text] [text] NOT NULL
)