﻿CREATE TABLE [dbo].[Clinical_Letter_Procedures](
	[Procedure_Index] [int] NOT NULL,
	[Letter_Index] [int] NOT NULL,
	[Procedure_Type] [char](1) NOT NULL,
	[Procedure_Code] [varchar](8) NOT NULL,
	[Procedure_Text] [varchar](255) NOT NULL,
	[Episode_Index] [int] NOT NULL,
	[Date_Of_Procedure] [datetime] NOT NULL,
	[Comments] [text] NULL
)