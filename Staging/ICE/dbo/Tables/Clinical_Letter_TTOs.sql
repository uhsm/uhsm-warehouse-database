﻿CREATE TABLE [dbo].[Clinical_Letter_TTOs](
	[TTO_Index] [int] NOT NULL,
	[Letter_Index] [int] NOT NULL,
	[Formulary_Index] [int] NULL,
	[Source_Option_Index] [int] NULL,
	[Frequency_Option_Index] [int] NULL,
	[Route_Option_Index] [int] NULL,
	[GPAction_Option_Index] [int] NULL,
	[Signatory_Index] [int] NOT NULL,
	[Dose] [varchar](50) NULL,
	[Days] [varchar](50) NULL,
	[Last_Amended_Index] [int] NULL,
	[Non_Formulary_Index] [int] NULL
)