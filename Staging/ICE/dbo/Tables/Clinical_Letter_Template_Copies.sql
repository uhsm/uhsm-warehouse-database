﻿CREATE TABLE [dbo].[Clinical_Letter_Template_Copies](
	[Template_Copy_Index] [int] NOT NULL,
	[Template_Index] [int] NOT NULL,
	[Copy_Name] [varchar](50) NOT NULL,
	[Order] [int] NOT NULL,
	[Print_Font] [int] NOT NULL
)