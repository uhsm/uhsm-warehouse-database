﻿CREATE TABLE [dbo].[Clinical_Letters_Archive](
	[Archive_Letter_Index] [int] NOT NULL,
	[Letter_Index] [int] NOT NULL,
	[User_Index] [int] NOT NULL,
	[Version] [int] NOT NULL,
	[Date_Added] [datetime] NOT NULL,
	[XML] [text] NOT NULL
)