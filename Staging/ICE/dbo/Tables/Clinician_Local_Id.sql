﻿CREATE TABLE [dbo].[Clinician_Local_Id](
	[Clinician_Local_code] [varchar](35) NOT NULL,
	[Date_Added] [datetime] NULL,
	[EDI_LTS_Index] [int] NOT NULL,
	[Clinician_Index] [int] NOT NULL,
	[Inbound] [bit] NOT NULL,
	[Outbound] [bit] NOT NULL
)