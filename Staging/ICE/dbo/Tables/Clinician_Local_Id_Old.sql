﻿CREATE TABLE [dbo].[Clinician_Local_Id_Old](
	[Clinician_National_Code] [varchar](8) NOT NULL,
	[Clinician_Local_code] [varchar](16) NOT NULL,
	[Date_Added] [datetime] NULL,
	[EDI_LTS_Index] [int] NOT NULL
)