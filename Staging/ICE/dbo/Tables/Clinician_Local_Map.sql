﻿CREATE TABLE [dbo].[Clinician_Local_Map](
	[Organisation] [varchar](6) NOT NULL,
	[Input_Clinician_Code] [varchar](16) NOT NULL,
	[Local_Clinician_Code] [varchar](16) NOT NULL,
	[EDI_LTS_Index] [int] NOT NULL
)