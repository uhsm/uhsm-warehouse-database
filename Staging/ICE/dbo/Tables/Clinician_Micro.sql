﻿CREATE TABLE [dbo].[Clinician_Micro](
	[MicroCode] [varchar](16) NOT NULL,
	[Microname] [varchar](35) NULL,
	[PathCode] [varchar](16) NOT NULL,
	[Pathname] [varchar](35) NULL,
	[NatCode] [varchar](50) NULL
)