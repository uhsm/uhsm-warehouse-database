﻿CREATE TABLE [dbo].[Clinician_Rad](
	[RadCode] [varchar](16) NOT NULL,
	[RadName] [varchar](50) NULL,
	[NatCode] [varchar](8) NULL,
	[ICECode] [varchar](8) NULL
)