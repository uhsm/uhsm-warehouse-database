﻿CREATE TABLE [dbo].[Colours](
	[Colour_Index] [int] NOT NULL,
	[Colour_Name] [varchar](20) NULL,
	[Colour_Code] [varchar](11) NULL,
	[Date_Added] [datetime] NULL,
	[organisation] [varchar](6) NOT NULL
)