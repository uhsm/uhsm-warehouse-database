﻿CREATE TABLE [dbo].[Common_Codes](
	[Index] [int] NOT NULL,
	[CommonCode] [char](5) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Type] [char](1) NOT NULL
)