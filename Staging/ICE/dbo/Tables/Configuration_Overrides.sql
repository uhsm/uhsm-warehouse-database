﻿CREATE TABLE [dbo].[Configuration_Overrides](
	[Config_Type_Index] [int] NOT NULL,
	[Config_Index] [int] NOT NULL,
	[Override_Index] [int] NOT NULL,
	[Override_Description] [varchar](50) NOT NULL,
	[Override_Type] [smallint] NULL,
	[Override_Value] [varchar](255) NOT NULL,
	[Override_Notes] [varchar](255) NULL
)