﻿CREATE TABLE [dbo].[Contract](
	[Contact_key] [int] NULL,
	[Contract_number] [varchar](17) NULL,
	[Contract_line_number] [varchar](10) NULL,
	[Purchaser_ref_number] [varchar](17) NULL,
	[Contract_prov_code] [varchar](5) NULL,
	[Purchaser_code] [varchar](5) NULL,
	[Serv_arrange_chng_type] [varchar](2) NULL,
	[Serv_arrange_chng_date] [datetime] NULL,
	[Date_added] [datetime] NULL
)