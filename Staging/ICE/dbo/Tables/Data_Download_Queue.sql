﻿CREATE TABLE [dbo].[Data_Download_Queue](
	[DDQ_Index] [int] NOT NULL,
	[LocationIndex] [int] NOT NULL,
	[ServiceIndex] [int] NOT NULL,
	[InteropIndex] [int] NULL,
	[Status] [bit] NOT NULL,
	[ServiceType] [char](3) NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateDownloaded] [datetime] NULL,
	[XMLMessage] [text] NOT NULL
)