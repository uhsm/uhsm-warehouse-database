﻿CREATE TABLE [dbo].[Discharge_Date_Algorithms](
	[id] [int] NOT NULL,
	[procedure_id] [int] NOT NULL,
	[clinician_id] [varchar](8) NULL,
	[patient_age_min] [tinyint] NOT NULL,
	[patient_age_max] [tinyint] NOT NULL,
	[hours] [smallint] NOT NULL,
	[type] [tinyint] NOT NULL
)