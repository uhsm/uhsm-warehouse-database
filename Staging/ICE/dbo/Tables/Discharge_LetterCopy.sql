﻿CREATE TABLE [dbo].[Discharge_LetterCopy](
	[CopyIndex] [int] NOT NULL,
	[RoleIndex] [tinyint] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[Description] [varchar](30) NOT NULL
)