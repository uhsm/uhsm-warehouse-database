﻿CREATE TABLE [dbo].[Discharge_Letter_Paragraphs](
	[ParagraphID] [int] NOT NULL,
	[LetterID] [int] NOT NULL,
	[Letter_Paragraph_Index] [int] NOT NULL,
	[Text] [varchar](8000) NULL,
	[Date_Amended] [datetime] NULL,
	[Revision] [int] NULL,
	[Date_Added] [datetime] NULL,
	[User_Index] [int] NULL
)