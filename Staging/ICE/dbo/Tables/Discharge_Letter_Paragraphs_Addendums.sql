﻿CREATE TABLE [dbo].[Discharge_Letter_Paragraphs_Addendums](
	[AddendumID] [int] NOT NULL,
	[ParagraphID] [int] NOT NULL,
	[AddendumDate] [datetime] NOT NULL,
	[AddendumText] [varchar](5000) NOT NULL,
	[User_Index] [int] NULL
)