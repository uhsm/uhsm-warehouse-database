﻿CREATE TABLE [dbo].[Discharge_Letter_Paragraphs_Results](
	[ParagraphID] [int] NOT NULL,
	[SequenceID] [int] NOT NULL,
	[Results] [text] NULL,
	[ID] [int] NOT NULL
)