﻿CREATE TABLE [dbo].[Discharge_Letter_TTOs_Substituted](
	[SubstitutedTTOID] [int] NOT NULL,
	[TTOID] [int] NOT NULL,
	[ReadCode] [varchar](8) NOT NULL,
	[TermCode] [char](2) NOT NULL,
	[RouteCode] [char](10) NULL,
	[Quantity] [varchar](20) NULL,
	[FrequencyCode] [char](10) NULL,
	[Days] [tinyint] NULL,
	[Source] [varchar](20) NULL,
	[GPAction] [char](10) NULL,
	[DateAmended] [datetime] NULL
)