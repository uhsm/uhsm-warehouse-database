﻿CREATE TABLE [dbo].[Discharge_Procedures](
	[id] [int] NOT NULL,
	[procedure_code] [varchar](6) NOT NULL,
	[specialty_id] [smallint] NOT NULL,
	[codetype] [int] NOT NULL
)