﻿CREATE TABLE [dbo].[Discharge_Secretary](
	[Discharge_Secretary_ID] [int] NULL,
	[Secretary_User_ID] [varchar](50) NULL,
	[Secretary_Fullname] [varchar](50) NULL,
	[Secretary_Initials] [varchar](4) NULL,
	[Discharge_Workgroup] [int] NULL
)