﻿CREATE TABLE [dbo].[Dynamic_Page_Register](
	[Application_ID] [int] NOT NULL,
	[Screen_ID] [int] NOT NULL,
	[Page_URL] [varchar](200) NOT NULL,
	[Page_Description] [varchar](200) NOT NULL,
	[Help_URL] [varchar](255) NULL
)