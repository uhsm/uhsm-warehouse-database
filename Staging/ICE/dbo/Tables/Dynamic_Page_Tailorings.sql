﻿CREATE TABLE [dbo].[Dynamic_Page_Tailorings](
	[Application_ID] [int] NOT NULL,
	[Screen_ID] [int] NOT NULL,
	[Field] [varchar](50) NOT NULL,
	[Property] [varchar](30) NOT NULL,
	[Property_Type] [char](1) NOT NULL,
	[Value] [varchar](300) NULL,
	[Language_Lookup_Key_Ind] [bit] NOT NULL
)