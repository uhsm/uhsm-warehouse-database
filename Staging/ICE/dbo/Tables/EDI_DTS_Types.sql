﻿CREATE TABLE [dbo].[EDI_DTS_Types](
	[DTS_TypeIndex] [int] NULL,
	[DTS_Id] [varchar](30) NULL,
	[DTS_Ack_Id] [varchar](30) NULL,
	[Comment] [varchar](200) NULL
)