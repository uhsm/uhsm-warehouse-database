﻿CREATE TABLE [dbo].[EDI_Health_Parties](
	[EDI_HP_Index] [int] NOT NULL,
	[EDI_Report_Index] [int] NULL,
	[EDI_HP_Nat_code] [varchar](35) NULL,
	[EDI_HP_Type] [varchar](3) NULL,
	[EDI_HP_Specialty] [varchar](35) NULL,
	[EDI_HP_Address] [varchar](50) NULL
)