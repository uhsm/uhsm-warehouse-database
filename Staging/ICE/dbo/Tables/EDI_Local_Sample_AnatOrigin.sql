﻿CREATE TABLE [dbo].[EDI_Local_Sample_AnatOrigin](
	[National_Code] [char](5) NOT NULL,
	[Local_Text] [varchar](50) NULL,
	[ID] [int] NOT NULL
)