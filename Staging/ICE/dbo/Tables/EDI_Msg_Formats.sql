﻿CREATE TABLE [dbo].[EDI_Msg_Formats](
	[Msg_Index] [int] NOT NULL,
	[Type] [varchar](20) NOT NULL,
	[Version] [varchar](20) NOT NULL,
	[Description] [varchar](20) NOT NULL,
	[Status_Flag] [int] NULL,
	[Active] [bit] NULL,
	[File_Extension] [varchar](5) NULL
)