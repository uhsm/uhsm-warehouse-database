﻿CREATE TABLE [dbo].[EDI_Recipient_Individuals](
	[Individual_Index] [int] NOT NULL,
	[Organisation] [varchar](6) NOT NULL,
	[EDI_Org_NatCode] [varchar](10) NOT NULL,
	[EDI_NatCode] [varchar](10) NOT NULL,
	[EDI_GP_Name] [varchar](35) NULL,
	[EDI_Active] [bit] NOT NULL,
	[EDI_SMTP_Active] [bit] NOT NULL,
	[EDI_SMTP_Mail] [varchar](250) NULL
)