﻿CREATE TABLE [dbo].[Email_Addresses](
	[Email_Index] [int] NOT NULL,
	[Mail_Address] [varchar](255) NOT NULL,
	[Mail_Type] [int] NOT NULL,
	[User_Index] [int] NULL,
	[Username] [varchar](60) NULL,
	[ExternalAddress] [bit] NOT NULL,
	[Active] [bit] NOT NULL,
	[Choke_Limit] [int] NULL
)