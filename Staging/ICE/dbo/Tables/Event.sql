﻿CREATE TABLE [dbo].[Event](
	[TransactionID] [uniqueidentifier] NOT NULL,
	[EventType] [tinyint] NOT NULL,
	[EventCode] [int] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[server] [varchar](20) NOT NULL,
	[ID] [int] NOT NULL,
	[Recovery_DateTime] [smalldatetime] NULL,
	[PatientId] [int] NULL
)