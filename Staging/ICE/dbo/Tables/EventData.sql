﻿CREATE TABLE [dbo].[EventData](
	[KeyID] [int] NOT NULL,
	[Value] [text] NOT NULL,
	[Event_ID] [int] NOT NULL,
	[ID] [int] NOT NULL,
	[HashOfValue] [int] NULL
)