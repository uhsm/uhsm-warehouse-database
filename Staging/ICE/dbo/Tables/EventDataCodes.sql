﻿CREATE TABLE [dbo].[EventDataCodes](
	[KeyID] [int] NOT NULL,
	[KeyName] [varchar](30) NOT NULL,
	[KeyNotes] [text] NULL
)