﻿CREATE TABLE [dbo].[External_Links](
	[ID] [int] NOT NULL,
	[Base_URL] [varchar](255) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Active] [bit] NOT NULL
)