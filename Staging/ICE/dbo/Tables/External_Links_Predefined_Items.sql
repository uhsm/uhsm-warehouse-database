﻿CREATE TABLE [dbo].[External_Links_Predefined_Items](
	[ID] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[RequiresPatient] [bit] NOT NULL
)