﻿CREATE TABLE [dbo].[External_Links_QS_Values](
	[ID] [int] NOT NULL,
	[Link_ID] [int] NOT NULL,
	[QS_Item] [int] NULL,
	[Name] [varchar](100) NOT NULL,
	[Value] [varchar](100) NULL,
	[RequiresPatient] [bit] NULL
)