﻿CREATE TABLE [dbo].[HTML_Templates](
	[Id] [float] NULL,
	[Type] [float] NULL,
	[Name] [nvarchar](255) NULL,
	[Data] [ntext] NULL,
	[Comment] [nvarchar](255) NULL
)