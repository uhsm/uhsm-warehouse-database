﻿CREATE TABLE [dbo].[ICD9_CPT_Map](
	[Mapping_Index] [int] NOT NULL,
	[Organisation] [varchar](6) NOT NULL,
	[ICD9_Code] [varchar](10) NOT NULL,
	[CPT_Code] [varchar](5) NOT NULL,
	[Active_Date] [smalldatetime] NOT NULL,
	[Inactive_Date] [smalldatetime] NULL,
	[Active_Marker] [int] NOT NULL
)