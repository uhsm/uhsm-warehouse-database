﻿CREATE TABLE [dbo].[ICEMail](
	[Mail_Id] [int] NOT NULL,
	[From_UserId] [int] NULL,
	[To_UserId] [int] NOT NULL,
	[Date_Added] [datetime] NOT NULL,
	[High_Priority] [bit] NOT NULL,
	[Title] [varchar](200) NOT NULL,
	[Message] [text] NOT NULL,
	[Read] [bit] NOT NULL,
	[Date_Read] [datetime] NULL
)