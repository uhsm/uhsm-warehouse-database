﻿CREATE TABLE [dbo].[ICEMail_Attachment](
	[Attachment_Id] [int] NOT NULL,
	[Mail_Id] [int] NOT NULL,
	[Application_Id] [int] NOT NULL,
	[Remote_Index] [int] NOT NULL
)