﻿CREATE TABLE [dbo].[ICE_Auto_Merge](
	[Id] [int] NOT NULL,
	[MinPid] [int] NOT NULL,
	[MaxPid] [int] NOT NULL,
	[Master] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[Date_Added] [datetime] NULL,
	[Merged] [bit] NOT NULL,
	[Remaining] [int] NOT NULL
)