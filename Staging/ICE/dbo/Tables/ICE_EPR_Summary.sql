﻿CREATE TABLE [dbo].[ICE_EPR_Summary](
	[Id] [int] NOT NULL,
	[Patient_Index] [int] NOT NULL,
	[Time_Effective] [datetime] NOT NULL,
	[Remote_App] [tinyint] NOT NULL,
	[Remote_Index] [int] NULL,
	[Active] [bit] NOT NULL
)