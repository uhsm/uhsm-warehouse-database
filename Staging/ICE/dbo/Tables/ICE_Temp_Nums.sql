﻿CREATE TABLE [dbo].[ICE_Temp_Nums](
	[ICE_Num_ID] [int] NULL,
	[ICE_Clinician_Num] [int] NULL,
	[ICE_GP_Num] [int] NULL,
	[ICE_Request_Num] [int] NULL,
	[ICE_Patient_Num] [int] NULL,
	[ICE_Location_Num] [int] NULL
)