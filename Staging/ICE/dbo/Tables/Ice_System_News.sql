﻿CREATE TABLE [dbo].[Ice_System_News](
	[id] [int] NOT NULL,
	[title] [varchar](100) NOT NULL,
	[news] [varchar](7000) NOT NULL,
	[start_dt] [datetime] NOT NULL,
	[end_dt] [datetime] NULL,
	[system_active] [smallint] NOT NULL,
	[active] [smallint] NOT NULL,
	[create_dt] [datetime] NOT NULL
)