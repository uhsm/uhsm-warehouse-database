﻿CREATE TABLE [dbo].[Internet_Sessions](
	[IPAddress] [varchar](15) NULL,
	[SessionID] [varchar](15) NULL,
	[Product] [varchar](50) NULL,
	[UserName] [varchar](25) NULL
)