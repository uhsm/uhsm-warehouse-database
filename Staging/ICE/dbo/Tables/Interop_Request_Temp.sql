﻿CREATE TABLE [dbo].[Interop_Request_Temp](
	[Guid] [uniqueidentifier] NOT NULL,
	[Code] [varchar](20) NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[IsReadCode] [bit] NOT NULL
)