﻿CREATE TABLE [dbo].[Interop_Services](
	[service] [varchar](50) NOT NULL,
	[url] [varchar](200) NOT NULL,
	[description] [varchar](200) NOT NULL,
	[requery] [bit] NOT NULL,
	[findpatient] [bit] NOT NULL,
	[addpatient] [bit] NOT NULL,
	[menuname] [varchar](30) NULL,
	[active] [bit] NOT NULL,
	[applicationid] [int] NULL,
	[updatepatient] [bit] NOT NULL,
	[id] [smallint] NOT NULL
)