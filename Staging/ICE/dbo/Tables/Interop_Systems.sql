﻿CREATE TABLE [dbo].[Interop_Systems](
	[Interop_System_Index] [smallint] NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Description] [varchar](50) NOT NULL
)