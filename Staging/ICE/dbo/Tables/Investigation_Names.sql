﻿CREATE TABLE [dbo].[Investigation_Names](
	[Investigation_Name_Index] [int] NOT NULL,
	[Investigation_Name] [varchar](60) NOT NULL,
	[Investigation_Order] [int] NULL,
	[Can_Archive] [bit] NOT NULL
)