﻿CREATE TABLE [dbo].[Lab_Codes](
	[Index] [int] NOT NULL,
	[OrgCode] [char](5) NOT NULL,
	[Serv_Provider] [smallint] NOT NULL,
	[LabCode] [varchar](50) NOT NULL,
	[LabDescription] [varchar](50) NULL,
	[CommonCode] [varchar](50) NOT NULL
)