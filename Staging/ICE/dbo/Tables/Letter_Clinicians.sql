﻿CREATE TABLE [dbo].[Letter_Clinicians](
	[Clinician_National_Code] [varchar](8) NOT NULL,
	[Letter_Type_Key] [varchar](20) NOT NULL,
	[DefaultTemplate] [bit] NULL
)