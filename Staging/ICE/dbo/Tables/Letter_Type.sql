﻿CREATE TABLE [dbo].[Letter_Type](
	[Letter_Type_Key] [varchar](20) NOT NULL,
	[Version] [int] NOT NULL,
	[Letter_Heading_1] [varchar](100) NULL,
	[Letter_Heading_2] [varchar](50) NULL,
	[Letter_Heading_3] [varchar](50) NULL,
	[Letter_Heading_4] [varchar](50) NULL,
	[Workgroup] [varchar](10) NULL,
	[Letter_Type] [varchar](3) NULL,
	[Letter_Footer] [int] NOT NULL,
	[Diagnosis] [bit] NULL,
	[Procedures] [bit] NULL,
	[TTO] [bit] NULL,
	[Letter_Heading_Logo] [varchar](250) NULL
)