﻿CREATE TABLE [dbo].[Letter_Type_Details](
	[Letter_Paragraph_Index] [int] NOT NULL,
	[Letter_Type_Key] [varchar](20) NOT NULL,
	[Version] [int] NOT NULL,
	[Position] [int] NULL,
	[Clinical_Heading_Paper] [varchar](50) NULL,
	[Force_Print] [bit] NOT NULL,
	[Mandatory] [bit] NOT NULL,
	[Paragraph_Type] [tinyint] NULL
)