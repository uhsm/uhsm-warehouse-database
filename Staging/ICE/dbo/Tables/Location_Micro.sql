﻿CREATE TABLE [dbo].[Location_Micro](
	[Micro_code] [varchar](10) NULL,
	[Location_Name] [varchar](30) NULL,
	[Path_code] [varchar](10) NULL,
	[Path_Name] [varchar](30) NULL,
	[Location_Index] [int] NULL
)