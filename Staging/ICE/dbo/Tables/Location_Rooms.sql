﻿CREATE TABLE [dbo].[Location_Rooms](
	[Room_Code] [varchar](10) NULL,
	[Room_Active] [bit] NOT NULL,
	[Date_Added] [datetime] NULL,
	[Location_Index] [int] NOT NULL,
	[room_index] [int] NOT NULL,
	[Description] [varchar](100) NULL
)