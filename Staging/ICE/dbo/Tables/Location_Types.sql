﻿CREATE TABLE [dbo].[Location_Types](
	[Location_Type_Index] [smallint] NOT NULL,
	[Location_Type] [varchar](100) NOT NULL,
	[Active] [bit] NOT NULL,
	[SendExternalMail] [bit] NOT NULL
)