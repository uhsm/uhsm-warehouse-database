﻿CREATE TABLE [dbo].[MI_Query](
	[Index] [int] NOT NULL,
	[Name] [varchar](100) NULL,
	[Description] [varchar](1000) NULL,
	[SQL] [text] NOT NULL,
	[Timeout] [int] NOT NULL,
	[Max_Rows] [int] NOT NULL,
	[RestrictedApplicationId] [int] NOT NULL
)