﻿CREATE TABLE [dbo].[MI_Query_Parameters](
	[Index] [int] NOT NULL,
	[Query_Index] [int] NOT NULL,
	[Name] [varchar](25) NOT NULL,
	[Prompt] [varchar](100) NOT NULL,
	[Description] [varchar](1000) NULL,
	[Default] [varchar](100) NULL,
	[Mandatory] [bit] NULL,
	[Type] [varchar](20) NOT NULL,
	[Size] [smallint] NULL,
	[Min] [int] NULL,
	[Max] [int] NULL,
	[Regex] [varchar](100) NULL,
	[Lookup_SQL] [text] NULL
)