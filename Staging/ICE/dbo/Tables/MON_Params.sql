﻿CREATE TABLE [dbo].[MON_Params](
	[ParamID] [varchar](50) NOT NULL,
	[Type] [int] NOT NULL,
	[DisplayType] [int] NULL,
	[DisplayTable] [varchar](30) NULL,
	[DisplayField] [varchar](30) NULL,
	[SelectionCode] [varchar](50) NULL,
	[DataType] [int] NULL,
	[SearchTable] [varchar](30) NULL,
	[SearchField] [varchar](40) NULL,
	[SummaryRow] [int] NULL,
	[SummaryCol] [int] NULL,
	[SummaryPage] [int] NULL,
	[ColWidth] [int] NULL
)