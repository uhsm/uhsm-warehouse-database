﻿CREATE TABLE [dbo].[MON_QueryContents](
	[ID] [int] NOT NULL,
	[ParamID] [varchar](50) NULL,
	[Relationship] [varchar](12) NULL,
	[Value1] [varchar](50) NULL,
	[Value2] [varchar](50) NULL,
	[SortLevel] [int] NULL,
	[RepPos] [int] NULL
)