﻿CREATE TABLE [dbo].[MON_QueryDescription](
	[ID] [int] NOT NULL,
	[RecType] [varchar](1) NULL,
	[ReportType] [varchar](1) NULL,
	[Show] [int] NULL,
	[FinYear] [varchar](4) NULL,
	[Date] [datetime] NULL,
	[Title] [varchar](40) NULL,
	[UserName] [varchar](25) NULL
)