﻿CREATE TABLE [dbo].[MON_User](
	[LicenseName] [varchar](50) NULL,
	[DateLicensed] [datetime] NULL,
	[NextQueryID] [int] NULL,
	[PCG_Code] [varchar](5) NULL
)