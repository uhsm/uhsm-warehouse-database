﻿CREATE TABLE [dbo].[MSHV_Patient_Registration](
	[Index] [int] NOT NULL,
	[Patient_Registration_ID] [varchar](50) NOT NULL,
	[Patient_Index] [int] NOT NULL,
	[Friendly_Name] [varchar](100) NOT NULL,
	[Security_Question] [varchar](100) NOT NULL,
	[Security_Answer] [varchar](100) NOT NULL,
	[Registration_Code] [varchar](50) NULL
)