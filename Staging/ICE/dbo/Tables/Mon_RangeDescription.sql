﻿CREATE TABLE [dbo].[Mon_RangeDescription](
	[RangeID] [int] NULL,
	[RangeParam] [varchar](50) NULL,
	[RangeName] [varchar](50) NULL,
	[RangeType] [smallint] NULL
)