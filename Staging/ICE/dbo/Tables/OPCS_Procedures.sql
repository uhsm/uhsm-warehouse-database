﻿CREATE TABLE [dbo].[OPCS_Procedures](
	[opcs_code] [varchar](6) NOT NULL,
	[opcs_desc] [varchar](225) NOT NULL
)