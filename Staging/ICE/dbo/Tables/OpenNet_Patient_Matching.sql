﻿CREATE TABLE [dbo].[OpenNet_Patient_Matching](
	[Match_Index] [int] NOT NULL,
	[Registration] [int] NOT NULL,
	[Match_Type] [smallint] NOT NULL,
	[Order] [tinyint] NOT NULL,
	[Patient_Identifier_Type_Id] [int] NULL
)