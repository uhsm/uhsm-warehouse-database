﻿CREATE TABLE [dbo].[PA_Action_Taken_Against_Alert](
	[Action_Index] [int] NOT NULL,
	[Alert_Index] [int] NOT NULL,
	[Date_Taken] [datetime] NOT NULL,
	[Taken_By] [int] NOT NULL,
	[User_Entered_Text] [text] NOT NULL
)