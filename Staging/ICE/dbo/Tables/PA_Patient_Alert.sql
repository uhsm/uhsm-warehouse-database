﻿CREATE TABLE [dbo].[PA_Patient_Alert](
	[Alert_Index] [int] NOT NULL,
	[Type_Index] [int] NOT NULL,
	[Patient_Id_Key] [int] NOT NULL,
	[Expiry_Date] [smalldatetime] NULL,
	[Lives_Remaining] [tinyint] NOT NULL,
	[User_Entered_Text] [text] NULL,
	[Is_Active] [int] NOT NULL,
	[Last_Updated] [datetime] NULL
)