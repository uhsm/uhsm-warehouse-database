﻿CREATE TABLE [dbo].[PA_Patient_Alert_HL7_Trigger](
	[Trigger_Index] [int] NOT NULL,
	[Trigger_Set_Index] [int] NOT NULL,
	[Sequence] [int] NOT NULL,
	[HL7_Segment] [char](3) NOT NULL,
	[HL7_Field] [tinyint] NOT NULL,
	[HL7_Component] [tinyint] NULL,
	[HL7_Repetition] [smallint] NULL,
	[Expression] [varchar](255) NOT NULL
)