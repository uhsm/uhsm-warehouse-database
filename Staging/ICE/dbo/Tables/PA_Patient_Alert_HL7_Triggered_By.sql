﻿CREATE TABLE [dbo].[PA_Patient_Alert_HL7_Triggered_By](
	[Trigger_Set_Index] [int] NOT NULL,
	[HL7_Event_Type] [char](3) NOT NULL
)