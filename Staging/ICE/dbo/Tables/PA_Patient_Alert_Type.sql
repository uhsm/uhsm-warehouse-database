﻿CREATE TABLE [dbo].[PA_Patient_Alert_Type](
	[Alert_Type_Index] [int] NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](255) NOT NULL,
	[Description] [text] NULL,
	[Severity] [tinyint] NOT NULL,
	[Active] [bit] NOT NULL,
	[Days_Until_Expiry] [smallint] NULL,
	[Lives_Granted_Upon_Creation] [tinyint] NOT NULL,
	[Icon] [image] NULL,
	[Icon_Content_Type] [varchar](255) NULL,
	[Users_Can_Set] [bit] NOT NULL,
	[Users_Can_Clear] [bit] NOT NULL,
	[Users_Can_Action] [bit] NOT NULL,
	[Users_Can_Add_Text] [bit] NOT NULL,
	[Indicative_Of_High_Risk] [bit] NOT NULL
)