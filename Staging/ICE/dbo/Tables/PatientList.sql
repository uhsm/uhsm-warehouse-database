﻿CREATE TABLE [dbo].[PatientList](
	[ListIndex] [int] NOT NULL,
	[ListType] [tinyint] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Owner] [int] NOT NULL,
	[Refresh] [int] NOT NULL,
	[Timeout] [int] NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[LastUpdate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[MasterListIndex] [int] NULL,
	[IsPublic] [bit] NOT NULL,
	[ShowPendingRemovals] [bit] NOT NULL,
	[ClinicalFormTemplateIndex] [int] NULL,
	[Organisation] [varchar](6) NULL
)