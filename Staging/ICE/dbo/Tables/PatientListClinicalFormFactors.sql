﻿CREATE TABLE [dbo].[PatientListClinicalFormFactors](
	[PatientListIndex] [int] NOT NULL,
	[TemplateId] [int] NOT NULL,
	[FactorId] [int] NOT NULL,
	[IsExcluded] [bit] NOT NULL,
	[SortBy] [bit] NOT NULL,
	[ColumnWidth] [int] NOT NULL
)