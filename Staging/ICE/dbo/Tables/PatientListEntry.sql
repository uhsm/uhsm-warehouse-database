﻿CREATE TABLE [dbo].[PatientListEntry](
	[Id] [int] NOT NULL,
	[ListIndex] [int] NOT NULL,
	[PatientIndex] [int] NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[LastUpdate] [datetime] NULL,
	[DeleteTime] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[UserIndex] [int] NULL
)