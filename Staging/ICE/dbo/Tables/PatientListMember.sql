﻿CREATE TABLE [dbo].[PatientListMember](
	[Id] [int] NOT NULL,
	[ListIndex] [int] NOT NULL,
	[TeamIndex] [int] NULL,
	[UserIndex] [int] NULL,
	[CanWrite] [bit] NOT NULL
)