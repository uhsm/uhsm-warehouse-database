﻿CREATE TABLE [dbo].[PatientListQueryParameter](
	[ParameterIndex] [int] NOT NULL,
	[ListIndex] [int] NOT NULL,
	[ParameterType] [int] NOT NULL,
	[Value] [varchar](100) NULL
)