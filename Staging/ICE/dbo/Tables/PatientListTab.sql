﻿CREATE TABLE [dbo].[PatientListTab](
	[Id] [int] NOT NULL,
	[UserIndex] [int] NOT NULL,
	[ListIndex] [int] NULL,
	[Order] [int] NOT NULL
)