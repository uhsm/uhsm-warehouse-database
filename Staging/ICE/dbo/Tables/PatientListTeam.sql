﻿CREATE TABLE [dbo].[PatientListTeam](
	[TeamIndex] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Owner] [int] NOT NULL,
	[DateAdded] [datetime] NOT NULL
)