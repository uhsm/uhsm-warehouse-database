﻿CREATE TABLE [dbo].[PatientListTeamUser](
	[Id] [int] NOT NULL,
	[TeamIndex] [int] NOT NULL,
	[UserIndex] [int] NOT NULL,
	[Manager] [bit] NOT NULL,
	[ExpiryDate] [datetime] NULL
)