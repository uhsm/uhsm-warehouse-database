﻿CREATE TABLE [dbo].[PatientTemp](
	[patient_id_key] [int] NOT NULL,
	[Patient_Local_id] [varchar](30) NOT NULL,
	[PatMatchType] [smallint] NULL,
	[Surname] [varchar](35) NULL,
	[Forename] [varchar](35) NULL,
	[Date_Of_Birth] [datetime] NULL,
	[Sex] [smallint] NULL,
	[New_NHS_No] [varchar](10) NULL,
	[Pat_Addr_Line1] [varchar](35) NULL,
	[Pat_Postcode] [varchar](8) NULL,
	[Date_Last_Amended] [datetime] NULL,
	[Date_Added] [datetime] NULL
)