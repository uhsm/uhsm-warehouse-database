﻿CREATE TABLE [dbo].[Patient_Drugs](
	[Patient_Drug_Index] [int] NULL,
	[Patient_ID] [varchar](16) NULL,
	[Drug_Type] [varchar](3) NULL,
	[Drug_Code] [varchar](8) NULL,
	[Drug_description] [varchar](70) NULL,
	[DateTime_Administered] [datetime] NULL,
	[Drug_Dosage_Code] [varchar](8) NULL,
	[Drug_Dosage_description] [varchar](70) NULL,
	[Who_Administered_Code] [varchar](8) NULL,
	[Comment_Marker] [bit] NOT NULL,
	[Date_Added] [datetime] NULL
)