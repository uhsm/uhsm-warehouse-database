﻿CREATE TABLE [dbo].[Patient_Duplicates](
	[id] [int] NOT NULL,
	[GroupKey] [varchar](50) NOT NULL,
	[Patient_ID_Key] [int] NOT NULL,
	[Date_Added] [datetime] NOT NULL
)