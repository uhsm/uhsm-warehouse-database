﻿CREATE TABLE [dbo].[Patient_Extra_Data](
	[Patient_Index] [int] NOT NULL,
	[Key_Index] [int] NOT NULL,
	[Coded_Value] [varchar](50) NOT NULL
)