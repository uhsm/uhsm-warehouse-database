﻿CREATE TABLE [dbo].[Patient_Extra_Data_KeyValues_Lookups](
	[Key_Index] [int] NOT NULL,
	[Coded_Value] [varchar](50) NOT NULL,
	[Description] [varchar](100) NOT NULL
)