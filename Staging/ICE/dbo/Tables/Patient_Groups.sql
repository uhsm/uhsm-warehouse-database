﻿CREATE TABLE [dbo].[Patient_Groups](
	[GroupID] [int] NOT NULL,
	[GroupName] [varchar](50) NOT NULL,
	[Organisation] [varchar](6) NOT NULL
)