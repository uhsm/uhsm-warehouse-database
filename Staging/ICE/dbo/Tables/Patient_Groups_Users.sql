﻿CREATE TABLE [dbo].[Patient_Groups_Users](
	[GroupID] [int] NOT NULL,
	[UserIndex] [int] NOT NULL,
	[CanAdd] [bit] NOT NULL,
	[CanRemove] [bit] NOT NULL
)