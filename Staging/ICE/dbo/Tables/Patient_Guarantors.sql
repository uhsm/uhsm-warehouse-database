﻿CREATE TABLE [dbo].[Patient_Guarantors](
	[Patient_Index] [int] NOT NULL,
	[Surname] [varchar](50) NOT NULL,
	[Forename] [varchar](50) NOT NULL,
	[Date_Of_Birth] [smalldatetime] NULL,
	[Address_Line1] [varchar](50) NULL,
	[Address_Line2] [varchar](50) NULL,
	[Address_Line3] [varchar](50) NULL,
	[Address_Line4] [varchar](50) NULL,
	[Address_Line5] [varchar](50) NULL,
	[PostCode] [varchar](10) NULL,
	[Telephone] [varchar](20) NULL,
	[Relationship_Index] [int] NOT NULL
)