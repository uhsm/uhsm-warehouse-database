﻿CREATE TABLE [dbo].[Patient_Local_IDS](
	[Patient_Id_key] [int] NOT NULL,
	[Patient_Local_id] [varchar](30) NOT NULL,
	[Org_Code] [varchar](6) NULL,
	[Main_Identifier] [bit] NULL,
	[Active_Casenote] [bit] NOT NULL,
	[Hidden] [bit] NOT NULL,
	[HospitalNumber] [varchar](24) NULL,
	[Id] [int] NOT NULL,
	[Retired] [bit] NOT NULL,
	[Type_Id] [int] NULL
)