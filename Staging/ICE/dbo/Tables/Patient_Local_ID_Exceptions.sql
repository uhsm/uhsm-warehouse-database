﻿CREATE TABLE [dbo].[Patient_Local_ID_Exceptions](
	[Exception_ID] [int] NOT NULL,
	[Problem_Local_ID] [int] NULL,
	[Main_Local_ID] [int] NULL,
	[Date_Added] [datetime] NOT NULL,
	[Exception_Type] [int] NOT NULL,
	[TRXID] [varchar](20) NULL,
	[Date_Processed] [datetime] NULL,
	[Action_Type] [bit] NULL,
	[ApplicationID] [tinyint] NOT NULL
)