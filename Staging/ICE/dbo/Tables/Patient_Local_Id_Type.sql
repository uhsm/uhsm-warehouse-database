﻿CREATE TABLE [dbo].[Patient_Local_Id_Type](
	[Id] [int] NOT NULL,
	[Type_Code] [varchar](10) NOT NULL,
	[Active] [bit] NULL
)