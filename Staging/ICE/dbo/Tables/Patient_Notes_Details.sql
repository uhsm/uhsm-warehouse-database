﻿CREATE TABLE [dbo].[Patient_Notes_Details](
	[Notes_Key] [varchar](50) NULL,
	[Read_V2] [varchar](6) NULL,
	[Read_Term] [varchar](2) NULL,
	[Read_Rubric] [varchar](90) NULL,
	[Free_Text] [text] NULL,
	[Position] [int] NULL
)