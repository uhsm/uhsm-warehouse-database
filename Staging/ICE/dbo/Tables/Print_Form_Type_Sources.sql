﻿CREATE TABLE [dbo].[Print_Form_Type_Sources](
	[Source_Index] [int] NOT NULL,
	[Form_Type] [varchar](50) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Local_URL] [varchar](255) NOT NULL,
	[Suppress_Page_Title] [bit] NOT NULL
)