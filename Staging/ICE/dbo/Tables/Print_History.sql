﻿CREATE TABLE [dbo].[Print_History](
	[Print_Index] [int] NOT NULL,
	[Profile_Index] [int] NOT NULL,
	[Date_Added] [datetime] NOT NULL,
	[User] [int] NULL,
	[UserRole] [int] NULL,
	[UserRoleType] [char](1) NULL,
	[Location] [int] NULL,
	[LocationType] [int] NULL,
	[ServiceProvider] [int] NULL,
	[WorkstationID] [int] NULL,
	[ServerSide_PrinterID] [int] NULL,
	[ClientSide_ClientId] [int] NULL
)