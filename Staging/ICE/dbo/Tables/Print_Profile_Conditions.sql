﻿CREATE TABLE [dbo].[Print_Profile_Conditions](
	[Condition_Index] [int] NOT NULL,
	[Profile_Index] [int] NOT NULL,
	[Condition_Type] [varchar](10) NOT NULL,
	[Positivity] [bit] NOT NULL
)