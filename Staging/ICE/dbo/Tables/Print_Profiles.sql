﻿CREATE TABLE [dbo].[Print_Profiles](
	[Profile_Index] [int] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Form_Type_Index] [int] NOT NULL,
	[Raw_Print_Ind] [bit] NOT NULL,
	[Print_Per_Sample] [bit] NOT NULL,
	[Form_To_Print] [varchar](50) NULL,
	[Label_To_Print] [int] NULL,
	[Method] [char](2) NOT NULL,
	[Copies] [tinyint] NOT NULL,
	[Active] [bit] NOT NULL,
	[Locked_By_Admin] [bit] NOT NULL,
	[SuppressHeader] [bit] NOT NULL,
	[SuppressFooter] [bit] NOT NULL,
	[LabelPrint] [bit] NOT NULL
)