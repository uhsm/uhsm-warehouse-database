﻿CREATE TABLE [dbo].[Profile](
	[Profile_Index] [int] NOT NULL,
	[Profile_Name] [varchar](60) NOT NULL,
	[Created] [datetime] NOT NULL,
	[Notes] [varchar](200) NULL
)