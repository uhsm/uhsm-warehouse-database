﻿CREATE TABLE [dbo].[Read_Snomed_Map](
	[Map_Index] [int] NOT NULL,
	[Read_Code] [varchar](5) NOT NULL,
	[Snomed_Code] [varchar](40) NOT NULL
)