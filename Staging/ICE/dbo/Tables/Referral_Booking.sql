﻿CREATE TABLE [dbo].[Referral_Booking](
	[id] [int] NOT NULL,
	[specialty_id] [smallint] NOT NULL,
	[clinic_id] [varchar](10) NOT NULL,
	[clinician_id] [varchar](10) NOT NULL,
	[appoint_dt] [datetime] NOT NULL,
	[appoint_slot] [tinyint] NOT NULL,
	[patient_cat] [tinyint] NOT NULL,
	[transport] [varchar](1) NOT NULL,
	[priority] [varchar](1) NOT NULL,
	[appointment_id] [int] NOT NULL,
	[episode_id] [tinyint] NOT NULL,
	[email] [tinyint] NOT NULL,
	[create_dt] [datetime] NOT NULL
)