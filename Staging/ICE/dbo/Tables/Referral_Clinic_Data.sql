﻿CREATE TABLE [dbo].[Referral_Clinic_Data](
	[clinic_id] [varchar](10) NOT NULL,
	[description] [varchar](100) NOT NULL,
	[address_line1] [varchar](50) NULL,
	[address_line2] [varchar](50) NULL,
	[address_line3] [varchar](50) NULL,
	[post_cd] [varchar](10) NULL,
	[telephone] [varchar](30) NULL
)