﻿CREATE TABLE [dbo].[Referral_Diagnosis_Codes](
	[id] [int] NOT NULL,
	[diagnosis_id] [varchar](5) NOT NULL,
	[specialty_id] [smallint] NOT NULL,
	[description] [varchar](50) NOT NULL,
	[active] [tinyint] NOT NULL
)