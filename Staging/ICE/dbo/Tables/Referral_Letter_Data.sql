﻿CREATE TABLE [dbo].[Referral_Letter_Data](
	[id] [int] NOT NULL,
	[specialty_id] [int] NOT NULL,
	[clinic_id] [varchar](10) NULL,
	[header] [varchar](4000) NULL,
	[footer] [varchar](4000) NULL
)