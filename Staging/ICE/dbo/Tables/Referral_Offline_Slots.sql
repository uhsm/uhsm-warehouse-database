﻿CREATE TABLE [dbo].[Referral_Offline_Slots](
	[id] [int] NOT NULL,
	[clinic_id] [varchar](10) NOT NULL,
	[appoint_dt] [datetime] NOT NULL,
	[priority] [varchar](1) NOT NULL,
	[slot] [tinyint] NOT NULL,
	[status] [tinyint] NOT NULL
)