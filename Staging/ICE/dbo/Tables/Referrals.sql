﻿CREATE TABLE [dbo].[Referrals](
	[id] [int] NOT NULL,
	[referral_id] [int] NOT NULL,
	[patient_id] [int] NOT NULL,
	[referrer_id] [varchar](10) NOT NULL,
	[role] [tinyint] NOT NULL,
	[organisation_code] [varchar](6) NOT NULL,
	[status] [smallint] NOT NULL,
	[guid] [uniqueidentifier] NULL,
	[create_dt] [datetime] NOT NULL
)