﻿CREATE TABLE [dbo].[Report_External_Resources](
	[External_Resource_Index] [int] NOT NULL,
	[Service_Report_Index] [int] NOT NULL,
	[Resource_Name] [varchar](50) NOT NULL,
	[Resource_URL] [varchar](256) NOT NULL,
	[Date_Added] [datetime] NOT NULL,
	[HideFileLocation] [bit] NOT NULL
)