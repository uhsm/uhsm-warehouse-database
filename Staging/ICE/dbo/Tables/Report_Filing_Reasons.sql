﻿CREATE TABLE [dbo].[Report_Filing_Reasons](
	[Reason_Index] [int] NOT NULL,
	[Reason_Code] [varchar](5) NOT NULL,
	[Reason] [varchar](50) NOT NULL,
	[User_Selectable] [bit] NOT NULL,
	[Deceased_Reason] [bit] NOT NULL,
	[Active] [bit] NOT NULL,
	[Default] [bit] NOT NULL,
	[FreeText] [bit] NOT NULL,
	[Non_Editable] [bit] NOT NULL
)