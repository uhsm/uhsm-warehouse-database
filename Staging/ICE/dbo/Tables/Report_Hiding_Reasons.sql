﻿CREATE TABLE [dbo].[Report_Hiding_Reasons](
	[Reason_Index] [int] NOT NULL,
	[Reason_Code] [varchar](5) NOT NULL,
	[Reason] [varchar](50) NOT NULL,
	[User_Selectable] [bit] NOT NULL,
	[Interim_Replaced_Reason] [bit] NOT NULL,
	[Final_Replaced_Reason] [bit] NOT NULL,
	[VIP_Hide_Reason] [bit] NOT NULL,
	[Active] [bit] NOT NULL,
	[Default] [bit] NOT NULL,
	[Display_On_Linked_Reports] [bit] NOT NULL
)