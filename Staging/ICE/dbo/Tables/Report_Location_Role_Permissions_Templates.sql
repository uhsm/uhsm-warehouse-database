﻿CREATE TABLE [dbo].[Report_Location_Role_Permissions_Templates](
	[Location_Index] [int] NOT NULL,
	[Role_Index] [tinyint] NOT NULL,
	[Permitted] [bit] NOT NULL
)