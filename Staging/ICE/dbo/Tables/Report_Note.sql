﻿CREATE TABLE [dbo].[Report_Note](
	[Id] [int] NOT NULL,
	[Master_Id] [int] NULL,
	[Report_Index] [int] NOT NULL,
	[User_Index] [int] NOT NULL,
	[Text] [varchar](500) NOT NULL,
	[Create_Date] [datetime] NOT NULL,
	[Is_Deleted] [bit] NOT NULL
)