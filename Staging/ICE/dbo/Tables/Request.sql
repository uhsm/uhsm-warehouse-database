﻿CREATE TABLE [dbo].[Request](
	[Request_Id] [int] NOT NULL,
	[Date_Added] [datetime] NULL,
	[Interop_Index] [int] NULL,
	[GlobalClinicalDetail] [varchar](2048) NULL
)