﻿CREATE TABLE [dbo].[Request_Bloodbank_Products](
	[Test_Index] [int] NOT NULL,
	[Product_Name_Index] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[Variable] [bit] NOT NULL,
	[Required] [bit] NOT NULL,
	[Mandatory] [bit] NOT NULL,
	[Reason_Mandatory] [bit] NOT NULL
)