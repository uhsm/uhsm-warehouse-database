﻿CREATE TABLE [dbo].[Request_Forms](
	[Abstract_Name] [varchar](10) NOT NULL,
	[Form] [varchar](50) NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[LabelForm] [bit] NOT NULL
)