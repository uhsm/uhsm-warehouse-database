﻿CREATE TABLE [dbo].[Request_Forms_Tailoring](
	[Abstract_Name] [varchar](10) NOT NULL,
	[Tailor_Field] [varchar](100) NOT NULL,
	[Tailor_Type_Code] [varchar](20) NOT NULL,
	[Tailoring] [text] NOT NULL,
	[ConditionField] [varchar](100) NULL,
	[ConditionOperator] [varchar](100) NULL
)