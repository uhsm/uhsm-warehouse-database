﻿CREATE TABLE [dbo].[Request_Labels](
	[Label_Index] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Label] [text] NOT NULL,
	[LabelForm] [bit] NOT NULL,
	[Form] [varchar](10) NULL
)