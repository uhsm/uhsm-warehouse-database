﻿CREATE TABLE [dbo].[Request_Page_TitleBars](
	[Page_Id] [int] NOT NULL,
	[Page_Type] [tinyint] NOT NULL,
	[Page_Position_Index] [int] NOT NULL,
	[User_Index] [int] NOT NULL,
	[Text] [varchar](50) NOT NULL,
	[Colour] [int] NULL,
	[BackColour] [int] NULL
)