﻿CREATE TABLE [dbo].[Request_Phlebotomy](
	[Phlebotomy_Index] [int] NOT NULL,
	[Daily_Cutoff_Time] [smalldatetime] NULL,
	[Daily_Display_Time] [smalldatetime] NULL,
	[Weekend_Cutoff_Time] [smalldatetime] NULL,
	[Weekend_Display_Time] [smalldatetime] NULL,
	[Date_Added] [smalldatetime] NULL,
	[Organisation] [varchar](6) NOT NULL
)