﻿CREATE TABLE [dbo].[Request_Picklist](
	[Picklist_Index] [int] NOT NULL,
	[Picklist_Name] [varchar](50) NOT NULL,
	[Multichoice] [bit] NOT NULL,
	[Organisation] [varchar](6) NOT NULL
)