﻿CREATE TABLE [dbo].[Request_Picklist_Data](
	[Picklist_Index] [int] NOT NULL,
	[Picklist_Value] [varchar](50) NOT NULL,
	[Picklist_Item_Index] [int] NOT NULL,
	[Barcode_Value] [varchar](50) NOT NULL,
	[order_index] [int] NOT NULL,
	[Select_Action_Type] [int] NULL,
	[Default] [bit] NULL
)