﻿CREATE TABLE [dbo].[Request_Sample_Panels](
	[Sample_Panel_ID] [int] NOT NULL,
	[Panel_Name] [varchar](50) NOT NULL,
	[Danger] [bit] NOT NULL,
	[organisation] [varchar](6) NOT NULL,
	[Clinical_Details_Display] [bit] NOT NULL,
	[Freetext_Maximum_Length] [smallint] NULL
)