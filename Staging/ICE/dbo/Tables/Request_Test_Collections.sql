﻿CREATE TABLE [dbo].[Request_Test_Collections](
	[Collection_Id] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[BackColour] [int] NULL,
	[Help_Text] [varchar](140) NULL,
	[Help_Colour] [int] NULL,
	[Help_BackColour] [int] NULL,
	[Organisation] [varchar](6) NULL,
	[Exclude_Personal_Panel] [bit] NOT NULL,
	[Exclude_Select_All] [bit] NOT NULL,
	[Exclude_DeSelect_All] [bit] NOT NULL,
	[Select_All_By_Default] [bit] NOT NULL
)