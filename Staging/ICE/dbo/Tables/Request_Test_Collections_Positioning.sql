﻿CREATE TABLE [dbo].[Request_Test_Collections_Positioning](
	[Collection_Id] [int] NOT NULL,
	[Page_Id] [int] NOT NULL,
	[Page_Type] [tinyint] NOT NULL,
	[Page_Position_Index] [int] NOT NULL
)