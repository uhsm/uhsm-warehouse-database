﻿CREATE TABLE [dbo].[Request_Tests_CPT_Map](
	[Mapping_Index] [int] NOT NULL,
	[Test_Index] [int] NOT NULL,
	[CPT_Code] [varchar](5) NOT NULL,
	[Active_Date] [smalldatetime] NOT NULL,
	[Active_Marker] [bit] NOT NULL,
	[Inactive_Date] [smalldatetime] NULL
)