﻿CREATE TABLE [dbo].[Request_Worklists](
	[Index] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[Processed] [bit] NOT NULL,
	[DateProcessed] [datetime] NULL,
	[DateEdited] [datetime] NULL,
	[UserIndex] [int] NOT NULL,
	[Comments] [text] NULL,
	[Active] [bit] NOT NULL,
	[organisation] [varchar](6) NOT NULL
)