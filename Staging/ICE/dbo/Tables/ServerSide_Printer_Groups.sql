﻿CREATE TABLE [dbo].[ServerSide_Printer_Groups](
	[Group_Index] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Active] [bit] NOT NULL
)