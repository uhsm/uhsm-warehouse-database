﻿CREATE TABLE [dbo].[ServiceUserReportingProfile](
	[Index] [int] NOT NULL,
	[UserIndex] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL
)