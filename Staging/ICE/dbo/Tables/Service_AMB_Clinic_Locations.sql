﻿CREATE TABLE [dbo].[Service_AMB_Clinic_Locations](
	[LocationID] [int] NULL,
	[DestinationID] [int] NULL,
	[ClinicID] [int] NULL,
	[LocationName] [varchar](50) NULL
)