﻿CREATE TABLE [dbo].[Service_AMB_Destinations](
	[DestinationID] [int] NOT NULL,
	[DestinationName] [varchar](50) NULL,
	[Place] [varchar](50) NULL,
	[Street1] [varchar](50) NULL,
	[Street2] [varchar](50) NULL,
	[Town] [varchar](50) NULL,
	[County] [varchar](50) NULL,
	[PostCode] [varchar](9) NULL,
	[Zone] [varchar](10) NULL,
	[TelNo] [varchar](12) NULL,
	[DHA] [int] NULL,
	[Contract] [int] NULL,
	[Private] [bit] NOT NULL,
	[ManUnit] [int] NULL,
	[Comments] [varchar](50) NULL,
	[DisplayOrder] [int] NULL
)