﻿CREATE TABLE [dbo].[Service_AMB_Index](
	[Case_Index] [int] NOT NULL,
	[Patient_ID] [varchar](50) NULL,
	[Hospital_Number] [varchar](50) NULL,
	[GP_Index] [int] NULL,
	[Cons_Nat_Code] [varchar](50) NULL,
	[Work_Tel_No] [varchar](50) NULL,
	[School] [varchar](255) NULL,
	[Con_Addr_Line1] [varchar](50) NULL,
	[Con_Addr_Line2] [varchar](50) NULL,
	[Con_Addr_Line3] [varchar](50) NULL,
	[Con_Addr_Line4] [varchar](50) NULL,
	[Con_Postcode] [varchar](50) NULL,
	[Active] [bit] NOT NULL,
	[Date_Last_Amended] [datetime] NULL,
	[Date_Added] [datetime] NULL,
	[Special_Admin] [varchar](255) NULL
)