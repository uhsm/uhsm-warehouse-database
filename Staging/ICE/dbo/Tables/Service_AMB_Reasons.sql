﻿CREATE TABLE [dbo].[Service_AMB_Reasons](
	[Reason_Index] [int] NOT NULL,
	[Reason_Type] [varchar](50) NULL,
	[Reason_Code] [varchar](50) NULL,
	[Reason_Order] [int] NULL,
	[Reason_Text] [varchar](255) NULL,
	[Active] [bit] NOT NULL
)