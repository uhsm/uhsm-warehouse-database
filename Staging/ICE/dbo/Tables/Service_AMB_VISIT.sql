﻿CREATE TABLE [dbo].[Service_AMB_VISIT](
	[Visit_Index] [int] NOT NULL,
	[Case_Index] [int] NULL,
	[Visit_Date] [datetime] NULL,
	[Date_Added] [datetime] NULL
)