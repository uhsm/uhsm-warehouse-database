﻿CREATE TABLE [dbo].[Service_AMB_Visit_Types](
	[VisitTypeCode] [varchar](4) NOT NULL,
	[TypeText] [varchar](20) NULL,
	[AvailDays] [varchar](17) NULL,
	[EarliestTime] [smalldatetime] NULL,
	[LatestTime] [smalldatetime] NULL
)