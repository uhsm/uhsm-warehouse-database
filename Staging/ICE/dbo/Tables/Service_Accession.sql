﻿CREATE TABLE [dbo].[Service_Accession](
	[Accession_Index] [int] NOT NULL,
	[Accession_Type] [varchar](50) NULL,
	[Provider_ID] [int] NULL,
	[Accession_Prefix] [varchar](50) NULL,
	[Accession_Number] [int] NULL,
	[Accession_Minimum] [int] NULL,
	[Accession_Maximum] [int] NULL,
	[Allow_Reuse] [bit] NOT NULL,
	[Location_Index] [int] NOT NULL,
	[organisation] [varchar](6) NULL,
	[DayOfWeek] [tinyint] NULL,
	[RangeForSample] [bit] NOT NULL,
	[Site_Code] [varchar](30) NULL
)