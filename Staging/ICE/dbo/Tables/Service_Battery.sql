﻿CREATE TABLE [dbo].[Service_Battery](
	[Service_Battery_Index] [int] NOT NULL,
	[Service_Battery_Code] [varchar](10) NULL,
	[Service_Battery_Name] [varchar](30) NULL,
	[Service_Battery_OrderInd] [bit] NOT NULL
)