﻿CREATE TABLE [dbo].[Service_Comments](
	[Comment_Index] [int] NOT NULL,
	[Comment_Orig_Index] [int] NULL,
	[Comment_Orig_Type] [varchar](4) NULL,
	[Comment] [varchar](140) NULL
)