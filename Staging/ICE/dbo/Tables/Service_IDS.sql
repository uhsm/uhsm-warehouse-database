﻿CREATE TABLE [dbo].[Service_IDS](
	[Service_ID_Index] [int] NOT NULL,
	[Service_Request_Index] [int] NOT NULL,
	[Lab_Number] [varchar](35) NULL,
	[Organisation] [varchar](6) NOT NULL
)