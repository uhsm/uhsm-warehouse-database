﻿CREATE TABLE [dbo].[Service_ImpExp_Acks](
	[Service_ImpExp_ID] [int] NOT NULL,
	[Service_Report_Index] [int] NOT NULL,
	[Ack_Status] [smallint] NOT NULL
)