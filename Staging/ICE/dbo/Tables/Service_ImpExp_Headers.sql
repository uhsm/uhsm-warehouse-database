﻿CREATE TABLE [dbo].[Service_ImpExp_Headers](
	[Service_ImpExp_ID] [int] NOT NULL,
	[Organisation] [varchar](6) NULL,
	[Trader_Code] [varchar](15) NULL,
	[Imp_Exp] [char](1) NULL,
	[ImpExp_File] [varchar](120) NOT NULL,
	[Provider_ID] [int] NULL,
	[Service_Type] [int] NULL,
	[Messages] [int] NULL,
	[Date_Added] [datetime] NULL,
	[Control_Ref] [varchar](14) NULL,
	[Error_Found] [int] NOT NULL,
	[Comment_Marker] [bit] NOT NULL,
	[Status_Flag] [int] NULL,
	[EDI_LTS_Index] [int] NULL,
	[Warning_Flag] [bit] NOT NULL,
	[File_Identifier] [varchar](20) NULL,
	[Header_Status] [int] NULL,
	[Comment_Status] [int] NULL,
	[Last_Comment_Added] [datetime] NULL,
	[EDI_Recipient_Id] [varchar](6) NULL,
	[DTS_TypeIndex] [int] NULL
)