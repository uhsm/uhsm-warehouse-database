﻿CREATE TABLE [dbo].[Service_Invest_Comments](
	[Investigation_Comment_Index] [int] NOT NULL,
	[Service_Investigation_Index] [int] NULL,
	[Service_Investigation_Comment] [varchar](140) NULL,
	[Comment_Type] [varchar](2) NULL,
	[Date_Added] [datetime] NULL
)