﻿CREATE TABLE [dbo].[Service_Legends](
	[Legend_No] [varchar](20) NOT NULL,
	[Legend_Text] [varchar](255) NULL,
	[Legend_Form] [varchar](255) NULL,
	[Legend_Event] [varchar](255) NULL,
	[Legend_Value] [varchar](255) NULL
)