﻿CREATE TABLE [dbo].[Service_Phlebotomy](
	[Phlebotomy_Index] [int] NOT NULL,
	[Organisation_Code] [varchar](6) NULL,
	[Provider_ID] [int] NULL,
	[Daily_Cutoff_Time] [datetime] NULL,
	[Weekend_Cutoff_Time] [datetime] NULL,
	[Access_Code] [varchar](10) NULL,
	[Date_Added] [datetime] NULL
)