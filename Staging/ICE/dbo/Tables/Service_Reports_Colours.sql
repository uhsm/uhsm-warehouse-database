﻿CREATE TABLE [dbo].[Service_Reports_Colours](
	[Report_Colour_Index] [int] NOT NULL,
	[Report_Type] [varchar](5) NOT NULL,
	[Report_Colour] [varchar](25) NULL
)