﻿CREATE TABLE [dbo].[Service_Request_Information](
	[Service_Request_Id] [int] NOT NULL,
	[Information_Type] [char](1) NOT NULL,
	[sequence_id] [tinyint] NOT NULL,
	[parent_sequence_id] [tinyint] NULL,
	[remote_key] [int] NOT NULL,
	[value] [varchar](2048) NULL,
	[Date_Added] [datetime] NULL,
	[User_Index] [int] NULL
)