﻿CREATE TABLE [dbo].[Service_Request_Test](
	[Service_Request_Id] [int] NOT NULL,
	[Test_Id] [int] NOT NULL,
	[Test_Status] [tinyint] NOT NULL,
	[Service_Request_Test_Index] [int] NOT NULL,
	[ABN_Reqd] [bit] NULL,
	[Last_Updated] [datetime] NULL,
	[Repeat_Reason] [varchar](1000) NULL,
	[PriceCode] [money] NULL
)