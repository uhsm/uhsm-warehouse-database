﻿CREATE TABLE [dbo].[Service_Requests_Outstanding](
	[SRO_Index] [int] NOT NULL,
	[SRO_Patient_Local_Id] [varchar](30) NULL,
	[SRO_Request_Id] [varchar](35) NULL
)