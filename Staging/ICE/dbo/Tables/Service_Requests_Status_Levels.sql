﻿CREATE TABLE [dbo].[Service_Requests_Status_Levels](
	[Status_Index] [tinyint] NOT NULL,
	[Status] [varchar](3) NOT NULL,
	[Description] [varchar](50) NOT NULL
)