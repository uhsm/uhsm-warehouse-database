﻿CREATE TABLE [dbo].[Service_Sample_Codes](
	[Sample_Description] [varchar](70) NOT NULL,
	[Type_Code] [varchar](5) NOT NULL,
	[Collection_Code] [varchar](5) NULL,
	[AnatomicalOrigin_Code] [varchar](5) NULL,
	[Residual_Text] [varchar](30) NULL,
	[Active] [bit] NOT NULL
)