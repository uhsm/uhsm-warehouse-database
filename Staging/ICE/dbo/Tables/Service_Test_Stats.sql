﻿CREATE TABLE [dbo].[Service_Test_Stats](
	[Test_Stat_Index] [int] NOT NULL,
	[Service_Request_Index] [int] NOT NULL,
	[Order_Code] [varchar](8) NULL,
	[Billing_Month] [smallint] NULL,
	[Billing_Year] [varchar](9) NULL
)