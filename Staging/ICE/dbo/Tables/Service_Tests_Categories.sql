﻿CREATE TABLE [dbo].[Service_Tests_Categories](
	[Category_Index] [int] NOT NULL,
	[Category_Caption] [nvarchar](50) NULL,
	[Category_Order] [int] NULL,
	[Category_OrgCode] [nvarchar](6) NULL
)