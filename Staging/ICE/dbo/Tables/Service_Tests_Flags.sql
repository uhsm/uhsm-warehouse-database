﻿CREATE TABLE [dbo].[Service_Tests_Flags](
	[Flag_Index] [int] NOT NULL,
	[Flag_Caption] [varchar](30) NULL,
	[Flag_Order] [int] NULL,
	[Flag_OrgCode] [varchar](6) NULL
)