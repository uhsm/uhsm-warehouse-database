﻿CREATE TABLE [dbo].[Service_Tests_PerClient](
	[ClientTest_Index] [int] NOT NULL,
	[ClientTest_Location_Code] [varchar](16) NOT NULL,
	[ClientTest_Position] [int] NULL,
	[ClientTest_Caption] [varchar](25) NULL,
	[ClientTest_Colour] [varchar](25) NULL,
	[Provider_ID] [int] NULL,
	[ClientTest_Help] [varchar](65) NULL,
	[ClientTest_Help_Backcolour] [varchar](25) NULL,
	[ClientTest_Tests] [varchar](200) NULL,
	[ClientTest_TextString] [varchar](50) NULL,
	[Enabled] [bit] NOT NULL,
	[Date_Added] [datetime] NULL
)