﻿CREATE TABLE [dbo].[Service_Tests_SubCats](
	[SubCat_Index] [int] NOT NULL,
	[MainCat_Index] [int] NULL,
	[SubCat_Caption] [varchar](14) NULL,
	[SubCat_Order] [int] NULL,
	[SubCat_OrgCode] [varchar](6) NULL
)