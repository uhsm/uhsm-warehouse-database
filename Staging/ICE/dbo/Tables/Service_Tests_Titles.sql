﻿CREATE TABLE [dbo].[Service_Tests_Titles](
	[Title_Index] [int] NOT NULL,
	[Organisation_Code] [varchar](6) NOT NULL,
	[Title_Position] [varchar](50) NULL,
	[Title_Text] [varchar](50) NULL
)