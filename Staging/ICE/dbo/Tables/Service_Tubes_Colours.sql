﻿CREATE TABLE [dbo].[Service_Tubes_Colours](
	[Colour_Index] [int] NOT NULL,
	[Colour_Name] [varchar](20) NULL,
	[Colour_Code] [varchar](11) NULL,
	[Date_Added] [datetime] NULL
)