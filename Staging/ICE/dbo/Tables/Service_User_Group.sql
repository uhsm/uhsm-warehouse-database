﻿CREATE TABLE [dbo].[Service_User_Group](
	[User_GroupID] [int] NOT NULL,
	[Group_Name] [varchar](35) NULL,
	[Group_Description] [varchar](50) NULL,
	[Security_Levels] [varchar](50) NULL
)