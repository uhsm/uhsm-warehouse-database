﻿CREATE TABLE [dbo].[Service_User_Identity_Tokens](
	[Token_ID] [int] NOT NULL,
	[User_Index] [int] NOT NULL,
	[Token_Type] [char](3) NULL,
	[Token_Identifier] [varchar](50) NULL,
	[PIN] [smallint] NOT NULL,
	[Expiry_Date] [smalldatetime] NOT NULL
)