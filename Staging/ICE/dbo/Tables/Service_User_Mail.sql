﻿CREATE TABLE [dbo].[Service_User_Mail](
	[Mail_ID] [int] NOT NULL,
	[Mail_From] [varchar](10) NULL,
	[Mail_To] [varchar](10) NULL,
	[Mail_Subject] [varchar](255) NULL,
	[Mail_Text] [varchar](255) NULL,
	[Mail_Read] [bit] NOT NULL,
	[Mail_Expiry] [datetime] NULL,
	[Date_Added] [datetime] NULL
)