﻿CREATE TABLE [dbo].[Service_User_Permissions](
	[Permission_Index] [int] NOT NULL,
	[Permission_Type] [char](1) NOT NULL,
	[Remote_Index] [int] NOT NULL,
	[Permission_Class] [varchar](10) NOT NULL,
	[Permission_Subclass] [varchar](20) NULL,
	[Permission_Value] [varchar](30) NULL
)