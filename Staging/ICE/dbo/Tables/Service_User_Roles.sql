﻿CREATE TABLE [dbo].[Service_User_Roles](
	[Role_Index] [tinyint] NOT NULL,
	[Role_Name] [varchar](20) NOT NULL,
	[Role_Type] [char](1) NOT NULL,
	[AD_Security_Group] [varchar](255) NULL,
	[Demand_LR] [bit] NOT NULL,
	[NumRestrictedLocsToDisplay] [tinyint] NOT NULL,
	[ApplyLocationRestrictions] [bit] NOT NULL,
	[ApplyLocationRestrictionsToSearch] [bit] NOT NULL,
	[MaximumLoginAttempts] [tinyint] NULL,
	[Active] [bit] NULL
)