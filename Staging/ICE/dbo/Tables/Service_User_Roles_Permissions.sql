﻿CREATE TABLE [dbo].[Service_User_Roles_Permissions](
	[Index] [int] NOT NULL,
	[RoleIndex] [int] NOT NULL,
	[Value] [int] NOT NULL,
	[DateAdded] [datetime] NULL,
	[AddedBy] [int] NOT NULL,
	[Class] [nchar](40) NULL
)