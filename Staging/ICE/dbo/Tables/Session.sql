﻿CREATE TABLE [dbo].[Session](
	[ID] [char](32) NOT NULL,
	[Active] [bit] NOT NULL,
	[LastAccessed] [datetime] NOT NULL,
	[LastQualified] [datetime] NOT NULL,
	[Data] [text] NOT NULL,
	[TimeoutMinutes] [int] NOT NULL
)