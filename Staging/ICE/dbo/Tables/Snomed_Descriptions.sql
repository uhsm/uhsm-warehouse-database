﻿CREATE TABLE [dbo].[Snomed_Descriptions](
	[DescriptionID] [varchar](10) NOT NULL,
	[DescriptionStatus] [tinyint] NOT NULL,
	[ConceptId] [varchar](9) NOT NULL,
	[Term] [varchar](255) NOT NULL,
	[InitialCapitalStatus] [tinyint] NOT NULL,
	[DescriptionType] [tinyint] NOT NULL,
	[LanguageCode] [varchar](5) NOT NULL
)