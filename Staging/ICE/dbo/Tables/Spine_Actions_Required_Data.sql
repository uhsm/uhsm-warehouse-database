﻿CREATE TABLE [dbo].[Spine_Actions_Required_Data](
	[MessageGUID] [uniqueidentifier] NOT NULL,
	[Key] [varchar](20) NOT NULL,
	[Value] [varchar](255) NOT NULL
)