﻿CREATE TABLE [dbo].[Spine_Clinic_Lookup](
	[ID] [int] NOT NULL,
	[ClinicCode] [varchar](20) NOT NULL,
	[Specialty] [smallint] NULL
)