﻿CREATE TABLE [dbo].[Spine_Clinic_Slot_Patterns](
	[Clinic_ID] [int] NOT NULL,
	[DayOfWeek] [tinyint] NOT NULL,
	[StartTime] [char](5) NOT NULL,
	[EndTime] [char](5) NOT NULL,
	[Sequence] [tinyint] NOT NULL,
	[Urgent] [bit] NOT NULL
)