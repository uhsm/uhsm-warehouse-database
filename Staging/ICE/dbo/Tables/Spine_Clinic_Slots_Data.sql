﻿CREATE TABLE [dbo].[Spine_Clinic_Slots_Data](
	[USRN] [uniqueidentifier] NOT NULL,
	[DataKey] [varchar](20) NOT NULL,
	[DataValue] [varchar](100) NOT NULL,
	[BookingInformation] [bit] NOT NULL
)