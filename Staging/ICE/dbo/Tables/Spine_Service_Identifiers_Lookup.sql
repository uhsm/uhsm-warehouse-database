﻿CREATE TABLE [dbo].[Spine_Service_Identifiers_Lookup](
	[id] [int] NOT NULL,
	[Service_Identifier_ID] [varchar](20) NOT NULL,
	[Clinic_Lookup_ID] [int] NOT NULL
)