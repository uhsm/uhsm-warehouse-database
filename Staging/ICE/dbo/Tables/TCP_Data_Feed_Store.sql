﻿CREATE TABLE [dbo].[TCP_Data_Feed_Store](
	[id] [int] NOT NULL,
	[server] [varchar](20) NOT NULL,
	[feedname] [varchar](100) NOT NULL,
	[host] [varchar](100) NOT NULL,
	[port] [int] NOT NULL,
	[feed] [text] NOT NULL
)