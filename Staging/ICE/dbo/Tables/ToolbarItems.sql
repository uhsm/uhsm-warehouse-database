﻿CREATE TABLE [dbo].[ToolbarItems](
	[ItemID] [int] NOT NULL,
	[PanelName] [varchar](25) NOT NULL,
	[ItemURL] [varchar](200) NULL,
	[ItemRequiresPatient] [bit] NULL,
	[EditPatient] [bit] NULL,
	[Position] [tinyint] NULL,
	[ApplicationID] [int] NULL,
	[Local_Admin] [bit] NULL
)