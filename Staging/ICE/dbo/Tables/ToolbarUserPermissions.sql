﻿CREATE TABLE [dbo].[ToolbarUserPermissions](
	[UserName] [varchar](25) NOT NULL,
	[ToolbarItemID] [int] NOT NULL,
	[PermissionType] [char](1) NOT NULL
)