﻿CREATE TABLE [dbo].[Transaction](
	[TransactionID] [uniqueidentifier] NOT NULL,
	[ParentID] [uniqueidentifier] NULL,
	[ApplicationID] [int] NOT NULL,
	[StartServer] [varchar](20) NULL,
	[StartTimestamp] [datetime] NULL,
	[EndServer] [varchar](20) NULL,
	[EndTimestamp] [datetime] NULL,
	[UserId] [int] NOT NULL,
	[HasArchived_Data] [bit] NOT NULL
)