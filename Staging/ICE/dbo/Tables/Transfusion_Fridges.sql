﻿CREATE TABLE [dbo].[Transfusion_Fridges](
	[Fridge_Index] [smallint] NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Location_Index] [int] NULL,
	[Distribution_Indicator] [bit] NOT NULL,
	[Active] [bit] NOT NULL,
	[Has_Magnetic_Lock] [bit] NOT NULL,
	[IP_Address] [varchar](15) NULL,
	[Port_Number] [int] NULL,
	[Door_ID] [tinyint] NULL,
	[Fridge_Type_Index] [int] NOT NULL
)