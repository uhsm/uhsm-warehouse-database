﻿CREATE TABLE [dbo].[UHSM_IDType](
	[Master_Patient_Id_Key] [int] NOT NULL,
	[Non-PAS_ID - hidden/retired] [int] NULL,
	[Non-PAS_ID - major] [int] NULL,
	[Non-PAS_ID - minor] [int] NULL,
	[PAS_ID - hidden/retired] [int] NULL,
	[PAS_ID - major] [int] NULL,
	[PAS_ID - minor] [int] NULL
)