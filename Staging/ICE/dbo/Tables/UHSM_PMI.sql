﻿CREATE TABLE [dbo].[UHSM_PMI](
	[FACIL_ID] [nvarchar](50) NULL,
	[SURNAME] [nvarchar](50) NULL,
	[GIVEN_NAME] [nvarchar](50) NULL,
	[SECOND_NAME] [nvarchar](50) NULL,
	[PREFIX] [nvarchar](50) NULL,
	[DOB] [datetime] NULL,
	[SEX] [nvarchar](50) NULL,
	[ADDRESS_LINE1] [nvarchar](50) NULL,
	[ADDRESS_LINE2] [nvarchar](50) NULL,
	[ADDRESS_LINE3] [nvarchar](50) NULL,
	[ADDRESS_LINE4] [nvarchar](50) NULL,
	[POSTCODE] [nvarchar](50) NULL,
	[NHS_NUMBER] [nvarchar](50) NULL,
	[DEATH_INDICATOR] [nvarchar](50) NULL,
	[NHS_NUMBER_VERIFIED] [nvarchar](50) NULL
)