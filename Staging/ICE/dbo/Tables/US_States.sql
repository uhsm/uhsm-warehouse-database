﻿CREATE TABLE [dbo].[US_States](
	[State_Index] [smallint] NOT NULL,
	[State_Code] [varchar](2) NOT NULL,
	[State] [varchar](50) NOT NULL
)