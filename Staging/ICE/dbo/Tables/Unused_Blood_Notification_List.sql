﻿CREATE TABLE [dbo].[Unused_Blood_Notification_List](
	[Notification_Index] [int] NOT NULL,
	[User_Index] [int] NOT NULL,
	[Hours] [smallint] NOT NULL,
	[Alert_Blood_Group_Change] [bit] NOT NULL
)