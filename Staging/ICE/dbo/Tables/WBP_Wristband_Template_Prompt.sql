﻿CREATE TABLE [dbo].[WBP_Wristband_Template_Prompt](
	[Template_Prompt_Index] [int] NOT NULL,
	[Template_Index] [int] NOT NULL,
	[Sequence] [tinyint] NOT NULL,
	[Display_Text] [varchar](255) NOT NULL
)