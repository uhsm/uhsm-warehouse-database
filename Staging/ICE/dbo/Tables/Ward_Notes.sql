﻿CREATE TABLE [dbo].[Ward_Notes](
	[id] [int] NOT NULL,
	[usertype] [smallint] NOT NULL,
	[active] [bit] NOT NULL,
	[note] [varchar](8000) NOT NULL,
	[Location_Index] [int] NOT NULL
)