﻿CREATE TABLE [dbo].[Ward_stays](
	[Loc_stay_key] [varchar](38) NULL,
	[Location_start_date] [datetime] NULL,
	[Location_end_date] [datetime] NULL,
	[Applicable_date] [datetime] NULL,
	[Comment_Marker] [bit] NOT NULL,
	[Date_added] [datetime] NULL
)