﻿CREATE TABLE [dbo].[Whiteboard](
	[Index] [int] NOT NULL,
	[PatientList_Index] [int] NOT NULL,
	[Name] [varchar](32) NOT NULL,
	[Refresh_Rate_Seconds] [int] NOT NULL,
	[Request_Timeout_Minutes] [int] NULL,
	[Min_Patient_Alert_Level] [int] NULL,
	[Patient_Demographics_Flags] [bigint] NULL,
	[Custom_Data] [varchar](32) NULL,
	[Template_Index] [int] NOT NULL,
	[Last_Updated] [datetime] NOT NULL,
	[Restart_Point] [datetime] NULL,
	[UseBackgroundImage] [bit] NOT NULL
)