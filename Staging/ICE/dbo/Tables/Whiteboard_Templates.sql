﻿CREATE TABLE [dbo].[Whiteboard_Templates](
	[Index] [int] NOT NULL,
	[Name] [varchar](32) NOT NULL,
	[Description] [varchar](256) NULL,
	[File] [varchar](128) NOT NULL
)