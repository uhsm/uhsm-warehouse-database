﻿CREATE TABLE [dbo].[gp_locations_redundant_keep_for_a_while](
	[gp_org_national_code] [varchar](6) NOT NULL,
	[Address1] [varchar](35) NULL,
	[Address2] [varchar](35) NULL,
	[Address3] [varchar](35) NULL,
	[Address4] [varchar](35) NULL,
	[Postcode] [varchar](10) NULL,
	[Telephone] [varchar](23) NULL
)