﻿CREATE TABLE [dbo].[tmpQuery1](
	[ICE_INTERNAL_ID] [float] NULL,
	[PAS_HOSPITALNUMBER] [nvarchar](255) NULL,
	[PAS_FULLNAME] [nvarchar](255) NULL,
	[IDENTIFIER_IN_ICE] [nvarchar](255) NULL,
	[ICE_FULLNAME] [nvarchar](255) NULL,
	[REPORT_IN_ICE_FOR] [nvarchar](255) NULL
)