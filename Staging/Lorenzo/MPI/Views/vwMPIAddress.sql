﻿CREATE view [MPI].[vwMPIAddress] as
SELECT AddressID = PatientAddressRole.ROLES_REFNO
	,SourcePatientID = PatientAddressRole.PATNT_REFNO
	,AddressLine1 = PatientAddress.LINE1
	,AddressLine2 = PatientAddress.LINE2
	,AddressLine3 = PatientAddress.LINE3
	,AddressLine4 = PatientAddress.LINE4
	,Postcode = PatientAddress.PCODE
	,Country = Country.MAIN_CODE
	,AddressType = PatientAddressRole.ROTYP_CODE
	,HealthDistrictCode = PatientAddress.HDIST_CODE
	,AddressStartDate = PatientAddress.START_DTTM--CM 22/03/2016 as per KO request
	,AddressEndDate =PatientAddress.END_DTTM--CM 22/03/2016 as per KO request
	,CensusDate = GETDATE()
FROM dbo.Patient
INNER JOIN dbo.PatientAddressRole ON (Patient.PATNT_REFNO = PatientAddressRole.PATNT_REFNO)
INNER JOIN dbo.PatientAddress ON (PatientAddressRole.ADDSS_REFNO = PatientAddress.ADDSS_REFNO)
LEFT JOIN dbo.ReferenceValue AS Country ON (PatientAddress.CNTRY_REFNO = Country.RFVAL_REFNO)
WHERE Patient.ARCHV_FLAG = 'N' -- Patient is not logically deleted.
	AND PatientAddressRole.ARCHV_FLAG = 'N' -- Patient address role is not logically deleted.
	AND PatientAddressRole.CURNT_FLAG = 'Y' -- Patient address role is currently active.
	AND PatientAddress.ARCHV_FLAG = 'N' -- Patient address is not logically deleted.
	AND PatientAddress.ADTYP_CODE = 'POSTL' -- Postal addresses only.
	AND PatientAddress.END_DTTM IS NULL -- Patient address is still active.