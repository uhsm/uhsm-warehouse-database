﻿CREATE view [MPI].[vwMPIAlias] as

SELECT AliasID = PatientAlias.PATAL_REFNO
      ,SourcePatientID = PatientAlias.PATNT_REFNO
      ,Surname = PatientAlias.ALIAS_NAME
      ,GivenName = PatientAlias.FORENAME
      ,MiddleInitialOrName = NULL
      ,Prefix = NULL
      ,NameTypeCode = PatientAlias.PAAIL_REFNO_MAIN_CODE
      ,CensusDate = GETDATE()
  FROM dbo.Patient 
  LEFT JOIN  dbo.PatientAlias ON (Patient.PATNT_REFNO = PatientAlias.PATNT_REFNO)
  WHERE Patient.ARCHV_FLAG = 'N'
      AND PatientAlias.ARCHV_FLAG = 'N'
      AND PatientAlias.CURNT_FLAG = 'Y'