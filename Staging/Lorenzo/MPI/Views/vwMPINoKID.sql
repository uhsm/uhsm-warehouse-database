﻿CREATE view [MPI].[vwMPINoKID] as

SELECT NOKID = PatientPersonalCarer.PAPCA_REFNO
	,ID = PatientIdentifier.IDENTIFIER
	,AssigningAuthority = COALESCE(MedicalRecordsOrg.MAIN_IDENT,Organisation.MAIN_IDENT)
	,IdentifierTypeCodeID = IdentifierType.MAIN_CODE
	,AssigningFacility = CASE 
							WHEN IdentifierType.MAIN_CODE <> 'NHS' THEN OwnerOrg.MAIN_IDENT
	                     END
	,CensusDate = GETDATE()
FROM dbo.Patient 
INNER JOIN dbo.PatientPersonalCarer ON (Patient.PATNT_REFNO = PatientPersonalCarer.PATNT_REFNO)
INNER JOIN dbo.PersonalCarer ON (PatientPersonalCarer.PERCA_REFNO = PersonalCarer.PERCA_REFNO)
LEFT JOIN dbo.ReferenceValue AS ContactRole ON (PatientPersonalCarer.PETYP_REFNO = ContactRole.RFVAL_REFNO)
INNER JOIN dbo.PatientIdentifier ON (PersonalCarer.PATNT_REFNO = PatientIdentifier.PATNT_REFNO)
LEFT JOIN dbo.ReferenceValue AS IdentifierType ON (PatientIdentifier.PITYP_REFNO = IdentifierType.RFVAL_REFNO)
LEFT JOIN dbo.Organisation AS MedicalRecordsOrg ON (PatientIdentifier.MRD_HEORG_REFNO = MedicalRecordsOrg.HEORG_REFNO)
LEFT JOIN dbo.Organisation  ON (PatientIdentifier.HEORG_REFNO = Organisation.HEORG_REFNO)
LEFT JOIN dbo.Organisation AS OwnerOrg ON (PatientIdentifier.OWNER_HEORG_REFNO = OwnerOrg.HEORG_REFNO)
WHERE Patient.ARCHV_FLAG = 'N'
    AND PatientPersonalCarer.ARCHV_FLAG = 'N'
	AND PatientPersonalCarer.CURNT_FLAG = 'Y'
	AND PersonalCarer.ARCHV_FLAG = 'N'
	AND ContactRole.MAIN_CODE = 'NOK'  -- Next of kin only.
	AND PatientIdentifier.ARCHV_FLAG = 'N'
	AND PatientIdentifier.CURNT_FLAG = 'Y'