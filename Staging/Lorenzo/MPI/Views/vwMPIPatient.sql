﻿CREATE view [MPI].[vwMPIPatient] as
WITH HomePhone_CTE
AS (
	SELECT PatientAddressRole.ROLES_REFNO
		,PatientAddressRole.PATNT_REFNO
		,PatientAddressRole.ADDSS_REFNO
		,PatientAddressRole.START_DTTM
		,PatientAddress.LINE1
		,RowNumber = ROW_NUMBER() OVER (
			PARTITION BY PatientAddressRole.PATNT_REFNO ORDER BY PatientAddressRole.START_DTTM DESC
			)
	FROM dbo.PatientAddressRole
	LEFT JOIN dbo.PatientAddress ON (PatientAddressRole.ADDSS_REFNO = PatientAddress.ADDSS_REFNO)
	WHERE PatientAddressRole.ROTYP_CODE = 'HOME'
		AND PatientAddressRole.ARCHV_FLAG = 'N'
		AND PatientAddressRole.CURNT_FLAG = 'Y'
		AND PatientAddressRole.END_DTTM IS NULL
		AND PatientAddress.ADTYP_CODE = 'PHONE'
		AND PatientAddress.ARCHV_FLAG = 'N'
	)
	,WorkPhone_CTE
AS (
	SELECT PatientAddressRole.ROLES_REFNO
		,PatientAddressRole.PATNT_REFNO
		,PatientAddressRole.ADDSS_REFNO
		,PatientAddressRole.START_DTTM
		,PatientAddress.LINE1
		,RowNumber = ROW_NUMBER() OVER (
			PARTITION BY PatientAddressRole.PATNT_REFNO ORDER BY PatientAddressRole.START_DTTM DESC
			)
	FROM dbo.PatientAddressRole
	LEFT JOIN dbo.PatientAddress ON (PatientAddressRole.ADDSS_REFNO = PatientAddress.ADDSS_REFNO)
	WHERE PatientAddressRole.ROTYP_CODE = 'WP'
		AND PatientAddressRole.ARCHV_FLAG = 'N'
		AND PatientAddressRole.CURNT_FLAG = 'Y'
		AND PatientAddressRole.END_DTTM IS NULL
		AND PatientAddress.ADTYP_CODE = 'PHONE'
		AND PatientAddress.ARCHV_FLAG = 'N'
	)
	,AllActivity_CTE
AS (
	SELECT SourcePatientID = ScheduleEvent.PATNT_REFNO
		,MaxDate = MAX(ScheduleEvent.START_DTTM)
	FROM dbo.ScheduleEvent
	WHERE ScheduleEvent.ARCHV_FLAG = 'N'
	GROUP BY ScheduleEvent.PATNT_REFNO
	
	UNION ALL
	
	SELECT SourcePatientID = ProviderSpell.PATNT_REFNO
		,MaxDate = MAX(ProviderSpell.ADMIT_DTTM)
	FROM dbo.ProviderSpell
	WHERE ProviderSpell.ARCHV_FLAG = 'N'
	GROUP BY ProviderSpell.PATNT_REFNO
	
	UNION ALL
	
	SELECT SourcePatientID = Referral.PATNT_REFNO
		,MaxDate = MAX(Referral.RECVD_DTTM)
	FROM dbo.Referral
	WHERE Referral.ARCHV_FLAG = 'N'
	GROUP BY Referral.PATNT_REFNO
	)
	,LatestActivity_CTE
AS (
	SELECT AllActivity.SourcePatientID
		,LatestDate = MAX(AllActivity.MaxDate)
	FROM AllActivity_CTE AS AllActivity
	GROUP BY AllActivity.SourcePatientID
	)
	,PatientGP_CTE
AS (
	/* Get all the patient registered GPs.  
As there can be multiple entries per patient assign a row number in 
reversed order, so the latest entry is always row number 1.  */
	SELECT PatientProfessionalCarer.PATPC_REFNO
		,PatientProfessionalCarer.PROCA_REFNO
		,PatientProfessionalCarer.PROCA_MAIN_IDENT
		,PatientProfessionalCarer.PATNT_REFNO
		,PatientProfessionalCarer.HEORG_REFNO
		,PatientProfessionalCarer.START_DTTM
		,RowNumber = ROW_NUMBER() OVER (
			PARTITION BY PatientProfessionalCarer.PATNT_REFNO ORDER BY PatientProfessionalCarer.START_DTTM DESC
			)
	FROM dbo.PatientProfessionalCarer
	WHERE PatientProfessionalCarer.PRTYP_MAIN_CODE = 'GMPRC' -- GPs only.
		AND PatientProfessionalCarer.ARCHV_FLAG = 'N' -- Not logically deleted rows.
		AND PatientProfessionalCarer.END_DTTM IS NULL -- Only active rows.
	)
	,PatientGPDetails_CTE
AS (
	SELECT SourcePatientID = PatientGP.PATNT_REFNO
		,GPIdentifierCheckDigit = PatientGP.PROCA_REFNO
		,GPID = PatientGP.PROCA_MAIN_IDENT
		,GPSurname = GP.SURNAME
		,GPGivenName = GP.FORENAME
		,GPPrefix = GP.TITLE_REFNO_DESCRIPTION
		,GPPracticeID = Practice.MAIN_IDENT
		,GPPracticeName = Practice.[DESCRIPTION]
		,GPPracticeNameTypeCode = PracticeNameType.MAIN_CODE
		,GPSourceTable = 'NATGP'
		,GPAssigningAuthority = NULL -- Should be alright to leave this blank as NATGP has no assiging authority.
	FROM PatientGP_CTE AS PatientGP
	LEFT JOIN dbo.ProfessionalCarer AS GP ON (PatientGP.PROCA_REFNO = GP.PROCA_REFNO)
	LEFT JOIN dbo.Organisation AS Practice ON (PatientGP.HEORG_REFNO = Practice.HEORG_REFNO)
	LEFT JOIN dbo.ReferenceValue AS PracticeNameType ON (Practice.HOTYP_REFNO = PracticeNameType.RFVAL_REFNO)
	WHERE PatientGP.RowNumber = 1 -- Only include the latest GP details.
	)
	,OverseasStatus_CTE
AS (
	SELECT OverseasVisitorStatus.OVSEA_REFNO
		,OverseasVisitorStatus.OVSVS_REFNO_MAIN_CODE
		,OverseasVisitorStatus.PATNT_REFNO
		,OverseasVisitorStatus.START_DTTM
		,RowNumber = ROW_NUMBER() OVER (
			PARTITION BY OverseasVisitorStatus.PATNT_REFNO ORDER BY OverseasVisitorStatus.START_DTTM DESC
			)
	FROM dbo.OverseasVisitorStatus
	WHERE OverseasVisitorStatus.ARCHV_FLAG = 'N'
		AND OverseasVisitorStatus.END_DTTM IS NULL
	)
SELECT Distinct SourcePatientID = Patient.PATNT_REFNO
	,Surname = Patient.SURNAME
	,GivenName = Patient.FORENAME
	,MiddleInitalOrName = Patient.SECOND_FORENAME + ' ' + Patient.THIRD_FORENAME
	,Prefix = Patient.TITLE_REFNO_DESCRIPTION
	,DOB = Patient.DATE_OF_BIRTH
	,Sex = SEXX.MAIN_CODE--LEFT(Patient.SEXXX_REFNO_DESCRIPTION, 1) --29/03/2016 CM Amended to include this join to get Gender from ref value table as per KO request 
	,PhoneNumberHome = HomePhone.LINE1
	,PhoneNumberBusiness = WorkPhone.LINE1
	,PrimaryLanguage = PrimaryLanguage.MAIN_CODE
	,MaritalStatus = MaritalStatus.MAIN_CODE
	,Religion = Religion.MAIN_CODE
	,EthnicGroup = EthnicGroup.MAIN_CODE
	,BirthPlace = Patient.PLACE_OF_BIRTH
	,MultipleBirthIndicator = MultipleBirthIndicator.MAIN_CODE
	,Citizenship = OverseasStatus.OVSVS_REFNO_MAIN_CODE
	,PatientDeathDT = Patient.DTTM_OF_DEATH
	,PatientDeathIndicator = Patient.DECSD_FLAG
	,IdentityReliabilityCode = NULL -- Not sure how to populate this at the moment.
	,GPPracticeName = PatientGPDetails.GPPracticeName
	,GPPracticeNameTypeCode = PatientGPDetails.GPPracticeNameTypeCode
	,GPPracticeID = PatientGPDetails.GPPracticeID
	,GPID = PatientGPDetails.GPID
	,GPSurname = PatientGPDetails.GPSurname
	,GPGivenName = PatientGPDetails.GPGivenName
	,GPPrefix = PatientGPDetails.GPPrefix
	,GPSourceTable = PatientGPDetails.GPSourceTable
	,GPAssigningAuthority = PatientGPDetails.GPAssigningAuthority
	,GPIdentifierCheckDigit = PatientGPDetails.GPIdentifierCheckDigit
	,PatientNHSNo = Patient.NHS_IDENTIFIER--24/03/2016 CM Added NHS Identifier as not always in the PatientIdentfier table - as per KO
	,AnyUHSMActivity = CASE 
		WHEN LatestActivity.LatestDate IS NOT NULL
			THEN 'Y'
		ELSE 'N'
		END
	,LastUHSMActivityDT = LatestActivity.LatestDate
	,CensusDate = GETDATE()
	,MinorID = case when MergePatient.PREV_PATNT_REFNO is not null then 1 else 0 end--26/04/2016 CM added these field to help identify merged patients as per KO request


FROM dbo.Patient
LEFT JOIN HomePhone_CTE AS HomePhone ON (
		Patient.PATNT_REFNO = HomePhone.PATNT_REFNO
		AND HomePhone.RowNumber = 1
		)
LEFT JOIN WorkPhone_CTE AS WorkPhone ON (
		Patient.PATNT_REFNO = WorkPhone.PATNT_REFNO
		AND WorkPhone.RowNumber = 1
		)
LEFT JOIN dbo.ReferenceValue AS PrimaryLanguage ON (Patient.SPOKL_REFNO = PrimaryLanguage.RFVAL_REFNO)
LEFT JOIN dbo.ReferenceValue AS MaritalStatus ON (Patient.MARRY_REFNO = MaritalStatus.RFVAL_REFNO)
LEFT JOIN dbo.ReferenceValue AS Religion ON (Patient.RELIG_REFNO = Religion.RFVAL_REFNO)
LEFT JOIN dbo.ReferenceValue AS EthnicGroup ON (Patient.ETHGR_REFNO = EthnicGroup.RFVAL_REFNO)
LEFT JOIN dbo.ReferenceValue AS MultipleBirthIndicator ON (Patient.MULTB_REFNO = MultipleBirthIndicator.RFVAL_REFNO)
LEFT JOIN dbo.ReferenceValue AS SEXX ON (Patient.SEXXX_REFNO = SEXX.RFVAL_REFNO)--29/03/2016 CM Amended to include this join to get Gender from ref value table as per KO request 

LEFT JOIN LatestActivity_CTE AS LatestActivity ON (Patient.PATNT_REFNO = LatestActivity.SourcePatientID)
LEFT JOIN PatientGPDetails_CTE AS PatientGPDetails ON (Patient.PATNT_REFNO = PatientGPDetails.SourcePatientID)
LEFT JOIN OverseasStatus_CTE AS OverseasStatus ON (
		Patient.PATNT_REFNO = OverseasStatus.PATNT_REFNO
		AND OverseasStatus.RowNumber = 1
		)
LEFT JOIN dbo.MergePatient on Patient.PATNT_REFNO = MergePatient.PREV_PATNT_REFNO


WHERE Patient.ARCHV_FLAG = 'N'