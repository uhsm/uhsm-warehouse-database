﻿CREATE view [MPI].[vwMPIPatientID] as
SELECT Distinct  IdentifierID = PatientIdentifier.PATID_REFNO
	,SourcePatientID = PatientIdentifier.PATNT_REFNO
	,ID = PatientIdentifier.IDENTIFIER
	,AssigningAuthority = COALESCE(MedicalRecordsOrg.MAIN_IDENT, Organisation.MAIN_IDENT)
	,IdentifierTypeCodeID = IdentifierType.MAIN_CODE
	,AssigningFacility = CASE 
		WHEN IdentifierType.MAIN_CODE <> 'NHS'
			THEN OwnerOrg.MAIN_IDENT
		END
	,PatientIdentifierStartDate = PatientIdentifier.START_DTTM--CM 22/03/2016  as req by KO
	,PatientIdentifierEndDate = PatientIdentifier.END_DTTM--CM 22/03/2016  as req by KO
	,PIRefno = PatientIdentifier.PITYP_REFNO--CM 22/03/2016  as req by KO
	,CensusDate = GETDATE()
	--,MinorID = case when MergePatient.PREV_PATNT_REFNO is not null then 1 else 0 end--26/04/2016 CM added these field to help identify merged patients as per KO request
	--,MergedWithID = m2.PREV_PATNT_REFNO--26/04/2016 CM added these field to help identify merged patients as per KO request


FROM dbo.Patient
INNER JOIN dbo.PatientIdentifier ON (Patient.PATNT_REFNO = PatientIdentifier.PATNT_REFNO)
LEFT JOIN dbo.ReferenceValue AS IdentifierType ON (PatientIdentifier.PITYP_REFNO = IdentifierType.RFVAL_REFNO)
LEFT JOIN dbo.Organisation AS MedicalRecordsOrg ON (PatientIdentifier.MRD_HEORG_REFNO = MedicalRecordsOrg.HEORG_REFNO)
LEFT JOIN dbo.Organisation ON (PatientIdentifier.HEORG_REFNO = Organisation.HEORG_REFNO)
LEFT JOIN dbo.Organisation AS OwnerOrg ON (PatientIdentifier.OWNER_HEORG_REFNO = OwnerOrg.HEORG_REFNO)
LEFT JOIN dbo.MergePatient on Patient.PATNT_REFNO = MergePatient.PREV_PATNT_REFNO
LEFT JOIN dbo.MergePatient as  m2 on Patient.PATNT_REFNO = m2.PATNT_REFNO

WHERE Patient.ARCHV_FLAG = 'N' -- Patient is not logically deleted.
	AND PatientIdentifier.ARCHV_FLAG = 'N' -- Patient identifier is not logically deleted.
	AND PatientIdentifier.CURNT_FLAG = 'Y' -- Patient identifier is current.
	--AND Patient.PATNT_REFNO in ('13252363','11594896')