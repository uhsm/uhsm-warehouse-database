﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:	Add new test patient.

Notes:		Stored in 

Versions:
		1.0.0.0 - 08/10/2015 - MT
			Created sproc
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [dbo].[AddNewTestPatient]
	@PATNT_REFNO int
As

Insert Into ExcludedPatient(
	SourcePatientNo,
	PatientTitle,
	PatientForename,
	PatientSurname,
	DateOfBirth,
	NHSNumber,
	DateCreated
	)
Select	
	src.PATNT_REFNO,		--SourcePatientNo
	src.TITLE_REFNO_DESCRIPTION,	--PatientTitle
	src.FORENAME,			--PatientForename
	src.SURNAME,			--PatientSurname
	src.DTTM_OF_BIRTH,		--DateOfBirth
	src.PATNT_REFNO_NHS_IDENTIFIER, --NHSNumber
	src.CREATE_DTTM			--DateCreated

From	Patient src

Where	src.PATNT_REFNO = @PATNT_REFNO