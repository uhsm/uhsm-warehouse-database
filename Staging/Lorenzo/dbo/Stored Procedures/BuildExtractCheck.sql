﻿CREATE procedure [dbo].[BuildExtractCheck] as

/*
--Author: K Oakden
--Date Created: 05/11/2014
--To keep track of import from extract files
*/

--drop table dbo.ExtractCheck
truncate table dbo.ExtractCheck
insert into dbo.ExtractCheck
select * from (
	select distinct     TableName = 'PatientAddress'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from PatientAddress where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'PatientAddressRole'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from PatientAddressRole where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'AdmissionDecision'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from AdmissionDecision where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'AugmentedCarePeriod'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from AugmentedCarePeriod where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'Casenote'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from Casenote where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'CasenoteActivity'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from CasenoteActivity where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'CasenoteVolume'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from CasenoteVolume where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'ContractRule'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from ContractRule where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'ClinicalCoding'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from ClinicalCoding where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'AllocationFailure'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from AllocationFailure where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'AllocatorRequest'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from AllocatorRequest where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'ContractAllocation'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from ContractAllocation where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'ContractAllocationHistory'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from ContractAllocationHistory where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'AdmissionOffer'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from AdmissionOffer where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'APCStay'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from APCStay where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'MaternitySpell'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from MaternitySpell where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'ProfessionalCareEpisode'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from ProfessionalCareEpisode where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'ProviderSpell'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from ProviderSpell where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'Birth'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from Birth where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	--union select distinct     TableName = 'Letter'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from Letter where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	--union select distinct     TableName = 'LetterAudit'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from LetterAudit where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	--union select distinct     TableName = 'LetterAuthorship'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from LetterAuthorship where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	--union select distinct     TableName = 'LetterConfiguration'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from LetterConfiguration where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	--union select distinct     TableName = 'LetterDefinition'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from LetterDefinition where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	--union select distinct     TableName = 'LetterMergeField'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from LetterMergeField where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	--union select distinct     TableName = 'LetterParameter'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from LetterParameter where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	--union select distinct     TableName = 'LetterRecipient'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from LetterRecipient where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'NoteHistory'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from NoteHistory where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'Participate'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from Participate where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'Referral'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from Referral where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'ScheduleHistory'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from ScheduleHistory where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'ScheduleEvent'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from ScheduleEvent where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'WaitingList'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from WaitingList where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'ReferenceValueDependancy'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from ReferenceValueDependancy where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'RTT'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from RTT where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'OrganisationAccess'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from OrganisationAccess where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'Organisation'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from Organisation where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'HomeLeave'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from HomeLeave where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'MergeActivity'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from MergeActivity where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'NoteRole'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from NoteRole where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'OverseasVisitorStatus'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from OverseasVisitorStatus where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'PatientAlias'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from PatientAlias where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'PatientIdentifier'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from PatientIdentifier where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'Patient'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from Patient where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'PatientPersonalCarer'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from PatientPersonalCarer where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'PatientProfessionalCarer'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from PatientProfessionalCarer where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'PersonalCarer'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from PersonalCarer where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'PeriodOfCare'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from PeriodOfCare where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'ProfessionalCareIdentifier'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from ProfessionalCareIdentifier where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'ProfessionalCarer'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from ProfessionalCarer where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'ProfessionalCarerSpecialty'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from ProfessionalCarerSpecialty where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'ReferenceValue'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from ReferenceValue where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'ServicePoint'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from ServicePoint where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'ServicePointSlot'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from ServicePointSlot where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'ServicePointSession'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from ServicePointSession where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'ServicePointStay'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from ServicePointStay where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'User'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from [User] where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
	union select distinct     TableName = 'WaitingListSuspension'         ,DateCreated = dateadd(day, 1, datediff(day, 1, Created))       ,RecordCount = COUNT(1)   from WaitingListSuspension where Created > dateadd(MONTH, -1, GETDATE())   group by dateadd(day, 1, datediff(day, 1, Created))
) summary

--create pivot summary

declare @cols varchar(4000)   --dynamically define column headers
declare @sql NVARCHAR(4000)   

--select @cols = isnull(@cols + ',', '') + '[' + DateCreated + ']'
--from 
--	(
--	select distinct CONVERT(VARCHAR(8), DateCreated, 112)as DateCreated from dbo.ExtractCheck

--	)dt
--order by DateCreated

select @cols = isnull(@cols + ',', '') + '[' + TableName + ']'
from 
	(
	select distinct TableName from dbo.ExtractCheck
	)tn
order by TableName

--print @cols
drop table dbo.ExtractCheckSummary

SET @sql = 
	'select * into dbo.ExtractCheckSummary from (
		select 
			DateCreated = CONVERT(VARCHAR(8), DateCreated, 112)
			,TableName
			,RecordCount
		from dbo.ExtractCheck
		--group by TableName, CONVERT(VARCHAR(8), DateCreated, 112)
	)grp
	PIVOT (SUM(RecordCount) FOR TableName IN ('
	+ @cols +
	')) AS pvt'

--print @sql
execute(@sql)