﻿CREATE procedure [dbo].[BuildExtractDatasetEncounterAPC]
	@from smalldatetime
as

declare @localfrom smalldatetime

select
	@localfrom = @from

delete
from
	dbo.ExtractDatasetEncounter
where
	DatasetCode = 'APC'

insert
into
	dbo.ExtractDatasetEncounter
select
	 DatasetCode = 'APC'
	,EncounterRecno = APC.PRCAE_REFNO
from
	dbo.ProfessionalCareEpisode APC
where
	APC.Created > @localfrom

or	exists
	(
	select
		1
	from
		dbo.Patient
	where
		Patient.PATNT_REFNO = APC.PATNT_REFNO
	and	Patient.Created > @localfrom
	)

or	exists
	(
	select
		1
	from
		dbo.PatientIdentifier DistrictNo
	where
		DistrictNo.Created > @localfrom
	and	DistrictNo.PATNT_REFNO = APC.PATNT_REFNO
	)

or	exists
	(
	select
		1
	from
		dbo.PatientAddressRole Activity
	where
		Activity.Created > @localfrom
	and	Activity.PATNT_REFNO = APC.PATNT_REFNO
	and	Activity.ROTYP_CODE = 'HOME'
	)

or	exists
	(
	select
		1
	from
		dbo.PatientAddress Activity

	inner join dbo.PatientAddressRole
	on	PatientAddressRole.ADDSS_REFNO = Activity.ADDSS_REFNO
	and	PatientAddressRole.PATNT_REFNO = APC.PATNT_REFNO
	and	PatientAddressRole.ROTYP_CODE = 'HOME'

	where
		Activity.Created > @localfrom
	)

or	exists
	(
	select
		1
	from
		dbo.PatientProfessionalCarer Activity
	where
		Activity.Created > @localfrom
	and	Activity.PATNT_REFNO = APC.PATNT_REFNO
	)

or	exists
	(
	select
		1
	from
		dbo.Contract Activity
	where
		Activity.Created > @localfrom
	and	Activity.CONTR_REFNO = APC.CONTR_REFNO
	)

or	exists
	(
	select
		1
	from
		dbo.Referral Activity
	where
		Activity.Created > @localfrom
	and	Activity.REFRL_REFNO = APC.REFRL_REFNO
	)

or	exists
	(
	select
		1
	from
		dbo.ServicePoint Activity
	where
		Activity.Created > @localfrom
	and	Activity.SPONT_REFNO = APC.SPONT_REFNO
	)

or	exists
	(
	select
		1
	from
		dbo.OverseasVisitorStatus Activity
	where
		Activity.Created > @localfrom
	and	Activity.PATNT_REFNO = APC.PATNT_REFNO
	)

or	exists
	(
	select
		1
	from
		dbo.ProviderSpell Activity
	where
		Activity.Created > @localfrom
	and	Activity.PRVSP_REFNO = APC.PRVSP_REFNO
	)

or	exists
	(
	select
		1
	from
		dbo.Birth Activity
	where
		Activity.PATNT_REFNO = APC.PATNT_REFNO
	and	dateadd(day, datediff(day, 0, Activity.DTTM_OF_BIRTH), 0) = dateadd(day, datediff(day, 0, APC.START_DTTM), 0)
	and	Activity.ARCHV_FLAG = 'N'
	and	Activity.Created > @localfrom
	)

or	exists
	(
	select
		1
	from
		dbo.ServicePointStay Activity
	where
		Activity.Created > @localfrom
	and	Activity.PRVSP_REFNO = APC.PRVSP_REFNO
	)

or	exists
	(
	select
		1
	from
		dbo.ClinicalCoding Activity
	where
		Activity.Created > @localfrom
	and	Activity.PATNT_REFNO = APC.PATNT_REFNO
	and	Activity.START_DTTM <= APC.END_DTTM
	and	Activity.CODE in ('009', 'INFC006')
	and	Activity.DPTYP_CODE = 'ALERT'
	)

or	exists
	(
	select
		1
	from
		dbo.ClinicalCoding Activity
	where
		Activity.Created > @localfrom
	and	Activity.SORCE_REFNO = APC.PRCAE_REFNO
	and	Activity.SORCE_CODE = 'PRCAE'
	and	Activity.START_DTTM <= APC.END_DTTM
	)


--remove test patients (done here as delete due to performance reasons when included in above select)
delete from dbo.ExtractDatasetEncounter
where
	exists
	(
	select
		1
	from
		dbo.ProfessionalCareEpisode Activity

	inner join dbo.ExcludedPatient
	on	ExcludedPatient.SourcePatientNo = Activity.PATNT_REFNO
	and	ExtractDatasetEncounter.DatasetCode = 'APC'

	where
		Activity.PRCAE_REFNO = ExtractDatasetEncounter.SourceUniqueID
	)