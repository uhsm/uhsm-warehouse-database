﻿Create procedure [dbo].[BuildExtractDatasetEncounterOP_fullload]
	@from smalldatetime
as

declare @localfrom smalldatetime

select
	@localfrom = @from

--use of temporary table shouldn't be required but IS!
select
	*
into
	#ExtractDatasetEncounter
from
	(
	select
		 DatasetCode = 'OUTPATIENT'
		,SourceUniqueID = Outpatient.SCHDL_REFNO
	from
		dbo.ScheduleEvent Outpatient
	where
		Outpatient.SCTYP_REFNO in (1470, 1472) --1472 = WA, 1470 = OP
	and	(
			Outpatient.Created > @localfrom

		or	exists
			(
			select
				1
			from
				dbo.Patient
			where
				Patient.PATNT_REFNO = Outpatient.PATNT_REFNO
			and	Patient.Created > @localfrom
			)

		or	exists
			(
			select
				1
			from
				dbo.PatientIdentifier DistrictNo
			where
				DistrictNo.Created > @localfrom
			and	DistrictNo.PATNT_REFNO = Outpatient.PATNT_REFNO
			)

		or	exists
			(
			select
				1
			from
				dbo.PatientAddressRole Activity
			where
				Activity.Created > @localfrom
			and	Activity.PATNT_REFNO = Outpatient.PATNT_REFNO
			and	Activity.ROTYP_CODE = 'HOME'
			)

		or	exists
			(
			select
				1
			from
				dbo.PatientAddress Activity

			inner join dbo.PatientAddressRole
			on	PatientAddressRole.ADDSS_REFNO = Activity.ADDSS_REFNO
			and	PatientAddressRole.PATNT_REFNO = Outpatient.PATNT_REFNO
			and	PatientAddressRole.ROTYP_CODE = 'HOME'

			where
				Activity.Created > @localfrom
			)

		or	exists
			(
			select
				1
			from
				dbo.PatientProfessionalCarer Activity
			where
				Activity.Created > @localfrom
			and	Activity.PATNT_REFNO = Outpatient.PATNT_REFNO
			)

		or	exists
			(
			select
				1
			from
				dbo.Contract Activity
			where
				Activity.Created > @localfrom
			and	Activity.CONTR_REFNO = Outpatient.CONTR_REFNO
			)

		or	exists
			(
			select
				1
			from
				dbo.[User] Activity
			where
				Activity.Created > @localfrom
			and	Activity.USERS_REFNO = Outpatient.USER_REFNO
			)

		or	exists
			(
			select
				1
			from
				dbo.ScheduleEvent Activity
			where
				Activity.Created > @localfrom
			and	Activity.SCHDL_REFNO = Outpatient.SCHDL_REFNO
			)

		or	exists
			(
			select
				1
			from
				dbo.Referral Activity
			where
				Activity.Created > @localfrom
			and	Activity.REFRL_REFNO = Outpatient.REFRL_REFNO
			)

		or	exists
			(
			select
				1
			from
				dbo.ServicePoint Activity
			where
				Activity.Created > @localfrom
			and	Activity.SPONT_REFNO = Outpatient.SPONT_REFNO
			)

		or	exists
			(
			select
				1
			from
				dbo.OverseasVisitorStatus Activity
			where
				Activity.Created > @localfrom
			and	Activity.PATNT_REFNO = Outpatient.PATNT_REFNO
			)

		or	exists
			(
			select
				1
			from
				dbo.WaitingList Activity
			where
				Activity.Created > @localfrom
			and	Activity.WLIST_REFNO = Outpatient.WLIST_REFNO
			)
		
	--09/12/2015 CM added section below to capture clinical coding net change	
		or	exists
			(
			select
				1
			from
				dbo.ClinicalCoding Activity
			where
				Activity.SORCE_CODE = 'SCHDL'
			and Activity.Created > @localfrom
			and	Activity.SORCE_REFNO = Outpatient.SCHDL_REFNO
			)


		or	exists
			(
			select
				1
			from
				dbo.ScheduleHistory Activity
			where
				Activity.Created > @localfrom
			and	Activity.SCHDL_REFNO = Outpatient.SCHDL_REFNO
			and	Activity.MOVRN_REFNO in (
					 2003544	--Other - Patient cancellation
					,3248		--Other - Patient cancellation
					,2004148	--Patient is current Inpatient
					,2003541	--Patient - Other more pressing engagement
					,2003540	--Patient - On holiday
					,383		--Patient died
					,2003539	--Patient ill
					,4520		--Patient Cancelled
				)
			)
		)

--	union --shouldn't need to do this but adding this to above select causes duplicates!
--
--	select
--		 DatasetCode = 'OUTPATIENT'
--		,SourceUniqueID = Outpatient.SCHDL_REFNO
--	from
--		dbo.ScheduleEvent Outpatient
--	where
--		Outpatient.SCTYP_REFNO in (1470, 1472) --1472 = WA, 1470 = OP
--	and	(
--			Outpatient.Created > @localfrom
--
--		or	exists
--			(
--			select
--				1
--			from
--				dbo.ScheduleHistory Activity
--			where
--				Activity.Created > @localfrom
--			and	Activity.SCHDL_REFNO = Outpatient.SCHDL_REFNO
--			and	Activity.MOVRN_REFNO in (
--					 2003544	--Other - Patient cancellation
--					,3248		--Other - Patient cancellation
--					,2004148	--Patient is current Inpatient
--					,2003541	--Patient - Other more pressing engagement
--					,2003540	--Patient - On holiday
--					,383		--Patient died
--					,2003539	--Patient ill
--					,4520		--Patient Cancelled
--				)
--			)
--		)
	) Activity



delete
from
	dbo.ExtractDatasetEncounter
where
	DatasetCode = 'OUTPATIENT'


insert
into
	dbo.ExtractDatasetEncounter
select distinct
	*
from
	#ExtractDatasetEncounter

--remove test patients (done here as delete due to performance reasons when included in above select)
delete from dbo.ExtractDatasetEncounter
where
	exists
	(
	select
		1
	from
		dbo.ScheduleEvent Activity

	inner join dbo.ExcludedPatient
	on	ExcludedPatient.SourcePatientNo = Activity.PATNT_REFNO
	and	ExtractDatasetEncounter.DatasetCode = 'OUTPATIENT'

	where
		Activity.SCHDL_REFNO = ExtractDatasetEncounter.SourceUniqueID
	)