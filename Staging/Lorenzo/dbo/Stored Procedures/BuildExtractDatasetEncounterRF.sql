﻿CREATE procedure [dbo].[BuildExtractDatasetEncounterRF]
	@from smalldatetime
as

declare @localfrom smalldatetime

select
	@localfrom = @from

delete
from
	dbo.ExtractDatasetEncounter
where
	DatasetCode = 'REFERRAL'

insert
into
	dbo.ExtractDatasetEncounter
select Distinct 
	 DatasetCode = 'REFERRAL'
	,EncounterRecno = Referral.REFRL_REFNO
from
	dbo.Referral

where
	Referral.Created > @localfrom

or	exists
	(
	select
		1
	from
		dbo.Patient
	where
		Patient.PATNT_REFNO = Referral.PATNT_REFNO
	and	Patient.Created > @localfrom
	)

or	exists
	(
	select
		1
	from
		dbo.PatientIdentifier DistrictNo
	where
		DistrictNo.Created > @localfrom
	and	DistrictNo.PATNT_REFNO = Referral.PATNT_REFNO
	)

or	exists
	(
	select
		1
	from
		dbo.PatientAddressRole Activity
	where
		Activity.Created > @localfrom
	and	Activity.PATNT_REFNO = Referral.PATNT_REFNO
	and	Activity.ROTYP_CODE = 'HOME'
	)

or	exists
	(
	select
		1
	from
		dbo.PatientAddress Activity

	inner join dbo.PatientAddressRole
	on	PatientAddressRole.ADDSS_REFNO = Activity.ADDSS_REFNO
	and	PatientAddressRole.PATNT_REFNO = Referral.PATNT_REFNO

	where
		Activity.Created > @localfrom
	)

or	exists
	(
	select
		1
	from
		dbo.PatientProfessionalCarer Activity
	where
		Activity.Created > @localfrom
	and	Activity.PATNT_REFNO = Referral.PATNT_REFNO
	)

or	exists
	(
	select
		1
	from
		dbo.Contract Activity
	where
		Activity.Created > @localfrom
	and	Activity.CONTR_REFNO = Referral.CONTR_REFNO
	)

or	exists
	(
	select
		1
	from
		dbo.[User] Activity
	where
		Activity.Created > @localfrom
	and	Activity.USERS_REFNO = Referral.USER_REFNO
	)

or	exists
	(
	select
		1
	from
		dbo.ScheduleEvent Activity
	where
		Activity.Created > @localfrom
	and	Activity.REFRL_REFNO = Referral.REFRL_REFNO
	)
	
--06/04/2016 CM - Added Section below to build in to get net change for referrals which have had referral comments and referral details comments fields update
insert
into
	dbo.ExtractDatasetEncounter
select Distinct 
	 DatasetCode = 'REFERRAL'
	,EncounterRecno = Referral.REFRL_REFNO
from
	dbo.Referral

where
exists
	(
	select
		1
	
	from dbo.NoteRole Activity
	where SORCE_CODE in ('RFCMT')
		and Activity.Created > @localfrom
	and	Activity.SORCE_REFNO = Referral.REFRL_REFNO
	)
and not exists 
	(Select 1 
	from ExtractDatasetEncounter
	where Referral.REFRL_REFNO = ExtractDatasetEncounter.SourceUniqueID
	)
	
	
	
insert
into
	dbo.ExtractDatasetEncounter
select Distinct 
	 DatasetCode = 'REFERRAL'
	,EncounterRecno = Referral.REFRL_REFNO
from
	dbo.Referral

where
exists
	(
	select
		1
	
	from dbo.NoteRole Activity
	where SORCE_CODE in ('RDCMT')
		and Activity.Created > @localfrom
	and	Activity.SORCE_REFNO = Referral.REFRL_REFNO
	)
and not exists 
	(Select 1 
	from ExtractDatasetEncounter
	where Referral.REFRL_REFNO = ExtractDatasetEncounter.SourceUniqueID
	)
	



--remove test patients (done here as delete due to performance reasons when included in above select)
delete from dbo.ExtractDatasetEncounter
where
	exists
	(
	select
		1
	from
		dbo.Referral Activity

	inner join dbo.ExcludedPatient
	on	ExcludedPatient.SourcePatientNo = Activity.PATNT_REFNO
	and	ExtractDatasetEncounter.DatasetCode = 'REFERRAL'

	where
		Activity.REFRL_REFNO = ExtractDatasetEncounter.SourceUniqueID
	)