﻿CREATE procedure [dbo].[BuildImportTable]
	 @table varchar(50) = null
	,@schema varchar(50) = 'dbo'
as

BEGIN

	/**************************************************************************/
	/* General Declarations                                                   */
	/**************************************************************************/
	declare @FetchStatus int
	declare @sql varchar(8000)

	declare @ViewName varchar(255)
	declare @ExtractTableCode varchar(255)
	declare @ExtractFilePrefix varchar(255)

	declare @ColumnName varchar(255)
	declare @ColumnType varchar(255)
	declare @ColumnOrder int
	declare @sourceTable varchar(255)
	declare @columnOffset int
	declare @pkColumn varchar(255)


	/**************************************************************************/
	/* Declaration for trigger cursor                                         */
	/**************************************************************************/

	declare TableCursor CURSOR fast_forward FOR
	select
		ViewName =
			coalesce(
				 Extract.ViewName
				,Extract.ExtractCode
			)


		,ExtractFilePrefix = 
			'''' + Extract.ExtractFilePrefix + '%'''

		,Extract.ExtractTableCode
	from
		dbo.Extract
	where
		Extract.Active = 1
	and	(
			Extract.ViewName = @table
		or	@table is null
		)


	/**************************************************************************/
	/* Initialisation                                                         */
	/**************************************************************************/
	Select
		 @sourceTable = 'dbo.TImportCSVParsed'
		,@columnOffset = 4
		,@sql = ''


	/**************************************************************************/
	/* Initialisation                                                         */
	/**************************************************************************/
	OPEN TableCursor

	FETCH NEXT FROM TableCursor INTO
		 @ViewName
		,@ExtractFilePrefix 
		,@ExtractTableCode

	Select @FetchStatus = @@FETCH_STATUS


    /**************************************************************************/
    /* Main Processing Loop                                                   */
    /**************************************************************************/
    WHILE (@FetchStatus  <> -1) and (@FetchStatus  <> -2)

	begin

		declare ColumnCursor CURSOR fast_forward FOR
		select
			ColumnName = Attribute.AttributeCode

			,ColumnType =
				Attribute.AttributeDataType +
				case
				when lower(Attribute.AttributeDataType) = 'varchar'
				then '(' + convert(varchar, Attribute.AttributeLength) + ')'
				else ''
				end
		from
			dbo.Attribute
		where
			Attribute.ExtractTableCode = @ExtractTableCode
		order by
			Attribute.AttributeOrder

		OPEN ColumnCursor

		FETCH NEXT FROM ColumnCursor INTO
			 @ColumnName
			,@ColumnType

		Select @FetchStatus = @@FETCH_STATUS

		select
			@pkColumn = @ColumnName

		/**************************************************************************/
		/* Column Processing Loop                                                 */
		/**************************************************************************/
		WHILE (@FetchStatus  <> -1) and (@FetchStatus  <> -2)

		begin

			select
				@sql =
					@sql + ',[' + @ColumnName + '] ' + @ColumnType


			FETCH NEXT FROM ColumnCursor INTO
				 @ColumnName
				,@ColumnType

			Select @FetchStatus = @@FETCH_STATUS

		end

		select
			 @sql = 
				'create' +
				' table [' + 
				@schema + '].[' +
				@ViewName + '] (' +
				right(
					 @sql
					,datalength(@sql) - 1
				) +
				',Created datetime' +
				',CONSTRAINT [PK' + '_' + @schema + '_' + @ViewName + '_' + @pkColumn + '] PRIMARY KEY CLUSTERED (' + @pkColumn + ')' +
				')'

		print (@sql)

		CLOSE ColumnCursor
		DEALLOCATE ColumnCursor

		select
			 @sql = ''

		FETCH NEXT FROM TableCursor INTO
			 @ViewName
			,@ExtractFilePrefix 
			,@ExtractTableCode

		Select @FetchStatus = @@FETCH_STATUS

    END

	/**************************************************************************/
	/* Termination                                                            */
	/**************************************************************************/
	CLOSE TableCursor
	DEALLOCATE TableCursor

END