﻿CREATE procedure [dbo].[BuildImportView]
	 @table varchar(50) = null
	,@schema varchar(50) = 'dbo'
as

BEGIN

	/**************************************************************************/
	/* General Declarations                                                   */
	/**************************************************************************/
	declare @FetchStatus int
	declare @sql varchar(8000)
	declare @sql1 varchar(8000)
	declare @sqlpart1 varchar(8000)
	declare @sqlpart2 varchar(8000)
	declare @crlf char(2)
	declare @tab char(1)

	declare @ViewName varchar(255)
	declare @ExtractTableCode varchar(255)
	declare @ExtractFilePrefix varchar(255)

	declare @ColumnName varchar(255)
	declare @ColumnValue varchar(255)
	declare @ColumnOrder int
	declare @sourceTable varchar(255)
	declare @columnOffset int


	/**************************************************************************/
	/* Declaration for trigger cursor                                         */
	/**************************************************************************/

	declare TableCursor CURSOR fast_forward FOR
	select
		ViewName =
			'TLoad' +
			coalesce(
				 Extract.ViewName
				,Extract.ExtractCode
			)


		,ExtractFilePrefix = 
			'''' + Extract.ExtractFilePrefix + '%'''

		,Extract.ExtractTableCode
	from
		dbo.Extract
	where
		Extract.Active = 1
	and	(
			Extract.ViewName = @table
		or	@table is null
		)


	/**************************************************************************/
	/* Initialisation                                                         */
	/**************************************************************************/
	Select
		 @sourceTable = 'dbo.TImportCSVParsed'
		,@columnOffset = 4
		,@sql = ''
		,@sql1 = ''
		,@crlf = char(13) + char(10)
		,@tab = char(9)


	/**************************************************************************/
	/* Initialisation                                                         */
	/**************************************************************************/
	OPEN TableCursor

	FETCH NEXT FROM TableCursor INTO
		 @ViewName
		,@ExtractFilePrefix 
		,@ExtractTableCode

	Select @FetchStatus = @@FETCH_STATUS


    /**************************************************************************/
    /* Main Processing Loop                                                   */
    /**************************************************************************/
    WHILE (@FetchStatus  <> -1) and (@FetchStatus  <> -2)

	begin

		declare ColumnCursor CURSOR fast_forward FOR
		select
			ColumnName =
					Attribute.AttributeCode
--				coalesce(
--					 Attribute.AttributeColumnName
--					,Attribute.AttributeCode
--				)

			,ColumnValue =
				case
				when lower(Attribute.AttributeDataType) in ('varchar', 'char')
				then ' = Column' + convert(varchar, Attribute.AttributeOrder + @columnOffset)
				else
					' = convert(' +
					Attribute.AttributeDataType +
					',Column' + convert(varchar, Attribute.AttributeOrder + @columnOffset)+	
					')'
				end
		from
			dbo.Attribute
		where
			Attribute.ExtractTableCode = @ExtractTableCode
		order by
			Attribute.AttributeOrder

		OPEN ColumnCursor

		FETCH NEXT FROM ColumnCursor INTO
			 @ColumnName
			,@ColumnValue

		Select @FetchStatus = @@FETCH_STATUS

		/**************************************************************************/
		/* Column Processing Loop                                                 */
		/**************************************************************************/
		WHILE (@FetchStatus  <> -1) and (@FetchStatus  <> -2)

		begin

			if datalength(@sql) > 7000
			begin
				select
					 @sql1 = @crlf + @tab + ' ' +
						right(
							 @sql
							,datalength(@sql) - 2
						)

					,@sql = ''
			end

			select
				@sql =
					@sql + @tab + ',[' + @ColumnName + ']' + @ColumnValue + @crlf


			FETCH NEXT FROM ColumnCursor INTO
				 @ColumnName
				,@ColumnValue

			Select @FetchStatus = @@FETCH_STATUS

		end

		select
			 @sqlpart1 = 
				case
				when
					exists
						(
						select
							1
						from
							INFORMATION_SCHEMA.VIEWS
						where
							TABLE_SCHEMA + '.' + TABLE_NAME = @schema + '.' + @ViewName
						)
				then 'alter'
				else 'create'
				end +
				' view ' + 
				@schema + '.[' +
				@ViewName + '] as' + @crlf + @crlf +
				'select' +
				@sql1

			 ,@sqlpart2 = 
				case
				when @sql1 = ''
				then @tab + ' ' +
					right(
						 @sql
						,datalength(@sql) - 2
					)
				else @sql
				end
				+ @tab + ',Created' + @crlf +

				'from' + @crlf + @tab + @sourceTable + @crlf +
				'where'  + @crlf + @tab + 'Column0 like ' + @ExtractFilePrefix + @crlf +
				'and'  + @tab + 'Valid = 1'

				 + @crlf + 'GO' + @crlf + @crlf

--		exec (@sqlpart1 + @sqlpart2)
		print @sqlpart1
		print @sqlpart2

		CLOSE ColumnCursor
		DEALLOCATE ColumnCursor

		select
			 @sql = ''
			,@sql1 = ''

		FETCH NEXT FROM TableCursor INTO
			 @ViewName
			,@ExtractFilePrefix 
			,@ExtractTableCode

		Select @FetchStatus = @@FETCH_STATUS

    END

	/**************************************************************************/
	/* Termination                                                            */
	/**************************************************************************/
	CLOSE TableCursor
	DEALLOCATE TableCursor

END