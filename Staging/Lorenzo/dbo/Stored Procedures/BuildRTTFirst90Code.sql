﻿CREATE procedure [dbo].[BuildRTTFirst90Code] as
/**
Author: K Oakden
Date: 22/10/2014
Additions to Non admitted PTL - 90 codes
Note that the 90 code additions are used for monthly and weekly RTT reporting only
Not the daily Non admitted PTL.

**/

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @RowsInserted int
	
select @StartTime = getdate()
if Object_id('tempdb..#refsWith90', 'U') is not null
	drop table #refsWith90

--if Object_id('tempdb..#first90Code', 'U') is not null
--	drop table #first90Code
	
--if Object_id('tempdb..#rtt', 'U') is not null
--	drop table #rtt
	
--select distinct ref_ref into #refsWith90
--from RTT.IPMRTTOutcomes 
--where RTTStatusCode in ('90', '91',' 92')


--All refs that have a code 90,91,92 and don't have a preceeding clock stop (30,31,32,34,35,36).
select distinct 
	ref_ref = REFRL_REFNO
	,RTTST_DATE
	,RTTST_REFNO
into #refsWith90
from dbo.RTT 
--where SORCE = 'REFRL' 
where ARCHV_FLAG = 'N'
and RTTST_REFNO in (
	3007090		--90 Post 1st Treatment
	,3007091	--91 During WW period
	,3007092	--92 Undergoing tests by GP
	)
and REFRL_REFNO not in (
	select distinct REFRL_REFNO
	from dbo.RTT rtt
	--where SORCE = 'REFRL' 
	where ARCHV_FLAG = 'N'
	and RTTST_REFNO in (
		3007090		--90 Post 1st Treatment
		,3007091	--91 During WW period
		,3007092	--92 Undergoing tests by GP
		)
	and exists (
		select 1 from dbo.RTT rtt30
		--where rtt30.SORCE = 'REFRL' 
		where rtt30.ARCHV_FLAG = 'N'
		and rtt.REFRL_REFNO = rtt30.REFRL_REFNO
		and rtt.RTTST_DATE > rtt30.RTTST_DATE
		and rtt30.RTTST_REFNO in (
			3007098	--30 Start 1st treatment	30
			,3007099	--31 Start WW by pt	31
			,3007100	--32 Start WW by clinician	32
			,3007087	--34 Decision not to treat	34
			,3007088	--35 Pt declined treatment	35
			,3007089	--36 Pt deceased	36
			)
	)
)

truncate table dbo.RTTFirst90Code
insert into dbo.RTTFirst90Code
select ref_ref, MIN(RTTST_DATE) as First90Code
--into dbo.RTTFirst90Code
from #refsWith90
where RTTST_REFNO in (
	3007090		--90 Post 1st Treatment
	,3007091	--91 During WW period
	,3007092	--92 Undergoing tests by GP
	)
group by ref_ref

--alter table dbo.RTTFirst90Code alter column ref_ref [numeric](18, 0) NOT NULL
--alter table dbo.RTTFirst90Code add PRIMARY KEY CLUSTERED (ref_ref ASC)



select @RowsInserted = @@rowcount

------------------------------------------------------------------------------------------------
----Write to log
------------------------------------------------------------------------------------------------
select @Elapsed = DATEDIFF(SECOND, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Secs'
	
exec Audit.WriteLogEvent 'Lorenzo - BuildRTTFirst90Code', @Stats, @StartTime
