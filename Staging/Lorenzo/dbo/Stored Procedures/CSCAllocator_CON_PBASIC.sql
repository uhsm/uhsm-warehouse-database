﻿create procedure [dbo].[CSCAllocator_CON_PBASIC] 
	@sorcecode varchar(10),
	@sorcerefno int
as 

SELECT  -- Manual Allocation Details  
  CASE  -- Referral refno - for checking if contract is locked on a waiting list (NOT Referrals)    
	  WHEN @sorcecode = 'PRCAE' 
	  THEN (SELECT REFRL_REFNO FROM dbo.ProfessionalCareEpisode WHERE PRCAE_REFNO = @sorcerefno)    
	  WHEN @sorcecode = 'SCHDL' 
	  THEN (SELECT REFRL_REFNO FROM dbo.ScheduleEvent WHERE SCHDL_REFNO = @sorcerefno)    
	  --WHEN @sorcecode = 'AEATT' 
	  --THEN (SELECT REFRL_REFNO FROM ae_attendances      WHERE aeatt_refno = @sorcerefno)    
	  WHEN @sorcecode = 'REFRL' 
	  THEN @sorcerefno    -- not required to select a value because this is already provided by the BASE dataset    
	  WHEN @sorcecode = 'WLIST' 
	  THEN (SELECT REFRL_REFNO FROM dbo.WaitingList WHERE WLIST_REFNO = @sorcerefno)  
  END   AS REFRL_REFNO,  
  -- Basic Patient Details  
  CASE    -- episode effective date    
	  WHEN @sorcecode = 'PRCAE' 
	  THEN (SELECT convert(varchar,isnull(END_DTTM,START_DTTM),112) FROM dbo.ProfessionalCareEpisode  WHERE PRCAE_REFNO = @sorcerefno)    
	  WHEN @sorcecode = 'SCHDL' 
	  THEN (SELECT convert(varchar,START_DTTM,112) FROM dbo.ScheduleEvent WHERE SCHDL_REFNO = @sorcerefno)    
	  --WHEN @sorcecode = 'AEATT' 
	  --THEN (SELECT convert(varchar,arrived_dttm,112)                FROM ae_attendances       WHERE aeatt_refno = @sorcerefno)    
	  WHEN @sorcecode = 'REFRL' 
	  THEN (SELECT convert(varchar,RECVD_DTTM,112) FROM dbo.Referral WHERE REFRL_REFNO = @sorcerefno)    
	  WHEN @sorcecode = 'WLIST' 
	  THEN (SELECT convert(varchar,WLIST_DTTM,112) FROM dbo.WaitingList WHERE WLIST_REFNO = @sorcerefno)  
  END    AS episode_dttm,  
  CASE   -- Administrative Category Refno Used to determine if patient is private. For inpatients use provider spell    
	  WHEN @sorcecode = 'PRCAE' 
	  THEN (SELECT ADCAT_REFNO FROM dbo.ProfessionalCareEpisode  WHERE PRCAE_REFNO = @sorcerefno)    
	  WHEN @sorcecode = 'SCHDL' 
	  THEN (SELECT ADCAT_REFNO FROM dbo.ScheduleEvent WHERE SCHDL_REFNO = @sorcerefno)    
	  --WHEN @sorcecode = 'AEATT' 
	  --THEN (SELECT rfval_refno FROM dbo.ReferenceValue    WHERE rfvdm_code  = 'ADCAT' AND main_code = '1' AND archv_flag = 'N') --RFVAL for NHS Patient    
	  WHEN @sorcecode = 'REFRL' 
	  THEN (SELECT ADCAT_REFNO FROM dbo.Referral  WHERE REFRL_REFNO = @sorcerefno)    
	  WHEN @sorcecode = 'WLIST' 
	  THEN (SELECT ADCAT_REFNO FROM dbo.WaitingList WHERE WLIST_REFNO = @sorcerefno)  
  END    AS adcat_refno,  
  CASE  -- patcl Patient Classification on Admission  -- Elective booked/planned emergency etc   prof carer episodes only    
	  WHEN @sorcecode = 'PRCAE' 
	  THEN (SELECT rfval.MAIN_CODE FROM dbo.ReferenceValue rfval 
		INNER JOIN dbo.ProviderSpell prvsp ON rfval.RFVAL_REFNO = prvsp.PATCL_REFNO 
		INNER JOIN dbo.ProfessionalCareEpisode prcae ON prvsp.PRVSP_REFNO = prcae.PRVSP_REFNO 
		WHERE PRCAE_REFNO = @sorcerefno)    
	  WHEN @sorcecode = 'SCHDL' 
	  THEN NULL    
	  --WHEN @sorcecode = 'AEATT' 
	  --THEN NULL    
	  WHEN @sorcecode = 'REFRL' 
	  THEN NULL    
	  WHEN @sorcecode = 'WLIST' 
	  THEN NULL  
  END   AS patcl_refno,  
  CASE  -- Visit_type     
	  WHEN @sorcecode = 'PRCAE' 
	  THEN NULL    
	  WHEN @sorcecode = 'SCHDL' 
	  THEN (SELECT rfval.MAIN_CODE FROM  dbo.ReferenceValue rfval with (nolock)
		INNER JOIN dbo.ScheduleEvent schdl with (nolock) ON schdl.VISIT_REFNO = rfval.RFVAL_REFNO 
		WHERE SCHDL_REFNO = @sorcerefno and rfval.RFVDM_CODE ='VISIT')    
	  --WHEN @sorcecode = 'AEATT' 
	  --THEN NULL    
	  WHEN @sorcecode = 'REFRL' 
	  THEN NULL    
	  WHEN @sorcecode = 'WLIST' 
	  THEN NULL  
  END   AS visit_type