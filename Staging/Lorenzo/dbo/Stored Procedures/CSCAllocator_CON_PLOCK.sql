﻿create procedure [dbo].[CSCAllocator_CON_PLOCK] 
	@sorcecode varchar(10),
	@sorcerefno int
as 


select   top 1 
  --cnsrv.citem_refno,  
  CASE  -- Purchase refno - for checking if contract is locked    
  WHEN callo.SORCE_CODE = 'PRCAE' 
  THEN (SELECT PURCH_REFNO FROM dbo.ProfessionalCareEpisode (nolock) WHERE PRCAE_REFNO= callo.SORCE_REFNO and ARCHV_FLAG = 'N')    
  WHEN callo.SORCE_CODE = 'SCHDL' 
  THEN (SELECT PURCH_REFNO FROM dbo.ScheduleEvent (nolock) WHERE SCHDL_REFNO= callo.SORCE_REFNO and ARCHV_FLAG = 'N')    
  --WHEN callo.SORCE_CODE = 'AEATT' 
  --THEN (SELECT PURCH_REFNO FROM AE_attendances (nolock) WHERE aeatt_refno= callo.SORCE_REFNO and ARCHV_FLAG = 'N')     
  WHEN callo.SORCE_CODE = 'REFRL' 
  THEN (SELECT PURCH_REFNO FROM dbo.Referral (nolock) WHERE REFRL_REFNO = callo.SORCE_REFNO and ARCHV_FLAG = 'N')    
  WHEN callo.SORCE_CODE = 'WLIST' 
  THEN (SELECT PURCH_REFNO FROM dbo.WaitingList (nolock) where WLIST_REFNO= callo.SORCE_REFNO and ARCHV_FLAG = 'N')  
  end   Purchaser_refno  
from    
  dbo.ContractAllocation callo with (nolock)  
  --,contract_services cnsrv with (nolock)  
--where  
  --callo.cnsrv_refno = cnsrv.cnsrv_refno  
where  callo.SORCE_CODE = @sorcecode  
and  callo.SORCE_REFNO = @sorcerefno  
and  callo.LOCKED_FLAG = 'Y'  
--and  isnull(cnsrv.ARCHV_FLAG, 'N') = 'N'"