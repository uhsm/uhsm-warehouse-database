﻿CREATE procedure [dbo].[FixTImportCSV] as

/******************************************************************************
 Modification History --------------------------------------------------------

 Date		Author			Change
10/06/2014	KO				Added fake last row to fix import error with carriage 
							return on last row
******************************************************************************/

declare @encounterRecno int
declare @dodgyEncounterRecno int

declare @row varchar(8000)
declare @dodgyRow varchar(8000)

declare @lastRowFiller varchar(10)
declare @FetchStatus int

set @lastRowFiller = 'KOWOSERE'

--put in temp row so no failure if carriage return on last record
insert into dbo.TImportCSV (Column0) values (@lastRowFiller)

delete
from
	dbo.TImportCSV
where
	Column0 like 'FILE_NAME,DATA_START_DATE,DATA_END_DATE,TOTAL_ROW_COUNT,%'


declare RowCursor CURSOR fast_forward FOR
select
	 Import.EncounterRecno
	,Import.Column0
from
	dbo.TImportCSV Import

inner join 
	(
	select
		 EncounterRecno
		,Column0
	from
		dbo.TImportCSV
	where
		Column0 not like 'RM2[_]%'
	) DodgyRecord
on	DodgyRecord.EncounterRecno = Import.EncounterRecno + 1

where
	Import.Column0 like 'RM2[_]%'


/**************************************************************************/
/* Initialisation                                                         */
/**************************************************************************/

OPEN RowCursor

FETCH NEXT FROM RowCursor INTO
	 @encounterRecno
	,@row

Select @FetchStatus = @@FETCH_STATUS


/**************************************************************************/
/* Main Processing Loop                                                   */
/**************************************************************************/
WHILE (@FetchStatus  <> -1) and (@FetchStatus  <> -2)
begin

	declare DodgyRowCursor CURSOR fast_forward FOR
	select
		 Import.EncounterRecno
		,Import.Column0
	from
		dbo.TImportCSV Import
	where
		Import.EncounterRecno > @encounterRecno
	and	Import.EncounterRecno <
		(
		select
			min(EncounterRecno)
		from
			dbo.TImportCSV LastDodgyRow
		where
			LastDodgyRow.EncounterRecno > @encounterRecno
		--and	LastDodgyRow.Column0 like 'RM2[_]%'
		and	(LastDodgyRow.Column0 like 'RM2[_]%'
			or LastDodgyRow.Column0 = @lastRowFiller)
		)

	OPEN DodgyRowCursor

	FETCH NEXT FROM DodgyRowCursor INTO
		 @dodgyEncounterRecno
		,@dodgyRow

	Select @FetchStatus = @@FETCH_STATUS


	WHILE (@FetchStatus  <> -1) and (@FetchStatus  <> -2)
	begin

		select
--			@row = @row + ' ' + @dodgyRow
			@row = @row + char(13) + char(10) + @dodgyRow

		FETCH NEXT FROM DodgyRowCursor INTO
			 @dodgyEncounterRecno
			,@dodgyRow

		Select @FetchStatus = @@FETCH_STATUS

	end

	update
		dbo.TImportCSV
	set
		Column0 = @row
	where
		EncounterRecno = @encounterRecno

	CLOSE DodgyRowCursor
	DEALLOCATE DodgyRowCursor



	FETCH NEXT FROM RowCursor INTO
		 @encounterRecno
		,@row

	Select @FetchStatus = @@FETCH_STATUS


end


CLOSE RowCursor
DEALLOCATE RowCursor


delete
from
	dbo.TImportCSV
where
	Column0 not like 'RM2[_]%'