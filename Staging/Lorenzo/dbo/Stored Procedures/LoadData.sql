﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:	WH - Load PAS - Load Data.

Notes:		Stored in 

Versions:
			1.0.0.6 - 31/03/2016 - CM 
				Added code for all new extracts required for Community PAS migration work
			
			1.0.0.5 - 21/01/2016 - MT
				Added code to capture how many rows are inserted overall.
				If any rows are inserted then create an audit record so that
				downstream sprocs know whether to run or not.

				Added Try-Catch.

				Removed code that updates patients table with title, ethnicity,
				marital status and sex and placed in new post load sproc:
				WH.PAS.uspPostLoadLorenzo_Patient.

				Moved call to Pseudonomisation.dbo.PseudonomiseIPM to
				WH.PAS.uspPostLoadLorenzo

			1.0.0.4 - 09/10/2015 - MT
				Removed call to dbo.BuildExcludedPatient.
				Not rebuilding this table every day.
				Created an alert for possible new test patients instead.

			1.0.0.3 - 13/11/2014 - KO
				Add PatientNew updates

			1.0.0.2 - 10/10/2014 - KO
				Add letters updates

			1.0.0.1 - 21/03/2012 - KO
				Rebuild excluded patient table first
		
			1.0.0.0 - 09/10/2009 - ??
				Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [dbo].[LoadData]
As

Begin Try

Declare @StartTime datetime
Declare @Elapsed int
Declare @Stats varchar(255)
Declare @RowsInserted int = 0

Set @StartTime = getdate()

delete from dbo.PatientAddress_old where exists (select 1 from dbo.TLoadPatientAddress where PatientAddress_old.ADDSS_REFNO = TLoadPatientAddress.ADDSS_REFNO)
Insert Into Debug(Details, EventDateTime) Values('LorenzoLoadData: LoadPatientAddress_old started', GETDATE())
insert into dbo.PatientAddress_old select * from dbo.TLoadPatientAddress
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.GpAddress where exists (select 1 from dbo.TLoadGpAddress where GpAddress.ADDSS_REFNO = TLoadGpAddress.ADDSS_REFNO)
Insert Into Debug(Details, EventDateTime) Values('LorenzoLoadData: LoadGpAddress started', GETDATE())
insert into dbo.GpAddress select * from dbo.TLoadGpAddress
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.AdmissionDecision_old where exists (select 1 from dbo.TLoadAdmissionDecision where AdmissionDecision_old.ADMDC_REFNO = TLoadAdmissionDecision.ADMDC_REFNO)
Insert Into Debug(Details, EventDateTime) Values('LorenzoLoadData: LoadAdmissionDecision_old started', GETDATE())
insert into dbo.AdmissionDecision_old select * from dbo.TLoadAdmissionDecision
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.AdmissionOffer_old where exists (select 1 from dbo.TLoadAdmissionOffer where AdmissionOffer_old.ADMOF_REFNO = TLoadAdmissionOffer.ADMOF_REFNO)
Insert Into Debug(Details, EventDateTime) Values('LorenzoLoadData: LoadAdmissionOffer_old started', GETDATE())
insert into dbo.AdmissionOffer_old select * from dbo.TLoadAdmissionOffer
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.AllocationSQL where exists (select 1 from dbo.TLoadAllocationSQL where AllocationSQL.ALDST_REFNO = TLoadAllocationSQL.ALDST_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: LoadAllocationSQL started', GETDATE())
insert into dbo.AllocationSQL select * from dbo.TLoadAllocationSQL
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.AllocationFailure where exists (select 1 from dbo.TLoadAllocationFailure where AllocationFailure.ALFLR_REFNO = TLoadAllocationFailure.ALFLR_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: LoadAllocationFailure started', GETDATE())
insert into dbo.AllocationFailure select * from dbo.TLoadAllocationFailure
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.AllocationRule where exists (select 1 from dbo.TLoadAllocationRule where AllocationRule.ALOCR_REFNO = TLoadAllocationRule.ALOCR_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: LoadAllocationRule started', GETDATE())
insert into dbo.AllocationRule select * from dbo.TLoadAllocationRule
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.AllocatorRequest where exists (select 1 from dbo.TLoadAllocatorRequest where AllocatorRequest.ALREQ_REFNO = TLoadAllocatorRequest.ALREQ_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: LoadAllocatorRequest started', GETDATE())
insert into dbo.AllocatorRequest select * from dbo.TLoadAllocatorRequest
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.AllocationRuleVersion where exists (select 1 from dbo.TLoadAllocationRuleVersion where AllocationRuleVersion.ALVER_REFNO = TLoadAllocationRuleVersion.ALVER_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: AllocationRuleVersion started', GETDATE())
insert into dbo.AllocationRuleVersion select * from dbo.TLoadAllocationRuleVersion
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.APCStay where exists (select 1 from dbo.TLoadAPCStay where APCStay.APCST_REFNO = TLoadAPCStay.APCST_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: APCStay started', GETDATE())
insert into dbo.APCStay select * from dbo.TLoadAPCStay
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.AugmentedCarePeriod where exists (select 1 from dbo.TLoadAugmentedCarePeriod where AugmentedCarePeriod.AUGCP_REFNO = TLoadAugmentedCarePeriod.AUGCP_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: AugmentedCarePeriod started', GETDATE())
insert into dbo.AugmentedCarePeriod select * from dbo.TLoadAugmentedCarePeriod
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ContractAllocation where exists (select 1 from dbo.TLoadContractAllocation where ContractAllocation.CALLO_REFNO = TLoadContractAllocation.CALLO_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ContractAllocation started', GETDATE())
insert into dbo.ContractAllocation select * from dbo.TLoadContractAllocation
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.CasenoteActivity where exists (select 1 from dbo.TLoadCasenoteActivity where CasenoteActivity.CASAC_REFNO = TLoadCasenoteActivity.CASAC_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: CasenoteActivity started', GETDATE())
insert into dbo.CasenoteActivity select * from dbo.TLoadCasenoteActivity
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.Casenote where exists (select 1 from dbo.TLoadCasenote where Casenote.CASNT_REFNO = TLoadCasenote.CASNT_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: Casenote started', GETDATE())
insert into dbo.Casenote select * from dbo.TLoadCasenote
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.CasenoteVolume where exists (select 1 from dbo.TLoadCasenoteVolume where CasenoteVolume.CASVL_REFNO = TLoadCasenoteVolume.CASVL_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: CasenoteVolume started', GETDATE())
insert into dbo.CasenoteVolume select * from dbo.TLoadCasenoteVolume
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ContractAllocationHistory where exists (select 1 from dbo.TLoadContractAllocationHistory where ContractAllocationHistory.COAHS_REFNO = TLoadContractAllocationHistory.COAHS_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ContractAllocationHistory started', GETDATE())
insert into dbo.ContractAllocationHistory select * from dbo.TLoadContractAllocationHistory
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.Contract where exists (select 1 from dbo.TLoadContract where Contract.CONTR_REFNO = TLoadContract.CONTR_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: Contract started', GETDATE())
insert into dbo.Contract select * from dbo.TLoadContract
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ClinicalCoding where exists (select 1 from dbo.TLoadClinicalCoding where ClinicalCoding.DGPRO_REFNO = TLoadClinicalCoding.DGPRO_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ClinicalCoding started', GETDATE())
insert into dbo.ClinicalCoding select * from dbo.TLoadClinicalCoding
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ContractRule where exists (select 1 from dbo.TLoadContractRule where ContractRule.DPRUL_REFNO = TLoadContractRule.DPRUL_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ContractRule started', GETDATE())
insert into dbo.ContractRule select * from dbo.TLoadContractRule
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.DependantSurrogacy where exists (select 1 from dbo.TLoadDependantSurrogacy where DependantSurrogacy.DPSUR_REFNO = TLoadDependantSurrogacy.DPSUR_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: DependantSurrogacy started', GETDATE())
insert into dbo.DependantSurrogacy select * from dbo.TLoadDependantSurrogacy
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.OrganisationAccess where exists (select 1 from dbo.TLoadOrganisationAccess where OrganisationAccess.HEOAC_REFNO = TLoadOrganisationAccess.HEOAC_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: OrganisationAccess started', GETDATE())
insert into dbo.OrganisationAccess select * from dbo.TLoadOrganisationAccess
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.Organisation where exists (select 1 from dbo.TLoadOrganisation where Organisation.HEORG_REFNO = TLoadOrganisation.HEORG_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: Organisation started', GETDATE())
insert into dbo.Organisation select * from dbo.TLoadOrganisation
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.HomeLeave where exists (select 1 from dbo.TLoadHomeLeave where HomeLeave.HOMEL_REFNO = TLoadHomeLeave.HOMEL_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: HomeLeave started', GETDATE())
insert into dbo.HomeLeave select * from dbo.TLoadHomeLeave
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.LocalDatasetItem where exists (select 1 from dbo.TLoadLocalDatasetItem where LocalDatasetItem.LCDSI_REFNO = TLoadLocalDatasetItem.LCDSI_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: LocalDatasetItem started', GETDATE())
insert into dbo.LocalDatasetItem select * from dbo.TLoadLocalDatasetItem
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.LocalData where exists (select 1 from dbo.TLoadLocalData where LocalData.LCLDA_REFNO = TLoadLocalData.LCLDA_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: LocalData started', GETDATE())
insert into dbo.LocalData select * from dbo.TLoadLocalData
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.LocalDataItem where exists (select 1 from dbo.TLoadLocalDataItem where LocalDataItem.LCLDI_REFNO = TLoadLocalDataItem.LCLDI_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: LocalDataItem started', GETDATE())
insert into dbo.LocalDataItem select * from dbo.TLoadLocalDataItem
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.LocalDataset where exists (select 1 from dbo.TLoadLocalDataset where LocalDataset.LCLDS_REFNO = TLoadLocalDataset.LCLDS_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: LocalDataset started', GETDATE())
insert into dbo.LocalDataset select * from dbo.TLoadLocalDataset
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.MaternitySpell where exists (select 1 from dbo.TLoadMaternitySpell where MaternitySpell.MATSP_REFNO = TLoadMaternitySpell.MATSP_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: MaternitySpell started', GETDATE())
insert into dbo.MaternitySpell select * from dbo.TLoadMaternitySpell
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.MergeActivity_old where exists (select 1 from dbo.TLoadMergeActivity where MergeActivity_old.MGACT_REFNO = TLoadMergeActivity.MGACT_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: MergeActivity_old started', GETDATE())
insert into dbo.MergeActivity_old select * from dbo.TLoadMergeActivity
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.MergePatient where exists (select 1 from dbo.TLoadMergePatient where MergePatient.MGPAT_REFNO = TLoadMergePatient.MGPAT_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: MergePatient started', GETDATE())
insert into dbo.MergePatient select * from dbo.TLoadMergePatient
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.NoteHistory where exists (select 1 from dbo.TLoadNoteHistory where NoteHistory.NOTEH_REFNO = TLoadNoteHistory.NOTEH_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: NoteHistory started', GETDATE())
insert into dbo.NoteHistory select * from dbo.TLoadNoteHistory
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.NoteRole where exists (select 1 from dbo.TLoadNoteRole where NoteRole.NOTRL_REFNO = TLoadNoteRole.NOTRL_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: NoteRole started', GETDATE())
insert into dbo.NoteRole select * from dbo.TLoadNoteRole
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.Diagnosis_old where exists (select 1 from dbo.TLoadDiagnosis where Diagnosis_old.ODPCD_REFNO = TLoadDiagnosis.ODPCD_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: Diagnosis_old started', GETDATE())
insert into dbo.Diagnosis_old select * from dbo.TLoadDiagnosis
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.OverseasVisitorStatus where exists (select 1 from dbo.TLoadOverseasVisitorStatus where OverseasVisitorStatus.OVSEA_REFNO = TLoadOverseasVisitorStatus.OVSEA_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: OverseasVisitorStatus started', GETDATE())
insert into dbo.OverseasVisitorStatus select * from dbo.TLoadOverseasVisitorStatus
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.PatientPersonalCarer where exists (select 1 from dbo.TLoadPatientPersonalCarer where PatientPersonalCarer.PAPCA_REFNO = TLoadPatientPersonalCarer.PAPCA_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: PatientPersonalCarer started', GETDATE())
insert into dbo.PatientPersonalCarer select * from dbo.TLoadPatientPersonalCarer
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.PatientAlias where exists (select 1 from dbo.TLoadPatientAlias where PatientAlias.PATAL_REFNO = TLoadPatientAlias.PATAL_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: PatientAlias started', GETDATE())
insert into dbo.PatientAlias select * from dbo.TLoadPatientAlias
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.PatientIdentifier where exists (select 1 from dbo.TLoadPatientIdentifier where PatientIdentifier.PATID_REFNO = TLoadPatientIdentifier.PATID_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: PatientIdentifier started', GETDATE())
insert into dbo.PatientIdentifier select * from dbo.TLoadPatientIdentifier
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.PatientProfessionalCarer where exists (select 1 from dbo.TLoadPatientProfessionalCarer where PatientProfessionalCarer.PATPC_REFNO = TLoadPatientProfessionalCarer.PATPC_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: PatientProfessionalCarer started', GETDATE())
insert into dbo.PatientProfessionalCarer select * from dbo.TLoadPatientProfessionalCarer
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ProfessionalCarerStaffTeam where exists (select 1 from dbo.TLoadProfessionalCarerStaffTeam where ProfessionalCarerStaffTeam.PCSTT_REFNO = TLoadProfessionalCarerStaffTeam.PCSTT_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ProfessionalCarerStaffTeam started', GETDATE())
insert into dbo.ProfessionalCarerStaffTeam select * from dbo.TLoadProfessionalCarerStaffTeam
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.PersonalCarer where exists (select 1 from dbo.TLoadPersonalCarer where PersonalCarer.PERCA_REFNO = TLoadPersonalCarer.PERCA_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: PersonalCarer started', GETDATE())
insert into dbo.PersonalCarer select * from dbo.TLoadPersonalCarer
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.PeriodOfCare where exists (select 1 from dbo.TLoadPeriodOfCare where PeriodOfCare.POCAR_REFNO = TLoadPeriodOfCare.POCAR_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: PeriodOfCare started', GETDATE())
insert into dbo.PeriodOfCare select * from dbo.TLoadPeriodOfCare
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ProfessionalCareEpisode_old where exists (select 1 from dbo.TLoadProfessionalCareEpisode where ProfessionalCareEpisode_old.PRCAE_REFNO = TLoadProfessionalCareEpisode.PRCAE_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ProfessionalCareEpisode_old started', GETDATE())
insert into dbo.ProfessionalCareEpisode_old select * from dbo.TLoadProfessionalCareEpisode
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ProfessionalCareIdentifier where exists (select 1 from dbo.TLoadProfessionalCareIdentifier where ProfessionalCareIdentifier.PRCAI_REFNO = TLoadProfessionalCareIdentifier.PRCAI_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ProfessionalCareIdentifier started', GETDATE())
insert into dbo.ProfessionalCareIdentifier select * from dbo.TLoadProfessionalCareIdentifier
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ProfessionalCarerSpecialty where exists (select 1 from dbo.TLoadProfessionalCarerSpecialty where ProfessionalCarerSpecialty.PRCAS_REFNO = TLoadProfessionalCarerSpecialty.PRCAS_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ProfessionalCarerSpecialty started', GETDATE())
insert into dbo.ProfessionalCarerSpecialty select * from dbo.TLoadProfessionalCarerSpecialty
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ProfessionalCarerMap where exists (select 1 from dbo.TLoadProfessionalCarerMap where ProfessionalCarerMap.PRCMP_REFNO = TLoadProfessionalCarerMap.PRCMP_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ProfessionalCarerMap started', GETDATE())
insert into dbo.ProfessionalCarerMap select * from dbo.TLoadProfessionalCarerMap
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ProfessionalCarer where exists (select 1 from dbo.TLoadProfessionalCarer where ProfessionalCarer.PROCA_REFNO = TLoadProfessionalCarer.PROCA_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ProfessionalCarer started', GETDATE())
insert into dbo.ProfessionalCarer select * from dbo.TLoadProfessionalCarer
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.Participate where exists (select 1 from dbo.TLoadParticipate where Participate.PRTCP_REFNO = TLoadParticipate.PRTCP_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: Participate started', GETDATE())
insert into dbo.Participate select * from dbo.TLoadParticipate
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ProviderSpell_old where exists (select 1 from dbo.TLoadProviderSpell where ProviderSpell_old.PRVSP_REFNO = TLoadProviderSpell.PRVSP_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ProviderSpell_old started', GETDATE())
insert into dbo.ProviderSpell_old select * from dbo.TLoadProviderSpell
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.Purchaser where exists (select 1 from dbo.TLoadPurchaser where Purchaser.PURCH_REFNO = TLoadPurchaser.PURCH_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: Purchaser started', GETDATE())
insert into dbo.Purchaser select * from dbo.TLoadPurchaser
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ReferralHistory where exists (select 1 from dbo.TLoadReferralHistory where ReferralHistory.REFHS_REFNO = TLoadReferralHistory.REFHS_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ReferralHistory started', GETDATE())
insert into dbo.ReferralHistory select * from dbo.TLoadReferralHistory
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.Referral_old where exists (select 1 from dbo.TLoadReferral where Referral_old.REFRL_REFNO = TLoadReferral.REFRL_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: Referral_old started', GETDATE())
insert into dbo.Referral_old select * from dbo.TLoadReferral
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.RTT where exists (select 1 from dbo.TLoadRTT where RTT.RTTPR_REFNO = TLoadRTT.RTTPR_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: RTT started', GETDATE())
insert into dbo.RTT select * from dbo.TLoadRTT
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ReferenceValue where exists (select 1 from dbo.TLoadReferenceValue where ReferenceValue.RFVAL_REFNO = TLoadReferenceValue.RFVAL_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ReferenceValue started', GETDATE())
insert into dbo.ReferenceValue select * from dbo.TLoadReferenceValue
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ReferenceValueDomain where exists (select 1 from dbo.TLoadReferenceValueDomain where ReferenceValueDomain.RFVDM_REFNO = TLoadReferenceValueDomain.RFVDM_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ReferenceValueDomain started', GETDATE())
insert into dbo.ReferenceValueDomain select * from dbo.TLoadReferenceValueDomain
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ReferenceValueDependancy where exists (select 1 from dbo.TLoadReferenceValueDependancy where ReferenceValueDependancy.RFVLD_REFNO = TLoadReferenceValueDependancy.RFVLD_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ReferenceValueDependancy started', GETDATE())
insert into dbo.ReferenceValueDependancy select * from dbo.TLoadReferenceValueDependancy
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ReferenceValueIdentifier where exists (select 1 from dbo.TLoadReferenceValueIdentifier where ReferenceValueIdentifier.RFVLI_REFNO = TLoadReferenceValueIdentifier.RFVLI_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ReferenceValueIdentifier started', GETDATE())
insert into dbo.ReferenceValueIdentifier select * from dbo.TLoadReferenceValueIdentifier
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.Birth where exists (select 1 from dbo.TLoadBirth where Birth.RGBIR_REFNO = TLoadBirth.RGBIR_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: Birth started', GETDATE())
insert into dbo.Birth select * from dbo.TLoadBirth
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.PatientAddressRole_old where exists (select 1 from dbo.TLoadPatientAddressRole where PatientAddressRole_old.ROLES_REFNO = TLoadPatientAddressRole.ROLES_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: PatientAddressRole_old started', GETDATE())
insert into dbo.PatientAddressRole_old select * from dbo.TLoadPatientAddressRole
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.[Rule] where exists (select 1 from dbo.TLoadRule where [Rule].RULES_REFNO = TLoadRule.RULES_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: Rule started', GETDATE())
insert into dbo.[Rule] select * from dbo.TLoadRule
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ScheduleHistory where exists (select 1 from dbo.TLoadScheduleHistory where ScheduleHistory.SCDHS_REFNO = TLoadScheduleHistory.SCDHS_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ScheduleHistory started', GETDATE())
insert into dbo.ScheduleHistory select * from dbo.TLoadScheduleHistory
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ScheduleEvent_old where exists (select 1 from dbo.TLoadScheduleEvent where ScheduleEvent_old.SCHDL_REFNO = TLoadScheduleEvent.SCHDL_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ScheduleEvent_old started', GETDATE())
insert into dbo.ScheduleEvent_old select * from dbo.TLoadScheduleEvent
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.SchedulePersonalCarer where exists (select 1 from dbo.TLoadSchedulePersonalCarer where SchedulePersonalCarer.SCHPC_REFNO = TLoadSchedulePersonalCarer.SCHPC_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: SchedulePersonalCarer started', GETDATE())
insert into dbo.SchedulePersonalCarer select * from dbo.TLoadSchedulePersonalCarer
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.SharedCare where exists (select 1 from dbo.TLoadSharedCare where SharedCare.SHARC_REFNO = TLoadSharedCare.SHARC_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: SharedCare started', GETDATE())
insert into dbo.SharedCare select * from dbo.TLoadSharedCare
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.SharedServicePoint where exists (select 1 from dbo.TLoadSharedServicePoint where SharedServicePoint.SHSPT_REFNO = TLoadSharedServicePoint.SHSPT_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: SharedServicePoint started', GETDATE())
insert into dbo.SharedServicePoint select * from dbo.TLoadSharedServicePoint
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.Specialty where exists (select 1 from dbo.TLoadSpecialty where Specialty.SPECT_REFNO = TLoadSpecialty.SPECT_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: Specialty started', GETDATE())
insert into dbo.Specialty select * from dbo.TLoadSpecialty
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.SpecialtyIdentifier where exists (select 1 from dbo.TLoadSpecialtyIdentifier where SpecialtyIdentifier.SPEID_REFNO = TLoadSpecialtyIdentifier.SPEID_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: SpecialtyIdentifier started', GETDATE())
insert into dbo.SpecialtyIdentifier select * from dbo.TLoadSpecialtyIdentifier
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ServicePointLink where exists (select 1 from dbo.TLoadServicePointLink where ServicePointLink.SPLIN_REFNO = TLoadServicePointLink.SPLIN_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ServicePointLink started', GETDATE())
insert into dbo.ServicePointLink select * from dbo.TLoadServicePointLink
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ServicePoint_old where exists (select 1 from dbo.TLoadServicePoint where ServicePoint_old.SPONT_REFNO = TLoadServicePoint.SPONT_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ServicePoint_old started', GETDATE())
insert into dbo.ServicePoint_old select * from dbo.TLoadServicePoint
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ServicePointProfile where exists (select 1 from dbo.TLoadServicePointProfile where ServicePointProfile.SPPRO_REFNO = TLoadServicePointProfile.SPPRO_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ServicePointProfile started', GETDATE())
insert into dbo.ServicePointProfile select * from dbo.TLoadServicePointProfile
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ServicePointSlot_old where exists (select 1 from dbo.TLoadServicePointSlot where ServicePointSlot_old.SPSLT_REFNO = TLoadServicePointSlot.SPSLT_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ServicePointSlot_old started', GETDATE())
insert into dbo.ServicePointSlot_old select * from dbo.TLoadServicePointSlot
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ServicePointSession where exists (select 1 from dbo.TLoadServicePointSession where ServicePointSession.SPSSN_REFNO = TLoadServicePointSession.SPSSN_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ServicePointSession started', GETDATE())
insert into dbo.ServicePointSession select * from dbo.TLoadServicePointSession
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ServicePointSuspension where exists (select 1 from dbo.TLoadServicePointSuspension where ServicePointSuspension.SPSUS_REFNO = TLoadServicePointSuspension.SPSUS_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ServicePointSuspension started', GETDATE())
insert into dbo.ServicePointSuspension select * from dbo.TLoadServicePointSuspension
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.ServicePointStay where exists (select 1 from dbo.TLoadServicePointStay where ServicePointStay.SSTAY_REFNO = TLoadServicePointStay.SSTAY_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ServicePointStay started', GETDATE())
insert into dbo.ServicePointStay select * from dbo.TLoadServicePointStay
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.StaffTeam where exists (select 1 from dbo.TLoadStaffTeam where StaffTeam.STEAM_REFNO = TLoadStaffTeam.STEAM_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: StaffTeam started', GETDATE())
insert into dbo.StaffTeam select * from dbo.TLoadStaffTeam
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.[User] where exists (select 1 from dbo.TLoadUser where [User].USERS_REFNO = TLoadUser.USERS_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: User started', GETDATE())
insert into dbo.[User] select * from dbo.TLoadUser
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.UserScreen where exists (select 1 from dbo.TLoadUserScreen where UserScreen.USSCR_REFNO = TLoadUserScreen.USSCR_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: UserScreen started', GETDATE())
insert into dbo.UserScreen select * from dbo.TLoadUserScreen
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.UserScreenForm where exists (select 1 from dbo.TLoadUserScreenForm where UserScreenForm.USSFR_REFNO = TLoadUserScreenForm.USSFR_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: UserScreenForm started', GETDATE())
insert into dbo.UserScreenForm select * from dbo.TLoadUserScreenForm
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.UserScreenObject where exists (select 1 from dbo.TLoadUserScreenObject where UserScreenObject.USSOB_REFNO = TLoadUserScreenObject.USSOB_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: UserScreenObject started', GETDATE())
insert into dbo.UserScreenObject select * from dbo.TLoadUserScreenObject
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.WardProfile where exists (select 1 from dbo.TLoadWardProfile where WardProfile.WDPRO_REFNO = TLoadWardProfile.WDPRO_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: WardProfile started', GETDATE())
insert into dbo.WardProfile select * from dbo.TLoadWardProfile
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.WaitingList_old where exists (select 1 from dbo.TLoadWaitingList where WaitingList_old.WLIST_REFNO = TLoadWaitingList.WLIST_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: WaitingList_old started', GETDATE())
insert into dbo.WaitingList_old select * from dbo.TLoadWaitingList
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.WaitingListReview where exists (select 1 from dbo.TLoadWaitingListReview where WaitingListReview.WLREV_REFNO = TLoadWaitingListReview.WLREV_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: WaitingListReview started', GETDATE())
insert into dbo.WaitingListReview select * from dbo.TLoadWaitingListReview
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.WaitingListRule_old where exists (select 1 from dbo.TLoadWaitingListRule where WaitingListRule_old.WLRUL_REFNO = TLoadWaitingListRule.WLRUL_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: WaitingListRule_old started', GETDATE())
insert into dbo.WaitingListRule_old select * from dbo.TLoadWaitingListRule
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.WaitingListReviewBatch where exists (select 1 from dbo.TLoadWaitingListReviewBatch where WaitingListReviewBatch.WLRVB_REFNO = TLoadWaitingListReviewBatch.WLRVB_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: WaitingListReviewBatch started', GETDATE())
insert into dbo.WaitingListReviewBatch select * from dbo.TLoadWaitingListReviewBatch
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.WaitingListTransfer_old where exists (select 1 from dbo.TLoadWaitingListTransfer where WaitingListTransfer_old.WLTFR_REFNO = TLoadWaitingListTransfer.WLTFR_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: WaitingListTransfer_old started', GETDATE())
insert into dbo.WaitingListTransfer_old select * from dbo.TLoadWaitingListTransfer
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.WaitingListSuspension where exists (select 1 from dbo.TLoadWaitingListSuspension where WaitingListSuspension.WSUSP_REFNO = TLoadWaitingListSuspension.WSUSP_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: WaitingListSuspension started', GETDATE())
insert into dbo.WaitingListSuspension select * from dbo.TLoadWaitingListSuspension
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.Letter where exists (select 1 from dbo.TLoadLetter where Letter.LETTR_REFNO = TLoadLetter.LETTR_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: Letter started', GETDATE())
insert into dbo.Letter select * from dbo.TLoadLetter
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.LetterAudit where exists (select 1 from dbo.TLoadLetterAudit where LetterAudit.LTAUD_REFNO = TLoadLetterAudit.LTAUD_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: LetterAudit started', GETDATE())
insert into dbo.LetterAudit select * from dbo.TLoadLetterAudit
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.LetterAuthorship where exists (select 1 from dbo.TLoadLetterAuthorship where LetterAuthorship.LTAUT_REFNO = TLoadLetterAuthorship.LTAUT_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: LetterAuthorship started', GETDATE())
insert into dbo.LetterAuthorship select * from dbo.TLoadLetterAuthorship
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.LetterConfiguration where exists (select 1 from dbo.TLoadLetterConfiguration where LetterConfiguration.LTCON_REFNO = TLoadLetterConfiguration.LTCON_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: LetterConfiguration started', GETDATE())
insert into dbo.LetterConfiguration select * from dbo.TLoadLetterConfiguration
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.LetterDatasets where exists (select 1 from dbo.TLoadLetterDatasets where LetterDatasets.LTDAT_REFNO = TLoadLetterDatasets.LTDAT_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: LetterDatasets started', GETDATE())
insert into dbo.LetterDatasets select * from dbo.TLoadLetterDatasets
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.LetterDefinition where exists (select 1 from dbo.TLoadLetterDefinition where LetterDefinition.LTDEF_REFNO = TLoadLetterDefinition.LTDEF_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: LetterDefinition started', GETDATE())
insert into dbo.LetterDefinition select * from dbo.TLoadLetterDefinition
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.LetterMergeField where exists (select 1 from dbo.TLoadLetterMergeField where LetterMergeField.LTMGF_REFNO = TLoadLetterMergeField.LTMGF_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: LetterMergeField started', GETDATE())
insert into dbo.LetterMergeField select * from dbo.TLoadLetterMergeField
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.LetterParameter where exists (select 1 from dbo.TLoadLetterParameter where LetterParameter.LTPRM_REFNO = TLoadLetterParameter.LTPRM_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: LetterParameter started', GETDATE())
insert into dbo.LetterParameter select * from dbo.TLoadLetterParameter
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.LetterRecipient where exists (select 1 from dbo.TLoadLetterRecipient where LetterRecipient.LTREC_REFNO = TLoadLetterRecipient.LTREC_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: LetterRecipient started', GETDATE())
insert into dbo.LetterRecipient select * from dbo.TLoadLetterRecipient
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.PatientOld where exists (select 1 from dbo.TLoadPatientOld where PatientOld.PATNT_REFNO = TLoadPatientOld.PATNT_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: PatientOld started', GETDATE())
insert into dbo.PatientOld select * from dbo.TLoadPatientOld
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

delete from dbo.Patient where exists (select 1 from dbo.TLoadPatient where Patient.PATNT_REFNO = TLoadPatient.PATNT_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: Patient started', GETDATE())insert into dbo.Patient select * from dbo.TLoadPatient
Set @RowsInserted = @RowsInserted + @@ROWCOUNT





--Roles
delete from dbo.PatientAddressRole where exists (select 1 from dbo.TLoadPatientAddressRoleNew where PatientAddressRole.ROLES_REFNO = TLoadPatientAddressRoleNew.ROLES_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: PatientAddressRole started', GETDATE())
insert into dbo.PatientAddressRole select * from dbo.TLoadPatientAddressRoleNew
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

--Addss
delete from dbo.PatientAddress where exists (select 1 from dbo.TLoadPatientAddressNew where PatientAddress.ADDSS_REFNO = TLoadPatientAddressNew.ADDSS_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: PatientAddress started', GETDATE())
insert into dbo.PatientAddress select * from dbo.TLoadPatientAddressNew
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

--AdmDc
delete from dbo.AdmissionDecision where exists (select 1 from dbo.TLoadAdmissionDecisionNew where AdmissionDecision.ADMDC_REFNO = TLoadAdmissionDecisionNew.ADMDC_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: AdmissionDecision started', GETDATE())
insert into dbo.AdmissionDecision select * from dbo.TLoadAdmissionDecisionNew
Set @RowsInserted = @RowsInserted + @@ROWCOUNT


--AdmOf
delete from dbo.AdmissionOffer where exists (select 1 from dbo.TLoadAdmissionOfferNew where AdmissionOffer.ADMOF_REFNO = TLoadAdmissionOfferNew.ADMOF_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: AdmissionOffer started', GETDATE())
insert into dbo.AdmissionOffer select * from dbo.TLoadAdmissionOfferNew
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

--MGACT
delete from dbo.MergeActivity where exists (select 1 from dbo.TLoadMergeActivityNew where MergeActivity.MGACT_REFNO = TLoadMergeActivityNew.MGACT_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: MergeActivity started', GETDATE())
insert into dbo.MergeActivity select * from dbo.TLoadMergeActivityNew
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

--ODPCD
delete from dbo.Diagnosis where exists (select 1 from dbo.TLoadODPCDCodes where Diagnosis.ODPCD_REFNO = TLoadODPCDCodes.ODPCD_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: Diagnosis started', GETDATE())
insert into dbo.Diagnosis select * from dbo.TLoadODPCDCodes
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

--PRCAE
delete from dbo.ProfessionalCareEpisode where exists (select 1 from dbo.TLoadProfessionalCareEpisodeNew where ProfessionalCareEpisode.PRCAE_REFNO = TLoadProfessionalCareEpisodeNew.PRCAE_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ProfessionalCareEpisode started', GETDATE())
insert into dbo.ProfessionalCareEpisode select * from dbo.TLoadProfessionalCareEpisodeNew
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

--PCAHO
delete from dbo.ProfCarerHealthOrgs where exists (select 1 from dbo.TLoadProfCarerHealthOrgs where ProfCarerHealthOrgs.PCAHO_REFNO = TLoadProfCarerHealthOrgs.PCAHO_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ProfCarerHealthOrgs started', GETDATE())
insert into dbo.ProfCarerHealthOrgs select * from dbo.TLoadProfCarerHealthOrgs
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

--PRVSP
delete from dbo.ProviderSpell where exists (select 1 from dbo.TLoadProviderSpellNew where ProviderSpell.PRVSP_REFNO = TLoadProviderSpellNew.PRVSP_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ProviderSpell started', GETDATE())
insert into dbo.ProviderSpell select * from dbo.TLoadProviderSpellNew
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

--REFRL
delete from dbo.Referral where exists (select 1 from dbo.TLoadReferralNew where Referral.REFRL_REFNO = TLoadReferralNew.REFRL_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: Referral started', GETDATE())
insert into dbo.Referral select * from dbo.TLoadReferralNew
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

--SCHDL
delete from dbo.ScheduleEvent where exists (select 1 from dbo.TLoadScheduleEventNew where ScheduleEvent.SCHDL_REFNO = TLoadScheduleEventNew.SCHDL_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ScheduleEvent started', GETDATE())
insert into dbo.ScheduleEvent select * from dbo.TLoadScheduleEventNew
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

--SPSLT

delete from dbo.ServicePointSlot where exists (select 1 from dbo.TLoadServicePointSlotNew where ServicePointSlot.SPSLT_REFNO = TLoadServicePointSlotNew.SPSLT_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ServicePointSlot started', GETDATE())
insert into dbo.ServicePointSlot select * from dbo.TLoadServicePointSlotNew
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

--SPONT
delete from dbo.ServicePoint where exists (select 1 from dbo.TLoadServicePointNew where ServicePoint.SPONT_REFNO = TLoadServicePointNew.SPONT_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ServicePoint started', GETDATE())
insert into dbo.ServicePoint select * from dbo.TLoadServicePointNew
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

--WLIST

delete from dbo.WaitingList where exists (select 1 from dbo.TLoadWaitingListNew where WaitingList.WLIST_REFNO = TLoadWaitingListNew.WLIST_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: WaitingList started', GETDATE())
insert into dbo.WaitingList select * from dbo.TLoadWaitingListNew
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

--WLRUL
delete from dbo.WaitingListRule where exists (select 1 from dbo.TLoadWaitingListRuleNew where WaitingListRule.WLRUL_REFNO = TLoadWaitingListRuleNew.WLRUL_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: WaitingListRule started', GETDATE())
insert into dbo.WaitingListRule select * from dbo.TLoadWaitingListRuleNew
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

--WLTFR

delete from dbo.WaitingListTransfer where exists (select 1 from dbo.TLoadWaitingListTransferNew where WaitingListTransfer.WLTFR_REFNO = TLoadWaitingListTransferNew.WLTFR_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: WaitingListTransfer started', GETDATE())
insert into dbo.WaitingListTransfer select * from dbo.TLoadWaitingListTransferNew
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

/*********New Feeds**********/
--ASSDT
delete from dbo.AssessmentDetails where exists (select 1 from dbo.TLoadAssessmentDetails where AssessmentDetails.ASSDT_REFNO = TLoadAssessmentDetails.ASSDT_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: AssessmentDetails started', GETDATE())
insert into dbo.AssessmentDetails select * from dbo.TLoadAssessmentDetails
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

--ASSES
delete from dbo.Assessment where exists (select 1 from dbo.TLoadAssessment where Assessment.ASSES_REFNO = TLoadAssessment.ASSES_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: Assessment started', GETDATE())
insert into dbo.Assessment select * from dbo.TLoadAssessment
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

--CLENT
delete from dbo.CaseloadEntries where exists (select 1 from dbo.TLoadCaseloadEntries where CaseloadEntries.CLENT_REFNO = TLoadCaseloadEntries.CLENT_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: CaseloadEntries started', GETDATE())
insert into dbo.CaseloadEntries select * from dbo.TLoadCaseloadEntries
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

--CASHI
delete from dbo.CasenoteHistories where exists (select 1 from dbo.TloadCasenoteHistories where CasenoteHistories.CASHI_REFNO = TloadCasenoteHistories.CASHI_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: CasenoteHistories started', GETDATE())
insert into dbo.CasenoteHistories select * from dbo.TloadCasenoteHistories
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

--DPHOL
delete from dbo.DependantHolidays where exists (select 1 from dbo.TLoadDependantHolidays where DependantHolidays.DPHOL_REFNO = TLoadDependantHolidays.DPHOL_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: DependantHolidays started', GETDATE())
insert into dbo.DependantHolidays select * from dbo.TLoadDependantHolidays
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

 --DPRES
delete from dbo.DependantResources where exists (select 1 from dbo.TLoadDependantResources where DependantResources.DPRES_REFNO = TLoadDependantResources.DPRES_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: DependantResources started', GETDATE())
insert into dbo.DependantResources select * from dbo.TLoadDependantResources
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

 --MHCEP
delete from dbo.MentalHealthCareEpisodes where exists (select 1 from dbo.TLoadMentalHealthCareEpisodes where MentalHealthCareEpisodes.MHCEP_REFNO = TLoadMentalHealthCareEpisodes.MHCEP_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: MentalHealthCareEpisodes started', GETDATE())
insert into dbo.MentalHealthCareEpisodes select * from dbo.TLoadMentalHealthCareEpisodes
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

 --MHRVW
delete from dbo.MentalHealthReviews where exists (select 1 from dbo.TLoadMentalHealthReviews where MentalHealthReviews.MHRVW_REFNO = TLoadMentalHealthReviews.MHRVW_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: MentalHealthReviews started', GETDATE())
insert into dbo.MentalHealthReviews select * from dbo.TLoadMentalHealthReviews
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

 --MHSEC
delete from dbo.MHASections where exists (select 1 from dbo.TLoadMHASections where MHASections.MHSEC_REFNO = TLoadMHASections.MHSEC_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: MHASections started', GETDATE())
insert into dbo.MHASections select * from dbo.TLoadMHASections
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

 --NOTES
delete from dbo.Notes where exists (select 1 from dbo.TLoadNotes where Notes.NOTES_REFNO = TLoadNotes.NOTES_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: Notes started', GETDATE())
insert into dbo.Notes select * from dbo.TLoadNotes
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

 --ODPPL
delete from dbo.ODPCDPickLists where exists (select 1 from dbo.TLoadODPCDPickLists where ODPCDPickLists.ODPPL_REFNO = TLoadODPCDPickLists.ODPPL_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: ODPCDPickLists started', GETDATE())
insert into dbo.ODPCDPickLists select * from dbo.TLoadODPCDPickLists
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

 --PAASD
delete from dbo.PatientAssessmentDetails where exists (select 1 from dbo.TLoadPatientAssessmentDetails where PatientAssessmentDetails.PAASD_REFNO = TLoadPatientAssessmentDetails.PAASD_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: PatientAssessmentDetails started', GETDATE())
insert into dbo.PatientAssessmentDetails select * from dbo.TLoadPatientAssessmentDetails
Set @RowsInserted = @RowsInserted + @@ROWCOUNT


 --PAASS
delete from dbo.PatientAssessments where exists (select 1 from dbo.TLoadPatientAssessments where PatientAssessments.PAASS_REFNO = TLoadPatientAssessments.PAASS_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: PatientAssessments started', GETDATE())
insert into dbo.PatientAssessments select * from dbo.TLoadPatientAssessments
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

 --PATCT
delete from dbo.PatientConsentComments where exists (select 1 from dbo.TLoadPatientConsentComments where PatientConsentComments.PATCT_REFNO = TLoadPatientConsentComments.PATCT_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: PatientConsentComments started', GETDATE())
insert into dbo.PatientConsentComments select * from dbo.TLoadPatientConsentComments
Set @RowsInserted = @RowsInserted + @@ROWCOUNT


 --PATRN
delete from dbo.PatientTransportations where exists (select 1 from dbo.TLoadPatientTransportations where PatientTransportations.PATRN_REFNO = TLoadPatientTransportations.PATRN_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: PatientTransportations started', GETDATE())
insert into dbo.PatientTransportations select * from dbo.TLoadPatientTransportations
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

 --RTTHS
delete from dbo.RTTHistories where exists (select 1 from dbo.TLoadRTTHistories where RTTHistories.RTTHS_REFNO = TLoadRTTHistories.RTTHS_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: RTTHistories started', GETDATE())
insert into dbo.RTTHistories select * from dbo.TLoadRTTHistories
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

 --PATCP
delete from dbo.PatientCarePlans where exists (select 1 from dbo.TLoadPatientCarePlans where PatientCarePlans.PATCP_REFNO = TLoadPatientCarePlans.PATCP_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: PatientCarePlans started', GETDATE())
insert into dbo.PatientCarePlans select * from dbo.TLoadPatientCarePlans
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

 --PCACT
delete from dbo.PatientCarePlanActivities where exists (select 1 from dbo.TLoadPatientCarePlanActivities where PatientCarePlanActivities.PCPAT_REFNO = TLoadPatientCarePlanActivities.PCPAT_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: PatientCarePlanActivities started', GETDATE())
insert into dbo.PatientCarePlanActivities select * from dbo.TLoadPatientCarePlanActivities
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

 --PCHST
delete from dbo.PatientCarePlanHistories where exists (select 1 from dbo.TLoadPatientCarePlanHistories where PatientCarePlanHistories.PCHST_REFNO = TLoadPatientCarePlanHistories.PCHST_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: PatientCarePlanHistories started', GETDATE())
insert into dbo.PatientCarePlanHistories select * from dbo.TLoadPatientCarePlanHistories
Set @RowsInserted = @RowsInserted + @@ROWCOUNT

 --PCPST
delete from dbo.PatientCarePlanStrategies where exists (select 1 from dbo.TLoadPatientCarePlanStrategies where PatientCarePlanStrategies.PCPST_REFNO = TLoadPatientCarePlanStrategies.PCPST_REFNO)
Insert Into   Debug(Details, EventDateTime) Values('LorenzoLoadData: PatientCarePlanStrategies started', GETDATE())
insert into dbo.PatientCarePlanStrategies select * from dbo.TLoadPatientCarePlanStrategies
Set @RowsInserted = @RowsInserted + @@ROWCOUNT







Update	dbo.Parameter
Set		DateValue = @StartTime
Where	Parameter = 'DATALOADDATE'

If @@rowcount = 0
Begin
	Insert Into dbo.Parameter(Parameter,DateValue)
	Select	Parameter = 'DATALOADDATE'
			,DateValue = @StartTime
End

Select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

Select @Stats = 'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

Exec Audit.WriteLogEvent 'PAS - Lorenzo LoadLorenzoData', @Stats, @StartTime

If @RowsInserted > 0
Begin
	Set @Stats = 'Rows inserted: ' + Convert(varchar(10),@RowsInserted)
	Exec Audit.WriteLogEvent 'PAS - Load Lorenzo Data - Rows Affected',@Stats,@StartTime
End

End Try

Begin Catch
	Exec msdb.dbo.sp_send_dbmail
	@profile_name = 'Business Intelligence',
	@recipients = 'Business.Intelligence@uhsm.nhs.uk',
	@subject = 'Load Lorenzo Data',
	@body_format = 'HTML',
	@body = 'dbo.LoadData failed.<br><br>';
End Catch