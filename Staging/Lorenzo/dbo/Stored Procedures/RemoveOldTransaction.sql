﻿CREATE procedure [dbo].[RemoveOldTransaction] as

delete
from
	dbo.TImportCSVParsed
where
	not exists
	(
	select
		1
	from
		dbo.TImportCSVParsed Latest
	where
		Latest.Valid = 1
	and	not exists
		(
		select
			1
		from
			dbo.TImportCSVParsed Previous
		where
			Previous.Valid = 1
		and	Previous.ExtractCode = Latest.ExtractCode
		and	Previous.Column5 = Latest.Column5
		and	(
				Previous.Column2 > Latest.Column2
			or	(
					Previous.Column2 = Latest.Column2
				and	Previous.EncounterRecno > Latest.EncounterRecno
				)
			)
		)
	and	Latest.EncounterRecno = TImportCSVParsed.EncounterRecno
	)