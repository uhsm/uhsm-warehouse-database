﻿CREATE procedure [dbo].[TImportCSVParseOld]
as

truncate table dbo.TImportCSVParsed

set nocount on

BEGIN

	/**************************************************************************/
	/* General Declarations                                                   */
	/**************************************************************************/
	declare @FetchStatus int
	declare @row varchar(8000)
	declare @sql varchar(8000)
	declare @insertSql varchar(8000)
	declare @columnsSql varchar(8000)
	declare @valuesSql varchar(8000)
	declare @columnValue varchar(255)
	declare @delimiter char(1)
	declare @textDelimiter char(1)
	declare @column int
	declare @fieldStart int
	declare @fieldEnd int
	declare @encounterRecno int
	declare @AttributeColumnCount int
	declare @created varchar(50)
	declare @extractCode varchar(255)


	/**************************************************************************/
	/* Declaration for trigger cursor                                         */
	/**************************************************************************/
	declare RowCursor CURSOR fast_forward FOR
	select
		 EncounterRecno
		,Column0 = Column0 + ',#CHECKSUM#'

		,AttributeColumnCount =
			(
			select
				count(*)
			from
				dbo.Attribute

			inner join dbo.Extract
			on	Extract.ExtractTableCode = Attribute.ExtractTableCode
			and	Extract.Active = 1

			where
				TImportCSV.Column0 like ExtractFilePrefix + '%'
			) + 5 --adjustment for prefix columns

		,Extract.ExtractCode
	from
		dbo.TImportCSV

	inner join dbo.Extract
	on	TImportCSV.Column0 like Extract.ExtractFilePrefix + '%'
	and	Extract.Active = 1


	/**************************************************************************/
	/* Initialisation                                                         */
	/**************************************************************************/
	Select
		 @delimiter  = ','
		,@textDelimiter = '"'
		,@insertSql = 'insert into dbo.TImportCSVParsed (EncounterRecno,'
		,@created = convert(varchar, getdate(), 113)


	/**************************************************************************/
	/* Initialisation                                                         */
	/**************************************************************************/
	OPEN RowCursor

	FETCH NEXT FROM RowCursor INTO
		 @encounterRecno
		,@row
		,@AttributeColumnCount
		,@extractCode

	Select @FetchStatus = @@FETCH_STATUS


    /**************************************************************************/
    /* Main Processing Loop                                                   */
    /**************************************************************************/
    WHILE (@FetchStatus  <> -1) and (@FetchStatus  <> -2)
    BEGIN

	--ignore blank lines
	if @row != ''
		begin

		select
			 @column = 0
			,@fieldStart = 1
			,@valuesSql = convert(varchar, @encounterRecno) + ','

		select
			 @fieldEnd = 
				case
				when charindex(@delimiter, @row, @fieldStart) = 0 
				then datalength(@row) - @fieldStart
				else
					case
					when substring(@row, @fieldStart, 1) = @textDelimiter
					then charindex(@textDelimiter, @row, @fieldStart + 1) + 1
					else charindex(@delimiter, @row, @fieldStart)
					end
				end

			,@columnsSql =
				'Column' + convert(varchar, @column)

		--loop through the row
		while charindex(@delimiter, @row, @fieldStart) > 0
			begin

				select
					@columnValue = substring(@row, @fieldStart, @fieldEnd - @fieldStart)

				select
					@valuesSql =
						@valuesSql +

						case @columnValue
						when '' then 'null'
						else '''' + replace(replace(@columnValue, '''', ''''''), '"', '') + ''''
						end

						+ ','

				select
					 @column = @column + 1
					,@fieldStart = @fieldEnd + 1

				select
					 @fieldEnd = 
						case
						when charindex(@delimiter, @row, @fieldStart) = 0 
						then datalength(@row) - @fieldStart
						else
							case
							when substring(@row, @fieldStart, 1) = @textDelimiter
							then charindex(@textDelimiter, @row, @fieldStart + 1) + 1
							else charindex(@delimiter, @row, @fieldStart)
							end
						end
					,@columnsSql = 
						@columnsSql + ',Column' + convert(varchar, @column)

--found a dodgy record so print details and continue
				if @column > 255
				begin
					print @row
					select
						@fieldStart = datalength(@row)
				end
			end

		select
			@sql =
				@insertSql + 
				left(
					 @columnsSql
					,datalength(@columnsSql) - datalength(',Column' + convert(varchar, @column))
				) + 
				',Valid ,ExtractCode ,Created) ' +
				'values (' + left(@valuesSql, datalength(@valuesSql) -1) +
				case
				when @column = @AttributeColumnCount --correct number of columns
				then ',1'
				else ',0'
				end +

				', ''' + @extractCode + '''' +
				', ''' + @created + ''')'

		print @sql
		exec (@sql)

		end

	FETCH NEXT FROM RowCursor INTO
		 @encounterRecno
		,@row
		,@AttributeColumnCount
		,@extractCode

	Select @FetchStatus = @@FETCH_STATUS

    END

	/**************************************************************************/
	/* Termination                                                            */
	/**************************************************************************/
	CLOSE RowCursor
	DEALLOCATE RowCursor

END