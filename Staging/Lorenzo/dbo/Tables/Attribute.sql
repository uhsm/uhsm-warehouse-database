﻿CREATE TABLE [dbo].[Attribute](
	[ExtractTableCode] [varchar](255) NOT NULL,
	[AttributeCode] [varchar](255) NOT NULL,
	[Attribute] [varchar](255) NULL,
	[AttributeDataType] [varchar](255) NULL,
	[AttributePrecision] [smallint] NULL,
	[AttributeLength] [smallint] NULL,
	[AttributeOrder] [smallint] NULL,
	[AttributeColumnName] [varchar](255) NULL,
 CONSTRAINT [PK_Attribute] PRIMARY KEY CLUSTERED 
(
	[ExtractTableCode] ASC,
	[AttributeCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_Attribute_Attribute]    Script Date: 07/04/2016 10:24:05 ******/
ALTER TABLE [dbo].[Attribute] ADD  CONSTRAINT [FK_Attribute_Attribute] FOREIGN KEY([ExtractTableCode], [AttributeCode])
REFERENCES [dbo].[Attribute] ([ExtractTableCode], [AttributeCode])
GO

ALTER TABLE [dbo].[Attribute] CHECK CONSTRAINT [FK_Attribute_Attribute]