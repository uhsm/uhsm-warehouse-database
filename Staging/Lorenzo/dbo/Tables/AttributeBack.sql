﻿CREATE TABLE [dbo].[AttributeBack](
	[ExtractTableCode] [varchar](255) NOT NULL,
	[AttributeCode] [varchar](255) NOT NULL,
	[Attribute] [varchar](255) NULL,
	[AttributeDataType] [varchar](255) NULL,
	[AttributePrecision] [smallint] NULL,
	[AttributeLength] [smallint] NULL,
	[AttributeOrder] [smallint] NULL,
	[AttributeColumnName] [varchar](255) NULL
) ON [PRIMARY]