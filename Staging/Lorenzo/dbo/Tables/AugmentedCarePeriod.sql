﻿CREATE TABLE [dbo].[AugmentedCarePeriod](
	[AUGCP_REFNO] [int] NOT NULL,
	[PRVSP_REFNO] [numeric](18, 0) NULL,
	[PRCAE_REFNO] [numeric](18, 0) NULL,
	[ACP_NUMBER] [numeric](18, 0) NULL,
	[START_DTTM] [smalldatetime] NULL,
	[END_DTTM] [smalldatetime] NULL,
	[APCSO_REFNO] [int] NULL,
	[ICU_DAYS] [numeric](18, 0) NULL,
	[HDU_DAYS] [numeric](18, 0) NULL,
	[APCLO_REFNO] [int] NULL,
	[ORGANS_SUPPORTED] [char](1) NULL,
	[PROCA_REFNO] [int] NULL,
	[SPECT_REFNO] [int] NULL,
	[PLANNED_FLAG] [char](1) NULL,
	[APCOC_REFNO] [int] NULL,
	[APCDI_REFNO] [int] NULL,
	[USER_CREATE] [varchar](30) NULL,
	[USER_MODIF] [varchar](30) NULL,
	[CREATE_DTTM] [datetime] NULL,
	[MODIF_DTTM] [datetime] NULL,
	[ARCHV_FLAG] [varchar](1) NULL,
	[STRAN_REFNO] [numeric](18, 0) NULL,
	[PRIOR_POINTER] [int] NULL,
	[EXTERNAL_KEY] [varchar](20) NULL,
	[CCASO_REFNO] [int] NULL,
	[CCATY_REFNO] [int] NULL,
	[CCDDE_REFNO] [int] NULL,
	[CCDLO_REFNO] [int] NULL,
	[CCDST_REFNO] [int] NULL,
	[CCSLO_REFNO] [int] NULL,
	[CCUBC_REFNO] [int] NULL,
	[CCUFU_REFNO] [int] NULL,
	[CCP_FLAG] [char](1) NULL,
	[READY_DTTM] [smalldatetime] NULL,
	[OWNER_HEORG_REFNO] [int] NULL,
	[Created] [datetime] NULL,
 CONSTRAINT [PK_dbo_AugmentedCarePeriod_AUGCP_REFNO] PRIMARY KEY CLUSTERED 
(
	[AUGCP_REFNO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Created_AugmentedCarePeriod] ON [dbo].[AugmentedCarePeriod] 
(
	[Created] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]