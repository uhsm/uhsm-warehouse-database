﻿CREATE TABLE [dbo].[Birth](
	[RGBIR_REFNO] [int] NOT NULL,
	[MATSP_REFNO] [int] NULL,
	[PATNT_REFNO] [int] NULL,
	[MTHER_REFNO] [int] NULL,
	[PROCA_REFNO] [int] NULL,
	[BIRTH_ORDER] [numeric](18, 0) NULL,
	[DTTM_OF_BIRTH] [smalldatetime] NULL,
	[SEXXX_REFNO] [int] NULL,
	[DPSTS_REFNO] [int] NULL,
	[BIRST_REFNO] [int] NULL,
	[DEPLA_REFNO] [int] NULL,
	[RESME_REFNO] [int] NULL,
	[RESMD_REFNO] [int] NULL,
	[BIRTH_WEIGHT] [numeric](18, 0) NULL,
	[APGAR_ONEMIN] [numeric](18, 0) NULL,
	[APGAR_FIVEMIN] [numeric](18, 0) NULL,
	[BCGAD_REFNO] [int] NULL,
	[CIRCM_HEAD] [numeric](18, 0) NULL,
	[BIRTH_LENGTH] [numeric](18, 0) NULL,
	[EXHIP_REFNO] [int] NULL,
	[FOLUP_REFNO] [int] NULL,
	[FPRES_REFNO] [int] NULL,
	[METSC_REFNO] [int] NULL,
	[JAUND_REFNO] [int] NULL,
	[FDTYP_REFNO] [int] NULL,
	[DELME_REFNO] [int] NULL,
	[MTDRG_REFNO] [int] NULL,
	[GESTN_LENGTH] [numeric](18, 0) NULL,
	[APGAR_TENMIN] [int] NULL,
	[ARCHV_FLAG] [varchar](1) NULL,
	[STRAN_REFNO] [numeric](18, 0) NULL,
	[REFRL_REFNO] [int] NULL,
	[PRIOR_POINTER] [int] NULL,
	[USER_CREATE] [varchar](30) NULL,
	[USER_MODIF] [varchar](30) NULL,
	[CREATE_DTTM] [datetime] NULL,
	[MODIF_DTTM] [datetime] NULL,
	[EXTERNAL_KEY] [varchar](20) NULL,
	[NNLVL_REFNO] [int] NULL,
	[OWNER_HEORG_REFNO] [int] NULL,
	[Created] [datetime] NULL,
 CONSTRAINT [PK_dbo_Birth_RGBIR_REFNO] PRIMARY KEY CLUSTERED 
(
	[RGBIR_REFNO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Birth] ON [dbo].[Birth] 
(
	[MATSP_REFNO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Birth_1] ON [dbo].[Birth] 
(
	[PATNT_REFNO] ASC,
	[DTTM_OF_BIRTH] ASC,
	[ARCHV_FLAG] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Created_Birth] ON [dbo].[Birth] 
(
	[Created] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]