﻿CREATE TABLE [dbo].[CurrentCasenoteLocationLastMoved](
	[SourcePatientNo] [int] NULL,
	[CasenoteUniqueID] [int] NOT NULL,
	[CasenoteNo] [varchar](30) NULL,
	[CurrentLocationUniqueID] [int] NULL,
	[CurrentCasenoteLocationCode] [varchar](20) NULL,
	[CurrentCasenoteLocation] [varchar](255) NULL,
	[CasenoteActivityUniqueID] [int] NOT NULL,
	[RequestStatusUniqueID] [int] NULL,
	[Received] [varchar](1) NOT NULL,
	[CasenoteMovementCommets] [varchar](255) NULL
) ON [PRIMARY]