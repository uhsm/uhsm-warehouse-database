﻿CREATE TABLE [dbo].[Debug](
	[DebugID] [int] IDENTITY(1,1) NOT NULL,
	[EventDateTime] [datetime] NOT NULL,
	[Details] [varchar](500) NOT NULL
) ON [PRIMARY]