﻿CREATE TABLE [dbo].[ExcludedPatient](
	[SourcePatientNo] [int] NOT NULL,
	[PatientTitle] [varchar](80) NULL,
	[PatientForename] [varchar](30) NULL,
	[PatientSurname] [varchar](30) NULL,
	[DateOfBirth] [datetime] NULL,
	[NHSNumber] [varchar](20) NULL,
	[DateCreated] [datetime] NULL,
 CONSTRAINT [PK_ExcludedPatient] PRIMARY KEY CLUSTERED 
(
	[SourcePatientNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]