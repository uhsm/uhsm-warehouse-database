﻿CREATE TABLE [dbo].[ExcludedPatientTest](
	[SourcePatientNo] [int] NOT NULL,
	[PatientTitle] [varchar](80) NULL,
	[PatientForename] [varchar](30) NULL,
	[PatientSurname] [varchar](30) NOT NULL,
	[DateOfBirth] [datetime] NULL,
	[NHSNumber] [varchar](20) NULL,
	[DistrictNo] [varchar](20) NULL,
	[ArchiveFlag] [varchar](1) NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL
) ON [PRIMARY]