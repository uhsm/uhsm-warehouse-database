﻿CREATE TABLE [dbo].[Extract](
	[ExtractCode] [varchar](10) NOT NULL,
	[Extract] [varchar](255) NULL,
	[ExtractTableCode] [varchar](255) NULL,
	[ExtractFilePrefix] [varchar](255) NULL,
	[Active] [bit] NULL,
	[ActiveLetter] [bit] NULL,
	[ViewName] [varchar](255) NULL,
 CONSTRAINT [PK_Extract] PRIMARY KEY CLUSTERED 
(
	[ExtractCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_Extract_ExtractTable]    Script Date: 07/04/2016 10:24:05 ******/
ALTER TABLE [dbo].[Extract] ADD  CONSTRAINT [FK_Extract_ExtractTable] FOREIGN KEY([ExtractTableCode])
REFERENCES [dbo].[ExtractTable] ([ExtractTableCode])
GO

ALTER TABLE [dbo].[Extract] CHECK CONSTRAINT [FK_Extract_ExtractTable]