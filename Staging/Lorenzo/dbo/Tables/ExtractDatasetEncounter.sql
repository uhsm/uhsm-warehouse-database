﻿CREATE TABLE [dbo].[ExtractDatasetEncounter](
	[DatasetCode] [varchar](20) NOT NULL,
	[SourceUniqueID] [int] NOT NULL,
 CONSTRAINT [PK_ExtractDatasetEncounter] PRIMARY KEY CLUSTERED 
(
	[DatasetCode] ASC,
	[SourceUniqueID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]