﻿CREATE TABLE [dbo].[ExtractTable](
	[ExtractTableCode] [varchar](255) NOT NULL,
	[ExtractTable] [varchar](255) NULL,
 CONSTRAINT [PK_ExtractTable] PRIMARY KEY CLUSTERED 
(
	[ExtractTableCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]