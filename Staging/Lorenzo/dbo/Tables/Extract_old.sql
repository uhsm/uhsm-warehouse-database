﻿CREATE TABLE [dbo].[Extract_old](
	[ExtractCode] [varchar](10) NOT NULL,
	[Extract] [varchar](255) NULL,
	[ExtractTableCode] [varchar](255) NULL,
	[ExtractFilePrefix] [varchar](255) NULL,
	[Active] [bit] NULL,
	[ActiveLetter] [bit] NULL,
	[ViewName] [varchar](255) NULL
) ON [PRIMARY]