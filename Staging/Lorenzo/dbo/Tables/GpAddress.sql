﻿CREATE TABLE [dbo].[GpAddress](
	[ADDSS_REFNO] [int] NOT NULL,
	[ADTYP_CODE] [varchar](5) NULL,
	[LINE1] [varchar](50) NULL,
	[LINE2] [varchar](50) NULL,
	[LINE3] [varchar](50) NULL,
	[LINE4] [varchar](50) NULL,
	[PCODE] [varchar](25) NULL,
	[HDIST_CODE] [varchar](10) NULL,
	[CNTRY_REFNO] [int] NULL,
	[CNTRY_REFNO_MAIN_CODE] [varchar](25) NULL,
	[CNTRY_REFNO_DESCRIPTION] [varchar](80) NULL,
	[START_DTTM] [datetime] NULL,
	[END_DTTM] [smalldatetime] NULL,
	[CREATE_DTTM] [datetime] NULL,
	[MODIF_DTTM] [datetime] NULL,
	[USER_CREATE] [varchar](30) NULL,
	[USER_MODIF] [varchar](30) NULL,
	[ARCHV_FLAG] [varchar](1) NULL,
	[EXTERNAL_KEY] [varchar](20) NULL,
	[PCG_CODE] [varchar](10) NULL,
	[OWNER_HEORG_REFNO] [int] NULL,
	[OWNER_HEORG_REFNO_MAIN_IDENT] [varchar](25) NULL,
	[PROCA_REFNO] [int] NULL,
	[PROCA_REFNO_MAIN_IDENT] [varchar](25) NULL,
	[ROTYP_CODE] [varchar](25) NULL,
	[ROTYP_CODE_DESCRIPTION] [varchar](80) NULL,
	[Created] [datetime] NULL,
 CONSTRAINT [PK_dbo_GpAddress_ADDSS_REFNO] PRIMARY KEY CLUSTERED 
(
	[ADDSS_REFNO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]