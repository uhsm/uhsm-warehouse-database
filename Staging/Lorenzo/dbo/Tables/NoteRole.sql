﻿CREATE TABLE [dbo].[NoteRole](
	[NOTRL_REFNO] [numeric](18, 0) NOT NULL,
	[NOTES_REFNO] [numeric](18, 0) NULL,
	[NOTES_REFNO_KEY_WORDS] [varchar](100) NULL,
	[NOTES_REFNO_NOTE] [varchar](2000) NULL,
	[SORCE_REFNO] [numeric](18, 0) NULL,
	[SORCE_CODE] [varchar](5) NULL,
	[START_DTTM] [datetime] NULL,
	[END_DTTM] [datetime] NULL,
	[USER_CREATE] [varchar](30) NULL,
	[USER_MODIF] [varchar](30) NULL,
	[CREATE_DTTM] [datetime] NULL,
	[MODIF_DTTM] [datetime] NULL,
	[SERIAL] [numeric](18, 0) NULL,
	[ARCHV_FLAG] [char](1) NULL,
	[NRTYP_REFNO] [numeric](18, 0) NULL,
	[NRTYP_REFNO_MAIN_CODE] [varchar](25) NULL,
	[NRTYP_REFNO_DESCRIPTION] [varchar](80) NULL,
	[OWNER_HEORG_REFNO] [numeric](18, 0) NULL,
	[OWNER_HEORG_REFNO_MAIN_IDENT] [varchar](25) NULL,
	[Created] [datetime] NULL,
 CONSTRAINT [PK_dbo_NoteRole_NOTRL_REFNO] PRIMARY KEY CLUSTERED 
(
	[NOTRL_REFNO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Created_NoteRole] ON [dbo].[NoteRole] 
(
	[Created] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_NoteRole] ON [dbo].[NoteRole] 
(
	[SORCE_CODE] ASC,
	[SORCE_REFNO] ASC,
	[ARCHV_FLAG] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]