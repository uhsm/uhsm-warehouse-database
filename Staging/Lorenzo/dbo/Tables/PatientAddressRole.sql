﻿CREATE TABLE [dbo].[PatientAddressRole](
	[ROLES_REFNO] [int] NOT NULL,
	[PROCA_REFNO] [int] NULL,
	[PATNT_REFNO] [int] NULL,
	[PERCA_REFNO] [int] NULL,
	[HEORG_REFNO] [int] NULL,
	[ADDSS_REFNO] [int] NULL,
	[ROTYP_CODE] [varchar](25) NULL,
	[CORRESPONDENCE] [char](1) NULL,
	[START_DTTM] [datetime] NULL,
	[END_DTTM] [datetime] NULL,
	[DEPRT_PATRN_REFNO] [int] NULL,
	[CURNT_FLAG] [varchar](1) NULL,
	[HOMEL_REFNO] [int] NULL,
	[PURCH_REFNO] [int] NULL,
	[PROVD_REFNO] [int] NULL,
	[SECURE_FLAG] [char](1) NOT NULL,
	[PATPC_REFNO] [int] NULL,
	[PERSS_REFNO] [int] NULL,
	[ORDRR_REFNO] [int] NULL,
	[CREATE_DTTM] [datetime] NULL,
	[MODIF_DTTM] [datetime] NULL,
	[USER_CREATE] [varchar](50) NULL,
	[USER_MODIF] [varchar](50) NULL,
	[ARCHV_FLAG] [char](1) NULL,
	[STRAN_REFNO] [int] NULL,
	[PRIOR_POINTER] [int] NULL,
	[EXTERNAL_KEY] [varchar](50) NULL,
	[BFORM_REFNO] [int] NULL,
	[SYN_CODE] [varchar](50) NULL,
	[PDS_IDENTIFIER] [varchar](50) NULL,
	[REFRL_REFNO] [int] NULL,
	[DECOUPLE_FLAG] [char](1) NULL,
	[OWNER_HEORG_REFNO] [int] NULL,
	[Created] [datetime] NULL,
 CONSTRAINT [PK_dbo_PatientAddressRoleNew_ROLES_REFNO] PRIMARY KEY NONCLUSTERED 
(
	[ROLES_REFNO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Created_PatientAddressRoleNew] ON [dbo].[PatientAddressRole]
(
	[Created] ASC
)
GO
