﻿CREATE TABLE [dbo].[PatientAssessmentDetails](
	[PAASD_REFNO] [int] NOT NULL,
	[PAASS_REFNO] [int] NULL,
	[ASSDT_REFNO] [int] NULL,
	[SCORE] [varchar](255) NULL,
	[COMMENTS] [varchar](255) NULL,
	[PROBLEM_FLAG] [char](1) NULL,
	[CREATE_DTTM] [datetime] NULL,
	[MODIF_DTTM] [datetime] NULL,
	[USER_CREATE] [varchar](50) NOT NULL,
	[USER_MODIF] [varchar](50) NOT NULL,
	[ARCHV_FLAG] [char](1) NULL,
	[STRAN_REFNO] [int] NULL,
	[PRIOR_POINTER] [int] NULL,
	[EXTERNAL_KEY] [varchar](50) NULL,
	[ACTIVE] [char](1) NULL,
	[OWNER_HEORG_REFNO] [int] NULL,
	[Created] [datetime] NULL,
 CONSTRAINT [PK_dbo_PatientAssessmentDetails_PAASD_REFNO] PRIMARY KEY CLUSTERED 
(
	[PAASD_REFNO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]