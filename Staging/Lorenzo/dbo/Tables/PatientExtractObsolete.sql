﻿CREATE TABLE [dbo].[PatientExtractObsolete](
	[table_name] [sysname] NOT NULL,
	[COLUMN_NAME] [sysname] NULL,
	[DATA_TYPE] [nvarchar](128) NULL,
	[ORDINAL_POSITION] [int] NULL,
	[Notes] [varchar](500) NULL
) ON [PRIMARY]