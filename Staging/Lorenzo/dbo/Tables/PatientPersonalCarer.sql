﻿CREATE TABLE [dbo].[PatientPersonalCarer](
	[PAPCA_REFNO] [numeric](18, 0) NOT NULL,
	[PETYP_REFNO] [numeric](18, 0) NULL,
	[RELTN_REFNO] [numeric](18, 0) NULL,
	[PERCA_REFNO] [numeric](18, 0) NULL,
	[PATNT_REFNO] [numeric](18, 0) NULL,
	[START_DTTM] [datetime] NULL,
	[END_DTTM] [datetime] NULL,
	[CURNT_FLAG] [char](1) NULL,
	[CREATE_DTTM] [datetime] NULL,
	[MODIF_DTTM] [datetime] NULL,
	[USER_CREATE] [varchar](30) NULL,
	[USER_MODIF] [varchar](30) NULL,
	[CNTCT_ALLOWED] [char](1) NULL,
	[ARCHV_FLAG] [char](1) NULL,
	[STRAN_REFNO] [numeric](18, 0) NULL,
	[PRIOR_POINTER] [numeric](18, 0) NULL,
	[EXTERNAL_KEY] [varchar](20) NULL,
	[INFOR_REFNO] [numeric](18, 0) NULL,
	[LGRSP_REFNO] [numeric](18, 0) NULL,
	[LEGAL_RESPONSIBILITY] [char](1) NULL,
	[USE_PATIENT_ADDRESS] [char](1) NULL,
	[SEND_PATNT_CORRES_TO] [char](1) NULL,
	[OWNER_HEORG_REFNO] [numeric](18, 0) NULL,
	[BABY_LINK] [char](1) NULL,
	[Created] [datetime] NULL,
 CONSTRAINT [PK_dbo_PatientPersonalCarer_PAPCA_REFNO] PRIMARY KEY CLUSTERED 
(
	[PAPCA_REFNO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Created_PatientPersonalCarer] ON [dbo].[PatientPersonalCarer] 
(
	[Created] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]