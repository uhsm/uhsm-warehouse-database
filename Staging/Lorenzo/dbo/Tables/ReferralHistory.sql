﻿CREATE TABLE [dbo].[ReferralHistory](
	[REFHS_REFNO] [int] NOT NULL,
	[STRAN_REFNO] [numeric](18, 0) NULL,
	[sorce_code] [varchar](10) NULL,
	[PATNT_REFNO] [int] NULL,
	[REFRL_REFNO] [int] NULL,
	[DGPRO_REFNO] [int] NULL,
	[RFUNP_REFNO] [int] NULL,
	[start_dttm] [smalldatetime] NULL,
	[end_dttm] [smalldatetime] NULL,
	[Driving_table] [varchar](50) NULL,
	[User_create] [varchar](30) NULL,
	[Create_dttm] [datetime] NULL,
	[User_modif] [varchar](30) NULL,
	[MODIF_DTTM] [datetime] NULL,
	[ARCHV_FLAG] [char](1) NULL,
	[comments] [varchar](500) NULL,
	[OWNER_HEORG_REFNO] [int] NULL,
	[Created] [datetime] NULL,
 CONSTRAINT [PK_dbo_ReferralHistory_REFHS_REFNO] PRIMARY KEY CLUSTERED 
(
	[REFHS_REFNO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Created_ReferralHistory] ON [dbo].[ReferralHistory] 
(
	[Created] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]