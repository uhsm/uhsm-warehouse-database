﻿CREATE TABLE [dbo].[ScheduleHistory](
	[SCDHS_REFNO] [int] NOT NULL,
	[WLIST_REFNO] [numeric](18, 0) NULL,
	[OLD_SPECT_REFNO] [int] NULL,
	[NEW_SPECT_REFNO] [int] NULL,
	[STRAN_REFNO] [numeric](18, 0) NULL,
	[SCHDL_REFNO] [numeric](18, 0) NULL,
	[OLD_START_DTTM] [smalldatetime] NULL,
	[OLD_END_DTTM] [smalldatetime] NULL,
	[NEW_START_DTTM] [smalldatetime] NULL,
	[NEW_END_DTTM] [smalldatetime] NULL,
	[MOVRN_REFNO] [int] NULL,
	[MOVE_DTTM] [smalldatetime] NULL,
	[SPSSN_REFNO] [int] NULL,
	[SPSSN_CODE] [varchar](20) NULL,
	[USER_CREATE] [varchar](30) NULL,
	[USER_MODIF] [varchar](30) NULL,
	[CREATE_DTTM] [datetime] NULL,
	[MODIF_DTTM] [datetime] NULL,
	[ARCHV_FLAG] [varchar](1) NULL,
	[PRIOR_POINTER] [int] NULL,
	[EXTERNAL_KEY] [varchar](20) NULL,
	[OLD_PROCA_REFNO] [int] NULL,
	[NEW_PROCA_REFNO] [int] NULL,
	[OLD_TRANS_REFNO] [int] NULL,
	[NEW_TRANS_REFNO] [int] NULL,
	[COMMENTS] [varchar](255) NULL,
	[MURQB_REFNO] [numeric](18, 0) NULL,
	[NEW_SPSSN_REFNO] [numeric](18, 0) NULL,
	[RTTST_REFNO] [numeric](18, 0) NULL,
	[ERO_DTTM] [datetime] NULL,
	[OWNER_HEORG_REFNO] [int] NULL,
	[Created] [datetime] NULL,
 CONSTRAINT [PK_dbo_ScheduleHistory_SCDHS_REFNO] PRIMARY KEY CLUSTERED 
(
	[SCDHS_REFNO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Created_ScheduleHistory] ON [dbo].[ScheduleHistory] 
(
	[Created] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ScheduleHistory] ON [dbo].[ScheduleHistory] 
(
	[SCHDL_REFNO] ASC,
	[MOVRN_REFNO] ASC,
	[ARCHV_FLAG] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]