﻿CREATE TABLE [dbo].[SchedulePersonalCarer](
	[SCHPC_REFNO] [int] NOT NULL,
	[SCHDL_REFNO] [numeric](18, 0) NULL,
	[PERCA_REFNO] [int] NULL,
	[DURATION] [numeric](18, 0) NULL,
	[TUNIT_REFNO] [int] NULL,
	[CREATE_DTTM] [datetime] NULL,
	[MODIF_DTTM] [datetime] NULL,
	[USER_CREATE] [varchar](30) NULL,
	[USER_MODIF] [varchar](30) NULL,
	[ARCHV_FLAG] [varchar](1) NULL,
	[STRAN_REFNO] [numeric](18, 0) NULL,
	[EXTERNAL_KEY] [varchar](20) NULL,
	[OWNER_HEORG_REFNO] [int] NULL,
	[Created] [datetime] NULL,
 CONSTRAINT [PK_dbo_SchedulePersonalCarer_SCHPC_REFNO] PRIMARY KEY CLUSTERED 
(
	[SCHPC_REFNO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Created_SchedulePersonalCarer] ON [dbo].[SchedulePersonalCarer] 
(
	[Created] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]