﻿CREATE TABLE [dbo].[ServicePointSlot](
	[SPSLT_REFNO] [int] NOT NULL,
	[STRAN_REFNO] [int] NULL,
	[SPSSN_REFNO] [int] NULL,
	[START_DTTM] [datetime] NULL,
	[END_DTTM] [datetime] NULL,
	[PREV_START_DTTM] [datetime] NULL,
	[PREV_END_DTTM] [datetime] NULL,
	[NOOF_BOOKINGS] [int] NULL,
	[TSTAT_REFNO] [int] NULL,
	[PREV_TSTAT_REFNO] [int] NULL,
	[INSTRUCTIONS] [varchar](255) NULL,
	[CREATE_DTTM] [datetime] NULL,
	[MODIF_DTTM] [datetime] NULL,
	[USER_CREATE] [varchar](50) NULL,
	[USER_MODIF] [varchar](50) NULL,
	[DURATION] [int] NULL,
	[USRN] [varchar](255) NULL,
	[ARCHV_FLAG] [char](1) NULL,
	[PRIOR_POINTER] [int] NULL,
	[TEMPLATE_FLAG] [char](1) NULL,
	[EBS_PUB_FLAG] [char](1) NULL,
	[SPONT_REFNO] [int] NULL,
	[EXTERNAL_KEY] [varchar](50) NULL,
	[TMPLT_REFNO] [int] NULL,
	[CANCEL_DTTM] [datetime] NULL,
	[COMMENTS] [varchar](255) NULL,
	[SLCHR_REFNO] [int] NULL,
	[MAX_BOOKINGS] [int] NULL,
	[SDS_USER_ID] [varchar](50) NULL,
	[STCRB_REFNO] [int] NULL,
	[DIARY_CHANGED] [varchar](255) NULL,
	[OWNER_HEORG_REFNO] [int] NULL,
	[Created] [datetime] NULL,
 CONSTRAINT [PK_dbo_ServicePointSlotNew_SPSLT_REFNO] PRIMARY KEY CLUSTERED 
(
	[SPSLT_REFNO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Created_ServicePointSlotNew] ON [dbo].[ServicePointSlot] 
(
	[Created] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]