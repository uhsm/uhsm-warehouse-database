﻿CREATE TABLE [dbo].[UserScreenObject](
	[USSOB_REFNO] [int] NOT NULL,
	[USSFR_REFNO] [int] NULL,
	[LCDSI_REFNO] [int] NULL,
	[NAME] [varchar](50) NULL,
	[POS_LEFT] [numeric](18, 0) NULL,
	[POS_TOP] [numeric](18, 0) NULL,
	[CTRL_WIDTH] [numeric](18, 0) NULL,
	[CTRL_HEIGHT] [numeric](18, 0) NULL,
	[CTRL_ENAB] [varchar](255) NULL,
	[CTRL_MAND] [varchar](255) NULL,
	[ACCEP_VALS] [varchar](255) NULL,
	[PROMPT_LEFT] [numeric](18, 0) NULL,
	[PROMPT_TOP] [numeric](18, 0) NULL,
	[CREATE_DTTM] [datetime] NULL,
	[MODIF_DTTM] [datetime] NULL,
	[USER_CREATE] [varchar](30) NULL,
	[USER_MODIF] [varchar](30) NULL,
	[ARCHV_FLAG] [char](1) NULL,
	[STRAN_REFNO] [numeric](18, 0) NULL,
	[EXTERNAL_KEY] [varchar](20) NULL,
	[ASSES_SCORE_FLAG] [char](1) NULL,
	[ASSES_TRIG_FLAG] [char](1) NULL,
	[ASSES_TRIG_OPERATOR] [char](1) NULL,
	[ASSES_TRIG_VALUE] [numeric](18, 0) NULL,
	[ASSES_REFNO] [int] NULL,
	[OWNER_HEORG_REFNO] [int] NULL,
	[Created] [datetime] NULL,
 CONSTRAINT [PK_dbo_UserScreenObject_USSOB_REFNO] PRIMARY KEY CLUSTERED 
(
	[USSOB_REFNO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Created_UserScreenObject] ON [dbo].[UserScreenObject] 
(
	[Created] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]