﻿CREATE TABLE [dbo].[WardProfile](
	[WDPRO_REFNO] [int] NOT NULL,
	[SPONT_REFNO] [int] NULL,
	[AGEGR_REFNO] [int] NULL,
	[SEXXX_REFNO] [int] NULL,
	[BRPAG_REFNO] [int] NULL,
	[INCLI_REFNO] [int] NULL,
	[LODGERS_FLAG] [char](1) NULL,
	[START_DTTM] [smalldatetime] NULL,
	[END_DTTM] [smalldatetime] NULL,
	[HEORG_REFNO] [int] NULL,
	[USER_CREATE] [varchar](30) NULL,
	[USER_MODIF] [varchar](30) NULL,
	[CREATE_DTTM] [datetime] NULL,
	[MODIF_DTTM] [datetime] NULL,
	[STRAN_REFNO] [numeric](18, 0) NULL,
	[ARCHV_FLAG] [char](1) NULL,
	[EXTERNAL_KEY] [varchar](20) NULL,
	[OWNER_HEORG_REFNO] [int] NULL,
	[Created] [datetime] NULL,
 CONSTRAINT [PK_dbo_WardProfile_WDPRO_REFNO] PRIMARY KEY CLUSTERED 
(
	[WDPRO_REFNO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Created_WardProfile] ON [dbo].[WardProfile] 
(
	[Created] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]