﻿CREATE TABLE [dbo].[refsWith90](
	[ref_ref] [numeric](18, 0) NULL,
	[RTTST_DATE] [datetime] NULL,
	[RTTST_REFNO] [numeric](18, 0) NULL
) ON [PRIMARY]