﻿CREATE TABLE [dbo].[ziMHASections](
	[FILE_NAME] [varchar](50) NULL,
	[DATA_START_DATE] [varchar](50) NULL,
	[DATA_END_DATE] [varchar](50) NULL,
	[TOTAL_ROW_COUNT] [varchar](50) NULL,
	[ROW_IDENTIFIER] [varchar](50) NULL,
	[MHSEC_REFNO] [varchar](50) NULL,
	[CODE] [varchar](50) NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[INITIAL_TUNITS] [varchar](50) NULL,
	[INITIAL_TUNIT_REFNO] [varchar](50) NULL,
	[MAX_TUNITS] [varchar](50) NULL,
	[MAX_TUNIT_REFNO] [varchar](50) NULL,
	[RENEWAL_TUNITS] [varchar](50) NULL,
	[RENEWAL_TUNIT_REFNO] [varchar](50) NULL,
	[SUBRENEWAL_TUNITS] [varchar](50) NULL,
	[SUBRENEWAL_TUNIT_REFNO] [varchar](50) NULL,
	[START_DTTM] [varchar](50) NULL,
	[END_DTTM] [varchar](50) NULL,
	[PRLID_REFNO] [varchar](50) NULL,
	[CREATE_DTTM] [varchar](50) NULL,
	[MODIF_DTTM] [varchar](50) NULL,
	[USER_CREATE] [varchar](50) NULL,
	[USER_MODIF] [varchar](50) NULL,
	[ARCHV_FLAG] [varchar](50) NULL,
	[STRAN_REFNO] [varchar](50) NULL,
	[PRIOR_POINTER] [varchar](50) NULL,
	[EXTERNAL_KEY] [varchar](50) NULL,
	[LEGSC_REFNO] [varchar](50) NULL,
	[SECRENEWAL_TUNITS] [varchar](50) NULL,
	[SECRENEWAL_TUNIT_REFNO] [varchar](50) NULL,
	[ODPCD_REFNO] [varchar](50) NULL,
	[MIDNIGHT_FLAG] [varchar](50) NULL,
	[RENEW_FLAG] [varchar](50) NULL,
	[CONVERT_FLAG] [varchar](50) NULL,
	[DISCHARGE_FLAG] [varchar](50) NULL,
	[SECTP_REFNO] [varchar](50) NULL,
	[SORT_ORDER] [varchar](50) NULL,
	[OWNER_HEORG_REFNO] [varchar](50) NULL
) ON [PRIMARY]