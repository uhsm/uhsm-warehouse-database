﻿CREATE TABLE [dbo].[ziPatientCarePlanStrategies](
	[FILE_NAME] [varchar](50) NULL,
	[DATA_START_DATE] [varchar](50) NULL,
	[DATA_END_DATE] [varchar](50) NULL,
	[TOTAL_ROW_COUNT] [varchar](50) NULL,
	[ROW_IDENTIFIER] [varchar](50) NULL,
	[PCPST_REFNO] [varchar](50) NOT NULL,
	[PATCP_REFNO] [varchar](50) NOT NULL,
	[CPSTR_REFNO] [varchar](50) NOT NULL,
	[SELECTED_FLAG] [varchar](50) NOT NULL,
	[CREATE_DTTM] [varchar](50) NOT NULL,
	[MODIF_DTTM] [varchar](50) NOT NULL,
	[USER_CREATE] [varchar](50) NOT NULL,
	[USER_MODIF] [varchar](50) NOT NULL,
	[ARCHV_FLAG] [varchar](50) NOT NULL,
	[STRAN_REFNO] [varchar](50) NULL,
	[EXTERNAL_KEY] [varchar](50) NULL,
	[OWNER_HEORG_REFNO] [varchar](50) NULL
) ON [PRIMARY]