﻿CREATE TABLE [dbo].[ziWaitingListNew](
	[FILE_NAME] [varchar](50) NULL,
	[DATA_START_DATE] [varchar](50) NULL,
	[DATA_END_DATE] [varchar](50) NULL,
	[TOTAL_ROW_COUNT] [varchar](50) NULL,
	[ROW_IDENTIFIER] [varchar](50) NULL,
	[WLIST_REFNO] [varchar](50) NOT NULL,
	[WLRUL_REFNO] [varchar](50) NULL,
	[ADMDC_REFNO] [varchar](50) NULL,
	[REFRL_REFNO] [varchar](50) NULL,
	[SPECT_REFNO] [varchar](50) NULL,
	[PROCA_REFNO] [varchar](50) NULL,
	[HEORG_REFNO] [varchar](50) NULL,
	[PATNT_REFNO] [varchar](50) NOT NULL,
	[ADMET_REFNO] [varchar](50) NOT NULL,
	[ADCAT_REFNO] [varchar](50) NOT NULL,
	[INMGT_REFNO] [varchar](50) NOT NULL,
	[PRITY_REFNO] [varchar](50) NOT NULL,
	[URGNC_REFNO] [varchar](50) NOT NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[SHORT_NOTICE_FLAG] [varchar](50) NOT NULL,
	[ORDTA_DTTM] [varchar](50) NULL,
	[WLIST_DTTM] [varchar](50) NULL,
	[REMVL_DTTM] [varchar](50) NULL,
	[REMVL_REFNO] [varchar](50) NOT NULL,
	[PRIOR_REFNO] [varchar](50) NULL,
	[GUADM_DTTM] [varchar](50) NULL,
	[TRANS_REFNO] [varchar](50) NOT NULL,
	[HOTEL_STAY_FLAG] [varchar](50) NOT NULL,
	[ADMIT_DTTM] [varchar](50) NULL,
	[SVTYP_REFNO] [varchar](50) NOT NULL,
	[SPONT_REFNO] [varchar](50) NULL,
	[DAYOW_REFNO] [varchar](50) NOT NULL,
	[THEAT_TIME] [varchar](50) NULL,
	[WHOCO_REFNO] [varchar](50) NOT NULL,
	[CLPTY_REFNO] [varchar](50) NOT NULL,
	[NO_FUNDS_FLAG] [varchar](50) NULL,
	[CONTR_REFNO] [varchar](50) NULL,
	[PURCH_REFNO] [varchar](50) NULL,
	[PROVD_REFNO] [varchar](50) NULL,
	[LOCAL_WLRUL_REFNO] [varchar](50) NULL,
	[CSTAT_REFNO] [varchar](50) NOT NULL,
	[XFER_SPONT_REFNO] [varchar](50) NULL,
	[LENGTH_STAY] [varchar](50) NULL,
	[XFER_LENGTH_STAY] [varchar](50) NULL,
	[ANTYP_REFNO] [varchar](50) NOT NULL,
	[WAITING_START_DTTM] [varchar](50) NULL,
	[WAITING_SUSP_START_DTTM] [varchar](50) NULL,
	[WAITING_SUSP_TOTAL] [varchar](50) NULL,
	[ADMIT_READY_FLAG] [varchar](50) NULL,
	[ACTIVE_INDICATOR] [varchar](50) NULL,
	[THVISIT_YNUNK_REFNO] [varchar](50) NOT NULL,
	[DEPOSIT] [varchar](50) NULL,
	[WAITING_LIST_NUMBER] [varchar](50) NULL,
	[CREATE_DTTM] [varchar](50) NOT NULL,
	[MODIF_DTTM] [varchar](50) NOT NULL,
	[USER_CREATE] [varchar](50) NOT NULL,
	[USER_MODIF] [varchar](50) NOT NULL,
	[ARCHV_FLAG] [varchar](50) NULL,
	[STRAN_REFNO] [varchar](50) NULL,
	[PRIOR_POINTER] [varchar](50) NULL,
	[EXTERNAL_KEY] [varchar](50) NULL,
	[PLANNED_PROC] [varchar](255) NULL,
	[SPSSN_REFNO] [varchar](50) NULL,
	[DYCEP_REFNO] [varchar](50) NULL,
	[ACLEV_REFNO] [varchar](50) NULL,
	[STEAM_REFNO] [varchar](50) NULL,
	[CONTP_REFNO] [varchar](50) NULL,
	[REMVL_HEORG_REFNO] [varchar](50) NULL,
	[PBK_NUMBER] [varchar](50) NULL,
	[VISIT_REFNO] [varchar](50) NULL,
	[BKTYP_REFNO] [varchar](50) NULL,
	[STAGE_REFNO] [varchar](50) NULL,
	[ACKNW_DTTM] [varchar](50) NULL,
	[ACKNW_LETST_REFNO] [varchar](50) NULL,
	[INVIT_DTTM] [varchar](50) NULL,
	[INVIT_LETST_REFNO] [varchar](50) NULL,
	[REMND_DTTM] [varchar](50) NULL,
	[REMND_LETST_REFNO] [varchar](50) NULL,
	[CANCL_DTTM] [varchar](50) NULL,
	[CANCL_LETST_REFNO] [varchar](50) NULL,
	[DAYS_SUSPENDED] [varchar](50) NULL,
	[PATNT_CHOICE_FLG] [varchar](50) NOT NULL,
	[NO_OF_OCCURENCES] [varchar](50) NULL,
	[TREAT_INIT_FLAG] [varchar](50) NOT NULL,
	[DELETED_FLAG] [varchar](50) NULL,
	[REMOVEDBY_USER] [varchar](50) NULL,
	[REMOVEDBY_DTTM] [varchar](50) NULL,
	[REMOVEDBY_REFNO] [varchar](50) NULL,
	[RTTST_REFNO] [varchar](50) NULL,
	[DSRTP_REFNO] [varchar](50) NULL,
	[DIAG_WAIT_FLAG] [varchar](50) NOT NULL,
	[WL_STEAM_REFNO] [varchar](50) NULL,
	[FIRST_DEF_TRMNT] [varchar](50) NULL,
	[OWNER_HEORG_REFNO] [varchar](50) NULL
) ON [PRIMARY]