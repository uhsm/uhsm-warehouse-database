﻿CREATE VIEW [dbo].[ExtractACPCCP] AS 

/***********************************************************************************************************************

Purpose:			Create Base Critical Care Data For Reporting

CreateDate:			13/09/2010
Author:				Kevin Daley

Current Version:	1.0
Version Date:		13/09/2010

Change Log: 
					13/09/2010 Kevin Daley
						Initial Version
					<ENTER DATE> <ENTER AUTHOR>
						<ENTER CHANGE>


***********************************************************************************************************************/
SELECT DISTINCT --DISTINCT Required due to some duplicate values for organs suported from ReferenceValueDependancy

	 SourceCCPStayNo = APCStay.APCST_REFNO
	,SourceCCPNo = AugmentedCarePeriod.AUGCP_REFNO
	,CCPSequenceNo = AugmentedCarePeriod.ACP_NUMBER
	,SourceEncounterNo = AugmentedCarePeriod.PRCAE_REFNO
	,SourceWardStayNo = APCStay.SSTAY_REFNO
	,SourceSpellNo = AugmentedCarePeriod.PRVSP_REFNO
	,CCPIndicator = AugmentedCarePeriod.CCP_FLAG
	,CCPStartDate = AugmentedCarePeriod.START_DTTM
	,CCPEndDate = AugmentedCarePeriod.END_DTTM
	,CCPReadyDate = AugmentedCarePeriod.READY_DTTM
	,CCPNoOfOrgansSupported = AugmentedCarePeriod.ORGANS_SUPPORTED
	,CCPICUDays = ISNULL(AugmentedCarePeriod.ICU_DAYS,0)
	,CCPHDUDays = ISNULL(AugmentedCarePeriod.HDU_DAYS,0)
	,CCPAdmissionSource = AugmentedCarePeriod.CCASO_REFNO
	,CCPAdmissionType = AugmentedCarePeriod.CCATY_REFNO
	,CCPAdmissionSourceLocation = AugmentedCarePeriod.CCSLO_REFNO
	,CCPUnitFunction = AugmentedCarePeriod.CCUFU_REFNO
	,CCPUnitBedFunction = AugmentedCarePeriod.CCUBC_REFNO
	,CCPDischargeStatus = AugmentedCarePeriod.CCDST_REFNO
	,CCPDischargeDestination = AugmentedCarePeriod.CCDDE_REFNO
	,CCPDischargeLocation = AugmentedCarePeriod.CCDLO_REFNO
	,CCPStayOrganSupported = RFDP.RFVAL_REFNO
	,CCPStayCareLevel = APCStay.CARE_LEVEL
	,CCPStayNoOfOrgansSupported = APCStay.ORGANS_SUPPORTED
	,CCPStayWard = ServicePointStay.SPONT_REFNO
	,CCPStayDate = APCStay.START_DTTM
	,CCPStay = 1
	--ko added 06/03/2014
	,CCPStayWithOrganSupported = 
		case
			when RFDP.RFVAL_REFNO is null
			then 0
			else 1
		end
	

FROM 
	AugmentedCarePeriod 
	LEFT JOIN APCStay 
		ON AugmentedCarePeriod.AUGCP_REFNO = APCStay.APCDT_REFNO AND APCStay.ARCHV_FLAG = 'N'
	LEFT JOIN ReferenceValueDependancy RFDP 
		ON APCStay.APCST_REFNO = RFDP.SORCE_REFNO 
		--ko removed 06/03/2014
		--AND APCStay.STRAN_REFNO = RFDP.STRAN_REFNO
		AND RFDP.ARCHV_FLAG = 'N' 
		--AND RFDP.SORCE_CODE = 'APCST'
	LEFT JOIN ServicePointStay 
		ON APCStay.SSTAY_REFNO = ServicePointStay.SSTAY_REFNO
		AND ServicePointStay.ARCHV_FLAG = 'N'
	
WHERE 
	AugmentedCarePeriod.ARCHV_FLAG = 'N'
	--and RFDP.RFVAL_REFNO is not null