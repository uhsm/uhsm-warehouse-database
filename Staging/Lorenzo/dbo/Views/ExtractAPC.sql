﻿CREATE view [dbo].[ExtractAPC] as

select
	 SourceUniqueID = Episode.PRCAE_REFNO
	,SourcePatientNo = Episode.PATNT_REFNO
	,SourceSpellNo = Spell.PRVSP_REFNO
	,ProviderSpellNo = Spell.PRVSP_REFNO
	,SourceEncounterNo = Episode.IDENTIFIER
	,ReferralSourceUniqueID = Episode.REFRL_REFNO
	,WaitingListSourceUniqueID = Episode.WLIST_REFNO
	,PatientTitle = Patient.TITLE_REFNO_DESCRIPTION
	,PatientForename = Patient.FORENAME
	,PatientSurname = Patient.SURNAME
	,DateOfBirth = Patient.DTTM_OF_BIRTH
	,DateOfDeath = Patient.DTTM_OF_DEATH
	,SexCode = Patient.SEXXX_REFNO
	,NHSNumber = Coalesce(dbo.Patient.PATNT_REFNO_NHS_IDENTIFIER, dbo.Patient.NHS_IDENTIFIER)--CM 04/04/2016 It doesn't look like the 'PATNT_REFNO_NHS_IDENTIFIER' is always populated so have added the 'NHS_IDENTIFIER' in to capture the NHS Number for those where it is null
	,DistrictNo = DistrictNo.IDENTIFIER
	,Postcode = PatientAddress.PCODE
	,PatientAddress1 = PatientAddress.LINE1
	,PatientAddress2 = PatientAddress.LINE2
	,PatientAddress3 = PatientAddress.LINE3
	,PatientAddress4 = PatientAddress.LINE4
	,DHACode = PatientAddress.HDIST_CODE
	,EthnicOriginCode = Patient.ETHGR_REFNO
	,MaritalStatusCode = Patient.MARRY_REFNO
	,ReligionCode = Patient.RELIG_REFNO

	,DateOnWaitingList =
		convert(
			date
			,coalesce(
				(
				select
					max(AdmissionOffer.TCI_DTTM)
				from
					dbo.AdmissionOffer
				where
					AdmissionOffer.WLIST_REFNO = WaitingList.WLIST_REFNO
				and	AdmissionOffer.ARCHV_FLAG = 'N'
				and	AdmissionOffer.OFOCM_REFNO in
					(
					 985	--Did Not Attend (DNA)
					,2003601	--No Contact
					,2000657	--Admission Cancelled by or on behalf of Patient
					,2003600	--DNA (Did Not Attend)
					,2003599	--Cancelled due to Patient Death
					)
				)

				,WaitingList.WAITING_START_DTTM
			)
		)

	,AdmissionDate = dateadd(day, datediff(day, 0, Spell.ADMIT_DTTM), 0)
	,DischargeDate = --dateadd(day, datediff(day, 0, Spell.DISCH_DTTM), 0)
				--CM 19/05/2016 Amended Discharge Date to be defined as the below logic to handle the provisional end indicated discharges
					case when Spell.PRVSN_END_FLAG = 'N' 
						then dateadd(day, datediff(day, 0, Spell.DISCH_DTTM), 0)
						else null
						end

	,EpisodeStartDate =  dateadd(day, datediff(day, 0, Episode.START_DTTM), 0)
	,EpisodeEndDate =  dateadd(day, datediff(day, 0, Episode.END_DTTM), 0)
	,StartSiteCode = Episode.OWNER_HEORG_REFNO

	,StartWardTypeCode =
			(
		select
			AdmissionWardStay.SPONT_REFNO
		from
			dbo.ServicePointStay AdmissionWardStay
		where
			AdmissionWardStay.PRVSP_REFNO = Spell.PRVSP_REFNO
		and	AdmissionWardStay.ARCHV_FLAG = 'N'
		and	not exists
			(
			select
				1
			from
				dbo.ServicePointStay Previous
			where
				Previous.PRVSP_REFNO = AdmissionWardStay.PRVSP_REFNO
			and	Previous.ARCHV_FLAG = 'N'
			and	(
					Previous.START_DTTM < AdmissionWardStay.START_DTTM
				or	(
						Previous.START_DTTM = AdmissionWardStay.START_DTTM
					and	Previous.SSTAY_REFNO < AdmissionWardStay.SSTAY_REFNO
					)
				)
			)
		)
-------------

	,EndSiteCode = Episode.OWNER_HEORG_REFNO

	,EndWardTypeCode =
			(
		select
			AdmissionWardStay.SPONT_REFNO
		from
			dbo.ServicePointStay AdmissionWardStay
		where
			AdmissionWardStay.PRVSP_REFNO = Spell.PRVSP_REFNO
		and	AdmissionWardStay.ARCHV_FLAG = 'N'
		and	not exists
			(
			select
				1
			from
				dbo.ServicePointStay Previous
			where
				Previous.PRVSP_REFNO = AdmissionWardStay.PRVSP_REFNO
			and	Previous.ARCHV_FLAG = 'N'
			and	(
					Previous.START_DTTM > AdmissionWardStay.START_DTTM
				or	(
						Previous.START_DTTM = AdmissionWardStay.START_DTTM
					and	Previous.SSTAY_REFNO > AdmissionWardStay.SSTAY_REFNO
					)
				)
			)
		)

------------------

	,RegisteredGpCode = 
		coalesce(
			 RegisteredGp.PROCA_REFNO
			,DefaultRegisteredGp.PROCA_REFNO
		)

	,RegisteredGpPracticeCode =
		coalesce(
			 RegisteredGp.HEORG_REFNO
			,DefaultRegisteredGp.HEORG_REFNO
		)

	,EpisodicGpCode = 
		coalesce(
			 RegisteredGp.PROCA_REFNO
			,DefaultRegisteredGp.PROCA_REFNO
		)

	,EpisodicGpPracticeCode =
		coalesce(
			 RegisteredGp.HEORG_REFNO
			,DefaultRegisteredGp.HEORG_REFNO
		)

	,SiteCode = Episode.SPONT_REFNO

	,AdmissionMethodCode = Spell.ADMET_REFNO
	,AdmissionSourceCode = Spell.ADSOR_REFNO
	,PatientClassificationCode = Spell.PATCL_REFNO
	,ManagementIntentionCode = Spell.INMGT_REFNO
	,DischargeMethodCode = Spell.DISMT_REFNO
	,DischargeDestinationCode = Spell.DISDE_REFNO
	,AdminCategoryCode = Episode.ADCAT_REFNO
	,ConsultantCode = Episode.PROCA_REFNO
	,SpecialtyCode = Episode.SPECT_REFNO

	,LastEpisodeInSpellIndicator =
		convert(
			 bit
			,case
			when exists
				(
				select
					1
				from
					dbo.ProfessionalCareEpisode FutureEpisode
				where
					FutureEpisode.PRVSP_REFNO = Spell.PRVSP_REFNO
				and	FutureEpisode.START_DTTM > Episode.START_DTTM
				and	FutureEpisode.ARCHV_FLAG = 'N'
				)
			then 0
			else 1
			end
		)

	,FirstRegDayOrNightAdmit = Spell.FIRST_REGULAR

	,NeonatalLevelOfCare = Birth.NNLVL_REFNO
		
	,PASHRGCode = Episode.HRG_CODE
	,PrimaryDiagnosisCode = null
	,SubsidiaryDiagnosisCode = null
	,SecondaryDiagnosisCode1 = null
	,SecondaryDiagnosisCode2 = null
	,SecondaryDiagnosisCode3 = null
	,SecondaryDiagnosisCode4 = null
	,SecondaryDiagnosisCode5 = null
	,SecondaryDiagnosisCode6 = null
	,SecondaryDiagnosisCode7 = null
	,SecondaryDiagnosisCode8 = null
	,SecondaryDiagnosisCode9 = null
	,SecondaryDiagnosisCode10 = null
	,SecondaryDiagnosisCode11 = null
	,SecondaryDiagnosisCode12 = null
	,PrimaryOperationCode = null
	,PrimaryOperationDate = null
	,SecondaryOperationCode1 = null
	,SecondaryOperationDate1 = null
	,SecondaryOperationCode2 = null
	,SecondaryOperationDate2 = null
	,SecondaryOperationCode3 = null
	,SecondaryOperationDate3 = null
	,SecondaryOperationCode4 = null
	,SecondaryOperationDate4 = null
	,SecondaryOperationCode5 = null
	,SecondaryOperationDate5 = null
	,SecondaryOperationCode6 = null
	,SecondaryOperationDate6 = null
	,SecondaryOperationCode7 = null
	,SecondaryOperationDate7 = null
	,SecondaryOperationCode8 = null
	,SecondaryOperationDate8 = null
	,SecondaryOperationCode9 = null
	,SecondaryOperationDate9 = null
	,SecondaryOperationCode10 = null
	,SecondaryOperationDate10 = null
	,SecondaryOperationCode11 = null
	,SecondaryOperationDate11 = null
	,OperationStatusCode = null
	,ContractSerialNo = Contract.SERIAL_NUMBER
	,CodingCompleteFlag = --Episode.CODING_COMPLETE_FLAG
						Case Episode.CSTAT_REFNO 
							When 454 Then 'I'	-- Incomplete
							When 455 Then 'C'	-- Complete
							When 456 Then 'A'	-- Awaiting Results
							Else 'N'				-- Not Specified
						End
	,PurchaserCode = Episode.PURCH_REFNO
	,ProviderCode = Episode.PROVD_REFNO --this is null
	,EpisodeStartTime = Episode.START_DTTM
	,EpisodeEndTime = Episode.END_DTTM
	,RegisteredGdpCode = null
	,InterfaceCode = 'LOR'
	,CasenoteNumber = null
	,NHSNumberStatusCode = Patient.NNNTS_CODE
	,AdmissionTime = Spell.ADMIT_DTTM
	,DischargeTime = Spell.DISCH_DTTM
	,TransferFrom = null

	,ExpectedLOS =
		datediff(
			day
			,Spell.ADMIT_DTTM
			,Spell.EXPDS_DTTM
		)

	,MRSAFlag =
		convert(
			 bit
			,case
			when exists
				(
				select
					1
				from
					dbo.ClinicalCoding
				where
					ClinicalCoding.PATNT_REFNO = Episode.PATNT_REFNO
				and	ClinicalCoding.START_DTTM <= Episode.END_DTTM
				and	ClinicalCoding.CODE in ('009', 'INFC006')
				and	ClinicalCoding.DPTYP_CODE = 'ALERT'
				and	ClinicalCoding.ARCHV_FLAG = 'N'
				)
			then 1
			else 0
			end
		)

	,RTTPathwayID = Referral.PATNT_PATHWAY_ID
	,RTTPathwayCondition = null
	,RTTStartDate = Referral.RTT_START_DATE
	,RTTEndDate = null
	,RTTSpecialtyCode = null
	,RTTCurrentProviderCode = null

	,RTTCurrentStatusCode = RTT.RTTST_REFNO
	,RTTCurrentStatusDate = RTT.RTTST_DATE

	,RTTCurrentPrivatePatientFlag = null
	,RTTOverseasStatusFlag = null

	,RTTPeriodStatusCode = 
		coalesce(
			 EpisodePeriodStatus.RTTST_REFNO
			,SpellPeriodStatus.RTTST_REFNO
			,Spell.RTTST_REFNO
		)

	,OverseasStatusFlag = OverseasVisitorStatus.OVSVS_REFNO

	,PASCreated = Episode.CREATE_DTTM
	,PASUpdated = Episode.MODIF_DTTM
	,PASCreatedByWhom = Episode.USER_CREATE
	,PASUpdatedByWhom = Episode.USER_MODIF

	,ArchiveFlag =
		case
		when Spell.PRVSN_START_FLAG = 'Y'
		then 1
		when Spell.ARCHV_FLAG = 'Y'
		then 1
		when Spell.ARCHV_FLAG = 'C'
		then 1
		when Episode.ARCHV_FLAG = 'N'
		then 0
		else 1
		end

	,EpisodeOutcomeCode = Episode.CEOCM_REFNO
	,LegalStatusClassificationOnAdmissionCode = Spell.ADMIT_LEGSC_REFNO
	,LegalStatusClassificationOnDischargeCode = Spell.DISCH_LEGSC_REFNO
	,DecisionToAdmitDate = Spell.DTA_DTTM
	,OriginalDecisionToAdmitDate = AdmissionDecision.DTA_DTTM

	,DurationOfElectiveWait = 
		DATEDIFF(
			DAY
			,cast(AdmissionDecision.DTA_DTTM as date)
			,cast(Spell.ADMIT_DTTM as date)
		)
--7 Jun 2012 - PDO - Do not adjust for CDS
--		- WaitingList.WAITING_SUSP_TOTAL

from
	dbo.ProfessionalCareEpisode Episode

inner join dbo.ProviderSpell Spell
on	Spell.PRVSP_REFNO = Episode.PRVSP_REFNO

--this may be reinstated depending on how merges are handled in PAS - awaiting CSC feedback - 7 jun 2012
--left join dbo.MergePatient MergePatient
--on	MergePatient.PATNT_REFNO = Episode.PATNT_REFNO
--and	MergePatient.ARCHV_FLAG = 'N'
--and	not exists
--	(
--	select
--		1
--	from
--		dbo.MergePatient Previous
--	where
--		Previous.PATNT_REFNO = Episode.PATNT_REFNO
--	and	Previous.ARCHV_FLAG = 'N'
--	and	Previous.MGPAT_REFNO > MergePatient.MGPAT_REFNO
--	)

--left join dbo.Patient
--on	Patient.PATNT_REFNO =
--		coalesce(
--			 MergePatient.PREV_PATNT_REFNO
--			,Episode.PATNT_REFNO
--		)

left join dbo.Patient
on	Patient.PATNT_REFNO = Episode.PATNT_REFNO

left join dbo.Referral
on	Referral.REFRL_REFNO = Episode.REFRL_REFNO

left join dbo.WaitingList
on	WaitingList.WLIST_REFNO = Episode.WLIST_REFNO

left join dbo.AdmissionDecision
on	AdmissionDecision.ADMDC_REFNO = Spell.ADMDC_REFNO
and	AdmissionDecision.ARCHV_FLAG = 'N'

left join dbo.PatientIdentifier DistrictNo
on	DistrictNo.PATNT_REFNO = Patient.PATNT_REFNO
and	DistrictNo.PITYP_REFNO = 2001232 --facility
and	DistrictNo.IDENTIFIER like 'RM2%'
and	DistrictNo.ARCHV_FLAG = 'N'
and	not exists
	(
	select
		1
	from
		dbo.PatientIdentifier Previous
	where
		Previous.PATNT_REFNO = DistrictNo.PATNT_REFNO
	and	Previous.PITYP_REFNO = DistrictNo.PITYP_REFNO
	and	Previous.IDENTIFIER like 'RM2%'
	and	Previous.ARCHV_FLAG = 'N'
	and	(
			Previous.START_DTTM > DistrictNo.START_DTTM
		or
			(
				Previous.START_DTTM = DistrictNo.START_DTTM
			and	Previous.PATID_REFNO > DistrictNo.PATID_REFNO
			)
		)
	)

left join dbo.PatientAddressRole
on	PatientAddressRole.PATNT_REFNO = Patient.PATNT_REFNO
and	PatientAddressRole.ROTYP_CODE = 'HOME'
and	PatientAddressRole.ARCHV_FLAG = 'N'
and	exists
	(
	select
		1
	from
		dbo.PatientAddress
	where
		PatientAddress.ADDSS_REFNO = PatientAddressRole.ADDSS_REFNO
	and	PatientAddress.ADTYP_CODE = 'POSTL'
	and	PatientAddress.ARCHV_FLAG = 'N'
	)
and	coalesce(Episode.END_DTTM, getdate()) between 
		PatientAddressRole.START_DTTM 
	and coalesce(
			 PatientAddressRole.END_DTTM
			,Episode.END_DTTM
			,getdate()
		)
and	not exists
	(
	select
		1
	from
		dbo.PatientAddressRole Previous
	where
		Previous.PATNT_REFNO = PatientAddressRole.PATNT_REFNO
	and	Previous.ROTYP_CODE = PatientAddressRole.ROTYP_CODE
	and	Previous.ARCHV_FLAG = 'N'
	and	exists
		(
		select
			1
		from
			dbo.PatientAddress
		where
			PatientAddress.ADDSS_REFNO = Previous.ADDSS_REFNO
		and	PatientAddress.ADTYP_CODE = 'POSTL'
		and	PatientAddress.ARCHV_FLAG = 'N'
		)
	and	coalesce(Episode.END_DTTM, getdate()) between 
			Previous.START_DTTM 
		and coalesce(
				 Previous.END_DTTM
				,Episode.END_DTTM
				,getdate()
			)
	and	(
			Previous.START_DTTM > PatientAddressRole.START_DTTM
		or	(
				Previous.START_DTTM = PatientAddressRole.START_DTTM
			and	Previous.ROLES_REFNO > PatientAddressRole.ROLES_REFNO
			)
		)
	)

left join dbo.PatientAddress
on	PatientAddress.ADDSS_REFNO = PatientAddressRole.ADDSS_REFNO
and	PatientAddress.ARCHV_FLAG = 'N'


left join dbo.PatientProfessionalCarer DefaultRegisteredGp
on	DefaultRegisteredGp.PATNT_REFNO = Patient.PATNT_REFNO
and	DefaultRegisteredGp.PRTYP_MAIN_CODE = 'GMPRC'
and	DefaultRegisteredGp.ARCHV_FLAG = 'N'
and	Episode.START_DTTM < DefaultRegisteredGp.START_DTTM
and	not exists
	(
	select
		1
	from
		dbo.PatientProfessionalCarer Previous
	where
		Previous.PATNT_REFNO = DefaultRegisteredGp.PATNT_REFNO
	and	Previous.PRTYP_MAIN_CODE = DefaultRegisteredGp.PRTYP_MAIN_CODE
	and	Previous.ARCHV_FLAG = 'N'
	and	Episode.START_DTTM < Previous.START_DTTM
	and	(
			Previous.START_DTTM > DefaultRegisteredGp.START_DTTM
		or	(
				Previous.START_DTTM = DefaultRegisteredGp.START_DTTM
			and	Previous.PATPC_REFNO > DefaultRegisteredGp.PATPC_REFNO
			)
		)
	)

left join dbo.PatientProfessionalCarer RegisteredGp
on	RegisteredGp.PATNT_REFNO = Patient.PATNT_REFNO
and	RegisteredGp.PRTYP_MAIN_CODE = 'GMPRC'
and	RegisteredGp.ARCHV_FLAG = 'N'
and	Episode.START_DTTM between 
		RegisteredGp.START_DTTM 
	and coalesce(
			RegisteredGp.END_DTTM
			,Episode.START_DTTM
		)
and	not exists
	(
	select
		1
	from
		dbo.PatientProfessionalCarer Previous
	where
		Previous.PATNT_REFNO = RegisteredGp.PATNT_REFNO
	and	Previous.PRTYP_MAIN_CODE = RegisteredGp.PRTYP_MAIN_CODE
	and	Previous.ARCHV_FLAG = 'N'
	and	Episode.START_DTTM between 
			Previous.START_DTTM 
		and coalesce(
				Previous.END_DTTM
				,Episode.START_DTTM
			)
	and	(
			Previous.START_DTTM > RegisteredGp.START_DTTM
		or	(
				Previous.START_DTTM = RegisteredGp.START_DTTM
			and	Previous.PATPC_REFNO > RegisteredGp.PATPC_REFNO
			)
		)
	)

left join dbo.Contract
on	Contract.CONTR_REFNO = Episode.CONTR_REFNO
and	Contract.ARCHV_FLAG = 'N'

left join dbo.OverseasVisitorStatus OverseasVisitorStatus
on	OverseasVisitorStatus.PATNT_REFNO = Patient.PATNT_REFNO
and	OverseasVisitorStatus.ARCHV_FLAG = 'N'
and	Episode.START_DTTM between 
		OverseasVisitorStatus.START_DTTM 
	and coalesce(
			OverseasVisitorStatus.END_DTTM
			,Episode.START_DTTM
		)
and	not exists
	(
	select
		1
	from
		dbo.OverseasVisitorStatus Previous
	where
		Previous.PATNT_REFNO = OverseasVisitorStatus.PATNT_REFNO
	and	Previous.ARCHV_FLAG = 'N'
	and	Episode.START_DTTM between 
			Previous.START_DTTM 
		and coalesce(
				Previous.END_DTTM
				,Episode.START_DTTM
			)
	and	(
			Previous.START_DTTM > OverseasVisitorStatus.START_DTTM
		or	(
				Previous.START_DTTM = OverseasVisitorStatus.START_DTTM
			and	Previous.OVSEA_REFNO > OverseasVisitorStatus.OVSEA_REFNO
			)
		)
	)

--left join dbo.MaternitySpell
--on	MaternitySpell.PRCAE_REFNO = Episode.PRCAE_REFNO
--and	MaternitySpell.ARCHV_FLAG = 'N'
--
--left join dbo.Birth
--on	Birth.MATSP_REFNO = MaternitySpell.MATSP_REFNO
--and	Birth.ARCHV_FLAG = 'N'

left join dbo.Birth
on	Birth.PATNT_REFNO = Episode.PATNT_REFNO
and	dateadd(day, datediff(day, 0, Birth.DTTM_OF_BIRTH), 0) = dateadd(day, datediff(day, 0, Episode.START_DTTM), 0)
and	Birth.ARCHV_FLAG = 'N'
and	not exists
	(
	select
		1
	from
		dbo.Birth Previous
	where
		Previous.PATNT_REFNO = Episode.PATNT_REFNO
	and	dateadd(day, datediff(day, 0, Previous.DTTM_OF_BIRTH), 0) = dateadd(day, datediff(day, 0, Episode.START_DTTM), 0)
	and	Previous.ARCHV_FLAG = 'N'
	and	(
			Previous.CREATE_DTTM > Birth.CREATE_DTTM
		or	(
				Previous.CREATE_DTTM = Birth.CREATE_DTTM
			and	Previous.RGBIR_REFNO > Birth.RGBIR_REFNO
			)
		)
	)

left join dbo.RTT EpisodePeriodStatus
on	EpisodePeriodStatus.SORCE = 'PRCAE'
and	EpisodePeriodStatus.SORCE_REFNO = Episode.PRCAE_REFNO
and	EpisodePeriodStatus.ARCHV_FLAG = 'N'
and	not exists
	(
	select
		1
	from
		dbo.RTT Previous
	where
		Previous.SORCE = 'PRCAE'
	and	Previous.SORCE_REFNO = Episode.PRCAE_REFNO
	and	Previous.ARCHV_FLAG = 'N'
	and	(
			Previous.RTTST_DATE > EpisodePeriodStatus.RTTST_DATE
		or	(
				Previous.RTTST_DATE = EpisodePeriodStatus.RTTST_DATE
			and	Previous.RTTPR_REFNO > EpisodePeriodStatus.RTTPR_REFNO
			)
		)
	)					

left join dbo.RTT SpellPeriodStatus
on	SpellPeriodStatus.SORCE = 'PRVSP'
and	SpellPeriodStatus.SORCE_REFNO = Spell.PRVSP_REFNO
and	SpellPeriodStatus.ARCHV_FLAG = 'N'
and	not exists
	(
	select
		1
	from
		dbo.RTT Previous
	where
		Previous.SORCE = 'PRVSP'
	and	Previous.SORCE_REFNO = Spell.PRVSP_REFNO
	and	Previous.ARCHV_FLAG = 'N'
	and	(
			Previous.RTTST_DATE > SpellPeriodStatus.RTTST_DATE
		or	(
				Previous.RTTST_DATE = SpellPeriodStatus.RTTST_DATE
			and	Previous.RTTPR_REFNO > SpellPeriodStatus.RTTPR_REFNO
			)
		)
	)					

left join dbo.RTT
on	RTT.REFRL_REFNO = Episode.REFRL_REFNO
and	RTT.ARCHV_FLAG = 'N'
and	not exists
	(
	select
		1
	from
		dbo.RTT Previous
	where
		Previous.REFRL_REFNO = Episode.REFRL_REFNO
	and	Previous.ARCHV_FLAG = 'N'
	and	(
			Previous.RTTST_DATE > RTT.RTTST_DATE
		or	(
				Previous.RTTST_DATE = RTT.RTTST_DATE
			and	Previous.RTTPR_REFNO > RTT.RTTPR_REFNO
			)
		)
	)