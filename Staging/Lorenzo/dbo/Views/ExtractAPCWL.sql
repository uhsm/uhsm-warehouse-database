﻿CREATE view [dbo].[ExtractAPCWL] as

select
	 SourceUniqueID = WaitingList.WLIST_REFNO
	,SourcePatientNo = WaitingList.PATNT_REFNO
	,SourceEncounterNo = WaitingList.WLIST_REFNO
	,ReferralSourceUniqueID = WaitingList.REFRL_REFNO
	,PatientTitle = Patient.TITLE_REFNO_DESCRIPTION
	,PatientForename = Patient.FORENAME
	,PatientSurname = Patient.SURNAME
	,DateOfBirth = Patient.DTTM_OF_BIRTH
	,DateOfDeath = Patient.DTTM_OF_DEATH
	,SexCode = Patient.SEXXX_REFNO
	,NHSNumber = Patient.PATNT_REFNO_NHS_IDENTIFIER
	,DistrictNo = DistrictNo.IDENTIFIER
	,Postcode = PatientAddress.PCODE
	,PatientAddress1 = PatientAddress.LINE1
	,PatientAddress2 = PatientAddress.LINE2
	,PatientAddress3 = PatientAddress.LINE3
	,PatientAddress4 = PatientAddress.LINE4
	,DHACode = PatientAddress.HDIST_CODE
	,HomePhone = PatientHomeTelephone.LINE1
	,WorkPhone = PatientWorkTelephone.LINE1
	,EthnicOriginCode = Patient.ETHGR_REFNO
	,MaritalStatusCode = Patient.MARRY_REFNO
	,ReligionCode = Patient.RELIG_REFNO
	,ConsultantCode = WaitingList.PROCA_REFNO
	,SpecialtyCode = WaitingList.SPECT_REFNO
	,PASSpecialtyCode = WaitingList.SPECT_REFNO
	,ManagementIntentionCode = WaitingList.INMGT_REFNO
	,AdmissionMethodCode = WaitingList.ADMET_REFNO
	,PriorityCode = Referral.PRITY_REFNO
	,WaitingListCode = WaitingList.WAITING_LIST_NUMBER
	,CommentClinical = null
	,CommentNonClinical = null

	,IntendedPrimaryOperationCode = 
		left(
			coalesce(
				WaitingList.PLANNED_PROC
				,ClinicalCoding.CODE
			)
			, 4
		)
		
	--,Operation = WaitingList.PLANNED_PROC
	--KO added 01/03/2013 as per issues log
	,Operation = 
			coalesce(
				WaitingList.PLANNED_PROC
				,ClinicalCoding.CODE + ' (' + ClinicalCoding.[DESCRIPTION] + ')'
			)

	,SiteCode = WaitingList.SPONT_REFNO
	,WardCode = WaitingList.XFER_SPONT_REFNO

	,WLStatus =
		case
		when WaitingListSuspension.WSUSP_REFNO is null then 'Waiting'
		when WaitingListSuspension.END_DTTM > getdate() then 'Suspended'
		when WaitingListSuspension.END_DTTM is null then 'Suspended'
		else 'Waiting'
		end

	,PurchaserCode = WaitingList.PURCH_REFNO
	,ProviderCode = WaitingList.PROVD_REFNO
	,ContractSerialNo = Contract.SERIAL_NUMBER
	,AdminCategoryCode = WaitingList.ADCAT_REFNO
	,CancelledBy = null
	,BookingTypeCode = WaitingList.BKTYP_REFNO
	,CasenoteNumber = null
	,OriginalDateOnWaitingList = AdmissionDecision.DTA_DTTM

--DateOnWaitingList is reset to the most recent DNA'd TCI if applicable

	,DateOnWaitingList =
		convert(
			date
			,coalesce(
				(
				select
					max(AdmissionOffer.TCI_DTTM)
				from
					dbo.AdmissionOffer
				where
					AdmissionOffer.WLIST_REFNO = WaitingList.WLIST_REFNO
				and	AdmissionOffer.TCI_DTTM < Census.DateValue
				and	AdmissionOffer.ARCHV_FLAG = 'N'
				and	AdmissionOffer.OFOCM_REFNO in
					(
					 985	--Did Not Attend (DNA)
					,2003601	--No Contact
					,2000657	--Admission Cancelled by or on behalf of Patient
					,2003600	--DNA (Did Not Attend)
					,2003599	--Cancelled due to Patient Death
					)
				)

				,WaitingList.WAITING_START_DTTM
			)
		)

	,TCIDate = AdmissionOffer.TCI_DTTM

	,KornerWait = null --calculated later
	,CountOfDaysSuspended = WaitingList.WAITING_SUSP_TOTAL
	,SuspensionStartDate = WaitingListSuspension.START_DTTM
	,SuspensionEndDate = WaitingListSuspension.END_DTTM
	,SuspensionReasonCode = WaitingListSuspension.SUSRS_REFNO
	,SuspensionReason = WaitingListSuspension.SUSRS_REFNO_DESCRIPTION
	,InterfaceCode = 'LOR'

	,RegisteredGpCode = 
		coalesce(
			 RegisteredGp.PROCA_REFNO
			,DefaultRegisteredGp.PROCA_REFNO
		)

	,RegisteredGpPracticeCode =
		coalesce(
			 RegisteredGp.HEORG_REFNO
			,DefaultRegisteredGp.HEORG_REFNO
		)

	,EpisodicGpCode = 
		coalesce(
			 RegisteredGp.PROCA_REFNO
			,DefaultRegisteredGp.PROCA_REFNO
		)

	,EpisodicGpPracticeCode =
		coalesce(
			 RegisteredGp.HEORG_REFNO
			,DefaultRegisteredGp.HEORG_REFNO
		)

	,SourceTreatmentFunctionCode = WaitingList.SPECT_REFNO
	,TreatmentFunctionCode = WaitingList.SPECT_REFNO
	,NationalSpecialtyCode = null
	,PCTCode = null
	,BreachDate = null

	,ExpectedAdmissionDate = AdmissionOffer.TCI_DTTM

	,ExpectedDischargeDate = AdmissionOffer.EXPDS_DTTM

	,ReferralDate = Referral.RECVD_DTTM

	,FutureCancelDate =



--PDO 24 Feb 2012
--Use the admission offer outcome to determine a cancellation
		case
		when
			AdmissionOffer.OFOCM_REFNO in
				(
				 982		--Cancelled by hosp. before Day of Admission
				,2003605	--Cancelled
				,2000657	--Admission Cancelled by or on behalf of Patient
				,2003597	--Admission Cancelled by Hospital
				,4423		--Cancelled by hosp. on Day of Admission
				,2003599	--Cancelled due to Patient Death
				,2005636	--Admission Cancelled by Hospital before Day of Admission
				)
		and	AdmissionOffer.OFOCM_DTTM > Census.DateValue
		then AdmissionOffer.OFOCM_DTTM
		else null
		end

	,ProcedureTime = AdmissionOffer.OPERATION_DTTM

	,TheatreCode = null

	,AnaestheticTypeCode = WaitingList.ANTYP_REFNO

	,AdmissionReason = null
	,EpisodeNo = null
	,BreachDays = null

	,MRSAFlag =
		convert(
			 bit
			,case
			when exists
				(
				select
					1
				from
					dbo.ClinicalCoding
				where
					ClinicalCoding.PATNT_REFNO = WaitingList.PATNT_REFNO
				and	ClinicalCoding.START_DTTM <= getdate()
				and	ClinicalCoding.CODE in ('009', 'INFC006')
				and	ClinicalCoding.DPTYP_CODE = 'ALERT'
				and	ClinicalCoding.ARCHV_FLAG = 'N'
				)
			then 1
			else 0
			end
		)

	,RTTPathwayID = Referral.PATNT_PATHWAY_ID
	,RTTPathwayCondition = null
	,RTTStartDate = Referral.RTT_START_DATE
	,RTTEndDate = null
	,RTTSpecialtyCode = null
	,RTTCurrentProviderCode = null

	,RTTCurrentStatusCode = RTT.RTTST_REFNO
	,RTTCurrentStatusDate = RTT.RTTST_DATE

	,RTTCurrentPrivatePatientFlag = null
	,RTTOverseasStatusFlag = null
	,NationalBreachDate = null
	,NationalBreachDays = null
	,DerivedBreachDays = null
	,DerivedClockStartDate = null
	,DerivedBreachDate = null
	,RTTBreachDate = null
	,RTTDiagnosticBreachDate = null
	,NationalDiagnosticBreachDate = null
	,SocialSuspensionDays = null
	,BreachTypeCode = null

	,PASCreated = WaitingList.CREATE_DTTM
	,PASUpdated = WaitingList.MODIF_DTTM
	,PASCreatedByWhom = WaitingList.USER_CREATE
	,PASUpdatedByWhom = WaitingList.USER_MODIF

	,ArchiveFlag =
		case
		when WaitingList.ARCHV_FLAG = 'N'
		then 0
		else 1
		end

	,RemovalDate =
		WaitingList.REMVL_DTTM

	,CancelReasonCode = AdmissionOffer.CANCR_REFNO

	,GeneralComment = GeneralComment.NOTES_REFNO_NOTE
	,PatientPreparation = PatientPreparation.NOTES_REFNO_NOTE

	,AdmissionOfferOutcomeCode = AdmissionOffer.OFOCM_REFNO

	,EstimatedTheatreTime = WaitingList.THEAT_TIME
	,WhoCanOperateCode = WaitingList.WHOCO_REFNO
	,WaitingListRuleCode = WaitingList.WLRUL_REFNO
	,ShortNoticeCode = WaitingList.SHORT_NOTICE_FLAG

	,PreOpDate = Outpatient.START_DTTM
	,PreOpClinicCode = Outpatient.SPONT_REFNO
	,AdmissionOfferSourceUniqueID = AdmissionOffer.ADMOF_REFNO
	,LocalCategoryCode = 
		case when WaitingList.LOCAL_WLRUL_REFNO in (
			10001222	--CS; Cancer Suspected
			,10001223	--CD; Cancer Diagnosed
			,10003882	--2SC; 2nd/Subsequent Cancer 
		) then LocalWLRule.CODE
		end
from
	dbo.WaitingList

inner join dbo.Parameter Census
on	Census.Parameter = 'APCWLCENSUSDATE'

left join dbo.WaitingListRule LocalWLRule on  
LocalWLRule.WLRUL_REFNO = WaitingList.LOCAL_WLRUL_REFNO

left join dbo.Patient
on	Patient.PATNT_REFNO = WaitingList.PATNT_REFNO

left join dbo.Referral
on	Referral.REFRL_REFNO = WaitingList.REFRL_REFNO

left join dbo.PatientIdentifier DistrictNo
on	DistrictNo.PATNT_REFNO = WaitingList.PATNT_REFNO
and	DistrictNo.PITYP_REFNO = 2001232 --facility
and	DistrictNo.ARCHV_FLAG = 'N'
and	DistrictNo.IDENTIFIER like 'RM2%'
and	not exists
	(
	select
		1
	from
		dbo.PatientIdentifier Previous
	where
		Previous.PATNT_REFNO = DistrictNo.PATNT_REFNO
	and	Previous.PITYP_REFNO = DistrictNo.PITYP_REFNO
	and	Previous.ARCHV_FLAG = 'N'
	and	Previous.IDENTIFIER like 'RM2%'
	and	(
			Previous.START_DTTM > DistrictNo.START_DTTM
		or
			(
				Previous.START_DTTM = DistrictNo.START_DTTM
			and	Previous.PATID_REFNO > DistrictNo.PATID_REFNO
			)
		)
	)

left join dbo.PatientAddressRole
on	PatientAddressRole.PATNT_REFNO = Patient.PATNT_REFNO
and	PatientAddressRole.ROTYP_CODE = 'HOME'
and	PatientAddressRole.ARCHV_FLAG = 'N'
and	exists
	(
	select
		1
	from
		dbo.PatientAddress
	where
		PatientAddress.ADDSS_REFNO = PatientAddressRole.ADDSS_REFNO
	and	PatientAddress.ADTYP_CODE = 'POSTL'
	and	PatientAddress.ARCHV_FLAG = 'N'
	)
and	WaitingList.WLIST_DTTM between 
		PatientAddressRole.START_DTTM 
	and coalesce(
			PatientAddressRole.END_DTTM
			,WaitingList.WLIST_DTTM
		)
and	not exists
	(
	select
		1
	from
		dbo.PatientAddressRole Previous
	where
		Previous.PATNT_REFNO = PatientAddressRole.PATNT_REFNO
	and	Previous.ROTYP_CODE = PatientAddressRole.ROTYP_CODE
	and	Previous.ARCHV_FLAG = 'N'
	and	exists
		(
		select
			1
		from
			dbo.PatientAddress
		where
			PatientAddress.ADDSS_REFNO = Previous.ADDSS_REFNO
		and	PatientAddress.ADTYP_CODE = 'POSTL'
		and	PatientAddress.ARCHV_FLAG = 'N'
		)
	and	WaitingList.WLIST_DTTM between 
			Previous.START_DTTM 
		and coalesce(
				Previous.END_DTTM
				,WaitingList.WLIST_DTTM
			)
	and	(
			Previous.START_DTTM > PatientAddressRole.START_DTTM
		or	(
				Previous.START_DTTM = PatientAddressRole.START_DTTM
			and	Previous.ROLES_REFNO > PatientAddressRole.ROLES_REFNO
			)
		)
	)

left join dbo.PatientAddress
on	PatientAddress.ADDSS_REFNO = PatientAddressRole.ADDSS_REFNO
and	PatientAddress.ARCHV_FLAG = 'N'


left join dbo.PatientAddressRole PatientAddressRoleTelephone
on	PatientAddressRoleTelephone.PATNT_REFNO = Patient.PATNT_REFNO
and	PatientAddressRoleTelephone.ROTYP_CODE = 'HOME'
and	PatientAddressRoleTelephone.ARCHV_FLAG = 'N'
and	exists
	(
	select
		1
	from
		dbo.PatientAddress
	where
		PatientAddress.ADDSS_REFNO = PatientAddressRoleTelephone.ADDSS_REFNO
	and	PatientAddress.ADTYP_CODE = 'PHONE'
	and	PatientAddress.ARCHV_FLAG = 'N'
	)
and	WaitingList.WLIST_DTTM between 
		PatientAddressRoleTelephone.START_DTTM 
	and coalesce(
			PatientAddressRoleTelephone.END_DTTM
			,WaitingList.WLIST_DTTM
		)
and	not exists
	(
	select
		1
	from
		dbo.PatientAddressRole Previous
	where
		Previous.PATNT_REFNO = PatientAddressRoleTelephone.PATNT_REFNO
	and	Previous.ROTYP_CODE = PatientAddressRoleTelephone.ROTYP_CODE
	and	Previous.ARCHV_FLAG = 'N'
	and	exists
		(
		select
			1
		from
			dbo.PatientAddress
		where
			PatientAddress.ADDSS_REFNO = Previous.ADDSS_REFNO
		and	PatientAddress.ADTYP_CODE = 'PHONE'
		and	PatientAddress.ARCHV_FLAG = 'N'
		)
	and	WaitingList.WLIST_DTTM between 
			Previous.START_DTTM 
		and coalesce(
				Previous.END_DTTM
				,WaitingList.WLIST_DTTM
			)
	and	(
			Previous.START_DTTM > PatientAddressRoleTelephone.START_DTTM
		or	(
				Previous.START_DTTM = PatientAddressRoleTelephone.START_DTTM
			and	Previous.ROLES_REFNO > PatientAddressRoleTelephone.ROLES_REFNO
			)
		)
	)

left join dbo.PatientAddress PatientHomeTelephone
on	PatientHomeTelephone.ADDSS_REFNO = PatientAddressRoleTelephone.ADDSS_REFNO
and	PatientHomeTelephone.ARCHV_FLAG = 'N'

left join dbo.PatientAddressRole PatientAddressRoleWorkTelephone
on	PatientAddressRoleWorkTelephone.PATNT_REFNO = Patient.PATNT_REFNO
and	PatientAddressRoleWorkTelephone.ROTYP_CODE = 'WP' --Work Telephone Number
and	PatientAddressRoleWorkTelephone.ARCHV_FLAG = 'N'
and	exists
	(
	select
		1
	from
		dbo.PatientAddress
	where
		PatientAddress.ADDSS_REFNO = PatientAddressRoleWorkTelephone.ADDSS_REFNO
	and	PatientAddress.ADTYP_CODE = 'PHONE'
	and	PatientAddress.ARCHV_FLAG = 'N'
	)
and	WaitingList.WLIST_DTTM between 
		PatientAddressRoleWorkTelephone.START_DTTM 
	and coalesce(
			PatientAddressRoleWorkTelephone.END_DTTM
			,WaitingList.WLIST_DTTM
		)
and	not exists
	(
	select
		1
	from
		dbo.PatientAddressRole Previous
	where
		Previous.PATNT_REFNO = PatientAddressRoleWorkTelephone.PATNT_REFNO
	and	Previous.ROTYP_CODE = PatientAddressRoleWorkTelephone.ROTYP_CODE
	and	Previous.ARCHV_FLAG = 'N'
	and	exists
		(
		select
			1
		from
			dbo.PatientAddress
		where
			PatientAddress.ADDSS_REFNO = Previous.ADDSS_REFNO
		and	PatientAddress.ADTYP_CODE = 'PHONE'
		and	PatientAddress.ARCHV_FLAG = 'N'
		)
	and	WaitingList.WLIST_DTTM between 
			Previous.START_DTTM 
		and coalesce(
				Previous.END_DTTM
				,WaitingList.WLIST_DTTM
			)
	and	(
			Previous.START_DTTM > PatientAddressRoleWorkTelephone.START_DTTM
		or	(
				Previous.START_DTTM = PatientAddressRoleWorkTelephone.START_DTTM
			and	Previous.ROLES_REFNO > PatientAddressRoleWorkTelephone.ROLES_REFNO
			)
		)
	)

left join dbo.PatientAddress PatientWorkTelephone
on	PatientWorkTelephone.ADDSS_REFNO = PatientAddressRoleWorkTelephone.ADDSS_REFNO
and	PatientWorkTelephone.ARCHV_FLAG = 'N'

left join dbo.Contract
on	Contract.CONTR_REFNO = WaitingList.CONTR_REFNO
and	Contract.ARCHV_FLAG = 'N'

inner join dbo.AdmissionDecision
on	AdmissionDecision.ADMDC_REFNO = WaitingList.ADMDC_REFNO
and	AdmissionDecision.DTA_DTTM <= Census.DateValue
and	AdmissionDecision.ARCHV_FLAG = 'N'

left join dbo.AdmissionOffer
on	AdmissionOffer.WLIST_REFNO = WaitingList.WLIST_REFNO
and	AdmissionOffer.ARCHV_FLAG = 'N'
and	not exists
	(
	select
		1
	from
		dbo.AdmissionOffer Previous
	where
		Previous.WLIST_REFNO = WaitingList.WLIST_REFNO
	and	Previous.ARCHV_FLAG = 'N'
	and	(
			Previous.ADMOF_DTTM > AdmissionOffer.ADMOF_DTTM
		or	(
				Previous.ADMOF_DTTM = AdmissionOffer.ADMOF_DTTM
			and	Previous.ADMOF_REFNO > AdmissionOffer.ADMOF_REFNO
			)
		)
	)

left join dbo.WaitingListSuspension
on	WaitingListSuspension.WLIST_REFNO = WaitingList.WLIST_REFNO
and	WaitingListSuspension.ARCHV_FLAG = 'N'
and	not exists
	(
	select
		1
	from
		dbo.WaitingListSuspension Previous
	where
		Previous.WLIST_REFNO = WaitingList.WLIST_REFNO
	and	Previous.ARCHV_FLAG = 'N'

	and	(
			Previous.START_DTTM > WaitingListSuspension.START_DTTM
		or	(
				Previous.START_DTTM = WaitingListSuspension.START_DTTM
			and	Previous.WSUSP_REFNO > WaitingListSuspension.WSUSP_REFNO
			)
		)
	)

left join dbo.PatientProfessionalCarer DefaultRegisteredGp
on	DefaultRegisteredGp.PATNT_REFNO = Referral.PATNT_REFNO
and	DefaultRegisteredGp.PRTYP_MAIN_CODE = 'GMPRC'
and	DefaultRegisteredGp.ARCHV_FLAG = 'N'
and	WaitingList.WLIST_DTTM < DefaultRegisteredGp.START_DTTM
and	not exists
	(
	select
		1
	from
		dbo.PatientProfessionalCarer Previous
	where
		Previous.PATNT_REFNO = DefaultRegisteredGp.PATNT_REFNO
	and	Previous.PRTYP_MAIN_CODE = DefaultRegisteredGp.PRTYP_MAIN_CODE
	and	Previous.ARCHV_FLAG = DefaultRegisteredGp.ARCHV_FLAG
	and	WaitingList.WLIST_DTTM < Previous.START_DTTM
	and	(
			Previous.START_DTTM > DefaultRegisteredGp.START_DTTM
		or	(
				Previous.START_DTTM = DefaultRegisteredGp.START_DTTM
			and	Previous.PATPC_REFNO > DefaultRegisteredGp.PATPC_REFNO
			)
		)
	)

left join dbo.PatientProfessionalCarer RegisteredGp
on	RegisteredGp.PATNT_REFNO = WaitingList.PATNT_REFNO
and	RegisteredGp.PRTYP_MAIN_CODE = 'GMPRC'
and	RegisteredGp.ARCHV_FLAG = 'N'
and	WaitingList.WLIST_DTTM between 
		RegisteredGp.START_DTTM 
	and coalesce(
			RegisteredGp.END_DTTM
			,WaitingList.WLIST_DTTM
		)
and	not exists
	(
	select
		1
	from
		dbo.PatientProfessionalCarer Previous
	where
		Previous.PATNT_REFNO = RegisteredGp.PATNT_REFNO
	and	Previous.PRTYP_MAIN_CODE = RegisteredGp.PRTYP_MAIN_CODE
	and	Previous.ARCHV_FLAG = 'N'
	and	WaitingList.WLIST_DTTM between 
			Previous.START_DTTM 
		and coalesce(
				Previous.END_DTTM
				,WaitingList.WLIST_DTTM
			)
	and	(
			Previous.START_DTTM > RegisteredGp.START_DTTM
		or	(
				Previous.START_DTTM = RegisteredGp.START_DTTM
			and	Previous.PATPC_REFNO > RegisteredGp.PATPC_REFNO
			)
		)
	)

left join dbo.RTT
on	RTT.REFRL_REFNO = WaitingList.REFRL_REFNO
and	RTT.RTTST_DATE <= Census.DateValue
and	RTT.RTTST_REFNO is not null
and	RTT.ARCHV_FLAG = 'N'
and	not exists
	(
	select
		1
	from
		dbo.RTT Previous
	where
		Previous.REFRL_REFNO = WaitingList.REFRL_REFNO
	and	Previous.RTTST_DATE <= Census.DateValue
	and	Previous.RTTST_REFNO is not null
	and	Previous.ARCHV_FLAG = 'N'
	and	(
			Previous.RTTST_DATE > RTT.RTTST_DATE
		or	(
				Previous.RTTST_DATE = RTT.RTTST_DATE
			and	Previous.RTTPR_REFNO > RTT.RTTPR_REFNO
			)
		)
	)

left join dbo.ClinicalCoding
on	ClinicalCoding.SORCE_REFNO = WaitingList.WLIST_REFNO
and	ClinicalCoding.SORCE_CODE = 'WLIST'
and	ClinicalCoding.SORT_ORDER = 1
and	ClinicalCoding.DPTYP_CODE = 'PROCE'
and	ClinicalCoding.ARCHV_FLAG = 'N'


left join dbo.NoteRole GeneralComment
on	GeneralComment.SORCE_REFNO = WaitingList.WLIST_REFNO
and	GeneralComment.SORCE_CODE = 'WLCMT'
and	GeneralComment.ARCHV_FLAG = 'N'
and	not exists
	(
	select
		1
	from
		dbo.NoteRole Previous
	where
		Previous.SORCE_REFNO = WaitingList.WLIST_REFNO
	and	Previous.SORCE_CODE = 'WLCMT'
	and	Previous.ARCHV_FLAG = 'N'
	and	Previous.NOTRL_REFNO > GeneralComment.NOTRL_REFNO
	)

left join dbo.NoteRole PatientPreparation
on	PatientPreparation.SORCE_REFNO = WaitingList.WLIST_REFNO
and	PatientPreparation.SORCE_CODE = 'WLPRP'
and	PatientPreparation.ARCHV_FLAG = 'N'
and	not exists
	(
	select
		1
	from
		dbo.NoteRole Previous
	where
		Previous.SORCE_REFNO = WaitingList.WLIST_REFNO
	and	Previous.SORCE_CODE = 'WLPRP'
	and	Previous.ARCHV_FLAG = 'N'
	and	Previous.NOTRL_REFNO > PatientPreparation.NOTRL_REFNO
	)

left join dbo.ScheduleEvent Outpatient
on	Outpatient.ADMOF_REFNO = AdmissionOffer.ADMOF_REFNO
and	Outpatient.ARCHV_FLAG = 'N'
and	not exists
	(
	select
		1
	from
		dbo.ScheduleEvent Previous
	where
		Previous.ADMOF_REFNO = AdmissionOffer.ADMOF_REFNO
	and	Previous.ARCHV_FLAG = 'N'
	and	Previous.SCHDL_REFNO > Outpatient.SCHDL_REFNO
	)

where
	WaitingList.SVTYP_REFNO = 1579 --1579 = ip, 4223 = op
and	WaitingList.ARCHV_FLAG = 'N'
and	(
		(
			WaitingList.ADMIT_DTTM > Census.DateValue
		or	WaitingList.ADMIT_DTTM is null
		)
	and	(
			WaitingList.REMVL_DTTM > Census.DateValue
		or	WaitingList.REMVL_DTTM is null
		and	(
				WaitingList.REMVL_REFNO = 1166
			or	WaitingList.REMVL_REFNO is null
			)
		)
	)

--remove test patients
and	not exists
	(
	select
		1
	from
		dbo.ExcludedPatient
	where
		ExcludedPatient.SourcePatientNo = WaitingList.PATNT_REFNO
	)