﻿CREATE view [dbo].[ExtractAPCWLSuspension] as

select
	 SourceUniqueID = WaitingListSuspension.WSUSP_REFNO
	,WaitingListSourceUniqueID = WaitingListSuspension.WLIST_REFNO
	,SuspensionStartDate = WaitingListSuspension.START_DTTM
	,SuspensionEndDate = WaitingListSuspension.END_DTTM
	,SuspensionReasonCode = WaitingListSuspension.SUSRS_REFNO
	,PASCreated = WaitingListSuspension.CREATE_DTTM
	,PASUpdated = WaitingListSuspension.MODIF_DTTM
	,PASCreatedByWhom = WaitingListSuspension.USER_CREATE
	,PASUpdatedByWhom = WaitingListSuspension.USER_MODIF

	,ArchiveFlag =
		case
		when WaitingListSuspension.ARCHV_FLAG = 'N'
		then 0
		else 1
		end

	,OriginalDateOnWaitingList = AdmissionDecision.DTA_DTTM
	,RemovalDate = WaitingList.REMVL_DTTM

from
	dbo.WaitingListSuspension

inner join dbo.Parameter Census
on	Census.Parameter = 'APCWLCENSUSDATE'

inner join dbo.WaitingList
on	WaitingList.WLIST_REFNO = WaitingListSuspension.WLIST_REFNO
and	WaitingList.SVTYP_REFNO = 1579 --1579 = ip, 4223 = op

--remove test patients
and	not exists
	(
	select
		1
	from
		dbo.ExcludedPatient
	where
		ExcludedPatient.SourcePatientNo = WaitingList.PATNT_REFNO
	)

and	WaitingList.ARCHV_FLAG = 'N'

inner join dbo.AdmissionDecision
on	AdmissionDecision.ADMDC_REFNO = WaitingList.ADMDC_REFNO

where
	WaitingListSuspension.ARCHV_FLAG = 'N'
and	AdmissionDecision.DTA_DTTM <= Census.DateValue
and	(
		WaitingList.REMVL_DTTM > Census.DateValue
	or	WaitingList.REMVL_DTTM is null
	)