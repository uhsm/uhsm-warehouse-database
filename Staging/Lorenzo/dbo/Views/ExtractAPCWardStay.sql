﻿CREATE view [dbo].[ExtractAPCWardStay] as

select
	 SourceUniqueID = WardStay.SSTAY_REFNO
	,SourceSpellNo = WardStay.PRVSP_REFNO
	,SourcePatientNo = WardStay.PATNT_REFNO
	,StartTime = WardStay.START_DTTM
	,EndTime = WardStay.END_DTTM
	,WardCode = WardStay.SPONT_REFNO

	,ProvisionalFlag =
		case
		when WardStay.PRVSN_FLAG = 'N'
		then 0
		else 1
		end

	,PASCreated = WardStay.CREATE_DTTM
	,PASUpdated = WardStay.MODIF_DTTM
	,PASCreatedByWhom = WardStay.USER_CREATE
	,PASUpdatedByWhom = WardStay.USER_MODIF

	,ArchiveFlag =
		case
		when WardStay.ARCHV_FLAG = 'Y'
		then 1
		when WardStay.ARCHV_FLAG = 'C'
		then 1
		else 0
		end

	,WardStay.Created
from
	dbo.ServicePointStay WardStay