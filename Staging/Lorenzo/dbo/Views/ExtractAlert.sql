﻿create view [dbo].[ExtractAlert] as
/**
-- =============================================
-- Author:		K Oakden
-- Create date: 20/01/2015
-- Description:	Get IPM patient alert data
-- =============================================
**/
select 
	AlertDescription = d.DESCRIPTION,
	AlertCode = coding.CODE,
	--coding.DGPRO_REFNO,
	SourcePatientNo = coding.PATNT_REFNO,
	DistrictNo = DistrictNo.IDENTIFIER,
	AlertComments = coding.COMMENTS,
	coding.CREATE_DTTM,
	coding.MODIF_DTTM,
	coding.START_DTTM,
	coding.END_DTTM,
	coding.Created
	--coding.*  --distinct SORCE_CODE
from dbo.ClinicalCoding coding
left join dbo.Diagnosis as d
	--on coding.CODE = d.CODE
	on coding.ODPCD_REFNO = d.ODPCD_REFNO
left join dbo.Patient
	on	Patient.PATNT_REFNO = coding.PATNT_REFNO
left join dbo.PatientIdentifier DistrictNo
	on	DistrictNo.PATNT_REFNO = Patient.PATNT_REFNO
	and	DistrictNo.PITYP_REFNO = 2001232 --facility
	and	DistrictNo.IDENTIFIER like 'RM2%'
	and	DistrictNo.ARCHV_FLAG = 'N'
	and	not exists
		(
		select
			1
		from
			dbo.PatientIdentifier Previous
		where
			Previous.PATNT_REFNO = DistrictNo.PATNT_REFNO
		and	Previous.PITYP_REFNO = DistrictNo.PITYP_REFNO
		and	Previous.IDENTIFIER like 'RM2%'
		and	Previous.ARCHV_FLAG = 'N'
		and	(
				Previous.START_DTTM > DistrictNo.START_DTTM
			or
				(
					Previous.START_DTTM = DistrictNo.START_DTTM
				and	Previous.PATID_REFNO > DistrictNo.PATID_REFNO
				)
			)
		)
where coding.DPTYP_CODE = 'alert'
and coding.ARCHV_FLAG = 'N'
and d.ARCHV_FLAG = 'N'
--order by DistrictNo.IDENTIFIER

--select * from dbo.Patient where PATNT_REFNO = 11438246