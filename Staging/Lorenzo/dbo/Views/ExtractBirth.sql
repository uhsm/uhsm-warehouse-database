﻿CREATE view [dbo].[ExtractBirth] as

select
	 SourceUniqueID = Birth.RGBIR_REFNO
	,MaternitySpellSourceUniqueID = Birth.MATSP_REFNO
	,SourcePatientNo = Birth.PATNT_REFNO
	,ReferralSourceUniqueID = Birth.REFRL_REFNO
	,ProfessionalCarerCode = Birth.PROCA_REFNO
	,MotherSourcePatientNo = [MTHER_REFNO]
	,BirthOrder = [BIRTH_ORDER]
	,DateOfBirth = [DTTM_OF_BIRTH]
	,SexCode = [SEXXX_REFNO]
	,StatusOfPersonConductingDeliveryCode = Birth.[DPSTS_REFNO]
	,LiveOrStillBirthCode = [BIRST_REFNO]
	,DeliveryPlaceCode = Birth.[DEPLA_REFNO]
	,ResuscitationMethodByPressureCode = [RESME_REFNO]
	,ResuscitationMethodByDrugCode = [RESMD_REFNO]
	,BirthWeight = [BIRTH_WEIGHT]
	,ApgarScoreAt1Minute = [APGAR_ONEMIN]
	,ApgarScoreAt5Minutes = [APGAR_FIVEMIN]
	,BCGAdministeredCode = [BCGAD_REFNO]
	,CircumferenceOfBabysHead = [CIRCM_HEAD]
	,LengthOfBaby = [BIRTH_LENGTH]
	,ExaminationOfHipsCode = [EXHIP_REFNO]
	,FollowUpCareCode = Birth.[FOLUP_REFNO]
	,FoetalPresentationCode = [FPRES_REFNO]
	,MetabolicScreeningCode = [METSC_REFNO]
	,JaundiceCode = [JAUND_REFNO]
	,FeedingTypeCode = [FDTYP_REFNO]
	,DeliveryMethodCode = [DELME_REFNO]
	,MaternityDrugsCode = [MTDRG_REFNO]
	,GestationLength = Birth.[GESTN_LENGTH]
	,ApgarScoreAt10Minutes = [APGAR_TENMIN]
	,NeonatalLevelOfCareCode = [NNLVL_REFNO]

	,PASCreated = Birth.CREATE_DTTM
	,PASUpdated = Birth.MODIF_DTTM
	,PASCreatedByWhom = Birth.USER_CREATE
	,PASUpdatedByWhom = Birth.USER_MODIF

	,ArchiveFlag =
		case
		when MaternitySpell.ARCHV_FLAG = 'Y'
		then 1
		when MaternitySpell.ARCHV_FLAG = 'C'
		then 1
		when ProviderSpell.ARCHV_FLAG = 'Y'
		then 1
		when ProviderSpell.ARCHV_FLAG = 'C'
		then 1
		when Birth.ARCHV_FLAG = 'Y'
		then 1
		when Birth.ARCHV_FLAG = 'C'
		then 1
		when Birth.ARCHV_FLAG = 'N'
		then 0
		else 1
		end

	,Birth.Created

from
	dbo.Birth

inner join dbo.MaternitySpell
on	MaternitySpell.MATSP_REFNO = Birth.MATSP_REFNO

left join dbo.ProviderSpell
on	ProviderSpell.PRVSP_REFNO = MaternitySpell.PRVSP_REFNO

where
--handle duplicate births with different mothers
	not exists
	(
	select
		1
	from
		dbo.Birth Previous
	where
		Previous.PATNT_REFNO = Birth.PATNT_REFNO
	and	(
			Previous.MODIF_DTTM > Birth.MODIF_DTTM
		or	(
				Previous.MODIF_DTTM = Birth.MODIF_DTTM
			and	Previous.RGBIR_REFNO > Birth.RGBIR_REFNO
			)
		)
	)