﻿CREATE VIEW [dbo].[ExtractCriticalCarePeriod] AS 

/***********************************************************************************************************************

Purpose:			Create Base Critical Care Period Data For Reporting

CreateDate:			01/12/2010
Author:				Kevin Daley

Current Version:	1.0
Version Date:		01/12/2010

Change Log: 
					01/12/2010 Kevin Daley
						Initial Version
					<ENTER DATE> <ENTER AUTHOR>
						<ENTER CHANGE>


***********************************************************************************************************************/
SELECT --DISTINCT --DISTINCT Required due to some duplicate values for organs suported from ReferenceValueDependancy

	 
	 SourceCCPNo = AugmentedCarePeriod.AUGCP_REFNO
	,CCPSequenceNo = AugmentedCarePeriod.ACP_NUMBER
	,SourceEncounterNo = AugmentedCarePeriod.PRCAE_REFNO
	,SourceSpellNo = AugmentedCarePeriod.PRVSP_REFNO
	,CCPIndicator = AugmentedCarePeriod.CCP_FLAG
	,CCPStartDate = AugmentedCarePeriod.START_DTTM
	,CCPEndDate = AugmentedCarePeriod.END_DTTM
	,CCPReadyDate = AugmentedCarePeriod.READY_DTTM
	,CCPNoOfOrgansSupported = AugmentedCarePeriod.ORGANS_SUPPORTED
	,CCPICUDays = ISNULL(AugmentedCarePeriod.ICU_DAYS,0)
	,CCPHDUDays = ISNULL(AugmentedCarePeriod.HDU_DAYS,0)
	,CCPAdmissionSource = AugmentedCarePeriod.CCASO_REFNO
	,CCPAdmissionType = AugmentedCarePeriod.CCATY_REFNO
	,CCPAdmissionSourceLocation = AugmentedCarePeriod.CCSLO_REFNO
	,CCPUnitFunction = AugmentedCarePeriod.CCUFU_REFNO
	,CCPUnitBedFunction = AugmentedCarePeriod.CCUBC_REFNO
	,CCPDischargeStatus = AugmentedCarePeriod.CCDST_REFNO
	,CCPDischargeDestination = AugmentedCarePeriod.CCDDE_REFNO
	,CCPDischargeLocation = AugmentedCarePeriod.CCDLO_REFNO
	,CCPDischargeStatusDescription=r.[DESCRIPTION]

	

FROM 
	AugmentedCarePeriod 
	left join dbo.ReferenceValue r
	on CCDST_REFNO=r.RFVAL_REFNO
WHERE 
	AugmentedCarePeriod.ARCHV_FLAG = 'N'