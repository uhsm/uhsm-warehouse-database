﻿/*
SELECT 
A.APCST_REFNO,
A.APCDT_REFNO,
A.SSTAY_REFNO,
cast(F.patnt_refno as varchar(20)) as PATNT_REFNO,
E.ACP_Number AS CCP_Number,
cast(E.PRVSP_REFNO as varchar(20)) AS SPELL_REFNO,
A.ORGANS_SUPPORTED AS OrgansSupported,
A.CARE_LEVEL as CareLevel,
A.CCP_FLAG CCPFlag,
CAST(A.START_DTTM AS DATETIME) AS CCPDate,
D.CODE AS WardCode, 
d.spont_refno_name WardName,
1 AS Counter,
IsNull(G.[Advanced Respiratory Support],0) AS [ADVANCED_RESPIRATORY_SUPPORT],
IsNull(G.[Basic Respiratory Support],0) AS [BASIC_RESPIRATORY_SUPPORT],
IsNull(G.[Circulatory Support],0) AS [CIRCULATORY_SUPPORT],
IsNull(G.[Neurological Support],0) AS [NEUROLOGICAL_SUPPORT],
IsNull(G.[Renal Support],0) AS [RENAL_SUPPORT],
IsNull(G.[Liver Support],0) AS [LIVER_SUPPORT],
IsNull(G.[Dermatological Support],0) AS [DERMATOLOGICAL_SUPPORT],
IsNull(G.[Gastrointestinal Support],0) AS [GASTROINTESTINAL_SUPPORT],
IsNull(G.[Advanced Cardiovascular Support],0) AS [ADVANCED_CARDIOVASCULAR_SUPPORT],
IsNull(G.[Basic Cardiovascular Support],0) AS [BASIC_CARDIOVASCULAR_SUPPORT],
IsNull(G.[Enhanced Nursing Care],0) AS [ENHANCED_NURSING_CARE],
IsNull(G.[Hepatic],0) AS [HEPATIC],
IsNull(G.[Not Specified Support],0) AS [NOT_SPECIFIED_SUPPORT]
INTO INFORMATION_REPORTING.DBO.CCP_STAY_DATASET
FROM APCST A 
LEFT OUTER JOIN SSTAY C ON A.SSTAY_REFNO = C.SSTAY_REFNO AND C.ARCHV_FLAG = 'N'
LEFT OUTER JOIN SPONT D ON C.SPONT_REFNO = D.SPONT_REFNO
left outer join augcp e on a.apcdt_refno = e.augcp_refno
left outer join provspell f on e.prvsp_refno = f.prvsp_refno and f.archv_flag = 'n'
Left Outer Join vwCCOrganSupport G on A.APCST_REFNO = G.APCST_REFNO
WHERE A.ARCHV_FLAG = 'N' --AND A.AUGCP_REFNO = 1000179
*/
CREATE VIEW [dbo].[ExtractCriticalCarePeriodStay]
AS
SELECT     dbo.APCStay.APCST_REFNO AS SourceCCPStayNo, dbo.APCStay.APCDT_REFNO AS SourceCCPNo, 
                      dbo.APCStay.SSTAY_REFNO AS SourceWardStayNo, Spell.PATNT_REFNO AS SourcePatientNo, CCP.ACP_NUMBER AS CCPSequenceNo, 
                      dbo.APCStay.START_DTTM AS StayDate, SStay.SPONT_REFNO AS CCPStaywWard, SStay.SPONT_REFNO_CODE, 
                      dbo.APCStay.ORGANS_SUPPORTED AS OrgansSupported, dbo.APCStay.CCP_FLAG AS CCPFlag, 1 AS CCPStay, dbo.APCStay.CARE_LEVEL
FROM         dbo.APCStay LEFT OUTER JOIN
                      dbo.AugmentedCarePeriod AS CCP ON dbo.APCStay.APCDT_REFNO = CCP.AUGCP_REFNO LEFT OUTER JOIN
                      dbo.ProviderSpell AS Spell ON CCP.PRVSP_REFNO = Spell.PRVSP_REFNO LEFT OUTER JOIN
                      dbo.ServicePointStay AS SStay ON dbo.APCStay.SSTAY_REFNO = SStay.SSTAY_REFNO LEFT OUTER JOIN
                      dbo.vwCriticalCareOrganSupport AS OS ON dbo.APCStay.APCST_REFNO = OS.APCST_REFNO
WHERE     (dbo.APCStay.ARCHV_FLAG = 'N')
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -96
         Left = 0
      End
      Begin Tables = 
         Begin Table = "APCStay"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 237
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "CCP"
            Begin Extent = 
               Top = 6
               Left = 275
               Bottom = 114
               Right = 474
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Spell"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 255
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SStay"
            Begin Extent = 
               Top = 149
               Left = 315
               Bottom = 257
               Right = 582
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "OS"
            Begin Extent = 
               Top = 255
               Left = 159
               Bottom = 363
               Right = 402
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 12
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths =' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ExtractCriticalCarePeriodStay'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' 11
         Column = 1440
         Alias = 2130
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ExtractCriticalCarePeriodStay'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ExtractCriticalCarePeriodStay'