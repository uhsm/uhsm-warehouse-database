﻿CREATE View [dbo].[ExtractLorenzoClinicBase] AS 

/***********************************************************************************************************************

Purpose:			Create a reference to the Staff type seeing patient in outpatient cube

CreateDate:			16/09/2010
Author:				Kevin Daley

Current Version:	1.0
Version Date:		16/09/2010

Change Log: 
					13/09/2010 Kevin Daley
						Initial Version
					<ENTER DATE> <ENTER AUTHOR>
						<ENTER CHANGE>


***********************************************************************************************************************/
select  
	ClinicRefno = sp.SPONT_REFNO
	,ClinicCode = sp.CODE
	,ClinicTypeCode = pc.PRTYP_REFNO
	,[Clinic Type] = pc.PRTYP_REFNO_DESCRIPTION
	 
from ServicePoint sp
left outer join ProfessionalCarer pc on sp.PROCA_REFNO = pc.PROCA_REFNO
WHERE SPTYP_REFNO = 1514
and sp.ARCHV_FLAG = 'n'