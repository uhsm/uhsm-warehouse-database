﻿CREATE View [dbo].[ExtractLorenzoUser] as

Select
	 USERS_REFNO
	,UserUniqueID = [CODE] 
	,UserName = [USER_NAME]
	,UserForename = SUBSTRING([USER_NAME],charindex(' ',[USER_NAME])+1,Len([USER_NAME]) - charindex(' ',[USER_NAME])+1)
	,UserSurname =	Case When charindex(' ',[USER_NAME]) <= 0 then
						[USER_NAME]
					Else
					Left([USER_NAME],charindex(' ',[USER_NAME])-1) 
					End
	 
From dbo.[User]
where ARCHV_FLAG = 'N'