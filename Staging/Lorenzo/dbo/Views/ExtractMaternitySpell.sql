﻿CREATE view [dbo].[ExtractMaternitySpell] as

select
	 SourceUniqueID = MaternitySpell.MATSP_REFNO

--occasionally this column is linked to an archived episode
--in this case find the first, non-archived, episode in the spell

	,EpisodeSourceUniqueID =
		case
		when exists
			(
			select
				1
			from
				dbo.ProfessionalCareEpisode
			where
				ProfessionalCareEpisode.PRCAE_REFNO = MaternitySpell.PRCAE_REFNO
			and	ProfessionalCareEpisode.ARCHV_FLAG = 'N'
			)
		then MaternitySpell.PRCAE_REFNO

		else
			(
			select
				ProfessionalCareEpisode.PRCAE_REFNO
			from
				dbo.ProfessionalCareEpisode
			where
				ProfessionalCareEpisode.PRVSP_REFNO = MaternitySpell.PRVSP_REFNO
			and	ProfessionalCareEpisode.ARCHV_FLAG = 'N'
			and	not exists
				(
				select
					1
				from
					dbo.ProfessionalCareEpisode Previous
				where
					Previous.PRVSP_REFNO = MaternitySpell.PRVSP_REFNO
				and	Previous.ARCHV_FLAG = 'N'
				and	Previous.START_DTTM < ProfessionalCareEpisode.START_DTTM
				)
			)

		end

	,SourcePatientNo = MaternitySpell.PATNT_REFNO
	,SourceSpellNo = MaternitySpell.PRVSP_REFNO
	,ReferralSourceUniqueID = MaternitySpell.REFRL_REFNO
	,ProfessionalCarerCode = MaternitySpell.PROCA_REFNO
	,ServicePointStayCode = MaternitySpell.SSTAY_REFNO
	,ServicePointCode = MaternitySpell.SPONT_REFNO

	,DeliveryPlaceCode = MaternitySpell.DEPLA_REFNO
	,DeliveryPlaceChangeReasonCode = MaternitySpell.DEPCR_REFNO
	,PregnancyNumber = MaternitySpell.PREGNANCY_NUMBER
	,Parity = MaternitySpell.PARITY
	,FirstAnteNatalAssessmentDate = MaternitySpell.FIRST_ANTENATAL
	,MaternalHeight = MaternitySpell.MATERNAL_HEIGHT
	,NumberOfPreviousCaesareanDeliveries = MaternitySpell.NOOF_PRE_CAESAREANS
	,NumberOfPreviousInducedAbortions = MaternitySpell.NOOF_IND_ABORTIONS
	,TotalLiveBirths = MaternitySpell.TOTAL_LIVE_BIRTHS
	,TotalStillBirths = MaternitySpell.TOTAL_STILL_BIRTHS
	,TotalNeonatalDeaths = MaternitySpell.TOTAL_NEONATAL_DEATHS
	,PreviousPregnancies = MaternitySpell.TOTAL_PREVIOUS_PREGS
	,NumberOfPreviousNonInducedAbortions = MaternitySpell.NOOF_NIN_ABORTIONS
	,StartTime = MaternitySpell.START_DTTM
	,EndTime = MaternitySpell.END_DTTM
	,PreviousBloodTransfusions = MaternitySpell.PREVIOUS_BLOOD_TRANS
	,RubellaImmunity = MaternitySpell.RUBLA_IMMUNE
	,RubellaTested = MaternitySpell.RUBLA_TESTED
	,RubellaPositive = MaternitySpell.RUBLA_POSITIVE
	,RubellaImmunised = MaternitySpell.RUBLA_IMMUNISED
	,DeliveryTime = MaternitySpell.DELIV_DTTM
	,LabourOnsetMethodCode = MaternitySpell.ONSET_REFNO
	,StatusOfPersonConductingDeliveryCode = MaternitySpell.DPSTS_REFNO
	,GestationLength = MaternitySpell.GESTN_LENGTH
	,FirstStageOfPregnancyWeeks = MaternitySpell.FIRST_STAGE
	,SecondStageOfPregnancyWeeks = MaternitySpell.SECOND_STAGE
	,NumberOfBabies = MaternitySpell.NOOF_BABIES
	,AnaestheticGivenDuringCode = MaternitySpell.DURING_ANALC_REFNO
	,AnaestheticGivenPostCode = MaternitySpell.AFTER_ANALC_REFNO

	,Stage1Time = MaternitySpell.STAGE1_DTTM
	,Stage2Time = MaternitySpell.STAGE2_DTTM
	,IntendedDeliveryPlaceTypeCode = MaternitySpell.INITIAL_DEPLA_REFNO
	,DelivererCode = MaternitySpell.DELIV_PROCA_REFNO
	,AdditionalDelivererCode = MaternitySpell.ADDITIONAL_PROCA_REFNO
	,PriorProfessionalCarer = MaternitySpell.PRIOR_PROCA_REFNO
	,AnaestheticGivenDuringReasonCode = MaternitySpell.DUR_ANRSN_REFNO
	,AnaestheticGivenPostReasonCode = MaternitySpell.AFT_ANRSN_REFNO
	,MaternityPeriodCode = MaternitySpell.MTPER_REFNO
	,AnaestheticReasonCode = MaternitySpell.ANRSN_REFNO
	,SelfAdministeredInhalationCode = MaternitySpell.SELF_YNUNK_REFNO
	,NarcoticsCode = MaternitySpell.NARC_YNUNK_REFNO
	,EpiduralOrCaudalCode = MaternitySpell.EPID_YNUNK_REFNO
	,SpinalCode = MaternitySpell.SPIN_YNUNK_REFNO
	,LocationInfiltrationCode = MaternitySpell.LOCN_YNUNK_REFNO
	,GeneralCode = MaternitySpell.GENR_YNUNK_REFNO
	,OtherCode = MaternitySpell.OTHR_YNUNK_REFNO
	,PreviousBloodTransfusionComments = MaternitySpell.PREV_BLOOD_TRANS_COMMENTS

	,PASCreated = MaternitySpell.CREATE_DTTM
	,PASUpdated = MaternitySpell.MODIF_DTTM
	,PASCreatedByWhom = MaternitySpell.USER_CREATE
	,PASUpdatedByWhom = MaternitySpell.USER_MODIF

	,ArchiveFlag =
		case
		when MaternitySpell.ARCHV_FLAG = 'Y'
		then 1
		when MaternitySpell.ARCHV_FLAG = 'C'
		then 1
		when ProviderSpell.ARCHV_FLAG = 'Y'
		then 1
		when ProviderSpell.ARCHV_FLAG = 'C'
		then 1
		when MaternitySpell.ARCHV_FLAG = 'N'
		then 0
		else 1
		end

	,MaternitySpell.Created


from
	dbo.MaternitySpell

left join dbo.ProviderSpell
on	ProviderSpell.PRVSP_REFNO = MaternitySpell.PRVSP_REFNO

where
	exists
		(
		select
			1
		from
			dbo.ExtractBirth
		where
			ExtractBirth.MaternitySpellSourceUniqueID = MaternitySpell.MATSP_REFNO
		)