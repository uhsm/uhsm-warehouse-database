﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		View based on dbo.ScheduleEvent.
				Used in the load of the TImportOPEncounter table.

Notes:			Stored in 

Versions:
				1.0.0.0 - 15/10/2009
					Original view.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE VIEW [dbo].[ExtractOP]
AS
--SELECT     Outpatient.SCHDL_REFNO AS SourceUniqueID, Outpatient.PATNT_REFNO AS SourcePatientNo, Outpatient.SCHDL_REFNO AS SourceEncounterNo, 
--                      Outpatient.REFRL_REFNO AS ReferralSourceUniqueID, Outpatient.WLIST_REFNO AS WaitingListSourceUniqueID, 
--                      dbo.Patient.TITLE_REFNO_DESCRIPTION AS PatientTitle, dbo.Patient.FORENAME AS PatientForename, dbo.Patient.SURNAME AS PatientSurname, 
--                      dbo.Patient.DTTM_OF_BIRTH AS DateOfBirth, dbo.Patient.DTTM_OF_DEATH AS DateOfDeath, dbo.Patient.SEXXX_REFNO AS SexCode, 
--                      dbo.Patient.PATNT_REFNO_NHS_IDENTIFIER AS NHSNumber, DistrictNo.IDENTIFIER AS DistrictNo, dbo.PatientAddress.PCODE AS Postcode, 
--                      dbo.PatientAddress.LINE1 AS PatientAddress1, dbo.PatientAddress.LINE2 AS PatientAddress2, dbo.PatientAddress.LINE3 AS PatientAddress3, 
--                      dbo.PatientAddress.LINE4 AS PatientAddress4, dbo.PatientAddress.HDIST_CODE AS DHACode, dbo.Patient.ETHGR_REFNO AS EthnicOriginCode, 
--                      dbo.Patient.MARRY_REFNO AS MaritalStatusCode, dbo.Patient.RELIG_REFNO AS ReligionCode, COALESCE (RegisteredGp.PROCA_REFNO, 
--                      DefaultRegisteredGp.PROCA_REFNO) AS RegisteredGpCode, COALESCE (RegisteredGp.HEORG_REFNO, DefaultRegisteredGp.HEORG_REFNO) 
--                      AS RegisteredGpPracticeCode, COALESCE (RegisteredGp.PROCA_REFNO, DefaultRegisteredGp.PROCA_REFNO) AS EpisodicGpCode, 
--                      COALESCE (RegisteredGp.HEORG_REFNO, DefaultRegisteredGp.HEORG_REFNO) AS EpisodicGpPracticeCode, Outpatient.SPONT_REFNO AS SiteCode, DATEADD(day, 
--                      DATEDIFF(day, 0, Outpatient.START_DTTM), 0) AS AppointmentDate, Outpatient.START_DTTM AS AppointmentTime, Clinic.SPONT_REFNO_CODE AS ClinicCode, 
--                      Outpatient.ADCAT_REFNO AS AdminCategoryCode, dbo.Referral.SORRF_REFNO AS SourceOfReferralCode, Outpatient.REASN_REFNO AS ReasonForReferralCode, 
--                      dbo.Referral.PRITY_REFNO AS PriorityCode, Outpatient.VISIT_REFNO AS FirstAttendanceFlag, Outpatient.ATTND_REFNO AS AppointmentStatusCode, 
--                      Outpatient.CANCB_REFNO AS CancelledByCode, Outpatient.TRANS_REFNO AS TransportRequiredFlag, Outpatient.SCTYP_REFNO AS AppointmentTypeCode, 
--                      Outpatient.SCOCM_REFNO AS DisposalCode, Outpatient.PROCA_REFNO AS ConsultantCode, Outpatient.SPECT_REFNO AS SpecialtyCode, NULL 
--                      AS ReferringConsultantCode, NULL AS ReferringSpecialtyCode, Outpatient.BKTYP_REFNO AS BookingTypeCode, NULL AS CasenoteNo, 
--                      Outpatient.CREATE_DTTM AS AppointmentCreateDate, Clinic.PROCA_REFNO AS DoctorCode, NULL AS PrimaryDiagnosisCode, NULL 
--                      AS SubsidiaryDiagnosisCode, NULL AS SecondaryDiagnosisCode1, NULL AS SecondaryDiagnosisCode2, NULL AS SecondaryDiagnosisCode3, NULL 
--                      AS SecondaryDiagnosisCode4, NULL AS SecondaryDiagnosisCode5, NULL AS SecondaryDiagnosisCode6, NULL AS SecondaryDiagnosisCode7, NULL 
--                      AS SecondaryDiagnosisCode8, NULL AS SecondaryDiagnosisCode9, NULL AS SecondaryDiagnosisCode10, NULL AS SecondaryDiagnosisCode11, NULL 
--                      AS SecondaryDiagnosisCode12, NULL AS PrimaryOperationCode, NULL AS PrimaryOperationDate, NULL AS SecondaryOperationCode1, NULL 
--                      AS SecondaryOperationDate1, NULL AS SecondaryOperationCode2, NULL AS SecondaryOperationDate2, NULL AS SecondaryOperationCode3, NULL 
--                      AS SecondaryOperationDate3, NULL AS SecondaryOperationCode4, NULL AS SecondaryOperationDate4, NULL AS SecondaryOperationCode5, NULL 
--                      AS SecondaryOperationDate5, NULL AS SecondaryOperationCode6, NULL AS SecondaryOperationDate6, NULL AS SecondaryOperationCode7, NULL 
--                      AS SecondaryOperationDate7, NULL AS SecondaryOperationCode8, NULL AS SecondaryOperationDate8, NULL AS SecondaryOperationCode9, NULL 
--                      AS SecondaryOperationDate9, NULL AS SecondaryOperationCode10, NULL AS SecondaryOperationDate10, NULL AS SecondaryOperationCode11, NULL 
--                      AS SecondaryOperationDate11, Outpatient.PURCH_REFNO AS PurchaserCode, Outpatient.PROVD_REFNO AS ProviderCode, 
--                      dbo.Contract.SERIAL_NUMBER AS ContractSerialNo, dbo.Referral.RECVD_DTTM AS ReferralDate, dbo.Referral.PATNT_PATHWAY_ID AS RTTPathwayID, NULL 
--                      AS RTTPathwayCondition, dbo.Referral.RTT_START_DATE AS RTTStartDate, NULL AS RTTEndDate, NULL AS RTTSpecialtyCode, NULL AS RTTCurrentProviderCode, 
--                      dbo.RTT.RTTST_REFNO AS RTTCurrentStatusCode, dbo.RTT.RTTST_DATE AS RTTCurrentStatusDate, NULL AS RTTCurrentPrivatePatientFlag, NULL 
--                      AS RTTOverseasStatusFlag, COALESCE (PeriodStatus.RTTST_REFNO, dbo.Referral.RTTST_REFNO) AS RTTPeriodStatusCode, NULL AS AppointmentCategoryCode, 
--                      COALESCE (CreatedUser.USER_NAME, Outpatient.USER_CREATE) AS AppointmentCreatedBy, Outpatient.CANCR_DTTM AS AppointmentCancelDate, 
--                      Outpatient.MODIF_DTTM AS LastRevisedDate, COALESCE (UpdatedUser.USER_NAME, Outpatient.USER_MODIF) AS LastRevisedBy, 
--                      OverseasVisitorStatus.OVSVS_REFNO AS OverseasStatusFlag, NULL AS PatientChoiceCode, Outpatient.CANCR_REFNO AS ScheduledCancelReasonCode, 
--                      Outpatient.DNARS_REFNO AS PatientCancelReason, dbo.Referral.CLOSR_DATE AS DischargeDate, 
--                      COALESCE (CASE WHEN ScheduleHistoryQM08StartDate.OLD_START_DTTM IS NULL 
--                      THEN ScheduleEventQM08StartDate.REQST_DTTM WHEN ScheduleEventQM08StartDate.REQST_DTTM IS NULL 
--                      THEN ScheduleHistoryQM08StartDate.OLD_START_DTTM WHEN ScheduleHistoryQM08StartDate.OLD_START_DTTM > ScheduleEventQM08StartDate.REQST_DTTM THEN
--                       ScheduleHistoryQM08StartDate.OLD_START_DTTM ELSE ScheduleEventQM08StartDate.REQST_DTTM END, dbo.WaitingList.WLIST_DTTM) AS QM08StartWaitDate, 
--                      dbo.WaitingList.REMVL_DTTM AS QM08EndWaitDate, NULL AS DestinationSiteCode, Outpatient.UBRN AS EBookingReferenceNo, 'LOR' AS InterfaceCode, 
--                      CONVERT(bit, CASE WHEN Outpatient.SCTYP_REFNO = 1470 THEN 0 ELSE 1 END) AS IsWardAttender, Outpatient.CREATE_DTTM AS PASCreated, 
--                      Outpatient.MODIF_DTTM AS PASUpdated, COALESCE (CreatedUser.USER_NAME, Outpatient.USER_CREATE) AS PASCreatedByWhom, 
--                      COALESCE (UpdatedUser.USER_NAME, Outpatient.USER_MODIF) AS PASUpdatedByWhom, CONVERT(bit, 
--                      CASE WHEN Outpatient.ARCHV_FLAG = 'N' THEN 0 ELSE 1 END) AS ArchiveFlag, SpontProfCarer.PRTYP_REFNO AS ClinicType, 
--                      Outpatient.SPSSN_REFNO AS SessionUniqueID, dbo.Patient.NNNTS_CODE AS NHSNumberStatusCode
--FROM         dbo.ScheduleEvent AS Outpatient LEFT OUTER JOIN
--                      dbo.Patient ON dbo.Patient.PATNT_REFNO = Outpatient.PATNT_REFNO LEFT OUTER JOIN
--                      dbo.Referral ON dbo.Referral.REFRL_REFNO = Outpatient.REFRL_REFNO LEFT OUTER JOIN
--                      dbo.PatientIdentifier AS DistrictNo ON DistrictNo.PATNT_REFNO = Outpatient.PATNT_REFNO AND DistrictNo.PITYP_REFNO = 2001232 AND 
--                      DistrictNo.IDENTIFIER LIKE 'RM2%' AND DistrictNo.ARCHV_FLAG = 'N' AND NOT EXISTS
--                          (SELECT     1 AS Expr1
--                            FROM          dbo.PatientIdentifier AS Previous
--                            WHERE      (PATNT_REFNO = DistrictNo.PATNT_REFNO) AND (PITYP_REFNO = DistrictNo.PITYP_REFNO) AND (IDENTIFIER LIKE 'RM2%') AND (ARCHV_FLAG = 'N') 
--                                                   AND (START_DTTM > DistrictNo.START_DTTM) OR
--                                                   (PATNT_REFNO = DistrictNo.PATNT_REFNO) AND (PITYP_REFNO = DistrictNo.PITYP_REFNO) AND (IDENTIFIER LIKE 'RM2%') AND (ARCHV_FLAG = 'N') 
--                                                   AND (START_DTTM = DistrictNo.START_DTTM) AND (PATID_REFNO > DistrictNo.PATID_REFNO)) LEFT OUTER JOIN
--                      dbo.PatientAddressRole ON dbo.PatientAddressRole.PATNT_REFNO = dbo.Patient.PATNT_REFNO AND dbo.PatientAddressRole.ROTYP_CODE = 'HOME' AND 
--                      dbo.PatientAddressRole.ARCHV_FLAG = 'N' AND EXISTS
--                          (SELECT     1 AS Expr1
--                            FROM          dbo.PatientAddress
--                            WHERE      (ADDSS_REFNO = dbo.PatientAddressRole.ADDSS_REFNO) AND (ADTYP_CODE = 'POSTL') AND (ARCHV_FLAG = 'N')) AND 
--                      Outpatient.START_DTTM BETWEEN dbo.PatientAddressRole.START_DTTM AND COALESCE (dbo.PatientAddressRole.END_DTTM, Outpatient.START_DTTM) AND 
--                      NOT EXISTS
--                          (SELECT     1 AS Expr1
--                            FROM          dbo.PatientAddressRole AS Previous
--                            WHERE      (PATNT_REFNO = dbo.PatientAddressRole.PATNT_REFNO) AND (ROTYP_CODE = dbo.PatientAddressRole.ROTYP_CODE) AND (ARCHV_FLAG = 'N') AND 
--                                                   EXISTS
--                                                       (SELECT     1 AS Expr1
--                                                         FROM          dbo.PatientAddress
--                                                         WHERE      (ADDSS_REFNO = Previous.ADDSS_REFNO) AND (ADTYP_CODE = 'POSTL') AND (ARCHV_FLAG = 'N')) AND 
--                                                   (Outpatient.START_DTTM BETWEEN START_DTTM AND COALESCE (END_DTTM, Outpatient.START_DTTM)) AND 
--                                                   (START_DTTM > dbo.PatientAddressRole.START_DTTM) OR
--                                                   (PATNT_REFNO = dbo.PatientAddressRole.PATNT_REFNO) AND (ROTYP_CODE = dbo.PatientAddressRole.ROTYP_CODE) AND (ARCHV_FLAG = 'N') AND 
--                                                   (Outpatient.START_DTTM BETWEEN START_DTTM AND COALESCE (END_DTTM, Outpatient.START_DTTM)) AND 
--                                                   (START_DTTM = dbo.PatientAddressRole.START_DTTM) AND EXISTS
--                                                       (SELECT     1 AS Expr1
--                                                         FROM          dbo.PatientAddress
--                                                         WHERE      (ADDSS_REFNO = Previous.ADDSS_REFNO) AND (ADTYP_CODE = 'POSTL') AND (ARCHV_FLAG = 'N')) AND 
--                                                   (ROLES_REFNO > dbo.PatientAddressRole.ROLES_REFNO)) LEFT OUTER JOIN
--                      dbo.PatientAddress ON dbo.PatientAddress.ADDSS_REFNO = dbo.PatientAddressRole.ADDSS_REFNO AND dbo.PatientAddress.ARCHV_FLAG = 'N' LEFT OUTER JOIN
--                      dbo.PatientProfessionalCarer AS DefaultRegisteredGp ON DefaultRegisteredGp.PATNT_REFNO = Outpatient.PATNT_REFNO AND 
--                      DefaultRegisteredGp.PRTYP_MAIN_CODE = 'GMPRC' AND DefaultRegisteredGp.ARCHV_FLAG = 'N' AND 
--                      Outpatient.START_DTTM < DefaultRegisteredGp.START_DTTM AND NOT EXISTS
--                          (SELECT     1 AS Expr1
--                            FROM          dbo.PatientProfessionalCarer AS Previous
--                            WHERE      (PATNT_REFNO = DefaultRegisteredGp.PATNT_REFNO) AND (PRTYP_MAIN_CODE = DefaultRegisteredGp.PRTYP_MAIN_CODE) AND 
--                                                   (ARCHV_FLAG = DefaultRegisteredGp.ARCHV_FLAG) AND (Outpatient.START_DTTM < START_DTTM) AND 
--                                                   (START_DTTM > DefaultRegisteredGp.START_DTTM) OR
--                                                   (PATNT_REFNO = DefaultRegisteredGp.PATNT_REFNO) AND (PRTYP_MAIN_CODE = DefaultRegisteredGp.PRTYP_MAIN_CODE) AND 
--                                                   (ARCHV_FLAG = DefaultRegisteredGp.ARCHV_FLAG) AND (Outpatient.START_DTTM < START_DTTM) AND 
--                                                   (START_DTTM = DefaultRegisteredGp.START_DTTM) AND (PATPC_REFNO > DefaultRegisteredGp.PATPC_REFNO)) LEFT OUTER JOIN
--                      dbo.PatientProfessionalCarer AS RegisteredGp ON RegisteredGp.PATNT_REFNO = Outpatient.PATNT_REFNO AND RegisteredGp.PRTYP_MAIN_CODE = 'GMPRC' AND 
--                      RegisteredGp.ARCHV_FLAG = 'N' AND Outpatient.START_DTTM BETWEEN RegisteredGp.START_DTTM AND COALESCE (RegisteredGp.END_DTTM, 
--                      Outpatient.START_DTTM) AND NOT EXISTS
--                          (SELECT     1 AS Expr1
--                            FROM          dbo.PatientProfessionalCarer AS Previous
--                            WHERE      (PATNT_REFNO = RegisteredGp.PATNT_REFNO) AND (PRTYP_MAIN_CODE = RegisteredGp.PRTYP_MAIN_CODE) AND (ARCHV_FLAG = 'N') AND 
--                                                   (Outpatient.START_DTTM BETWEEN START_DTTM AND COALESCE (END_DTTM, Outpatient.START_DTTM)) AND 
--                                                   (START_DTTM > RegisteredGp.START_DTTM) OR
--                                                   (PATNT_REFNO = RegisteredGp.PATNT_REFNO) AND (PRTYP_MAIN_CODE = RegisteredGp.PRTYP_MAIN_CODE) AND (ARCHV_FLAG = 'N') AND 
--                                                   (Outpatient.START_DTTM BETWEEN START_DTTM AND COALESCE (END_DTTM, Outpatient.START_DTTM)) AND 
--                                                   (START_DTTM = RegisteredGp.START_DTTM) AND (PATPC_REFNO > RegisteredGp.PATPC_REFNO)) LEFT OUTER JOIN
--                      dbo.Contract ON dbo.Contract.CONTR_REFNO = Outpatient.CONTR_REFNO AND dbo.Contract.ARCHV_FLAG = 'N' LEFT OUTER JOIN
--                      dbo.[User] AS CreatedUser ON CreatedUser.USERS_REFNO = Outpatient.USER_REFNO AND CreatedUser.ARCHV_FLAG = 'N' LEFT OUTER JOIN
--                      dbo.[User] AS UpdatedUser ON UpdatedUser.USERS_REFNO = Outpatient.USER_REFNO AND UpdatedUser.ARCHV_FLAG = 'N' LEFT OUTER JOIN
--                      dbo.ServicePoint AS Clinic ON Clinic.SPONT_REFNO = Outpatient.SPONT_REFNO AND Clinic.ARCHV_FLAG = 'N' LEFT OUTER JOIN
--                      dbo.ProfessionalCarer AS SpontProfCarer ON Clinic.PROCA_REFNO = SpontProfCarer.PROCA_REFNO AND SpontProfCarer.ARCHV_FLAG = 'N' LEFT OUTER JOIN
--                      dbo.OverseasVisitorStatus AS OverseasVisitorStatus ON OverseasVisitorStatus.PATNT_REFNO = Outpatient.PATNT_REFNO AND 
--                      OverseasVisitorStatus.ARCHV_FLAG = 'N' AND Outpatient.START_DTTM BETWEEN OverseasVisitorStatus.START_DTTM AND 
--                      COALESCE (OverseasVisitorStatus.END_DTTM, Outpatient.START_DTTM) AND NOT EXISTS
--                          (SELECT     1 AS Expr1
--                            FROM          dbo.OverseasVisitorStatus AS Previous
--                            WHERE      (PATNT_REFNO = OverseasVisitorStatus.PATNT_REFNO) AND (ARCHV_FLAG = 'N') AND (Outpatient.START_DTTM BETWEEN START_DTTM AND 
--                                                   COALESCE (END_DTTM, Outpatient.START_DTTM)) AND (START_DTTM > OverseasVisitorStatus.START_DTTM) OR
--                                                   (PATNT_REFNO = OverseasVisitorStatus.PATNT_REFNO) AND (ARCHV_FLAG = 'N') AND (Outpatient.START_DTTM BETWEEN START_DTTM AND 
--                                                   COALESCE (END_DTTM, Outpatient.START_DTTM)) AND (START_DTTM = OverseasVisitorStatus.START_DTTM) AND 
--                                                   (OVSEA_REFNO > OverseasVisitorStatus.OVSEA_REFNO)) LEFT OUTER JOIN
--                      dbo.WaitingList ON dbo.WaitingList.WLIST_REFNO = Outpatient.WLIST_REFNO AND dbo.WaitingList.ARCHV_FLAG = 'N' LEFT OUTER JOIN
--                      dbo.ScheduleHistory AS ScheduleHistoryQM08StartDate ON ScheduleHistoryQM08StartDate.SCHDL_REFNO = Outpatient.SCHDL_REFNO AND 
--                      ScheduleHistoryQM08StartDate.MOVRN_REFNO IN (2003544, 3248, 2004148, 2003541, 2003540, 383, 2003539, 4520) AND 
--                      ScheduleHistoryQM08StartDate.ARCHV_FLAG = 'N' AND ScheduleHistoryQM08StartDate.NEW_START_DTTM > ScheduleHistoryQM08StartDate.OLD_START_DTTM AND
--                       ScheduleHistoryQM08StartDate.WLIST_REFNO IS NOT NULL AND ScheduleHistoryQM08StartDate.OLD_START_DTTM < Outpatient.START_DTTM AND 
--                      NOT EXISTS
--                          (SELECT     1 AS Expr1
--                            FROM          dbo.ScheduleHistory AS Previous
--                            WHERE      (SCHDL_REFNO = Outpatient.SCHDL_REFNO) AND (MOVRN_REFNO IN (2003544, 3248, 2004148, 2003541, 2003540, 383, 2003539, 4520)) AND 
--                                                   (ARCHV_FLAG = 'N') AND (NEW_START_DTTM > OLD_START_DTTM) AND (WLIST_REFNO IS NOT NULL) AND 
--                                                   (OLD_START_DTTM < Outpatient.START_DTTM) AND (OLD_START_DTTM > ScheduleHistoryQM08StartDate.OLD_START_DTTM) OR
--                                                   (SCHDL_REFNO = Outpatient.SCHDL_REFNO) AND (MOVRN_REFNO IN (2003544, 3248, 2004148, 2003541, 2003540, 383, 2003539, 4520)) AND 
--                                                   (ARCHV_FLAG = 'N') AND (NEW_START_DTTM > OLD_START_DTTM) AND (WLIST_REFNO IS NOT NULL) AND 
--                                                   (OLD_START_DTTM < Outpatient.START_DTTM) AND (OLD_START_DTTM = ScheduleHistoryQM08StartDate.OLD_START_DTTM) AND 
--                                                   (SCDHS_REFNO > ScheduleHistoryQM08StartDate.SCDHS_REFNO)) LEFT OUTER JOIN
--                      dbo.ScheduleEvent AS ScheduleEventQM08StartDate ON ScheduleEventQM08StartDate.REFRL_REFNO = Outpatient.REFRL_REFNO AND 
--                      ScheduleEventQM08StartDate.ATTND_REFNO IN (2868, 2870, 358, 2003495, 2004301, 2003494, 2000724) AND ScheduleEventQM08StartDate.ARCHV_FLAG = 'N' AND 
--                      ScheduleEventQM08StartDate.WLIST_REFNO IS NOT NULL AND ScheduleEventQM08StartDate.START_DTTM < Outpatient.START_DTTM AND 
--                      ScheduleEventQM08StartDate.REQST_DTTM IS NOT NULL AND NOT EXISTS
--                          (SELECT     1 AS Expr1
--                            FROM          dbo.ScheduleEvent AS Previous
--                            WHERE      (REFRL_REFNO = Outpatient.REFRL_REFNO) AND (ATTND_REFNO IN (2868, 2870, 358, 2003495, 2004301, 2003494, 2000724)) AND (ARCHV_FLAG = 'N') 
--                                                   AND (WLIST_REFNO IS NOT NULL) AND (START_DTTM < Outpatient.START_DTTM) AND 
--                                                   (REQST_DTTM > ScheduleEventQM08StartDate.REQST_DTTM) OR
--                                                   (REFRL_REFNO = Outpatient.REFRL_REFNO) AND (ATTND_REFNO IN (2868, 2870, 358, 2003495, 2004301, 2003494, 2000724)) AND (ARCHV_FLAG = 'N') 
--                                                   AND (WLIST_REFNO IS NOT NULL) AND (START_DTTM < Outpatient.START_DTTM) AND (REQST_DTTM = ScheduleEventQM08StartDate.REQST_DTTM) 
--                                                   AND (SCHDL_REFNO > ScheduleEventQM08StartDate.SCHDL_REFNO)) LEFT OUTER JOIN
--                      dbo.RTT AS PeriodStatus ON PeriodStatus.SORCE = 'SCHDL' AND PeriodStatus.SORCE_REFNO = Outpatient.SCHDL_REFNO AND PeriodStatus.ARCHV_FLAG = 'N' AND
--                       NOT EXISTS
--                          (SELECT     1 AS Expr1
--                            FROM          dbo.RTT AS Previous
--                            WHERE      (SORCE = 'SCHDL') AND (SORCE_REFNO = Outpatient.SCHDL_REFNO) AND (ARCHV_FLAG = 'N') AND (RTTST_DATE > PeriodStatus.RTTST_DATE) OR
--                                                   (SORCE = 'SCHDL') AND (SORCE_REFNO = Outpatient.SCHDL_REFNO) AND (ARCHV_FLAG = 'N') AND (RTTST_DATE = PeriodStatus.RTTST_DATE) AND 
--                                                   (RTTPR_REFNO > PeriodStatus.RTTPR_REFNO)) LEFT OUTER JOIN
--                      dbo.RTT ON dbo.RTT.REFRL_REFNO = Outpatient.REFRL_REFNO AND dbo.RTT.ARCHV_FLAG = 'N' AND NOT EXISTS
--                          (SELECT     1 AS Expr1
--                            FROM          dbo.RTT AS Previous
--                            WHERE      (REFRL_REFNO = Outpatient.REFRL_REFNO) AND (ARCHV_FLAG = 'N') AND (RTTST_DATE > dbo.RTT.RTTST_DATE) OR
--                                                   (REFRL_REFNO = Outpatient.REFRL_REFNO) AND (ARCHV_FLAG = 'N') AND (RTTST_DATE = dbo.RTT.RTTST_DATE) AND 
--                                                   (RTTPR_REFNO > dbo.RTT.RTTPR_REFNO))
--WHERE     (Outpatient.SCTYP_REFNO IN (1470, 1472))

SELECT    
	Outpatient.SCHDL_REFNO AS SourceUniqueID, 
	Outpatient.PATNT_REFNO AS SourcePatientNo, 
	Outpatient.SCHDL_REFNO AS SourceEncounterNo, 
	Outpatient.REFRL_REFNO AS ReferralSourceUniqueID, 
	Outpatient.WLIST_REFNO AS WaitingListSourceUniqueID, 
	dbo.Patient.TITLE_REFNO_DESCRIPTION AS PatientTitle, 
	dbo.Patient.FORENAME AS PatientForename, 
	dbo.Patient.SURNAME AS PatientSurname, 
	dbo.Patient.DTTM_OF_BIRTH AS DateOfBirth, 
	dbo.Patient.DTTM_OF_DEATH AS DateOfDeath, 
	dbo.Patient.SEXXX_REFNO AS SexCode, 
	Coalesce(dbo.Patient.PATNT_REFNO_NHS_IDENTIFIER, dbo.Patient.NHS_IDENTIFIER) AS NHSNumber, --CM 29/03/2016 It doesn't look like the 'PATNT_REFNO_NHS_IDENTIFIER' is always populated so have added the 'NHS_IDENTIFIER' in to capture the NHS Number for those where it is null
	DistrictNo.IDENTIFIER AS DistrictNo, 
	dbo.PatientAddress.PCODE AS Postcode, 
	dbo.PatientAddress.LINE1 AS PatientAddress1, 
	dbo.PatientAddress.LINE2 AS PatientAddress2, 
	dbo.PatientAddress.LINE3 AS PatientAddress3, 
	dbo.PatientAddress.LINE4 AS PatientAddress4, 
	dbo.PatientAddress.HDIST_CODE AS DHACode, 
	dbo.Patient.ETHGR_REFNO AS EthnicOriginCode, 
	dbo.Patient.MARRY_REFNO AS MaritalStatusCode, 
	dbo.Patient.RELIG_REFNO AS ReligionCode, 
	COALESCE (RegisteredGp.PROCA_REFNO, DefaultRegisteredGp.PROCA_REFNO) AS RegisteredGpCode, 
	COALESCE (RegisteredGp.HEORG_REFNO, DefaultRegisteredGp.HEORG_REFNO) AS RegisteredGpPracticeCode, 
	COALESCE (RegisteredGp.PROCA_REFNO, DefaultRegisteredGp.PROCA_REFNO) AS EpisodicGpCode, 
    COALESCE (RegisteredGp.HEORG_REFNO, DefaultRegisteredGp.HEORG_REFNO) AS EpisodicGpPracticeCode, 
	Outpatient.SPONT_REFNO AS SiteCode, DATEADD(day, DATEDIFF(day, 0, Outpatient.START_DTTM), 0) AS AppointmentDate, 
	Outpatient.START_DTTM AS AppointmentTime, 
	Clinic.CODE AS ClinicCode, 
    Outpatient.ADCAT_REFNO AS AdminCategoryCode, 
	dbo.Referral.SORRF_REFNO AS SourceOfReferralCode, 
	Outpatient.REASN_REFNO AS ReasonForReferralCode, 
	dbo.Referral.PRITY_REFNO AS PriorityCode, 
	Outpatient.VISIT_REFNO AS FirstAttendanceFlag, 
	Outpatient.ATTND_REFNO AS AppointmentStatusCode, 
    Outpatient.CANCB_REFNO AS CancelledByCode, 
	Outpatient.TRANS_REFNO AS TransportRequiredFlag, 
	Outpatient.SCTYP_REFNO AS AppointmentTypeCode, 
    Outpatient.SCOCM_REFNO AS DisposalCode, 
	Outpatient.PROCA_REFNO AS ConsultantCode, 
	Outpatient.SPECT_REFNO AS SpecialtyCode, 
	NULL AS ReferringConsultantCode, 
	NULL AS ReferringSpecialtyCode, 
	Outpatient.BKTYP_REFNO AS BookingTypeCode, 
	NULL AS CasenoteNo, 
    Outpatient.CREATE_DTTM AS AppointmentCreateDate, 
	Clinic.PROCA_REFNO AS DoctorCode, 
	NULL AS PrimaryDiagnosisCode, 
	NULL AS SubsidiaryDiagnosisCode, 
	NULL AS SecondaryDiagnosisCode1, 
	NULL AS SecondaryDiagnosisCode2, 
	NULL AS SecondaryDiagnosisCode3, 
	NULL AS SecondaryDiagnosisCode4, 
	NULL AS SecondaryDiagnosisCode5, 
	NULL AS SecondaryDiagnosisCode6, 
	NULL AS SecondaryDiagnosisCode7, 
	NULL AS SecondaryDiagnosisCode8, 
	NULL AS SecondaryDiagnosisCode9, 
	NULL AS SecondaryDiagnosisCode10, 
	NULL AS SecondaryDiagnosisCode11, 
	NULL AS SecondaryDiagnosisCode12, 
	NULL AS PrimaryOperationCode, 
	NULL AS PrimaryOperationDate, 
	NULL AS SecondaryOperationCode1, 
	NULL AS SecondaryOperationDate1, 
	NULL AS SecondaryOperationCode2, 
	NULL AS SecondaryOperationDate2, 
	NULL AS SecondaryOperationCode3, 
	NULL AS SecondaryOperationDate3, 
	NULL AS SecondaryOperationCode4, 
	NULL AS SecondaryOperationDate4, 
	NULL AS SecondaryOperationCode5,
	NULL AS SecondaryOperationDate5, 
	NULL AS SecondaryOperationCode6, 
	NULL AS SecondaryOperationDate6, 
	NULL AS SecondaryOperationCode7, 
	NULL AS SecondaryOperationDate7, 
	NULL AS SecondaryOperationCode8, 
	NULL AS SecondaryOperationDate8, 
	NULL AS SecondaryOperationCode9, 
	NULL AS SecondaryOperationDate9, 
	NULL AS SecondaryOperationCode10, 
	NULL AS SecondaryOperationDate10, 
	NULL AS SecondaryOperationCode11, 
	NULL AS SecondaryOperationDate11, 
	Outpatient.PURCH_REFNO AS PurchaserCode, 
	Outpatient.PROVD_REFNO AS ProviderCode, 
    dbo.Contract.SERIAL_NUMBER AS ContractSerialNo, 
	dbo.Referral.RECVD_DTTM AS ReferralDate, 
	dbo.Referral.PATNT_PATHWAY_ID AS RTTPathwayID, 
	NULL AS RTTPathwayCondition, 
	dbo.Referral.RTT_START_DATE AS RTTStartDate, 
	NULL AS RTTEndDate, 
	NULL AS RTTSpecialtyCode, 
	NULL AS RTTCurrentProviderCode, 
    dbo.RTT.RTTST_REFNO AS RTTCurrentStatusCode, 
	dbo.RTT.RTTST_DATE AS RTTCurrentStatusDate, 
	NULL AS RTTCurrentPrivatePatientFlag, 
	NULL  AS RTTOverseasStatusFlag, 
	COALESCE (PeriodStatus.RTTST_REFNO, dbo.Referral.RTTST_REFNO) AS RTTPeriodStatusCode, 
	NULL AS AppointmentCategoryCode, 
    COALESCE (CreatedUser.USER_NAME, Outpatient.USER_CREATE) AS AppointmentCreatedBy, 
	Outpatient.CANCR_DTTM AS AppointmentCancelDate, 
    Outpatient.MODIF_DTTM AS LastRevisedDate, 
	COALESCE (UpdatedUser.USER_NAME, Outpatient.USER_MODIF) AS LastRevisedBy, 
	OverseasVisitorStatus.OVSVS_REFNO AS OverseasStatusFlag, 
	OverseasStatusStartDate = OverseasVisitorStatus.START_DTTM,
	OverseasStatusEndDate = OverseasVisitorStatus.END_DTTM,
	NULL AS PatientChoiceCode, 
	Outpatient.CANCR_REFNO AS ScheduledCancelReasonCode, 
    Outpatient.DNARS_REFNO AS PatientCancelReason, 
	dbo.Referral.CLOSR_DATE AS DischargeDate, 
  --  COALESCE (CASE 
		--WHEN ScheduleHistoryQM08StartDate.OLD_START_DTTM IS NULL                     
		--	THEN ScheduleEventQM08StartDate.REQST_DTTM 
		--WHEN ScheduleEventQM08StartDate.REQST_DTTM IS NULL 
		--	THEN ScheduleHistoryQM08StartDate.OLD_START_DTTM 
		--WHEN ScheduleHistoryQM08StartDate.OLD_START_DTTM > ScheduleEventQM08StartDate.REQST_DTTM 
		--	THEN ScheduleHistoryQM08StartDate.OLD_START_DTTM 
		--ELSE ScheduleEventQM08StartDate.REQST_DTTM 
		--END, dbo.WaitingList.WLIST_DTTM) AS QM08StartWaitDate, 
  --   dbo.WaitingList.REMVL_DTTM AS QM08EndWaitDate, 
  
  --temp
	NULL AS QM08StartWaitDate, 
	NULL AS QM08EndWaitDate, 
	NULL AS DestinationSiteCode, Outpatient.UBRN AS EBookingReferenceNo, 
	'LOR' AS InterfaceCode, 
    CONVERT(bit, CASE WHEN Outpatient.SCTYP_REFNO = 1470 THEN 0 ELSE 1 END) AS IsWardAttender, 
	Outpatient.CREATE_DTTM AS PASCreated, 
    Outpatient.MODIF_DTTM AS PASUpdated, 
	COALESCE (CreatedUser.USER_NAME, Outpatient.USER_CREATE) AS PASCreatedByWhom, 
    COALESCE (UpdatedUser.USER_NAME, Outpatient.USER_MODIF) AS PASUpdatedByWhom, 
	CONVERT(bit, CASE WHEN Outpatient.ARCHV_FLAG = 'N' THEN 0 ELSE 1 END) AS ArchiveFlag, 
	SpontProfCarer.PRTYP_REFNO AS ClinicType, 
	Outpatient.SPSSN_REFNO AS SessionUniqueID, 
	dbo.Patient.NNNTS_CODE AS NHSNumberStatusCode
--INTO dbo.ExtractOutpatient
FROM         dbo.ScheduleEvent AS Outpatient 
	
	LEFT OUTER JOIN dbo.Patient 
		ON dbo.Patient.PATNT_REFNO = Outpatient.PATNT_REFNO 
	
	LEFT OUTER JOIN dbo.Referral 
		ON dbo.Referral.REFRL_REFNO = Outpatient.REFRL_REFNO 
	
	LEFT OUTER JOIN dbo.PatientIdentifier AS DistrictNo 
		ON DistrictNo.PATNT_REFNO = Outpatient.PATNT_REFNO 
		AND DistrictNo.PITYP_REFNO = 2001232 
		AND DistrictNo.IDENTIFIER LIKE 'RM2%' 
		AND DistrictNo.ARCHV_FLAG = 'N' 
		AND NOT EXISTS
                          (SELECT     1 AS Expr1
                            FROM          dbo.PatientIdentifier AS Previous
                            WHERE      (PATNT_REFNO = DistrictNo.PATNT_REFNO) AND (PITYP_REFNO = DistrictNo.PITYP_REFNO) AND (IDENTIFIER LIKE 'RM2%') AND (ARCHV_FLAG = 'N') 
                                                   AND (START_DTTM > DistrictNo.START_DTTM) OR
                                                   (PATNT_REFNO = DistrictNo.PATNT_REFNO) AND (PITYP_REFNO = DistrictNo.PITYP_REFNO) AND (IDENTIFIER LIKE 'RM2%') AND (ARCHV_FLAG = 'N') 
                                                   AND (START_DTTM = DistrictNo.START_DTTM) AND (PATID_REFNO > DistrictNo.PATID_REFNO)) 
	
	LEFT OUTER JOIN dbo.PatientAddressRole 
		ON dbo.PatientAddressRole.PATNT_REFNO = dbo.Patient.PATNT_REFNO 
		AND dbo.PatientAddressRole.ROTYP_CODE = 'HOME' 
		AND dbo.PatientAddressRole.ARCHV_FLAG = 'N' 
		AND EXISTS
                          (SELECT     1 AS Expr1
                            FROM          dbo.PatientAddress
                            WHERE      (ADDSS_REFNO = dbo.PatientAddressRole.ADDSS_REFNO) AND (ADTYP_CODE = 'POSTL') AND (ARCHV_FLAG = 'N')) 
		AND Outpatient.START_DTTM BETWEEN dbo.PatientAddressRole.START_DTTM 
		AND COALESCE (dbo.PatientAddressRole.END_DTTM, Outpatient.START_DTTM) 
		AND NOT EXISTS
                          (SELECT     1 AS Expr1
                            FROM          dbo.PatientAddressRole AS Previous
                            WHERE      (PATNT_REFNO = dbo.PatientAddressRole.PATNT_REFNO) AND (ROTYP_CODE = dbo.PatientAddressRole.ROTYP_CODE) AND (ARCHV_FLAG = 'N') AND 
                                                   EXISTS
                                                       (SELECT     1 AS Expr1
                                                         FROM          dbo.PatientAddress
                                                         WHERE      (ADDSS_REFNO = Previous.ADDSS_REFNO) AND (ADTYP_CODE = 'POSTL') AND (ARCHV_FLAG = 'N')) AND 
                                                   (Outpatient.START_DTTM BETWEEN START_DTTM AND COALESCE (END_DTTM, Outpatient.START_DTTM)) AND 
                                                   (START_DTTM > dbo.PatientAddressRole.START_DTTM) OR
                                                   (PATNT_REFNO = dbo.PatientAddressRole.PATNT_REFNO) AND (ROTYP_CODE = dbo.PatientAddressRole.ROTYP_CODE) AND (ARCHV_FLAG = 'N') AND 
                                                   (Outpatient.START_DTTM BETWEEN START_DTTM AND COALESCE (END_DTTM, Outpatient.START_DTTM)) AND 
                                                   (START_DTTM = dbo.PatientAddressRole.START_DTTM) AND EXISTS
                                                       (SELECT     1 AS Expr1
                                                         FROM          dbo.PatientAddress
                                                         WHERE      (ADDSS_REFNO = Previous.ADDSS_REFNO) AND (ADTYP_CODE = 'POSTL') AND (ARCHV_FLAG = 'N')) AND 
                                                   (ROLES_REFNO > dbo.PatientAddressRole.ROLES_REFNO)) 
	
	LEFT OUTER JOIN dbo.PatientAddress 
		ON dbo.PatientAddress.ADDSS_REFNO = dbo.PatientAddressRole.ADDSS_REFNO 
		AND dbo.PatientAddress.ARCHV_FLAG = 'N' 
		      
	LEFT OUTER JOIN dbo.PatientProfessionalCarer AS DefaultRegisteredGp 
		ON DefaultRegisteredGp.PATNT_REFNO = Outpatient.PATNT_REFNO 
		AND DefaultRegisteredGp.PRTYP_MAIN_CODE = 'GMPRC' 
		AND DefaultRegisteredGp.ARCHV_FLAG = 'N' 
		AND Outpatient.START_DTTM < DefaultRegisteredGp.START_DTTM 
		AND NOT EXISTS
                          (SELECT     1 AS Expr1
                            FROM          dbo.PatientProfessionalCarer AS Previous
                            WHERE      (PATNT_REFNO = DefaultRegisteredGp.PATNT_REFNO) AND (PRTYP_MAIN_CODE = DefaultRegisteredGp.PRTYP_MAIN_CODE) AND 
                                                   (ARCHV_FLAG = DefaultRegisteredGp.ARCHV_FLAG) AND (Outpatient.START_DTTM < START_DTTM) AND 
                                                   (START_DTTM > DefaultRegisteredGp.START_DTTM) OR
                                                   (PATNT_REFNO = DefaultRegisteredGp.PATNT_REFNO) AND (PRTYP_MAIN_CODE = DefaultRegisteredGp.PRTYP_MAIN_CODE) AND 
                                                   (ARCHV_FLAG = DefaultRegisteredGp.ARCHV_FLAG) AND (Outpatient.START_DTTM < START_DTTM) AND 
                                                   (START_DTTM = DefaultRegisteredGp.START_DTTM) AND (PATPC_REFNO > DefaultRegisteredGp.PATPC_REFNO)) 
	
	LEFT OUTER JOIN dbo.PatientProfessionalCarer AS RegisteredGp 
		ON RegisteredGp.PATNT_REFNO = Outpatient.PATNT_REFNO 
		AND RegisteredGp.PRTYP_MAIN_CODE = 'GMPRC' 
		AND RegisteredGp.ARCHV_FLAG = 'N' 
		AND Outpatient.START_DTTM BETWEEN RegisteredGp.START_DTTM 
		AND COALESCE (RegisteredGp.END_DTTM, Outpatient.START_DTTM) 
		AND NOT EXISTS
                          (SELECT     1 AS Expr1
                            FROM          dbo.PatientProfessionalCarer AS Previous
                            WHERE      (PATNT_REFNO = RegisteredGp.PATNT_REFNO) AND (PRTYP_MAIN_CODE = RegisteredGp.PRTYP_MAIN_CODE) AND (ARCHV_FLAG = 'N') AND 
                                                   (Outpatient.START_DTTM BETWEEN START_DTTM AND COALESCE (END_DTTM, Outpatient.START_DTTM)) AND 
                                                   (START_DTTM > RegisteredGp.START_DTTM) OR
                                                   (PATNT_REFNO = RegisteredGp.PATNT_REFNO) AND (PRTYP_MAIN_CODE = RegisteredGp.PRTYP_MAIN_CODE) AND (ARCHV_FLAG = 'N') AND 
                                                   (Outpatient.START_DTTM BETWEEN START_DTTM AND COALESCE (END_DTTM, Outpatient.START_DTTM)) AND 
                                                   (START_DTTM = RegisteredGp.START_DTTM) AND (PATPC_REFNO > RegisteredGp.PATPC_REFNO)) 
	
	LEFT OUTER JOIN dbo.Contract 
		ON dbo.Contract.CONTR_REFNO = Outpatient.CONTR_REFNO 
		AND dbo.Contract.ARCHV_FLAG = 'N' 
	
	LEFT OUTER JOIN dbo.[User] AS CreatedUser 
		ON CreatedUser.USERS_REFNO = Outpatient.USER_REFNO 
		AND CreatedUser.ARCHV_FLAG = 'N' 
		
	LEFT OUTER JOIN dbo.[User] AS UpdatedUser 
		ON UpdatedUser.USERS_REFNO = Outpatient.USER_REFNO 
		AND UpdatedUser.ARCHV_FLAG = 'N' 
		
	LEFT OUTER JOIN dbo.ServicePoint AS Clinic 
		ON Clinic.SPONT_REFNO = Outpatient.SPONT_REFNO 
		AND Clinic.ARCHV_FLAG = 'N' 
		
	LEFT OUTER JOIN dbo.ProfessionalCarer AS SpontProfCarer 
		ON Clinic.PROCA_REFNO = SpontProfCarer.PROCA_REFNO 
		AND SpontProfCarer.ARCHV_FLAG = 'N' 
		
	LEFT OUTER JOIN dbo.OverseasVisitorStatus AS OverseasVisitorStatus 
		ON OverseasVisitorStatus.PATNT_REFNO = Outpatient.PATNT_REFNO 
		AND OverseasVisitorStatus.ARCHV_FLAG = 'N' 
		AND Outpatient.START_DTTM BETWEEN OverseasVisitorStatus.START_DTTM 
		AND COALESCE (OverseasVisitorStatus.END_DTTM, Outpatient.START_DTTM) 
		AND NOT EXISTS
                          (SELECT     1 AS Expr1
                            FROM          dbo.OverseasVisitorStatus AS Previous
                            WHERE      (PATNT_REFNO = OverseasVisitorStatus.PATNT_REFNO) AND (ARCHV_FLAG = 'N') AND (Outpatient.START_DTTM BETWEEN START_DTTM AND 
                                                   COALESCE (END_DTTM, Outpatient.START_DTTM)) AND (START_DTTM > OverseasVisitorStatus.START_DTTM) OR
                                                   (PATNT_REFNO = OverseasVisitorStatus.PATNT_REFNO) AND (ARCHV_FLAG = 'N') AND (Outpatient.START_DTTM BETWEEN START_DTTM AND 
                                                   COALESCE (END_DTTM, Outpatient.START_DTTM)) AND (START_DTTM = OverseasVisitorStatus.START_DTTM) AND 
                                                   (OVSEA_REFNO > OverseasVisitorStatus.OVSEA_REFNO)) 
						   
	--LEFT OUTER JOIN dbo.WaitingList 
	--	ON dbo.WaitingList.WLIST_REFNO = Outpatient.WLIST_REFNO 
	--	AND dbo.WaitingList.ARCHV_FLAG = 'N' 
		
	--LEFT OUTER JOIN dbo.ScheduleHistory AS ScheduleHistoryQM08StartDate 
	--	ON ScheduleHistoryQM08StartDate.SCHDL_REFNO = Outpatient.SCHDL_REFNO 
	--	AND ScheduleHistoryQM08StartDate.MOVRN_REFNO IN (2003544, 3248, 2004148, 2003541, 2003540, 383, 2003539, 4520) 
	--	AND ScheduleHistoryQM08StartDate.ARCHV_FLAG = 'N' 
	--	AND ScheduleHistoryQM08StartDate.NEW_START_DTTM > ScheduleHistoryQM08StartDate.OLD_START_DTTM 
	--	AND ScheduleHistoryQM08StartDate.WLIST_REFNO IS NOT NULL 
	--	AND ScheduleHistoryQM08StartDate.OLD_START_DTTM < Outpatient.START_DTTM 
	--	AND NOT EXISTS
 --                         (SELECT     1 AS Expr1
 --                           FROM          dbo.ScheduleHistory AS Previous
 --                           WHERE      (SCHDL_REFNO = Outpatient.SCHDL_REFNO) AND (MOVRN_REFNO IN (2003544, 3248, 2004148, 2003541, 2003540, 383, 2003539, 4520)) AND 
 --                                                  (ARCHV_FLAG = 'N') AND (NEW_START_DTTM > OLD_START_DTTM) AND (WLIST_REFNO IS NOT NULL) AND 
 --                                                  (OLD_START_DTTM < Outpatient.START_DTTM) AND (OLD_START_DTTM > ScheduleHistoryQM08StartDate.OLD_START_DTTM) OR
 --                                                  (SCHDL_REFNO = Outpatient.SCHDL_REFNO) AND (MOVRN_REFNO IN (2003544, 3248, 2004148, 2003541, 2003540, 383, 2003539, 4520)) AND 
 --                                                  (ARCHV_FLAG = 'N') AND (NEW_START_DTTM > OLD_START_DTTM) AND (WLIST_REFNO IS NOT NULL) AND 
 --                                                  (OLD_START_DTTM < Outpatient.START_DTTM) AND (OLD_START_DTTM = ScheduleHistoryQM08StartDate.OLD_START_DTTM) AND 
 --                                                  (SCDHS_REFNO > ScheduleHistoryQM08StartDate.SCDHS_REFNO)) 
						   
	--LEFT OUTER JOIN dbo.ScheduleEvent AS ScheduleEventQM08StartDate 
	--	ON ScheduleEventQM08StartDate.REFRL_REFNO = Outpatient.REFRL_REFNO 
	--	AND ScheduleEventQM08StartDate.ATTND_REFNO IN (2868, 2870, 358, 2003495, 2004301, 2003494, 2000724) 
	--	AND ScheduleEventQM08StartDate.ARCHV_FLAG = 'N' 
	--	AND ScheduleEventQM08StartDate.WLIST_REFNO IS NOT NULL 
	--	AND ScheduleEventQM08StartDate.START_DTTM < Outpatient.START_DTTM 
	--	AND ScheduleEventQM08StartDate.REQST_DTTM IS NOT NULL 
	--	AND NOT EXISTS
 --                         (SELECT     1 AS Expr1
 --                           FROM          dbo.ScheduleEvent AS Previous
 --                           WHERE      (REFRL_REFNO = Outpatient.REFRL_REFNO) AND (ATTND_REFNO IN (2868, 2870, 358, 2003495, 2004301, 2003494, 2000724)) AND (ARCHV_FLAG = 'N') 
 --                                                  AND (WLIST_REFNO IS NOT NULL) AND (START_DTTM < Outpatient.START_DTTM) AND 
 --                                                  (REQST_DTTM > ScheduleEventQM08StartDate.REQST_DTTM) OR
 --                                                  (REFRL_REFNO = Outpatient.REFRL_REFNO) AND (ATTND_REFNO IN (2868, 2870, 358, 2003495, 2004301, 2003494, 2000724)) AND (ARCHV_FLAG = 'N') 
 --                                                  AND (WLIST_REFNO IS NOT NULL) AND (START_DTTM < Outpatient.START_DTTM) AND (REQST_DTTM = ScheduleEventQM08StartDate.REQST_DTTM) 
 --                                                  AND (SCHDL_REFNO > ScheduleEventQM08StartDate.SCHDL_REFNO)) 
						   
	LEFT OUTER JOIN dbo.RTT AS PeriodStatus 
		ON PeriodStatus.SORCE = 'SCHDL' 
		AND PeriodStatus.SORCE_REFNO = Outpatient.SCHDL_REFNO 
		AND PeriodStatus.ARCHV_FLAG = 'N' 
		AND NOT EXISTS
                          (SELECT     1 AS Expr1
                            FROM          dbo.RTT AS Previous
                            WHERE      (SORCE = 'SCHDL') AND (SORCE_REFNO = Outpatient.SCHDL_REFNO) AND (ARCHV_FLAG = 'N') AND (RTTST_DATE > PeriodStatus.RTTST_DATE) OR
                                                   (SORCE = 'SCHDL') AND (SORCE_REFNO = Outpatient.SCHDL_REFNO) AND (ARCHV_FLAG = 'N') AND (RTTST_DATE = PeriodStatus.RTTST_DATE) AND 
                                                   (RTTPR_REFNO > PeriodStatus.RTTPR_REFNO)) 
						   
	LEFT OUTER JOIN dbo.RTT 
		ON dbo.RTT.REFRL_REFNO = Outpatient.REFRL_REFNO 
		AND dbo.RTT.ARCHV_FLAG = 'N' 
		AND NOT EXISTS
                          (SELECT     1 AS Expr1
                            FROM          dbo.RTT AS Previous
                            WHERE      (REFRL_REFNO = Outpatient.REFRL_REFNO) AND (ARCHV_FLAG = 'N') AND (RTTST_DATE > dbo.RTT.RTTST_DATE) OR
                                                   (REFRL_REFNO = Outpatient.REFRL_REFNO) AND (ARCHV_FLAG = 'N') AND (RTTST_DATE = dbo.RTT.RTTST_DATE) AND 
                                                   (RTTPR_REFNO > dbo.RTT.RTTPR_REFNO))
WHERE     (Outpatient.SCTYP_REFNO IN (1470, 1472))
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -876
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Outpatient"
            Begin Extent = 
               Top = 28
               Left = 526
               Bottom = 136
               Right = 752
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Patient"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 305
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Referral"
            Begin Extent = 
               Top = 222
               Left = 38
               Bottom = 330
               Right = 270
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DistrictNo"
            Begin Extent = 
               Top = 330
               Left = 38
               Bottom = 438
               Right = 237
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PatientAddressRole"
            Begin Extent = 
               Top = 330
               Left = 275
               Bottom = 438
               Right = 474
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PatientAddress"
            Begin Extent = 
               Top = 438
               Left = 38
               Bottom = 546
               Right = 238
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DefaultRegisteredGp"
            Begin Extent = 
               Top = 438
               Left = 276
               Bottom = 546
               Right = 477
      ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ExtractOP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'      End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "RegisteredGp"
            Begin Extent = 
               Top = 546
               Left = 38
               Bottom = 654
               Right = 239
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Contract"
            Begin Extent = 
               Top = 654
               Left = 38
               Bottom = 762
               Right = 305
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CreatedUser"
            Begin Extent = 
               Top = 762
               Left = 38
               Bottom = 870
               Right = 253
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "UpdatedUser"
            Begin Extent = 
               Top = 870
               Left = 38
               Bottom = 978
               Right = 253
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Clinic"
            Begin Extent = 
               Top = 978
               Left = 38
               Bottom = 1086
               Right = 305
            End
            DisplayFlags = 280
            TopColumn = 16
         End
         Begin Table = "SpontProfCarer"
            Begin Extent = 
               Top = 1086
               Left = 38
               Bottom = 1194
               Right = 305
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "OverseasVisitorStatus"
            Begin Extent = 
               Top = 1194
               Left = 38
               Bottom = 1302
               Right = 279
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WaitingList"
            Begin Extent = 
               Top = 1302
               Left = 38
               Bottom = 1410
               Right = 265
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ScheduleHistoryQM08StartDate"
            Begin Extent = 
               Top = 546
               Left = 277
               Bottom = 654
               Right = 476
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ScheduleEventQM08StartDate"
            Begin Extent = 
               Top = 1410
               Left = 38
               Bottom = 1518
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PeriodStatus"
            Begin Extent = 
               Top = 1518
               Left = 38
               Bottom = 1626
               Right = 237
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "RTT"
            Begin Extent = 
               Top = 1518
               Left = 275
               Bottom = 1626
               Right = 474
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ExtractOP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane3', @value=N'1
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ExtractOP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=3 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ExtractOP'