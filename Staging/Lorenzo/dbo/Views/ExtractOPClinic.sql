﻿Create View [dbo].[ExtractOPClinic] AS
select 
	 ServicePointUniqueID = SPONT_REFNO
	,ClinicCode = CODE
	,ClinicName = NAME
	,ClinicDescription = DESCRIPTION
	,ClinicLocation = HEORG_REFNO
	,ClinicEffectiveFromDate = Cast(START_DTTM as DATE)
	,ClinicEffectiveToDate = Cast(END_DTTM as Date)
	,ClinicSpecialtyCode = SPECT_REFNO
	,ClinicProfessionalCarerCode = PROCA_REFNO
	,ClinicPurposeCode = PURPS_REFNO
	,Horizon = HORIZ_VALUE
	,PartialBookingFlag = PBK_FLAG
	,PartialBookingLeadTime = 
			Case WHen PBK_FLAG = 'N' THen
				'N/A'
			Else
				Cast(PBK_LEAD_TIME as varchar) + '-' + PBK_LEAD_TIME_UNITS
			End

	,ArchiveFlag =
		CONVERT(
				Bit,
					Case When ARCHV_FLAG = 'N' Then
						0
					Else
						1
					End
				)
	
	,PASCreated = CREATE_DTTM
	,PASUpdated = MODIF_DTTM
	,PASCreatedByWhom = USER_CREATE
	,PASUpdatedByWhom = USER_MODIF
from ServicePoint
Where SPTYP_REFNO = 1514