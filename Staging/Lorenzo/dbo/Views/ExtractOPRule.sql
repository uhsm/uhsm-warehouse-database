﻿CREATE View [dbo].[ExtractOPRule] AS

Select  
	 RuleUniqueID = DepRules.DPRUL_REFNO
	,AppliedTo = DepRules.SORCE_CODE
	,AppliedToUniqueID = DepRules.SORCE_REFNO
	,EnforcedFlag = DepRules.ENFORCE_FLAG
	,RuleAppliedUniqueID = DepRules.RULES_REFNO
	,RuleValueCode = DepRules.VALUE
	,ArchiveFlag = 
			CONVERT(
					Bit,
						Case When DepRules.ARCHV_FLAG = 'N' THen
							0
						Else
							1
						End
					)
	,PASCreated = DepRules.CREATE_DTTM
	,PASUpdated = DepRules.MODIF_DTTM
	,PASCreatedByWhom = DepRules.USER_CREATE
	,PASUpdatedByWhom = DepRules.USER_MODIF
/*
	,Rules.DESCRIPTION
	,RuleValue.MAIN_CODE
	,RuleValue.DESCRIPTION
*/
	,DepRules.Created

from dbo.ContractRule DepRules
/*
left outer join dbo.[Rule] Rules
	on DepRules.RULES_REFNO = Rules.RULES_REFNO
left outer join dbo.ReferenceValue RuleValue
	on DepRules.[VALUE] = RuleValue.RFVAL_REFNO
*/
where 
DepRules.SORCE_CODE = 'SPSLT' or 
DepRules.SORCE_CODE = 'SPSSN' or 
DepRules.SORCE_CODE = 'SPONT'