﻿CREATE View [dbo].[ExtractOPSession] AS
select 
	 SessionUniqueID = SPSSN_REFNO
	,ServicePointUniqueID = SPONT_REFNO
	,SessionCode = CODE
	,SessionName = DESCRIPTION
	,SessionDate = Cast(START_DTTM as DATE)
	,SessionStartTime = CAST(Start_dttm as Time(0))
	,SessionStartDateTime = Cast(START_DTTM as smallDatetime)
	,SessionEndTime = Cast(END_DTTM as Time(0))
	,SessionEndDateTime = CAst(END_DTTM as SmallDatetime)
	,SessionDuration = SESSION_DURATION
	,SlotDuration = SLOT_DURATION
	,SessionStatusCode = SSTAT_REFNO
	,SessionStatusChangeReasonCode = SSCHR_REFNO
	,SessionStatusChangeRequestByCode = STCRB_REFNO
	,SessionSpecialtyCode = SPECT_REFNO
	,SessionProfessionalCarerCode = PROCA_REFNO
	,ActualSessionProfessionalCarerCode = ACTUAL_PROCA_REFNO
	,SessionStartStatusCode = SSSTS_REFNO 
	,SessionStartStatusReasonCode = SSSRS_REFNO
	,SessionEndStatusCode = SSENS_REFNO
	,SessionEndStatusReasonCode = SSERS_REFNO
	,VariableSlotsFlag = VAR_SLOTS_FLAG
	,AllowOverbookedSlots = USE_OVERBOOK_FLAG
	,PartialBookingFlag = PBK_FLAG
	,PartialBookingLeadTime = 
			Case WHen PBK_FLAG = 'N' THen
				'N/A'
			Else
				Cast(PBK_LEAD_TIME as varchar) + '-' + PBK_LEAD_TIME_UNITS
			End
	,EBSPublishedFlag = 
		CONVERT
			(
			Bit,
				Case when EBS_FLAG = 'Y' then
					1
				Else
					0
				End
			)
	,EBSPublishAllSlots = 
		CONVERT
			(
			Bit,
				Case when PUB_ALL_SLOTS = 'Y' then
					1
				Else
					0
				End
			)
	,EBSServiceID = EBS_SER_ID
	,NewVisits = NEW_VISITS
	,SessionTemplateUnqiueID = TMPLT_REFNO
	,SessionCancelledFlag =
		CONVERT(
				Bit,
					Case When CANCEL_DTTM IS NULL then	
						0
					Else
						1
					End
				)
	,SessionCancelledDate = Cast(CANCEL_DTTM as SmallDatetime)
	,SessionInstructions = INSTRUCTIONS
	,SessionComments = COMMENTS
	,IsTemplate = 	
		CONVERT(
				Bit,
					Case When TEMPLATE_FLAG = 'Y' then	
						1
					Else
						0
					End
				)
				
	,FrequencyCode = FREQN_REFNO
	,FrequencyOffsetCode = FREOF_REFNO
	,SessionFrequencyCode = SFRQN_REFNO
	,SessionFrequencyOffsetCode = SFROF_REFNO	
	,SessionWeeks = ON_WEEKS
	,SessionDays = ON_DAYS
	,DaysInWeek1 = DAYS_IN_WEEK1
	,DaysInWeek2 = DAYS_IN_WEEK2
	,DaysInWeek3 = DAYS_IN_WEEK3
	,DaysInWeek4 = DAYS_IN_WEEK4
	,DaysInWeek5 = DAYS_IN_WEEK5
	,DaysInWeekLast = DAYS_IN_WEEKL
	,ArchiveFlag =
		CONVERT(
				Bit,
					Case When ARCHV_FLAG = 'N' Then
						0
					Else
						1
					End
				)
	
	,PASCreated = CREATE_DTTM
	,PASUpdated = MODIF_DTTM
	,PASCreatedByWhom = USER_CREATE
	,PASUpdatedByWhom = USER_MODIF

	,Created

from ServicePointSession