﻿CREATE View [dbo].[ExtractOPSlot] AS
select 
	 SlotUniqueID = Slot.SPSLT_REFNO
	,SessionUniqueID = Slot.SPSSN_REFNO
	,ServicePointUniqueID = Slot.SPONT_REFNO
	,SlotDate = Cast(Slot.START_DTTM as DATE)
	,SlotStartTime = CAST(Slot.START_DTTM as Time(0))
	,SlotStartDateTime = Cast(Slot.START_DTTM as smallDatetime)
	,SlotEndTime = Cast(Slot.END_DTTM as Time(0))
	,SlotEndDateTime = CAst(Slot.END_DTTM as SmallDatetime)
	,SlotDuration = Slot.DURATION
	,SlotStatusCode = Slot.TSTAT_REFNO
	,SlotChangeReasonCode = Slot.SLCHR_REFNO
	,SlotChangeRequestByCode = Slot.STCRB_REFNO
	,SlotPublishedToEBS = Slot.EBS_PUB_FLAG
	,SlotMaxBookings = Slot.MAX_BOOKINGS
	,SlotNumberofBookings = Slot.NOOF_BOOKINGS
	,SlotTemplateUnqiueID = TMPLT_REFNO
	,IsTemplate = TEMPLATE_FLAG	
	,SlotCancelledFlag =
		CONVERT(
				Bit,
					Case When Slot.CANCEL_DTTM IS NULL then	
						0
					Else
						1
					End
				)
	,SlotCancelledDate = Slot.CANCEL_DTTM
	,SlotInstructions = Slot.INSTRUCTIONS
	,ArchiveFlag =
		CONVERT(
				Bit,
					Case When ARCHV_FLAG = 'N' Then
						0
					Else
						1
					End
				)
	,PASCreated = Slot.CREATE_DTTM
	,PASUpdated = Slot.MODIF_DTTM
	,PASCreatedByWhom = Slot.USER_CREATE
	,PASUpdatedByWhom = Slot.USER_MODIF

	,Slot.Created

from
	ServicePointSlot Slot