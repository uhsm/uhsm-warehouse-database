﻿CREATE view [dbo].[ExtractOPWL] as

/****************************************************************************************

	2011-01-17 KD
		Added WaitingListRule (WLRUL_REFNO)
		So that PartialBooking Outpatient Follow Up Waiting Lists can
		be identified.
		
****************************************************************************************/

select
	 SourceUniqueID = WaitingList.WLIST_REFNO
	,SourceUniqueIDTypeCode = 'WL'
	,SourcePatientNo = WaitingList.PATNT_REFNO
	,ReferralSourceUniqueID = WaitingList.REFRL_REFNO
	,AppointmentSourceUniqueID = Appointment.SCHDL_REFNO
	,PatientTitle = Patient.TITLE_REFNO_DESCRIPTION
	,PatientForename = Patient.FORENAME
	,PatientSurname = Patient.SURNAME
	,DateOfBirth = Patient.DTTM_OF_BIRTH
	,DateOfDeath = Patient.DTTM_OF_DEATH
	,SexCode = Patient.SEXXX_REFNO
	,NHSNumber = Patient.PATNT_REFNO_NHS_IDENTIFIER
	,DistrictNo = DistrictNo.IDENTIFIER
	,Postcode = PatientAddress.PCODE
	,PatientAddress1 = PatientAddress.LINE1
	,PatientAddress2 = PatientAddress.LINE2
	,PatientAddress3 = PatientAddress.LINE3
	,PatientAddress4 = PatientAddress.LINE4
	,DHACode = PatientAddress.HDIST_CODE
	,HomePhone = PatientHomeTelephone.LINE1
	,WorkPhone = PatientWorkTelephone.LINE1
	,EthnicOriginCode = Patient.ETHGR_REFNO
	,MaritalStatusCode = Patient.MARRY_REFNO
	,ReligionCode = Patient.RELIG_REFNO
	,ConsultantCode = WaitingList.PROCA_REFNO
	,SpecialtyCode = WaitingList.SPECT_REFNO
	,PASSpecialtyCode = WaitingList.SPECT_REFNO
	,PriorityCode = Referral.PRITY_REFNO
	,WaitingListCode = WaitingList.WAITING_LIST_NUMBER
	,CommentClinical = null
	,CommentNonClinical = null
	,SiteCode = WaitingList.SPONT_REFNO
	,PurchaserCode = WaitingList.PURCH_REFNO
	,ProviderCode = WaitingList.PROVD_REFNO
	,ContractSerialNo = Contract.SERIAL_NUMBER
	,AdminCategoryCode = WaitingList.ADCAT_REFNO

	,CancelledBy =
		case
		when Appointment.CANCR_DTTM is null
		then null
		else
			case
			when Appointment.ATTND_REFNO in
				(
				 2870		--Cancelled By Patient
				,358		--Did Not Attend
				,2003495	--Refused To Wait
				,2004301	--Cancelled by Patient
				,2003494	--Patient Left / Not seen
				,2000724	--Patient Late / Not Seen
				)
			then 'P'
			else 'H'
			end
		end

	,DoctorCode = Clinic.PROCA_REFNO


	,BookingTypeCode = WaitingList.BKTYP_REFNO
	,CasenoteNumber = null
	,InterfaceCode = 'LOR'

	,RegisteredGpCode = 
		coalesce(
			 RegisteredGp.PROCA_REFNO
			,DefaultRegisteredGp.PROCA_REFNO
		)

	,RegisteredGpPracticeCode =
		coalesce(
			 RegisteredGp.HEORG_REFNO
			,DefaultRegisteredGp.HEORG_REFNO
		)

	,EpisodicGpCode = 
		coalesce(
			 RegisteredGp.PROCA_REFNO
			,DefaultRegisteredGp.PROCA_REFNO
		)

	,EpisodicGpPracticeCode =
		coalesce(
			 RegisteredGp.HEORG_REFNO
			,DefaultRegisteredGp.HEORG_REFNO
		)

	,SourceTreatmentFunctionCode = WaitingList.SPECT_REFNO
	,TreatmentFunctionCode = WaitingList.SPECT_REFNO
	,NationalSpecialtyCode = null
	,PCTCode = null

	,ReferralDate = Referral.RECVD_DTTM
	,BookedDate = dateadd(day, datediff(day, 0, WaitingList.CREATE_DTTM), 0)
	,BookedTime = WaitingList.CREATE_DTTM
	,AppointmentDate = dateadd(day, datediff(day, 0, Appointment.REQST_DTTM), 0)
	,AppointmentTypeCode = Appointment.SCTYP_REFNO
	,AppointmentStatusCode = Appointment.ATTND_REFNO
	,AppointmentCategoryCode = null

	,QM08StartWaitDate =
		coalesce(
			case
			when ScheduleHistoryQM08StartDate.OLD_START_DTTM is null
			then ScheduleEventQM08StartDate.REQST_DTTM

			when ScheduleEventQM08StartDate.REQST_DTTM is null
			then ScheduleHistoryQM08StartDate.OLD_START_DTTM

			when ScheduleHistoryQM08StartDate.OLD_START_DTTM > ScheduleEventQM08StartDate.REQST_DTTM
			then ScheduleHistoryQM08StartDate.OLD_START_DTTM

			else ScheduleEventQM08StartDate.REQST_DTTM
			end
			,WaitingList.WLIST_DTTM
		)

	,QM08EndWaitDate = WaitingList.REMVL_DTTM

	,AppointmentTime = Appointment.REQST_DTTM
	,ClinicCode = Clinic.CODE
	,SourceOfReferralCode = Referral.SORRF_REFNO

	,MRSAFlag =
		convert(
			 bit
			,case
			when exists
				(
				select
					1
				from
					dbo.ClinicalCoding
				where
					ClinicalCoding.PATNT_REFNO = WaitingList.PATNT_REFNO
				and	ClinicalCoding.START_DTTM <= Census.DateValue
				and	ClinicalCoding.CODE in ('009', 'INFC006')
				and	ClinicalCoding.DPTYP_CODE = 'ALERT'
				and	ClinicalCoding.ARCHV_FLAG = 'N'
				)
			then 1
			else 0
			end
		)

	,RTTPathwayID = Referral.PATNT_PATHWAY_ID
	,RTTPathwayCondition = null
	,RTTStartDate = Referral.RTT_START_DATE
	,RTTEndDate = null
	,RTTSpecialtyCode = null
	,RTTCurrentProviderCode = null

	,RTTCurrentStatusCode = RTT.RTTST_REFNO
	,RTTCurrentStatusDate = RTT.RTTST_DATE

	,RTTCurrentPrivatePatientFlag = null
	,RTTOverseasStatusFlag = null

	,PASCreated = WaitingList.CREATE_DTTM
	,PASUpdated = WaitingList.MODIF_DTTM
	,PASCreatedByWhom = WaitingList.USER_CREATE
	,PASUpdatedByWhom = WaitingList.USER_MODIF

	,ArchiveFlag =
		case
		when WaitingList.ARCHV_FLAG = 'N'
		then 0
		else 1
		end

	,RemovalDate =
		WaitingList.REMVL_DTTM

	,FuturePatientCancelDate =
		FutureCancelledAppointment.REQST_DTTM

	,AdditionFlag =
		case
		when
			datediff(
				 day
				,WaitingList.CREATE_DTTM
				,Census.DateValue
			) < 8
		then 1
		else 0
		end

	,CountOfDNAs = DNA.DNAs
	,CountOfHospitalCancels = Cancellation.HospitalCancels
	,CountOfPatientCancels = Cancellation.PatientCancels

	,DateOnWaitingList = WaitingList.WLIST_DTTM
	,IntendedPrimaryOperationCode = left(WaitingList.PLANNED_PROC, 4)
	,Operation = WaitingList.PLANNED_PROC
	,WaitingListRule = WaitingList.WLRUL_REFNO

	,VisitCode = WaitingList.VISIT_REFNO
	,InviteDate = WaitingList.INVIT_DTTM
	,WaitingListClinicCode = WaitingList.SPONT_REFNO
	,GeneralComment = GeneralComment.NOTES_REFNO_NOTE
from
	dbo.WaitingList

inner join dbo.Parameter Census
on	Census.Parameter = 'OPWLCENSUSDATE'

left join dbo.ScheduleEvent Appointment
on	Appointment.WLIST_REFNO = WaitingList.WLIST_REFNO
and	Appointment.ARCHV_FLAG = 'N'
and	Appointment.REQST_DTTM is not null
and	not exists
	(
	select
		1
	from
		dbo.ScheduleEvent Previous
	where
		Previous.WLIST_REFNO = WaitingList.WLIST_REFNO
	and	Previous.ARCHV_FLAG = 'N'
	and	Previous.REQST_DTTM is not null
	and	(
			Previous.REQST_DTTM > Appointment.REQST_DTTM
		or	(
				Previous.REQST_DTTM = Appointment.REQST_DTTM
			and	Previous.SCHDL_REFNO > Appointment.SCHDL_REFNO
			)
		)
	)

left join dbo.ScheduleEvent FutureCancelledAppointment
on	FutureCancelledAppointment.WLIST_REFNO = WaitingList.WLIST_REFNO
and	FutureCancelledAppointment.ARCHV_FLAG = 'N'
and	FutureCancelledAppointment.REQST_DTTM is not null
and	FutureCancelledAppointment.MOVRN_REFNO in (
		 2003544	--Other - Patient cancellation
		,3248		--Other - Patient cancellation
		,2004148	--Patient is current Inpatient
		,2003541	--Patient - Other more pressing engagement
		,2003540	--Patient - On holiday
		,383		--Patient died
		,2003539	--Patient ill
		,4520		--Patient Cancelled
		)
and	not exists
	(
	select
		1
	from
		dbo.ScheduleEvent Previous
	where
		Previous.WLIST_REFNO = WaitingList.WLIST_REFNO
	and	Previous.ARCHV_FLAG = 'N'
	and	Previous.REQST_DTTM is not null
	and	Previous.MOVRN_REFNO in (
			 2003544	--Other - Patient cancellation
			,3248		--Other - Patient cancellation
			,2004148	--Patient is current Inpatient
			,2003541	--Patient - Other more pressing engagement
			,2003540	--Patient - On holiday
			,383		--Patient died
			,2003539	--Patient ill
			,4520		--Patient Cancelled
			)
	and	(
			Previous.REQST_DTTM > FutureCancelledAppointment.REQST_DTTM
		or	(
				Previous.REQST_DTTM = FutureCancelledAppointment.REQST_DTTM
			and	Previous.SCHDL_REFNO > FutureCancelledAppointment.SCHDL_REFNO
			)
		)
	)

left join dbo.ServicePoint Clinic
on	Clinic.SPONT_REFNO = Appointment.SPONT_REFNO

left join dbo.Patient
on	Patient.PATNT_REFNO = WaitingList.PATNT_REFNO

left join dbo.Referral
on	Referral.REFRL_REFNO = WaitingList.REFRL_REFNO

left join dbo.ScheduleHistory ScheduleHistoryQM08StartDate
on	ScheduleHistoryQM08StartDate.SCHDL_REFNO = Appointment.SCHDL_REFNO
and	ScheduleHistoryQM08StartDate.MOVRN_REFNO in (
		 2003544	--Other - Patient cancellation
		,3248		--Other - Patient cancellation
		,2004148	--Patient is current Inpatient
		,2003541	--Patient - Other more pressing engagement
		,2003540	--Patient - On holiday
		,383		--Patient died
		,2003539	--Patient ill
		,4520		--Patient Cancelled
		)
and	ScheduleHistoryQM08StartDate.ARCHV_FLAG = 'N'
and	ScheduleHistoryQM08StartDate.NEW_START_DTTM > ScheduleHistoryQM08StartDate.OLD_START_DTTM
and	ScheduleHistoryQM08StartDate.WLIST_REFNO is not null
and	ScheduleHistoryQM08StartDate.OLD_START_DTTM < Appointment.REQST_DTTM
and	not exists
	(
	select
		1
	from
		dbo.ScheduleHistory Previous
	where
		Previous.SCHDL_REFNO = Appointment.SCHDL_REFNO
	and	Previous.MOVRN_REFNO in (
			 2003544	--Other - Patient cancellation
			,3248		--Other - Patient cancellation
			,2004148	--Patient is current Inpatient
			,2003541	--Patient - Other more pressing engagement
			,2003540	--Patient - On holiday
			,383		--Patient died
			,2003539	--Patient ill
			,4520		--Patient Cancelled
			)
	and	Previous.ARCHV_FLAG = 'N'
	and	Previous.NEW_START_DTTM > Previous.OLD_START_DTTM
	and	Previous.WLIST_REFNO is not null
	and	Previous.OLD_START_DTTM < Appointment.REQST_DTTM
	and	(
			Previous.OLD_START_DTTM > ScheduleHistoryQM08StartDate.OLD_START_DTTM
		or	(
				Previous.OLD_START_DTTM = ScheduleHistoryQM08StartDate.OLD_START_DTTM
			and	Previous.SCDHS_REFNO > ScheduleHistoryQM08StartDate.SCDHS_REFNO
			)
		)
	)

left join dbo.ScheduleEvent ScheduleEventQM08StartDate
on	ScheduleEventQM08StartDate.REFRL_REFNO = Appointment.REFRL_REFNO
and	ScheduleEventQM08StartDate.ATTND_REFNO in (
		 2868		--Patient Late / Seen
		,2870		--Cancelled By Patient
		,358		--Did Not Attend
		,2003495	--Refused To Wait
		,2004301	--Cancelled by Patient
		,2003494	--Patient Left / Not seen
		,2000724	--Patient Late / Not Seen
		)
and	ScheduleEventQM08StartDate.ARCHV_FLAG = 'N'
and	ScheduleEventQM08StartDate.WLIST_REFNO is not null
and	ScheduleEventQM08StartDate.REQST_DTTM < Appointment.REQST_DTTM
and	not exists
	(
	select
		1
	from
		dbo.ScheduleEvent Previous
	where
		Previous.REFRL_REFNO = Appointment.REFRL_REFNO
	and	Previous.ATTND_REFNO in (
		 2868		--Patient Late / Seen
		,2870		--Cancelled By Patient
		,358		--Did Not Attend
		,2003495	--Refused To Wait
		,2004301	--Cancelled by Patient
		,2003494	--Patient Left / Not seen
		,2000724	--Patient Late / Not Seen
			)
	and	Previous.ARCHV_FLAG = 'N'
	and	Previous.WLIST_REFNO is not null
	and	Previous.REQST_DTTM < Appointment.REQST_DTTM
	and	(
			Previous.REQST_DTTM > ScheduleEventQM08StartDate.REQST_DTTM
		or	(
				Previous.REQST_DTTM = ScheduleEventQM08StartDate.REQST_DTTM
			and	Previous.SCHDL_REFNO > ScheduleEventQM08StartDate.SCHDL_REFNO
			)
		)
	)

left join dbo.PatientIdentifier DistrictNo
on	DistrictNo.PATNT_REFNO = WaitingList.PATNT_REFNO
and	DistrictNo.PITYP_REFNO = 2001232 --facility
and	DistrictNo.IDENTIFIER like 'RM2%'
and	not exists
	(
	select
		1
	from
		dbo.PatientIdentifier Previous
	where
		Previous.PATNT_REFNO = DistrictNo.PATNT_REFNO
	and	Previous.PITYP_REFNO = DistrictNo.PITYP_REFNO
	and	Previous.IDENTIFIER like 'RM2%'
	and	(
			Previous.START_DTTM > DistrictNo.START_DTTM
		or
			(
				Previous.START_DTTM = DistrictNo.START_DTTM
			and	Previous.PATID_REFNO > DistrictNo.PATID_REFNO
			)
		)
	)

left join dbo.PatientAddressRole
on	PatientAddressRole.PATNT_REFNO = Patient.PATNT_REFNO
and	PatientAddressRole.ROTYP_CODE = 'HOME'
and	exists
	(
	select
		1
	from
		dbo.PatientAddress
	where
		PatientAddress.ADDSS_REFNO = PatientAddressRole.ADDSS_REFNO
	and	PatientAddress.ADTYP_CODE = 'POSTL'
	)
and	WaitingList.WLIST_DTTM between 
		PatientAddressRole.START_DTTM 
	and coalesce(
			PatientAddressRole.END_DTTM
			,WaitingList.WLIST_DTTM
		)
and	not exists
	(
	select
		1
	from
		dbo.PatientAddressRole Previous
	where
		Previous.PATNT_REFNO = PatientAddressRole.PATNT_REFNO
	and	Previous.ROTYP_CODE = PatientAddressRole.ROTYP_CODE
	and	exists
		(
		select
			1
		from
			dbo.PatientAddress
		where
			PatientAddress.ADDSS_REFNO = Previous.ADDSS_REFNO
		and	PatientAddress.ADTYP_CODE = 'POSTL'
		)
	and	WaitingList.WLIST_DTTM between 
			Previous.START_DTTM 
		and coalesce(
				Previous.END_DTTM
				,WaitingList.WLIST_DTTM
			)
	and	(
			Previous.START_DTTM > PatientAddressRole.START_DTTM
		or	(
				Previous.START_DTTM = PatientAddressRole.START_DTTM
			and	Previous.ROLES_REFNO > PatientAddressRole.ROLES_REFNO
			)
		)
	)

left join dbo.PatientAddress
on	PatientAddress.ADDSS_REFNO = PatientAddressRole.ADDSS_REFNO


left join dbo.PatientAddressRole PatientAddressRoleTelephone
on	PatientAddressRoleTelephone.PATNT_REFNO = Patient.PATNT_REFNO
and	PatientAddressRoleTelephone.ROTYP_CODE = 'HOME'
and	exists
	(
	select
		1
	from
		dbo.PatientAddress
	where
		PatientAddress.ADDSS_REFNO = PatientAddressRoleTelephone.ADDSS_REFNO
	and	PatientAddress.ADTYP_CODE = 'PHONE'
	)
and	WaitingList.WLIST_DTTM between 
		PatientAddressRoleTelephone.START_DTTM 
	and coalesce(
			PatientAddressRoleTelephone.END_DTTM
			,WaitingList.WLIST_DTTM
		)
and	not exists
	(
	select
		1
	from
		dbo.PatientAddressRole Previous
	where
		Previous.PATNT_REFNO = PatientAddressRoleTelephone.PATNT_REFNO
	and	Previous.ROTYP_CODE = PatientAddressRoleTelephone.ROTYP_CODE
	and	exists
		(
		select
			1
		from
			dbo.PatientAddress
		where
			PatientAddress.ADDSS_REFNO = Previous.ADDSS_REFNO
		and	PatientAddress.ADTYP_CODE = 'PHONE'
		)
	and	WaitingList.WLIST_DTTM between 
			Previous.START_DTTM 
		and coalesce(
				Previous.END_DTTM
				,WaitingList.WLIST_DTTM
			)
	and	(
			Previous.START_DTTM > PatientAddressRoleTelephone.START_DTTM
		or	(
				Previous.START_DTTM = PatientAddressRoleTelephone.START_DTTM
			and	Previous.ROLES_REFNO > PatientAddressRoleTelephone.ROLES_REFNO
			)
		)
	)

left join dbo.PatientAddress PatientHomeTelephone
on	PatientHomeTelephone.ADDSS_REFNO = PatientAddressRoleTelephone.ADDSS_REFNO

left join dbo.PatientAddressRole PatientAddressRoleWorkTelephone
on	PatientAddressRoleWorkTelephone.PATNT_REFNO = Patient.PATNT_REFNO
and	PatientAddressRoleWorkTelephone.ROTYP_CODE = 'WP' --Work Telephone Number
and	exists
	(
	select
		1
	from
		dbo.PatientAddress
	where
		PatientAddress.ADDSS_REFNO = PatientAddressRoleWorkTelephone.ADDSS_REFNO
	and	PatientAddress.ADTYP_CODE = 'PHONE'
	)
and	WaitingList.WLIST_DTTM between 
		PatientAddressRoleWorkTelephone.START_DTTM 
	and coalesce(
			PatientAddressRoleWorkTelephone.END_DTTM
			,WaitingList.WLIST_DTTM
		)
and	not exists
	(
	select
		1
	from
		dbo.PatientAddressRole Previous
	where
		Previous.PATNT_REFNO = PatientAddressRoleWorkTelephone.PATNT_REFNO
	and	Previous.ROTYP_CODE = PatientAddressRoleWorkTelephone.ROTYP_CODE
	and	exists
		(
		select
			1
		from
			dbo.PatientAddress
		where
			PatientAddress.ADDSS_REFNO = Previous.ADDSS_REFNO
		and	PatientAddress.ADTYP_CODE = 'PHONE'
		)
	and	WaitingList.WLIST_DTTM between 
			Previous.START_DTTM 
		and coalesce(
				Previous.END_DTTM
				,WaitingList.WLIST_DTTM
			)
	and	(
			Previous.START_DTTM > PatientAddressRoleWorkTelephone.START_DTTM
		or	(
				Previous.START_DTTM = PatientAddressRoleWorkTelephone.START_DTTM
			and	Previous.ROLES_REFNO > PatientAddressRoleWorkTelephone.ROLES_REFNO
			)
		)
	)

left join dbo.PatientAddress PatientWorkTelephone
on	PatientWorkTelephone.ADDSS_REFNO = PatientAddressRoleWorkTelephone.ADDSS_REFNO

left join dbo.Contract
on	Contract.CONTR_REFNO = WaitingList.CONTR_REFNO

left join dbo.PatientProfessionalCarer DefaultRegisteredGp
on	DefaultRegisteredGp.PATNT_REFNO = Referral.PATNT_REFNO
and	DefaultRegisteredGp.PRTYP_MAIN_CODE = 'GMPRC'
--and	DefaultRegisteredGp.ARCHV_FLAG = 'N'
and	WaitingList.WLIST_DTTM < DefaultRegisteredGp.START_DTTM
and	not exists
	(
	select
		1
	from
		dbo.PatientProfessionalCarer Previous
	where
		Previous.PATNT_REFNO = DefaultRegisteredGp.PATNT_REFNO
	and	Previous.PRTYP_MAIN_CODE = DefaultRegisteredGp.PRTYP_MAIN_CODE
--	and	Previous.ARCHV_FLAG = DefaultRegisteredGp.ARCHV_FLAG
	and	WaitingList.WLIST_DTTM < Previous.START_DTTM
	and	(
			Previous.START_DTTM > DefaultRegisteredGp.START_DTTM
		or	(
				Previous.START_DTTM = DefaultRegisteredGp.START_DTTM
			and	Previous.PATPC_REFNO > DefaultRegisteredGp.PATPC_REFNO
			)
		)
	)

left join dbo.PatientProfessionalCarer RegisteredGp
on	RegisteredGp.PATNT_REFNO = WaitingList.PATNT_REFNO
and	RegisteredGp.PRTYP_MAIN_CODE = 'GMPRC'
and	RegisteredGp.ARCHV_FLAG = 'N'
and	WaitingList.WLIST_DTTM between 
		RegisteredGp.START_DTTM 
	and coalesce(
			RegisteredGp.END_DTTM
			,WaitingList.WLIST_DTTM
		)
and	not exists
	(
	select
		1
	from
		dbo.PatientProfessionalCarer Previous
	where
		Previous.PATNT_REFNO = RegisteredGp.PATNT_REFNO
	and	Previous.PRTYP_MAIN_CODE = RegisteredGp.PRTYP_MAIN_CODE
	and	Previous.ARCHV_FLAG = RegisteredGp.ARCHV_FLAG
	and	WaitingList.WLIST_DTTM between 
			Previous.START_DTTM 
		and coalesce(
				Previous.END_DTTM
				,WaitingList.WLIST_DTTM
			)
	and	(
			Previous.START_DTTM > RegisteredGp.START_DTTM
		or	(
				Previous.START_DTTM = RegisteredGp.START_DTTM
			and	Previous.PATPC_REFNO > RegisteredGp.PATPC_REFNO
			)
		)
	)


left join
	(
	select
		 ScheduleEvent.WLIST_REFNO

		,DNAs = count(*)
	from
		dbo.ScheduleEvent ScheduleEvent
	where
		ScheduleEvent.ATTND_REFNO = 358	--Did Not Attend
	and	ScheduleEvent.ARCHV_FLAG = 'N'
	group by
		 ScheduleEvent.WLIST_REFNO
	) DNA
on	DNA.WLIST_REFNO = WaitingList.WLIST_REFNO


left join
	(
	select
		 ScheduleHistory.WLIST_REFNO

		,HospitalCancels =
			sum(
				case
				when ScheduleHistory.MOVRN_REFNO in (
					 3249		--Slot cancelled
					,2002068	--C&B Cancellation
					,2003537	--Clinic overrun
					,2003538	--Other - Provider cancellation
					,2003542	--Clashes with another appointment
					,2003543	--Appt. made with wrong provider
					,2004147	--GP Request
					,2004338	--C&B timed out
					,2005554	--Clinic cancelled
					,2006151	--Intend to go private
					,2006152	--Addional req unavailable for this appt
					,2006153	--Addional req not provided at location
					,2006154	--Not eligible for additional requirement
					,2006155	--Inappropriate priority
					,2006156	--Priority change for other reasons
					,2006157	--Treatment no longer required
					)
				then 1
				else 0
				end
			)

		,PatientCancels =
			sum(
				case
				when ScheduleHistory.MOVRN_REFNO in (
						 2003544	--Other - Patient cancellation
						,3248		--Other - Patient cancellation
						,2004148	--Patient is current Inpatient
						,2003541	--Patient - Other more pressing engagement
						,2003540	--Patient - On holiday
						,383		--Patient died
						,2003539	--Patient ill
						,4520		--Patient Cancelled
						)
				then 1
				else 0
				end
			)
	from
		dbo.ScheduleHistory ScheduleHistory
	where
		ScheduleHistory.ARCHV_FLAG = 'N'
	group by
		 ScheduleHistory.WLIST_REFNO
	) Cancellation
on	Cancellation.WLIST_REFNO = WaitingList.WLIST_REFNO

left join dbo.RTT
on	RTT.REFRL_REFNO = WaitingList.REFRL_REFNO
and	RTT.RTTST_DATE <= Census.DateValue
and	not exists
	(
	select
		1
	from
		dbo.RTT Previous
	where
		Previous.REFRL_REFNO = WaitingList.REFRL_REFNO
	and	Previous.RTTST_DATE <= Census.DateValue
	and	(
			Previous.RTTST_DATE > RTT.RTTST_DATE
		or	(
				Previous.RTTST_DATE = RTT.RTTST_DATE
			and	Previous.RTTPR_REFNO > RTT.RTTPR_REFNO
			)
		)
	)					

left join dbo.NoteRole GeneralComment
on	GeneralComment.SORCE_REFNO = WaitingList.WLIST_REFNO
and	GeneralComment.SORCE_CODE = 'WLCMT'
and	GeneralComment.ARCHV_FLAG = 'N'

where
	WaitingList.SVTYP_REFNO = 4223 --1579 = ip, 4223 = op
and	WaitingList.ARCHV_FLAG = 'N'
and	Referral.RECVD_DTTM <= Census.DateValue
and	(
		WaitingList.REMVL_DTTM > Census.DateValue
	or	WaitingList.REMVL_DTTM is null
	)

--remove test patients
and	not exists
	(
	select
		1
	from
		dbo.ExcludedPatient
	where
		ExcludedPatient.SourcePatientNo = WaitingList.PATNT_REFNO
	)


union all

--open referrals
select
	 SourceUniqueID = Referral.REFRL_REFNO
	,SourceUniqueIDTypeCode = 'RF'
	,SourcePatientNo = Referral.PATNT_REFNO
	,ReferralSourceUniqueID = Referral.REFRL_REFNO
	,AppointmentSourceUniqueID = null
	,PatientTitle = Patient.TITLE_REFNO_DESCRIPTION
	,PatientForename = Patient.FORENAME
	,PatientSurname = Patient.SURNAME
	,DateOfBirth = Patient.DTTM_OF_BIRTH
	,DateOfDeath = Patient.DTTM_OF_DEATH
	,SexCode = Patient.SEXXX_REFNO
	,NHSNumber = Patient.PATNT_REFNO_NHS_IDENTIFIER
	,DistrictNo = DistrictNo.IDENTIFIER
	,Postcode = PatientAddress.PCODE
	,PatientAddress1 = PatientAddress.LINE1
	,PatientAddress2 = PatientAddress.LINE2
	,PatientAddress3 = PatientAddress.LINE3
	,PatientAddress4 = PatientAddress.LINE4
	,DHACode = PatientAddress.HDIST_CODE
	,HomePhone = PatientHomeTelephone.LINE1
	,WorkPhone = PatientWorkTelephone.LINE1
	,EthnicOriginCode = Patient.ETHGR_REFNO
	,MaritalStatusCode = Patient.MARRY_REFNO
	,ReligionCode = Patient.RELIG_REFNO
	,ConsultantCode = Referral.REFTO_PROCA_REFNO
	,SpecialtyCode = Referral.REFTO_SPECT_REFNO
	,PASSpecialtyCode = Referral.REFTO_SPECT_REFNO
	,PriorityCode = Referral.PRITY_REFNO
	,WaitingListCode = null
	,CommentClinical = null
	,CommentNonClinical = null

	,SiteCode =
		coalesce(
			 Referral.REFTO_CLINIC_SPONT_REFNO
			,Referral.REFTO_WARD_SPONT_REFNO
		)

	,PurchaserCode = Referral.PURCH_REFNO --?
	,ProviderCode = Referral.PROVD_REFNO --?
	,ContractSerialNo = Contract.SERIAL_NUMBER
	,AdminCategoryCode = Referral.ADCAT_REFNO 
	,CancelledBy = null
	,DoctorCode = null
	,BookingTypeCode = null
	,CasenoteNumber = null
	,InterfaceCode = 'LOR'

	,RegisteredGpCode = 
		coalesce(
			 RegisteredGp.PROCA_REFNO
			,DefaultRegisteredGp.PROCA_REFNO
		)

	,RegisteredGpPracticeCode =
		coalesce(
			 RegisteredGp.HEORG_REFNO
			,DefaultRegisteredGp.HEORG_REFNO
		)

	,EpisodicGpCode = 
		coalesce(
			 RegisteredGp.PROCA_REFNO
			,DefaultRegisteredGp.PROCA_REFNO
		)

	,EpisodicGpPracticeCode =
		coalesce(
			 RegisteredGp.HEORG_REFNO
			,DefaultRegisteredGp.HEORG_REFNO
		)

	,SourceTreatmentFunctionCode = Referral.REFTO_SPECT_REFNO
	,TreatmentFunctionCode = Referral.REFTO_SPECT_REFNO
	,NationalSpecialtyCode = null
	,PCTCode = null

	,ReferralDate = Referral.RECVD_DTTM
	,BookedDate = null
	,BookedTime = null
	,AppointmentDate = null
	,AppointmentTypeCode = null
	,AppointmentStatusCode = null
	,AppointmentCategoryCode = null
	,QM08StartWaitDate = Referral.RECVD_DTTM
	,QM08EndWaitDate = Referral.CLOSR_DATE
	,AppointmentTime = null
	,ClinicCode = null
	,SourceOfReferralCode = Referral.SORRF_REFNO

	,MRSAFlag =
		convert(
			 bit
			,case
			when exists
				(
				select
					1
				from
					dbo.ClinicalCoding
				where
					ClinicalCoding.PATNT_REFNO = Referral.PATNT_REFNO
				and	ClinicalCoding.START_DTTM <= Census.DateValue
				and	ClinicalCoding.CODE in ('009', 'INFC006')
				and	ClinicalCoding.DPTYP_CODE = 'ALERT'
				and	ClinicalCoding.ARCHV_FLAG = 'N'
				)
			then 1
			else 0
			end
		)

	,RTTPathwayID = Referral.PATNT_PATHWAY_ID
	,RTTPathwayCondition = null
	,RTTStartDate = Referral.RTT_START_DATE
	,RTTEndDate = null
	,RTTSpecialtyCode = null
	,RTTCurrentProviderCode = null

	,RTTCurrentStatusCode = RTT.RTTST_REFNO
	,RTTCurrentStatusDate = RTT.RTTST_DATE

	,RTTCurrentPrivatePatientFlag = null
	,RTTOverseasStatusFlag = null

	,PASCreated = Referral.CREATE_DTTM
	,PASUpdated = Referral.MODIF_DTTM

	,PASCreatedByWhom = 
		coalesce(
			 CreatedUser.[USER_NAME]
			,Referral.USER_CREATE
		)

	,PASUpdatedByWhom =
		coalesce(
			 UpdatedUser.[USER_NAME]
			,Referral.USER_MODIF
		)

	,ArchiveFlag =
		case
		when Referral.ARCHV_FLAG = 'N'
		then 0
		else 1
		end

	,RemovalDate =
		Referral.CLOSR_DATE

	,FuturePatientCancelDate = null

	,AdditionFlag =
		case
		when
			datediff(
				 day
				,Referral.CREATE_DTTM
				,Census.DateValue
			) < 8
		then 1
		else 0
		end

	,CountOfDNAs = null
	,CountOfHospitalCancels = null
	,CountOfPatientCancels = null

	,DateOnWaitingList = null
	,IntendedPrimaryOperationCode = null
	,Operation = null
	,WaitingListRule = -1

	,VisitCode = null
	,InviteDate = null
	,WaitingListClinicCode = null
	,GeneralComment = null
from
	dbo.Referral

inner join dbo.Parameter Census
on	Census.Parameter = 'OPWLCENSUSDATE'

left join dbo.Patient
on	Patient.PATNT_REFNO = Referral.PATNT_REFNO

left join dbo.PatientIdentifier DistrictNo
on	DistrictNo.PATNT_REFNO = Referral.PATNT_REFNO
and	DistrictNo.PITYP_REFNO = 2001232 --facility
and	left(DistrictNo.IDENTIFIER, 3) = 'RM2'
and	DistrictNo.ARCHV_FLAG = 'N'
and	not exists
	(
	select
		1
	from
		dbo.PatientIdentifier Previous
	where
		Previous.PATNT_REFNO = DistrictNo.PATNT_REFNO
	and	Previous.PITYP_REFNO = 2001232
	and	left(Previous.IDENTIFIER, 3) = 'RM2'
	and	Previous.ARCHV_FLAG = 'N'
	and	(
			Previous.START_DTTM > DistrictNo.START_DTTM
		or
			(
				Previous.START_DTTM = DistrictNo.START_DTTM
			and	Previous.PATID_REFNO > DistrictNo.PATID_REFNO
			)
		)
	)

left join dbo.PatientAddressRole
on	PatientAddressRole.PATNT_REFNO = Patient.PATNT_REFNO
and	PatientAddressRole.ROTYP_CODE = 'HOME'
and	PatientAddressRole.ARCHV_FLAG = 'N'
and	exists
	(
	select
		1
	from
		dbo.PatientAddress
	where
		PatientAddress.ADDSS_REFNO = PatientAddressRole.ADDSS_REFNO
	and	PatientAddress.ADTYP_CODE = 'POSTL'
	and	PatientAddress.ARCHV_FLAG = 'N'
	)
and	Referral.RECVD_DTTM between 
		PatientAddressRole.START_DTTM 
	and coalesce(
			PatientAddressRole.END_DTTM
			,Referral.RECVD_DTTM
		)
and	not exists
	(
	select
		1
	from
		dbo.PatientAddressRole Previous
	where
		Previous.PATNT_REFNO = PatientAddressRole.PATNT_REFNO
	and	Previous.ROTYP_CODE = PatientAddressRole.ROTYP_CODE
	and	Previous.ARCHV_FLAG = 'N'
	and	exists
		(
		select
			1
		from
			dbo.PatientAddress
		where
			PatientAddress.ADDSS_REFNO = Previous.ADDSS_REFNO
		and	PatientAddress.ADTYP_CODE = 'POSTL'
		and	PatientAddress.ARCHV_FLAG = 'N'
		)
	and	Referral.RECVD_DTTM between 
			Previous.START_DTTM 
		and coalesce(
				Previous.END_DTTM
				,Referral.RECVD_DTTM
			)
	and	(
			Previous.START_DTTM > PatientAddressRole.START_DTTM
		or	(
				Previous.START_DTTM = PatientAddressRole.START_DTTM
			and	Previous.ROLES_REFNO > PatientAddressRole.ROLES_REFNO
			)
		)
	)

left join dbo.PatientAddress
on	PatientAddress.ADDSS_REFNO = PatientAddressRole.ADDSS_REFNO
and	PatientAddress.ARCHV_FLAG = 'N'

left join dbo.PatientAddressRole PatientAddressRoleTelephone
on	PatientAddressRoleTelephone.PATNT_REFNO = Patient.PATNT_REFNO
and	PatientAddressRoleTelephone.ROTYP_CODE = 'HOME'
and	PatientAddressRoleTelephone.ARCHV_FLAG = 'N'
and	exists
	(
	select
		1
	from
		dbo.PatientAddress
	where
		PatientAddress.ADDSS_REFNO = PatientAddressRoleTelephone.ADDSS_REFNO
	and	PatientAddress.ADTYP_CODE = 'PHONE'
	and	PatientAddress.ARCHV_FLAG = 'N'
	)
and	Referral.RECVD_DTTM between 
		PatientAddressRoleTelephone.START_DTTM 
	and coalesce(
			PatientAddressRoleTelephone.END_DTTM
			,Referral.RECVD_DTTM
		)
and	not exists
	(
	select
		1
	from
		dbo.PatientAddressRole Previous
	where
		Previous.PATNT_REFNO = PatientAddressRoleTelephone.PATNT_REFNO
	and	Previous.ROTYP_CODE = PatientAddressRoleTelephone.ROTYP_CODE
	and	Previous.ARCHV_FLAG = 'N'
	and	exists
		(
		select
			1
		from
			dbo.PatientAddress
		where
			PatientAddress.ADDSS_REFNO = Previous.ADDSS_REFNO
		and	PatientAddress.ADTYP_CODE = 'PHONE'
		and	PatientAddress.ARCHV_FLAG = 'N'
		)
	and	Referral.RECVD_DTTM between 
			Previous.START_DTTM 
		and coalesce(
				Previous.END_DTTM
				,Referral.RECVD_DTTM
			)
	and	(
			Previous.START_DTTM > PatientAddressRoleTelephone.START_DTTM
		or	(
				Previous.START_DTTM = PatientAddressRoleTelephone.START_DTTM
			and	Previous.ROLES_REFNO > PatientAddressRoleTelephone.ROLES_REFNO
			)
		)
	)

left join dbo.PatientAddress PatientHomeTelephone
on	PatientHomeTelephone.ADDSS_REFNO = PatientAddressRoleTelephone.ADDSS_REFNO
and	PatientHomeTelephone.ARCHV_FLAG = 'N'

left join dbo.PatientAddressRole PatientAddressRoleWorkTelephone
on	PatientAddressRoleWorkTelephone.PATNT_REFNO = Patient.PATNT_REFNO
and	PatientAddressRoleWorkTelephone.ROTYP_CODE = 'WP' --Work Telephone Number
and	PatientAddressRoleWorkTelephone.ARCHV_FLAG = 'N'
and	exists
	(
	select
		1
	from
		dbo.PatientAddress
	where
		PatientAddress.ADDSS_REFNO = PatientAddressRoleWorkTelephone.ADDSS_REFNO
	and	PatientAddress.ADTYP_CODE = 'PHONE'
	and	PatientAddress.ARCHV_FLAG = 'N'
	)
and	Referral.RECVD_DTTM between 
		PatientAddressRoleWorkTelephone.START_DTTM 
	and coalesce(
			PatientAddressRoleWorkTelephone.END_DTTM
			,Referral.RECVD_DTTM
		)
and	not exists
	(
	select
		1
	from
		dbo.PatientAddressRole Previous
	where
		Previous.PATNT_REFNO = PatientAddressRoleWorkTelephone.PATNT_REFNO
	and	Previous.ROTYP_CODE = PatientAddressRoleWorkTelephone.ROTYP_CODE
	and	Previous.ARCHV_FLAG = 'N'
	and	exists
		(
		select
			1
		from
			dbo.PatientAddress
		where
			PatientAddress.ADDSS_REFNO = Previous.ADDSS_REFNO
		and	PatientAddress.ADTYP_CODE = 'PHONE'
		and	PatientAddress.ARCHV_FLAG = 'N'
		)
	and	Referral.RECVD_DTTM between 
			Previous.START_DTTM 
		and coalesce(
				Previous.END_DTTM
				,Referral.RECVD_DTTM
			)
	and	(
			Previous.START_DTTM > PatientAddressRoleWorkTelephone.START_DTTM
		or	(
				Previous.START_DTTM = PatientAddressRoleWorkTelephone.START_DTTM
			and	Previous.ROLES_REFNO > PatientAddressRoleWorkTelephone.ROLES_REFNO
			)
		)
	)

left join dbo.PatientAddress PatientWorkTelephone
on	PatientWorkTelephone.ADDSS_REFNO = PatientAddressRoleWorkTelephone.ADDSS_REFNO
and	PatientAddressRoleTelephone.ARCHV_FLAG = 'N'

left join dbo.PatientProfessionalCarer RegisteredGp
on	RegisteredGp.PATNT_REFNO = Referral.PATNT_REFNO
and	RegisteredGp.PRTYP_MAIN_CODE = 'GMPRC'
and	RegisteredGp.ARCHV_FLAG = 'N'
and	Referral.RECVD_DTTM between 
		RegisteredGp.START_DTTM 
	and coalesce(
			RegisteredGp.END_DTTM
			,Referral.RECVD_DTTM
		)
and	not exists
	(
	select
		1
	from
		dbo.PatientProfessionalCarer Previous
	where
		Previous.PATNT_REFNO = RegisteredGp.PATNT_REFNO
	and	Previous.PRTYP_MAIN_CODE = RegisteredGp.PRTYP_MAIN_CODE
	and	Previous.ARCHV_FLAG = 'N'
	and	Referral.RECVD_DTTM between 
			Previous.START_DTTM 
		and coalesce(
				Previous.END_DTTM
				,Referral.RECVD_DTTM
			)
	and	(
			Previous.START_DTTM > RegisteredGp.START_DTTM
		or	(
				Previous.START_DTTM = RegisteredGp.START_DTTM
			and	Previous.PATPC_REFNO > RegisteredGp.PATPC_REFNO
			)
		)
	)

left join dbo.PatientProfessionalCarer DefaultRegisteredGp
on	DefaultRegisteredGp.PATNT_REFNO = Referral.PATNT_REFNO
and	DefaultRegisteredGp.PRTYP_MAIN_CODE = 'GMPRC'
and	DefaultRegisteredGp.ARCHV_FLAG = 'N'
and	Referral.RECVD_DTTM < DefaultRegisteredGp.START_DTTM
and	not exists
	(
	select
		1
	from
		dbo.PatientProfessionalCarer Previous
	where
		Previous.PATNT_REFNO = DefaultRegisteredGp.PATNT_REFNO
	and	Previous.PRTYP_MAIN_CODE = DefaultRegisteredGp.PRTYP_MAIN_CODE
	and	Previous.ARCHV_FLAG = 'N'
	and	Referral.RECVD_DTTM < Previous.START_DTTM
	and	(
			Previous.START_DTTM > DefaultRegisteredGp.START_DTTM
		or	(
				Previous.START_DTTM = DefaultRegisteredGp.START_DTTM
			and	Previous.PATPC_REFNO > DefaultRegisteredGp.PATPC_REFNO
			)
		)
	)

left join dbo.Contract
on	Contract.CONTR_REFNO = Referral.CONTR_REFNO
and	Contract.ARCHV_FLAG = 'N'

left join dbo.[User] CreatedUser
on	CreatedUser.USERS_REFNO = Referral.USER_REFNO
and	CreatedUser.ARCHV_FLAG = 'N'

left join dbo.[User] UpdatedUser
on	UpdatedUser.USERS_REFNO = Referral.USER_REFNO
and	UpdatedUser.ARCHV_FLAG = 'N'

left join dbo.RTT
on	RTT.REFRL_REFNO = Referral.REFRL_REFNO
and	RTT.RTTST_REFNO is not null
and	RTT.ARCHV_FLAG = 'N'
and	not exists
	(
	select
		1
	from
		dbo.RTT Previous
	where
		Previous.REFRL_REFNO = Referral.REFRL_REFNO
	and	Previous.RTTST_REFNO is not null
	and	Previous.ARCHV_FLAG = 'N'
	and	(
			Previous.RTTST_DATE > RTT.RTTST_DATE
		or	(
				Previous.RTTST_DATE = RTT.RTTST_DATE
			and	Previous.RTTPR_REFNO > RTT.RTTPR_REFNO
			)
		)
	)					

where
	Referral.ACTYP_REFNO in 
		(
		 232		--Outpatient Activity
		,3007077	--Outpatients
		)

and	Referral.ARCHV_FLAG = 'N'
and	Referral.RECVD_DTTM <= Census.DateValue
and	(
		Referral.CLOSR_DATE > Census.DateValue
	or	Referral.CLOSR_DATE is null
	)

--no waiting list activity
and	not exists
	(
	select
		1
	from
		dbo.WaitingList
	where
		WaitingList.REFRL_REFNO = Referral.REFRL_REFNO
	and	dateadd(day, datediff(day, 0, WaitingList.CREATE_DTTM), 0) <= Census.DateValue
	and	WaitingList.ARCHV_FLAG = 'N'
	)

--no appointment activity
and	not exists
	(
	select
		1
	from
		dbo.ScheduleEvent
	where
		ScheduleEvent.REFRL_REFNO = Referral.REFRL_REFNO
	and	dateadd(day, datediff(day, 0, ScheduleEvent.CREATE_DTTM), 0) <= Census.DateValue
	and	ScheduleEvent.ARCHV_FLAG = 'N'
	)

--no spell activity
and	not exists
	(
	select
		1
	from
		dbo.ProviderSpell
	where
		ProviderSpell.REFRL_REFNO = Referral.REFRL_REFNO
	and	dateadd(day, datediff(day, 0, ProviderSpell.CREATE_DTTM), 0) <= Census.DateValue
	and	ProviderSpell.ARCHV_FLAG = 'N'
	)

--remove test patients
and	not exists
	(
	select
		1
	from
		dbo.ExcludedPatient
	where
		ExcludedPatient.SourcePatientNo = Referral.PATNT_REFNO
	)