﻿CREATE view [dbo].[ExtractRF] as
/*
	2013-01-14 KO
	Added
	Patient.NNNTS_CODE AS NHSNumberStatusCode
	
	
	2011-04-06 KD
	Added
	ReferredByConsultantCode - REFBY_PROCA_REFNO
	ReferredByOrganisationCode - REFBY_HEORG_REFNO
	ReferredBySpecialtyCode - REFBY_SPECT_REFNO



	2011-01-11 KD
	Added 
	Requested Service - ACTYP_REFNO
	CancellationDate CANC_DTTM
	CancellationReason CANRS_REFNO
	ReferralMedium RFMED_REFNO
*/

select
	 SourceUniqueID = Referral.REFRL_REFNO
	,SourcePatientNo = Referral.PATNT_REFNO
	,SourceEncounterNo = Referral.REFRL_REFNO
	,PatientTitle = Patient.TITLE_REFNO_DESCRIPTION
	,PatientForename = Patient.FORENAME
	,PatientSurname = Patient.SURNAME
	,DateOfBirth = Patient.DTTM_OF_BIRTH
	,DateOfDeath = Patient.DTTM_OF_DEATH
	,SexCode = Patient.SEXXX_REFNO
	,NHSNumber = Patient.PATNT_REFNO_NHS_IDENTIFIER
	,DistrictNo = DistrictNo.IDENTIFIER
	,Postcode = PatientAddress.PCODE
	,PatientAddress1 = PatientAddress.LINE1
	,PatientAddress2 = PatientAddress.LINE2
	,PatientAddress3 = PatientAddress.LINE3
	,PatientAddress4 = PatientAddress.LINE4
	,DHACode = PatientAddress.HDIST_CODE
	,EthnicOriginCode = Patient.ETHGR_REFNO
	,MaritalStatusCode = Patient.MARRY_REFNO
	,ReligionCode = Patient.RELIG_REFNO

	,RegisteredGpCode = 
		coalesce(
			 RegisteredGp.PROCA_REFNO
			,DefaultRegisteredGp.PROCA_REFNO
		)

	,RegisteredGpPracticeCode =
		coalesce(
			 RegisteredGp.HEORG_REFNO
			,DefaultRegisteredGp.HEORG_REFNO
		)

	,EpisodicGpCode = 
		coalesce(
			 RegisteredGp.PROCA_REFNO
			,DefaultRegisteredGp.PROCA_REFNO
		)

	,EpisodicGpPracticeCode =
		coalesce(
			 RegisteredGp.HEORG_REFNO
			,DefaultRegisteredGp.HEORG_REFNO
		)

	,EpisodicGdpCode = null

	,SiteCode =
		coalesce(
			 Referral.REFTO_CLINIC_SPONT_REFNO
			,Referral.REFTO_WARD_SPONT_REFNO
		)

	,ConsultantCode = Referral.REFTO_PROCA_REFNO
	,SpecialtyCode = Referral.REFTO_SPECT_REFNO
	,SourceOfReferralCode = Referral.SORRF_REFNO
	,PriorityCode = Referral.PRITY_REFNO
	,ReferralDate = Referral.RECVD_DTTM
	,DischargeDate = Referral.CLOSR_DATE
	,DischargeReasonCode = Referral.CLORS_REFNO
	,DischargeReason = null
	,AdminCategoryCode = Referral.ADCAT_REFNO
	,ContractSerialNo = Contract.SERIAL_NUMBER
	,RTTPathwayID = Referral.PATNT_PATHWAY_ID
	,RTTPathwayCondition = null
	,RTTStartDate = Referral.RTT_START_DATE
	,RTTEndDate = null
	,RTTSpecialtyCode = null
	,RTTCurrentProviderCode = null

	,RTTCurrentStatusCode = RTT.RTTST_REFNO
	,RTTCurrentStatusDate = RTT.RTTST_DATE

	,RTTCurrentPrivatePatientFlag = null
	,RTTOverseasStatusFlag = null

	,NextFutureAppointmentDate =
		(
		select
			Outpatient.REQST_DTTM
		from
			dbo.ScheduleEvent Outpatient
		where
			Outpatient.REFRL_REFNO = Referral.REFRL_REFNO
		and	Outpatient.SCTYP_REFNO in (1470, 1472) --1472 = WA, 1470 = OP
		and	Outpatient.REQST_DTTM > dateadd(day, datediff(day, 0, getdate()), 0)
		and	not exists
			(
			select
				1
			from
				dbo.ScheduleEvent Previous
			where
				Previous.REFRL_REFNO = Outpatient.REFRL_REFNO
			and	Previous.SCTYP_REFNO in (1470, 1472) --1472 = WA, 1470 = OP
			and	Previous.REQST_DTTM > dateadd(day, datediff(day, 0, getdate()), 0)
			and	(
					Previous.REQST_DTTM < Outpatient.REQST_DTTM
				or	(
						Previous.REQST_DTTM = Outpatient.REQST_DTTM
					and	Previous.SCHDL_REFNO < Outpatient.SCHDL_REFNO
					)
				)
			)
		)

	,InterfaceCode = 'LOR'

	,PASCreated = Referral.CREATE_DTTM
	,PASUpdated = Referral.MODIF_DTTM

	,PASCreatedByWhom = 
		coalesce(
			 CreatedUser.[USER_NAME]
			,Referral.USER_CREATE
		)

	,PASUpdatedByWhom =
		coalesce(
			 UpdatedUser.[USER_NAME]
			,Referral.USER_MODIF
		)

	,ArchiveFlag =
		case
		when Referral.ARCHV_FLAG = 'N'
		then 0
		else 1
		end

	,ReferralCreated = Referral.Created
	
	,ReferralStatus = Referral.RSTAT_REFNO

	,OriginalProviderReferralDate = Referral.ORDTA_DTTM

	,ParentSourceUniqueID = Referral.PARNT_REFNO
	
	,RequestedService = Referral.ACTYP_REFNO
	
	,CancellationDate = CANCD_DTTM
	
	,CancellationReason = CANRS_REFNO
	
	,ReferralMedium = Referral.RFMED_REFNO
	
	,ReferredByConsultantCode = Referral.REFBY_PROCA_REFNO
	
	,ReferredByOrganisationCode = Referral.REFBY_HEORG_REFNO
	
	,ReferredBySpecialtyCode = Referral.REFBY_SPECT_REFNO
	
	,SuspectedCancerTypeCode = Referral.UCTYP_REFNO

	,ReferralSentDate = Referral.SENT_DTTM

	,PatientDeceased =
		case
		when Patient.DECSD_FLAG = 'Y'
		then 1
		else 0
		end

	,GeneralComment = GeneralComment.NOTES_REFNO_NOTE
	,Patient.NNNTS_CODE AS NHSNumberStatusCode
	,ReferralComment =  ReferralComment.NOTES_REFNO_NOTE
from
	dbo.Referral

left join dbo.Patient
on	Patient.PATNT_REFNO = Referral.PATNT_REFNO

--copied from ExtractOP. Not sure if required
	--LEFT OUTER JOIN dbo.OverseasVisitorStatus AS OverseasVisitorStatus 
	--	ON OverseasVisitorStatus.PATNT_REFNO = Outpatient.PATNT_REFNO 
	--	AND OverseasVisitorStatus.ARCHV_FLAG = 'N' 
	--	AND Outpatient.START_DTTM BETWEEN OverseasVisitorStatus.START_DTTM 
	--	AND COALESCE (OverseasVisitorStatus.END_DTTM, Outpatient.START_DTTM) 
	--	AND NOT EXISTS
 --                         (SELECT     1 AS Expr1
 --                           FROM          dbo.OverseasVisitorStatus AS Previous
 --                           WHERE      (PATNT_REFNO = OverseasVisitorStatus.PATNT_REFNO) AND (ARCHV_FLAG = 'N') AND (Outpatient.START_DTTM BETWEEN START_DTTM AND 
 --                                                  COALESCE (END_DTTM, Outpatient.START_DTTM)) AND (START_DTTM > OverseasVisitorStatus.START_DTTM) OR
 --                                                  (PATNT_REFNO = OverseasVisitorStatus.PATNT_REFNO) AND (ARCHV_FLAG = 'N') AND (Outpatient.START_DTTM BETWEEN START_DTTM AND 
 --                                                  COALESCE (END_DTTM, Outpatient.START_DTTM)) AND (START_DTTM = OverseasVisitorStatus.START_DTTM) AND 
 --                                                  (OVSEA_REFNO > OverseasVisitorStatus.OVSEA_REFNO)) 
                                                   
left join dbo.PatientIdentifier DistrictNo
on	DistrictNo.PATNT_REFNO = Referral.PATNT_REFNO
and	DistrictNo.PITYP_REFNO = 2001232 --facility
and	DistrictNo.IDENTIFIER like 'RM2%'
and	DistrictNo.ARCHV_FLAG = 'N'
and	not exists
	(
	select
		1
	from
		dbo.PatientIdentifier Previous
	where
		Previous.PATNT_REFNO = DistrictNo.PATNT_REFNO
	and	Previous.PITYP_REFNO = 2001232
	and	Previous.IDENTIFIER like 'RM2%'
	and	Previous.ARCHV_FLAG = 'N'
	and	(
			Previous.START_DTTM > DistrictNo.START_DTTM
		or
			(
				Previous.START_DTTM = DistrictNo.START_DTTM
			and	Previous.PATID_REFNO > DistrictNo.PATID_REFNO
			)
		)
	)

left join dbo.PatientAddressRole
on	PatientAddressRole.PATNT_REFNO = Patient.PATNT_REFNO
and	PatientAddressRole.ROTYP_CODE = 'HOME'
and	PatientAddressRole.ARCHV_FLAG = 'N'
and	exists
	(
	select
		1
	from
		dbo.PatientAddress
	where
		PatientAddress.ADDSS_REFNO = PatientAddressRole.ADDSS_REFNO
	and	PatientAddress.ADTYP_CODE = 'POSTL'
	and	PatientAddress.ARCHV_FLAG = 'N'
	)
and	Referral.RECVD_DTTM between 
		PatientAddressRole.START_DTTM 
	and coalesce(
			PatientAddressRole.END_DTTM
			,Referral.RECVD_DTTM
		)
and	not exists
	(
	select
		1
	from
		dbo.PatientAddressRole Previous
	where
		Previous.PATNT_REFNO = PatientAddressRole.PATNT_REFNO
	and	Previous.ROTYP_CODE = PatientAddressRole.ROTYP_CODE
	and	Previous.ARCHV_FLAG = 'N'
	and	exists
		(
		select
			1
		from
			dbo.PatientAddress
		where
			PatientAddress.ADDSS_REFNO = Previous.ADDSS_REFNO
		and	PatientAddress.ADTYP_CODE = 'POSTL'
		and	PatientAddress.ARCHV_FLAG = 'N'
		)
	and	Referral.RECVD_DTTM between 
			Previous.START_DTTM 
		and coalesce(
				Previous.END_DTTM
				,Referral.RECVD_DTTM
			)
	and	(
			Previous.START_DTTM > PatientAddressRole.START_DTTM
		or	(
				Previous.START_DTTM = PatientAddressRole.START_DTTM
			and	Previous.ROLES_REFNO > PatientAddressRole.ROLES_REFNO
			)
		)
	)

left join dbo.PatientAddress
on	PatientAddress.ADDSS_REFNO = PatientAddressRole.ADDSS_REFNO
and	PatientAddress.ARCHV_FLAG = 'N'

left join dbo.PatientProfessionalCarer RegisteredGp
on	RegisteredGp.PATNT_REFNO = Referral.PATNT_REFNO
and	RegisteredGp.PRTYP_MAIN_CODE = 'GMPRC'
and	RegisteredGp.ARCHV_FLAG = 'N'
and	Referral.RECVD_DTTM between 
		RegisteredGp.START_DTTM 
	and coalesce(
			RegisteredGp.END_DTTM
			,Referral.RECVD_DTTM
		)
and	not exists
	(
	select
		1
	from
		dbo.PatientProfessionalCarer Previous
	where
		Previous.PATNT_REFNO = RegisteredGp.PATNT_REFNO
	and	Previous.PRTYP_MAIN_CODE = RegisteredGp.PRTYP_MAIN_CODE
	and	Previous.ARCHV_FLAG = 'N'
	and	Referral.RECVD_DTTM between 
			Previous.START_DTTM 
		and coalesce(
				Previous.END_DTTM
				,Referral.RECVD_DTTM
			)
	and	(
			Previous.START_DTTM > RegisteredGp.START_DTTM
		or	(
				Previous.START_DTTM = RegisteredGp.START_DTTM
			and	Previous.PATPC_REFNO > RegisteredGp.PATPC_REFNO
			)
		)
	)

left join dbo.PatientProfessionalCarer DefaultRegisteredGp
on	DefaultRegisteredGp.PATNT_REFNO = Referral.PATNT_REFNO
and	DefaultRegisteredGp.PRTYP_MAIN_CODE = 'GMPRC'
and	DefaultRegisteredGp.ARCHV_FLAG = 'N'
and	Referral.RECVD_DTTM < DefaultRegisteredGp.START_DTTM
and	not exists
	(
	select
		1
	from
		dbo.PatientProfessionalCarer Previous
	where
		Previous.PATNT_REFNO = DefaultRegisteredGp.PATNT_REFNO
	and	Previous.PRTYP_MAIN_CODE = DefaultRegisteredGp.PRTYP_MAIN_CODE
	and	Previous.ARCHV_FLAG = 'N'
	and	Referral.RECVD_DTTM < Previous.START_DTTM
	and	(
			Previous.START_DTTM > DefaultRegisteredGp.START_DTTM
		or	(
				Previous.START_DTTM = DefaultRegisteredGp.START_DTTM
			and	Previous.PATPC_REFNO > DefaultRegisteredGp.PATPC_REFNO
			)
		)
	)

left join dbo.Contract
on	Contract.CONTR_REFNO = Referral.CONTR_REFNO
and	Contract.ARCHV_FLAG = 'N'

left join dbo.[User] CreatedUser
on	CreatedUser.USERS_REFNO = Referral.USER_REFNO
and	CreatedUser.ARCHV_FLAG = 'N'

left join dbo.[User] UpdatedUser
on	UpdatedUser.USERS_REFNO = Referral.USER_REFNO
and	UpdatedUser.ARCHV_FLAG = 'N'

left join dbo.RTT
on	RTT.REFRL_REFNO = Referral.REFRL_REFNO
and	RTT.ARCHV_FLAG = 'N'
and	not exists
	(
	select
		1
	from
		dbo.RTT Previous
	where
		Previous.REFRL_REFNO = Referral.REFRL_REFNO
	and	Previous.ARCHV_FLAG = 'N'
	and	(
			Previous.RTTST_DATE > RTT.RTTST_DATE
		or	(
				Previous.RTTST_DATE = RTT.RTTST_DATE
			and	Previous.RTTPR_REFNO > RTT.RTTPR_REFNO
			)
		)
	)					


left join dbo.NoteRole GeneralComment
on	GeneralComment.SORCE_REFNO = Referral.REFRL_REFNO
and	GeneralComment.SORCE_CODE = 'RDCMT'
and	GeneralComment.ARCHV_FLAG = 'N'
and	not exists
	(
	select
		1
	from
		dbo.NoteRole Previous
	where
		Previous.SORCE_REFNO = Referral.REFRL_REFNO
	and	Previous.SORCE_CODE = 'RDCMT'
	and	Previous.ARCHV_FLAG = 'N'
	and	Previous.NOTRL_REFNO > GeneralComment.NOTRL_REFNO
	)


left join dbo.NoteRole ReferralComment
on	ReferralComment.SORCE_REFNO = Referral.REFRL_REFNO
and	ReferralComment.SORCE_CODE = 'RFCMT'
and	ReferralComment.ARCHV_FLAG = 'N'
and	not exists
	(
	select
		1
	from
		dbo.NoteRole Previous
	where
		Previous.SORCE_REFNO = Referral.REFRL_REFNO
	and	Previous.SORCE_CODE = 'RFCMT'
	and	Previous.ARCHV_FLAG = 'N'
	and	Previous.NOTRL_REFNO > ReferralComment.NOTRL_REFNO
	)




where
--remove test patients
	not exists
	(
	select
		1
	from
		dbo.ExcludedPatient
	where
		ExcludedPatient.SourcePatientNo = Referral.PATNT_REFNO
	)