﻿CREATE view [dbo].[TLoadAPCStay] as

select
	 [APCST_REFNO] = convert(int,Column5)
	,[APCDT_REFNO] = convert(int,Column6)
	,[BEDST_REFNO] = convert(numeric,Column7)
	,[SSTAY_REFNO] = convert(numeric,Column8)
	,[ORGANS_SUPPORTED] = convert(numeric,Column9)
	,[CARE_LEVEL] = Column10
	,[CCP_FLAG] = Column11
	,[START_DTTM] = convert(smalldatetime,Column12)
	,[USER_CREATE] = Column13
	,[USER_MODIF] = Column14
	,[CREATE_DTTM] = convert(datetime,Column15)
	,[MODIF_DTTM] = convert(datetime,Column16)
	,[ARCHV_FLAG] = Column17
	,[STRAN_REFNO] = convert(numeric,Column18)
	,[PRIOR_POINTER] = convert(int,Column19)
	,[EXTERNAL_KEY] = Column20
	,[OWNER_HEORG_REFNO] = convert(int,Column21)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_IP_APCST_%'
and	Valid = 1