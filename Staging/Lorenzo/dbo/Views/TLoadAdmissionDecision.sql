﻿CREATE view [dbo].[TLoadAdmissionDecision] as

select
	 [ADMDC_REFNO] = convert(numeric,Column5)
	,[SPECT_REFNO] = convert(numeric,Column6)
	,[SPECT_REFNO_MAIN_IDENT] = Column7
	,[PRITY_REFNO] = convert(numeric,Column8)
	,[PRITY_REFNO_MAIN_CODE] = Column9
	,[PRITY_REFNO_DESCRIPTION] = Column10
	,[REFRL_REFNO] = convert(numeric,Column11)
	,[PROCA_REFNO] = convert(numeric,Column12)
	,[PROCA_REFNO_MAIN_IDENT] = Column13
	,[PATNT_REFNO] = convert(numeric,Column14)
	,[PATNT_REFNO_PASID] = Column15
	,[PATNT_REFNO_NHS_IDENTIFIER] = Column16
	,[DTA_DTTM] = convert(datetime,Column17)
	,[CREATE_DTTM] = convert(datetime,Column18)
	,[MODIF_DTTM] = convert(datetime,Column19)
	,[USER_CREATE] = Column20
	,[USER_MODIF] = Column21
	,[ARCHV_FLAG] = Column22
	,[ADMIT_NOTAF_DTTM] = convert(datetime,Column23)
	,[OWNER_HEORG_REFNO] = convert(numeric,Column24)
	,[OWNER_HEORG_REFNO_MAIN_IDENT] = Column25
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_ADMDC_AD_%'
and	Valid = 1