﻿/***************************************************
Admission Decisions
*************************************************/


CREATE View [dbo].[TLoadAdmissionDecisionNew]
AS 
Select ADMDC_REFNO =  convert(INT,Column5),
CCCCC_REFNO =  convert(INT,Column6),
SPECT_REFNO =  convert(INT,Column7),
PRITY_REFNO =  convert(INT,Column8),
REFRL_REFNO =  convert(INT,Column9),
PROCA_REFNO =  convert(INT,Column10),
PATNT_REFNO =  convert(INT,Column11),
SCHDL_REFNO =  convert(INT,Column12),
DTA_DTTM =  convert(datetime,Column13),
CREATE_DTTM =  convert(datetime,Column14),
MODIF_DTTM =  convert(datetime,Column15),
USER_CREATE =  Column16,
USER_MODIF =  Column17,
ARCHV_FLAG =  Column18,
STRAN_REFNO =  convert(INT,Column19),
PRIOR_POINTER =  convert(INT,Column20),
EXTERNAL_KEY =  Column21,
ADMIT_NOTAF_DTTM =  convert(datetime,Column22),
STEAM_REFNO =  convert(INT,Column23),
OWNER_HEORG_REFNO =  convert(INT,Column24),
Created = Created

From TImportCSVParsed
where
	Column0 like 'RM2_GE_IP_ADMDC_%'
and	Valid = 1