﻿CREATE view [dbo].[TLoadAdmissionOffer] as

select
	 [ADMOF_REFNO] = convert(int,Column5)
	,[WLIST_REFNO] = convert(numeric,Column6)
	,[DEFER_REFNO] = convert(int,Column7)
	,[OFOCM_REFNO] = convert(int,Column8)
	,[SPBED_REFNO] = convert(int,Column9)
	,[ADMOF_DTTM] = convert(smalldatetime,Column10)
	,[OFOCM_DTTM] = convert(smalldatetime,Column11)
	,[TCI_DTTM] = convert(smalldatetime,Column12)
	,[CANCEL_DTTM] = convert(smalldatetime,Column13)
	,[CONFM_DTTM] = convert(smalldatetime,Column14)
	,[COMMENTS] = Column15
	,[DEFER_FLAG] = Column16
	,[DEFER_DTTM] = convert(smalldatetime,Column17)
	,[SPONT_REFNO] = convert(int,Column18)
	,[BDCAT_REFNO] = convert(int,Column19)
	,[XFER_BDCAT_REFNO] = convert(int,Column20)
	,[EXPDS_DTTM] = convert(smalldatetime,Column21)
	,[XFER_SPONT_REFNO] = convert(int,Column22)
	,[XFER_DTTM] = convert(smalldatetime,Column23)
	,[TCI_TO_DTTM] = convert(smalldatetime,Column24)
	,[CONFM_REFNO] = convert(int,Column25)
	,[OPERATION_DTTM] = convert(smalldatetime,Column26)
	,[NIL_BY_MOUTH_DTTM] = convert(smalldatetime,Column27)
	,[ACCOMPANIED_FLAG] = Column28
	,[LOCKED_BED_FLAG] = Column29
	,[CANCR_REFNO] = convert(int,Column30)
	,[CREATE_DTTM] = convert(datetime,Column31)
	,[MODIF_DTTM] = convert(datetime,Column32)
	,[USER_CREATE] = Column33
	,[USER_MODIF] = Column34
	,[ARCHV_FLAG] = Column35
	,[STRAN_REFNO] = convert(numeric,Column36)
	,[PRIOR_POINTER] = convert(int,Column37)
	,[EXTERNAL_KEY] = Column38
	,[SPBED_ORI_REFNO] = convert(int,Column39)
	,[ADOFT_REFNO] = convert(int,Column40)
	,[IPBKT_REFNO] = convert(int,Column41)
	,[OTHER_DTTM] = convert(datetime,Column42)
	,[SHORT_NOTICE_FLAG] = Column43
	,[RTTST_REFNO] = convert(int,Column44)
	,[ERO_DTTM] = convert(datetime,Column45)
	,[OWNER_HEORG_REFNO] = convert(int,Column46)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_IP_ADMOF_%'
and	Valid = 1