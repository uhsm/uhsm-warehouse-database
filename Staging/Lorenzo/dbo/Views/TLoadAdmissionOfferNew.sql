﻿/*************************
Admission Offer
***************************/

CREATE View [dbo].[TLoadAdmissionOfferNew]
AS
SELECT ADMOF_REFNO =  convert(INT,Column5),
WLIST_REFNO =  convert(INT,Column6),
DEFER_REFNO =  convert(INT,Column7),
OFOCM_REFNO =  convert(INT,Column8),
SPBED_REFNO =  convert(INT,Column9),
ADMOF_DTTM =  convert(datetime,Column10),
OFOCM_DTTM =  convert(datetime,Column11),
TCI_DTTM =  convert(datetime,Column12),
CANCEL_DTTM =  convert(datetime,Column13),
CONFM_DTTM =  convert(datetime,Column14),
COMMENTS =  Column15,
DEFER_FLAG =  Column16,
DEFER_DTTM =  convert(datetime,Column17),
SPONT_REFNO =  convert(INT,Column18),
BDCAT_REFNO =  convert(INT,Column19),
XFER_BDCAT_REFNO =  convert(INT,Column20),
EXPDS_DTTM =  convert(datetime,Column21),
XFER_SPONT_REFNO =  convert(INT,Column22),
XFER_DTTM = convert(datetime, Column23),
TCI_TO_DTTM =  convert(datetime,Column24),
CONFM_REFNO =  convert(INT,Column25),
OPERATION_DTTM =  convert(datetime,Column26),
NIL_BY_MOUTH_DTTM =  convert(datetime,Column27),
ACCOMPANIED_FLAG =  Column28,
LOCKED_BED_FLAG =  Column29,
CANCR_REFNO =  convert(INT,Column30),
CREATE_DTTM =  convert(datetime,Column31),
MODIF_DTTM =  convert(datetime,Column32),
USER_CREATE =  Column33,
USER_MODIF =  Column34,
ARCHV_FLAG =  Column35,
STRAN_REFNO =  convert(INT,Column36),
PRIOR_POINTER =  convert(INT,Column37),
EXTERNAL_KEY =  Column38,
SPBED_ORI_REFNO =  convert(INT,Column39),
ADOFT_REFNO =  convert(INT,Column40),
IPBKT_REFNO =  convert(INT,Column41),
OTHER_DTTM =  convert(datetime,Column42),
SHORT_NOTICE_FLAG =  Column43,
RTTST_REFNO =  Column44,
ERO_DTTM =  convert(datetime,Column45),
VERBAL_FLAG =  Column46,
PATNT_UNAVAILABLE =  Column47,
CONDT_REFNO =  convert(INT,Column48),
OCMDT_REFNO =  convert(INT,Column49),
OWNER_HEORG_REFNO =  convert(INT,Column50),
Created = Created

FROM dbo.TImportCSVParsed
WHERE Column0 LIKE 'RM2_GE_IP_ADMOF_%'
	AND Column0 LIKE '%_V03.csv'
	--AND Valid = 1