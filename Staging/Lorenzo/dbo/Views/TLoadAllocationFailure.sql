﻿CREATE view [dbo].[TLoadAllocationFailure] as

select
	 [ALFLR_REFNO] = convert(int,Column5)
	,[SORCE_CODE] = Column6
	,[SORCE_REFNO] = convert(numeric,Column7)
	,[PRCAE_REFNO] = convert(numeric,Column8)
	,[ALTRT_REFNO] = convert(int,Column9)
	,[ALFER_REFNO] = convert(int,Column10)
	,[ALFST_REFNO] = convert(int,Column11)
	,[USER_CREATE] = Column12
	,[USER_MODIF] = Column13
	,[CREATE_DTTM] = convert(datetime,Column14)
	,[MODIF_DTTM] = convert(datetime,Column15)
	,[PRIOR_POINTER] = convert(int,Column16)
	,[STRAN_REFNO] = convert(numeric,Column17)
	,[ARCHV_FLAG] = Column18
	,[EXTERNAL_KEY] = Column19
	,[ALREQ_REFNO] = convert(int,Column20)
	,[OWNER_HEORG_REFNO] = convert(int,Column21)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_CO_ALFLR_%'
and	Valid = 1