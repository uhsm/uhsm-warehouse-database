﻿CREATE view [dbo].[TLoadAllocationRule] as

select
	 [ALOCR_REFNO] = convert(int,Column5)
	,[ALVER_REFNO] = convert(int,Column6)
	,[CODE] = Column7
	,[DESCRIPTION] = Column8
	,[EXPRESSION] = Column9
	,[TRUE_ALOCR_REFNO] = convert(int,Column10)
	,[FALSE_ALOCR_REFNO] = convert(int,Column11)
	,[SORCE_CODE] = Column12
	,[SORCE_REFNO] = convert(numeric,Column13)
	,[SORCE_REFNO_EXPR] = Column14
	,[ALRPC_REFNO] = convert(int,Column15)
	,[ALDST_REFNO] = convert(int,Column16)
	,[ALRTP_REFNO] = convert(int,Column17)
	,[USER_CREATE] = Column18
	,[USER_MODIF] = Column19
	,[CREATE_DTTM] = convert(datetime,Column20)
	,[MODIF_DTTM] = convert(datetime,Column21)
	,[STRAN_REFNO] = convert(numeric,Column22)
	,[ARCHV_FLAG] = Column23
	,[EXTERNAL_KEY] = Column24
	,[SCLVL_REFNO] = convert(int,Column25)
	,[OWNER_HEORG_REFNO] = convert(int,Column26)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_CO_ALOCR_%'
and	Valid = 1