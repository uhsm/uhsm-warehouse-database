﻿CREATE view [dbo].[TLoadAllocationSQL] as

select
	 [ALDST_REFNO] = convert(int,Column5)
	,[CODE] = Column6
	,[DESCRIPTION] = Column7
	,[MAPPED_TAG] = Column8
	,[SQL_TEXT] = Column9
	,[USER_CREATE] = Column10
	,[USER_MODIF] = Column11
	,[CREATE_DTTM] = convert(datetime,Column12)
	,[MODIF_DTTM] = convert(datetime,Column13)
	,[STRAN_REFNO] = convert(numeric,Column14)
	,[ARCHV_FLAG] = Column15
	,[EXTERNAL_KEY] = Column16
	,[NAME] = Column17
	,[FILTER_COLUMN] = Column18
	,[FILTER_VALUE] = Column19
	,[SCLVL_REFNO] = convert(int,Column20)
	,[OWNER_HEORG_REFNO] = convert(int,Column21)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_CO_ALDST_%'
and	Valid = 1