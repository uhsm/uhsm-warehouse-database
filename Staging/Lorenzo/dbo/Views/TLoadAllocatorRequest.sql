﻿CREATE view [dbo].[TLoadAllocatorRequest] as

select
	 [ALREQ_REFNO] = convert(int,Column5)
	,[SORCE_CODE] = Column6
	,[SORCE_REFNO] = convert(numeric,Column7)
	,[PRCAE_REFNO] = convert(numeric,Column8)
	,[ALTRT_REFNO] = convert(int,Column9)
	,[ALRQS_REFNO] = convert(int,Column10)
	,[USER_CREATE] = Column11
	,[USER_MODIF] = Column12
	,[CREATE_DTTM] = convert(datetime,Column13)
	,[MODIF_DTTM] = convert(datetime,Column14)
	,[PRIOR_POINTER] = convert(int,Column15)
	,[STRAN_REFNO] = convert(numeric,Column16)
	,[ARCHV_FLAG] = Column17
	,[EXTERNAL_KEY] = Column18
	,[OWNER_HEORG_REFNO] = convert(int,Column19)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_CO_ALREQ_%'
and	Valid = 1