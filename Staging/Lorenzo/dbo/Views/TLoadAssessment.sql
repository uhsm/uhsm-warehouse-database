﻿/***************************************************
Assessments
*************************************************/
CREATE View [dbo].[TLoadAssessment]
As
Select ASSES_REFNO =  convert(INT,Column5),
ASCAT_REFNO =  convert(INT,Column6),
NAME =  Column7,
CODE =  Column8,
DESCRIPTION =  Column9,
START_DTTM =  convert(datetime,Column10),
END_DTTM =    convert(datetime,Column11),
MAX_SCORE =  Column12,
CREATE_DTTM =    convert(datetime,Column13),
MODIF_DTTM =    convert(datetime,Column14),
USER_CREATE =  Column15,
USER_MODIF =  Column16,
ARCHV_FLAG =  Column17,
STRAN_REFNO =  Column18,
PRIOR_POINTER =  convert(INT,Column19),
EXTERNAL_KEY =  Column20,
USE_PREVIOUS_FLAG =  Column21,
THRESHOLD =  Column22,
FURTHER_ASSES_REFNO =  convert(INT,Column23),
MULTI_FLAG =  Column24,
CUMUL_FLAG =  Column25,
CARER_FLAG =  Column26,
RESTRICT_ACCESS =  Column27,
HIDE_REF_TAB =  Column28,
HIDE_SCORE_TAB =  Column29,
HIDE_ANSWER_TAB =  Column30,
HIDE_CHART_TAB =  Column31,
HIDE_NOTES_TAB =  Column32,
HIDE_CUMUL_TAB =  Column33,
LDDI_TRIG =  Column34,
LATE_TRIG =  Column35,
LATE_TRIG_VALUE =  convert(INT,Column36),
MAX_TRIG =  Column37,
NEEDS_ASSESS_FLAG =  Column38,
MHA_FLAG =  Column39,
OWNER_HEORG_REFNO =  convert(INT,Column40),
Created = Created

From dbo.TImportCSVParsed
where Column0 like 'RM2_GE_RF_ASSES_%'
and Valid = 1