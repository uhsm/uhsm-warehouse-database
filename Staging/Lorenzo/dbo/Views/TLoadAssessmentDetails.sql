﻿/***************************************************
Assessment Details
*************************************************/



CREATE View [dbo].[TLoadAssessmentDetails]
AS

Select ASSDT_REFNO =  convert(INT,Column5),
ASSES_REFNO =  convert(INT,Column6),
PARNT_REFNO =  convert(INT,Column7),
SERIAL =  convert(INT,Column8),
DETAIL_TYPE_CODE =  Column9,
NAME =  Column10,
DESCRIPTION =  Column11,
PPTYP_REFNO =  convert(INT,Column12),
START_VALUE =  Column13,
END_VALUE =  Column14,
DEFAULT_VALUE =  Column15,
AXSNM_REFNO =  convert(INT,Column16),
ASQID_REFNO =  convert(INT,Column17),
DOMAIN_CODE =  Column18,
CREATE_DTTM =  convert(datetime,Column19),
MODIF_DTTM =  convert(datetime,Column20),
USER_CREATE =  Column21,
USER_MODIF =  Column22,
ARCHV_FLAG =  Column23,
STRAN_REFNO =  Column24,
PRIOR_POINTER =  convert(INT,Column25),
EXTERNAL_KEY =  Column26,
MANDATORY_FLAG =  Column27,
THRESHOLD =  convert(INT,Column28),
FURTHER_ASSES_REFNO =  convert(INT,Column29),
OWNER_HEORG_REFNO =  convert(INT,Column30),
Created = Created

From dbo.TImportCSVParsed
where Column0 like 'RM2_GE_RF_ASSDT_%'
and Valid = 1