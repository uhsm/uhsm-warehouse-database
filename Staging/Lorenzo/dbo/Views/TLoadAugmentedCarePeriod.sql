﻿CREATE view [dbo].[TLoadAugmentedCarePeriod] as

select
	 [AUGCP_REFNO] = convert(int,Column5)
	,[PRVSP_REFNO] = convert(numeric,Column6)
	,[PRCAE_REFNO] = convert(numeric,Column7)
	,[ACP_NUMBER] = convert(numeric,Column8)
	,[START_DTTM] = convert(smalldatetime,Column9)
	,[END_DTTM] = convert(smalldatetime,Column10)
	,[APCSO_REFNO] = convert(int,Column11)
	,[ICU_DAYS] = convert(numeric,Column12)
	,[HDU_DAYS] = convert(numeric,Column13)
	,[APCLO_REFNO] = convert(int,Column14)
	,[ORGANS_SUPPORTED] = Column15
	,[PROCA_REFNO] = convert(int,Column16)
	,[SPECT_REFNO] = convert(int,Column17)
	,[PLANNED_FLAG] = Column18
	,[APCOC_REFNO] = convert(int,Column19)
	,[APCDI_REFNO] = convert(int,Column20)
	,[USER_CREATE] = Column21
	,[USER_MODIF] = Column22
	,[CREATE_DTTM] = convert(datetime,Column23)
	,[MODIF_DTTM] = convert(datetime,Column24)
	,[ARCHV_FLAG] = Column25
	,[STRAN_REFNO] = convert(numeric,Column26)
	,[PRIOR_POINTER] = convert(int,Column27)
	,[EXTERNAL_KEY] = Column28
	,[CCASO_REFNO] = convert(int,Column29)
	,[CCATY_REFNO] = convert(int,Column30)
	,[CCDDE_REFNO] = convert(int,Column31)
	,[CCDLO_REFNO] = convert(int,Column32)
	,[CCDST_REFNO] = convert(int,Column33)
	,[CCSLO_REFNO] = convert(int,Column34)
	,[CCUBC_REFNO] = convert(int,Column35)
	,[CCUFU_REFNO] = convert(int,Column36)
	,[CCP_FLAG] = Column37
	,[READY_DTTM] = convert(smalldatetime,Column38)
	,[OWNER_HEORG_REFNO] = convert(int,Column39)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_AUGCP_IP_%'
and	Valid = 1