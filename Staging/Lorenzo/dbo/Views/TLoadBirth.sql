﻿CREATE view [dbo].[TLoadBirth] as

select
	 [RGBIR_REFNO] = convert(int,Column5)
	,[MATSP_REFNO] = convert(int,Column6)
	,[PATNT_REFNO] = convert(int,Column7)
	,[MTHER_REFNO] = convert(int,Column8)
	,[PROCA_REFNO] = convert(int,Column9)
	,[BIRTH_ORDER] = convert(numeric,Column10)
	,[DTTM_OF_BIRTH] = convert(smalldatetime,Column11)
	,[SEXXX_REFNO] = convert(int,Column12)
	,[DPSTS_REFNO] = convert(int,Column13)
	,[BIRST_REFNO] = convert(int,Column14)
	,[DEPLA_REFNO] = convert(int,Column15)
	,[RESME_REFNO] = convert(int,Column16)
	,[RESMD_REFNO] = convert(int,Column17)
	,[BIRTH_WEIGHT] = convert(numeric,Column18)
	,[APGAR_ONEMIN] = convert(numeric,Column19)
	,[APGAR_FIVEMIN] = convert(numeric,Column20)
	,[BCGAD_REFNO] = convert(int,Column21)
	,[CIRCM_HEAD] = convert(numeric,Column22)
	,[BIRTH_LENGTH] = convert(numeric,Column23)
	,[EXHIP_REFNO] = convert(int,Column24)
	,[FOLUP_REFNO] = convert(int,Column25)
	,[FPRES_REFNO] = convert(int,Column26)
	,[METSC_REFNO] = convert(int,Column27)
	,[JAUND_REFNO] = convert(int,Column28)
	,[FDTYP_REFNO] = convert(int,Column29)
	,[DELME_REFNO] = convert(int,Column30)
	,[MTDRG_REFNO] = convert(int,Column31)
	,[GESTN_LENGTH] = convert(numeric,Column32)
	,[APGAR_TENMIN] = convert(int,Column33)
	,[ARCHV_FLAG] = Column34
	,[STRAN_REFNO] = convert(numeric,Column35)
	,[REFRL_REFNO] = convert(int,Column36)
	,[PRIOR_POINTER] = convert(int,Column37)
	,[USER_CREATE] = Column38
	,[USER_MODIF] = Column39
	,[CREATE_DTTM] = convert(datetime,Column40)
	,[MODIF_DTTM] = convert(datetime,Column41)
	,[EXTERNAL_KEY] = Column42
	,[NNLVL_REFNO] = convert(int,Column43)
	,[OWNER_HEORG_REFNO] = convert(int,Column44)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_IP_RGBIR_%'
and	Valid = 1