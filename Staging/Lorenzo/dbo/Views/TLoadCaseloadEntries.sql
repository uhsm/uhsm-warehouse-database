﻿/***************************************************
CASELOAD_ENTRIES
*************************************************/
CREATE View [dbo].[TLoadCaseloadEntries]
AS
Select CLENT_REFNO =  convert(INT,Column5),
REFRL_REFNO =  convert(INT,Column6),
CRTYP_REFNO =  convert(INT,Column7),
CLSTA_REFNO =  convert(INT,Column8),
ALLOC_DTTM =  convert(datetime,Column9),
CLRSN_REFNO =  convert(INT,Column10),
INTLV_REFNO =  convert(INT,Column11),
DISCHARGE_DTTM =  convert(datetime,Column12),
CLOCM_REFNO =  convert(INT,Column13),
PROCA_REFNO =  convert(INT,Column14),
SPECT_REFNO =  convert(INT,Column15),
STEAM_REFNO =  convert(INT,Column16),
RESPTO_HEORG_REFNO =  convert(INT,Column17),
SUSP_START_DTTM =  convert(datetime,Column18),
SUSP_END_DTTM =  convert(datetime,Column19),
CLSRS_REFNO =  convert(INT,Column20),
PATNT_REFNO =  convert(INT,Column21),
TRANS_FROM_REFNO =  convert(INT,Column22),
CREATE_DTTM =  convert(datetime,Column23),
MODIF_DTTM =  convert(datetime,Column24),
USER_CREATE =  Column25,
USER_MODIF =  Column26,
ARCHV_FLAG =  Column27,
STRAN_REFNO =  convert(INT,Column28),
PRIOR_POINTER =   convert(INT,Column29),
EXTERNAL_KEY =  Column30,
CSDDR_REFNO =  convert(INT,Column31),
TEMP_FLAG =  Column32,
PRIOR_REFNO =  convert(INT,Column33),
ACLEV_REFNO =  convert(INT,Column34),
OWNER_HEORG_REFNO =  convert(INT,Column35),
Created = Created


From dbo.TImportCSVParsed
where Column0 like 'RM2_GE_OA_CLENT_%'
and Valid = 1