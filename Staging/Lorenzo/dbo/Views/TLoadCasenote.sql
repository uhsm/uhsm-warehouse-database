﻿CREATE view [dbo].[TLoadCasenote] as

select
	 [CASNT_REFNO] = convert(int,Column5)
	,[CASVL_COUNT] = convert(numeric,Column6)
	,[CURNT_HEORG_REFNO] = convert(int,Column7)
	,[CURNT_SPONT_REFNO] = convert(int,Column8)
	,[DESTR_FLAG] = Column9
	,[EXTRN_ID] = Column10
	,[IDENTIFIER] = Column11
	,[PATNT_REFNO] = convert(int,Column12)
	,[PDTYP_REFNO] = convert(int,Column13)
	,[STORE_HEORG_REFNO] = convert(int,Column14)
	,[STORE_SPONT_REFNO] = convert(int,Column15)
	,[CREATE_DTTM] = convert(datetime,Column16)
	,[MODIF_DTTM] = convert(datetime,Column17)
	,[USER_CREATE] = Column18
	,[USER_MODIF] = Column19
	,[ARCHV_FLAG] = Column20
	,[STRAN_REFNO] = convert(numeric,Column21)
	,[PRIOR_POINTER] = convert(int,Column22)
	,[EXTERNAL_KEY] = Column23
	,[RETAIN_FLAG] = Column24
	,[TEMP_HEORG_REFNO] = convert(int,Column25)
	,[OWNER_HEORG_REFNO] = convert(int,Column26)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_CASENOTES_CN_%'
and	Valid = 1