﻿CREATE view [dbo].[TLoadCasenoteActivity] as

select
	 [CASAC_REFNO] = convert(int,Column5)
	,[CASVL_REFNO] = convert(int,Column6)
	,[COMMENTS] = Column7
	,[DELMC_REFNO] = convert(int,Column8)
	,[END_DTTM] = convert(datetime,Column9)
	,[EXPCD_RETURN_DTTM] = convert(datetime,Column10)
	,[EXPCD_RETURN_NODAYS] = convert(numeric,Column11)
	,[PATNT_REFNO] = convert(int,Column12)
	,[REQBY_DTTM] = convert(datetime,Column13)
	,[REQBY_HEORG_REFNO] = convert(int,Column14)
	,[REQBY_PROCA_REFNO] = convert(int,Column15)
	,[REQBY_SPONT_REFNO] = convert(int,Column16)
	,[REQBY_USERS_REFNO] = convert(int,Column17)
	,[REQBY_WKSTN_REFNO] = convert(int,Column18)
	,[REQST_DTTM] = convert(datetime,Column19)
	,[REQST_ID] = convert(int,Column20)
	,[REQTO_PROCA_REFNO] = convert(int,Column21)
	,[RQPRI_REFNO] = convert(int,Column22)
	,[RQREA_REFNO] = convert(int,Column23)
	,[RQSTA_DTTM] = convert(datetime,Column24)
	,[RQSTA_REFNO] = convert(int,Column25)
	,[START_DTTM] = convert(datetime,Column26)
	,[SORCE_REFNO] = convert(numeric,Column27)
	,[SORCE_CODE] = Column28
	,[PDUSE_REFNO] = convert(int,Column29)
	,[REQON_DTTM] = convert(datetime,Column30)
	,[CREATE_DTTM] = convert(datetime,Column31)
	,[MODIF_DTTM] = convert(datetime,Column32)
	,[USER_CREATE] = Column33
	,[USER_MODIF] = Column34
	,[ARCHV_FLAG] = Column35
	,[STRAN_REFNO] = convert(numeric,Column36)
	,[PRIOR_POINTER] = convert(int,Column37)
	,[EXTERNAL_KEY] = Column38
	,[OWNER_HEORG_REFNO] = convert(int,Column39)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_CASE_ACT_CN_%'
and	Valid = 1