﻿CREATE view [dbo].[TLoadCasenoteVolume] as

select
	 [CASVL_REFNO] = convert(int,Column5)
	,[CASNT_REFNO] = convert(int,Column6)
	,[CASSL_REFNO] = convert(int,Column7)
	,[CURNT_HEORG_REFNO] = convert(int,Column8)
	,[CURNT_SPONT_REFNO] = convert(int,Column9)
	,[END_DTTM] = convert(datetime,Column10)
	,[EXTRN_ID] = Column11
	,[IDENTIFIER] = Column12
	,[MDTYP_REFNO] = convert(int,Column13)
	,[PATNT_REFNO] = convert(int,Column14)
	,[PDSTA_REFNO] = convert(int,Column15)
	,[PRIOR_REFNO] = convert(int,Column16)
	,[SERIAL] = convert(numeric,Column17)
	,[START_DTTM] = convert(datetime,Column18)
	,[STORE_HEORG_REFNO] = convert(int,Column19)
	,[STORE_SPONT_REFNO] = convert(int,Column20)
	,[TEMP_FLAG] = Column21
	,[VLTYP_REFNO] = convert(int,Column22)
	,[PDUSE_REFNO] = convert(int,Column23)
	,[REQON_DTTM] = convert(datetime,Column24)
	,[CREATE_DTTM] = convert(datetime,Column25)
	,[MODIF_DTTM] = convert(datetime,Column26)
	,[USER_CREATE] = Column27
	,[USER_MODIF] = Column28
	,[ARCHV_FLAG] = Column29
	,[STRAN_REFNO] = convert(numeric,Column30)
	,[PRIOR_POINTER] = convert(int,Column31)
	,[EXTERNAL_KEY] = Column32
	,[CASVL_PASID] = Column33
	,[CASVL_DOC_MCODE] = Column34
	,[CASVL_DOC_SEQUENCE] = convert(numeric,Column35)
	,[FTURE_HEORG_REFNO] = convert(int,Column36)
	,[FTURE_SPONT_REFNO] = convert(int,Column37)
	,[OWNER_HEORG_REFNO] = convert(int,Column38)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_CASE_VOL_CN_%'
and	Valid = 1