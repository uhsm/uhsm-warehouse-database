﻿CREATE view [dbo].[TLoadClinicalCoding] as

select
	 [DGPRO_REFNO] = convert(int,Column5)
	,[DPCLA_REFNO] = convert(int,Column6)
	,[PATNT_REFNO] = convert(int,Column7)
	,[MPLEV_REFNO] = convert(int,Column8)
	,[CPTYP_REFNO] = convert(int,Column9)
	,[ODPCD_REFNO] = convert(int,Column10)
	,[LINK_DGPRO_REFNO] = convert(int,Column11)
	,[COMMENTS] = Column12
	,[DESCRIPTION] = Column13
	,[SORCE_REFNO] = convert(numeric,Column14)
	,[SORCE_CODE] = Column15
	,[DGPRO_DTTM] = convert(smalldatetime,Column16)
	,[DPTYP_CODE] = Column17
	,[CREATE_DTTM] = convert(datetime,Column18)
	,[MODIF_DTTM] = convert(datetime,Column19)
	,[USER_CREATE] = Column20
	,[USER_MODIF] = Column21
	,[CODE] = Column22
	,[CCSXT_CODE] = Column23
	,[SUPL_CODE] = Column24
	,[SUPL_CCSXT_CODE] = Column25
	,[ARCHV_FLAG] = Column26
	,[STRAN_REFNO] = convert(numeric,Column27)
	,[PRIOR_POINTER] = convert(int,Column28)
	,[EXTERNAL_KEY] = Column29
	,[START_DTTM] = convert(smalldatetime,Column30)
	,[END_DTTM] = convert(smalldatetime,Column31)
	,[UPDATE_DTTM] = convert(smalldatetime,Column32)
	,[STEAM_REFNO] = convert(int,Column33)
	,[PROCA_REFNO] = convert(int,Column34)
	,[DURATION] = convert(int,Column35)
	,[CONFIDENTIAL] = Column36
	,[DOSAGE] = convert(int,Column37)
	,[DOUNT_REFNO] = convert(int,Column38)
	,[DPLOC_REFNO] = convert(int,Column39)
	,[FREQUENCY] = convert(int,Column40)
	,[FRUNT_REFNO] = convert(int,Column41)
	,[CANCEL_DTTM] = convert(smalldatetime,Column42)
	,[PARNT_REFNO] = convert(int,Column43)
	,[RULES_REFNO] = convert(int,Column44)
	,[VALUE] = Column45
	,[PROBLEM] = Column46
	,[LATRL_REFNO] = convert(int,Column47)
	,[PRCAE_REFNO] = convert(numeric,Column48)
	,[PERAD_REFNO] = convert(int,Column49)
	,[RSADM_REFNO] = convert(int,Column50)
	,[ANALC_REFNO] = convert(int,Column51)
	,[CAUSE_OF_DEATH] = Column52
	,[SORT_ORDER] = convert(numeric,Column53)
	,[COD_AUTH_FLAG] = Column54
	,[LATCH_AUTH_FLAG] = Column55
	,[ACTION] = Column56
	,[ALSEV_REFNO] = convert(int,Column57)
	,[CONTRACTED_PROCEDURE] = Column58
	,[SPECT_REFNO] = convert(int,Column59)
	,[HOSP_SERV_FLAG] = Column60
	,[MIN_PRICE] = convert(numeric,Column61)
	,[MAX_PRICE] = convert(numeric,Column62)
	,[DGPRO_END_DTTM] = convert(smalldatetime,Column63)
	,[BILL_ODPCD_REFNO] = convert(int,Column64)
	,[PLCTN_REFNO] = convert(int,Column65)
	,[THEBR_REFNO] = convert(int,Column66)
	,[ICDST_REFNO] = convert(int,Column67)
	,[SYN_CODE] = Column68
	,[ANTYP_REFNO] = convert(int,Column69)
	,[CODON_REFNO] = convert(int,Column70)
	,[VISTP_REFNO] = convert(numeric,Column71)
	,[OWNER_HEORG_REFNO] = convert(int,Column72)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_DGPRO_DP_%'
and	Valid = 1