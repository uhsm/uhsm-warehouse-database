﻿CREATE view [dbo].[TLoadContract] as

/*******************************************************************
Updated 2011-03-18 by KD
	Changed import spec for Purchaser table from LE22 to MR1
*******************************************************************/

select
	 [CONTR_REFNO] = Convert(int, Column5)
	,[PURCH_REFNO] = Convert(int, Column6)
	,[SERIAL_NUMBER] = Column7
	,[IDENTIFIER] = Column8
	,[DESCRIPTION] = Column9
	,[START_DTTM] = Convert(smalldatetime, Column10)
	,[END_DTTM] = Convert(smalldatetime, Column11)
	,[CTTYP_REFNO] = Convert(int, Column12)
	,[CTTYP_REFNO_MAIN_CODE] = Column13
	,[CTTYP_REFNO_DESCRIPTION] = Column14
	,[CTSTS_REFNO] = Convert(int, Column15)
	,[CTSTS_REFNO_MAIN_CODE] = Column16
	,[CTSTS_REFNO_DESCRIPTION] = Column17
	,[USER_CREATE] = Column18
	,[USER_MODIF] = Column19
	,[CREATE_DTTM] = Convert(datetime, Column20)
	,[MODIF_DTTM] = Convert(datetime, Column21)
	,[ARCHV_FLAG] = Convert(char, Column22)
	,[REVIEW_DTTM] = Convert(smalldatetime, Column23)
	,[USE_SERVICE_GROUPS] = Convert(char, Column24)
	,[PRCTP_REFNO] = Convert(int, Column25)
	,[PRCTP_REFNO_MAIN_CODE] = Column26
	,[PRCTP_REFNO_DESCRIPTION] = Column27
	,[OWNER_HEORG_REFNO] = Convert(int, Column28)
	,[OWNER_HEORG_REFNO_MAIN_IDENT] = Column29
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_CONTRACTS_RF_%'
and	Valid = 1