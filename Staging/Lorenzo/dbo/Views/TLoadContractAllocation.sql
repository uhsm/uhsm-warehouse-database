﻿CREATE view [dbo].[TLoadContractAllocation] as

select
	 [CALLO_REFNO] = convert(int,Column5)
	,[SORCE_CODE] = Column6
	,[SORCE_REFNO] = convert(numeric,Column7)
	,[CNSRV_REFNO] = convert(int,Column8)
	,[QUANTITY] = convert(int,Column9)
	,[TOTAL_PRICE] = convert(numeric,Column10)
	,[USER_CREATE] = Column11
	,[USER_MODIF] = Column12
	,[CREATE_DTTM] = convert(datetime,Column13)
	,[MODIF_DTTM] = convert(datetime,Column14)
	,[PRIOR_POINTER] = convert(int,Column15)
	,[STRAN_REFNO] = convert(numeric,Column16)
	,[ARCHV_FLAG] = Column17
	,[EXTERNAL_KEY] = Column18
	,[LOCKED_FLAG] = Column19
	,[OWNER_HEORG_REFNO] = convert(int,Column20)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_CO_CALLO_%'
and	Valid = 1