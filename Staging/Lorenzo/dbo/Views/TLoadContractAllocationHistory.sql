﻿CREATE view [dbo].[TLoadContractAllocationHistory] as

select
	 [COAHS_REFNO] = convert(int,Column5)
	,[OLD_CONTR_REFNO] = convert(numeric,Column6)
	,[NEW_CONTR_REFNO] = convert(numeric,Column7)
	,[SORCE_CODE] = Column8
	,[SORCE_REFNO] = convert(numeric,Column9)
	,[ALTRT_REFNO] = convert(numeric,Column10)
	,[START_DTTM] = convert(datetime,Column11)
	,[CREATE_DTTM] = convert(datetime,Column12)
	,[MODIF_DTTM] = convert(datetime,Column13)
	,[USER_CREATE] = Column14
	,[USER_MODIF] = Column15
	,[ARCHV_FLAG] = Column16
	,[STRAN_REFNO] = convert(numeric,Column17)
	,[EXTERNAL_KEY] = Column18
	,[OWNER_HEORG_REFNO] = convert(numeric,Column19)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_CO_COAHS_%'
and	Valid = 1