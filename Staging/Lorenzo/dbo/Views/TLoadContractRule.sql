﻿CREATE view [dbo].[TLoadContractRule] as

select
	 [DPRUL_REFNO] = convert(int,Column5)
	,[RULES_REFNO] = convert(int,Column6)
	,[SORCE_REFNO] = convert(numeric,Column7)
	,[SORCE_CODE] = Column8
	,[ENFORCE_FLAG] = Column9
	,[OPERATOR] = Column10
	,[VALUE] = Column11
	,[CREATE_DTTM] = convert(datetime,Column12)
	,[MODIF_DTTM] = convert(datetime,Column13)
	,[USER_CREATE] = Column14
	,[USER_MODIF] = Column15
	,[ARCHV_FLAG] = Column16
	,[STRAN_REFNO] = convert(numeric,Column17)
	,[PRIOR_POINTER] = convert(int,Column18)
	,[EXTERNAL_KEY] = Column19
	,[SERIAL] = convert(numeric,Column20)
	,[OPEN_CLOSE_PARENTHESIS] = Column21
	,[LOGICAL_OPERATOR] = Column22
	,[ASSOC_VALUE] = Column23
	,[OWNER_HEORG_REFNO] = convert(int,Column24)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_DEP_RULES_DE_%'
and	Valid = 1