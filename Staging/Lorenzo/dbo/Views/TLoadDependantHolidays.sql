﻿/***************************************************
Dependant Holidays
*************************************************/
CREATE View [dbo].[TLoadDependantHolidays]
as
Select DPHOL_REFNO =   convert(INT,Column5),
SORCE_REFNO =   convert(INT,Column6),
SORCE_CODE =  Column7,
START_DTTM =  convert(datetime,Column8),
END_DTTM =  convert(datetime,Column9),
CREATE_DTTM =  convert(datetime,Column10),
MODIF_DTTM =  convert(datetime,Column11),
USER_CREATE =  Column12,
USER_MODIF =  Column13,
ARCHV_FLAG =  Column14,
STRAN_REFNO =   convert(INT,Column15),
PRIOR_POINTER =   convert(INT,Column16),
EXTERNAL_KEY =  Column17,
PHCAT_REFNO =   convert(INT,Column18),
OWNER_HEORG_REFNO =   convert(INT,Column19),
Created = Created

From dbo.TImportCSVParsed
where Column0 like 'RM2_GE_PD_DPHOL_%'
and Valid = 1