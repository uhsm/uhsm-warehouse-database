﻿/***************************************************
Dependant Resources
*************************************************/



CREATE View [dbo].[TLoadDependantResources]
AS
Select DPRES_REFNO =   convert(INT,Column5),
RESRC_REFNO =   convert(INT,Column6),
SORCE_REFNO =   convert(INT,Column7),
SORCE_CODE =  Column8,
CREATE_DTTM =  convert(datetime,Column9),
MODIF_DTTM =  convert(datetime,Column10),
USER_CREATE =  Column11,
USER_MODIF =  Column12,
ARCHV_FLAG =  Column13,
STRAN_REFNO =   convert(INT,Column14),
PRIOR_POINTER =   convert(INT,Column15),
ENFORCE_FLAG =  Column16,
OPERATOR =  Column17,
EXTERNAL_KEY =  Column18,
OWNER_HEORG_REFNO =   convert(INT,Column19),
Created = Created

From dbo.TImportCSVParsed
where Column0 like 'RM2_GE_OA_DPRES_%'
and Valid = 1