﻿CREATE view [dbo].[TLoadDependantSurrogacy] as

select
	 [DPSUR_REFNO] = convert(int,Column5)
	,[SORCE_REFNO] = convert(numeric,Column6)
	,[SORCE_CODE] = Column7
	,[OPERATOR] = Column8
	,[SURRL_CODE] = Column9
	,[USERS_REFNO] = convert(int,Column10)
	,[USGRP_REFNO] = convert(int,Column11)
	,[START_DTTM] = convert(smalldatetime,Column12)
	,[END_DTTM] = convert(smalldatetime,Column13)
	,[Heorg_refno] = convert(int,Column14)
	,[MAIN_PROCA] = Column15
	,[CREATE_DTTM] = convert(datetime,Column16)
	,[MODIF_DTTM] = convert(datetime,Column17)
	,[USER_CREATE] = Column18
	,[USER_MODIF] = Column19
	,[ARCHV_FLAG] = Column20
	,[STRAN_REFNO] = convert(numeric,Column21)
	,[PRIOR_POINTER] = convert(int,Column22)
	,[EXTERNAL_KEY] = Column23
	,[APROL_REFNO] = convert(int,Column24)
	,[OWNER_HEORG_REFNO] = convert(int,Column25)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_RF_DPSUR_%'
and	Valid = 1