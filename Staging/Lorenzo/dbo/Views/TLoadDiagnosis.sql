﻿CREATE view [dbo].[TLoadDiagnosis] as

select
	 [ODPCD_REFNO] = convert(int,Column5)
	,[CODE] = Column6
	,[CCSXT_CODE] = Column7
	,[DESCRIPTION] = Column8
	,[SUPL_CODE] = Column9
	,[SUPL_CCSXT_CODE] = Column10
	,[PARNT_REFNO] = convert(int,Column11)
	,[PRICE] = convert(numeric,Column12)
	,[CREATE_DTTM] = convert(datetime,Column13)
	,[MODIF_DTTM] = convert(datetime,Column14)
	,[USER_CREATE] = Column15
	,[USER_MODIF] = Column16
	,[SUPL_DESCRIPTION] = Column17
	,[ARCHV_FLAG] = Column18
	,[STRAN_REFNO] = convert(numeric,Column19)
	,[PRIOR_POINTER] = convert(int,Column20)
	,[EXTERNAL_KEY] = Column21
	,[TRAVERSE_ONLY_FLAG] = Column22
	,[DURATION] = convert(numeric,Column23)
	,[CITEM_REFNO] = convert(int,Column24)
	,[BLOCK_NUMBER] = Column25
	,[CDTYP_REFNO] = convert(int,Column26)
	,[START_DTTM] = convert(smalldatetime,Column27)
	,[END_DTTM] = convert(smalldatetime,Column28)
	,[SCLVL_REFNO] = convert(int,Column29)
	,[SYN_CODE] = Column30
	,[OWNER_HEORG_REFNO] = convert(int,Column31)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_ODPCD_RF_%'
and	Valid = 1