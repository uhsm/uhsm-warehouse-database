﻿CREATE view [dbo].[TLoadGpAddress] as

select
	 [ADDSS_REFNO] = convert(int,Column5)
	,[ADTYP_CODE] = Column6
	,[LINE1] = Column7
	,[LINE2] = Column8
	,[LINE3] = Column9
	,[LINE4] = Column10
	,[PCODE] = Column11
	,[HDIST_CODE] = Column12
	,[CNTRY_REFNO] = convert(int,Column13)
	,[CNTRY_REFNO_MAIN_CODE] = Column14
	,[CNTRY_REFNO_DESCRIPTION] = Column15
	,[START_DTTM] = convert(datetime,Column16)
	,[END_DTTM] = convert(smalldatetime,Column17)
	,[CREATE_DTTM] = convert(datetime,Column18)
	,[MODIF_DTTM] = convert(datetime,Column19)
	,[USER_CREATE] = Column20
	,[USER_MODIF] = Column21
	,[ARCHV_FLAG] = Column22
	,[EXTERNAL_KEY] = Column23
	,[PCG_CODE] = Column24
	,[OWNER_HEORG_REFNO] = convert(int,Column25)
	,[OWNER_HEORG_REFNO_MAIN_IDENT] = Column26
	,[PROCA_REFNO] = convert(int,Column27)
	,[PROCA_REFNO_MAIN_IDENT] = Column28
	,[ROTYP_CODE] = Column29
	,[ROTYP_CODE_DESCRIPTION] = Column30
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GP_ADDSS_RF_%'
and	Valid = 1