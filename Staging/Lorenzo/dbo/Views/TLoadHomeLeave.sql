﻿CREATE view [dbo].[TLoadHomeLeave] as

select
	 [HOMEL_REFNO] = convert(numeric,Column5)
	,[SSTAY_REFNO] = convert(numeric,Column6)
	,[PRVSP_REFNO] = convert(numeric,Column7)
	,[PATNT_REFNO] = convert(numeric,Column8)
	,[PATNT_REFNO_PASID] = Column9
	,[PATNT_REFNO_NHS_IDENTIFIER] = Column10
	,[FREE_BED] = Column11
	,[START_DTTM] = convert(datetime,Column12)
	,[END_DTTM] = convert(datetime,Column13)
	,[ARCHV_FLAG] = Column14
	,[CREATE_DTTM] = convert(datetime,Column15)
	,[MODIF_DTTM] = convert(datetime,Column16)
	,[USER_CREATE] = Column17
	,[USER_MODIF] = Column18
	,[EXPTD_RETURN_DTTM] = convert(datetime,Column19)
	,[LATEST_RETURN_DTTM] = convert(datetime,Column20)
	,[LVTYP_REFNO] = convert(numeric,Column21)
	,[LVTYP_REFNO_MAIN_CODE] = Column22
	,[LVTYP_REFNO_DESCRIPTION] = Column23
	,[LVEOC_REFNO] = convert(numeric,Column24)
	,[LVEOC_REFNO_MAIN_CODE] = Column25
	,[LVEOC_REFNO_DESCRIPTION] = Column26
	,[PRVSN_FLAG] = Column27
	,[PERCA_REFNO] = convert(numeric,Column28)
	,[OWNER_HEORG_REFNO] = convert(numeric,Column29)
	,[OWNER_HEORG_REFNO_MAIN_IDENT] = Column30
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_HOMEL_IP_%'
and	Valid = 1