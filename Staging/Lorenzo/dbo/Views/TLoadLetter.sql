﻿create view [dbo].[TLoadLetter] as 
/**
Author: K Oakden
Date: 10/10/2014
Main Letter  import view
**/
select
	[LETTR_REFNO] = convert(int,Column5),
	[LTQUE_REFNO] = convert(int,Column6),
	[LTDEF_REFNO] = convert(int,Column7),
	[STRAN_REFNO] = convert(int,Column8),
	[PATNT_REFNO] = convert(int,Column9),
	[PAPCA_REFNO] = convert(int,Column10),
	[BATCH_REFNO] = convert(int,Column11),
	[PERCA_REFNO] = convert(int,Column12),
	[PROCA_REFNO] = convert(int,Column13),
	[HEORG_REFNO] = convert(int,Column14),
	[STORAGE_LOCATION] = Column15,
	[SARGS] = Column16,
	[QUEUD_DTTM] = Column17,
	[PRINT_FLAG] = Column18,
	[SUSPEND_FLAG] = Column19,
	[USER_CREATE] = Column20,
	[USER_MODIF] = Column21,
	[OWNER_HEORG_REFNO] = convert(int,Column22),
	[PRITY_SORT_ORDER] = convert(int,Column23),
	[PRITY_DESCRIPTION] = Column24,
	[DOC_TYPE_SORT_ORDER] = convert(int,Column25),
	[DOC_TYPE_DESCRIPTION] = Column26,
	[PATNT_PCODE] = Column27,
	[SORCE_REFNO] = convert(int,Column28),
	[SORCE_CODE] = Column29,
	[CREATE_DTTM] = Column30,
	[MODIF_DTTM] = Column31,
	[ARCHV_FLAG] = Column32,
	[EXTERNAL_KEY] = Column33,
	[PRINT_DTTM] = Column34,
	[PRQUE_REFNO] = convert(int,Column35),
	[LETTER_FILE_NAME] = Column36,
	[CDSTA_REFNO] = convert(int,Column37),
	[PARNT_REFNO] = convert(int,Column38),
	[QUEUD_FLAG] = Column39,
	[SPOOL_USERS_REFNO] = convert(int,Column40),
	[AUTHORISE_BY_USER] = Column41,
	[AUTHORISE_BY_DTTM] = Column42,
	[PARAGRAPHS] = Column43,
	[WKSTN_REFNO] = convert(int,Column44),
	[LETTR_OID] = Column45,
	[MRD_HEORG_REFNO] = convert(int,Column46)
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_LE_LETTR_%'
and	Valid = 1