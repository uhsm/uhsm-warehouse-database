﻿CREATE view [dbo].[TLoadLetterAudit] as 
/**
Author: K Oakden
Date: 09/10/2014
Letter Audit import view
**/
select
	LTAUD_REFNO = convert(int,Column5),
	LETTR_REFNO = convert(int,Column6),
	EDIT_USER_REFNO = convert(int,Column7),
	EDIT_DTTM = Column8,
	PRINT_USER_REFNO = convert(int,Column9),
	PRINT_DTTM = Column10,
	STATUS_USER_REFNO = convert(int,Column11),
	STATUS_DTTM = Column12,
	OLD_CDSTA_REFNO = convert(int,Column13),
	NEW_CDSTA_REFNO = convert(int,Column14),
	OWNER_HEORG_REFNO = convert(int,Column15),
	CREATE_DTTM = Column16,
	MODIF_DTTM = Column17,
	USER_CREATE = Column18,
	USER_MODIF = Column19,
	ARCHV_FLAG = Column20,
	STRAN_REFNO = convert(int,Column21),
	EXTERNAL_KEY = Column22
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_LE_LTAUD_%'
and	Valid = 1