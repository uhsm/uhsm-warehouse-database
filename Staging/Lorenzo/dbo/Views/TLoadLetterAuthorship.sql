﻿create view [dbo].[TLoadLetterAuthorship] as 
/**
Author: K Oakden
Date: 09/10/2014
Letter Authorship import view
**/
select
	[LTAUT_REFNO] = convert(int,Column5),
	[LETTR_REFNO] = convert(int,Column6),
	[LTHIS_REFNO] = convert(int,Column7),
	[USERS_REFNO] = convert(int,Column8),
	[PROCA_REFNO] = convert(int,Column9),
	[STEAM_REFNO] = convert(int,Column10),
	[HEORG_REFNO] = convert(int,Column11),
	[OWNER_HEORG_REFNO] = convert(int,Column12),
	[CREATE_DTTM] = Column13,
	[MODIF_DTTM] = Column14,
	[USER_CREATE] = Column15,
	[USER_MODIF] = Column16,
	[ARCHV_FLAG] = Column17,
	[STRAN_REFNO] = convert(int,Column18),
	[EXTERNAL_KEY] = Column19
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_LE_LTAUT_%'
and	Valid = 1