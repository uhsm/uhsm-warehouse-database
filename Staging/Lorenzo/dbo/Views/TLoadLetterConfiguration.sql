﻿create view [dbo].[TLoadLetterConfiguration] as 
/**
Author: K Oakden
Date: 09/10/2014
Letter Config import view
**/
select
	[LTCON_REFNO] = convert(int,Column5),
	[LTDEF_REFNO] = convert(int,Column6),
	[SORCE_REFNO] = convert(int,Column7),
	[SORCE_CODE] = Column8,
	[ENFORCE_FLAG] = Column9,
	[OPERATOR] = Column10,
	[OWNER_HEORG_REFNO] = convert(int,Column11),
	[CREATE_DTTM]= Column12,
	[MODIF_DTTM] = Column13,
	[USER_CREATE] = Column14,
	[USER_MODIF] = Column15,
	[ARCHV_FLAG] = Column16,
	[STRAN_REFNO] = convert(int,Column17),
	[EXTERNAL_KEY] = Column18,
	[VALUE] = Column19,
	[PROCA_REFNO] = convert(int,Column20),
	[SPECT_REFNO] = convert(int,Column21),
	[DGPRO_REFNO] = convert(int,Column22)
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_LE_LTCON_%'
and	Valid = 1