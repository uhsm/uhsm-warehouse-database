﻿create view [dbo].[TLoadLetterDatasets] as 
/**
Author: K Oakden
Date: 09/10/2014
Letter Datasets import view
**/
select
	[LTDAT_REFNO] = convert(int,Column5),
	[NAME] = Column6,
	[DESCRIPTION] = Column7,
	[DATASET_QUERY] = Column8,
	[INPATIENT_FLAG] = Column9,
	[OUTPATIENT_FLAG] = Column10,
	[OWNER_HEORG_REFNO] = convert(int,Column11),
	[CREATE_DTTM] = Column12,
	[MODIF_DTTM] = Column13,
	[USER_CREATE] = Column14,
	[USER_MODIF] = Column15,
	[ARCHV_FLAG] = Column16,
	[STRAN_REFNO] = convert(int,Column17),
	[DTYPE_REFNO] = convert(int,Column18),
	[EXTERNAL_KEY] = Column19
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_LE_LTDAT_%'
and	Valid = 1