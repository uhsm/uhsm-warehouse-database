﻿create view [dbo].[TLoadLetterDefinition] as 
/**
Author: K Oakden
Date: 09/10/2014
Letter Definition import view
**/
select
	[LTDEF_REFNO] = convert(int,Column5),
	[LTDAT_REFNO] = convert(int,Column6),
	[LDTYP_REFNO] = convert(int,Column7),
	[TMPLT_NAME] = Column8,
	[TMPLT_LOCATION] = Column9,
	[DESCRIPTION] = Column10,
	[NAME] = Column11,
	[SORT_ORDER] = convert(int,Column12),
	[PREDEFINED_FLAG] = Column13,
	[START_DTTM] = Column14,
	[END_DTTM] = Column15,
	[HTML_MERGE] = Column16,
	[OWNER_HEORG_REFNO] = convert(int,Column17),
	[CREATE_DTTM] = Column18,
	[MODIF_DTTM] = Column19,
	[USER_CREATE] = Column20,
	[USER_MODIF] = Column21,
	[ARCHV_FLAG] = Column22,
	[STRAN_REFNO] = convert(int,Column23),
	[EXTERNAL_KEY] = Column24,
	[STYPE_REFNO] = convert(int,Column25),
	[INC_LOGO] = Column26,
	[QUEUE_FLAG] = Column27,
	[SYN_CODE] = Column28,
	[LTYPE_REFNO] = convert(int,Column29),
	[DEF_CONFUD_REFNO] = convert(int,Column30)
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_LE_LTDEF_%'
and	Valid = 1