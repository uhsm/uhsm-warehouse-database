﻿create view [dbo].[TLoadLetterMergeField] as 
/**
Author: K Oakden
Date: 09/10/2014
Letter Merge Field import view
**/
select
	[LTMGF_REFNO] = convert(int,Column5),
	[LTDEF_REFNO] = convert(int,Column6),
	[NAME] = Column7,
	[OWNER_HEORG_REFNO] = convert(int,Column8),
	[CREATE_DTTM] = Column9,
	[MODIF_DTTM] = Column10,
	[USER_CREATE] = Column11,
	[USER_MODIF] = Column12,
	[ARCHV_FLAG] = Column13,
	[STRAN_REFNO] = convert(int,Column14),
	[EXTERNAL_KEY] = Column15
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_LE_LTMGF_%'
and	Valid = 1