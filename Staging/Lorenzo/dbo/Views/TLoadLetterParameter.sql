﻿CREATE view [dbo].[TLoadLetterParameter] as 
/**
Author: K Oakden
Date: 09/10/2014
Letter Parameter import view
**/
select
	[LTPRM_REFNO] = convert(int,Column5),
	[LETTR_REFNO] = convert(int,Column6),
	[SORCE_CODE] = Column7,
	[SORCE_REFNO] = convert(int,Column8),
	[USER_CREATE] = Column9,
	[USER_MODIF] = Column10,
	[OWNER_HEORG_REFNO] = convert(int,Column11),
	[CREATE_DTTM] = Column12,
	[MODIF_DTTM] = Column13,
	[ARCHV_FLAG] = Column14,
	[STRAN_REFNO] = convert(int,Column15),
	[EXTERNAL_KEY] = Column16
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_LE_LTPRM_%'
and	Valid = 1