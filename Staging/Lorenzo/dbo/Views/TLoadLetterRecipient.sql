﻿create view [dbo].[TLoadLetterRecipient] as 
/**
Author: K Oakden
Date: 10/10/2014
Letter Recipient import view
**/
select
	[LTREC_REFNO] = convert(int,Column5),
	[LETTR_REFNO] = convert(int,Column6),
	[PROCA_REFNO] = convert(int,Column7),
	[PERCA_REFNO] = convert(int,Column8),
	[PURCH_REFNO] = convert(int,Column9),
	[HEORG_REFNO] = convert(int,Column10),
	[STEAM_REFNO] = convert(int,Column11),
	[OWNER_HEORG_REFNO] = convert(int,Column12),
	[PROCA_NAME] = Column13,
	[HEORG_DESCRIPTION] = Column14,
	[RCPNT_PCODE] = Column15,
	[IS_GP] = convert(int,Column16),
	[CREATE_DTTM] = Column17,
	[MODIF_DTTM] = Column18,
	[USER_CREATE] = Column19,
	[USER_MODIF] = Column20,
	[ARCHV_FLAG] = Column21,
	[STRAN_REFNO] = convert(int,Column22),
	[EXTERNAL_KEY] = Column23,
	[MAIN_RECIP] = Column24,
	[PATNT_REFNO] = convert(int,Column25),
	[PERSS_REFNO] = convert(int,Column26)
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_LE_LTREC_%'
and	Valid = 1