﻿CREATE view [dbo].[TLoadLocalDataItem] as

select
	 [LCLDI_REFNO] = convert(int,Column5)
	,[USERS_REFNO] = convert(int,Column6)
	,[NAME] = Column7
	,[CODE] = Column8
	,[PROMPT] = Column9
	,[FIELD_FORMAT] = Column10
	,[DATA_LEN] = convert(int,Column11)
	,[LCLDT_REFNO] = convert(int,Column12)
	,[DT_INFO] = Column13
	,[START_DTTM] = convert(smalldatetime,Column14)
	,[END_DTTM] = convert(smalldatetime,Column15)
	,[HELP_TEXT] = Column16
	,[MERGEFIELD_FLAG] = Column17
	,[EDITFIELD_FLAG] = Column18
	,[XDITFIELD_FLAG] = Column19
	,[CREATE_DTTM] = convert(datetime,Column20)
	,[MODIF_DTTM] = convert(datetime,Column21)
	,[USER_CREATE] = Column22
	,[USER_MODIF] = Column23
	,[ARCHV_FLAG] = Column24
	,[STRAN_REFNO] = convert(numeric,Column25)
	,[EXTERNAL_KEY] = Column26
	,[KEYPRESS_ENAB] = Column27
	,[CALCULATE_CHECK_VALUE] = convert(numeric,Column28)
	,[OWNER_HEORG_REFNO] = convert(int,Column29)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_LOCAL_DATIT_LO_%'
and	Valid = 1