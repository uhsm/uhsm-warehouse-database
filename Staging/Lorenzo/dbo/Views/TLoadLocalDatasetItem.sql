﻿CREATE view [dbo].[TLoadLocalDatasetItem] as

select
	 [LCDSI_REFNO] = convert(int,Column5)
	,[LCLDS_REFNO] = convert(int,Column6)
	,[LCLDI_REFNO] = convert(int,Column7)
	,[MAPPED_COLUMN] = Column8
	,[CREATE_DTTM] = convert(datetime,Column9)
	,[MODIF_DTTM] = convert(datetime,Column10)
	,[USER_CREATE] = Column11
	,[USER_MODIF] = Column12
	,[ARCHV_FLAG] = Column13
	,[STRAN_REFNO] = convert(numeric,Column14)
	,[EXTERNAL_KEY] = Column15
	,[KEYPRESS_ENAB] = Column16
	,[OWNER_HEORG_REFNO] = convert(int,Column17)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_LOCAL_DASIT_LO_%'
and	Valid = 1