﻿/***************************************************
MHA Sections
*************************************************/

CREATE View [dbo].[TLoadMHASections]
AS
Select MHSEC_REFNO =  convert(INT,Column5),
CODE =  Column6,
DESCRIPTION =  Column7,
INITIAL_TUNITS =   convert(INT,Column8),
INITIAL_TUNIT_REFNO =   convert(INT,Column9),
MAX_TUNITS =   convert(INT,Column10),
MAX_TUNIT_REFNO =   convert(INT,Column11),
RENEWAL_TUNITS =   convert(INT,Column12),
RENEWAL_TUNIT_REFNO =   convert(INT,Column13),
SUBRENEWAL_TUNITS =   convert(INT,Column14),
SUBRENEWAL_TUNIT_REFNO =   convert(INT,Column15),
START_DTTM =  convert(Datetime,Column16),
END_DTTM =  convert(Datetime,Column17),
PRLID_REFNO =   convert(INT,Column18),
CREATE_DTTM =  convert(Datetime,Column19),
MODIF_DTTM =  convert(Datetime,Column20),
USER_CREATE =  Column21,
USER_MODIF =  Column22,
ARCHV_FLAG =  Column23,
STRAN_REFNO =   convert(INT,Column24),
PRIOR_POINTER =   convert(INT,Column25),
EXTERNAL_KEY =  Column26,
LEGSC_REFNO =   convert(INT,Column27),
SECRENEWAL_TUNITS =   convert(INT,Column28),
SECRENEWAL_TUNIT_REFNO =   convert(INT,Column29),
ODPCD_REFNO =   convert(INT,Column30),
MIDNIGHT_FLAG =  Column31,
RENEW_FLAG =  Column32,
CONVERT_FLAG =  Column33,
DISCHARGE_FLAG =  Column34,
SECTP_REFNO =   convert(INT,Column35),
SORT_ORDER =   convert(INT,Column36),
OWNER_HEORG_REFNO =   convert(INT,Column37),
Created = Created

From dbo.TImportCSVParsed
where Column0 like 'RM2_GE_MH_MHSEC_%'
and Valid = 1