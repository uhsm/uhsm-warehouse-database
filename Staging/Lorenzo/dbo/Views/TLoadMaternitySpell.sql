﻿CREATE view [dbo].[TLoadMaternitySpell] as

select
	 [MATSP_REFNO] = convert(int,Column5)
	,[PATNT_REFNO] = convert(int,Column6)
	,[PRVSP_REFNO] = convert(numeric,Column7)
	,[PRCAE_REFNO] = convert(numeric,Column8)
	,[PROCA_REFNO] = convert(int,Column9)
	,[SSTAY_REFNO] = convert(numeric,Column10)
	,[SPONT_REFNO] = convert(int,Column11)
	,[REFRL_REFNO] = convert(int,Column12)
	,[DEPLA_REFNO] = convert(int,Column13)
	,[DEPCR_REFNO] = convert(int,Column14)
	,[PREGNANCY_NUMBER] = convert(numeric,Column15)
	,[PARITY] = Column16
	,[FIRST_ANTENATAL] = convert(smalldatetime,Column17)
	,[MATERNAL_HEIGHT] = convert(numeric,Column18)
	,[NOOF_PRE_CAESAREANS] = convert(numeric,Column19)
	,[NOOF_IND_ABORTIONS] = convert(numeric,Column20)
	,[TOTAL_LIVE_BIRTHS] = convert(numeric,Column21)
	,[TOTAL_STILL_BIRTHS] = convert(numeric,Column22)
	,[TOTAL_NEONATAL_DEATHS] = convert(numeric,Column23)
	,[TOTAL_PREVIOUS_PREGS] = convert(numeric,Column24)
	,[NOOF_NIN_ABORTIONS] = convert(numeric,Column25)
	,[START_DTTM] = convert(smalldatetime,Column26)
	,[END_DTTM] = convert(smalldatetime,Column27)
	,[PREVIOUS_BLOOD_TRANS] = Column28
	,[RUBLA_IMMUNE] = Column29
	,[RUBLA_TESTED] = Column30
	,[RUBLA_POSITIVE] = Column31
	,[RUBLA_IMMUNISED] = Column32
	,[DELIV_DTTM] = convert(smalldatetime,Column33)
	,[ONSET_REFNO] = convert(int,Column34)
	,[DPSTS_REFNO] = convert(int,Column35)
	,[GESTN_LENGTH] = convert(numeric,Column36)
	,[FIRST_STAGE] = convert(numeric,Column37)
	,[SECOND_STAGE] = convert(numeric,Column38)
	,[NOOF_BABIES] = convert(numeric,Column39)
	,[DURING_ANALC_REFNO] = convert(int,Column40)
	,[AFTER_ANALC_REFNO] = convert(int,Column41)
	,[ARCHV_FLAG] = Column42
	,[STRAN_REFNO] = convert(numeric,Column43)
	,[PRIOR_POINTER] = convert(int,Column44)
	,[USER_CREATE] = Column45
	,[USER_MODIF] = Column46
	,[MODIF_DTTM] = convert(datetime,Column47)
	,[CREATE_DTTM] = convert(datetime,Column48)
	,[EXTERNAL_KEY] = Column49
	,[STAGE1_DTTM] = convert(smalldatetime,Column50)
	,[STAGE2_DTTM] = convert(smalldatetime,Column51)
	,[INITIAL_DEPLA_REFNO] = convert(int,Column52)
	,[DELIV_PROCA_REFNO] = convert(int,Column53)
	,[ADDITIONAL_PROCA_REFNO] = convert(int,Column54)
	,[PRIOR_PROCA_REFNO] = convert(int,Column55)
	,[DUR_ANRSN_REFNO] = convert(int,Column56)
	,[AFT_ANRSN_REFNO] = convert(int,Column57)
	,[MTPER_REFNO] = convert(int,Column58)
	,[ANRSN_REFNO] = convert(int,Column59)
	,[SELF_YNUNK_REFNO] = convert(int,Column60)
	,[NARC_YNUNK_REFNO] = convert(int,Column61)
	,[EPID_YNUNK_REFNO] = convert(int,Column62)
	,[SPIN_YNUNK_REFNO] = convert(int,Column63)
	,[LOCN_YNUNK_REFNO] = convert(int,Column64)
	,[GENR_YNUNK_REFNO] = convert(int,Column65)
	,[OTHR_YNUNK_REFNO] = convert(int,Column66)
	,[PREV_BLOOD_TRANS_COMMENTS] = Column67
	,[OWNER_HEORG_REFNO] = convert(int,Column68)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_IP_MATSP_%'
and	Valid = 1