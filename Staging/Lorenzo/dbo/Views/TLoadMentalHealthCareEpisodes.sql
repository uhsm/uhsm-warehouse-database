﻿/***************************************************
MENTAL_HEALTH_CARE_EPISODES
*************************************************/
CREATE  View [dbo].[TLoadMentalHealthCareEpisodes]
AS
Select MHCEP_REFNO = convert(INT,Column5),
PATNT_REFNO =  convert(INT,Column6),
START_DTTM =  convert(Datetime,Column7),
END_DTTM =  convert(Datetime,Column8),
DISCH_HEORG_REFNO =  convert(INT,Column9),
DISCH_REFNO =  convert(INT,Column10),
ACMTP_REFNO =  convert(INT,Column11),
USER_CREATE =  Column12,
USER_MODIF =  Column13,
CREATE_DTTM =  convert(Datetime,Column14),
MODIF_DTTM =  convert(Datetime,Column15),
STRAN_REFNO =  convert(INT,Column16),
ARCHV_FLAG =  Column17,
EXTERNAL_KEY =  Column18,
OWNER_HEORG_REFNO =  Column19,
Created = Created 
  
From dbo.TImportCSVParsed
where Column0 like 'RM2_GE_MH_MHCEP_%'
and Valid = 1