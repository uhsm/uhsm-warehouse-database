﻿/***************************************************
MENTAL_HEALTH_REVIEWS
*************************************************/

CREATE View [dbo].[TLoadMentalHealthReviews]
AS 
Select MHRVW_REFNO =   convert(INT,Column5),
PATNT_REFNO =   convert(INT,Column6),
PROCA_REFNO =   convert(INT,Column7),
REFRL_REFNO =   convert(INT,Column8),
RWTYP_REFNO =   convert(INT,Column9),
PLANNED_START_DTTM =  convert(Datetime,Column10),
PLANNED_END_DTTM =  convert(Datetime,Column11),
PLANNED_DURATION =  Column12,
ACTUAL_START_DTTM =  convert(Datetime,Column13),
ACTUAL_END_DTTM =  convert(Datetime,Column14),
ACTUAL_DURATION =   convert(INT,Column15),
LOTYP_REFNO =   convert(INT,Column16),
REVIEW_SPONT_REFNO =  convert(INT,Column17),
RWOCM_REFNO =  convert(INT,Column18),
CPALV_REFNO =  convert(INT,Column19),
NEXT_REVIEW_DTTM =  convert(Datetime,Column20),
RWCAN_REFNO =  convert(INT,Column21),
SORCE_CODE =  Column22,
SORCE_REFNO =  convert(INT,Column23),
CANCL_DTTM =  convert(Datetime,Column24),
NEXT_LOTYP_REFNO =  convert(INT,Column25),
NEXT_SPONT_REFNO =  convert(INT,Column26),
USER_CREATE =  Column27,
USER_MODIF =  Column28,
ACLEV_REFNO =  convert(INT,Column29),
CREATE_DTTM =  convert(Datetime,Column30),
MODIF_DTTM =  convert(Datetime,Column31),
STRAN_REFNO =  convert(INT,Column32),
ARCHV_FLAG =  Column33,
EXTERNAL_KEY =  Column34,
PRIOR_POINTER =  convert(INT,Column35),
ORSTS_REFNO =  convert(INT,Column36),
ABQUE_REFNO =  convert(INT,Column37),
MHCEP_REFNO =  convert(INT,Column38),
OWNER_HEORG_REFNO =  convert(INT,Column39),
Created= Created


From dbo.TImportCSVParsed
where Column0 like 'RM2_GE_MH_MHRVW_%'
and Valid = 1