﻿CREATE view [dbo].[TLoadMergeActivity] as

select
	 [MGACT_REFNO] = convert(int,Column5)
	,[MGPAT_REFNO] = convert(int,Column6)
	,[SORCE_CODE] = Column7
	,[SORCE_REFNO] = convert(numeric,Column8)
	,[PREV_IDENTIFIER] = Column9
	,[MAJOR_NOTE_REFNO] = convert(numeric,Column10)
	,[MINOR_NOTE_REFNO] = convert(int,Column11)
	,[CREATE_DTTM] = convert(datetime,Column12)
	,[MODIF_DTTM] = convert(datetime,Column13)
	,[USER_CREATE] = Column14
	,[USER_MODIF] = Column15
	,[ARCHV_FLAG] = Column16
	,[STRAN_REFNO] = convert(numeric,Column17)
	,[EXTERNAL_KEY] = Column18
	,[OWNER_HEORG_REFNO] = convert(int,Column19)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_MERGE_ACT_ME_%'
and	Valid = 1