﻿/***************************************************
Merge Activity
*************************************************/

CREATE View [dbo].[TLoadMergeActivityNew]
AS
Select MGACT_REFNO =  convert(INT,Column5),
MGPAT_REFNO =  convert(INT,Column6),
SORCE_CODE =  Column7,
SORCE_REFNO =  convert(INT,Column8),
PREV_IDENTIFIER =  Column9,
MAJOR_NOTE_REFNO =  convert(INT,Column10),
MINOR_NOTE_REFNO =  convert(INT,Column11),
CREATE_DTTM =  convert(Datetime,Column12),
MODIF_DTTM =  convert(Datetime,Column13),
USER_CREATE =  Column14,
USER_MODIF =  Column15,
ARCHV_FLAG =  Column16,
STRAN_REFNO =  convert(INT,Column17),
EXTERNAL_KEY =  Column18,
PREV_MINOR_END_DTTM =  convert(Datetime,Column19),
UPDATED_FLAG =  Column20,
OWNER_HEORG_REFNO =  convert(INT,Column21),
Created = Created

From TImportCSVParsed
where 
	Column0 like 'RM2_GE_OA_MGACT_%'
and	Valid = 1