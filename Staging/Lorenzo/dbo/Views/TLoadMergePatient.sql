﻿CREATE view [dbo].[TLoadMergePatient] as

select
	 [MGPAT_REFNO] = convert(int,Column5)
	,[PATNT_REFNO] = convert(int,Column6)
	,[PREV_PATNT_REFNO] = convert(int,Column7)
	,[MGRSN_REFNO] = convert(int,Column8)
	,[PARTIAL_FLAG] = Column9
	,[CREATE_DTTM] = convert(datetime,Column10)
	,[MODIF_DTTM] = convert(datetime,Column11)
	,[USER_CREATE] = Column12
	,[USER_MODIF] = Column13
	,[ARCHV_FLAG] = Column14
	,[STRAN_REFNO] = convert(numeric,Column15)
	,[EXTERNAL_KEY] = Column16
	,[OWNER_HEORG_REFNO] = convert(int,Column17)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_MERGE_PAT_ME_%'
and	Valid = 1