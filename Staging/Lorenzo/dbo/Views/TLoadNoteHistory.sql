﻿CREATE view [dbo].[TLoadNoteHistory] as

select
	 [NOTEH_REFNO] = convert(int,Column5)
	,[NOTES_REFNO] = convert(numeric,Column6)
	,[NOTE] = Column7
	,[START_DTTM] = convert(smalldatetime,Column8)
	,[END_DTTM] = convert(smalldatetime,Column9)
	,[CASAC_REFNO] = convert(int,Column10)
	,[CREATE_DTTM] = convert(datetime,Column11)
	,[MODIF_DTTM] = convert(datetime,Column12)
	,[USER_CREATE] = Column13
	,[USER_MODIF] = Column14
	,[ARCHV_FLAG] = Column15
	,[STRAN_REFNO] = convert(numeric,Column16)
	,[EXTERNAL_KEY] = Column17
	,[OWNER_HEORG_REFNO] = convert(int,Column18)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_OA_NOTEH_%'
and	Valid = 1