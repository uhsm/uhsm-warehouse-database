﻿CREATE view [dbo].[TLoadNoteRole] as

select
	 [NOTRL_REFNO] = convert(numeric,Column5)
	,[NOTES_REFNO] = convert(numeric,Column6)
	,[NOTES_REFNO_KEY_WORDS] = Column7
	,[NOTES_REFNO_NOTE] = Column8
	,[SORCE_REFNO] = convert(numeric,Column9)
	,[SORCE_CODE] = Column10
	,[START_DTTM] = convert(datetime,Column11)
	,[END_DTTM] = convert(datetime,Column12)
	,[USER_CREATE] = Column13
	,[USER_MODIF] = Column14
	,[CREATE_DTTM] = convert(datetime,Column15)
	,[MODIF_DTTM] = convert(datetime,Column16)
	,[SERIAL] = convert(numeric,Column17)
	,[ARCHV_FLAG] = Column18
	,[NRTYP_REFNO] = convert(numeric,Column19)
	,[NRTYP_REFNO_MAIN_CODE] = Column20
	,[NRTYP_REFNO_DESCRIPTION] = Column21
	,[OWNER_HEORG_REFNO] = convert(numeric,Column22)
	,[OWNER_HEORG_REFNO_MAIN_IDENT] = Column23
	,Created
from
	--dbo.TImportCSVParsedTest
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_NOTE_ROLES_NR_%'
and	Valid = 1