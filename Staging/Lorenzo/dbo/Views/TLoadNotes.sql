﻿/*************************************
Notes
*************************************************/

CREATE View [dbo].[TLoadNotes]
AS 
Select NOTES_REFNO =  convert(INT,Column5),
NOTE =  Column6,
KEY_WORDS =  Column7,
NOTEX_CODE =  Column8,
CREATE_DTTM =  convert(Datetime,Column9),
MODIF_DTTM =  convert(Datetime,Column10),
USER_CREATE =  Column11,
USER_MODIF =  Column12,
ARCHV_FLAG =  Column13,
STRAN_REFNO =    convert(INT,Column14),
PRIOR_POINTER =    convert(INT,Column15),
EXTERNAL_KEY =  Column16,
SYN_CODE =  Column17,
OWNER_HEORG_REFNO =    convert(INT,Column18),
Created = Created

From dbo.TImportCSVParsed
where Column0 like 'RM2_GE_OA_NOTES_%'
and Valid = 1