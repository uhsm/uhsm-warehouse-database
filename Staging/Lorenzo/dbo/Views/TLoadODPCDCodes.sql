﻿/***************************************************
ODPCD_CODES
*************************************************/


CREATE View [dbo].[TLoadODPCDCodes]
AS
Select ODPCD_REFNO =   convert(INT,Column5),
CODE =  Column6,
CCSXT_CODE =  Column7,
DESCRIPTION =  Column8,
SUPL_CODE =  Column9,
SUPL_CCSXT_CODE =  Column10,
PARNT_REFNO =   convert(INT,Column11),
PRICE =   convert(INT,Column12),
CREATE_DTTM =  convert(Datetime,Column13),
MODIF_DTTM =  convert(Datetime,Column14),
USER_CREATE =  Column15,
USER_MODIF =  Column16,
SUPL_DESCRIPTION =  Column17,
ARCHV_FLAG =  Column18,
STRAN_REFNO =   convert(INT,Column19),
PRIOR_POINTER =   convert(INT,Column20),
EXTERNAL_KEY =  Column21,
TRAVERSE_ONLY_FLAG =  Column22,
DURATION =   convert(INT,Column23),
CITEM_REFNO =   convert(INT,Column24),
BLOCK_NUMBER =  Column25,
CDTYP_REFNO =   convert(INT,Column26),
START_DTTM =  convert(Datetime,Column27),
END_DTTM =  convert(Datetime,Column28),
SCLVL_REFNO =   convert(INT,Column29),
SYN_CODE =  Column30,
NCCP_FLAG =  Column31,
OWNER_HEORG_REFNO =   convert(INT,Column32),
Created = Created

From TImportCSVParsed
where
	Column0 like 'RM2_GE_RF_ODPCD_%'
and	Valid = 1