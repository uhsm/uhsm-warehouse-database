﻿/*************************************
ODPCD_PICK_LISTS
*************************************************/

CREATE View  [dbo].[TLoadODPCDPickLists]
AS 
Select ODPPL_REFNO =  Convert(INT,Column5),
ODPCD_REFNO =   Convert(INT,Column6),
SPECT_REFNO =   Convert(INT,Column7),
PROCA_REFNO =   Convert(INT,Column8),
ODPLT_REFNO =   Convert(INT,Column9),
PRTYP_REFNO =   Convert(INT,Column10),
HEORG_REFNO =   Convert(INT,Column11),
USER_CREATE =  Column12,
USER_MODIF =  Column13,
CREATE_DTTM =  convert(Datetime,Column14),
MODIF_DTTM =   convert(Datetime,Column15),
SORT_ORDER =   Convert(INT,Column16),
STRAN_REFNO =   Convert(INT,Column17),
ARCHV_FLAG =  Column18,
EXTERNAL_KEY =  Column19,
OWNER_HEORG_REFNO =   Convert(INT,Column20),
Created = Created

From dbo.TImportCSVParsed
where Column0 like 'RM2_GE_RF_ODPPL_%'
and Valid = 1