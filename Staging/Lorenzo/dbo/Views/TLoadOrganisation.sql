﻿CREATE view [dbo].[TLoadOrganisation] as

select
	 [HEORG_REFNO] = convert(int,Column5)
	,[HOTYP_REFNO] = convert(int,Column6)
	,[CASLT_REFNO] = convert(int,Column7)
	,[MAIN_IDENT] = Column8
	,[DESCRIPTION] = Column9
	,[PARNT_REFNO] = convert(int,Column10)
	,[START_DTTM] = convert(smalldatetime,Column11)
	,[END_DTTM] = convert(smalldatetime,Column12)
	,[PDTYP_REFNO] = convert(int,Column13)
	,[CREATE_DTTM] = convert(datetime,Column14)
	,[MODIF_DTTM] = convert(datetime,Column15)
	,[USER_CREATE] = Column16
	,[USER_MODIF] = Column17
	,[RANKING] = convert(numeric,Column18)
	,[LOCAL_FLAG] = Column19
	,[ARCHV_FLAG] = Column20
	,[STRAN_REFNO] = convert(numeric,Column21)
	,[PRIOR_POINTER] = convert(int,Column22)
	,[EXTERNAL_KEY] = Column23
	,[BILLING_CENTRE] = Column24
	,[HOLVL_REFNO] = convert(int,Column25)
	,[COMPANY_CODE] = Column26
	,[SCLVL_REFNO] = convert(int,Column27)
	,[SYN_CODE] = Column28
	,[PasId_Seed] = Column29
	,[PasId_FORMAT] = Column30
	,[PAS_ID_SUFFIX] = Column31
	,[PAS_ID_MANUAL] = Column32
	,[DUMMY_FLAG] = Column33
	,[OWNER_HEORG_REFNO] = convert(int,Column34)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_HEORG_RF_%'
and	Valid = 1