﻿CREATE view [dbo].[TLoadOverseasVisitorStatus] as

select
	 [OVSEA_REFNO] = convert(numeric,Column5)
	,[OVSVS_REFNO] = convert(numeric,Column6)
	,[OVSVS_REFNO_MAIN_CODE] = Column7
	,[OVSVS_REFNO_DESCRIPTION] = Column8
	,[PATNT_REFNO] = convert(numeric,Column9)
	,[PATNT_REFNO_PASID] = Column10
	,[PATNT_REFNO_NHS_IDENTIFIER] = Column11
	,[START_DTTM] = convert(datetime,Column12)
	,[END_DTTM] = convert(datetime,Column13)
	,[ENTRY_DTTM] = convert(datetime,Column14)
	,[CREATE_DTTM] = convert(datetime,Column15)
	,[MODIF_DTTM] = convert(datetime,Column16)
	,[USER_CREATE] = Column17
	,[USER_MODIF] = Column18
	,[ARCHV_FLAG] = Column19
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_OVSEA_MP_%'
and	Valid = 1