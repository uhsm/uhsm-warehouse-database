﻿CREATE view [dbo].[TLoadParticipate] as

select
	 [PRTCP_REFNO] = convert(int,Column5)
	,[SORCE_REFNO] = convert(numeric,Column6)
	,[SORCE_CODE] = Column7
	,[PROCA_REFNO] = convert(int,Column8)
	,[PERCA_REFNO] = convert(int,Column9)
	,[PRROL_REFNO] = convert(int,Column10)
	,[RELTN_REFNO] = convert(int,Column11)
	,[START_DTTM] = convert(smalldatetime,Column12)
	,[END_DTTM] = convert(smalldatetime,Column13)
	,[PRTCP_TIME] = convert(numeric,Column14)
	,[NOOF_PARTICIPANTS] = convert(numeric,Column15)
	,[COMMENTS] = Column16
	,[CREATE_DTTM] = convert(datetime,Column17)
	,[MODIF_DTTM] = convert(datetime,Column18)
	,[USER_CREATE] = Column19
	,[USER_MODIF] = Column20
	,[ARCHV_FLAG] = Column21
	,[STRAN_REFNO] = convert(numeric,Column22)
	,[EXTERNAL_KEY] = Column23
	,[INVITED] = Column24
	,[SCHEDULED] = Column25
	,[PRESENT] = Column26
	,[OWNER_HEORG_REFNO] = convert(int,Column27)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_OA_PRTCP_%'
and	Valid = 1