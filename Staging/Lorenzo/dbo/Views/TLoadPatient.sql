﻿CREATE view [dbo].[TLoadPatient] as
/**
KO 25/11/2014
Latest patient extract
**/
select
	 PATNT_REFNO = convert(numeric,Column5)
	,SEXXX_REFNO = convert(numeric,Column6)
	,FORENAME = Column7
	,TITLE_REFNO = convert(numeric,Column8)
	,SURNAME = Column9
	,SEARCH_SURNAME = Column10
	,ETHGR_REFNO = convert(numeric,Column11)
	,MARRY_REFNO = convert(numeric,Column12)
	,DTTM_OF_BIRTH = convert(datetime,Column13)
	,OCCUP_REFNO = convert(numeric,Column14)
	,SPOKL_REFNO = convert(numeric,Column15)
	,ACLEV_REFNO = convert(numeric,Column16)
	,SNDEX_FORENAME = Column17
	,SNDEX_SURNAME = Column18
	,UPPER_FORENAME = Column19
	,UPPER_SURNAME = Column20
	,CHRON_SICK = Column21
	,DISABLED = Column22
	,DECSD_FLAG = Column23
	,CONFIDENTIAL = Column24
	,DTTM_OF_DEATH = convert(datetime,Column25)
	,PLACE_OF_BIRTH = Column26
	,PLACE_OF_DEATH = Column27
	,RELIG_REFNO = convert(numeric,Column28)
	,CNTRY_REFNO = convert(numeric,Column29)
	,INSRC_REFNO = convert(numeric,Column30)
	,OCCUP_DESCRIPTION = Column31
	,FAST_TRACK = Column32
	,CREATE_DTTM = convert(datetime,Column33)
	,MODIF_DTTM = convert(datetime,Column34)
	,USER_CREATE = Column35
	,USER_MODIF = Column36
	,PASID = Column37
	,DATE_OF_BIRTH = convert(datetime,Column38)
	,DATE_OF_DEATH = convert(datetime,Column39)
	,DEATH_NOTIFIED_DTTM = convert(datetime,Column40)
	,COMMENTS = Column41
	,ARCHV_FLAG = Column42
	,STRAN_REFNO = convert(numeric,Column43)
	,PRIOR_POINTER = convert(numeric,Column44)
	,EXTERNAL_KEY = Column45
	,TEMP_PAS_ID = Column46
	,BIRTH_DTTM_ESTIMATE_FLAG = Column47
	,INTRP_REQD_FLAG = Column48
	,BLOOD_REFNO = convert(numeric,Column49)
	,LOCAL_FLAG = Column50
	,PCT_IDEAL_BODY_WEGHT = convert(numeric,Column51)
	,REBES_REFNO = convert(numeric,Column52)
	,REGIS_DTTM = convert(datetime,Column53)
	,MULTB_REFNO = convert(numeric,Column54)
	,PRIVATE_IDENTIFIER = Column55
	,NHS_IDENTIFIER = Column56
	,LAST_SPELL_IDENTIFIER = Column57
	,CNTST_REFNO = convert(numeric,Column58)
	,APDTH_REFNO = convert(numeric,Column59)
	,CASLT_REFNO = convert(numeric,Column60)
	,PREFRD_FORENAME = Column61
	,UPPER_PREFRD_FORENAME = Column62
	,SNDEX_PREFRD_FORENAME = Column63
	,SECOND_FORENAME = Column64
	,UPPER_SECOND_FORENAME = Column65
	,SNDEX_SECOND_FORENAME = Column66
	,THIRD_FORENAME = Column67
	,UPPER_THIRD_FORENAME = Column68
	,SNDEX_THIRD_FORENAME = Column69
	,SECOND_ETHGR_REFNO = convert(numeric,Column70)
	,THIRD_ETHGR_REFNO = convert(numeric,Column71)
	,IDSUF_REFNO = convert(numeric,Column72)
	,DEATH_DTTM_ESTIMATE_FLAG = Column73
	,NNNTS_CODE = Column74
	,NATNL_REFNO = convert(numeric,Column75)
	,CFU_UPDATE_DTTM = convert(datetime,Column76)
	,RESTRICTED_ACCESS = Column77
	,RESTRICTED_VIP = Column78
	,RESTRICTION_START_DTTM = convert(datetime,Column79)
	,RESTRICTION_END_DTTM = convert(datetime,Column80)
	,RSTTP_REFNO = convert(numeric,Column81)
	,DIETY_REFNO = convert(numeric,Column82)
	,RESTRICTION_REQUESTBY = Column83
	,RELTN_REFNO = convert(numeric,Column84)
	,RSTLV_REFNO = convert(numeric,Column85)
	,MULTILINGUAL_NAME = Column86
	,RESIDENT_YNUNK_REFNO = convert(numeric,Column87)
	,SUNDRY_DEBTOR = Column88
	,PLDTH_REFNO = convert(numeric,Column89)
	,DCEST_REFNO = convert(numeric,Column90)
	,CCDTH_REFNO = convert(numeric,Column91)
	,IMMED_CODTH_ODPCD_REFNO = convert(numeric,Column92)
	,CONDT_CODTH_ODPCD_REFNO = convert(numeric,Column93)
	,UNCON_CODTH_ODPCD_REFNO = convert(numeric,Column94)
	,SGNIF_CODTH_ODPCD_REFNO = convert(numeric,Column95)
	,CODTH_DISCREPANCY = Column96
	,MERGE_MINOR_FLAG = Column97
	,STAFF_FLAG = Column98
	,PAYEE_ONLY = Column99
	,SCN_NUMBER = convert(numeric,Column100)
	,PDS_UPDATE_NEEDED = Column101
	,PDS_REASON = Column102
	,PDS_STOP_NOTED = Column103
	,DENOT_REFNO = convert(numeric,Column104)
	,INTERESTED_ORGS = Column105
	,OWNER_HEORG_SUB_FLAG = Column106
	,PRNHS_REFNO = convert(numeric,Column107)
	,PDS_NAME_IDENTIFIER = Column108
	,PDS_DOB = Column109
	,CSTST_REFNO = convert(numeric,Column110)
	,DEMEN_REFNO = convert(numeric,Column111)
	,FRAIL_REFNO = convert(numeric,Column112)
	,FIRSTCARE_DTTM = convert(datetime,Column113)
	,DD_COUNT = convert(numeric,Column114)
	,SUFFIX = Column115
	,SCHOOL_HEORG_REFNO = convert(numeric,Column116)
	,EMPST_REFNO = convert(numeric,Column117)
	,OTHER_LANG_REFNO = convert(numeric,Column118)
	,NO_OF_DPNDS = convert(numeric,Column119)
	,PDS_DOD = Column120
	,PTCNT_REFNO = convert(numeric,Column121)
	,DECEASE_REMOVED_FLAG = Column122
	,SUPERSEDED_ON_PDS = Column123
	,SUPERSEDING_NHS = Column124
	,SEXOR_REFNO = convert(numeric,Column125)
	,SUITABLITY_INDICATOR = Column126
	,REFRL_DTTM = convert(datetime,Column127)
	,SSPIN_REFNO = convert(numeric,Column128)
	,PREF_PLDTH_REFNO = convert(numeric,Column129)
	,OWNER_HEORG_REFNO = convert(numeric,Column130)
	,Created
	,PATNT_REFNO_NHS_IDENTIFIER = null
	,ETHGR_REFNO_DESCRIPTION = null
	,MARRY_REFNO_DESCRIPTION = null
	,SEXXX_REFNO_DESCRIPTION = null
	,TITLE_REFNO_DESCRIPTION = null
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_PD_PATNT_%'
and	Valid = 1