﻿CREATE view [dbo].[TLoadPatientAddress] as

select
	 [ADDSS_REFNO] = convert(int,Column5)
	,[ADTYP_CODE] = Column6
	,[LINE1] = Column7
	,[LINE2] = Column8
	,[LINE3] = Column9
	,[LINE4] = Column10
	,[PCODE] = Column11
	,[LOCAT_CODE] = Column12
	,[HDIST_CODE] = Column13
	,[ELECT_CODE] = Column14
	,[COUNTY] = Column15
	,[CNTRY_REFNO] = convert(int,Column16)
	,[START_DTTM] = convert(datetime,Column17)
	,[END_DTTM] = convert(smalldatetime,Column18)
	,[CREATE_DTTM] = convert(datetime,Column19)
	,[MODIF_DTTM] = convert(datetime,Column20)
	,[USER_CREATE] = Column21
	,[USER_MODIF] = Column22
	,[ARCHV_FLAG] = Column23
	,[STRAN_REFNO] = convert(numeric,Column24)
	,[PRIOR_POINTER] = convert(numeric,Column25)
	,[EXTERNAL_KEY] = Column26
	,[COUNT_OTCOD_REFNO] = convert(int,Column27)
	,[COMMU_OTCOD_REFNO] = convert(int,Column28)
	,[PARSH_OTCOD_REFNO] = convert(int,Column29)
	,[PUARE_OTCOD_REFNO] = convert(int,Column30)
	,[PCARE_OTCOD_REFNO] = convert(int,Column31)
	,[RES_REG_DTTM] = convert(smalldatetime,Column32)
	,[SUBURB] = Column33
	,[STATE_CODE] = Column34
	,[MPIAS_REFNO] = convert(int,Column35)
	,[PCG_CODE] = Column36
	,[TEAMA_REFNO] = convert(int,Column37)
	,[CMMCA_REFNO] = convert(int,Column38)
	,[CMMSC_REFNO] = convert(int,Column39)
	,[TLAND_REFNO] = convert(int,Column40)
	,[SYN_CODE] = Column41
	,[DEL_POINT_ID] = convert(int,Column42)
	,[QAS_BARCODE] = Column43
	,[OWNER_HEORG_REFNO] = convert(int,Column44)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_ADDRESSES_AD_%'
and	Valid = 1