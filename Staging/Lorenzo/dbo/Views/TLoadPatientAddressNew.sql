﻿/***************************************************
Addresses 
*************************************************/

CREATE View [dbo].[TLoadPatientAddressNew]
AS 
SELECT 
ADDSS_REFNO =  convert(INT,Column5),
ADTYP_CODE =  Column6,
LINE1 =  Column7,
LINE2 =  Column8,
LINE3 =  Column9,
LINE4 =  Column10,
PCODE =  Column11,
LOCAT_CODE =  Column12,
HDIST_CODE =  Column13,
ELECT_CODE =  Column14,
COUNTY =  Column15,
CNTRY_REFNO =  convert(INT,Column16),
START_DTTM =  convert(datetime,Column17),
END_DTTM =  convert(datetime,Column18),
CREATE_DTTM =  convert(datetime,Column19),
MODIF_DTTM =  convert(datetime,Column20),
USER_CREATE =  Column21,
USER_MODIF =  Column22,
ARCHV_FLAG =  Column23,
STRAN_REFNO =  convert(INT,Column24),
PRIOR_POINTER =  convert(INT,Column25),
EXTERNAL_KEY =  Column26,
COUNT_OTCOD_REFNO =  convert(INT,Column27),
COMMU_OTCOD_REFNO =  convert(INT,Column28),
PARSH_OTCOD_REFNO =  convert(INT,Column29),
PUARE_OTCOD_REFNO =  convert(INT,Column30),
PCARE_OTCOD_REFNO =  convert(INT,Column31),
RES_REG_DTTM =  convert(datetime,Column32),
SUBURB =  Column33,
STATE_CODE =  Column34,
MPIAS_REFNO =  convert(INT,Column35),
PCG_CODE =  Column36,
TEAMA_REFNO =  convert(INT,Column37),
CMMCA_REFNO =  convert(INT,Column38),
CMMSC_REFNO =  convert(INT,Column39),
TLAND_REFNO =  convert(INT,Column40),
SYN_CODE =  Column41,
DEL_POINT_ID =  Column42,
QAS_BARCODE =  Column43,
PAFKey =  convert(INT,Column44),
QAS_VERIFIED =  Column45,
PAF_ADD_KEY =  Column46,
PAF_INVALID_DATE =  convert(datetime,Column47),
OWNER_HEORG_REFNO =  convert(INT,Column48),
Created = Created

FROM dbo.TImportCSVParsed
Where Column0 like 'RM2_GE_PD_ADDSS_%'
and Valid = 1