﻿CREATE view [dbo].[TLoadPatientAddressRole] as

select
	 [ROLES_REFNO] = convert(numeric,Column5)
	,[PROCA_REFNO] = convert(numeric,Column6)
	,[PATNT_REFNO] = convert(numeric,Column7)
	,[PERCA_REFNO] = convert(numeric,Column8)
	,[HEORG_REFNO] = convert(numeric,Column9)
	,[ADDSS_REFNO] = convert(numeric,Column10)
	,[ROTYP_CODE] = Column11
	,[CORRESPONDENCE] = Column12
	,[START_DTTM] = convert(datetime,Column13)
	,[END_DTTM] = convert(datetime,Column14)
	,[DEPRT_PATRN_REFNO] = convert(numeric,Column15)
	,[CURNT_FLAG] = Column16
	,[HOMEL_REFNO] = convert(numeric,Column17)
	,[PURCH_REFNO] = convert(numeric,Column18)
	,[PROVD_REFNO] = convert(numeric,Column19)
	,[SECURE_FLAG] = Column20
	,[PATPC_REFNO] = convert(numeric,Column21)
	,[PERSS_REFNO] = convert(numeric,Column22)
	,[ORDRR_REFNO] = convert(numeric,Column23)
	,[CREATE_DTTM] = convert(datetime,Column24)
	,[MODIF_DTTM] = convert(datetime,Column25)
	,[USER_CREATE] = Column26
	,[USER_MODIF] = Column27
	,[ARCHV_FLAG] = Column28
	,[STRAN_REFNO] = convert(numeric,Column29)
	,[PRIOR_POINTER] = convert(numeric,Column30)
	,[EXTERNAL_KEY] = Column31
	,[BFORM_REFNO] = convert(numeric,Column32)
	,[SYN_CODE] = Column33
	,[OWNER_HEORG_REFNO] = convert(numeric,Column34)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_ADDSS_ROLES_AD_%'
and	Valid = 1