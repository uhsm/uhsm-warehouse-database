﻿/***************************************************
Address Roles
*************************************************/

CREATE View [dbo].[TLoadPatientAddressRoleNew]
AS

SELECT ROLES_REFNO =  convert(INT,Column5),
PROCA_REFNO =  convert(INT,Column6),
PATNT_REFNO =  convert(INT,Column7),
PERCA_REFNO =  convert(INT,Column8),
HEORG_REFNO =  convert(INT,Column9),
ADDSS_REFNO =  convert(INT,Column10),
ROTYP_CODE =  Column11,
CORRESPONDENCE =  Column12,
START_DTTM =  convert(datetime,Column13),
END_DTTM =  convert(datetime,Column14),
DEPRT_PATRN_REFNO =  convert(INT,Column15),
CURNT_FLAG =  Column16,
HOMEL_REFNO =  convert(INT,Column17),
PURCH_REFNO =  convert(INT,Column18),
PROVD_REFNO =  convert(INT,Column19),
SECURE_FLAG =  Column20,
PATPC_REFNO =  convert(INT,Column21),
PERSS_REFNO =  convert(INT,Column22),
ORDRR_REFNO =  convert(INT,Column23),
CREATE_DTTM =  convert(datetime,Column24),
MODIF_DTTM =  convert(datetime,Column25),
USER_CREATE =  Column26,
USER_MODIF =  Column27,
ARCHV_FLAG =  Column28,
STRAN_REFNO =  convert(INT,Column29),
PRIOR_POINTER =  convert(INT,Column30),
EXTERNAL_KEY =  Column31,
BFORM_REFNO =  convert(INT,Column32),
SYN_CODE =  Column33,
PDS_IDENTIFIER =  Column34,
REFRL_REFNO =  convert(INT,Column35),
DECOUPLE_FLAG =  Column36,
OWNER_HEORG_REFNO =  convert(INT,Column37),
Created = Created

FROM dbo.TImportCSVParsed
Where Column0 like 'RM2_GE_PD_ROLES_%'
and Valid = 1