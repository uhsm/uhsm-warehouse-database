﻿CREATE view [dbo].[TLoadPatientAlias] as

select
	 [PATAL_REFNO] = convert(numeric,Column5)
	,[PAAIL_REFNO] = convert(numeric,Column6)
	,[PAAIL_REFNO_MAIN_CODE] = Column7
	,[PAAIL_REFNO_DESCRIPTION] = Column8
	,[PATNT_REFNO] = convert(numeric,Column9)
	,[PATNT_REFNO_PASID] = Column10
	,[PATNT_REFNO_NHS_IDENTIFIER] = Column11
	,[ALIAS_NAME] = Column12
	,[FORENAME] = Column13
	,[START_DTTM] = convert(datetime,Column14)
	,[END_DTTM] = convert(datetime,Column15)
	,[CURNT_FLAG] = Column16
	,[CREATE_DTTM] = convert(datetime,Column17)
	,[MODIF_DTTM] = convert(datetime,Column18)
	,[USER_CREATE] = Column19
	,[USER_MODIF] = Column20
	,[ARCHV_FLAG] = Column21
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_PATAL_MP_%'
and	Valid = 1