﻿/*************************************
PATIENT_ASSESSMENT_DETAILS
*************************************************/

CREATE View [dbo].[TLoadPatientAssessmentDetails]
AS 
Select PAASD_REFNO =  Convert(INT,Column5),
PAASS_REFNO =  Convert(INT,Column6),
ASSDT_REFNO =  Convert(INT,Column7),
SCORE =  Column8,
COMMENTS =  Column9,
PROBLEM_FLAG =  Column10,
CREATE_DTTM =  Convert(Datetime,Column11),
MODIF_DTTM =  Convert(Datetime,Column12),
USER_CREATE =  Column13,
USER_MODIF =  Column14,
ARCHV_FLAG =  Column15,
STRAN_REFNO =  Convert(INT,Column16),
PRIOR_POINTER =  Convert(INT,Column17),
EXTERNAL_KEY =  Column18,
ACTIVE =  Column19,
OWNER_HEORG_REFNO =  Convert(INT,Column20),
Created = Created

From dbo.TImportCSVParsed
where Column0 like 'RM2_GE_OA_PAASD_%'
and Valid = 1