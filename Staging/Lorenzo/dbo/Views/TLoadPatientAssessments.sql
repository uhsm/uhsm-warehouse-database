﻿/*************************************
PATIENT_ASSESSMENTs
*************************************************/

CREATE View [dbo].[TLoadPatientAssessments]
AS
Select PAASS_REFNO =  Convert(INT,Column5),
PATNT_REFNO =  Convert(INT,Column6),
REFRL_REFNO =  Convert(INT,Column7),
PROCA_REFNO =  Convert(INT,Column8),
ASSESS_DTTM =  Convert(Datetime,Column9),
ASSES_REFNO =  Convert(INT,Column10),
LOTYP_REFNO =  Convert(INT,Column11),
LOCATION =  Column12,
LOTYP_HEORG_REFNO =  Convert(INT,Column13),
TOTAL_SCORE =  Convert(INT,Column14),
SORCE_CODE =  Column15,
SORCE_REFNO =  Convert(INT,Column16),
CREATE_DTTM =   Convert(Datetime,Column17),
MODIF_DTTM =   Convert(Datetime,Column18),
USER_CREATE =  Column19,
USER_MODIF =  Column20,
ARCHV_FLAG =  Column21,
STRAN_REFNO =  Convert(INT,Column21),
PRIOR_POINTER =  Convert(INT,Column23),
EXTERNAL_KEY =  Column24,
SPECT_REFNO =  Convert(INT,Column25),
STEAM_REFNO =  Convert(INT,Column26),
PATYP_REFNO =  Convert(INT,Column27),
PAOUT_REFNO =  Convert(INT,Column28),
PAORN_REFNO =  Convert(INT,Column29),
CSTAT_REFNO =  Convert(INT,Column30),
NEXT_PAASS_REFNO =  Convert(INT,Column31),
ACLEV_REFNO =  Convert(INT,Column32),
PERCA_REFNO =  Convert(INT,Column33),
LTRSN_REFNO =  Convert(INT,Column34),
ORSTS_REFNO =  Convert(INT,Column35),
CPALV_REFNO =  Convert(INT,Column36),
INITL_FLAG =  Column37,
MHRVW_REFNO =  Convert(INT,Column38),
TRCAT_REFNO =  Convert(INT,Column39),
ASSESS_END_DTTM =   Convert(Datetime,Column40),
JOINT_ASSESS_FLAG =  Column41,
ASSRN_REFNO =  Convert(INT,Column42),
CACLP_REFNO =  Convert(INT,Column43),
OWNER_HEORG_REFNO =  Convert(INT,Column44),
Created = Created


From dbo.TImportCSVParsed
where Column0 like 'RM2_GE_OA_PAASS_%'
and Valid = 1