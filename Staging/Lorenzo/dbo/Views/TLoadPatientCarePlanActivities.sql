﻿/*************************************
PATIENT_CAREPLAN_ACTIVITIES
*************************************************/


CREATE View [dbo].[TLoadPatientCarePlanActivities]
AS
Select PCPAT_REFNO =  convert(INT,Column5),
PCPST_REFNO =  convert(INT,Column6),
CPACT_REFNO =  convert(INT,Column7),
DURATION =  convert(INT,Column8),
DURATION_TUNIT_REFNO =  convert(INT,Column9),
FREQN_REFNO =  convert(INT,Column10),
PLANNED_DTTM =   convert(Datetime, Column11),
ACTUAL_DTTM =   convert(Datetime, Column12),
CPAST_REFNO =  convert(INT,Column13),
COMMENTS =  Column14,
CREATE_DTTM =   convert(Datetime, Column15),
MODIF_DTTM =   convert(Datetime, Column16),
USER_CREATE =  Column17,
USER_MODIF =  Column18,
ARCHV_FLAG =  Column19,
STRAN_REFNO =  convert(INT,Column20),
EXTERNAL_KEY =  Column21,
DGPRO_REFNO =  convert(INT,Column22),
PROVIDED_BY =  Column23,
OBJECTIVE =  Column24,
ACTUAL_END_DTTM =   convert(Datetime, Column25),
PLAN_ACTIVITY_FLAG =  Column26,
SORCE_CODE =  Column27,
SORCE_REFNO =  convert(INT,Column28),
OWNER_HEORG_REFNO = convert(INT, Column29),
Created =Created

From dbo.TImportCSVParsed
where Column0 like 'RM2_GE_OA_PCACT_%'
and Valid = 1




/*************************************
PATIENT_CAREPLAN_HISTORIES
*************************************************/