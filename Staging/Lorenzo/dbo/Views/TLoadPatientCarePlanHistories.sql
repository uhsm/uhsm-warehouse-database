﻿CREATE View [dbo].[TLoadPatientCarePlanHistories]
AS
Select PCHST_REFNO =   convert(INT, Column5),
PATCP_REFNO =  convert(INT,  Column6),
AUDIT_IND =  Column7,
SORCE_CODE =  Column8,
SORCE_REFNO =   convert(INT, Column9),
DESCRIPTION =  Column10,
CREATE_DTTM =   convert(Datetime, Column11),
MODIF_DTTM =    convert(Datetime,Column12),
USER_CREATE =  Column13,
USER_MODIF =  Column14,
ARCHV_FLAG =  Column15,
STRAN_REFNO =  convert(INT, Column16),
EXTERNAL_KEY =  Column17,
OWNER_HEORG_REFNO =   convert(INT, Column18),
Created =Created

From dbo.TImportCSVParsed
where Column0 like 'RM2_GE_OA_PCHST_%'
and Valid = 1