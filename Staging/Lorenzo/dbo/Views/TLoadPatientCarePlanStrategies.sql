﻿/*************************************
PATIENT_CAREPLAN_Strategies
*************************************************/


CREATE View [dbo].[TLoadPatientCarePlanStrategies]
AS
Select PCPST_REFNO =  convert(INT, Column5),
PATCP_REFNO =  convert(INT, Column6),
CPSTR_REFNO =   convert(INT,Column7),
SELECTED_FLAG =  Column8,
CREATE_DTTM =   convert(Datetime, Column9),
MODIF_DTTM =   convert(Datetime, Column10),
USER_CREATE =  Column11,
USER_MODIF =  Column12,
ARCHV_FLAG =  Column13,
STRAN_REFNO =   convert(INT,Column14),
EXTERNAL_KEY =  Column15,
OWNER_HEORG_REFNO =   convert(INT,Column16),
Created = Created

 From dbo.TImportCSVParsed
where Column0 like 'RM2_GE_OA_PCPST_%'
and Valid = 1