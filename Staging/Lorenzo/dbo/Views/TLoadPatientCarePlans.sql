﻿/*************************************
PATIENT_CAREPLANS
*************************************************/

CREATE View [dbo].[TLoadPatientCarePlans] 
AS 
Select PATCP_REFNO =   convert(INT,Column5),
PATNT_REFNO =  convert(INT, Column6),
REFRL_REFNO =   convert(INT,Column7),
CODE =  Column8,
DESCRIPTION =  Column9,
OBJECTIVE =  Column10,
START_DTTM =   convert(Datetime, Column11),
END_DTTM =   convert(Datetime, Column12),
CPSTS_REFNO =   convert(INT,Column13),
PROCA_REFNO =   convert(INT,Column14),
STEAM_REFNO =   convert(INT,Column15),
SPECT_REFNO =   convert(INT,Column16),
CPOCM_REFNO =   convert(INT,Column17),
CREATE_DTTM =   convert(Datetime, Column18),
MODIF_DTTM =   convert(Datetime, Column19),
USER_CREATE =  Column20,
USER_MODIF =  Column21,
ARCHV_FLAG =  Column22,
STRAN_REFNO =  Column23,
EXTERNAL_KEY =  Column24,
ACLEV_REFNO =   convert(INT,Column25),
OWNER_HEORG_REFNO =   convert(INT,Column26),
Created = Created

From dbo.TImportCSVParsed
where Column0 like 'RM2_GE_OA_PATCP_%'
and Valid = 1