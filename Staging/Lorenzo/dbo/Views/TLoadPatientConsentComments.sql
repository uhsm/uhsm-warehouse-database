﻿/*************************************
PATIENT_CONSENT_COMMENTS
*************************************************/

CREATE View [dbo].[TLoadPatientConsentComments]
AS
Select PATCT_REFNO =  convert(INT,Column5),
PATNT_REFNO =  convert(INT,Column6),
CSTST_REFNO =  convert(INT,Column7),
COMMENTS =  Column8,
START_DTTM =   Convert(Datetime,Column9),
END_DTTM =   Convert(Datetime,Column10),
CREATE_DTTM =   Convert(Datetime,Column11),
MODIF_DTTM =   Convert(Datetime,Column12),
USER_CREATE =  Column13,
USER_MODIF =  Column14,
ARCHV_FLAG =  Column15,
STRAN_REFNO =  convert(INT,Column16),
PRIOR_POINTER =  convert(INT,Column17),
EXTERNAL_KEY =  Column18,
OWNER_HEORG_REFNO =  convert(INT,Column19),
Created = Created

From dbo.TImportCSVParsed
where Column0 like 'RM2_GE_PD_PATCT_%'
and Valid = 1