﻿CREATE view [dbo].[TLoadPatientIdentifier] as

select
	 [PATID_REFNO] = convert(numeric,Column5)
	,[PITYP_REFNO] = convert(numeric,Column6)
	,[PATNT_REFNO] = convert(numeric,Column7)
	,[IDENTIFIER] = Column8
	,[RDPRJ_REFNO] = convert(numeric,Column9)
	,[END_DTTM] = convert(datetime,Column10)
	,[START_DTTM] = convert(datetime,Column11)
	,[CURNT_FLAG] = Column12
	,[PISTS_REFNO] = convert(numeric,Column13)
	,[SRANK_REFNO] = convert(numeric,Column14)
	,[CREATE_DTTM] = convert(datetime,Column15)
	,[MODIF_DTTM] = convert(datetime,Column16)
	,[USER_CREATE] = Column17
	,[USER_MODIF] = Column18
	,[ARCHV_FLAG] = Column19
	,[STRAN_REFNO] = convert(numeric,Column20)
	,[PRIOR_POINTER] = convert(numeric,Column21)
	,[EXTERNAL_KEY] = Column22
	,[IDSUF_REFNO] = convert(numeric,Column23)
	,[HEORG_REFNO] = convert(numeric,Column24)
	,[MRD_HEORG_REFNO] = convert(numeric,Column25)
	,[OWNER_HEORG_REFNO] = convert(numeric,Column26)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_PATID_MP_%'
and	Valid = 1