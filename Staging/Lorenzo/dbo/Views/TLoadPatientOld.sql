﻿CREATE view [dbo].[TLoadPatientOld] as

select
	 [PATNT_REFNO] = convert(numeric,Column5)
	,[PATNT_REFNO_PASID] = Column6
	,[PATNT_REFNO_NHS_IDENTIFIER] = Column7
	,[SEXXX_REFNO] = convert(numeric,Column8)
	,[SEXXX_REFNO_MAIN_CODE] = Column9
	,[SEXXX_REFNO_DESCRIPTION] = Column10
	,[FORENAME] = Column11
	,[TITLE_REFNO] = convert(numeric,Column12)
	,[TITLE_REFNO_MAIN_CODE] = Column13
	,[TITLE_REFNO_DESCRIPTION] = Column14
	,[SURNAME] = Column15
	,[ETHGR_REFNO] = convert(numeric,Column16)
	,[ETHGR_REFNO_MAIN_CODE] = Column17
	,[ETHGR_REFNO_DESCRIPTION] = Column18
	,[MARRY_REFNO] = convert(numeric,Column19)
	,[MARRY_REFNO_MAIN_CODE] = Column20
	,[MARRY_REFNO_DESCRIPTION] = Column21
	,[DTTM_OF_BIRTH] = convert(datetime,Column22)
	,[OCCUP_REFNO] = convert(numeric,Column23)
	,[OCCUP_REFNO_MAIN_CODE] = Column24
	,[OCCUP_REFNO_DESCRIPTION] = Column25
	,[SPOKL_REFNO] = convert(numeric,Column26)
	,[SPOKL_REFNO_MAIN_CODE] = Column27
	,[SPOKL_REFNO_DESCRIPTION] = Column28
	,[CHRON_SICK] = Column29
	,[DISABLED] = Column30
	,[DECSD_FLAG] = Column31
	,[CONFIDENTIAL] = Column32
	,[DTTM_OF_DEATH] = convert(datetime,Column33)
	,[PLACE_OF_BIRTH] = Column34
	,[PLACE_OF_DEATH] = Column35
	,[RELIG_REFNO] = convert(numeric,Column36)
	,[RELIG_REFNO_MAIN_CODE] = Column37
	,[RELIG_REFNO_DESCRIPTION] = Column38
	,[DEATH_NOTIFIED_DTTM] = convert(datetime,Column39)
	,[CREATE_DTTM] = convert(datetime,Column40)
	,[MODIF_DTTM] = convert(datetime,Column41)
	,[USER_CREATE] = Column42
	,[USER_MODIF] = Column43
	,[ARCHV_FLAG] = Column44
	,[TEMP_PAS_ID] = Column45
	,[BIRTH_DTTM_ESTIMATE_FLAG] = Column46
	,[INTRP_REQD_FLAG] = Column47
	,[REBES_REFNO] = convert(numeric,Column48)
	,[REBES_REFNO_MAIN_CODE] = Column49
	,[REBES_REFNO_DESCRIPTION] = Column50
	,[REGIS_DTTM] = convert(datetime,Column51)
	,[MULTB_REFNO] = convert(numeric,Column52)
	,[MULTB_REFNO_MAIN_CODE] = Column53
	,[MULTB_REFNO_DESCRIPTION] = Column54
	,[CNTST_REFNO] = convert(numeric,Column55)
	,[CNTST_REFNO_MAIN_CODE] = Column56
	,[CNTST_REFNO_DESCRIPTION] = Column57
	,[APDTH_REFNO] = convert(numeric,Column58)
	,[APDTH_REFNO_MAIN_CODE] = Column59
	,[APDTH_REFNO_DESCRIPTION] = Column60
	,[CASLT_REFNO] = convert(numeric,Column61)
	,[CASLT_REFNO_MAIN_CODE] = Column62
	,[CASLT_REFNO_DESCRIPTION] = Column63
	,[DEATH_DTTM_ESTIMATE_FLAG] = Column64
	,[NNNTS_CODE] = Column65
	,[NATNL_REFNO] = convert(numeric,Column66)
	,[NATNL_REFNO_MAIN_CODE] = Column67
	,[NATNL_REFNO_DESCRIPTION] = Column68
	,[MERGE_MINOR_FLAG] = Column69
	,[CCDTH_REFNO] = convert(numeric,Column70)
	,[CCDTH_REFNO_MAIN_CODE] = Column71
	,[CCDTH_REFNO_DESCRIPTION] = Column72
	,[IMMED_CODTH_ODPCD_REFNO] = convert(numeric,Column73)
	,[CONDT_CODTH_ODPCD_REFNO] = convert(numeric,Column74)
	,[UNCON_CODTH_ODPCD_REFNO] = convert(numeric,Column75)
	,[SGNIF_CODTH_ODPCD_REFNO] = convert(numeric,Column76)
	,[CODTH_DISCREPANCY] = Column77
	,[OWNER_HEORG_REFNO] = convert(numeric,Column78)
	,[OWNER_HEORG_REFNO_MAIN_IDENT] = Column79
	,[PDS_UPDATE_NEEDED] = Column80
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_PATNT_MP_%'
and	Valid = 1