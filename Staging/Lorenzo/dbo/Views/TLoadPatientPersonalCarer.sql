﻿CREATE view [dbo].[TLoadPatientPersonalCarer] as

select
	 [PAPCA_REFNO] = convert(numeric,Column5)
	,[PETYP_REFNO] = convert(numeric,Column6)
	,[RELTN_REFNO] = convert(numeric,Column7)
	,[PERCA_REFNO] = convert(numeric,Column8)
	,[PATNT_REFNO] = convert(numeric,Column9)
	,[START_DTTM] = convert(datetime,Column10)
	,[END_DTTM] = convert(datetime,Column11)
	,[CURNT_FLAG] = Column12
	,[CREATE_DTTM] = convert(datetime,Column13)
	,[MODIF_DTTM] = convert(datetime,Column14)
	,[USER_CREATE] = Column15
	,[USER_MODIF] = Column16
	,[CNTCT_ALLOWED] = Column17
	,[ARCHV_FLAG] = Column18
	,[STRAN_REFNO] = convert(numeric,Column19)
	,[PRIOR_POINTER] = convert(numeric,Column20)
	,[EXTERNAL_KEY] = Column21
	,[INFOR_REFNO] = convert(numeric,Column22)
	,[LGRSP_REFNO] = convert(numeric,Column23)
	,[LEGAL_RESPONSIBILITY] = Column24
	,[USE_PATIENT_ADDRESS] = Column25
	,[SEND_PATNT_CORRES_TO] = Column26
	,[OWNER_HEORG_REFNO] = convert(numeric,Column27)
	,[BABY_LINK] = Column28
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_PATNT_PERCA_PC_%'
and	Valid = 1