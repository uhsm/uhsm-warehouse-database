﻿CREATE view [dbo].[TLoadPatientProfessionalCarer] as

select
	 [PATPC_REFNO] = convert(numeric,Column5)
	,[PRTYP_REFNO] = convert(numeric,Column6)
	,[PRTYP_MAIN_CODE] = Column7
	,[PRTYP_DESCRIPTION] = Column8
	,[PROCA_REFNO] = convert(numeric,Column9)
	,[PROCA_MAIN_IDENT] = Column10
	,[PATNT_REFNO] = convert(numeric,Column11)
	,[PATNT_PASID] = Column12
	,[PATNT_NHS_IDENTIFIER] = Column13
	,[HEORG_REFNO] = convert(numeric,Column14)
	,[HEORG_MAIN_IDENT] = Column15
	,[START_DTTM] = convert(datetime,Column16)
	,[END_DTTM] = convert(datetime,Column17)
	,[CREATE_DTTM] = convert(datetime,Column18)
	,[MODIF_DTTM] = convert(datetime,Column19)
	,[USER_CREATE] = Column20
	,[USER_MODIF] = Column21
	,[ARCHV_FLAG] = Column22
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_PATPC_MP_%'
and	Valid = 1