﻿/*************************************
PATIENT_TRANSPORTATIONS
*************************************************/
CREATE View [dbo].[TLoadPatientTransportations]
as
Select PATRN_REFNO =  convert(INT,Column5),
SORCE_CODE =  Column6,
SORCE_REFNO =  convert(INT,Column7),
ARRIV_DURATION =  convert(INT,Column8),
ARRIV_TRANS_REFNO =  convert(INT,Column9),
DEPRT_TRANS_REFNO =  convert(INT,Column10),
ARRIV_TRSTA_REFNO =  convert(INT,Column11),
DEPRT_TRSTA_REFNO =  convert(INT,Column12),
ARRIV_HEORG_REFNO =  convert(INT,Column13),
DEPRT_HEORG_REFNO =  convert(INT,Column14),
USER_CREATE =  Column15,
USER_MODIF =  Column16,
CREATE_DTTM =  convert(Datetime,Column17),
MODIF_DTTM =  convert(Datetime,Column18),
STRAN_REFNO =  convert(INT,Column19),
ARCHV_FLAG =  Column20,
EXTERNAL_KEY =  Column21,
FROM_HEORG_REFNO =  convert(INT,Column22),
TO_HEORG_REFNO =  convert(INT,Column23),
OWNER_HEORG_REFNO =  convert(INT,Column24),
Created = Created


From dbo.TImportCSVParsed
where Column0 like 'RM2_GE_OA_PATRN_%'
and Valid = 1