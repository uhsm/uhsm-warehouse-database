﻿CREATE view [dbo].[TLoadPeriodOfCare] as

select
	 [POCAR_REFNO] = convert(numeric,Column5)
	,[REASN_REFNO] = convert(numeric,Column6)
	,[REASN_REFNO_MAIN_CODE] = Column7
	,[REASN_REFNO_DESCRIPTION] = Column8
	,[PROCA_REFNO] = convert(numeric,Column9)
	,[PROCA_REFNO_MAIN_IDENT] = Column10
	,[PATNT_REFNO] = convert(numeric,Column11)
	,[PATNT_REFNO_PASID] = Column12
	,[PATNT_REFNO_NHS_IDENTIFIER] = Column13
	,[START_DTTM] = convert(datetime,Column14)
	,[END_DTTM] = convert(datetime,Column15)
	,[WAIT_END_DTTM] = convert(datetime,Column16)
	,[USER_CREATE] = Column17
	,[USER_MODIF] = Column18
	,[CREATE_DTTM] = convert(datetime,Column19)
	,[MODIF_DTTM] = convert(datetime,Column20)
	,[ARCHV_FLAG] = Column21
	,[OWNER_HEORG_REFNO] = convert(numeric,Column22)
	,[OWNER_HEORG_REFNO_MAIN_IDENT] = Column23
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_POCAR_IP_%'
and	Valid = 1