﻿CREATE view [dbo].[TLoadPersonalCarer] as

select
	 [PERCA_REFNO] = convert(numeric,Column5)
	,[TITLE_REFNO] = convert(numeric,Column6)
	,[SEXXX_REFNO] = convert(numeric,Column7)
	,[PATNT_REFNO] = convert(numeric,Column8)
	,[FORENAME] = Column9
	,[SURNAME] = Column10
	,[DATE_OF_BIRTH] = convert(datetime,Column11)
	,[CREATE_DTTM] = convert(datetime,Column12)
	,[MODIF_DTTM] = convert(datetime,Column13)
	,[USER_CREATE] = Column14
	,[USER_MODIF] = Column15
	,[ARCHV_FLAG] = Column16
	,[STRAN_REFNO] = convert(numeric,Column17)
	,[PRIOR_POINTER] = convert(numeric,Column18)
	,[EXTERNAL_KEY] = Column19
	,[SPOKL_REFNO] = convert(numeric,Column20)
	,[INTRP_REQD_FLAG] = Column21
	,[SNDEX_FORENAME] = Column22
	,[SNDEX_SURNAME] = Column23
	,[UPPER_FORENAME] = Column24
	,[UPPER_SURNAME] = Column25
	,[PREFRD_FORENAME] = Column26
	,[UPPER_PREFRD_FORENAME] = Column27
	,[SNDEX_PREFRD_FORENAME] = Column28
	,[SECOND_FORENAME] = Column29
	,[UPPER_SECOND_FORENAME] = Column30
	,[SNDEX_SECOND_FORENAME] = Column31
	,[THIRD_FORENAME] = Column32
	,[UPPER_THIRD_FORENAME] = Column33
	,[SNDEX_THIRD_FORENAME] = Column34
	,[MPICR_REFNO] = convert(numeric,Column35)
	,[IS_CARER_FLAG] = Column36
	,[OWNER_HEORG_REFNO] = convert(numeric,Column37)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_PERS_CARER_PC_%'
and	Valid = 1