﻿/***************************************************
PROF_CARER_HEALTH_ORGS
*************************************************/


CREATE View [dbo].[TLoadProfCarerHealthOrgs]
AS 
Select PCAHO_REFNO =  convert(INT,Column5),
PROCA_REFNO =  convert(INT,Column6),
HEORG_REFNO = convert(INT, Column7),
END_DTTM =  convert(Datetime,Column8),
START_DTTM =  convert(Datetime,Column9),
CURNT_FLAG =  Column10,
PROVIDER_NUMBER =  Column11,
CREATE_DTTM =  convert(Datetime,Column12),
MODIF_DTTM =  convert(Datetime,Column13),
USER_CREATE =  Column14,
USER_MODIF =  Column15,
ARCHV_FLAG =  Column16,
STRAN_REFNO =  convert(INT,Column17),
PRIOR_POINTER =  convert(INT,Column18),
EXTERNAL_KEY =  Column19,
COMMENTS =  Column20,
CHTYP_REFNO =  convert(INT,Column21),
Lead_GP_Flag =  Column22,
OWNER_HEORG_REFNO =  convert(INT,Column23),
Created = Created

from TImportCSVParsed
where Column0 like 'RM2_GE_RF_PCAHO_%'
and Valid = 1