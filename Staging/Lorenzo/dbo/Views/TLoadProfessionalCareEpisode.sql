﻿CREATE view [dbo].[TLoadProfessionalCareEpisode] as

select
	 [PRCAE_REFNO] = convert(int,Column5)
	,[WLIST_REFNO] = convert(numeric,Column6)
	,[SSTAY_REFNO] = convert(numeric,Column7)
	,[PRVSP_REFNO] = convert(numeric,Column8)
	,[SSTAY_SPONT_REFNO] = convert(int,Column9)
	,[PROVD_REFNO] = convert(int,Column10)
	,[SPECT_REFNO] = convert(int,Column11)
	,[PURCH_REFNO] = convert(int,Column12)
	,[SORRF_REFNO] = convert(int,Column13)
	,[EPISO_REFNO] = convert(int,Column14)
	,[INMGT_REFNO] = convert(int,Column15)
	,[ADCAT_REFNO] = convert(int,Column16)
	,[EPTYP_REFNO] = convert(int,Column17)
	,[MENCT_REFNO] = convert(int,Column18)
	,[CONTR_REFNO] = convert(int,Column19)
	,[ATTND_REFNO] = convert(int,Column20)
	,[PATCL_REFNO] = convert(int,Column21)
	,[CEOCM_REFNO] = convert(int,Column22)
	,[INLOC_REFNO] = convert(int,Column23)
	,[PAGRP_REFNO] = convert(int,Column24)
	,[REFRL_REFNO] = convert(int,Column25)
	,[LEGSC_REFNO] = convert(int,Column26)
	,[PROCA_REFNO] = convert(int,Column27)
	,[PATNT_REFNO] = convert(int,Column28)
	,[PAYMT_REFNO] = convert(int,Column29)
	,[VISIT_REFNO] = convert(int,Column30)
	,[START_DTTM] = convert(smalldatetime,Column31)
	,[END_DTTM] = convert(smalldatetime,Column32)
	,[DNARS_REFNO] = convert(int,Column33)
	,[ADMET_REFNO] = convert(int,Column34)
	,[PRIOR_REFNO] = convert(int,Column35)
	,[SPXRL_REFNO] = convert(int,Column36)
	,[COMMENTS] = Column37
	,[CREATE_DTTM] = convert(datetime,Column38)
	,[MODIF_DTTM] = convert(datetime,Column39)
	,[USER_CREATE] = Column40
	,[USER_MODIF] = Column41
	,[ARCHV_FLAG] = Column42
	,[IDENTIFIER] = Column43
	,[STRAN_REFNO] = convert(numeric,Column44)
	,[PRIOR_POINTER] = convert(numeric,Column45)
	,[EXTERNAL_KEY] = Column46
	,[CODING_COMPLETE_FLAG] = Column47
	,[AGEBD_REFNO] = convert(int,Column48)
	,[MHCEP_REFNO] = convert(int,Column49)
	,[HRG_CODE] = Column50
	,[CTRSV_REFNO] = convert(int,Column51)
	,[SPONT_REFNO] = convert(int,Column52)
	,[ALLCN_OVERRIDE_FLAG] = Column53
	,[SUMMARY_COMPLETE_FLAG] = Column54
	,[PRVSN_FLAG] = Column55
	,[CCCCC_REFNO] = convert(int,Column56)
	,[ADMOF_REFNO] = convert(int,Column57)
	,[RECTR_REFNO] = convert(int,Column58)
	,[CSTAT_REFNO] = convert(int,Column59)
	,[COD_AUTH_FLAG] = Column60
	,[COD_AUTH_USER] = Column61
	,[COD_AUTH_DTTM] = convert(smalldatetime,Column62)
	,[LATCH_AUTH_FLAG] = Column63
	,[INVOICE_TAG] = Column64
	,[SVCAT_REFNO] = convert(int,Column65)
	,[PTCLS_REFNO] = convert(int,Column66)
	,[RESET_PTCLS_DAYS] = Column67
	,[PRIOR_PTCLS_DAYS] = convert(int,Column68)
	,[ICU_HOURS] = convert(int,Column69)
	,[CCU_HOURS] = convert(int,Column70)
	,[MECH_VENT_HRS] = convert(int,Column71)
	,[PALST_REFNO] = convert(int,Column72)
	,[DGPRO_REFNO] = convert(int,Column73)
	,[PTCLS_MANUAL_FLAG] = Column74
	,[DDGVP_CODE] = Column75
	,[WIES_CMX_VALUE] = convert(numeric,Column76)
	,[SORCE_CODE] = Column77
	,[SORCE_REFNO] = convert(numeric,Column78)
	,[HRG_VERSION] = Column79
	,[OWNER_HEORG_REFNO] = convert(int,Column80)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_IP_PRCAE_%'
and	Valid = 1