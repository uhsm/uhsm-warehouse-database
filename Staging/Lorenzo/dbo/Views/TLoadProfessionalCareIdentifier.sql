﻿CREATE view [dbo].[TLoadProfessionalCareIdentifier] as

select
	 [PRCAI_REFNO] = convert(int,Column5)
	,[CITYP_REFNO] = convert(int,Column6)
	,[PROCA_REFNO] = convert(int,Column7)
	,[IDENTIFIER] = Column8
	,[START_DTTM] = convert(smalldatetime,Column9)
	,[END_DTTM] = convert(smalldatetime,Column10)
	,[CREATE_DTTM] = convert(datetime,Column11)
	,[MODIF_DTTM] = convert(datetime,Column12)
	,[USER_CREATE] = Column13
	,[USER_MODIF] = Column14
	,[CURNT_FLAG] = Column15
	,[ARCHV_FLAG] = Column16
	,[STRAN_REFNO] = convert(numeric,Column17)
	,[PRIOR_POINTER] = convert(int,Column18)
	,[EXTERNAL_KEY] = Column19
	,[OWNER_HEORG_REFNO] = convert(int,Column20)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_PROCA_ID_RF_%'
and	Valid = 1