﻿CREATE view [dbo].[TLoadProfessionalCarer] as

select
	 [PROCA_REFNO] = convert(numeric,Column5)
	,[PROCA_REFNO_MAIN_IDENT] = Column6
	,[PRTYP_REFNO] = convert(numeric,Column7)
	,[PRTYP_REFNO_MAIN_CODE] = Column8
	,[PRTYP_REFNO_DESCRIPTION] = Column9
	,[FORENAME] = Column10
	,[TITLE_REFNO] = convert(numeric,Column11)
	,[TITLE_REFNO_MAIN_CODE] = Column12
	,[TITLE_REFNO_DESCRIPTION] = Column13
	,[GRADE_REFNO] = convert(numeric,Column14)
	,[GRADE_REFNO_MAIN_CODE] = Column15
	,[GRADE_REFNO_DESCRIPTION] = Column16
	,[SURNAME] = Column17
	,[SEXXX_REFNO] = convert(numeric,Column18)
	,[SEXXX_REFNO_MAIN_CODE] = Column19
	,[SEXXX_REFNO_DESCRIPTION] = Column20
	,[DATE_OF_BIRTH] = convert(datetime,Column21)
	,[CONTR_HOURS] = convert(numeric,Column22)
	,[LEAVE_ENTITLEMENT] = convert(numeric,Column23)
	,[PLACE_OF_BIRTH] = Column24
	,[QUALIFICATIONS] = Column25
	,[GLOVE_INITM_REFNO] = convert(numeric,Column26)
	,[GLOVE_INITM_REFNO_MAIN_CODE] = Column27
	,[GLOVE_INITM_REFNO_DESCRIPTION] = Column28
	,[SKNPR_INITM_REFNO] = convert(numeric,Column29)
	,[SKNPR_INITM_REFNO_MAIN_CODE] = Column30
	,[SKNPR_INITM_REFNO_DESCRIPTION] = Column31
	,[DEF_PATH_SPONT_REFNO] = convert(numeric,Column32)
	,[DEF_PATH_SPONT_REFNO_CODE] = Column33
	,[DEF_PATH_SPONT_REFNO_NAME] = Column34
	,[START_DTTM] = convert(datetime,Column35)
	,[END_DTTM] = convert(datetime,Column36)
	,[DESCRIPTION] = Column37
	,[CREATE_DTTM] = convert(datetime,Column38)
	,[MODIF_DTTM] = convert(datetime,Column39)
	,[USER_CREATE] = Column40
	,[USER_MODIF] = Column41
	,[LOCAL_FLAG] = Column42
	,[MAIN_IDENT] = Column43
	,[ARCHV_FLAG] = Column44
	,[PRIMARY_STEAM_REFNO] = convert(numeric,Column45)
	,[PRIMARY_STEAM_REFNO_CODE] = Column46
	,[PRIMARY_STEAM_REFNO_NAME] = Column47
	,[OWNER_HEORG_REFNO] = convert(numeric,Column48)
	,[OWNER_HEORG_REFNO_MAIN_IDENT] = Column49
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_PROCA_RF_%'
and	Valid = 1