﻿CREATE view [dbo].[TLoadProfessionalCarerMap] as

select
	 [PRCMP_REFNO] = convert(int,Column5)
	,[PROCA_REFNO] = convert(int,Column6)
	,[STEAM_REFNO] = convert(int,Column7)
	,[SPONT_REFNO] = convert(int,Column8)
	,[SPSSN_REFNO] = convert(int,Column9)
	,[SCHDL_REFNO] = convert(numeric,Column10)
	,[PRROL_REFNO] = convert(int,Column11)
	,[START_DTTM] = convert(smalldatetime,Column12)
	,[END_DTTM] = convert(smalldatetime,Column13)
	,[CREATE_DTTM] = convert(datetime,Column14)
	,[MODIF_DTTM] = convert(datetime,Column15)
	,[USER_CREATE] = Column16
	,[USER_MODIF] = Column17
	,[ARCHV_FLAG] = Column18
	,[STRAN_REFNO] = convert(numeric,Column19)
	,[EXTERNAL_KEY] = Column20
	,[REFRL_REFNO] = convert(int,Column21)
	,[WLRUL_REFNO] = convert(int,Column22)
	,[SPECT_REFNO] = convert(int,Column23)
	,[PCRTP_REFNO] = convert(int,Column24)
	,[ROLE_ORDER] = convert(numeric,Column25)
	,[OWNER_HEORG_REFNO] = convert(int,Column26)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_PROCA_MAP_RF_%'
and	Valid = 1