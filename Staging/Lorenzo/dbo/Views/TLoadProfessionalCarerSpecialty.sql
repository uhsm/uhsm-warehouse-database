﻿CREATE view [dbo].[TLoadProfessionalCarerSpecialty] as

select
	 [PRCAS_REFNO] = convert(int,Column5)
	,[SPECT_REFNO] = convert(int,Column6)
	,[CSTYP_REFNO] = convert(int,Column7)
	,[PROCA_REFNO] = convert(int,Column8)
	,[START_DTTM] = convert(smalldatetime,Column9)
	,[END_DTTM] = convert(smalldatetime,Column10)
	,[CREATE_DTTM] = convert(datetime,Column11)
	,[MODIF_DTTM] = convert(datetime,Column12)
	,[USER_CREATE] = Column13
	,[USER_MODIF] = Column14
	,[ARCHV_FLAG] = Column15
	,[STRAN_REFNO] = convert(numeric,Column16)
	,[PRIOR_POINTER] = convert(int,Column17)
	,[EXTERNAL_KEY] = Column18
	,[PBK_LEAD_TIME] = convert(int,Column19)
	,[PBK_LEAD_TIME_UNITS] = Column20
	,[OWNER_HEORG_REFNO] = convert(int,Column21)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_PROCA_SPECT_RF_%'
and	Valid = 1