﻿CREATE view [dbo].[TLoadProfessionalCarerStaffTeam] as

select
	 [PCSTT_REFNO] = convert(int,Column5)
	,[PRROL_REFNO] = convert(int,Column6)
	,[STEAM_REFNO] = convert(int,Column7)
	,[PROCA_REFNO] = convert(int,Column8)
	,[START_DTTM] = convert(smalldatetime,Column9)
	,[END_DTTM] = convert(smalldatetime,Column10)
	,[CREATE_DTTM] = convert(datetime,Column11)
	,[MODIF_DTTM] = convert(datetime,Column12)
	,[USER_CREATE] = Column13
	,[USER_MODIF] = Column14
	,[ARCHV_FLAG] = Column15
	,[STRAN_REFNO] = convert(numeric,Column16)
	,[EXTERNAL_KEY] = Column17
	,[OWNER_HEORG_REFNO] = convert(int,Column18)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_PROCA_STEAM_RF_%'
and	Valid = 1