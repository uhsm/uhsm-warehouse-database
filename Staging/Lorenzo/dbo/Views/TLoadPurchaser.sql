﻿CREATE view [dbo].[TLoadPurchaser] as

/*******************************************************************
Updated 2011-03-18 by KD
	Changed import spec for Purchaser table from LE22 to MR1
*******************************************************************/

select
	 [PURCH_REFNO] = Convert(int,Column5)
	,[PUTYP_REFNO] = Convert(int,Column6)
	,[PUTYP_MAIN_CODE] = Column7
	,[PUTYP_DESCRIPTION] = Column8
	,[PLATP_REFNO] = Convert(int,Column9)
	,[PLATP_MAIN_CODE] = Column10
	,[PLATP_DESCRIPTION] = Column11
	,[MAIN_IDENT] = Column12
	,[DESCRIPTION] = Column13
	,[START_DATE] = Convert(smalldatetime,14)
	,[END_DATE] = Convert(smalldatetime,Column15)
	,[GPFHL_REFNO] = Convert(int,Column16)
	,[GPFHL_MAIN_CODE] = Column17
	,[GPFHL_DESCRIPTION] = Column18
	,[MAIN_HEORG_REFNO] = Convert(int,Column19)
	,[MAIN_HEORG_REFNO_MAIN_IDENT] = Column20
	,[INSEQ_REFNO] = Convert(int,Column21)
	,[INSEQ_MAIN_CODE] = Column22
	,[INSEQ_DESCRIPTION] = Column23
	,[INFRQ_REFNO] = Convert(int,Column24)
	,[INFRQ_MAIN_CODE] = Column25
	,[INFRQ_DESCRIPTION] = Column26
	,[USER_CREATE] = Column27
	,[USER_MODIF] = Column28
	,[CREATE_DTTM] = Convert(datetime,Column29)
	,[MODIF_DTTM] = Convert(datetime,Column30)
	,[ARCHV_FLAG] = Convert(char,Column31)
	,[IVGRP_REFNO] = Convert(int,Column32)
	,[IVGRP_MAIN_CODE] = Column33
	,[IVGRP_DESCRIPTION] = Column34
	,[IVDET_REFNO] = Convert(int,Column35)
	,[IVDET_MAIN_CODE] = Column36
	,[IVDET_DESCRIPTION] = Column37
	,[INVFM_REFNO] = Convert(int,Column38)
	,[INVFM_MAIN_CODE] = Column39
	,[INVFM_DESCRIPTION] = Column40
	,[PURCH_DEBT_FLAG] = Convert(char,Column41)
	,[SCLVL_REFNO] = Convert(int,Column42)
	,[SCLVL_MAIN_CODE] = Column43
	,[SCLVL_DESCRIPTION] = Column44
	,[DIAGN_CCSXT_REFNO] = Convert(int,Column45)
	,[PURGP_REFNO] = Convert(int,Column46)
	,[PURGP_MAIN_CODE] = Column47
	,[PURGP_DESCRIPTION] = Column48
	,[PSTYP_REFNO] = Convert(int,Column49)
	,[PSTYP_MAIN_CODE] = Column50
	,[PSTYP_DESCRIPTION] = Column51
	,[OWNER_HEORG_REFNO] = Convert(int,Column52)
	,[OWNER_HEORG_REFNO_MAIN_IDENT] = Column53
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_PURCHASERS_RF_%'
and	Valid = 1