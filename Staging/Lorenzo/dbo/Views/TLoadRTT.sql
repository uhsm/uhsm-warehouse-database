﻿CREATE view [dbo].[TLoadRTT] as

select
	 [RTTPR_REFNO] = convert(numeric,Column5)
	,[REFRL_REFNO] = convert(numeric,Column6)
	,[PATNT_REFNO] = convert(numeric,Column7)
	,[SORCE] = Column8
	,[SORCE_REFNO] = convert(numeric,Column9)
	,[RTTST_DATE] = convert(datetime,Column10)
	,[RTTST_REFNO] = convert(numeric,Column11)
	,[COMMENTS] = Column12
	,[CREATE_DTTM] = convert(datetime,Column13)
	,[MODIF_DTTM] = convert(datetime,Column14)
	,[USER_CREATE] = Column15
	,[USER_MODIF] = Column16
	,[ARCHV_FLAG] = Column17
	,[STRAN_REFNO] = convert(numeric,Column18)
	,[EXTERNAL_KEY] = Column19
	,[OWNER_HEORG_REFNO] = convert(numeric,Column20)
	,Created
from
	dbo.TImportCSVParsed
where
	--Column0 like 'RM2_GE_OA_REFTT_%'
	--KO changed 13/12/2012 when RTT extract renamed by CSC
	Column0 like 'RM2_GE_RT_REFTT_%'
and	Valid = 1