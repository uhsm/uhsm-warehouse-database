﻿/*************************************
RTT_HISTORIES
*************************************************/

CREATE View [dbo].[TLoadRTTHistories]
AS 
Select RTTHS_REFNO =  convert(INT,Column5),
RTTPR_REFNO =  convert(INT,Column6),
REFRL_REFNO =  convert(INT,Column7),
PATNT_REFNO =  convert(INT,Column8),
SORCE =  Column9,
SORCE_REFNO =  convert(INT,Column10),
RTTST_DATE =  convert(Datetime,  Column11),
RTTST_REFNO =  convert(INT,Column12),
COMMENTS =  Column13,
CREATE_DTTM =  convert(Datetime,  Column14),
MODIF_DTTM =  convert(Datetime,  Column15),
USER_CREATE =  Column16,
USER_MODIF =  Column17,
ARCHV_FLAG =  Column18,
STRAN_REFNO =  convert(INT,Column19),
EXTERNAL_KEY =  Column20,
OWNER_HEORG_REFNO =  convert(INT,Column21),
Created = Created

From dbo.TImportCSVParsed
where Column0 like 'RM2_GE_RT_RTTHS_%'
and Valid = 1