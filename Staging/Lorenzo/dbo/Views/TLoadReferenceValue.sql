﻿CREATE view [dbo].[TLoadReferenceValue] as

select
	 [RFVAL_REFNO] = convert(int,Column5)
	,[RFVDM_CODE] = Column6
	,[DESCRIPTION] = Column7
	,[MAIN_CODE] = Column8
	,[PARNT_REFNO] = convert(int,Column9)
	,[SMODE_VALUE] = Column10
	,[DEFAULT_VALUE] = Column11
	,[SORT_ORDER] = convert(numeric,Column12)
	,[SELECT_VALUE] = Column13
	,[DISPLAY_VALUE] = Column14
	,[CREATE_DTTM] = convert(datetime,Column15)
	,[MODIF_DTTM] = convert(datetime,Column16)
	,[USER_CREATE] = Column17
	,[USER_MODIF] = Column18
	,[RFTYP_CODE] = Column19
	,[ARCHV_FLAG] = Column20
	,[STRAN_REFNO] = convert(numeric,Column21)
	,[PRIOR_POINTER] = convert(int,Column22)
	,[EXTERNAL_KEY] = Column23
	,[START_DTTM] = convert(smalldatetime,Column24)
	,[END_DTTM] = convert(smalldatetime,Column25)
	,[LOW_VALUE] = Column26
	,[HIGH_VALUE] = Column27
	,[PROCESS_MODULE_ID] = Column28
	,[SCLVL_REFNO] = convert(int,Column29)
	,[SYN_CODE] = Column30
	,[OWNER_HEORG_REFNO] = convert(int,Column31)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_REFER_VALUE_RF_%'
and	Valid = 1