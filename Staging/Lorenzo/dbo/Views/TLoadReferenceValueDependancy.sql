﻿CREATE view [dbo].[TLoadReferenceValueDependancy] as

select
	 [RFVLD_REFNO] = convert(int,Column5)
	,[SORCE_REFNO] = convert(numeric,Column6)
	,[SORCE_CODE] = Column7
	,[START_DTTM] = convert(smalldatetime,Column8)
	,[END_DTTM] = convert(smalldatetime,Column9)
	,[RFVAL_REFNO] = convert(int,Column10)
	,[RFLTP_REFNO] = convert(int,Column11)
	,[USER_CREATE] = Column12
	,[USER_MODIF] = Column13
	,[CREATE_DTTM] = convert(datetime,Column14)
	,[MODIF_DTTM] = convert(datetime,Column15)
	,[STRAN_REFNO] = convert(numeric,Column16)
	,[EXTERNAL_KEY] = Column17
	,[ARCHV_FLAG] = Column18
	,[VALUE] = Column19
	,[OWNER_HEORG_REFNO] = convert(int,Column20)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_RF_RFVLD_%'
and	Valid = 1