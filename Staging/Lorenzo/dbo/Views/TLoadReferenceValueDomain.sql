﻿CREATE view [dbo].[TLoadReferenceValueDomain] as

select
	 [RFVDM_REFNO] = convert(int,Column5)
	,[RFVDM_CODE] = Column6
	,[DESCRIPTION] = Column7
	,[CREATE_DTTM] = convert(datetime,Column8)
	,[MODIF_DTTM] = convert(datetime,Column9)
	,[USER_CREATE] = Column10
	,[USER_MODIF] = Column11
	,[ARCHV_FLAG] = Column12
	,[STRAN_REFNO] = convert(numeric,Column13)
	,[PRIOR_POINTER] = convert(int,Column14)
	,[EXTERNAL_KEY] = Column15
	,[OWNER_HEORG_REFNO] = convert(int,Column16)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_RF_RFVDM_%'
and	Valid = 1