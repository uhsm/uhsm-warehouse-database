﻿CREATE view [dbo].[TLoadReferenceValueIdentifier] as

select
	 [RFVLI_REFNO] = convert(int,Column5)
	,[RFVAL_REFNO] = convert(int,Column6)
	,[IDENTIFIER] = Column7
	,[CREATE_DTTM] = convert(datetime,Column8)
	,[MODIF_DTTM] = convert(datetime,Column9)
	,[USER_CREATE] = Column10
	,[USER_MODIF] = Column11
	,[RITYP_CODE] = Column12
	,[ARCHV_FLAG] = Column13
	,[STRAN_REFNO] = convert(numeric,Column14)
	,[PRIOR_POINTER] = convert(int,Column15)
	,[EXTERNAL_KEY] = Column16
	,[OWNER_HEORG_REFNO] = convert(int,Column17)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_REFER_VALID_RF_%'
and	Valid = 1