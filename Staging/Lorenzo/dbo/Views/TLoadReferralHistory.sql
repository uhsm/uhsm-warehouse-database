﻿CREATE view [dbo].[TLoadReferralHistory] as

select
	 [REFHS_REFNO] = convert(int,Column5)
	,[STRAN_REFNO] = convert(numeric,Column6)
	,[sorce_code] = Column7
	,[PATNT_REFNO] = convert(int,Column8)
	,[REFRL_REFNO] = convert(int,Column9)
	,[DGPRO_REFNO] = convert(int,Column10)
	,[RFUNP_REFNO] = convert(int,Column11)
	,[start_dttm] = convert(smalldatetime,Column12)
	,[end_dttm] = convert(smalldatetime,Column13)
	,[Driving_table] = Column14
	,[User_create] = Column15
	,[Create_dttm] = convert(datetime,Column16)
	,[User_modif] = Column17
	,[MODIF_DTTM] = convert(datetime,Column18)
	,[ARCHV_FLAG] = Column19
	,[comments] = Column20
	,[OWNER_HEORG_REFNO] = convert(int,Column21)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_OA_REFHS_%'
and	Valid = 1