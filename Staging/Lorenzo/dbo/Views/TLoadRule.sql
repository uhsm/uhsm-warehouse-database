﻿CREATE view [dbo].[TLoadRule] as

select
	 [RULES_REFNO] = convert(int,Column5)
	,[STRAN_REFNO] = convert(numeric,Column6)
	,[CODE] = Column7
	,[DESCRIPTION] = Column8
	,[DEFLT_OPERATOR] = Column9
	,[DEFLT_VALUE] = Column10
	,[CREATE_DTTM] = convert(datetime,Column11)
	,[MODIF_DTTM] = convert(datetime,Column12)
	,[USER_CREATE] = Column13
	,[USER_MODIF] = Column14
	,[ARCHV_FLAG] = Column15
	,[PRIOR_POINTER] = convert(int,Column16)
	,[RFVDM_CODE] = Column17
	,[EXTERNAL_KEY] = Column18
	,[LOW_VALUE] = Column19
	,[HIGH_VALUE] = Column20
	,[RLTYP_CODE] = Column21
	,[PROCESS_MODULE_ID] = Column22
	,[ACTYP_REFNO] = convert(int,Column23)
	,[DATA_TYPE] = Column24
	,[BIND_TYPE] = Column25
	,[TABLS_ALIAS_NAME] = Column26
	,[TABLE_NAME] = Column27
	,[TABLE_COLUMN] = Column28
	,[DFTOP_REFNO] = convert(int,Column29)
	,[CDRUS_REFNO] = convert(int,Column30)
	,[DFTVL_REFNO] = convert(int,Column31)
	,[OWNER_HEORG_REFNO] = convert(int,Column32)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_RULES_DE_%'
and	Valid = 1