﻿CREATE view [dbo].[TLoadScheduleHistory] as

select
	 [SCDHS_REFNO] = convert(int,Column5)
	,[WLIST_REFNO] = convert(numeric,Column6)
	,[OLD_SPECT_REFNO] = convert(int,Column7)
	,[NEW_SPECT_REFNO] = convert(int,Column8)
	,[STRAN_REFNO] = convert(numeric,Column9)
	,[SCHDL_REFNO] = convert(numeric,Column10)
	,[OLD_START_DTTM] = convert(smalldatetime,Column11)
	,[OLD_END_DTTM] = convert(smalldatetime,Column12)
	,[NEW_START_DTTM] = convert(smalldatetime,Column13)
	,[NEW_END_DTTM] = convert(smalldatetime,Column14)
	,[MOVRN_REFNO] = convert(int,Column15)
	,[MOVE_DTTM] = convert(smalldatetime,Column16)
	,[SPSSN_REFNO] = convert(int,Column17)
	,[SPSSN_CODE] = Column18
	,[USER_CREATE] = Column19
	,[USER_MODIF] = Column20
	,[CREATE_DTTM] = convert(datetime,Column21)
	,[MODIF_DTTM] = convert(datetime,Column22)
	,[ARCHV_FLAG] = Column23
	,[PRIOR_POINTER] = convert(int,Column24)
	,[EXTERNAL_KEY] = Column25
	,[OLD_PROCA_REFNO] = convert(int,Column26)
	,[NEW_PROCA_REFNO] = convert(int,Column27)
	,[OLD_TRANS_REFNO] = convert(int,Column28)
	,[NEW_TRANS_REFNO] = convert(int,Column29)
	,[COMMENTS] = Column30
	,[MURQB_REFNO] = convert(numeric,Column31)
	,[NEW_SPSSN_REFNO] = convert(numeric,Column32)
	,[RTTST_REFNO] = convert(numeric,Column33)
	,[ERO_DTTM] = convert(datetime,Column34)
	,[OWNER_HEORG_REFNO] = convert(int,Column35)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_OA_SCDHS_%'
and	Valid = 1