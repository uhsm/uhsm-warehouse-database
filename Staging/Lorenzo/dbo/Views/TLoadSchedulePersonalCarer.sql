﻿CREATE view [dbo].[TLoadSchedulePersonalCarer] as

select
	 [SCHPC_REFNO] = convert(int,Column5)
	,[SCHDL_REFNO] = convert(numeric,Column6)
	,[PERCA_REFNO] = convert(int,Column7)
	,[DURATION] = convert(numeric,Column8)
	,[TUNIT_REFNO] = convert(int,Column9)
	,[CREATE_DTTM] = convert(datetime,Column10)
	,[MODIF_DTTM] = convert(datetime,Column11)
	,[USER_CREATE] = Column12
	,[USER_MODIF] = Column13
	,[ARCHV_FLAG] = Column14
	,[STRAN_REFNO] = convert(numeric,Column15)
	,[EXTERNAL_KEY] = Column16
	,[OWNER_HEORG_REFNO] = convert(int,Column17)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_OA_SCHPC_%'
and	Valid = 1