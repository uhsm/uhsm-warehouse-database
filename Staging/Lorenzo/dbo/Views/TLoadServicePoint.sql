﻿CREATE view [dbo].[TLoadServicePoint] as

select
	 [SPONT_REFNO] = convert(numeric,Column5)
	,[SPONT_REFNO_CODE] = Column6
	,[SPONT_REFNO_NAME] = Column7
	,[SPECT_REFNO] = convert(numeric,Column8)
	,[SPECT_REFNO_MAIN_IDENT] = Column9
	,[STEAM_REFNO] = convert(numeric,Column10)
	,[STEAM_REFNO_CODE] = Column11
	,[STEAM_REFNO_NAME] = Column12
	,[PROCA_REFNO] = convert(numeric,Column13)
	,[PROCA_REFNO_MAIN_IDENT] = Column14
	,[HEORG_REFNO] = convert(numeric,Column15)
	,[HEORG_REFNO_MAIN_IDENT] = Column16
	,[CODE] = Column17
	,[NOMINAL_DEPT_CODE] = Column18
	,[DESCRIPTION] = Column19
	,[NAME] = Column20
	,[START_DTTM] = convert(datetime,Column21)
	,[END_DTTM] = convert(datetime,Column22)
	,[PURPS_REFNO] = convert(numeric,Column23)
	,[PURPS_REFNO_MAIN_CODE] = Column24
	,[PURPS_REFNO_DESCRIPTION] = Column25
	,[SPTYP_REFNO] = convert(numeric,Column26)
	,[SPTYP_REFNO_MAIN_CODE] = Column27
	,[SPTYP_REFNO_DESCRIPTION] = Column28
	,[AE_FLAG] = Column29
	,[PDTYP_REFNO] = convert(numeric,Column30)
	,[PDTYP_REFNO_MAIN_CODE] = Column31
	,[PDTYP_REFNO_DESCRIPTION] = Column32
	,[CREATE_DTTM] = convert(datetime,Column33)
	,[MODIF_DTTM] = convert(datetime,Column34)
	,[USER_CREATE] = Column35
	,[USER_MODIF] = Column36
	,[ARCHV_FLAG] = Column37
	,[USE_BED_MANAGEMENT] = Column38
	,[HORIZ_VALUE] = convert(numeric,Column39)
	,[AGE_TYPE] = Column40
	,[AGE_QUALIFIER] = convert(numeric,Column41)
	,[WARN_LEVEL] = convert(numeric,Column42)
	,[MAX_LEVEL] = convert(numeric,Column43)
	,[FCPUR_REFNO] = convert(numeric,Column44)
	,[FCPUR_REFNO_MAIN_CODE] = Column45
	,[FCPUR_REFNO_DESCRIPTION] = Column46
	,[EXCLUDE_HOLS] = Column47
	,[RESCH_MAX_DIFF] = convert(numeric,Column48)
	,[OWNER_HEORG_REFNO] = convert(numeric,Column49)
	,[OWNER_HEORG_REFNO_MAIN_IDENT] = Column50
	,[INSTRUCTIONS] = Column51
	,[CHG_AC_AUTO] = Column52
	,[PBK_FLAG] = Column53
	,[PBK_LEAD_TIME] = convert(numeric,Column54)
	,[PBK_LEAD_TIME_UNITS] = Column55
	,[CONTACT_FLAG] = Column56
	,[LEAD_TIME_CLIN_FLAG] = Column57
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_SPONT_RF_%'
and	Valid = 1