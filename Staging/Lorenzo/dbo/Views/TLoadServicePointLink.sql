﻿CREATE view [dbo].[TLoadServicePointLink] as

select
	 [SPLIN_REFNO] = convert(int,Column5)
	,[FROM_SPONT_REFNO] = convert(int,Column6)
	,[TO_SPONT_REFNO] = convert(int,Column7)
	,[START_DTTM] = convert(smalldatetime,Column8)
	,[END_DTTM] = convert(smalldatetime,Column9)
	,[USER_CREATE] = Column10
	,[USER_MODIF] = Column11
	,[CREATE_DTTM] = convert(datetime,Column12)
	,[MODIF_DTTM] = convert(datetime,Column13)
	,[STRAN_REFNO] = convert(numeric,Column14)
	,[ARCHV_FLAG] = Column15
	,[EXTERNAL_KEY] = Column16
	,[OWNER_HEORG_REFNO] = convert(int,Column17)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_RF_SPLIN_%'
and	Valid = 1