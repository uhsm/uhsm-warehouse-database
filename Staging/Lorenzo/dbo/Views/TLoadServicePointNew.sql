﻿/***************************************************
SERVICE_POINTs
*************************************************/

CREATE View [dbo].[TLoadServicePointNew]
AS
Select SPONT_REFNO =  convert(INT,  Column5),
STRAN_REFNO =   convert(INT, Column6),
SPECT_REFNO =   convert(INT, Column7),
STEAM_REFNO =   convert(INT, Column8),
PROCA_REFNO =  convert(INT,  Column9),
HEORG_REFNO =  convert(INT,  Column10),
CODE =  Column11,
NOMINAL_DEPT_CODE =  Column12,
DESCRIPTION =  Column13,
NAME =  Column14,
WARN_LEVEL =   convert(INT, Column15),
MAX_LEVEL =  convert(INT,  Column16),
MAX_CAPACITY =  convert(INT,  Column17),
FCPUR_REFNO =   convert(INT, Column18),
EXCLUDE_HOLS =  Column19,
START_DTTM =  convert(Datetime, Column20),
END_DTTM =  convert(Datetime, Column21),
PURPS_REFNO =   convert(INT, Column22),
SPTYP_REFNO =   convert(INT, Column23),
CURNT_FLAG =  Column24,
AE_FLAG =  Column25,
CONTACT_FLAG =  Column26,
PDTYP_REFNO =   convert(INT, Column27),
LOCAT_REFNO =   convert(INT, Column28),
DIAG_FLAG =  Column29,
INSTRUCTIONS =  Column30,
RESCH_MAX_DIFF =   convert(INT, Column31),
AGE_TYPE =  Column32,
AGE_QUALIFIER =   convert(INT, Column33),
CREATE_DTTM = convert(Datetime,  Column34),
MODIF_DTTM =  convert(Datetime, Column35),
USER_CREATE =  Column36,
USER_MODIF =  Column37,
ARCHV_FLAG =  Column38,
PRIOR_POINTER =   convert(INT, Column39),
EXTERNAL_KEY =  Column40,
USE_BED_MANAGEMENT =  Column41,
HORIZ_VALUE =   convert(INT, Column42),
THTYP_REFNO =   convert(INT, Column43),
SEP_ANAES_ROOM =  Column44,
SEP_RECOV_ROOM =  Column45,
GROUP_CLINIC_FLAG =  Column46,
AUTO_DEPART_FLAG =  Column47,
CHG_AC_AUTO =  Column48,
PBK_FLAG =  Column49,
PBK_LEAD_TIME =  convert(INT,  Column50),
PBK_LEAD_TIME_UNITS =  Column51,
LEAD_TIME_CLIN_FLAG =  Column52,
CLN_DOCTMPLT_REFNO =  convert(INT,  Column53),
EXCLUDE_IN_CDS =  Column54,
CFADT_REFNO =   convert(INT, Column55),
COMMUNITY_FLAG =  Column56,
CSVTP_REFNO =   convert(INT, Column57),
OWNER_HEORG_REFNO =   convert(INT, Column58),
Created = Created

From TImportCSVParsed

where
	Column0 like 'RM2_GE_RF_SPONT_%'
and	Valid = 1