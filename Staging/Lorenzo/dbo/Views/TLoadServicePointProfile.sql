﻿CREATE view [dbo].[TLoadServicePointProfile] as

select
	 [SPPRO_REFNO] = convert(int,Column5)
	,[SPONT_REFNO] = convert(int,Column6)
	,[SPBAY_REFNO] = convert(int,Column7)
	,[SPBED_REFNO] = convert(int,Column8)
	,[SPECT_REFNO] = convert(int,Column9)
	,[PROCA_REFNO] = convert(int,Column10)
	,[BEDSS_REFNO] = convert(int,Column11)
	,[RFVAL_REFNO] = convert(int,Column12)
	,[RULES_REFNO] = convert(int,Column13)
	,[OPERATOR] = Column14
	,[VALUE] = Column15
	,[BDCAT_REFNO] = convert(int,Column16)
	,[START_DTTM] = convert(smalldatetime,Column17)
	,[END_DTTM] = convert(smalldatetime,Column18)
	,[CREATE_DTTM] = convert(datetime,Column19)
	,[MODIF_DTTM] = convert(datetime,Column20)
	,[USER_CREATE] = Column21
	,[USER_MODIF] = Column22
	,[ARCHV_FLAG] = Column23
	,[STRAN_REFNO] = convert(numeric,Column24)
	,[EXTERNAL_KEY] = Column25
	,[SPCHR_REFNO] = convert(int,Column26)
	,[WDPRO_REFNO] = convert(int,Column27)
	,[PLANNED_DTTM] = convert(smalldatetime,Column28)
	,[OWNER_HEORG_REFNO] = convert(int,Column29)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_SP_PROF_BD_%'
and	Valid = 1