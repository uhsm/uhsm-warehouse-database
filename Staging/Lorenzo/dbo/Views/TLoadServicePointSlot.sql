﻿CREATE view [dbo].[TLoadServicePointSlot] as

select
	 [SPSLT_REFNO] = convert(numeric,Column5)
	,[SPSSN_REFNO] = convert(numeric,Column6)
	,[START_DTTM] = convert(datetime,Column7)
	,[END_DTTM] = convert(datetime,Column8)
	,[NOOF_BOOKINGS] = convert(numeric,Column9)
	,[TSTAT_REFNO] = convert(numeric,Column10)
	,[TSTAT_REFNO_MAIN_CODE] = Column11
	,[TSTAT_REFNO_DESCRIPTION] = Column12
	,[INSTRUCTIONS] = Column13
	,[CREATE_DTTM] = convert(datetime,Column14)
	,[MODIF_DTTM] = convert(datetime,Column15)
	,[USER_CREATE] = Column16
	,[USER_MODIF] = Column17
	,[DURATION] = convert(numeric,Column18)
	,[ARCHV_FLAG] = Column19
	,[TEMPLATE_FLAG] = Column20
	,[SPONT_REFNO] = convert(numeric,Column21)
	,[SPONT_REFNO_CODE] = Column22
	,[SPONT_REFNO_NAME] = Column23
	,[TMPLT_REFNO] = convert(numeric,Column24)
	,[CANCEL_DTTM] = convert(datetime,Column25)
	,[COMMENTS] = Column26
	,[SLCHR_REFNO] = convert(numeric,Column27)
	,[SLCHR_REFNO_MAIN_CODE] = Column28
	,[SLCHR_REFNO_DESCRIPTION] = Column29
	,[MAX_BOOKINGS] = convert(numeric,Column30)
	,[STCRB_REFNO] = convert(numeric,Column31)
	,[STCRB_REFNO_MAIN_CODE] = Column32
	,[STCRB_REFNO_DESCRIPTION] = Column33
	,[OWNER_HEORG_REFNO] = convert(numeric,Column34)
	,[OWNER_HEORG_REFNO_MAIN_IDENT] = Column35
	,[EBS_PUB_FLAG] = Column36
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_SPSLT_RF_%'
and	Valid = 1