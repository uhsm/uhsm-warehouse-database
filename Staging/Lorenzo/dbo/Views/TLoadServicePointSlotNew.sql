﻿/***************************************************
SERVICE_POINT_TIME_SLOTS
*************************************************/

CREATE View [dbo].[TLoadServicePointSlotNew]
AS 
Select SPSLT_REFNO =  convert(INT, Column5),
STRAN_REFNO =  convert(INT, Column6),
SPSSN_REFNO =  convert(INT, Column7),
START_DTTM =  convert(Datetime, Column8),
END_DTTM =   convert(Datetime, Column9),
PREV_START_DTTM =   convert(Datetime, Column10),
PREV_END_DTTM =   convert(Datetime, Column11),
NOOF_BOOKINGS =  convert(INT, Column12),
TSTAT_REFNO =  convert(INT, Column13),
PREV_TSTAT_REFNO =  convert(INT, Column14),
INSTRUCTIONS =  Column15,
CREATE_DTTM =   convert(Datetime, Column16),
MODIF_DTTM =   convert(Datetime, Column17),
USER_CREATE =  Column18,
USER_MODIF =  Column19,
DURATION =  convert(INT, Column20),
USRN =  Column21,
ARCHV_FLAG =  Column22,
PRIOR_POINTER =  convert(INT, Column23),
TEMPLATE_FLAG =  Column24,
EBS_PUB_FLAG =  Column25,
SPONT_REFNO =  convert(INT, Column26),
EXTERNAL_KEY =  Column27,
TMPLT_REFNO =  convert(INT, Column28),
CANCEL_DTTM =   convert(Datetime, Column29),
COMMENTS =  Column30,
SLCHR_REFNO =  convert(INT, Column31),
MAX_BOOKINGS =  convert(INT, Column32),
SDS_USER_ID =  Column33,
STCRB_REFNO =  convert(INT, Column34),
DIARY_CHANGED =  Column35,
OWNER_HEORG_REFNO =  convert(INT, Column36),
Created = Created


From TImportCSVParsed

where
	Column0 like 'RM2_GE_RF_SPSLT_%'
and	Valid = 1