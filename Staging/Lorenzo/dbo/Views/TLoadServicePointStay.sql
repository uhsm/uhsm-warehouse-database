﻿CREATE view [dbo].[TLoadServicePointStay] as

select
	 [SSTAY_REFNO] = convert(numeric,Column5)
	,[PATNT_REFNO] = convert(numeric,Column6)
	,[PATNT_REFNO_PASID] = Column7
	,[PATNT_REFNO_NHS_IDENTIFIER] = Column8
	,[SPONT_REFNO] = convert(numeric,Column9)
	,[SPONT_REFNO_CODE] = Column10
	,[SPONT_REFNO_NAME] = Column11
	,[PRVSP_REFNO] = convert(numeric,Column12)
	,[START_DTTM] = convert(datetime,Column13)
	,[END_DTTM] = convert(datetime,Column14)
	,[CREATE_DTTM] = convert(datetime,Column15)
	,[MODIF_DTTM] = convert(datetime,Column16)
	,[USER_CREATE] = Column17
	,[USER_MODIF] = Column18
	,[ARCHV_FLAG] = Column19
	,[PRVSN_FLAG] = Column20
	,[PROCA_REFNO] = convert(numeric,Column21)
	,[PROCA_REFNO_MAIN_IDENT] = Column22
	,[BDCAT_REFNO] = convert(numeric,Column23)
	,[BDCAT_REFNO_MAIN_CODE] = Column24
	,[BDCAT_REFNO_DESCRIPTION] = Column25
	,[REQTD_BDCAT_REFNO] = convert(numeric,Column26)
	,[REQTD_BDCAT_REFNO_MAIN_CODE] = Column27
	,[REQTD_BDCAT_REFNO_DESCRIPTION] = Column28
	,[CHAPLAIN_VISIT_FLAG] = Column29
	,[VISITORS_ALLOWED_FLAG] = Column30
	,[OWNER_HEORG_REFNO] = convert(numeric,Column31)
	,[OWNER_HEORG_REFNO_MAIN_IDENT] = Column32
	,[WARD_START_DTTM] = convert(datetime,Column33)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_SSTAY_IP_%'
and	Valid = 1