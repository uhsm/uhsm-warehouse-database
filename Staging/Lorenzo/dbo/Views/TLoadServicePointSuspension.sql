﻿CREATE view [dbo].[TLoadServicePointSuspension] as

select
	 [SPSUS_REFNO] = convert(int,Column5)
	,[SPBAY_REFNO] = convert(int,Column6)
	,[SPONT_REFNO] = convert(int,Column7)
	,[BEDSS_REFNO] = convert(int,Column8)
	,[START_DTTM] = convert(smalldatetime,Column9)
	,[END_DTTM] = convert(smalldatetime,Column10)
	,[SPSUR_REFNO] = convert(int,Column11)
	,[CREATE_DTTM] = convert(datetime,Column12)
	,[MODIF_DTTM] = convert(datetime,Column13)
	,[USER_CREATE] = Column14
	,[USER_MODIF] = Column15
	,[ARCHV_FLAG] = Column16
	,[STRAN_REFNO] = convert(numeric,Column17)
	,[EXTERNAL_KEY] = Column18
	,[OWNER_HEORG_REFNO] = convert(int,Column19)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_SP_SUSP_BD_%'
and	Valid = 1