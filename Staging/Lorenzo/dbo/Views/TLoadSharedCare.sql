﻿CREATE view [dbo].[TLoadSharedCare] as

select
	 [SHARC_REFNO] = convert(int,Column5)
	,[SPECT_REFNO] = convert(int,Column6)
	,[PROCA_REFNO] = convert(int,Column7)
	,[PRCAE_REFNO] = convert(numeric,Column8)
	,[PRVSP_REFNO] = convert(numeric,Column9)
	,[ACTUAL_FLAG] = Column10
	,[PRIME_FLAG] = Column11
	,[START_DTTM] = convert(smalldatetime,Column12)
	,[END_DTTM] = convert(smalldatetime,Column13)
	,[CREATE_DTTM] = convert(datetime,Column14)
	,[MODIF_DTTM] = convert(datetime,Column15)
	,[USER_CREATE] = Column16
	,[USER_MODIF] = Column17
	,[ARCHV_FLAG] = Column18
	,[PRIOR_POINTER] = convert(int,Column19)
	,[EXTERNAL_KEY] = Column20
	,[STRAN_REFNO] = convert(numeric,Column21)
	,[OWNER_HEORG_REFNO] = convert(int,Column22)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_RF_SHARC_%'
and	Valid = 1