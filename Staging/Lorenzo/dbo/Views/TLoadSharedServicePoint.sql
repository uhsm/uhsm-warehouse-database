﻿CREATE view [dbo].[TLoadSharedServicePoint] as

select
	 [SHSPT_REFNO] = convert(int,Column5)
	,[SPECT_REFNO] = convert(int,Column6)
	,[SPSSN_REFNO] = convert(int,Column7)
	,[PROCA_REFNO] = convert(int,Column8)
	,[SPONT_REFNO] = convert(int,Column9)
	,[START_DTTM] = convert(smalldatetime,Column10)
	,[END_DTTM] = convert(smalldatetime,Column11)
	,[CREATE_DTTM] = convert(datetime,Column12)
	,[MODIF_DTTM] = convert(datetime,Column13)
	,[USER_CREATE] = Column14
	,[USER_MODIF] = Column15
	,[ARCHV_FLAG] = Column16
	,[CARER_FLAG] = Column17
	,[PRIOR_POINTER] = convert(int,Column18)
	,[EXTERNAL_KEY] = Column19
	,[STRAN_REFNO] = convert(numeric,Column20)
	,[OWNER_HEORG_REFNO] = convert(int,Column21)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_RF_SHSPT_%'
and	Valid = 1