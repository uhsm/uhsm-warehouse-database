﻿CREATE view [dbo].[TLoadSpecialty] as

select
	 [SPECT_REFNO] = convert(numeric,Column5)
	,[SPECT_REFNO_MAIN_IDENT] = Column6
	,[MAIN_IDENT] = Column7
	,[DESCRIPTION] = Column8
	,[PARNT_REFNO] = convert(numeric,Column9)
	,[DIVSN_REFNO] = convert(numeric,Column10)
	,[DIVSN_REFNO_MAIN_CODE] = Column11
	,[DIVSN_REFNO_DESCRIPTION] = Column12
	,[CREATE_DTTM] = convert(datetime,Column13)
	,[MODIF_DTTM] = convert(datetime,Column14)
	,[USER_CREATE] = Column15
	,[USER_MODIF] = Column16
	,[ARCHV_FLAG] = Column17
	,[START_DTTM] = convert(datetime,Column18)
	,[END_DTTM] = convert(datetime,Column19)
	,[OWNER_HEORG_REFNO] = convert(numeric,Column20)
	,[OWNER_HEORG_REFNO_MAIN_IDENT] = Column21
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_SPECT_RF_%'
and	Valid = 1