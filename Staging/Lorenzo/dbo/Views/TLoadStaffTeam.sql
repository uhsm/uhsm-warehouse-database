﻿CREATE view [dbo].[TLoadStaffTeam] as

select
	 [STEAM_REFNO] = convert(int,Column5)
	,[START_DTTM] = convert(smalldatetime,Column6)
	,[END_DTTM] = convert(smalldatetime,Column7)
	,[DESCRIPTION] = Column8
	,[NAME] = Column9
	,[CODE] = Column10
	,[STTYP_REFNO] = convert(int,Column11)
	,[HEORG_REFNO] = convert(int,Column12)
	,[SPECT_REFNO] = convert(int,Column13)
	,[LEADR_PROCA_REFNO] = convert(int,Column14)
	,[CREATE_DTTM] = convert(datetime,Column15)
	,[MODIF_DTTM] = convert(datetime,Column16)
	,[USER_CREATE] = Column17
	,[USER_MODIF] = Column18
	,[ARCHV_FLAG] = Column19
	,[STRAN_REFNO] = convert(numeric,Column20)
	,[EXTERNAL_KEY] = Column21
	,[OWNER_HEORG_REFNO] = convert(int,Column22)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_STEAM_RF_%'
and	Valid = 1