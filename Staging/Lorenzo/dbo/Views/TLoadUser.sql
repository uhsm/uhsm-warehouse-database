﻿CREATE view [dbo].[TLoadUser] as

select
	 [USERS_REFNO] = convert(numeric,Column5)
	,[PROCA_REFNO] = convert(numeric,Column6)
	,[CODE] = Column7
	,[USER_NAME] = Column8
	,[DEPARTMENT] = Column9
	,[EMAIL] = Column10
	,[EVENTS_EXIST_FLAG] = Column11
	,[USER_CREATE] = Column12
	,[USER_MODIF] = Column13
	,[CREATE_DTTM] = convert(datetime,Column14)
	,[MODIF_DTTM] = convert(datetime,Column15)
	,[EXTERNAL_KEY] = Column16
	,[ARCHV_FLAG] = Column17
	,[STRAN_REFNO] = convert(numeric,Column18)
	,[ALTER_DTTM] = convert(datetime,Column19)
	,[LAST_LOGON_DTTM] = convert(datetime,Column20)
	,[LOGGED_ON] = convert(numeric,Column21)
	,[LOCKED_FLAG] = Column22
	,[LOCKED_COMMENTS] = Column23
	,[CHANGE_PASSWORD_FLAG] = Column24
	,[ACLEV_REFNO] = convert(numeric,Column25)
	,[NHS_USER_FLAG] = Column26
	,[OWNER_HEORG_REFNO] = convert(numeric,Column27)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_USERS_RF_%'
and	Valid = 1