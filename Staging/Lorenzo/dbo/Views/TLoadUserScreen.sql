﻿CREATE view [dbo].[TLoadUserScreen] as

select
	 [USSCR_REFNO] = convert(int,Column5)
	,[LCLDS_REFNO] = convert(int,Column6)
	,[USERS_REFNO] = convert(int,Column7)
	,[DRFRM_REFNO] = convert(int,Column8)
	,[NAME] = Column9
	,[CODE] = Column10
	,[CREATE_DTTM] = convert(datetime,Column11)
	,[MODIF_DTTM] = convert(datetime,Column12)
	,[USER_CREATE] = Column13
	,[USER_MODIF] = Column14
	,[ARCHV_FLAG] = Column15
	,[STRAN_REFNO] = convert(numeric,Column16)
	,[EXTERNAL_KEY] = Column17
	,[DISP_LOGIC] = Column18
	,[OWNER_HEORG_REFNO] = convert(int,Column19)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_USER_SCREN_LO_%'
and	Valid = 1