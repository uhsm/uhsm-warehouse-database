﻿CREATE view [dbo].[TLoadUserScreenForm] as

select
	 [USSFR_REFNO] = convert(int,Column5)
	,[USSCR_REFNO] = convert(int,Column6)
	,[SEQ_NUMBER] = convert(int,Column7)
	,[SCR_WIDTH] = convert(int,Column8)
	,[SCR_HEIGHT] = convert(int,Column9)
	,[CREATE_DTTM] = convert(datetime,Column10)
	,[MODIF_DTTM] = convert(datetime,Column11)
	,[USER_CREATE] = Column12
	,[USER_MODIF] = Column13
	,[ARCHV_FLAG] = Column14
	,[STRAN_REFNO] = convert(numeric,Column15)
	,[EXTERNAL_KEY] = Column16
	,[TAB_DISPLAY_LOGIC] = Column17
	,[TAB_NAME] = Column18
	,[OWNER_HEORG_REFNO] = convert(int,Column19)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_USER_SCFRM_LO_%'
and	Valid = 1