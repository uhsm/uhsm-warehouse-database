﻿CREATE view [dbo].[TLoadUserScreenObject] as

select
	 [USSOB_REFNO] = convert(int,Column5)
	,[USSFR_REFNO] = convert(int,Column6)
	,[LCDSI_REFNO] = convert(int,Column7)
	,[NAME] = Column8
	,[POS_LEFT] = convert(numeric,Column9)
	,[POS_TOP] = convert(numeric,Column10)
	,[CTRL_WIDTH] = convert(numeric,Column11)
	,[CTRL_HEIGHT] = convert(numeric,Column12)
	,[CTRL_ENAB] = Column13
	,[CTRL_MAND] = Column14
	,[ACCEP_VALS] = Column15
	,[PROMPT_LEFT] = convert(numeric,Column16)
	,[PROMPT_TOP] = convert(numeric,Column17)
	,[CREATE_DTTM] = convert(datetime,Column18)
	,[MODIF_DTTM] = convert(datetime,Column19)
	,[USER_CREATE] = Column20
	,[USER_MODIF] = Column21
	,[ARCHV_FLAG] = Column22
	,[STRAN_REFNO] = convert(numeric,Column23)
	,[EXTERNAL_KEY] = Column24
	,[ASSES_SCORE_FLAG] = Column25
	,[ASSES_TRIG_FLAG] = Column26
	,[ASSES_TRIG_OPERATOR] = Column27
	,[ASSES_TRIG_VALUE] = convert(numeric,Column28)
	,[ASSES_REFNO] = convert(int,Column29)
	,[OWNER_HEORG_REFNO] = convert(int,Column30)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_USER_SCOBJ_LO_%'
and	Valid = 1