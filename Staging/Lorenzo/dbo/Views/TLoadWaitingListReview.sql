﻿CREATE view [dbo].[TLoadWaitingListReview] as

select
	 [WLREV_REFNO] = convert(int,Column5)
	,[WLRVB_REFNO] = convert(int,Column6)
	,[WLIST_REFNO] = convert(numeric,Column7)
	,[WRSTA_REFNO] = convert(int,Column8)
	,[OUTCOME_DTTM] = convert(smalldatetime,Column9)
	,[USER_CREATE] = Column10
	,[USER_MODIF] = Column11
	,[CREATE_DTTM] = convert(datetime,Column12)
	,[MODIF_DTTM] = convert(datetime,Column13)
	,[STRAN_REFNO] = convert(numeric,Column14)
	,[EXTERNAL_KEY] = Column15
	,[PRIOR_REFNO] = convert(int,Column16)
	,[ARCHV_FLAG] = Column17
	,[OWNER_HEORG_REFNO] = convert(int,Column18)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_WLIST_REV_WL_%'
and	Valid = 1