﻿CREATE view [dbo].[TLoadWaitingListReviewBatch] as

select
	 [WLRVB_REFNO] = convert(int,Column5)
	,[BATCH_NUMBER] = Column6
	,[REVIEW_DTTM] = convert(smalldatetime,Column7)
	,[USER_CREATE] = Column8
	,[USER_MODIF] = Column9
	,[CREATE_DTTM] = convert(datetime,Column10)
	,[MODIF_DTTM] = convert(datetime,Column11)
	,[STRAN_REFNO] = convert(numeric,Column12)
	,[EXTERNAL_KEY] = Column13
	,[ARCHV_FLAG] = Column14
	,[OWNER_HEORG_REFNO] = convert(int,Column15)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_WLIST_REVBT_WL_%'
and	Valid = 1