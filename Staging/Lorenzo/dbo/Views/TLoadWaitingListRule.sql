﻿CREATE view [dbo].[TLoadWaitingListRule] as

select
	 [WLRUL_REFNO] = convert(int,Column5)
	,[SPECT_REFNO] = convert(int,Column6)
	,[PROCA_REFNO] = convert(int,Column7)
	,[HEORG_REFNO] = convert(int,Column8)
	,[MWAIT_DAYCASE] = convert(numeric,Column9)
	,[MWAIT_ORDINARY] = convert(numeric,Column10)
	,[CREATE_DTTM] = convert(datetime,Column11)
	,[MODIF_DTTM] = convert(datetime,Column12)
	,[USER_CREATE] = Column13
	,[USER_MODIF] = Column14
	,[CODE] = Column15
	,[NAME] = Column16
	,[COMMENTS] = Column17
	,[STRAN_REFNO] = convert(numeric,Column18)
	,[PRIOR_POINTER] = convert(int,Column19)
	,[ARCHV_FLAG] = Column20
	,[EXTERNAL_KEY] = Column21
	,[SVTYP_REFNO] = convert(int,Column22)
	,[SPONT_REFNO] = convert(int,Column23)
	,[LOCAL_FLAG] = Column24
	,[END_DTTM] = convert(smalldatetime,Column25)
	,[OWNER_HEORG_REFNO] = convert(int,Column26)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_WLIST_RULES_WL_%'
and	Valid = 1