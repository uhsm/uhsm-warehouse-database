﻿/***************************************************
Waiting List Rules
*************************************************/
CREATE View [dbo].[TLoadWaitingListRuleNew]
AS
Select WLRUL_REFNO = convert(INT, Column5),
SPECT_REFNO =  convert(INT,Column6),
PROCA_REFNO =  convert(INT,Column7),
HEORG_REFNO = convert(INT, Column8),
MWAIT_DAYCASE =convert(INT,  Column9),
MWAIT_ORDINARY =  convert(INT,Column10),
CREATE_DTTM =  convert(Datetime, Column11),
MODIF_DTTM =  convert(Datetime, Column12),
USER_CREATE =  Column13,
USER_MODIF =  Column14,
CODE =  Column15,
NAME =  Column16,
COMMENTS =  Column17,
STRAN_REFNO =  convert(INT,Column18),
PRIOR_POINTER = convert(INT, Column19),
ARCHV_FLAG =  Column20,
EXTERNAL_KEY =  Column21,
SVTYP_REFNO =  convert(INT,Column22),
SPONT_REFNO =  convert(INT,Column23),
LOCAL_FLAG =  Column24,
END_DTTM =  convert(Datetime, Column25),
START_DTTM =  convert(Datetime, Column26),
DIAG_WAIT_FLAG =  Column27,
OWNER_HEORG_REFNO =  convert(INT,Column28),
Created =Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_RF_WLRUL_%'
and	Valid = 1