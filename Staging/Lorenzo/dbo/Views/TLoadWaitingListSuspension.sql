﻿CREATE view [dbo].[TLoadWaitingListSuspension] as

select
	 [WSUSP_REFNO] = convert(numeric,Column5)
	,[WLIST_REFNO] = convert(numeric,Column6)
	,[SUSRS_REFNO] = convert(numeric,Column7)
	,[SUSRS_REFNO_MAIN_CODE] = Column8
	,[SUSRS_REFNO_DESCRIPTION] = Column9
	,[START_DTTM] = convert(datetime,Column10)
	,[END_DTTM] = convert(datetime,Column11)
	,[COMMENTS] = Column12
	,[CREATE_DTTM] = convert(datetime,Column13)
	,[MODIF_DTTM] = convert(datetime,Column14)
	,[USER_CREATE] = Column15
	,[USER_MODIF] = Column16
	,[ARCHV_FLAG] = Column17
	,[REVIEW_DTTM] = convert(datetime,Column18)
	,[OWNER_HEORG_REFNO] = convert(numeric,Column19)
	,[OWNER_HEORG_REFNO_MAIN_IDENT] = Column20
	,[END_FLAG] = Column21
	,[START_FLAG] = Column22
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_WLIST_WSUSP_IP_%'
and	Valid = 1