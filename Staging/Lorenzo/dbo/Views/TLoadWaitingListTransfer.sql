﻿CREATE view [dbo].[TLoadWaitingListTransfer] as

select
	 [WLTFR_REFNO] = convert(int,Column5)
	,[WLIST_REFNO] = convert(numeric,Column6)
	,[TRANSFER_DTTM] = convert(smalldatetime,Column7)
	,[WLTRN_REFNO] = convert(int,Column8)
	,[FROM_PROCA_REFNO] = convert(int,Column9)
	,[FROM_SPECT_REFNO] = convert(int,Column10)
	,[FROM_WLRUL_REFNO] = convert(int,Column11)
	,[FROM_LOCAL_WLRUL_REFNO] = convert(int,Column12)
	,[TO_PROCA_REFNO] = convert(int,Column13)
	,[TO_SPECT_REFNO] = convert(int,Column14)
	,[TO_WLRUL_REFNO] = convert(int,Column15)
	,[TO_LOCAL_WLRUL_REFNO] = convert(int,Column16)
	,[CREATE_DTTM] = convert(datetime,Column17)
	,[MODIF_DTTM] = convert(datetime,Column18)
	,[USER_CREATE] = Column19
	,[USER_MODIF] = Column20
	,[ARCHV_FLAG] = Column21
	,[STRAN_REFNO] = convert(numeric,Column22)
	,[EXTERNAL_KEY] = Column23
	,[OWNER_HEORG_REFNO] = convert(int,Column24)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_WLIST_TRANS_WL_%'
and	Valid = 1