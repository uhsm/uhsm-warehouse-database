﻿/***************************************************
Waiting List Transfers
*************************************************/

CREATE View [dbo].[TLoadWaitingListTransferNew]
AS
Select WLTFR_REFNO =  convert(INT,Column5),
WLIST_REFNO =  convert(INT,Column6),
TRANSFER_DTTM =  convert(Datetime, Column7),
WLTRN_REFNO =  convert(INT,Column8),
FROM_PROCA_REFNO =  convert(INT,Column9),
FROM_SPECT_REFNO =  convert(INT,Column10),
FROM_WLRUL_REFNO =  convert(INT,Column11),
FROM_LOCAL_WLRUL_REFNO =  convert(INT,Column12),
TO_PROCA_REFNO =  convert(INT,Column13),
TO_SPECT_REFNO =  convert(INT,Column14),
TO_WLRUL_REFNO =  convert(INT,Column15),
TO_LOCAL_WLRUL_REFNO = convert(INT, Column16),
CREATE_DTTM = convert(Datetime,  Column17),
MODIF_DTTM = convert(Datetime,  Column18),
USER_CREATE =  Column19,
USER_MODIF =  Column20,
ARCHV_FLAG =  Column21,
STRAN_REFNO =  convert(INT,Column22),
EXTERNAL_KEY =  Column23,
FROM_STEAM_REFNO =  convert(INT,Column24),
TO_STEAM_REFNO = convert(INT, Column25),
OWNER_HEORG_REFNO = convert(INT, Column26),
Created = Created

from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_GE_OA_WLTFR_%'
and	Valid = 1