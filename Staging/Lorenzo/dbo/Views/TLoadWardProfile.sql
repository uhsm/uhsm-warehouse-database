﻿CREATE view [dbo].[TLoadWardProfile] as

select
	 [WDPRO_REFNO] = convert(int,Column5)
	,[SPONT_REFNO] = convert(int,Column6)
	,[AGEGR_REFNO] = convert(int,Column7)
	,[SEXXX_REFNO] = convert(int,Column8)
	,[BRPAG_REFNO] = convert(int,Column9)
	,[INCLI_REFNO] = convert(int,Column10)
	,[LODGERS_FLAG] = Column11
	,[START_DTTM] = convert(smalldatetime,Column12)
	,[END_DTTM] = convert(smalldatetime,Column13)
	,[HEORG_REFNO] = convert(int,Column14)
	,[USER_CREATE] = Column15
	,[USER_MODIF] = Column16
	,[CREATE_DTTM] = convert(datetime,Column17)
	,[MODIF_DTTM] = convert(datetime,Column18)
	,[STRAN_REFNO] = convert(numeric,Column19)
	,[ARCHV_FLAG] = Column20
	,[EXTERNAL_KEY] = Column21
	,[OWNER_HEORG_REFNO] = convert(int,Column22)
	,Created
from
	dbo.TImportCSVParsed
where
	Column0 like 'RM2_WARD_PROF_BD_%'
and	Valid = 1