﻿/***************************************************
Casenote histories
*************************************************/
CREATE View [dbo].[TloadCasenoteHistories]
AS
Select CASHI_REFNO =  convert(INT,Column5),
CASNT_REFNO =  convert(INT,Column6),
CASVL_REFNO =  convert(INT,Column7),
OLD_MDTYP_REFNO =  convert(INT,Column8),
OLD_PDTYP_REFNO =  convert(INT,Column9),
OLD_STORE_HEORG_REFNO =  convert(INT,Column10),
OLD_TEMP_FLAG =  Column11,
NEW_MDTYP_REFNO =  convert(INT,Column12),
NEW_PDTYP_REFNO =  convert(INT,Column13),
NEW_STORE_HEORG_REFNO =  convert(INT,Column14),
NEW_TEMP_FLAG =  Column15,
CREATE_DTTM =  convert(Datetime,Column16),
MODIF_DTTM =  convert(Datetime,Column17),
USER_CREATE =  Column18,
USER_MODIF =  Column19,
ARCHV_FLAG =  Column20,
STRAN_REFNO =  convert(INT,Column21),
PRIOR_POINTER =  convert(INT,Column22),
EXTERNAL_KEY =  Column23,
OWNER_HEORG_REFNO =  convert(INT,Column24),
Created = Created


From dbo.TImportCSVParsed
where Column0 like 'RM2_GE_OA_CASHI_%'
and Valid = 1