﻿CREATE View [dbo].[vwCasenoteTrackingForSpecificUser] as 

Select
	 FaciltyID = DistrictNo.IDENTIFIER
	,CaseNoteID = Casenote.IDENTIFIER 
	,StatusDate = CAST(Movement.RQSTA_DTTM as date)
	,StatusTime = CAST(Movement.RQSTA_DTTM as time(0))
	,[Status] = RequestStatus.[DESCRIPTION]
--	,StatusDate = Movement.RQSTA_DTTM
--	,StartDate = Movement.START_DTTM
--	,iPMCreateDate = Movement.CREATE_DTTM
	,[Location] = ServicePoint.DESCRIPTION
	,ipmUser.CODE
	,ipmUser.[USER_NAME]
	,UsualLocation = VolUsual.[DESCRIPTION]
	,VolNo = Volume.SERIAL
	,TrackingComments = Movement.COMMENTS
from CasenoteActivity Movement
inner join CasenoteVolume Volume
	on Movement.CASVL_REFNO = Volume.CASVL_REFNO
inner join Casenote Casenote
	on Volume.CASNT_REFNO = Casenote.CASNT_REFNO
left outer join ReferenceValue RequestStatus
	on Movement.RQSTA_REFNO = RequestStatus.RFVAL_REFNO
left outer join ServicePoint ServicePoint
	on Movement.REQBY_SPONT_REFNO = ServicePoint.SPONT_REFNO
left outer join [User] ipmUser 
	on Movement.REQBY_USERS_REFNO = ipmuser.USERS_REFNO 
left outer join ServicePoint VolUsual
	on Volume.STORE_SPONT_REFNO = VolUsual.SPONT_REFNO
left outer join dbo.PatientIdentifier DistrictNo
		on	DistrictNo.PATNT_REFNO = Casenote.PATNT_REFNO
		and	DistrictNo.PITYP_REFNO = 2001232 --facility
		and	DistrictNo.IDENTIFIER like 'RM2%'
		and DistrictNo.ARCHV_FLAG = 'N'
		and	not exists
			(
			select
				1
			from
				dbo.PatientIdentifier Previous
			where
				Previous.PATNT_REFNO = DistrictNo.PATNT_REFNO
			and	Previous.PITYP_REFNO = DistrictNo.PITYP_REFNO
			and	Previous.IDENTIFIER like 'RM2%'
			and Previous.ARCHV_FLAG = 'N'
			and	(
					Previous.START_DTTM > DistrictNo.START_DTTM
				or
					(
						Previous.START_DTTM = DistrictNo.START_DTTM
					and	Previous.PATID_REFNO > DistrictNo.PATID_REFNO
					)
				)
			)


Where 
(
	(
		Movement.CREATE_DTTM >= '20131104 00:00:00' 
	and Movement.CREATE_DTTM <'20131105 00:00:00'
	)
or
	(
		Movement.MODIF_DTTM >= '20131104 00:00:00' 
	and Movement.MODIF_DTTM <'20131105 00:00:00'
	)
or
	(
		Movement.RQSTA_DTTM >= '20131104 00:00:00' 
	and Movement.RQSTA_DTTM <'20131105 00:00:00'
	)

)	
	
and REQBY_USERS_REFNO = 10014718
and Movement.ARCHV_FLAG = 'N'

--select * from dbo.[User] where users_refno = 10014718