﻿CREATE  View [dbo].[vwCriticalCareOrganSupportDays] AS
SELECT
APCDT_REFNO,
IsNull(SUM([Advanced Respiratory Support]),0) AS [Advanced Respiratory Support Days],
IsNull(SUM([Basic Respiratory Support]),0) AS [Basic Respiratory Support Days],
IsNull(SUM([Circulatory Support]),0) AS [Circulatory Support Days],
IsNull(SUM([Neurological Support]),0) AS [Neurological Support Days],
IsNull(SUM([Renal Support]),0) AS [Renal Support Days],
IsNull(SUM([Liver Support]),0) AS [Liver Support Days],
IsNull(SUM([Dermatological Support]),0) AS [Dermatological Support Days],
IsNull(SUM([Gastrointestinal Support]),0) AS [Gastrointestinal Support Days],
IsNull(SUM([Advanced Cardiovascular Support]),0) AS [Advanced Cardiovascular Support Days],
IsNull(SUM([Basic Cardiovascular Support]),0) AS [Basic Cardiovascular Support Days],
--IsNull(SUM([Dermatological]),0) AS [Dermatological], not populated possibly replaced [Dermatological Support]?
IsNull(SUM([Enhanced Nursing Care]),0) AS [Enhanced Nursing Care Days],
--IsNull(SUM([Gastro intestinal]),0) AS [Gastro intestinal],   not populated possibly replaced [Gastrointestinal Support]?
IsNull(SUM([Hepatic]),0) AS [Hepatic Days],
IsNull(SUM([Not Specified Support]),0) AS [Not Specified Support Days],
count(1) as AllSupported
FROM dbo.APCStay A 
LEFT OUTER JOIN dbo.vwCriticalCareOrganSupport B ON A.APCST_REFNO = B.APCST_REFNO
WHERE A.ARCHV_FLAG = 'N'
GROUP BY
APCDT_REFNO