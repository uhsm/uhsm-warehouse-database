﻿CREATE View [dbo].[vwCurrentCasenoteLocation] as

Select
	 SourcePatientNo = Casenote.PATNT_REFNO
	,CasenoteUniqueID = Casenote.CASNT_REFNO
	,CasenoteNo = Casenote.IDENTIFIER
	,CurrentLocationUniqueID = Volume.CURNT_SPONT_REFNO
	,CurrentCasenoteLocationCode = ServicePoint.CODE
	,CurrentCasenoteLocation = ServicePoint.DESCRIPTION
	,CasenoteActivityUniqueID = Activity.CASAC_REFNO
	,RequestStatusUniqueID = Activity.RQSTA_REFNO
	,Received = 
		Case When Activity.RQSTA_REFNO = 1426 Then
			'Y'
		Else
			'N'
		End
	,CasenoteMovementCommets = COMMENTS

from dbo.Casenote Casenote
left outer join dbo.CasenoteVolume Volume
	on Casenote.CASNT_REFNO = Volume.CASNT_REFNO
	and Volume.ARCHV_FLAG = 'N'
	and Volume.SERIAL = 1
	and Volume.END_DTTM is null
left outer join dbo.CasenoteActivity Activity
	on 
	Volume.CASVL_REFNO = Activity.CASVL_REFNO
	and Activity.ARCHV_FLAG = 'N'
left outer join ServicePoint ServicePoint
	on Volume.CURNT_SPONT_REFNO = ServicePoint.SPONT_REFNO

Where
	Casenote.ARCHV_FLAG = 'N'
	and Casenote.PDTYP_REFNO = 3444
	and RIGHT(Casenote.IDENTIFIER,2) = '01'
--	and Casenote.PATNT_REFNO = 11101309
	and not exists
		(
		Select 1 
		from dbo.Casenote Previous
		where 
		Previous.ARCHV_FLAG = 'N'
		and Previous.PDTYP_REFNO = 3444
		and RIGHT(previous.IDENTIFIER,2) = '01'
		and casenote.PATNT_REFNO = Previous.PATNT_REFNO
		and Casenote.CASNT_REFNO < Previous.CASNT_REFNO
		)
	and not exists
		(Select 1 
		from dbo.CasenoteVolume PreviousVolume
		where 
		PreviousVolume.ARCHV_FLAG = 'N'
		and PreviousVolume.SERIAL = 1
		and PreviousVolume.END_DTTM is null
		and Volume.CASNT_REFNO = PreviousVolume.CASNT_REFNO
		and Volume.CASVL_REFNO < PreviousVolume.CASVL_REFNO
		)
		
	and not exists
		(
		Select 1
		From dbo.CasenoteActivity PreviousActivity
		Where
		PreviousActivity.ARCHV_FLAG = 'N'
		and Activity.CASVL_REFNO = PreviousActivity.CASVL_REFNO
		and Activity.CASAC_REFNO < PreviousActivity.CASAC_REFNO
		)