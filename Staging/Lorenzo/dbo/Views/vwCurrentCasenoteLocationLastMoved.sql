﻿CREATE View [dbo].[vwCurrentCasenoteLocationLastMoved] as
Select
	 SourcePatientNo = Casenote.PATNT_REFNO
	,CasenoteUniqueID = Casenote.CASNT_REFNO
	,CasenoteNo = Casenote.IDENTIFIER
	,CurrentLocationUniqueID = Volume.CURNT_SPONT_REFNO
	,CurrentCasenoteLocationCode = ServicePoint.CODE
	,CurrentCasenoteLocation = ServicePoint.DESCRIPTION
	,CasenoteActivityUniqueID = Activity.CASAC_REFNO
	,RequestStatusUniqueID = Activity.RQSTA_REFNO
	,Received = 
		Case When Activity.RQSTA_REFNO = 1426 Then
			'Y'
		Else
			'N'
		End
	,CasenoteMovementCommets = COMMENTS

/*
Select 
	 Activity.CASAC_REFNO 
	,Activity.CASVL_REFNO
	,Activity.PATNT_REFNO
	,Spont.CODE
*/
from CasenoteActivity Activity
Inner Join CasenoteVolume Volume
	on Activity.CASVL_REFNO = Volume.CASVL_REFNO
Inner Join Casenote
	on Volume.CASNT_REFNO = Casenote.CASNT_REFNO
inner join ServicePoint 
	on Volume.CURNT_SPONT_REFNO = ServicePoint.SPONT_REFNO

Where 
Activity.ARCHV_FLAG = 'N'
and Casenote.PDTYP_REFNO = 3444
and Not Exists 
	(
	Select 1 
	From CasenoteActivity PreviousActivity
	Where 
	PreviousActivity.ARCHV_FLAG = 'N'
	And Activity.PATNT_REFNO = PreviousActivity.PATNT_REFNO
	And Activity.CASAC_REFNO < PreviousActivity.CASAC_REFNO
	)
	and Casenote.PATNT_REFNO <> 13208206
--and Activity.CASAC_REFNO <> 160545662;