﻿CREATE ROLE [MaternityReportingUsers]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'MaternityReportingUsers', @membername = N'MaternityReportingUser';


GO

