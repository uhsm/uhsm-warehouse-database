﻿



CREATE FUNCTION func_Calculate_Age (@fromdate datetime, @todate datetime) RETURNS INT AS
BEGIN
	DECLARE @age as int
	SET @age = NULL
	IF @fromdate IS NOT NULL AND @todate IS NOT NULL 
	BEGIN
		SET @age = DATEDIFF(YY, @fromdate, @todate)
		IF Month(@fromdate) > Month(@todate) OR (Month(@fromdate) = Month(@todate) AND Day(@fromdate)   > Day(@todate)) SET @age = @age - 1
	END
	RETURN @age
END














