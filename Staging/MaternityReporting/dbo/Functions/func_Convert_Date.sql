﻿


CREATE FUNCTION [dbo].[func_Convert_Date] (@DateStr VARCHAR(10))
RETURNS DateTime AS  
BEGIN 
	DECLARE @DT DateTime
	SET @DT = NULL

	SET @DateStr = ltrim(rtrim(@DateStr))

	IF @DateStr LIKE '%/%/%'
	BEGIN
		--SET @DT = cast(@DateStr as DateTime)
		SET @DT = convert(datetime, @DateStr,103)
	END
	ELSE
	BEGIN
		IF IsNumeric(@DateStr)=1
		BEGIN
			SET @DT = DATEADD(D,CAST(right(@DateStr,len(@DateStr)-1) AS INT),'19160513 00:00:00')--'13/05/1916') 
		END
	END
	RETURN @DT
END







