﻿


CREATE FUNCTION dbo.func_Convert_HrsMins (@Minutes INT)  
RETURNS VARCHAR(10) AS  
BEGIN 
	DECLARE @HM VARCHAR(10)
	DECLARE @Hours INT

	SET @Hours = @Minutes / 60
	SET @Minutes = @Minutes - (@Hours * 60)

	SET @HM = CAST(@Hours AS VARCHAR(3)) + ':' + RIGHT('00'+CAST(@Minutes AS VARCHAR(2)),2)
	RETURN @HM
END










