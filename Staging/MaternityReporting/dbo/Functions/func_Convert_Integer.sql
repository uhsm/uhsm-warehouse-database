﻿


CREATE FUNCTION dbo.func_Convert_Integer (@NumStr VARCHAR(60), @Units VARCHAR(10))  
RETURNS INT AS  
BEGIN 
	DECLARE @I INT
	-- trim the input variables
	SET @NumStr = ltrim(rtrim(@NumStr))
	SET @Units = ltrim(rtrim(@Units))
	-- if the number string has the units in the end, remove them
	IF isnull(@Units ,'') <> '' IF upper(right(@NumStr,len(@Units))) = upper(@Units) SET @NumStr = ltrim(rtrim(left(@NumStr,len(@NumStr)-len(@Units))))
	-- extract the integer
	IF charindex('.',@NumStr) > 0 SET @NumStr = left(@NumStr,charindex('.',@NumStr)-1)
	SET @I = cast(case when IsNumeric(@NumStr)=1 then @NumStr else null end AS Integer) 
	RETURN @I
END



