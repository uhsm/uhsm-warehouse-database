﻿


CREATE FUNCTION dbo.func_Convert_Numeric52 (@NumStr VARCHAR(60), @Units VARCHAR(10))  
RETURNS NUMERIC (5,2) AS  
BEGIN 
	DECLARE @I NUMERIC(5,2)
	-- trim the input variables
	SET @NumStr = ltrim(rtrim(@NumStr))
	SET @Units = ltrim(rtrim(@Units))
	-- if the number string has the units in the end, remove them
	IF isnull(@Units ,'') <> '' IF upper(right(@NumStr,len(@Units))) = upper(@Units) SET @NumStr = ltrim(rtrim(left(@NumStr,len(@NumStr)-len(@Units))))
	-- extract the number
	SET @I = cast(case when IsNumeric(@NumStr)=1 then @NumStr else null end AS NUMERIC(5,2)) 
	RETURN @I
END






