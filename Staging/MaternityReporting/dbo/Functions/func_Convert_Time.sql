﻿


CREATE FUNCTION dbo.func_Convert_Time (@TimeStr VARCHAR(10))
RETURNS VARCHAR(5) AS  
BEGIN 
	DECLARE @T VARCHAR(5)
	SET @T = NULL
	IF ltrim(rtrim(@TimeStr)) LIKE '%:%'
	BEGIN
		SET @T = substring(ltrim(rtrim(@TimeStr)),1,5)
	END
	ELSE
	BEGIN
		IF IsNumeric(@TimeStr)=1
		BEGIN
			SET @T = RIGHT(Substring(Convert(Char(19),(DATEADD(S,CAST(ltrim(rtrim(@TimeStr)) AS INT),'01 JANUARY 1900')) , 120),1,23),5) 
		END
	END
	RETURN @T
END







