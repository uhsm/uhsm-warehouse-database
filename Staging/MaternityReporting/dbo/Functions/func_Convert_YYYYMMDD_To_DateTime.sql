﻿





CREATE FUNCTION [dbo].[func_Convert_YYYYMMDD_To_DateTime]
(
@DateStr VARCHAR(8)
)
RETURNS DateTime
AS
BEGIN
	DECLARE @TempDateStr VARCHAR(10)
	DECLARE @TempDT DateTime	

	SET @TempDateStr = SUBSTRING(@DateStr,7,2) + '/' + SUBSTRING(@DateStr,5,2) + '/' + SUBSTRING(@DateStr,1,4) 

	IF IsDate(@TempDateStr) = 1 SET @TempDT = CAST(@TempDateStr AS DateTime)
	ELSE	SET @TempDT = NULL

	-- Return the result of the function
	RETURN @TempDT

END









