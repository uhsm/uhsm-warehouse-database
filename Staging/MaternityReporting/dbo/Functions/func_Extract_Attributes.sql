﻿/*
**********************************************************************************************************************************************
**	NAME:			func_Extract_From_Attributes
**	DESCRIPTION:	Grabs info out of Attributes
**	AUTHOR:			Unknown
**	VERSIONS:	
**			1.1	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Attributes has increased from (5000) to (7000)
**											tbl_Notes_Copy.Noteno has increased from (5) to (25)
**********************************************************************************************************************************************
*/
CREATE FUNCTION [dbo].[func_Extract_Attributes] (
@pointer int,
@noteno varchar(25),
@code varchar(5) ) RETURNS varchar(7000)
AS
BEGIN
 DECLARE @Attributes varchar(7000)
 DECLARE @count INT

 SELECT @count = count(*) FROM tbl_Notes_Copy WITH (NOLOCK)
	WHERE Pointer = @pointer
	AND   substring(Noteno,1,len('x'+@noteno+'x')-2) = @noteno
	AND  Code = @code

 IF @count > 1
	SET @Attributes = 'ERROR' -- +@code
 ELSE
 IF @count = 0
	SET @Attributes = NULL
 ELSE	
 SET @Attributes = (SELECT Attributes FROM tbl_Notes_Copy WITH (NOLOCK)
	WHERE Pointer = @pointer
	AND   substring(Noteno,1,len('x'+@noteno+'x')-2) = @noteno
	AND  Code = @code)


 RETURN (@Attributes)
END
