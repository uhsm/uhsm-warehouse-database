﻿/*
**********************************************************************************************************************************************
**	NAME:			func_Extract_From_Attributes
**	DESCRIPTION:	Grabs info out of Attributes
**	AUTHOR:			Unknown
**	VERSIONS:	
**			1.1	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Attributes has increased from (5000) to (7000)
**********************************************************************************************************************************************
*/

Create FUNCTION [dbo].[func_Extract_From_Attributes_copy] (
@Attributes varchar(7000), @ItemName varchar(60)
) RETURNS VARCHAR(1000)
AS
BEGIN
	DECLARE @AttrRes VARCHAR(1000) 
	DECLARE @StartPos INT
	DECLARE @EndPos INT

	SET @StartPos = charindex(@ItemName+',',@Attributes)
	IF @StartPos > 0 
	BEGIN
		SET @EndPos = CHARINDEX('|',@Attributes,@StartPos+Len(@ItemName))
		IF @EndPos > 0
			SET @AttrRes = SUBSTRING(@Attributes,@StartPos+Len(@ItemName)+2,@EndPos-(@StartPos+Len(@ItemName)+2))
		ELSE
			SET @AttrRes = RIGHT(rtrim(@Attributes),Len(rtrim(@Attributes))-@StartPos-Len(@ItemName)-1)
	END
	ELSE
		SET @AttrRes = NULL
	
	SET @AttrRes = ltrim(rtrim(@AttrRes))
	IF @AttrRes = '' SET @AttrRes = NULL
	RETURN (@AttrRes)
END
