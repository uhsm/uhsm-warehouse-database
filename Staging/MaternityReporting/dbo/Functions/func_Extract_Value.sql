﻿/*
**********************************************************************************************************************************************
**	NAME:			func_Extract_Value
**	DESCRIPTION:	Grabs info out of Attributes
**	AUTHOR:			Unknown
**	VERSIONS:	
**			1.1	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**********************************************************************************************************************************************
*/
CREATE    FUNCTION [dbo].[func_Extract_Value] (
@pointer int,
@noteno varchar(25),
@code varchar(5) ) 
RETURNS VARCHAR(60)
AS
BEGIN

	DECLARE @count INT
 	DECLARE @nvalue varchar(60)

 	SELECT @count = count(*)
	FROM tbl_Notes_Copy WITH(NOLOCK)
	WHERE Pointer = @pointer
	AND   substring(Noteno,1,len('x'+@noteno+'x')-2) = @noteno
	AND  Code = @code

	IF @count > 1
		SET @nvalue = 'ERROR' -- +@code+' '+dbo.func_ShowNoteno(@noteno)+' '+cast(@pointer as varchar(6))
	ELSE
	IF @count = 0
		SET @nvalue = NULL
	ELSE
	 	SET @nvalue = (SELECT rtrim(case 	when len(Nvalue) = 30 and Attributes like '{{%'
				then Nvalue + SUBSTRING(Attributes, CHARINDEX('{{',Attributes)+2, CHARINDEX('}}',Attributes)-CHARINDEX('{{',Attributes)-2)
				else Nvalue end)
		FROM tbl_Notes_Copy WITH(NOLOCK)
		WHERE Pointer = @pointer
		AND   substring(Noteno,1,len('x'+@noteno+'x')-2) = @noteno
		AND  Code = @code)

SET @nvalue = ltrim(rtrim(@nvalue))
IF @nvalue = '' SET @nvalue = NULL
RETURN (@nvalue)

END
