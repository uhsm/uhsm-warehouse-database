﻿


CREATE         FUNCTION [dbo].[func_GetLink_GPPostcode] (@Attributes varchar(1000))
RETURNS VARCHAR(10)
AS
BEGIN
 	DECLARE @nvalue varchar(10)
	DECLARE @regpointer int
	DECLARE @linktype char(1)

	SET @nvalue = NULL

	SET @linktype = CASE WHEN CHARINDEX('Link,',@Attributes) > 0 THEN SUBSTRING (@Attributes,(CHARINDEX('Link,',@Attributes)+5),1) ELSE NULL END

 	SET @regpointer = CASE WHEN CHARINDEX('Link,',@Attributes) > 0 
				THEN SUBSTRING (@Attributes,(CHARINDEX('Link,',@Attributes)+6),(CASE WHEN (CHARINDEX('|',@Attributes) = 0 OR (CHARINDEX('|',@Attributes) < CHARINDEX('Link,',@Attributes))) THEN LEN(@Attributes) ELSE CHARINDEX('|',@Attributes) - (CHARINDEX('Link,',@Attributes) + 6) END))
				ELSE 0 END

	IF @linktype = 'L' 
	BEGIN	
		SET @regpointer =  (SELECT Organisation FROM tbl_Stafflink_Copy WHERE Linkid = @regpointer)
		IF @regpointer > 0
		BEGIN
			SELECT 	@nvalue = rtrim(Postcode)
			FROM 	tbl_Register_Copy WITH(NOLOCK)
			WHERE 	Pointer = @regpointer
			AND  	Regtype = 's'
		END
	END
	ELSE
	IF @regpointer > 0
	BEGIN
		SELECT 	@nvalue = rtrim(Postcode)
		FROM 	tbl_Register_Copy WITH(NOLOCK)
		WHERE 	Pointer = @regpointer
		AND  	Regtype = 'G'
	END

	SET @nvalue = ltrim(rtrim(@nvalue))
	RETURN (@nvalue)

END




