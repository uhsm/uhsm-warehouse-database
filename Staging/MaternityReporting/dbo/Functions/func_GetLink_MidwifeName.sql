﻿


CREATE         FUNCTION [dbo].[func_GetLink_MidwifeName] (@Attributes varchar(1000))
RETURNS VARCHAR(255)
AS
BEGIN
 	DECLARE @nvalue varchar(255)
	DECLARE @regpointer int
	DECLARE @linktype char(1)

	SET @nvalue = NULL

	SET @linktype = CASE WHEN CHARINDEX('Link,',@Attributes) > 0 THEN SUBSTRING (@Attributes,(CHARINDEX('Link,',@Attributes)+5),1) ELSE NULL END

 	SET @regpointer = CASE WHEN CHARINDEX('Link,',@Attributes) > 0 
				THEN SUBSTRING (@Attributes,(CHARINDEX('Link,',@Attributes)+6),(CASE WHEN (CHARINDEX('|',@Attributes) = 0 OR (CHARINDEX('|',@Attributes) < CHARINDEX('Link,',@Attributes))) THEN LEN(@Attributes) ELSE CHARINDEX('|',@Attributes) - (CHARINDEX('Link,',@Attributes) + 6) END))
				ELSE 0 END

	IF @linktype = 'L' SET @regpointer =  (SELECT Staffpointer FROM tbl_Stafflink_Copy WHERE Linkid = @regpointer)

	IF @regpointer > 0
	BEGIN
		SELECT 	@nvalue = ltrim(rtrim(Title))+case when ltrim(rtrim(isnull(Title,''))) = '' then '' else ' ' end+ltrim(rtrim(Forename))+case when ltrim(rtrim(isnull(Forename,''))) = '' then '' else ' ' end+ltrim(rtrim(Surname))
		FROM 	tbl_Register_Copy WITH(NOLOCK)
		WHERE 	Pointer = @regpointer
		AND  	Regtype = 'M'
	END

	SET @nvalue = ltrim(rtrim(@nvalue))
	RETURN (@nvalue)

END
















