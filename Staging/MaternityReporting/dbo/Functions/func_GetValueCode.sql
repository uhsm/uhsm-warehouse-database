﻿





CREATE  FUNCTION [dbo].[func_GetValueCode] (@FieldValue VARCHAR(100))
Returns INT
AS
BEGIN
	DECLARE @FieldCode INT
	SET @FieldCode = NULL
	IF CHARINDEX('[',@FieldValue) > 0
	BEGIN
		SET @FieldCode = SUBSTRING(@FieldValue, CHARINDEX('[',@FieldValue)+1, CHARINDEX(']',@FieldValue)-CHARINDEX('[',@FieldValue)-1) 
	END
	RETURN @FieldCode
END














