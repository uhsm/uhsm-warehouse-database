﻿


CREATE  FUNCTION [dbo].[func_GetValueDesc] (@FieldValue VARCHAR(100))
Returns VARCHAR(100) 
AS
BEGIN
	DECLARE @FieldDesc VARCHAR(100)
			
	IF CHARINDEX('[',@FieldValue) > 0
	BEGIN
		SET @FieldDesc = SUBSTRING(@FieldValue, 1,CHARINDEX('[',@FieldValue)-1)	
	END
	ELSE
	BEGIN
		SET @FieldDesc = @FieldValue
	END

	RETURN @FieldDesc
END



