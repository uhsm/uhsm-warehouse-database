﻿/*
**********************************************************************************************************************************************
**	NAME:			func_ShowNoteno
**	DESCRIPTION:	Returns a readable noteno
**	AUTHOR:			Unknown
**	VERSIONS:	
**			1.1	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**											Noteno is now ASCII and doesn't need translating!
**********************************************************************************************************************************************
*/
CREATE FUNCTION [dbo].[func_ShowNoteno] (@Noteno varchar(25))
--RETURNS varchar(20) AS  
RETURNS varchar(25) AS  
BEGIN
/*
	v1.1 : Keith Lawrence
	We don't actually need to do anything, Noteno is now readable ASCII
	so bin all the existing code and simply return what came in

	DECLARE @RES VARCHAR(20)
	SET @RES = NULL
	DECLARE @i INT
	SET @i = 1
	WHILE @i <= len(@Noteno+'x')-1
	BEGIN
		IF @RES IS NULL SET @RES = '' ELSE SET @RES = @RES+'-'
		SET @RES = @RES+RIGHT('000'+CAST(ASCII(SUBSTRING(@NOTENO,@i,1)) AS VARCHAR(3)),3)
		SET @i = @i + 1
	END
	RETURN @RES
*/
	RETURN @Noteno
END
