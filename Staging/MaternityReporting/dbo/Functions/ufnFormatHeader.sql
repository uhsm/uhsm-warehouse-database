﻿
CREATE   FUNCTION [ufnFormatHeader](@Header NVARCHAR(200))
RETURNS NVARCHAR(60)
AS
  BEGIN
-- Strip @number of extra chracters
	DECLARE @lenPhone INT ,@phoneStr NVARCHAR(200)
	WHILE PATINDEX('%[^A-Z0-9a-z_ ]%', @Header) > 0 
        SET @Header = REPLACE(@Header,SUBSTRING(@Header,PATINDEX('%[^A-Z0-9a-z_ ]%', @Header),1),' ') 

    RETURN @Header
END


