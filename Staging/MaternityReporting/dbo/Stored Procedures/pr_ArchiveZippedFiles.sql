﻿

/*
******************************************************************************************************************************************************************
**	NAME:		pr_ArchiveZippedFile											**
**	DESCRIPTION:	Moves processed zip files to the archived directory								**
**			Archives any files older than <x> days									**
**	AUTHOR:	Stephen Creek												**
**	VERSIONS:	1.0	Stephen Creek	22/10/2007	Initial version created						**
**			1.1	Stephen Creek	29/10/2007	Added routine to remove old zip files from the archive directory	**
******************************************************************************************************************************************************************
*/
CREATE    procedure [dbo].[pr_ArchiveZippedFiles]
AS
BEGIN

	SET LANGUAGE 'British'

	DECLARE @RawZipFileLoc VARCHAR(255), @ArchiveLoc VARCHAR(255)
	SELECT @RawZipFileLoc = Value FROM tbl_SiteSpecificParameters WHERE ParameterName = 'File_Location_Zip'
	SELECT @ArchiveLoc = @RawZipFileLoc + 'Archive\'

	--
	-- Move any files from the Zipped folder to the Archive folder
	--
	DECLARE @MoveCommand VARCHAR(255)
--	SET @Movecommand = 'move /y ' + @RawZipFileLoc + '*.* ' + @ArchiveLoc
	SET @MoveCommand = 'move ' + @RawZipFileLoc + '*.* ' + @ArchiveLoc
	DECLARE @Res VARCHAR(255)
	EXEC @Res = master.dbo.xp_cmdshell @MoveCommand

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	VALUES ( getdate(), 'pr_ArchiveZippedFiles', @MoveCommand, @Res )

	--
	-- Delete any files from the archive folder that are older than the specified parameter
	--

	-- Retrieve the parameter setting
	DECLARE @ArchiveDays INT
	SELECT @ArchiveDays = Value FROM tbl_SiteSpecificParameters WHERE ParameterName = 'Delete_ArchiveZip_Days'
	IF @ArchiveDays IS NULL OR @ArchiveDays < 1 SET @ArchiveDays = 30

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	VALUES ( getdate(), 'pr_ArchiveZippedFiles', 'Deleting archived zip files older than '+cast(@ArchiveDays as varchar(5))+' days', 0 )

	DECLARE @DeleteCommand VARCHAR(255)
	DECLARE @Filename VARCHAR(200)
	DECLARE @Filedate VARCHAR(20)
	DECLARE @FileDT DateTime

	CREATE TABLE #TEMP(Fname VARCHAR(200), Lvl INT, IsFile INT, FileDate VARCHAR(20))

	-- Retrieve the list of files from the unzip location
	INSERT INTO #TEMP ( Fname, Lvl, IsFile )
	EXEC master.dbo.xp_dirtree @ArchiveLoc , 1 , 1

	-- Ignore anything that is not a .bak or .log file
	DELETE FROM #TEMP WHERE (IsFile = 0) 

	-- Fetch the date/time
	UPDATE #TEMP SET FileDate = REPLACE(SUBSTRING(Fname,CHARINDEX('.',Fname)-14,14),'_','')

	-- Loop through the list deleting any files that are older than required
	DECLARE MYCURSOR CURSOR FOR 
	SELECT Fname, FileDate FROM #TEMP

	OPEN MYCURSOR 
	FETCH NEXT FROM MYCURSOR INTO @Filename, @Filedate

	WHILE @@FETCH_STATUS = 0
	BEGIN 
		-- Parse the date of the file
		SET @Filedate = substring(@Filedate,5,2)+'/'+substring(@Filedate,3,2)+'/'+substring(@Filedate,1,2)+' '+substring(@Filedate,7,2)+':'+substring(@Filedate,1,2)
		IF IsDate(@Filedate) = 1
		BEGIN
			-- Check file age 
			IF Datediff(dd, cast(@Filedate as DateTime), getdate()) > @ArchiveDays 
			BEGIN
				-- Delete the file
				SET @DeleteCommand = 'del '+@ArchiveLoc+@Filename+' /q'

				EXEC @Res = master.dbo.xp_cmdshell @DeleteCommand

				INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
				VALUES ( getdate(), 'pr_ArchiveZippedFiles', @DeleteCommand, @Res )

			END
		END
		ELSE
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_ArchiveZippedFiles', 'Unsure about archiving file "'+@Filename+'"', 1 )
		END

		FETCH NEXT FROM MYCURSOR INTO @Filename, @Filedate
	END

	CLOSE MYCURSOR
	DEALLOCATE MYCURSOR

	DROP TABLE #TEMP

END

