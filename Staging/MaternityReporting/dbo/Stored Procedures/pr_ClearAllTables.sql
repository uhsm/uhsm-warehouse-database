﻿

/*
**********************************************************************************************************************************************
**	NAME:		pr_ClearAllTables									**
**	DESCRIPTION:	Clears down all local maternity reporting tables						**
**	AUTHOR:	Stephen Creek										**
**	VERSIONS:	1.0	Stephen Creek	18/10/2007	Initial version created				**
**			1.1	Stephen Creek	22/10/2007	Added logging					**	
**			1.2	Stephen Creek	29/11/2007	Updated with new tables				**
**			1.3	Stephen Creek	08/02/2008	Added tbl_BloodSpotScreening, tbl_Stafflink	**
**********************************************************************************************************************************************
*/
CREATE PROCEDURE [dbo].[pr_ClearAllTables] AS
BEGIN
	TRUNCATE TABLE [tbl_Notes_Copy]
	TRUNCATE TABLE [tbl_Register_Copy]
	TRUNCATE TABLE [tbl_Stafflink_Copy] -- v1.3

	TRUNCATE TABLE [tbl_Patients]
	TRUNCATE TABLE [tbl_FamilyHistory]
	TRUNCATE TABLE [tbl_MedicalHistory]
	TRUNCATE TABLE [tbl_ObstetricSummary]
	TRUNCATE TABLE [tbl_PersonalInformation]
	TRUNCATE TABLE [tbl_SocialHistory]
	TRUNCATE TABLE [tbl_ObstetricProblemSummary]
	TRUNCATE TABLE [tbl_InpatientSpells]
	TRUNCATE TABLE [tbl_AdmissionTransferEpisodes]
	TRUNCATE TABLE [tbl_Pregnancy]
	TRUNCATE TABLE [tbl_Referrals]
	TRUNCATE TABLE [tbl_ClinicalReviews]
	TRUNCATE TABLE [tbl_FetalObservations]
	TRUNCATE TABLE [tbl_ReviewExaminations]
	TRUNCATE TABLE [tbl_ReviewInvestigations]
	TRUNCATE TABLE [tbl_ReviewTreatments]
	TRUNCATE TABLE [tbl_PregnancyCarePlans]
	TRUNCATE TABLE [tbl_SocialServicesReferral]
	TRUNCATE TABLE [tbl_Delivery]
	TRUNCATE TABLE [tbl_Baby]
	TRUNCATE TABLE [tbl_DeathOfBaby]
	TRUNCATE TABLE [tbl_BloodSpotScreening] -- v1.3

	TRUNCATE TABLE [tbl_EventLog]
	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	VALUES ( getdate(),  'pr_ClearAllTables', 'All local reporting tables cleared', 0 )
END
