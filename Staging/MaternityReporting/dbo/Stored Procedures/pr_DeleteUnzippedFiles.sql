﻿




/*
****************************************************************************************************************
**	NAME:		pr_DeleteUnzippedFiles						**
**	DESCRIPTION:	Deletes previously unzipped files					**
**	AUTHOR:	Stephen Creek							**
**	VERSIONS:	1.0	Stephen Creek	22/10/2007	Initial version created	**
****************************************************************************************************************
*/
CREATE    procedure [dbo].[pr_DeleteUnzippedFiles]
AS
BEGIN
	DECLARE @UnzipFileLoc VARCHAR(255)
	SELECT @UnzipFileLoc = Value FROM tbl_SiteSpecificParameters WHERE ParameterName = 'File_Location_Unzip'

	DECLARE @Res INT
	SET @Res = 999

	IF substring(replace(@@version,'  ',' ') , 1 , 25) = 'Microsoft SQL Server 2000' 
	BEGIN
		-- On 2000, we can just delete all of the files present
		DECLARE @DeleteCommand VARCHAR(255)
		SET @DeleteCommand = 'del ' + @UnzipFileLoc + '*.* /q'

		EXEC @Res = master.dbo.xp_cmdshell @DeleteCommand

		INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
		VALUES ( getdate(), 'pr_DeleteUnzippedFiles', @DeleteCommand, @Res )
	END
	ELSE
	BEGIN
		-- On non-2000 servers, we need to keep the most recent .bak file 
		-- and all subsequent log files
		CREATE TABLE #TEMP(Fname VARCHAR(200), Lvl INT, IsFile INT, FileDate VARCHAR(20))

		-- Fetch the list of files from the unzipped directory
		INSERT INTO #TEMP (Fname, Lvl, IsFile )
		EXEC master.dbo.xp_dirtree @UnzipFileLoc , 1 , 1

		-- Ignore anything that's not a file
		DELETE FROM #TEMP WHERE IsFile = 0

		-- Fetch the date/times for the files
		UPDATE #TEMP SET 
		FileDate = REPLACE(SUBSTRING(Fname,CHARINDEX('.',Fname)-14,14),'_','')

		-- Loop round the list of files and call the unzip utility
		DECLARE MYCURSOR CURSOR FOR 
		SELECT Fname FROM #TEMP ORDER BY FileDate DESC

		DECLARE @FileName VARCHAR(200)
		OPEN MYCURSOR 
		FETCH NEXT FROM MYCURSOR INTO @FileName

		DECLARE @BakFound INT
		SET @BakFound = 0

		WHILE @@FETCH_STATUS = 0
		BEGIN 
			IF @BakFound = 1
			BEGIN
				-- Already found a .bak file so anything else can be deleted
				SET @DeleteCommand = 'del ' + @UnzipFileLoc + @FileName + ' /q'
				EXEC @Res = master.dbo.xp_cmdshell @DeleteCommand

				INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
				VALUES ( getdate(), 'pr_DeleteUnzippedFiles', @DeleteCommand, @Res )
			END
			ELSE
			BEGIN
				IF RIGHT(@FileName,4) = '.bak' SET @BakFound = 1
			END

			FETCH NEXT FROM MYCURSOR INTO @FileName

		END

		CLOSE MYCURSOR
		DEALLOCATE MYCURSOR

		DROP TABLE #TEMP

	END
END

