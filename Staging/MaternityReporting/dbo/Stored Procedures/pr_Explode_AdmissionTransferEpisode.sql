﻿/*
****************************************************************************************************************
**	NAME:		pr_Explode_AdmissionTransferEpisode			**
**	DESCRIPTION:	Runs the New Baby 'explode' routine				**
**	AUTHOR:	Stephen Creek							**
**	VERSIONS:	1.0	Stephen Creek	08/11/2007	Initial version created	**
**				1.1	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**												Noteno has changed from a 5 byte binary coded structured key
**												to a 25 byte (5 x 5 bytes) ASCII coded key.
****************************************************************************************************************
*/

CREATE  PROCEDURE [dbo].[pr_Explode_AdmissionTransferEpisode]
	(@pointer int
	,@p_spell_number int
	,@p_noteno char(25))
AS
BEGIN
	DECLARE @NOTENO  CHAR(25)
	DECLARE @l_admissiontransfer_number varchar(30)
	DECLARE @SQLError INT

	DECLARE get_admissiontransfer CURSOR FOR
	SELECT Noteno
 	FROM tbl_Notes_Copy
	WHERE Code = 'ZEAEZ'
	AND Pointer = @pointer
	AND substring(Noteno,1,len(@p_noteno)) = @p_noteno
	ORDER BY Noteno

	OPEN get_admissiontransfer
	FETCH next from get_admissiontransfer into @NOTENO

	SET @l_admissiontransfer_number = 0

	WHILE @@fetch_status = 0
	BEGIN
		--v1.1 SET @NOTENO = SUBSTRING(@NOTENO,1,3)
		SET @NOTENO = SUBSTRING(@NOTENO,1,15)

		SET @l_admissiontransfer_number = @l_admissiontransfer_number + 1

		INSERT INTO [tbl_AdmissionTransferEpisodes] 
			(Patient_Pointer,
			Spell_Number,
			AdmissionTransfer_Number,
			[Adm/trans deletion allowed],
			[Adm/trans episode number],
			[Adm/trans event number],
			[Admission/transfer flag],
			[Admission/transfer type],
			[BAPM Source of admission],
			[Consultants code],
			[Consultants name],
			[COPPISH admission type],
			[COPPISH admission/transfer from],
			[Date of admission/transfer],
			[External adm/trans code],
			[General Practitioner Name],
			[GP Address code],
			[GP code],
			[GP Practice code],
			[Hospital adm this pregnancy],
			[Internal adm/trans number],
			[Location admitted/trans to],
			[Method of admission/transfer],
			[Significant Facilty],
			[Source of admission/transfer],
			[Specialty code],
			[Time of admission/transfer],
			[Type of unit admitted/transferred to],
			[BirthRate date of arrival],
			[BirthRate date of departure],
			[BirthRate time of arrival],
			[BirthRate time of departure],
			[Still on Labour Ward],
			[Admission Plan],
			[Admission Plan Text],
			[Admission/transfer reason 1],
			[Admission/transfer reason 1 Text],
			[Admission/transfer reason 2],
			[Admission/transfer reason 2 Text],
			[Admission/transfer reason 3],
			[Admission/transfer reason 3 Text],
			[BAPM category of admission],
			[Brought to unit by],
			[Brought to unit by Text],
			[COPPISH Admission Reason],
			[GP informed],
			[GP informed Text],
			[Placed in],
			[Placed in Oxygen],
			[Reason for admission 4],
			[Reason for admission 4 Text],
			[Reason for admission 5],
			[Reason for admission 5 Text],
			[Reason for admission 6],
			[Reason for admission 6 Text],
			[Reason for discharge],
			[Reason for discharge Text],
			[Transport Method],
			[Transport Method Oxygen],
			[Transport Method Ventilated])
		    VALUES
			(@pointer
			,@p_spell_number
			,@l_admissiontransfer_number
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAEG')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAEC')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAED')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAEA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAEN')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSBAS')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAE8')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAE7')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAH2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAH1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAE1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAEE')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMDI4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMDI2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMDI1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMDI3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAEM')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAEF')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAE4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAE5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZESSF')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAE6')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAE9')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAE2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSBAL')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZADTc')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZADTa')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZADTd')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZADTb')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr18')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSARV')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSARV'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAR1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEAR1'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAR2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEAR2'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAR3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEAR3'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAR0')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSARR')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSARR'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAR7')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSARW')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSARW'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSART')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSART'),'Oxygen')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAR4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEAR4'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAR5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEAR5'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEAR6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEAR6'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEDDR')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEDDR'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSARS')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSARS'),'Oxygen')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSARS'),'Ventilated'))

			SET @SQLError = @@error
			IF @SQLError > 0
			BEGIN
				INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
				VALUES ( getdate(), 'pr_Explode_AdmissionTransferEpisode', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating admission/transfer record for spell ' + @p_spell_number + ' for patient ' + cast(@pointer as varchar), -1 )
			END

		FETCH NEXT FROM get_admissiontransfer INTO @NOTENO
	 END
	
	CLOSE get_admissiontransfer
 	DEALLOCATE get_admissiontransfer

END
