﻿

/*
****************************************************************************************************************
**	NAME:		pr_Explode_All							**
**	DESCRIPTION:	Runs all the New 'explode' routines				**
**	AUTHOR:	Stephen Creek							**
**	VERSIONS:	1.0	Stephen Creek	01/11/2007	Initial version created	**
****************************************************************************************************************
*/
CREATE PROCEDURE [dbo].[pr_Explode_All]
AS
BEGIN

SET NOCOUNT ON

SET LANGUAGE 'British'

EXEC dbo.pr_Explode_Patients
EXEC dbo.pr_Explode_Pregnancy 0

END

