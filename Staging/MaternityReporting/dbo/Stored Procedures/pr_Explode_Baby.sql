﻿/*
**********************************************************************************************************************************************
**	NAME:		pr_Explode_Baby									**
**	DESCRIPTION:	Runs the New Baby 'explode' routine							**
**	AUTHOR:	Stephen Creek										**
**	VERSIONS:	1.0	Stephen Creek	06/11/2007	Initial version created				**
**			1.1	Stephen Creek	16/01/2008	Added error trap for missing Birth Order		**
**			1.2	Stephen Creek	08/02/2008	Corrected use of len() function			**
**			1.3	Stephen Creek	11/02/2008	Added Pregnancy_ID				**
**								Corrected use of Baby_Number/Birth Order	**
**			1.4	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**												Noteno has changed from a 5 byte binary coded structured key
**												to a 25 byte (5 x 5 bytes) ASCII coded key.
**********************************************************************************************************************************************
*/

CREATE  PROCEDURE [dbo].[pr_Explode_Baby]
	(@pointer int
	,@p_pregnancy_id int --v1.3
	,@p_pregnancy_number varchar(30)
	,@p_delivery_number int
	,@p_noteno varchar(25))
AS
BEGIN
	DECLARE @NOTENO  VARCHAR(25)
	DECLARE @l_baby_number int
	DECLARE @l_birth_order varchar(30)

	DECLARE get_baby CURSOR FOR
	SELECT Noteno
 	FROM tbl_Notes_Copy
	WHERE Code = 'ZMHB1'
	AND Pointer = @pointer
	AND substring(Noteno,1,len('x'+@p_noteno+'x')-2) = @p_noteno  --v1.2
	ORDER BY Noteno

	OPEN get_baby
	FETCH next from get_baby into @NOTENO

	DECLARE @SQLError INT
	SET @l_baby_number = 0  --v1.3

	WHILE @@fetch_status = 0
	BEGIN
		--v1.4 SET @NOTENO = SUBSTRING(@NOTENO,1,3)
		SET @NOTENO = SUBSTRING(@NOTENO,1,15)

		-- v1.3 start
		SET @l_baby_number = @l_baby_number + 1 
		SET @l_birth_order = dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbor') 
		-- Error trap for missing Birth Order - found at Barnsley 16/1
		IF isnull(@l_birth_order,'') = ''
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_Baby', 'Missing Birth Order encountered creating baby record '+cast(@l_baby_number as varchar)+' for pregnancy ' + cast(@p_pregnancy_id as varchar)+ ' for patient ' + cast(@pointer as varchar), -1 )
		END
		-- v1.3 end

		INSERT INTO [tbl_Baby] 
			(Patient_Pointer,
			Pregnancy_ID,  --v1.3
			Delivery_Number,
			Baby_Number, --v1.3
			[Pregnancy number],
			[Birth Order], --v1.3
			[Abdominal Lie],
			[Abdominal station],
			[Actual Place of Birth],
			[Actual Place of Birth Text],
			[Additional important reasons 1],
			[Additional important reasons 1 Text],
			[Additional important reasons 2],
			[Additional important reasons 2 Text],
			[Adm to del interval(hrs:mins)],
			[Anaesthetic inserted by],
			[Anaesthetic inserted by Grade],
			[Any failed assisted type deliveries],
			[Any familiar staff present],
			[Any shoulder dystocia],
			[Any shoulder dystocia Method of Management 5],
			[Any shoulder dystocia Time taken 5],
--20
			[Any shoulder dystocia Details],
			[Any shoulder dystocia Method of Management 1],
			[Any shoulder dystocia Time taken 1],
			[Any shoulder dystocia Method of Management 2],
			[Any shoulder dystocia Time taken 2],
			[Any shoulder dystocia Method of Management 3],
			[Any shoulder dystocia Time taken 3],
			[Any shoulder dystocia Method of Management 4],
			[Any shoulder dystocia Time taken 4],
			[Application 1],
			[Application 2],
			[Assigned Midwife present],
			[Assistance decision taken by],
			[Assistance decision taken by Status],
			[Assisted delivery decision by],
			[Assisted delivery decision by Grade],
			[Assisted_breech],
			[Birth Analgesia/Anaesthesia],
			[Bishops Score],
			[Booked_professional],
--40
			[Breast feeding],
			[Breast feeding Date 1st put baby to breast],
			[Breast feeding Time 1st put baby to breast],
			[Breast feeding Date 1st given breast milk],
			[Breast feeding Time 1st given breast milk],
			[Breast feeding Method of feeding],
			[Breast feeding Details],
			[Breastfeeding initiatives within 48 hours of delivery],
			[Breech diagnosis time],
			[Caput],
			[Catheterised],
			[Caudal],
			[Caudal Details],
			[Cephalic position],
			[Cervical consistency],
			[Cervical dilation 1],
			[Cervical dilation 2],
			[Cervical length],
			[Cervical position],
			[Classification of urgency 1],
--60
			[Classification of urgency 2],
			[Combined spinal/epidural],
			[Combined spinal/epidural Details],
			[Condition of liquor],
			[Confirm Original Booking],
			[Confirm Original Booking Text],
			[Consent obtained for C/S 1],
			[Consent obtained for C/S 1 Reason],
			[Consent obtained for C/S 2],
			[Consent obtained for C/S 2 Reason],
			[Consent obtained for Vaginal Examination],
			[Consultant at Delivery],
			[Consultant episode start date],
			[Consultant episode start time],
			[Cord gases at delivery],
			[Cord gases at delivery Arterial pH],
			[Cord gases at delivery Arterial Base Excess],
			[Cord gases at delivery Venous pH],
			[Cord gases at delivery Venous Base Excess],
			[Cord gases at delivery Lowest cord blood pH],
--80
			[Cord gases at delivery Arterial pC02],
			[Cord gases at delivery Venous pCO2],
			[Cord Prolapse or Presentation],
			[Cord Prolapse or Presentation Text],
			[CTG during Labour],
			[CTG during Labour Text],
			[Date of Birth],
			[Date of decision for Emerg C/S],
			[Date of Onset of Pushing],
			[Date of Onset of Second Stage],
			[Date of recording information],
			[Date of Rupture of Membranes],
			[Date of Vaginal Examination],
			[Decide/Del Interval (hrs:mins)],
			[Decision discussed with],
			[Decision discussed with Grade],
			[Delivered by Assigned Midwife],
			[Delivery_anaesthesia],
			[Derived_fetal_distress],
			[Duration 1st Stage(hrs:mins)],
--100
			[Duration 2nd Stage(hrs:mins)],
			[E.C.V. attempted],
			[E.C.V. attempted Text],
			[Early_labour],
			[Elective C/S done early],
			[Elective_caesarean_delivery],
			[Emergency_caesarean_delivery],
			[Epidural],
			[Epidural Details],
			[Estimate of gestation],
			[Estimate of gestation Days],
			[Failed Method of Delivery],
			[Failed Method of Delivery Details],
			[Fetal blood samples taken],
			[Fetal blood samples taken Date sample taken3],
			[Fetal blood samples taken Time sample taken3],
			[Fetal blood samples taken pH3],
			[Fetal blood samples taken Base excess3],
			[Fetal blood samples taken Date sample taken4],
			[Fetal blood samples taken Time sample taken4],
--120
			[Fetal blood samples taken pH4],
			[Fetal blood samples taken Base excess4],
			[Fetal blood samples taken Date sample taken5],
			[Fetal blood samples taken Time sample taken5],
			[Fetal blood samples taken pH5],
			[Fetal blood samples taken Base excess5],
			[Fetal blood samples taken Date sample taken6],
			[Fetal blood samples taken Time sample taken6],
			[Fetal blood samples taken pH6],
			[Fetal blood samples taken Base excess6],
			[Fetal blood samples taken Date sample taken1],
			[Fetal blood samples taken Time sample taken1],
			[Fetal blood samples taken pH1],
			[Fetal blood samples taken Base excess1],
			[Fetal blood samples taken Date sample taken2],
			[Fetal blood samples taken Time sample taken2],
			[Fetal blood samples taken pH2],
			[Fetal blood samples taken Base excess2],
			[Fetal distress],
			[Fetal distress Reason],
--140
			[Fetal position],
			[Fetal_distress],
			[Final method of delivery],
			[Final method of delivery Details],
			[First additional midwife],
			[First additional midwife Grade],
			[First feed],
			[First feed Date of first feed],
			[First feed Time of first feed],
			[First feed Method],
			[First feed Other],
			[First feed offered within 1hr],
			[First feed offered within 1hr Date],
			[First feed offered within 1hr Time],
			[Forceps delivery successful],
			[Forceps delivery successful Additional procedure],
			[Forceps delivery successful Text],
			[General Anaesthetic],
			[General Anaesthetic Details],
			[Gestation at membrane rupture],
--160
			[Gestation at membrane rupture Days],
			[Indication for Elective C/S],
			[Indication for Elective C/S Text],
			[Indication for Emerg. C/S],
			[Indication for Emerg. C/S Text],
			[Indication for forceps],
			[Indication for forceps Text],
			[Indication for ventouse],
			[Indication for ventouse Text],
			[Indication Forceps/Ventouse],
			[Indication Forceps/Ventouse Text],
			[Induced_abortion],
			[Korner Actual Place of Birth],
			[Korner Analgesia/Anaesthesia],
			[Korner Intended Place of Birth],
			[Korner Method of delivery],
			[Korner Method of Labour Onset],
			[Korner Post Delivery Analgesia],
			[Korner Reason for Change],
			[Liquor state],
--180
			[Liquor volume],
			[Local Infiltration],
			[Local Infiltration Details],
			[Local_anaesthesia],
			[Local_anaesthesia_nerve_block],
			[Location at Delivery],
			[Low_cephalic_forceps_delivery],
			[Management of vaginal breech],
			[Management_of_Vaginal_breech],
			[Manoeuvres used],
--190
			[Manoeuvres used Text],
			[Mem. Rupt/Birth Int.(hrs:mins)],
			[Method of feeding at delivery],
			[Mid_forceps],
			[Midwifery team delivering],
--195
			[Moulding],
			[Name of Assigned Midwife],
			[Name of Assigned Midwife Grade],
			[Name of person delivering],
			[Name of person delivering Grade],
--200
			[Name of person recording],
			[Name of person recording Grade],
			[Name of principal assistant],
			[Name of principal assistant Grade],
			[No. contractions with forceps],
			[No. of contractions with cup],
			[No. of Midwives Involved],
			[Normal_delivery],
			[Obs_estimate_of_gestation],
			[Obstetric Consultant present],
			[Obstetric Consultant present Grade],
			[Other findings on Vaginal Examination],
			[Other findings on Vaginal Examination Text],
			[Other individuals present],
			[Other individuals present Name_1],
			[Other individuals present Status_1],
			[Other individuals present Name_2],
			[Other individuals present Status_2],
			[Other individuals present Name_3],
			[Other individuals present Status_3],
--220
			[Other individuals present Others],
			[Others present at delivery],
			[Outcome of birth],
			[Outcome of birth Death_of_Fetus],
			[Partner present at delivery],
			[Pelvimetry performed],
			[Plan change prior to delivery],
			[Position of presenting part],
			[Post_term_birth],
			[Post-term birth],
			[Presentation before Lab/Caesar],
			[Presentation before Lab/Caesar Text],
			[Presentation prior to delivery],
			[Presentation prior to delivery Text],
			[Presenting part],
			[Presenting part Text],
			[Pre-term birth],
			[Prev. failed Forcep/Ventouse],
			[Principal anaesthetist present],
			[Principal anaesthetist present Grade],
--240
			[Principal Floor Nurse],
			[Principal Floor Nurse Grade],
			[Principal paed. present],
			[Principal paed. present Grade],
			[Principal Scrub Nurse],
			[Principal Scrub Nurse Grade],
			[Prolonged_1st_stage],
			[Prolonged_2nd_stage],
			[Pudendal Block],
			[Pudendal Block Details],
			[Push Start/Del. Int.(hrs:mins)],
			[Quantity of liquor],
			[Rank_stillbth_livebth_nbr_born],
			[Reapplication required 1],
			[Reapplication required 1 Text],
			[Reapplication required 2],
			[Reapplication required 2 Details],
			[Reason for Delay],
			[Reason for doing El. C/S early],
			[Reason for doing El. C/S early Text],
--260
			[Responsible Hospital],
			[Revised post delivery findings],
			[Revised post delivery findings Fetal position],
			[Revised post delivery findings Text],
			[Room Number],
			[Rotation_forceps],
			[Scan for breech assessment],
			[Scan for breech assessment Estimated fetal weight],
			[Scan for breech assessment Attitude],
			[Scrub midwife],
			[Scrub midwife Grade],
			[Second additional midwife],
			[Second additional midwife Grade],
			[Self admin inhalation],
			[Self admin inhalation Details],
			[Shoulder_dystocia],
			[Skin - skin contact within 1hr],
			[Skin - skin contact within 1hr Details],
			[Skin to skin contact],
			[Skin to skin contact Date attempted],
--280
			[Skin to skin contact Time attempted],
			[Skin to skin contact Details],
			[Specialty Code at Delivery],
			[Spinal],
			[Spinal Details],
			[Spontaneous_breech],
			[Station of presenting part 1],
			[Station of presenting part 2],
			[Status of Operative Deliverer],
			[Stillbirth],
--290
			[Success of Regional Block],
			[Time of Birth],
			[Time of decision for Emerg C/S],
			[Time of decision to use Ventouse Application],
			[Time of forceps application],
			[Time of Onset of Pushing],
			[Time of Onset of Second Stage],
			[Time of recording information],
			[Time of Rupture of Membranes],
			[Time of use of Ventouse Application],
--300
			[Time of Vaginal Examination],
			[Time to first breast feed],
			[Time to first feed],
			[Time to first put to breast],
			[Time to skin to skin contact],
			[Transfer in labour or delivery],
			[Transfer plan date],
			[Transfer plan time],
			[Twin_delivery],
			[Type of assisted delivery],
			[Type of breech delivery],
			[Type of breech presentation],
			[Type of cup],
			[Type of decision assistant],
			[Type of decision maker],
			[Type of Fetal Monitoring],
			[Type of forceps],
			[Type of forceps Text],
			[Type of forceps used],
			[Type of forceps used Text],
--320
			[Type of person delivering],
			[Type of person recording],
			[Type of principal assistant],
			[Type of staff inserting anaesthesia],
			[Unit Responsible for Birth],
			[Unit Responsible for Birth Text],
			[Urgency of Caesarean],
			[Vacuum_delivery],
			[Vaginal Examination],
			[Vaginal Examination Reason],
			[Ventouse delivery successful],
			[Ventouse delivery successful Additional procedure],
			[Ventouse delivery successful Text],
			[Venue for operative procedure],
			[Venue for operative procedure Text],
			[Was a fetal blood sample taken],
			[Was a fetal blood sample taken Date sample taken3],
			[Was a fetal blood sample taken Time sample taken3],
			[Was a fetal blood sample taken pH3],
			[Was a fetal blood sample taken Base excess3],
--340
			[Was a fetal blood sample taken Date sample taken4],
			[Was a fetal blood sample taken Time sample taken4],
			[Was a fetal blood sample taken pH4],
			[Was a fetal blood sample taken Base excess4],
			[Was a fetal blood sample taken Date sample taken5],
			[Was a fetal blood sample taken Time sample taken5],
			[Was a fetal blood sample taken pH5],
			[Was a fetal blood sample taken Base excess5],
			[Was a fetal blood sample taken Date sample taken6],
			[Was a fetal blood sample taken Time sample taken6],
			[Was a fetal blood sample taken pH6],
			[Was a fetal blood sample taken Base excess6],
			[Was a fetal blood sample taken Date sample taken1],
			[Was a fetal blood sample taken Time sample taken1],
			[Was a fetal blood sample taken pH1],
			[Was a fetal blood sample taken Base excess1],
			[Was a fetal blood sample taken Date sample taken2],
			[Was a fetal blood sample taken Time sample taken2],
			[Was a fetal blood sample taken pH2],
			[Was a fetal blood sample taken Base excess2],
--360
			[Was baby delivered in water],
			[Was baby delivered in water Complications],
			[Was C/S done in 2nd stage],
			[Was deliverer assisted],
			[Was the baby breast fed within 1 hour],
			[Were forceps used],
			[Were forceps used Text],
			[Apgar at 1 min breakdown],
			[Apgar at 1 min breakdown Colour],
			[Apgar at 1 min breakdown Tone],
			[Apgar at 1 min breakdown Heart_rate],
			[Apgar at 1 min breakdown Respiration],
			[Apgar at 1 min breakdown Response_to_stimulus],
			[Apgar at 10 min breakdown],
			[Apgar at 10 min breakdown Colour],
			[Apgar at 10 min breakdown Tone],
			[Apgar at 10 min breakdown Heart_rate],
			[Apgar at 10 min breakdown Respiration],
			[Apgar at 10 min breakdown Response_to_stimulus],
			[Apgar at 3 min breakdown],
--380
			[Apgar at 3 min breakdown Colour],
			[Apgar at 3 min breakdown Tone],
			[Apgar at 3 min breakdown Heart_rate],
			[Apgar at 3 min breakdown Respiration],
			[Apgar at 3 min breakdown Response_to_stimulus],
			[Apgar at 5 min breakdown],
			[Apgar at 5 min breakdown Colour],
			[Apgar at 5 min breakdown Tone],
			[Apgar at 5 min breakdown Heart_rate],
			[Apgar at 5 min breakdown Respiration],
			[Apgar at 5 min breakdown Response_to_stimulus],
			[Apgar Score at 1 minute],
			[Apgar Score at 10 minutes],
			[Apgar Score at 3 minutes],
			[Apgar Score at 5 minutes],
			[Delay to first breath],
			[Delay to first breath First breath],
			[Delay to first heart beat],
			[Delay to first heart beat Cardiac massage given],
			[Delay to first heart beat First beat at],
--400
			[Drugs given for Resuscitation],
			[Drugs given for Resuscitation Naloxone_(Narcan)],
			[Drugs given for Resuscitation Dextrose],
			[Drugs given for Resuscitation Bicarbonate],
			[Drugs given for Resuscitation Atropine],
			[Drugs given for Resuscitation Adrenaline],
			[Drugs given for Resuscitation Other],
			[Method of Resuscitation 1],
			[Method of Resuscitation 2],
			[Method of Resuscitation 3],
			[Method of Resuscitation 4],
			[Method of Resuscitation 5],
			[Method of Resuscitation 6],
			[Method of Resuscitation 7],
			[Method of Resuscitation 8],
			[Name of person giving drugs],
			[Name of person giving drugs Grade],
			[National Resuscitation Code],
			[Resuscitation attempted],
			[Spont. respiration achieved],
--420
			[Spont. respiration achieved At minute],
			[Total Care Resuscitation],
			[Type of person giving drugs],
			[1st Dose vitamin K given],
			[1st Dose vitamin K given Location],
			[1st Dose vitamin K given Route & Dose],
			[1st Dose vitamin K given Date given],
			[1st Dose vitamin K given Time given],
			[2nd Dose vitamin K given],
			[2nd Dose vitamin K given Location],
			[2nd Dose vitamin K given Route & Dose],
			[2nd Dose vitamin K given Date given],
			[2nd Dose vitamin K given Time given],
			[3rd Dose vitamin K given],
			[3rd Dose vitamin K given Location],
			[3rd Dose vitamin K given Route & Dose],
			[3rd Dose vitamin K given Date given],
			[3rd Dose vitamin K given Time given],
			[Admitted directly to NNU],
			[Admitted directly to NNU Reason],
--440
			[Admitted directly to NNU Other],
			[Baby Discharge Summary Printed],
			[Baby Discharge Summary Printed filename],
			[Baby District number],
			[Baby examined],
			[Baby examined Text],
			[Baby`s first temperature],
			[Babys blood group],
			[Babys Casenote number],
			[Babys ethnic category],
			[Babys first name],
			[Babys Hospital number],
			[Babys NHS number],
			[Babys NHS number status],
			[Babys NHS number status GUID],
			[Babys NHS number status Username],
			[Babys NHS number status Date updated],
			[Babys NHS number status Time updated],
			[Babys rhesus group],
			[Babys Surname],
--460
			[Babys Surname at discharge],
			[Birth register],
			[Birth register Filename],
			[Birth Registration status],
			[Birth Registration status Username],
			[Birth Registration status Date updated],
			[Birth Registration status Time updated],
			[Birth Registration status Comments],
			[Birth weight in grams],
			[Congenital anomaly],
			[Congenital anomaly Downs],
			[Congenital anomaly Neural Tube Defect],
			[Congenital anomaly Absent Digits],
			[Congenital anomaly Cleft Lip],
			[Congenital anomaly Cleft Palate],
			[Congenital anomaly Diagnosed Antenatally],
			[Congenital anomaly Other],
			[Consent obtained for vitamin K],
			[Cord length],
--480
			[Cord round neck],
			[Cord round neck Text],
			[Direct coombs test],
			[Does baby look this gestation],
			[Does baby look this gestation Baby],
			[Does baby look this gestation Gestation estimate],
			[Have cord samples been taken],
			[Have cord samples been taken Sample 1],
			[Have cord samples been taken Sample 2],
			[Have cord samples been taken Sample 3],
			[Have cord samples been taken Text],
			[Head circumference at Birth],
			[Internal Baby Reference],
			[Is this also the Baby`s surname],
			[Korner return status],
			[Korner return status Username],
			[Korner return status Date updated],
			[Korner return status Time updated],
			[Korner return status Comments],
			[Length of Baby at Birth],
--500
			[Name of prsn who examined baby],
			[Name of prsn who examined baby Grade],
			[Neonatal medical problems],
			[Neonatal medical problems Text],
			[Number of vessels in cord],
			[Other congenital anomalies],
			[Other congenital anomalies Text],
			[Paediatric Consultant on duty],
			[Paediatrician involved],
			[Paediatrician involved Text],
			[Placenta Weight],
			[Ponderal Index],
			[Sex of baby],
			[True knot in cord],
			[Type of prsn who examined baby],
			[Abdominal examination],
			[Abdominal examination Abdomen],
			[Abdominal examination Spleen],
			[Abdominal examination Liver],
			[Abdominal examination Kidneys],
--520
			[Abdominal examination Anus],
			[Abdominal examination Umbilicus],
			[Abdominal examination Genitalia],
			[Abdominal examination Text],
			[Additional comments],
			[Additional comments Text],
			[Any baby problems],
			[Any baby problems Text],
			[Any skin problems detected],
			[Any skin problems detected Bruising],
			[Any skin problems detected Rash],
			[Any skin problems detected Naevus],
			[Any skin problems detected Text],
			[Baby assessment completed by],
			[Baby assessment completed by Status],
			[Baby Discharged To],
			[Baby Problems at Discharge],
			[Baby Problems at Discharge Irritability],
			[Baby Problems at Discharge Infection],
			[Baby Problems at Discharge Jaundice],
--540
			[Baby Problems at Discharge Nappy Rash],
			[Baby Problems at Discharge Other Rash],
			[Baby Problems at Discharge Constipation],
			[Baby Problems at Discharge Text],
			[Baby screening completed by],
			[Baby screening completed by Status],
			[Baby temperature],
			[Baby weight at com. discharge],
			[Babys Discharge Summary],
			[Babys Discharge Summary Filename],
			[BCG accepted],
			[BCG given],
			[BCG given Details],
			[BCG injection],
			[BCG injection Comments],
			[BCG injection Injection site],
			[BCG injection Location],
			[BCG injection Batch number],
			[BCG injection Date given],
			[BCG injection Time given],
--560
			[BCG offered],
			[Bowels opened],
			[Bowels opened Stool],
			[Cardiovascular examination],
			[Cardiovascular examination Heart sounds],
			[Cardiovascular examination Heart murmurs],
			[Cardiovascular examination Femoral pulses],
			[Cardiovascular examination Text],
			[Child still with mother],
			[Child still with mother Reason],
			[Child still with mother Text],
			[Childs condition now],
			[Childs condition now Text],
			[CNS examination],
			[CNS examination Muscle tone],
			[CNS examination Posture],
			[CNS examination Movement],
			[CNS examination Cry],
			[CNS examination Grasp],
			[CNS examination Moro],
--580
			[CNS examination Text],
			[Confirmed Congenital anomalies],
			[Confirmed Congenital anomalies Text],
			[Congen. Hip Dislocation Risk],
			[Congen. Hip Dislocation Risk Scan],
			[Congen. Hip Dislocation Risk Text],
			[Congenital anomaly at birth 1],
			[Congenital anomaly at birth 1 Text],
			[Congenital anomaly at birth 2],
			[Congenital anomaly at birth 2 Text],
			[Congenital anomaly at birth 3],
			[Congenital anomaly at birth 3 Text],
			[Cord status at discharge],
			[Date BCG given],
			[Date feeding method changed],
			[Date feeding method changed Reason],
			[Date Hearing screening test taken],
			[Date of Baby PN Assessment],
			[Details of Hearing screening],
			[Discharge Weight],
--600
			[Eyes],
			[Eyes Swab],
			[Facial examination],
			[Facial examination Face],
			[Facial examination Red reflex],
			[Facial examination Eyes],
			[Facial examination Ears],
			[Facial examination Nose],
			[Facial examination Palate],
			[Facial examination Mouth],
			[Facial examination Text],
			[Feeding at 10 days],
			[Feeding pattern],
			[Feeding problems],
			[Feeding problems Text],
			[Follow up arrangements],
			[Follow up arrangements Text],
			[General examination],
			[General examination Colour],
			[General examination Skin],
--620
			[General examination Body shape],
			[General examination Text],
			[Head and neck examination],
			[Head and neck examination Sutures],
			[Head and neck examination Fontanelles],
			[Head and neck examination Head],
			[Head and neck examination Neck],
			[Head and neck examination Text],
			[Hearing Risk Factors],
			[Hearing Risk Factors Text],
			[Hearing screening accepted],
			[Hearing screening offered],
			[Hearing screening offered Reason],
			[Hearing test],
			[Hearing test left ear],
			[Hearing test right ear],
			[Hepatitis B Risk],
			[Hepatitis Vaccine],
			[Hepatitis Vaccine Injection site],
			[Hepatitis Vaccine Location],
--640
			[Hepatitis Vaccine Batch number],
			[Hepatitis Vaccine Date given],
			[Hepatitis Vaccine Time given],
			[Hip examination],
			[Hip examination Text],
			[Hip screening scan],
			[Hypoglycaemia],
			[Hypoglycaemia Lowest glucose],
			[Is baby discharged with mother],
			[Is baby discharged with mother Because],
			[Is baby discharged with mother Text],
			[Is the baby still on NNU],
			[Is the child with you now],
			[Is the child with you now Reason],
			[Is the child with you now Text],
			[Length of Neonatal Unit stay],
			[Limb/back examination],
			[Limb/back examination Arms],
			[Limb/back examination Hands],
			[Limb/back examination Spine],
--660
			[Limb/back examination Legs],
			[Limb/back examination Feet],
			[Limb/back examination Text],
			[Maternal ABO/Rhesus antibodies],
			[Maternal ABO/Rhesus antibodies Text],
			[Metabolic Screen (PKU)],
			[Metabolic Screen (PKU) When],
			[Metabolic Screen (PKU) Result],
			[Method of feeding at discharge],
			[Neonatal diagnosis 1],
			[Neonatal diagnosis 1 Text],
			[Neonatal diagnosis 2],
			[Neonatal diagnosis 2 Text],
			[Neonatal diagnosis 3],
			[Neonatal diagnosis 3 Text],
			[Neonatal diagnosis 4],
			[Neonatal diagnosis 4 Text],
			[Neonatal diagnosis 5],
			[Neonatal diagnosis 5 Text],
			[Neonatal diagnosis 6],
--680
			[Neonatal diagnosis 6 Text],
			[Other examination findings],
			[Other examination findings Text],
			[Outpatients appt to be sent],
			[Place Hearing screening test taken 1],
			[Place Hearing screening test taken 1 Text],
			[Place Hearing screening test taken 2],
			[Planned feeding mthd at disch.],
			[Post Discharge baby illness],
			[Post Discharge baby illness Details],
			[Postnatal admission to NNU],
			[Postnatal admission to NNU Text],
			[Presence/Severity of Jaundice],
			[Presence/Severity of Jaundice Maximum SBR],
			[Presence/Severity of Jaundice Phototherapy],
			[Reason BCG not given],
			[Reason for ending breast feeds],
			[Reason for Rejection],
			[Recommendations],
			[Recommendations Text],
--700
			[Referrals made],
			[Referrals made Audiologist],
			[Referrals made Orthopaedic surgeon],
			[Referrals made Other],
			[Referrals made Second],
			[Respiratory examination],
			[Respiratory examination Chest auscultation],
			[Respiratory examination Text],
			[Result of Hearing screening test - Left Ear],
			[Result of Hearing screening test - Right Ear],
			[Serum bilirubin result],
			[Serum bilirubin result Max SBR],
			[Time of Baby PN Assessment],
			[Type of persn doing assessment],
			[Umbilicus],
			[Umbilicus Swab],
			[Urine passed])
--717
		    VALUES
			(@pointer
			,@p_pregnancy_id  --v1.3
			,@p_delivery_number
			,@l_baby_number --v1.3
			,@p_pregnancy_number
			,@l_birth_order
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPC')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb05')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb05'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb92')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb92'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb97')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb97'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCTV')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb36') + ',' + dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb36')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb36'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb77')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb43')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb34')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb34'),'0Method of Management 5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb34'),'1Time taken 5')
--20
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb34'),'2Details')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb34'),'Method of Management 1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb34'),'Time taken 1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb34'),'Method of Management 2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb34'),'Time taken 2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb34'),'Method of Management 3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb34'),'Time taken 3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb34'),'Method of Management 4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb34'),'Time taken 4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPO')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPr')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbAI')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb98')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb98'),'Status')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMba5') + ',' + dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMba5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMba5'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME58')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb70')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOQJ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME05')
--40
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC74')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC74'),'Date 1st put baby to breast')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC74'),'Time 1st put baby to breast')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC74'),'Date 1st given breast milk')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC74'),'Time 1st given breast milk')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC74'),'Method of feeding')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC74'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC72')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb53')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPG')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPq')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr06')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zmr06'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPE')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOQG')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPD')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOQF')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOQH')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOQI')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb47')
--60
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb59')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr08')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zmr08'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb25')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb88')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb88'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb45')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb45'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb49')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb49'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOQL')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCTQ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCTR')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCTS')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPON')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPON'),'Arterial pH')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPON'),'Arterial Base Excess')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPON'),'Venous pH')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPON'),'Venous Base Excess')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPON'),'Lowest cord blood pH')
--80
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPON'),'Arterial pC02')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPON'),'Venous pCO2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb27')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb27'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb28')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb28'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb01')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb79')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb64')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb41')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPk')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb21')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMDS6')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb81')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMba4') + ',' + dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMba4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMba4'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbA6')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME20')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME19')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb19')
--100
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb20')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPo')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPo'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME43')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb93')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME53')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME54')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr04')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zmr04'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb04')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb04'),'Days')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb78')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb78'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPOM')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'0Date sample taken3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'1Time sample taken3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'2pH3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'3Base excess3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'4Date sample taken4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'5Time sample taken4')
--120
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'6pH4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'7Base excess4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'8Date sample taken5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'9Time sample taken5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'0pH5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'1Base excess5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'2Date sample taken6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'3Time sample taken6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'4pH6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'5Base excess6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'Date sample taken1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'Time sample taken1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'pH1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'Base excess1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'Date sample taken2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'Time sample taken2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'pH2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOM'),'Base excess2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb33')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb33'),'Reason')
--140
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMA3K')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME22')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb14')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb14'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbA1') + ',' + dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbA1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbA1'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC99')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC99'),'Date of first feed')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC99'),'Time of first feed')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC99'),'Method')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC99'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb69')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb69'),'Date')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb69'),'Time')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPa')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPa'),'Additional procedure')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPa'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr07')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zmr07'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb24')
--160
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb24'),'Days')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb95')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb95'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb90')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb90'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPV')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPV'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPP')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPP'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMba0')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMba0'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME04')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb61')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb29')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb09')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb13')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMk30')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMk29')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb10')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPI')
--180
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPJ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr03')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zmr03'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME45')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME44')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCTP')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME51')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb52')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME27')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPu')
---190
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPu'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb23')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMDFM')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME26')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbAH')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPH')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb48') + ',' + dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb48') -- Name of assigned midwife
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb48'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb44') + ',' + dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb44') -- Name of person delivering
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb44'),'Grade')
--200
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPn') + ',' + dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPn') -- Name of person recording
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPn'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb46') + ',' + dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb46') -- Name of principal assistant
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb46'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPZ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPT')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb55')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME21')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME18')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbA3') + ',' + dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbA3') -- Obs Cons present
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbA3'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOQK')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOQK'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbA4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbA4'),'Name_1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbA4'),'Status_1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbA4'),'Name_2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbA4'),'Status_2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbA4'),'Name_3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbA4'),'Status_3')
--220
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbA4'),'Others')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbA0')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb12')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb12'),'Death_of_Fetus')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbAA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPL')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbkF')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMba7')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME15')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb56')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb17')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb17'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPB')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPB'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPp')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPp'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb57')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb58')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbAB') + ',' + dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbAB') -- Principal anaesthetist present
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbAB'),'Grade')
--240
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbAE') + ',' + dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbAE') -- Principal Floor Nurse
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbAE'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbAC') + ',' + dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbAC') -- Principal paed. present
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbAC'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbAD') + ',' + dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbAD') -- Principal Scrub Nurse
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbAD'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME24')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME25')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr02')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zmr02'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb32')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb26')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME17')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPS')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPS'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPW')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPW'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmry1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb94')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb94'),'Text')
--260
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb0H')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPg')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPg'),'Fetal position')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPg'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMnp0')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME55')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPK')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPK'),'Estimated fetal weight')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPK'),'Attitude')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbAS') + ',' + dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbAS') -- Scrub midwife
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbAS'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbA2') + ',' + dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbA2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbA2'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr01')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zmr01'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME23')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb68')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb68'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb75')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb75'),'Date attempted')
--280
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb75'),'Time attempted')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb75'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCTT')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr05')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zmr05'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME59')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPF')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOQE')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMba9')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME16')
--290
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb37')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb02')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb80')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb51')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPY')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb65')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb18')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPl')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb22')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPR')
--300
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMDS7')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC97')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC98')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC96')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC95')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbkG')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMpln')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMplt')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME08')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPj')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPs')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMba8')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPQ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbAb') 
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbAa')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb30')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPX')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPX'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb50')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb50'),'Text')
--320
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbA7')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPm')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbPA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbAT')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb89')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb89'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb85')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME52')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPN')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPN'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPU')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPU'),'Additional procedure')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPU'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMba6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMba6'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb31')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'0Date sample taken3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'1Time sample taken3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'2pH3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'3Base excess3')
--340
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'4Date sample taken4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'5Time sample taken4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'6pH4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'7Base excess4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'8Date sample taken5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'9Time sample taken5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'0pH5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'1Base excess5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'2Date sample taken6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'3Time sample taken6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'4pH6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'5Base excess6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'Date sample taken1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'Time sample taken1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'pH1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'Base excess1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'Date sample taken2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'Time sample taken2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'pH2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb31'),'Base excess2')
--360
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMba3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMba3'),'Complications')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbb1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbA5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC71')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPt')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPt'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMR11')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR11'),'Colour')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR11'),'Tone')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR11'),'Heart_rate')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR11'),'Respiration')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR11'),'Response_to_stimulus')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMR25')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR25'),'Colour')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR25'),'Tone')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR25'),'Heart_rate')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR25'),'Respiration')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR25'),'Response_to_stimulus')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMR13')
--380
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR13'),'Colour')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR13'),'Tone')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR13'),'Heart_rate')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR13'),'Respiration')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR13'),'Response_to_stimulus')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMR18')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR18'),'Colour')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR18'),'Tone')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR18'),'Heart_rate')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR18'),'Respiration')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR18'),'Response_to_stimulus')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMR12')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMR26')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMR14')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMR19')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMR07')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR07'),'First breath')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMR06')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR06'),'Cardiac massage given')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR06'),'First beat at')
--400
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMR02')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR02'),'Naloxone_(Narcan)')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR02'),'Dextrose')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR02'),'Bicarbonate')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR02'),'Atropine')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR02'),'Adrenaline')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR02'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMR01')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmrx2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmrx3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmrx4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmrx5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmrx6')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmrx7')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmrx8')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMR04') + ',' + dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR04')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR04'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMNRC')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMR08')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMR05')
--420
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMR05'),'At minute')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMTCR')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMR03')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN16')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN16'),'Location')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN16'),'Route & Dose')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN16'),'Date given')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN16'),'Time given')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN66')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN66'),'Location')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN66'),'Route & Dose')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN66'),'Date given')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN66'),'Time given')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN68')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN68'),'Location')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN68'),'Route & Dose')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN68'),'Date given')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN68'),'Time given')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN18')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN18'),'Reason')
--440
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN18'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMBDS')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMBDS'),'filename')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMnu5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN19')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN19'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN07')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN0b')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMnu2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNq')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNz')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMnu3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMnu1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMnu4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMnu4'),'GUID')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMnu4'),'Username')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMnu4'),'Date updated')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMnu4'),'Time updated')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN0c')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNx')
--460
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPN1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbd1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbd1'),'Filename')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMnu6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMnu6'),'Username')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMnu6'),'Date updated')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMnu6'),'Time updated')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMnu6'),'Comments')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN01')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN13')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN13'),'Downs')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN13'),'Neural Tube Defect')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN13'),'Absent Digits')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN13'),'Cleft Lip')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN13'),'Cleft Palate')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN13'),'Diagnosed Antenatally')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN13'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN67')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN14')
--480
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN09')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN09'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN0d')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN12')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN12'),'Baby')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN12'),'Gestation estimate')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN22')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN22'),'Sample 1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN22'),'Sample 2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN22'),'Sample 3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN22'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN06')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNi')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr15')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMnu7')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMnu7'),'Username')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMnu7'),'Date updated')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMnu7'),'Time updated')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMnu7'),'Comments')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN05')
--500
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN21') + ',' + dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN21')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN21'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMG1G')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMG1G'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN08')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN15')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN15'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN17')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN35')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN35'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN02')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN0a')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb03')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN10')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN20')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBA')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBA'),'Abdomen')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBA'),'Spleen')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBA'),'Liver')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBA'),'Kidneys')
--520
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBA'),'Anus')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBA'),'Umbilicus')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBA'),'Genitalia')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBA'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNP')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNP'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLP7')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLP7'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPB6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB6'),'Bruising')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB6'),'Rash')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB6'),'Naevus')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB6'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNk')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNk'),'Status')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPN0')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBE')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBE'),'Irritability')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBE'),'Infection')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBE'),'Jaundice')
--540
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBE'),'Nappy Rash')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBE'),'Other Rash')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBE'),'Constipation')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBE'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCI0')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMCI0'),'Status')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPN7')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPOa')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBS')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBS'),'Filename')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP19')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP20')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP20'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNE')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNE'),'Comments')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNE'),'Injection site')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNE'),'Location')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNE'),'Batch number')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNE'),'Date given')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNE'),'Time given')
--560
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP18')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMTH6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMTH6'),'Stool')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPB9')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB9'),'Heart sounds')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB9'),'Heart murmurs')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB9'),'Femoral pulses')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB9'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPP1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPP1'),'Reason')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPP1'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMTH9')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMTH9'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBC')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBC'),'Muscle tone')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBC'),'Posture')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBC'),'Movement')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBC'),'Cry')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBC'),'Grasp')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBC'),'Moro')
--580
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBC'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNH')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNH'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPB3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB3'),'Scan')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB3'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCG1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMCG1'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCG2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMCG2'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCG3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMCG3'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNa')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP21')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPOQ')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOQ'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zx003')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPAD')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zx006')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNj')
--600
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMTH7')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMTH7'),'Swab')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPB8')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB8'),'Face')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB8'),'Red reflex')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB8'),'Eyes')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB8'),'Ears')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB8'),'Nose')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB8'),'Palate')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB8'),'Mouth')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB8'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNJ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMTH3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMTH4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMTH4'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNg')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNg'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBJ')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBJ'),'Colour')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBJ'),'Skin')
--620
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBJ'),'Body shape')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBJ'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPB7')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB7'),'Sutures')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB7'),'Fontanelles')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB7'),'Head')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB7'),'Neck')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB7'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPB2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB2'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zx002')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zx001')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zx001'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMTH1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPOA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPOB')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPB4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPB5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB5'),'Injection site')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB5'),'Location')
--640
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB5'),'Batch number')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB5'),'Date given')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB5'),'Time given')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNC')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNC'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMTH2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNl')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNl'),'Lowest glucose')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNm')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNm'),'Because')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNm'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNI')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMG1H')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMG1H'),'Reason')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMG1H'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPst')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBB')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBB'),'Arms')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBB'),'Hands')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBB'),'Spine')
--660
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBB'),'Legs')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBB'),'Feet')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBB'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPB1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPB1'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPND')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPND'),'When')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPND'),'Result')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNf')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCH1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMCH1'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCH2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMCH2'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCH3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMCH3'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCH4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMCH4'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCH5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMCH5'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCH6')
--680
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMCH6'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBD')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBD'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPOC')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmry5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zmry5'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zx004')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPL6')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLP4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLP4'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNG')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNG'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNB')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNB'),'Maximum SBR')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNB'),'Phototherapy')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP22')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPLB')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zx007')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBH')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBH'),'Text')
--700
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBG')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBG'),'Audiologist')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBG'),'Orthopaedic surgeon')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBG'),'Other')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBG'),'Second')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPC0')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPC0'),'Chest auscultation')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPC0'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zx005')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmry4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPL5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPL5'),'Max SBR')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPAT')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNF')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMTH8')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMTH8'),'Swab')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMTH5'))
--717
			SET @SQLError = @@error
			IF @SQLError > 0
			BEGIN
				INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
				VALUES ( getdate(), 'pr_Explode_Baby', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating baby record for pregnancy ' + @p_pregnancy_number + ' for patient ' + cast(@pointer as varchar), -1 )
			END
			ELSE
			BEGIN
				EXEC dbo.pr_Explode_DeathOfBaby @pointer, @p_pregnancy_id, @p_pregnancy_number,@p_delivery_number, @l_baby_number , @NOTENO
				EXEC dbo.pr_Explode_BloodSpotScreening @pointer, @p_pregnancy_id, @p_pregnancy_number,@p_delivery_number, @l_baby_number , @NOTENO
			END

		FETCH NEXT FROM get_baby INTO @NOTENO
	 END
	
	CLOSE get_baby
 	DEALLOCATE get_baby

END
