﻿/*
****************************************************************************************************************
**	NAME:		pr_Explode_BloodSpotScreening					**
**	DESCRIPTION:	Runs the Baby Bloodspotscreening 'explode' routine		**
**	AUTHOR:	Stephen Creek							**
**	VERSIONS:	1.0	Stephen Creek	07/02/2008	Initial version created	**
**				1.1	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**												Noteno has changed from a 5 byte binary coded structured key
**												to a 25 byte (5 x 5 bytes) ASCII coded key.
****************************************************************************************************************
*/
CREATE PROCEDURE [dbo].[pr_Explode_BloodSpotScreening]
	(@pointer int
	,@p_pregnancy_id int
	,@p_pregnancy_number varchar(30)
	,@p_delivery_number varchar(30)
	,@p_baby_number varchar(30)
	,@p_noteno varchar(25))
AS
BEGIN
	DECLARE @NOTENO1 VARCHAR(25)
	DECLARE @NOTENO2 VARCHAR(25)
	DECLARE @NOTENO VARCHAR(25)
	DECLARE @SQLError INT

-- ZMPBh	Newborn Blood Spot screening
-- ZMPRh	Repeat Newborn Blood Spot screening

	SET @NOTENO1 = (SELECT Noteno
	FROM tbl_Notes_Copy
	WHERE Code = 'ZMPBh' -- Newborn Blood Spot screening
	AND Pointer = @pointer
	AND substring(Noteno,1,len('x'+@p_noteno+'x')-2) = @p_noteno)

	SET @NOTENO2 = (SELECT Noteno
	FROM tbl_Notes_Copy
	WHERE Code = 'ZMPRh' -- Repeat Newborn Blood Spot screening
	AND Pointer = @pointer
	AND substring(Noteno,1,len('x'+@p_noteno+'x')-2) = @p_noteno)

	IF @NOTENO1 IS NOT NULL or @NOTENO2 IS NOT NULL
	BEGIN

		--v1.1 SET @NOTENO = SUBSTRING(isnull(@NOTENO1,@NOTENO2),1,3)
		SET @NOTENO = SUBSTRING(isnull(@NOTENO1,@NOTENO2),1,15)

		INSERT INTO [tbl_BloodSpotScreening] 
	           		(Patient_Pointer,
			Pregnancy_ID,
 			[Pregnancy number],
			Delivery_Number,
			Baby_Number,
			[Blood spot screening accepted],
			[Blood spot screening accepted Reason],
			[Blood spot screening offered],
			[Blood spot screening offered Reason],
			[Consent obtained for Cystic Fibrosis screening],
			[Consent obtained for Cystic Fibrosis screening Reason],
			[Consent obtained for future research contact],
			[Consent obtained for future research contact Reason],
			[Consent obtained for MCADD screening],
			[Consent obtained for MCADD screening Reason],
			[Consent obtained for PKU screening],
			[Consent obtained for PKU screening Reason],
			[Consent obtained for Sickle cell screening],
			[Consent obtained for Sickle cell screening Reason],
			[Consent obtained for TSH screening],
			[Consent obtained for TSH screening Reason],
			[Date blood spot specimen dispatched],
			[Date blood spot specimen taken],
			[Grade of person taking test],
			[Name of person taking test],
			[Outcome of Bloodspot screening],
			[Outcome of Bloodspot screening Details],
			[Outcome of Cystic Fibrosis screening],
			[Outcome of Cystic Fibrosis screening Details],
			[Outcome of MCADD screening],
			[Outcome of MCADD screening Details],
			[Outcome of PKU screening],
			[Outcome of PKU screening Details],
			[Outcome of Sickle cell screening],
			[Outcome of Sickle cell screening Details],
			[Outcome of TSH screening],
			[Outcome of TSH screening Details],
			[Repeat newborn blood spot screening],
			[Type of person taking test],
			[Consent obtained for repeat future research contact],
			[Consent obtained for repeat future research contact Reason],
			[Consent obtained for repeat Cystic Fibrosis screening],
			[Consent obtained for repeat Cystic Fibrosis screening Reason],
			[Consent obtained for repeat MCADD screening],
			[Consent obtained for repeat MCADD screening Reason],
			[Consent obtained for repeat PKU screening],
			[Consent obtained for repeat PKU screening Reason],
			[Consent obtained for repeat Sickle cell screening],
			[Consent obtained for repeat Sickle cell screening Reason],
			[Consent obtained for repeat TSH screening],
			[Consent obtained for repeat TSH screening Reason],
			[Date repeat blood spot specimen dispatched],
			[Date repeat blood spot specimen taken],
			[Grade of person taking repeat test],
			[Name of person taking repeat test],
			[Outcome of repeat Bloodspot screening],
			[Outcome of repeat Bloodspot screening Details],
			[Outcome of repeat Cystic Fibrosis screening],
			[Outcome of repeat Cystic Fibrosis screening Details],
			[Outcome of repeat MCADD screening],
			[Outcome of repeat MCADD screening Details],
			[Outcome of repeat PKU screening],
			[Outcome of repeat PKU screening Details],
			[Outcome of repeat Sickle cell screening],
			[Outcome of repeat Sickle cell screening Details],
			[Outcome of repeat TSH screening],
			[Outcome of repeat TSH screening Details],
			[Repeat blood spot screening accepted],
			[Repeat blood spot screening accepted Reason],
			[Repeat blood spot screening offered],
			[Repeat blood spot screening offered Reason],
			[Type of person taking repeat test])
		VALUES	
	            		(@pointer
            			,@p_pregnancy_id
            			,@p_pregnancy_number
			,@p_delivery_number
			,@p_baby_number
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBb')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBb'),'Reason')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBa')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBa'),'Reason')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBi')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBi'),'Reason')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBk')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBk'),'Reason')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBj')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBj'),'Reason')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBe')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBe'),'Reason')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBg')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBg'),'Reason')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBf')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBf'),'Reason')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBd')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBc')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBs')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBr')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP01')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP01'),'Details')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP06')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP06'),'Details')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP07')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP07'),'Details')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP02')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP02'),'Details')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP05')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP05'),'Details')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP04')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP04'),'Details')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBl')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBq')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBZ')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBZ'),'Reason')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBz')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBz'),'Reason')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBY')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBY'),'Reason')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBw')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBw'),'Reason')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBy')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBy'),'Reason')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBx')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBx'),'Reason')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBp')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBo')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBv')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBu')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP08')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP08'),'Details')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP14')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP14'),'Details')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP15')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP15'),'Details')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP09')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP09'),'Details')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP12')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP12'),'Details')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP10')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP10'),'Details')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBn')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBn'),'Reason')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBm')
,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBm'),'Reason')
,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBt'))
		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_BloodSpotScreening', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating blood spot screening record for baby ' + cast(@p_baby_number as varchar) + ' for pregancy ' + @p_pregnancy_number + ' for patient ' + cast(@pointer as varchar), -1 )
		END
	END
END
