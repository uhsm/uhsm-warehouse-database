﻿
/*
**************************************************************************************************************************
**	NAME:		pr_Explode_Cervicogram							**
**	DESCRIPTION:	Runs the Cervicogram 'explode' routine					**
**	AUTHOR:	Stephen Creek								**
**	VERSIONS:	1.0	Stephen Creek	27/02/2008	Initial version created		**
**************************************************************************************************************************
*/
CREATE PROCEDURE [dbo].[pr_Explode_Cervicogram]
	(@pointer int
	,@p_pregnancy_id int
	,@p_pregnancy_number varchar(30)
	,@p_noteno varchar(5))
AS
BEGIN
	DECLARE @NOTENO  VARCHAR(5)
	DECLARE @l_exam_number INT
	DECLARE @SQLError INT

	DECLARE get_exam CURSOR FOR
	SELECT Noteno
	FROM tbl_Notes_Copy
	WHERE Code = 'ZSE3T' -- Vaginal Examination On
	AND Pointer = @pointer
	AND substring(Noteno,1,len('x'+@p_noteno+'x')-2) = @p_noteno
	ORDER BY Noteno

	SET @l_exam_number = 0
 
	OPEN get_exam
	FETCH next from get_exam into @NOTENO

	WHILE @@fetch_status = 0
	BEGIN
		SET @l_exam_number = @l_exam_number + 1

		SET @NOTENO = SUBSTRING(@NOTENO,1,3)

		INSERT INTO [tbl_Cervicogram] 
			(Patient_Pointer,
			Pregnancy_ID, 
			Examination_Number,
			[Pregnancy number],
			[Hours in labour],
			[Date of Vaginal Examination],
			[Time of Vaginal Examination],
			[Cervical dilation],
			[Date of Review],
			[Time of Review],
			[Type of Review],
			[Birth Order])
		VALUES
			(@pointer
			,@p_pregnancy_id  
			,@l_exam_number
			,@p_pregnancy_number
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMBS8')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMBT1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMBT2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMBT3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMBT4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMBT5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMBT6')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMBT7'))

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_Cervicogram', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating cervicogram record ' + cast(@l_exam_number as varchar) + ' for pregancy ' + @p_pregnancy_number + ' for patient ' + cast(@pointer as varchar), -1 )
		END

	            FETCH NEXT FROM get_exam INTO @NOTENO
	END

	CLOSE get_exam
	DEALLOCATE get_exam
END
