﻿/*
**************************************************************************************************************************
**	NAME:		pr_Explode_ClinicalReviews						**
**	DESCRIPTION:	Runs the Clinical Reviews 'explode' routine				**
**	AUTHOR:	Stephen Creek								**
**	VERSIONS:	1.0	Stephen Creek	06/11/2007	Initial version created		**
**			1.1	Stephen Creek	07/02/2008	Corrected use of len() function	**
**			1.2	Stephen Creek	11/02/2007	Added Pregnancy_ID		**
**			1.3	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**												Noteno has changed from a 5 byte binary coded structured key
**												to a 25 byte (5 x 5 bytes) ASCII coded key.
*************************************************************************************************************************
*/
CREATE  PROCEDURE [dbo].[pr_Explode_ClinicalReviews]
	(@pointer int
	,@p_pregnancy_id int -- v1.2
	,@p_pregnancy_number varchar(30)
	,@p_noteno VARCHAR(25))
AS
BEGIN
	DECLARE @NOTENO  VARCHAR(25)
	DECLARE @l_review_number INT
	DECLARE @SQLError INT

 	DECLARE get_ClinicalReviews CURSOR FOR
 	SELECT Noteno
 	FROM tbl_Notes_Copy
	WHERE Code = 'ZSE2T'
		AND Pointer = @pointer
		AND substring(Noteno,1,len('x'+@p_noteno+'x')-2) = @p_noteno --v1.1
	ORDER BY Noteno

	SET @l_review_number = 0

	OPEN get_ClinicalReviews
 	FETCH NEXT FROM get_ClinicalReviews INTO
	@NOTENO

	WHILE @@fetch_status = 0
	BEGIN
		SET @l_review_number = @l_review_number + 1

--print 'Review Number: '+cast(@l_review_number as varchar(60))

		--v1.3 SET @NOTENO = SUBSTRING(@NOTENO,1,3)
		SET @NOTENO = SUBSTRING(@NOTENO,1,15)

		INSERT INTO [tbl_ClinicalReviews] 
			(Patient_Pointer,
			Pregnancy_ID, -- v1.2
			[Pregnancy number],
			Review_Number,
			[1st additional anaes.],
			[1st additional anaes. Grade],
			[2nd additional anaes.],
			[2nd additional anaes. Grade],
			[Add comments to user messages],
			[Admission Letter Filename],
			[Admission Letter],
			[Anaesthetic discharge comments],
			[Anaesthetic discharge comments Text],
			[Antenatal or postnatal review],
			[Any other anaesthetist present],
			[Any procedures planned],
			[Any procedures planned Procedure 1],
			[Any procedures planned Procedure 2],
			[Any procedures planned Recalldate],
			[Booked clinician code],
			[Booked for this pregnancy],
			[Booked time of review],
			[Care level],
			[Category of review],
			[Clinic Letter],
			[Clinic Letter Filename],
			[Clinical review completed by],
			[Clinicians grade],
			[Comments on scan],
			[Comments on scan Text],
			[Conclusion from review],
			[Conclusion from review Text],
			[Create a message to other users],
			[Create a message to other users Text],
			[Date anaesthetist attended],
			[Date anaesthetist called],
			[Date combined proc. started],
			[Date G.A started],
			[Date of review],
			[Date procedure completed],
			[Date procedure started],
			[Date review completed],
			[Date spinal started],
			[Day],
			[Diary event],
			[Diary event Description of event],
			[Discharge Letter],
			[Discharge Letter Filename],
			[Domicillary fetal monitoring],
			[Duration of review],
			[EDD from scan],
			[Final diagnosis],
			[Gestation at procedure],
			[Gestation at review],
			[Gestation at review Days],
			[Interim Letter],
			[Interim Letter Filename],
			[Kick chart arranged],
			[Location of procedure Text],
			[Location of procedure],
			[Location of review],
			[Name of anaesthetist],
			[Name of anaesthetist Grade],
			[Name of clinician],
			[Name of clinician Grade],
			[Name of person requesting scan],
			[Name of supervising anaes.],
			[Name of supervising anaes. Grade],
			[NNNI Category],
			[No. of days postnatal],
			[Outcome of review],
			[Pathology interface results],
			[Patient Letter],
			[Patient Letter Filename],
			[Primary reason for review],
			[Primary reason for review Text],
			[Principal activity],
			[Principle procedure],
			[Principle procedure Procedure 2],
			[Principle procedure Procedure 3],
			[Principle procedure Procedure 4],
			[Principle procedure Text],
			[Procedure comments],
			[Procedure comments Text],
			[Procedure duration (hrs:mins)],
			[Procedure planned],
			[Reason for more than one anaes],
			[Reason for more than one anaes Text],
			[Reason for procedure],
			[Reason for procedure Text],
			[Reason for scan],
			[Review comments],
			[Review comments Text],
			[Review Deletion allowed],
			[Review episode number],
			[Review event number],
			[Review follow up],
			[Review follow up Recalldate],
			[Review follow up Text],
			[Review source],
			[Review source Text],
			[Review status],
			[Scan arranged],
			[Scan arranged When],
			[Scan arranged Result],
			[Scan ID],
			[Scan machine used],
			[Scan Operator],
			[Scanning method used],
			[Secondary activity 1],
			[Secondary activity 2],
			[Secondary reason for review Text],
			[Secondary reason for review],
			[Time anaesthetist attended],
			[Time anaesthetist called],
			[Time combined proc. started],
			[Time G.A started],
			[Time of arrival],
			[Time of review],
			[Time patient left],
			[Time procedure completed],
			[Time procedure started],
			[Time review completed],
			[Time spinal started],
			[Timing of procedure],
			[Transfer Letter],
			[Transfer Letter Filename],
			[Type of anaesthetist],
			[Type of clinician],
			[Type of procedure],
			[Type of review],
			[Visit number],
			[Was anaesthetist supervised])
		VALUES
			(@pointer
			,@p_pregnancy_id --v1.2
			,@p_pregnancy_number
			,@l_review_number
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF15')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEF15'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF16')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEF16'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF71')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZTRL2'),'Filename')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZTRL2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMY65')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMY65'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAUx')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF14')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAUa')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAUa'),'Procedure 1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAUa'),'Procedure 2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAUa'),'Recalldate')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSMS1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPOc')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMant')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSCL2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCCp')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZTRL1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZTRL1'),'Filename')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDGIP')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDGIB')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSO7')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMSO7'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSO8')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMSO8'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAUd')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAUd'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF09')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF07')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF90')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF94')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZTGIA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF72')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF00')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZTGDL')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF92')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDAY')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMRFc')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMRFc'),'Description of event')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZTRL3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZTRL3'),'Filename')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAU4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCC5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZTSDD')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAUR')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF67')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAUb')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAUb'),'Days')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZTRL5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZTRL5'),'Filename')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAU3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEF05'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF05')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZTGID')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF11')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEF11'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMGIP')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMGIP'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMUS1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF13')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEF13'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSCL8')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAUc')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZTNV5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZASTM')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZTRL6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZTRL6'),'Filename')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMWA5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMWA5'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCCa')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF03')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEF03'),'Procedure 2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEF03'),'Procedure 3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEF03'),'Procedure 4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEF03'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF81')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEF81'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF74')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF04')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF17')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEF17'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF06')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEF06'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMUS4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZTGIn')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZTGIn'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMed1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMep1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMev1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAUB')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAUB'),'Recalldate')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAUB'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMopb')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMopb'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMast')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAU8')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAU8'),'When')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAU8'),'Result')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMUS0')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMUS2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMUS5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMUS3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCCb')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCCc')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAR2'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAR2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF10')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF08')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF91')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF95')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZTGIE')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZTGIB')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMWA3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF73')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF01')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZTGIF')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF93')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF02')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZTRL4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZTRL4'),'Filename')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF68')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMGIB')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF66')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZTGIC')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPOb')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEF12'))

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_ClinicalReview', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating clinical review record ' + cast(@l_review_number as varchar) + ' for pregancy ' + @p_pregnancy_number + ' for patient ' + cast(@pointer as varchar), -1 )
		END
		ELSE
		BEGIN
			EXEC dbo.pr_Explode_FetalObservations @pointer, @p_pregnancy_id, @p_pregnancy_number, @l_review_number, @NOTENO
			EXEC dbo.pr_Explode_ReviewInvestigations @pointer, @p_pregnancy_id, @p_pregnancy_number, @l_review_number, @NOTENO
			EXEC dbo.pr_Explode_ReviewExaminations @pointer, @p_pregnancy_id, @p_pregnancy_number, @l_review_number, @NOTENO
			EXEC dbo.pr_Explode_ReviewTreatments @pointer, @p_pregnancy_id, @p_pregnancy_number, @l_review_number, @NOTENO
		END

		FETCH NEXT FROM get_ClinicalReviews INTO
		@NOTENO
	END
 
	CLOSE get_ClinicalReviews
	DEALLOCATE get_ClinicalReviews
END
