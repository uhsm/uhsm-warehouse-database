﻿/*
**************************************************************************************************************************
**	NAME:		pr_Explode_DeathOfBaby						**
**	DESCRIPTION:	Runs the New DeathOfBaby 'explode' routine				**
**	AUTHOR:	Stephen Creek								**
**	VERSIONS:	1.0	Stephen Creek	08/11/2007	Initial version created		**
**			1.1	Stephen Creek	07/02/2008	Corrected use of len() function	**
**			1.2	Stephen Creek	11/02/2008	Added Pregnancy_ID		**
**			1.3	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**											Noteno has changed from a 5 byte binary coded structured key
**											to a 25 byte (5 x 5 bytes) ASCII coded key.
**************************************************************************************************************************
*/
CREATE PROCEDURE [dbo].[pr_Explode_DeathOfBaby]
	(@pointer int
	,@p_pregnancy_id int --v1.2
	,@p_pregnancy_number varchar(30)
	,@p_delivery_number int
	,@p_baby_number int
	,@p_noteno VARCHAR(25))
AS
BEGIN
	DECLARE @NOTENO  VARCHAR(25)
	DECLARE @SQLError INT

	SET @NOTENO = (SELECT Noteno
	FROM tbl_Notes_Copy
	WHERE Code = 'ZSDEz' -- Death of Baby
	AND Pointer = @pointer
	AND substring(Noteno,1,len('x'+@p_noteno+'x')-2) = @p_noteno) -- v1.1

	IF @NOTENO IS NOT NULL
	BEGIN

		--v1.3 SET @NOTENO = SUBSTRING(@NOTENO,1,3)
		SET @NOTENO = SUBSTRING(@NOTENO,1,15)

		INSERT INTO [tbl_DeathOfBaby] 
	           		(Patient_Pointer,
			Pregnancy_ID, -- v1.2
 			[Pregnancy number],
			Delivery_Number,
			Baby_Number,
			[Case definition],
			[Category of child death],
			[Clinical cause of death],
			[Clinical cause of death Text],
			[Condition at birth],
			[Condition at birth Audible cry],
			[Condition at birth Spontaneous breathing],
			[Condition at birth Heart beat detected],
			[Date death diagnosed],
			[Date last known alive],
			[Date last known alive Time],
			[Death in Region of Residence],
			[Death in Region of Residence Region],
			[Fetal/Neonatal Classification],
			[Gestation at first scan],
			[Gestation at first scan Days],
			[Gestation death Confirmed],
			[Gestation death Confirmed Days],
			[Name of person delivering],
			[Name of person delivering Grade],
			[Obstetric Classification],
			[Planned follow up of parents],
			[Planned follow up of parents Text],
			[Post mortem],
			[Primary cause of death],
			[Same address at time of death],
			[Same address at time of death House_Number],
			[Same address at time of death Address_1],
			[Same address at time of death Address_2],
			[Same address at time of death Address_3],
			[Same address at time of death Postcode],
			[Secondary cause of death 1],
			[Secondary cause of death 2],
			[Secondary cause of death 3],
			[Time death diagnosed],
			[Timing of death],
			[Type of person delivering],
			[Was there evidence of fetal growth restriction],
			[Was there evidence of fetal growth restriction Evidence],
			[Was this a legal abortion],
			[Where did baby die],
			[Where did baby die Name of unit or place],
			[Wigglesworth Classification])
		VALUES	
	            		(@pointer
            			,@p_pregnancy_id -- v1.2
            			,@p_pregnancy_number
			,@p_delivery_number
			,@p_baby_number
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDF5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMG1I')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGDT2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGDT2'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDE8')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDE8'),'Audible cry')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDE8'),'Spontaneous breathing')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDE8'),'Heart beat detected')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGDT1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDCA')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDCA'),'Time')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDEF')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDEF'),'Region')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGDT3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDF4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDF4'),'Days')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDF1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDF1'),'Days')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr11')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zmr11'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGDT4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDE4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDE4'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDE2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDEA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDEG')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDEG'),'House_Number')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDEG'),'Address_1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDEG'),'Address_2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDEG'),'Address_3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDEG'),'Postcode')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDEB')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDEC')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDED')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDE5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDE7')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr10')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDF2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDF2'),'Evidence')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDF3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDE9')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDE9'),'Name of unit or place')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGDT5'))

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_DeathOfBaby', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating death record for baby ' + cast(@p_baby_number as varchar) + ' for pregancy ' + @p_pregnancy_number + ' for patient ' + cast(@pointer as varchar), -1 )
		END
	END
END
