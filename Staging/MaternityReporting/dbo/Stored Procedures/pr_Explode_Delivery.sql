﻿/*
**************************************************************************************************************************
**	NAME:		pr_Explode_Delivery							**
**	DESCRIPTION:	Runs the New Delivery 'explode' routine					**
**	AUTHOR:	Stephen Creek								**
**	VERSIONS:	1.0	Stephen Creek	06/11/2007	Initial version created		**
**			1.1	Stephen Creek	07/02/2007	Corrected use of len() function	**
**			1.2	Stephen Creek	11/02/2007	Added Pregnancy_ID		**
**			1.3	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**												Noteno has changed from a 5 byte binary coded structured key
**												to a 25 byte (5 x 5 bytes) ASCII coded key.
**************************************************************************************************************************
*/
CREATE PROCEDURE [dbo].[pr_Explode_Delivery]
	(@pointer int
	,@p_pregnancy_id int --v1.2
	,@p_pregnancy_number varchar(30)
	,@p_noteno varchar(25))
AS
BEGIN
	DECLARE @NOTENO  VARCHAR(25)
	DECLARE @l_delivery_number INT
	DECLARE @SQLError INT

	DECLARE get_delivery CURSOR FOR
	SELECT Noteno
	FROM tbl_Notes_Copy
	WHERE Code = 'ZMC00'
	AND Pointer = @pointer
	AND substring(Noteno,1,len('x'+@p_noteno+'x')-2) = @p_noteno --v1.1
	ORDER BY Noteno

	SET @l_delivery_number = 0
 
	OPEN get_delivery
	FETCH next from get_delivery into @NOTENO

	WHILE @@fetch_status = 0
	BEGIN
		SET @l_delivery_number = @l_delivery_number + 1
		--v1.3 SET @NOTENO = SUBSTRING(@NOTENO,1,2)
		SET @NOTENO = SUBSTRING(@NOTENO,1,10)
		INSERT INTO [tbl_Delivery] 
			(Patient_Pointer,
			Pregnancy_ID,  --v1.2
			Delivery_Number,
			[Pregnancy number],
			[Antiphospholipid antibody],
			[Current Infection],
			[Ext mjr pelvic/abdo surgery],
			[Gross Varicous Veins],
			[Immobility > 4days],
			[Major current illness],
			[Paralysis of lower limbs],
			[Prophylaxis Risk Action],
			[Prophylaxis Risk Score],
			[Thrombophilia],
			[Acupuncture],
			[Analgesia During Labour],
			[Antenatal steroids given],
			[Antenatal steroids given Date started],
			[Antenatal steroids given Number of doses],
			[Antibiotics in pregnancy Text],
			[Antibiotics in pregnancy],
			[Antibiotics in pregnancy Date started],
			[Antibiotics in pregnancy Reason],
			[Any drugs given in labour],
			[Any drugs given in labour Drug 1],
			[Any drugs given in labour Drug 2],
			[Any drugs given in labour Drug 3],
			[Any drugs given in labour Drug 4],
			[Any drugs given in labour Text],
			[ARM done for induction],
			[ARM_done_for_induction],
			[Date labour established],
			[Date of induction ARM],
			[Drugs given prior to section Drug 2],
			[Drugs given prior to section Drug 3],
			[Drugs given prior to section Drug 4],
			[Drugs given prior to section Text],
			[Drugs given prior to section],
			[Drugs given prior to section Drug 1],
			[Epidural],
			[Epidural Method],
			[Epidural inserted by],
			[Epidural inserted by Grade],
			[Epidural resited at any time],
			[Epidural_for_labour],
			[Failed induction],
			[Failed induction Text],
			[Group B strep prophylaxis Reason],
			[Group B strep prophylaxis Text],
			[Group B strep prophylaxis],
			[Group B strep prophylaxis Date started],
			[Group B strep prophylaxis Time started],
			[Indication for augmentation Text],
			[Indication for augmentation],
			[Indication for induction],
			[Indication for induction Text],
			[Induction ARM done by],
			[Induction ARM done by Grade],
			[Induction done by staff Type],
			[Induction drug 1],
			[Induction drug 1 Date given],
			[Induction drug 1 Time given],
			[Induction drug 2 Date given],
			[Induction drug 2 Time given],
			[Induction drug 2],
			[Induction drug 3 Date given],
			[Induction drug 3 Time given],
			[Induction drug 3],
			[Medic. indication for epidural],
			[Medic. indication for epidural Text],
			[Method of Induction],
			[Mothers attitude to epidural],
			[Narcotics Text],
			[Narcotics],
			[Narcotics Drug 1],
			[Narcotics Drug 2],
			[Narcotics Drug 3],
			[Number of induction drug doses],
			[Other regional block in labour],
			[Prostin_induction],
			[Reason for epidural request],
			[Reason why epidural not given],
			[Self-administered inhalational],
			[Spontaneous_rupture],
			[Staff Type inserting epidural],
			[Success of Epidural],
			[Surgical_induction],
			[Syntocinon used for induction Date Started],
			[Syntocinon used for induction Time Started],
			[Syntocinon used for induction],
			[Syntocinon_infusion],
			[TENS],
			[Time labour established],
			[Time of induction ARM],
			[Type of augmentation],
			[Type of augmentation ARM_Date],
			[Type of augmentation ARM_Time],
			[Type of augmentation Date Syntocinon Started],
			[Type of augmentation Time Syntocinon Started],
			[Type of augmentation Other Method],
			[Type of onset of labour],
			[Was an epidural requested],
			[Was there labour before birth],
			[Water immersion],
			[Water immersion Complications],
			[Water immersion Comments],
			[2nd Suture material used],
			[2nd Suture material used Text],
			[3rd Suture material used],
			[3rd Suture material used Text],
			[Additional operative details],
			[Additional operative details Details],
			[Age_at_confinement],
			[Analgesia/Anaes During Delivery],
			[Any maternal problems],
			[Any maternal problems Text],
			[Any P/N Analgesia/Anaesthesia],
			[Any suturing analg/anaes],
			[Any suturing analg/anaes Self_admin._inhalation],
			[Any suturing analg/anaes Pudendal_Block],
			[Any suturing analg/anaes Local_Infiltration],
			[Any suturing analg/anaes Epidural],
			[Any suturing analg/anaes Spinal],
			[Any suturing analg/anaes Caudal],
			[Any suturing analg/anaes General_Anaesthetic],
			[Any suturing analg/anaes Combined spinal/epidural],
			[At delivery - ready to quit smoking],
			[At delivery - referred for help quitting smoking?],
			[At delivery - referred for help quitting smoking? Type of help to which refd.],
			[Baby died immed. after birth],
			[Birth notif insert or update],
			[Birth Rate Plus Category],
			[Birth_weight_indicator],
			[Blood Transfusion in labour],
			[Blood Transfusion in labour Number of units],
			[Blood_transfusion_given],
			[Blood_transfusion_in_labour],
			[Breech_most_sig_birth_method],
			[C/S hysterectomy performed],
			[C/S hysterectomy performed Text],
			[Caesar_most_sig_birth_method],
			[Case of special interest Text],
			[Case of special interest],
			[Catheterised during delivery Details],
			[Catheterised during delivery],
			[Catheterised during delivery Technique],
			[Catheterised during delivery Catheter in situ],
			[Check P.R. P.V.],
			[Closure following section],
			[Closure following section Details],
			[Completeness of membranes],
			[Completeness of membranes Text],
			[Completeness of placenta],
			[Completeness of placenta Text],
			[Consultant on call],
			[Continued Birth Anaesthesia],
			[Continued Birth Anaesthesia Local Anaesthetic],
			[Continued Birth Anaesthesia Spinal],
			[Continued Birth Anaesthesia Epidural],
			[Continued Birth Anaesthesia General Anaesthesia],
			[Continued Birth Anaesthesia Other],
			[Correct needle count],
			[Correct swab count],
			[Critical incident],
			[Critical incident Text],
			[Current smoker at delivery],
			[Current smoker at delivery Currently],
			[Damage required suturing],
			[Date of delivery],
			[Date of delivery admission],
			[Date of incident],
			[Date of placental delivery],
			[Degree of perineal tearing],
			[Delivery admission],
			[Delivery episode number],
			[Drains inserted],
			[Drains inserted Text],
			[Early post-partum haemorrhage],
			[Early post-partum haemorrhage Probable Reason],
			[Early post-partum haemorrhage Text],
			[Early_post_partum_haemorrhage],
			[Elderly_primigravida],
			[Epidural catheter removed],
			[Episiotomy],
			[Episiotomy performed],
			[ERPC_performed],
			[Feeding Intention at delivery],
			[Forceps_most_sig_birth_method],
			[Grade of person suturing],
			[Have samples been sent],
			[Hospital follow-up of perineum],
			[Intrapartum problems Text],
			[Intrapartum problems],
			[Intrapartum problems Problem 1],
			[Intrapartum problems Problem 2],
			[Intrapartum problems Problem 3],
			[IV Infusion in labour],
			[IV Infusion in labour Induction],
			[IV Infusion in labour Augmentation],
			[IV Infusion in labour Other],
			[Korner insert or update],
			[Length of 3rd stage(hrs:mins)],
			[Location of incident],
			[Man_removal_of_placenta],
			[Management of Third Stage],
			[Management of Third Stage Text],
			[Medical problems in labour],
			[Medical problems in labour Problem],
			[Method of feeding at delivery],
			[Most senior doctor present],
			[Most senior midwife present],
			[Most significant Birth Method],
			[Most_significant_birth_method],
			[Mother sterilised at Caesarean],
			[Mothers age at delivery],
			[Mothers attitude to Caesarean],
			[Multiple delivery anticipated],
			[Name of person supervising],
			[Name of person supervising Grade],
			[Name of prsn removing catheter],
			[Name of prsn removing catheter Grade],
			[Name of suturing person],
			[Name reporting an incident Grade],
			[Name reporting an incident],
			[Normal intra-abdomen findings],
			[Normal intra-abdomen findings Findings],
			[Normal_most_sig_birth_method],
			[Number born this delivery],
			[Number of sutures],
			[Other intensive care input Care Required],
			[Other intensive care input],
			[Other IV Infusion post-deliv.],
			[Other IV Infusion post-deliv. Infusion],
			[Oxytocics in third stage Drug 1],
			[Oxytocics in third stage Drug 2],
			[Oxytocics in third stage Drug 3],
			[Oxytocics in third stage Text],
			[Oxytocics in third stage],
			[Patient rhesus negative],
			[Patient rhesus negative Cord samples taken],
			[Patient rhesus negative Maternal samples taken],
			[Patient rhesus negative Text],
			[Per vaginum loss Text],
			[Per vaginum loss],
			[Perineal_damage],
			[Perineal_damage_repair],
			[Placental condition],
			[Placental condition Text],
			[Post delivery problems],
			[Post delivery problems Problem 1],
			[Post delivery problems Problem 2],
			[Post delivery problems Problem 3],
			[Post delivery problems Text],
			[Premature],
			[Reason for P/N Analg/Anaes],
			[Removal of epidural catheter],
			[Remove skin sutures Text],
			[Remove skin sutures],
			[Retained placenta],
			[Secondary_PPH],
			[Severe_birth_asphix],
			[Skin closure material],
			[Smoking at delivery Amount smoked currently],
			[Smoking at delivery],
			[SMR Op Deliv ICD10 Code],
			[Special interest memo required],
			[Special interest memo required Recipient],
			[Staff Type reporting incident],
			[Sterilised_at_caesarean],
			[Suture material used],
			[Suture material used Text],
			[Time in labour (hrs:mins)],
			[Time of delivery admission],
			[Time of first Birth],
			[Time of incident],
			[Time of placental delivery],
			[Tot. length labour (hrs:mins)],
			[Total Blood Loss],
			[Total time on labour ward],
			[total_number_of_pregnancies],
			[Type of delivery],
			[Type of person supervising],
			[Type of prsn removing catheter],
			[Type of skin incision],
			[Type of skin incision Method],
			[Type of uterine incision],
			[Type of uterine incision Method],
			[Type_I_damage_repaired],
			[Type_I_perineal_damage],
			[Type_II_damage_repaired],
			[Type_II_perineal_damage],
			[Type_III_or_IV_perineal_damage],
			[Type_III_or_IV_repaired],
			[Urine passed],
			[Uterine closure material],
			[Uterine state post-partum],
			[Uterine state post-partum Text],
			[Vacuum_most_sig_birth_method],
			[Vaginal or cervical damage Other],
			[Vaginal or cervical damage],
			[Vaginal or cervical damage Vaginal Tear],
			[Vaginal or cervical damage Labial Tear],
			[Vaginal or cervical damage Cervical Tear],
			[Very_premature],
			[Was any equipment involved],
			[Was any equipment involved Text],
			[Was suturing person supervised],
			[Was there placenta praevia],
			[Was there placenta praevia Type],
			[Anaesthetic follow-up arranged When],
			[Anaesthetic follow-up arranged Text],
			[Anaesthetic follow-up arranged],
			[Anaesthetic problems],
			[Anaesthetic problems Text],
			[Anti-D given post-partum Batch],
			[Anti-D given post-partum Text],
			[Anti-D given post-partum],
			[Anti-D given post-partum Date],
			[Anti-D given post-partum Time],
			[Anti-D given post-partum Dose],
			[Any other Appointment],
			[Any other Appointment When],
			[Any other Appointment Text],
			[Any Postnatal illnesses],
			[Any Postnatal illnesses Details],
			[Blood transfusion given],
			[Blood transfusion given Amount],
			[Breast condition],
			[Breast condition Text],
			[Breasts],
			[Breasts Text],
			[Community discharge comments],
			[Community discharge comments Text],
			[Date admitted to PN Ward],
			[Date of Community Discharge],
			[Date of Mothers PN assessment],
			[Effectiveness],
			[Epidural problems],
			[Epidural problems Headache],
			[Epidural problems Backache],
			[Epidural problems Other Problems],
			[ERPC performed],
			[Family planning advice given],
			[Family planning advice given Method],
			[Feeding Intention at Discharge],
			[Follow up arrangements Text],
			[Follow up arrangements],
			[Follow up arrangements When],
			[Gynae Appointment Arranged Text],
			[Gynae Appointment Arranged],
			[Gynae Appointment Arranged When],
			[Important puerperal problems],
			[Infection Problems Text],
			[Infection Problems Abdominal Wound],
			[Infection Problems],
			[Infection Problems Perineal],
			[Infection Problems Uterine],
			[Infection Problems Urinary],
			[Maternity Care given by own GP],
			[Maternity Care given by own GP GP name],
			[Maternity Care given by own GP Address_1],
			[Maternity Care given by own GP Address_2],
			[Maternity Care given by own GP Address_3],
			[Maternity Care given by own GP Address_4],
			[Maternity Care given by own GP Postcode],
			[Medical Appointment Arranged Text],
			[Medical Appointment Arranged],
			[Medical Appointment Arranged When],
			[Midwife to Consultant Transfer],
			[Most recent postpartum Hb],
			[Most recent postpartum Hb Date],
			[Mother Assessment completed by],
			[Mother Assessment completed by Status],
			[Mother Discharge Summary],
			[Mother Discharge Summary Filename],
			[Mothers Disch. Summary Printed],
			[Mothers Disch. Summary Printed filename],
			[No. of P/N days at Discharge],
			[No. of P/N visits by Midwife],
			[No. of wasted visits],
			[Number of midwives seen],
			[Other post-partum problems],
			[Other post-partum problems Text],
			[Other Puerperal Problems],
			[Other Puerperal Problems Text],
			[Perineal or episiotomy problem Text],
			[Perineal or episiotomy problem],
			[Post Partum blood press. done],
			[Post Partum blood press. done Systolic],
			[Post Partum blood press. done Diastolic],
			[Post-natal check],
			[Postnatal Maternal problems],
			[Postnatal Maternal problems Perineum],
			[Postnatal Maternal problems Other],
			[Postnatal Maternal problems Abdominal Wound],
			[Postnatal Maternal problems Postnatal Depression],
			[Postnatal smear required],
			[Reason Anti-D not given],
			[Reason for late comm. disch.],
			[Recommendations],
			[Recommendations Text],
			[Rubella Vaccination],
			[Rubella Vaccination Reason],
			[Secondary PPH],
			[Secondary PPH Amount],
			[Smoking postnatally],
			[Take Home Medications],
			[Take Home Medications Iron],
			[Take Home Medications Analgesics],
			[Take Home Medications Other],
			[Take Home Medications Antibiotics],
			[Take Home Medications Antihypertensives],
			[Time of Mothers PN assessment],
			[Type of persn doing assessment])
		VALUES
			(@pointer
			,@p_pregnancy_id  --v1.2
			,@l_delivery_number
			,@p_pregnancy_number
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPPi')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPPc')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPPf')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPPb')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPPd')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPPe')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPPh')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPPk')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPPj')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPPg')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLSX')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLs1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMMD4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMMD4'),'Date started')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMMD4'),'Number of doses')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLSZ'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLSZ')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLSZ'),'Date started')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLSZ'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLDG')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLDG'),'Drug 1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLDG'),'Drug 2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLDG'),'Drug 3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLDG'),'Drug 4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLDG'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLIB')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME11')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLS5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLI9')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLSW'),'Drug 2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLSW'),'Drug 3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLSW'),'Drug 4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLSW'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLSW')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLSW'),'Drug 1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLSB')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLSB'),'Method')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLSF')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLSF'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLSD')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME09')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLSN')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLSN'),'Text')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMABL'),'Reason')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMABL'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMABL')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMABL'),'Date started')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMABL'),'Time started')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMnp1'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMnp1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLI3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLI3'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLIC')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLIC'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMIAS')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLI2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLI2'),'Date given')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLI2'),'Time given')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMID2'),'Date given')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMID2'),'Time given')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMID2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMID3'),'Date given')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMID3'),'Time given')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMID3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLSE')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLSE'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLIK')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLSC')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLS8'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLS8')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLS8'),'Drug 1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLS8'),'Drug 2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLS8'),'Drug 3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLI5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLSQ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME10')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLSK')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLSJ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLS9')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME13')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSIE')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLSG')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME12')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLIF'),'Date Started')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLIF'),'Time Started')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLIF')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME14')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLSA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLS6')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLIA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLS4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLS4'),'ARM_Date')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLS4'),'ARM_Time')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLS4'),'Date Syntocinon Started')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLS4'),'Time Syntocinon Started')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLS4'),'Other Method')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLS1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLSH')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLSO')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLSY')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLSY'),'Complications')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLSY'),'Comments')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC33')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC33'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC34')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC34'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPi')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPi'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME31')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLs2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLP6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLP6'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC30')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPOP')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOP'),'Self_admin._inhalation')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOP'),'Pudendal_Block')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOP'),'Local_Infiltration')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOP'),'Epidural')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOP'),'Spinal')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOP'),'Caudal')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOP'),'General_Anaesthetic')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOP'),'Combined spinal/epidural')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC76')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC77')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC77'),'Type of help to which refd.')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC84')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbns')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC0A')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME29')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC13')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC13'),'Number of units')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME42')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME39')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME49')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC50')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC50'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME50')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC55'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC55')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMCE4'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCE4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMCE4'),'Technique')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMCE4'),'Catheter in situ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPv')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPe')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPe'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC16')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC16'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC08')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC08'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC02')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC31')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC31'),'Local Anaesthetic')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC31'),'Spinal')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC31'),'Epidural')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC31'),'General Anaesthesia')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC31'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPx')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPw')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC57')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC57'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC70')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC70'),'Currently')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC27')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC01')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCTX')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC62')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC05')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC10')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCTZ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCTU')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPd')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPd'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC11')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC11'),'Probable Reason')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC11'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME38')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME32')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCEA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME35')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC25')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME40')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC60')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME47')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCSA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPy')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC42')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC40'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC40')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC40'),'Problem 1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC40'),'Problem 2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC40'),'Problem 3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC80')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC80'),'Induction')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC80'),'Augmentation')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC80'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMkns')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC07')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC64')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME34')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC12')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC12'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC82')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC82'),'Problem')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC73')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMS01')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMS02')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLSS')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME33')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb38')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC04')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb35')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLSM')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC24')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC24'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCED')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMCED'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC29')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC61'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC61')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPc')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPc'),'Findings')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME46')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLSL')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMnp2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC83'),'Care Required')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC83')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC81')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC81'),'Infusion')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC09'),'Drug 1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC09'),'Drug 2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC09'),'Drug 3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC09'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC09')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCRN')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMCRN'),'Cord samples taken')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMCRN'),'Maternal samples taken')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMCRN'),'Text')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMCE2'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCE2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME36')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME37')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMN04')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMN04'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC14')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC14'),'Problem 1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC14'),'Problem 2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC14'),'Problem 3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC14'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME57')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC32')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCEB')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPz'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPz')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC90')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME41')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME30')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPf')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC75'),'Amount smoked currently')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC75')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLSI')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC56')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC56'),'Recipient')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC59')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME28')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC28')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC28'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC21')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCTW')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC17')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC63')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC06')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC20')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC41')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC0B')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME99')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCTY')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC23')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCEC')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPb')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPb'),'Method')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPh')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOPh'),'Method')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME63')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME60')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME64')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME61')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME62')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME65')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCE3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOPM')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCE1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMCE1'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME48')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC26'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC26')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC26'),'Vaginal Tear')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC26'),'Labial Tear')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC26'),'Cervical Tear')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME56')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC58')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC58'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC22')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMC35')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMC35'),'Type')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNo'),'When')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNo'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNo')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNR')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNR'),'Text')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNh'),'Batch')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNh'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNh')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNh'),'Date')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNh'),'Time')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNh'),'Dose')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNu')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNu'),'When')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNu'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLP3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLP3'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNb')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNb'),'Amount')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLP2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLP2'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPN9')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPN9'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZFCDC')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZFCDC'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNS')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPL8')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPQ1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNM')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNO')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNO'),'Headache')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNO'),'Backache')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNO'),'Other Problems')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNn')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNp')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNp'),'Method')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNL')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZFFUA'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZFFUA')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZFFUA'),'When')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNs'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNs')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNs'),'When')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPN2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPN6'),'Text')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPN6'),'Abdominal Wound')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPN6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPN6'),'Perineal')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPN6'),'Uterine')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPN6'),'Urinary')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBF')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBF'),'GP name')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBF'),'Address_1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBF'),'Address_2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBF'),'Address_3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBF'),'Address_4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPBF'),'Postcode')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNt'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNt')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNt'),'When')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPBI')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNK')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNK'),'Date')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNX')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNX'),'Status')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPMD')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPMD'),'Filename')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMMDS')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMMDS'),'filename')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPL9')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPLA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPLE')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPLD')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPRH')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPRH'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNU')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNU'),'Text')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNT'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNT')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPN5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPN5'),'Systolic')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPN5'),'Diastolic')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMDSG')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLP1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLP1'),'Perineum')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLP1'),'Other')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLP1'),'Abdominal Wound')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMLP1'),'Postnatal Depression')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNV')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPnh')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPLC')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNv')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNv'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNd')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNd'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPN3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPN3'),'Amount')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMLPD')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNQ')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNQ'),'Iron')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNQ'),'Analgesics')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNQ'),'Other')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNQ'),'Antibiotics')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPNQ'),'Antihypertensives')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPQ2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPNA'))

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_Delivery', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating delivery record ' + cast(@l_delivery_number as varchar) + ' for pregancy ' + @p_pregnancy_number + ' for patient ' + cast(@pointer as varchar), -1 )
		END
		ELSE
		BEGIN
					EXEC dbo.pr_Explode_Baby @pointer, @p_pregnancy_id, @p_pregnancy_number,@l_delivery_number, @NOTENO
		END

	            FETCH NEXT FROM get_delivery INTO @NOTENO
	END

	CLOSE get_delivery
	DEALLOCATE get_delivery
END


