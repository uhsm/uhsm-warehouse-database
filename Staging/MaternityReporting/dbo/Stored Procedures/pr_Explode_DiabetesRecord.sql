﻿
/*
**************************************************************************************************************************
**	NAME:		pr_Explode_DiabetesRecord						**
**	DESCRIPTION:	Runs the Diabetes Record'explode' routine				**
**	AUTHOR:	Stephen Creek								**
**	VERSIONS:	1.0	Stephen Creek	28/02/2008	Initial version created 		**
**************************************************************************************************************************
*/
CREATE PROCEDURE [dbo].[pr_Explode_DiabetesRecord]
AS
BEGIN
	DECLARE @pointer INT
	DECLARE @NOTENO  VARCHAR(5)
	DECLARE @SQLError INT

	DECLARE get_DiabetesRecord CURSOR FOR
	SELECT Pointer,Noteno
 	FROM tbl_Notes_Copy
	WHERE Code = 'ZEP2D' -- Diabetes Record

	TRUNCATE TABLE [tbl_DiabetesRecord]

	OPEN get_DiabetesRecord
	FETCH NEXT FROM get_DiabetesRecord INTO
	@pointer,@NOTENO

	DECLARE @RowCount INT
	SET @RowCount = 0

	WHILE @@fetch_status = 0
	BEGIN
		SET @NOTENO = SUBSTRING(@NOTENO,1,1)

		INSERT INTO [tbl_DiabetesRecord] 
			(Patient_Pointer,
			[Date of diabetes registration],
			[Diabetes registration status],
			[Diabetes registration status Reason],
			[Blood glucose at diagnosis 1],
			[Blood glucose at diagnosis 1 Method],
			[Blood glucose at diagnosis 2],
			[Blood glucose at diagnosis 2 Method],
			[Diabetes diagnostic criteria],
			[Diabetes diagnostic criteria Text],
			[OGTT 0 hour glucose at diagnosis],
			[OGTT 0 hour glucose at diagnosis Method],
			[OGTT 2 hour glucose at diagnosis],
			[OGTT 2 hour glucose at diagnosis Method],
			[Urine ketones at diagnosis]
			)
		VALUES
			(@pointer
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDHI9')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDREC')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDREC'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDHS1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDHS1'),'Method')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDHSB')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDHSB'),'Method')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDHI5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDHI5'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDHS8')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDHS8'),'Method')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDHS2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDHS2'),'Method')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDHS3')
			)

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_DiabetesRecord', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating diabetes record for patient ' + cast(@pointer as varchar), -1 )
		END
		ELSE
			SET @RowCount = @RowCount + 1

		FETCH next from get_DiabetesRecord into
		@pointer,@NOTENO
	END
	CLOSE get_DiabetesRecord
	DEALLOCATE get_DiabetesRecord

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	VALUES ( getdate(), 'pr_Explode_DiabetesRecord', 'tbl_DiabetesRecord records created', @RowCount )
END
