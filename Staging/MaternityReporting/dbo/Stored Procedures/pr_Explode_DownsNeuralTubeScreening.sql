﻿

/*
**************************************************************************************************************************
**	NAME:		pr_Explode_DownsNeuralTubeScreening					**
**	DESCRIPTION:	Runs the New Delivery 'explode' routine					**
**	AUTHOR:	Stephen Creek								**
**	VERSIONS:	1.0	Stephen Creek	27/02/2007	Initial version created		**
**			1.1	Kevin Daley	08/03/2010	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**											Noteno has changed from a 5 byte binary coded structured key
**											to a 25 byte (5 x 5 bytes) ASCII coded key.
**************************************************************************************************************************
*/
CREATE  PROCEDURE [dbo].[pr_Explode_DownsNeuralTubeScreening]
	(@pointer int
	,@p_pregnancy_id int
	,@p_pregnancy_number varchar(30)
	,@p_noteno varchar(25))
AS
BEGIN
	DECLARE @NOTENO  VARCHAR(25)
	DECLARE @l_delivery_number INT
	DECLARE @SQLError INT

	SELECT @NOTENO = Noteno
	FROM tbl_Notes_Copy
	WHERE Code = 'ZMHPN'  -- Downs/Neural Tube Screening
	AND Pointer = @pointer
	AND substring(Noteno,1,len('x'+@p_noteno+'x')-2) = @p_noteno 
	ORDER BY Noteno

	IF @NOTENO IS NOT NULL 
	BEGIN

--		SET @NOTENO = SUBSTRING(@NOTENO,1,2)
--changed by KD 1.1
		SET @NOTENO = SUBSTRING(@NOTENO,1,10)
		INSERT INTO [tbl_DownsNeuralTubeScreening] 
			(Patient_Pointer,
			Pregnancy_ID,
			[Pregnancy number], 
			[Consent to Use of Downs screening data for audit],
			[Date of booking],
			[Number of babies expected],
			[Number of babies expected Basis],
			[Date of informing client],
			[Method of informing client],
			[Type of specialist informing client],
			[Type of specialist informing client Screening coordinator],
			[Type of specialist informing client Clinic midwife],
			[Type of specialist informing client Community midwife],
			[Type of specialist informing client GP],
			[Type of specialist informing client Consultant],
			[Type of specialist informing client Sonographer],
			[Type of specialist informing client Other Specialist],
			[Initial counselling offered],
			[Initial counselling offered Reason],
			[Initial counselling accepted],
			[Initial counselling accepted Reason],
			[Date initial counselling offered],
			[Type of person offering initial counselling],
			[Name of person offering initial counselling],
			[Grade of person offering initial counselling],
			[Type of specialist performing initial counselling],
			[Type of specialist performing initial counselling Screening coordinator],
			[Type of specialist performing initial counselling Clinic midwife],
			[Type of specialist performing initial counselling Community midwife],
			[Type of specialist performing initial counselling GP],
			[Type of specialist performing initial counselling Consultant],
			[Type of specialist performing initial counselling Sonographer],
			[Type of specialist performing initial counselling Other Specialist],
			[Options counselling offered],
			[Options counselling offered Reason],
			[Options counselling accepted],
			[Options counselling accepted Reason],
			[Date options counselling offered],
			[Type of person offering options counselling],
			[Name of person offering options counselling],
			[Grade of person offering options counselling],
			[Type of specialist performing options counselling],
			[Type of specialist performing options counselling Screening coordinator],
			[Type of specialist performing options counselling Clinic midwife],
			[Type of specialist performing options counselling Community midwife],
			[Type of specialist performing options counselling GP],
			[Type of specialist performing options counselling Consultant],
			[Type of specialist performing options counselling Sonographer],
			[Type of specialist performing options counselling Other specialist],
			[Information leaflet given],
			[Type of person giving information],
			[Name of person giving information],
			[Grade of person giving information],
			[Downs screening offered],
			[Downs screening offered Reason],
			[Downs screening offered Date],
			[Downs screening accepted],
			[Downs screening accepted Reason],
			[Type of person offering Downs screening],
			[Name of person offering Downs screening],
			[Grade of person offering Downs screening],
			[First trimester serum test offered],
			[First trimester test accepted],
			[Date first trimester serum test taken],
			[Date first trimester serum test result released],
			[First trimester serum test result],
			[First trimester serum test result Risk is 1:],
			[Neural tube defect test offered],
			[Neural tube defect test accepted],
			[Date neural tube defect test taken],
			[Date neural tube defect test result released],
			[Neural tube defect test result],
			[Neural tube defect test result Weight corrected MoM:],
			[First trimester nuchal translucency test offered],
			[First trimester nuchal translucency test accepted],
			[Date first trimester nuchal translucency test taken],
			[Date nuchal translucency test result released],
			[Type of pre-natal test],
			[Pre-natal test indication Other],
			[Pre-natal test indication],
			[Pre-natal test indication High screening risk],
			[Pre-natal test indication Maternal age],
			[Pre-natal test indication Family History],
			[Pre-natal test indication Ultrasound indication],
			[Date of pre-natal test],
			[Type of person performing pre-natal test],
			[Name of person performing pre-natal test],
			[Grade of person performing pre-natal test],
			[Cytogenetic laboratory technique],
			[Cytogenetic laboratory technique Full Karyotype],
			[Cytogenetic laboratory technique QPCR],
			[Cytogenetic laboratory technique FISH],
			[Cytogenetic laboratory technique DNA analysis],
			[Double test offered],
			[Double test accepted],
			[Date double test performed],
			[Date double test result released],
			[Double test result],
			[Double test result Risk is 1:],
			[Triple test offered],
			[Triple test accepted],
			[Date triple test performed],
			[Date triple test result released],
			[Triple test result],
			[Triple test result Risk is 1:],
			[Quadruple test offered],
			[Quadruple test accepted],
			[Date quadruple test performed],
			[Date quadruple test result released],
			[Quadruple test result],
			[Quadruple test result Risk is 1:])
		VALUES
			(@pointer
			,@p_pregnancy_id
			,@p_pregnancy_number  --v1.2
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD01')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD02')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD80')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD80'),'Basis')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD29')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD30')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD31')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD31'),'Screening coordinator')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD31'),'Clinic midwife')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD31'),'Community midwife')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD31'),'GP')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD31'),'Consultant')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD31'),'Sonographer')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD31'),'Other Specialist')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD32')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD32'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD33')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD33'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD34')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD35')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD36')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD37')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD38')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD38'),'Screening coordinator')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD38'),'Clinic midwife')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD38'),'Community midwife')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD38'),'GP')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD38'),'Consultant')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD38'),'Sonographer')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD38'),'Other Specialist')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD39')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD39'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD40')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD40'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD41')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD42')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD43')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD44')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD45')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD45'),'Screening coordinator')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD45'),'Clinic midwife')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD45'),'Community midwife')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD45'),'GP')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD45'),'Consultant')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD45'),'Sonographer')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD45'),'Other specialist')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD03')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD04')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD05')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD06')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD07')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD07'),'Reason')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD07'),'Date')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD08')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD08'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD09')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD10')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD11')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD12')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD13')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD14')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD58')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD59')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD59'),'Risk is 1:')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD68')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD69')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD70')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD71')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD72')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD72'),'Weight corrected MoM:')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD15')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD16')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD17')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD60')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD46')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD47'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD47')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD47'),'High screening risk')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD47'),'Maternal age')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD47'),'Family History')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD47'),'Ultrasound indication')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD48')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD49')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD50')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD51')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD52')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD52'),'Full Karyotype')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD52'),'QPCR')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD52'),'FISH')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD52'),'DNA analysis')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD18')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD19')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD20')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD62')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD63')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD63'),'Risk is 1:')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD21')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD22')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD23')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD64')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD65')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD65'),'Risk is 1:')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD24')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD25')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD26')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD66')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD67')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD67'),'Risk is 1:')
			)

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_DownsNeuralTubeScreening', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating downs screening record for pregancy ' + @p_pregnancy_number + ' for patient ' + cast(@pointer as varchar), -1 )
		END
		ELSE
		BEGIN
			EXEC dbo.pr_Explode_DownsNeuralTubeScreening_Fetus @pointer, @p_pregnancy_id, @p_pregnancy_number, @NOTENO
		END
	END

END

