﻿


/*
************************************************************************************************************************************
**	NAME:		pr_Explode_DownseuralTubeScreening_Fetus					**
**	DESCRIPTION:	Runs the Downs/Neural Tube screening (Fetus) 'explode' routine			**
**	AUTHOR:	Stephen Creek									**
**	VERSIONS:	1.0	Stephen Creek	27/02/2008	Initial version created			**
**			1.1	Kevin Daley	08/03/2010	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**											Noteno has changed from a 5 byte binary coded structured key
**											to a 25 byte (5 x 5 bytes) ASCII coded key.
************************************************************************************************************************************
*/
CREATE  PROCEDURE [dbo].[pr_Explode_DownsNeuralTubeScreening_Fetus]
	(@pointer int
	,@p_pregnancy_id int
	,@p_pregnancy_number varchar(30)
	,@p_noteno VARCHAR(25))
AS
BEGIN
	DECLARE @NOTENO  VARCHAR(25)
	DECLARE @SQLError INT
	DECLARE @l_fetus_number int
	DECLARE @l_fetal_identifier varchar(30)

	DECLARE get_fetus_screening CURSOR FOR
	SELECT Noteno
	FROM tbl_Notes_Copy
	WHERE Code = 'ZMHPf' -- Fetus
	AND Pointer = @pointer
	AND substring(Noteno,1,len('x'+@p_noteno+'x')-2) = @p_noteno 
	ORDER BY Noteno

	OPEN get_fetus_screening
	FETCH next from get_fetus_screening into @NOTENO

	SET @l_fetus_number = 0

	WHILE @@fetch_status = 0
	BEGIN
--		SET @NOTENO = SUBSTRING(@NOTENO,1,3)
		SET @NOTENO = SUBSTRING(@NOTENO,1,15)
		SET @l_fetus_number = @l_fetus_number + 1	
		SET @l_fetal_identifier = dbo.func_Extract_Value(@pointer,@NOTENO,'ZMDID')

		-- Error trap for missing Fetus Identifier 
		IF @l_fetal_identifier IS NULL
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_DownsNeuralTubeScreening_Fetus', 'Missing Fetal Identifier encountered creating fetus screening record '+cast(@l_fetus_number as varchar)+' for pregnancy ' + cast(@p_pregnancy_id as varchar)+ ' for patient ' + cast(@pointer as varchar), -1)
		END

		INSERT INTO [tbl_DownsNeuralTubeScreening_Fetus] 
			(Patient_Pointer,
			Pregnancy_ID,
			[Pregnancy number],
			Fetus_Number,
			[Fetal identifier],
			[Nuchal translucency test result],
			[Nuchal translucency test result Risk is 1:],
			[Combined test result],
			[Combined test result Risk is 1:],
			[Date pre-natal test result released],
			[Pre-natal test result],
			[Pre-natal test result Other],
			[Date of informing client of test result],
			[Method of informing client of test result],
			[Type of specialist informing client of test result],
			[Type of specialist informing client of test result Screening coordinator],
			[Type of specialist informing client of test result Clinic midwife],
			[Type of specialist informing client of test result Community midwife],
			[Type of specialist informing client of test result GP],
			[Type of specialist informing client of test result Consultant],
			[Type of specialist informing client of test result Sonographer],
			[Type of specialist informing client of test result Other specialist])
		VALUES
			(@pointer
			,@p_pregnancy_id
			,@p_pregnancy_number
			,@l_fetus_number
			,@l_fetal_identifier
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD61')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD61'),'Risk is 1:')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD81')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD81'),'Risk is 1:')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD53')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD54')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD54'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD55')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD56')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMD57')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD57'),'Screening coordinator')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD57'),'Clinic midwife')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD57'),'Community midwife')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD57'),'GP')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD57'),'Consultant')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD57'),'Sonographer')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMD57'),'Other specialist'))

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_DownsNeuralTubeScreening_Fetus', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating fetus screening record for pregnancy ' + cast(@p_pregnancy_number as varchar) + ' for patient ' + cast(@pointer as varchar), -1 )
		END

	            FETCH NEXT FROM get_fetus_screening INTO @NOTENO
	END

	CLOSE get_fetus_screening
	DEALLOCATE get_fetus_screening
END

