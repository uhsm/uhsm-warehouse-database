﻿/*
****************************************************************************************************************
**	NAME:		pr_Explode_FamilyHistory					**
**	DESCRIPTION:	Runs the Family History 'explode' routine				**
**	AUTHOR:	Stephen Creek							**
**	VERSIONS:	1.0	Stephen Creek	18/10/2007	Initial version created	**
**			1.1	Stephen Creek	22/10/2007	Added event logging	**
**			1.2	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**											Noteno has changed from a 5 byte binary coded structured key
**											to a 25 byte (5 x 5 bytes) ASCII coded key.
****************************************************************************************************************
*/
CREATE PROCEDURE [dbo].[pr_Explode_FamilyHistory]
AS
BEGIN
	DECLARE @pointer INT
	DECLARE @NOTENO  VARCHAR(25)
	DECLARE @SQLError INT

	DECLARE get_FamilyHistory CURSOR FOR
	SELECT Pointer,Noteno
 	FROM tbl_Notes_Copy
	WHERE Code = 'ZERFM'

	TRUNCATE TABLE [tbl_FamilyHistory]

	OPEN get_FamilyHistory
	FETCH next from get_FamilyHistory into
	@pointer,@NOTENO

	DECLARE @RowCount INT
	SET @RowCount = 0

	WHILE @@fetch_status = 0
	BEGIN
		--v1.2 SET @NOTENO = SUBSTRING(@NOTENO,1,1)
		SET @NOTENO = SUBSTRING(@NOTENO,1,5)

		INSERT INTO [tbl_FamilyHistory] 
			(Patient_Pointer,
			[Fam hist premature CVS disease],
			[Fam hist premature CVS disease Text],
			[Other family history],
			[Other family history Problem 1],
			[Other family history Problem 2],
			[Other family history Text],
			[Fam hist of premature CVA],
			[Fam hist of premature CVA Text],
			[Fam hist premature CVS death],
			[Fam hist premature CVS death Text],
			[Fam hist of premature CHD],
			[Fam hist of premature CHD Text],
			[Family history hyperlipidaemia],
			[Family history hyperlipidaemia Text],
			[Family history of diabetes],
			[Family history of diabetes Type_1],
			[Family history of diabetes Type_2],
			[Family history of diabetes Type_unknown],
			[Family history of diabetes Details],
			[Family history of sickle cell],
			[Family history of sickle cell Details],
			[Family history of thalassaemia],
			[Family history of thalassaemia Details],
			[FH Thrombo-embolic disease],
			[FH Thrombo-embolic disease Details],
			[FH of preg ind hypertension],
			[FH of preg ind hypertension Details],
			[Fam hist of autoimmune disease],
			[Fam hist of autoimmune disease Thyroid disease],
			[Fam hist of autoimmune disease Pernicious anaemia],
			[Fam hist of autoimmune disease Addisons disease],
			[Fam hist of autoimmune disease Coelic disease],
			[Fam hist of autoimmune disease Vitilgo],
			[Fam hist of autoimmune disease Text],
			[Family history of hypertension],
			[Family history of hypertension Details])
		VALUES
			(@pointer
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZHHFA')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZHHFA'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSMDF')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSMDF'),'Problem 1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSMDF'),'Problem 2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSMDF'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZHHF2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZHHF2'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZHHF3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZHHF3'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZHHF4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZHHF4'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZHHF5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZHHF5'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDHI7')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDHI7'),'Type_1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDHI7'),'Type_2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDHI7'),'Type_unknown')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDHI7'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFM7')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMFM7'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFM8')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMFM8'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMHFA')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMHFA'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMHF2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMHF2'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERF1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZERF1'),'Thyroid disease')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZERF1'),'Pernicious anaemia')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZERF1'),'Addisons disease')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZERF1'),'Coelic disease')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZERF1'),'Vitilgo')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZERF1'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMHF3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMHF3'),'Details'))

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_FamilyHistory', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating family history record  for patient ' + cast(@pointer as varchar), -1 )
		END
		ELSE
			SET @RowCount = @RowCount + 1

    		FETCH next from get_FamilyHistory into
		@pointer,@NOTENO
	END
	CLOSE get_FamilyHistory
	DEALLOCATE get_FamilyHistory

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	VALUES ( getdate(), 'pr_Explode_FamilyHistory', 'tbl_FamilyHistory records created', @RowCount )
END
