﻿/*
************************************************************************************************************************************
**	NAME:		pr_Explode_FetalObservations							**
**	DESCRIPTION:	Runs the New Fetal Observations 'explode' routine					**
**	AUTHOR:	Stephen Creek									**
**	VERSIONS:	1.0	Stephen Creek	08/11/2007	Initial version created			**
**			1.1	Stephen Creek	08/02/2007	Corrected use of len() function		**
								Added check for missing Fetal Identifier	**
***			1.2	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**											Noteno has changed from a 5 byte binary coded structured key
**											to a 25 byte (5 x 5 bytes) ASCII coded key.
************************************************************************************************************************************
*/
CREATE PROCEDURE [dbo].[pr_Explode_FetalObservations]
	(@pointer int
	,@p_pregnancy_id int
	,@p_pregnancy_number varchar(30)
	,@p_review_number int
	,@p_noteno VARCHAR(25))
AS
BEGIN
	DECLARE @NOTENO  VARCHAR(25)
	DECLARE @SQLError INT
-- v1.1 start
	DECLARE @l_fetus_number int
	DECLARE @l_fetal_identifier varchar(30)
-- v1.1 end

	DECLARE get_observations CURSOR FOR
	SELECT Noteno
	FROM tbl_Notes_Copy
	WHERE Code = 'ZMFOO' -- Fetal Observations
	AND Pointer = @pointer
	AND substring(Noteno,1,len('x'+@p_noteno+'x')-2) = @p_noteno   -- v1.1
	ORDER BY Noteno

	OPEN get_observations
	FETCH next from get_observations into @NOTENO

	SET @l_fetus_number = 0

	WHILE @@fetch_status = 0
	BEGIN
		--v1.2 SET @NOTENO = SUBSTRING(@NOTENO,1,4)
		SET @NOTENO = SUBSTRING(@NOTENO,1,20)

		SET @l_fetus_number = @l_fetus_number + 1	
		SET @l_fetal_identifier = dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFID')

-- v1.1 start
		-- Error trap for missing Birth Order - found at Barnsley 16/1
		IF @l_fetal_identifier IS NULL
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_Fetalbservations', 'Missing Fetal Identifier encountered creating fetal observations record '+cast(@l_fetus_number as varchar)+' for review '+cast(@p_review_number as varchar)+' for pregnancy ' + cast(@p_pregnancy_id as varchar)+ ' for patient ' + cast(@pointer as varchar), -1)
		END
-- v1.1 end

		INSERT INTO [tbl_FetalObservations] 
			(Patient_Pointer,
			Pregnancy_ID,
			[Pregnancy number],
			Review_Number,
			Fetus_Number,
			[Fetal identifier],
			[Abdomen at scan],
			[Abdomen at scan Text],
			[Abdomen circumference at scan],
			[Amniocentesis],
			[Amniocentesis Text],
			[Amniotic fluid index],
			[Amniotic fluid volume],
			[Biparietal diameter at scan],
			[Cardiotocograph],
			[Cardiotocograph Result],
			[Cardiotocograph Text],
			[Chorionic villus biopsy],
			[Chorionic villus biopsy Text],
			[Comments on review scan],
			[Comments on review scan Comments],
			[Crown-rump length at scan],
			[Extremities at scan],
			[Extremities at scan Text],
			[Face at scan],
			[Face at scan Text],
			[Femur length at scan],
			[Fetal blood sample],
			[Fetal blood sample Date sample taken3],
			[Fetal blood sample Time sample taken3],
			[Fetal blood sample pH3],
			[Fetal blood sample Base excess3],
			[Fetal blood sample Date sample taken4],
			[Fetal blood sample Time sample taken4],
			[Fetal blood sample pH4],
			[Fetal blood sample Base excess4],
			[Fetal blood sample Date sample taken5],
			[Fetal blood sample Time sample taken5],
			[Fetal blood sample pH5],
			[Fetal blood sample Base excess5],
			[Fetal blood sample Date sample taken6],
			[Fetal blood sample Time sample taken6],
			[Fetal blood sample pH6],
			[Fetal blood sample Base excess6],
			[Fetal blood sample Date sample taken1],
			[Fetal blood sample Time sample taken1],
			[Fetal blood sample pH1],
			[Fetal blood sample Base excess1],
			[Fetal blood sample Date sample taken2],
			[Fetal blood sample Time sample taken2],
			[Fetal blood sample pH2],
			[Fetal blood sample Base excess2],
			[Fetal gestation at scan],
			[Fetal gestation at scan Days],
			[Fetal heartbeat],
			[Fetal heartbeat Rate],
			[Fetal heartbeat Method],
			[Fetal lie],
			[Fetal measurements],
			[Fetal position],
			[Fetal presentation],
			[Fetal umbilical artery doppler],
			[Fetal umbilical artery doppler Result],
			[Fetal umbilical artery doppler A:B Ratio],
			[Fetal umbilical artery doppler Text],
			[Genitalia at scan],
			[Genitalia at scan Text],
			[Head at scan],
			[Head at scan Text],
			[Head circumference at scan],
			[Heart at scan],
			[Heart at scan Text],
			[Indication for amniocentesis],
			[Indication for amniocentesis Text],
			[Indication for CVB],
			[Indication for CVB Text],
			[Indication for Nuchal scan],
			[Indication for Nuchal scan Text],
			[Kidneys at scan],
			[Kidneys at scan Text],
			[Liquor volume],
			[Liquor volume at scan],
			[Lungs at scan],
			[Lungs at scan Text],
			[Neck/skin  at scan],
			[Neck/skin  at scan Text],
			[Nuchal scan test],
			[Nuchal scan test Text],
			[Nuchal translucency risk fctr],
			[Placental appearance at scan],
			[Placental appearance at scan Text],
			[Placental biopsy],
			[Placental biopsy Text],
			[Placental relation to Lwr seg.],
			[Placental site],
			[Placental site Text],
			[Presentation at scan],
			[Relationship to brim],
			[Spine at scan],
			[Spine at scan Text])
		VALUES
			(@pointer
			,@p_pregnancy_id
			,@p_pregnancy_number
			,@p_review_number
			,@l_fetus_number
			,@l_fetal_identifier
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZF007')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZF007'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPIB')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP1P')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP1P'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMA1Z')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMA11')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPI9')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMA1F')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1F'),'Result')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1F'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPI3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPI3'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP2S')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP2S'),'Comments')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPIF')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZF009')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZF009'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZF002')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZF002'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPIC')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMA1L')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'0Date sample taken3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'1Time sample taken3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'2pH3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'3Base excess3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'4Date sample taken4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'5Time sample taken4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'6pH4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'7Base excess4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'8Date sample taken5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'9Time sample taken5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'0pH5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'1Base excess5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'2Date sample taken6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'3Time sample taken6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'4pH6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'5Base excess6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'Date sample taken1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'Time sample taken1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'pH1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'Base excess1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'Date sample taken2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'Time sample taken2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'pH2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1L'),'Base excess2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPI8')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPI8'),'Days')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMA1A')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1A'),'Rate')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1A'),'Method')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMA1G')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMA1Y')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMA1K')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMA1B')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMA1E')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1E'),'Result')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1E'),'A:B Ratio')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1E'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZF010')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZF010'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZF001')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZF001'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPIA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZF006')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZF006'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP03')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP03'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPI1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPI1'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPIJ')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPIJ'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZF008')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZF008'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMA1D')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPID')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZF005')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZF005'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZF004')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZF004'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPII')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPII'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP1U')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMS11')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMS11'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPIT')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPIT'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSO9')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMS10')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMS10'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPIE')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMA1I')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZF003')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZF003'),'Text'))

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_FetalObservations', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating fetal observations record for review ' + cast(@p_review_number as varchar) + ' for pregnancy ' + cast(@p_pregnancy_number as varchar) + ' for patient ' + cast(@pointer as varchar), -1 )
		END

	            FETCH NEXT FROM get_observations INTO @NOTENO
	END

	CLOSE get_observations
	DEALLOCATE get_observations
END
