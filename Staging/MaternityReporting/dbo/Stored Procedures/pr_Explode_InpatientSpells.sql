﻿/*
**************************************************************************************************************************
**	NAME:		pr_Explode_InpatientSpells						**
**	DESCRIPTION:	Runs the New Inpatient Spells 'explode' routine				**
**	AUTHOR:	Stephen Creek								**
**	VERSIONS:	1.0	Stephen Creek	07/11/2007	Initial version created		**
**			1.1	Stephen Creek	02/02/2008	Corrected use of len() function	**
**			1.2	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**												Noteno has changed from a 5 byte binary coded structured key
**												to a 25 byte (5 x 5 bytes) ASCII coded key.
**************************************************************************************************************************
*/
CREATE PROCEDURE [dbo].[pr_Explode_InpatientSpells]
	(@pointer int
	,@p_noteno VARCHAR(25))
AS
BEGIN
	DECLARE @NOTENO  VARCHAR(25)
	DECLARE @NVALUE  VARCHAR(60)
	DECLARE @l_spell_number INT
	DECLARE @SQLError INT

	DECLARE get_spell CURSOR FOR
	SELECT Noteno
	FROM tbl_Notes_Copy
	WHERE Code = 'ZEISZ' -- Inpatient spells
	AND Pointer = @pointer
	AND substring(Noteno,1,len('x'+@p_noteno+'x')-2) = @p_noteno
	ORDER BY Noteno

	SET @l_spell_number = 0
 
	OPEN get_spell
	FETCH NEXT FROM get_spell INTO @NOTENO

	WHILE @@fetch_status = 0
	BEGIN

		SET @l_spell_number = @l_spell_number + 1

		--v1.2 SET @NOTENO = SUBSTRING(@NOTENO,1,2)
		SET @NOTENO = SUBSTRING(@NOTENO,1,10)

		INSERT INTO [tbl_InpatientSpells] 
			(Patient_Pointer,
			Spell_Number,
			[Start_of_episode],
			[Current location],
			[Current Pregnancy],
			[End_of_episode],
			[Next adm/trans episode number],
			[Planned discharge date],
			[Planned discharge destination],
			[Planned discharge destination Hospital name],
			[Planned discharge time],
			[Alternative discharge address],
			[Alternative discharge address Address_1],
			[Alternative discharge address Address_2],
			[Alternative discharge address Address_3],
			[Alternative discharge address Address_4],
			[Alternative discharge address Postcode],
			[COPPISH Discharge Type],
			[COPPISH Discharge/Transfer To],
			[Date of discharge],
			[Discharge deletion allowed],
			[Discharge destination],
			[Discharge destination Hospital name],
			[Discharge event number],
			[Discharge method],
			[Discharge protocol flag],
			[Discharge source],
			[Discharge spell number],
			[Length of stay],
			[Time of discharge],
			[Discharge comments],
			[Discharge comments Text],
			[Follow up arrangements],
			[Follow up arrangements When],
			[Follow up arrangements Text],
			[Primary diagnosis],
			[Primary diagnosis Reason],
			[Secondary diagnosis 1],
			[Secondary diagnosis 1 Reason],
			[Secondary diagnosis 2],
			[Secondary diagnosis 2 Reason],
			[Secondary diagnosis 3],
			[Secondary diagnosis 3 Reason])
		VALUES
			(@pointer,
			@l_spell_number
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME06')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEISL')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMA40')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME07')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEISN')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZECD1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZECD5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZECD5'),'Hospital name')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZECD2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEDD8')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEDD8'),'Address_1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEDD8'),'Address_2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEDD8'),'Address_3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEDD8'),'Address_4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEDD8'),'Postcode')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEDH2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEDH3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEDD1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEDDA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEDD5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZEDD5'),'Hospital name')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEDD7')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEDD4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEDDF')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEDDS')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEDD6')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEDD3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEDD2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMDDC')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMDDC'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMDDF')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMDDF'),'When')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMDDF'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAD1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAD1'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAD2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAD2'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAD3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAD3'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAD4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAD4'),'Reason'))

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_Inpatientspells', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating tbl_InpatientSpells record '+cast(@l_spell_number as varchar)+' for patient '+cast(@pointer as varchar), -1 )
		END
		ELSE
		BEGIN
			EXEC dbo.pr_Explode_AdmissionTransferEpisode @pointer,@l_spell_number, @NOTENO
		END

		FETCH NEXT FROM get_spell INTO @NOTENO

	END

	CLOSE get_spell
	DEALLOCATE get_spell


END
