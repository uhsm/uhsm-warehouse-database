﻿
/*
****************************************************************************************************************
**	NAME:		pr_Explode_MedicalHistory					**
**	DESCRIPTION:	Runs the New Medical History 'explode' routine			**
**	AUTHOR:	Stephen Creek							**
**	VERSIONS:	1.0	Stephen Creek	10/11/2007	Initial version created	**
**				1.1	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**												Noteno has changed from a 5 byte binary coded structured key
**												to a 25 byte (5 x 5 bytes) ASCII coded key.
**			10/11/2009 Line Added By KD to get Consent to Blood Transfusion
****************************************************************************************************************
*/
CREATE  PROCEDURE [dbo].[pr_Explode_MedicalHistory]
AS
BEGIN
	DECLARE @pointer INT
	DECLARE @NOTENO  VARCHAR(25)
	DECLARE @SQLError INT

	DECLARE get_MedicalHistory CURSOR FOR
	SELECT Pointer,Noteno
 	FROM tbl_Notes_Copy
	WHERE Code = 'ZMSEP'

	TRUNCATE TABLE [tbl_MedicalHistory]

	OPEN get_MedicalHistory
	FETCH next from get_MedicalHistory into
	@pointer,@NOTENO

	DECLARE @RowCount INT
	SET @RowCount = 0

	WHILE @@fetch_status = 0
	BEGIN
		--v1.1 SET @NOTENO = SUBSTRING(@NOTENO,1,1)
		SET @NOTENO = SUBSTRING(@NOTENO,1,5)

		INSERT INTO [tbl_MedicalHistory] 
			(Patient_Pointer,
			[History of Genital Infections],
			[History of Genital Infections Disease 1],
			[History of Genital Infections Disease 2],
			[History of Genital Infections Disease 3],
			[History of Genital Infections Disease 4],
			[History of Genital Infections Details],
			[Previous pelvic/leg fracture],
			[Previous pelvic/leg fracture Text],
			[Previous gynae operations],
			[Previous gynae operations Cone Biopsy],
			[Previous gynae operations D&C],
			[Previous gynae operations Hysteroscopy],
			[Previous gynae operations Myomectomy],
			[Previous gynae operations Pelvic Floor Repair],
			[Previous gynae operations Reconstruction Uterus],
			[Previous gynae operations Other],
			[Previous infertility treatment],
			[Previous infertility treatment Clomiphene],
			[Previous infertility treatment Tamoxifen],
			[Previous infertility treatment Pergonyl],
			[Previous infertility treatment HCG],
			[Previous infertility treatment Insemination],
			[Previous infertility treatment IVF],
			[Previous infertility treatment GIFT],
			[Previous infertility treatment Other],
			[History of back problems],
			[History of back problems Text],
			[HIV status],
			[HIV status Most recent test],
			[History of diabetes],
			[History of diabetes Kind],
			[History of diabetes Text],
			[Previous heart surgery],
			[Previous heart surgery Text],
			[Established AIDS sufferer],
			[Any haemoglobinopathy],
			[Any haemoglobinopathy Condition],
			[Any haemoglobinopathy Condition 2],
			[Any haemoglobinopathy Text],
			[History of thrombosis],
			[History of thrombosis DVT],
			[History of thrombosis Pulmonary Embolism],
			[History of thrombosis Other],
			[General gynaecological prob.],
			[General gynaecological prob. Text],
			[Previous blood transfusion],
			[Previous blood transfusion Text],
			[Hist of essential hypertension],
			[Hist of essential hypertension Text],
			[History of asthma],
			[History of asthma Text],
			[Other medical problems],
			[Other medical problems Text],
			[Potential anaesthetic hazards],
			[Potential anaesthetic hazards Text],
			[Any psychological problems],
			[Any psychological problems Text],
			[Previous tubal surgery],
			[Previous tubal surgery Sterilisation Reversal],
			[Previous tubal surgery P.I.D Repair],
			[Previous tubal surgery Ectopic],
			[Previous tubal surgery Text],
			[History of allergies],
			[History of allergies Allergy 1],
			[History of allergies Allergy 2],
			[History of allergies Allergy 3],
			[History of allergies Text],
			[Previous anaesthetic problem],
			[Previous anaesthetic problem Text],
			[Other past surgical history],
			[Other past surgical history Text],
			[Previous SB/NND/Misc],
			[Previous SB/NND/Misc Text],
			[History of migraine],
			[History of migraine Text],
			[History of recurrent UTIs],
			[History of recurrent UTIs Text],
			[History of heart problems],
			[History of heart problems Level of Symptoms],
			[History of heart problems Details],
			[Any mental health problems],
			[Any mental health problems Text],
			[Any previous HIV test],
			[Any previous HIV test Date],
			[Hist of preg ind hypertension],
			[Hist of preg ind hypertension Text],
			[Ever had smear test],
			[Ever had smear test Last Test],
			[Ever had smear test Results],
			[Ever had smear test Text],
			[Previous infertility problems],
			[Previous infertility problems Text],
			[Kidney disease or surgery],
			[Kidney disease or surgery Kidney Transplant],
			[Kidney disease or surgery Ureteric Reimplantation],
			[Kidney disease or surgery Other],
			[History of clotting disorders],
			[History of clotting disorders Haemophilia carrier],
			[History of clotting disorders Von Willebrands],
			[History of clotting disorders Other],
			[History of clotting disorders Details],
			[History of epilepsy],
			[History of epilepsy Text],
			[History of thyroid disease],
			[History of thyroid disease Current Status],
			[History of thyroid disease Text],
			[Group B Strep Carrier],
			[Group B Strep Carrier Details],
			[History of anaemia],
			[History of anaemia Iron Deficiency],
			[History of anaemia Folate Deficiancy],
			[History of anaemia Pernicious Anaemia],
			[History of anaemia Text],
			[History of chest problems],
			[History of chest problems Text],
			[Previous non gynae operations],
			[Previous non gynae operations Appendicectomy],
			[Previous non gynae operations Laparotomy],
			[Previous non gynae operations Other],
			[Any Previous Colposcopy],
			[Any Previous Colposcopy Text],
			[Hepatitis or jaundice],
			[Hepatitis or jaundice Infective Hepatitis(A)],
			[Hepatitis or jaundice Serum Hepatitis(B)],
			[Hepatitis or jaundice Serum Hepatitis(C)],
			[Hepatitis or jaundice Cholestasis],
			[Hepatitis or jaundice Other],
			[Details of tuberculosis],
			[History of tuberculosis],
			[History of tuberculosis Active],
			[Consent For Blood Transfusion])
		VALUES 
			(@pointer
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF13')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF13'),'Disease 1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF13'),'Disease 2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF13'),'Disease 3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF13'),'Disease 4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF13'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF36')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF36'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF14')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF14'),'Cone Biopsy')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF14'),'D&C')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF14'),'Hysteroscopy')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF14'),'Myomectomy')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF14'),'Pelvic Floor Repair')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF14'),'Reconstruction Uterus')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF14'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF03')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF03'),'Clomiphene')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF03'),'Tamoxifen')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF03'),'Pergonyl')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF03'),'HCG')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF03'),'Insemination')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF03'),'IVF')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF03'),'GIFT')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF03'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF37')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF37'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFMQ')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMFMQ'),'Most recent test')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF15')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF15'),'Kind')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF15'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF59')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF59'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFMR')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX15')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX15'),'Condition')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX15'),'Condition 2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX15'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF17')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF17'),'DVT')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF17'),'Pulmonary Embolism')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF17'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF07')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF07'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF18')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF18'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF08')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF08'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF19')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF19'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX19')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX19'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF80')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF80'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFM5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMFM5'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF70')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF70'),'Sterilisation Reversal')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF70'),'P.I.D Repair')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF70'),'Ectopic')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF70'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF82')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF82'),'Allergy 1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF82'),'Allergy 2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF82'),'Allergy 3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF82'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFM6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMFM6'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF71')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF71'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPOk')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOk'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF62')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF62'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF51')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF51'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFH3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMFH3'),'Level of Symptoms')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMFH3'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF63')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF63'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMBbA')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMBbA'),'Date')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFH4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMFH4'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF20')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF20'),'Last Test')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF20'),'Results')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF20'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF53')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF53'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF64')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF64'),'Kidney Transplant')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF64'),'Ureteric Reimplantation')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF64'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF75')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF75'),'Haemophilia carrier')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF75'),'Von Willebrands')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF75'),'Other')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF75'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF32')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF32'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF65')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF65'),'Current Status')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF65'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF00')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF00'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF11')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF11'),'Iron Deficiency')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF11'),'Folate Deficiancy')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF11'),'Pernicious Anaemia')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF11'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFC2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMFC2'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF67')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF67'),'Appendicectomy')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF67'),'Laparotomy')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF67'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFC3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMFC3'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF12')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF12'),'Infective Hepatitis(A)')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF12'),'Serum Hepatitis(B)')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF12'),'Serum Hepatitis(C)')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF12'),'Cholestasis')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF12'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmrx9')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF10')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF10'),'Active')--) bracket removed KD 10/11/2009
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMf15')) -- KD Line Added 10/11/2009
		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_MedicalHistory', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating medical history record  for patient ' + cast(@pointer as varchar), -1 )
		END
		ELSE
			SET @RowCount = @RowCount + 1

		FETCH next from get_MedicalHistory into
		@pointer,@NOTENO
	END

	CLOSE get_MedicalHistory
	DEALLOCATE get_MedicalHistory

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	VALUES ( getdate(), 'pr_Explode_MedicalHistory', 'tbl_MedicalHistory records created', @RowCount )

END

