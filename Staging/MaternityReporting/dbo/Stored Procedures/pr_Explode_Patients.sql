﻿/*
******************************************************************************************************************************************************************
**	NAME:		pr_Explode_Patients											**
**	DESCRIPTION:	Runs the Patient 'explode' routines									**
**	AUTHOR:	Stephen Creek												**
**	VERSIONS:	1.0	Stephen Creek	02/11/2007	Initial version created						**
**			1.1	Stephen Creek	08/01/2007	Small change to cater for Extra field having too few delimeters 	**
**			1.2	Stephen Creek	31/01/2007	Added trap for Null values in Extra field				**
**			1.3	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**												Noteno has changed from a 5 byte binary coded structured key
**												to a 25 byte (5 x 5 bytes) ASCII coded key.
******************************************************************************************************************************************************************
*/
CREATE PROCEDURE [dbo].[pr_Explode_Patients]
AS
BEGIN
	TRUNCATE TABLE [tbl_Patients]

	DECLARE @RowCount INT
	DECLARE @SQLError INT

	INSERT INTO tbl_Patients
		(Patient_Pointer, 
		Surname, 
		Forename, 
		Forename2, 
		Title, 
		Sex, 
		BirthDate, 
		Address1, 
		Address2, 
		Address3, 
		Address4, 
		Postcode, 
		Phone, 
		Number1, 
		Number2, 
		Number3,
		Status)
	SELECT     
		Pointer, 
		rtrim(Surname), 
		rtrim(Forename), 
		rtrim(Forename2), 
		rtrim(Title), 
		rtrim(Sex), 
		dbo.func_Convert_YYYYMMDD_To_DateTime(Birthdate) AS BirthDate, 
		rtrim(Address1), 
		rtrim(Address2), 
		rtrim(Address3), 
		rtrim(Address4), 
		rtrim(Postcode), 
		rtrim(Phone), 
		rtrim(Number1), 
		rtrim(Number2), 
		rtrim(Number3),
		rtrim(Status)
	FROM         dbo.tbl_Register_Copy
	WHERE     (Regtype = 'P') 

	SET @RowCount = @@rowcount

	SET @SQLError = @@error
	IF @SQLError > 0
	BEGIN
		INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
		VALUES ( getdate(), 'pr_Explode_Patients', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating patient table ', -1 )
		GOTO SkipAll
	END

	DECLARE @pointer INT
	DECLARE @EXTRA  VARCHAR(255)
	DECLARE @MSTAT VARCHAR(50)
	DECLARE @GP VARCHAR(50)
	DECLARE @PHONE2 VARCHAR(50)
	DECLARE @CONS VARCHAR(50)
	DECLARE @NOKNAME VARCHAR(50)
	DECLARE @NOKRELATION VARCHAR(50)
	DECLARE @NOKCONTACT VARCHAR(50)
	DECLARE @SHA VARCHAR(50)
	DECLARE @UNK VARCHAR(50)
	DECLARE @DOD VARCHAR(50)

	DECLARE get_Extra CURSOR FOR
--	SELECT Pointer, left(RIGHT(Extra,LEN(Extra)-1)+'~~~~~~~~~~~',255) -- v1.1 
	SELECT Pointer, left(isnull(Extra,'')+'~~~~~~~~~~~',255) -- v1.2
 	FROM tbl_Register_Copy
	WHERE     (Regtype = 'P') 

	OPEN get_Extra
	FETCH NEXT FROM get_Extra INTO
	@pointer, @EXTRA
	
	WHILE @@fetch_status = 0
	BEGIN

		-- Remove leading ~ - v1.2
		SET @EXTRA = RIGHT(@EXTRA,LEN(@EXTRA)-1)

		-- Marital Status
		IF LEFT(@EXTRA,1) = '~' SET @MSTAT = NULL
		ELSE SET @MSTAT = RTRIM(LEFT(@EXTRA, CHARINDEX('~',@EXTRA)-1))
		SET @EXTRA = RIGHT(@EXTRA,LEN(@EXTRA)-CHARINDEX('~',@EXTRA))

		-- GP
		IF LEFT(@EXTRA,1) = '~' SET @GP = NULL
	 	SET @GP = RTRIM(LEFT(@EXTRA, CHARINDEX('~',@EXTRA)-1))
		SET @EXTRA = RIGHT(@EXTRA,LEN(@EXTRA)-CHARINDEX('~',@EXTRA))

		-- Phone2
		IF LEFT(@EXTRA,1) = '~' SET @PHONE2 = NULL
	 	SET @PHONE2 = RTRIM(LEFT(@EXTRA, CHARINDEX('~',@EXTRA)-1))
		SET @EXTRA = RIGHT(@EXTRA,LEN(@EXTRA)-CHARINDEX('~',@EXTRA))

		-- Consultant
		IF LEFT(@EXTRA,1) = '~' SET @CONS = NULL
	 	SET @CONS = RTRIM(LEFT(@EXTRA, CHARINDEX('~',@EXTRA)-1))
		SET @EXTRA = RIGHT(@EXTRA,LEN(@EXTRA)-CHARINDEX('~',@EXTRA))

		-- NOK Name
		IF LEFT(@EXTRA,1) = '~' SET @NOKNAME = NULL
	 	SET @NOKNAME = RTRIM(LEFT(@EXTRA, CHARINDEX('~',@EXTRA)-1))
		SET @EXTRA = RIGHT(@EXTRA,LEN(@EXTRA)-CHARINDEX('~',@EXTRA))

		-- NOK Relation
		IF LEFT(@EXTRA,1) = '~' SET @NOKRELATION = NULL
	 	SET @NOKRELATION = RTRIM(LEFT(@EXTRA, CHARINDEX('~',@EXTRA)-1))
		SET @EXTRA = RIGHT(@EXTRA,LEN(@EXTRA)-CHARINDEX('~',@EXTRA))

		-- NOK Contact
		IF LEFT(@EXTRA,1) = '~' SET @NOKCONTACT = NULL
	 	SET @NOKCONTACT = RTRIM(LEFT(@EXTRA, CHARINDEX('~',@EXTRA)-1))
		SET @EXTRA = RIGHT(@EXTRA,LEN(@EXTRA)-CHARINDEX('~',@EXTRA))

		-- SHA
		IF LEFT(@EXTRA,1) = '~' SET @SHA = NULL
	 	SET @SHA = RTRIM(LEFT(@EXTRA, CHARINDEX('~',@EXTRA)-1))
		SET @EXTRA = RIGHT(@EXTRA,LEN(@EXTRA)-CHARINDEX('~',@EXTRA))

		-- Unknown
		IF LEFT(@EXTRA,1) = '~' SET @UNK = NULL
	 	SET @UNK = RTRIM(LEFT(@EXTRA, CHARINDEX('~',@EXTRA)-1))
		SET @EXTRA = RIGHT(@EXTRA,LEN(@EXTRA)-CHARINDEX('~',@EXTRA))

		-- Date of Death
		IF LEFT(@EXTRA,1) = '~' SET @DOD = NULL
	 	SET @DOD = RTRIM(LEFT(@EXTRA, CHARINDEX('~',@EXTRA)-1))
		SET @EXTRA = RIGHT(@EXTRA,LEN(@EXTRA)-CHARINDEX('~',@EXTRA))

		UPDATE tbl_Patients
		SET
			MaritalStatus = CASE WHEN @MSTAT='' THEN NULL ELSE @MSTAT END,
			GP_Pointer = CASE WHEN @GP='' THEN NULL ELSE @GP END,
			Phone2 = CASE WHEN @PHONE2='' THEN NULL ELSE @PHONE2 END,
			Consultant_Pointer = CASE WHEN @CONS='' THEN NULL ELSE @CONS END,
			NOK_Name = CASE WHEN @NOKNAME='' THEN NULL ELSE @NOKNAME END,
			NOK_Relationship = CASE WHEN @NOKRELATION='' THEN NULL ELSE @NOKRELATION END,
			NOK_Contact = CASE WHEN @NOKCONTACT='' THEN NULL ELSE @NOKCONTACT END,
			SHA_Code = CASE WHEN @SHA='' THEN NULL ELSE @SHA END,
			Unknown1 = CASE WHEN @UNK='' THEN NULL ELSE @UNK END,
			DateOfDeath = CASE WHEN @DOD='' THEN NULL ELSE @DOD END
		WHERE
			Patient_Pointer = @pointer

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_Patients', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered extracting Extra information for patient '+cast(@pointer as varchar), -1 )
		END

		FETCH NEXT FROM get_Extra INTO
		@pointer, @EXTRA
	END

	CLOSE get_Extra
	DEALLOCATE get_Extra

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	VALUES ( getdate(), 'pr_Explode_Patients', 'tbl_Patients records created', @RowCount )

	EXEC pr_Explode_FamilyHistory
	EXEC pr_Explode_MedicalHistory
	EXEC pr_Explode_SocialHistory

	-- Now get Personal Information (ZERPI)

	TRUNCATE TABLE [tbl_PersonalInformation]

	DECLARE @NOTENO  VARCHAR(25)
	DECLARE @NVALUE  VARCHAR(60)

	DECLARE get_PersonalInformation CURSOR FOR
	SELECT Pointer,Noteno
 	FROM tbl_Notes_Copy
	WHERE Code = 'ZERPI'

	OPEN get_PersonalInformation
	FETCH NEXT FROM get_PersonalInformation INTO
	@pointer,@NOTENO

	SET @RowCount = 0

	WHILE @@fetch_status = 0
	BEGIN
		--v1.3 SET @NOTENO = SUBSTRING(@NOTENO,1,1)
		SET @NOTENO = SUBSTRING(@NOTENO,1,5)

		INSERT INTO [tbl_PersonalInformation] 
			(Patient_Pointer,
			[Mothers Place of Birth],
			[Age at menarche],
			[Language ability],
			[Language ability Native_tongue],
			[Language ability Interpreter needed],
			[Age at menopause],
			[Internal number],
			[Personal hist of thalassaemia],
			[Personal hist of thalassaemia Text],
			[Usual weight in Kg],
			[Personal hist of sickle cell],
			[Personal hist of sickle cell Text],
			[Diet],
			[Diet Text],
			[Patients preferred forename],
			[Other Surnames used previously],
			[Sight problems],
			[Sight problems Text],
			[Previous surname 1],
			[Previous surname 1 Forename],
			[Previous surname 1 Number],
			[Hearing ability],
			[Hearing ability Able to lip read],
			[Hearing ability Text],
			[Previous surname 2],
			[Previous surname 2 Forename],
			[Previous surname 2 Number],
			[Aliases],
			[Speech problems],
			[Speech problems Text],
			[Any communication problem],
			[Previous surname 3],
			[Previous surname 3 Forename],
			[Previous surname 3 Number],
			[Adult height in centimetres],
			[Father`s Place of Birth],
			[Mobility],
			[Mobility Text],
			[Alternative patient number 1],
			[Alternative patient number 2],
			[Alternative patient number 3],
			[Any other disability],
			[Any other disability Text],
			[Surname at marriage],
			[BTS ref number],
			[Usual Adult Body Mass Index])
		VALUES
			(@pointer
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMI10')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP80')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX24')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX24'),'Native_tongue')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX24'),'Interpreter needed')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP81')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEpt5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFMA')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMFMA'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF16')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFMB')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMFMB'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFMC')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMFMC'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMIN1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMIN2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF92')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF92'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMIN3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMIN3'),'Forename')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMIN3'),'Number')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF93')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF93'),'Able to lip read')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF93'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMIN4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMIN4'),'Forename')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMIN4'),'Number')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERAI')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF94')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF94'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX60')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMIN5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMIN5'),'Forename')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMIN5'),'Number')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP13')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMI11')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF95')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF95'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMIN6')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMIN7')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMIN8')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF87')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF87'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMIN9')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMMTL')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF89'))

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_Patients', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating tbl_PersonalInformation record for patient '+cast(@pointer as varchar), -1 )
		END
		ELSE
			SET @RowCount = @RowCount + 1

    		FETCH next from get_PersonalInformation into
		@pointer,@NOTENO
	END
	CLOSE get_PersonalInformation
	DEALLOCATE get_PersonalInformation

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	VALUES ( getdate(), 'pr_Explode_Patients', 'tbl_PersonalInformation records created', @RowCount )

	-- Now get Obstetric Summary (ZEROB)

	TRUNCATE TABLE [tbl_ObstetricSummary]

	DECLARE get_ObstetricSummary CURSOR FOR
	SELECT Pointer,Noteno
 	FROM tbl_Notes_Copy
	WHERE Code = 'ZEROB'

	OPEN get_ObstetricSummary
	FETCH NEXT FROM get_ObstetricSummary INTO
	@pointer,@NOTENO

	SET @RowCount = 0

	WHILE @@fetch_status = 0
	BEGIN
		--v1.3 SET @NOTENO = SUBSTRING(@NOTENO,1,1)
		SET @NOTENO = SUBSTRING(@NOTENO,1,5)

		INSERT INTO [tbl_ObstetricSummary] 
			(Patient_Pointer,
			[Number of still births],
			[Pregs ending before 12 wks],
			[Pregs ending before 12 wks Miscarriages],
			[Pregs ending before 12 wks Induced abortions],
			[Pregs ending before 12 wks Terminations],
			[Pregs ending before 12 wks Ectopics],
			[Pregs ending before 12 wks Hydatidiform moles],
			[Pregs ending between 12 - 24 w],
			[Pregs ending between 12 - 24 w Miscarriages],
			[Pregs ending between 12 - 24 w Induced abortions],
			[Pregs ending between 12 - 24 w Terminations],
			[Pregs ending between 12 - 24 w Ectopics],
			[Pregs ending between 12 - 24 w Hysterotomys],
			[Gravida],
			[No. of Past Pregnancies],
			[Parity at Booking],
			[No. of registerable TOP],
			[Female contraceptive status],
			[Currently pregnant],
			[Currently pregnant EDD],
			[No. of Registerable Pregnancies],
			[No. of Non-registerable Pregs],
			[No. of registerable livebirths],
			[Number of live births],
			[No. of registerable stillbirths],
			[No. of children living now],
			[No. of non-registerable births],
			[Number_of_previous_births],
			[No. of children who have died],
			[Number_of_previous_caesars],
			[No. of neonatal deaths],
			[No. of pre-term livebirths],
			[Parity after Delivery],
			[Number of previous pregnancies])
		VALUES
			(@pointer
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDHIS')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMO30')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMO30'),'Miscarriages')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMO30'),'Induced abortions')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMO30'),'Terminations')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMO30'),'Ectopics')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMO30'),'Hydatidiform moles')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMO31')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMO31'),'Miscarriages')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMO31'),'Induced abortions')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMO31'),'Terminations')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMO31'),'Ectopics')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMO31'),'Hysterotomys')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMO21')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMO22')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmry2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmry3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDRPE')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDPHP')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDPHP'),'EDD')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMO23')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMO24')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMO25')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDHI0')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMO26')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMO27')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMO38')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME00')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMO28')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'XME01')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMO29')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOL1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMO09')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDHIP'))

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_Patients', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating tbl_ObstetricSummary record for patient '+cast(@pointer as varchar), -1 )
		END
		ELSE
			SET @RowCount = @RowCount + 1

    		FETCH next from get_ObstetricSummary into
		@pointer,@NOTENO
	END
	CLOSE get_ObstetricSummary
	DEALLOCATE get_ObstetricSummary

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	VALUES ( getdate(), 'pr_Explode_Patients', 'tbl_ObstetricSummary records created', @RowCount )

	-- Now get Obstetric Problem Summary (ZMp00)

	TRUNCATE TABLE [tbl_ObstetricProblemSummary]

	DECLARE get_ObstetricProblemSummary CURSOR FOR
	SELECT Pointer,Noteno
 	FROM tbl_Notes_Copy
	WHERE Code = 'ZMp00'

	OPEN get_ObstetricProblemSummary
	FETCH NEXT FROM get_ObstetricProblemSummary INTO
	@pointer,@NOTENO

	SET @RowCount = 0

	WHILE @@fetch_status = 0
	BEGIN
		--v1.3 SET @NOTENO = SUBSTRING(@NOTENO,1,1)
		SET @NOTENO = SUBSTRING(@NOTENO,1,5)

		INSERT INTO [tbl_ObstetricProblemSummary] 
			(Patient_Pointer,
			[Previous deliv. over 42 weeks],
			[Previous deliv. over 42 weeks Confidential],
			[Previous deliv. over 42 weeks Management],
			[Previous deliv. over 42 weeks History],
			[Previous child with cleft lip],
			[Previous child with cleft lip Confidential],
			[Previous child with cleft lip Management],
			[Previous child with cleft lip History],
			[Previous stillbirth],
			[Previous stillbirth Confidential],
			[Previous stillbirth Management],
			[Previous stillbirth History],
			[Previous child absent digits],
			[Previous child absent digits Confidential],
			[Previous child absent digits Management],
			[Previous child absent digits History],
			[Previous forceps delivery],
			[Previous forceps delivery Confidential],
			[Previous forceps delivery Management],
			[Previous forceps delivery History],
			[Prev. child with cong abnorm],
			[Prev. child with cong abnorm Confidential],
			[Prev. child with cong abnorm Management],
			[Prev. child with cong abnorm History],
			[Previous ventouse delivery],
			[Previous ventouse delivery Confidential],
			[Previous ventouse delivery Management],
			[Previous ventouse delivery History],
			[Prev. neonatal medical problem],
			[Prev. neonatal medical problem Confidential],
			[Prev. neonatal medical problem Management],
			[Prev. neonatal medical problem History],
			[Previous vaginal breech],
			[Previous vaginal breech Confidential],
			[Previous vaginal breech Management],
			[Previous vaginal breech History],
			[Previous cot death],
			[Previous cot death Confidential],
			[Previous cot death Management],
			[Previous cot death History],
			[Previous elective section],
			[Previous elective section Confidential],
			[Previous elective section Management],
			[Previous elective section History],
			[Maternal pyrexia],
			[Maternal pyrexia Confidential],
			[Maternal pyrexia Management],
			[Maternal pyrexia History],
			[Previous child death],
			[Previous child death Confidential],
			[Previous child death Management],
			[Previous child death History],
			[Previous emergency section],
			[Previous emergency section Confidential],
			[Previous emergency section Management],
			[Previous emergency section History],
			[Pregnancy Induced Hypertension],
			[Pregnancy Induced Hypertension Confidential],
			[Pregnancy Induced Hypertension Management],
			[Pregnancy Induced Hypertension History],
			[Previous child adopted],
			[Previous child adopted Confidential],
			[Previous child adopted Management],
			[Previous child adopted History],
			[Previous delivery < 2500g],
			[Previous delivery < 2500g Confidential],
			[Previous delivery < 2500g Management],
			[Previous delivery < 2500g History],
			[Ruptured uterus],
			[Ruptured uterus Confidential],
			[Ruptured uterus Management],
			[Ruptured uterus History],
			[Spontaneous Miscarriage],
			[Spontaneous Miscarriage Confidential],
			[Spontaneous Miscarriage Management],
			[Spontaneous Miscarriage History],
			[Previous child fostered],
			[Previous child fostered Confidential],
			[Previous child fostered Management],
			[Previous child fostered History],
			[Previous delivery > 4500g],
			[Previous delivery > 4500g Confidential],
			[Previous delivery > 4500g Management],
			[Previous delivery > 4500g History],
			[Intrapartum problems],
			[Intrapartum problems Confidential],
			[Intrapartum problems Management],
			[Intrapartum problems History],
			[Induced abortion],
			[Induced abortion Confidential],
			[Induced abortion Management],
			[Induced abortion History],
			[Prev. child with other parent],
			[Prev. child with other parent Confidential],
			[Prev. child with other parent Management],
			[Prev. child with other parent History],
			[Previous Downs Child],
			[Previous Downs Child Confidential],
			[Previous Downs Child Management],
			[Previous Downs Child History],
			[Retained placenta],
			[Retained placenta Confidential],
			[Retained placenta Management],
			[Retained placenta History],
			[Termination],
			[Termination Confidential],
			[Termination Management],
			[Termination History],
			[Previous child with NTD],
			[Previous child with NTD Confidential],
			[Previous child with NTD Management],
			[Previous child with NTD History],
			[Early PPH],
			[Early PPH Confidential],
			[Early PPH Management],
			[Early PPH History],
			[Ectopic pregnancy],
			[Ectopic pregnancy Confidential],
			[Ectopic pregnancy Management],
			[Ectopic pregnancy History],
			[Perineal or Episiotomy problem],
			[Perineal or Episiotomy problem Confidential],
			[Perineal or Episiotomy problem Management],
			[Perineal or Episiotomy problem History],
			[Hydatiform mole],
			[Hydatiform mole Confidential],
			[Hydatiform mole Management],
			[Hydatiform mole History],
			[Postpartum complications],
			[Postpartum complications Confidential],
			[Postpartum complications Management],
			[Postpartum complications History],
			[Induction],
			[Induction Confidential],
			[Induction Management],
			[Induction Text],
			[Labour under 2 hours],
			[Labour under 2 hours Confidential],
			[Labour under 2 hours Management],
			[Labour under 2 hours History],
			[Labour over 24 hrs],
			[Labour over 24 hrs Confidential],
			[Labour over 24 hrs Management],
			[Labour over 24 hrs History],
			[Previous child not with parent],
			[Previous child not with parent Confidential],
			[Previous child not with parent Management],
			[Previous child not with parent History],
			[Prev. delivery under 37 weeks],
			[Prev. delivery under 37 weeks Confidential],
			[Prev. delivery under 37 weeks Management],
			[Prev. delivery under 37 weeks History],
			[Prev. child with cleft palate],
			[Prev. child with cleft palate Confidential],
			[Prev. child with cleft palate Management],
			[Prev. child with cleft palate History])
		VALUES
			(@pointer
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp72')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp72'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp72'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp72'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp61')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp61'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp61'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp61'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp50')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp50'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp50'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp50'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp62')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp62'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp62'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp62'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp51')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp51'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp51'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp51'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp63')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp63'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp63'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp63'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp52')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp52'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp52'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp52'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp64')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp64'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp64'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp64'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp53')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp53'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp53'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp53'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp65')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp65'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp65'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp65'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp54')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp54'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp54'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp54'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp10')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp10'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp10'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp10'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp66')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp66'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp66'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp66'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp55')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp55'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp55'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp55'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp11')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp11'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp11'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp11'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp67')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp67'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp67'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp67'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp56')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp56'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp56'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp56'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp12')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp12'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp12'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp12'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp01')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp01'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp01'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp01'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp68')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp68'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp68'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp68'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp57')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp57'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp57'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp57'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp13')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp13'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp13'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp13'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp02')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp02'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp02'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp02'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp69')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp69'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp69'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp69'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp58')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp58'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp58'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp58'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp14')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp14'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp14'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp14'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp03')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp03'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp03'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp03'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp59')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp59'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp59'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp59'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp15')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp15'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp15'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp15'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp04')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp04'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp04'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp04'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp16')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp16'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp16'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp16'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp05')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp05'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp05'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp05'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp17')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp17'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp17'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp17'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp06')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp06'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp06'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp06'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp08')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp08'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp08'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp08'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp09')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp09'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp09'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp09'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp70')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp70'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp70'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp70'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp71')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp71'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp71'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp71'),'History')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zpp60')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp60'),'Confidential')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp60'),'Management')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zpp60'),'History'))

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_Patients', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating tbl_ObstetricProblemSummary record for patient '+cast(@pointer as varchar), -1 )
		END
		ELSE
			SET @RowCount = @RowCount + 1

    		FETCH next from get_ObstetricProblemSummary into
		@pointer,@NOTENO
	END
	CLOSE get_ObstetricProblemSummary
	DEALLOCATE get_ObstetricProblemSummary

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	VALUES ( getdate(), 'pr_Explode_Patients', 'tbl_ObstetricProblemSummary records created', @RowCount )

	-- Now get Inpatient Records (ZEIRZ)

	TRUNCATE TABLE [tbl_InpatientSpells]
	TRUNCATE TABLE [tbl_AdmissionTransferEpisodes]

	DECLARE get_InpatientRecords CURSOR FOR
	--v1.3 SELECT Pointer,substring(Noteno,1,1)
	SELECT Pointer,substring(Noteno,1,5)
 	FROM tbl_Notes_Copy
	WHERE Code = 'ZEIRZ' -- Inpatient Records
	ORDER BY Pointer, Noteno

	OPEN get_InpatientRecords
	FETCH NEXT FROM get_InpatientRecords INTO
	@pointer,@NOTENO

	WHILE @@fetch_status = 0
	BEGIN
		EXEC dbo.pr_Explode_InpatientSpells @pointer, @NOTENO

    		FETCH next from get_InpatientRecords into
		@pointer,@NOTENO
	END
	CLOSE get_InpatientRecords
	DEALLOCATE get_InpatientRecords

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	SELECT getdate(), 'pr_Explode_Patients', 'tbl_InpatientSpells records created', count(*) from tbl_InpatientSpells

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	SELECT getdate(), 'pr_Explode_Patients', 'tbl_AdmissionTransferEpisode records created', count(*) from tbl_AdmissionTransferEpisodes

SkipAll:

END
