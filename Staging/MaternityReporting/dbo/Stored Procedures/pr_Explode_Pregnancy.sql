﻿
/*
**********************************************************************************************************************************************
**	NAME:		pr_Explode_Pregnancy									**
**	DESCRIPTION:	Runs the New Pregnacy 'explode' routine							**
**	AUTHOR:	Stephen Creek										**
**	VERSIONS:	1.0	Stephen Creek	05/11/2007	Initial version created				**
**			1.1	Stephen Creek	07/02/2008	Added null value check for reason for termination 	**
**								Added blood spot screening tables		**
**			1.2	Stephen Creek	11/02/2208	Added pregnancy identifier			**
**			1.3	Stephen Creek	25/02/2008	Added DownsNeuralTubeScreening		**
**			1.4	Stephen Creek	27/02/2008	Added Cervicogram				**
**			1.5	Stephen Creek	28/02/2008	Added Summary of Anaesthesia			**
**			1.6	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**												Noteno has changed from a 5 byte binary coded structured key
**												to a 25 byte (5 x 5 bytes) ASCII coded key.
**********************************************************************************************************************************************
*/
CREATE    PROCEDURE [dbo].[pr_Explode_Pregnancy] (@p_pointer INT)
AS 
BEGIN
	SET LANGUAGE 'British'
	
	DECLARE @pointer INT
	DECLARE @NOTENO  VARCHAR(25)
	DECLARE @l_pregnancy_number VARCHAR(30)
	DECLARE @l_pregnancy_id INT
	DECLARE @SQLError INT
	DECLARE @RowCount INT

	SET @RowCount = 0

	IF @p_pointer = 0 -- 0 = all patients
	BEGIN
	 	TRUNCATE TABLE [tbl_Delivery]
	 	TRUNCATE TABLE [tbl_Pregnancy]
		TRUNCATE TABLE [tbl_ClinicalReviews]
		TRUNCATE TABLE [tbl_ReviewInvestigations]
		TRUNCATE TABLE [tbl_ReviewExaminations]
		TRUNCATE TABLE [tbl_ReviewTreatments]
		TRUNCATE TABLE [tbl_FetalObservations]
		TRUNCATE TABLE [tbl_Referrals]
		TRUNCATE TABLE [tbl_SocialServicesReferral]
		TRUNCATE TABLE [tbl_PregnancyCarePlans]
	 	TRUNCATE TABLE [tbl_Baby]
	 	TRUNCATE TABLE [tbl_DeathOfBaby]
	 	TRUNCATE TABLE [tbl_BloodSpotScreening] -- v1.1
	 	TRUNCATE TABLE [tbl_DownsNeuralTubeScreening] -- v1.3
	 	TRUNCATE TABLE [tbl_DownsNeuralTubeScreening_Fetus] -- v1.3
	 	TRUNCATE TABLE [tbl_Cervicogram] -- v1.4
		TRUNCATE TABLE [tbl_SummaryOfAnaesthesia] -- v1.5

		DECLARE get_pregnancy CURSOR FOR
		SELECT Pointer,Noteno
	 	FROM tbl_Notes_Copy
		WHERE Code = 'ZEP0M' -- Pregnancy Record
		ORDER BY Pointer, Noteno
	END
	Else
	BEGIN
		DECLARE get_pregnancy CURSOR FOR
		SELECT Pointer,Noteno
	 	FROM tbl_Notes_Copy
		WHERE Code = 'ZEP0M' -- Pregnancy Record
		AND Pointer = @p_pointer
		ORDER BY Pointer, Noteno

	 	DELETE FROM [tbl_Delivery] WHERE Patient_Pointer = @p_pointer
	 	DELETE FROM [tbl_Pregnancy] WHERE Patient_Pointer = @p_pointer
		DELETE FROM  [tbl_ClinicalReviews] WHERE Patient_Pointer = @p_pointer
		DELETE FROM  [tbl_ReviewInvestigations] WHERE Patient_Pointer = @p_pointer
		DELETE FROM  [tbl_ReviewExaminations] WHERE Patient_Pointer = @p_pointer
		DELETE FROM  [tbl_ReviewTreatments] WHERE Patient_Pointer = @p_pointer
		DELETE FROM  [tbl_FetalObservations] WHERE Patient_Pointer = @p_pointer
		DELETE FROM  [tbl_Referrals] WHERE Patient_Pointer = @p_pointer
		DELETE FROM  [tbl_SocialServicesReferral] WHERE Patient_Pointer = @p_pointer
		DELETE FROM  [tbl_PregnancyCarePlans] WHERE Patient_Pointer = @p_pointer
	 	DELETE FROM  [tbl_Baby] WHERE Patient_Pointer = @p_pointer
	 	DELETE FROM  [tbl_DeathOfBaby] WHERE Patient_Pointer = @p_pointer
	 	DELETE FROM  [tbl_BloodSpotScreening] WHERE Patient_Pointer = @p_pointer -- v1.1
	 	DELETE FROM [tbl_DownsNeuralTubeScreening] WHERE Patient_Pointer = @p_pointer -- v1.3
	 	DELETE FROM [tbl_DownsNeuralTubeScreening_Fetus] WHERE Patient_Pointer = @p_pointer -- v1.3
	 	DELETE FROM [tbl_Cervicogram] WHERE Patient_Pointer = @p_pointer -- v1.4
	 	DELETE FROM [tbl_SummaryOfAnaesthesia] WHERE Patient_Pointer = @p_pointer -- v1.5

	END

	OPEN get_pregnancy
	FETCH NEXT FROM get_pregnancy INTO
	@pointer,@NOTENO

	SELECT @l_pregnancy_id = isnull(max(Pregnancy_ID),0) from tbl_Pregnancy

	WHILE @@fetch_status = 0
	BEGIN
		--PRINT	OBJECT_NAME(@@PROCID) + ' get_pregnancy '
		--	+	'Patient_Pointer=' + ISNULL(CAST(@pointer AS VARCHAR(10)),'NULL')
		--	+	', Noteno=' + ISNULL(@NOTENO,'NULL')

		SET @l_pregnancy_id = @l_pregnancy_id+1
		--v1.3 SET @NOTENO = SUBSTRING(@NOTENO,1,1)
		SET @NOTENO = SUBSTRING(@NOTENO,1,5)
		SET @l_pregnancy_number = rtrim(dbo.func_Extract_Value(@pointer,@NOTENO,'ZMB40')) -- Pregnancy Number
		IF isnull(@l_pregnancy_number,'') = ''
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_Pregnancy', 'Missing [Pregnancy number] encountered creating pregnancy record '+cast(@l_pregnancy_id as varchar)+' for patient ' + cast(@pointer as varchar), -1 )
		END

		INSERT INTO [tbl_Pregnancy] 
				(Patient_Pointer,
				Pregnancy_ID,
				[Pregnancy number],
				[Pregnancy status],
				[Pregnancy status User],
				[Pregnancy status Date],
				[Pregnancy status Time],
				[Pregnancy status Reason],
				[Any other special requests],
				[Any other special requests Text],
				[ARM],
				[ARM Text],
				[Companions at delivery],
				[Companions at delivery Text],
				[Confidential pregnancy],
				[Created in],
				[Date birth plan completed],
				[Date pregnancy ended],
				[Date pregnancy record closed],
				[Discussed with midwife],
				[Episiotomy],
--20
				[Episiotomy Text],
				[Feeding intention],
				[Fetal monitoring],
				[Gestation at admin. closure],
				[Gestation at adverse outcome],
				[Involvement of students],
				[Involvement of students Text],
				[Maternal position for delivery],
				[Maternal position for delivery Text],
				[Mobility],
				[Mobility Text],
				[Name of midwife],
				[Name of midwife Grade],
				[NonPharmacological Pain Relief],
				[NonPharmacological Pain Relief Acupuncture],
				[NonPharmacological Pain Relief Aromatherapy],
				[NonPharmacological Pain Relief Hot Packs],
				[NonPharmacological Pain Relief Relaxation Methods],
				[NonPharmacological Pain Relief TENS],
				[NonPharmacological Pain Relief Water],
--40
				[NonPharmacological Pain Relief Text],
				[Outcome of this pregnancy],
				[Oxytocics],
				[Oxytocics Drug 1],
				[Oxytocics Drug 2],
				[Oxytocics Drug 3],
				[Oxytocics Text],
				[Pharmacological Pain Relief],
				[Pharmacological Pain Relief Epidural],
				[Pharmacological Pain Relief Self_admin._inhalation],
				[Pharmacological Pain Relief Local_Infiltration],
				[Pharmacological Pain Relief Non-opioid e.g. paracetamol],
				[Pharmacological Pain Relief Opioid analgesia eg. pethidine],
				[Pharmacological Pain Relief Text],
				[Place pregnancy ended],
				[Place pregnancy ended Text],
				[Previous Parity],
				[Reason for closing preg record],
				[Reason for closing preg record Text],
				[Reason for end of pregnancy],
--60
				[Reason for end of pregnancy Text],
				[Skin to skin contact],
				[Specific closure date known],
				[Type of pregnancy record],
				[Vitamin K (Konakion)],
				[Vitamin K (Konakion) Route & Dose],
				[Year pregnancy ended],
				[FH of congenital anomalies],
				[FH of congenital anomalies Downs],
				[FH of congenital anomalies Neural Tube Defect],
				[FH of congenital anomalies Other],
				[FH of hearing problems],
				[FH of hearing problems Details],
				[FH of hip problems],
				[FH of hip problems Details],
				[FH of inherited disease],
				[FH of inherited disease Cystic Fibrosis],
				[FH of inherited disease Muscular Dystrophy],
				[FH of inherited disease Other],
				[FH of learning difficulty],
--80
				[FH of learning difficulty Sex],
				[FH of learning difficulty Details],
				[FH of twins],
				[FH of twins Details],
				[Gravida],
				[No. of children living now],
				[No. of children who have died],
				[No. of Neonatal Deaths],
				[No. of Non-Registerable Births],
				[No. of Non-registerable Pregs],
				[No. of Past Pregnancies],
				[No. of pre-term livebirths],
				[No. of Registerable Livebirths],
				[No. of Registerable Pregnancies],
				[No. of Registerable Stillbirths],
				[Parity],
				[Pregs ending before 12 wks],
				[Pregs ending before 12 wks Miscarriages],
				[Pregs ending before 12 wks Induced Abortions],
				[Pregs ending before 12 wks Terminations],
--100
				[Pregs ending before 12 wks Ectopics],
				[Pregs ending before 12 wks Hydatidiform Moles],
				[Pregs ending between 12 - 24 w],
				[Pregs ending between 12 - 24 w Miscarriages],
				[Pregs ending between 12 - 24 w Induced Abortions],
				[Pregs ending between 12 - 24 w Terminations],

				[Pregs ending between 12 - 24 w Ectopics],
				[Pregs ending between 12 - 24 w Hysterotomys],
				[Previous Caesarean Sections],
				[Previous Spontaneous Abortions],
				[Previous Therapeutic Abortions],
				[Alternative contact],
				[Alternative contact Contact name],
				[Alternative contact Address 1],
				[Alternative contact Address 2],
				[Alternative contact Address 3],
				[Alternative contact Address 4],
				[Alternative contact Daytime Telephone],
				[Alternative contact Evening Telephone],
				[Any other smoker in household],
--120
				[Any other smoker in household Who],
				[At booking - ever smoked],
				[At booking - ever smoked Average amount smoked],
				[At booking - ever smoked Date stopped smoking],
				[At booking - ready to quit smoking],
				[At booking-referred for help quitting smoking?],
				[At booking-referred for help quitting smoking? Type of help to which refd.],
				[Baby fathers occ. status],
				[Baby fathers occupation],
				[Is partner the babys father],
				[Is partner the babys father Fathers name],
				[Is partner the babys father Address 1],
				[Is partner the babys father Address 2],
				[Is partner the babys father Address 3],
				[Is partner the babys father Address 4],
				[Is partner the babys father Postcode],
				[Is partner the babys father Daytime telephone],
				[Is partner the babys father Evening telephone],
				[Mothers Occupation],
				[Occupational Status of Mother],
--140
				[One or Two parent Family],
				[Partners Country of origin],
				[Partners ethnic category],
				[Partners faith or religion],
				[Partners Occupation],
				[Partners occupational status],
				[Partners smoking habits],
				[Partners smoking habits Currently],
				[Partners surname],
				[Partners surname Forename],
				[Partners surname Work Phone Number],
				[Preferred contact],
				[Preferred contact Contact name],
				[Preferred contact Address 1],
				[Preferred contact Address 2],
				[Preferred contact Address 3],
				[Preferred contact Address 4],
				[Preferred contact Daytime Telephone],
				[Preferred contact Evening Telephone],
				[Reason for stopping smoking],
--160
				[Reason for stopping smoking Prompted by],
				[Reason for stopping smoking Text],
				[Should BCG be recommended Reason],
				[Should BCG be recommended],
				[Sickle Cell risk],
				[Smoked during 12 months pre-conception],
				[Smoking at booking],
				[Smoking at booking Amount smoked currently],
				[Smoking status at booking],
				[Smoking status at booking Before pregnancy],
				[Smoking status at booking Currently],
				[Smoking status at booking Date stopped smoking],
				[Thalassaemia risk],
				[Additional GP / Midwife issues],
				[Additional GP / Midwife issues Include in Letter],
				[Additional GP / Midwife issues Text],
				[Alcohol consumption (Mother)],
				[Are parents blood relatives],
				[Assessment Completed by],
				[Assessment Completed by Status],
--180
				[Booking Body Mass Index],
				[Conceived with coil in situ],
				[Conceived with coil in situ Still in Situ],
				[Confidential information],	
				[Confidential information Text],
				[Date contraception stopped],
				[Folic acid taken],
				[Folic acid taken When started],
				[Gest. at initial assessment],
				[History of drug abuse],
				[History of drug abuse Drug 1],
				[History of drug abuse Drug 2],
				[History of drug abuse Drug 3],
				[History of drug abuse Text],
				[Most recent contraception],
				[Most recent contraception Text],
				[Mothers booking weight in Kg],
				[Over the counter medicines],
				[Over the counter medicines Drug 1],
				[Over the counter medicines Drug 2],
--200
				[Over the counter medicines Drug 3],
				[Over the counter medicines Text],
				[Prescribed medication in preg],
				[Prescribed medication in preg Drug 1],
				[Prescribed medication in preg Drug 2],
				[Prescribed medication in preg Drug 3],
				[Prescribed medication in preg Text],
				[Suppress sensitive data],
				[Tattoos],
				[Vaginal discharge],
				[Wishes maternal serum screen],
				[Abnormal_smear],
				[Antenatal Booking Summary P1],
				[Antenatal Booking Summary P1 Filename],
				[Antenatal Booking Summary P2],
				[Antenatal Booking Summary P2 Filename],
				[Antenatal Booking Summary P3],
				[Antenatal Booking Summary P3 Filename],
				[Antepartum Haemorrhage],
				[Any flu-like illnesses],
--220
				[Any flu-like illnesses Details],
				[Chicken pox contact Details],
				[Chicken pox contact],
				[Confirm Parentcraft offered],
				[Confirm Parentcraft offered Reason not offered],
--225
				[Date of Initial Assessment],
				[Early vaginal bleeding],
				[Early vaginal bleeding Details],
				[Feeding Intention at Booking],
				[Fertility treatment this preg.],
--230
				[Fertility treatment this preg. Treatment 1],
				[Fertility treatment this preg. Treatment 2],
				[Fertility treatment this preg. Treatment 3],
				[Fertility treatment this preg. Treatment 4],
				[Fertility treatment this preg. Text],
				[Fetal risk category Reason],
				[Fetal risk category],
				[Gestational Diabetes],
				[Gestational Diabetes Insulin given],
				[Gestational_diabetes],
--240
				[Group B streptococcus risk],
				[Group B streptococcus risk Indication 1],
				[Group B streptococcus risk Indication 2],
				[Group B streptococcus risk Indication 3],
				[Group B streptococcus risk Text],
				[Maternal risk category Reason],
				[Maternal risk category],
				[No. babies expected this preg.],
				[No. babies expected this preg. Basis],
				[Other Antenatal Problems],
				[Other Antenatal Problems Text],
				[Paediatric risk],
				[Paediatric risk Text],
				[Planned p/partum sterilisation Details],
				[Planned p/partum sterilisation],
				[Preallocated baby number 1],
				[Preallocated baby number 2],
				[Preallocated baby number 3],
				[Preallocated baby number 4],
				[Preallocated baby number 5],
--260
				[Pregnancy Induced Hypertension],
				[Rubella contact Details],
				[Rubella contact],
				[Suggested action],
				[Urine infections],
				[Best EDD from dating scan],
				[Best EDD from LMP data],
				[Confidence in date of LMP],
				[Date of LMP],
				[Final due date],
				[Final due date Basis],
				[Number of days in cycle],
				[Pattern of period before preg],
				[Scan observations],
				[Scan observations Date],
				[Scan observations Weeks Gestation],
				[Scan observations Days Gestation],
				[Scan observations Number of babies],
				[Scan observations Text],
				[Was LMP a withdrawal bleed],
--280
				[Absent fetal heartbeat in preg],
				[Blood urine during pregnancy],
				[Diastolic BP above 90],
				[Glycosuria this pregnancy],
				[High liquor volume],
				[Low liquor volume],
				[Lowest Hb this pregnancy],
				[MAP > 105 mmhg],
				[Per vaginum loss this preg.],
				[Proteinuria this pregnancy],
				[Raised HbA1c this pregnancy],
				[Reduced Fetal movements in prg],
				[Significant oedema this preg],
				[Systolic BP above 140],
				[Unsatisfactory CTG this preg.],
				[Urine ketones during pregnancy],
				[Method of abortion],
				[Reason for induced abortion],
				[Reason for induced abortion Details],
				[Reason for termination],
--300
				[Reason for termination Details],
				[Referred to social services])
	    	VALUES 
				(@pointer
				,@l_pregnancy_id
				,@l_pregnancy_number
--				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMB40')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMBcu')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMBcu'),'User')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMBcu'),'Date')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMBcu'),'Time')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMBcu'),'Reason')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbTO')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTO'),'Text')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbTG')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTG'),'Text')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbTN')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTN'),'Text')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPRC')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMsys')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbTA')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZME04')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZME08')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbTQ')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbTH')
--20
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTH'),'Text')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbTK')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbTE')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZME07')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZME03')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbTM')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTM'),'Text')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbTB')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTB'),'Text')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbTC')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTC'),'Text')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbTR')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTR'),'Grade')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbTP')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTP'),'Acupuncture')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTP'),'Aromatherapy')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTP'),'Hot Packs')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTP'),'Relaxation Methods')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTP'),'TENS')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTP'),'Water')
--40
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTP'),'Text')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPR1')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbTI')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTI'),'Drug 1')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTI'),'Drug 2')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTI'),'Drug 3')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTI'),'Text')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbTF')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTF'),'Epidural')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTF'),'Self_admin._inhalation')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTF'),'Local_Infiltration')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTF'),'Non-opioid e.g. paracetamol')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTF'),'Opioid analgesia eg. pethidi')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTF'),'Text')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPR2')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPR2'),'Text')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZNxy1')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZME09')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZME09'),'Text')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZME01')
--60
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZME01'),'Text')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbTL')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZME05')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCRT')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbTJ')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbTJ'),'Route & Dose')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPR3')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMHF4')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMHF4'),'Downs')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMHF4'),'Neural Tube Defect')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMHF4'),'Other')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMHP4')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMHP4'),'Details')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFM9')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMFM9'),'Details')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMHF8')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMHF8'),'Cystic Fibrosis')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMHF8'),'Muscular Dystrophy')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMHF8'),'Other')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMHF6')
--80
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMHF6'),'Sex')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMHF6'),'Details')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMHF9')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMHF9'),'Details')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF21')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF27')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF28')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF29')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF38')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF24')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF22')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPL1')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF25')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF23')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF26')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF09')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF30')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF30'),'Miscarriages')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF30'),'Induced Abortions')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF30'),'Terminations')
--100
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF30'),'Ectopics')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF30'),'Hydatidiform Moles')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF31')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF31'),'Miscarriages')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF31'),'Induced Abortions')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF31'),'Terminations')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF31'),'Ectopics')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF31'),'Hysterotomys')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF42')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF40')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF41')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX34')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX34'),'Contact name')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX34'),'Address 1')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX34'),'Address 2')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX34'),'Address 3')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX34'),'Address 4')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX34'),'Daytime Telephone')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX34'),'Evening Telephone')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX55')
--120
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX55'),'Who')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX64')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX64'),'Average amount smoked')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX64'),'Date stopped smoking')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX62')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX63')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX63'),'Type of help to which refd.')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX28')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX27')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX26')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX26'),'Fathers name')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX26'),'Address 1')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX26'),'Address 2')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX26'),'Address 3')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX26'),'Address 4')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX26'),'Postcode')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX26'),'Daytime telephone')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX26'),'Evening telephone')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX20')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX25')
--140
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX06')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMHco')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX22')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX08')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX21')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX09')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX54')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX54'),'Currently')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX32')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX32'),'Forename')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX32'),'Work Phone Number')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX33')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX33'),'Contact name')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX33'),'Address 1')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX33'),'Address 2')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX33'),'Address 3')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX33'),'Address 4')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX33'),'Daytime Telephone')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX33'),'Evening Telephone')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX95')
--160
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX95'),'Prompted by')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX95'),'Text')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF57'),'Reason')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF57')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX17')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX65')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX61')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX61'),'Amount smoked currently')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX11')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX11'),'Before pregnancy')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX11'),'Currently')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX11'),'Date stopped smoking')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX16')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF52')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF52'),'Include in Letter')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF52'),'Text')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX10')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX50')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF88')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF88'),'Status')
--180
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF78')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF43')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF43'),'Still in Situ')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFA2')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMFA2'),'Text')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFB2')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMH01')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMH01'),'When started')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF0A')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF83')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF83'),'Drug 1')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF83'),'Drug 2')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF83'),'Drug 3')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF83'),'Text')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFB1')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMFB1'),'Text')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF86')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF02')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF02'),'Drug 1')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF02'),'Drug 2')
--200
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF02'),'Drug 3')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF02'),'Text')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF99')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF99'),'Drug 1')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF99'),'Drug 2')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF99'),'Drug 3')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF99'),'Text')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFA1')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMHP3')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMHP1')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMHP2')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'XME03')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAB3')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAB3'),'Filename')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAB1')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAB1'),'Filename')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAB2')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAB2'),'Filename')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP7b')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF85')
--220
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF85'),'Details')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF35'),'Details')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF35')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMpc1')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMpc1'),'Reason not offered')
--225
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF01')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF84')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF84'),'Details')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMB96')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF66')
--230
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF66'),'Treatment 1')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF66'),'Treatment 2')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF66'),'Treatment 3')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF66'),'Treatment 4')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF66'),'Text')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMfrk'),'Reason')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMfrk')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP72')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP72'),'Insulin given')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'XME02')
--240
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSBR')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMSBR'),'Indication 1')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMSBR'),'Indication 2')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMSBR'),'Indication 3')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMSBR'),'Text')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMrsk'),'Reason')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMrsk')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMA19')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA19'),'Basis')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPRI')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPRI'),'Text')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP30')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP30'),'Text')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP70'),'Details')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP70')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMro5')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMro6')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMro7')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMro8')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMro9')
--260
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP7a')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF33'),'Details')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF33')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP31')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMp17')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFMP')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFMG')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFM3')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF60')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP11')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP11'),'Basis')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFM2')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFM1')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP1S')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP1S'),'Date')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP1S'),'Weeks Gestation')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP1S'),'Days Gestation')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP1S'),'Number of babies')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP1S'),'Text')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMFM4')
--280
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMp09')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMp13')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMp06')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMp02')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMp16')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMp15')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMp04')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMp07')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMp12')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMp01')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMp03')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMp10')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMp08')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMp05')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMp11')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMp14')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMEA2')
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMEA1')
				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMEA1'),'Details')
-- 1.1 start
--				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMEA4')
--				,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMEA4'),'Details')
				,isnull(dbo.func_Extract_Value(@pointer,@NOTENO,'ZMEA4'),dbo.func_Extract_Value(@pointer,@NOTENO,'ZMEA3'))
				,isnull(dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMEA4'),'Details'),dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMEA3'),'Details'))
-- 1.1 end
				,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSRF'))

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_Pregnancy', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating pregnancy record ' + @l_pregnancy_number + ' for patient ' + cast(@pointer as varchar), -1 )
		END
		ELSE
		BEGIN
			SET @RowCount = @RowCount + 1
			EXEC dbo.pr_Explode_Referrals @pointer, @l_pregnancy_id, @l_pregnancy_number,@NOTENO
			EXEC dbo.pr_Explode_ClinicalReviews @pointer, @l_pregnancy_id, @l_pregnancy_number, @NOTENO
			EXEC dbo.pr_Explode_Delivery @pointer, @l_pregnancy_id, @l_pregnancy_number, @NOTENO
			EXEC dbo.pr_Explode_PregnancyCarePlans @pointer, @l_pregnancy_id, @l_pregnancy_number, @NOTENO
			IF dbo.func_Extract_Value(@pointer, @NOTENO, 'ZMSRF') = 'Yes' 
				EXEC dbo.pr_Explode_SocialServicesReferral @pointer, @l_pregnancy_id, @l_pregnancy_number, @NOTENO
			EXEC dbo.pr_Explode_DownsNeuralTubeScreening @pointer, @l_pregnancy_id, @l_pregnancy_number, @NOTENO  -- v1.3
			EXEC dbo.pr_Explode_Cervicogram @pointer, @l_pregnancy_id, @l_pregnancy_number, @NOTENO  -- v1.4
			EXEC dbo.pr_Explode_SummaryOfAnaesthesia @pointer, @l_pregnancy_id, @l_pregnancy_number, @NOTENO  -- v1.5

		END
    	
		FETCH NEXT FROM get_pregnancy INTO
		@pointer,@NOTENO
	END

	CLOSE get_pregnancy
	DEALLOCATE get_pregnancy

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	VALUES ( getdate(), 'pr_Explode_Pregnancy', 'tbl_Pregnancy records created', @RowCount )

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	SELECT getdate(),  'pr_Explode_Pregnancy', 'tbl_Delivery records created', count(*) from tbl_Delivery

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	SELECT getdate(),  'pr_Explode_Pregnancy', 'tbl_Baby records created', count(*) from tbl_Baby

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	SELECT getdate(),  'pr_Explode_Pregnancy', 'tbl_DeathOfBaby records created', count(*) from tbl_DeathOfBaby

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	SELECT getdate(),  'pr_Explode_Pregnancy', 'tbl_ClinicalReviews records created', count(*) from tbl_ClinicalReviews

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	SELECT getdate(),  'pr_Explode_Pregnancy', 'tbl_FetalObservations records created', count(*) from tbl_FetalObservations

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	SELECT getdate(),  'pr_Explode_Pregnancy', 'tbl_ReviewExaminations records created', count(*) from tbl_ReviewExaminations

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	SELECT getdate(),  'pr_Explode_Pregnancy', 'tbl_ReviewInvestigations records created', count(*) from tbl_ReviewInvestigations

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	SELECT getdate(),  'pr_Explode_Pregnancy', 'tbl_ReviewTreatments records created', count(*) from tbl_ReviewTreatments

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	SELECT getdate(),  'pr_Explode_Pregnancy', 'tbl_Referrals records created', count(*) from tbl_Referrals

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	SELECT getdate(),  'pr_Explode_Pregnancy', 'tbl_SocialServicesReferral records created', count(*) from tbl_SocialServicesReferral

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	SELECT getdate(),  'pr_Explode_Pregnancy', 'tbl_PregnancyCarePlan records created', count(*) from tbl_PregnancyCarePlans

-- v1.1 start
	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	SELECT getdate(),  'pr_Explode_Pregnancy', 'tbl_BloodSpotScreening records created', count(*) from tbl_BloodSpotScreening
-- v1.1 end


	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	SELECT getdate(),  'pr_Explode_Pregnancy', 'tbl_DownsNeuralTubeScreening records created', count(*) from tbl_DownsNeuralTubeScreening




END

