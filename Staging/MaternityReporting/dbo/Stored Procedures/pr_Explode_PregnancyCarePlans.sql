﻿/*
**************************************************************************************************************************
**	NAME:		pr_Explode_PregnancyCarePlans						**
**	DESCRIPTION:	Runs the Pregnancy Care Plans  'explode' routine				**
**	AUTHOR:	Stephen Creek								**
**	VERSIONS:	1.0	Stephen Creek	08/11/2007	Initial version created		**
**			1.1	Stephen Creek	30/11/2007	Updated with new table names	**
**			1.2	Stephen Creek	11/02/2007	Added Pregnancy_ID		**
**			1.3 Keith Lawrence (Barnsley) 02/03/2009 Truncated Maternity GP manually to stop failures
**			1.4	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**											Noteno has changed from a 5 byte binary coded structured key
**											to a 25 byte (5 x 5 bytes) ASCII coded key.
**************************************************************************************************************************
*/
CREATE PROCEDURE [dbo].[pr_Explode_PregnancyCarePlans]
	(@pointer int
	,@p_pregnancy_id int
	,@p_pregnancy_number varchar(30)
	,@p_noteno VARCHAR(25))
AS
BEGIN
	DECLARE @NOTENO  VARCHAR(25)
	DECLARE @l_careplan_number INT
	DECLARE @SQLError INT

	DECLARE get_careplan CURSOR FOR
	SELECT Noteno
	FROM tbl_Notes_Copy
	WHERE Code = 'ZMbkn'
	AND Pointer = @pointer
	AND substring(Noteno,1,len('x'+@p_noteno+'x')-2) = @p_noteno
	ORDER BY Noteno

	SET @l_careplan_number = 0
 
	OPEN get_careplan
	FETCH NEXT FROM get_careplan INTO @NOTENO

	WHILE @@fetch_status = 0
	BEGIN
		SET @l_careplan_number = @l_careplan_number + 1

		--v1.4 SET @NOTENO = SUBSTRING(@NOTENO,1,3)
		SET @NOTENO = SUBSTRING(@NOTENO,1,15)

		INSERT INTO [tbl_PregnancyCarePlans] 
            			(Patient_Pointer,
			Pregnancy_ID,
            			[Pregnancy number],
			CarePlan_Number,
			[Care plan recommendation],
			[Care plan recommendation Text],
			[Care plan revised at review],
			[Care scheme],
			[Care scheme Text],
			[Consent for Caesarean section accepted],
			[Consent for Caesarean section accepted Reason],
			[Consent for Caesarean section offered],
			[Consent for Caesarean section offered Reason],
			[Consultant Obstetrician], --13
			[Current risk category],
			[Current risk category Reason],
			[Date of care plan review],
			[Evident based information provided],
			[Evident based information provided Reason],
			[Gestation at plan review],
			[Indication for plan review],
			[Indication for plan review Indication 1],
			[Indication for plan review Indication 2],
			[Indication for plan review Indication 3],
			[Indication for plan review Indication 4],
			[Indication for plan review Text],
			[Intended resp. for birth],
			[Intended resp. for birth Text],
			[Lead Professional],
			[Management of deliv. admission],
			[Maternity care GP],  -- 30
			[Midwifery Team],
			[Named Midwife],
			[Plan reviewed at request of],
			[Plan reviewed with],
			[Planned date of procedure],
			[Planned management of delivery],
			[Reason for transfer in Labour],
			[Reason for transfer in Labour Reason],
			[Reason for unplanned home conf],
			[Reason for unplanned home conf Reason],
			[Time of care plan review],
			[Timing of plan review],
			[Transfer out due to lack of beds])
		VALUES
            			(@pointer
            			,@p_pregnancy_id
            			,@p_pregnancy_number
            			,@l_careplan_number
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbkK')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbkK'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbkL')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbkD')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbkD'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbkQ')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbkQ'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbkP')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbkP'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbk6') + ',' + dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbk6') -- Consultant Obstetrician
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbkC')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbkC'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbk2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbkN')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbkN'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbk3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb06')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb06'),'Indication 1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb06'),'Indication 2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb06'),'Indication 3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb06'),'Indication 4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb06'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbk9')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbk9'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbk5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbkE')
			-- OLD 02/03/2009 : ,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbk1')+ ',' + dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbk1') -- Maternity GP
			,LEFT(dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbk1')+ ',' + dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbk1'),60) -- Maternity GP
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbk8')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbk7') + ',' + dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMbk7') -- Named Midwife
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbkI')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbkJ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbkH')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbkA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb07')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb07'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMb08')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMb08'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbki')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMbk4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMnp3'))

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_PregnancyCarePlans', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating care plan record ' + cast(@l_careplan_number as varchar) + ' for pregnancy ' + cast(@p_pregnancy_number as varchar) + ' for patient ' + cast(@pointer as varchar), -1 )
		END

	            FETCH NEXT FROM get_careplan INTO @NOTENO
	END

	CLOSE get_careplan
	DEALLOCATE get_careplan
END
