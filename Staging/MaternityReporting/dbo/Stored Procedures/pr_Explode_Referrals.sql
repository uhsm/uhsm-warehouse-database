﻿/*
****************************************************************************************************************
**	NAME:		pr_Explode_Referrals						**
**	DESCRIPTION:	Runs the Referrals  'explode' routine				**
**	AUTHOR:	Stephen Creek							**
**	VERSIONS:	1.0	Stephen Creek	06/11/2007	Initial version created	**
**			1.1	Stephen Creek	11/02/2007	Added Pregnancy_ID	**
**			1.2	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**											Noteno has changed from a 5 byte binary coded structured key
**											to a 25 byte (5 x 5 bytes) ASCII coded key.
****************************************************************************************************************
*/
CREATE PROCEDURE [dbo].[pr_Explode_Referrals]
	(@pointer int
	,@p_pregnancy_id int
	,@p_pregnancy_number varchar(30)
	,@p_noteno VARCHAR(25))
AS
BEGIN
	DECLARE @NOTENO  VARCHAR(25)
	DECLARE @l_referral_number INT
	DECLARE @SQLError INT

	DECLARE get_referral CURSOR FOR
	SELECT Noteno
	FROM tbl_Notes_Copy
	WHERE Code = 'ZMH11'
	AND Pointer = @pointer
	AND substring(Noteno,1,len('x'+@p_noteno+'x')-2) = @p_noteno
	ORDER BY Noteno

	SET @l_referral_number = 0
 
	OPEN get_referral
	FETCH NEXT FROM get_referral INTO @NOTENO

	WHILE @@fetch_status = 0
	BEGIN
		SET @l_referral_number = @l_referral_number + 1
		--v1.2 SET @NOTENO = SUBSTRING(@NOTENO,1,3)
		SET @NOTENO = SUBSTRING(@NOTENO,1,15)
		INSERT INTO [tbl_Referrals] 
            			(Patient_Pointer,
			Pregnancy_ID,
            			[Pregnancy number],
			Referral_Number,
			[Clinical Reason for referral],
			[Clinical Reason for referral Text],
			[Consultant action at referral],
			[Consultant action at referral Text],
			[Consultant referred to],
			[Date of Referral],
			[Days Postnatal],
			[Deletion allowed],
			[General Practitioner],
			[Hospital transferred from Hospital],
			[Hospital transferred from],
			[Initial estimate of due date],
			[Intended Delivery Place],
			[Intended Delivery Place Text],
			[Intended Responsible Hospital],
			[Is this the Registered GP],
			[Lead Profession],
			[Midwifery Team],
			[Name of referrer],
			[Named Midwife],
			[PAS Reason for referral],
			[Planned care scheme at referl.],
			[Planned care scheme at referl. Text],
			[Planned management],
			[Reason for referral],
			[Reason for transfer Text],
			[Reason for transfer],
			[Reason for transfer of care],
			[Referral episode number],
			[Referral event number],
			[Referral status],
			[Referral status Date of Discharge],
			[Referral status Reason],
			[SMR Institution code],
			[Source of referral],
			[UK or Non UK Hospital])
		VALUES
            			(@pointer
            			,@p_pregnancy_id
            			,@p_pregnancy_number
            			,@l_referral_number
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGAD3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGAD3'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMBCA')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMBCA'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGAM9')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGAD1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPn1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMed2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMB22')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMB70'),'Hospital')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMB70')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMB04')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMB03')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMB03'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMB0I')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMB25')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMB21')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMCCq')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGAD4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMB23')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMBI0')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMB15')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMB15'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMB06')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMB00')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMB14'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMB14')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMB30')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMep2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMev2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDRE4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDRE4'),'Date of Discharge')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDRE4'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMB71')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGAD2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMB72'))

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_Referrals', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating referral record ' + cast(@l_referral_number as varchar) + ' for pregnancy ' + cast(@p_pregnancy_number as varchar) + ' for patient ' + cast(@pointer as varchar), -1 )
		END

	            FETCH NEXT FROM get_referral INTO @NOTENO
	END

	CLOSE get_referral
	DEALLOCATE get_referral
END
