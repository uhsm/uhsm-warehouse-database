﻿/*
**************************************************************************************************************************
**	NAME:		pr_Explode_ReviewExaminations						**
**	DESCRIPTION:	Runs the New Review Examination 'explode' routine			**
**	AUTHOR:	Stephen Creek								**
**	VERSIONS:	1.0	Stephen Creek	08/11/2007	Initial version created		**
**			1.1	Stephen Creek	07/02/2008	Corrected use of len() function	**
**			1.2	Stephen Creek	11/02/2008	Added Pregnancy_ID		**
**			1.3	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**											Noteno has changed from a 5 byte binary coded structured key
**											to a 25 byte (5 x 5 bytes) ASCII coded key.
**************************************************************************************************************************
*/
CREATE PROCEDURE [dbo].[pr_Explode_ReviewExaminations]
	(@pointer int
	,@p_pregnancy_id int  --v1.2
	,@p_pregnancy_number varchar(30)
	,@p_review_number int
	,@p_noteno VARCHAR(25))
AS
BEGIN
	DECLARE @NOTENO  VARCHAR(25)
	DECLARE @SQLError INT

	DECLARE get_examinations CURSOR FOR
	SELECT Noteno
	FROM tbl_Notes_Copy
	WHERE Code = 'ZTSOE' -- Review Examinations
	AND Pointer = @pointer
	AND substring(Noteno,1,len('x'+@p_noteno+'x')-2) = @p_noteno  --v1.1
	ORDER BY Noteno

	OPEN get_examinations
	FETCH next from get_examinations into @NOTENO

	WHILE @@fetch_status = 0
	BEGIN
		-- v1.3 SET @NOTENO = SUBSTRING(@NOTENO,1,4)
		SET @NOTENO = SUBSTRING(@NOTENO,1,20)
		
		--PRINT	OBJECT_NAME(@@PROCID) + ' INSERT INTO [tbl_ReviewExaminations] '
		--	+	'Patient_Pointer=' + ISNULL(CAST(@pointer AS VARCHAR(10)),'NULL')
		--	+	', Pregnancy_ID=' + ISNULL(CAST(@p_pregnancy_id AS VARCHAR(10)),'NULL')
		--	+	', Pregnancy number=' + ISNULL(@p_pregnancy_number,'NULL')
		--	+	', Review_Number=' + ISNULL(CAST(@p_review_number AS VARCHAR(10)),'NULL')

		INSERT INTO [tbl_ReviewExaminations] 
            			(Patient_Pointer,
			Pregnancy_ID,  --v1.2
            			[Pregnancy number],
            			Review_Number,
			[10 year CHD event risk (BCS)],
			[10 yr mod. CHD event risk(BCS)],
			[Abdominal Lie],
			[Abdominal station],
			[Antenatal Body Mass Index],
			[Any fetal heart detected],
			[Any fetal pole detected],
			[Any other complaints],
			[Any other complaints Comments],
			[Appears jaundiced],
			[Arcus],
			[Aspiration or vomit],
			[Aspiration or vomit Bile stained],
			[Aspiration or vomit Blood stained],
			[Aspiration or vomit Milk],
			[Aspiration or vomit Meconium],
			[Aspiration or vomit Amount],
			[Baby visited by],
			[Baby visited by Text],
			[Babys weight in grams],
			[Bishops Score],
			[Bladder],
			[Blood pressure reading],
			[Blood pressure reading Systolic],
			[Blood pressure reading Diastolic],
			[Blood pressure reading Mean],
			[Blood pressure reading 1],
			[Blood pressure reading 1 Systolic],
			[Blood pressure reading 1 Diastolic],
			[Blood pressure reading 2],
			[Blood pressure reading 2 Systolic],
			[Blood pressure reading 2 Diastolic],
			[Blood pressure reading 3],
			[Blood pressure reading 3 Systolic],
			[Blood pressure reading 3 Diastolic],
			[Blood pressure reading 4],
			[Blood pressure reading 4 Systolic],
			[Blood pressure reading 4 Diastolic],
			[Blood pressure reading 5],
			[Blood pressure reading 5 Systolic],
			[Blood pressure reading 5 Diastolic],
			[Body Mass Index],
			[Body Mass Index centile],
			[Bowels opened 1],
			[Bowels opened 1 Text],
			[Bowels opened 2],
			[Bowels opened 2 Stool],
			[Bowels opened Consistency],
			[Breast problems],
			[Breast problems Text],
			[Breasts],
			[Breath sounds],
			[Breath sounds Text],
			[Catheterised during delivery],
			[Catheterised during delivery Technique],
			[Catheterised during delivery Catheter in situ],
			[Catheterised during delivery Details],
			[Cephalic position],
			[Cervical consistency],
			[Cervical dilation],
			[Cervical length],
			[Cervical os],
			[Cervical position],
			[Colour],
			[Colour of stool passed],
			[Colour of stool passed Text],
			[Coma - eyes open],
			[Consent obtained for Vaginal Examination],
			[Contractions],
			[Contractions Date],
			[Contractions Time],
			[Contractions Strength],
			[Contractions Duration],
			[Contractions Frequency(per 10 mins)],
			[Contractions Text],
			[Cord],
			[Cord Text],
			[Date of Vaginal Examination],
			[Effacement],
			[Evacuation],
			[Evacuation Text],
			[Eyes 1],
			[Eyes 1 Text],
			[Eyes 2],
			[Eyes 2 Side],
			[Eyes 2 Swab taken],
			[Feeding ability 1],
			[Feeding ability 1 Appetite],
			[Feeding ability 1 Time taken to feed],
			[Feeding ability 1 Text],
			[Feeding ability 2],
			[Feeding ability 2 Appetite],
			[Feeding ability 2 Time taken to feed],
			[Feeding ability 2 Text],
			[Feeding frequency],
			[Feeding frequency Amount],
			[Feeding frequency Text],
			[Feeding pattern],
			[Feeding pattern Text],
			[Feeding problems],
			[Feeding problems Text],
			[Fetal movements felt],
			[Fetal position],
			[FEV],
			[Fundal Height],
			[FVC],
			[Head circumference],
			[Head circumference centile],
			[Heart sounds],
			[Heart sounds Text],
			[Height centile],
			[Height in centimetres],
			[Is the baby still on NNU],
			[Left adnexae],
			[Left adnexae Text],
			[Left arm movements],
			[Left leg movements],
			[Left ovary],
			[Left ovary Text],
			[Left pupil reaction],
			[Left pupil size],
			[Legs],
			[Legs Text],
			[Liquor],
			[Liquor state],
			[Liquor volume],
			[Mallampati intubation score],
			[Mallampati intubation score Text],
			[Method of feeding],
			[Motor response],
			[Moulding],
			[Nuchal translucency risk fctr],
			[Number of babies expected],
			[Number of babies expected Basis],
			[Oedema],
			[Other findings on Vaginal Examination],
			[Other findings on Vaginal Examination Text],
			[Parents performing care],
			[Parents performing care Text],
			[PEFR],
			[Pelvic examination],
			[Pelvic examination Text],
			[Per vaginum loss],
			[Per vaginum loss Text],
			[Perineum],
			[Perineum Text],
			[Population normal risk],
			[Postnatal admission to NNU],
			[Postnatal admission to NNU Text],
			[Postnatal problems],
			[Postnatal problems Problem 1],
			[Postnatal problems Problem 2],
			[Postnatal problems Problem 3],
			[Postnatal problems Problem 4],
			[Postnatal problems Text],
			[Predicted FEV],
			[Predicted FVC],
			[Predicted PEFR],
			[Presence/Severity of Jaundice],
			[Presence/Severity of Jaundice Maximum SBR],
			[Presentation prior to delivery],
			[Presentation prior to delivery Text],
			[Problems passing urine],
			[Problems passing urine Text],
			[Progress report for parents],
			[Progress report for parents Text],
			[Pubertal stage],
			[Pulse or heart rate],
			[Respiration rate],
			[Retinal Changes],
			[Retinal Changes Papilloedema],
			[Retinal Changes Cotton Wool Spots],
			[Retinal Changes Haemorrhages],
			[Retinal Changes Retinal Vein Occlusion],
			[Right adnexae],
			[Right adnexae Text],
			[Right arm movements],
			[Right leg movements],
			[Right ovary],
			[Right ovary Text],
			[Right pupil reaction],
			[Right pupil size],
			[Signs of respiratory distress],
			[Signs of respiratory distress Reason 1],
			[Signs of respiratory distress Reason 2],
			[Signs of respiratory distress Reason 3],
			[Signs of respiratory distress Reason 4],
			[Spine],
			[Spine Text],
			[Station of presenting part],
			[Sutures removed],
			[Sutures removed Comment],
			[Syntocinon],
			[Temperature],
			[Time of Vaginal Examination],
			[Tone],
			[Umbilicus],
			[Umbilicus Swab taken],
			[Urine passed 1],
			[Urine passed 2],
			[Uterine contents],
			[Uterine contents Text],
			[Uterine state post-partum],
			[Uterine state post-partum Text],
			[Uterus],
			[Verbal response],
			[Waist circumference],
			[Weight at antenatal review in Kg],
			[Weight centile],
			[Weight in kilograms],
			[Wound],
			[Wound Comment],
			[Xanthomata])
		VALUES
            			(@pointer
            			,@p_pregnancy_id --v1.2
            			,@p_pregnancy_number
            			,@p_review_number
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZREX7')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZREX9')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOQA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOQC')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZmrxA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPEj')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPEi')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAUv')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAUv'),'Comments')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXC')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGEX1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSRE9')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSRE9'),'Bile stained')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSRE9'),'Blood stained')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSRE9'),'Milk')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSRE9'),'Meconium')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSRE9'),'Amount')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSFIA')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSFIA'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMREf')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMZ06')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSO1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGDI5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGDI5'),'Systolic')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGDI5'),'Diastolic')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGDI5'),'Mean')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGBP1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGBP1'),'Systolic')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGBP1'),'Diastolic')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGBP2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGBP2'),'Systolic')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGBP2'),'Diastolic')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGBP3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGBP3'),'Systolic')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGBP3'),'Diastolic')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGBP4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGBP4'),'Systolic')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGBP4'),'Diastolic')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGBP5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGBP5'),'Systolic')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGBP5'),'Diastolic')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDGI4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDCE3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMRE5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMRE5'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSFE6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSFE6'),'Stool')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSFE6'),'Consistency')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMRE1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMRE1'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMRE0')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMY52')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMY52'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMxy0')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMxy0'),'Technique')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMxy0'),'Catheter in situ')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMxy0'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZPARE')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMBS2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMBS5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMBS3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPOe')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMBS4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXG')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMREc')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMREc'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZNE01')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOQD')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMA1H')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1H'),'Date')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1H'),'Time')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1H'),'Strength')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1H'),'Duration')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1H'),'Frequency(per 10 mins)')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA1H'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMREe')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMREe'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMBS6')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZPARG')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPEn')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPEn'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMREd')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMREd'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXR')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDXR'),'Side')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDXR'),'Swab taken')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMREz')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMREz'),'Appetite')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMREz'),'Time taken to feed')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMREz'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSREz')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSREz'),'Appetite')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSREz'),'Time taken to feed')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSREz'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSFEB')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSFEB'),'Amount')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSFEB'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMREa')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMREa'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMREb')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMREb'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMA1C')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMA2K')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZREX1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAUw')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZREX2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSOD6')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSODC')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMY51')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMY51'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDCE1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGDI9')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMREh')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSO3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMSO3'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZNE07')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZNE09')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSO4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMSO4'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZNE05')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZNE04')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMRE3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMRE3'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMZ09')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZPARI')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZPARJ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMY54')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMY54'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMRE9')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZNE03')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZPARH')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPIU')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMA18')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMA18'),'Basis')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMA26')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMZ07')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMZ07'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSFIB')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSFIB'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZREX3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMF90')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMF90'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMRE7')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMRE7'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMRE2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMRE2'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZREX8')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMREg')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMREg'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMY61')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMY61'),'Problem 1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMY61'),'Problem 2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMY61'),'Problem 3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMY61'),'Problem 4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMY61'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZREX4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZREX5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZREX6')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMREi')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMREi'),'Maximum SBR')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMOQB')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMOQB'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMRE4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMRE4'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSFIC')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSFIC'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDPU1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXB')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXD')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGEX3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGEX3'),'Papilloedema')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGEX3'),'Cotton Wool Spots')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGEX3'),'Haemorrhages')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGEX3'),'Retinal Vein Occlusion')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSO5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMSO5'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZNE06')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZNE08')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSO6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMSO6'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZNE0B')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZNE0A')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXE')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDXE'),'Reason 1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDXE'),'Reason 2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDXE'),'Reason 3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDXE'),'Reason 4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMY53')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMY53'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMA1J')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMstr')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMstr'),'Comment')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMZ08')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMBS7')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXT')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXS')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSDXS'),'Swab taken')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMRE8')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSRE8')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPEh')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPEh'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMRE6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMRE6'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSO2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZNE02')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDGI5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmrx1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDCE2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZD121')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMwnd')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMwnd'),'Comment')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGEX2'))

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_ReviewExaminations', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating examination record for review ' + cast(@p_review_number as varchar) + ' for pregnancy ' + cast(@p_pregnancy_number as varchar) + ' for patient ' + cast(@pointer as varchar), -1 )
		END

	            FETCH NEXT FROM get_examinations INTO @NOTENO
	END

	CLOSE get_examinations
	DEALLOCATE get_examinations
END
