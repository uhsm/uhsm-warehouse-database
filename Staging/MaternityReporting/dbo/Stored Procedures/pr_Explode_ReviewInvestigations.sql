﻿/*
**************************************************************************************************************************
**	NAME:		pr_Explode_ReviewInvestigations					**
**	DESCRIPTION:	Runs the New Investigation 'explode' routine				**
**	AUTHOR:	Stephen Creek								**
**	VERSIONS:	1.0	Stephen Creek	08/11/2007	Initial version created		**
**			1.1	Stephen Creek   07/02/2008	Added Ultrasound/Reason/Text	**
**								Corrected use of len() function	**
**			1.2	Stephen Creek	11/02/2008	Added Pregnancy_ID		**
**			1.3	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**											Noteno has changed from a 5 byte binary coded structured key
**											to a 25 byte (5 x 5 bytes) ASCII coded key.
**************************************************************************************************************************
*/
CREATE PROCEDURE [dbo].[pr_Explode_ReviewInvestigations]
	(@pointer int
	,@p_pregnancy_id int
	,@p_pregnancy_number varchar(30)
	,@p_review_number int
	,@p_noteno VARCHAR(25))
AS
BEGIN
	DECLARE @NOTENO  VARCHAR(25)
	DECLARE @SQLError INT

	DECLARE get_investigations CURSOR FOR
	SELECT Noteno
	FROM tbl_Notes_Copy
	WHERE Code = 'ZTSOD' -- Review Investigations
	AND Pointer = @pointer
	AND substring(Noteno,1,len('x'+@p_noteno+'x')-2) = @p_noteno  --v1.1
	ORDER BY Noteno

	OPEN get_investigations
	FETCH next from get_investigations into @NOTENO

	WHILE @@fetch_status = 0
	BEGIN
		--v1.3 SET @NOTENO = SUBSTRING(@NOTENO,1,4)
		SET @NOTENO = SUBSTRING(@NOTENO,1,20)
		
		--PRINT	OBJECT_NAME(@@PROCID) + ' INSERT INTO [tbl_ReviewInvestigations] '
		--	+	'Patient_Pointer=' + ISNULL(CAST(@pointer AS VARCHAR(10)),'NULL')
		--	+	', Pregnancy_ID=' + ISNULL(CAST(@p_pregnancy_id AS VARCHAR(10)),'NULL')
		--	+	', Pregnancy number=' + ISNULL(@p_pregnancy_number,'NULL')
		--	+	', Review_Number=' + ISNULL(CAST(@p_review_number AS VARCHAR(10)),'NULL')

		INSERT INTO [tbl_ReviewInvestigations] 
            			(Patient_Pointer,
			Pregnancy_ID, --v1.2
            			[Pregnancy number],
            			Review_Number,
			[AAT],
			[Abdominal xray],
			[ABO antibodies],
			[ABO blood group],
			[AFP and downs screen],
			[AFP and downs screen Text],
			[Alanine Transaminase],
			[Albumin],
			[Alkaline phosphatase],
			[ALT],
			[Amino Acids],
			[Ammonia],
			[Anti nuclear factor],
			[APPT],
			[APTT],
			[Aspartate Transaminase],
			[AST],
			[Asymp. bacteriuria screening/MSU accepted],
			[Asymp. bacteriuria screening/MSU accepted Date MSU performed],
			[Asymp. bacteriuria screening/MSU accepted Date of result],
			[Asymp. bacteriuria screening/MSU accepted Result],
			[Asymp. bacteriuria screening/MSU accepted Comments],
			[Asymp. bacteriuria screening/MSU accepted Reason MSU not performed],
			[Asymp. bacteriuria screening/MSU offered],
			[Asymp. bacteriuria screening/MSU offered Reason],
			[Autoantibodies],
			[Barium or gastrograffin enema],
			[Barium swallow],
			[Basophils],
			[Bicarbonate],
			[Blood culture],
			[Blood glucose],
			[Blood pCO2 (max this period)],
			[Blood pCO2 (min this period)],
			[Blood pH],
			[Blood pO2 (max this period)],
			[Blood pO2 (min this period)],
			[C peptide],
			[C reactive protein 1],
			[C Reactive Protein 2],
			[C Reactive Protein 3],
			[Cardiac ultrasound scan],
			[Cervical smear],
			[Chest xray],
			[Chloride],
			[Chromosomes],
			[Copper],
			[Cranial ultrasound scan],
			[Creatinine clearance],
			[CSF culture],
			[Date Group B Streptococcus screening performed],
			[Date Group B Streptococcus screening result],
			[Date Hepatitis A screening performed],
			[Date Hepatitis B screening performed],
			[Date Hepatitis C screening performed],
			[Date HIV screen performed],
			[Date of Diagnosis],
			[Date of Hepatitis A screening result],
			[Date of Hepatitis B screening result],
			[Date of Hepatitis C screening result],
			[Date of HIV screen result],
			[Date of Other screening result - 1],
			[Date of Other screening result - 2],
			[Date of Other screening result - 3],
			[Date of Rubella screening result],
			[Date of Syphilis screening result],
			[Date Other screening performed - 1],
			[Date Other screening performed - 2],
			[Date Other screening performed - 3],
			[Date Rubella screening performed],
			[Date Syphilis screening performed],
			[Details of Hepatitis A screening],
			[Details of Hepatitis B screening],
			[Details of Hepatitis C screening],
			[Details of HIV screening],
			[Details of Rubella screening],
			[Details of Syphilis screening],
			[Digoxin],
			[Direct Bilirubin],
			[Direct Coombs test],
			[DMSA scan],
			[Dobutamine],
			[Dopamine],
			[DTPA scan],
			[ECG],
			[ECG (SV1 + RV5 - 6)],
			[Eosinophils],
			[ESR],
			[ET Aspirate Culture],
			[Fermoral arteriogram],
			[Fibrinogen],
			[Fibrinogen Degredation Product],
			[Film],
			[Free T3],
			[Free T4],
			[Fructosamine],
			[Gentamicin trough level],
			[GGT],
			[Group B streptococcus screen],
			[Group B streptococcus screen Text],
			[Group B Streptococcus screening accepted],
			[Group B Streptococcus screening offered],
			[Group B Streptococcus screening offered Reason],
			[Haematocrit],
			[Haemoglobin],
			[Haemoglobin Qualifier],
			[Hb Electrophoresis],
			[HbA1c],
			[HCG and results],
			[Hepatitis A screening accepted],
			[Hepatitis A screening offered],
			[Hepatitis A screening offered Reason],
			[Hepatitis B screening accepted],
			[Hepatitis B screening offered],
			[Hepatitis B screening offered Reason],
			[Hepatitis C screening accepted],
			[Hepatitis C screening offered],
			[Hepatitis C screening offered Reason],
			[Hepatitis Screen],
			[Hepatitis-A screen],
			[Hepatitis-B screen],
			[Hepatitis-C screen],
			[High vaginal swab],
			[High vaginal swab Organism],
			[High vaginal swab Text],
			[Hip Ultrasound],
			[HIV screen],
			[HIV screen Text],
			[HIV Screen_1],
			[HIV screening accepted],
			[HIV screening offered],
			[HIV screening offered Reason],
			[HIV status known],
			[Immunoglobulins],
			[INR],
			[Investigation 1],
			[Investigation 10],
			[Investigation 2],
			[Investigation 3],
			[Investigation 4],
			[Investigation 5],
			[Investigation 6],
			[Investigation 7],
			[Investigation 8],
			[Investigation 9],
			[Investigations requested],
			[Investigations requested Type 1],
			[Investigations requested Type 2],
			[Investigations requested Type 3],
			[Investigations requested Text],
			[Lactic Acid or Lactate],
			[Leucocytes],
			[Lipaeamic serum],
			[Local urinalysis test],
			[Local urinalysis test Text],
			[Lymphocytes],
			[Magnesium],
			[MAGscan],
			[Maternal Hepatitis B status],
			[MCH],
			[MCHC],
			[MCV],
			[MetHb (max this period)],
			[MetHb (min this period)],
			[Microsomal autoantibodies],
			[Micturating cystogram],
			[Monocytes],
			[MSU],
			[MSU Organism],
			[MSU Text],
			[Netilmicin peak level],
			[Netilmicin trough level],
			[Neutrophils],
			[OGTT 0 hour glucose],
			[OGTT 2 hour glucose],
			[Other blood group antibodies],
			[Other screening accepted - 1],
			[Other screening accepted - 2],
			[Other screening accepted - 3],
			[Other screening offered - 1],
			[Other screening offered - 1 Name of the screening],
			[Other screening offered - 2],
			[Other screening offered - 2 Name of the screening],
			[Other screening offered - 3],
			[Other screening offered - 3 Name of the screening],
			[Oxygen saturation],
			[PCV],
			[Phenobarbitone level],
			[Phenytoin],
			[Platelets],
			[Products of conception felt],
			[Products of conception felt Sent to pathology],
			[Progesterone(17-OH)],
			[Protein (blood total)],
			[Prothrombin time],
			[Reason for change 1],
			[Reason for change 2],
			[Reason for GTT],
			[Reason for GTT Text],
			[Reason Group B Streptococcus screening not taken],
			[Reason Hepatitis A screening not done],
			[Reason Hepatitis B screening not done],
			[Reason Hepatitis C screening not done],
			[Reason HIV screening not done],
			[Reason Other screening not done - 1],
			[Reason Other screening not done - 2],
			[Reason Other screening not done - 3],
			[Reason Rubella screening not done],
			[Reason Syphilis screen not done],
			[Red cell count],
			[Referral to HIV Midwife],
			[Referral to HIV Midwife Text],
			[Renal ultrasound scan],
			[Result of Group B Streptococcus screening],
			[Result of Hepatitis A screening],
			[Result of Hepatitis B screening],
			[Result of Hepatitis C screening],
			[Result of HIV],
			[Result of HIV screening],
			[Result of Other screening - 1],
			[Result of Other screening - 2],
			[Result of Other screening - 3],
			[Result of Rubella screening],
			[Result of Syphilis screening],
			[Rhesus antibodies],
			[Rhesus antibodies Antibody Type],
			[Rhesus blood group],
			[Rheumatoid factor],
			[Rubella antibody screen],
			[Rubella screening accepted],
			[Rubella screening offered],
			[Rubella screening offered Reason],
			[Serum calcium],
			[Serum cholesterol],
			[Serum creatinine],
			[Serum electrophoresis],
			[Serum HDL],
			[Serum IgA],
			[Serum IgE],
			[Serum IgG],
			[Serum IgM],
			[Serum LDL],
			[Serum phosphate],
			[Serum potassium],
			[Serum sodium],
			[Serum total thyroxine],
			[Serum triglycerides],
			[Serum urea],
			[Serum uric acid],
			[Sickle Screen],
			[Skeletal survey],
			[Skull xray],
			[Speculum],
			[Speculum Text],
			[Spine Ultrasound],
			[Swab Culture],
			[Swabs taken],
			[Swabs taken HVS],
			[Swabs taken Endocervical],
			[Swabs taken Smear],
			[Swabs taken Chlamydia],
			[Swabs taken Anal],
			[Swabs taken Interoital],
			[Syphilis screen],
			[Syphilis screening accepted],
			[Syphilis screening offered],
			[Syphilis screening offered Reason],
			[T3 (triiodothyronine)],
			[TcPO2],
			[TCT],
			[Tests requested],
			[Tests requested 1],
			[Tests requested 2],
			[Tests requested 3],
			[Tests requested 4],
			[Tests requested 5],
			[Tests requested 6],
			[Tests requested 7],
			[Tests requested 8],
			[Theophyline or Caffeine],
			[Thyroid function tests],
			[TORCH Screen],
			[Total Bilirubin],
			[Total Chol/HDLC ratio],
			[TSH],
			[Urine albumin excretion rate],
			[Urine albumin/creatinine ratio],
			[Urine Amino Acids],
			[Urine culture],
			[Urine dipstick blood],
			[Urine dipstick glucose],
			[Urine dipstick ketones],
			[Urine dipstick leucocytes],
			[Urine dipstick nitrites],
			[Urine dipstick protein],
			[Urine microalbuminuria],
			[Urine Organic acids],
			[Urine protein - 24 hour],
			[Urine Reducing Substances],
			[Vancomycin peak level],
			[Vancomycin trough level],
			[White cell count],
			[Worst base deficit-this period],
			[Xray exam of feet],
			[Xray exam of hands],
			[Year],
			[Zinc],
-- 1.1 start
			[Ultrasound],
			[Ultrasound Reason],
			[Ultrasound Text])
-- 1.1 end
		VALUES
            			(@pointer
            			,@p_pregnancy_id --v1.2
            			,@p_pregnancy_number
            			,@p_review_number
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGRE6')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSIXA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERB2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERB1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP94')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP94'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOBa')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGRE7')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGRE3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGRE5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOBc')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOBd')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZJRE3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZAATT')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOBe')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOBf')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGRE2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP99')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP99'),'Date MSU performed')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP99'),'Date of result')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP99'),'Result')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP99'),'Comments')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP99'),'Reason MSU not performed')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP88')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMP88'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDMS9')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSIXb')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSIXa')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIHF')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOBg')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEMT2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIB5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXJ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDR1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXK')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXI')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDRO')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEBT1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZJRE1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEMTC')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOBh')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSUSC')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPIN')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIX1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOBi')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOBj')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOBl')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSUSN')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDMS8')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSPr3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERBC')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERBD')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHQ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHD')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHW')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPH1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr21')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHL')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHK')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHM')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHH')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr33')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr38')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr43')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHI')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHJ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr32')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr37')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr42')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPH5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPH9')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHS')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHF')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHY')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPH3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPH7')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHB')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXa')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGREA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERB6')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSIXc')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXb')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXc')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSIXd')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIN1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZECG1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIHE')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIH9')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEMTa')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDFO7')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIHR')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOHb')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOHc')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIBM')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIBK')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDGI3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXL')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGRE4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERB9')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZERB9'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERBB')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERBA')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZERBA'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIH4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIH2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGIH2'),'Qualifier')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOHd')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIBO')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPOh')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHP')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHO')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPHO'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPIQ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPIP')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPIP'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHV')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHU')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPHU'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEMTb')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPIS')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPI5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPIR')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMMT1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMMT1'),'Organism')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMMT1'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSUSa')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPIL')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPIL'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEMTc')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPIY')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPIZ')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPIZ'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr19')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOBo')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZAINR')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDSA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDSJ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDSB')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDSC')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDSD')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDSE')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDSF')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDSG')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDSH')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDSI')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMY55')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMY55'),'Type 1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMY55'),'Type 2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMY55'),'Type 3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMY55'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOBn')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIHG')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSFM1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPOd')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOd'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIHC')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOBp')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSIXe')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPI6')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIH7')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIH8')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIH6')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSCRM')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSRM7')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZTRE2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSIXf')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIHD')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMMT2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMMT2'),'Organism')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMMT2'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXX')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXW')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIHB')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIBS')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIBR')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERB5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr31')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr36')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr41')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr30')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zmr30'),'Name of the screening')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr35')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zmr35'),'Name of the screening')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr40')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'Zmr40'),'Name of the screening')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXH')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIHP')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXN')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXd')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIH3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPOf')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOf'),'Sent to pathology')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOBq')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOBr')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZAPTT')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP16')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP17')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAUi')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAUi'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERBF')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHT')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHG')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHZ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPH4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr50')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr51')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr52')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPH8')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHC')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIH5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPIM')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPIM'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSUSR')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERBE')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHR')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHE')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHX')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr20')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPH2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr34')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr39')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr44')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPH6')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPHA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERB4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZERB4'),'Antibody Type')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERB3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZJRE2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP77')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPIW')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPIX')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPIX'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIB7')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIBG')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIB6')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIBA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIBP')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIBB')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZRRE1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIB9')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIBC')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIBQ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIB8')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIB3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIB2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIBJ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIBH')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIB4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIUR')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOHg')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSIXg')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSIXS')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPOi')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOi'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSUSc')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEMTe')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPOg')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOg'),'HVS')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOg'),'Endocervical')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOg'),'Smear')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOg'),'Chlamydia')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOg'),'Anal')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOg'),'Interoital')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMP1D')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPIV')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPIO')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPIO'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIBL')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXY')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIHQ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAUp')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAUp'),'Tests requested 1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAUp'),'Tests requested 2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAUp'),'Tests requested 3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAUp'),'Tests requested 4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAUp'),'Tests requested 5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAUp'),'Tests requested 6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAUp'),'Tests requested 7')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAUp'),'Tests requested 8')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXe')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOBs')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEMTf')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGRE1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIBT')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIBN')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDMS3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDMS2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOBv')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEMT1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDUTb')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDUTs')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDUTk')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDUTl')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDUTn')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDNS3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDUTm')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOBw')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDMS4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEMTd')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXM')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSDXV')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGIHA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSPRW')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZJRE5')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZJRE4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'Zmr22')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZEOBx')
-- 1.1 start
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPOj')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOj'),'Reason')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPOj'),'Text'))
-- 1.1 end

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_ReviewInvestigations', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating investigations record for review ' + cast(@p_review_number as varchar) + ' for pregnancy ' + cast(@p_pregnancy_number as varchar) + ' for patient ' + cast(@pointer as varchar), -1 )

/*
ZGRE6	AAT
ZSIXA	Abdominal xray
ZERB2	ABO antibodies
ZEOBa	Alanine Transaminase
ZGRE7	Albumin
ZGRE3	Alkaline phosphatase
ZGRE5	ALT
ZEOBc	Amino Acids
ZEOBd	Ammonia
ZJRE3	Anti nuclear factor
ZAATT	APPT
ZEOBe	APTT
ZEOBf	Aspartate Transaminase
ZGRE2	AST
ZDMS9	Autoantibodies
ZSIXb	Barium or gastrograffin enema
ZSIXa	Barium swallow
ZGIHF	Basophils
ZEOBg	Bicarbonate
ZEMT2	Blood culture
ZGIB5	Blood glucose
ZSDXJ	Blood pCO2 (max this period)
ZSDR1	Blood pCO2 (min this period)
ZSDXK	Blood pH
ZSDXI	Blood pO2 (max this period)
ZSDRO	Blood pO2 (min this period)
ZEBT1	C peptide
ZEMTC	C Reactive Protein
ZEOBh	C Reactive Protein
ZJRE1	C reactive protein
ZSUSC	Cardiac ultrasound scan
ZMPIN	Cervical smear
ZGIX1	Chest xray
ZEOBi	Chloride
ZEOBj	Chromosomes
ZEOBl	Copper
ZSUSN	Cranial ultrasound scan
ZDMS8	Creatinine clearance
ZSPr3	CSF culture
ZSDXa	Digoxin
ZGREA	Direct Bilirubin
ZERB6	Direct Coombs test
ZSIXc	DMSA scan
ZSDXb	Dobutamine
ZSDXc	Dopamine
ZSIXd	DTPA scan
ZGIN1	ECG
ZECG1	ECG (SV1 + RV5 - 6)
ZGIHE	Eosinophils
ZGIH9	ESR
ZEMTa	ET Aspirate Culture
ZDFO7	Fermoral arteriogram
ZGIHR	Fibrinogen
ZEOHb	Fibrinogen Degredation Product
ZEOHc	Film
ZGIBM	Free T3
ZGIBK	Free T4
ZDGI3	Fructosamine
ZSDXL	Gentamicin trough level
ZGRE4	GGT
ZGIH4	Haematocrit
ZEOHd	Hb Electrophoresis
ZGIBO	HbA1c
ZMPOh	HCG and results
ZEMTb	Hepatitis Screen
ZMPIS	Hepatitis-A screen
ZMPI5	Hepatitis-B screen
ZMPIR	Hepatitis-C screen
ZSUSa	Hip Ultrasound
ZEMTc	HIV Screen
ZEOBo	Immunoglobulins
ZAINR	INR
ZSDSA	Investigation 1
ZSDSJ	Investigation 10
ZSDSB	Investigation 2
ZSDSC	Investigation 3
ZSDSD	Investigation 4
ZSDSE	Investigation 5
ZSDSF	Investigation 6
ZSDSG	Investigation 7
ZSDSH	Investigation 8
ZSDSI	Investigation 9
ZEOBn	Lactic Acid or Lactate
ZGIHG	Leucocytes
ZGIHC	Lymphocytes
ZEOBp	Magnesium
ZSIXe	MAGscan
ZGIH7	MCH
ZGIH8	MCHC
ZGIH6	MCV
ZTRE2	Microsomal autoantibodies
ZSIXf	Micturating cystogram
ZGIHD	Monocytes
ZSDXX	Netilmicin peak level
ZSDXW	Netilmicin trough level
ZGIHB	Neutrophils
ZGIBS	OGTT 0 hour glucose
ZGIBR	OGTT 2 hour glucose
ZERB5	Other blood group antibodies
ZSDXH	Oxygen saturation
ZGIHP	PCV
ZSDXN	Phenobarbitone level
ZSDXd	Phenytoin
ZGIH3	Platelets
ZEOBq	Progesterone(17-OH)
ZEOBr	Protein (blood total)
ZAPTT	Prothrombin time
ZGIH5	Red cell count
ZSUSR	Renal ultrasound scan
ZERB3	Rhesus blood group
ZJRE2	Rheumatoid factor
ZMP77	Rubella antibody screen
ZGIB7	Serum calcium
ZGIBG	Serum cholesterol
ZGIB6	Serum creatinine
ZGIBA	Serum electrophoresis
ZGIBP	Serum HDL
ZGIBB	Serum IgA
ZRRE1	Serum IgE
ZGIB9	Serum IgG
ZGIBC	Serum IgM
ZGIBQ	Serum LDL
ZGIB8	Serum phosphate
ZGIB3	Serum potassium
ZGIB2	Serum sodium
ZGIBJ	Serum total thyroxine
ZGIBH	Serum triglycerides
ZGIB4	Serum urea
ZGIUR	Serum uric acid
ZEOHg	Sickle Screen
ZSIXg	Skeletal survey
ZSIXS	Skull xray
ZSUSc	Spine Ultrasound
ZEMTe	Swab Culture
ZMP1D	Syphilis screen
ZGIBL	T3 (triiodothyronine)
ZSDXY	TcPO2
ZGIHQ	TCT
ZMAUp	Tests requested
ZSDXe	Theophyline or Caffeine
ZEOBs	Thyroid function tests
ZEMTf	TORCH Screen
ZGRE1	Total Bilirubin
ZGIBT	Total Chol/HDLC ratio
ZGIBN	TSH
ZDMS3	Urine albumin excretion rate
ZDMS2	Urine albumin/creatinine ratio
ZEOBv	Urine Amino Acids
ZEMT1	Urine culture
ZDUTb	Urine dipstick blood
ZDUTs	Urine dipstick glucose
ZDUTk	Urine dipstick ketones
ZDUTl	Urine dipstick leucocytes
ZDUTn	Urine dipstick nitrites
ZDNS3	Urine dipstick protein
ZDUTm	Urine microalbuminuria
ZEOBw	Urine Organic acids
ZDMS4	Urine protein - 24 hour
ZEMTd	Urine Reducing Substances
ZSDXM	Vancomycin peak level
ZSDXV	Vancomycin trough level
ZGIHA	White cell count
ZJRE5	Xray exam of feet
ZJRE4	Xray exam of hands
ZEOBx	Zinc
*/
		END

	            FETCH NEXT FROM get_investigations INTO @NOTENO
	END

	CLOSE get_investigations
	DEALLOCATE get_investigations
END
