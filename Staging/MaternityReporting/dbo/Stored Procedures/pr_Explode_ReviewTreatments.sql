﻿/*
**************************************************************************************************************************
**	NAME:		pr_Explode_ReviewTreatments						**
**	DESCRIPTION:	Runs the New Review Treatments 'explode' routine			**
**	AUTHOR:	Stephen Creek								**
**	VERSIONS:	1.0	Stephen Creek	08/11/2007	Initial version created		**
**			1.1	Stephen Creek	07/02/2008	Corrected use of len() function	**
**			1.2	Stephen Creek	11/02/2008	Added Pregnancy_ID		**
**			1.3	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**											Noteno has changed from a 5 byte binary coded structured key
**											to a 25 byte (5 x 5 bytes) ASCII coded key.
**************************************************************************************************************************
*/
CREATE PROCEDURE [dbo].[pr_Explode_ReviewTreatments]
	(@pointer int
	,@p_pregnancy_id int --v1.2
	,@p_pregnancy_number varchar(30)
	,@p_review_number int
	,@p_noteno VARCHAR(25))
AS
BEGIN
	DECLARE @NOTENO  VARCHAR(25)
	DECLARE @SQLError INT

	DECLARE get_treatments CURSOR FOR
	SELECT Noteno
	FROM tbl_Notes_Copy
	WHERE Code = 'ZTSOG' -- Review Treatments
	AND Pointer = @pointer
	AND substring(Noteno,1,len('x'+@p_noteno+'x')-2) = @p_noteno -- v1.1
	ORDER BY Noteno

	OPEN get_treatments
	FETCH next from get_treatments into @NOTENO

	WHILE @@fetch_status = 0
	BEGIN
		--v1.3 SET @NOTENO = SUBSTRING(@NOTENO,1,4)
		SET @NOTENO = SUBSTRING(@NOTENO,1,20)

		INSERT INTO [tbl_ReviewTreatments] 
            			(Patient_Pointer,
			Pregnancy_ID,  --v1.2
            			[Pregnancy number],
            			Review_Number,
			[Antibiotics today],
			[Anticoagulant],
			[Anticonvulsants today],
			[Anti-D],
			[Anti-D Date],
			[Anti-D Time],
			[Anti-D Dose],
			[Anti-D Batch],
			[Anti-D Text],
			[Anti-D accepted],
			[Anti-D accepted Date administered],
			[Anti-D accepted Time administered],
			[Anti-D accepted Dose administered],
			[Anti-D accepted Batch],
			[Anti-D accepted Reason anti-D not given],
			[Anti-D offered],
			[Anti-D offered Reason],
			[Base corrections given today],
			[Diabetes treatment this review],
			[Inotropic support given today],
			[Insulin 1],
			[Insulin 2],
			[Insulin 3],
			[Intrapartum IV Fluids given],
			[Intrapartum IV Fluids given Dextrose 5],
			[Intrapartum IV Fluids given Dextrose 10],
			[Intrapartum IV Fluids given Normal Saline],
			[Intrapartum IV Fluids given Hartmanns],
			[Intrapartum IV Fluids given Other],
			[Intrapartum Pain Relief given],
			[Intrapartum Pain Relief given Self_admin_inhalation],
			[Intrapartum Pain Relief given Non-opioid analgesia],
			[Intrapartum Pain Relief given Pethidine],
			[Intrapartum Pain Relief given Diamorphine],
			[Intrapartum Pain Relief given Epidural Marcaine],
			[Intrapartum Pain Relief given Other],
			[Kleihauer taken],
			[Kleihauer taken Text],
			[Muscle relaxants used today],
			[Other IV infusions],
			[Other IV infusions Blood],
			[Other IV infusions FFP],
			[Other IV infusions Platelets],
			[Other IV infusions Saline bolus],
			[Other IV infusions Cryoprecipitate],
			[Other IV infusions HAS 4.5%],
			[Other IV infusions HAS 20%],
			[Other IV infusions Text],
			[Pref. insulin delivery method],
			[Reason Anti-D not given],
			[Sedatives today],
			[Surfactant used today],
			[Surfactant used today Doses],
			[Syntocinon],
			[Thyroid replacement],
			[Treatment X],
			[Treatment Y],
			[Treatment Z],
			[Treatment 1],
			[Treatment 10],
			[Treatment 11],
			[Treatment 12],
			[Treatment 13],
			[Treatment 14],
			[Treatment 15],
			[Treatment 16],
			[Treatment 17],
			[Treatment 18],
			[Treatment 19],
			[Treatment 2],
			[Treatment 20],
			[Treatment 21],
			[Treatment 22],
			[Treatment 23],
			[Treatment 24],
			[Treatment 25],
			[Treatment 26],
			[Treatment 27],
			[Treatment 28],
			[Treatment 29],
			[Treatment 3],
			[Treatment 30],
			[Treatment 4],
			[Treatment 5],
			[Treatment 6],
			[Treatment 7],
			[Treatment 8],
			[Treatment 9])
		VALUES
            			(@pointer
            			,@p_pregnancy_id --v1.2
            			,@p_pregnancy_number
            			,@p_review_number
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSTR1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERXP')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSTR2')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPEq')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPEq'),'Date')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPEq'),'Time')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPEq'),'Dose')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPEq'),'Batch')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPEq'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPEb')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPEb'),'Date administered')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPEb'),'Time administered')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPEb'),'Dose administered')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPEb'),'Batch')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPEb'),'Reason anti-D not given')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPEa')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPEa'),'Reason')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSTR7')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDRXA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSTR4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERXM')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERXN')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERXO')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZPARL')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZPARL'),'Dextrose 5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZPARL'),'Dextrose 10')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZPARL'),'Normal Saline')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZPARL'),'Hartmanns')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZPARL'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZPARM')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZPARM'),'Self_admin_inhalation')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZPARM'),'Non-opioid analgesia')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZPARM'),'Pethidine')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZPARM'),'Diamorphine')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZPARM'),'Epidural Marcaine')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZPARM'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPEp')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMPEp'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSTR6')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSCLB')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSCLB'),'Blood')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSCLB'),'FFP')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSCLB'),'Platelets')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSCLB'),'Saline bolus')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSCLB'),'Cryoprecipitate')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSCLB'),'HAS 4.5%')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSCLB'),'HAS 20%')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSCLB'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDRXR')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMPeq')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSTR8')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZSCL4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZSCL4'),'Doses')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZPARK')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERXQ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERTO')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERTR')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERTZ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERXA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERXJ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERXK')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERXL')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERXR')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERXS')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERXT')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERYA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERYB')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERYC')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERYD')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERXB')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERYE')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERYF')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERYG')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERYH')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERYI')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERYJ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERYK')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERYL')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERYM')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERYN')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERXC')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERYO')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERXD')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERXE')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERXF')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERXG')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERXH')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZERXI'))

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_ReviewTreatments', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating treatments record for review ' + cast(@p_review_number as varchar) + ' for pregnancy ' + cast(@p_pregnancy_number as varchar) + ' for patient ' + cast(@pointer as varchar), -1 )
		END

	            FETCH NEXT FROM get_treatments INTO @NOTENO
	END

	CLOSE get_treatments
	DEALLOCATE get_treatments
END
