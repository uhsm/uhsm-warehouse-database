﻿/*
**********************************************************************************************************************************************
**	NAME:		pr_Explode_SocialHistory								**
**	DESCRIPTION:	Runs the New Social History 'explode' routine						**
**	AUTHOR:	Stephen Creek										**
**	VERSIONS:	1.0	Stephen Creek	01/10/2007	Initial version created based on orinal version	**
**				1.1	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**												Noteno has changed from a 5 byte binary coded structured key
**												to a 25 byte (5 x 5 bytes) ASCII coded key.
**********************************************************************************************************************************************
*/
CREATE PROCEDURE [dbo].[pr_Explode_SocialHistory]
AS
BEGIN
	DECLARE @pointer INT
	DECLARE @NOTENO  VARCHAR(25)
	DECLARE @SQLError INT

	DECLARE get_SocialHistory CURSOR FOR
	SELECT Pointer,Noteno
 	FROM tbl_Notes_Copy
	WHERE Code = 'ZGSOZ'

	TRUNCATE TABLE [tbl_SocialHistory]

	OPEN get_SocialHistory
	FETCH NEXT FROM get_SocialHistory INTO
	@pointer,@NOTENO

	DECLARE @RowCount INT
	SET @RowCount = 0

	WHILE @@fetch_status = 0
	BEGIN
		--v1.1 SET @NOTENO = SUBSTRING(@NOTENO,1,1)
		SET @NOTENO = SUBSTRING(@NOTENO,1,5)


		INSERT INTO [tbl_SocialHistory] 
			(Patient_Pointer,
			[Language ability],
			[Language ability Interpreter required],
			[Language ability Native tongue],
			[Mobility],
			[Mobility Text],
			[Faith/Religion],
			[Ethnic category],
			[Cooking facilities],
			[Cooking facilities Text],
			[Cooking skills],
			[Cooking skills Text],
			[Motor Vehicle driver],
			[Motor Vehicle driver Category],
			[Occupational Status],
			[Occupational Status Occupation],
			[Occupational Status Shift worker],
			[Occupational Status Text],
			[Special dietary requirements],
			[Special dietary requirements Text],
			[Meals taken outside the home],
			[Meals taken outside the home Eats out],
			[Meals taken outside the home Meals on wheels],
			[Meals taken outside the home Day centre],
			[Meals taken outside the home Luncheon clubs],
			[Meals taken outside the home Family visits],
			[Meals taken outside the home Text],
			[DVLA informed],
			[Shopping capabilities],
			[Shopping capabilities Family],
			[Shopping capabilities Home carer],
			[Shopping capabilities Neighbour],
			[Shopping capabilities Text],
			[Health visitor],
			[Health visitor Phone],
			[Health visitor Address 1],
			[Health visitor Address 2],
			[Health visitor Address 3],
			[Health visitor Address 4],
			[Country of origin])
		VALUES
			(@pointer
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGSOC')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGSOC'),'Interpreter required')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGSOC'),'Native tongue')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDD0B')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDD0B'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGSO3')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGSOE')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDD03')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDD03'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDD04')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDD04'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZNSDe')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZNSDe'),'Category')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGSO6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGSO6'),'Occupation')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGSO6'),'Shift worker')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZGSO6'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDD27')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDD27'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDD05')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDD05'),'Eats out')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDD05'),'Meals on wheels')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDD05'),'Day centre')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDD05'),'Luncheon clubs')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDD05'),'Family visits')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDD05'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZNSDf')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDD06')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDD06'),'Family')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDD06'),'Home carer')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDD06'),'Neighbour')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDD06'),'Text')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZDD07')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDD07'),'Phone')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDD07'),'Address 1')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDD07'),'Address 2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDD07'),'Address 3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZDD07'),'Address 4')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZGSco'))

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_SocialHistory', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating social history record for patient ' + cast(@pointer as varchar), -1 )
		END
		ELSE
			SET @RowCount = @RowCount + 1

		FETCH next from get_SocialHistory into
		@pointer,@NOTENO
	END
	CLOSE get_SocialHistory
	DEALLOCATE get_SocialHistory

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	VALUES ( getdate(), 'pr_Explode_SocialHistory', 'tbl_SocialHistory records created', @RowCount )
END
