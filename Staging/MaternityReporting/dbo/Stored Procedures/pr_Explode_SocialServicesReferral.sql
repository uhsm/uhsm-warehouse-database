﻿/*
**************************************************************************************************************************
**	NAME:		pr_Explode_SocialServicesReferral					**
**	DESCRIPTION:	Runs the New SocialServicesreferral 'explode' routine			**
**	AUTHOR:	Stephen Creek								**
**	VERSIONS:	1.0	Stephen Creek	07/11/2007	Initial version created		**
**			1.1	Stephen Creek	07/02/2008	Corrected use of len() function	**
**			1.2	Stephen Creek	11/02/2007	Added Pregnancy_ID		**
**			1.3	Keith Lawrence	04/08/2009	tbl_Notes_Copy.Noteno has increased from (5) to (25)
**											Noteno has changed from a 5 byte binary coded structured key
**											to a 25 byte (5 x 5 bytes) ASCII coded key.
**************************************************************************************************************************
*/
CREATE PROCEDURE [dbo].[pr_Explode_SocialServicesReferral]
	(@pointer int
	,@p_pregnancy_id int --v1.2
	,@p_pregnancy_number varchar(30)
	,@p_noteno VARCHAR(25))
AS
BEGIN
	DECLARE @NOTENO  VARCHAR(25)
	DECLARE @SQLError INT

	SET @NOTENO = (SELECT Noteno
	FROM tbl_Notes_Copy
	WHERE Code = 'ZMH17' -- Social Services Referral
	AND Pointer = @pointer
	AND substring(Noteno,1,len('x'+@p_noteno+'x')-2) = @p_noteno) --v1.1

	-- v1.3 SET @NOTENO = SUBSTRING(@NOTENO,1,2)
	SET @NOTENO = SUBSTRING(@NOTENO,1,10)

	INSERT INTO [tbl_SocialServicesReferral] 
           		(Patient_Pointer,
		Pregnancy_ID, --v1.2
 		[Pregnancy number],
		[Additional reason for ref 1],
		[Additional reason for ref 1 Text],
		[Additional reason for ref 2],
		[Additional reason for ref 2 Text],
		[Additional reason for ref 3],
		[Additional reason for ref 3 Text],
		[Antenatal issues],
		[Antenatal issues Text],
		[Case conference],
		[Case conference Text],
		[Case discussion],
		[Case discussion Text],
		[Child protection issue],
		[Child protection issue Issue 1],
		[Child protection issue Issue 2],
		[Child protection issue Issue 3],
		[Child protection issue Text],
		[Child protection officer],
		[Child protection officer Contact Number],
		[Child protection outcome],
		[Child protection outcome Text],
		[Consent obtained],
		[Date of referral],
		[Delivery issues],
		[Delivery issues Text],
		[Further comments],
		[Further comments Text],
		[Name of staff],
		[Name of staff Grade],
		[Named community midwife],
		[Named community midwife Contact Number],
		[Named health visitor],
		[Named health visitor Contact Number],
		[Named social worker],
		[Named social worker Contact Number],
		[Other named professional],
		[Other named professional Contact Number],
		[Postnatal issues],
		[Postnatal issues Text],
		[Protection case referred by],
		[Reason for soc services ref],
		[Reason for soc services ref Text],
		[Specific protection action],
		[Specific protection action Text],
		[Time of referral],
		[Type of staff])
	VALUES
            		(@pointer
            		,@p_pregnancy_id --v1.2
            		,@p_pregnancy_number
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSSS')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMSSS'),'Text')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSST')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMSST'),'Text')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSSU')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMSSU'),'Text')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSSX')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMSSX'),'Text')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSSW')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMSSW'),'Text')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSSV')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMSSV'),'Text')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX92')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX92'),'Issue 1')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX92'),'Issue 2')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX92'),'Issue 3')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX92'),'Text')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX99')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX99'),'Contact Number')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX97')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX97'),'Text')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSRI')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSRG')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSSY')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMSSY'),'Text')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX00')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX00'),'Text')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSSb')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMSSb'),'Grade')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX96')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX96'),'Contact Number')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX90')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX90'),'Contact Number')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX94')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX94'),'Contact Number')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX91')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX91'),'Contact Number')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSSZ')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMSSZ'),'Text')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX98')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSSR')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMSSR'),'Text')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMX93')
		,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMX93'),'Text')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSRH')
		,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMSSa'))

	SET @SQLError = @@error
	IF @SQLError > 0
	BEGIN
		INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
		VALUES ( getdate(), 'pr_Explode_SocialServicesReferral', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating social services referral record for pregnancy ' + cast(@p_pregnancy_number as varchar) + ' for patient ' + cast(@pointer as varchar), -1 )
	END

END
