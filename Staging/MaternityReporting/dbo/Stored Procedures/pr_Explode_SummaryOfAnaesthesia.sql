﻿
/*
**************************************************************************************************************************
**	NAME:		pr_Explode_SummaryOfAnaesthesia					**
**	DESCRIPTION:	Runs the Pregnancy / Summmary of Anaesthesia 'explode' routine		**
**	AUTHOR:	Stephen Creek								**
**	VERSIONS:	1.0	Stephen Creek	28/02/2008	Initial version created		**
**************************************************************************************************************************
*/
CREATE PROCEDURE [dbo].[pr_Explode_SummaryOfAnaesthesia]
	(@pointer int
	,@p_pregnancy_id int 
	,@p_pregnancy_number varchar(30)
	,@p_noteno varchar(5))
AS
BEGIN
	DECLARE @NOTENO  VARCHAR(5)
	DECLARE @SQLError INT

	SELECT @NOTENO = Noteno
	FROM tbl_Notes_Copy
	WHERE Code = 'ZMANT' -- Summary of Anaesthesia
	AND Pointer = @pointer
	AND substring(Noteno,1,len('x'+@p_noteno+'x')-2) = @p_noteno 
	ORDER BY Noteno

	IF @NOTENO IS NOT NULL
	BEGIN
		SET @NOTENO = SUBSTRING(@NOTENO,1,2)

		INSERT INTO [tbl_SummaryOfAnaesthesia] 
			(Patient_Pointer,
			Pregnancy_ID, 
			[Pregnancy number],
			[Acute comp.with Spinal],
			[Acute comp.with Spinal Hypotension],
			[Acute comp.with Spinal Inadequate block],
			[Acute comp.with Spinal CSF not found],
			[Acute complications with GA],
			[Acute complications with GA Difficult Intubation],
			[Acute complications with GA Drug Reaction],
			[Acute complications with GA Hypotension],
			[Acute complications with GA Other],
			[Additional comments],
			[Additional comments Comments],
			[Anaesthetists assessment],
			[Any other anaesthetist present],
			[Any other anaesthetist present First anaesthetist],
			[Any other anaesthetist present Second anaesthetist],
			[Any other anaesthetist present Third anaesthetist],
			[Doctor or consultant anaes.],
			[Grade of principal anaes.],
			[Mothers perception],
			[Name of anaesthetic supervisor],
			[Name of anaesthetic supervisor Grade],
			[Name of principal anaesthetist],
			[Post op complications with GA],
			[Post op complications with GA Backache],
			[Post op complications with GA Nausea or Vomiting],
			[Post op complications with GA Drug Reaction],
			[Post op complications with GA Other],
			[Post op comps with Epidural],
			[Post op comps with Epidural Headache],
			[Post op comps with Epidural Motor deficit],
			[Post op comps with Epidural Sensory deficit],
			[Post op comps with Epidural Backache],
			[Post op comps with Epidural Urinary Retension],
			[Post op comps with Epidural Itching],
			[Post op comps with Epidural Other],
			[Post op comps with Spinal],
			[Post op comps with Spinal Neurological deficit],
			[Post op comps with Spinal Urinary retension],
			[Post op comps with Spinal Headache],
			[Post op comps with Spinal Itching],
			[Post op comps with Spinal Blood patch],
			[Post op comps with Spinal Other],
			[Problems in situ],
			[Problems in situ Missed segment],
			[Problems in situ Unilateral block],
			[Problems in situ Perineal pain],
			[Problems in situ Hypotension],
			[Problems in situ Shivering],
			[Problems in situ Itching],
			[Problems in situ Other],
			[Problems with catheter removal],
			[Problems with catheter removal Details],
			[Problems with insertion],
			[Problems with insertion Missed location],
			[Problems with insertion Catheter insertion],
			[Problems with insertion Blood in catheter/needle],
			[Problems with insertion CSF in catheter/needle],
			[Problems with insertion Equipment failure],
			[Problems with insertion Other problems],
			[Reason for anaesthesia],
			[Was anaesthetist supervised],
			[Was epidural anaesthesia used],
			[Was general anaesthesia used],
			[Was spinal anaesthesia used]
			)
		VALUES
			(@pointer
			,@p_pregnancy_id  
			,@p_pregnancy_number
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAN6')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN6'),'Hypotension')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN6'),'Inadequate block')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN6'),'CSF not found')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAN8')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN8'),'Difficult Intubation')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN8'),'Drug Reaction')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN8'),'Hypotension')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN8'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMANG')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMANG'),'Comments')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMANF')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAND')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAND'),'First anaesthetist')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAND'),'Second anaesthetist')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAND'),'Third anaesthetist')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMANL')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMANK')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMANE')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMANC')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMANC'),'Grade')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMANA')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAN9')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN9'),'Backache')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN9'),'Nausea or Vomiting')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN9'),'Drug Reaction')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN9'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAN5')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN5'),'Headache')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN5'),'Motor deficit')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN5'),'Sensory deficit')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN5'),'Backache')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN5'),'Urinary Retension')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN5'),'Itching')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN5'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAN7')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN7'),'Neurological deficit')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN7'),'Urinary retension')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN7'),'Headache')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN7'),'Itching')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN7'),'Blood patch')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN7'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAN3')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN3'),'Missed segment')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN3'),'Unilateral block')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN3'),'Perineal pain')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN3'),'Hypotension')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN3'),'Shivering')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN3'),'Itching')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN3'),'Other')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAN4')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN4'),'Details')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAN2')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN2'),'Missed location')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN2'),'Catheter insertion')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN2'),'Blood in catheter/needle')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN2'),'CSF in catheter/needle')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN2'),'Equipment failure')
			,dbo.func_Extract_From_Attributes(dbo.func_Extract_Attributes(@pointer,@NOTENO,'ZMAN2'),'Other problems')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMAN1')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMANB')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMANH')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMANJ')
			,dbo.func_Extract_Value(@pointer,@NOTENO,'ZMANI')
			)

		SET @SQLError = @@error
		IF @SQLError > 0
		BEGIN
			INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
			VALUES ( getdate(), 'pr_Explode_SummaryOfAnesthesia', 'SQL Error ' + cast(@SQLError as varchar) + ' encountered creating anesthesia summary record for pregancy ' + @p_pregnancy_number + ' for patient ' + cast(@pointer as varchar), -1 )
		END

	END

END
