﻿




/*
**************************************************************************************************************************
**	NAME:		pr_ReplicateTables							**
**	DESCRIPTION:	Replicates the Register and Notes tables from the Staging database	**
**	AUTHOR:	Stephen Creek								**
**	VERSIONS:	1.0	Stephen Creek	18/10/2007	Initial version created		**
**			1.1	Stephen Creek	23/10/2007	Added event logging		**
**************************************************************************************************************************
*/
CREATE PROCEDURE [dbo].[pr_ReplicateTables] AS
BEGIN
	DECLARE @RowCount INT

	-- Notes

	IF EXISTS (SELECT 1 FROM sysindexes WHERE name = 'Idx_Notes_1')
	EXEC('DROP INDEX tbl_Notes_Copy.Idx_Notes_1')

	BEGIN TRAN NOTESRECREATE

		TRUNCATE TABLE tbl_Notes_Copy

		INSERT INTO tbl_Notes_Copy
		SELECT * FROM MaternityStaging.dbo.Notes

		SET @RowCount = @@RowCount

		INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
		VALUES ( getdate(), 'pr_ReplicateTables', 'Replicated Notes Table', @RowCount )


	COMMIT TRAN NOTESRECREATE

	EXEC('CREATE CLUSTERED INDEX [Idx_Notes_1] ON [dbo].[tbl_Notes_Copy] ([Code], [Pointer], [Noteno]) ON [PRIMARY]')

	-- Register

	IF EXISTS (SELECT 1 FROM sysindexes WHERE name = 'Idx_Register_1')
	EXEC('DROP INDEX tbl_Register_Copy.Idx_Register_1')

	BEGIN TRAN REGISTERRECREATE

		TRUNCATE TABLE tbl_Register_Copy

		INSERT INTO tbl_Register_Copy
		SELECT * FROM MaternityStaging.dbo.Register

		SET @RowCount = @@RowCount

		INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
		VALUES ( getdate(), 'pr_ReplicateTables', 'Replicated Register Table', @RowCount )

	COMMIT TRAN REGISTERRECREATE

	EXEC('CREATE CLUSTERED INDEX [Idx_Register_1] ON [dbo].[tbl_Register_Copy] ([Pointer], [Regtype]) ON [PRIMARY]')

	-- Stafflink

	IF EXISTS (SELECT 1 FROM sysindexes WHERE name = 'Idx_Stafflink_1')
	EXEC('DROP INDEX tbl_Stafflink_Copy.Idx_Stafflink_1')

	BEGIN TRAN STAFFLINKCREATE

		TRUNCATE TABLE tbl_Stafflink_Copy

		INSERT INTO tbl_Stafflink_Copy
		SELECT * FROM MaternityStaging.dbo.Stafflink

		SET @RowCount = @@RowCount

		INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
		VALUES ( getdate(), 'pr_ReplicateTables', 'Replicated Stafflink Table', @RowCount )

	COMMIT TRAN STAFFLINKCREATE

	EXEC('CREATE CLUSTERED INDEX [Idx_Stafflink_1] ON [dbo].[tbl_Stafflink_Copy] ([Linkid]) ON [PRIMARY]')
END

