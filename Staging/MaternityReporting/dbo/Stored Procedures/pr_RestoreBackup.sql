﻿CREATE procedure [dbo].[pr_RestoreBackup] as 
DECLARE @UnzippedFileLoc AS VARCHAR(150)
DECLARE @DataFileLoc AS VARCHAR(200)

DECLARE @BakFile AS VARCHAR(150)
DECLARE @LogFile AS VARCHAR(150)
DECLARE @BakFileDate AS VARCHAR(15)

DECLARE @FullPath AS VARCHAR(500)
DECLARE @Counter AS INT

DECLARE @MDFPath AS VARCHAR(150)
DECLARE @NDFPathA AS VARCHAR(150)
DECLARE @NDFPathB AS VARCHAR(150)
DECLARE @NDFPathC AS VARCHAR(150)
DECLARE @LDFPath AS VARCHAR(150)

DECLARE @MDFLogicalName AS VARCHAR(150)
DECLARE @NDFLogicalNameA AS VARCHAR(150)
DECLARE @NDFLogicalNameB AS VARCHAR(150)
DECLARE @NDFLogicalNameC AS VARCHAR(150)
DECLARE @LDFLogicalName AS VARCHAR(150)

DECLARE @SQLError INT
SET @SQLError = -1

-- Retrieve file locations from parameters table
SELECT @UnzippedFileLoc = Value FROM tbl_SiteSpecificParameters WHERE ParameterName = 'File_Location_Unzip'
SELECT @DataFileLoc = Value FROM tbl_SiteSpecificParameters WHERE ParameterName = 'File_Location_Database'

CREATE TABLE #TEMP(Fname VARCHAR(200), Lvl INT, IsFile INT, FileDate VARCHAR(20))

-- Retrieve the list of files from the unzip location
INSERT INTO #TEMP ( Fname, Lvl, IsFile )
EXEC master.dbo.xp_dirtree @UnzippedFileLoc , 1 , 1

-- Ignore anything that is not a .bak or .log file
DELETE FROM #TEMP
WHERE (IsFile = 0) OR (RIGHT(Fname,4) <> '.bak' AND RIGHT(Fname,4) <> '.log')

-- Fetch the date/time
UPDATE #TEMP SET 
FileDate = REPLACE(SUBSTRING(Fname,CHARINDEX('.',Fname)-14,14),'_','')



-- Pick the latest .bak file
SET @BakFile = (SELECT TOP 1 Fname FROM #TEMP WHERE RIGHT(Fname,4) = '.bak' ORDER BY FileDate Desc)
SELECT @BakFileDate = FileDate FROM #TEMP WHERE Fname = @BakFile

-- Restore the .bak file
IF @BakFile IS NULL 
BEGIN
	IF substring(replace(@@version,'  ',' ') , 1 , 25) = 'Microsoft SQL Server 2000' 
	BEGIN
		-- If this version is 2000, having no .bak file is OK as logfiles can just be restored
		INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
		VALUES ( getdate(), 'pr_RestoreBackup', 'No .bak files(s) present to restore', 1 )
	END
	ELSE
	BEGIN
		-- If this version is not 2000, a .bak file is required
		INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
		VALUES ( getdate(), 'pr_RestoreBackup', 'A .bak file must be supplied to restore onto a non-2000 database', 1 )
		GOTO SkipLabel
	END
END
ELSE
BEGIN
	PRINT 'Bakfile: ' + @BakFile
	PRINT 'Bakfiledate: ' + @BakFileDate

	-- Ignore any files that are before the date of the .bak file
	DELETE FROM #TEMP 
	WHERE  FileDate < @BakFileDate

	SET @FullPath = @UnzippedFileLoc + @BakFile
	SET @MDFPath = @DataFileLoc + 'MaternityStaging_Data.MDF'
	SET @NDFPathA = @DataFileLoc + 'MaternityStaging_AuditTrail.NDF'
	SET @NDFPathB = @DataFileLoc + 'MaternityStaging_DDict.NDF'
	SET @NDFPathC = @DataFileLoc + 'MaternityStaging_Queue.NDF'
	SET @LDFPath = @DataFileLoc + 'MaternityStaging_Log.LDF'



	-- Retrieve the logical filenames from the backup files.  This was added because
	-- logical filenames were different bettwen the live and deployment environments
	IF substring(replace(@@version,'  ',' ') , 1 , 25) = 'Microsoft SQL Server 2000' 
	BEGIN
		CREATE TABLE #RestoreFileListOnly2000
		( 
		  LogicalName VARCHAR(512), 
		  PhysicalName VARCHAR(512), 
		  FileType VARCHAR(8), 
		  FileGroupName VARCHAR(256), 
		  FileSize NUMERIC(20,0), 
		  FileMaxSize NUMERIC(20,0)
		)

		INSERT INTO #RestoreFileListOnly2000
		EXEC('RESTORE FILELISTONLY FROM DISK = '''+@FullPath+'''')
	
		SELECT @MDFLogicalName = LogicalName FROM #RestoreFileListOnly2000 WHERE PhysicalName LIKE '%.mdf' AND LogicalName LIKE '%Data%'
		SELECT @NDFLogicalNameA = LogicalName FROM #RestoreFileListOnly2000 WHERE FileGroupName = 'AuditTrail'
		SELECT @NDFLogicalNameB = LogicalName FROM #RestoreFileListOnly2000 WHERE FileGroupName = 'DDict'
		SELECT @NDFLogicalNameC = LogicalName FROM #RestoreFileListOnly2000 WHERE FileGroupName = 'Queue'
		SELECT @LDFLogicalName = LogicalName FROM #RestoreFileListOnly2000 WHERE FileType = 'L'

		DROP TABLE #RestoreFileListOnly2000

	END
	ELSE
	BEGIN
		CREATE TABLE #RestoreFileListOnly2005
		( 
		  LogicalName VARCHAR(512), 
		  PhysicalName VARCHAR(512), 
		  FileType VARCHAR(8), 
		  FileGroupName VARCHAR(256), 
		  FileSize NUMERIC(30,0), 
		  FileMaxSize NUMERIC(20,0), 
		  FileID NUMERIC(20,0), 
		  CreateLSN NUMERIC(20,0), 
		  DropLSN NUMERIC(20,0), 
		  UniqueID VARCHAR(50), 
		  ReadOnlyLSN NUMERIC(20,0), 
		  ReadWriteLSN NUMERIC(20,0), 
		  BackupSizeInBytes NUMERIC(20,0), 
		  SourceBlockSize NUMERIC(20,0), 
		  FileGroupID NUMERIC(20,0), 
		  LogGroupGUID VARCHAR(50), 
		  DifferentialBaseLSN NUMERIC(30,0), 
		  DifferentialBaseGUID VARCHAR(50), 
		  IsReadOnly NUMERIC(20,0), 
		  IsPresent NUMERIC(20,0),
		  TDEThumbprint varchar(200)
		)

		INSERT INTO #RestoreFileListOnly2005
		EXEC('RESTORE FILELISTONLY FROM DISK = '''+@FullPath+'''')
	
		SELECT @MDFLogicalName = LogicalName FROM #RestoreFileListOnly2005 WHERE PhysicalName LIKE '%.mdf' AND LogicalName LIKE '%Data%'
		SELECT @NDFLogicalNameA = LogicalName FROM #RestoreFileListOnly2005 WHERE FileGroupName = 'AuditTrail'
		SELECT @NDFLogicalNameB = LogicalName FROM #RestoreFileListOnly2005 WHERE FileGroupName = 'DDict'
		SELECT @NDFLogicalNameC = LogicalName FROM #RestoreFileListOnly2005 WHERE FileGroupName = 'Queue'
		SELECT @LDFLogicalName = LogicalName FROM #RestoreFileListOnly2005 WHERE FileType = 'L'

		DROP TABLE #RestoreFileListOnly2005

	END

	-- Log file names to Event Log
	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	VALUES ( getdate(), 'pr_RestoreBackup', 'Data File "'+@MDFLogicalName+'" --> ' + @MDFPath , 0 )

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	VALUES ( getdate(), 'pr_RestoreBackup', 'AuditTrail "'+@NDFLogicalNameA + '" --> ' + @NDFPathA , 0 )

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	VALUES ( getdate(), 'pr_RestoreBackup', 'DDict "'+@NDFLogicalNameB + '" --> ' + @NDFPathB , 0 )

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	VALUES ( getdate(), 'pr_RestoreBackup', 'Queue "'+@NDFLogicalNameC + '" --> ' + @NDFPathC , 0 )

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	VALUES ( getdate(), 'pr_RestoreBackup', 'Log File "'+@LDFLogicalName + '" --> ' + @LDFPath , 0 )

	-- Ensure that the loginal filenames were retrieved correctly from the backup file
	IF 	@MDFLogicalName IS NULL OR @MDFLogicalName = '' OR
		@NDFLogicalNameA IS NULL OR @NDFLogicalNameA = '' OR
		@NDFLogicalNameB IS NULL OR @NDFLogicalNameB = '' OR
		@NDFLogicalNameC IS NULL OR @NDFLogicalNameC = '' OR
		@LDFLogicalName IS NULL OR @LDFLogicalName = ''
	BEGIN
		INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
		VALUES ( getdate(), 'pr_RestoreBackup', 'Error retrieving logical file names from backup files', 1 )
		GOTO SkipLabel
	END
	
	-- .bak file but no .log files
	RESTORE DATABASE MaternityStaging
	FROM DISK = @FullPath
	WITH NORECOVERY,
	MOVE @MDFLogicalName TO @MDFPath, REPLACE,
	MOVE @NDFLogicalNameA TO @NDFPathA,
	MOVE @NDFLogicalNameB TO @NDFPathB,
	MOVE @NDFLogicalNameC TO @NDFPathC,
	MOVE @LDFLogicalName TO @LDFPath

	SET @SQLError = @@Error

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	VALUES ( getdate(), 'pr_RestoreBackup', @FullPath, @SQLError )
END

DELETE FROM #TEMP WHERE Fname = @BakFile

-- Count how many .log files are present
SELECT @Counter = COUNT(*) FROM #TEMP
WHERE RIGHT(Fname,4) = '.log'

-- Skip the next section if there are no log files to process
IF @Counter <= 0 
BEGIN
	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	VALUES ( getdate(), 'pr_RestoreBackup', 'No .log files(s) present to restore', 1 )
END
BEGIN
	-- restore any .log files	
	WHILE @Counter > 0
	BEGIN
		SELECT @LogFile = Fname FROM #TEMP
		WHERE RIGHT(Fname,4) = '.log' AND FileDate = (SELECT MIN(FileDate) FROM #TEMP)

		SET @FullPath = @UnzippedFileLoc + @LogFile

		-- Restore Log File
		RESTORE LOG MaternityStaging
		FROM DISK = @FullPath
		WITH NORECOVERY

		SET @SQLError = @@Error

		INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
		VALUES ( getdate(), 'pr_RestoreBackup', @FullPath, @SQLError )
		
		DELETE FROM #TEMP WHERE Fname = @LogFile

		SET @Counter = @Counter - 1
	END
END

-- Finalise the database
IF substring(replace(@@version,'  ',' ') , 1 , 25) = 'Microsoft SQL Server 2000' 
BEGIN
	-- If this version is 2000 then the database can safely be restored in
	-- standby (read only) mode
	DECLARE @StandbyFile AS VARCHAR(500)
	SET @StandbyFile = @DataFileLoc + 'MaternityStaging_Standby.dat'
	RESTORE DATABASE MaternityStaging WITH STANDBY = @StandbyFile
END
ELSE
BEGIN
	-- If this version is not 2000, we'll need to do a full recovery
	-- to be able to access the data
	RESTORE DATABASE MaternityStaging WITH RECOVERY
END

SkipLabel:

DROP TABLE #TEMP

-- Finally, if the result was success, remove the unzipped files and archive the zipped ones
 --Also remove any Event Log entries over 7 days old
IF @SQLError = 0
BEGIN
	EXEC pr_DeleteUnzippedFiles

	DECLARE @NumDays INT
	SELECT @NumDays = Value FROM tbl_SiteSpecificParameters WHERE ParameterName = 'Delete_EventLog_Days'
	IF @NumDays IS NULL SET @NumDays = 7

	INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
	VALUES ( getdate(), 'pr_RestoreBackup', 'Deleting event log entries older than '+cast(@NumDays as varchar(5))+' days', 0 )
	DELETE FROM tbl_EventLog WHERE datediff(day,EventDateTime,getdate()) > @NumDays
END

