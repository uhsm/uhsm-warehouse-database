﻿

/*
************************************************************************************************************************************
**	NAME:		pr_RunEverything								**
**	DESCRIPTION:	Runs all the Replicate, and Explode procedures					**
**	AUTHOR:	Stephen Creek									**
**	VERSIONS:	1.0	Stephen Creek	08/11/2007	Initial version created			**
**			1.1	Stephen Creek	30/11/2007	Updated with new procedure names	**
************************************************************************************************************************************
*/

CREATE procedure [dbo].[pr_RunEverything]
AS

SET LANGUAGE 'British'

SET NOCOUNT ON

-- Copy the tables from the source database
EXEC dbo.pr_ReplicateTables

-- Explode the tables
EXEC dbo.pr_Explode_All

