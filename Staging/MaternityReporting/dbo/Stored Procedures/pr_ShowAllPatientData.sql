﻿

CREATE PROCEDURE dbo.pr_ShowAllPatientData ( @p_pointer INT )
AS
SELECT 
	N.Pointer, dbo.func_ShowNoteno(Noteno), N.Code, C.Term30, N.Nvalue, 
	CASE 
		WHEN LEN(Nvalue)=6 and IsNumeric(Nvalue)=1
		THEN dbo.func_Convert_Date(Nvalue) 
		ELSE NULL 
	END DateValue,
	len(N.Nvalue), 
	len(N.Attributes),
	N.Attributes
FROM
	tbl_Notes_Copy N
JOIN MaternityStaging.dbo.Codes C on C.Rc5code = N.Code and C.Termcode = '00'
WHERE 
	Pointer = @p_pointer
ORDER BY 
	N.Noteno

