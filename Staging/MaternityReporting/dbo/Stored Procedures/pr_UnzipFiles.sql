﻿

/*
************************************************************************************************************************************
**	NAME:		pr_UnzipFiles									**
**	DESCRIPTION:	Unzips any files present in the specified location					**
**	AUTHOR:	Stephen Creek									**
**	VERSIONS:	1.0	Stephen Creek	18/10/2007	Initial version created			**
**			1.1	Stephen Creek	22/10/2007	Added event logging			**
**			1.2	Stephen Creek	01/11/2007	Added archive of zip files on success	**
************************************************************************************************************************************
*/
CREATE    procedure [dbo].[pr_UnzipFiles]
AS
BEGIN
	DECLARE @RawZipFileLoc AS VARCHAR(150)
	DECLARE @FileName AS VARCHAR(200)
	DECLARE @UnzippedFileLoc AS VARCHAR(150)
	DECLARE @FullPath AS VARCHAR(500)
	DECLARE @UnzipUtility AS VARCHAR(150)
	DECLARE @UnzipParameters AS VARCHAR(150)

	SELECT @RawZipFileLoc = Value FROM tbl_SiteSpecificParameters WHERE ParameterName = 'File_Location_Zip'
	SELECT @UnzippedFileLoc = Value FROM tbl_SiteSpecificParameters WHERE ParameterName = 'File_Location_Unzip'

	SELECT @UnzipUtility = Value FROM tbl_SiteSpecificParameters WHERE ParameterName = 'Unzip_Utility_Path'
	SELECT @UnzipParameters = Value FROM tbl_SiteSpecificParameters WHERE ParameterName = 'Unzip_Utility_Parameters'

	-- Skip this procedure if no Zip utility is specified
	IF rtrim(isnull(@UnzipUtility,'')) = '' 
	BEGIN
		INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
		VALUES ( getdate(), 'pr_UnzipFiles', 'No Zip File Utility Specified in Parameters', 1 )
		GOTO SkipLabel
	END

	-- Remove any previously unzipped files
	EXEC pr_DeleteUnzippedFiles

	CREATE TABLE #TEMP(Fname VARCHAR(200), Lvl INT, IsFile INT)

	-- Fetch the list of files from the unzipped directory
	INSERT INTO #TEMP (Fname, Lvl, IsFile )
	EXEC master.dbo.xp_dirtree @RawZipFileLoc , 1 , 1

	-- Ignore anything that's not a file
	DELETE FROM #TEMP WHERE IsFile = 0

	-- Loop round the list of files and call the unzip utility
	DECLARE MYCURSOR CURSOR FOR 
	SELECT Fname FROM #TEMP

	OPEN MYCURSOR 
	FETCH NEXT FROM MYCURSOR INTO @FileName

	DECLARE @Res INT, @Cnt INT, @SQLError INT
	SET @Cnt = 0
	SET @SQLError = 0

	WHILE @@FETCH_STATUS = 0
	BEGIN 
		SET @Cnt = @Cnt + 1

--		SET @FullPath = @UnzipUtility + ' "'+@RawZipFileLoc + @FileName + '" '  + isnull(@UnzipParameters,'') +' "'+@UnzippedFileLoc + '"'
		SET @FullPath = @UnzipUtility + ' "'+@RawZipFileLoc + @FileName + '" '  + isnull(@UnzipParameters,'') +' -o"'+@UnzippedFileLoc + '"' + ' -aoa'

		EXEC @Res = master.dbo.xp_cmdshell @FullPath

		INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
		VALUES ( getdate(), 'pr_UnzipFiles', @FullPath, @Res )

		SET @SQLError = @SQLError + @Res

		FETCH NEXT FROM MYCURSOR INTO @FileName
	END

	CLOSE MYCURSOR
	DEALLOCATE MYCURSOR

	DROP TABLE #TEMP

	IF @Cnt <= 0 
	BEGIN
		INSERT INTO tbl_EventLog ( EventDateTime , CallingProcedure, EventDescription, EventStatus )
		VALUES ( getdate(), 'pr_UnzipFiles', 'No Files Found to Unzip in "'+@RawZipFileLoc+'"', 1 )
	END
	ELSE
	BEGIN
		IF @SQLError = 0 EXEC pr_ArchiveZippedFiles
	END

SkipLabel:

END

