﻿CREATE PROCEDURE uspCreatetblLatestPregnancy AS

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'utbl_LatestPregnancy')
DROP TABLE utbl_LatestPregnancy

SELECT *
INTO utbl_LatestPregnancy
FROM vwGetLatestPregnancyNumber
ORDER BY Patient_Pointer


CREATE INDEX idxPatient ON utbl_LatestPregnancy(Patient_Pointer)