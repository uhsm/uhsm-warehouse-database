﻿CREATE TABLE [dbo].[reftemp] (
    [SourceUniqueID]           INT           NOT NULL,
    [FacilityID]               VARCHAR (20)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Patient_Pointer]          INT           NULL,
    [AppointmentDate]          DATE          NULL,
    [ClinicCode]               VARCHAR (20)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ClinicName]               VARCHAR (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [AttendStatus]             VARCHAR (80)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ReferralDate]             DATETIME      NULL,
    [PatientSurname]           VARCHAR (30)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [PatientForename]          VARCHAR (30)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [DateOfBirth]              SMALLDATETIME NULL,
    [InitialEstimateOfDueDate] DATETIME      NULL,
    [FinalDueDate]             DATETIME      NULL,
    [BestEDDFromLMPData]       DATETIME      NULL,
    [Postcode]                 VARCHAR (25)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [AppointmentPCTCCGCode]    VARCHAR (10)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [AppointmentPCTCCG]        VARCHAR (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [AgeAtAppointment]         INT           NULL
);

