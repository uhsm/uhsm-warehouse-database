﻿CREATE TABLE [dbo].[tbl_Cervicogram] (
    [Patient_Pointer]             INT          NOT NULL,
    [Pregnancy_ID]                INT          NOT NULL,
    [Pregnancy number]            VARCHAR (60) NULL,
    [Examination_Number]          INT          NOT NULL,
    [Hours in labour]             VARCHAR (60) NULL,
    [Date of Vaginal Examination] VARCHAR (10) NULL,
    [Time of Vaginal Examination] VARCHAR (6)  NULL,
    [Cervical dilation]           VARCHAR (30) NULL,
    [Date of Review]              VARCHAR (10) NULL,
    [Time of Review]              VARCHAR (6)  NULL,
    [Type of Review]              VARCHAR (60) NULL,
    [Birth Order]                 VARCHAR (30) NULL,
    CONSTRAINT [PK_tbl_Cervicogram] PRIMARY KEY CLUSTERED ([Patient_Pointer] ASC, [Pregnancy_ID] ASC, [Examination_Number] ASC)
);

