﻿CREATE TABLE [dbo].[tbl_DiabetesRecord] (
    [Patient_Pointer]                         INT           NOT NULL,
    [Date of diabetes registration]           VARCHAR (10)  NULL,
    [Diabetes registration status]            VARCHAR (60)  NULL,
    [Diabetes registration status Reason]     VARCHAR (60)  NULL,
    [Blood glucose at diagnosis 1]            VARCHAR (30)  NULL,
    [Blood glucose at diagnosis 1 Method]     VARCHAR (60)  NULL,
    [Blood glucose at diagnosis 2]            VARCHAR (30)  NULL,
    [Blood glucose at diagnosis 2 Method]     VARCHAR (60)  NULL,
    [Diabetes diagnostic criteria]            VARCHAR (60)  NULL,
    [Diabetes diagnostic criteria Text]       VARCHAR (255) NULL,
    [OGTT 0 hour glucose at diagnosis]        VARCHAR (30)  NULL,
    [OGTT 0 hour glucose at diagnosis Method] VARCHAR (60)  NULL,
    [OGTT 2 hour glucose at diagnosis]        VARCHAR (30)  NULL,
    [OGTT 2 hour glucose at diagnosis Method] VARCHAR (60)  NULL,
    [Urine ketones at diagnosis]              VARCHAR (60)  NULL,
    CONSTRAINT [PK_tbl_DiabetesRecord] PRIMARY KEY CLUSTERED ([Patient_Pointer] ASC)
);

