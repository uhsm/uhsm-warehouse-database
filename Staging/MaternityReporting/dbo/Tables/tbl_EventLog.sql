﻿CREATE TABLE [dbo].[tbl_EventLog] (
    [EventDateTime]    DATETIME    NOT NULL,
    [CallingProcedure] CHAR (50)   NOT NULL,
    [EventDescription] CHAR (1000) NOT NULL,
    [EventStatus]      INT         NOT NULL
);

