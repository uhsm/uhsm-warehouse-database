﻿CREATE TABLE [dbo].[tbl_Notes_Copy] (
    [Pointer]    INT            NOT NULL,
    [Noteno]     CHAR (25)      COLLATE Latin1_General_BIN NOT NULL,
    [Revnoteno]  CHAR (25)      COLLATE Latin1_General_BIN NOT NULL,
    [Code]       CHAR (5)       NOT NULL,
    [Notedate]   INT            NULL,
    [Recall]     INT            NULL,
    [Notetype]   CHAR (1)       NULL,
    [Valuetype]  CHAR (1)       NULL,
    [Nvalue]     CHAR (30)      NULL,
    [Attributes] VARCHAR (7000) NULL,
    CONSTRAINT [PK_tbl_Notes_Copy] PRIMARY KEY NONCLUSTERED ([Pointer] ASC, [Noteno] ASC, [Revnoteno] ASC, [Code] ASC)
);


GO
CREATE CLUSTERED INDEX [Idx_Notes_1]
    ON [dbo].[tbl_Notes_Copy]([Code] ASC, [Pointer] ASC, [Noteno] ASC);

