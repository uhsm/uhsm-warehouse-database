﻿CREATE TABLE [dbo].[tbl_ObstetricSummary] (
    [Patient_Pointer]                                  INT            NOT NULL,
    [Number of still births]                           VARCHAR (30)   NULL,
    [Pregs ending before 12 wks]                       VARCHAR (60)   NULL,
    [Pregs ending before 12 wks Miscarriages]          VARCHAR (30)   NULL,
    [Pregs ending before 12 wks Induced abortions]     VARCHAR (30)   NULL,
    [Pregs ending before 12 wks Terminations]          VARCHAR (30)   NULL,
    [Pregs ending before 12 wks Ectopics]              VARCHAR (30)   NULL,
    [Pregs ending before 12 wks Hydatidiform moles]    VARCHAR (30)   NULL,
    [Pregs ending between 12 - 24 w]                   VARCHAR (60)   NULL,
    [Pregs ending between 12 - 24 w Miscarriages]      VARCHAR (30)   NULL,
    [Pregs ending between 12 - 24 w Induced abortions] VARCHAR (30)   NULL,
    [Pregs ending between 12 - 24 w Terminations]      VARCHAR (30)   NULL,
    [Pregs ending between 12 - 24 w Ectopics]          VARCHAR (30)   NULL,
    [Pregs ending between 12 - 24 w Hysterotomys]      VARCHAR (30)   NULL,
    [Gravida]                                          VARCHAR (30)   NULL,
    [No. of Past Pregnancies]                          VARCHAR (30)   NULL,
    [Parity at Booking]                                VARCHAR (1000) NULL,
    [No. of registerable TOP]                          VARCHAR (30)   NULL,
    [Female contraceptive status]                      VARCHAR (60)   NULL,
    [Currently pregnant]                               VARCHAR (60)   NULL,
    [Currently pregnant EDD]                           INT            NULL,
    [No. of Registerable Pregnancies]                  VARCHAR (30)   NULL,
    [No. of Non-registerable Pregs]                    VARCHAR (30)   NULL,
    [No. of registerable livebirths]                   VARCHAR (30)   NULL,
    [Number of live births]                            VARCHAR (30)   NULL,
    [No. of registerable stillbirths]                  VARCHAR (30)   NULL,
    [No. of children living now]                       VARCHAR (30)   NULL,
    [No. of non-registerable births]                   VARCHAR (30)   NULL,
    [Number_of_previous_births]                        VARCHAR (60)   NULL,
    [No. of children who have died]                    VARCHAR (30)   NULL,
    [Number_of_previous_caesars]                       VARCHAR (60)   NULL,
    [No. of neonatal deaths]                           VARCHAR (30)   NULL,
    [No. of pre-term livebirths]                       VARCHAR (30)   NULL,
    [Parity after Delivery]                            VARCHAR (1000) NULL,
    [Number of previous pregnancies]                   VARCHAR (30)   NULL,
    CONSTRAINT [PK_tbl_New_ObstetricSummary] PRIMARY KEY CLUSTERED ([Patient_Pointer] ASC)
);

