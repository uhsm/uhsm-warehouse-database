﻿CREATE TABLE [dbo].[tbl_RefNatDelStat] (
    [DelvPersonStatus]          VARCHAR (60) NOT NULL,
    [DelvPersonStatus_NAT_CODE] VARCHAR (1)  NULL,
    CONSTRAINT [PK_tbl_RefNatDelStat] PRIMARY KEY CLUSTERED ([DelvPersonStatus] ASC)
);

