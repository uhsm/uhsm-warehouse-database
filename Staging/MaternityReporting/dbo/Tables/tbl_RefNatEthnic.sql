﻿CREATE TABLE [dbo].[tbl_RefNatEthnic] (
    [EthnicCategory]     VARCHAR (60) NOT NULL,
    [Ethnicity_NAT_CODE] CHAR (10)    NULL,
    CONSTRAINT [PK_tbl_RefNatEthnic] PRIMARY KEY CLUSTERED ([EthnicCategory] ASC)
);

