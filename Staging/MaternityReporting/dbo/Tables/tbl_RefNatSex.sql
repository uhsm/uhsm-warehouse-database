﻿CREATE TABLE [dbo].[tbl_RefNatSex] (
    [SexOfBaby]          VARCHAR (60) NOT NULL,
    [SexOfBaby_NAT_CODE] VARCHAR (1)  NULL,
    CONSTRAINT [PK_tbl_RefNatSex] PRIMARY KEY CLUSTERED ([SexOfBaby] ASC)
);

