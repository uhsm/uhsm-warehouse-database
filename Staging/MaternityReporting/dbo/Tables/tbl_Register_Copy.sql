﻿CREATE TABLE [dbo].[tbl_Register_Copy] (
    [Regtype]         CHAR (1)      NULL,
    [Status]          CHAR (1)      NULL,
    [Surname]         CHAR (25)     NULL,
    [Forename]        CHAR (25)     NULL,
    [Forename2]       CHAR (15)     NULL,
    [Title]           CHAR (5)      NULL,
    [Sex]             CHAR (20)     NULL,
    [Birthdate]       INT           NULL,
    [Pointer]         INT           NOT NULL,
    [Address1]        CHAR (35)     NULL,
    [Address2]        CHAR (35)     NULL,
    [Address3]        CHAR (35)     NULL,
    [Address4]        CHAR (35)     NULL,
    [Postcode]        CHAR (10)     NULL,
    [Phone]           CHAR (35)     NULL,
    [Number1]         CHAR (14)     NULL,
    [Number2]         CHAR (14)     NULL,
    [Number3]         CHAR (14)     NULL,
    [Dateadded]       INT           NULL,
    [Datemodified]    INT           NULL,
    [Semaphore]       CHAR (1)      NULL,
    [Previous]        CHAR (20)     NULL,
    [Extra]           VARCHAR (250) NULL,
    [Nhsnumberstatus] CHAR (50)     NULL,
    CONSTRAINT [PK_tbl_Register_Copy] PRIMARY KEY NONCLUSTERED ([Pointer] ASC)
);


GO
CREATE CLUSTERED INDEX [Idx_Register_1]
    ON [dbo].[tbl_Register_Copy]([Pointer] ASC, [Regtype] ASC);

