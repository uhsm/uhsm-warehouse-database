﻿CREATE TABLE [dbo].[tbl_SiteSpecificParameters] (
    [ParameterName] VARCHAR (255) NOT NULL,
    [Value]         VARCHAR (255) NULL,
    CONSTRAINT [PK_tbl_SiteSpecificParameters] PRIMARY KEY CLUSTERED ([ParameterName] ASC)
);

