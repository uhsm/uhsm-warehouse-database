﻿CREATE TABLE [dbo].[tbl_Stafflink_Copy] (
    [Linkid]       INT       NOT NULL,
    [Staffpointer] INT       NOT NULL,
    [Organisation] INT       NOT NULL,
    [Pascode]      CHAR (20) NULL,
    CONSTRAINT [PK_tbl_Stafflink_Copy] PRIMARY KEY NONCLUSTERED ([Linkid] ASC)
);


GO
CREATE CLUSTERED INDEX [Idx_Stafflink_1]
    ON [dbo].[tbl_Stafflink_Copy]([Linkid] ASC);

