﻿CREATE TABLE [dbo].[utbl_LatestPregnancy] (
    [Patient_Pointer] INT NOT NULL,
    [Pregnancy_ID]    INT NOT NULL,
    [Pregnancy Order] INT NULL
);


GO
CREATE NONCLUSTERED INDEX [idxPatient]
    ON [dbo].[utbl_LatestPregnancy]([Patient_Pointer] ASC);

