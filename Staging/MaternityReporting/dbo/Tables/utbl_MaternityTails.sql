﻿CREATE TABLE [dbo].[utbl_MaternityTails] (
    [Date of delivery]                            DATETIME     NULL,
    [BabyPatientPointer]                          INT          NULL,
    [BabyPregnancyNumber]                         VARCHAR (60) NULL,
    [BabyDeliveryNumber]                          INT          NULL,
    [BabyNumber]                                  INT          NULL,
    [Spell_refno]                                 VARCHAR (60) NULL,
    [Local Patient Identifier (Mother)]           VARCHAR (14) NULL,
    [NHS Number (Mother)]                         VARCHAR (14) NULL,
    [Postcode (Mother)]                           VARCHAR (10) NULL,
    [Birth Date Mother)]                          DATETIME     NULL,
    [Number of Past Pregnancies]                  INT          NULL,
    [Local Patient Identifier (Baby)]             VARCHAR (60) NULL,
    [NHS Number (Baby)]                           VARCHAR (14) NULL,
    [Birth Date (Baby)]                           DATETIME     NULL,
    [Gender (Baby)]                               VARCHAR (1)  NULL,
    [Ethnic Category (Baby)]                      CHAR (10)    NULL,
    [Number of Babies]                            INT          NULL,
    [First Antenatal Assessment Date]             DATETIME     NULL,
    [First Antenatal Assessment Date Status]      INT          NOT NULL,
    [Code of GMP Responsible for Antenatal Care]  VARCHAR (60) NULL,
    [Registered GMP - Antenatal Care]             VARCHAR (60) NULL,
    [Delivery Place Change Reason]                INT          NULL,
    [Delivery Place Type (Intended)]              INT          NULL,
    [Anaesthetic Given During Labour or Delivery] INT          NULL,
    [Anaesthetic Given Post Labour or Delivery]   INT          NULL,
    [Labour or Delivery Onset Method]             INT          NULL,
    [Delivery Date]                               DATETIME     NULL,
    [Birth Order]                                 INT          NULL,
    [Delivery Method]                             INT          NULL,
    [Gestation Length (Assessment)]               INT          NULL,
    [Resuscitation Method]                        INT          NULL,
    [Status of Person Conducting Delivery]        VARCHAR (1)  NULL,
    [Delivery Place Type (Actual)]                INT          NULL,
    [Live or Still Birth]                         VARCHAR (1)  NULL,
    [Birth Weight]                                INT          NULL,
    [Patient_Pointer]                             INT          NULL,
    [Pregnancy number]                            VARCHAR (60) NULL
);

