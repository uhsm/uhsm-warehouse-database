﻿

CREATE VIEW dbo.view_AdmissionTransferEpisodes
AS 
SELECT 
 [Patient_Pointer],
 [Spell_Number],
 [AdmissionTransfer_Number],

 dbo.func_Convert_Date( [Date of admission/transfer]) [Date of admission/transfer],
 dbo.func_Convert_Time( [Time of admission/transfer]) [Time of admission/transfer],

 [Source of admission/transfer],
 [Method of admission/transfer],
 [Location admitted/trans to],
 dbo.func_Convert_Integer ( [Hospital adm this pregnancy] ,'') [Hospital adm this pregnancy],
 [Significant Facilty],
 [Type of unit admitted/transferred to],

 [COPPISH admission/transfer from],
 [COPPISH admission type],

 [BAPM category of admission],
 [BAPM Source of admission],

 [Consultants code],
 [Consultants name],
 [Specialty code],

 [Admission/transfer flag],
 [Admission/transfer type],
 [Adm/trans episode number],
 [Adm/trans event number],
 [External adm/trans code],
 [Internal adm/trans number],
 [Adm/trans deletion allowed],

 [GP code],
 [GP Address code],
 [GP Practice code],
 [General Practitioner Name],


 [Brought to unit by],
 [Brought to unit by Text],
 [Transport Method],
 [Transport Method Oxygen],
 [Transport Method Ventilated],
 [Placed in],
 [Placed in Oxygen],
 [Admission Plan],
 [Admission Plan Text],
 [GP informed],
 [GP informed Text],

 [Admission/transfer reason 1],
 [Admission/transfer reason 1 Text],
 [Admission/transfer reason 2],
 [Admission/transfer reason 2 Text],
 [Admission/transfer reason 3],
 [Admission/transfer reason 3 Text],
 [Reason for admission 4],
 [Reason for admission 4 Text],
 [Reason for admission 5],
 [Reason for admission 5 Text],
 [Reason for admission 6],
 [Reason for admission 6 Text],
 [COPPISH Admission Reason],

 dbo.func_Convert_Date( [BirthRate date of arrival]) [BirthRate date of arrival],
 dbo.func_Convert_Date( [BirthRate date of departure]) [BirthRate date of departure],
 dbo.func_Convert_Time( [BirthRate time of arrival]) [BirthRate time of arrival],
 dbo.func_Convert_Time( [BirthRate time of departure]) [BirthRate time of departure],
 [Still on Labour Ward],

 [Reason for discharge],
 [Reason for discharge Text]

 FROM 
 tbl_AdmissionTransferEpisodes














