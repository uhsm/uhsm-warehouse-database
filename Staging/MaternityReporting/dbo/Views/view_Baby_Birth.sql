﻿





CREATE VIEW dbo.view_Baby_Birth
AS 
SELECT 
 [Patient_Pointer],
 Pregnancy_ID,
 [Pregnancy number],
 [Delivery_Number],
 [Baby_Number],

-- Birth (8)
 dbo.func_Convert_Integer( [Birth Order], '') [Birth Order],
 dbo.func_Convert_Date( [Date of Birth]) [Date of Birth],
 dbo.func_Convert_Time( [Time of Birth]) [Time of Birth],
 dbo.func_Convert_Integer( [Estimate of gestation], 'weeks') [Estimate of gestation (weeks)],
 dbo.func_Convert_Integer( [Estimate of gestation Days], 'days') [Estimate of gestation (days)],

-- Rupture of Membranes
 dbo.func_Convert_Date( [Date of Rupture of Membranes]) [Date of Rupture of Membranes],
 dbo.func_Convert_Time( [Time of Rupture of Membranes]) [Time of Rupture of Membranes],
 dbo.func_Convert_HrsMins( [Mem. Rupt/Birth Int.(hrs:mins)]) [Membrane Rupt/Birth Interval (hrs:mins)],
 dbo.func_Convert_Integer( [Gestation at membrane rupture], 'weeks') [Gestation at membrane rupture (weeks)],
 dbo.func_Convert_Integer( [Gestation at membrane rupture Days], 'days') [Gestation at membrane rupture (days)],

-- Liquor
 [Condition of liquor],
 [Quantity of liquor],
 [Liquor state],
 [Liquor volume],

-- Location (16)
 [Actual Place of Birth],
 [Actual Place of Birth Text],
 [Responsible Hospital],
 [Korner Intended Place of Birth],
 [Korner Actual Place of Birth],
 [Korner Reason for Change],
 [Confirm Original Booking],
 [Confirm Original Booking Text],
 [Unit Responsible for Birth],
 [Unit Responsible for Birth Text],
 [Room Number],
 [Location at Delivery],
 [Plan change prior to delivery],
 [Transfer in labour or delivery],
 dbo.func_Convert_Date( [Transfer plan date]) [Transfer plan date],
 dbo.func_Convert_Time( [Transfer plan time]) [Transfer plan time],

-- Outcome
 [Outcome of birth],
 [Outcome of birth Death_of_Fetus],
 [Presentation before Lab/Caesar],
 [Presentation before Lab/Caesar Text],
 [Post-term birth],
 [Pre-term birth],
 [Korner Method of delivery],
 [Final method of delivery],
 [Final method of delivery Details],
 [Failed Method of Delivery],
 [Failed Method of Delivery Details],

-- Presentation/Position
 [Abdominal Lie],
 [Presentation prior to delivery],
 [Presentation prior to delivery Text],
 [Abdominal station],
 [Fetal position],
 [Cephalic position],
 [Consent obtained for Vaginal Examination],
 [Vaginal Examination],
 [Vaginal Examination Reason],
 dbo.func_Convert_Date( [Date of Vaginal Examination]) [Date of Vaginal Examination],
 dbo.func_Convert_Time( [Time of Vaginal Examination]) [Time of Vaginal Examination],
 dbo.func_Convert_Integer( [Station of presenting part 2], 'cm') [Station of presenting part (cm)],
 [Presenting part],
 [Presenting part Text],
 [Position of presenting part],
 dbo.func_Convert_Integer( [Cervical dilation 2], 'cm') [Cervical dilation (cm)],
 [Cervical consistency],
 dbo.func_Convert_Integer( [Cervical length], 'cm') [Cervical length (cm)],
 [Cervical position],
 dbo.func_Convert_Integer( [Bishops Score], '') [Bishops Score],
 [Caput],
 [Moulding],
 [Other findings on Vaginal Examination],
 [Other findings on Vaginal Examination Text],

-- Dates/Times (14)
 dbo.func_Convert_Date( [Date of Onset of Pushing]) [Date of Onset of Pushing],
 dbo.func_Convert_Time( [Time of Onset of Pushing]) [Time of Onset of Pushing],
 dbo.func_Convert_HrsMins( [Push Start/Del. Int.(hrs:mins)]) [Push Start/Delivery Interval (hrs:mins)],
 dbo.func_Convert_Date( [Date of Onset of Second Stage]) [Date of Onset of Second Stage],
 dbo.func_Convert_Time( [Time of Onset of Second Stage]) [Time of Onset of Second Stage],
 dbo.func_Convert_HrsMins( [Duration 1st Stage(hrs:mins)]) [Duration 1st Stage(hrs:mins)],
 dbo.func_Convert_HrsMins( [Duration 2nd Stage(hrs:mins)]) [Duration 2nd Stage(hrs:mins)],
 dbo.func_Convert_HrsMins( [Adm to del interval(hrs:mins)]) [Adm to del interval (hrs:mins)],

-- Breech
 [Scan for breech assessment],
 [Scan for breech assessment Estimated fetal weight],
 [Scan for breech assessment Attitude],
 [Pelvimetry performed],
 dbo.func_Convert_Time( [Breech diagnosis time]) [Breech diagnosis time],
 [Type of breech delivery],
 [Management of vaginal breech],
 [Type of breech presentation],
 [E.C.V. attempted] [ECV attempted],
 [E.C.V. attempted Text] [ECV attempted Text],

-- Complications
 [Cord Prolapse or Presentation],
 [Cord Prolapse or Presentation Text],
 [Fetal distress],
 [Fetal distress Reason],
 [Any shoulder dystocia],
 [Any shoulder dystocia Details],
 [Any shoulder dystocia Method of Management 1],
 dbo.func_Convert_Integer( [Any shoulder dystocia Time taken 1], 'mins') [Any shoulder dystocia Time taken 1 (mins)],
 [Any shoulder dystocia Method of Management 2],
 dbo.func_Convert_Integer( [Any shoulder dystocia Time taken 2], 'mins') [Any shoulder dystocia Time taken 2 (mins)],
 [Any shoulder dystocia Method of Management 3],
 dbo.func_Convert_Integer( [Any shoulder dystocia Time taken 3], 'mins') [Any shoulder dystocia Time taken 3 (mins)],
 [Any shoulder dystocia Method of Management 4],
 dbo.func_Convert_Integer( [Any shoulder dystocia Time taken 4], 'mins') [Any shoulder dystocia Time taken 4 (mins)],
 [Any shoulder dystocia Method of Management 5],
 dbo.func_Convert_Integer( [Any shoulder dystocia Time taken 5], 'mins') [Any shoulder dystocia Time taken 5 (mins)],

-- Assisted Delivery
 [Assistance decision taken by],
 [Assistance decision taken by Status],
 dbo.func_GetLink_StaffName( [Decision discussed with]) [Decision discussed with Name],
 dbo.func_GetLink_StaffCode( [Decision discussed with]) [Decision discussed with Code],
 [Decision discussed with Grade],
 dbo.func_GetLink_StaffName( [Assisted delivery decision by]) [Assisted delivery decision by Name],
 dbo.func_GetLink_StaffCode( [Assisted delivery decision by]) [Assisted delivery decision by Code],
 [Assisted delivery decision by Grade],
 [Type of decision maker],
 [Type of decision assistant],

 [Any failed assisted type deliveries],
 [Type of assisted delivery],
 [Was baby delivered in water],
 [Was baby delivered in water Complications],
 [Indication Forceps/Ventouse],
 [Indication Forceps/Ventouse Text],
 [Prev. failed Forcep/Ventouse] [Prevous failed Forcep/Ventouse],
 [Were forceps used],
 [Were forceps used Text],
 [Indication for forceps],
 [Indication for forceps Text],
 [Type of forceps],
 [Type of forceps Text],
 [Type of forceps used],
 [Type of forceps used Text],
 [Manoeuvres used],
 [Manoeuvres used Text],
 [Application 1] [Application Forceps],
 dbo.func_Convert_Time( [Time of forceps application]) [Time of forceps application],
 dbo.func_Convert_Integer( [No. contractions with forceps] ,'') [Number of contractions with forceps],
 [Reapplication required 2] [Reapplication required Forceps],
 [Reapplication required 2 Details] [Reapplication required Forceps Details],
 [Forceps delivery successful],
 [Forceps delivery successful Additional procedure],
 [Forceps delivery successful Text],
 [Catheterised],
 [Indication for ventouse],
 [Indication for ventouse Text],
 [Application 2] [Application Ventouse],
 [Time of decision to use Ventouse Application],
 dbo.func_Convert_Time( [Time of use of Ventouse Application]) [Time of use of Ventouse Application],
 [Type of cup],
 dbo.func_Convert_Integer( [No. of contractions with cup],'') [Number of contractions with cup],
 [Reapplication required 1] [Reapplication required Ventouse],
 [Reapplication required 1 Text] [Reapplication required Ventouse Text],
 [Ventouse delivery successful],
 [Ventouse delivery successful Additional procedure],
 [Ventouse delivery successful Text],
 [Korner Method of Labour Onset],
 [Korner Post Delivery Analgesia],
 [Revised post delivery findings],
 [Revised post delivery findings Fetal position],
 [Revised post delivery findings Text],

-- Caesarean
 [Urgency of Caesarean],
 dbo.func_Convert_Date( [Date of decision for Emerg C/S]) [Date of decision for Emerg C/S],
 dbo.func_Convert_Time( [Time of decision for Emerg C/S]) [Time of decision for Emerg C/S],
 dbo.func_Convert_HrsMins( [Decide/Del Interval (hrs:mins)]) [Decide/Del Interval (hrs:mins)],
 [Indication for Emerg. C/S] [Indication for Emergency C/S],
 [Indication for Emerg. C/S Text] [Indication for Emergency C/S Text],
 [Additional important reasons 1] [Additional important reasons for Emerg C/S],
 [Additional important reasons 1 Text] [Additional important reasons for Emerg C/S Text],
 [Elective C/S done early],
 [Reason for doing El. C/S early] [Reason for doing Elective C/S early],
 [Reason for doing El. C/S early Text] [Reason for doing Elective C/S early Text],
 [Indication for Elective C/S],
 [Indication for Elective C/S Text],
 [Additional important reasons 2] [Additional important reasons for Elective C/S],
 [Additional important reasons 2 Text] [Additional important reasons for Elective C/S Text],
 [Consent obtained for C/S 1] [Consent obtained for Elective C/S],
 [Consent obtained for C/S 1 Reason] [Consent obtained for Elective C/S Reason],
 [Classification of urgency 1] [Classification of urgency Elective C/S],
 [Consent obtained for C/S 2] [Consent obtained for Emergency C/S],
 [Consent obtained for C/S 2 Reason] [Consent obtained for Emergency C/S Reason],
 [Classification of urgency 2] [Classification of urgency Emergency C/S],
 [Was C/S done in 2nd stage],
 [Venue for operative procedure],
 [Venue for operative procedure Text],
 [Status of Operative Deliverer],

-- Anaesthetic
 dbo.func_GetLink_StaffName( [Anaesthetic inserted by]) [Anaesthetic inserted by Name],
 dbo.func_GetLink_StaffCode( [Anaesthetic inserted by]) [Anaesthetic inserted by Code],
 [Anaesthetic inserted by Grade],
 [Type of staff inserting anaesthesia],
 [Success of Regional Block],
 [Korner Analgesia/Anaesthesia],
 [Birth Analgesia/Anaesthesia],
 [Self admin inhalation],
 [Self admin inhalation Details],
 [Pudendal Block],
 [Pudendal Block Details],
 [Local Infiltration],
 [Local Infiltration Details],
 [Epidural],
 [Epidural Details],
 [Spinal],
 [Spinal Details],
 [Caudal],
 [Caudal Details],
 [General Anaesthetic],
 [General Anaesthetic Details],
 [Combined spinal/epidural],
 [Combined spinal/epidural Details],
 [Reason for Delay]

 FROM 
 tbl_Baby


















