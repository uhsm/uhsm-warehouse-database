﻿




CREATE VIEW dbo.view_Baby_Birth_2
AS 
SELECT 
 [Patient_Pointer],
 Pregnancy_ID,
 [Pregnancy number],
 [Delivery_Number],
 [Baby_Number],
 dbo.func_Convert_Integer( [Birth Order], '') [Birth Order],

-- Monitioring
 [Type of Fetal Monitoring],
 [CTG during Labour],
 [CTG during Labour Text],
 [Was a fetal blood sample taken],
 dbo.func_Convert_Date( [Was a fetal blood sample taken Date sample taken1]) [Was a fetal blood sample taken Date sample taken 1],
 dbo.func_Convert_Time( [Was a fetal blood sample taken Time sample taken1]) [Was a fetal blood sample taken Time sample taken 1],
 dbo.func_Convert_Numeric53( [Was a fetal blood sample taken pH1],'') [Was a fetal blood sample taken pH 1],
 dbo.func_Convert_Numeric52( [Was a fetal blood sample taken Base excess1], 'mmol/l') [Was a fetal blood sample taken Base excess 1 (mmol/l)],
 dbo.func_Convert_Date( [Was a fetal blood sample taken Date sample taken2]) [Was a fetal blood sample taken Date sample taken 2],
 dbo.func_Convert_Time( [Was a fetal blood sample taken Time sample taken2]) [Was a fetal blood sample taken Time sample taken 2],
 dbo.func_Convert_Numeric53( [Was a fetal blood sample taken pH2],'') [Was a fetal blood sample taken pH 2],
 dbo.func_Convert_Numeric52( [Was a fetal blood sample taken Base excess2], 'mmol/l') [Was a fetal blood sample taken Base excess 2 (mmol/l)],
 dbo.func_Convert_Date( [Was a fetal blood sample taken Date sample taken3]) [Was a fetal blood sample taken Date sample taken 3],
 dbo.func_Convert_Time( [Was a fetal blood sample taken Time sample taken3]) [Was a fetal blood sample taken Time sample taken 3],
 dbo.func_Convert_Numeric53( [Was a fetal blood sample taken pH3],'') [Was a fetal blood sample taken pH 3],
 dbo.func_Convert_Numeric52( [Was a fetal blood sample taken Base excess3], 'mmol/l') [Was a fetal blood sample taken Base excess 3 (mmol/l)],
 dbo.func_Convert_Date( [Was a fetal blood sample taken Date sample taken4]) [Was a fetal blood sample taken Date sample taken 4],
 dbo.func_Convert_Time( [Was a fetal blood sample taken Time sample taken4]) [Was a fetal blood sample taken Time sample taken 4],
 dbo.func_Convert_Numeric53( [Was a fetal blood sample taken pH4],'') [Was a fetal blood sample taken pH 4],
 dbo.func_Convert_Numeric52( [Was a fetal blood sample taken Base excess4], 'mmol/l') [Was a fetal blood sample taken Base excess 4 (mmol/l)],
 dbo.func_Convert_Date( [Was a fetal blood sample taken Date sample taken5]) [Was a fetal blood sample taken Date sample taken 5],
 dbo.func_Convert_Time( [Was a fetal blood sample taken Time sample taken5]) [Was a fetal blood sample taken Time sample taken 5],
 dbo.func_Convert_Numeric53( [Was a fetal blood sample taken pH5],'') [Was a fetal blood sample taken pH 5],
 dbo.func_Convert_Numeric52( [Was a fetal blood sample taken Base excess5], 'mmol/l') [Was a fetal blood sample taken Base excess 5 (mmol/l)],
 dbo.func_Convert_Date( [Was a fetal blood sample taken Date sample taken6]) [Was a fetal blood sample taken Date sample taken 6],
 dbo.func_Convert_Time( [Was a fetal blood sample taken Time sample taken6]) [Was a fetal blood sample taken Time sample taken 6],
 dbo.func_Convert_Numeric53( [Was a fetal blood sample taken pH6],'') [Was a fetal blood sample taken pH 6],
 dbo.func_Convert_Numeric52( [Was a fetal blood sample taken Base excess6], 'mmol/l') [Was a fetal blood sample taken Base excess 6 (mmol/l)],
 [Fetal blood samples taken],
 dbo.func_Convert_Date( [Fetal blood samples taken Date sample taken1]) [Fetal blood samples taken Date sample taken 1],
 dbo.func_Convert_Time( [Fetal blood samples taken Time sample taken1]) [Fetal blood samples taken Time sample taken 1],
 dbo.func_Convert_Numeric53( [Fetal blood samples taken pH1],'') [Fetal blood samples taken pH 1],
 dbo.func_Convert_Numeric52( [Fetal blood samples taken Base excess1], 'mmol/l') [Fetal blood samples taken Base excess 1 (mmol/l)],
 dbo.func_Convert_Date( [Fetal blood samples taken Date sample taken2]) [Fetal blood samples taken Date sample taken 2],
 dbo.func_Convert_Time( [Fetal blood samples taken Time sample taken2]) [Fetal blood samples taken Time sample taken 2],
 dbo.func_Convert_Numeric53( [Fetal blood samples taken pH2],'') [Fetal blood samples taken pH2],
 dbo.func_Convert_Numeric52( [Fetal blood samples taken Base excess2], 'mmol/l') [Fetal blood samples taken Base excess 2 (mmol/l)],
 dbo.func_Convert_Date( [Fetal blood samples taken Date sample taken3]) [Fetal blood samples taken Date sample taken 3],
 dbo.func_Convert_Time( [Fetal blood samples taken Time sample taken3]) [Fetal blood samples taken Time sample taken 3],
 dbo.func_Convert_Numeric53( [Fetal blood samples taken pH3],'') [Fetal blood samples taken pH 3],
 dbo.func_Convert_Numeric52( [Fetal blood samples taken Base excess3], 'mmol/l') [Fetal blood samples taken Base excess3 (mmol/l)],
 dbo.func_Convert_Date( [Fetal blood samples taken Date sample taken4]) [Fetal blood samples taken Date sample taken 4],
 dbo.func_Convert_Time( [Fetal blood samples taken Time sample taken4]) [Fetal blood samples taken Time sample taken 4],
 dbo.func_Convert_Numeric53( [Fetal blood samples taken pH4],'') [Fetal blood samples taken pH 4],
 dbo.func_Convert_Numeric52( [Fetal blood samples taken Base excess4], 'mmol/l') [Fetal blood samples taken Base excess 4 (mmol/l)],
 dbo.func_Convert_Date( [Fetal blood samples taken Date sample taken5]) [Fetal blood samples taken Date sample taken 5],
 dbo.func_Convert_Time( [Fetal blood samples taken Time sample taken5]) [Fetal blood samples taken Time sample taken 5],
 dbo.func_Convert_Numeric53( [Fetal blood samples taken pH5],'') [Fetal blood samples taken pH 5],
 dbo.func_Convert_Numeric52( [Fetal blood samples taken Base excess5], 'mmol/l') [Fetal blood samples taken Base excess 5 (mmol/l)],
 dbo.func_Convert_Date( [Fetal blood samples taken Date sample taken6]) [Fetal blood samples taken Date sample taken 6],
 dbo.func_Convert_Time( [Fetal blood samples taken Time sample taken6]) [Fetal blood samples taken Time sample taken 6],
 dbo.func_Convert_Numeric53( [Fetal blood samples taken pH6],'') [Fetal blood samples taken pH 6],
 dbo.func_Convert_Numeric52( [Fetal blood samples taken Base excess6], 'mmol/l') [Fetal blood samples taken Base excess 6 (mmol/l)],

-- Skin-to-skin
 [Skin - skin contact within 1hr],
 [Skin - skin contact within 1hr Details],
 [Skin to skin contact],
 dbo.func_Convert_Date( [Skin to skin contact Date attempted]) [Skin to skin contact Date attempted],
 dbo.func_Convert_Time( [Skin to skin contact Time attempted]) [Skin to skin contact Time attempted],
 [Skin to skin contact Details],
 dbo.func_Convert_Integer( [Time to skin to skin contact], 'hour(s)') [Time to skin to skin contact (hours)],

-- Feeding
 [First feed offered within 1hr],
 [First feed offered within 1hr Date],
 [First feed offered within 1hr Time],
 [Breast feeding],
 dbo.func_Convert_Date( [Breast feeding Date 1st put baby to breast]) [Breast feeding Date 1st put baby to breast],
 dbo.func_Convert_Time( [Breast feeding Time 1st put baby to breast]) [Breast feeding Time 1st put baby to breast],
 dbo.func_Convert_Date( [Breast feeding Date 1st given breast milk]) [Breast feeding Date 1st given breast milk],
 dbo.func_Convert_Time( [Breast feeding Time 1st given breast milk]) [Breast feeding Time 1st given breast milk],
 [Breast feeding Method of feeding],
 [Breast feeding Details],
 [Breastfeeding initiatives within 48 hours of delivery],
 dbo.func_Convert_Integer( [Time to first breast feed], 'hour(s)') [Time to first breast feed (hours)],
 dbo.func_Convert_Integer( [Time to first feed], 'hour(s)') [Time to first feed (hours)],
 dbo.func_Convert_Integer( [Time to first put to breast], 'hour(s)') [Time to first put to breast (hours)],
 [First feed],
 dbo.func_Convert_Date( [First feed Date of first feed]) [First feed Date of first feed],
 dbo.func_Convert_Time( [First feed Time of first feed]) [First feed Time of first feed],
 [First feed Method],
 [First feed Other],
 [Method of feeding at delivery],
 [Was the baby breast fed within 1 hour],

-- Cord Gases
 [Cord gases at delivery],
 dbo.func_Convert_Numeric53( [Cord gases at delivery Arterial pH], '') [Cord gases at delivery Arterial pH],
 dbo.func_Convert_Numeric52( [Cord gases at delivery Arterial Base Excess], 'mmol/l') [Cord gases at delivery Arterial Base Excess (mmol/l)],
 dbo.func_Convert_Numeric53( [Cord gases at delivery Venous pH], '') [Cord gases at delivery Venous pH],
 dbo.func_Convert_Numeric52( [Cord gases at delivery Venous Base Excess], 'mmol/l') [Cord gases at delivery Venous Base Excess (mmol/l) ],
 dbo.func_Convert_Numeric53( [Cord gases at delivery Lowest cord blood pH],'') [Cord gases at delivery Lowest cord blood pH],
 dbo.func_Convert_Numeric52( [Cord gases at delivery Arterial pC02], 'kPa') [Cord gases at delivery Arterial pC02 (kPa)],
 dbo.func_Convert_Numeric52( [Cord gases at delivery Venous pCO2], 'kPa') [Cord gases at delivery Venous pCO2 (kPa)],

-- Consultant Episode
 [Consultant at Delivery],
 [Specialty Code at Delivery],
 dbo.func_Convert_Date( [Consultant episode start date]) [Consultant episode start date],
 dbo.func_Convert_Time( [Consultant episode start time]) [Consultant episode start time],

-- Recording
 dbo.func_Convert_Date( [Date of recording information]) [Date of recording information],
 dbo.func_Convert_Time( [Time of recording information]) [Time of recording information],
 [Type of person recording],
 dbo.func_GetLink_StaffName( [Name of person recording]) [Person recording Name],
 dbo.func_GetLink_StaffCode( [Name of person recording]) [Person recording Code],
 [Name of person recording Grade] [Person recording Grade],

--- Other
 [Induced_abortion],
 [Booked_professional],
 [Twin_delivery],
 [Post_term_birth],
 [Stillbirth],
 [Rank_stillbth_livebth_nbr_born],
 [Obs_estimate_of_gestation],
 [Derived_fetal_distress],
 [Delivery_anaesthesia],
 [Normal_delivery],
 [Fetal_distress],
 [Shoulder_dystocia],
 [Prolonged_1st_stage],
 [Prolonged_2nd_stage],
 [Mid_forceps],
 [Management_of_Vaginal_breech],
 [Early_labour],
 [Local_anaesthesia_nerve_block],
 [Local_anaesthesia],
 [Low_cephalic_forceps_delivery],
 [Vacuum_delivery],
 [Elective_caesarean_delivery],
 [Emergency_caesarean_delivery],
 [Rotation_forceps],
 [Assisted_breech],
 [Spontaneous_breech]

 FROM 
 tbl_Baby












