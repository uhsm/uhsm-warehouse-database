﻿




CREATE VIEW dbo.view_Baby_Birth_StaffPresent
AS 
SELECT 
 [Patient_Pointer],
 Pregnancy_ID,
 [Pregnancy number],
 [Delivery_Number],
 [Baby_Number],
 dbo.func_Convert_Integer( [Birth Order], '') [Birth Order],

-- Staff/Persons present
 dbo.func_GetLink_MidwifeName( [Name of Assigned Midwife]) [Assigned Midwife Name],
 dbo.func_GetLink_MidwifeCode( [Name of Assigned Midwife]) [Assigned Midwife Code],
 [Name of Assigned Midwife Grade] [Assigned Midwife Grade],
 [Assigned Midwife present],
 [Delivered by Assigned Midwife],
 [Type of person delivering],
 dbo.func_GetLink_StaffName( [Name of person delivering]) [Person delivering Name],
 dbo.func_GetLink_StaffCode( [Name of person delivering]) [Person delivering Code],
 [Name of person delivering Grade] [Person delivering Grade],
 [Midwifery team delivering],
 [Was deliverer assisted],
 [Type of principal assistant],
 dbo.func_GetLink_StaffName( [Name of principal assistant]) [Principal assistant Name],
 dbo.func_GetLink_StaffCode( [Name of principal assistant]) [Principal assistant Code],
 [Name of principal assistant Grade] [Principal assistant Grade],
 [Partner present at delivery],
 [Others present at delivery],
 dbo.func_GetLink_MidwifeName( [First additional midwife]) [First additional midwife Name],
 dbo.func_GetLink_MidwifeCode( [First additional midwife]) [First additional midwife Code],
 [First additional midwife Grade],
 dbo.func_GetLink_MidwifeName( [Second additional midwife]) [Second additional midwife Name],
 dbo.func_GetLink_MidwifeCode( [Second additional midwife]) [Second additional midwife Code],
 [Second additional midwife Grade],
 dbo.func_GetLink_StaffName( [Obstetric Consultant present]) [Obstetric Consultant present Name],
 dbo.func_GetLink_StaffCode( [Obstetric Consultant present]) [Obstetric Consultant present Code],
 [Obstetric Consultant present Grade],
 dbo.func_GetLink_StaffName( [Scrub midwife]) [Scrub midwife Name],
 dbo.func_GetLink_StaffCode( [Scrub midwife]) [Scrub midwife Code],
 [Scrub midwife Grade],
 dbo.func_GetLink_StaffName( [Principal anaesthetist present]) [Principal anaesthetist present Name],
 dbo.func_GetLink_StaffCode( [Principal anaesthetist present]) [Principal anaesthetist present Code],
 [Principal anaesthetist present Grade],
 dbo.func_GetLink_StaffName( [Principal paed. present]) [Principal paediatrician present Name] ,
 dbo.func_GetLink_StaffCode( [Principal paed. present]) [Principal paediatrician present Code] ,
 [Principal paed. present Grade] [Principal paediatrician present Grade],
 dbo.func_GetLink_StaffName( [Principal Scrub Nurse]) [Principal Scrub Nurse Name],
 dbo.func_GetLink_StaffCode( [Principal Scrub Nurse]) [Principal Scrub Nurse Code],
 [Principal Scrub Nurse Grade],
 dbo.func_GetLink_StaffName( [Principal Floor Nurse]) [Principal Floor Nurse Name],
 dbo.func_GetLink_StaffCode( [Principal Floor Nurse]) [Principal Floor Nurse Code],
 [Principal Floor Nurse Grade],
 [Other individuals present],
 [Other individuals present Name_1],
 [Other individuals present Status_1],
 [Other individuals present Name_2],
 [Other individuals present Status_2],
 [Other individuals present Name_3],
 [Other individuals present Status_3],
 [Other individuals present Others],
 [Any familiar staff present],
 dbo.func_Convert_Integer( [No. of Midwives Involved],'') [Number of Midwives Involved]

 FROM 
 tbl_Baby




















