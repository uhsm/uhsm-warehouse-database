﻿





CREATE VIEW [dbo].[view_Baby_Details]
AS 
SELECT 
 [Patient_Pointer],
 Pregnancy_ID,
 [Pregnancy number],
 [Delivery_Number],
 [Baby_Number],
 dbo.func_Convert_Integer( [Birth Order], '') [Birth Order],

-- Baby Details
 [Sex of baby],
 dbo.func_Convert_Date( [Date of Birth]) [Date of Birth],
 [Birth register],
 [Birth register Filename],
 [Baby Discharge Summary Printed],
 [Baby Discharge Summary Printed filename],
 [Neonatal medical problems],
 [Neonatal medical problems Text],
 dbo.func_Convert_Integer( [Birth weight in grams], 'grams') [Birth weight (grams)],
 dbo.func_Convert_Integer( [Placenta Weight], 'grams') [Placenta Weight (grams)],
 dbo.func_Convert_Numeric51( [Length of Baby at Birth], 'cm') [Length of Baby at Birth (cm)],
 dbo.func_Convert_Integer( [Head circumference at Birth], 'cm') [Head circumference at Birth (cm)],
 dbo.func_Convert_Numeric51( [Baby`s first temperature], 'oC') [Baby's first temperature (oC)],
 dbo.func_Convert_Numeric52( [Ponderal Index], '') [Ponderal Index],
 [Babys blood group],
 [Babys rhesus group],
 [Direct coombs test],
 [Does baby look this gestation],
 [Does baby look this gestation Baby],
 dbo.func_Convert_Integer( [Does baby look this gestation Gestation estimate], 'weeks') [Does baby look this gestation Gestation estimate (weeks)],

 cast(replace([Babys NHS number],' ','') as varchar(14)) [Babys NHS number],
 [Babys NHS number status],
 dbo.func_Convert_Date( [Babys NHS number status Date updated]) [Babys NHS number status Date updated],
 dbo.func_Convert_Time( [Babys NHS number status Time updated]) [Babys NHS number status Time updated],
 [Babys NHS number status GUID],
 [Babys NHS number status Username],
 [Babys Casenote number],
 [Babys Hospital number],
 [Baby District number],
 [Birth Registration status],
 [Birth Registration status Comments],
 dbo.func_Convert_Date( [Birth Registration status Date updated]) [Birth Registration status Date updated],
 dbo.func_Convert_Time( [Birth Registration status Time updated]) [Birth Registration status Time updated],
 [Birth Registration status Username],
 [Korner return status],
 [Korner return status Username],
 dbo.func_Convert_Date( [Korner return status Date updated]) [Korner return status Date updated],
 dbo.func_Convert_Time( [Korner return status Time updated]) [Korner return status Time updated],
 [Korner return status Comments],
 [Babys Surname at discharge],
 [Internal Baby Reference],
 [Babys ethnic category],
 [Babys Surname],
 [Babys first name],
 [Is this also the Baby`s surname] [Is this also the Baby's surname],

 dbo.func_Convert_Integer( [Number of vessels in cord],'') [Number of vessels in cord],
 [Cord round neck],
 [Cord round neck Text],
 [True knot in cord],
 [Cord length],
 [Have cord samples been taken],
 [Have cord samples been taken Sample 1],
 [Have cord samples been taken Sample 2],
 [Have cord samples been taken Sample 3],
 [Have cord samples been taken Text],

 [Consent obtained for vitamin K],
 [1st Dose vitamin K given],
 dbo.func_Convert_Date( [1st Dose vitamin K given Date given]) [1st Dose vitamin K given Date given],
 dbo.func_Convert_Time( [1st Dose vitamin K given Time given]) [1st Dose vitamin K given Time given],
 [1st Dose vitamin K given Location],
 [1st Dose vitamin K given Route & Dose],
 [2nd Dose vitamin K given],
 dbo.func_Convert_Date( [2nd Dose vitamin K given Date given]) [2nd Dose vitamin K given Date given],
 dbo.func_Convert_Time( [2nd Dose vitamin K given Time given]) [2nd Dose vitamin K given Time given],
 [2nd Dose vitamin K given Location],
 [2nd Dose vitamin K given Route & Dose],
 [3rd Dose vitamin K given],
 dbo.func_Convert_Date( [3rd Dose vitamin K given Date given]) [3rd Dose vitamin K given Date given],
 dbo.func_Convert_Time( [3rd Dose vitamin K given Time given]) [3rd Dose vitamin K given Time given],
 [3rd Dose vitamin K given Location],
 [3rd Dose vitamin K given Route & Dose],

 [Admitted directly to NNU],
 [Admitted directly to NNU Reason],
 [Admitted directly to NNU Other],
 [Baby examined],
 [Baby examined Text],
 [Type of prsn who examined baby],
 dbo.func_GetLink_StaffName( [Name of prsn who examined baby]) [Person who examined baby Name],
 dbo.func_GetLink_StaffName( [Name of prsn who examined baby]) [Person who examined baby Code],
 [Name of prsn who examined baby Grade] [Person who examined baby Grade],
 [Paediatric Consultant on duty],
 [Paediatrician involved],
 [Paediatrician involved Text],

 [Congenital anomaly],
 [Congenital anomaly Downs],
 [Congenital anomaly Neural Tube Defect],
 [Congenital anomaly Absent Digits],
 [Congenital anomaly Cleft Lip],
 [Congenital anomaly Cleft Palate],
 [Congenital anomaly Diagnosed Antenatally],
 [Congenital anomaly Other],
 [Other congenital anomalies],
 [Other congenital anomalies Text]

 FROM 
 tbl_Baby








