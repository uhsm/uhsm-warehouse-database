﻿




CREATE VIEW dbo.view_Baby_PostNatal
AS 
SELECT 
 [Patient_Pointer],
 [Pregnancy_ID],
 [Pregnancy number],
 [Delivery_Number],
 [Baby_Number],
 dbo.func_Convert_Integer( [Birth Order], '') [Birth Order],

-- Postnatal
 dbo.func_Convert_Date( [Date of Baby PN Assessment]) [Date of Baby PN Assessment],
 dbo.func_Convert_Time( [Time of Baby PN Assessment]) [Time of Baby PN Assessment],
 [Baby assessment completed by],
 [Baby assessment completed by Status],
 [Type of persn doing assessment],

 [Baby screening completed by],
 [Baby screening completed by Status],

 [Is the child with you now],
 [Is the child with you now Reason],
 [Is the child with you now Text],
 [Child still with mother],
 [Child still with mother Reason],
 [Child still with mother Text],
 [Childs condition now],
 [Childs condition now Text],

 [Cord status at discharge],
 [Baby Discharged To],
 [Is baby discharged with mother],
 [Is baby discharged with mother Because],
 [Is baby discharged with mother Text],
 dbo.func_Convert_Integer( [Discharge Weight], 'grams') [Discharge Weight (grams)],
 dbo.func_Convert_Integer( [Baby weight at com. discharge], 'grams') [Baby weight at community discharge (grams)],
 [Babys Discharge Summary],
 [Babys Discharge Summary Filename],

 [Outpatients appt to be sent],
 [Post Discharge baby illness],
 [Post Discharge baby illness Details],

 [Planned feeding mthd at disch.] [Planned feeding mthd at discharge],
 [Method of feeding at discharge],
 dbo.func_Convert_Date( [Date feeding method changed]) [Date feeding method changed],
 [Date feeding method changed Reason],
 [Feeding at 10 days],
 [Feeding pattern],
 [Feeding problems],
 [Feeding problems Text],
 [Reason for ending breast feeds],

 [Additional comments],
 [Additional comments Text],

 [Postnatal admission to NNU],
 [Postnatal admission to NNU Text],
 [Is the baby still on NNU],
 [Length of Neonatal Unit stay],

 [Presence/Severity of Jaundice],
 dbo.func_Convert_Integer( [Presence/Severity of Jaundice Maximum SBR], 'umol/l') [Presence/Severity of Jaundice Maximum SBR (umol/l)],
 [Presence/Severity of Jaundice Phototherapy],

 [Hypoglycaemia],
 dbo.func_Convert_Numeric51( [Hypoglycaemia Lowest glucose], 'mmol/l') [Hypoglycaemia Lowest glucose (mmol/l)],

 [Baby temperature],

 [Confirmed Congenital anomalies],
 [Confirmed Congenital anomalies Text],

 [Any baby problems],
 [Any baby problems Text],

 [Baby Problems at Discharge],
 [Baby Problems at Discharge Irritability],
 [Baby Problems at Discharge Infection],
 [Baby Problems at Discharge Jaundice],
 [Baby Problems at Discharge Nappy Rash],
 [Baby Problems at Discharge Other Rash],
 [Baby Problems at Discharge Constipation],
 [Baby Problems at Discharge Text],

 [Any skin problems detected],
 [Any skin problems detected Bruising],
 [Any skin problems detected Rash],
 [Any skin problems detected Naevus],
 [Any skin problems detected Text],

 [Urine passed],

 [Bowels opened],
 [Bowels opened Stool],

 [Congenital anomaly at birth 1],
 [Congenital anomaly at birth 1 Text],
 [Congenital anomaly at birth 2],
 [Congenital anomaly at birth 2 Text],
 [Congenital anomaly at birth 3],
 [Congenital anomaly at birth 3 Text],

 [Follow up arrangements],
 [Follow up arrangements Text],

 [Neonatal diagnosis 1],
 [Neonatal diagnosis 1 Text],
 [Neonatal diagnosis 2],
 [Neonatal diagnosis 2 Text],
 [Neonatal diagnosis 3],
 [Neonatal diagnosis 3 Text],
 [Neonatal diagnosis 4],
 [Neonatal diagnosis 4 Text],
 [Neonatal diagnosis 5],
 [Neonatal diagnosis 5 Text],
 [Neonatal diagnosis 6],
 [Neonatal diagnosis 6 Text],

 [Reason for Rejection],

 [Recommendations],
 [Recommendations Text],

 [Referrals made],
 [Referrals made Audiologist],
 [Referrals made Orthopaedic surgeon],
 [Referrals made Other],
 [Referrals made Second],

-- Innoculations

 [Reason BCG not given],
 dbo.func_Convert_Date( [Date BCG given]) [Date BCG given],
 [BCG accepted],
 [BCG given],
 [BCG given Details],
 [BCG injection],
 [BCG injection Comments],
 [BCG injection Injection site],
 [BCG injection Location],
 [BCG injection Batch number],
 dbo.func_Convert_Date( [BCG injection Date given]) [BCG injection Date given],
 dbo.func_Convert_Time( [BCG injection Time given]) [BCG injection Time given],
 [BCG offered],

 [Hepatitis B Risk],
 [Hepatitis Vaccine],
 [Hepatitis Vaccine Injection site],
 [Hepatitis Vaccine Location],
 [Hepatitis Vaccine Batch number],
 dbo.func_Convert_Date( [Hepatitis Vaccine Date given]) [Hepatitis Vaccine Date given],
 dbo.func_Convert_Time( [Hepatitis Vaccine Time given]) [Hepatitis Vaccine Time given],

-- Screening
 [Congen. Hip Dislocation Risk] [Congenital Hip Dislocation Risk],
 [Congen. Hip Dislocation Risk Scan] [Congenital Hip Dislocation Risk Scan],
 [Congen. Hip Dislocation Risk Text] [Congenital Hip Dislocation Risk Text],

 [Eyes],
 [Eyes Swab],

 [Details of Hearing screening],
 [Hearing Risk Factors],
 [Hearing Risk Factors Text],
 [Hearing screening accepted],
 [Hearing screening offered],
 [Hearing screening offered Reason],
 [Place Hearing screening test taken 1],
 [Place Hearing screening test taken 1 Text],
 [Place Hearing screening test taken 2],
 [Result of Hearing screening test - Left Ear],
 [Result of Hearing screening test - Right Ear],

 dbo.func_Convert_Date( [Date Hearing screening test taken]) [Date Hearing screening test taken],
 [Hearing test],
 [Hearing test left ear],
 [Hearing test right ear],

 [Hip screening scan],

 [Maternal ABO/Rhesus antibodies],
 [Maternal ABO/Rhesus antibodies Text],

 [Metabolic Screen (PKU)],
 dbo.func_Convert_Date( [Metabolic Screen (PKU) When]) [Metabolic Screen (PKU) When],
 [Metabolic Screen (PKU) Result],

 [Serum bilirubin result],
 [Serum bilirubin result Max SBR],

 [Umbilicus],
 [Umbilicus Swab],

-- Examinations

 [Abdominal examination],
 [Abdominal examination Abdomen],
 [Abdominal examination Spleen],
 [Abdominal examination Liver],
 [Abdominal examination Kidneys],
 [Abdominal examination Anus],
 [Abdominal examination Umbilicus],
 [Abdominal examination Genitalia],
 [Abdominal examination Text],

 [Cardiovascular examination],
 [Cardiovascular examination Heart sounds],
 [Cardiovascular examination Heart murmurs],
 [Cardiovascular examination Femoral pulses],
 [Cardiovascular examination Text],

 [CNS examination],
 [CNS examination Muscle tone],
 [CNS examination Posture],
 [CNS examination Movement],
 [CNS examination Cry],
 [CNS examination Grasp],
 [CNS examination Moro],
 [CNS examination Text],

 [Facial examination],
 [Facial examination Face],
 [Facial examination Red reflex],
 [Facial examination Eyes],
 [Facial examination Ears],
 [Facial examination Nose],
 [Facial examination Palate],
 [Facial examination Mouth],
 [Facial examination Text],

 [General examination],
 [General examination Colour],
 [General examination Skin],
 [General examination Body shape],
 [General examination Text],

 [Head and neck examination],
 [Head and neck examination Sutures],
 [Head and neck examination Fontanelles],
 [Head and neck examination Head],
 [Head and neck examination Neck],
 [Head and neck examination Text],

 [Hip examination],
 [Hip examination Text],

 [Limb/back examination],
 [Limb/back examination Arms],
 [Limb/back examination Hands],
 [Limb/back examination Spine],
 [Limb/back examination Legs],
 [Limb/back examination Feet],
 [Limb/back examination Text],

 [Respiratory examination],
 [Respiratory examination Chest auscultation],
 [Respiratory examination Text],

 [Other examination findings],
 [Other examination findings Text]

 FROM 
 tbl_Baby

















