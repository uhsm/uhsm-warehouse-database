﻿




CREATE VIEW dbo.view_Baby_Resuscitation
AS 
SELECT 
 [Patient_Pointer],
 [Pregnancy_ID],
 [Pregnancy number],
 [Delivery_Number],
 [Baby_Number],
 dbo.func_Convert_Integer( [Birth Order], '') [Birth Order],

-- Resuscitation
 [Resuscitation attempted],
 [Method of Resuscitation 1],
 [Method of Resuscitation 2],
 [Method of Resuscitation 3],
 [Method of Resuscitation 4],
 [Method of Resuscitation 5],
 [Method of Resuscitation 6],
 [Method of Resuscitation 7],
 [Method of Resuscitation 8],

 [Drugs given for Resuscitation],
 [Drugs given for Resuscitation Naloxone_(Narcan)] [Drugs given for Resuscitation Naloxone (Narcan)],
 [Drugs given for Resuscitation Dextrose],
 [Drugs given for Resuscitation Bicarbonate],
 [Drugs given for Resuscitation Atropine],
 [Drugs given for Resuscitation Adrenaline],
 [Drugs given for Resuscitation Other],
 [Type of person giving drugs],
 dbo.func_GetLink_StaffName( [Name of person giving drugs]) [Person giving drugs Name],
 dbo.func_GetLink_StaffName( [Name of person giving drugs]) [Person giving drugs Code],
 [Name of person giving drugs Grade] [Person giving drugs Grade],

 [Delay to first heart beat],
 [Delay to first heart beat Cardiac massage given],
 [Delay to first heart beat First beat at],
 [Delay to first breath],
 dbo.func_Convert_Integer( [Delay to first breath First breath], 'mins') [Delay to first breath First breath (mins)],
 [Spont. respiration achieved] [Spontaneous respiration achieved],
 dbo.func_Convert_Integer( [Spont. respiration achieved At minute], 'mins') [Spontaneous respiration achieved At minute (mins)],

 dbo.func_Convert_Integer( [Apgar Score at 1 minute], '') [Apgar Score at 1 minute],
 dbo.func_Convert_Integer( [Apgar Score at 3 minutes], '') [Apgar Score at 3 minutes],
 dbo.func_Convert_Integer( [Apgar Score at 5 minutes], '') [Apgar Score at 5 minutes],
 dbo.func_Convert_Integer( [Apgar Score at 10 minutes], '') [Apgar Score at 10 minutes],
 [Apgar at 1 min breakdown],
 [Apgar at 1 min breakdown Colour],
 [Apgar at 1 min breakdown Tone],
 [Apgar at 1 min breakdown Heart_rate],
 [Apgar at 1 min breakdown Respiration],
 [Apgar at 1 min breakdown Response_to_stimulus],
 [Apgar at 3 min breakdown],
 [Apgar at 3 min breakdown Colour],
 [Apgar at 3 min breakdown Tone],
 [Apgar at 3 min breakdown Heart_rate],
 [Apgar at 3 min breakdown Respiration],
 [Apgar at 3 min breakdown Response_to_stimulus],
 [Apgar at 5 min breakdown],
 [Apgar at 5 min breakdown Colour],
 [Apgar at 5 min breakdown Tone],
 [Apgar at 5 min breakdown Heart_rate],
 [Apgar at 5 min breakdown Respiration],
 [Apgar at 5 min breakdown Response_to_stimulus],
 [Apgar at 10 min breakdown],
 [Apgar at 10 min breakdown Colour],
 [Apgar at 10 min breakdown Tone],
 [Apgar at 10 min breakdown Heart_rate],
 [Apgar at 10 min breakdown Respiration],
 [Apgar at 10 min breakdown Response_to_stimulus],
 [Total Care Resuscitation],
 [National Resuscitation Code]
 FROM 
 tbl_Baby













