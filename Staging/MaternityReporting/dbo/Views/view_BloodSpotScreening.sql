﻿


CREATE VIEW dbo.view_BloodSpotScreening
AS 
SELECT 
[Patient_Pointer],
[Pregnancy number],
[Delivery_Number],
[Baby_Number],
-- ZMPBh	Newborn Blood Spot screening
[Blood spot screening offered],
[Blood spot screening offered Reason],
[Blood spot screening accepted],
[Blood spot screening accepted Reason],
[Type of person taking test],
[Name of person taking test],
[Grade of person taking test],
dbo.func_Convert_Date( [Date blood spot specimen taken] ) [Date blood spot specimen taken],
dbo.func_Convert_Date( [Date blood spot specimen dispatched] ) [Date blood spot specimen dispatched],
[Consent obtained for PKU screening],
[Consent obtained for PKU screening Reason],
[Consent obtained for TSH screening],
[Consent obtained for TSH screening Reason],
[Consent obtained for Sickle cell screening],
[Consent obtained for Sickle cell screening Reason],
[Consent obtained for Cystic Fibrosis screening],
[Consent obtained for Cystic Fibrosis screening Reason],
[Consent obtained for MCADD screening],
[Consent obtained for MCADD screening Reason],
[Consent obtained for future research contact],
[Consent obtained for future research contact Reason],
[Outcome of Bloodspot screening],
[Outcome of Bloodspot screening Details],
[Outcome of PKU screening],
[Outcome of PKU screening Details],
[Outcome of TSH screening],
[Outcome of TSH screening Details],
[Outcome of Sickle cell screening],
[Outcome of Sickle cell screening Details],
[Outcome of Cystic Fibrosis screening],
[Outcome of Cystic Fibrosis screening Details],
[Outcome of MCADD screening],
[Outcome of MCADD screening Details],
[Repeat newborn blood spot screening],
-- ZMPRh	Repeat Newborn Blood Spot screening
[Repeat blood spot screening offered],
[Repeat blood spot screening offered Reason],
[Repeat blood spot screening accepted],
[Repeat blood spot screening accepted Reason],
[Type of person taking repeat test],
[Name of person taking repeat test],
[Grade of person taking repeat test],
dbo.func_Convert_Date( [Date repeat blood spot specimen taken] ) [Date repeat blood spot specimen taken],
dbo.func_Convert_Date( [Date repeat blood spot specimen dispatched] ) [Date repeat blood spot specimen dispatched],
[Consent obtained for repeat PKU screening],
[Consent obtained for repeat PKU screening Reason],
[Consent obtained for repeat TSH screening],
[Consent obtained for repeat TSH screening Reason],
[Consent obtained for repeat Sickle cell screening],
[Consent obtained for repeat Sickle cell screening Reason],
[Consent obtained for repeat Cystic Fibrosis screening],
[Consent obtained for repeat Cystic Fibrosis screening Reason],
[Consent obtained for repeat MCADD screening],
[Consent obtained for repeat MCADD screening Reason],
[Consent obtained for repeat future research contact],
[Consent obtained for repeat future research contact Reason],
[Outcome of repeat Bloodspot screening],
[Outcome of repeat Bloodspot screening Details],
[Outcome of repeat PKU screening],
[Outcome of repeat PKU screening Details],
[Outcome of repeat TSH screening],
[Outcome of repeat TSH screening Details],
[Outcome of repeat Sickle cell screening],
[Outcome of repeat Sickle cell screening Details],
[Outcome of repeat Cystic Fibrosis screening],
[Outcome of repeat Cystic Fibrosis screening Details],
[Outcome of repeat MCADD screening],
[Outcome of repeat MCADD screening Details]
FROM
tbl_BloodSpotScreening







