﻿
CREATE VIEW [dbo].[view_Cervicogram]
AS SELECT
	[Patient_Pointer] ,
	[Pregnancy_ID] ,
	[Pregnancy number] ,
	[Examination_Number] ,
	dbo.func_Convert_Numeric52 ( [Hours in labour] ,'') [Hours in labour],
	dbo.func_Convert_Date( [Date of Vaginal Examination]) [Date of Vaginal Examination],
	dbo.func_Convert_Time( [Time of Vaginal Examination]) [Time of Vaginal Examination] ,
	dbo.func_Convert_Integer ( [Cervical dilation] , 'cm') [Cervical dilation (cm)],
	dbo.func_Convert_Date( [Date of Review]) [Date of Review],
	dbo.func_Convert_Time( [Time of Review] ) [Time of Review],
	[Type of Review] ,
	dbo.func_Convert_Integer ( [Birth Order] ,'') [Birth Order]
FROM
tbl_Cervicogram




