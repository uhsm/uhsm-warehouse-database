﻿





CREATE VIEW dbo.view_ClinicalReviews
AS 
SELECT 
 [Patient_Pointer],
 [Pregnancy_ID],
 [Pregnancy number],
 [Review_Number],

-- Review Details

 dbo.func_Convert_Date( [Date of review]) [Date of review],
 dbo.func_Convert_Time( [Booked time of review]) [Booked time of review],
 dbo.func_Convert_Time( [Time of review]) [Time of review],

 [Review source],
 [Review source Text],
 [Review status],

 [Type of review],
 [Antenatal or postnatal review],
 [Primary reason for review],
 [Primary reason for review Text],
 [Secondary reason for review Text],
 [Secondary reason for review],
 [Care level],
 [NNNI Category],
 [Day],
 [Location of review],

 dbo.func_Convert_Integer( [Gestation at review], 'weeks') [Gestation at review (weeks)],
 dbo.func_Convert_Integer( [Gestation at review Days], 'days') [Gestation at review (days)],

 dbo.func_Convert_Time( [Time of arrival]) [Time of arrival],
 dbo.func_Convert_Time( [Time patient left]) [Time patient left],

 dbo.func_Convert_HrsMins( [Duration of review]) [Duration of review (hrs:mins)],
 [Category of review],
 [Principal activity],
 [Secondary activity 1],
 [Secondary activity 2],

 dbo.func_Convert_Date( [Date review completed]) [Date review completed],
 dbo.func_Convert_Time( [Time review completed]) [Time review completed],
 [Clinical review completed by],

 [Conclusion from review],
 [Conclusion from review Text],

 [Outcome of review],

 [Review follow up],
 dbo.func_Convert_Date( [Review follow up Recalldate]) [Review follow up Recalldate],
 [Review follow up Text],

 dbo.func_Convert_Integer( [No. of days postnatal],'') [Number of days postnatal],
 [Domicillary fetal monitoring],
 [Kick chart arranged],
 [Scan arranged],
 dbo.func_Convert_Date( [Scan arranged When]) [Scan arranged When],
 [Scan arranged Result],

 [Final diagnosis],
 [Pathology interface results],

 [Review comments],
 [Review comments Text],

 [Procedure planned],
 [Any procedures planned],
 [Any procedures planned Procedure 1],
 [Any procedures planned Procedure 2],
 dbo.func_Convert_Date( [Any procedures planned Recalldate]) [Any procedures planned Recalldate],

-- Clinician
 [Name of clinician],
 [Name of clinician Grade],
 [Type of clinician],
 [Clinicians grade],
 [Booked clinician code],

-- Scan

 [Reason for scan],
 [Name of person requesting scan],
 [Visit number],
 [Booked for this pregnancy],
 [Diary event],
 [Diary event Description of event],
 [Scan ID],
 [Scan machine used],
 [Scan Operator],
 [Scanning method used],
 dbo.func_Convert_Date( [EDD from scan]) [EDD from scan],
 [Comments on scan],
 [Comments on scan Text],

-- Procedure

 dbo.func_Convert_Date( [Date procedure started]) [Date procedure started],
 dbo.func_Convert_Time( [Time procedure started]) [Time procedure started],
 [Timing of procedure],

 [Principle procedure],
 [Principle procedure Procedure 2],
 [Principle procedure Procedure 3],
 [Principle procedure Procedure 4],
 [Principle procedure Text],

 [Location of procedure Text],
 [Location of procedure],
 [Reason for procedure],
 [Reason for procedure Text],

 dbo.func_Convert_Date( [Date anaesthetist called]) [Date anaesthetist called],
 dbo.func_Convert_Time( [Time anaesthetist called]) [Time anaesthetist called],
 dbo.func_Convert_Date( [Date anaesthetist attended]) [Date anaesthetist attended],
 dbo.func_Convert_Time( [Time anaesthetist attended]) [Time anaesthetist attended],

 [Name of anaesthetist],
 [Name of anaesthetist Grade],
 [Type of anaesthetist],
 [Was anaesthetist supervised],
 [Name of supervising anaes.] [Name of supervising anaesthetist],
 [Name of supervising anaes. Grade] [Name of supervising anaesthetist Grade],
 [Any other anaesthetist present],
 [1st additional anaes.] [1st additional anaesthetist],
 [1st additional anaes. Grade] [1st additional anaesthetist Grade],
 [2nd additional anaes.] [2nd additional anaesthetist],
 [2nd additional anaes. Grade] [2nd additional anaesthetist Grade],
 [Reason for more than one anaes] [Reason for more than one anaesthetist],
 [Reason for more than one anaes Text] [Reason for more than one anaesthetist Text],

 [Type of procedure],
 dbo.func_Convert_Integer( [Gestation at procedure], 'weeks') [Gestation at procedure (weeks)],

 dbo.func_Convert_Date( [Date procedure completed]) [Date procedure completed],
 dbo.func_Convert_Time( [Time procedure completed]) [Time procedure completed],
 dbo.func_Convert_HrsMins( [Procedure duration (hrs:mins)]) [Procedure duration (hrs:mins)],
 [Procedure comments],
 [Procedure comments Text],

 dbo.func_Convert_Date( [Date combined proc. started]) [Date combined procedure started],
 dbo.func_Convert_Time( [Time combined proc. started]) [Time combined procedure started],

 dbo.func_Convert_Date( [Date spinal started]) [Date spinal started],
 dbo.func_Convert_Time( [Time spinal started]) [Time spinal started],

 dbo.func_Convert_Date( [Date G.A started]) [Date GA started],
 dbo.func_Convert_Time( [Time G.A started]) [Time GA started],

 [Anaesthetic discharge comments],
 [Anaesthetic discharge comments Text],

-- External Cephalic Version

[External cephalic version offered],
[External cephalic version offered Reason],
[External cephalic version accepted],
dbo.func_Convert_Date( [Date ECV Performed] ) [Date ECV Performed],
dbo.func_Convert_Time( [Time ECV Performed] ) [Time ECV Performed],
[Type of person performing ECV],
[Name of person performing ECV],
[Grade of person performing ECV],
[Immediate outcome of ECV],
[Immediate outcome of ECV Details],

-- Induction of Labour

[Membrane sweep offered],
[Membrane sweep offered Reason],
[Membrane sweep accepted],
dbo.func_Convert_Date( [Date membrane sweep performed] ) [Date membrane sweep performed],
dbo.func_Convert_Time( [Time membrane sweep performed] ) [Time membrane sweep performed],
[Type of clinician performing membrane sweep],
[Name of clinician performing membrane sweep],
[Grade of clinician performing membrane sweep],
[Outcome of membrane sweep],
[Outcome of membrane sweep Details],

-- Letters

 [Admission Letter Filename],
 [Admission Letter],
 [Clinic Letter],
 [Clinic Letter Filename],
 [Discharge Letter],
 [Discharge Letter Filename],
 [Patient Letter],
 [Patient Letter Filename],
 [Transfer Letter],
 [Transfer Letter Filename],
 [Interim Letter],
 [Interim Letter Filename],

-- ???

 [Create a message to other users],
 [Create a message to other users Text],
 [Add comments to user messages],

 [Review episode number],
 [Review event number],
 [Review Deletion allowed]

 FROM 
 tbl_ClinicalReviews






















