﻿





CREATE VIEW dbo.view_DeathOfBaby
AS 
SELECT 
 [Patient_Pointer],
 [Pregnancy_ID],
 [Pregnancy number],
 [Delivery_Number],
 [Baby_Number],

 [Type of person delivering],
 [Name of person delivering],
 [Name of person delivering Grade],

 dbo.func_Convert_Date( [Date death diagnosed]) [Date death diagnosed],
 dbo.func_Convert_Time( [Time death diagnosed]) [Time death diagnosed],
 [Timing of death],
 [Where did baby die],
 [Where did baby die Name of unit or place],
 dbo.func_Convert_Date( [Date last known alive]) [Date last known alive],
 dbo.func_Convert_Time( [Date last known alive Time])  [Date last known alive Time],
 dbo.func_Convert_Integer( [Gestation at first scan], 'weeks') [Gestation at first scan (weeks)],
 dbo.func_Convert_Integer( [Gestation at first scan Days], 'days') [Gestation at first scan (days)],
 dbo.func_Convert_Integer( [Gestation death Confirmed], 'weeks') [Gestation death Confirmed (weeks)],
 dbo.func_Convert_Integer( [Gestation death Confirmed Days], 'days') [Gestation death Confirmed (days)],
 [Was there evidence of fetal growth restriction],
 [Was there evidence of fetal growth restriction Evidence],
 [Was this a legal abortion],
 [Case definition],
 [Wigglesworth Classification],

 [Clinical cause of death],
 [Clinical cause of death Text],
 [Fetal/Neonatal Classification],
 [Obstetric Classification],
 [Category of child death],

 [Post mortem],
 [Planned follow up of parents],
 [Planned follow up of parents Text],
 [Condition at birth],
 [Condition at birth Audible cry],
 [Condition at birth Spontaneous breathing],
 [Condition at birth Heart beat detected],
 [Primary cause of death],
 [Secondary cause of death 1],
 [Secondary cause of death 2],
 [Secondary cause of death 3],
 [Death in Region of Residence],
 [Death in Region of Residence Region],
 [Same address at time of death],
 [Same address at time of death House_Number],
 [Same address at time of death Address_1],
 [Same address at time of death Address_2],
 [Same address at time of death Address_3],
 [Same address at time of death Postcode]

 FROM 
 tbl_DeathOfBaby












