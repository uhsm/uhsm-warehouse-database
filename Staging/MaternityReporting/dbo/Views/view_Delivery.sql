﻿






CREATE VIEW [dbo].[view_Delivery]
AS 
SELECT 
 [Patient_Pointer],
 Pregnancy_ID,
 [Pregnancy number],
 [Delivery_Number],

-- Delivery
 [Type of delivery],
 [Multiple delivery anticipated],
 [Number born this delivery],

 [Delivery admission],
 dbo.func_Convert_Date( [Date of delivery admission]) [Date of delivery admission],
 dbo.func_Convert_Time( [Time of delivery admission]) [Time of delivery admission],

 [Delivery episode number],
 dbo.func_Convert_Date( [Date of delivery]) [Date of delivery],
 dbo.func_Convert_Time( [Time of first Birth]) [Time of first Birth],
 [Birth Rate Plus Category],
 [SMR Op Deliv ICD10 Code],
 dbo.func_Convert_Integer( [Mothers age at delivery], 'years') [Mothers age at delivery],
 [Most significant Birth Method],

 [Consultant on call],
 [Most senior doctor present],
 [Most senior midwife present],

 [Baby died immed. after birth] [Baby died immediately after birth],

-- Third Stage
 dbo.func_Convert_Date( [Date of placental delivery]) [Date of placental delivery],
 dbo.func_Convert_Time( [Time of placental delivery]) [Time of placental delivery],
 dbo.func_Convert_HrsMins( [Length of 3rd stage(hrs:mins)]) [Length of 3rd stage (hrs:mins)],
 [Management of Third Stage],
 [Management of Third Stage Text],
 [Oxytocics in third stage],
 [Oxytocics in third stage Drug 1],
 [Oxytocics in third stage Drug 2],
 [Oxytocics in third stage Drug 3],
 [Oxytocics in third stage Text],
 [Placental condition],
 [Placental condition Text],
-- missing Placenta weight
 [Completeness of placenta],
 [Completeness of placenta Text],
 [Completeness of membranes],
 [Completeness of membranes Text],
 [Was there placenta praevia],
 [Was there placenta praevia Type],
 [Retained placenta],
 [Patient rhesus negative],
 [Patient rhesus negative Cord samples taken],
 [Patient rhesus negative Maternal samples taken],
 [Patient rhesus negative Text],
 dbo.func_Convert_HrsMins( [Tot. length labour (hrs:mins)]) [Total length of labour (hrs:mins)],
 dbo.func_Convert_HrsMins( [Time in labour (hrs:mins)]) [Time in labour (hrs:mins)],

 [Episiotomy performed],
 [Degree of perineal tearing],
 [Vaginal or cervical damage],
 [Vaginal or cervical damage Vaginal Tear],
 [Vaginal or cervical damage Labial Tear],
 [Vaginal or cervical damage Cervical Tear],
 [Vaginal or cervical damage Other],
 [Damage required suturing],
 [Grade of person suturing],
 [Name of suturing person],
 [Was suturing person supervised],
 [Type of person supervising],
 [Name of person supervising],
 [Name of person supervising Grade],
 [Suture material used],
 [Suture material used Text],
 [2nd Suture material used],
 [2nd Suture material used Text],
 [3rd Suture material used],
 [3rd Suture material used Text],
 dbo.func_Convert_Integer( [Number of sutures],'') [Number of sutures],
 [Any suturing analg/anaes],
 [Any suturing analg/anaes Self_admin._inhalation] [Any suturing analg/anaes Self_admin_inhalation],
 [Any suturing analg/anaes Pudendal_Block],
 [Any suturing analg/anaes Local_Infiltration],
 [Any suturing analg/anaes Epidural],
 [Any suturing analg/anaes Spinal],
 [Any suturing analg/anaes Caudal],
 [Any suturing analg/anaes General_Anaesthetic],
 [Any suturing analg/anaes Combined spinal/epidural],
 [Hospital follow-up of perineum],

 [Type of skin incision],
 [Type of skin incision Method],
 [Skin closure material],
 [Remove skin sutures],
 [Remove skin sutures Text],
 [Type of uterine incision],
 [Type of uterine incision Method],
 [Uterine closure material],

 dbo.func_Convert_Integer( [Total Blood Loss], 'mls') [Total Blood Loss (mls)],
 [Early post-partum haemorrhage],
 [Early post-partum haemorrhage Probable Reason],
 [Early post-partum haemorrhage Text],
 [Blood Transfusion in labour],
 [Blood Transfusion in labour Number of units],

 [Any maternal problems],
 [Any maternal problems Text],

 [Intrapartum problems],
 [Intrapartum problems Problem 1],
 [Intrapartum problems Problem 2],
 [Intrapartum problems Problem 3],
 [Intrapartum problems Text],

 [Post delivery problems],
 [Post delivery problems Problem 1],
 [Post delivery problems Problem 2],
 [Post delivery problems Problem 3],
 [Post delivery problems Text],

 [Uterine state post-partum],
 [Uterine state post-partum Text],
 [Per vaginum loss],
 [Per vaginum loss Text],
 [Catheterised during delivery],
 [Catheterised during delivery Technique],
 [Catheterised during delivery Catheter in situ],
 [Catheterised during delivery Details],
 [Urine passed],

 [Epidural catheter removed],
 [Removal of epidural catheter],
 [Type of prsn removing catheter],
 [Name of prsn removing catheter],
 [Name of prsn removing catheter Grade],

 [Analgesia/Anaes During Delivery],
 [Any P/N Analgesia/Anaesthesia],
 [Continued Birth Anaesthesia],
 [Continued Birth Anaesthesia Local Anaesthetic],
 [Continued Birth Anaesthesia Spinal],
 [Continued Birth Anaesthesia Epidural],
 [Continued Birth Anaesthesia General Anaesthesia],
 [Continued Birth Anaesthesia Other],
 [Reason for P/N Analg/Anaes],

 [Other IV Infusion post-deliv.] [Other IV Infusion post-delivery],
 [Other IV Infusion post-deliv. Infusion] [Other IV Infusion post-delivery Infusion],

 [Critical incident],
 [Critical incident Text],
 dbo.func_Convert_Date( [Date of incident]) [Date of incident],
 dbo.func_Convert_Time( [Time of incident]) [Time of incident],
 [Location of incident],
 [Was any equipment involved],
 [Was any equipment involved Text],
 [Staff Type reporting incident],
 [Name reporting an incident],
 [Name reporting an incident Grade],
 [Case of special interest],
 [Case of special interest Text],
 [Special interest memo required],
 [Special interest memo required Recipient],

 [Feeding Intention at delivery],
 [Method of feeding at delivery],
 [Current smoker at delivery],
 dbo.func_Convert_Integer( [Current smoker at delivery Currently], 'per day') [Current smoker at delivery Currently (per day)],

 [Smoking at delivery],
 dbo.func_Convert_Integer( [Smoking at delivery Amount smoked currently], 'per day') [Smoking at delivery Amount smoked currently (per day)],
 [At delivery - ready to quit smoking],
 [At delivery - referred for help quitting smoking?],
 [At delivery - referred for help quitting smoking? Type of help to which refd.] [At delivery - Type of help to which refd],

-- Risk Assessment
 [Gross Varicous Veins],
 [Current Infection],
 [Immobility > 4days],
 [Major current illness],
 [Ext mjr pelvic/abdo surgery],
 [Thrombophilia],
 [Paralysis of lower limbs],
 [Antiphospholipid antibody],
 [Prophylaxis Risk Score],
 [Prophylaxis Risk Action],

-- Other

 [IV Infusion in labour],
 [IV Infusion in labour Induction],
 [IV Infusion in labour Augmentation],
 [IV Infusion in labour Other],

 [Medical problems in labour],
 [Medical problems in labour Problem],

 [Mothers attitude to Caesarean],
 [Mother sterilised at Caesarean],
 [C/S hysterectomy performed],
 [C/S hysterectomy performed Text],
 [Closure following section],
 [Closure following section Details],

 [Normal intra-abdomen findings],
 [Normal intra-abdomen findings Findings],
 [Have samples been sent],
 [Drains inserted],
 [Drains inserted Text],
 [Check P.R. P.V.] [Check PR PV],
 [Correct swab count],
 [Correct needle count],
 [Additional operative details],
 [Additional operative details Details],

 [Other intensive care input],
 [Other intensive care input Care Required],

-- Other
 [Korner insert or update],
 [Birth notif insert or update],
 [Sterilised_at_caesarean],
 [Birth_weight_indicator],
 [Very_premature],
 [Premature],
 [Severe_birth_asphix],
 [Age_at_confinement],
 [Elderly_primigravida],
 [Most_significant_birth_method],
 [Man_removal_of_placenta],
 [Episiotomy],
 [Perineal_damage],
 [Type_I_perineal_damage],
 [Type_II_perineal_damage],
 [Type_III_or_IV_perineal_damage],
 [Perineal_damage_repair],
 [Type_I_damage_repaired],
 [Type_II_damage_repaired],
 [Type_III_or_IV_repaired],
 [Early_post_partum_haemorrhage],
 [Blood_transfusion_in_labour],
 [Normal_most_sig_birth_method],
 [Forceps_most_sig_birth_method],
 [Vacuum_most_sig_birth_method],
 [Breech_most_sig_birth_method],
 [Caesar_most_sig_birth_method],
 [ERPC_performed],
 [Secondary_PPH],
 [Blood_transfusion_given],
 [total_number_of_pregnancies]

 FROM 
 tbl_Delivery















