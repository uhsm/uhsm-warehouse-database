﻿




CREATE VIEW dbo.view_Delivery_Labour
AS 
SELECT 
 [Patient_Pointer],
 Pregnancy_ID,
 [Pregnancy number],
 [Delivery_Number],

-- Labour
 [Was there labour before birth],
 [Type of onset of labour],

 dbo.func_Convert_Date( [Date labour established]) [Date labour established],
 dbo.func_Convert_Time( [Time labour established]) [Time labour established],
 dbo.func_Convert_HrsMins( [Total time on labour ward]) [Total time on labour ward (hrs:mins)],

 [Type of augmentation],
 dbo.func_Convert_Date( [Type of augmentation ARM_Date]) [Type of augmentation ARM_Date],
 dbo.func_Convert_Time( [Type of augmentation ARM_Time]) [Type of augmentation ARM_Time],
 dbo.func_Convert_Date( [Type of augmentation Date Syntocinon Started]) [Type of augmentation Date Syntocinon Started],
 dbo.func_Convert_Time( [Type of augmentation Time Syntocinon Started]) [Type of augmentation Time Syntocinon Started],
 [Type of augmentation Other Method],
 [Indication for augmentation],
 [Indication for augmentation Text],
 [ARM done for induction],
 dbo.func_Convert_Date( [Date of induction ARM]) [Date of induction ARM],
 dbo.func_Convert_Time( [Time of induction ARM]) [Time of induction ARM],
 [Induction done by staff Type],
 [Induction ARM done by],
 [Induction ARM done by Grade],
 [Syntocinon used for induction],
 dbo.func_Convert_Date( [Syntocinon used for induction Date Started]) [Syntocinon used for induction Date Started],
 dbo.func_Convert_Time( [Syntocinon used for induction Time Started]) [Syntocinon used for induction Time Started],
 [Method of Induction],
 [Indication for induction],
 [Indication for induction Text],
 [Induction drug 1],
 dbo.func_Convert_Date( [Induction drug 1 Date given]) [Induction drug 1 Date given],
 dbo.func_Convert_Time( [Induction drug 1 Time given]) [Induction drug 1 Time given],
 [Induction drug 2],
 dbo.func_Convert_Date( [Induction drug 2 Date given]) [Induction drug 2 Date given],
 dbo.func_Convert_Time( [Induction drug 2 Time given]) [Induction drug 2 Time given],
 [Induction drug 3],
 dbo.func_Convert_Date( [Induction drug 3 Date given]) [Induction drug 3 Date given],
 dbo.func_Convert_Time( [Induction drug 3 Time given]) [Induction drug 3 Time given],
 dbo.func_Convert_Integer( [Number of induction drug doses],'') [Number of induction drug doses],
 [Failed induction],
 [Failed induction Text],

 [Any drugs given in labour],
 [Any drugs given in labour Drug 1],
 [Any drugs given in labour Drug 2],
 [Any drugs given in labour Drug 3],
 [Any drugs given in labour Drug 4],
 [Any drugs given in labour Text],

 [Group B strep prophylaxis],
 dbo.func_Convert_Date( [Group B strep prophylaxis Date started]) [Group B strep prophylaxis Date started],
 dbo.func_Convert_Time( [Group B strep prophylaxis Time started]) [Group B strep prophylaxis Time started],
 [Group B strep prophylaxis Reason],
 [Group B strep prophylaxis Text],

 [Self-administered inhalational],
 [TENS],
 [Acupuncture],
 [Water immersion],
 [Water immersion Complications],
 [Water immersion Comments],
 [Narcotics],
 [Narcotics Drug 1],
 [Narcotics Drug 2],
 [Narcotics Drug 3],
 [Narcotics Text],
 [Epidural],
 [Epidural Method],
 [Epidural inserted by],
 [Epidural inserted by Grade],
 [Other regional block in labour],

 [Was an epidural requested],
 [Reason for epidural request],
 [Reason why epidural not given],
 [Mothers attitude to epidural],
 [Medic. indication for epidural] [Medical indication for epidural],
 [Medic. indication for epidural Text] [Medical indication for epidural Text],
 [Staff Type inserting epidural],
 [Epidural resited at any time],
 [Success of Epidural],

 [Antenatal steroids given],
 dbo.func_Convert_Date( [Antenatal steroids given Date started]) [Antenatal steroids given Date started],
 dbo.func_Convert_Integer( [Antenatal steroids given Number of doses],'') [Antenatal steroids given Number of doses],
 [Antibiotics in pregnancy Text],
 [Antibiotics in pregnancy],
 dbo.func_Convert_Date( [Antibiotics in pregnancy Date started]) [Antibiotics in pregnancy Date started],
 [Antibiotics in pregnancy Reason],

 [Drugs given prior to section],
 [Drugs given prior to section Drug 1],
 [Drugs given prior to section Drug 2],
 [Drugs given prior to section Drug 3],
 [Drugs given prior to section Drug 4],
 [Drugs given prior to section Text],

 [Analgesia During Labour],
 [Epidural_for_labour],
 [Prostin_induction],
 [ARM_done_for_induction],
 [Surgical_induction],
 [Spontaneous_rupture],
 [Syntocinon_infusion]
 FROM 
 tbl_Delivery








