﻿
CREATE VIEW view_DiabetesRecord 
AS 
SELECT
Patient_Pointer,
[Date of diabetes registration],
[Diabetes registration status],
[Diabetes registration status Reason],
dbo.func_Convert_Numeric51( [Blood glucose at diagnosis 1], 'mmol/l') [Blood glucose at diagnosis 1 (mmol/l)],
[Blood glucose at diagnosis 1 Method],
dbo.func_Convert_Numeric51( [Blood glucose at diagnosis 2], 'mmol/l') [Blood glucose at diagnosis 2 (mmol/l)],
[Blood glucose at diagnosis 2 Method],
[Diabetes diagnostic criteria],
[Diabetes diagnostic criteria Text],
dbo.func_Convert_Numeric51( [OGTT 0 hour glucose at diagnosis], 'mmol/l') [OGTT 0 hour glucose at diagnosis (mmol/l)],
[OGTT 0 hour glucose at diagnosis Method],
dbo.func_Convert_Numeric51( [OGTT 2 hour glucose at diagnosis], 'mmol/l') [OGTT 2 hour glucose at diagnosis (mmol/l)],
[OGTT 2 hour glucose at diagnosis Method],
[Urine ketones at diagnosis]
FROM
tbl_DiabetesRecord 





