﻿
CREATE VIEW view_DownsNeuralTubeScreening_Fetus
AS
SELECT
[Patient_Pointer],
[Pregnancy_ID],
[Pregnancy number],
[Fetus_Number],
[Fetal identifier],
[Nuchal translucency test result],
dbo.func_Convert_Integer([Nuchal translucency test result Risk is 1:],'') [Nuchal translucency test result Risk is 1:],
[Combined test result],
dbo.func_Convert_Integer([Combined test result Risk is 1:],'') [Combined test result Risk is 1:],
dbo.func_Convert_Date([Date pre-natal test result released]) [Date pre-natal test result released],
[Pre-natal test result],
[Pre-natal test result Other],
dbo.func_Convert_Date([Date of informing client of test result]) [Date of informing client of test result],
[Method of informing client of test result],
[Type of specialist informing client of test result],
[Type of specialist informing client of test result Screening coordinator]	[Type of specialist informing client Screening coordinator],
[Type of specialist informing client of test result Clinic midwife]		[Type of specialist informing client Clinic midwife],
[Type of specialist informing client of test result Community midwife]		[Type of specialist informing client Community midwife],
[Type of specialist informing client of test result GP]				[Type of specialist informing client GP],
[Type of specialist informing client of test result Consultant]			[Type of specialist informing client Consultant],
[Type of specialist informing client of test result Sonographer]		[Type of specialist informing client Sonographer],
[Type of specialist informing client of test result Other specialist]		[Type of specialist informing client Other specialist]
FROM
tbl_DownsNeuralTubeScreening_Fetus






