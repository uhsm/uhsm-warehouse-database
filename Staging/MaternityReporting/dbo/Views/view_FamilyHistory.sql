﻿




CREATE VIEW dbo.view_FamilyHistory
AS
SELECT     
	Patient_Pointer, 

	[Family history of diabetes], 
	[Family history of diabetes Type_1], 
	[Family history of diabetes Type_2], 
	[Family history of diabetes Type_unknown], 
	[Family history of diabetes Details], 

	[Family history of hypertension], 
	[Family history of hypertension Details],
	[FH of preg ind hypertension], 
	[FH of preg ind hypertension Details], 

	[Family history of sickle cell], 
	[Family history of sickle cell Details], 

	[Family history of thalassaemia], 
	[Family history of thalassaemia Details], 

	[FH Thrombo-embolic disease], 
	[FH Thrombo-embolic disease Details], 

	[Family history hyperlipidaemia], 
	[Family history hyperlipidaemia Text], 

	[Fam hist premature CVS disease], 
	[Fam hist premature CVS disease Text], 
	[Fam hist premature CVS death], 
	[Fam hist premature CVS death Text], 

	[Fam hist of premature CVA], 
	[Fam hist of premature CVA Text], 

	[Fam hist of premature CHD], 
	[Fam hist of premature CHD Text], 
                      
	[Fam hist of autoimmune disease], 
	[Fam hist of autoimmune disease Thyroid disease], 
	[Fam hist of autoimmune disease Pernicious anaemia], 
	[Fam hist of autoimmune disease Addisons disease], 
	[Fam hist of autoimmune disease Coelic disease], 
	[Fam hist of autoimmune disease Vitilgo], 
	[Fam hist of autoimmune disease Text], 

	[Other family history], 
	[Other family history Problem 1], 
	[Other family history Problem 2], 
	[Other family history Text]
FROM         
	dbo.tbl_FamilyHistory









