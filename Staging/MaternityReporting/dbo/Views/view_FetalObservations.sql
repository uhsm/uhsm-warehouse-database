﻿





CREATE VIEW dbo.view_FetalObservations
AS 
SELECT 
 [Patient_Pointer],
 [Pregnancy_ID],
 [Pregnancy number],
 [Review_Number],
 [Fetus_Number],
 [Fetal identifier],
 [Abdomen at scan],
 [Abdomen at scan Text],
 [Abdomen circumference at scan],
 [Amniocentesis],
 [Amniocentesis Text],
 dbo.func_Convert_Numeric51 ( [Amniotic fluid index], 'cm') [Amniotic fluid index],
 [Amniotic fluid volume],
 [Biparietal diameter at scan],
 [Cardiotocograph],
 [Cardiotocograph Result],
 [Cardiotocograph Text],
 [Chorionic villus biopsy],
 [Chorionic villus biopsy Text],
 [Comments on review scan],
 [Comments on review scan Comments],
 [Crown-rump length at scan],
 [Extremities at scan],
 [Extremities at scan Text],
 [Face at scan],
 [Face at scan Text],
 [Femur length at scan],
 [Fetal blood sample],
 dbo.func_Convert_Date ( [Fetal blood sample Date sample taken1]) [Fetal blood sample Date sample taken1],
 dbo.func_Convert_Time ( [Fetal blood sample Time sample taken1]) [Fetal blood sample Time sample taken1],
 dbo.func_Convert_Numeric53( [Fetal blood sample pH1],'') [Fetal blood sample pH 1],
 dbo.func_Convert_Numeric52( [Fetal blood sample Base excess1], 'mmol/l') [Fetal blood sample Base excess 1 (mmol/l)],
 dbo.func_Convert_Date ( [Fetal blood sample Date sample taken2]) [Fetal blood sample Date sample taken2],
 dbo.func_Convert_Time ( [Fetal blood sample Time sample taken2]) [Fetal blood sample Time sample taken2],
 dbo.func_Convert_Numeric53( [Fetal blood sample pH2],'') [Fetal blood sample pH 2],
 dbo.func_Convert_Numeric52( [Fetal blood sample Base excess2], 'mmol/l') [Fetal blood sample Base excess 2 (mmol/l)],
 dbo.func_Convert_Date ( [Fetal blood sample Date sample taken3]) [Fetal blood sample Date sample taken3],
 dbo.func_Convert_Time ( [Fetal blood sample Time sample taken3]) [Fetal blood sample Time sample taken3],
 dbo.func_Convert_Numeric53( [Fetal blood sample pH3],'') [Fetal blood sample pH 3],
 dbo.func_Convert_Numeric52( [Fetal blood sample Base excess3], 'mmol/l') [Fetal blood sample Base excess 3 (mmol/l)],
 dbo.func_Convert_Date ( [Fetal blood sample Date sample taken4]) [Fetal blood sample Date sample taken4],
 dbo.func_Convert_Time ( [Fetal blood sample Time sample taken4]) [Fetal blood sample Time sample taken 4],
 dbo.func_Convert_Numeric53( [Fetal blood sample pH4],'') [Fetal blood sample pH 4],
 dbo.func_Convert_Numeric52( [Fetal blood sample Base excess4], 'mmol/l') [Fetal blood sample Base excess 4 (mmol/l)],
 dbo.func_Convert_Date ( [Fetal blood sample Date sample taken5]) [Fetal blood sample Date sample taken5],
 dbo.func_Convert_Time ( [Fetal blood sample Time sample taken5]) [Fetal blood sample Time sample taken5],
 dbo.func_Convert_Numeric53( [Fetal blood sample pH5],'') [Fetal blood sample pH 5],
 dbo.func_Convert_Numeric52( [Fetal blood sample Base excess5], 'mmol/l') [Fetal blood sample Base excess 5 (mmol/l)],
 dbo.func_Convert_Date ( [Fetal blood sample Date sample taken6]) [Fetal blood sample Date sample taken6],
 dbo.func_Convert_Time ( [Fetal blood sample Time sample taken6]) [Fetal blood sample Time sample taken6],
 dbo.func_Convert_Numeric53( [Fetal blood sample pH6],'') [Fetal blood sample pH 6],
 dbo.func_Convert_Numeric52( [Fetal blood sample Base excess6], 'mmol/l') [Fetal blood sample Base excess 6 (mmol/l)],
 dbo.func_Convert_Integer( [Fetal gestation at scan], 'weeks') [Fetal gestation at scan (weeks)],
 dbo.func_Convert_Integer( [Fetal gestation at scan Days], 'days') [Fetal gestation at scan (days)],
 [Fetal heartbeat],
 dbo.func_Convert_Integer ( [Fetal heartbeat Rate], 'bpm') [Fetal heartbeat Rate (bpm)],
 [Fetal heartbeat Method],
 [Fetal lie],
 [Fetal measurements],
 [Fetal position],
 [Fetal presentation],
 [Fetal umbilical artery doppler],
 [Fetal umbilical artery doppler Result],
 [Fetal umbilical artery doppler A:B Ratio],
 [Fetal umbilical artery doppler Text],
 [Genitalia at scan],
 [Genitalia at scan Text],
 [Head at scan],
 [Head at scan Text],
 [Head circumference at scan],
 [Heart at scan],
 [Heart at scan Text],
 [Indication for amniocentesis],
 [Indication for amniocentesis Text],
 [Indication for CVB],
 [Indication for CVB Text],
 [Indication for Nuchal scan],
 [Indication for Nuchal scan Text],
 [Kidneys at scan],
 [Kidneys at scan Text],
 [Liquor volume],
 [Liquor volume at scan],
 [Lungs at scan],
 [Lungs at scan Text],
 [Neck/skin  at scan],
 [Neck/skin  at scan Text],
 [Nuchal scan test],
 [Nuchal scan test Text],
 [Nuchal translucency risk fctr],
 [Placental appearance at scan],
 [Placental appearance at scan Text],
 [Placental biopsy],
 [Placental biopsy Text],
 [Placental relation to Lwr seg.] [Placental relation to Lwr seg],
 [Placental site],
 [Placental site Text],
 [Presentation at scan],
 [Relationship to brim],
 [Spine at scan],
 [Spine at scan Text]
 FROM 
 tbl_FetalObservations
















