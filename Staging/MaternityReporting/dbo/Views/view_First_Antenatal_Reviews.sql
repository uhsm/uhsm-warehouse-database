﻿




CREATE VIEW dbo.view_First_Antenatal_Reviews
AS
SELECT view_ClinicalReviews.* FROM view_ClinicalReviews 
JOIN
(
SELECT Patient_Pointer [First_Review_Patient_Pointer],[Pregnancy_ID] [First_Review_Preg_ID],min(Review_Number) [First_Review_Number]
FROM view_ClinicalReviews
JOIN
(
SELECT Patient_Pointer [Earliest_Review_Patient_Pointer],[Pregnancy_ID] [Earliest_Review_Preg_ID],min([Date of review]) [Earliest_Review_Date] from view_ClinicalReviews 
where [Type of review] = 'Antenatal Review'
GROUP BY Patient_Pointer,[Pregnancy_ID]
) EarliestReviews
ON
EarliestReviews.[Earliest_Review_Patient_Pointer] = view_ClinicalReviews.Patient_Pointer AND
EarliestReviews.[Earliest_Review_Preg_ID] = view_ClinicalReviews.[Pregnancy_ID] AND
EarliestReviews.[Earliest_Review_Date] = view_ClinicalReviews.[Date of review] AND view_ClinicalReviews.[Type of review] = 'Antenatal Review'
GROUP BY
view_ClinicalReviews.Patient_Pointer,view_ClinicalReviews.[Pregnancy_ID]
) FirstReviews
ON
FirstReviews.[First_Review_Patient_Pointer] = view_ClinicalReviews.Patient_Pointer AND
FirstReviews.[First_Review_Preg_ID] = view_ClinicalReviews.[Pregnancy_ID] AND
FirstReviews.[First_Review_Number] = view_ClinicalReviews.[Review_Number] AND view_ClinicalReviews.[Type of review] = 'Antenatal Review'











