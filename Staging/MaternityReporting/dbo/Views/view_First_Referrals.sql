﻿




CREATE VIEW dbo.view_First_Referrals
AS
SELECT view_Referrals.* FROM view_Referrals
JOIN
(
SELECT Patient_Pointer [First_Referral_Patient_Pointer],[Pregnancy_ID] [First_Referral_Preg_ID],min(Referral_Number) [First_Referral_Number]
FROM view_Referrals
JOIN
(
SELECT Patient_Pointer [Earliest_Referral_Patient_Pointer],[Pregnancy_ID] [Earliest_Referral_Preg_ID],min([Date of Referral]) [Earliest_Referral_Date] 
from view_Referrals 
GROUP BY Patient_Pointer,[Pregnancy_ID]
) EarliestReferrals
ON
EarliestReferrals.[Earliest_Referral_Patient_Pointer] = view_Referrals.Patient_Pointer AND
EarliestReferrals.[Earliest_Referral_Preg_ID] = view_Referrals.[Pregnancy_ID] AND
EarliestReferrals.[Earliest_Referral_Date] = view_Referrals.[Date of Referral]
GROUP BY
view_Referrals.Patient_Pointer,view_Referrals.[Pregnancy_ID]
) FirstReferrals
ON
FirstReferrals.[First_Referral_Patient_Pointer] = view_Referrals.Patient_Pointer AND
FirstReferrals.[First_Referral_Preg_ID] = view_Referrals.[Pregnancy_ID] AND
FirstReferrals.[First_Referral_Number] = view_Referrals.[Referral_Number]









