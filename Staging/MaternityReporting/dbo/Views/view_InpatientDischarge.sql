﻿



CREATE VIEW dbo.view_InpatientDischarge
AS
SELECT view_InpatientSpells.* FROM view_InpatientSpells
JOIN
(
SELECT Patient_Pointer [Last_Spell_Patient_Pointer],[Current Pregnancy] [Last_Spell_Preg_Number],max(Spell_Number) [Last_Spell_Number]
FROM view_InpatientSpells
JOIN
	(
	SELECT Patient_Pointer [Latest_Spell_Patient_Pointer],[Current Pregnancy] [Latest_Spell_Preg_Number],max([Date of discharge]) [Latest_Discharge_Date] 
	FROM view_InpatientSpells
	WHERE view_InpatientSpells.[Current Pregnancy] is not null
	GROUP BY Patient_Pointer,[Current Pregnancy]
	) LatestDischarge
	ON
	LatestDischarge.[Latest_Spell_Patient_Pointer] = view_InpatientSpells.Patient_Pointer AND
	LatestDischarge.[Latest_Spell_Preg_Number] = view_InpatientSpells.[Current Pregnancy] AND
	LatestDischarge.[Latest_Discharge_Date] = view_InpatientSpells.[Date of discharge]
	WHERE view_InpatientSpells.[Current Pregnancy] is not null
	GROUP BY
	view_InpatientSpells.Patient_Pointer, view_InpatientSpells.[Current Pregnancy]
) LastDischarge
ON
LastDischarge.[Last_Spell_Patient_Pointer] = view_InpatientSpells.Patient_Pointer AND
LastDischarge.[Last_Spell_Preg_Number] = view_InpatientSpells.[Current Pregnancy] AND
LastDischarge.[Last_Spell_Number] = view_InpatientSpells.[Spell_Number]
WHERE view_InpatientSpells.[Current Pregnancy] is not null







