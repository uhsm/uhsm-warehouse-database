﻿






CREATE VIEW dbo.view_InpatientSpells
AS 
SELECT 
 [Patient_Pointer],
 [Current Pregnancy],
 [Spell_Number],
 [Current location],
 [Start_of_episode],
 [End_of_episode],

 [Next adm/trans episode number],

 [Primary diagnosis],
 [Primary diagnosis Reason],
 [Secondary diagnosis 1],
 [Secondary diagnosis 1 Reason],
 [Secondary diagnosis 2],
 [Secondary diagnosis 2 Reason],
 [Secondary diagnosis 3],
 [Secondary diagnosis 3 Reason],

 dbo.func_Convert_Date( [Planned discharge date]) [Planned discharge date],
 dbo.func_Convert_Time( [Planned discharge time]) [Planned discharge time],
 [Planned discharge destination],
 [Planned discharge destination Hospital name],

 dbo.func_Convert_Date( [Date of discharge]) [Date of discharge],
 dbo.func_Convert_Time( [Time of discharge]) [Time of discharge],
 [Length of stay],

 [Discharge source],
 [Discharge method],
 [Discharge destination],
 [Discharge destination Hospital name],

 [COPPISH Discharge Type],
 [COPPISH Discharge/Transfer To],

 [Alternative discharge address],
 [Alternative discharge address Address_1],
 [Alternative discharge address Address_2],
 [Alternative discharge address Address_3],
 [Alternative discharge address Address_4],
 [Alternative discharge address Postcode],

 [Discharge spell number],
 [Discharge event number],
 [Discharge deletion allowed],
 [Discharge protocol flag],

 [Discharge comments],
 [Discharge comments Text],

 [Follow up arrangements],
 dbo.func_Convert_Date( [Follow up arrangements When]) [Follow up arrangements When],
 [Follow up arrangements Text],

-- Clinical Admission Codes
[Diagnosis code 1],
[Diagnosis code 1 Code],
[Diagnosis code 2],
[Diagnosis code 2 Code],
[Diagnosis code 3],
[Diagnosis code 3 Code],
[Diagnosis code 4],
[Diagnosis code 4 Code],
[Diagnosis code 5],
[Diagnosis code 5 Code],
[Diagnosis code 6],
[Diagnosis code 6 Code],
[Diagnosis code 7],
[Diagnosis code 7 Code],
[Diagnosis code 8],
[Diagnosis code 8 Code],
[Diagnosis code 9],
[Diagnosis code 9 Code],
[Diagnosis code 10],
[Diagnosis code 10 Code],
[Procedure code 1],
[Procedure code 1 OPCS_Code],
[Procedure code 2],
[Procedure code 2 OPCS_Code],
[Procedure code 3],
[Procedure code 3 OPCS_Code],
[Procedure code 4],
[Procedure code 4 OPCS_Code],
[Procedure code 5],
[Procedure code 5 OPCS_Code],
[Procedure code 6],
[Procedure code 6 OPCS_Code],
[Procedure code 7],
[Procedure code 7 OPCS_Code],
[Procedure code 8],
[Procedure code 8 OPCS_Code],
[Procedure code 9],
[Procedure code 9 OPCS_Code],
[Procedure code 10],
[Procedure code 10 OPCS_Code]

 FROM 
 tbl_InpatientSpells










