﻿

CREATE VIEW view_MaternityTails AS
SELECT 
	-- Mother
	view_Patients.[Unit Number] [Local Patient Identifier (Mother)],
	view_Patients.[NHS Number] [NHS Number (Mother)],
	view_Patients.Postcode [Postcode (Mother)],
	view_Patients.BirthDate [Birth Date Mother)],
	view_ObstetricSummary.[Number of Past Pregnancies] [Number of Past Pregnancies],
	-- Baby
	view_Baby_Details.[Babys Hospital number] [Local Patient Identifier (Baby)],
	view_Baby_Details.[Babys NHS number] [NHS Number (Baby)],
	view_Baby_Birth.[Date of Birth] [Birth Date (Baby)],
	ISNULL(tbl_RefNatSex.SexOfBaby_NAT_CODE,0) [Gender (Baby)],
	ISNULL(tbl_RefNatEthnic.Ethnicity_NAT_CODE,0) [Ethnic Category (Baby)],
	-- Details
	dbo.func_GetValueCode(view_Delivery.[Number born this delivery]) [Number of Babies],
	isnull(view_Pregnancy_Assessment.[Date of Initial Assessment],view_Referral_Booking_for_Delivery.[Date of Referral]) [First Antenatal Assessment Date],
	CASE WHEN isnull(view_Pregnancy_Assessment.[Date of Initial Assessment],view_Referral_Booking_for_Delivery.[Date of Referral]) IS NOT NULL THEN 1 ELSE 9 END [First Antenatal Assessment Date Status],
	ISNULL(view_PregnancyCarePlan_Booking.[Maternity care GP Code], view_Patients.GP_Code) [Code of GMP Responsible for Antenatal Care],
	ISNULL(view_PregnancyCarePlan_Booking.[Maternity care GP Practice Code], view_Patients.GP_Practice) [Registered GMP - Antenatal Care],
	dbo.func_GetValueCode(view_Baby_Birth.[Korner Reason for Change]) [Delivery Place Change Reason],
	dbo.func_GetValueCode(view_Baby_Birth.[Korner Intended Place of Birth]) [Delivery Place Type (Intended)],
	dbo.func_GetValueCode(view_Baby_Birth.[Korner Analgesia/Anaesthesia]) [Anaesthetic Given During Labour or Delivery],
	dbo.func_GetValueCode(view_Baby_Birth.[Korner Post Delivery Analgesia]) [Anaesthetic Given Post Labour or Delivery],
	dbo.func_GetValueCode(view_Baby_Birth.[Korner Method of Labour Onset]) [Labour or Delivery Onset Method],
 	view_Delivery.[Date of delivery] [Delivery Date],
	view_Baby_Details.[Birth Order],
	dbo.func_GetValueCode(view_Baby_Birth.[Korner Method of delivery]) [Delivery Method],
	view_Baby_Birth.[Estimate of gestation (weeks)] [Gestation Length (Assessment)],
	dbo.func_GetValueCode(view_Baby_Resuscitation.[National Resuscitation Code]) [Resuscitation Method],
	ISNULL(tbl_RefNatDelStat.DelvPersonStatus_NAT_CODE,8) [Status of Person Conducting Delivery],
	dbo.func_GetValueCode(view_Baby_Birth.[Korner Actual Place of Birth]) [Delivery Place Type (Actual)],
	CASE 
		WHEN view_Baby_Birth.[Outcome of birth]  = 'Livebirth' THEN '1'
		WHEN view_Baby_Birth.[Outcome of birth]  = 'Stillbirth' AND view_Baby_Birth.[Outcome of birth Death_of_Fetus] in ('Probably before Labour','Confirmed before Labour') THEN '2'
		WHEN view_Baby_Birth.[Outcome of birth]  = 'Stillbirth' AND view_Baby_Birth.[Outcome of birth Death_of_Fetus] = 'During Labour' THEN '3'
		WHEN view_Baby_Birth.[Outcome of birth]  = 'Stillbirth' AND (view_Baby_Birth.[Outcome of birth Death_of_Fetus] = 'Time Unknown' OR view_Baby_Birth.[Outcome of birth Death_of_Fetus] IS NULL) THEN '4'
	END  [Live or Still Birth],
	view_Baby_Details.[Birth weight (grams)] [Birth Weight]
,view_Patients.Patient_Pointer
,view_Pregnancy_Assessment.[Pregnancy number]
FROM view_Baby_Birth
JOIN view_Baby_Details ON 
	view_Baby_Details.[Patient_Pointer] = view_Baby_Birth.[Patient_Pointer] and
	view_Baby_Details.[Pregnancy number] = view_Baby_Birth.[Pregnancy number] and
	view_Baby_Details.[Delivery_Number] = view_Baby_Birth.[Delivery_Number] and
	view_Baby_Details.[Baby_Number] = view_Baby_Birth.[Baby_Number] 
JOIN view_Baby_Resuscitation ON 
	view_Baby_Resuscitation.[Patient_Pointer] = view_Baby_Birth.[Patient_Pointer] and
	view_Baby_Resuscitation.[Pregnancy number] = view_Baby_Birth.[Pregnancy number] and
	view_Baby_Resuscitation.[Delivery_Number] = view_Baby_Birth.[Delivery_Number] and
	view_Baby_Resuscitation.[Baby_Number] = view_Baby_Birth.[Baby_Number] 
JOIN view_Baby_Birth_StaffPresent ON 
	view_Baby_Birth_StaffPresent.[Patient_Pointer] = view_Baby_Birth.[Patient_Pointer] and
	view_Baby_Birth_StaffPresent.[Pregnancy number] = view_Baby_Birth.[Pregnancy number] and
	view_Baby_Birth_StaffPresent.[Delivery_Number] = view_Baby_Birth.[Delivery_Number] and
	view_Baby_Birth_StaffPresent.[Baby_Number] = view_Baby_Birth.[Baby_Number] 
JOIN view_Delivery ON 
	view_Delivery.[Patient_Pointer] = view_Baby_Birth.[Patient_Pointer] and
	view_Delivery.[Pregnancy number] = view_Baby_Birth.[Pregnancy number] and
	view_Delivery.[Delivery_Number] = view_Baby_Birth.[Delivery_Number]
JOIN view_Pregnancy_Assessment ON 
	view_Pregnancy_Assessment.[Patient_Pointer] = view_Delivery.[Patient_Pointer] and
	view_Pregnancy_Assessment.[Pregnancy number] = view_Delivery.[Pregnancy number]
LEFT JOIN view_PregnancyCarePlan_Booking ON 
	view_PregnancyCarePlan_Booking.[Patient_Pointer] = view_Pregnancy_Assessment.[Patient_Pointer] and
	view_PregnancyCarePlan_Booking.[Pregnancy number] = view_Pregnancy_Assessment.[Pregnancy number]
LEFT JOIN view_Referral_Booking_for_Delivery ON 
	view_Referral_Booking_for_Delivery.[Patient_Pointer] = view_Pregnancy_Assessment.[Patient_Pointer] and
	view_Referral_Booking_for_Delivery.[Pregnancy number] = view_Pregnancy_Assessment.[Pregnancy number]
JOIN view_Patients ON 
	view_Patients.[Patient_Pointer] = view_Pregnancy_Assessment.[Patient_Pointer]
LEFT JOIN view_ObstetricSummary ON
	view_ObstetricSummary.[Patient_Pointer] = view_Patients.[Patient_Pointer]
LEFT JOIN tbl_RefNatSex ON 
	tbl_RefNatSex.SexOfBaby = view_Baby_Details.[Sex of baby]
LEFT JOIN tbl_RefNatEthnic ON 
	tbl_RefNatEthnic.EthnicCategory = dbo.func_GetValueCode(view_Baby_Details.[Babys ethnic category]) 
LEFT JOIN tbl_RefNatDelStat ON 
	tbl_RefNatDelStat.DelvPersonStatus = view_Baby_Birth_StaffPresent.[Type of person delivering]
WHERE 
view_Delivery.[Date of delivery] IS NOT NULL














