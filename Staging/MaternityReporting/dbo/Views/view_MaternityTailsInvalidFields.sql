﻿CREATE View view_MaternityTailsInvalidFields as
SELECT
*,
CASE WHEN [Anaesthetic Given During Labour or Delivery] = 9 THEN
	1
ELSE
	0
END AS 	[Invalid - Anaesthetic Given During Labour or Delivery],
CASE WHEN [Birth Order] = 9 THEN
	1
ELSE
	0
END AS 	[Invalid Birth Order],
CASE WHEN [Delivery Method] = 9 THEN
	1
ELSE
	0
END AS 	[Invalid Delivery Method],
CASE WHEN ([Birth Weight] <= 0 OR [Birth Weight] =9999) THEN
	1
ELSE
	0
END AS [Invalid Birth Weight],
CASE WHEN [Delivery Place Change Reason] = 9 THEN
	1
ELSE
	0
END AS 	[Invalid Delivery Place Change Reason],
CASE WHEN [Delivery Place Type (Actual)] = 9 THEN
	1
ELSE
	0
END AS 	[Invalid Delivery Place Type (Actual)],
CASE WHEN [Delivery Place Type (Intended)] = 9 THEN
	1
ELSE
	0
END AS 	[Invalid Delivery Place Type (Intended)],
CASE WHEN [Ethnic Category (Baby)] = '99' THEN
	1
ELSE
	0
END AS 	[Invalid Ethnic Category (Baby)],
CASE WHEN [First Antenatal Assessment Date] IS NULL THEN
	1
ELSE
	0
END AS 	[Invalid First Antenatal Assessment Date],
CASE WHEN [Gender (Baby)] = 9 THEN
	1
ELSE
	0
END AS 	[Invalid Gender (Baby)],
CASE WHEN [Gestation Length (Assessment)] = 9 THEN
	1
ELSE
	0
END AS 	[Invalid Gestation Length (Assessment)],
CASE WHEN [Labour or Delivery Onset Method] = 9 THEN
	1
ELSE
	0
END AS 	[Invalid Labour or Delivery Onset Method],
CASE WHEN ([Number of Past Pregnancies] = 99 or [Number of Past Pregnancies] is null) THEN
	1
ELSE
	0
END AS 	[Invalid Number of Past Pregnancies],
CASE WHEN [Resuscitation Method] = 9 THEN
	1
ELSE
	0
END AS 	[Invalid Resuscitation Method],
CASE WHEN [Status of Person Conducting Delivery] = 9 THEN
	1
ELSE
	0
END AS 	[Invalid Status of Person Conducting Delivery]

from dbo.view_MaternityTails
WHERE 
	(
	[Birth Date (Baby)] >= '20090401'
	and [Birth Date (Baby)] <= '20091031'
	)

