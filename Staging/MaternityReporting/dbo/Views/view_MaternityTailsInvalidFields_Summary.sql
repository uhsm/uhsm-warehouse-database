﻿CREATE VIEW view_MaternityTailsInvalidFields_Summary AS
SELECT
COUNT(*) AS TotalRecords,
Sum([Invalid - Anaesthetic Given During Labour or Delivery]) AS [Invalid - Anaesthetic Given During Labour or Delivery],
Sum([Invalid Birth Order]) AS [Invalid Birth Order],
Sum([Invalid Delivery Method]) AS [Invalid Delivery Method],
Sum([Invalid Birth Weight]) AS [Invalid Birth Weight],
Sum([Invalid Delivery Place Change Reason]) AS [Invalid Delivery Place Change Reason],
Sum([Invalid Delivery Place Type (Actual)]) AS [Invalid Delivery Place Type (Actual)],
Sum([Invalid Delivery Place Type (Intended)]) AS [Invalid Delivery Place Type (Intended)],
Sum([Invalid Ethnic Category (Baby)]) AS [Invalid Ethnic Category (Baby)],
Sum([Invalid First Antenatal Assessment Date]) AS [Invalid First Antenatal Assessment Date],
Sum([Invalid Gender (Baby)]) AS [Invalid Gender (Baby)],
Sum([Invalid Gestation Length (Assessment)]) AS [Invalid Gestation Length (Assessment)],
Sum([Invalid Labour or Delivery Onset Method]) AS [Invalid Labour or Delivery Onset Method],
Sum([Invalid Number of Past Pregnancies]) AS [Invalid Number of Past Pregnancies],
Sum([Invalid Resuscitation Method]) AS [Invalid Resuscitation Method],
Sum([Invalid Status of Person Conducting Delivery]) AS [Invalid Status of Person Conducting Delivery]
FROM view_MaternityTailsInvalidFields

