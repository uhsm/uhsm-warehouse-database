﻿





CREATE  VIEW dbo.view_MedicalHistory
AS
SELECT     
	Patient_Pointer, 

-- gynae
	[History of recurrent UTIs], 
	[History of recurrent UTIs Text], 

	[History of Genital Infections], 
	[History of Genital Infections Disease 1], 
	[History of Genital Infections Disease 2], 
	[History of Genital Infections Disease 3], 
	[History of Genital Infections Disease 4], 
	[History of Genital Infections Details], 

	[Previous gynae operations], 
	[Previous gynae operations Cone Biopsy], 
	[Previous gynae operations D&C], 
	[Previous gynae operations Hysteroscopy], 
	[Previous gynae operations Myomectomy], 
	[Previous gynae operations Pelvic Floor Repair], 
	[Previous gynae operations Reconstruction Uterus], 
	[Previous gynae operations Other], 

	[General gynaecological prob.] [General gynaecological problems], 
	[General gynaecological prob. Text] [General gynaecological problems Text],

	[Ever had smear test], 
	dbo.func_Convert_Date( [Ever had smear test Last Test]) [Ever had smear test Last Test], 
	[Ever had smear test Results], 
	[Ever had smear test Text], 

	[Any Previous Colposcopy], 
	[Any Previous Colposcopy Text], 

	[Group B Strep Carrier], 
	[Group B Strep Carrier Details], 

	[Previous SB/NND/Misc], 
	[Previous SB/NND/Misc Text], 

-- infertility
	[Previous infertility treatment], 
	[Previous infertility treatment Clomiphene], 
	[Previous infertility treatment Tamoxifen], 
	[Previous infertility treatment Pergonyl], 
	[Previous infertility treatment HCG], 
	[Previous infertility treatment Insemination], 
	[Previous infertility treatment IVF], 
	[Previous infertility treatment GIFT], 
	[Previous infertility treatment Other], 
	[Previous infertility problems], 
	[Previous infertility problems Text], 

	[Previous tubal surgery], 
	[Previous tubal surgery Sterilisation Reversal], 
	[Previous tubal surgery P.I.D Repair] [Previous tubal surgery PID Repair], 
	[Previous tubal surgery Ectopic], 
	[Previous tubal surgery Text], 

-- infectious
	[Established AIDS sufferer], 

	[HIV status], 
	dbo.func_Convert_Date( [HIV status Most recent test]) [HIV status Most recent test], 

	[Any previous HIV test], 
	dbo.func_Convert_Date( [Any previous HIV test Date]) [Any previous HIV test Date], 

	[Hepatitis or jaundice], 
	[Hepatitis or jaundice Infective Hepatitis(A)], 
	[Hepatitis or jaundice Serum Hepatitis(B)], 
	[Hepatitis or jaundice Serum Hepatitis(C)], 
	[Hepatitis or jaundice Cholestasis], 
	[Hepatitis or jaundice Other], 

-- blood
	[History of diabetes], 
	[History of diabetes Kind], 
	[History of diabetes Text], 

	[History of thrombosis], 
	[History of thrombosis DVT], 
	[History of thrombosis Pulmonary Embolism], 
	[History of thrombosis Other], 

	[Any haemoglobinopathy], 
	[Any haemoglobinopathy Condition], 
	[Any haemoglobinopathy Condition 2], 
	[Any haemoglobinopathy Text], 

	[Previous blood transfusion], 
	[Previous blood transfusion Text], 

	[Hist of essential hypertension], 
	[Hist of essential hypertension Text], 

	[Hist of preg ind hypertension], 
	[Hist of preg ind hypertension Text], 

	[History of anaemia], 
	[History of anaemia Iron Deficiency], 
	[History of anaemia Folate Deficiancy], 
	[History of anaemia Pernicious Anaemia], 
	[History of anaemia Text], 

	[History of clotting disorders], 
	[History of clotting disorders Haemophilia carrier], 
	[History of clotting disorders Von Willebrands], 
	[History of clotting disorders Other], 
	[History of clotting disorders Details], 

-- psycho/mental/head
	[Any psychological problems], 
	[Any psychological problems Text], 

	[Any mental health problems], 
	[Any mental health problems Text], 

	[History of epilepsy], 
	[History of epilepsy Text], 

	[History of migraine], 
	[History of migraine Text], 

-- allergies
	[History of allergies], 
	[History of allergies Allergy 1], 
	[History of allergies Allergy 2], 
	[History of allergies Allergy 3], 
	[History of allergies Text], 

	[History of asthma], 
	[History of asthma Text], 

	[Potential anaesthetic hazards], 
	[Potential anaesthetic hazards Text], 

	[Previous anaesthetic problem],
	[Previous anaesthetic problem Text], 

-- heart/chest/lungs
	[Previous heart surgery], 
	[Previous heart surgery Text], 
	[History of heart problems], 
	[History of heart problems Level of Symptoms], 
	[History of heart problems Details], 
	[History of chest problems], 
	[History of chest problems Text], 
	[Details of tuberculosis], 
	[History of tuberculosis], 
	[History of tuberculosis Active],

-- kidney/thyroid
	[Kidney disease or surgery], 
	[Kidney disease or surgery Kidney Transplant], 
	[Kidney disease or surgery Ureteric Reimplantation], 
	[Kidney disease or surgery Other], 

	[History of thyroid disease], 
	[History of thyroid disease Current Status], 
	[History of thyroid disease Text], 

-- other
	[Previous pelvic/leg fracture], 
	[Previous pelvic/leg fracture Text], 

	[History of back problems], 
	[History of back problems Text], 
                      
	[Previous non gynae operations], 
	[Previous non gynae operations Appendicectomy], 
	[Previous non gynae operations Laparotomy], 
	[Previous non gynae operations Other], 
                      
	[Other past surgical history], 
	[Other past surgical history Text],

	[Other medical problems], 
	[Other medical problems Text],
	[Consent For Blood Transfusion]
                                            
FROM         
	dbo.tbl_MedicalHistory












