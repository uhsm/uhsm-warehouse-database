﻿




CREATE VIEW dbo.view_ObstetricProblemSummary
AS 
SELECT 
 [Patient_Pointer],

-- Zpp01
 [Spontaneous Miscarriage],
 [Spontaneous Miscarriage Confidential],
 [Spontaneous Miscarriage Management],
 [Spontaneous Miscarriage History],

-- Zpp02
 [Termination],
 [Termination Confidential],
 [Termination Management],
 [Termination History],

-- Zpp03
 [Induced abortion],
 [Induced abortion Confidential],
 [Induced abortion Management],
 [Induced abortion History],

-- Zpp04
 [Ectopic pregnancy],
 [Ectopic pregnancy Confidential],
 [Ectopic pregnancy Management],
 [Ectopic pregnancy History],

-- Zpp05
 [Hydatiform mole],
 [Hydatiform mole Confidential],
 [Hydatiform mole Management],
 [Hydatiform mole History],

-- Zpp06
 [Induction],
 [Induction Confidential],
 [Induction Management],
 [Induction Text],

-- Zpp08
 [Labour under 2 hours],
 [Labour under 2 hours Confidential],
 [Labour under 2 hours Management],
 [Labour under 2 hours History],

-- Zpp09
 [Labour over 24 hrs],
 [Labour over 24 hrs Confidential],
 [Labour over 24 hrs Management],
 [Labour over 24 hrs History],

-- Zpp10
 [Maternal pyrexia],
 [Maternal pyrexia Confidential],
 [Maternal pyrexia Management],
 [Maternal pyrexia History],

-- Zpp11
 [Pregnancy Induced Hypertension],
 [Pregnancy Induced Hypertension Confidential],
 [Pregnancy Induced Hypertension Management],
 [Pregnancy Induced Hypertension History],

-- Zpp12
 [Ruptured uterus],
 [Ruptured uterus Confidential],
 [Ruptured uterus Management],
 [Ruptured uterus History],

-- Zpp13
 [Intrapartum problems],
 [Intrapartum problems Confidential],
 [Intrapartum problems Management],
 [Intrapartum problems History],

-- Zpp14
 [Retained placenta],
 [Retained placenta Confidential],
 [Retained placenta Management],
 [Retained placenta History],

-- Zpp15
 [Early PPH],
 [Early PPH Confidential],
 [Early PPH Management],
 [Early PPH History],

-- Zpp16
 [Perineal or Episiotomy problem],
 [Perineal or Episiotomy problem Confidential],
 [Perineal or Episiotomy problem Management],
 [Perineal or Episiotomy problem History],

-- Zpp17
 [Postpartum complications],
 [Postpartum complications Confidential],
 [Postpartum complications Management],
 [Postpartum complications History],

-- Zpp50
 [Previous stillbirth],
 [Previous stillbirth Confidential],
 [Previous stillbirth Management],
 [Previous stillbirth History],

-- Zpp51
 [Previous forceps delivery],
 [Previous forceps delivery Confidential],
 [Previous forceps delivery Management],
 [Previous forceps delivery History],

-- Zpp52
 [Previous ventouse delivery],
 [Previous ventouse delivery Confidential],
 [Previous ventouse delivery Management],
 [Previous ventouse delivery History],

-- Zpp53
 [Previous vaginal breech],
 [Previous vaginal breech Confidential],
 [Previous vaginal breech Management],
 [Previous vaginal breech History],

-- Zpp54
 [Previous elective section],
 [Previous elective section Confidential],
 [Previous elective section Management],
 [Previous elective section History],

-- Zpp55
 [Previous emergency section],
 [Previous emergency section Confidential],
 [Previous emergency section Management],
 [Previous emergency section History],

-- Zpp71
 [Prev. delivery under 37 weeks] [Previous delivery under 37 weeks],
 [Prev. delivery under 37 weeks Confidential] [Previous delivery under 37 weeks Confidential],
 [Prev. delivery under 37 weeks Management] [Previous delivery under 37 weeks Management],
 [Prev. delivery under 37 weeks History] [Previous delivery under 37 weeks History],

-- Zpp72
 [Previous deliv. over 42 weeks] [Previous delivery over 42 weeks],
 [Previous deliv. over 42 weeks Confidential] [Previous delivery over 42 weeks Confidential],
 [Previous deliv. over 42 weeks Management] [Previous delivery over 42 weeks Management],
 [Previous deliv. over 42 weeks History] [Previous delivery over 42 weeks History],

-- Zpp56
 [Previous delivery < 2500g],
 [Previous delivery < 2500g Confidential],
 [Previous delivery < 2500g Management],
 [Previous delivery < 2500g History],

-- Zpp57
 [Previous delivery > 4500g],
 [Previous delivery > 4500g Confidential],
 [Previous delivery > 4500g Management],
 [Previous delivery > 4500g History],

-- Zpp58
 [Previous Downs Child],
 [Previous Downs Child Confidential],
 [Previous Downs Child Management],
 [Previous Downs Child History],

-- Zpp59
 [Previous child with NTD],
 [Previous child with NTD Confidential],
 [Previous child with NTD Management],
 [Previous child with NTD History],

-- Zpp60
 [Prev. child with cleft palate] [Previous child with cleft palate],
 [Prev. child with cleft palate Confidential] [Previous child with cleft palate Confidential],
 [Prev. child with cleft palate Management] [Previous child with cleft palate Management],
 [Prev. child with cleft palate History] [Previous child with cleft palate History],

-- Zpp61
 [Previous child with cleft lip],
 [Previous child with cleft lip Confidential],
 [Previous child with cleft lip Management],
 [Previous child with cleft lip History],

-- Zpp62
 [Previous child absent digits],
 [Previous child absent digits Confidential],
 [Previous child absent digits Management],
 [Previous child absent digits History],

-- Zpp63
 [Prev. child with cong abnorm] [Previous child with cong abnorm],
 [Prev. child with cong abnorm Confidential] [Previous child with cong abnorm Confidential],
 [Prev. child with cong abnorm Management] [Previous child with cong abnorm Management],
 [Prev. child with cong abnorm History] [Previous child with cong abnorm History],

-- Zpp64
 [Prev. neonatal medical problem] [Previous neonatal medical problem],
 [Prev. neonatal medical problem Confidential] [Previous neonatal medical problem Confidential],
 [Prev. neonatal medical problem Management] [Previous neonatal medical problem Management],
 [Prev. neonatal medical problem History] [Previous neonatal medical problem History],

-- Zpp65
 [Previous cot death],
 [Previous cot death Confidential],
 [Previous cot death Management],
 [Previous cot death History],

-- Zpp66
 [Previous child death],
 [Previous child death Confidential],
 [Previous child death Management],
 [Previous child death History],

-- Zpp67
 [Previous child adopted],
 [Previous child adopted Confidential],
 [Previous child adopted Management],
 [Previous child adopted History],

-- Zpp68
 [Previous child fostered],
 [Previous child fostered Confidential],
 [Previous child fostered Management],
 [Previous child fostered History],

-- Zpp69
 [Previous child not with parent],
 [Previous child not with parent Confidential],
 [Previous child not with parent Management],
 [Previous child not with parent History],

-- Zpp70
 [Prev. child with other parent] [Previous child with other parent],
 [Prev. child with other parent Confidential] [Previous child with other parent Confidential],
 [Prev. child with other parent Management] [Previous child with other parent Management],
 [Prev. child with other parent History] [Previous child with other parent History]

 FROM 
	tbl_ObstetricProblemSummary











