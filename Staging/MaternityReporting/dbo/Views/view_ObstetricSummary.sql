﻿




CREATE VIEW dbo.view_ObstetricSummary
AS 
SELECT 
 [Patient_Pointer],

 [Female contraceptive status],
 [Currently pregnant],
 [Currently pregnant EDD],

 dbo.func_Convert_Integer( [No. of Past Pregnancies],'') [Number of Past Pregnancies],
 dbo.func_Convert_Integer( [Number of previous pregnancies],'') [Number of previous pregnancies],

 dbo.func_Convert_Integer( [Number_of_previous_caesars],'') [Number_of_previous_caesars],

 dbo.func_Convert_Integer( [Gravida], '') [Gravida],
 [Parity at Booking],
 [Parity after Delivery],

 dbo.func_Convert_Integer( [No. of registerable TOP],'') [Number of registerable TOP],

 dbo.func_Convert_Integer( [No. of Registerable Pregnancies],'') [Number of Registerable Pregnancies],
 dbo.func_Convert_Integer( [No. of Non-registerable Pregs],'') [Number of Non-registerable Pregs],

 dbo.func_Convert_Integer( [Number_of_previous_births],'') [Number_of_previous_births],
 dbo.func_Convert_Integer( [No. of non-registerable births],'') [Number of non-registerable births],

 dbo.func_Convert_Integer( [No. of neonatal deaths],'') [Number of neonatal deaths],
 dbo.func_Convert_Integer( [No. of pre-term livebirths],'') [Number of pre-term livebirths],

 [Pregs ending before 12 wks],
 [Pregs ending before 12 wks Miscarriages],
 [Pregs ending before 12 wks Induced abortions],
 [Pregs ending before 12 wks Terminations],
 [Pregs ending before 12 wks Ectopics],
 [Pregs ending before 12 wks Hydatidiform moles],

 [Pregs ending between 12 - 24 w],
 [Pregs ending between 12 - 24 w Miscarriages],
 [Pregs ending between 12 - 24 w Induced abortions],
 [Pregs ending between 12 - 24 w Terminations],
 [Pregs ending between 12 - 24 w Ectopics],
 [Pregs ending between 12 - 24 w Hysterotomys],

 dbo.func_Convert_Integer( [No. of registerable livebirths],'') [Number of registerable livebirths],
 dbo.func_Convert_Integer( [No. of registerable stillbirths],'') [Number of registerable stillbirths],

 dbo.func_Convert_Integer( [Number of live births],'') [Number of live births],
 dbo.func_Convert_Integer( [Number of still births],'') [Number of still births],

 dbo.func_Convert_Integer( [No. of children who have died],'') [Number of children who have died],
 dbo.func_Convert_Integer( [No. of children living now],'') [Number of children living now]

 FROM 
 tbl_ObstetricSummary













