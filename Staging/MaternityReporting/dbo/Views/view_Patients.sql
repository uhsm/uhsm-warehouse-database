﻿




CREATE VIEW dbo.view_Patients
AS
SELECT     
	Patient_Pointer, 
	Surname, 
	Forename, 
	Forename2, 
	Title, 
	Sex, 
	BirthDate, 
	CASE WHEN BirthDate IS NULL THEN NULL ELSE dbo.func_Calculate_Age(BirthDate,getdate()) END [Age Now],
	Address1, 
	Address2, 
	Address3, 
	Address4, 
	Postcode, 
	Phone, 
	Number1 [Unit Number],
	cast(replace(Number2,' ','') as varchar(14)) [NHS Number], 
	Number3 [Other Number], 
	Status, 
	MaritalStatus, 
	dbo.func_GetLink_GPCode(GP_Pointer) AS GP_Code,
	dbo.func_GetLink_GPName(GP_Pointer) AS GP_Name,
	dbo.func_GetLink_GPPracticeCode(GP_Pointer) AS GP_Practice,
	dbo.func_GetLink_GPPostcode(GP_Pointer) AS GP_Postcode,
	Phone2, 
	dbo.func_GetLink_ConsultantCode(Consultant_Pointer) AS Consultant_Code,
	dbo.func_GetLink_ConsultantName(Consultant_Pointer) AS Consultant_Name,
	NOK_Name, 
	NOK_Relationship, 
	NOK_Contact, 
	SHA_Code, 
             dbo.func_Convert_Date(DateOfDeath) AS DateOfDeath
FROM         
	dbo.tbl_Patients








