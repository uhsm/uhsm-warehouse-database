﻿




CREATE VIEW dbo.view_PersonalInformation
AS
SELECT     
	Patient_Pointer, 

	[Alternative patient number 1], 
	[Alternative patient number 2], 
	[Alternative patient number 3], 
	[Internal number], 
	[BTS ref number], 

	[Patients preferred forename], 
	[Surname at marriage], 
	[Other Surnames used previously], 
	[Previous surname 1], 
	[Previous surname 1 Forename], 
	[Previous surname 1 Number], 
	[Previous surname 2], 
	[Previous surname 2 Forename], 
	[Previous surname 2 Number], 
	[Previous surname 3], 
	[Previous surname 3 Forename], 
	[Previous surname 3 Number], 
	Aliases, 

	[Age at menarche], 
	[Age at menopause], 

	[Mothers Place of Birth] [Mother's Place of Birth], 
	[Father`s Place of Birth] [Father's Place of Birth], 

	dbo.func_Convert_Numeric52 ( [Usual weight in Kg] , 'kg' ) AS [Usual weight (kg)],
	dbo.func_Convert_Integer ( [Adult height in centimetres] , 'cm' ) AS [Adult height (cm)],
	dbo.func_Convert_Numeric52 ( [Usual Adult Body Mass Index] , 'kg/m' ) AS [Usual Adult Body Mass Index (kg/m)],
	Diet, 
	[Diet Text], 

	[Language ability], 
	[Language ability Native_tongue], 
	[Language ability Interpreter needed],
                       
	Mobility, 
	[Mobility Text], 
	[Any other disability], 
	[Any other disability Text], 
	[Sight problems], 
	[Sight problems Text], 
	[Hearing ability], 
	[Hearing ability Able to lip read], 
	[Hearing ability Text], 
	[Speech problems], 
	[Speech problems Text], 
	[Any communication problem], 

	[Personal hist of thalassaemia], 
	[Personal hist of thalassaemia Text], 
	[Personal hist of sickle cell], 
	[Personal hist of sickle cell Text]

FROM         
	dbo.tbl_PersonalInformation






