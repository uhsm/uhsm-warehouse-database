﻿





CREATE VIEW dbo.view_Pregnancy
AS 
SELECT 
 [Patient_Pointer],
 Pregnancy_ID,
 [Pregnancy number],

-- Due Date (15)
 dbo.func_Convert_Date( [Date of LMP]) [Date of LMP],
 [Confidence in date of LMP],
 dbo.func_Convert_Integer( [Number of days in cycle],'') [Number of days in cycle],
 [Pattern of period before preg],
 [Was LMP a withdrawal bleed],
 dbo.func_Convert_Date( [Best EDD from LMP data] ) [Best EDD from LMP data],

 dbo.func_Convert_Date( [Best EDD from dating scan] ) [Best EDD from dating scan],
 dbo.func_Convert_Date( [Final due date]) [Final due date],
 [Final due date Basis],

 [Scan observations],
 dbo.func_Convert_Date( [Scan observations Date] ) [Scan observations Date],
 dbo.func_Convert_Integer ( [Scan observations Weeks Gestation], 'weeks') [Scan observations Gestation (weeks)],
 dbo.func_Convert_Integer ( [Scan observations Days Gestation], 'days') [Scan observations Gestation (days)],
 dbo.func_Convert_Integer ( [Scan observations Number of babies], '') [Scan observations Number of babies],
 [Scan observations Text],

-- Record Summary (65)
 [Reason for end of pregnancy],
 [Reason for end of pregnancy Text],
 dbo.func_Convert_Integer ( [Gestation at admin. closure] ,'weeks') [Gestation at admin closure (weeks)],
 dbo.func_Convert_Date( [Date pregnancy record closed] ) [Date pregnancy record closed],
 [Reason for closing preg record],
 [Reason for closing preg record Text],
 dbo.func_Convert_Date( [Specific closure date known] ) [Specific closure date known],
 dbo.func_Convert_Date( [Date birth plan completed] ) [Date birth plan completed],
 [Mobility],
 [Mobility Text],
 [Fetal monitoring],
 [Pharmacological Pain Relief],
 [Pharmacological Pain Relief Epidural],
 [Pharmacological Pain Relief Self_admin._inhalation] [Pharmacological Pain Relief Self_admin_inhalation],
 [Pharmacological Pain Relief Local_Infiltration],
 [Pharmacological Pain Relief Non-opioid e.g. paracetamol] [Pharmacological Pain Relief Non-opioid (eg paracetamol)],
 [Pharmacological Pain Relief Opioid analgesia eg. pethidine] [Pharmacological Pain Relief Opioid analgesia (eg pethidine)],
 [Pharmacological Pain Relief Text],
 [NonPharmacological Pain Relief],
 [NonPharmacological Pain Relief Acupuncture],
 [NonPharmacological Pain Relief Aromatherapy],
 [NonPharmacological Pain Relief Hot Packs],
 [NonPharmacological Pain Relief Relaxation Methods],
 [NonPharmacological Pain Relief TENS],
 [NonPharmacological Pain Relief Water],
 [NonPharmacological Pain Relief Text],
 [ARM],
 [ARM Text],
 [Maternal position for delivery],
 [Maternal position for delivery Text],
 [Episiotomy],
 [Episiotomy Text],
 [Oxytocics],
 [Oxytocics Drug 1],
 [Oxytocics Drug 2],
 [Oxytocics Drug 3],
 [Oxytocics Text],
 [Vitamin K (Konakion)],
 [Vitamin K (Konakion) Route & Dose],
 [Feeding intention],
 [Skin to skin contact],
 [Involvement of students],
 [Involvement of students Text],
 [Companions at delivery],
 [Companions at delivery Text],
 [Any other special requests],
 [Any other special requests Text],
 [Discussed with midwife],
 [Name of midwife],
 [Name of midwife Grade],
 [Previous Parity],
 dbo.func_Convert_Integer ( [Gestation at adverse outcome], 'weeks') [Gestation at adverse outcome (weeks)],
 [Pregnancy status],
 dbo.func_Convert_Date( [Pregnancy status Date] ) [Pregnancy status Date],
 dbo.func_Convert_Time( [Pregnancy status Time] ) [Pregnancy status Time],
 [Pregnancy status Reason],
 [Pregnancy status User],
 [Created in],
 convert ( varchar(4) , dbo.func_Convert_Date( [Year pregnancy ended]) , 120) [Year pregnancy ended],
 dbo.func_Convert_Date( [Date pregnancy ended] ) [Date pregnancy ended],
 [Type of pregnancy record],
 [Outcome of this pregnancy],
 [Place pregnancy ended],
 [Place pregnancy ended Text],
 [Confidential pregnancy],

-- Pregnancy Problem Summary (16)
 dbo.func_Convert_Integer ( [Lowest Hb this pregnancy], 'g/l') [Lowest Hb this pregnancy g/l],
 [Proteinuria this pregnancy],
 [Glycosuria this pregnancy],
 [Raised HbA1c this pregnancy],
 [Systolic BP above 140],
 [Diastolic BP above 90],
 [MAP > 105 mmhg],
 [Significant oedema this preg],
 [Absent fetal heartbeat in preg],
 [Reduced Fetal movements in prg],
 [Unsatisfactory CTG this preg.]  [Unsatisfactory CTG this preg],
 [Per vaginum loss this preg.] [Per vaginum loss this preg],
 [Blood urine during pregnancy],
 [Urine ketones during pregnancy],
 [Low liquor volume],
 [High liquor volume],

-- Abortion (5)
 [Method of abortion],
 [Reason for induced abortion],
 [Reason for induced abortion Details],
 [Reason for termination],
 [Reason for termination Details],

-- Finally....
 [Referred to social services],

-- Family History (17)
 [FH of hip problems],
 [FH of hip problems Details],
 [FH of twins],
 [FH of twins Details],
 [FH of congenital anomalies],
 [FH of congenital anomalies Downs],
 [FH of congenital anomalies Neural Tube Defect],
 [FH of congenital anomalies Other],
 [FH of inherited disease],
 [FH of inherited disease Cystic Fibrosis],
 [FH of inherited disease Muscular Dystrophy],
 [FH of inherited disease Other],
 [FH of hearing problems],
 [FH of hearing problems Details],
 [FH of learning difficulty],
 [FH of learning difficulty Sex],
 [FH of learning difficulty Details],

-- Obstetric Summary (27)
 [Gravida],
 dbo.func_Convert_Integer( [No. of Past Pregnancies],'') [Number of Past Pregnancies],
 dbo.func_Convert_Integer( [No. of Registerable Pregnancies],'') [Number of Registerable Pregnancies],
 dbo.func_Convert_Integer( [No. of Non-registerable Pregs],'') [Number of Non-registerable Pregs],
 dbo.func_Convert_Integer( [No. of Registerable Livebirths],'') [Number of Registerable Livebirths],
 dbo.func_Convert_Integer( [No. of Registerable Stillbirths],'') [Number of Registerable Stillbirths],
 dbo.func_Convert_Integer( [No. of Non-Registerable Births],'') [Number of Non-Registerable Births],
 dbo.func_Convert_Integer( [No. of pre-term livebirths],'') [Number of pre-term livebirths],
 dbo.func_Convert_Integer( [No. of children living now],'') [Number of children living now],
 [Parity],
 dbo.func_Convert_Integer( [No. of children who have died],'') [Number of children who have died],
 dbo.func_Convert_Integer( [No. of Neonatal Deaths],'') [Number of Neonatal Deaths],
 [Pregs ending before 12 wks],
 [Pregs ending before 12 wks Miscarriages],
 [Pregs ending before 12 wks Induced Abortions],
 [Pregs ending before 12 wks Terminations],
 [Pregs ending before 12 wks Ectopics],
 [Pregs ending before 12 wks Hydatidiform Moles],
 [Pregs ending between 12 - 24 w],
 [Pregs ending between 12 - 24 w Miscarriages],
 [Pregs ending between 12 - 24 w Induced Abortions],
 [Pregs ending between 12 - 24 w Terminations],
 [Pregs ending between 12 - 24 w Ectopics],
 [Pregs ending between 12 - 24 w Hysterotomys],
 [Previous Spontaneous Abortions],
 [Previous Therapeutic Abortions],
 [Previous Caesarean Sections]

 FROM 
 tbl_Pregnancy










