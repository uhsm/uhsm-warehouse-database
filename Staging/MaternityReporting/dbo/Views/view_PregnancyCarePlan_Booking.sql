﻿



CREATE VIEW dbo.view_PregnancyCarePlan_Booking
AS
SELECT view_PregnancyCarePlans.* FROM view_PregnancyCarePlans
JOIN
(
SELECT Patient_Pointer [First_Plan_Patient_Pointer],[Pregnancy_ID] [First_Plan_Preg_ID],min(CarePlan_Number) [First_Plan_Number]
FROM view_PregnancyCarePlans
JOIN
	(
	SELECT Patient_Pointer [First_Plan_Patient_Pointer],[Pregnancy_ID] [First_Plan_Preg_ID],max([Date of care plan review]) [First_Plan_Date] 
	FROM view_PregnancyCarePlans
	WHERE [Timing of plan review] in ('Booking','Delivery & Booking')
	GROUP BY Patient_Pointer,[Pregnancy_ID]
	) FirstPlan
	ON
	FirstPlan.[First_Plan_Patient_Pointer] = view_PregnancyCarePlans.Patient_Pointer AND
	FirstPlan.[First_Plan_Preg_ID] = view_PregnancyCarePlans.[Pregnancy_ID] AND
	FirstPlan.[First_Plan_Date] = view_PregnancyCarePlans.[Date of care plan review]
	WHERE view_PregnancyCarePlans.[Timing of plan review] in ('Booking','Delivery & Booking')
	GROUP BY
	view_PregnancyCarePlans.Patient_Pointer, view_PregnancyCarePlans.[Pregnancy_ID]
) First_Plan
ON
First_Plan.[First_Plan_Patient_Pointer] = view_PregnancyCarePlans.Patient_Pointer AND
First_Plan.[First_Plan_Preg_ID] = view_PregnancyCarePlans.[Pregnancy_ID] AND
First_Plan.[First_Plan_Number] = view_PregnancyCarePlans.[CarePlan_Number]
WHERE view_PregnancyCarePlans.[Timing of plan review] in ('Booking','Delivery & Booking')









