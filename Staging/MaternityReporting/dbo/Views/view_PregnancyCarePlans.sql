﻿





CREATE VIEW dbo.view_PregnancyCarePlans
AS 
SELECT 
 [Patient_Pointer],
 [Pregnancy_ID],
 [Pregnancy number],
 [CarePlan_Number],

 dbo.func_Convert_Date( [Date of care plan review]) [Date of care plan review],
 dbo.func_Convert_Time( [Time of care plan review]) [Time of care plan review],
 [Timing of plan review],
 dbo.func_Convert_Integer( [Gestation at plan review], '') [Gestation at plan review (weeks)],

 [Intended resp. for birth] [Intended responsibility for birth],
 [Intended resp. for birth Text] [Intended responsibility for birth Text],

 dbo.func_GetLink_GPName([Maternity care GP]) [Maternity care GP Name],
 dbo.func_GetLink_GPCode([Maternity care GP]) [Maternity care GP Code],
 dbo.func_GetLink_GPPracticeCode([Maternity care GP]) [Maternity care GP Practice Code],
 [Lead Professional],
 dbo.func_GetLink_ConsultantName([Consultant Obstetrician]) [Consultant Obstetrician Name],
 dbo.func_GetLink_ConsultantCode([Consultant Obstetrician]) [Consultant Obstetrician Code],
 dbo.func_GetLink_MidwifeName([Named Midwife]) [Named Midwife Name],
 dbo.func_GetLink_MidwifeCode([Named Midwife]) [Named Midwife Code],
 [Midwifery Team],

 [Care scheme],
 [Care scheme Text],

 [Care plan recommendation],
 [Care plan recommendation Text],
 [Care plan revised at review],

 [Consent for Caesarean section accepted],
 [Consent for Caesarean section accepted Reason],
 [Consent for Caesarean section offered],
 [Consent for Caesarean section offered Reason],

 [Current risk category],
 [Current risk category Reason],

 [Evident based information provided],
 [Evident based information provided Reason],

 [Indication for plan review],
 [Indication for plan review Indication 1],
 [Indication for plan review Indication 2],
 [Indication for plan review Indication 3],
 [Indication for plan review Indication 4],
 [Indication for plan review Text],

 [Management of deliv. admission] [Management of delivery admission],

 [Plan reviewed at request of],
 [Plan reviewed with],

 dbo.func_Convert_Date( [Planned date of procedure]) [Planned date of procedure],
 [Planned management of delivery],

 [Reason for transfer in Labour],
 [Reason for transfer in Labour Reason],

 [Reason for unplanned home conf],
 [Reason for unplanned home conf Reason],

 [Transfer out due to lack of beds]

 FROM 
 tbl_PregnancyCarePlans

















