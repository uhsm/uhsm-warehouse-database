﻿




CREATE VIEW [dbo].[view_Referral_Booking]
AS
SELECT view_Referrals.*,dbo.func_Calculate_Age(view_Patients.BirthDate,view_Referrals.[Date of Referral]) as [Mothers Age at Booking] FROM view_Referrals
JOIN
(
SELECT Patient_Pointer [Last_Referral_Patient_Pointer],[Pregnancy_ID] [Last_Referral_Preg_ID],min(Referral_Number) [Last_Referral_Number]
FROM view_Referrals
JOIN
	(
	SELECT Patient_Pointer [Latest_Referral_Patient_Pointer],[Pregnancy_ID] [Latest_Referral_Preg_ID],min([Date of Referral]) [Latest_Referral_Date] 
	FROM view_Referrals 
	--WHERE [Reason for referral] = 'Booking for Delivery'
	GROUP BY Patient_Pointer,[Pregnancy_ID]
	) EarliestReferrals
	ON
	EarliestReferrals.[Latest_Referral_Patient_Pointer] = view_Referrals.Patient_Pointer AND
	EarliestReferrals.[Latest_Referral_Preg_ID] = view_Referrals.[Pregnancy_ID] AND
	EarliestReferrals.[Latest_Referral_Date] = view_Referrals.[Date of Referral]
	--WHERE [Reason for referral] = 'Booking for Delivery'
	GROUP BY
	view_Referrals.Patient_Pointer,view_Referrals.[Pregnancy_ID]
) FirstReferrals
ON
FirstReferrals.[Last_Referral_Patient_Pointer] = view_Referrals.Patient_Pointer AND
FirstReferrals.[Last_Referral_Preg_ID] = view_Referrals.[Pregnancy_ID] AND
FirstReferrals.[Last_Referral_Number] = view_Referrals.[Referral_Number]
JOIN 
view_Patients on view_Referrals.Patient_Pointer = view_Patients.Patient_Pointer
--WHERE view_Referrals.[Reason for referral] = 'Booking for Delivery'






