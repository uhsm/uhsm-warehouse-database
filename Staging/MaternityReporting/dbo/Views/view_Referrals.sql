﻿





CREATE VIEW dbo.view_Referrals
AS 
SELECT 
 [Patient_Pointer],
 [Pregnancy_ID],
 [Pregnancy number],
 [Referral_Number],

 dbo.func_Convert_Date( [Date of Referral]) [Date of Referral],

 [Referral episode number],
 [Referral event number],
 [Referral status],
 dbo.func_Convert_Date( [Referral status Date of Discharge]) [Referral status Date of Discharge],
 [Referral status Reason],

 dbo.func_Convert_Date( [Initial estimate of due date]) [Initial estimate of due date],

 [Source of referral],
 [Name of referrer],

 [Reason for referral],
 [PAS Reason for referral],
 [Clinical Reason for referral],
 [Clinical Reason for referral Text],

 [General Practitioner],
 [Is this the Registered GP],

 [Lead Profession],
 [Consultant referred to],
 [Consultant action at referral],
 [Consultant action at referral Text],

 [Named Midwife],
 [Midwifery Team],

 [Planned care scheme at referl.] [Planned care scheme at referral],
 [Planned care scheme at referl. Text] [Planned care scheme at referral Text],
 [Planned management],

 [Intended Delivery Place],
 [Intended Delivery Place Text],
 [Intended Responsible Hospital],

 [Hospital transferred from Hospital],
 [Hospital transferred from],
 [Reason for transfer Text],
 [Reason for transfer],
 [Reason for transfer of care],

 [SMR Institution code],
 [UK or Non UK Hospital],

 [Days Postnatal],

 [Deletion allowed]

 FROM 
 tbl_Referrals








