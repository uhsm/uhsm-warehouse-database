﻿





CREATE VIEW dbo.view_ReviewExaminations
AS 
SELECT 
 [Patient_Pointer],
 [Pregnancy_ID],
 [Pregnancy number],
 [Review_Number],

 dbo.func_Convert_Numeric51( [Weight at antenatal review in Kg], 'kg') [Weight at antenatal review in Kg],
 dbo.func_Convert_Numeric52( [Antenatal Body Mass Index], 'kg/m') [Antenatal Body Mass Index (kg/m)],
 dbo.func_Convert_Integer( [Height in centimetres], 'cm') [Height in centimetres],
 [Height centile],
 dbo.func_Convert_Numeric51( [Weight in kilograms], 'Kg') [Weight in kilograms],
 [Weight centile],
 dbo.func_Convert_Numeric52( [Body Mass Index], 'kg/m') [Body Mass Index (kg/m)],
 [Body Mass Index centile],
 [Waist circumference],
 [Blood pressure reading],
 dbo.func_Convert_Integer( [Blood pressure reading Systolic], 'mmHg') [Blood pressure reading Systolic (mmHg)],
 dbo.func_Convert_Integer( [Blood pressure reading Diastolic], 'mmHg') [Blood pressure reading Diastolic (mmHg)],
 [Blood pressure reading Mean],
 [Blood pressure reading 1],
 dbo.func_Convert_Integer( [Blood pressure reading 1 Systolic], 'mmHg') [Blood pressure reading 1 Systolic (mmHg)],
 dbo.func_Convert_Integer( [Blood pressure reading 1 Diastolic], 'mmHg') [Blood pressure reading 1 Diastolic (mmHg)],
 [Blood pressure reading 2],
 dbo.func_Convert_Integer( [Blood pressure reading 2 Systolic], 'mmHg') [Blood pressure reading 2 Systolic (mmHg)],
 dbo.func_Convert_Integer( [Blood pressure reading 2 Diastolic], 'mmHg') [Blood pressure reading 2 Diastolic (mmHg)],
 [Blood pressure reading 3],
 dbo.func_Convert_Integer( [Blood pressure reading 3 Systolic], 'mmHg') [Blood pressure reading 3 Systolic (mmHg)],
 dbo.func_Convert_Integer( [Blood pressure reading 3 Diastolic], 'mmHg') [Blood pressure reading 3 Diastolic (mmHg)],
 [Blood pressure reading 4],
 dbo.func_Convert_Integer( [Blood pressure reading 4 Systolic], 'mmHg') [Blood pressure reading 4 Systolic (mmHg)],
 dbo.func_Convert_Integer( [Blood pressure reading 4 Diastolic], 'mmHg') [Blood pressure reading 4 Diastolic (mmHg)],
 [Blood pressure reading 5],
 dbo.func_Convert_Integer( [Blood pressure reading 5 Systolic], 'mmHg') [Blood pressure reading 5 Systolic (mmHg)],
 dbo.func_Convert_Integer( [Blood pressure reading 5 Diastolic], 'mmHg') [Blood pressure reading 5 Diastolic (mmHg)],

 [Abdominal Lie],
 [Presentation prior to delivery],
 [Presentation prior to delivery Text],
 [Abdominal station],
 [Consent obtained for Vaginal Examination],
 dbo.func_Convert_Date( [Date of Vaginal Examination]) [Date of Vaginal Examination],
 dbo.func_Convert_Time( [Time of Vaginal Examination]) [Time of Vaginal Examination],
 [Cervical consistency],
 dbo.func_Convert_Integer( [Cervical dilation], 'cm') [Cervical dilation (cm)],
 dbo.func_Convert_Integer( [Cervical length], 'cm') [Cervical length (cm)],
 [Cervical position],
 [Cervical os],
 [Liquor],
 [Liquor state],
 [Liquor volume],
 dbo.func_Convert_Integer( [Syntocinon], 'mu/min') [Syntocinon (mu/min)],
 dbo.func_Convert_Integer( [Bishops Score] ,'') [Bishops Score],
 [Other findings on Vaginal Examination],
 [Other findings on Vaginal Examination Text],
 [Cephalic position],
 [Effacement],
 [Moulding],
 [Nuchal translucency risk fctr],
 [Pelvic examination],
 [Pelvic examination Text],

 dbo.func_Convert_Integer( [Number of babies expected],'') [Number of babies expected],
 dbo.func_Convert_Integer( [Number of babies expected Basis],'') [Number of babies expected Basis],
 [Fetal movements felt],
 [Contractions],
 dbo.func_Convert_Date( [Contractions Date]) [Contractions Date],
 dbo.func_Convert_Time( [Contractions Time]) [Contractions Time],
 [Contractions Strength],
 dbo.func_Convert_Integer( [Contractions Duration], 'secs') [Contractions Duration (secs)],
 dbo.func_Convert_Integer( [Contractions Frequency(per 10 mins)],'') [Contractions Frequency (per 10 mins)],
 [Contractions Text],
 dbo.func_Convert_Integer( [Station of presenting part], 'cm') [Station of presenting part (cm)],
 [Oedema],
 [Fetal position],
 dbo.func_Convert_Integer( [Fundal Height], 'cms') [Fundal Height (cms)],
 [Any other complaints],
 [Any other complaints Comments],

 dbo.func_Convert_Integer( [Temperature], 'oC') [Temperature (oC)],
 dbo.func_Convert_Integer( [Pulse or heart rate], 'bpm') [Pulse or heart rate (bpm)],

 [Breasts],
 [Breast problems],
 [Breast problems Text],
 [Perineum],
 [Perineum Text],
 [Legs],
 [Legs Text],
 [Problems passing urine],
 [Problems passing urine Text],
 [Bowels opened 1],
 [Bowels opened 1 Text],
 [Uterine state post-partum],
 [Uterine state post-partum Text],
 [Per vaginum loss],
 [Per vaginum loss Text],
 [Urine passed 1],
 [Method of feeding],
 [Feeding pattern],
 [Feeding pattern Text],
 [Feeding problems],
 [Feeding problems Text],
 [Colour of stool passed],
 [Colour of stool passed Text],
 [Eyes 1],
 [Eyes 1 Text],
 [Cord],
 [Cord Text],
 [Babys weight in grams],
 [Postnatal admission to NNU],
 [Postnatal admission to NNU Text],
 [Is the baby still on NNU],
 [Presence/Severity of Jaundice],
 [Presence/Severity of Jaundice Maximum SBR],
 [Feeding ability 1],
 [Feeding ability 1 Appetite],
 [Feeding ability 1 Time taken to feed],
 [Feeding ability 1 Text],

 [Bowels opened 2],
 [Bowels opened 2 Stool],
 [Bowels opened Consistency],
 [Feeding frequency],
 [Feeding frequency Amount],
 [Feeding frequency Text],
 [Baby visited by],
 [Baby visited by Text],
 [Parents performing care],
 [Parents performing care Text],
 [Progress report for parents],
 [Progress report for parents Text],

 [Sutures removed],
 [Sutures removed Comment],
 [Wound],
 [Wound Comment],

 [Catheterised during delivery],
 [Catheterised during delivery Technique],
 [Catheterised during delivery Catheter in situ],
 [Catheterised during delivery Details],

 [Pubertal stage],

 [Arcus],
 [Xanthomata],
 [Retinal Changes],
 [Retinal Changes Papilloedema],
 [Retinal Changes Cotton Wool Spots],
 [Retinal Changes Haemorrhages],
 [Retinal Changes Retinal Vein Occlusion],

 [FEV],
 [FVC],
 [PEFR],
 [Predicted FEV],
 [Predicted FVC],
 [Predicted PEFR],
 [10 year CHD event risk (BCS)],
 [Population normal risk],
 [10 yr mod. CHD event risk(BCS)] [10 yr mod CHD event risk(BCS)],

 [Bladder],
 [Uterus],
 [Left adnexae],
 [Left adnexae Text],
 [Left ovary],
 [Left ovary Text],
 [Right adnexae],
 [Right adnexae Text],
 [Right ovary],
 [Right ovary Text],

 [Heart sounds],
 [Heart sounds Text],
 [Breath sounds],
 [Breath sounds Text],
 [Spine],
 [Spine Text],
 [Mallampati intubation score],
 [Mallampati intubation score Text],
 [Postnatal problems],
 [Postnatal problems Problem 1],
 [Postnatal problems Problem 2],
 [Postnatal problems Problem 3],
 [Postnatal problems Problem 4],
 [Postnatal problems Text],

 [Appears jaundiced],
 [Respiration rate],
 [Signs of respiratory distress],
 [Signs of respiratory distress Reason 1],
 [Signs of respiratory distress Reason 2],
 [Signs of respiratory distress Reason 3],
 [Signs of respiratory distress Reason 4],
 [Colour],
 [Eyes 2],
 [Eyes 2 Side],
 [Eyes 2 Swab taken],
 [Umbilicus],
 [Umbilicus Swab taken],
 [Tone],

 [Coma - eyes open],
 [Verbal response],
 [Motor response],
 [Left pupil size],
 [Left pupil reaction],
 [Right pupil size],
 [Right pupil reaction],
 [Left arm movements],
 [Right arm movements],
 [Left leg movements],
 [Right leg movements],

 [Head circumference],
 [Head circumference centile],

 [Uterine contents],
 [Uterine contents Text],
 [Any fetal pole detected],
 [Any fetal heart detected],
 [Evacuation],
 [Evacuation Text],

 [Urine passed 2],
 [Aspiration or vomit],
 [Aspiration or vomit Bile stained],
 [Aspiration or vomit Blood stained],
 [Aspiration or vomit Milk],
 [Aspiration or vomit Meconium],
 [Aspiration or vomit Amount],
 [Feeding ability 2],
 [Feeding ability 2 Appetite],
 [Feeding ability 2 Time taken to feed],
 [Feeding ability 2 Text]

 FROM 
 tbl_ReviewExaminations










