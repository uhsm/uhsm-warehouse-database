﻿




CREATE VIEW dbo.view_ReviewInvestigations_Screening
AS 
SELECT 
 [Patient_Pointer],
 [Pregnancy_ID],
 [Pregnancy number],
 [Review_Number],

 [ABO blood group],
 [AFP and downs screen],
 [AFP and downs screen Text],
 dbo.func_Convert_Integer( [Haemoglobin], 'g/l' ) [Haemoglobin (g/l)],
 [Haemoglobin Qualifier],
 [MetHb (max this period)],
 [MetHb (min this period)],
 [Local urinalysis test],
 [Local urinalysis test Text],
 [Products of conception felt],
 [Products of conception felt Sent to pathology],
 [Reason for change 1],
 [Reason for change 2],
 [Reason for GTT],
 [Reason for GTT Text],
 [Rhesus antibodies],
 [Rhesus antibodies Antibody Type],
 [Speculum],
 [Speculum Text],
 [Worst base deficit-this period],
 [Lipaeamic serum],

-- Asymp bacteriuria screening
 [Asymp. bacteriuria screening/MSU offered] [Asymp bacteriuria screening/MSU offered],
 [Asymp. bacteriuria screening/MSU offered Reason] [Asymp bacteriuria screening/MSU offered Reason],
 [Asymp. bacteriuria screening/MSU accepted] [Asymp bacteriuria screening/MSU accepted],
 dbo.func_Convert_Date( [Asymp. bacteriuria screening/MSU accepted Date MSU performed]) [Asymp bacteriuria/MSU accepted Date MSU performed],
 dbo.func_Convert_Date( [Asymp. bacteriuria screening/MSU accepted Date of result]) [Asymp bacteriuria/MSU accepted Date of result],
 [Asymp. bacteriuria screening/MSU accepted Result] [Asymp bacteriuria screening/MSU accepted Result],
 [Asymp. bacteriuria screening/MSU accepted Comments] [Asymp bacteriuria screening/MSU accepted Comments],
 [Asymp. bacteriuria screening/MSU accepted Reason MSU not performed] [Asymp bacteriuria/MSU accepted Reason MSU not performed],
 [MSU],
 [MSU Organism],
 [MSU Text],
 [High vaginal swab],
 [High vaginal swab Organism],
 [High vaginal swab Text],

-- Group B Streptococcus screening
 [Group B Streptococcus screening offered],
 [Group B Streptococcus screening offered Reason],
 [Group B Streptococcus screening accepted],
 [Reason Group B Streptococcus screening not taken],
 dbo.func_Convert_Date( [Date Group B Streptococcus screening performed]) [Date Group B Streptococcus screening performed],
 dbo.func_Convert_Date( [Date Group B Streptococcus screening result]) [Date Group B Streptococcus screening result],
 [Result of Group B Streptococcus screening],
 [Group B streptococcus screen],
 [Group B streptococcus screen Text],

-- Hepatitis A screening
 [Hepatitis A screening offered],
 [Hepatitis A screening offered Reason],
 [Hepatitis A screening accepted],
 [Reason Hepatitis A screening not done],
 dbo.func_Convert_Date( [Date Hepatitis A screening performed]) [Date Hepatitis A screening performed],
 [Details of Hepatitis A screening],
 dbo.func_Convert_Date( [Date of Hepatitis A screening result]) [Date of Hepatitis A screening result],
 [Result of Hepatitis A screening],

-- Hepatitis B screening
 [Maternal Hepatitis B status],
 [Hepatitis B screening offered],
 [Hepatitis B screening offered Reason],
 [Hepatitis B screening accepted],
 [Reason Hepatitis B screening not done],
 dbo.func_Convert_Date( [Date Hepatitis B screening performed]) [Date Hepatitis B screening performed],
 [Details of Hepatitis B screening],
 dbo.func_Convert_Date( [Date of Hepatitis B screening result]) [Date of Hepatitis B screening result],
 [Result of Hepatitis B screening],

-- Hepatitis C screening
 [Hepatitis C screening offered],
 [Hepatitis C screening offered Reason],
 [Hepatitis C screening accepted],
 [Reason Hepatitis C screening not done],
 dbo.func_Convert_Date( [Date Hepatitis C screening performed]) [Date Hepatitis C screening performed],
 [Details of Hepatitis C screening],
 dbo.func_Convert_Date( [Date of Hepatitis C screening result]) [Date of Hepatitis C screening result],
 [Result of Hepatitis C screening],

-- HIV
 [HIV status known],
 dbo.func_Convert_Date( [Date of Diagnosis]) [Date of Diagnosis],
 [Year],
 [HIV screening offered],
 [HIV screening offered Reason],
 [HIV screening accepted],
 [Reason HIV screening not done],
 dbo.func_Convert_Date( [Date HIV screen performed]) [Date HIV screen performed],
 [Details of HIV screening],
 dbo.func_Convert_Date( [Date of HIV screen result]) [Date of HIV screen result],
 [Result of HIV],
 [Result of HIV screening],
 [Referral to HIV Midwife],
 [Referral to HIV Midwife Text],
 [HIV screen],
 [HIV screen Text],

-- Rubella screening
 [Rubella screening offered],
 [Rubella screening offered Reason],
 [Rubella screening accepted],
 [Reason Rubella screening not done],
 dbo.func_Convert_Date( [Date Rubella screening performed]) [Date Rubella screening performed],
 [Details of Rubella screening],
 dbo.func_Convert_Date( [Date of Rubella screening result]) [Date of Rubella screening result],
 [Result of Rubella screening],

-- Syphilis screening
 [Syphilis screening offered],
 [Syphilis screening offered Reason],
 [Syphilis screening accepted],
 [Reason Syphilis screen not done],
 dbo.func_Convert_Date( [Date Syphilis screening performed]) [Date Syphilis screening performed],
 [Details of Syphilis screening],
 dbo.func_Convert_Date( [Date of Syphilis screening result]) [Date of Syphilis screening result],
 [Result of Syphilis screening],

-- Other screening 1
 [Other screening offered - 1],
 [Other screening offered - 1 Name of the screening],
 [Other screening accepted - 1],
 [Reason Other screening not done - 1],
 dbo.func_Convert_Date( [Date Other screening performed - 1]) [Date Other screening performed - 1],
 dbo.func_Convert_Date( [Date of Other screening result - 1]) [Date of Other screening result - 1],
 [Result of Other screening - 1],

-- Other screening 2
 [Other screening offered - 2],
 [Other screening offered - 2 Name of the screening],
 [Other screening accepted - 2],
 [Reason Other screening not done - 2],
 dbo.func_Convert_Date( [Date Other screening performed - 2]) [Date Other screening performed - 2],
 dbo.func_Convert_Date( [Date of Other screening result - 2]) [Date of Other screening result - 2],
 [Result of Other screening - 2],

-- Other screening 3
 [Other screening offered - 3],
 [Other screening offered - 3 Name of the screening],
 [Other screening accepted - 3],
 [Reason Other screening not done - 3],
 dbo.func_Convert_Date( [Date Other screening performed - 3]) [Date Other screening performed - 3],
 dbo.func_Convert_Date( [Date of Other screening result - 3]) [Date of Other screening result - 3],
 [Result of Other screening - 3],

 [Swabs taken],
 [Swabs taken HVS],
 [Swabs taken Endocervical],
 [Swabs taken Smear],
 [Swabs taken Chlamydia],
 [Swabs taken Anal],
 [Swabs taken Interoital]

 FROM 
 tbl_ReviewInvestigations









