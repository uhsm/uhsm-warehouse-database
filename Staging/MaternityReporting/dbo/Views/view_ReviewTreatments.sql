﻿





CREATE VIEW dbo.view_ReviewTreatments
AS 
SELECT 
 [Patient_Pointer],
 [Pregnancy_ID],
 [Pregnancy number],
 [Review_Number],
 [Antibiotics today],
 [Anticoagulant],
 [Anticonvulsants today],

 [Anti-D],
 dbo.func_Convert_Date( [Anti-D Date]) [Anti-D Date],
 dbo.func_Convert_Time( [Anti-D Time]) [Anti-D Time],
 dbo.func_Convert_Integer( [Anti-D Dose], 'IU') [Anti-D Dose (IU)],
 [Anti-D Batch],
 [Anti-D Text],
 [Anti-D accepted],
 dbo.func_Convert_Date( [Anti-D accepted Date administered]) [Anti-D accepted Date administered],
 dbo.func_Convert_Time( [Anti-D accepted Time administered]) [Anti-D accepted Time administered],
 dbo.func_Convert_Integer( [Anti-D accepted Dose administered], 'IU') [Anti-D accepted Dose administered (IU)],
 [Anti-D accepted Batch],
 [Anti-D accepted Reason anti-D not given],
 [Anti-D offered],
 [Anti-D offered Reason],
 [Reason Anti-D not given],

 [Base corrections given today],
 [Diabetes treatment this review],
 [Inotropic support given today],
 [Insulin 1],
 [Insulin 2],
 [Insulin 3],
 [Intrapartum IV Fluids given],
 [Intrapartum IV Fluids given Dextrose 5],
 [Intrapartum IV Fluids given Dextrose 10],
 [Intrapartum IV Fluids given Normal Saline],
 [Intrapartum IV Fluids given Hartmanns],
 [Intrapartum IV Fluids given Other],
 [Intrapartum Pain Relief given],
 [Intrapartum Pain Relief given Self_admin_inhalation],
 [Intrapartum Pain Relief given Non-opioid analgesia],
 [Intrapartum Pain Relief given Pethidine],
 [Intrapartum Pain Relief given Diamorphine],
 [Intrapartum Pain Relief given Epidural Marcaine],
 [Intrapartum Pain Relief given Other],
 [Kleihauer taken],
 [Kleihauer taken Text],
 [Muscle relaxants used today],
 [Other IV infusions],
 [Other IV infusions Blood],
 [Other IV infusions FFP],
 [Other IV infusions Platelets],
 [Other IV infusions Saline bolus],
 [Other IV infusions Cryoprecipitate],
 [Other IV infusions HAS 4.5%] [Other IV infusions HAS 4-5%],
 [Other IV infusions HAS 20%],
 [Other IV infusions Text],
 [Pref. insulin delivery method] [Pref insulin delivery method],
 [Sedatives today],
 [Surfactant used today],
 [Surfactant used today Doses],
 [Syntocinon],
 [Thyroid replacement],
 [Treatment 1],
 [Treatment 2],
 [Treatment 3],
 [Treatment 4],
 [Treatment 5],
 [Treatment 6],
 [Treatment 7],
 [Treatment 8],
 [Treatment 9],
 [Treatment 10],
 [Treatment 11],
 [Treatment 12],
 [Treatment 13],
 [Treatment 14],
 [Treatment 15],
 [Treatment 16],
 [Treatment 17],
 [Treatment 18],
 [Treatment 19],
 [Treatment 20],
 [Treatment 21],
 [Treatment 22],
 [Treatment 23],
 [Treatment 24],
 [Treatment 25],
 [Treatment 26],
 [Treatment 27],
 [Treatment 28],
 [Treatment 29],
 [Treatment 30],
 [Treatment X],
 [Treatment Y],
 [Treatment Z]
 FROM
 tbl_ReviewTreatments








