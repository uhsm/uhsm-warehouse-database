﻿


CREATE VIEW [dbo].[view_ShowTableCounts]
AS
-- Run Counts
select 1 [Ref],'tbl_Patients' [Table Name],count(*) [Record Count] from tbl_Patients
union
select 2,'tbl_FamilyHistory',count(*) from tbl_FamilyHistory
union
select 3,'tbl_MedicalHistory',count(*) from tbl_MedicalHistory
union
select 4,'tbl_ObstetricSummary',count(*) from tbl_ObstetricSummary
union
select 5,'tbl_PersonalInformation',count(*) from tbl_PersonalInformation
union
select 6,'tbl_SocialHistory',count(*) from tbl_SocialHistory
union
select 7,'tbl_ObstetricProblemSummary',count(*) from tbl_ObstetricProblemSummary
union
select 8,'tbl_InpatientSpells',count(*) from tbl_InpatientSpells
union
select 9,'tbl_AdmissionTransferEpisodes',count(*) from tbl_AdmissionTransferEpisodes
union
select 10,'tbl_Pregnancy',count(*) from tbl_Pregnancy
union
select 11,'tbl_Referrals',count(*) from tbl_Referrals
union
select 12,'tbl_ClinicalReviews',count(*) from tbl_ClinicalReviews
union
select 13,'tbl_FetalObservations',count(*) from tbl_FetalObservations
union
select 14,'tbl_ReviewExaminations',count(*) from tbl_ReviewExaminations
union
select 15,'tbl_ReviewInvestigations',count(*) from tbl_ReviewInvestigations
union
select 16,'tbl_ReviewTreatments',count(*) from tbl_ReviewTreatments
union
select 17,'tbl_PregnancyCarePlans',count(*) from tbl_PregnancyCarePlans
union
select 18,'tbl_SocialServicesReferral',count(*) from tbl_SocialServicesReferral
union
select 19,'tbl_Delivery',count(*) from tbl_Delivery
union
select 20,'tbl_Baby',count(*) from tbl_Baby
union
select 21,'tbl_DeathOfBaby',count(*) from tbl_DeathOfBaby
union
select 22,'tbl_BloodSpotScreening',count(*) from tbl_BloodSpotScreening
union
select 99,'tbl_Register_Copy',count(*) from tbl_Register_Copy
union
select 99,'tbl_Notes_Copy',count(*) from tbl_Notes_Copy
union
select 99,'tbl_Stafflink_Copy',count(*) from tbl_Stafflink_Copy





