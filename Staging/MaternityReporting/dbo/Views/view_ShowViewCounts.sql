﻿



--DROP VIEW [dbo].[view_ShowViewCounts]

CREATE VIEW [dbo].[view_ShowViewCounts]
AS
-- Run Counts
select 1.0 [Ref],'view_Patients' [Table Name],count(*) [Record Count] from view_Patients
union
select 2,'view_FamilyHistory',count(*) from view_FamilyHistory
union
select 3,'view_MedicalHistory',count(*) from view_MedicalHistory
union
select 4,'view_ObstetricSummary',count(*) from view_ObstetricSummary
union
select 5,'view_PersonalInformation',count(*) from view_PersonalInformation
union
select 6,'view_SocialHistory',count(*) from view_SocialHistory
union
select 7,'view_ObstetricProblemSummary',count(*) from view_ObstetricProblemSummary
union
select 8,'view_InpatientSpells',count(*) from view_InpatientSpells
union
select 9,'view_AdmissionTransferEpisodes',count(*) from view_AdmissionTransferEpisodes
union
select 10.1,'view_Pregnancy',count(*) from view_Pregnancy
union
select 10.2,'view_Pregnancy_Assessment',count(*) from view_Pregnancy_Assessment
union
select 11,'view_Referrals',count(*) from view_Referrals
union
select 12,'view_ClinicalReviews',count(*) from view_ClinicalReviews
union
select 13,'view_FetalObservations',count(*) from view_FetalObservations
union
select 14,'view_ReviewExaminations',count(*) from view_ReviewExaminations
union
select 15.1,'view_ReviewInvestigations_Screening',count(*) from view_ReviewInvestigations_Screening
union
select 15.2,'view_ReviewInvestigations_Tests',count(*) from view_ReviewInvestigations_Tests
union
select 16,'view_ReviewTreatments',count(*) from view_ReviewTreatments
union
select 17,'view_PregnancyCarePlans',count(*) from view_PregnancyCarePlans
union
select 18,'view_SocialServicesReferral',count(*) from view_SocialServicesReferral
union
select 19.1,'view_Delivery',count(*) from view_Delivery
union
select 19.2,'view_Delivery_Labour',count(*) from view_Delivery_Labour
union
select 19.3,'view_Delivery_PostNatal',count(*) from view_Delivery_PostNatal
union
select 20.1,'view_Baby_Birth',count(*) from view_Baby_Birth
union
select 20.2,'view_Baby_Birth_2',count(*) from view_Baby_Birth_2
union
select 20.3,'view_Baby_Birth_StaffPresent',count(*) from view_Baby_Birth_StaffPresent
union
select 20.4,'view_Baby_Resuscitation',count(*) from view_Baby_Resuscitation
union
select 20.5,'view_Baby_Details',count(*) from view_Baby_Details
union
select 20.6,'view_Baby_PostNatal',count(*) from view_Baby_PostNatal
union
select 21,'view_DeathOfBaby Of Baby',count(*) from view_DeathOfBaby
union
select 22,'view_BloodSpotScreening',count(*) from view_BloodSpotScreening







