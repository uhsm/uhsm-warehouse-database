﻿




CREATE VIEW dbo.view_SocialHistory
AS
SELECT     
	Patient_Pointer, 

	[Country of origin],
	[Faith/Religion], 
	[Ethnic category], 

	[Occupational Status], 
	[Occupational Status Occupation], 
	[Occupational Status Shift worker], 
	[Occupational Status Text], 

	[Health visitor], 
	[Health visitor Phone], 
	[Health visitor Address 1], 
	[Health visitor Address 2], 
	[Health visitor Address 3], 
	[Health visitor Address 4], 

	[Language ability], 
	[Language ability Interpreter required], 
	[Language ability Native tongue], 

	[Special dietary requirements], 
	[Special dietary requirements Text], 

	Mobility, 
	[Mobility Text], 

	[Motor Vehicle driver], 
	[Motor Vehicle driver Category], 
	[DVLA informed], 
                                            
	[Cooking facilities], 
	[Cooking facilities Text], 
	[Cooking skills], 
	[Cooking skills Text], 

	[Meals taken outside the home], 
	[Meals taken outside the home Eats out], 
	[Meals taken outside the home Meals on wheels], 
	[Meals taken outside the home Day centre], 
	[Meals taken outside the home Luncheon clubs], 
	[Meals taken outside the home Family visits], 
	[Meals taken outside the home Text], 

	[Shopping capabilities], 
	[Shopping capabilities Family], 
	[Shopping capabilities Home carer], 
	[Shopping capabilities Neighbour], 
	[Shopping capabilities Text]
FROM         
	dbo.tbl_SocialHistory





