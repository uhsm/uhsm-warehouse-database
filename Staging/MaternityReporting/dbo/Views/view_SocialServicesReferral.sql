﻿





CREATE VIEW dbo.view_SocialServicesReferral
AS 
SELECT 
 [Patient_Pointer],
 [Pregnancy_ID],
 [Pregnancy number],

 dbo.func_Convert_Date( [Date of referral]) [Date of referral],
 dbo.func_Convert_Time( [Time of referral]) [Time of referral],

 [Reason for soc services ref],
 [Reason for soc services ref Text],

 [Additional reason for ref 1],
 [Additional reason for ref 1 Text],
 [Additional reason for ref 2],
 [Additional reason for ref 2 Text],
 [Additional reason for ref 3],
 [Additional reason for ref 3 Text],

 [Named social worker],
 [Named social worker Contact Number],

 [Named community midwife],
 [Named community midwife Contact Number],

 [Named health visitor],
 [Named health visitor Contact Number],

 [Other named professional],
 [Other named professional Contact Number],

 [Name of staff],
 [Name of staff Grade],
 [Type of staff],

 [Antenatal issues],
 [Antenatal issues Text],

 [Case conference],
 [Case conference Text],

 [Case discussion],
 [Case discussion Text],

 [Child protection issue],
 [Child protection issue Issue 1],
 [Child protection issue Issue 2],
 [Child protection issue Issue 3],
 [Child protection issue Text],
 [Child protection officer],
 [Child protection officer Contact Number],
 [Child protection outcome],
 [Child protection outcome Text],

 [Consent obtained],

 [Delivery issues],
 [Delivery issues Text],

 [Further comments],
 [Further comments Text],

 [Postnatal issues],
 [Postnatal issues Text],

 [Protection case referred by],

 [Specific protection action],
 [Specific protection action Text]

 FROM 
 tbl_SocialServicesReferral






