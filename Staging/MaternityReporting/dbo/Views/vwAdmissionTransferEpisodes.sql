﻿Create View vwAdmissionTransferEpisodes AS SELECT 	[Patient_Pointer] AS [Patient_Pointer],
	[Spell_Number] AS [Spell_Number],
	[AdmissionTransfer_Number] AS [AdmissionTransfer_Number],
	[Adm/trans deletion allowed] AS [Adm trans deletion allowed],
	[Adm/trans episode number] AS [Adm trans episode number],
	[Adm/trans event number] AS [Adm trans event number],
	[Admission/transfer flag] AS [Admission transfer flag],
	[Admission/transfer type] AS [Admission transfer type],
	[BAPM Source of admission] AS [BAPM Source of admission],
	[Consultants code] AS [Consultants code],
	[Consultants name] AS [Consultants name],
	[COPPISH admission type] AS [COPPISH admission type],
	[COPPISH admission/transfer from] AS [COPPISH admission transfer from],
	[Date of admission/transfer] AS [Date of admission transfer],
	[External adm/trans code] AS [External adm trans code],
	[General Practitioner Name] AS [General Practitioner Name],
	[GP Address code] AS [GP Address code],
	[GP code] AS [GP code],
	[GP Practice code] AS [GP Practice code],
	[Hospital adm this pregnancy] AS [Hospital adm this pregnancy],
	[Internal adm/trans number] AS [Internal adm trans number],
	[Location admitted/trans to] AS [Location admitted trans to],
	[Method of admission/transfer] AS [Method of admission transfer],
	[Significant Facilty] AS [Significant Facilty],
	[Source of admission/transfer] AS [Source of admission transfer],
	[Specialty code] AS [Specialty code],
	[Time of admission/transfer] AS [Time of admission transfer],
	[Type of unit admitted/transferred to] AS [Type of unit admitted transferred to],
	[BirthRate date of arrival] AS [BirthRate date of arrival],
	[BirthRate date of departure] AS [BirthRate date of departure],
	[BirthRate time of arrival] AS [BirthRate time of arrival],
	[BirthRate time of departure] AS [BirthRate time of departure],
	[Still on Labour Ward] AS [Still on Labour Ward],
	[Admission Plan] AS [Admission Plan],
	[Admission Plan Text] AS [Admission Plan Text],
	[Admission/transfer reason 1] AS [Admission transfer reason 1],
	[Admission/transfer reason 1 Text] AS [Admission transfer reason 1 Text],
	[Admission/transfer reason 2] AS [Admission transfer reason 2],
	[Admission/transfer reason 2 Text] AS [Admission transfer reason 2 Text],
	[Admission/transfer reason 3] AS [Admission transfer reason 3],
	[Admission/transfer reason 3 Text] AS [Admission transfer reason 3 Text],
	[BAPM category of admission] AS [BAPM category of admission],
	[Brought to unit by] AS [Brought to unit by],
	[Brought to unit by Text] AS [Brought to unit by Text],
	[COPPISH Admission Reason] AS [COPPISH Admission Reason],
	[GP informed] AS [GP informed],
	[GP informed Text] AS [GP informed Text],
	[Placed in] AS [Placed in],
	[Placed in Oxygen] AS [Placed in Oxygen],
	[Reason for admission 4] AS [Reason for admission 4],
	[Reason for admission 4 Text] AS [Reason for admission 4 Text],
	[Reason for admission 5] AS [Reason for admission 5],
	[Reason for admission 5 Text] AS [Reason for admission 5 Text],
	[Reason for admission 6] AS [Reason for admission 6],
	[Reason for admission 6 Text] AS [Reason for admission 6 Text],
	[Reason for discharge] AS [Reason for discharge],
	[Reason for discharge Text] AS [Reason for discharge Text],
	[Transport Method] AS [Transport Method],
	[Transport Method Oxygen] AS [Transport Method Oxygen],
	[Transport Method Ventilated] AS [Transport Method Ventilated] FROM tbl_AdmissionTransferEpisodes
 
