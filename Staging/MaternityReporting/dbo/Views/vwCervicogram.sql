﻿Create View vwCervicogram AS SELECT 	[Patient_Pointer] AS [Patient_Pointer],
	[Pregnancy_ID] AS [Pregnancy_ID],
	[Pregnancy number] AS [Pregnancy number],
	[Examination_Number] AS [Examination_Number],
	[Hours in labour] AS [Hours in labour],
	[Date of Vaginal Examination] AS [Date of Vaginal Examination],
	[Time of Vaginal Examination] AS [Time of Vaginal Examination],
	[Cervical dilation] AS [Cervical dilation],
	[Date of Review] AS [Date of Review],
	[Time of Review] AS [Time of Review],
	[Type of Review] AS [Type of Review],
	[Birth Order] AS [Birth Order] FROM tbl_Cervicogram
 
