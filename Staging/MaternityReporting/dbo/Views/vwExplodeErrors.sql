﻿CREATE View vwExplodeErrors as
select 
Ltrim(Rtrim(EventDescription)) as ErrDesc
from tbl_EventLog
Where EventDescription Like 'SQL Error%'
and (cast(floor(cast(EventDateTime as float)) as Datetime) =  cast(floor(cast(GetDate() as float)) as Datetime))
