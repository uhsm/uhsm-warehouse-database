﻿Create View vwGetLatestPregnancyNumber AS



SELECT
A.Patient_Pointer,
A.Pregnancy_ID,
A.[Pregnancy Order]
FROM vwGetPregnancyNumber A
INNER JOIN
	(
	SELECT 
	Patient_Pointer,
	MAX([Pregnancy Order]) AS LatestPregnancy
	FROM vwGetPregnancyNumber
	GROUP BY 
	[Patient_Pointer]
	) B ON A.Patient_Pointer = B.Patient_Pointer AND A.[Pregnancy Order] = B.LatestPregnancy
	
UNION
SELECT
Patient_Pointer,
Pregnancy_ID,
CAST(REPLACE(SUBSTRING([Pregnancy number],CHARINDEX('[',[Pregnancy number])+1,2),']','') AS INT) AS [Pregnancy Order]
FROM tbl_Delivery
WHERE 
	CAST(REPLACE(SUBSTRING([Pregnancy number],CHARINDEX('[',[Pregnancy number])+1,2),']','') AS INT) = 99
AND Patient_Pointer NOT IN (
	SELECT Patient_Pointer from vwGetPregnancyNumber)
