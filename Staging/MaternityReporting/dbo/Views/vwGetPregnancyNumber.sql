﻿Create View vwGetPregnancyNumber AS

	SELECT 
	Patient_Pointer,
	Pregnancy_ID,
	[Pregnancy number],
	CAST(REPLACE(SUBSTRING([Pregnancy number],CHARINDEX('[',[Pregnancy number])+1,2),']','') AS INT) AS [Pregnancy Order]
	FROM dbo.tbl_Delivery
	WHERE 
	CAST(REPLACE(SUBSTRING([Pregnancy number],CHARINDEX('[',[Pregnancy number])+1,2),']','') AS INT) != 99
