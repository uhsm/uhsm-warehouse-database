﻿Create View vwMedicalHistory AS SELECT 	[Patient_Pointer] AS [Patient_Pointer],
	[History of Genital Infections] AS [History of Genital Infections],
	[History of Genital Infections Disease 1] AS [History of Genital Infections Disease 1],
	[History of Genital Infections Disease 2] AS [History of Genital Infections Disease 2],
	[History of Genital Infections Disease 3] AS [History of Genital Infections Disease 3],
	[History of Genital Infections Disease 4] AS [History of Genital Infections Disease 4],
	[History of Genital Infections Details] AS [History of Genital Infections Details],
	[Previous pelvic/leg fracture] AS [Previous pelvic leg fracture],
	[Previous pelvic/leg fracture Text] AS [Previous pelvic leg fracture Text],
	[Previous gynae operations] AS [Previous gynae operations],
	[Previous gynae operations Cone Biopsy] AS [Previous gynae operations Cone Biopsy],
	[Previous gynae operations D&C] AS [Previous gynae operations D C],
	[Previous gynae operations Hysteroscopy] AS [Previous gynae operations Hysteroscopy],
	[Previous gynae operations Myomectomy] AS [Previous gynae operations Myomectomy],
	[Previous gynae operations Pelvic Floor Repair] AS [Previous gynae operations Pelvic Floor Repair],
	[Previous gynae operations Reconstruction Uterus] AS [Previous gynae operations Reconstruction Uterus],
	[Previous gynae operations Other] AS [Previous gynae operations Other],
	[Previous infertility treatment] AS [Previous infertility treatment],
	[Previous infertility treatment Clomiphene] AS [Previous infertility treatment Clomiphene],
	[Previous infertility treatment Tamoxifen] AS [Previous infertility treatment Tamoxifen],
	[Previous infertility treatment Pergonyl] AS [Previous infertility treatment Pergonyl],
	[Previous infertility treatment HCG] AS [Previous infertility treatment HCG],
	[Previous infertility treatment Insemination] AS [Previous infertility treatment Insemination],
	[Previous infertility treatment IVF] AS [Previous infertility treatment IVF],
	[Previous infertility treatment GIFT] AS [Previous infertility treatment GIFT],
	[Previous infertility treatment Other] AS [Previous infertility treatment Other],
	[History of back problems] AS [History of back problems],
	[History of back problems Text] AS [History of back problems Text],
	[HIV status] AS [HIV status],
	[HIV status Most recent test] AS [HIV status Most recent test],
	[History of diabetes] AS [History of diabetes],
	[History of diabetes Kind] AS [History of diabetes Kind],
	[History of diabetes Text] AS [History of diabetes Text],
	[Previous heart surgery] AS [Previous heart surgery],
	[Previous heart surgery Text] AS [Previous heart surgery Text],
	[Established AIDS sufferer] AS [Established AIDS sufferer],
	[Any haemoglobinopathy] AS [Any haemoglobinopathy],
	[Any haemoglobinopathy Condition] AS [Any haemoglobinopathy Condition],
	[Any haemoglobinopathy Condition 2] AS [Any haemoglobinopathy Condition 2],
	[Any haemoglobinopathy Text] AS [Any haemoglobinopathy Text],
	[History of thrombosis] AS [History of thrombosis],
	[History of thrombosis DVT] AS [History of thrombosis DVT],
	[History of thrombosis Pulmonary Embolism] AS [History of thrombosis Pulmonary Embolism],
	[History of thrombosis Other] AS [History of thrombosis Other],
	[General gynaecological prob.] AS [General gynaecological prob],
	[General gynaecological prob. Text] AS [General gynaecological prob  Text],
	[Previous blood transfusion] AS [Previous blood transfusion],
	[Previous blood transfusion Text] AS [Previous blood transfusion Text],
	[Hist of essential hypertension] AS [Hist of essential hypertension],
	[Hist of essential hypertension Text] AS [Hist of essential hypertension Text],
	[History of asthma] AS [History of asthma],
	[History of asthma Text] AS [History of asthma Text],
	[Other medical problems] AS [Other medical problems],
	[Other medical problems Text] AS [Other medical problems Text],
	[Potential anaesthetic hazards] AS [Potential anaesthetic hazards],
	[Potential anaesthetic hazards Text] AS [Potential anaesthetic hazards Text],
	[Any psychological problems] AS [Any psychological problems],
	[Any psychological problems Text] AS [Any psychological problems Text],
	[Previous tubal surgery] AS [Previous tubal surgery],
	[Previous tubal surgery Sterilisation Reversal] AS [Previous tubal surgery Sterilisation Reversal],
	[Previous tubal surgery P.I.D Repair] AS [Previous tubal surgery P I D Repair],
	[Previous tubal surgery Ectopic] AS [Previous tubal surgery Ectopic],
	[Previous tubal surgery Text] AS [Previous tubal surgery Text],
	[History of allergies] AS [History of allergies],
	[History of allergies Allergy 1] AS [History of allergies Allergy 1],
	[History of allergies Allergy 2] AS [History of allergies Allergy 2],
	[History of allergies Allergy 3] AS [History of allergies Allergy 3],
	[History of allergies Text] AS [History of allergies Text],
	[Previous anaesthetic problem] AS [Previous anaesthetic problem],
	[Previous anaesthetic problem Text] AS [Previous anaesthetic problem Text],
	[Other past surgical history] AS [Other past surgical history],
	[Other past surgical history Text] AS [Other past surgical history Text],
	[Previous SB/NND/Misc] AS [Previous SB NND Misc],
	[Previous SB/NND/Misc Text] AS [Previous SB NND Misc Text],
	[History of migraine] AS [History of migraine],
	[History of migraine Text] AS [History of migraine Text],
	[History of recurrent UTIs] AS [History of recurrent UTIs],
	[History of recurrent UTIs Text] AS [History of recurrent UTIs Text],
	[History of heart problems] AS [History of heart problems],
	[History of heart problems Level of Symptoms] AS [History of heart problems Level of Symptoms],
	[History of heart problems Details] AS [History of heart problems Details],
	[Any mental health problems] AS [Any mental health problems],
	[Any mental health problems Text] AS [Any mental health problems Text],
	[Any previous HIV test] AS [Any previous HIV test],
	[Any previous HIV test Date] AS [Any previous HIV test Date],
	[Hist of preg ind hypertension] AS [Hist of preg ind hypertension],
	[Hist of preg ind hypertension Text] AS [Hist of preg ind hypertension Text],
	[Ever had smear test] AS [Ever had smear test],
	[Ever had smear test Last Test] AS [Ever had smear test Last Test],
	[Ever had smear test Results] AS [Ever had smear test Results],
	[Ever had smear test Text] AS [Ever had smear test Text],
	[Previous infertility problems] AS [Previous infertility problems],
	[Previous infertility problems Text] AS [Previous infertility problems Text],
	[Kidney disease or surgery] AS [Kidney disease or surgery],
	[Kidney disease or surgery Kidney Transplant] AS [Kidney disease or surgery Kidney Transplant],
	[Kidney disease or surgery Ureteric Reimplantation] AS [Kidney disease or surgery Ureteric Reimplantation],
	[Kidney disease or surgery Other] AS [Kidney disease or surgery Other],
	[History of clotting disorders] AS [History of clotting disorders],
	[History of clotting disorders Haemophilia carrier] AS [History of clotting disorders Haemophilia carrier],
	[History of clotting disorders Von Willebrands] AS [History of clotting disorders Von Willebrands],
	[History of clotting disorders Other] AS [History of clotting disorders Other],
	[History of clotting disorders Details] AS [History of clotting disorders Details],
	[History of epilepsy] AS [History of epilepsy],
	[History of epilepsy Text] AS [History of epilepsy Text],
	[History of thyroid disease] AS [History of thyroid disease],
	[History of thyroid disease Current Status] AS [History of thyroid disease Current Status],
	[History of thyroid disease Text] AS [History of thyroid disease Text],
	[Group B Strep Carrier] AS [Group B Strep Carrier],
	[Group B Strep Carrier Details] AS [Group B Strep Carrier Details],
	[History of anaemia] AS [History of anaemia],
	[History of anaemia Iron Deficiency] AS [History of anaemia Iron Deficiency],
	[History of anaemia Folate Deficiancy] AS [History of anaemia Folate Deficiancy],
	[History of anaemia Pernicious Anaemia] AS [History of anaemia Pernicious Anaemia],
	[History of anaemia Text] AS [History of anaemia Text],
	[History of chest problems] AS [History of chest problems],
	[History of chest problems Text] AS [History of chest problems Text],
	[Previous non gynae operations] AS [Previous non gynae operations],
	[Previous non gynae operations Appendicectomy] AS [Previous non gynae operations Appendicectomy],
	[Previous non gynae operations Laparotomy] AS [Previous non gynae operations Laparotomy],
	[Previous non gynae operations Other] AS [Previous non gynae operations Other],
	[Any Previous Colposcopy] AS [Any Previous Colposcopy],
	[Any Previous Colposcopy Text] AS [Any Previous Colposcopy Text],
	[Hepatitis or jaundice] AS [Hepatitis or jaundice],
	[Hepatitis or jaundice Infective Hepatitis(A)] AS [Hepatitis or jaundice Infective Hepatitis A],
	[Hepatitis or jaundice Serum Hepatitis(B)] AS [Hepatitis or jaundice Serum Hepatitis B],
	[Hepatitis or jaundice Serum Hepatitis(C)] AS [Hepatitis or jaundice Serum Hepatitis C],
	[Hepatitis or jaundice Cholestasis] AS [Hepatitis or jaundice Cholestasis],
	[Hepatitis or jaundice Other] AS [Hepatitis or jaundice Other],
	[Details of tuberculosis] AS [Details of tuberculosis],
	[History of tuberculosis] AS [History of tuberculosis],
	[History of tuberculosis Active] AS [History of tuberculosis Active] FROM tbl_MedicalHistory
 
