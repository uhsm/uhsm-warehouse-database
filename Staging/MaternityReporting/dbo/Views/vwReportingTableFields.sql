﻿Create View vwReportingTableFields as 
select
a.name as TName,
b.colorder,
b.name as ColumnName,
dbo.ufnFormatHeader(b.name) as UseableName
from sysobjects a 
inner join syscolumns b on a.[id] = b.[id]
where a.name like 'tbl_%'
and a.xtype = 'U'



