﻿Create View vwTableFirstLastCols AS
select
TName,
Min(colorder) as FirstCol,
Max(colorder) as LastCol
From vwReportingTableFields
Group By TName

