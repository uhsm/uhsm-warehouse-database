﻿CREATE TABLE [CSC].[NHSEVOLUTION_user_admin_requests] (
    [request_id]  INT             IDENTITY (1, 1) NOT NULL,
    [date_logged] DATETIME        CONSTRAINT [DF_NE_user_admin_requests_1] DEFAULT (getdate()) NOT NULL,
    [logged_by]   [sysname]       CONSTRAINT [DF_NE_user_admin_requests_2] DEFAULT (suser_name()) NOT NULL,
    [new_login]   BIT             CONSTRAINT [DF_NE_user_admin_requests_3] DEFAULT ((0)) NOT NULL,
    [login_name]  [sysname]       NOT NULL,
    [password]    [sysname]       NOT NULL,
    [modified]    DATETIME        CONSTRAINT [DF_NE_user_admin_requests_4] DEFAULT (getdate()) NOT NULL,
    [modified_by] [sysname]       CONSTRAINT [DF_NE_user_admin_requests_5] DEFAULT (suser_name()) NOT NULL,
    [message]     NVARCHAR (2047) NULL,
    [processed]   BIT             CONSTRAINT [DF_NE_user_admin_requests_6] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_NE_user_admin_requests] PRIMARY KEY NONCLUSTERED ([request_id] ASC)
);


GO
CREATE TRIGGER [CSC].[NHSEVOLUTION_user_admin_requests_DIU_trigger]
    ON [CSC].[NHSEVOLUTION_user_admin_requests]
    WITH ENCRYPTION
    AFTER INSERT, DELETE, UPDATE
    AS 
BEGIN
--The script body was encrypted and cannot be reproduced here.
    RETURN
END


GO
CREATE TRIGGER [CSC].[NHSEVOLUTION_user_admin_requests_IU_after_trigger]
    ON [CSC].[NHSEVOLUTION_user_admin_requests]
    WITH ENCRYPTION
    AFTER INSERT, UPDATE
    AS 
BEGIN
--The script body was encrypted and cannot be reproduced here.
    RETURN
END

