﻿CREATE ROLE [EvolutionRole]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Abailey';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Abaxter';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Acain';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Acain2';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Aclarke';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Acoyne';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Afieldhouse';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ahanford';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Aleahy';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Aleaper';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Algrundy';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Allcoca';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Alloyd';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Aosullivan';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Arashud';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Areid';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Armittt';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Aswilson';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Atininsey';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Atinsey';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Avisond';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Axone';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Azkarim';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Baileyh';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Baskinv';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Bcarson';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Becketta';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Bging';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Bmoreton';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Bmorris';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Boneil';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Bradbury';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Brightj';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Brocklm';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Brogers';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Browns';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Burkej';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Bwebster';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Carmstrong';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Castrol';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cburns';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ccash';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cdonaldson';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ceckersall';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cgralton';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Chkeating';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Chpearson';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ckong';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cleigh';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Clondon';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Clongrigg';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cmcgovern';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cmckay';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cmggovern';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cmurphy';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cnavin';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Corker';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Corpeul';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cpreston';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Csalamone';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Csanders';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscabarlow';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscaclark';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscadoyle';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscafinney';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscaharvey';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscahussain';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscamitchell';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscamitchell7';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscamogra';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscaverey';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscawebster';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscbleyland';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscbsonigra';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Csccschofield';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscdallison';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscdgood';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscdhall';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscdkelly';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscdpeters';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscdsmith';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscdstanbury';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscfjaffer';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscfthompson';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscgdelaney';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscgdewhirst';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscgleatham';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cschhoang';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscihogg';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscipatel';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscjdudley';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscjhalpin';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscjhawley';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscjlloyd';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscjmohammed';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscjturvey';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscjturvey2';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscjvoller';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscjwarrington';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Csckault';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscklawson';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscksiddons';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscksimpson';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Csclhall';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscmcrayston';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscmgee6';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscmhenderson';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscmjudge';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscmjudge2';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscmtucker';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscnwhitelaw';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscpblack';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscphewitson';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscpowen';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscprenshaw';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscprotospms';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscprotospms2';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscpsmith';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscrbent';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscrjenkins';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscrlonergan';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscrpimblett';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscsabel';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscsashmore';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscsdevine';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscsgregory';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscshagan';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscshowarth';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscskelly';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscsmgee6';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscsougrader';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscspillar';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscsward';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscswhitelaw';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Csctgorton';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscthanley';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cstanistreet';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cstoney';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cvenables';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cwadsworth';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Darlinc';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Daviess';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Dboardman';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Dbradbury';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Dcoombs';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ddreniw';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Demmett';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Dfosterleon';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Dgriffiths';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Dhussey';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Djames';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Dkeighran';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Dking';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Dlyons';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Dmokate';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Dnoblett';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Dpriestl';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Drobinson';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Drose';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Dtaylor';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Dturner';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Eigreen';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Englisl';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Eroles';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Eshaw';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Estilwell';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Aleigh';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Evercoe';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ewood';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Fdodd';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Fmccarthy';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Fmccrorie';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Frenshaw';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Fsalami';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Fwilliams';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Gardinm';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Gballance';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Gbriggs';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ghartley';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ghealy';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Gjardine';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Gmansell';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Goughk';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Grichardson';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Growley';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Gweinberg';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Harrisc';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Hatchmanc';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Hbeeston';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Hcarroll';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Hdavies';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Hgreenwood';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Higginr';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Hprince';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Hquayle';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Hslatter';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Hwilkinson';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ibashir';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Irvingm';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ispence';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jabennett';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jaldred';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jastaylor';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jblackhurst';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jblair';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jbrownbill';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jdavies';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jduckworth';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jekelly';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jerichardson';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jhare';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jharvey';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jhayton';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jhewkin';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jhickie';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jjennings';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jlittle';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jogrimshaw';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jonesb';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jperkin';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jpotluri';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jprytz';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jrisk';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jstorey';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Juevans';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Kashley';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Kbreadon';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Kedaley';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Kennedt';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Kgrady';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Kgrimshaw';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Khannon';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Khunt';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Kmakin';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Kmorton';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Kosekbm';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Kreillycooper';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Kroddis';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ksather';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ksmeaton';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Kwalker';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Kwall';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lacton';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lacyd';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lallen';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lancell';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lburcham';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lchappell';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lconnors';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lee';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Leeb';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Leigha';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lewiso';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lhope';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Llemmers';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lloydk';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lmcgleenan';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lmckinnon';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lrichardson';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lrose';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lserrage';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lshrieve';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lspruce';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lsptest';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ltravis';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ltyrell';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Magenim';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mbellman';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mbiddle';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mbrogan';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mburns';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mbwebster';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mcdonnj';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mcvicke';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mdavis';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mdavis1';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mdawson';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mfowler';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mhankinson';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Millwal';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mlowcock';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mmcgrath';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mpalmerhoyes';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mperry';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mquraishi';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mwhitby';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mwillis';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mworth';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Nbraude';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Nbraude1';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Nchapman';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Nharrison';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Nmwimbi';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Nthorburn';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Nuttall';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Nwatson';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ohill';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Pagemaa';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Palibone';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Pdumanshie';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Pflinders';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Phopkins';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Pkatiff';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Plloyd';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ppoizer';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Pryan';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Rallen';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Retaylor';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Riley-joycef';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Rlang';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Rlee';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Rmccarthy';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Rmeaney';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Rmoham';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Robertc';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Robinsoa';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Rogersj';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Sair';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Salonso';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Salonso2';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Sbatt';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Sblantern';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Sbrown';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Scarroll';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Schainey';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Sdecunha';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Sdeighan';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Serevitchj';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Seroberts';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Sfitzmaurice';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Shickey';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Shsmith';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Shyde';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Sjames';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Sjfitzmaurice';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Skarsa';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Slee';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Sleigh';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Sleighatkins';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Sleighton';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Slomas';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Smacfari';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Smaguire';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Smcauliffe';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Smoss';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Smulvey';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Spalin';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Spowell';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Spruce';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Sralphs';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Sramirez';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Sullivc';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Sumnerv';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Swheatly';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Swilliams';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Thomasm';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Tmoors';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Tpreston';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Vaggj';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Vallomp';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Vbrown';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Vcole';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Vibrown';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Vlevy';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Vmadden';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Webbrin';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Wenlee';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Wheelee';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Wilsons';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Worslej';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Yatesc';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Yingleby';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Youngg';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Zdomezweni';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'cscbfear';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Hbridgeman';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Fairclougha';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Biggsk';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Forbesm';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Akarim';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Kendean';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Bhatahet';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lcastro';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lhobbs';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Arobinson';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Tkennedy';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lbrewbut';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Rogersons';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Rsykes';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jpowell';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Rdelvin';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lcousins';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jtapster';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Dmatthews';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscnbaily';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Hmiller';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Tclarke';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ebarrington';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mainnk';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Etressie';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Fagbo';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Croberts';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cdarling';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ddrenwin';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Egreen';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ewheeler';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Gbalance';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jrogers';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Klloyd';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lbrewbutler';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lmillward';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mirving';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mfoster';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Nthornburn';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Swilson';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Vsummers';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Gloverd';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mthomas';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jbellis';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cwood';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Nwebbriley';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lwebber';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mbrocklebank';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cjohnson';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Rbarber';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Karena';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lcopues';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Hhennessy';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cyates';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lhunter';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mjohn';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Sowen';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jgillespie';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ypike';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jclarke';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lmcgregor';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Kaspinall';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Dcullen';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Dlaw';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Griffithsd';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Tgreenwood';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Nflood';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jmcphee';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Nbradbury';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cmme';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Dshinwell';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Nmoss';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Fallen';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cduffy';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mclough';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Dgarrod';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Rstephens';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Vparker';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Akaur';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lgoddard';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lplummer';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Miredale';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Gjohnson';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Gmartin';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cfootitt';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cowen';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Kgrzegor';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscpbeh';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Csctwalsh';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Csckjoyce';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Charrison';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lfinch';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jmellor';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mhayes';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jheeley';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Fslade';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lflatman';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Tayanlere';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cgarratt';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Joconnell';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Elambert';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Clangley';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mnockallscsc';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ghibbert';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Vbrown1';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Kshearman';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lwalsh';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Nobrien';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Iokonkwo';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cwai';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Amharris';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Pwesthead';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Krichards';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cwalsh';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Aelwell';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mheeran';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Gturnbull';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mgibbons';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jbent';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Rdevlin';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Hhawkins';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Shaynes';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Nbates';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lsmeaton';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cscjcox';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cbissell';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Sallen';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Ehwang';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Uhasmi';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Tquinn';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lcraig';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cneil';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Roatway';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Emcclure';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lwright';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cbresnahan';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jenhall';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Dbristow';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Sholland';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Lhulse';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cdronsfield';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Dferrario';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Cmee';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mclay';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Uhashmi';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Mwhyatt';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Abarwood';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Hhuntoon';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jmead';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jablair';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Jbrooks';


GO
EXECUTE sp_addrolemember @rolename = N'EvolutionRole', @membername = N'Rbrown';

