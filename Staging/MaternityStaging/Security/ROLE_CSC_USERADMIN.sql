﻿CREATE ROLE [ROLE_CSC_USERADMIN]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscabarlow';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscaclark';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscadoyle';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscafinney';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscaharvey';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscahussain';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscamitchell';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscamitchell7';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscamogra';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscaverey';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscawebster';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscbleyland';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscbsonigra';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Csccschofield';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscdallison';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscdgood';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscdhall';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscdkelly';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscdpeters';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscdsmith';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscdstanbury';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscfjaffer';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscfthompson';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscgdelaney';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscgdewhirst';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscgleatham';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cschhoang';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscihogg';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscipatel';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscjdudley';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscjhalpin';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscjhawley';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscjlloyd';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscjmohammed';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscjturvey';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscjturvey2';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscjvoller';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscjwarrington';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Csckault';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscklawson';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscksiddons';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscksimpson';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Csclhall';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscmcrayston';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscmgee6';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscmhenderson';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscmjudge';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscmjudge2';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscmtucker';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscnwhitelaw';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscpblack';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscphewitson';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscpowen';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscprenshaw';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscprotospms';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscprotospms2';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscpsmith';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscrbent';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscrjenkins';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscrlonergan';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscrpimblett';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscsabel';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscsashmore';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscsdevine';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscsgregory';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscshagan';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscshowarth';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscskelly';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscsmgee6';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscsougrader';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscspillar';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscsward';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscswhitelaw';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Csctgorton';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscthanley';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'cscbfear';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscnbaily';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscpbeh';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Csctwalsh';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Csckjoyce';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'EvolutionAdmin';


GO
EXECUTE sp_addrolemember @rolename = N'ROLE_CSC_USERADMIN', @membername = N'Cscjcox';

