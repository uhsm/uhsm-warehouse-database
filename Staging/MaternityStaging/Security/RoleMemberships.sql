﻿EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'interface';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'lspadmin';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'protosadmin';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CSCLSP\PERM-SQL-DPS-ReadOnly';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'EvolutionAdmin';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CSCLSP\zBOServ';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CSCLSP\ROLE-G-NHS-GTS_MessengingTeam';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'interface';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'lspadmin';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'protosadmin';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'EvolutionAdmin';

