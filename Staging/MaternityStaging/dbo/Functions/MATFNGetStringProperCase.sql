﻿

CREATE FUNCTION [dbo].[MATFNGetStringProperCase]
(
	@InputString VARCHAR(255)
)  
RETURNS VARCHAR(255) AS  
BEGIN  

	DECLARE @ReturnString	VARCHAR(255)  
	DECLARE @StringLength	INT  
	DECLARE @FinishedFlag	BIT  
	DECLARE @Position		INT  
	DECLARE @ThisChar		CHAR(1)  
	DECLARE @ThisAsc		INT  
	DECLARE @Capitalise		BIT  
	
	IF (@InputString is not null)
		BEGIN
	
			SET @FinishedFlag	= 0  
			SET @InputString	= LOWER(@InputString)  
			SET @ReturnString	= @InputString  
			SET @StringLength	= DATALENGTH(@ReturnString)  

			IF @StringLength = 0  
			BEGIN
				SET @FinishedFlag = 1  
			END
			SET @Position		= 0  
			SET @Capitalise		= 1  
			WHILE @FinishedFlag = 0  
			BEGIN  
				SET @Position = @Position + 1  
				IF @Position > @StringLength  
				BEGIN
					SET @FinishedFlag = 1  
				END
				ELSE  
					BEGIN  
					SET @ThisChar = SUBSTRING(@ReturnString, @Position, 1)  
					SET @ThisAsc = ASCII(@ThisChar)  
					IF @ThisChar IN (' ')  
						BEGIN  
						SET @ReturnString = LEFT(@ReturnString, @Position - 1) + ' ' + RIGHT(@ReturnString, @StringLength - @Position)  
						SET @Capitalise = 1  
						END  
					ELSE
						BEGIN  
						IF @Capitalise = 1 AND (@ThisAsc BETWEEN 97 AND 122)  
							BEGIN  
								SET @ThisAsc = @ThisAsc - 32  
								SET @ReturnString = LEFT(@ReturnString, @Position - 1) + CHAR(@ThisAsc) + RIGHT(@ReturnString, @StringLength - @Position)  
								SET @Capitalise = 0  
							END  
						END  
					END  
			END 
		END
	ELSE
		BEGIN
			SET @InputString='';
		END
RETURN @ReturnString  
END
