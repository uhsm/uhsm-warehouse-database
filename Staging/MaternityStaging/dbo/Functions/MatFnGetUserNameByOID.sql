﻿

CREATE FUNCTION [dbo].[MatFnGetUserNameByOID]
(
	@UserOID as BIGINT
)
RETURNS VARCHAR(100)
AS
BEGIN
DECLARE @Username VARCHAR(100)
IF(@UserOID IS NOT NULL)
	BEGIN
		IF EXISTS(SELECT * FROM Register WHERE Regtype = 'P' AND Pointer = @UserOID)
		BEGIN
			SELECT  @Username =
				CASE
					WHEN ISNULL(Us.Title,'')!='' THEN LTRIM (UPPER(Us.Surname) + ', ' + ISNULL(dbo.MATFNGetStringProperCase(Us.Forename),'') + ' ' +'('+ Us.Title +')')
				ELSE	
					LTRIM (UPPER(Us.Surname) + ', ' + ISNULL(dbo.MATFNGetStringProperCase(Us.Forename),'')) 
				END
			FROM
				Register Us
			WHERE 
				Us.Pointer = @UserOID
		END
		ELSE
		BEGIN
			SELECT  @Username = LTRIM(UPPER(Surname)) FROM Register WHERE Pointer = @UserOID
		END	
	END
ELSE
	BEGIN
		SET @Username=NULL
	END
RETURN (@Username)
END
