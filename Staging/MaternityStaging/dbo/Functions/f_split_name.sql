﻿
CREATE   FUNCTION dbo.f_split_name (@l_gp_name varchar(30), @type varchar)
RETURNS	VARCHAR(30)
AS
/***********************************************************************************
*	Name		:	f_split_name
*	Purpose		:	
*	Author		:	
*	Notes		:	
*	Version History	:	Date		Version	User	Comment		
*				14-NOV-2007	1.0	TDh	Created		
***********************************************************************************/
BEGIN

DECLARE @Firstname_Position 	int
DECLARE @position		smallint
DECLARE	@surname		varchar(30)
DECLARE	@forename		varchar(30)
DECLARE @len            	smallint
DECLARE @finalvalue		varchar(30)

set @finalvalue = ''

if ( charindex (' ', @l_gp_name) = 0 /* one word only ! */
   )
   begin
        set @forename = NULL
        set @surname = @l_gp_name
   end
else
   begin
        set @len = len(@l_gp_name)
        set @position = @len
/*
print 'debugline 0090010 @l_gp_name/len/@position' + @l_gp_name + str( len(@l_gp_name) ) + str(@position)
*/ 
        while @position > 0
           begin  --wh1{
/*
print 'debugline 0090020 substring(@l_gp_name,@position,1) ' + substring(@l_gp_name,@position,1)
*/
              if substring(@l_gp_name,@position,1) = ' '
                 begin  --wh2{
                 set @Firstname_Position=@position+1
                 break
                 end    --wh2{
              set @position = @position - 1
           end    --wh1}

        set @forename = substring(@l_gp_name,@Firstname_Position,@len)
        set @surname = rtrim(substring(@l_gp_name,1,@Firstname_Position-1))
end

	if @type = 'f'
		BEGIN
		 set @finalvalue = @forename
		END
		
	if @type = 's'
		BEGIN
		 set @finalvalue = @surname
		END

RETURN @finalvalue

END

