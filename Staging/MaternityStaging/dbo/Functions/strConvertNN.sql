﻿
CREATE FUNCTION strConvertNN
    (@strNoteNo char(19))
RETURNS char(19)
AS
BEGIN

DECLARE @strResult char(19)

    Set @strResult = dbo.strFormatNoteByte(@strNoteNo, 1) + dbo.strFormatNoteByte(@strNoteNo, 2) + dbo.strFormatNoteByte(@strNoteNo, 3) + dbo.strFormatNoteByte(@strNoteNo, 4) + dbo.strFormatNoteByte(@strNoteNo, 5)
    RETURN(@strResult)

END
