﻿
CREATE FUNCTION strCreateNN
    (@strRevNo varchar(5))
RETURNS varchar(5)
AS
BEGIN

DECLARE @strResult varchar(5)

    SET @strResult = REPLACE(@strRevNo,CHAR(1),'')

    SET @strResult = @strResult + REPLICATE(CHAR(1),5-LEN(@strResult))
    RETURN(@strResult)

END
