﻿
CREATE FUNCTION strFormatNoteByte
    (@strNoteNo char(5),
    @intNotePosit int)
    RETURNS varchar(4)
AS
BEGIN

    DECLARE @strReturn varchar(4)
    DECLARE @strNoteByte char(3)
    DECLARE @strNoteDot varchar(1)

    If (@intNotePosit > 1)
    Begin
        Set @strNoteDot = '.'
    End
    Else
    Begin
        Set @strNoteDot = ''
    End

    Set @strNoteByte = Cast(Ascii(Substring(@strNoteNo, @intNotePosit, 1)) As Char(3))

    Set @strReturn = RTrim(@strNoteDot + @strNoteByte)

Return (@strReturn)
End
