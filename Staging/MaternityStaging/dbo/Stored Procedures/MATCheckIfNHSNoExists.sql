﻿

CREATE PROCEDURE [dbo].[MATCheckIfNHSNoExists]
(
	@NHSNumber		VARCHAR(12),
	@ErrorNo		INT OUTPUT
)
AS
BEGIN
	SET @ErrorNo = 0
	
	IF EXISTS (SELECT TOP 1 Pointer FROM Notes WHERE Code = 'ZMnu1' AND RTRIM(LTRIM(Nvalue)) = @NHSNumber)
		SET @ErrorNo = 1	/* ----- NHS number exists */
	ELSE IF EXISTS (SELECT TOP 1 Pointer FROM Register WHERE Regtype = 'P' AND RTRIM(LTRIM(Number2)) = @NHSNumber)
		SET @ErrorNo = 1	/* ----- NHS number exists */
END
