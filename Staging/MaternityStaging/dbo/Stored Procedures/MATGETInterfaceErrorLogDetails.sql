﻿

CREATE PROCEDURE [dbo].[MATGETInterfaceErrorLogDetails]
(
@TriggerLogOID BIGINT,
@ErrorNo INT OUTPUT
)
AS
BEGIN
	SELECT	ErrorSummary, 
			ErrorDescription, 
			SuggestedAction, 
			CreatedAt, 
			dbo.MatFnGetUserNameByOID(CreatedBy) as CreatedBy, 
			CASE LEN(ISNULL(MessageID,''))   WHEN 0 THEN 'Not applicable'     ELSE MessageID END AS MessageID, 
			TriggerLogOID, 
			OID,
			ErrorID, 
			ErrorStatus,
			ErrorSource,
			Direction,
			EventType,
			dbo.MatFnGetUserNameByOID(ModifiedBy) as ModifiedBy,			
			TransactionXML,
			Workstation,
			Location
		FROM 
			InterfaceErrorLog A
		WHERE
			A.TriggerLogOID= @TriggerLogOID
		ORDER BY ModifiedAt DESC
END
