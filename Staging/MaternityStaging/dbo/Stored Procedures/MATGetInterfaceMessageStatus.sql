﻿

CREATE PROCEDURE [dbo].[MATGetInterfaceMessageStatus]
(        
   @PatientOID   BIGINT  = 0,        
   @UserOID    BIGINT  = 0,        
   @EventType   VARCHAR(50) = NULL,        
   @FromDate   VARCHAR(20) = '01/01/1900',        
   @ToDate    VARCHAR(20) = '31/12/2050',       
   @NoOfrecords   INT   = 100,        
   @MessageID   VARCHAR(50) = NULL,  
   @ErrorStatus   CHAR(1)  = NULL,  
   @OrderBy    INT   = 1,   -- 1 = CreatedAt; 2 = PatientName; 3 = UserName; 4 = EventType; 5 = TransactionStatus;  
   @ErrorNo    INT   OUTPUT        
         
)        
AS        
BEGIN        

 IF (@ToDate <> '')
	SET @ToDate = @ToDate + ' 23:59'	

 IF (@EventType='')  
 BEGIN  
 SET @EventType=NULL  
 END  
   
 IF (@MessageID='')  
 BEGIN  
 SET @MessageID=NULL  
 END  
   
 IF (@ErrorStatus='')  
 BEGIN  
 SET @ErrorStatus=NULL  
 END  
   
 SET @NoOfrecords  = ISNULL(@NoOfrecords,100)
   
      
 SELECT          
  TOP(SELECT @NoOfrecords)      
  dbo.MatFnGetUserNameByOID(P.Pointer) AS 'Patient Name',
  P.Number1			AS 'Patient HOSPNo',        
  A.OID as TriggerLogOID,       
  B.OID,   
  A.PatientPointer,        
  A.PregnancyNo,        
  A.BabyOrder,        
  A.EventType,     
  dbo.MatFnGetUserNameByOID(B.CreatedBy) as ProcessedBy, 
  B.CreatedAt as ProcessedDate,        
  A.TransactionStatus,         
  B.ErrorID,        
  B.ErrorDescription,        
  B.ErrorStatus,        
  B.ErrorSource,        
  B.Direction,        
  B.MessageID,        
  B.Priority,        
  B.ErrorSummary,        
  B.SuggestedAction,  
  dbo.MatFnGetUserNameByOID(B.ModifiedBy) as ModifiedBy
 FROM  
  InterfaceTriggerLog  A WITH (NOLOCK)      
  LEFT JOIN  InterfaceErrorLog     B WITH (NOLOCK)  ON A.OID   = B.TriggerLogOID 
  AND B.OID=(SELECT Top 1 OID from InterfaceErrorLog C where C.TriggerLogOID=A.OID  Order by OID desc)         
  LEFT JOIN  Register  P WITH (NOLOCK)  ON P.Pointer   = B.PatientPointer                
 -- APPLY WHERE CLAUSE BASED ON PARAMETER VALUE  
 WHERE      
  (ISNULL(@PatientOID,0) = 0 OR B.PatientPointer = @PatientOID )        
  AND (ISNULL(@UserOID,0)= 0 OR B.ModifiedBy  = @UserOID )        
  AND (@EventType  IS NULL OR A.EventType = @EventType )        
  AND (@FromDate  IS NULL OR CONVERT(DATETIME,LTRIM(RTRIM(B.CreatedAt)),104)       
          BETWEEN CONVERT(DATETIME,LTRIM(RTRIM(@FromDate )),104) AND CONVERT(DATETIME,LTRIM(RTRIM(@ToDate )),104))    
  AND (@ErrorStatus IS NULL OR B.ErrorStatus = @ErrorStatus)  
  AND B.Status='A'              
 -- APPLY ORDERBY CLAUSE BASED ON @OrderBy           
 ORDER BY       
  CASE @OrderBy  WHEN 1 THEN  B.CreatedAt End DESC,  
  CASE @OrderBy  WHEN 2 THEN  P.Surname + ' ,' +  P.Forename END ASC,  
  CASE @OrderBy  WHEN 3 THEN  B.ModifiedBy END ASC,  
  CASE @OrderBy  WHEN 4 THEN  A.EventType END ASC,  
  CASE @OrderBy  WHEN 5 THEN  B.ErrorStatus  End ASC  
         
 IF @@ROWCOUNT = 0        
 BEGIN        
  SET @ErrorNo = -1         
 END        
END
