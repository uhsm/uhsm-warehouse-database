﻿

CREATE PROCEDURE [dbo].[MATGetPDSAuditByMessageID]
(
	@MessageID		VARCHAR(50),
	@FetchDataFlag	CHAR(1), -- 0 : Get data to update, 1 : get the spine PDS response
	@ErrorNo		INT OUTPUT
)
AS
BEGIN
	SET	@ErrorNo = 0
	
	IF(@FetchDataFlag = '0')
	BEGIN
		SELECT TOP 1 ITL.OID,ITL.PatientPointer,ITL.PregnancyNo,ITL.BabyOrder,ITL.MessageID,IEL.ErrorDescription,IEL.ModifiedBy  
		FROM InterfaceTriggerLog ITL JOIN InterfaceErrorLog IEL ON IEL.TriggerLogOID = ITL.OID 
		WHERE ITL.MessageID  = @MessageID
	END
	ELSE
	BEGIN
		SELECT TOP 1 ITL.OID,ITL.PatientPointer,ITL.PregnancyNo,ITL.BabyOrder,ITL.MessageID,IEL.CMATXML as ErrorDescription,IEL.ModifiedBy
		FROM InterfaceTriggerLog ITL JOIN InterfaceErrorLog IEL ON IEL.TriggerLogOID = ITL.OID 
		WHERE ITL.MessageID  = @MessageID AND IEL.PDSMessageStatus = 'Spine Response'
	END
			
END
