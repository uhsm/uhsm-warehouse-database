﻿

CREATE PROCEDURE [dbo].[MATInsertPDSTriggerLogData]
(
	@MessageID			VARCHAR(50),
	@PatientPointer		BIGINT,  
	@PregnancyNo		BIGINT,  
	@BabyOrder			BIGINT, 
	@EventType			VARCHAR(50),	-- NN4B,MothersDischarge,BabysDischarge,KornerReturn,PDS etc..
	@ProcessedBy		BIGINT,
	@ProcessedDate		DATETIME,
	@TransactionStatus	CHAR(1),		--Transaction status:  Processed - 1 ,Yet to be Processed – 0
	@MessageSummary		VARCHAR(500),
	@MessgaeDescription VARCHAR(5000),
	@Direction			VARCHAR(50),
	@ErrorStatus		VARCHAR(50),
	@SourceID			BIGINT,
	@CMATXML			VARCHAR(MAX),	-- Contains transformed MIM XML format
	@PDSMessageStatus	VARCHAR(100),	--Send/Acknownledge/Timeout/Invalid NHS number/duplicate NHS number
	@ErrorID			VARCHAR(50),	-- 1 - Sending packet ;2 -	Received MHS acknowledgement;3 - Timeout;4 - Received spine MIM response;5 - Successful;6 - Invalid NHS number;7 - Duplicate NHS number;8 - Potential match;9 - Potential match - YES;10 - Potential match - NO;11 - MCCI_IN010000UK13;12 - Exact match failer;13 - Exception
	@TransactionXML		VARCHAR(MAX),	-- Contains Packet XML & MIM response
	@Workstation		VARCHAR(50),
	@Location			VARCHAR(500),
	@ErrorNo			INT OUTPUT
)
AS
BEGIN

	DECLARE @InterfaceTriggerLogOID AS BIGINT
	
	IF NOT EXISTS( SELECT 1 FROM InterfaceTriggerLog WHERE MessageID = @MessageID)
	BEGIN
		INSERT INTO InterfaceTriggerLog
		(
		PatientPointer,PregnancyNo,BabyOrder,EventType,ProcessedBy,ProcessedDate,TransactionStatus,
		CreatedBy,CreatedAt,ModifiedBy,ModifiedAt,Status,MessageID
		)
		VALUES
		(
		@PatientPointer,@PregnancyNo,@BabyOrder,@EventType,@ProcessedBy,@ProcessedDate,@TransactionStatus,
		@ProcessedBy,GETDATE(),@ProcessedBy,GETDATE(),'A',@MessageID
		)
		
		SET @InterfaceTriggerLogOID  = SCOPE_IDENTITY();

		INSERT INTO InterfaceErrorLog
		(
		PatientPointer,ErrorSummary,ErrorDescription,ErrorStatus,EventType,TriggerLogOID,Direction,
		CreatedBy,CreatedAt,ModifiedBy,ModifiedAt,Status,MessageID,SourceOID,CMATXML,PDSMessageStatus,ErrorID,TransactionXML,
		Workstation,Location
		)
		VALUES
		(
		@PatientPointer,@MessageSummary,@MessgaeDescription,@ErrorStatus,@EventType,@InterfaceTriggerLogOID,'Outbound',
		@ProcessedBy,GETDATE(),@ProcessedBy,GETDATE(),'A',@MessageID,@SourceID,@CMATXML,@PDSMessageStatus,@ErrorID,@TransactionXML,
		@Workstation,@Location
		)
	END
	ELSE
	BEGIN
		SELECT TOP 1 @InterfaceTriggerLogOID = OID from InterfaceTriggerLog where MessageID = @MessageID
			
		IF(@InterfaceTriggerLogOID IS NOT NULL)
		BEGIN
			INSERT INTO InterfaceErrorLog
			(
			PatientPointer,ErrorSummary,ErrorDescription,ErrorStatus,EventType,TriggerLogOID,Direction,
			CreatedBy,CreatedAt,ModifiedBy,ModifiedAt,Status,MessageID,SourceOID,CMATXML,PDSMessageStatus,ErrorID,TransactionXML,
			Workstation,Location
			)
			VALUES
			(
			@PatientPointer,@MessageSummary,@MessgaeDescription,@ErrorStatus,@EventType,@InterfaceTriggerLogOID,'Inbound',
			@ProcessedBy,GETDATE(),@ProcessedBy,GETDATE(),'A',@MessageID,@SourceID,@CMATXML,@PDSMessageStatus,@ErrorID,@TransactionXML,
			@Workstation,@Location
			)
		END
	END
			
END
