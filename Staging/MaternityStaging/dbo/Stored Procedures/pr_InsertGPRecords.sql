﻿
/* Create stored procedure */

CREATE PROC pr_InsertGPRecords
AS
BEGIN

DECLARE gp_cursor CURSOR
FOR
	SELECT	[Organisation Code],
		[Parent Organisation Code],
		substring([Organisation Name],1,25),
		[Open Date],		[Address Line 1],
		[Address Line 2],
		[Address Line 3],
		[Address Line 4],
		[Postcode],
		[Contact Telephone Number]
	FROM	protos_gp_information_i	pgii
	WHERE	NOT EXISTS
	(	SELECT	Number1	
		FROM	Register	r
		WHERE	r.[Number1] = pgii.[Organisation Code]
			COLLATE Latin1_General_CS_AI
	)
	ORDER BY [Organisation Code]

DECLARE	@register_pointer	int
DECLARE @G_pointer		int
DECLARE @s_pointer		int
DECLARE @stafflink_linkid	int

DECLARE @l_gp_code		varchar(25)
DECLARE @l_gp_practice		varchar(25)
DECLARE @l_gp_name		varchar(25)
DECLARE @l_open_date		datetime
DECLARE @l_address1		varchar(35)
DECLARE @l_address2		varchar(35)
DECLARE @l_address3		varchar(35)
DECLARE @l_address4		varchar(35)
DECLARE @l_postcode		varchar(8)
DECLARE @l_phone		varchar(12)

DECLARE @l_practice_open_date	datetime
DECLARE @l_practice_address1	varchar(35)
DECLARE @l_practice_address2	varchar(35)
DECLARE @l_practice_address3	varchar(35)
DECLARE @l_practice_address4	varchar(35)
DECLARE @l_practice_postcode	varchar(8)
DECLARE @l_practice_phone	varchar(12)

DECLARE @l_PCG_code		varchar(6)
DECLARE @Firstname_Position 	int
DECLARE	@surname		varchar(30)
DECLARE	@forename		varchar(30)


--1. 
SELECT	@register_pointer = MAX(Pointer) 
FROM	Register
	IF @register_pointer IS NULL
	SET @register_pointer = 0

--2.
SELECT	@stafflink_linkid = MAX(Linkid)
FROM	Stafflink
	IF @stafflink_linkid IS NULL
	SET @stafflink_linkid =0

--3 For each new gp record …

OPEN gp_cursor
FETCH NEXT FROM gp_cursor INTO @l_gp_code,@l_gp_practice ,@l_gp_name,@l_open_date,
	@l_address1, @l_address2, @l_address3, @l_address4, @l_postcode, @l_phone

WHILE (@@FETCH_STATUS = 0)
BEGIN
--3.1 Lookup the practice details ...
	SELECT	@l_PCG_code = MAX([Parent Organisation Code]),
		@l_practice_open_date = MAX([Open Date]),
		@l_practice_address1 = MAX([Address Line 1]),
		@l_practice_address2 = MAX([Address Line 2]),
		@l_practice_address3 = MAX([Address Line 3]),
		@l_practice_address4 = MAX([Address Line 4]),
		@l_practice_postcode = MAX([Postcode]),
		@l_practice_phone = MAX([Contact Telephone Number])
	FROM	protos_practice_information_i
	WHERE	[Organisation Code] = @l_gp_practice

--3.2 Add the next record to the Register with the GP code and other details
	SET @register_pointer = @register_pointer + 1
	SET @G_pointer = @register_pointer


set @forename = substring(dbo.f_split_name (@l_gp_name, 'f'),1, 25) 
set @surname = substring(dbo.f_split_name (@l_gp_name, 's'),1, 25)

	INSERT INTO Register
		(Regtype, Status, Surname, Forename, Forename2,
		Title, Sex, Birthdate,
		Pointer,
		Address1, Address2, Address3, Address4,
		Postcode, Phone,
		Number1, Number2, Number3,
		Dateadded, Datemodified,
		Semaphore, Previous,
		Extra)
	VALUES
		('G','A', @surname, @forename,'',
		'Dr', '', convert(Char(8),@l_open_date,112),
		@G_pointer,
		@l_address1, @l_address2, @l_address3, @l_address4,
		@l_postcode, @l_phone,
		@l_gp_code, @l_gp_code, @l_gp_practice,
		convert(Char(8),getdate(),112), convert(Char(8),getdate(),112),
		'','',
		'~GP~~~'+rtrim(@l_PCG_code)+'~~~~~')

--3.3 Lookup Register record where [Regtype] =’s’, [Number2] = practice code and remember Pointer found.
	SET	@s_pointer = 0
	SELECT	@s_pointer = Pointer
	FROM	Register
	WHERE	Regtype	= 's'
	AND	Status	= 'A'
	AND	Number2	= @l_gp_practice

--If record NOT found then add the next record to the Register with the practice code and other details
	IF (@s_pointer = 0)
	BEGIN
		SET @register_pointer = @register_pointer + 1
		SET @s_pointer = @register_pointer

		INSERT INTO Register
			(Regtype, Status, Surname, Forename, Forename2,
			Title, Sex, Birthdate,
			Pointer,
			Address1, Address2, Address3, Address4,
			Postcode, Phone,
			Number1, Number2, Number3,
			Dateadded, Datemodified,
			Semaphore, Previous,
			Extra)
		VALUES
			('s','A', '', '', '',
			'', '', convert(Char(8),@l_practice_open_date,112),
			@s_pointer,
			@l_practice_address1, @l_practice_address2,
			@l_practice_address3, @l_practice_address4,
			@l_practice_postcode, @l_practice_phone,
			@l_gp_practice+'1',@l_gp_practice,'1',
			convert(Char(8),getdate(),112), convert(Char(8),getdate(),112),
			'','','')
		
	END

	SET @stafflink_linkid = @stafflink_linkid + 1

--3.4 Add the next record to the StaffLink with …
	INSERT INTO Stafflink
		(Linkid,Staffpointer,Organisation,
		Pascode)
	VALUES
		(@stafflink_linkid,@G_pointer,@s_pointer,
		rtrim(@l_gp_code)+'.'+rtrim(@l_gp_practice)+'.1')

	FETCH NEXT FROM gp_cursor INTO @l_gp_code,@l_gp_practice ,@l_gp_name,@l_open_date,
	@l_address1, @l_address2, @l_address3, @l_address4, @l_postcode, @l_phone
END

CLOSE gp_cursor
DEALLOCATE gp_cursor

END

