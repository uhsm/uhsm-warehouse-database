﻿
/* Create stored procedure */

CREATE PROC pr_InsertPracticeNoChildRecords
AS
BEGIN

DECLARE practice_cursor CURSOR
FOR
	SELECT	DISTINCT  -- make sure no duplicates creep in ...
		[Organisation Code],
		[Open Date],
		[Address Line 1],
		[Address Line 2],
		[Address Line 3],
		[Address Line 4],
		[Postcode],
		[Contact Telephone Number]
	FROM	protos_practice_information_i	ppii
	WHERE	NOT EXISTS
	(	SELECT	Number2
		FROM	Register	r
		WHERE	r.[Number2] = ppii.[Organisation Code]
			COLLATE Latin1_General_CS_AI
	)
	ORDER BY [Organisation Code]

DECLARE	@register_pointer	int
DECLARE @G_pointer		int
DECLARE @s_pointer		int

DECLARE @l_practice_code	varchar(25)
DECLARE @l_practice_open_date	datetime
DECLARE @l_practice_address1	varchar(35)
DECLARE @l_practice_address2	varchar(35)
DECLARE @l_practice_address3	varchar(35)
DECLARE @l_practice_address4	varchar(35)
DECLARE @l_practice_postcode	varchar(8)
DECLARE @l_practice_phone	varchar(12)
DECLARE @l_PCG_code		varchar(6)

-- 1.0
SELECT	@register_pointer = MAX(Pointer) 
FROM	Register
	IF @register_pointer IS NULL
	SET @register_pointer = 0

-- 2.0 
-- Childless Practices may exist. Load these for completeness and
-- because they may in future have GP's assigned (and therefore, 
-- the update script will need to have the Practice available in
-- Evolution)

-- 2.1 Open the Practice cursor and fetch the first record
OPEN practice_cursor
FETCH NEXT FROM practice_cursor INTO @l_practice_code, @l_practice_open_date,
	@l_practice_address1, @l_practice_address2, @l_practice_address3,
	@l_practice_address4, @l_practice_postcode, @l_practice_phone

WHILE (@@FETCH_STATUS = 0)
BEGIN

-- 2.2 Increment the counter
	SET @register_pointer = @register_pointer + 1
	SET @s_pointer = @register_pointer

-- 2.3 Insert the Practice record
	INSERT INTO Register
		(Regtype, Status, Surname, Forename, Forename2,
		Title, Sex, Birthdate,
		Pointer,
		Address1, Address2, Address3, Address4,
		Postcode, Phone,
		Number1, Number2, Number3,
		Dateadded, Datemodified,
		Semaphore, Previous,
		Extra)
	VALUES
		('s','A', '','','',
		'', '', convert(Char(8),@l_practice_open_date,112),
		@s_pointer,
		@l_practice_address1, @l_practice_address2,
		@l_practice_address3, @l_practice_address4,
		@l_practice_postcode, @l_practice_phone,
		@l_practice_code+'1',@l_practice_code,'1',
		convert(Char(8),getdate(),112), convert(Char(8),getdate(),112),
		'','','')
	
	FETCH NEXT FROM practice_cursor INTO @l_practice_code, @l_practice_open_date,
		@l_practice_address1, @l_practice_address2, @l_practice_address3,
		@l_practice_address4, @l_practice_postcode, @l_practice_phone

END

CLOSE practice_cursor
DEALLOCATE practice_cursor

END

