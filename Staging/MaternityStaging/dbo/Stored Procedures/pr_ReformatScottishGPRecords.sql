﻿
/* Create stored procedure */

CREATE PROC pr_ReformatScottishGPRecords
AS
BEGIN

-- The ...information... tables are the data source tables for
-- the true protos tables. Hence the NACS csv data is inserted
-- into the ...information... tables.

BEGIN tran

/*
* Insert the Scotland practice data into the main interface table :-
*/

INSERT INTO protos_practice_information_i
(
[Organisation Code],
[Organisation Name],
[RO/IT Cluster code],
[SHA code],
[Address Line 1],
[Address Line 2],
[Address Line 3],
[Address Line 4],
[Address Line 5],
[Postcode],
[Open Date],
[Close Date],
[Status Code],
[Organisation Sub-Type code],
[Parent Organisation Code],
[Join Parent Date],
[Left Parent Date],
[Contact Telephone Number],
[Contact Name],
[Address Type],
[Field 0],
[Amended record indicator],
[Wave Number],
[Current Care Organisation Code],
[Available for future use],
[Practice Type],
[Available for future use1] 
)
(
SELECT
substring(psii.[Text String],2,6),		-- [Organisation Code],
max(substring(psii.[Text String],8,8)),		-- [Organisation Name],
max('S00'),					-- [RO/IT Cluster code],
max('X98'),					-- [SHA code],
max(substring(psii.[Text String],16,25)),	-- [Address Line 1],
max(substring(psii.[Text String],41,25)),	-- [Address Line 2],
max(substring(psii.[Text String],66,25)),	-- [Address Line 3],
max(substring(psii.[Text String],91,25)),	-- [Address Line 4],
null,						-- [Address Line 5],
max(substring(psii.[Text String],116,8)),	-- [Postcode],
CASE
  WHEN substring(max(psii.[Text String]),124,8) = '00000000' THEN '19000101'
  ELSE substring(max(psii.[Text String]),124,8)
END,						-- [Open Date],
CASE
  WHEN substring(max(psii.[Text String]),132,8) = '00000000' THEN Null
  ELSE substring(max(psii.[Text String]),132,8)
END,						-- [Close Date],
max('A'),					-- [Status Code],
null,						-- [Organisation Sub-Type code],
max(substring(psii.[Text String],140,5)),	-- [Parent Organisation Code],
null,						-- [Join Parent Date],
null,						-- [Left Parent Date],
max(substring(psii.[Text String],154,11)),	-- [Contact Telephone Number],
null,						-- [Contact Name],
null,						-- [Address Type],
null,						-- [Field 0],
null,						-- [Amended record indicator ],
null,						-- [Wave Number],
null,						-- [Current Care Organisation Code],
null,						-- [Available for future use],
null,						-- [Practice Type],
null						-- [Available for future use1] 
FROM	protos_scotland_information_i 	psii
WHERE	cast(substring(psii.[Text String],1,1) as int) = 4
GROUP BY substring(psii.[Text String],2,6)
)

/*
* Insert the Scotland gp data into the main interface table :-
*/

INSERT INTO protos_gp_information_i
(
[Organisation Code],
[Organisation Name],
[RO/IT Cluster code],
[SHA code],
[Address Line 1],
[Address Line 2],
[Address Line 3],
[Address Line 4],
[Address Line 5],
[Postcode],
[Open Date],
[Close Date],
[Status Code],
[Organisation Sub-Type code],
[Parent Organisation Code],
[Join Parent Date],
[Left Parent Date],
[Contact Telephone Number],
[Contact Name],
[Address Type],
[Field 0],
[Amended record indicator],
[Wave Number],
[Current Care Organisation Code],
[Available for future use],
[Available for future use1],
[Available for future use2]
)
(
SELECT
substring(psii_gp.[Text String],2,8),		-- [GP Code],
max(substring(psii_gp.[Text String],10,30)),	-- [GP Name],
max('S00'),					-- [RO/IT Cluster code],
max('X98'),					-- [SHA code],
max(substring(psii_gp.[Text String],40,25)),	-- [Address Line 1],
max(substring(psii_gp.[Text String],65,25)),	-- [Address Line 2],
max(substring(psii_gp.[Text String],90,25)),	-- [Address Line 3],
max(substring(psii_gp.[Text String],115,25)),	-- [Address Line 4],
null,						-- [Address Line 5],
max(substring(psii_gp.[Text String],140,8)),	-- [Postcode],
max(substring(psii_gp.[Text String],154,8)),	-- [Open Date],
CASE
  WHEN substring(max(psii_gp.[Text String]),162,8) = '00000000' THEN Null
  ELSE substring(max(psii_gp.[Text String]),162,8)
END,						-- [Close Date],
max('A'),					-- [Status Code],
null,
max(substring(psii_gp.[Text String],148,6)),	-- [Parent Organisation Code ],
max(substring(psii_gp.[Text String],154,8)),	-- [Join Parent Date ],
CASE
  WHEN substring(max(psii_gp.[Text String]),162,8) = '00000000' THEN Null
  ELSE substring(max(psii_gp.[Text String]),162,8)
END,			-- [Left Parent Date ],
max(substring(psii_practice.[Text String],154,11)),
						-- [Contact Telephone Number ],
null,
null,
null,
null,
null,
null,
null,
null,
null
FROM	protos_scotland_information_i		psii_gp
LEFT OUTER JOIN	protos_scotland_information_i	psii_practice
	ON	substring(psii_gp.[Text String],148,6)	
		-- psii_gp.[Parent Organisation Code]
		=
		substring(psii_practice.[Text String],2,6)
		-- psii_practice.[Practice Code]
WHERE	cast(substring(psii_gp.[Text String],1,1) as int) = 5
AND	cast(substring(psii_practice.[Text String],1,1) as int) = 4
GROUP BY substring(psii_gp.[Text String],2,8)
)

commit tran

END

