﻿
/* Create stored procedure */

CREATE PROC pr_UpdateGPRecords
AS
BEGIN

BEGIN tran

DECLARE @l_gp_name varchar(30)

UPDATE	Register
SET	
	Surname		=	substring(dbo.f_split_name (pgii.[Organisation Name], 's'),1, 25),
	Forename	=	substring(dbo.f_split_name (pgii.[Organisation Name], 'f'),1, 25),
	-- forename2	=	this is not set
	-- title	=	is defaulted to 'Dr'
	-- Sex		=	this is not set
	Birthdate	=	pgii.[Open Date],
	Address1	=	pgii.[Address Line 1],
	Address2	=	pgii.[Address Line 2],
	Address3	=	pgii.[Address Line 3],
	Address4	=	pgii.[Address Line 4],
	-- address5	=	this is not set
	Postcode	=	pgii.[Postcode],
	Phone		=	pgii.[Contact Telephone Number],
	Number1		=	pgii.[Organisation Code],
	Number2		=	pgii.[Organisation Code],
	Number3		=	pgii.[Parent Organisation Code],
	Datemodified	=	convert(Char(8),getdate(),112),
	-- semaphore	=	this is not set
	-- previous	=	this is not set
	Extra		=	'~GP~~~'+rtrim(ppii.[Parent Organisation Code])+'~~~~~'
FROM	Register			r
JOIN	protos_gp_information_i		pgii
	ON r.Number2	=	pgii.[Organisation Code]
LEFT OUTER JOIN	protos_practice_information_i	ppii
	ON pgii.[Parent Organisation Code] = ppii.[Organisation Code]
WHERE	r.Regtype = 'G'
AND
	isnull(rtrim(r.Surname),'-') +
	isnull(rtrim(r.Forename),'-') +
	isnull(rtrim(convert(nvarchar(8),r.Birthdate)),'-') +
	isnull(rtrim(r.Address1),'-') +
	isnull(rtrim(r.Address2),'-') +
	isnull(rtrim(r.Address3),'-') +
	isnull(rtrim(r.Address4),'-') +
	isnull(rtrim(r.Postcode),'-') +
	isnull(rtrim(r.Phone),'-') +
	isnull(rtrim(r.Number1),'-') +
	isnull(rtrim(r.Number2),'-') +
	isnull(rtrim(r.Number3),'-') +
	--isnull(r.datemodified,'-') +
	isnull(rtrim(r.Extra),'-')
!=
	isnull(substring(dbo.f_split_name (pgii.[Organisation Name], 's'),1, 25), '-') +
	isnull(substring(dbo.f_split_name (pgii.[Organisation Name], 'f'),1, 25), '-') +
	isnull(rtrim(pgii.[Open Date]),'-') +
	isnull(rtrim(pgii.[Address Line 1]),'-') +
	isnull(rtrim(pgii.[Address Line 2]),'-') +
	isnull(rtrim(pgii.[Address Line 3]),'-') +
	isnull(rtrim(pgii.[Address Line 4]),'-') +
	isnull(rtrim(pgii.[Postcode]),'-') +
	isnull(rtrim(pgii.[Contact Telephone Number]),'-') +
	isnull(rtrim(pgii.[Organisation Code]),'-') +
	isnull(rtrim(pgii.[Organisation Code]),'-') +
	isnull(rtrim(pgii.[Parent Organisation Code]),'-') +
	--isnull(convert(Char(8),getdate(),112),'-') + 
	isnull('~GP~~~'+rtrim(ppii.[Parent Organisation Code])+'~~~~~','-')

-- Update Evolutions's practice information

UPDATE	Register
SET	
	-- Surname	=	this is not set
	-- forename	=	this is not set
	-- forename2	=	this is not set
	-- title	=	is defaulted to 'Dr'
	-- Sex		=	this is not set
	Birthdate	=	ppii.[Open Date],
	Address1	=	ppii.[Address Line 1],
	Address2	=	ppii.[Address Line 2],
	Address3	=	ppii.[Address Line 3],
	Address4	=	ppii.[Address Line 4],
	-- address5	=	this is not set
	Postcode	=	ppii.[Postcode],
	Phone		=	ppii.[Contact Telephone Number],
	Number1		=	ppii.[Organisation Code]+'1',
	Number2		=	ppii.[Organisation Code],
	Number3		=	'1',
	Datemodified	=	convert(Char(8),getdate(),112)
	-- semaphore	=	this is not set
	-- previous	=	this is not set
	-- Extra	=	this is not set
FROM	Register			r
JOIN	protos_practice_information_i	ppii
	ON r.Number2	=	ppii.[Organisation Code]
WHERE	r.Regtype = 's'
AND
	isnull(rtrim(convert(nvarchar(8),r.Birthdate)),'-') +
	isnull(rtrim(r.Address1),'-') +
	isnull(rtrim(r.Address2),'-') +
	isnull(rtrim(r.Address3),'-') +
	isnull(rtrim(r.Address4),'-') +
	isnull(rtrim(r.Postcode),'-') +
	isnull(rtrim(r.Phone),'-') +
	isnull(rtrim(r.Number1),'-') +
	isnull(rtrim(r.Number2),'-') +
	isnull(rtrim(r.Number3),'-')
!=
	isnull(rtrim(ppii.[Open Date]),'-') +
	isnull(rtrim(ppii.[Address Line 1]),'-') +
	isnull(rtrim(ppii.[Address Line 2]),'-') +
	isnull(rtrim(ppii.[Address Line 3]),'-') +
	isnull(rtrim(ppii.[Address Line 4]),'-') +
	isnull(rtrim(ppii.[Postcode]),'-') +
	isnull(rtrim(ppii.[Contact Telephone Number]),'-') +
	isnull(rtrim(ppii.[Organisation Code])+'1','-') +
	isnull(rtrim(ppii.[Organisation Code]),'-') +
	'1'

-- Update Evolutions's stafflink information

UPDATE	Stafflink
SET	Pascode	= rtrim(substring(rtrim(r_gp.[Number1]) + '.' + rtrim(r_gp.Number3) + '.1',1,20)),
	Organisation = r_practice.Pointer
FROM	Stafflink	s
JOIN	Register	r_gp
	ON s.Staffpointer = r_gp.Pointer
JOIN	Register	r_practice
	ON r_gp.Number3 = r_practice.Number2
WHERE	r_gp.Regtype		=	'G'
AND	r_practice.Regtype	=	's'
AND	rtrim(s.Pascode)	!=	rtrim(r_gp.[Number1]) + '.' + rtrim(r_gp.Number3) + '.1'

commit tran

END

