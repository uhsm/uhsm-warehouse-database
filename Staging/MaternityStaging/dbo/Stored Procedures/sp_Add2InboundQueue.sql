﻿

--########################################################
--STORED PROCEDURES
--########################################################
CREATE PROCEDURE sp_Add2InboundQueue
@DestID char(10),
@TransData text
AS
BEGIN
    Insert Into Queue(DestID, Direction,  InsertDate, TransData)
    Values(@DestID, 1, getDate(), @TransData)
END
