﻿

CREATE PROCEDURE sp_Add2OutboundQueue
@DestID char(10),
@TransData text
AS
BEGIN
    Insert Into Queue(DestID, Direction,  InsertDate, TransData)
    Values(@DestID, 3, getDate(), @TransData)
END
