﻿CREATE TABLE [dbo].[Action] (
    [ProcessID]  INT            NOT NULL,
    [ParentSeq]  INT            NULL,
    [Sequence]   INT            NULL,
    [Level]      INT            NULL,
    [CommandID]  INT            NULL,
    [SysError]   INT            NULL,
    [ErrorTable] INT            NULL,
    [Revnoteno]  CHAR (5)       COLLATE Latin1_General_BIN NULL,
    [Message]    VARCHAR (2000) NULL
) ON [AuditTrail];


GO
CREATE NONCLUSTERED INDEX [KeyAction]
    ON [dbo].[Action]([ProcessID] ASC)
    ON [AuditTrail];

