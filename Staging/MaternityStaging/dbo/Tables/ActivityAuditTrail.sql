﻿CREATE TABLE [dbo].[ActivityAuditTrail] (
    [SessionID]        INT       NOT NULL,
    [SequenceID]       INT       NOT NULL,
    [UserPointer]      INT       NOT NULL,
    [DateTime]         DATETIME  CONSTRAINT [DF_ActivityAuditTrail_DateTime] DEFAULT (getdate()) NOT NULL,
    [EventID]          INT       NOT NULL,
    [PatientSessionID] INT       NULL,
    [PatientPointer]   INT       NULL,
    [PatientName]      CHAR (25) COLLATE Latin1_General_CI_AS NULL,
    [Data]             TEXT      COLLATE Latin1_General_CI_AS NULL,
    [Machine]          CHAR (25) COLLATE Latin1_General_CI_AS NULL,
    CONSTRAINT [idx_Primary_ActivityAuditTrail] PRIMARY KEY CLUSTERED ([SequenceID] ASC, [SessionID] ASC) WITH (FILLFACTOR = 90) ON [AuditTrail]
) TEXTIMAGE_ON [AuditTrail];

