﻿CREATE TABLE [dbo].[Codes] (
    [Termkey]       CHAR (10)    NULL,
    [Rc5code]       CHAR (5)     NULL,
    [Termcode]      CHAR (2)     NULL,
    [Uniqueid]      CHAR (2)     NULL,
    [Specialtyflag] CHAR (8)     NULL,
    [Conceptstatus] CHAR (1)     NULL,
    [Term30]        CHAR (30)    NULL,
    [Icd9]          CHAR (20)    NULL,
    [Icd9def]       CHAR (2)     NULL,
    [Icd9cm]        CHAR (20)    NULL,
    [Icd9cmdef]     CHAR (2)     NULL,
    [Opcs]          CHAR (20)    NULL,
    [Opcsdef]       CHAR (2)     NULL,
    [Term60]        VARCHAR (60) NULL
) ON [DDict];


GO
CREATE NONCLUSTERED INDEX [KeyTermkey]
    ON [dbo].[Codes]([Termkey] ASC)
    ON [DDict];


GO
CREATE UNIQUE NONCLUSTERED INDEX [KeyTermcode]
    ON [dbo].[Codes]([Rc5code] ASC, [Termcode] ASC)
    ON [DDict];

