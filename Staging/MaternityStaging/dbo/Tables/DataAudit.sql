﻿CREATE TABLE [dbo].[DataAudit] (
    [Dateadded] DATETIME     CONSTRAINT [DF_DataAudit_Dateadded] DEFAULT (getdate()) NULL,
    [Addedby]   VARCHAR (50) CONSTRAINT [DF_DataAudit_Addedby] DEFAULT (suser_sname()) NULL,
    [Source]    CHAR (1)     NULL,
    [Action]    CHAR (1)     NULL,
    [Pointer]   INT          NULL,
    [Revnoteno] CHAR (25)    NULL,
    [Data]      TEXT         NULL
) ON [AuditTrail] TEXTIMAGE_ON [AuditTrail];


GO
CREATE NONCLUSTERED INDEX [idx_DAudit_Revnoteno]
    ON [dbo].[DataAudit]([Revnoteno] ASC) WITH (FILLFACTOR = 90)
    ON [AuditTrail];


GO
CREATE NONCLUSTERED INDEX [idx_DAudit_User]
    ON [dbo].[DataAudit]([Addedby] ASC) WITH (FILLFACTOR = 90)
    ON [AuditTrail];


GO
CREATE NONCLUSTERED INDEX [idx_DAudit_Pointer]
    ON [dbo].[DataAudit]([Pointer] ASC) WITH (FILLFACTOR = 90)
    ON [AuditTrail];


GO
CREATE NONCLUSTERED INDEX [idx_DAudit_DateAdded]
    ON [dbo].[DataAudit]([Dateadded] ASC) WITH (FILLFACTOR = 90)
    ON [AuditTrail];

