﻿CREATE TABLE [dbo].[DataExtractPatientLog] (
    [PatientPointer] INT       NOT NULL,
    [UserPointer]    INT       NOT NULL,
    [LoggedDate]     DATETIME  CONSTRAINT [DF_DataExtractPatientLog_LoggedDate] DEFAULT (getdate()) NOT NULL,
    [Machine]        CHAR (25) NULL
) ON [AuditTrail];


GO
CREATE NONCLUSTERED INDEX [NDX1_DataExtractPatientLog]
    ON [dbo].[DataExtractPatientLog]([LoggedDate] ASC)
    ON [AuditTrail];

