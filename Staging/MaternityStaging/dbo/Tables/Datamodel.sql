﻿CREATE TABLE [dbo].[Datamodel] (
    [Setid]    INT            NULL,
    [Type]     SMALLINT       NULL,
    [Code]     CHAR (5)       NULL,
    [Heading]  CHAR (5)       NULL,
    [Sequence] SMALLINT       NULL,
    [Mode]     SMALLINT       NULL,
    [Prompt]   CHAR (40)      NULL,
    [Help]     VARCHAR (1000) NULL
) ON [DDict];


GO
CREATE UNIQUE NONCLUSTERED INDEX [KeySetCode]
    ON [dbo].[Datamodel]([Setid] ASC, [Code] ASC)
    ON [DDict];


GO
CREATE NONCLUSTERED INDEX [KeyCode]
    ON [dbo].[Datamodel]([Code] ASC)
    ON [DDict];


GO
CREATE NONCLUSTERED INDEX [KeySetHeading]
    ON [dbo].[Datamodel]([Setid] ASC, [Heading] ASC)
    ON [DDict];


GO
CREATE NONCLUSTERED INDEX [KeyTypeSet]
    ON [dbo].[Datamodel]([Type] ASC, [Setid] ASC)
    ON [DDict];

