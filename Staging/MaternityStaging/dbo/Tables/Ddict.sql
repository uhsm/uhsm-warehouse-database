﻿CREATE TABLE [dbo].[Ddict] (
    [Fieldname]  CHAR (35) NULL,
    [Prompt]     CHAR (50) NULL,
    [Dtype]      CHAR (2)  NULL,
    [Validation] CHAR (40) NULL,
    [Dlength]    SMALLINT  NULL,
    [Label]      CHAR (40) NULL,
    [Dmask]      CHAR (10) NULL,
    [Units]      CHAR (10) NULL
) ON [DDict];


GO
CREATE UNIQUE NONCLUSTERED INDEX [KeyDDict]
    ON [dbo].[Ddict]([Fieldname] ASC)
    ON [DDict];

