﻿CREATE TABLE [dbo].[EventLog] (
    [DateTime]        DATETIME  CONSTRAINT [DF_EventLog_DateTime] DEFAULT (getdate()) NOT NULL,
    [Sequence]        INT       IDENTITY (1, 1) NOT NULL,
    [MessageID]       INT       NULL,
    [EventID]         INT       NOT NULL,
    [ErrorNo]         INT       NOT NULL,
    [AppID]           CHAR (25) COLLATE Latin1_General_CI_AS NULL,
    [RegisterPointer] INT       NULL,
    [Data]            TEXT      COLLATE Latin1_General_CI_AS NULL,
    CONSTRAINT [Primary] PRIMARY KEY CLUSTERED ([DateTime] ASC, [Sequence] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [Messages]
    ON [dbo].[EventLog]([MessageID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [Register]
    ON [dbo].[EventLog]([RegisterPointer] ASC, [MessageID] ASC) WITH (FILLFACTOR = 90);


GO



/*trgEventLogInsert */
CREATE TRIGGER trgEventLogInsert ON dbo.EventLog
FOR INSERT AS
BEGIN
    Declare @l_lngPointer  int
    Declare @l_lngMessageID  int


    Select @l_lngPointer = (Select RegisterPointer from inserted)
    Select @l_lngMessageID = (Select MessageID  FROM inserted)

      If  @l_lngPointer > 0
      Begin
         UPDATE  EventLog Set
         RegisterPointer = @l_lngPointer
         Where MessageID = @l_lngMessageID
         And RegisterPointer = -1
      End
End

