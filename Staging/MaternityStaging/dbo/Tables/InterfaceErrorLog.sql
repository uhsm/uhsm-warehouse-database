﻿CREATE TABLE [dbo].[InterfaceErrorLog] (
    [OID]              BIGINT         IDENTITY (1, 1) NOT NULL,
    [PatientPointer]   BIGINT         NULL,
    [ErrorID]          VARCHAR (50)   NULL,
    [ErrorSummary]     VARCHAR (500)  NULL,
    [ErrorDescription] VARCHAR (MAX)  NULL,
    [ErrorStatus]      VARCHAR (50)   NULL,
    [SuggestedAction]  VARCHAR (1000) NULL,
    [ErrorSource]      VARCHAR (50)   NULL,
    [Direction]        VARCHAR (50)   NULL,
    [EventType]        VARCHAR (50)   NULL,
    [MessageID]        VARCHAR (50)   NULL,
    [Priority]         INT            NULL,
    [TriggerLogOID]    BIGINT         NULL,
    [CreatedBy]        BIGINT         NOT NULL,
    [CreatedAt]        DATETIME       NOT NULL,
    [ModifiedBy]       BIGINT         NOT NULL,
    [ModifiedAt]       DATETIME       NOT NULL,
    [SourceOID]        BIGINT         NULL,
    [Status]           CHAR (1)       NOT NULL,
    [CMATXML]         VARCHAR (MAX)  NULL,
    [PDSMessageStatus] VARCHAR (50)   NULL,
    [TransactionXML]   VARCHAR (MAX)  NULL,
    [Workstation]      VARCHAR (100)  NULL,
    [Location]         VARCHAR (1000) NULL
);


GO
CREATE NONCLUSTERED INDEX [INDX_InterfaceErrorLog_OID]
    ON [dbo].[InterfaceErrorLog]([OID] ASC);


GO
CREATE NONCLUSTERED INDEX [INDX_InterfaceErrorLog_TrigLogOID]
    ON [dbo].[InterfaceErrorLog]([TriggerLogOID] ASC);

