﻿CREATE TABLE [dbo].[InterfaceTriggerLog] (
    [OID]               BIGINT        IDENTITY (1, 1) NOT NULL,
    [PatientPointer]    BIGINT        NULL,
    [PregnancyNo]       BIGINT        NULL,
    [BabyOrder]         BIGINT        NULL,
    [EventType]         VARCHAR (50)  NULL,
    [ProcessedBy]       BIGINT        NULL,
    [ProcessedDate]     DATETIME      NOT NULL,
    [TransactionStatus] CHAR (1)      NULL,
    [CreatedBy]         BIGINT        NOT NULL,
    [CreatedAt]         DATETIME      NOT NULL,
    [ModifiedBy]        BIGINT        NOT NULL,
    [ModifiedAt]        DATETIME      NOT NULL,
    [Status]            CHAR (1)      NOT NULL,
    [MessageID]         VARCHAR (MAX) NULL
);


GO
CREATE NONCLUSTERED INDEX [INDX_InterfaceTriggerLog_OID]
    ON [dbo].[InterfaceTriggerLog]([OID] ASC);

