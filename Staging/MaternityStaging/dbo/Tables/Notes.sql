﻿CREATE TABLE [dbo].[Notes] (
    [Pointer]    INT            NULL,
    [Noteno]     CHAR (25)      NULL,
    [Revnoteno]  CHAR (25)      NULL,
    [Code]       CHAR (5)       NULL,
    [Notedate]   INT            NULL,
    [Recall]     INT            NULL,
    [Notetype]   CHAR (1)       NULL,
    [Valuetype]  CHAR (1)       NULL,
    [Nvalue]     CHAR (30)      NULL,
    [Attributes] VARCHAR (7000) NULL
);


GO
CREATE UNIQUE CLUSTERED INDEX [KeyHierarchy]
    ON [dbo].[Notes]([Pointer] ASC, [Revnoteno] ASC);


GO
CREATE NONCLUSTERED INDEX [KeyIntCode]
    ON [dbo].[Notes]([Pointer] ASC, [Code] ASC, [Notedate] ASC);


GO
CREATE NONCLUSTERED INDEX [KeyExtCode]
    ON [dbo].[Notes]([Code] ASC, [Notedate] ASC);


GO
CREATE NONCLUSTERED INDEX [KeyRecall]
    ON [dbo].[Notes]([Code] ASC, [Recall] ASC);


GO
CREATE NONCLUSTERED INDEX [KeyNoteValue]
    ON [dbo].[Notes]([Code] ASC, [Nvalue] ASC);


GO
CREATE NONCLUSTERED INDEX [KeyContext]
    ON [dbo].[Notes]([Pointer] ASC, [Noteno] ASC, [Code] ASC);


GO


/****** Object:  Trigger dbo.trgNoteInsert    Script Date: 09/07/01 09:30:13 ******/
--########################################################
--NOTE TRIGGERS
--########################################################
CREATE TRIGGER trgNoteInsert ON dbo.Notes
FOR INSERT AS
BEGIN
  INSERT INTO DataAudit
    (Source, Action, Pointer, Revnoteno)
  SELECT 'N', 'I', ins.Pointer, ins.Revnoteno
  FROM inserted ins
END

GO


/****** Object:  Trigger dbo.trgNoteUpdate    Script Date: 09/07/01 09:30:13 ******/
CREATE TRIGGER trgNoteUpdate ON dbo.Notes
FOR UPDATE AS
BEGIN
  INSERT INTO DataAudit
    (Source, Action, Pointer, Revnoteno, Data)
  SELECT 'N', 'U', del.Pointer, del.Revnoteno ,
  del.Code + '~' +
  CAST(del.Notedate AS varchar(50)) + '~' +
  CAST(del.Recall AS varchar(20)) + '~' +
  del.Notetype + '~' +
  del.Valuetype  + '~' +
  RTRIM(del.Nvalue) + '~' +
  RTRIM(del.Attributes)
  FROM deleted del
END

GO



/****** Object:  Trigger dbo.trgNoteDelete    Script Date: 09/07/01 09:30:13 ******/
CREATE TRIGGER trgNoteDelete ON dbo.Notes
FOR DELETE AS
BEGIN
  INSERT INTO DataAudit
    (Source, Action, Pointer,  Revnoteno, Data)
  SELECT 'N', 'D', del.Pointer, del.Revnoteno,
  del.Code + '~' +
  CAST(del.Notedate AS varchar(50)) + '~' +
  CAST(del.Recall AS varchar(20)) + '~' +
  del.Notetype + '~' +
  del.Valuetype  + '~' +
  RTRIM(del.Nvalue) + '~' +
  RTRIM(del.Attributes)
  FROM deleted del
END
