﻿CREATE TABLE [dbo].[Process] (
    [ProcessID]      INT          NOT NULL,
    [AppID]          INT          NULL,
    [Flags]          INT          NULL,
    [RecordDate]     DATETIME     NULL,
    [PatientPointer] INT          NULL,
    [UserPointer]    INT          NULL,
    [Machine]        VARCHAR (50) NULL
) ON [AuditTrail];


GO
CREATE NONCLUSTERED INDEX [KeyProcess]
    ON [dbo].[Process]([ProcessID] ASC)
    ON [AuditTrail];

