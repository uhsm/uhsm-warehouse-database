﻿CREATE TABLE [dbo].[Queue] (
    [TransID]     NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [FullTransID] CHAR (30)    NULL,
    [DestID]      CHAR (10)    NULL,
    [Direction]   NUMERIC (18) NULL,
    [MessageID]   NUMERIC (18) NULL,
    [SourceID]    CHAR (20)    NULL,
    [InsertDate]  DATETIME     NULL,
    [Priority]    NUMERIC (18) NULL,
    [TransData]   TEXT         NULL,
    CONSTRAINT [PK_Queue] PRIMARY KEY CLUSTERED ([TransID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IdxQ1]
    ON [dbo].[Queue]([Direction] ASC, [DestID] ASC);

