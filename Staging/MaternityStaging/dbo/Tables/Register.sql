﻿CREATE TABLE [dbo].[Register] (
    [Regtype]         CHAR (1)      NULL,
    [Status]          CHAR (1)      NULL,
    [Surname]         CHAR (25)     NULL,
    [Forename]        CHAR (25)     NULL,
    [Forename2]       CHAR (15)     NULL,
    [Title]           CHAR (5)      NULL,
    [Sex]             CHAR (20)     NULL,
    [Birthdate]       INT           NULL,
    [Pointer]         INT           NOT NULL,
    [Address1]        CHAR (35)     NULL,
    [Address2]        CHAR (35)     NULL,
    [Address3]        CHAR (35)     NULL,
    [Address4]        CHAR (35)     NULL,
    [Postcode]        CHAR (10)     NULL,
    [Phone]           CHAR (35)     NULL,
    [Number1]         CHAR (14)     NULL,
    [Number2]         CHAR (14)     NULL,
    [Number3]         CHAR (14)     NULL,
    [Dateadded]       INT           NULL,
    [Datemodified]    INT           NULL,
    [Semaphore]       CHAR (1)      NULL,
    [Previous]        CHAR (20)     NULL,
    [Extra]           VARCHAR (250) NULL,
    [Nhsnumberstatus] CHAR (50)     NULL
);


GO
CREATE CLUSTERED INDEX [KeyName]
    ON [dbo].[Register]([Regtype] ASC, [Status] ASC, [Surname] ASC, [Forename] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [KeyPointer]
    ON [dbo].[Register]([Pointer] ASC);


GO
CREATE NONCLUSTERED INDEX [KeyBirthdate]
    ON [dbo].[Register]([Regtype] ASC, [Status] ASC, [Birthdate] ASC);


GO
CREATE NONCLUSTERED INDEX [KeyAddress]
    ON [dbo].[Register]([Regtype] ASC, [Status] ASC, [Address3] ASC, [Address2] ASC, [Address1] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [KeyNumber]
    ON [dbo].[Register]([Regtype] ASC, [Status] ASC, [Number1] ASC);


GO
CREATE NONCLUSTERED INDEX [KeyNumber2]
    ON [dbo].[Register]([Regtype] ASC, [Status] ASC, [Number2] ASC);


GO
CREATE NONCLUSTERED INDEX [KeyNumber3]
    ON [dbo].[Register]([Regtype] ASC, [Status] ASC, [Number3] ASC);


GO


--########################################################
--REGISTER TRIGGERS
--########################################################
CREATE TRIGGER trgRegisterInsert ON dbo.Register
FOR INSERT AS
BEGIN
  INSERT INTO DataAudit
    (Source, Action, Pointer)
  SELECT 'R', 'I', ins.Pointer
  FROM inserted ins
END


GO

	CREATE TRIGGER trgRegisterUpdate ON dbo.Register
	FOR UPDATE AS
	BEGIN
	  INSERT INTO DataAudit
		(Source, Action, Pointer,  Data)
	  SELECT 'R', 'U', del.Pointer,
	  del.Regtype + '~' +
	  del.Status + '~' +
	  RTRIM(del.Surname) + '~' +
	  RTRIM(del.Forename) + '~' +
	  RTRIM(del.Forename2) + '~' +
	  RTRIM(del.Title) + '~' +
	  RTRIM(del.Sex) + '~' +
	  CAST(del.Birthdate AS varchar(50)) + '~' +
	  RTRIM(del.Address1) + '~' +
	  RTRIM(del.Address2) + '~' +
	  RTRIM(del.Address3) + '~' +
	  RTRIM(del.Address4) + '~' +
	  RTRIM(del.Postcode) + '~' +
	  RTRIM(del.Phone) + '~' +
	  RTRIM(del.Number1) + '~' +
	  RTRIM(del.Number2) + '~' +
	  RTRIM(del.Number3) + '~' +
	  CAST(del.Dateadded AS varchar(50)) + '~' +
	  CAST(del.Datemodified AS varchar(50)) + '~' +
	  del.Semaphore + '~' +
	  RTRIM(del.Previous) + '~' +
	  RTRIM(del.Extra) + '~' +
	  RTRIM(del.Nhsnumberstatus)
	  FROM deleted del
	END

GO

	CREATE TRIGGER trgRegisterDelete ON dbo.Register
	FOR DELETE AS
	BEGIN
	  INSERT INTO DataAudit
		(Source, Action, Pointer,  Data)
	  SELECT 'R', 'D', del.Pointer,
	  del.Regtype + '~' +
	  del.Status + '~' +
	  RTRIM(del.Surname) + '~' +
	  RTRIM(del.Forename) + '~' +
	  RTRIM(del.Forename2) + '~' +
	  RTRIM(del.Title) + '~' +
	  RTRIM(del.Sex) + '~' +
	  CAST(del.Birthdate AS varchar(50)) + '~' +
	  RTRIM(del.Address1) + '~' +
	  RTRIM(del.Address2) + '~' +
	  RTRIM(del.Address3) + '~' +
	  RTRIM(del.Address4) + '~' +
	  RTRIM(del.Postcode) + '~' +
	  RTRIM(del.Phone) + '~' +
	  RTRIM(del.Number1) + '~' +
	  RTRIM(del.Number2) + '~' +
	  RTRIM(del.Number3) + '~' +
	  CAST(del.Dateadded AS varchar(50)) + '~' +
	  CAST(del.Datemodified AS varchar(50)) + '~' +
	  del.Semaphore + '~' +
	  RTRIM(del.Previous) + '~' +
	  RTRIM(del.Extra) + '~' +
	  RTRIM(del.Nhsnumberstatus)
	  FROM deleted del
	END
