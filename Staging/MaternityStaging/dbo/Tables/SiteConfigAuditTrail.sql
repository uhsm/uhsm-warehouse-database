﻿CREATE TABLE [dbo].[SiteConfigAuditTrail] (
    [SessionID]       INT         NULL,
    [DateAdded]       DATETIME    NULL,
    [UserName]        CHAR (25)   NULL,
    [ModuleName]      CHAR (25)   NULL,
    [ConfigTableName] CHAR (50)   NULL,
    [ConfigTableKey]  CHAR (25)   NULL,
    [QuestionCode]    CHAR (5)    NULL,
    [QuestionRubric]  CHAR (60)   NULL,
    [ProtocolName]    CHAR (50)   NULL,
    [ValidationName]  CHAR (50)   NULL,
    [Description]     CHAR (1000) NULL,
    [PreviousValue]   TEXT        NULL,
    [UpdatedValue]    TEXT        NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_SiteConfigAudit]
    ON [dbo].[SiteConfigAuditTrail]([DateAdded] ASC, [UserName] ASC);

