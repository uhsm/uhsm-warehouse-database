﻿CREATE TABLE [dbo].[Stafflink] (
    [Linkid]       INT       NOT NULL,
    [Staffpointer] INT       NOT NULL,
    [Organisation] INT       NOT NULL,
    [Pascode]      CHAR (20) NULL
) ON [DDict];


GO
CREATE NONCLUSTERED INDEX [KeyLink]
    ON [dbo].[Stafflink]([Linkid] ASC)
    ON [DDict];


GO
CREATE NONCLUSTERED INDEX [KeyStaff]
    ON [dbo].[Stafflink]([Staffpointer] ASC)
    ON [DDict];


GO
CREATE NONCLUSTERED INDEX [KeyOrganisation]
    ON [dbo].[Stafflink]([Organisation] ASC)
    ON [DDict];


GO
CREATE NONCLUSTERED INDEX [KeyPascode]
    ON [dbo].[Stafflink]([Pascode] ASC)
    ON [DDict];


GO


/****** Object:  Trigger dbo.trgStafflinkInsert    Script Date: 16/08/02 ******/
--########################################################
--Stafflink TRIGGERS
--########################################################
CREATE TRIGGER trgStafflinkInsert ON dbo.Stafflink
FOR INSERT AS
BEGIN
  INSERT INTO DataAudit
    (Source, Action, Pointer)
  SELECT 'L', 'I', ins.Linkid
  FROM inserted ins
END

GO


/****** Object:  Trigger dbo.trgStafflinkUpdate    Script Date: 16/08/02 ******/
CREATE TRIGGER trgStafflinkUpdate ON dbo.Stafflink
FOR UPDATE AS
BEGIN
  INSERT INTO DataAudit
    (Source, Action, Pointer, Data)
  SELECT 'L', 'U', del.Linkid,
  RTRIM(CAST(del.Staffpointer AS varchar(50))) + '~' +
  RTRIM(CAST(del.Organisation AS varchar(50))) + '~' +
  RTRIM(del.Pascode)
  FROM deleted del
END

GO


/****** Object:  Trigger dbo.trgNoteDelete    Script Date: 16/08/02 ******/
CREATE TRIGGER trgStafflinkDelete ON dbo.Stafflink
FOR DELETE AS
BEGIN
  INSERT INTO DataAudit
    (Source, Action, Pointer, Data)
  SELECT 'L', 'D', del.Linkid,
  RTRIM(CAST(del.Staffpointer AS varchar(50))) + '~' +
  RTRIM(CAST(del.Organisation AS varchar(50))) + '~' +
  RTRIM(del.Pascode)
  FROM deleted del
END
