﻿CREATE TABLE [dbo].[UserGroupAccess] (
    [UserGroup_ID]  VARCHAR (50)  NOT NULL,
    [Screen_ID]     VARCHAR (200) NOT NULL,
    [Screen_Name]   VARCHAR (100) NOT NULL,
    [Screen_Type]   CHAR (1)      NULL,
    [Access_Rights] CHAR (1)      NULL,
    [Updated_By]    VARCHAR (50)  NULL,
    [Updated_On]    DATETIME      CONSTRAINT [DF_UserGroupAccess_UpdatedOn] DEFAULT (getdate()) NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_UserGroupAccess_ID]
    ON [dbo].[UserGroupAccess]([UserGroup_ID] ASC, [Screen_ID] ASC);

