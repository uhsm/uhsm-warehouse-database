﻿CREATE TABLE [dbo].[Valid] (
    [Vtable] CHAR (50)    NULL,
    [Vkey]   CHAR (50)    NULL,
    [Vfield] CHAR (250)   NULL,
    [Vtype]  CHAR (1)     NULL,
    [Code]   CHAR (15)    NULL,
    [Seq]    NUMERIC (18) IDENTITY (1, 1) NOT NULL
);


GO
CREATE UNIQUE CLUSTERED INDEX [KeyValid]
    ON [dbo].[Valid]([Seq] ASC, [Vfield] ASC, [Vtable] ASC, [Vkey] ASC);

