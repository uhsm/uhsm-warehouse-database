﻿Create FUNCTION [dbo].[SCMSiteIDFn]() 
-- Function to return SiteID. Used by defaults in table creation scripts
RETURNS SMALLINT
AS
BEGIN
    DECLARE @SiteID SMALLINT

        IF EXISTS (SELECT Name FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[Site]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
        SELECT @SiteID=siteID FROM Site

    IF @SiteID is NULL
        SELECT @SiteID=SiteID FROM SCMInstallDetail          
    RETURN ISNULL(@SiteID,0) -- Default to 0
END
