﻿CREATE FUNCTION [dbo].[SXAGNAbnormalCodeFn]
(
	@param1 int,
	@param2 char(5)
)
RETURNS @CodeConversion TABLE (Code VARCHAR(5) Primary Key,
                               SequenceID VARCHAR(10),
                               SequenceNumber INT)
AS
BEGIN
	INSERT @CodeConversion (Code, SequenceID, SequenceNumber)
    SELECT 'U', 'Unknown', 3

    INSERT @CodeConversion (Code, SequenceID, SequenceNumber)
    SELECT 'HH', 'Critical', 1

    INSERT @CodeConversion (Code, SequenceID, SequenceNumber)
    SELECT 'LL', 'Critical', 1

    INSERT @CodeConversion (Code, SequenceID, SequenceNumber)
    SELECT 'AA', 'Critical', 1
 
    INSERT @CodeConversion (Code, SequenceID, SequenceNumber)
    SELECT 'H', 'Abnormal', 2

    INSERT @CodeConversion (Code, SequenceID, SequenceNumber)
    SELECT 'L', 'Abnormal', 2

    INSERT @CodeConversion (Code, SequenceID, SequenceNumber)
    SELECT 'A', 'Abnormal', 2

    INSERT @CodeConversion (Code, SequenceID, SequenceNumber)
    SELECT 'N', 'See Report', 4
    RETURN
END
