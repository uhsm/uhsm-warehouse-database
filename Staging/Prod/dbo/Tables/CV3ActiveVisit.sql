﻿CREATE TABLE [dbo].[CV3ActiveVisit] (
    [SiteID]              SMALLINT         NULL,
    [RepFlags]            TINYINT          NULL,
    [VisitGUID]           [dbo].[HVCIDdt]  NOT NULL,
    [ClientGUID]          [dbo].[HVCIDdt]  NOT NULL,
    [MSrepl_tran_version] UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    [MSReplrowguid]       UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [CV3ActiveVisitPK] PRIMARY KEY CLUSTERED ([VisitGUID] ASC) WITH (FILLFACTOR = 80)
);


GO
