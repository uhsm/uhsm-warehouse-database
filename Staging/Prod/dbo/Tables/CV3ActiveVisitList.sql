﻿CREATE TABLE [dbo].[CV3ActiveVisitList] (
    [SiteID]                      SMALLINT         NULL,
    [RepFlags]                    TINYINT          NULL,
    [Build]                       INT              NOT NULL,
    [TouchedBy]                   VARCHAR (50)     NULL,
    [TouchedWhen]                 DATETIME         NULL,
    [CreatedBy]                   VARCHAR (50)     NULL,
    [CreatedWhen]                 DATETIME         NULL,
    [Active]                      BIT              NOT NULL,
    [GUID]                        [dbo].[HVCIDdt]  NOT NULL,
    [VisitStatus]                 CHAR (3)         NULL,
    [InternalVisitStatus]         CHAR (3)         NULL,
    [ChartGUID]                   [dbo].[HVCIDdt]  NULL,
    [ClientGUID]                  [dbo].[HVCIDdt]  NULL,
    [AdmitDtm]                    DATETIME         NULL,
    [DischargeDtm]                DATETIME         NULL,
    [CloseDtm]                    DATETIME         NULL,
    [IDCode]                      CHAR (20)        NULL,
    [VisitIDCode]                 CHAR (20)        NULL,
    [TypeCode]                    CHAR (15)        NULL,
    [CareLevelCode]               CHAR (15)        NULL,
    [ServiceGUID]                 [dbo].[HVCIDdt]  NULL,
    [ClientDisplayName]           VARCHAR (50)     NULL,
    [ProviderDisplayName]         VARCHAR (50)     NULL,
    [CurrentLocation]             VARCHAR (50)     NULL,
    [CurrentLocationGUID]         [dbo].[HVCIDdt]  NULL,
    [NumUnackAlerts]              INT              NOT NULL,
    [NextLocnGrpGUID]             [dbo].[HVCIDdt]  NULL,
    [NextLocn]                    VARCHAR (50)     NULL,
    [PlanDischDtm]                DATETIME         NULL,
    [FacilityGUID]                [dbo].[HVCIDdt]  NULL,
    [LocnGroupName]               VARCHAR (30)     NULL,
    [GenderCode]                  VARCHAR (30)     NULL,
    [IsPermanent]                 TINYINT          NULL,
    [IsMaterialized]              BIT              NOT NULL,
    [BirthDayNum]                 INT              NULL,
    [BirthMonthNum]               INT              NULL,
    [BirthYearNum]                INT              NULL,
    [VisitReason]                 VARCHAR (255)    NULL,
    [ServiceDescription]          VARCHAR (60)     NULL,
    [TempLocnGUID]                [dbo].[HVCIDdt]  NULL,
    [TemporaryLocation]           VARCHAR (50)     NULL,
    [PrivacyStatusCode]           VARCHAR (30)     NULL,
    [SecurityLevel]               INT              NULL,
    [DeceasedDtm]                 DATETIME         NULL,
    [CurrentLocationArrivedDtm]   DATETIME         NULL,
    [TemporaryLocationArrivedDtm] DATETIME         NULL,
    [RoomLocnGrpGUID]             [dbo].[HVCIDdt]  NULL,
    [MSReplrowguid]               UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [CV3ActiveVisitListPK] PRIMARY KEY NONCLUSTERED ([GUID] ASC) WITH (FILLFACTOR = 80)
);


GO
ALTER TABLE [dbo].[CV3ActiveVisitList] SET (LOCK_ESCALATION = DISABLE);


GO
CREATE UNIQUE CLUSTERED INDEX [ActiveVisitClustIdx]
    ON [dbo].[CV3ActiveVisitList]([ClientGUID] ASC, [GUID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [ActiveVisitAlt1Idx]
    ON [dbo].[CV3ActiveVisitList]([CurrentLocationGUID] ASC, [VisitStatus] ASC, [AdmitDtm] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [ActiveVisitAlt2Idx]
    ON [dbo].[CV3ActiveVisitList]([NextLocnGrpGUID] ASC, [VisitStatus] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [ActiveVisitAlt3Idx]
    ON [dbo].[CV3ActiveVisitList]([DischargeDtm] ASC, [VisitStatus] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [ActiveVisitAlt4Idx]
    ON [dbo].[CV3ActiveVisitList]([AdmitDtm] ASC, [VisitStatus] ASC, [CurrentLocationGUID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [CV3ActiveVisitListAI05]
    ON [dbo].[CV3ActiveVisitList]([TempLocnGUID] ASC, [VisitStatus] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [CV3ActiveVisitListAI06]
    ON [dbo].[CV3ActiveVisitList]([RoomLocnGrpGUID] ASC, [VisitStatus] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [CV3ActiveVisitListAI07]
    ON [dbo].[CV3ActiveVisitList]([VisitStatus] ASC, [Active] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [CV3ActiveVisitListAI08]
    ON [dbo].[CV3ActiveVisitList]([FacilityGUID] ASC, [VisitStatus] ASC) WITH (FILLFACTOR = 80);


GO
