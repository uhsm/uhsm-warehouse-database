﻿CREATE TABLE [dbo].[CV3Address] (
    [SiteID]              SMALLINT         DEFAULT NULL,
    [RepFlags]            TINYINT          DEFAULT ((0)) NULL,
    [Build]               INT              NOT NULL,
    [TouchedBy]           VARCHAR (50)     NULL,
    [TouchedWhen]         DATETIME         DEFAULT (getdate()) NULL,
    [CreatedBy]           VARCHAR (50)     NULL,
    [CreatedWhen]         DATETIME         DEFAULT (getdate()) NULL,
    [Active]              BIT              NOT NULL,
    [GUID]                [dbo].[HVCIDdt]  NOT NULL,
    [Line1]               VARCHAR (64)     NULL,
    [Line2]               VARCHAR (50)     NULL,
    [Line3]               VARCHAR (50)     NULL,
    [City]                VARCHAR (25)     NULL,
    [CountryCode]         VARCHAR (30)     NULL,
    [PostalCode]          VARCHAR (15)     NULL,
    [PhoneNumber]         VARCHAR (40)     NULL,
    [AreaCode]            VARCHAR (10)     NULL,
    [IsCurrent]           BIT              NOT NULL,
    [EffectiveDate]       DATETIME         NULL,
    [ExpiryDate]          DATETIME         NULL,
    [ParentGUID]          [dbo].[HVCIDdt]  NULL,
    [ClientVisitGUID]     [dbo].[HVCIDdt]  NULL,
    [TypeCode]            VARCHAR (15)     NULL,
    [ApplicSource]        CHAR (5)         NULL,
    [CountryDvsnCode]     VARCHAR (30)     NULL,
    [ScopeLevel]          CHAR (1)         NULL,
    [Status]              VARCHAR (10)     NULL,
    [MSrepl_tran_version] UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    [County]              VARCHAR (50)     NULL,
    [IsIncomplete]        BIT              DEFAULT ((0)) NOT NULL,
    [MSReplrowguid]       UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [CV3AddressPK] PRIMARY KEY NONCLUSTERED ([GUID] ASC)
);


GO
CREATE CLUSTERED INDEX [AddressClustIdx]
    ON [dbo].[CV3Address]([ParentGUID] ASC);


GO
