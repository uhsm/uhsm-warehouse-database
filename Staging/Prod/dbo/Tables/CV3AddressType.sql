﻿CREATE TABLE [dbo].[CV3AddressType] (
    [SiteID]              SMALLINT         DEFAULT  NULL,
    [RepFlags]            TINYINT          DEFAULT ((0)) NULL,
    [Build]               INT              NOT NULL,
    [TouchedBy]           VARCHAR (50)     NULL,
    [TouchedWhen]         DATETIME         DEFAULT (getdate()) NULL,
    [CreatedBy]           VARCHAR (50)     NULL,
    [CreatedWhen]         DATETIME         DEFAULT (getdate()) NULL,
    [Active]              BIT              NOT NULL,
    [GUID]                [dbo].[HVCIDdt]  NOT NULL,
    [Code]                VARCHAR (30)     NULL,
    [IsMultiple]          BIT              NOT NULL,
    [IsInteractiveEdit]   BIT              NOT NULL,
    [IsGeneralType]       BIT              NOT NULL,
    [SortSeqNum]          INT              NOT NULL,
    [MSrepl_tran_version] UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    [Abbreviation]        VARCHAR (5)      NULL,
    [AddressStyleType]    INT              DEFAULT ((2)) NOT NULL,
    [LineLength]          TINYINT          DEFAULT ((50)) NULL,
    [IsHVCDefined]        BIT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [CV3AddressTypePK] PRIMARY KEY NONCLUSTERED ([GUID] ASC)
);


GO
CREATE UNIQUE CLUSTERED INDEX [DictionaryIdx]
    ON [dbo].[CV3AddressType]([Code] ASC);

