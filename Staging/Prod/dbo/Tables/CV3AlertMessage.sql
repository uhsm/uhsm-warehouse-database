﻿CREATE TABLE [dbo].[CV3AlertMessage](
	[SiteID] [smallint] NULL,
	[RepFlags] [tinyint] NULL,
	[Build] [int] NOT NULL,
	[TouchedBy] [varchar](50) NULL,
	[TouchedWhen] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedWhen] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[GUID] [numeric](16, 0) NOT NULL,
	[ParentGUID] [dbo].[HVCIDdt] NULL,
	[TextLine] [varchar](255) NULL,
	[LineNumber] [int] NOT NULL,
	[ClientGUID] [numeric](16, 0) NULL,
	[ScopeLevel] [char](1) NULL,
	[MSrepl_tran_version] [uniqueidentifier] NULL,
	[MSReplrowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [CV3AlertMessagePK] PRIMARY KEY NONCLUSTERED 
(
	[GUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CV3AlertMessage] SET (LOCK_ESCALATION = DISABLE)
GO

ALTER TABLE [dbo].[CV3AlertMessage] ADD  DEFAULT ((0)) FOR [RepFlags]
GO

ALTER TABLE [dbo].[CV3AlertMessage] ADD  DEFAULT (getdate()) FOR [TouchedWhen]
GO

ALTER TABLE [dbo].[CV3AlertMessage] ADD  DEFAULT (getdate()) FOR [CreatedWhen]
GO

ALTER TABLE [dbo].[CV3AlertMessage] ADD  DEFAULT (newid()) FOR [MSrepl_tran_version]
GO

ALTER TABLE [dbo].[CV3AlertMessage] ADD  DEFAULT (newsequentialid()) FOR [MSReplrowguid]
GO


