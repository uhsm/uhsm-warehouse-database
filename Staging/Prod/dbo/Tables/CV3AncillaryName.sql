﻿CREATE TABLE [dbo].[CV3AncillaryName]
(
	[SiteID] [smallint] NULL,
	[RepFlags] [tinyint] NULL,
	[Build] [int] NOT NULL,
	[TouchedBy] [varchar](50) NULL,
	[TouchedWhen] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedWhen] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[GUID] [dbo].[HVCIDdt] NOT NULL,
	[Name] [varchar](125) NULL,
	[UpperName] [varchar](125) NULL,
	[CatalogType] [int] NOT NULL,
	[MainCatItemGUID] [dbo].[HVCIDdt] NULL,
	[AuxCatItemGUID] [dbo].[HVCIDdt] NULL,
	[CodingStd] [varchar](30) NULL,
	[UpperCodingStd] [varchar](30) NULL,
	[MSrepl_tran_version] [uniqueidentifier] NULL,
	[MainCatItemID] [int] NULL,
 CONSTRAINT [CV3AncillaryNamePK] PRIMARY KEY NONCLUSTERED 
(
	[GUID] ASC
)
)
