﻿CREATE TABLE [dbo].[CV3BasicObservation] (
    [SiteID]                              SMALLINT         DEFAULT  NULL,
    [RepFlags]                            TINYINT          DEFAULT ((0)) NULL,
    [Build]                               INT              NOT NULL,
    [TouchedBy]                           VARCHAR (50)     NULL,
    [TouchedWhen]                         DATETIME         DEFAULT (getdate()) NULL,
    [CreatedBy]                           VARCHAR (50)     NULL,
    [CreatedWhen]                         DATETIME         DEFAULT (getdate()) NULL,
    [Active]                              BIT              NOT NULL,
    [GUID]                                [dbo].[HVCIDdt]  NOT NULL,
    [Entered]                             DATETIME         NULL,
    [Status]                              VARCHAR (10)     NULL,
    [ToBeVerified]                        BIT              NOT NULL,
    [ToBeSigned]                          BIT              NOT NULL,
    [EnterRole]                           VARCHAR (30)     NULL,
    [UserGUID]                            [dbo].[HVCIDdt]  NULL,
    [ClientVisitGUID]                     [dbo].[HVCIDdt]  NULL,
    [ClientGUID]                          [dbo].[HVCIDdt]  NULL,
    [ChartGUID]                           [dbo].[HVCIDdt]  NULL,
    [ItemName]                            VARCHAR (125)    NULL,
    [RestrictedAccess]                    BIT              NOT NULL,
    [AuthorizedDtm]                       DATETIME         NULL,
    [ClusterID]                           VARCHAR (20)     NULL,
    [Value]                               VARCHAR (60)     NULL,
    [TypeCode]                            VARCHAR (2)      NULL,
    [IsHistory]                           BIT              NOT NULL,
    [IsTextual]                           BIT              NOT NULL,
    [HasHistory]                          BIT              NOT NULL,
    [AbnormalityCode]                     VARCHAR (5)      NULL,
    [ReferenceUpperLimit]                 VARCHAR (10)     NULL,
    [ReferenceLowerLimit]                 VARCHAR (10)     NULL,
    [UnitOfMeasure]                       VARCHAR (15)     NULL,
    [OrderGUID]                           [dbo].[HVCIDdt]  NULL,
    [ResultItemGUID]                      [dbo].[HVCIDdt]  NULL,
    [MasterGUID]                          [dbo].[HVCIDdt]  NULL,
    [ArrivalDtm]                          DATETIME         NULL,
    [HasMediaLink]                        TINYINT          NULL,
    [MSrepl_tran_version]                 UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    [IsReleasedToPatient]                 BIT              NULL,
    [ObservationDtm]                      DATETIME         NULL,
    [PerformingOrganizationObserverID]    INT              NULL,
    [PerformingOrganizationID]            INT              NULL,
    [PerformingOrganizationAddressID]     INT              NULL,
    [PerformingOrganizationDirectorID]    INT              NULL,
    [AnalysisDtm]                         DATETIME         NULL,
    [ResultItemCode]                      VARCHAR (125)    NULL,
    [ResultItemCodingStandard]            VARCHAR (60)     NULL,
    [ValueCode]                           VARCHAR (125)    NULL,
    [ValueCodingStandard]                 VARCHAR (30)     NULL,
    [AlternateValueCode]                  VARCHAR (20)     NULL,
    [AlternateValue]                      VARCHAR (200)    NULL,
    [AlternateValueCodingStandard]        VARCHAR (15)     NULL,
    [CodingSystemVersionID]               VARCHAR (10)     NULL,
    [AlternateCodingSystemVersionID]      VARCHAR (10)     NULL,
    [ObsMethodCode]                       VARCHAR (20)     NULL,
    [ObsMethodText]                       VARCHAR (200)    NULL,
    [ObsMethodCodingStandard]             VARCHAR (20)     NULL,
    [ObsMethodAltObservationMethodCode]   VARCHAR (20)     NULL,
    [ObsMethodAltObservationMethodText]   VARCHAR (200)    NULL,
    [ObsMethodAltCodingStandard]          VARCHAR (20)     NULL,
    [ObsMethodCodingStandardVersionId]    VARCHAR (10)     NULL,
    [ObsMethodAltCodingStandardVersionId] VARCHAR (10)     NULL,
    [CommentStartPosition]                INT              NULL,
    [HL7AbnormalityCode]                  VARCHAR (30)     NULL,
    [ObsDataType]                         INT              DEFAULT ((0)) NULL,
    [MSReplrowguid]                       UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [CV3BasicObservationPK] PRIMARY KEY NONCLUSTERED ([GUID] ASC)
);


GO
ALTER TABLE [dbo].[CV3BasicObservation] SET (LOCK_ESCALATION = DISABLE);


GO
CREATE CLUSTERED INDEX [BasicObservationClustIdx]
    ON [dbo].[CV3BasicObservation]([ClientGUID] ASC, [ChartGUID] ASC, [OrderGUID] ASC);


GO
CREATE STATISTICS [_WA_Sys_00000020_69BC9054]
    ON [dbo].[CV3BasicObservation]([OrderGUID]);


GO
 