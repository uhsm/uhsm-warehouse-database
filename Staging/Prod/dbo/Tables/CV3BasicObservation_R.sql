﻿CREATE TABLE [dbo].[CV3BasicObservation_R]
(
	[JobID] [int] NOT NULL,
	[ObjectName] [varchar](30) NULL,
	[ObjectGUID] [dbo].[HVCIDdt] NULL,
	[SubmittedReportGUID] [dbo].[HVCIDdt] NULL,
	[TotalLine] [int] NULL,
	[TextLine] [varchar](max) NULL,
	[BasicObservationJoin_RID] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[MSReplrowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [CV3BasicObservationJoin_RPK] PRIMARY KEY NONCLUSTERED 
(
	[BasicObservationJoin_RID] ASC
)
) 
