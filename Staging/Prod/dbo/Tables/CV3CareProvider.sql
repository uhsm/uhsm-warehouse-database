﻿CREATE TABLE [dbo].[CV3CareProvider] (
    [SiteID]              SMALLINT         DEFAULT  NULL,
    [RepFlags]            TINYINT          DEFAULT ((0)) NULL,
    [Build]               INT              NOT NULL,
    [TouchedBy]           VARCHAR (50)     NULL,
    [TouchedWhen]         DATETIME         DEFAULT (getdate()) NULL,
    [CreatedBy]           VARCHAR (50)     NULL,
    [CreatedWhen]         DATETIME         DEFAULT (getdate()) NULL,
    [Active]              BIT              NOT NULL,
    [GUID]                [dbo].[HVCIDdt]  NOT NULL,
    [IDCode]              CHAR (9)         NULL,
    [BillingIDCode]       CHAR (9)         NULL,
    [TypeCode]            VARCHAR (30)     NULL,
    [SignOrderFlag]       BIT              NOT NULL,
    [DisplayName]         VARCHAR (50)     NULL,
    [Discipline]          VARCHAR (30)     NULL,
    [TaskRoleType]        VARCHAR (30)     NULL,
    [MSrepl_tran_version] UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    [Note]                VARCHAR (50)     NULL,
    [IsFreeText]          BIT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [CV3CareProviderPK] PRIMARY KEY CLUSTERED ([GUID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [CV3CareProviderAI01]
    ON [dbo].[CV3CareProvider]([DisplayName] ASC);


GO
