﻿CREATE TABLE [dbo].[CV3CareProviderVisitRole]
(
	[SiteID] [smallint] NULL,
	[RepFlags] [tinyint] NULL,
	[Build] [int] NOT NULL,
	[TouchedBy] [varchar](50) NULL,
	[TouchedWhen] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedWhen] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[GUID] [dbo].[HVCIDdt] NOT NULL,
	[FromDtm] [datetime] NULL,
	[ToDtm] [datetime] NULL,
	[Status] [varchar](10) NULL,
	[ClientGUID] [dbo].[HVCIDdt] NULL,
	[ProviderGUID] [dbo].[HVCIDdt] NULL,
	[RoleTypeGUID] [dbo].[HVCIDdt] NULL,
	[RoleCode] [varchar](30) NULL,
	[ApplicSource] [char](5) NULL,
	[ClientVisitGUID] [dbo].[HVCIDdt] NULL,
	[NumProviderOrdersToSign] [int] NOT NULL,
	[ScopeLevel] [char](1) NULL,
	[ChartGUID] [dbo].[HVCIDdt] NULL,
	[MSrepl_tran_version] [uniqueidentifier] NULL,
	[MSReplrowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [CV3CareProviderVisitRolePK] PRIMARY KEY NONCLUSTERED 
(
	[GUID] ASC
)
)
