﻿CREATE TABLE [dbo].[CV3ClientID] (
    [SiteID]              SMALLINT         DEFAULT  NULL,
    [RepFlags]            TINYINT          DEFAULT ((0)) NULL,
    [Build]               INT              NOT NULL,
    [TouchedBy]           VARCHAR (50)     NULL,
    [TouchedWhen]         DATETIME         DEFAULT (getdate()) NULL,
    [CreatedBy]           VARCHAR (50)     NULL,
    [CreatedWhen]         DATETIME         DEFAULT (getdate()) NULL,
    [Active]              BIT              NOT NULL,
    [GUID]                [dbo].[HVCIDdt]  NOT NULL,
    [ClientIDCode]        VARCHAR (20)     NULL,
    [IDStatus]            VARCHAR (3)      NULL,
    [TypeCode]            VARCHAR (30)     NULL,
    [ClientGUID]          [dbo].[HVCIDdt]  NULL,
    [MSrepl_tran_version] UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    [IDSourceType]        INT              DEFAULT ((0)) NOT NULL,
    [MSReplrowguid]       UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [CV3ClientIDPK] PRIMARY KEY NONCLUSTERED ([GUID] ASC)
);


GO
ALTER TABLE [dbo].[CV3ClientID] SET (LOCK_ESCALATION = DISABLE);


GO
CREATE CLUSTERED INDEX [ClientIDClustIdx]
    ON [dbo].[CV3ClientID]([ClientGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [ClientIDAlt0Idx]
    ON [dbo].[CV3ClientID]([ClientIDCode] ASC, [TypeCode] ASC, [Active] ASC)
    INCLUDE([GUID]);


GO
 