﻿CREATE TABLE [dbo].[CV3ClientVisit] (
    [SiteID]                 SMALLINT         DEFAULT  NULL,
    [RepFlags]               TINYINT          DEFAULT ((0)) NULL,
    [Build]                  INT              NOT NULL,
    [TouchedBy]              VARCHAR (50)     NULL,
    [TouchedWhen]            DATETIME         DEFAULT (getdate()) NULL,
    [CreatedBy]              VARCHAR (50)     NULL,
    [CreatedWhen]            DATETIME         DEFAULT (getdate()) NULL,
    [Active]                 BIT              NOT NULL,
    [GUID]                   [dbo].[HVCIDdt]  NOT NULL,
    [VisitStatus]            CHAR (3)         NULL,
    [InternalVisitStatus]    CHAR (3)         NULL,
    [IsVIP]                  BIT              NOT NULL,
    [ChartGUID]              [dbo].[HVCIDdt]  NULL,
    [ClientGUID]             [dbo].[HVCIDdt]  NULL,
    [AdmitDtm]               DATETIME         NULL,
    [DischargeDtm]           DATETIME         NULL,
    [CloseDtm]               DATETIME         NULL,
    [PlannedDischargeDtm]    DATETIME         NULL,
    [IDCode]                 CHAR (20)        NULL,
    [VisitIDCode]            CHAR (20)        NULL,
    [TypeCode]               CHAR (15)        NULL,
    [CareLevelCode]          CHAR (15)        NULL,
    [ServiceGUID]            [dbo].[HVCIDdt]  NULL,
    [VisitTypeCareLevelGUID] [dbo].[HVCIDdt]  NULL,
    [ClientDisplayName]      VARCHAR (50)     NULL,
    [ProviderDisplayName]    VARCHAR (50)     NULL,
    [CurrentLocation]        VARCHAR (50)     NULL,
    [CurrentLocationGUID]    [dbo].[HVCIDdt]  NULL,
    [AdmitViaGUID]           [dbo].[HVCIDdt]  NULL,
    [NumUnackAlerts]         INT              NOT NULL,
    [AmbulStatusCode]        VARCHAR (30)     NULL,
    [ApplicSource]           CHAR (5)         NULL,
    [NextLocnGrpGUID]        [dbo].[HVCIDdt]  NULL,
    [NextLocn]               VARCHAR (50)     NULL,
    [TempLocnGUID]           [dbo].[HVCIDdt]  NULL,
    [TemporaryLocation]      VARCHAR (50)     NULL,
    [DischargeDisposition]   VARCHAR (75)     NULL,
    [DischargeLocation]      VARCHAR (75)     NULL,
    [MSrepl_tran_version]    UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    [ArcType]                INT              DEFAULT ((0)) NOT NULL,
    [MSReplrowguid]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [CV3ClientVisitPK] PRIMARY KEY NONCLUSTERED ([GUID] ASC),
    CONSTRAINT [CV3ClientVisitArcTypeNotNullCK] CHECK ([ArcType] IS NOT NULL)
);


GO
ALTER TABLE [dbo].[CV3ClientVisit] SET (LOCK_ESCALATION = DISABLE);


GO
CREATE UNIQUE CLUSTERED INDEX [CV3ClientVisitCI]
    ON [dbo].[CV3ClientVisit]([ClientGUID] ASC, [GUID] ASC);


GO
CREATE NONCLUSTERED INDEX [ClientVisitAlt0Idx]
    ON [dbo].[CV3ClientVisit]([ChartGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [ClientVisitAlt1Idx]
    ON [dbo].[CV3ClientVisit]([CurrentLocationGUID] ASC, [VisitStatus] ASC);


GO
CREATE NONCLUSTERED INDEX [ClientVisitAlt2Idx]
    ON [dbo].[CV3ClientVisit]([NextLocnGrpGUID] ASC, [VisitStatus] ASC);


GO
CREATE NONCLUSTERED INDEX [ClientVisitAlt3Idx]
    ON [dbo].[CV3ClientVisit]([TempLocnGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [ClientVisitAlt4Idx]
    ON [dbo].[CV3ClientVisit]([DischargeDtm] ASC, [VisitStatus] ASC);


GO
CREATE NONCLUSTERED INDEX [ClientVisitAlt5Idx]
    ON [dbo].[CV3ClientVisit]([AdmitDtm] ASC, [VisitStatus] ASC);


GO
CREATE NONCLUSTERED INDEX [ClientVisitAlt6Idx]
    ON [dbo].[CV3ClientVisit]([VisitStatus] ASC, [IDCode] ASC, [VisitIDCode] ASC);


GO
CREATE NONCLUSTERED INDEX [ClientVisitAlt7Idx]
    ON [dbo].[CV3ClientVisit]([VisitTypeCareLevelGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [CV3ClientVisitAI08]
    ON [dbo].[CV3ClientVisit]([ArcType] ASC)
    INCLUDE([GUID], [CreatedWhen]) WHERE ([ArcType]=(99));


GO
