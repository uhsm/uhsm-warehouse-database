﻿CREATE TABLE [dbo].[CV3ClientVisitLocation]
(
	[SiteID] [smallint] NULL,
	[RepFlags] [tinyint] NULL,
	[Build] [int] NOT NULL,
	[TouchedBy] [varchar](50) NULL,
	[TouchedWhen] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedWhen] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[GUID] [dbo].[HVCIDdt] NOT NULL,
	[ClientLocnTypeCode] [char](1) NULL,
	[Status] [char](3) NULL,
	[ClientVisitGUID] [dbo].[HVCIDdt] NULL,
	[LocationGUID] [dbo].[HVCIDdt] NULL,
	[TransferRequestDtm] [datetime] NULL,
	[ClientGUID] [dbo].[HVCIDdt] NULL,
	[MSrepl_tran_version] [uniqueidentifier] NULL,
	[PrivacyType] [int] NULL,
	[PrivacyStatusGUID] [dbo].[HVCIDdt] NULL,
	[TempEndDtm] [datetime] NULL,
	[CorrectedDtm] [datetime] NULL,
	[AccommodationID] [int] NULL,
	[LocationName] [varchar](30) NULL,
	[MSReplrowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [CV3ClientVisitLocationPK] PRIMARY KEY NONCLUSTERED 
(
	[GUID] ASC
)
)
