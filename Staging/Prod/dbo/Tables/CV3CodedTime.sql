﻿CREATE TABLE [dbo].[CV3CodedTime]
(
	[SiteID] [smallint] NULL,
	[RepFlags] [tinyint] NULL,
	[Build] [int] NOT NULL,
	[TouchedBy] [varchar](50) NULL,
	[TouchedWhen] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedWhen] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[GUID] [dbo].[HVCIDdt] NOT NULL,
	[Name] [varchar](30) NULL,
	[StartTime] [int] NOT NULL,
	[EndTime] [int] NOT NULL,
	[DefaultTime] [int] NOT NULL,
	[PriorityCode] [varchar](30) NULL,
	[AltPriorityCode] [varchar](30) NULL,
	[UseNext] [bit] NOT NULL,
	[IsValidForStopTime] [bit] NOT NULL,
	[IsEventBased] [tinyint] NULL,
	[MSrepl_tran_version] [uniqueidentifier] NULL,
 CONSTRAINT [CV3CodedTimePK] PRIMARY KEY NONCLUSTERED 
(
	[GUID] ASC
)
)
