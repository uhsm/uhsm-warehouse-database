﻿CREATE TABLE [dbo].[CV3DataItem]
(
	[SiteID] [smallint] NULL,
	[RepFlags] [tinyint] NULL,
	[Build] [int] NOT NULL,
	[TouchedBy] [varchar](50) NULL,
	[TouchedWhen] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedWhen] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[GUID] [dbo].[HVCIDdt] NOT NULL,
	[Code] [varchar](40) NULL,
	[Description] [varchar](40) NULL,
	[DataTypeCode] [varchar](30) NULL,
	[OrderTypeCode] [varchar](30) NULL,
	[DictionaryName] [varchar](30) NULL,
	[IsHVCField] [bit] NOT NULL,
	[MaxLen] [int] NOT NULL,
	[NumLines] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[IsCarryFwdUserData] [bit] NOT NULL,
	[MSrepl_tran_version] [uniqueidentifier] NULL,
	[EnterpriseEntityPathway] [varchar](1000) NULL,
 CONSTRAINT [CV3DataItemPK] PRIMARY KEY NONCLUSTERED 
(
	[GUID] ASC
)
)
