﻿CREATE TABLE [dbo].[CV3DiagnosticExtension]
(
	[SiteID] [smallint] NULL,
	[RepFlags] [tinyint] NULL,
	[Build] [int] NOT NULL,
	[TouchedBy] [varchar](50) NULL,
	[TouchedWhen] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedWhen] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[GUID] [dbo].[HVCIDdt] NOT NULL,
	[PrintLocationCode] [varchar](20) NULL,
	[SpecimenTypeCode] [varchar](30) NULL,
	[ResultPriorityCode] [varchar](30) NULL,
	[SystemResultPriorityCode] [char](6) NULL,
	[IsTemplate] [bit] NOT NULL,
	[SpecimenGUID] [dbo].[HVCIDdt] NULL,
	[ClientGUID] [dbo].[HVCIDdt] NULL,
	[MSrepl_tran_version] [uniqueidentifier] NULL,
	[ReportDtm] [datetime] NULL,
	[PrincipalInterpreterPersonID] [int] NULL,
	[ParentObservationGUID] [dbo].[HVCIDdt] NULL,
	[MSReplrowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [CV3DiagnosticExtensionPK] PRIMARY KEY CLUSTERED 
(
	[GUID] ASC
)
)