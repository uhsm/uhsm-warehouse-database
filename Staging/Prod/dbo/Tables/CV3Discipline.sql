﻿CREATE TABLE [dbo].[CV3Discipline](
	[SiteID] [smallint] NULL,
	[RepFlags] [tinyint] NULL,
	[Build] [int] NOT NULL,
	[TouchedBy] [varchar](50) NULL,
	[TouchedWhen] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedWhen] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[GUID] [dbo].[HVCIDdt] NOT NULL,
	[Code] [varchar](30) NULL,
	[MSrepl_tran_version] [uniqueidentifier] NULL,
	[TaxonomyCodeID] [int] NULL,
 CONSTRAINT [CV3DisciplinePK] PRIMARY KEY NONCLUSTERED 
(
	[GUID] ASC
)On [PRIMARY]
)