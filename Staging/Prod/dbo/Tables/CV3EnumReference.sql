﻿CREATE TABLE [dbo].[CV3EnumReference]
(
	[SiteID] [smallint] NULL,
	[RepFlags] [tinyint] NULL,
	[Build] [int] NOT NULL,
	[TouchedBy] [varchar](50) NULL,
	[TouchedWhen] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedWhen] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[GUID] [dbo].[HVCIDdt] NOT NULL,
	[TableName] [varchar](50) NULL,
	[ColumnName] [varchar](30) NULL,
	[EnumValue] [int] NOT NULL,
	[ReferenceString] [varchar](100) NULL,
	[MSrepl_tran_version] [uniqueidentifier] NULL,
 CONSTRAINT [CV3EnumReferencePK] PRIMARY KEY NONCLUSTERED 
(
	[GUID] ASC
)
)
