﻿CREATE TABLE [dbo].[CV3Order] (
    [SiteID]                     SMALLINT         DEFAULT  NULL,
    [RepFlags]                   TINYINT          DEFAULT ((0)) NULL,
    [Build]                      INT              NOT NULL,
    [TouchedBy]                  VARCHAR (50)     NULL,
    [TouchedWhen]                DATETIME         DEFAULT (getdate()) NULL,
    [CreatedBy]                  VARCHAR (50)     NULL,
    [CreatedWhen]                DATETIME         DEFAULT (getdate()) NULL,
    [Active]                     BIT              NOT NULL,
    [GUID]                       [dbo].[HVCIDdt]  NOT NULL,
    [Entered]                    DATETIME         NULL,
    [Status]                     VARCHAR (10)     NULL,
    [ToBeVerified]               BIT              NOT NULL,
    [ToBeSigned]                 BIT              NOT NULL,
    [EnterRole]                  VARCHAR (30)     NULL,
    [UserGUID]                   [dbo].[HVCIDdt]  NULL,
    [ClientVisitGUID]            [dbo].[HVCIDdt]  NULL,
    [ClientGUID]                 [dbo].[HVCIDdt]  NULL,
    [ChartGUID]                  [dbo].[HVCIDdt]  NULL,
    [Name]                       VARCHAR (125)    NULL,
    [Modifier]                   VARCHAR (125)    NULL,
    [RequestedDtm]               DATETIME         NULL,
    [RequestedDate]              VARCHAR (20)     NULL,
    [RequestedTime]              VARCHAR (30)     NULL,
    [StopDtm]                    DATETIME         NULL,
    [StopDate]                   VARCHAR (20)     NULL,
    [StopTime]                   VARCHAR (30)     NULL,
    [Duration]                   INT              NOT NULL,
    [IsDurationChangeable]       BIT              NOT NULL,
    [PerformedDtm]               DATETIME         NULL,
    [SignificantDtm]             DATETIME         NULL,
    [SignificantTime]            VARCHAR (30)     NULL,
    [ScheduledDtm]               DATETIME         NULL,
    [AncillaryReferenceCode]     VARCHAR (75)     NULL,
    [SummaryLine]                VARCHAR (2000)   NULL,
    [IDCode]                     CHAR (9)         NULL,
    [TypeCode]                   VARCHAR (15)     NULL,
    [ApplicSource]               VARCHAR (5)      NULL,
    [IsForDischarge]             BIT              NOT NULL,
    [HasBeenModified]            BIT              NOT NULL,
    [IsConditional]              BIT              NOT NULL,
    [ConditionsText]             VARCHAR (255)    NULL,
    [IsHeld]                     BIT              NOT NULL,
    [HoldReasonText]             VARCHAR (255)    NULL,
    [IsAutoActivatable]          BIT              NOT NULL,
    [ActivateDateReference]      VARCHAR (30)     NULL,
    [ActivateDaysBefore]         INT              NOT NULL,
    [ActivateHoursBefore]        INT              NOT NULL,
    [ActivateStatus]             VARCHAR (5)      NULL,
    [MinimumStatusForActivation] VARCHAR (5)      NULL,
    [IsAutoCompletable]          BIT              NOT NULL,
    [CompleteDateReference]      VARCHAR (30)     NULL,
    [CompleteDaysAfter]          INT              NOT NULL,
    [CompleteHoursAfter]         INT              NOT NULL,
    [CompleteStatus]             VARCHAR (5)      NULL,
    [MinimumStatusForCompletion] VARCHAR (5)      NULL,
    [IsCompleteTemplate]         BIT              NOT NULL,
    [TranspMethodCode]           VARCHAR (30)     NULL,
    [FrequencyCode]              VARCHAR (30)     NULL,
    [SourceCode]                 VARCHAR (30)     NULL,
    [OrderPriorityCode]          VARCHAR (30)     NULL,
    [SystemOrderPriorityCode]    VARCHAR (10)     NULL,
    [OrderStatusCode]            VARCHAR (5)      NULL,
    [OrderStatusLevelNum]        INT              NOT NULL,
    [IsPRN]                      BIT              NOT NULL,
    [IsSuspended]                BIT              NOT NULL,
    [IsPartOfSet]                BIT              NOT NULL,
    [SequenceNum]                INT              NOT NULL,
    [OrderSetName]               VARCHAR (125)    NULL,
    [OrderSetHeading]            VARCHAR (30)     NULL,
    [OrderSetGUID]               [dbo].[HVCIDdt]  NULL,
    [CareProviderGUID]           [dbo].[HVCIDdt]  NULL,
    [OrderEntryFormGUID]         [dbo].[HVCIDdt]  NULL,
    [OrderCatalogMasterItemGUID] [dbo].[HVCIDdt]  NULL,
    [IsIncluded]                 BIT              NOT NULL,
    [PathwayColumnHeaderGUID]    [dbo].[HVCIDdt]  NULL,
    [PathwayRowHeaderGUID]       [dbo].[HVCIDdt]  NULL,
    [OrderSetType]               INT              NOT NULL,
    [IsTrackVariance]            BIT              NOT NULL,
    [IsGenericSet]               BIT              NOT NULL,
    [SubSequenceNum]             INT              NOT NULL,
    [ModifyUserGUID]             [dbo].[HVCIDdt]  NULL,
    [ArrivalDtm]                 DATETIME         NULL,
    [ModifiedDtm]                DATETIME         NULL,
    [ReqCodedTime]               VARCHAR (30)     NULL,
    [ReqTimeValue]               INT              NULL,
    [ReqTimeUom]                 VARCHAR (30)     NULL,
    [ReqTimeEventModifier]       INT              NULL,
    [ReqTimeEventCode]           VARCHAR (30)     NULL,
    [VarianceType]               INT              NULL,
    [RepeatOrder]                INT              NULL,
    [IsAutoGenerated]            TINYINT          NULL,
    [OrderTemplateGUID]          [dbo].[HVCIDdt]  NULL,
    [IsApplyProtection]          TINYINT          NULL,
    [MSrepl_tran_version]        UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    [IsOrderSet]                 BIT              NULL,
    [IsAddedGenericSet]          TINYINT          NULL,
    [OtherSessionType]           INT              NULL,
    [IsRequired]                 BIT              NULL,
    [CanBeModified]              BIT              NULL,
    [DiscCancelGroupNum]         TINYINT          NULL,
    [SuspendGroupNum]            TINYINT          NULL,
    [VerifyGroupNum]             TINYINT          NULL,
    [ReleaseGroupNum]            TINYINT          NULL,
    [ActivateGroupNum]           TINYINT          NULL,
    [OrderSubsetName]            VARCHAR (125)    NULL,
    [OrderSubsetGUID]            [dbo].[HVCIDdt]  NULL,
    [OrderSubsetType]            INT              NULL,
    [WorklistRefItemGroupNum]    TINYINT          NULL,
    [TaskRescheduleGroupNum]     TINYINT          NULL,
    [TaskGroupNum]               TINYINT          NULL,
    [ReviewDtm]                  DATETIME         NULL,
    [ComplexOrderType]           INT              NULL,
    [IsLinkedForDiscCancel]      BIT              NULL,
    [IsLinkedForSuspend]         BIT              NULL,
    [CanBeSuspended]             BIT              NULL,
    [AlternateOrderType]         INT              DEFAULT ((0)) NULL,
    [InitialSessionTypeCode]     VARCHAR (30)     NULL,
    [MSReplrowguid]              UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [CV3OrderPK] PRIMARY KEY NONCLUSTERED ([GUID] ASC)
);


GO
ALTER TABLE [dbo].[CV3Order] SET (LOCK_ESCALATION = DISABLE);


GO
CREATE CLUSTERED INDEX [CV3OrderCI]
    ON [dbo].[CV3Order]([ClientGUID] ASC, [ChartGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [OrderAlt1Idx]
    ON [dbo].[CV3Order]([IDCode] ASC);


GO
CREATE NONCLUSTERED INDEX [CV3OrderAI02]
    ON [dbo].[CV3Order]([OrderCatalogMasterItemGUID] ASC, [OrderStatusLevelNum] ASC);


GO
CREATE NONCLUSTERED INDEX [CV3OrderAI03]
    ON [dbo].[CV3Order]([CareProviderGUID] ASC, [Entered] ASC);


GO
CREATE STATISTICS [_WA_Sys_00000010_595DA207]
    ON [dbo].[CV3Order]([ClientVisitGUID]);


GO
CREATE STATISTICS [_WA_Sys_0000000F_595DA207]
    ON [dbo].[CV3Order]([UserGUID]);


GO
CREATE STATISTICS [_WA_Sys_00000012_595DA207]
    ON [dbo].[CV3Order]([ChartGUID]);


GO
CREATE STATISTICS [_WA_Sys_00000013_595DA207]
    ON [dbo].[CV3Order]([Name]);


GO
CREATE STATISTICS [_WA_Sys_0000003E_595DA207]
    ON [dbo].[CV3Order]([OrderStatusCode]);


GO
CREATE STATISTICS [_WA_Sys_0000001D_595DA207]
    ON [dbo].[CV3Order]([PerformedDtm]);


GO
CREATE STATISTICS [_WA_Sys_00000024_595DA207]
    ON [dbo].[CV3Order]([TypeCode]);


GO
 