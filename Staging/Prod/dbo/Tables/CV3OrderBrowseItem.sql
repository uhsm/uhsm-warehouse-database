﻿CREATE TABLE [dbo].[CV3OrderBrowseItem] (
    [SiteID]              SMALLINT         DEFAULT  NULL,
    [RepFlags]            TINYINT          DEFAULT ((0)) NULL,
    [Build]               INT              NOT NULL,
    [TouchedBy]           VARCHAR (50)     NULL,
    [TouchedWhen]         DATETIME         DEFAULT (getdate()) NULL,
    [CreatedBy]           VARCHAR (50)     NULL,
    [CreatedWhen]         DATETIME         DEFAULT (getdate()) NULL,
    [Active]              BIT              NOT NULL,
    [GUID]                [dbo].[HVCIDdt]  NOT NULL,
    [IsLeafNode]          BIT              NOT NULL,
    [Hierarchy]           VARCHAR (255)    NULL,
    [ParentGUID]          [dbo].[HVCIDdt]  NULL,
    [Name]                VARCHAR (294)    NULL,
    [IsForOrderEntry]     BIT              NOT NULL,
    [IsForOrderReview]    BIT              NOT NULL,
    [IsInformational]     BIT              NOT NULL,
    [InfoMessage]         VARCHAR (255)    NULL,
    [OrderItemGUID]       [dbo].[HVCIDdt]  NULL,
    [IsOrderCatalogSet]   BIT              NOT NULL,
    [OrderCatalogSetType] INT              NOT NULL,
    [MSrepl_tran_version] UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    CONSTRAINT [CV3OrderBrowseItemPK] PRIMARY KEY NONCLUSTERED ([GUID] ASC)
);


GO
CREATE CLUSTERED INDEX [OrderBrowseItemClustIdx]
    ON [dbo].[CV3OrderBrowseItem]([ParentGUID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [OrderBrowseItemAlt0Idx]
    ON [dbo].[CV3OrderBrowseItem]([OrderItemGUID] ASC, [Name] ASC, [ParentGUID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [CV3OrderBrowseItemAI01]
    ON [dbo].[CV3OrderBrowseItem]([Hierarchy] ASC, [IsForOrderReview] ASC, [IsLeafNode] ASC, [Active] ASC, [GUID] ASC);

