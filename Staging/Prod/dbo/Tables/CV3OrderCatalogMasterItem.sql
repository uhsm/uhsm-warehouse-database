﻿CREATE TABLE [dbo].[CV3OrderCatalogMasterItem] (
    [SiteID]                             SMALLINT         DEFAULT  NULL,
    [RepFlags]                           TINYINT          DEFAULT ((0)) NULL,
    [Build]                              INT              NOT NULL,
    [TouchedBy]                          VARCHAR (50)     NULL,
    [TouchedWhen]                        DATETIME         DEFAULT (getdate()) NULL,
    [CreatedBy]                          VARCHAR (50)     NULL,
    [CreatedWhen]                        DATETIME         DEFAULT (getdate()) NULL,
    [Active]                             BIT              NOT NULL,
    [GUID]                               [dbo].[HVCIDdt]  NOT NULL,
    [Name]                               VARCHAR (125)    NULL,
    [Description]                        VARCHAR (40)     NULL,
    [EffectiveDate]                      DATETIME         NULL,
    [ExpiryDate]                         DATETIME         NULL,
    [AncillaryName]                      VARCHAR (125)    NULL,
    [AncillaryCodingStd]                 VARCHAR (60)     NULL,
    [IsCompletable]                      BIT              NOT NULL,
    [IsRepeatable]                       BIT              NOT NULL,
    [Cost]                               VARCHAR (10)     NULL,
    [CostUnitOfMeasure]                  VARCHAR (20)     NULL,
    [Duration]                           INT              NOT NULL,
    [IsDurationChangeable]               BIT              NOT NULL,
    [PrintClass]                         VARCHAR (30)     NULL,
    [SpecimenType]                       VARCHAR (40)     NULL,
    [IsAutoActivatable]                  BIT              NOT NULL,
    [ActivateStatus]                     VARCHAR (40)     NULL,
    [MinimumStatusForActivation]         VARCHAR (40)     NULL,
    [ActivateDateReference]              VARCHAR (40)     NULL,
    [ActivateDaysBefore]                 INT              NOT NULL,
    [ActivateHoursBefore]                INT              NOT NULL,
    [IsAutoCompletable]                  BIT              NOT NULL,
    [CompleteStatus]                     VARCHAR (40)     NULL,
    [MinimumStatusForCompletion]         VARCHAR (40)     NULL,
    [CompleteDateReference]              VARCHAR (40)     NULL,
    [CompleteDaysAfter]                  INT              NOT NULL,
    [CompleteHoursAfter]                 INT              NOT NULL,
    [ItemInfoID]                         INT              NOT NULL,
    [CodedTimeGroupCode]                 VARCHAR (40)     NULL,
    [VerificationPolicyCode]             VARCHAR (30)     NULL,
    [OrgUnitGUID]                        [dbo].[HVCIDdt]  NULL,
    [TransmitPolicyGUID]                 [dbo].[HVCIDdt]  NULL,
    [EntryFormGUID]                      [dbo].[HVCIDdt]  NULL,
    [OrderReviewCategoryGUID]            [dbo].[HVCIDdt]  NULL,
    [IsIVAdditive]                       BIT              NOT NULL,
    [CanBeSuspended]                     BIT              NOT NULL,
    [AutoCompleteType]                   INT              NOT NULL,
    [IsOngoingOrder]                     BIT              NOT NULL,
    [DuplicatePolicyGUID]                [dbo].[HVCIDdt]  NULL,
    [PatientCareDocumentGUID]            [dbo].[HVCIDdt]  NULL,
    [TaskEffectiveDate]                  DATETIME         NULL,
    [TaskExpirationDate]                 DATETIME         NULL,
    [CodedFreqGroupGUID]                 [dbo].[HVCIDdt]  NULL,
    [MaxRepeatsAllowed]                  INT              NULL,
    [OverrideMaxRepeats]                 TINYINT          NULL,
    [AdvanceDaysRepeat]                  INT              NULL,
    [MinAdvanceOrdersRepeat]             INT              NULL,
    [IsPrimaryHidden]                    TINYINT          NULL,
    [IsBaseSolution]                     TINYINT          NULL,
    [WarnMsgDisplayType]                 INT              DEFAULT ((0)) NOT NULL,
    [CanBeConditional]                   BIT              DEFAULT ((1)) NOT NULL,
    [MSrepl_tran_version]                UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    [IsDurationExtendable]               BIT              DEFAULT ((0)) NOT NULL,
    [AllowAutomaticStopOrder]            BIT              DEFAULT ((0)) NOT NULL,
    [RelevantInfoPolicyGUID]             [dbo].[HVCIDdt]  NULL,
    [DoseBasisGroupGUID]                 [dbo].[HVCIDdt]  NULL,
    [IsLMRPQueryItem]                    BIT              DEFAULT ((0)) NOT NULL,
    [OrderAvailPolicyGUID]               [dbo].[HVCIDdt]  NULL,
    [RxVerifyPolicyGUID]                 [dbo].[HVCIDdt]  NULL,
    [MedAdminWarningPolicyGUID]          [dbo].[HVCIDdt]  NULL,
    [IsLargeVolumeParenteral]            BIT              DEFAULT ((0)) NOT NULL,
    [ReviewInterval]                     INT              DEFAULT ((0)) NOT NULL,
    [ReviewIntervalType]                 INT              NULL,
    [RelativeReviewInDays]               INT              DEFAULT ((0)) NOT NULL,
    [IsReviewDateChangeable]             BIT              DEFAULT ((0)) NOT NULL,
    [AutoExtendStopDateType]             INT              DEFAULT ((0)) NOT NULL,
    [IsInformational]                    BIT              DEFAULT ((0)) NOT NULL,
    [DosingOptionGroupGUID]              [dbo].[HVCIDdt]  NULL,
    [PRNReasonDictionaryType]            INT              DEFAULT ((0)) NOT NULL,
    [PRNReasonDictionaryName]            VARCHAR (30)     NULL,
    [StopAfterTimesDisplayType]          INT              DEFAULT ((0)) NOT NULL,
    [TherapeuticCategory]                VARCHAR (255)    NULL,
    [TherapeuticCategoryID]              INT              NULL,
    [TransferAutoStatusOrderPolicyGUID]  [dbo].[HVCIDdt]  NULL,
    [DischargeAutoStatusOrderPolicyGUID] [dbo].[HVCIDdt]  NULL,
    [IsUnmappedPrescriptionItem]         BIT              DEFAULT ((0)) NOT NULL,
    [AlternateMappingName]               VARCHAR (255)    NULL,
    [RxOrderReviewPolicyID]              INT              DEFAULT ((0)) NOT NULL,
    [RxDispensingPolicyID]               INT              NULL,
    [DrcGrouperID]                       INT              NULL,
    CONSTRAINT [CV3OrderCatalogMasterItemPK] PRIMARY KEY CLUSTERED ([GUID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [CV3OrderCatalogMasterItemAI01]
    ON [dbo].[CV3OrderCatalogMasterItem]([OrderAvailPolicyGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [CV3OrderCatalogMasterItemAI02]
    ON [dbo].[CV3OrderCatalogMasterItem]([IsUnmappedPrescriptionItem] ASC);


GO
CREATE STATISTICS [_WA_Sys_0000002A_4D0CD9BB]
    ON [dbo].[CV3OrderCatalogMasterItem]([OrderReviewCategoryGUID]);


GO
CREATE STATISTICS [_WA_Sys_0000000A_4D0CD9BB]
    ON [dbo].[CV3OrderCatalogMasterItem]([Name]);


GO
