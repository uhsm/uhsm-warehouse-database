﻿CREATE TABLE [dbo].[CV3OrderEntryForm] (
    [SiteID]                   SMALLINT         DEFAULT  NULL,
    [RepFlags]                 TINYINT          DEFAULT ((0)) NULL,
    [Build]                    INT              NOT NULL,
    [TouchedBy]                VARCHAR (50)     NULL,
    [TouchedWhen]              DATETIME         DEFAULT (getdate()) NULL,
    [CreatedBy]                VARCHAR (50)     NULL,
    [CreatedWhen]              DATETIME         DEFAULT (getdate()) NULL,
    [Active]                   BIT              NOT NULL,
    [GUID]                     [dbo].[HVCIDdt]  NOT NULL,
    [Name]                     VARCHAR (20)     NULL,
    [Title]                    VARCHAR (60)     NULL,
    [Description]              VARCHAR (40)     NULL,
    [IsCurrent]                BIT              NOT NULL,
    [DisplayForm]              BIT              NOT NULL,
    [FormGUID]                 [dbo].[HVCIDdt]  NULL,
    [OrderTypeCode]            VARCHAR (30)     NULL,
    [Type]                     INT              NOT NULL,
    [MSrepl_tran_version]      UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    [NumColumns]               INT              DEFAULT ((1)) NOT NULL,
    [ShowEmptyRows]            BIT              DEFAULT ((0)) NOT NULL,
    [LabelWidth]               INT              DEFAULT ((20)) NOT NULL,
    [OnOrderCreateMLM]         VARCHAR (80)     NULL,
    [OnOrderSaveMLM]           VARCHAR (80)     NULL,
    [DisplayFormFromFlowsheet] BIT              DEFAULT ((0)) NOT NULL,
    [IsPrintAllowed]           BIT              DEFAULT ((0)) NOT NULL,
    [IsSXADefined]             BIT              DEFAULT ((0)) NOT NULL,
    [TabName]                  VARCHAR (20)     NULL,
    [IsSupplementalTab]        BIT              CONSTRAINT [CV3OrderEntryFormIsSupplementalTabDF] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [CV3OrderEntryFormPK] PRIMARY KEY NONCLUSTERED ([GUID] ASC)
);


GO
CREATE CLUSTERED INDEX [CV3OrderEntryFormCI]
    ON [dbo].[CV3OrderEntryForm]([FormGUID] ASC, [IsCurrent] ASC);


GO
CREATE NONCLUSTERED INDEX [CV3OrderEntryFormAI01]
    ON [dbo].[CV3OrderEntryForm]([Name] ASC);

