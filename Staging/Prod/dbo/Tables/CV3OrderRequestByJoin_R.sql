﻿CREATE TABLE [dbo].[CV3OrderRequestByJoin_R]
(
	[JobID] [int] NOT NULL,
	[ObjectName] [varchar](30) NULL,
	[ObjectGUID] [dbo].[HVCIDdt] NULL,
	[SubmittedReportGUID] [dbo].[HVCIDdt] NULL,
	[TextLine] [varchar](max) NULL,
	[OrderRequestByJoin_RID] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[MSReplrowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [CV3OrderRequestByJoin_RPK] PRIMARY KEY NONCLUSTERED 
(
	[OrderRequestByJoin_RID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

