﻿CREATE TABLE [dbo].[CV3OrderReviewCategory] (
    [SiteID]              SMALLINT         DEFAULT  NULL,
    [RepFlags]            TINYINT          DEFAULT ((0)) NULL,
    [Build]               INT              NOT NULL,
    [TouchedBy]           VARCHAR (50)     NULL,
    [TouchedWhen]         DATETIME         DEFAULT (getdate()) NULL,
    [CreatedBy]           VARCHAR (50)     NULL,
    [CreatedWhen]         DATETIME         DEFAULT (getdate()) NULL,
    [Active]              BIT              NOT NULL,
    [GUID]                [dbo].[HVCIDdt]  NOT NULL,
    [Code]                VARCHAR (30)     NULL,
    [SortCode]            VARCHAR (10)     NULL,
    [MSrepl_tran_version] UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    CONSTRAINT [CV3OrderReviewCategoryPK] PRIMARY KEY CLUSTERED ([GUID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [CV3OrderReviewCategoryAI01]
    ON [dbo].[CV3OrderReviewCategory]([Code] ASC);

