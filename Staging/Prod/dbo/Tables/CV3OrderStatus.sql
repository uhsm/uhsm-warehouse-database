﻿CREATE TABLE [dbo].[CV3OrderStatus](
	[SiteID] [smallint] NULL,
	[RepFlags] [tinyint] NULL,
	[Build] [int] NOT NULL,
	[TouchedBy] [varchar](50) NULL,
	[TouchedWhen] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedWhen] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[GUID] [dbo].[HVCIDdt] NOT NULL,
	[Code] [varchar](30) NULL,
	[Description] [varchar](60) NULL,
	[LevelNumber] [int] NOT NULL,
	[MSrepl_tran_version] [uniqueidentifier] NULL,
	[StatusGroup] [int] NOT NULL,
 CONSTRAINT [CV3OrderStatusPK] PRIMARY KEY NONCLUSTERED 
(
	[GUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

