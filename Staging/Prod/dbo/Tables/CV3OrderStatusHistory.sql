﻿CREATE TABLE [dbo].[CV3OrderStatusHistory]
(
	[SiteID] [smallint] NULL,
	[RepFlags] [tinyint] NULL,
	[Build] [int] NOT NULL,
	[TouchedBy] [varchar](50) NULL,
	[TouchedWhen] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedWhen] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[GUID] [dbo].[HVCIDdt] NOT NULL,
	[OrderStatusCode] [varchar](5) NULL,
	[ReasonText] [varchar](255) NULL,
	[CreatedByDisplayName] [varchar](60) NULL,
	[OrderGUID] [dbo].[HVCIDdt] NULL,
	[UserGUID] [dbo].[HVCIDdt] NULL,
	[ClientGUID] [dbo].[HVCIDdt] NULL,
	[RequestedByGUID] [dbo].[HVCIDdt] NULL,
	[SourceCode] [varchar](30) NULL,
	[ToBeSigned] [int] NULL,
	[ArrivalDtm] [datetime] NULL,
	[OrderMaintIDCode] [int] NULL,
	[OrderMaintFunctionCode] [varchar](30) NULL,
	[ReqByDisplayName] [varchar](60) NULL,
	[OrderMaintSummaryLine] [varchar](2000) NULL,
	[FunctionCodeNum] [int] NULL,
	[MSrepl_tran_version] [uniqueidentifier] NULL,
	[MSReplrowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [CV3OrderStatusHistoryPK] PRIMARY KEY NONCLUSTERED 
(
	[GUID] ASC
)

)
