﻿CREATE TABLE [dbo].[CV3OrderUserData] (
    [SiteID]              SMALLINT         DEFAULT  NULL,
    [RepFlags]            TINYINT          DEFAULT ((0)) NULL,
    [Build]               INT              NOT NULL,
    [TouchedBy]           VARCHAR (50)     NULL,
    [TouchedWhen]         DATETIME         DEFAULT (getdate()) NULL,
    [CreatedBy]           VARCHAR (50)     NULL,
    [CreatedWhen]         DATETIME         DEFAULT (getdate()) NULL,
    [Active]              BIT              NOT NULL,
    [GUID]                [dbo].[HVCIDdt]  NOT NULL,
    [UserDataCode]        VARCHAR (40)     NULL,
    [Value]               VARCHAR (2000)   NULL,
    [IsTemplate]          BIT              NOT NULL,
    [OrderGUID]           [dbo].[HVCIDdt]  NULL,
    [ClientGUID]          [dbo].[HVCIDdt]  NULL,
    [MSrepl_tran_version] UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    [MSReplrowguid]       UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [CV3OrderUserDataPK] PRIMARY KEY NONCLUSTERED ([GUID] ASC)
);


GO
ALTER TABLE [dbo].[CV3OrderUserData] SET (LOCK_ESCALATION = DISABLE);


GO
CREATE CLUSTERED INDEX [OrderUserDataClustIdx]
    ON [dbo].[CV3OrderUserData]([ClientGUID] ASC, [OrderGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [CV3OrderUserDataAI01]
    ON [dbo].[CV3OrderUserData]([OrderGUID] ASC);


GO
