﻿CREATE TABLE [dbo].[CV3OrganizationalUnit]
(
	[SiteID] [smallint] NULL,
	[RepFlags] [tinyint] NULL,
	[Build] [int] NOT NULL,
	[TouchedBy] [varchar](50) NULL,
	[TouchedWhen] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedWhen] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[GUID] [dbo].[HVCIDdt] NOT NULL,
	[Code] [varchar](215) NULL,
	[LevelCode] [varchar](15) NULL,
	[Name] [varchar](40) NULL,
	[Description] [varchar](40) NULL,
	[ParentGUID] [dbo].[HVCIDdt] NULL,
	[CanOwnChart] [bit] NOT NULL,
	[CanOwnRequestItem] [bit] NOT NULL,
	[CanOwnPatientCareDocs] [bit] NOT NULL,
	[CanOwnObservations] [tinyint] NULL,
	[MSrepl_tran_version] [uniqueidentifier] NULL,
 CONSTRAINT [CV3OrganizationalUnitPK] PRIMARY KEY NONCLUSTERED 
(
	[GUID] ASC
)
)
