﻿CREATE TABLE [dbo].[CV3Phone] (
    [SiteID]              SMALLINT         DEFAULT  NULL,
    [RepFlags]            TINYINT          DEFAULT ((0)) NULL,
    [Build]               INT              NOT NULL,
    [TouchedBy]           VARCHAR (50)     NULL,
    [TouchedWhen]         DATETIME         DEFAULT (getdate()) NULL,
    [CreatedBy]           VARCHAR (50)     NULL,
    [CreatedWhen]         DATETIME         DEFAULT (getdate()) NULL,
    [Active]              BIT              NOT NULL,
    [GUID]                [dbo].[HVCIDdt]  NOT NULL,
    [PhoneNumber]         VARCHAR (40)     NULL,
    [PhoneNote]           VARCHAR (60)     NULL,
    [PhoneType]           VARCHAR (15)     NULL,
    [AreaCode]            VARCHAR (10)     NULL,
    [PersonGUID]          [dbo].[HVCIDdt]  NULL,
    [ClientVisitGUID]     [dbo].[HVCIDdt]  NULL,
    [ApplicSource]        CHAR (5)         NULL,
    [ScopeLevel]          CHAR (1)         NULL,
    [MSrepl_tran_version] UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    [Extension]           VARCHAR (20)     NULL,
    [MSReplrowguid]       UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [CV3PhonePK] PRIMARY KEY NONCLUSTERED ([GUID] ASC)
);


GO
CREATE CLUSTERED INDEX [PhoneClustIdx]
    ON [dbo].[CV3Phone]([PersonGUID] ASC);


GO
