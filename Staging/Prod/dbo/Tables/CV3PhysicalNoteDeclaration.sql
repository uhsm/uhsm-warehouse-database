﻿CREATE TABLE [dbo].[CV3PhysicalNoteDeclaration] (
    [SiteID]               SMALLINT         DEFAULT  NULL,
    [RepFlags]             TINYINT          DEFAULT ((0)) NULL,
    [Build]                INT              NOT NULL,
    [TouchedBy]            VARCHAR (50)     NULL,
    [TouchedWhen]          DATETIME         DEFAULT (getdate()) NULL,
    [CreatedBy]            VARCHAR (50)     NULL,
    [CreatedWhen]          DATETIME         DEFAULT (getdate()) NULL,
    [Active]               BIT              NOT NULL,
    [GUID]                 [dbo].[HVCIDdt]  NOT NULL,
    [Entered]              DATETIME         NULL,
    [Status]               VARCHAR (10)     NULL,
    [ToBeVerified]         BIT              NOT NULL,
    [ToBeSigned]           BIT              NOT NULL,
    [EnterRole]            VARCHAR (30)     NULL,
    [UserGUID]             [dbo].[HVCIDdt]  NULL,
    [ClientVisitGUID]      [dbo].[HVCIDdt]  NULL,
    [ClientGUID]           [dbo].[HVCIDdt]  NULL,
    [ChartGUID]            [dbo].[HVCIDdt]  NULL,
    [IsChronic]            BIT              NOT NULL,
    [ScopeLevel]           CHAR (1)         NULL,
    [Description]          VARCHAR (60)     NULL,
    [Text]                 VARCHAR (255)    NULL,
    [OnsetDayNum]          INT              NOT NULL,
    [OnsetMonthNum]        INT              NOT NULL,
    [OnsetYearNum]         INT              NOT NULL,
    [ResolvedDate]         DATETIME         NULL,
    [CharacteristicNumber] CHAR (4)         NULL,
    [ApplicSource]         CHAR (5)         NULL,
    [TypeCode]             VARCHAR (15)     NULL,
    [MSrepl_tran_version]  UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    [CoreUOM]              VARCHAR (30)     NULL,
    [MSReplrowguid]        UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [CV3PhysicalNoteDeclarationPK] PRIMARY KEY NONCLUSTERED ([GUID] ASC)
);


GO
ALTER TABLE [dbo].[CV3PhysicalNoteDeclaration] SET (LOCK_ESCALATION = DISABLE);


GO
