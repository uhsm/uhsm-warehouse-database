﻿CREATE TABLE [dbo].[CV3ResultCatalogItem](
	[SiteID] [smallint] NULL,
	[RepFlags] [tinyint] NULL,
	[Build] [int] NOT NULL,
	[TouchedBy] [varchar](50) NULL,
	[TouchedWhen] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedWhen] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[GUID] [dbo].[HVCIDdt] NOT NULL,
	[ItemName] [varchar](125) NULL,
	[Description] [varchar](40) NULL,
	[FormatStyle] [varchar](2) NULL,
	[AncillaryName] [varchar](125) NULL,
	[AncillaryCodingStd] [varchar](60) NULL,
	[SelectComponent] [bit] NOT NULL,
	[AuditTrail] [bit] NOT NULL,
	[Battery] [bit] NOT NULL,
	[RestrictedAccess] [bit] NOT NULL,
	[OrderMasterItemGUID] [dbo].[HVCIDdt] NULL,
	[EffectiveDate] [datetime] NULL,
	[ExpiryDate] [datetime] NULL,
	[OrgUnitGUID] [dbo].[HVCIDdt] NULL,
	[ItemInfoID] [int] NOT NULL,
	[ResultCategoriesGUID] [dbo].[HVCIDdt] NULL,
	[CategoryDisplaySeqNum] [int] NOT NULL,
	[IsComponent] [bit] NOT NULL,
	[MSrepl_tran_version] [uniqueidentifier] NULL,
	[ReleaseToPatientPolicyGUID] [dbo].[HVCIDdt] NULL,
 CONSTRAINT [CV3ResultCatalogItemPK] PRIMARY KEY NONCLUSTERED 
(
	[GUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[CV3ResultCatalogItem] ADD  DEFAULT ((0)) FOR [RepFlags]
GO

ALTER TABLE [dbo].[CV3ResultCatalogItem] ADD  DEFAULT (getdate()) FOR [TouchedWhen]
GO

ALTER TABLE [dbo].[CV3ResultCatalogItem] ADD  DEFAULT (getdate()) FOR [CreatedWhen]
GO

ALTER TABLE [dbo].[CV3ResultCatalogItem] ADD  DEFAULT (newid()) FOR [MSrepl_tran_version]
GO
