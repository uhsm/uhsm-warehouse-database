﻿CREATE TABLE [dbo].[CV3ResultComponent](
	[SiteID] [smallint] NULL,
	[RepFlags] [tinyint] NULL,
	[Build] [int] NOT NULL,
	[TouchedBy] [varchar](50) NULL,
	[TouchedWhen] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedWhen] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[GUID] [dbo].[HVCIDdt] NOT NULL,
	[DisplaySequence] [int] NOT NULL,
	[ItemParentGUID] [dbo].[HVCIDdt] NULL,
	[ItemChildGUID] [dbo].[HVCIDdt] NULL,
	[MSrepl_tran_version] [uniqueidentifier] NULL,
 CONSTRAINT [CV3ResultComponentPK] PRIMARY KEY NONCLUSTERED 
(
	[GUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[CV3ResultComponent] ADD  DEFAULT ((0)) FOR [RepFlags]
GO

ALTER TABLE [dbo].[CV3ResultComponent] ADD  DEFAULT (getdate()) FOR [TouchedWhen]
GO

ALTER TABLE [dbo].[CV3ResultComponent] ADD  DEFAULT (getdate()) FOR [CreatedWhen]
GO

ALTER TABLE [dbo].[CV3ResultComponent] ADD  DEFAULT (newid()) FOR [MSrepl_tran_version]
GO

