﻿

CREATE TABLE [dbo].[CV3SendOrderQueue](
	[SiteID] [smallint] NULL,
	[RepFlags] [tinyint] NULL,
	[Build] [int] NOT NULL,
	[TouchedBy] [varchar](50) NULL,
	[TouchedWhen] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedWhen] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[GUID] [dbo].[HVCIDdt] NOT NULL,
	[ClientID] [varchar](20) NULL,
	[MessageIDCode] [varchar](20) NULL,
	[AppMnemReceiver] [varchar](3) NULL,
	[ComsPrcsMnemReceiver] [varchar](30) NULL,
	[StatusCode] [varchar](4) NULL,
	[SentAt] [datetime] NULL,
	[IDCode] [varchar](9) NULL,
	[SendAfter] [datetime] NULL,
	[SendBefore] [datetime] NULL,
	[IntfStatusCode] [varchar](2) NULL,
	[Priority] [int] NOT NULL,
	[OrderGroup] [int] NOT NULL,
	[TaskOccurrenceGUID] [dbo].[HVCIDdt] NULL,
	[OrderStatusHistoryGUID] [dbo].[HVCIDdt] NULL,
	[MSrepl_tran_version] [uniqueidentifier] NULL,
	[HVCTransaction] [varchar](max) NULL,
	[MSReplrowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [CV3SendOrderQueuePK] PRIMARY KEY NONCLUSTERED 
(
	[GUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


