﻿CREATE TABLE [dbo].[CV3Service](
	[SiteID] [smallint] NULL,
	[RepFlags] [tinyint] NULL,
	[Build] [int] NOT NULL,
	[TouchedBy] [varchar](50) NULL,
	[TouchedWhen] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedWhen] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[GUID] [dbo].[HVCIDdt] NOT NULL,
	[GroupCode] [varchar](20) NULL,
	[SubGroupCode] [varchar](20) NULL,
	[Description] [varchar](60) NULL,
	[MSrepl_tran_version] [uniqueidentifier] NULL,
 CONSTRAINT [CV3ServicePK] PRIMARY KEY NONCLUSTERED 
(
	[GUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
