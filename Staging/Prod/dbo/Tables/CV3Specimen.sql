﻿CREATE TABLE [dbo].[CV3Specimen]
(
	[SiteID] [smallint] NULL,
	[RepFlags] [tinyint] NULL,
	[Build] [int] NOT NULL,
	[TouchedBy] [varchar](50) NULL,
	[TouchedWhen] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedWhen] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[GUID] [dbo].[HVCIDdt] NOT NULL,
	[Entered] [datetime] NULL,
	[Status] [varchar](10) NULL,
	[ToBeVerified] [bit] NOT NULL,
	[ToBeSigned] [bit] NOT NULL,
	[EnterRole] [varchar](30) NULL,
	[UserGUID] [dbo].[HVCIDdt] NULL,
	[ClientVisitGUID] [dbo].[HVCIDdt] NULL,
	[ClientGUID] [dbo].[HVCIDdt] NULL,
	[ChartGUID] [dbo].[HVCIDdt] NULL,
	[AncillaryReferenceCode] [varchar](75) NULL,
	[SpecimenComment] [varchar](255) NULL,
	[PersonNameCollect] [varchar](40) NULL,
	[Volume] [varchar](20) NULL,
	[Uom] [varchar](30) NULL,
	[IDCode] [varchar](10) NULL,
	[CollectionStartDtm] [datetime] NULL,
	[CollectionStopDtm] [datetime] NULL,
	[SpecimenTypeCode] [varchar](30) NULL,
	[AncillaryFacilityID] [varchar](12) NULL,
	[MSrepl_tran_version] [uniqueidentifier] NULL,
	[MSReplrowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [CV3SpecimenPK] PRIMARY KEY NONCLUSTERED 
(
	[GUID] ASC
)
) 