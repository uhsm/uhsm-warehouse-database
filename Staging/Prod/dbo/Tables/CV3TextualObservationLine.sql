﻿CREATE TABLE [dbo].[CV3TextualObservationLine] (
    [SiteID]              SMALLINT         DEFAULT  NULL,
    [RepFlags]            TINYINT          DEFAULT ((0)) NULL,
    [Build]               INT              NOT NULL,
    [TouchedBy]           VARCHAR (50)     NULL,
    [TouchedWhen]         DATETIME         DEFAULT (getdate()) NULL,
    [CreatedBy]           VARCHAR (50)     NULL,
    [CreatedWhen]         DATETIME         DEFAULT (getdate()) NULL,
    [Active]              BIT              NOT NULL,
    [GUID]                [dbo].[HVCIDdt]  NOT NULL,
    [Text]                VARCHAR (255)    NULL,
    [LineNum]             INT              NOT NULL,
    [ObservationGUID]     [dbo].[HVCIDdt]  NULL,
    [ClientGUID]          [dbo].[HVCIDdt]  NULL,
    [MSrepl_tran_version] UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    [MSReplrowguid]       UNIQUEIDENTIFIER DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [CV3TextualObservationLinePK] PRIMARY KEY NONCLUSTERED ([GUID] ASC)
);


GO
ALTER TABLE [dbo].[CV3TextualObservationLine] SET (LOCK_ESCALATION = DISABLE);


GO
CREATE UNIQUE CLUSTERED INDEX [TextualObservationLineClustIdx]
    ON [dbo].[CV3TextualObservationLine]([ClientGUID] ASC, [ObservationGUID] ASC, [LineNum] ASC);


GO
