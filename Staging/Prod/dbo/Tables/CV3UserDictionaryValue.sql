﻿CREATE TABLE [dbo].[CV3UserDictionaryValue]
(
	[SiteID] [smallint] NULL,
	[RepFlags] [tinyint] NULL,
	[Build] [int] NOT NULL,
	[TouchedBy] [varchar](50) NULL,
	[TouchedWhen] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedWhen] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[GUID] [dbo].[HVCIDdt] NOT NULL,
	[UserDictionaryCode] [varchar](30) NULL,
	[Value] [varchar](2000) NULL,
	[MSrepl_tran_version] [uniqueidentifier] NULL,
	[SortSeqNum] [int] NOT NULL,
 CONSTRAINT [CV3UserDictionaryValuePK] PRIMARY KEY NONCLUSTERED 
(
	[GUID] ASC
)
)
