﻿CREATE TABLE [dbo].[HVCEnvProfile]
(
	[SiteID] [smallint] NULL,
	[RepFlags] [tinyint] NULL,
	[Build] [int] NOT NULL,
	[TouchedBy] [varchar](50) NULL,
	[TouchedWhen] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedWhen] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[GUID] [dbo].[HVCIDdt] NOT NULL,
	[IsLeafNode] [bit] NOT NULL,
	[Hierarchy] [varchar](255) NULL,
	[ParentGUID] [dbo].[HVCIDdt] NULL,
	[ApplicationID] [varchar](32) NULL,
	[Code] [varchar](50) NULL,
	[HierarchyCode] [varchar](255) NULL,
	[DisplaySequence] [int] NOT NULL,
	[Value] [varchar](255) NULL,
	[HVCValue] [varchar](255) NULL,
	[Description] [varchar](255) NULL,
	[IsHVCDefined] [bit] NOT NULL,
	[MSrepl_tran_version] [uniqueidentifier] NULL,
 CONSTRAINT [HVCEnvProfilePK] PRIMARY KEY NONCLUSTERED 
(
	[GUID] ASC
)
)
