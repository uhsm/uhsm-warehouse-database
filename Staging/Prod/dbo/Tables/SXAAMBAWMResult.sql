﻿CREATE TABLE [dbo].[SXAAMBAWMResult]
(
[Build] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedWhen] [datetime] NOT NULL,
	[UserGUID] [dbo].[HVCIDdt] NOT NULL,
	[OrderResultsID] [bigint] NOT NULL,
	[OrderGUID] [dbo].[HVCIDdt] NOT NULL,
	[CanViewOnly] [bit] NOT NULL,
	[IsOrderResultsViewed] [bit] NOT NULL,
	[MSRowVersion] [timestamp] NOT NULL,
	[MSReplrowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [SXAAMBAWMResultPK] PRIMARY KEY CLUSTERED 
(
	[OrderGUID] ASC,
	[UserGUID] ASC
)
)
