﻿CREATE TABLE [dbo].[SXAAMCareProviderSpecialtyXREF](
	[Build] [int] NOT NULL,
	[TouchedBy] [varchar](50) NOT NULL,
	[TouchedWhenUTC] [datetimeoffset](3) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedWhenUTC] [datetimeoffset](3) NOT NULL,
	[CareProviderSpecialtyXREFID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CareProviderGUID] [dbo].[HVCIDdt] NOT NULL,
	[DisciplineGUID] [dbo].[HVCIDdt] NOT NULL,
	[IsDefault] [bit] NOT NULL,
	[MSRowVersion] [timestamp] NOT NULL,
	[SequenceNumber] [int] NOT NULL,
 CONSTRAINT [SXAAMCareProviderSpecialtyXREFPK] PRIMARY KEY NONCLUSTERED 
 (
	[CareProviderSpecialtyXREFID] ASC
)) 
