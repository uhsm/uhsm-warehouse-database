﻿
CREATE TABLE [dbo].[SXADBEnvironmentInformation](
	[TouchedBy] [varchar](50) NOT NULL,
	[TouchedWhen] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedWhen] [datetime] NOT NULL,
	[AttributeName] [varchar](30) NOT NULL,
	[AttributeValue] [varchar](2000) NULL,
 CONSTRAINT [SXADBEnvironmentInformationPK] PRIMARY KEY CLUSTERED 
(
	[AttributeName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) 
