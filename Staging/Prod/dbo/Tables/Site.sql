﻿CREATE TABLE [dbo].[Site](
	[SiteID] [smallint] NOT NULL,
	[MatSchedule] [int] NULL,
	[FinSchedule] [int] NULL,
	[MatStarvationMinutes] [int] NULL,
	[MatZombieMinutes] [int] NULL,
	[FinStarvationMinutes] [int] NULL,
	[FinZombieMinutes] [int] NULL,
	[MaxConcurrentMatSync] [int] NULL,
	[MaxConcurrentFinSync] [int] NULL,
	[MaxConcurrentMatAsync] [int] NULL,
	[MaxConcurrentFinAsync] [int] NULL,
	[DDBLockDistributionLag] [int] NULL,
	[DDBLockZombieMinutes] [int] NULL,
	[MatFinParallel] [int] NULL,
	[MergeRAMSyncTime] [int] NULL,
	[MergeHangTime] [int] NULL,
	[MSReplrowguid] [uniqueidentifier] ROWGUIDCOL  NULL,
 CONSTRAINT [SitePK] PRIMARY KEY CLUSTERED 
(
	[SiteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)