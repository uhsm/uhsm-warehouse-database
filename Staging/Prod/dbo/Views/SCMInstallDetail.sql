﻿
Create VIEW [dbo].[SCMInstallDetail] AS 
    SELECT  CAST(0 as smallint) as SiteID
	      , c.Build 
	      , c.TouchedBy
	      , c.TouchedWhen
	      , 'SQL' as ServerType
	      , c.PreviousBuild
	      , c.RootSQLServerName as DataServer
	      , ( SELECT AttributeValue FROM SXADBEnvironmentInformation WHERE AttributeName = 'EnterpriseSQLServerName') as MasterDataServer
	      , c.ComponentDbName as DbName
	      ,( SELECT AttributeValue FROM SXADBEnvironmentInformation WHERE AttributeName = 'SXAScriptsDirectory') as ScriptsDirectory
	      , c.MSrepl_tran_version 
    FROM SXADBApplicationInformation c
    WHERE c.AppID = 1 
