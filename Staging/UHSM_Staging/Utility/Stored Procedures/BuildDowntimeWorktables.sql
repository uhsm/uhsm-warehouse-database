﻿CREATE PROCEDURE [Utility].[BuildDowntimeWorktables]
	@Jobs int output
AS

--generate active visits work table
truncate table dbo.CV3VisitListJoin_R

insert
into
	dbo.CV3VisitListJoin_R
	(
	JobID
	,ObjectGUID
	,AdditionalReportInfo
	)
select
	JobID =
		dense_rank()
		over(
			order by
				case
				when CV3ActiveVisitList.TypeCode in ('Emergency','Outpatient')
				then CV3ActiveVisitList.TypeCode

				else cast(CV3ActiveVisitList.CurrentLocationGUID as varchar)
				end
		)

	,ObjectGUID = CV3ActiveVisitList.GUID

	,AdditionalReportInfo =
		case
		when CV3ActiveVisitList.TypeCode in ('Emergency','Outpatient')
		then CV3ActiveVisitList.TypeCode

		else cast(CV3ActiveVisitList.CurrentLocationGUID as varchar)
		end

from
	[$(EPR)].dbo.CV3ActiveVisitList
where
	CV3ActiveVisitList.VisitStatus = 'ADM'
and CV3ActiveVisitList.Active = 1
and	CV3ActiveVisitList.TypeCode in ('Inpatient', 'Emergency','Outpatient')
and	CV3ActiveVisitList.IDCode not In ('999999','999998','999997','999996')


select
	@Jobs = max(JobID)
from
	dbo.CV3VisitListJoin_R


--generate orders work table
truncate table dbo.CV3Order_R

insert
into
	dbo.CV3Order_R
	(
	JobID
	,ObjectGUID
	)
select distinct
	JobID = CV3VisitListJoin_R.JobID
	,ObjectGUID = CV3Order.GUID
from
	dbo.CV3VisitListJoin_R

left join [$(EPR)].dbo.CV3Order
on	CV3Order.ClientVisitGUID = CV3VisitListJoin_R.ObjectGUID


RETURN 0

