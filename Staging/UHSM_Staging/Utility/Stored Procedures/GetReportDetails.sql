﻿CREATE PROCEDURE [Utility].[GetReportDetails]
	@ReportCode varchar(255)
as

select
	ReportFilename = Report.Report

	,ReportOutputPath = ReportOutputPath.TextValue + '\<LocationCode>'

	,rsCommandArguments =
		replace(
			' -e Exec2005' +
			' -s "' + Report.ReportServerUrl + '"' +
			' -i "' + rssFile.TextValue + '"' +
			' -v vFormat="PDF"' +
			' -v vFullPathOfOutputFile="' + ReportOutputPath.TextValue + '\<LocationCode>\' + Report.Report + '<TerminalDigit>.pdf"' +
			' -v vReportPath="' + Report.ReportPath + '/' + Report.ReportCode + '"' +
			' -v JobID="<JobID>"' +
	--		' -v TerminalDigit="<TerminalDigit>"' +
			' ' + Report.ReportParameters

			,'<ServerDateTime>'
			,convert(
				varchar
				,getdate()
			)
		)
from
	Utility.Report

inner join Utility.Parameter ReportOutputPath
on	ReportOutputPath.Parameter = 'ReportOutputPath'

inner join Utility.Parameter rssFile
on	rssFile.Parameter = 'rssFile'

where
	Report.ReportCode = @ReportCode


return 0

