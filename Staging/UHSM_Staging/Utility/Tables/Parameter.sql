﻿CREATE TABLE [Utility].[Parameter] (
    [Parameter]    VARCHAR (128)   NOT NULL,
    [TextValue]    VARCHAR (255)   NULL,
    [NumericValue] DECIMAL (18, 5) NULL,
    [DateValue]    DATETIME        NULL,
    [BooleanValue] BIT             NULL,
    [Created] DATETIME NOT NULL DEFAULT getdate(), 
    [Updated] DATETIME NULL, 
    [ByWhom] VARCHAR(255) NOT NULL DEFAULT suser_name(), 
    CONSTRAINT [PK_Parameter] PRIMARY KEY CLUSTERED ([Parameter] ASC)
);
