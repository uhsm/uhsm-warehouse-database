﻿CREATE TABLE [Utility].[Report]
(
	ReportCode varchar(255) not null
	,Report varchar(255) not null
	,ReportServerUrl varchar(255) not null
	,ReportPath varchar(255) not null
	,ReportParameters varchar(255) NULL
    ,CONSTRAINT [PK_Report] PRIMARY KEY ([ReportCode])
)
