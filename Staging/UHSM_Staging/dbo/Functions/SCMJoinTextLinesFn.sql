﻿CREATE FUNCTION [dbo].[SCMJoinTextLinesFn]
/*'*******************************************************************************
'Copyright (c) 2002 Eclipsys Technologies Corporation. All Rights Reserved.    '
'                                                                              '
'  PROPRIETARY NOTICE: This software has been provided pursuant to a           '
'  License Agreement that contains restrictions on its use. This software      '
'  contains valuable trade secrets and proprietary information of              '
'  Eclipsys Technologies Corporation and is protected by Federal               '
'  copyright law.  It may not be copied or distributed in any form or medium,  '
'  disclosed to third parties, or used in any manner that is not provided      '
'  for in said License Agreement, except with the prior written                '
'  authorization of Eclipsys Technologies Corporation.                         '
'                                                                              '
'******************************************************************************'
*/
(@TableName varchar(50),
@ParentGUID [numeric](16, 0),
@ClientGUID [numeric](16, 0))
RETURNS VARCHAR(8000)
AS
--===================================================================
-- Function:  SCMJoinTextLinesFn
-- Description: Return the join text lines for a given patient.
--
-- Note: This function currently supports two scenarios: Data gather from CV3TextualObservationLine
--       and data gather from CV3AlertMessage.
-- Retuns a varchar(8000) with the concatenated entered text from the tables involved.
--
-- History:
-- Date         Author    Description
-- 04/02/2002   crussi       Initial Revision
--===================================================================

BEGIN
	DECLARE 
		@TextLine VARCHAR(8000),
		@TextResult VARCHAR(8000)
	
	IF @TableName = 'CV3TextualObservationLine'
	BEGIN
		DECLARE TextCursor CURSOR FOR
		SELECT Text FROM [$(EPR)].dbo.CV3TextualObservationLine
		WHERE ObservationGUID = @ParentGUID
		AND ClientGUID = @ClientGUID
		ORDER BY LineNum
		SET @TextResult = ''
		OPEN TextCursor
		FETCH TextCursor INTO @TextLine
		WHILE @@FETCH_STATUS = 0 
		BEGIN
			SET @TextResult = @TextResult + ISNULL(@TextLine,'')
			FETCH TextCursor INTO @TextLine
		END
		DEALLOCATE TextCursor
	END
	ELSE IF @TableName = 'CV3AlertMessage'
	BEGIN
		DECLARE TextCursor CURSOR FOR
		SELECT TextLine FROM [$(EPR)].dbo.CV3AlertMessage
		WHERE ParentGUID = @ParentGUID
		AND ClientGUID = @ClientGUID
		ORDER BY LineNumber
		SET @TextResult = ''
		OPEN TextCursor
		FETCH TextCursor INTO @TextLine
		WHILE @@FETCH_STATUS = 0 
		BEGIN
			SET @TextResult = @TextResult + ISNULL(@TextLine,'')
			FETCH TextCursor INTO @TextLine
		END
		DEALLOCATE TextCursor
	END
	RETURN(@TextResult)
END
