﻿CREATE FUNCTION [dbo].[UHSM_fn_AddressAsSingleString]
(
	@Address1 varchar(250),
	@Address2 varchar(250),
	@Address3 varchar(250),
	@Address4 varchar(250)
	)
	Returns varchar(1000)
As

Begin

	Declare @Address varchar(1000)

	-- Initialise
	Set @Address = ''

	-- Add Address1
	Set @Address = @Address + Coalesce(@Address1,'')

	-- Add Address2
	If @Address = '' And Coalesce(@Address2,'') <> ''
	Begin
		Set @Address = @Address + @Address2
	End

	Else If @Address <> '' And Coalesce(@Address2,'') <> ''
	Begin
		Set @Address = @Address + ', ' + @Address2
	End

	-- Add Address3
	If @Address = '' And Coalesce(@Address3,'') <> ''
	Begin
		Set @Address = @Address + @Address3
	End

	Else If @Address <> '' And Coalesce(@Address3,'') <> ''
	Begin
		Set @Address = @Address + ', ' + @Address3
	End

	-- Add Address4
	If @Address = '' And Coalesce(@Address4,'') <> ''
	Begin
		Set @Address = @Address + @Address4
	End

	Else If @Address <> '' And Coalesce(@Address4,'') <> ''
	Begin
		Set @Address = @Address + ', ' + @Address4
	End

	If @Address = ''
	Begin 
		Set @Address = Null
	End

	MyFinish:
	Return (@Address)

End
