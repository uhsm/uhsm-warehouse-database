﻿CREATE FUNCTION [dbo].[UHSM_fn_AgeAsInteger](@DOB datetime,@ReferenceDate datetime)
	Returns int
As

Begin

	Declare @intAge int
	
	If @DOB Is Null Or @ReferenceDate Is Null
		Begin
			Set @intAge = Null
			Goto MyFinish
		End

	Set @intAge = DateDiff(yy,@DOB,@ReferenceDate) -
		Case
			When @ReferenceDate < DateAdd(yy,DateDiff(yy,@DOB,@ReferenceDate),@DOB) Then 1
			Else 0
		End

	MyFinish:
	Return(@intAge)

End

