﻿/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:	Returns the second word in a string

Notes:		Stored in DWREPORTING.LIB

Versions:
		1.0.0.0 - 28/07/2010 - MT - CRI# 1837.4824
			Created function.
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/
Create Function dbo.fn_ExtractSecondWord(@Input varchar(4000))
	Returns varchar(4000)
As

Begin

	Declare @Output varchar(4000)

	Set @Output = dbo.fn_TrimAndNull(@Input)

	If @Output Is Null
	Begin
		Goto MyFinish
	End

	-- Get rid of the first word and space
	Set @Output = dbo.fn_ReturnCharsAfterFirstSpecifiedChar(@Output,' ')

	-- Get rid of any remaining text after the second word
	Set @Output = dbo.fn_ReturnCharsUpToFirstSpecifiedChar(@Output,' ')


	MyFinish:
	Return(@Output)

End