﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:	Get just the date part of any given datetime.

Notes:		Stored in DW_REPORTING.

Versions:
			1.0.0.0 - 09/10/2015 - MT
				Created function
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
Create Function dbo.fn_GetJustDate(@MyDate as datetime)
	Returns date
As

Begin
	Declare @JustDate date
	Set @JustDate = convert(datetime,floor(convert(float,@MyDate)))

	MyFinish:
	Return(@JustDate)

End