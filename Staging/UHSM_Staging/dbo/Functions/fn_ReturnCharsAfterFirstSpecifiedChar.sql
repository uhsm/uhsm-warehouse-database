﻿/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:	Return characters in a string after a specified character.

Notes:		If the specified character does not appear in the string then it returns Null.

Versions:

		1.0.0.2 - 08/11/2010 - ML - CRI# 2169.5280
			Alter logic so that it returns the original string if there are no occurrences of Specified Char in that string
		

		1.0.0.1 - 28/07/2010 - MT - CRI# 1837.4825
			Make the function return Null if the specified character is
			not in the string.  Previously returned the given string.

		1.0.0.0 - 08/06/2005 - ML
			Created function.
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/
Create Function dbo.fn_ReturnCharsAfterFirstSpecifiedChar(@strInputString varchar(4000), @strSpecifiedChar varchar(1))

	Returns varchar(4000)

As

Begin

	Declare @int int
	Declare @Output varchar(4000)
	Set @int = CharIndex(@strSpecifiedChar, @strInputString)

	-- This will happen if the string contains nothing characters after the first Specified Char
	If @int > len(@strInputString)
	Begin
		Set @Output = left(@strInputString, len(@strInputString))
	End

	If @int = 0
	Begin
		Set @Output = Null
	End
	Else
	Begin
		If @int > len(@strInputString)
		Begin
			Set @Output = left(@strInputString, len(@strInputString))
		End
		Else
		Begin	
			Set @Output = SubString(@strInputString, @int + 1, Len(@strInputString) - @int)
		End
	End

	Return(@Output)
End