﻿-- Author:  Michael Lysons
-- Date:    08/06/2005
-- Purpose: Takes an input string and returns a substring
--          which is all the characters up to the first Specified Character
--
--          e.g. an input of 'MKL 123', with a specified character of '1'
--               will return 'MKL 1'
CReate Function [dbo].[fn_ReturnCharsUpToFirstSpecifiedChar](@strInputString varchar(200), @strSpecifiedChar varchar(1) )

	Returns varchar(200)

As

Begin

	Declare @int int

	Set @int = CharIndex(@strSpecifiedChar, @strInputString, 1) - 1
	If @int < 0
		Set @int = Len(@strInputString)

	Return SubString(@strInputString, 1, @int)

End