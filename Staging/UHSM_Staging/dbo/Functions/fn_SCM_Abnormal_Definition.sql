﻿/*
----------------------------------------------------------------------------------------------
Purpose:	SCM - Convert CV3BasicObservation.AbnormalCode into definition.

Notes:		Stored in SM2SCM-CPM1.UHSM_Staging

Versions:
			1.0.0.0 - 24/11/2016 - MT
				Created function.
----------------------------------------------------------------------------------------------
*/
Create Function dbo.fn_SCM_Abnormal_Definition(@AbnormalityCode varchar(10))
	Returns varchar(20)
As

Begin

	Declare @Definition varchar(1000)

	Set @Definition = 
		Case
			When @AbnormalityCode = 'A' Then 'Abnormal'
			When @AbnormalityCode = 'AA' Then 'Very abnormal'
			When @AbnormalityCode = 'H' Then 'High'
			When @AbnormalityCode = 'HH' Then 'Very high'
			When @AbnormalityCode = 'L' Then 'Low'
			When @AbnormalityCode = 'LL' Then 'Very low'
			When @AbnormalityCode = 'N' Then 'Normal'
			When @AbnormalityCode = 'U' Then 'Unknown'
			Else 'Unknown'
		End

	MyFinish:
	Return (@Definition)

End