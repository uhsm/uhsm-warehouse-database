﻿CREATE FUNCTION [dbo].[fn_SCM_DOB]
(
@BirthYearNum int,@BirthMonthNum int,@BirthDayNum int
)
	Returns date
As

Begin

	Declare @DOB date

	If @BirthYearNum = 0 Or @BirthMonthNum = 0 Or @BirthDayNum = 0
	Begin
		Set @DOB = Null
		Goto MyFinish
	End

	Set @DOB = DATEFROMPARTS(@BirthYearNum,@BirthMonthNum,@BirthDayNum)

	MyFinish:
	Return (@DOB)

End
