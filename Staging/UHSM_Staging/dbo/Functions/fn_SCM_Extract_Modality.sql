﻿/*
----------------------------------------------------------------------------------------------
Purpose:	SCM - Extract Modality from Order Name

Notes:		Stored in SM2SCM-CPM1.UHSM_Staging

Versions:
			1.0.0.0 - 23/11/2016 - MT
				Created function.
----------------------------------------------------------------------------------------------
*/
CREATE Function dbo.fn_SCM_Extract_Modality(@OrderName varchar(500),@CatalogMasterItemName varchar(500))
	Returns varchar(50)
As

Begin

	Declare @Modality varchar(1000)

	-- Initialise
	Set @Modality = dbo.fn_ExtractSecondWord(@OrderName)

	-- Exceptions 1
	Set @Modality = 
		Case
			When @Modality = 'Bro' Then 'Bronchoscopy'
			When @Modality Like 'Fluor%' Then 'Fluoroscopy'
			When @Modality Like 'Inter%' Then 'Interventional'
			When @Modality Like 'Temp%' Then 'Other'
			When @Modality Is Null Then 'Other'
			When @Modality = '' Then 'Other'
			Else @Modality
		End

	-- Exceptions 2
	Set @Modality = 
		Case
			When @Modality = 'Other' And @CatalogMasterItemName Like '%Fluor%' Then 'Fluoroscopy'
			Else @Modality
		End

	MyFinish:
	Return (@Modality)

End