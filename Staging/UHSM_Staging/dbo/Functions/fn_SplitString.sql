﻿/*
------------------------------------------------------------------------------------------------------------------------
Purpose:	Split a delimited string into a table with records and return the table

Notes:		Originally Created on 20/12/2006, but never had a Change Control item for it or a Function SQL file
		This version created in 2014 so we have a file and a record in Change Control

Versions:
		1.0.0.0 - 22/01/2014 - ML - CRI# 4494
			Send email to NHS Net that becomes an SMS
------------------------------------------------------------------------------------------------------------------------
*/
Create Function dbo.fn_SplitString(@strInputString varchar(8000), @strDelimiter varchar(1))
	Returns @tblSplitValues Table(SplitValue varchar(8000))
As

-- This function splits the string in @strInputString
-- based on the delimiter in @strDelimiter
-- The value returned is a table with one row for each delimited value
-- e.g. @strInputString = '1;2;3;4'
--      @strDelimiter   = ';'
--      @tblSplitValues = 1
--                      = 2
--                      = 3
--                      = 4

Begin
	Declare @strTemp        varchar(8000)
	Declare @strTempValue   varchar(8000)
	Declare @intPosition    int

	Set @strTemp = @strInputString 

	-- Find the first position of a Delimiter in the input string
	Set @intPosition = CharIndex(@strDelimiter, @strTemp)

	-- If we find a Delimiter we want to strip out the value
	While @intPosition > 0
	Begin
		-- Strip out the value preceding the Delimiter
		Set @strTempValue = SubString(@strTemp, 1, @intPosition - 1)

		-- If it's not an empty string then add it to the Table
		If Len(@strTempValue) > 0
		Begin
			Insert @tblSplitValues(SplitValue)
			Values(@strTempValue)
		End
	
		-- Remove the Value and Delimiter from the Temp string
		Set @strTemp = SubString(@strTemp, @intPosition + 1, Len(@strTemp))

		-- Find the position of the next Delimiter in the shortened Temp string
		Set @intPosition = CharIndex(@strDelimiter, @strTemp)
	End 
	
	-- 0 means no Delimiter was found, so this could be the last value in the list
	If @intPosition = 0
	Begin
		-- If it's not an empty string then it must be a Value so add it to the Table
		If Len(@strTemp) > 0
		Begin
			Insert @tblSplitValues(SplitValue)
			Values(@strTemp)	
		End
	End
	
	Return
End