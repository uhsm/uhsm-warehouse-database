﻿/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose: Returns AE Patient Type for the supplied code.

Notes:	 Discharge Patient Type is entered into the Customer Defined Screen (CDS) called
	 Review Clinic which is Page 2 of the A&E Assessment screen.

Versions:
	1.0.0.1 - 16/04/2008 - MT - CRI# 628.1762
		Removed stepping thru each character to eliminate control codes
		in favour of just LTrim, RTrim and Null.

	1.0.0.0 - 20/02/2003 - MT
		Created function.
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

Create Function dbo.fn_TrimAndNull(@strInput varchar(500))
	Returns varchar(500)
As

Begin

	Declare @strOutput varchar(500)

	Set @strOutput = LTrim(RTrim(@strInput))

	If @strOutput = ''
		Begin
			Set @strOutput = Null
		End


	Return(@strOutput)

End