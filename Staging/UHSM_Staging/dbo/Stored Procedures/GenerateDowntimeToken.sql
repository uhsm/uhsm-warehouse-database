﻿CREATE PROCEDURE [Utility].[GenerateDowntimeToken] @Path varchar(100)

AS


DECLARE @Text AS VARCHAR(100)
DECLARE @Cmd AS VARCHAR(100)
SET @Text = ''
SET @Cmd ='echo ' +  @Text + ' > ' + @Path 
EXECUTE master.dbo.xp_cmdshell  @Cmd


RETURN 0
