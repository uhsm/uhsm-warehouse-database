﻿CREATE PROCEDURE [dbo].[LocationListPhlebotomy]
AS

select
	 Value = Location.GUID
	,Label = Location.Name
from
	[$(EPR)].dbo.CV3Location Location
where
	exists
	(
	select
		1
	from
		[$(EPR)].dbo.CV3ClientVisit Visit

	inner join [$(EPR)].dbo.CV3Order
	on	CV3Order.ClientVisitGUID = Visit.GUID

	inner join [$(EPR)].dbo.CV3OrderCatalogMasterItem OrderItem
	on	OrderItem.GUID = CV3Order.OrderCatalogMasterItemGUID

	where
		Visit.CurrentLocationGUID = Location.GUID
	and	OrderItem.SpecimenType = 'Blood'
	and	CV3Order.OrderStatusCode = 'PCOL'
	)


union all

select
	 Value = 0
	,Label = '<All>'

order by
	Value
