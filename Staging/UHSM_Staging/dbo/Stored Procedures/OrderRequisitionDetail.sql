﻿CREATE PROCEDURE [dbo].[OrderRequisitionDetail]
	@OrderGUID numeric(16,0)
As

select
	 CV3OrderEntryField.Label

	,Value =
		case
		when CV3DataItem.DataTypeCode = 'Checkbox' and CV3OrderUserData.Value = 1 Then 'Yes'
		when CV3DataItem.DataTypeCode = 'Checkbox' and CV3OrderUserData.Value = 0 Then 'No'
		else CV3OrderUserData.Value
		end
from
	[$(EPR)].dbo.CV3AllOrdersVw CV3Order

inner join [$(EPR)].dbo.CV3OrderEntryField CV3OrderEntryField
on	CV3OrderEntryField.OrderEntryFormGUID = CV3Order.OrderEntryFormGUID

inner join [$(EPR)].dbo.CV3DataItem CV3DataItem
on	CV3DataItem.Code = CV3OrderEntryField.DataItemCode

left join [$(EPR)].dbo.CV3OrderUserData CV3OrderUserData
on CV3Order.GUID = CV3OrderUserData.OrderGUID
and CV3OrderUserData.UserDataCode = CV3OrderEntryField.DataItemCode

where
	CV3Order.GUID = @OrderGUID
and CV3OrderEntryField.IsVisible = 1
and CV3OrderEntryField.Label is not null

-- ExclCV3OrderUserDatae header items
and CV3OrderEntryField.DataItemCode not in
	(
	'RequestedDate'
	,'RequestedTime'	-- Priority
	,'TranspMethodCode'	-- Mobility
	)

order by
	CV3OrderEntryField.[Sequence]
	,CV3OrderEntryField.ColumnNumber
