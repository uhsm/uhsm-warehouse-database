
CREATE PROCEDURE [dbo].[OrderRequisitionHeader]
	@OrderGUID numeric(16,0)
as

Declare @WaitTime varchar(20)
Declare @KnownAs varchar(255)
Declare @OrderedAs varchar(255)
Declare @MinValueThousandSeparator as varchar(40)


-- Store the environment profile settings for the ordered as column.
Set @KnownAs =
	(Select [Value]
	From	[$(EPR)].dbo.HVCEnvProfile
	Where	IsHVCDefined = 1
			And IsLeafNode = 1
			And Code = 'DisplayPrefixKnownAs'
			And HierarchyCode = 'Orders')

Set @OrderedAs = 
	(Select [Value]
	From	[$(EPR)].dbo.HVCEnvProfile
	Where	IsHVCDefined = 1
			And IsLeafNode = 1
			And Code = 'DisplayPrefixOrderedAs'
			And HierarchyCode = 'Orders')

-- Retrieve environment profile orders|MinValueThousandSeparator
Set @MinValueThousandSeparator =
	(Select	[Value]
	From	[$(EPR)].dbo.hvcEnvProfile
	Where	IsHVCDefined = 1
			And IsLeafNode = 1
			And Code = 'MinValueThousandSeparator'
			And HierarchyCode = 'Orders')

;
with

Addr as
	(
	select
		Address1 = CV3Address.Line1
		,Address2 = CV3Address.Line2
		,Address3 = CV3Address.Line3
		,Address4 = CV3Address.City
		,Postcode = CV3Address.PostalCode
		,RowNo =
			ROW_NUMBER()
			Over
				(
				Order By
					CV3Address.TouchedWhen desc
				)
	from
		[$(EPR)].dbo.CV3AllOrdersVw CV3Order

	inner join [$(EPR)].dbo.CV3Address
	on	CV3Address.ParentGUID = CV3Order.ClientGUID

	where
		CV3Order.GUID = @OrderGUID
	and CV3Address.Active = 1
	and CV3Address.IsCurrent = 1
	and CV3Address.Status = 'Active'
	)

Select	
	Age = dbo.UHSM_fn_AgeAsInteger(DATEFROMPARTS(CV3Client.BirthYearNum,CV3Client.BirthMonthNum,CV3Client.BirthDayNum),GETDATE())
	,Consultant = CV3ClientVisit.ProviderDisplayName
	,CV3ClientVisit.CurrentLocation
	,DOB = DATEFROMPARTS(CV3Client.BirthYearNum,CV3Client.BirthMonthNum,CV3Client.BirthDayNum)
	,Mobility = CV3Order.TranspMethodCode
	,NHSNo = nhs.ClientIDCode
	,OrderName = CV3Order.[Name]
	,PatientAddress = dbo.UHSM_fn_AddressAsSingleString(
		addr.Address1
		,addr.Address2
		,addr.Address3
		,addr.Address4)
	,PatientName = CV3ClientVisit.ClientDisplayName
	,CV3Client.FirstName
	,CV3Client.LastName
	,addr.Postcode
	,[Priority] =
		Case
			When CV3CodedTime.IsEventBased = 1 Then
				RTrim(CV3Order.RequestedTime) + ' ' +
				Convert(varchar,ReqTimeValue) + ' ' +
				RTrim(ReqTimeUom) + ' ' +
				(SELECT ReferenceString from [$(EPR)].dbo.CV3EnumReference e
				WHERE ReqTimeEventModifier = e.EnumValue
				AND e.ColumnName = 'ReqTimeEventModifier'
				AND e.TableName = 'CV3Order') + ' ' +
				RTrim(ReqTimeEventCode)
			When CV3Order.ReqCodedTime Is Not Null Then
				CV3Order.RequestedTime +  ' (' +
				RTrim(CV3Order.ReqCodedTime) + ') '
			Else CV3Order.RequestedTime
		End
	,CV3Order.RequestedDtm
	,RM2Number = CV3ClientVisit.IDCode
	,Sex = CV3Client.GenderCode
	,OrderID = CV3Order.IDCode
	,CreatedByUser = CreatedByUser.DisplayName
	,VisitID = CV3ClientVisit.VisitIDCode
	,VisitTime = CV3ClientVisit.AdmitDtm
	,CV3ClientVisit.ProviderDisplayName
	,CV3Phone.PhoneNumber
	
from
	[$(EPR)].dbo.CV3AllOrdersVw CV3Order

Left Join [$(EPR)].dbo.CV3CodedTime
on	CV3CodedTime.[Name] = CV3Order.RequestedTime

Inner Join [$(EPR)].dbo.CV3ClientVisit
on	CV3ClientVisit.GUID = CV3Order.ClientVisitGUID

Inner Join [$(EPR)].dbo.CV3Client
On CV3ClientVisit.ClientGUID = CV3Client.GUID

Left Join Addr addr
On addr.RowNo = 1

Left Join [$(EPR)].dbo.CV3ClientID nhs
On CV3ClientVisit.ClientGUID = nhs.ClientGUID
And nhs.TypeCode = 'NHS Number'

LEFT JOIN [$(EPR)].dbo.CV3Phone
On CV3Phone.guid = CV3Client.GUID

left Join [$(EPR)].dbo.CV3User CreatedByUser
On CreatedByUser.IDCode = right(CV3Order.CreatedBy, len(CV3Order.CreatedBy) - charindex('_', CV3Order.CreatedBy))

Where
	CV3Order.GUID = @OrderGUID

