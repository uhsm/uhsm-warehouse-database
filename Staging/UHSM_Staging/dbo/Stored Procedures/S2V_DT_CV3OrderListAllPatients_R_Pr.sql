﻿CREATE PROCEDURE [dbo].[S2V_DT_CV3OrderListAllPatients_R_Pr]          
(          
  @JobID int          
)          
AS          
--  '*****************************************************************************************************************************************************************************    
                 
--*** Allscripts Disclaimer:                   
--  Client is responsible for all decisions, acts, and omissions of any persons in connection with the delivery of medical care or other services to any patients.                    
--  Before any Licensed Materials are placed into a live production environment, it is Client’s responsibility to review and test all Licensed Materials and associated                   
--  workflows and other content, as implemented, make independent decisions about system settings and configuration based upon Client’s needs, practices, standards and                   
--  environment, and reach its own independent determination that they are appropriate for such live production use.  Any such use by Client (or its Authorized Users)                   
--  will constitute Client’s representation that it has complied with the foregoing. Client shall ensure that all Authorized Users are appropriately trained in use of                   
--  the then-deployed release of the Software prior to their use of the Software in a live production environment.  Clinical Materials are tools to assist Authorized                   
--  Users in the delivery of medical care, but should not be viewed as prescriptive or authoritative. Clinical Materials are not a substitute for, and Client shall                   
--  ensure that each Authorized User applies in conjunction with the use thereof, independent professional medical judgment.  Clinical Materials are not designed for use,                   
--  and Client shall not use them, in any system that provides medical care without the participation of properly trained personnel.  Any live production use of Clinical                   
--  Materials by Client (or its Authorized Users) will constitute Client’s acceptance of clinical responsibility for the use of such materials.                  
                                                             
--  ' CREATED BY :  CD,AllScripts                                                            
--  ' Date   :  10.09.2013    
--  ' Description :  Downtime Order List Report                                                            
--  ' S2V_DT_CV3OrderListAllPatients_R_Pr 593000    
    
--  ' Modification History:    
--    
--  '****************************************************************************************************************************************************************************         
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED          
SET NOCount ON          

IF @JobID <>0  --created from SCM  
BEGIN
	SELECT   
		  CV3Order.ClientVisitGUID,  
		  CV3Order.Name,  
		  CV3Order.IDCode as OrderID,  
		  CV3Order.SummaryLine,  
		  CV3Order.SignificantDtm,  
		  CV3Order.SignificantTime,  
		  CV3Order.IsConditional,  
		  CV3Order.ConditionsText,  
		  CV3Order.RepeatOrder,  
		  CV3Order.ComplexOrderType,  
		  CV3Order.IsSuspended,  
		  CV3Order.Name as OrderStatusCode --needed the length to hold the spot  
	INTO #tmp_order  
	FROM [$(EPR)].dbo.CV3Order,CV3Order_R  
	WHERE 1 = 2  
  
	INSERT  INTO #tmp_order  
	SELECT  DISTINCT  
		  CV3Order.ClientVisitGUID,  
		  CV3Order.Name,  
		  CV3Order.IDCode as OrderID,  
		  CV3Order.SummaryLine,  
		  CV3Order.SignificantDtm,  
		  CV3Order.SignificantTime,  
		  CV3Order.IsConditional,  
		  CV3Order.ConditionsText,  
		  CV3Order.RepeatOrder,  
		  CV3Order.ComplexOrderType,  
		  CV3Order.IsSuspended,  
		  CV3OrderStatus.Description as OrderStatusCode  
	 FROM dbo.CV3Order_R
	 INNER JOIN [$(EPR)].dbo.CV3Order  
	 ON CV3Order_R.ObjectGUID = CV3Order.GUID  
           
	 INNER JOIN [$(EPR)].dbo.CV3OrderStatus  
	 ON CV3Order.OrderStatusCode = CV3OrderStatus.Code  
     
	 WHERE CV3Order_R.JobID = @JobID AND   
	 CV3OrderStatus.LevelNumber = 50  

  
	SELECT   
		  OrderName = IsNull(#tmp_order.Name, 'No Orders Entered'),  
		  OrderID = #tmp_order.OrderID,  
		  SummaryLine = #tmp_order.SummaryLine,  
		  OrderSignificantDtm = #tmp_order.SignificantDtm,  
		  OrderSignificantTime =  #tmp_order.SignificantTime,  
		  ClientVisitGUID = CV3ClientVisit.GUID,  
		  CV3ClientVisit.ClientDisplayName,  
		  CV3ClientVisit.CurrentLocation,  
		  CV3Client.BirthDayNum,  
		  CV3Client.BirthMonthNum,  
		  CV3Client.BirthYearNum,  
		  CV3Client.GenderCode,  
		  CV3ClientVisit.IDCode,   
		  CV3ClientVisit.VisitIDCode,  
		  ClientGUID = CV3Client.GUID,  
		  CV3Location.Name as Unit,  
		  IsConditional = #tmp_order.IsConditional,  
		  ConditionsText = #tmp_order.ConditionsText,  
		  RepeatOrder = #tmp_order.RepeatOrder,  
		  ComplexOrderType = isnull(#tmp_order.ComplexOrderType,0),  
		  IsSuspended = #tmp_order.IsSuspended,  
		  #tmp_order.OrderStatusCode  
	 FROM CV3VisitListJoin_R  
	 INNER JOIN [$(EPR)].dbo.CV3ActiveVisitList CV3ClientVisit  
	 ON (CV3VisitListJoin_R.ObjectGUID = CV3ClientVisit.GUID)  

	 INNER JOIN [$(EPR)].dbo.CV3Client  
	 ON (CV3ClientVisit.ClientGUID = CV3Client.GUID)  

	 INNER JOIN [$(EPR)].dbo.CV3Location  
	 ON CV3ClientVisit.CurrentLocationGUID = CV3Location.GUID  
	 
	 LEFT OUTER JOIN #tmp_order  
	 ON (CV3VisitListJoin_R.ObjectGUID = #tmp_order.ClientVisitGUID)  
	 WHERE  CV3VisitListJoin_R.JobID = @JobID   
  

 /******************************************************************************************************************  
 SSRS Version  
 *************************************************************************************************************************/   
END

ELSE  
IF @JobID = 0   
BEGIN  
	SELECT   
		  CV3Order.ClientVisitGUID,  
		  CV3Order.ClientGUID,  
		  CV3Order.ChartGUID,  
		  CV3Order.Name,  
		  CV3Order.IDCode as OrderID,  
		  CV3Order.SummaryLine,  
		  CV3Order.SignificantDtm,  
		  CV3Order.SignificantTime,  
		  CV3Order.IsConditional,  
		  CV3Order.ConditionsText,  
		  CV3Order.RepeatOrder,  
		  CV3Order.ComplexOrderType,  
		  CV3Order.IsSuspended,  
		  CV3Order.Name as OrderStatusCode --needed the length to hold the spot  
	 INTO #tmp_order2  
	 FROM [$(EPR)].dbo.CV3Order,CV3Order_R  
	 WHERE 1 = 2  
  
	INSERT  INTO #tmp_order2  
	SELECT DISTINCT  
		  CV3Order.ClientVisitGUID,  
		  CV3Order.ClientGUID,  
		  CV3Order.ChartGUID,  
		  CV3Order.Name,  
		  CV3Order.IDCode as OrderID,  
		  CV3Order.SummaryLine,  
		  CV3Order.SignificantDtm,  
		  CV3Order.SignificantTime,  
		  CV3Order.IsConditional,  
		  CV3Order.ConditionsText,  
		  CV3Order.RepeatOrder,  
		  CV3Order.ComplexOrderType,  
		  CV3Order.IsSuspended,  
		  CV3OrderStatus.Description as OrderStatusCode  
  
	 FROM  [$(EPR)].dbo.CV3Order  
	 INNER JOIN [$(EPR)].dbo.CV3ActiveVisitList  
	 ON CV3Order.ClientGUID = CV3ActiveVisitList.ClientGUID  
	 AND CV3Order.ChartGUID = CV3ActiveVisitList.ChartGUID  
	 AND CV3Order.ClientVisitGUID = CV3ActiveVisitList.GUID  
	 AND CV3ActiveVisitList.VisitStatus = 'ADM'   
	 AND CV3ActiveVisitList.Active = 1--index  

	 INNER JOIN [$(EPR)].dbo.CV3Location  
	 ON CV3ActiveVisitList.CurrentLocationGUID = CV3Location.GUID 
	  
	 INNER JOIN [$(EPR)].dbo.CV3OrderStatus  
	   ON CV3Order.OrderStatusCode = CV3OrderStatus.Code
	     
	 WHERE  
			  CV3OrderStatus.LevelNumber = 50 --Active only  


    
     
   CREATE NONCLUSTERED INDEX NC_IDX_tmp_order2 ON #tmp_order2 (ClientGUID,ChartGUID,ClientVisitGUID)  
     
	SELECT   
		  OrderName = IsNull(#tmp_order2.Name, 'No Orders Entered'),  
		  OrderID = #tmp_order2.OrderID,  
		  SummaryLine = #tmp_order2.SummaryLine,  
		  OrderSignificantDtm = #tmp_order2.SignificantDtm,  
		  OrderSignificantTime =  #tmp_order2.SignificantTime,  
		  ClientVisitGUID = CV3ClientVisit.GUID,  
		  CV3ClientVisit.ClientDisplayName,  
		  CV3ClientVisit.CurrentLocation,  
		  CV3Client.BirthDayNum,  
		  CV3Client.BirthMonthNum,  
		  CV3Client.BirthYearNum,  
		  CV3Client.GenderCode,  
		  CV3ClientVisit.IDCode,   
		  CV3ClientVisit.VisitIDCode,  
		  ClientGUID = CV3Client.GUID,  
		  CV3Location.Name as Unit,  
		  IsConditional = #tmp_order2.IsConditional,  
		  ConditionsText = #tmp_order2.ConditionsText,  
		  RepeatOrder = #tmp_order2.RepeatOrder,  
		  ComplexOrderType = isnull(#tmp_order2.ComplexOrderType,0),  
		  IsSuspended = #tmp_order2.IsSuspended,  
		  #tmp_order2.OrderStatusCode  
	 FROM   
		 [$(EPR)].dbo. CV3ActiveVisitList CV3ClientVisit  
     
	  INNER JOIN [$(EPR)].dbo.CV3Client  
	  ON (CV3ClientVisit.ClientGUID = CV3Client.GUID)  

	  INNER JOIN [$(EPR)].dbo.CV3Location  
	  ON CV3ClientVisit.CurrentLocationGUID = CV3Location.GUID  

	  LEFT OUTER JOIN #tmp_order2  
	  ON #tmp_order2.ClientGUID = CV3ClientVisit.ClientGUID  
	  AND #tmp_order2.ChartGUID = CV3ClientVisit.ChartGUID  
	  AND #tmp_order2.ClientVisitGUID = CV3ClientVisit.GUID  
	 
	 WHERE (CV3ClientVisit.VisitStatus = 'ADM' and CV3ClientVisit.Active = 1--index  
			AND CV3ClientVisit.TypeCode in ('Inpatient', 'Emergency'))
		OR   
			(CV3ClientVisit.VisitStatus = 'ADM' and CV3ClientVisit.Active = 1  
			AND CV3ClientVisit.TypeCode = 'Outpatient')
     
  
END