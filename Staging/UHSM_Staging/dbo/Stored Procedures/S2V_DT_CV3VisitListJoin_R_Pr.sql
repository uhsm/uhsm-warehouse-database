﻿Create PROCEDURE [dbo].[S2V_DT_CV3VisitListJoin_R_Pr]
(            
  @JobID int             
)             
AS            
--  '*****************************************************************************************************************************************************************************        
                     
--*** Allscripts Disclaimer:                       
--  Client is responsible for all decisions, acts, and omissions of any persons in connection with the delivery of medical care or other services to any patients.                        
--  Before any Licensed Materials are placed into a live production environment, it is Client’s responsibility to review and test all Licensed Materials and associated                       
--  workflows and other content, as implemented, make independent decisions about system settings and configuration based upon Client’s needs, practices, standards and                       
--  environment, and reach its own independent determination that they are appropriate for such live production use.  Any such use by Client (or its Authorized Users)                       
--  will constitute Client’s representation that it has complied with the foregoing. Client shall ensure that all Authorized Users are appropriately trained in use of                       
--  the then-deployed release of the Software prior to their use of the Software in a live production environment.  Clinical Materials are tools to assist Authorized                       
--  Users in the delivery of medical care, but should not be viewed as prescriptive or authoritative. Clinical Materials are not a substitute for, and Client shall                       
--  ensure that each Authorized User applies in conjunction with the use thereof, independent professional medical judgment.  Clinical Materials are not designed for use,                       
--  and Client shall not use them, in any system that provides medical care without the participation of properly trained personnel.  Any live production use of Clinical                       
--  Materials by Client (or its Authorized Users) will constitute Client’s acceptance of clinical responsibility for the use of such materials.                      
                                                                 
--  ' CREATED BY :  CD,AllScripts                                                                
--  ' Date   :  10.09.2013        
--  ' Description :  Patient List By Location Report                                                                
--  ' S2V_DT_CV3TaskWorklist_V2_R_Pr 593000        
        
--  ' Modification History:        
--        
--  '****************************************************************************************************************************************************************************          
SET NOCOUNT ON            
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  


IF @JobID <> 0  --created from SCM

	SELECT
		CodeNum = 5, 
		CV3ClientVisit.GUID,
		CV3Client.GUID AS ClientGUID,
		CV3ClientVisit.ChartGUID AS ChartGUID,
		CV3ClientVisit.ClientDisplayName,
		CV3ClientVisit.CurrentLocation,
		CV3ClientVisit.ProviderDisplayName,
		CV3ClientVisit.InternalVisitStatus,
		CV3Client.IsMaterialized,
		CV3Client.BirthYearNum,
		CV3Client.BirthMonthNum,
		CV3Client.BirthDayNum,
		CV3ClientVisit.IDCode as MRN,
		CV3ClientVisit.VisitIDCode as VisitID,
		CV3ClientVisit.AdmitDTM,
		CV3Service.GroupCode,
		CV3Service.SubGroupCode,
		HID.ShortName,
		Loc1.Name as Bed,
		CASE
		WHEN LOC1.TypeCode = 'Bed'
		THEN LOC3.Name
		WHEN LOC1.TypeCode = 'Room'
		THEN LOC2.Name
		ELSE LOC1.Name
		END AS Unit
	FROM
		[$(EPR)].dbo.CV3ActiveVisitList CV3ClientVisit

	INNER JOIN dbo.CV3VisitListJoin_R
	ON	CV3VisitListJoin_R.ObjectGUID = CV3ClientVisit.GUID
	AND CV3VisitListJoin_R.JobID =  @JobID

	INNER JOIN [$(EPR)].dbo.CV3Client
	ON	CV3ClientVisit.ClientGUID = CV3Client.GUID
		
	INNER JOIN [$(EPR)].dbo.CV3Location AS LOC1
	ON	CV3ClientVisit.CurrentLocationGUID = LOC1.GUID

	INNER JOIN [$(EPR)].dbo.CV3Location AS LOC2
	ON LOC1.ParentGUID = LOC2.GUID

	INNER JOIN [$(EPR)].dbo.CV3Location AS LOC3
	ON LOC2.ParentGUID = LOC3.GUID
	
	LEFT OUTER JOIN [$(EPR)].dbo.CV3Service
	ON	CV3ClientVisit.ServiceGUID = CV3Service.GUID
	
	LEFT OUTER JOIN [$(EPR)].dbo.CV3HealthIssueDeclaration HID
	ON	HID.ClientGUID = CV3ClientVisit.ClientGUID
	And	CV3ClientVisit.ChartGUID = HID.ChartGUID
	And	CV3ClientVisit.GUID = HID.ClientVisitGUID
	AND	hid.status = 'Active' 
	and hid.TypeCode = 'Admitting Dx'
	AND HID.Active = 1

	WHERE
		CV3ClientVisit.VisitStatus = 'ADM' and CV3ClientVisit.Active = 1
	AND	CV3ClientVisit.TypeCode in ('Inpatient', 'Emergency','Outpatient')
	AND CV3ClientVisit.IDCode not In ('999999','999998','999997','999996')
	AND CASE
		WHEN LOC1.TypeCode = 'Bed'
		THEN LOC3.Name
		WHEN LOC1.TypeCode = 'Room'
		THEN LOC2.Name
		ELSE LOC1.Name
		END NOT IN ('Wellington Unit','XXTESTWARDEPR','CDU','CTCU','AICU','AICU/ICA','PITU','Neonatal','SAU','OPAT','Delivery Suite')

ELSE

	SELECT
		CodeNum = 5, 
		CV3ClientVisit.GUID,
		CV3Client.GUID AS ClientGUID,
		CV3ClientVisit.ChartGUID AS ChartGUID,
		CV3ClientVisit.ClientDisplayName,
		CV3ClientVisit.CurrentLocation,
		CV3ClientVisit.ProviderDisplayName,
		CV3ClientVisit.InternalVisitStatus,
		CV3Client.IsMaterialized,
		CV3Client.BirthYearNum,
		CV3Client.BirthMonthNum,
		CV3Client.BirthDayNum,
		CV3ClientVisit.IDCode as MRN,
		CV3ClientVisit.VisitIDCode as VisitID,
		CV3ClientVisit.AdmitDTM,
		CV3Service.GroupCode,
		CV3Service.SubGroupCode,
		HID.ShortName,
		Loc1.Name as Bed,
		CASE
		WHEN LOC1.TypeCode = 'Bed'
		THEN LOC3.Name
		WHEN LOC1.TypeCode = 'Room'
		THEN LOC2.Name
		ELSE LOC1.Name
		END AS Unit
	FROM
		[$(EPR)].dbo.CV3ActiveVisitList CV3ClientVisit

	INNER JOIN [$(EPR)].dbo.CV3Client
	ON	CV3ClientVisit.ClientGUID = CV3Client.GUID
		
	INNER JOIN [$(EPR)].dbo.CV3Location AS LOC1
	ON	CV3ClientVisit.CurrentLocationGUID = LOC1.GUID

	INNER JOIN [$(EPR)].dbo.CV3Location AS LOC2
	ON LOC1.ParentGUID = LOC2.GUID

	INNER JOIN [$(EPR)].dbo.CV3Location AS LOC3
	ON LOC2.ParentGUID = LOC3.GUID
	
	LEFT OUTER JOIN [$(EPR)].dbo.CV3Service
	ON	CV3ClientVisit.ServiceGUID = CV3Service.GUID
	
	LEFT OUTER JOIN [$(EPR)].dbo.CV3HealthIssueDeclaration HID
	ON	HID.ClientGUID = CV3ClientVisit.ClientGUID
	And	CV3ClientVisit.ChartGUID = HID.ChartGUID
	And	CV3ClientVisit.GUID = HID.ClientVisitGUID
	AND	hid.status = 'Active' 
	and hid.TypeCode = 'Admitting Dx'
	AND HID.Active = 1

	WHERE
		CV3ClientVisit.VisitStatus = 'ADM' and CV3ClientVisit.Active = 1
	AND	CV3ClientVisit.TypeCode in ('Inpatient', 'Emergency','Outpatient')
	AND CV3ClientVisit.IDCode not In ('999999','999998','999997','999996')
	AND CASE
		WHEN LOC1.TypeCode = 'Bed'
		THEN LOC3.Name
		WHEN LOC1.TypeCode = 'Room'
		THEN LOC2.Name
		ELSE LOC1.Name
		END NOT IN ('Wellington Unit','XXTESTWARDEPR','CDU','CTCU','AICU','AICU/ICA','PITU','Neonatal','SAU','OPAT','Delivery Suite')