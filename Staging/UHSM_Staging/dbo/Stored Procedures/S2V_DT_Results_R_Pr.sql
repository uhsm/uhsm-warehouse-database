﻿



CREATE PROCEDURE [dbo].[S2V_DT_Results_R_Pr]          
(          
  @JobID int 
 , @TerminalDigit int 
)          
AS          
--  '*****************************************************************************************************************************************************************************  
               
--*** Allscripts Disclaimer:                 
--  Client is responsible for all decisions, acts, and omissions of any persons in connection with the delivery of medical care or other services to any patients.                  
--  Before any Licensed Materials are placed into a live production environment, it is Client’s responsibility to review and test all Licensed Materials and associated                 
--  workflows and other content, as implemented, make independent decisions about system settings and configuration based upon Client’s needs, practices, standards and                 
--  environment, and reach its own independent determination that they are appropriate for such live production use.  Any such use by Client (or its Authorized Users)                 
--  will constitute Client’s representation that it has complied with the foregoing. Client shall ensure that all Authorized Users are appropriately trained in use of                 
--  the then-deployed release of the Software prior to their use of the Software in a live production environment.  Clinical Materials are tools to assist Authorized                 
--  Users in the delivery of medical care, but should not be viewed as prescriptive or authoritative. Clinical Materials are not a substitute for, and Client shall                 
--  ensure that each Authorized User applies in conjunction with the use thereof, independent professional medical judgment.  Clinical Materials are not designed for use,                 
--  and Client shall not use them, in any system that provides medical care without the participation of properly trained personnel.  Any live production use of Clinical                 
--  Materials by Client (or its Authorized Users) will constitute Client’s acceptance of clinical responsibility for the use of such materials.                
                                                           
--  ' CREATED BY :  CD,AllScripts                                                          
--  ' Date   :  10.09.2013  
--  ' Description :  Result Report                                                          
--  ' S2V_DT_Results_R_Pr 593000  
  
--  ' Modification History:  
--  
--  '****************************************************************************************************************************************************************************   
  
  
SET NOCOUNT ON          
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED          
          

CREATE TABLE #tmpResultItems    
(    
    TotalLine               int NULL,     
    TextLine                text NULL,     
    ClientGUID              numeric(16,0),     
    BasicObsEntered         datetime NULL,     
    Status                  varchar(10) NULL,     
    ItemName                varchar(125) NULL,    
    Value                   varchar(60) NULL,     
    TypeCode                varchar(2) NULL,     
    HasHistory              bit,    
    AbnormalityCode         varchar(5) NULL,    
    ReferenceUpperLimit     varchar(10) NULL,     
    ReferenceLowerLimit     varchar(10) NULL,     
    UnitOfMeasure           varchar(15) NULL,     
    OrderEntered            datetime NULL,     
    Name                    varchar(125) NULL,    
    PerformedDtm            datetime NULL,    
    OrderStatusCode         varchar(5) NULL,     
    Text                    text NULL,     
    LineNum                 int NULL,    
    HasMediaLink            tinyint NULL,    
    AncillaryReferenceCode  varchar(75) NULL,    
    OrderIDCode             char(9) NULL,    
    ClusterID               varchar(20) NULL,    
    DisplaySequence         int NULL    
)    
    
    
/* Create the temp table first, no data */     
		SELECT     
				CV3ClientVisit.ChartGUID,    
				ClientVisitGUID = CV3ClientVisit.GUID,    
				CV3ClientVisit.VisitStatus,    
				ClientIDCode = CV3ClientVisit.IDCode,    
				CV3ClientVisit.VisitIDCode,     
				CV3ClientVisit.ClientDisplayName,    
				CV3ClientVisit.ProviderDisplayName,     
				CV3ClientVisit.CurrentLocation,    
				CV3Client.GUID As ClientGUID,     
				CV3Client.BirthDayNum,     
				CV3Client.BirthMonthNum,     
				CV3Client.BirthYearNum,     
				CV3Client.GenderCode,     
				CV3Client.IsMaterialized    
		INTO     
				#tmpVisit    
		FROM      
		  [$(EPR)].dbo.CV3ClientVisit,    
		   [$(EPR)].dbo.CV3Client    
		WHERE    
		  1 = 2    
    
    
  --select * from #tmpVisit  
    
    
IF @JobID <>0  --created from SCM    
BEGIN    
/* Select Client Visits */    
		INSERT INTO #tmpVisit    
		SELECT  distinct  
            
				CV3ClientVisit.ChartGUID,    
				ClientVisitGUID = CV3ClientVisit.GUID,    
				CV3ClientVisit.VisitStatus,    
				ClientIDCode = CV3ClientVisit.IDCode,    
				CV3ClientVisit.VisitIDCode,     
				CV3ClientVisit.ClientDisplayName,    
				CV3ClientVisit.ProviderDisplayName,     
				CV3ClientVisit.CurrentLocation,    
				CV3Client.GUID As ClientGUID,     
				CV3Client.BirthDayNum,     
				CV3Client.BirthMonthNum,     
				CV3Client.BirthYearNum,     
				CV3Client.GenderCode,     
				CV3Client.IsMaterialized    
      
		   FROM dbo.CV3VisitListJoin_R    
      
		  JOIN  [$(EPR)].dbo.CV3ClientVisit    
		  ON CV3VisitListJoin_R.ObjectGUID = CV3ClientVisit.GUID    
		  --AND (CV3ClientVisit.IDCode not In ('999999','999998','999997','999996'))     
      
		  JOIN  [$(EPR)].dbo.CV3Client    
		  ON ( CV3ClientVisit.ClientGUID = CV3Client.GUID )    
      
		  JOIN  [$(EPR)].dbo.CV3Location    
		  ON CV3ClientVisit.CurrentLocationGUID = CV3Location.GUID    
      
		  JOIN  [$(EPR)].dbo.CV3BasicObservation    
		  ON CV3ClientVisit.ClientGUID = CV3BasicObservation.ClientGUID    
		  AND CV3ClientVisit.ChartGUID = CV3BasicObservation.ChartGUID    
		  AND CV3ClientVisit.GUID = CV3BasicObservation.ClientVisitGUID    
    
		WHERE CV3VisitListJoin_R.JobID = @JobID    
    
    /* Select Results */    
		INSERT INTO #tmpResultItems    
		SELECT  Distinct  
				CASE WHEN CV3BasicObservation.IsTextual = 1 THEN 2 ELSE 0 END,     
				NULL, --dbo.SCMJoinTextLinesFn('CV3TextualObservationLine',CV3BasicObservation.GUID, CV3BasicObservation.ClientGUID) ,     
				ClientGUID = CV3BasicObservation.ClientGUID,     
				CV3BasicObservation.Entered As BasicObsEntered,     
				CV3BasicObservation.Status,     
				CV3BasicObservation.ItemName,     
				CV3BasicObservation.Value,     
				CV3BasicObservation.TypeCode,     
				CV3BasicObservation.HasHistory,     
				CV3BasicObservation.AbnormalityCode,     
				CV3BasicObservation.ReferenceUpperLimit,     
				CV3BasicObservation.ReferenceLowerLimit,     
				CV3BasicObservation.UnitOfMeasure,     
				OrderEntered = CV3Order.Entered,     
				CV3Order.Name,    
				CV3Order.PerformedDtm,    
				CV3Order.OrderStatusCode,     
				dbo.SCMJoinTextLinesFn('CV3TextualObservationLine',CV3BasicObservation.GUID, CV3BasicObservation.ClientGUID),     
				CASE WHEN CV3BasicObservation.IsTextual = 1 THEN 1 ELSE 0 END AS LineNum,    
				HasMediaLink = convert( int, ISNULL( CV3BasicObservation.HasMediaLink, 0)),    
				CV3Order.AncillaryReferenceCode,    
				OrderIDCode = CV3Order.IDCode,    
				CV3BasicObservation.ClusterID,    
			    DisplaySequence = convert(int, isnull(CV3ResultComponent.DisplaySequence, 10000))    
		FROM     
		 [$(EPR)].dbo.CV3Order    
            
		JOIN  [$(EPR)].dbo.CV3BasicObservation    
		ON CV3Order.ClientGUID = CV3BasicObservation.ClientGUID    
		AND CV3Order.ChartGUID = CV3BasicObservation.ChartGUID    
		AND CV3Order.ClientVisitGUID = CV3BasicObservation.ClientVisitGUID    
		AND CV3Order.GUID = CV3BasicObservation.OrderGUID    
                      
		JOIN #tmpVisit tc    
		ON   tc.ClientGUID = CV3Order.ClientGUID    
		AND  tc.ChartGUID = CV3Order.ChartGUID    
		AND  tc.ClientVisitGUID = CV3Order.ClientVisitGUID    
    
		INNER JOIN  [$(EPR)].dbo.CV3ResultCatalogItem    
		ON (   CV3BasicObservation.ResultItemGUID = CV3ResultCatalogItem.GUID )     
    
		INNER JOIN  [$(EPR)].dbo.CV3ResultCatalogItem  Cv3ResultCatalogItem2    
				   on ( CV3Order.OrderCatalogMasterItemGUID = CV3ResultCatalogItem2.OrderMasterItemGUID )     
    
				LEFT OUTER JOIN  [$(EPR)].dbo.CV3ResultComponent    
				   on ( CV3ResultCatalogItem.GUID = CV3ResultComponent.ItemChildGUID AND     
						CV3ResultCatalogItem2.GUID = CV3ResultComponent.ItemParentGUID  )   
		WHERE   CV3BasicObservation.IsHistory = 0
		--WHERE CV3BasicObservation.TouchedWhen between GETDATE()-2 AND GETDATE()     
 
    
		SELECT  cv.ClientVisitGUID,    
				cv.VisitStatus,    
				cv.VisitIDCode,    
				cv.ClientDisplayName,    
				cv.ProviderDisplayName,    
				cv.CurrentLocation,    
				cv.BirthDayNum,    
				cv.BirthMonthNum,    
				cv.BirthYearNum,    
				cv.GenderCode,    
				cv.ClientIDCode,    
				cv.IsMaterialized,    
				ri.TotalLine,    
				ri.TextLine,    
				ri.ClientGUID,    
				ri.BasicObsEntered,    
				ri.Status,    
				ri.ItemName,    
				ri.Value,    
				ri.TypeCode,    
				ri.HasHistory,    
				CASE    
					WHEN ri.AbnormalityCode = 'U' THEN NULL    
					ELSE ri.AbnormalityCode    
				END AS AbnormalityCode,    
				ri.ReferenceUpperLimit,    
				ri.ReferenceLowerLimit,    
				ri.UnitOfMeasure,    
				ri.OrderEntered,    
				ri.PerformedDtm,    
				ri.Text,    
				ri.LineNum,    
				ri.HasMediaLink,    
				OrderStatusDesc =    
				CASE    
					WHEN ri.OrderStatusCode IS NOT NULL    
					  THEN  ( SELECT Description from  [$(EPR)].dbo.CV3OrderStatus    
							  WHERE CV3OrderStatus.Code = ri.OrderStatusCode )    
					ELSE NULL    
				END,    
				ri.AncillaryReferenceCode,    
				ri.OrderIDCode,    
				ri.ClusterID,    
				ri.DisplaySequence,    
				ri.Name    
		FROM    #tmpVisit cv 
		INNER JOIN  #tmpResultItems ri    
				on ( cv.ClientGUID = ri.ClientGUID )    
    
 /******************************************************************************************************************    
 SSRS Version    
 *************************************************************************************************************************/     
END     
ELSE    
IF @JobID = 0     
BEGIN    



---- Load index tables
--Exec dbo.uspLoadIndexTables


	Select  CV3Order.ClientVisitGUID,	
			CV3ClientVisit.VisitStatus,
			CV3ClientVisit.VisitIDCode,
			CV3ClientVisit.ClientDisplayName,
			CV3ClientVisit.ProviderDisplayName,
			CV3ClientVisit.CurrentLocation,
			CV3Client.GUID As ClientGUID,     
			CV3Client.BirthDayNum,     
			CV3Client.BirthMonthNum,     
			CV3Client.BirthYearNum,     
			CV3Client.GenderCode, 
			CV3ClientVisit.IDCode as ClientIDCode ,
			TotalLine = CASE WHEN CV3BasicObservation.IsTextual = 1 THEN 2 ELSE 0 END,
			Text = dbo.SCMJoinTextLinesFn('CV3TextualObservationLine',CV3BasicObservation.GUID, CV3BasicObservation.ClientGUID) ,
			CV3BasicObservation.Entered As BasicObsEntered,     
			CV3BasicObservation.Status,     
			CV3BasicObservation.ItemName,     
			CV3BasicObservation.Value,     
			CV3BasicObservation.TypeCode,     
			CV3BasicObservation.HasHistory,     
			CV3BasicObservation.AbnormalityCode,     
			CV3BasicObservation.ReferenceUpperLimit,     
			CV3BasicObservation.ReferenceLowerLimit,     
			CV3BasicObservation.UnitOfMeasure,     
			OrderEntered = CV3Order.Entered ,
			CV3Order.Name,    
			CV3Order.PerformedDtm,    
			CV3Order.OrderStatusCode,
			CASE    
			WHEN CV3Order.OrderStatusCode IS NOT NULL    
			THEN  ( SELECT Description from [$(EPR)].dbo.CV3OrderStatus    
				WHERE CV3OrderStatus.Code = CV3Order.OrderStatusCode )    
			ELSE NULL    
			END OrderStatusDesc,
			CASE WHEN CV3BasicObservation.IsTextual = 1 THEN 1 ELSE 0 END AS LineNum,    
			 HasMediaLink = convert( int, ISNULL( CV3BasicObservation.HasMediaLink, 0)),    
			CV3Order.AncillaryReferenceCode,    
			OrderIDCode = CV3Order.IDCode,    
			CV3BasicObservation.ClusterID,
			DATEFROMPARTS(CV3Client.BirthYearNum,CV3Client.BirthMonthNum,CV3Client.BirthDayNum)     as BirthDate,
			  DisplaySequence = convert(int, isnull(CV3ResultComponent.DisplaySequence, 10000))    


	From dbo.CV3OrderIndex
	INNER JOIN  [$(EPR)].dbo.CV3Order 
	ON CV3OrderIndex.GUID = CV3Order.GUID
	INNER JOIN   [$(EPR)].dbo.CV3ActiveVisitList
	ON CV3OrderIndex.ClientVisitGUID = CV3ActiveVisitList.GUID
	AND CV3ActiveVisitList.VisitStatus = 'ADM'
	AND CV3ActiveVisitList.Active = 1
	INNER JOIN  [$(EPR)].dbo.CV3ClientVisit 
	ON CV3ActiveVisitList.GUID = CV3ClientVisit.GUID
	INNER JOIN  [$(EPR)].dbo.CV3Client
	ON CV3ClientVisit.ClientGUID = CV3Client.GUID
 	INNER JOIN CV3BasicObservationIndex
	ON CV3Order.GUID = CV3BasicObservationIndex.OrderGUID
	INNER JOIN  [$(EPR)].dbo.CV3BasicObservation 
	ON CV3BasicObservationIndex.GUID = CV3BasicObservation.GUID

	INNER JOIN  [$(EPR)].dbo.CV3ResultCatalogItem    
	ON CV3BasicObservation.ResultItemGUID = CV3ResultCatalogItem.GUID
    
	INNER JOIN  [$(EPR)].dbo.CV3ResultCatalogItem  Cv3ResultCatalogItem2    
	ON CV3Order.OrderCatalogMasterItemGUID = CV3ResultCatalogItem2.OrderMasterItemGUID      
    
	LEFT OUTER JOIN  [$(EPR)].dbo.CV3ResultComponent    
	ON CV3ResultCatalogItem.GUID = CV3ResultComponent.ItemChildGUID 
	AND CV3ResultCatalogItem2.GUID = CV3ResultComponent.ItemParentGUID  


WHERE   CV3BasicObservation.IsHistory = 0
		AND Right(RTRIM(CV3ClientVisit.IDCode),1)  = @TerminalDigit




	
----CREATE TABLE #tmpResultItems    
----(    
----    TotalLine               int NULL,     
----    TextLine                text NULL,     
----    ClientGUID              numeric(16,0),     
----    BasicObsEntered         datetime NULL,     
----    Status                  varchar(10) NULL,     
----    ItemName                varchar(125) NULL,    
----    Value                   varchar(60) NULL,     
----    TypeCode                varchar(2) NULL,     
----    HasHistory              bit,    
----    AbnormalityCode         varchar(5) NULL,    
----    ReferenceUpperLimit     varchar(10) NULL,     
----    ReferenceLowerLimit     varchar(10) NULL,     
----    UnitOfMeasure           varchar(15) NULL,     
----    OrderEntered            datetime NULL,     
----    Name                    varchar(125) NULL,    
----    PerformedDtm            datetime NULL,    
----    OrderStatusCode         varchar(5) NULL,     
----    Text                    text NULL,     
----    LineNum                 int NULL,    
----    HasMediaLink            tinyint NULL,    
----    AncillaryReferenceCode  varchar(75) NULL,    
----    OrderIDCode             char(9) NULL,    
----    ClusterID               varchar(20) NULL,    
----    DisplaySequence         int NULL    
----)    
    
--/* Create the temp table first, no data */     
----SELECT     
----        CV3ClientVisit.ChartGUID,    
----        ClientVisitGUID = CV3ClientVisit.GUID,    
----        CV3ClientVisit.VisitStatus,    
----        ClientIDCode = CV3ClientVisit.IDCode,    
----        CV3ClientVisit.VisitIDCode,     
----        CV3ClientVisit.ClientDisplayName,    
----        CV3ClientVisit.ProviderDisplayName,     
----        CV3ClientVisit.CurrentLocation,    
----        CV3Client.GUID As ClientGUID,     
----        CV3Client.BirthDayNum,     
----        CV3Client.BirthMonthNum,     
----        CV3Client.BirthYearNum,     
----        CV3Client.GenderCode,     
----        CV3Client.IsMaterialized    
----INTO     
----        #tmpVisit    
----FROM      
----  CV3ClientVisit,    
----  CV3Client    
----WHERE    
----  1 = 2    
    
    
--/* Select Client Visits */    
--		INSERT INTO #tmpVisit    
--		SELECT  distinct  
            
--				CV3ClientVisit.ChartGUID,    
--				ClientVisitGUID = CV3ClientVisit.GUID,    
--				CV3ClientVisit.VisitStatus,    
--				ClientIDCode = CV3ClientVisit.IDCode,    
--				CV3ClientVisit.VisitIDCode,     
--				CV3ClientVisit.ClientDisplayName,    
--				CV3ClientVisit.ProviderDisplayName,     
--				CV3ClientVisit.CurrentLocation,    
--				CV3Client.GUID As ClientGUID,     
--				CV3Client.BirthDayNum,     
--				CV3Client.BirthMonthNum,     
--				CV3Client.BirthYearNum,     
--				CV3Client.GenderCode,     
--			   CV3Client.IsMaterialized    
      
--		   FROM     
--		   --CV3VisitListJoin_R    
--			--INNER JOIN     
--		  [PROD].dbo.CV3ActiveVisitList    
--		  JOIN [PROD].dbo.CV3ClientVisit    
--		  ON CV3ClientVisit.ClientGUID = CV3ActiveVisitList.ClientGUID    
--		  AND CV3ClientVisit.ChartGUID = CV3ActiveVisitList.ChartGUID    
--		  AND CV3ClientVisit.GUID = CV3ActiveVisitList.GUID    
--		  AND CV3ActiveVisitList.VisitStatus = 'ADM'     
--		  AND CV3ActiveVisitList.Active = 1    

      
--		  JOIN [PROD].dbo.CV3Client    
--		  ON ( CV3ClientVisit.ClientGUID = CV3Client.GUID )    
      
--		  JOIN [PROD].dbo.CV3Location    
--		  ON CV3ClientVisit.CurrentLocationGUID = CV3Location.GUID    
      
--		  JOIN [PROD].dbo.CV3BasicObservation    
--		  ON CV3ClientVisit.ClientGUID = CV3BasicObservation.ClientGUID    
--		  AND CV3ClientVisit.ChartGUID = CV3BasicObservation.ChartGUID    
--		  AND CV3ClientVisit.GUID = CV3BasicObservation.ClientVisitGUID    
    
--		--WHERE CV3VisitListJoin_R.JobID = @JobID    
  
  
----select distinct * from #tmpVisit  
    
--    /* Select Results */    
--		INSERT INTO #tmpResultItems    
--		SELECT  Distinct  
--				CASE WHEN CV3BasicObservation.IsTextual = 1 THEN 2 ELSE 0 END,     
--				NULL, --dbo.SCMJoinTextLinesFn('CV3TextualObservationLine',CV3BasicObservation.GUID, CV3BasicObservation.ClientGUID) ,     
--				ClientGUID = CV3BasicObservation.ClientGUID,     
--				CV3BasicObservation.Entered As BasicObsEntered,     
--				CV3BasicObservation.Status,     
--				CV3BasicObservation.ItemName,     
--				CV3BasicObservation.Value,     
--				CV3BasicObservation.TypeCode,     
--				CV3BasicObservation.HasHistory,     
--				CV3BasicObservation.AbnormalityCode,     
--				CV3BasicObservation.ReferenceUpperLimit,     
--				CV3BasicObservation.ReferenceLowerLimit,     
--				CV3BasicObservation.UnitOfMeasure,     
--				OrderEntered = CV3Order.Entered,     
--				CV3Order.Name,    
--				CV3Order.PerformedDtm,    
--				CV3Order.OrderStatusCode,     
--				dbo.SCMJoinTextLinesFn('CV3TextualObservationLine',CV3BasicObservation.GUID, CV3BasicObservation.ClientGUID),     
--				CASE WHEN CV3BasicObservation.IsTextual = 1 THEN 1 ELSE 0 END AS LineNum,    
--			 HasMediaLink = convert( int, ISNULL( CV3BasicObservation.HasMediaLink, 0)),    
--				CV3Order.AncillaryReferenceCode,    
--				OrderIDCode = CV3Order.IDCode,    
--				CV3BasicObservation.ClusterID,    
--			  DisplaySequence = convert(int, isnull(CV3ResultComponent.DisplaySequence, 10000))    
--		FROM     
--		[PROD].dbo.CV3Order    
            
--		JOIN [PROD].dbo.CV3BasicObservation    
--		ON CV3Order.ClientGUID = CV3BasicObservation.ClientGUID    
--		AND CV3Order.ChartGUID = CV3BasicObservation.ChartGUID    
--		AND CV3Order.ClientVisitGUID = CV3BasicObservation.ClientVisitGUID    
--		AND CV3Order.GUID = CV3BasicObservation.OrderGUID    
                      
--		JOIN #tmpVisit tc    
--		ON   tc.ClientGUID = CV3Order.ClientGUID    
--		AND  tc.ChartGUID = CV3Order.ChartGUID    
--		AND  tc.ClientVisitGUID = CV3Order.ClientVisitGUID   
     
      
    
--		INNER JOIN [PROD].dbo.CV3ResultCatalogItem    
--		ON CV3BasicObservation.ResultItemGUID = CV3ResultCatalogItem.GUID
    
--		INNER JOIN [PROD].dbo.CV3ResultCatalogItem  Cv3ResultCatalogItem2    
--		ON CV3Order.OrderCatalogMasterItemGUID = CV3ResultCatalogItem2.OrderMasterItemGUID      
    
--		LEFT OUTER JOIN [PROD].dbo.CV3ResultComponent    
--		ON CV3ResultCatalogItem.GUID = CV3ResultComponent.ItemChildGUID 
--		AND CV3ResultCatalogItem2.GUID = CV3ResultComponent.ItemParentGUID  
		  
--		WHERE   CV3BasicObservation.IsHistory = 0
--		--WHERE CV3BasicObservation.TouchedWhen between GETDATE()-2 AND GETDATE()     
--		--select * from #tmpVisit  
  
----select *  from #tmpResultItems order by ItemName asc  
    
--		SELECT  cv.ClientVisitGUID,    
--				cv.VisitStatus,    
--				cv.VisitIDCode,    
--				cv.ClientDisplayName,    
--				cv.ProviderDisplayName,    
--				cv.CurrentLocation,    
--				cv.BirthDayNum,    
--				cv.BirthMonthNum,    
--				cv.BirthYearNum,    
--				cv.GenderCode,    
--				cv.ClientIDCode,    
--				cv.IsMaterialized,    
--				ri.TotalLine,    
--				ri.TextLine,    
--				ri.ClientGUID,    
--				ri.BasicObsEntered,    
--				ri.Status,    
--				ri.ItemName,    
--				ri.Value,    
--				ri.TypeCode,    
--				ri.HasHistory,    
--				CASE    
--					WHEN ri.AbnormalityCode = 'U' THEN NULL    
--					ELSE ri.AbnormalityCode    
--				END AS AbnormalityCode,    
--				ri.ReferenceUpperLimit,    
--				ri.ReferenceLowerLimit,    
--				ri.UnitOfMeasure,    
--				ri.OrderEntered,    
--				ri.PerformedDtm,    
--				ri.Text,    
--				ri.LineNum,    
--				ri.HasMediaLink,    
--				OrderStatusDesc =    
--				CASE    
--					WHEN ri.OrderStatusCode IS NOT NULL    
--					  THEN  ( SELECT Description from [PROD].dbo.CV3OrderStatus    
--							  WHERE CV3OrderStatus.Code = ri.OrderStatusCode )    
--					ELSE NULL    
--				END,    
--				ri.AncillaryReferenceCode,    
--				ri.OrderIDCode,    
--				ri.ClusterID,    
--				--ri.DisplaySequence,    
--				ri.Name,
--				DATEFROMPARTS(cv.BirthYearNum,cv.BirthMonthNum,cv.BirthDayNum)    AS BirthDate   
--		FROM    #tmpVisit cv 
--		INNER JOIN  #tmpResultItems ri    
--		ON ( cv.ClientGUID = ri.ClientGUID )    
--		Where cv.ClientIDCode = 'RM24487073'
            
END