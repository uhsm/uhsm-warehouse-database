﻿
/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		[dbo].[UHSM_Alerts_UB]

Notes:			Stored in LocalAdmin.

Versions:			1.0.0.0 - 01/12/2016 - DBH
					Created sproc.
					
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE PROC [dbo].[UHSM_Alerts_UB] 
AS

SELECT
 [Alerts].[Code] AS 'AlertCode'
,[Alerts].[Description] AS 'AlertDescription'
,[Alerts].[Text] AS 'AlertComments'
,[Alerts].[GUID] AS 'AlertIDCode'
,[PTIDRM2].[ClientIDCode] AS 'DistrictNo'
,[PTIDNHS].[ClientIDCode] AS 'NHSNo'
,[Patient].[FirstName] + ' ' + [Patient].[LastName] AS 'PatientName'
,CASE 
WHEN ([Patient].[BirthYearNum] = 0 OR [Patient].BirthMonthNum = 0 OR [Patient].BirthDayNum = 0)
THEN ''
ELSE DATEFROMPARTS([Patient].BirthYearNum,[Patient].BirthMonthNum,[Patient].BirthDayNum) 
END AS DOB
,[Alerts].[CreatedWhen] AS 'Created_DTTM'
,[Alerts].[TouchedWhen] AS 'Modified_DTTM'
,CASE
WHEN ([Alerts].[OnsetYearNum] NOT BETWEEN 1850 AND 2199 OR [Alerts].[OnsetMonthNum]  NOT BETWEEN 1 AND 12 OR [Alerts].[OnsetDayNum]  NOT BETWEEN 1 AND 31
OR [Alerts].[OnsetYearNum] IS NULL OR [Alerts].[OnsetMonthNum]  IS NULL OR [Alerts].[OnsetDayNum] IS NULL)
THEN NULL
ELSE DATEFROMPARTS (CAST([Alerts].[OnsetYearNum] AS INT),CAST([Alerts].[OnsetMonthNum] AS INT),CAST([Alerts].[OnsetDayNum] AS INT)) 
END AS 'Started_DTTM'
,CAST ([Spell].[AdmitDtm] AS DATE) AS 'AdmissionDate'
,[Spell].[AdmitDtm] AS 'AdmissionDateTime'
,[Spell].[CurrentLocation] AS 'AdmissionWard'
,CASE 
WHEN [Spell].[TemporaryLocation] IS NULL 
THEN [Spell].[CurrentLocation]
ELSE [Spell].[TemporaryLocation]
END AS 'CurrentWard'
,[Spell].[TypeCode]
,[Service].[GroupCode]
,[Hierarchy].[SpecialtyFunction]
,[Hierarchy].[Directorate]
,[Hierarchy].[Division]

FROM [$(EPR)].[dbo].[CV3ClientVisit] as [Spell]

LEFT JOIN [$(EPR)].[dbo].[CV3HealthIssueDeclaration] AS [Alerts]
ON [Spell].[ClientGUID] = [Alerts].[ClientGUID]

LEFT JOIN [$(EPR)].[dbo].[CV3Client] AS [Patient]
ON [Patient].[GUID] = [Alerts].[ClientGUID]

LEFT JOIN [$(EPR)].[dbo].[CV3ClientID] AS [PTIDRM2]
ON [PTIDRM2].[ClientGUID] = [Alerts].[ClientGUID]
AND [PTIDRM2].[TypeCode] = 'Hospital Number'

LEFT JOIN [$(EPR)].[dbo].[CV3ClientID] AS [PTIDNHS]
ON [PTIDNHS].[ClientGUID] = [Alerts].[ClientGUID]
AND [PTIDNHS].[TypeCode] = 'NHS Number'
AND [PTIDNHS].[ClientIDCode] <> 'UNKNOWN'

LEFT JOIN [$(EPR)].[dbo].[CV3Service] AS [Service]
ON [Spell].[ServiceGUID] = [Service].[GUID]

LEFT JOIN [dbo].[Specialty] AS [Hierarchy]
ON [Hierarchy].[SubSpecialtyCode] = [Service].[GroupCode]



WHERE [Alerts].[Status] = 'Active'
AND [Spell].DischargeDtm IS NULL
AND [Spell].[TypeCode] LIKE '%Inpatient%'
AND [Alerts].[CodingScheme] = 'INFECTION ALERT'