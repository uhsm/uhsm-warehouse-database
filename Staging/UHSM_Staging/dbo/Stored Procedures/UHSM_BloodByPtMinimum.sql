﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		SCM Order Requisition

Notes:			Stored in SM1SCM-CPM1.PROD

Versions:		
		
				1.0.0.0 - 22/12/2016 - DBH
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [dbo].[UHSM_BloodByPtMinimum] AS 
SELECT DISTINCT
[RM2].[ClientIDCode] AS 'RM2'
,[Patient].[DisplayName] AS 'PatientName'
,CASE
WHEN ([Patient].[BirthYearNum] = 0 OR [Patient].[BirthMonthNum] = 0 OR [Patient].[BirthDayNum] = 0)
THEN ''
ELSE DATEFROMPARTS ([Patient].[BirthYearNum],[Patient].[BirthMonthNum],[Patient].[BirthDayNum]) END AS 'DOB'
,[Visit].[VisitIDCode] AS 'VisitID'
,[Visit].[TypeCode]
,[Visit].[CurrentLocation]
,CASE 
WHEN [CustomData].[Value] IS NULL 
THEN 'No' 
ELSE [CustomData].[Value] 
END AS 'Fasting'

FROM [$(EPR)].[dbo].[CV3Order] AS [Orders]

LEFT JOIN [$(EPR)].[dbo].[CV3ClientVisit] AS [Visit]
ON [Visit].[GUID] = [Orders].[ClientVisitGUID]

LEFT JOIN [$(EPR)].[dbo].[CV3ClientID] AS [RM2]
ON [RM2].[ClientGUID] = [Orders].[ClientGUID]
AND [RM2].[TypeCode] = 'HOSPITAL NUMBER'

LEFT JOIN [$(EPR)].[dbo].[CV3Client] AS [Patient]
ON [Patient].[GUID] = [Orders].[ClientGUID]

LEFT JOIN [$(EPR)].[dbo].[CV3OrderCatalogMasterItem] AS [Item]
ON [Orders].[OrderCatalogMasterItemGUID] = [Item].[GUID]

LEFT JOIN [$(EPR)].[dbo].[CV3OrderUserData] AS [CustomData]
ON [CustomData].[OrderGUID] = [Orders].[GUID]
AND [CustomData].[UserDataCode] LIKE '%FAST%'

WHERE 
[Item].[SpecimenType] = 'Blood'
AND 
[Orders].[OrderStatusCode] = 'PCOL'
AND 
[Visit].[TypeCode] = 'Outpatient'

