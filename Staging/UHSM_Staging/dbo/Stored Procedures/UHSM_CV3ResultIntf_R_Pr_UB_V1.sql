﻿/* 
**  MODIFICATION HISTORY:
-- 
-- 1   09/16/2009 13:40 mpatel6
-- [CR128150] (5.5) When no Value Defaulted in the Abnormality Code OBX-8 a Value of U Displays on Result Reports
--
*/
--drop procedure CV3ResultIntf_R_PR
CREATE PROCEDURE [dbo].[UHSM_CV3ResultIntf_R_Pr_UB_V1]
(
  @JobID int
)
AS
/* P r o p r i e t a r y  N o t i c e */
/* Unpublished � 2002-2015 Allscripts Healthcare, LLC.  and/or its affiliates. All Rights Reserved.
*
* P r o p r i e t a r y  N o t i c e: This software has been provided pursuant to a License Agreement, with
Allscripts Healthcare, LLC.  and/or its affiliates, containing restrictions on its use. This software contains
valuable trade secrets and proprietary information of Allscripts Healthcare, LLC.  and/or its affiliates and is
protected by trade secret and copyright law. This software may not be copied or distributed in any form or medium,
disclosed to any third parties, or used in any manner not provided for in said License Agreement except with prior
written authorization from Allscripts Healthcare, LLC.  and/or its affiliates. Notice to U.S. Government Users:
This software is �Commercial Computer Software.�

All product names are the trademarks or registered trademarks of Allscripts Healthcare, LLC.  and/or its affiliates.

*
**/
/* P r o p r i e t a r y  N o t i c e */
SET NOCOUNT ON 

    SELECT
        CV3BasicObservation_R.JobID,                              CV3BasicObservation_R.ObjectGUID,
        CV3BasicObservation_R.TotalLine,
		CASE WHEN nullif(CV3BasicObservation_R.TextLine, '') IS NOT NULL AND 
			isnull(CV3BasicObservation.CommentStartPosition, 0) BETWEEN 2 AND LEN(CV3BasicObservation_R.TextLine) THEN
			STUFF(CV3BasicObservation_R.TextLine, CV3BasicObservation.CommentStartPosition-1, 1, char(10)+char(13) )
		ELSE
			CV3BasicObservation_R.TextLine END AS TextLine, 
        BasicObsGUID = CV3BasicObservation.GUID,                  BasicObsEntered = CV3BasicObservation.Entered,
        BasicObsChartGUID = CV3BasicObservation.ChartGUID,        ClientGUID = CV3BasicObservation.ClientGUID,
        CV3BasicObservation.Status,                               CV3BasicObservation.ItemName,
        CV3BasicObservation.ClusterID,                            CV3BasicObservation.Value,
        CV3BasicObservation.TypeCode,                             CV3BasicObservation.HasHistory,
		CV3BasicObservation.IsHistory,
        CASE
            WHEN CV3BasicObservation.AbnormalityCode = 'U' THEN NULL
            ELSE CV3BasicObservation.AbnormalityCode
        END AS AbnormalityCode,
        CV3BasicObservation.ReferenceUpperLimit,
        CV3BasicObservation.ReferenceLowerLimit,                  CV3BasicObservation.UnitOfMeasure,
        CV3ClientVisit.VisitStatus,                               CV3ClientVisit.VisitIDCode,
        CV3ClientVisit.ClientDisplayName,                         CV3ClientVisit.ProviderDisplayName,
        CV3ClientVisit.CurrentLocation,                           OrderGUID = CV3Order.GUID,
        OrderEntered = CV3Order.Entered,                          CV3Order.Name,
        CV3Order.PerformedDtm,                                    CV3Order.AncillaryReferenceCode,
        OrderIDCode = CV3Order.IDCode,
		CASE WHEN nullif(CV3TextualObservationLine.Text, '') IS NOT NULL AND 
			isnull(CV3BasicObservation.CommentStartPosition, 0) BETWEEN 2 AND LEN(CV3TextualObservationLine.Text) THEN
			STUFF(CV3TextualObservationLine.Text, CV3BasicObservation.CommentStartPosition-1, 1, char(10)+char(13) )
		ELSE
			CV3TextualObservationLine.Text END AS [Text],  
        CV3TextualObservationLine.LineNum,                        CV3Client.BirthDayNum,
        CV3Client.BirthMonthNum,                                  CV3Client.BirthYearNum,
        CV3Client.GenderCode,                                     ClientID = CV3ClientVisit.IDCode,
        ResultCatItemGUID = CV3ResultCatalogItem.GUID,            OrderStatusDesc = CV3OrderStatus.Description,
        ResultCompGUID = CV3ResultComponent.GUID,                 
      	DisplaySequence = convert(int, isnull(CV3ResultComponent.DisplaySequence, 10000)),
	    HasMediaLink = convert( int, ISNULL( CV3BasicObservation.HasMediaLink, 0))
    FROM
        [$(EPR)].[dbo].CV3BasicObservation CV3BasicObservation
		  INNER JOIN [$(EPR)].[dbo].CV3BasicObservation_R CV3BasicObservation_R
		    ON ( CV3BasicObservation_R.ObjectGUID = CV3BasicObservation.GUID AND
			     CV3BasicObservation_R.JobID = @JobID )
		  INNER JOIN [$(EPR)].[dbo].CV3ClientVisit CV3ClientVisit
		    ON ( CV3BasicObservation.ClientVisitGUID = CV3ClientVisit.GUID )
		  INNER JOIN [$(EPR)].[dbo].CV3Client CV3Client
		    ON ( CV3BasicObservation.ClientGUID = CV3Client.GUID )
		  INNER JOIN [$(EPR)].[dbo].CV3Order CV3Order
		    ON ( CV3BasicObservation.ClientGUID = CV3Order.ClientGUID AND
			     CV3BasicObservation.ChartGUID = CV3Order.ChartGUID AND
			     CV3BasicObservation.OrderGUID = CV3Order.GUID )
		  INNER JOIN [$(EPR)].[dbo].CV3OrderStatus CV3OrderStatus
		    ON ( CV3Order.OrderStatusCode = CV3OrderStatus.Code )
		  INNER JOIN [$(EPR)].[dbo].CV3ResultCatalogItem CV3ResultCatalogItem
		    ON ( CV3BasicObservation.ResultItemGUID = CV3ResultCatalogItem.GUID )
		  INNER JOIN [$(EPR)].[dbo].CV3ResultCatalogItem CV3ResultCatalogItem2
		    ON ( CV3Order.OrderCatalogMasterItemGUID = CV3ResultCatalogItem2.OrderMasterItemGUID )
		  LEFT OUTER JOIN [$(EPR)].[dbo].CV3TextualObservationLine CV3TextualObservationLine
		    ON ( CV3BasicObservation.ClientGUID = CV3TextualObservationLine.ClientGUID AND
                 CV3BasicObservation.GUID = CV3TextualObservationLine.ObservationGUID  AND
				 CV3TextualObservationLine.LineNum = 1 )
		  LEFT OUTER JOIN [$(EPR)].[dbo].CV3ResultComponent CV3ResultComponent
		    ON ( CV3ResultCatalogItem.GUID = CV3ResultComponent.ItemChildGUID AND
			     CV3ResultCatalogItem2.GUID = CV3ResultComponent.ItemParentGUID )

return