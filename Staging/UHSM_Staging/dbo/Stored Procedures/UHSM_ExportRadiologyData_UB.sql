﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		EPR Load to Data Warehouse to allow joining to CRIS. Key field is OrderID (which can be joined to DW_REPORTING.CRIS.Orders.OrderID).
				
Notes:			Stored in SM2SCM-CPM1.UHSM_Staging

Versions:		1.0.0.0 - 09/12/2016 - GR
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [dbo].UHSM_ExportRadiologyData_UB
as

;With Obs As (
	Select	RowNo = ROW_NUMBER() Over (Partition By CV3BasicObservation.OrderGUID Order By CV3BasicObservation.GUID Asc)
			,CV3BasicObservation.*
	From	[$(EPR)].dbo.CV3BasicObservation 
	join	[$(EPR)].dbo.CV3AllOrdersVw
	on		CV3AllOrdersVw.GUID = CV3BasicObservation.OrderGUID
	Join [$(EPR)].dbo.CV3OrderCatalogMasterItem cmi
	On CV3AllOrdersVw.OrderCatalogMasterItemGUID = cmi.GUID
	join [$(EPR)].dbo.CV3OrganizationalUnit cou
	On cmi.OrgUnitGUID = cou.GUID--to get the unit which 'owns' the test - e.g. Microbiology, Radiology, Cellular Pathology, etc.
	Where	ItemName not like '%Autofile%'
			and ItemName=cmi.[Name]
		And cou.Name = 'Diagnostic Imaging'
		And CV3AllOrdersVw.RequestedDtm >= '06 dec 2016 20:00'
		and CV3AllOrdersVw.RequestedDtm < cast(getdate() as date)
		--		and obs.IsHistory = 0
	)
--This may be more complicated for Pathology (as they can have a number of different results returned at different times),
--but when looking at how the EPR and CRIS data relate to each other, joining on the above CTE on the OrderGUID and only
--including where RowNo = 1 will give you an Obs.Entered value which is the equivalent to the Report Date/Time we used to get
--in ICE (i.e. 2 minutes after the first verified report was submitted from CRIS and not affected by any subsequent amendments/corrects
--sent from CRIS at a future time, which is what Steph Carney considers the correct way of measuring the reporting time for the purpose
--of calculating turnaround times)

Select 
--RowNo = ROW_NUMBER() Over (Partition By obs.OrderGUID Order By obs.GUID Asc)
--,obs.OrderGUID
--,obs.ArrivalDtm
--,obs.ObservationDtm
--,stat.[Description]
--,obs.ItemName
--,obs.TypeCode
--,obs.IsHistory
--,obs.AbnormalityCode
--,obs.ResultItemCode
--,obs.*
	OrderID = src.IDCode
	,SubmittedRequestDateTime = src.RequestedDtm
	,ExportedReportDateTime = obs.Entered
	,RM2Number = vis.IDCode
	,PatientName = vis.ClientDisplayName
	,Sex = pat.GenderCode
	,DOB = dbo.fn_SCM_DOB(pat.BirthYearNum,pat.BirthMonthNum,pat.BirthDayNum)
	,Exam = Case When an.[Name] Is Null Then '' Else LTRIM(RTRIM(an.[Name])) + ' ' End + Coalesce(cmi.[Name],'')
	,Consultant = vis.ProviderDisplayName
	,[Location] = vis.CurrentLocation
	,[Status] = stat.[Description]
	,Modality = CASE WHEN src.Name in 
					('AUTOFLUORESCENCE BRONCHOSCOPY'
					,'BRONCH RESEARCH'
					,'BRONCHIAL THERMOPLASTY'
					,'BRONCHOSCOPIC LVRT'
					,'BRONCHOSCOPIC STENT'
					,'BRONCHOSCOPY '
					,'CHARTIS ASSESSMENT'
					,'DIAGNOSTIC PLEURAL ASPIRATION'
					,'DRAINAGE OF INDWELLING PLEURAL CATHETER'
					,'EBUS-TBNA'
					,'EBUS-TBLB (1.7mm probe)'
					,'EBUS-TBNA (1.7mm Probe)' --GR 14/12/16 Added as this name is used on EPR but not CRIS
					,'EBUS-TBNA (GA)' 
					,'Vocal Cord Inspection' --GR 15/12/16 Added as reqested by Dawn Hand
					,'ENDOSCOPY OF NASAL CAVITY/UPPER AIRWAY'
					,'INTRALUMINAL BRACHYTHERAPY'
					,'IPC INS FOR IPC-PLUS TRIAL'
					,'LGE BORE BLUNT DISSECTION CHES DRAIN INS'
					,'LOCAL ANAESTHETIC THORACOSCOPY'
					,'INSERTION OF INDWELLING PLEURAL CATHETER'
					,'LOCAL ANAESTHETIC THORACOSCOPY'
					,'MEDICAL THORACOSCOPY WITH BIOPSY'
					,'PHOTODYNAMIC THERAPY'
					,'RADIAL EBUS (2.6mm probe)'
					,'REMOVAL OF INDWELLING PLEURAL CATHETER'
					,'REVIEW FOR ENDOBRONCHIAL THERAPY'
					,'ROUTINE BRONCHOSCOPY WITH BLIND TBNA'
					,'ROUTINE BRONCHOSCOPY WITH  TBLB'
					,'ROUTINE BRONCHOSCOPY WITH  BAL'
					,'ROUTINE BRONCHOSCOPY WITH TBLB'
					,'ROUTINE BRONCHOSCOPY WITH BAL'
					,'SELDINGER CHEST DRAIN INSERTION'
					,'SUSPECTED FOREIGN BODY REMOVAL'
					,'TALC SLURRY'
					,'TEMPORARY PLEURAL DRAIN'
					,'THERAPEUTIC IRRIGATION & BRONC TOILETING'
					,'THERAPEUTIC PLEURAL ASPIRATION'
					,'THORACOSCOPIC TALC POUDRAGE'
					,'ULTRASOUND GUIDED ASPIRATION OF THORAX'
					,'ULTRASOUND GUIDED DRAINAGE THORAX '
					,'US GUIDED PERCUTANEOUS PLEURAL BIOPSY'
					) 
				THEN 'Bronchoscopy'
				ELSE Coalesce(ex.Modality,'Other')
				END
	,RequestedBy = us.DisplayName
	,OrderGuid = src.GUID
	,Orderstatus = src.Status
	,OrderStatusCode = src.OrderStatusCode
	,frm.[Name]
	,Discipline = cou.Name


From	 [$(EPR)].dbo.CV3AllOrdersVw src With (NoLock)

		Inner Join [$(EPR)].dbo.CV3ClientVisit vis With (NoLock)
			On src.ClientVisitGUID = vis.GUID

		Inner Join [$(EPR)].dbo.CV3Client pat With (NoLock)
			On vis.ClientGUID = pat.GUID

		Left Join [$(EPR)].dbo.CV3OrderStatus stat With (NoLock)
			On src.OrderStatusCode = stat.Code

		Left Join [$(EPR)].dbo.CV3OrderCatalogMasterItem cmi With (NoLock)
			On src.OrderCatalogMasterItemGUID = cmi.GUID

		Left Join [$(EPR)].dbo.CV3AncillaryName an With (NoLock)
			On cmi.GUID = an.MainCatItemGUID
			And an.CodingStd Like '%OUT'

		Left Join [$(EPR)].dbo.CV3OrderEntryForm frm With (NoLock)
			On src.OrderEntryFormGUID = frm.GUID

		Left Join [$(EPR)].dbo.CV3User us With (NoLock)
			On src.UserGUID = us.GUID

		Left Join CRISExam ex With (NoLock)
			On LTRIM(RTRIM(an.[Name])) = ex.ExamCode

		--Name field is where the code is found
		Left outer join [$(EPR)].dbo.CV3OrganizationalUnit cou
			On cmi.OrgUnitGUID = cou.GUID--to get the unit which 'owns' the test - e.g. Microbiology, Radiology, Cellular Pathology, etc.

		Left outer join Obs obs
			On obs.RowNo = 1
			And src.GUID = obs.OrderGUID


Where	--frm.[Name] Like 'RAD%'
		cou.Name = 'Diagnostic Imaging'
		And src.RequestedDtm >= '06 dec 2016 20:00'
		and src.RequestedDtm < cast(getdate() as date)
		order by src.GUID