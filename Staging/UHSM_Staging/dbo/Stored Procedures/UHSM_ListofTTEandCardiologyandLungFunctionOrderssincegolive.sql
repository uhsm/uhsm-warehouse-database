﻿
/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		[dbo].[UHSM_ListofTTEandCardiologyandLungFunctionOrderssincegolive]

Notes:			Stored in LocalAdmin.

Versions:			1.0.0.0 - 09/12/2016 - DBH
					Created sproc.
					
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE PROC [dbo].[UHSM_ListofTTEandCardiologyandLungFunctionOrderssincegolive] 
AS


Select
	RM2Number = vis.IDCode
	,PatientName = vis.ClientDisplayName
	,Sex = pat.GenderCode
	,DOB = dbo.fn_SCM_DOB(pat.BirthYearNum,pat.BirthMonthNum,pat.BirthDayNum)
	,RequestedDateTime = src.RequestedDtm
	,SignificantDtm
	,Exam = Case When an.[Name] Is Null Then '' Else LTRIM(RTRIM(an.[Name])) + ' ' End + Coalesce(cmi.[Name],'')
	,Consultant = vis.ProviderDisplayName
	,[Location] = vis.CurrentLocation
	,[Status] = stat.[Description]
	,RequestedBy = us.DisplayName
	,OrderGuid = src.GUID
	,Orderstatus = src.Status
	,OrderStatusCode = src.OrderStatusCode
	,frm.[Name]
	,Discipline = cou.Name
	,OrderID = src.IDCode
	,CASE 
	WHEN cou.Name = 'Respiratory'
	THEN 'Lung Function'
	WHEN cou.Name = 'Cardiology'
	THEN 'Cardiology' 
	ELSE 'ERROR'
	END AS 'Dataset'
	,CASE
	WHEN CASE WHEN an.[Name] Is Null Then '' Else LTRIM(RTRIM(an.[Name])) + ' ' End + Coalesce(cmi.[Name],'') = 'ECHO_ECH TTE'--'src.OrderStatusCode In ('PDVR')
	THEN 'TTE'
	ELSE 'Other'
	END AS 'SubDataset'
	,RecordCount = 1

From	 [$(EPR)].dbo.CV3AllOrdersVw src With (NoLock)

		Inner Join [$(EPR)].dbo.CV3ClientVisit vis With (NoLock)
			On src.ClientVisitGUID = vis.GUID

		Inner Join [$(EPR)].dbo.CV3Client pat With (NoLock)
			On vis.ClientGUID = pat.GUID

		Left Join [$(EPR)].dbo.CV3OrderStatus stat With (NoLock)
			On src.OrderStatusCode = stat.Code

		Left Join [$(EPR)].dbo.CV3OrderCatalogMasterItem cmi With (NoLock)
			On src.OrderCatalogMasterItemGUID = cmi.GUID

		Left Join [$(EPR)].dbo.CV3AncillaryName an With (NoLock)
			On cmi.GUID = an.MainCatItemGUID
			And an.CodingStd Like '%OUT'

		Left Join [$(EPR)].dbo.CV3OrderEntryForm frm With (NoLock)
			On src.OrderEntryFormGUID = frm.GUID

		Left Join [$(EPR)].dbo.CV3User us With (NoLock)
			On src.UserGUID = us.GUID

		Left Join dbo.CRISExam ex With (NoLock)
			On LTRIM(RTRIM(an.[Name])) = ex.ExamCode

		--Name field is where the code is found
		Left outer join [$(EPR)].dbo.CV3OrganizationalUnit cou
			On cmi.OrgUnitGUID = cou.GUID--to get the unit which 'owns' the test - e.g. Microbiology, Radiology, Cellular Pathology, etc.

Where SignificantDtm >= '2016-12-06 20:00:00.000'
AND
	(
	cou.Name = 'Respiratory'
	OR
	cou.Name = 'Cardiology'
	OR
	CASE WHEN an.[Name] Is Null Then '' Else LTRIM(RTRIM(an.[Name])) + ' ' End + Coalesce(cmi.[Name],'') = 'ECHO_ECH TTE'--'src.OrderStatusCode In ('PDVR')
	)
	and (src.OrderStatusCode<>'CANC' or src.OrderStatusCode is null)
--Where  Case When an.[Name] Is Null Then '' Else LTRIM(RTRIM(an.[Name])) + ' ' End + Coalesce(cmi.[Name],'') = 'ECHO_ECH TTE'--'src.OrderStatusCode In ('PDVR')