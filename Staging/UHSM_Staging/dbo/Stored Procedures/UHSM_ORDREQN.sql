﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		SCM Order Requisition

Notes:			Stored in SM1SCM-CPM1.PROD

Versions:		
				1.0.0.1 - 06/12/2016 - MT
					Combined the previously separate header and detail sprocs
					into this single sproc to get multiple orders for a single
					JobID to print on a separate page with a header for each.

				1.0.0.0 - 23/11/2016 - DBH/MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [dbo].[UHSM_ORDREQN]
	@JobID int
	,@DistrictNo varchar (255)
As

Declare @WaitTime varchar(20)
Declare @KnownAs varchar(255)
Declare @OrderedAs varchar(255)
Declare @MinValueThousandSeparator as varchar(40)

Set @WaitTime = '000:00:05'

-- Store the environment profile settings for the ordered as column.
Set @KnownAs =
	(Select [Value]
	From	[$(EPR)].dbo.HVCEnvProfile
	Where	IsHVCDefined = 1
			And IsLeafNode = 1
			And Code = 'DisplayPrefixKnownAs'
			And HierarchyCode = 'Orders')

Set @OrderedAs = 
	(Select [Value]
	From	[$(EPR)].dbo.HVCEnvProfile
	Where	IsHVCDefined = 1
			And IsLeafNode = 1
			And Code = 'DisplayPrefixOrderedAs'
			And HierarchyCode = 'Orders')

-- Test the CV3OrderStatusHistory table to see if data has been replicated into the table yet.
If Not Exists
	(Select	* 
	From	[$(EPR)].dbo.CV3OrderRequestByJoin_R src
			Inner Join [$(EPR)].dbo.CV3OrderStatusHistory his
				On src.ObjectGUID = his.GUID
	Where	src.JobID = @JobID)
Begin
	RAISERROR('No rows in CV3OrderStatusHistory for JobID %d.  Waiting  %s seconds...',9,-1,@JobID,@WaitTime)
	WAITFOR DELAY @WaitTime -- Wait 5 seconds if empty
End

If Not Exists
	(Select	* 
	From	[$(EPR)].dbo.CV3OrderRequestByJoin_R src
			Inner Join [$(EPR)].dbo.CV3OrderStatusHistory his
				On src.ObjectGUID = his.GUID
	Where	src.JobID = @JobID)
Begin
	RAISERROR ('No rows in CV3OrderStatusHistory for JobID %d.  Failed. ',9,-1,@JobID)
End

-- Retrieve environment profile orders|MinValueThousandSeparator
Set @MinValueThousandSeparator =
	(Select	[Value]
	From	[$(EPR)].dbo.hvcEnvProfile
	Where	IsHVCDefined = 1
			And IsLeafNode = 1
			And Code = 'MinValueThousandSeparator'
			And HierarchyCode = 'Orders')

;With Addr As (
	Select	Address1 = addr.Line1
			,Address2 = addr.Line2
			,Address3 = addr.Line3
			,Address4 = addr.City
			,Postcode = addr.PostalCode
			,RowNo = ROW_NUMBER() Over (Order By addr.TouchedWhen Desc)
	From	[$(EPR)].dbo.CV3OrderRequestByJoin_R src With (NoLock)

			Inner Join [$(EPR)].dbo.CV3OrderStatusHistory his With (NoLock)
				On src.ObjectGUID = his.GUID
		  
			Inner Join [$(EPR)].dbo.CV3AllOrdersVw ord With (NoLock)
				On his.OrderGUID = ord.GUID

			Inner Join [$(EPR)].dbo.CV3Address addr With (NoLock)
				On ord.ClientGUID = addr.ParentGUID

Where	src.JobID = @JobID
		And addr.Active = 1
		And addr.IsCurrent = 1
		And addr.[Status] = 'Active'
	),
	TotalOrders As (
	Select	TotalOrders = Count(*)
	From	[$(EPR)].dbo.CV3OrderRequestByJoin_R src With (NoLock)
	Where	src.JobID = @JobID
	),
	OrderNumber As (
	Select	RowNo = ROW_NUMBER() Over (Order By src.ObjectGUID)
			,src.ObjectGUID
	From	[$(EPR)].dbo.CV3OrderRequestByJoin_R src With (NoLock)
	Where	src.JobID = @JobID
	)

Select	
		RequestedBy = us.DisplayName
		,vis.CurrentLocation
		,DOB = DATEFROMPARTS(pat.BirthYearNum,pat.BirthMonthNum,pat.BirthDayNum)
		,Mobility = ord.TranspMethodCode
		,NHSNo = nhs.ClientIDCode
		,PatientAddress = dbo.UHSM_fn_AddressAsSingleString(
			addr.Address1
			,addr.Address2
			,addr.Address3
			,addr.Address4)
		,PatientName = vis.ClientDisplayName
		,addr.Postcode
		,[Priority] =
		    Case
				When ct.IsEventBased = 1 Then
					RTrim(ord.RequestedTime) + ' ' +
					Convert(varchar,ReqTimeValue) + ' ' +
					RTrim(ReqTimeUom) + ' ' +
					(SELECT ReferenceString from [$(EPR)].dbo.CV3EnumReference e
					WHERE ReqTimeEventModifier = e.EnumValue
					AND e.ColumnName = 'ReqTimeEventModifier'
					AND e.TableName = 'CV3Order') + ' ' +
					RTrim(ReqTimeEventCode)
				When ord.ReqCodedTime Is Not Null Then
					ord.RequestedTime +  ' (' +
					RTrim(ord.ReqCodedTime) + ') '
				Else ord.RequestedTime
			End
		,ord.RequestedDtm
		,RM2Number = vis.IDCode
		,Gender = pat.GenderCode
		,Age = dbo.UHSM_fn_AgeAsInteger(DATEFROMPARTS(pat.BirthYearNum,pat.BirthMonthNum,pat.BirthDayNum),GETDATE())
		,OrderName = ord.[Name] + '   (' + Convert(varchar(4),ordno.RowNo) + ' of ' + Convert(varchar(4),tot.TotalOrders) + ')'
		,fld.Label
		,[Value] = Case
			When di.DataTypeCode = 'Checkbox' And ud.Value = '1' Then 'Yes'
			When di.DataTypeCode = 'Checkbox' And ud.Value = '0' Then 'No'
			Else ud.[Value]
		End

From	[$(EPR)].dbo.CV3OrderRequestByJoin_R src With (NoLock)

		Inner Join [$(EPR)].dbo.CV3OrderStatusHistory his With (NoLock)
			On src.ObjectGUID = his.[GUID]
		  
		Inner Join [$(EPR)].dbo.CV3AllOrdersVw ord With (NoLock)
			On his.OrderGUID = ord.[GUID]

		Left Join [$(EPR)].dbo.CV3User us With (NoLock)
			On ord.UserGUID = us.[GUID]

		Left Join [$(EPR)].dbo.CV3CodedTime ct With (NoLock)
			On ord.RequestedTime = ct.[Name]

		Inner Join [$(EPR)].dbo.CV3ClientVisit vis With (NoLock)
			On ord.ClientVisitGUID = vis.[GUID]

		Inner Join [$(EPR)].dbo.CV3Client pat With (NoLock)
			On vis.ClientGUID = pat.[GUID]

		Left Join Addr addr With (NoLock)
			On addr.RowNo = 1

		Left Join [$(EPR)].dbo.CV3ClientID nhs With (NoLock)
			On vis.ClientGUID = nhs.ClientGUID
			And nhs.TypeCode = 'NHS Number'

		Left Join [$(EPR)].dbo.CV3OrderEntryField fld With (NoLock)
			On ord.OrderEntryFormGUID = fld.OrderEntryFormGUID

		Left Join [$(EPR)].dbo.CV3DataItem di With (NoLock)
			On fld.DataItemCode = di.Code

		Left Join [$(EPR)].dbo.CV3OrderUserData ud With (NoLock)
			On ord.GUID = ud.OrderGUID
			And fld.DataItemCode = ud.UserDataCode

		Cross Join TotalOrders tot With (NoLock)

		Left Join OrderNumber ordno With (NoLock)
			On src.ObjectGUID = ordno.ObjectGUID

Where	--src.JobID = @JobID -------change me
--nhs.ClientIDCode = @NHSNo
vis.IDCode = @DistrictNo
		And fld.IsVisible = 1
		And fld.Label Is Not Null
		-- Exclude header items
		And fld.DataItemCode Not In (
			'RequestedDate',
			'RequestedTime',	-- Priority
			'TranspMethodCode')	-- Mobility

Order By
		src.ObjectGUID,
		fld.[Sequence],
		fld.ColumnNumber