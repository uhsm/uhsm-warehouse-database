﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		[dbo].[UHSM_OrdersReports_UB] 

Notes:			Stored in LocalAdmin.

Versions:
				
				1.0.0.4 - 10/12/2016 - DBH
					Discrepancy identified between raw data and report data
					Corrected.
				
				1.0.0.3 - 07/12/2016 - GR
					Filter on request date instead of request datetime
					Add unreported orders to help with drill-through report

				1.0.0.2 - 25/11/2016 - DBH
					Moved to UHSM Staging 
					AND Date range limited to <= Today

				1.0.0.1 - 25/11/2016 - DBH
					Ammended RequestedMonthField

				1.0.0.0 - 24/11/2016 - DBH
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE PROC [dbo].[UHSM_OrdersReports_UB] 
AS

WITH [CTE_Reports] AS
(
SELECT  [Order].[GUID]
,1 as NoOfReports
,Count (1) as NoOfItems
FROM [$(EPR)].[dbo].CV3BasicObservation as [Result]

	LEFT JOIN [$(EPR)].[dbo].CV3Order AS [Order]
	ON [Result].[OrderGUID] = [Order].[GUID]

WHERE [Result].[Active] = 1
AND [Result].[IsHistory] = 0

GROUP BY [Order].[GUID]
)
---------------------------------------------------------------
SELECT

[OrderItem]
,[ItemGroup]
,[RequestedDate]
,[RequestedMonth]
,[RequestedFinancialYear]
,[Clinician]
,[Discipline]
,[IPorOP]
,[CurrentLocation]
,[TemporaryLocation]
,SUM ([NoOfOrders]) AS [NoOfOrders]
,SUM ([ReportedOrders]) AS [ReportedOrders]
,SUM ([UnreportedOrders]) AS [UnreportedOrders]
,SUM ([NoOfReportedItems]) AS [NoOfReportedItems]
,SUM ([AcknowledgedReports]) AS [AcknowledgedReports]
,SUM ([NoOfSpecimens]) AS [NoOfSpecimens]

---------------------------------------------------------------

FROM 
(
SELECT
[Order].GUID
,[Order].[Name] AS 'OrderItem'
,CASE
WHEN CodedTimeGroupCode IN ('LAB CP Times','LAB HAE Times','LAB Hist Times','LAB MICRO Times','Lab Times')
THEN 'Labs'
WHEN CodedTimeGroupCode IN ('CRD AMB Times','CRD DEVICE Times','CRD ECHO Times','CRD EXERCISE Times','CRD TILT Times')
THEN 'CRD'
WHEN CodedTimeGroupCode = 'Rad Times'
THEN 'Radiology'
WHEN CodedTimeGroupCode = 'ENDO Times'
THEN 'Endoscopy'
WHEN CodedTimeGroupCode = 'Pharm Times'
THEN 'Pharmacy'
WHEN CodedTimeGroupCode = 'Telemetry Times'
THEN 'Telemetry'
WHEN CodedTimeGroupCode IN ('Diet Meal Times','Nursing Times','Referral Times','General')
THEN 'Other'
WHEN CodedTimeGroupCode = 'BRONC Times'
THEN 'Bronchoscopy'
WHEN CodedTimeGroupCode = 'AUDIO Times'
THEN 'Audiology'
WHEN CodedTimeGroupCode IN ('Resp Times','LF Times')
THEN 'Respiratory'
WHEN CodedTimeGroupCode = 'Cardiology Times'
THEN 'Cardiology'
ELSE CodedTimeGroupCode
END AS 'ItemGroup'
,CAST ([Order].[RequestedDtm] AS DATE) AS 'RequestedDate'
,dateadd(day,-DATEPART(day,cast(cast(RequestedDtm as date) as datetime))+1,cast(cast(RequestedDtm as date) as datetime))  AS 'RequestedMonth'
,CASE 
	WHEN MONTH ([Order].[RequestedDtm]) <= 3 
	THEN CAST (YEAR(DATEADD(YEAR,-1,[Order].[RequestedDtm])) AS VARCHAR) + '/' + CAST (YEAR ([Order].[RequestedDtm]) AS VARCHAR)
	WHEN MONTH ([Order].[RequestedDtm]) > 3
	THEN CAST (YEAR ([Order].[RequestedDtm]) AS VARCHAR) + '/' + CAST (YEAR(DATEADD(YEAR,1,[Order].[RequestedDtm])) AS VARCHAR)
	ELSE 'ERROR'
	END AS RequestedFinancialYear
,[Provider].[DisplayName] AS 'Clinician'
,[Provider].[Discipline] AS 'Discipline'
,[Visit].[TypeCode] AS [IPorOP]
,[Visit].[CurrentLocation]
,[Visit].[TemporaryLocation]
,1 AS 'NoOfOrders'
,CASE 
		WHEN [CTE_Reports].[GUID] IS NOT NULL 
		THEN 1 
		ELSE 0 
		END AS 'ReportedOrders'
,CASE 
		WHEN [CTE_Reports].[GUID] IS NULL 
		THEN 1 
		ELSE 0 
		END AS 'UnreportedOrders'
,SUM (CASE 
		WHEN [CTE_Reports].[NoOfItems] IS NULL 
		THEN 0 
		ELSE [CTE_Reports].[NoOfItems]
		END) AS NoOfReportedItems
,CASE
		WHEN [SXACMPCompletionObject].[ObjectGUID] IS NOT NULL
		THEN 1 
		ELSE 0 
		END AS 'AcknowledgedReports'
,CASE
		WHEN [Specimen].[GUID] IS NOT NULL 
		THEN 1 
		ELSE 0 
		END AS 'NoOfSpecimens'

FROM [$(EPR)].[dbo].[CV3Order] AS [Order]

LEFT JOIN [$(EPR)].[dbo].CV3DiagnosticExtension AS [Diagnostic]
ON [Order].[GUID] = [Diagnostic].[GUID]

LEFT JOIN [$(EPR)].[dbo].[CV3Specimen] AS [Specimen]
ON [Diagnostic].[SpecimenGUID] = [Specimen].[GUID]

LEFT JOIN [$(EPR)].[dbo].[CV3CareProvider] AS [Provider]
ON [Order].CareProviderGUID = [Provider].[GUID]

LEFT JOIN [$(EPR)].[dbo].[CV3ClientVisit] AS [Visit]
ON [Order].[ClientVisitGUID] = [Visit].[GUID]

LEFT JOIN [CTE_Reports] 
ON [CTE_Reports].[GUID] = [Order].[GUID]

LEFT JOIN [$(EPR)].[dbo].[SXACMPCompletionObject] 
ON [Order].[GUID] = [SXACMPCompletionObject].[ObjectGUID]
AND [SXACMPCompletionObject].[CompletionStatus] = 'Complete'

LEFT JOIN [$(EPR)].[dbo].[CV3OrderCatalogMasterItem]
ON [CV3OrderCatalogMasterItem].[GUID] = [Order].[OrderCatalogMasterItemGUID]

WHERE [Order].[Active] = 1
AND CAST([Order].[RequestedDtm] AS DATE) <= CAST (GETDATE () AS DATE)
--AND [Visit].[TypeCode]<>'Reference' --Excluding these causes code to run in a third of the time
GROUP BY 
[Order].[GUID]
,[Order].[Name]
,CASE
WHEN CodedTimeGroupCode IN ('LAB CP Times','LAB HAE Times','LAB Hist Times','LAB MICRO Times','Lab Times')
THEN 'Labs'
WHEN CodedTimeGroupCode IN ('CRD AMB Times','CRD DEVICE Times','CRD ECHO Times','CRD EXERCISE Times','CRD TILT Times')
THEN 'CRD'
WHEN CodedTimeGroupCode = 'Rad Times'
THEN 'Radiology'
WHEN CodedTimeGroupCode = 'ENDO Times'
THEN 'Endoscopy'
WHEN CodedTimeGroupCode = 'Pharm Times'
THEN 'Pharmacy'
WHEN CodedTimeGroupCode = 'Telemetry Times'
THEN 'Telemetry'
WHEN CodedTimeGroupCode IN ('Diet Meal Times','Nursing Times','Referral Times','General')
THEN 'Other'
WHEN CodedTimeGroupCode = 'BRONC Times'
THEN 'Bronchoscopy'
WHEN CodedTimeGroupCode = 'AUDIO Times'
THEN 'Audiology'
WHEN CodedTimeGroupCode IN ('Resp Times','LF Times')
THEN 'Respiratory'
WHEN CodedTimeGroupCode = 'Cardiology Times'
THEN 'Cardiology'
ELSE CodedTimeGroupCode
END
,CAST ([Order].[RequestedDtm] AS DATE)
,dateadd(day,-DATEPART(day,cast(cast(RequestedDtm as date) as datetime))+1,cast(cast(RequestedDtm as date) as datetime)) 
,CASE 
WHEN MONTH ([Order].[RequestedDtm]) <= 3 
THEN CAST (YEAR(DATEADD(YEAR,-1,[Order].[RequestedDtm])) AS VARCHAR) + '/' + CAST (YEAR ([Order].[RequestedDtm]) AS VARCHAR)
WHEN MONTH ([Order].[RequestedDtm]) > 3
THEN CAST (YEAR ([Order].[RequestedDtm]) AS VARCHAR) + '/' + CAST (YEAR(DATEADD(YEAR,1,[Order].[RequestedDtm])) AS VARCHAR)
ELSE 'ERROR'
END
,[Provider].[DisplayName]
,[Provider].[Discipline]
,[Visit].[TypeCode]
,[Visit].[CurrentLocation]
,[Visit].[TemporaryLocation]
,CASE 
		WHEN [CTE_Reports].[GUID] IS NOT NULL 
		THEN 1 
		ELSE 0 
		END
,CASE 
		WHEN [CTE_Reports].[GUID] IS NULL 
		THEN 1 
		ELSE 0 
		END
,CASE
		WHEN [SXACMPCompletionObject].[ObjectGUID] IS NOT NULL
		THEN 1 
		ELSE 0 
		END
,CASE
		WHEN [Specimen].[GUID] IS NOT NULL 
		THEN 1 
		ELSE 0 
		END

) AS MasterDataset

GROUP BY 
[OrderItem]
,[ItemGroup]
,[RequestedDate]
,[RequestedMonth]
,[RequestedFinancialYear]
,[Clinician]
,[Discipline]
,[IPorOP]
,[CurrentLocation]
,[TemporaryLocation]
,[NoOfOrders]

ORDER BY 
[OrderItem]
,[ItemGroup]
,[RequestedDate]
,[RequestedMonth]
,[RequestedFinancialYear]
,[Clinician]
,[Discipline]
,[IPorOP]
,[CurrentLocation]
,[TemporaryLocation]
,[NoOfOrders]