﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		CRIS - Clinician List

Notes:			Stored in SCM2CM-CPM1.UHSM_Staging

Versions:		1.0.0.1 - 11/12/2016 - CM
					Added additional order statuses as per Jenny Armstrong email
				1.0.0.0 - 10/12/2016 - GR
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [dbo].[UHSM_PendingOrdersByDaysSinceOrder_ClinicianList_UB]
As

Select	Distinct
		Clinician = us.DisplayName
		
From	[$(EPR)].dbo.CV3AllOrdersVw src With (NoLock)

		Inner Join [$(EPR)].dbo.CV3ClientVisit vis With (NoLock)
			On src.ClientVisitGUID = vis.GUID

		Inner Join [$(EPR)].dbo.CV3Client pat With (NoLock)
			On vis.ClientGUID = pat.GUID

		Left Join [$(EPR)].dbo.CV3OrderStatus stat With (NoLock)
			On src.OrderStatusCode = stat.Code

		Left Join [$(EPR)].dbo.CV3OrderCatalogMasterItem cmi With (NoLock)
			On src.OrderCatalogMasterItemGUID = cmi.GUID

		Left Join [$(EPR)].dbo.CV3AncillaryName an With (NoLock)
			On cmi.GUID = an.MainCatItemGUID
			And an.CodingStd Like '%OUT'

		Left Join [$(EPR)].dbo.CV3OrderEntryForm frm With (NoLock)
			On src.OrderEntryFormGUID = frm.GUID

		Left Join [$(EPR)].dbo.CV3User us With (NoLock)
			On src.UserGUID = us.GUID

		--Name field is where the code is found
		Left outer join [$(EPR)].dbo.CV3OrganizationalUnit cou
			On cmi.OrgUnitGUID = cou.GUID--to get the unit which 'owns' the test - e.g. Microbiology, Radiology, Cellular Pathology, etc.



Where	--frm.[Name] Like 'RAD%'
--		cou.Name = 'Diagnostic Imaging'
		(src.OrderStatusCode in ('PEND',--Pending
								'PCOL',--Pending Collection
								'PDVR')--Pending Varification
								
		Or src.OrderStatusCode Is Null)
		And src.RequestedDtm>='06 dec 2016 20:00'

Union All

Select	'<All>' As Clinician

Order By Clinician