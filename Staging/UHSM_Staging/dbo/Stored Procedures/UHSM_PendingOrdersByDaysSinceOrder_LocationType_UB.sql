﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Downtime - CRIS - Location List

Notes:			Stored in SCM1CM-CPM1.UHSM_Staging

Versions:		
				1.0.0.0 - 10/12/2016 - GR
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[UHSM_PendingOrdersByDaysSinceOrder_LocationType_UB]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
Select	Distinct [Location] = vis.TypeCode

From	[$(EPR)].dbo.CV3AllOrdersVw src With (NoLock)

		Inner Join [$(EPR)].dbo.CV3ClientVisit vis With (NoLock)
			On src.ClientVisitGUID = vis.GUID

		Inner Join [$(EPR)].dbo.CV3Client pat With (NoLock)
			On vis.ClientGUID = pat.GUID

		Left Join [$(EPR)].dbo.CV3OrderStatus stat With (NoLock)
			On src.OrderStatusCode = stat.Code

		Left Join [$(EPR)].dbo.CV3OrderCatalogMasterItem cmi With (NoLock)
			On src.OrderCatalogMasterItemGUID = cmi.GUID

		Left Join [$(EPR)].dbo.CV3AncillaryName an With (NoLock)
			On cmi.GUID = an.MainCatItemGUID
			And an.CodingStd Like '%OUT'

		Left Join [$(EPR)].dbo.CV3OrderEntryForm frm With (NoLock)
			On src.OrderEntryFormGUID = frm.GUID

		Left Join [$(EPR)].dbo.CV3User us With (NoLock)
			On src.UserGUID = us.GUID

--Where	frm.[Name] Like 'RAD%'
--		And (src.OrderStatusCode In ('PEND','SCHD')
--		Or src.OrderStatusCode Is Null)
Where src.RequestedDtm>='06 dec 2016 20:00'

Union All

Select	'<All>' As Location

Order By Location

END