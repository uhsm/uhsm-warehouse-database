﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Downtime - CRIS - Status List

Notes:			Stored in SM1CCM-CPM1.UHSM_Staging

Versions:			1.0.0.0 - 24/11/2016 - CM
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [dbo].[UHSM_PendingOrdersByDaysSinceOrder_StatusList_UB]
As


Select distinct CV3OrderStatus.Code AS StatusCode,
				CV3OrderStatus.Description AS StatusDef
From  [$(EPR)].dbo.CV3Order
left outer join [$(EPR)].dbo.CV3OrderStatus
on OrderStatusCode = CV3OrderStatus.Code
Where CV3Order.OrderStatusCode in ('PEND','PCOL','PDVR')

Union all 

Select 'Unk' as StatusCode,
		'Unknown' As StatusDef