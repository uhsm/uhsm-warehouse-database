﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Show all orders pending by days since order

Notes:			Stored in SM2SCM-CPM1.UHSM_Staging

Versions:		1.0.0.1 - 11/12/2016 - CM
					Added additional order statuses as per Jenny Armstrong email
				1.0.0.0 - 10/12/2016 - GR
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [dbo].[UHSM_PendingOrdersByDaysSinceOrder_UB]
	@Location varchar(100)
	,@StatusCode varchar(100)
	,@RequestingClinician varchar(100)
--	,@Modality varchar(100)
--	,@StartDate datetime
--	,@EndDate datetime
	,@LocationType varchar(100)
	,@DaysSinceOrder integer
As

Select
	datediff(day,src.RequestedDtm,getdate()) as DaysSinceOrder
	,RM2Number = vis.IDCode
	,PatientName = vis.ClientDisplayName
	,Sex = pat.GenderCode
	,DOB = dbo.fn_SCM_DOB(pat.BirthYearNum,pat.BirthMonthNum,pat.BirthDayNum)
	,RequestedDateTime = src.RequestedDtm
	,Exam = Case When an.[Name] Is Null Then '' Else LTRIM(RTRIM(an.[Name])) + ' ' End + Coalesce(cmi.[Name],'')
	,Consultant = vis.ProviderDisplayName
	,[Location] = vis.CurrentLocation
	,[Status] = stat.[Description]
	,Modality = CASE WHEN src.Name in 
					('AUTOFLUORESCENCE BRONCHOSCOPY'
					,'BRONCH RESEARCH'
					,'BRONCHIAL THERMOPLASTY'
					,'BRONCHOSCOPIC LVRT'
					,'BRONCHOSCOPIC STENT'
					,'BRONCHOSCOPY '
					,'CHARTIS ASSESSMENT'
					,'DIAGNOSTIC PLEURAL ASPIRATION'
					,'DRAINAGE OF INDWELLING PLEURAL CATHETER'
					,'EBUS-TBNA'
					,'EBUS-TBLB (1.7mm probe)'
					,'EBUS-TBNA (1.7mm Probe)' --GR 14/12/16 Added as this name is used on EPR but not CRIS
					,'EBUS-TBNA (GA)' 
					,'Vocal Cord Inspection' --GR 15/12/16 Added as reqested by Dawn Hand
					,'ENDOSCOPY OF NASAL CAVITY/UPPER AIRWAY'
					,'INTRALUMINAL BRACHYTHERAPY'
					,'IPC INS FOR IPC-PLUS TRIAL'
					,'LGE BORE BLUNT DISSECTION CHES DRAIN INS'
					,'LOCAL ANAESTHETIC THORACOSCOPY'
					,'INSERTION OF INDWELLING PLEURAL CATHETER'
					,'LOCAL ANAESTHETIC THORACOSCOPY'
					,'MEDICAL THORACOSCOPY WITH BIOPSY'
					,'PHOTODYNAMIC THERAPY'
					,'RADIAL EBUS (2.6mm probe)'
					,'REMOVAL OF INDWELLING PLEURAL CATHETER'
					,'REVIEW FOR ENDOBRONCHIAL THERAPY'
					,'ROUTINE BRONCHOSCOPY WITH BLIND TBNA'
					,'ROUTINE BRONCHOSCOPY WITH  TBLB'
					,'ROUTINE BRONCHOSCOPY WITH  BAL'
					,'ROUTINE BRONCHOSCOPY WITH TBLB'
					,'ROUTINE BRONCHOSCOPY WITH BAL'
					,'SELDINGER CHEST DRAIN INSERTION'
					,'SUSPECTED FOREIGN BODY REMOVAL'
					,'TALC SLURRY'
					,'TEMPORARY PLEURAL DRAIN'
					,'THERAPEUTIC IRRIGATION & BRONC TOILETING'
					,'THERAPEUTIC PLEURAL ASPIRATION'
					,'THORACOSCOPIC TALC POUDRAGE'
					,'ULTRASOUND GUIDED ASPIRATION OF THORAX'
					,'ULTRASOUND GUIDED DRAINAGE THORAX '
					,'US GUIDED PERCUTANEOUS PLEURAL BIOPSY'
					) 
				THEN 'Bronchoscopy'
				ELSE Coalesce(ex.Modality,'Other')
				END
	,RequestedBy = us.DisplayName
	,OrderGuid = src.GUID
	,Orderstatus = src.Status
	,OrderStatusCode =  CASE WHEN src.OrderStatusCode is NULL THEN 'Unk' ELSE src.OrderStatusCode END
	,frm.[Name]
	,Discipline = cou.Name
	,OrderID = src.IDCode

From	 [$(EPR)].dbo.CV3AllOrdersVw src With (NoLock)

		Inner Join [$(EPR)].dbo.CV3ClientVisit vis With (NoLock)
			On src.ClientVisitGUID = vis.GUID

		Inner Join [$(EPR)].dbo.CV3Client pat With (NoLock)
			On vis.ClientGUID = pat.GUID

		Left Join [$(EPR)].dbo.CV3OrderStatus stat With (NoLock)
			On src.OrderStatusCode = stat.Code

		Left Join [$(EPR)].dbo.CV3OrderCatalogMasterItem cmi With (NoLock)
			On src.OrderCatalogMasterItemGUID = cmi.GUID

		Left Join [$(EPR)].dbo.CV3AncillaryName an With (NoLock)
			On cmi.GUID = an.MainCatItemGUID
			And an.CodingStd Like '%OUT'

		Left Join [$(EPR)].dbo.CV3OrderEntryForm frm With (NoLock)
			On src.OrderEntryFormGUID = frm.GUID

		Left Join [$(EPR)].dbo.CV3User us With (NoLock)
			On src.UserGUID = us.GUID

		Left Join CRISExam ex With (NoLock)
			On LTRIM(RTRIM(an.[Name])) = ex.ExamCode

		--Name field is where the code is found
		Left outer join [$(EPR)].dbo.CV3OrganizationalUnit cou
			On cmi.OrgUnitGUID = cou.GUID--to get the unit which 'owns' the test - e.g. Microbiology, Radiology, Cellular Pathology, etc.


Where	--frm.[Name] Like 'RAD%'
--		cou.Name = 'Diagnostic Imaging'
		CASE WHEN src.OrderStatusCode is NULL THEN 'Unk' ELSE src.OrderStatusCode END in (SELECT Item
																						  FROM   dbo.Split (@StatusCode, ','))
								--('PEND',--Pending
								--	'PCOL', --Pending Collection
								--	'PDVR'--Pending Verification
								--	)
		--Or src.OrderStatusCode Is Null)
		And src.RequestedDtm>='06 dec 2016 20:00'
		And datediff(day,src.RequestedDtm,getdate())>=@DaysSinceOrder
		And	 (vis.CurrentLocation = @Location Or @Location = '<All>')
		--And (src.OrderStatusCode = @StatusCode Or @StatusCode = '<All>')
		And (us.DisplayName = @RequestingClinician Or @RequestingClinician = '<All>')
		--And (CASE WHEN src.Name in 
		--			('AUTOFLUORESCENCE BRONCHOSCOPY'
		--			,'BRONCH RESEARCH'
		--			,'BRONCHIAL THERMOPLASTY'
		--			,'BRONCHOSCOPIC LVRT'
		--			,'BRONCHOSCOPIC STENT'
		--			,'BRONCHOSCOPY '
		--			,'CHARTIS ASSESSMENT'
		--			,'DIAGNOSTIC PLEURAL ASPIRATION'
		--			,'DRAINAGE OF INDWELLING PLEURAL CATHETER'
		--			,'EBUS-TBNA'
		--			,'EBUS-TBLB (1.7mm probe)'
		--			,'EBUS-TBNA (GA)'
		--			,'ENDOSCOPY OF NASAL CAVITY/UPPER AIRWAY'
		--			,'INTRALUMINAL BRACHYTHERAPY'
		--			,'IPC INS FOR IPC-PLUS TRIAL'
		--			,'LGE BORE BLUNT DISSECTION CHES DRAIN INS'
		--			,'LOCAL ANAESTHETIC THORACOSCOPY'
		--			,'INSERTION OF INDWELLING PLEURAL CATHETER'
		--			,'LOCAL ANAESTHETIC THORACOSCOPY'
		--			,'MEDICAL THORACOSCOPY WITH BIOPSY'
		--			,'PHOTODYNAMIC THERAPY'
		--			,'RADIAL EBUS (2.6mm probe)'
		--			,'REMOVAL OF INDWELLING PLEURAL CATHETER'
		--			,'REVIEW FOR ENDOBRONCHIAL THERAPY'
		--			,'ROUTINE BRONCHOSCOPY WITH BLIND TBNA'
		--			,'ROUTINE BRONCHOSCOPY WITH  TBLB'
		--			,'ROUTINE BRONCHOSCOPY WITH  BAL'
		--			,'ROUTINE BRONCHOSCOPY WITH TBLB'
		--			,'ROUTINE BRONCHOSCOPY WITH BAL'
		--			,'SELDINGER CHEST DRAIN INSERTION'
		--			,'SUSPECTED FOREIGN BODY REMOVAL'
		--			,'TALC SLURRY'
		--			,'TEMPORARY PLEURAL DRAIN'
		--			,'THERAPEUTIC IRRIGATION & BRONC TOILETING'
		--			,'THERAPEUTIC PLEURAL ASPIRATION'
		--			,'THORACOSCOPIC TALC POUDRAGE'
		--			,'ULTRASOUND GUIDED ASPIRATION OF THORAX'
		--			,'ULTRASOUND GUIDED DRAINAGE THORAX '
		--			,'US GUIDED PERCUTANEOUS PLEURAL BIOPSY'
		--			) 
		--		THEN 'Bronchoscopy'
		--		ELSE Coalesce(ex.Modality,'Other')
		--		END = @Modality Or @Modality = '<All>')
		--And Cast(src.RequestedDtm as Date)>= @StartDate
		--And Cast(src.RequestedDtm as Date) <= @EndDate
		And (vis.TypeCode = @LocationType Or @LocationType = '<All>')





Order By src.RequestedDtm