﻿




CREATE Procedure [dbo].[UHSM_ReportHIVScreenEPRRequestsFromACU_UB] 

@WeekEndingOfRequest nvarchar(max)
,@RequestStatus nvarchar(max)
as

--If exists 
--      (
--      select * 
--      from tempdb.INFORMATION_SCHEMA.TABLES 
--      Where TABLE_SCHEMA = 'dbo' 
--      and TABLE_NAME like '#LocationOrder%'
--      )
--drop table #LocationOrder


--select CV3ClientVisitLocation.GUID as ClientVisitLocationGUID
--,CV3ClientVisitLocation.ClientLocnTypeCode
--,CV3ClientVisitLocation.Status
--,CV3ClientVisitLocation.ClientVisitGUID
--,CV3ClientVisitLocation.LocationGUID
--,CV3ClientVisitLocation.TransferRequestDtm
--,CV3ClientVisitLocation.ClientGUID
----,CV3ClientVisitLocation.LocationName
--,CV3Location.Name
--,ROW_NUMBER() over (partition by CV3ClientVisitLocation.ClientVisitGUID order by CV3ClientVisitLocation.TransferRequestDtm) as RowNum
----,CV3Location.*
--into #LocationOrder
--from [$(EPR)].dbo.CV3ClientVisitLocation
--left join [$(EPR)].dbo.CV3Location
--on CV3ClientVisitLocation.LocationGUID = CV3Location.GUID
----where ClientVisitGUID='5322900270'
----where IDCode='1008'
--order by ClientVisitGUID, TransferRequestDtm



;With Obs As (
	Select	RowNo = ROW_NUMBER() Over (Partition By src.OrderGUID Order By src.GUID Desc)
			,src.*
	From	[$(EPR)].dbo.CV3BasicObservation src
	Where	src.IsHistory = 0
	and ItemName not like '%Autofile%'
	),

	LocationOrder As (
	select CV3ClientVisitLocation.GUID as ClientVisitLocationGUID
	,CV3ClientVisitLocation.ClientLocnTypeCode
	,CV3ClientVisitLocation.Status
	,CV3ClientVisitLocation.ClientVisitGUID
	,CV3ClientVisitLocation.LocationGUID
	,CV3ClientVisitLocation.TransferRequestDtm
	,CV3ClientVisitLocation.ClientGUID
	--,CV3ClientVisitLocation.LocationName
	,CV3Location.Name
	,ROW_NUMBER() over (partition by CV3ClientVisitLocation.ClientVisitGUID order by CV3ClientVisitLocation.TransferRequestDtm) as RowNum
	--,CV3Location.*
	from [$(EPR)].dbo.CV3ClientVisitLocation
	left join [$(EPR)].dbo.CV3Location
	on CV3ClientVisitLocation.LocationGUID = CV3Location.GUID
	--where ClientVisitGUID='5322900270'
	--where IDCode='1008'
	)

--NEW EPR VERSION
SELECT 
--DISTINCT 
case when datename(weekday,cast(cast(RequestedDtm as date) as datetime))='Monday' then dateadd(day,6,cast(cast(RequestedDtm as date) as datetime))
			when datename(weekday,cast(cast(RequestedDtm as date) as datetime))='Tuesday' then dateadd(day,5,cast(cast(RequestedDtm as date) as datetime))
			when datename(weekday,cast(cast(RequestedDtm as date) as datetime))='Wednesday' then dateadd(day,4,cast(cast(RequestedDtm as date) as datetime))
			when datename(weekday,cast(cast(RequestedDtm as date) as datetime))='Thursday' then dateadd(day,3,cast(cast(RequestedDtm as date) as datetime))
			when datename(weekday,cast(cast(RequestedDtm as date) as datetime))='Friday' then dateadd(day,2,cast(cast(RequestedDtm as date) as datetime))
			when datename(weekday,cast(cast(RequestedDtm as date) as datetime))='Saturday' then dateadd(day,1,cast(cast(RequestedDtm as date) as datetime))
			when datename(weekday,cast(cast(RequestedDtm as date) as datetime))='Sunday' then dateadd(day,0,cast(cast(RequestedDtm as date) as datetime))
			else null end as WeekEndingOfRequest
,CV3Order.[Name] as Test_Name
--,startlocation.Name as LocationName
,vwLocationHierarchy.LocationLevel3 as LocationName

,IDSet1.ClientIDCode as FacilID
--,cast(cast(RequestedDtm as date) as datetime) as RequestDate
,RequestedDtm as RequestDate
,case when CV3Order.[Status] is null then 'Unknown' else CV3Order.[Status] end as RequestStatus
--,CV3Order.ClientVisitGUID
,CV3Order.GUID
--,cast(cast(min(CV3BasicObservation.Entered) as date) as datetime) as ReportDate
,Obs.Entered as ReportDate
--,min(CV3BasicObservation.Entered) as ReportDate
--,min(CV3BasicObservation.ArrivalDtm) as ReportDate
--,min(CV3BasicObservation.ObservationDtm) as ReportDate
--cal.LastDayOfWeek as WeekEndingOfRequest
--,pat.NHSNumber
--,prov.ProviderDiscipline
--      ,pat.ICEHospitalNumber
--      ,pat.FacilID
--      ,loc.LocationType
--      ,loc.LocationName
--      ,rq.RequestIndex
--      ,rep.ReportDate
--      ,RequestDate + cast(RequestTime AS DATETIME) AS RequestDateTime
--      ,cast(RequestDate as DATETIME) as RequestDate
--      ,datename(weekday,RequestDate) as RequestDayOfWeek
--      ,cl.ClinicianForename + ' ' + cl.ClinicianSurname AS RequestingClinician -- this is the clinician under which the test was ordered
--      ,prov.ProviderType 
--      ,rq.SetRequested
--      ,tests.Service_Request_Test_Index
----      ,tests.Test_Code
--      ,tests.Test_Name
--      ,(case when CV3Order.[Status] is null then 'Unknown' else CV3Order.[Status] end) as RequestStatus
      
--      --to get the request status
--      , RequestStatusMap.RequestStatusICECode
--      , case when RequestStatusMap.RequestStatus is null then 'Unknown' else RequestStatusMap.RequestStatus end as RequestStatus

--      ,Case when rq.RequestStatusCode=7 then 'Deleted request' else 'Other' end as [Deleted Request?]
--      ,srd.UserName--user who ordered the test
--   --   ,RP.Prompt_Desc--this is the prompt which displays on screen to ask user to enter MDT date
--   --   ,cast(ServiceRequestTestInformation.value AS DateTime) as MDTDate -- this is the MDT Date
--   --   ,datename(weekday,cast(ServiceRequestTestInformation.value AS DateTime)) as MDTDayOfWeek      
--      --,Case when DATEDIFF(minute,RequestDate + cast(RequestTime AS DATETIME),cast(ServiceRequestTestInformation.value AS Date) + CAST('09:00' as DATETIME))
--      --<64*60
--      --then '< 64 Hrs'
--      --when DATEDIFF(minute,RequestDate + cast(RequestTime AS DATETIME),cast(ServiceRequestTestInformation.value AS Date) + CAST('09:00' as DATETIME))
--      -->=64*60
--      --then '>= 64 Hrs' else null end as TimeBetweenRequestAnd9amOnMDTDate
      
      
----,case when rq.RequestDate between '01 jan 2014' and '31 jul 2014' then '1st Jan 2014 - 31st Jul 2014'
----when rq.RequestDate between '01 jan 2015' and '31 jul 2015' then '1st Jan 2015 - 31st Jul 2015' else null end as RequestPeriod
----INTO #Req

----,srd.*
----,tests.*




FROM [$(EPR)].dbo.CV3Order
LEFT JOIN [$(EPR)].dbo.CV3Client ON CV3Order.ClientGUID = CV3Client.GUID

LEFT JOIN [$(EPR)].dbo.CV3ClientID AS IDSet1 ON CV3Order.ClientGUID = IDSet1.ClientGUID
AND  IDSet1.TypeCode = 'Hospital Number'
AND IDStatus = 'ACT'
--LEFT JOIN [$(EPR)].dbo.CV3ClientID AS IDset2 ON CV3Order.ClientGUID = IDset2.ClientGUID
--AND  IDset2.TypeCode = 'NHS Number'

--LEFT JOIN [$(EPR)].dbo.CV3Phone ON CV3Phone.PersonGUID = CV3Client.GUID

--LEFT JOIN [$(EPR)].dbo.CV3Address ON CV3Client.GUID = CV3Address.ParentGUID

LEFT JOIN [$(EPR)].dbo.CV3ClientVisit ON CV3ClientVisit.GUID = CV3Order.ClientVisitGUID

--LEFT JOIN [$(EPR)].dbo.CV3User ON CV3Order.UserGUID = CV3User.GUID

--LEFT JOIN [$(EPR)].dbo.CV3Phone AS UserPhone ON CV3Order.UserGUID = UserPhone.PersonGUID


--LEFT JOIN [$(EPR)].dbo.CV3PhysicalNoteDeclaration ON CV3ClientVisit.GUID = CV3PhysicalNoteDeclaration.ClientVisitGUID
--AND CV3PhysicalNoteDeclaration.TypeCode = 'WEIGHT'

--LEFT JOIN [$(EPR)].dbo.CV3OrderUserData AS WeightDataset
--ON WeightDataset.OrderGUID = CV3Order.GUID
--AND WeightDataset.UserDataCode = 'Weight'

--LEFT JOIN [$(EPR)].dbo.CV3OrderUserData AS InformedDataset
--ON InformedDataset.OrderGUID = CV3Order.GUID
--AND InformedDataset.UserDataCode = 'Rad PT Informed'

--LEFT JOIN [$(EPR)].dbo.CV3OrderUserData AS DiabeticDataset
--ON DiabeticDataset.OrderGUID = CV3Order.GUID
--AND DiabeticDataset.UserDataCode = 'Rad Diabetic Question'

--LEFT JOIN [$(EPR)].dbo.CV3OrderUserData AS IodineDataset
--ON IodineDataset.OrderGUID = CV3Order.GUID
--AND IodineDataset.UserDataCode = 'Rad Iodine/x-ray allergy'

--LEFT JOIN [$(EPR)].dbo.CV3OrderUserData AS ThyroidDataset
--ON ThyroidDataset.OrderGUID = CV3Order.GUID
--AND ThyroidDataset.UserDataCode = 'Rad overactive thyroid'

--LEFT JOIN [$(EPR)].dbo.CV3OrderUserData AS LeafletDataset
--ON LeafletDataset.OrderGUID = CV3Order.GUID
--AND LeafletDataset.UserDataCode = 'Rad PT Leaflet'

--LEFT JOIN [$(EPR)].dbo.CV3OrderUserData AS ReasonDataset
--ON ReasonDataset.OrderGUID = CV3Order.GUID
--AND ReasonDataset.UserDataCode = 'Rad_ReasonsforExam'

LEFT JOIN LocationOrder startlocation ON CV3ClientVisit.GUID = startlocation.ClientVisitGUID
and CV3Order.RequestedDtm >= startlocation.TransferRequestDtm

LEFT JOIN LocationOrder nextlocation ON nextlocation.ClientVisitGUID = startlocation.ClientVisitGUID
and nextlocation.RowNum = startlocation.RowNum+1
and CV3Order.RequestedDtm < nextlocation.TransferRequestDtm



		Left Join Obs obs
			On obs.RowNo = 1
			And CV3Order.GUID = obs.OrderGUID

LEFT JOIN dbo.vwLocationHierarchy
on vwLocationHierarchy.LocationGUID = startlocation.LocationGUID

WHERE 
--srd.UserName = 'pkeast'
--      AND 
--      RequestDate >= '20140331' 
--      and RequestDate<='20150208'
--and cal.WeekNoKey<=(select WeekNoKey from WHREPORTING.LK.Calendar where thedate=DATEADD(day,DATEDIFF(day,0,GETDATE())-1,-6))
--rq.RequestDate>='01 jan 2015'


--and cal.LastDayOfWeek in(@WeekEndingOfRequest)

--cast(cast(cal.LastDayOfWeek as int) as nvarchar(max)) in(SELECT Item FROM WHREPORTING.dbo.Split (@WeekEndingOfRequest, ','))

(startlocation.ClientVisitGUID is null
or (CV3Order.RequestedDtm >= startlocation.TransferRequestDtm
and (nextlocation.ClientVisitGUID is null or CV3Order.RequestedDtm < nextlocation.TransferRequestDtm)))



and cast((
case when datename(weekday,cast(cast(RequestedDtm as date) as datetime))='Monday' then dateadd(day,6,cast(cast(RequestedDtm as date) as datetime))
			when datename(weekday,cast(cast(RequestedDtm as date) as datetime))='Tuesday' then dateadd(day,5,cast(cast(RequestedDtm as date) as datetime))
			when datename(weekday,cast(cast(RequestedDtm as date) as datetime))='Wednesday' then dateadd(day,4,cast(cast(RequestedDtm as date) as datetime))
			when datename(weekday,cast(cast(RequestedDtm as date) as datetime))='Thursday' then dateadd(day,3,cast(cast(RequestedDtm as date) as datetime))
			when datename(weekday,cast(cast(RequestedDtm as date) as datetime))='Friday' then dateadd(day,2,cast(cast(RequestedDtm as date) as datetime))
			when datename(weekday,cast(cast(RequestedDtm as date) as datetime))='Saturday' then dateadd(day,1,cast(cast(RequestedDtm as date) as datetime))
			when datename(weekday,cast(cast(RequestedDtm as date) as datetime))='Sunday' then dateadd(day,0,cast(cast(RequestedDtm as date) as datetime))
			else null end
			) as int) in(SELECT Item FROM dbo.Split ((@WeekEndingOfRequest), ','))


--cast(cast(cal.FirstDateOfMonth as int) as nvarchar(max)) in(SELECT Item FROM WHREPORTING.dbo.Split (@FirstDateOfMonth, ','))

--cast(cal.FirstDateOfMonth as int) in(SELECT Item FROM WHREPORTING.dbo.Split (@FirstDateOfMonth, ','))

--cal.FirstDateOfMonth='01 feb 2016'

--      AND RequestDate < '20140930'
--      AND rq.RequestStatusCode <> 7 -- only those requesst which have not been deleted    
--and tests.Test_Name like '%Ambulatory%'
--and (tests.Test_Name like '%EEG%'
--or (tests.Test_Name like '%ELECTRO MYOGRAPHY%'
--or (tests.Test_Name like '%NERVE CONDUCTION STUDY%'
--or (tests.Test_Name like '%Carpal Tunnel Syndrome%'))))
--and tests.Test_Code in
--(
--'EFOBR'
--,'ZEFOBR1'
--,'ZEFOBR2'
--,'ZEFOBR3'
--,'ZEFOBR4'
--,'ZUCHESA'
--,'ZUCHESB'
--,'ZUCHESC'
--,'ZUCHESR'
--,'ZPDYT'
--,'ZEBT'
--,'ZCHARTA'
--,'ZEFOBR5'
--,'ZETHOC2'
--,'ZETHOC3'
--,'ZETHOC4'
--,'ZETHOC9'
--,'ZETHOC8'
--,'ZETHOC5'
--,'ZETHOC6'
--,'ZETHOC7'
--,'ZETHOC9'
--,'ZBRRS'
--,'CLUNGB'
--,'CBIOPB')

and (CV3Order.[Name] = 'HIV' or CV3Order.[Name] = 'HIV Screen')
and vwLocationHierarchy.LocationLevel3='AMRU'
-- in
--(
--'CT GUIDED BIOPSY'
--,'CT CHEST AND BIOPSY'
--,'BRONCHOSCOPY'
--,'BRONCH RESEARCH'
--,'CHARTIS ASSESSMENT'
--,'REVIEW FOR ENDOBRONCHIAL THERAPY'
--,'ROUTINE BRONCHOSCOPY WITH BLIND TBNA'
--,'ROUTINE BRONCHOSCOPY WITH  TBLB'
--,'ROUTINE BRONCHOSCOPY WITH  BAL'
--,'ROUTINE BRONCHOSCOPY WITH TBLB'
--,'ROUTINE BRONCHOSCOPY WITH BAL'
--,'AUTOFLUORESCENCE BRONCHOSCOPY'
--,'BRONCHIAL THERMOPLASTY'
--,'DIAGNOSTIC PLEURAL ASPIRATION'
--,'THERAPEUTIC PLEURAL ASPIRATION'
--,'LOCAL ANAESTHETIC THORACOSCOPY'
--,'US GUIDED PERCUTANEOUS PLEURAL BIOPSY'
--,'SELDINGER CHEST DRAIN INSERTION'
--,'LGE BORE BLUNT DISSECTION CHES DRAIN INS'
--,'DRAINAGE OF INDWELLING PLEURAL CATHETER'
--,'INSERTION OF INDWELLING PLEURAL CATHETER'
--,'IPC INS FOR IPC-PLUS TRIAL'
--,'PHOTODYNAMIC THERAPY'
--,'EBUS-TBNA'
--,'EBUS-TBLB (1.7mm probe)'
--,'EBUS-TBNA (GA)'
--,'RADIAL EBUS (2.6mm probe)'
--)
--and loc.LocationName='ACU Ambulatory Care Unit'
--and (case when RequestStatusMap.RequestStatus is null then 'Unknown' else RequestStatusMap.RequestStatus end) in(SELECT Item FROM WHREPORTING.dbo.Split ((@RequestStatus), ','))


and (case when CV3Order.[Status] is null then 'Unknown' else CV3Order.[Status] end) in(SELECT Item FROM dbo.Split ((@RequestStatus), ','))



--and (case when RequestStatusMap.RequestStatus is null then 'Unknown' else RequestStatusMap.RequestStatus end) in(@RequestStatus)
--and cl.ClinicianForename + ' ' + cl.ClinicianSurname <> 'X Xxtest'


and RequestedDtm IS NOT NULL

--group by
--CV3Order.[Name]
--,startlocation.Name
--,IDSet1.ClientIDCode
----,cast(cast(RequestedDtm as date) as datetime)
--,RequestedDtm
--,CV3Order.[Status]
----,CV3Order.ClientVisitGUID
--,CV3Order.GUID

order by 
FacilID
,RequestedDtm




----OLD ICE VERSION
--SELECT 
--DISTINCT 
----cal.WeekNoKey
----,cal.WeekNo
--cal.LastDayOfWeek as WeekEndingOfRequest
--,pat.NHSNumber
--,prov.ProviderDiscipline
--      ,pat.ICEHospitalNumber
--      ,pat.FacilID
--      ,loc.LocationType
--      ,loc.LocationName
--      ,rq.RequestIndex
--      ,rep.ReportDate
--      ,RequestDate + cast(RequestTime AS DATETIME) AS RequestDateTime
--      ,cast(RequestDate as DATETIME) as RequestDate
--      ,datename(weekday,RequestDate) as RequestDayOfWeek
--      ,cl.ClinicianForename + ' ' + cl.ClinicianSurname AS RequestingClinician -- this is the clinician under which the test was ordered
--      ,prov.ProviderType 
--      ,rq.SetRequested
--      ,tests.Service_Request_Test_Index
--      ,tests.Test_Code
--      ,tests.Test_Name
--      ,rq.RequestStatusCode
      
--      --to get the request status
--      , RequestStatusMap.RequestStatusICECode
--      , case when RequestStatusMap.RequestStatus is null then 'Unknown' else RequestStatusMap.RequestStatus end as RequestStatus

--      ,Case when rq.RequestStatusCode=7 then 'Deleted request' else 'Other' end as [Deleted Request?]
--      ,srd.UserName--user who ordered the test
--   --   ,RP.Prompt_Desc--this is the prompt which displays on screen to ask user to enter MDT date
--   --   ,cast(ServiceRequestTestInformation.value AS DateTime) as MDTDate -- this is the MDT Date
--   --   ,datename(weekday,cast(ServiceRequestTestInformation.value AS DateTime)) as MDTDayOfWeek      
--      --,Case when DATEDIFF(minute,RequestDate + cast(RequestTime AS DATETIME),cast(ServiceRequestTestInformation.value AS Date) + CAST('09:00' as DATETIME))
--      --<64*60
--      --then '< 64 Hrs'
--      --when DATEDIFF(minute,RequestDate + cast(RequestTime AS DATETIME),cast(ServiceRequestTestInformation.value AS Date) + CAST('09:00' as DATETIME))
--      -->=64*60
--      --then '>= 64 Hrs' else null end as TimeBetweenRequestAnd9amOnMDTDate
      
      
----,case when rq.RequestDate between '01 jan 2014' and '31 jul 2014' then '1st Jan 2014 - 31st Jul 2014'
----when rq.RequestDate between '01 jan 2015' and '31 jul 2015' then '1st Jan 2015 - 31st Jul 2015' else null end as RequestPeriod
----INTO #Req

----,srd.*
----,tests.*
--FROM WHREPORTING.ICE.FactRequest rq
--LEFT JOIN WHREPORTING.ICE.Patient pat ON rq.MasterPatientCode = pat.PatientKey
--LEFT JOIN WHREPORTING.ICE.DimLocation loc ON rq.MasterLocationCode = loc.LocationKey
--LEFT JOIN WHREPORTING.ICE.DimProvider prov ON rq.ServiceProviderCode = prov.ProviderKey
--LEFT JOIN ICE_DB.dbo.OrderTests tests ON rq.RequestIndex = tests.Service_Request_Index
--LEFT JOIN ICE_DB.dbo.ServiceRequestDetail srd ON rq.RequestIndex = srd.Request_Index
--LEFT JOIN WHREPORTING.ICE.DimClinician cl ON rq.MasterClinicianCode = cl.ClinicianKey
--LEFT JOIN WHREPORTING.ICE.FactReport rep ON rep.RequestIndex = rq.RequestIndex
--left join WHREPORTING.LK.Calendar cal
--on rq.RequestDate=cal.TheDate
--/****The next join below allows us to pull through answers relating to questions which relate only to the MDT tests***********/

----LEFT Join (Select * from ICE_DB.dbo.ServiceRequestTestInformation -- this is the table where the value of the MDT data is stored
----                        where Test_Id = '2465'-- this is the id for the MDTLUNGCREF test
----                                                                                                            and remote_key = '1096' --this is the remote key id which will allow us to extract the row that specifically relates to the MDT Date entry
----                  )ServiceRequestTestInformation  on rq.RequestIndex = ServiceRequestTestInformation.Service_Request_Id
----Left outer join ICE_DB.dbo.Request_Prompt RP  --this is the table detailing the dofferent prompts which appear depending on which tests have been requested
----on ServiceRequestTestInformation.remote_key = RP.Prompt_Index

----to get the request status 
--Left outer join ICE_DB.dbo.RequestStatusMap on rq.RequestStatusCode = RequestStatusMap.RequestStatusKey


--WHERE 
----srd.UserName = 'pkeast'
----      AND 
----      RequestDate >= '20140331' 
----      and RequestDate<='20150208'
----and cal.WeekNoKey<=(select WeekNoKey from WHREPORTING.LK.Calendar where thedate=DATEADD(day,DATEDIFF(day,0,GETDATE())-1,-6))
----rq.RequestDate>='01 jan 2015'


----and cal.LastDayOfWeek in(@WeekEndingOfRequest)

--cast(cast(cal.LastDayOfWeek as int) as nvarchar(max)) in(SELECT Item FROM WHREPORTING.dbo.Split (@WeekEndingOfRequest, ','))



----      AND RequestDate < '20140930'
----      AND rq.RequestStatusCode <> 7 -- only those requesst which have not been deleted    
----and tests.Test_Name like '%Ambulatory%'
----and (tests.Test_Name like '%EEG%'
----or (tests.Test_Name like '%ELECTRO MYOGRAPHY%'
----or (tests.Test_Name like '%NERVE CONDUCTION STUDY%'
----or (tests.Test_Name like '%Carpal Tunnel Syndrome%'))))
--and tests.Test_Name like '%HIV Screen%'
--and loc.LocationName='ACUTE MEDICINE RECEIVING UNIT'--CM 13/04/2016 as per query from LMcjannett - amended from this location 'ACU Ambulatory Care Unit'
--and (case when RequestStatusMap.RequestStatus is null then 'Unknown' else RequestStatusMap.RequestStatus end) in(@RequestStatus)
----and cl.ClinicianForename + ' ' + cl.ClinicianSurname <> 'X Xxtest'

--order by RequestDate