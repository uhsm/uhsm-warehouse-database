﻿
/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		[dbo].[UHSM_UnsolicitedResults_UB] 

Notes:			Stored in LocalAdmin.

Versions:	1.0.0.1 - 08/12/2016 - CM 
				Removed CTE for Location as there is a table which can be used  		
			
			1.0.0.0 - 24/11/2016 - DBH
					Created sproc.
			
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE PROC [dbo].[UHSM_UnsolicitedResults_UB] 
AS


--WITH CTE_Location AS 
--(
--select 
--step1.GUID as LocationGUID
--,step1.Name
--,LocationLevel1=
--              case when step6.GUID is null then
--              (case when step5.GUID is null then
--              (case when step4.GUID is null then
--              (case when step3.GUID is null then
--              (case when step2.GUID is null then
--              (case when step1.GUID is null then
--              null
--              else step1.Name end)
--              else step2.Name end)
--              else step3.Name end)
--              else step4.Name end)
--              else step5.Name end)
--              else step6.Name end
--,LocationLevel2=
--              case when step6.GUID is null then
--              (case when step5.GUID is null then
--              (case when step4.GUID is null then
--              (case when step3.GUID is null then
--              (case when step2.GUID is null then
--              null
--              else step1.Name end)
--              else step2.Name end)
--              else step3.Name end)
--              else step4.Name end)
--              else step5.Name end
--,LocationLevel3=
--              case when step6.GUID is null then
--              (case when step5.GUID is null then
--              (case when step4.GUID is null then
--              (case when step3.GUID is null then
--              null
--              else step1.Name end)
--              else step2.Name end)
--              else step3.Name end)
--              else step4.Name end
--,LocationLevel4=
--              case when step6.GUID is null then
--              (case when step5.GUID is null then
--              (case when step4.GUID is null then
--              null
--              else step1.Name end)
--              else step2.Name end)
--              else step3.Name end
--,LocationLevel5=
--              case when step6.GUID is null then
--              (case when step5.GUID is null then
--              null
--              else step1.Name end)
--              else step2.Name end
--,LocationLevel6=
--              case when step6.GUID is null then
--              null
--              else step1.Name end
--from [$(EPR)].dbo.CV3Location step1
--left join [$(EPR)].dbo.CV3Location step2
--on step2.GUID=step1.ParentGUID
--left join [$(EPR)].dbo.CV3Location step3
--on step3.GUID=step2.ParentGUID
--left join [$(EPR)].dbo.CV3Location step4
--on step4.GUID=step3.ParentGUID
--left join [$(EPR)].dbo.CV3Location step5
--on step5.GUID=step4.ParentGUID
--left join [$(EPR)].dbo.CV3Location step6
--on step6.GUID=step5.ParentGUID
--)
SELECT
[IDSet1].[ClientIDCode] AS [Hospital No]
,[IDset2].[ClientIDCode] AS [NHS Number]
,[Client].[LastName] AS [Surname]
,[Client].[FirstName] AS [Forname]
,CASE 
WHEN ([Client].[BirthYearNum] = 0 OR [Client].[BirthMonthNum] = 0 OR [Client].[BirthDayNum] = 0)
THEN NULL
ELSE DATEFROMPARTS([Client].BirthYearNum,[Client].BirthMonthNum,[Client].BirthDayNum) 
END AS DOB
,[Visit].[TypeCode] AS [IPorOP]
,[LocationLevel3] as [WardOrDepartment]
,Result.[Entered]
,CAST ([Result].[Entered] AS DATE) AS 'ResultDate'
,DATENAME (MONTH,[Order].[Entered]) + ' ' + DATENAME(YEAR,[Result].[Entered]) AS 'ResultMonth'
,CASE 
	WHEN MONTH ([Result].[Entered]) <= 3 
	THEN CAST (YEAR(DATEADD(YEAR,-1,[Result].[Entered])) AS VARCHAR) + '/' + CAST (YEAR ([Result].[Entered]) AS VARCHAR)
	WHEN MONTH ([Result].[Entered]) > 3
	THEN CAST (YEAR ([Result].[Entered]) AS VARCHAR) + '/' + CAST (YEAR(DATEADD(YEAR,1,[Result].[Entered])) AS VARCHAR)
	ELSE 'ERROR'
	END AS 'ResultFinancialYear'
	,Result.[ItemName]
	,Result.[ResultItemCode]
	,result.[Value]
	

FROM [$(EPR)].[dbo].[CV3BasicObservation] AS [Result]

LEFT JOIN [$(EPR)].[dbo].[CV3Client] AS [Client]
ON [Result].[ClientGUID] = [Client].[GUID]

LEFT JOIN [$(EPR)].[dbo].[CV3ClientID] AS [IDSet1] 
ON [Result].[ClientGUID] = [IDSet1].[ClientGUID]
AND  [IDSet1].[TypeCode] = 'Hospital Number'

LEFT JOIN [$(EPR)].[dbo].[CV3ClientID] AS [IDset2]
ON [Result].[ClientGUID] = [IDset2].[ClientGUID]
AND  [IDset2].[TypeCode] = 'NHS Number'

LEFT JOIN [$(EPR)].[dbo].[CV3Order] AS [Order]
ON [Result].[OrderGUID] = [Order].[GUID]

LEFT JOIN [$(EPR)].[dbo].[CV3ClientVisit] AS [Visit]
ON [Result].[ClientVisitGUID] = [Visit].[GUID]

LEFT JOIN dbo.LocationHierarchy AS  [Location]
ON [Visit].[CurrentLocationGUID] = [Location].LocationGUID

WHERE [Result].[Active] = 1
AND [Result].[IsHistory] = 0
AND [Result].[Entered] <= getdate()
AND [Order].[GUID] IS NULL

ORDER BY CAST ([Result].[Entered] AS DATE)