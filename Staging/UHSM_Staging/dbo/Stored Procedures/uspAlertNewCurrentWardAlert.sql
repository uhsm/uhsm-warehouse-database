﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:	Populate a table in UHSM_staging database when a new location appears in current location field in the client visit table


Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:		1.0.0.0 - 10/12/2016 - CM
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE uspAlertNewCurrentWardAlert

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



Insert Into NewWardAlert
	(
	CurrentLocationGUID,
	CurrentLocation
	)



Select Distinct  CurrentLocationGUID,
				CurrentLocation
From [$(EPR)].dbo.CV3ClientVisit
Where TypeCode = 'Inpatient'
and CurrentLocation NOT IN 
			(
			Select [Value]
				From [$(EPR)].[dbo].[CV3UserDictionaryValue]
				Where [UserDictionaryCode] = 'wardlist'
			)

END