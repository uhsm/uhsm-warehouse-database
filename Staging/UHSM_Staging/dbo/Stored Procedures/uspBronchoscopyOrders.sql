﻿CREATE PROCEDURE [dbo].[uspBronchoscopyOrders]
	@LocationTypeCode varchar(100)
	,@LocationGUID float
AS

SELECT [Client].[GUID] AS 'ClientGUID'
	,[Client].[BirthDayNum]
	,[Client].[BirthMonthNum]
	,[Client].[BirthYearNum]
	,[dbo].[fn_SCM_DOB]([Client].[BirthYearNum],[Client].[BirthMonthNum],[Client].[BirthDayNum]) AS 'DOB'
	,[Client].[GenderCode]
	,[PTIDRM2].[ClientIDCode] AS 'RM2Number'
	,[PTIDNHS].[ClientIDCode] AS 'NHSNo'
	,[Visit].[GUID] AS 'OriginalVisitGUID'
	,[Visit].[AdmitDtm] AS 'OriginalVisitDtm'
	,[Visit].[ClientDisplayName]
	,[Visit].[ProviderDisplayName]
	,[Visit].[CurrentLocation] AS 'VisitLocation'
	,CASE 
		WHEN [Visit].[TemporaryLocation] IS NULL
			THEN [Visit].[CurrentLocation]
		ELSE [Visit].[TemporaryLocation]
		END AS 'Ward Patient Is On'
	,[Orders].[GUID] AS OrderGUID
	,[Orders].[IDCode] as OrderID
	,[Orders].[Name] AS OrderName
	,[Orders].[SignificantDtm]
	,[Orders].[SignificantTime]
	,[Orders].[IsConditional]
	,[Orders].[ConditionsText]
	,[Orders].[IsSuspended]
	,[Orders].[RepeatOrder]
	,[Orders].[ComplexOrderType]
	,[Item].[SpecimenType]
	,[Orders].[Status]
	,[Orders].[TypeCode]
	,[Orders].[OrderStatusCode]
	,[Orders].[RequestedDtm]
FROM [$(EPR)].[dbo].[CV3Order] AS [Orders]

INNER JOIN [$(EPR)].[dbo].[CV3ClientVisit] AS [Visit]
ON [Visit].[GUID] = [Orders].[ClientVisitGUID]

INNER JOIN [$(EPR)].[dbo].[CV3Client] AS [Client]
ON [Client].[GUID] = [Orders].[ClientGUID]

LEFT JOIN [$(EPR)].[dbo].[CV3ClientID] AS [PTIDRM2]
ON [PTIDRM2].ClientGUID = [Orders].[ClientGUID]
AND [PTIDRM2].TypeCode = 'Hospital Number'

LEFT JOIN [$(EPR)].[dbo].CV3ClientID AS [PTIDNHS] 
ON [PTIDNHS].[ClientGUID] = [Orders].[ClientGUID]
AND [PTIDNHS].[TypeCode] = 'NHS Number'
AND [PTIDNHS].[ClientIDCode] <> 'UNKNOWN'

LEFT JOIN [$(EPR)].[dbo].[CV3OrderCatalogMasterItem] AS [Item]
ON [Orders].[OrderCatalogMasterItemGUID] = [Item].[GUID]

WHERE-- PTIDRM2.ClientIDCode = @DistrictNo )
	-- AND 
[Orders].[Name] IN 
(
'CT GUIDED BIOPSY'
,'CT CHEST AND BIOPSY'
,'BRONCHOSCOPY'
,'BRONCH RESEARCH'
,'CHARTIS ASSESSMENT'
,'REVIEW FOR ENDOBRONCHIAL THERAPY'
,'ROUTINE BRONCHOSCOPY WITH BLIND TBNA'
,'ROUTINE BRONCHOSCOPY WITH  TBLB'
,'ROUTINE BRONCHOSCOPY WITH  BAL'
,'ROUTINE BRONCHOSCOPY WITH TBLB'
,'ROUTINE BRONCHOSCOPY WITH BAL'
,'AUTOFLUORESCENCE BRONCHOSCOPY'
,'BRONCHIAL THERMOPLASTY'
,'DIAGNOSTIC PLEURAL ASPIRATION'
,'THERAPEUTIC PLEURAL ASPIRATION'
,'LOCAL ANAESTHETIC THORACOSCOPY'
,'US GUIDED PERCUTANEOUS PLEURAL BIOPSY'
,'SELDINGER CHEST DRAIN INSERTION'
,'LGE BORE BLUNT DISSECTION CHES DRAIN INS'
,'DRAINAGE OF INDWELLING PLEURAL CATHETER'
,'INSERTION OF INDWELLING PLEURAL CATHETER'
,'IPC INS FOR IPC-PLUS TRIAL'
,'PHOTODYNAMIC THERAPY'
,'EBUS-TBNA'
,'EBUS-TBLB (1.7mm probe)'
					,'EBUS-TBNA (1.7mm Probe)' --GR 14/12/16 Added as this name is used on EPR but not CRIS
					,'EBUS-TBNA (GA)' 
					,'Vocal Cord Inspection' --GR 15/12/16 Added as reqested by Dawn Hand
,'RADIAL EBUS (2.6mm probe)'
)
AND [Orders].[RequestedDtm] >= '06 DEC 2016 20:00'
AND [Orders].[OrderStatusCode] IN (
 'PCOL' -- Pending Collection
,'PDVR' -- Pending Verification
,'PDVR1' -- Pending Verification Status 1
,'PDVR2' -- Pending Verification Status 2
,'PDVR3' -- Pending Verification Status 3
,'PDVR4' -- Pending Verification Status 4
,'PDVR5' -- Pending Verification Status 5
,'PDVR6' -- Pending Verification Status 6
,'PDVR7' -- Pending Verification Status 7
,'PDVR8' -- Pending Verification Status 8
,'PDVR9' -- Pending Verification Status 9
,'PEND' -- Pending
,'SCHD' -- Scheduled
)
AND	([Visit].TypeCode = @LocationTypeCode Or @LocationTypeCode = '<All>')
AND	([Visit].CurrentLocationGUID = @LocationGUID
	or	@LocationGUID = 0
	)
GO


