﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Bronchoscopy Requests

Notes:			Stored in SC1SCM-CPM1.UHSM_Staging

Versions:
				1.0.0.1 - 16/12/2016 - MT
					Previosuly UHSM_ReportBronchoscopyRequestsEPR_UB.
					Using CV3OrderIndex table to speed up execution.
					Created CV3OrderCatalogMasterItemGroup table with IsBronchoscopy field
					to replace hard-coded list of orders previously used.
					Display data for all months in the selected financial year.

				1.0.0.0 - GR
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure dbo.uspBronchoscopyRequests
	@FinancialYear varchar(9)
	,@OrderStatus varchar(max)
As

Select
	cal.FinancialMonthKey
	,cal.ShortMonth
	,ord.[Name]
	,[Status] = Coalesce(st.[Description],'Unknown')
	,Count(*) as NoOfOrders

From	CV3OrderIndex src

		Inner Join [$(EPR)].dbo.CV3Order ord
			On src.[GUID] = ord.[GUID]

		Inner Join Calendar cal
			On src.RequestedDate = cal.TheDate
			And cal.FinancialYear = @FinancialYear
			And cal.TheDate <= GETDATE()

		Inner Join CV3OrderCatalogMasterItemGroup cat
			On ord.OrderCatalogMasterItemGUID = cat.[GUID]
			And cat.IsBronchoscopy = 1

		Left Join [$(EPR)].dbo.CV3OrderStatus st
			On ord.OrderStatusCode = st.Code

		Inner Join dbo.Split(@OrderStatus,',') os
			On Coalesce(ord.OrderStatusCode,'UNK') = os.Item

Group By
	cal.FinancialMonthKey
	,cal.ShortMonth
	,ord.[Name]
	,Coalesce(st.[Description],'Unknown')

Order By
	cal.FinancialMonthKey
	,ord.[Name]
	,Coalesce(st.[Description],'Unknown')