﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Bronchoscopy Requests - Status List

Notes:			Stored in SC1SCM-CPM1.UHSM_Staging

Versions:
				1.0.0.1 - 16/12/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure dbo.uspBronchoscopyRequests_StatusList
	@FinancialYear varchar(9)
As

Select	Distinct
		StatusCode = Coalesce(ord.OrderStatusCode,'UNK')
		,StatusDef = Coalesce(st.[Description],'Unknown')

From	CV3OrderIndex src

		Inner Join [$(EPR)].dbo.CV3Order ord
			On src.[GUID] = ord.[GUID]

		Inner Join Calendar cal
			On src.RequestedDate = cal.TheDate
			And cal.FinancialYear = @FinancialYear
			And cal.TheDate <= GETDATE()

		Inner Join CV3OrderCatalogMasterItemGroup cat
			On ord.OrderCatalogMasterItemGUID = cat.[GUID]
			And cat.IsBronchoscopy = 1

		Left Join [$(EPR)].dbo.CV3OrderStatus st
			On ord.OrderStatusCode = st.Code

Order By Coalesce(st.[Description],'Unknown')