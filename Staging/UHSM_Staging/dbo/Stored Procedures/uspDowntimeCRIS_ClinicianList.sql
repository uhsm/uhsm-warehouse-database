﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Downtime - CRIS - Clinician List

Notes:			Stored in SCM2CM-CPM1.UHSM_Staging

Versions:		1.0.0.1 - 05/12/2016 - CM 
				Amended the identification of Radiology examinations to use the [$(EPR)].dbo.CV3OrganizationalUnit table	
				1.0.0.0 - 24/11/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [dbo].[uspDowntimeCRIS_ClinicianList]
As

Select	Distinct
		Clinician = us.DisplayName
		
From	[$(EPR)].dbo.CV3AllOrdersVw src With (NoLock)

		Inner Join [$(EPR)].dbo.CV3ClientVisit vis With (NoLock)
			On src.ClientVisitGUID = vis.GUID

		Inner Join [$(EPR)].dbo.CV3Client pat With (NoLock)
			On vis.ClientGUID = pat.GUID

		Left Join [$(EPR)].dbo.CV3OrderStatus stat With (NoLock)
			On src.OrderStatusCode = stat.Code

		Left Join [$(EPR)].dbo.CV3OrderCatalogMasterItem cmi With (NoLock)
			On src.OrderCatalogMasterItemGUID = cmi.GUID

		Left Join [$(EPR)].dbo.CV3AncillaryName an With (NoLock)
			On cmi.GUID = an.MainCatItemGUID
			And an.CodingStd Like '%OUT'

		Left Join [$(EPR)].dbo.CV3OrderEntryForm frm With (NoLock)
			On src.OrderEntryFormGUID = frm.GUID

		Left Join [$(EPR)].dbo.CV3User us With (NoLock)
			On src.UserGUID = us.GUID

		--Name field is where the code is found
		Left outer join [$(EPR)].dbo.CV3OrganizationalUnit cou
			On cmi.OrgUnitGUID = cou.GUID--to get the unit which 'owns' the test - e.g. Microbiology, Radiology, Cellular Pathology, etc.



Where	--frm.[Name] Like 'RAD%'
		(cou.Name = 'Diagnostic Imaging'
		or src.Name in 
					('AUTOFLUORESCENCE BRONCHOSCOPY'
					,'BRONCH RESEARCH'
					,'BRONCHIAL THERMOPLASTY'
					,'BRONCHOSCOPIC LVRT'
					,'BRONCHOSCOPIC STENT'
					,'BRONCHOSCOPY '
					,'CHARTIS ASSESSMENT'
					,'DIAGNOSTIC PLEURAL ASPIRATION'
					,'DRAINAGE OF INDWELLING PLEURAL CATHETER'
					,'EBUS-TBNA'
					,'EBUS-TBLB (1.7mm probe)'
					,'EBUS-TBNA (1.7mm Probe)' --GR 14/12/16 Added as this name is used on EPR but not CRIS
					,'EBUS-TBNA (GA)' 
					,'Vocal Cord Inspection' --GR 15/12/16 Added as reqested by Dawn Hand
					,'ENDOSCOPY OF NASAL CAVITY/UPPER AIRWAY'
					,'INTRALUMINAL BRACHYTHERAPY'
					,'IPC INS FOR IPC-PLUS TRIAL'
					,'LGE BORE BLUNT DISSECTION CHES DRAIN INS'
					,'LOCAL ANAESTHETIC THORACOSCOPY'
					,'INSERTION OF INDWELLING PLEURAL CATHETER'
					,'LOCAL ANAESTHETIC THORACOSCOPY'
					,'MEDICAL THORACOSCOPY WITH BIOPSY'
					,'PHOTODYNAMIC THERAPY'
					,'RADIAL EBUS (2.6mm probe)'
					,'REMOVAL OF INDWELLING PLEURAL CATHETER'
					,'REVIEW FOR ENDOBRONCHIAL THERAPY'
					,'ROUTINE BRONCHOSCOPY WITH BLIND TBNA'
					,'ROUTINE BRONCHOSCOPY WITH  TBLB'
					,'ROUTINE BRONCHOSCOPY WITH  BAL'
					,'ROUTINE BRONCHOSCOPY WITH TBLB'
					,'ROUTINE BRONCHOSCOPY WITH BAL'
					,'SELDINGER CHEST DRAIN INSERTION'
					,'SUSPECTED FOREIGN BODY REMOVAL'
					,'TALC SLURRY'
					,'TEMPORARY PLEURAL DRAIN'
					,'THERAPEUTIC IRRIGATION & BRONC TOILETING'
					,'THERAPEUTIC PLEURAL ASPIRATION'
					,'THORACOSCOPIC TALC POUDRAGE'
					,'ULTRASOUND GUIDED ASPIRATION OF THORAX'
					,'ULTRASOUND GUIDED DRAINAGE THORAX '
					,'US GUIDED PERCUTANEOUS PLEURAL BIOPSY'
					) )
		And (src.OrderStatusCode In ('PEND','SCHD')
		Or src.OrderStatusCode Is Null)

Union All

Select	'<All>' As Clinician

Order By Clinician