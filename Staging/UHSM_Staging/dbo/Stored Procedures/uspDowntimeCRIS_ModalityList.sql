﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Downtime - CRIS - Modality List

Notes:			Stored in SCM2CM-CPM1.UHSM_Staging

Versions:		
				1.0.0.0 - 24/11/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure dbo.uspDowntimeCRIS_ModalityList
As

Select	Distinct Modality
		
From	CRISExam src With (NoLock)

Union All

Select 'Bronchoscopy' as Modality

Union All

Select	'Other' As Modality

Union All

Select	'<All>' As Modality

Order By Modality