﻿/****** Object:  StoredProcedure [dbo].[uspEndoscopyOrders]    Script Date: 13/12/2016 10:09:49 ******/

CREATE PROCEDURE [dbo].[uspEndoscopyOrders]
	@LocationTypeCode varchar(100)
	,@LocationGUID float
AS

SELECT [Client].[GUID] AS 'ClientGUID'
	,[Client].[BirthDayNum]
	,[Client].[BirthMonthNum]
	,[Client].[BirthYearNum]
	,[dbo].[fn_SCM_DOB]([Client].[BirthYearNum],[Client].[BirthMonthNum],[Client].[BirthDayNum]) AS 'DOB'
	,[Client].[GenderCode]
	,[PTIDRM2].[ClientIDCode] AS 'RM2Number'
	,[PTIDNHS].[ClientIDCode] AS 'NHSNo'
	,[Visit].[GUID] AS 'OriginalVisitGUID'
	,[Visit].[AdmitDtm] AS 'OriginalVisitDtm'
	,[Visit].[ClientDisplayName]
	,[Visit].[ProviderDisplayName]
	,[Visit].[CurrentLocation] AS 'VisitLocation'
	,CASE 
		WHEN [Visit].[TemporaryLocation] IS NULL
			THEN [Visit].[CurrentLocation]
		ELSE [Visit].[TemporaryLocation]
		END AS 'Ward Patient Is On'
	,[Orders].[GUID] AS OrderGUID
	,[Orders].[IDCode] as OrderID
	,[Orders].[Name] AS OrderName
	,[Orders].[SignificantDtm]
	,[Orders].[SignificantTime]
	,[Orders].[IsConditional]
	,[Orders].[ConditionsText]
	,[Orders].[IsSuspended]
	,[Orders].[RepeatOrder]
	,[Orders].[ComplexOrderType]
	,[Item].[SpecimenType]
	,[Orders].[Status]
	,[Orders].[TypeCode]
	,[Orders].[OrderStatusCode]
	,[Orders].[RequestedDtm]
FROM [$(EPR)].[dbo].[CV3Order] AS [Orders]

INNER JOIN [$(EPR)].[dbo].[CV3ClientVisit] AS [Visit]
ON [Visit].[GUID] = [Orders].[ClientVisitGUID]

INNER JOIN [$(EPR)].[dbo].[CV3Client] AS [Client]
ON [Client].[GUID] = [Orders].[ClientGUID]

LEFT JOIN [$(EPR)].[dbo].[CV3ClientID] AS [PTIDRM2]
ON [PTIDRM2].ClientGUID = [Orders].[ClientGUID]
AND [PTIDRM2].TypeCode = 'Hospital Number'

LEFT JOIN [$(EPR)].[dbo].CV3ClientID AS [PTIDNHS] 
ON [PTIDNHS].[ClientGUID] = [Orders].[ClientGUID]
AND [PTIDNHS].[TypeCode] = 'NHS Number'
AND [PTIDNHS].[ClientIDCode] <> 'UNKNOWN'

LEFT JOIN [$(EPR)].[dbo].[CV3OrderCatalogMasterItem] AS [Item]
ON [Orders].[OrderCatalogMasterItemGUID] = [Item].[GUID]

INNER JOIN
(
SELECT DISTINCT 
[CMI].[Name] AS 'OrderItemName'
,[org].[Name] AS 'OrderGroup'

FROM [$(EPR)].[dbo].[CV3OrderCatalogMasterItem] AS [CMI]

LEFT JOIN [$(EPR)].[dbo].[CV3OrganizationalUnit] AS [ORG] With (NoLock)
On [CMI].[OrgUnitGUID] = [ORG].[GUID]

WHERE (
[ORG].[Name] = 'ENDOSCOPY'
OR
[CMI].[Name] in ('Video Capsule Endoscopy'
,'ERCP'
,'ERCP and sphincterotomy'
,'ERCP Biliary stent metal'
,'ERCP Biliary stent plastic'
,'ERCP Biliary stone removal'
,'ERCP Balln Dilat Sphinc Oddi Fluoro Guid'
,'ERCP Biliary dilatation'
,'Percutaneous Endoscopic Gastrostomy'
,'ERCP Plastic stent Panc duct Fluoro Guid'
,'ERCP Remov biliary calculus Fluoro Guid'
,'ERCP Removal biliary stent Fluoro guided'
,'ERCP Removal calculus Fluoro guided'
,'ERCP Repl biliary stent Fluoro guided'
,'ERCP Repl Stent Pancreas Fluoro Guided'
)
)
AND [CMI].[Name] NOT IN ('Gastrostomy Replacement')
)
AS [EndoProc]
ON [EndoProc].[OrderItemName] = [Orders].[Name]

WHERE
[Orders].[RequestedDtm] >= '06 DEC 2016 20:00'
AND	([Visit].TypeCode = @LocationTypeCode Or @LocationTypeCode = '<All>')
AND	([Visit].CurrentLocationGUID = @LocationGUID
	or	@LocationGUID = 0
	)
GO


