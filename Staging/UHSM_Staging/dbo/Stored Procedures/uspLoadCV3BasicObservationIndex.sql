﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Load CV3BasicObservationIndex

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:		
				1.0.0.0 - 09/12/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
Create Procedure dbo.uspLoadCV3BasicObservationIndex
As

;With LastGUID As (
	Select	Max([GUID]) As LastGUID
	From	CV3BasicObservationIndex src
	)
Insert Into CV3BasicObservationIndex(EnteredDate,EnteredDateTime,[GUID],OrderGUID)
Select	dbo.fn_GetJustDate(src.Entered),src.Entered,src.[GUID],src.OrderGUID
From	[$(EPR)].dbo.CV3BasicObservation src
		Cross Join LastGUID lg
Where	src.[GUID] > lg.LastGUID