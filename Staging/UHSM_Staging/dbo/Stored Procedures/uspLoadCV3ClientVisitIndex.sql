﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Load CV3ClientVisitIndex

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:		
				1.0.0.0 - 09/12/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
Create Procedure dbo.uspLoadCV3ClientVisitIndex
As

-- Update existing
Update	src
Set		AdmissionDate = dbo.fn_GetJustDate(vis.AdmitDtm)
		,AdmissionDateTime = vis.AdmitDtm
	,ConsultantName = vis.ProviderDisplayName
	,CurrentLocation = vis.CurrentLocation
	,DischargeDate = dbo.fn_GetJustDate(vis.DischargeDtm)
	,DischargeDateTime = vis.DischargeDtm
	,PatientName = vis.ClientDisplayName
	,RM2Number = vis.IDCode
	,SubSpecialtyCode = serv.GroupCode

From	CV3ClientVisitIndex src

		Inner Join [$(EPR)].dbo.CV3ClientVisit vis
			On src.[GUID] = vis.[GUID]

		Left Join [$(EPR)].dbo.CV3Service serv
			On vis.ServiceGUID = serv.[GUID]

Where	Coalesce(src.AdmissionDateTime,'1900-01-01') <> Coalesce(vis.AdmitDtm,'1900-01-01')
		Or Coalesce(src.ConsultantName,'') <> Coalesce(vis.ProviderDisplayName,'')
		Or Coalesce(src.CurrentLocation,'') <> Coalesce(vis.CurrentLocation,'')
		Or Coalesce(src.DischargeDateTime,'1900-01-01') <> Coalesce(vis.DischargeDtm,'1900-01-01')
		Or Coalesce(src.PatientName,'') <> Coalesce(vis.ClientDisplayName,'')
		Or Coalesce(src.RM2Number,'') <> Coalesce(vis.IDCode,'')
		Or Coalesce(src.SubSpecialtyCode,'') <> Coalesce(serv.GroupCode,'')

-- Add new
;With LastGUID As (
	Select	Max([GUID]) As LastGUID
	From	CV3ClientVisitIndex src
	)
Insert Into CV3ClientVisitIndex(
	[GUID]
	,AdmissionDate
	,AdmissionDateTime
	,ConsultantName
	,CurrentLocation
	,DischargeDate
	,DischargeDateTime
	,PatientName
	,RM2Number
	,SubSpecialtyCode
	)
Select
	src.[GUID]
	,dbo.fn_GetJustDate(src.AdmitDtm)
	,src.AdmitDtm
	,src.ProviderDisplayName
	,src.CurrentLocation
	,dbo.fn_GetJustDate(src.DischargeDtm)
	,src.DischargeDtm
	,src.ClientDisplayName
	,src.IDCode
	,serv.GroupCode
From	[$(EPR)].dbo.CV3ClientVisit src

		Left Join [$(EPR)].dbo.CV3Service serv
			On src.ServiceGUID = serv.[GUID]

		Cross Join LastGUID lg

Where	src.[GUID] > lg.LastGUID