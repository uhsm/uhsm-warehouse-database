﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Load CV3ClientVisitLocationIndex

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:		
				1.0.0.0 - 08/12/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
Create Procedure dbo.uspLoadCV3ClientVisitLocationIndex
As

-- Update existing
Update	src
Set		LocationGUID = loc.LocationGUID,
		TransferRequestDate = dbo.fn_GetJustDate(loc.TransferRequestDtm),
		TransferRequestDateTime = loc.TransferRequestDtm
From	CV3ClientVisitLocationIndex src

		Inner Join [$(EPR)].dbo.CV3ClientVisitLocation loc
			On src.[GUID] = loc.[GUID]

Where	src.LocationGUID <> loc.LocationGUID
		Or src.TransferRequestDateTime <> loc.TransferRequestDtm

-- Add new
;With LastGUID As (
	Select	Max([GUID]) As LastGUID
	From	CV3ClientVisitLocationIndex src
	)
Insert Into CV3ClientVisitLocationIndex([GUID],ClientVisitGUID,LocationGUID,TransferRequestDate,TransferRequestDateTime)
Select	[GUID],
		ClientVisitGUID,
		LocationGUID,
		dbo.fn_GetJustDate(src.TransferRequestDtm),
		src.TransferRequestDtm
From	[$(EPR)].dbo.CV3ClientVisitLocation src
		Cross Join LastGUID lg
Where	src.[GUID] > lg.LastGUID