﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Load CV3OrderIndex

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:		
				1.0.0.0 - 09/12/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
Create Procedure dbo.uspLoadCV3OrderIndex
As

;With LastGUID As (
	Select	Max([GUID]) As LastGUID
	From	CV3OrderIndex src
	)
Insert Into CV3OrderIndex(
	CareProviderGUID,
	ClientVisitGUID,
	GUID,
	OrderCatalogMasterItemGUID,
	OrderEntryFormGUID,
	RequestedDate,
	RequestedDateTime,
	UserGUID
	)
Select
	CareProviderGUID,
	ClientVisitGUID,
	GUID,
	OrderCatalogMasterItemGUID,
	OrderEntryFormGUID,
	dbo.fn_GetJustDate(RequestedDtm),
	RequestedDtm,
	UserGUID
From	[$(EPR)].dbo.CV3Order src
		Cross Join LastGUID lg
Where	src.[GUID] > lg.LastGUID