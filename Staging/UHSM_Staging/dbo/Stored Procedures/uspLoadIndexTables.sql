﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Load Index Tables

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:		
				1.0.0.1 - 09/12/2016 - MT
					Created individual sprocs for each table.
					This sproc calls those sprocs.

				1.0.0.0 - 08/12/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure dbo.uspLoadIndexTables
As

Exec dbo.uspLoadCV3BasicObservationIndex
Exec dbo.uspLoadCV3ClientVisitIndex
Exec dbo.uspLoadCV3ClientVisitLocationIndex
Exec dbo.uspLoadCV3OrderIndex