﻿
--/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
--Purpose:		Report Results Acknowledgement

--Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

--Versions:		--	1.0.0.0 - 24/01/2017 - CM
--					Created sproc.
--------------------------------------------------------------------------------------------------------------------------------------------------------
--*/
Create Procedure [dbo].[uspLoadReportResultsAcknowledgement]

AS 

--Load Index Tables 
Exec dbo.uspLoadIndexTables



--Prep for load of Final Table - some of this logic taken from the stored procedure uspReportsSummary
Declare @Division_Unknown_ID int
Declare @Directorate_Unknown_ID int

Set @Division_Unknown_ID = (Select DivisionID From Division Where Division = 'Unknown')
Set @Directorate_Unknown_ID = (Select DirectorateID From Directorate Where Directorate = 'Unknown')

Declare @StartDate date = '2016-11-01'--this is the earliest date which observations were entered
Declare @EndDate date = GetDate()
Declare @SSRSUserID varchar(50) = 'UHSM\Cman'--parameter which is currently used in the existing report - have kept this for simplicity so that we can continue to use the zt tables which failitate the build of the final dataset

;
-- Clear out the temp tables for this user
Delete ztReportsPTL_Obs Where SSRSUserID =  @SSRSUserID
Delete ztReportsPTL_Abnormal Where SSRSUserID = @SSRSUserID
Delete ztReportsPTL_FilingStatus Where SSRSUserID = @SSRSUserID
Delete ztReportsPTL_Locations1 Where SSRSUserID = @SSRSUserID
Delete ztReportsPTL_Locations2 Where SSRSUserID =  @SSRSUserID
Delete ztReportsPTL_Visit Where SSRSUserID =@SSRSUserID
Delete ztReportsPTL_ReqByStatDetail Where SSRSUserID =@SSRSUserID
Drop Table ztReportsPTL_ReqByStat --used drop here as to insert into this table was taking a long time
Truncate Table LoadReportResultsAcknowledgement --as the query takes a little while to run, load into a staging table  first and the import into the final ReportResultsAcknowldgement table from this one 






-- Load the temp tables
Insert Into ztReportsPTL_Obs(SSRSUserID,RowNo,OrderGUID,EnteredDate,EnteredDateTime,AbnormalityCode, ObsGUID)
	Select	 @SSRSUserID
			,RowNo = ROW_NUMBER() Over (Partition By src.OrderGUID Order By src.GUID Desc)
			,src.OrderGUID
			,src.EnteredDate
			,src.EnteredDateTime
			,obs.AbnormalityCode
			,obs.GUID
	From	CV3BasicObservationIndex src With (NoLock)
			Inner Join [$(EPR)].dbo.CV3BasicObservation obs With (NoLock)
				On src.[GUID] = obs.[GUID]
	Where	src.EnteredDate Between @StartDate And @EndDate
			And obs.IsHistory = 0
			And obs.ItemName Not Like '%Autofile%'

--Get Requested By GUID from Order Status History table - to be used for orders which are attached to a reference visit and have no requested by 
--Dropping and recreating as it is faster
--Insert into  ztReportsPTL_ReqByStat (OrderGUID, RequestedByGUID, RowNo)
	Select	OrderGUID,
			RequestedByGUID,
			RowNo = ROW_NUMBER() Over (Partition By stat.OrderGUID Order By stat.CreatedWhenDateTime Desc)
	into ztReportsPTL_ReqByStat
	From CV3OrderStatusHistoryIndex stat
	Where OrderStatusCode like  'RES%'
	AND RequestedByGUID Is NOT NULL 



--Created second zt report table which holds the Care Provider details of the last row of the order status history
Insert into ztReportsPTL_ReqByStatDetail(SSRSUserID, OrderGUID, RequestedByGUID, RequestedByProv) 
	Select	@SSRSUserID as SSRSUserID,
			 Obs.OrderGUID, 
			stat.RequestedByGUID,
			prov.DisplayName as RequestedByProv

	From ztReportsPTL_Obs Obs
	Left outer join ztReportsPTL_ReqByStat stat on 
		Obs.OrderGUID = stat.OrderGUID and stat.RowNo = 1
	Left outer join [$(EPR)].dbo.CV3CareProvider prov on 
		stat.RequestedByGUID = prov.GUID
	Where Obs.RowNo = 1
	And Obs.SSRSUserID = @SSRSUserID




--
Insert Into ztReportsPTL_Abnormal(SSRSUserID,RowNo,OrderGUID,AbnormalityDef,AbnormalityGroup, ObsGUID)
	Select	@SSRSUserID
			,RowNo = ROW_NUMBER() Over (Partition By obs.OrderGUID Order By obs.GUID)
			,obs.OrderGUID
			,AbnormalityDef = dbo.fn_SCM_Abnormal_Definition(obs.AbnormalityCode)
			,AbnormalityGroup = Case when dbo.fn_SCM_Abnormal_Definition(obs.AbnormalityCode) in ('High', 'Very high', 'Low', 'Very Low', 'Abnormal', 'Very abnormal') Then 'Abnormal'
									when  dbo.fn_SCM_Abnormal_Definition(obs.AbnormalityCode) in ('Normal', 'Unknown') Then 'Normal'
								Else 'Unknown'
								End
			,src.ObsGUID

	From	ztReportsPTL_Obs src With (NoLock)
			Inner Join [$(EPR)].dbo.CV3BasicObservation obs With (NoLock)
				On src.ObsGUID = obs.GUID
			Left Join [$(EPR)].dbo.SXAGNAbnormalCodeFn() ab
				On src.AbnormalityCode = ab.Code
	Where	src.SSRSUserID = @SSRSUserID
			--And src.RowNo = 1
			And obs.IsHistory = 0
			And obs.ItemName Not Like '%Autofile%'
			And obs.AbnormalityCode not in ('N','U')--these are deemed to be the normal codes
	

--
Insert Into ztReportsPTL_FilingStatus(SSRSUserID,RowNo,ObjectGUID,Reason)
	Select	@SSRSUserID
			,RowNo = ROW_NUMBER() Over (Partition By co.ObjectGUID Order By co.CompletionObjectID Desc)
			,co.ObjectGUID
			,co.Reason
	From	ztReportsPTL_Obs src With (NoLock)
			Inner Join [$(EPR)].dbo.SXACMPCompletionObject co With (NoLock)
				On src.OrderGUID = co.ObjectGUID
	Where	src.SSRSUserID = @SSRSUserID
			And src.RowNo = 1



--
Insert Into ztReportsPTL_Locations1(SSRSUserID,RowNo,ClientVisitGUID,LocationGUID,TransferRequestDtm)
	Select	@SSRSUserID
			,RowNo = ROW_NUMBER() Over (Partition By vl.ClientVisitGUID Order By vl.TransferRequestDtm)
			,vl.ClientVisitGUID
			,vl.LocationGUID
			,vl.TransferRequestDtm
	From	ztReportsPTL_Obs src With (NoLock)
			Inner Join CV3OrderIndex ord With (NoLock)
				On src.OrderGUID = ord.[GUID]
			Inner Join [$(EPR)].dbo.CV3ClientVisitLocation vl With (NoLock)
				On ord.ClientVisitGUID = vl.ClientVisitGUID
	Where	src.SSRSUserID = @SSRSUserID
			And src.RowNo = 1




--second CTE to get the end time of the patient visit on that location
Insert Into ztReportsPTL_Locations2(SSRSUserID,ClientVisitGUID,LocationGUID,StartDateTime,EndDateTime)
	Select	 @SSRSUserID
			,src.ClientVisitGUID
			,src.LocationGUID
			,StartDateTime = src.TransferRequestDtm
			,EndDateTime = Coalesce(loc.TransferRequestDtm,vis.DischargeDtm)
	From	ztReportsPTL_Locations1 src With (NoLock)

			Left Join ztReportsPTL_Locations1 loc With (NoLock)
				On loc.SSRSUserID = @SSRSUserID
				And src.ClientVisitGUID = loc.ClientVisitGUID
				And src.RowNo + 1 = loc.RowNo

			Left Join [$(EPR)].dbo.CV3ClientVisit vis With (NoLock)
				On src.ClientVisitGUID = vis.[GUID]

	Where	src.SSRSUserID = @SSRSUserID


--
Insert Into ztReportsPTL_Visit(ClientGUID,[GUID],[Location],LocationType,PatientName,RM2Number,SubSpecialtyCode,SSRSUserID, OrderGUID)
Select	Distinct 
		vis.ClientGUID
		,vis.[GUID]
		,loch.LocationLevel3
		,loch.LocationLevel2
		,vis.ClientDisplayName
		,vis.IDCode
		,Coalesce(serv.GroupCode,'ZZZ') -- ZZZ is a pseudo code for SubSpecialty = Unknown
		,@SSRSUserID-- @SSRSUserID
		,src.OrderGUID
From	ztReportsPTL_Obs src With (NoLock)

		Inner Join CV3OrderIndex ord With (NoLock)
			On src.OrderGUID = ord.[GUID]

		Inner Join [$(EPR)].dbo.CV3ClientVisit vis With (NoLock)
			On ord.ClientVisitGUID = vis.[GUID]
	
		Left Join [$(EPR)].dbo.CV3Service serv With (NoLock)
			On vis.ServiceGUID = serv.[GUID]

		Left Join ztReportsPTL_Locations2 loc With (NoLock)
			On loc.SSRSUserID = @SSRSUserID
			And vis.[GUID] = loc.ClientVisitGUID
			And ord.RequestedDateTime >= loc.StartDateTime
			And (ord.RequestedDateTime < loc.EndDateTime Or loc.EndDateTime Is Null)

		Left Join LocationHierarchy loch With (NoLock)
			On	Coalesce(loc.LocationGUID,vis.CurrentLocationGUID) = loch.LocationGUID

Where	src.SSRSUserID = @SSRSUserID
		And src.RowNo = 1


--Query to build final dataset
Insert into  LoadReportResultsAcknowledgement

Select	DivisionID = 	Coalesce(clsp.DivisionID,clsp2.DivisionID,@Division_Unknown_ID)
	,Division1 = Coalesce(clsp.Division,clsp2.Division,'Unknown')
	,DirectorateID1 = Coalesce(clsp.DirectorateID,clsp2.DirectorateID,@Directorate_Unknown_ID)
	,Directorate1 = Coalesce(clsp.Directorate,clsp2.Directorate,'Unknown')
	--,vis.SubSpecialtyCode
	,SpecialtyCode = Coalesce(clsp.SubSpecCode,clsp2.SubSpecCode,'ZZZ')
	,Specialty = Coalesce(clsp.Specialty,clsp2.Specialty,'Unknown')
	,ClinicianID = Case when cv3Order.CareProviderGUID = 0 Then stat.REquestedByGUID Else cv3Order.CareProviderGUID End --ord.UserGUID
	,Clinician = Coalesce(orderprov.DisplayName, stat.RequestedByProv)--us.DisplayName
	,[Location] = Coalesce(vis.[Location],'Unknown')
	,LocationType = Coalesce(vis.LocationType, 'Unknown')
	,Discipline = org.[Name]
	,ProviderType =  Case
				When frm.[Name] Like 'RAD%' Then 'Radiology'
				Else 'Pathology'
			End
	,vis.RM2Number
	,cv3Order.GUID as OrderGUID
	,cmi.Name as OrderItemName
	,an.Name as OrderItemCode
	,cv3order.RequestedDtm
	,cv3Order.SignificantDtm
	,vis.GUID as VisitGUID
	,ClientVis.VisitIDCode
	,ClientVis.TypeCode 
	,Clientvis.DischargeDtm
	,visitprov.DisplayName as VisitProvider
	,Case When ab.OrderGUID is not null Then 'Abnormal' Else 'Normal' End as Abnormal
	,Filingstatus = Case When fs.ObjectGUID Is Not Null Then 'Y' Else 'N' End
	,ChestXray = Case When an.[Name] Like 'XCHES%' Then 'Y' Else 'N' End 
	,vis.PatientName


From	ztReportsPTL_Obs src With (NoLock)

		Inner Join CV3OrderIndex ord With (NoLock)
			On src.OrderGUID = ord.[GUID]

		Left outer join [$(EPR)].dbo.CV3Order cv3Order
				On ord.GUID = cv3Order.GUID

		Left outer join [$(EPR)].dbo.CV3CareProvider orderprov--link here to get the Requested By Clinician on the order-this is where we should be getting the responsible clinician from
				On cv3Order.CareProviderGUID = orderprov.GUID

		Inner Join ztReportsPTL_Visit vis With (NoLock)
			On vis.SSRSUserID = @SSRSUserID
			And ord.ClientVisitGUID = vis.[GUID]
			And ord.GUID = vis.OrderGUID


		Inner Join [$(EPR)].dbo.CV3ClientVisit Clientvis 
			On vis.GUID = Clientvis.GUID


		Left Join [$(EPR)].dbo.CV3CareProviderVisitRole  provvis--link here to get lead care provider of the visit
				On vis.GUID = provvis.ClientVisitGUID And provvis.RoleCode = 'Lead Provider' And provvis.[Status] = 'Active' AND provvis.ToDtm  IS NULL  and provvis.ScopeLevel = 1

		Left Join [$(EPR)].dbo.CV3CareProvider visitprov 
				On provvis.ProviderGUID = visitprov.GUID 

		Left Join [$(EPR)].dbo.CV3OrderCatalogMasterItem cmi With (NoLock)
			On ord.OrderCatalogMasterItemGUID = cmi.[GUID]


		Left Join [$(EPR)].dbo.CV3AncillaryName an With (NoLock)
			On cmi.[GUID] = an.MainCatItemGUID
			And an.CodingStd Like '%OUT'

		Left Join [$(EPR)].dbo.CV3OrganizationalUnit org With (NoLock)
			On cmi.OrgUnitGUID = org.[GUID]

		Left Join [$(EPR)].dbo.CV3OrderEntryForm frm With (NoLock)
			On ord.OrderEntryFormGUID = frm.[GUID]

		Left Join [$(EPR)].dbo.CV3User us With (NoLock)
			On ord.UserGUID = us.[GUID]

		Left Join  ztReportsPTL_Abnormal ab With (NoLock)
			On ab.SSRSUserID = @SSRSUserID
			And ab.RowNo = 1
			And ord.[GUID] = ab.OrderGUID

		Left Join ztReportsPTL_FilingStatus fs With (NoLock)
			On fs.SSRSUserID =  @SSRSUserID
			And fs.RowNo = 1
			And ord.[GUID] = fs.ObjectGUID

		Left Join Specialty sp With (NoLock)
			On vis.SubSpecialtyCode = sp.SubSpecialtyCode

		Left Outer Join ztReportsPTL_ReqByStatDetail stat--in some cases - i.e. where the order is as a result of an unsolicited result, the requested by clinican field on the order will be blank. In these scenarios, the message which comes back from Telepath/CRIS will have the clinician on.  The preparateion for this zt table is where this information is found
			On ord.GUID = stat.OrderGUID

		Left Join (Select	prov.GUID,
					prov.DisplayName,
					serv.Description,
					serv.GroupCode  as SubSpecCode,
					serv.Description as Specialty,
					sp.DirectorateID,
					sp.Directorate,
					sp.Division,
					sp.DivisionID
					From [$(EPR)].dbo.CV3CareProvider prov
					Left outer join  [$(EPR)].dbo.CV3Service serv
					On prov.Discipline  = serv.Description
					Left outer join Specialty sp
					On serv.GroupCode = sp.SubSpecialtyCode
					where TypeCode = 'Consultant') as Clsp
					On Coalesce(orderprov.GUID, stat.RequestedByGUID)  = Clsp.GUID--sub query to identify the specialty of the responsible clinician (from requeested buy on the order or from the result message

			Left outer join 
						(Select CareProviderGUID,
								sp.DisciplineGUID,
								dis.Code,
								sv.GroupCode as SubSpecCode,
								sv.Description as Specialty,
								spec.DirectorateID,
								spec.Directorate,
								spec.Division,
								spec.DivisionID
							From (Select  CareProviderGUID, DisciplineGUID, RowNo = ROW_NUMBER() Over (Partition By CareProviderGUID Order By SequenceNumber)
									From [$(EPR)].dbo.SXAAMCareProviderSpecialtyXREF
								) sp --secondary table to get the care provider discipline
							Left outer join [$(EPR)].dbo.CV3Discipline dis 
							On sp.DisciplineGUID = dis.GUID
							Left outer join [$(EPR)].dbo.CV3Service sv 
							on 	dis.Code = sv.Description
							Left outer join Specialty spec
							On sv.GroupCode = spec.SubSpecialtyCode	
							where sp.RowNo =  1
							) clsp2 --second subquery to get the specialty of the care provider as the discipline in the care provider table is not always populated - unsure why?
				On  Coalesce(orderprov.GUID, stat.RequestedByGUID)  = Clsp2.CareProviderGUID
		


Where	src.SSRSUserID = @SSRSUserID
		And src.RowNo = 1



--Load into final table

Truncate Table ReportResultsAcknowledgement

Insert into ReportResultsAcknowledgement
Select *
From LoadReportResultsAcknowledgement
--Where RM2Number = 'RM24507792'