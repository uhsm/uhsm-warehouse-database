﻿/****** Object:  StoredProcedure [dbo].[uspEndoscopyOrders]    Script Date: 13/12/2016 10:09:49 ******/


/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Order List for various departments  - for SSRS report  Orders Requested on EPR

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging
				Requestes as per email from Jonathan Stone 06/01/2017
				Mapping of tests to department as per JStone email :
				--Cancer MDT
				 ('Referral MDT Lung Cancer',
				 'Referral MDT Mesothelioma')

				 --Cardiac MR
				 ('MRI Cardiac with Stress Perfusion',
				'MRA Aorta Thoracic',
				'MRI Cardiac Scan (without stress)',
				'MRI Cardiac Myocardial Viability')

				--Neurophys
				[org].[Name]  = 'NEUROPHYS'
				or 
				 ('Electro Myography',
				'Nerve Conduction Study and EMG',
				'Carpal Tunnel Syndrome',
				'Standard EEG',
				'Paediatric Sleep Sedation EEG',
				'Sleep Deprived EEG',
				'Ambulatory EEG'
				)

				--NeuroGastro
				[org].[Name]  = 'NEUROGASTRO'
				Or
				('Lactose Intolerance',
				'Small Bowel Bacterial Overgrowth',
				'Gut Transit Study',
				'Oesophageal Manometry',
				'Anorectal Manometry',
				'Endo Anal Ultrasound')--this one falls under diagnostic imaging


				--Audiology
				[org].[Name]  = 'AUDIOLOGY'
					

Versions:		
				1.0.0.0 - 09/01/2017 - CM
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[uspMultipleDeptOrders]
	@LocationTypeCode varchar(100)
	,@LocationGUID float
	,@Department varchar(150)
AS
;

With EndoProc 
AS

(SELECT DISTINCT 
[CMI].[Name] AS 'OrderItemName'
,[org].[Name] AS 'OrderGroup'
,CASE WHEN [org].[Name]  = 'AUDIOLOGY' THEN 'Audiology'
		  WHEN [org].[Name]  = 'NEUROPHYS' THEN 'Neurophysiology'
		  WHEN ([org].[Name]  = 'NEUROGASTRO' OR [CMI].[Name] in ('Endo Anal Ultrasound')) --this one falls under diagnostic imaging dept
											THEN 'Neurogastroenterology'
		  WHEN  [CMI].[Name] in  ('Referral MDT Lung Cancer',
									'Referral MDT Mesothelioma') THEN 'Lung MDT Coordinators'
		  WHEN   [CMI].[Name] in  ('MRI Cardiac with Stress Perfusion',
									'MRA Aorta Thoracic',
									'MRI Cardiac Scan (without stress)',
									'MRI Cardiac Myocardial Viability') THEN 'Cardiac MR'
		  WHEN (([ORG].[Name] = 'ENDOSCOPY' AND [CMI].[Name] NOT IN ('Gastrostomy Replacement'))--Endoscopy
				OR [CMI].[Name] in ('Video Capsule Endoscopy'
								,'ERCP'
								,'ERCP and sphincterotomy'
								,'ERCP Biliary stent metal'
								,'ERCP Biliary stent plastic'
								,'ERCP Biliary stone removal')--Endoscopy
								) THEN 'Endoscopy'

END AS Department


FROM [$(EPR)].[dbo].[CV3OrderCatalogMasterItem] AS [CMI]

LEFT JOIN [$(EPR)].[dbo].[CV3OrganizationalUnit] AS [ORG] With (NoLock)
On [CMI].[OrgUnitGUID] = [ORG].[GUID]

WHERE (
[org].[Name]  = 'AUDIOLOGY' --Audiology
OR [org].[Name]  = 'NEUROPHYS'--/neurophysiology
OR [org].[Name]  = 'NEUROGASTRO' --Neurogastroenterology
OR [CMI].[Name] in ('Endo Anal Ultrasound') --this one falls under diagnostic imaging dept
OR [CMI].[Name] in  ('Referral MDT Lung Cancer',
									'Referral MDT Mesothelioma') --Lung MDT
OR [CMI].[Name] in  ('MRI Cardiac with Stress Perfusion',
									'MRA Aorta Thoracic',
									'MRI Cardiac Scan (without stress)',
									'MRI Cardiac Myocardial Viability')--Cardiac MR
OR ([ORG].[Name] = 'ENDOSCOPY' AND [CMI].[Name] NOT IN ('Gastrostomy Replacement'))--Endoscopy
OR [CMI].[Name] in ('Video Capsule Endoscopy'
					,'ERCP'
					,'ERCP and sphincterotomy'
					,'ERCP Biliary stent metal'
					,'ERCP Biliary stent plastic'
					,'ERCP Biliary stone removal')--Endoscopy

)
)




SELECT [Client].[GUID] AS 'ClientGUID'
	,[Client].[BirthDayNum]
	,[Client].[BirthMonthNum]
	,[Client].[BirthYearNum]
	,[dbo].[fn_SCM_DOB]([Client].[BirthYearNum],[Client].[BirthMonthNum],[Client].[BirthDayNum]) AS 'DOB'
	,[Client].[GenderCode]
	,[PTIDRM2].[ClientIDCode] AS 'RM2Number'
	,[PTIDNHS].[ClientIDCode] AS 'NHSNo'
	,[Visit].[GUID] AS 'OriginalVisitGUID'
	,[Visit].[AdmitDtm] AS 'OriginalVisitDtm'
	,[Visit].[ClientDisplayName]
	,[Visit].[ProviderDisplayName]
	,[Visit].[CurrentLocation] AS 'VisitLocation'
	,CASE 
		WHEN [Visit].[TemporaryLocation] IS NULL
			THEN [Visit].[CurrentLocation]
		ELSE [Visit].[TemporaryLocation]
		END AS 'Ward Patient Is On'
	,[Orders].[GUID] AS OrderGUID
	,[Orders].[IDCode] as OrderID
	,[Orders].[Name] AS OrderName
	,[Orders].[SignificantDtm]
	,[Orders].[SignificantTime]
	,[Orders].[IsConditional]
	,[Orders].[ConditionsText]
	,[Orders].[IsSuspended]
	,[Orders].[RepeatOrder]
	,[Orders].[ComplexOrderType]
	,[Item].[SpecimenType]
	,[Orders].[Status]
	,[Orders].[TypeCode]
	,[Orders].[OrderStatusCode]
	,[Orders].[RequestedDtm]
	,EndoProc.Department

FROM [$(EPR)].[dbo].[CV3Order] AS [Orders]

INNER JOIN [$(EPR)].[dbo].[CV3ClientVisit] AS [Visit]
ON [Visit].[GUID] = [Orders].[ClientVisitGUID]

INNER JOIN [$(EPR)].[dbo].[CV3Client] AS [Client]
ON [Client].[GUID] = [Orders].[ClientGUID]

LEFT JOIN [$(EPR)].[dbo].[CV3ClientID] AS [PTIDRM2]
ON [PTIDRM2].ClientGUID = [Orders].[ClientGUID]
AND [PTIDRM2].TypeCode = 'Hospital Number'

LEFT JOIN [$(EPR)].[dbo].CV3ClientID AS [PTIDNHS] 
ON [PTIDNHS].[ClientGUID] = [Orders].[ClientGUID]
AND [PTIDNHS].[TypeCode] = 'NHS Number'
AND [PTIDNHS].[ClientIDCode] <> 'UNKNOWN'

LEFT JOIN [$(EPR)].[dbo].[CV3OrderCatalogMasterItem] AS [Item]
ON [Orders].[OrderCatalogMasterItemGUID] = [Item].[GUID]

INNER JOIN [EndoProc]
ON [EndoProc].[OrderItemName] = [Orders].[Name]

WHERE
[Orders].[RequestedDtm] >= '06 DEC 2016 20:00'
AND	([Visit].TypeCode = @LocationTypeCode Or @LocationTypeCode = '<All>')
AND	([Visit].CurrentLocationGUID = @LocationGUID or	@LocationGUID = 0)
AND (EndoProc.Department = @Department Or @Department = '<All>')
