﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Orders Requested On EPR - Department List to be used in SSRS report Orders requested in EPR - which lists orders placed for specific departments

Notes:			Stored in SCM2CM-CPM1.UHSM_Staging
				Linked to stored proc uspMultipleDeptOrders

Versions:			
				1.0.0.0 - 09/01/2017  - CM
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[uspMultipleDeptOrders_Dept]
AS


With EndoProc 
AS

(SELECT DISTINCT 
[CMI].[Name] AS 'OrderItemName'
,[org].[Name] AS 'OrderGroup'
,CASE WHEN [org].[Name]  = 'AUDIOLOGY' THEN 'Audiology'
		  WHEN [org].[Name]  = 'NEUROPHYS' THEN 'Neurophysiology'
		  WHEN ([org].[Name]  = 'NEUROGASTRO' OR [CMI].[Name] in ('Endo Anal Ultrasound')) --this one falls under diagnostic imaging dept
											THEN 'Neurogastroenterology'
		  WHEN  [CMI].[Name] in  ('Referral MDT Lung Cancer',
									'Referral MDT Mesothelioma') THEN 'Lung MDT Coordinators'
		  WHEN   [CMI].[Name] in  ('MRI Cardiac with Stress Perfusion',
									'MRA Aorta Thoracic',
									'MRI Cardiac Scan (without stress)') THEN 'Cardiac MR'
		  WHEN (([ORG].[Name] = 'ENDOSCOPY' AND [CMI].[Name] NOT IN ('Gastrostomy Replacement'))--Endoscopy
				OR [CMI].[Name] in ('Video Capsule Endoscopy'
								,'ERCP'
								,'ERCP and sphincterotomy'
								,'ERCP Biliary stent metal'
								,'ERCP Biliary stent plastic'
								,'ERCP Biliary stone removal')--Endoscopy
								) THEN 'Endoscopy'

END AS Department


FROM [$(EPR)].[dbo].[CV3OrderCatalogMasterItem] AS [CMI]

LEFT JOIN [$(EPR)].[dbo].[CV3OrganizationalUnit] AS [ORG] With (NoLock)
On [CMI].[OrgUnitGUID] = [ORG].[GUID]

WHERE (
[org].[Name]  = 'AUDIOLOGY' --Audiology
OR [org].[Name]  = 'NEUROPHYS'--/neurophysiology
OR [org].[Name]  = 'NEUROGASTRO' --Neurogastroenterology
OR [CMI].[Name] in ('Endo Anal Ultrasound') --this one falls under diagnostic imaging dept
OR [CMI].[Name] in  ('Referral MDT Lung Cancer',
									'Referral MDT Mesothelioma') --Lung MDT
OR [CMI].[Name] in  ('MRI Cardiac with Stress Perfusion',
									'MRA Aorta Thoracic',
									'MRI Cardiac Scan (without stress)')--Cardiac MR
OR ([ORG].[Name] = 'ENDOSCOPY' AND [CMI].[Name] NOT IN ('Gastrostomy Replacement'))--Endoscopy
OR [CMI].[Name] in ('Video Capsule Endoscopy'
					,'ERCP'
					,'ERCP and sphincterotomy'
					,'ERCP Biliary stent metal'
					,'ERCP Biliary stent plastic'
					,'ERCP Biliary stone removal')--Endoscopy

)
)


Select Distinct  Department
From EndoProc

Union All

Select	'<All>' As Department

Order By Department




