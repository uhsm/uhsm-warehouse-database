﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Orders And Specimens

Notes:			Stored in SC1SCM-CPM1.UHSM_Staging

Versions:
				1.0.0.0 - 16/12/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
Create Procedure dbo.uspOrdersAndSpecimens
	@FinancialYear varchar(9)
As

-- Load index tables
Exec dbo.uspLoadIndexTables

Select
	cal.FinancialMonthKey
	,cal.ShortMonth
	,cal.[1stOfMonth]
	,cal.EndOfMonth
	,VisitType = vis.TypeCode
	,NoOfOrders = Count(*)
	,NoOfSpecimens = Count(specimen.[GUID])

From	CV3OrderIndex src With (NoLock)

		Inner Join Calendar cal
			On src.RequestedDate = cal.TheDate
			And cal.FinancialYear = @FinancialYear
			And cal.TheDate <= GETDATE()

		Inner Join [$(EPR)].dbo.CV3ClientVisit vis With (NoLock)
			On src.ClientVisitGUID = vis.[GUID]

		Left Join [$(EPR)].dbo.CV3DiagnosticExtension diag
			On src.[GUID] = diag.[GUID]

		Left Join [$(EPR)].dbo.CV3Specimen Specimen
			On diag.SpecimenGUID = Specimen.[GUID]

Group By
	cal.FinancialMonthKey
	,cal.ShortMonth
	,cal.[1stOfMonth]
	,cal.EndOfMonth
	,vis.TypeCode

Order By
	cal.FinancialMonthKey
	,vis.TypeCode