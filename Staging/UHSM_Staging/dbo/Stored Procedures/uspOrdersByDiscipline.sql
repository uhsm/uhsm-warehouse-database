﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Orders By Discipline

Notes:			Stored in SC1SCM-CPM1.UHSM_Staging

Versions:
				1.0.0.0 - 16/12/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure dbo.uspOrdersByDiscipline
	@FinancialYear varchar(9)
As

-- Load index tables
Exec dbo.uspLoadIndexTables

Select
	cal.FinancialMonthKey
	,cal.ShortMonth
	,cal.[1stOfMonth]
	,cal.EndOfMonth
	,VisitType = vis.TypeCode
	,Discipline = org.[Name]
	,NoOfOrders = Count(*)

From	CV3OrderIndex src With (NoLock)

		Inner Join Calendar cal
			On src.RequestedDate = cal.TheDate
			And cal.FinancialYear = @FinancialYear
			And cal.TheDate <= GETDATE()

		Inner Join [$(EPR)].dbo.CV3ClientVisit vis With (NoLock)
			On src.ClientVisitGUID = vis.[GUID]

		Inner Join [$(EPR)].dbo.CV3OrderCatalogMasterItem cmi With (NoLock)
			On src.OrderCatalogMasterItemGUID = cmi.[GUID]

		Inner Join [$(EPR)].dbo.CV3OrganizationalUnit org With (NoLock)
			On cmi.OrgUnitGUID = org.[GUID]

Group By
	cal.FinancialMonthKey
	,cal.ShortMonth
	,cal.[1stOfMonth]
	,cal.EndOfMonth
	,vis.TypeCode
	,org.[Name]

Order By
	cal.FinancialMonthKey
	,vis.TypeCode
	,org.[Name]