﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Comparison between ICE and EPR - Orders Daily By Specialty

Notes:			Stored in SC1SCM-CPM1.UHSM_Staging

				ICE tests are equivalent to EPR orders.

Versions:
				1.0.0.0 - 15/12/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
Create Procedure dbo.uspOrdersDailyBySpecialty
As

-- Load index tables
Exec dbo.uspLoadIndexTables

Create Table #Output(
	RequestDate date
	,SpecialtyCode varchar(20)
	,SpecialtyName varchar(80)
	,Orders int
	)

-- Load ICE data
Insert Into #Output(RequestDate,SpecialtyCode,SpecialtyName,Orders)
Select	src.RequestDate
		,src.SpecialtyCode
		,sp.SubSpecialty
		,src.Tests
From	ICETestsDailyBySpecialty src

		Left Join Specialty sp
			On src.SpecialtyCode = sp.SubSpecialtyCode

Where	src.RequestDate >= '2016-11-28'

-- Load EPR data
Insert Into #Output(RequestDate,SpecialtyCode,SpecialtyName,Orders)
Select	src.RequestedDate
		,vis.SubSpecialtyCode
		,sp.SubSpecialty
		,Count(*) As Orders
From	CV3OrderIndex src

		Inner Join CV3ClientVisitIndex vis
			On src.ClientVisitGUID = vis.[GUID]

		Inner Join Specialty sp
			On vis.SubSpecialtyCode = sp.SubSpecialtyCode

Where	src.RequestedDate Between '2016-12-07' And dbo.fn_GetJustDate(DateAdd(d,-1,GETDATE()))

Group By src.RequestedDate
		,vis.SubSpecialtyCode
		,sp.SubSpecialty

-- Output
Select	*
From	#Output src
Order By src.RequestDate,src.SpecialtyName