﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Order Vs Reports PTL

Notes:			Stored in SC1SCM-CPM1.UHSM_Staging

Versions:
				1.0.0.2 - 09/12/2016 - MT
					Previously UHSM_OrdersReports_PatientLevel_UB.
					Revised to use index tables and zt tables to speed up execution.

				1.0.0.1 - 17/12/2016 - GR
					Adapted to use as drill-through from UHSM_OrdersReports_UB (this proc hadn't actually been created yet).

				1.0.0.0 - 24/11/2016 - DBH
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure dbo.uspOrdersVsReportsPTL
	@SSRSUserID varchar(50)
	,@StartDate date
	,@EndDate date
	,@VisitType varchar(Max)
	,@Reported varchar(30)
As

-- @EndDate cannot be in the future
If @EndDate > dbo.fn_GetJustDate(GETDATE())
Begin
	Set @EndDate = dbo.fn_GetJustDate(GETDATE())
End

-- Clear temp tables
Delete ztOrdersVsReportsPTL_Order Where SSRSUserID = @SSRSUserID
Delete ztOrdersVsReportsPTL_Visit Where SSRSUserID = @SSRSUserID
Delete ztOrdersVsReportsPTL_Locations1 Where SSRSUserID = @SSRSUserID
Delete ztOrdersVsReportsPTL_Locations2 Where SSRSUserID = @SSRSUserID

-- Load temp tables
Insert Into ztOrdersVsReportsPTL_Order(
	SSRSUserID
	,GUID
	,CareProviderGUID
	,ClientVisitGUID
	,[Name]
	,OrderCatalogMasterItemGUID
	,RequestedDate
	,RequestedDateTime
	,ObsCount
	)
Select
	@SSRSUserID
	,src.[GUID]
	,src.CareProviderGUID
	,src.ClientVisitGUID
	,ord.[Name]
	,src.OrderCatalogMasterItemGUID
	,src.RequestedDate
	,src.RequestedDateTime
	,Count(obs.[GUID])
From	CV3OrderIndex src

		Inner Join [$(EPR)].dbo.CV3Order ord
			On src.[GUID] = ord.[GUID]
			And ord.Active = 1

		Left Join CV3BasicObservationIndex obs
			On src.[GUID] = obs.OrderGUID

Where	src.RequestedDate Between @StartDate And @EndDate

Group By
	src.[GUID]
	,src.CareProviderGUID
	,src.ClientVisitGUID
	,ord.[Name]
	,src.OrderCatalogMasterItemGUID
	,src.RequestedDate
	,src.RequestedDateTime


Insert Into ztOrdersVsReportsPTL_Visit(
	SSRSUserID
	,[GUID]
	,CurrentLocation
	,CurrentLocationGUID
	,DischargeDate
	,DischargeDateTime
	,PatientName
	,RM2Number
	,TemporaryLocation
	,TypeCode
	)
Select	Distinct
	@SSRSUserID
	,vis.[GUID]
	,vis.CurrentLocation
	,vis.CurrentLocationGUID
	,dbo.fn_GetJustDate(vis.DischargeDtm)
	,vis.DischargeDtm
	,vis.ClientDisplayName
	,vis.IDCode
	,vis.TemporaryLocation
	,vis.TypeCode
From	ztOrdersVsReportsPTL_Order src

		Inner Join [$(EPR)].dbo.CV3ClientVisit vis
			On src.ClientVisitGUID = vis.[GUID]

Where	src.SSRSUserID = @SSRSUserID

Insert Into ztOrdersVsReportsPTL_Locations1(SSRSUserID,RowNo,ClientVisitGUID,LocationGUID,TransferRequestDtm)
	Select	@SSRSUserID
			,RowNo = ROW_NUMBER() Over (Partition By vl.ClientVisitGUID Order By vl.TransferRequestDateTime)
			,vl.ClientVisitGUID
			,vl.LocationGUID
			,vl.TransferRequestDateTime
	From	ztOrdersVsReportsPTL_Visit src With (NoLock)
			Inner Join CV3ClientVisitLocationIndex vl With (NoLock)
				On src.[GUID] = vl.ClientVisitGUID

	Where	src.SSRSUserID = @SSRSUserID

Insert Into ztOrdersVsReportsPTL_Locations2(SSRSUserID,ClientVisitGUID,LocationGUID,StartDateTime,EndDateTime)
	Select	@SSRSUserID
			,src.ClientVisitGUID
			,src.LocationGUID
			,StartDateTime = src.TransferRequestDtm
			,EndDateTime = Coalesce(loc.TransferRequestDtm,vis.DischargeDateTime)
	From	ztOrdersVsReportsPTL_Locations1 src With (NoLock)

			Left Join ztOrdersVsReportsPTL_Locations1 loc With (NoLock)
				On loc.SSRSUserID = @SSRSUserID
				And src.ClientVisitGUID = loc.ClientVisitGUID
				And src.RowNo + 1 = loc.RowNo

			Left Join CV3ClientVisitIndex vis With (NoLock)
				On src.ClientVisitGUID = vis.[GUID]

	Where	src.SSRSUserID = @SSRSUserID

Select
	vis.RM2Number
	,vis.PatientName
	,OrderName = src.[Name]
	,Discipline = org.[Name]
	,src.RequestedDateTime
	,Consultant = con.DisplayName
	,VisitType = vis.TypeCode
	,vis.CurrentLocation
	,vis.TemporaryLocation
	,RequestingLocationType = loch.LocationLevel2
	,RequestingLocation = Coalesce(loch.LocationLevel3,'Unknown')
	,RequestingClinician = Case When us.DisplayName Is Null Then 'Unknown' Else us.DisplayName End
	,Directorate = sp.Directorate
	,Specialty = serv.[Description]
	,Investigation = Case When an.[Name] Is Null Then '' Else an.[Name] + ' ' End + Coalesce(cmi.[Name],'')
	,ReportedItems = src.ObsCount

From	ztOrdersVsReportsPTL_Order src With (NoLock)

		Left Join ztOrdersVsReportsPTL_Visit vis With (NoLock)
			On vis.SSRSUserID = @SSRSUserID
			And src.ClientVisitGUID = vis.[GUID]

		Left Join ztOrdersVsReportsPTL_Locations2 loc With (NoLock)
			On loc.SSRSUserID = @SSRSUserID
			And vis.[GUID] = loc.ClientVisitGUID
			And src.RequestedDateTime >= loc.StartDateTime
			And (src.RequestedDateTime < loc.EndDateTime Or loc.EndDateTime Is Null)

		Left Join LocationHierarchy loch With (NoLock)
			On	Coalesce(loc.LocationGUID,vis.CurrentLocationGUID) = loch.LocationGUID

		Left Join [$(EPR)].dbo.CV3CareProvider con With (NoLock)
			On src.CareProviderGUID = con.[GUID]

		Left Join [$(EPR)].dbo.CV3User us With (NoLock)
			On src.UserGUID = us.[GUID]

		Left Join [$(EPR)].dbo.CV3Service serv With (NoLock)
			On vis.ServiceGUID = serv.[GUID]

		Left Join Specialty sp With (NoLock)
			On serv.GroupCode = sp.SubSpecialtyCode

		Left Join [$(EPR)].dbo.CV3OrderCatalogMasterItem cmi With (NoLock)
			On src.OrderCatalogMasterItemGUID = cmi.[GUID]

		Left Join [$(EPR)].dbo.CV3AncillaryName an With (NoLock)
			On cmi.GUID = an.MainCatItemGUID
			And an.CodingStd Like '%OUT'

		Left Join [$(EPR)].dbo.CV3OrganizationalUnit org With (NoLock)
			On cmi.OrgUnitGUID = org.[GUID]

		Inner Join dbo.Split(@VisitType,',') vt
			On vis.TypeCode = vt.Item

		Inner Join dbo.Split(@Reported,',') rp
			On Case When src.ObsCount = 0 Then 'No' Else 'Yes' End = rp.Item

Where	src.SSRSUserID = @SSRSUserID

Order By src.RequestedDateTime