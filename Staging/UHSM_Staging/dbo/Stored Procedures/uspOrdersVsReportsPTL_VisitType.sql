﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Order Vs Reports PTL - Visit Type List

Notes:			Stored in LocalAdmin.

Versions:
				1.0.0.0 - 13/12/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure dbo.uspOrdersVsReportsPTL_VisitType
As

-- Update index tables
Exec dbo.uspLoadIndexTables

Select	Distinct
		VisitType = vis.TypeCode

From	CV3OrderIndex src

		Inner Join [$(EPR)].dbo.CV3ClientVisit vis
			On src.ClientVisitGUID = vis.[GUID]

Where	src.RequestedDate >= DateAdd(yy,-1,GETDATE())

Order By vis.TypeCode