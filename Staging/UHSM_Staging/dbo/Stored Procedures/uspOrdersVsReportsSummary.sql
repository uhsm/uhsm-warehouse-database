﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Order Vs Reports Summary

Notes:			Stored in SC1SCM-CPM1.UHSM_Staging

Versions:
				1.0.0.0 - 14/12/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure dbo.uspOrdersVsReportsSummary
	@SSRSUserID varchar(50)
	,@FinancialYear varchar(9)
As

-- Clear temp tables
Delete ztOrdersVsReportsPTL_Order Where SSRSUserID = @SSRSUserID
Delete ztOrdersVsReportsPTL_Visit Where SSRSUserID = @SSRSUserID

-- Load temp tables
Insert Into ztOrdersVsReportsPTL_Order(
	SSRSUserID
	,GUID
	,CareProviderGUID
	,ClientVisitGUID
	,[Name]
	,OrderCatalogMasterItemGUID
	,RequestedDate
	,RequestedDateTime
	,ObsCount
	)
Select
	@SSRSUserID
	,src.[GUID]
	,src.CareProviderGUID
	,src.ClientVisitGUID
	,ord.[Name]
	,src.OrderCatalogMasterItemGUID
	,src.RequestedDate
	,src.RequestedDateTime
	,Count(obs.[GUID])
From	CV3OrderIndex src

		Inner Join Calendar cal
			On src.RequestedDate = cal.TheDate
			And cal.FinancialYear = @FinancialYear

		Inner Join [$(EPR)].dbo.CV3Order ord
			On src.[GUID] = ord.[GUID]
			And ord.Active = 1

		Left Join CV3BasicObservationIndex obs
			On src.[GUID] = obs.OrderGUID

Where	src.RequestedDateTime <= GETDATE() -- Exclude future orders

Group By
	src.[GUID]
	,src.CareProviderGUID
	,src.ClientVisitGUID
	,ord.[Name]
	,src.OrderCatalogMasterItemGUID
	,src.RequestedDate
	,src.RequestedDateTime


Insert Into ztOrdersVsReportsPTL_Visit(
	SSRSUserID
	,[GUID]
	,CurrentLocation
	,CurrentLocationGUID
	,DischargeDate
	,DischargeDateTime
	,PatientName
	,RM2Number
	,TemporaryLocation
	,TypeCode
	)
Select	Distinct
	@SSRSUserID
	,vis.[GUID]
	,vis.CurrentLocation
	,vis.CurrentLocationGUID
	,dbo.fn_GetJustDate(vis.DischargeDtm)
	,vis.DischargeDtm
	,vis.ClientDisplayName
	,vis.IDCode
	,vis.TemporaryLocation
	,vis.TypeCode
From	ztOrdersVsReportsPTL_Order src

		Inner Join [$(EPR)].dbo.CV3ClientVisit vis
			On src.ClientVisitGUID = vis.[GUID]

Where	src.SSRSUserID = @SSRSUserID

Select
	cal.FinancialMonthKey
	,cal.ShortMonth
	,cal.[1stOfMonth]
	,cal.EndOfMonth
	,VisitType = vis.TypeCode
	,NoOfOrders = Count(*)
	,ReportedOrders = Sum(Case When src.ObsCount > 0 Then 1 Else 0 End)
	,UnreportedOrders = Sum(Case When src.ObsCount = 0 Then 1 Else 0 End)

From	ztOrdersVsReportsPTL_Order src With (NoLock)

		Inner Join Calendar cal
			On src.RequestedDate = cal.TheDate

		Left Join ztOrdersVsReportsPTL_Visit vis With (NoLock)
			On vis.SSRSUserID = @SSRSUserID
			And src.ClientVisitGUID = vis.[GUID]

		Left Join [$(EPR)].dbo.CV3OrderCatalogMasterItem cmi With (NoLock)
			On src.OrderCatalogMasterItemGUID = cmi.[GUID]

Where	src.SSRSUserID = @SSRSUserID

Group By
	cal.FinancialMonthKey
	,cal.ShortMonth
	,cal.[1stOfMonth]
	,cal.EndOfMonth
	,vis.TypeCode

Order By
	cal.FinancialMonthKey
	,vis.TypeCode