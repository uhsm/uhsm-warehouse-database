﻿CREATE PROCEDURE [dbo].[uspOrdersWithOrderDateInFutureSpecialtyList]
AS

SELECT distinct
	cou.Name AS 'Specialty' -- GR 19/12/16 Added as all Specialties to be included in report
	--,[Client].[GUID] AS 'ClientGUID'
	--,[Client].[BirthDayNum]
	--,[Client].[BirthMonthNum]
	--,[Client].[BirthYearNum]
	--,[dbo].[fn_SCM_DOB]([Client].[BirthYearNum],[Client].[BirthMonthNum],[Client].[BirthDayNum]) AS 'DOB'
	--,[Client].[GenderCode]
	--,[PTIDRM2].[ClientIDCode] AS 'RM2Number'
	--,[PTIDNHS].[ClientIDCode] AS 'NHSNo'
	--,[Visit].[GUID] AS 'OriginalVisitGUID'
	--,[Visit].[AdmitDtm] AS 'OriginalVisitDtm'
	--,[Visit].[ClientDisplayName]
	--,[Visit].[ProviderDisplayName]
	--,[Visit].[CurrentLocation] AS 'VisitLocation'
	--,CASE 
	--	WHEN [Visit].[TemporaryLocation] IS NULL
	--		THEN [Visit].[CurrentLocation]
	--	ELSE [Visit].[TemporaryLocation]
	--	END AS 'Ward Patient Is On'
	--,[Orders].[GUID] AS OrderGUID
	--,[Orders].[IDCode] as OrderID
	--,[Orders].[Name] AS OrderName
	--,[Orders].[SignificantDtm]
	--,[Orders].[SignificantTime]
	--,[Orders].[IsConditional]
	--,[Orders].[ConditionsText]
	--,[Orders].[IsSuspended]
	--,[Orders].[RepeatOrder]
	--,[Orders].[ComplexOrderType]
	--,[Item].[SpecimenType]
	--,[Orders].[Status]
	--,[Orders].[TypeCode]
	--,[Orders].[OrderStatusCode]
	--,[Orders].[RequestedDtm]
FROM [$(EPR)].[dbo].[CV3Order] AS [Orders]

INNER JOIN [$(EPR)].[dbo].[CV3ClientVisit] AS [Visit]
ON [Visit].[GUID] = [Orders].[ClientVisitGUID]

INNER JOIN [$(EPR)].[dbo].[CV3Client] AS [Client]
ON [Client].[GUID] = [Orders].[ClientGUID]

--LEFT JOIN [$(EPR)].[dbo].[CV3ClientID] AS [PTIDRM2]
--ON [PTIDRM2].ClientGUID = [Orders].[ClientGUID]
--AND [PTIDRM2].TypeCode = 'Hospital Number'

--LEFT JOIN [$(EPR)].[dbo].CV3ClientID AS [PTIDNHS] 
--ON [PTIDNHS].[ClientGUID] = [Orders].[ClientGUID]
--AND [PTIDNHS].[TypeCode] = 'NHS Number'
--AND [PTIDNHS].[ClientIDCode] <> 'UNKNOWN'

LEFT JOIN [$(EPR)].[dbo].[CV3OrderCatalogMasterItem] AS [Item]
ON [Orders].[OrderCatalogMasterItemGUID] = [Item].[GUID]

		Left outer join [$(EPR)].dbo.CV3OrganizationalUnit cou
			On [Item].OrgUnitGUID = cou.GUID--to get the unit which 'owns' the test - e.g. Microbiology, Radiology, Cellular Pathology, etc.

LEFT JOIN [$(EPR)].[dbo].[CV3SendOrderQueue] AS [Queue]
on [Queue].IDCode=[Orders].IDCode

WHERE-- PTIDRM2.ClientIDCode = @DistrictNo )
	-- AND 
([Orders].[RequestedDtm] >= dateadd(day,1,cast(getdate() as date))

OR (((cou.Name = 'Diagnostic Imaging'
		or Orders.Name in 
					('AUTOFLUORESCENCE BRONCHOSCOPY'
					,'BRONCH RESEARCH'
					,'BRONCHIAL THERMOPLASTY'
					,'BRONCHOSCOPIC LVRT'
					,'BRONCHOSCOPIC STENT'
					,'BRONCHOSCOPY '
					,'CHARTIS ASSESSMENT'
					,'DIAGNOSTIC PLEURAL ASPIRATION'
					,'DRAINAGE OF INDWELLING PLEURAL CATHETER'
					,'EBUS-TBNA'
					,'EBUS-TBLB (1.7mm probe)'
					,'EBUS-TBNA (1.7mm Probe)' --GR 14/12/16 Added as this name is used on EPR but not CRIS
					,'EBUS-TBNA (GA)' 
					,'Vocal Cord Inspection' --GR 15/12/16 Added as reqested by Dawn Hand
					,'ENDOSCOPY OF NASAL CAVITY/UPPER AIRWAY'
					,'INTRALUMINAL BRACHYTHERAPY'
					,'IPC INS FOR IPC-PLUS TRIAL'
					,'LGE BORE BLUNT DISSECTION CHES DRAIN INS'
					,'LOCAL ANAESTHETIC THORACOSCOPY'
					,'INSERTION OF INDWELLING PLEURAL CATHETER'
					,'LOCAL ANAESTHETIC THORACOSCOPY'
					,'MEDICAL THORACOSCOPY WITH BIOPSY'
					,'PHOTODYNAMIC THERAPY'
					,'RADIAL EBUS (2.6mm probe)'
					,'REMOVAL OF INDWELLING PLEURAL CATHETER'
					,'REVIEW FOR ENDOBRONCHIAL THERAPY'
					,'ROUTINE BRONCHOSCOPY WITH BLIND TBNA'
					,'ROUTINE BRONCHOSCOPY WITH  TBLB'
					,'ROUTINE BRONCHOSCOPY WITH  BAL'
					,'ROUTINE BRONCHOSCOPY WITH TBLB'
					,'ROUTINE BRONCHOSCOPY WITH BAL'
					,'SELDINGER CHEST DRAIN INSERTION'
					,'SUSPECTED FOREIGN BODY REMOVAL'
					,'TALC SLURRY'
					,'TEMPORARY PLEURAL DRAIN'
					,'THERAPEUTIC IRRIGATION & BRONC TOILETING'
					,'THERAPEUTIC PLEURAL ASPIRATION'
					,'THORACOSCOPIC TALC POUDRAGE'
					,'ULTRASOUND GUIDED ASPIRATION OF THORAX'
					,'ULTRASOUND GUIDED DRAINAGE THORAX '
					,'US GUIDED PERCUTANEOUS PLEURAL BIOPSY'
					) ) and [Orders].[OrderStatusCode] = 'PCOL')
					)
					or ([Queue].SendAfter>= dateadd(day,1,cast(getdate() as date)) or [Queue].SendBefore>= dateadd(day,1,cast(getdate() as date))))

--and cou.Name = 'Diagnostic Imaging' -- GR 19/12/16 Removed as all Specialties to be included in report

AND [Orders].[OrderStatusCode] <> 'CANC'

UNION
Select 'Bronchoscopy' as 'Specialty'

UNION
Select 'UNKNOWN' as 'Specialty'
GO