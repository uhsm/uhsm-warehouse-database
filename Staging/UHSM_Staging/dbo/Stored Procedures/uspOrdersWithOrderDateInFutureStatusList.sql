﻿CREATE PROCEDURE [dbo].[uspOrdersWithOrderDateInFutureStatusList]

AS

SELECT distinct
	case when ([Orders].[RequestedDtm] < dateadd(day,1,cast(getdate() as date))

AND (((cou.Name = 'Diagnostic Imaging'
		or Orders.Name in 
					('AUTOFLUORESCENCE BRONCHOSCOPY'
					,'BRONCH RESEARCH'
					,'BRONCHIAL THERMOPLASTY'
					,'BRONCHOSCOPIC LVRT'
					,'BRONCHOSCOPIC STENT'
					,'BRONCHOSCOPY '
					,'CHARTIS ASSESSMENT'
					,'DIAGNOSTIC PLEURAL ASPIRATION'
					,'DRAINAGE OF INDWELLING PLEURAL CATHETER'
					,'EBUS-TBNA'
					,'EBUS-TBLB (1.7mm probe)'
					,'EBUS-TBNA (1.7mm Probe)' --GR 14/12/16 Added as this name is used on EPR but not CRIS
					,'EBUS-TBNA (GA)' 
					,'Vocal Cord Inspection' --GR 15/12/16 Added as reqested by Dawn Hand
					,'ENDOSCOPY OF NASAL CAVITY/UPPER AIRWAY'
					,'INTRALUMINAL BRACHYTHERAPY'
					,'IPC INS FOR IPC-PLUS TRIAL'
					,'LGE BORE BLUNT DISSECTION CHES DRAIN INS'
					,'LOCAL ANAESTHETIC THORACOSCOPY'
					,'INSERTION OF INDWELLING PLEURAL CATHETER'
					,'LOCAL ANAESTHETIC THORACOSCOPY'
					,'MEDICAL THORACOSCOPY WITH BIOPSY'
					,'PHOTODYNAMIC THERAPY'
					,'RADIAL EBUS (2.6mm probe)'
					,'REMOVAL OF INDWELLING PLEURAL CATHETER'
					,'REVIEW FOR ENDOBRONCHIAL THERAPY'
					,'ROUTINE BRONCHOSCOPY WITH BLIND TBNA'
					,'ROUTINE BRONCHOSCOPY WITH  TBLB'
					,'ROUTINE BRONCHOSCOPY WITH  BAL'
					,'ROUTINE BRONCHOSCOPY WITH TBLB'
					,'ROUTINE BRONCHOSCOPY WITH BAL'
					,'SELDINGER CHEST DRAIN INSERTION'
					,'SUSPECTED FOREIGN BODY REMOVAL'
					,'TALC SLURRY'
					,'TEMPORARY PLEURAL DRAIN'
					,'THERAPEUTIC IRRIGATION & BRONC TOILETING'
					,'THERAPEUTIC PLEURAL ASPIRATION'
					,'THORACOSCOPIC TALC POUDRAGE'
					,'ULTRASOUND GUIDED ASPIRATION OF THORAX'
					,'ULTRASOUND GUIDED DRAINAGE THORAX '
					,'US GUIDED PERCUTANEOUS PLEURAL BIOPSY'
					) ) and [Orders].[OrderStatusCode] = 'PCOL')))
	then 'Pending Collection (Radiology with Req Date not in future)'
	when [Orders].[OrderStatusCode] = 'PCOL' then 'Pending Collection'
	when stat.[Description] is null then 'Unknown'
	else stat.[Description] end as 'Status'

FROM [$(EPR)].[dbo].[CV3Order] AS [Orders]

INNER JOIN [$(EPR)].[dbo].[CV3ClientVisit] AS [Visit]
ON [Visit].[GUID] = [Orders].[ClientVisitGUID]

INNER JOIN [$(EPR)].[dbo].[CV3Client] AS [Client]
ON [Client].[GUID] = [Orders].[ClientGUID]

LEFT JOIN [$(EPR)].[dbo].[CV3ClientID] AS [PTIDRM2]
ON [PTIDRM2].ClientGUID = [Orders].[ClientGUID]
AND [PTIDRM2].TypeCode = 'Hospital Number'

LEFT JOIN [$(EPR)].[dbo].CV3ClientID AS [PTIDNHS] 
ON [PTIDNHS].[ClientGUID] = [Orders].[ClientGUID]
AND [PTIDNHS].[TypeCode] = 'NHS Number'
AND [PTIDNHS].[ClientIDCode] <> 'UNKNOWN'

LEFT JOIN [$(EPR)].[dbo].[CV3OrderCatalogMasterItem] AS [Item]
ON [Orders].[OrderCatalogMasterItemGUID] = [Item].[GUID]

		Left outer join [$(EPR)].dbo.CV3OrganizationalUnit cou
			On [Item].OrgUnitGUID = cou.GUID--to get the unit which 'owns' the test - e.g. Microbiology, Radiology, Cellular Pathology, etc.
LEFT JOIN [$(EPR)].[dbo].[CV3SendOrderQueue] AS [Queue]
on [Queue].IDCode = [Orders].IDCode

		Left Join [$(EPR)].dbo.CV3OrderStatus stat With (NoLock)
			On [Orders].OrderStatusCode = stat.Code

WHERE-- PTIDRM2.ClientIDCode = @DistrictNo )
	-- AND 
([Orders].[RequestedDtm] >= dateadd(day,1,cast(getdate() as date))

OR (((cou.Name = 'Diagnostic Imaging'
		or Orders.Name in 
					('AUTOFLUORESCENCE BRONCHOSCOPY'
					,'BRONCH RESEARCH'
					,'BRONCHIAL THERMOPLASTY'
					,'BRONCHOSCOPIC LVRT'
					,'BRONCHOSCOPIC STENT'
					,'BRONCHOSCOPY '
					,'CHARTIS ASSESSMENT'
					,'DIAGNOSTIC PLEURAL ASPIRATION'
					,'DRAINAGE OF INDWELLING PLEURAL CATHETER'
					,'EBUS-TBNA'
					,'EBUS-TBLB (1.7mm probe)'
					,'EBUS-TBNA (1.7mm Probe)' --GR 14/12/16 Added as this name is used on EPR but not CRIS
					,'EBUS-TBNA (GA)' 
					,'Vocal Cord Inspection' --GR 15/12/16 Added as reqested by Dawn Hand
					,'ENDOSCOPY OF NASAL CAVITY/UPPER AIRWAY'
					,'INTRALUMINAL BRACHYTHERAPY'
					,'IPC INS FOR IPC-PLUS TRIAL'
					,'LGE BORE BLUNT DISSECTION CHES DRAIN INS'
					,'LOCAL ANAESTHETIC THORACOSCOPY'
					,'INSERTION OF INDWELLING PLEURAL CATHETER'
					,'LOCAL ANAESTHETIC THORACOSCOPY'
					,'MEDICAL THORACOSCOPY WITH BIOPSY'
					,'PHOTODYNAMIC THERAPY'
					,'RADIAL EBUS (2.6mm probe)'
					,'REMOVAL OF INDWELLING PLEURAL CATHETER'
					,'REVIEW FOR ENDOBRONCHIAL THERAPY'
					,'ROUTINE BRONCHOSCOPY WITH BLIND TBNA'
					,'ROUTINE BRONCHOSCOPY WITH  TBLB'
					,'ROUTINE BRONCHOSCOPY WITH  BAL'
					,'ROUTINE BRONCHOSCOPY WITH TBLB'
					,'ROUTINE BRONCHOSCOPY WITH BAL'
					,'SELDINGER CHEST DRAIN INSERTION'
					,'SUSPECTED FOREIGN BODY REMOVAL'
					,'TALC SLURRY'
					,'TEMPORARY PLEURAL DRAIN'
					,'THERAPEUTIC IRRIGATION & BRONC TOILETING'
					,'THERAPEUTIC PLEURAL ASPIRATION'
					,'THORACOSCOPIC TALC POUDRAGE'
					,'ULTRASOUND GUIDED ASPIRATION OF THORAX'
					,'ULTRASOUND GUIDED DRAINAGE THORAX '
					,'US GUIDED PERCUTANEOUS PLEURAL BIOPSY'
					) ) and [Orders].[OrderStatusCode] = 'PCOL')
					)
					or ([Queue].SendAfter>= dateadd(day,1,cast(getdate() as date)) or [Queue].SendBefore>= dateadd(day,1,cast(getdate() as date))))

--and cou.Name = 'Diagnostic Imaging' -- GR 19/12/16 Removed as all Specialties to be included in report

AND [Orders].[OrderStatusCode] <> 'CANC'



--AND [Orders].[OrderStatusCode] IN (
-- 'PCOL' -- Pending Collection
--,'PDVR' -- Pending Verification
--,'PDVR1' -- Pending Verification Status 1
--,'PDVR2' -- Pending Verification Status 2
--,'PDVR3' -- Pending Verification Status 3
--,'PDVR4' -- Pending Verification Status 4
--,'PDVR5' -- Pending Verification Status 5
--,'PDVR6' -- Pending Verification Status 6
--,'PDVR7' -- Pending Verification Status 7
--,'PDVR8' -- Pending Verification Status 8
--,'PDVR9' -- Pending Verification Status 9
--,'PEND' -- Pending
--,'SCHD' -- Scheduled
--)

GO