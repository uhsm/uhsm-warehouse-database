﻿CREATE PROCEDURE [dbo].[PhlebotomyCancelledOrders]
AS

--CTE to get the latest Status History for an order
With OrdStat
AS
(
Select  OrderGUID,
		GUID as OrderStatusHistoryGuid,
		OrderStatusCode,
		ReasonText,
		substring(ReasonText,0,Charindex('-',ReasonText)-1) as ReasonTextShort,
		CreatedWhen,
		 ROW_NUMBER() Over (Partition By src.OrderGUID Order By src.GUID Desc) as SeqNo


FROM [$(EPR)].dbo.CV3OrderStatusHistory src
WHERE OrderStatusCode in ('CANC', 'DISC')
)


SELECT Client.GUID AS ClientGUID
	,Client.BirthDayNum
	,Client.BirthMonthNum
	,Client.BirthYearNum
	,DOB = dbo.fn_SCM_DOB(Client.BirthYearNum,Client.BirthMonthNum,Client.BirthDayNum)
	,Client.GenderCode
	,PTIDRM2.ClientIDCode AS RM2Number
	,PTIDNHS.ClientIDCode AS NHSNo
	,Visit.GUID AS OriginalVisitGUID
	,Visit.VisitIDCode
	,Visit.AdmitDtm AS OriginalVisitDtm
	,Visit.ClientDisplayName
	,Visit.ProviderDisplayName
	,Visit.CurrentLocation AS VisitLocation
	,CASE 
		WHEN Visit.TemporaryLocation IS NULL
			THEN Visit.CurrentLocation
		ELSE Visit.TemporaryLocation
		END AS 'Ward Patient Is On'
	,Orders.GUID AS OrderGUID
	,Orders.IDCode as OrderID
	,Orders.Name AS OrderName
	,Orders.SignificantDtm
	,Orders.SignificantTime
	,Orders.IsConditional
	,Orders.ConditionsText
	,Orders.IsSuspended
	,Orders.RepeatOrder
	,Orders.ComplexOrderType
	,Item.SpecimenType
	,Orders.Status
	,Orders.TypeCode
	,Orders.OrderStatusCode
	,Orders.RequestedDtm
	,Item.Name
	,Ordstat.ReasonText
	,OrdStat.ReasonTextShort

FROM [$(EPR)].dbo.CV3Order AS Orders
INNER JOIN [$(EPR)].dbo.CV3ClientVisit Visit ON Visit.GUID = Orders.ClientVisitGUID
INNER JOIN [$(EPR)].dbo.CV3Client Client ON Client.GUID = Orders.ClientGUID
LEFT JOIN [$(EPR)].dbo.CV3ClientID AS PTIDRM2 ON PTIDRM2.ClientGUID = Orders.ClientGUID
	AND PTIDRM2.TypeCode = 'Hospital Number'
LEFT JOIN [$(EPR)].dbo.CV3ClientID AS PTIDNHS ON PTIDNHS.ClientGUID = Orders.ClientGUID
	AND PTIDNHS.TypeCode = 'NHS Number'
LEFT JOIN [$(EPR)].dbo.CV3OrderCatalogMasterItem AS Item ON Orders.OrderCatalogMasterItemGUID = Item.GUID
LEFT JOIN OrdStat On Orders.GUID = OrdStat.OrderGUID 
			AND OrdStat.SeqNo = 1
WHERE 	Item.SpecimenType = 'Blood'
AND  Orders.OrderStatusCode in ('CANC', 'DISC')
ANd OrdStat.ReasonTextShort = 'Unable To Collect Sample'

