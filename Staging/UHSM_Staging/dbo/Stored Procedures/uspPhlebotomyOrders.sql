﻿Create PROCEDURE uspPhlebotomyOrders
	@LocationTypeCode varchar(100)
	,@LocationGUID float
AS

SELECT Client.GUID AS ClientGUID
	,Client.BirthDayNum
	,Client.BirthMonthNum
	,Client.BirthYearNum
	,DOB = dbo.fn_SCM_DOB(Client.BirthYearNum,Client.BirthMonthNum,Client.BirthDayNum)
	,Client.GenderCode
	,PTIDRM2.ClientIDCode AS RM2Number
	,PTIDNHS.ClientIDCode AS NHSNo
	,Visit.GUID AS OriginalVisitGUID
	,Visit.AdmitDtm AS OriginalVisitDtm
	,Visit.ClientDisplayName
	,Visit.ProviderDisplayName
	,Visit.CurrentLocation AS VisitLocation
	,CASE 
		WHEN Visit.TemporaryLocation IS NULL
			THEN Visit.CurrentLocation
		ELSE Visit.TemporaryLocation
		END AS 'Ward Patient Is On'
	,Orders.GUID AS OrderGUID
	,Orders.IDCode as OrderID
	,Orders.Name AS OrderName
	,Orders.SignificantDtm
	,Orders.SignificantTime
	,Orders.IsConditional
	,Orders.ConditionsText
	,Orders.IsSuspended
	,Orders.RepeatOrder
	,Orders.ComplexOrderType
	,Item.SpecimenType
	,Orders.Status
	,Orders.TypeCode
	,Orders.OrderStatusCode
	,Orders.RequestedDtm
FROM [$(EPR)].dbo.CV3Order AS Orders
INNER JOIN [$(EPR)].dbo.CV3ClientVisit Visit ON Visit.GUID = Orders.ClientVisitGUID
INNER JOIN [$(EPR)].dbo.CV3Client Client ON Client.GUID = Orders.ClientGUID
LEFT JOIN [$(EPR)].dbo.CV3ClientID AS PTIDRM2 ON PTIDRM2.ClientGUID = Orders.ClientGUID
	AND PTIDRM2.TypeCode = 'Hospital Number'
LEFT JOIN [$(EPR)].dbo.CV3ClientID AS PTIDNHS ON PTIDNHS.ClientGUID = Orders.ClientGUID
	AND PTIDNHS.TypeCode = 'NHS Number'
LEFT JOIN [$(EPR)].dbo.CV3OrderCatalogMasterItem AS Item ON Orders.OrderCatalogMasterItemGUID = Item.GUID
WHERE-- PTIDRM2.ClientIDCode = @DistrictNo )
	-- AND 
	Item.SpecimenType = 'Blood'
	AND Orders.OrderStatusCode = 'PCOL'

and	(Visit.TypeCode = @LocationTypeCode Or @LocationTypeCode = '<All>')

and	(
		Visit.CurrentLocationGUID = @LocationGUID
	or	@LocationGUID = 0
	)
