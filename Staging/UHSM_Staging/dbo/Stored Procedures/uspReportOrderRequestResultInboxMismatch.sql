﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Order Requested By and Results Inbox Mismatch

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:						1.0.0.0 - 19/01/2017 - CM
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
Create PROCEDURE [dbo].[uspReportOrderRequestResultInboxMismatch] @Start datetime, @End datetime, @RedAlert varchar(25), @Category varchar(150)

As



--Declare @Start datetime = '2016-12-06'
--		, @End datetime = '2017-01-18'
--		, @RedAlert varchar(1)  = 'Y'
--		, @Category varchar(50) = 'Inbox User is Blank'

--;

--Identify all the Red Alerts -- these can only be identified in the textual results
With RedAlert 
AS
(
Select  obs.GUID as ObsGuid, 
		obs.OrderGUID, 
		obs.value,
		RowNo =  ROW_NUMBER() Over (Partition By obs.GUID Order By txt.GUID), 
		Obs.Entered  as ResultDtm
From [$(EPR)].dbo.CV3BasicObservation obs
Left Outer Join [$(EPR)].dbo.CV3TextualObservationLine  txt on obs.GUID = txt.ObservationGUID
Where IsTextual = 1
		And txt.Text like '%Red Alert%'
		And Cast(obs.Entered as Date) >= @Start
		And Cast(obs.Entered as Date) <= @End

)

,

--Identify all the Results - add row number to get just one row per order as there are multiple results per test
Obs
AS

(
Select	RowNo = ROW_NUMBER() Over (Partition By Obs.OrderGUID Order By Obs.GUID Desc)
		,Obs.OrderGUID
		,Obs.Entered
		,Obs.GUID as ObsGUID
		,RedAlert = Case When RedAlert.OrderGUID is not null Then 1 Else 0 End 
		,ResultDtm =  Obs.Entered 
From	[$(EPR)].dbo.CV3BasicObservation obs With (NoLock)
		Left outer join RedAlert on Obs.OrderGUID = RedAlert.OrderGUID and RedAlert.RowNo = 1
		
Where	 Cast(obs.Entered as Date) >= @Start
		And Cast(obs.Entered as Date) <= @End

		And obs.IsHistory = 0
		--And obs.ItemName Not Like '%Autofile%'
		
)



,
--Get a list of all acknowledged results.
--There can be multiple aknowledgements per order, therefore added row number sequencing here so we can get the last one based on Completion Dtm and completion object guid - desc on both to get the last one when filtering on row number = 1 in seubseuqnt query
Ack
as
(Select ObjectGUID,
		 RowNo =  ROW_NUMBER() Over (Partition By ObjectGUID Order By CompletionDtm desc, CompletionObjectID desc),
		 CompletionStatus,
		 CompletionDtm
From  [$(EPR)].dbo.SXACMPCompletionObject 
Where CompletionStep = 'ResultAck' 
	And CompletionStatus Is Not Null)


,
--to get a list of orders and the result inbox user
Inbox
AS
(
Select em.*, 
		us.DisplayName as [User], 
		prov.DisplayName as Provider
From [$(EPR)].dbo.SXAAMBAWMResult em 
Left outer join [$(EPR)].dbo.CV3User us 
On Em.UserGUID = us.GUID 
Left outer join [$(EPR)].dbo.CV3CareProvider prov 
On em.UserGUID = prov.GUID
)


, Main
AS
(
--Final query
Select	Ord.GUID,
		Obs.ObsGUID,
		Vis.GUID as VisitGUID,
		Vis.IDCode as RM2Number,
		Vis.ClientDisplayName,
		Vis.VisitIDCode,
		Vis.TypeCode,
		ord.IDCode as OrderID,
		ord.Name,
		ord.RequestedDtm,
		ord.SignificantDtm, 
		--ord.CareProviderGUID,
		OrdCP.DisplayName as RequestedBy,
	--	CP.DisplayName ,
		LeadProv.ProviderGUID,
		CPLead.DisplayName LeadProvider,  
		Obs.ResultDtm,
		--Obs.*,
		Ord.CareProviderGUID
		,LeadProv.RoleCode
		,CASE WHEN ord.CareProviderGUID <> LeadProv.ProviderGUID THEN 1
				WHEN LeadProv.ProviderGUID Is NULL THEN 1 
				ELSE 0
				END as NoMatch -- add this case statement to flag those records where the Requested by care provider is not the same as the Lead provider on the visit -  
		,comp.CompletionStatus
		,comp.CompletionDtm
		,Inbox.[User] as InboxUser
		,Inbox.Provider as InboxProvider
		,CASE WHEN Inbox.UserGUID is NULL THEN 'Inbox User is Blank'
				WHEN ord.CareProviderGUID <> Inbox.UserGUID THEN 'Inbox User Different'
				WHEN ord.CareProviderGUID IS NULL  THEN 'Requested By is Blank'
				WHEn ord.CareProviderGUID = '4972201190'  THEN 'Requested By Unknown Clinician'
				Else 'OK' End as Category
		--,CPVis.*
		,CASE WHEN RedAlert.OrderGUID IS NOT NULL THEN 'Y' Else 'N' End  as RedAlertFlag

From Obs --get all observations where row number = 1 so that we get a single row per order as there are multipple observation rows per order
Left outer join [$(EPR)].dbo.CV3Order ord --link back to the order to get details of the order - Requested By, visit guid etc
		On Obs.OrderGUID = ord.GUID
--Left outer join  (Select * From PROD.dbo.CV3CareProviderVisitRole where RoleCode = 'Lead Provider') CPVis
--		On ord.CareProviderGUID = CPVis.ProviderGUID  AND ord.ClientVisitGUID = CPVis.ClientVisitGUID 
Left Outer Join [$(EPR)].dbo.CV3ClientVisit vis --link to client visit to get the patient's details - RM2, Name, etc
		On ord.ClientVisitGUID = vis.GUID
--Left Outer Join PROD.dbo.CV3CareProvider CP 
--			On CPVis.ProviderGUID = CP.GUID
Left Outer Join [$(EPR)].dbo.CV3CareProvider OrdCP --Care provider GUID in the orders table is Requested By care provider
			On ord.CareProviderGUID = OrdCP.GUID
Left outer join  (Select * From [$(EPR)].dbo.CV3CareProviderVisitRole where RoleCode = 'Lead Provider' and Status = 'Active' and ToDtm is not null) LeadProv --join to this table to indetify who the lead provider for the visit
				On ord.ClientVisitGUID = LeadProv.ClientVisitGUID 
Left Outer Join [$(EPR)].dbo.CV3CareProvider CPLead--join to this table to get the name of the Lead provider 
			On LeadProv.ProviderGUID = CPLead.GUID
--Left Outer Join (Select * from  PROD.dbo.SXACMPCompletionObject Where CompletionStep = 'ResultAck' ) comp
--			On ord.GUID = comp.ObjectGUID
Left Outer Join Ack comp --join to this table to see if the observation has been acknowledged
			On ord.GUID = comp.ObjectGUID AND comp.RowNo = 1
Left Outer Join Inbox 
			On ord.GUID = Inbox.OrderGUID 
Left Outer Join RedAlert 
			On ord.GUID = RedAlert.OrderGUID and RedAlert.RowNo = 1

Where Obs.RowNo = 1	
--AND Obs.ObsGUID  not in
--('3715568100070',
--'3959117300070',
--'4037433000070',
--'4037433100070')
)





Select RM2Number,
		PatientName  = ClientDisplayName,
		VisitID =  VisitIDCode,
		VisitType = TypeCode,
		OrderID,
		OrderRequestDate = RequestedDtm,
		OrderSignificantDate = SignificantDtm,
		OrderTest = Name,
		OrderRequestedBy = RequestedBy,
		VisitLeadProvider = LeadProvider,
		ResultDateTime =  ResultDtm,
		AcknowledgedDate = CompletionDtm,
		InboxUser,
		RedAlertFlag,
		Category

From Main
Where CompletionStatus Is Null
And RedAlertFlag in (SELECT Item
                         FROM   dbo.Split (@RedAlert, ',')) 
And Category in (SELECT Item
                         FROM   dbo.Split (@Category, ','))