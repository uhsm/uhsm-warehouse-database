﻿
--/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
--Purpose:		Report Results Acknowledgement Summary SSRS report

--Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

--Versions:		--	1.0.0.0 - 24/01/2017 - CM
--					Created sproc.
--------------------------------------------------------------------------------------------------------------------------------------------------------
--*/


Create PROCEDURE [dbo].[uspReportResultsAcknowledgement] 

     @SSRSUserID varchar(50)
	,@StartDate date
	,@EndDate date
	,@Specialty varchar(Max)
	,@ClinicianID varchar(Max)
	,@VisitType varchar(Max)
	--,@LocationType varchar(Max)
	,@Location varchar(Max)
	,@ProviderType varchar(Max)
	,@Discipline varchar(Max)
	,@FilingStatus varchar(Max)
	,@Abnormal varchar(Max)
	,@ChestXray varchar(10)

AS 

Select  DivisionID 
	,Division 
	,DirectorateID
	,Directorate 
	,SpecialtyCode as SubSpecialtyCode
	,Specialty
	, ClinicianID = Coalesce(ClinicianID,'9999999') 
	,Clinician =  Coalesce(Clinician,'Unknown')  
	,[Location]
	,Discipline
	,Reports = Count(*)
	,UnacknowledgedReports = Sum(Case When Filingstatus = 'N' Then 1 Else 0 End)

from ReportResultsAcknowledgement
Where SpecialtyCode in 
		(Select Item From  dbo.Split(@Specialty,','))
	And Coalesce(ClinicianID,'9999999')  in 
		(Select Item From  dbo.Split(@ClinicianID,','))
	And RTRIM(TypeCode) in 
		(Select Item From  dbo.Split(@VisitType,','))
	--And LocationType in 
	--	(Select Item From  dbo.Split(@LocationType,','))
	And Location in 
		(Select Item From  dbo.Split(@Location,','))
	And ProviderType in
		(Select Item From  dbo.Split(@ProviderType,','))
	And Discipline in 
		(Select Item From  dbo.Split(@Discipline,','))	
	And FilingStatus in 
		(Select Item From  dbo.Split(@FilingStatus,','))
	And Case When Abnormal = 'Normal' Then 'N' Else 'A' End  in 
		(Select Item From  dbo.Split(@Abnormal,','))
	And ChestXray in
		(Select Item From  dbo.Split(@ChestXray,','))
	And Cast(SignificantDtm as Date) Between @StartDate and @EndDate

Group by  DivisionID 
	,Division 
	,DirectorateID
	,Directorate 
	,SpecialtyCode
	,Specialty
	, Coalesce(ClinicianID,'9999999') 
	, Coalesce(Clinician,'Unknown') 
	,[Location]
	,Discipline


		--Inner Join dbo.Split(@Specialty,',') spec
		--	On vis.SubSpecialtyCode = spec.Item

		--Inner Join dbo.Split(@ClinicianID,',') clin
		--	On ord.UserGUID = clin.Item

		--Inner Join dbo.Split(@LocationType,',') loctype
		--	On vis.LocationType = loctype.Item

		--Inner Join dbo.Split(@Location,',') loclist
		--	On vis.[Location] = loclist.Item

		--Inner Join dbo.Split(@ProviderType,',') ptype
		--	On Case
		--		When frm.[Name] Like 'RAD%' Then 'Radiology'
		--		Else 'Pathology'
		--	End = ptype.Item

		--Inner Join dbo.Split(@Discipline,',') pdisc
		--	On org.[Name] = pdisc.Item

		--Inner Join dbo.Split(@FilingStatus,',') fstat
		--	On Case When fs.ObjectGUID Is Not Null Then 'Y' Else 'N' End = fstat.Item

		--Inner Join dbo.Split(@Abnormal,',') ablist
		--	On src.AbnormalityCode = ablist.Item

		--Inner Join dbo.Split(@ChestXray,',') cxr
		--	On Case When an.[Name] Like 'XCHES%' Then 'Y' Else 'N' End = cxr.Item