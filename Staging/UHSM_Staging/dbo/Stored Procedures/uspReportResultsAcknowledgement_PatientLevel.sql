﻿
--/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
--Purpose:		Report Results Acknowledgement PTL SSRS report

--Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

--Versions:		--	1.0.0.0 - 24/01/2017 - CM
--					Created sproc.
--------------------------------------------------------------------------------------------------------------------------------------------------------
--*/
Create PROCEDURE  [dbo].[uspReportResultsAcknowledgement_PatientLevel]
     @SSRSUserID varchar(50)
	,@StartDate date
	,@EndDate date
	,@Specialty varchar(Max)
	,@ClinicianID varchar(Max)
	,@VisitType varchar(Max)
	--,@LocationType varchar(Max)
	,@Location varchar(Max)
	,@ProviderType varchar(Max)
	,@Discipline varchar(Max)
	,@FilingStatus varchar(Max)
	,@Abnormal varchar(Max)
	,@ChestXray varchar(10)

AS 
Select Division,
		Directorate,
		DivisionID,
		DirectorateID,
		SpecialtyCode,
		Specialty,
		 ClinicianID = Coalesce(ClinicianID,'9999999') ,
		Clinician =  Coalesce(Clinician,'Unknown') , 
		Consultant = Clinician,
		Location,
		LocationType,
		Discipline,
		ProviderType,
		RM2Number,
		OrderGUID,
		Investigation = OrderItemName,
		ack.RequestedDtm,
		ack.SignificantDtm,
		ack.TypeCode,
		Abnormal,
		FilingStatus,
		ChestXray,
		PatientName 

From ReportResultsAcknowledgement ack 


Where SpecialtyCode in 
		(Select Item From  dbo.Split(@Specialty,','))
	And Coalesce(ClinicianID,'9999999')  in 
		(Select Item From  dbo.Split(@ClinicianID,','))
	--And LocationType in 
	--	(Select Item From  dbo.Split(@LocationType,','))
	And RTRIM(ack.TypeCode) in 
		(Select * From  dbo.Split(@VisitType,','))
	And Location in 
		(Select Item From  dbo.Split(@Location,','))
	And ProviderType in
		(Select Item From  dbo.Split(@ProviderType,','))
	And Discipline in 
		(Select Item From  dbo.Split(@Discipline,','))	
	And FilingStatus in 
		(Select Item From  dbo.Split(@FilingStatus,','))
	And Case When Abnormal = 'Normal' Then 'N' Else 'A' End  in 
		(Select Item From  dbo.Split(@Abnormal,','))
	And ChestXray in
		(Select Item From  dbo.Split(@ChestXray,','))
	And Cast(ack.SignificantDtm as Date) Between @StartDate and @EndDate
Order By ack.SignificantDtm