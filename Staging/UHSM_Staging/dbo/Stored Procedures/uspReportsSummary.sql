﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Reports Summary

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:		
				1.0.0.0 - 08/12/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure dbo.uspReportsSummary
	@SSRSUserID varchar(50)
	,@StartDate date
	,@EndDate date
	,@Specialty varchar(Max)
	,@ClinicianID varchar(Max)
	,@LocationType varchar(Max)
	,@Location varchar(Max)
	,@ProviderType varchar(Max)
	,@Discipline varchar(Max)
	,@FilingStatus varchar(Max)
	,@Abnormal varchar(Max)
	,@ChestXray varchar(10)
As

Declare @Division_Unknown_ID int
Declare @Directorate_Unknown_ID int

Set @Division_Unknown_ID = (Select DivisionID From Division Where Division = 'Unknown')
Set @Directorate_Unknown_ID = (Select DirectorateID From Directorate Where Directorate = 'Unknown')

-- Clear out the temp tables for this user
Delete ztReportsPTL_Obs Where SSRSUserID = @SSRSUserID
Delete ztReportsPTL_Abnormal Where SSRSUserID = @SSRSUserID
Delete ztReportsPTL_FilingStatus Where SSRSUserID = @SSRSUserID
Delete ztReportsPTL_Locations1 Where SSRSUserID = @SSRSUserID
Delete ztReportsPTL_Locations2 Where SSRSUserID = @SSRSUserID
Delete ztReportsPTL_Visit Where SSRSUserID = @SSRSUserID

-- Load the temp tables
Insert Into ztReportsPTL_Obs(SSRSUserID,RowNo,OrderGUID,EnteredDate,EnteredDateTime,AbnormalityCode)
	Select	@SSRSUserID
			,RowNo = ROW_NUMBER() Over (Partition By src.OrderGUID Order By src.GUID Desc)
			,src.OrderGUID
			,src.EnteredDate
			,src.EnteredDateTime
			,obs.AbnormalityCode
	From	CV3BasicObservationIndex src With (NoLock)
			Inner Join [$(EPR)].dbo.CV3BasicObservation obs With (NoLock)
				On src.[GUID] = obs.[GUID]
	Where	src.EnteredDate Between @StartDate And @EndDate
			And obs.IsHistory = 0
			And obs.ItemName Not Like '%Autofile%'
	
Insert Into ztReportsPTL_Abnormal(SSRSUserID,RowNo,OrderGUID,AbnormalityDef)
	Select	@SSRSUserID
			,RowNo = ROW_NUMBER() Over (Partition By obs.OrderGUID Order By ab.SequenceNumber)
			,obs.OrderGUID
			,AbnormalityDef = dbo.fn_SCM_Abnormal_Definition(obs.AbnormalityCode)
	From	ztReportsPTL_Obs src With (NoLock)
			Inner Join [$(EPR)].dbo.CV3BasicObservation obs With (NoLock)
				On src.OrderGUID = obs.OrderGUID
			Left Join [$(EPR)].dbo.SXAGNAbnormalCodeFn() ab
				On src.AbnormalityCode = ab.Code
	Where	src.SSRSUserID = @SSRSUserID
			And src.RowNo = 1
			And obs.IsHistory = 0
			And obs.ItemName Not Like '%Autofile%'

Insert Into ztReportsPTL_FilingStatus(SSRSUserID,RowNo,ObjectGUID,FilingStatus)
	Select	@SSRSUserID
			,RowNo = ROW_NUMBER() Over (Partition By co.ObjectGUID Order By co.CompletionObjectID Desc)
			,co.ObjectGUID
			,FilingStatus = LTRIM(RTRIM(co.CompletionStatus)) + ' - ' + LTRIM(RTRIM(co.CompletionStep))
	From	ztReportsPTL_Obs src With (NoLock)
			Inner Join [$(EPR)].dbo.SXACMPCompletionObject co With (NoLock)
				On src.OrderGUID = co.ObjectGUID
	Where	src.SSRSUserID = @SSRSUserID
			And src.RowNo = 1

Insert Into ztReportsPTL_Locations1(SSRSUserID,RowNo,ClientVisitGUID,LocationGUID,TransferRequestDtm)
	Select	@SSRSUserID
			,RowNo = ROW_NUMBER() Over (Partition By vl.ClientVisitGUID Order By vl.TransferRequestDtm)
			,vl.ClientVisitGUID
			,vl.LocationGUID
			,vl.TransferRequestDtm
	From	ztReportsPTL_Obs src With (NoLock)
			Inner Join CV3OrderIndex ord With (NoLock)
				On src.OrderGUID = ord.[GUID]
			Inner Join [$(EPR)].dbo.CV3ClientVisitLocation vl With (NoLock)
				On ord.ClientVisitGUID = vl.ClientVisitGUID
	Where	src.SSRSUserID = @SSRSUserID
			And src.RowNo = 1

Insert Into ztReportsPTL_Locations2(SSRSUserID,ClientVisitGUID,LocationGUID,StartDateTime,EndDateTime)
	Select	@SSRSUserID
			,src.ClientVisitGUID
			,src.LocationGUID
			,StartDateTime = src.TransferRequestDtm
			,EndDateTime = Coalesce(loc.TransferRequestDtm,vis.DischargeDtm)
	From	ztReportsPTL_Locations1 src With (NoLock)

			Left Join ztReportsPTL_Locations1 loc With (NoLock)
				On loc.SSRSUserID = @SSRSUserID
				And src.ClientVisitGUID = loc.ClientVisitGUID
				And src.RowNo + 1 = loc.RowNo

			Left Join [$(EPR)].dbo.CV3ClientVisit vis With (NoLock)
				On src.ClientVisitGUID = vis.[GUID]

	Where	src.SSRSUserID = @SSRSUserID

Insert Into ztReportsPTL_Visit(ClientGUID,[GUID],[Location],LocationType,PatientName,RM2Number,SubSpecialtyCode,SSRSUserID)
Select	Distinct 
		vis.ClientGUID
		,vis.[GUID]
		,loch.LocationLevel3
		,loch.LocationLevel2
		,vis.ClientDisplayName
		,vis.IDCode
		,Coalesce(serv.GroupCode,'ZZZ') -- ZZZ is a pseudo code for SubSpecialty = Unknown
		,@SSRSUserID
From	ztReportsPTL_Obs src With (NoLock)

		Inner Join CV3OrderIndex ord With (NoLock)
			On src.OrderGUID = ord.[GUID]

		Inner Join [$(EPR)].dbo.CV3ClientVisit vis With (NoLock)
			On ord.ClientVisitGUID = vis.[GUID]
	
		Left Join [$(EPR)].dbo.CV3Service serv With (NoLock)
			On vis.ServiceGUID = serv.[GUID]

		Left Join ztReportsPTL_Locations2 loc With (NoLock)
			On loc.SSRSUserID = @SSRSUserID
			And vis.[GUID] = loc.ClientVisitGUID
			And ord.RequestedDateTime >= loc.StartDateTime
			And (ord.RequestedDateTime < loc.EndDateTime Or loc.EndDateTime Is Null)

		Left Join LocationHierarchy loch With (NoLock)
			On	Coalesce(loc.LocationGUID,vis.CurrentLocationGUID) = loch.LocationGUID

Where	src.SSRSUserID = @SSRSUserID
		And src.RowNo = 1

Select	
	DivisionID = Coalesce(sp.DivisionID,@Division_Unknown_ID)
	,Division = Coalesce(sp.Division,'Unknown')
	,DirectorateID = Coalesce(sp.DirectorateID,@Directorate_Unknown_ID)
	,Directorate = Coalesce(sp.Directorate,'Unknown')
	,vis.SubSpecialtyCode
	,Specialty = Coalesce(sp.SubSpecialty,'Unknown')
	,ClinicianID = ord.UserGUID
	,Clinician = us.DisplayName
	,[Location] = Coalesce(vis.[Location],'Unknown')
	,Discipline = org.[Name]
	,Count(*) As Reports

From	ztReportsPTL_Obs src With (NoLock)

		Inner Join CV3OrderIndex ord With (NoLock)
			On src.OrderGUID = ord.[GUID]

		Inner Join ztReportsPTL_Visit vis With (NoLock)
			On vis.SSRSUserID = @SSRSUserID
			And ord.ClientVisitGUID = vis.[GUID]

		Left Join [$(EPR)].dbo.CV3OrderCatalogMasterItem cmi With (NoLock)
			On ord.OrderCatalogMasterItemGUID = cmi.[GUID]

		Left Join [$(EPR)].dbo.CV3AncillaryName an With (NoLock)
			On cmi.[GUID] = an.MainCatItemGUID
			And an.CodingStd Like '%OUT'

		Left Join [$(EPR)].dbo.CV3OrganizationalUnit org With (NoLock)
			On cmi.OrgUnitGUID = org.[GUID]

		Left Join [$(EPR)].dbo.CV3OrderEntryForm frm With (NoLock)
			On ord.OrderEntryFormGUID = frm.[GUID]

		Left Join [$(EPR)].dbo.CV3User us With (NoLock)
			On ord.UserGUID = us.[GUID]

		Left Join ztReportsPTL_Abnormal ab With (NoLock)
			On ab.SSRSUserID = @SSRSUserID
			And ab.RowNo = 1
			And ord.[GUID] = ab.OrderGUID

		Left Join ztReportsPTL_FilingStatus fs With (NoLock)
			On fs.SSRSUserID = @SSRSUserID
			And fs.RowNo = 1
			And ord.[GUID] = fs.ObjectGUID

		Left Join Specialty sp With (NoLock)
			On vis.SubSpecialtyCode = sp.SubSpecialtyCode

		Inner Join dbo.Split(@Specialty,',') spec
			On vis.SubSpecialtyCode = spec.Item

		Inner Join dbo.Split(@ClinicianID,',') clin
			On ord.UserGUID = clin.Item

		Inner Join dbo.Split(@LocationType,',') loctype
			On vis.LocationType = loctype.Item

		Inner Join dbo.Split(@Location,',') loclist
			On vis.[Location] = loclist.Item

		Inner Join dbo.Split(@ProviderType,',') ptype
			On Case
				When frm.[Name] Like 'RAD%' Then 'Radiology'
				Else 'Pathology'
			End = ptype.Item

		Inner Join dbo.Split(@Discipline,',') pdisc
			On org.[Name] = pdisc.Item

		Inner Join dbo.Split(@FilingStatus,',') fstat
			On Coalesce(fs.FilingStatus,'Unknown') = fstat.Item

		Inner Join dbo.Split(@Abnormal,',') ablist
			On src.AbnormalityCode = ablist.Item

		Inner Join dbo.Split(@ChestXray,',') cxr
			On Case When an.[Name] Like 'XCHES%' Then 'Y' Else 'N' End = cxr.Item

Where	src.SSRSUserID = @SSRSUserID
		And src.RowNo = 1

Group By
	Coalesce(sp.DivisionID,@Division_Unknown_ID)
	,Coalesce(sp.Division,'Unknown')
	,Coalesce(sp.DirectorateID,@Directorate_Unknown_ID)
	,Coalesce(sp.Directorate,'Unknown')
	,vis.SubSpecialtyCode
	,Coalesce(sp.SubSpecialty,'Unknown')
	,ord.UserGUID
	,us.DisplayName
	,Coalesce(vis.[Location],'Unknown')
	,org.[Name]

Order By
	Coalesce(sp.Division,'Unknown')
	,Coalesce(sp.Directorate,'Unknown')
	,Coalesce(sp.SubSpecialty,'Unknown')
	,us.DisplayName
	,Coalesce(vis.[Location],'Unknown')
	,org.[Name]