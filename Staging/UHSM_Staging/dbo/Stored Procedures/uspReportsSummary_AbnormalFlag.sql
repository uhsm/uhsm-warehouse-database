﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Reports Summary - Abnormality List

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:		
				1.0.0.0 - 30/11/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [dbo].[uspReportsSummary_AbnormalFlag]
As

--Select	Distinct 
--		AbnormalityCode = src.Code,
--		AbnormalityDef = dbo.fn_SCM_Abnormal_Definition(src.Code)

--From	PROD.dbo.SXAGNAbnormalCodeFn() src

--Order By src.Code

Select 'A' as AbnormalityCode ,
		'Abnormal' as AbnormalityDef

Union All 

Select 'N' ,
		'Normal'