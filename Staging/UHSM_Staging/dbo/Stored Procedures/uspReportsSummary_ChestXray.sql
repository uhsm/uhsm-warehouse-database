﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Reports Summary - Chest Xray List

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:		
				1.0.0.0 - 30/11/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
Create Procedure dbo.uspReportsSummary_ChestXray
As

Select	'N' As Code,
		'No' As Def

Union All

Select	'Y' As Code,
		'Yes' As Def

Order By Code