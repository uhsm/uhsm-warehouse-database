﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Reports Summary - Clinician List

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:		
				1.0.0.0 - 30/11/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure dbo.uspReportsSummary_Clinician
	@StartDate date
	,@EndDate date
	,@Specialty varchar(Max)
As

Declare @Directorate_Unknown_ID int
Set @Directorate_Unknown_ID = (Select DirectorateID From Directorate Where Directorate = 'Unknown')

Select	Distinct 
		ClinicianID = ord.UserGUID
		,Clinician = Replace(us.DisplayName,',','')

From	CV3BasicObservationIndex src With (NoLock)
	
		Inner Join CV3OrderIndex ord With (NoLock)
			On src.OrderGUID = ord.[GUID]

		Inner Join [$(EPR)].dbo.CV3ClientVisit vis With (NoLock)
			On ord.ClientVisitGUID = vis.[GUID]

		Left Join [$(EPR)].dbo.CV3Service serv With (NoLock)
			On vis.ServiceGUID = serv.[GUID]

		Left Join Specialty sp With (NoLock)
			On serv.GroupCode = sp.SubSpecialtyCode

		Inner Join [$(EPR)].dbo.CV3User us With (NoLock)
			On ord.UserGUID = us.[GUID]
		Inner Join dbo.Split(@Specialty,',') spec
			On Coalesce(serv.GroupCode,'ZZZ') = spec.Item

Where	src.EnteredDate Between @StartDate And @EndDate

Order By Replace(us.DisplayName,',','')