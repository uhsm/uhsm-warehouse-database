﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Reports Summary - Clinician List

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:		
				1.0.0.0 - 30/11/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
--*/
CREATE Procedure [dbo].[uspReportsSummary_ClinicianFlag]
	@StartDate date
	,@EndDate date
	,@Specialty varchar(Max)
As

Declare @Directorate_Unknown_ID int
Set @Directorate_Unknown_ID = (Select DirectorateID From Directorate Where Directorate = 'Unknown')


--Declare @Specialty varchar(50) = 'ZZZ,330',
--@StartDate datetime = '2017-01-01',
--@EndDate datetime = '2017-01-23'


Select	Distinct 
		ClinicianID = ClinicianID 
		, Clinician = Replace(Clinician,',','')


From	ReportResultsAcknowledgement src With (NoLock)
	
Where  SpecialtyCode in (Select Item From dbo.Split(@Specialty,',') )
--And  Cast(src.SignificantDtm as Date) Between @StartDate And @EndDate

Union All

Select '9999999'
		,'Unknown'

Order By Replace(Clinician,',','')