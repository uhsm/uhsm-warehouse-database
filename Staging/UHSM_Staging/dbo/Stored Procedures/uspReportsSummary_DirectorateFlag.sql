﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Reports Summary - Directorate List

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:		
				1.0.0.0 - 30/11/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
Create Procedure [dbo].[uspReportsSummary_DirectorateFlag]
	@DivisionID varchar(Max)
As

Declare @Division_Unknown_ID int
Set @Division_Unknown_ID = (Select DivisionID From Division Where Division = 'Unknown')

Select	src.DirectorateID,
		src.Directorate

From	Directorate src With (NoLock)

		Inner Join dbo.Split(@DivisionID,',') div
			On Coalesce(src.DivisionID,@Division_Unknown_ID) = div.Item

Order By src.Directorate