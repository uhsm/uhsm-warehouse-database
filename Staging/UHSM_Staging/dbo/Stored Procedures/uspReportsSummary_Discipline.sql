﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Reports Summary - Discipline List

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:		
				1.0.0.0 - 30/11/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure dbo.uspReportsSummary_Discipline
	@StartDate date,
	@EndDate date
As

Select	Distinct org.[Name] As Discipline

From	CV3BasicObservationIndex src With (NoLock)

		Inner Join CV3OrderIndex ord With (NoLock)
			On src.OrderGUID = ord.[GUID]

		Inner Join [$(EPR)].dbo.CV3OrderCatalogMasterItem cmi With (NoLock)
			On ord.OrderCatalogMasterItemGUID = cmi.[GUID]

		Inner Join [$(EPR)].dbo.CV3OrganizationalUnit org With (NoLock)
			On cmi.OrgUnitGUID = org.[GUID]

Where	src.EnteredDate Between @StartDate and @EndDate

Order By org.[Name]