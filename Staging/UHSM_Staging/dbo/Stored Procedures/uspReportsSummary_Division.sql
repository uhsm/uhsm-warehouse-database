﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Reports Summary - Division List

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:
				1.0.0.2 - 08/12/2016 - MT
					Replaced code that updates index tables with a called to a sub procedure.
					This sub procedure can then be used by other SSRS reports.

				1.0.0.1 - 07/12/2016 - MT
					CV3BasicObservation does not have an index on the key field called Entered.
					Created a temp table to be used as an index - CV3BasicObservationIndex.
					As this is the first sproc to run when the SSRS report loads I'm adding code here to
					bring this table up to date.  Note that thist table on has data from go live (06/12/2016 20:00) as
					I believe that data prior to this will not be required in the SSRS report.
					Ditto for CV3OrderIndex.

				1.0.0.0 - 30/11/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure dbo.uspReportsSummary_Division
As

-- Update Index Tables
Exec dbo.uspLoadIndexTables

-- Select Divisions
Select	src.DivisionID,
		src.Division

From	Division src With (NoLock)

Order By src.Division