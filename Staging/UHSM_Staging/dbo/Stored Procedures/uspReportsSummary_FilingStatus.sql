﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Reports Summary - Filing Status List

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:		
				1.0.0.0 - 30/11/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure dbo.uspReportsSummary_FilingStatus
As

Select	Distinct 
		LTRIM(RTRIM(st.CompletionStatus)) + ' - ' + LTRIM(RTRIM(st.CompletionStep)) As FilingStatus

From	CV3OrderIndex src With (NoLock)

		Inner Join [$(EPR)].dbo.SXACMPCompletionObject st With (NoLock)
			On src.[GUID] = st.ObjectGUID

Where	src.RequestedDate >= DateAdd(yy,-1,GETDATE())

Union All

Select	'Unknown' As FilingStatus

Order By FilingStatus