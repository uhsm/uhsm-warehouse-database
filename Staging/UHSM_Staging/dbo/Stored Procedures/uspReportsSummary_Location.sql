﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Reports Summary - Location List

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:		
				1.0.0.0 - 30/11/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure dbo.uspReportsSummary_Location
	@StartDate date
	,@EndDate date
	,@Specialty varchar(Max)
	,@ClinicianID varchar(Max)
	,@LocationType varchar(Max)
As

Declare @Directorate_Unknown_ID int
Set @Directorate_Unknown_ID = (Select DirectorateID From Directorate Where Directorate = 'Unknown')

Select	Distinct Coalesce(loch.LocationLevel3,'Unknown') As [Location]

From	CV3BasicObservationIndex src With (NoLock)

		Inner Join CV3OrderIndex ord With (NoLock)
			On src.OrderGUID = ord.[GUID]

		Inner Join [$(EPR)].dbo.CV3ClientVisit vis With (NoLock)
			On ord.ClientVisitGUID = vis.[GUID]

		Left Join [$(EPR)].dbo.CV3Service serv With (NoLock)
			On vis.ServiceGUID = serv.[GUID]

		Left Join Specialty sp With (NoLock)
			On serv.GroupCode = sp.SubSpecialtyCode

		Inner Join [$(EPR)].dbo.CV3User us With (NoLock)
			On ord.UserGUID = us.[GUID]

		Inner Join [$(EPR)].dbo.CV3ClientVisitLocation loc With (NoLock)
			On vis.GUID = loc.ClientVisitGUID

		Inner Join LocationHierarchy loch With (NoLock)
			On	Coalesce(loc.LocationGUID,vis.CurrentLocationGUID) = loch.LocationGUID

		Inner Join dbo.Split(@Specialty,',') spec
			On Coalesce(serv.GroupCode,'ZZZ') = spec.Item

		Inner Join dbo.Split(@ClinicianID,',') clin
			On ord.UserGUID = clin.Item

		Inner Join dbo.Split(@LocationType,',') loctype
			On loch.LocationLevel2 = loctype.Item

Where	src.EnteredDate Between @StartDate And @EndDate

Order By Coalesce(loch.LocationLevel3,'Unknown')