﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Reports Summary - Location Type List

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:		
				1.0.0.0 - 30/11/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
Create Procedure [dbo].[uspReportsSummary_LocationTypeFlag]
As

--Select	Distinct loch.LocationLevel2 As LocationType

--From	CV3OrderIndex src With (NoLock)

--		Inner Join PROD.dbo.CV3ClientVisit vis With (NoLock)
--			On src.ClientVisitGUID = vis.[GUID]

--		Inner Join LocationHierarchy loch With (NoLock)
--			On vis.CurrentLocationGUID = loch.LocationGUID

--Where	src.RequestedDate >= DateAdd(yy,-1,GETDATE())

--Order By loch.LocationLevel2

Select Distinct LocationType 
From ReportResultsAcknowledgement