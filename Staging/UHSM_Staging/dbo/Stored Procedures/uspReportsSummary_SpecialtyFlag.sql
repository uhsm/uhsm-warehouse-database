﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Reports Summary - Specialty List

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:		
				1.0.0.0 - 30/11/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE  Procedure [dbo].[uspReportsSummary_SpecialtyFlag]
	@StartDate date
	,@EndDate date
	,@DirectorateID varchar(Max)
As

Declare @Directorate_Unknown_ID int
Set @Directorate_Unknown_ID = (Select DirectorateID From Directorate Where Directorate = 'Unknown')


Select distinct SpecialtyCode = SpecialtyCode,
				SpecialtyName = Specialty
From ReportResultsAcknowledgement
Where DirectorateID in ( Select Item From dbo.Split(@DirectorateID,',') )
And Cast(SignificantDtm as Date) Between @StartDate And @EndDate
Order By Specialty

--Select	Distinct 
--		serv.GroupCode As SpecialtyCode
--		,sp.SubSpecialty As SpecialtyName

--From	CV3BasicObservationIndex src With (NoLock)

--		Inner Join CV3OrderIndex ord With (NoLock)
--			On src.OrderGUID = ord.[GUID]

--		Inner Join PROD.dbo.CV3ClientVisit vis With (NoLock)
--			On ord.ClientVisitGUID = vis.[GUID]

--		Inner Join PROD.dbo.CV3Service serv With (NoLock)
--			On vis.ServiceGUID = serv.[GUID]

--		Inner Join Specialty sp With (NoLock)
--			On serv.GroupCode = sp.SubSpecialtyCode

--		Inner Join dbo.Split(@DirectorateID,',') dir
--			On Coalesce(sp.DirectorateID,@Directorate_Unknown_ID) = dir.Item

--Where	src.EnteredDate Between @StartDate And @EndDate

--Union All

--Select	'ZZZ' As SpecialtyCode
--		,'Unknown' As SpecialtyName

--Order By SpecialtyName