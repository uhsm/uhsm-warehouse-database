﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Reports Summary - Location Type List

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:		
				1.0.0.0 - 25/01/2017 - CM
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [dbo].[uspReportsSummary_VisitType]
As

Select  Distinct RTRIM(TypeCode) as TypeCode
From ReportResultsAcknowledgement
Order by RTRIM(TypeCode)