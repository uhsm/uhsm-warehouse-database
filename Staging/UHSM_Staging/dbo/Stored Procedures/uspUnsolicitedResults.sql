﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Unsolicited Results

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:
				1.0.0.2 - 20/12/2016 - MT
					Previously UHSM_UnsolicitedResults_UB.
					Modified to select only those records where the time that the
					result was entered closely matched the time that the order was created.

				1.0.0.1 - 08/12/2016 - CM 
					Removed CTE for Location as there is a table which can be used  		
			
				1.0.0.0 - 24/11/2016 - DBH
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure dbo.uspUnsolicitedResults
	@SSRSUserID varchar(50),
	@StartDate date,
	@EndDate date,
	@Consultant varchar(max)
As

Delete ztUnsolicitedResults_Order Where SSRSUserID = @SSRSUserID

Insert Into ztUnsolicitedResults_Order(SSRSUserID,[GUID])
Select	Distinct
		@SSRSUserID
		,src.[GUID]
From	CV3OrderIndex src With (NoLock)

		Left Join CV3BasicObservationIndex obs With (NoLock)
			On src.[GUID] = obs.OrderGUID

Where	src.RequestedDate Between @StartDate And @EndDate
		And DateDiff(ss,src.RequestedDateTime,obs.EnteredDateTime) Between -60 and 60

Select
	RM2Number = rm2.ClientIDCode
	,NHSNo = nhs.ClientIDCode
	,PatientName = pat.DisplayName
	,DOB = dbo.fn_SCM_DOB(pat.BirthYearNum,pat.BirthMonthNum,pat.BirthDayNum)
	,vis.[CurrentLocation]
	,Consultant = pr.DisplayName
	,ResultDateTime = ord.RequestedDtm
	,OrderName = ord.[Name]

From	ztUnsolicitedResults_Order src With (NoLock)

		Inner Join [$(EPR)].dbo.CV3Order ord With (NoLock)
			On src.[GUID] = ord.[GUID]

		Left Join [$(EPR)].dbo.CV3Client pat With (NoLock)
			On ord.ClientGUID = pat.[GUID]

		Left Join [$(EPR)].dbo.CV3ClientID As rm2 With (NoLock)
			On ord.ClientGUID = rm2.ClientGUID
			And rm2.TypeCode = 'Hospital Number'

		Left Join [$(EPR)].[dbo].[CV3ClientID] nhs With (NoLock)
			On ord.ClientGUID = nhs.ClientGUID
			And nhs.TypeCode = 'NHS Number'

		Left Join CV3ClientVisitIndex vis
			On ord.ClientVisitGUID = vis.[GUID]

		Left Join [$(EPR)].dbo.CV3CareProvider pr
			On ord.CareProviderGUID = pr.[GUID]

Where	src.SSRSUserID = @SSRSUserID
		And (Convert(varchar(20),pr.[GUID]) = @Consultant Or @Consultant = '<All>')

Order By
	rm2.ClientIDCode
	,nhs.ClientIDCode
	,pat.DisplayName
	,ord.RequestedDtm