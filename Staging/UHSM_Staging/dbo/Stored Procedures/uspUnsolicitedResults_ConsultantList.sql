﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Unsolicited Results - Consultant List

Notes:			Stored in SM1SCM-CPM1.UHSM_Staging

Versions:
				1.0.0.0 - 20/12/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
Create Procedure dbo.uspUnsolicitedResults_ConsultantList
	@StartDate date,
	@EndDate date
As

Select	Distinct
	ConsultantCode = ord.CareProviderGUID
	,ConsultantName = pr.DisplayName

From	CV3OrderIndex src With (NoLock)

		Inner Join CV3BasicObservationIndex obs With (NoLock)
			On src.[GUID] = obs.OrderGUID

		Inner Join [$(EPR)].dbo.CV3Order ord With (NoLock)
			On src.[GUID] = ord.[GUID]

		Inner Join [$(EPR)].dbo.CV3CareProvider pr
			On ord.CareProviderGUID = pr.[GUID]

Where	src.RequestedDate Between @StartDate And @EndDate
		And DateDiff(ss,src.RequestedDateTime,obs.EnteredDateTime) Between -60 and 60

Order By
	pr.DisplayName