﻿CREATE TABLE [dbo].[CRISExam]
(
	[ExamKey] [int] IDENTITY(1,1) NOT NULL,
	[ExamCode] [nvarchar](8) NOT NULL,
	[ExamName] [nvarchar](40) NULL,
	[RealExam] [nvarchar](3) NOT NULL,
	[ModalityCode] [nvarchar](3) NULL,
	[Modality] [nvarchar](128) NULL,
	[ModalityCategory] [nvarchar](50) NULL,
	[NoOfProcedures] [int] NULL,
	[LocalGroupCode] [varchar](10) NULL,
	[LocalGroupCodeDescription] [varchar](35) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_CRISExam] PRIMARY KEY CLUSTERED 
(
	[ExamKey] ASC
)
)
