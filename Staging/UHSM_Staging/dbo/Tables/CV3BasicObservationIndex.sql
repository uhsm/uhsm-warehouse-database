﻿CREATE TABLE [dbo].[CV3BasicObservationIndex] (
    [EnteredDate]     DATE     NOT NULL,
    [EnteredDateTime] DATETIME NOT NULL,
    [GUID]            BIGINT   NOT NULL,
    [OrderGUID]       BIGINT   NOT NULL,
    CONSTRAINT [PK_CV3BasicObservationIndex] PRIMARY KEY NONCLUSTERED ([GUID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_CV3BasicObservationIndex_OrderGUID]
    ON [dbo].[CV3BasicObservationIndex]([OrderGUID] ASC);


GO
CREATE CLUSTERED INDEX [IX_CV3BasicObservationIndex_EnteredDate]
    ON [dbo].[CV3BasicObservationIndex]([EnteredDate] ASC);

