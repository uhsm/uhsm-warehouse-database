﻿CREATE TABLE [dbo].[CV3ClientVisitIndex] (
    [GUID]              BIGINT        NOT NULL,
    [AdmissionDate]     DATE          NULL,
    [AdmissionDateTime] DATETIME      NULL,
    [ConsultantName]    VARCHAR (100) NULL,
    [CurrentLocation]   VARCHAR (80)  NULL,
    [DischargeDate]     DATE          NULL,
    [DischargeDateTime] DATETIME      NULL,
    [PatientName]       VARCHAR (100) NULL,
    [RM2Number]         VARCHAR (20)  NULL,
    [SubSpecialtyCode]  VARCHAR (20)  NULL,
    CONSTRAINT [PK_CV3ClientVisitIndex] PRIMARY KEY CLUSTERED ([GUID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_CV3ClientVisitIndex_DischargeDate]
    ON [dbo].[CV3ClientVisitIndex]([DischargeDate] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CV3ClientVisitIndex_SubSpecialtyCode]
    ON [dbo].[CV3ClientVisitIndex]([SubSpecialtyCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CV3ClientVisitIndex_AdmissionDate]
    ON [dbo].[CV3ClientVisitIndex]([AdmissionDate] ASC);

