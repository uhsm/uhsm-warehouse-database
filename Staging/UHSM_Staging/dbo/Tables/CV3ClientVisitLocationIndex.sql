﻿CREATE TABLE [dbo].[CV3ClientVisitLocationIndex] (
    [GUID]                    BIGINT   NOT NULL,
    [ClientVisitGUID]         BIGINT   NULL,
    [LocationGUID]            BIGINT   NULL,
    [TransferRequestDate]     DATE     NULL,
    [TransferRequestDateTime] DATETIME NULL,
    CONSTRAINT [PK_CV3ClientVisitLocationIndex] PRIMARY KEY CLUSTERED ([GUID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_CV3ClientVisitLocationIndex_TransferRequestDateTime]
    ON [dbo].[CV3ClientVisitLocationIndex]([TransferRequestDateTime] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CV3ClientVisitLocationIndex_TransferRequestDate]
    ON [dbo].[CV3ClientVisitLocationIndex]([TransferRequestDate] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CV3ClientVisitLocationIndex_LocationGUID]
    ON [dbo].[CV3ClientVisitLocationIndex]([LocationGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CV3ClientVisitLocationIndex_ClientVisitGUID]
    ON [dbo].[CV3ClientVisitLocationIndex]([ClientVisitGUID] ASC);

