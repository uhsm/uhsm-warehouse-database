﻿CREATE TABLE [dbo].[CV3OrderCatalogMasterItemGroup] (
    [GUID]           BIGINT        NOT NULL,
    [Name]           VARCHAR (125) NOT NULL,
    [IsCoded]        BIT           NOT NULL,
    [IsBronchoscopy] BIT           NOT NULL,
    PRIMARY KEY CLUSTERED ([GUID] ASC)
);

