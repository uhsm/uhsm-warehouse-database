﻿CREATE TABLE [dbo].[CV3OrderIndex] (
    [CareProviderGUID]           BIGINT   NULL,
    [ClientVisitGUID]            BIGINT   NULL,
    [GUID]                       BIGINT   NOT NULL,
    [OrderCatalogMasterItemGUID] BIGINT   NULL,
    [OrderEntryFormGUID]         BIGINT   NULL,
    [RequestedDate]              DATE     NULL,
    [RequestedDateTime]          DATETIME NULL,
    [UserGUID]                   BIGINT   NULL,
    CONSTRAINT [PK_CV3OrderIndex] PRIMARY KEY CLUSTERED ([GUID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_CV3OrderIndex_UserGUID]
    ON [dbo].[CV3OrderIndex]([UserGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CV3OrderIndex_RequestedDate]
    ON [dbo].[CV3OrderIndex]([RequestedDate] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CV3OrderIndex_OrderEntryFormGUID]
    ON [dbo].[CV3OrderIndex]([OrderEntryFormGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CV3OrderIndex_OrderCatalogMasterItemGUID]
    ON [dbo].[CV3OrderIndex]([OrderCatalogMasterItemGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CV3OrderIndex_ClientVisitGUID]
    ON [dbo].[CV3OrderIndex]([ClientVisitGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CV3OrderIndex_CareProviderGUID]
    ON [dbo].[CV3OrderIndex]([CareProviderGUID] ASC);

