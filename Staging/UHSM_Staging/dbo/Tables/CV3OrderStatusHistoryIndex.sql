﻿CREATE TABLE [dbo].[CV3OrderStatusHistoryIndex] (
    [CreatedWhenDate]     DATE        NULL,
    [CreatedWhenDateTime] DATETIME    NULL,
    [GUID]                BIGINT      NOT NULL,
    [OrderGUID]           BIGINT      NULL,
    [OrderStatusCode]     VARCHAR (5) NULL,
    [RequestedByGUID]     BIGINT      NULL,
    CONSTRAINT [PK_CV3OrderStatusHistoryIndex] PRIMARY KEY NONCLUSTERED ([GUID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_CV3OrderStatusHistoryIndex_RequestedByGUID]
    ON [dbo].[CV3OrderStatusHistoryIndex]([RequestedByGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CV3OrderStatusHistoryIndex_OrderGUID]
    ON [dbo].[CV3OrderStatusHistoryIndex]([OrderGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CV3OrderStatusHistoryIndex_OrderStatusCode]
    ON [dbo].[CV3OrderStatusHistoryIndex]([OrderStatusCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CV3OrderStatusHistoryIndex_CreatedWhenDate]
    ON [dbo].[CV3OrderStatusHistoryIndex]([CreatedWhenDate] ASC);

