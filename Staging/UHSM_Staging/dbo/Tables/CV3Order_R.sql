﻿CREATE TABLE [dbo].[CV3Order_R](
	[JobID] [int] NOT NULL,
	[ObjectName] [varchar](30) NULL,
	[ObjectGUID] [numeric](16, 0) NULL,
	[SubmittedReportGUID] [numeric](16, 0) NULL,
	[TextLine] [varchar](max) NULL,
	[Order_RID] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[MSReplrowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [CV3Order_RPK] PRIMARY KEY NONCLUSTERED 
(
	[Order_RID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[CV3Order_R] ADD  DEFAULT (newsequentialid()) FOR [MSReplrowguid]
GO
