﻿CREATE TABLE [dbo].[CV3VisitListJoin_R](
	[JobID] [int] NOT NULL,
	[ObjectName] [varchar](30) NULL,
	[ObjectGUID] [numeric](16, 0) NULL,
	[SubmittedReportGUID] [numeric](16, 0) NULL,
	[VisitListJoin_RID] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[AdditionalReportInfo] [varchar](8000) NULL,
	[MSReplrowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [CV3VisitListJoin_RPK] PRIMARY KEY NONCLUSTERED 
(
	[VisitListJoin_RID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CV3VisitListJoin_R] ADD  DEFAULT (newsequentialid()) FOR [MSReplrowguid]
GO
