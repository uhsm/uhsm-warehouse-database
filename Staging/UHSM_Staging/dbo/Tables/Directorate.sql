﻿CREATE TABLE [dbo].[Directorate] (
    [DirectorateID] INT          NOT NULL,
    [Directorate]   VARCHAR (80) NOT NULL,
    [DivisionID]    INT          NULL,
    [Division]      VARCHAR (80) NULL,
    CONSTRAINT [PK_Directorate] PRIMARY KEY CLUSTERED ([DirectorateID] ASC)
);

