﻿CREATE TABLE [dbo].[Division] (
    [DivisionID] INT          NOT NULL,
    [Division]   VARCHAR (80) NOT NULL,
    CONSTRAINT [PK_Division] PRIMARY KEY CLUSTERED ([DivisionID] ASC)
);

