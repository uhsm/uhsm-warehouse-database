﻿CREATE TABLE [dbo].[ICETestsDailyBySpecialty] (
    [RequestDate]   DATE         NOT NULL,
    [SpecialtyCode] VARCHAR (20) NOT NULL,
    [Tests]         INT          NULL,
    PRIMARY KEY CLUSTERED ([RequestDate] ASC, [SpecialtyCode] ASC)
);

