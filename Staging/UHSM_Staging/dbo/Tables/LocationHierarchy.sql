﻿CREATE TABLE [dbo].[LocationHierarchy] (
    [LocationGUID]   INT          NOT NULL,
    [Name]           VARCHAR (30) NULL,
    [LocationLevel1] VARCHAR (30) NULL,
    [LocationLevel2] VARCHAR (30) NULL,
    [LocationLevel3] VARCHAR (30) NULL,
    [LocationLevel4] VARCHAR (30) NULL,
    [LocationLevel5] VARCHAR (30) NULL,
    [LocationLevel6] VARCHAR (30) NULL,
    CONSTRAINT [PK_LocationHierarchy] PRIMARY KEY CLUSTERED ([LocationGUID] ASC)
);

