﻿CREATE TABLE [dbo].[ReportResultsAcknowledgement] (
    [DivisionID]     INT           NULL,
    [Division]       VARCHAR (80)  NULL,
    [DirectorateID]  INT           NULL,
    [Directorate]    VARCHAR (80)  NULL,
    [SpecialtyCode]  VARCHAR (20)  NULL,
    [Specialty]      VARCHAR (60)  NULL,
    [ClinicianID]    NUMERIC (19)  NULL,
    [Clinician]      VARCHAR (50)  NULL,
    [Location]       VARCHAR (100) NULL,
    [LocationType]   VARCHAR (50)  NULL,
    [Discipline]     VARCHAR (40)  NULL,
    [ProviderType]   VARCHAR (9)   NOT NULL,
    [RM2Number]      VARCHAR (20)  NULL,
    [OrderGUID]      NUMERIC (16)  NULL,
    [OrderItemName]  VARCHAR (125) NULL,
    [OrderItemCode]  VARCHAR (125) NULL,
    [RequestedDtm]   DATETIME      NULL,
    [SignificantDtm] DATETIME      NULL,
    [VisitGUID]      BIGINT        NOT NULL,
    [VisitIDCode]    CHAR (20)     NULL,
    [TypeCode]       CHAR (15)     NULL,
    [DischargeDtm]   DATETIME      NULL,
    [VisitProvider]  VARCHAR (50)  NULL,
    [Abnormal]       VARCHAR (8)   NOT NULL,
    [Filingstatus]   VARCHAR (1)   NOT NULL,
    [ChestXray]      VARCHAR (1)   NOT NULL,
    [PatientName]    VARCHAR (100) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ReportResultsAcknowledgement_OrderGUID]
    ON [dbo].[ReportResultsAcknowledgement]([OrderGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ReportResultsAcknowledgement_SignificantDtm]
    ON [dbo].[ReportResultsAcknowledgement]([SignificantDtm] ASC);

