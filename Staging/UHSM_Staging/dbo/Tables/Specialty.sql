﻿CREATE TABLE [dbo].[Specialty] (
    [SubSpecialtyCode]      VARCHAR (20) NOT NULL,
    [SubSpecialty]          VARCHAR (80) NULL,
    [SpecialtyFunctionCode] VARCHAR (20) NULL,
    [SpecialtyFunction]     VARCHAR (80) NULL,
    [MainSpecialtyCode]     VARCHAR (20) NULL,
    [MainSpecialty]         VARCHAR (80) NULL,
    [DirectorateID]         INT          NULL,
    [Directorate]           VARCHAR (80) NULL,
    [DivisionID]            INT          NULL,
    [Division]              VARCHAR (80) NULL,
    [LastLoadedDateTime]    DATETIME     NOT NULL,
    CONSTRAINT [PK_Specialty] PRIMARY KEY CLUSTERED ([SubSpecialtyCode] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Specialty_DivisionID]
    ON [dbo].[Specialty]([DivisionID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Specialty_DirectorateID]
    ON [dbo].[Specialty]([DirectorateID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Specialty_SubSpecialtyCode]
    ON [dbo].[Specialty]([SubSpecialtyCode] ASC);

