﻿CREATE TABLE [dbo].[UHSM_SpecialtyDivision_UB] (
    [SpecialtyRefno]        INT          NOT NULL,
    [SpecialtyCode]         VARCHAR (20) NULL,
    [Specialty]             VARCHAR (80) NULL,
    [SpecialtyFunctionCode] VARCHAR (20) NULL,
    [SpecialtyFunction]     VARCHAR (80) NULL,
    [MainSpecialtyCode]     VARCHAR (20) NULL,
    [MainSpecialty]         VARCHAR (80) NULL,
    [RTTSpecialtyCode]      VARCHAR (20) NULL,
    [RTTExcludeFlag]        VARCHAR (1)  NULL,
    [Direcorate]            VARCHAR (80) NULL,
    [Division]              VARCHAR (80) NULL
);

