﻿CREATE TABLE [dbo].[ztOrdersVsReportsPTL_Locations1] (
    [SSRSUserID]         VARCHAR (50) NULL,
    [RowNo]              INT          NULL,
    [ClientVisitGUID]    BIGINT       NULL,
    [LocationGUID]       BIGINT       NULL,
    [TransferRequestDtm] DATETIME     NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ztOrdersVsReportsPTL_Locations1_SSRSUserID]
    ON [dbo].[ztOrdersVsReportsPTL_Locations1]([SSRSUserID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ztOrdersVsReportsPTL_Locations1_ClientVisitGUID]
    ON [dbo].[ztOrdersVsReportsPTL_Locations1]([ClientVisitGUID] ASC);

