﻿CREATE TABLE [dbo].[ztOrdersVsReportsPTL_Locations2] (
    [SSRSUserID]      VARCHAR (50) NULL,
    [ClientVisitGUID] BIGINT       NULL,
    [LocationGUID]    BIGINT       NULL,
    [StartDateTime]   DATETIME     NULL,
    [EndDateTime]     DATETIME     NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ztOrdersVsReportsPTL_Locations2_StartDateTime]
    ON [dbo].[ztOrdersVsReportsPTL_Locations2]([StartDateTime] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ztOrdersVsReportsPTL_Locations2_SSRSUserID]
    ON [dbo].[ztOrdersVsReportsPTL_Locations2]([SSRSUserID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ztOrdersVsReportsPTL_Locations2_LocationGUID]
    ON [dbo].[ztOrdersVsReportsPTL_Locations2]([LocationGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ztOrdersVsReportsPTL_Locations2_ClientVisitGUID]
    ON [dbo].[ztOrdersVsReportsPTL_Locations2]([ClientVisitGUID] ASC);

