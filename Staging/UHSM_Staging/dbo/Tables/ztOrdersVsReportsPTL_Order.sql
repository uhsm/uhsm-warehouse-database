﻿CREATE TABLE [dbo].[ztOrdersVsReportsPTL_Order] (
    [SSRSUserID]                 VARCHAR (50)  NOT NULL,
    [GUID]                       BIGINT        NOT NULL,
    [CareProviderGUID]           BIGINT        NULL,
    [ClientVisitGUID]            BIGINT        NULL,
    [Name]                       VARCHAR (125) NULL,
    [ObsCount]                   INT           NULL,
    [OrderCatalogMasterItemGUID] BIGINT        NULL,
    [RequestedDate]              DATE          NULL,
    [RequestedDateTime]          DATETIME      NULL,
    [UserGUID]                   BIGINT        NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ztOrdersVsReportsPTL_Order_UserGUID]
    ON [dbo].[ztOrdersVsReportsPTL_Order]([UserGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ztOrdersVsReportsPTL_Order_OrderCatalogMasterItemGUID]
    ON [dbo].[ztOrdersVsReportsPTL_Order]([OrderCatalogMasterItemGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ztOrdersVsReportsPTL_Order_ClientVisitID]
    ON [dbo].[ztOrdersVsReportsPTL_Order]([ClientVisitGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ztOrdersVsReportsPTL_Order_GUID]
    ON [dbo].[ztOrdersVsReportsPTL_Order]([GUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ztOrdersVsReportsPTL_Order_SSRSUserID]
    ON [dbo].[ztOrdersVsReportsPTL_Order]([SSRSUserID] ASC);

