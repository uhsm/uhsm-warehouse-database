﻿CREATE TABLE [dbo].[ztOrdersVsReportsPTL_Visit] (
    [SSRSUserID]          VARCHAR (20)  NOT NULL,
    [GUID]                BIGINT        NOT NULL,
    [CurrentLocation]     VARCHAR (50)  NULL,
    [CurrentLocationGUID] BIGINT        NULL,
    [DischargeDate]       DATE          NULL,
    [DischargeDateTime]   DATETIME      NULL,
    [PatientName]         VARCHAR (100) NULL,
    [RM2Number]           VARCHAR (20)  NULL,
    [ServiceGUID]         BIGINT        NULL,
    [TemporaryLocation]   VARCHAR (50)  NULL,
    [TypeCode]            VARCHAR (15)  NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ztOrdersVsReportsPTL_Visit_ServiceGUID]
    ON [dbo].[ztOrdersVsReportsPTL_Visit]([ServiceGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ztOrdersVsReportsPTL_Visit_GUID]
    ON [dbo].[ztOrdersVsReportsPTL_Visit]([GUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ztOrdersVsReportsPTL_Visit_SSRSUserID]
    ON [dbo].[ztOrdersVsReportsPTL_Visit]([SSRSUserID] ASC);

