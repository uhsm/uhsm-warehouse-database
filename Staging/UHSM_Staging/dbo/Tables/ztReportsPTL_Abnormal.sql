﻿CREATE TABLE [dbo].[ztReportsPTL_Abnormal] (
    [SSRSUserID]     VARCHAR (50) NULL,
    [RowNo]          INT          NULL,
    [OrderGUID]      BIGINT       NULL,
    [AbnormalityDef] VARCHAR (20) NULL,
	[AbnormalityGroup] [varchar](50) NULL,
	[ObsGUID] [bigint] NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ztReportsPTL_Abnormal_OrderGUID]
    ON [dbo].[ztReportsPTL_Abnormal]([OrderGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ztReportsPTL_Abnormal_SSRSUserID]
    ON [dbo].[ztReportsPTL_Abnormal]([SSRSUserID] ASC);

