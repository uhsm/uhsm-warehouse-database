﻿CREATE TABLE [dbo].[ztReportsPTL_FilingStatus] (
    [SSRSUserID]   VARCHAR (50) NULL,
    [RowNo]        INT          NULL,
    [ObjectGUID]   BIGINT       NULL,
    [FilingStatus] VARCHAR (30) NULL,
	[Reason] [varchar](255) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ztReportsPTL_FilingStatus_ObjectID]
    ON [dbo].[ztReportsPTL_FilingStatus]([ObjectGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ztReportsPTL_FilingStatus_SSRSUserID]
    ON [dbo].[ztReportsPTL_FilingStatus]([SSRSUserID] ASC);

