﻿CREATE TABLE [dbo].[ztReportsPTL_Locations1] (
    [SSRSUserID]         VARCHAR (50) NULL,
    [RowNo]              INT          NULL,
    [ClientVisitGUID]    BIGINT       NULL,
    [LocationGUID]       BIGINT       NULL,
    [TransferRequestDtm] DATETIME     NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ztReportsPTL_Locations1_ClientVisitGUID]
    ON [dbo].[ztReportsPTL_Locations1]([ClientVisitGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ztReportsPTL_Locations1_SSRSUserID]
    ON [dbo].[ztReportsPTL_Locations1]([SSRSUserID] ASC);

