﻿CREATE TABLE [dbo].[ztReportsPTL_Locations2] (
    [SSRSUserID]      VARCHAR (50) NULL,
    [ClientVisitGUID] BIGINT       NULL,
    [LocationGUID]    BIGINT       NULL,
    [StartDateTime]   DATETIME     NULL,
    [EndDateTime]     DATETIME     NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ztReportsPTL_Locations2_StartDateTime]
    ON [dbo].[ztReportsPTL_Locations2]([StartDateTime] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ztReportsPTL_Locations2_LocationGUID]
    ON [dbo].[ztReportsPTL_Locations2]([LocationGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ztReportsPTL_Locations2_ClientVisitGUID]
    ON [dbo].[ztReportsPTL_Locations2]([ClientVisitGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ztReportsPTL_Locations2_SSRSUserID]
    ON [dbo].[ztReportsPTL_Locations2]([SSRSUserID] ASC);

