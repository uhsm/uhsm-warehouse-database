﻿CREATE TABLE [dbo].[ztReportsPTL_Obs] (
    [SSRSUserID]      VARCHAR (50) NULL,
    [RowNo]           INT          NULL,
    [OrderGUID]       BIGINT       NULL,
    [EnteredDate]     DATE         NULL,
    [EnteredDateTime] DATETIME     NULL,
    [AbnormalityCode] VARCHAR (10) NULL,
	[ObsGUID] [bigint] NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ztReportsPTL_Obs_OrderGUID]
    ON [dbo].[ztReportsPTL_Obs]([OrderGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ztReportsPTL_Obs_SSRSUserID]
    ON [dbo].[ztReportsPTL_Obs]([SSRSUserID] ASC);

