﻿CREATE TABLE [dbo].[ztReportsPTL_ReqByStatDetail] (
    [SSRSUserID]      VARCHAR (9)  NOT NULL,
    [OrderGUID]       BIGINT       NULL,
    [RequestedByGUID] BIGINT       NULL,
    [RequestedByProv] VARCHAR (50) NULL
);

