﻿CREATE TABLE [dbo].[ztReportsPTL_Visit] (
    [ClientGUID]       BIGINT        NULL,
    [GUID]             BIGINT        NOT NULL,
    [Location]         VARCHAR (100) NULL,
    [LocationType]     VARCHAR (50)  NULL,
    [PatientName]      VARCHAR (100) NULL,
    [RM2Number]        VARCHAR (20)  NULL,
    [SubSpecialtyCode] VARCHAR (20)  NULL,
    [SSRSUserID]       VARCHAR (50)  NULL,
	[OrderGUID] [bigint] NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ztReportsPTL_Visit_GUID]
    ON [dbo].[ztReportsPTL_Visit]([GUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ztReportsPTL_Visit_SSRSUserID]
    ON [dbo].[ztReportsPTL_Visit]([SSRSUserID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ztReportsPTL_Visit_SubSpecialtyCode]
    ON [dbo].[ztReportsPTL_Visit]([SubSpecialtyCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ztReportsPTL_Visit_ClientGUID]
    ON [dbo].[ztReportsPTL_Visit]([ClientGUID] ASC);

