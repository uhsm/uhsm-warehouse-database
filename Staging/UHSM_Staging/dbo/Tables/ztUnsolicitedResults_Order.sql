﻿CREATE TABLE [dbo].[ztUnsolicitedResults_Order] (
    [SSRSUserID] VARCHAR (50) NOT NULL,
    [GUID]       BIGINT       NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ztUnsolicitedResults_Order_GUID]
    ON [dbo].[ztUnsolicitedResults_Order]([GUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ztUnsolicitedResults_Order_SSRSUserID]
    ON [dbo].[ztUnsolicitedResults_Order]([SSRSUserID] ASC);

