﻿

CREATE view [dbo].[vwLocationHierarchy] as

select 
step1.GUID as LocationGUID
,step1.Name
,LocationLevel1=
		case when step6.GUID is null then
		(case when step5.GUID is null then
		(case when step4.GUID is null then
		(case when step3.GUID is null then
		(case when step2.GUID is null then
		(case when step1.GUID is null then
		null
		else step1.Name end)
		else step2.Name end)
		else step3.Name end)
		else step4.Name end)
		else step5.Name end)
		else step6.Name end
,LocationLevel2=
		case when step6.GUID is null then
		(case when step5.GUID is null then
		(case when step4.GUID is null then
		(case when step3.GUID is null then
		(case when step2.GUID is null then
		null
		else step1.Name end)
		else step2.Name end)
		else step3.Name end)
		else step4.Name end)
		else step5.Name end
,LocationLevel3=
		case when step6.GUID is null then
		(case when step5.GUID is null then
		(case when step4.GUID is null then
		(case when step3.GUID is null then
		null
		else step1.Name end)
		else step2.Name end)
		else step3.Name end)
		else step4.Name end
,LocationLevel4=
		case when step6.GUID is null then
		(case when step5.GUID is null then
		(case when step4.GUID is null then
		null
		else step1.Name end)
		else step2.Name end)
		else step3.Name end
,LocationLevel5=
		case when step6.GUID is null then
		(case when step5.GUID is null then
		null
		else step1.Name end)
		else step2.Name end
,LocationLevel6=
		case when step6.GUID is null then
		null
		else step1.Name end
from [$(EPR)].dbo.CV3Location step1
left join [$(EPR)].dbo.CV3Location step2
on step2.GUID=step1.ParentGUID
left join [$(EPR)].dbo.CV3Location step3
on step3.GUID=step2.ParentGUID
left join [$(EPR)].dbo.CV3Location step4
on step4.GUID=step3.ParentGUID
left join [$(EPR)].dbo.CV3Location step5
on step5.GUID=step4.ParentGUID
left join [$(EPR)].dbo.CV3Location step6
on step6.GUID=step5.ParentGUID