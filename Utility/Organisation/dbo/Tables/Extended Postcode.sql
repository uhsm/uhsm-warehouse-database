﻿CREATE TABLE [dbo].[Extended Postcode] (
    [Postcode]                      NVARCHAR (8)  NOT NULL,
    [Local Authority]               NVARCHAR (50) NULL,
    [Health Authority of Residence] NVARCHAR (3)  NULL,
    [PCG of Residence]              NVARCHAR (5)  NULL,
    [LastUpdated]                   DATETIME      NULL,
    CONSTRAINT [PK_Extended Postcode] PRIMARY KEY CLUSTERED ([Postcode] ASC)
);

