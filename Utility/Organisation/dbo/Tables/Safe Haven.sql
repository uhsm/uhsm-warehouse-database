﻿CREATE TABLE [dbo].[Safe Haven] (
    [Organisation Code]  NVARCHAR (8)   NULL,
    [Organisation Name]  NVARCHAR (100) NULL,
    [Safe Haven Contact] NVARCHAR (40)  NULL,
    [Address Line 1]     NVARCHAR (35)  NULL,
    [Address Line 2]     NVARCHAR (35)  NULL,
    [Address Line 3]     NVARCHAR (35)  NULL,
    [Address Line 4]     NVARCHAR (35)  NULL,
    [Postcode]           NVARCHAR (8)   NULL,
    [Telephone Number]   NVARCHAR (40)  NULL,
    [Fax Number]         NVARCHAR (24)  NULL,
    [Safe Haven Comment] NVARCHAR (40)  NULL,
    [LastUpdated]        DATETIME       NULL
);

