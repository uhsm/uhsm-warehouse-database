﻿
CREATE view [dbo].[Postcode] as
SELECT
	 [Postcode]
	,[Local Authority] LocalAuthorityCode
	,[Health Authority of Residence] DistrictOfResidence
	,[PCG of Residence] PCTCode
	,REPLACE(Postcode, ' ','') SlimPostcode
FROM
	[dbo].[Extended Postcode]

