﻿EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'UHSM\svc-dw01';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\svc-dw01';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\cbellhartley';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\migray';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'svclpi';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\CBeckett';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\svc-dw02';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'svc-link';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\HSlim';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'login1';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\svc-sqlexec';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'svc-reporting';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'login2';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'login3';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM-DW-1\InformationUser';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\GFenton';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\DNorring';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\pasemota';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'UHSM\svc-dw01';

