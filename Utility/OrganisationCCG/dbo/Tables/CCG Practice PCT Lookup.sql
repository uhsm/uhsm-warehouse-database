﻿CREATE TABLE [dbo].[CCG Practice PCT Lookup] (
    [Practice] NVARCHAR (255) NULL,
    [CCG]      NVARCHAR (255) NULL,
    [CCG Name] NVARCHAR (255) NULL,
    [PCT]      NVARCHAR (255) NULL,
    [PCT Name] NVARCHAR (255) NULL
);

