﻿CREATE TABLE [dbo].[Local Authority] (
    [Local Authority Code] NVARCHAR (4)   NULL,
    [Local Authority Name] NVARCHAR (100) NULL,
    [LastUpdated]          DATETIME       NULL
);

