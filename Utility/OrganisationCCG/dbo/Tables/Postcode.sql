﻿CREATE TABLE [dbo].[Postcode] (
    [Postcode]            NVARCHAR (8)  NOT NULL,
    [LocalAuthorityCode]  NVARCHAR (50) NULL,
    [DistrictOfResidence] NVARCHAR (3)  NULL,
    [CCGCode]             NVARCHAR (5)  NULL,
    [SlimPostcode]        NVARCHAR (10) NULL
);


GO
CREATE NONCLUSTERED INDEX [ix_PostcodeTest_SlimPostcode]
    ON [dbo].[Postcode]([SlimPostcode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Postcode]
    ON [dbo].[Postcode]([Postcode] ASC)
    INCLUDE([CCGCode]);

