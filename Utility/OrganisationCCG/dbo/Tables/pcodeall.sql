﻿CREATE TABLE [dbo].[pcodeall] (
    [Postcode]                           VARCHAR (10) NULL,
    [HighLevelHealthGeography]           VARCHAR (5)  NULL,
    [PrimaryCareOrganisationOfResidence] VARCHAR (5)  NULL
);

