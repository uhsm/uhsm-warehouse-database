﻿
CREATE view [dbo].[LocalAuthority] as

select
	 [Local Authority Code] LocalAuthorityCode
	,[Local Authority Name] LocalAuthority
from
	[dbo].[Local Authority]

