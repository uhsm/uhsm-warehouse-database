﻿







/***********************************************************************************
 Description: Called by SSIS Package to create the CSV export file required
              for the A&E Friends and Family Test
 
 Modification History --------------------------------------------------------

 Date     Author      Change
 17/12/13 Tim Dean	  Created.
 04/02/14 Tim Dean    Added the opt-in flag.  Only send mobile and landline
                      number for patients that have agreed to receive communications.
 05/02/14 Tim Dean    Added to where clause to filter out null LocalPatientID.
 12/02/14 Tim Dean    Alter the usage of the opt-in flag.  Extract mobile and 
                      landline numbers for patients that have refused to receive
                      communications.
 23/06/15 Tim Dean    Changed criteria to a better method for excluding any deaths.
 30/06/15 Tim Dean    Exclude Obstetric conditions to cover miscarriages.
 12/08/15 Tim Dean    Added A&E Telephone and Disposal DF
 01/02/16 Tim Dean	  Added change to only use A&E obtained contact number for
                      children under the age of 16.
************************************************************************************/

CREATE Procedure [AE].[ExtractFriendsFamilyTest]
as

SELECT Rtrim(ae.LocalPatientID) patient_ref
	,Rtrim(ae.EncounterRecno) discharge_ref
	,CONVERT(CHAR(10), ae.AttendanceConclusionTime, 103) discharge_date
	,CONVERT(CHAR(5), ae.AttendanceConclusionTime, 108) discharge_time
	,'RM2|RM200|AE' location
	,180 lead_specialty_1
	,'' lead_specialty_2
	--,CASE {REMOVED TJD 12/08/2015}
	--	WHEN ae.FFTextStatus IN (
	--			'A'
	--			,'U'
	--			)
	--		THEN Rtrim(Isnull(ppn.MobilePhone, ''))
	--	ELSE ''
	--	END mobile_number
	,CASE -- {ADDED TJD 12/08/2015}
		--WHEN ae.FFTextStatus IN ( {REMOVED TJD 01/02/2015}
		--		'A'
		--		,'U'
		--		) 
		--    THEN RTRIM(ISNULL(COALESCE(ae.Telephone,ppn.MobilePhone),''))
	-- {ADDED TJD 01/02/2016 -----------------------------------}		
	WHEN ae.FFTextStatus IN ( 
				'A'
				,'U'
				) AND ae.AgeOnArrival >= 16
		    THEN RTRIM(ISNULL(COALESCE(ae.Telephone,ppn.MobilePhone),''))	
	WHEN ae.FFTextStatus IN ( 
				'A'
				,'U'
				) AND ae.AgeOnArrival < 16
		    THEN RTRIM(ISNULL(ae.Telephone,''))
		  -- {END ADDED TJD 01/02/2016	-------------------------------}		    	
		ELSE ''
		END mobile_number
	,CASE 
	    -- {REMOVED TJD 01/02/2016}
		--WHEN ae.FFTextStatus IN (
		--		'A'
		--		,'U'
		--		)
		--	THEN Rtrim(Isnull(ppn.HomePhone, ''))

		--{ADDED TJD 01/02/2016}
		WHEN ae.FFTextStatus IN (
				'A'
				,'U'
				) AND ae.AgeOnArrival >= 16
			THEN Rtrim(Isnull(ppn.HomePhone, ''))			
		ELSE ''
		END landline_number
	,'' email_address
	,Rtrim(Isnull(ae.PatientTitle, '')) title
	,Rtrim(ae.PatientForename) first_name
	,Rtrim(ae.PatientSurname) last_name
	,'' address_1
	,'' address_2
	,'' address_3
	,'' address_4
	,'' address_5
	,'' postcode
	,'' country
FROM AE.Encounter ae
LEFT JOIN Lorenzo.dbo.PatientIdentifier pid ON (ae.LocalPatientID = pid.IDENTIFIER)
LEFT JOIN PAS.PatientPhoneNumber ppn ON (pid.PATNT_REFNO = ppn.SourcePatientNo)
WHERE Cast(ae.AttendanceConclusionTime AS DATE) = Cast(dateadd(DAY, - 1, Getdate()) AS DATE) -- yesterday
	AND ae.AttendanceCategoryCode IN (
		'1'
		,'2'
		)
	----AND ae.DateOfDeath IS NULL -- exclude patients that have died {REMOVED TJD 23/06/15}
	--{REMOVED TJD 12/08/2015}
	--AND ae.AttendanceDisposalCode = 'DA' --{ADDED TJD 23/06/15} only include discharged after treatment.  This
	--                                     -- excludes admitted and deaths.
	AND ae.AttendanceDisposalCode IN ('DA','DF') -- {ADDED TJD 12/08/2015}
	--AND ae.AttendanceDisposalCode <> 'AD' -- not admitted {REMOVED TJD 23/06/15}
	AND ae.LocalPatientID IS NOT NULL
	AND (ae.AttendanceDisposalCode <> '28'  --{ADDED TJD 30/06/2015} Obstetric conditions
	    OR ae.AttendanceDisposalCode IS NULL); 

      




