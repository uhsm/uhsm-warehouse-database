﻿

CREATE procedure [AE].[LoadSLSEncounter] as 

/*
--Author: K Oakden
--Date created: 22/10/2013
--SP to push Cascade data to v-uhsm-salford for Salford Lung Study.
*/

declare @sql nvarchar(100)
declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()
select @sql = 'truncate table dbo.AEVisits'

--Truncate existing data
exec [V-UHSM-SALFORD].Patients.dbo.sp_executesql @sql

--Load new data
insert into [V-UHSM-SALFORD].[Patients].[dbo].[AEVisits]

select 
	SourceUniqueID
	,DistrictNo
	,NHSNumber
	,PatientTitle
	,PatientForename
	,PatientSurname
	,Postcode
	,DateOfBirth
	,SexCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,CCGCode
	,AttendanceNumber
	,ArrivalModeCode
	,AttendanceCategoryCode
	,AttendanceDisposalCode
	,ArrivalDate
	,ArrivalTime
	,AgeOnArrival
	,TriageCategoryCode
	,ReferredToSpecialtyCode
	,InitialAssessmentTime
	,SeenForTreatmentTime
	,AttendanceConclusionTime
	,ToSpecialtyTime
	,DecisionToAdmitTime
	,RegisteredTime
	,WhereSeen = 
		case WhereSeen
			when 1 then 'Main Dept'
			when 2 then 'UCC Minor'
			when 3 then 'UCC'
			when 4 then 'GPOOH'
			else null
		end
	,EncounterDurationMinutes
	,BreachReason
	,AdmitToWard
	,GETDATE()
from AE.CurrentEncounter
where ArrivalDate > dateadd(MONTH, -6, getdate())
and NHSNumber IN (SELECT [NHS_NUMBER] COLLATE Latin1_General_CI_AS 
  FROM [V-UHSM-SALFORD].[Patients].[dbo].[Patients])
select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(SECOND, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Secs'

exec WriteAuditLogEvent 'AE - WH LoadSLSEncounter', @Stats, @StartTime
--print @Stats 
--select * from [V-UHSM-SALFORD].[Patients].[dbo].[AEVisits]