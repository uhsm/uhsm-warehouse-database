﻿


/*
--Author: J Cleworth
--Date created: 22/06/2013
--AE Alcohol Monitoring Report
--Amended by Graham Ryder on 10/06/2014 to change ArrivalModeCode to SourceOfReferralCode due to
--change in population of the ArrivalModeCode field so that it no longer matches SourceOfReferralCode
*/

CREATE Procedure [AE].[ReportAlcoholMonitoring]
as

select
distinct
case when ae.ageonarrival<16 then 'Other'
when ae.AgeOnArrival <18 then '16 and 17 years'
when ae.AgeOnArrival <26 then '18-25 years'
when ae.AgeOnArrival <31 then '26-30 years'
when ae.AgeOnArrival <41 then '31-40 years'
when ae.AgeOnArrival <51 then '41-50 years'
when ae.AgeOnArrival <61 then '51-60 years'
when ae.AgeOnArrival <71 then '61-70 years'
when ae.AgeOnArrival <81 then '71-80 years'
else '80 years or over' end as AgeGroup
,
C.financialyear,
c.TheMonth,
c.FinancialMonthKey
,

      sum(case when [AlcoholAuditParticipation]='Impaired' then 1 else 0 end) as Impairments
      ,sum(Case when alcoholauditparticipation is not null then 1 else 0 end)-sum(case when [AlcoholAuditParticipation]='Impaired' then 1 else 0 end) as Screened
      ,Sum(Case when alcoholauditparticipation IN ('Agreed') then 1 else 0 end) as Agreed
      ,Sum(Case when alcoholauditparticipation IN ('Refused') then 1 else 0 end) as Refused
        ,Sum(Case when alcoholauditlocation is not null and alcoholauditparticipation in ('Agreed') then 1 else 0 end) as AskedInLast12Hours
        ,Sum(Case when alcoholauditlocation is not null and AlcoholAuditLocation<>'Left Blank' and AlcoholAuditLocation<>'Not in last 12 hours' and alcoholauditparticipation in ('Agreed') then 1 else 0 end) as AskedInLast12HoursYes
         ,Sum(Case when alcoholauditlocation is not null and AlcoholAuditLocation='Not in last 12 hours' and alcoholauditparticipation in ('Agreed') then 1 else 0 end) as AskedInLast12HoursNo
        ,Sum(Case when alcoholauditlocation ='Left Blank' and alcoholauditparticipation IN ('Agreed') then 1 else 0 end) as AskedInLast12HoursRefused
        ,Sum(Case when AlcoholAuditFrequency is not null then 1 else 0 end) as ScreenedAuditCYes
        ,Sum(Case when AlcoholAuditFrequency+AlcoholAuditTypicalDay+AlcoholAuditSingleOccasion>=5 then 1 else 0 end) as Score5orAbove
         ,Sum(Case when AlcoholAuditFrequency+AlcoholAuditTypicalDay+AlcoholAuditSingleOccasion>=5 and AdviceTime is not null then 1 else 0 end) as Score5orAboveAdvice
     ,count(SourceUniqueID) as EligiblePatients
     ,ms.Screened as MonthTotalScreened
,ec.EthnicCategoryDescription
,case when ae.EthnicCategoryCode in (17,18,19) then 'White'
when ae.EthnicCategoryCode in (9,10,11,12) then 'Mixed'
when ae.EthnicCategoryCode in (2,3,4,5) then 'Asian or Asian British'
when ae.EthnicCategoryCode in (6,7,8) then 'Black or Black British'
when ae.EthnicCategoryCode in (1,16) then 'Chinese or other ethnic group'
when ae.EthnicCategoryCode=15 then 'Not Stated'
else 'Not Specified' end as EthnicCategoryGroup
,case when ae.EthnicCategoryCode in (17,18,19) then 1
when ae.EthnicCategoryCode in (9,10,11,12) then 2
when ae.EthnicCategoryCode in (2,3,4,5) then 3
when ae.EthnicCategoryCode in (6,7,8) then 4
when ae.EthnicCategoryCode in (1,16) then 5
when ae.EthnicCategoryCode=15 then 6
else 7 end as EthnicCategoryGroupOrder
,ae.EthnicCategoryCode
,ec.EthnicCategoryNHSCode
,SexCode
,AgeOnArrival
  FROM [AE].[Encounter] ae
  inner join whreporting.lk.calendar c on c.TheDate=ae.ArrivalDate
  left join [AE].[EthnicCategoryLookup]ec on ec.EthnicCategoryCode=ae.EthnicCategoryCode
  left join (
  
  
  
  select c2.FinancialMonthKey, sum(Case when alcoholauditparticipation is not null then 1 else 0 end)-sum(case when [AlcoholAuditParticipation]='Impaired' then 1 else 0 end) as Screened
  from [AE].[Encounter] ae2 
    inner join WHREPORTING.LK.Calendar c2 on c2.TheDate=cast(ae2.ArrivalDate as date)
  where
   AttendanceCategoryCode in ('1','2') --Only New Pts (Excludes Review Pts)
AND AttendanceDisposalCode not in ('DI','X') --Exclude Pts who died or Pts who Did Not Wait
AND (AgeOnArrival >=16 OR AgeOnArrival is null)--Excludes Pts under 16 
AND WhereSeen not in ('4','3')--Exclude Pts seen in GPOOH & Deflections
AND TriageCategoryCode not in ('R','O')--Excludes Pts with Red & Orange Triage
--AND ArrivalModeCode not in (' 3','19')-- Exclude Bed Bureau Attends (& Bed Bur RR)
AND SourceOfReferralCode not in ('3','19')-- Exclude Bed Bureau Attends (& Bed Bur RR)
  

  group by FinancialMonthKey
  
  
  
  
  ) as ms on ms.FinancialMonthKey=c.FinancialMonthKey
  where ArrivalDate > '2012-03-31'
  AND c.FinancialMonthKey<(SELECT FinancialMonthKey FROM WHREPORTING.LK.Calendar where TheDate=CAST(getdate() as DATE))
  and FinancialYear=(select distinct financialyear from whreporting.lk.calendar where thedate=cast(getdate() as date))
 -- and alcoholauditparticipation in('Agreed')
  

AND AttendanceCategoryCode in ('1','2') --Only New Pts (Excludes Review Pts)
AND AttendanceDisposalCode not in ('DI','X') --Exclude Pts who died or Pts who Did Not Wait
AND (AgeOnArrival >=16 OR AgeOnArrival is null)--Excludes Pts under 16 
AND WhereSeen not in ('4','3')--Exclude Pts seen in GPOOH & Deflections
AND TriageCategoryCode not in ('R','O')--Excludes Pts with Red & Orange Triage
--AND ArrivalModeCode not in (' 3','19')-- Exclude Bed Bureau Attends (& Bed Bur RR)
AND SourceOfReferralCode not in ('3','19')-- Exclude Bed Bureau Attends (& Bed Bur RR)

group by
 case when ae.ageonarrival<16 then 'Other'
when ae.AgeOnArrival <18 then '16 and 17 years'
when ae.AgeOnArrival <26 then '18-25 years'
when ae.AgeOnArrival <31 then '26-30 years'
when ae.AgeOnArrival <41 then '31-40 years'
when ae.AgeOnArrival <51 then '41-50 years'
when ae.AgeOnArrival <61 then '51-60 years'
when ae.AgeOnArrival <71 then '61-70 years'
when ae.AgeOnArrival <81 then '71-80 years'
else '80 years or over' end,
C.financialyear,c.TheMonth,
c.FinancialMonthKey
,ec.EthnicCategoryDescription
,ae.EthnicCategoryCode
,ec.EthnicCategoryNHSCode
,SexCode
,AgeOnArrival
,ms.Screened
order by c.FinancialMonthKey


