﻿
/*
--Author: J Cleworth
--Date created: 20/05/2013
--AE Alcohol Report
--Amended by Graham Ryder on 10/06/2014 to change ArrivalModeCode to SourceOfReferralCode due to
--change in population of the ArrivalModeCode field so that it no longer matches SourceOfReferralCode
*/

CREATE Procedure [AE].[ReportAlcoholScreenRate]



@Month as int

as

select 
Consultant,
ScreenRate,
row_number() over(order by screenrate desc) as Position
from



(Select 
Consultant,
Screened/(EligiblePatients*1.00) as ScreenRate

from

(select
distinct
Consultant,
c.financialyear,
c.TheMonth,
c.FinancialMonthKey
,

      sum(case when [AlcoholAuditParticipation]='Impaired' then 1 else 0 end) as Impairments
      ,sum(Case when alcoholauditparticipation is not null then 1 else 0 end) as Screened
      ,Sum(Case when alcoholauditparticipation IN ('Agreed') then 1 else 0 end) as Agreed
      ,Sum(Case when alcoholauditparticipation IN ('Refused') then 1 else 0 end) as Refused
     ,count(SourceUniqueID) as EligiblePatients

  FROM [AE].[Encounter] ae
  inner join whreporting.lk.calendar c on c.TheDate=ae.ArrivalDate
  where ArrivalDate > '2012-03-31'
  and c.FinancialMonthKey in (@Month)
  AND FinancialMonthKey<(SELECT FinancialMonthKey FROM WHREPORTING.LK.Calendar where TheDate=CAST(getdate() as DATE))
  
 -- and alcoholauditparticipation in('Agreed')
  

AND AttendanceCategoryCode in ('1','2') --Only New Pts (Excludes Review Pts)
AND AttendanceDisposalCode not in ('DI','X') --Exclude Pts who died or Pts who Did Not Wait
AND (AgeOnArrival >=16 OR AgeOnArrival is null)--Excludes Pts under 16 
AND WhereSeen not in ('4','3')--Exclude Pts seen in GPOOH & Deflections
AND TriageCategoryCode not in ('R','O')--Excludes Pts with Red & Orange Triage
--AND ArrivalModeCode not in (' 3','19')-- Exclude Bed Bureau Attends (& Bed Bur RR)
AND SourceOfReferralCode not in ('3','19')-- Exclude Bed Bureau Attends (& Bed Bur RR)
group by Consultant,C.financialyear,c.TheMonth,
c.FinancialMonthKey

) as t) as u

order by ScreenRate desc
