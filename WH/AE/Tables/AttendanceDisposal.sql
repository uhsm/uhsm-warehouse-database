﻿CREATE TABLE [AE].[AttendanceDisposal] (
    [AttendanceDisposalCode] VARCHAR (50)  NOT NULL,
    [AttendanceDisposal]     VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_AttendanceDisposal] PRIMARY KEY CLUSTERED ([AttendanceDisposalCode] ASC)
);

