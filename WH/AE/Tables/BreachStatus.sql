﻿CREATE TABLE [AE].[BreachStatus] (
    [BreachStatusCode] VARCHAR (10) NOT NULL,
    [BreachStatus]     VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_BreachStatus] PRIMARY KEY CLUSTERED ([BreachStatusCode] ASC)
);

