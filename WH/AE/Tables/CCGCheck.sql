﻿CREATE TABLE [AE].[CCGCheck] (
    [LocalPatientID]      VARCHAR (12) NULL,
    [SourceUniqueID]      VARCHAR (11) NULL,
    [ArrivalDate]         DATETIME     NULL,
    [CommissionerCodeCCG] NVARCHAR (7) NULL,
    [SourceOfCCG]         VARCHAR (7)  NULL,
    [CascadePracticeCode] VARCHAR (6)  NULL,
    [CascadeCCGCode]      NVARCHAR (6) NULL,
    [PASPracticeCode]     VARCHAR (20) NULL,
    [PASCCG]              NVARCHAR (6) NULL
);

