﻿CREATE TABLE [AE].[CodingUser] (
    [SourceUniqueID]   VARCHAR (11) NULL,
    [ModifiedDateTime] DATETIME     NULL,
    [ModifiedByUser]   VARCHAR (5)  NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_CodingUser_SourceUniqueID]
    ON [AE].[CodingUser]([SourceUniqueID] ASC);

