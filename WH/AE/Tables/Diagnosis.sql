﻿CREATE TABLE [AE].[Diagnosis] (
    [DiagnosisRecno]        INT          IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]        VARCHAR (50) NULL,
    [SequenceNo]            SMALLINT     NOT NULL,
    [DiagnosticSchemeInUse] VARCHAR (10) NULL,
    [DiagnosisCode]         VARCHAR (50) NULL,
    [AESourceUniqueID]      VARCHAR (50) NOT NULL,
    [SourceDiagnosisCode]   VARCHAR (50) NULL,
    [SourceSequenceNo]      SMALLINT     NULL,
    [DiagnosisSiteCode]     VARCHAR (10) NULL,
    [DiagnosisSideCode]     VARCHAR (10) NULL,
    [CascadeDiagnosisCode]  VARCHAR (2)  NULL,
    CONSTRAINT [PK_Diagnosis] PRIMARY KEY CLUSTERED ([DiagnosisRecno] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'KO For updated AE load.', @level0type = N'SCHEMA', @level0name = N'AE', @level1type = N'TABLE', @level1name = N'Diagnosis', @level2type = N'COLUMN', @level2name = N'DiagnosisRecno';

