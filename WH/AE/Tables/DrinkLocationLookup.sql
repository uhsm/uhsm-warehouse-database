﻿CREATE TABLE [AE].[DrinkLocationLookup] (
    [LocationCode]        VARCHAR (2)  NOT NULL,
    [LocationDescription] VARCHAR (20) NULL,
    CONSTRAINT [PK_DrinkLocationLookup] PRIMARY KEY CLUSTERED ([LocationCode] ASC)
);

