﻿CREATE TABLE [AE].[DurationBand] (
    [DurationBandCode] VARCHAR (10) NOT NULL,
    [DurationBand]     VARCHAR (50) NULL,
    CONSTRAINT [PK_DurationBand] PRIMARY KEY CLUSTERED ([DurationBandCode] ASC)
);

