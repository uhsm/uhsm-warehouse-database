﻿CREATE TABLE [AE].[EncounterUnvalidatedPerformance] (
    [SourceUniqueID]           VARCHAR (50)  NOT NULL,
    [ArrivalDate]              SMALLDATETIME NOT NULL,
    [EncounterDurationMinutes] INT           NULL,
    [BreachReason]             VARCHAR (50)  NULL,
    CONSTRAINT [PK_EncounterUnvalidatedPerformance] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);

