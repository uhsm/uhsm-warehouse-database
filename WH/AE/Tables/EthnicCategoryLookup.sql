﻿CREATE TABLE [AE].[EthnicCategoryLookup] (
    [EthnicCategoryCode]        CHAR (2)     NOT NULL,
    [EthnicCategoryDescription] VARCHAR (50) NULL,
    [EthnicCategoryPASCode]     VARCHAR (5)  NULL,
    [EthnicCategoryNHSCode]     VARCHAR (5)  NULL,
    CONSTRAINT [PK_EthnicCategoryLookup] PRIMARY KEY CLUSTERED ([EthnicCategoryCode] ASC)
);

