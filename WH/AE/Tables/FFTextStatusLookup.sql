﻿CREATE TABLE [AE].[FFTextStatusLookup] (
    [FFTextStatusCode]        VARCHAR (1)  NOT NULL,
    [FFTextStatusDescription] VARCHAR (40) NULL,
    CONSTRAINT [PK_FFTextStatusLookup] PRIMARY KEY CLUSTERED ([FFTextStatusCode] ASC)
);

