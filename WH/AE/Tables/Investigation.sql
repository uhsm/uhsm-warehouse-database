﻿CREATE TABLE [AE].[Investigation] (
    [InvestigationRecno]       INT           IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]           VARCHAR (50)  NULL,
    [InvestigationDate]        SMALLDATETIME NULL,
    [SequenceNo]               TINYINT       NULL,
    [InvestigationSchemeInUse] VARCHAR (10)  NULL,
    [InvestigationCode]        VARCHAR (50)  NULL,
    [AESourceUniqueID]         VARCHAR (50)  NULL,
    [SourceInvestigationCode]  VARCHAR (50)  NULL,
    [SourceSequenceNo]         TINYINT       NULL,
    [ResultDate]               SMALLDATETIME NULL,
    CONSTRAINT [PK_Investigation] PRIMARY KEY CLUSTERED ([InvestigationRecno] ASC)
);

