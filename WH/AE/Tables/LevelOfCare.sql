﻿CREATE TABLE [AE].[LevelOfCare] (
    [LevelOfCareCode]  VARCHAR (10) NOT NULL,
    [LevelOfCare]      VARCHAR (6)  NOT NULL,
    [LevelOfCareGroup] VARCHAR (50) NULL,
    CONSTRAINT [PK_LevelOfCare] PRIMARY KEY CLUSTERED ([LevelOfCareCode] ASC)
);

