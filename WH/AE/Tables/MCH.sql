﻿CREATE TABLE [AE].[MCH] (
    [SourceUniqueID] VARCHAR (50)  NOT NULL,
    [Period]         SMALLDATETIME NULL,
    [Days]           FLOAT (53)    NULL,
    [FirstAttends]   INT           NULL,
    [ReviewAttends]  INT           NULL,
    [Attends]        INT           NULL,
    [OtherAttends]   INT           NULL,
    [SiteCode]       VARCHAR (50)  NULL,
    [Created]        DATETIME      NOT NULL,
    [Updated]        DATETIME      NULL,
    [ByWhom]         VARCHAR (50)  NULL,
    CONSTRAINT [PK_MCH] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);

