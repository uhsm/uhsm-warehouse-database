﻿CREATE TABLE [AE].[ManchesterTriageLookup] (
    [ManTriageCode]        VARCHAR (3)  NOT NULL,
    [ManTriageDescription] VARCHAR (40) NULL,
    CONSTRAINT [PK_ManchesterTriageLookup] PRIMARY KEY CLUSTERED ([ManTriageCode] ASC)
);

