﻿CREATE TABLE [AE].[Procedure] (
    [ProcedureRecno]       INT           IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]       VARCHAR (50)  NULL,
    [ProcedureDate]        SMALLDATETIME NULL,
    [SequenceNo]           TINYINT       NULL,
    [ProcedureSchemeInUse] VARCHAR (10)  NULL,
    [ProcedureCode]        VARCHAR (50)  NULL,
    [AESourceUniqueID]     VARCHAR (50)  NULL,
    [SourceProcedureCode]  VARCHAR (50)  NULL,
    [SourceSequenceNo]     TINYINT       NULL,
    CONSTRAINT [PK_Procedure] PRIMARY KEY CLUSTERED ([ProcedureRecno] ASC)
);

