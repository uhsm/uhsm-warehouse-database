﻿CREATE TABLE [AE].[RowCounts] (
    [TableName]    [sysname] NOT NULL,
    [ColumnName]   [sysname] NOT NULL,
    [NullCount]    BIGINT    NULL,
    [NotNullCount] BIGINT    NULL
);

