﻿CREATE TABLE [AE].[SourceOfReferralLookup] (
    [SourceOfReferralCode] VARCHAR (2)  NOT NULL,
    [SourceOfReferral]     VARCHAR (10) NULL,
    [ArrivalModeCode]      VARCHAR (2)  NULL,
    [ArrivalMode]          VARCHAR (50) NULL,
    CONSTRAINT [PK_SourceOfReferralLookup] PRIMARY KEY CLUSTERED ([SourceOfReferralCode] ASC)
);

