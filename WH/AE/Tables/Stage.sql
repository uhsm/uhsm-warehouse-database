﻿CREATE TABLE [AE].[Stage] (
    [StageCode] VARCHAR (50)  NOT NULL,
    [Stage]     VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_Stage] PRIMARY KEY CLUSTERED ([StageCode] ASC)
);

