﻿CREATE TABLE [AE].[StageBreach_] (
    [StageCode]   VARCHAR (50) NOT NULL,
    [BreachValue] INT          NULL,
    CONSTRAINT [PK_StageBreach] PRIMARY KEY CLUSTERED ([StageCode] ASC),
    CONSTRAINT [FK_StageBreach_Stage] FOREIGN KEY ([StageCode]) REFERENCES [AE].[Stage] ([StageCode])
);

