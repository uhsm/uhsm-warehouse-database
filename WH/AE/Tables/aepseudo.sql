﻿CREATE TABLE [AE].[aepseudo] (
    [PseudoID]                 NUMERIC (15)  NULL,
    [AEps]                     VARCHAR (20)  NULL,
    [APCps]                    VARCHAR (20)  NULL,
    [ArrivalTime]              SMALLDATETIME NULL,
    [NHStrust]                 VARCHAR (4)   NOT NULL,
    [NHStrustsite]             VARCHAR (5)   NOT NULL,
    [DepartmentType]           VARCHAR (2)   NOT NULL,
    [AttendanceCategoryCode]   VARCHAR (10)  NULL,
    [AgeOnArrival]             TINYINT       NULL,
    [GPPractice]               VARCHAR (10)  NULL,
    [ArrivalModeCode]          VARCHAR (10)  NULL,
    [Stream]                   VARCHAR (5)   NOT NULL,
    [TriageCategoryCode]       VARCHAR (10)  NULL,
    [PatientGroup]             VARCHAR (4)   NOT NULL,
    [SourceOfReferralCode]     VARCHAR (10)  NULL,
    [InitialAssessmentTime]    SMALLDATETIME NULL,
    [SeenForTreatmentTime]     SMALLDATETIME NULL,
    [AttendanceConclusionTime] SMALLDATETIME NULL,
    [AttendanceDisposalCode]   VARCHAR (10)  NULL,
    [DepartureTime]            SMALLDATETIME NULL
);

