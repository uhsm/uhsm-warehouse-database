﻿


CREATE view [AE].[AmbulanceCallCategoryList] as

SELECT DISTINCT [Call Category] = CALL_CATEGORY_GOV_STANDARD
FROM AE.AmbulanceDailyMDSExtract;

