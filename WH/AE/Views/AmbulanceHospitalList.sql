﻿



CREATE view [AE].[AmbulanceHospitalList] as

SELECT DISTINCT [Hospital Name] = HOSPITAL_NAME
	,[Clinic Attended] = CLINIC_ATTENDED
	,[NWAS Area] = HOSPITAL_NWAS_AREA
FROM AE.AmbulanceDailyMDSExtract;



