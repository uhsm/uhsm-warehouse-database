﻿







CREATE view [AE].[AmbulanceLateTurnaroundReasonList] as

SELECT DISTINCT [Late Turnaround Reason] = CASE 
		WHEN LEN(LATE_TURNAROUND_REASON) = 0
			THEN 'Not Applicable'
		ELSE LATE_TURNAROUND_REASON
		END
FROM AE.AmbulanceDailyMDSExtract;



