﻿


CREATE view [AE].[AttendanceCategory] as

select
	 AttendanceCategoryCode = [code]
	,AttendanceCategory = [label]

	,AttendanceCategoryGroup =
		case
		when [code] in ('1', '2')
		then 'Type 1'
		else 'Other'
		end

from
	[CASCADE].[AEAtType]

union all

select
	 AttendanceCategoryCode = 'WA'
	,AttendanceCategory = 'Ward Attender'
	,AttendanceCategoryGroup = 'Type 1'

union all

select
	 AttendanceCategoryCode = 'W'
	,AttendanceCategory = 'Walk-In'
	,AttendanceCategoryGroup = 'Type 3'



