﻿







CREATE view [AE].[CurrentEncounter] as
  SELECT [SourceUniqueID]
      ,[DistrictNo]
      ,[NHSNumber]
      ,[PatientTitle]
      ,[PatientForename]
      ,[PatientSurname]
      ,[Postcode]
      ,[DateOfBirth]
      ,[SexCode]
      ,[RegisteredGpCode]
      ,[RegisteredGpPracticeCode]
      ,PCTCode = [PracticePCTCode] 
      ,CCGCode = null
      ,[AttendanceNumber]
      ,[SourceOfReferralCode]
      ,[ArrivalModeCode]
      ,[AttendanceCategoryCode]
      ,[AttendanceDisposalCode]
      ,[ArrivalDate]
      ,[ArrivalTime]
      ,[AgeOnArrival]
      ,[TriageCategoryCode]
      ,[ReferredToSpecialtyCode]
      ,[InitialAssessmentTime]
      ,[SeenForTreatmentTime]
      ,[SeenBySpecialtyTime]
      ,[AttendanceConclusionTime]
      ,[ToSpecialtyTime]
      ,[DecisionToAdmitTime]
      ,[RegisteredTime]
      ,[WhereSeen]
      ,[EncounterDurationMinutes]
      ,[STATInterventionDone]
      ,[STATInterventionTime]
      ,[BreachReason]
      ,[AdmitToWard]
  FROM [AE].CurrentData
  union
  SELECT [SourceUniqueID]
      ,[DistrictNo]
      ,[NHSNumber]
      ,[PatientTitle]
      ,[PatientForename]
      ,[PatientSurname]
      ,[Postcode]
      ,[DateOfBirth]
      ,[SexCode]
      ,[RegisteredGpCode]
      ,[RegisteredGpPracticeCode]
      ,[PCTCode]
      ,[CCGCode]
      ,[AttendanceNumber]
      ,[SourceOfReferralCode]
      ,[ArrivalModeCode]
      ,[AttendanceCategoryCode]
      ,[AttendanceDisposalCode]
      ,[ArrivalDate]
      ,[ArrivalTime]
      ,[AgeOnArrival]
      ,[TriageCategoryCode]
      ,[ReferredToSpecialtyCode]
      ,[InitialAssessmentTime]
      ,[SeenForTreatmentTime]
      ,[SeenBySpecialtyTime]
      ,[AttendanceConclusionTime]
      ,[ToSpecialtyTime]
      ,[DecisionToAdmitTime]
      ,[RegisteredTime]
      ,[WhereSeen]
      ,[EncounterDurationMinutes]
      ,[STATInterventionDone]
      ,[STATInterventionTime]
      ,[BreachReason]
      ,[AdmitToWard]
  FROM [AE].[Encounter] where ArrivalDate > '20110331'
    and SourceUniqueID not in (select SourceUniqueID from [AE].CurrentData)








