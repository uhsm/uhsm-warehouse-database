﻿--/****** Object:  View [AE].[Practice]    Script Date: 05/13/2010 16:35:19 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

CREATE view [AE].[Practice] as

select
	 PracticeCode = [prcode]
	,Practice = [prname]
	,NationalPracticeCode = 
		case
		when [pracncode] = ''
		then null
		else [pracncode]
		end

	,PracticePCT.PCTCode
	,PracticePCT.PCT
from
	[CASCADE].AEGpPrac

left join 
	(
	select
		 PracticeCode = PASPractice.OrganisationLocalCode
		,PCTCode = PASPCT.OrganisationLocalCode
		,PCT = PASPCT.Organisation
	from
		[PAS].[Organisation] PASPractice

	inner join [PAS].[Organisation] PASPCT
	on	PASPCT.OrganisationCode = PASPractice.ParentOrganisationCode
	and	PASPCT.OrganisationTypeCode = 629 --pct

	where
		PASPractice.OrganisationTypeCode = 626 --practice
	) PracticePCT
on	PracticePCT.PracticeCode = [pracncode]



