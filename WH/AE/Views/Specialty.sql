﻿
CREATE view [AE].[Specialty] as

select
	 SpecialtyCode = [code]
	,Specialty = [label]

	,IsMentalHealth =
		CONVERT(
			bit
			,case
--			when division = 'MEN'
			when code = 'MENT'
			then 1
			else 0
			end
		)

	,DivisionCode = 
		case
		when [division] = ''
		then null
		else [division]
		end
from
	[CASCADE].[AESpecs]


