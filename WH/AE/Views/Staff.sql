﻿create view AE.Staff as
select 
	Code = staffcode
	,Forename = staffforen
	,Surname = staffsurn 
	,Title = stafftitle
	,IsInactive = stinactive
from [CASCADE].AEStaff