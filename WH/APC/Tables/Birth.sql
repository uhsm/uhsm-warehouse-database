﻿CREATE TABLE [APC].[Birth] (
    [SourceUniqueID]                       INT           NOT NULL,
    [MaternitySpellSourceUniqueID]         INT           NULL,
    [SourcePatientNo]                      INT           NULL,
    [ReferralSourceUniqueID]               INT           NULL,
    [ProfessionalCarerCode]                INT           NULL,
    [MotherSourcePatientNo]                INT           NULL,
    [BirthOrder]                           NUMERIC (18)  NULL,
    [DateOfBirth]                          SMALLDATETIME NULL,
    [SexCode]                              INT           NULL,
    [StatusOfPersonConductingDeliveryCode] INT           NULL,
    [LiveOrStillBirthCode]                 INT           NULL,
    [DeliveryPlaceCode]                    INT           NULL,
    [ResuscitationMethodByPressureCode]    INT           NULL,
    [ResuscitationMethodByDrugCode]        INT           NULL,
    [BirthWeight]                          NUMERIC (18)  NULL,
    [ApgarScoreAt1Minute]                  NUMERIC (18)  NULL,
    [ApgarScoreAt5Minutes]                 NUMERIC (18)  NULL,
    [BCGAdministeredCode]                  INT           NULL,
    [CircumferenceOfBabysHead]             NUMERIC (18)  NULL,
    [LengthOfBaby]                         NUMERIC (18)  NULL,
    [ExaminationOfHipsCode]                INT           NULL,
    [FollowUpCareCode]                     INT           NULL,
    [FoetalPresentationCode]               INT           NULL,
    [MetabolicScreeningCode]               INT           NULL,
    [JaundiceCode]                         INT           NULL,
    [FeedingTypeCode]                      INT           NULL,
    [DeliveryMethodCode]                   INT           NULL,
    [MaternityDrugsCode]                   INT           NULL,
    [GestationLength]                      NUMERIC (18)  NULL,
    [ApgarScoreAt10Minutes]                INT           NULL,
    [NeonatalLevelOfCareCode]              INT           NULL,
    [PASCreated]                           DATETIME      NULL,
    [PASUpdated]                           DATETIME      NULL,
    [PASCreatedByWhom]                     VARCHAR (30)  NULL,
    [PASUpdatedByWhom]                     VARCHAR (30)  NULL,
    [Created]                              DATETIME      NOT NULL,
    [Updated]                              DATETIME      NULL,
    [ByWhom]                               VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Birth] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX1_APC_Birth]
    ON [APC].[Birth]([MaternitySpellSourceUniqueID] ASC, [BirthOrder] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Birth]
    ON [APC].[Birth]([SourcePatientNo] ASC, [PASUpdated] ASC, [SourceUniqueID] ASC);

