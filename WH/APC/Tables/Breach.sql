﻿CREATE TABLE [APC].[Breach] (
    [EncounterRecno]               INT           IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]               INT           NULL,
    [BreachDays]                   INT           NULL,
    [ClockStartDate]               SMALLDATETIME NULL,
    [BreachDate]                   SMALLDATETIME NULL,
    [RTTBreachDate]                SMALLDATETIME NULL,
    [RTTDiagnosticBreachDate]      SMALLDATETIME NULL,
    [NationalBreachDate]           SMALLDATETIME NULL,
    [NationalDiagnosticBreachDate] SMALLDATETIME NULL,
    [SocialSuspensionDays]         INT           NULL,
    [BreachTypeCode]               VARCHAR (10)  NULL,
    [UnadjustedClockStartDate]     SMALLDATETIME NULL,
    CONSTRAINT [PK_Breach] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);

