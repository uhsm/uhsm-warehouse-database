﻿CREATE TABLE [APC].[CriticalCare] (
    [SourceUniqueID]                    VARCHAR (254) NOT NULL,
    [SourceSpellNo]                     VARCHAR (9)   NOT NULL,
    [SourcePatientNo]                   VARCHAR (9)   NOT NULL,
    [StartDate]                         SMALLDATETIME NULL,
    [StartTime]                         SMALLDATETIME NULL,
    [EndDate]                           SMALLDATETIME NULL,
    [EndTime]                           SMALLDATETIME NULL,
    [AdvancedCardiovascularSupportDays] SMALLINT      NULL,
    [AdvancedRespiratorySupportDays]    SMALLINT      NULL,
    [BasicCardiovascularSupportDays]    SMALLINT      NULL,
    [BasicRespiratorySupportDays]       SMALLINT      NULL,
    [CriticalCareLevel2Days]            SMALLINT      NULL,
    [CriticalCareLevel3Days]            SMALLINT      NULL,
    [DermatologicalSupportDays]         SMALLINT      NULL,
    [LiverSupportDays]                  SMALLINT      NULL,
    [NeurologicalSupportDays]           SMALLINT      NULL,
    [RenalSupportDays]                  SMALLINT      NULL,
    [CreatedByUser]                     VARCHAR (3)   NULL,
    [CreatedByTime]                     VARCHAR (12)  NULL,
    [LocalIdentifier]                   VARCHAR (8)   NULL,
    [LocationCode]                      VARCHAR (4)   NULL,
    [StatusCode]                        VARCHAR (11)  NULL,
    [TreatmentFunctionCode]             VARCHAR (4)   NULL,
    [PlannedAcpPeriod]                  VARCHAR (3)   NULL,
    [Created]                           DATETIME      NULL,
    [ByWhom]                            VARCHAR (50)  NULL,
    CONSTRAINT [PK_CriticalCare] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);

