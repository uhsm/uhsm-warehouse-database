﻿CREATE TABLE [APC].[DiagnosisCoding] (
    [SourceUniqueID]    INT           NOT NULL,
    [SequenceNo]        NUMERIC (18)  NULL,
    [DiagnosisCode]     VARCHAR (20)  NULL,
    [CodeType]          VARCHAR (30)  NULL,
    [DiagnosisDate]     SMALLDATETIME NULL,
    [APCSourceUniqueID] NUMERIC (18)  NULL,
    [SupplementaryCode] VARCHAR (20)  NULL,
    [PASCreated]        DATETIME      NULL,
    [PASUpdated]        DATETIME      NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_APC_DiagnosisCoding_APCSourceUniqueID]
    ON [APC].[DiagnosisCoding]([APCSourceUniqueID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_APC_DiagnosisCoding_DiagnosisCode]
    ON [APC].[DiagnosisCoding]([DiagnosisCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_APC_DiagnosisCoding_SequenceNo]
    ON [APC].[DiagnosisCoding]([SequenceNo] ASC);

