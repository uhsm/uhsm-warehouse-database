﻿CREATE TABLE [APC].[DiagnosisCodingTEST] (
    [SourceUniqueID]    INT           NOT NULL,
    [SequenceNo]        NUMERIC (18)  NULL,
    [DiagnosisCode]     VARCHAR (20)  NULL,
    [CodeType]          VARCHAR (30)  NULL,
    [DiagnosisDate]     SMALLDATETIME NULL,
    [APCSourceUniqueID] NUMERIC (18)  NULL,
    [SupplementaryCode] VARCHAR (20)  NULL,
    [PASCreated]        DATETIME      NULL,
    [PASUpdated]        DATETIME      NULL
);

