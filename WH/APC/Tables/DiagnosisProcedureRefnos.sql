﻿CREATE TABLE [APC].[DiagnosisProcedureRefnos] (
    [EpisodeUniqueID] INT          NULL,
    [DPTypCode]       VARCHAR (20) NULL,
    [SortOrder]       INT          NULL,
    [DGProRefo]       INT          NULL,
    [Created]         DATETIME     NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_APCDiagProcRefno_DGProRefo]
    ON [APC].[DiagnosisProcedureRefnos]([DGProRefo] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_APCDiagProcRefno_EpisodeUniqueID]
    ON [APC].[DiagnosisProcedureRefnos]([EpisodeUniqueID] ASC);

