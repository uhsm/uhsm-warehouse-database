﻿CREATE TABLE [APC].[HRG45Encounter] (
    [EncounterRecno]            INT          NOT NULL,
    [ProviderSpellNo]           VARCHAR (20) NULL,
    [SourceUniqueID]            VARCHAR (20) NULL,
    [HRGCode]                   VARCHAR (10) NULL,
    [GroupingMethodFlag]        VARCHAR (10) NULL,
    [DominantOperationCode]     VARCHAR (10) NULL,
    [PBCCode]                   VARCHAR (10) NULL,
    [CalculatedEpisodeDuration] SMALLINT     NULL,
    [ReportingEpisodeDuration]  SMALLINT     NULL,
    [Trimpoint]                 SMALLINT     NULL,
    [ExcessBeddays]             SMALLINT     NULL,
    [SpellReportFlag]           VARCHAR (10) NULL,
    [Created]                   DATETIME     NULL,
    CONSTRAINT [PK_HRG45Encounter] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);

