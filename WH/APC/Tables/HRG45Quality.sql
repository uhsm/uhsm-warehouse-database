﻿CREATE TABLE [APC].[HRG45Quality] (
    [EncounterRecno]  INT           NOT NULL,
    [SequenceNo]      SMALLINT      NOT NULL,
    [QualityTypeCode] VARCHAR (10)  NULL,
    [QualityCode]     VARCHAR (10)  NULL,
    [QualityMessage]  VARCHAR (255) NULL,
    CONSTRAINT [PK_HRG45Quality] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC, [SequenceNo] ASC)
);

