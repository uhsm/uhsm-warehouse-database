﻿CREATE TABLE [APC].[HRG45SpecialistService] (
    [EncounterRecno]        INT          NOT NULL,
    [SequenceNo]            SMALLINT     NOT NULL,
    [SpecialistServiceCode] VARCHAR (10) NULL,
    CONSTRAINT [PK_HRG45SpecialistService] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC, [SequenceNo] ASC)
);

