﻿CREATE TABLE [APC].[HRG45Spell] (
    [EncounterRecno]         INT          NOT NULL,
    [ProviderSpellNo]        VARCHAR (20) NULL,
    [HRGCode]                VARCHAR (10) NULL,
    [DominantOperationCode]  VARCHAR (10) NULL,
    [PBCCode]                VARCHAR (10) NULL,
    [PrimaryDiagnosisCode]   VARCHAR (10) NULL,
    [SecondaryDiagnosisCode] VARCHAR (10) NULL,
    [EpisodeCount]           SMALLINT     NULL,
    [LOS]                    SMALLINT     NULL,
    [ReportingSpellLOS]      SMALLINT     NULL,
    [Trimpoint]              SMALLINT     NULL,
    [ExcessBeddays]          SMALLINT     NULL,
    [CCDays]                 SMALLINT     NULL,
    CONSTRAINT [PK_HRG45Spell] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);

