﻿CREATE TABLE [APC].[HRG45SpellFlags] (
    [EncounterRecno] INT          NOT NULL,
    [SequenceNo]     INT          NOT NULL,
    [SpellFlagCode]  VARCHAR (10) NULL,
    CONSTRAINT [PK_HRG45APCSpellFlags] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC, [SequenceNo] ASC)
);

