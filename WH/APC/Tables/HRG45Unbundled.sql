﻿CREATE TABLE [APC].[HRG45Unbundled] (
    [EncounterRecno] INT          NOT NULL,
    [SequenceNo]     SMALLINT     NOT NULL,
    [HRGCode]        VARCHAR (10) NULL,
    CONSTRAINT [PK_HRG45Unbundled] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC, [SequenceNo] ASC)
);

