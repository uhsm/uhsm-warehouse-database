﻿CREATE TABLE [APC].[PBRSpell] (
    [HRGEncounterRecNo]         INT          IDENTITY (1, 1) NOT NULL,
    [SourceSpellNo]             INT          NULL,
    [SpellHRGCode]              VARCHAR (20) NULL,
    [SpellSSCCode]              VARCHAR (3)  NULL,
    [AdjustedExcessBedDays]     INT          NULL,
    [FinalExcessBedDays]        INT          NULL,
    [POD2]                      VARCHAR (10) NULL,
    [POD3]                      VARCHAR (10) NULL,
    [PBRSpellActivityIndicator] BIT          NULL,
    [DateLoaded]                DATETIME     NULL,
    CONSTRAINT [PK_HRG] PRIMARY KEY CLUSTERED ([HRGEncounterRecNo] ASC),
    CONSTRAINT [PKHRG_SPELL] UNIQUE NONCLUSTERED ([SourceSpellNo] ASC)
);

