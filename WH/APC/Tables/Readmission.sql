﻿CREATE TABLE [APC].[Readmission] (
    [ReadmissionRecno]       INT NOT NULL,
    [PreviousAdmissionRecno] INT NOT NULL,
    CONSTRAINT [PK_Readmission] PRIMARY KEY CLUSTERED ([ReadmissionRecno] ASC, [PreviousAdmissionRecno] ASC)
);

