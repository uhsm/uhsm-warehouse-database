﻿CREATE TABLE [APC].[WaitingListBreach] (
    [EncounterRecno]               INT           IDENTITY (1, 1) NOT NULL,
    [CensusDate]                   SMALLDATETIME NULL,
    [SourceUniqueID]               INT           NULL,
    [BreachDays]                   INT           NULL,
    [ClockStartDate]               SMALLDATETIME NULL,
    [BreachDate]                   SMALLDATETIME NULL,
    [RTTBreachDate]                SMALLDATETIME NULL,
    [RTTDiagnosticBreachDate]      SMALLDATETIME NULL,
    [NationalBreachDate]           SMALLDATETIME NULL,
    [NationalDiagnosticBreachDate] SMALLDATETIME NULL,
    [SocialSuspensionDays]         INT           NULL,
    [BreachTypeCode]               VARCHAR (10)  NULL,
    CONSTRAINT [PK_WaitingListBreach] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_WaitingListBreach]
    ON [APC].[WaitingListBreach]([CensusDate] ASC, [SourceUniqueID] ASC);

