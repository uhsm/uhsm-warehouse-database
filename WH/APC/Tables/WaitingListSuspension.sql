﻿CREATE TABLE [APC].[WaitingListSuspension] (
    [SuspensionRecno]           INT           IDENTITY (1, 1) NOT NULL,
    [CensusDate]                SMALLDATETIME NOT NULL,
    [SourceUniqueID]            INT           NOT NULL,
    [WaitingListSourceUniqueID] INT           NOT NULL,
    [SuspensionStartDate]       SMALLDATETIME NULL,
    [SuspensionEndDate]         SMALLDATETIME NULL,
    [SuspensionReasonCode]      INT           NULL,
    [PASCreated]                DATETIME      NULL,
    [PASUpdated]                DATETIME      NULL,
    [PASCreatedByWhom]          VARCHAR (30)  NULL,
    [PASUpdatedByWhom]          VARCHAR (30)  NULL,
    [Created]                   DATETIME      NULL,
    [Updated]                   DATETIME      NULL,
    [ByWhom]                    VARCHAR (50)  NULL,
    CONSTRAINT [PK_Suspension] PRIMARY KEY CLUSTERED ([CensusDate] ASC, [SourceUniqueID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_WaitingListSuspension]
    ON [APC].[WaitingListSuspension]([SuspensionRecno] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_WaitingListSuspension_1]
    ON [APC].[WaitingListSuspension]([WaitingListSourceUniqueID] ASC, [SuspensionReasonCode] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_WaitingListSuspension_13_1586820715__K4_K2_K8_K3_6_7]
    ON [APC].[WaitingListSuspension]([WaitingListSourceUniqueID] ASC, [CensusDate] ASC, [SuspensionReasonCode] ASC, [SourceUniqueID] ASC)
    INCLUDE([SuspensionStartDate], [SuspensionEndDate]);


GO
CREATE STATISTICS [_dta_stat_1586820715_8_2]
    ON [APC].[WaitingListSuspension]([SuspensionReasonCode], [CensusDate]);


GO
CREATE STATISTICS [_dta_stat_1586820715_3_4_8]
    ON [APC].[WaitingListSuspension]([SourceUniqueID], [WaitingListSourceUniqueID], [SuspensionReasonCode]);


GO
CREATE STATISTICS [_dta_stat_1586820715_2_8_3]
    ON [APC].[WaitingListSuspension]([CensusDate], [SuspensionReasonCode], [SourceUniqueID]);


GO
CREATE STATISTICS [_dta_stat_1586820715_4_2_3]
    ON [APC].[WaitingListSuspension]([WaitingListSourceUniqueID], [CensusDate], [SourceUniqueID]);


GO
CREATE STATISTICS [_dta_stat_1586820715_8_4_2_3]
    ON [APC].[WaitingListSuspension]([SuspensionReasonCode], [WaitingListSourceUniqueID], [CensusDate], [SourceUniqueID]);

