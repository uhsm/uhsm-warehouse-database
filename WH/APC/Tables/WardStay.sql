﻿CREATE TABLE [APC].[WardStay] (
    [SourceUniqueID]   INT           NOT NULL,
    [SourceSpellNo]    INT           NOT NULL,
    [SourcePatientNo]  INT           NOT NULL,
    [StartTime]        SMALLDATETIME NULL,
    [EndTime]          SMALLDATETIME NULL,
    [WardCode]         INT           NULL,
    [ProvisionalFlag]  BIT           NULL,
    [PASCreated]       DATETIME      NULL,
    [PASUpdated]       DATETIME      NULL,
    [PASCreatedByWhom] VARCHAR (30)  NULL,
    [PASUpdatedByWhom] VARCHAR (30)  NULL,
    [Created]          DATETIME      NOT NULL,
    [Updated]          DATETIME      NULL,
    [ByWhom]           VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_WardStay] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_APCWard_Stay_1]
    ON [APC].[WardStay]([SourceSpellNo] ASC, [StartTime] ASC);

