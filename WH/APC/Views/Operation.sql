﻿CREATE view [APC].[Operation] as

select
	 SourceUniqueID = DGPRO_REFNO
	,SequenceNo = SORT_ORDER
	,SourceOperationCode = ODPCD_REFNO
	,OperationCode = CODE
	,OperationDate = DGPRO_DTTM
	,APCSourceUniqueID = SORCE_REFNO
from
	PAS.ClinicalCodingBase
where
	SORCE_CODE = 'PRCAE'
and	DPTYP_CODE = 'PROCE'
and	ARCHV_FLAG = 'N'
