﻿CREATE view [APC].[SocialSuspension] as

select
	 WaitingListSourceUniqueID
	,CensusDate
	,sum(datediff(day, SuspensionStartDate, coalesce(SuspensionEndDate, CensusDate))) DaysSuspended
from
	APC.WaitingListSuspension Suspension
where
	SuspensionReasonCode in
	(
	 4398	--Patient Away
	,4397	--Patient Request
	)
group by
	 WaitingListSourceUniqueID
	,CensusDate
