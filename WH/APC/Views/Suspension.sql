﻿CREATE view [APC].[Suspension] as

select
	 WaitingListSourceUniqueID
	,CensusDate
	,sum(datediff(day, SuspensionStartDate, SuspensionEndDate)) DaysSuspended
from
	APC.WaitingListSuspension Suspension
group by
	 WaitingListSourceUniqueID
	,CensusDate
