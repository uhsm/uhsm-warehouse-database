﻿

CREATE procedure [CASCADE].[UpdateCopiedFlag] as 

/*
--Author: K Oakden
--Date created: 08/10/2014
--Get copied datetime stamp from DWCOPIED.dbf
*/
--Need to check file exists - to do
update [CASCADE].DWCopied set
	lastchecked = GETDATE(),
	lastcopied = DATEADD(
	minute 	
		,case
			when copytime like '[012][0123456789]:[012345][0123456789]%'
			then convert(int, left(copytime, 2)) * 60 + convert(int, substring(copytime, 4, 2))
			else 0
		end
		,
		--case
		--when isdate(copydate) = 1
		--then 
		convert(datetime, copydate, 103)
		--else null
		--end
	)
from [CASCADE].DWCopied

declare @fileDate datetime;
declare @paramDate datetime;

select @fileDate = lastcopied from [CASCADE].DWCopied
select @paramDate = DateValue from dbo.Parameter where Parameter = 'COPYAEDATE'

if (@fileDate > @paramDate)
begin
	update dbo.Parameter set DateValue = @fileDate where Parameter = 'COPYAEDATE'
end

--select  * from [CASCADE].DWCopied
--select  * from dbo.Parameter where Parameter = 'COPYAEDATE'
