﻿CREATE TABLE [CASCADE].[AEAppmts] (
    [clinictype] VARCHAR (10) NULL,
    [apptdate]   DATE         NULL,
    [appttime]   VARCHAR (5)  NULL,
    [aepatno]    VARCHAR (8)  NULL,
    [attno]      VARCHAR (3)  NULL,
    [clinstat]   VARCHAR (2)  NULL,
    [xrayneeded] BIT          NULL,
    [doxray]     BIT          NULL,
    [poparrival] BIT          NULL,
    [bloodres]   BIT          NULL,
    [ambulreq]   BIT          NULL,
    [attendstat] VARCHAR (3)  NULL,
    [cancelifok] BIT          NULL,
    [extra]      BIT          NULL,
    [timelength] NUMERIC (1)  NULL,
    [remake]     BIT          NULL
);

