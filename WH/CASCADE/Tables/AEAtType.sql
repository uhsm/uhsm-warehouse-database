﻿CREATE TABLE [CASCADE].[AEAtType] (
    [code]    VARCHAR (1)  NULL,
    [label]   VARCHAR (11) NULL,
    [disprow] NUMERIC (3)  NULL
);

