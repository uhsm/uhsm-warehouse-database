﻿CREATE TABLE [CASCADE].[AEBreachLU] (
    [br_code]   NVARCHAR (1)  NULL,
    [br_reason] NVARCHAR (30) NULL,
    [disprow]   NUMERIC (2)   NULL,
    [redundant] BIT           NULL
);

