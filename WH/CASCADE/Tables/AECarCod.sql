﻿CREATE TABLE [CASCADE].[AECarCod] (
    [diagref]   VARCHAR (11) NULL,
    [codetype]  VARCHAR (1)  NULL,
    [codefound] VARCHAR (6)  NULL,
    [weighting] NUMERIC (3)  NULL
);


GO
CREATE NONCLUSTERED INDEX [IX1_CASCADE_AECarCod]
    ON [CASCADE].[AECarCod]([codetype] ASC)
    INCLUDE([diagref], [codefound], [weighting]);

