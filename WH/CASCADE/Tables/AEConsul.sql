﻿CREATE TABLE [CASCADE].[AEConsul] (
    [concode]    VARCHAR (4)  NULL,
    [conname]    VARCHAR (25) NULL,
    [coninits]   VARCHAR (4)  NULL,
    [contitle]   VARCHAR (4)  NULL,
    [specialty1] VARCHAR (4)  NULL,
    [specialty2] VARCHAR (4)  NULL,
    [specialty3] VARCHAR (4)  NULL,
    [specialty4] VARCHAR (4)  NULL,
    [specialty5] VARCHAR (4)  NULL,
    [constatus]  VARCHAR (1)  NULL
);

