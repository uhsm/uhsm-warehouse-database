﻿CREATE TABLE [CASCADE].[AEExtras] (
    [attendref]  VARCHAR (11) NOT NULL,
    [cardnoted]  BIT          NULL,
    [partaking]  VARCHAR (1)  NULL,
    [alcohfreq]  VARCHAR (1)  NULL,
    [alcoquant]  VARCHAR (1)  NULL,
    [alcoexcess] VARCHAR (1)  NULL,
    [location12] VARCHAR (2)  NULL,
    [appaccept]  NUMERIC (1)  NULL,
    [safemechex] VARCHAR (1)  NULL,
    [capability] VARCHAR (1)  NULL,
    [assault]    VARCHAR (1)  NULL,
    [naiconcern] VARCHAR (1)  NULL,
    [contactspr] VARCHAR (1)  NULL,
    [alcdrrelat] VARCHAR (1)  NULL,
    [alcdruguse] VARCHAR (1)  NULL,
    [domestviol] VARCHAR (1)  NULL,
    [traumainjy] VARCHAR (1)  NULL,
    CONSTRAINT [PK_AEExtras] PRIMARY KEY CLUSTERED ([attendref] ASC)
);

