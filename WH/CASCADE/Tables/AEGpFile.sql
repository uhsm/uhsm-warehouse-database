﻿CREATE TABLE [CASCADE].[AEGpFile] (
    [gpref]     VARCHAR (6)  NOT NULL,
    [gptitle]   VARCHAR (3)  NULL,
    [gpini]     VARCHAR (4)  NULL,
    [gpsurname] VARCHAR (24) NULL,
    [prcode]    VARCHAR (4)  NULL,
    [gppascode] VARCHAR (4)  NULL,
    [gpnatcode] VARCHAR (6)  NULL,
    [gpfh]      BIT          NULL,
    [gone]      BIT          NULL,
    [checked]   BIT          NULL,
    [gprunexcl] VARCHAR (1)  NULL,
    CONSTRAINT [PK_AEGpFile] PRIMARY KEY CLUSTERED ([gpref] ASC)
);

