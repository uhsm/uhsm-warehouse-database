﻿CREATE TABLE [CASCADE].[AEGpPrac] (
    [prcode]     VARCHAR (4)  NOT NULL,
    [prname]     VARCHAR (30) NULL,
    [praddress1] VARCHAR (30) NULL,
    [praddress2] VARCHAR (30) NULL,
    [praddress3] VARCHAR (30) NULL,
    [praddress4] VARCHAR (30) NULL,
    [prpostcode] VARCHAR (10) NULL,
    [prphone]    VARCHAR (25) NULL,
    [prfax]      VARCHAR (25) NULL,
    [xrref]      VARCHAR (10) NULL,
    [checked]    BIT          NULL,
    [gprunexcl]  VARCHAR (1)  NULL,
    [pracncode]  VARCHAR (6)  NULL,
    CONSTRAINT [PK_AEGpPrac] PRIMARY KEY CLUSTERED ([prcode] ASC)
);

