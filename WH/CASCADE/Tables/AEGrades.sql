﻿CREATE TABLE [CASCADE].[AEGrades] (
    [grade]      VARCHAR (3)  NULL,
    [profession] VARCHAR (1)  NULL,
    [detail]     VARCHAR (30) NULL,
    [inactive]   BIT          NULL
);

