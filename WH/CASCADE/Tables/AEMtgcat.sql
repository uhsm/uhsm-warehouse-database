﻿CREATE TABLE [CASCADE].[AEMtgcat] (
    [mtg_code]   VARCHAR (3)  NOT NULL,
    [mtg_item]   VARCHAR (40) NULL,
    [mtg_categ]  VARCHAR (1)  NOT NULL,
    [mtg_prog]   VARCHAR (3)  NULL,
    [redundant]  BIT          NULL,
    [infectiond] VARCHAR (1)  NULL,
    CONSTRAINT [PK_AEMtgcat] PRIMARY KEY CLUSTERED ([mtg_code] ASC, [mtg_categ] ASC)
);

