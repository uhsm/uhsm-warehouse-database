﻿CREATE TABLE [CASCADE].[AEPERCVD] (
    [itemcode]  VARCHAR (3)   NULL,
    [label]     VARCHAR (200) NULL,
    [disprow]   INT           NULL,
    [redundant] BIT           NULL,
    [location]  VARCHAR (2)   NULL
);

