﻿CREATE TABLE [CASCADE].[AEReTrge] (
    [retriref]   VARCHAR (11) NULL,
    [retrino]    VARCHAR (2)  NULL,
    [retriage]   VARCHAR (1)  NULL,
    [retridate]  DATE         NULL,
    [retritime]  VARCHAR (5)  NULL,
    [retri_comm] TEXT         NULL,
    [retrinurse] VARCHAR (5)  NULL,
    [rtobjpain]  VARCHAR (2)  NULL,
    [rtsubpain]  VARCHAR (2)  NULL
);

