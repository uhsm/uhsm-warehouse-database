﻿CREATE TABLE [CASCADE].[AESAVES] (
    [savedate]   VARCHAR (50) NULL,
    [module]     VARCHAR (10) NULL,
    [ref]        VARCHAR (11) NULL,
    [saveuser]   VARCHAR (5)  NULL,
    [savetime]   VARCHAR (5)  NULL,
    [nwusername] VARCHAR (10) NULL,
    [downtime]   VARCHAR (2)  NULL,
    [comment]    VARCHAR (40) NULL,
    [pcid]       VARCHAR (10) NULL
);

