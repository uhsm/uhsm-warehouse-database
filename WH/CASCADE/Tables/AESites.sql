﻿CREATE TABLE [CASCADE].[AESites] (
    [code]      VARCHAR (3)  NULL,
    [label]     VARCHAR (15) NULL,
    [bodyorder] NUMERIC (3)  NULL,
    [bodyarea]  NUMERIC (1)  NULL,
    [headflag]  BIT          NULL,
    [eyeflag]   BIT          NULL,
    [noseflag]  BIT          NULL,
    [neckflag]  BIT          NULL,
    [backflag]  BIT          NULL,
    [abdflag]   BIT          NULL,
    [entflag]   BIT          NULL,
    [chestflag] BIT          NULL
);

