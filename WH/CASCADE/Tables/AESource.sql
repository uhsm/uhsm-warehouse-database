﻿CREATE TABLE [CASCADE].[AESource] (
    [code]      VARCHAR (2)  NULL,
    [label]     VARCHAR (10) NULL,
    [disprow]   NUMERIC (3)  NULL,
    [redundant] BIT          NULL
);

