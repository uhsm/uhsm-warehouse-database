﻿CREATE TABLE [CASCADE].[AESpecs] (
    [code]      VARCHAR (4)  NULL,
    [label]     VARCHAR (30) NULL,
    [disprow]   NUMERIC (3)  NULL,
    [division]  VARCHAR (3)  NULL,
    [redundant] BIT          NULL
);

