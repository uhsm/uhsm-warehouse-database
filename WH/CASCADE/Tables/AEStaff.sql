﻿CREATE TABLE [CASCADE].[AEStaff] (
    [staffcode]  VARCHAR (5)  NULL,
    [seencode]   VARCHAR (5)  NULL,
    [staffsurn]  VARCHAR (24) NULL,
    [staffforen] VARCHAR (20) NULL,
    [stafftype]  VARCHAR (3)  NULL,
    [stinactive] BIT          NULL,
    [startdate]  DATE         NULL,
    [enddate]    DATE         NULL,
    [location]   VARCHAR (1)  NULL,
    [stafftitle] VARCHAR (5)  NULL,
    [stfenddets] VARCHAR (30) NULL,
    [staffid]    VARCHAR (3)  NULL
);

