﻿CREATE TABLE [CASCADE].[AEStndBy] (
    [stattendrf] VARCHAR (11) NOT NULL,
    [stcalldate] VARCHAR (22) NULL,
    [stcalltime] VARCHAR (5)  NOT NULL,
    [sttrauma]   BIT          NULL,
    [stcomplnt]  TEXT         NULL,
    [stmechan]   TEXT         NULL,
    [airwayown]  VARCHAR (1)  NULL,
    [intubation] VARCHAR (1)  NULL,
    [o2saturate] VARCHAR (3)  NULL,
    [stresprate] VARCHAR (1)  NULL,
    [centpulse]  VARCHAR (1)  NULL,
    [cardrhythm] VARCHAR (1)  NULL,
    [disability] VARCHAR (1)  NULL,
    [crcomments] TEXT         NULL,
    [steta]      VARCHAR (2)  NULL,
    [stothcomms] TEXT         NULL,
    [stuser]     VARCHAR (5)  NULL,
    CONSTRAINT [PK_AEStndBy] PRIMARY KEY NONCLUSTERED ([stattendrf] ASC, [stcalltime] ASC)
);


GO
CREATE CLUSTERED INDEX [IX_AEStndBy]
    ON [CASCADE].[AEStndBy]([stattendrf] ASC);

