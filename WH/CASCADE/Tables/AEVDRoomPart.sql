﻿CREATE TABLE [CASCADE].[AEVDRoomPart] (
    [aepatno]    VARCHAR (8) NULL,
    [attno]      VARCHAR (3) NULL,
    [specdrdate] DATETIME    NULL,
    [specdrtime] VARCHAR (5) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_AEVDRoomPart]
    ON [CASCADE].[AEVDRoomPart]([aepatno] ASC, [attno] ASC);

