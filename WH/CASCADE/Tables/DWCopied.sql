﻿CREATE TABLE [CASCADE].[DWCopied] (
    [copydate]    VARCHAR (50) NULL,
    [copytime]    VARCHAR (50) NULL,
    [lastchecked] DATETIME     NULL,
    [lastcopied]  DATETIME     NULL
);

