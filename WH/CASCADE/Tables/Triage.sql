﻿CREATE TABLE [CASCADE].[Triage] (
    [SourceUniqueID]       VARCHAR (15) NOT NULL,
    [ArrivalDate]          DATETIME     NULL,
    [ArrivalTime]          VARCHAR (5)  NULL,
    [triagedate]           VARCHAR (22) NULL,
    [triagetime]           VARCHAR (5)  NULL,
    [triage]               VARCHAR (1)  NULL,
    [ManchesterTriageCode] VARCHAR (3)  NULL,
    [TriageCategoryCode]   VARCHAR (1)  NULL,
    [NonTrauma]            BIT          NULL,
    [MtgDiscr]             VARCHAR (2)  NULL,
    PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);

