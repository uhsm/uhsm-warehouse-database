﻿CREATE VIEW "CASCADE".AE_ATTEND_ADDITIONAL_VIEW (ATTEND_REF,CALLED_DATE,CALLED_TIME,ROUTE,ATTEND_MECH,PT_COMPLAINT,AE_ASSESSMENT)
AS(
    select  t1."ATTEND_REF", 
            t2."triagedate", 
            t2."triagetime",
            'TRIAGE',            
            t2."trclomech", 
            cast (t2."othcomms" as varchar (500)) ,
            t3."mtg_item"
    from AE_ATTEND_VIEW t1
    inner JOIN "AETriage" t2 ON t1."ATTEND_REF" = t2."attendref"
    left join "AEMtgcat" t3 on t2."mtg_categ" = t3."mtg_code" where t3."mtg_categ" ='M'
  UNION
    select t1."ATTEND_REF",
           t2."stcalldate",
           t2."stcalltime",
           'STANDBY',
           cast (t2."stmechan" as varchar (500)), 
           cast (t2."stcomplnt" as varchar (500)),
           /*IsNull (cast (t2."stmechan" as nvarchar (max)),'') + IsNull (cast (t2."stothcomms" as nvarchar (max)),''),*/
           cast (t2."stothcomms" as varchar (500))
    from AE_ATTEND_VIEW t1
    inner JOIN AEStndBy t2 ON t1."ATTEND_REF" = t2."stattendrf"
  )