﻿CREATE VIEW "CASCADE"."AE_ATTEND_VIEW" (AEPATNO, ATTEND_NO,ATTEND_REF,REGISTER_DATE,REGISTER_TIME,SPECIALTY,ATTEND_TYPE,ATTEND_SOURCE,DOCTOR_CODE,DOCTOR_FORNAMES, DOCTOR_SURNAME, DISPOSAL_CODE,DISPOSAL_DESC,ADMIT_WARD,CONSULTANT,HOSPITAL_NAME,OTHERHOSPITAL_WARD,TREATMENT_COMP_DATE, TREATMENT_COMP_TIME)
AS (
select t1."aepatno", 
      t1."attno", 
      t1."aepatno" + t1."attno" attend_ref,
      t1."regdate",/*convert(nvarchar(10),t1."regdate") regdate,*/
      t1."regtime", 
      t1."specialty",
      t2."label" attend_type,
      t3."label" attend_source,
      t1."doctor" attending_doctor_code,
      t4."staffforen" doctor_forenames,
      t4."staffsurn" doctor_surname,
      t1."disposal" disposal_code,
      disposal_desc = CASE t1."disposal"  
           WHEN 'DF' THEN 'Discharge for further treatment in the future' 
           WHEN 'DA' THEN 'Discharge home'
           WHEN 'DI' THEN 'Patient brought in dead or died at AE'
           WHEN 'TH' THEN 'Transferred out for another hospital'
           WHEN 'X' THEN 'Patient did not wait'
           WHEN 'AD' THEN 'Patient was admitted to a ward'
           ELSE 'Unknown'
      END,
      t1."admitward",
      t1."consultant",
      t1."hospname",
      t1."othhosward",
      convert(varchar(10),t1."trtcompdat") trtcompdat, 
      t1."trtcomptim" 
from "AEAttend" t1
left outer join "AEAtType" t2 on t1."atttype" = t2."code"
left outer join "AESource" t3 on t1."source" = t3."code"
left outer join "AEStaff"  t4 on t1."doctor" = t4."staffcode"
where t1."regdate">= DateAdd(DD, DateDiff(DD,0,GETDATE())-1, 0) 
  AND t1."regdate" < DateAdd(DD, DateDiff(DD,0,GETDATE()),0)
)