﻿CREATE VIEW "CASCADE"."AE_PATIENT_VIEW" ("AEPATNO", "FACIL_ID", "NHS_NUMBER", "SURNAME", "FORENAMES", "TITLE", "DOB", "SEX",  
"PAT_ADD_LINE1","PAT_ADD_LINE2","PAT_ADD_LINE3","PAT_ADD_LINE4","PAT_POSTCODE","PAT_HD_CODE", "PAT_PHONE","MARITAL","RELIGION",
"GP_PAS_CODE","GP_NATIONAL_CODE", "PRACTICE_NAT_CODE","PRACTICE_POSTCODE", "NOK_NAME", "NOK_RELATIONSHIP", "NOK_ADD_LINE1", "NOK_ADD_LINE2",
"NOK_ADD_LINE3", "NOK_ADD_LINE4", "NOK_POSTCODE", "NOK_PHONE", "LAST_ATTEND")
AS
  (SELECT t1."aepatno", 
          t1."rm2number",
          t1."nhsno",
          t1."surname",
          t1."forename",
          t1."title",
          t1."dob",
          t1."sex",
          t1."address1",
          t1."address2",
          t1."address3",
          t1."address4",
          t1."postcode",
          t1."distno",
          t1."telephone",
          t1."marital",
          t1."religion", 
          t2."gppascode",
          t2."gpnatcode", 
          t3."pracncode", 
          t3."prpostcode",
          t1."nokname",
          t1."nokrelate",
          t1."nokadd1",
          t1."nokadd2",
          t1."nokadd3",
          t1."nokadd4",
          t1."nokadd5",
          t1."noktele",
          t1."lastattend"
  FROM AE_ATTEND_VIEW t4
  LEFT JOIN "AEPatMas" t1 on t4."AEPATNO" = t1."aepatno"
  LEFT JOIN "AEGpFile" t2  ON t1."gpref" = t2."gpref"
  LEFT JOIN "AEGpPrac" t3  ON t2."prcode" = t3."prcode"
where t4."REGISTER_DATE" >= DateAdd(DD, DateDiff(DD,0,GETDATE())-1, 0) 
  AND t4."REGISTER_DATE" < DateAdd(DD, DateDiff(DD,0,GETDATE()),0)
  )