﻿CREATE VIEW "CASCADE".AE_STANDBY_VIEW (ATTEND_REF, ATTEND_TYPE, STD_DATE, STD_TIME,STD_REASON, STD_COMMENT)
AS(
    select t1."ATTEND_REF",  
            t1."ATTEND_TYPE",
           t2."stcalldate",
           t2."stcalltime",
           t2."stcomplnt",
           IsNull (cast (t2."stmechan" as nvarchar (max)),'') + IsNull (cast (t2."stothcomms" as nvarchar (max)),'')
    from AE_ATTEND_VIEW t1
    inner JOIN AEStndBy t2 ON t1."ATTEND_REF" = t2."stattendrf"
  )