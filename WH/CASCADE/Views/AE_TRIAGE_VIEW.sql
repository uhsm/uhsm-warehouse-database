﻿CREATE VIEW "CASCADE".AE_TRIAGE_VIEW (ATTEND_REF, ATTEND_TYPE, TRIAGE_DATE,TRIAGE_TIME,TR_REASON,TR_COMMENT)
AS(
    select  t1."ATTEND_REF", 
            t1."ATTEND_TYPE",  
            t2."triagedate", 
            t2."triagetime", 
            t2."trclomech", 
            t2."othcomms" 
    from AE_ATTEND_VIEW t1
    inner JOIN "AETriage" t2 ON t1."ATTEND_REF" = t2."attendref"
  )