﻿




CREATE view [CASCADE].[TLoadAEAttend] as
select
			 Encounter.aepatno
			,Encounter.attno
			,Encounter.regdate
			,Encounter.regtime

--Arrival date is the earlier of regdate and triagedate
			,ArrivalDate =

				case
				when isdate(Triage.triagedate) <> 1
				then Encounter.regdate

				when isdate(Encounter.regdate) <> 1
				then Triage.triagedate

				when convert(datetime, Encounter.regdate) < convert(datetime, Triage.triagedate)
				--then Encounter.regdate
				then convert(datetime, Encounter.regdate)
				--else Triage.triagedate
				else convert(datetime, Triage.triagedate)
				end

			,ArrivalTime =
				case
				when isdate(Triage.triagedate) <> 1
				then Encounter.regtime

				when isdate(Encounter.regdate) <> 1
				then Triage.triagetime

				when convert(datetime, Encounter.regdate) = convert(datetime, Triage.triagedate)
				and	Encounter.regtime < Triage.triagetime
				then Encounter.regtime

				when convert(datetime, Encounter.regdate) = convert(datetime, Triage.triagedate)
				and	Encounter.regtime > Triage.triagetime
				then Triage.triagetime

				when convert(datetime, Encounter.regdate) < convert(datetime, Triage.triagedate)
				then Encounter.regtime

				else Triage.triagetime
				end

			,Encounter.child
			,Encounter.atttype
			,Encounter.source
			,Encounter.specialty
			,Encounter.carddate
			,Encounter.cardtime
			,Encounter.drseendate
			,Encounter.drseentime
			,Encounter.endcomment
			,Encounter.dispdate
			,Encounter.disptime
			,Encounter.gpref
			,Encounter.practice
			,Encounter.setting
			,Encounter.rtaprinted
			,Encounter.disposal
			,Encounter.admitward
			,Encounter.consultant
			,Encounter.hospname
			,Encounter.othhosward
			,Encounter.wheredied
			,Encounter.attstatus
			,Encounter.quickmajor
			,Encounter.cubicle
			,Encounter.amincident
			,Encounter.clapptdate
			,Encounter.clappttime
			,Encounter.trtcompdat
			,Encounter.trtcomptim
			,Encounter.dishome
			,Encounter.aesos
			,Encounter.refusedtrt
			,Encounter.policecust
			,Encounter.prisoncust
			,Encounter.sservices
			,Encounter.disother
			,Encounter.disothspec
			,Encounter.revclin
			,Encounter.revcldate
			,Encounter.revcltime
			,Encounter.refphysio
			,Encounter.refergp
			,Encounter.refergpdat
			,Encounter.refowndent
			,Encounter.refereph
			,Encounter.refercpn
			,Encounter.referbsv
			,Encounter.refchiropo
			,Encounter.refother
			,Encounter.refothspec
			,Encounter.refotherae
			,Encounter.fractclin
			,Encounter.opd
			,Encounter.opdspec
			,Encounter.refothsite
			,Encounter.rfouttrust
			,Encounter.doctor
			,Encounter.clinicflag
			,Encounter.referondat
			,Encounter.referontim
			,Encounter.toadmitdat
			,Encounter.toadmittim
			,Encounter.research1
			,Encounter.research2
			,Encounter.research3
			,Encounter.xrgonedate
			,Encounter.xrgonetime
			,Encounter.xrbackdate
			,Encounter.xrbacktime
			,Encounter.xrnstosend
			,Encounter.advicetime
			,Encounter.advicedate
			,Encounter.staff
			,Encounter.stafftime
			,Encounter.staffdate
			,Encounter.fasttrack
			,Encounter.specialchk
			,Encounter.prescpay
			,Encounter.recpresent
			,Encounter.EncounterRecno
			,Encounter.fftextok
			,Triage.triagedate
			,Triage.triagetime
			,triage = 
				case
				when Triage.triage = ''
				then null
				else Triage.triage
				end
			,Encounter.pathway
			,Encounter.assault
			,ManchesterTriageCode = Triage.mtg_categ
			
		from
			[CASCADE].AEAttend Encounter

		left join [CASCADE].AETriage Triage
		on	Triage.attendref = Encounter.aepatno + Encounter.attno
		and	not exists
			(
			select
				1
			from
				[CASCADE].AETriage Previous
			where
				Previous.attendref = Triage.attendref
			and	(
					Previous.triagedate > Triage.triagedate
				or	(
						Previous.triagedate = Triage.triagedate
					and	Previous.triagetime < Triage.triagetime
					)
				or	(
						Previous.triagedate = Triage.triagedate
					and	Previous.triagetime = Triage.triagetime
					and	Previous.EncounterRecno < Triage.EncounterRecno
					)
				)
			)





