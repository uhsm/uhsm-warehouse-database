﻿

CREATE view [CASCADE].[UpdateStatus] as
(
select 1 as UpdateOrder, 'AEAppmts' as TableName, count(1) as Total from [CASCADE].AEAppmts --1
union
select 2, 'AEAttend', count(1) from [CASCADE].AEAttend --2
union
select 3, 'AEAtType', count(1) from [CASCADE].AEAtType --3
union
select 4, 'AEConsul', count(1) from [CASCADE].AEConsul --4
union
select 5, 'AEDiagnsPart', count(1) from [CASCADE].AEDiagnsPart--5
--select count(1) from [CASCADE].AEDiagns 
union
select 6, 'AEGpFile', count(1) from [CASCADE].AEGpFile--6
union
select 7, 'AEGpPrac', count(1) from [CASCADE].AEGpPrac --7
union
select 8, 'AEGrades', count(1) from [CASCADE].AEGrades --8
union
select 9, 'AEICAU', count(1) from [CASCADE].AEICAU --9
union
select 10, 'AEMerges', count(1) from [CASCADE].AEMerges --10
union
select 11, 'AEPatMas', count(1) from [CASCADE].AEPatMas --11
union
select 12, 'AEReTrge', count(1) from [CASCADE].AEReTrge --12
union
select 13, 'AESites', count(1) from [CASCADE].AESites --13
union
select 14, 'AESiteSp', count(1) from [CASCADE].AESiteSp --14
union
select 15, 'AESource', count(1) from [CASCADE].AESource --15
union
select 16, 'AESpecs', count(1) from [CASCADE].AESpecs --16
union
select 17, 'AEStaff', count(1) from [CASCADE].AEStaff --17
union
select 18, 'AETriage', count(1) from [CASCADE].AETriage --18
union
select 19, 'AEStndBy', count(1) from [CASCADE].AEStndBy --19
union
select 20, 'AEMtgcat', count(1) from [CASCADE].AEMtgcat --20
union
select 21, 'AEcarcod', count(1) from [CASCADE].AEcarcod --21
union
select 22, 'AEBreachLU', count(1) from [CASCADE].AEBreachLU --22
union
select 23, 'AEBreach', count(1) from [CASCADE].AEBreach  --23
union
select 24, 'AEExtras', count(1) from [CASCADE].AEExtras --24
union
select 25, 'AEDrinks', count(1) from [CASCADE].AEDrinks --25
)
