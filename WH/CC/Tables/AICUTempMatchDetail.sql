﻿CREATE TABLE [CC].[AICUTempMatchDetail] (
    [SourceSpellNo]                 INT           NULL,
    [NHSNumber]                     VARCHAR (20)  NULL,
    [PatientSurname]                VARCHAR (30)  NULL,
    [AdmissionDate]                 SMALLDATETIME NULL,
    [DischargeDate]                 SMALLDATETIME NULL,
    [SourceCCPStayNo]               INT           NULL,
    [SourceCCPNo]                   INT           NOT NULL,
    [CCPStayDate]                   SMALLDATETIME NULL,
    [Ward]                          VARCHAR (80)  NULL,
    [newCount]                      INT           NULL,
    [oldCount]                      INT           NULL,
    [CountOrgans]                   INT           NULL,
    [newCCPStayNoOfOrgansSupported] NUMERIC (18)  NULL,
    [oldCCPStayNoOfOrgansSupported] NUMERIC (18)  NULL,
    [CCPIndicator]                  CHAR (1)      NULL,
    [AllSupported]                  INT           NULL
);

