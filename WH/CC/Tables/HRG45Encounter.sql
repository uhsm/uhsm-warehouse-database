﻿CREATE TABLE [CC].[HRG45Encounter] (
    [EncounterRecno]   INT          NOT NULL,
    [HRGCode]          VARCHAR (10) NULL,
    [CriticalCareDays] SMALLINT     NULL,
    [WarningCode]      VARCHAR (10) NULL,
    [Created]          DATETIME     NULL,
    [SourceSpellNo]    VARCHAR (20) NULL,
    CONSTRAINT [PK_CC_HRG45Encounter] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);

