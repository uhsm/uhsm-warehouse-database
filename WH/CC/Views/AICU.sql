﻿


CREATE view [CC].[AICU] as

--select
--	 CriticalCareUnitFunctionCode
--	,BasicCardiovascularSupportDays
--	,AdvancedCardiovascularSupportDays
--	,BasicRespiratorySupportDays
--	,AdvancedRespiratorySupportDays
--	,RenalSupportDays
--	,NeurologicalSupportDays
--	,DermatologicalSupportDays
--	,LiverSupportDays
--	,Level2SupportDays
--	,Level3SupportDays
--	,StartDate
--	,EndDate
--	,CriticalCareLocalIdentifier
--	,SourceSpellNo
--	,DischargeDate
--from
--	CC.AICU
--where SourceSpellNo='150381152'

select 
	CriticalCareUnitFunctionCode = CCP.UnitFunctionCode
	,BasicCardiovascularSupportDays = CCP.[Basic Cardiovascular Support Days]
	,AdvancedCardiovascularSupportDays = CCP.[Advanced Cardiovascular Support Days]
	,BasicRespiratorySupportDays = CCP.[Basic Respiratory Support Days]
	,AdvancedRespiratorySupportDays = CCP.[Advanced Respiratory Support Days]
	,RenalSupportDays = CCP.[Renal Support Days]
	,NeurologicalSupportDays = CCP.[Neurological Support Days]
	,DermatologicalSupportDays = CCP.[Dermatological Support Days]
	,LiverSupportDays = CCP.[Liver Support Days]
	,AllSupportDays = CCP.AllSupported
	,Level2SupportDays = CCP.[HDU Days]
	,Level3SupportDays = CCP.[ICU Days]
	,StartDate = REPLACE(CONVERT(VARCHAR(10), CCP.CriticalCareStartDatetime, 102),'.','')
	,EndDate = REPLACE(CONVERT(VARCHAR(10), CCP.CriticalCareEndDatetime, 102),'.','')
	,CriticalCareLocalIdentifier = CCP.SourceCCPNo
	,CCP.SourceSpellNo
	,DischargeDate = REPLACE(CONVERT(VARCHAR(10), Spell.DischargeDate, 102),'.','')
from WHREPORTING.APC.CriticalCarePeriod CCP
left join WHREPORTING.APC.Spell Spell
on Spell.SourceSpellNo = CCP.SourceSpellNo
inner join CC.AICUOld CCPICU
on CCPICU.CriticalCareLocalIdentifier = CCP.SourceCCPNo
--where CCP.SourceSpellNo='150381152'



