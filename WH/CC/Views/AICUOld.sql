﻿
CREATE view [CC].[AICUOld] as

select
	 CriticalCareUnitFunctionCode =
		CriticalCareUnitFunctionCode.MappedCode

	,BasicCardiovascularSupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayOrganSupported in (2005289, 2005514)	--	Basic Cardiovascular Support
			then 1
			else 0
			end
		)

	,AdvancedCardiovascularSupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayOrganSupported in (2005288, 2005513)	--	Advanced Cardiovascular Support
			then 1
			else 0
			end
		)

	,BasicRespiratorySupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayOrganSupported = 8865	--	Basic Respiratory Support
			then 1
			else 0
			end
		)

	,AdvancedRespiratorySupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayOrganSupported = 8864	--	Advanced Respiratory Support
			then 1
			else 0
			end
		)

	,RenalSupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayOrganSupported = 8868	--	Renal Support
			then 1
			else 0
			end
		)

	,NeurologicalSupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayOrganSupported = 8867	--	Neurological Support
			then 1
			else 0
			end
		)

	,DermatologicalSupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayOrganSupported in (2005291, 2005515)	--	Dermatological Support
			then 1
			else 0
			end
		)

	,LiverSupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayOrganSupported = 2005290	--	Liver Cardiovascular Support
			then 1
			else 0
			end
		)

	,Level2SupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayCareLevel = 2
			then 1
			else 0
			end
		)

	,Level3SupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayCareLevel = 3
			then 1
			else 0
			end
		)

	,StartDate =
		min(convert(varchar, CriticalCarePeriod.CCPStartDate, 112))

	,EndDate =
		max(convert(varchar, CriticalCarePeriod.CCPEndDate, 112))

	,CriticalCareLocalIdentifier = CriticalCarePeriod.SourceCCPNo
	,CriticalCarePeriod.SourceSpellNo

	,DischargeDate =
		max(convert(varchar, Encounter.DischargeDate, 112))

	--,CirculatorySupportDays =
	--	SUM(
	--		case
	--		when CriticalCarePeriod.CCPStayOrganSupported = 8866	--	Circulatory Support
	--		then 1
	--		else 0
	--		end
	--	)


	--,GastrointestinalSupportDays =
	--	SUM(
	--		case
	--		when CriticalCarePeriod.CCPStayOrganSupported in (2005288, 2005517)	--	Gastrointestinal Support
	--		then 1
	--		else 0
	--		end
	--	)

	--,EnhancedNursingCareSupportDays =
	--	SUM(
	--		case
	--		when CriticalCarePeriod.CCPStayOrganSupported = 2005516	--	Enhanced Nursing Care Support
	--		then 1
	--		else 0
	--		end
	--	)

	--,HepaticSupportDays =
	--	SUM(
	--		case
	--		when CriticalCarePeriod.CCPStayOrganSupported = 2005288	--	Hepatic Support
	--		then 1
	--		else 0
	--		end
	--	)

from
	APC.CriticalCarePeriod

inner join PAS.ReferenceValue CriticalCareUnitFunctionCode
on	CriticalCareUnitFunctionCode.ReferenceValueCode = CriticalCarePeriod.CCPUnitFunction
and	CriticalCareUnitFunctionCode.MainCode not in ('04','16','17','18','19','92') --remove paediatric

inner join PAS.ServicePoint
on	ServicePoint.ServicePointCode = CriticalCarePeriod.CCPStayWard

inner join dbo.EntityXref WardServiceMap
on	WardServiceMap.EntityTypeCode = 'WARD'
and	WardServiceMap.XrefEntityTypeCode = 'AICUSERVICE'
and	WardServiceMap.EntityCode = ServicePoint.ServicePointLocalCode

inner join APC.Encounter
on	Encounter.EpisodeNo = 1
and	Encounter.SourceSpellNo = CriticalCarePeriod.SourceSpellNo

where
	CriticalCarePeriod.CCPStayCareLevel is not null
and	CriticalCarePeriod.CCPBedStay = 1
and	Encounter.DischargeDate is not null

group by
	 CriticalCarePeriod.SourceCCPNo
	,CriticalCarePeriod.SourceSpellNo
	,WardServiceMap.XrefEntityCode
	,CriticalCareUnitFunctionCode.MappedCode

