﻿



CREATE view [CC].[AICUOld2] as

select
	-- CriticalCareUnitFunctionCode = CriticalCareUnitFunctionCode.MappedCode
	--,Level2SupportDays = SUM(case when CriticalCarePeriod.CCPStayCareLevel = 2 then 1 else 0 end)
	--,Level3SupportDays = SUM(case when CriticalCarePeriod.CCPStayCareLevel = 3 then 1 else 0 end)
	--,StartDate = min(convert(varchar, CriticalCarePeriod.CCPStartDate, 112))
	--,EndDate = max(convert(varchar, CriticalCarePeriod.CCPEndDate, 112))
 	CriticalCareLocalIdentifier = CriticalCarePeriod.SourceCCPNo
	--,CriticalCarePeriod.SourceSpellNo
 -- 	,DischargeDate = max(convert(varchar, Encounter.DischargeDate, 112))
from
	APC.CriticalCarePeriod

inner join PAS.ReferenceValue CriticalCareUnitFunctionCode
on	CriticalCareUnitFunctionCode.ReferenceValueCode = CriticalCarePeriod.CCPUnitFunction
and	CriticalCareUnitFunctionCode.MainCode not in ('04','16','17','18','19','92') --remove paediatric

inner join PAS.ServicePoint
on	ServicePoint.ServicePointCode = CriticalCarePeriod.CCPStayWard

inner join dbo.EntityXref WardServiceMap
on	WardServiceMap.EntityTypeCode = 'WARD'
and	WardServiceMap.XrefEntityTypeCode = 'AICUSERVICE'
and	WardServiceMap.EntityCode = ServicePoint.ServicePointLocalCode

inner join APC.Encounter
on	Encounter.EpisodeNo = 1
and	Encounter.SourceSpellNo = CriticalCarePeriod.SourceSpellNo

where
	CriticalCarePeriod.CCPStayCareLevel is not null
and	CriticalCarePeriod.CCPBedStay = 1
and	Encounter.DischargeDate is not null

group by
 	CriticalCarePeriod.SourceCCPNo


