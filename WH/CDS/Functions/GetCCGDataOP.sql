﻿-- =============================================
-- Author:		K Oakden
-- Create date: 15-05-2013
-- Description:	To generate CCG data for CDS deataset, based on source unique id
-- Usage: select HA from CDS.GetCCGData('BRM200000192599550')
-- =============================================
CREATE FUNCTION [CDS].[GetCCGDataOP]
(
	@cdsid varchar(20)
)
RETURNS  
   @CCGData TABLE ( 
		SourceUniqueID int
		,OverseasStatus varchar(2)
		,PrimeRecipient varchar(5) 
		,PurchaserBase varchar(5)  
		,Purchaser varchar(5)  
		,HABase varchar(5)
		,HA varchar(5)
		,Comments varchar(20)
   ) 

AS
BEGIN

declare @suid varchar(20)
declare @exists int

select @suid  = right(@cdsid, charindex('1', @cdsid, 1) - 1)

insert into @CCGData (
	SourceUniqueID
	,OverseasStatus
	,PurchaserBase
	,HABase
       )
select 
	@suid
	,OverseasStatus.MappedCode
	--,PurchaserCodeCCG =
	,case 
			when OverseasStatus.MappedCode in (1,2)
				then '01N00'	--OS - Exempt from charges
			when OverseasStatus.MappedCode in (3,4)
				then 'VPP00'	--OS - Liable for charges
			when Encounter.Postcode like 'IM%'
				then 'YAC00'	--IOM patients don't seem to have overseas status code, so hard-code YAC here to
								--avoid purchaser being pulled from iPM Purchaser (usually TDH00)
			--IPM purchaser data is not correct, so use CCG of practice, unless
			--non English, private or overseas patient
			when 
				(left(
					coalesce(
						Purchaser.PurchaserMainCode
						,ODSPractice.ParentOrganisationCode
						,PCT.OrganisationLocalCode)
					, 1) in ('Z','Y','V','S','7') or
				left(Purchaser.PurchaserMainCode, 2) = 'TD')
				then 
					coalesce(
						left(Purchaser.PurchaserMainCode + '00', 5)
						,left(ODSPractice.ParentOrganisationCode + '00', 5)
						,left(PCT.OrganisationLocalCode + '00', 5)
					)
		else
			left(ODSPractice.ParentOrganisationCode + '00', 5)
		end
		
	--,CCGofResidenceCode = 
	,case when OverseasStatus.MappedCode in
			(
			 1	-- Exempt (reciprocal agreement)
			,2	-- Exempt from payment - other
			,3	-- To pay hotel fees only
			,4	-- To pay all fees
			,9	-- Charging rate not known
			)
		then 'X98'

		--For Scotland, NI and CI (NOT WALES) use DistrictOfResidence as lookup instead of PCTCode
		when left(PostcodeCCG.DistrictOfResidence,1) in ('S','Z','Y') then PostcodeCCG.DistrictOfResidence
		
		else
			--Lookup to ODS postcode_to_PCT table
			PostcodeCCG.CCGCode
		end
from OP.Encounter Encounter
left join PAS.ReferenceValue OverseasStatus
	on OverseasStatus.ReferenceValueCode = Encounter.OverseasStatusFlag
	
left join OrganisationCCG.dbo.Postcode PostcodeCCG
	on	PostcodeCCG.Postcode =
		case
		when len(Encounter.Postcode) = 8 then Encounter.Postcode
		else left(Encounter.Postcode, 3) + ' ' + right(Encounter.Postcode, 4) 
		end
	
left join PAS.Purchaser
	on    Purchaser.PurchaserCode = Encounter.PurchaserCode

left join PAS.Organisation IPMPractice
	on	IPMPractice.OrganisationCode = Encounter.EpisodicGpPracticeCode
	
left join PAS.Organisation PCT
	on	PCT.OrganisationCode = IPMPractice.ParentOrganisationCode
	
left join OrganisationCCG.dbo.Practice ODSPractice
	on ODSPractice.OrganisationCode = IPMPractice.OrganisationLocalCode
where SourceUniqueID = @suid

update @CCGData set HA = 
	case 		
		--Where no Postcode or GP then organisation code for unknown practice 'V81999' maps to 'SHA'
		--Change this to 'Q99'
		when HABase = 'SHA' then 'Q99'
		--Wales, Scotland, NI, CI
		when left(HABase, 1) in ('7','S','Y', 'Z', '6') then 'X98'
		else Coalesce(HABase, 'Q99')
	end
from  @CCGData 		
		
update @CCGData set Purchaser = 
		case when PurchaserBase = 'SHA00'
			then Coalesce(
			case when HABase = 'X98' then '01N00' end
			,left(HABase + '00', 5)
			,'01N00')
		else Coalesce(
			PurchaserBase
			,case when HABase = 'X98' then '01N00' end
			,left(HABase + '00', 5)
			,'01N00')
		end
from  @CCGData 		
		
update @CCGData set PrimeRecipient = 
	case
	when OverseasStatus in ('1','2') then 'TDH00'
	--when OverseasStatus in ('3','4','9') then 'VPP00'
	when left(HABase, 1) = 'Y' then 'TDH00'
	when HABase in ('X98', 'Q99', 'SHA') then 
		coalesce(
			case when PurchaserBase = 'SHA00' then '01N00' else PurchaserBase end
			--PurchaserCodeCCG 
			, '01N00'
			)
	else
		coalesce(
			 left(HABase, 3) + '00'
			,case when PurchaserBase = 'SHA00' then '01N00' else PurchaserBase end
			--,PurchaserCodeCCG 
			,'01N00'
			)
	end
from  @CCGData 

		
select @exists = count(1) from @CCGData 

if @exists = 0 
--No Records Match Criteria 
insert into @CCGData (Comments) values ('Not found') 

return 

END
