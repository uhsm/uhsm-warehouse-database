﻿
/******************************************************************************
 Description: Get list of Epsidoes to process for the APC CDS extract.
  
 Parameters:  @CDSProtocolIdentifierCode - 010 = Net change or 020 = Bulk replacement
              
              The ReportPeriod is used by both protocols and is used to 
              define the period for activity to be included. This could be two
              months for bulk or a complete financial year for net change.
              
              The ModifiedPeriod is only required for the Net change protocol
              and is used to define the period for created, amended or 
              deleted activity.  The time must be used e.g. 00:00:00 at the 
              start and 23:59:59 at the end.
              
 Modification History --------------------------------------------------------

 Date     Author      Change
 28/01/14 Tim Dean	  Created

******************************************************************************/

CREATE Procedure [CDS].[APCGetEpisodesToProcess]
 @CDSProtocolIdentifierCode CHAR(3)
 ,@ReportPeriodStartDate DATE
 ,@ReportPeriodEndDate DATE
 ,@ModifiedPeriodStartDateTime DATETIME
 ,@ModifiedPeriodEndDateTime DATETIME
AS

SET NOCOUNT ON

DECLARE @ExtractDateTime DATETIME = GETDATE()

TRUNCATE TABLE CDS.APCEpisodesToProcess

INSERT INTO CDS.APCEpisodesToProcess
            (EpisodeSourceUniqueID,
             BirthNotificationID,
             DeliveryBirthNotificationID,
             CDSProtocolIdentifierCode,
             ReportPeriodStartDate,
             ReportPeriodEndDate,
             ModifiedPeriodStartDateTime,
             ModifiedPeriodEndDateTime,
             ExtractDateTime)
SELECT Episode.SourceUniqueID,
       Birth.BirthNotificationID,
       Delivery.BirthNotificationID,
       @CDSProtocolIdentifierCode,
       @ReportPeriodStartDate,
       @ReportPeriodEndDate,
       @ModifiedPeriodStartDateTime,
       @ModifiedPeriodEndDateTime,
       @ExtractDateTime
FROM   APC.Encounter Episode
       -- Join the original Lorenzo episode data, so that the archive flag can be checked *****************************
       INNER JOIN Lorenzo.dbo.ProfessionalCareEpisode
               ON ( Episode.SourceUniqueID = ProfessionalCareEpisode.PRCAE_REFNO )
       -- Join the latest Evolution birth notifcation for the birth that matches the episode ***************************
       LEFT JOIN CDS.MaternityBirthNotification Birth
              ON ( Birth.BirthNotificationID = (SELECT TOP 1 BirthLookup.BirthNotificationID
                                                FROM   CDS.MaternityBirthNotification BirthLookup
                                                WHERE  BirthLookup.BirthSourceUniqueID = Episode.SourceUniqueID
                                                ORDER  BY BirthLookup.ModifiedDtm DESC) )
       -- Join the latest Evolution birth notification for the delivery that matches the epside ************************
       LEFT JOIN CDS.MaternityBirthNotification Delivery
              ON ( Delivery.BirthNotificationID = (SELECT TOP 1 DeliveryLookup.BirthNotificationID
                                                   FROM   CDS.MaternityBirthNotification DeliveryLookup
                                                   WHERE  DeliveryLookup.DeliverySourceUniqueID = Episode.SourceUniqueID
                                                   ORDER  BY DeliveryLookup.ModifiedDtm DESC) )
WHERE  Episode.EpisodeEndDate BETWEEN @ReportPeriodStartDate AND @ReportPeriodEndDate -- The activity must be within the reporting period.
       -- If net change protocol then check to see if the activity has been modified within the required modified period.
       -- Ignore for bulk replacement protocol.
       AND ( ( @CDSProtocolIdentifierCode = '010'
               AND ( ProfessionalCareEpisode.MODIF_DTTM BETWEEN @ModifiedPeriodStartDateTime AND @ModifiedPeriodEndDateTime
                      OR Delivery.ModifiedDtm BETWEEN @ModifiedPeriodStartDateTime AND @ModifiedPeriodEndDateTime
                      OR Birth.ModifiedDtm BETWEEN @ModifiedPeriodStartDateTime AND @ModifiedPeriodEndDateTime ) )
              OR @CDSProtocolIdentifierCode = '020' )
       -- If bulk replacement protocol then exclude deleted activity.
       -- If net change protocol then include deleted activity.
       AND ( ( @CDSProtocolIdentifierCode = '020'
               AND ProfessionalCareEpisode.ARCHV_FLAG = 'N' )
              OR ( @CDSProtocolIdentifierCode = '010'
                   AND ProfessionalCareEpisode.ARCHV_FLAG IN ( 'N', 'Y' ) ) ) 


