﻿




/***********************************************************************************
 Description: 
 
 Modification History --------------------------------------------------------

 Date     Author      Change
 03/03/14 Tim Dean	  Created.
************************************************************************************/

CREATE PROCEDURE [CDS].[APCTranslateColumn]
  @DestinationTable VARCHAR(250),
  @DestinationColumn VARCHAR(100),
  @Cardinality INT,
  @SourceExpression VARCHAR(4000),
  @SourceTable VARCHAR(4000),
  @SourceFilter VARCHAR(4000)
AS

SET NOCOUNT ON;

DECLARE @Sql NVARCHAR(MAX);

SET @Sql = 'UPDATE ' + @DestinationTable;

SET @Sql = @Sql + ' SET ' + @DestinationColumn;

IF @Cardinality > 0
BEGIN
  SET @Sql = @Sql + CAST(@Cardinality AS VARCHAR(2));
END

SET @Sql = @Sql + ' = ' + @SourceExpression; 

SET @Sql = @Sql + ' FROM ' + @SourceTable;

IF LEN(@SourceFilter) > 0 
BEGIN
  SET @Sql = @Sql + ' WHERE ' + @SourceFilter;
END

EXEC sp_executesql @Sql, N'@Cardinality VARCHAR(2)',@Cardinality = @Cardinality;

--PRINT @Sql;




