﻿
/*
--Author: K Oakden
--Date created: 12TH October 2012
--
--SP does additional processing on APC CDS extract
*/
CREATE procedure [CDS].[AmendAPCBaseArdentia] as

declare @StartTime datetime
declare @RowsForExport Int
declare @RowsDeleted Int
declare @Stats varchar(255)

select @StartTime = getdate()

----------------------------------------------------------------------------
--PRINT 'Record count'
--Complete address fields where no fixed abode
update CDS.APCBaseArdentia set 
	  PatientAddress2 = 'Unknown Address',
	  PatientAddress3 = 'Unknown Address',
	  PatientAddress4 = 'Unknown Address'
where len(PatientAddress1) > 1 and len(PatientAddress2) = 0 and len(PatientAddress3) = 0

update CDS.APCBaseArdentia set 
	  PatientAddress1 = 'Unknown Address',
	  PatientAddress3 = 'Unknown Address',
	  PatientAddress4 = 'Unknown Address'
where len(PatientAddress1) = 0 and len(PatientAddress2) > 1 and len(PatientAddress3) = 0


update CDS.APCBaseArdentia set
	ProcedureSchemeInUse = null
	,OperationStatus = 8
where OperationStatus = 1 and ProcedureSchemeInUse = '02' and OperationCode1 is null


update CDS.APCBaseArdentia set
	ProcedureSchemeInUse = '02'
	,OperationStatus = 1
where ProcedureSchemeInUse is null and OperationCode1 is not null and OperationStatus = 8


update CDS.APCBaseArdentia
set MotherNNNStatusIndicator = '04'
where MotherNNNStatusIndicator = '01' and MotherNHSNumber = ''

update CDS.APCBaseArdentia set 
	  MotherPatientAddress2 = 'Unknown Address',
	  MotherPatientAddress3 = 'Unknown Address',
	  MotherPatientAddress4 = 'Unknown Address'
where len(MotherPatientAddress1) > 1 and len(MotherPatientAddress2) = 0 and len(MotherPatientAddress3) = 0
and MotherNNNStatusIndicator<>'01' and CDSType = '120'


--Home births
update CDS.APCBaseArdentia
SET CDSType = '150',
CDSGroup = '040'
from CDS.APCBaseArdentia base 
inner join APC.Encounter encounter on base.EpisodeSourceUniqueID = encounter.SourceUniqueID
left join PAS.ServicePointBase SWard
	on encounter.StartWardTypeCode = SWard.SPONT_REFNO
	where SWard.CODE = 'HOM'
	and base.CDSType = '120'

--Home deliveries
update CDS.APCBaseArdentia 
SET CDSType = '160',
CDSGroup = '030'
from CDS.APCBaseArdentia base 
inner join APC.Encounter encounter on base.EpisodeSourceUniqueID = encounter.SourceUniqueID
left join PAS.ServicePointBase SWard
	on encounter.StartWardTypeCode = SWard.SPONT_REFNO
	where SWard.CODE = 'HOM'
	and base.CDSType = '140'


--Treatment Function
update CDS.APCBaseArdentia set
TreatmentFunctionCode = '100'
where TreatmentFunctionCode = '103A'

--update CDS.APCBaseArdentia set
--MotherNHSNumber = null,
--MotherLocalPatientID = Null,
--MotherLocalPatientIDOrganisationCode = null,
--MotherNNNStatusIndicator = null,
--MotherPatientAddress1 = null,
--MotherPatientAddress2 = null,
--MotherPatientAddress3 = null,
--MotherPatientAddress4 = null,
--MotherPatientAddress5 = null,
--MotherPostcode = null
--where CDSType = '140'

PRINT 'DELETING NON APPLICABLE PATIENTS...'
DELETE 
FROM CDS.APCBaseArdentia WHERE NHSNumber = '4722504253'

select @RowsDeleted = @@rowcount
---------------------------------------------------------------------------------------------------------
--REMOVE TEST PATIENTS
---------------------------------------------------------------------------------------------------------
--select *
DELETE 
FROM CDS.APCBaseArdentia WHERE NHSNumber LIKE '999%'
select @RowsDeleted = @RowsDeleted + @@rowcount
--select *
DELETE 
FROM CDS.APCBaseArdentia WHERE PatientSurname LIKE '%XTEST%'
select @RowsDeleted = @RowsDeleted + @@rowcount

---------------------------------------------------------------------------------------------------------
-- LOCAL PATIENT IDENTIFIER ID ORGANISATION
---------------------------------------------------------------------------------------------------------
PRINT 'UPDATING PROVIDER CODE...'
UPDATE CDS.APCBaseArdentia
SET ProviderCode = 'RM200'
--SELECT CDSType, UniqueEpisodeSerialNo, ProviderCode, LocalPatientID
--FROM CDS.APCBaseArdentia
WHERE CDSType IN ('120','140','130','150','160')
AND NOT (LocalPatientID IS NULL OR LocalPatientID = '')



--------------------------------------------------------------------------------------------------------
--UPDATE MATERNITY TAILS GESTATION INFORMATION
---------------------------------------------------------------------------------------------------------

-- UPDATE GESTAT APPLIES TO BIRTH AND DELIVERY CDS TYPES
update CDS.APCBaseArdentia
SET GestationLength = '99'
--SELECT CDSType, UniqueEpisodeSerialNo, GestationLength
--FROM CDS.APCBaseArdentiaTest
WHERE 
CDSType IN ('120','140','150','160')
AND (GestationLength IS NULL OR GestationLength = '' OR GestationLength = '0')

-- UPDATE GESTATON 1 APPLIES TO BIRTH AND DELIVERY CDS TYPES
UPDATE CDS.APCBaseArdentia
SET Birth1AssessmentGestationLength = GestationLength
--SELECT CDSType, UniqueEpisodeSerialNo, GestationLength, Birth1AssessmentGestationLength
--FROM CDS.APCBaseArdentiaTest
WHERE 
CDSType IN ('120','140','150','160')
AND (Birth1AssessmentGestationLength IS NULL OR Birth1AssessmentGestationLength = '' OR Birth1AssessmentGestationLength = '0')


-- UPDATE GESTATON 2 APPLIES TO DELIVERY CDS TYPES
UPDATE CDS.APCBaseArdentia
SET Birth2AssessmentGestationLength = GestationLength
--SELECT CDSType, UniqueEpisodeSerialNo, GestationLength, Birth2AssessmentGestationLength, Birth2DateOfBirth
--FROM CDS.APCBaseArdentiaTest
WHERE 
CDSType IN ('140','160')
AND Birth2DateOfBirth IS NOT NULL
AND (Birth2AssessmentGestationLength IS NULL OR Birth2AssessmentGestationLength = '' OR Birth2AssessmentGestationLength = '0')





---------------------------------------------------------------------------------------------------------
-- UPDATE MATERNITY TAILS DELIVERY DATE
-- Due to a limitation on iPM some deliveries have to be entered with a date of 01/01/<year>
-- most of them will be tidied up with the processes above, but some may still remain
-- so the delivery date is assumed to be the birth date of the first baby
---------------------------------------------------------------------------------------------------------

UPDATE CDS.APCBaseArdentia
SET DeliveryDate = Birth1DateOfBirth
--select CDSType, UniqueEpisodeSerialNo, DeliveryDate, Birth1DateOfBirth
--from CDS.APCBaseArdentia
WHERE CDSType IN ('140','160','150')
AND 
RIGHT(DeliveryDate,5) = '01-01' 

---------------------------------------------------------------------------------------------------------
-- BABY LOCAL PATIENT IDENTIFIER
---------------------------------------------------------------------------------------------------------

update CDS.APCBaseArdentia
SET Birth1LocalPatientIdentifier = LocalPatientID
--select CDSType, UniqueEpisodeSerialNo, Birth1LocalPatientIdentifier, LocalPatientID
--from CDS.APCBaseArdentia
where CDSType = '120'
and 
(
Birth1LocalPatientIdentifier is null or 
Birth1LocalPatientIdentifier = ''
)

--select @RowsForExport =  COUNT(1) from CDS.APCArdentiaExport
declare @CommissionerType varchar(5)
select @CommissionerType = TextValue from dbo.Parameter where Parameter = 'CDSCOMMISSIONERTYPE'

if(@CommissionerType = 'CCG')
	begin
		select @RowsForExport =  COUNT(1) from CDS.APCArdentiaExportCCG
	end
else
	begin
		select @RowsForExport =  COUNT(1) from CDS.APCArdentiaExportPCT
	end
	
select @Stats =  'Rows deleted = '  + CONVERT(varchar(10), @RowsDeleted) 
	+ '. Rows for export = ' + CONVERT(varchar(10), @RowsForExport)

exec WriteAuditLogEvent 'CDS - WH AmendAPCBaseArdentia', @Stats, @StartTime

--print @Stats