﻿



/*
--Author: K Oakden
--Date created: 12TH October 2012
--
--SP does additional processing on OP CDS extract
*/
CREATE procedure [CDS].[AmendOPBaseArdentia] as
--declare @result int
--declare @textout varchar(8000)
--declare @sql varchar(8000)

declare @StartTime datetime
declare @RowsForExport Int
declare @RowsDeleted Int
declare @Stats varchar(255)

select @StartTime = getdate()
----------------------------------------------------------------------------
--PRINT 'Record count'
--select COUNT(*) from CDS.OPBaseArdentia;
----select @result = @@rowcount
----select @textout = 'Count CDS.OPBaseArdentia = ' + CAST(@result AS varchar

----SELECT @sql = 'bcp "COUNT(1) from CDS.OPBaseArdentia;" queryout c:\temp\filename.txt -c -t, -T -S' + @@servername
----exec master..xp_cmdshell @sql

--select COUNT(*) from CDS.OPArdentiaExport;
----------------------------------------------------------------------------
--PRINT 'Reporting period'
--select * from dbo.Parameter where Parameter like 'CDSREPORTPERIOD%'

--select MIN(CDSActivityDate), MAX(CDSActivityDate) from CDS.OPBaseArdentia
----------------------------------------------------------------------------
PRINT 'Tidy RTT status data'
--Remove RTTPeriodStatusCode where no other RTT details exist

--select top 1000 * from CDS.OPBaseArdentia
--where RTTPeriodStatusCode = 99 and (RTTStartDate is null or RTTPathwayID is null)
update CDS.OPBaseArdentia set 
	RTTPeriodStatusCode = null
where RTTPeriodStatusCode = 99 and (RTTStartDate is null or RTTPathwayID is null)

--Remove Other RTT data where there is no RTTPathwayID

--select * from CDS.OPBaseArdentia
--where RTTPathwayID is null
update CDS.OPBaseArdentia set 
	RTTCurrentProviderCode = null
	,RTTPeriodStatusCode = null
	,RTTStartDate = null
	,RTTEndDate = null
where RTTPathwayID is null
----------------------------------------------------------------------------
--PRINT 'Get missing patient demographics'
--update with data from old run (only where NHSNO verified)

--SELECT mp.UniqueEpisodeSerialNo, 
--	mp.PatientRefNo,
--	kd.NHS_NO, 
--	kd.NHS_NO_STATUS, 
--	kd.LOCPATID, 
--	kd.POSTCODE,
--	kd.HA,
--	kd.SEX,
--	kd.CAR_SUP_IND,
--    kd.DOB,
--    kd.GPREG,
--    kd.REGPRACT,
--    kd.AGE_AT_CDS_ACTIVITY_DATE,
--	'OP' as CDSRUN
--FROM CDS.vwOPCDSMissingPat mp
--left outer join
--CDSValidation.dbo.vwCDSExportOPV611 kd 
--on mp.UniqueEpisodeSerialNo = kd.CDS_ID
--order by kd.LOCPATID

--merge CDS.OPBaseArdentia opdata
--using
--(
--SELECT mp.UniqueEpisodeSerialNo, 
--	kd.NHS_NO, 
--	kd.NHS_NO_STATUS, 
--	kd.LOCPATID, 
--	kd.POSTCODE,
--	kd.HA,
--	kd.SEX,
--	--kd.CAR_SUP_IND,
--    kd.DOB,
--    kd.GPREG,
--    kd.REGPRACT,
--    kd.AGE_AT_CDS_ACTIVITY_DATE
--FROM CDS.vwOPCDSMissingPat mp
--inner join
--CDSValidation.dbo.vwCDSExportOPV611 kd 
--on mp.UniqueEpisodeSerialNo = kd.CDS_ID
--where kd.NHS_NO_STATUS = '01'
--) kddata
--on (opdata.UniqueEpisodeSerialNo = kddata.UniqueEpisodeSerialNo)
--WHEN MATCHED THEN
--UPDATE
--	SET
--		NHSNumber = kddata.NHS_NO
--	    ,NNNStatusIndicator = kddata.NHS_NO_STATUS
--	    ,LocalPatientID = kddata.LOCPATID
--	    ,Postcode = kddata.POSTCODE
--	    ,PCTofResidenceCode = kddata.HA
--		,SexCode = kddata.SEX
--		,DateOfBirth = kddata.DOB
--		,RegisteredGpCode = kddata.GPREG
--		,RegisteredGpPracticeCode = kddata.REGPRACT
--        ,AgeAtCDSActivityDate = kddata.AGE_AT_CDS_ACTIVITY_DATE
--        ;


----------------------------------------------------------------------------
PRINT 'Get rid of extraneous NSP codes in Source of Referral'

--select * from CDS.OPBaseArdentia
--where SourceOfReferralCode in (20, 21)
update CDS.OPBaseArdentia set SourceOfReferralCode = 97
where SourceOfReferralCode in (20, 21)


----------------------------------------------------------------------------
PRINT 'Tidy address data'
----Complete address fields where no fixed abode
--select * from CDS.OPBaseArdentia
--where PatientAddress1 = 'No Fixed Abode'

--select * from CDS.OPBaseArdentia
--where len(PatientAddress1) > 1 and len(PatientAddress2) = 0 and len(PatientAddress3) = 0
  update CDS.OPBaseArdentia set 
	  PatientAddress2 = 'Unknown Address',
	  PatientAddress3 = 'Unknown Address',
	  PatientAddress4 = 'Unknown Address'
where len(PatientAddress1) > 1 and len(PatientAddress2) = 0 and len(PatientAddress3) = 0


----------------------------------------------------------------------------
PRINT 'Tidy Treatment Function Code'

--select * from CDS.OPArdentiaExport 
--where CON_SPEC = '950'
--order by ClinicCode
----------------------------
--KO temp 17/12/12
--select * from CDS.OPBaseArdentia where MainSpecialtyCode is null


--select * from CDS.OPBaseArdentia where TreatmentFunctionCode = '960'
--select * from CDS.OPBaseArdentia where ClinicCode = 'HANDPT5'
-----------------------------

update CDS.OPBaseArdentia set 
	MainSpecialtyCode = '960'
	,TreatmentFunctionCode = '650'
where TreatmentFunctionCode in ('9044','9046')

update CDS.OPBaseArdentia set TreatmentFunctionCode = '300'
where TreatmentFunctionCode like '950%'
and ClinicCode in ('WYTHSMOCP','WCHSMOCP')

update CDS.OPBaseArdentia set TreatmentFunctionCode = '103'
where TreatmentFunctionCode like '950%'
and ClinicCode in ('BREPOSTOP')

update CDS.OPBaseArdentia set TreatmentFunctionCode = '315'
where TreatmentFunctionCode like '950%'
and ClinicCode in ('PAPAFU')

update CDS.OPBaseArdentia set TreatmentFunctionCode = '140'
where TreatmentFunctionCode = '950'
and ClinicCode = ('HYGMAM')

update CDS.OPBaseArdentia set TreatmentFunctionCode = '654'
where TreatmentFunctionCode = '960'
and ClinicCode = ('DIET1R')

update CDS.OPBaseArdentia set TreatmentFunctionCode = MainSpecialtyCode
where TreatmentFunctionCode is null

update CDS.OPBaseArdentia set MainSpecialtyCode = TreatmentFunctionCode
where MainSpecialtyCode is null

update CDS.OPBaseArdentia set TreatmentFunctionCode = '650'
where TreatmentFunctionCode = '960'

--{16/05/2016 ADDED TJD - 343 is not a valid main specialty code.}
UPDATE CDS.OPBaseArdentia
SET MainSpecialtyCode = '340'
WHERE MainSpecialtyCode = '343';


----------------------------------------------------------------------------
PRINT 'Exclude / Map selected clinics'

--select * from CDS.OPBaseArdentia
--where upper(ClinicCode) in (
--select upper(ClinicCode) from CDS.OPExcludedClinics   where [Action] = 'Exclude'
--)

delete from CDS.OPBaseArdentia
where upper(ClinicCode) in (
select upper(ClinicCode) from CDS.OPExcludedClinics   where [Action] = 'Exclude'
)

select @RowsDeleted = @@rowcount

update CDS.OPBaseArdentia set TreatmentFunctionCode = '174'
where upper(ClinicCode) in (
select upper(ClinicCode) from CDS.OPExcludedClinics   where [Action] = '174'
)


----------------------------------------------------------------------------
PRINT 'First Attendance Flag needs updating for telephone appointments'
---- First attendance flag
--select 
--	Base.SourceUniqueID
--	,Encounter.FirstAttendanceFlag as ENC_FAF_CODE
--	,FirstAttendance.MappedCode as FA_MAPPED_CODE
--	,Base.FirstAttendanceFlag as CDS_FAF
--	,s.CMDIA_REFNO
--	,s.VISIT_REFNO
--	,Case when (
--			s.CMDIA_REFNO ='2005362' --Telephone
--			and FirstAttendance.MappedCode ='2')
--		then '4'
--		else FirstAttendance.MappedCode
--		end as 'NewFirstAttendanceFlag'
--from CDS.OPBaseArdentia Base
--inner join OP.Encounter Encounter on Base.SourceUniqueID = Encounter.SourceUniqueID
--left join Lorenzo.dbo.ScheduleEvent s
--on base.SourceUniqueID = s.SCHDL_REFNO
--left join PAS.ReferenceValue FirstAttendance
--	on    FirstAttendance.ReferenceValueCode = Encounter.FirstAttendanceFlag
--where s.CMDIA_REFNO ='2005362'
--and FirstAttendance.MappedCode in ('1','2')
----where Base.SourceUniqueID = 188189582

update CDS.OPBaseArdentia
set FirstAttendanceFlag = 4
from CDS.OPBaseArdentia Base
inner join OP.Encounter Encounter on Base.SourceUniqueID = Encounter.SourceUniqueID
left join Lorenzo.dbo.ScheduleEvent s
on base.SourceUniqueID = s.SCHDL_REFNO
left join PAS.ReferenceValue FirstAttendance
	on    FirstAttendance.ReferenceValueCode = Encounter.FirstAttendanceFlag
where s.CMDIA_REFNO ='2005362'
and FirstAttendance.MappedCode ='2'

update CDS.OPBaseArdentia
set FirstAttendanceFlag = 3
from CDS.OPBaseArdentia Base
inner join OP.Encounter Encounter on Base.SourceUniqueID = Encounter.SourceUniqueID
left join Lorenzo.dbo.ScheduleEvent s
on base.SourceUniqueID = s.SCHDL_REFNO
left join PAS.ReferenceValue FirstAttendance
	on    FirstAttendance.ReferenceValueCode = Encounter.FirstAttendanceFlag
where s.CMDIA_REFNO ='2005362'
and FirstAttendance.MappedCode ='1'


----------------------------------------------------------------------------
PRINT 'Tidy commissioning serial number'

----serial number
--select  
--	--COUNT (*)
--	C.identifier
--	,base.CommissioningSerialNo
--	,base.UniqueEpisodeSerialNo
--from CDS.OPBaseArdentia base
--left join Lorenzo.dbo.ScheduleEvent s
--on base.AttendanceIdentifier = s.SCHDL_REFNO 
-- LEFT join lorenzo.dbo.Contract c
--on s.contr_refno = C.contr_refno
--where C.identifier = 'PPAT4='
--  group by    C.identifier, base.CommissioningSerialNo
--  order by   C.identifier

--update CDS.OPBaseArdentia
--set NHSServiceAgreementLineNo = contr.identifier
--from CDS.OPBaseArdentia base
--left join Lorenzo.dbo.ScheduleEvent sched
--on base.AttendanceIdentifier = sched.SCHDL_REFNO 
-- LEFT join lorenzo.dbo.Contract contr
--on sched.contr_refno = contr.contr_refno

--update CDS.OPBaseArdentia
--set CommissioningSerialNo = '999999'
--where CommissioningSerialNo is null

----------------------------------------------------------------------------
PRINT 'Update dodgy consultant code'
update CDS.OPBaseArdentia 
set ConsultantCode = 'C3259986'
where ConsultantCode = 'C32599886'

----------------------------------------------------------------------------
PRINT 'Delete non-applicable patients'
--requested exclusion
DELETE 
FROM CDS.OPBaseArdentia WHERE NHSNumber = '4722504253'
select @RowsDeleted = @RowsDeleted + @@rowcount

--Test patients
--select *
DELETE 
FROM CDS.OPBaseArdentia WHERE NHSNumber LIKE '999%'
select @RowsDeleted = @RowsDeleted + @@rowcount

--select *
DELETE 
FROM CDS.OPBaseArdentia WHERE PatientSurname LIKE '%XTEST%'
select @RowsDeleted = @RowsDeleted + @@rowcount

----------------------------------------------------------------------------
PRINT 'Update site code'
update CDS.OPBaseArdentia set sitecode = 'RM201'


----------------------------------------------------------------------------
PRINT 'Update provider details'
--UPDATE CDS.OPBaseArdentia SET ProviderCode = 'RM200'


----------------------------------------------------------------------------
PRINT 'Update procedure details'

----UPDATE PRIOCPS DETAILS
UPDATE CDS.OPBaseArdentia
SET ProcedureSchemeInUse = '02',
OperationDate1 = AppointmentDate
WHERE OperationCode1 IS NOT NULL

--UPDATE OPCS2 DETAILS
UPDATE CDS.OPBaseArdentia
SET ProcedureSchemeInUse = '02',
OperationDate2 = AppointmentDate
WHERE LEN(OperationCode2) >0

UPDATE CDS.OPBaseArdentia
SET ProcedureSchemeInUse = '02',
OperationDate3 = AppointmentDate
WHERE LEN(OperationCode3) >0

UPDATE CDS.OPBaseArdentia
SET ProcedureSchemeInUse = '02',
OperationDate4 = AppointmentDate
WHERE LEN(OperationCode4) >0

UPDATE CDS.OPBaseArdentia
SET ProcedureSchemeInUse = '02',
OperationDate5 = AppointmentDate
WHERE LEN(OperationCode5) >0

UPDATE CDS.OPBaseArdentia
SET ProcedureSchemeInUse = '02',
OperationDate6 = AppointmentDate
WHERE LEN(OperationCode6) >0

UPDATE CDS.OPBaseArdentia
SET ProcedureSchemeInUse = '02',
OperationDate7 = AppointmentDate
WHERE LEN(OperationCode7) >0

UPDATE CDS.OPBaseArdentia
SET ProcedureSchemeInUse = '02',
OperationDate8 = AppointmentDate
WHERE LEN(OperationCode8) >0

UPDATE CDS.OPBaseArdentia
SET ProcedureSchemeInUse = '02',
OperationDate9 = AppointmentDate
WHERE LEN(OperationCode9) >0

UPDATE CDS.OPBaseArdentia
SET ProcedureSchemeInUse = '02',
OperationDate10 = AppointmentDate
WHERE LEN(OperationCode10) >0

UPDATE CDS.OPBaseArdentia
SET ProcedureSchemeInUse = '02',
OperationDate11 = AppointmentDate
WHERE LEN(OperationCode11) >0

UPDATE CDS.OPBaseArdentia
SET ProcedureSchemeInUse = '02',
OperationDate12 = AppointmentDate
WHERE LEN(OperationCode12) >0

-- *********************************************************************
-- * 07/10/2014 TJD ADDED - Changes for specialised commissioning.
-- *********************************************************************

--UPDATE CDS.OPBaseArdentia
--  SET PurchaserCodeCCG = CASE
--						 --  WHEN PbR.FinalSLACode IN ('Q44','Q44PC','Q46','Q46PC','Q72', 'Q73') THEN LEFT(PbR.FinalSLACode,3)
--						   WHEN PbR.FinalSLACode IN ('13Y') THEN  LEFT(PbR.FinalSLACode,3)
--						   WHEN PbR.FinalSLACode  LIKE 'Q%' THEN LEFT(PbR.FinalSLACode,3)
--                           WHEN PbR.FinalSLACode = '7A5HC' THEN '7A5'
--                           ELSE COALESCE(PbR.CommissionerCodeCCG,'01N') + '00' 
--                         END
--    FROM CDS.OPBaseArdentia
--	LEFT JOIN CDS.PbRExtract PbR ON (OPBaseArdentia.SourceUniqueID = PbR.SourceUniqueID
--										AND ((PbR.DatasetCode = 'ENCOUNTER'AND PbR.EncounterTypeCode = ('OP'))
--												OR (PbR.DatasetCode = 'WARDATT') --02/03/2016 CM added the additional of 'WARDATT' into criteria to assign CCG for ward attenders correctly
--												)
--												)
--	WHERE OPBaseArdentia.DNACode  in ('5','6') --02/03/2016 CM added criteria to specify updating purchaser code only for that activity which is attended as this will be the only activity which appears in the PbR table for collecting the purchaser from. PuchaserCCG for DNA,Cancelled activty to be left as that which is determined in the original build of CDS data
                                                            
                         
                         
                         
                         
-- UPDATE CDS.OPBaseArdentia                       
--  SET CommissioningSerialNo = CASE
--							    --WHEN PbR.PbRExcluded = 1 AND PbR.FinalSLACode IN ('Q44','Q44PC','Q46PC','Q46','Q72', 'Q73','7A5HC') THEN '=' + LEFT(PbR.ExclusionType,5)
--							    WHEN PbR.FinalSLACode IN ('13Y') THEN '=' + LEFT(PbR.ExclusionType,5)
--								WHEN PbR.FinalSLACode  LIKE 'Q%' THEN '=' + LEFT(PbR.ExclusionType,5)
--								WHEN PbR.FinalSLACode = '7A5HC' THEN '=' + LEFT(PbR.ExclusionType,5)
--                                WHEN PbR.PbRExcluded = 1 THEN LEFT(PbR.ExclusionType,6)
--                                ELSE LEFT(PbR.FinalSLACode,6)
--                              END
--	FROM CDS.OPBaseArdentia
--	LEFT JOIN CDS.PbRExtract PbR ON (OPBaseArdentia.SourceUniqueID = PbR.SourceUniqueID
--											AND ((PbR.DatasetCode = 'ENCOUNTER'AND PbR.EncounterTypeCode = ('OP'))
--													OR (PbR.DatasetCode = 'WARDATT') 
--													)
--													)


-- 16/05/2016 TJD Added code for change of financial year.
UPDATE CDS.OPBaseArdentia
SET PurchaserCodeCCG = CASE 
		WHEN PbR.FinalSLACode IN ('13Y')
			THEN LEFT(PbR.FinalSLACode, 3)
		WHEN PbR.FinalSLACode LIKE 'Q%'
			THEN LEFT(PbR.FinalSLACode, 3)
		WHEN PbR.FinalSLACode = '7A5HC'
			THEN '7A5'
		ELSE COALESCE(PbR.CommissionerCodeCCG, '01N') + '00'
		END
FROM CDS.OPBaseArdentia AS Main
LEFT JOIN PbR2016.dbo.PbR AS PbR ON (
		Main.SourceUniqueID = PbR.SourceUniqueID
		AND (
			(
				PbR.DatasetCode = 'ENCOUNTER'
				AND PbR.EncounterTypeCode = 'OP'
				)
			OR PbR.DatasetCode = 'WARDATT'
			)
		)
WHERE Main.DNACode IN (
		'5'
		,'6'
		);

UPDATE CDS.OPBaseArdentia
SET CommissioningSerialNo = CASE 
		WHEN PbR.FinalSLACode IN ('13Y')
			THEN '=' + LEFT(PbR.ExclusionType, 5)
		WHEN PbR.FinalSLACode LIKE 'Q%'
			THEN '=' + LEFT(PbR.ExclusionType, 5)
		WHEN PbR.FinalSLACode = '7A5HC'
			THEN '=' + LEFT(PbR.ExclusionType, 5)
		WHEN PbR.PbRExcluded = 1
			THEN LEFT(PbR.ExclusionType, 6)
		ELSE LEFT(PbR.FinalSLACode, 6)
		END
FROM CDS.OPBaseArdentia AS Main
LEFT JOIN PbR2016.dbo.PbR AS PbR ON (
		Main.SourceUniqueID = PbR.SourceUniqueID
		AND (
			(
				PbR.DatasetCode = 'ENCOUNTER'
				AND PbR.EncounterTypeCode = 'OP'
				)
			OR PbR.DatasetCode = 'WARDATT'
			)
		);
                                    
-- *********************************************************************                                    

/*
15/05/2015 TJD Added - Fix to ensure the site code is set correctly
for Withington(RM201) and Wythenshawe(RM202).  Any activity that cannot be attributed to
one of these sites will be allocated to Wythenshawe.
*/

UPDATE CDS.OPBaseArdentia
SET SiteCode = CASE crm.ChronosArea
		WHEN 'Withington'
			THEN 'RM201'
		ELSE 'RM202'  -- everything else is Wythenshawe
		END
FROM CDS.OPBaseArdentia AS cds
LEFT JOIN PAS.ServicePointBase AS clinic ON (cds.ClinicCode = clinic.SPONT_REFNO_CODE)
LEFT JOIN CRM.ChronosLocations AS crm ON (crm.LocationCode collate database_default = clinic.HEORG_REFNO_MAIN_IDENT collate database_default);



/********************************************************************************************/

declare @CommissionerType varchar(5)
select @CommissionerType = TextValue from dbo.Parameter where Parameter = 'CDSCOMMISSIONERTYPE'

if(@CommissionerType = 'CCG')
	begin
		select @RowsForExport =  COUNT(1) from CDS.OPArdentiaExportCCG
	end
else
	begin
		select @RowsForExport =  COUNT(1) from CDS.OPArdentiaExportPCT
	end
	
	
select @Stats =  'Rows deleted = '  + CONVERT(varchar(10), @RowsDeleted) 
	+ '. Rows for export = ' + CONVERT(varchar(10), @RowsForExport)

exec WriteAuditLogEvent 'CDS - WH AmendOPBaseArdentia', @Stats, @StartTime

--print @Stats


