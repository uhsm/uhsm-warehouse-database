﻿CREATE procedure [CDS].[Build]
	 @CDSBulkReplacementGroupCode varchar (10) = '010'
	,@from smalldatetime = null
	,@to smalldatetime = null
	,@PreviousSubmissionDate date = null
as

--CODE  CLASSIFICATION  
--010 Finished General, Delivery and Birth Episodes 
--020 Unfinished General, Delivery and Birth Episodes 
--030 Other Delivery 
--040 Other Birth 
--050 Detained and/or Long Term Psychiatric Census 
--060 Outpatient (known as Care Activity in the Schema)  
--070 Standard variation of Elective Admission List End Of Period Census 
--080 New and Old variations of Elective Admission List End Of Period Census 
--090 Add variation of Elective Admission List Event During Period 
--100 Remove variation of Elective Admission List Event During Period 
--110 Offer variation of Elective Admission List Event During Period 
--120 Available/Unavailable variation of Elective Admission List Event During Period 
--130 New and Old variations of Elective Admission List Event During Period 
--140 Accident and Emergency Attendance 
--150 Future Outpatient (introduced in CDS Version 6 - known as Future Care Activity in the Schema)  

--Net change processing works as follows.
--
--The currency used to handle net change is the complete YTD dataset. (This implies that the date range
--parameters are not strictly required however they have been left in to provide some measure of user control
--when generating the CDS).
--
--A complete YTD CDS file is generated and compared with the previous submitted CDS data. This will throw up
--discrepencies as follows:
--
--	1. New record
--	2. Amended record
--	3. Deleted record
--
--A new dataset is then generated based on this information containing only new, amended and deleted records
--with the appropriate UpdateType. This dataset is submitted.
-- 
--On submission (generation of flat file) the previous submitted data table is replaced with the 
--latest complete CDS dataset ready for the next run.

--The CDSPROTOCOLIDENTIFIER parameter in the Parameter table determines whether
--we are dealing with net change or bulk

--select * from dbo.Parameter where Parameter = 'CDSPROTOCOLIDENTIFIER'

declare @isNet bit =
	case
	when exists
		(
		select
			1
		from
			dbo.Parameter
		where
			Parameter = 'CDSPROTOCOLIDENTIFIER'
		and	TextValue = '020' --bulk
		)
	then 0
	else 1
	end

--print @isNet

declare @financialYearStartDate smalldatetime
declare @lastRunDate smalldatetime

select
	@lastRunDate =
		(select DateValue from dbo.Parameter where Parameter = 'LASTCDS' + @CDSBulkReplacementGroupCode + 'EXTRACTDATE')

select
	 @from =
		coalesce(@from, @lastRunDate)

	,@to =
		coalesce(@to, getdate())

select
	@financialYearStartDate =
		case
		when datepart(month, @from) < 4
		then '1 apr ' + datename(year, dateadd(year, -1, @from))
		else '1 apr ' + datename(year, @from)
		end

--select DATEPART(DAY, DateValue) from dbo.Parameter where Parameter = 'CDSREPORTPERIODSTARTDATE'
--select DATEPART(MONTH, DateValue) from dbo.Parameter where Parameter = 'CDSREPORTPERIODSTARTDATE'
--select * from dbo.Parameter where Parameter = 'CDSREPORTPERIODENDDATE'

delete
from
	dbo.Parameter
where
	Parameter in
		(
		 'CDSREPORTPERIODSTARTDATE'
		,'CDSREPORTPERIODENDDATE'
		)

if @isNet = 0
	begin

		insert
		into
			dbo.Parameter
		(
			 Parameter
			,DateValue
		)
		select
			 Parameter = 'CDSREPORTPERIODSTARTDATE'
			,DateValue = @from

		insert
		into
			dbo.Parameter
		(
			 Parameter
			,DateValue
		)
		select
			 Parameter = 'CDSREPORTPERIODENDDATE'
			,DateValue = @to

	end


--finished
if @CDSBulkReplacementGroupCode = '010' 
begin

-- CDSType 120, 130, 140
--this relies on an up-to-date MaternityReporting.dbo.MaternityTailsExport table
	--exec CDS.BuildMaternityTails

	delete
	from
		CDS.wrk
	where
		CDSTypeCode in ('120', '130', '140')


	insert into CDS.wrk
	(
		 CDSTypeCode
		,EncounterRecno
	)
	select
		CDSTypeCode =
			--'130'

			case
			when
				exists
					(
					select
						1
					from
						CDS.Birth
					where
						Birth.EpisodeSourceUniqueID = Encounter.SourceUniqueID
					)
			then '120' --birth

			when 
				exists
					(
					select
						1
					from
						CDS.Delivery
					where
						Delivery.EpisodeSourceUniqueID = Encounter.SourceUniqueID
					)
			then '140' --delivery

			else '130'
			end

		,Encounter.EncounterRecno
	from
		APC.Encounter Encounter
	where
		Encounter.EpisodeEndDate between @from and @to

	exec CDS.BuildType010
	--exec CDS.BuildAPC @PreviousSubmissionDate

end

----unfinished
--if @CDSBulkReplacementGroupCode = '020' 
--begin
--
--	-- CDSType 190
--	if datepart(day, @to) = 31 and datepart(month, @to) = 3
--	begin
--		insert into wrkCDS
--		(
--			CDSTypeCode
--			,EncounterRecno
--		)
--		select
--			'190'
--			,EncounterRecno
--		from
--			dbo.APCEncounter Encounter
--		where
--			Encounter.EpisodeStartDate between 
--				dateadd(day, 1, dateadd(year, -1, @to)) -- 1st April this financial year
--			and	@to
--		and	(
--				Encounter.EpisodeEndDate > @to
--			or	Encounter.EpisodeEndDate is null
--			)
--
--		select
--			@rowsInserted = @rowsInserted + @@rowcount
--
--	end
--
--	exec dbo.BuildCDSType190
--end
--
--

--outpatient
if @CDSBulkReplacementGroupCode = '060' 
begin
	-- CDSType 020

	delete
	from
		CDS.wrk
	where
		CDSTypeCode in ('020')

	insert into CDS.wrk
	(
		 CDSTypeCode
		,EncounterRecno
	)
	select
		'020'
		,EncounterRecno
	from
		OP.Encounter Encounter
	where
		Encounter.AppointmentDate between @from and @to
	--and	Encounter.CancelledByCode is null
	--and	Encounter.ConsultantCode <> 'CRF' --CLINICAL RESEARCH FACILITY
	--and	Encounter.AttendanceOutcomeCode <> 'NR'

	exec CDS.BuildType060

end

--if @CDSBulkReplacementGroupCode = '140' --A&E
--begin

--	-- CDSType 010
--	insert into wrkCDS
--	(
--		CDSTypeCode
--		,EncounterRecno
--	)
--	select
--		'010'
--		,EncounterRecno
--	from
--		AE.Encounter Encounter
--	where
----		Encounter.AttendanceCategoryCode <> 1 --remove clinic attenders
----	and
--		(
--			(
--				Encounter.ArrivalDate between @from and @to
----			and	@isNet = 1
--			)
--		or
--			(
--				dateadd(day, datediff(day, 0, Encounter.ArrivalDate), 0) between @from and @to
----			and	@isNet = 0
--			)
--		)
--	and	Encounter.InterfaceCode <> 'PAS'
--	--and	Encounter.Reportable = 1


--	exec dbo.BuildCDSAngliaType140 @CDSProtocolIdentifier = @CDSProtocolIdentifier
--end


update dbo.Parameter
set
	DateValue = getdate ()
--@to
where
	Parameter = 'LASTCDS' + @CDSBulkReplacementGroupCode + 'EXTRACTDATE'
	
--select * from dbo.Parameter --where Parameter like 'LASTCDS%'
--order by Parameter
