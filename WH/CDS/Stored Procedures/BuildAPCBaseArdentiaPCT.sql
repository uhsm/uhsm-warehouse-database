﻿CREATE procedure [CDS].[BuildAPCBaseArdentiaPCT] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @Created datetime

select @StartTime = getdate()

truncate table CDS.APCBaseArdentia

insert
into
	CDS.APCBaseArdentia
(
	 EncounterRecno
	,EpisodeSourceUniqueID
	,CDSGroup
	,CDSType
	,UniqueEpisodeSerialNo
	,UpdateType
	,CDSUpdateDate
	,CDSUpdateTime
	,CDSActivityDate
	,RTTPathwayID
	,RTTCurrentProviderCode
	,RTTPeriodStatusCode
	,RTTStartDate
	,RTTEndDate
	,NNNStatusIndicator
	,OverseasStatus
	,PCTofResidenceCode
	,PracticePCT
	,LocalPatientID
	,LocalPatientIDOrganisationCode
	,NHSNumber
	,Postcode
	,PostcodeAmended
	,PatientRefNo
	,PatientName
	,PatientAddress
	,DateOfBirth
	,SexCode
	,CarerSupportIndicator
	,EthnicGroupCode
	,MaritalStatusCode
	,LegalStatusClassificationCode
	,ProviderSpellNo
	,AdministrationCategoryCode
	,PatientClassificationCode
	,AdmissionMethodCode
	,SourceOfAdmissionCode
	,AdmissionDate
	,AgeOnAdmission
	,DischargeDestinationCode
	,DischargeMethodCode
	,DischargeReadyDate
	,DischargeDate
	,EpisodeNo
	,LastEpisodeInSpellIndicator
	,OperationStatus
	,NeonatalLevelOfCare
	,FirstRegularDayNightAdmission
	,PsychiatricPatientStatusCode
	,EpisodeStartDate
	,EpisodeEndDate
	,AgeAtCDSActivityDate
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,PurchaserReferenceNo
	,CommissionerReferenceNo
	,ProviderCode
	,PurchaserCodePCT
	,ConsultantCode
	,MainSpecialtyCode
	,TreatmentFunctionCode
	,DiagnosisSchemeInUse
	,ProcedureSchemeInUse
	,StartOfEpisodeLocationClassCode
	,StartOfEpisodeSiteCode
	,StartOfEpisodeIntendedClinicalIntensityCode
	,StartOfEpisodeAgeGroupIntendedCode
	,StartOfEpisodeSexOfPatientsCode
	,StartOfEpisodeWardDayPeriodAvailabilityCode
	,StartOfEpisodeWardNightPeriodAvailabilityCode
	,WardStayLocationClassCode
	,WardStaySiteCode
	,WardStayIntendedClinicalIntensityCode
	,WardStayAgeGroupIntendedCode
	,WardStaySexOfPatientsCode
	,WardStayWardDayPeriodAvailabilityCode
	,WardStayWardNightPeriodAvailabilityCode
	,WardStayStartDate
	,WardStayEndDate
	,EndOfEpisodeLocationClassCode
	,EndOfEpisodeSiteCode
	,EndOfEpisodeIntendedClinicalIntensityCode
	,EndOfEpisodeAgeGroupIntendedCode
	,EndOfEpisodeSexOfPatientsCode
	,EndOfEpisodeWardDayPeriodAvailabilityCode
	,EndOfEpisodeWardNightPeriodAvailabilityCode
	,AdultCriticalCareLocalIdentifier1
	,AdultCriticalCareStartDate1
	,AdultCriticalCareStartTime1
	,AdultCriticalCareUnitFunctionCode1
	,AdultCriticalCareUnitBedConfigurationCode1
	,AdultCriticalCareAdmissionSourceCode1
	,AdultCriticalCareSourceLocationCode1
	,AdultCriticalCareAdmissionTypeCode1
	,AdvancedRespiratorySupportDays1
	,BasicRespiratorySupportDays1
	,AdvancedCardiovascularSupportDays1
	,BasicCardiovascularSupportDays1
	,RenalSupportDays1
	,NeurologicalSupportDays1
	,GastroIntestinalSupportDays1
	,DermatologicalSupportDays1
	,LiverSupportDays1
	,OrganSupportMaximum1
	,CriticalCareLevel2Days1
	,CriticalCareLevel3Days1
	,AdultCriticalCareDischargeDate1
	,AdultCriticalCareDischargeTime1
	,AdultCriticalCareDischargeReadyDate1
	,AdultCriticalCareDischargeReadyTime1
	,AdultCriticalCareDischargeStatusCode1
	,AdultCriticalCareDischargeDestinationCode1
	,AdultCriticalCareDischargeLocationCode1
	,AdultCriticalCareLocalIdentifier2
	,AdultCriticalCareStartDate2
	,AdultCriticalCareStartTime2
	,AdultCriticalCareUnitFunctionCode2
	,AdultCriticalCareUnitBedConfigurationCode2
	,AdultCriticalCareAdmissionSourceCode2
	,AdultCriticalCareSourceLocationCode2
	,AdultCriticalCareAdmissionTypeCode2
	,AdvancedRespiratorySupportDays2
	,BasicRespiratorySupportDays2
	,AdvancedCardiovascularSupportDays2
	,BasicCardiovascularSupportDays2
	,RenalSupportDays2
	,NeurologicalSupportDays2
	,GastroIntestinalSupportDays2
	,DermatologicalSupportDays2
	,LiverSupportDays2
	,OrganSupportMaximum2
	,CriticalCareLevel2Days2
	,CriticalCareLevel3Days2
	,AdultCriticalCareDischargeDate2
	,AdultCriticalCareDischargeTime2
	,AdultCriticalCareDischargeReadyDate2
	,AdultCriticalCareDischargeReadyTime2
	,AdultCriticalCareDischargeStatusCode2
	,AdultCriticalCareDischargeDestinationCode2
	,AdultCriticalCareDischargeLocationCode2
	,AdultCriticalCareLocalIdentifier3
	,AdultCriticalCareStartDate3
	,AdultCriticalCareStartTime3
	,AdultCriticalCareUnitFunctionCode3
	,AdultCriticalCareUnitBedConfigurationCode3
	,AdultCriticalCareAdmissionSourceCode3
	,AdultCriticalCareSourceLocationCode3
	,AdultCriticalCareAdmissionTypeCode3
	,AdvancedRespiratorySupportDays3
	,BasicRespiratorySupportDays3
	,AdvancedCardiovascularSupportDays3
	,BasicCardiovascularSupportDays3
	,RenalSupportDays3
	,NeurologicalSupportDays3
	,GastroIntestinalSupportDays3
	,DermatologicalSupportDays3
	,LiverSupportDays3
	,OrganSupportMaximum3
	,CriticalCareLevel2Days3
	,CriticalCareLevel3Days3
	,AdultCriticalCareDischargeDate3
	,AdultCriticalCareDischargeTime3
	,AdultCriticalCareDischargeReadyDate3
	,AdultCriticalCareDischargeReadyTime3
	,AdultCriticalCareDischargeStatusCode3
	,AdultCriticalCareDischargeDestinationCode3
	,AdultCriticalCareDischargeLocationCode3
	,AdultCriticalCareLocalIdentifier4
	,AdultCriticalCareStartDate4
	,AdultCriticalCareStartTime4
	,AdultCriticalCareUnitFunctionCode4
	,AdultCriticalCareUnitBedConfigurationCode4
	,AdultCriticalCareAdmissionSourceCode4
	,AdultCriticalCareSourceLocationCode4
	,AdultCriticalCareAdmissionTypeCode4
	,AdvancedRespiratorySupportDays4
	,BasicRespiratorySupportDays4
	,AdvancedCardiovascularSupportDays4
	,BasicCardiovascularSupportDays4
	,RenalSupportDays4
	,NeurologicalSupportDays4
	,GastroIntestinalSupportDays4
	,DermatologicalSupportDays4
	,LiverSupportDays4
	,OrganSupportMaximum4
	,CriticalCareLevel2Days4
	,CriticalCareLevel3Days4
	,AdultCriticalCareDischargeDate4
	,AdultCriticalCareDischargeTime4
	,AdultCriticalCareDischargeReadyDate4
	,AdultCriticalCareDischargeReadyTime4
	,AdultCriticalCareDischargeStatusCode4
	,AdultCriticalCareDischargeDestinationCode4
	,AdultCriticalCareDischargeLocationCode4
	,AdultCriticalCareLocalIdentifier5
	,AdultCriticalCareStartDate5
	,AdultCriticalCareStartTime5
	,AdultCriticalCareUnitFunctionCode5
	,AdultCriticalCareUnitBedConfigurationCode5
	,AdultCriticalCareAdmissionSourceCode5
	,AdultCriticalCareSourceLocationCode5
	,AdultCriticalCareAdmissionTypeCode5
	,AdvancedRespiratorySupportDays5
	,BasicRespiratorySupportDays5
	,AdvancedCardiovascularSupportDays5
	,BasicCardiovascularSupportDays5
	,RenalSupportDays5
	,NeurologicalSupportDays5
	,GastroIntestinalSupportDays5
	,DermatologicalSupportDays5
	,LiverSupportDays5
	,OrganSupportMaximum5
	,CriticalCareLevel2Days5
	,CriticalCareLevel3Days5
	,AdultCriticalCareDischargeDate5
	,AdultCriticalCareDischargeTime5
	,AdultCriticalCareDischargeReadyDate5
	,AdultCriticalCareDischargeReadyTime5
	,AdultCriticalCareDischargeStatusCode5
	,AdultCriticalCareDischargeDestinationCode5
	,AdultCriticalCareDischargeLocationCode5
	,AdultCriticalCareLocalIdentifier6
	,AdultCriticalCareStartDate6
	,AdultCriticalCareStartTime6
	,AdultCriticalCareUnitFunctionCode6
	,AdultCriticalCareUnitBedConfigurationCode6
	,AdultCriticalCareAdmissionSourceCode6
	,AdultCriticalCareSourceLocationCode6
	,AdultCriticalCareAdmissionTypeCode6
	,AdvancedRespiratorySupportDays6
	,BasicRespiratorySupportDays6
	,AdvancedCardiovascularSupportDays6
	,BasicCardiovascularSupportDays6
	,RenalSupportDays6
	,NeurologicalSupportDays6
	,GastroIntestinalSupportDays6
	,DermatologicalSupportDays6
	,LiverSupportDays6
	,OrganSupportMaximum6
	,CriticalCareLevel2Days6
	,CriticalCareLevel3Days6
	,AdultCriticalCareDischargeDate6
	,AdultCriticalCareDischargeTime6
	,AdultCriticalCareDischargeReadyDate6
	,AdultCriticalCareDischargeReadyTime6
	,AdultCriticalCareDischargeStatusCode6
	,AdultCriticalCareDischargeDestinationCode6
	,AdultCriticalCareDischargeLocationCode6
	,AdultCriticalCareLocalIdentifier7
	,AdultCriticalCareStartDate7
	,AdultCriticalCareStartTime7
	,AdultCriticalCareUnitFunctionCode7
	,AdultCriticalCareUnitBedConfigurationCode7
	,AdultCriticalCareAdmissionSourceCode7
	,AdultCriticalCareSourceLocationCode7
	,AdultCriticalCareAdmissionTypeCode7
	,AdvancedRespiratorySupportDays7
	,BasicRespiratorySupportDays7
	,AdvancedCardiovascularSupportDays7
	,BasicCardiovascularSupportDays7
	,RenalSupportDays7
	,NeurologicalSupportDays7
	,GastroIntestinalSupportDays7
	,DermatologicalSupportDays7
	,LiverSupportDays7
	,OrganSupportMaximum7
	,CriticalCareLevel2Days7
	,CriticalCareLevel3Days7
	,AdultCriticalCareDischargeDate7
	,AdultCriticalCareDischargeTime7
	,AdultCriticalCareDischargeReadyDate7
	,AdultCriticalCareDischargeReadyTime7
	,AdultCriticalCareDischargeStatusCode7
	,AdultCriticalCareDischargeDestinationCode7
	,AdultCriticalCareDischargeLocationCode7
	,AdultCriticalCareLocalIdentifier8
	,AdultCriticalCareStartDate8
	,AdultCriticalCareStartTime8
	,AdultCriticalCareUnitFunctionCode8
	,AdultCriticalCareUnitBedConfigurationCode8
	,AdultCriticalCareAdmissionSourceCode8
	,AdultCriticalCareSourceLocationCode8
	,AdultCriticalCareAdmissionTypeCode8
	,AdvancedRespiratorySupportDays8
	,BasicRespiratorySupportDays8
	,AdvancedCardiovascularSupportDays8
	,BasicCardiovascularSupportDays8
	,RenalSupportDays8
	,NeurologicalSupportDays8
	,GastroIntestinalSupportDays8
	,DermatologicalSupportDays8
	,LiverSupportDays8
	,OrganSupportMaximum8
	,CriticalCareLevel2Days8
	,CriticalCareLevel3Days8
	,AdultCriticalCareDischargeDate8
	,AdultCriticalCareDischargeTime8
	,AdultCriticalCareDischargeReadyDate8
	,AdultCriticalCareDischargeReadyTime8
	,AdultCriticalCareDischargeStatusCode8
	,AdultCriticalCareDischargeDestinationCode8
	,AdultCriticalCareDischargeLocationCode8
	,AdultCriticalCareLocalIdentifier9
	,AdultCriticalCareStartDate9
	,AdultCriticalCareStartTime9
	,AdultCriticalCareUnitFunctionCode9
	,AdultCriticalCareUnitBedConfigurationCode9
	,AdultCriticalCareAdmissionSourceCode9
	,AdultCriticalCareSourceLocationCode9
	,AdultCriticalCareAdmissionTypeCode9
	,AdvancedRespiratorySupportDays9
	,BasicRespiratorySupportDays9
	,AdvancedCardiovascularSupportDays9
	,BasicCardiovascularSupportDays9
	,RenalSupportDays9
	,NeurologicalSupportDays9
	,GastroIntestinalSupportDays9
	,DermatologicalSupportDays9
	,LiverSupportDays9
	,OrganSupportMaximum9
	,CriticalCareLevel2Days9
	,CriticalCareLevel3Days9
	,AdultCriticalCareDischargeDate9
	,AdultCriticalCareDischargeTime9
	,AdultCriticalCareDischargeReadyDate9
	,AdultCriticalCareDischargeReadyTime9
	,AdultCriticalCareDischargeStatusCode9
	,AdultCriticalCareDischargeDestinationCode9
	,AdultCriticalCareDischargeLocationCode9
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,ReferrerCode
	,ReferringOrganisationCode
	,DurationOfElectiveWait
	,ManagementIntentionCode
	,DecidedToAdmitDate
	,EarliestReasonableOfferDate
	,HRGCode
	,HRGVersionNumber
	,DGVPCode
	,PreviousPregnancies
	,NumberOfBabies
	,FirstAnteNatalAssessmentDate
	,AnteNatalGpCode
	,AnteNatalGpPracticeCode
	,DeliveryLocationClassCode
	,DeliveryPlaceChangeReasonCode
	,IntendedDeliveryPlaceTypeCode
	,AnaestheticGivenDuringCode
	,AnaestheticGivenPostCode
	,GestationLength
	,LabourOnsetMethodCode
	,DeliveryDate
	,DeliveryMethodCode
	,Birth1AssessmentGestationLength
	,Birth1BirthOrder
	,Birth1BirthWeight
	,Birth1DateOfBirth
	,Birth1DeliveryLocationClassCode
	,Birth1DeliveryMethodCode
	,Birth1DeliveryPlaceTypeActualCode
	,Birth1LiveOrStillBirth
	,Birth1LocalPatientIdentifier
	,Birth1NHSNumber
	,Birth1NHSNumberStatusIndicator
	,Birth1ProviderCode
	,Birth1ResuscitationMethodCode
	,Birth1SexCode
	,Birth1StatusOfPersonConductingDeliveryCode
	,ActualDeliveryPlaceTypeCode
	,StatusOfPersonConductingDeliveryCode
	,MotherLocalPatientID
	,MotherLocalPatientIDOrganisationCode
	,MotherNHSNumber
	,MotherNNNStatusIndicator
	,MotherPostcode
	,MotherPCTOfResidenceCode
	,MotherDateOfBirth
	,MotherPatientAddress
	,MotherPatientAddress1
	,MotherPatientAddress2
	,MotherPatientAddress3
	,MotherPatientAddress4
	,MotherPatientAddress5
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientAddress5
	--,HACode
	,PatientForename
	,PatientSurname
)
select
	 Encounter.EncounterRecno
	,Encounter.SourceUniqueID

------------------------------------------------------------------------
--DATA GROUP: CDS V6 TYPE 005N - COMMISSIONING DATA SET TRANSACTION HEADER GROUP - NET CHANGE PROTOCOL
------------------------------------------------------------------------

	,CDSGroup = '010'

	,CDSType = wrk.CDSTypeCode

	 ,UniqueEpisodeSerialNo =
		left(
			'BRM2' +
			right(
				 replicate('0', 14) +
				 cast(Encounter.SourceUniqueID as varchar)
				,14
			)
			,35
		)

	,UpdateType = '9                ' --all records are either new or updated

	,CDSUpdateDate =
		left(
			CONVERT(varchar, getdate(), 120)
			,10
		)

	,CDSUpdateTime =
		left(
			replace(
				 CONVERT(varchar, getdate(), 108)
				,':'
				,''
			)
			,6
		)

	,CDSActivityDate =
		left(
			CONVERT(varchar, Encounter.EpisodeEndDate, 120)
			,10
		)


------------------------------------------------------------------------
--DATA GROUP: PATIENT PATHWAY
------------------------------------------------------------------------

	,RTTPathwayID =  --this is 25 characters on PAS
		left(
			case
			when len(Encounter.RTTPathwayID) < 20
			then
				stuff(
					 Encounter.RTTPathwayID
					,6
					,0
					,replicate('0', 20 - LEN(Encounter.RTTPathwayID))
				)
			else Encounter.RTTPathwayID
			end

			,20
		)

	,RTTCurrentProviderCode =
		case
		when Encounter.RTTPathwayID is null
		then null
		else
			coalesce(
				Encounter.RTTCurrentProviderCode
				,'RM200'
			)
		end

	,RTTPeriodStatusCode =
		coalesce(
			case
			when RTTPeriodStatus.NationalRTTStatusCode = 'NSP'
			then '99'
			else RTTPeriodStatus.NationalRTTStatusCode
			end
			,'99'
		)

	,RTTStartDate = 
		left(
			convert(varchar, Encounter.RTTStartDate, 120)
			,10
		)

	,RTTEndDate = 
		left(
			convert(varchar, Encounter.RTTEndDate, 120)
			,10
		)

------------------------------------------------------------------------
--DATA GROUP: PATIENT IDENTITY
------------------------------------------------------------------------

	,NNNStatusIndicator =
		case
		when SensitiveDiagnosis.APCSourceUniqueID is not null
		then '07'
		when SensitiveOperation.APCSourceUniqueID is not null
		then '07'
		when Encounter.NHSNumber is null
		then '04' --Trace attempted - No match or multiple match found
		else
			case
			when Encounter.NHSNumberStatusCode = 'SUCCS' then '01'
			when Encounter.NHSNumberStatusCode = 'RESET' then '02'
			when Encounter.NHSNumberStatusCode is null then '01'
			else Encounter.NHSNumberStatusCode
			end
		end
		
	,OverseasStatus = OverseasStatus.MappedCode
	--KO changed 02/10/2012
	--,PCTofResidenceCode =
	--	coalesce(
	--		 Postcode.PCTCode
	--		,PCT.OrganisationLocalCode
	--		,'5NT'
	--		--,'XXX'
	--	)
	,PCTofResidenceCode = 
		case when OverseasStatus.MappedCode in
			(
			 1	-- Exempt (reciprocal agreement)
			,2	-- Exempt from payment - other
			,3	-- To pay hotel fees only
			,4	-- To pay all fees
			,9	-- Charging rate not known
			)
		then 'X98'

		--For Scotland, NI and CI (NOT WALES) use DistrictOfResidence as lookup instead of PCTCode
		when left(Postcode.DistrictOfResidence,1) in ('S','Z','Y') then Postcode.DistrictOfResidence
		
		else
			--Lookup to ODS postcode_to_PCT table
			Postcode.PCTCode
		end

	,PracticePCT = PCT.OrganisationLocalCode
	
	,LocalPatientID =
		left(
			Encounter.DistrictNo
			,10
		)


	--,LocalPatientID =
	--	case
	--	when SensitiveDiagnosis.APCSourceUniqueID is not null
	--	then null
	--	when SensitiveOperation.APCSourceUniqueID is not null
	--	then null
	--	else
	--		left(
	--			Encounter.DistrictNo
	--			,10
	--		)
	--	end

	,LocalPatientIDOrganisationCode = 'RM200' --UHSM

	,NHSNumber =
		case
		when SensitiveDiagnosis.APCSourceUniqueID is not null
		then null
		when SensitiveOperation.APCSourceUniqueID is not null
		then null
		else Encounter.NHSNumber
		end

	,Postcode = Encounter.Postcode
	,PostcodeAmended =
			case
			when SensitiveDiagnosis.APCSourceUniqueID is not null
			then null
			when SensitiveOperation.APCSourceUniqueID is not null
			then null
			when Encounter.Postcode is null
			then 'ZZ99 3AZ'
			when len(Encounter.Postcode) = 8 then Encounter.Postcode
			else left(Encounter.Postcode, 3) + ' ' + right(Encounter.Postcode, 4) 
			end

--KO added to allow link to Missing Patient data
	,PatientRefNo = Encounter.SourcePatientNo

	,PatientName = 
		left(
			case
			when SensitiveDiagnosis.APCSourceUniqueID is not null
			then null
			when SensitiveOperation.APCSourceUniqueID is not null
			then null
			when 
				case
				when Encounter.NHSNumber is null
				then '04' --Trace attempted - No match or multiple match found
				else
					case
					when Encounter.NHSNumberStatusCode = 'SUCCS' then '01'
					when Encounter.NHSNumberStatusCode = 'RESET' then '02'
					when Encounter.NHSNumberStatusCode is null then '01'
					else Encounter.NHSNumberStatusCode
					end
				end <> '01'
			then ltrim(rtrim(coalesce(Encounter.PatientForename, '') + ' ' + coalesce(Encounter.PatientSurname, '')))
			else ''
			end
			,70
		)

	,PatientAddress =
		left(
			case
			when SensitiveDiagnosis.APCSourceUniqueID is not null
			then null
			when SensitiveOperation.APCSourceUniqueID is not null
			then null
			when 
				case
				when Encounter.NHSNumber is null
				then '04' --Trace attempted - No match or multiple match found
				else
					case
					when Encounter.NHSNumberStatusCode = 'SUCCS' then '01'
					when Encounter.NHSNumberStatusCode = 'RESET' then '02'
					when Encounter.NHSNumberStatusCode is null then '01'
					else Encounter.NHSNumberStatusCode
					end
				end <> '01'
			then
				coalesce(Encounter.PatientAddress1 + ' ', 'Unknown Address ') + 
				coalesce(Encounter.PatientAddress2 + ' ', 'Unknown Address ') + 
				coalesce(Encounter.PatientAddress3 + ' ', 'Unknown Address ') + 
				coalesce(Encounter.PatientAddress4, 'Unknown Address')
			else ''
			end
			,175
		)


------------------------------------------------------------------------
--DATA GROUP: PATIENT CHARACTERISTICS
------------------------------------------------------------------------

	,DateOfBirth =
		left(
			CONVERT(varchar, Encounter.DateOfBirth, 120)
			,10
		)

	,SexCode = Sex.MappedCode
	
	,CarerSupportIndicator = null --not required on CDS

	,EthnicGroupCode =
		EthnicOrigin.MappedCode

	,MaritalStatusCode =
		MaritalStatus.MappedCode

	,LegalStatusClassificationCode =
		LegalStatusClassification.MappedCode

------------------------------------------------------------------------
--DATA GROUP: HOSPITAL PROVIDER SPELL - ADMISSION CHARACTERISTICS
------------------------------------------------------------------------

	,ProviderSpellNo =
		Encounter.ProviderSpellNo

	,AdministrationCategoryCode =
		AdminCategory.MappedCode

	,PatientClassificationCode = --requires work
		case
		when
			Encounter.ManagementIntentionCode in
			(
			 2003683	--Not Applicable	8
			,2003684	--Not Known	9
			,2005563	--Not Specified	NSP
			)
		then
			case
			when datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
			then 2
			else 1
			end

		when 
			Encounter.ManagementIntentionCode in
				(
				 8850	--No Overnight Stay	2
				,8851	--Planned Sequence - No Overnight Stays	4
				,9487	--Planned Sequence - Inc. Over Night Stays	3
				)
		and
			Encounter.AdmissionMethodCode not in
			(
			 2003470	--Elective
			,8811		--Elective - Booked
			,8810		--Elective - Waiting List
			,8812		--Elective - Planned
			,13			--Not Specified
			,2003472	--OTHER
			)
		then 1

		when
			Encounter.ManagementIntentionCode in
			(
			 8851	--Planned Sequence - No Overnight Stays	4
			)
		and	datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
		then 4

		when
			Encounter.ManagementIntentionCode in
			(
			 9487	--Planned Sequence - Inc. Over Night Stays	3
			)
		and	datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
		then 3

		when datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
		then 2

		else 1
		end

	,AdmissionMethodCode =
		AdmissionMethod.MappedCode

	,SourceOfAdmissionCode =
		AdmissionSource.MappedCode

	,AdmissionDate =
		left(
			CONVERT(varchar, Encounter.AdmissionDate, 120)
			,10
		)

	,AgeOnAdmission = --CDS specifies completed years
		datediff(year, Encounter.DateOfBirth, Encounter.AdmissionDate)
		-- -
		--case
		--when datepart(day, Encounter.DateOfBirth) = datepart(day, Encounter.AdmissionDate)
		--and	datepart(month, Encounter.DateOfBirth) = datepart(month,Encounter.AdmissionDate)
		--and datepart(year, Encounter.DateOfBirth) <> datepart(year,Encounter.AdmissionDate)
		--then 0
		--else 
		--	case
		--	when Encounter.DateOfBirth > Encounter.AdmissionDate then 0
		--	when datepart(dy, Encounter.DateOfBirth) > datepart(dy, Encounter.AdmissionDate) 
		--	then 1
		--	else 0 
		--	end 
		--end

------------------------------------------------------------------------
--DATA GROUP: HOSPITAL PROVIDER SPELL - DISCHARGE CHARACTERISTICS
------------------------------------------------------------------------

	,DischargeDestinationCode =
		DischargeDestination.MappedCode

	,DischargeMethodCode =
		DischargeMethod.MappedCode

	,DischargeReadyDate = null --not required

	,DischargeDate =
		left(
			CONVERT(varchar, Encounter.DischargeDate, 120)
			,10
		)

------------------------------------------------------------------------
--DATA GROUP: CONSULTANT EPISODE - ACTIVITY CHARACTERISTICS
------------------------------------------------------------------------

	,EpisodeNo = 
		Encounter.EpisodeNo

	,LastEpisodeInSpellIndicator =
		case Encounter.LastEpisodeInSpellIndicator
		when 0
		then 2
		else 1
		end

	,OperationStatus =
		case
		when Encounter.PrimaryOperationCode is null
		then 8
		else 1
		end

	,NeonatalLevelOfCare = null --Optional

	,FirstRegularDayNightAdmission = null --Optional

	,PsychiatricPatientStatusCode = --may need to fetch this from PAS --not defined on current CDS template (InfoSQL.CDS.APCV6_Template)
		case
		when Encounter.AdmissionTime = Encounter.EpisodeStartTime
		then '?'
		else null
		end

	,EpisodeStartDate =
		left(
			CONVERT(varchar, Encounter.EpisodeStartDate, 120)
			,10
		)

	,EpisodeEndDate =
		left(
			CONVERT(varchar, Encounter.EpisodeEndDate, 120)
			,10
		)

	,AgeAtCDSActivityDate = --CDS specifies completed years
		datediff(year, Encounter.DateOfBirth, Encounter.EpisodeEndDate)
		-- -
		--case
		--when datepart(day, Encounter.DateOfBirth) = datepart(day, Encounter.EpisodeEndDate)
		--and	datepart(month, Encounter.DateOfBirth) = datepart(month, Encounter.EpisodeEndDate)
		--and datepart(year, Encounter.DateOfBirth) <> datepart(year, Encounter.EpisodeEndDate)
		--then 0
		--else 
		--	case
		--	when Encounter.DateOfBirth > Encounter.EpisodeEndDate then 0
		--	when datepart(dy, Encounter.DateOfBirth) > datepart(dy, Encounter.EpisodeEndDate) 
		--	then 1
		--	else 0 
		--	end 
		--end

------------------------------------------------------------------------
--DATA GROUP: CONSULTANT EPISODE - SERVICE AGREEMENT DETAILS
------------------------------------------------------------------------

	,CommissioningSerialNo =
		Encounter.ContractSerialNo --UHSM - IPMAIN on old CDS

	,NHSServiceAgreementLineNo = null --Optional

	,PurchaserReferenceNo = null --Optional

	,CommissionerReferenceNo = '9' --Referral.REFBY_REFERENCE

	,ProviderCode = 'RM200' --UHSM

	--KO 14/11/2012
	--,PurchaserCode =
	--	isnull(
	--	coalesce(
	--		 left(PCTMap.XRefEntityCode, 3) + '00'
	--		,left(Purchaser.PurchaserMainCode + '00', 5)
	--	),'5NT00')
	--NOTE THAT NULL GPPRACTICE RETURNS 'SHA'
	,PurchaserCode =
		case 
			when OverseasStatus.MappedCode in (1,2)
				then '5NT00'	--OS - Exempt from charges
			when OverseasStatus.MappedCode in (3,4)
				then 'VPP00'	--OS - Liable for charges
			when Encounter.Postcode like 'IM%'
				then 'YAC00'	--IOM patients don't seem to have overseas status code, so hard-code YAC here to
								--avoid purchaser being pulled from iPM Purchaser (usually TDH00)
		else
			coalesce(
				left(PCTMapPurchaser.XRefEntityCode, 3) + '00'
				,left(Purchaser.PurchaserMainCode + '00', 5)
				,left(PCTMapPractice.XRefEntityCode, 3) + '00'
				,left(PCT.OrganisationLocalCode + '00', 5)
			)
		end
------------------------------------------------------------------------
--DATA GROUP: CONSULTANT EPISODE - PERSON GROUP (CONSULTANT)
------------------------------------------------------------------------

	--,ConsultantCode = Consultant.NationalConsultantCode
	,ConsultantCode = 
		case 
			when coalesce(
				 ConsSpecMapping.XrefEntityCode
				,ConsultantSpecialty.NationalSpecialtyCode
				) in (
				'960' --ALLIED HEALTH PROFESSIONAL EPISODE
				,'901' --OCCUPATIONAL MEDICINE
				,'713' --PSYCHOTHERAPY
				)
				then 'H9999998'
			when Consultant.ProfessionalCarerType = 'Consultant'
				then case when Consultant.NationalConsultantCode not like '[C][0123456789]%'
					then 'C9999998'
					else Consultant.NationalConsultantCode
				end
			when Consultant.ProfessionalCarerType = 'Dentist'
				then case when Consultant.NationalConsultantCode not like '[D][0123456789]%'
					then 'D9999998'
					else Consultant.NationalConsultantCode
				end
			when Consultant.ProfessionalCarerType in (
				'Nurse'
				,'Nurse Practitioner'
				,'Specialist Nurse Practitioner'
				,'Specialist Practitioner'
				,'Clinical Nurse Specialist'
				)
				or Consultant.NationalConsultantCode like 'NUR%'
				then 'N9999998'
			when Consultant.NationalConsultantCode = 'MIDWIFE'
				then 'M9999998'
			when Consultant.ProfessionalCarerType in (
				'Allied Healthcare Professional'
				,'Dietician'
				,'Health Care Professional'
				,'Health Visitor'
				,'Occupational Therapist'
				,'Physiotherapist'
				,'Speech and Language Therapist'
				,'Associate Specialist'
				)
				then 'H9999998'
			when Consultant.NationalConsultantCode like '[C][0123456789]%'
				then Consultant.NationalConsultantCode
			else 'H9999998'
		end
		
	,MainSpecialtyCode = 
		case
		when
			left(
				coalesce(
					TxFuncMapping.XrefEntityCode
					,TreatmentFunction.NationalSpecialtyCode
				)
				,3
			) = '500'
		then ConsultantSpecialty.NationalSpecialtyCode

		else 
			coalesce(
				 ConsSpecMapping.XrefEntityCode
				,ConsultantSpecialty.NationalSpecialtyCode
				)
		end

	,TreatmentFunctionCode =
		coalesce(
			 TxFuncMapping.XrefEntityCode
			,TreatmentFunction.NationalSpecialtyCode
		)

------------------------------------------------------------------------
--DATA GROUP: CONSULTANT EPISODE - CLINICAL DIAGNOSIS GROUP (ICD)
------------------------------------------------------------------------

	,DiagnosisSchemeInUse =
		case
		when Encounter.PrimaryDiagnosisCode is null
		then null
		else '02' --ICD10
		end

--diagnoses are held in CDS.Diagnosis view

------------------------------------------------------------------------
--DATA GROUP: CONSULTANT EPISODE - CLINICAL DIAGNOSIS GROUP (READ)
------------------------------------------------------------------------
--Whole group is optional


------------------------------------------------------------------------
--DATA GROUP: CONSULTANT EPISODE - CLINICAL ACTIVITY GROUP (OPCS)
------------------------------------------------------------------------
	,ProcedureSchemeInUse =
		case
		when Encounter.PrimaryOperationCode is null
		then null
		else '02' --OPCS4
		end



--operations are held in CDS.Operation view


------------------------------------------------------------------------
--DATA GROUP: CONSULTANT EPISODE - CLINICAL ACTIVITY GROUP (OPCS)
------------------------------------------------------------------------
--Whole group is optional


------------------------------------------------------------------------
--DATA GROUP: LOCATION GROUP (AT START OF EPISODE)
------------------------------------------------------------------------

	,StartOfEpisodeLocationClassCode = '04'

	,StartOfEpisodeSiteCode = 'RM202'

	,StartOfEpisodeIntendedClinicalIntensityCode = null --Optional

	,StartOfEpisodeAgeGroupIntendedCode = null --Optional

	,StartOfEpisodeSexOfPatientsCode = null --Optional

	,StartOfEpisodeWardDayPeriodAvailabilityCode = null --Optional

	,StartOfEpisodeWardNightPeriodAvailabilityCode = null --Optional


------------------------------------------------------------------------
--DATA GROUP: LOCATION GROUP (AT WARD STAY)
------------------------------------------------------------------------

	,WardStayLocationClassCode = '04'

	,WardStaySiteCode = 'RM202'

	,WardStayIntendedClinicalIntensityCode = null --Optional

	,WardStayAgeGroupIntendedCode = null --Optional

	,WardStaySexOfPatientsCode = null --Optional

	,WardStayWardDayPeriodAvailabilityCode = null --Optional

	,WardStayWardNightPeriodAvailabilityCode = null --Optional

	,WardStayStartDate = null --Optional

	,WardStayEndDate = null --Optional

------------------------------------------------------------------------
--DATA GROUP: LOCATION GROUP (AT END OF EPISODE)
------------------------------------------------------------------------

	,EndOfEpisodeLocationClassCode = '05'

	,EndOfEpisodeSiteCode = 'RM202'

	,EndOfEpisodeIntendedClinicalIntensityCode = null --Optional

	,EndOfEpisodeAgeGroupIntendedCode = null --Optional

	,EndOfEpisodeSexOfPatientsCode = null --Optional

	,EndOfEpisodeWardDayPeriodAvailabilityCode = null --Optional

	,EndOfEpisodeWardNightPeriodAvailabilityCode = null --Optional


--UHSM do not have a neonatal system - these are picked  up from the national Badger system

------------------------------------------------------------------------
--DATA GROUP: NEONATAL CRITICAL CARE PERIOD
------------------------------------------------------------------------

----FUNCTION: See CRITICAL CARE PERIOD
----To carry the details of the first 9 Critical Care Periods for care provided using Neonatal Care facilities.

--------------------------------------------------------------------------
----DATA GROUP: NEONATAL CRITICAL CARE - ADMISSION CHARACTERISTICS
--------------------------------------------------------------------------

--	,NeonatalCriticalCareLocalIdentifier1 = NeonatalCriticalCarePeriod1.CriticalCareLocalIdentifier

--	,NeonatalCriticalCareStartDate1 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod1.CriticalCareStartTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)

--	,NeonatalCriticalCareStartTime1 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod1.CriticalCareStartTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)

--	,NeonatalCriticalCareUnitFunctionCode1 = NeonatalCriticalCarePeriod1.CriticalCareUnitFunctionCode

--	,NeonatalCriticalCareGestationLengthAtDelivery1 = NeonatalCriticalCarePeriod1.NeonatalCriticalCareGestationLengthAtDelivery

--------------------------------------------------------------------------
----DATA GROUP: NEONATAL DAILY CARE - ACTIVITY CHARACTERISTICS
--------------------------------------------------------------------------

----Repeating Group

--	--,NeonatalCriticalCareActivityDate1 = null
--	--,NeonatalCriticalCareActivityCode1 = null
--	--,NeonatalCriticalCareHighCostDrugs1 = null

--------------------------------------------------------------------------
----DATA GROUP: NEONATAL CRITICAL CARE - DISCHARGE CHARACTERISTICS
--------------------------------------------------------------------------

--	,NeonatalCriticalCareDischargeDate1 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod1.CriticalCareDischargeTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)
--	,NeonatalCriticalCareDischargeTime1 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod1.CriticalCareDischargeTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)


--------------------------------------------------------------------------
----DATA GROUP: NEONATAL CRITICAL CARE - ADMISSION CHARACTERISTICS
--------------------------------------------------------------------------

--	,NeonatalCriticalCareLocalIdentifier2 = NeonatalCriticalCarePeriod2.CriticalCareLocalIdentifier

--	,NeonatalCriticalCareStartDate2 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod2.CriticalCareStartTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)

--	,NeonatalCriticalCareStartTime2 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod2.CriticalCareStartTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)

--	,NeonatalCriticalCareUnitFunctionCode2 = NeonatalCriticalCarePeriod2.CriticalCareUnitFunctionCode

--	,NeonatalCriticalCareGestationLengthAtDelivery2 = NeonatalCriticalCarePeriod2.NeonatalCriticalCareGestationLengthAtDelivery

--------------------------------------------------------------------------
----DATA GROUP: NEONATAL DAILY CARE - ACTIVITY CHARACTERISTICS
--------------------------------------------------------------------------

----Repeating Group

--	--,NeonatalCriticalCareActivityDate2 = null
--	--,NeonatalCriticalCareActivityCode2 = null
--	--,NeonatalCriticalCareHighCostDrugs2 = null

--------------------------------------------------------------------------
----DATA GROUP: NEONATAL CRITICAL CARE - DISCHARGE CHARACTERISTICS
--------------------------------------------------------------------------

--	,NeonatalCriticalCareDischargeDate2 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod2.CriticalCareDischargeTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)
--	,NeonatalCriticalCareDischargeTime2 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod2.CriticalCareDischargeTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)


--------------------------------------------------------------------------
----DATA GROUP: NEONATAL CRITICAL CARE - ADMISSION CHARACTERISTICS
--------------------------------------------------------------------------

--	,NeonatalCriticalCareLocalIdentifier3 = NeonatalCriticalCarePeriod3.CriticalCareLocalIdentifier

--	,NeonatalCriticalCareStartDate3 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod3.CriticalCareStartTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)

--	,NeonatalCriticalCareStartTime3 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod3.CriticalCareStartTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)

--	,NeonatalCriticalCareUnitFunctionCode3 = NeonatalCriticalCarePeriod3.CriticalCareUnitFunctionCode

--	,NeonatalCriticalCareGestationLengthAtDelivery3 = NeonatalCriticalCarePeriod3.NeonatalCriticalCareGestationLengthAtDelivery

--------------------------------------------------------------------------
----DATA GROUP: NEONATAL DAILY CARE - ACTIVITY CHARACTERISTICS
--------------------------------------------------------------------------

----Repeating Group

--	--,NeonatalCriticalCareActivityDate3 = null
--	--,NeonatalCriticalCareActivityCode3 = null
--	--,NeonatalCriticalCareHighCostDrugs3 = null

--------------------------------------------------------------------------
----DATA GROUP: NEONATAL CRITICAL CARE - DISCHARGE CHARACTERISTICS
--------------------------------------------------------------------------

--	,NeonatalCriticalCareDischargeDate3 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod3.CriticalCareDischargeTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)
--	,NeonatalCriticalCareDischargeTime3 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod3.CriticalCareDischargeTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)


--------------------------------------------------------------------------
----DATA GROUP: NEONATAL CRITICAL CARE - ADMISSION CHARACTERISTICS
--------------------------------------------------------------------------

--	,NeonatalCriticalCareLocalIdentifier4 = NeonatalCriticalCarePeriod4.CriticalCareLocalIdentifier

--	,NeonatalCriticalCareStartDate4 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod4.CriticalCareStartTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)

--	,NeonatalCriticalCareStartTime4 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod4.CriticalCareStartTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)

--	,NeonatalCriticalCareUnitFunctionCode4 = NeonatalCriticalCarePeriod4.CriticalCareUnitFunctionCode

--	,NeonatalCriticalCareGestationLengthAtDelivery4 = NeonatalCriticalCarePeriod4.NeonatalCriticalCareGestationLengthAtDelivery

--------------------------------------------------------------------------
----DATA GROUP: NEONATAL DAILY CARE - ACTIVITY CHARACTERISTICS
--------------------------------------------------------------------------

----Repeating Group

--	--,NeonatalCriticalCareActivityDate4 = null
--	--,NeonatalCriticalCareActivityCode4 = null
--	--,NeonatalCriticalCareHighCostDrugs4 = null

--------------------------------------------------------------------------
----DATA GROUP: NEONATAL CRITICAL CARE - DISCHARGE CHARACTERISTICS
--------------------------------------------------------------------------

--	,NeonatalCriticalCareDischargeDate4 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod4.CriticalCareDischargeTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)
--	,NeonatalCriticalCareDischargeTime4 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod4.CriticalCareDischargeTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)


--------------------------------------------------------------------------
----DATA GROUP: NEONATAL CRITICAL CARE - ADMISSION CHARACTERISTICS
--------------------------------------------------------------------------

--	,NeonatalCriticalCareLocalIdentifier5 = NeonatalCriticalCarePeriod5.CriticalCareLocalIdentifier

--	,NeonatalCriticalCareStartDate5 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod5.CriticalCareStartTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)

--	,NeonatalCriticalCareStartTime5 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod5.CriticalCareStartTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)

--	,NeonatalCriticalCareUnitFunctionCode5 = NeonatalCriticalCarePeriod5.CriticalCareUnitFunctionCode

--	,NeonatalCriticalCareGestationLengthAtDelivery5 = NeonatalCriticalCarePeriod5.NeonatalCriticalCareGestationLengthAtDelivery

--------------------------------------------------------------------------
----DATA GROUP: NEONATAL DAILY CARE - ACTIVITY CHARACTERISTICS
--------------------------------------------------------------------------

----Repeating Group

--	--,NeonatalCriticalCareActivityDate5 = null
--	--,NeonatalCriticalCareActivityCode5 = null
--	--,NeonatalCriticalCareHighCostDrugs5 = null

--------------------------------------------------------------------------
----DATA GROUP: NEONATAL CRITICAL CARE - DISCHARGE CHARACTERISTICS
--------------------------------------------------------------------------

--	,NeonatalCriticalCareDischargeDate5 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod5.CriticalCareDischargeTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)
--	,NeonatalCriticalCareDischargeTime5 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod5.CriticalCareDischargeTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)


--------------------------------------------------------------------------
----DATA GROUP: NEONATAL CRITICAL CARE - ADMISSION CHARACTERISTICS
--------------------------------------------------------------------------

--	,NeonatalCriticalCareLocalIdentifier6 = NeonatalCriticalCarePeriod6.CriticalCareLocalIdentifier

--	,NeonatalCriticalCareStartDate6 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod6.CriticalCareStartTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)

--	,NeonatalCriticalCareStartTime6 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod6.CriticalCareStartTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)

--	,NeonatalCriticalCareUnitFunctionCode6 = NeonatalCriticalCarePeriod6.CriticalCareUnitFunctionCode

--	,NeonatalCriticalCareGestationLengthAtDelivery6 = NeonatalCriticalCarePeriod6.NeonatalCriticalCareGestationLengthAtDelivery

--------------------------------------------------------------------------
----DATA GROUP: NEONATAL DAILY CARE - ACTIVITY CHARACTERISTICS
--------------------------------------------------------------------------

----Repeating Group

--	--,NeonatalCriticalCareActivityDate6 = null
--	--,NeonatalCriticalCareActivityCode6 = null
--	--,NeonatalCriticalCareHighCostDrugs6 = null

--------------------------------------------------------------------------
----DATA GROUP: NEONATAL CRITICAL CARE - DISCHARGE CHARACTERISTICS
--------------------------------------------------------------------------

--	,NeonatalCriticalCareDischargeDate6 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod6.CriticalCareDischargeTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)
--	,NeonatalCriticalCareDischargeTime6 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod6.CriticalCareDischargeTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)


--------------------------------------------------------------------------
----DATA GROUP: NEONATAL CRITICAL CARE - ADMISSION CHARACTERISTICS
--------------------------------------------------------------------------

--	,NeonatalCriticalCareLocalIdentifier7 = NeonatalCriticalCarePeriod7.CriticalCareLocalIdentifier

--	,NeonatalCriticalCareStartDate7 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod7.CriticalCareStartTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)

--	,NeonatalCriticalCareStartTime7 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod7.CriticalCareStartTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)

--	,NeonatalCriticalCareUnitFunctionCode7 = NeonatalCriticalCarePeriod7.CriticalCareUnitFunctionCode

--	,NeonatalCriticalCareGestationLengthAtDelivery7 = NeonatalCriticalCarePeriod7.NeonatalCriticalCareGestationLengthAtDelivery

--------------------------------------------------------------------------
----DATA GROUP: NEONATAL DAILY CARE - ACTIVITY CHARACTERISTICS
--------------------------------------------------------------------------

----Repeating Group

--	--,NeonatalCriticalCareActivityDate7 = null
--	--,NeonatalCriticalCareActivityCode7 = null
--	--,NeonatalCriticalCareHighCostDrugs7 = null

--------------------------------------------------------------------------
----DATA GROUP: NEONATAL CRITICAL CARE - DISCHARGE CHARACTERISTICS
--------------------------------------------------------------------------

--	,NeonatalCriticalCareDischargeDate7 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod7.CriticalCareDischargeTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)
--	,NeonatalCriticalCareDischargeTime7 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod7.CriticalCareDischargeTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)


--------------------------------------------------------------------------
----DATA GROUP: NEONATAL CRITICAL CARE - ADMISSION CHARACTERISTICS
--------------------------------------------------------------------------

--	,NeonatalCriticalCareLocalIdentifier8 = NeonatalCriticalCarePeriod8.CriticalCareLocalIdentifier

--	,NeonatalCriticalCareStartDate8 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod8.CriticalCareStartTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)

--	,NeonatalCriticalCareStartTime8 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod8.CriticalCareStartTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)

--	,NeonatalCriticalCareUnitFunctionCode8 = NeonatalCriticalCarePeriod8.CriticalCareUnitFunctionCode

--	,NeonatalCriticalCareGestationLengthAtDelivery8 = NeonatalCriticalCarePeriod8.NeonatalCriticalCareGestationLengthAtDelivery

--------------------------------------------------------------------------
----DATA GROUP: NEONATAL DAILY CARE - ACTIVITY CHARACTERISTICS
--------------------------------------------------------------------------

----Repeating Group

--	--,NeonatalCriticalCareActivityDate8 = null
--	--,NeonatalCriticalCareActivityCode8 = null
--	--,NeonatalCriticalCareHighCostDrugs8 = null

--------------------------------------------------------------------------
----DATA GROUP: NEONATAL CRITICAL CARE - DISCHARGE CHARACTERISTICS
--------------------------------------------------------------------------

--	,NeonatalCriticalCareDischargeDate8 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod8.CriticalCareDischargeTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)
--	,NeonatalCriticalCareDischargeTime8 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod8.CriticalCareDischargeTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)


--------------------------------------------------------------------------
----DATA GROUP: NEONATAL CRITICAL CARE - ADMISSION CHARACTERISTICS
--------------------------------------------------------------------------

--	,NeonatalCriticalCareLocalIdentifier9 = NeonatalCriticalCarePeriod9.CriticalCareLocalIdentifier

--	,NeonatalCriticalCareStartDate9 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod9.CriticalCareStartTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)

--	,NeonatalCriticalCareStartTime9 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod9.CriticalCareStartTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)

--	,NeonatalCriticalCareUnitFunctionCode9 = NeonatalCriticalCarePeriod9.CriticalCareUnitFunctionCode

--	,NeonatalCriticalCareGestationLengthAtDelivery9 = NeonatalCriticalCarePeriod9.NeonatalCriticalCareGestationLengthAtDelivery

--------------------------------------------------------------------------
----DATA GROUP: NEONATAL DAILY CARE - ACTIVITY CHARACTERISTICS
--------------------------------------------------------------------------

----Repeating Group

--	--,NeonatalCriticalCareActivityDate9 = null
--	--,NeonatalCriticalCareActivityCode9 = null
--	--,NeonatalCriticalCareHighCostDrugs9 = null

--------------------------------------------------------------------------
----DATA GROUP: NEONATAL CRITICAL CARE - DISCHARGE CHARACTERISTICS
--------------------------------------------------------------------------

--	,NeonatalCriticalCareDischargeDate9 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod9.CriticalCareDischargeTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)
--	,NeonatalCriticalCareDischargeTime9 =
--		left(
--			replace(
--				 CONVERT(varchar, NeonatalCriticalCarePeriod9.CriticalCareDischargeTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)



--UHSM do not have a paediatric system - these come through the adult critical care records

--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC CRITICAL CARE PERIOD
--------------------------------------------------------------------------

----FUNCTION: See CRITICAL CARE PERIOD
----To carry the details of the first 9 Critical Care Periods for care provided using Paediatric Care facilities.

--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC CRITICAL CARE - ADMISSION CHARACTERISTICS
--------------------------------------------------------------------------

--	,PaediatricCriticalCareLocalIdentifier1 = PaediatricCriticalCarePeriod1.CriticalCareLocalIdentifier

--	,PaediatricCriticalCareStartDate1 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod1.CriticalCareStartTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)

--	,PaediatricCriticalCareStartTime1 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod1.CriticalCareStartTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)

--	,PaediatricCriticalCareUnitFunctionCode1 = PaediatricCriticalCarePeriod1.CriticalCareUnitFunctionCode

--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC DAILY CARE - ACTIVITY CHARACTERISTICS
--------------------------------------------------------------------------

----Repeating Group

--	--,PaediatricCriticalCareActivityDate1 = null
--	--,PaediatricCriticalCareActivityCode1 = null
--	--,PaediatricCriticalCareHighCostDrugs1 = null

--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC CRITICAL CARE - DISCHARGE CHARACTERISTICS
--------------------------------------------------------------------------

--	,PaediatricCriticalCareDischargeDate1 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod1.CriticalCareDischargeTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)
--	,PaediatricCriticalCareDischargeTime1 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod1.CriticalCareDischargeTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)


--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC CRITICAL CARE - ADMISSION CHARACTERISTICS
--------------------------------------------------------------------------

--	,PaediatricCriticalCareLocalIdentifier2 = PaediatricCriticalCarePeriod2.CriticalCareLocalIdentifier

--	,PaediatricCriticalCareStartDate2 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod2.CriticalCareStartTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)

--	,PaediatricCriticalCareStartTime2 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod2.CriticalCareStartTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)

--	,PaediatricCriticalCareUnitFunctionCode2 = PaediatricCriticalCarePeriod2.CriticalCareUnitFunctionCode

--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC DAILY CARE - ACTIVITY CHARACTERISTICS
--------------------------------------------------------------------------

----Repeating Group

--	--,PaediatricCriticalCareActivityDate2 = null
--	--,PaediatricCriticalCareActivityCode2 = null
--	--,PaediatricCriticalCareHighCostDrugs2 = null

--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC CRITICAL CARE - DISCHARGE CHARACTERISTICS
--------------------------------------------------------------------------

--	,PaediatricCriticalCareDischargeDate2 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod2.CriticalCareDischargeTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)
--	,PaediatricCriticalCareDischargeTime2 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod2.CriticalCareDischargeTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)


--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC CRITICAL CARE - ADMISSION CHARACTERISTICS
--------------------------------------------------------------------------

--	,PaediatricCriticalCareLocalIdentifier3 = PaediatricCriticalCarePeriod3.CriticalCareLocalIdentifier

--	,PaediatricCriticalCareStartDate3 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod3.CriticalCareStartTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)

--	,PaediatricCriticalCareStartTime3 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod3.CriticalCareStartTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)

--	,PaediatricCriticalCareUnitFunctionCode3 = PaediatricCriticalCarePeriod3.CriticalCareUnitFunctionCode

--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC DAILY CARE - ACTIVITY CHARACTERISTICS
--------------------------------------------------------------------------

----Repeating Group

--	--,PaediatricCriticalCareActivityDate3 = null
--	--,PaediatricCriticalCareActivityCode3 = null
--	--,PaediatricCriticalCareHighCostDrugs3 = null

--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC CRITICAL CARE - DISCHARGE CHARACTERISTICS
--------------------------------------------------------------------------

--	,PaediatricCriticalCareDischargeDate3 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod3.CriticalCareDischargeTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)
--	,PaediatricCriticalCareDischargeTime3 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod3.CriticalCareDischargeTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)


--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC CRITICAL CARE - ADMISSION CHARACTERISTICS
--------------------------------------------------------------------------

--	,PaediatricCriticalCareLocalIdentifier4 = PaediatricCriticalCarePeriod4.CriticalCareLocalIdentifier

--	,PaediatricCriticalCareStartDate4 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod4.CriticalCareStartTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)

--	,PaediatricCriticalCareStartTime4 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod4.CriticalCareStartTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)

--	,PaediatricCriticalCareUnitFunctionCode4 = PaediatricCriticalCarePeriod4.CriticalCareUnitFunctionCode

--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC DAILY CARE - ACTIVITY CHARACTERISTICS
--------------------------------------------------------------------------

----Repeating Group

--	--,PaediatricCriticalCareActivityDate4 = null
--	--,PaediatricCriticalCareActivityCode4 = null
--	--,PaediatricCriticalCareHighCostDrugs4 = null

--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC CRITICAL CARE - DISCHARGE CHARACTERISTICS
--------------------------------------------------------------------------

--	,PaediatricCriticalCareDischargeDate4 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod4.CriticalCareDischargeTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)
--	,PaediatricCriticalCareDischargeTime4 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod4.CriticalCareDischargeTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)


--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC CRITICAL CARE - ADMISSION CHARACTERISTICS
--------------------------------------------------------------------------

--	,PaediatricCriticalCareLocalIdentifier5 = PaediatricCriticalCarePeriod5.CriticalCareLocalIdentifier

--	,PaediatricCriticalCareStartDate5 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod5.CriticalCareStartTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)

--	,PaediatricCriticalCareStartTime5 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod5.CriticalCareStartTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)

--	,PaediatricCriticalCareUnitFunctionCode5 = PaediatricCriticalCarePeriod5.CriticalCareUnitFunctionCode

--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC DAILY CARE - ACTIVITY CHARACTERISTICS
--------------------------------------------------------------------------

----Repeating Group

--	--,PaediatricCriticalCareActivityDate5 = null
--	--,PaediatricCriticalCareActivityCode5 = null
--	--,PaediatricCriticalCareHighCostDrugs5 = null

--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC CRITICAL CARE - DISCHARGE CHARACTERISTICS
--------------------------------------------------------------------------

--	,PaediatricCriticalCareDischargeDate5 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod5.CriticalCareDischargeTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)
--	,PaediatricCriticalCareDischargeTime5 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod5.CriticalCareDischargeTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)


--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC CRITICAL CARE - ADMISSION CHARACTERISTICS
--------------------------------------------------------------------------

--	,PaediatricCriticalCareLocalIdentifier6 = PaediatricCriticalCarePeriod6.CriticalCareLocalIdentifier

--	,PaediatricCriticalCareStartDate6 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod6.CriticalCareStartTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)

--	,PaediatricCriticalCareStartTime6 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod6.CriticalCareStartTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)

--	,PaediatricCriticalCareUnitFunctionCode6 = PaediatricCriticalCarePeriod6.CriticalCareUnitFunctionCode

--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC DAILY CARE - ACTIVITY CHARACTERISTICS
--------------------------------------------------------------------------

----Repeating Group

--	--,PaediatricCriticalCareActivityDate6 = null
--	--,PaediatricCriticalCareActivityCode6 = null
--	--,PaediatricCriticalCareHighCostDrugs6 = null

--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC CRITICAL CARE - DISCHARGE CHARACTERISTICS
--------------------------------------------------------------------------

--	,PaediatricCriticalCareDischargeDate6 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod6.CriticalCareDischargeTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)
--	,PaediatricCriticalCareDischargeTime6 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod6.CriticalCareDischargeTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)


--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC CRITICAL CARE - ADMISSION CHARACTERISTICS
--------------------------------------------------------------------------

--	,PaediatricCriticalCareLocalIdentifier7 = PaediatricCriticalCarePeriod7.CriticalCareLocalIdentifier

--	,PaediatricCriticalCareStartDate7 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod7.CriticalCareStartTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)

--	,PaediatricCriticalCareStartTime7 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod7.CriticalCareStartTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)

--	,PaediatricCriticalCareUnitFunctionCode7 = PaediatricCriticalCarePeriod7.CriticalCareUnitFunctionCode

--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC DAILY CARE - ACTIVITY CHARACTERISTICS
--------------------------------------------------------------------------

----Repeating Group

--	--,PaediatricCriticalCareActivityDate7 = null
--	--,PaediatricCriticalCareActivityCode7 = null
--	--,PaediatricCriticalCareHighCostDrugs7 = null

--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC CRITICAL CARE - DISCHARGE CHARACTERISTICS
--------------------------------------------------------------------------

--	,PaediatricCriticalCareDischargeDate7 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod7.CriticalCareDischargeTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)
--	,PaediatricCriticalCareDischargeTime7 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod7.CriticalCareDischargeTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)


--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC CRITICAL CARE - ADMISSION CHARACTERISTICS
--------------------------------------------------------------------------

--	,PaediatricCriticalCareLocalIdentifier8 = PaediatricCriticalCarePeriod8.CriticalCareLocalIdentifier

--	,PaediatricCriticalCareStartDate8 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod8.CriticalCareStartTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)

--	,PaediatricCriticalCareStartTime8 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod8.CriticalCareStartTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)

--	,PaediatricCriticalCareUnitFunctionCode8 = PaediatricCriticalCarePeriod8.CriticalCareUnitFunctionCode

--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC DAILY CARE - ACTIVITY CHARACTERISTICS
--------------------------------------------------------------------------

----Repeating Group

--	--,PaediatricCriticalCareActivityDate8 = null
--	--,PaediatricCriticalCareActivityCode8 = null
--	--,PaediatricCriticalCareHighCostDrugs8 = null

--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC CRITICAL CARE - DISCHARGE CHARACTERISTICS
--------------------------------------------------------------------------

--	,PaediatricCriticalCareDischargeDate8 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod8.CriticalCareDischargeTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)
--	,PaediatricCriticalCareDischargeTime8 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod8.CriticalCareDischargeTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)


--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC CRITICAL CARE - ADMISSION CHARACTERISTICS
--------------------------------------------------------------------------

--	,PaediatricCriticalCareLocalIdentifier9 = PaediatricCriticalCarePeriod9.CriticalCareLocalIdentifier

--	,PaediatricCriticalCareStartDate9 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod9.CriticalCareStartTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)

--	,PaediatricCriticalCareStartTime9 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod9.CriticalCareStartTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)

--	,PaediatricCriticalCareUnitFunctionCode9 = PaediatricCriticalCarePeriod9.CriticalCareUnitFunctionCode

--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC DAILY CARE - ACTIVITY CHARACTERISTICS
--------------------------------------------------------------------------

----Repeating Group

--	--,PaediatricCriticalCareActivityDate9 = null
--	--,PaediatricCriticalCareActivityCode9 = null
--	--,PaediatricCriticalCareHighCostDrugs9 = null

--------------------------------------------------------------------------
----DATA GROUP: PAEDIATRIC CRITICAL CARE - DISCHARGE CHARACTERISTICS
--------------------------------------------------------------------------

--	,PaediatricCriticalCareDischargeDate9 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod9.CriticalCareDischargeTime, 120)
--				,':'
--				,''
--			)
--			,10
--		)
--	,PaediatricCriticalCareDischargeTime9 =
--		left(
--			replace(
--				 CONVERT(varchar, PaediatricCriticalCarePeriod9.CriticalCareDischargeTime, 108)
--				,':'
--				,''
--			)
--			,6
--		)




------------------------------------------------------------------------
--DATA GROUP: ADULT CRITICAL CARE PERIOD
------------------------------------------------------------------------

--FUNCTION: See CRITICAL CARE PERIOD
--To carry the details of the first 9 Critical Care Periods for care provided using Adult Care facilities.

------------------------------------------------------------------------
--DATA GROUP: ADULT CRITICAL CARE - ADMISSION CHARACTERISTICS
------------------------------------------------------------------------

	,AdultCriticalCareLocalIdentifier1 = AdultCriticalCarePeriod1.CriticalCareLocalIdentifier

	,AdultCriticalCareStartDate1 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod1.CriticalCareStartTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareStartTime1 =
		left(
			CONVERT(varchar, AdultCriticalCarePeriod1.CriticalCareStartTime, 108)
			,8
		)

	,AdultCriticalCareUnitFunctionCode1 = AdultCriticalCarePeriod1.CriticalCareUnitFunctionCode

	,AdultCriticalCareUnitBedConfigurationCode1 = null --Optional
	,AdultCriticalCareAdmissionSourceCode1 = null --Optional
	,AdultCriticalCareSourceLocationCode1 = null --Optional
	,AdultCriticalCareAdmissionTypeCode1 = null --Optional


------------------------------------------------------------------------
--DATA GROUP: ADULT DAILY CARE - ACTIVITY CHARACTERISTICS
------------------------------------------------------------------------

	,AdvancedRespiratorySupportDays1 = AdultCriticalCarePeriod1.AdvancedRespiratorySupportDays
	,BasicRespiratorySupportDays1 = AdultCriticalCarePeriod1.BasicRespiratorySupportDays
	,AdvancedCardiovascularSupportDays1 = AdultCriticalCarePeriod1.AdvancedCardiovascularSupportDays
	,BasicCardiovascularSupportDays1 = AdultCriticalCarePeriod1.BasicCardiovascularSupportDays
	,RenalSupportDays1 = AdultCriticalCarePeriod1.RenalSupportDays
	,NeurologicalSupportDays1 = AdultCriticalCarePeriod1.NeurologicalSupportDays
	,GastroIntestinalSupportDays1 = AdultCriticalCarePeriod1.GastrointestinalSupportDays
	,DermatologicalSupportDays1 = AdultCriticalCarePeriod1.DermatologicalSupportDays
	,LiverSupportDays1 = AdultCriticalCarePeriod1.LiverSupportDays
	,OrganSupportMaximum1 = null --Optional
	,CriticalCareLevel2Days1 = AdultCriticalCarePeriod1.Level2SupportDays
	,CriticalCareLevel3Days1 = AdultCriticalCarePeriod1.Level3SupportDays

------------------------------------------------------------------------
--DATA GROUP: ADULT CRITICAL CARE - DISCHARGE CHARACTERISTICS
------------------------------------------------------------------------

	,AdultCriticalCareDischargeDate1 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod1.CriticalCareDischargeTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareDischargeTime1 =
		left(
			CONVERT(varchar, AdultCriticalCarePeriod1.CriticalCareDischargeTime, 108)
			,8
		)

	,AdultCriticalCareDischargeReadyDate1 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod1.DischargeReadyTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareDischargeReadyTime1 = 
		left(
			CONVERT(varchar, AdultCriticalCarePeriod1.DischargeReadyTime, 108)
			,8
		)

	,AdultCriticalCareDischargeStatusCode1 = AdultCriticalCarePeriod1.DischargeStatusCode
	,AdultCriticalCareDischargeDestinationCode1 = AdultCriticalCarePeriod1.DischargeDestinationCode
	,AdultCriticalCareDischargeLocationCode1 = AdultCriticalCarePeriod1.DischargeLocationCode

------------------------------------------------------------------------
--DATA GROUP: ADULT CRITICAL CARE - ADMISSION CHARACTERISTICS
------------------------------------------------------------------------

	,AdultCriticalCareLocalIdentifier2 = AdultCriticalCarePeriod2.CriticalCareLocalIdentifier

	,AdultCriticalCareStartDate2 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod2.CriticalCareStartTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareStartTime2 =
		left(
			CONVERT(varchar, AdultCriticalCarePeriod2.CriticalCareStartTime, 108)
			,8
		)

	,AdultCriticalCareUnitFunctionCode2 = AdultCriticalCarePeriod2.CriticalCareUnitFunctionCode

	,AdultCriticalCareUnitBedConfigurationCode2 = null --Optional
	,AdultCriticalCareAdmissionSourceCode2 = null --Optional
	,AdultCriticalCareSourceLocationCode2 = null --Optional
	,AdultCriticalCareAdmissionTypeCode2 = null --Optional


------------------------------------------------------------------------
--DATA GROUP: ADULT DAILY CARE - ACTIVITY CHARACTERISTICS
------------------------------------------------------------------------

	,AdvancedRespiratorySupportDays2 = AdultCriticalCarePeriod2.AdvancedRespiratorySupportDays
	,BasicRespiratorySupportDays2 = AdultCriticalCarePeriod2.BasicRespiratorySupportDays
	,AdvancedCardiovascularSupportDays2 = AdultCriticalCarePeriod2.AdvancedCardiovascularSupportDays
	,BasicCardiovascularSupportDays2 = AdultCriticalCarePeriod2.BasicCardiovascularSupportDays
	,RenalSupportDays2 = AdultCriticalCarePeriod2.RenalSupportDays
	,NeurologicalSupportDays2 = AdultCriticalCarePeriod2.NeurologicalSupportDays
	,GastroIntestinalSupportDays2 = AdultCriticalCarePeriod2.GastrointestinalSupportDays
	,DermatologicalSupportDays2 = AdultCriticalCarePeriod2.DermatologicalSupportDays
	,LiverSupportDays2 = AdultCriticalCarePeriod2.LiverSupportDays
	,OrganSupportMaximum2 = null --Optional
	,CriticalCareLevel2Days2 = AdultCriticalCarePeriod2.Level2SupportDays
	,CriticalCareLevel3Days2 = AdultCriticalCarePeriod2.Level3SupportDays

------------------------------------------------------------------------
--DATA GROUP: ADULT CRITICAL CARE - DISCHARGE CHARACTERISTICS
------------------------------------------------------------------------

	,AdultCriticalCareDischargeDate2 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod2.CriticalCareDischargeTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareDischargeTime2 =
		left(
			CONVERT(varchar, AdultCriticalCarePeriod2.CriticalCareDischargeTime, 108)
			,8
		)

	,AdultCriticalCareDischargeReadyDate2 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod2.DischargeReadyTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareDischargeReadyTime2 = 
		left(
			CONVERT(varchar, AdultCriticalCarePeriod2.DischargeReadyTime, 108)
			,8
		)

	,AdultCriticalCareDischargeStatusCode2 = AdultCriticalCarePeriod2.DischargeStatusCode
	,AdultCriticalCareDischargeDestinationCode2 = AdultCriticalCarePeriod2.DischargeDestinationCode
	,AdultCriticalCareDischargeLocationCode2 = AdultCriticalCarePeriod2.DischargeLocationCode


------------------------------------------------------------------------
--DATA GROUP: ADULT CRITICAL CARE - ADMISSION CHARACTERISTICS
------------------------------------------------------------------------

	,AdultCriticalCareLocalIdentifier3 = AdultCriticalCarePeriod3.CriticalCareLocalIdentifier

	,AdultCriticalCareStartDate3 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod3.CriticalCareStartTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareStartTime3 =
		left(
			CONVERT(varchar, AdultCriticalCarePeriod3.CriticalCareStartTime, 108)
			,8
		)

	,AdultCriticalCareUnitFunctionCode3 = AdultCriticalCarePeriod3.CriticalCareUnitFunctionCode

	,AdultCriticalCareUnitBedConfigurationCode3 = null --Optional
	,AdultCriticalCareAdmissionSourceCode3 = null --Optional
	,AdultCriticalCareSourceLocationCode3 = null --Optional
	,AdultCriticalCareAdmissionTypeCode3 = null --Optional


------------------------------------------------------------------------
--DATA GROUP: ADULT DAILY CARE - ACTIVITY CHARACTERISTICS
------------------------------------------------------------------------

	,AdvancedRespiratorySupportDays3 = AdultCriticalCarePeriod3.AdvancedRespiratorySupportDays
	,BasicRespiratorySupportDays3 = AdultCriticalCarePeriod3.BasicRespiratorySupportDays
	,AdvancedCardiovascularSupportDays3 = AdultCriticalCarePeriod3.AdvancedCardiovascularSupportDays
	,BasicCardiovascularSupportDays3 = AdultCriticalCarePeriod3.BasicCardiovascularSupportDays
	,RenalSupportDays3 = AdultCriticalCarePeriod3.RenalSupportDays
	,NeurologicalSupportDays3 = AdultCriticalCarePeriod3.NeurologicalSupportDays
	,GastroIntestinalSupportDays3 = AdultCriticalCarePeriod3.GastrointestinalSupportDays
	,DermatologicalSupportDays3 = AdultCriticalCarePeriod3.DermatologicalSupportDays
	,LiverSupportDays3 = AdultCriticalCarePeriod3.LiverSupportDays
	,OrganSupportMaximum3 = null --Optional
	,CriticalCareLevel2Days3 = AdultCriticalCarePeriod3.Level2SupportDays
	,CriticalCareLevel3Days3 = AdultCriticalCarePeriod3.Level3SupportDays

------------------------------------------------------------------------
--DATA GROUP: ADULT CRITICAL CARE - DISCHARGE CHARACTERISTICS
------------------------------------------------------------------------

	,AdultCriticalCareDischargeDate3 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod3.CriticalCareDischargeTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareDischargeTime3 =
		left(
			CONVERT(varchar, AdultCriticalCarePeriod3.CriticalCareDischargeTime, 108)
			,8
		)

	,AdultCriticalCareDischargeReadyDate3 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod3.DischargeReadyTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareDischargeReadyTime3 = 
		left(
			CONVERT(varchar, AdultCriticalCarePeriod3.DischargeReadyTime, 108)
			,8
		)

	,AdultCriticalCareDischargeStatusCode3 = AdultCriticalCarePeriod3.DischargeStatusCode
	,AdultCriticalCareDischargeDestinationCode3 = AdultCriticalCarePeriod3.DischargeDestinationCode
	,AdultCriticalCareDischargeLocationCode3 = AdultCriticalCarePeriod3.DischargeLocationCode


------------------------------------------------------------------------
--DATA GROUP: ADULT CRITICAL CARE - ADMISSION CHARACTERISTICS
------------------------------------------------------------------------

	,AdultCriticalCareLocalIdentifier4 = AdultCriticalCarePeriod4.CriticalCareLocalIdentifier

	,AdultCriticalCareStartDate4 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod4.CriticalCareStartTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareStartTime4 =
		left(
			CONVERT(varchar, AdultCriticalCarePeriod4.CriticalCareStartTime, 108)
			,8
		)


	,AdultCriticalCareUnitFunctionCode4 = AdultCriticalCarePeriod4.CriticalCareUnitFunctionCode

	,AdultCriticalCareUnitBedConfigurationCode4 = null --Optional
	,AdultCriticalCareAdmissionSourceCode4 = null --Optional
	,AdultCriticalCareSourceLocationCode4 = null --Optional
	,AdultCriticalCareAdmissionTypeCode4 = null --Optional


------------------------------------------------------------------------
--DATA GROUP: ADULT DAILY CARE - ACTIVITY CHARACTERISTICS
------------------------------------------------------------------------

	,AdvancedRespiratorySupportDays4 = AdultCriticalCarePeriod4.AdvancedRespiratorySupportDays
	,BasicRespiratorySupportDays4 = AdultCriticalCarePeriod4.BasicRespiratorySupportDays
	,AdvancedCardiovascularSupportDays4 = AdultCriticalCarePeriod4.AdvancedCardiovascularSupportDays
	,BasicCardiovascularSupportDays4 = AdultCriticalCarePeriod4.BasicCardiovascularSupportDays
	,RenalSupportDays4 = AdultCriticalCarePeriod4.RenalSupportDays
	,NeurologicalSupportDays4 = AdultCriticalCarePeriod4.NeurologicalSupportDays
	,GastroIntestinalSupportDays4 = AdultCriticalCarePeriod4.GastrointestinalSupportDays
	,DermatologicalSupportDays4 = AdultCriticalCarePeriod4.DermatologicalSupportDays
	,LiverSupportDays4 = AdultCriticalCarePeriod4.LiverSupportDays
	,OrganSupportMaximum4 = null --Optional
	,CriticalCareLevel2Days4 = AdultCriticalCarePeriod4.Level2SupportDays
	,CriticalCareLevel3Days4 = AdultCriticalCarePeriod4.Level3SupportDays

------------------------------------------------------------------------
--DATA GROUP: ADULT CRITICAL CARE - DISCHARGE CHARACTERISTICS
------------------------------------------------------------------------

	,AdultCriticalCareDischargeDate4 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod4.CriticalCareDischargeTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareDischargeTime4 =
		left(
			CONVERT(varchar, AdultCriticalCarePeriod4.CriticalCareDischargeTime, 108)
			,8
		)

	,AdultCriticalCareDischargeReadyDate4 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod4.DischargeReadyTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareDischargeReadyTime4 = 
		left(
			CONVERT(varchar, AdultCriticalCarePeriod4.DischargeReadyTime, 108)
			,8
		)

	,AdultCriticalCareDischargeStatusCode4 = AdultCriticalCarePeriod4.DischargeStatusCode
	,AdultCriticalCareDischargeDestinationCode4 = AdultCriticalCarePeriod4.DischargeDestinationCode
	,AdultCriticalCareDischargeLocationCode4 = AdultCriticalCarePeriod4.DischargeLocationCode


------------------------------------------------------------------------
--DATA GROUP: ADULT CRITICAL CARE - ADMISSION CHARACTERISTICS
------------------------------------------------------------------------

	,AdultCriticalCareLocalIdentifier5 = AdultCriticalCarePeriod5.CriticalCareLocalIdentifier

	,AdultCriticalCareStartDate5 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod5.CriticalCareStartTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareStartTime5 =
		left(
			CONVERT(varchar, AdultCriticalCarePeriod5.CriticalCareStartTime, 108)
			,8
		)

	,AdultCriticalCareUnitFunctionCode5 = AdultCriticalCarePeriod5.CriticalCareUnitFunctionCode

	,AdultCriticalCareUnitBedConfigurationCode5 = null --Optional
	,AdultCriticalCareAdmissionSourceCode5 = null --Optional
	,AdultCriticalCareSourceLocationCode5 = null --Optional
	,AdultCriticalCareAdmissionTypeCode5 = null --Optional


------------------------------------------------------------------------
--DATA GROUP: ADULT DAILY CARE - ACTIVITY CHARACTERISTICS
------------------------------------------------------------------------

	,AdvancedRespiratorySupportDays5 = AdultCriticalCarePeriod5.AdvancedRespiratorySupportDays
	,BasicRespiratorySupportDays5 = AdultCriticalCarePeriod5.BasicRespiratorySupportDays
	,AdvancedCardiovascularSupportDays5 = AdultCriticalCarePeriod5.AdvancedCardiovascularSupportDays
	,BasicCardiovascularSupportDays5 = AdultCriticalCarePeriod5.BasicCardiovascularSupportDays
	,RenalSupportDays5 = AdultCriticalCarePeriod5.RenalSupportDays
	,NeurologicalSupportDays5 = AdultCriticalCarePeriod5.NeurologicalSupportDays
	,GastroIntestinalSupportDays5 = AdultCriticalCarePeriod5.GastrointestinalSupportDays
	,DermatologicalSupportDays5 = AdultCriticalCarePeriod5.DermatologicalSupportDays
	,LiverSupportDays5 = AdultCriticalCarePeriod5.LiverSupportDays
	,OrganSupportMaximum5 = null --Optional
	,CriticalCareLevel2Days5 = AdultCriticalCarePeriod5.Level2SupportDays
	,CriticalCareLevel3Days5 = AdultCriticalCarePeriod5.Level3SupportDays

------------------------------------------------------------------------
--DATA GROUP: ADULT CRITICAL CARE - DISCHARGE CHARACTERISTICS
------------------------------------------------------------------------

	,AdultCriticalCareDischargeDate5 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod5.CriticalCareDischargeTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareDischargeTime5 =
		left(
			CONVERT(varchar, AdultCriticalCarePeriod5.CriticalCareDischargeTime, 108)
			,8
		)

	,AdultCriticalCareDischargeReadyDate5 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod5.DischargeReadyTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareDischargeReadyTime5 = 
		left(
			CONVERT(varchar, AdultCriticalCarePeriod5.DischargeReadyTime, 108)
			,8
		)

	,AdultCriticalCareDischargeStatusCode5 = AdultCriticalCarePeriod5.DischargeStatusCode
	,AdultCriticalCareDischargeDestinationCode5 = AdultCriticalCarePeriod5.DischargeDestinationCode
	,AdultCriticalCareDischargeLocationCode5 = AdultCriticalCarePeriod5.DischargeLocationCode

------------------------------------------------------------------------
--DATA GROUP: ADULT CRITICAL CARE - ADMISSION CHARACTERISTICS
------------------------------------------------------------------------

	,AdultCriticalCareLocalIdentifier6 = AdultCriticalCarePeriod6.CriticalCareLocalIdentifier

	,AdultCriticalCareStartDate6 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod6.CriticalCareStartTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareStartTime6 =
		left(
			CONVERT(varchar, AdultCriticalCarePeriod6.CriticalCareStartTime, 108)
			,8
		)


	,AdultCriticalCareUnitFunctionCode6 = AdultCriticalCarePeriod6.CriticalCareUnitFunctionCode

	,AdultCriticalCareUnitBedConfigurationCode6 = null --Optional
	,AdultCriticalCareAdmissionSourceCode6 = null --Optional
	,AdultCriticalCareSourceLocationCode6 = null --Optional
	,AdultCriticalCareAdmissionTypeCode6 = null --Optional


------------------------------------------------------------------------
--DATA GROUP: ADULT DAILY CARE - ACTIVITY CHARACTERISTICS
------------------------------------------------------------------------

	,AdvancedRespiratorySupportDays6 = AdultCriticalCarePeriod6.AdvancedRespiratorySupportDays
	,BasicRespiratorySupportDays6 = AdultCriticalCarePeriod6.BasicRespiratorySupportDays
	,AdvancedCardiovascularSupportDays6 = AdultCriticalCarePeriod6.AdvancedCardiovascularSupportDays
	,BasicCardiovascularSupportDays6 = AdultCriticalCarePeriod6.BasicCardiovascularSupportDays
	,RenalSupportDays6 = AdultCriticalCarePeriod6.RenalSupportDays
	,NeurologicalSupportDays6 = AdultCriticalCarePeriod6.NeurologicalSupportDays
	,GastroIntestinalSupportDays6 = AdultCriticalCarePeriod6.GastrointestinalSupportDays
	,DermatologicalSupportDays6 = AdultCriticalCarePeriod6.DermatologicalSupportDays
	,LiverSupportDays6 = AdultCriticalCarePeriod6.LiverSupportDays
	,OrganSupportMaximum6 = null --Optional
	,CriticalCareLevel2Days6 = AdultCriticalCarePeriod6.Level2SupportDays
	,CriticalCareLevel3Days6 = AdultCriticalCarePeriod6.Level3SupportDays

------------------------------------------------------------------------
--DATA GROUP: ADULT CRITICAL CARE - DISCHARGE CHARACTERISTICS
------------------------------------------------------------------------

	,AdultCriticalCareDischargeDate6 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod6.CriticalCareDischargeTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareDischargeTime6 =
		left(
			CONVERT(varchar, AdultCriticalCarePeriod6.CriticalCareDischargeTime, 108)
			,8
		)

	,AdultCriticalCareDischargeReadyDate6 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod6.DischargeReadyTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareDischargeReadyTime6 = 
		left(
			CONVERT(varchar, AdultCriticalCarePeriod6.DischargeReadyTime, 108)
			,8
		)

	,AdultCriticalCareDischargeStatusCode6 = AdultCriticalCarePeriod6.DischargeStatusCode
	,AdultCriticalCareDischargeDestinationCode6 = AdultCriticalCarePeriod6.DischargeDestinationCode
	,AdultCriticalCareDischargeLocationCode6 = AdultCriticalCarePeriod6.DischargeLocationCode

------------------------------------------------------------------------
--DATA GROUP: ADULT CRITICAL CARE - ADMISSION CHARACTERISTICS
------------------------------------------------------------------------

	,AdultCriticalCareLocalIdentifier7 = AdultCriticalCarePeriod7.CriticalCareLocalIdentifier

	,AdultCriticalCareStartDate7 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod7.CriticalCareStartTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareStartTime7 =
		left(
			CONVERT(varchar, AdultCriticalCarePeriod7.CriticalCareStartTime, 108)
			,8
		)


	,AdultCriticalCareUnitFunctionCode7 = AdultCriticalCarePeriod7.CriticalCareUnitFunctionCode

	,AdultCriticalCareUnitBedConfigurationCode7 = null --Optional
	,AdultCriticalCareAdmissionSourceCode7 = null --Optional
	,AdultCriticalCareSourceLocationCode7 = null --Optional
	,AdultCriticalCareAdmissionTypeCode7 = null --Optional


------------------------------------------------------------------------
--DATA GROUP: ADULT DAILY CARE - ACTIVITY CHARACTERISTICS
------------------------------------------------------------------------

	,AdvancedRespiratorySupportDays7 = AdultCriticalCarePeriod7.AdvancedRespiratorySupportDays
	,BasicRespiratorySupportDays7 = AdultCriticalCarePeriod7.BasicRespiratorySupportDays
	,AdvancedCardiovascularSupportDays7 = AdultCriticalCarePeriod7.AdvancedCardiovascularSupportDays
	,BasicCardiovascularSupportDays7 = AdultCriticalCarePeriod7.BasicCardiovascularSupportDays
	,RenalSupportDays7 = AdultCriticalCarePeriod7.RenalSupportDays
	,NeurologicalSupportDays7 = AdultCriticalCarePeriod7.NeurologicalSupportDays
	,GastroIntestinalSupportDays7 = AdultCriticalCarePeriod7.GastrointestinalSupportDays
	,DermatologicalSupportDays7 = AdultCriticalCarePeriod7.DermatologicalSupportDays
	,LiverSupportDays7 = AdultCriticalCarePeriod7.LiverSupportDays
	,OrganSupportMaximum7 = null --Optional
	,CriticalCareLevel2Days7 = AdultCriticalCarePeriod7.Level2SupportDays
	,CriticalCareLevel3Days7 = AdultCriticalCarePeriod7.Level3SupportDays

------------------------------------------------------------------------
--DATA GROUP: ADULT CRITICAL CARE - DISCHARGE CHARACTERISTICS
------------------------------------------------------------------------

	,AdultCriticalCareDischargeDate7 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod7.CriticalCareDischargeTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareDischargeTime7 =
		left(
			CONVERT(varchar, AdultCriticalCarePeriod7.CriticalCareDischargeTime, 108)
			,8
		)

	,AdultCriticalCareDischargeReadyDate7 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod7.DischargeReadyTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareDischargeReadyTime7 = 
		left(
			CONVERT(varchar, AdultCriticalCarePeriod7.DischargeReadyTime, 108)
			,8
		)

	,AdultCriticalCareDischargeStatusCode7 = AdultCriticalCarePeriod7.DischargeStatusCode
	,AdultCriticalCareDischargeDestinationCode7 = AdultCriticalCarePeriod7.DischargeDestinationCode
	,AdultCriticalCareDischargeLocationCode7 = AdultCriticalCarePeriod7.DischargeLocationCode


------------------------------------------------------------------------
--DATA GROUP: ADULT CRITICAL CARE - ADMISSION CHARACTERISTICS
------------------------------------------------------------------------

	,AdultCriticalCareLocalIdentifier8 = AdultCriticalCarePeriod8.CriticalCareLocalIdentifier

	,AdultCriticalCareStartDate8 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod8.CriticalCareStartTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareStartTime8 =
		left(
			CONVERT(varchar, AdultCriticalCarePeriod8.CriticalCareStartTime, 108)
			,8
		)


	,AdultCriticalCareUnitFunctionCode8 = AdultCriticalCarePeriod8.CriticalCareUnitFunctionCode

	,AdultCriticalCareUnitBedConfigurationCode8 = null --Optional
	,AdultCriticalCareAdmissionSourceCode8 = null --Optional
	,AdultCriticalCareSourceLocationCode8 = null --Optional
	,AdultCriticalCareAdmissionTypeCode8 = null --Optional


------------------------------------------------------------------------
--DATA GROUP: ADULT DAILY CARE - ACTIVITY CHARACTERISTICS
------------------------------------------------------------------------

	,AdvancedRespiratorySupportDays8 = AdultCriticalCarePeriod8.AdvancedRespiratorySupportDays
	,BasicRespiratorySupportDays8 = AdultCriticalCarePeriod8.BasicRespiratorySupportDays
	,AdvancedCardiovascularSupportDays8 = AdultCriticalCarePeriod8.AdvancedCardiovascularSupportDays
	,BasicCardiovascularSupportDays8 = AdultCriticalCarePeriod8.BasicCardiovascularSupportDays
	,RenalSupportDays8 = AdultCriticalCarePeriod8.RenalSupportDays
	,NeurologicalSupportDays8 = AdultCriticalCarePeriod8.NeurologicalSupportDays
	,GastroIntestinalSupportDays8 = AdultCriticalCarePeriod8.GastrointestinalSupportDays
	,DermatologicalSupportDays8 = AdultCriticalCarePeriod8.DermatologicalSupportDays
	,LiverSupportDays8 = AdultCriticalCarePeriod8.LiverSupportDays
	,OrganSupportMaximum8 = null --Optional
	,CriticalCareLevel2Days8 = AdultCriticalCarePeriod8.Level2SupportDays
	,CriticalCareLevel3Days8 = AdultCriticalCarePeriod8.Level3SupportDays

------------------------------------------------------------------------
--DATA GROUP: ADULT CRITICAL CARE - DISCHARGE CHARACTERISTICS
------------------------------------------------------------------------

	,AdultCriticalCareDischargeDate8 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod8.CriticalCareDischargeTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareDischargeTime8 =
		left(
			CONVERT(varchar, AdultCriticalCarePeriod8.CriticalCareDischargeTime, 108)
			,8
		)

	,AdultCriticalCareDischargeReadyDate8 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod8.DischargeReadyTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareDischargeReadyTime8 = 
		left(
			CONVERT(varchar, AdultCriticalCarePeriod8.DischargeReadyTime, 108)
			,8
		)

	,AdultCriticalCareDischargeStatusCode8 = AdultCriticalCarePeriod8.DischargeStatusCode
	,AdultCriticalCareDischargeDestinationCode8 = AdultCriticalCarePeriod8.DischargeDestinationCode
	,AdultCriticalCareDischargeLocationCode8 = AdultCriticalCarePeriod8.DischargeLocationCode


------------------------------------------------------------------------
--DATA GROUP: ADULT CRITICAL CARE - ADMISSION CHARACTERISTICS
------------------------------------------------------------------------

	,AdultCriticalCareLocalIdentifier9 = AdultCriticalCarePeriod9.CriticalCareLocalIdentifier

	,AdultCriticalCareStartDate9 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod9.CriticalCareStartTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareStartTime9 =
		left(
			CONVERT(varchar, AdultCriticalCarePeriod9.CriticalCareStartTime, 108)
			,8
		)


	,AdultCriticalCareUnitFunctionCode9 = AdultCriticalCarePeriod9.CriticalCareUnitFunctionCode

	,AdultCriticalCareUnitBedConfigurationCode9 = null --Optional
	,AdultCriticalCareAdmissionSourceCode9 = null --Optional
	,AdultCriticalCareSourceLocationCode9 = null --Optional
	,AdultCriticalCareAdmissionTypeCode9 = null --Optional


------------------------------------------------------------------------
--DATA GROUP: ADULT DAILY CARE - ACTIVITY CHARACTERISTICS
------------------------------------------------------------------------

	,AdvancedRespiratorySupportDays9 = AdultCriticalCarePeriod9.AdvancedRespiratorySupportDays
	,BasicRespiratorySupportDays9 = AdultCriticalCarePeriod9.BasicRespiratorySupportDays
	,AdvancedCardiovascularSupportDays9 = AdultCriticalCarePeriod9.AdvancedCardiovascularSupportDays
	,BasicCardiovascularSupportDays9 = AdultCriticalCarePeriod9.BasicCardiovascularSupportDays
	,RenalSupportDays9 = AdultCriticalCarePeriod9.RenalSupportDays
	,NeurologicalSupportDays9 = AdultCriticalCarePeriod9.NeurologicalSupportDays
	,GastroIntestinalSupportDays9 = AdultCriticalCarePeriod9.GastrointestinalSupportDays
	,DermatologicalSupportDays9 = AdultCriticalCarePeriod9.DermatologicalSupportDays
	,LiverSupportDays9 = AdultCriticalCarePeriod9.LiverSupportDays
	,OrganSupportMaximum9 = null --Optional
	,CriticalCareLevel2Days9 = AdultCriticalCarePeriod9.Level2SupportDays
	,CriticalCareLevel3Days9 = AdultCriticalCarePeriod9.Level3SupportDays

------------------------------------------------------------------------
--DATA GROUP: ADULT CRITICAL CARE - DISCHARGE CHARACTERISTICS
------------------------------------------------------------------------

	,AdultCriticalCareDischargeDate9 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod9.CriticalCareDischargeTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareDischargeTime9 =
		left(
			CONVERT(varchar, AdultCriticalCarePeriod9.CriticalCareDischargeTime, 108)
			,8
		)

	,AdultCriticalCareDischargeReadyDate9 =
		left(
			replace(
				 CONVERT(varchar, AdultCriticalCarePeriod9.DischargeReadyTime, 120)
				,':'
				,''
			)
			,10
		)

	,AdultCriticalCareDischargeReadyTime9 = 
		left(
			CONVERT(varchar, AdultCriticalCarePeriod9.DischargeReadyTime, 108)
			,8
		)

	,AdultCriticalCareDischargeStatusCode9 = AdultCriticalCarePeriod9.DischargeStatusCode
	,AdultCriticalCareDischargeDestinationCode9 = AdultCriticalCarePeriod9.DischargeDestinationCode
	,AdultCriticalCareDischargeLocationCode9 = AdultCriticalCarePeriod9.DischargeLocationCode



------------------------------------------------------------------------
--DATA GROUP: GP REGISTRATION
------------------------------------------------------------------------

	,RegisteredGpCode = GP.ProfessionalCarerMainCode

	,RegisteredGpPracticeCode = Practice.OrganisationLocalCode


------------------------------------------------------------------------
--DATA GROUP: REFERRER
------------------------------------------------------------------------

	,ReferrerCode =
		case
		when SourceOfReferral.MappedCode = '17' --National Screening Programme
		then 'R9999981'

		when SourceOfReferral.MappedCode = '97' --Other
		then 'R9999981'
		else
			case
			when Referral.ReferredByConsultantCode is null
			then 'X9999998'

			when Referral.ReferredByConsultantCode = 198272 --Birth
			then 'X9999998'

			when Referral.ReferredByConsultantCode in
				(
				 205632 --not specified
				,10540092 --bad mapping
				,10100528 --Dr Day?
				)
			then 'C9999998'

			when Referral.ReferredByConsultantCode in
				(
				 10000470 --Midwife
				,150002484 --Specialist Nurse (NIBA)
				,10000472 --Nurse Led
				,198273 --Nurse
				,150000128 --Nurse
				,10539696 --Nurse
				,10540170 --Nurse Led (Gyn)
				,10539309 --PHTH
				,10542422 --Physio
				,10543349 --Physio
				,10542436 --Physio
				,10542421 --Physio
				,10542449 --Physio
				)
			then 'R9999981'

			else Referrer.ProfessionalCarerMainCode
			end
		end


	--,ReferringOrganisationCode =
	--	case
	--	when ReferrerOrganisation.OrganisationLocalCode is null
	--	then 'X99999' --not known
	--	else ReferrerOrganisation.OrganisationLocalCode
	--	end
	,ReferringOrganisationCode =
		case
			when ReferrerOrganisation.OrganisationLocalCode is null
				then 'X99999' --not known
			--else ReferrerOrganisation.OrganisationLocalCode
			when len(ReferrerOrganisation.OrganisationLocalCode) = 3
				then ReferrerOrganisation.OrganisationLocalCode + '00'
			else ReferrerOrganisation.OrganisationLocalCode
		end

------------------------------------------------------------------------
--DATA GROUP: ELECTIVE ADMISSION LIST ENTRY
------------------------------------------------------------------------

	,DurationOfElectiveWait =
		coalesce(
			 Encounter.DurationOfElectiveWait
			,9998
		)

	,ManagementIntentionCode =
		ManagementIntention.MappedCode

	,DecidedToAdmitDate = 
		left(
			CONVERT(varchar
				,coalesce(
					 Encounter.DecisionToAdmitDate
					,Encounter.OriginalDecisionToAdmitDate
					,Encounter.AdmissionDate
				), 120)
			,10
		)

	,EarliestReasonableOfferDate = null --Optional


------------------------------------------------------------------------
--DATA GROUP: HEALTHCARE RESOURCE GROUP - ACTIVITY CHARACTERISTICS
------------------------------------------------------------------------

	,HRGCode = null --HRG4Encounter.HRGCode --Optional

	,HRGVersionNumber = null --'4.4' --Optional


------------------------------------------------------------------------
--DATA GROUP: HEALTHCARE RESOURCE GROUP - CLINICAL ACTIVITY
------------------------------------------------------------------------

	,DGVPCode = null --replace(HRG4Encounter.DominantOperationCode, '.', '') --Optional


------------------------------------------------------------------------
--Fields for CDS type 120/140
------------------------------------------------------------------------

------------------------------------------------------------------------
--DATA GROUP: DELIVERY CHARACTERISTICS
------------------------------------------------------------------------

	,PreviousPregnancies =
		coalesce(
			 Delivery.PreviousPregnancies
			,Birth.PreviousPregnancies
		)

------------------------------------------------------------------------
--DATA GROUP: PREGNANCY - ACTIVITY CHARACTERISTICS
------------------------------------------------------------------------

	,NumberOfBabies =
		coalesce(
			 Delivery.NumberOfBabies
			,Birth.NumberOfBabies
		)

------------------------------------------------------------------------
--DATA GROUP: ANTENATAL CARE - ACTIVITY CHARACTERISTICS 
------------------------------------------------------------------------

	,FirstAnteNatalAssessmentDate =
		coalesce(
			 Delivery.FirstAnteNatalAssessmentDate
			,Birth.FirstAnteNatalAssessmentDate
		)

------------------------------------------------------------------------
--DATA GROUP: ANTENATAL CARE - PERSON GROUP (RESPONSIBLE CLINICIAN)
------------------------------------------------------------------------

	,AnteNatalGpCode =
		coalesce(
			 Delivery.AnteNatalGpCode
			,Birth.AnteNatalGpCode
		)

	,AnteNatalGpPracticeCode =
		coalesce(
			 Delivery.AnteNatalGpPracticeCode
			,Birth.AnteNatalGpPracticeCode
		)

------------------------------------------------------------------------
--DATA GROUP: ANTENATAL CARE - LOCATION GROUP - DELIVERY PLACE INTENDED
------------------------------------------------------------------------

	,DeliveryLocationClassCode =
		coalesce(
			 Delivery.DeliveryLocationClassCode
			,Birth.DeliveryLocationClassCode
		)

	,DeliveryPlaceChangeReasonCode =
		coalesce(
			 Delivery.DeliveryPlaceChangeReasonCode
			,Birth.DeliveryPlaceChangeReasonCode
		)

	,IntendedDeliveryPlaceTypeCode =
		coalesce(
			 Delivery.IntendedDeliveryPlaceTypeCode
			,Birth.IntendedDeliveryPlaceTypeCode
		)

------------------------------------------------------------------------
--DATA GROUP: LABOUR/DELIVERY - ACTIVITY CHARACTERISTICS
------------------------------------------------------------------------

	,AnaestheticGivenDuringCode =
		coalesce(
			 Delivery.AnaestheticGivenDuringCode
			,Birth.AnaestheticGivenDuringCode
		)

	,AnaestheticGivenPostCode =
		coalesce(
			 Delivery.AnaestheticGivenPostCode
			,Birth.AnaestheticGivenPostCode
		)

	,GestationLength =
		coalesce(
			 Delivery.GestationLength
			,Birth.GestationLength
		)

	,LabourOnsetMethodCode =
		coalesce(
			 Delivery.LabourOnsetMethodCode
			,Birth.LabourOnsetMethodCode
		)

	,DeliveryDate =
		left(
			CONVERT(
				 varchar
				,coalesce(
					 Delivery.DeliveryDate
					,Birth.DeliveryDate
				)
				,120
			)
			,10
		)

	,DeliveryMethodCode =
		coalesce(
			 Delivery.DeliveryMethodCode
			,Birth.DeliveryMethodCode
		)

------------------------------------------------------------------------
--DATA GROUP: BIRTH OCCURRENCE
------------------------------------------------------------------------

--Repeating group held in CDS.Birth
	,Birth1AssessmentGestationLength = Birth.AssessmentGestationLength
	,Birth1BirthOrder = Birth.BirthOrder
	,Birth1BirthWeight = Birth.BirthWeight

	,Birth1DateOfBirth =
		left(
			CONVERT(varchar, Birth.DateOfBirth, 120)
			,10
		)

	,Birth1DeliveryLocationClassCode = Birth.DeliveryLocationClassCode
	,Birth1DeliveryMethodCode = Birth.DeliveryMethodCode
	,Birth1DeliveryPlaceTypeActualCode = Birth.DeliveryPlaceTypeActualCode
	,Birth1LiveOrStillBirth = Birth.LiveOrStillBirth
	,Birth1LocalPatientIdentifier = Birth.LocalPatientIdentifier
	,Birth1NHSNumber = Birth.NHSNumber
	,Birth1NHSNumberStatusIndicator = Birth.NHSNumberStatusIndicator
	,Birth1ProviderCode = Birth.ProviderCode
	,Birth1ResuscitationMethodCode = Birth.ResuscitationMethodCode
	,Birth1SexCode = Birth.SexCode
	,Birth1StatusOfPersonConductingDeliveryCode = Birth.StatusOfPersonConductingDeliveryCode

------------------------------------------------------------------------
--DATA GROUP: BIRTH OCCURRENCE
------------------------------------------------------------------------

	,ActualDeliveryPlaceTypeCode =
		coalesce(
			 Delivery.ActualDeliveryPlaceTypeCode
			,Birth.DeliveryPlaceTypeActualCode
		)

	,StatusOfPersonConductingDeliveryCode =
		coalesce(
			 Delivery.StatusOfPersonConductingDeliveryCode
			,Birth.StatusOfPersonConductingDeliveryCode
		)


------------------------------------------------------------------------
--DATA GROUP: DELIVERY OCCURRENCE PERSON IDENTITY - MOTHER
------------------------------------------------------------------------

	,MotherLocalPatientID =
		coalesce(
			 Birth.MotherLocalPatientID
			,Encounter.DistrictNo
		)

	,MotherLocalPatientIDOrganisationCode =
		coalesce(
			 Birth.MotherLocalPatientIDOrganisationCode
			,'RM200'
		)

	,MotherNHSNumber =
		coalesce(
			 Birth.MotherNHSNumber
			,Encounter.NHSNumber
		)

	,MotherNNNStatusIndicator =
		coalesce(
			 Birth.MotherNNNStatusIndicator
			,
			case
			when SensitiveDiagnosis.APCSourceUniqueID is not null
			then '07'
			when SensitiveOperation.APCSourceUniqueID is not null
			then '07'
			when Encounter.NHSNumber is null
			then '04' --Trace attempted - No match or multiple match found
			else
				case
				when Encounter.NHSNumberStatusCode = 'SUCCS' then '01'
				when Encounter.NHSNumberStatusCode = 'RESET' then '02'
				when Encounter.NHSNumberStatusCode is null then '01'
				else Encounter.NHSNumberStatusCode
				end
			end
		)

	,MotherPostcode =
		coalesce(
			 Birth.MotherPostcode
			,Encounter.Postcode
			,'ZZ99 3AZ'
		)

	,MotherPCTOfResidenceCode =
		coalesce(
			 Birth.MotherPCTOfResidenceCode
			,Postcode.PCTCode
			,PCT.OrganisationLocalCode
			,'5NT'
		)

	,MotherDateOfBirth =
		left(
			CONVERT(
				 varchar
				,coalesce(
					 Birth.MotherDateOfBirth
					,Encounter.DateOfBirth
				)
				,120
			)
			,10
		)


	,MotherPatientAddress = null

	,MotherPatient1Address =
		coalesce(
			 Birth.MotherPatient1Address
			,Encounter.PatientAddress1
		)

	,MotherPatient2Address =
		coalesce(
			 Birth.MotherPatient2Address
			,Encounter.PatientAddress2
		)

	,MotherPatient3Address =
		coalesce(
			 Birth.MotherPatient3Address
			,Encounter.PatientAddress3
		)

	,MotherPatient4Address =
		coalesce(
			 Birth.MotherPatient4Address
			,Encounter.PatientAddress4
		)

	,MotherPatient5Address =
		Birth.MotherPatient5Address


--UHSM new columns

	,PatientAddress1 =
		left(
			case
			when SensitiveDiagnosis.APCSourceUniqueID is not null
			then ''
			when SensitiveOperation.APCSourceUniqueID is not null
			then ''
			when 
				case
				when Encounter.NHSNumber is null
				then '04' --Trace attempted - No match or multiple match found
				else
					case
					when Encounter.NHSNumberStatusCode = 'SUCCS' then '01'
					when Encounter.NHSNumberStatusCode = 'RESET' then '02'
					when Encounter.NHSNumberStatusCode is null then '01'
					else Encounter.NHSNumberStatusCode
					end
				end <> '01'
			then
				coalesce(Encounter.PatientAddress1 + ' ', 'Unknown Address ')
			else ''
			end
			,35
		)

	,PatientAddress2 =
		left(
			case
			when SensitiveDiagnosis.APCSourceUniqueID is not null
			then ''
			when SensitiveOperation.APCSourceUniqueID is not null
			then ''
			when 
				case
				when Encounter.NHSNumber is null
				then '04' --Trace attempted - No match or multiple match found
				else
					case
					when Encounter.NHSNumberStatusCode = 'SUCCS' then '01'
					when Encounter.NHSNumberStatusCode = 'RESET' then '02'
					when Encounter.NHSNumberStatusCode is null then '01'
					else Encounter.NHSNumberStatusCode
					end
				end <> '01'
			then
				coalesce(Encounter.PatientAddress2 + ' ', 'Unknown Address ')
			else ''
			end
			,35
		)

	,PatientAddress3 =
		left(
			case
			when SensitiveDiagnosis.APCSourceUniqueID is not null
			then ''
			when SensitiveOperation.APCSourceUniqueID is not null
			then ''
			when 
				case
				when Encounter.NHSNumber is null
				then '04' --Trace attempted - No match or multiple match found
				else
					case
					when Encounter.NHSNumberStatusCode = 'SUCCS' then '01'
					when Encounter.NHSNumberStatusCode = 'RESET' then '02'
					when Encounter.NHSNumberStatusCode is null then '01'
					else Encounter.NHSNumberStatusCode
					end
				end <> '01'
			then
				coalesce(Encounter.PatientAddress3 + ' ', 'Unknown Address ')
			else ''
			end
			,35
		)

	,PatientAddress4 =
		left(
			case
			when SensitiveDiagnosis.APCSourceUniqueID is not null
			then ''
			when SensitiveOperation.APCSourceUniqueID is not null
			then ''
			when 
				case
				when Encounter.NHSNumber is null
				then '04' --Trace attempted - No match or multiple match found
				else
					case
					when Encounter.NHSNumberStatusCode = 'SUCCS' then '01'
					when Encounter.NHSNumberStatusCode = 'RESET' then '02'
					when Encounter.NHSNumberStatusCode is null then '01'
					else Encounter.NHSNumberStatusCode
					end
				end <> '01'
			then
				coalesce(Encounter.PatientAddress4 + ' ', 'Unknown Address ')
			else ''
			end
			,35
		)

	,PatientAddress5 = cast(null as varchar(35))

	--,HACode =
	--	coalesce(
	--		 Encounter.DHACode
	--		,Postcode.PCTCode
	--		,PCT.OrganisationLocalCode
	--		,'5NT'
	--	)


	,PatientForename = 
		left(
			case
			when SensitiveDiagnosis.APCSourceUniqueID is not null
			then null
			when SensitiveOperation.APCSourceUniqueID is not null
			then null
			when 
				case
				when Encounter.NHSNumber is null
				then '04' --Trace attempted - No match or multiple match found
				else
					case
					when Encounter.NHSNumberStatusCode = 'SUCCS' then '01'
					when Encounter.NHSNumberStatusCode = 'RESET' then '02'
					when Encounter.NHSNumberStatusCode is null then '01'
					else Encounter.NHSNumberStatusCode
					end
				end <> '01'
			then ltrim(rtrim(coalesce(Encounter.PatientForename, '')))
			else ''
			end
			,35
		)

	,PatientSurname = 
		left(
			case
			when SensitiveDiagnosis.APCSourceUniqueID is not null
			then null
			when SensitiveOperation.APCSourceUniqueID is not null
			then null
			when 
				case
				when Encounter.NHSNumber is null
				then '04' --Trace attempted - No match or multiple match found
				else
					case
					when Encounter.NHSNumberStatusCode = 'SUCCS' then '01'
					when Encounter.NHSNumberStatusCode = 'RESET' then '02'
					when Encounter.NHSNumberStatusCode is null then '01'
					else Encounter.NHSNumberStatusCode
					end
				end <> '01'
			then ltrim(rtrim(coalesce(Encounter.PatientSurname, '')))
			else ''
			end
			,35
		)


from
	APC.Encounter

inner join CDS.wrk
on	wrk.EncounterRecno = Encounter.EncounterRecno
and	wrk.CDSTypeCode in ('120', '130', '140')

left join PAS.Purchaser
on    Purchaser.PurchaserCode = Encounter.PurchaserCode

left join PAS.ReferenceValue Sex
on    Sex.ReferenceValueCode = Encounter.SexCode

left join PAS.ProfessionalCarer GP
on    GP.ProfessionalCarerCode = Encounter.RegisteredGpCode

left join PAS.Organisation Practice
--KO changed 04/10/2012
--on	Practice.OrganisationCode = Encounter.RegisteredGpPracticeCode
on	Practice.OrganisationCode = Encounter.EpisodicGpPracticeCode

left join PAS.Organisation PCT
on	PCT.OrganisationCode = Practice.ParentOrganisationCode

left join PAS.ReferenceValue EthnicOrigin
on    EthnicOrigin.ReferenceValueCode = Encounter.EthnicOriginCode

left join RF.Encounter Referral
on	Referral.SourceUniqueID = Encounter.ReferralSourceUniqueID

left join PAS.ProfessionalCarer Referrer
on    Referrer.ProfessionalCarerCode = Referral.ReferredByConsultantCode

left join PAS.ReferenceValue SourceOfReferral
on	SourceOfReferral.ReferenceValueCode = Referral.SourceOfReferralCode
and	SourceOfReferral.ReferenceDomainCode = 'SORRF'

left join PAS.Organisation ReferrerOrganisation
on    ReferrerOrganisation.OrganisationCode = Referral.ReferredByOrganisationCode

left join PAS.ReferenceValue AdminCategory
on    AdminCategory.ReferenceValueCode = Encounter.AdminCategoryCode

left join PAS.ReferenceValue AdmissionMethod
on    AdmissionMethod.ReferenceValueCode = Encounter.AdmissionMethodCode

left join PAS.ReferenceValue ManagementIntention
on    ManagementIntention.ReferenceValueCode = Encounter.ManagementIntentionCode

left join PAS.ReferenceValue AdmissionSource
on    AdmissionSource.ReferenceValueCode = Encounter.AdmissionSourceCode

left join PAS.ReferenceValue DischargeMethod
on    DischargeMethod.ReferenceValueCode = Encounter.DischargeMethodCode

left join PAS.ReferenceValue DischargeDestination
on    DischargeDestination.ReferenceValueCode = Encounter.DischargeDestinationCode

left join PAS.Specialty TreatmentFunction
on	TreatmentFunction.SpecialtyCode = Encounter.SpecialtyCode

left join PAS.Consultant
on	Consultant.ConsultantCode = Encounter.ConsultantCode

left join PAS.Specialty ConsultantSpecialty
on	ConsultantSpecialty.SpecialtyCode = Consultant.MainSpecialtyCode

left join PAS.ReferenceValue OverseasStatus
on OverseasStatus.ReferenceValueCode = Encounter.OverseasStatusFlag
	
left join PAS.ReferenceValue MaritalStatus
on	MaritalStatus.ReferenceValueCode = Encounter.MaritalStatusCode

left join PAS.ReferenceValue LegalStatusClassification
on	LegalStatusClassification.ReferenceValueCode = Encounter.LegalStatusClassificationOnAdmissionCode

left join PAS.RTTStatus RTTPeriodStatus
on	RTTPeriodStatus.RTTStatusCode = Encounter.RTTPeriodStatusCode

left join Organisation.dbo.Postcode Postcode
on	Postcode.Postcode =
		case
		when len(Encounter.Postcode) = 8 then Encounter.Postcode
		else left(Encounter.Postcode, 3) + ' ' + right(Encounter.Postcode, 4) 
		end

--UHSM do not have a neonatal system - these are picked  up from the national Badger system

--left join CDS.CriticalCarePeriod NeonatalCriticalCarePeriod1
--on	NeonatalCriticalCarePeriod1.SourceEncounterNo = Encounter.SourceUniqueID
--and	NeonatalCriticalCarePeriod1.SequenceNo = 1

--left join CDS.CriticalCarePeriod NeonatalCriticalCarePeriod2
--on	NeonatalCriticalCarePeriod2.SourceEncounterNo = Encounter.SourceUniqueID
--and	NeonatalCriticalCarePeriod2.SequenceNo = 2

--left join CDS.CriticalCarePeriod NeonatalCriticalCarePeriod3
--on	NeonatalCriticalCarePeriod3.SourceEncounterNo = Encounter.SourceUniqueID
--and	NeonatalCriticalCarePeriod3.SequenceNo = 3

--left join CDS.CriticalCarePeriod NeonatalCriticalCarePeriod4
--on	NeonatalCriticalCarePeriod4.SourceEncounterNo = Encounter.SourceUniqueID
--and	NeonatalCriticalCarePeriod4.SequenceNo = 4

--left join CDS.CriticalCarePeriod NeonatalCriticalCarePeriod5
--on	NeonatalCriticalCarePeriod5.SourceEncounterNo = Encounter.SourceUniqueID
--and	NeonatalCriticalCarePeriod5.SequenceNo = 5

--left join CDS.CriticalCarePeriod NeonatalCriticalCarePeriod6
--on	NeonatalCriticalCarePeriod6.SourceEncounterNo = Encounter.SourceUniqueID
--and	NeonatalCriticalCarePeriod6.SequenceNo = 6

--left join CDS.CriticalCarePeriod NeonatalCriticalCarePeriod7
--on	NeonatalCriticalCarePeriod7.SourceEncounterNo = Encounter.SourceUniqueID
--and	NeonatalCriticalCarePeriod7.SequenceNo = 7

--left join CDS.CriticalCarePeriod NeonatalCriticalCarePeriod8
--on	NeonatalCriticalCarePeriod8.SourceEncounterNo = Encounter.SourceUniqueID
--and	NeonatalCriticalCarePeriod8.SequenceNo = 8

--left join CDS.CriticalCarePeriod NeonatalCriticalCarePeriod9
--on	NeonatalCriticalCarePeriod9.SourceEncounterNo = Encounter.SourceUniqueID
--and	NeonatalCriticalCarePeriod9.SequenceNo = 9

--UHSM do not have a paediatric system - these come through the adult critical care records

--left join CDS.CriticalCarePeriod PaediatricCriticalCarePeriod1
--on	PaediatricCriticalCarePeriod1.SourceEncounterNo = Encounter.SourceUniqueID
--and	PaediatricCriticalCarePeriod1.SequenceNo = 1

--left join CDS.CriticalCarePeriod PaediatricCriticalCarePeriod2
--on	PaediatricCriticalCarePeriod2.SourceEncounterNo = Encounter.SourceUniqueID
--and	PaediatricCriticalCarePeriod2.SequenceNo = 2

--left join CDS.CriticalCarePeriod PaediatricCriticalCarePeriod3
--on	PaediatricCriticalCarePeriod3.SourceEncounterNo = Encounter.SourceUniqueID
--and	PaediatricCriticalCarePeriod3.SequenceNo = 3

--left join CDS.CriticalCarePeriod PaediatricCriticalCarePeriod4
--on	PaediatricCriticalCarePeriod4.SourceEncounterNo = Encounter.SourceUniqueID
--and	PaediatricCriticalCarePeriod4.SequenceNo = 4

--left join CDS.CriticalCarePeriod PaediatricCriticalCarePeriod5
--on	PaediatricCriticalCarePeriod5.SourceEncounterNo = Encounter.SourceUniqueID
--and	PaediatricCriticalCarePeriod5.SequenceNo = 5

--left join CDS.CriticalCarePeriod PaediatricCriticalCarePeriod6
--on	PaediatricCriticalCarePeriod6.SourceEncounterNo = Encounter.SourceUniqueID
--and	PaediatricCriticalCarePeriod6.SequenceNo = 6

--left join CDS.CriticalCarePeriod PaediatricCriticalCarePeriod7
--on	PaediatricCriticalCarePeriod7.SourceEncounterNo = Encounter.SourceUniqueID
--and	PaediatricCriticalCarePeriod7.SequenceNo = 7

--left join CDS.CriticalCarePeriod PaediatricCriticalCarePeriod8
--on	PaediatricCriticalCarePeriod8.SourceEncounterNo = Encounter.SourceUniqueID
--and	PaediatricCriticalCarePeriod8.SequenceNo = 8

--left join CDS.CriticalCarePeriod PaediatricCriticalCarePeriod9
--on	PaediatricCriticalCarePeriod9.SourceEncounterNo = Encounter.SourceUniqueID
--and	PaediatricCriticalCarePeriod9.SequenceNo = 9

left join CDS.AdultCriticalCarePeriod AdultCriticalCarePeriod1
on	AdultCriticalCarePeriod1.SourceEncounterNo = Encounter.SourceUniqueID
and	AdultCriticalCarePeriod1.SequenceNo = 1

left join CDS.AdultCriticalCarePeriod AdultCriticalCarePeriod2
on	AdultCriticalCarePeriod2.SourceEncounterNo = Encounter.SourceUniqueID
and	AdultCriticalCarePeriod2.SequenceNo = 2

left join CDS.AdultCriticalCarePeriod AdultCriticalCarePeriod3
on	AdultCriticalCarePeriod3.SourceEncounterNo = Encounter.SourceUniqueID
and	AdultCriticalCarePeriod3.SequenceNo = 3

left join CDS.AdultCriticalCarePeriod AdultCriticalCarePeriod4
on	AdultCriticalCarePeriod4.SourceEncounterNo = Encounter.SourceUniqueID
and	AdultCriticalCarePeriod4.SequenceNo = 4

left join CDS.AdultCriticalCarePeriod AdultCriticalCarePeriod5
on	AdultCriticalCarePeriod5.SourceEncounterNo = Encounter.SourceUniqueID
and	AdultCriticalCarePeriod5.SequenceNo = 5

left join CDS.AdultCriticalCarePeriod AdultCriticalCarePeriod6
on	AdultCriticalCarePeriod6.SourceEncounterNo = Encounter.SourceUniqueID
and	AdultCriticalCarePeriod6.SequenceNo = 6

left join CDS.AdultCriticalCarePeriod AdultCriticalCarePeriod7
on	AdultCriticalCarePeriod7.SourceEncounterNo = Encounter.SourceUniqueID
and	AdultCriticalCarePeriod7.SequenceNo = 7

left join CDS.AdultCriticalCarePeriod AdultCriticalCarePeriod8
on	AdultCriticalCarePeriod8.SourceEncounterNo = Encounter.SourceUniqueID
and	AdultCriticalCarePeriod8.SequenceNo = 8

left join CDS.AdultCriticalCarePeriod AdultCriticalCarePeriod9
on	AdultCriticalCarePeriod9.SourceEncounterNo = Encounter.SourceUniqueID
and	AdultCriticalCarePeriod9.SequenceNo = 9

left join CDS.Delivery
on	Delivery.EpisodeSourceUniqueID = Encounter.SourceUniqueID

left join CDS.Birth
on	Birth.EpisodeSourceUniqueID = Encounter.SourceUniqueID

left join CDS.Delivery Mother
on	Mother.EpisodeSourceUniqueID = Birth.MotherEpisodeSourceUniqueID

left join dbo.EntityXref ConsSpecMapping
--on	TreatmentFunctionSpecialtyMap.EntityTypeCode = 'TREATMENTFUNCTIONCODE'
on	ConsSpecMapping.XrefEntityTypeCode = 'SPECIALTYCODE'
and	ConsSpecMapping.EntityCode =
		coalesce(
			left(ConsultantSpecialty.NationalSpecialtyCode,3)
			,left(TreatmentFunction.NationalSpecialtyCode, 3)
		)

left join dbo.EntityXref TxFuncMapping
--on	SpecialtyTreatmentFunctionMap.EntityTypeCode = 'SPECIALTYCODE'
on	TxFuncMapping.XrefEntityTypeCode = 'TREATMENTFUNCTIONCODE'
and	TxFuncMapping.EntityCode =
		coalesce(
			 left(TreatmentFunction.NationalSpecialtyCode, 3)
			,left(ConsultantSpecialty.NationalSpecialtyCode,3)
		)

left join
	(
	select distinct
		Diagnosis.APCSourceUniqueID
	from
		APC.Diagnosis
	where
		exists
		(
		select
			1
		from
			dbo.EntityLookup SensitiveDiagnosis
		where
			Diagnosis.DiagnosisCode = SensitiveDiagnosis.EntityCode
		and	SensitiveDiagnosis.EntityTypeCode = 'SENSITIVEDIAGNOSIS'
		)
	) SensitiveDiagnosis
on	SensitiveDiagnosis.APCSourceUniqueID = Encounter.SourceUniqueID

left join
	(
	select distinct
		Operation.APCSourceUniqueID
	from
		APC.Operation
	where
		exists
		(
		select
			1
		from
			dbo.EntityLookup SensitiveOperation
		where
			Operation.OperationCode = SensitiveOperation.EntityCode
		and	SensitiveOperation.EntityTypeCode = 'SENSITIVEOPERATION'
		)
	) SensitiveOperation
on	SensitiveOperation.APCSourceUniqueID = Encounter.SourceUniqueID

--KO changed 14/11/2012
--left join dbo.EntityXref PCTMap
--on	PCTMap.EntityTypeCode = 'OLDPCT'
--and	PCTMap.XrefEntityTypeCode = 'NEWPCT'
----and	PCTMap.EntityCode = left(Encounter.PurchaserCode, 3)
--and	PCTMap.EntityCode = left(Purchaser.PurchaserMainCode, 3)


left join dbo.EntityXref PCTMapPurchaser
	on	PCTMapPurchaser.EntityTypeCode = 'OLDPCT'
	and	PCTMapPurchaser.XrefEntityTypeCode = 'NEWPCT'
	and	PCTMapPurchaser.EntityCode = left(Purchaser.PurchaserMainCode, 3)
	
left join dbo.EntityXref PCTMapPractice
	on	PCTMapPurchaser.EntityTypeCode = 'OLDPCT'
	and	PCTMapPurchaser.XrefEntityTypeCode = 'NEWPCT'
	and	PCTMapPurchaser.EntityCode = left(PCT.OrganisationLocalCode, 3)

select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'CDS - WH BuildAPCBaseArdentiaPCT', @Stats, @StartTime

--print @Stats