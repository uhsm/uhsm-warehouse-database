﻿
CREATE procedure [CDS].[BuildCHKSAPC]
as

/*
--Author: K Oakden
--Date created: 14/05/2013
--[BuildCHKSAPC] runs either CDS.BuildCHKSAPCCCG or CDS.BuildCHKSAPCPCT

*/

declare @CommissionerType varchar(5)

select @CommissionerType = TextValue from dbo.Parameter where Parameter = 'CDSCOMMISSIONERTYPE'

--select @CommissionerType
--fill base here
if(@CommissionerType = 'CCG')
	begin
		exec CDS.BuildCHKSAPCCCG
	end
else
	begin
		exec CDS.BuildCHKSAPCPCT
	end

--declare @StartTime datetime
--declare @Elapsed int
--declare @RowsInserted Int
--declare @Stats varchar(255)
--declare @Created datetime

--select @StartTime = getdate()

--TRUNCATE TABLE CDS.CHKSAPC

--INSERT INTO CDS.CHKSAPC

--SELECT
--A.LOCPATID,
--A.NHS_NO,
--REPLACE(A.DOB, '-',''),
--A.ETHNICOR,
--B.MaritalStatusCode,
--A.SEX,
--A.POSTCODE,
--A.HA,
--A.PROV_SPELL,
--A.ADMIN_CATEGORY,
--A.CLASSPAT,
--A.ADMIMETH,
--A.ADMISORC,
--A.ELECDUR,
--A.MAN_INTENT,
--REPLACE(A.ELECDATE,'-',''),
--A.GPREG,
--A.PRACREG,
--A.REFERRER,
--A.REF_ORG,
--A.DISDEST,
--A.DISMETH,
--AdmissionDate = REPLACE(A.ADMIDATE,'-',''),
--DischargeDate = REPLACE(A.DISDATE,'-',''),
--A.EPIORDER,
--A.FIRST_REG_ADM,
--A.LAST_IN_SPELL,
--A.NEONATAL_LEVEL,
--A.CARER_SUPPORT_IND,
--NULL AS LegalStatus, --LEGAL STATUS
--A.OPER_STATUS,
--EpisodeStartDate = REPLACE(A.EPISTART,'-',''),
--EpisodeEndDate = REPLACE(A.EPIEND,'-',''),
--A.SERIAL_NO,
--A.CONTRACT_LINE_NO,
--A.[Provider Reference Number],
--A.PURCH_REF,
--A.PROVIDER,
--A.PURCHASER,
--A.CONS_CODE,
--CASE WHEN A.CONS_MAINSPEF = '501' THEN A.CONS_MAINSPEF ELSE A.MAINSPEF END,
--A.CONS_MAINSPEF,
--SubSpecialty = A.CONS_MAINSPEF,
--A.PRIMARY_DIAG,
--A.SECONDARY_DIAG1,
--A.SECONDARY_DIAG2,
--A.SECONDARY_DIAG3,
--A.SECONDARY_DIAG4,
--A.SECONDARY_DIAG5,
--A.SECONDARY_DIAG6,
--A.SECONDARY_DIAG7,
--A.SECONDARY_DIAG8,
--A.SECONDARY_DIAG9,
--A.SECONDARY_DIAG10,
--A.SECONDARY_DIAG11,
--A.SECONDARY_DIAG12,
--NULL, --SECONDARY_DIAG13
--NULL, --SECONDARY_DIAG14
--A.PRIMARY_PROC,
--PrimaryProcedureDate = REPLACE(A.PRIMARY_PROC_DATE,'-',''),
--A.PROC2,
--ProcedureDate2 = REPLACE(A.PROC2_DATE,'-',''),
--A.PROC3,
--ProcedureDate3 = REPLACE(A.PROC3_DATE,'-',''),
--A.PROC4,
--ProcedureDate4 = REPLACE(A.PROC4_DATE,'-',''),
--A.PROC5,
--ProcedureDate5 = REPLACE(A.PROC5_DATE,'-',''),
--A.PROC6,
--ProcedureDate6 = REPLACE(A.PROC6_DATE,'-',''),
--A.PROC7,
--ProcedureDate7 = REPLACE(A.PROC7_DATE,'-',''),
--A.PROC8,
--ProcedureDate8 = REPLACE(A.PROC8_DATE,'-',''),
--A.PROC9,
--ProcedureDate9 = REPLACE(A.PROC9_DATE,'-',''),
--A.PROC10,
--ProcedureDate10 = REPLACE(A.PROC10_DATE,'-',''),
--A.PROC11,
--ProcedureDate11 = REPLACE(A.PROC11_DATE,'-',''),
--A.PROC12,
--ProcedureDate12 = REPLACE(A.PROC12_DATE,'-',''),
--NULL, -- PROC13
--NULL, -- PROC13 DATE
--NULL, -- PROC14
--NULL, -- PROC14 DATE
--NULL, -- PROC15
--NULL, -- PROC13 DATE
--A.HRG,
--A.[HRG Version]
--FROM CDS.APCArdentiaExport A
--INNER JOIN CDS.APCBaseArdentia B on
--A.CDS_ID = B.UniqueEpisodeSerialNo
--WHERE 
--A.[CDS TYPE] in ('120','130','140')
--AND (NOT A.POSTCODE IS NULL AND NOT A.DOB IS NULL)

--select @RowsInserted = @@rowcount


--select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

--select @Stats = 
--	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
--	CONVERT(varchar(6), @Elapsed) + ' Mins'

--exec WriteAuditLogEvent 'CDS - WH BuildCHKSAPC', @Stats, @StartTime

--print @Stats

