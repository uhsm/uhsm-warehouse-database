﻿

CREATE procedure [CDS].[BuildCHKSOPCCG]
	--@SubmissionDateStr varchar(10) = null
as
/*
--Author: K Oakden
--Date created: 27/07/2012
--[BuildCHKSOP] generates the OP CHKS dataset ready for submission
--Copied and updated from CDS.dbo.uspCHKSOutpatients on InfoSQL
*/
declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @Created datetime

select @StartTime = getdate()

TRUNCATE TABLE CDS.CHKSOP

INSERT INTO CDS.CHKSOP

SELECT
--@SubmissionDateStr,
AttendanceDate = REPLACE(op.ATTDATE,'-',''),
op.ATTENDED,
op.ATT_IDENT,
DOB = replace(op.DOB,'-',''),
CASE WHEN op.DOB IS NULL THEN
	9
ELSE
	1
END AS BIRTHDATESTATUS,
CarerSupport = isnull(op.CAR_SUP_IND, ''),
op.CATEGORY,
ClinicPurpose = isnull(clpurp.MainCode, 'NSP'),
op.REGPRACT,
op.CCODE,
op.CON_SPEC,
op.SERIAL_NO,
op.FIRSTATT,
op.GPREG,
ContractLineNo = isnull(op.CONT_LINE_NO,''),
LastDNADate = isnull(replace(op.LASTDNADATE,'-',''),''),
LASTDNADATESTATUS = '',
op.LOCPATID,
LocalSubSpecialty = isnull(op.LOC_SPEC,''),
op.LOCATION_CLASS,
op.MED_STAFF,
op.NAME_FORMAT,
op.NHS_NO,
op.OPSTATUS,
op.PROVIDER,
op.PURCHASER,
op.HA,
op.OUTCOME,
Diagnosis1 = isnull(opbase.DiagnosisCode1, ''),
Diagnosis2 = isnull(opbase.DiagnosisCode2, ''),
Diagnosis3 = isnull(opbase.DiagnosisCode3, ''),
Diagnosis4 = '',
Procedure1 = isnull(op.PRIOPCS, ''),
Procedure2 = isnull(op.OPCS2, ''),
Procedure3 = isnull(op.OPCS3, ''),
Procedure4 = isnull(op.OPCS4, ''),
Procedure5 = isnull(op.OPCS5, ''),
Procedure6 = isnull(op.OPCS6, ''),
Procedure7 = isnull(op.OPCS7, ''),
Procedure8 = isnull(op.OPCS8, ''),
Procedure9 = isnull(op.OPCS9, ''),
Procedure10 = isnull(op.OPCS10, ''),
Procedure11 = isnull(op.OPCS11, ''),
Procedure12 = isnull(op.OPCS12, ''),
op.POSTCODE,
op.PRIORITY,
op.SERIAL_NO,
RequestDate = REPLACE(op.REQDATE,'-',''),
REFDATESTATUS = '',
op.REFERRER,
ReferringOrganisation = case when len(op.REF_ORG) = 3
						then op.REF_ORG + '00'
						else op.REF_ORG
						end,
op.SERV_TYPE,
op.SEX,
op.SITECODE,
op.REFSRCE,
op.MAINSPEF

FROM CDS.OPArdentiaExportCCG op
inner join CDS.OPBaseArdentia opbase
on op.CDS_ID = opbase.UniqueEpisodeSerialNo
left join 
(select ClinicCode, max(ClinicPurposeCode) as MaxClinicPurposeCode from OP.Clinic group by ClinicCode) cl
on opbase.ClinicCode = cl.ClinicCode
left join PAS.ReferenceValue clpurp
on    clpurp.ReferenceValueCode = cl.MaxClinicPurposeCode
WHERE (NOT op.POSTCODE IS NULL AND NOT op.DOB IS NULL and not op.Postcode like 'ZZ99%')
and opbase.DNACode <> ''

select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'CDS - WH BuildCHKSOPCCG', @Stats, @StartTime

print @Stats


