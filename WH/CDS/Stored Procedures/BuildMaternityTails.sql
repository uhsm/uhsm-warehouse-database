﻿CREATE procedure [CDS].[BuildMaternityTails] as

--ensure the Evolution data is current for CDS generation
exec MaternityReporting.dbo.uspCreateMaternityTailsForExport

--KO 12/10/2012 Delete archive data from Births table - it should not be there
delete from APC.Birth where SourceUniqueID in (
select RGBIR_REFNO from Lorenzo.dbo.Birth where ARCHV_FLAG = 'Y'
)

--populate CDS.Delivery
truncate table CDS.Delivery

insert
into
	CDS.Delivery
(
	 EpisodeSourceUniqueID
	,PreviousPregnancies
	,NumberOfBabies
	,FirstAnteNatalAssessmentDate
	,AnteNatalGpCode
	,AnteNatalGpPracticeCode
	,DeliveryLocationClassCode
	,DeliveryPlaceChangeReasonCode
	,IntendedDeliveryPlaceTypeCode
	,AnaestheticGivenDuringCode
	,AnaestheticGivenPostCode
	,GestationLength
	,LabourOnsetMethodCode
	,DeliveryDate
	,DeliveryMethodCode
	,ActualDeliveryPlaceTypeCode
	,StatusOfPersonConductingDeliveryCode
	,SourceSpellNo
)
--this relies on an up-to-date MaternityReporting.dbo.MaternityTailsExport table

select
	 MaternitySpell.EpisodeSourceUniqueID

	,PreviousPregnancies =
		case
		when
			coalesce(
				 MaternitySpell.PreviousPregnancies
				,Delivery.[Number of Past Pregnancies]
			) > 19
		then 99
		else
			coalesce(
				 MaternitySpell.PreviousPregnancies
				,Delivery.[Number of Past Pregnancies]
			)
		end

	,NumberOfBabies =
		coalesce(
			 MaternitySpell.NumberOfBabies
			,(
			select
				MAX(Birth.BabyNumber)
			from
				MaternityReporting.dbo.MaternityTailsExport Birth
			where
				Birth.Spell_refno = Delivery.Spell_refno

			--this can contain incorrect mapping of SourceSpellNo to more than 1 delivery so take the latest
			and	not exists
				(
				select
					1
				from
					MaternityReporting.dbo.MaternityTailsExport Previous
				where
					Previous.Spell_refno = MaternitySpell.SourceSpellNo
				and	Previous.[Delivery Date] > Birth.[Delivery Date]
				)
			)
		)

	,FirstAnteNatalAssessmentDate =
		left(
			CONVERT(
				 varchar
				,coalesce(
					 MaternitySpell.FirstAnteNatalAssessmentDate
					,Delivery.[First Antenatal Assessment Date]
				)
				,120
			)
			,10
		)

	,AnteNatalGpCode = Delivery.[Code of GMP Responsible for Antenatal Care]
	,AnteNatalGpPracticeCode = Delivery.[Registered GMP - Antenatal Care]

	,DeliveryLocationClassCode = null

	,DeliveryPlaceChangeReasonCode =
		coalesce(
			 DeliveryPlaceChangeReason.MappedCode
			,Delivery.[Delivery Place Change Reason]
		)

	,IntendedDeliveryPlaceTypeCode =
		coalesce(
			 IntendedDeliveryPlaceType.MappedCode
			,Delivery.[Delivery Place Type (Intended)]
		)

	,AnaestheticGivenDuringCode = 
		coalesce(
			 AnaestheticGivenDuring.MappedCode
			,Delivery.[Anaesthetic Given During Labour or Delivery]
		)

	,AnaestheticGivenPostCode =
		coalesce(
			 AnaestheticGivenPost.MappedCode
			,Delivery.[Anaesthetic Given Post Labour or Delivery]
		)

	,GestationLength =
		coalesce(
			case
			when isnumeric(MaternitySpell.GestationLength) = 1
			then
				case
				when MaternitySpell.GestationLength = '0'
				then 99
				else MaternitySpell.GestationLength / 7
				end
			
			when isnumeric(Delivery.[Gestation Length (Assessment)]) = 1
			then
				case
				when Delivery.[Gestation Length (Assessment)] = '0'
				then 99
				else Delivery.[Gestation Length (Assessment)] / 7
				end
			end

			,99
		)

	,LabourOnsetMethodCode =
		case
		when coalesce(LabourOnsetMethod.MappedCode, '9') = '9'
		then Delivery.[Labour or Delivery Onset Method]
		else LabourOnsetMethod.MappedCode
		end

	,DeliveryDate =
		coalesce(
			 Delivery.[Delivery Date]
			,cast(MaternitySpell.DeliveryTime as date)
			,Delivery.[Birth Date (Baby)]
			)

	,DeliveryMethodCode =
		case
		when coalesce(DeliveryMethod.MappedCode, '9') = '9'
		then Delivery.[Delivery Method]
		else DeliveryMethod.MappedCode
		end

	,ActualDeliveryPlaceTypeCode =
		coalesce(
			 ActualDeliveryPlace.MappedCode
			,Delivery.[Delivery Place Type (Actual)]
		)

	,StatusOfPersonConductingDeliveryCode =
		coalesce(
			 StatusOfPersonConductingDelivery.MappedCode
			,Delivery.[Status of Person Conducting Delivery] collate database_default
		)

	,SourceSpellNo = Delivery.Spell_refno

from
	APC.MaternitySpell

left join MaternityReporting.dbo.MaternityTailsExport Delivery
on	Delivery.Spell_refno = MaternitySpell.SourceSpellNo
and	Delivery.BabyNumber = 1

--this can contain incorrect mapping of SourceSpellNo to more than 1 delivery so take the latest
and	not exists
	(
	select
		1
	from
		MaternityReporting.dbo.MaternityTailsExport Previous
	where
		Previous.Spell_refno = MaternitySpell.SourceSpellNo
	and	Previous.BabyNumber = 1
	and	(
			Previous.[Delivery Date] > Delivery.[Delivery Date]
		or	(
				Previous.[Delivery Date] = Delivery.[Delivery Date]
			and	Previous.EncounterRecno > Delivery.EncounterRecno
			)
		)
	)

left join APC.Birth
on	Birth.MaternitySpellSourceUniqueID = MaternitySpell.SourceUniqueID
and	Birth.BirthOrder = 1

--PDO 20 June 2012
--this fixes the issue where a delivery does not have a linkable birth
left join APC.Encounter BirthEncounter
on	Birth.SourcePatientNo = BirthEncounter.SourcePatientNo
and	not exists
	(
	select
		1
	from
		APC.Encounter Previous
	where
		Previous.SourcePatientNo = BirthEncounter.SourcePatientNo
	and	Previous.EpisodeStartTime < BirthEncounter.EpisodeStartTime
	)

left join PAS.ReferenceValue DeliveryMethod
on	DeliveryMethod.ReferenceValueCode = Birth.DeliveryMethodCode

left join PAS.ReferenceValue LabourOnsetMethod
on	LabourOnsetMethod.ReferenceValueCode = MaternitySpell.LabourOnsetMethodCode

left join PAS.ReferenceValue DeliveryPlaceChangeReason
on	DeliveryPlaceChangeReason.ReferenceValueCode = MaternitySpell.DeliveryPlaceChangeReasonCode

left join PAS.ReferenceValue IntendedDeliveryPlaceType
on	IntendedDeliveryPlaceType.ReferenceValueCode = MaternitySpell.IntendedDeliveryPlaceTypeCode

left join PAS.ReferenceValue AnaestheticGivenDuring
on	AnaestheticGivenDuring.ReferenceValueCode = MaternitySpell.AnaestheticGivenDuringCode

left join PAS.ReferenceValue AnaestheticGivenPost
on	AnaestheticGivenPost.ReferenceValueCode = MaternitySpell.AnaestheticGivenPostCode

left join PAS.ReferenceValue ActualDeliveryPlace
on	ActualDeliveryPlace.ReferenceValueCode = MaternitySpell.DeliveryPlaceCode

left join PAS.ReferenceValue StatusOfPersonConductingDelivery
on	StatusOfPersonConductingDelivery.ReferenceValueCode = MaternitySpell.StatusOfPersonConductingDeliveryCode

inner join APC.Encounter
on	Encounter.SourceUniqueID = MaternitySpell.EpisodeSourceUniqueID

--inner join CDS.wrk
--on	wrk.EncounterRecno = Encounter.EncounterRecno

where
	not exists
	(
	select
		1
	from
		APC.MaternitySpell Previous
	where
		Previous.EpisodeSourceUniqueID = MaternitySpell.EpisodeSourceUniqueID
	and	(
			Previous.PASUpdated > MaternitySpell.PASUpdated
		or	(
				Previous.PASUpdated = MaternitySpell.PASUpdated
			and	Previous.SourceUniqueID > MaternitySpell.SourceUniqueID
			)
		)
	)

and	(
		BirthEncounter.EncounterRecno is not null
	or	(
			BirthEncounter.EncounterRecno is null
		and	(
				coalesce
				(
					(
					select
						NumericValue
					from
						dbo.Parameter
					where
						Parameter.Parameter = 'CDSVALIDATIONLEVELAPC'
					)
					,99
				) > 1
			)
		)
	)

--populate CDS.Birth
truncate table CDS.Birth

insert
into
	CDS.Birth
(
	 EpisodeSourceUniqueID
	,MotherEpisodeSourceUniqueID
	,BirthOrder
	,DeliveryMethodCode
	,AssessmentGestationLength
	,ResuscitationMethodCode
	,StatusOfPersonConductingDeliveryCode
	,NHSNumberStatusIndicator
	,DateOfBirth
	,LocalPatientIdentifier
	,ProviderCode
	,NHSNumber
	,SexCode
	,LiveOrStillBirth
	,BirthWeight
	,DeliveryPlaceTypeActualCode
	,DeliveryLocationClassCode
	,MotherSourceSpellNo
	,MotherLocalPatientID
	,MotherLocalPatientIDOrganisationCode
	,MotherNHSNumber
	,MotherNNNStatusIndicator
	,MotherPostcode
	,MotherPCTOfResidenceCode
	,MotherDateOfBirth
	,MotherPatient1Address
	,MotherPatient2Address
	,MotherPatient3Address
	,MotherPatient4Address
	,MotherPatient5Address
	,EvolutionPatientNumber
	,PreviousPregnancies
	,NumberOfBabies
	,FirstAnteNatalAssessmentDate
	,AnteNatalGpCode
	,AnteNatalGpPracticeCode
	,DeliveryPlaceChangeReasonCode
	,IntendedDeliveryPlaceTypeCode
	,AnaestheticGivenDuringCode
	,AnaestheticGivenPostCode
	,GestationLength
	,LabourOnsetMethodCode
	,DeliveryDate
)
select
	 EpisodeSourceUniqueID = BirthEncounter.SourceUniqueID
	,MotherEpisodeSourceUniqueID = MaternitySpell.EpisodeSourceUniqueID

	,BirthOrder =
		coalesce(
			 EvolutionBirth.BabyNumber
			,Birth.BirthOrder
			,1
		)

	,DeliveryMethodCode =
		case
		when coalesce(DeliveryMethod.MappedCode, '9') = '9'
		then EvolutionBirth.[Delivery Method]
		else DeliveryMethod.MappedCode
		end

	,AssessmentGestationLength =
		case
		when
			ISNUMERIC(
				coalesce(
					 Birth.GestationLength
					,EvolutionBirth.[Gestation Length (Assessment)]
				)
			)
			= 1
		and	
			coalesce(
				 Birth.GestationLength
				,EvolutionBirth.[Gestation Length (Assessment)]
			) <> '0'
		then
			coalesce(
				 Birth.GestationLength
				,EvolutionBirth.[Gestation Length (Assessment)]
			) /7
		else 99
		end

	,ResuscitationMethodCode =
		case
		when coalesce(ResuscitationMethod.MappedCode, '9') = '9'
		then
			coalesce(
				case
					when EvolutionBirth.[Resuscitation Method] = '8'
					then
						case
						when EvolutionBirth.[Live or Still Birth] = '1'
						then '1'
						else EvolutionBirth.[Resuscitation Method]
						end
					else EvolutionBirth.[Resuscitation Method]
				end
				,case
				when 
					coalesce(
						 LiveOrStillBirth.MappedCode
						,EvolutionBirth.[Live or Still Birth] collate database_default
					) = '1'
				then '1'
				else ResuscitationMethod.MappedCode
				end
			)
		else ResuscitationMethod.MappedCode
		end

	,StatusOfPersonConductingDeliveryCode =
		coalesce(
			 StatusOfPersonConductingDelivery.MappedCode
			,EvolutionBirth.[Status of Person Conducting Delivery] collate database_default
		)

	,NHSNumberStatusIndicator = '8'

	,DateOfBirth =
		coalesce(
			 Birth.DateOfBirth
			,EvolutionBirth.[Birth Date (Baby)]
		)

	,LocalPatientIdentifier =
		coalesce(
			 BirthEncounter.DistrictNo
			,EvolutionBirth.[Local Patient Identifier (Baby)] collate database_default
		)

	,ProviderCode = 'RM200'

	,NHSNumber =
		coalesce(
			 BirthEncounter.NHSNumber
			,EvolutionBirth.[NHS Number (Baby)] collate database_default
		)

	,SexCode =
		coalesce(
			 Sex.MappedCode
			,EvolutionBirth.[Gender (Baby)] collate database_default
		)


	,LiveOrStillBirth =
		coalesce(
			 LiveOrStillBirth.MappedCode
			,EvolutionBirth.[Live or Still Birth] collate database_default
		)

	,BirthWeight =
		case
		when
			Birth.BirthWeight is null
		or	Birth.BirthWeight = '0'
		or	Birth.BirthWeight = '0000'
		then 
			case
			when
				EvolutionBirth.[Birth Weight] is null
			or	EvolutionBirth.[Birth Weight] = '0'
			or	EvolutionBirth.[Birth Weight] = '0000'
			then 9999
			else EvolutionBirth.[Birth Weight]
			end
		else Birth.BirthWeight
		end

	,DeliveryPlaceTypeActualCode =
		coalesce(
			 ActualDeliveryPlace.MappedCode
			,EvolutionBirth.[Delivery Place Type (Actual)]
		)

	,DeliveryLocationClassCode = null --??

	,MotherSourceSpellNo = MaternitySpell.SourceSpellNo

	,MotherLocalPatientID =
		coalesce(
			 MaternityEncounter.DistrictNo
			,EvolutionBirth.[Local Patient Identifier (Mother)] collate database_default
		)

	,MotherLocalPatientIDOrganisationCode = 'RM200'

	,MotherNHSNumber =
		coalesce(
			 MaternityEncounter.NHSNumber
			,EvolutionBirth.[NHS Number (Mother)] collate database_default
		)

	,MotherNNNStatusIndicator = '01'

	,MotherPostcode =
		coalesce(
			 MaternityEncounter.Postcode
			,[Postcode (Mother)] collate database_default
		)

	,MotherPCTOfResidenceCode = null

	,MotherDateOfBirth =
		coalesce(
			 MaternityEncounter.DateOfBirth
			,EvolutionBirth.[Birth Date Mother)]
		)

	,MotherPatient1Address = null
	,MotherPatient2Address = null
	,MotherPatient3Address = null
	,MotherPatient4Address = null
	,MotherPatient5Address = null

	,EvolutionPatientNumber = EvolutionBirth.BabyPatientPointer

	,PreviousPregnancies =
		case
		when Delivery.PreviousPregnancies > 19
		then 99
		else Delivery.PreviousPregnancies
		end

	,Delivery.NumberOfBabies
	,Delivery.FirstAnteNatalAssessmentDate
	,Delivery.AnteNatalGpCode
	,Delivery.AnteNatalGpPracticeCode
	,Delivery.DeliveryPlaceChangeReasonCode
	,Delivery.IntendedDeliveryPlaceTypeCode
	,Delivery.AnaestheticGivenDuringCode
	,Delivery.AnaestheticGivenPostCode
	,Delivery.GestationLength
	,Delivery.LabourOnsetMethodCode
	,Delivery.DeliveryDate

from
	APC.Birth

inner join APC.MaternitySpell
on	MaternitySpell.SourceUniqueID = Birth.MaternitySpellSourceUniqueID
and	MaternitySpell.SourcePatientNo = Birth.MotherSourcePatientNo

left join APC.Encounter MaternityEncounter
on	MaternityEncounter.SourceUniqueID = MaternitySpell.EpisodeSourceUniqueID

left join MaternityReporting.dbo.MaternityTailsExport EvolutionBirth
on	EvolutionBirth.Spell_refno = MaternitySpell.SourceSpellNo
and	EvolutionBirth.BabyNumber = Birth.BirthOrder

--this can contain incorrect mapping of SourceSpellNo to more than 1 delivery so take the latest
and	not exists
	(
	select
		1
	from
		MaternityReporting.dbo.MaternityTailsExport Previous
	where
		Previous.Spell_refno = MaternitySpell.SourceSpellNo
	and	Previous.BabyNumber = Birth.BirthOrder
	and	(
			Previous.[Delivery Date] > EvolutionBirth.[Delivery Date]
		or	(
				Previous.[Delivery Date] = EvolutionBirth.[Delivery Date]
			and	Previous.EncounterRecno > EvolutionBirth.EncounterRecno
			)
		)
	)


inner join APC.Encounter BirthEncounter
on	Birth.SourcePatientNo = BirthEncounter.SourcePatientNo
and	not exists
	(
	select
		1
	from
		APC.Encounter Previous
	where
		Previous.SourcePatientNo = BirthEncounter.SourcePatientNo
	and	Previous.EpisodeStartTime < BirthEncounter.EpisodeStartTime
	)


left join PAS.ReferenceValue DeliveryMethod
on	DeliveryMethod.ReferenceValueCode = Birth.DeliveryMethodCode

left join PAS.ReferenceValue ResuscitationMethod
on	ResuscitationMethod.ReferenceValueCode = Birth.ResuscitationMethodByPressureCode

left join PAS.ReferenceValue LiveOrStillBirth
on	LiveOrStillBirth.ReferenceValueCode = Birth.LiveOrStillBirthCode

left join PAS.ReferenceValue StatusOfPersonConductingDelivery
on	StatusOfPersonConductingDelivery.ReferenceValueCode = MaternitySpell.StatusOfPersonConductingDeliveryCode

left join PAS.ReferenceValue Sex
on	Sex.ReferenceValueCode = Birth.SexCode

left join PAS.ReferenceValue ActualDeliveryPlace
on	ActualDeliveryPlace.ReferenceValueCode = Birth.DeliveryPlaceCode

inner join CDS.Delivery
on	Delivery.EpisodeSourceUniqueID = MaternitySpell.EpisodeSourceUniqueID

--inner join CDS.wrk
--on	wrk.EncounterRecno = BirthEncounter.EncounterRecno
--and	wrk.CDSTypeCode = '120'
