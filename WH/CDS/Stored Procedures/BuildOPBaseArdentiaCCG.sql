﻿

CREATE procedure [CDS].[BuildOPBaseArdentiaCCG] 
As
/*
--Author: P Orrell / K Oakden
--UPdated from [BuildOPBaseArdentia] to change PCT data to CCG data
*/

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @Created datetime
declare @Bulkstart varchar(10)
declare @Bulkend varchar(10)

select @StartTime = getdate()

select @Bulkstart = 
	left(CONVERT(varchar, DateValue, 120),10)
	from dbo.Parameter
	where Parameter = 'CDSREPORTPERIODSTARTDATE'

select @Bulkend = 
	left(CONVERT(varchar, DateValue, 120),10)
	from dbo.Parameter
	where Parameter = 'CDSREPORTPERIODENDDATE'


truncate table CDS.OPBaseArdentia

insert
into
	CDS.OPBaseArdentia
(
	 EncounterRecno
	,SourceUniqueID
	,CDSGroup
	,CDSType
	,UniqueEpisodeSerialNo
	,UpdateType
	,CDSUpdateDate
	,CDSUpdateTime
	,CDSActivityDate
	,RTTPathwayID
	,RTTCurrentProviderCode
	,RTTPeriodStatusCode
	,RTTStartDate
	,RTTEndDate
	,NNNStatusIndicator
	,OverseasStatus
	,CCGofResidenceCode
	,PracticeCCG
	,LocalPatientID
	,LocalPatientIDOrganisationCode
	,NHSNumber
	,Postcode
	,PostcodeAmended
	,PatientRefNo
	,PatientName
	,PatientAddress
	,DateOfBirth
	,SexCode
	,CarerSupportIndicator
	,EthnicGroupCode
	,MaritalStatusCode
	,ConsultantCode
	,MainSpecialtyCode
	,TreatmentFunctionCode
	,DiagnosisSchemeInUse
	,AttendanceIdentifier
	,AdministrationCategoryCode
	,DNACode
	,FirstAttendanceFlag
	,MedicalStaffTypeSeeingPatientCode
	,OperationStatus
	,OutcomeOfAttendanceCode
	,AppointmentDate
	,AgeAtCDSActivityDate
	,EarliestReasonableOfferDate
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,PbRSourceDataset
	,PurchaserReferenceNo
	,CommissionerReferenceNo
	,ProviderCode
	,PurchaserCodeCCG
	,ProcedureSchemeInUse
	,LocationClassCode
	,SiteCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,PriorityTypeCode
	,ServiceTypeRequestedCode
	,SourceOfReferralCode
	,ReferralDate
	,ReferrerCode
	,ReferringOrganisationCode
	,LastDNAOrPatientCancelledDate
	,HRGCode
	,HRGVersionNumber
	,DGVPCode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientAddress5
	,HACode
	,PatientForename
	,PatientSurname
	,ClinicCode
)
select
	 Encounter.EncounterRecno
	,Encounter.SourceUniqueID

------------------------------------------------------------------------
--DATA GROUP: CDS V6 TYPE 005N - COMMISSIONING DATA SET TRANSACTION HEADER GROUP - NET CHANGE PROTOCOL
------------------------------------------------------------------------

	,CDSGroup = '060'

	,CDSType = wrk.CDSTypeCode

	 ,UniqueEpisodeSerialNo =
		left(
			'BRM2' +
			right(
				 replicate('0', 14) +
				 cast(Encounter.SourceUniqueID as varchar)
				,14
			)
			,35
		)

	,UpdateType = '9                ' --all records are either new or updated

	,CDSUpdateDate =
		left(
			CONVERT(varchar, getdate(), 120)
			,10
		)

	,CDSUpdateTime =
		left(
			replace(
				 CONVERT(varchar, getdate(), 108)
				,':'
				,''
			)
			,6
		)

	,CDSActivityDate =
		left(
			CONVERT(varchar, Encounter.AppointmentDate, 120)
			,10
		)


------------------------------------------------------------------------
--DATA GROUP: PATIENT PATHWAY
------------------------------------------------------------------------

	,RTTPathwayID =  --this is 25 characters on PAS
		left(
			case
			when len(Encounter.RTTPathwayID) < 20
			then
				stuff(
					 Encounter.RTTPathwayID
					,6
					,0
					,replicate('0', 20 - LEN(Encounter.RTTPathwayID))
				)
			else Encounter.RTTPathwayID
			end

			,20
		)

	,RTTCurrentProviderCode =
		case
		when Encounter.RTTPathwayID is null
		then null
		else
			coalesce(
				Encounter.RTTCurrentProviderCode
				,'RM200'
			)
		end

	,RTTPeriodStatusCode =
		coalesce(
			case
			when RTTPeriodStatus.NationalRTTStatusCode = 'NSP'
			then '99'
			else RTTPeriodStatus.NationalRTTStatusCode
			end
			,'99'
		)

	,RTTStartDate = 
		left(
			convert(varchar, Encounter.RTTStartDate, 120)
			,10
		)

	,RTTEndDate = 
		left(
			convert(varchar, Encounter.RTTEndDate, 120)
			,10
		)


------------------------------------------------------------------------
--DATA GROUP: PATIENT IDENTITY
------------------------------------------------------------------------

	,NNNStatusIndicator =
		case
		when SensitiveDiagnosis.OPSourceUniqueID is not null
		then '07'
		when SensitiveOperation.OPSourceUniqueID is not null
		then '07'
		when Encounter.NHSNumber is null
		then '04' --Trace attempted - No match or multiple match found
		else
			case
			when Encounter.NHSNumberStatusCode = 'SUCCS' then '01'
			when Encounter.NHSNumberStatusCode = 'RESET' then '02'
			when Encounter.NHSNumberStatusCode is null then '01'
			else Encounter.NHSNumberStatusCode
			end
		end

	,OverseasStatus = OverseasStatus.MappedCode
	
	,CCGofResidenceCode = 
		case when OverseasStatus.MappedCode in
			(
			 1	-- Exempt (reciprocal agreement)
			,2	-- Exempt from payment - other
			,3	-- To pay hotel fees only
			,4	-- To pay all fees
			,9	-- Charging rate not known
			)
		then 'X98'

		--For Scotland, NI and CI (NOT WALES) use DistrictOfResidence as lookup instead of PCTCode
		when left(PostcodeCCG.DistrictOfResidence,1) in ('S','Z','Y') then PostcodeCCG.DistrictOfResidence
		
		else
			--Lookup to ODS postcode_to_PCT table
			PostcodeCCG.CCGCode
		end
		

	,PracticeCCG = coalesce(
		ODSPractice.ParentOrganisationCode
		,PCT.OrganisationLocalCode)
	
	,LocalPatientID =
		left(
			Encounter.DistrictNo
			,10
		)

	,LocalPatientIDOrganisationCode = 'RM200' --UHSM

	,NHSNumber =
		case
		when SensitiveDiagnosis.OPSourceUniqueID is not null
		then ''
		when SensitiveOperation.OPSourceUniqueID is not null
		then ''
		else Encounter.NHSNumber
		end

	,Postcode = Encounter.Postcode
	
	,PostcodeAmended =
			case
			when SensitiveDiagnosis.OPSourceUniqueID is not null
			then null
			when SensitiveOperation.OPSourceUniqueID is not null
			then null
			when Encounter.Postcode is null
			then 'ZZ99 3AZ'
			when len(Encounter.Postcode) = 8 then Encounter.Postcode
			else left(Encounter.Postcode, 3) + ' ' + right(Encounter.Postcode, 4) 
			end

--KO added to allow link to Missing Patient data
	,PatientRefNo = Encounter.SourcePatientNo

	,PatientName = 
		left(
			case
			when SensitiveDiagnosis.OPSourceUniqueID is not null
			then null
			when SensitiveOperation.OPSourceUniqueID is not null
			then null
			when 
				case
				when Encounter.NHSNumber is null
				then '04' --Trace attempted - No match or multiple match found
				else
					case
					when Encounter.NHSNumberStatusCode = 'SUCCS' then '01'
					when Encounter.NHSNumberStatusCode = 'RESET' then '02'
					when Encounter.NHSNumberStatusCode is null then '01'
					else Encounter.NHSNumberStatusCode
					end
				end <> '01'
			then ltrim(rtrim(coalesce(Encounter.PatientForename, '') + ' ' + coalesce(Encounter.PatientSurname, '')))
			else ''
			end
			,70
		)

	,PatientAddress =
		left(
			case
			when SensitiveDiagnosis.OPSourceUniqueID is not null
			then null
			when SensitiveOperation.OPSourceUniqueID is not null
			then null
			when 
				case
				when Encounter.NHSNumber is null
				then '04' --Trace attempted - No match or multiple match found
				else
					case
					when Encounter.NHSNumberStatusCode = 'SUCCS' then '01'
					when Encounter.NHSNumberStatusCode = 'RESET' then '02'
					when Encounter.NHSNumberStatusCode is null then '01'
					else Encounter.NHSNumberStatusCode
					end
				end <> '01'
			then
				coalesce(Encounter.PatientAddress1 + ' ', 'Unknown Address ') + 
				coalesce(Encounter.PatientAddress2 + ' ', 'Unknown Address ') + 
				coalesce(Encounter.PatientAddress3 + ' ', 'Unknown Address ') + 
				coalesce(Encounter.PatientAddress4, 'Unknown Address')
			else ''
			end
			,175
		)


------------------------------------------------------------------------
--DATA GROUP: PATIENT CHARACTERISTICS
------------------------------------------------------------------------

	,DateOfBirth =
		left(
			CONVERT(varchar, Encounter.DateOfBirth, 120)
			,10
		)

	,SexCode = Sex.MappedCode
	
	,CarerSupportIndicator = null --not required on CDS

	,EthnicGroupCode =
		EthnicOrigin.MappedCode

	,MaritalStatusCode =
		MaritalStatus.MappedCode

	--,LegalStatusClassificationCode =
	--	LegalStatusClassification.MappedCode


------------------------------------------------------------------------
--DATA GROUP: CARE EPISODE - PERSON GROUP (CONSULTANT)
------------------------------------------------------------------------
	
	--,ConsultantCodeOld = Consultant.NationalConsultantCode
	--KO changed 14/02/2013 after CHKS report
	,ConsultantCode = 
		case 
			when coalesce(
				 ConsSpecMapping.XrefEntityCode
				,ConsultantSpecialty.NationalSpecialtyCode
				) in (
				'960' --ALLIED HEALTH PROFESSIONAL EPISODE
				,'901' --OCCUPATIONAL MEDICINE
				,'713' --PSYCHOTHERAPY
				)
				then 'H9999998'
			when Consultant.ProfessionalCarerType = 'Consultant'
				then case when Consultant.NationalConsultantCode not like '[C][0123456789]%'
					then 'C9999998'
					else Consultant.NationalConsultantCode
				end
			when Consultant.ProfessionalCarerType = 'Dentist'
				then case when Consultant.NationalConsultantCode not like '[D][0123456789]%'
					then 'D9999998'
					else Consultant.NationalConsultantCode
				end
			when Consultant.ProfessionalCarerType in (
				'Nurse'
				,'Nurse Practitioner'
				,'Specialist Nurse Practitioner'
				,'Specialist Practitioner'
				,'Clinical Nurse Specialist'
				)
				or Consultant.NationalConsultantCode like 'NUR%'
				then 'N9999998'
			when Consultant.NationalConsultantCode = 'MIDWIFE'
				then 'M9999998'
			when Consultant.ProfessionalCarerType in (
				'Allied Healthcare Professional'
				,'Dietician'
				,'Health Care Professional'
				,'Health Visitor'
				,'Occupational Therapist'
				,'Physiotherapist'
				,'Speech and Language Therapist'
				,'Associate Specialist'
				)
				then 'H9999998'
			when Consultant.NationalConsultantCode like '[C][0123456789]%'
				then Consultant.NationalConsultantCode
			else 'H9999998'
		end
	-- KO amended post NetTransform validation errors
	--,MainSpecialtyCode =
	--	coalesce(
	--		TreatmentFunctionSpecialtyMap.XrefEntityCode
	--		,TreatmentFunction.NationalSpecialtyCode
	--	)

	-- PDO amended to use Xref table
	--,MainSpecialtyCode = 
	--case left(coalesce(TreatmentFunctionSpecialtyMap.XrefEntityCode,TreatmentFunction.NationalSpecialtyCode),3)
	--when '650' then '950'	--Physiotherapy -> Nursing Episode
	--when '654' then '950'	--Dietetics -> Nursing Episode
	--when '901' then '310'	--Audiology (non-consultant) -> Audiology
	--when '656' then '904'	--Clinical Psychology -> Psychology
	--when '263' then '904'	--Paediatric Diabetic Medicine -> Paediatrics
	--when '500' then MainSpecialty.NationalSpecialtyCode
	--else
	--	coalesce(TreatmentFunctionSpecialtyMap.XrefEntityCode,TreatmentFunction.NationalSpecialtyCode)
	--end

	--,MainSpecialtyCode = 
	--	case
	--	when
	--		left(
	--			coalesce(
	--				 TreatmentFunctionSpecialtyMap.XrefEntityCode
	--				,TreatmentFunction.NationalSpecialtyCode
	--			)
	--			,3
	--		) = '500'
	--	then MainSpecialty.NationalSpecialtyCode

	--	else 
	--		coalesce(
	--			 TreatmentFunctionSpecialtyMap.XrefEntityCode
	--			,TreatmentFunction.NationalSpecialtyCode
	--		)
	--	end

	--,TreatmentFunctionCode =
	--	coalesce(
	--		 SpecialtyTreatmentFunctionMap.XrefEntityCode
	--		,TreatmentFunction.NationalSpecialtyCode
	--	)
		,MainSpecialtyCode = 
		case
		when
			left(
				coalesce(
					TxFuncMapping.XrefEntityCode
					,TreatmentFunction.NationalSpecialtyCode
				)
				,3
			) = '500'
		then ConsultantSpecialty.NationalSpecialtyCode

		else 
			coalesce(
				 ConsSpecMapping.XrefEntityCode
				,ConsultantSpecialty.NationalSpecialtyCode
				)
		end

	,TreatmentFunctionCode =
		case 
			when TreatmentFunction.NationalSpecialtyCode = '9010' --Audiology
			then '840'
			else coalesce(
					 TxFuncMapping.XrefEntityCode
					,TreatmentFunction.NationalSpecialtyCode
				)
		end
------------------------------------------------------------------------
--DATA GROUP: CARE EPISODE - CLINICAL DIAGNOSIS GROUP (ICD)
------------------------------------------------------------------------

	,DiagnosisSchemeInUse =
		case
		when not exists
			(
			select
				1
			from
				OP.Diagnosis
			where
				Diagnosis.OPSourceUniqueID = Encounter.SourceUniqueID
			)
		then null
		else '02' --ICD10
		end

--diagnoses are held in CDS.Diagnosis view

------------------------------------------------------------------------
--DATA GROUP: CARE EPISODE - CLINICAL DIAGNOSIS GROUP (READ)
------------------------------------------------------------------------
--Whole group is optional

------------------------------------------------------------------------
--DATA GROUP: CARE ATTENDANCE - ACTIVITY CHARACTERISTICS
------------------------------------------------------------------------

	,AttendanceIdentifier =
		Encounter.SourceUniqueID

	,AdministrationCategoryCode =
		AdminCategory.MappedCode

--KO amened post validation errors
-- DNA code 9 is not accepted - need to resolve to an acceptable code (lookup where RFVDM_CODE = 'ATTND')
	--,DNACode =
	--	Encounter.DNACode
	--,DNACode =
	--	case Encounter.AppointmentStatusCode
	--		when '2870' then '2'  --Cancelled By Patient
	--		when '2003534' then '2'  --Unable to Attend
	--		when '2004151' then '5'  --Attended
	--		--KO 08/08/2012 added extra DNA code mapping
	--		when '2868' then '6' --Patient Late / Seen
	--		when '2004301' then '2' --Cancelled by Patient
	--		--when '2004528' then '4' --Cancelled by EBS
	--		when '3006508' then '4' --Referral Cancelled
	--		when '2003494' then '3' --Patient Left Not Seen
	--		when '357' then '5' --Attended On Time
	--		when '358' then '3' --Did Not Attend
	--		when '2000724' then '7' --Arrived Late Not Seen
	--		when '2003495' then '3' --Refused To Wait
	--		--when '2003532' then '4' --Cancelled
	--		when '2004300' then '4' --Cancelled by Hospital
	--		--when '45' or '2004528' then --NSP (45) or Cancelled by EBS (2004528)
	--	else 
	--		case when CancelledReasonLu.MappedCode = 'P'
	--			then '2' --Not Specified
	--			when CancelledReasonLu.MappedCode = 'H'
	--			then '4' --Not Specified
	--		end--Encounter.DNACode
	--	end
	,DNACode = 
		case 
			when Encounter.DNACode = '9' then ''
			when Encounter.DNACode = '0' then ''
			else Encounter.DNACode
		end
		
	,FirstAttendanceFlag =
		FirstAttendance.MappedCode

--03 Lead Care Professional (Effective 2005-04-01) 
--04 Member of Care Professional team (Effective 2005-04-01) 
	,MedicalStaffTypeSeeingPatientCode =
		case ProfessionalCarerType.MappedCode
		when '01' then '03'
		when '02' then '04'
		when '98' then '08'
		else '09'
		end

	,OperationStatus =
		case
		when Encounter.PrimaryOperationCode is null
		then 8
		else 1
		end

	,OutcomeOfAttendanceCode =
		coalesce(
			OutcomeOfAttendance.MappedCode

			,case
			when Referral.DischargeDate is not null
			then 1	--Discharged from CONSULTANT's care (last attendance) 

			when exists
				(
				select
					1
				from
					OP.Encounter FutureAppointment
				where
					FutureAppointment.SourcePatientNo = Encounter.SourcePatientNo
				and	FutureAppointment.AppointmentDate > Encounter.AppointmentDate
				)
			then 2	--Another APPOINTMENT given
			else 3	--APPOINTMENT to be made at a later date
			end
		)

	,AppointmentDate =
		left(
			CONVERT(varchar, Encounter.AppointmentDate, 120)
			,10
		)

	,AgeAtCDSActivityDate = --CDS specifies completed years
		datediff(year, Encounter.DateOfBirth, Encounter.AppointmentDate)

	,EarliestReasonableOfferDate = --optional
		null

------------------------------------------------------------------------
--DATA GROUP: CARE ATTENDANCE - SERVICE AGREEMENT DETAILS
------------------------------------------------------------------------

	,CommissioningSerialNo =
		--Encounter.ContractSerialNo --UHSM - IPMAIN on old CDS
		coalesce(
			case
				when PbRDataset.PbRExcluded = 1 
				then PbRDataset.ExclusionType
				else PbRDataset.FinalSLACode
			end,
		'999999')
				
	,NHSServiceAgreementLineNo = null
		--Encounter.ContractSerialNo --Optional

	,PbRSourceDataset = 
		PbRDataset.EncounterTypeCode
		
	,PurchaserReferenceNo = null --Optional

	,CommissionerReferenceNo = '9' --Referral.REFBY_REFERENCE

	,ProviderCode = 'RM200' --UHSM
	,PurchaserCodeCCG =
		case 
			when OverseasStatus.MappedCode in (1,2)
				then '01N00'	--OS - Exempt from charges
			when OverseasStatus.MappedCode in (3,4)
				then 'VPP00'	--OS - Liable for charges
			when Encounter.Postcode like 'IM%'
				then 'YAC00'	--IOM patients don't seem to have overseas status code, so hard-code YAC here to
								--avoid purchaser being pulled from iPM Purchaser (usually TDH00)
			--IPM purchaser data is not correct, so use CCG of practice, unless
			--non English, private or overseas patient
			when 
				(left(
					coalesce(
						Purchaser.PurchaserMainCode
						,ODSPractice.ParentOrganisationCode
						,PCT.OrganisationLocalCode)
					, 1) in ('Z','Y','V','S','7') or
				left(Purchaser.PurchaserMainCode, 2) = 'TD')
				then 
					coalesce(
						left(Purchaser.PurchaserMainCode + '00', 5)
						,left(ODSPractice.ParentOrganisationCode + '00', 5)
						,left(PCT.OrganisationLocalCode + '00', 5)
					)
		else
			left(ODSPractice.ParentOrganisationCode + '00', 5)
		end
------------------------------------------------------------------------
--DATA GROUP: CARE ATTENDANCE - CLINICAL ACTIVITY GROUP (OPCS)
------------------------------------------------------------------------

	,ProcedureSchemeInUse =
		case
		when Encounter.PrimaryOperationCode is null
		then null
		else '02' --OPCS4
		end

--operations are held in CDS.Operation view


------------------------------------------------------------------------
--DATA GROUP: CARE ATTENDANCE - CLINICAL ACTIVITY GROUP (READ) 
------------------------------------------------------------------------
--Whole group is optional

------------------------------------------------------------------------
--DATA GROUP: LOCATION GROUP - ATTENDANCE
------------------------------------------------------------------------

	,LocationClassCode = '01'

	,SiteCode = 'RM202'

------------------------------------------------------------------------
--DATA GROUP: GP REGISTRATION
------------------------------------------------------------------------

	,RegisteredGpCode = GP.ProfessionalCarerMainCode

	,RegisteredGpPracticeCode = IPMPractice.OrganisationLocalCode

------------------------------------------------------------------------
--DATA GROUP: ACTIVITY CHARACTERISTICS - REFERRAL
------------------------------------------------------------------------

	,PriorityTypeCode =
		PriorityType.MappedCode

	,ServiceTypeRequestedCode =
		ReasonForReferral.MappedCode

	,SourceOfReferralCode =
		coalesce(
			SourceOfReferral.MappedCode
			,'97' --other - not initiated by the CONSULTANT responsible for the Consultant Out-Patient Episode
		)

	,ReferralDate =
		left(
			CONVERT(varchar, Encounter.ReferralDate, 120)
			,10
		)


------------------------------------------------------------------------
--DATA GROUP: REFERRER
------------------------------------------------------------------------
    -- New code added 04/07/2014 TJD to stop invalid local codes being included in this data item.
    ,ReferrerCode = CASE
                   WHEN SourceOfReferral.MappedCode = '17' THEN 'R9999981'
                   WHEN SourceOfReferral.MappedCode = '97' THEN 'R9999981'
                   WHEN Referrer.ProfessionalCarerType = 'Consultant'
                        AND Referrer.ProfessionalCarerMainCode LIKE 'C[0-9][0-9][0-9][0-9][0-9][0-9][0-9]' THEN Referrer.ProfessionalCarerMainCode
                   WHEN Referrer.ProfessionalCarerType = 'Consultant'
                        AND Referrer.ProfessionalCarerMainCode NOT LIKE 'C[0-9][0-9][0-9][0-9][0-9][0-9][0-9]' THEN 'C9999998'                        
                   WHEN Referrer.ProfessionalCarerType = 'General Practitioner'
                        AND Referrer.ProfessionalCarerMainCode LIKE 'G[0-9][0-9][0-9][0-9][0-9][0-9][0-9]' THEN Referrer.ProfessionalCarerMainCode                        
                   WHEN Referrer.ProfessionalCarerType = 'General Practitioner'
                        AND Referrer.ProfessionalCarerMainCode NOT LIKE 'G[0-9][0-9][0-9][0-9][0-9][0-9][0-9]' THEN 'G9999998'                          
                   WHEN Referrer.ProfessionalCarerType = 'Dentist'
                        AND Referrer.ProfessionalCarerMainCode LIKE 'D[0-9][0-9][0-9][0-9][0-9][0-9][0-9]' THEN Referrer.ProfessionalCarerMainCode                        
                   WHEN Referrer.ProfessionalCarerType = 'Dentist'
                        AND Referrer.ProfessionalCarerMainCode NOT LIKE 'D[0-9][0-9][0-9][0-9][0-9][0-9][0-9]' THEN 'D9999998'  
                   WHEN Referrer.ProfessionalCarerMainCode IS NOT NULL
                        AND Referrer.ProfessionalCarerMainCode <> 'X9999998' THEN 'R9999981'
                   ELSE 'X9999998' -- Not applicable, e.g. patient has self presented or not known
                 END
	--,ReferrerCode =
	--	case
	--	when SourceOfReferral.MappedCode = '17' --National Screening Programme
	--	then 'R9999981'

	--	when SourceOfReferral.MappedCode = '97' --Other
	--	then 'R9999981'
	--	else
	--		case
	--		when Referral.ReferredByConsultantCode is null
	--		then 'X9999998'

	--		when Referral.ReferredByConsultantCode = 198272 --Birth
	--		then 'X9999998'

	--		when Referral.ReferredByConsultantCode in
	--			(
	--			 205632 --not specified
	--			,10540092 --bad mapping
	--			,10100528 --Dr Day?
	--			)
	--		then 'C9999998'

	--		when Referral.ReferredByConsultantCode in
	--			(
	--			 10000470 --Midwife
	--			,150002484 --Specialist Nurse (NIBA)
	--			,10000472 --Nurse Led
	--			,198273 --Nurse
	--			,150000128 --Nurse
	--			,10539696 --Nurse
	--			,10540170 --Nurse Led (Gyn)
	--			,10539309 --PHTH
	--			,10542422 --Physio
	--			,10543349 --Physio
	--			,10542436 --Physio
	--			,10542421 --Physio
	--			,10542449 --Physio
	--			)
	--		then 'R9999981'

	--		else Referrer.ProfessionalCarerMainCode
	--		end
	--	end

	,ReferringOrganisationCode =
		case
			when ReferrerOrganisation.OrganisationLocalCode is null
				then 'X99999' --not known
			--else ReferrerOrganisation.OrganisationLocalCode
			when len(ReferrerOrganisation.OrganisationLocalCode) = 3
				then ReferrerOrganisation.OrganisationLocalCode + '00'
			else ReferrerOrganisation.OrganisationLocalCode
		end
------------------------------------------------------------------------
--DATA GROUP: CARE REFERRAL - MISSED APPOINTMENT OCCURRENCE
------------------------------------------------------------------------
--KO amended post validation failures.  
	--,LastDNAOrPatientCancelledDate =
	--	Encounter.QM08StartWaitDate
--CDS.OPBaseArdentia.LastDNAOrPatientCancelledDate data type changed from smalldate to varchar(10)
	--,LastDNAOrPatientCancelledDate =
	--	CONVERT(varchar(10), QM08StartWaitDate, 120)
--KO amended 18/02/2013
	--,LastDNAOrPatientCancelledDate = (
	--	select top 1 
	--		left(CONVERT(varchar, AppointmentTime, 120),10) 
	--	from OP.Encounter PreviousDNA
	--	where PreviousDNA.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID 
	--		and PreviousDNA.SourceUniqueID < Encounter.SourceUniqueID 
	--		and PreviousDNA.AppointmentStatusCode in (
	--			2870 --Cancelled By Patient
	--			,2003534  --Unable to Attend
	--			,2004301 --Cancelled by Patient
	--			,2003494 --Patient Left Not Seen
	--			,358 --Did Not Attend
	--			,2000724 --Arrived Late Not Seen
	--			,2003495 --Refused To Wait
	--			,2003532 --Cancelled - not sure about this as can't be sure patient cancelled
	--		)
	--	order by PreviousDNA.AppointmentTime desc
 --       )
	,LastDNAOrPatientCancelledDate = (
		select top 1 
			left(CONVERT(varchar, sq.AppointmentTime, 120),10) 
		from OP.Encounter sq
		where sq.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID 
			and sq.SourceUniqueID < Encounter.SourceUniqueID 
			and sq.DNACode in ('3','2')
		order by AppointmentTime desc
        )
------------------------------------------------------------------------
--DATA GROUP: HEALTHCARE RESOURCE GROUP - ACTIVITY CHARACTERISTICS
------------------------------------------------------------------------

	,HRGCode = null --HRG4Encounter.HRGCode --Optional

	,HRGVersionNumber = null --'4.4' --Optional


------------------------------------------------------------------------
--DATA GROUP: HEALTHCARE RESOURCE GROUP - CLINICAL ACTIVITY
------------------------------------------------------------------------

	,DGVPCode = null --replace(HRG4Encounter.DominantOperationCode, '.', '') --Optional


--UHSM new columns

	,PatientAddress1 =
		left(
			case
			when SensitiveDiagnosis.OPSourceUniqueID is not null
			then ''
			when SensitiveOperation.OPSourceUniqueID is not null
			then ''
			when 
				case
				when Encounter.NHSNumber is null
				then '04' --Trace attempted - No match or multiple match found
				else
					case
					when Encounter.NHSNumberStatusCode = 'SUCCS' then '01'
					when Encounter.NHSNumberStatusCode = 'RESET' then '02'
					when Encounter.NHSNumberStatusCode is null then '01'
					else Encounter.NHSNumberStatusCode
					end
				end <> '01'
			then
				coalesce(Encounter.PatientAddress1 + ' ', 'Unknown Address ')
			else ''
			end
			,35
		)

	,PatientAddress2 =
		left(
			case
			when SensitiveDiagnosis.OPSourceUniqueID is not null
			then ''
			when SensitiveOperation.OPSourceUniqueID is not null
			then ''
			when 
				case
				when Encounter.NHSNumber is null
				then '04' --Trace attempted - No match or multiple match found
				else
					case
					when Encounter.NHSNumberStatusCode = 'SUCCS' then '01'
					when Encounter.NHSNumberStatusCode = 'RESET' then '02'
					when Encounter.NHSNumberStatusCode is null then '01'
					else Encounter.NHSNumberStatusCode
					end
				end <> '01'
			then
				coalesce(Encounter.PatientAddress2 + ' ', 'Unknown Address ')
			else ''
			end
			,35
		)

	,PatientAddress3 =
		left(
			case
			when SensitiveDiagnosis.OPSourceUniqueID is not null
			then ''
			when SensitiveOperation.OPSourceUniqueID is not null
			then ''
			when 
				case
				when Encounter.NHSNumber is null
				then '04' --Trace attempted - No match or multiple match found
				else
					case
					when Encounter.NHSNumberStatusCode = 'SUCCS' then '01'
					when Encounter.NHSNumberStatusCode = 'RESET' then '02'
					when Encounter.NHSNumberStatusCode is null then '01'
					else Encounter.NHSNumberStatusCode
					end
				end <> '01'
			then
				coalesce(Encounter.PatientAddress3 + ' ', 'Unknown Address ')
			else ''
			end
			,35
		)

	,PatientAddress4 =
		left(
			case
			when SensitiveDiagnosis.OPSourceUniqueID is not null
			then ''
			when SensitiveOperation.OPSourceUniqueID is not null
			then ''
			when 
				case
				when Encounter.NHSNumber is null
				then '04' --Trace attempted - No match or multiple match found
				else
					case
					when Encounter.NHSNumberStatusCode = 'SUCCS' then '01'
					when Encounter.NHSNumberStatusCode = 'RESET' then '02'
					when Encounter.NHSNumberStatusCode is null then '01'
					else Encounter.NHSNumberStatusCode
					end
				end <> '01'
			then
				coalesce(Encounter.PatientAddress4 + ' ', 'Unknown Address ')
			else ''
			end
			,35
		)

	,PatientAddress5 = cast(null as varchar(35))

	,HACode =
		coalesce(
			 Encounter.DHACode
			,Postcode.PCTCode
			,PCT.OrganisationLocalCode
			,'5NT'
		)

	,PatientForename = 
		left(
			case
			when SensitiveDiagnosis.OPSourceUniqueID is not null
			then null
			when SensitiveOperation.OPSourceUniqueID is not null
			then null
			when 
				case
				when Encounter.NHSNumber is null
				then '04' --Trace attempted - No match or multiple match found
				else
					case
					when Encounter.NHSNumberStatusCode = 'SUCCS' then '01'
					when Encounter.NHSNumberStatusCode = 'RESET' then '02'
					when Encounter.NHSNumberStatusCode is null then '01'
					else Encounter.NHSNumberStatusCode
					end
				end <> '01'
			then ltrim(rtrim(coalesce(Encounter.PatientForename, '')))
			else ''
			end
			,35
		)

	,PatientSurname = 
		left(
			case
			when SensitiveDiagnosis.OPSourceUniqueID is not null
			then null
			when SensitiveOperation.OPSourceUniqueID is not null
			then null
			when 
				case
				when Encounter.NHSNumber is null
				then '04' --Trace attempted - No match or multiple match found
				else
					case
					when Encounter.NHSNumberStatusCode = 'SUCCS' then '01'
					when Encounter.NHSNumberStatusCode = 'RESET' then '02'
					when Encounter.NHSNumberStatusCode is null then '01'
					else Encounter.NHSNumberStatusCode
					end
				end <> '01'
			then ltrim(rtrim(coalesce(Encounter.PatientSurname, '')))
			else ''
			end
			,35
		)

	,Encounter.ClinicCode
--into CDS.OPBaseArdentiaCCG
from
	OP.Encounter

inner join CDS.wrk
on	wrk.EncounterRecno = Encounter.EncounterRecno
and	wrk.CDSTypeCode in ('020')

left join PAS.RTTStatus RTTPeriodStatus
on	RTTPeriodStatus.RTTStatusCode = Encounter.RTTPeriodStatusCode

left join Organisation.dbo.Postcode Postcode
on	Postcode.Postcode =
		case
		when len(Encounter.Postcode) = 8 then Encounter.Postcode
		else left(Encounter.Postcode, 3) + ' ' + right(Encounter.Postcode, 4) 
		end

left join OrganisationCCG.dbo.Postcode PostcodeCCG
	on	PostcodeCCG.Postcode =
		case
		when len(Encounter.Postcode) = 8 then Encounter.Postcode
		else left(Encounter.Postcode, 3) + ' ' + right(Encounter.Postcode, 4) 
		end
		
left join PAS.ReferenceValue Sex
on    Sex.ReferenceValueCode = Encounter.SexCode

left join PAS.ReferenceValue EthnicOrigin
on    EthnicOrigin.ReferenceValueCode = Encounter.EthnicOriginCode

left join PAS.ReferenceValue MaritalStatus
on	MaritalStatus.ReferenceValueCode = Encounter.MaritalStatusCode

left join
	(
	select distinct
		Diagnosis.OPSourceUniqueID
	from
		OP.Diagnosis
	where
		exists
		(
		select
			1
		from
			dbo.EntityLookup SensitiveDiagnosis
		where
			Diagnosis.DiagnosisCode = SensitiveDiagnosis.EntityCode
		and	SensitiveDiagnosis.EntityTypeCode = 'SENSITIVEDIAGNOSIS'
		)
	) SensitiveDiagnosis
on	SensitiveDiagnosis.OPSourceUniqueID = Encounter.SourceUniqueID

left join
	(
	select distinct
		Operation.OPSourceUniqueID
	from
		OP.Operation
	where
		exists
		(
		select
			1
		from
			dbo.EntityLookup SensitiveOperation
		where
			Operation.OperationCode = SensitiveOperation.EntityCode
		and	SensitiveOperation.EntityTypeCode = 'SENSITIVEOPERATION'
		)
	) SensitiveOperation
on	SensitiveOperation.OPSourceUniqueID = Encounter.SourceUniqueID

left join PAS.Specialty TreatmentFunction
on	TreatmentFunction.SpecialtyCode = Encounter.SpecialtyCode

left join PAS.Consultant
on	Consultant.ConsultantCode = Encounter.ConsultantCode

left join PAS.Specialty ConsultantSpecialty
on	ConsultantSpecialty.SpecialtyCode = Consultant.MainSpecialtyCode

--left join dbo.EntityXref TreatmentFunctionSpecialtyMap
--on	TreatmentFunctionSpecialtyMap.EntityTypeCode = 'TREATMENTFUNCTIONCODE'
--and	TreatmentFunctionSpecialtyMap.XrefEntityTypeCode = 'SPECIALTYCODE'
--and	TreatmentFunctionSpecialtyMap.EntityCode =
--		coalesce(
--			 left(TreatmentFunction.NationalSpecialtyCode, 3)
--			,MainSpecialty.NationalSpecialtyCode
--		)

--left join dbo.EntityXref SpecialtyTreatmentFunctionMap
--on	SpecialtyTreatmentFunctionMap.EntityTypeCode = 'SPECIALTYCODE'
--and	SpecialtyTreatmentFunctionMap.XrefEntityTypeCode = 'TREATMENTFUNCTIONCODE'
--and	SpecialtyTreatmentFunctionMap.EntityCode =
--		coalesce(
--			 left(TreatmentFunction.NationalSpecialtyCode, 3)
--			,MainSpecialty.NationalSpecialtyCode
--		)
		
left join dbo.EntityXref ConsSpecMapping
on	ConsSpecMapping.XrefEntityTypeCode = 'SPECIALTYCODE'
and	ConsSpecMapping.EntityCode =
		coalesce(
			left(ConsultantSpecialty.NationalSpecialtyCode, 3)
			,left(TreatmentFunction.NationalSpecialtyCode, 3)
		)

left join dbo.EntityXref TxFuncMapping
on	TxFuncMapping.XrefEntityTypeCode = 'TREATMENTFUNCTIONCODE'
and	TxFuncMapping.EntityCode =
		coalesce(
			 left(TreatmentFunction.NationalSpecialtyCode, 3)
			,left(ConsultantSpecialty.NationalSpecialtyCode,3)
		)

left join PAS.ReferenceValue OverseasStatus
on OverseasStatus.ReferenceValueCode = Encounter.OverseasStatusFlag

left join PAS.ReferenceValue AdminCategory
on    AdminCategory.ReferenceValueCode = Encounter.AdminCategoryCode

left join PAS.ReferenceValue FirstAttendance
on    FirstAttendance.ReferenceValueCode = Encounter.FirstAttendanceFlag

left join PAS.ProfessionalCarer
on    ProfessionalCarer.ProfessionalCarerCode = Encounter.ConsultantCode

left join PAS.ReferenceValue ProfessionalCarerType
on	ProfessionalCarerType.ReferenceValueCode = ProfessionalCarer.ProfessionalCarerTypeCode

left join PAS.ReferenceValue OutcomeOfAttendance
on	OutcomeOfAttendance.ReferenceValueCode = Encounter.DisposalCode

left join PAS.Purchaser
on    Purchaser.PurchaserCode = Encounter.PurchaserCode

left join PAS.ProfessionalCarer GP
on    GP.ProfessionalCarerCode = Encounter.RegisteredGpCode

left join PAS.Organisation IPMPractice
--KO changed 02/10/2012
--on	Practice.OrganisationCode = Encounter.RegisteredGpPracticeCode
on	IPMPractice.OrganisationCode = Encounter.EpisodicGpPracticeCode

left join PAS.Organisation PCT
on	PCT.OrganisationCode = IPMPractice.ParentOrganisationCode

left join OrganisationCCG.dbo.Practice ODSPractice
on ODSPractice.OrganisationCode = IPMPractice.OrganisationLocalCode
	
left join PAS.ReferenceValue PriorityType
on	PriorityType.ReferenceValueCode = Encounter.PriorityCode

left join PAS.ReferenceValue SourceOfReferral
on	SourceOfReferral.ReferenceValueCode = Encounter.SourceOfReferralCode

left join PAS.ReferenceValue ReasonForReferral
on	ReasonForReferral.ReferenceValueCode = Encounter.ReasonForReferralCode

left join RF.Encounter Referral
on	Referral.SourceUniqueID = Encounter.ReferralSourceUniqueID

left join PAS.ProfessionalCarer Referrer
on    Referrer.ProfessionalCarerCode = Referral.ReferredByConsultantCode

left join PAS.Organisation ReferrerOrganisation
on    ReferrerOrganisation.OrganisationCode = Referral.ReferredByOrganisationCode

--left join WHREPORTING.PbR.CDSSLACode SLA
--on SLA.SourceUniqueID = Encounter.SourceUniqueID

left join PbR2016.dbo.PbR PbRDataset
on PbRDataset.SourceUniqueID = Encounter.SourceUniqueID
and PbRDataset.DatasetCode in ('ENCOUNTER', 'WARDATT')

--KO changed 14/11/2012
--left join dbo.EntityXref PCTMap
--on	PCTMap.EntityTypeCode = 'OLDPCT'
--and	PCTMap.XrefEntityTypeCode = 'NEWPCT'
----and	PCTMap.EntityCode = left(Encounter.PurchaserCode, 3)
--and	PCTMap.EntityCode = left(Purchaser.PurchaserMainCode, 3)

left join dbo.EntityXref PCTMapPurchaser
	on	PCTMapPurchaser.EntityTypeCode = 'OLDPCT'
	and	PCTMapPurchaser.XrefEntityTypeCode = 'NEWPCT'
	and	PCTMapPurchaser.EntityCode = left(Purchaser.PurchaserMainCode, 3)
	
left join dbo.EntityXref PCTMapPractice
	on	PCTMapPurchaser.EntityTypeCode = 'OLDPCT'
	and	PCTMapPurchaser.XrefEntityTypeCode = 'NEWPCT'
	and	PCTMapPurchaser.EntityCode = left(PCT.OrganisationLocalCode, 3)

left join PTL.OPLookups CancelledReasonLu 
	on Encounter.ScheduledCancelReasonCode = CancelledReasonLu.ReferenceValueCode

where Encounter.SourceUniqueID <> 210766884

select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Date range = ' + @Bulkstart + ' to ' + @Bulkend +
	', Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'CDS - WH BuildOPBaseArdentiaCCG', @Stats, @StartTime

