﻿CREATE procedure [CDS].[BuildType010]  --FCE
as

declare @CommissionerType varchar(5)

select @CommissionerType = TextValue from dbo.Parameter where Parameter = 'CDSCOMMISSIONERTYPE'

--select @CommissionerType
--fill base here
if(@CommissionerType = 'CCG')
	begin
		exec CDS.BuildAPCBaseArdentiaCCG
	end
else
	begin
		exec CDS.BuildAPCBaseArdentiaPCT
	end
	
--populate diagnosis
exec CDS.IterativeSQL @sqlTemplate =
'update
	CDS.APCBaseArdentia
set
	DiagnosisCode<iteration> = Diagnosis.DiagnosisCode
from
	CDS.Diagnosis
where
	Diagnosis.EncounterRecno = APCBaseArdentia.EncounterRecno
and	Diagnosis.SequenceNo = <iteration>
'
,@iterations = 13

--populate operations
exec CDS.IterativeSQL @sqlTemplate =
'update
	CDS.APCBaseArdentia
set
	 OperationCode<iteration> = Operation.OperationCode
	,OperationDate<iteration> = Operation.OperationDate
from
	CDS.Operation
where
	Operation.EncounterRecno = APCBaseArdentia.EncounterRecno
and	Operation.SequenceNo = <iteration>
'
,@iterations = 12


--populate births
exec CDS.IterativeSQL @sqlTemplate =
'update
	CDS.APCBaseArdentia
set
	 Birth<iteration>AssessmentGestationLength =
		Birth.GestationLength
		--case
		--when isnumeric(Birth.AssessmentGestationLength) = 1
		--then Birth.AssessmentGestationLength
		--end

	,Birth<iteration>BirthOrder = Birth.BirthOrder
	,Birth<iteration>BirthWeight = Birth.BirthWeight

	,Birth<iteration>DateOfBirth =
		left(
			CONVERT(varchar, Birth.DateOfBirth, 120)
			,10
		)

	,Birth<iteration>DeliveryLocationClassCode = Birth.DeliveryLocationClassCode
	,Birth<iteration>DeliveryMethodCode = Birth.DeliveryMethodCode
	,Birth<iteration>DeliveryPlaceTypeActualCode = Birth.DeliveryPlaceTypeActualCode
	,Birth<iteration>LiveOrStillBirth = Birth.LiveOrStillBirth
	,Birth<iteration>LocalPatientIdentifier = Birth.LocalPatientIdentifier
	,Birth<iteration>NHSNumber = Birth.NHSNumber
	,Birth<iteration>NHSNumberStatusIndicator = Birth.NHSNumberStatusIndicator
	,Birth<iteration>ProviderCode = Birth.ProviderCode
	,Birth<iteration>ResuscitationMethodCode = Birth.ResuscitationMethodCode
	,Birth<iteration>SexCode = Birth.SexCode
	,Birth<iteration>StatusOfPersonConductingDeliveryCode = Birth.StatusOfPersonConductingDeliveryCode
from
	CDS.Birth
where
	Birth.MotherEpisodeSourceUniqueID = APCBaseArdentia.EpisodeSourceUniqueID
and	Birth.BirthOrder = <iteration>
'
,@iterations = 6
