﻿CREATE procedure [CDS].[BuildType060]
as

declare @CommissionerType varchar(5)

select @CommissionerType = TextValue from dbo.Parameter where Parameter = 'CDSCOMMISSIONERTYPE'

--select @CommissionerType
--fill base here
if(@CommissionerType = 'CCG')
	begin
		exec CDS.BuildOPBaseArdentiaCCG
	end
else
	begin
		exec CDS.BuildOPBaseArdentiaPCT
	end
	
	
--populate diagnosis
exec CDS.IterativeSQL @sqlTemplate =
'update
	CDS.OPBaseArdentia
set
	DiagnosisCode<iteration> = Diagnosis.DiagnosisCode
from
	CDS.OPDiagnosis Diagnosis
where
	Diagnosis.EncounterRecno = OPBaseArdentia.EncounterRecno
and	Diagnosis.SequenceNo = <iteration>
'
,@iterations = 13

--temporary exclusion of OPCS due to validation errors
--populate operations
exec CDS.IterativeSQL @sqlTemplate =
'update
	CDS.OPBaseArdentia
set
	 OperationCode<iteration> = Operation.OperationCode
	,OperationDate<iteration> = Operation.OperationDate
from
	CDS.OPOperation Operation
where
	Operation.EncounterRecno = OPBaseArdentia.EncounterRecno
and	Operation.SequenceNo = <iteration>
'
,@iterations = 12

