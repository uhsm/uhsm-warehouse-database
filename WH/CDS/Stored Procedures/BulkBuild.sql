﻿
CREATE procedure [CDS].[BulkBuild]
	 @CDSBulkReplacementGroupCode varchar (10) = '010'
	,@from smalldatetime = null
	,@to smalldatetime = null
	,@PreviousSubmissionDate date = null
as

--09/04/2013 - KO copy of PO Build SP, to remove net processing code
--SP creates a file suffix used to denote the date range selected in the CDS job.
--This suffix is later used by the SSIS package to name the output file.
--If no date parameters are set then @from will default to the start of the current 
--financial year and @end will default to yesterday.

--CODE  CLASSIFICATION  
--010 Finished General, Delivery and Birth Episodes 
--020 Unfinished General, Delivery and Birth Episodes 
--030 Other Delivery 
--040 Other Birth 
--050 Detained and/or Long Term Psychiatric Census 
--060 Outpatient (known as Care Activity in the Schema)  
--070 Standard variation of Elective Admission List End Of Period Census 
--080 New and Old variations of Elective Admission List End Of Period Census 
--090 Add variation of Elective Admission List Event During Period 
--100 Remove variation of Elective Admission List Event During Period 
--110 Offer variation of Elective Admission List Event During Period 
--120 Available/Unavailable variation of Elective Admission List Event During Period 
--130 New and Old variations of Elective Admission List Event During Period 
--140 Accident and Emergency Attendance 
--150 Future Outpatient (introduced in CDS Version 6 - known as Future Care Activity in the Schema)  


declare @financialYearStartDate smalldatetime

select
	@financialYearStartDate =
		case
		when datepart(month, GETDATE()) < 4
		then '1 apr ' + datename(year, dateadd(year, -1, GETDATE()))
		else '1 apr ' + datename(year, GETDATE())
		end
		
select
	 @from =
		coalesce(@from, @financialYearStartDate)

	,@to =
		coalesce(@to, DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(day, -1, GETDATE()))))

delete
from
	dbo.Parameter
where
	Parameter in
		(
		 'CDSREPORTPERIODSTARTDATE'
		,'CDSREPORTPERIODENDDATE'
		)

insert
into
	dbo.Parameter
(
	 Parameter
	,DateValue
)
select
	 Parameter = 'CDSREPORTPERIODSTARTDATE'
	,DateValue = @from

insert
into
	dbo.Parameter
(
	 Parameter
	,DateValue
)
select
	 Parameter = 'CDSREPORTPERIODENDDATE'
	,DateValue = @to


update dbo.Parameter set TextValue = 
	case 
		when @from > '2013-03-31 00:00:00.000'
		then 'CCG' 
		else 'PCT'
	end
where Parameter = 'CDSCOMMISSIONERTYPE'


declare @stdd varchar(2) 
declare @stmm varchar(2) 
declare @styy varchar(2) 

declare @enddd varchar(2) 
declare @endmm varchar(2) 
declare @endyy varchar(2) 

set @stdd = 
(select convert(varchar, datepart(day, DateValue)) 
from dbo.Parameter 
where Parameter = 'CDSREPORTPERIODSTARTDATE')

set @stmm = 
(select convert(varchar, datepart(month, DateValue)) 
from dbo.Parameter 
where Parameter = 'CDSREPORTPERIODSTARTDATE')

set @styy = 
(select right(convert(varchar, datepart(year, DateValue)),2) 
from dbo.Parameter 
where Parameter = 'CDSREPORTPERIODSTARTDATE')

set @enddd = 
(select convert(varchar, datepart(day, DateValue)) 
from dbo.Parameter 
where Parameter = 'CDSREPORTPERIODENDDATE')

set @endmm = 
(select convert(varchar, datepart(month, DateValue)) 
from dbo.Parameter 
where Parameter = 'CDSREPORTPERIODENDDATE')

set @endyy = 
(select right(convert(varchar, datepart(year, DateValue)),2) 
from dbo.Parameter 
where Parameter = 'CDSREPORTPERIODENDDATE')

update dbo.Parameter set TextValue = 
--'WK_' +
(replicate('0', 2 - datalength(@stdd)) + @stdd) +
(replicate('0', 2 - datalength(@stmm)) + @stmm) +
@styy +
'_' +
(replicate('0', 2 - datalength(@enddd)) + @enddd) + 
(replicate('0', 2 - datalength(@endmm)) + @endmm) + 
@endyy
where Parameter = 'CDSOUTPUTSUFFIX'

update dbo.Parameter set TextValue = 
(select TextValue from dbo.Parameter where Parameter = 'CDSCOMMISSIONERTYPE')
+ '_' + (select TextValue from dbo.Parameter where Parameter = 'CDSOUTPUTSUFFIX')
where Parameter = 'CDSOUTPUTSUFFIX'


--finished
if @CDSBulkReplacementGroupCode = '010' 
begin

-- CDSType 120, 130, 140
--this relies on an up-to-date MaternityReporting.dbo.MaternityTailsExport table
exec CDS.BuildMaternityTails

	delete
	from
		CDS.wrk
	where
		CDSTypeCode in ('120', '130', '140')


	insert into CDS.wrk
	(
		 CDSTypeCode
		,EncounterRecno
	)
	select
		CDSTypeCode =
			--'130'

			case
			when
				exists
					(
					select
						1
					from
						CDS.Birth
					where
						Birth.EpisodeSourceUniqueID = Encounter.SourceUniqueID
					)
			then '120' --birth

			when 
				exists
					(
					select
						1
					from
						CDS.Delivery
					where
						Delivery.EpisodeSourceUniqueID = Encounter.SourceUniqueID
					)
			then '140' --delivery

			else '130'
			end

		,Encounter.EncounterRecno
	from
		APC.Encounter Encounter
	where
		Encounter.EpisodeEndDate between @from and @to

	exec CDS.BuildType010

end



--outpatient
if @CDSBulkReplacementGroupCode = '060' 
begin
	-- CDSType 020

	delete
	from
		CDS.wrk
	where
		CDSTypeCode in ('020')

	insert into CDS.wrk
	(
		 CDSTypeCode
		,EncounterRecno
	)
	select
		'020'
		,EncounterRecno
	from
		OP.Encounter Encounter
	where
		Encounter.AppointmentDate between @from and @to

	exec CDS.BuildType060

end



