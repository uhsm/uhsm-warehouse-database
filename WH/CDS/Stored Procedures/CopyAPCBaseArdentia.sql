﻿

/*
--Author: K Oakden
--Date created: 08/04/2013
--Called by CDS-APC job

Copies of the APCBaseArdentia and CHKSAPC tables are archived to the CDSArchive database
with the date range and date stamp. So if the CDS job is run several times during a given day, 
then the archive copies of the output tables will be overwritten for that day only.

*/

CREATE procedure [CDS].[CopyAPCBaseArdentia] as


declare @tablename varchar(100) 
declare @Source varchar(100)
declare @StartTime datetime
declare @RowsInserted Int
declare @Stats varchar(255)
declare @CommissionerType varchar(5)

select @StartTime = getdate()
select @CommissionerType = TextValue from dbo.Parameter where Parameter = 'CDSCOMMISSIONERTYPE'

if(@CommissionerType = 'CCG')
	begin
		select @Source = 'CDS.APCArdentiaExportCCG'
	end
else
	begin
		select @Source = 'CDS.APCArdentiaExportPCT'
	end
--declare @stmm varchar(2) 
--declare @styy varchar(2) 

--declare @endmm varchar(2) 
--declare @endyy varchar(2) 

--set @stmm = 
--(select convert(varchar, datepart(month, DateValue)) 
--from dbo.Parameter 
--where Parameter = 'CDSREPORTPERIODSTARTDATE')

--set @styy = 
--(select right(convert(varchar, datepart(year, DateValue)),2) 
--from dbo.Parameter 
--where Parameter = 'CDSREPORTPERIODSTARTDATE')

--set @endmm = 
--(select convert(varchar, datepart(month, DateValue)) 
--from dbo.Parameter 
--where Parameter = 'CDSREPORTPERIODENDDATE')

--set @endyy = 
--(select right(convert(varchar, datepart(year, DateValue)),2) 
--from dbo.Parameter 
--where Parameter = 'CDSREPORTPERIODENDDATE')

--update dbo.Parameter set TextValue = (replicate('0', 2 - datalength(@stmm)) + @stmm) + @styy 
--+ '_' + (replicate('0', 2 - datalength(@endmm)) + @endmm) + @endyy
--where Parameter = 'CDSOUTPUTSUFFIXAPC'

------------------
--Copy CDS data
------------------
select @tablename = 
	'APCArdentiaExport_' 
	+ CONVERT(VARCHAR(8), GETDATE(), 112)
	+ '_' + TextValue
	from dbo.Parameter where Parameter = 'CDSOUTPUTSUFFIX'

exec(
'IF OBJECT_ID(''CDSArchive.CDS.' + @tablename + ''', ''U'') IS NOT NULL
DROP TABLE CDSArchive.CDS.' + @tablename
)

--exec ('select * into CDSArchive.CDS.' + @tablename + ' from CDS.APCBaseArdentia')
exec ('select * into CDSArchive.CDS.' + @tablename + ' from ' + @Source)

select @RowsInserted = @@rowcount
select @Stats = 
	'Rows archived to ' + @tablename + ' = ' + CONVERT(varchar(10), @RowsInserted) 
	
------------------
--Copy CHKS data
------------------
select @tablename = 
	'CHKSAPC_' 
	+ CONVERT(VARCHAR(8), GETDATE(), 112)
	+ '_' + TextValue
	from dbo.Parameter where Parameter = 'CDSOUTPUTSUFFIX'
print @tablename

exec(
'IF OBJECT_ID(''CDSArchive.CHKS.' + @tablename + ''', ''U'') IS NOT NULL
DROP TABLE CDSArchive.CHKS.' + @tablename
)

exec ('select * into CDSArchive.CHKS.' + @tablename + ' from CDS.CHKSAPC')
select @RowsInserted = @@rowcount
select @Stats = @Stats + 
	'. Rows archived to ' + @tablename + ' = ' + CONVERT(varchar(10), @RowsInserted) 

exec WriteAuditLogEvent 'CDS - WH CopyAPCBaseArdentia', @Stats, @StartTime

--insert into dbo.parameter (Parameter, TextValue)
--values ('CDSOUTPUTSUFFIXAPC', '')


--select * from dbo.Parameter where Parameter like 'CDSOUTPUTSUFFIX%'
