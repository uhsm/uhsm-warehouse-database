﻿
/*
--Author: K Oakden
--Date created: 08/03/2013
--Called by CDS-OP job

Copies of the OPArdentiaExport and CHKSOP tables are archived to the CDSArchive database
with the date range and date stamp. So if the CDS job is run several times during a given day, 
then the archive copies of the output tables will be overwritten for that day only.

*/
--select * from dbo.Parameter where Parameter like 'CDS%'

CREATE procedure [CDS].[CopyOPArdentiaExport] as

declare @tablename varchar(100) 
declare @Source varchar(100)
declare @StartTime datetime
declare @RowsInserted Int
declare @Stats varchar(255)
declare @CommissionerType varchar(5)

select @StartTime = getdate()
select @CommissionerType = TextValue from dbo.Parameter where Parameter = 'CDSCOMMISSIONERTYPE'

if(@CommissionerType = 'CCG')
	begin
		select @Source = 'CDS.OPArdentiaExportCCG'
	end
else
	begin
		select @Source = 'CDS.OPArdentiaExportPCT'
	end

------------------
--Copy CDS data
------------------
select @tablename = 
	'OPArdentiaExport_' 
	+ TextValue + '_'
	+ CONVERT(VARCHAR(8), GETDATE(), 112)
	--+ REPLACE(CONVERT(VARCHAR(10), GETDATE(), 3), '/', '') 
	from dbo.Parameter where Parameter = 'CDSOUTPUTSUFFIX'
print @tablename

exec(
'IF OBJECT_ID(''CDSArchive.CDS.' + @tablename + ''', ''U'') IS NOT NULL
DROP TABLE CDSArchive.CDS.' + @tablename
)

exec ('select * into CDSArchive.CDS.' + @tablename + ' from ' + @Source)

select @RowsInserted = @@rowcount
select @Stats = 
	'Rows archived to ' + @tablename + ' = ' + CONVERT(varchar(10), @RowsInserted) 

------------------
--Copy CHKS data
------------------
select @tablename = 
	'CHKSOP_' 
	+ TextValue + '_' 
	+ CONVERT(VARCHAR(8), GETDATE(), 112)
	--+ REPLACE(CONVERT(VARCHAR(10), GETDATE(), 3), '/', '') 
	from dbo.Parameter where Parameter = 'CDSOUTPUTSUFFIX'
print @tablename

exec(
'IF OBJECT_ID(''CDSArchive.CHKS.' + @tablename + ''', ''U'') IS NOT NULL
DROP TABLE CDSArchive.CHKS.' + @tablename
)

exec ('select * into CDSArchive.CHKS.' + @tablename + ' from CDS.CHKSOP')
select @RowsInserted = @@rowcount
select @Stats = @Stats + 
	'. Rows archived to ' + @tablename + ' = ' + CONVERT(varchar(10), @RowsInserted) 

exec WriteAuditLogEvent 'CDS - WH CopyOPArdentiaExport', @Stats, @StartTime

