﻿
/***********************************************************************************
 Description: Extract data from the A&E CDS archive for the required reporting
              period into the WH working tables ready for exporting.
              
              The BULKSTART and BULKEND field are overwritten with the specified
              reporting period dates as you may be extracting a different period to
              the one originally submitted to SUS, i.e. a quarter instead of a month.
              
              The DATETIME_CREATED is also overwritten with the current system date
              and time as we are treating it as a new SUS submission.
 
 Parameters:  @ReportPeriodStart - Start of the report period.
              @ReportPeriodEnd - End of the report period.
              
 Modification History --------------------------------------------------------------

 Date     Author      Change
 26/08/14 Tim Dean	  Created.
************************************************************************************/

CREATE Procedure [CDS].[ExtractAEArchive]
 @ReportPeriodStart DATETIME
,@ReportPeriodEnd DATETIME
AS

SET NOCOUNT ON;

TRUNCATE TABLE CDS.AEMain;
TRUNCATE TABLE CDS.AEDiagnosis;
TRUNCATE TABLE CDS.AEInvestigations;
TRUNCATE TABLE CDS.AEProcedures;

-- Extract the required A&E main data from the archive 
INSERT INTO CDS.AEMain
SELECT
  Main.PRIME_RECIPIENT
 ,Main.COPY_RECIPIENT_1
 ,Main.COPY_RECIPIENT_2
 ,Main.COPY_RECIPIENT_3
 ,Main.COPY_RECIPIENT_4
 ,Main.COPY_RECIPIENT_5
 ,Main.SENDER
 ,Main.CDS_GROUP
 ,Main.CDS_TYPE
 ,Main.CDS_ID
 ,Main.UPDATE_TYPE
 ,Main.PROTOCOL_IDENTIFIER
 ,BULKSTART = COALESCE(LEFT(CONVERT(VARCHAR,@ReportPeriodStart,120),10),'')  --Main.BULKSTART
 ,BULKEND = COALESCE(LEFT(CONVERT(VARCHAR,@ReportPeriodEnd,120),10),'') --Main.BULKEND
 ,DATETIME_CREATED = LEFT(CONVERT(VARCHAR,GETDATE(),120),16) --Main.DATETIME_CREATED
 ,Main.PROVIDER
 ,Main.PURCHASER
 ,Main.SERIAL_NO
 ,Main.NHS_NO
 ,Main.NHS_NO_STATUS
 ,Main.NAME_FORMAT
 ,Main.ADDRESS_FORMAT
 ,Main.NAME
 ,Main.FORENAME
 ,Main.HOMEADD1
 ,Main.HOMEADD2
 ,Main.HOMEADD3
 ,Main.HOMEADD4
 ,Main.HOMEADD5
 ,Main.POSTCODE
 ,Main.HA
 ,Main.WITHHELD_IDENTITY_REASON
 ,Main.SEX
 ,Main.CARER_SUPPORT_IND
 ,Main.DOB
 ,Main.GPREG
 ,Main.PRACREG
 ,Main.LOCPATID
 ,Main.ATTENDNO
 ,Main.CATEGORY
 ,Main.ARRDATE
 ,Main.REFSOURCE
 ,Main.DEPARTMENT_TYPE
 ,Main.ARRMODE
 ,Main.ARRTIME
 ,Main.ACTIVITY_DATE
 ,Main.AGE_AT_CDS_ACTIVITY_DATE
 ,Main.VISITOR_STAT_AT_ACTIVITY_DATE
 ,Main.PATGROUP
 ,Main.INCLOCTYPE
 ,Main.AE_INITIAL_ASSESSMENT_DATE
 ,Main.ASSTIME
 ,Main.AE_SEEN_FOR_TREATMENT_DATE
 ,Main.TREATIME
 ,Main.STAFFCODE
 ,Main.AE_ATTENDANCE_CONCLUSION_DATE
 ,Main.ENDTIME
 ,Main.AE_DEPARTURE_DATE
 ,Main.DEPTIME
 ,Main.AMBULANCE_INCIDENT_NUMBER
 ,Main.CONVEYING_AMBULANCE_TRUST_ORG
 ,Main.DISPOSAL
 ,Main.LOCAL_PATIENT_ID_ORG
 ,Main.HEALTH_CLINIC
 ,Main.PURCHASER_REF
 ,Main.PROVIDER_REF_NO
 ,Main.UBRN
 ,Main.CARE_PATHWAY_ID
 ,Main.CARE_PATHWAY_ID_ORG
 ,Main.REF_TO_TREAT_PERIOD_STATUS
 ,Main.REF_TO_TREAT_PERIOD_START_DATE
 ,Main.REF_TO_TREAT_PERIOD_END_DATE
 ,Main.WAIT_TIME_MEASUREMENT_TYPE
 ,Main.ETHNIC_CATEGORY
 ,Main.SITE_CODE_OF_TREATMENT
 ,Main.INVESTIGATION_SCHEME
 ,Main.DIAGNOSIS_SCHEME
 ,Main.TREATMENT_SCHEME
 ,Main.ICD_DIAG_SCHEME
 ,Main.READ_DIAG_SCHEME
 ,Main.OPCS_PROC_SCHEME
 ,Main.READ_PROC_SCHEME
FROM (
      -- Identify the latest verison of each CDS_ID within the required ACTIVITY_DATE range
      SELECT  DataLookup.CDS_ID
             ,MAX(DataLookup.SUS_SENT_DATE) LATEST_SUS_SENT_DATE
      FROM CDSArchive.CDS.AEArchiveMain DataLookup
      WHERE ACTIVITY_DATE BETWEEN @ReportPeriodStart AND @ReportPeriodEnd
      GROUP BY DataLookup.CDS_ID
     ) DataToExtract
INNER JOIN CDSArchive.CDS.AEArchiveMain Main
    ON (DataToExtract.CDS_ID = Main.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Main.SUS_SENT_DATE);
    
-- Extract the required A&E diagnosis data from the archive         
INSERT INTO CDS.AEDiagnosis
SELECT
  Diag.CDS_ID
 ,Diag.SEQUENCE_NUMBER
 ,Diag.AE_DIAG
FROM (
      -- Identify the latest verison of each CDS_ID within the required ACTIVITY_DATE range
      SELECT  DataLookup.CDS_ID
             ,MAX(DataLookup.SUS_SENT_DATE) LATEST_SUS_SENT_DATE
      FROM CDSArchive.CDS.AEArchiveMain DataLookup
      WHERE ACTIVITY_DATE BETWEEN @ReportPeriodStart AND @ReportPeriodEnd
      GROUP BY DataLookup.CDS_ID
     ) DataToExtract
INNER JOIN CDSArchive.CDS.AEArchiveDiagnosis Diag
    ON (DataToExtract.CDS_ID = Diag.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Diag.SUS_SENT_DATE);     

-- Extract the required A&E investigations data from the archive        
INSERT INTO CDS.AEInvestigations
SELECT
  Invest.CDS_ID
 ,Invest.SEQUENCE_NUMBER
 ,Invest.AE_INVEST
FROM (
      -- Identify the latest verison of each CDS_ID within the required ACTIVITY_DATE range
      SELECT  DataLookup.CDS_ID
             ,MAX(DataLookup.SUS_SENT_DATE) LATEST_SUS_SENT_DATE
      FROM CDSArchive.CDS.AEArchiveMain DataLookup
      WHERE ACTIVITY_DATE BETWEEN @ReportPeriodStart AND @ReportPeriodEnd
      GROUP BY DataLookup.CDS_ID
     ) DataToExtract
INNER JOIN CDSArchive.CDS.AEArchiveInvestigations Invest
    ON (DataToExtract.CDS_ID = Invest.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Invest.SUS_SENT_DATE);  
        
-- Extract the required A&E procedures data from the archive        
INSERT INTO CDS.AEProcedures
SELECT
  Pro.CDS_ID
 ,Pro.SEQUENCE_NUMBER
 ,Pro.AE_PROC
 ,Pro.AE_PROC_DATE
FROM (
      -- Identify the latest verison of each CDS_ID within the required ACTIVITY_DATE range
      SELECT  DataLookup.CDS_ID
             ,MAX(DataLookup.SUS_SENT_DATE) LATEST_SUS_SENT_DATE
      FROM CDSArchive.CDS.AEArchiveMain DataLookup
      WHERE ACTIVITY_DATE BETWEEN @ReportPeriodStart AND @ReportPeriodEnd
      GROUP BY DataLookup.CDS_ID
     ) DataToExtract
INNER JOIN CDSArchive.CDS.AEArchiveProcedures Pro
    ON (DataToExtract.CDS_ID = Pro.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Pro.SUS_SENT_DATE);          


