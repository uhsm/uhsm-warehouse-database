﻿

/***********************************************************************************
 Description: Extract APC Birth Occurrence data into staging data table.
 
 Modification History --------------------------------------------------------------

 Date     Author      Change
 26/03/14 Tim Dean	  Created.
 25/06/14 Tim Dean    Problem with loading of Evolution data has highlighted an
                      issue with the MaternityBirthNotification not excluding deleted
                      rows.
************************************************************************************/

CREATE Procedure [CDS].[ExtractAPCBirthOccurrence]
AS

SET NOCOUNT ON;

TRUNCATE TABLE CDS.APCStagingBirth;

-- Extract required Evolution maternity birth data for the birth episodes.                  
INSERT INTO CDS.APCStagingBirth
            (
             EpisodeSourceUniqueID
	        ,BirthOrder
	        ,ActualPlaceOfBirth
	        ,AnalgesiaOrAnaesthesia
	        ,BabyBirthDate
	        ,BabysHospitalNumber
	        ,BabysNHSNumber
	        ,BabysNHSNumberStatus
	        ,BirthWeight
	        ,DeliveryDate
            ,DeliveryMethod
	        ,EstimateGestationWeeks
	        ,InitialAssessmentDate
	        ,IntendedPlaceOfBirth
	        ,MethodOfLabourOnset
	        ,MotherAddress1
	        ,MotherAddress2
	        ,MotherAddress3
	        ,MotherAddress4
	        ,MotherAddress5
	        ,MotherBirthDate
	        ,MotherGpCode
	        ,MotherGpPracticeCode
	        ,MotherHospitalNumber
	        ,MotherNHSNumber
	        ,MotherPostCode
	        ,NationalResuscitationCode
	        ,NumberBornThisDelivery
 	        ,NumberOfPastPregnancies
	        ,OutcomeOfBirth
	        ,OutcomeOfBirthDeathOfFetus
	        ,PostDeliveryAnalgesia
	        ,ReasonForChange
	        ,SexOfBaby
	        ,TypeOfPersonDelivering)
SELECT EpisodeSourceUniqueID = APCStagingMain .EpisodeSourceUniqueID
      ,BirthOrder = Birth.[Birth Order]
      ,ActualPlaceOfBirth = Birth.[Korner Actual Place of Birth]
      ,AnalgesiaOrAnaesthesia = Birth.[Korner Analgesia/Anaesthesia]
      ,BabyBirthDate = Baby.[Date of Birth]
      ,BabysHospitalNumber = Baby.[Babys Hospital number]
      ,BabysNHSNumber = Baby.[Babys NHS number]
      ,BabysNHSNumberStatus = Baby.[Babys NHS number status]
      ,BirthWeight = Baby.[Birth weight (grams)]
      ,DeliveryDate = Delivery.[Date of delivery]
      ,DeliveryMethod = Birth.[Korner Method of delivery]
      ,EstimateGestationWeeks = Birth.[Estimate of gestation (weeks)]
      ,InitialAssessmentDate = PregnancyAssessment.[Date of Initial Assessment]
      ,IntendedPlaceOfBirth = Birth.[Korner Intended Place of Birth]
      ,MethodOfLabourOnset = Birth.[Korner Method of Labour Onset]
      ,MotherAddress1 = Mother.Address1
      ,MotherAddress2 = Mother.Address2
      ,MotherAddress3 = Mother.Address3
      ,MotherAddress4 = Mother.Address4
      ,MotherAddress5 = '' -- There is currently no 5th address line.
      ,MotherBirthDate = Mother.BirthDate
      ,MotherGpCode = Mother.GP_Code
      ,MotherGpPracticeCode = Mother.GP_Practice
      ,MotherHospitalNumber = Mother.[Unit Number]
      ,MotherNHSNumber = Mother.[NHS Number]
      ,MotherPostCode = Mother.Postcode
      ,NationalResuscitationCode = BabyResusitation.[National Resuscitation Code]
      ,NumberBornThisDelivery = Delivery.[Number born this delivery]
      ,NumberOfPastPregnancies = ObstetricSummary.[Number of Past Pregnancies]
      ,OutcomeOfBirth = Birth.[Outcome of birth]
      ,OutcomeOfBirthDeathOfFetus = Birth.[Outcome of birth Death_of_Fetus]
      ,PostDeliveryAnalgesia = Birth.[Korner Post Delivery Analgesia]
      ,ReasonForChange = Birth.[Korner Reason for Change]
      ,SexOfBaby = Baby.[Sex of baby]
      ,TypeOfPersonDelivering = StaffPresent.[Type of person delivering]
FROM   CDS.APCStagingMain       
       
  INNER JOIN CDS.MaternityBirthNotification
    ON APCStagingMain.EpisodeSourceUniqueID = MaternityBirthNotification.BirthSourceUniqueID  
           
  LEFT JOIN MaternityReporting.dbo.view_Baby_Birth Birth
    ON MaternityBirthNotification.PatientPointer = Birth.Patient_Pointer
        AND MaternityBirthNotification.BirthOrder = Birth.[Birth Order]
        AND MaternityBirthNotification.BirthDtm = (Birth.[Date of Birth] + Birth.[Time of Birth]) 
                 
  LEFT JOIN MaternityReporting.dbo.view_Baby_Details Baby
    ON Birth.Patient_Pointer = Baby.Patient_Pointer
        AND Birth.Pregnancy_ID = Baby.Pregnancy_ID
        AND Birth.Delivery_Number = Baby.Delivery_Number
        AND Birth.Baby_Number = Baby.Baby_Number
       
  LEFT JOIN MaternityReporting.dbo.view_Delivery Delivery
    ON Birth.Patient_Pointer = Delivery.Patient_Pointer
        AND Birth.Pregnancy_ID = Delivery.Pregnancy_ID
        AND Birth.Delivery_Number = Delivery.Delivery_Number
       
  LEFT JOIN MaternityReporting.dbo.view_Pregnancy Pregnancy
    ON Birth.Patient_Pointer = Pregnancy.Patient_Pointer
        AND Birth.Pregnancy_ID = Pregnancy.Pregnancy_ID
       
  LEFT JOIN MaternityReporting.dbo.view_Pregnancy_Assessment PregnancyAssessment
    ON Birth.Patient_Pointer = PregnancyAssessment.Patient_Pointer
        AND Birth.Pregnancy_ID = PregnancyAssessment.Pregnancy_ID
       
  LEFT JOIN MaternityReporting.dbo.view_Patients Mother
    ON Birth.Patient_Pointer = Mother.Patient_Pointer
       
  LEFT JOIN MaternityReporting.dbo.view_Baby_Birth_StaffPresent StaffPresent
    ON Birth.Patient_Pointer = StaffPresent.Patient_Pointer
        AND Birth.Pregnancy_ID = StaffPresent.Pregnancy_ID
        AND Birth.Delivery_Number = StaffPresent.Delivery_Number
        AND Birth.Baby_Number = StaffPresent.Baby_Number
       
  LEFT JOIN MaternityReporting.dbo.view_Baby_Resuscitation BabyResusitation
    ON Baby.Patient_Pointer = BabyResusitation.Patient_Pointer
        AND Baby.Pregnancy_ID = BabyResusitation.Pregnancy_ID
        AND Baby.Delivery_Number = BabyResusitation.Delivery_Number
        AND Baby.Baby_Number = BabyResusitation.Baby_Number
             
  LEFT JOIN MaternityReporting.dbo.view_ObstetricSummary ObstetricSummary
    ON Birth.Patient_Pointer = ObstetricSummary.Patient_Pointer
WHERE Birth.[Birth Order] IS NOT NULL
      AND MaternityBirthNotification.IsDeleted = 'N' -- 25/06/2014 TJD Added
;   

-- Extract required Evolution maternity birth data for the delivery episodes.  
INSERT INTO CDS.APCStagingBirth
            (
             EpisodeSourceUniqueID
	        ,BirthOrder
	        ,ActualPlaceOfBirth
	        ,AnalgesiaOrAnaesthesia
	        ,BabyBirthDate
	        ,BabysHospitalNumber
	        ,BabysNHSNumber
	        ,BabysNHSNumberStatus
	        ,BirthWeight
	        ,DeliveryDate
            ,DeliveryMethod
	        ,EstimateGestationWeeks
	        ,InitialAssessmentDate
	        ,IntendedPlaceOfBirth
	        ,MethodOfLabourOnset
	        ,MotherAddress1
	        ,MotherAddress2
	        ,MotherAddress3
	        ,MotherAddress4
	        ,MotherAddress5
	        ,MotherBirthDate
	        ,MotherGpCode
	        ,MotherGpPracticeCode
	        ,MotherHospitalNumber
	        ,MotherNHSNumber
	        ,MotherPostCode
	        ,NationalResuscitationCode
	        ,NumberBornThisDelivery
 	        ,NumberOfPastPregnancies
	        ,OutcomeOfBirth
	        ,OutcomeOfBirthDeathOfFetus
	        ,PostDeliveryAnalgesia
	        ,ReasonForChange
	        ,SexOfBaby
	        ,TypeOfPersonDelivering)
SELECT EpisodeSourceUniqueID = APCStagingMain .EpisodeSourceUniqueID
      ,BirthOrder = Birth.[Birth Order]
      ,ActualPlaceOfBirth = Birth.[Korner Actual Place of Birth]
      ,AnalgesiaOrAnaesthesia = Birth.[Korner Analgesia/Anaesthesia]
      ,BabyBirthDate = Baby.[Date of Birth]
      ,BabysHospitalNumber = Baby.[Babys Hospital number]
      ,BabysNHSNumber = Baby.[Babys NHS number]
      ,BabysNHSNumberStatus = Baby.[Babys NHS number status]
      ,BirthWeight = Baby.[Birth weight (grams)]
      ,DeliveryDate = Delivery.[Date of delivery]
      ,DeliveryMethod = Birth.[Korner Method of delivery]
      ,EstimateGestationWeeks = Birth.[Estimate of gestation (weeks)]
      ,InitialAssessmentDate = PregnancyAssessment.[Date of Initial Assessment]
      ,IntendedPlaceOfBirth = Birth.[Korner Intended Place of Birth]
      ,MethodOfLabourOnset = Birth.[Korner Method of Labour Onset]
      ,MotherAddress1 = Mother.Address1
      ,MotherAddress2 = Mother.Address2
      ,MotherAddress3 = Mother.Address3
      ,MotherAddress4 = Mother.Address4
      ,MotherAddress5 = '' -- There is currently no 5th address line.
      ,MotherBirthDate = Mother.BirthDate
      ,MotherGpCode = Mother.GP_Code
      ,MotherGpPracticeCode = Mother.GP_Practice
      ,MotherHospitalNumber = Mother.[Unit Number]
      ,MotherNHSNumber = Mother.[NHS Number]
      ,MotherPostCode = Mother.Postcode
      ,NationalResuscitationCode = BabyResusitation.[National Resuscitation Code]
      ,NumberBornThisDelivery = Delivery.[Number born this delivery]
      ,NumberOfPastPregnancies = ObstetricSummary.[Number of Past Pregnancies]
      ,OutcomeOfBirth = Birth.[Outcome of birth]
      ,OutcomeOfBirthDeathOfFetus = Birth.[Outcome of birth Death_of_Fetus]
      ,PostDeliveryAnalgesia = Birth.[Korner Post Delivery Analgesia]
      ,ReasonForChange = Birth.[Korner Reason for Change]
      ,SexOfBaby = Baby.[Sex of baby]
      ,TypeOfPersonDelivering = StaffPresent.[Type of person delivering]
FROM   CDS.APCStagingMain       
       
  INNER JOIN CDS.MaternityBirthNotification
    ON APCStagingMain.EpisodeSourceUniqueID = MaternityBirthNotification.DeliverySourceUniqueID
       
  LEFT JOIN MaternityReporting.dbo.view_Baby_Birth Birth
    ON MaternityBirthNotification.PatientPointer = Birth.Patient_Pointer
        AND MaternityBirthNotification.BirthOrder = Birth.[Birth Order]
        AND MaternityBirthNotification.BirthDtm = (Birth.[Date of Birth] + Birth.[Time of Birth]) 
                 
  LEFT JOIN MaternityReporting.dbo.view_Baby_Details Baby
    ON Birth.Patient_Pointer = Baby.Patient_Pointer
        AND Birth.Pregnancy_ID = Baby.Pregnancy_ID
        AND Birth.Delivery_Number = Baby.Delivery_Number
        AND Birth.Baby_Number = Baby.Baby_Number
       
  LEFT JOIN MaternityReporting.dbo.view_Delivery Delivery
    ON Birth.Patient_Pointer = Delivery.Patient_Pointer
        AND Birth.Pregnancy_ID = Delivery.Pregnancy_ID
        AND Birth.Delivery_Number = Delivery.Delivery_Number
       
  LEFT JOIN MaternityReporting.dbo.view_Pregnancy Pregnancy
    ON Birth.Patient_Pointer = Pregnancy.Patient_Pointer
        AND Birth.Pregnancy_ID = Pregnancy.Pregnancy_ID
       
  LEFT JOIN MaternityReporting.dbo.view_Pregnancy_Assessment PregnancyAssessment
    ON Birth.Patient_Pointer = PregnancyAssessment.Patient_Pointer
        AND Birth.Pregnancy_ID = PregnancyAssessment.Pregnancy_ID
       
  LEFT JOIN MaternityReporting.dbo.view_Patients Mother
    ON Birth.Patient_Pointer = Mother.Patient_Pointer
       
  LEFT JOIN MaternityReporting.dbo.view_Baby_Birth_StaffPresent StaffPresent
    ON Birth.Patient_Pointer = StaffPresent.Patient_Pointer
        AND Birth.Pregnancy_ID = StaffPresent.Pregnancy_ID
        AND Birth.Delivery_Number = StaffPresent.Delivery_Number
        AND Birth.Baby_Number = StaffPresent.Baby_Number
       
  LEFT JOIN MaternityReporting.dbo.view_Baby_Resuscitation BabyResusitation
    ON Baby.Patient_Pointer = BabyResusitation.Patient_Pointer
        AND Baby.Pregnancy_ID = BabyResusitation.Pregnancy_ID
        AND Baby.Delivery_Number = BabyResusitation.Delivery_Number
        AND Baby.Baby_Number = BabyResusitation.Baby_Number
             
  LEFT JOIN MaternityReporting.dbo.view_ObstetricSummary ObstetricSummary
    ON Birth.Patient_Pointer = ObstetricSummary.Patient_Pointer
WHERE Birth.[Birth Order] IS NOT NULL
      AND MaternityBirthNotification.IsDeleted = 'N' -- 25/06/2014 TJD Added
;  


