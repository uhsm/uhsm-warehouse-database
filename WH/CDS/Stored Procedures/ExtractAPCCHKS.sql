﻿



/***********************************************************************************
 Description: Extract APC CHKS dataset for required period.
 
 Parameters:  @ReportPeriodStart - Start of the reporting period.
              @ReportPeriodEnd - End of the reporting period.
              
              Executed by CDS APC CHKS Export SSIS Package
 Modification History --------------------------------------------------------------

 Date     Author      Change
 23/04/14 Tim Dean	  Created.
 19/05/14 Tim Dean    Corrected join for Procedure 12 details.
 22/05/15 Tim Dean	  Replace NULL values with empty string.

************************************************************************************/

CREATE Procedure [CDS].[ExtractAPCCHKS]
 @ReportPeriodStart DATETIME
,@ReportPeriodEnd DATETIME
AS

SELECT [LOCAL PATIENT IDENTIFIER] = Main.LOCPATID
      ,[NHS NUMBER] = Main.NHS_NO
      ,[BIRTH DATE] = REPLACE(Main.DOB, '-','')
      ,[ETHNIC CATEGORY] = Main.ETHNICOR
      ,[MARITAL STATUS] = Main.MARITAL_STATUS
      ,[SEX] = Main.SEX
      ,[POSTCODE OF USUAL ADDRESS] = Main.POSTCODE
      ,[PCT OF RESIDENCE] = Main.HA
      ,[HOSPITAL PROVIDER SPELL NUMBER] = Main.PROV_SPELL
      ,[ADMINISTRATIVE CATEGORY] = Main.ADMIN_CATGRY
      ,[PATIENT CLASSIFICATION] = Main.CLASSPAT
      ,[ADMISSION METHOD (HOSPITAL PROVIDER SPELL)] = Main.ADMIMETH
      ,[SOURCE OF ADMISSION (HOSPITAL PROVIDER SPELL)] = Main.ADMISORC
      ,[DURATION OF ELECTIVE WAIT] = Main.ELECDUR
      ,[INTENDED MANAGEMENT] = Main.MAN_INTENT     
      ,[DECIDED TO ADMIT DATE] = REPLACE(Main.ELECDATE,'-','')
      ,[GMP (CODE OF REGISTERED OR REFERRING GMP)] = Main.GPREG
      ,[CODE OF GP PRACTICE (REGISTERED GMP)] = Main.PRACREG
      ,[REFERRER CODE] = Main.REFERRER
      ,[REFERRING ORGANISATION CODE] = Main.REF_ORG
      ,[DISCHARGE DESTINATION (HOSPITAL PROVIDER SPELL)] = Main.DISDEST
      ,[DISCHARGE METHOD (HOSPITAL PROVIDER SPELL)] = Main.DISMETH
      ,[START DATE (HOSPITAL PROVIDER SPELL)] = REPLACE(Main.ADMIDATE,'-','')
      ,[DISCHARGE DATE (HOSPITAL PROVIDER SPELL)] = REPLACE(Main.DISDATE,'-','')
      ,[EPISODE NUMBER] = Main.EPIORDER
      ,[FIRST REGULAR DAY OR NIGHT ADMISSION] = Main.FIRST_REG_ADM
      ,[LAST EPISODE IN SPELL INDICATOR] = Main.LAST_IN_SPELL
      ,[NEONATAL LEVEL OF CARE] = Main.NEONATAL_LEVEL
      ,[CARER SUPPORT INDICATOR] = Main.CARER_SUPPORT_IND
      ,[LEGAL STATUS CLASSIFICATION CODE (ON ADMISSION)] = Main.LEGAL_STATUS
      ,[OPERATION STATUS] = Main.OPER_STATUS
      ,[START DATE (CONSULTANT OR MIDWIFE EPISODE)] = REPLACE(Main.EPISTART,'-','')
      ,[END DATE (CONSULTANT OR MIDWIFE EPISODE)] = REPLACE(Main.EPIEND,'-','')
      ,[COMMISSIONING SERIAL NUMBER] = Main.SERIAL_NO
      ,[NHS SERVICE AGREEMENT LINE NUMBER] = Main.CONTRACT_LINE_NO
      ,[PROVIDER REFERENCE NUMBER] = Main.PROVIDER_REFERENCE_NUMBER
      ,[COMMISSIONER REFERENCE NUMBER] = Main.PURCH_REF
      ,[ORGANISATION CODE (CODE OF PROVIDER)] = Main.PROVIDER
      ,[ORGANISATION CODE (CODE OF COMMISSIONER)] = Main.PURCHASER
      ,[CONSULTANT CODE] = Main.CONS_CODE      
      ,[SPECIALTY FUNCTION CODE] = CASE
                                     WHEN Main.CONS_MAINSPEF = '501' THEN '501'
                                     ELSE Main.MAINSPEF
                                   END
      ,[CONSULTANT SPECIALTY FUNCTION CODE] = Main.CONS_MAINSPEF
      ,[SUB-SPECIALTY CODE] = Main.CONS_MAINSPEF
      ,[PRIMARY DIAGNOSIS (ICD)] = Diag1.DIAG_CODE_ICD
      ,[1st SECONDARY DIAGNOSIS (ICD)] = Diag2.DIAG_CODE_ICD
      ,[2nd SECONDARY DIAGNOSIS (ICD)] = Diag3.DIAG_CODE_ICD
      ,[3rd SECONDARY DIAGNOSIS (ICD)] = Diag4.DIAG_CODE_ICD
      ,[4th SECONDARY DIAGNOSIS (ICD)] = Diag5.DIAG_CODE_ICD
      ,[5th SECONDARY DIAGNOSIS (ICD)] = Diag6.DIAG_CODE_ICD
      ,[6th SECONDARY DIAGNOSIS (ICD)] = Diag7.DIAG_CODE_ICD
      ,[7th SECONDARY DIAGNOSIS (ICD)] = Diag8.DIAG_CODE_ICD
      ,[8th SECONDARY DIAGNOSIS (ICD)] = Diag9.DIAG_CODE_ICD
      ,[9th SECONDARY DIAGNOSIS (ICD)] = Diag10.DIAG_CODE_ICD
      ,[10th SECONDARY DIAGNOSIS (ICD)] = Diag11.DIAG_CODE_ICD
      ,[11th SECONDARY DIAGNOSIS (ICD)] = Diag12.DIAG_CODE_ICD
      ,[12th SECONDARY DIAGNOSIS (ICD)] = Diag13.DIAG_CODE_ICD
      ,[13th SECONDARY DIAGNOSIS (ICD)] = NULL
      ,[14th SECONDARY DIAGNOSIS (ICD)] = NULL
      ,[PRIMARY PROCEDURE (OPCS)] = Proc1.[PROC]
      ,[PROCEDURE DATE] = REPLACE(Proc1.PROC_DATE,'-','')
      ,[2nd PROCEDURE (OPCS)] = Proc2.[PROC]
      ,[2nd PROCEDURE DATE] = REPLACE(Proc2.PROC_DATE,'-','')
      ,[3rd PROCEDURE (OPCS)] = Proc3.[PROC]
      ,[3rd PROCEDURE DATE] = REPLACE(Proc3.PROC_DATE,'-','')      
      ,[4th PROCEDURE (OPCS)] = Proc4.[PROC]
      ,[4th PROCEDURE DATE] = REPLACE(Proc4.PROC_DATE,'-','')      
      ,[5th PROCEDURE (OPCS)] = Proc5.[PROC]
      ,[5th PROCEDURE DATE] = REPLACE(Proc5.PROC_DATE,'-','')
      ,[6th PROCEDURE (OPCS)] = Proc6.[PROC]
      ,[6th PROCEDURE DATE] = REPLACE(Proc6.PROC_DATE,'-','')      
      ,[7th PROCEDURE (OPCS)] = Proc7.[PROC]
      ,[7th PROCEDURE DATE] = REPLACE(Proc7.PROC_DATE,'-','')      
      ,[8th PROCEDURE (OPCS)] = Proc8.[PROC]
      ,[8th PROCEDURE DATE] = REPLACE(Proc8.PROC_DATE,'-','')      
      ,[9th PROCEDURE (OPCS)] = Proc9.[PROC]
      ,[9th PROCEDURE DATE] = REPLACE(Proc9.PROC_DATE,'-','')
      ,[10th PROCEDURE (OPCS)] = Proc10.[PROC]
      ,[10th PROCEDURE DATE] = REPLACE(Proc10.PROC_DATE,'-','')      
      ,[11th PROCEDURE (OPCS)] = Proc11.[PROC]
      ,[11th PROCEDURE DATE] = REPLACE(Proc11.PROC_DATE,'-','')      
      ,[12th PROCEDURE (OPCS)] = Proc12.[PROC]
      ,[12th PROCEDURE DATE] = REPLACE(Proc12.PROC_DATE,'-','')
      ,[13th PROCEDURE (OPCS)] = ''
      ,[13th PROCEDURE DATE] = ''      
      ,[14th PROCEDURE (OPCS)] = ''
      ,[14th PROCEDURE DATE] = ''
      ,[15th PROCEDURE (OPCS)] = ''
      ,[15th PROCEDURE DATE] = ''    
      ,[HEALTHCARE RESOURCE GROUP CODE] = '' -- No longer a data item in CDS v6-2.
      ,[HEALTHCARE RESOURCE GROUP CODE-VERSION NUMBER]= '' -- No longer a data item in CDS v6-2.
FROM (
      SELECT DataLookup.CDS_ID
            ,MAX(DataLookup.SUS_SENT_DATE) LATEST_SUS_SENT_DATE
      FROM CDS.APCArchiveMain DataLookup
      WHERE CONVERT(DATETIME,DataLookup.ACTIVITY_DATE,101) BETWEEN @ReportPeriodStart AND @ReportPeriodEnd
      GROUP BY CDS_ID
     ) DataToExtract
  INNER JOIN CDS.APCArchiveMain Main
    ON (DataToExtract.CDS_ID = Main.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Main.SUS_SENT_DATE)
  LEFT JOIN CDS.APCArchiveDiagnosis Diag1
    ON (DataToExtract.CDS_ID = Diag1.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Diag1.SUS_SENT_DATE
        AND Diag1.SEQUENCE_NUMBER = '1')
  LEFT JOIN CDS.APCArchiveDiagnosis Diag2
    ON (DataToExtract.CDS_ID = Diag2.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Diag2.SUS_SENT_DATE
        AND Diag2.SEQUENCE_NUMBER = '2')
  LEFT JOIN CDS.APCArchiveDiagnosis Diag3
    ON (DataToExtract.CDS_ID = Diag3.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Diag3.SUS_SENT_DATE
        AND Diag3.SEQUENCE_NUMBER = '3')   
  LEFT JOIN CDS.APCArchiveDiagnosis Diag4
    ON (DataToExtract.CDS_ID = Diag4.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Diag4.SUS_SENT_DATE
        AND Diag4.SEQUENCE_NUMBER = '4')             
  LEFT JOIN CDS.APCArchiveDiagnosis Diag5
    ON (DataToExtract.CDS_ID = Diag5.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Diag5.SUS_SENT_DATE
        AND Diag5.SEQUENCE_NUMBER = '5')
  LEFT JOIN CDS.APCArchiveDiagnosis Diag6
    ON (DataToExtract.CDS_ID = Diag6.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Diag6.SUS_SENT_DATE
        AND Diag6.SEQUENCE_NUMBER = '6')
  LEFT JOIN CDS.APCArchiveDiagnosis Diag7
    ON (DataToExtract.CDS_ID = Diag7.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Diag7.SUS_SENT_DATE
        AND Diag7.SEQUENCE_NUMBER = '7')  
  LEFT JOIN CDS.APCArchiveDiagnosis Diag8
    ON (DataToExtract.CDS_ID = Diag8.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Diag8.SUS_SENT_DATE
        AND Diag8.SEQUENCE_NUMBER = '8') 
  LEFT JOIN CDS.APCArchiveDiagnosis Diag9
    ON (DataToExtract.CDS_ID = Diag9.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Diag9.SUS_SENT_DATE
        AND Diag9.SEQUENCE_NUMBER = '9')
  LEFT JOIN CDS.APCArchiveDiagnosis Diag10
    ON (DataToExtract.CDS_ID = Diag10.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Diag10.SUS_SENT_DATE
        AND Diag10.SEQUENCE_NUMBER = '10')
  LEFT JOIN CDS.APCArchiveDiagnosis Diag11
    ON (DataToExtract.CDS_ID = Diag11.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Diag11.SUS_SENT_DATE
        AND Diag11.SEQUENCE_NUMBER = '11')  
  LEFT JOIN CDS.APCArchiveDiagnosis Diag12
    ON (DataToExtract.CDS_ID = Diag12.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Diag12.SUS_SENT_DATE
        AND Diag12.SEQUENCE_NUMBER = '12')   
  LEFT JOIN CDS.APCArchiveDiagnosis Diag13
    ON (DataToExtract.CDS_ID = Diag13.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Diag13.SUS_SENT_DATE
        AND Diag13.SEQUENCE_NUMBER = '13') 
  LEFT JOIN CDS.APCArchiveProcedure Proc1   
    ON (DataToExtract.CDS_ID = Proc1.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc1.SUS_SENT_DATE
        AND Proc1.SEQUENCE_NUMBER = '1')      
  LEFT JOIN CDS.APCArchiveProcedure Proc2   
    ON (DataToExtract.CDS_ID = Proc2.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc2.SUS_SENT_DATE
        AND Proc2.SEQUENCE_NUMBER = '2')                  
  LEFT JOIN CDS.APCArchiveProcedure Proc3   
    ON (DataToExtract.CDS_ID = Proc3.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc3.SUS_SENT_DATE
        AND Proc3.SEQUENCE_NUMBER = '3')         
  LEFT JOIN CDS.APCArchiveProcedure Proc4
    ON (DataToExtract.CDS_ID = Proc4.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc4.SUS_SENT_DATE
        AND Proc4.SEQUENCE_NUMBER = '4')         
  LEFT JOIN CDS.APCArchiveProcedure Proc5   
    ON (DataToExtract.CDS_ID = Proc5.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc5.SUS_SENT_DATE
        AND Proc5.SEQUENCE_NUMBER = '5')         
  LEFT JOIN CDS.APCArchiveProcedure Proc6   
    ON (DataToExtract.CDS_ID = Proc6.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc6.SUS_SENT_DATE
        AND Proc6.SEQUENCE_NUMBER = '6')         
  LEFT JOIN CDS.APCArchiveProcedure Proc7   
    ON (DataToExtract.CDS_ID = Proc7.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc7.SUS_SENT_DATE
        AND Proc7.SEQUENCE_NUMBER = '7')         
  LEFT JOIN CDS.APCArchiveProcedure Proc8   
    ON (DataToExtract.CDS_ID = Proc8.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc8.SUS_SENT_DATE
        AND Proc8.SEQUENCE_NUMBER = '8') 
  LEFT JOIN CDS.APCArchiveProcedure Proc9
    ON (DataToExtract.CDS_ID = Proc9.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc9.SUS_SENT_DATE
        AND Proc9.SEQUENCE_NUMBER = '9') 
  LEFT JOIN CDS.APCArchiveProcedure Proc10   
    ON (DataToExtract.CDS_ID = Proc10.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc10.SUS_SENT_DATE
        AND Proc10.SEQUENCE_NUMBER = '10')         
  LEFT JOIN CDS.APCArchiveProcedure Proc11   
    ON (DataToExtract.CDS_ID = Proc11.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc11.SUS_SENT_DATE
        AND Proc11.SEQUENCE_NUMBER = '11')         
  LEFT JOIN CDS.APCArchiveProcedure Proc12   
    ON (DataToExtract.CDS_ID = Proc12.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc12.SUS_SENT_DATE
        AND Proc12.SEQUENCE_NUMBER = '12')   
WHERE Main.CDS_TYPE in ('120','130','140')
      AND (NOT Main.POSTCODE IS NULL AND NOT Main.DOB IS NULL)             
;









