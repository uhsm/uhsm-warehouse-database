﻿
/***********************************************************************************
 Description: Extract APC Critical Care Period data into staging data table.
 
 Modification History --------------------------------------------------------------

 Date     Author      Change
 26/03/14 Tim Dean	  Created.

************************************************************************************/

CREATE Procedure [CDS].[ExtractAPCCriticalCarePeriod]
AS

SET NOCOUNT ON;

TRUNCATE TABLE CDS.APCStagingCriticalCare;

-- Extract adult critical care data.
INSERT INTO CDS.APCStagingCriticalCare
            (EpisodeSourceUniqueID
            ,SequenceNumber
            ,AdvancedCardiovascularSupportDays
            ,AdvancedRespiratorySupportDays
            ,BasicCardiovascularSupportDays
            ,BasicRespiratorySupportDays
            ,CriticalCareDischargeTime
            ,CriticalCareLocalIdentifier
            ,CriticalCareStartTime
            ,CriticalCareUnitFunctionCode
            ,DermatologicalSupportDays
            ,DischargeDestinationCode
            ,DischargeLocationCode
            ,DischargeReadyTime
            ,DischargeStatusCode
            ,GastrointestinalSupportDays
            ,Level2SupportDays
            ,Level3SupportDays
            ,LiverSupportDays
            ,NeurologicalSupportDays
            ,RenalSupportDays)              
SELECT APCStagingMain.EpisodeSourceUniqueID
      ,CriticalCarePeriod.CCPSequenceNo
      ,CriticalCarePeriod.AdvancedCardiovascularDays
      ,CriticalCarePeriod.AdvancedRespiratorySupportDays
      ,CriticalCarePeriod.BasicCardiovascularSupportDays
      ,CriticalCarePeriod.BasicRespiratorySupportDays
      ,CriticalCarePeriod.EndDate
      ,CriticalCarePeriod.SourceCCPNo
      ,CriticalCarePeriod.StartDate
      ,UnitFunction.MappedCode
      ,CriticalCarePeriod.DermatologicalSupportDays
      ,DischargeDestination.MappedCode
      ,DischargeLocation.MappedCode
      ,CriticalCarePeriod.ReadyDate
      ,DischargeStatus.MappedCode
      ,CriticalCarePeriod.GastrointestinalSupportDays
      ,CriticalCarePeriod.CriticalCareLevel2Days
      ,CriticalCarePeriod.CriticalCareLevel3Days
      ,CriticalCarePeriod.LiverSupportDays
      ,CriticalCarePeriod.NeurologicalSupportDays
      ,CriticalCarePeriod.RenalSupportDays
FROM CDS.APCStagingMain
  
  INNER JOIN CDS.vwAPCCriticalCarePeriod CriticalCarePeriod
    ON APCStagingMain.EpisodeSourceUniqueID = CriticalCarePeriod.SourceEncounterNo
  
  LEFT JOIN PAS.ReferenceValue DischargeDestination
    ON CriticalCarePeriod.DischargeDestination = DischargeDestination.ReferenceValueCode
  
  LEFT JOIN PAS.ReferenceValue DischargeLocation
    ON CriticalCarePeriod.DischargeLocation = DischargeLocation.ReferenceValueCode
  
  LEFT JOIN PAS.ReferenceValue DischargeStatus
    ON CriticalCarePeriod.DischargeStatus = DischargeStatus.ReferenceValueCode
       
  LEFT JOIN PAS.ReferenceValue UnitFunction
    ON CriticalCarePeriod.UnitFunction = UnitFunction.ReferenceValueCode
WHERE  -- Only require adult critical care, so exclude any paediatric.
       UnitFunction.MappedCode NOT IN ('04' -- Paediatric critical care (incl. infants >28 days old on Neonatal ICU).
                                      ,'16' -- Ward for children and young people.
                                      ,'17' -- High Dependency Unit for children and young people.
                                      ,'18' -- Renal Unit for children and young people.
                                      ,'19' -- Burns Unit for children and young people.
                                      ,'92'-- Non standard location using the operating department for children & young people.
                                      ); 
       

