﻿

/***********************************************************************************
 Description: Extract APC Diagnosis data into staging data table.
 
 Modification History --------------------------------------------------------------

 Date     Author      Change
 26/03/14 Tim Dean	  Created.

************************************************************************************/

CREATE Procedure [CDS].[ExtractAPCDiagnosis]
AS

SET NOCOUNT ON;

TRUNCATE TABLE CDS.APCStagingDiagnosis;

-- Extract any episode diagnosis data items.
INSERT INTO CDS.APCStagingDiagnosis
            (EpisodeSourceUniqueID
            ,SequenceNumber
            ,DiagnosisCode
            ,DiagnosisPresentOnAdmissionFlag
            ,SensitiveDiagnosisFlag)
SELECT EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
      ,SequenceNumber = Diagnosis.SORT_ORDER -- - 1
      ,DiagnosisCode = Diagnosis.CODE
      ,DiagnosisPresentOnAdmissionFlag = NULL -- New data item for CDS V6-2, optional so currently not populated.
      ,SensitiveDiagnosisFlag = CASE
                                  WHEN SensitiveDiagnosis.EntityCode IS NOT NULL THEN 'Y'
                                  ELSE 'N'
                                END
FROM   CDS.APCStagingMain
       -- Clinical coding episode diagnosis
       LEFT JOIN PAS.ClinicalCodingBase Diagnosis
              ON ( APCStagingMain.EpisodeSourceUniqueID = Diagnosis.SORCE_REFNO )
       -- Link sensitive diagnosis list.  Used in the CDS to withhold the patient identity.
       LEFT JOIN dbo.EntityLookup SensitiveDiagnosis
              ON ( Diagnosis.CODE = SensitiveDiagnosis.EntityCode
                   AND SensitiveDiagnosis.EntityTypeCode = 'SENSITIVEDIAGNOSIS')              
WHERE  Diagnosis.ARCHV_FLAG = 'N' -- Not deleted.
       AND Diagnosis.DPTYP_CODE = 'DIAGN' -- Diagnosis codes.
       /*There are duplicate diagnosis details coming through from Lorenzo.
         If there are duplicate diagnosis sort orders for the episode we only
         need to include the latest one.*/
       AND NOT EXISTS (SELECT 1
                       FROM   PAS.ClinicalCodingBase DiagnosisDuplicate
                       WHERE  DiagnosisDuplicate.SORCE_REFNO = Diagnosis.SORCE_REFNO
                              AND DiagnosisDuplicate.DGPRO_REFNO > Diagnosis.DGPRO_REFNO
                              AND DiagnosisDuplicate.SORT_ORDER = Diagnosis.SORT_ORDER -- Same sort order.
                              AND DiagnosisDuplicate.ARCHV_FLAG = 'N' -- Not deleted.
                              AND DiagnosisDuplicate.DPTYP_CODE = 'DIAGN'); -- Diagnosis codes.   
                          

