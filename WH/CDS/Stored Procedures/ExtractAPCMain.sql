﻿

/***********************************************************************************
 Description: Extract APC Main dataset into staging data table.
 
 Parameters:  @ReportPeriodStart - Start of the report period.
              @ReportPeriodEnd - End of the report period.
              @ModifiedPeriodStart -  Start of the data modified period for Net Change.
              @ModifiedPeriodEnd - End of the data modified period for Net Change.
              @ProtocolIdentifier - 010 for Net Change or 020 for Bulk Replacement.
              
              Executes the following Stored Procedures:-
              
                1. CDS.ExtractAPCDiagnosis         
                2. CDS.ExtractAPCProcedure
                3. CDS.ExtractAPCBirthOccurrence
                4. CDS.ExtractAPCCriticalCarePeriod
              
 Modification History --------------------------------------------------------------

 Date     Author      Change
 26/03/14 Tim Dean	  Created.
 25/06/14 Tim Dean    Amended the join to dbo.EntityXref ConsultantSpecialtyXref
 10/09/14 Tim Dean    Change source of PbR data to CDS.PbRExtract
 11/09/14 Tim Dean    Added code to set withheld identity for patient choice for specified patients
 02/10/14 Tim Dean	  Change the code to generate main specialty & treatment function
 06/10/14 Tim Dean	  Added PbR change tracking to Net Change.
************************************************************************************/

CREATE Procedure [CDS].[ExtractAPCMain]
 @ReportPeriodStart DATETIME
,@ReportPeriodEnd DATETIME
,@ModifiedPeriodStart DATETIME
,@ModifiedPeriodEnd DATETIME
,@ProtocolIdentifier CHAR(3)
AS

SET NOCOUNT ON;

-- Empty staging tables.
TRUNCATE TABLE CDS.APCStagingMain;

 --Load the initial data into the staging table to determine 
 --which episodes require processing.
INSERT INTO CDS.APCStagingMain
            (
             EpisodeSourceUniqueID
	        ,EpisodeModified
            ,EpisodeDeletedFlag
            ,BirthNotificationID
            ,BirthModified
            ,BirthDeletedFlag 
            ,DeliveryBirthNotificationID     
            ,DeliveryModified
	        ,DeliveryDeletedFlag
	        ,ProtocolIdentifier
	        ,ReportPeriodStart
	        ,ReportPeriodEnd
            )
SELECT EpisodeSourceUniqueID = Encounter.SourceUniqueID
      ,EpisodeModified = ProfessionalCareEpisode.MODIF_DTTM
      ,EpisodeDeletedFlag = ProfessionalCareEpisode.ARCHV_FLAG
      ,BirthNotificationID = Birth.BirthNotificationID
      ,BirthModified = Birth.ModifiedDtm
      ,BirthDeletedFlag = Birth.IsDeleted    
      ,DeliveryBirthNotificationID = Delivery.BirthNotificationID    
      ,DeliveryModified = Delivery.ModifiedDtm
	  ,DeliveryDeletedFlag = Delivery.IsDeleted
	  ,ProtocolIdentifier = @ProtocolIdentifier
	  ,ReportPeriodStart = @ReportPeriodStart
	  ,ReportPeriodEnd = @ReportPeriodEnd
FROM   APC.Encounter
       -- Join the original Lorenzo episode data, so that the archive flag can be checked.
       INNER JOIN Lorenzo.dbo.ProfessionalCareEpisode
               ON ( Encounter.SourceUniqueID = ProfessionalCareEpisode.PRCAE_REFNO )
               
        -- Join the latest Evolution birth notifcation for the birth that matches the episode.       
       LEFT JOIN CDS.MaternityBirthNotification Birth
               ON ( Birth.BirthNotificationID = (SELECT TOP 1 BirthLookup.BirthNotificationID
                                                 FROM   CDS.MaternityBirthNotification BirthLookup
                                                 WHERE  BirthLookup.BirthSourceUniqueID = Encounter.SourceUniqueID
                                                 ORDER  BY BirthLookup.ModifiedDtm DESC) )    

       -- Join the latest Evolution birth notification for the delivery that matches the episode.
       LEFT JOIN CDS.MaternityBirthNotification Delivery
              ON ( Delivery.BirthNotificationID = (SELECT TOP 1 DeliveryLookup.BirthNotificationID
                                                   FROM   CDS.MaternityBirthNotification DeliveryLookup
                                                   WHERE  DeliveryLookup.DeliverySourceUniqueID = Encounter.SourceUniqueID
                                                   ORDER  BY DeliveryLookup.ModifiedDtm DESC) )    
       -- 06/10/2014 TJD ADDED - Join to the PbR change tracking table so that we can include for Net Change.
	   LEFT JOIN CDS.PbRExtract PbR ON (Encounter.SourceSpellNo = PbR.ProviderSpellNo
										   AND PbR.DatasetCode = 'ENCOUNTER'
										   AND PbR.EncounterTypeCode = 'IP')  
		                                                                                                                                                             
WHERE  Encounter.EpisodeEndDate IS NOT NULL
       AND Encounter.EpisodeEndDate BETWEEN @ReportPeriodStart AND @ReportPeriodEnd -- The activity must be within the required reporting period.
       
       -- Determine if deleted activity is to be include in extract.
       AND ( ( @ProtocolIdentifier = '020' -- Bulk replacement excludes deleted activity.
               AND ProfessionalCareEpisode.ARCHV_FLAG = 'N' )
              OR ( @ProtocolIdentifier = '010' -- Net change includes deleted activity.
                   AND ProfessionalCareEpisode.ARCHV_FLAG IN ( 'N', 'Y' ) ) )   
       
       -- For net change check to see if the activity has been modified with the required modified period. 
       -- Need to check both iPM Episodes and Evolution maternity data for changes.                     
       AND ( ( @ProtocolIdentifier = '010'
               AND ( CAST(ProfessionalCareEpisode.MODIF_DTTM AS DATE) BETWEEN CAST(@ModifiedPeriodStart AS DATE) AND CAST(@ModifiedPeriodEnd AS DATE)
                     OR CAST(Delivery.ModifiedDtm AS DATE) BETWEEN CAST(@ModifiedPeriodStart AS DATE) AND CAST(@ModifiedPeriodEnd AS DATE)
                     OR CAST(Birth.ModifiedDtm AS DATE) BETWEEN CAST(@ModifiedPeriodStart AS DATE) AND CAST(@ModifiedPeriodEnd AS DATE)
                     -- 06/10/2014 TJD ADDED - check for PbR changes to include in Net Change.
                     OR CAST(PbR.ModifiedDtm AS DATE) BETWEEN CAST(@ModifiedPeriodStart AS DATE) AND CAST(@ModifiedPeriodEnd AS DATE)
                    ) )                   
               OR @ProtocolIdentifier = '020' ); -- Ignore for bulk replacement.

-- Extract the remaining data items for the episodes identified to be processed.               
UPDATE CDS.APCStagingMain
SET    AdminCategoryCode = AdminCategory.MappedCode
      ,AdmissionDate = Encounter.AdmissionDate
      ,AdmissionMethodCode = AdmissionMethod.MappedCode
      ,AdmissionSourceCode = AdmissionSource.MappedCode
      -- 02/10/14 TJD Removed --
      --,ConsultantMainSpecialtyCode= COALESCE(ConsultantSpecialtyXref.XrefEntityCode,ConsultantSpecialty.NationalSpecialtyCode)
      -- 02/01/14 TJD Added --
      ,ConsultantMainSpecialtyCode = LEFT(CASE
                                            WHEN LEFT(COALESCE(TreatmentFunctionSpecialtyMap.XrefEntityCode
													,EpisodeSpecialty.NationalSpecialtyCode),3) = '500' THEN ConsultantSpecialty.NationalSpecialtyCode

											WHEN LEFT(COALESCE(TreatmentFunctionSpecialtyMap.XrefEntityCode
													,EpisodeSpecialty.NationalSpecialtyCode),3) = '257' THEN '420'
								            ELSE COALESCE(TreatmentFunctionSpecialtyMap.XrefEntityCode,EpisodeSpecialty.NationalSpecialtyCode)
								           END,3)
      --,ConsultantTreatmentFunctionCode = ConsultantSpecialty.SpecialtyFunctionCode
      ,ContractSerialNo = Encounter.ContractSerialNo
      ,DateOfBirth = Encounter.DateOfBirth
      ,DecisionToAdmitDate = Encounter.DecisionToAdmitDate
      ,DecisionToAdmitDateOriginal = Encounter.OriginalDecisionToAdmitDate
      ,DischargeDate = Encounter.DischargeDate
      ,DischargeDestinationCode = DischargeDestination.MappedCode
      ,DischargeMethodCode = DischargeMethod.MappedCode
      ,DistrictNo = Encounter.DistrictNo
      ,ElectiveWaitDuration = Encounter.DurationOfElectiveWait
      ,EpisodeEndDate = Encounter.EpisodeEndTime --Encounter.EpisodeEndDate
      --,EpisodeMainSpecialtyCode = EpisodeSpecialty.MainSpecialtyCode
      ,EpisodeNo = Encounter.EpisodeNo
      ,EpisodeStartDate = Encounter.EpisodeStartDate
      -- 02/10/14 TJD Removed --
      --,EpisodeTreatmentFunctionCode = COALESCE(EpisodeSpecialtyXref.XrefEntityCode,EpisodeSpecialty.NationalSpecialtyCode)
      -- 02/01/14 TJD Added --
      ,EpisodeTreatmentFunctionCode = CASE
                                        WHEN LEFT(COALESCE(SpecialtyTreatmentFunctionMap.XrefEntityCode
                                                  ,EpisodeSpecialty.NationalSpecialtyCode),3) = '904' THEN '650'
                                        ELSE LEFT(COALESCE(SpecialtyTreatmentFunctionMap.XrefEntityCode,EpisodeSpecialty.NationalSpecialtyCode),3)
                                      END
      ,EthnicOriginCode = EthnicOrigin.MappedCode
      ,GenderCode = Gender.MappedCode
      ,GPCodeRegistered = GP.ProfessionalCarerMainCode
      ,HealthAuthorityUsualResidence_ODS = UsualResidence_ODS.DistrictOfResidence
      ,LastEpisodeInSpellIndicator = Encounter.LastEpisodeInSpellIndicator
      ,LegalStatusClassificationOnAdmissionCode = LegalStatusClassification.MappedCode
      ,LocalAuthorityUsualResidence_ODS = UsualResidence_ODS.LocalAuthorityCode
      ,ManagementIntentionCode = ManagementIntention.MappedCode
      ,MaritalStatusCode = MaritalStatus.MappedCode
      ,NationalConsultantCode = Consultant.NationalConsultantCode
      ,NHSNumber = Encounter.NHSNumber
      ,NHSNumberStatusCode = Encounter.NHSNumberStatusCode
      ,OverseasStatusCode = OverseasStatus.MappedCode
      ,PatientAddress1 = Encounter.PatientAddress1
      ,PatientAddress2 = Encounter.PatientAddress2
      ,PatientAddress3 = Encounter.PatientAddress3
      ,PatientAddress4 = Encounter.PatientAddress4
      ,PatientForename = Encounter.PatientForename
      ,PatientSurname = Encounter.PatientSurname
      ,PbRCommissionerCode = PbR.CommissionerCodeCCG
	  ,PbRExcluded = PbR.PbRExcluded
	  ,PbRExclusionType = PbR.ExclusionType
	  ,PbRFinalSLACode = PbR.FinalSLACode
      ,Postcode = Encounter.Postcode
      ,PracticeCodeEpisodic = PracticeEpisodic_IPM.OrganisationLocalCode
      ,PracticeCodeRegistered = PracticeRegistered_IPM.OrganisationLocalCode
      ,PrimaryCareOrganisationEpisodic_ODS = PracticeEpisodic_ODS.ParentOrganisationCode
      ,PrimaryCareOrganisationRegistered_ODS = PracticeRegistered_ODS.ParentOrganisationCode
      ,PrimaryCareOrganisationResidence_ODS = UsualResidence_ODS.CCGCode
      ,ProfessionalCarerType = Consultant.ProfessionalCarerType
      ,ReferredByConsultantCode = Referral.ReferredByConsultantCode
      ,ReferrerMainCode = Referrer.ProfessionalCarerMainCode
      ,ReferrerOrganisationCode = ReferrerOrganisation.OrganisationLocalCode
      ,ReferrerType = Referrer.ProfessionalCarerType
      ,RTTCurrentProviderCode = Encounter.RTTCurrentProviderCode
      ,RTTEndDate = Encounter.RTTEndDate
      ,RTTPathwayID = Encounter.RTTPathwayID
      ,RTTStartDate = Encounter.RTTStartDate
      ,RTTStatusCodeNational = RTTPeriodStatus.NationalRTTStatusCode
      ,SourceOfReferralCode = SourceOfReferral.MappedCode
      ,SourceSpellNo = Encounter.SourceSpellNo
FROM   CDS.APCStagingMain
       LEFT JOIN APC.Encounter
              ON ( APCStagingMain.EpisodeSourceUniqueID = Encounter.SourceUniqueID )
      
       LEFT JOIN PAS.ReferenceValue OverseasStatus
              ON ( Encounter.OverseasStatusFlag = OverseasStatus.ReferenceValueCode )
      
       LEFT JOIN PAS.ReferenceValue Gender
              ON ( Encounter.SexCode = Gender.ReferenceValueCode ) 
      
       LEFT JOIN PAS.ReferenceValue EthnicOrigin
              ON ( Encounter.EthnicOriginCode = EthnicOrigin.ReferenceValueCode )                           
      
       LEFT JOIN PAS.ReferenceValue MaritalStatus
              ON ( Encounter.MaritalStatusCode = MaritalStatus.ReferenceValueCode )
      
       LEFT JOIN PAS.ReferenceValue LegalStatusClassification
              ON ( Encounter.LegalStatusClassificationOnAdmissionCode = LegalStatusClassification.ReferenceValueCode )
      
       LEFT JOIN PAS.ReferenceValue AdminCategory
              ON ( Encounter.AdminCategoryCode = AdminCategory.ReferenceValueCode )
      
       LEFT JOIN PAS.ReferenceValue AdmissionMethod
              ON ( Encounter.AdmissionMethodCode = AdmissionMethod.ReferenceValueCode )
      
       LEFT JOIN PAS.ReferenceValue ManagementIntention
              ON ( Encounter.ManagementIntentionCode = ManagementIntention.ReferenceValueCode )
      
       LEFT JOIN PAS.ReferenceValue AdmissionSource
              ON ( Encounter.AdmissionSourceCode = AdmissionSource.ReferenceValueCode )
      
       LEFT JOIN PAS.ReferenceValue DischargeMethod
              ON ( Encounter.DischargeMethodCode = DischargeMethod.ReferenceValueCode )
      
       LEFT JOIN PAS.ReferenceValue DischargeDestination
              ON ( Encounter.DischargeDestinationCode = DischargeDestination.ReferenceValueCode )
      
       LEFT JOIN PAS.Consultant
              ON ( Encounter.ConsultantCode = Consultant.ConsultantCode )   

       LEFT JOIN PAS.Specialty ConsultantSpecialty
              ON (Consultant.MainSpecialtyCode = ConsultantSpecialty.SpecialtyCode)
          
       -- 02/10/14 TJD Removed --        
       --LEFT JOIN dbo.EntityXref ConsultantSpecialtyXref
       --       -- ON (ConsultantSpecialty.NationalSpecialtyCode = ConsultantSpecialtyXref.EntityCode 25/06/2014 TJD Removed
       --       ON (LEFT(ConsultantSpecialty.NationalSpecialtyCode,3) = ConsultantSpecialtyXref.EntityCode -- 25/06/2014 TJD Added              
       --           AND ConsultantSpecialtyXref.EntityTypeCode = 'TREATMENTFUNCTIONCODE')
       
       LEFT JOIN PAS.Specialty EpisodeSpecialty
              ON (Encounter.SpecialtyCode = EpisodeSpecialty.SpecialtyCode)
       
       -- 02/10/14 TJD Removed --       
       --LEFT JOIN dbo.EntityXref EpisodeSpecialtyXref
       --       ON (EpisodeSpecialty.NationalSpecialtyCode = EpisodeSpecialtyXref.EntityCode
       --           AND EpisodeSpecialtyXref.EntityTypeCode = 'SPECIALTYCODE')
       
       -- 02/10/14 TJD Added --
       LEFT JOIN dbo.EntityXref TreatmentFunctionSpecialtyMap ON (TreatmentFunctionSpecialtyMap.EntityTypeCode = 'TREATMENTFUNCTIONCODE'
														   AND TreatmentFunctionSpecialtyMap.XrefEntityTypeCode = 'SPECIALTYCODE'
														   AND TreatmentFunctionSpecialtyMap.EntityCode = COALESCE(LEFT(EpisodeSpecialty.NationalSpecialtyCode, 3)
                                                                                                               ,ConsultantSpecialty.NationalSpecialtyCode))
	   
	   -- 02/10/14 TJD Added --
	   LEFT JOIN dbo.EntityXref SpecialtyTreatmentFunctionMap ON (SpecialtyTreatmentFunctionMap.EntityTypeCode = 'SPECIALTYCODE'
															  AND SpecialtyTreatmentFunctionMap.XrefEntityTypeCode like 'TREATMENTFUNCTIONCODE%'
															  AND SpecialtyTreatmentFunctionMap.EntityCode = COALESCE(LEFT(EpisodeSpecialty.NationalSpecialtyCode, 3)
																													  ,ConsultantSpecialty.NationalSpecialtyCode))
       
       LEFT JOIN OrganisationCCG.dbo.Postcode UsualResidence_ODS
              ON ( UsualResidence_ODS.Postcode = CASE
                                                   WHEN Len(Encounter.Postcode) = 8 THEN Encounter.Postcode
                                                   ELSE LEFT(Encounter.Postcode, 3) + ' '
                                                        + RIGHT(Encounter.Postcode, 4)
                                                 END )
                                                 
       LEFT JOIN PAS.Organisation PracticeEpisodic_IPM
              ON ( Encounter.EpisodicGpPracticeCode = PracticeEpisodic_IPM.OrganisationCode ) 
              
       LEFT JOIN PAS.Organisation PracticeRegistered_IPM
              ON ( Encounter.RegisteredGpPracticeCode = PracticeRegistered_IPM.OrganisationCode ) 
              
       LEFT JOIN OrganisationCCG.dbo.Practice PracticeEpisodic_ODS
              ON ( PracticeEpisodic_IPM.OrganisationLocalCode = PracticeEpisodic_ODS.OrganisationCode )  
              
       LEFT JOIN OrganisationCCG.dbo.Practice PracticeRegistered_ODS
              ON ( PracticeRegistered_IPM.OrganisationLocalCode = PracticeRegistered_ODS.OrganisationCode ) 
              
       LEFT JOIN RF.Encounter Referral
              ON ( Encounter.ReferralSourceUniqueID = Referral.SourceUniqueID) 
              
       LEFT JOIN PAS.ReferenceValue SourceOfReferral
              ON ( Referral.SourceOfReferralCode = SourceOfReferral.ReferenceValueCode
                   AND SourceOfReferral.ReferenceDomainCode = 'SORRF')
                   
       LEFT JOIN PAS.ProfessionalCarer Referrer
              ON (Referral.ReferredByConsultantCode = Referrer.ProfessionalCarerCode) 
              
       LEFT JOIN PAS.Organisation ReferrerOrganisation
              ON (Referral.ReferredByOrganisationCode  = ReferrerOrganisation.OrganisationCode ) 
              
       LEFT JOIN PAS.RTTStatus RTTPeriodStatus
              ON (Encounter.RTTPeriodStatusCode = RTTPeriodStatus.RTTStatusCode ) 
              
       LEFT JOIN PAS.ProfessionalCarer GP 
              ON (Encounter.RegisteredGpCode = GP.ProfessionalCarerCode)             
       
       -- 10/09/2014 TJD Removed existing join       
       --LEFT JOIN PbR2014.dbo.PbR 
       --       ON Encounter.SourceSpellNo = PbR.ProviderSpellNo
       --          AND PbR.DatasetCode = 'ENCOUNTER'  
       --          AND PbR.EncounterTypeCode = 'IP')
       
       -- 10/09/2014 TJD Added new join      
       LEFT JOIN CDS.PbRExtract PbR
            ON (Encounter.SourceSpellNo = PbR.ProviderSpellNo
                AND PbR.DatasetCode = 'ENCOUNTER'
                AND PbR.EncounterTypeCode = 'IP')                                                                                                                                                                                            
;  

-- Update the cross border flag.

-- See: Who pays? Determining responsibility for payments to providers, NHS England August 2013.
-- http://www.england.nhs.uk/wp-content/uploads/2013/08/who-pays-aug13.pdf
UPDATE CDS.APCStagingMain 
SET CrossBorderFlag = CASE
                        -- England and Wales border protocol - Resident in English border and registered to Welsh GP Practice.
                        WHEN PrimaryCareOrganisationResidence_ODS IN ( '01R', -- NHS South Cheshire CCG
                                                                       '02F', -- NHS West Cheshire CCG
                                                                       '12F', -- NHS Wirral CCG
                                                                       '05F', -- NHS Herefordshire CCG
                                                                       '05N', -- NHS Shropshire CCG
                                                                       '05X', -- NHS Telford and Wrekin CCG
                                                                       '11M', -- NHS Gloucestershire CCG
                                                                       '12A' ) -- NHS South Gloucestershire CCG
                              AND LEFT(PracticeCodeEpisodic,1) = 'W' THEN 1 -- English cross border.
                        -- England and Wales border protocol - Resident in Welsh border and registered to non-Welsh GP Practice.
                        WHEN LocalAuthorityUsualResidence_ODS IN ( '00NJ', -- Flintshire 
                                                                   '00NL', -- Wrexham
                                                                   '00NG', -- Denbighshire
                                                                   '00NN', -- Powys
                                                                   '00PP' ) -- Monmouthshire
                          AND LEFT(PracticeCodeEpisodic,1) <> 'W' THEN 2 -- Welsh cross border.
                        ELSE 0
                      END                                  
FROM   CDS.APCStagingMain;  

EXEC CDS.ExtractAPCDiagnosis;         
EXEC CDS.ExtractAPCProcedure;
  
-- Update the withheld identity flag
-- This is a new field in CDS v6-2.
-- Checks to see if the episode has any sensitive diagnosis or procedure codes.                           
UPDATE CDS.APCStagingMain 
SET    WithheldIdentityReasonCode = CASE
                                      WHEN APCStagingDiagnosis.DiagnosisCode IS NOT NULL 
                                           OR APCStagingProcedure.ProcedureCode IS NOT NULL THEN '97' -- Record anonymised for other reason. 
                                      WHEN DistrictNo = 'RM21826299' THEN '03'     -- 11/09/2014 TJD Added - Record anonymised at request of PATIENT                      
                                    END
FROM   CDS.APCStagingMain
       LEFT JOIN CDS.APCStagingDiagnosis
              ON( APCStagingMain.EpisodeSourceUniqueID = APCStagingDiagnosis.EpisodeSourceUniqueID
                  AND APCStagingDiagnosis.SensitiveDiagnosisFlag = 'Y' )
       
       LEFT JOIN CDS.APCStagingProcedure
              ON( APCStagingMain.EpisodeSourceUniqueID = APCStagingProcedure.EpisodeSourceUniqueID
                  AND APCStagingProcedure.SensitiveProcedureFlag = 'Y' );

EXEC CDS.ExtractAPCBirthOccurrence;
EXEC CDS.ExtractAPCCriticalCarePeriod;













