﻿


/***********************************************************************************
 Description: Extract APC NWCIS dataset for required period.
 
 Parameters:  @ReportPeriodStart - Start of the reporting period.
              @ReportPeriodEnd - End of the reporting period.
              
              Executed by CDS APC NWCIS Export SSIS Package
 Modification History --------------------------------------------------------------

 Date     Author      Change
 29/04/14 Tim Dean	  Created.
 19/05/14 Tim Dean    Correct join for Procedure 12 details.

************************************************************************************/

CREATE Procedure [CDS].[ExtractAPCNWCIS]
 @ReportPeriodStart DATETIME
,@ReportPeriodEnd DATETIME
AS

SELECT NHS_No = LTRIM(RTRIM(Main.NHS_NO))
      ,Local_Patient_Identifier = LTRIM(RTRIM(Main.LOCPATID))
      ,Organisation_Code_Provider = 'RM200'
      ,Surname = Encounter.PatientSurname
      ,Previous_Surname = LTRIM(RTRIM(PrevName.ALIAS_NAME))
      ,Forename_s = Encounter.PatientForename
      ,[Address] = CASE 
                     WHEN LEN(COALESCE(Encounter.PatientAddress1,'')) = 0 THEN
                          LTRIM(RTRIM(COALESCE(Encounter.PatientAddress2,''))) + ', ' + LTRIM(RTRIM(COALESCE(Encounter.PatientAddress3,''))) + ', ' + LTRIM(RTRIM(COALESCE(Encounter.PatientAddress4,'')))
                     ELSE
                          LTRIM(RTRIM(COALESCE(Encounter.PatientAddress1,''))) + ', ' + LTRIM(RTRIM(COALESCE(Encounter.PatientAddress2,''))) + ', ' + LTRIM(RTRIM(COALESCE(Encounter.PatientAddress3,''))) + ', ' + LTRIM(RTRIM(COALESCE(Encounter.PatientAddress4,'')))
                   END
      ,Postcode = LTRIM(RTRIM(Main.POSTCODE))
      ,Sex = LTRIM(RTRIM(Main.SEX))
      ,Date_of_Birth = Main.DOB
      ,Org_Code_Reg_Ref_GMP = LTRIM(RTRIM(Main.GPREG))      
      ,Code_GP_Practice = LTRIM(RTRIM(Main.PRACREG))
      ,Ethnicity = CASE Main.ETHNICOR
                     WHEN '99' THEN 'Z'
                     ELSE Main.ETHNICOR
                   END
      ,Parent_Org_Code_PCT = LTRIM(RTRIM(Main.PURCHASER))
      ,Diagnosis_Date = Main.EPIEND
      ,Primary_Diag_ICD10 = LTRIM(RTRIM(Diag1.DIAG_CODE_ICD))
      ,Subsidiary_Diag_ICD10 = LTRIM(RTRIM(Diag2.DIAG_CODE_ICD))
      ,[1st_Secondary_Diag_ICD10] = LTRIM(RTRIM(Diag3.DIAG_CODE_ICD))
      ,Primary_Proc_OPCS = LTRIM(RTRIM(Proc1.[PROC]))
      ,Proc_OPCS_sub_proc = ''
      ,[2nd_Proc] = LTRIM(RTRIM(Proc2.[PROC]))
      ,[3rd_Proc] = LTRIM(RTRIM(Proc3.[PROC]))
      ,[4th_Proc] = LTRIM(RTRIM(Proc4.[PROC]))
      ,[5th_Proc] = LTRIM(RTRIM(Proc5.[PROC]))
      ,[6th_Proc] = LTRIM(RTRIM(Proc6.[PROC]))
      ,[7th_Proc] = LTRIM(RTRIM(Proc7.[PROC]))
      ,[8th_Proc] = LTRIM(RTRIM(Proc8.[PROC]))
      ,[9th_Proc] = LTRIM(RTRIM(Proc9.[PROC]))
      ,[10th_Proc] = LTRIM(RTRIM(Proc10.[PROC]))
      ,[11th_Proc] = LTRIM(RTRIM(Proc11.[PROC]))
      ,[12th_Proc] = LTRIM(RTRIM(Proc12.[PROC]))
      ,Consultant_Code = LTRIM(RTRIM(Main.CONS_CODE))
      ,Episode_Start = Main.EPISTART
      ,Episode_End = Main.EPIEND
      ,Pt_Date_Of_Death = LEFT(CONVERT(VARCHAR,Encounter.DateOfDeath , 120), 10)
      ,Patient_Type = CASE Main.CLASSPAT
                        WHEN 2 THEN 'DayCase'
                        ELSE 'Inpatient'
                      END
      ,Admission_Method = Main.ADMIMETH
      ,Admission_Date = Main.ADMIDATE
      ,Treatment_Date = Proc1.PROC_DATE
      ,Spell_Number = ''
      ,Discharge_Date = Main.DISDATE
      ,Discharge_Destination = Main.DISDEST
FROM (
      SELECT DataLookup.CDS_ID
            ,MAX(DataLookup.SUS_SENT_DATE) LATEST_SUS_SENT_DATE
      FROM CDS.APCArchiveMain DataLookup
      WHERE CONVERT(DATETIME,DataLookup.EPIEND,101) BETWEEN @ReportPeriodStart AND @ReportPeriodEnd
      GROUP BY CDS_ID
     ) DataToExtract
  INNER JOIN CDS.APCArchiveMain Main
    ON (DataToExtract.CDS_ID = Main.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Main.SUS_SENT_DATE)
  
  LEFT JOIN APC.Encounter 
    ON (Main.EpisodeSourceUniqueID = Encounter.SourceUniqueID)
  
  LEFT JOIN Lorenzo.DBO.vwPreviousName PrevName 
    ON (Encounter.SourcePatientNo = PrevName.patnt_refno 
        AND Encounter.PatientSurname <> PrevName.ALIAS_NAME)
  
  LEFT JOIN CDS.APCArchiveDiagnosis Diag1
    ON (DataToExtract.CDS_ID = Diag1.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Diag1.SUS_SENT_DATE
        AND Diag1.SEQUENCE_NUMBER = '1')
  
  LEFT JOIN CDS.APCArchiveDiagnosis Diag2
    ON (DataToExtract.CDS_ID = Diag2.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Diag2.SUS_SENT_DATE
        AND Diag2.SEQUENCE_NUMBER = '2')
  
  LEFT JOIN CDS.APCArchiveDiagnosis Diag3
    ON (DataToExtract.CDS_ID = Diag3.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Diag3.SUS_SENT_DATE
        AND Diag3.SEQUENCE_NUMBER = '3')   
  
  LEFT JOIN CDS.APCArchiveProcedure Proc1   
    ON (DataToExtract.CDS_ID = Proc1.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc1.SUS_SENT_DATE
        AND Proc1.SEQUENCE_NUMBER = '1')      
  
  LEFT JOIN CDS.APCArchiveProcedure Proc2   
    ON (DataToExtract.CDS_ID = Proc2.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc2.SUS_SENT_DATE
        AND Proc2.SEQUENCE_NUMBER = '2')                  
  
  LEFT JOIN CDS.APCArchiveProcedure Proc3   
    ON (DataToExtract.CDS_ID = Proc3.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc3.SUS_SENT_DATE
        AND Proc3.SEQUENCE_NUMBER = '3')         
  
  LEFT JOIN CDS.APCArchiveProcedure Proc4
    ON (DataToExtract.CDS_ID = Proc4.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc4.SUS_SENT_DATE
        AND Proc4.SEQUENCE_NUMBER = '4')         
  
  LEFT JOIN CDS.APCArchiveProcedure Proc5   
    ON (DataToExtract.CDS_ID = Proc5.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc5.SUS_SENT_DATE
        AND Proc5.SEQUENCE_NUMBER = '5')         
  
  LEFT JOIN CDS.APCArchiveProcedure Proc6   
    ON (DataToExtract.CDS_ID = Proc6.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc6.SUS_SENT_DATE
        AND Proc6.SEQUENCE_NUMBER = '6')         
  
  LEFT JOIN CDS.APCArchiveProcedure Proc7   
    ON (DataToExtract.CDS_ID = Proc7.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc7.SUS_SENT_DATE
        AND Proc7.SEQUENCE_NUMBER = '7')         
  
  LEFT JOIN CDS.APCArchiveProcedure Proc8   
    ON (DataToExtract.CDS_ID = Proc8.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc8.SUS_SENT_DATE
        AND Proc8.SEQUENCE_NUMBER = '8') 
  
  LEFT JOIN CDS.APCArchiveProcedure Proc9
    ON (DataToExtract.CDS_ID = Proc9.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc9.SUS_SENT_DATE
        AND Proc9.SEQUENCE_NUMBER = '9') 
  
  LEFT JOIN CDS.APCArchiveProcedure Proc10   
    ON (DataToExtract.CDS_ID = Proc10.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc10.SUS_SENT_DATE
        AND Proc10.SEQUENCE_NUMBER = '10')         
  
  LEFT JOIN CDS.APCArchiveProcedure Proc11   
    ON (DataToExtract.CDS_ID = Proc11.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc11.SUS_SENT_DATE
        AND Proc11.SEQUENCE_NUMBER = '11')         
  
  LEFT JOIN CDS.APCArchiveProcedure Proc12   
    ON (DataToExtract.CDS_ID = Proc12.CDS_ID
        AND DataToExtract.LATEST_SUS_SENT_DATE = Proc12.SUS_SENT_DATE
        AND Proc12.SEQUENCE_NUMBER = '12')                   
WHERE (Diag1.DIAG_CODE_ICD IN ('Z530','Z531','Z532','Z538','Z539','D352','D354')
        OR (LEFT(Diag1.DIAG_CODE_ICD,3) >= 'C00'
             AND LEFT(Diag1.DIAG_CODE_ICD,3) <= 'C97')
        OR (LEFT(Diag1.DIAG_CODE_ICD,3) >= 'D37'
             AND LEFT(Diag1.DIAG_CODE_ICD,3) <= 'D48')
        OR LEFT(Diag1.DIAG_CODE_ICD,3) IN ('D32','D33'))
      AND NOT Main.CDS_TYPE IN ('180','190','200');



