﻿
/***********************************************************************************
 Description: Extract APC Procedure data into staging data table.
 
 Modification History --------------------------------------------------------------

 Date     Author      Change
 26/03/14 Tim Dean	  Created.

************************************************************************************/

CREATE Procedure [CDS].[ExtractAPCProcedure]
AS

SET NOCOUNT ON;

TRUNCATE TABLE CDS.APCStagingProcedure;

INSERT INTO CDS.APCStagingProcedure
            (EpisodeSourceUniqueID
             ,SequenceNumber
             ,ProcedureCode
             ,ProcedureDate
             ,RegistrationIssuerCode_HP
             ,RegistrationCode_HP
             ,RegistrationIssuerCode_RA
             ,RegistrationCode_RA
             ,SensitiveProcedureFlag)
SELECT EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
      ,SequenceNumber = Operation.SORT_ORDER
      ,ProcedureCode = Operation.CODE
      ,ProcedureDate = Operation.DGPRO_DTTM
      ,RegistrationIssuerCode_HP = NULL -- New data item for CDS V6-2, optional so currently not populated.
      ,RegistrationCode_HP = NULL -- New data item for CDS V6-2, optional so currently not populated.
      ,RegistrationIssuerCode_RA = NULL -- New data item for CDS V6-2, optional so currently not populated.
      ,RegistrationCode_RA = NULL -- New data item for CDS V6-2, optional so currently not populated.
      ,SensitiveProcedureFlag = CASE
                                  WHEN SensitiveProcedure.EntityCode IS NOT NULL THEN 'Y'
                                  ELSE 'N'
                                END
FROM CDS.APCStagingMain
  -- Clinical coding episode procedure
  LEFT JOIN PAS.ClinicalCodingBase Operation
    ON APCStagingMain.EpisodeSourceUniqueID = Operation.SORCE_REFNO
     
  -- Link sensitive diagnosis list.  Used in the CDS to withhold the patient identity.
  LEFT JOIN dbo.EntityLookup SensitiveProcedure
    ON Operation.CODE = SensitiveProcedure.EntityCode
        AND SensitiveProcedure.EntityTypeCode = 'SENSITIVEOPERATION'              
WHERE Operation.ARCHV_FLAG = 'N'
      AND Operation.DPTYP_CODE = 'PROCE'
      AND Operation.SORCE_CODE = 'PRCAE'
      /*There are duplicate procerdure details coming through from Lorenzo.
        If there are duplicate procedure sort orders for the episode we only
        need to include the latest one.*/
      AND NOT EXISTS (SELECT 1
                      FROM   PAS.ClinicalCodingBase OperationDuplicate
                      WHERE  OperationDuplicate.SORCE_REFNO = Operation.SORCE_REFNO
                              AND OperationDuplicate.DGPRO_REFNO > Operation.DGPRO_REFNO
                              AND OperationDuplicate.SORT_ORDER = Operation.SORT_ORDER -- Same sort order.
                              AND OperationDuplicate.ARCHV_FLAG = 'N' -- Not deleted.
                              AND OperationDuplicate.DPTYP_CODE = 'PROCE'
                              AND OperationDuplicate.SORCE_CODE = 'PRCAE'); -- Diagnosis codes.    

