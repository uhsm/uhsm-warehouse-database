﻿





/******************************************************************************
 Description: Called by SSIS Package to extract the Evolution maternity
              birth notifications to be used in the CDS.
 
 Parameters:  @StartDate - Start date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author      Change
 12/03/14 Tim Dean	  Created

******************************************************************************/

CREATE Procedure [CDS].[ExtractMaternityBirthNotification]
 @StartDate as Datetime
as

SELECT BirthNotification.PatientPointer
      ,BirthNotification.BirthOrder
      ,BirthNotification.BirthDtm
      ,BirthNotification.BirthSourceUniqueID
      ,BirthNotification.DeliverySourceUniqueID
      ,RowHash = CONVERT(VARCHAR(32), Hashbytes('MD5',BirthNotification.RowDataConcat),2)
FROM CDS.vExtractMaternityBirthNotification BirthNotification
WHERE BirthNotification.BirthDtm >= @StartDate
--	Sort required by SSIS package.                 
ORDER  BY BirthNotification.PatientPointer ASC
          ,BirthNotification.BirthOrder ASC
          ,BirthNotification.BirthDtm ASC ;
          
 


