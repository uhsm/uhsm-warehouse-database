﻿CREATE procedure CDS.IterativeSQL
	 @sqlTemplate varchar(max)
	,@iterations int
	,@debug bit = 0
as

declare @sql varchar(max)
declare @iteration int = 0


while @iteration < @iterations
begin

	set @iteration = @iteration + 1

	set @sql =
		REPLACE(
			@sqlTemplate
			,'<iteration>'
			,@iteration
		)

	if @debug = 'true'
		print @sql
	else
		exec (@sql)

end

