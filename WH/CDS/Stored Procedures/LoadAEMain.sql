﻿
CREATE Procedure [CDS].[LoadAEMain]  @ReportingPeriodStart datetime, @ReportingPeriodEnd datetime
as


--Declare @ReportingPeriodStart datetime = '20160401',
--@ReportingPeriodEnd datetime = '20160430'


/**********************************************
CDS AE Main Table 
**********************************************/
Truncate table   CDS.AEMain--FromDW
Insert into  CDS.AEMain--FromDW

SELECT 	Distinct --Encounter.SourceUniqueID
		--, Encounter.LocalPatientID
		--, PbR.CommissionerCodeCCG
		--, PbR.FinalSLACode-- for Scottish, Welsh and Northern Ireland - patients visiting A&E - payment is covered by the host CCG - page  8 of Who pays document
							--overseas patients where GP practice and residence is not uk - need to send to host - see page 40 and 41 of who pays document 
							-- for no fixed abode then under usually resident test the host CCG is the commissioner - see page 9 and 43 (Anned B point 7) of the Who pays document - where residence is to be determined as the place where they are present - i.e. CCG of host provider?
		--, Encounter.ResidenceCCGCode				
							
					
		 PRIME_RECIPIENT = 
		 
				CASE WHEN Encounter.ResidenceCCGCode   IS NULL  THEN '01N'
						WHEN Encounter.ResidenceCCGCode  = 'X98' THEN PbR.FinalSLACode--CDS submissions do not allow for the 'X98' in the PRIME RECIPIENT field. Most of these tend to be overseas patients - for emergency care these costs are covered by host. Therefore,  where the PRIME RECIPIENT is coming through as 'X98' the PbR Commissioning CCG code is used to get the data submitted.
						ELSE Encounter.ResidenceCCGCode
						END-- Prime Recipient should be the Organisation Code of Residence Resposibility
		, COPY_RECIPIENT_1 = CASE WHEN PbR.FinalSLACode <> CASE WHEN Encounter.ResidenceCCGCode   IS NULL  THEN '01N'
				ELSE Encounter.ResidenceCCGCode
				END THEN PbR.FinalSLACode ELSE '' END  -- Cope recipient should be the organisation code of commissioner	
		, COPY_RECIPIENT_2 = ''
		, COPY_RECIPIENT_3 = ''
		, COPY_RECIPIENT_4 = '' 
		, COPY_RECIPIENT_5 = ''
		, SENDER =  'RM200' 
		, CDS_GROUP = '140'
		, CDS_TYPE = '010'
		, CDS_IDENTIFIER = 	'BRM200' + Right('00000000000000000000000000000000000000' + 
					Cast(1101793 +  CAST(SUBSTRING(Encounter.DistrictNo,1,CHARINDEX('G', Encounter.DistrictNo)-1) as INT) as varchar) + 'C' + AttendanceNumber
					+ Right('000000000' + cast(Datediff(DAY,'19941117',Encounter.RegisteredTime)	 as varchar),8)	,29)	
		, UPDATE_TYPE = '9'
		, PROTOCOL_IDENTIFIER =  '020'
		, BULKSTART = CAST( @ReportingPeriodStart as DATE) --CDS REPORT PERIOD START DATE - defines the start date for the CDS Bulk Replacement Update time period. 
		, BULKEND = CAST(@ReportingPeriodEnd as DATE)-- CDS REPORT PERIOD END DATE - defines the end date for the CDS Bulk Replacement Update time period.
		, DATETIME_CREATED =  CONVERT(varchar(10), GetDate(),126) + ' ' + 
		  RIGHT('0' + CONVERT(VARCHAR, DATEPART(hh, GetDate())), 2) + ':' + 
		  RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, GetDate())), 2)
		--	CAST(GETDATE() as DateTime) --CDS_EXTRACT_DATE 
		, Provider = 'RM200'--OrganisationCodeOfProvider
		, Purchaser = PbR.FinalSLACode --Organisation Code of Commissioner--CASE WHEN PbR.FinalSLACode = '13Q' THEN 'XMD00' --? Do we need to send 13Q to XMD00 as per default codes on Organisation Code of Commissioner page on Data Dictionary--need to read who pays
		, Serial_No = 'SMUTAE'--CommissioningSerialNumber
		, NHS_NO = ISNULL(Encounter.NHSNumber,'')
		, NHS_NO_Status = --PatientCurrentDetails.NHSNumberStatusCode --will need to build in logic from below to get valid code for CDS submission
		CASE
                         --WHEN WithheldIdentityReasonCode IS NOT NULL THEN '07' -- Number not present and trace not required.--There is no WithheldIdentityReasonCode in Cascade - try to find it a build it into patient current details?
                         WHEN PatientCurrentDetails.NHSNo IS NULL THEN '04' -- Trace apptempted - no match or multiple match found.
                         WHEN PatientCurrentDetails.NHSNumberStatusCode = 'SUCCS' THEN '01' -- Number present and verified.
                         WHEN PatientCurrentDetails.NHSNumberStatusCode = 'RESET' THEN '02' -- Number present but not traced.
                         WHEN PatientCurrentDetails.NHSNumberStatusCode IS NULL THEN '03' -- Trace required.
                         ELSE PatientCurrentDetails.NHSNumberStatusCode
                       END   
 		, Name_Format =  CASE WHEN PatientCurrentDetails.NHSNumberStatusCode not in ('SUCCS', '01')  OR   PatientCurrentDetails.NHSNumberStatusCode  IS NULL THEN '1' Else '' END--should only be populated for those records which do not have a valid and successfully traced NHS No as that is when name details need to be provided.  Field is used to indicate whether the name provided is structured or unstructured 1 Structured - two element name, forename followed by surname, each element an35 . 2 Unstructured - an70 
		, Address_Format =  CASE WHEN PatientCurrentDetails.NHSNumberStatusCode not in ('SUCCS', '01')  OR   PatientCurrentDetails.NHSNumberStatusCode  IS NULL THEN '1' Else '' END--should only be populated for those records which do not have a valid and successfully traced NHS No as that is when address details need to be provided.  Field is used to indicate whether the address provided is structured or unstructured 
		--1 or S To denote a "Label format" address, i.e. an address consisting of up to five address lines of 35 characters where each line is left justified as a specific data element 
		--2 or U To denote an unstructured address, i.e. an address consisting of up to five contiguous data elements of 35 characters representing a 175 character string 

		, Name =  CASE WHEN PatientCurrentDetails.NHSNumberStatusCode not in ('SUCCS', '01')  OR   PatientCurrentDetails.NHSNumberStatusCode  IS NULL THEN Encounter.PatientSurname Else '' END
		, Forename = CASE WHEN PatientCurrentDetails.NHSNumberStatusCode not in ('SUCCS', '01') OR   PatientCurrentDetails.NHSNumberStatusCode  IS NULL THEN Encounter.PatientForename Else '' END
		, HomeAdd1 = CASE WHEN PatientCurrentDetails.NHSNumberStatusCode not in ('SUCCS', '01') OR   PatientCurrentDetails.NHSNumberStatusCode  IS NULL THEN ISNULL(Encounter.PatientAddress1, 'NO ADDRESS DATA AVAILABLE') Else '' END
		, HomeAdd2 = CASE WHEN PatientCurrentDetails.NHSNumberStatusCode not in ('SUCCS', '01') OR   PatientCurrentDetails.NHSNumberStatusCode  IS NULL THEN ISNULL(Encounter.PatientAddress2,'NO MORE ADDRESS DATA AVAILABLE') Else '' END
		, HomeAdd3 = CASE WHEN PatientCurrentDetails.NHSNumberStatusCode not in ('SUCCS', '01')  OR   PatientCurrentDetails.NHSNumberStatusCode  IS NULL THEN ISNULL(Encounter.PatientAddress3,'') Else '' END
		, HomeAdd4 =  CASE WHEN PatientCurrentDetails.NHSNumberStatusCode not in ('SUCCS', '01')  OR   PatientCurrentDetails.NHSNumberStatusCode  IS NULL THEN ISNULL(Encounter.PatientAddress4,'') Else '' END
		, HomeAdd5 =  ''
		, Postcode = Left(CASE WHEN Encounter.Postcode NOT LIKE '%[0-9]%' THEN 'ZZ99 3AZ'--to handle when a non acceptable value has been entered into postcode field
						WHEN Encounter.Postcode IS NULL THEN 'ZZ99 3AZ'--to handle when a postcode has now been entered
						ELSE Encounter.Postcode
						END , 8) --added Left 8 as there are some instances of DQ issue where postcode is longer than 8
		, HA = CASE WHEN Encounter.ResidenceCCGCode   IS NULL  THEN '01N'
				ELSE Encounter.ResidenceCCGCode
				END --OrganisationCode (Residence Responsibility)
		, Withheld_Identity_Reason = ''
		, Sex = CASE WHEN Encounter.SexCode = 'M' THEN '1'
										WHEN Encounter.SexCode = 'F' THEN '2'
										ELSE 0
										END--PersonGenderCodeCurrent
		, Carer_Support_Ind = ISNULL(CAST(Encounter.CarerSupportIndicator as VARCHAR),'')
		, DOB = cast(Encounter.DateOfBirth  as DATE)
		, GPREG =	ISNULL(CASE WHEN Encounter.RegisteredGPCode not like 'G%' THEN NULL ELSE Encounter.RegisteredGPCode END, 'G9999998')-- G9999998 - GENERAL MEDICAL PRACTITIONER PPD CODE not known --GeneralMedicalPractionerSpecified
		, PRACREG = ISNULL(Encounter.RegisteredGpPracticeCode, 'V81997') -- V81997 - Default for No Registered GP Practice --GeneralMedicalPracticeCode
		, LocPatID = Encounter.DistrictNo -- LocalPatientIdentifier
		, AttendNo =Cast(1101793 +  CAST(SUBSTRING(Encounter.DistrictNo,1,CHARINDEX('G', Encounter.DistrictNo)-1) as INT) as varchar) + 'C' + AttendanceNumber-- AandEAttendanceNumber
		, Category = CASE WHEN Encounter.AttendanceCategoryCode in ('3','5','6', '7','8','9','A','B') THEN 2
										WHEN Encounter.AttendanceCategoryCode in ('2','4') THEN 3
										ELSE 1
										End--AttendanceCategoryCode--Logic as per MWaterworth email- data from AEAttend.ATTTYPE field
		, ArrDate = CASE WHEN (Encounter.RegisteredTime < Encounter.InitialAssessmentTime OR Encounter.InitialAssessmentTime IS NULL)THEN Cast(Encounter.RegisteredTime as Date) ELSE Cast(Encounter.InitialAssessmentTime as Date) END --ArrivalDate: Logic From Michael Waterworth = Arrival Date and Time is whichever is earlier of TriageDate and TriageTime, and Regdate and RegTime in AEATTEND.
		, RefSource =  CASE WHEN Encounter.AttendanceCategoryCode = 6 THEN '07'
									WHEN Encounter.AttendanceCategoryCode <> 6 AND Encounter.SourceOfReferralCode in ('0','2','8','11','13','17')	THEN '08'
									WHEN Encounter.AttendanceCategoryCode <> 6 AND Encounter.SourceOfReferralCode in ('1','16','21') THEN '03'
									WHEN Encounter.AttendanceCategoryCode <> 6 AND Encounter.SourceOfReferralCode in ('3','19','4') THEN '00'
									WHEN Encounter.AttendanceCategoryCode <> 6 AND Encounter.SourceOfReferralCode in ('5','9','12') THEN '04'
									WHEN Encounter.AttendanceCategoryCode <> 6 AND Encounter.SourceOfReferralCode in ('7') THEN '01'
									WHEN Encounter.AttendanceCategoryCode <> 6 AND Encounter.SourceOfReferralCode in ('10') THEN '06'
									WHEN Encounter.AttendanceCategoryCode <> 6 AND Encounter.SourceOfReferralCode in ('14') THEN '02'
									WHEN Encounter.AttendanceCategoryCode <> 6 AND Encounter.SourceOfReferralCode in ('15','18') THEN '07'
								ELSE '08'
								END--SourceOfReferral
		, Department_Type = '01'--01 Emergency departments are a CONSULTANT led 24 hour service with full resuscitation facilities and designated accommodation for the reception of accident and emergency PATIENTS  
		, ArrMode = Encounter.ArrivalModeCode-- this is derived from the AEAttend.Source field 
		
		, ArrTime = CASE WHEN (Encounter.RegisteredTime < Encounter.InitialAssessmentTime OR Encounter.InitialAssessmentTime IS NULL) THEN convert(varchar,Encounter.RegisteredTime, 108) ELSE Convert(varchar, Encounter.InitialAssessmentTime, 108) END  --Logic From Michael Waterworth = Arrival Date and Time is whichever is earlier of TriageDate and TriageTime, and Regdate and RegTime in AEATTEND.
		, Activity_date = Cast(Encounter.ArrivalDate as DATE) --CDS Activity Date--
		, Age_At_CDS_Activity_Date = Encounter.AgeOnArrival
		, VisitorStatusAtActivityDate = ''
		, PatGroup = CASE WHEN Encounter.TriageTrauma = 0 THEN '80'
							WHEN Encounter.AttendanceDisposalCode = 'DI' and Encounter.WhereDied = 1 THEN '70'
							WHEN Encounter.mtg_categ = '004' THEN '20'
							WHEN Encounter.mtg_categ = '013' THEN '30'
							WHEN Encounter.IncidentLocationTypeCode = 'L' THEN '40'
							WHEN Encounter.trclomech like '%FIREWORK%'
									OR Encounter.fbmech like '%FIREWORK%'
									OR Encounter.trhedmech like '%FIREWORK%'
									OR Encounter.treyemech like '%FIREWORK%'
									OR Encounter.trnosmech like '%FIREWORK%'
									OR Encounter.trnecmech like '%FIREWORK%'
									OR Encounter.trbacmech like '%FIREWORK%'
									THEN '50'
							WHEN Encounter.IncidentLocationTypeCode = 'R' THEN 10
						ELSE '60'
						END
									
		, IncLocType = CASE WHEN Encounter.TriageTrauma = 0  THEN ''--Data Dictionary Notes: This applies to trauma and accident cases only.10 Home , 40 Work,50 Educational Establishment,60 Public place ,91 Other 
							WHEN Encounter.IncidentLocationTypeCode in ('D','H','E') THEN '10'
							WHEN Encounter.IncidentLocationTypeCode in ('W')  THEN '40'
							WHEN Encounter.IncidentLocationTypeCode in ('S')  THEN '50'
							WHEN Encounter.IncidentLocationTypeCode in ('L','B','C','P','N','R')  THEN '60'
							WHEN Encounter.TriageTrauma =  1 and Encounter.IncidentLocationTypeCode IS NULL THEN '91'
							ELSE ''
							END--IncidentLocationType
		, AE_Initial_Assessment_Date = ISNULL(Cast(Encounter.InitialAssessmentTime as Date),Cast(Encounter.RegisteredTime as Date) )--AEInitialAssessmentDate.  Looks like Michael W uses Registration/Arrival date if there is not initial assessment date time
		, AssTime = ISNULL(convert(varchar,Encounter.InitialAssessmentTime, 108),convert(varchar,Encounter.RegisteredTime, 108) ) --AEInitialAssessmentTime. Looks like Michael W uses Registration/Arrival date if there is not initial assessment date time
		, AE_Seen_For_Treatment_Date = CASE WHEN Encounter.WhereSeen = 4 THEN Cast(Encounter.AttendanceConclusionTime as Date)
										ELSE CAST(Seen.MinSeenDate as DATE)
										END--MW Logic for this field = If the pathway in AEATTEND is 4 then then the Seen date is TRTCOMPDAT in AEATTEND and the Seen time is TRTCOMPTIM in  AEATTEND.
											--If the pathway is other than 4 the Seen date and time are the earliest of DRSEENDATE and DRSEENTIME in AEATTEND, TOADMITDAT and TOADMITTIM in AEATTEND, SPECDRDATE and SPECDRTIME in AEVDROOM.
		, Treatime = CASE WHEN Encounter.WhereSeen = 4 THEN Convert(varchar,Encounter.AttendanceConclusionTime,108)
										ELSE Convert(varchar, Seen.MinSeenDate,108)
										END --MW Logic for this field = If the pathway in AEATTEND is 4 then then the Seen date is TRTCOMPDAT in AEATTEND and the Seen time is TRTCOMPTIM in  AEATTEND.
											--If the pathway is other than 4 the Seen date and time are the earliest of DRSEENDATE and DRSEENTIME in AEATTEND, TOADMITDAT and TOADMITTIM in AEATTEND, SPECDRDATE and SPECDRTIME in AEVDROOM.
		, StaffCode = ISNULL(SeenByStaffID, '')--Staff Member Code
		, AE_Attendance_Conclusion_Date = Cast(Encounter.AttendanceConclusionTime as Date)--AEAttendanceConclusionDate
		, EndTime =  convert(varchar,Encounter.AttendanceConclusionTime, 108)--AEAttendanceConclusionTime
		, AE_Departure_Date = Cast(Encounter.AttendanceConclusionTime as Date)--AEDepartureDate--Departure Date not filled in in the A&E Data - check with Michael - for now replace it with Attendance Conclusion Time
		, DepTime =  convert(varchar,Encounter.AttendanceConclusionTime, 108)--AEDepartureTimeDeparture Date not filled in in the A&E Data - check with Michael - for now replace it with Attendance Conclusion Time
		, AmbulanceIncidentNumber = RTrim(Encounter.AmbulanceIncidentNo)--Comment from 'Ambulance Incident Number is found in AEATTEND in the AMICIDENT field. When populated the first three characters indicate which Ambulance Trust was the conveyor so SUS have the info anyway.'
		, ConveyingAmbulanceTrust = LEFT(Encounter.AmbulanceIncidentNo,3)--Comment from MW 'Ambulance Incident Number is found in AEATTEND in the AMICIDENT field. When populated the first three characters indicate which Ambulance Trust was the conveyor so SUS have the info anyway.'
		, Disposal =  CASE WHEN Encounter.AttendanceDisposalCode = 'AD' THEN '01'
										WHEN Encounter.AttendanceDisposalCode = 'TH' THEN '07'
										WHEN Encounter.AttendanceDisposalCode = 'DA' THEN '03'
										WHEN Encounter.AttendanceDisposalCode = 'DF' AND Encounter.Revclin  =  1 THEN '04'
										WHEN Encounter.AttendanceDisposalCode = 'DF' AND Encounter.ReferGP = 1 THEN '02'
										WHEN Encounter.AttendanceDisposalCode = 'DF' AND Encounter.Fractclin = 1 THEN '05'
										WHEN Encounter.AttendanceDisposalCode = 'DF' AND Encounter.OPD =  1 THEN '05'
										WHEN Encounter.AttendanceDisposalCode = 'DF' AND Encounter.RefOthSite =  1 THEN '05'
										WHEN Encounter.AttendanceDisposalCode = 'DF' AND Encounter.RfOutTrust = 1 THEN '05' 
										WHEN Encounter.AttendanceDisposalCode = 'DF' AND Encounter.Revclin  <> 1 AND Encounter.ReferGP <> 1 AND Encounter.Fractclin <> 1 AND  Encounter.OPD <>  1 AND  Encounter.RefOthSite <> 1 AND Encounter.RfOutTrust <> 1 THEN '11'
										WHEN Encounter.AttendanceDisposalCode =  'X' AND SeenForTreatmentTime IS NULL AND Encounter.SeenBySpecialtyTime is NULL THEN '12'						
										WHEN Encounter.AttendanceDisposalCode =  'X' AND (SeenForTreatmentTime IS NOT NULL OR Encounter.SeenBySpecialtyTime is NOT NULL) THEN '13'
										WHEN Encounter.AttendanceDisposalCode =  'DI' AND Encounter.WhereDied = 2 THEN '10'
										Else '14'
										END --AttendanceDisposalCode
		, Local_Patient_ID_Org = 'RM200' -- OrganisationCode
		, Health_Clinic= PbR.FinalSLACode+ '001'
		, Purchaser_ref = ISNULL(Encounter.CommissionerReferenceNo,'')--Commissioner Reference No
		, Provider_Ref_no = ISNULL(Encounter.ProviderReferenceNo,'')--Provider Reference No
		, UBRN= ''
		, Care_Pathway_ID = ''
		, Care_Pathway_ID_Org =  ''
		, REF_TO_TREAT_PERIOD_STATUS = ''
		, REF_TO_TREAT_PERIOD_START_DATE = ''
		, REF_TO_TREAT_PERIOD_END_DATE = ''
		, WAIT_TIME_MEASUREMENT_TYPE = ''
		, ETHNIC_CATEGORY = EthnicCategoryLookup.EthnicCategoryNHSCode
		, SITE_CODE_OF_TREATMENT = 'RM200'
		, INVESTIGATION_SCHEME ='01'
		, DIAGNOSIS_SCHEME = '01'
		, TREATMENT_SCHEME = '01'
		, ICD_DIAG_SCHEME = ''
		, READ_DIAG_SCHEME = ''
		, OPCS_PROC_SCHEME = ''
		, READ_PROC_SCHEME = ''


FROM AE.Encounter  
Left outer join WHREPORTING.LK.PatientCurrentDetails --join to this table to get the NHS number status code 
on Encounter.LocalPatientID = PatientCurrentDetails.FacilityID and Encounter.NHSNumber = PatientCurrentDetails.NHSNo--match on both to ensure that the status is right for the right nhs number
Left outer join AE.EthnicCategoryLookup on Encounter.EthnicCategoryCode = EthnicCategoryLookup.EthnicCategoryCode
Left outer join (Select * from PbR2016.dbo.PbR where DatasetCode = 'A&E') PbR on Encounter.SourceUniqueID = PbR.SourceUniqueID
Left outer join (SELECT SourceUniqueID
	,Min(SeenDate) as MinSeenDate
FROM (
	SELECT SourceUniqueID
		,SeenForTreatmentTime AS SeenDate
	FROM AE.Encounter
	WHERE Encounter.ArrivalDate >= @ReportingPeriodStart
	and Encounter.ArrivalDate <= @ReportingPeriodEnd
	
	UNION ALL
	
	SELECT SourceUniqueID
		,SeenBySpecialtyTime
	FROM AE.Encounter
	WHERE Encounter.ArrivalDate >= @ReportingPeriodStart
	and Encounter.ArrivalDate <= @ReportingPeriodEnd
	
	UNION ALL
	
	SELECT SourceUniqueID
		,DecisionToAdmitTime
	FROM AE.Encounter
	WHERE Encounter.ArrivalDate >= @ReportingPeriodStart
	and Encounter.ArrivalDate <= @ReportingPeriodEnd
	) Max_Seen
GROUP BY SourceUniqueID
) Seen --subquery to get the earliest date out of the 3 - SeenForTreatmentTime, SeenBySpecialtyTime and DecisionToAdmitTime for use in the AE_Seen_For_Treatment_Date field
ON Encounter.SourceUniqueID = Seen.SourceUniqueID
where Encounter.ArrivalDate >= @ReportingPeriodStart
and Encounter.ArrivalDate <= @ReportingPeriodEnd

Print 'AEMain Loaded' 



/**********************************************
CDS AE Diagnosis Child Table 
**********************************************/
Truncate table   CDS.AEDiagnosis--FromDW
Insert into   CDS.AEDiagnosis--FromDW
Select  CDS_IDENTIFIER = 	'BRM200' + Right('00000000000000000000000000000000000000' + 
					Cast(1101793 +  CAST(SUBSTRING(Encounter.DistrictNo,1,CHARINDEX('G', Encounter.DistrictNo)-1) as INT) as varchar) + 'C' + AttendanceNumber
					+ Right('000000000' + cast(Datediff(DAY,'19941117',Encounter.RegisteredTime)	 as varchar),8)	,29)	
		, SEQUENCE_NUMBER = Right('000000' + Cast(ISNULL(Diagnosis.SequenceNo, '01') as Varchar), 4)
		, AE_DIAG =  ISNULL(Diagnosis.SourceDiagnosisCode, '38')--where a Diagnosis has not been entered, MW replaces with a default of 38 - as per email :
															--After further investigation, I find that the process which generates the files uses 
															--“38” “Diagnosis not classifiable” as the default for Diagnosis, 
														--	“99” “None” for Treatment 
															--and 
															--“24” “None” for Investigation.

FROM AE.Encounter  
LEFT OUTER JOIN AE.Diagnosis 
ON Encounter.SourceUniqueID = Diagnosis.AESourceUniqueID
where Encounter.ArrivalDate >= @ReportingPeriodStart
and Encounter.ArrivalDate <= @ReportingPeriodEnd


Print 'AEDiagnosis Loaded' 


/**********************************************
CDS AE Investigation Child Table 
**********************************************/
Truncate table   CDS.AEInvestigations--FromDW
Insert into   CDS.AEInvestigations--FromDW
Select  CDS_IDENTIFIER = 	'BRM200' + Right('00000000000000000000000000000000000000' + 
					Cast(1101793 +  CAST(SUBSTRING(Encounter.DistrictNo,1,CHARINDEX('G', Encounter.DistrictNo)-1) as INT) as varchar) + 'C' + AttendanceNumber
					+ Right('000000000' + cast(Datediff(DAY,'19941117',Encounter.RegisteredTime)	 as varchar),8)	,29)	
		, SEQUENCE_NUMBER = Right('000000' + Cast(ISNULL(Investigation.SequenceNo, '01') as Varchar), 4)
		, AE_INV =  ISNULL(Investigation.SourceInvestigationCode, '24')--where an Investigation has not been entered, MW replaces with a default of 24 - as per email 
															--After further investigation, I find that the process which generates the files uses 
															--“38” “Diagnosis not classifiable” as the default for Diagnosis, 
														--	“99” “None” for Treatment 
															--and 
															--“24” “None” for Investigation.

FROM AE.Encounter  
LEFT OUTER JOIN AE.Investigation 
ON Encounter.SourceUniqueID = Investigation.AESourceUniqueID
where Encounter.ArrivalDate >= @ReportingPeriodStart
and Encounter.ArrivalDate <= @ReportingPeriodEnd


Print 'AEInvestigation Loaded' 

/**********************************************
CDS AE Procedure Child Table 
**********************************************/
Truncate table   CDS.AEProcedures--FromDW
Insert into   CDS.AEProcedures--FromDW
Select  CDS_IDENTIFIER = 	'BRM200' + Right('00000000000000000000000000000000000000' + 
					Cast(1101793 +  CAST(SUBSTRING(Encounter.DistrictNo,1,CHARINDEX('G', Encounter.DistrictNo)-1) as INT) as varchar) + 'C' + AttendanceNumber
					+ Right('000000000' + cast(Datediff(DAY,'19941117',Encounter.RegisteredTime)	 as varchar),8)	,29)	
		, SEQUENCE_NUMBER = Right('000000' + Cast(ISNULL([Procedure].SequenceNo, '01') as Varchar), 4)
		, AE_PROC =  ISNULL([Procedure].SourceProcedureCode, '99')--where a Treatment Code has not been entered, MW replaces with a default of 99 - as per email 
															--After further investigation, I find that the process which generates the files uses 
															--“38” “Diagnosis not classifiable” as the default for Diagnosis, 
														--	“99” “None” for Treatment 
															--and 
															--“24” “None” for Investigation.
		, AE_PROC_DATE = Cast(Encounter.ArrivalDate as Date)

FROM AE.Encounter  
LEFT OUTER JOIN AE.[Procedure] 
ON Encounter.SourceUniqueID = [Procedure].AESourceUniqueID
where Encounter.ArrivalDate >= @ReportingPeriodStart
and Encounter.ArrivalDate <= @ReportingPeriodEnd


Print 'AEProcedure Loaded' 