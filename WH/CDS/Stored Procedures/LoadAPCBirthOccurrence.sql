﻿


/***********************************************************************************
 Description: Load final APC Birth Occurrence table from staging data table.
 
 Modification History --------------------------------------------------------------

 Date     Author      Change
 19/03/14 Tim Dean	  Created.
 19/05/14 Tim Dean    Added code to validate birth weight as there was a still birth 
                      with a weight of zero which is outside the acceptable range. 

************************************************************************************/

CREATE Procedure [CDS].[LoadAPCBirthOccurrence]
AS

SET NOCOUNT ON;

TRUNCATE TABLE CDS.APCBirthOccurrence;

INSERT INTO CDS.APCBirthOccurrence
            (
             EpisodeSourceUniqueID
	        ,CDS_ID
	        ,BIRTHORD
	        ,DELIVERY_METHOD
	        ,GESTATION
	        ,BIRESUS
	        ,STAT_PERSON_CONDEL
	        ,LOC_PAT_ID
	        ,ORG_CODE_BAB_ID
	        ,NHS_NO_BABY
	        ,NHS_NO_STAT
	        ,BABYDOB
	        ,BIRWEIT
 	        ,BIRSTAT
	        ,BABY_VISSTAT_AS_ACTIVITY_DATE
	        ,SEXBABY
	        ,LOCATION_CLASS_DEL_PLC_ACTUAL
	        ,LOCATION_TYPE_DEL_PLC_ACTUAL
	        ,DELPLACE
	        ,WITHHELD_IDENTITY_REASON_BABY
            )
SELECT EpisodeSourceUniqueID
	  ,CDS_ID = LEFT('BRM2' + RIGHT( Replicate('0', 14) + CAST(EpisodeSourceUniqueID AS VARCHAR), 14 ), 35)
	  ,BIRTHORD = CAST(BirthOrder AS CHAR(1))
	  ,DELIVERY_METHOD = CAST(MaternityReporting.dbo.func_GetValueCode(DeliveryMethod) AS CHAR)
	  ,GESTATION = COALESCE(CAST(EstimateGestationWeeks AS VARCHAR(2)),'99')
      ,BIRESUS = CASE
                   WHEN OutcomeOfBirth = 'Stillbirth' THEN '8' -- Not applicable(e.g. stillborn).
                   ELSE COALESCE(CAST(MaternityReporting.dbo.func_GetValueCode(NationalResuscitationCode) AS CHAR), 9)
                 END     
	  ,STAT_PERSON_CONDEL = CASE
                              WHEN TypeOfPersonDelivering = 'Consultant' THEN '1' -- Hospital doctor.
                              WHEN TypeOfPersonDelivering = 'Hospital Doctor' THEN '1' -- Hospital doctor.
                              WHEN TypeOfPersonDelivering = 'General Practitioner' THEN '2' -- General medical practitioner.
                              WHEN TypeOfPersonDelivering = 'Midwife' THEN '3' -- Midwife.
                              WHEN TypeOfPersonDelivering IS NOT NULL THEN '8' -- Other.
                              ELSE '9' -- Not known: a validation error.
                            END  
      ,LOC_PAT_ID = LEFT(BabysHospitalNumber, 10)  
      ,ORG_CODE_BAB_ID = CASE
                           WHEN BabysHospitalNumber IS NOT NULL THEN 'RM200'
                         END  
	  ,NHS_NO_BABY = BabysNHSNumber    
	  ,NHS_NO_STAT = CASE
                       WHEN BabysNHSNumber IS NOT NULL
                            AND BabysNHSNumberStatus = 'NN4B successful' THEN '01' -- Number present and verified.
                       WHEN BabysNHSNumber IS NOT NULL
                            AND BabysNHSNumberStatus = 'NHS number recorded by user' THEN '02' -- Number present but not traced.
                       WHEN BabysNHSNumber IS NOT NULL
                            AND BabysNHSNumberStatus = 'NHS number updated by user' THEN '02' -- Number present but not traced.         
                       WHEN BabysNHSNumber IS NOT NULL
                            AND BabysNHSNumberStatus = 'NN4B failed' THEN '04' -- Trace attempted - No match or multiple match found.
                       WHEN BabysNHSNumber IS NOT NULL
                            AND BabysNHSNumberStatus = 'Process Started' THEN '06' -- Trace in progress.
                       WHEN BabysNHSNumber IS NOT NULL
                            AND BabysNHSNumberStatus = 'Message sent' THEN '06' -- Trace in progress.         
                       WHEN BabysNHSNumber IS NULL THEN '07' -- Number not present and trace not required.
                       ELSE ''
                     END 
      ,BABYDOB = LEFT(CONVERT(VARCHAR, BabyBirthDate, 120), 10)   
      -- 19/05/2014 TJD Added code to validate birth weight as there was a still birth with
      --                a weight of zero which is outside the acceptable range.   
	  ,BIRWEIT = CASE
	               WHEN BirthWeight BETWEEN 1 AND 9998 THEN CAST(BirthWeight AS VARCHAR(4))
	               ELSE '9999' -- 9999 Not Known.
	             END      
	  ,BIRSTAT = CASE
                   WHEN OutcomeOfBirth = 'Livebirth' THEN '1' -- Live.
                   WHEN OutcomeOfBirth = 'Stillbirth'
                        AND OutcomeOfBirthDeathOfFetus IN ( 'Probably before Labour', 'Confirmed before Labour' ) THEN '2' -- Still birth ante-partum.
                   WHEN OutcomeOfBirth = 'Stillbirth'
                        AND OutcomeOfBirthDeathOfFetus = 'During Labour' THEN '3' -- Still birth intra-partum.
                   WHEN OutcomeOfBirth = 'Stillbirth'
                        AND ( OutcomeOfBirthDeathOfFetus = 'Time Unknown'
                              OR OutcomeOfBirthDeathOfFetus IS NULL ) THEN '4' -- Still birth indeterminate.
                   ELSE ''                  
                 END       
	  ,BABY_VISSTAT_AS_ACTIVITY_DATE = '' 
	  ,SEXBABY = CASE
                   WHEN SexOfBaby = 'Male' THEN '1'
                   WHEN SexOfBaby = 'Female' THEN '2'
                   WHEN SexOfBaby LIKE '%nown%' THEN '0' -- Not known.
                   WHEN SexOfBaby LIKE 'Indeterminate (ambiguous)' THEN '0' -- Not known.
                   ELSE '9' -- Not specified.
                 END  
      ,LOCATION_CLASS_DEL_PLC_ACTUAL = ''
      ,LOCATION_TYPE_DEL_PLC_ACTUAL = ''
      ,DELPLACE = COALESCE(CAST(MaternityReporting.dbo.func_GetValueCode(ActualPlaceOfBirth) AS CHAR),'9')
      ,WITHHELD_IDENTITY_REASON_BABY = ''      	      
FROM CDS.APCStagingBirth;
       



