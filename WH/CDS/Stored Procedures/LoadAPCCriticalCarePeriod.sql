﻿


/***********************************************************************************
 Description: Load final APC Critical Care Period table from staging data table.
 
 Modification History --------------------------------------------------------------

 Date     Author      Change
 19/03/14 Tim Dean	  Created.
 02/10/14 Tim Dean    Only populate discharge details if there is a discharge date.
************************************************************************************/

CREATE Procedure [CDS].[LoadAPCCriticalCarePeriod]
AS

SET NOCOUNT ON;

TRUNCATE TABLE CDS.APCCriticalCarePeriod;

INSERT INTO CDS.APCCriticalCarePeriod
            (
              EpisodeSourceUniqueID
             ,CDS_ID
             ,SEQUENCE_NUMBER
             ,CC_LOCAL_IDENTIFIER
             ,CC_START_DATE
             ,CC_START_TIME
             ,CC_UNIT_FUNCTION
             ,CC_ADV_RESPI_SUPPORT_DAYS
             ,CC_BASIC_RESPIR_SUPPORT_DAYS
             ,CC_ADV_CARDIO_SUPPORT_DAYS
             ,CC_BASIC_CARDIO_SUPPORT_DAYS
             ,CC_RENAL_SUPPORT_DAYS
             ,CC_NEURO_SYSTEM_SUPPORT_DAYS
             ,CC_DERMA_SYSTEM_SUPPORT_DAY
             ,CC_LIVER_SUPPORT_DAYS             
             ,CC_GASTRO_SUPPORT_DAYS
             ,CC_ORGAN_SUPPORT_MAXIMUM             
             ,CC_LEVEL_2_DAYS     
             ,CC_LEVEL_3_DAYS
             ,CC_DISCHARGE_DATE
             ,CC_DISCHARGE_TIME
             ,CC_UNIT_BED_CONFIG
             ,CC_ADMISSION_SOURCE
             ,CC_SOURCE_LOCATION
             ,CC_ADMISSION_TYPE
             ,CC_DISCHARGE_READY_DATE
             ,CC_DISCHARGE_READY_TIME
             ,CC_DISCHARGE_STATUS
             ,CC_DISCHARGE_DEST
             ,CC_DISCHARGE_LOCATION--
             ,CC_GESTATION_LENGTH_AT_DELIV
             ,CC_PERSON_WEIGHT
             ,CC_ACTIVITY_DATE
             ,CC_ACTIVITY_CODE_1
             ,CC_HIGH_COST_DRUGS_1
             ,CC_ACTIVITY_CODE_2
             ,CC_HIGH_COST_DRUGS_2
             ,CC_ACTIVITY_CODE_3
             ,CC_HIGH_COST_DRUGS_3 
             ,CC_ACTIVITY_CODE_4
             ,CC_HIGH_COST_DRUGS_4 
             ,CC_ACTIVITY_CODE_5
             ,CC_HIGH_COST_DRUGS_5
             ,CC_ACTIVITY_CODE_6
             ,CC_HIGH_COST_DRUGS_6
             ,CC_ACTIVITY_CODE_7
             ,CC_HIGH_COST_DRUGS_7
             ,CC_ACTIVITY_CODE_8
             ,CC_HIGH_COST_DRUGS_8
             ,CC_ACTIVITY_CODE_9
             ,CC_HIGH_COST_DRUGS_9
             ,CC_ACTIVITY_CODE_10
             ,CC_HIGH_COST_DRUGS_10
             ,CC_ACTIVITY_CODE_11
             ,CC_HIGH_COST_DRUGS_11
             ,CC_ACTIVITY_CODE_12
             ,CC_HIGH_COST_DRUGS_12
             ,CC_ACTIVITY_CODE_13
             ,CC_HIGH_COST_DRUGS_13
             ,CC_ACTIVITY_CODE_14
             ,CC_HIGH_COST_DRUGS_14
             ,CC_ACTIVITY_CODE_15
             ,CC_HIGH_COST_DRUGS_15  
             ,CC_ACTIVITY_CODE_16
             ,CC_HIGH_COST_DRUGS_16
             ,CC_ACTIVITY_CODE_17
             ,CC_HIGH_COST_DRUGS_17 
             ,CC_ACTIVITY_CODE_18
             ,CC_HIGH_COST_DRUGS_18
             ,CC_ACTIVITY_CODE_19
             ,CC_HIGH_COST_DRUGS_19
             ,CC_ACTIVITY_CODE_20
             ,CC_HIGH_COST_DRUGS_20                                                                                                                                                                                                                                     
            )
SELECT EpisodeSourceUniqueID
	  ,CDS_ID = LEFT('BRM2' + RIGHT( Replicate('0', 14) + CAST(EpisodeSourceUniqueID AS VARCHAR), 14 ), 35)
      ,SEQUENCE_NUMBER = CAST(SequenceNumber AS VARCHAR(4))
      ,CC_LOCAL_IDENTIFIER = LEFT(CAST(CriticalCareLocalIdentifier AS VARCHAR),2)
                               + RIGHT(CAST(CriticalCareLocalIdentifier AS VARCHAR),6)
      ,CC_START_DATE = LEFT(CONVERT(VARCHAR,CriticalCareStartTime , 120), 10)  
      ,CC_START_TIME = LEFT(CONVERT(VARCHAR,CriticalCareStartTime, 108), 8)
      ,CC_UNIT_FUNCTION = LEFT(CriticalCareUnitFunctionCode,2)
      ,CC_ADV_RESPI_SUPPORT_DAYS = CAST(AdvancedRespiratorySupportDays AS VARCHAR(3))
      ,CC_BASIC_RESPIR_SUPPORT_DAYS = CAST(BasicRespiratorySupportDays AS VARCHAR(3))
      ,CC_ADV_CARDIO_SUPPORT_DAYS = CAST(AdvancedCardiovascularSupportDays AS VARCHAR(3))
      ,CC_BASIC_CARDIO_SUPPORT_DAYS = CAST(BasicCardiovascularSupportDays AS VARCHAR(3))
      ,CC_RENAL_SUPPORT_DAYS = CAST(RenalSupportDays AS VARCHAR(3))
      ,CC_NEURO_SYSTEM_SUPPORT_DAYS = CAST(NeurologicalSupportDays AS VARCHAR(3))
      ,CC_DERMA_SYSTEM_SUPPORT_DAY = CAST(DermatologicalSupportDays AS VARCHAR(3))
      ,CC_LIVER_SUPPORT_DAYS = CAST(LiverSupportDays AS VARCHAR(3))
      ,CC_GASTRO_SUPPORT_DAYS = CAST(GastrointestinalSupportDays AS VARCHAR(3))
      ,CC_ORGAN_SUPPORT_MAXIMUM = '' -- Optional data item, not populated. 
      ,CC_LEVEL_2_DAYS = CAST(Level2SupportDays AS VARCHAR(3))
      ,CC_LEVEL_3_DAYS = CAST(Level3SupportDays AS VARCHAR(3))
      ,CC_DISCHARGE_DATE = LEFT(CONVERT(VARCHAR,CriticalCareDischargeTime , 120), 10)
      ,CC_DISCHARGE_TIME = LEFT(CONVERT(VARCHAR,CriticalCareDischargeTime, 108), 8)
      ,CC_UNIT_BED_CONFIG = '' -- Optional data item, not populated. 
      ,CC_ADMISSION_SOURCE = '' -- Optional data item, not populated. 
      ,CC_SOURCE_LOCATION = '' -- Optional data item, not populated. 
      ,CC_ADMISSION_TYPE = '' -- Optional data item, not populated. 
      ,CC_DISCHARGE_READY_DATE = LEFT(CONVERT(VARCHAR,DischargeReadyTime , 120), 10)
      ,CC_DISCHARGE_READY_TIME = LEFT(CONVERT(VARCHAR,DischargeReadyTime, 108), 8)
      ,CC_DISCHARGE_STATUS = CASE
                               WHEN CriticalCareDischargeTime IS NOT NULL THEN LEFT(DischargeStatusCode,2)
                             END
      ,CC_DISCHARGE_DEST = CASE 
                             WHEN CriticalCareDischargeTime IS NOT NULL THEN LEFT(DischargeDestinationCode,2)
                           END
      ,CC_DISCHARGE_LOCATION = CASE 
                                 WHEN CriticalCareDischargeTime IS NOT NULL THEN LEFT(DischargeLocationCode,2)
                               END
      -- All the following columns are not populated for an adult critical care period.
      ,CC_GESTATION_LENGTH_AT_DELIV = ''
      ,CC_PERSON_WEIGHT = ''
      ,CC_ACTIVITY_DATE = ''
      ,CC_ACTIVITY_CODE_1 = ''
      ,CC_HIGH_COST_DRUGS_1 = ''
      ,CC_ACTIVITY_CODE_2 = ''
      ,CC_HIGH_COST_DRUGS_2 = ''
      ,CC_ACTIVITY_CODE_3 = ''
      ,CC_HIGH_COST_DRUGS_3 = ''
      ,CC_ACTIVITY_CODE_4 = ''
      ,CC_HIGH_COST_DRUGS_4 = ''
      ,CC_ACTIVITY_CODE_5 = ''
      ,CC_HIGH_COST_DRUGS_5 = ''
      ,CC_ACTIVITY_CODE_6 = ''
      ,CC_HIGH_COST_DRUGS_6 = ''
      ,CC_ACTIVITY_CODE_7 = ''
      ,CC_HIGH_COST_DRUGS_7 = ''
      ,CC_ACTIVITY_CODE_8 = ''
      ,CC_HIGH_COST_DRUGS_8 = ''
      ,CC_ACTIVITY_CODE_9 = ''
      ,CC_HIGH_COST_DRUGS_9 = ''
      ,CC_ACTIVITY_CODE_10 = ''
      ,CC_HIGH_COST_DRUGS_10 = ''
      ,CC_ACTIVITY_CODE_11 = ''
      ,CC_HIGH_COST_DRUGS_11 = ''
      ,CC_ACTIVITY_CODE_12 = ''
      ,CC_HIGH_COST_DRUGS_12 = ''
      ,CC_ACTIVITY_CODE_13 = ''
      ,CC_HIGH_COST_DRUGS_13 = ''
      ,CC_ACTIVITY_CODE_14 = ''
      ,CC_HIGH_COST_DRUGS_14 = ''
      ,CC_ACTIVITY_CODE_15 = ''
      ,CC_HIGH_COST_DRUGS_15 = ''
      ,CC_ACTIVITY_CODE_16 = ''
      ,CC_HIGH_COST_DRUGS_16 = ''
      ,CC_ACTIVITY_CODE_17 = ''
      ,CC_HIGH_COST_DRUGS_17 = ''
      ,CC_ACTIVITY_CODE_18 = ''
      ,CC_HIGH_COST_DRUGS_18 = ''
      ,CC_ACTIVITY_CODE_19 = ''
      ,CC_HIGH_COST_DRUGS_19 = ''
      ,CC_ACTIVITY_CODE_20 = ''
      ,CC_HIGH_COST_DRUGS_20 = ''   
FROM CDS.APCStagingCriticalCare;
       



