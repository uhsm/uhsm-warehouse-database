﻿


/***********************************************************************************
 Description: Load final APC Diagnosis table from staging data table.
 
 Modification History --------------------------------------------------------------

 Date     Author      Change
 19/03/14 Tim Dean	  Created.

************************************************************************************/

CREATE Procedure [CDS].[LoadAPCDiagnosis]
AS

SET NOCOUNT ON;

TRUNCATE TABLE CDS.APCDiagnosis;

INSERT INTO CDS.APCDiagnosis
            (
              EpisodeSourceUniqueID
             ,CDS_ID
             ,SEQUENCE_NUMBER
             ,DIAG_CODE_ICD
             ,POA_IND_DIAG
            )
SELECT EpisodeSourceUniqueID
      ,CDS_ID = LEFT('BRM2' + RIGHT( Replicate('0', 14) + CAST(EpisodeSourceUniqueID AS VARCHAR), 14 ), 35)
      ,SEQUENCE_NUMBER = CAST(SequenceNumber AS VARCHAR(4))
      ,DIAG_CODE_ICD = DiagnosisCode
      ,POA_IND_DIAG = COALESCE(DiagnosisPresentOnAdmissionFlag,'9')  -- 9 - Not known.
FROM CDS.APCStagingDiagnosis;
       



