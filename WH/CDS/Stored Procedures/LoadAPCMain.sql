﻿




/***********************************************************************************
 Description: Load final APC Main dataset table from staging data table.
 
              Executes the following Stored Procedures:-
              
                CDS.LoadAPCBirthOccurrence
                CDS.LoadAPCCriticalCarePeriod
                CDS.LoadAPCDiagnosis
                CDS.LoadAPCProcedure
 
 Modification History --------------------------------------------------------------

 Date     Author      Change
 19/03/14 Tim Dean	  Created.
 19/05/14 Tim Dean    Added additional check to make sure at the minimum of two
                      address elements are populated.
 06/10/14 Tim Dean    Specialised Commissioning.
 28/11/14 Tim Dean    COPY_RECEIPIENT_1 FOR 7A5HC to 7A500.
 18/05/15 Tim Dean	  Added logic to determine the start/end site codes to split
						Withington and Wythenshawe activity.
************************************************************************************/

CREATE Procedure [CDS].[LoadAPCMain]
AS

SET NOCOUNT ON;

TRUNCATE TABLE CDS.APCMain;

-- Identify the rows that need translating and loading from the staging table.
INSERT INTO CDS.APCMain
            (
              EpisodeSourceUniqueID
	         ,CDS_ID
	         ,PROTOCOL_IDENTIFIER
	         ,UPDATE_TYPE
	         ,DATETIME_CREATED
            )
SELECT EpisodeSourceUniqueID
      -- CDS UNIQUE IDENTIFIER - providing a unique identity for the life-time of an episode carried in a CDS message.
      ,CDS_ID = LEFT('BRM2' + RIGHT( Replicate('0', 14) + CAST(EpisodeSourceUniqueID AS VARCHAR), 14 ), 35)
      -- CDS PROTOCOL IDENTIFIER CODE - a code to identify the CDS Submission Protocol associated with the transaction.
      -- 010 = Net Change Update Mechanism.
      -- 020 = Bulk Replacement Update Mechanism.
      ,PROTOCOL_IDENTIFIER = ProtocolIdentifier 
      -- CDS UPDATE TYPE - a code to indicate the required database update process for the submitted CDS Message.
      -- The is a mandatory data item when using the Net Change Update Mechanism.  It is not required when
      -- using the Bulk Replacement Update Mechanism.
      
      -- Only need to track deleted episodes as any deleted Evolution data will only result in a change of
      -- CDS TYPE CODE ie 120(birth episode) or 140(delivery episode) to 130(general episode).
      ,UPDATE_TYPE = CASE
                       -- 010 = Net Change Update Mechanism.
                       WHEN ProtocolIdentifier = '010' AND EpisodeDeletedFlag = 'N' THEN '9' -- To indicate a CDS Original or Replacement.
                       WHEN ProtocolIdentifier = '010' AND EpisodeDeletedFlag = 'Y' THEN '1' -- To indicate a CDS Deletion or Cancellation.
                     END 
      -- For Net Change this is the CDS APPLICABLE DATE/TIME
      -- For Bulk Replacement this is the CDS EXTRACT DATE/TIME
      
      -- The update event (or nearest equivalent) that resulted in the need to exchange this CDS.
      ,DATETIME_CREATED = CASE
                            -- Bulk Replacement Update Mechanism use the current system date/time.
                            WHEN ProtocolIdentifier = '020' THEN LEFT(CONVERT(VARCHAR,GETDATE(),120),16)
                            -- Net Change Update Mechanism use the latest modified date/time from the
                            -- Episode, birth notification or delivery birth notification.
                            -- NOTE: 1 day is added to the dates as NetTransform does not like
                            -- episodes that end on the same day as last modified!
                            WHEN EpisodeModified > COALESCE(DeliveryModified,0)
                                 AND EpisodeModified > COALESCE(BirthModified,0) THEN LEFT(CONVERT(VARCHAR,DATEADD(DAY,1,EpisodeModified),120),16)
                            WHEN COALESCE(DeliveryModified,0) > COALESCE(BirthModified,0) THEN LEFT(CONVERT(VARCHAR,DATEADD(DAY,1,DeliveryModified),120),16)
                            ELSE LEFT(CONVERT(VARCHAR,DATEADD(DAY,1,BirthModified),120),16)                          
                          END                      
FROM CDS.APCStagingMain
WHERE DistrictNo IS NOT NULL;

/**********************************************************************************************
* DATA GROUP: COMMISSIONING DATA SET TRANSACTION HEADER GROUP
* Update the CDS TYPE CODE to identify the specific type of CDS data.
* For births and deliveries that took place at home are classified as 'Other'.
***********************************************************************************************/
UPDATE CDS.APCMain
SET    CDS_TYPE = CASE
                    WHEN APCStagingBirth.ActualPlaceOfBirth = 'Domestic address[1]'
                         AND APCStagingMain.BirthDeletedFlag = 'N' THEN '150' -- Other Birth.
                    WHEN APCStagingBirth.ActualPlaceOfBirth <> 'Domestic address[1]'
                         AND APCStagingMain.BirthDeletedFlag = 'N' THEN '120' -- Finished Birth Episode.
                    WHEN APCStagingBirth.ActualPlaceOfBirth = 'Domestic address[1]'
                         AND APCStagingMain.DeliveryDeletedFlag = 'N' THEN '160' -- Other Delivery.
                    WHEN APCStagingBirth.ActualPlaceOfBirth <> 'Domestic address[1]'
                         AND APCStagingMain.DeliveryDeletedFlag = 'N' THEN '140' -- Finished Delivery Episode.
                    ELSE '130' -- Finished General Episode.
                  END
FROM CDS.APCMain 
 INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
 
 -- Link to the first birth for the episode, if applicable.
 LEFT JOIN CDS.APCStagingBirth
            ON APCStagingMain.EpisodeSourceUniqueID = APCStagingBirth.EpisodeSourceUniqueID
               AND APCStagingBirth.BirthOrder = 1
WHERE COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions.

/**********************************************************************************************
* DATA GROUP: COMMISSIONING DATA SET TRANSACTION HEADER GROUP
* Update the remaining header items.
***********************************************************************************************/
UPDATE CDS.APCMain
SET   -- CDS BULK REPLACEMENT GROUP CODE is the CDS group into which CDS types must be grouped when
      -- using the Bulk Replacement Update Mechanism.
      CDS_GROUP = CASE 
                     WHEN PROTOCOL_IDENTIFIER = '020' AND CDS_TYPE IN ('120' -- Finished Birth Episode.
                                                                      ,'130' -- Finished General Episode.
                                                                      ,'140') -- Finished Delivery Episode.
                          THEN '010' -- Finished General, Delivery and Birth Episodes.
                     WHEN PROTOCOL_IDENTIFIER = '020' AND CDS_TYPE = '160' -- Other Delivery.
                          THEN '030' -- Other Delivery.
                     WHEN PROTOCOL_IDENTIFIER = '020' AND CDS_TYPE = '150' -- Other Birth.
                          THEN '040' -- Other Birth.
                   END
      -- CDS REPORT PERIOD START DATE - defines the start date for the CDS Bulk Replacement Update time period.           
      ,BULKSTART = CASE ProtocolIdentifier
                     WHEN '020' THEN COALESCE(LEFT(CONVERT(VARCHAR,ReportPeriodStart,120),10),'')  
                   END
      -- CDS REPORT PERIOD END DATE - defines the end date for the CDS Bulk Replacement Update time period.
	  ,BULKEND = CASE ProtocolIdentifier
                   WHEN '020' THEN COALESCE(LEFT(CONVERT(VARCHAR,ReportPeriodEnd,120),10),'')  
                 END  
	  -- CDS ACTIVITY DATE
	  ,ACTIVITY_DATE = CASE
	                     -- For other birth and delivery use the delivery date (this is effectively the birth date).
	                     WHEN CDS_TYPE IN ('150','160') THEN CASE
	                                                           WHEN APCStagingBirth.BabyBirthDate < ReportPeriodStart THEN LEFT(CONVERT(VARCHAR,EpisodeEndDate , 120), 10)
	                                                           ELSE LEFT(CONVERT(VARCHAR,APCStagingBirth.BabyBirthDate , 120), 10)
	                                                         END 
	                     ELSE LEFT(CONVERT(VARCHAR,EpisodeEndDate , 120), 10) -- Otherwise use the episode end date.
	                   END                 
      -- CDS SENDER IDENTITY                 
      ,SENDER = 'RM200'       
FROM CDS.APCMain
 INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
 
 -- Link to the first birth for the episode, if applicable.
 LEFT JOIN CDS.APCStagingBirth
            ON APCStagingMain.EpisodeSourceUniqueID = APCStagingBirth.EpisodeSourceUniqueID
               AND APCStagingBirth.BirthOrder = 1             
WHERE COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions.             

/**********************************************************************************************
* DATA GROUP: PATIENT PATHWAY
* This Group must be present if the record relates to a Referral To Treatment Period Included
*   In 18 Weeks Target.
* Used in the following CDS Types: 120,130,140,150,160
***********************************************************************************************/
UPDATE CDS.APCMain
SET    UBRN = ''  -- This item is not populated as using the CARE_PATHWAY_ID.
      ,CARE_PATHWAY_ID = LEFT(CASE
			                    WHEN LEN(RTTPathwayID) < 20 THEN STUFF(RTTPathwayID,6,0,REPLICATE('0',20 - LEN(RTTPathwayID)))
			                    ELSE RTTPathwayID
			                  END,20)
      ,CARE_PATHWAY_ID_ORG = CASE
                               WHEN RTTPathwayID IS NOT NULL THEN COALESCE(RTTCurrentProviderCode,'RM200')
                             END
      ,REF_TO_TREAT_PERIOD_STATUS = CASE
                                      WHEN RTTStatusCodeNational IN ('10','11','12','20','21','30','31','32','33','34','35','36','90','91','92','98','99') THEN LEFT(RTTStatusCodeNational,2) 
                                      ELSE '99' -- 99 - not yet known.
                                    END 
      ,REF_TO_TREAT_PERIOD_START_DATE = LEFT(CONVERT(VARCHAR,RTTStartDate , 120), 10)
      ,REF_TO_TREAT_PERIOD_END_DATE = LEFT(CONVERT(VARCHAR,RTTEndDate , 120), 10)
      -- Waiting Time Measurement Type is new in CDS v6-2 and is mandatory if the patient pathway information is provided.
      -- There is currently no information from iPM into the data warehouse, so having to derive the best we can.
      ,WAIT_TIME_MEASUREMENT_TYPE = CASE
                                      WHEN ProfessionalCarerType = 'Consultant' THEN '01' -- Consultant led.
                                      WHEN ProfessionalCarerType = 'Dentist' THEN '09' -- Other.
                                      WHEN ProfessionalCarerType = 'Midwife' THEN '09' -- Other.
                                      WHEN ProfessionalCarerType IN ( 'Nurse'
                                                                     ,'Nurse Practitioner'
                                                                     ,'Specialist Nurse Practitioner'
                                                                     ,'Specialist Practitioner'
                                                                     ,'Clinical Nurse Specialist' ) THEN '09' -- Other.
                                      ELSE '02' -- Allied Health Professional.
                                    END
FROM CDS.APCMain 
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
WHERE COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions.              

/**********************************************************************************************
* DATA GROUP: PATIENT IDENTITY
* One of the following data sub-groups must be used: WITHHELD IDENTITY STRUCTURE
*                                                    VERIFIED IDENTITY STRUCTURE
*                                                    UNVERIFIED IDENTITY STRUCTURE
* Used in the following CDS Types: 120,130,140,150,160
***********************************************************************************************/
UPDATE CDS.APCMain
SET    -- NHS NUMBER STATUS INDICATOR CODE - required for all sub-groups.
       NNN_STATUS_ID = CASE
                         WHEN WithheldIdentityReasonCode IS NOT NULL THEN '07' -- Number not present and trace not required.
                         WHEN NHSNumber IS NULL THEN '04' -- Trace apptempted - no match or multiple match found.
                         WHEN NHSNumberStatusCode = 'SUCCS' THEN '01' -- Number present and verified.
                         WHEN NHSNumberStatusCode = 'RESET' THEN '02' -- Number present but not traced.
                         WHEN NHSNumberStatusCode IS NULL THEN '03' -- Trace required.
                         ELSE NHSNumberStatusCode
                       END    
       /* ORGANISATION CODE (RESIDENCE RESPONSIBILITY) - required for all sub-groups.
       
          The Organisation Code derived from the patient's postcode of usual address, where they
          reside within the boundary of a:
          
          - Clinical Commissioning Group
          - Care Trust
          - Local Health Board (Wales)
          - Scottish Health Board
          - Northern Ireland Local Commissioning Group
          - Primary Healthcare Directorate (Isle of Man)
       
       */  
      ,HA = CASE
              -- Overseas visitors use default X98 - not applicable.
              WHEN OverseasStatusCode IN ('1' -- Exempt from payment - subject to Reciprocal Healthcare Agreement.
                                         ,'2' -- Exempt from payment - other.
                                         ,'3' -- To pay hotel fees only.
                                         ,'4' -- To pay all fees.
                                         ,'9') -- Charging rate not known.
                                         THEN 'X98'
              -- For Scotland use the Scottish Health Board code.
              WHEN LEFT(HealthAuthorityUsualResidence_ODS,1) = 'S' THEN HealthAuthorityUsualResidence_ODS
              ELSE COALESCE(PrimaryCareOrganisationResidence_ODS,'Q99') -- Q99 - Not known
            END
      -- WITHHELD IDENTITY REASON - Only used in WITHHELD IDENTITY STRUCTURE.
      -- indicates that the record has been purposely anonymised for a valid reason.
      ,WITHHELD_IDENTITY_REASON = WithheldIdentityReasonCode
      -- LOCAL PATIENT IDENTIFIER - Used in VERIFIED/UNVERIFIED IDENTITY STRUCTURE.
      ,LOCPATID = CASE
                    -- Only populate if the identity has not been withheld.
                    WHEN WithheldIdentityReasonCode IS NULL THEN DistrictNo
                  END
      -- ORGANISATION CODE (LOCAL PATIENT IDENTIFIER)- Used in VERIFIED/UNVERIFIED IDENTITY STRUCTURE.
      ,ORG_CODE_LOCAL_PAT_ID = CASE
                                 -- Only populate if the identity has not been withheld.
                                 WHEN WithheldIdentityReasonCode IS NULL THEN 'RM200'
                               END
      -- NHS NUMBER - Used in VERIFIED/UNVERIFIED IDENTITY STRUCTURE.
      ,NHS_NO = CASE
                  -- Only populate if the identity has not been withheld.
                  WHEN WithheldIdentityReasonCode IS NULL THEN NHSNumber
                END
      -- POSTCODE OF USUAL ADDRESS - Used in VERIFIED/UNVERIFIED IDENTITY STRUCTURE.         
      ,POSTCODE = CASE
                    -- Only populate if the identity has not been withheld.
                    WHEN WithheldIdentityReasonCode IS NOT NULL THEN ''
                    WHEN APCStagingMain.Postcode IS NULL THEN 'ZZ99 3WZ' -- Unknown address
                    ELSE APCStagingMain.Postcode
                  END
      -- PERSON BIRTH DATE - Used in VERIFIED/UNVERIFIED IDENTITY STRUCTURE.   
      ,DOB = CASE 
               -- Only populate if the identity has not been withheld.
               WHEN WithheldIdentityReasonCode IS NULL THEN LEFT(CONVERT(VARCHAR,DateOfBirth , 120), 10)
             END
      -- NAME FORMAT CODE - Used in UNVERIFIED IDENTITY STRUCTURE. 
      ,NAME_FORMAT = CASE
                       -- Only populate if the identity has not been withheld and the NHS number has not been verified.
                       WHEN WithheldIdentityReasonCode IS NULL
                            AND NHSNumberStatusCode <> 'SUCCS' THEN '1' -- Structured - two element name, forename followed by surname, each element an35.
                     END    
      -- PATIENT NAME - Used in UNVERIFIED IDENTITY STRUCTURE. 
      ,NAME = CASE
                -- Only populate if the identity has not been withheld and the NHS number has not been verified.
                WHEN WithheldIdentityReasonCode IS NULL 
                     AND NHSNumberStatusCode <> 'SUCCS' THEN PatientSurname
              END
      ,FORENAME = CASE
                    WHEN WithheldIdentityReasonCode IS NULL 
                         AND NHSNumberStatusCode <> 'SUCCS' THEN PatientForename
                  END 
      -- ADDRESS FORMAT CODE - Used in UNVERIFIED IDENTITY STRUCTURE.            
      ,ADDRESS_FORMAT = CASE
                         -- Only populate if the identity has not been withheld and the NHS number has not been verified.
                         WHEN WithheldIdentityReasonCode IS NULL
                              AND NHSNumberStatusCode <> 'SUCCS' THEN '1' -- To denote a "Label format" address, i.e. an address consisting of up to five address lines of 35 characters where each line is left justified as a specific data element.
                        END
      -- PATIENT USUAL ADDRESS - Used in UNVERIFIED IDENTITY STRUCTURE.   
      ,HOMEADD1 = CASE
                    -- Only populate if the identity has not been withheld and the NHS number has not been verified.
                    WHEN WithheldIdentityReasonCode IS NULL
                         AND NHSNumberStatusCode <> 'SUCCS' THEN CASE
                                                                   -- No address set to 'Address unknown'
                                                                   WHEN PatientAddress1 IS NULL
                                                                        AND PatientAddress2 IS NULL
                                                                        AND PatientAddress3 IS NULL
                                                                        AND PatientAddress4 IS NULL THEN 'Address unknown'
                                                                   ELSE PatientAddress1
                                                                 END
                  END  
      ,HOMEADD2 = CASE
                    -- Only populate if the identity has not been withheld and the NHS number has not been verified.
                    WHEN WithheldIdentityReasonCode IS NULL
                         AND NHSNumberStatusCode <> 'SUCCS' THEN CASE
                                                                   -- 19/05/2014 TJD Added additional check to make sure
                                                                   --                at least two address elements are populated.
                                                                   -- No address set to 'Address unknown'
                                                                   WHEN (PatientAddress2 IS NULL OR LEN(PatientAddress2) = 0)
                                                                        AND (PatientAddress3 IS NULL OR LEN(PatientAddress3) = 0)
                                                                        AND (PatientAddress4 IS NULL OR LEN(PatientAddress4) = 0) THEN 'Address unknown'
                                                                   ELSE PatientAddress2
                                                                 END
                  END   
      ,HOMEADD3 = CASE
                    -- Only populate if the identity has not been withheld and the NHS number has not been verified.
                    WHEN WithheldIdentityReasonCode IS NULL
                         AND NHSNumberStatusCode <> 'SUCCS' THEN PatientAddress3
                  END    
      ,HOMEADD4 = CASE
                    -- Only populate if the identity has not been withheld and the NHS number has not been verified.
                    WHEN WithheldIdentityReasonCode IS NULL
                         AND NHSNumberStatusCode <> 'SUCCS' THEN PatientAddress4
                  END                                                  
      ,HOMEADD5 = NULL -- Do not have a 5th line of address.
FROM CDS.APCMain 
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
WHERE COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions. 

/**********************************************************************************************
* DATA GROUP: PATIENT CHARACTERISTICS
* Used in the following CDS Types: 120,130,140,150,160 (Unless otherwise specified)
***********************************************************************************************/
UPDATE CDS.APCMain
SET   -- PERSON GENDER CODE CURRENT 
      SEX = COALESCE(LEFT(GenderCode,1),'0') -- 0 - Not known (not recorded).
      -- CARER SUPPORT INDICATOR
      ,CARER_SUPPORT_IND = NULL -- Optional data item, not populated.
      -- ETHNIC CATEGORY
      ,ETHNICOR = COALESCE(LEFT(EthnicOriginCode,2),'99') -- 99 - Not known.
      -- PERSON MARITAL STATUS - Should only be populated for psychiatric specialties.
      ,MARITAL_STATUS = NULL -- Not provided.                        
      -- MENTAL HEALTH ACT LEGAL STATUS CLASSIFICATION CODE (ON ADMISSION)                        
      ,LEGAL_STATUS = CASE 
                        WHEN CDS_TYPE IN ('130','140') THEN LEFT(LegalStatusClassificationOnAdmissionCode,2)
                      END
FROM CDS.APCMain 
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
WHERE COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions. 

/**********************************************************************************************
* DATA GROUP: HOSPITAL PROVIDER SPELL - ADMISSION CHARACTERISTICS
* Used in the following CDS Types: 120,130,140
***********************************************************************************************/
UPDATE CDS.APCMain
SET   -- HOSPITAL PROVIDER SPELL NUMBER 
      PROV_SPELL = CAST(SourceSpellNo AS VARCHAR(12))
      -- ADMINISTRATIVE CATEGORY CODE (ON ADMISSION)
      ,ADMIN_CATGRY = COALESCE(AdminCategoryCode,'99') -- 99 - Not known: a validation error.
      -- PATIENT CLASSIFICATION CODE is derived from the ADMISSION METHOD, INTENDED MANAGEMENT and the duration of stay of the PATIENT.
      ,CLASSPAT = CASE
                    -- Intended management - 1(patient to stay in hospital for at least one night).
                    WHEN ManagementIntentionCode = '1' THEN '1' -- 1 - Ordinary admission.
                    -- Intended management - 2(patient not to stay in hospital overnight) and LoS = 0.
                    WHEN ManagementIntentionCode = '2' 
                         AND DATEDIFF(DAY,AdmissionDate,DischargeDate) = 0 THEN '2' -- 2 - Day case admission. 
                    -- Intended management - 4(patient to be admitted for a planned sequence of admissions with no overnight stay) and LoS less than 24 hours.
                    WHEN ManagementIntentionCode = '4' 
                         AND DATEDIFF(HOUR,AdmissionDate,DischargeDate) < 24 THEN '3' -- 3 - Regular day admission.
                    -- Intended management - 5(patient to be admitted regularly for planned sequence of nights who returns home for the remainder of the 24 hour period and LoS less than 24 hours.
                    WHEN ManagementIntentionCode = '5'  
                         AND DATEDIFF(HOUR,AdmissionDate,DischargeDate) < 24 THEN '4' -- 4 - Regular night admission.      
                    ELSE '1' -- Default 1 - Ordinary admission.      
                  END
      -- ADMISSION METHOD CODE (HOSPITAL PROVIDER SPELL)
      ,ADMIMETH = CASE
                    -- National code 28 (other means) is no longer valid in CDS version 6-2
                    -- At present it is unclear if they correct code will be sent in the iPM extract.
                    WHEN AdmissionMethodCode = '28' THEN '2D' -- Other emergency admission.
                    WHEN AdmissionMethodCode IS NULL THEN '99' -- 99 - Not known: a validation error.
                    ELSE AdmissionMethodCode
                  END
      -- SOURCE OF ADMISSION CODE (HOSPITAL PROVIDER SPELL)
      ,ADMISORC = COALESCE(AdmissionSourceCode,'99') -- 99 - Not known: a validation error.
      -- START DATE (HOSPITAL PROVIDER SPELL)
      ,ADMIDATE = LEFT(CONVERT(VARCHAR,AdmissionDate , 120), 10)
      -- START TIME (HOSPITAL PROVIDER SPELL)
      ,HPS_START_TIME = NULL -- Optional data item, not populated.  New data item in CDS v6-2.
      -- AGE ON ADMISSION
      ,AGE_ON_ADMISSION_DATE = CAST(COALESCE(DATEDIFF(YEAR,DateOfBirth,AdmissionDate),999) AS VARCHAR(3)) -- 999 - Not known i.e. date of birth not known and age cannot be estimated.
      -- AMBULANCE INCIDENT NUMBER
      ,AMBULANCE_INCIDENT_NUMBER = NULL -- Optional data item, not populated. New data item in CDS v6-2.
      -- ORGANISATION CODE (CONVEYING AMBULANCE TRUST)
      ,CONVEYING_AMBULANCE_TRUST_ORG = NULL -- Optional data item, not populated. New data item in CDS v6-2.
FROM CDS.APCMain 
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
WHERE CDS_TYPE IN ('120','130','140') -- Only required for these CDS types.
      AND COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions.

/**********************************************************************************************
* DATA GROUP: HOSPITAL PROVIDER SPELL - DISCHARGE CHARACTERISTICS
* Used in the following CDS Types: 120,130,140
***********************************************************************************************/
UPDATE CDS.APCMain
SET    --DISCHARGE DESTINATION CODE (HOSPITAL PROVIDER SPELL)
       DISDEST = CASE 
                   WHEN DischargeDate IS NULL THEN '98' -- 98 - Not applicable, hospital provider spell not finished at episode end.
                   ELSE COALESCE(LEFT(DischargeDestinationCode,2),'99') -- 99 - Not known: a validation error.
                 END
      -- DISCHARGE METHOD CODE (HOSPITAL PROVIDER SPELL)
      ,DISMETH = CASE 
                   WHEN DischargeDate IS NULL THEN '8' -- 8 - Not applicable, hospital provider spell not finished at episode end.
                   ELSE COALESCE(LEFT(DischargeMethodCode,1),'9') -- 9 - Not known: a validation error.
                 END
      -- DISCHARGE READY DATE (HOSPITAL PROVIDER SPELL)
      ,DISCHARGE_READY_DATE = NULL -- Optional data item, not populated.
      -- DISCHARGE DATE (HOSPITAL PROVIDER SPELL)
      ,DISDATE = LEFT(CONVERT(VARCHAR,DischargeDate , 120), 10)
	  -- DISCHARGE TIME (HOSPITAL PROVIDER SPELL)
	  ,HPS_DISCHARGE_TIME = '' -- Optional data item, not populated. New data item in CDS v6-2.
	  -- DISCHARGED TO HOSPITAL AT HOME SERVICE INDICATOR
	  ,DISCHARGED_TO_HHS_IND = '' -- Optional data item, not populated. New data item in CDS v6-2.
FROM CDS.APCMain 
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
WHERE CDS_TYPE IN ('120','130','140') -- Only required for these CDS types.
      AND COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions.

/**********************************************************************************************
* DATA GROUP: CONSULTANT EPISODE - ACTIVITY CHARACTERISTICS
* Used in the following CDS Types: 120,130,140
***********************************************************************************************/
UPDATE CDS.APCMain
SET    -- EPISODE NUMBER
       EPIORDER = RIGHT('00' + CAST(EpisodeNo AS VARCHAR(2)),2)
      -- LAST EPISODE IN SPELL INDICATOR CODE
      ,LAST_IN_SPELL = CASE LastEpisodeInSpellIndicator
                         WHEN 0 THEN '2' -- The episode is not the last episode in the spell.
                         ELSE '1' -- The episode is the last episode in the spell.
                       END
      --OPERATION STATUS CODE                 
      ,OPER_STATUS = CASE
		               WHEN PrimaryProcedure.ProcedureCode IS NULL THEN '8' -- 8 - Not applicable i.e. no operative procedures performed or intended.
		               ELSE '1' -- 1 - One or more operative procedures carried out.
		             END
	  -- OPCS_PROC_SCHEME is not part of this data group.  Updated here for convenience as already checking the primary procedure.
      ,OPCS_PROC_SCHEME = CASE
                            WHEN PrimaryProcedure.ProcedureCode IS NOT NULL THEN '02' -- 02 - OPCS-4
                            ELSE NULL 
                          END 
      -- NEONATAL LEVEL OF CARE CODE                      
      ,NEONATAL_LEVEL = '' -- Optional data item, not populated.
      -- FIRST REGULAR DAY OR NIGHT ADMISSION CODE
      ,FIRST_REG_ADM = '' -- Optional data item, not populated.
      -- PSYCHIATRIC PATIENT STATUS CODE
      ,ADMISTAT = '8' -- Not applicable: the PATIENT is not receiving admitted patient care under a CONSULTANT in a psychiatric specialty. 
      -- START DATE (EPISODE)
      ,EPISTART = LEFT(CONVERT(VARCHAR,EpisodeStartDate , 120), 10)
	  -- START TIME (EPISODE)
	  ,EPISODE_START_TIME = '' -- Optional data item, not populated. New data item in CDS v6-2.
	  -- END DATE (EPISODE)
	  ,EPIEND = LEFT(CONVERT(VARCHAR,EpisodeEndDate , 120), 10)
      -- END TIME (EPISODE)
	  ,EPISODE_END_TIME = CONVERT(VARCHAR(8),EpisodeEndDate , 108) -- New data item in CDS v6-2.
	  -- AGE AT CDS ACTIVITY DATE
	  ,AGE_AT_CDS_ACTIVITY_DATE = CAST(DATEDIFF(YEAR,DateOfBirth,EpisodeEndDate) AS VARCHAR(3))          
      -- MULTI-PROFESSIONAL OR MULTI-DISCIPLINARY INDICATION CODE (PAYMENT BY RESULTS)	                              
	  ,PBR_MP_OR_MDC_CONS_IND_CODE = '' -- Optional data item, not populated. New data item in CDS v6-2.
	  -- REHABILITATION ASSESSMENT TEAM TYPE
	  ,REHAB_ASSESSMENT_TEAM_TYPE = ''  -- Optional data item, not populated. New data item in CDS v6-2.
FROM CDS.APCMain 
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
     
     -- Link to the first birth for the episode, if applicable.             
     LEFT JOIN CDS.APCStagingBirth
             ON APCStagingMain.EpisodeSourceUniqueID = APCStagingBirth.EpisodeSourceUniqueID
                AND APCStagingBirth.BirthOrder = 1
     
     -- Link to the primary procedure for the episode, if applicable.
     LEFT JOIN CDS.APCStagingProcedure PrimaryProcedure
            ON APCStagingMain.EpisodeSourceUniqueID = PrimaryProcedure.EpisodeSourceUniqueID
               AND PrimaryProcedure.SequenceNumber = 1
WHERE CDS_TYPE IN ('120','130','140') -- Only required for these CDS types.
      AND COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions.               

/**********************************************************************************************
* DATA GROUP: CONSULTANT EPISODE - ACTIVITY CHARACTERISTICS
* Used in the following CDS Types: 150,160
***********************************************************************************************/
UPDATE CDS.APCMain
SET    -- AGE AT CDS ACTIVITY DATE
	   AGE_AT_CDS_ACTIVITY_DATE = CAST(DATEDIFF(YEAR,DateOfBirth,APCStagingBirth.BabyBirthDate) AS VARCHAR(3))	                                     
FROM CDS.APCMain 
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
     
     -- Link to the first birth for the episode, if applicable.             
     LEFT JOIN CDS.APCStagingBirth
             ON APCStagingMain.EpisodeSourceUniqueID = APCStagingBirth.EpisodeSourceUniqueID
                AND APCStagingBirth.BirthOrder = 1
     
     -- Link to the primary procedure for the episode, if applicable.
     LEFT JOIN CDS.APCStagingProcedure PrimaryProcedure
            ON APCStagingMain.EpisodeSourceUniqueID = PrimaryProcedure.EpisodeSourceUniqueID
               AND PrimaryProcedure.SequenceNumber = 1
WHERE CDS_TYPE IN ('150','160') -- Only required for these CDS types.
      AND COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions.  

/**********************************************************************************************
* DATA GROUP: CONSULTANT EPISODE - LENGTH OF STAY ADJUSTMENT
* Used in the following CDS Types: 130
***********************************************************************************************/
UPDATE CDS.APCMain
SET    REHAB_LOS_ADJUSTMENT = NULL -- Optional data item, not populated. New data item in CDS v6-2.
      ,SPC_LOS_ADJUSTMENT = NULL -- Optional data item, not populated. New data item in CDS v6-2.
FROM CDS.APCMain 
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
WHERE CDS_TYPE = '130' -- Only required for this CDS type.
      AND COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions.              

/**********************************************************************************************
* DATA GROUP: CONSULTANT EPISODE- OVERSEAS VISITOR STATUS GROUP
* Used in the following CDS Types: 120,130,140
***********************************************************************************************/
UPDATE CDS.APCMain
SET   -- OVERSEAS VISITOR STATUS CLASSIFICATION
      VISITOR_STATUS_1 = COALESCE(OverseasStatusCode,'8') -- 8 - Not applicable (not an Overseas Visitor).
      -- OVERSEAS VISITOR STATUS START DATE - The Start Date of the OVERSEAS VISITOR STATUS CLASSIFICATION within an ACTIVITY.
      -- Bit of a grey area, so using the spell admission date.
      ,VISITOR_STATUS_START_DATE_1 = LEFT(CONVERT(VARCHAR,AdmissionDate , 120), 10)
      --OVERSEAS VISITOR STATUS END DATE - The End Date of the OVERSEAS VISITOR STATUS CLASSIFICATION within an ACTIVITY.
      -- Bit of a grey area, so using the spell discharge date.
      ,VISITOR_STATUS_END_DATE_1 = LEFT(CONVERT(VARCHAR,DischargeDate , 120), 10)
      
      -- We do not have the data available to track changes to the overseas visitor status, so not populating the remaining items.
      ,VISITOR_STATUS_2 = NULL
      ,VISITOR_STATUS_START_DATE_2 = NULL
      ,VISITOR_STATUS_END_DATE_2 = NULL
      ,VISITOR_STATUS_3 = NULL
      ,VISITOR_STATUS_START_DATE_3 = NULL
      ,VISITOR_STATUS_END_DATE_3 = NULL
      ,VISITOR_STATUS_4 = NULL
      ,VISITOR_STATUS_START_DATE_4 = NULL
      ,VISITOR_STATUS_END_DATE_4 = NULL      
      ,VISITOR_STATUS_5 = NULL
      ,VISITOR_STATUS_START_DATE_5 = NULL
      ,VISITOR_STATUS_END_DATE_5 = NULL
      ,VISITOR_STAT_AT_ACTIVITY_DATE =  COALESCE(OverseasStatusCode,'8') -- 8 - Not applicable (not an Overseas Visitor).
FROM CDS.APCMain 
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
WHERE CDS_TYPE IN ('120','130','140') -- Only required for these CDS types.
      AND COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions.    

/**********************************************************************************************
* DATA GROUP: CONSULTANT EPISODE - SERVICE AGREEMENT DETAILS
* Used in the following CDS Types: 120,130,140,150,160
***********************************************************************************************/
UPDATE CDS.APCMain
SET    -- COMMISSIONING SERIAL NUMBER
       SERIAL_NO = CASE
		             -- 06/10/2014 TJD ADDED - Specialised Commissioning.
		             WHEN PbRExcluded = 1 AND PbRFinalSLACode IN ('Q44','Q44PC','Q46PC','Q46','Q50','7A5HC') THEN '=' + LEFT(PbRExclusionType,5)
                     WHEN PbRExcluded = 1 THEN LEFT(PbRExclusionType,6)
                     ELSE LEFT(PbRFinalSLACode,6)
                   END
      -- NHS SERVICE AGREEMENT LINE NUMBER
      ,CONTRACT_LINE_NO = '' -- Optional data item, not populated. 
      -- PROVIDER REFERENCE NUMBER
      ,PROVIDER_REFERENCE_NUMBER = '' -- Optional data item, not populated. 
      -- COMMISSIONER REFERENCE NUMBER
                  --12345678901234567        
      ,PURCH_REF = '9                ' -- 9 (left justified padded with spaces)  - Not known.
      -- ORGANISATION CODE (CODE OF PROVIDER)
      ,PROVIDER = 'RM200'
      -- ORGANISATION CODE (CODE OF COMMISSIONER) 
      --,PURCHASER = COALESCE(PbRCommissionerCode,'01N') + '00'       -- 06/10/2014 TJD REMOVED - Specialised Commissioning.
      -- 06/10/2014 TJD ADDED - Specialised Commissioning.
      ,PURCHASER = CASE
                     WHEN PbRFinalSLACode IN ('Q44','Q44PC','Q46','Q46PC','Q50') THEN LEFT(PbRFinalSLACode,3)
                     WHEN PbRFinalSLACode = '7A5HC' THEN '7A5'
                     ELSE COALESCE(PbRCommissionerCode,'01N') + '00' 
                   END
FROM CDS.APCMain 
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
WHERE COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions.                 

/**********************************************************************************************
* DATA GROUP: CONSULTANT EPISODE - PERSON GROUP (CONSULTANT)
* Used in the following CDS Types: 120,130,140
***********************************************************************************************/
UPDATE CDS.APCMain
SET    -- CONSULTANT CODE
       CONS_CODE = CASE
                     -- Consultant led - use national code or C9999998 not known code if blank.
                     WHEN ProfessionalCarerType = 'Consultant' AND LEN(NationalConsultantCode) = 8 THEN NationalConsultantCode
                     WHEN ProfessionalCarerType = 'Consultant' AND LEN(NationalConsultantCode) <> 8 THEN 'C9999998'
                     -- Dentist led - use national code or D9999998 not known code if blank.
                     WHEN ProfessionalCarerType = 'Dentist' AND LEN(NationalConsultantCode) = 8 THEN NationalConsultantCode
                     WHEN ProfessionalCarerType = 'Dentist' AND LEN(NationalConsultantCode) <> 8 THEN 'D9999998'
                     -- Midwife led - use national code M9999998.
                     WHEN ProfessionalCarerType = 'Midwife' THEN 'M9999998'
                     -- Nurse led - use national code N9999998.
                     WHEN ProfessionalCarerType IN ( 'Nurse'
                                                    ,'Nurse Practitioner'
                                                    ,'Specialist Nurse Practitioner'
                                                    ,'Specialist Practitioner'
                                                    ,'Clinical Nurse Specialist' ) THEN 'N9999998'
                     ELSE 'H9999998' -- Other health care professional led - use national code H9999998.
                   END
      -- CARE PROFESSIONAL MAIN SPECIALTY CODE - has to be a main specialty code.            
      ,MAINSPEF = LEFT(COALESCE(ConsultantMainSpecialtyCode,EpisodeMainSpecialtyCode),3)
      -- ACTIVITY TREATMENT FUNCTION CODE - has to be a treatment function code.
      ,CONS_MAINSPEF = CASE
                         WHEN LEFT(COALESCE(EpisodeTreatmentFunctionCode,ConsultantTreatmentFunctionCode),3) = '902' THEN '450'  
                         ELSE LEFT(COALESCE(EpisodeTreatmentFunctionCode,ConsultantTreatmentFunctionCode),3)
                       END
      -- LOCAL SUB-SPECIALTY CODE
      ,LOCAL_SUB_SPECIALTY_CODE = NULL -- Optional data item, not populated. New data item in CDS v6-2.
FROM CDS.APCMain 
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
WHERE CDS_TYPE IN ('120','130','140') -- Only required for these CDS types.
      AND COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions.                

/**********************************************************************************************
* DATA GROUP: LOCATION GROUP (AT START OF EPISODE)
* Used in the following CDS Types: 120,130,140
***********************************************************************************************/
UPDATE CDS.APCMain
SET    -- LOCATION CLASS
       LOCATION_CLASS_START = '04'  -- 04 = Health site at the start of Health Care Activity.
      -- SITE CODE (OF TREATMENT)
     -- ,SITECODE_AT_EPISODE_START = 'RM202'  -- RM202 = Wythenshawe Hospital. {REMOVED TJD 18/05/2015}
     ,SITECODE_AT_EPISODE_START = CASE
                                    WHEN ServicePoint.SiteCode = 10004845 THEN 'RM201' -- Withington
                                    ELSE 'RM202' -- Wythenshawe
                                  END -- {ADDED TJD 18/05/2015}
      -- ACTIVITY LOCATION TYPE CODE
      ,LOCATION_TYPE_START = '' -- Optional data item, not populated. 
      -- INTENDED CLINICAL CARE INTENSITY CODE
      ,INT_CC_INTENSITY_START = '' -- Optional data item, not populated. 
      -- INTENDED AGE GROUP
      ,AGE_GRP_INT_START = '' -- Optional data item, not populated. 
      -- SEX OF PATIENTS CODE
      ,SEX_OF_PATIENTS_START = '' -- Optional data item, not populated. 
      -- WARD DAY PERIOD AVAILABILITY CODE
      ,WARD_DAY_PERIOD_AVAIL_START = '' -- Optional data item, not populated. 
      -- WARD NIGHT PERIOD AVAILABILITY CODE
      ,WARD_NIGHT_PERIOD_AVAIL_START = '' -- Optional data item, not populated. 
      -- WARD CODE
      ,WARD_CODE_START = '' -- Optional data item, not populated. 
      -- WARD SECURITY LEVEL
      ,WARD_SECURITY_LEVEL_START = '' -- Optional data item, not populated. 
FROM CDS.APCMain 
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
     LEFT JOIN APC.Encounter ON APCStagingMain.EpisodeSourceUniqueID = Encounter.SourceUniqueID --{ADDED TJD 18/05/2015}
     LEFT JOIN PAS.ServicePoint ON Encounter.StartWardTypeCode = ServicePoint.ServicePointCode --{ADDED TJD 18/05/2015}
WHERE CDS_TYPE IN ('120','130','140') -- Only required for these CDS types.
      AND COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions.                  

/**********************************************************************************************
* DATA GROUP: LOCATION GROUP (AT END OF EPISODE)
* Used in the following CDS Types: 120,130,140
***********************************************************************************************/
UPDATE CDS.APCMain
SET    -- LOCATION CLASS
       LOCATION_CLASS_END = '05' -- 05 = Health site at the end of Health Care Activity.
      -- SITE CODE (OF TREATMENT)
     -- ,SITECODE_AT_EPISODE_END = 'RM202' -- RM202 = Wythenshawe Hospital. {REMOVED TJD 18/05/2015}
     ,SITECODE_AT_EPISODE_END = CASE
                                    WHEN ServicePoint.SiteCode = 10004845 THEN 'RM201' -- Withington
                                    ELSE 'RM202' -- Wythenshawe
                                  END -- {ADDED TJD 18/05/2015}
      -- ACTIVITY LOCATION TYPE CODE
      ,LOCATION_TYPE_END = '' -- Optional data item, not populated. 
      -- INTENDED CLINICAL CARE INTENSITY CODE
      ,INT_CC_INTENSITY_END = '' -- Optional data item, not populated. 
      -- INTENDED AGE GROUP
      ,AGE_GRP_INT_END = '' -- Optional data item, not populated. 
      -- SEX OF PATIENTS CODE
      ,SEX_OF_PATIENTS_END = '' -- Optional data item, not populated. 
      -- WARD DAY PERIOD AVAILABILITY CODE
      ,WARD_DAY_PERIOD_AVAIL_END = '' -- Optional data item, not populated. 
      -- WARD NIGHT PERIOD AVAILABILITY CODE
      ,WARD_NIGHT_PERIOD_AVAIL_END = '' -- Optional data item, not populated. 
      -- WARD CODE
      ,WARD_CODE_END = '' -- Optional data item, not populated. 
      -- WARD SECURITY LEVEL
      ,WARD_SECURITY_LEVEL_END = '' -- Optional data item, not populated. 
FROM CDS.APCMain 
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
     LEFT JOIN APC.Encounter ON APCStagingMain.EpisodeSourceUniqueID = Encounter.SourceUniqueID --{ADDED TJD 18/05/2015}
     LEFT JOIN PAS.ServicePoint ON Encounter.EndWardTypeCode = ServicePoint.ServicePointCode --{ADDED TJD 18/05/2015}
WHERE CDS_TYPE IN ('120','130','140') -- Only required for these CDS types.
      AND COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions.                

/**********************************************************************************************
* DATA GROUP: CONSULTANT EPISODE - CLINICAL DIAGNOSIS GROUP (ICD)
* Used in the following CDS Types: 120,130,140
***********************************************************************************************/
UPDATE CDS.APCMain
SET   -- DIAGNOSIS SCHEME IN USE
      ICD_DIAG_SCHEME = CASE
                            WHEN PrimaryDiagnosis.DiagnosisCode IS NOT NULL THEN '02' -- 02 - ICD-10
                            ELSE NULL 
                          END 
FROM CDS.APCMain 
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
     
     -- Link to the primary diagnosis for the episode, if applicable.
     LEFT JOIN CDS.APCStagingDiagnosis PrimaryDiagnosis
            ON APCStagingMain.EpisodeSourceUniqueID = PrimaryDiagnosis.EpisodeSourceUniqueID
               AND PrimaryDiagnosis.SequenceNumber = 1
WHERE CDS_TYPE IN ('120','130','140') -- Only required for these CDS types.
      AND COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions. 

/**********************************************************************************************
* DATA GROUP: GP REGISTRATION
* Used in the following CDS Types: 120,130,140,150,160
***********************************************************************************************/
UPDATE CDS.APCMain
SET   -- GENERAL MEDICAL PRACTITIONER (SPECIFIED) 
      GPREG = LEFT(COALESCE(GPCodeRegistered,'G9999998'),8) -- G9999998 - GENERAL MEDICAL PRACTITIONER PPD CODE not known
      -- GENERAL MEDICAL PRACTICE CODE (PATIENT REGISTRATION)
      ,PRACREG = LEFT(COALESCE(PracticeCodeEpisodic,PracticeCodeRegistered,'V81999'),12) -- V81999 - GP Practice Code not known.
FROM CDS.APCMain 
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
AND COALESCE(UPDATE_TYPE,0) <> 1; -- Exclude any Net Change deletions.      
             
/**********************************************************************************************
* DATA GROUP: REFERRER
* Used in the following CDS Types: 120,130,140
***********************************************************************************************/
UPDATE CDS.APCMain
SET    -- REFERRER CODE
       REFERRER = CASE
                    WHEN SourceOfReferralCode = '17' THEN 'R9999981' -- National screening programme.
                    WHEN SourceOfReferralCode = '97' THEN 'R9999981' -- Other.
                    WHEN ReferredByConsultantCode IS NULL THEN 'X9999998' -- Not applicable, e.g patient has self-presented or not known.                 
                    WHEN ReferrerType = 'Consultant' AND LEN(ReferrerMainCode) = 8 THEN ReferrerMainCode
                    WHEN ReferrerType = 'Consultant' AND LEN(ReferrerMainCode) <> 8 THEN 'C9999998'
                    WHEN ReferrerType = 'General Practitioner' AND LEN(ReferrerMainCode) = 8 THEN ReferrerMainCode
                    WHEN ReferrerType = 'General Practitioner' AND LEN(ReferrerMainCode) <> 8 THEN 'X9999998'
                    WHEN ReferrerType = 'Dentist' AND LEN(ReferrerMainCode) = 8 THEN ReferrerMainCode                  
                    WHEN ReferrerType = 'Dentist' AND LEN(ReferrerMainCode) <> 8 THEN 'D9999998' 
                    ELSE 'R9999981' -- Referrer other than GMP, GDP or Consultant.                 
                  END
      -- REFERRING ORGANISATION CODE 
      ,REF_ORG = CASE
                   WHEN ReferrerOrganisationCode IS NULL THEN 'X99999' -- Not known.
                   WHEN LEN(ReferrerOrganisationCode) = 3 THEN ReferrerOrganisationCode + '00'
                   ELSE ReferrerOrganisationCode
                 END
      ,DIRECT_ACCESS_REFERRAL_IND = NULL -- Optional data item, not populated. 
FROM CDS.APCMain 
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
WHERE CDS_TYPE IN ('120','130','140') -- Only required for these CDS types.
      AND COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions.               

/**********************************************************************************************
* DATA GROUP: PREGNANCY - ACTIVITY CHARACTERISTICS
* Used in the following CDS Types: 120,140,150,160
***********************************************************************************************/
UPDATE CDS.APCMain
SET    -- NUMBER OF BABIES INDICATION CODE
       NUMBABY = LEFT(CAST(MaternityReporting.dbo.func_GetValueCode(APCStagingBirth.NumberBornThisDelivery) AS CHAR),1)                                                                                                                               
FROM CDS.APCMain 
     
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
     
     LEFT JOIN CDS.APCStagingBirth
             ON APCMain.EpisodeSourceUniqueID = APCStagingBirth.EpisodeSourceUniqueID
                AND APCStagingBirth.BirthOrder = 1 
WHERE CDS_TYPE IN ('120','140','150','160') -- Only required for these CDS types.
      AND COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions.                  

/**********************************************************************************************
* DATA GROUP: ANTENATAL CARE - ACTIVITY CHARACTERISTICS
* Used in the following CDS Types: 120,140,150,160
***********************************************************************************************/
UPDATE CDS.APCMain
SET    -- FIRST ANTENATAL ASSESSMENT DATE
       ANTENATAL_ASSESSDATE = LEFT(CONVERT(VARCHAR,APCStagingBirth.InitialAssessmentDate , 120), 10)                                                                                                                            
FROM CDS.APCMain 
     
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
     
     LEFT JOIN CDS.APCStagingBirth
             ON APCMain.EpisodeSourceUniqueID = APCStagingBirth.EpisodeSourceUniqueID
                AND APCStagingBirth.BirthOrder = 1 
WHERE CDS_TYPE IN ('120','140','150','160') -- Only required for these CDS types.
      AND COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions. 
      
/**********************************************************************************************
* DATA GROUP: ANTENATAL CARE - PERSON GROUP (RESPONSIBLE CLINICIAN)
* Used in the following CDS Types: 120,140,150,160
***********************************************************************************************/
UPDATE CDS.APCMain
SET    -- GENERAL MEDICAL PRACTITIONER (ANTENATAL CARE) 
       ANTENATAL_GP = COALESCE(LEFT(APCStagingBirth.MotherGpCode,8),'')
      -- GENERAL MEDICAL PRACTITIONER PRACTICE (ANTENATAL CARE)
      ,ANTENATAL_GP_PRAC = COALESCE(LEFT(APCStagingBirth.MotherGpPracticeCode,12),'')                                                                                                                          
FROM CDS.APCMain 
     
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
     
     LEFT JOIN CDS.APCStagingBirth
             ON APCMain.EpisodeSourceUniqueID = APCStagingBirth.EpisodeSourceUniqueID
                AND APCStagingBirth.BirthOrder = 1 
WHERE CDS_TYPE IN ('120','140','150','160') -- Only required for these CDS types.
      AND COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions.       

/**********************************************************************************************
* DATA GROUP: ANTENATAL CARE - LOCATION GROUP - DELIVERY PLACE INTENDED
* Used in the following CDS Types: 120,140,150,160
***********************************************************************************************/
UPDATE CDS.APCMain
SET    -- LOCATION CLASS
       LOCATION_CLASS_DELPLCINT = CASE
                                  WHEN APCStagingBirth.IntendedPlaceOfBirth = 'Domestic address[1]' THEN '02' -- 02 - Home.
                                  WHEN APCStagingBirth.IntendedPlaceOfBirth IS NOT NULL THEN '01' -- 01 - Health Site (General Occurrence).
                                END
      -- ACTIVITY LOCATION TYPE CODE
      ,LOCATION_TYPE_DEL_PLC_INT = NULL -- Optional data item, not populated. 
      -- DELIVERY PLACE TYPE CODE (INTENDED)
      ,DELINTEN = CAST(MaternityReporting.dbo.func_GetValueCode(APCStagingBirth.IntendedPlaceOfBirth) AS CHAR)
      -- DELIVERY PLACE CHANGE REASON CODE
      ,DELCHANG = CAST(MaternityReporting.dbo.func_GetValueCode(APCStagingBirth.ReasonForChange)AS CHAR)                                                                                                                    
FROM CDS.APCMain 
     
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
     
     LEFT JOIN CDS.APCStagingBirth
             ON APCMain.EpisodeSourceUniqueID = APCStagingBirth.EpisodeSourceUniqueID
                AND APCStagingBirth.BirthOrder = 1 
WHERE CDS_TYPE IN ('120','140','150','160') -- Only required for these CDS types.
      AND COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions.       

/**********************************************************************************************
* DATA GROUP: LABOUR/DELIVERY - ACTIVITY CHARACTERISTICS
* Used in the following CDS Types: 120,140,150,160
***********************************************************************************************/
UPDATE CDS.APCMain
SET    -- ANAESTHETIC GIVEN DURING LABOUR OR DELIVERY CODE
       DELPREAN = CAST(MaternityReporting.dbo.func_GetValueCode(APCStagingBirth.AnalgesiaOrAnaesthesia) AS CHAR)
      -- ANAESTHETIC GIVEN POST LABOUR OR DELIVERY CODE
      ,DELPOSAN = CAST(MaternityReporting.dbo.func_GetValueCode(APCStagingBirth.PostDeliveryAnalgesia) AS CHAR)
      -- GESTATION LENGTH (LABOUR ONSET)
      ,GESTAT = NULL -- Optional data item, not populated.
      -- LABOUR OR DELIVERY ONSET METHOD CODE
      ,DELONSET = CAST(MaternityReporting.dbo.func_GetValueCode(APCStagingBirth.MethodOfLabourOnset)AS CHAR)
      -- DELIVERY DATE
      ,DELDATE = LEFT(CONVERT(VARCHAR,APCStagingBirth.DeliveryDate , 120), 10)                                                                                                    
FROM CDS.APCMain 
     
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
     
     LEFT JOIN CDS.APCStagingBirth
             ON APCMain.EpisodeSourceUniqueID = APCStagingBirth.EpisodeSourceUniqueID
                AND APCStagingBirth.BirthOrder = 1 
WHERE CDS_TYPE IN ('120','140','150','160') -- Only required for these CDS types.
      AND COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions.  

/**********************************************************************************************
* DATA GROUP: DELIVERY CHARACTERISTICS
* Used in the following CDS Types: 140,160
***********************************************************************************************/
UPDATE CDS.APCMain
SET    -- PREGNANCY TOTAL PREVIOUS PREGNANCIES
       NUMPREG = CASE
                   WHEN APCStagingBirth.NumberOfPastPregnancies BETWEEN 0 AND 29 THEN RIGHT('00' + CAST(APCStagingBirth.NumberOfPastPregnancies AS VARCHAR(2)),2)  
                   ELSE '99' -- 99 - Not Known.
                 END       
FROM CDS.APCMain 
     
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
     
     LEFT JOIN CDS.APCStagingBirth
             ON APCMain.EpisodeSourceUniqueID = APCStagingBirth.EpisodeSourceUniqueID
                AND APCStagingBirth.BirthOrder = 1 
WHERE CDS_TYPE IN ('140','160') -- Only required for these CDS types.
      AND COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions.  


/**********************************************************************************************
* DATA GROUP: DELIVERY OCCURRENCE PERSON IDENTITY - MOTHER
* One of the following data sub-groups must be used: WITHHELD IDENTITY STRUCTURE
*                                                    VERIFIED IDENTITY STRUCTURE
*                                                    UNVERIFIED IDENTITY STRUCTURE
* Used in the following CDS Types: 120,150
***********************************************************************************************/
UPDATE CDS.APCMain
SET    -- WITHHELD IDENTITY REASON - Used in WITHHELD IDENTITY STRUCTURE.
       WITHHELD_IDENTITY_REASON_MOTH = NULL -- There is no information from Evolution relating to this.
      -- NHS NUMBER STATUS INDICATOR CODE (MOTHER) - Used in all sub-groups. 
      ,MOM_NNN_STATUS = CASE
                         -- There is no NHS number status information from Evolution.
                         WHEN APCStagingBirth.MotherNHSNumber IS NOT NULL
                              AND LEN(APCStagingBirth.MotherNHSNumber) > 5 THEN '01' -- 01 - Number present and verified
                         ELSE '07' -- 07 - Number not present and trace not required.                         
                        END
                        
      ,MOM_NHS_NUMBER = LEFT(APCStagingBirth.MotherNHSNumber,10)                   
      ,MOM_PAT_ID = LEFT(APCStagingBirth.MotherHospitalNumber,10)
      -- ORGANISATION CODE (LOCAL PATIENT IDENTIFIER (MOTHER))
      ,MOM_PAT_ID_ORG = 'RM200'

      ,MOM_POSTCODE_OF_USUAL_ADDRESS = COALESCE(LEFT(APCStagingBirth.MotherPostCode,8),'ZZ99 3WZ')

      ,MUMDOB  = LEFT(CONVERT(VARCHAR,APCStagingBirth.MotherBirthDate,120),10)

      ,MOM_USUAL_ADDRESS_LINE_1 = CASE                    
                                    WHEN APCStagingBirth.MotherNHSNumber IS NULL THEN LEFT(APCStagingBirth.MotherAddress1,35)
                                  END
      ,MOM_USUAL_ADDRESS_LINE_2 = CASE
                                    WHEN APCStagingBirth.MotherNHSNumber IS NULL THEN LEFT(APCStagingBirth.MotherAddress2,35) 
                                  END  
      ,MOM_USUAL_ADDRESS_LINE_3 = CASE                    
                                    WHEN APCStagingBirth.MotherNHSNumber IS NULL THEN LEFT(APCStagingBirth.MotherAddress3,35)
                                  END   
      ,MOM_USUAL_ADDRESS_LINE_4 = CASE                    
                                    WHEN APCStagingBirth.MotherNHSNumber IS NULL THEN LEFT(APCStagingBirth.MotherAddress4,35)
                                  END   
      ,MOM_USUAL_ADDRESS_LINE_5 = CASE
                                    WHEN APCStagingBirth.MotherNHSNumber IS NULL THEN LEFT(APCStagingBirth.MotherAddress5,35)
                                  END                                                                                                                                
FROM CDS.APCMain 
     
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
     
     LEFT JOIN CDS.APCStagingBirth
             ON APCMain.EpisodeSourceUniqueID = APCStagingBirth.EpisodeSourceUniqueID
                AND APCStagingBirth.BirthOrder = 1
WHERE CDS_TYPE IN ('120','150') -- Only required for these CDS types.
      AND COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions.                 

/**********************************************************************************************
* DATA GROUP: ELECTIVE ADMISSION LIST ENTRY
* Used in the following CDS Types: 130
***********************************************************************************************/
UPDATE CDS.APCMain
SET    -- DURATION OF ELECTIVE WAIT
       ELECDUR = CASE
                   WHEN AdmissionMethodCode IN ('11','12','13') THEN COALESCE(CAST(ElectiveWaitDuration AS VARCHAR(4)),'9999') -- 9999 - Not known (i.e. no date known for decision to admit): a validation error.
                   ELSE '9998' -- Not applicable. 
                 END
      -- INTENDED MANAGEMENT CODE
      ,MAN_INTENT = CASE
                      WHEN AdmissionMethodCode IN ('11','12','13') THEN LEFT(ManagementIntentionCode,1)
                      ELSE '8' -- Not applicable.  
                    END
      -- DECIDED TO ADMIT DATE
      ,ELECDATE = CASE 
                   WHEN AdmissionMethodCode IN ('11','12','13') THEN LEFT(CONVERT(VARCHAR,COALESCE(DecisionToAdmitDate,DecisionToAdmitDateOriginal) , 120), 10)                   
                 END
      -- EARLIEST REASONABLE OFFER DATE
      ,EARLIEST_REASONABLE_DATE_OFFER = NULL -- Optional data item, not populated. 
FROM CDS.APCMain 
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
WHERE CDS_TYPE = '130' -- Only required for these CDS types.
      AND COALESCE(UPDATE_TYPE,0) <> 1;  -- Exclude any Net Change deletions. 

/**********************************************************************************************
* 
***********************************************************************************************/
UPDATE CDS.APCMain
SET    PRIME_RECIPIENT = CASE
                           -- Use the previously submitted Prime Recipient value, if available.
                           WHEN APCArchiveMain.PRIME_RECIPIENT IS NOT NULL THEN APCArchiveMain.PRIME_RECIPIENT
                           -- Overseas visitor exempt from charges.
                           WHEN OverseasStatusCode IN ('1','2') THEN 'TDH00'
                           -- Check for default codes from Organisation Code (Residence Responsibility) that are not allowed.
                           WHEN APCMain.HA IN ('X98','Q99') THEN '01N00'
                           ELSE APCMain.HA + '00' 
                         END
FROM CDS.APCMain 
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID
     -- Link to the archive of previously submitted weekly CDS datasets.
     -- Required to make sure the Prime Recipient value does not change for amended records.
     LEFT JOIN CDS.APCArchiveMain
             ON APCMain.CDS_ID = APCArchiveMain.CDS_ID
                AND APCArchiveMain.SUS_SENT_DATE = (
                                                    SELECT TOP 1 MAX(SUS_SENT_DATE)
                                                    FROM CDS.APCArchiveMain MaxSentDate
                                                    WHERE MaxSentDate.CDS_ID = APCMain.CDS_ID
                                                   );             
             
UPDATE CDS.APCMain
SET    COPY_RECIPIENT_1 = CASE
                            -- Private patient
                            WHEN AdminCategoryCode = '02' THEN 'VPP00'
                            -- Overseas visitor liable for charges.
                            WHEN OverseasStatusCode IN ('3','4') THEN 'VPP00'
                            WHEN PbRFinalSLACode = '7A5HC' THEN '7A500' -- Added TJD 28/11/2014
                            ELSE PURCHASER
                          END
FROM CDS.APCMain 
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID;

-- Wipe any Copy Recipient 1 if the same as the Prime Recipient.
UPDATE CDS.APCMain
SET    COPY_RECIPIENT_1 = CASE
                            WHEN COPY_RECIPIENT_1 = PRIME_RECIPIENT THEN ''
                            ELSE COPY_RECIPIENT_1
                          END
FROM CDS.APCMain 
     INNER JOIN CDS.APCStagingMain
             ON APCMain.EpisodeSourceUniqueID = APCStagingMain.EpisodeSourceUniqueID;

-- Populate final child file tables from staging data.
EXEC CDS.LoadAPCBirthOccurrence;
EXEC CDS.LoadAPCCriticalCarePeriod;
EXEC CDS.LoadAPCDiagnosis;
EXEC CDS.LoadAPCProcedure;


       





























