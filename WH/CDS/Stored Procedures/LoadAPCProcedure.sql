﻿

/***********************************************************************************
 Description: Load final APC Procedure table from staging data table.
 
 Modification History --------------------------------------------------------------

 Date     Author      Change
 19/03/14 Tim Dean	  Created.

************************************************************************************/

CREATE Procedure [CDS].[LoadAPCProcedure]
AS

SET NOCOUNT ON;

TRUNCATE TABLE CDS.APCProcedure;

INSERT INTO CDS.APCProcedure
            (
              EpisodeSourceUniqueID
             ,CDS_ID
             ,SEQUENCE_NUMBER
             ,[PROC]
             ,PROC_DATE
             ,PRO_REG_ISSUER_CODE_HP
             ,MOCP_PRO_REG_ENTRY_ID_HP
             ,PRO_REG_ISSUER_CODE_RA
             ,RA_PRO_REG_ENTRY_ID_RA
            )
SELECT EpisodeSourceUniqueID
      ,CDS_ID = LEFT('BRM2' + RIGHT( Replicate('0', 14) + CAST(EpisodeSourceUniqueID AS VARCHAR), 14 ), 35)
      ,SEQUENCE_NUMBER = CAST(SequenceNumber AS VARCHAR(4))
      ,[PROC] = ProcedureCode
      ,PROC_DATE = LEFT(CONVERT(VARCHAR,ProcedureDate , 120), 10)
      ,PRO_REG_ISSUER_CODE_HP = '' -- Optional data item, not populated. 
      ,MOCP_PRO_REG_ENTRY_ID_HP = '' -- Optional data item, not populated. 
      ,PRO_REG_ISSUER_CODE_RA = '' -- Optional data item, not populated. 
      ,RA_PRO_REG_ENTRY_ID_RA = '' -- Optional data item, not populated. 
FROM CDS.APCStagingProcedure;
       


