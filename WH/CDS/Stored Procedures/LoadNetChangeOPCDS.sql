﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:	OP CDS Net Change processing.

Notes:		Stored in 

			Takes less than 2 mins to run.

			To work on this you need to refer to the following:
			CDS.BuildOPBaseArdentiaCCG (proc)
			CDS.AmendOPBaseArdentia (proc)
			CDS.OPArdentiaExportCCG (view)
			CDS.LoadOPMain (proc)

			Consider creating a CTE to store the fields where complex transforms
			are going on - i.e. use the CTE as a first stage transform.
			e.g. OverseasStatus, CCGofResidenceCode, PurchaserCodeCCG

			AmendOPBaseArdentia (proc) has lots of updates which have not be coded here yet - in progress.

Versions:
			1.0.0.0 - 09/10/2015 - MT
				Created sproc
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure CDS.LoadNetChangeOPCDS
As

Declare @StartDate date
Declare @EndDate date

-- Find out the earliest activity date that can still be sent to SUS.
Set @StartDate = (
	Select	MIN(src.ActivityStartDate)
	From	CDS.SUSTimetable src
	Where	src.PostRecInclusionDate >= GETDATE()
	)
	
-- Set the latest activity date to be 14 days back to allow coders time to code.
Set @EndDate = DATEADD(d,-14,DW_REPORTING.LIB.fn_GetJustDate(GETDATE()))

Truncate Table CDS.znOP_Appts

;With Stage1 As (
	Select	src.SourceUniqueID
		--Replace the following with either new commissioning fields from source or the relevant function call when ready
		,OverseasStatus = 'tbc'
		,CCGResi = 'tbc'
		,CCGHA = 'tbc'
		,Provider = 'tbc'
		,Purchaser = 'tbc'
		,CommSerialNo = 'tbc'
		,PrimeRecipient = 'tbc'
		,CopyRecipient1 = 'tbc'
		,CopyRecipient2 = 'tbc'
		,ConsultantCode = DW_REPORTING.LIB.fn_GetCDSConsultantCode(
			consultant.NationalConsultantCode,
			consultant.ProfessionalCarerType,
			ConsultantSpecialty.NationalSpecialtyCode,
			ConsSpecMapping.XrefEntityCode)
		,MainSpecialtyCode = DW_REPORTING.LIB.fn_GetCDSMainSpecialtyCode(
			TxFuncMapping.XrefEntityCode,
			TreatmentFunction.NationalSpecialtyCode,
			ConsultantSpecialty.NationalSpecialtyCode)
		,TreatmentFunctionCode = DW_REPORTING.LIB.fn_GetCDSTreatmentFunctionCode(
			TxFuncMapping.XrefEntityCode,
			TreatmentFunction.NationalSpecialtyCode,
			src.ClinicCode,
			exc174.[Action])
		
	From	OP.Encounter src
	
		Left Join PAS.Consultant consultant
			On src.ConsultantCode = consultant.ConsultantCode

		Left Join PAS.Specialty ConsultantSpecialty
			On consultant.MainSpecialtyCode = ConsultantSpecialty.SpecialtyCode

		Left Join PAS.Specialty TreatmentFunction
			On src.SpecialtyCode = TreatmentFunction.SpecialtyCode

		Left Join PAS.ReferenceValue ovs
			on src.OverseasStatusFlag = ovs.ReferenceValueCode

		Left Join OrganisationCCG.dbo.[Extended Postcode] pcod
			On src.Postcode = pcod.Postcode

		Left Join dbo.EntityXref ConsSpecMapping
			On ConsSpecMapping.XrefEntityTypeCode = 'SPECIALTYCODE'
			And ConsSpecMapping.EntityCode =
			Coalesce(
				Left(ConsultantSpecialty.NationalSpecialtyCode, 3)
				,Left(TreatmentFunction.NationalSpecialtyCode, 3)
			)

		Left Join dbo.EntityXref TxFuncMapping
			On TxFuncMapping.XrefEntityTypeCode = 'TREATMENTFUNCTIONCODE'
			And TxFuncMapping.EntityCode =
			coalesce(
					 Left(TreatmentFunction.NationalSpecialtyCode, 3)
					,Left(ConsultantSpecialty.NationalSpecialtyCode,3)
				)

		Left Join CDS.OPExcludedClinics exc174
			On src.ClinicCode = exc174.ClinicCode
			And exc174.[Action] = '174'

	Where	src.AppointmentDate Between @StartDate And @EndDate
	),
	SensitiveDiagnosis As (
	Select	Distinct src.OPSourceUniqueID
	From	OP.Diagnosis src

			Inner Join EntityLookup el
				On el.EntityTypeCode = 'SENSITIVEDIAGNOSIS'
				And src.DiagnosisCode = el.EntityCode
	),
	SensitiveProcedure As (
	Select	Distinct src.OPSourceUniqueID
	From	OP.Operation src

			Inner Join EntityLookup el
				On el.EntityTypeCode = 'SENSITIVEOPERATION'
				And src.OperationCode = el.EntityCode
	),
	Stage2 As (
	Select	src.SourceUniqueID,
		Case
			When sd.OPSourceUniqueID Is Null
			And sp.OPSourceUniqueID Is Null
			And wip.SourcePatientNo Is Null
			Then src.NHSNumber
		End As NHSNumber,
		Case
			When sd.OPSourceUniqueID Is Not Null Then '07'
			When sp.OPSourceUniqueID Is Not Null Then '07'
			When wip.SourcePatientNo Is Not Null Then '07'
			When src.NHSNumber Is Null Then '04'
			When src.NHSNumberStatusCode = 'SUCCS' Then '01'
			When src.NHSNumberStatusCode = 'RESET' Then '02'
			When src.NHSNumberStatusCode Is Null Then '01'
			Else src.NHSNumberStatusCode
		End As NHSNumberStatus,
		Case
			When sd.OPSourceUniqueID Is Not Null Then 'N'
			When sp.OPSourceUniqueID Is Not Null Then 'N'
			When wip.SourcePatientNo Is Not Null Then 'N'
			When src.NHSNumber Is Null Then 'Y'
			When src.NHSNumberStatusCode = 'SUCCS' Then 'N'
			When src.NHSNumberStatusCode = 'RESET' Then 'Y'
			When src.NHSNumberStatusCode Is Null Then 'N'
			When src.NHSNumberStatusCode = '01' Then 'N'
			Else 'Y'
		End As SendDemographicsFlag,
		Case
			When sd.OPSourceUniqueID Is Null
			And sp.OPSourceUniqueID Is Null
			And wip.SourcePatientNo Is Null
			Then src.Postcode
		End As Postcode

	From	OP.Encounter src
	
		Left Join SensitiveDiagnosis sd
			On src.SourceUniqueID = sd.OPSourceUniqueID

		Left Join SensitiveProcedure sp
			On src.SourceUniqueID = sp.OPSourceUniqueID

		Left Join WithheldIdentityPatient wip
			On src.SourcePatientNo = wip.SourcePatientNo

	Where	src.AppointmentDate Between @StartDate And @EndDate
	),
	ApptsInOrder As (
	Select	src.SourceUniqueID,
		src.SourcePatientNo,
		ROW_NUMBER() Over (Partition By src.SourcePatientNo Order By src.AppointmentTime, src.SourceUniqueID) As RowNum
	From	OP.Encounter src
	Where	src.AppointmentDate >= @StartDate
	),
	DiagnosisScheme As (
	Select	Distinct src.DGPRO_REFNO As SourceUniqueID
	From	PAS.ClinicalCodingBase src
	Where	SORCE_CODE = 'SCHDL'
		And DPTYP_CODE = 'DIAGN'
		And ARCHV_FLAG = 'N'
	),
	ProcedureScheme As (
	Select	Distinct src.DGPRO_REFNO As SourceUniqueID
	From	PAS.ClinicalCodingBase src
	Where	SORCE_CODE = 'SCHDL'
		And DPTYP_CODE = 'PROCE'
		And ARCHV_FLAG = 'N'
	),
	LastDNACanc As (
	Select	src.SourceUniqueID,
		dna.AppointmentDate,
		ROW_NUMBER() Over (Partition By src.SourceUniqueID Order By dna.AppointmentTime Desc) As RowNum
		From	OP.Encounter src

		Inner Join OP.Encounter dna
			On src.ReferralSourceUniqueID = dna.ReferralSourceUniqueID
			And dna.DNACode In ('2','3')
			And src.AppointmentTime > dna.AppointmentTime

	Where	src.AppointmentDate Between @StartDate And @EndDate
	)
/*
Insert Into CDS.znOP_Appts(
	[PRIME_RECIPIENT]
	,[COPY_RECIPIENT_1]
	,[COPY_RECIPIENT_2]
	,[COPY_RECIPIENT_3]
	,[COPY_RECIPIENT_4]
	,[COPY_RECIPIENT_5]
	,[SENDER]
	,[CDS_GROUP]
	,[CDS_TYPE]
	,[CDS_ID]
	,[UPDATE_TYPE]
	,[PROTOCOL_IDENTIFIER]
	,[BULKSTART]
	,[BULKEND]
	,[PROVIDER]
	,[PURCHASER]
	,[SERIAL_NO]
	,[CONT_LINE_NO]
	,[PURCHREF]
	,[NHS_NO]
	,[NHS_NO_STATUS]
	,[NAME_FORMAT]
	,[ADDRESS_FORMAT]
	,[NAME]
	,[FORENAME]
	,[HOMEADD1]
	,[HOMEADD2]
	,[HOMEADD3]
	,[HOMEADD4]
	,[HOMEADD5]
	,[POSTCODE]
	,[HA]
	,[WITHHELD_IDENTITY_REASON]
	,[SEX]
	,[CAR_SUP_IND]
	,[DOB]
	,[GPREG]
	,[REGPRACT]
	,[LOCPATID]
	,[REFERRER]
	,[REF_ORG]
	,[SERV_TYPE]
	,[REQDATE]
	,[PRIORITY]
	,[REFSRCE]
	,[MAINSPEF]
	,[CON_SPEC]
	,[LOC_SPEC]
	,[CCODE]
	,[LOCAL_SUB_SPECIALTY_CODE]
	,[ATT_IDENT]
	,[CATEGORY]
	,[LOCATION_CLASS]
	,[SITECODE]
	,[LOCATION_TYPE]
	,[CLINIC_CODE]
	,[MED_STAFF]
	,[ATTDATE]
	,[ARRIVAL_TIME]
	,[EXPECTED_APPOINTMENT_DURATION]
	,[ACTIVITY_DATE]
	,[AGE_AT_CDS_ACTIVITY_DATE]
	,[VISITOR_STAT_AT_ACTIVITY_DATE]
	,[EARLIEST_REASONABLE_DATE_OFFER]
	,[EARLIEST_CLINICALLY_APPRO_DATE]
	,[CONSULTATION_MEDIUM_USED]
	,[PBR_MP_OR_MDC_CONS_IND_CODE]
	,[REHAB_ASSESSMENT_TEAM_TYPE]
	,[FIRSTATT]
	,[ATTENDED]
	,[OUTCOME]
	,[LASTDNADATE]
	,[OPSTATUS]
	,[PROVIDER_REF_NO]
	,[LOCAL_PATIENT_ID_ORG]
	,[UBRN]
	,[CARE_PATHWAY_ID]
	,[CARE_PATHWAY_ID_ORG]
	,[REF_TO_TREAT_PERIOD_STATUS]
	,[REF_TO_TREAT_PERIOD_START_DATE]
	,[REF_TO_TREAT_PERIOD_END_DATE]
	,[WAIT_TIME_MEASUREMENT_TYPE]
	,[ETHNIC_CATEGORY]
	,[DIRECT_ACCESS_REFERRAL_IND]
	,[ICD_DIAG_SCHEME]
	,[READ_DIAG_SCHEME]
	,[OPCS_PROC_SCHEME]
	,[READ_PROC_SCHEME]
	)
*/
Select	
	[PRIME_RECIPIENT] = st1.PrimeRecipient
	,[COPY_RECIPIENT_1] = st1.CopyRecipient1
	,[COPY_RECIPIENT_2] = st1.CopyRecipient2
	,[COPY_RECIPIENT_3] = Null
	,[COPY_RECIPIENT_4] = Null
	,[COPY_RECIPIENT_5] = Null
	,[SENDER] = 'RM2'
	,[CDS_GROUP] = '060'
	,[CDS_TYPE] = '020'				
	,[CDS_ID] = 'BRM2' + DW_REPORTING.LIB.fn_PadStringFromFront(Cast(src.SourceUniqueID as varchar),'0',14)
	,[UPDATE_TYPE] = '?'
	,[PROTOCOL_IDENTIFIER] = '020' --Check
	,[BULKSTART] = Null
	,[BULKEND] = Null
	,[PROVIDER] = 'RM2'
	,[PURCHASER] = st1.Purchaser
	,[SERIAL_NO] = st1.CommSerialNo
	,[CONT_LINE_NO] = Null
	,[PURCHREF] = '9'
	,[NHS_NO] = st2.NHSNumber
	,[NHS_NO_STATUS] = st2.NHSNumberStatus
	,[NAME_FORMAT] = '1' --check
	,[ADDRESS_FORMAT] = '1' --check
	,[NAME] =
		Case
			When st2.SendDemographicsFlag = 'Y' Then src.PatientSurname
		End
	,[FORENAME] =
		Case
			When st2.SendDemographicsFlag = 'Y' Then src.PatientForename
		End
	,[HOMEADD1] = 
		Case
			When st2.SendDemographicsFlag = 'Y' Then Coalesce(src.PatientAddress1,'Unknown Address')
		End
	,[HOMEADD2] =
		Case
			When st2.SendDemographicsFlag = 'Y' Then Coalesce(src.PatientAddress2,'Unknown Address')
		End
	,[HOMEADD3] =
		Case
			When st2.SendDemographicsFlag = 'Y' Then Coalesce(src.PatientAddress3,'Unknown Address')
		End
	,[HOMEADD4] =
		Case
			When st2.SendDemographicsFlag = 'Y' Then Coalesce(src.PatientAddress4,'Unknown Address')
		End
	,[HOMEADD5] = Null
	,[POSTCODE] = st2.Postcode
	,[HA] = st1.CCGHA
	,[WITHHELD_IDENTITY_REASON] =
		Case 
			When wip.SourcePatientNo Is Not Null Then '03' -- Record anonymised at request of PATIENT
			When st2.NHSNumberStatus = '07' Then '97' -- Record anonymised for other reason
		End
	,[SEX] = sex.MappedCode
	,[CAR_SUP_IND] = Null
	,[DOB] = src.DateOfBirth
	,[GPREG] = gp.ProfessionalCarerMainCode
	,[REGPRACT] = IPMPractice.OrganisationLocalCode
	,[LOCPATID] = src.DistrictNo
	,[REFERRER] = DW_REPORTING.LIB.fn_GetCDSReferrerCode(SourceOfReferral.MappedCode,Referrer.ProfessionalCarerType,Referrer.ProfessionalCarerMainCode)
	,[REF_ORG] = Coalesce(ReferrerOrganisation.OrganisationLocalCode,'X99999')
	,[SERV_TYPE] = ReasonForReferral.MappedCode
	,[REQDATE] = src.ReferralDate
	,[PRIORITY] = PriorityType.MappedCode
	,[REFSRCE] = SourceOfReferral.MappedCode
	,[MAINSPEF] = st1.MainSpecialtyCode
	,[CON_SPEC] = st1.TreatmentFunctionCode
	,[LOC_SPEC] = Null
	,[CCODE] = st1.ConsultantCode
	,[LOCAL_SUB_SPECIALTY_CODE] = Null
	,[ATT_IDENT] = src.SourceUniqueID
	,[CATEGORY] = AdminCategory.MappedCode
	,[LOCATION_CLASS] = '01'
	,[SITECODE] = Case When cl.ChronosArea = 'Withington' Then 'RM201' Else 'RM202' End
	,[LOCATION_TYPE] = Null
	,[CLINIC_CODE] = Null
	,[MED_STAFF] = 
		Case ProfessionalCarerType.MappedCode
			When '01' Then '03'
			When '02' Then '04'
			When '98' Then '08'
			Else '09'
		End
	,[ATTDATE] = src.AppointmentDate
	,[ARRIVAL_TIME] = Null
	,[EXPECTED_APPOINTMENT_DURATION] = Null
	,[ACTIVITY_DATE] = src.AppointmentDate
	,[AGE_AT_CDS_ACTIVITY_DATE] = DW_REPORTING.LIB.fn_AgeAsInteger(src.DateOfBirth,src.AppointmentDate)
	,[VISITOR_STAT_AT_ACTIVITY_DATE] = Null
	,[EARLIEST_REASONABLE_DATE_OFFER] = Null
	,[EARLIEST_CLINICALLY_APPRO_DATE] = Null
	,[CONSULTATION_MEDIUM_USED] = Null
	,[PBR_MP_OR_MDC_CONS_IND_CODE] = Null
	,[REHAB_ASSESSMENT_TEAM_TYPE] = Null
	,[FIRSTATT] = 
		Case
			When FirstAttendance.MappedCode = '1' And ev.SCHDL_REFNO Is Not Null Then '3'
			When FirstAttendance.MappedCode = '2' And ev.SCHDL_REFNO Is Not Null Then '4'
			Else FirstAttendance.MappedCode
		End
	,[ATTENDED] =
		Case
			When src.DNACode = '9' Then ''
			When src.DNACode = '0' Then ''
			Else src.DNACode
		End
	,[OUTCOME] =
		Case
			When OutcomeOfAttendance.MappedCode Is Not Null Then OutcomeOfAttendance.MappedCode
			When Referral.DischargeDate Is Not Null Then '1' -- Discharged
			When ord2.SourceUniqueID Is Not Null Then '2' -- Another appt given
			Else '3' --Appt to be made at a later date
		End
	,[LASTDNADATE] = dna.AppointmentDate
	,[OPSTATUS] =
		Case
			When src.PrimaryOperationCode Is Null Then '8'
			Else '1'
		End
	,[PROVIDER_REF_NO] = Null
	,[LOCAL_PATIENT_ID_ORG] = 'RM2'
	,[UBRN] = Null
	,[CARE_PATHWAY_ID] = 
		Case
			When LEN(src.RTTPathwayID) < 20 Then
			STUFF(src.RTTPathwayID,6,0,Replicate('0',20 - LEN(src.RTTPathwayID)))
			Else src.RTTPathwayID
		End
	,[CARE_PATHWAY_ID_ORG] =
		Case
			When src.RTTPathwayID Is Null Then Null
			When src.RTTCurrentProviderCode Is Not Null Then src.RTTCurrentProviderCode
			Else 'RM2'
		
		End
	,[REF_TO_TREAT_PERIOD_STATUS] =
		Case
			When RTTPeriodStatus.NationalRTTStatusCode = 'NSP' Then '99'
			When RTTPeriodStatus.NationalRTTStatusCode Is Not Null Then RTTPeriodStatus.NationalRTTStatusCode
			When src.RTTPathwayID Is Null Then Null
			When src.RTTStartDate Is Null Then Null
			Else '99'
		End -- Check this logic with TJD
	,[REF_TO_TREAT_PERIOD_START_DATE] = 
		Case
			When src.RTTPathwayID Is Null Then Null
			Else src.RTTStartDate
		End
	,[REF_TO_TREAT_PERIOD_END_DATE] =
		Case
			When src.RTTPathwayID Is Null Then Null
			Else src.RTTEndDate
		End
	,[WAIT_TIME_MEASUREMENT_TYPE] =
		Case
			When RTTPeriodStatus.NationalRTTStatusCode Is Null Then Null
			When Left(st1.ConsultantCode,1) In ('C','D') Then '01' -- Referral To Treatment Period Included In Referral To Treatment Consultant-Led Waiting Times Measurement.
			When Left(st1.ConsultantCode,1) = 'H' Then '02' -- Allied Health Professional Referral To Treatment Measurement.
			Else '09' -- Other Referral To Treatment Measurement Type.
		End
	,[ETHNIC_CATEGORY] = EthnicOrigin.MappedCode
	,[DIRECT_ACCESS_REFERRAL_IND] = Null
	,[ICD_DIAG_SCHEME] = Case When ds.SourceUniqueID Is Not Null Then '02' End --ICD10
	,[READ_DIAG_SCHEME] = Null
	,[OPCS_PROC_SCHEME] = Case When ps.SourceUniqueID Is Not Null Then '02' End --OPCS-4
	,[READ_PROC_SCHEME] = Null

Into	dbo.Test

From	OP.Encounter src

	Left Join Stage1 st1
		On src.SourceUniqueID = st1.SourceUniqueID

	Left Join Stage2 st2
		On src.SourceUniqueID = st2.SourceUniqueID

	Left Join PAS.ServicePointBase spb
		On src.ClinicCode = spb.SPONT_REFNO_CODE
	
		Left Join CRM.ChronosLocations cl
			On spb.HEORG_REFNO_MAIN_IDENT = cl.LocationCode Collate Database_Default

	Left Join PAS.RTTStatus RTTPeriodStatus
		On src.RTTPeriodStatusCode = RTTPeriodStatus.RTTStatusCode

	left join Organisation.dbo.Postcode Postcode
	on	Postcode.Postcode =
			case
			when len(src.Postcode) = 8 then src.Postcode
			else left(src.Postcode, 3) + ' ' + right(src.Postcode, 4) 
			end

	left join OrganisationCCG.dbo.Postcode PostcodeCCG
		on	PostcodeCCG.Postcode =
			case
			when len(src.Postcode) = 8 then src.Postcode
			else left(src.Postcode, 3) + ' ' + right(src.Postcode, 4) 
			end
			
	left join PAS.ReferenceValue Sex
	on    Sex.ReferenceValueCode = src.SexCode

	left join PAS.ReferenceValue EthnicOrigin
	on    EthnicOrigin.ReferenceValueCode = src.EthnicOriginCode

	left join PAS.ReferenceValue MaritalStatus
	on	MaritalStatus.ReferenceValueCode = src.MaritalStatusCode

	left join PAS.ReferenceValue OverseasStatus
	on OverseasStatus.ReferenceValueCode = src.OverseasStatusFlag

	left join PAS.ReferenceValue AdminCategory
	on    AdminCategory.ReferenceValueCode = src.AdminCategoryCode

	left join PAS.ReferenceValue FirstAttendance
	on    FirstAttendance.ReferenceValueCode = src.FirstAttendanceFlag

	left join PAS.ProfessionalCarer
	on    ProfessionalCarer.ProfessionalCarerCode = src.ConsultantCode

	left join PAS.ReferenceValue ProfessionalCarerType
	on	ProfessionalCarerType.ReferenceValueCode = ProfessionalCarer.ProfessionalCarerTypeCode

	left join PAS.ReferenceValue OutcomeOfAttendance
	on	OutcomeOfAttendance.ReferenceValueCode = src.DisposalCode

	left join PAS.Purchaser
	on    Purchaser.PurchaserCode = src.PurchaserCode

	left join PAS.ProfessionalCarer GP
	on    GP.ProfessionalCarerCode = src.RegisteredGpCode

	left join PAS.Organisation IPMPractice
	on	IPMPractice.OrganisationCode = src.EpisodicGpPracticeCode

	left join PAS.Organisation PCT
	on	PCT.OrganisationCode = IPMPractice.ParentOrganisationCode

	left join OrganisationCCG.dbo.Practice ODSPractice
	on ODSPractice.OrganisationCode = IPMPractice.OrganisationLocalCode
		
	left join PAS.ReferenceValue PriorityType
	on	PriorityType.ReferenceValueCode = src.PriorityCode

	Left Join PAS.ReferenceValue SourceOfReferral
		On Case When src.SourceOfReferralCode In ('20','21') Then '97' Else src.SourceOfReferralCode End =
			SourceOfReferral.ReferenceValueCode

	left join PAS.ReferenceValue ReasonForReferral
	on	ReasonForReferral.ReferenceValueCode = src.ReasonForReferralCode

	left join RF.Encounter Referral
	on	Referral.SourceUniqueID = src.ReferralSourceUniqueID

	left join PAS.ProfessionalCarer Referrer
	on    Referrer.ProfessionalCarerCode = Referral.ReferredByConsultantCode

	left join PAS.Organisation ReferrerOrganisation
	on    ReferrerOrganisation.OrganisationCode = Referral.ReferredByOrganisationCode

	left join PbR2014.dbo.PbR PbRDataset
	on PbRDataset.SourceUniqueID = src.SourceUniqueID
	and PbRDataset.DatasetCode in ('ENCOUNTER', 'WARDATT')

	left join dbo.EntityXref PCTMapPurchaser
		on	PCTMapPurchaser.EntityTypeCode = 'OLDPCT'
		and	PCTMapPurchaser.XrefEntityTypeCode = 'NEWPCT'
		and	PCTMapPurchaser.EntityCode = left(Purchaser.PurchaserMainCode, 3)
		
	left join dbo.EntityXref PCTMapPractice
		on	PCTMapPurchaser.EntityTypeCode = 'OLDPCT'
		and	PCTMapPurchaser.XrefEntityTypeCode = 'NEWPCT'
		and	PCTMapPurchaser.EntityCode = left(PCT.OrganisationLocalCode, 3)

	left join PTL.OPLookups CancelledReasonLu 
		on src.ScheduledCancelReasonCode = CancelledReasonLu.ReferenceValueCode
		
	Left Join Lorenzo.dbo.ScheduleEvent ev
		On ev.CMDIA_REFNO = '2005362'
		And src.SourceUniqueID = ev.SCHDL_REFNO
		
	-- Join onto ApptsInOrder to find out if the patient has a future appointment
	Left Join ApptsInOrder ord1
		On src.SourceUniqueID = ord1.SourceUniqueID

		Left Join ApptsInOrder ord2
			On ord1.SourcePatientNo = ord2.SourcePatientNo
			And ord1.RowNum + 1 = ord2.RowNum

	Left Join DiagnosisScheme ds
		On src.SourceUniqueID = ds.SourceUniqueID

	Left Join ProcedureScheme ps
		On src.SourceUniqueID = ps.SourceUniqueID

	Left Join LastDNACanc dna
		On src.SourceUniqueID = dna.SourceUniqueID
		And dna.RowNum = 1

	Left Join CDS.OPExcludedClinics exc
		On src.ClinicCode = exc.ClinicCode
		And exc.[Action] = 'Exclude'
		
	Left Join WithheldIdentityPatient wip
		On src.SourcePatientNo = wip.SourcePatientNo
		
	Left Join Lorenzo.dbo.ExcludedPatient expat
		On src.SourcePatientNo = expat.SourcePatientNo
		
Where	
	src.AppointmentDate Between @StartDate And @EndDate
	And exc.ClinicCode Is Null
	And expat.SourcePatientNo Is Null
	