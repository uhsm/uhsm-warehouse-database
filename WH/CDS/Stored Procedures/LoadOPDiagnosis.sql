﻿



/***********************************************************************************
 Description: Load v6-2 OP Diagnosis table from existing CDS V6-1 extract.
 
 Modification History --------------------------------------------------------------

 Date     Author      Change
 01/01/14 Tim Dean	  Created.

************************************************************************************/

CREATE Procedure [CDS].[LoadOPDiagnosis]
AS

SET NOCOUNT ON;

TRUNCATE TABLE CDS.OP_Diagnosis;

INSERT INTO CDS.OP_Diagnosis
            (CDS_ID
             ,SEQUENCE_NUMBER
             ,DIAG_CODE_ICD
             ,POA_IND_DIAG)
SELECT CDS_ID = CDS_ID
       ,SEQUENCE_NUMBER = '1'
       ,DIAG_CODE_ICD = PRIICD
       ,POA_IND_DIAG = NULL
FROM   CDS.OPArdentiaExportCCG
WHERE  PRIICD IS NOT NULL; 

INSERT INTO CDS.OP_Diagnosis
            (CDS_ID
             ,SEQUENCE_NUMBER
             ,DIAG_CODE_ICD
             ,POA_IND_DIAG)
SELECT CDS_ID = CDS_ID
       ,SEQUENCE_NUMBER = '2'
       ,DIAG_CODE_ICD = FIRSTICD
       ,POA_IND_DIAG = NULL
FROM   CDS.OPArdentiaExportCCG
WHERE  FIRSTICD IS NOT NULL; 


