﻿



/***********************************************************************************
 Description: Load existing OP CDS v6-1 extract into new CDS.OPMain table in
              Wessex V6-2 format.
              
              Executes 
                - CDS.LoadOPDiagnosis
                - CDS.LoadOPProcedure
 
 Modification History --------------------------------------------------------------

 Date     Author      Change
 01/04/14 Tim Dean	  Created.

************************************************************************************/

CREATE Procedure [CDS].[LoadOPMain]
AS

SET NOCOUNT ON;

TRUNCATE TABLE CDS.OPMain;

INSERT INTO CDS.OPMain
            (PRIME_RECIPIENT
             ,COPY_RECIPIENT_1
             ,COPY_RECIPIENT_2
             ,COPY_RECIPIENT_3
             ,COPY_RECIPIENT_4
             ,COPY_RECIPIENT_5
             ,SENDER
             ,CDS_GROUP
             ,CDS_TYPE
             ,CDS_ID
             ,UPDATE_TYPE
             ,PROTOCOL_IDENTIFIER
             ,BULKSTART
             ,BULKEND
             ,DATETIME_CREATED
             ,PROVIDER
             ,PURCHASER
             ,SERIAL_NO
             ,CONT_LINE_NO
             ,PURCHREF
             ,NHS_NO
             ,NHS_NO_STATUS
             ,NAME_FORMAT
             ,ADDRESS_FORMAT
             ,NAME
             ,FORENAME
             ,HOMEADD1
             ,HOMEADD2
             ,HOMEADD3
             ,HOMEADD4
             ,HOMEADD5
             ,POSTCODE
             ,HA
             ,WITHHELD_IDENTITY_REASON
             ,SEX
             ,CAR_SUP_IND
             ,DOB
             ,GPREG
             ,REGPRACT
             ,LOCPATID
             ,REFERRER
             ,REF_ORG
             ,SERV_TYPE
             ,REQDATE
             ,PRIORITY
             ,REFSRCE
             ,MAINSPEF
             ,CON_SPEC
             ,LOC_SPEC
             ,CCODE
             ,LOCAL_SUB_SPECIALTY_CODE
             ,ATT_IDENT
             ,CATEGORY
             ,LOCATION_CLASS
             ,SITECODE
             ,LOCATION_TYPE
             ,CLINIC_CODE
             ,MED_STAFF
             ,ATTDATE
             ,ARRIVAL_TIME
             ,EXPECTED_APPOINTMENT_DURATION
             ,ACTIVITY_DATE
             ,AGE_AT_CDS_ACTIVITY_DATE
             ,VISITOR_STAT_AT_ACTIVITY_DATE
             ,EARLIEST_REASONABLE_DATE_OFFER
             ,EARLIEST_CLINICALLY_APPRO_DATE
             ,CONSULTATION_MEDIUM_USED
             ,PBR_MP_OR_MDC_CONS_IND_CODE
             ,REHAB_ASSESSMENT_TEAM_TYPE
             ,FIRSTATT
             ,ATTENDED
             ,OUTCOME
             ,LASTDNADATE
             ,OPSTATUS
             ,PROVIDER_REF_NO
             ,LOCAL_PATIENT_ID_ORG
             ,UBRN
             ,CARE_PATHWAY_ID
             ,CARE_PATHWAY_ID_ORG
             ,REF_TO_TREAT_PERIOD_STATUS
             ,REF_TO_TREAT_PERIOD_START_DATE
             ,REF_TO_TREAT_PERIOD_END_DATE
             ,WAIT_TIME_MEASUREMENT_TYPE
             ,ETHNIC_CATEGORY
             ,DIRECT_ACCESS_REFERRAL_IND
             ,ICD_DIAG_SCHEME
             ,READ_DIAG_SCHEME
             ,OPCS_PROC_SCHEME
             ,READ_PROC_SCHEME
             )
SELECT PRIME_RECIPIENT = [Prime Recipient]
      ,COPY_RECIPIENT_1 = [Copy Recipient 1]
      ,COPY_RECIPIENT_2 = [Copy Recipient 2]
      ,COPY_RECIPIENT_3 = [Copy Recipient 3]
      ,COPY_RECIPIENT_4 = [Copy Recipient 4]
      ,COPY_RECIPIENT_5 = [Copy Recipient 5] 
      ,SENDER = SENDER
      ,CDS_GROUP = CDS_Group
      ,CDS_TYPE = CDS_Type
      ,CDS_ID = CDS_ID  
      ,UPDATE_TYPE = [Update Type]
      ,PROTOCOL_IDENTIFIER = PROTOCOL_IDENTIFIER
      ,BULKSTART = BULKSTART
      ,BULKEND = BULKEND
      ,DATETIME_CREATED = DATETIME_CREATED
      ,PROVIDER = PROVIDER
      ,PURCHASER = PURCHASER
      ,SERIAL_NO = SERIAL_NO
      ,CONT_LINE_NO = CONT_LINE_NO
      ,PURCHREF = PURCHREF
      ,NHS_NO = NHS_NO
      ,NHS_NO_STATUS = NHS_NO_STATUS
      ,NAME_FORMAT = NAME_FORMAT
      ,ADDRESS_FORMAT = ADDRESS_FORMAT
      ,NAME = NAME
      ,FORENAME = FORENAME
      ,HOMEADD1 = HOMEADD1
      ,HOMEADD2 = HOMEADD2
      ,HOMEADD3 = HOMEADD3
      ,HOMEADD4 = HOMEADD4
      ,HOMEADD5 = HOMEADD5
      ,POSTCODE = POSTCODE
      ,HA = HA
      -- WITHHELD IDENTITY REASON - New item CDS v6-2.
      -- Have to derive the best we can from then NHS NUMBER STATUS INDICATOR CODE set in the v6-1 extract.
      ,WITHHELD_IDENTITY_REASON = CASE
                                    WHEN NHS_NO_STATUS = '07' THEN '97' -- Record anonymised for other reason.
                                  END
      ,SEX = SEX
      ,CAR_SUP_IND = CAR_SUP_IND
      ,DOB = DOB
      ,GPREG = GPREG
      ,REGPRACT = REGPRACT
      ,LOCPATID = LOCPATID
      ,REFERRER = LEFT(REFERRER + '         ',8) -- NOTE REQUIRED HERE!
      ,REF_ORG = REF_ORG
      ,SERV_TYPE = SERV_TYPE
      ,REQDATE = REQDATE
      ,PRIORITY = PRIORITY
      ,REFSRCE = REFSRCE
      ,MAINSPEF = CON_SPEC
      ,CON_SPEC = MAINSPEF --COALESCE(MainSpecialtyXref.XrefEntityCode,MAINSPEF)
      ,LOC_SPEC = LOC_SPEC
      ,CCODE = CCODE
      ,LOCAL_SUB_SPECIALTY_CODE = NULL -- Optional.
      ,ATT_IDENT = ATT_IDENT
      ,CATEGORY = CATEGORY
      ,LOCATION_CLASS = LOCATION_CLASS
      ,SITECODE = SITECODE
      ,LOCATION_TYPE = NULL -- Optional.
      ,CLINIC_CODE = ClinicCode
      ,MED_STAFF = MED_STAFF
      ,ATTDATE = ATTDATE
      ,ARRIVAL_TIME = NULL -- Optional.
      ,EXPECTED_APPOINTMENT_DURATION = NULL -- Optional.
      ,ACTIVITY_DATE = ACTIVITY_DATE
      ,AGE_AT_CDS_ACTIVITY_DATE = AGE_AT_CDS_ACTIVITY_DATE
      ,VISITOR_STAT_AT_ACTIVITY_DATE = NULL -- Optional.
      ,EARLIEST_REASONABLE_DATE_OFFER = EARLIEST_REASONABLE_DATE_OFFER
      ,EARLIEST_CLINICALLY_APPRO_DATE = NULL -- Optional.
      ,CONSULTATION_MEDIUM_USED = NULL -- Optional.
      ,PBR_MP_OR_MDC_CONS_IND_CODE = NULL -- Optional.
      ,REHAB_ASSESSMENT_TEAM_TYPE = NULL -- Optional.
      ,FIRSTATT = FIRSTATT
      ,ATTENDED = ATTENDED
      ,OUTCOME = OUTCOME
      ,LASTDNADATE = LASTDNADATE
      ,OPSTATUS = OPSTATUS
      ,PROVIDER_REF_NO = PROVIDER_REF_NO
      ,LOCAL_PATIENT_ID_ORG = LOCAL_PATIENT_ID_ORG
      ,UBRN = UBRN
      ,CARE_PATHWAY_ID = CARE_PATHWAY_ID
      ,CARE_PATHWAY_ID_ORG = CARE_PATHWAY_ID_ORG
      ,REF_TO_TREAT_PERIOD_STATUS = REF_TO_TREAT_PERIOD_STATUS
      ,REF_TO_TREAT_PERIOD_START_DATE = REF_TO_TREAT_PERIOD_START_DATE
      ,REF_TO_TREAT_PERIOD_END_DATE = REF_TO_TREAT_PERIOD_END_DATE
      -- WAITING TIME MEASUREMENT TYPE - New item in CDS v6-2.
      -- Derive from existing v6-1 CDS item.
      ,WAIT_TIME_MEASUREMENT_TYPE = CASE
                                      WHEN REF_TO_TREAT_PERIOD_STATUS IS NULL THEN NULL
                                      WHEN LEFT(CCODE,1) IN ('C','D') THEN '01' -- Referral To Treatment Period Included In Referral To Treatment Consultant-Led Waiting Times Measurement.
                                      WHEN LEFT(CCODE,1) = 'H' THEN '02' -- Allied Health Professional Referral To Treatment Measurement.
                                      ELSE '09' -- Other Referral To Treatment Measurement Type.
                                    END 
      ,ETHNIC_CATEGORY = ETHNIC_CATEGORY
      ,DIRECT_ACCESS_REFERRAL_IND = NULL -- Optional.
      ,ICD_DIAG_SCHEME = ICD_DIAG_SCHEME
      ,READ_DIAG_SCHEME = READ_DIAG_SCHEME
      ,OPCS_PROC_SCHEME = OPCS_PROC_SCHEME
      ,READ_PROC_SCHEME = READ_PROC_SCHEME
FROM   CDS.OPArdentiaExportCCG
         
       LEFT JOIN dbo.EntityXref MainSpecialtyXref
              ON (MAINSPEF = MainSpecialtyXref.EntityCode
                  AND MainSpecialtyXref.EntityTypeCode = 'SPECIALTYCODE'); 

EXEC CDS.LoadOPDiagnosis;
EXEC CDS.LoadOPProcedure;



UPDATE CDS.OPMain
SET COPY_RECIPIENT_1 = NULL
FROM CDS.OPMain
where PRIME_RECIPIENT = COPY_RECIPIENT_1;



