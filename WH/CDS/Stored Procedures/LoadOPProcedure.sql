﻿


/***********************************************************************************
 Description: Load v6-2 OP Procedure table from existing CDS V6-1 extract.
 
 Modification History --------------------------------------------------------------

 Date     Author      Change
 01/01/14 Tim Dean	  Created.

************************************************************************************/

CREATE Procedure [CDS].[LoadOPProcedure]
AS

SET NOCOUNT ON;

TRUNCATE TABLE CDS.OPProcedure;

INSERT INTO CDS.OPProcedure
            (CDS_ID
             ,SEQUENCE_NUMBER
             ,[PROC]
             ,PROC_DATE
             ,PRO_REG_ISSUER_CODE_HP
             ,MOCP_PRO_REG_ENTRY_ID_HP
             ,PRO_REG_ISSUER_CODE_RA
             ,RA_PRO_REG_ENTRY_ID_RA)
SELECT CDS_ID = CDS_ID
       ,SEQUENCE_NUMBER = '1'
       ,[PROC] = PRIOPCS
       ,PROC_DATE = OPCS_DATE_1
       ,PRO_REG_ISSUER_CODE_HP = NULL
       ,MOCP_PRO_REG_ENTRY_ID_HP = NULL
       ,PRO_REG_ISSUER_CODE_RA = NULL
       ,RA_PRO_REG_ENTRY_ID_RA = NULL
FROM   CDS.OPArdentiaExportCCG
WHERE  PRIOPCS IS NOT NULL; 

INSERT INTO CDS.OPProcedure
            (CDS_ID
             ,SEQUENCE_NUMBER
             ,[PROC]
             ,PROC_DATE
             ,PRO_REG_ISSUER_CODE_HP
             ,MOCP_PRO_REG_ENTRY_ID_HP
             ,PRO_REG_ISSUER_CODE_RA
             ,RA_PRO_REG_ENTRY_ID_RA)
SELECT CDS_ID = CDS_ID
       ,SEQUENCE_NUMBER = '2'
       ,[PROC] = OPCS2
       ,PROC_DATE = OPCS_DATE_2
       ,PRO_REG_ISSUER_CODE_HP = NULL
       ,MOCP_PRO_REG_ENTRY_ID_HP = NULL
       ,PRO_REG_ISSUER_CODE_RA = NULL
       ,RA_PRO_REG_ENTRY_ID_RA = NULL
FROM   CDS.OPArdentiaExportCCG
WHERE  OPCS2 IS NOT NULL; 

INSERT INTO CDS.OPProcedure
            (CDS_ID
             ,SEQUENCE_NUMBER
             ,[PROC]
             ,PROC_DATE
             ,PRO_REG_ISSUER_CODE_HP
             ,MOCP_PRO_REG_ENTRY_ID_HP
             ,PRO_REG_ISSUER_CODE_RA
             ,RA_PRO_REG_ENTRY_ID_RA)
SELECT CDS_ID = CDS_ID
       ,SEQUENCE_NUMBER = '3'
       ,[PROC] = OPCS3
       ,PROC_DATE = OPCS_DATE_3
       ,PRO_REG_ISSUER_CODE_HP = NULL
       ,MOCP_PRO_REG_ENTRY_ID_HP = NULL
       ,PRO_REG_ISSUER_CODE_RA = NULL
       ,RA_PRO_REG_ENTRY_ID_RA = NULL
FROM   CDS.OPArdentiaExportCCG
WHERE  OPCS3 IS NOT NULL; 

INSERT INTO CDS.OPProcedure
            (CDS_ID
             ,SEQUENCE_NUMBER
             ,[PROC]
             ,PROC_DATE
             ,PRO_REG_ISSUER_CODE_HP
             ,MOCP_PRO_REG_ENTRY_ID_HP
             ,PRO_REG_ISSUER_CODE_RA
             ,RA_PRO_REG_ENTRY_ID_RA)
SELECT CDS_ID = CDS_ID
       ,SEQUENCE_NUMBER = '4'
       ,[PROC] = OPCS4
       ,PROC_DATE = OPCS_DATE_4
       ,PRO_REG_ISSUER_CODE_HP = NULL
       ,MOCP_PRO_REG_ENTRY_ID_HP = NULL
       ,PRO_REG_ISSUER_CODE_RA = NULL
       ,RA_PRO_REG_ENTRY_ID_RA = NULL
FROM   CDS.OPArdentiaExportCCG
WHERE  OPCS4 IS NOT NULL; 

INSERT INTO CDS.OPProcedure
            (CDS_ID
             ,SEQUENCE_NUMBER
             ,[PROC]
             ,PROC_DATE
             ,PRO_REG_ISSUER_CODE_HP
             ,MOCP_PRO_REG_ENTRY_ID_HP
             ,PRO_REG_ISSUER_CODE_RA
             ,RA_PRO_REG_ENTRY_ID_RA)
SELECT CDS_ID = CDS_ID
       ,SEQUENCE_NUMBER = '5'
       ,[PROC] = OPCS5
       ,PROC_DATE = OPCS_DATE_5
       ,PRO_REG_ISSUER_CODE_HP = NULL
       ,MOCP_PRO_REG_ENTRY_ID_HP = NULL
       ,PRO_REG_ISSUER_CODE_RA = NULL
       ,RA_PRO_REG_ENTRY_ID_RA = NULL
FROM   CDS.OPArdentiaExportCCG
WHERE  OPCS5 IS NOT NULL; 

INSERT INTO CDS.OPProcedure
            (CDS_ID
             ,SEQUENCE_NUMBER
             ,[PROC]
             ,PROC_DATE
             ,PRO_REG_ISSUER_CODE_HP
             ,MOCP_PRO_REG_ENTRY_ID_HP
             ,PRO_REG_ISSUER_CODE_RA
             ,RA_PRO_REG_ENTRY_ID_RA)
SELECT CDS_ID = CDS_ID
       ,SEQUENCE_NUMBER = '6'
       ,[PROC] = OPCS6
       ,PROC_DATE = OPCS_DATE_6
       ,PRO_REG_ISSUER_CODE_HP = NULL
       ,MOCP_PRO_REG_ENTRY_ID_HP = NULL
       ,PRO_REG_ISSUER_CODE_RA = NULL
       ,RA_PRO_REG_ENTRY_ID_RA = NULL
FROM   CDS.OPArdentiaExportCCG
WHERE  OPCS6 IS NOT NULL; 

INSERT INTO CDS.OPProcedure
            (CDS_ID
             ,SEQUENCE_NUMBER
             ,[PROC]
             ,PROC_DATE
             ,PRO_REG_ISSUER_CODE_HP
             ,MOCP_PRO_REG_ENTRY_ID_HP
             ,PRO_REG_ISSUER_CODE_RA
             ,RA_PRO_REG_ENTRY_ID_RA)
SELECT CDS_ID = CDS_ID
       ,SEQUENCE_NUMBER = '7'
       ,[PROC] = OPCS7
       ,PROC_DATE = OPCS_DATE_7
       ,PRO_REG_ISSUER_CODE_HP = NULL
       ,MOCP_PRO_REG_ENTRY_ID_HP = NULL
       ,PRO_REG_ISSUER_CODE_RA = NULL
       ,RA_PRO_REG_ENTRY_ID_RA = NULL
FROM   CDS.OPArdentiaExportCCG
WHERE  OPCS7 IS NOT NULL;

INSERT INTO CDS.OPProcedure
            (CDS_ID
             ,SEQUENCE_NUMBER
             ,[PROC]
             ,PROC_DATE
             ,PRO_REG_ISSUER_CODE_HP
             ,MOCP_PRO_REG_ENTRY_ID_HP
             ,PRO_REG_ISSUER_CODE_RA
             ,RA_PRO_REG_ENTRY_ID_RA)
SELECT CDS_ID = CDS_ID
       ,SEQUENCE_NUMBER = '8'
       ,[PROC] = OPCS8
       ,PROC_DATE = OPCS_DATE_8
       ,PRO_REG_ISSUER_CODE_HP = NULL
       ,MOCP_PRO_REG_ENTRY_ID_HP = NULL
       ,PRO_REG_ISSUER_CODE_RA = NULL
       ,RA_PRO_REG_ENTRY_ID_RA = NULL
FROM   CDS.OPArdentiaExportCCG
WHERE  OPCS8 IS NOT NULL;  

INSERT INTO CDS.OPProcedure
            (CDS_ID
             ,SEQUENCE_NUMBER
             ,[PROC]
             ,PROC_DATE
             ,PRO_REG_ISSUER_CODE_HP
             ,MOCP_PRO_REG_ENTRY_ID_HP
             ,PRO_REG_ISSUER_CODE_RA
             ,RA_PRO_REG_ENTRY_ID_RA)
SELECT CDS_ID = CDS_ID
       ,SEQUENCE_NUMBER = '9'
       ,[PROC] = OPCS9
       ,PROC_DATE = OPCS_DATE_9
       ,PRO_REG_ISSUER_CODE_HP = NULL
       ,MOCP_PRO_REG_ENTRY_ID_HP = NULL
       ,PRO_REG_ISSUER_CODE_RA = NULL
       ,RA_PRO_REG_ENTRY_ID_RA = NULL
FROM   CDS.OPArdentiaExportCCG
WHERE  OPCS9 IS NOT NULL; 

INSERT INTO CDS.OPProcedure
            (CDS_ID
             ,SEQUENCE_NUMBER
             ,[PROC]
             ,PROC_DATE
             ,PRO_REG_ISSUER_CODE_HP
             ,MOCP_PRO_REG_ENTRY_ID_HP
             ,PRO_REG_ISSUER_CODE_RA
             ,RA_PRO_REG_ENTRY_ID_RA)
SELECT CDS_ID = CDS_ID
       ,SEQUENCE_NUMBER = '10'
       ,[PROC] = OPCS10
       ,PROC_DATE = OPCS_DATE_10
       ,PRO_REG_ISSUER_CODE_HP = NULL
       ,MOCP_PRO_REG_ENTRY_ID_HP = NULL
       ,PRO_REG_ISSUER_CODE_RA = NULL
       ,RA_PRO_REG_ENTRY_ID_RA = NULL
FROM   CDS.OPArdentiaExportCCG
WHERE  OPCS10 IS NOT NULL; 

INSERT INTO CDS.OPProcedure
            (CDS_ID
             ,SEQUENCE_NUMBER
             ,[PROC]
             ,PROC_DATE
             ,PRO_REG_ISSUER_CODE_HP
             ,MOCP_PRO_REG_ENTRY_ID_HP
             ,PRO_REG_ISSUER_CODE_RA
             ,RA_PRO_REG_ENTRY_ID_RA)
SELECT CDS_ID = CDS_ID
       ,SEQUENCE_NUMBER = '11'
       ,[PROC] = OPCS11
       ,PROC_DATE = OPCS_DATE_11
       ,PRO_REG_ISSUER_CODE_HP = NULL
       ,MOCP_PRO_REG_ENTRY_ID_HP = NULL
       ,PRO_REG_ISSUER_CODE_RA = NULL
       ,RA_PRO_REG_ENTRY_ID_RA = NULL
FROM   CDS.OPArdentiaExportCCG
WHERE  OPCS11 IS NOT NULL; 

INSERT INTO CDS.OPProcedure
            (CDS_ID
             ,SEQUENCE_NUMBER
             ,[PROC]
             ,PROC_DATE
             ,PRO_REG_ISSUER_CODE_HP
             ,MOCP_PRO_REG_ENTRY_ID_HP
             ,PRO_REG_ISSUER_CODE_RA
             ,RA_PRO_REG_ENTRY_ID_RA)
SELECT CDS_ID = CDS_ID
       ,SEQUENCE_NUMBER = '12'
       ,[PROC] = OPCS12
       ,PROC_DATE = OPCS_DATE_12
       ,PRO_REG_ISSUER_CODE_HP = NULL
       ,MOCP_PRO_REG_ENTRY_ID_HP = NULL
       ,PRO_REG_ISSUER_CODE_RA = NULL
       ,RA_PRO_REG_ENTRY_ID_RA = NULL
FROM   CDS.OPArdentiaExportCCG
WHERE  OPCS12 IS NOT NULL; 
       



