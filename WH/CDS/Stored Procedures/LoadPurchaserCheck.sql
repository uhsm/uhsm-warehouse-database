﻿
CREATE procedure [CDS].[LoadPurchaserCheck] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

----APC---------------------------------------


--drop table CDS.PurchaserCheckBase
truncate table CDS.PurchaserCheckBase
insert into CDS.PurchaserCheckBase
select 
	--base.UniqueEpisodeSerialNo
	enc.SourceUniqueID
	,LocalPatientID = enc.DistrictNo
	,EncounterDate = enc.EpisodeStartTime
	,EndDate = enc.DischargeDate
	,IPMPurchaser = Purchaser.PurchaserMainCode
	,RegisteredPractice = enc.RegisteredGpPracticeCode
	,IPMPracCodeEp = IPMPractice.OrganisationLocalCode
	,IPMParent = PCT.OrganisationLocalCode
	,PCTofPracticeCode = PCT.OrganisationLocalCode
	,CCGofPracticeCode = coalesce(
		ODSPractice.ParentOrganisationCode
		,PCT.OrganisationLocalCode)
	--,cdsPostcode = base.Postcode
	,encPostcode = enc.Postcode
	,Postcode.DistrictOfResidence
	,OverseasStatus = OverseasStatus.MappedCode
	--,base.AdministrationCategoryCode
	--,base.PostcodeAmended
	
	,PCTofResidenceCode = 
		case when OverseasStatus.MappedCode in
			(
			 1	-- Exempt (reciprocal agreement)
			,2	-- Exempt from payment - other
			,3	-- To pay hotel fees only
			,4	-- To pay all fees
			,9	-- Charging rate not known
			)
		then 'X98'

		--For Scotland, NI and CI (NOT WALES) use DistrictOfResidence as lookup instead of PCTCode
		when left(Postcode.DistrictOfResidence,1) in ('S','Z','Y') then Postcode.DistrictOfResidence
		
		else
			--Lookup to ODS postcode_to_PCT table
			Postcode.PCTCode
		end

	,CCGofResidenceCode = 
		case when OverseasStatus.MappedCode in
			(
			 1	-- Exempt (reciprocal agreement)
			,2	-- Exempt from payment - other
			,3	-- To pay hotel fees only
			,4	-- To pay all fees
			,9	-- Charging rate not known
			)
		then 'X98'

		--For Scotland, NI and CI (NOT WALES) use DistrictOfResidence as lookup instead of PCTCode
		when left(PostcodeCCG.DistrictOfResidence,1) in ('S','Z','Y') then PostcodeCCG.DistrictOfResidence
		
		else
			--Lookup to ODS postcode_to_PCT table
			PostcodeCCG.CCGCode
		end
	
	--NOTE THAT NULL GPPRACTICE RETURNS 'SHA'
	,PurchaserCode =
		case 
			when OverseasStatus.MappedCode in (1,2)
				then '5NT00'	--OS - Exempt from charges
			when OverseasStatus.MappedCode in (3,4)
				then 'VPP00'	--OS - Liable for charges
			when enc.Postcode like 'IM%'
				then 'YAC00'	--IOM patients don't seem to have overseas status code, so hard-code YAC here to
								--avoid purchaser being pulled from iPM Purchaser (usually TDH00)
		else
			coalesce(
				left(PCTMapPurchaser.XRefEntityCode, 3) + '00'
				,left(Purchaser.PurchaserMainCode + '00', 5)
				,left(PCTMapPractice.XRefEntityCode, 3) + '00'
				,left(PCT.OrganisationLocalCode + '00', 5)
			)
		end
			
	,PurchaserCodeCCG =
		case 
			when OverseasStatus.MappedCode in (1,2)
				then '01N00'	--OS - Exempt from charges
			when OverseasStatus.MappedCode in (3,4)
				then 'VPP00'	--OS - Liable for charges
			when enc.Postcode like 'IM%'
				then 'YAC00'	--IOM patients don't seem to have overseas status code, so hard-code YAC here to
								--avoid purchaser being pulled from iPM Purchaser (usually TDH00)
			--IPM purchaser data is not correct, so use CCG of practice, unless
			--non English, private or overseas patient
			when 
				(left(
					coalesce(
						Purchaser.PurchaserMainCode
						,ODSPractice.ParentOrganisationCode
						,PCT.OrganisationLocalCode)
					, 1) in ('Z','Y','V','S','7') or
				left(Purchaser.PurchaserMainCode, 2) = 'TD')
				then 
					coalesce(
						left(Purchaser.PurchaserMainCode + '00', 5)
						,left(ODSPractice.ParentOrganisationCode + '00', 5)
						,left(PCT.OrganisationLocalCode + '00', 5)
					)
		else
			left(ODSPractice.ParentOrganisationCode + '00', 5)
		end
	,Attended = 'N/A'

--into CDS.PurchaserCheckBase
from APC.Encounter enc
--inner join APC.Encounter enc on enc.EncounterRecno = base.EncounterRecno

left join PAS.ReferenceValue OverseasStatus
	on OverseasStatus.ReferenceValueCode = enc.OverseasStatusFlag

left join Organisation.dbo.Postcode Postcode
	on	Postcode.Postcode =
		case
		when len(enc.Postcode) = 8 then enc.Postcode
		else left(enc.Postcode, 3) + ' ' + right(enc.Postcode, 4) 
		end

left join OrganisationCCG.dbo.Postcode PostcodeCCG
	on	PostcodeCCG.Postcode =
		case
		when len(enc.Postcode) = 8 then enc.Postcode
		else left(enc.Postcode, 3) + ' ' + right(enc.Postcode, 4) 
		end
		
left join PAS.Purchaser
	on    Purchaser.PurchaserCode = enc.PurchaserCode

left join PAS.Organisation IPMPractice
	on	IPMPractice.OrganisationCode = enc.EpisodicGpPracticeCode
	
left join PAS.Organisation PCT
	on	PCT.OrganisationCode = IPMPractice.ParentOrganisationCode
	
left join OrganisationCCG.dbo.Practice ODSPractice
	on ODSPractice.OrganisationCode = IPMPractice.OrganisationLocalCode
	
left join dbo.EntityXref PCTMapPurchaser
	on	PCTMapPurchaser.EntityTypeCode = 'OLDPCT'
	and	PCTMapPurchaser.XrefEntityTypeCode = 'NEWPCT'
	and	PCTMapPurchaser.EntityCode = left(Purchaser.PurchaserMainCode, 3)
	
left join dbo.EntityXref PCTMapPractice
	on	PCTMapPurchaser.EntityTypeCode = 'OLDPCT'
	and	PCTMapPurchaser.XrefEntityTypeCode = 'NEWPCT'
	and	PCTMapPurchaser.EntityCode = left(PCT.OrganisationLocalCode, 3)
--where UniqueEpisodeSerialNo = 'BRM200000188168188'
where enc.DischargeDate > '2012-03-31 00:00:00'


--drop table CDS.PurchaserCheckExport
truncate table CDS.PurchaserCheckExport
insert into CDS.PurchaserCheckExport
select 
	ActivityType
	,SourceUniqueID
	,LocalPatientID
	,EncounterDate
	,EndDate
	,PostcodeSource = encPostcode
	--,PostcodeCDS = cdsPostcode
	,DistrictOfResidence
	,OverseasStatus
	,PCTofResidence = PCTRESBASE
	,CCGofResidence = CCGRESBASE
	,PCTofResidenceCDS = PCTRES
	,CCGofResidenceCDS = CCGRES
	,GPPracticeCode = Practice
	,PCTofPractice = PCTPRACT
	,CCGofPractice = CCGPRACT
	,IPMPurchaser
	,PURCHASERPCTBASE
	,PCTPurchaser = PURCHASERPCT
	,PURCHASERCCGBASE
	,PracticePCTMatch = case when left(PURCHASERPCT,3) = left(PCTPRACT,3) then 1 else 0 end
	,PostcodePCTMatch = case when left(PURCHASERPCT,3) = left(PCTRESBASE,3) then 1 else 0 end
	,CCGPurchaser = PURCHASERCCG
	,PCTPrimeRecip = PRIMERECIPIENTPCT
	,CCGPrimeRecip = PRIMERECIPIENTCCG
	,Attended
--into CDS.PurchaserCheckExport
FROM 
  (
select
	ActivityType = 'APC'
	,SourceUniqueID
	,LocalPatientID
	,EncounterDate
	,EndDate
	--,cdsPostcode
	,encPostcode
	,DistrictOfResidence
	,OverseasStatus
	,PCTRESBASE = PCTofResidenceCode
	,CCGRESBASE = CCGofResidenceCode
	,PCTRES = 
		case 		
			--Where no Postcode or GP then organisation code for unknown practice 'V81999' maps to 'SHA'
			--Change this to 'Q99'
			when PCTofResidenceCode = 'SHA' then 'Q99'
			--Wales, Scotland, NI, CI
			when left(PCTofResidenceCode, 1) in ('7','S','Y', 'Z', '6') then 'X98'
			else PCTofResidenceCode
		end

	,CCGRES = 
		case 		
			--Where no Postcode or GP then organisation code for unknown practice 'V81999' maps to 'SHA'
			--Change this to 'Q99'
			when CCGofResidenceCode = 'SHA' then 'Q99'
			--Wales, Scotland, NI, CI
			when left(CCGofResidenceCode, 1) in ('7','S','Y', 'Z', '6') then 'X98'
			else CCGofResidenceCode
		end
		
	,Practice = IPMPracCodeEp
	,PCTPRACT = PCTofPracticeCode
	,CCGPRACT = CCGofPracticeCode
	,IPMPurchaser

	,PURCHASERPCTBASE = PurchaserCode
	-- Use purchaser from purchaser table or practice lookup* [PurchaserCodeNew] (1), 
	-- else use PCT of residence code (map X98 to 5NT first) (2),
	-- else use 5NT (3).
	-- * Unless = SHA00 (which has been generated from null practice lookup) then skip to value (2)
	,PURCHASERPCT = 
		case when PurchaserCode = 'SHA00'
			then Coalesce(
			case when PCTofResidenceCode = 'X98' then '5NT00' end
			,left(PCTofResidenceCode + '00', 5)
			,'5NT00')
		else Coalesce(
			PurchaserCode
			,case when PCTofResidenceCode = 'X98' then '5NT00' end
			,left(PCTofResidenceCode + '00', 5)
			,'5NT00')
		end
					

	,PURCHASERCCGBASE = PurchaserCodeCCG
	-- Use purchaser from purchaser table or practice lookup* [PurchaserCodeNew] (1), 
	-- else use PCT of residence code (map X98 to 5NT first) (2),
	-- else use 5NT (3).
	-- * Unless = SHA00 (which has been generated from null practice lookup) then skip to value (2)
	,PURCHASERCCG = 
		case when PurchaserCodeCCG = 'SHA00'
			then Coalesce(
			case when CCGofResidenceCode = 'X98' then '01N00' end
			,left(CCGofResidenceCode + '00', 5)
			,'01N00')
		else Coalesce(
			PurchaserCodeCCG
			,case when CCGofResidenceCode = 'X98' then '01N00' end
			,left(CCGofResidenceCode + '00', 5)
			,'01N00')
		end
		
	--,PracticePCTMatch = case when left(ex.PURCHASERPCT,3) = left(ex.PCTPRACT,3) then 1 else 0 end
	--,PostcodePCTMatch = case when left(ex.PURCHASERPCT,3) = left(ex.PCTRESBASE,3) then 1 else 0 end
	,PRIMERECIPIENTPCT =
		case
		when OverseasStatus in ('1','2') then 'TDH00'
		--when OverseasStatus in ('3','4','9') then 'VPP00'
		when left(PCTofResidenceCode, 1) = 'Y' then 'TDH00'
		when PCTofResidenceCode in ('X98', 'Q99', 'SHA') then 
			coalesce(
				case when PurchaserCode = 'SHA00' then '5NT00' else PurchaserCode end
				, '5NT00'
				)
		else
			coalesce(
				 left(PCTofResidenceCode, 3) + '00'
				,case when PurchaserCode = 'SHA00' then '5NT00' else PurchaserCode end
				,'5NT00'
				)
		end

	,PRIMERECIPIENTCCG =
		case
		when OverseasStatus in ('1','2') then 'TDH00'
		--when OverseasStatus in ('3','4','9') then 'VPP00'
		when left(CCGofResidenceCode, 1) = 'Y' then 'TDH00'
		when CCGofResidenceCode in ('X98', 'Q99', 'SHA') then 
			coalesce(
				--case when PurchaserCodeCCG = 'SHA00' then '01N00' else PurchaserCodeCCG end
				PurchaserCodeCCG 
				, '01N00'
				)
		else
			coalesce(
				 left(CCGofResidenceCode, 3) + '00'
				--,case when PurchaserCodeCCG = 'SHA00' then '01N00' else PurchaserCodeCCG end
				,PurchaserCodeCCG 
				,'01N00'
				)
		end
	,Attended
from CDS.PurchaserCheckBase 
) base

select @RowsInserted = @@rowcount
select @Stats = 
	'APC records loaded  = ' + CONVERT(varchar(10), @RowsInserted) 
--OP--------------------------------------------------------------------------------
------------------------------------------------------------------------------------

--drop table CDS.PurchaserCheckBase
truncate table CDS.PurchaserCheckBase
insert into CDS.PurchaserCheckBase
select 
	--base.UniqueEpisodeSerialNo
	enc.SourceUniqueID
	,LocalPatientID = enc.DistrictNo
	,EncounterDate = enc.AppointmentTime
	,EndDate = enc.AppointmentDate
	,IPMPurchaser = Purchaser.PurchaserMainCode
	,RegisteredPractice = enc.RegisteredGpPracticeCode
	,IPMPracCodeEp = IPMPractice.OrganisationLocalCode
	,IPMParent = PCT.OrganisationLocalCode
	,PCTofPracticeCode = PCT.OrganisationLocalCode
	,CCGofPracticeCode = coalesce(
		ODSPractice.ParentOrganisationCode
		,PCT.OrganisationLocalCode)
	--,cdsPostcode = base.Postcode
	,encPostcode = enc.Postcode
	,Postcode.DistrictOfResidence
	,OverseasStatus = OverseasStatus.MappedCode
	--,base.AdministrationCategoryCode
	--,base.PostcodeAmended

	,PCTofResidenceCode = 
		case when OverseasStatus.MappedCode in
			(
			 1	-- Exempt (reciprocal agreement)
			,2	-- Exempt from payment - other
			,3	-- To pay hotel fees only
			,4	-- To pay all fees
			,9	-- Charging rate not known
			)
		then 'X98'

		--For Scotland, NI and CI (NOT WALES) use DistrictOfResidence as lookup instead of PCTCode
		when left(Postcode.DistrictOfResidence,1) in ('S','Z','Y') then Postcode.DistrictOfResidence
		
		else
			--Lookup to ODS postcode_to_PCT table
			Postcode.PCTCode
		end

	,CCGofResidenceCode = 
		case when OverseasStatus.MappedCode in
			(
			 1	-- Exempt (reciprocal agreement)
			,2	-- Exempt from payment - other
			,3	-- To pay hotel fees only
			,4	-- To pay all fees
			,9	-- Charging rate not known
			)
		then 'X98'

		--For Scotland, NI and CI (NOT WALES) use DistrictOfResidence as lookup instead of PCTCode
		when left(PostcodeCCG.DistrictOfResidence,1) in ('S','Z','Y') then PostcodeCCG.DistrictOfResidence
		
		else
			--Lookup to ODS postcode_to_PCT table
			PostcodeCCG.CCGCode
		end
	
	--NOTE THAT NULL GPPRACTICE RETURNS 'SHA'
	,PurchaserCode =
		case 
			when OverseasStatus.MappedCode in (1,2)
				then '5NT00'	--OS - Exempt from charges
			when OverseasStatus.MappedCode in (3,4)
				then 'VPP00'	--OS - Liable for charges
			when enc.Postcode like 'IM%'
				then 'YAC00'	--IOM patients don't seem to have overseas status code, so hard-code YAC here to
								--avoid purchaser being pulled from iPM Purchaser (usually TDH00)
		else
			coalesce(
				left(PCTMapPurchaser.XRefEntityCode, 3) + '00'
				,left(Purchaser.PurchaserMainCode + '00', 5)
				,left(PCTMapPractice.XRefEntityCode, 3) + '00'
				,left(PCT.OrganisationLocalCode + '00', 5)
			)
		end
			
	,PurchaserCodeCCG =
		case 
			when OverseasStatus.MappedCode in (1,2)
				then '01N00'	--OS - Exempt from charges
			when OverseasStatus.MappedCode in (3,4)
				then 'VPP00'	--OS - Liable for charges
			when enc.Postcode like 'IM%'
				then 'YAC00'	--IOM patients don't seem to have overseas status code, so hard-code YAC here to
								--avoid purchaser being pulled from iPM Purchaser (usually TDH00)
			--IPM purchaser data is not correct, so use CCG of practice, unless
			--non English, private or overseas patient
			when 
				(left(
					coalesce(
						Purchaser.PurchaserMainCode
						,ODSPractice.ParentOrganisationCode
						,PCT.OrganisationLocalCode)
					, 1) in ('Z','Y','V','S','7') or
				left(Purchaser.PurchaserMainCode, 2) = 'TD')
				then 
					coalesce(
						left(Purchaser.PurchaserMainCode + '00', 5)
						,left(ODSPractice.ParentOrganisationCode + '00', 5)
						,left(PCT.OrganisationLocalCode + '00', 5)
					)
		else
			left(ODSPractice.ParentOrganisationCode + '00', 5)
		end
	,Attended = 
		case 
			when enc.DNACode = '5' then 'Attended'
			when enc.DNACode = '6' then 'Attended'
			when enc.DNACode = '0' then 'Unknown'
			else 'Not attended'
		end
--into CDS.PurchaserCheckBase
from OP.Encounter enc
--inner join APC.Encounter enc on enc.EncounterRecno = base.EncounterRecno

left join PAS.ReferenceValue OverseasStatus
	on OverseasStatus.ReferenceValueCode = enc.OverseasStatusFlag

left join Organisation.dbo.Postcode Postcode
	on	Postcode.Postcode =
		case
		when len(enc.Postcode) = 8 then enc.Postcode
		else left(enc.Postcode, 3) + ' ' + right(enc.Postcode, 4) 
		end

left join OrganisationCCG.dbo.Postcode PostcodeCCG
	on	PostcodeCCG.Postcode =
		case
		when len(enc.Postcode) = 8 then enc.Postcode
		else left(enc.Postcode, 3) + ' ' + right(enc.Postcode, 4) 
		end
		
left join PAS.Purchaser
	on    Purchaser.PurchaserCode = enc.PurchaserCode

left join PAS.Organisation IPMPractice
	on	IPMPractice.OrganisationCode = enc.EpisodicGpPracticeCode
	
left join PAS.Organisation PCT
	on	PCT.OrganisationCode = IPMPractice.ParentOrganisationCode
	
left join OrganisationCCG.dbo.Practice ODSPractice
	on ODSPractice.OrganisationCode = IPMPractice.OrganisationLocalCode
	
left join dbo.EntityXref PCTMapPurchaser
	on	PCTMapPurchaser.EntityTypeCode = 'OLDPCT'
	and	PCTMapPurchaser.XrefEntityTypeCode = 'NEWPCT'
	and	PCTMapPurchaser.EntityCode = left(Purchaser.PurchaserMainCode, 3)
	
left join dbo.EntityXref PCTMapPractice
	on	PCTMapPurchaser.EntityTypeCode = 'OLDPCT'
	and	PCTMapPurchaser.XrefEntityTypeCode = 'NEWPCT'
	and	PCTMapPurchaser.EntityCode = left(PCT.OrganisationLocalCode, 3)
--where UniqueEpisodeSerialNo = 'BRM200000188168188'
where enc.AppointmentDate > '2012-03-31 00:00:00'
and enc.AppointmentDate < dateadd(day, -1, getdate())


--drop table CDS.PurchaserCheckExport
--truncate table CDS.PurchaserCheckExport
insert into CDS.PurchaserCheckExport
select 
	ActivityType
	,SourceUniqueID
	,LocalPatientID
	,EncounterDate
	,EndDate
	,PostcodeSource = encPostcode
	--,PostcodeCDS = cdsPostcode
	,DistrictOfResidence
	,OverseasStatus
	,PCTofResidence = PCTRESBASE
	,CCGofResidence = CCGRESBASE
	,PCTofResidenceCDS = PCTRES
	,CCGofResidenceCDS = CCGRES
	,GPPracticeCode = Practice
	,PCTofPractice = PCTPRACT
	,CCGofPractice = CCGPRACT
	,IPMPurchaser
	,PURCHASERPCTBASE
	,PCTPurchaser = PURCHASERPCT
	,PURCHASERCCGBASE
	,PracticePCTMatch = case when left(PURCHASERPCT,3) = left(PCTPRACT,3) then 1 else 0 end
	,PostcodePCTMatch = case when left(PURCHASERPCT,3) = left(PCTRESBASE,3) then 1 else 0 end
	,CCGPurchaser = PURCHASERCCG
	,PCTPrimeRecip = PRIMERECIPIENTPCT
	,CCGPrimeRecip = PRIMERECIPIENTCCG
	,Attended
--into CDS.PurchaserCheckExport
FROM 
  (
select
	ActivityType = 'OP'
	,SourceUniqueID
	,LocalPatientID
	,EncounterDate
	,EndDate
	--,cdsPostcode
	,encPostcode
	,DistrictOfResidence
	,OverseasStatus
	,PCTRESBASE = PCTofResidenceCode
	,CCGRESBASE = CCGofResidenceCode
	,PCTRES = 
		case 		
			--Where no Postcode or GP then organisation code for unknown practice 'V81999' maps to 'SHA'
			--Change this to 'Q99'
			when PCTofResidenceCode = 'SHA' then 'Q99'
			--Wales, Scotland, NI, CI
			when left(PCTofResidenceCode, 1) in ('7','S','Y', 'Z', '6') then 'X98'
			else PCTofResidenceCode
		end

	,CCGRES = 
		case 		
			--Where no Postcode or GP then organisation code for unknown practice 'V81999' maps to 'SHA'
			--Change this to 'Q99'
			when CCGofResidenceCode = 'SHA' then 'Q99'
			--Wales, Scotland, NI, CI
			when left(CCGofResidenceCode, 1) in ('7','S','Y', 'Z', '6') then 'X98'
			else CCGofResidenceCode
		end
		
	,Practice = IPMPracCodeEp
	,PCTPRACT = PCTofPracticeCode
	,CCGPRACT = CCGofPracticeCode
	,IPMPurchaser

	,PURCHASERPCTBASE = PurchaserCode
	-- Use purchaser from purchaser table or practice lookup* [PurchaserCodeNew] (1), 
	-- else use PCT of residence code (map X98 to 5NT first) (2),
	-- else use 5NT (3).
	-- * Unless = SHA00 (which has been generated from null practice lookup) then skip to value (2)
	,PURCHASERPCT = 
		case when PurchaserCode = 'SHA00'
			then Coalesce(
			case when PCTofResidenceCode = 'X98' then '5NT00' end
			,left(PCTofResidenceCode + '00', 5)
			,'5NT00')
		else Coalesce(
			PurchaserCode
			,case when PCTofResidenceCode = 'X98' then '5NT00' end
			,left(PCTofResidenceCode + '00', 5)
			,'5NT00')
		end
					

	,PURCHASERCCGBASE = PurchaserCodeCCG
	-- Use purchaser from purchaser table or practice lookup* [PurchaserCodeNew] (1), 
	-- else use PCT of residence code (map X98 to 5NT first) (2),
	-- else use 5NT (3).
	-- * Unless = SHA00 (which has been generated from null practice lookup) then skip to value (2)
	,PURCHASERCCG = 
		case when PurchaserCodeCCG = 'SHA00'
			then Coalesce(
			case when CCGofResidenceCode = 'X98' then '01N00' end
			,left(CCGofResidenceCode + '00', 5)
			,'01N00')
		else Coalesce(
			PurchaserCodeCCG
			,case when CCGofResidenceCode = 'X98' then '01N00' end
			,left(CCGofResidenceCode + '00', 5)
			,'01N00')
		end
		
	--,PracticePCTMatch = case when left(ex.PURCHASERPCT,3) = left(ex.PCTPRACT,3) then 1 else 0 end
	--,PostcodePCTMatch = case when left(ex.PURCHASERPCT,3) = left(ex.PCTRESBASE,3) then 1 else 0 end
	,PRIMERECIPIENTPCT =
		case
		when OverseasStatus in ('1','2') then 'TDH00'
		--when OverseasStatus in ('3','4','9') then 'VPP00'
		when left(PCTofResidenceCode, 1) = 'Y' then 'TDH00'
		when PCTofResidenceCode in ('X98', 'Q99', 'SHA') then 
			coalesce(
				case when PurchaserCode = 'SHA00' then '5NT00' else PurchaserCode end
				, '5NT00'
				)
		else
			coalesce(
				 left(PCTofResidenceCode, 3) + '00'
				,case when PurchaserCode = 'SHA00' then '5NT00' else PurchaserCode end
				,'5NT00'
				)
		end

	,PRIMERECIPIENTCCG =
		case
		when OverseasStatus in ('1','2') then 'TDH00'
		--when OverseasStatus in ('3','4','9') then 'VPP00'
		when left(CCGofResidenceCode, 1) = 'Y' then 'TDH00'
		when CCGofResidenceCode in ('X98', 'Q99', 'SHA') then 
			coalesce(
				--case when PurchaserCodeCCG = 'SHA00' then '01N00' else PurchaserCodeCCG end
				PurchaserCodeCCG 
				, '01N00'
				)
		else
			coalesce(
				 left(CCGofResidenceCode, 3) + '00'
				--,case when PurchaserCodeCCG = 'SHA00' then '01N00' else PurchaserCodeCCG end
				,PurchaserCodeCCG 
				,'01N00'
				)
		end
	,Attended
from CDS.PurchaserCheckBase 
) base

select @RowsInserted = @@rowcount

select @Stats = @Stats + 
	'. OP records loaded = ' + CONVERT(varchar(10), @RowsInserted) 
	
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = @Stats + 
	'. Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'CDS - WH LoadPurchaserCheck', @Stats, @StartTime