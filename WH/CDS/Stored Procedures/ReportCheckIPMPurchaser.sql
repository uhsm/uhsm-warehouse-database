﻿
CREATE procedure [CDS].[ReportCheckIPMPurchaser] as (
select distinct
	ex.ActivityType
	,ex.SourceUniqueID
	,ex.LocalPatientID
	,ex.EncounterDate
	,EncounterDateStr = convert(varchar(10),ex.EncounterDate,103) + ' ' + convert(varchar(5),ex.EncounterDate,108)
	,ex.EndDate
	,PostcodeStart = substring (ex.PostcodeSource, 0, CHARINDEX(CHAR(32), ex.PostcodeSource))
	,ex.GPPracticeCode
	,OverseasStatusDescription = ovsvs.ReferenceValue
	,ex.PCTofResidence
	,ex.CCGofResidence
	,ex.PCTofPractice
	,ex.CCGofPractice
	,ex.IPMPurchaser
	,ex.PCTPurchaser
	,ex.CCGPurchaser
	,ex.PCTPrimeRecip
	,ex.CCGPrimeRecip
	,ex.Attended
from CDS.PurchaserCheckExport ex
left join PAS.ReferenceValue ovsvs
on ovsvs.MainCode = ex.OverseasStatus
and ovsvs.ReferenceDomainCode = 'OVSVS'
where ex.PracticePCTMatch = 0 and ex.PostcodePCTMatch = 0
and ex.CCGOfResidenceCDS <> 'X98'
and LEFT(isnull(ex.IPMPurchaser,'xx'), 1) = '5'
and ex.EndDate > '2013-03-31 00:00:00'
--order by ex.LocalPatientID
)
