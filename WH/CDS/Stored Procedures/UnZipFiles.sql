﻿





/***********************************************************************************
 Description: UnZip files - Unzip all the files contained within an Ardentia 
              NetTransform processing zip file.
  
 Parameters:  @Source - Full path and name of the source zip file to be unzipped.
              @Target - Full path to the folder to unzip the files to.   
              
 Modification History --------------------------------------------------------------

 Date     Author      Change
 06/06/14 Tim Dean	  Created.

************************************************************************************/

CREATE Procedure [CDS].[UnZipFiles]
 @Source VARCHAR(200)
,@Target VARCHAR(200)
AS

DECLARE @ZipProgram VARCHAR(100) = 'C:\Progra~1\7-Zip\7z x ';  -- Installed on the server.
DECLARE @Cmd VARCHAR(1000)
DECLARE @Res INT;

SET @Cmd = @ZipProgram + '"' + @Source + '" -o"' + @Target + '"'

EXEC @RES = MASTER.DBO.XP_CMDSHELL @Cmd;




