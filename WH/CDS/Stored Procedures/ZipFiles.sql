﻿


/***********************************************************************************
 Description: Zip files - Ardentia NetTransform require main files and all
              supplementary files to be contain within a single zip file for processing
 
 Parameters:  @Source - Full path to source files to be zipped.
              @Target - Full path and name of target zip file.      
              
 Modification History --------------------------------------------------------------

 Date     Author      Change
 04/04/14 Tim Dean	  Created.

************************************************************************************/

CREATE Procedure [CDS].[ZipFiles]
 @Source VARCHAR(200)
,@Target VARCHAR(200)
AS

DECLARE @ZipProgram VARCHAR(100) = 'C:\Progra~1\7-Zip\7z a -tzip ';  -- Installed on the server.
DECLARE @Cmd VARCHAR(1000)
DECLARE @Res INT;

SET @Cmd = @ZipProgram + '"' + @Target + '" "' + @Source + '"'

EXEC @RES = MASTER.DBO.XP_CMDSHELL @Cmd;

