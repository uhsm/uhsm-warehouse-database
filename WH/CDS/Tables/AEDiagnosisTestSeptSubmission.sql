﻿CREATE TABLE [CDS].[AEDiagnosisTestSeptSubmission] (
    [CDS_ID]          VARCHAR (35) NOT NULL,
    [SEQUENCE_NUMBER] VARCHAR (4)  NOT NULL,
    [AE_DIAG]         VARCHAR (6)  NOT NULL
);

