﻿CREATE TABLE [CDS].[AEInvestigationsTestSeptSubmission] (
    [CDS_ID]          VARCHAR (35) NOT NULL,
    [SEQUENCE_NUMBER] VARCHAR (4)  NOT NULL,
    [AE_INVEST]       VARCHAR (6)  NOT NULL
);

