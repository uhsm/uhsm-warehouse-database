﻿CREATE TABLE [CDS].[AEMain_KPMG] (
    [CDS_ID]                     VARCHAR (35) NOT NULL,
    [Prov_No]                    VARCHAR (12) NULL,
    [CCG_Code]                   VARCHAR (12) NULL,
    [Pat_Sex]                    CHAR (1)     NULL,
    [Pat_Age]                    VARCHAR (3)  NULL,
    [GP_Code]                    VARCHAR (8)  NULL,
    [Practice_Code]              VARCHAR (12) NULL,
    [Pat_ID]                     VARCHAR (10) NULL,
    [Attendance_Number]          VARCHAR (12) NULL,
    [Attendance_Category]        VARCHAR (2)  NULL,
    [Arrival_Date]               VARCHAR (10) NULL,
    [Source_Of_Referal]          VARCHAR (2)  NULL,
    [Arrival_Mode]               CHAR (1)     NULL,
    [Arrival_Time]               VARCHAR (8)  NULL,
    [Patient_Group]              VARCHAR (2)  NULL,
    [Incident_Loc]               VARCHAR (2)  NULL,
    [Initial_Assessment_Time]    VARCHAR (8)  NULL,
    [Time_Seen_For_Treatment]    VARCHAR (8)  NULL,
    [Investigation_01]           VARCHAR (6)  NULL,
    [Investigation_02]           VARCHAR (6)  NULL,
    [Investigation_03]           VARCHAR (6)  NULL,
    [Investigation_04]           VARCHAR (6)  NULL,
    [Investigation_05]           VARCHAR (6)  NULL,
    [Investigation_06]           VARCHAR (6)  NULL,
    [Investigation_07]           VARCHAR (6)  NULL,
    [Investigation_08]           VARCHAR (6)  NULL,
    [Investigation_09]           VARCHAR (6)  NULL,
    [Investigation_10]           VARCHAR (6)  NULL,
    [Investigation_11]           VARCHAR (6)  NULL,
    [Investigation_12]           VARCHAR (6)  NULL,
    [Diagnosis_01]               VARCHAR (6)  NULL,
    [Diagnosis_02]               VARCHAR (6)  NULL,
    [Diagnosis_03]               VARCHAR (6)  NULL,
    [Treatment_01]               VARCHAR (6)  NULL,
    [Treatment_02]               VARCHAR (6)  NULL,
    [Treatment_03]               VARCHAR (6)  NULL,
    [Treatment_04]               VARCHAR (6)  NULL,
    [Treatment_05]               VARCHAR (6)  NULL,
    [Treatment_06]               VARCHAR (6)  NULL,
    [Treatment_07]               VARCHAR (6)  NULL,
    [Treatment_08]               VARCHAR (6)  NULL,
    [Treatment_09]               VARCHAR (6)  NULL,
    [Treatment_10]               VARCHAR (6)  NULL,
    [Treatment_11]               VARCHAR (6)  NULL,
    [Treatment_12]               VARCHAR (6)  NULL,
    [Attendance_Conclusion_Time] VARCHAR (8)  NULL,
    [Departure_Time]             VARCHAR (8)  NULL,
    [Disposal]                   VARCHAR (2)  NULL,
    [HRG]                        VARCHAR (10) NULL,
    [Trf]                        INT          NULL,
    [Local_Tariff_Ind]           INT          NULL,
    [NHSNo]                      VARCHAR (10) NULL
);

