﻿CREATE TABLE [CDS].[APCArchiveDiagnosis] (
    [EpisodeSourceUniqueID] INT          NOT NULL,
    [CDS_ID]                VARCHAR (35) NOT NULL,
    [SEQUENCE_NUMBER]       VARCHAR (4)  NOT NULL,
    [DIAG_CODE_ICD]         VARCHAR (6)  NOT NULL,
    [POA_IND_DIAG]          CHAR (1)     NULL,
    [SUS_SENT_DATE]         DATETIME     NOT NULL,
    CONSTRAINT [PK_APCArchiveDiagnosis] PRIMARY KEY CLUSTERED ([EpisodeSourceUniqueID] ASC, [CDS_ID] ASC, [SEQUENCE_NUMBER] ASC, [SUS_SENT_DATE] ASC)
);

