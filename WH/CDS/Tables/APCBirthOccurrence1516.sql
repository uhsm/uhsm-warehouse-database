﻿CREATE TABLE [CDS].[APCBirthOccurrence1516] (
    [EpisodeSourceUniqueID]         INT          NOT NULL,
    [CDS_ID]                        VARCHAR (35) NOT NULL,
    [BIRTHORD]                      CHAR (1)     NOT NULL,
    [DELIVERY_METHOD]               CHAR (1)     NULL,
    [GESTATION]                     VARCHAR (2)  NULL,
    [BIRESUS]                       CHAR (1)     NULL,
    [STAT_PERSON_CONDEL]            CHAR (1)     NULL,
    [LOC_PAT_ID]                    VARCHAR (10) NULL,
    [ORG_CODE_BAB_ID]               VARCHAR (12) NULL,
    [NHS_NO_BABY]                   VARCHAR (10) NULL,
    [NHS_NO_STAT]                   VARCHAR (2)  NULL,
    [BABYDOB]                       VARCHAR (10) NULL,
    [BIRWEIT]                       VARCHAR (4)  NULL,
    [BIRSTAT]                       CHAR (1)     NULL,
    [BABY_VISSTAT_AS_ACTIVITY_DATE] CHAR (1)     NULL,
    [SEXBABY]                       CHAR (1)     NULL,
    [LOCATION_CLASS_DEL_PLC_ACTUAL] VARCHAR (2)  NULL,
    [LOCATION_TYPE_DEL_PLC_ACTUAL]  VARCHAR (3)  NULL,
    [DELPLACE]                      CHAR (1)     NULL,
    [WITHHELD_IDENTITY_REASON_BABY] VARCHAR (2)  NULL
);

