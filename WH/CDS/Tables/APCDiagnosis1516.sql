﻿CREATE TABLE [CDS].[APCDiagnosis1516] (
    [EpisodeSourceUniqueID] INT          NOT NULL,
    [CDS_ID]                VARCHAR (35) NOT NULL,
    [SEQUENCE_NUMBER]       VARCHAR (4)  NOT NULL,
    [DIAG_CODE_ICD]         VARCHAR (6)  NOT NULL,
    [POA_IND_DIAG]          CHAR (1)     NOT NULL
);

