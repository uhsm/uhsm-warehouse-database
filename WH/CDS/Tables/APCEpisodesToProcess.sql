﻿CREATE TABLE [CDS].[APCEpisodesToProcess] (
    [EpisodeSourceUniqueID]       INT      NOT NULL,
    [BirthNotificationID]         INT      NULL,
    [DeliveryBirthNotificationID] INT      NULL,
    [CDSProtocolIdentifierCode]   CHAR (3) NOT NULL,
    [ReportPeriodStartDate]       DATE     NOT NULL,
    [ReportPeriodEndDate]         DATE     NOT NULL,
    [ModifiedPeriodStartDateTime] DATETIME NULL,
    [ModifiedPeriodEndDateTime]   DATETIME NULL,
    [ExtractDateTime]             DATETIME NULL,
    CONSTRAINT [PK_APCEpisodesToProcess] PRIMARY KEY CLUSTERED ([EpisodeSourceUniqueID] ASC)
);

