﻿CREATE TABLE [CDS].[APCExtractWardStay] (
    [EpisodeSourceUniqueID]        INT          NOT NULL,
    [SequenceNumber]               INT          NOT NULL,
    [WARD_CODE_WARDSTAY]           VARCHAR (12) NOT NULL,
    [WARD_SECURITY_LEVEL_WARDSTAY] CHAR (1)     NULL,
    [LOCATION_CLASS_WARD]          VARCHAR (2)  NULL,
    [SITECODE_DURING_WARD_STAY]    VARCHAR (9)  NULL,
    [LOCATION_TYPE_WARD]           VARCHAR (3)  NULL,
    [INT_CC_INTENSITY_WARD]        VARCHAR (2)  NULL,
    [AGE_GRP_INT_WARD]             CHAR (1)     NULL,
    [SEX_OF_PATIENTS_WARD]         CHAR (1)     NULL,
    [WARD_DAY_PERIOD_AVAIL_WARD]   CHAR (1)     NULL,
    [WARD_NIGHT_PERIOD_AVAIL_WARD] CHAR (1)     NULL,
    [WARD_STAY_START_DATE]         DATETIME     NULL,
    [WARD_STAY_END_DATE]           DATETIME     NULL,
    CONSTRAINT [PK_APCExtractWardStay] PRIMARY KEY CLUSTERED ([EpisodeSourceUniqueID] ASC, [SequenceNumber] ASC)
);

