﻿CREATE TABLE [CDS].[APCProcedure_1516REF] (
    [EpisodeSourceUniqueID]    INT          NOT NULL,
    [CDS_ID]                   VARCHAR (35) NOT NULL,
    [SEQUENCE_NUMBER]          VARCHAR (4)  NOT NULL,
    [PROC]                     VARCHAR (4)  NOT NULL,
    [PROC_DATE]                VARCHAR (10) NULL,
    [PRO_REG_ISSUER_CODE_HP]   VARCHAR (2)  NULL,
    [MOCP_PRO_REG_ENTRY_ID_HP] VARCHAR (12) NULL,
    [PRO_REG_ISSUER_CODE_RA]   VARCHAR (2)  NULL,
    [RA_PRO_REG_ENTRY_ID_RA]   VARCHAR (12) NULL
);

