﻿CREATE TABLE [CDS].[APCStagingDiagnosis] (
    [EpisodeSourceUniqueID]           INT         NOT NULL,
    [SequenceNumber]                  INT         NOT NULL,
    [DiagnosisCode]                   VARCHAR (6) NOT NULL,
    [DiagnosisPresentOnAdmissionFlag] CHAR (1)    NULL,
    [SensitiveDiagnosisFlag]          CHAR (1)    NULL,
    CONSTRAINT [PK_APCStagingDiagnosis] PRIMARY KEY CLUSTERED ([EpisodeSourceUniqueID] ASC, [SequenceNumber] ASC)
);

