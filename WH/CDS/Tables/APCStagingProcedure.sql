﻿CREATE TABLE [CDS].[APCStagingProcedure] (
    [EpisodeSourceUniqueID]     INT          NOT NULL,
    [SequenceNumber]            INT          NOT NULL,
    [ProcedureCode]             VARCHAR (4)  NOT NULL,
    [ProcedureDate]             DATETIME     NULL,
    [RegistrationIssuerCode_HP] VARCHAR (2)  NULL,
    [RegistrationCode_HP]       VARCHAR (12) NULL,
    [RegistrationIssuerCode_RA] VARCHAR (2)  NULL,
    [RegistrationCode_RA]       VARCHAR (12) NULL,
    [SensitiveProcedureFlag]    CHAR (1)     NULL,
    CONSTRAINT [PK_APCStagingProcedure] PRIMARY KEY CLUSTERED ([EpisodeSourceUniqueID] ASC, [SequenceNumber] ASC)
);

