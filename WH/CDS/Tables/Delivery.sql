﻿CREATE TABLE [CDS].[Delivery] (
    [EpisodeSourceUniqueID]                NUMERIC (18) NOT NULL,
    [PreviousPregnancies]                  NUMERIC (18) NULL,
    [NumberOfBabies]                       NUMERIC (18) NULL,
    [FirstAnteNatalAssessmentDate]         VARCHAR (10) NULL,
    [AnteNatalGpCode]                      VARCHAR (60) NULL,
    [AnteNatalGpPracticeCode]              VARCHAR (60) NULL,
    [DeliveryLocationClassCode]            INT          NULL,
    [DeliveryPlaceChangeReasonCode]        INT          NULL,
    [IntendedDeliveryPlaceTypeCode]        INT          NULL,
    [AnaestheticGivenDuringCode]           INT          NULL,
    [AnaestheticGivenPostCode]             INT          NULL,
    [GestationLength]                      NUMERIC (18) NULL,
    [LabourOnsetMethodCode]                INT          NULL,
    [DeliveryDate]                         DATETIME     NULL,
    [DeliveryMethodCode]                   INT          NULL,
    [ActualDeliveryPlaceTypeCode]          INT          NULL,
    [StatusOfPersonConductingDeliveryCode] VARCHAR (50) NULL,
    [SourceSpellNo]                        VARCHAR (60) NULL,
    CONSTRAINT [PK_Delivery] PRIMARY KEY CLUSTERED ([EpisodeSourceUniqueID] ASC)
);

