﻿CREATE TABLE [CDS].[MainSpecialty] (
    [MainSpecialtyCode]     VARCHAR (3)   NOT NULL,
    [MainSpecialty]         VARCHAR (100) NOT NULL,
    [ArchiveFlag]           CHAR (1)      NULL,
    [TreatmentFunctionCode] VARCHAR (3)   NULL,
    CONSTRAINT [PK_MainSpecialty] PRIMARY KEY CLUSTERED ([MainSpecialtyCode] ASC)
);

