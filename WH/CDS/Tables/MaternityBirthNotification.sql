﻿CREATE TABLE [CDS].[MaternityBirthNotification] (
    [BirthNotificationID]    INT          IDENTITY (1, 1) NOT NULL,
    [PatientPointer]         INT          NOT NULL,
    [BirthOrder]             INT          NOT NULL,
    [BirthDtm]               DATETIME     NOT NULL,
    [BirthSourceUniqueID]    INT          NULL,
    [DeliverySourceUniqueID] INT          NULL,
    [IsDeleted]              CHAR (1)     DEFAULT ('N') NOT NULL,
    [CreatedDtm]             DATETIME     NOT NULL,
    [ModifiedDtm]            DATETIME     NOT NULL,
    [RowHash]                VARCHAR (32) NOT NULL,
    CONSTRAINT [PK_MaternityBirthNotification] PRIMARY KEY CLUSTERED ([PatientPointer] ASC, [BirthOrder] ASC, [BirthDtm] ASC, [CreatedDtm] ASC)
);

