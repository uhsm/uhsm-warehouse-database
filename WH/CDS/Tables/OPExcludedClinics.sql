﻿CREATE TABLE [CDS].[OPExcludedClinics] (
    [ClinicCode]        NVARCHAR (255) NULL,
    [ClinicDescription] VARCHAR (255)  NULL,
    [ClinicType]        NVARCHAR (255) NULL,
    [Action]            NVARCHAR (255) NULL
);

