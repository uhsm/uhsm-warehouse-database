﻿CREATE TABLE [CDS].[PayloadContents] (
    [Filename] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_PayloadContents] PRIMARY KEY CLUSTERED ([Filename] ASC)
);

