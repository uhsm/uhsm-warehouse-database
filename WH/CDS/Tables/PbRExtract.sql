﻿CREATE TABLE [CDS].[PbRExtract] (
    [DatasetCode]         VARCHAR (15) NOT NULL,
    [SourceUniqueID]      VARCHAR (50) NOT NULL,
    [EncounterTypeCode]   VARCHAR (10) NOT NULL,
    [CommissionerCodeCCG] VARCHAR (10) NULL,
    [ExclusionType]       VARCHAR (20) NULL,
    [FinalSLACode]        VARCHAR (15) NULL,
    [PbRExcluded]         BIT          NULL,
    [ProviderSpellNo]     VARCHAR (50) NULL,
    [CreatedDtm]          DATETIME     NOT NULL,
    [ModifiedDtm]         DATETIME     NOT NULL,
    [RowHash]             VARCHAR (32) NOT NULL,
    CONSTRAINT [PK_PbRExtract] PRIMARY KEY CLUSTERED ([DatasetCode] ASC, [SourceUniqueID] ASC)
);

