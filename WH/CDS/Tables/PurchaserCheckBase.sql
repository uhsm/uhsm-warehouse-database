﻿CREATE TABLE [CDS].[PurchaserCheckBase] (
    [SourceUniqueID]      INT           NOT NULL,
    [LocalPatientID]      VARCHAR (25)  NULL,
    [EncounterDate]       SMALLDATETIME NULL,
    [EndDate]             SMALLDATETIME NULL,
    [IPMPurchaser]        VARCHAR (20)  NULL,
    [RegisteredPractice]  VARCHAR (25)  NULL,
    [IPMPracCodeEp]       VARCHAR (20)  NULL,
    [IPMParent]           VARCHAR (20)  NULL,
    [PCTofPracticeCode]   VARCHAR (20)  NULL,
    [CCGofPracticeCode]   NVARCHAR (20) NULL,
    [encPostcode]         VARCHAR (25)  NULL,
    [DistrictOfResidence] NVARCHAR (3)  NULL,
    [OverseasStatus]      VARCHAR (50)  NULL,
    [PCTofResidenceCode]  NVARCHAR (5)  NULL,
    [CCGofResidenceCode]  NVARCHAR (5)  NULL,
    [PurchaserCode]       VARCHAR (5)   NULL,
    [PurchaserCodeCCG]    NVARCHAR (5)  NULL,
    [Attended]            VARCHAR (3)   NOT NULL
);

