﻿CREATE TABLE [CDS].[PurchaserCheckBaseCDS] (
    [SourceUniqueID]     INT           NOT NULL,
    [PracticeCode]       VARCHAR (20)  NULL,
    [EncounterDate]      SMALLDATETIME NULL,
    [PCTofPracticeCode]  VARCHAR (20)  NULL,
    [CCGofPracticeCode]  NVARCHAR (20) NULL,
    [PCTofResidenceCode] NVARCHAR (5)  NULL,
    [CCGofResidenceCode] NVARCHAR (5)  NULL,
    [PurchaserCode]      VARCHAR (3)   NULL,
    [PurchaserCodeCCG]   NVARCHAR (3)  NULL
);

