﻿CREATE TABLE [CDS].[SUSTimetable] (
    [ActivityStartDate]      DATE NOT NULL,
    [ActivityEndDate]        DATE NOT NULL,
    [PBR1stCutDate]          DATE NULL,
    [CodingDate]             DATE NULL,
    [PBR2ndCutDate]          DATE NULL,
    [FinanceDate]            DATE NULL,
    [RecInclusionDate]       DATE NULL,
    [HESDeadlineDate]        DATE NULL,
    [RecPublicationDate]     DATE NULL,
    [FreezeDate]             DATE NULL,
    [PostRecInclusionDate]   DATE NULL,
    [AnnualHESRefreshDate]   DATE NULL,
    [PostRecPublicationDate] DATE NULL,
    CONSTRAINT [PK_SUSTimetable] PRIMARY KEY CLUSTERED ([ActivityStartDate] ASC)
);

