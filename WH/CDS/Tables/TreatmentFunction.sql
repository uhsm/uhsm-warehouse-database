﻿CREATE TABLE [CDS].[TreatmentFunction] (
    [TreatmentFunctionCode] VARCHAR (3)   NOT NULL,
    [TreatmentFunction]     VARCHAR (100) NOT NULL,
    [ArchiveFlag]           CHAR (1)      NULL,
    [CDSVersionFrom]        INT           NULL,
    [MainSpecialtyCode]     VARCHAR (3)   NULL,
    CONSTRAINT [PK_TreatmentFunction] PRIMARY KEY CLUSTERED ([TreatmentFunctionCode] ASC)
);

