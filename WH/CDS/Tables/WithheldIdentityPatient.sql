﻿CREATE TABLE [CDS].[WithheldIdentityPatient] (
    [SourcePatientNo] INT      NOT NULL,
    [CreatedDateTime] DATETIME NOT NULL,
    CONSTRAINT [PK_WithheldIdentityPatient] PRIMARY KEY CLUSTERED ([SourcePatientNo] ASC)
);

