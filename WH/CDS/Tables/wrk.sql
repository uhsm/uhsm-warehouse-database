﻿CREATE TABLE [CDS].[wrk] (
    [CDSRecno]       BIGINT      IDENTITY (1, 1) NOT NULL,
    [CDSTypeCode]    VARCHAR (3) NOT NULL,
    [EncounterRecno] INT         NOT NULL,
    CONSTRAINT [PK_wrkCDS] PRIMARY KEY CLUSTERED ([CDSRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_wrkCDS]
    ON [CDS].[wrk]([CDSTypeCode] ASC, [EncounterRecno] ASC);

