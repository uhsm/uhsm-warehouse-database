﻿CREATE TABLE [CDS].[znOP_Appts] (
    [PRIME_RECIPIENT]                VARCHAR (12) NULL,
    [COPY_RECIPIENT_1]               VARCHAR (12) NULL,
    [COPY_RECIPIENT_2]               VARCHAR (12) NULL,
    [COPY_RECIPIENT_3]               VARCHAR (12) NULL,
    [COPY_RECIPIENT_4]               VARCHAR (12) NULL,
    [COPY_RECIPIENT_5]               VARCHAR (12) NULL,
    [SENDER]                         VARCHAR (12) NULL,
    [CDS_GROUP]                      VARCHAR (3)  NULL,
    [CDS_TYPE]                       VARCHAR (3)  NULL,
    [CDS_ID]                         VARCHAR (35) NOT NULL,
    [UPDATE_TYPE]                    CHAR (1)     NULL,
    [PROTOCOL_IDENTIFIER]            VARCHAR (3)  NULL,
    [BULKSTART]                      DATE         NULL,
    [BULKEND]                        DATE         NULL,
    [PROVIDER]                       VARCHAR (12) NULL,
    [PURCHASER]                      VARCHAR (12) NULL,
    [SERIAL_NO]                      VARCHAR (6)  NULL,
    [CONT_LINE_NO]                   VARCHAR (10) NULL,
    [PURCHREF]                       VARCHAR (17) NULL,
    [NHS_NO]                         VARCHAR (17) NULL,
    [NHS_NO_STATUS]                  VARCHAR (2)  NULL,
    [NAME_FORMAT]                    CHAR (1)     NULL,
    [ADDRESS_FORMAT]                 CHAR (1)     NULL,
    [NAME]                           VARCHAR (70) NULL,
    [FORENAME]                       VARCHAR (35) NULL,
    [HOMEADD1]                       VARCHAR (35) NULL,
    [HOMEADD2]                       VARCHAR (35) NULL,
    [HOMEADD3]                       VARCHAR (35) NULL,
    [HOMEADD4]                       VARCHAR (35) NULL,
    [HOMEADD5]                       VARCHAR (35) NULL,
    [POSTCODE]                       VARCHAR (8)  NULL,
    [HA]                             VARCHAR (12) NULL,
    [WITHHELD_IDENTITY_REASON]       VARCHAR (2)  NULL,
    [SEX]                            CHAR (1)     NULL,
    [CAR_SUP_IND]                    VARCHAR (2)  NULL,
    [DOB]                            DATE         NULL,
    [GPREG]                          VARCHAR (8)  NULL,
    [REGPRACT]                       VARCHAR (12) NULL,
    [LOCPATID]                       VARCHAR (10) NULL,
    [REFERRER]                       VARCHAR (8)  NULL,
    [REF_ORG]                        VARCHAR (12) NULL,
    [SERV_TYPE]                      CHAR (1)     NULL,
    [REQDATE]                        DATE         NULL,
    [PRIORITY]                       CHAR (1)     NULL,
    [REFSRCE]                        VARCHAR (2)  NULL,
    [MAINSPEF]                       VARCHAR (3)  NULL,
    [CON_SPEC]                       VARCHAR (3)  NULL,
    [LOC_SPEC]                       VARCHAR (3)  NULL,
    [CCODE]                          VARCHAR (8)  NULL,
    [LOCAL_SUB_SPECIALTY_CODE]       VARCHAR (8)  NULL,
    [ATT_IDENT]                      VARCHAR (12) NULL,
    [CATEGORY]                       VARCHAR (2)  NULL,
    [LOCATION_CLASS]                 VARCHAR (2)  NULL,
    [SITECODE]                       VARCHAR (12) NULL,
    [LOCATION_TYPE]                  VARCHAR (3)  NULL,
    [CLINIC_CODE]                    VARCHAR (12) NULL,
    [MED_STAFF]                      VARCHAR (2)  NULL,
    [ATTDATE]                        DATE         NULL,
    [ARRIVAL_TIME]                   DATETIME     NULL,
    [EXPECTED_APPOINTMENT_DURATION]  VARCHAR (3)  NULL,
    [ACTIVITY_DATE]                  DATE         NULL,
    [AGE_AT_CDS_ACTIVITY_DATE]       INT          NULL,
    [VISITOR_STAT_AT_ACTIVITY_DATE]  CHAR (1)     NULL,
    [EARLIEST_REASONABLE_DATE_OFFER] DATE         NULL,
    [EARLIEST_CLINICALLY_APPRO_DATE] DATE         NULL,
    [CONSULTATION_MEDIUM_USED]       VARCHAR (2)  NULL,
    [PBR_MP_OR_MDC_CONS_IND_CODE]    CHAR (1)     NULL,
    [REHAB_ASSESSMENT_TEAM_TYPE]     CHAR (1)     NULL,
    [FIRSTATT]                       CHAR (1)     NULL,
    [ATTENDED]                       CHAR (1)     NULL,
    [OUTCOME]                        CHAR (1)     NULL,
    [LASTDNADATE]                    DATE         NULL,
    [OPSTATUS]                       CHAR (1)     NULL,
    [PROVIDER_REF_NO]                VARCHAR (17) NULL,
    [LOCAL_PATIENT_ID_ORG]           VARCHAR (12) NULL,
    [UBRN]                           VARCHAR (12) NULL,
    [CARE_PATHWAY_ID]                VARCHAR (20) NULL,
    [CARE_PATHWAY_ID_ORG]            VARCHAR (12) NULL,
    [REF_TO_TREAT_PERIOD_STATUS]     VARCHAR (2)  NULL,
    [REF_TO_TREAT_PERIOD_START_DATE] DATE         NULL,
    [REF_TO_TREAT_PERIOD_END_DATE]   DATE         NULL,
    [WAIT_TIME_MEASUREMENT_TYPE]     VARCHAR (2)  NULL,
    [ETHNIC_CATEGORY]                VARCHAR (2)  NULL,
    [DIRECT_ACCESS_REFERRAL_IND]     CHAR (1)     NULL,
    [ICD_DIAG_SCHEME]                VARCHAR (2)  NULL,
    [READ_DIAG_SCHEME]               VARCHAR (2)  NULL,
    [OPCS_PROC_SCHEME]               VARCHAR (2)  NULL,
    [READ_PROC_SCHEME]               VARCHAR (2)  NULL,
    CONSTRAINT [PK_znOP_Appts] PRIMARY KEY NONCLUSTERED ([CDS_ID] ASC)
);

