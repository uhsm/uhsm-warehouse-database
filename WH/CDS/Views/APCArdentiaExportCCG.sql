﻿

CREATE view [CDS].[APCArdentiaExportCCG] as

select
	 [PRIME RECIPIENT] = cast([PRIME RECIPIENT CCG] as varchar(6))
	,[COPY RECIPIENT 1] = cast([COPY RECIPIENT 1 CCG] as varchar(6))
	,[COPY RECIPIENT 2] = cast([COPY RECIPIENT 2] as varchar(6))
	,[COPY RECIPIENT 3] = cast([COPY RECIPIENT 3] as varchar(6))
	,[COPY RECIPIENT 4] = cast([COPY RECIPIENT 4] as varchar(6))
	,[COPY RECIPIENT 5] = cast([COPY RECIPIENT 5] as varchar(6))
	,[SENDER] = cast([SENDER] as varchar(5))
	,[CDS GROUP] = cast([CDS GROUP] as varchar(3))
	,[CDS TYPE] = cast([CDS TYPE] as varchar(3))
	,[CDS_ID] = cast([CDS_ID] as varchar(35))
	,[TEST_FLAG] = cast([TEST_FLAG] as varchar(1))
	,[UPDATE TYPE] = cast([UPDATE TYPE] as varchar(1))
	,[PROTOCOL IDENTIFIER] = cast([PROTOCOL IDENTIFIER] as varchar(3))
	,[BULKSTART] = cast([BULKSTART] as varchar(10))
	,[BULKEND] = cast([BULKEND] as varchar(10))
	,[DATETIME_CREATED] = cast([DATETIME_CREATED] as varchar(16))
	,[PROVIDER] = cast([PROVIDER] as varchar(5))
	,[PURCHASER] = cast([PURCHASER CCG] as varchar(5))
	,[SERIAL_NO] = cast([SERIAL_NO] as varchar(6))
	,[CONTRACT_LINE_NO] = cast([CONTRACT_LINE_NO] as varchar(10))
	,[PURCH_REF] = cast([PURCH_REF] as varchar(17))
	,[NHS_NO] = cast([NHS_NO] as varchar(17))
	,[NNN_STATUS_IND] = cast([NNN_STATUS_IND] as varchar(2))
	,[NAME_FORMAT] = cast([NAME_FORMAT] as varchar(1))
	,[ADDRESS_FORMAT] = cast([ADDRESS_FORMAT] as varchar(1))
	,[NAME] = cast([NAME] as varchar(70))
	,[FORENAME] = cast([FORENAME] as varchar(35))
	,[HOMEADD1] = cast([HOMEADD1] as varchar(35))
	,[HOMEADD2] = cast([HOMEADD2] as varchar(35))
	,[HOMEADD3] = cast([HOMEADD3] as varchar(35))
	,[HOMEADD4] = cast([HOMEADD4] as varchar(35))
	,[HOMEADD5] = cast([HOMEADD5] as varchar(35))
	,[POSTCODE] = cast([POSTCODE] as varchar(8))
	,[HA] = cast([HA CCG] as varchar(3))
	,[SEX] = cast([SEX] as varchar(1))
	,[CARER_SUPPORT_IND] = cast([CARER_SUPPORT_IND] as varchar(2))
	,[DOB] = cast([DOB] as varchar(10))
	,[GPREG] = cast([GPREG] as varchar(8))
	,[PRACREG] = cast([PRACREG] as varchar(6))
	,[ETHNICOR] = cast([ETHNICOR] as varchar(2))
	,[LOCPATID] = cast([LOCPATID] as varchar(10))
	,[REFERRER] = cast([REFERRER] as varchar(8))
	,[REF_ORG] = cast([REF_ORG] as varchar(6))
	,[PROV_SPELL] = cast([PROV_SPELL] as varchar(12))
	,[ADMIN_CATEGORY] = cast([ADMIN_CATEGORY] as varchar(2))
	,[LEGAL_STATUS] = cast([LEGAL_STATUS] as varchar(2))
	,[ELECDATE] = cast([ELECDATE] as varchar(10))
	,[EARLIEST_REASONABLE_DATE_OFFER] = cast([EARLIEST_REASONABLE_DATE_OFFER] as varchar(10))
	,[ADMIDATE] = cast([ADMIDATE] as varchar(10))
	,[AGE_ON_ADMISSION_DATE] = cast([AGE_ON_ADMISSION_DATE] as varchar(3))
	,[ADMIMETH] = cast([ADMIMETH] as varchar(2))
	,[ELECDUR] = cast([ELECDUR] as varchar(4))
	,[MAN_INTENT] = cast([MAN_INTENT] as varchar(1))
	,[ADMISORC] = cast([ADMISORC] as varchar(2))
	,[DISCHARGE_READY_DATE] = cast([DISCHARGE_READY_DATE] as varchar(10))
	,[DISDATE] = cast([DISDATE] as varchar(10))
	,[DISMETH] = cast([DISMETH] as varchar(1))
	,[DISDEST] = cast([DISDEST] as varchar(2))
	,[CLASSPAT] = cast([CLASSPAT] as varchar(1))
	,[EPIORDER] = cast([EPIORDER] as varchar(2))
	,[LAST_IN_SPELL] = cast([LAST_IN_SPELL] as varchar(1))
	,[EPISTART] = cast([EPISTART] as varchar(10))
	,[EPIEND] = cast([EPIEND] as varchar(10))
	,[ACTIVITY_DATE] = cast([ACTIVITY_DATE] as varchar(10))
	,[AGE_AT_CDS_ACTIVITY_DATE] = cast([AGE_AT_CDS_ACTIVITY_DATE] as varchar(3))
	,[MAINSPEF] = cast([MAINSPEF] as varchar(3))
	,[CONS_MAINSPEF] = cast([CONS_MAINSPEF] as varchar(3))
	,[CONS_CODE] = cast([CONS_CODE] as varchar(8))
	,[LOCATION_CLASS_START] = cast([LOCATION_CLASS_START] as varchar(2))
	,[SITECODE_AT_EPISODE_START] = cast([SITECODE_AT_EPISODE_START] as varchar(5))
	,[INT_CC_INTENSITY_START] = cast([INT_CC_INTENSITY_START] as varchar(2))
	,[AGE_GRP_INT_START] = cast([AGE_GRP_INT_START] as varchar(1))
	,[SEX_OF_PATIENTS_START] = cast([SEX_OF_PATIENTS_START] as varchar(1))
	,[WARD_DAY_PERIOD_AVAIL_START] = cast([WARD_DAY_PERIOD_AVAIL_START] as varchar(1))
	,[WARD_NIGHT_PERIOD_AVAIL_START] = cast([WARD_NIGHT_PERIOD_AVAIL_START] as varchar(1))
	,[LOCATION_CLASS_WARD] = cast([LOCATION_CLASS_WARD] as varchar(2))
	,[SITECODE_DURING_WARD_STAY] = cast([SITECODE_DURING_WARD_STAY] as varchar(5))
	,[INT_CC_INTENSITY_WARD] = cast([INT_CC_INTENSITY_WARD] as varchar(2))
	,[AGE_GRP_INT_WARD] = cast([AGE_GRP_INT_WARD] as varchar(1))
	,[SEX_OF_PATIENTS_WARD] = cast([SEX_OF_PATIENTS_WARD] as varchar(1))
	,[WARD_DAY_PERIOD_AVAIL_WARD] = cast([WARD_DAY_PERIOD_AVAIL_WARD] as varchar(1))
	,[WARD_NIGHT_PERIOD_AVAIL_WARD] = cast([WARD_NIGHT_PERIOD_AVAIL_WARD] as varchar(1))
	,[WARD_STAY_START_DATE] = cast([WARD_STAY_START_DATE] as varchar(10))
	,[WARD_STAY_END_DATE] = cast([WARD_STAY_END_DATE] as varchar(10))
	,[LOCATION_CLASS_END] = cast([LOCATION_CLASS_END] as varchar(2))
	,[SITECODE_AT_EPISODE_END] = cast([SITECODE_AT_EPISODE_END] as varchar(5))
	,[INT_CC_INTENSITY_END] = cast([INT_CC_INTENSITY_END] as varchar(2))
	,[AGE_GRP_INT_END] = cast([AGE_GRP_INT_END] as varchar(1))
	,[SEX_OF_PATIENTS_END] = cast([SEX_OF_PATIENTS_END] as varchar(1))
	,[WARD_DAY_PERIOD_AVAIL_END] = cast([WARD_DAY_PERIOD_AVAIL_END] as varchar(1))
	,[WARD_NIGHT_PERIOD_AVAIL_END] = cast([WARD_NIGHT_PERIOD_AVAIL_END] as varchar(1))
	,[FIRST_REG_ADM] = cast([FIRST_REG_ADM] as varchar(1))
	,[NEONATAL_LEVEL] = cast([NEONATAL_LEVEL] as varchar(1))
	,[ADMISTAT] = cast([ADMISTAT] as varchar(1))
	,[ICD_DIAG_SCHEME] = cast([ICD_DIAG_SCHEME] as varchar(2))
	,[PRIMARY_DIAG] = cast([PRIMARY_DIAG] as varchar(6))
	,[SECONDARY_DIAG1] = cast([SECONDARY_DIAG1] as varchar(6))
	,[SECONDARY_DIAG2] = cast([SECONDARY_DIAG2] as varchar(6))
	,[SECONDARY_DIAG3] = cast([SECONDARY_DIAG3] as varchar(6))
	,[SECONDARY_DIAG4] = cast([SECONDARY_DIAG4] as varchar(6))
	,[SECONDARY_DIAG5] = cast([SECONDARY_DIAG5] as varchar(6))
	,[SECONDARY_DIAG6] = cast([SECONDARY_DIAG6] as varchar(6))
	,[SECONDARY_DIAG7] = cast([SECONDARY_DIAG7] as varchar(6))
	,[SECONDARY_DIAG8] = cast([SECONDARY_DIAG8] as varchar(6))
	,[SECONDARY_DIAG9] = cast([SECONDARY_DIAG9] as varchar(6))
	,[SECONDARY_DIAG10] = cast([SECONDARY_DIAG10] as varchar(6))
	,[SECONDARY_DIAG11] = cast([SECONDARY_DIAG11] as varchar(6))
	,[SECONDARY_DIAG12] = cast([SECONDARY_DIAG12] as varchar(6))
	,[READ_DIAG_SCHEME] = cast([READ_DIAG_SCHEME] as varchar(2))
	,[PRIMARY_READ_DIAG1] = cast([PRIMARY_READ_DIAG1] as varchar(7))
	,[READ_DIAG2] = cast([READ_DIAG2] as varchar(7))
	,[READ_DIAG3] = cast([READ_DIAG3] as varchar(7))
	,[READ_DIAG4] = cast([READ_DIAG4] as varchar(7))
	,[READ_DIAG5] = cast([READ_DIAG5] as varchar(7))
	,[READ_DIAG6] = cast([READ_DIAG6] as varchar(7))
	,[READ_DIAG7] = cast([READ_DIAG7] as varchar(7))
	,[READ_DIAG8] = cast([READ_DIAG8] as varchar(7))
	,[READ_DIAG9] = cast([READ_DIAG9] as varchar(7))
	,[READ_DIAG10] = cast([READ_DIAG10] as varchar(7))
	,[READ_DIAG11] = cast([READ_DIAG11] as varchar(7))
	,[READ_DIAG12] = cast([READ_DIAG12] as varchar(7))
	,[READ_DIAG13] = cast([READ_DIAG13] as varchar(7))
	,[READ_DIAG14] = cast([READ_DIAG14] as varchar(7))
	,[OPER_STATUS] = cast([OPER_STATUS] as varchar(1))
	,[OPCS_PROC_SCHEME] = cast([OPCS_PROC_SCHEME] as varchar(2))
	,[PRIMARY_PROC] = cast([PRIMARY_PROC] as varchar(4))
	,[PRIMARY_PROC_DATE] = cast([PRIMARY_PROC_DATE] as varchar(10))
	,[PROC2] = cast([PROC2] as varchar(4))
	,[PROC2_DATE] = cast([PROC2_DATE] as varchar(10))
	,[PROC3] = cast([PROC3] as varchar(4))
	,[PROC3_DATE] = cast([PROC3_DATE] as varchar(10))
	,[PROC4] = cast([PROC4] as varchar(4))
	,[PROC4_DATE] = cast([PROC4_DATE] as varchar(10))
	,[PROC5] = cast([PROC5] as varchar(4))
	,[PROC5_DATE] = cast([PROC5_DATE] as varchar(10))
	,[PROC6] = cast([PROC6] as varchar(4))
	,[PROC6_DATE] = cast([PROC6_DATE] as varchar(10))
	,[PROC7] = cast([PROC7] as varchar(4))
	,[PROC7_DATE] = cast([PROC7_DATE] as varchar(10))
	,[PROC8] = cast([PROC8] as varchar(4))
	,[PROC8_DATE] = cast([PROC8_DATE] as varchar(10))
	,[PROC9] = cast([PROC9] as varchar(4))
	,[PROC9_DATE] = cast([PROC9_DATE] as varchar(10))
	,[PROC10] = cast([PROC10] as varchar(4))
	,[PROC10_DATE] = cast([PROC10_DATE] as varchar(10))
	,[PROC11] = cast([PROC11] as varchar(4))
	,[PROC11_DATE] = cast([PROC11_DATE] as varchar(10))
	,[PROC12] = cast([PROC12] as varchar(4))
	,[PROC12_DATE] = cast([PROC12_DATE] as varchar(10))
	,[READ_PROC_SCHEME] = cast([READ_PROC_SCHEME] as varchar(2))
	,[READ_PROC1] = cast([READ_PROC1] as varchar(7))
	,[READ_PROC2] = cast([READ_PROC2] as varchar(7))
	,[READ_PROC3] = cast([READ_PROC3] as varchar(7))
	,[READ_PROC4] = cast([READ_PROC4] as varchar(7))
	,[READ_PROC5] = cast([READ_PROC5] as varchar(7))
	,[READ_PROC6] = cast([READ_PROC6] as varchar(7))
	,[READ_PROC7] = cast([READ_PROC7] as varchar(7))
	,[READ_PROC8] = cast([READ_PROC8] as varchar(7))
	,[READ_PROC9] = cast([READ_PROC9] as varchar(7))
	,[READ_PROC10] = cast([READ_PROC10] as varchar(7))
	,[READ_PROC11] = cast([READ_PROC11] as varchar(7))
	,[READ_PROC12] = cast([READ_PROC12] as varchar(7))
	,[READ_PROC1_DATE] = cast([READ_PROC1_DATE] as varchar(10))
	,[READ_PROC2_DATE] = cast([READ_PROC2_DATE] as varchar(10))
	,[READ_PROC3_DATE] = cast([READ_PROC3_DATE] as varchar(10))
	,[READ_PROC4_DATE] = cast([READ_PROC4_DATE] as varchar(10))
	,[READ_PROC5_DATE] = cast([READ_PROC5_DATE] as varchar(10))
	,[READ_PROC6_DATE] = cast([READ_PROC6_DATE] as varchar(10))
	,[READ_PROC7_DATE] = cast([READ_PROC7_DATE] as varchar(10))
	,[READ_PROC8_DATE] = cast([READ_PROC8_DATE] as varchar(10))
	,[READ_PROC9_DATE] = cast([READ_PROC9_DATE] as varchar(10))
	,[READ_PROC10_DATE] = cast([READ_PROC10_DATE] as varchar(10))
	,[READ_PROC11_DATE] = cast([READ_PROC11_DATE] as varchar(10))
	,[READ_PROC12_DATE] = cast([READ_PROC12_DATE] as varchar(10))
	,[ANTENATAL_GP] = cast([ANTENATAL_GP] as varchar(8))
	,[ANTENATAL_GP_PRAC] = cast([ANTENATAL_GP_PRAC] as varchar(6))
	,[ANTENATAL_ASSESSDATE] = cast([ANTENATAL_ASSESSDATE] as varchar(10))
	,[NUMPREG] = cast([NUMPREG] as varchar(2))
	,[DELPLACE] = cast([DELPLACE] as varchar(1))
	,[DELDATE] = cast([DELDATE] as varchar(10))
	,[LOCATION_CLASS_DELPLCINT] = cast([LOCATION_CLASS_DELPLCINT] as varchar(2))
	,[DELINTEN] = cast([DELINTEN] as varchar(1))
	,[DELCHANG] = cast([DELCHANG] as varchar(1))
	,[GESTAT] = cast([GESTAT] as varchar(2))
	,[DELONSET] = cast([DELONSET] as varchar(1))
	,[DELMETH] = cast([DELMETH] as varchar(1))
	,[DELSTAT] = cast([DELSTAT] as varchar(1))
	,[DELPREAN] = cast([DELPREAN] as varchar(1))
	,[DELPOSAN] = cast([DELPOSAN] as varchar(1))
	,[MUMDOB] = cast([MUMDOB] as varchar(10))
	,[NUMBBABY] = cast([NUMBBABY] as varchar(1))
	,[SEXBABY1] = cast([SEXBABY1] as varchar(1))
	,[BIRORD1] = cast([BIRORD1] as varchar(1))
	,[BIRSTAT1] = cast([BIRSTAT1] as varchar(1))
	,[BIRWEIT1] = cast([BIRWEIT1] as varchar(4))
	,[BIRESUS1] = cast([BIRESUS1] as varchar(1))
	,[BABYDOB1] = cast([BABYDOB1] as varchar(10))
	,[GESTATON 1] = cast([GESTATON 1] as varchar(2))
	,[DELIVERY METHOD 1] = cast([DELIVERY METHOD 1] as varchar(1))
	,[STAT PERSON CONDEL1] = cast([STAT PERSON CONDEL1] as varchar(1))
	,[LOC PAT ID 1] = cast([LOC PAT ID 1] as varchar(10))
	,[ORG CODE BAB ID 1] = cast([ORG CODE BAB ID 1] as varchar(5))
	,[NHS NO BABY 1] = cast([NHS NO BABY 1] as varchar(10))
	,[NHS NO STAT 1] = cast([NHS NO STAT 1] as varchar(2))
	,[SEXBABY2] = cast([SEXBABY2] as varchar(1))
	,[BIRORD2] = cast([BIRORD2] as varchar(1))
	,[BIRSTAT2] = cast([BIRSTAT2] as varchar(1))
	,[BIRWEIT2] = cast([BIRWEIT2] as varchar(4))
	,[BIRESUS2] = cast([BIRESUS2] as varchar(1))
	,[BABYDOB2] = cast([BABYDOB2] as varchar(10))
	,[GESTATON 2] = cast([GESTATON 2] as varchar(2))
	,[DELIVERY METHOD 2] = cast([DELIVERY METHOD 2] as varchar(1))
	,[STAT PERSON CONDEL2] = cast([STAT PERSON CONDEL2] as varchar(1))
	,[LOC PAT ID 2] = cast([LOC PAT ID 2] as varchar(10))
	,[ORG CODE BAB ID 2] = cast([ORG CODE BAB ID 2] as varchar(5))
	,[NHS NO BABY 2] = cast([NHS NO BABY 2] as varchar(10))
	,[NHS NO STAT 2] = cast([NHS NO STAT 2] as varchar(2))
	,[SEXBABY3] = cast([SEXBABY3] as varchar(1))
	,[BIRORD3] = cast([BIRORD3] as varchar(1))
	,[BIRSTAT3] = cast([BIRSTAT3] as varchar(1))
	,[BIRWEIT3] = cast([BIRWEIT3] as varchar(4))
	,[BIRESUS3] = cast([BIRESUS3] as varchar(1))
	,[BABYDOB3] = cast([BABYDOB3] as varchar(10))
	,[GESTATON 3] = cast([GESTATON 3] as varchar(2))
	,[DELIVERY METHOD 3] = cast([DELIVERY METHOD 3] as varchar(1))
	,[STAT PERSON CONDEL3] = cast([STAT PERSON CONDEL3] as varchar(1))
	,[LOC PAT ID 3] = cast([LOC PAT ID 3] as varchar(10))
	,[ORG CODE BAB ID 3] = cast([ORG CODE BAB ID 3] as varchar(5))
	,[NHS NO BABY 3] = cast([NHS NO BABY 3] as varchar(10))
	,[NHS NO STAT 3] = cast([NHS NO STAT 3] as varchar(2))
	,[SEXBABY4] = cast([SEXBABY4] as varchar(1))
	,[BIRORD4] = cast([BIRORD4] as varchar(1))
	,[BIRSTAT4] = cast([BIRSTAT4] as varchar(1))
	,[BIRWEIT4] = cast([BIRWEIT4] as varchar(4))
	,[BIRESUS4] = cast([BIRESUS4] as varchar(1))
	,[BABYDOB4] = cast([BABYDOB4] as varchar(10))
	,[GESTATON 4] = cast([GESTATON 4] as varchar(2))
	,[DELIVERY METHOD 4] = cast([DELIVERY METHOD 4] as varchar(1))
	,[STAT PERSON CONDEL4] = cast([STAT PERSON CONDEL4] as varchar(1))
	,[LOC PAT ID 4] = cast([LOC PAT ID 4] as varchar(10))
	,[ORG CODE BAB ID 4] = cast([ORG CODE BAB ID 4] as varchar(5))
	,[NHS NO BABY 4] = cast([NHS NO BABY 4] as varchar(10))
	,[NHS NO STAT 4] = cast([NHS NO STAT 4] as varchar(2))
	,[SEXBABY5] = cast([SEXBABY5] as varchar(1))
	,[BIRORD5] = cast([BIRORD5] as varchar(1))
	,[BIRSTAT5] = cast([BIRSTAT5] as varchar(1))
	,[BIRWEIT5] = cast([BIRWEIT5] as varchar(4))
	,[BIRESUS5] = cast([BIRESUS5] as varchar(1))
	,[BABYDOB5] = cast([BABYDOB5] as varchar(10))
	,[GESTATON 5] = cast([GESTATON 5] as varchar(2))
	,[DELIVERY METHOD 5] = cast([DELIVERY METHOD 5] as varchar(1))
	,[STAT PERSON CONDEL5] = cast([STAT PERSON CONDEL5] as varchar(1))
	,[LOC PAT ID 5] = cast([LOC PAT ID 5] as varchar(10))
	,[ORG CODE BAB ID 5] = cast([ORG CODE BAB ID 5] as varchar(5))
	,[NHS NO BABY 5] = cast([NHS NO BABY 5] as varchar(10))
	,[NHS NO STAT 5] = cast([NHS NO STAT 5] as varchar(2))
	,[SEXBABY6] = cast([SEXBABY6] as varchar(1))
	,[BIRORD6] = cast([BIRORD6] as varchar(1))
	,[BIRSTAT6] = cast([BIRSTAT6] as varchar(1))
	,[BIRWEIT6] = cast([BIRWEIT6] as varchar(4))
	,[BIRESUS6] = cast([BIRESUS6] as varchar(1))
	,[BABYDOB6] = cast([BABYDOB6] as varchar(10))
	,[GESTATON 6] = cast([GESTATON 6] as varchar(2))
	,[DELIVERY METHOD 6] = cast([DELIVERY METHOD 6] as varchar(1))
	,[STAT PERSON CONDEL6] = cast([STAT PERSON CONDEL6] as varchar(1))
	,[LOC PAT ID 6] = cast([LOC PAT ID 6] as varchar(10))
	,[ORG CODE BAB ID 6] = cast([ORG CODE BAB ID 6] as varchar(5))
	,[NHS NO BABY 6] = cast([NHS NO BABY 6] as varchar(10))
	,[NHS NO STAT 6] = cast([NHS NO STAT 6] as varchar(2))
	,[HRG] = cast([HRG] as varchar(3))
	,[HRG VERSION] = cast([HRG VERSION] as varchar(3))
	,[DGVP_SCHEME] = cast([DGVP_SCHEME] as varchar(2))
	,[HDGVP] = cast([HDGVP] as varchar(4))
	,[CC_LOCAL_IDENTIFIER1] = cast([CC_LOCAL_IDENTIFIER1] as varchar(8))
	,[CC_START_DATE1] = cast([CC_START_DATE1] as varchar(10))
	,[CC_START_TIME1] = cast([CC_START_TIME1] as varchar(8))
	,[CC_UNIT_FUNCTION1] = cast([CC_UNIT_FUNCTION1] as varchar(2))
	,[CC_UNIT_BED_CONFIG1] = cast([CC_UNIT_BED_CONFIG1] as varchar(2))
	,[CC_ADMISSION_SOURCE1] = cast([CC_ADMISSION_SOURCE1] as varchar(2))
	,[CC_SOURCE_LOCATION1] = cast([CC_SOURCE_LOCATION1] as varchar(2))
	,[CC_ADMISSION_TYPE1] = cast([CC_ADMISSION_TYPE1] as varchar(2))
	,[CC_GESTATION_LENGTH_AT_DELIV1] = cast([CC_GESTATION_LENGTH_AT_DELIV1] as varchar(2))
	,[CC_ACTIVITY_DATE1] = cast([CC_ACTIVITY_DATE1] as varchar(10))
	,[CC_PERSON_WEIGHT1] = cast([CC_PERSON_WEIGHT1] as varchar(7))
	,[CC_ACTIVITY_CODE1] = cast([CC_ACTIVITY_CODE1] as varchar(2))
	,[CC_HIGH_COST_DRUGS1] = cast([CC_HIGH_COST_DRUGS1] as varchar(4))
	,[CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS1] = cast([CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS1] as varchar(3))
	,[CC_BASIC_RESPIRATORY_SUPPORT_DAYS1] = cast([CC_BASIC_RESPIRATORY_SUPPORT_DAYS1] as varchar(3))
	,[CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS1] = cast([CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS1] as varchar(3))
	,[CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS1] = cast([CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS1] as varchar(3))
	,[CC_RENAL_SUPPORT_DAYS1] = cast([CC_RENAL_SUPPORT_DAYS1] as varchar(3))
	,[CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS1] = cast([CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS1] as varchar(3))
	,[CC_GASTRO_SUPPORT_DAYS1] = cast([CC_GASTRO_SUPPORT_DAYS1] as varchar(3))
	,[CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY1] = cast([CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY1] as varchar(3))
	,[CC_LIVER_SUPPORT_DAYS1] = cast([CC_LIVER_SUPPORT_DAYS1] as varchar(3))
	,[CC_ORGAN_SUPPORT_MAXIMUM1] = cast([CC_ORGAN_SUPPORT_MAXIMUM1] as varchar(2))
	,[CC_LEVEL_2_DAYS1] = cast([CC_LEVEL_2_DAYS1] as varchar(3))
	,[CC_LEVEL_3_DAYS1] = cast([CC_LEVEL_3_DAYS1] as varchar(3))
	,[CC_DISCHARGE_DATE1] = cast([CC_DISCHARGE_DATE1] as varchar(10))
	,[CC_DISCHARGE_TIME1] = cast([CC_DISCHARGE_TIME1] as varchar(8))
	,[CC_DISCHARGE_READY_DATE1] = cast([CC_DISCHARGE_READY_DATE1] as varchar(10))
	,[CC_DISCHARGE_READY_TIME1] = cast([CC_DISCHARGE_READY_TIME1] as varchar(8))
	,[CC_DISCHARGE_STATUS1] = cast([CC_DISCHARGE_STATUS1] as varchar(2))
	,[CC_DISCHARGE_DEST1] = cast([CC_DISCHARGE_DEST1] as varchar(2))
	,[CC_DISCHARGE_LOCATION1] = cast([CC_DISCHARGE_LOCATION1] as varchar(2))
	,[CC_LOCAL_IDENTIFIER2] = cast([CC_LOCAL_IDENTIFIER2] as varchar(8))
	,[CC_START_DATE2] = cast([CC_START_DATE2] as varchar(10))
	,[CC_START_TIME2] = cast([CC_START_TIME2] as varchar(8))
	,[CC_UNIT_FUNCTION2] = cast([CC_UNIT_FUNCTION2] as varchar(2))
	,[CC_UNIT_BED_CONFIG2] = cast([CC_UNIT_BED_CONFIG2] as varchar(2))
	,[CC_ADMISSION_SOURCE2] = cast([CC_ADMISSION_SOURCE2] as varchar(2))
	,[CC_SOURCE_LOCATION2] = cast([CC_SOURCE_LOCATION2] as varchar(2))
	,[CC_ADMISSION_TYPE2] = cast([CC_ADMISSION_TYPE2] as varchar(2))
	,[CC_GESTATION_LENGTH_AT_DELIV2] = cast([CC_GESTATION_LENGTH_AT_DELIV2] as varchar(2))
	,[CC_ACTIVITY_DATE2] = cast([CC_ACTIVITY_DATE2] as varchar(10))
	,[CC_PERSON_WEIGHT2] = cast([CC_PERSON_WEIGHT2] as varchar(7))
	,[CC_ACTIVITY_CODE2] = cast([CC_ACTIVITY_CODE2] as varchar(2))
	,[CC_HIGH_COST_DRUGS2] = cast([CC_HIGH_COST_DRUGS2] as varchar(4))
	,[CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS2] = cast([CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS2] as varchar(3))
	,[CC_BASIC_RESPIRATORY_SUPPORT_DAYS2] = cast([CC_BASIC_RESPIRATORY_SUPPORT_DAYS2] as varchar(3))
	,[CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS2] = cast([CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS2] as varchar(3))
	,[CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS2] = cast([CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS2] as varchar(3))
	,[CC_RENAL_SUPPORT_DAYS2] = cast([CC_RENAL_SUPPORT_DAYS2] as varchar(3))
	,[CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS2] = cast([CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS2] as varchar(3))
	,[CC_GASTRO_SUPPORT_DAYS2] = cast([CC_GASTRO_SUPPORT_DAYS2] as varchar(3))
	,[CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY2] = cast([CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY2] as varchar(3))
	,[CC_LIVER_SUPPORT_DAYS2] = cast([CC_LIVER_SUPPORT_DAYS2] as varchar(3))
	,[CC_ORGAN_SUPPORT_MAXIMUM2] = cast([CC_ORGAN_SUPPORT_MAXIMUM2] as varchar(2))
	,[CC_LEVEL_2_DAYS2] = cast([CC_LEVEL_2_DAYS2] as varchar(3))
	,[CC_LEVEL_3_DAYS2] = cast([CC_LEVEL_3_DAYS2] as varchar(3))
	,[CC_DISCHARGE_DATE2] = cast([CC_DISCHARGE_DATE2] as varchar(10))
	,[CC_DISCHARGE_TIME2] = cast([CC_DISCHARGE_TIME2] as varchar(8))
	,[CC_DISCHARGE_READY_DATE2] = cast([CC_DISCHARGE_READY_DATE2] as varchar(10))
	,[CC_DISCHARGE_READY_TIME2] = cast([CC_DISCHARGE_READY_TIME2] as varchar(8))
	,[CC_DISCHARGE_STATUS2] = cast([CC_DISCHARGE_STATUS2] as varchar(2))
	,[CC_DISCHARGE_DEST2] = cast([CC_DISCHARGE_DEST2] as varchar(2))
	,[CC_DISCHARGE_LOCATION2] = cast([CC_DISCHARGE_LOCATION2] as varchar(2))
	,[CC_LOCAL_IDENTIFIER3] = cast([CC_LOCAL_IDENTIFIER3] as varchar(8))
	,[CC_START_DATE3] = cast([CC_START_DATE3] as varchar(10))
	,[CC_START_TIME3] = cast([CC_START_TIME3] as varchar(8))
	,[CC_UNIT_FUNCTION3] = cast([CC_UNIT_FUNCTION3] as varchar(2))
	,[CC_UNIT_BED_CONFIG3] = cast([CC_UNIT_BED_CONFIG3] as varchar(2))
	,[CC_ADMISSION_SOURCE3] = cast([CC_ADMISSION_SOURCE3] as varchar(2))
	,[CC_SOURCE_LOCATION3] = cast([CC_SOURCE_LOCATION3] as varchar(2))
	,[CC_ADMISSION_TYPE3] = cast([CC_ADMISSION_TYPE3] as varchar(2))
	,[CC_GESTATION_LENGTH_AT_DELIV3] = cast([CC_GESTATION_LENGTH_AT_DELIV3] as varchar(2))
	,[CC_ACTIVITY_DATE3] = cast([CC_ACTIVITY_DATE3] as varchar(10))
	,[CC_PERSON_WEIGHT3] = cast([CC_PERSON_WEIGHT3] as varchar(7))
	,[CC_ACTIVITY_CODE3] = cast([CC_ACTIVITY_CODE3] as varchar(2))
	,[CC_HIGH_COST_DRUGS3] = cast([CC_HIGH_COST_DRUGS3] as varchar(4))
	,[CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS3] = cast([CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS3] as varchar(3))
	,[CC_BASIC_RESPIRATORY_SUPPORT_DAYS3] = cast([CC_BASIC_RESPIRATORY_SUPPORT_DAYS3] as varchar(3))
	,[CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS3] = cast([CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS3] as varchar(3))
	,[CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS3] = cast([CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS3] as varchar(3))
	,[CC_RENAL_SUPPORT_DAYS3] = cast([CC_RENAL_SUPPORT_DAYS3] as varchar(3))
	,[CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS3] = cast([CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS3] as varchar(3))
	,[CC_GASTRO_SUPPORT_DAYS3] = cast([CC_GASTRO_SUPPORT_DAYS3] as varchar(3))
	,[CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY3] = cast([CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY3] as varchar(3))
	,[CC_LIVER_SUPPORT_DAYS3] = cast([CC_LIVER_SUPPORT_DAYS3] as varchar(3))
	,[CC_ORGAN_SUPPORT_MAXIMUM3] = cast([CC_ORGAN_SUPPORT_MAXIMUM3] as varchar(2))
	,[CC_LEVEL_2_DAYS3] = cast([CC_LEVEL_2_DAYS3] as varchar(3))
	,[CC_LEVEL_3_DAYS3] = cast([CC_LEVEL_3_DAYS3] as varchar(3))
	,[CC_DISCHARGE_DATE3] = cast([CC_DISCHARGE_DATE3] as varchar(10))
	,[CC_DISCHARGE_TIME3] = cast([CC_DISCHARGE_TIME3] as varchar(8))
	,[CC_DISCHARGE_READY_DATE3] = cast([CC_DISCHARGE_READY_DATE3] as varchar(10))
	,[CC_DISCHARGE_READY_TIME3] = cast([CC_DISCHARGE_READY_TIME3] as varchar(8))
	,[CC_DISCHARGE_STATUS3] = cast([CC_DISCHARGE_STATUS3] as varchar(2))
	,[CC_DISCHARGE_DEST3] = cast([CC_DISCHARGE_DEST3] as varchar(2))
	,[CC_DISCHARGE_LOCATION3] = cast([CC_DISCHARGE_LOCATION3] as varchar(2))
	,[CC_LOCAL_IDENTIFIER4] = cast([CC_LOCAL_IDENTIFIER4] as varchar(8))
	,[CC_START_DATE4] = cast([CC_START_DATE4] as varchar(10))
	,[CC_START_TIME4] = cast([CC_START_TIME4] as varchar(8))
	,[CC_UNIT_FUNCTION4] = cast([CC_UNIT_FUNCTION4] as varchar(2))
	,[CC_UNIT_BED_CONFIG4] = cast([CC_UNIT_BED_CONFIG4] as varchar(2))
	,[CC_ADMISSION_SOURCE4] = cast([CC_ADMISSION_SOURCE4] as varchar(2))
	,[CC_SOURCE_LOCATION4] = cast([CC_SOURCE_LOCATION4] as varchar(2))
	,[CC_ADMISSION_TYPE4] = cast([CC_ADMISSION_TYPE4] as varchar(2))
	,[CC_GESTATION_LENGTH_AT_DELIV4] = cast([CC_GESTATION_LENGTH_AT_DELIV4] as varchar(2))
	,[CC_ACTIVITY_DATE4] = cast([CC_ACTIVITY_DATE4] as varchar(10))
	,[CC_PERSON_WEIGHT4] = cast([CC_PERSON_WEIGHT4] as varchar(7))
	,[CC_ACTIVITY_CODE4] = cast([CC_ACTIVITY_CODE4] as varchar(2))
	,[CC_HIGH_COST_DRUGS4] = cast([CC_HIGH_COST_DRUGS4] as varchar(4))
	,[CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS4] = cast([CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS4] as varchar(3))
	,[CC_BASIC_RESPIRATORY_SUPPORT_DAYS4] = cast([CC_BASIC_RESPIRATORY_SUPPORT_DAYS4] as varchar(3))
	,[CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS4] = cast([CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS4] as varchar(3))
	,[CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS4] = cast([CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS4] as varchar(3))
	,[CC_RENAL_SUPPORT_DAYS4] = cast([CC_RENAL_SUPPORT_DAYS4] as varchar(3))
	,[CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS4] = cast([CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS4] as varchar(3))
	,[CC_GASTRO_SUPPORT_DAYS4] = cast([CC_GASTRO_SUPPORT_DAYS4] as varchar(3))
	,[CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY4] = cast([CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY4] as varchar(3))
	,[CC_LIVER_SUPPORT_DAYS4] = cast([CC_LIVER_SUPPORT_DAYS4] as varchar(3))
	,[CC_ORGAN_SUPPORT_MAXIMUM4] = cast([CC_ORGAN_SUPPORT_MAXIMUM4] as varchar(2))
	,[CC_LEVEL_2_DAYS4] = cast([CC_LEVEL_2_DAYS4] as varchar(3))
	,[CC_LEVEL_3_DAYS4] = cast([CC_LEVEL_3_DAYS4] as varchar(3))
	,[CC_DISCHARGE_DATE4] = cast([CC_DISCHARGE_DATE4] as varchar(10))
	,[CC_DISCHARGE_TIME4] = cast([CC_DISCHARGE_TIME4] as varchar(8))
	,[CC_DISCHARGE_READY_DATE4] = cast([CC_DISCHARGE_READY_DATE4] as varchar(10))
	,[CC_DISCHARGE_READY_TIME4] = cast([CC_DISCHARGE_READY_TIME4] as varchar(8))
	,[CC_DISCHARGE_STATUS4] = cast([CC_DISCHARGE_STATUS4] as varchar(2))
	,[CC_DISCHARGE_DEST4] = cast([CC_DISCHARGE_DEST4] as varchar(2))
	,[CC_DISCHARGE_LOCATION4] = cast([CC_DISCHARGE_LOCATION4] as varchar(2))
	,[CC_LOCAL_IDENTIFIER5] = cast([CC_LOCAL_IDENTIFIER5] as varchar(8))
	,[CC_START_DATE5] = cast([CC_START_DATE5] as varchar(10))
	,[CC_START_TIME5] = cast([CC_START_TIME5] as varchar(8))
	,[CC_UNIT_FUNCTION5] = cast([CC_UNIT_FUNCTION5] as varchar(2))
	,[CC_UNIT_BED_CONFIG5] = cast([CC_UNIT_BED_CONFIG5] as varchar(2))
	,[CC_ADMISSION_SOURCE5] = cast([CC_ADMISSION_SOURCE5] as varchar(2))
	,[CC_SOURCE_LOCATION5] = cast([CC_SOURCE_LOCATION5] as varchar(2))
	,[CC_ADMISSION_TYPE5] = cast([CC_ADMISSION_TYPE5] as varchar(2))
	,[CC_GESTATION_LENGTH_AT_DELIV5] = cast([CC_GESTATION_LENGTH_AT_DELIV5] as varchar(2))
	,[CC_ACTIVITY_DATE5] = cast([CC_ACTIVITY_DATE5] as varchar(10))
	,[CC_PERSON_WEIGHT5] = cast([CC_PERSON_WEIGHT5] as varchar(7))
	,[CC_ACTIVITY_CODE5] = cast([CC_ACTIVITY_CODE5] as varchar(2))
	,[CC_HIGH_COST_DRUGS5] = cast([CC_HIGH_COST_DRUGS5] as varchar(4))
	,[CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS5] = cast([CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS5] as varchar(3))
	,[CC_BASIC_RESPIRATORY_SUPPORT_DAYS5] = cast([CC_BASIC_RESPIRATORY_SUPPORT_DAYS5] as varchar(3))
	,[CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS5] = cast([CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS5] as varchar(3))
	,[CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS5] = cast([CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS5] as varchar(3))
	,[CC_RENAL_SUPPORT_DAYS5] = cast([CC_RENAL_SUPPORT_DAYS5] as varchar(3))
	,[CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS5] = cast([CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS5] as varchar(3))
	,[CC_GASTRO_SUPPORT_DAYS5] = cast([CC_GASTRO_SUPPORT_DAYS5] as varchar(3))
	,[CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY5] = cast([CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY5] as varchar(3))
	,[CC_LIVER_SUPPORT_DAYS5] = cast([CC_LIVER_SUPPORT_DAYS5] as varchar(3))
	,[CC_ORGAN_SUPPORT_MAXIMUM5] = cast([CC_ORGAN_SUPPORT_MAXIMUM5] as varchar(2))
	,[CC_LEVEL_2_DAYS5] = cast([CC_LEVEL_2_DAYS5] as varchar(3))
	,[CC_LEVEL_3_DAYS5] = cast([CC_LEVEL_3_DAYS5] as varchar(3))
	,[CC_DISCHARGE_DATE5] = cast([CC_DISCHARGE_DATE5] as varchar(10))
	,[CC_DISCHARGE_TIME5] = cast([CC_DISCHARGE_TIME5] as varchar(8))
	,[CC_DISCHARGE_READY_DATE5] = cast([CC_DISCHARGE_READY_DATE5] as varchar(10))
	,[CC_DISCHARGE_READY_TIME5] = cast([CC_DISCHARGE_READY_TIME5] as varchar(8))
	,[CC_DISCHARGE_STATUS5] = cast([CC_DISCHARGE_STATUS5] as varchar(2))
	,[CC_DISCHARGE_DEST5] = cast([CC_DISCHARGE_DEST5] as varchar(2))
	,[CC_DISCHARGE_LOCATION5] = cast([CC_DISCHARGE_LOCATION5] as varchar(2))
	,[CC_LOCAL_IDENTIFIER6] = cast([CC_LOCAL_IDENTIFIER6] as varchar(8))
	,[CC_START_DATE6] = cast([CC_START_DATE6] as varchar(10))
	,[CC_START_TIME6] = cast([CC_START_TIME6] as varchar(8))
	,[CC_UNIT_FUNCTION6] = cast([CC_UNIT_FUNCTION6] as varchar(2))
	,[CC_UNIT_BED_CONFIG6] = cast([CC_UNIT_BED_CONFIG6] as varchar(2))
	,[CC_ADMISSION_SOURCE6] = cast([CC_ADMISSION_SOURCE6] as varchar(2))
	,[CC_SOURCE_LOCATION6] = cast([CC_SOURCE_LOCATION6] as varchar(2))
	,[CC_ADMISSION_TYPE6] = cast([CC_ADMISSION_TYPE6] as varchar(2))
	,[CC_GESTATION_LENGTH_AT_DELIV6] = cast([CC_GESTATION_LENGTH_AT_DELIV6] as varchar(2))
	,[CC_ACTIVITY_DATE6] = cast([CC_ACTIVITY_DATE6] as varchar(10))
	,[CC_PERSON_WEIGHT6] = cast([CC_PERSON_WEIGHT6] as varchar(7))
	,[CC_ACTIVITY_CODE6] = cast([CC_ACTIVITY_CODE6] as varchar(2))
	,[CC_HIGH_COST_DRUGS6] = cast([CC_HIGH_COST_DRUGS6] as varchar(4))
	,[CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS6] = cast([CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS6] as varchar(3))
	,[CC_BASIC_RESPIRATORY_SUPPORT_DAYS6] = cast([CC_BASIC_RESPIRATORY_SUPPORT_DAYS6] as varchar(3))
	,[CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS6] = cast([CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS6] as varchar(3))
	,[CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS6] = cast([CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS6] as varchar(3))
	,[CC_RENAL_SUPPORT_DAYS6] = cast([CC_RENAL_SUPPORT_DAYS6] as varchar(3))
	,[CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS6] = cast([CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS6] as varchar(3))
	,[CC_GASTRO_SUPPORT_DAYS6] = cast([CC_GASTRO_SUPPORT_DAYS6] as varchar(3))
	,[CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY6] = cast([CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY6] as varchar(3))
	,[CC_LIVER_SUPPORT_DAYS6] = cast([CC_LIVER_SUPPORT_DAYS6] as varchar(3))
	,[CC_ORGAN_SUPPORT_MAXIMUM6] = cast([CC_ORGAN_SUPPORT_MAXIMUM6] as varchar(2))
	,[CC_LEVEL_2_DAYS6] = cast([CC_LEVEL_2_DAYS6] as varchar(3))
	,[CC_LEVEL_3_DAYS6] = cast([CC_LEVEL_3_DAYS6] as varchar(3))
	,[CC_DISCHARGE_DATE6] = cast([CC_DISCHARGE_DATE6] as varchar(10))
	,[CC_DISCHARGE_TIME6] = cast([CC_DISCHARGE_TIME6] as varchar(8))
	,[CC_DISCHARGE_READY_DATE6] = cast([CC_DISCHARGE_READY_DATE6] as varchar(10))
	,[CC_DISCHARGE_READY_TIME6] = cast([CC_DISCHARGE_READY_TIME6] as varchar(8))
	,[CC_DISCHARGE_STATUS6] = cast([CC_DISCHARGE_STATUS6] as varchar(2))
	,[CC_DISCHARGE_DEST6] = cast([CC_DISCHARGE_DEST6] as varchar(2))
	,[CC_DISCHARGE_LOCATION6] = cast([CC_DISCHARGE_LOCATION6] as varchar(2))
	,[CC_LOCAL_IDENTIFIER7] = cast([CC_LOCAL_IDENTIFIER7] as varchar(8))
	,[CC_START_DATE7] = cast([CC_START_DATE7] as varchar(10))
	,[CC_START_TIME7] = cast([CC_START_TIME7] as varchar(8))
	,[CC_UNIT_FUNCTION7] = cast([CC_UNIT_FUNCTION7] as varchar(2))
	,[CC_UNIT_BED_CONFIG7] = cast([CC_UNIT_BED_CONFIG7] as varchar(2))
	,[CC_ADMISSION_SOURCE7] = cast([CC_ADMISSION_SOURCE7] as varchar(2))
	,[CC_SOURCE_LOCATION7] = cast([CC_SOURCE_LOCATION7] as varchar(2))
	,[CC_ADMISSION_TYPE7] = cast([CC_ADMISSION_TYPE7] as varchar(2))
	,[CC_GESTATION_LENGTH_AT_DELIV7] = cast([CC_GESTATION_LENGTH_AT_DELIV7] as varchar(2))
	,[CC_ACTIVITY_DATE7] = cast([CC_ACTIVITY_DATE7] as varchar(10))
	,[CC_PERSON_WEIGHT7] = cast([CC_PERSON_WEIGHT7] as varchar(7))
	,[CC_ACTIVITY_CODE7] = cast([CC_ACTIVITY_CODE7] as varchar(2))
	,[CC_HIGH_COST_DRUGS7] = cast([CC_HIGH_COST_DRUGS7] as varchar(4))
	,[CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS7] = cast([CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS7] as varchar(3))
	,[CC_BASIC_RESPIRATORY_SUPPORT_DAYS7] = cast([CC_BASIC_RESPIRATORY_SUPPORT_DAYS7] as varchar(3))
	,[CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS7] = cast([CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS7] as varchar(3))
	,[CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS7] = cast([CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS7] as varchar(3))
	,[CC_RENAL_SUPPORT_DAYS7] = cast([CC_RENAL_SUPPORT_DAYS7] as varchar(3))
	,[CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS7] = cast([CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS7] as varchar(3))
	,[CC_GASTRO_SUPPORT_DAYS7] = cast([CC_GASTRO_SUPPORT_DAYS7] as varchar(3))
	,[CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY7] = cast([CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY7] as varchar(3))
	,[CC_LIVER_SUPPORT_DAYS7] = cast([CC_LIVER_SUPPORT_DAYS7] as varchar(3))
	,[CC_ORGAN_SUPPORT_MAXIMUM7] = cast([CC_ORGAN_SUPPORT_MAXIMUM7] as varchar(2))
	,[CC_LEVEL_2_DAYS7] = cast([CC_LEVEL_2_DAYS7] as varchar(3))
	,[CC_LEVEL_3_DAYS7] = cast([CC_LEVEL_3_DAYS7] as varchar(3))
	,[CC_DISCHARGE_DATE7] = cast([CC_DISCHARGE_DATE7] as varchar(10))
	,[CC_DISCHARGE_TIME7] = cast([CC_DISCHARGE_TIME7] as varchar(8))
	,[CC_DISCHARGE_READY_DATE7] = cast([CC_DISCHARGE_READY_DATE7] as varchar(10))
	,[CC_DISCHARGE_READY_TIME7] = cast([CC_DISCHARGE_READY_TIME7] as varchar(8))
	,[CC_DISCHARGE_STATUS7] = cast([CC_DISCHARGE_STATUS7] as varchar(2))
	,[CC_DISCHARGE_DEST7] = cast([CC_DISCHARGE_DEST7] as varchar(2))
	,[CC_DISCHARGE_LOCATION7] = cast([CC_DISCHARGE_LOCATION7] as varchar(2))
	,[CC_LOCAL_IDENTIFIER8] = cast([CC_LOCAL_IDENTIFIER8] as varchar(8))
	,[CC_START_DATE8] = cast([CC_START_DATE8] as varchar(10))
	,[CC_START_TIME8] = cast([CC_START_TIME8] as varchar(8))
	,[CC_UNIT_FUNCTION8] = cast([CC_UNIT_FUNCTION8] as varchar(2))
	,[CC_UNIT_BED_CONFIG8] = cast([CC_UNIT_BED_CONFIG8] as varchar(2))
	,[CC_ADMISSION_SOURCE8] = cast([CC_ADMISSION_SOURCE8] as varchar(2))
	,[CC_SOURCE_LOCATION8] = cast([CC_SOURCE_LOCATION8] as varchar(2))
	,[CC_ADMISSION_TYPE8] = cast([CC_ADMISSION_TYPE8] as varchar(2))
	,[CC_GESTATION_LENGTH_AT_DELIV8] = cast([CC_GESTATION_LENGTH_AT_DELIV8] as varchar(2))
	,[CC_ACTIVITY_DATE8] = cast([CC_ACTIVITY_DATE8] as varchar(10))
	,[CC_PERSON_WEIGHT8] = cast([CC_PERSON_WEIGHT8] as varchar(7))
	,[CC_ACTIVITY_CODE8] = cast([CC_ACTIVITY_CODE8] as varchar(2))
	,[CC_HIGH_COST_DRUGS8] = cast([CC_HIGH_COST_DRUGS8] as varchar(4))
	,[CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS8] = cast([CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS8] as varchar(3))
	,[CC_BASIC_RESPIRATORY_SUPPORT_DAYS8] = cast([CC_BASIC_RESPIRATORY_SUPPORT_DAYS8] as varchar(3))
	,[CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS8] = cast([CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS8] as varchar(3))
	,[CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS8] = cast([CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS8] as varchar(3))
	,[CC_RENAL_SUPPORT_DAYS8] = cast([CC_RENAL_SUPPORT_DAYS8] as varchar(3))
	,[CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS8] = cast([CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS8] as varchar(3))
	,[CC_GASTRO_SUPPORT_DAYS8] = cast([CC_GASTRO_SUPPORT_DAYS8] as varchar(3))
	,[CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY8] = cast([CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY8] as varchar(3))
	,[CC_LIVER_SUPPORT_DAYS8] = cast([CC_LIVER_SUPPORT_DAYS8] as varchar(3))
	,[CC_ORGAN_SUPPORT_MAXIMUM8] = cast([CC_ORGAN_SUPPORT_MAXIMUM8] as varchar(2))
	,[CC_LEVEL_2_DAYS8] = cast([CC_LEVEL_2_DAYS8] as varchar(3))
	,[CC_LEVEL_3_DAYS8] = cast([CC_LEVEL_3_DAYS8] as varchar(3))
	,[CC_DISCHARGE_DATE8] = cast([CC_DISCHARGE_DATE8] as varchar(10))
	,[CC_DISCHARGE_TIME8] = cast([CC_DISCHARGE_TIME8] as varchar(8))
	,[CC_DISCHARGE_READY_DATE8] = cast([CC_DISCHARGE_READY_DATE8] as varchar(10))
	,[CC_DISCHARGE_READY_TIME8] = cast([CC_DISCHARGE_READY_TIME8] as varchar(8))
	,[CC_DISCHARGE_STATUS8] = cast([CC_DISCHARGE_STATUS8] as varchar(2))
	,[CC_DISCHARGE_DEST8] = cast([CC_DISCHARGE_DEST8] as varchar(2))
	,[CC_DISCHARGE_LOCATION8] = cast([CC_DISCHARGE_LOCATION8] as varchar(2))
	,[CC_LOCAL_IDENTIFIER9] = cast([CC_LOCAL_IDENTIFIER9] as varchar(8))
	,[CC_START_DATE9] = cast([CC_START_DATE9] as varchar(10))
	,[CC_START_TIME9] = cast([CC_START_TIME9] as varchar(8))
	,[CC_UNIT_FUNCTION9] = cast([CC_UNIT_FUNCTION9] as varchar(2))
	,[CC_UNIT_BED_CONFIG9] = cast([CC_UNIT_BED_CONFIG9] as varchar(2))
	,[CC_ADMISSION_SOURCE9] = cast([CC_ADMISSION_SOURCE9] as varchar(2))
	,[CC_SOURCE_LOCATION9] = cast([CC_SOURCE_LOCATION9] as varchar(2))
	,[CC_ADMISSION_TYPE9] = cast([CC_ADMISSION_TYPE9] as varchar(2))
	,[CC_GESTATION_LENGTH_AT_DELIV9] = cast([CC_GESTATION_LENGTH_AT_DELIV9] as varchar(2))
	,[CC_ACTIVITY_DATE9] = cast([CC_ACTIVITY_DATE9] as varchar(10))
	,[CC_PERSON_WEIGHT9] = cast([CC_PERSON_WEIGHT9] as varchar(7))
	,[CC_ACTIVITY_CODE9] = cast([CC_ACTIVITY_CODE9] as varchar(2))
	,[CC_HIGH_COST_DRUGS9] = cast([CC_HIGH_COST_DRUGS9] as varchar(4))
	,[CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS9] = cast([CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS9] as varchar(3))
	,[CC_BASIC_RESPIRATORY_SUPPORT_DAYS9] = cast([CC_BASIC_RESPIRATORY_SUPPORT_DAYS9] as varchar(3))
	,[CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS9] = cast([CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS9] as varchar(3))
	,[CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS9] = cast([CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS9] as varchar(3))
	,[CC_RENAL_SUPPORT_DAYS9] = cast([CC_RENAL_SUPPORT_DAYS9] as varchar(3))
	,[CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS9] = cast([CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS9] as varchar(3))
	,[CC_GASTRO_SUPPORT_DAYS9] = cast([CC_GASTRO_SUPPORT_DAYS9] as varchar(3))
	,[CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY9] = cast([CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY9] as varchar(3))
	,[CC_LIVER_SUPPORT_DAYS9] = cast([CC_LIVER_SUPPORT_DAYS9] as varchar(3))
	,[CC_ORGAN_SUPPORT_MAXIMUM9] = cast([CC_ORGAN_SUPPORT_MAXIMUM9] as varchar(2))
	,[CC_LEVEL_2_DAYS9] = cast([CC_LEVEL_2_DAYS9] as varchar(3))
	,[CC_LEVEL_3_DAYS9] = cast([CC_LEVEL_3_DAYS9] as varchar(3))
	,[CC_DISCHARGE_DATE9] = cast([CC_DISCHARGE_DATE9] as varchar(10))
	,[CC_DISCHARGE_TIME9] = cast([CC_DISCHARGE_TIME9] as varchar(8))
	,[CC_DISCHARGE_READY_DATE9] = cast([CC_DISCHARGE_READY_DATE9] as varchar(10))
	,[CC_DISCHARGE_READY_TIME9] = cast([CC_DISCHARGE_READY_TIME9] as varchar(8))
	,[CC_DISCHARGE_STATUS9] = cast([CC_DISCHARGE_STATUS9] as varchar(2))
	,[CC_DISCHARGE_DEST9] = cast([CC_DISCHARGE_DEST9] as varchar(2))
	,[CC_DISCHARGE_LOCATION9] = cast([CC_DISCHARGE_LOCATION9] as varchar(2))
	,[MOM_NHS_NUM] = cast([MOM_NHS_NUM] as varchar(10))
	,[MOM_PAT_ID] = cast([MOM_PAT_ID] as varchar(10))
	,[MOM_PAT_ID_ORG] = cast([MOM_PAT_ID_ORG] as varchar(5))
	,[MOM_NNN_STATUS] = cast([MOM_NNN_STATUS] as varchar(2))
	,[MOM USUAL ADDRESS LINE 1] = cast([MOM USUAL ADDRESS LINE 1] as varchar(35))
	,[MOM USUAL ADDRESS LINE 2] = cast([MOM USUAL ADDRESS LINE 2] as varchar(35))
	,[MOM USUAL ADDRESS LINE 3] = cast([MOM USUAL ADDRESS LINE 3] as varchar(35))
	,[MOM USUAL ADDRESS LINE 4] = cast([MOM USUAL ADDRESS LINE 4] as varchar(35))
	,[MOM USUAL ADDRESS LINE 5] = cast([MOM USUAL ADDRESS LINE 5] as varchar(35))
	,[MOM POSTCODE OF USUAL ADDRESS] = cast([MOM POSTCODE OF USUAL ADDRESS] as varchar(8))
	,[PROVIDER REFERENCE NUMBER] = cast([PROVIDER REFERENCE NUMBER] as varchar(17))
	,[ORGANISATION CODE (LOCAL PAT ID)] = cast([ORGANISATION CODE (LOCAL PAT ID)] as varchar(5))
	,[UBRN] = cast([UBRN] as varchar(12))
	,[CARE_PATHWAY_ID] = cast([CARE_PATHWAY_ID] as varchar(20))
	,[CARE_PATHWAY_ID_ORG] = cast([CARE_PATHWAY_ID_ORG] as varchar(5))
	,[REF_TO_TREAT_PERIOD_STATUS] = cast([REF_TO_TREAT_PERIOD_STATUS] as varchar(2))
	,[REF_TO_TREAT_PERIOD_START_DATE] = cast([REF_TO_TREAT_PERIOD_START_DATE] as varchar(10))
	,[REF_TO_TREAT_PERIOD_END_DATE] = cast([REF_TO_TREAT_PERIOD_END_DATE] as varchar(10))
from
(
select
--KO removed 02/10/2012
	 --[PRIME RECIPIENT] =
		--case
		--when PurchaserCode in
		--		(
		--		 'TDH00' -- Overseas Visitor exempt from charges
		--		,'VPP00' -- Private Patient
		--		)
		--then PurchaserCode
		--else
		--	coalesce(
		--		 left(PCTofResidenceCode, 3) + '00'
		--		,PurchaserCode
		--	)
		--end
	-- Apply codes for Exemptions and private patients.
	-- Otherwise use PCTofResidenceCodeNew* (1),
	-- else purchaser code (map SHA to 5NT first) (2),
	-- else 5NT00 (3).
	-- * Unless PCTofResidenceCodeNew = X98, Q99 or SHA then skip to value (2)
	--[PRIME RECIPIENT PCT] =
	--	case
	--	when OverseasStatus in ('1','2') then 'TDH00'
	--	--when OverseasStatus in ('3','4','9') then 'VPP00'
	--	when left(PCTofResidenceCode, 1) = 'Y' then 'TDH00'
	--	when PCTofResidenceCode in ('X98', 'Q99', 'SHA') then 
	--		coalesce(
	--			case when PurchaserCodePCT = 'SHA00' then '5NT00' else PurchaserCodePCT end
	--			, '5NT00'
	--			)
	--	else
	--		coalesce(
	--			 left(PCTofResidenceCode, 3) + '00'
	--			,case when PurchaserCodePCT = 'SHA00' then '5NT00' else PurchaserCodePCT end
	--			,'5NT00'
	--			)
	--	end

	[PRIME RECIPIENT CCG] =
		case
		when OverseasStatus in ('1','2') then 'TDH00'
		--when OverseasStatus in ('3','4','9') then 'VPP00'
		when left(CCGofResidenceCode, 1) = 'Y' then 'TDH00'
		when CCGofResidenceCode in ('X98', 'Q99', 'SHA') then 
			coalesce(
				case when PurchaserCodeCCG = 'SHA00' then '01N00' else PurchaserCodeCCG end
				--PurchaserCodeCCG 
				, '01N00'
				)
		else
			coalesce(
				 left(CCGofResidenceCode, 3) + '00'
				,case when PurchaserCodeCCG = 'SHA00' then '01N00' else PurchaserCodeCCG end
				--,PurchaserCodeCCG 
				,'01N00'
				)
		end
	--Optional - added for records where prime recipient resolves to 'TDH00'
	--use same code as for generating purchaser
	--Where  prime recipient resolves to 'VPP00'
		--and valid practice code, use PCT of practice
	--,[COPY RECIPIENT 1 PCT] =
	--	case
	--		when OverseasStatus in ('1','2') 
	--			then '5NT00' --For OS 1&2 Purchaser code will always wbe '5NT00'
	--		when left(PCTofResidenceCode, 1) = 'Y' 
	--			--then left(PCTofResidenceCodeNew, 3) + '00'
	--			then 
	--				case when Coalesce(
	--					PurchaserCodePCT
	--					,case when PCTofResidenceCode = 'X98' then '5NT00' end
	--					,left(PCTofResidenceCode + '00', 5)
	--					,'5NT00') != 'TDH00'
					
	--				then Coalesce(
	--					PurchaserCodePCT
	--					,case when PCTofResidenceCode = 'X98' then '5NT00' end
	--					,left(PCTofResidenceCode + '00', 5)
	--					,'5NT00')
	--				end
							
	--		when OverseasStatus in ('3','4') 
	--			then 
	--				case when isnull(PracticePCT,'SHA') <> 'SHA'
	--					then PracticePCT + '00'
	--			end
	--	end

	,[COPY RECIPIENT 1 CCG] =
		case
			when OverseasStatus in ('1','2') 
				then '01N00' --For OS 1&2 Purchaser code will always wbe '01N00'
			when left(CCGofResidenceCode, 1) = 'Y' 
				--then left(PCTofResidenceCodeNew, 3) + '00'
				then 
					case when Coalesce(
						PurchaserCodeCCG
						,case when CCGofResidenceCode = 'X98' then '01N00' end
						,left(CCGofResidenceCode + '00', 5)
						,'01N00') != 'TDH00'
					
					then Coalesce(
						PurchaserCodeCCG
						,case when CCGofResidenceCode = 'X98' then '01N00' end
						,left(CCGofResidenceCode + '00', 5)
						,'01N00')
					end
							
			when OverseasStatus in ('3','4') 
				then 
					case when isnull(PracticeCCG,'SHA') <> 'SHA'
						then PracticeCCG + '00'
				end
		end
		
	,[COPY RECIPIENT 2] = null --Optional
	,[COPY RECIPIENT 3] = null --Optional
	,[COPY RECIPIENT 4] = null --Optional
	,[COPY RECIPIENT 5] = null --Optional
	,[SENDER] = 'RM200'
	,[CDS GROUP] = CDSGroup
	,[CDS TYPE] = CDSType
	,[CDS_ID] = UniqueEpisodeSerialNo
	,[TEST_FLAG] = (select TextValue from dbo.Parameter where Parameter = 'CDSTESTINDICATOR')
	,[UPDATE TYPE] = null
	,[PROTOCOL IDENTIFIER] = (select TextValue from dbo.Parameter where Parameter = 'CDSPROTOCOLIDENTIFIER')

	,[BULKSTART] =
		(
		select
			left(
				CONVERT(varchar, DateValue, 120)
				,10
			)
		from
			dbo.Parameter
		where
			Parameter = 'CDSREPORTPERIODSTARTDATE'
		)

	,[BULKEND] =
		(
		select
			left(
				CONVERT(varchar, DateValue, 120)
				,10
			)
		from
			dbo.Parameter
		where
			Parameter = 'CDSREPORTPERIODENDDATE'
		)

	,[DATETIME_CREATED] =
			--left(
			--	CONVERT(varchar, GETDATE(), 120)
			--	,16
			--)
		CDSUpdateDate + ' ' + LEFT(CDSUpdateTime, 2) + ':' + Substring(CDSUpdateTime, 3, 2)
	
	,[PROVIDER] = ProviderCode
	--,[PURCHASER] = PurchaserCode --KO changed 14/11/2012
	-- Use purchaser from purchaser table or practice lookup* [PurchaserCodeNew] (1), 
	-- else use PCT of residence code (map X98 to 5NT first) (2),
	-- else use 5NT (3).
	-- * Unless = SHA00 (which has been generated from null practice lookup) then skip to value (2)
	--,[PURCHASER PCT] = 
	--	case when PurchaserCodePCT = 'SHA00'
	--		then Coalesce(
	--		case when PCTofResidenceCode = 'X98' then '5NT00' end
	--		,left(PCTofResidenceCode + '00', 5)
	--		,'5NT00')
	--	else Coalesce(
	--		PurchaserCodePCT
	--		,case when PCTofResidenceCode = 'X98' then '5NT00' end
	--		,left(PCTofResidenceCode + '00', 5)
	--		,'5NT00')
	--	end
		
	,[PURCHASER CCG] = 
		case when PurchaserCodeCCG = 'SHA00'
			then Coalesce(
			case when CCGofResidenceCode = 'X98' then '01N00' end
			,left(CCGofResidenceCode + '00', 5)
			,'01N00')
		else Coalesce(
			PurchaserCodeCCG
			,case when CCGofResidenceCode = 'X98' then '01N00' end
			,left(CCGofResidenceCode + '00', 5)
			,'01N00')
		end
		
	,[SERIAL_NO] = CommissioningSerialNo
	,[CONTRACT_LINE_NO] = NHSServiceAgreementLineNo
	--,[PURCH_REF] = PurchaserReferenceNo
	,[PURCH_REF] = CommissionerReferenceNo
	,[NHS_NO] = NHSNumber
	,[NNN_STATUS_IND] = NNNStatusIndicator
	,[NAME_FORMAT] = 1
	,[ADDRESS_FORMAT] = 1
	,[NAME] = PatientSurname
	,[FORENAME] = PatientForename
	,[HOMEADD1] = PatientAddress1
	,[HOMEADD2] = PatientAddress2
	,[HOMEADD3] = PatientAddress3
	,[HOMEADD4] = PatientAddress4
	,[HOMEADD5] = PatientAddress5
	,[POSTCODE] = PostcodeAmended
		--,[HA] = HACode KO changed 20/09/2012
	--,[HA PCT] = 
	--	case 		
	--		--Where no Postcode or GP then organisation code for unknown practice 'V81999' maps to 'SHA'
	--		--Change this to 'Q99'
	--		when PCTofResidenceCode = 'SHA' then 'Q99'
	--		--Wales, Scotland, NI, CI
	--		when left(PCTofResidenceCode, 1) in ('7','S','Y', 'Z', '6') then 'X98'
	--		else PCTofResidenceCode
	--	end
		
	,[HA CCG] = 
		case 		
			--Where no Postcode or GP then organisation code for unknown practice 'V81999' maps to 'SHA'
			--Change this to 'Q99'
			when CCGofResidenceCode = 'SHA' then 'Q99'
			--Wales, Scotland, NI, CI
			when left(CCGofResidenceCode, 1) in ('7','S','Y', 'Z', '6') then 'X98'
			else Coalesce(CCGofResidenceCode, 'Q99')
		end
		
	,[SEX] = SexCode
	,[CARER_SUPPORT_IND] = CarerSupportIndicator
	,[DOB] = DateOfBirth
	,[GPREG] = RegisteredGpCode
	,[PRACREG] = RegisteredGpPracticeCode
	,[ETHNICOR] = EthnicGroupCode
	,[LOCPATID] = LocalPatientID
	,[REFERRER] = ReferrerCode
	,[REF_ORG] = ReferringOrganisationCode
	,[PROV_SPELL] = ProviderSpellNo
	,[ADMIN_CATEGORY] = AdministrationCategoryCode
	,[LEGAL_STATUS] = LegalStatusClassificationCode
	,[ELECDATE] = DecidedToAdmitDate
	,[EARLIEST_REASONABLE_DATE_OFFER] = EarliestReasonableOfferDate
	,[ADMIDATE] = AdmissionDate
	,[AGE_ON_ADMISSION_DATE] = AgeOnAdmission
	,[ADMIMETH] = AdmissionMethodCode
	,[ELECDUR] = DurationOfElectiveWait
	,[MAN_INTENT] = ManagementIntentionCode
	,[ADMISORC] = SourceOfAdmissionCode
	,[DISCHARGE_READY_DATE] = DischargeReadyDate
	,[DISDATE] = DischargeDate
	,[DISMETH] = DischargeMethodCode
	,[DISDEST] = DischargeDestinationCode
	,[CLASSPAT] = PatientClassificationCode
	,[EPIORDER] = EpisodeNo
	,[LAST_IN_SPELL] = LastEpisodeInSpellIndicator
	,[EPISTART] = EpisodeStartDate
	,[EPIEND] = EpisodeEndDate
	,[ACTIVITY_DATE] = CDSActivityDate
	,[AGE_AT_CDS_ACTIVITY_DATE] = AgeAtCDSActivityDate
	,[MAINSPEF] = MainSpecialtyCode
	,[CONS_MAINSPEF] = TreatmentFunctionCode  
	--KO changed 10/10 to replace null TFC with main spec. Changed back
	--,[CONS_MAINSPEF] = IsNull(TreatmentFunctionCode, MainSpecialtyCode)
	,[CONS_CODE] = ConsultantCode
	,[LOCATION_CLASS_START] = StartOfEpisodeLocationClassCode
	,[SITECODE_AT_EPISODE_START] = StartOfEpisodeSiteCode
	,[INT_CC_INTENSITY_START] = StartOfEpisodeIntendedClinicalIntensityCode
	,[AGE_GRP_INT_START] = StartOfEpisodeAgeGroupIntendedCode
	,[SEX_OF_PATIENTS_START] = StartOfEpisodeSexOfPatientsCode
	,[WARD_DAY_PERIOD_AVAIL_START] = StartOfEpisodeWardDayPeriodAvailabilityCode
	,[WARD_NIGHT_PERIOD_AVAIL_START] = StartOfEpisodeWardNightPeriodAvailabilityCode
	,[LOCATION_CLASS_WARD] = WardStayLocationClassCode
	,[SITECODE_DURING_WARD_STAY] = WardStaySiteCode
	,[INT_CC_INTENSITY_WARD] = WardStayIntendedClinicalIntensityCode
	,[AGE_GRP_INT_WARD]= WardStayAgeGroupIntendedCode
	,[SEX_OF_PATIENTS_WARD] = WardStaySexOfPatientsCode
	,[WARD_DAY_PERIOD_AVAIL_WARD] = WardStayWardDayPeriodAvailabilityCode
	,[WARD_NIGHT_PERIOD_AVAIL_WARD] = WardStayWardNightPeriodAvailabilityCode
	,[WARD_STAY_START_DATE] = WardStayStartDate
	,[WARD_STAY_END_DATE] = WardStayEndDate
	,[LOCATION_CLASS_END] = EndOfEpisodeLocationClassCode
	,[SITECODE_AT_EPISODE_END] = EndOfEpisodeSiteCode
	,[INT_CC_INTENSITY_END] = EndOfEpisodeIntendedClinicalIntensityCode
	,[AGE_GRP_INT_END] = EndOfEpisodeAgeGroupIntendedCode
	,[SEX_OF_PATIENTS_END] = EndOfEpisodeSexOfPatientsCode
	,[WARD_DAY_PERIOD_AVAIL_END] = EndOfEpisodeWardDayPeriodAvailabilityCode
	,[WARD_NIGHT_PERIOD_AVAIL_END] = EndOfEpisodeWardNightPeriodAvailabilityCode
	,[FIRST_REG_ADM] = FirstRegularDayNightAdmission
	,[NEONATAL_LEVEL] = NeonatalLevelOfCare
	,[ADMISTAT] = null
	,[ICD_DIAG_SCHEME] = DiagnosisSchemeInUse
	,[PRIMARY_DIAG] = DiagnosisCode1
	,[SECONDARY_DIAG1] = DiagnosisCode2
	,[SECONDARY_DIAG2] = DiagnosisCode3
	,[SECONDARY_DIAG3] = DiagnosisCode4
	,[SECONDARY_DIAG4] = DiagnosisCode5
	,[SECONDARY_DIAG5] = DiagnosisCode6
	,[SECONDARY_DIAG6] = DiagnosisCode7
	,[SECONDARY_DIAG7] = DiagnosisCode8
	,[SECONDARY_DIAG8] = DiagnosisCode9
	,[SECONDARY_DIAG9] = DiagnosisCode10
	,[SECONDARY_DIAG10] = DiagnosisCode11
	,[SECONDARY_DIAG11] = DiagnosisCode12
	,[SECONDARY_DIAG12] = DiagnosisCode13
	,[READ_DIAG_SCHEME] = null
	,[PRIMARY_READ_DIAG1] = null
	,[READ_DIAG2] = null
	,[READ_DIAG3] = null
	,[READ_DIAG4] = null
	,[READ_DIAG5] = null
	,[READ_DIAG6] = null
	,[READ_DIAG7] = null
	,[READ_DIAG8] = null
	,[READ_DIAG9] = null
	,[READ_DIAG10] = null
	,[READ_DIAG11] = null
	,[READ_DIAG12] = null
	,[READ_DIAG13] = null
	,[READ_DIAG14] = null
	,[OPER_STATUS] = OperationStatus
	,[OPCS_PROC_SCHEME] = ProcedureSchemeInUse
	,[PRIMARY_PROC] = OperationCode1
	,[PRIMARY_PROC_DATE] = OperationDate1
	,[PROC2] = OperationCode2
	,[PROC2_DATE] = OperationDate2
	,[PROC3] = OperationCode3
	,[PROC3_DATE] = OperationDate3
	,[PROC4] = OperationCode4
	,[PROC4_DATE] = OperationDate4
	,[PROC5] = OperationCode5
	,[PROC5_DATE] = OperationDate5
	,[PROC6] = OperationCode6
	,[PROC6_DATE] = OperationDate6
	,[PROC7] = OperationCode7
	,[PROC7_DATE] = OperationDate7
	,[PROC8] = OperationCode8
	,[PROC8_DATE] = OperationDate8
	,[PROC9] = OperationCode9
	,[PROC9_DATE] = OperationDate9
	,[PROC10] = OperationCode10
	,[PROC10_DATE] = OperationDate10
	,[PROC11] = OperationCode11
	,[PROC11_DATE] = OperationDate11
	,[PROC12] = OperationCode12
	,[PROC12_DATE] = OperationDate12
	,[READ_PROC_SCHEME] = null
	,[READ_PROC1] = null
	,[READ_PROC2] = null
	,[READ_PROC3] = null
	,[READ_PROC4] = null
	,[READ_PROC5] = null
	,[READ_PROC6] = null
	,[READ_PROC7] = null
	,[READ_PROC8] = null
	,[READ_PROC9] = null
	,[READ_PROC10] = null
	,[READ_PROC11] = null
	,[READ_PROC12] = null
	,[READ_PROC1_DATE] = null
	,[READ_PROC2_DATE] = null
	,[READ_PROC3_DATE] = null
	,[READ_PROC4_DATE] = null
	,[READ_PROC5_DATE] = null
	,[READ_PROC6_DATE] = null
	,[READ_PROC7_DATE] = null
	,[READ_PROC8_DATE] = null
	,[READ_PROC9_DATE] = null
	,[READ_PROC10_DATE] = null
	,[READ_PROC11_DATE] = null
	,[READ_PROC12_DATE] = null
	,[ANTENATAL_GP] = AnteNatalGpCode
	,[ANTENATAL_GP_PRAC] = AnteNatalGpPracticeCode
	,[ANTENATAL_ASSESSDATE] = FirstAnteNatalAssessmentDate
	,[NUMPREG] = PreviousPregnancies
	,[DELPLACE] = ActualDeliveryPlaceTypeCode
	,[DELDATE] = DeliveryDate
	,[LOCATION_CLASS_DELPLCINT] = DeliveryLocationClassCode
	,[DELINTEN] = IntendedDeliveryPlaceTypeCode
	,[DELCHANG] = DeliveryPlaceChangeReasonCode
	,[GESTAT] = GestationLength
	,[DELONSET] = LabourOnsetMethodCode
	,[DELMETH] = DeliveryMethodCode
	,[DELSTAT] = StatusOfPersonConductingDeliveryCode
	,[DELPREAN] = AnaestheticGivenDuringCode
	,[DELPOSAN] = AnaestheticGivenPostCode
	,[MUMDOB] = MotherDateOfBirth
	,[NUMBBABY] = NumberOfBabies

	,[SEXBABY1] = Birth1SexCode
	,[BIRORD1] = Birth1BirthOrder
	,[BIRSTAT1] = Birth1LiveOrStillBirth
	,[BIRWEIT1] = Birth1BirthWeight
	,[BIRESUS1] = Birth1ResuscitationMethodCode
	,[BABYDOB1] = Birth1DateOfBirth
	,[GESTATON 1] = Birth1AssessmentGestationLength
	,[DELIVERY METHOD 1] = Birth1DeliveryMethodCode
	,[STAT PERSON CONDEL1] = Birth1StatusOfPersonConductingDeliveryCode
	,[LOC PAT ID 1] = Birth1LocalPatientIdentifier
	,[ORG CODE BAB ID 1] = Birth1ProviderCode
	,[NHS NO BABY 1] = Birth1NHSNumber
	,[NHS NO STAT 1] = Birth1NHSNumberStatusIndicator

	,[SEXBABY2] = Birth2SexCode
	,[BIRORD2] = Birth2BirthOrder
	,[BIRSTAT2] = Birth2LiveOrStillBirth
	,[BIRWEIT2] = Birth2BirthWeight
	,[BIRESUS2] = Birth2ResuscitationMethodCode
	,[BABYDOB2] = Birth2DateOfBirth
	,[GESTATON 2] = Birth2AssessmentGestationLength
	,[DELIVERY METHOD 2] = Birth2DeliveryMethodCode
	,[STAT PERSON CONDEL2] = Birth2StatusOfPersonConductingDeliveryCode
	,[LOC PAT ID 2] = Birth2LocalPatientIdentifier
	,[ORG CODE BAB ID 2] = Birth2ProviderCode
	,[NHS NO BABY 2] = Birth2NHSNumber
	,[NHS NO STAT 2] = Birth2NHSNumberStatusIndicator

	,[SEXBABY3] = Birth3SexCode
	,[BIRORD3] = Birth3BirthOrder
	,[BIRSTAT3] = Birth3LiveOrStillBirth
	,[BIRWEIT3] = Birth3BirthWeight
	,[BIRESUS3] = Birth3ResuscitationMethodCode
	,[BABYDOB3] = Birth3DateOfBirth
	,[GESTATON 3] = Birth3AssessmentGestationLength
	,[DELIVERY METHOD 3] = Birth3DeliveryMethodCode
	,[STAT PERSON CONDEL3] = Birth3StatusOfPersonConductingDeliveryCode
	,[LOC PAT ID 3] = Birth3LocalPatientIdentifier
	,[ORG CODE BAB ID 3] = Birth3ProviderCode
	,[NHS NO BABY 3] = Birth3NHSNumber
	,[NHS NO STAT 3] = Birth3NHSNumberStatusIndicator

	,[SEXBABY4] = Birth4SexCode
	,[BIRORD4] = Birth4BirthOrder
	,[BIRSTAT4] = Birth4LiveOrStillBirth
	,[BIRWEIT4] = Birth4BirthWeight
	,[BIRESUS4] = Birth4ResuscitationMethodCode
	,[BABYDOB4] = Birth4DateOfBirth
	,[GESTATON 4] = Birth4AssessmentGestationLength
	,[DELIVERY METHOD 4] = Birth4DeliveryMethodCode
	,[STAT PERSON CONDEL4] = Birth4StatusOfPersonConductingDeliveryCode
	,[LOC PAT ID 4] = Birth4LocalPatientIdentifier
	,[ORG CODE BAB ID 4] = Birth4ProviderCode
	,[NHS NO BABY 4] = Birth4NHSNumber
	,[NHS NO STAT 4] = Birth4NHSNumberStatusIndicator

	,[SEXBABY5] = Birth5SexCode
	,[BIRORD5] = Birth5BirthOrder
	,[BIRSTAT5] = Birth5LiveOrStillBirth
	,[BIRWEIT5] = Birth5BirthWeight
	,[BIRESUS5] = Birth5ResuscitationMethodCode
	,[BABYDOB5] = Birth5DateOfBirth
	,[GESTATON 5] = Birth5AssessmentGestationLength
	,[DELIVERY METHOD 5] = Birth5DeliveryMethodCode
	,[STAT PERSON CONDEL5] = Birth5StatusOfPersonConductingDeliveryCode
	,[LOC PAT ID 5] = Birth5LocalPatientIdentifier
	,[ORG CODE BAB ID 5] = Birth5ProviderCode
	,[NHS NO BABY 5] = Birth5NHSNumber
	,[NHS NO STAT 5] = Birth5NHSNumberStatusIndicator

	,[SEXBABY6] = Birth6SexCode
	,[BIRORD6] = Birth6BirthOrder
	,[BIRSTAT6] = Birth6LiveOrStillBirth
	,[BIRWEIT6] = Birth6BirthWeight
	,[BIRESUS6] = Birth6ResuscitationMethodCode
	,[BABYDOB6] = Birth6DateOfBirth
	,[GESTATON 6] = Birth6AssessmentGestationLength
	,[DELIVERY METHOD 6] = Birth6DeliveryMethodCode
	,[STAT PERSON CONDEL6] = Birth6StatusOfPersonConductingDeliveryCode
	,[LOC PAT ID 6] = Birth6LocalPatientIdentifier
	,[ORG CODE BAB ID 6] = Birth6ProviderCode
	,[NHS NO BABY 6] = Birth6NHSNumber
	,[NHS NO STAT 6] = Birth6NHSNumberStatusIndicator

	,[HRG] = HRGCode
	,[HRG VERSION] = HRGVersionNumber
	,[DGVP_SCHEME] = DGVPCode
	,[HDGVP] = null

	,[CC_LOCAL_IDENTIFIER1] = AdultCriticalCareLocalIdentifier1
	,[CC_START_DATE1] = AdultCriticalCareStartDate1
	,[CC_START_TIME1] = AdultCriticalCareStartTime1
	,[CC_UNIT_FUNCTION1] = AdultCriticalCareUnitFunctionCode1
	,[CC_UNIT_BED_CONFIG1] = AdultCriticalCareUnitBedConfigurationCode1
	,[CC_ADMISSION_SOURCE1] = AdultCriticalCareAdmissionSourceCode1
	,[CC_SOURCE_LOCATION1] = AdultCriticalCareSourceLocationCode1
	,[CC_ADMISSION_TYPE1] = AdultCriticalCareAdmissionTypeCode1
	,[CC_GESTATION_LENGTH_AT_DELIV1] = null --neonatal delivered through other method at UHSM
	,[CC_ACTIVITY_DATE1] = null --neonatal delivered through other method at UHSM
	,[CC_PERSON_WEIGHT1] = null --neonatal delivered through other method at UHSM
	,[CC_ACTIVITY_CODE1] = null --neonatal delivered through other method at UHSM
	,[CC_HIGH_COST_DRUGS1] = null --neonatal delivered through other method at UHSM
	,[CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS1] = AdvancedRespiratorySupportDays1
	,[CC_BASIC_RESPIRATORY_SUPPORT_DAYS1] = BasicRespiratorySupportDays1
	,[CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS1] = AdvancedCardiovascularSupportDays1
	,[CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS1] = BasicCardiovascularSupportDays1
	,[CC_RENAL_SUPPORT_DAYS1] = RenalSupportDays1
	,[CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS1] = NeurologicalSupportDays1
	,[CC_GASTRO_SUPPORT_DAYS1] = GastroIntestinalSupportDays1
	,[CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY1] = DermatologicalSupportDays1
	,[CC_LIVER_SUPPORT_DAYS1] = LiverSupportDays1
	,[CC_ORGAN_SUPPORT_MAXIMUM1] = OrganSupportMaximum1
	,[CC_LEVEL_2_DAYS1] = CriticalCareLevel2Days1
	,[CC_LEVEL_3_DAYS1] = CriticalCareLevel3Days1
	,[CC_DISCHARGE_DATE1] = AdultCriticalCareDischargeDate1
	,[CC_DISCHARGE_TIME1] = AdultCriticalCareDischargeTime1
	,[CC_DISCHARGE_READY_DATE1] = AdultCriticalCareDischargeReadyDate1
	,[CC_DISCHARGE_READY_TIME1] = AdultCriticalCareDischargeReadyTime1
	,[CC_DISCHARGE_STATUS1] = AdultCriticalCareDischargeStatusCode1
	,[CC_DISCHARGE_DEST1] = AdultCriticalCareDischargeDestinationCode1
	,[CC_DISCHARGE_LOCATION1] = AdultCriticalCareDischargeLocationCode1

	,[CC_LOCAL_IDENTIFIER2] = AdultCriticalCareLocalIdentifier2
	,[CC_START_DATE2] = AdultCriticalCareStartDate2
	,[CC_START_TIME2] = AdultCriticalCareStartTime2
	,[CC_UNIT_FUNCTION2] = AdultCriticalCareUnitFunctionCode2
	,[CC_UNIT_BED_CONFIG2] = AdultCriticalCareUnitBedConfigurationCode2
	,[CC_ADMISSION_SOURCE2] = AdultCriticalCareAdmissionSourceCode2
	,[CC_SOURCE_LOCATION2] = AdultCriticalCareSourceLocationCode2
	,[CC_ADMISSION_TYPE2] = AdultCriticalCareAdmissionTypeCode2
	,[CC_GESTATION_LENGTH_AT_DELIV2] = null --neonatal delivered through other method at UHSM
	,[CC_ACTIVITY_DATE2] = null --neonatal delivered through other method at UHSM
	,[CC_PERSON_WEIGHT2] = null --neonatal delivered through other method at UHSM
	,[CC_ACTIVITY_CODE2] = null --neonatal delivered through other method at UHSM
	,[CC_HIGH_COST_DRUGS2] = null --neonatal delivered through other method at UHSM
	,[CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS2] = AdvancedRespiratorySupportDays2
	,[CC_BASIC_RESPIRATORY_SUPPORT_DAYS2] = BasicRespiratorySupportDays2
	,[CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS2] = AdvancedCardiovascularSupportDays2
	,[CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS2] = BasicCardiovascularSupportDays2
	,[CC_RENAL_SUPPORT_DAYS2] = RenalSupportDays2
	,[CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS2] = NeurologicalSupportDays2
	,[CC_GASTRO_SUPPORT_DAYS2] = GastroIntestinalSupportDays2
	,[CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY2] = DermatologicalSupportDays2
	,[CC_LIVER_SUPPORT_DAYS2] = LiverSupportDays2
	,[CC_ORGAN_SUPPORT_MAXIMUM2] = OrganSupportMaximum2
	,[CC_LEVEL_2_DAYS2] = CriticalCareLevel2Days2
	,[CC_LEVEL_3_DAYS2] = CriticalCareLevel3Days2
	,[CC_DISCHARGE_DATE2] = AdultCriticalCareDischargeDate2
	,[CC_DISCHARGE_TIME2] = AdultCriticalCareDischargeTime2
	,[CC_DISCHARGE_READY_DATE2] = AdultCriticalCareDischargeReadyDate2
	,[CC_DISCHARGE_READY_TIME2] = AdultCriticalCareDischargeReadyTime2
	,[CC_DISCHARGE_STATUS2] = AdultCriticalCareDischargeStatusCode2
	,[CC_DISCHARGE_DEST2] = AdultCriticalCareDischargeDestinationCode2
	,[CC_DISCHARGE_LOCATION2] = AdultCriticalCareDischargeLocationCode2

	,[CC_LOCAL_IDENTIFIER3] = AdultCriticalCareLocalIdentifier3
	,[CC_START_DATE3] = AdultCriticalCareStartDate3
	,[CC_START_TIME3] = AdultCriticalCareStartTime3
	,[CC_UNIT_FUNCTION3] = AdultCriticalCareUnitFunctionCode3
	,[CC_UNIT_BED_CONFIG3] = AdultCriticalCareUnitBedConfigurationCode3
	,[CC_ADMISSION_SOURCE3] = AdultCriticalCareAdmissionSourceCode3
	,[CC_SOURCE_LOCATION3] = AdultCriticalCareSourceLocationCode3
	,[CC_ADMISSION_TYPE3] = AdultCriticalCareAdmissionTypeCode3
	,[CC_GESTATION_LENGTH_AT_DELIV3] = null --neonatal delivered through other method at UHSM
	,[CC_ACTIVITY_DATE3] = null --neonatal delivered through other method at UHSM
	,[CC_PERSON_WEIGHT3] = null --neonatal delivered through other method at UHSM
	,[CC_ACTIVITY_CODE3] = null --neonatal delivered through other method at UHSM
	,[CC_HIGH_COST_DRUGS3] = null --neonatal delivered through other method at UHSM
	,[CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS3] = AdvancedRespiratorySupportDays3
	,[CC_BASIC_RESPIRATORY_SUPPORT_DAYS3] = BasicRespiratorySupportDays3
	,[CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS3] = AdvancedCardiovascularSupportDays3
	,[CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS3] = BasicCardiovascularSupportDays3
	,[CC_RENAL_SUPPORT_DAYS3] = RenalSupportDays3
	,[CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS3] = NeurologicalSupportDays3
	,[CC_GASTRO_SUPPORT_DAYS3] = GastroIntestinalSupportDays3
	,[CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY3] = DermatologicalSupportDays3
	,[CC_LIVER_SUPPORT_DAYS3] = LiverSupportDays3
	,[CC_ORGAN_SUPPORT_MAXIMUM3] = OrganSupportMaximum3
	,[CC_LEVEL_2_DAYS3] = CriticalCareLevel2Days3
	,[CC_LEVEL_3_DAYS3] = CriticalCareLevel3Days3
	,[CC_DISCHARGE_DATE3] = AdultCriticalCareDischargeDate3
	,[CC_DISCHARGE_TIME3] = AdultCriticalCareDischargeTime3
	,[CC_DISCHARGE_READY_DATE3] = AdultCriticalCareDischargeReadyDate3
	,[CC_DISCHARGE_READY_TIME3] = AdultCriticalCareDischargeReadyTime3
	,[CC_DISCHARGE_STATUS3] = AdultCriticalCareDischargeStatusCode3
	,[CC_DISCHARGE_DEST3] = AdultCriticalCareDischargeDestinationCode3
	,[CC_DISCHARGE_LOCATION3] = AdultCriticalCareDischargeLocationCode3

	,[CC_LOCAL_IDENTIFIER4] = AdultCriticalCareLocalIdentifier4
	,[CC_START_DATE4] = AdultCriticalCareStartDate4
	,[CC_START_TIME4] = AdultCriticalCareStartTime4
	,[CC_UNIT_FUNCTION4] = AdultCriticalCareUnitFunctionCode4
	,[CC_UNIT_BED_CONFIG4] = AdultCriticalCareUnitBedConfigurationCode4
	,[CC_ADMISSION_SOURCE4] = AdultCriticalCareAdmissionSourceCode4
	,[CC_SOURCE_LOCATION4] = AdultCriticalCareSourceLocationCode4
	,[CC_ADMISSION_TYPE4] = AdultCriticalCareAdmissionTypeCode4
	,[CC_GESTATION_LENGTH_AT_DELIV4] = null --neonatal delivered through other method at UHSM
	,[CC_ACTIVITY_DATE4] = null --neonatal delivered through other method at UHSM
	,[CC_PERSON_WEIGHT4] = null --neonatal delivered through other method at UHSM
	,[CC_ACTIVITY_CODE4] = null --neonatal delivered through other method at UHSM
	,[CC_HIGH_COST_DRUGS4] = null --neonatal delivered through other method at UHSM
	,[CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS4] = AdvancedRespiratorySupportDays4
	,[CC_BASIC_RESPIRATORY_SUPPORT_DAYS4] = BasicRespiratorySupportDays4
	,[CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS4] = AdvancedCardiovascularSupportDays4
	,[CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS4] = BasicCardiovascularSupportDays4
	,[CC_RENAL_SUPPORT_DAYS4] = RenalSupportDays4
	,[CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS4] = NeurologicalSupportDays4
	,[CC_GASTRO_SUPPORT_DAYS4] = GastroIntestinalSupportDays4
	,[CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY4] = DermatologicalSupportDays4
	,[CC_LIVER_SUPPORT_DAYS4] = LiverSupportDays4
	,[CC_ORGAN_SUPPORT_MAXIMUM4] = OrganSupportMaximum4
	,[CC_LEVEL_2_DAYS4] = CriticalCareLevel2Days4
	,[CC_LEVEL_3_DAYS4] = CriticalCareLevel3Days4
	,[CC_DISCHARGE_DATE4] = AdultCriticalCareDischargeDate4
	,[CC_DISCHARGE_TIME4] = AdultCriticalCareDischargeTime4
	,[CC_DISCHARGE_READY_DATE4] = AdultCriticalCareDischargeReadyDate4
	,[CC_DISCHARGE_READY_TIME4] = AdultCriticalCareDischargeReadyTime4
	,[CC_DISCHARGE_STATUS4] = AdultCriticalCareDischargeStatusCode4
	,[CC_DISCHARGE_DEST4] = AdultCriticalCareDischargeDestinationCode4
	,[CC_DISCHARGE_LOCATION4] = AdultCriticalCareDischargeLocationCode4

	,[CC_LOCAL_IDENTIFIER5] = AdultCriticalCareLocalIdentifier5
	,[CC_START_DATE5] = AdultCriticalCareStartDate5
	,[CC_START_TIME5] = AdultCriticalCareStartTime5
	,[CC_UNIT_FUNCTION5] = AdultCriticalCareUnitFunctionCode5
	,[CC_UNIT_BED_CONFIG5] = AdultCriticalCareUnitBedConfigurationCode5
	,[CC_ADMISSION_SOURCE5] = AdultCriticalCareAdmissionSourceCode5
	,[CC_SOURCE_LOCATION5] = AdultCriticalCareSourceLocationCode5
	,[CC_ADMISSION_TYPE5] = AdultCriticalCareAdmissionTypeCode5
	,[CC_GESTATION_LENGTH_AT_DELIV5] = null --neonatal delivered through other method at UHSM
	,[CC_ACTIVITY_DATE5] = null --neonatal delivered through other method at UHSM
	,[CC_PERSON_WEIGHT5] = null --neonatal delivered through other method at UHSM
	,[CC_ACTIVITY_CODE5] = null --neonatal delivered through other method at UHSM
	,[CC_HIGH_COST_DRUGS5] = null --neonatal delivered through other method at UHSM
	,[CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS5] = AdvancedRespiratorySupportDays5
	,[CC_BASIC_RESPIRATORY_SUPPORT_DAYS5] = BasicRespiratorySupportDays5
	,[CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS5] = AdvancedCardiovascularSupportDays5
	,[CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS5] = BasicCardiovascularSupportDays5
	,[CC_RENAL_SUPPORT_DAYS5] = RenalSupportDays5
	,[CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS5] = NeurologicalSupportDays5
	,[CC_GASTRO_SUPPORT_DAYS5] = GastroIntestinalSupportDays5
	,[CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY5] = DermatologicalSupportDays5
	,[CC_LIVER_SUPPORT_DAYS5] = LiverSupportDays5
	,[CC_ORGAN_SUPPORT_MAXIMUM5] = OrganSupportMaximum5
	,[CC_LEVEL_2_DAYS5] = CriticalCareLevel2Days5
	,[CC_LEVEL_3_DAYS5] = CriticalCareLevel3Days5
	,[CC_DISCHARGE_DATE5] = AdultCriticalCareDischargeDate5
	,[CC_DISCHARGE_TIME5] = AdultCriticalCareDischargeTime5
	,[CC_DISCHARGE_READY_DATE5] = AdultCriticalCareDischargeReadyDate5
	,[CC_DISCHARGE_READY_TIME5] = AdultCriticalCareDischargeReadyTime5
	,[CC_DISCHARGE_STATUS5] = AdultCriticalCareDischargeStatusCode5
	,[CC_DISCHARGE_DEST5] = AdultCriticalCareDischargeDestinationCode5
	,[CC_DISCHARGE_LOCATION5] = AdultCriticalCareDischargeLocationCode5

	,[CC_LOCAL_IDENTIFIER6] = AdultCriticalCareLocalIdentifier6
	,[CC_START_DATE6] = AdultCriticalCareStartDate6
	,[CC_START_TIME6] = AdultCriticalCareStartTime6
	,[CC_UNIT_FUNCTION6] = AdultCriticalCareUnitFunctionCode6
	,[CC_UNIT_BED_CONFIG6] = AdultCriticalCareUnitBedConfigurationCode6
	,[CC_ADMISSION_SOURCE6] = AdultCriticalCareAdmissionSourceCode6
	,[CC_SOURCE_LOCATION6] = AdultCriticalCareSourceLocationCode6
	,[CC_ADMISSION_TYPE6] = AdultCriticalCareAdmissionTypeCode6
	,[CC_GESTATION_LENGTH_AT_DELIV6] = null --neonatal delivered through other method at UHSM
	,[CC_ACTIVITY_DATE6] = null --neonatal delivered through other method at UHSM
	,[CC_PERSON_WEIGHT6] = null --neonatal delivered through other method at UHSM
	,[CC_ACTIVITY_CODE6] = null --neonatal delivered through other method at UHSM
	,[CC_HIGH_COST_DRUGS6] = null --neonatal delivered through other method at UHSM
	,[CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS6] = AdvancedRespiratorySupportDays6
	,[CC_BASIC_RESPIRATORY_SUPPORT_DAYS6] = BasicRespiratorySupportDays6
	,[CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS6] = AdvancedCardiovascularSupportDays6
	,[CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS6] = BasicCardiovascularSupportDays6
	,[CC_RENAL_SUPPORT_DAYS6] = RenalSupportDays6
	,[CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS6] = NeurologicalSupportDays6
	,[CC_GASTRO_SUPPORT_DAYS6] = GastroIntestinalSupportDays6
	,[CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY6] = DermatologicalSupportDays6
	,[CC_LIVER_SUPPORT_DAYS6] = LiverSupportDays6
	,[CC_ORGAN_SUPPORT_MAXIMUM6] = OrganSupportMaximum6
	,[CC_LEVEL_2_DAYS6] = CriticalCareLevel2Days6
	,[CC_LEVEL_3_DAYS6] = CriticalCareLevel3Days6
	,[CC_DISCHARGE_DATE6] = AdultCriticalCareDischargeDate6
	,[CC_DISCHARGE_TIME6] = AdultCriticalCareDischargeTime6
	,[CC_DISCHARGE_READY_DATE6] = AdultCriticalCareDischargeReadyDate6
	,[CC_DISCHARGE_READY_TIME6] = AdultCriticalCareDischargeReadyTime6
	,[CC_DISCHARGE_STATUS6] = AdultCriticalCareDischargeStatusCode6
	,[CC_DISCHARGE_DEST6] = AdultCriticalCareDischargeDestinationCode6
	,[CC_DISCHARGE_LOCATION6] = AdultCriticalCareDischargeLocationCode6

	,[CC_LOCAL_IDENTIFIER7] = AdultCriticalCareLocalIdentifier7
	,[CC_START_DATE7] = AdultCriticalCareStartDate7
	,[CC_START_TIME7] = AdultCriticalCareStartTime7
	,[CC_UNIT_FUNCTION7] = AdultCriticalCareUnitFunctionCode7
	,[CC_UNIT_BED_CONFIG7] = AdultCriticalCareUnitBedConfigurationCode7
	,[CC_ADMISSION_SOURCE7] = AdultCriticalCareAdmissionSourceCode7
	,[CC_SOURCE_LOCATION7] = AdultCriticalCareSourceLocationCode7
	,[CC_ADMISSION_TYPE7] = AdultCriticalCareAdmissionTypeCode7
	,[CC_GESTATION_LENGTH_AT_DELIV7] = null --neonatal delivered through other method at UHSM
	,[CC_ACTIVITY_DATE7] = null --neonatal delivered through other method at UHSM
	,[CC_PERSON_WEIGHT7] = null --neonatal delivered through other method at UHSM
	,[CC_ACTIVITY_CODE7] = null --neonatal delivered through other method at UHSM
	,[CC_HIGH_COST_DRUGS7] = null --neonatal delivered through other method at UHSM
	,[CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS7] = AdvancedRespiratorySupportDays7
	,[CC_BASIC_RESPIRATORY_SUPPORT_DAYS7] = BasicRespiratorySupportDays7
	,[CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS7] = AdvancedCardiovascularSupportDays7
	,[CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS7] = BasicCardiovascularSupportDays7
	,[CC_RENAL_SUPPORT_DAYS7] = RenalSupportDays7
	,[CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS7] = NeurologicalSupportDays7
	,[CC_GASTRO_SUPPORT_DAYS7] = GastroIntestinalSupportDays7
	,[CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY7] = DermatologicalSupportDays7
	,[CC_LIVER_SUPPORT_DAYS7] = LiverSupportDays7
	,[CC_ORGAN_SUPPORT_MAXIMUM7] = OrganSupportMaximum7
	,[CC_LEVEL_2_DAYS7] = CriticalCareLevel2Days7
	,[CC_LEVEL_3_DAYS7] = CriticalCareLevel3Days7
	,[CC_DISCHARGE_DATE7] = AdultCriticalCareDischargeDate7
	,[CC_DISCHARGE_TIME7] = AdultCriticalCareDischargeTime7
	,[CC_DISCHARGE_READY_DATE7] = AdultCriticalCareDischargeReadyDate7
	,[CC_DISCHARGE_READY_TIME7] = AdultCriticalCareDischargeReadyTime7
	,[CC_DISCHARGE_STATUS7] = AdultCriticalCareDischargeStatusCode7
	,[CC_DISCHARGE_DEST7] = AdultCriticalCareDischargeDestinationCode7
	,[CC_DISCHARGE_LOCATION7] = AdultCriticalCareDischargeLocationCode7

	,[CC_LOCAL_IDENTIFIER8] = AdultCriticalCareLocalIdentifier8
	,[CC_START_DATE8] = AdultCriticalCareStartDate8
	,[CC_START_TIME8] = AdultCriticalCareStartTime8
	,[CC_UNIT_FUNCTION8] = AdultCriticalCareUnitFunctionCode8
	,[CC_UNIT_BED_CONFIG8] = AdultCriticalCareUnitBedConfigurationCode8
	,[CC_ADMISSION_SOURCE8] = AdultCriticalCareAdmissionSourceCode8
	,[CC_SOURCE_LOCATION8] = AdultCriticalCareSourceLocationCode8
	,[CC_ADMISSION_TYPE8] = AdultCriticalCareAdmissionTypeCode8
	,[CC_GESTATION_LENGTH_AT_DELIV8] = null --neonatal delivered through other method at UHSM
	,[CC_ACTIVITY_DATE8] = null --neonatal delivered through other method at UHSM
	,[CC_PERSON_WEIGHT8] = null --neonatal delivered through other method at UHSM
	,[CC_ACTIVITY_CODE8] = null --neonatal delivered through other method at UHSM
	,[CC_HIGH_COST_DRUGS8] = null --neonatal delivered through other method at UHSM
	,[CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS8] = AdvancedRespiratorySupportDays8
	,[CC_BASIC_RESPIRATORY_SUPPORT_DAYS8] = BasicRespiratorySupportDays8
	,[CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS8] = AdvancedCardiovascularSupportDays8
	,[CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS8] = BasicCardiovascularSupportDays8
	,[CC_RENAL_SUPPORT_DAYS8] = RenalSupportDays8
	,[CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS8] = NeurologicalSupportDays8
	,[CC_GASTRO_SUPPORT_DAYS8] = GastroIntestinalSupportDays8
	,[CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY8] = DermatologicalSupportDays8
	,[CC_LIVER_SUPPORT_DAYS8] = LiverSupportDays8
	,[CC_ORGAN_SUPPORT_MAXIMUM8] = OrganSupportMaximum8
	,[CC_LEVEL_2_DAYS8] = CriticalCareLevel2Days8
	,[CC_LEVEL_3_DAYS8] = CriticalCareLevel3Days8
	,[CC_DISCHARGE_DATE8] = AdultCriticalCareDischargeDate8
	,[CC_DISCHARGE_TIME8] = AdultCriticalCareDischargeTime8
	,[CC_DISCHARGE_READY_DATE8] = AdultCriticalCareDischargeReadyDate8
	,[CC_DISCHARGE_READY_TIME8] = AdultCriticalCareDischargeReadyTime8
	,[CC_DISCHARGE_STATUS8] = AdultCriticalCareDischargeStatusCode8
	,[CC_DISCHARGE_DEST8] = AdultCriticalCareDischargeDestinationCode8
	,[CC_DISCHARGE_LOCATION8] = AdultCriticalCareDischargeLocationCode8

	,[CC_LOCAL_IDENTIFIER9] = AdultCriticalCareLocalIdentifier9
	,[CC_START_DATE9] = AdultCriticalCareStartDate9
	,[CC_START_TIME9] = AdultCriticalCareStartTime9
	,[CC_UNIT_FUNCTION9] = AdultCriticalCareUnitFunctionCode9
	,[CC_UNIT_BED_CONFIG9] = AdultCriticalCareUnitBedConfigurationCode9
	,[CC_ADMISSION_SOURCE9] = AdultCriticalCareAdmissionSourceCode9
	,[CC_SOURCE_LOCATION9] = AdultCriticalCareSourceLocationCode9
	,[CC_ADMISSION_TYPE9] = AdultCriticalCareAdmissionTypeCode9
	,[CC_GESTATION_LENGTH_AT_DELIV9] = null --neonatal delivered through other method at UHSM
	,[CC_ACTIVITY_DATE9] = null --neonatal delivered through other method at UHSM
	,[CC_PERSON_WEIGHT9] = null --neonatal delivered through other method at UHSM
	,[CC_ACTIVITY_CODE9] = null --neonatal delivered through other method at UHSM
	,[CC_HIGH_COST_DRUGS9] = null --neonatal delivered through other method at UHSM
	,[CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS9] = AdvancedRespiratorySupportDays9
	,[CC_BASIC_RESPIRATORY_SUPPORT_DAYS9] = BasicRespiratorySupportDays9
	,[CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS9] = AdvancedCardiovascularSupportDays9
	,[CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS9] = BasicCardiovascularSupportDays9
	,[CC_RENAL_SUPPORT_DAYS9] = RenalSupportDays9
	,[CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS9] = NeurologicalSupportDays9
	,[CC_GASTRO_SUPPORT_DAYS9] = GastroIntestinalSupportDays9
	,[CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY9] = DermatologicalSupportDays9
	,[CC_LIVER_SUPPORT_DAYS9] = LiverSupportDays9
	,[CC_ORGAN_SUPPORT_MAXIMUM9] = OrganSupportMaximum9
	,[CC_LEVEL_2_DAYS9] = CriticalCareLevel2Days9
	,[CC_LEVEL_3_DAYS9] = CriticalCareLevel3Days9
	,[CC_DISCHARGE_DATE9] = AdultCriticalCareDischargeDate9
	,[CC_DISCHARGE_TIME9] = AdultCriticalCareDischargeTime9
	,[CC_DISCHARGE_READY_DATE9] = AdultCriticalCareDischargeReadyDate9
	,[CC_DISCHARGE_READY_TIME9] = AdultCriticalCareDischargeReadyTime9
	,[CC_DISCHARGE_STATUS9] = AdultCriticalCareDischargeStatusCode9
	,[CC_DISCHARGE_DEST9] = AdultCriticalCareDischargeDestinationCode9
	,[CC_DISCHARGE_LOCATION9] = AdultCriticalCareDischargeLocationCode9

	,[MOM_NHS_NUM] = MotherNHSNumber
	,[MOM_PAT_ID] = MotherLocalPatientID
	,[MOM_PAT_ID_ORG] = MotherLocalPatientIDOrganisationCode
	,[MOM_NNN_STATUS] = MotherNNNStatusIndicator
	,[MOM USUAL ADDRESS LINE 1] = MotherPatientAddress1
	,[MOM USUAL ADDRESS LINE 2] = MotherPatientAddress2
	,[MOM USUAL ADDRESS LINE 3] = MotherPatientAddress3
	,[MOM USUAL ADDRESS LINE 4] = MotherPatientAddress4
	,[MOM USUAL ADDRESS LINE 5] = MotherPatientAddress5
	,[MOM POSTCODE OF USUAL ADDRESS] = MotherPostcode

	,[PROVIDER REFERENCE NUMBER] = null
	,[ORGANISATION CODE (LOCAL PAT ID)] = ProviderCode
	,[UBRN] = null
	,[CARE_PATHWAY_ID] = RTTPathwayID
	,[CARE_PATHWAY_ID_ORG] = RTTCurrentProviderCode
	,[REF_TO_TREAT_PERIOD_STATUS] = RTTPeriodStatusCode
	,[REF_TO_TREAT_PERIOD_START_DATE] = RTTStartDate
	,[REF_TO_TREAT_PERIOD_END_DATE] = RTTEndDate
from
	CDS.APCBaseArdentia
) Activity

--where
--	--HR post validation errors
--	NHS_NO is not null -- test to exclude merge pt issue






























