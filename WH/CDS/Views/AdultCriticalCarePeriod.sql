﻿

CREATE view [CDS].[AdultCriticalCarePeriod] as

select
	 SequenceNo = CriticalCarePeriod.CCPSequenceNo

--only 8 characters available so strip out a 0 as per the old CDS!
	,CriticalCareLocalIdentifier =
		left(
			cast(
				CriticalCarePeriod.SourceCCPNo
				as varchar
			)
			,2
		)
		+
		right(
			cast(
				CriticalCarePeriod.SourceCCPNo
				as varchar
			)
			,6
		)

	,CriticalCarePeriod.SourceEncounterNo
	,CriticalCareStartTime = CriticalCarePeriod.CCPStartDate
	,CriticalCareDischargeTime = CriticalCarePeriod.CCPEndDate
	,CriticalCareUnitFunctionCode = CriticalCareUnitFunctionCode.MainCode
	,DischargeDestinationCode = DischargeDestination.MappedCode
	,DischargeLocationCode = DischargeLocation.MappedCode
	,DischargeStatusCode = DischargeStatus.MappedCode
	,DischargeReadyTime = CriticalCarePeriod.CCPReadyDate

	,AdvancedRespiratorySupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayOrganSupported = 8864	--	Advanced Respiratory Support
			then 1
			else 0
			end
		)

	,BasicRespiratorySupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayOrganSupported = 8865	--	Basic Respiratory Support
			then 1
			else 0
			end
		)

	,CirculatorySupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayOrganSupported = 8866	--	Circulatory Support
			then 1
			else 0
			end
		)

	,NeurologicalSupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayOrganSupported = 8867	--	Neurological Support
			then 1
			else 0
			end
		)

	,RenalSupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayOrganSupported = 8868	--	Renal Support
			then 1
			else 0
			end
		)

	,AdvancedCardiovascularSupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayOrganSupported in (2005288, 2005513)	--	Advanced Cardiovascular Support
			then 1
			else 0
			end
		)

	,BasicCardiovascularSupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayOrganSupported in (2005289, 2005514)	--	Basic Cardiovascular Support
			then 1
			else 0
			end
		)

	,LiverSupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayOrganSupported = 2005290	--	Liver Cardiovascular Support
			then 1
			else 0
			end
		)

	,DermatologicalSupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayOrganSupported in (2005291, 2005515)	--	Dermatological Support
			then 1
			else 0
			end
		)

	,GastrointestinalSupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayOrganSupported in (2005288, 2005517)	--	Gastrointestinal Support
			then 1
			else 0
			end
		)

	,EnhancedNursingCareSupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayOrganSupported = 2005516	--	Enhanced Nursing Care Support
			then 1
			else 0
			end
		)

	,HepaticSupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayOrganSupported = 2005288	--	Hepatic Support
			then 1
			else 0
			end
		)

	,Level2SupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayCareLevel = 2
			then 1
			else 0
			end
		)

	,Level3SupportDays =
		SUM(
			case
			when CriticalCarePeriod.CCPStayCareLevel = 3
			then 1
			else 0
			end
		)

from
	APC.CriticalCarePeriod

inner join PAS.ReferenceValue CriticalCareUnitFunctionCode
on	CriticalCareUnitFunctionCode.ReferenceValueCode = CriticalCarePeriod.CCPUnitFunction
and	CriticalCareUnitFunctionCode.MainCode not in ('04','16','17','18','19','92') --remove paediatric

left join PAS.ReferenceValue DischargeDestination
on	DischargeDestination.ReferenceValueCode = CriticalCarePeriod.CCPDischargeDestination

left join PAS.ReferenceValue DischargeLocation
on	DischargeLocation.ReferenceValueCode = CriticalCarePeriod.CCPDischargeLocation

left join PAS.ReferenceValue DischargeStatus
on	DischargeStatus.ReferenceValueCode = CriticalCarePeriod.CCPDischargeStatus

group by
	 CriticalCarePeriod.CCPSequenceNo
	,left(
		cast(
			CriticalCarePeriod.SourceCCPNo
			as varchar
		)
		,2
	)
	+
	right(
		cast(
			CriticalCarePeriod.SourceCCPNo
			as varchar
		)
		,6
	)
	,CriticalCarePeriod.SourceEncounterNo
	,CriticalCarePeriod.CCPStartDate
	,CriticalCarePeriod.CCPEndDate
	,CriticalCareUnitFunctionCode.MainCode
	,DischargeDestination.MappedCode
	,DischargeLocation.MappedCode
	,DischargeStatus.MappedCode
	,CriticalCarePeriod.CCPReadyDate



