﻿
CREATE view [CDS].[CriticalCarePeriod] as

select
	 SequenceNo = CriticalCarePeriod.CCPSequenceNo

--only 8 characters available so strip out a 0 as per the old CDS!
	,CriticalCareLocalIdentifier =
		left(
			cast(
				CriticalCarePeriod.SourceCCPNo
				as varchar
			)
			,2
		)
		+
		right(
			cast(
				CriticalCarePeriod.SourceCCPNo
				as varchar
			)
			,6
		)


	,CriticalCarePeriod.SourceEncounterNo
	,CriticalCareStartTime = CriticalCarePeriod.CCPStartDate
	,CriticalCareDischargeTime = CriticalCarePeriod.CCPEndDate
	,CriticalCareUnitFunctionCode = CriticalCareUnitFunctionCode.MainCode
from
	APC.CriticalCarePeriod

left join PAS.ReferenceValue CriticalCareUnitFunctionCode
on	CriticalCareUnitFunctionCode.ReferenceValueCode = CriticalCarePeriod.CCPUnitFunction

where
	CriticalCarePeriod.CCPBedStay = 1

and	not exists
		(
		select
			1
		from
			APC.CriticalCarePeriod Previous
		where
			Previous.SourceCCPNo = CriticalCarePeriod.SourceCCPNo
		and	Previous.CCPSequenceNo = CriticalCarePeriod.CCPSequenceNo
		and Previous.CCPBedStay = CriticalCarePeriod.CCPBedStay
		and	Previous.SourceCCPStayNo > CriticalCarePeriod.SourceCCPStayNo
		)

