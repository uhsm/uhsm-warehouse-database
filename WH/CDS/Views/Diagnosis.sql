﻿
CREATE view [CDS].[Diagnosis] as

select
	 Encounter.EncounterRecno
	,DiagnosisCode = left(REPLACE(Diagnosis.DiagnosisCode, '.', ''), 6)
	,Diagnosis.SequenceNo
from
	APC.Encounter

inner join APC.Diagnosis
on	Diagnosis.APCSourceUniqueID = Encounter.SourceUniqueID

