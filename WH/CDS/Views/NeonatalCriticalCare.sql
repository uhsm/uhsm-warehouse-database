﻿CREATE view [CDS].[NeonatalCriticalCare] as

-- none at UHSM

select
	 EncounterRecno = null

	,NeonatalCriticalCareLocalIdentifier1 = null
	,NeonatalCriticalCareStartDate1 = null
	,NeonatalCriticalCareStartTime1 = null
	,NeonatalCriticalCareUnitFunctionCode1 = null
	,NeonatalCriticalCareGestationLength1 = null

	,NeonatalCriticalCareActivityDate1 = null
	,NeonatalCriticalCarePersonWeight1 = null
	,NeonatalCriticalCareActivityCode1 = null
	,NeonatalCriticalCareHighCostDrugs1 = null

	,NeonatalCriticalCareDischargeDate1 = null
	,NeonatalCriticalCareDischargeTime1 = null


