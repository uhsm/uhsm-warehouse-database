﻿






CREATE view [CDS].[OPArdentiaExportCCG] as

select 
	 --[Prime Recipient PCT] = cast([Prime Recipient PCT] as varchar(6))
	[Prime Recipient] = cast([Prime Recipient CCG] as varchar(6))
	,[Copy Recipient 1] = cast([Copy Recipient 1 CCG] as varchar(6))
	,[Copy Recipient 2] = cast([Copy Recipient 2] as varchar(6))
	,[Copy Recipient 3] = cast([Copy Recipient 3] as varchar(6))
	,[Copy Recipient 4] = cast([Copy Recipient 4] as varchar(6))
	,[Copy Recipient 5] = cast([Copy Recipient 5] as varchar(6))
	,[SENDER] = cast([SENDER] as varchar(5))
	,[CDS_Group] = cast([CDS_Group] as varchar(3))
	,[CDS_Type] = cast([CDS_Type] as varchar(3))
	,[CDS_ID] = cast([CDS_ID] as varchar(35))
	,[TEST_FLAG] = cast([TEST_FLAG] as varchar(1))
	,[Update Type] = cast([Update Type] as varchar(1))
	,[PROTOCOL_IDENTIFIER] = cast([PROTOCOL_IDENTIFIER] as varchar(3))
	,[BULKSTART] = cast([BULKSTART] as varchar(10))
	,[BULKEND] = cast([BULKEND] as varchar(10))
	,[DATETIME_CREATED] = cast([DATETIME_CREATED] as varchar(16))
	,[PROVIDER] = cast([PROVIDER] as varchar(5))
	--,[PURCHASER PCT] = cast([PURCHASER PCT] as varchar(5))
	,[PURCHASER] = cast([PURCHASER CCG] as varchar(5))
	,[SERIAL_NO] = cast([SERIAL_NO] as varchar(6))
	,[CONT_LINE_NO] = cast([CONT_LINE_NO] as varchar(10))
	,[PURCHREF] = cast([PURCHREF] as varchar(17))
	,[NHS_NO] = cast([NHS_NO] as varchar(17))
	,[NHS_NO_STATUS] = cast([NHS_NO_STATUS] as varchar(2))
	,[NAME_FORMAT] = cast([NAME_FORMAT] as varchar(1))
	,[ADDRESS_FORMAT] = cast([ADDRESS_FORMAT] as varchar(1))
	,[NAME] = cast([NAME] as varchar(70))
	,[FORENAME] = cast([FORENAME] as varchar(35))
	,[HOMEADD1] = cast([HOMEADD1] as varchar(35))
	,[HOMEADD2] = cast([HOMEADD2] as varchar(35))
	,[HOMEADD3] = cast([HOMEADD3] as varchar(35))
	,[HOMEADD4] = cast([HOMEADD4] as varchar(35))
	,[HOMEADD5] = cast([HOMEADD5] as varchar(35))
	,[POSTCODE] = cast([POSTCODE] as varchar(8))
	--,[HA PCT] = cast([HA PCT] as varchar(3))
	,[HA] = cast([HA CCG] as varchar(3))
	,[SEX] = cast([SEX] as varchar(1))
	,[CAR_SUP_IND] = cast([CAR_SUP_IND] as varchar(2))
	,[DOB] = cast([DOB] as varchar(10))
	,[GPREG] = cast([GPREG] as varchar(8))
	,[REGPRACT] = cast([REGPRACT] as varchar(6))
	,[LOCPATID] = cast([LOCPATID] as varchar(10))
	,[REFERRER] = cast([REFERRER] as varchar(8))
	,[REF_ORG] = cast([REF_ORG] as varchar(6))
	,[SERV_TYPE] = cast([SERV_TYPE] as varchar(1))
	,[REQDATE] = cast([REQDATE] as varchar(10))
	,[PRIORITY] = cast([PRIORITY] as varchar(1))
	,[REFSRCE] = cast([REFSRCE] as varchar(2))
	,[MAINSPEF] = cast([MAINSPEF] as varchar(3))
	,[CON_SPEC] = cast([CON_SPEC] as varchar(3))
	,[LOC_SPEC] = cast([LOC_SPEC] as varchar(3))
	--,[CCODEOLD] = cast([CCODEOLD] as varchar(10))
	,[CCODE] = cast([CCODE] as varchar(8))
	,[ATT_IDENT] = cast([ATT_IDENT] as varchar(12))
	,[CATEGORY] = cast([CATEGORY] as varchar(2))
	,[LOCATION_CLASS] = cast([LOCATION_CLASS] as varchar(2))
	,[SITECODE] = cast([SITECODE] as varchar(5))
	,[MED_STAFF] = cast([MED_STAFF] as varchar(2))
	,[ATTDATE] = cast([ATTDATE] as varchar(10))
	,[ACTIVITY_DATE] = cast([ACTIVITY_DATE] as varchar(10))
	,[AGE_AT_CDS_ACTIVITY_DATE] = cast([AGE_AT_CDS_ACTIVITY_DATE] as varchar(3))
	,[EARLIEST_REASONABLE_DATE_OFFER] = cast([EARLIEST_REASONABLE_DATE_OFFER] as varchar(10))
	,[FIRSTATT] = cast([FIRSTATT] as varchar(1))
	,[ATTENDED] = cast([ATTENDED] as varchar(1))
	,[OUTCOME] = cast([OUTCOME] as varchar(1))
	,[LASTDNADATE] = cast([LASTDNADATE] as varchar(10))
	,[ICD_DIAG_SCHEME] = cast([ICD_DIAG_SCHEME] as varchar(2))
	,[PRIICD] = cast([PRIICD] as varchar(6))
	,[FIRSTICD] = cast([FIRSTICD] as varchar(6))
	,[READ_DIAG_SCHEME] = cast([READ_DIAG_SCHEME] as varchar(2))
	,[PRIREAD] = cast([PRIREAD] as varchar(7))
	,[FIRSTREAD] = cast([FIRSTREAD] as varchar(7))
	,[OPSTATUS] = cast([OPSTATUS] as varchar(1))
	,[OPCS_PROC_SCHEME] = cast([OPCS_PROC_SCHEME] as varchar(2))
	,[PRIOPCS] = cast([PRIOPCS] as varchar(4))
	,[OPCS_DATE_1] = cast([OPCS_DATE_1] as varchar(10))
	,[OPCS2] = cast([OPCS2] as varchar(4))
	,[OPCS_DATE_2] = cast([OPCS_DATE_2] as varchar(10))
	,[OPCS3] = cast([OPCS3] as varchar(4))
	,[OPCS_DATE_3] = cast([OPCS_DATE_3] as varchar(10))
	,[OPCS4] = cast([OPCS4] as varchar(4))
	,[OPCS_DATE_4] = cast([OPCS_DATE_4] as varchar(10))
	,[OPCS5] = cast([OPCS5] as varchar(4))
	,[OPCS_DATE_5] = cast([OPCS_DATE_5] as varchar(10))
	,[OPCS6] = cast([OPCS6] as varchar(4))
	,[OPCS_DATE_6] = cast([OPCS_DATE_6] as varchar(10))
	,[OPCS7] = cast([OPCS7] as varchar(4))
	,[OPCS_DATE_7] = cast([OPCS_DATE_7] as varchar(10))
	,[OPCS8] = cast([OPCS8] as varchar(4))
	,[OPCS_DATE_8] = cast([OPCS_DATE_8] as varchar(10))
	,[OPCS9] = cast([OPCS9] as varchar(4))
	,[OPCS_DATE_9] = cast([OPCS_DATE_9] as varchar(10))
	,[OPCS10] = cast([OPCS10] as varchar(4))
	,[OPCS_DATE_10] = cast([OPCS_DATE_10] as varchar(10))
	,[OPCS11] = cast([OPCS11] as varchar(4))
	,[OPCS_DATE_11] = cast([OPCS_DATE_11] as varchar(10))
	,[OPCS12] = cast([OPCS12] as varchar(4))
	,[OPCS_DATE_12] = cast([OPCS_DATE_12] as varchar(10))
	,[READ_PROC_SCHEME] = cast([READ_PROC_SCHEME] as varchar(2))
	,[PRIREAD1] = cast([PRIREAD1] as varchar(7))
	,[READ_DATE_1] = cast([READ_DATE_1] as varchar(10))
	,[READ2] = cast([READ2] as varchar(7))
	,[READ_DATE_2] = cast([READ_DATE_2] as varchar(10))
	,[READ3] = cast([READ3] as varchar(7))
	,[READ_DATE_3] = cast([READ_DATE_3] as varchar(10))
	,[READ4] = cast([READ4] as varchar(7))
	,[READ_DATE_4] = cast([READ_DATE_4] as varchar(10))
	,[READ5] = cast([READ5] as varchar(7))
	,[READ_DATE_5] = cast([READ_DATE_5] as varchar(10))
	,[READ6] = cast([READ6] as varchar(7))
	,[READ_DATE_6] = cast([READ_DATE_6] as varchar(10))
	,[READ7] = cast([READ7] as varchar(7))
	,[READ_DATE_7] = cast([READ_DATE_7] as varchar(10))
	,[READ8] = cast([READ8] as varchar(7))
	,[READ_DATE_8] = cast([READ_DATE_8] as varchar(10))
	,[READ9] = cast([READ9] as varchar(7))
	,[READ_DATE_9] = cast([READ_DATE_9] as varchar(10))
	,[READ10] = cast([READ10] as varchar(7))
	,[READ_DATE_10] = cast([READ_DATE_10] as varchar(10))
	,[READ11] = cast([READ11] as varchar(7))
	,[READ_DATE_11] = cast([READ_DATE_11] as varchar(10))
	,[READ12] = cast([READ12] as varchar(7))
	,[READ_DATE_12] = cast([READ_DATE_12] as varchar(10))
	,[HRG] = cast([HRG] as varchar(3))
	,[HRG Version] = cast([HRG Version] as varchar(3))
	,[DGVP_SCHEME] = cast([DGVP_SCHEME] as varchar(2))
	,[HDGVP] = cast([HDGVP] as varchar(4))
	,[PROVIDER_REF_NO] = cast([PROVIDER_REF_NO] as varchar(17))
	,[LOCAL_PATIENT_ID_ORG] = cast([LOCAL_PATIENT_ID_ORG] as varchar(5))
	,[UBRN] = cast([UBRN] as varchar(12))
	,[CARE_PATHWAY_ID] = cast([CARE_PATHWAY_ID] as varchar(20))
	,[CARE_PATHWAY_ID_ORG] = cast([CARE_PATHWAY_ID_ORG] as varchar(5))
	,[REF_TO_TREAT_PERIOD_STATUS] = cast([REF_TO_TREAT_PERIOD_STATUS] as varchar(2))
	,[REF_TO_TREAT_PERIOD_START_DATE] = cast([REF_TO_TREAT_PERIOD_START_DATE] as varchar(10))
	,[REF_TO_TREAT_PERIOD_END_DATE] = cast([REF_TO_TREAT_PERIOD_END_DATE] as varchar(10))
	,[ETHNIC_CATEGORY] = cast([ETHNIC_CATEGORY] as varchar(2))
	,[ClinicCode] = cast([ClinicCode] as varchar(50))

from
(
select
	--KO changed 02/10/2012
	 --[PRIME RECIPIENT] =
		--case
		--when PurchaserCode in
		--		(
		--		 'TDH00' -- Overseas Visitor exempt from charges
		--		,'VPP00' -- Private Patient
		--		)
		--then PurchaserCode
		--else
		--	coalesce(
		--		 left(PCTofResidenceCode, 3) + '00'
		--		,PurchaserCode
		--		,'5NT00'
		--	)
		--end
	-- Apply codes for Exemptions and private patients.
	-- Otherwise use PCTofResidenceCodeNew* (1),
	-- else purchaser code (map SHA to 5NT first) (2),
	-- else 5NT00 (3).
	-- * Unless PCTofResidenceCodeNew = X98, Q99 or SHA then skip to value (2)
	--[PRIME RECIPIENT PCT] =
	--	case
	--	when OverseasStatus in ('1','2') then 'TDH00'
	--	--when OverseasStatus in ('3','4','9') then 'VPP00'
	--	when left(PCTofResidenceCode, 1) = 'Y' then 'TDH00'
	--	when PCTofResidenceCode in ('X98', 'Q99', 'SHA') then 
	--		coalesce(
	--			case when PurchaserCodePCT = 'SHA00' then '5NT00' else PurchaserCodePCT end
	--			, '5NT00'
	--			)
	--	else
	--		coalesce(
	--			 left(PCTofResidenceCode, 3) + '00'
	--			,case when PurchaserCodePCT = 'SHA00' then '5NT00' else PurchaserCodePCT end
	--			,'5NT00'
	--			)
	--	end
		
	[PRIME RECIPIENT CCG] =
		case
		when OverseasStatus in ('1','2') then 'TDH00'
		--when OverseasStatus in ('3','4','9') then 'VPP00'
		when left(CCGofResidenceCode, 1) = 'Y' then 'TDH00'
		when CCGofResidenceCode in ('X98', 'Q99', 'SHA') then 
			coalesce(
				case when PurchaserCodeCCG = 'SHA00' then '01N00' else PurchaserCodeCCG end
				--PurchaserCodeCCG 
				, '01N00'
				)
		else
			coalesce(
				 left(CCGofResidenceCode, 3) + '00'
				,case when PurchaserCodeCCG = 'SHA00' then '01N00' else PurchaserCodeCCG end
				--,PurchaserCodeCCG 
				,'01N00'
				)
		end
	--Optional  
	--for records where prime recipient resolves to 'TDH00'
	--use same code as for generating purchaser
	--Where  prime recipient resolves to 'VPP00'
		--and valid practice code, use PCT of practice

	--,[COPY RECIPIENT 1 PCT] =
	--	case
	--		when OverseasStatus in ('1','2') 
	--			then '5NT00' --For OS 1&2 Purchaser code will always wbe '5NT00'
	--		when left(PCTofResidenceCode, 1) = 'Y' 
	--			--then left(PCTofResidenceCodeNew, 3) + '00'
	--			then 
	--				case when Coalesce(
	--					PurchaserCodePCT
	--					,case when PCTofResidenceCode = 'X98' then '5NT00' end
	--					,left(PCTofResidenceCode + '00', 5)
	--					,'5NT00') != 'TDH00'
					
	--				then Coalesce(
	--					PurchaserCodePCT
	--					,case when PCTofResidenceCode = 'X98' then '5NT00' end
	--					,left(PCTofResidenceCode + '00', 5)
	--					,'5NT00')
	--				end
							
	--		when OverseasStatus in ('3','4') 
	--			then 
	--				case when isnull(PracticePCT,'SHA') <> 'SHA'
	--					then PracticePCT + '00'
	--			end
	--	end
		
	,[COPY RECIPIENT 1 CCG] =
		case
			when OverseasStatus in ('1','2') 
				then '01N00' --For OS 1&2 Purchaser code will always wbe '01N00'
			when left(CCGofResidenceCode, 1) = 'Y' 
				--then left(PCTofResidenceCodeNew, 3) + '00'
				then 
					case when Coalesce(
						PurchaserCodeCCG
						,case when CCGofResidenceCode = 'X98' then '01N00' end
						,left(CCGofResidenceCode + '00', 5)
						,'01N00') != 'TDH00'
					
					then Coalesce(
						PurchaserCodeCCG
						,case when CCGofResidenceCode = 'X98' then '01N00' end
						,left(CCGofResidenceCode + '00', 5)
						,'01N00')
					end
							
			when OverseasStatus in ('3','4') 
				then 
					case when isnull(PracticeCCG,'SHA') <> 'SHA'
						then PracticeCCG + '00'
				end
		--CM Added 16/05/2016
		Else Coalesce(
						PurchaserCodeCCG
						,case when CCGofResidenceCode = 'X98' then '01N00' end
						,left(CCGofResidenceCode + '00', 5)
						,'01N00')
				
				
		end
		
	,[COPY RECIPIENT 2] = null --Optional
	,[COPY RECIPIENT 3] = null --Optional
	,[COPY RECIPIENT 4] = null --Optional
	,[COPY RECIPIENT 5] = null --Optional
	,[SENDER] = 'RM200'
	,[CDS_Group] = CDSGroup
	,[CDS_Type] = CDSType
	,[CDS_ID] = UniqueEpisodeSerialNo
	,[TEST_FLAG] = (select TextValue from dbo.Parameter where Parameter = 'CDSTESTINDICATOR')
	,[Update Type] = null
	,[PROTOCOL_IDENTIFIER] = (select TextValue from dbo.Parameter where Parameter = 'CDSPROTOCOLIDENTIFIER')

	,[BULKSTART] =
		(
		select
			left(
				CONVERT(varchar, DateValue, 120)
				,10
			)
		from
			dbo.Parameter
		where
			Parameter = 'CDSREPORTPERIODSTARTDATE'
		)

	,[BULKEND] =
		(
		select
			left(
				CONVERT(varchar, DateValue, 120)
				,10
			)
		from
			dbo.Parameter
		where
			Parameter = 'CDSREPORTPERIODENDDATE'
		)

	,[DATETIME_CREATED] =
			--left(
			--	CONVERT(varchar, GETDATE(), 120)
			--	,16
			--)
			CDSUpdateDate + ' ' + LEFT(CDSUpdateTime, 2) + ':' + Substring(CDSUpdateTime, 3, 2)


	,[PROVIDER] = ProviderCode
	--,[PURCHASER] = PurchaserCode --KO changed 04/10/2012
	--,[PURCHASER] = IsNull(PurchaserCode, '5NT00') 
	-- Use purchaser from purchaser table or practice lookup* [PurchaserCodeNew] (1), 
	-- else use PCT of residence code (map X98 to 5NT first) (2),
	-- else use 5NT (3).
	-- * Unless = SHA00 (which has been generated from null practice lookup) then skip to value (2)
	--,[PURCHASER PCT] = 
	--	case when PurchaserCodePCT = 'SHA00'
	--		then Coalesce(
	--		case when PCTofResidenceCode = 'X98' then '5NT00' end
	--		,left(PCTofResidenceCode + '00', 5)
	--		,'5NT00')
	--	else Coalesce(
	--		PurchaserCodePCT
	--		,case when PCTofResidenceCode = 'X98' then '5NT00' end
	--		,left(PCTofResidenceCode + '00', 5)
	--		,'5NT00')
	--	end
	,[PURCHASER CCG] = 
		case when PurchaserCodeCCG = 'SHA00'
			then Coalesce(
			case when CCGofResidenceCode = 'X98' then '01N00' end
			,left(CCGofResidenceCode + '00', 5)
			,'01N00')
		else Coalesce(
			PurchaserCodeCCG
			,case when CCGofResidenceCode = 'X98' then '01N00' end
			,left(CCGofResidenceCode + '00', 5)
			,'01N00')
		end
	,[SERIAL_NO] = CommissioningSerialNo
	,[CONT_LINE_NO] = NHSServiceAgreementLineNo
	--,[PURCHREF] = PurchaserReferenceNo
	,[PURCHREF] = CommissionerReferenceNo
	,[NHS_NO] = NHSNumber
	,[NHS_NO_STATUS] = NNNStatusIndicator
	,[NAME_FORMAT] = 1
	,[ADDRESS_FORMAT] = 1
	,[NAME] = PatientSurname
	,[FORENAME] = PatientForename
	,[HOMEADD1] = PatientAddress1
	,[HOMEADD2] = PatientAddress2
	,[HOMEADD3] = PatientAddress3
	,[HOMEADD4] = PatientAddress4
	,[HOMEADD5] = PatientAddress5
	,[POSTCODE] = PostcodeAmended
	--KO changed 02/10/2012,[HA] = HACode
	--,[HA PCT] = 
	--	case 		
	--		--Where no Postcode or GP then organisation code for unknown practice 'V81999' maps to 'SHA'
	--		--Change this to 'Q99'
	--		when PCTofResidenceCode = 'SHA' then 'Q99'
	--		--Wales, Scotland, NI, CI
	--		when left(PCTofResidenceCode, 1) in ('7','S','Y', 'Z', '6') then 'X98'
	--		else coalesce(PCTofResidenceCode, 'Q99')
	--	end
	,[HA CCG] = 
		case 		
			--Where no Postcode or GP then organisation code for unknown practice 'V81999' maps to 'SHA'
			--Change this to 'Q99'
			when CCGofResidenceCode = 'SHA' then 'Q99'
			--Wales, Scotland, NI, CI
			when left(CCGofResidenceCode, 1) in ('7','S','Y', 'Z', '6') then 'X98'
			else Coalesce(CCGofResidenceCode, 'Q99')
		end
	,[SEX] = SexCode
	,[CAR_SUP_IND] = CarerSupportIndicator
	,[DOB] = DateOfBirth
	,[GPREG] = RegisteredGpCode
	--,[REGPRACT] = RegisteredGpPracticeCode KO changed 11/10
	,[REGPRACT] = IsNull(RegisteredGpPracticeCode, 'V81999')
	,[LOCPATID] = LocalPatientID
	,[REFERRER] = ReferrerCode
	,[REF_ORG] = ReferringOrganisationCode
	,[SERV_TYPE] = ServiceTypeRequestedCode
	,[REQDATE] = ReferralDate
	,[PRIORITY] = PriorityTypeCode
	,[REFSRCE] = SourceOfReferralCode
	,[MAINSPEF] = MainSpecialtyCode
	,[CON_SPEC] = TreatmentFunctionCode  
	--KO changed 10/10 to replace null TFC with main spec - changed back 11/10
	--,[CON_SPEC] = IsNull(TreatmentFunctionCode, MainSpecialtyCode)
	,[LOC_SPEC] = null
	--,[CCODEOLD] = ConsultantCodeOld
	,[CCODE] = ConsultantCode
	,[ATT_IDENT] = AttendanceIdentifier
	,[CATEGORY] = AdministrationCategoryCode
	,[LOCATION_CLASS] = LocationClassCode
	,[SITECODE] = SiteCode
	,[MED_STAFF] = MedicalStaffTypeSeeingPatientCode
	,[ATTDATE] = AppointmentDate
	,[ACTIVITY_DATE] = CDSActivityDate
	,[AGE_AT_CDS_ACTIVITY_DATE] = AgeAtCDSActivityDate
	,[EARLIEST_REASONABLE_DATE_OFFER] = EarliestReasonableOfferDate
	,[FIRSTATT] = FirstAttendanceFlag
	,[ATTENDED] = DNACode
	,[OUTCOME] = OutcomeOfAttendanceCode
	,[LASTDNADATE] = LastDNAOrPatientCancelledDate
	,[ICD_DIAG_SCHEME] = DiagnosisSchemeInUse
	,[PRIICD] = DiagnosisCode1
	,[FIRSTICD] = DiagnosisCode2
	,[READ_DIAG_SCHEME] = null
	,[PRIREAD] = null
	,[FIRSTREAD] = null
	,[OPSTATUS] = OperationStatus
	,[OPCS_PROC_SCHEME] = ProcedureSchemeInUse
	,[PRIOPCS] = OperationCode1
	,[OPCS_DATE_1] = OperationDate1
	,[OPCS2] = OperationCode2
	,[OPCS_DATE_2] = OperationDate2
	,[OPCS3] = OperationCode3
	,[OPCS_DATE_3] = OperationDate3
	,[OPCS4] = OperationCode4
	,[OPCS_DATE_4] = OperationDate4
	,[OPCS5] = OperationCode5
	,[OPCS_DATE_5] = OperationDate5
	,[OPCS6] = OperationCode6
	,[OPCS_DATE_6] = OperationDate6
	,[OPCS7] = OperationCode7
	,[OPCS_DATE_7] = OperationDate7
	,[OPCS8] = OperationCode8
	,[OPCS_DATE_8] = OperationDate8
	,[OPCS9] = OperationCode9
	,[OPCS_DATE_9] = OperationDate9
	,[OPCS10] = OperationCode10
	,[OPCS_DATE_10] = OperationDate10
	,[OPCS11] = OperationCode11
	,[OPCS_DATE_11] = OperationDate11
	,[OPCS12] = OperationCode12
	,[OPCS_DATE_12] = OperationDate12
	,[READ_PROC_SCHEME] = null
	,[PRIREAD1] = null
	,[READ_DATE_1] = null
	,[READ2] = null
	,[READ_DATE_2] = null
	,[READ3] = null
	,[READ_DATE_3] = null
	,[READ4] = null
	,[READ_DATE_4] = null
	,[READ5] = null
	,[READ_DATE_5] = null
	,[READ6] = null
	,[READ_DATE_6] = null
	,[READ7] = null
	,[READ_DATE_7] = null
	,[READ8] = null
	,[READ_DATE_8] = null
	,[READ9] = null
	,[READ_DATE_9] = null
	,[READ10] = null
	,[READ_DATE_10] = null
	,[READ11] = null
	,[READ_DATE_11] = null
	,[READ12] = null
	,[READ_DATE_12] = null
	,[HRG] = HRGCode
	,[HRG Version] = HRGVersionNumber
	,[DGVP_SCHEME] = DGVPCode
	,[HDGVP] = null
	,[PROVIDER_REF_NO] = null
	,[LOCAL_PATIENT_ID_ORG] = ProviderCode
	,[UBRN] = null
	,[CARE_PATHWAY_ID] = RTTPathwayID
	,[CARE_PATHWAY_ID_ORG] = RTTCurrentProviderCode
	,[REF_TO_TREAT_PERIOD_STATUS] = RTTPeriodStatusCode
	,[REF_TO_TREAT_PERIOD_START_DATE] = RTTStartDate
	,[REF_TO_TREAT_PERIOD_END_DATE] = RTTEndDate
	,[ETHNIC_CATEGORY] = EthnicGroupCode
	--,[ClinicCode] = ClinicCode
	,[ClinicCode] = ''
from
	CDS.OPBaseArdentia
) Activity

--where
--	--KO post validation errors
--	NHS_NO is not null -- test to exclude merge pt issue








