﻿create view [CDS].[OPDiagnosis] as

select
	 Encounter.EncounterRecno
	,DiagnosisCode = left(REPLACE(Diagnosis.DiagnosisCode, '.', ''), 6)
	,Diagnosis.SequenceNo
from
	OP.Encounter

inner join OP.Diagnosis
on	Diagnosis.OPSourceUniqueID = Encounter.SourceUniqueID


