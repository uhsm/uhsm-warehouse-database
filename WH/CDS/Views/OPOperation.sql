﻿create view [CDS].[OPOperation] as

select
	 Encounter.EncounterRecno
	,OperationCode = left(REPLACE(Operation.OperationCode, '.', ''), 4)

	,OperationDate =
		left(
			CONVERT(varchar, Operation.OperationDate, 120)
			,10
		)

	,Operation.SequenceNo
from
	OP.Encounter

inner join OP.Operation
on	Operation.OPSourceUniqueID = Encounter.SourceUniqueID


