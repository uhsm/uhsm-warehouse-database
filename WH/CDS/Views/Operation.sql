﻿
CREATE view [CDS].[Operation] as

select
	 Encounter.EncounterRecno
	,OperationCode = left(REPLACE(Operation.OperationCode, '.', ''), 4)

	,OperationDate =
		left(
			CONVERT(varchar, Operation.OperationDate, 120)
			,10
		)

	,Operation.SequenceNo
from
	APC.Encounter

inner join APC.Operation
on	Operation.APCSourceUniqueID = Encounter.SourceUniqueID

