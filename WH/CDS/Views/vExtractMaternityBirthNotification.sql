﻿

CREATE view [CDS].[vExtractMaternityBirthNotification] AS

/******************************************************************************
 Description: View to extract maternity birth notifications from Evolution. 
              Performs matching to Birth and Delivery Episodes.
 
 Modification History --------------------------------------------------------

 Date     Author      Change
 12/03/14 Tim Dean	  Created
 12/03/15 Tim Dean    Added fix to exclude duplicate delivery episodes
******************************************************************************/

SELECT PatientPointer = Birth.Patient_Pointer
      ,BirthOrder = Birth.[Birth Order]
      ,BirthDtm = Birth.[Date of Birth] + COALESCE(Birth.[Time of Birth],'')
      ,BirthSourceUniqueID = BirthEpisode.SourceUniqueID
      ,DeliverySourceUniqueID = DeliveryEpisode.SourceUniqueID
      
      -- Create a concatenated string of all the column to be monitored for changes.
      -- This is will be used by the Hashbytes Function to create a hash value for the row.
      ,RowDataConcat = COALESCE(CAST(Birth.[Birth Order] AS CHAR(1)),'')
                     + COALESCE(CONVERT(VARCHAR(10), Birth.[Date of Birth], 120),'')
                     + COALESCE(Birth.[Korner Actual Place of Birth],'')
                     + COALESCE(Birth.[Korner Analgesia/Anaesthesia],'')
                     + COALESCE(Birth.[Korner Intended Place of Birth],'')
                     + COALESCE(Birth.[Korner Method of delivery],'')
                     + COALESCE(Birth.[Korner Method of Labour Onset],'')
                     + COALESCE(Birth.[Korner Post Delivery Analgesia],'')
                     + COALESCE(Birth.[Korner Reason for Change],'')
                     + COALESCE(CAST(Birth.[Estimate of gestation (weeks)] AS VARCHAR(2)),'')
                     + COALESCE(Birth.[Outcome of birth],'')
                     + COALESCE(Birth.[Outcome of birth Death_of_Fetus],'')
                     + COALESCE(Baby.[Babys Hospital number],'')
                     + COALESCE(Baby.[Sex of baby],'')
                     + COALESCE(Baby.[Babys NHS number],'')
                     + COALESCE(Baby.[Babys NHS number status],'')
                     + COALESCE(CAST(Baby.[Birth weight (grams)] AS VARCHAR(5)),'')
                     + COALESCE(CONVERT(VARCHAR(10),Delivery.[Date of delivery],120),'')
                     + COALESCE(Delivery.[Number born this delivery],'')
                     + COALESCE(StaffPresent.[Type of person delivering],'')
                     + COALESCE(BabyResusitation.[National Resuscitation Code],'')
                     + COALESCE(CONVERT(VARCHAR(10),Mother.BirthDate,120),'')
                     + COALESCE(Mother.[Unit Number],'')
                     + COALESCE(Mother.[NHS Number],'')
                     + COALESCE(Mother.Address1,'')
                     + COALESCE(Mother.Address2,'')
                     + COALESCE(Mother.Address3,'')
                     + COALESCE(Mother.Address4,'')
                     + COALESCE(Mother.Postcode,'')
                     + COALESCE(Mother.GP_Code,'')
                     + COALESCE(Mother.GP_Practice,'')
                     + COALESCE(CONVERT(VARCHAR(10),PregnancyAssessment.[Date of Initial Assessment],120),'')
                     + COALESCE(CAST(ObstetricSummary.[Number of Past Pregnancies] AS VARCHAR(2)),'')
                     + COALESCE(CAST(BirthEpisode.SourceUniqueID AS VARCHAR(20)),'')
                     + COALESCE(CAST(DeliveryEpisode.SourceUniqueID AS VARCHAR(20)),'')
FROM   MaternityReporting.dbo.view_Baby_Birth Birth
       
       LEFT JOIN MaternityReporting.dbo.view_Baby_Details Baby
              ON Birth.Patient_Pointer = Baby.Patient_Pointer
                 AND Birth.Pregnancy_ID = Baby.Pregnancy_ID
                 AND Birth.Delivery_Number = Baby.Delivery_Number
                 AND Birth.Baby_Number = Baby.Baby_Number
       
       LEFT JOIN MaternityReporting.dbo.view_Delivery Delivery
              ON Birth.Patient_Pointer = Delivery.Patient_Pointer
                 AND Birth.Pregnancy_ID = Delivery.Pregnancy_ID
                 AND Birth.Delivery_Number = Delivery.Delivery_Number
       
       LEFT JOIN MaternityReporting.dbo.view_Pregnancy Pregnancy
              ON Birth.Patient_Pointer = Pregnancy.Patient_Pointer
                 AND Birth.Pregnancy_ID = Pregnancy.Pregnancy_ID
       
       LEFT JOIN MaternityReporting.dbo.view_Pregnancy_Assessment PregnancyAssessment
              ON Birth.Patient_Pointer = PregnancyAssessment.Patient_Pointer
                 AND Birth.Pregnancy_ID = PregnancyAssessment.Pregnancy_ID
       
       LEFT JOIN MaternityReporting.dbo.view_Patients Mother
              ON Birth.Patient_Pointer = Mother.Patient_Pointer
       
       LEFT JOIN MaternityReporting.dbo.view_Baby_Birth_StaffPresent StaffPresent
              ON Birth.Patient_Pointer = StaffPresent.Patient_Pointer
                 AND Birth.Pregnancy_ID = StaffPresent.Pregnancy_ID
                 AND Birth.Delivery_Number = StaffPresent.Delivery_Number
                 AND Birth.Baby_Number = StaffPresent.Baby_Number
       
       LEFT JOIN MaternityReporting.dbo.view_Baby_Resuscitation BabyResusitation
              ON Baby.Patient_Pointer = BabyResusitation.Patient_Pointer
                 AND Baby.Pregnancy_ID = BabyResusitation.Pregnancy_ID
                 AND Baby.Delivery_Number = BabyResusitation.Delivery_Number
                 AND Baby.Baby_Number = BabyResusitation.Baby_Number
             
       LEFT JOIN MaternityReporting.dbo.view_ObstetricSummary ObstetricSummary
              ON Birth.Patient_Pointer = ObstetricSummary.Patient_Pointer
        
       -- Link to the Baby birth episodem if a match found. 
       -- Match based on baby RM2 number and the date/time of birth matching the admission date/time and it is the first epsiode in the spell.      
       LEFT JOIN APC.Encounter BirthEpisode
              ON ( Baby.[Babys Hospital number] COLLATE DATABASE_DEFAULT = BirthEpisode.DistrictNo COLLATE DATABASE_DEFAULT
                   AND ( Birth.[Date of Birth] + Birth.[Time of Birth] ) = BirthEpisode.AdmissionTime
                   AND BirthEpisode.EpisodeNo = 1 ) 
                 
       -- Link to the Mother delivery episode, if a match found.
       -- Match based on Mother RM2 number and the date of delivery/time of first birth occurring within the inpatient episode.            
       LEFT JOIN APC.Encounter DeliveryEpisode
              ON ( Mother.[Unit Number] COLLATE DATABASE_DEFAULT = DeliveryEpisode.DistrictNo COLLATE DATABASE_DEFAULT
                   AND ( Delivery.[Date of delivery] + Delivery.[Time of first Birth] ) >= DeliveryEpisode.EpisodeStartTime 
                   AND ( Delivery.[Date of delivery] + Delivery.[Time of first Birth] ) < DeliveryEpisode.EpisodeEndTime )             

WHERE Birth.[Outcome of birth] IN ( 'Livebirth', 'Stillbirth' )
      AND Birth.[Date of Birth] IS NOT NULL
      -- Only include single birth entries from Evolution.  There are some duplicate entries that need to be excluded
      -- as it is not possble to determine which is the best birth row to keep.  Any duplicate births will require user intervention.
      AND EXISTS (SELECT BirthDuplicate.Patient_Pointer
                         ,BirthDuplicate.[Birth Order]
                         ,BirthDuplicate.[Date of Birth]
                         ,BirthDuplicate.[Time of Birth]
                  FROM   MaternityReporting.dbo.view_Baby_Birth BirthDuplicate
                  WHERE  BirthDuplicate.Patient_Pointer = Birth.Patient_Pointer
                         AND BirthDuplicate.[Birth Order] = Birth.[Birth Order]
                         AND BirthDuplicate.[Date of Birth] = Birth.[Date of Birth]
                         AND BirthDuplicate.[Time of Birth] = Birth.[Time of Birth]
                  GROUP  BY BirthDuplicate.Patient_Pointer
                           ,BirthDuplicate.[Birth Order]
                           ,BirthDuplicate.[Date of Birth]
                           ,BirthDuplicate.[Time of Birth]
                  HAVING Count(*) = 1)
      -- 12/03/15 Added TJD - Extra code added that will result in the last delivery episode being used
      --                      when more than one include the date/time of delivery entered on Evolution.
      --                      Most likely caused by a data quality issue and the incorrect episode start/end times have been entered.
      AND NOT EXISTS (
		SELECT 1
		FROM APC.Encounter NextDeliveryEpisode
		WHERE NextDeliveryEpisode.DistrictNo COLLATE DATABASE_DEFAULT = Mother.[Unit Number] COLLATE DATABASE_DEFAULT
		    -- Delivery episode period includes the current delivery date/time
			AND NextDeliveryEpisode.EpisodeStartTime <= (Delivery.[Date of delivery] + Delivery.[Time of first Birth])
			AND NextDeliveryEpisode.EpisodeEndTime > (Delivery.[Date of delivery] + Delivery.[Time of first Birth])
			AND NextDeliveryEpisode.EpisodeNo > DeliveryEpisode.EpisodeNo -- Episode is after the current one
		); 
                  

