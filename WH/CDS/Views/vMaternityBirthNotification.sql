﻿

CREATE view [CDS].[vMaternityBirthNotification] as

SELECT MaternityBirthNotification.BirthNotificationID,
       MaternityBirthNotification.CreatedDtm,
       MaternityBirthNotification.ModifiedDtm,
       MaternityBirthNotification.IsDeleted,
       Baby.[Babys Hospital number] LocalPatientIdentifierBaby,
       Birth.[Birth Order] BirthOrder,
       Birth.[Date of Birth] BirthDateBaby,
       Birth.[Time of Birth] BirthTimeBaby,
       --Birth.[Outcome of birth],
       --Birth.[Outcome of birth Death_of_Fetus],
       CASE
         WHEN Birth.[Outcome of birth] = 'Livebirth' THEN 1
         WHEN Birth.[Outcome of birth]  = 'Stillbirth' AND Birth.[Outcome of birth Death_of_Fetus] in ('Probably before Labour','Confirmed before Labour') THEN 2
		 WHEN Birth.[Outcome of birth]  = 'Stillbirth' AND Birth.[Outcome of birth Death_of_Fetus] = 'During Labour' THEN 3
		 WHEN Birth.[Outcome of birth]  = 'Stillbirth' AND (Birth.[Outcome of birth Death_of_Fetus] = 'Time Unknown' OR Birth.[Outcome of birth Death_of_Fetus] IS NULL) THEN 4
		 ELSE 4
       END LiveOrStillBirth,
       Birth.[Estimate of gestation (weeks)] GestationLength,
       Baby.[Birth weight (grams)] BirthWeight,
       Birth.[Korner Actual Place of Birth] KornerActualPlaceOfBirth,
       CASE
         WHEN Birth.[Korner Actual Place of Birth] = 'Midwife unit with other Consultant unit (theatre/anaesthetic' THEN 4
         WHEN Charindex(']', Birth.[Korner Actual Place of Birth], 1) = 0 THEN 9
         ELSE Substring(Birth.[Korner Actual Place of Birth], Charindex('[', Birth.[Korner Actual Place of Birth], 1)
                                                              + 1, ( Len(Birth.[Korner Actual Place of Birth]) - Charindex('[', Birth.[Korner Actual Place of Birth], 1) ) - 1)
       END ActualDeliveryPlaceNationalCode,
      -- Birth.[Korner Analgesia/Anaesthesia],
       CASE
         WHEN Charindex(']', Birth.[Korner Analgesia/Anaesthesia], 1) = 0 THEN 9
         ELSE Substring(Birth.[Korner Analgesia/Anaesthesia], Charindex('[', Birth.[Korner Analgesia/Anaesthesia], 1)
                                                              + 1, ( Len(Birth.[Korner Analgesia/Anaesthesia]) - Charindex('[', Birth.[Korner Analgesia/Anaesthesia], 1) ) - 1)
       END AnaestheticGivenDuringNationalCode,
       
       --Birth.[Korner Intended Place of Birth],
       CASE
         WHEN Birth.[Korner Intended Place of Birth] = 'Midwife unit with other Consultant unit (theatre/anaesthetic' THEN 4
         WHEN Charindex(']', Birth.[Korner Intended Place of Birth], 1) = 0 THEN 9
         ELSE Substring(Birth.[Korner Intended Place of Birth], Charindex('[', Birth.[Korner Intended Place of Birth], 1)
                                                                + 1, ( Len(Birth.[Korner Intended Place of Birth]) - Charindex('[', Birth.[Korner Intended Place of Birth], 1) ) - 1)
       END IntendedDeliveryPlaceNationalCode,
       --Birth.[Korner Method of delivery],
       CASE
         WHEN Charindex(']', Birth.[Korner Method of delivery], 1) = 0 THEN 9
         ELSE Substring(Birth.[Korner Method of delivery], Charindex('[', Birth.[Korner Method of delivery], 1)
                                                              + 1, ( Len(Birth.[Korner Method of delivery]) - Charindex('[', Birth.[Korner Method of delivery], 1) ) - 1)
       END DeliveryMethodNationalCode,
       
       --Birth.[Korner Method of Labour Onset],
       
       CASE
         WHEN Charindex(']', Birth.[Korner Method of Labour Onset], 1) = 0 THEN 9
         ELSE Substring(Birth.[Korner Method of Labour Onset], Charindex('[', Birth.[Korner Method of Labour Onset], 1)
                                                               + 1, ( Len(Birth.[Korner Method of Labour Onset]) - Charindex('[', Birth.[Korner Method of Labour Onset], 1) ) - 1)
       END LabourOnsetNationalCode,
       
       --Birth.[Korner Post Delivery Analgesia],
       CASE
         WHEN Charindex(']', Birth.[Korner Post Delivery Analgesia], 1) = 0 THEN 9
         ELSE Substring(Birth.[Korner Post Delivery Analgesia], Charindex('[', Birth.[Korner Post Delivery Analgesia], 1)
                                                              + 1, ( Len(Birth.[Korner Post Delivery Analgesia]) - Charindex('[', Birth.[Korner Post Delivery Analgesia], 1) ) - 1)
       END AnaestheticGivenPostNationalCode,
       
       --Birth.[Korner Reason for Change],
       CASE
         WHEN Charindex(']', Birth.[Korner Reason for Change], 1) = 0 THEN 9
         ELSE Substring(Birth.[Korner Reason for Change], Charindex('[', Birth.[Korner Reason for Change], 1)
                                                              + 1, ( Len(Birth.[Korner Reason for Change]) - Charindex('[', Birth.[Korner Reason for Change], 1) ) - 1)
       END DeliveryPlaceChangeNationalCode,
       Mother.[Unit Number] LocalPatientIdentifierMother,
       BirthEpisode.AdmissionTime AdmissionDtmBaby,
       BirthAdmissionMethod.MappedCode AdmissionMethodCodeBaby,
       AdmissionWardBirth.ServicePoint AdmissionWardBaby,
       DeliveryEpisode.AdmissionTime AdmissionDtmMother,
       DeliveryAdmissionMethod.MappedCode AdmissionMethodCodeMother,
       AdmissionWardDelivery.ServicePoint AdmissionWardMother
FROM   CDS.MaternityBirthNotification
       LEFT JOIN MaternityReporting.dbo.view_Baby_Birth Birth
              ON ( MaternityBirthNotification.PatientPointer = Birth.Patient_Pointer
                   AND MaternityBirthNotification.BirthOrder = Birth.[Birth Order]
                   AND MaternityBirthNotification.BirthDtm = ( Birth.[Date of Birth]
                                                               + Birth.[Time of Birth] ) )
       -- Join the baby details *******************************************************************************************
       INNER JOIN MaternityReporting.dbo.view_Baby_Details Baby
               ON ( Birth.Patient_Pointer = Baby.Patient_Pointer
                    AND Birth.Pregnancy_ID = Baby.Pregnancy_ID
                    AND Birth.Delivery_Number = Baby.Delivery_Number
                    AND Birth.Baby_Number = Baby.Baby_Number )
       -- Join the mother details *****************************************************************************************             
       INNER JOIN MaternityReporting.dbo.view_Patients Mother
               ON ( Birth.Patient_Pointer = Mother.Patient_Pointer ) 
       LEFT JOIN APC.Encounter BirthEpisode
               ON ( MaternityBirthNotification.BirthSourceUniqueID = BirthEpisode.SourceUniqueID)
       LEFT JOIN PAS.ReferenceValue BirthAdmissionMethod
              ON ( BirthEpisode.AdmissionMethodCode = BirthAdmissionMethod.ReferenceValueCode)
       LEFT JOIN PAS.ServicePoint AdmissionWardBirth
              ON (BirthEpisode.StartWardTypeCode = AdmissionWardBirth.ServicePointCode )
       LEFT JOIN APC.Encounter DeliveryEpisode
               ON ( MaternityBirthNotification.DeliverySourceUniqueID = DeliveryEpisode.SourceUniqueID)
       LEFT JOIN PAS.ReferenceValue DeliveryAdmissionMethod
              ON ( DeliveryEpisode.AdmissionMethodCode = DeliveryAdmissionMethod.ReferenceValueCode)
       LEFT JOIN PAS.ServicePoint AdmissionWardDelivery
              ON (DeliveryEpisode.StartWardTypeCode = AdmissionWardDelivery.ServicePointCode )       


