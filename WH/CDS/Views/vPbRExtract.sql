﻿



CREATE view [CDS].[vPbRExtract] AS

/******************************************************************************
 Description: View to extract the required columns from the daily built 
              PbR table and generate the row hash required for data change
              tracking.
              
              Used by a SSIS package to update the CDS.PbRExtract table.
 
 Modification History --------------------------------------------------------

 Date     Author      Change
 02/09/14 Tim Dean	  Created
 19/05/15 Tim Dean    Amended to look at the PbR2015 database for 2015/16.
 02/03/16 CMan		  Amended Where Clause to include dataset code of 'WardAtt' identifying PbR purchaser code for ward attenders activiy
******************************************************************************/

SELECT 
  DatasetCode
 ,SourceUniqueID
 ,EncounterTypeCode
 ,CommissionerCodeCCG
 ,ExclusionType
 ,FinalSLACode
 ,PbRExcluded
 ,ProviderSpellNo
 ,RowHash = CONVERT(VARCHAR(32), Hashbytes('MD5',COALESCE(DatasetCode,'')
                                               + COALESCE(SourceUniqueID, '')
                                               + COALESCE(EncounterTypeCode,'')
                                               + COALESCE(CommissionerCodeCCG,'')
                                               + COALESCE(ExclusionType,'')
                                               + COALESCE(FinalSLACode,'')
                                               + COALESCE(CAST(PbrExcluded AS CHAR(1)),'')
                                               + COALESCE(ProviderSpellNo,'')
                                           ),2)
FROM PbR2015.dbo.PbR
WHERE DatasetCode IN ('A&E','ENCOUNTER', 'WARDATT');
                  



