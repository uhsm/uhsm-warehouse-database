﻿







CREATE view [CDS].[vwAPCCriticalCareDaily] as

/***********************************************************************************
 Description: The table APC.CriticalCarePeriod contains a row for each day and
              organ supported for the critical care period. This view aggregates 
              them into a single row for each day of the critical care period.
              
              This view is then used by the view CDS.vwAPCCriticalCarePeriod
              to give a single row for each critical care period.
 
 Modification History --------------------------------------------------------------

 Date     Author      Change
 27/02/14 Tim Dean	  Created.

************************************************************************************/

SELECT CriticalCarePeriod.SourceEncounterNo,
       CriticalCarePeriod.SourceCCPNo,
       CriticalCarePeriod.SourceCCPStayNo,
       CriticalCarePeriod.CCPSequenceNo,
       Max(CriticalCarePeriod.CCPStartDate)               CCPStartDate,
       Max(CriticalCarePeriod.CCPAdmissionSource)         CCPAdmissionSource,
       Max(CriticalCarePeriod.CCPAdmissionType)           CCPAdmissionType,
       Max(CriticalCarePeriod.CCPAdmissionSourceLocation) CCPAdmissionSourceLocation,
       Max(CriticalCarePeriod.CCPUnitFunction)            CCPUnitFunction,
       Max(CriticalCarePeriod.CCPStayDate)                CCPStayDate,
       Max(CriticalCarePeriod.CCPStayCareLevel)           CCPStayCareLevel,
       Count(CriticalCarePeriod.CCPNoOfOrgansSupported)   CCPNoOfOrgansSupported,
       Count(CASE
               WHEN OrganSupported.ReferenceValue = 'Advanced Respiratory Support' THEN 1
               ELSE NULL
             END)                                         AdvancedRespiratorySupportFlag,
       Count(CASE
               WHEN OrganSupported.ReferenceValue = 'Basic Respiratory Support' THEN 1
               ELSE NULL
             END)                                         BasicRespiratorySupportFlag,
       Count(CASE
               WHEN OrganSupported.ReferenceValue = 'Advanced Cardiovascular Support' THEN 1
               ELSE NULL
             END)                                         AdvancedCardiovascularSupportFlag,
       Count(CASE
               WHEN OrganSupported.ReferenceValue = 'Basic Cardiovascular Support' THEN 1
               ELSE NULL
             END)                                         BasicCardiovascularSupportFlag,
       Count(CASE
               WHEN OrganSupported.ReferenceValue = 'Liver Support' THEN 1
               ELSE NULL
             END)                                         LiverSupportFlag,
       Count(CASE
               WHEN OrganSupported.ReferenceValue = 'Dermatological Support' THEN 1
               ELSE NULL
             END)                                         DermatologicalSupportFlag,
       Count(CASE
               WHEN OrganSupported.ReferenceValue = 'Gastrointestinal Support' THEN 1
               ELSE NULL
             END)                                         GastrointestinalSupportFlag,
       Count(CASE
               WHEN OrganSupported.ReferenceValue = 'Neurological Support' THEN 1
               ELSE NULL
             END)                                         NeurologicalSupportFlag,
       Count(CASE
               WHEN OrganSupported.ReferenceValue = 'Renal Support' THEN 1
               ELSE NULL
             END)                                         RenalSupportFlag,
       Max(CriticalCarePeriod.CCPEndDate)                 CCPEndDate,
       Max(CriticalCarePeriod.CCPReadyDate)               CCPReadyDate,
       Max(CriticalCarePeriod.CCPDischargeStatus)         CCPDischargeStatus,
       Max(CriticalCarePeriod.CCPDischargeDestination)    CCPDischargeDestination,
       Max(CriticalCarePeriod.CCPDischargeLocation)       CCPDischargeLocation
FROM   APC.CriticalCarePeriod
       INNER JOIN PAS.ReferenceValue OrganSupported
               ON CriticalCarePeriod.CCPStayOrganSupported = OrganSupported.ReferenceValueCode
GROUP  BY CriticalCarePeriod.SourceEncounterNo,
          CriticalCarePeriod.SourceCCPNo,
          CriticalCarePeriod.SourceCCPStayNo,
          CriticalCarePeriod.CCPSequenceNo 









