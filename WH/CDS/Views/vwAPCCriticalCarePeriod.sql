﻿



CREATE view [CDS].[vwAPCCriticalCarePeriod] as

/***********************************************************************************
 Description: This view uses the view CDS.vwAPCCriticalCareDaily to give a 
              single row for each critical care period.
              
 Modification History --------------------------------------------------------------

 Date     Author      Change
 27/02/14 Tim Dean	  Created.

************************************************************************************/

SELECT SourceEncounterNo,
       SourceCCPNo,
       CCPSequenceNo,
       Max(CCPStartDate)                      StartDate,
       Max(CCPAdmissionSource)                AdmissionSource,
       Max(CCPAdmissionType)                  AdmissionType,
       Max(CCPAdmissionSourceLocation)        AdmissionSourceLocation,
       Max(CCPUnitFunction)                   UnitFunction,
       Sum(AdvancedRespiratorySupportFlag)    AdvancedRespiratorySupportDays,
       Sum(BasicRespiratorySupportFlag)       BasicRespiratorySupportDays,
       Sum(AdvancedCardiovascularSupportFlag) AdvancedCardiovascularDays,
       Sum(BasicCardiovascularSupportFlag)    BasicCardiovascularSupportDays,
       Sum(RenalSupportFlag)                  RenalSupportDays,
       Sum(NeurologicalSupportFlag)           NeurologicalSupportDays,
       Sum(GastrointestinalSupportFlag)       GastrointestinalSupportDays,
       Sum(DermatologicalSupportFlag)         DermatologicalSupportDays,
       Sum(LiverSupportFlag)                  LiverSupportDays,
       Max(CCPNoOfOrgansSupported)            OrganSupportMaximum,
       Count(CASE
               WHEN CCPStayCareLevel = 2 THEN 1
               ELSE NULL
             END)                             CriticalCareLevel2Days,
       Count(CASE
               WHEN CCPStayCareLevel = 3 THEN 1
               ELSE NULL
             END)                             CriticalCareLevel3Days,
       Max(CCPEndDate)                        EndDate,
       Max(CCPReadyDate)                      ReadyDate,
       Max(CCPDischargeStatus)                DischargeStatus,
       Max(CCPDischargeDestination)           DischargeDestination,
       Max(CCPDischargeLocation)              DischargeLocation
FROM   CDS.vwAPCCriticalCareDaily
GROUP  BY SourceEncounterNo,
          SourceCCPNo,
          CCPSequenceNo 







