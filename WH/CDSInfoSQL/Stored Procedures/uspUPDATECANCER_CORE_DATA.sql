﻿




CREATE  PROCEDURE [CDSInfoSQL].[uspUPDATECANCER_CORE_DATA] AS 



truncate table [CDSInfoSQL].CANCER_CORE_TEMPLATE

INSERT INTO [CDSInfoSQL].CANCER_CORE_TEMPLATE
SELECT 
LTRIM(RTRIM(NHS_NO)),
LTRIM(RTRIM(LOCPATID)),
'RM200',
C.PatientSurname,
LTRIM(RTRIM(D.ALIAS_NAME)) AS PREVSURNAME,
C.PatientForename,
CASE WHEN LEN(C.PatientAddress1) = 0 THEN
LTRIM(RTRIM(C.PatientAddress2)) + ', ' + LTRIM(RTRIM(C.PatientAddress3)) + ', ' + LTRIM(RTRIM(C.PatientAddress4))
ELSE
LTRIM(RTRIM(C.PatientAddress1)) + ', ' + LTRIM(RTRIM(C.PatientAddress2)) + ', ' + LTRIM(RTRIM(C.PatientAddress3)) + ', ' + LTRIM(RTRIM(C.PatientAddress4))
END AS ADDSS,
LTRIM(RTRIM(A.POSTCODE)),
LTRIM(RTRIM(SEX)),
A.DOB,
LTRIM(RTRIM(GPREG)),
LTRIM(RTRIM(PRACREG)),
LTRIM(RTRIM(ETHNICOR)),
LTRIM(RTRIM(PURCHASER)),
A.EPIEND AS DIAGDATE,
LTRIM(RTRIM(LEFT(PRIMARY_DIAG,4))),
LTRIM(RTRIM(LEFT(SECONDARY_DIAG1,4))),
LTRIM(RTRIM(LEFT(SECONDARY_DIAG2,4))),
LTRIM(RTRIM(PRIMARY_PROC)),
'', 
LTRIM(RTRIM(PROC2)),
LTRIM(RTRIM(PROC3)),
LTRIM(RTRIM(PROC4)),
LTRIM(RTRIM(PROC5)),
LTRIM(RTRIM(PROC6)),
LTRIM(RTRIM(PROC7)),
LTRIM(RTRIM(PROC8)),
LTRIM(RTRIM(PROC9)),
LTRIM(RTRIM(PROC10)),
LTRIM(RTRIM(PROC11)),
LTRIM(RTRIM(PROC12)),
LTRIM(RTRIM(A.CONS_CODE)),
A.EPISTART AS EPSTART,
A.EPIEND AS EPEND,
CAST(DATEPART(Year,C.DateOfDeath) As Varchar(4)) 
	+ '-' + Right('0'+CAST(DATEPART(Month,C.DateOfDeath) As Varchar(4)),2) 
	+ '-' + Right('0' + CAST(DATEPART(Day,C.DateOfDeath) As Varchar(4)),2) AS DOD,
-- 12/03/14 Tim Dean Additional fields added ******************************************************
CASE A.CLASSPAT
  WHEN 2 THEN 'DayCase'
  ELSE 'Inpatient'
END AS Patient_Type,
A.ADMIMETH AS Admission_Method,
A.ADMIDATE AS Admission_Date,
A.PRIMARY_PROC_DATE AS Treatment_Date,  -- Used the primary procedure data as it is the closest we can get.
'' AS Spell_Number, -- They require an incremental spell count that is not part of the CDS dataset, so left blank.
A.DISDATE AS Discharge_Date,
A.DISDEST AS Discharge_Destination	
--*************************************************************************************************
FROM  CDSInfoSQL.APCV6_TEMPLATE A
inner join APC.Encounter C on C.SourceUniqueID = right(A.CDS_ID, charindex('1', A.CDS_ID, 1) - 1)
LEFT JOIN Lorenzo.DBO.vwPreviousName D ON C.SourcePatientNo = D.patnt_refno AND C.PatientSurname <> D.ALIAS_NAME
--LEFT OUTER JOIN INFORMATION_REPORTING.DBO.ADM_DISCH_dATASET B ON A.PROV_SPELL = B.SPELL_REFNO
--LEFT OUTER JOIN INFORMATION_REPORTING.DBO.TBL_MPI C ON B.PAT_REF = C.PT_ID
--LEFT OUTER JOIN Lorenzo.DBO.vwPreviousName D ON C.SourcePatientNo = D.patnt_refno AND C.PatientSurname <> D.ALIAS_NAME
WHERE 
(
LEFT(PRIMARY_DIAG,4) IN ('Z530','Z531','Z532','Z538','Z539','D352','D354')
OR (LEFT(PRIMARY_DIAG,3) >='C00' AND LEFT(PRIMARY_DIAG,3) <='C97')
OR (LEFT(PRIMARY_DIAG,3) >='D37' AND LEFT(PRIMARY_DIAG,3) <='D48')
OR (LEFT(PRIMARY_DIAG,3) IN ('D32','D33'))
)
and not [cds type] in ('180','190','200')
and (cast(a.epiend as datetime) >= '20140101 00:00:00' and cast(a.epiend as datetime) < '20140301 00:00:00')

UPDATE [CDSInfoSQL].CANCER_CORE_TEMPLATE
SET Ethnicity = 'Z'
WHERE Ethnicity = '99'




SELECT * INTO [CDSInfoSQL].CANCER_CORE_DATA_201402 FROM [CDSInfoSQL].CANCER_CORE_TEMPLATE









