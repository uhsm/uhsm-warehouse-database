﻿CREATE TABLE [CDSInfoSQL].[APCV6_Template] (
    [PRIME RECIPIENT]                          VARCHAR (6)  NULL,
    [COPY RECIPIENT 1]                         VARCHAR (6)  NULL,
    [COPY RECIPIENT 2]                         VARCHAR (6)  NULL,
    [COPY RECIPIENT 3]                         VARCHAR (6)  NULL,
    [COPY RECIPIENT 4]                         VARCHAR (6)  NULL,
    [COPY RECIPIENT 5]                         VARCHAR (6)  NULL,
    [SENDER]                                   VARCHAR (5)  NULL,
    [CDS GROUP]                                VARCHAR (3)  NULL,
    [CDS TYPE]                                 VARCHAR (3)  NULL,
    [CDS_ID]                                   VARCHAR (35) NULL,
    [TEST_FLAG]                                VARCHAR (1)  NULL,
    [UPDATE TYPE]                              VARCHAR (1)  NULL,
    [PROTOCOL IDENTIFIER]                      VARCHAR (3)  NULL,
    [BULKSTART]                                VARCHAR (10) NULL,
    [BULKEND]                                  VARCHAR (10) NULL,
    [DATETIME_CREATED]                         VARCHAR (16) NULL,
    [PROVIDER]                                 VARCHAR (5)  NULL,
    [PURCHASER]                                VARCHAR (5)  NULL,
    [SERIAL_NO]                                VARCHAR (6)  NULL,
    [CONTRACT_LINE_NO]                         VARCHAR (10) NULL,
    [PURCH_REF]                                VARCHAR (17) NULL,
    [NHS_NO]                                   VARCHAR (17) NULL,
    [NNN_STATUS_IND]                           VARCHAR (2)  NULL,
    [NAME_FORMAT]                              VARCHAR (1)  NULL,
    [ADDRESS_FORMAT]                           VARCHAR (1)  NULL,
    [NAME]                                     VARCHAR (70) NULL,
    [FORENAME]                                 VARCHAR (35) NULL,
    [HOMEADD1]                                 VARCHAR (35) NULL,
    [HOMEADD2]                                 VARCHAR (35) NULL,
    [HOMEADD3]                                 VARCHAR (35) NULL,
    [HOMEADD4]                                 VARCHAR (35) NULL,
    [HOMEADD5]                                 VARCHAR (35) NULL,
    [POSTCODE]                                 VARCHAR (8)  NULL,
    [HA]                                       VARCHAR (3)  NULL,
    [SEX]                                      VARCHAR (1)  NULL,
    [CARER_SUPPORT_IND]                        VARCHAR (2)  NULL,
    [DOB]                                      VARCHAR (10) NULL,
    [GPREG]                                    VARCHAR (8)  NULL,
    [PRACREG]                                  VARCHAR (6)  NULL,
    [ETHNICOR]                                 VARCHAR (2)  NULL,
    [LOCPATID]                                 VARCHAR (10) NULL,
    [REFERRER]                                 VARCHAR (8)  NULL,
    [REF_ORG]                                  VARCHAR (6)  NULL,
    [PROV_SPELL]                               VARCHAR (12) NULL,
    [ADMIN_CATEGORY]                           VARCHAR (2)  NULL,
    [LEGAL_STATUS]                             VARCHAR (2)  NULL,
    [ELECDATE]                                 VARCHAR (10) NULL,
    [EARLIEST_REASONABLE_DATE_OFFER]           VARCHAR (10) NULL,
    [ADMIDATE]                                 VARCHAR (10) NULL,
    [AGE_ON_ADMISSION_DATE]                    VARCHAR (3)  NULL,
    [ADMIMETH]                                 VARCHAR (2)  NULL,
    [ELECDUR]                                  VARCHAR (4)  NULL,
    [MAN_INTENT]                               VARCHAR (1)  NULL,
    [ADMISORC]                                 VARCHAR (2)  NULL,
    [DISCHARGE_READY_DATE]                     VARCHAR (10) NULL,
    [DISDATE]                                  VARCHAR (10) NULL,
    [DISMETH]                                  VARCHAR (1)  NULL,
    [DISDEST]                                  VARCHAR (2)  NULL,
    [CLASSPAT]                                 VARCHAR (1)  NULL,
    [EPIORDER]                                 VARCHAR (2)  NULL,
    [LAST_IN_SPELL]                            VARCHAR (1)  NULL,
    [EPISTART]                                 VARCHAR (10) NULL,
    [EPIEND]                                   VARCHAR (10) NULL,
    [ACTIVITY_DATE]                            VARCHAR (10) NULL,
    [AGE_AT_CDS_ACTIVITY_DATE]                 VARCHAR (3)  NULL,
    [MAINSPEF]                                 VARCHAR (3)  NULL,
    [CONS_MAINSPEF]                            VARCHAR (3)  NULL,
    [CONS_CODE]                                VARCHAR (8)  NULL,
    [LOCATION_CLASS_START]                     VARCHAR (2)  NULL,
    [SITECODE_AT_EPISODE_START]                VARCHAR (5)  NULL,
    [INT_CC_INTENSITY_START]                   VARCHAR (2)  NULL,
    [AGE_GRP_INT_START]                        VARCHAR (1)  NULL,
    [SEX_OF_PATIENTS_START]                    VARCHAR (1)  NULL,
    [WARD_DAY_PERIOD_AVAIL_START]              VARCHAR (1)  NULL,
    [WARD_NIGHT_PERIOD_AVAIL_START]            VARCHAR (1)  NULL,
    [LOCATION_CLASS_WARD]                      VARCHAR (2)  NULL,
    [SITECODE_DURING_WARD_STAY]                VARCHAR (5)  NULL,
    [INT_CC_INTENSITY_WARD]                    VARCHAR (2)  NULL,
    [AGE_GRP_INT_WARD]                         VARCHAR (1)  NULL,
    [SEX_OF_PATIENTS_WARD]                     VARCHAR (1)  NULL,
    [WARD_DAY_PERIOD_AVAIL_WARD]               VARCHAR (1)  NULL,
    [WARD_NIGHT_PERIOD_AVAIL_WARD]             VARCHAR (1)  NULL,
    [WARD_STAY_START_DATE]                     VARCHAR (10) NULL,
    [WARD_STAY_END_DATE]                       VARCHAR (10) NULL,
    [LOCATION_CLASS_END]                       VARCHAR (2)  NULL,
    [SITECODE_AT_EPISODE_END]                  VARCHAR (5)  NULL,
    [INT_CC_INTENSITY_END]                     VARCHAR (2)  NULL,
    [AGE_GRP_INT_END]                          VARCHAR (1)  NULL,
    [SEX_OF_PATIENTS_END]                      VARCHAR (1)  NULL,
    [WARD_DAY_PERIOD_AVAIL_END]                VARCHAR (1)  NULL,
    [WARD_NIGHT_PERIOD_AVAIL_END]              VARCHAR (1)  NULL,
    [FIRST_REG_ADM]                            VARCHAR (1)  NULL,
    [NEONATAL_LEVEL]                           VARCHAR (1)  NULL,
    [ADMISTAT]                                 VARCHAR (1)  NULL,
    [ICD_DIAG_SCHEME]                          VARCHAR (2)  NULL,
    [PRIMARY_DIAG]                             VARCHAR (6)  NULL,
    [SECONDARY_DIAG1]                          VARCHAR (6)  NULL,
    [SECONDARY_DIAG2]                          VARCHAR (6)  NULL,
    [SECONDARY_DIAG3]                          VARCHAR (6)  NULL,
    [SECONDARY_DIAG4]                          VARCHAR (6)  NULL,
    [SECONDARY_DIAG5]                          VARCHAR (6)  NULL,
    [SECONDARY_DIAG6]                          VARCHAR (6)  NULL,
    [SECONDARY_DIAG7]                          VARCHAR (6)  NULL,
    [SECONDARY_DIAG8]                          VARCHAR (6)  NULL,
    [SECONDARY_DIAG9]                          VARCHAR (6)  NULL,
    [SECONDARY_DIAG10]                         VARCHAR (6)  NULL,
    [SECONDARY_DIAG11]                         VARCHAR (6)  NULL,
    [SECONDARY_DIAG12]                         VARCHAR (6)  NULL,
    [READ_DIAG_SCHEME]                         VARCHAR (2)  NULL,
    [PRIMARY_READ_DIAG1]                       VARCHAR (7)  NULL,
    [READ_DIAG2]                               VARCHAR (7)  NULL,
    [READ_DIAG3]                               VARCHAR (7)  NULL,
    [READ_DIAG4]                               VARCHAR (7)  NULL,
    [READ_DIAG5]                               VARCHAR (7)  NULL,
    [READ_DIAG6]                               VARCHAR (7)  NULL,
    [READ_DIAG7]                               VARCHAR (7)  NULL,
    [READ_DIAG8]                               VARCHAR (7)  NULL,
    [READ_DIAG9]                               VARCHAR (7)  NULL,
    [READ_DIAG10]                              VARCHAR (7)  NULL,
    [READ_DIAG11]                              VARCHAR (7)  NULL,
    [READ_DIAG12]                              VARCHAR (7)  NULL,
    [READ_DIAG13]                              VARCHAR (7)  NULL,
    [READ_DIAG14]                              VARCHAR (7)  NULL,
    [OPER_STATUS]                              VARCHAR (1)  NULL,
    [OPCS_PROC_SCHEME]                         VARCHAR (2)  NULL,
    [PRIMARY_PROC]                             VARCHAR (4)  NULL,
    [PRIMARY_PROC_DATE]                        VARCHAR (10) NULL,
    [PROC2]                                    VARCHAR (4)  NULL,
    [PROC2_DATE]                               VARCHAR (10) NULL,
    [PROC3]                                    VARCHAR (4)  NULL,
    [PROC3_DATE]                               VARCHAR (10) NULL,
    [PROC4]                                    VARCHAR (4)  NULL,
    [PROC4_DATE]                               VARCHAR (10) NULL,
    [PROC5]                                    VARCHAR (4)  NULL,
    [PROC5_DATE]                               VARCHAR (10) NULL,
    [PROC6]                                    VARCHAR (4)  NULL,
    [PROC6_DATE]                               VARCHAR (10) NULL,
    [PROC7]                                    VARCHAR (4)  NULL,
    [PROC7_DATE]                               VARCHAR (10) NULL,
    [PROC8]                                    VARCHAR (4)  NULL,
    [PROC8_DATE]                               VARCHAR (10) NULL,
    [PROC9]                                    VARCHAR (4)  NULL,
    [PROC9_DATE]                               VARCHAR (10) NULL,
    [PROC10]                                   VARCHAR (4)  NULL,
    [PROC10_DATE]                              VARCHAR (10) NULL,
    [PROC11]                                   VARCHAR (4)  NULL,
    [PROC11_DATE]                              VARCHAR (10) NULL,
    [PROC12]                                   VARCHAR (4)  NULL,
    [PROC12_DATE]                              VARCHAR (10) NULL,
    [READ_PROC_SCHEME]                         VARCHAR (2)  NULL,
    [READ_PROC1]                               VARCHAR (7)  NULL,
    [READ_PROC2]                               VARCHAR (7)  NULL,
    [READ_PROC3]                               VARCHAR (7)  NULL,
    [READ_PROC4]                               VARCHAR (7)  NULL,
    [READ_PROC5]                               VARCHAR (7)  NULL,
    [READ_PROC6]                               VARCHAR (7)  NULL,
    [READ_PROC7]                               VARCHAR (7)  NULL,
    [READ_PROC8]                               VARCHAR (7)  NULL,
    [READ_PROC9]                               VARCHAR (7)  NULL,
    [READ_PROC10]                              VARCHAR (7)  NULL,
    [READ_PROC11]                              VARCHAR (7)  NULL,
    [READ_PROC12]                              VARCHAR (7)  NULL,
    [READ_PROC1_DATE]                          VARCHAR (10) NULL,
    [READ_PROC2_DATE]                          VARCHAR (10) NULL,
    [READ_PROC3_DATE]                          VARCHAR (10) NULL,
    [READ_PROC4_DATE]                          VARCHAR (10) NULL,
    [READ_PROC5_DATE]                          VARCHAR (10) NULL,
    [READ_PROC6_DATE]                          VARCHAR (10) NULL,
    [READ_PROC7_DATE]                          VARCHAR (10) NULL,
    [READ_PROC8_DATE]                          VARCHAR (10) NULL,
    [READ_PROC9_DATE]                          VARCHAR (10) NULL,
    [READ_PROC10_DATE]                         VARCHAR (10) NULL,
    [READ_PROC11_DATE]                         VARCHAR (10) NULL,
    [READ_PROC12_DATE]                         VARCHAR (10) NULL,
    [ANTENATAL_GP]                             VARCHAR (8)  NULL,
    [ANTENATAL_GP_PRAC]                        VARCHAR (6)  NULL,
    [ANTENATAL_ASSESSDATE]                     VARCHAR (10) NULL,
    [NUMPREG]                                  VARCHAR (2)  NULL,
    [DELPLACE]                                 VARCHAR (1)  NULL,
    [DELDATE]                                  VARCHAR (10) NULL,
    [LOCATION_CLASS_DELPLCINT]                 VARCHAR (2)  NULL,
    [DELINTEN]                                 VARCHAR (1)  NULL,
    [DELCHANG]                                 VARCHAR (1)  NULL,
    [GESTAT]                                   VARCHAR (2)  NULL,
    [DELONSET]                                 VARCHAR (1)  NULL,
    [DELMETH]                                  VARCHAR (1)  NULL,
    [DELSTAT]                                  VARCHAR (1)  NULL,
    [DELPREAN]                                 VARCHAR (1)  NULL,
    [DELPOSAN]                                 VARCHAR (1)  NULL,
    [MUMDOB]                                   VARCHAR (10) NULL,
    [NUMBBABY]                                 VARCHAR (1)  NULL,
    [SEXBABY1]                                 VARCHAR (1)  NULL,
    [BIRORD1]                                  VARCHAR (1)  NULL,
    [BIRSTAT1]                                 VARCHAR (1)  NULL,
    [BIRWEIT1]                                 VARCHAR (4)  NULL,
    [BIRESUS1]                                 VARCHAR (1)  NULL,
    [BABYDOB1]                                 VARCHAR (10) NULL,
    [GESTATON 1]                               VARCHAR (2)  NULL,
    [DELIVERY METHOD 1]                        VARCHAR (1)  NULL,
    [STAT PERSON CONDEL1]                      VARCHAR (1)  NULL,
    [LOC PAT ID 1]                             VARCHAR (10) NULL,
    [ORG CODE BAB ID 1]                        VARCHAR (5)  NULL,
    [NHS NO BABY 1]                            VARCHAR (10) NULL,
    [NHS NO STAT 1]                            VARCHAR (2)  NULL,
    [SEXBABY2]                                 VARCHAR (1)  NULL,
    [BIRORD2]                                  VARCHAR (1)  NULL,
    [BIRSTAT2]                                 VARCHAR (1)  NULL,
    [BIRWEIT2]                                 VARCHAR (4)  NULL,
    [BIRESUS2]                                 VARCHAR (1)  NULL,
    [BABYDOB2]                                 VARCHAR (10) NULL,
    [GESTATON 2]                               VARCHAR (2)  NULL,
    [DELIVERY METHOD 2]                        VARCHAR (1)  NULL,
    [STAT PERSON CONDEL2]                      VARCHAR (1)  NULL,
    [LOC PAT ID 2]                             VARCHAR (10) NULL,
    [ORG CODE BAB ID 2]                        VARCHAR (5)  NULL,
    [NHS NO BABY 2]                            VARCHAR (10) NULL,
    [NHS NO STAT 2]                            VARCHAR (2)  NULL,
    [SEXBABY3]                                 VARCHAR (1)  NULL,
    [BIRORD3]                                  VARCHAR (1)  NULL,
    [BIRSTAT3]                                 VARCHAR (1)  NULL,
    [BIRWEIT3]                                 VARCHAR (4)  NULL,
    [BIRESUS3]                                 VARCHAR (1)  NULL,
    [BABYDOB3]                                 VARCHAR (10) NULL,
    [GESTATON 3]                               VARCHAR (2)  NULL,
    [DELIVERY METHOD 3]                        VARCHAR (1)  NULL,
    [STAT PERSON CONDEL3]                      VARCHAR (1)  NULL,
    [LOC PAT ID 3]                             VARCHAR (10) NULL,
    [ORG CODE BAB ID 3]                        VARCHAR (5)  NULL,
    [NHS NO BABY 3]                            VARCHAR (10) NULL,
    [NHS NO STAT 3]                            VARCHAR (2)  NULL,
    [SEXBABY4]                                 VARCHAR (1)  NULL,
    [BIRORD4]                                  VARCHAR (1)  NULL,
    [BIRSTAT4]                                 VARCHAR (1)  NULL,
    [BIRWEIT4]                                 VARCHAR (4)  NULL,
    [BIRESUS4]                                 VARCHAR (1)  NULL,
    [BABYDOB4]                                 VARCHAR (10) NULL,
    [GESTATON 4]                               VARCHAR (2)  NULL,
    [DELIVERY METHOD 4]                        VARCHAR (1)  NULL,
    [STAT PERSON CONDEL4]                      VARCHAR (1)  NULL,
    [LOC PAT ID 4]                             VARCHAR (10) NULL,
    [ORG CODE BAB ID 4]                        VARCHAR (5)  NULL,
    [NHS NO BABY 4]                            VARCHAR (10) NULL,
    [NHS NO STAT 4]                            VARCHAR (2)  NULL,
    [SEXBABY5]                                 VARCHAR (1)  NULL,
    [BIRORD5]                                  VARCHAR (1)  NULL,
    [BIRSTAT5]                                 VARCHAR (1)  NULL,
    [BIRWEIT5]                                 VARCHAR (4)  NULL,
    [BIRESUS5]                                 VARCHAR (1)  NULL,
    [BABYDOB5]                                 VARCHAR (10) NULL,
    [GESTATON 5]                               VARCHAR (2)  NULL,
    [DELIVERY METHOD 5]                        VARCHAR (1)  NULL,
    [STAT PERSON CONDEL5]                      VARCHAR (1)  NULL,
    [LOC PAT ID 5]                             VARCHAR (10) NULL,
    [ORG CODE BAB ID 5]                        VARCHAR (5)  NULL,
    [NHS NO BABY 5]                            VARCHAR (10) NULL,
    [NHS NO STAT 5]                            VARCHAR (2)  NULL,
    [SEXBABY6]                                 VARCHAR (1)  NULL,
    [BIRORD6]                                  VARCHAR (1)  NULL,
    [BIRSTAT6]                                 VARCHAR (1)  NULL,
    [BIRWEIT6]                                 VARCHAR (4)  NULL,
    [BIRESUS6]                                 VARCHAR (1)  NULL,
    [BABYDOB6]                                 VARCHAR (10) NULL,
    [GESTATON 6]                               VARCHAR (2)  NULL,
    [DELIVERY METHOD 6]                        VARCHAR (1)  NULL,
    [STAT PERSON CONDEL6]                      VARCHAR (1)  NULL,
    [LOC PAT ID 6]                             VARCHAR (10) NULL,
    [ORG CODE BAB ID 6]                        VARCHAR (5)  NULL,
    [NHS NO BABY 6]                            VARCHAR (10) NULL,
    [NHS NO STAT 6]                            VARCHAR (2)  NULL,
    [HRG]                                      VARCHAR (3)  NULL,
    [HRG VERSION]                              VARCHAR (3)  NULL,
    [DGVP_SCHEME]                              VARCHAR (2)  NULL,
    [HDGVP]                                    VARCHAR (4)  NULL,
    [CC_LOCAL_IDENTIFIER1]                     VARCHAR (8)  NULL,
    [CC_START_DATE1]                           VARCHAR (10) NULL,
    [CC_START_TIME1]                           VARCHAR (8)  NULL,
    [CC_UNIT_FUNCTION1]                        VARCHAR (2)  NULL,
    [CC_UNIT_BED_CONFIG1]                      VARCHAR (2)  NULL,
    [CC_ADMISSION_SOURCE1]                     VARCHAR (2)  NULL,
    [CC_SOURCE_LOCATION1]                      VARCHAR (2)  NULL,
    [CC_ADMISSION_TYPE1]                       VARCHAR (2)  NULL,
    [CC_GESTATION_LENGTH_AT_DELIV1]            VARCHAR (2)  NULL,
    [CC_ACTIVITY_DATE1]                        VARCHAR (10) NULL,
    [CC_PERSON_WEIGHT1]                        VARCHAR (7)  NULL,
    [CC_ACTIVITY_CODE1]                        VARCHAR (2)  NULL,
    [CC_HIGH_COST_DRUGS1]                      VARCHAR (4)  NULL,
    [CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS1]    VARCHAR (3)  NULL,
    [CC_BASIC_RESPIRATORY_SUPPORT_DAYS1]       VARCHAR (3)  NULL,
    [CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS1] VARCHAR (3)  NULL,
    [CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS1]    VARCHAR (3)  NULL,
    [CC_RENAL_SUPPORT_DAYS1]                   VARCHAR (3)  NULL,
    [CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS1]     VARCHAR (3)  NULL,
    [CC_GASTRO_SUPPORT_DAYS1]                  VARCHAR (3)  NULL,
    [CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY1]    VARCHAR (3)  NULL,
    [CC_LIVER_SUPPORT_DAYS1]                   VARCHAR (3)  NULL,
    [CC_ORGAN_SUPPORT_MAXIMUM1]                VARCHAR (2)  NULL,
    [CC_LEVEL_2_DAYS1]                         VARCHAR (3)  NULL,
    [CC_LEVEL_3_DAYS1]                         VARCHAR (3)  NULL,
    [CC_DISCHARGE_DATE1]                       VARCHAR (10) NULL,
    [CC_DISCHARGE_TIME1]                       VARCHAR (8)  NULL,
    [CC_DISCHARGE_READY_DATE1]                 VARCHAR (10) NULL,
    [CC_DISCHARGE_READY_TIME1]                 VARCHAR (8)  NULL,
    [CC_DISCHARGE_STATUS1]                     VARCHAR (2)  NULL,
    [CC_DISCHARGE_DEST1]                       VARCHAR (2)  NULL,
    [CC_DISCHARGE_LOCATION1]                   VARCHAR (2)  NULL,
    [CC_LOCAL_IDENTIFIER2]                     VARCHAR (8)  NULL,
    [CC_START_DATE2]                           VARCHAR (10) NULL,
    [CC_START_TIME2]                           VARCHAR (8)  NULL,
    [CC_UNIT_FUNCTION2]                        VARCHAR (2)  NULL,
    [CC_UNIT_BED_CONFIG2]                      VARCHAR (2)  NULL,
    [CC_ADMISSION_SOURCE2]                     VARCHAR (2)  NULL,
    [CC_SOURCE_LOCATION2]                      VARCHAR (2)  NULL,
    [CC_ADMISSION_TYPE2]                       VARCHAR (2)  NULL,
    [CC_GESTATION_LENGTH_AT_DELIV2]            VARCHAR (2)  NULL,
    [CC_ACTIVITY_DATE2]                        VARCHAR (10) NULL,
    [CC_PERSON_WEIGHT2]                        VARCHAR (7)  NULL,
    [CC_ACTIVITY_CODE2]                        VARCHAR (2)  NULL,
    [CC_HIGH_COST_DRUGS2]                      VARCHAR (4)  NULL,
    [CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS2]    VARCHAR (3)  NULL,
    [CC_BASIC_RESPIRATORY_SUPPORT_DAYS2]       VARCHAR (3)  NULL,
    [CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS2] VARCHAR (3)  NULL,
    [CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS2]    VARCHAR (3)  NULL,
    [CC_RENAL_SUPPORT_DAYS2]                   VARCHAR (3)  NULL,
    [CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS2]     VARCHAR (3)  NULL,
    [CC_GASTRO_SUPPORT_DAYS2]                  VARCHAR (3)  NULL,
    [CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY2]    VARCHAR (3)  NULL,
    [CC_LIVER_SUPPORT_DAYS2]                   VARCHAR (3)  NULL,
    [CC_ORGAN_SUPPORT_MAXIMUM2]                VARCHAR (2)  NULL,
    [CC_LEVEL_2_DAYS2]                         VARCHAR (3)  NULL,
    [CC_LEVEL_3_DAYS2]                         VARCHAR (3)  NULL,
    [CC_DISCHARGE_DATE2]                       VARCHAR (10) NULL,
    [CC_DISCHARGE_TIME2]                       VARCHAR (8)  NULL,
    [CC_DISCHARGE_READY_DATE2]                 VARCHAR (10) NULL,
    [CC_DISCHARGE_READY_TIME2]                 VARCHAR (8)  NULL,
    [CC_DISCHARGE_STATUS2]                     VARCHAR (2)  NULL,
    [CC_DISCHARGE_DEST2]                       VARCHAR (2)  NULL,
    [CC_DISCHARGE_LOCATION2]                   VARCHAR (2)  NULL,
    [CC_LOCAL_IDENTIFIER3]                     VARCHAR (8)  NULL,
    [CC_START_DATE3]                           VARCHAR (10) NULL,
    [CC_START_TIME3]                           VARCHAR (8)  NULL,
    [CC_UNIT_FUNCTION3]                        VARCHAR (2)  NULL,
    [CC_UNIT_BED_CONFIG3]                      VARCHAR (2)  NULL,
    [CC_ADMISSION_SOURCE3]                     VARCHAR (2)  NULL,
    [CC_SOURCE_LOCATION3]                      VARCHAR (2)  NULL,
    [CC_ADMISSION_TYPE3]                       VARCHAR (2)  NULL,
    [CC_GESTATION_LENGTH_AT_DELIV3]            VARCHAR (2)  NULL,
    [CC_ACTIVITY_DATE3]                        VARCHAR (10) NULL,
    [CC_PERSON_WEIGHT3]                        VARCHAR (7)  NULL,
    [CC_ACTIVITY_CODE3]                        VARCHAR (2)  NULL,
    [CC_HIGH_COST_DRUGS3]                      VARCHAR (4)  NULL,
    [CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS3]    VARCHAR (3)  NULL,
    [CC_BASIC_RESPIRATORY_SUPPORT_DAYS3]       VARCHAR (3)  NULL,
    [CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS3] VARCHAR (3)  NULL,
    [CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS3]    VARCHAR (3)  NULL,
    [CC_RENAL_SUPPORT_DAYS3]                   VARCHAR (3)  NULL,
    [CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS3]     VARCHAR (3)  NULL,
    [CC_GASTRO_SUPPORT_DAYS3]                  VARCHAR (3)  NULL,
    [CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY3]    VARCHAR (3)  NULL,
    [CC_LIVER_SUPPORT_DAYS3]                   VARCHAR (3)  NULL,
    [CC_ORGAN_SUPPORT_MAXIMUM3]                VARCHAR (2)  NULL,
    [CC_LEVEL_2_DAYS3]                         VARCHAR (3)  NULL,
    [CC_LEVEL_3_DAYS3]                         VARCHAR (3)  NULL,
    [CC_DISCHARGE_DATE3]                       VARCHAR (10) NULL,
    [CC_DISCHARGE_TIME3]                       VARCHAR (8)  NULL,
    [CC_DISCHARGE_READY_DATE3]                 VARCHAR (10) NULL,
    [CC_DISCHARGE_READY_TIME3]                 VARCHAR (8)  NULL,
    [CC_DISCHARGE_STATUS3]                     VARCHAR (2)  NULL,
    [CC_DISCHARGE_DEST3]                       VARCHAR (2)  NULL,
    [CC_DISCHARGE_LOCATION3]                   VARCHAR (2)  NULL,
    [CC_LOCAL_IDENTIFIER4]                     VARCHAR (8)  NULL,
    [CC_START_DATE4]                           VARCHAR (10) NULL,
    [CC_START_TIME4]                           VARCHAR (8)  NULL,
    [CC_UNIT_FUNCTION4]                        VARCHAR (2)  NULL,
    [CC_UNIT_BED_CONFIG4]                      VARCHAR (2)  NULL,
    [CC_ADMISSION_SOURCE4]                     VARCHAR (2)  NULL,
    [CC_SOURCE_LOCATION4]                      VARCHAR (2)  NULL,
    [CC_ADMISSION_TYPE4]                       VARCHAR (2)  NULL,
    [CC_GESTATION_LENGTH_AT_DELIV4]            VARCHAR (2)  NULL,
    [CC_ACTIVITY_DATE4]                        VARCHAR (10) NULL,
    [CC_PERSON_WEIGHT4]                        VARCHAR (7)  NULL,
    [CC_ACTIVITY_CODE4]                        VARCHAR (2)  NULL,
    [CC_HIGH_COST_DRUGS4]                      VARCHAR (4)  NULL,
    [CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS4]    VARCHAR (3)  NULL,
    [CC_BASIC_RESPIRATORY_SUPPORT_DAYS4]       VARCHAR (3)  NULL,
    [CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS4] VARCHAR (3)  NULL,
    [CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS4]    VARCHAR (3)  NULL,
    [CC_RENAL_SUPPORT_DAYS4]                   VARCHAR (3)  NULL,
    [CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS4]     VARCHAR (3)  NULL,
    [CC_GASTRO_SUPPORT_DAYS4]                  VARCHAR (3)  NULL,
    [CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY4]    VARCHAR (3)  NULL,
    [CC_LIVER_SUPPORT_DAYS4]                   VARCHAR (3)  NULL,
    [CC_ORGAN_SUPPORT_MAXIMUM4]                VARCHAR (2)  NULL,
    [CC_LEVEL_2_DAYS4]                         VARCHAR (3)  NULL,
    [CC_LEVEL_3_DAYS4]                         VARCHAR (3)  NULL,
    [CC_DISCHARGE_DATE4]                       VARCHAR (10) NULL,
    [CC_DISCHARGE_TIME4]                       VARCHAR (8)  NULL,
    [CC_DISCHARGE_READY_DATE4]                 VARCHAR (10) NULL,
    [CC_DISCHARGE_READY_TIME4]                 VARCHAR (8)  NULL,
    [CC_DISCHARGE_STATUS4]                     VARCHAR (2)  NULL,
    [CC_DISCHARGE_DEST4]                       VARCHAR (2)  NULL,
    [CC_DISCHARGE_LOCATION4]                   VARCHAR (2)  NULL,
    [CC_LOCAL_IDENTIFIER5]                     VARCHAR (8)  NULL,
    [CC_START_DATE5]                           VARCHAR (10) NULL,
    [CC_START_TIME5]                           VARCHAR (8)  NULL,
    [CC_UNIT_FUNCTION5]                        VARCHAR (2)  NULL,
    [CC_UNIT_BED_CONFIG5]                      VARCHAR (2)  NULL,
    [CC_ADMISSION_SOURCE5]                     VARCHAR (2)  NULL,
    [CC_SOURCE_LOCATION5]                      VARCHAR (2)  NULL,
    [CC_ADMISSION_TYPE5]                       VARCHAR (2)  NULL,
    [CC_GESTATION_LENGTH_AT_DELIV5]            VARCHAR (2)  NULL,
    [CC_ACTIVITY_DATE5]                        VARCHAR (10) NULL,
    [CC_PERSON_WEIGHT5]                        VARCHAR (7)  NULL,
    [CC_ACTIVITY_CODE5]                        VARCHAR (2)  NULL,
    [CC_HIGH_COST_DRUGS5]                      VARCHAR (4)  NULL,
    [CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS5]    VARCHAR (3)  NULL,
    [CC_BASIC_RESPIRATORY_SUPPORT_DAYS5]       VARCHAR (3)  NULL,
    [CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS5] VARCHAR (3)  NULL,
    [CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS5]    VARCHAR (3)  NULL,
    [CC_RENAL_SUPPORT_DAYS5]                   VARCHAR (3)  NULL,
    [CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS5]     VARCHAR (3)  NULL,
    [CC_GASTRO_SUPPORT_DAYS5]                  VARCHAR (3)  NULL,
    [CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY5]    VARCHAR (3)  NULL,
    [CC_LIVER_SUPPORT_DAYS5]                   VARCHAR (3)  NULL,
    [CC_ORGAN_SUPPORT_MAXIMUM5]                VARCHAR (2)  NULL,
    [CC_LEVEL_2_DAYS5]                         VARCHAR (3)  NULL,
    [CC_LEVEL_3_DAYS5]                         VARCHAR (3)  NULL,
    [CC_DISCHARGE_DATE5]                       VARCHAR (10) NULL,
    [CC_DISCHARGE_TIME5]                       VARCHAR (8)  NULL,
    [CC_DISCHARGE_READY_DATE5]                 VARCHAR (10) NULL,
    [CC_DISCHARGE_READY_TIME5]                 VARCHAR (8)  NULL,
    [CC_DISCHARGE_STATUS5]                     VARCHAR (2)  NULL,
    [CC_DISCHARGE_DEST5]                       VARCHAR (2)  NULL,
    [CC_DISCHARGE_LOCATION5]                   VARCHAR (2)  NULL,
    [CC_LOCAL_IDENTIFIER6]                     VARCHAR (8)  NULL,
    [CC_START_DATE6]                           VARCHAR (10) NULL,
    [CC_START_TIME6]                           VARCHAR (8)  NULL,
    [CC_UNIT_FUNCTION6]                        VARCHAR (2)  NULL,
    [CC_UNIT_BED_CONFIG6]                      VARCHAR (2)  NULL,
    [CC_ADMISSION_SOURCE6]                     VARCHAR (2)  NULL,
    [CC_SOURCE_LOCATION6]                      VARCHAR (2)  NULL,
    [CC_ADMISSION_TYPE6]                       VARCHAR (2)  NULL,
    [CC_GESTATION_LENGTH_AT_DELIV6]            VARCHAR (2)  NULL,
    [CC_ACTIVITY_DATE6]                        VARCHAR (10) NULL,
    [CC_PERSON_WEIGHT6]                        VARCHAR (7)  NULL,
    [CC_ACTIVITY_CODE6]                        VARCHAR (2)  NULL,
    [CC_HIGH_COST_DRUGS6]                      VARCHAR (4)  NULL,
    [CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS6]    VARCHAR (3)  NULL,
    [CC_BASIC_RESPIRATORY_SUPPORT_DAYS6]       VARCHAR (3)  NULL,
    [CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS6] VARCHAR (3)  NULL,
    [CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS6]    VARCHAR (3)  NULL,
    [CC_RENAL_SUPPORT_DAYS6]                   VARCHAR (3)  NULL,
    [CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS6]     VARCHAR (3)  NULL,
    [CC_GASTRO_SUPPORT_DAYS6]                  VARCHAR (3)  NULL,
    [CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY6]    VARCHAR (3)  NULL,
    [CC_LIVER_SUPPORT_DAYS6]                   VARCHAR (3)  NULL,
    [CC_ORGAN_SUPPORT_MAXIMUM6]                VARCHAR (2)  NULL,
    [CC_LEVEL_2_DAYS6]                         VARCHAR (3)  NULL,
    [CC_LEVEL_3_DAYS6]                         VARCHAR (3)  NULL,
    [CC_DISCHARGE_DATE6]                       VARCHAR (10) NULL,
    [CC_DISCHARGE_TIME6]                       VARCHAR (8)  NULL,
    [CC_DISCHARGE_READY_DATE6]                 VARCHAR (10) NULL,
    [CC_DISCHARGE_READY_TIME6]                 VARCHAR (8)  NULL,
    [CC_DISCHARGE_STATUS6]                     VARCHAR (2)  NULL,
    [CC_DISCHARGE_DEST6]                       VARCHAR (2)  NULL,
    [CC_DISCHARGE_LOCATION6]                   VARCHAR (2)  NULL,
    [CC_LOCAL_IDENTIFIER7]                     VARCHAR (8)  NULL,
    [CC_START_DATE7]                           VARCHAR (10) NULL,
    [CC_START_TIME7]                           VARCHAR (8)  NULL,
    [CC_UNIT_FUNCTION7]                        VARCHAR (2)  NULL,
    [CC_UNIT_BED_CONFIG7]                      VARCHAR (2)  NULL,
    [CC_ADMISSION_SOURCE7]                     VARCHAR (2)  NULL,
    [CC_SOURCE_LOCATION7]                      VARCHAR (2)  NULL,
    [CC_ADMISSION_TYPE7]                       VARCHAR (2)  NULL,
    [CC_GESTATION_LENGTH_AT_DELIV7]            VARCHAR (2)  NULL,
    [CC_ACTIVITY_DATE7]                        VARCHAR (10) NULL,
    [CC_PERSON_WEIGHT7]                        VARCHAR (7)  NULL,
    [CC_ACTIVITY_CODE7]                        VARCHAR (2)  NULL,
    [CC_HIGH_COST_DRUGS7]                      VARCHAR (4)  NULL,
    [CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS7]    VARCHAR (3)  NULL,
    [CC_BASIC_RESPIRATORY_SUPPORT_DAYS7]       VARCHAR (3)  NULL,
    [CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS7] VARCHAR (3)  NULL,
    [CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS7]    VARCHAR (3)  NULL,
    [CC_RENAL_SUPPORT_DAYS7]                   VARCHAR (3)  NULL,
    [CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS7]     VARCHAR (3)  NULL,
    [CC_GASTRO_SUPPORT_DAYS7]                  VARCHAR (3)  NULL,
    [CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY7]    VARCHAR (3)  NULL,
    [CC_LIVER_SUPPORT_DAYS7]                   VARCHAR (3)  NULL,
    [CC_ORGAN_SUPPORT_MAXIMUM7]                VARCHAR (2)  NULL,
    [CC_LEVEL_2_DAYS7]                         VARCHAR (3)  NULL,
    [CC_LEVEL_3_DAYS7]                         VARCHAR (3)  NULL,
    [CC_DISCHARGE_DATE7]                       VARCHAR (10) NULL,
    [CC_DISCHARGE_TIME7]                       VARCHAR (8)  NULL,
    [CC_DISCHARGE_READY_DATE7]                 VARCHAR (10) NULL,
    [CC_DISCHARGE_READY_TIME7]                 VARCHAR (8)  NULL,
    [CC_DISCHARGE_STATUS7]                     VARCHAR (2)  NULL,
    [CC_DISCHARGE_DEST7]                       VARCHAR (2)  NULL,
    [CC_DISCHARGE_LOCATION7]                   VARCHAR (2)  NULL,
    [CC_LOCAL_IDENTIFIER8]                     VARCHAR (8)  NULL,
    [CC_START_DATE8]                           VARCHAR (10) NULL,
    [CC_START_TIME8]                           VARCHAR (8)  NULL,
    [CC_UNIT_FUNCTION8]                        VARCHAR (2)  NULL,
    [CC_UNIT_BED_CONFIG8]                      VARCHAR (2)  NULL,
    [CC_ADMISSION_SOURCE8]                     VARCHAR (2)  NULL,
    [CC_SOURCE_LOCATION8]                      VARCHAR (2)  NULL,
    [CC_ADMISSION_TYPE8]                       VARCHAR (2)  NULL,
    [CC_GESTATION_LENGTH_AT_DELIV8]            VARCHAR (2)  NULL,
    [CC_ACTIVITY_DATE8]                        VARCHAR (10) NULL,
    [CC_PERSON_WEIGHT8]                        VARCHAR (7)  NULL,
    [CC_ACTIVITY_CODE8]                        VARCHAR (2)  NULL,
    [CC_HIGH_COST_DRUGS8]                      VARCHAR (4)  NULL,
    [CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS8]    VARCHAR (3)  NULL,
    [CC_BASIC_RESPIRATORY_SUPPORT_DAYS8]       VARCHAR (3)  NULL,
    [CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS8] VARCHAR (3)  NULL,
    [CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS8]    VARCHAR (3)  NULL,
    [CC_RENAL_SUPPORT_DAYS8]                   VARCHAR (3)  NULL,
    [CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS8]     VARCHAR (3)  NULL,
    [CC_GASTRO_SUPPORT_DAYS8]                  VARCHAR (3)  NULL,
    [CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY8]    VARCHAR (3)  NULL,
    [CC_LIVER_SUPPORT_DAYS8]                   VARCHAR (3)  NULL,
    [CC_ORGAN_SUPPORT_MAXIMUM8]                VARCHAR (2)  NULL,
    [CC_LEVEL_2_DAYS8]                         VARCHAR (3)  NULL,
    [CC_LEVEL_3_DAYS8]                         VARCHAR (3)  NULL,
    [CC_DISCHARGE_DATE8]                       VARCHAR (10) NULL,
    [CC_DISCHARGE_TIME8]                       VARCHAR (8)  NULL,
    [CC_DISCHARGE_READY_DATE8]                 VARCHAR (10) NULL,
    [CC_DISCHARGE_READY_TIME8]                 VARCHAR (8)  NULL,
    [CC_DISCHARGE_STATUS8]                     VARCHAR (2)  NULL,
    [CC_DISCHARGE_DEST8]                       VARCHAR (2)  NULL,
    [CC_DISCHARGE_LOCATION8]                   VARCHAR (2)  NULL,
    [CC_LOCAL_IDENTIFIER9]                     VARCHAR (8)  NULL,
    [CC_START_DATE9]                           VARCHAR (10) NULL,
    [CC_START_TIME9]                           VARCHAR (8)  NULL,
    [CC_UNIT_FUNCTION9]                        VARCHAR (2)  NULL,
    [CC_UNIT_BED_CONFIG9]                      VARCHAR (2)  NULL,
    [CC_ADMISSION_SOURCE9]                     VARCHAR (2)  NULL,
    [CC_SOURCE_LOCATION9]                      VARCHAR (2)  NULL,
    [CC_ADMISSION_TYPE9]                       VARCHAR (2)  NULL,
    [CC_GESTATION_LENGTH_AT_DELIV9]            VARCHAR (2)  NULL,
    [CC_ACTIVITY_DATE9]                        VARCHAR (10) NULL,
    [CC_PERSON_WEIGHT9]                        VARCHAR (7)  NULL,
    [CC_ACTIVITY_CODE9]                        VARCHAR (2)  NULL,
    [CC_HIGH_COST_DRUGS9]                      VARCHAR (4)  NULL,
    [CC_ADVANCED_RESPIRATORY_SUPPORT_DAYS9]    VARCHAR (3)  NULL,
    [CC_BASIC_RESPIRATORY_SUPPORT_DAYS9]       VARCHAR (3)  NULL,
    [CC_ADVANCED_CARDIOVASCULAR_SUPPORT_DAYS9] VARCHAR (3)  NULL,
    [CC_BASIC_CARDIOVASCULAR_SUPPORT_DAYS9]    VARCHAR (3)  NULL,
    [CC_RENAL_SUPPORT_DAYS9]                   VARCHAR (3)  NULL,
    [CC_NEUROLOGICAL_SYSTEM_SUPPORT_DAYS9]     VARCHAR (3)  NULL,
    [CC_GASTRO_SUPPORT_DAYS9]                  VARCHAR (3)  NULL,
    [CC_DERMATOLOGICAL_SYSTEM_SUPPORT_DAY9]    VARCHAR (3)  NULL,
    [CC_LIVER_SUPPORT_DAYS9]                   VARCHAR (3)  NULL,
    [CC_ORGAN_SUPPORT_MAXIMUM9]                VARCHAR (2)  NULL,
    [CC_LEVEL_2_DAYS9]                         VARCHAR (3)  NULL,
    [CC_LEVEL_3_DAYS9]                         VARCHAR (3)  NULL,
    [CC_DISCHARGE_DATE9]                       VARCHAR (10) NULL,
    [CC_DISCHARGE_TIME9]                       VARCHAR (8)  NULL,
    [CC_DISCHARGE_READY_DATE9]                 VARCHAR (10) NULL,
    [CC_DISCHARGE_READY_TIME9]                 VARCHAR (8)  NULL,
    [CC_DISCHARGE_STATUS9]                     VARCHAR (2)  NULL,
    [CC_DISCHARGE_DEST9]                       VARCHAR (2)  NULL,
    [CC_DISCHARGE_LOCATION9]                   VARCHAR (2)  NULL,
    [MOM_NHS_NUM]                              VARCHAR (10) NULL,
    [MOM_PAT_ID]                               VARCHAR (10) NULL,
    [MOM_PAT_ID_ORG]                           VARCHAR (5)  NULL,
    [MOM_NNN_STATUS]                           VARCHAR (2)  NULL,
    [MOM USUAL ADDRESS LINE 1]                 VARCHAR (35) NULL,
    [MOM USUAL ADDRESS LINE 2]                 VARCHAR (35) NULL,
    [MOM USUAL ADDRESS LINE 3]                 VARCHAR (35) NULL,
    [MOM USUAL ADDRESS LINE 4]                 VARCHAR (35) NULL,
    [MOM USUAL ADDRESS LINE 5]                 VARCHAR (35) NULL,
    [MOM POSTCODE OF USUAL ADDRESS]            VARCHAR (8)  NULL,
    [PROVIDER REFERENCE NUMBER]                VARCHAR (17) NULL,
    [ORGANISATION CODE (LOCAL PAT ID)]         VARCHAR (5)  NULL,
    [UBRN]                                     VARCHAR (12) NULL,
    [CARE_PATHWAY_ID]                          VARCHAR (20) NULL,
    [CARE_PATHWAY_ID_ORG]                      VARCHAR (5)  NULL,
    [REF_TO_TREAT_PERIOD_STATUS]               VARCHAR (2)  NULL,
    [REF_TO_TREAT_PERIOD_START_DATE]           VARCHAR (10) NULL,
    [REF_TO_TREAT_PERIOD_END_DATE]             VARCHAR (10) NULL
);

