﻿

CREATE Procedure [CRM].[AddClinic] as 

/*

---------------------------BATCH TEMPLATE ID------------------------------
	DON'T FORGET TO CHECK WHAT THE BATCH TEMPLATE ID SHIOULD BE FOR THE CLINICS
	63 IS FOR GENERAL ADULT
	170 IS FOR PAEDS 
---------------------------BATCH TEMPLATE ID------------------------------


----------------------------SPECIALTY CODE -------------------------------
	THE SPECIALTY CODE IN THE UPLOAD IS USED TO TELL THE PATIENT ABOUT 
	THEIR APPOINTMENT.  IT IS MAPPED FROM THE CLINIC SPECIALTY AND USES
	THE SpecialtyFunctionCode from WHOLAP.DBO.SpecialtyDivisionBase.
	
----------------------------SPECIALTY CODE -------------------------------






STEP 1 - Check for existance

	CHECK TO SEE IF THE CLINIC HAS PREVIOUSLY BEEN INCLUDED / EXCLUDED
	BY SEARCHING FOR IT IN THE THE CRM.CLINIC TABLE.  IF IT HAS PREVIOUSLY BEEN
	EXCLUDED CHECK WITH THE REQUESTOR THAT IT SHOULD BE INCLUDED AGAIN.
	SELECT * FROM CRM.CLINICS WHERE CLINICCODE IN ('<INSERT CLINIC CODE>',........)


STEP 2 - Check for locations

---------------------------LOCATIONS------------------------------
	360 CRM Scripts work off locations so each appointment needs an associated location.

	These locations are mapped from iPM HealthOrganisationCodes from the clinic location.

	To get the location for a clinic in iPM run the following script.

	select  
		 ClinicRefno = Clinic.SPONT_REFNO
		,ClinicCode = Clinic.CODE
		,LocationName = ClinicLocation.MAIN_IDENT
		,ClinicLocation.[DESCRIPTION]
		,ChronosLocation.ChronosAreaCode
		,ChronosLocation.ChronosArea
	from Lorenzo.dbo.ServicePoint Clinic
	LEFT OUTER JOIN Lorenzo.dbo.Organisation ClinicLocation 
		ON Clinic.HEORG_REFNO = ClinicLocation.HEORG_REFNO
	LEFT OUTER JOIN CRM.ChronosLocations ChronosLocation 
		on ClinicLocation.MAIN_IDENT = ChronosLocation.LocationCode COLLATE SQL_Latin1_General_CP1_CI_AS
	Where 
	Clinic.ARCHV_FLAG = 'N'
	AND
	Clinic.CODE 
	in  ('SMSCIT2R','MTSCIT1R')


	In the main the locations should already exist, but if it doesn't then ask the requestor.
	If it should be one of the locations in:

	Select * from CRM.ChronosLocationMaster 

	If not then 360 CRM need to be advised of a new location for the uploads so they can update their master file.

	The UHSM master file also needs updating.
	Update CRM.ChronosLocations
	set ChronosAreaCode = <insert code>
	Where LocationCode = <yourlocationcode>
	That should enable the mapping to pull through in the script below.

---------------------------LOCATIONS------------------------------

--Step 3 Run the select before the insert to check all is in order, e.g. check specialtyCodes

--Step 4 if happy run update

*/

Insert Into CRM.Clinics
(
[ClinicRefno],
[ClinicCode],
[ClinicDescription],
[SpecialtyCode],
[SpecialtyDescription],
[Include],
[LocationName],
[LocationArea],
[OptInClinic],
[LocationCode],
[DateFrom],
[DateTo],
[BatchTemplateID]
)

select  
--clexists.ClinicRefno,
ClinicRefNo = A.SPONT_REFNO,
ClnicCode = A.CODE,
ClinicDescription = A.[DESCRIPTION],
SpecialtyCode = C.SpecialtyFunctionCode,
SpecialtyDescription = C.SpecialtyFunction,
Include = 'Y',
LocationName = D.MAIN_IDENT, -- A.HEORG_REFNO_MAIN_IDENT,--changed where this field is pulling from as Service Point table in the new Lorenzo extracts no longer have the HEORG_REFNO_MAIN_IDENT field in 
LocationArea = E.ChronosArea,
OptInClinic = 'N',   
LocationCode = E.ChronosAreaCode,
DateFrom = CAST(CONVERT(VARCHAR(8), GetDate(),112) AS DATETIME),
DateTo = Null,
BatchTemplateID = '63'
FROM Lorenzo.dbo.ServicePoint A WITH (NOLOCK) 
LEFT OUTER JOIN WHOLAP.dbo.SpecialtyDivisionBase  C ON A.SPECT_REFNO = C.SpecialtyCode
LEFT OUTER JOIN Lorenzo.dbo.Organisation D ON A.HEORG_REFNO = D.HEORG_REFNO
LEFT OUTER JOIN CRM.ChronosLocations E on D.MAIN_IDENT = E.LocationCode COLLATE SQL_Latin1_General_CP1_CI_AS
left join CRM.Clinics clexists on clexists.ClinicRefno = A.spont_refno

WHERE 
A.Archv_flag = 'n'
and clexists.ClinicRefno is null
and
CODE in (
'SMSCIT2R'
)

----update CRM.CLINICS set SpecialtyCode = '370C', SpecialtyDescription = 'Family History'
--select * from CRM.CLINICS
----WHERE CLINICCODE LIKE ('AHOW%') 
--where SpecialtyCode = '307'

--exec CRM.ReInstateClinic 'AHOW2R'
--exec CRM.ReInstateClinic 'AHOW3R'