﻿


CREATE Procedure [CRM].[AddClinicFromTable] as 

/*

---------------------------BATCH TEMPLATE ID------------------------------
	DON'T FORGET TO CHECK WHAT THE BATCH TEMPLATE ID SHIOULD BE FOR THE CLINICS
	63 IS FOR GENERAL ADULT
	170 IS FOR PAEDS 
---------------------------BATCH TEMPLATE ID------------------------------


----------------------------SPECIALTY CODE -------------------------------
	THE SPECIALTY CODE IN THE UPLOAD IS USED TO TELL THE PATIENT ABOUT 
	THEIR APPOINTMENT.  IT IS MAPPED FROM THE CLINIC SPECIALTY AND USES
	THE SpecialtyFunctionCode from WHOLAP.DBO.SpecialtyDivisionBase.
	
----------------------------SPECIALTY CODE -------------------------------






STEP 1 - Check for existance

	CHECK TO SEE IF THE CLINIC HAS PREVIOUSLY BEEN INCLUDED / EXCLUDED
	BY SEARCHING FOR IT IN THE THE CRM.CLINIC TABLE.  IF IT HAS PREVIOUSLY BEEN
	EXCLUDED CHECK WITH THE REQUESTOR THAT IT SHOULD BE INCLUDED AGAIN.
	SELECT * FROM CRM.CLINICS WHERE CLINICCODE IN ('<INSERT CLINIC CODE>',........)
	


STEP 2 - Check for locations

---------------------------LOCATIONS------------------------------
	360 CRM Scripts work off locations so each appointment needs an associated location.

	These locations are mapped from iPM HealthOrganisationCodes from the clinic location.

	To get the location for a clinic in iPM run the following script.

	select  
		 ClinicRefno = Clinic.SPONT_REFNO
		,ClinicCode = Clinic.CODE
		,LocationName = ClinicLocation.MAIN_IDENT
		,ClinicLocation.[DESCRIPTION]
		,ChronosLocation.ChronosAreaCode
		,ChronosLocation.ChronosArea
	from Lorenzo.dbo.ServicePoint Clinic
	LEFT OUTER JOIN Lorenzo.dbo.Organisation ClinicLocation 
		ON Clinic.HEORG_REFNO = ClinicLocation.HEORG_REFNO
	LEFT OUTER JOIN CRM.ChronosLocations ChronosLocation 
		on ClinicLocation.MAIN_IDENT = ChronosLocation.LocationCode COLLATE SQL_Latin1_General_CP1_CI_AS
	Where 
	Clinic.ARCHV_FLAG = 'N'
	AND
	Clinic.CODE in 
		(
		GYNAEPREOP
		SACHNEW
		SACHREV
		AMAJP2
		AMAJ5AM
		CYSTO
		AAHLHYST
		PMJHYST
		NLEPAU

		)
	

	In the main the locations should already exist, but if it doesn't then ask the requestor.
	If it should be one of the locations in:

	Select * from CRM.ChronosLocationMaster order by ChronosAreaCode
	Select * from CRM.ChronosLocations

	If not then 360 CRM need to be advised of a new location for the uploads so they can update their master file.

	The UHSM master file also needs updating.
	Update CRM.ChronosLocations
	set ChronosAreaCode = <insert code>
	Where LocationCode = <yourlocationcode>
	That should enable the mapping to pull through in the script below.

insert into CRM.ChronosLocations (LocationCode, LocationDescription)
(
select MAIN_IDENT, DESCRIPTION 
from Lorenzo.dbo.Organisation where MAIN_IDENT = 'BURN'	)

select * from CRM.ChronosLocations where LocationCode = 'BURN'	
		
Update CRM.ChronosLocations
set ChronosAreaCode = 2, ChronosArea = 'Wythenshawe'
Where LocationCode = 'BURN'

---------------------------LOCATIONS------------------------------

--Step 3 Run the select before the insert to check all is in order, e.g. check specialtyCodes

--Step 4 if happy run update

*/

Insert Into CRM.Clinics
(
[ClinicRefno],
[ClinicCode],
[ClinicDescription],
[SpecialtyCode],
[SpecialtyDescription],
[Include],
[LocationName],
[LocationArea],
[OptInClinic],
[LocationCode],
[DateFrom],
[DateTo],
[BatchTemplateID]
)


select  
ClinicRefNo = A.SPONT_REFNO,
ClinicCode = A.CODE,
ClinicDescription = A.[DESCRIPTION],
SpecialtyCode = C.SpecialtyFunctionCode,
SpecialtyDescription = C.SpecialtyFunction,
--N.Specialty,--temp
Include = 'Y',
LocationName = A.HEORG_REFNO_MAIN_IDENT,
LocationArea = E.ChronosArea,
OptInClinic = 'N',   
LocationCode = E.ChronosAreaCode,
DateFrom = CAST(CONVERT(VARCHAR(8), GetDate(),112) AS DATETIME),
DateTo = Null,
BatchTemplateID = case when N.comment = 'paed' then 170 else 63 end
--N.comment, --temp
--AlreadyIncluded = existing.[Include]
FROM Lorenzo.dbo.ServicePoint A WITH (NOLOCK) 
LEFT OUTER JOIN WHOLAP.dbo.SpecialtyDivisionBase  C ON A.SPECT_REFNO = C.SpecialtyCode
LEFT OUTER JOIN Lorenzo.dbo.Organisation D ON A.HEORG_REFNO = D.HEORG_REFNO
LEFT OUTER JOIN CRM.ChronosLocations E on D.MAIN_IDENT = E.LocationCode COLLATE SQL_Latin1_General_CP1_CI_AS
INNER JOIN CRM.New N on N.ClinicCode = A.CODE
left join CRM.Clinics existing on existing.ClinicCode collate database_default = A.CODE collate database_default
WHERE 
A.Archv_flag = 'n'
and existing.[Include] is null
and E.ChronosArea is not null
order by A.CODE


--update CRM.CLINICS set SpecialtyCode = '9044', SpecialtyDescription = 'Musculoskeletal Physio'
--select * from CRM.CLINICS
--WHERE CLINICCODE LIKE ('MSK%') 
--and SpecialtyCode = '650'



--SELECT crm.*, new.comment FROM CRM.CLINICS crm
--inner join CRM.NewClinics2 new
--on new.cliniccode collate database_default = crm.cliniccode collate database_default

