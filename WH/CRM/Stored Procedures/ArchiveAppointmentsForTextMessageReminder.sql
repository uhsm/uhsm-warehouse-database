﻿

/*****************************************************************************
* Procedure Name:		ArchiveAppointmentsForTextMessageReminder
* Procedure Desc:		Archives the last extracts of appointments send to 
*                       ESR Connect for a text message reminder.
* Parameters:			N/A
* Returns:				N/A
* Calling Mechanism:	SSIS Package
* Tables and Alias		CRM.AppointmentsForTextMessageReminder
* Definitions:			
*
* Modification History --------------------------------------------------------
*
* Date     Author	Change
* 05/10/15 TJD		Created	
******************************************************************************/
CREATE PROCEDURE [CRM].[ArchiveAppointmentsForTextMessageReminder]
AS

SET NOCOUNT ON;

INSERT INTO CRM.AppointmentsForTextMessageReminderHistory
SELECT NAME
	,DateOfBirth
	,Sex
	,HomePhone
	,MobilePhone
	,EmailAddress
	,RefNumber
	,CustomerAgent
	,ApptID
	,Appointment
	,Location
	,FaxNumber
	,Postcode
	,TimeOfDay
	,Address1
	,Address2
	,Address3
	,Address4
	,Address5
	,CensusDate
	,OptInClinic
	,BatchTemplateID
	,CustomerInternalData
	,ExtractDate
FROM CRM.AppointmentsForTextMessageReminder;




