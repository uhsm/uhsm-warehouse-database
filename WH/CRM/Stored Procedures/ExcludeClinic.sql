﻿CREATE Procedure [CRM].[ExcludeClinic] @ClinicCode varchar(255) as

Update CRM.Clinics
Set 
	 Include = 'N'
	,DateTo = GETDATE()
Where ClinicCode = @ClinicCode