﻿








/*****************************************************************************
* Procedure Name:		ExtractAppointmentsForTextMessageReminder
* Procedure Desc:		Extract appointments that have a mobile number to be
*						sent to ESR Connect, so that a text message reminder is
*						sent to the patient.
* Parameters:			ExtractDate - Determines which days appointments are
*									  included in the file.
* Returns:				N/A
* Calling Mechanism:	SSIS Package
* Tables and Alias		OP.Encounter AS Appointment	
* Definitions:			CRM.Clinics AS ReminderClinic
*						PAS.ReferenceValueBase AS Sex
*						PAS.PatientPhoneNumber AS Phone
*
* Modification History --------------------------------------------------------
*
* Date     Author	Change
* 05/10/15 TJD		Created	
******************************************************************************/
CREATE PROCEDURE [CRM].[ExtractAppointmentsForTextMessageReminder]
  @ExtractDate as Date
AS

SET NOCOUNT ON;



TRUNCATE TABLE CRM.AppointmentsForTextMessageReminder;

INSERT INTO CRM.AppointmentsForTextMessageReminder
SELECT NAME = REPLACE(Appointment.PatientForename, ',', '') + ' ' + Appointment.PatientSurname
	,DateOfBirth = REPLACE(CONVERT(VARCHAR(10), Appointment.DateOfBirth, 121), '-', '/')
	,Sex = CASE 
		WHEN Sex.MAIN_CODE NOT IN (
				'F'
				,'M'
				)
			THEN 'U'
		ELSE Sex.MAIN_CODE
		END
	,HomePhone = CASE 
		WHEN (LEFT(RIGHT(Phone.HomePhone, 11), 2) = '07')
			THEN LEFT(Phone.HomePhone, 11)
		ELSE Phone.HomePhone
		END
	,MobilePhone = CASE 
		WHEN (
				LEFT(RIGHT(Phone.HomePhone, 11), 2) = '07'
				AND Phone.MobilePhone IS NULL
				)
			THEN SUBSTRING(Phone.HomePhone, 12, 11)
		ELSE Phone.MobilePhone
		END
	,EmailAddress = ''
	,RefNumber = Appointment.DistrictNo
	,CustomerAgent = CASE 
		WHEN ReminderClinic.Cliniccode IN (
				'HYP1PC'
				,'HYP1VM'
				,'HYP1WF'
				,'HYP2PC'
				,'HYP2VM'
				,'HYP2VW'
				,'HYP2WF'
				,'HYP3PC'
				,'HYP3VM'
				,'HYP3VW'
				,'HYP3WF'
				,'HYP4PC'
				,'HYP4VM'
				,'HYP4VW'
				,'HYP4WF'
				,'HYP5VW'
				,'HYP5WF'
				,'HYP5VM'
				,'HYP1IR'
				,'HYP2IR'
				,'HYP3IR'
				,'HYP4IR'
				,'HYP5IR'
				)
			THEN '301a'
		ELSE CASE 
				WHEN ReminderClinic.ClinicCode = 'BREAPREOP'
					THEN '103preop'
				ELSE ReminderClinic.SpecialtyCode
				END
		END
	,ApptID = Appointment.SourceUniqueID
	,Appointment = REPLACE(CONVERT(VARCHAR(16), Appointment.AppointmentTime, 121), '-', '/')
	,Location = ReminderClinic.LocationCode
	,FaxNumber = NULL
	,Postcode = Appointment.Postcode
	,TimeOfDay = '786432'
	,Address1 = Appointment.PatientAddress1
	,Address2 = Appointment.PatientAddress2
	,Address3 = Appointment.PatientAddress3
	,Address4 = Appointment.PatientAddress4
	,Address5 = CAST(NULL AS VARCHAR)
	,CensusDate = CAST(CONVERT(VARCHAR(8), GETDATE(), 112) AS DATETIME)
	,OptInClinic
	,'508' as BatchTemplateID-- amended this Batch Template ID to 508 as per telephone conversation with Laura Macfarlane from ERS connect 21/10/2015
	,ReminderClinic.Cliniccode
	,ExtractDate = @ExtractDate
FROM OP.Encounter AS Appointment
INNER JOIN CRM.Clinics AS ReminderClinic ON (Appointment.ClinicCode COLLATE SQL_Latin1_General_CP1_CI_AS = ReminderClinic.ClinicCode COLLATE SQL_Latin1_General_CP1_CI_AS)
LEFT JOIN PAS.ReferenceValueBase AS Sex ON (Appointment.SexCode = Sex.RFVAL_REFNO)
LEFT JOIN PAS.PatientPhoneNumber AS Phone ON (Appointment.SourcePatientNo = Phone.SourcePatientNo)
--LEFT JOIN CRM.StrikeExcludedClinics20160427 AS StrikeExluded ON (Appointment.ClinicCode = StrikeExluded.ClinicCode) -- Temp change to exlude strike affected clinics
WHERE ReminderClinic.Include = 'Y'
	AND ReminderClinic.DateFrom <= CAST(CONVERT(VARCHAR(8), GETDATE(), 112) AS DATETIME)
	AND Appointment.AppointmentDate = @ExtractDate
	AND Appointment.AppointmentCancelDate IS NULL
	AND Appointment.PatientSurname IS NOT NULL
	AND Phone.MobilePhone IS NOT NULL
	AND Phone.MobilePhone <> ''--22/10/2015 CM Added this criteria as some mobile phone fields are blank rather than null - LMcfarlane from ERS Connect does not want these included.
	
	--22/10/2015 CM Added this criteria as some mobile phone fields have a landline recorded - LMcfarlane from ERS Connect does not want these included.
	--discussed the way ERS are identifying these and replicated their logic in the script here to remove these records from the upload.
	AND LEN(Phone.MobilePhone) =  11
	AND Phone.MobilePhone  like '07%'
	--AND StrikeExluded.ClinicCode IS NULL -- Temp change to exlude strike affected clinics
	
		
;







