﻿

CREATE Procedure [CRM].[GetPhoneNumbers] as

If Exists (Select * from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = 'dbo' and TABLE_NAME = 'TImportPasPatientPhone')
DROP TABLE dbo.TImportPasPatientPhone

Select * 
into TImportPasPatientPhone
from 
TLoadPasPatientPhoneNumber

CREATE UNIQUE CLUSTERED INDEX [IX_TImportPasPatientPhone_Patno] ON [dbo].[TImportPasPatientPhone] 
(
	[SourcePatientNo] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


If Exists (Select * from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = 'dbo' and TABLE_NAME = 'TImportPasPatientMobile')
DROP TABLE dbo.TImportPasPatientMobile

Select * 
into TImportPasPatientMobile
from 
TLoadPasPatientMobileNumber

CREATE UNIQUE CLUSTERED INDEX [IX_TImportPasPatientMobile_Patno] ON [dbo].[TImportPasPatientMobile] 
(
	[SourcePatientNo] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


TRUNCATE TABLE PAS.PatientPhoneNumber

Insert Into PAS.PatientPhoneNumber (
SourcePatientNo,
HomePhone,
MobilePhone
)
Select 
SourcePatientNo, 
HomePhone =
		Case When LEN(ltrim(rtrim(dbo.ufnFormatPhone(PhoneNUmber)))) = 11 and LEFT(ltrim(rtrim(dbo.ufnFormatPhone(PhoneNUmber))),2) = '07' Then
			null
		Else
			ltrim(rtrim(dbo.ufnFormatPhone(PhoneNUmber)))
		End,


MobilePhone = 
		Case When LEN(ltrim(rtrim(dbo.ufnFormatPhone(PhoneNUmber)))) = 11 and LEFT(ltrim(rtrim(dbo.ufnFormatPhone(PhoneNUmber))),2) = '07' Then
			ltrim(rtrim(dbo.ufnFormatPhone(PhoneNUmber)))
		Else
			Null
		End
from TImportPasPatientPhone;

--Test merge
MERGE PAS.PatientPhoneNumber AS Target
USING (SELECT SourcePatientNo, PhoneNumber FROM dbo.TLoadPasPatientMobileNumber) AS Source
ON (Target.SourcePatientNo = Source.SourcePatientNo)
WHEN MATCHED AND Target.MobilePhone IS NULL THEN
    UPDATE SET Target.MobilePhone = Source.PhoneNumber
WHEN NOT MATCHED BY TARGET THEN
    INSERT (SourcePatientNo, MobilePhone)
    VALUES (Source.SourcePatientNo, Source.PhoneNumber);
    

--OUTPUT $action, Inserted.*, Deleted.*;

update PAS.PatientPhoneNumber set HomePhone = null where LEN(HomePhone) > 11
update PAS.PatientPhoneNumber set MobilePhone = null where LEN(MobilePhone) > 11

delete from PAS.PatientPhoneNumber where HomePhone = null and MobilePhone = null








