﻿/****** Script for SelectTopNRows command from SSMS  ******/
CREATE procedure [CRM].[ReportClinicStatus] 

	@Status varchar(15)
	,@Specialty varchar(35)
	,@Consultant varchar(50)
	,@Location varchar(35)
	,@Scheduled varchar(5)
as 

/*
--Author:	K Oakden
--Date:		21-06-2013

--To cross match clinics included in CRM service with all active clinics.

*/
select 
	ClinicCode
	,ClinicName
	,Specialty
	,Consultant
	,Location
	,ActiveFrom
	,ActiveTo
	,BatchType
	,TotalScheduled
	,AnyScheduled
	,NextBooked
	,CRMClinicStatus
from CRM.ClinicStatus
where CRMClinicStatus in (@Status)
and Specialty in (@Specialty)
and Consultant in (@Consultant)
and Location in (@Location)
and AnyScheduled in (@Scheduled)
	--case	
	--	when crmclinics.ClinicCode is null
	--		then 'Not Added'
	--	when crmclinics.DateTo is null
	--		then 'Active'
	--	when crmclinics.DateTo is not null
	--		then 'Disabled'
	--end

--	(@Status  = isnull(CRMClinicStatus, '*') 
--	or @Status = '*')
--and
--	(@Specialty  = isnull(Specialty, '*') 
--	--or @Specialty is null
--	or @Specialty = '*')
--and
--	(@Consultant  = isnull(Consultant, '*') 
--	or @Consultant = '*')
--and
--	(@Location  = isnull(Location, '*') 
--	or @Location = '*')
--and
--	(@Scheduled  = isnull(AnyScheduled, '*') 
--	or @Scheduled = '*')


	--(Location = @Location
	-- or @Location is null)

--where @Status  = CRMClinicStatus
--and @Specialty  =  Specialty
--and @Location  = Location
--and @Scheduled  = AnyScheduled
