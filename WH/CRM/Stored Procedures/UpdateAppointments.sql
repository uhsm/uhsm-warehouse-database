﻿

--Before re-running job - need to reset status parameters
--select * from dbo.Parameter where Parameter = 'CRMOutpatient';
--select MAX(UploadDate) from CRM.OutpatientUploadLog;
----Yesterday
--update dbo.Parameter set DateValue = CAST(CONVERT(varchar(8), DateAdd(DAY,-1,getdate()),112) as datetime) where Parameter = 'CRMOutpatient';
--delete from CRM.OutpatientUploadLog where UploadDate = CAST(CONVERT(varchar(8),getdate(),112) as datetime);
--Today
--UPDATE dbo.Parameter SET DateValue = CAST(CONVERT(varchar(8), getdate(),112) as datetime) where Parameter = 'CRMOutpatient'
--INsert into CRM.OutpatientUploadLog Values(CAST(CONVERT(varchar(8),getdate(),112) as datetime),1);

CREATE Procedure [CRM].[UpdateAppointments] 
	@DaysAdvance int
as

Declare 
@CRMOutpatientDate datetime, 
@CRMReminderDate datetime, 
@ExportSQL varchar(8000),
@DTSCmd varchar(8000),
@Result int,

@StartTime datetime,
@Elapsed int,
@RowsInserted Int,
@Stats varchar(255)

select @StartTime = getdate()
--Set @DaysAdvance = 

Select @CRMOutpatientDate = DateAdd(DAY,1,DateValue) from dbo.Parameter where Parameter = 'CRMOutpatient'
print 'CRM update next run on ' + CONVERT(varchar(11), @CRMOutpatientDate, 106)

Select @CRMReminderDate = DATEADD(day,@DaysAdvance,@CRMOutpatientDate)
print 'For CRM reminder date ' + CONVERT(varchar(11), @CRMReminderDate, 106)

--Do not run if already run today
IF @CRMOutpatientDate <= CAST(Convert(varchar(8),getdate(),112) as datetime) 
BEGIN
		Truncate Table CRM.OutpatientUpload

		Insert into CRM.OutpatientUpload (
			 [Name]
			,DateOfBirth
			,Sex
			,HomePhone
			,MobilePhone
			,EmailAddress
			,RefNumber
			,CustomerAgent
			,ApptID
			,Appointment
			,Location
			,FaxNumber
			,Postcode
			,TimeOfDay
			,Address1
			,Address2
			,Address3
			,Address4
			,Address5
			,CensusDate
			,OptInClinic
			,BatchTemplateID
			,CustomerInternalData
		)
		Select
			 [Name] = REPLACE(Appt.PatientForename,',','') + ' ' + Appt.PatientSurname
			,DateOfBirth = REPLACE(CONVERT(varchar(10),Appt.DateOfBirth,121), '-','/')
			,Sex = Case When Sex.MAIN_CODE Not In ('F','M') THen 
						'U'
					Else
						Sex.MAIN_CODE
					End
			,HomePhone = Case When (LEFT(Right(Phone.HomePhone,11),2) = '07') Then
								Left(Phone.HomePhone,11)
							Else
								Phone.HomePhone
							End
					
			,MobilePhone = Case When (LEFT(Right(Phone.HomePhone,11),2) = '07' and Phone.MobilePhone IS NULL) Then
								SUBSTRING(Phone.HomePhone,12,11)
							Else
								Phone.MobilePhone
							End
			,EmailAddress = ''
			,RefNumber = Appt.DistrictNo
--			,CustomerAgent = CustomerAgent.MAIN_IDENT
			,CustomerAgent = 
				case when ReminderClinic.Cliniccode in 
				(
					'HYP1PC','HYP1VM','HYP1WF','HYP2PC','HYP2VM','HYP2VW',
					'HYP2WF','HYP3PC','HYP3VM','HYP3VW','HYP3WF','HYP4PC',
					'HYP4VM','HYP4VW','HYP4WF','HYP5VW','HYP5WF','HYP5VM',
					'HYP1IR','HYP2IR','HYP3IR','HYP4IR','HYP5IR'
				)
					Then '301a'
				Else
					case when ReminderClinic.ClinicCode = 'BREAPREOP' THEN
						'103preop'
					Else
						ReminderClinic.SpecialtyCode
					End
				End			
			,ApptID = Appt.SourceUniqueID
			,Appointment = REPLACE(CONVERT(varchar(16),Appt.AppointmentTime,121), '-','/')
			,Location = ReminderClinic.LocationCode
			,FaxNumber = null
			,Postcode = Appt.Postcode
			,TimeOfDay = '786432'
			,Address1 = Appt.PatientAddress1
			,Address2 = Appt.PatientAddress2
			,Address3 = Appt.PatientAddress3
			,Address4 = Appt.PatientAddress4
			,Address5 = CAST(null as varchar)
			,CensusDate = CAst(convert(varchar(8),GETDATE(),112) as datetime)
			,OptInClinic
			,BatchTemplateID
			,ReminderClinic.Cliniccode
		From OP.Encounter Appt
		Inner Join CRM.Clinics ReminderClinic
			on Appt.ClinicCode COLLATE SQL_Latin1_General_CP1_CI_AS = ReminderClinic.ClinicCode COLLATE SQL_Latin1_General_CP1_CI_AS
		Left Outer Join PAS.ReferenceValueBase Sex
			on Appt.SexCode = Sex.RFVAL_REFNO
		Left Outer Join PAS.PatientPhoneNumber Phone
			on Appt.SourcePatientNo = Phone.SourcePatientNo
--		Left Outer JOin PAS.SpecialtyBase CustomerAgent
--			on Appt.SpecialtyCode = CustomerAgent.SPECT_REFNO
		Where 
		ReminderClinic.Include = 'Y'
		And ReminderClinic.DateFrom <= CAST(Convert(varchar(8), getdate(),112) as datetime)
		And Appt.AppointmentDate = DATEADD(day,@DaysAdvance,@CRMOutpatientDate)
		And Appt.AppointmentCancelDate is null
		And Appt.PatientSurname is not null
select @RowsInserted = @@Rowcount


--Get Cancellations
Truncate Table CRM.OutpatientCancellation
Insert into CRM.OutpatientCancellation
Select 
OPAppt.SourceUniqueID,
[Cancel Reason] = 'Cancelled by ' + CancelledBy.DESCRIPTION + ' on ' + Convert(varchar(10), AppointmentCancelDate,103),
CAST(Convert(varchar(8), getdate(),112) as datetime)
FROM OP.Encounter OPAppt
left outer join PAS.ReferenceValueBase CancelledBy
	on OPAppt.CancelledByCode = CancelledBy.RFVAL_REFNO
Where 
(AppointmentDate <= CAST(Convert(varchar(8), getdate(),112) as datetime) +7
and AppointmentDate >CAST(Convert(varchar(8), getdate(),112) as datetime)
)
and AppointmentCancelDate is not null 

and
(
Exists 
(Select 1 from CRM.OutpatientUploadHistory UploadHist where OPAppt.SourceUniqueID = UploadHist.ApptID)
And Not Exists
(Select 1 from CRM.OutpatientCancellationHistory CancHist where OPAppt.SourceUniqueID = CancHist.[Internal Key])
)

--Handled in SSIS
--Insert into CRM.OutpatientCancellationHistory
--(
--[Internal Key] 
--,[Cancel Reason] 
--,CensusDate
--)
--Select
--[Internal Key]
--,[Cancel reason]
--,[censusdate]
--From CRM.OutpatientCancellation

--Update dispositions

--exec CRM.UpdateDispositions 14
exec CRM.UpdateDispositions @CRMReminderDate
/*
--Export data
			SET @dtscmd = 'dtexec /f "D:\SSIS\CRM Exports\CRM Exports\Outpatient.dtsx"'

			EXEC @Result =	Master..xp_cmdShell @dtscmd
			Print @result
			
			

				IF(@Result = 0)
					BEGIN
					PRINT 'DTS SUCCESS'
					UPDATE dbo.Parameter
					SET DateValue = CAST(CONVERT(varchar(8), getdate(),112) as datetime)
					Where Parameter = 'CRMOutpatient'
					
					Insert into CRM.OutpatientUploadHistory 
					Select * from CRM.OutpatientUpload


					END
*/
select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'CRM - WH UpdateAppointments', @Stats, @StartTime
END



