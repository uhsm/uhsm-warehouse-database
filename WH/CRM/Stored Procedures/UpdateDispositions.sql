﻿

CREATE Procedure [CRM].[UpdateDispositions] 
	--@DaysAgo int,
	@ReminderDate datetime
as

Declare @DispositionDate datetime
--Declare @AppointmentDate datetime
--Set @DaysAgo = 7
declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

--select @AppointmentDate = CAST(CONVERT(varchar(8),DATEADD(day,-@DaysAgo,GETDATE()),112) as datetime)
select @DispositionDate = DATEADD(day, -21, @ReminderDate)
BEGIN
--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CRM].[OutpatientDisposition]') AND type in (N'U'))
--DROP TABLE [CRM].[OutpatientDisposition]
--GO
	truncate table CRM.OutpatientDisposition

	/* Use appointment dates from CRM.OutpatientUploadHistory instead of OP.Encounter
	to include all data sent to CRM in the given time period. Some of these appointments 
	will since have been rescheduled to lie outside of time period in OP.Encounter
	*/
	INSERT INTO CRM.OutpatientDisposition
           (DispositionCensusDate
           ,CallKey
           ,Disposition
           ,Info
           ,EncounterDistrictNo
           ,EncounterClinicCode
           ,EncounterAppointmentTime
           ,AppointmentStatusCode
           ,UploadedAppointmentTime
           ,MatchingAppointmentTime
           ,UploadedCensusDate
           ,BatchTemplateID
           ,CancelReason
           ,CancelledCensusDate)
		(
		select distinct 
			DispositionCensusDate = CAST(CONVERT(varchar(8),GETDATE(),112) as datetime)
			,CallKey = Uploaded.ApptID
			,Disposition =
				case OPEncounter.AppointmentStatusCode
					when 357 then 'Attended'  --Attended On Time
					when 2868 then 'Attended'  --Patient Late / Seen
					when 2004151 then 'Attended'  --Attended
					when 2870 then 'Cancelled'  --Cancelled By Patient
					when 2003532 then 'Cancelled'  --Cancelled
					when 2003534 then 'Cancelled'  --Unable to Attend
					when 2004300 then 'Cancelled'  --Cancelled by Hospital
					when 2004301 then 'Cancelled'  --Cancelled by Patient
					when 2004528 then 'Cancelled'  --Cancelled by EBS
					when 3006508 then 'Cancelled'  --Referral Cancelled
					when 358 then 'DidNotAttend'  --Did Not Attend
					when 2000724 then 'DidNotAttend'  --Patient Late / Not Seen
					when 2003494 then 'DidNotAttend'  --Patient Left / Not seen
					when 2003495 then 'DidNotAttend'  --Refused To Wait
				else 'Other'
				end
			,Info = ATTNDCodeLU.ReferenceValue
			,EncounterDistrictNo = OPEncounter.DistrictNo
			,EncounterClinicCode = OPEncounter.ClinicCode
			,EncounterAppointmentTime = OPEncounter.AppointmentTime
			,OPEncounter.AppointmentStatusCode
			,UploadedAppointmentTime = CONVERT(datetime,REPLACE(Uploaded.Appointment, '/','-'),121)
			,MatchingAppointmentTime = DATEDIFF(MINUTE, OPEncounter.AppointmentTime, CONVERT(datetime,REPLACE(Uploaded.Appointment, '/','-'),121))
			,UploadedCensusDate = Uploaded.CensusDate
			,Uploaded.BatchTemplateID
			,CancelReason = Cancelled.[Cancel Reason]
			,CancelledCensusDate = Cancelled.CensusDate
		--into CRM.OutpatientDisposition
		from CRM.OutpatientUploadHistory Uploaded
		left join CRM.OutpatientCancellationHistory Cancelled
			on Cancelled.[Internal Key] = Uploaded.ApptID
		left join OP.Encounter OPEncounter
			on OPEncounter.SourceUniqueID = Uploaded.ApptID
		left join PAS.ReferenceValue ATTNDCodeLU 
			on OPEncounter.AppointmentStatusCode = ATTNDCodeLU.ReferenceValueCode
		
		--Select specific date
		where CONVERT(datetime,LEFT(Appointment, 10),111) = @DispositionDate
		
		--Exclude appointments not outcomed, either because they have been rescheduled, or attended and not outcomed yet
		and isnull(AppointmentStatusCode, 45) <> 45 
			
		--Exclude appointments where uploaded appointment date does not match actual appointment date
		and DATEDIFF(MINUTE, OPEncounter.AppointmentTime, CONVERT(datetime,REPLACE(Uploaded.Appointment, '/','-'),121)) = 0
		)
		
		----Archive - now handled in SSIS package
		----drop table CRM.OutpatientDispositionHistory
		----select * into CRM.OutpatientDispositionHistory from CRM.OutpatientDisposition
		----truncate table CRM.OutpatientDispositionHistory
		--Insert into CRM.OutpatientDispositionHistory
		--select * from CRM.OutpatientDisposition

select @RowsInserted = @@Rowcount
select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'CRM - WH UpdateDispositions', @Stats, @StartTime

END




