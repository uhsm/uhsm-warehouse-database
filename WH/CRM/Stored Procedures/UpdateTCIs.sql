﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		CRM.UpdateTCIs

Notes:			Stored in 

Versions:
				1.0.0.3 - 26/10/2015 - MT
					Modified to use the new PatientAddressHistory table.

				1.0.0.2 - 10/06/2013 - KO
					To include wards KDU and F8M for all specialties
					and F1 for specialty 107(Vascular Surgery) for batch 136.

				1.0.0.1 - 14/02/2013 KO
					To include wards F16 and F1 for specialties 103(Breast) 
					and 502(Gynaecology) for batch 136.

				1.0.0.0 - 10/12/2013 - KO
					Created sproc.
					Migrated from infoSQL.
					Additional data manipulation in CRM.TCIForExport
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [CRM].[UpdateTCIs] 
	@DaysAdvance int
As

/** 
Will only run from SSIS if following condition returns 1
To re-run todays batch, delete todays entry from Theatre uploadlog
--delete from CRM.AdmissionUploadLog where UploadDate = '2013-12-12 00:00:00.000'
 
Select
CanUpload = 
CONVERT(bit,
	Case 
		When Exists 
			(
			Select 1 
			from CRM.AdmissionUploadLog 
			Where 
			UploadDate = CAST(convert(varchar(8),getdate(),112) as datetime) 
			and Uploaded = 1
			)
		Then 0
		else 1
	end)
**/

Declare
	@StartTime datetime,
	@Elapsed int,
	@RowsInserted Int,
	@Stats varchar(255),
	@CRMTCIDate datetime, 
	@CRMReminderDate datetime
	
select @StartTime = getdate()

Select @CRMTCIDate =   DateAdd(DAY,1,DateValue) from dbo.Parameter where Parameter = 'CRMAdmission'
print 'CRM update next run on ' + CONVERT(varchar(11), @CRMTCIDate, 106)

Select @CRMReminderDate = DATEADD(day,@DaysAdvance,@CRMTCIDate)
print 'For CRM reminder date ' + CONVERT(varchar(11), @CRMReminderDate, 106)

truncate table CRM.TCI

insert into CRM.TCI

Select distinct
	Waiters.SourceUniqueID
	,Waiters.SourcePatientNo
	,Waiters.ReferralSourceUniqueID
	,Waiters.DistrictNo
	,Waiters.NHSNumber
	,Name = Waiters.PatientForename + ' ' + Waiters.PatientSurname
	,waiters.DateOfBirth
	,Sex = 
		case 
			when left(waiters.Sex,1) not in ('M','F')  
			then 'U'
			when left(waiters.Sex,1) is NULL--added this additional criteria as per Haroon Harif suggestion at CRM 15/02/2016
			then 'U'
			else left(waiters.sex,1)
		end
	,Age = DAteDiff(Year,waiters.DateOfBirth, Waiters.TCIDate)
	,LocationCode = '2' 
	,Address1 = Left(patadd.Address1,35)
	,Address2 = Left(patadd.Address2,35)
	,Address3 = Left(patadd.Address3,35)
	,Address4 = Left(patadd.Address4,35)
	,Address5 = Cast(Null as Varchar(35)) 
	,patadd.Postcode
	,[Address] = patadd.Address1 + ' ' +  patadd.Address2 + ' ' + patadd.Address3 + ' ' + patadd.Address4
	,HomePhone = 
		Case 
			When (LEFT(Right(Phone.HomePhone,11),2) = '07') 
			Then Left(Phone.HomePhone,11)
			Else Phone.HomePhone
		End
	,MobilePhone = 
		Case 
			When (LEFT(Right(Phone.HomePhone,11),2) = '07' and Phone.MobilePhone IS NULL) 
			Then SUBSTRING(Phone.HomePhone,12,11)
			Else Phone.MobilePhone
		End
	,ApptID = Spec.Specialty + '-' + cast(Waiters.SourceUniqueID as varchar)
	,Waiters.TCIDate
	,Waiters.AdmissionWard
	,BatchID = 
		Case Waiters.AdmissionWard
			WHEN 'ADMLOU' THEN '136'
			WHEN 'F16' THEN '136'
			WHEN 'F1' THEN '136'
			WHEN 'F8M' THEN '136'
			WHEN 'KDU' THEN '136'
			WHEN 'BS' THEN '141'
			WHEN 'TDC' THEN '145'
			WHEN 'GIU' THEN '145'
			ELSE '999'
		END

from 
(
	Select * from PTL.IPWL
	where 
	DataSource = 'IPM' 
	and EntryStatus = 'Waiting'
	and (
		(AdmissionWard in ('ADMLOU','BS','TDC','GIU', 'F8M', 'KDU'))
		or 
		(AdmissionWard in ('F16','F1') and SpecialtyCode in ('502','103'))
		or 
		(AdmissionWard = 'F1' and SpecialtyCode = '107')
		)
	and 
	(
	not
	(
		PlannedProcedure like '%evac%'
		or GeneralComment like '%evac%'
		or PatientPrepComments like '%evac%'
	)
	or
	(
		PlannedProcedure is null
		or GeneralComment is null
		or PatientPrepComments is null
	)
	)
) waiters
INNER JOIN
(
	select * 
	from PTL.IPWLAdmissionOffers
	where 
	LatestOffer = 'Y' 
	and cast(convert(varchar(8), TCIDate,112) as datetime) = cast(convert(varchar(8), getdate()	,112) as datetime) 
		+ @DaysAdvance
	AND 
		(
		OfferOutcomeCode NOT IN 
			(
			'PCAN', --Admission Cancelled by or on behalf of Patient
			'A',	--Admitted - Treatment Completed
			'A1',	--Admitted - Treatment Commenced
			'DNA',	--DNA (Did Not Attend)
			'PATAD', --Patient Admitted - Died
			'C6', 	 --Cancelled by hosp. before Day of Admission
			'CANONDAY', --	Cancelled by hosp. on Day of Admission
			'5', 	--Admitted - Treatment Deferred
			'DEATH' --Cancelled due to Patient Death
			--'NSP'	--Not Specified
			)
		or OfferOutcomeCode IS NULL
		)
		
) Offers 
	on Waiters.SourceUniqueID = Offers.WaitingListID
	
left join PAS.PatientAddressHistory patadd
	on Waiters.SourcePatientNo = patadd.PatientNo
	And patadd.CurrentFlag = 'Y'
	
left join PAS.Specialty spec 
	on Waiters.SpecialtyCode = spec.LocalSpecialtyCode
	
left join PAS.PatientPhoneNumber Phone
	on Waiters.SourcePatientNo = Phone.SourcePatientNo

Left Join Lorenzo.dbo.Patient pat
	On Waiters.SourcePatientNo = pat.PATNT_REFNO

Where pat.DATE_OF_DEATH is null


 --Keep History of records sent
insert into CRM.TCIHistory (
	UploadDate
	,Name
	,DateOfBirth
	,Sex
	,HomePhone
	,MobilePhone
	,EmailAddress
	,RefNumber
	,CustomerAgent
	,ApptID
	,Appointment
	,Location
	,FaxNumber
	,PostCode
	,TimeOfDay
	,Address1
	,Address2
	,Address3
	,Address4
	,Address5
	,Admission_ward
	,BatchID
	)
select 
	UploadDate = Getdate() 
	,Name
	,DateOfBirth
	,Sex
	,HomePhone
	,MobilePhone
	,EmailAddress
	,RefNumber
	,CustomerAgent
	,ApptID
	,Appointment
	,Location
	,FaxNumber
	,PostCode
	,TimeOfDay
	,Address1
	,Address2
	,Address3
	,Address4
	,Address5
	,AdmissionWard
	,BatchID

From CRM.TCIFOrExport 


select @RowsInserted = @@Rowcount

select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'CRM - WH UpdateTCIs', @Stats, @StartTime
