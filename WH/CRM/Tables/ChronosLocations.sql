﻿CREATE TABLE [CRM].[ChronosLocations] (
    [LocationCode]        VARCHAR (25)  COLLATE Latin1_General_CI_AS NULL,
    [LocationDescription] VARCHAR (255) COLLATE Latin1_General_CI_AS NULL,
    [ChronosAreaCode]     VARCHAR (20)  COLLATE Latin1_General_CI_AS NULL,
    [ChronosArea]         VARCHAR (100) COLLATE Latin1_General_CI_AS NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ChronosLocationCode]
    ON [CRM].[ChronosLocations]([LocationCode] ASC);

