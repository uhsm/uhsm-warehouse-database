﻿CREATE TABLE [CRM].[ClinicsBackUp] (
    [ClinicRefno]          NUMERIC (12)  NULL,
    [ClinicCode]           VARCHAR (25)  COLLATE Latin1_General_CI_AS NULL,
    [ClinicDescription]    VARCHAR (255) COLLATE Latin1_General_CI_AS NULL,
    [SpecialtyCode]        VARCHAR (25)  COLLATE Latin1_General_CI_AS NULL,
    [SpecialtyDescription] VARCHAR (255) COLLATE Latin1_General_CI_AS NULL,
    [Include]              CHAR (1)      COLLATE Latin1_General_CI_AS NULL,
    [LocationName]         VARCHAR (255) COLLATE Latin1_General_CI_AS NULL,
    [LocationArea]         VARCHAR (50)  COLLATE Latin1_General_CI_AS NULL,
    [OptInClinic]          CHAR (1)      COLLATE Latin1_General_CI_AS NULL,
    [LocationCode]         VARCHAR (2)   COLLATE Latin1_General_CI_AS NULL,
    [DateFrom]             DATETIME      NULL,
    [DateTo]               DATETIME      NULL,
    [BatchTemplateID]      VARCHAR (5)   NULL
);

