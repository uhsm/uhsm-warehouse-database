﻿CREATE TABLE [CRM].[OutpatientCancellation] (
    [Internal Key]  INT           NOT NULL,
    [Cancel Reason] VARCHAR (200) NULL,
    [CensusDate]    DATETIME      NULL
);

