﻿CREATE TABLE [CRM].[OutpatientCancellationHistory] (
    [Internal Key]  INT           NOT NULL,
    [Cancel Reason] VARCHAR (200) NULL,
    [CensusDate]    DATETIME      NULL
);

