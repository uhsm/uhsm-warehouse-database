﻿CREATE TABLE [CRM].[OutpatientDispositionHistory] (
    [DispositionCensusDate]    DATETIME      NULL,
    [CallKey]                  INT           NOT NULL,
    [Disposition]              VARCHAR (12)  NOT NULL,
    [Info]                     VARCHAR (80)  NULL,
    [EncounterDistrictNo]      VARCHAR (20)  NULL,
    [EncounterClinicCode]      VARCHAR (25)  NULL,
    [EncounterAppointmentTime] SMALLDATETIME NULL,
    [AppointmentStatusCode]    INT           NULL,
    [UploadedAppointmentTime]  DATETIME      NULL,
    [MatchingAppointmentTime]  INT           NULL,
    [UploadedCensusDate]       DATETIME      NOT NULL,
    [BatchTemplateID]          VARCHAR (5)   NULL,
    [CancelReason]             VARCHAR (200) NULL,
    [CancelledCensusDate]      DATETIME      NULL
);

