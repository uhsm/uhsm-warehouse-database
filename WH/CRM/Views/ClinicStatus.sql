﻿







/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [CRM].[ClinicStatus] 


as 

/*
--Author:	K Oakden
--Date:		21-06-2013

--To cross match clinics included in CRM service with all active clinics.
--Required for report CRM Clinic Status
*/
select distinct
	ClinicCode = isnull(crmclinics.ClinicCode, active.ClinicCode)
	,ClinicName = isnull(crmclinics.ClinicDescription, clinicAll.ServicePoint)
	--,Specialty = dbo.ConvertToProperCase(isnull(crmclinics.SpecialtyDescription, spec.Specialty))
	,Specialty = 
		case 
			when crmclinics.DateTo is not null
			then dbo.ConvertToProperCase(spec.Specialty)
			else dbo.ConvertToProperCase(isnull(crmclinics.SpecialtyDescription, spec.Specialty))
		end
	,Consultant = isnull(Consultant.Consultant, 'N/A')
	,spec.SpecialtyCode
	--,Location = isnull(crmclinics.LocationArea, 'CRM location not set')
	,Location = crmclinics.LocationArea
	,ActiveFrom = convert(varchar(10), crmclinics.DateFrom, 103)
	,ActiveTo = convert(varchar(10), crmclinics.DateTo, 103)
	,BatchType = case 
		when crmclinics.BatchTemplateID = 170 
			then 'Paediatric' 
		when crmclinics.BatchTemplateID = 63 
			then 'Adult' end 
	,TotalScheduled = isnull(active.TotalScheduled, 0)
	,ScheduledNext28Days = isnull(active.ScheduledDay28, 0)
	,AnyScheduled = 
		case 
			when isnull(active.TotalScheduled, 0) = 0 
			then 'No'
			else 'Yes'
		end
	,NextBooked = convert(varchar(10), active.NextAppointment, 103)
	,CRMClinicStatus = case	
		when crmclinics.ClinicCode is null
			then 'Not Added'
		when crmclinics.DateTo is null
			then 'Active'
		else 'Disabled'
		end
from CRM.Clinics crmclinics


	
full join
	(
	select 
		scheduled.ClinicCode
		,TotalScheduled = count(scheduled.SourceUniqueID)
		,ScheduledDay28 = sum(scheduled.Next28Day)
		--,ScheduledMonth1 = sum(scheduled.Next1Month)
		--,ScheduledMonth2 = sum(scheduled.Next2Month)
		--,ScheduledMonth3 = sum(scheduled.Next3Month)
		,NextAppointment = MIN(AppointmentDate)
	from 
	(
		select 
			enc.ClinicCode
			,enc.AppointmentDate
			,enc.SourceUniqueID
			,Next28Day = case when datediff(DAY, getdate(), enc.AppointmentDate) < 29 then 1 else 0 end
			--,Next1Month = case when datediff(MONTH, getdate(), enc.AppointmentDate) = 1 then 1 else 0 end
			--,Next2Month = case when datediff(MONTH, getdate(), enc.AppointmentDate) = 2 then 1 else 0 end
			--,Next3Month = case when datediff(MONTH, getdate(), enc.AppointmentDate) = 3 then 1 else 0 end
		from OP.Encounter enc
		where AppointmentCancelDate is null
		and AppointmentDate > GETDATE()
		--order by AppointmentDate
		)scheduled
	group by scheduled.ClinicCode
	)active
	on active.ClinicCode COLLATE DATABASE_DEFAULT = crmclinics.ClinicCode COLLATE DATABASE_DEFAULT
		
left join PAS.ServicePoint clinicAll
	on clinicAll.ServicePointLocalCode COLLATE DATABASE_DEFAULT
	= isnull(crmclinics.ClinicCode, active.ClinicCode) COLLATE DATABASE_DEFAULT
	and clinicAll.ArchiveFlag = 'N'
	
left join PAS.Consultant Consultant
	on	Consultant.ConsultantCode = clinicAll.ConsultantCode

	
left join PAS.Specialty spec
	on spec.SpecialtyCode = clinicAll.SpecialtyCode
	
--where 

--	(case	
--		when crmclinics.ClinicCode is null
--			then 'Not Added'
--		when crmclinics.DateTo is null
--			then 'Active'
--		when crmclinics.DateTo is not null
--			then 'Disabled'
--	end
--	 = @Status)
--	 or @Status is null









