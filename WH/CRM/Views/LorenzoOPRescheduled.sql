﻿CREATE view CRM.LorenzoOPRescheduled as (
select distinct
CAST(ScheduleEvent.PATNT_REFNO AS VARCHAR(20)) AS PAT_REF,
CAST(ScheduleHistory.SCDHS_REFNO AS VARCHAR(20)) AS HIST_REF,
CAST(ScheduleHistory.SCHDL_REFNO AS VARCHAR(20)) AS APPT_REF,
MoveReason.ReferenceValue,
--LastSchdlChange.TOTAL_CANC As TOTAL_PAT_CANCELLATIONS,
CAST(OLD_START_DTTM AS DATETIME) AS OLD_APPT_DATE,
CAST(ScheduleHistory.NEW_START_DTTM AS DATETIME) AS NEW_APPT_DATE,
CAST(ScheduleHistory.MOVE_DTTM AS DATETIME) AS DATE_RESCHEDULED,
ScheduleHistory.COMMENTS,
CAST(ScheduleHistory.CREATE_DTTM AS DATETIME) AS DATE_CREATED
--into PTL.AppointmentRescheduleCount

FROM Lorenzo.dbo.ScheduleHistory ScheduleHistory

--INNER JOIN 
--	(SELECT
--		SCHDL_REFNO,
--		MAX(SCDHS_REFNO) AS LAST_CHANGE,
--		COUNT(1) AS TOTAL_CANC
--	FROM Lorenzo.dbo.ScheduleHistory
--	WHERE MOVRN_REFNO = 4520
--		AND ARCHV_FLAG = 'N'
--		AND CAST(OLD_START_DTTM AS DATETIME) <= CAST(FLOOR(CAST(GETDATE() AS FLOAT)) AS DATETIME)
--	GROUP BY SCHDL_REFNO
--	) LastSchdlChange 
--	ON ScheduleHistory.SCDHS_REFNO = LastSchdlChange.LAST_CHANGE

INNER JOIN Lorenzo.dbo.ScheduleEvent ScheduleEvent 
	ON ScheduleHistory.SCHDL_REFNO = ScheduleEvent.SCHDL_REFNO

left join PAS.ReferenceValue MoveReason
	ON ScheduleHistory.MOVRN_REFNO = MoveReason.ReferenceValueCode
	
where ScheduleHistory.ARCHV_FLAG = 'N'
--AND ScheduleHistory.MOVRN_REFNO = 4520
AND ScheduleEvent.SCTYP_REFNO = 1470
AND CAST(ScheduleHistory.OLD_START_DTTM AS DATETIME) <= CAST(FLOOR(CAST(GETDATE() AS FLOAT)) AS DATETIME)
)

--select * from Lorenzo.dbo.ScheduleHistory where SCHDL_REFNO = 180336718
--select * from PAS.ReferenceValue where ReferenceDomainCode = 'MOVRN'