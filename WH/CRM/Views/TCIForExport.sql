﻿

CREATE view [CRM].[TCIForExport] as
/** 
10/12/2013 - KO Migrated from infoSQL. CRM.TCI table updated by SP [CRM].[UpdateTCIs]
which is called from SSIS.

--To Do: Merge into single SP
**/

Select 
	[Name],
	REPLACE(CONVERT(varchar(16),DateOfBirth,121), '-','/') AS DateOfBirth,
	Sex,
	HomePhone,
	MobilePhone,
	Cast(Null as varchar(10)) as EmailAddress,
	DistrictNo as RefNumber,
	--Cast(NUll as varchar(5)) as CustomerAgent,
	Case 
		When (AdmissionWard = 'TDC' or AdmissionWard = 'GIU') 
		Then AdmissionWard
		Else 'INPT' 
	End as CustomerAgent,
	ApptID,
	REPLACE(CONVERT(varchar(16),tcidate,121), '-','/') AS Appointment,
	'2' AS Location,
	NUll as FaxNumber,
	PostCode,
	'786432' AS TimeOfDay,
	Address1,
	Address2,
	Address3,
	Address4,
	Address5,
	AdmissionWard,
	BatchID
from CRM.TCI
Where (Address is null or address Not LIke '%Prison%')
and tcidate is not null
and DistrictNo is not null

