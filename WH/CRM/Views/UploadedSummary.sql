﻿CREATE view CRM.UploadedSummary as
/**
*	KO 13/08/2014
*	Summary of CRM uploads by day of week. This is used to check the expected number of 
*	uploads have been sent to CRM on a given day for the following batch ID's
*		136	Planned Admissions - Inpatient
*		141	Planned Admissions - Baguley Pain 
*		145	Planned Admissions - TDC
*		63	Outpatient - Adult
*		170	Outpatient - Paediatric
*	Upload counts should correspond to CRM upload logs at \\v-uhsm-dw01\H:\360 CRM
**/

select * from (
	select Batch, Uploaded,datename(WEEKDAY, Uploaded) as DayOfWk, COUNT(1) as total
	from 
	(
		select 
			Batch = BatchID collate database_default
			,Uploaded = dateadd(day, 0, datediff(day, 0, UploadDate))
			,TCIDate = dateadd(day, 0, datediff(day, 0, Appointment))
		from CRM.TCIHistory
		union all
		select 
			BatchTemplateID collate database_default
			,CensusDate
			,AppointmentDate = dateadd(day, 0, datediff(day, 0, Appointment))
		from CRM.OutpatientUploadHistory
	) uploaded
	group by Batch, Uploaded, TCIDate
)UploadedSummary
PIVOT (SUM(total) FOR [Batch] IN ([136], [141], [145], [63], [170])) AS pvt


--select * from CRM.UploadedSummary order by Uploaded desc

