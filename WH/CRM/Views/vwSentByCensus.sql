﻿create view CRM.vwSentByCensus as(
select	
	uploaded.ApptID
	,uploaded.Appointment
	,UploadedCensusDate = uploaded.CensusDate
	,cancelled.[Cancel Reason]
	,CancelledCensusDate = cancelled.CensusDate
from CRM.OutpatientUploadHistory uploaded
left join CRM.OutpatientCancellationHistory cancelled
	on cancelled.[Internal Key] = uploaded.ApptID
where uploaded.CensusDate = CAST(CONVERT(varchar(8),DATEADD(day,-14,GETDATE()),112) as datetime)
)