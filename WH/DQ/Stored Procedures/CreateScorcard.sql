﻿Create Procedure DQ.CreateScorcard as
/*
Create table DQ.IndicatorMaster (
	 IndicatorCode varchar(5) not null
	,IndicatorArea varchar(55) null
	,IndicatorSubArea varchar(55) null
	,IndicatorName varchar(55) null
	,IndicatorDescription varchar(100) null
	,IndicatorDefninition varchar(500) null
	,ImprovementTarget bit not null
	,StartDate date null
	,EndDate date null
, CONSTRAINT [pk_DQ_Indicators] PRIMARY KEY CLUSTERED (IndicatorCode)
)


Create Table DQ.Indicators (
	 RunDate datetime not null
	,IndicatorCode varchar(5) not null
	,Period varchar(6) not null
	,Value int null
	,pc numeric(2,0) null
	,IndicatorStatus varchar(20)
	,Constraint [pk_Indicator] PRIMARY KEY CLUSTERED (RunDate, IndicatorCode)
	)
*/



--Select * from DQ.IndicatorMaster
--Select * from DQ.Indicators
Select 
	 RunDate = GETDATE()
	,IndicatorCode = 'A-1'
	,Period = CAST(Datepart(Year,AdmissionDate) as varchar(4)) 
	+ '-'
	+ Right('0' + cast(datepart(month,AdmissionDate) as varchar(2)),2)
	,Value = Sum(Case When NHSNumber is null then 1 else 0 end)
	,pc = 100 - 
		(
		(
		cast(Sum(Case When NHSNumber is null then 1 else 0 end) as numeric)
		/ cast(Sum(Case When NHSNumber is null then 1 else 1 end) as numeric)
		) * 100
		)
from APC.Encounter
Where 
(
AdmissionDate >='20100401 00:00:00'
and AdmissionDate <'20110301 00:00:00'
)
Group By
	CAST(Datepart(Year,AdmissionDate) as varchar(4)) 
	+ '-'
	+ Right('0' + cast(datepart(month,AdmissionDate) as varchar(2)),2)


Union All
Select 
	 RunDate = GETDATE()
	,IndicatorCode = 'A-1'
	,Period = 'YTD'
	,Value = Sum(Case When NHSNumber is null then 1 else 0 end)
	,pc = 100 - 
		(
		(
		cast(Sum(Case When NHSNumber is null then 1 else 0 end) as numeric)
		/ cast(Sum(Case When NHSNumber is null then 1 else 1 end) as numeric)
		) * 100
		)
from APC.Encounter
Where 
(
AdmissionDate >='20100401 00:00:00'
and AdmissionDate <'20110301 00:00:00'
)


union all
Select 
	 RunDate = GETDATE()
	,IndicatorCode = 'A-2'
	,Period = CAST(Datepart(Year,AdmissionDate) as varchar(4)) 
	+ '-'
	+ Right('0' + cast(datepart(month,AdmissionDate) as varchar(2)),2)
	,Value = Sum(Case When Postcode is null then 1 else 0 end)
	,pc = 100 - 
		(
		(
		cast(Sum(Case When Postcode is null then 1 else 0 end) as numeric)
		/ cast(Sum(Case When Postcode is null then 1 else 1 end) as numeric)
		) * 100
		)
from APC.Encounter
Where 
(
AdmissionDate >='20100401 00:00:00'
and AdmissionDate <'20110301 00:00:00'
)
Group By
	CAST(Datepart(Year,AdmissionDate) as varchar(4)) 
	+ '-'
	+ Right('0' + cast(datepart(month,AdmissionDate) as varchar(2)),2)


Union All
Select 
	 RunDate = GETDATE()
	,IndicatorCode = 'A-2'
	,Period = 'YTD'
	,Value = Sum(Case When Postcode is null then 1 else 0 end)
	,pc = 100 - 
		(
		(
		cast(Sum(Case When Postcode is null then 1 else 0 end) as numeric)
		/ cast(Sum(Case When Postcode is null then 1 else 1 end) as numeric)
		) * 100
		)
from APC.Encounter
Where 
(
AdmissionDate >='20100401 00:00:00'
and AdmissionDate <'20110301 00:00:00'
)



Select top 100 * from APC.Encounter

