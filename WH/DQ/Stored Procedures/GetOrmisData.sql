﻿CREATE Procedure [DQ].[GetOrmisData] as

If Exists (Select * from INFORMATION_SCHEMA.TABLES where table_name = 'OrmisBase' and TABLE_SCHEMA = 'DQ')
BEGIN
DROP TABLE DQ.OrmisBase
END;



SELECT 
UniqueSessionID = F_SessionSumm.SS_SEQU,
FOPERAT.OP_SEQU AS [Ormis Operation Sequence Reference], 
FOPERAT.OP_EPISODE_NUM AS [Waiting List Reference], 
OP_Cancelled as [Operation Cancelled Y or N],
FTHEAT.TH_CODE AS [Operation Theatre Code], 
FTHEAT.TH_DESC AS [Operation Theatre Description], 
FOPERAT.OP_OPERATION_DATE AS [Operation date], 
dateName(dw,FOPERAT.OP_OPERATION_DATE) AS [Operation Day],
[SS_START_DATE_TIME] AS [Session Date], 
FOPERAT.OP_SESSION_NO AS [Session Number], 

Case OP_Session_NO 
	When 1 Then 'AM'
	When 2 Then 'PM'
	When 3 Then 'EVENING'
	When 0 Then 'EMERGENCY'
Else
	cast(OP_SESSION_NO as varchar)
End AS [Session Type], 

FOPERAT.OP_OPERAT_TYPE AS [Admission Method], 
[SS_START_DATE_TIME] AS [Session Expected Start Time], 
[SS_FINISH_DATE_TIME] AS [Session Expected Finish Date], 
[SS_FINISH_DATE_TIME] AS [Session Expected Finish Time], 
FSURGN_7.SU_CODE AS [Session Consultant Code], 
FSURGN_7.SU_FNAME AS [Session Consultant First Name], 
FSURGN_7.SU_LNAME AS [Session Consultant Surname], 
FS1SPEC_1.S1_CODE AS [Session Specialty Code], 
FS1SPEC_1.S1_DESC AS [Session Specialty Description], 
--NEW_SPEC_DIVISION_LK_2010_1.Division AS [Session Division], 
FS1SPEC.S1_CODE AS [Operating Specialty Code], 
FS1SPEC.S1_DESC AS [Operating Specialty Description], 
--NEW_SPEC_DIVISION_LK_2010.Division AS [Operating Division], 
FSURGN.SU_CODE AS [Patient  Consultant Code], 
FSURGN.SU_FNAME AS [Patient  Consultant First Name], 
FSURGN.SU_LNAME AS [Patient  Consultant Last Name], 
FSURGN_1.SU_CODE AS [Operating Surgeon 1 Code], 
FSURGN_1.SU_LNAME AS [Operating Surgeon 1 Name], 
FSURGN_2.SU_CODE AS [Operating Surgeon 2 Code], 
FSURGN_2.SU_LNAME AS [Operating Surgeon 2 Name], 
FSURGN_3.SU_CODE AS [Operating Surgeon 3 Code], 
FSURGN_3.SU_LNAME AS [Operating Surgeon 3 Name], 
FSURGN_4.SU_CODE AS [Operation Anaesthetist 1 Code], 
FSURGN_4.SU_LNAME AS [Operation Anaesthetist 1 Name], 
FSURGN_8.SU_CODE AS [Operation Anaesthetist 2 Code], 
FSURGN_8.SU_LNAME AS [Operation Anaesthetist 2 Name], 
FSURGN_6.SU_CODE AS [Operation Anaesthetist 3 Code], 
FSURGN_6.SU_LNAME AS [Operation Anaesthetist 3 Name], 
FPATS.PA_ANAES_TYPE AS [Anaesthetic Type], 
F_Anaesthetic.ANA_DESCRIPTION AS [Anaesthetic Description], 
FOPERAT.OP_DESCRIPTION AS [Operation Description Detail], 
FOPERAT.OP_MRN AS RM2, 
FOPERAT.OP_FNAME AS [Patient First Name], 
FOPERAT.OP_LNAME AS [Patient Last Name], 
FOPERAT.OP_GENDER AS [Patient Gender M or F], 
FOPERAT.OP_DOB AS [Patient DOB], 
FOPERAT.OP_AGE_YRS AS [Patient Age Years], 
FOPERAT.OP_AGE_MONTH AS [Patient Age Months], 
[OP_SENT_FOR_DATE_TIME] AS [Porter Sent for Time], 
[OP_PORTER_LEFT_DATE_TIME] AS [Porter Left for Patient Time], 
[OP_IN_SUITE_DATE_TIME] AS InSuite, 
[OP_IN_ANAES_DATE_TIME] AS [In Anaesthetic], 
[OP_ANAES_IND_DATE_TIME] AS [Anaesthetic Start Time], 
--[OP_ANAES_IND_DATE_TIME]))*60)*24 AS [Anaesthetic Start time mins], 
[OP_ANAES_IND_DATE_TIME],
[OP_ANAES_READY_DATE_TIME] AS [Anaesthetic Ready Time], 
[OP_START_DATE_TIME] AS [In OR], 
[OI_START] AS [Procedure Start], 
[OI_FINISH] AS [Procedure Finish], 
[OP_FINISH_DATE_TIME] AS [Out OR], 
[OP_IN_REC_DATE_TIME] AS [In Recovery], 
--TimeValue([OP_IN_REC_DATE_TIME]))*60)*24 AS [In Recovery mins], 
[OP_IN_REC_DATE_TIME],
[OP_READY_DEPART_DATE_TIME] AS [Ready For Discharge], 
[OP_DISCH_DATE_TIME] AS Discharged, 
FWARD.WA_CODE AS [From Ward], 
FWARD_1.WA_CODE AS [To Ward], 
F_Text_1.TXT_TEXT AS Other_OP_Comments, 
F_Text.TXT_TEXT AS Post_OP_Comments, 
Case When [OP_START_DATE_TIME] is NUll then 
	'N'
Else
	'Y'
End As [Include OP y or n], 
--[In Recovery mins]-[Anaesthetic Start time mins] AS [Operation length], 
FOITEM.OI_PRIMARY_CD, 
F_Delay.DE_DESCRIPTION AS Delay_Description

INTO DQ.OrmisBase

FROM ORMIS.DBO.FOPERAT 
LEFT JOIN ORMIS.DBO.FOITEM ON FOPERAT.OP_SEQU = FOITEM.OI_OP_SEQU 
LEFT JOIN ORMIS.DBO.FPATS ON FOPERAT.OP_PA_SEQU = FPATS.PA_SEQU 
LEFT JOIN ORMIS.DBO.FTHEAT ON FOPERAT.OP_TH_SEQU = FTHEAT.TH_SEQU 
LEFT JOIN ORMIS.DBO.FS1SPEC ON FOPERAT.OP_S1_SEQU = FS1SPEC.S1_SEQU 
LEFT JOIN ORMIS.DBO.FSURGN ON FOPERAT.OP_CONS_SEQU = FSURGN.SU_SEQU 
LEFT JOIN ORMIS.DBO.FWARD ON FOPERAT.OP_WA_FR_SEQU = FWARD.WA_SEQU 
LEFT JOIN ORMIS.DBO.FWARD AS FWARD_1 ON FOPERAT.OP_WA_TO_SEQU = FWARD_1.WA_SEQU 
LEFT JOIN ORMIS.DBO.F_Waiting_Details ON FOPERAT.OP_WD_SEQU = F_Waiting_Details.WD_SEQU 
LEFT JOIN ORMIS.DBO.F_SessionSumm ON FOPERAT.OP_SS_SEQU = F_SessionSumm.SS_SEQU 
LEFT JOIN ORMIS.DBO.FSURGN AS FSURGN_7 ON F_SessionSumm.SS_CONSULTANT = FSURGN_7.SU_SEQU 
LEFT JOIN ORMIS.DBO.FS1SPEC AS FS1SPEC_1 ON F_SessionSumm.SS_S1_SEQU = FS1SPEC_1.S1_SEQU 
LEFT JOIN ORMIS.DBO.F_Anaesthetic ON FPATS.PA_ANA_SEQU = F_Anaesthetic.ANA_SEQU 
LEFT JOIN ORMIS.DBO.FSURGN AS FSURGN_1 ON FOITEM.OI_SURG1_SEQU = FSURGN_1.SU_SEQU 
LEFT JOIN ORMIS.DBO.FSURGN AS FSURGN_2 ON FOITEM.OI_SURG2_SEQU = FSURGN_2.SU_SEQU 
LEFT JOIN ORMIS.DBO.FSURGN AS FSURGN_3 ON FOITEM.OI_SURG3_SEQU = FSURGN_3.SU_SEQU 
LEFT JOIN ORMIS.DBO.FSURGN AS FSURGN_4 ON FOITEM.OI_ANAES1_SEQU = FSURGN_4.SU_SEQU 
LEFT JOIN ORMIS.DBO.FSURGN AS FSURGN_8 ON FOITEM.OI_ANAES2_SEQU = FSURGN_8.SU_SEQU 
LEFT JOIN ORMIS.DBO.FSURGN AS FSURGN_6 ON FOITEM.OI_ANAES3_SEQU = FSURGN_6.SU_SEQU 
LEFT JOIN ORMIS.DBO.F_Delay ON FOPERAT.OP_DE_SEQU = F_Delay.DE_SEQU 
LEFT JOIN ORMIS.DBO.F_Text ON FOPERAT.OP_POST_OP_COMMENTS_SEQU = F_Text.TXT_SEQU 
LEFT JOIN ORMIS.DBO.F_Text AS F_Text_1 ON FOPERAT.OP_OTHER_COMMENTS_SEQU = F_Text_1.TXT_SEQU 
LEFT JOIN ORMIS.DBO.F_Delay_Items ON FOPERAT.OP_SEQU = F_Delay_Items.DIT_OP_SEQU
WHERE FOPERAT.OP_OPERATION_DATE>='2010-01-04'
and FOITEM.OI_PRIMARY_CD = 1
and [OP_START_DATE_TIME] is not NUll
