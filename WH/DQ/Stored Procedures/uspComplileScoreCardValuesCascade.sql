﻿


CREATE Procedure [DQ].[uspComplileScoreCardValuesCascade] 
@Date1 datetime, @Date2 Datetime
as 


--Declare 
--@Date1 As Datetime, @Date2 As Datetime


--Set @Date1 = '2011-04-01'
--Set @Date2 = '2011-10-01'





/******************************************************************************
GET E-01 NHS Number Coverage
******************************************************************************/

Insert Into DQ.Indicators
SELECT
  RunDate = GETDATE()
 ,Data.SpecialtyCode
 ,Data.IndicatorCode
 ,FinYear = Data.fYear
 ,Data.Period
 ,Value = SUM(Data.Value)
 ,Baseline = COUNT(*)
 ,pc = CAST((CAST(SUM([Value]) AS DECIMAL(10,2)) / CAST(COUNT(*) AS DECIMAL(10,2))) * 100 AS DECIMAL(10,2))
 ,IndicatorStatus = NUll
FROM (
      SELECT
        EncounterRecno = Archive.CDS_ID
       ,SpecialtyCode = -9999
       ,IndicatorCode = 'E-01'
       ,Value = CASE 
                  WHEN Archive.NHS_NO_STATUS in ('01','SUCCS') THEN 0
                  ELSE 1
                END
       ,fYear = CASE 
	              WHEN Month(Archive.ACTIVITY_DATE) IN (1,2,3) THEN CAST(YEAR(Archive.ACTIVITY_DATE)-1 AS VARCHAR(4)) 
	                                                                + '-' + RIGHT(CAST(YEAR(Archive.ACTIVITY_DATE) AS VARCHAR(4)),2)
	              ELSE CAST(YEAR(Archive.ACTIVITY_DATE) AS VARCHAR(4)) + '-' 
	                   + RIGHT(CAST(YEAR(Archive.ACTIVITY_DATE)+1 AS VARCHAR(4)),2)
	            END
       ,Period = RIGHT('0' + CAST(CASE MONTH(Archive.ACTIVITY_DATE)
	                                WHEN 1 THEN 10
	                                WHEN 2 THEN 11
	                                WHEN 3 THEN 12
	                                ELSE MONTH(Archive.ACTIVITY_DATE) -3
                                    END AS VARCHAR(2)),2)	      
      FROM (
            SELECT DataLookup.CDS_ID
                  ,LAST_SUS_SENT_DATE = MAX(DataLookup.SUS_SENT_DATE)
            FROM CDSArchive.CDS.AEArchiveMain DataLookup
            WHERE CAST(DataLookup.ACTIVITY_DATE AS DATE) BETWEEN @Date1 AND @Date2
            GROUP BY DataLookup.CDS_ID
           ) DataToExtract
      INNER JOIN CDSArchive.CDS.AEArchiveMain Archive
        ON (DataToExtract.CDS_ID = Archive.CDS_ID
            AND DataToExtract.LAST_SUS_SENT_DATE = Archive.SUS_SENT_DATE)
     ) Data
GROUP BY Data.IndicatorCode
        ,Data.SpecialtyCode
        ,Data.fYear
        ,Data.Period;
--Select
--RunDate = GETDATE(),
--SpecialtyCode,
--IndicatorCode,
--FinYear = fYear,
--Period,
--Value = SUM([Value]),
--Baseline = COUNT(*),
--pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) *100 as decimal(10,2)),
--IndicatorStatus = NUll
--From 
--(
--Select
--EncounterRecno = CDS_IDENTIFIER,
--SpecialtyCode = -9999,
--IndicatorCode = 'E-01',
--Value = Case When NHS_NO_STATUS in ('01','SUCCS') Then
--	0
--Else
--	1
--End,
--fYear = 
--	Case WHen Month(Activity_Date) in (1,2,3) Then
--		Cast(Year(Activity_Date)-1 as varchar(4)) + '-' + Right(Cast(Year(Activity_Date) as varchar(4)),2)
--	Else
--		Cast(Year(Activity_Date) as varchar(4)) + '-' + Right(Cast(Year(Activity_Date)+1 as varchar(4)),2)
--	ENd,
--Period = RIGHT('0' + CAST(
--Case MONTH(Activity_Date)
--	WHen 1 then 10
--	When 2 then 11
--	When 3 then 12
--	Else Month(Activity_Date) -3
--End 
--as varchar(2)),2)
--From infosql.information_reporting.dbo.aecds APE
--Where 
--Cast(Activity_Date as DATE) >= @Date1 and Cast(Activity_Date as DATE) < @Date2
--) As Source
--GROUP BY
--IndicatorCode,
--SpecialtyCode,
--fYear,
--Period



/******************************************************************************
GET E-02 Postcodes
******************************************************************************/


Insert Into DQ.Indicators
SELECT
  RunDate = GETDATE()
 ,Data.SpecialtyCode
 ,Data.IndicatorCode
 ,FinYear = Data.fYear
 ,Data.Period
 ,Value = SUM(Data.Value)
 ,Baseline = COUNT(*)
 ,pc = CAST((CAST(SUM([Value]) AS DECIMAL(10,2)) / CAST(COUNT(*) AS DECIMAL(10,2))) * 100 AS DECIMAL(10,2))
 ,IndicatorStatus = NUll
FROM (
      SELECT
        EncounterRecno = Archive.CDS_ID
       ,SpecialtyCode = -9999
       ,IndicatorCode = 'E-02'
       ,Value = CASE 
                  WHEN Archive.POSTCODE = 'ZZ99 3AZ' THEN 1
                  ELSE 0
                END
       ,fYear = CASE 
	              WHEN Month(Archive.ACTIVITY_DATE) IN (1,2,3) THEN CAST(YEAR(Archive.ACTIVITY_DATE)-1 AS VARCHAR(4)) 
	                                                                + '-' + RIGHT(CAST(YEAR(Archive.ACTIVITY_DATE) AS VARCHAR(4)),2)
	              ELSE CAST(YEAR(Archive.ACTIVITY_DATE) AS VARCHAR(4)) + '-' 
	                   + RIGHT(CAST(YEAR(Archive.ACTIVITY_DATE)+1 AS VARCHAR(4)),2)
	            END
       ,Period = RIGHT('0' + CAST(CASE MONTH(Archive.ACTIVITY_DATE)
	                                WHEN 1 THEN 10
	                                WHEN 2 THEN 11
	                                WHEN 3 THEN 12
	                                ELSE MONTH(Archive.ACTIVITY_DATE) -3
                                    END AS VARCHAR(2)),2)	      
      FROM (
            SELECT DataLookup.CDS_ID
                  ,LAST_SUS_SENT_DATE = MAX(DataLookup.SUS_SENT_DATE)
            FROM CDSArchive.CDS.AEArchiveMain DataLookup
            WHERE CAST(DataLookup.ACTIVITY_DATE AS DATE) BETWEEN @Date1 AND @Date2
            GROUP BY DataLookup.CDS_ID
           ) DataToExtract
      INNER JOIN CDSArchive.CDS.AEArchiveMain Archive
        ON (DataToExtract.CDS_ID = Archive.CDS_ID
            AND DataToExtract.LAST_SUS_SENT_DATE = Archive.SUS_SENT_DATE)
     ) Data
GROUP BY Data.IndicatorCode
        ,Data.SpecialtyCode
        ,Data.fYear
        ,Data.Period;
--Select
--RunDate = GETDATE(),
--SpecialtyCode,
--IndicatorCode,
--FinYear = fYear,
--Period,
--Value = SUM([Value]),
--Baseline = COUNT(*),
--pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) *100 as decimal(10,2)),
--IndicatorStatus = NUll
--From 
--(
--Select
--EncounterRecno = CDS_IDENTIFIER,
--SpecialtyCode = -9999,
--IndicatorCode = 'E-02',
--Value = Case When POSTCODE = 'ZZ99 3AZ' Then
--	1
--Else
--	0
--End,
--fYear = 
--	Case WHen Month(Activity_Date) in (1,2,3) Then
--		Cast(Year(Activity_Date)-1 as varchar(4)) + '-' + Right(Cast(Year(Activity_Date) as varchar(4)),2)
--	Else
--		Cast(Year(Activity_Date) as varchar(4)) + '-' + Right(Cast(Year(Activity_Date)+1 as varchar(4)),2)
--	ENd,
--Period = RIGHT('0' + CAST(
--Case MONTH(Activity_Date)
--	WHen 1 then 10
--	When 2 then 11
--	When 3 then 12
--	Else Month(Activity_Date) -3
--End 
--as varchar(2)),2)
--From infosql.information_reporting.dbo.aecds APE
--Where 
--Cast(Activity_Date as DATE) >= @Date1 and Cast(Activity_Date as DATE) < @Date2
--) As Source
--GROUP BY
--IndicatorCode,
--SpecialtyCode,
--fYear,
--Period


/******************************************************************************
GET E-06 Gender
******************************************************************************/
Insert Into DQ.Indicators
SELECT
  RunDate = GETDATE()
 ,s.SpecialtyCode
 ,s.IndicatorCode
 ,FinYear = s.fYear
 ,s.Period
 ,Value = SUM(s.Value)
 ,Baseline = COUNT(*)
 ,pc = CAST((CAST(SUM(s.Value) AS DECIMAL(10,2)) / CAST(COUNT(*) AS DECIMAL(10,2))) *100 AS DECIMAL(10,2))
 ,IndicatorStatus = NUll
FROM (
      SELECT DISTINCT
        EncounterRecno = CDS.CDS_ID
       ,SpecialtyCode = -9999
       ,IndicatorCode = 'E-06'
       ,Value = CASE 
                  WHEN SEX = 0 THEN 1
                  ELSE 0
                END 
       ,fYear = CASE 
                  WHEN MONTH(CDS.ACTIVITY_DATE) IN (1,2,3) THEN CAST(YEAR(CDS.ACTIVITY_DATE)-1 AS VARCHAR(4)) 
                                                                + '-' + RIGHT(CAST(YEAR(CDS.ACTIVITY_DATE) AS VARCHAR(4)),2)
	              ELSE CAST(YEAR(CDS.ACTIVITY_DATE) AS VARCHAR(4)) + '-' + RIGHT(CAST(YEAR(CDS.ACTIVITY_DATE)+1 AS VARCHAR(4)),2)
	            END   
       ,Period = RIGHT('0' + CAST(CASE MONTH(CDS.ACTIVITY_DATE)
	                                WHEN 1 THEN 10
	                                WHEN 2 THEN 11
	                                WHEN 3 THEN 12
	                                ELSE MONTH(CDS.ACTIVITY_DATE) -3
                                  END AS VARCHAR(2)),2)	             
      FROM (
            SELECT 
              a.ATTENDNO 
             ,MAX(a.SUS_SENT_DATE) MAX_SENT_DATE
            FROM CDSArchive.CDS.AEArchiveMain a
            WHERE CAST(a.ACTIVITY_DATE AS DATE) >= @Date1 
                  AND CAST(a.ACTIVITY_DATE AS DATE) < @Date2
            GROUP BY ATTENDNO 
           ) MAX_CDS
      INNER JOIN CDSArchive.CDS.AEArchiveMain CDS ON (MAX_CDS.ATTENDNO = CDS.ATTENDNO 
                                                      AND MAX_CDS.MAX_SENT_DATE = CDS.SUS_SENT_DATE) 
     ) s
GROUP BY s.IndicatorCode
        ,s.SpecialtyCode
        ,s.fYear
        ,s.Period






