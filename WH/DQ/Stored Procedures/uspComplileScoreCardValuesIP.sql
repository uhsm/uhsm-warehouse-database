﻿
CREATE Procedure [DQ].[uspComplileScoreCardValuesIP] 
@Date1 datetime, @Date2 Datetime
as 

/*
Declare 
@Date1 As Datetime, @Date2 As Datetime


Set @Date1 = '2011-04-01'
Set @Date2 = '2011-05-01'
*/




/******************************************************************************
GET A-01 NHS No Coverage
******************************************************************************/

Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) *100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode,
IndicatorCode = 'A-01',
Value = Case When Pat.NNNTS_CODE in ('01','SUCCS') Then
	0
Else
	1
End,
fYear = 
	Case WHen Month(EpisodeEndDate) in (1,2,3) Then
		Cast(Year(EpisodeEndDate)-1 as varchar(4)) + '-' + Right(Cast(Year(EpisodeEndDate) as varchar(4)),2)
	Else
		Cast(Year(EpisodeEndDate) as varchar(4)) + '-' + Right(Cast(Year(EpisodeEndDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(EpisodeEndDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(EpisodeEndDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.BaseAPC APC
left outer join Lorenzo.dbo.Patient pat
	on APC.SourcePatientNo = pat.PATNT_REFNO
Where 
EpisodeEndDate >= @Date1 and EpisodeEndDate <@Date2
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period



/******************************************************************************
GET A-02 Postcodes
******************************************************************************/


Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode,
IndicatorCode = 'A-02',
Value = Case When Postcode is null Then
	1
Else
	0
End,
fYear = 
	Case WHen Month(EpisodeEndDate) in (1,2,3) Then
		Cast(Year(EpisodeEndDate)-1 as varchar(4)) + '-' + Right(Cast(Year(EpisodeEndDate) as varchar(4)),2)
	Else
		Cast(Year(EpisodeEndDate) as varchar(4)) + '-' + Right(Cast(Year(EpisodeEndDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(EpisodeEndDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(EpisodeEndDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.BaseAPC APC
Where 
EpisodeEndDate >= @Date1 and EpisodeEndDate <@Date2
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period


/******************************************************************************
GET A-03 EthnicGroup
******************************************************************************/


Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode,
IndicatorCode = 'A-03',
Value = Case When EthnicOriginCode In(148,2004556) Then
	1
Else
	0
End,
fYear = 
	Case WHen Month(EpisodeEndDate) in (1,2,3) Then
		Cast(Year(EpisodeEndDate)-1 as varchar(4)) + '-' + Right(Cast(Year(EpisodeEndDate) as varchar(4)),2)
	Else
		Cast(Year(EpisodeEndDate) as varchar(4)) + '-' + Right(Cast(Year(EpisodeEndDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(EpisodeEndDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(EpisodeEndDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.BaseAPC APC
Where 
EpisodeEndDate >= @Date1 and EpisodeEndDate <@Date2
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period


/******************************************************************************
GET A-04 MissingGPs
******************************************************************************/

Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode,
IndicatorCode = 'A-04',
Value = Case When RegisteredGpCode Is Null Then
	1
Else
	0
End,
fYear = 
	Case WHen Month(EpisodeEndDate) in (1,2,3) Then
		Cast(Year(EpisodeEndDate)-1 as varchar(4)) + '-' + Right(Cast(Year(EpisodeEndDate) as varchar(4)),2)
	Else
		Cast(Year(EpisodeEndDate) as varchar(4)) + '-' + Right(Cast(Year(EpisodeEndDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(EpisodeEndDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(EpisodeEndDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.BaseAPC APC
Where 
EpisodeEndDate >= @Date1 and EpisodeEndDate <@Date2
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period



/******************************************************************************
GET A-07 RTT Status 99
******************************************************************************/


Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode,
IndicatorCode = 'A-05',
Value = Case When  RTTPeriodStatusCode in (3007110, 3007095) Then
	1
Else
	0
End,
fYear = 
	Case WHen Month(EpisodeEndDate) in (1,2,3) Then
		Cast(Year(EpisodeEndDate)-1 as varchar(4)) + '-' + Right(Cast(Year(EpisodeEndDate) as varchar(4)),2)
	Else
		Cast(Year(EpisodeEndDate) as varchar(4)) + '-' + Right(Cast(Year(EpisodeEndDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(EpisodeEndDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(EpisodeEndDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.BaseAPC APC
Where 
EpisodeEndDate >= @Date1 and EpisodeEndDate <@Date2
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period



/******************************************************************************
GET A-06 MissingPrimaryDiagnosis
******************************************************************************/

Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode,
IndicatorCode = 'A-06',
Value = Case When (PrimaryDiagnosisCode is null) Then
	1
Else
	0
End,
fYear = 
	Case WHen Month(DischargeDate) in (1,2,3) Then
		Cast(Year(DischargeDate)-1 as varchar(4)) + '-' + Right(Cast(Year(DischargeDate) as varchar(4)),2)
	Else
		Cast(Year(DischargeDate) as varchar(4)) + '-' + Right(Cast(Year(DischargeDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(DischargeDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(DischargeDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.BaseAPC APC
Where 
DischargeDate >= @Date1 and DischargeDate <@Date2
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period



/******************************************************************************
GET A-07 AdmissionsNotInputWithin2hrs
******************************************************************************/


Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode,
IndicatorCode = 'A-07',
Value = Case When DateDiff(minute,AdmissionTime,PASCreated) Between 0 and 120 Then
	0
Else
	1
End,
fYear = 
	Case WHen Month(AdmissionDate) in (1,2,3) Then
		Cast(Year(AdmissionDate)-1 as varchar(4)) + '-' + Right(Cast(Year(AdmissionDate) as varchar(4)),2)
	Else
		Cast(Year(AdmissionDate) as varchar(4)) + '-' + Right(Cast(Year(AdmissionDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(AdmissionDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(AdmissionDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.BaseAPC APC with (nolock)
Where 
AdmissionDate >= @Date1 and AdmissionDate <@Date2
And AdmissionTime = EpisodeStartTime
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period



/******************************************************************************
GET A-08 DayCaseDischarges NOt in 24 hours
******************************************************************************/

Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode,
IndicatorCode = 'A-08',
Value = Case When DateDiff(minute,sstay.END_DTTM,sstay.MODIF_DTTM) Between 0 and 1440 Then
	0
Else
	1
End,
fYear = 
	Case WHen Month(DischargeDate) in (1,2,3) Then
		Cast(Year(DischargeDate)-1 as varchar(4)) + '-' + Right(Cast(Year(DischargeDate) as varchar(4)),2)
	Else
		Cast(Year(DischargeDate) as varchar(4)) + '-' + Right(Cast(Year(DischargeDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(DischargeDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(DischargeDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.OlapAPC APC with (nolock)
inner Join Lorenzo.dbo.ServicePointStay SStay with (nolock)
on apc.SourceSpellNo = sstay.PRVSP_REFNO
and apc.DischargeTime = sstay.END_DTTM
Where 
DischargeDate >= @Date1 and DischargeDate <@Date2
and 	APC.AdmissionMethodTypeCode = 'EL'
and	APC.InpatientStayCode = 'D'
and	APC.EpisodeStartTime = APC.AdmissionTime

) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period


/******************************************************************************
GET A-09 All Discharges Not input within 2 hours
******************************************************************************/

Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
APC.AdmissionTime,
APC.EpisodeStartTime,
APC.DischargeTime,
APC.InpatientStayCode,
SStay.END_DTTM,
sstay.MODIF_DTTM,
SpecialtyCode,
IndicatorCode = 'A-09',
Value = Case When DateDiff(minute,sstay.END_DTTM,sstay.MODIF_DTTM) Between 0 and 1440 Then
	0
Else
	1
End,
fYear = 
	Case WHen Month(DischargeDate) in (1,2,3) Then
		Cast(Year(DischargeDate)-1 as varchar(4)) + '-' + Right(Cast(Year(DischargeDate) as varchar(4)),2)
	Else
		Cast(Year(DischargeDate) as varchar(4)) + '-' + Right(Cast(Year(DischargeDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(DischargeDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(DischargeDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.OlapAPC APC with (nolock)
inner Join Lorenzo.dbo.ServicePointStay SStay with (nolock)
on apc.SourceSpellNo = sstay.PRVSP_REFNO
and apc.DischargeTime = sstay.END_DTTM
and SStay.ARCHV_FLAG = 'N'
Where 
DischargeDate >= @Date1 and DischargeDate <@Date2
and	APC.EpisodeStartTime = APC.AdmissionTime

) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period

/******************************************************************************
GET A-11 Elective IP Finished Episodes Not Outcomed
******************************************************************************/

Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
APC.AdmissionTime,
APC.EpisodeStartTime,
APC.DischargeTime,
APC.InpatientStayCode,
SpecialtyCode,
IndicatorCode = 'A-11',
Episode.WLIST_REFNO,
Value = Case When Episode.CEocm_refno = 70 Then
	1
Else
	0
End,
fYear = 
	Case WHen Month(EpisodeEndDate) in (1,2,3) Then
		Cast(Year(EpisodeEndDate)-1 as varchar(4)) + '-' + Right(Cast(Year(EpisodeEndDate) as varchar(4)),2)
	Else
		Cast(Year(EpisodeEndDate) as varchar(4)) + '-' + Right(Cast(Year(EpisodeEndDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(EpisodeEndDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(EpisodeEndDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.OlapAPC APC with (nolock)
inner Join Lorenzo.dbo.ProfessionalCareEpisode Episode with (nolock)
on apc.SourceUniqueID = Episode.PRCAE_REFNO
Where 
APC.EpisodeEndDate >= @Date1 and APC.EpisodeEndDate <@Date2
--APC.EpisodeEndDate >= '20110401 00:00:00' and APC.EpisodeEndDate <'20110501 00:00:00'
And APC.AdmissionMethodCode in (8810, 8811)
And APC.WaitingListSourceUniqueID is not null
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period


/******************************************************************************
GET A-12 Elective IP Admission Offers, (TCIs),  Not Outcomed
******************************************************************************/

Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select

SpecialtyCode = WaitingList.SPECT_REFNO,
IndicatorCode = 'A-12',
Offer.WLIST_REFNO,
Value = Case When Offer.OFOCM_REFNO = 761 Then
	1
Else
	0
End,
fYear = 
	Case WHen Month(Offer.TCI_DTTM) in (1,2,3) Then
		Cast(Year(Offer.TCI_DTTM)-1 as varchar(4)) + '-' + Right(Cast(Year(Offer.TCI_DTTM) as varchar(4)),2)
	Else
		Cast(Year(Offer.TCI_DTTM) as varchar(4)) + '-' + Right(Cast(Year(Offer.TCI_DTTM)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(Offer.TCI_DTTM)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(Offer.TCI_DTTM) -3
End 
as varchar(2)),2)
From Lorenzo.dbo.AdmissionOffer Offer with (nolock)
left outer join Lorenzo.dbo.WaitingList WaitingList with (nolock)
	on Offer.WLIST_REFNO = WaitingList.WLIST_REFNO 
Where 
Offer.TCI_DTTM >= @Date1 and Offer.TCI_DTTM <@Date2
--Offer.TCI_DTTM >='20100401 00:00:00' and Offer.TCI_DTTM <'20110501 00:00:00'
and Offer.ARCHV_FLAG = 'N'
and WaitingList.ADMET_REFNO in (8810, 8811)
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period






/******************************************************************************
GET A-13 - No Religion
******************************************************************************/


Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode,
IndicatorCode = 'A-13',
Value = Case When ReligionCode IS NULL Or ReligionCode In(1164)-- this is 'not specified'
 Then
	1
Else
	0
End,
fYear = 
	Case WHen Month(EpisodeEndDate) in (1,2,3) Then
		Cast(Year(EpisodeEndDate)-1 as varchar(4)) + '-' + Right(Cast(Year(EpisodeEndDate) as varchar(4)),2)
	Else
		Cast(Year(EpisodeEndDate) as varchar(4)) + '-' + Right(Cast(Year(EpisodeEndDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(EpisodeEndDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(EpisodeEndDate) -3
End 
as varchar(2)),2)
From APC.Encounter--use this table as per discussion with TD.
Where 
EpisodeEndDate >= @Date1 
and EpisodeEndDate < @Date2
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period


/******************************************************************************
GET A-15 - Gender
******************************************************************************/


Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
ISNULL(SpecialtyCode, '-1'),
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select DISTINCT 
cds.CDS_ID,
SpecialtyCode,--using the Specialty code as specified in the APC.Encounter table obtained by linking the episode source unique id in the CDS archive table back to source unique id in the encounter table
IndicatorCode = 'A-15',
Value = Case When SEX = 0 -- this is 'Not Known'
 Then
	1
Else
	0
End,
fYear = 
	Case WHen Month(EPIEND) in (1,2,3) Then
		Cast(Year(EPIEND)-1 as varchar(4)) + '-' + Right(Cast(Year(EPIEND) as varchar(4)),2)
	Else
		Cast(Year(EPIEND) as varchar(4)) + '-' + Right(Cast(Year(EPIEND)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(EPIEND)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(EPIEND) -3
End 
as varchar(2)),2)
From CDS.APCArchiveMain cds
INNER JOIN (Select CDS_ID, MAX(SUS_SENT_DATE) as MAX_SENT from CDS.APCArchiveMain
Group by CDS_ID) max_cds on cds.CDS_ID = max_cds.CDS_ID -- need to use max here to bring back details of the last time the record was sent to SUS
Left outer join APC.Encounter  enc on cds.EpisodeSourceUniqueID = enc.SourceUniqueID-- join back to encounter to get the orginal specialty code 
Where 
EPIEND >= @Date1 
and EPIEND < @Date2
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period



--Select top 100 * from WHOLAP.dbo.BaseAPC
--select * from Lorenzo.dbo.ReferenceValue where RFVDM_CODE = 'RTTST'






