﻿CREATE Procedure [DQ].[uspComplileScoreCardValuesMaternity] 
@Date1 datetime, @Date2 Datetime
as 

/*
Declare 
@Date1 As Datetime, @Date2 As Datetime


Set @Date1 = '2011-07-01'
Set @Date2 = '2011-08-01'
*/




/******************************************************************************
GET M-01 Self Referring Babies
******************************************************************************/

Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) *100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode,
IndicatorCode = 'M-01',
Value = 
	Case when SelfReferringBaby = 1 and SourceOfReferralCode = 1509 Then 
	1
	Else
	0
	End,
fYear = 
	Case WHen Month(ReferralDate) in (1,2,3) Then
		Cast(Year(ReferralDate)-1 as varchar(4)) + '-' + Right(Cast(Year(ReferralDate) as varchar(4)),2)
	Else
		Cast(Year(ReferralDate) as varchar(4)) + '-' + Right(Cast(Year(ReferralDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(ReferralDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(ReferralDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.BaseRF RF
Where 
ReferralDate >= @Date1 and ReferralDate <@Date2
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period



