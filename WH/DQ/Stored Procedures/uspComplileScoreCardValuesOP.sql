﻿
CREATE Procedure [DQ].[uspComplileScoreCardValuesOP] 
@Date1 datetime, @Date2 Datetime
as 

/*
Declare 
@Date1 As Datetime, @Date2 As Datetime


Set @Date1 = '2009-04-01'
Set @Date2 = '2011-04-01'
*/




/******************************************************************************
GET O-01 NHS No Coverage
******************************************************************************/

Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) *100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode,
IndicatorCode = 'O-01',
Value = Case When Pat.NNNTS_CODE in ('01','SUCCS') Then
	0
Else
	1
End,
fYear = 
	Case WHen Month(AppointmentDate) in (1,2,3) Then
		Cast(Year(AppointmentDate)-1 as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate) as varchar(4)),2)
	Else
		Cast(Year(AppointmentDate) as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(AppointmentDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(AppointmentDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.BaseOP OP
left outer join Lorenzo.dbo.Patient pat
	on OP.SourcePatientNo = pat.PATNT_REFNO
Where 
AppointmentDate >= @Date1 and AppointmentDate <@Date2
And AppointmentCancelDate is null
And ExcludedClinic = 0
And IsWardAttender = 0 And SpecialtyCode Is Not Null
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period



/******************************************************************************
GET O-02 Postcodes
******************************************************************************/


Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode,
IndicatorCode = 'O-02',
Value = Case When Postcode is null Then
	1
Else
	0
End,
fYear = 
	Case WHen Month(AppointmentDate) in (1,2,3) Then
		Cast(Year(AppointmentDate)-1 as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate) as varchar(4)),2)
	Else
		Cast(Year(AppointmentDate) as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(AppointmentDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(AppointmentDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.BaseOP OP
Where 
AppointmentDate >= @Date1 and AppointmentDate <@Date2
And AppointmentCancelDate is null
And ExcludedClinic = 0
And IsWardAttender = 0 And SpecialtyCode Is Not Null
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period


/******************************************************************************
GET O-03 EthnicGroup
******************************************************************************/

Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode,
IndicatorCode = 'O-03',
Value = Case When EthnicOriginCode In(148,2004556) Then
	1
Else
	0
End,
fYear = 
	Case WHen Month(AppointmentDate) in (1,2,3) Then
		Cast(Year(AppointmentDate)-1 as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate) as varchar(4)),2)
	Else
		Cast(Year(AppointmentDate) as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(AppointmentDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(AppointmentDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.BaseOP OP
Where 
AppointmentDate >= @Date1 and AppointmentDate <@Date2
And AppointmentCancelDate is null
And ExcludedClinic = 0
And IsWardAttender = 0 And SpecialtyCode Is Not Null
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period


/******************************************************************************
GET O-04 EthnicGroup
******************************************************************************/

Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode,
IndicatorCode = 'O-04',
Value = Case When RegisteredGpCode Is Null Then
	1
Else
	0
End,
fYear = 
	Case WHen Month(AppointmentDate) in (1,2,3) Then
		Cast(Year(AppointmentDate)-1 as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate) as varchar(4)),2)
	Else
		Cast(Year(AppointmentDate) as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(AppointmentDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(AppointmentDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.BaseOP OP
Where 
AppointmentDate >= @Date1 and AppointmentDate <@Date2
And AppointmentCancelDate is null
And ExcludedClinic = 0
And IsWardAttender = 0 And SpecialtyCode Is Not Null
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period

/******************************************************************************
GET O-05 Non Attended Outpatient Appts
******************************************************************************/

Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode,
IndicatorCode = 'O-05',
Value = Case When AppointmentStatusCode = 45 Then
	1
Else
	0
End,
fYear = 
	Case WHen Month(AppointmentDate) in (1,2,3) Then
		Cast(Year(AppointmentDate)-1 as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate) as varchar(4)),2)
	Else
		Cast(Year(AppointmentDate) as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(AppointmentDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(AppointmentDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.BaseOP OP
Where 
AppointmentDate >= @Date1 and AppointmentDate <@Date2
And AppointmentCancelDate is null
And ExcludedClinic = 0
And IsWardAttender = 0 And SpecialtyCode Is Not Null
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period



/******************************************************************************
GET O-06 Non Outcome Outpatient Appts
******************************************************************************/

Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode,
IndicatorCode = 'O-06',
Value = Case When DisposalCode = 1210 Then
	1
Else
	0
End,
fYear = 
	Case WHen Month(AppointmentDate) in (1,2,3) Then
		Cast(Year(AppointmentDate)-1 as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate) as varchar(4)),2)
	Else
		Cast(Year(AppointmentDate) as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(AppointmentDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(AppointmentDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.BaseOP OP
Where 
AppointmentDate >= @Date1 and AppointmentDate <@Date2
And AppointmentCancelDate is null
And ExcludedClinic = 0
And IsWardAttender = 0 And SpecialtyCode Is Not Null
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period



/******************************************************************************
GET O-07 RTT Status 99
******************************************************************************/


Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode,
IndicatorCode = 'O-07',
Value = Case When  RTTPeriodStatusCode in (3007110, 3007095) Then
	1
Else
	0
End,
fYear = 
	Case WHen Month(AppointmentDate) in (1,2,3) Then
		Cast(Year(AppointmentDate)-1 as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate) as varchar(4)),2)
	Else
		Cast(Year(AppointmentDate) as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(AppointmentDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(AppointmentDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.BaseOP OP
Where 
AppointmentDate >= @Date1 and AppointmentDate <@Date2
And AppointmentCancelDate is null
And ExcludedClinic = 0
And IsWardAttender = 0 And SpecialtyCode Is Not Null
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period


/******************************************************************************
GET O-08 / O-09 Referral Source NSP
******************************************************************************/

Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode,
IndicatorCode = Case When IsWardAttender = 0 Then
	'O-08'
Else
	'O-09'
End,
Value = Case When (SourceOfReferralCode = 1226 or SourceOfReferralCode is null) Then
	1
Else
	0
End,
fYear = 
	Case WHen Month(AppointmentDate) in (1,2,3) Then
		Cast(Year(AppointmentDate)-1 as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate) as varchar(4)),2)
	Else
		Cast(Year(AppointmentDate) as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(AppointmentDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(AppointmentDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.BaseOP OP
Where 
AppointmentDate >= @Date1 and AppointmentDate <@Date2
And AppointmentCancelDate is null
And ExcludedClinic = 0
And SpecialtyCode Is Not Null
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period

/******************************************************************************
GET O-10 Ward Attenders Unknown ReferralDate
******************************************************************************/

Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode,
IndicatorCode = 'O-10',
Value = Case When (ReferralDate is null) Then
	1
Else
	0
End,
fYear = 
	Case WHen Month(AppointmentDate) in (1,2,3) Then
		Cast(Year(AppointmentDate)-1 as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate) as varchar(4)),2)
	Else
		Cast(Year(AppointmentDate) as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(AppointmentDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(AppointmentDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.BaseOP OP
Where 
AppointmentDate >= @Date1 and AppointmentDate <@Date2
And AppointmentCancelDate is null
And ExcludedClinic = 0
And IsWardAttender = 1
And SpecialtyCode Is Not Null
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period



/******************************************************************************
GET O-11 Religion
******************************************************************************/

Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode,
IndicatorCode = 'O-11',
Value = Case When ReligionCode IS NULL Or ReligionCode In(1164)-- this is 'not specified'
 Then
	1
Else
	0
End,
fYear = 
	Case WHen Month(AppointmentDate) in (1,2,3) Then
		Cast(Year(AppointmentDate)-1 as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate) as varchar(4)),2)
	Else
		Cast(Year(AppointmentDate) as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(AppointmentDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(AppointmentDate) -3
End 
as varchar(2)),2)
From OP.Encounter OP
Where 
AppointmentDate >= @Date1 and AppointmentDate <@Date2
And AppointmentCancelDate is null
And ExcludedClinic = 0
And IsWardAttender = 0 And SpecialtyCode Is Not Null
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period




/******************************************************************************
GET O-13 Gender
******************************************************************************/

Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) * 100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select distinct 
EncounterRecno,
ISNULL(SpecialtyCode,-1) as SpecialtyCode,
IndicatorCode = 'O-13',
Value = Case When SexCode IS NULL Or SexCode In('3006530','3678')-- this is ' Not Specified' and 'Please enter a value'
 Then
	1
Else
	0
End,
fYear = 
	Case WHen Month(AppointmentDate) in (1,2,3) Then
		Cast(Year(AppointmentDate)-1 as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate) as varchar(4)),2)
	Else
		Cast(Year(AppointmentDate) as varchar(4)) + '-' + Right(Cast(Year(AppointmentDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(AppointmentDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(AppointmentDate) -3
End 
as varchar(2)),2)
From OP.Encounter OP
Where 
AppointmentDate >= @Date1 
and AppointmentDate <@Date2
And AppointmentCancelDate is null
And ExcludedClinic = 0
And IsWardAttender = 0 
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period


--Select top 100 AttendanceOutcomeCode, * from WHOLAP.dbo.BaseOP
--select * from Lorenzo.dbo.ReferenceValue where RFVDM_CODE = 'RTTST'




