﻿CREATE Procedure [DQ].[uspComplileScoreCardValuesOrmis] 
@Date1 datetime, @Date2 Datetime
as 

/*
Declare 
@Date1 As Datetime, @Date2 As Datetime

Set @Date1 = '2011-01-01'
Set @Date2 = '2011-05-01'
*/


/******************************************************************************
GET T-01 Patients Still In Theatre
******************************************************************************/

Insert Into DQ.Indicators


Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) *100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
151 as EncounterRecno,
SpecialtyCode = IsNull(Spec.SpecCode,-1),
IndicatorCode = 'T-01',
Value = 
Case When [Out OR] IS null Then 
	1
Else
	0
End,
fYear = 
	Case WHen Month([Operation Date]) in (1,2,3) Then
		Cast(Year([Operation Date])-1 as varchar(4)) + '-' + Right(Cast(Year([Operation Date]) as varchar(4)),2)
	Else
		Cast(Year([Operation Date]) as varchar(4)) + '-' + Right(Cast(Year([Operation Date])+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH([Operation Date])
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month([Operation Date]) -3
End 
as varchar(2)),2)
From DQ.OrmisBase ODQ
Left Outer Join
	(
	Select 
	SubSpecialtyCode, 
	MIN(specialtyCode) as SpecCode  
	from WHOLAP.dbo.SpecialtyDivisionBase 
	group by SubSpecialtyCode
	) Spec
On ODQ.[Operating Specialty Code] = Spec.SubSpecialtyCode

Where 
[Operation Date] >= @Date1 and [Operation Date] <@Date2
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period


/******************************************************************************
GET T-02 Patients Still In Recovery
******************************************************************************/

Insert Into DQ.Indicators



Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) *100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
151 as EncounterRecno,
SpecialtyCode = IsNull(Spec.SpecCode,-1),
IndicatorCode = 'T-02',
Value = 
Case When 
Discharged is null Then 
	1
Else
	0
End,
fYear = 
	Case WHen Month([Operation Date]) in (1,2,3) Then
		Cast(Year([Operation Date])-1 as varchar(4)) + '-' + Right(Cast(Year([Operation Date]) as varchar(4)),2)
	Else
		Cast(Year([Operation Date]) as varchar(4)) + '-' + Right(Cast(Year([Operation Date])+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH([Operation Date])
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month([Operation Date]) -3
End 
as varchar(2)),2)
From DQ.OrmisBase ODQ
Left Outer Join
	(
	Select 
	SubSpecialtyCode, 
	MIN(specialtyCode) as SpecCode  
	from WHOLAP.dbo.SpecialtyDivisionBase 
	group by SubSpecialtyCode
	) Spec
On ODQ.[Operating Specialty Code] = Spec.SubSpecialtyCode

Where 
[Operation Date] >= @Date1 and [Operation Date] <@Date2
--and [Operation Cancelled Y or N] = 0
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period


/******************************************************************************
GET T-03 No Anaesthetic Type Recorded
******************************************************************************/

Insert Into DQ.Indicators



Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) *100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
151 as EncounterRecno,
SpecialtyCode = IsNull(Spec.SpecCode,-1),
IndicatorCode = 'T-03',
Value = 
Case When 
[Anaesthetic Type] is null Then 
	1
Else
	0
End,
fYear = 
	Case WHen Month([Operation Date]) in (1,2,3) Then
		Cast(Year([Operation Date])-1 as varchar(4)) + '-' + Right(Cast(Year([Operation Date]) as varchar(4)),2)
	Else
		Cast(Year([Operation Date]) as varchar(4)) + '-' + Right(Cast(Year([Operation Date])+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH([Operation Date])
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month([Operation Date]) -3
End 
as varchar(2)),2)
From DQ.OrmisBase ODQ
Left Outer Join
	(
	Select 
	SubSpecialtyCode, 
	MIN(specialtyCode) as SpecCode  
	from WHOLAP.dbo.SpecialtyDivisionBase 
	group by SubSpecialtyCode
	) Spec
On ODQ.[Operating Specialty Code] = Spec.SubSpecialtyCode

Where 
[Operation Date] >= @Date1 and [Operation Date] <@Date2
--and [Operation Cancelled Y or N] = 0
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period


/******************************************************************************
GET T-04 Missing Wards
******************************************************************************/

Insert Into DQ.Indicators



Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) *100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
151 as EncounterRecno,
SpecialtyCode = IsNull(Spec.SpecCode,-1),
IndicatorCode = 'T-04',
Value = 
Case When 
[To Ward] is null Then 
	1
Else
	0
End,
fYear = 
	Case WHen Month([Operation Date]) in (1,2,3) Then
		Cast(Year([Operation Date])-1 as varchar(4)) + '-' + Right(Cast(Year([Operation Date]) as varchar(4)),2)
	Else
		Cast(Year([Operation Date]) as varchar(4)) + '-' + Right(Cast(Year([Operation Date])+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH([Operation Date])
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month([Operation Date]) -3
End 
as varchar(2)),2)
From DQ.OrmisBase ODQ
Left Outer Join
	(
	Select 
	SubSpecialtyCode, 
	MIN(specialtyCode) as SpecCode  
	from WHOLAP.dbo.SpecialtyDivisionBase 
	group by SubSpecialtyCode
	) Spec
On ODQ.[Operating Specialty Code] = Spec.SubSpecialtyCode

Where 
[Operation Date] >= @Date1 and [Operation Date] <@Date2
--and [Operation Cancelled Y or N] = 0
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period


/******************************************************************************
GET T-05 Locum Surgeon
******************************************************************************/

Insert Into DQ.Indicators



Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) *100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
151 as EncounterRecno,
SpecialtyCode = IsNull(Spec.SpecCode,-1),
IndicatorCode = 'T-05',
Value = 
Case When 
LEFT([Operating Surgeon 1 Code],3) = 'LOC' Then 
	1
Else
	0
End,
fYear = 
	Case WHen Month([Operation Date]) in (1,2,3) Then
		Cast(Year([Operation Date])-1 as varchar(4)) + '-' + Right(Cast(Year([Operation Date]) as varchar(4)),2)
	Else
		Cast(Year([Operation Date]) as varchar(4)) + '-' + Right(Cast(Year([Operation Date])+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH([Operation Date])
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month([Operation Date]) -3
End 
as varchar(2)),2)
From DQ.OrmisBase ODQ
Left Outer Join
	(
	Select 
	SubSpecialtyCode, 
	MIN(specialtyCode) as SpecCode  
	from WHOLAP.dbo.SpecialtyDivisionBase 
	group by SubSpecialtyCode
	) Spec
On ODQ.[Operating Specialty Code] = Spec.SubSpecialtyCode

Where 
[Operation Date] >= @Date1 and [Operation Date] <@Date2
--and [Operation Cancelled Y or N] = 0
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period


/******************************************************************************
GET T-06 Invalid Sessions
******************************************************************************/

Insert Into DQ.Indicators



Select
RunDate = GETDATE(),
SpecialtyCode,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) *100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
151 as EncounterRecno,
SpecialtyCode = IsNull(Spec.SpecCode,-1),
IndicatorCode = 'T-06',
Value = 
case when [Admission Method] = 'Emergency' Then 
	0
Else
	case when [In OR] Between [Session Expected Start Time] and [Session Expected Finish Time] Then
		0
	Else
		Case When [Session Date] IS NULL AND [Operation Theatre Code] IS NULL AND [Session Number] = '0' Then
			0
		Else
			1
		End
	End
End,
fYear = 
	Case WHen Month([Operation Date]) in (1,2,3) Then
		Cast(Year([Operation Date])-1 as varchar(4)) + '-' + Right(Cast(Year([Operation Date]) as varchar(4)),2)
	Else
		Cast(Year([Operation Date]) as varchar(4)) + '-' + Right(Cast(Year([Operation Date])+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH([Operation Date])
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month([Operation Date]) -3
End 
as varchar(2)),2)
From DQ.OrmisBase ODQ
Left Outer Join
	(
	Select 
	SubSpecialtyCode, 
	MIN(specialtyCode) as SpecCode  
	from WHOLAP.dbo.SpecialtyDivisionBase 
	group by SubSpecialtyCode
	) Spec
On ODQ.[Operating Specialty Code] = Spec.SubSpecialtyCode

Where 
[Operation Date] >= @Date1 and [Operation Date] <@Date2
--and [Operation Cancelled Y or N] = 0
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period
