﻿Create Procedure [DQ].[uspComplileScoreCardValuesRIS] 
@Date1 datetime, @Date2 Datetime
as 


--Declare 
--@Date1 As Datetime, @Date2 As Datetime


--Set @Date1 = '2009-04-01'
--Set @Date2 = '2011-04-01'





/******************************************************************************
GET R-01 NHS No Coverage
******************************************************************************/

Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode = -9999,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) *100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode = -9999,
IndicatorCode = 'R-01',
Value = Case When [Order].NHSNumberVerifiedFlag = 'Y' Then
	0
Else
	1
End,
fYear = 
	Case WHen Month(PerformanceDate) in (1,2,3) Then
		Cast(Year(PerformanceDate)-1 as varchar(4)) + '-' + Right(Cast(Year(PerformanceDate) as varchar(4)),2)
	Else
		Cast(Year(PerformanceDate) as varchar(4)) + '-' + Right(Cast(Year(PerformanceDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(PerformanceDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(PerformanceDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.BaseOrder [Order]
Where 
PerformanceDate >= @Date1 and PerformanceDate <@Date2
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period





/******************************************************************************
GET R-02 Postcodes
******************************************************************************/


Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode = -9999,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) *100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode = -9999,
IndicatorCode = 'R-02',
Value = Case When [Order].Postcode IS null  or [Order].Postcode = ''  Then
	1
Else
	0
End,
fYear = 
	Case WHen Month(PerformanceDate) in (1,2,3) Then
		Cast(Year(PerformanceDate)-1 as varchar(4)) + '-' + Right(Cast(Year(PerformanceDate) as varchar(4)),2)
	Else
		Cast(Year(PerformanceDate) as varchar(4)) + '-' + Right(Cast(Year(PerformanceDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(PerformanceDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(PerformanceDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.BaseOrder [Order]
Where 
PerformanceDate >= @Date1 and PerformanceDate <@Date2
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period


/******************************************************************************
GET R-03 MissingGP
******************************************************************************/

Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode = -9999,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) *100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode = -9999,
IndicatorCode = 'R-03',
Value = Case When [Order].RegisteredGpPracticeCode IS null  or [Order].RegisteredGpPracticeCode = ''  Then
	1
Else
	0
End,
fYear = 
	Case WHen Month(PerformanceDate) in (1,2,3) Then
		Cast(Year(PerformanceDate)-1 as varchar(4)) + '-' + Right(Cast(Year(PerformanceDate) as varchar(4)),2)
	Else
		Cast(Year(PerformanceDate) as varchar(4)) + '-' + Right(Cast(Year(PerformanceDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(PerformanceDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(PerformanceDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.BaseOrder [Order]
Where 
PerformanceDate >= @Date1 and PerformanceDate <@Date2
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period


/******************************************************************************
GET R-04 MissingGender
******************************************************************************/

Insert Into DQ.Indicators

Select
RunDate = GETDATE(),
SpecialtyCode = -9999,
IndicatorCode,
FinYear = fYear,
Period,
Value = SUM([Value]),
Baseline = COUNT(*),
pc = Cast((cast(SUM([Value]) as decimal(10,2)) / cast(COUNT(*) as decimal(10,2))) *100 as decimal(10,2)),
IndicatorStatus = NUll
From 
(
Select
EncounterRecno,
SpecialtyCode = -9999,
IndicatorCode = 'R-04',
Value = Case When [Order].PatientGenderCode in ('M','F') Then
	0
Else
	1
End,
fYear = 
	Case WHen Month(PerformanceDate) in (1,2,3) Then
		Cast(Year(PerformanceDate)-1 as varchar(4)) + '-' + Right(Cast(Year(PerformanceDate) as varchar(4)),2)
	Else
		Cast(Year(PerformanceDate) as varchar(4)) + '-' + Right(Cast(Year(PerformanceDate)+1 as varchar(4)),2)
	ENd,
Period = RIGHT('0' + CAST(
Case MONTH(PerformanceDate)
	WHen 1 then 10
	When 2 then 11
	When 3 then 12
	Else Month(PerformanceDate) -3
End 
as varchar(2)),2)
From WHOLAP.dbo.BaseOrder [Order]
Where 
PerformanceDate >= @Date1 and PerformanceDate <@Date2
) As Source
GROUP BY
IndicatorCode,
SpecialtyCode,
fYear,
Period
