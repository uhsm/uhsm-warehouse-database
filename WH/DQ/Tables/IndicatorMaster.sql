﻿CREATE TABLE [DQ].[IndicatorMaster] (
    [IndicatorCode]        VARCHAR (5)   NOT NULL,
    [IndicatorArea]        VARCHAR (55)  NULL,
    [IndicatorSubArea]     VARCHAR (55)  NULL,
    [IndicatorName]        VARCHAR (55)  NULL,
    [IndicatorDescription] VARCHAR (500) NULL,
    [IndicatorDefninition] VARCHAR (500) NULL,
    [ImprovementTarget]    BIT           NOT NULL,
    [StartDate]            DATE          NULL,
    [EndDate]              DATE          NULL,
    [RiskRating]           VARCHAR (50)  NULL,
    [IndicatorEnabled]     BIT           NULL,
    CONSTRAINT [pk_DQ_Indicators] PRIMARY KEY CLUSTERED ([IndicatorCode] ASC)
);

