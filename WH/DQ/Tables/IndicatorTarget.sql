﻿CREATE TABLE [DQ].[IndicatorTarget] (
    [IndicatorCode] VARCHAR (6)     NOT NULL,
    [FinYear]       VARCHAR (7)     NOT NULL,
    [Period]        VARCHAR (2)     NOT NULL,
    [Target]        DECIMAL (10, 2) NULL,
    [Green]         DECIMAL (10, 2) NULL,
    [AmberHigh]     DECIMAL (10, 2) NULL,
    [AmberLow]      DECIMAL (10, 2) NULL,
    [Red]           DECIMAL (10, 2) NULL,
    CONSTRAINT [pk_IndicatorTaget] PRIMARY KEY CLUSTERED ([IndicatorCode] ASC, [FinYear] ASC, [Period] ASC)
);

