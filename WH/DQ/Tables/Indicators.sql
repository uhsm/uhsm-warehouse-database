﻿CREATE TABLE [DQ].[Indicators] (
    [RunDate]         DATETIME        NOT NULL,
    [SpecialtyCode]   INT             NOT NULL,
    [IndicatorCode]   VARCHAR (5)     NOT NULL,
    [FinYear]         VARCHAR (8)     NOT NULL,
    [Period]          VARCHAR (6)     NOT NULL,
    [Value]           INT             NULL,
    [Baseline]        INT             NULL,
    [pc]              DECIMAL (10, 2) NULL,
    [IndicatorStatus] VARCHAR (20)    NULL,
    CONSTRAINT [pk_Indicator] PRIMARY KEY CLUSTERED ([RunDate] ASC, [IndicatorCode] ASC, [SpecialtyCode] ASC, [FinYear] ASC, [Period] ASC)
);

