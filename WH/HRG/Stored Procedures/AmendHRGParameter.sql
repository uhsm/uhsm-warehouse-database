﻿CREATE procedure [HRG].[AmendHRGParameter] as

/**
HRG net change updates are based on Parameters set in WH
The HRG update jobs are run as part of the PAS update job (which runs every 15 minutes)
but the SSIS package will only run in the following conditions:
- APC: HRG46APCDATE < EXTRACTAPCDATE 
- OP:  HRG46OPDATE < EXTRACTOPDATE
- CC:  HRG46CCDATE < MAX(APC.CriticalCarePeriod.CCPEndDate)
- AE:  HRG46AEDATE < MAX(AE.Encounter.Updated (or created))

Use code below to update to one or all parameters. When the WH - Load PAS is next run
(either manually or scheduled), the given bulk uploads will run.
**/
--
--APC
select 'EXTRACTAPCDATE' as APC, convert(varchar, DateValue) from dbo.Parameter where Parameter = 'EXTRACTAPCDATE'
--union
--select 'HRGAPCDATE', DateValue from dbo.Parameter where Parameter = 'HRGAPCDATE'
union
select 'HRG46APCDATE', convert(varchar, DateValue) from dbo.Parameter where Parameter = 'HRG46APCDATE'
union
select 'HRG46APCSTARTDATE', convert(varchar,DateValue) from dbo.Parameter where Parameter = 'HRG46APCSTARTDATE'
--union
--select 'HRGAPCSTARTDATE', DateValue from dbo.Parameter where Parameter = 'HRGAPCSTARTDATE'
union
select 'APC Grouper will run', case when 
	(select DateValue from dbo.Parameter where Parameter = 'EXTRACTAPCDATE')
	>
	(select DateValue from dbo.Parameter where Parameter = 'HRG46APCDATE')
	then 'Yes'
	else 'No'
	end
	
	
--OP
select'EXTRACTOPDATE' as OP, convert(varchar, max(DateValue)) from dbo.Parameter where Parameter in (
       'EXTRACTOPREFERENCEDATE'
      ,'EXTRACTOPDATE')
--union
--select 'HRGOPDATE', DateValue from dbo.Parameter where Parameter = 'HRGOPDATE'
union
select 'HRG46OPDATE', convert(varchar, DateValue) from dbo.Parameter where Parameter = 'HRG46OPDATE'
union
select 'OP Grouper will run', case when 
	(select max(DateValue) from dbo.Parameter where Parameter in (
       'EXTRACTOPREFERENCEDATE'
      ,'EXTRACTOPDATE'))
	>
	(select DateValue from dbo.Parameter where Parameter = 'HRG46OPDATE')
	then 'Yes'
	else 'No'
	end
	
--CC - always runs bulk job if any new records
select 'CCPEndDate' as CC, convert(varchar, min(CCPEndDate)) from APC.CriticalCarePeriod where CCPEndDate > 
(select DateValue from dbo.Parameter where Parameter = 'HRG46CCDATE')
--union
--select 'HRGCCDATE', DateValue from dbo.Parameter where Parameter = 'HRGCCDATE'
union
select 'HRG46CCDATE', convert(varchar, DateValue) from dbo.Parameter where Parameter = 'HRG46CCDATE'
--select * from APC.CriticalCarePeriod order by CCPEndDate desc
union
select 'CC Grouper will run', case when 
	(select min(CCPEndDate) from APC.CriticalCarePeriod where CCPEndDate > 
		(select DateValue from dbo.Parameter where Parameter = 'HRG46CCDATE'))
	>
	(select DateValue from dbo.Parameter where Parameter = 'HRG46CCDATE')
	then 'Yes'
	else 'No'
	end

--AE
select 'Updated' as AE ,convert(varchar, min(coalesce(Updated, Created))) from AE.Encounter where coalesce(Updated, Created) > 
(select DateValue from dbo.Parameter where Parameter = 'HRG46AEDATE')
--union
--select 'HRGAEDATE', DateValue from dbo.Parameter where Parameter = 'HRGAEDATE'
union
select 'HRG46AEDATE', convert(varchar, DateValue) from dbo.Parameter where Parameter = 'HRG46AEDATE'
--select top 100 coalesce(Updated, Created) from AE.Encounter order by coalesce(Updated, Created) desc
union
select 'AE Grouper will run', case when 
	(select min(coalesce(Updated, Created)) from AE.Encounter where coalesce(Updated, Created) > 
		(select DateValue from dbo.Parameter where Parameter = 'HRG46AEDATE'))
	>
	(select DateValue from dbo.Parameter where Parameter = 'HRG46AEDATE')
	then 'Yes'
	else 'No'
	end

--To update parameters for bulk upload
-------------------------------------------------------------------------------------------------
--set dateformat ymd
--update dbo.Parameter set DateValue = '2013-03-31 00:00:00.000' where Parameter = 'HRGAPCDATE'
--update dbo.Parameter set DateValue = '2013-03-31 00:00:00.000' where Parameter = 'HRGAPCSTARTDATE'
--update dbo.Parameter set DateValue = '2013-03-31 00:00:00.000' where Parameter = 'HRGOPDATE'
--update dbo.Parameter set DateValue = '2012-03-31 00:00:00.000' where Parameter = 'HRGCCDATE'
--update dbo.Parameter set DateValue = '2012-03-31 00:00:00.000' where Parameter = 'HRGAEDATE'

--update dbo.Parameter set DateValue = '2013-03-31 00:00:00.000' where Parameter = 'HRG46APCDATE'
--update dbo.Parameter set DateValue = '2013-03-31 00:00:00.000' where Parameter = 'HRG46APCSTARTDATE'
--update dbo.Parameter set DateValue = '2013-06-06 00:00:00.000' where Parameter = 'HRG46OPDATE'
--update dbo.Parameter set DateValue = '2013-03-31 00:00:00.000' where Parameter = 'HRG46CCDATE'
--update dbo.Parameter set DateValue = '2013-03-31 00:00:00.000' where Parameter = 'HRG46AEDATE'

--Truncate existing HRG tables before full bulk load for the year
-------------------------------------------------------------------------------------------------
--truncate table APC.HRG45Quality
--truncate table APC.HRG45Spell
--truncate table APC.HRG45SpellFlags
--truncate table APC.HRG45Unbundled
--truncate table APC.HRG45Encounter


--truncate table OP.HRG45Encounter
--truncate table OP.HRG45Quality
--truncate table OP.HRG45Unbundled

--truncate table CC.HRG45Quality
--truncate table CC.HRG45Encounter

--truncate table AE.HRG45Quality
--truncate table AE.HRG45Encounter

-------------------------------------
--2013-14
--truncate table HRG.HRG46APCEncounter
--truncate table HRG.HRG46APCSpell
--truncate table HRG.HRG46APCSpellFlags
--truncate table HRG.HRG46APCUnbundled
--truncate table HRG.HRG46APCQuality

--truncate table HRG.HRG46OPEncounter
--truncate table HRG.HRG46OPUnbundled
--truncate table HRG.HRG46OPQuality

--truncate table HRG.HRG46AEEncounter
--truncate table HRG.HRG46AEQuality

--truncate table HRG.HRG46CCEncounter
--truncate table HRG.HRG46CCQuality
--update dbo.Parameter set TextValue = 'CCG' where Parameter = 'CDSCOMMISSIONERTYPE'
