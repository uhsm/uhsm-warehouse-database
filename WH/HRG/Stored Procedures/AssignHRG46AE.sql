﻿CREATE procedure [HRG].[AssignHRG46AE] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInsertedEncounter Int
declare @RowsInsertedQuality Int
declare @Stats varchar(255)
declare @Created datetime

select @StartTime = getdate()

--process AE Encounters
delete from HRG.HRG46AEEncounter
where
	not exists
	(
	select
		1
	from
		AE.Encounter Encounter
	where
		Encounter.EncounterRecno = HRG46AEEncounter.EncounterRecno
	)

delete from HRG.HRG46AEEncounter
where
	exists
	(
	select
		1
	from
		HRG.THRG46AEEncounter Encounter
	where
		Encounter.EncounterRecno = HRG46AEEncounter.EncounterRecno
	)


INSERT INTO HRG.HRG46AEEncounter
	(
	 EncounterRecno
	,HRGCode
	,Created
	)
select
	 Encounter.EncounterRecno
	,case when Encounter.HRGCode = '' then null else Encounter.HRGCode end
	,GETDATE()
from
	HRG.THRG46AEEncounter Encounter

select @RowsInsertedEncounter = @@rowcount


--process AE Quality
delete from HRG.HRG46AEQuality
where
	not exists
	(
	select
		1
	from
		AE.Encounter Encounter
	where
		Encounter.EncounterRecno = HRG46AEQuality.EncounterRecno
	)

delete from HRG.HRG46AEQuality
where
	exists
	(
	select
		1
	from
		HRG.THRG46AEEncounter Encounter
	where
		Encounter.EncounterRecno = HRG46AEQuality.EncounterRecno
	)


INSERT INTO HRG.HRG46AEQuality
	(
	 EncounterRecno
	,SequenceNo
	,QualityTypeCode
	,QualityCode
	,QualityMessage
	)
select
	 Encounter.EncounterRecno
	,Quality.SequenceNo
	,case when Quality.QualityTypeCode = '' then null else Quality.QualityTypeCode end
	,case when Quality.QualityCode = '' then null else Quality.QualityCode end
	,case when Quality.QualityMessage = '' then null else Quality.QualityMessage end
from
	HRG.THRG46AEQuality Quality

inner join HRG.THRG46AEEncounter Encounter
on	Quality.RowNo = Encounter.RowNo

select @RowsInsertedQuality = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted (encounter): ' + CONVERT(varchar(10), @RowsInsertedEncounter) 
	+ ', Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'HRG - WH AssignHRG46AE', @Stats, @StartTime

--print @Stats