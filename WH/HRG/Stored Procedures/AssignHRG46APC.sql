﻿
CREATE procedure [HRG].[AssignHRG46APC] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInsertedEncounter Int
declare @RowsInsertedSpell Int
declare @RowsInsertedSpellFlags Int
declare @RowsInsertedUnbundled Int
declare @RowsInsertedQuality Int
--declare @RowsSpecServ Int
declare @Stats varchar(255)
declare @Created datetime

select @StartTime = getdate()

--process APC Encounters
delete from HRG.HRG46APCEncounter
where
	not exists
	(
	select
		1
	from
		APC.Encounter Encounter
	where
		Encounter.EncounterRecno = HRG46APCEncounter.EncounterRecno
	)

delete from HRG.HRG46APCEncounter
where
	exists
	(
	select
		1
	from
		HRG.THRG46APCEncounter Encounter
	where
		Encounter.EncounterRecno = HRG46APCEncounter.EncounterRecno
	)


INSERT INTO HRG.HRG46APCEncounter
	(
	 EncounterRecno
	,ProviderSpellNo
	,SourceUniqueID
	,HRGCode
	,GroupingMethodFlag
	,DominantOperationCode
	,Created
	,RehabilitationDaysInput
	,CalculatedEpisodeDuration
	,SpellReportFlag
	)
select
	 Encounter.EncounterRecno
	,Encounter.ProviderSpellNo
	,Encounter.SourceUniqueID
	,case when Encounter.HRGCode = '' then null else Encounter.HRGCode end
	,case when Encounter.GroupingMethodFlag = '' then null else Encounter.GroupingMethodFlag end
	,case when Encounter.DominantOperationCode = '' then null else Encounter.DominantOperationCode end
	,GETDATE()
	,RehabilitationDaysInput
	,CalculatedEpisodeDuration
	,SpellReportFlag
from
	HRG.THRG46APCEncounter Encounter

select @RowsInsertedEncounter = @@rowcount

--process APC Spell
delete from HRG.HRG46APCSpell
where
	not exists
	(
	select
		1
	from
		APC.Encounter Encounter
	where
		Encounter.EncounterRecno = HRG46APCSpell.EncounterRecno
	)

--remove all records for the grouped spell
delete
from
	HRG.HRG46APCSpell
where
	exists
	(
	select
		1
	from
		HRG.THRG46APCEncounter HRGEncounter

	inner join APC.Encounter
	on	Encounter.EncounterRecno = HRGEncounter.EncounterRecno

	inner join APC.Encounter Episode
	on	Episode.ProviderSpellNo = Encounter.ProviderSpellNo

	where
		Episode.EncounterRecno = HRG46APCSpell.EncounterRecno
	)


INSERT INTO HRG.HRG46APCSpell
	(
	 EncounterRecno
	,ProviderSpellNo
	,HRGCode
	,DominantOperationCode
	,PrimaryDiagnosisCode
	,SecondaryDiagnosisCode
	,EpisodeCount
	,LOS
	,ReportingSpellLOS
	,Trimpoint
	,ExcessBeddays
	,CCDays
	)
select
	 Encounter.EncounterRecno
	,Spell.ProviderSpellNo
	,case when Spell.HRGCode = '' then null else Spell.HRGCode end
	,case when Spell.DominantOperationCode = '' then null else Spell.DominantOperationCode end
	,case when Spell.PrimaryDiagnosisCode = '' then null else Spell.PrimaryDiagnosisCode end
	,case when Spell.SecondaryDiagnosisCode = '' then null else Spell.SecondaryDiagnosisCode end
	,Spell.EpisodeCount
	,Spell.LOS
	,Spell.ReportingSpellLOS
	,Spell.Trimpoint
	,Spell.ExcessBeddays
	,Spell.CCDays
from
	HRG.THRG46APCSpell Spell

inner join HRG.THRG46APCEncounter Encounter
on	Spell.RowNo = Encounter.RowNo

select @RowsInsertedSpell = @@rowcount

--process APC Spell Flags
delete from HRG.HRG46APCSpellFlags
where
	not exists
	(
	select
		1
	from
		APC.Encounter Encounter
	where
		Encounter.EncounterRecno = HRG46APCSpellFlags.EncounterRecno
	)

delete from HRG.HRG46APCSpellFlags
where
	exists
	(
	select
		1
	from
		HRG.THRG46APCEncounter Encounter
	where
		Encounter.EncounterRecno = HRG46APCSpellFlags.EncounterRecno
	)


INSERT INTO HRG.HRG46APCSpellFlags
	(
	 EncounterRecno
	,SequenceNo
	,SpellFlagCode
	)
select
	 Encounter.EncounterRecno
	,SpellFlags.SequenceNo
	,SpellFlags.SpellFlagCode
from
	HRG.THRG46APCSpellFlags SpellFlags

inner join HRG.THRG46APCEncounter Encounter
on	SpellFlags.RowNo = Encounter.RowNo

select @RowsInsertedSpellFlags = @@rowcount

--process APC Unbundled
delete from HRG.HRG46APCUnbundled
where
	not exists
	(
	select
		1
	from
		APC.Encounter Encounter
	where
		Encounter.EncounterRecno = HRG46APCUnbundled.EncounterRecno
	)

delete from HRG.HRG46APCUnbundled
where
	exists
	(
	select
		1
	from
		HRG.THRG46APCEncounter Encounter
	where
		Encounter.EncounterRecno = HRG46APCUnbundled.EncounterRecno
	)


INSERT INTO HRG.HRG46APCUnbundled
	(
	 EncounterRecno
	,SequenceNo
	,HRGCode
	)
select
	 Encounter.EncounterRecno
	,Unbundled.SequenceNo
	,case when Unbundled.HRGCode = '' then null else Unbundled.HRGCode end
from
	HRG.THRG46APCUnbundled Unbundled

inner join HRG.THRG46APCEncounter Encounter
on	Unbundled.RowNo = Encounter.RowNo

select @RowsInsertedUnbundled = @@rowcount


--process APC Quality
delete from HRG.HRG46APCQuality
where
	not exists
	(
	select
		1
	from
		APC.Encounter Encounter
	where
		Encounter.EncounterRecno = HRG46APCQuality.EncounterRecno
	)

delete from HRG.HRG46APCQuality
where
	exists
	(
	select
		1
	from
		HRG.THRG46APCEncounter Encounter
	where
		Encounter.EncounterRecno = HRG46APCQuality.EncounterRecno
	)


INSERT INTO HRG.HRG46APCQuality
	(
	 EncounterRecno
	,SequenceNo
	,QualityTypeCode
	,QualityCode
	,QualityMessage
	)
select
	 Encounter.EncounterRecno
	,Quality.SequenceNo
	,case when Quality.QualityTypeCode = '' then null else Quality.QualityTypeCode end
	,case when Quality.QualityCode = '' then null else Quality.QualityCode end
	,case when Quality.QualityMessage = '' then null else Quality.QualityMessage end
from
	HRG.THRG46APCQuality Quality

inner join HRG.THRG46APCEncounter Encounter
on	Quality.RowNo = Encounter.RowNo

select @RowsInsertedQuality = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted (encounter): ' + CONVERT(varchar(10), @RowsInsertedEncounter) 
	+ '. Rows inserted (spell): ' + CONVERT(varchar(10), @RowsInsertedSpell) 
	+ ', Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'HRG - WH AssignHRG46APC', @Stats, @StartTime

--print @Stats
