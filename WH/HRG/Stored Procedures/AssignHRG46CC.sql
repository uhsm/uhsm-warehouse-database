﻿
CREATE procedure [HRG].[AssignHRG46CC] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInsertedEncounter Int
declare @RowsInsertedQuality Int
declare @Stats varchar(255)
declare @Created datetime

select @StartTime = getdate()

--process CC Encounters
delete from HRG.HRG46CCEncounter
where
	not exists
	(
	select
		1
	from
		APC.CriticalCarePeriod Encounter
	where
		Encounter.SourceCCPNo = HRG46CCEncounter.EncounterRecno
	)

delete from HRG.HRG46CCEncounter
where
	exists
	(
	select
		1
	from
		HRG.THRG46CCEncounter Encounter
	where
		Encounter.CriticalCareLocalIdentifier = HRG46CCEncounter.EncounterRecno
	)


INSERT INTO HRG.HRG46CCEncounter
	(
	 EncounterRecno
	,HRGCode
	,CriticalCareDays
	,WarningCode
	,Created
	,SourceSpellNo
	)
select
	 Encounter.CriticalCareLocalIdentifier
	,case when Encounter.HRGCode = '' then null else Encounter.HRGCode end
	,Encounter.CriticalCareDays
	,Encounter.WarningFlag
	,GETDATE()
	,SourceSpellNo
from
	HRG.THRG46CCEncounter Encounter
	
select @RowsInsertedEncounter = @@rowcount

--process CC Quality
delete from HRG.HRG46CCQuality
where
	not exists
	(
	select
		1
	from
		APC.CriticalCarePeriod Encounter
	where
		Encounter.SourceCCPNo = HRG46CCQuality.EncounterRecno
	)

delete from HRG.HRG46CCQuality
where
	exists
	(
	select
		1
	from
		HRG.THRG46CCEncounter Encounter
	where
		Encounter.CriticalCareLocalIdentifier = HRG46CCQuality.EncounterRecno
	)


INSERT INTO HRG.HRG46CCQuality
	(
	 EncounterRecno
	,SequenceNo
	,QualityTypeCode
	,QualityCode
	,QualityMessage
	)
select
	 Encounter.CriticalCareLocalIdentifier
	,Quality.SequenceNo
	,case when Quality.QualityTypeCode = '' then null else Quality.QualityTypeCode end
	,case when Quality.QualityCode = '' then null else Quality.QualityCode end
	,case when Quality.QualityMessage = '' then null else Quality.QualityMessage end
from
	HRG.THRG46CCQuality Quality

inner join HRG.THRG46CCEncounter Encounter
on	Quality.RowNo = Encounter.RowNo

select @RowsInsertedQuality = @@rowcount


--the cc days returned by the grouper does not take into account embeded, non-AICU days
--so fix them here

update
	HRG.HRG46CCEncounter
set
	CriticalCareDays = AICU.CriticalCareDays
from
	CC.HRG45Encounter

inner join
	(
	select
		 CriticalCarePeriod.SourceCCPNo

		,CriticalCareDays =
			SUM(
				case
				when CriticalCarePeriod.CCPStayCareLevel = 2
				then 1
				else 0
				end
			) +

			SUM(
				case
				when CriticalCarePeriod.CCPStayCareLevel = 3
				then 1
				else 0
				end
			)

	from
		APC.CriticalCarePeriod

	inner join PAS.ReferenceValue CriticalCareUnitFunctionCode
	on	CriticalCareUnitFunctionCode.ReferenceValueCode = CriticalCarePeriod.CCPUnitFunction
	and	CriticalCareUnitFunctionCode.MainCode not in ('04','16','17','18','19','92') --remove paediatric

	inner join PAS.ServicePoint
	on	ServicePoint.ServicePointCode = CriticalCarePeriod.CCPStayWard

	inner join dbo.EntityXref WardServiceMap
	on	WardServiceMap.EntityTypeCode = 'WARD'
	and	WardServiceMap.XrefEntityTypeCode = 'AICUSERVICE'
	and	WardServiceMap.EntityCode = ServicePoint.ServicePointLocalCode

	where
		CriticalCarePeriod.CCPStayCareLevel is not null

	group by
		 CriticalCarePeriod.SourceCCPNo

	) AICU
on	AICU.SourceCCPNo = HRG45Encounter.EncounterRecno

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted (encounter): ' + CONVERT(varchar(10), @RowsInsertedEncounter) 
	+ ', Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'HRG - WH AssignHRG46CC', @Stats, @StartTime

--print @Stats
