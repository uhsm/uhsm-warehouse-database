﻿
CREATE procedure [HRG].[AssignHRG46OP] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInsertedEncounter Int
declare @RowsInsertedUnbundled Int
declare @RowsInsertedQuality Int
declare @Stats varchar(255)
declare @Created datetime

select @StartTime = getdate()

--process OP Encounters
delete from HRG.HRG46OPEncounter
where
	not exists
	(
	select
		1
	from
		OP.Encounter Encounter
	where
		Encounter.EncounterRecno = HRG46OPEncounter.EncounterRecno
	)

delete from HRG.HRG46OPEncounter
where
	exists
	(
	select
		1
	from
		HRG.THRG46OPEncounter Encounter
	where
		Encounter.EncounterRecno = HRG46OPEncounter.EncounterRecno
	)


INSERT INTO HRG.HRG46OPEncounter
	(
	 [EncounterRecno]
	,[HRGCode]
	,[GroupingMethodFlag]
	,[DominantOperationCode]
	,Created
	)
select
	 Encounter.EncounterRecno
	,case when Encounter.HRGCode = '' then null else Encounter.HRGCode end
	,case when Encounter.GroupingMethodFlag = '' then null else Encounter.GroupingMethodFlag end
	,case when Encounter.DominantOperationCode = '' then null else Encounter.DominantOperationCode end
	,GETDATE()
from
	HRG.THRG46OPEncounter Encounter
	
select @RowsInsertedEncounter = @@rowcount

--process OP Unbundled
delete from HRG.HRG46OPUnbundled
where
	not exists
	(
	select
		1
	from
		OP.Encounter Encounter
	where
		Encounter.EncounterRecno = HRG46OPUnbundled.EncounterRecno
	)

delete from HRG.HRG46OPUnbundled
where
	exists
	(
	select
		1
	from
		HRG.THRG46OPEncounter Encounter
	where
		Encounter.EncounterRecno = HRG46OPUnbundled.EncounterRecno
	)


INSERT INTO HRG.HRG46OPUnbundled
	(
	 EncounterRecno
	,SequenceNo
	,HRGCode
	)
select
	 Encounter.EncounterRecno
	,Unbundled.SequenceNo
	,case when Unbundled.HRGCode = '' then null else Unbundled.HRGCode end
from
	HRG.THRG46OPUnbundled Unbundled

inner join HRG.THRG46OPEncounter Encounter
on	Unbundled.RowNo = Encounter.RowNo

select @RowsInsertedUnbundled = @@rowcount

--process OP Quality
delete from HRG.HRG46OPQuality
where
	not exists
	(
	select
		1
	from
		OP.Encounter Encounter
	where
		Encounter.EncounterRecno = HRG46OPQuality.EncounterRecno
	)

delete from HRG.HRG46OPQuality
where
	exists
	(
	select
		1
	from
		HRG.THRG46OPEncounter Encounter
	where
		Encounter.EncounterRecno = HRG46OPQuality.EncounterRecno
	)


INSERT INTO HRG.HRG46OPQuality
	(
	 EncounterRecno
	,SequenceNo
	,QualityTypeCode
	,QualityCode
	,QualityMessage
	)
select
	 Encounter.EncounterRecno
	,Quality.SequenceNo
	,case when Quality.QualityTypeCode = '' then null else Quality.QualityTypeCode end
	,case when Quality.QualityCode = '' then null else Quality.QualityCode end
	,case when Quality.QualityMessage = '' then null else Quality.QualityMessage end
from
	HRG.THRG46OPQuality Quality

inner join HRG.THRG46OPEncounter Encounter
on	Quality.RowNo = Encounter.RowNo

select @RowsInsertedQuality = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted (encounter): ' + CONVERT(varchar(10), @RowsInsertedEncounter) 
	+ ', Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'HRG - WH AssignHRG46OP', @Stats, @StartTime

--print @Stats
