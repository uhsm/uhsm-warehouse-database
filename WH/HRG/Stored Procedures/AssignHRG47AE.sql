﻿CREATE procedure [HRG].[AssignHRG47AE] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInsertedEncounter Int
declare @RowsInsertedQuality Int
declare @Stats varchar(255)
declare @Created datetime

select @StartTime = getdate()

----process AE Encounters
--delete from   HRG.HRG47AEEncounter
--where
--	not exists
--	(
--	select
--		1
--	from
--		AE.Encounter Encounter
--	where
--		Encounter.EncounterRecno = HRG47AEEncounter.EncounterRecno
--	)

--delete from HRG.HRG47AEEncounter
--where
--	exists
--	(
--	select
--		1
--	from
--		HRG.THRG47AEEncounter Encounter
--	where
--		Encounter.EncounterRecno = HRG47AEEncounter.EncounterRecno
--	)

--CM 03/10/2014 - changed to use truncate statement as the A&E data is extracted for the year to date and then all pushed through grouper.
Truncate table  HRG.HRG47AEEncounter


INSERT INTO HRG.HRG47AEEncounter
	(
	 EncounterRecno
	,HRGCode
	,Created
	)
select
	 Encounter.EncounterRecno
	,case when Encounter.HRGCode = '' then null else Encounter.HRGCode end
	,GETDATE()
from
	HRG.THRG47AEEncounter Encounter

select @RowsInsertedEncounter = @@rowcount


----process AE Quality
--delete from   HRG.HRG47AEQuality
--where
--	not exists
--	(
--	select
--		1
--	from
--		AE.Encounter Encounter
--	where
--		Encounter.EncounterRecno = HRG47AEQuality.EncounterRecno
--	)

--delete from HRG.HRG47AEQuality
--where
--	exists
--	(
--	select
--		1
--	from
--		HRG.THRG47AEEncounter Encounter
--	where
--		Encounter.EncounterRecno = HRG47AEQuality.EncounterRecno
--	)


--CM 03/10/2014 - changed to use truncate statement as the A&E data is extracted for the year to date and then all pushed through grouper.
Truncate table  HRG.HRG47AEQuality

INSERT INTO HRG.HRG47AEQuality
	(
	 EncounterRecno
	,SequenceNo
	,QualityTypeCode
	,QualityCode
	,QualityMessage
	)
select
	 Encounter.EncounterRecno
	,Quality.SequenceNo
	,case when Quality.QualityTypeCode = '' then null else Quality.QualityTypeCode end
	,case when Quality.QualityCode = '' then null else Quality.QualityCode end
	,case when Quality.QualityMessage = '' then null else Quality.QualityMessage end
from
	HRG.THRG47AEQuality Quality

inner join HRG.THRG47AEEncounter Encounter
on	Quality.RowNo = Encounter.RowNo

select @RowsInsertedQuality = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted (encounter): ' + CONVERT(varchar(10), @RowsInsertedEncounter) 
	+ ', Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'HRG - WH AssignHRG47AE', @Stats, @StartTime

--print @Stats