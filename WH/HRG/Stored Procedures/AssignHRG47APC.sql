﻿
CREATE procedure [HRG].[AssignHRG47APC] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInsertedEncounter Int
declare @RowsInsertedSpell Int
declare @RowsInsertedSpellFlags Int
declare @RowsInsertedUnbundled Int
declare @RowsInsertedQuality Int
--declare @RowsSpecServ Int
declare @Stats varchar(255)
declare @Created datetime

select @StartTime = getdate()

--process APC Encounters
delete from HRG.HRG47APCEncounter
where
	not exists
	(
	select
		1
	from
		APC.Encounter Encounter
	where
		Encounter.EncounterRecno = HRG47APCEncounter.EncounterRecno
	)

delete from HRG.HRG47APCEncounter
where
	exists
	(
	select
		1
	from
		HRG.THRG47APCEncounter Encounter
	where
		Encounter.EncounterRecno = HRG47APCEncounter.EncounterRecno
	)


INSERT INTO HRG.HRG47APCEncounter
	(
	 EncounterRecno
	,ProviderSpellNo
	,SourceUniqueID
	,HRGCode
	,GroupingMethodFlag
	,DominantOperationCode
	,Created
	,RehabilitationDaysInput
	,CalculatedEpisodeDuration
	,SpellReportFlag
	,UnbundledHRGs
	)
select
	 Encounter.EncounterRecno
	,Encounter.ProviderSpellNo
	,Encounter.SourceUniqueID
	,case when Encounter.HRGCode = '' then null else Encounter.HRGCode end
	,case when Encounter.GroupingMethodFlag = '' then null else Encounter.GroupingMethodFlag end
	,case when Encounter.DominantOperationCode = '' then null else Encounter.DominantOperationCode end
	,GETDATE()
	,RehabilitationDaysInput
	,CalculatedEpisodeDuration
	,SpellReportFlag
	,Encounter.UnbundledHRGs
from
	HRG.THRG47APCEncounter Encounter

select @RowsInsertedEncounter = @@rowcount

--process APC Spell
delete from HRG.HRG47APCSpell
where
	not exists
	(
	select
		1
	from
		APC.Encounter Encounter
	where
		Encounter.EncounterRecno = HRG47APCSpell.EncounterRecno
	)

--remove all records for the grouped spell
delete
from
	HRG.HRG47APCSpell
where
	exists
	(
	select
		1
	from
		HRG.THRG47APCEncounter HRGEncounter

	inner join APC.Encounter
	on	Encounter.EncounterRecno = HRGEncounter.EncounterRecno

	inner join APC.Encounter Episode
	on	Episode.ProviderSpellNo = Encounter.ProviderSpellNo

	where
		Episode.EncounterRecno = HRG47APCSpell.EncounterRecno
	)


INSERT INTO HRG.HRG47APCSpell
	(
	 EncounterRecno
	,ProviderSpellNo
	,HRGCode
	,DominantOperationCode
	,PrimaryDiagnosisCode
	,SecondaryDiagnosisCode
	,EpisodeCount
	,LOS
	,ReportingSpellLOS
	,Trimpoint
	,ExcessBeddays
	,CCDays
	,UnbundledHRGs
	)
select
	 Encounter.EncounterRecno
	,Spell.ProviderSpellNo
	,case when Spell.HRGCode = '' then null else Spell.HRGCode end
	,case when Spell.DominantOperationCode = '' then null else Spell.DominantOperationCode end
	,case when Spell.PrimaryDiagnosisCode = '' then null else Spell.PrimaryDiagnosisCode end
	,case when Spell.SecondaryDiagnosisCode = '' then null else Spell.SecondaryDiagnosisCode end
	,Spell.EpisodeCount
	,Spell.LOS
	,Spell.ReportingSpellLOS
	,Spell.Trimpoint
	,Spell.ExcessBeddays
	,Spell.CCDays
	,Spell.UnbundledHRGs--CM 28/07/2014 added this to help report on unbundled rehab
from
	HRG.THRG47APCSpell Spell

inner join HRG.THRG47APCEncounter Encounter
on	Spell.RowNo = Encounter.RowNo

select @RowsInsertedSpell = @@rowcount

--process APC Spell Flags
delete from HRG.HRG47APCSpellFlags
where
	not exists
	(
	select
		1
	from
		APC.Encounter Encounter
	where
		Encounter.EncounterRecno = HRG47APCSpellFlags.EncounterRecno
	)

delete from HRG.HRG47APCSpellFlags
where
	exists
	(
	select
		1
	from
		HRG.THRG47APCEncounter Encounter
	where
		Encounter.EncounterRecno = HRG47APCSpellFlags.EncounterRecno
	)


INSERT INTO HRG.HRG47APCSpellFlags
	(
	 EncounterRecno
	,SequenceNo
	,SpellFlagCode
	)
select
	 Encounter.EncounterRecno
	,SpellFlags.SequenceNo
	,SpellFlags.SpellFlagCode
from
	HRG.THRG47APCSpellFlags SpellFlags

inner join HRG.THRG47APCEncounter Encounter
on	SpellFlags.RowNo = Encounter.RowNo

select @RowsInsertedSpellFlags = @@rowcount

--process APC Unbundled
delete from HRG.HRG47APCUnbundled
where
	not exists
	(
	select
		1
	from
		APC.Encounter Encounter
	where
		Encounter.EncounterRecno = HRG47APCUnbundled.EncounterRecno
	)

delete from HRG.HRG47APCUnbundled
where
	exists
	(
	select
		1
	from
		HRG.THRG47APCEncounter Encounter
	where
		Encounter.EncounterRecno = HRG47APCUnbundled.EncounterRecno
	)


INSERT INTO HRG.HRG47APCUnbundled
	(
	 EncounterRecno
	,SequenceNo
	,HRGCode
	)
select
	 Encounter.EncounterRecno
	,Unbundled.SequenceNo
	,case when Unbundled.HRGCode = '' then null else Unbundled.HRGCode end
from
	HRG.THRG47APCUnbundled Unbundled

inner join HRG.THRG47APCEncounter Encounter
on	Unbundled.RowNo = Encounter.RowNo

select @RowsInsertedUnbundled = @@rowcount


--process APC Quality
delete from HRG.HRG47APCQuality
where
	not exists
	(
	select
		1
	from
		APC.Encounter Encounter
	where
		Encounter.EncounterRecno = HRG47APCQuality.EncounterRecno
	)

delete from HRG.HRG47APCQuality
where
	exists
	(
	select
		1
	from
		HRG.THRG47APCEncounter Encounter
	where
		Encounter.EncounterRecno = HRG47APCQuality.EncounterRecno
	)


INSERT INTO HRG.HRG47APCQuality
	(
	 EncounterRecno
	,SequenceNo
	,QualityTypeCode
	,QualityCode
	,QualityMessage
	)
select
	 Encounter.EncounterRecno
	,Quality.SequenceNo
	,case when Quality.QualityTypeCode = '' then null else Quality.QualityTypeCode end
	,case when Quality.QualityCode = '' then null else Quality.QualityCode end
	,case when Quality.QualityMessage = '' then null else Quality.QualityMessage end
from
	HRG.THRG47APCQuality Quality

inner join HRG.THRG47APCEncounter Encounter
on	Quality.RowNo = Encounter.RowNo

select @RowsInsertedQuality = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted (encounter): ' + CONVERT(varchar(10), @RowsInsertedEncounter) 
	+ '. Rows inserted (spell): ' + CONVERT(varchar(10), @RowsInsertedSpell) 
	+ ', Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'HRG - WH AssignHRG47APC', @Stats, @StartTime

--print @Stats
