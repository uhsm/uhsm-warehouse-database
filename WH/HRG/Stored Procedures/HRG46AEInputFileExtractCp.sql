﻿
CREATE procedure [HRG].[HRG46AEInputFileExtractCp] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsReturned Int
declare @Stats varchar(255)
declare @Created datetime
declare @InputStartDate datetime

select @StartTime = getdate()
--select @InputStartDate =  '2012-03-01 00:00:00.000'
select @InputStartDate =  '2013-03-31 00:00:00.000'
select
	 Age =
		FLOOR(
			datediff(
				 day
				,Encounter.DateOfBirth
				,Encounter.ArrivalDate
			) / 365.25
		)

	,Investigation1 =
	coalesce(left(
		replace(replace(Encounter.InvestigationCodeFirst, '.', ''), ',', '')
		, 4
	) , '')
	,Investigation2 =
	left(
		replace(replace(Encounter.InvestigationCodeSecond, '.', ''), ',', '')
		, 4
	)
	,NULL  Investigation3
	,NULL  Investigation4
	,NULL  Investigation5
	,NULL  Investigation6
	,NULL  Investigation7
	,NULL  Investigation8
	,NULL  Investigation9
	,NULL  Investigation10
	,NULL  Investigation11
	,NULL  Investigation12
	,Treatment1 =
	coalesce(
		left(
			replace(replace(Encounter.TreatmentCodeFirst, '.', ''), ',', '')
			, 4
			)
		 ,'99')
	,Treatment2 =
	left(
		replace(replace(Encounter.TreatmentCodeSecond, '.', ''), ',', '')
		, 4
	)
	,NULL Treatment3
	,NULL Treatment4
	,NULL Treatment5
	,NULL Treatment6
	,NULL Treatment7
	,NULL Treatment8
	,NULL Treatment9
	,NULL Treatment10
	,NULL Treatment11
	,NULL Treatment12
	,Encounter.EncounterRecno
	,Encounter.SourceUniqueID
	,Encounter.ArrivalTime
	--,Disposal =
	--	Encounter.AttendanceDisposalCode

from
	AE.Encounter Encounter

--left join AE.Investigation Investigation1
--on	Investigation1.AESourceUniqueID = Encounter.SourceUniqueID
--and	Investigation1.SequenceNo = 1

--left join AE.Investigation Investigation2
--on	Investigation2.AESourceUniqueID = Encounter.SourceUniqueID
--and	Investigation2.SequenceNo = 2

where
	coalesce(
		 Encounter.Updated
		,Encounter.Created
	)
	 > 
	(
	select
		DateValue
	from
		dbo.Parameter
	where
		Parameter = 'HRG46AEDATE'
	)
and Encounter.ArrivalDate > @InputStartDate

--where
--	Encounter.ArrivalDate between
--	(
--	select
--		min(ArrivalDate)
--	from
--		dbo.TLoadAEEncounter
--	)
--and
--	(
--	select
--		max(ArrivalDate)
--	from
--		dbo.TLoadAEEncounter
--	)

select @RowsReturned = @@rowcount

select @Elapsed = DATEDIFF(second, @StartTime, getdate())

select @Stats = 
	'InputStartDate = ' + CONVERT(varchar(11), @InputStartDate)
	+ ', rows returned: ' + CONVERT(varchar(10), @RowsReturned) 
	+ ', time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' secs'


exec WriteAuditLogEvent 'HRG - WH HRG46AEInputFileExtract', @Stats, @StartTime

--print @Stats
