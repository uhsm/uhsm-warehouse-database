﻿

CREATE procedure [HRG].[HRG46APCInputFileExtract] 
	--@BulkLoad int = null
as

declare @StartTime datetime
declare @Elapsed int
declare @RowsReturned Int
declare @Stats varchar(255)
declare @Created datetime
declare @InputStartDate datetime

select @StartTime = getdate()
select @InputStartDate =  '2013-03-31 00:00:00.000'

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[HRG].[HRG46APCInputFileExtractDump]') AND type in (N'U'))
DROP TABLE [HRG].[HRG46APCInputFileExtractDump]

--Optional start date parameter for bulk loading
--select @InputStartDate =  
--	case 
--		when DateValue is null then '2001-01-01 00:00:00.000'
--		else DateValue
--	end
--from dbo.Parameter 
--where Parameter = 'HRG46APCSTARTDATE'

--update dbo.Parameter set 
--	DateValue = 
--		case 
--			when @BulkLoad = 1 
--			then (select DateValue from dbo.Parameter where Parameter = 'HRG46APCSTARTDATE')
--			else (select DateValue from dbo.Parameter where Parameter = 'HRG46APCDATE')
--		end
--where Parameter = 'HRG46APCDATE'

--Comment this section out for the Unbundled Rehab data request
--------------------------------------------------------------------------
--create WardStay data to identify rehab days on F7
--------------------------------------------------------------------------
select 
	Dis.TheMonth as DischargeMonth
	,Episode.EpisodeUniqueID
	,Episode.[SourceSpellNo]
	,Episode.AdmissionDate
	,Episode.DischargeDate
	,Episode.EpisodeStartDate
	,Episode.EpisodeEndDate
	,WardStay.StartTime as WardStart
	,WardStay.EndTime as WardEnd
	,WardStay.WardCode
	,Episode.[SpecialtyCode(Function)]
	,Episode.[Specialty(Function)]
	,DATEDIFF(day,Episode.EpisodeStartDate,Episode.EpisodeEndDate) as  EpisodeLoS
	, COUNT(1) as TotalOccupancy
into #WardStayTemp
from WHREPORTING.APC.Episode Episode
  
inner join WHREPORTING.LK.Calendar Cal
	on Episode.EpisodeStartDateTime <= Cal.TheDate
    and coalesce(Episode.EpisodeEndDateTime,Episode.DischargeDateTime,(WHREPORTING.dbo.GetEndOfDay(getdate())))
		> Cal.TheDate	
			
left join WHREPORTING.LK.Calendar Dis
	on Episode.DischargeDate = Dis.TheDate
			
left join WHREPORTING.APC.WardStay WardStay
	on Episode.[SourceSpellNo] = WardStay.[SourceSpellNo]
		and WardStay.StartTime <= Cal.TheDate
        and coalesce(WardStay.EndTime,(WHREPORTING.dbo.GetEndOfDay(getdate())))
			> Cal.TheDate	
			
left join	
(	
	select distinct
		Location as WardLocation
		,ServicePointLocalCode as WardCode
		,ServicePoint as WardDesc
	  from PAS.ServicePoint
	  where ServicePointType = 'WARD'
	  and ArchiveFlag = 'N'
	  )	 Ward
	on WardStay.WardCode = Ward.WardCode
			
where Episode.[SpecialtyCode(Function)] = '314'
and isnull(WardStay.WardCode, '') = 'F7'
--Since RAD patients are only temporarily out of their bed we now include RAD
group by
	Dis.TheMonth
	,Episode.EpisodeUniqueID
	,Episode.SourceSpellNo
	,Episode.AdmissionDate
	,Episode.DischargeDate
	,Episode.EpisodeStartDate
	,Episode.EpisodeEndDate
	,WardStay.StartTime 
	,WardStay.EndTime
	,WardStay.WardCode
	,Episode.[SpecialtyCode(Function)]
	,Episode.[Specialty(Function)]
	,DATEDIFF(day,Episode.EpisodeStartDate,Episode.EpisodeEndDate)

	
--------------------------------------------------------------------------
	
select

	 ProviderCode = 'RM200'
	,Encounter.ProviderSpellNo

	,Encounter.EpisodeNo

	,EpisodeAge =
		floor(
			datediff(
				 day
				,Encounter.DateOfBirth
				,Encounter.EpisodeStartDate
			) / 365.25
		)

	,SexCode =
		cast(
			Sex.MappedCode
			as CHAR(1)
		)

	,PatientClassificationCode =
		case
		when
			Encounter.ManagementIntentionCode in
			(
			 2003683	--Not Applicable	8
			,2003684	--Not Known	9
			,2005563	--Not Specified	NSP
			)
		then
			case
			when datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
			then 2
			else 1
			end

		when 
			Encounter.ManagementIntentionCode in
				(
				 8850	--No Overnight Stay	2
				,8851	--Planned Sequence - No Overnight Stays	4
				,9487	--Planned Sequence - Inc. Over Night Stays	3
				)
		and
			Encounter.AdmissionMethodCode not in
			(
			 2003470	--Elective
			,8811		--Elective - Booked
			,8810		--Elective - Waiting List
			,8812		--Elective - Planned
			,13			--Not Specified
			,2003472	--OTHER
			)
		then 1

		when
			Encounter.ManagementIntentionCode in
			(
			 8851	--Planned Sequence - No Overnight Stays	4
			)
		and	datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
		then 3

		when
			Encounter.ManagementIntentionCode in
			(
			 9487	--Planned Sequence - Inc. Over Night Stays	3
			)
		and	datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
		then 4

		when datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
		then 2

		else 1
		end

	,AdmissionSourceCode =
		AdmissionSource.MappedCode

	,AdmissionMethodCode =
		AdmissionMethod.MappedCode

	,DischargeDestinationCode =
		DischargeDestination.MappedCode

	,DischargeMethodCode =
		DischargeMethod.MappedCode

	,EpisodeDuration =
		datediff(day, Encounter.EpisodeStartDate, Encounter.EpisodeEndDate)

	,MainSpecialtyCode = 
		LEFT(
			case
			when
				left(
					coalesce(
						 TreatmentFunctionSpecialtyMap.XrefEntityCode
						,TreatmentFunction.NationalSpecialtyCode
					)
					,3
				) = '500'
			then MainSpecialty.NationalSpecialtyCode

			else 
				coalesce(
					 TreatmentFunctionSpecialtyMap.XrefEntityCode
					,TreatmentFunction.NationalSpecialtyCode
				)
			end
			,3
		)

	,NeonatalLevelOfCareCode =
		coalesce(NeonatalLevelOfCare.MappedCode, '8')

	,TreatmentFunctionCode =
		left(
			coalesce(
				 SpecialtyTreatmentFunctionMap.XrefEntityCode
				,TreatmentFunction.NationalSpecialtyCode
			)
			,3
		)

	,Diagnosis1 =
	left(
		replace(replace(Encounter.PrimaryDiagnosisCode, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis2 =
	left(
		replace(replace(Encounter.SubsidiaryDiagnosisCode, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis3 = left(replace(replace(Encounter.SecondaryDiagnosisCode1,'.', ''), ',', ''), 5)
	,Diagnosis4 = left(replace(replace(Encounter.SecondaryDiagnosisCode2,'.', ''), ',', ''), 5)
	,Diagnosis5 = left(replace(replace(Encounter.SecondaryDiagnosisCode3,'.', ''), ',', ''), 5)
	,Diagnosis6 = left(replace(replace(Encounter.SecondaryDiagnosisCode4,'.', ''), ',', ''), 5)
	,Diagnosis7 = left(replace(replace(Encounter.SecondaryDiagnosisCode5,'.', ''), ',', ''), 5)
	,Diagnosis8 = left(replace(replace(Encounter.SecondaryDiagnosisCode6,'.', ''), ',', ''), 5)
	,Diagnosis9 = left(replace(replace(Encounter.SecondaryDiagnosisCode7,'.', ''), ',', ''), 5)
	,Diagnosis10 = left(replace(replace(Encounter.SecondaryDiagnosisCode8,'.', ''), ',', ''), 5)
	,Diagnosis11 = left(replace(replace(Encounter.SecondaryDiagnosisCode9,'.', ''), ',', ''), 5)
	,Diagnosis12 = left(replace(replace(Encounter.SecondaryDiagnosisCode10,'.', ''), ',', ''), 5)
	,Diagnosis13 = left(replace(replace(Encounter.SecondaryDiagnosisCode11,'.', ''), ',', ''), 5)
	,Diagnosis14 = left(replace(replace(Encounter.SecondaryDiagnosisCode12,'.', ''), ',', ''), 5)

	,Operation1 =
		left(
			replace(replace(Encounter.PrimaryOperationCode, '.', ''), ',', '')
			, 4
		) 

	,Operation2 = left(replace(replace(Encounter.SecondaryOperationCode1,'.', ''), ',', ''), 4)
	,Operation3 = left(replace(replace(Encounter.SecondaryOperationCode2,'.', ''), ',', ''), 4)
	,Operation4 = left(replace(replace(Encounter.SecondaryOperationCode3,'.', ''), ',', ''), 4)
	,Operation5 = left(replace(replace(Encounter.SecondaryOperationCode4,'.', ''), ',', ''), 4)
	,Operation6 = left(replace(replace(Encounter.SecondaryOperationCode5,'.', ''), ',', ''), 4)
	,Operation7 = left(replace(replace(Encounter.SecondaryOperationCode6,'.', ''), ',', ''), 4)
	,Operation8 = left(replace(replace(Encounter.SecondaryOperationCode7,'.', ''), ',', ''), 4)
	,Operation9 = left(replace(replace(Encounter.SecondaryOperationCode8,'.', ''), ',', ''), 4)
	,Operation10 = left(replace(replace(Encounter.SecondaryOperationCode9,'.', ''), ',', ''), 4)
	,Operation11 = left(replace(replace(Encounter.SecondaryOperationCode10,'.', ''), ',', ''), 4)
	,Operation12 = left(replace(replace(Encounter.SecondaryOperationCode11,'.', ''), ',', ''), 4)

	,CriticalCareDays = 0

	--19-03-2013: KO changed to 0. Not used at UHSM
	----reinstate this next part for the unbundled rehab data request
	--,RehabilitationDays =
	--Case 
	--when 
	--	left(
	--		coalesce(
	--			 SpecialtyTreatmentFunctionMap.XrefEntityCode
	--			,TreatmentFunction.NationalSpecialtyCode
	--		)
	--		,3) ='314'
	--then
	--datediff(day, Encounter.EpisodeStartDate, Encounter.EpisodeEndDate)
	--else
	-- 0
	-- end
-----
		--(
		--select
		--	SUM(
		--		DATEDIFF(
		--			DAY

		--			,case
		--			when WardStay.StartTime < Episode.EpisodeStartTime
		--			then Episode.EpisodeStartDate

		--			else cast(WardStay.StartTime as date)
		--			end

		--			,case
		--			when WardStay.EndTime < Episode.EpisodeEndTime
		--			then cast(WardStay.EndTime as date)

		--			else Episode.EpisodeEndDate
		--			end

		--		)
		--	)
		--from
		--	APC.WardStay

		--inner join APC.Encounter Episode
		--on	Episode.SourceSpellNo = WardStay.SourceSpellNo

		--where
		--	WardStay.WardCode = 150002661	--F10 DISCHARGE
		--and	WardStay.SourceSpellNo = Encounter.SourceSpellNo
		--and	Episode.EncounterRecno = Encounter.EncounterRecno
		--and	(
		--		WardStay.StartTime between Episode.EpisodeStartTime and Episode.EpisodeEndTime
		--	or	WardStay.EndTime between Episode.EpisodeStartTime and Episode.EpisodeEndTime
		--	)
		--)

	-- Comment out next part for Unbundled rehab data request
	,RehabilitationDays = isnull(WardStay.TotalDays, 0)
	,SpecialistPalliativeCareDays = 0
	,Encounter.EncounterRecno
	,Encounter.EpisodeEndDate
	,Encounter.SourceUniqueID
into HRG.HRG46APCInputFileExtractDump
from
	APC.Encounter Encounter
	
/*Comment this out for Unbundled Rehab data request */
left join (
	select episodeuniqueid, SourceSpellNo, COUNT(episodeuniqueid) as cnt,sum(TotalOccupancy) as TotalDays
	from #WardStayTemp 
	group by episodeuniqueid, SourceSpellNo
)
WardStay
on WardStay.EpisodeUniqueID = Encounter.SourceUniqueID

left join PAS.Specialty TreatmentFunction
on	TreatmentFunction.SpecialtyCode = Encounter.SpecialtyCode

left join PAS.Consultant
on	Consultant.ConsultantCode = Encounter.ConsultantCode

left join PAS.Specialty MainSpecialty
on	MainSpecialty.SpecialtyCode = Consultant.MainSpecialtyCode

left join dbo.EntityXref TreatmentFunctionSpecialtyMap
on	TreatmentFunctionSpecialtyMap.EntityTypeCode = 'TREATMENTFUNCTIONCODE'
and	TreatmentFunctionSpecialtyMap.XrefEntityTypeCode = 'SPECIALTYCODE'
and	TreatmentFunctionSpecialtyMap.EntityCode =
		coalesce(
			 left(TreatmentFunction.NationalSpecialtyCode, 3)
			,MainSpecialty.NationalSpecialtyCode
		)

left join dbo.EntityXref SpecialtyTreatmentFunctionMap
on	SpecialtyTreatmentFunctionMap.EntityTypeCode = 'SPECIALTYCODE'
and	SpecialtyTreatmentFunctionMap.XrefEntityTypeCode like 'TREATMENTFUNCTIONCODE%'
and	SpecialtyTreatmentFunctionMap.EntityCode =
		coalesce(
			 left(TreatmentFunction.NationalSpecialtyCode, 3)
			,MainSpecialty.NationalSpecialtyCode
		)

left join PAS.ReferenceValue AdmissionSource
on	AdmissionSource.ReferenceValueCode = Encounter.AdmissionSourceCode

left join PAS.ReferenceValue AdmissionMethod
on	AdmissionMethod.ReferenceValueCode = Encounter.AdmissionMethodCode

left join PAS.ReferenceValue DischargeDestination
on	DischargeDestination.ReferenceValueCode = Encounter.DischargeDestinationCode

left join PAS.ReferenceValue DischargeMethod
on	DischargeMethod.ReferenceValueCode = Encounter.DischargeMethodCode

left join PAS.ReferenceValue NeonatalLevelOfCare
on	NeonatalLevelOfCare.ReferenceValueCode = Encounter.NeonatalLevelOfCare

left join PAS.ReferenceValue Sex
on    Sex.ReferenceValueCode = Encounter.SexCode

where
	Encounter.DischargeDate is not null
--Use StartDate if present
and Encounter.DischargeDate > @InputStartDate
--	Encounter.DischargeDate > COALESCE(@InputStartDate, Encounter.DischargeDate)

--net change group
and	exists
	(
	select
		1
	from
		APC.Encounter Spell
	where
		coalesce(
			 Encounter.Updated
			,Encounter.Created
		)
		 > 
		(
		select
			DateValue
		from
			dbo.Parameter
		where
			Parameter = 'HRG46APCDATE'
		)
	and	Spell.SourceSpellNo = Encounter.SourceSpellNo
	)

--for testing individual spells
--and Encounter.ProviderSpellNo = 150413270

select @RowsReturned = @@rowcount

select @Elapsed = DATEDIFF(second, @StartTime, getdate())

select @Stats = 
	'InputStartDate = ' + CONVERT(varchar(11), 
		(select DateValue from dbo.Parameter where Parameter = 'HRG46APCDATE'))
	+ ', rows returned: ' + CONVERT(varchar(10), @RowsReturned) 
	+ ', time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' secs'

exec WriteAuditLogEvent 'HRG - WH HRG46APCInputFileExtract', @Stats, @StartTime

--print @Stats

