﻿
CREATE procedure [HRG].[HRG46CCInputFileExtract] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsReturned Int
declare @Stats varchar(255)
declare @Created datetime
declare @InputStartDate datetime

select @StartTime = getdate()
select @InputStartDate =  CONVERT(DATETIME, MIN(startdate), 112) from CC.AICU

select
	 CriticalCareUnitFunctionCode
	,BasicCardiovascularSupportDays
	,AdvancedCardiovascularSupportDays
	,BasicRespiratorySupportDays
	,AdvancedRespiratorySupportDays
	,RenalSupportDays
	,NeurologicalSupportDays
	,DermatologicalSupportDays
	,LiverSupportDays
	,Level2SupportDays
	,Level3SupportDays
	,StartDate
	,EndDate
	,CriticalCareLocalIdentifier
	,SourceSpellNo
	,DischargeDate
	--into dbo.HRG46CCInputFileAll
from
	CC.AICU

select @RowsReturned = @@rowcount

select @Elapsed = DATEDIFF(second, @StartTime, getdate())

select @Stats = 
	'InputStartDate = ' + CONVERT(varchar(11), @InputStartDate)
	+ ', rows returned: ' + CONVERT(varchar(10), @RowsReturned) 
	+ ', time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' secs'

exec WriteAuditLogEvent 'HRG - WH HRG46CCInputFileExtract', @Stats, @StartTime

--print @Stats
