﻿
CREATE procedure [HRG].[HRG47AEInputFileExtract] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsReturned Int
declare @Stats varchar(255)
declare @Created datetime
declare @InputStartDate datetime

select @StartTime = getdate()
--select @InputStartDate =  '2012-03-01 00:00:00.000'
select @InputStartDate =  '2014-03-31 00:00:00.000'--change this back to 2014-03-31 CM 01/04/2015

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[HRG].[HRG47AEInputFileExtractDump]') AND type in (N'U'))
DROP TABLE [HRG].[HRG47AEInputFileExtractDump]


select
	 Age =
		FLOOR(
			datediff(
				 day
				,Encounter.DateOfBirth
				,Encounter.ArrivalDate
			) / 365.25
		)

	,Investigation1 =
	coalesce(left(
		replace(replace(Encounter.InvestigationCodeFirst, '.', ''), ',', '')
		, 4
	) , '')
	,Investigation2 =
	left(
		replace(replace(Encounter.InvestigationCodeSecond, '.', ''), ',', '')
		, 4
	)
	,Investigation3 = null
	,Investigation4 = null
	,Investigation5 = null
	,Investigation6 = null
	,Investigation7 = null
	,Investigation8 = null
	,Investigation9 = null
	,Investigation10 = null
	,Investigation11 = null
	,Investigation12 = null
	,Treatment1 =
	coalesce(
		left(
			replace(replace(Encounter.TreatmentCodeFirst, '.', ''), ',', '')
			, 4
			)
		 ,'99')
	,Treatment2 =
	left(
		replace(replace(Encounter.TreatmentCodeSecond, '.', ''), ',', '')
		, 4
	)
	,Treatment3 = null
	,Treatment4 = null
	,Treatment5 = null
	,Treatment6 = null
	,Treatment7 = null
	,Treatment8 = null
	,Treatment9 = null
	,Treatment10 = null
	,Treatment11 = null
	,Treatment12 = null
	,Encounter.EncounterRecno
	,Encounter.SourceUniqueID
	,Encounter.ArrivalTime
	--,Disposal =
	--	Encounter.AttendanceDisposalCode
INTO HRG.HRG47AEInputFileExtractDump
from
	AE.Encounter Encounter

--left join AE.Investigation Investigation1
--on	Investigation1.AESourceUniqueID = Encounter.SourceUniqueID
--and	Investigation1.SequenceNo = 1

--left join AE.Investigation Investigation2
--on	Investigation2.AESourceUniqueID = Encounter.SourceUniqueID
--and	Investigation2.SequenceNo = 2

where
	coalesce(
		 Encounter.Updated
		,Encounter.Created
	)
	 > 
	(
	select
		DateValue
	from
		dbo.Parameter
	where
		Parameter = 'HRG47AEDATE'
	)
and Encounter.ArrivalDate > @InputStartDate

--where
--	Encounter.ArrivalDate between
--	(
--	select
--		min(ArrivalDate)
--	from
--		dbo.TLoadAEEncounter
--	)
--and
--	(
--	select
--		max(ArrivalDate)
--	from
--		dbo.TLoadAEEncounter
--	)

select @RowsReturned = @@rowcount

select @Elapsed = DATEDIFF(second, @StartTime, getdate())

select @Stats = 
	'InputStartDate = ' + CONVERT(varchar(11), @InputStartDate)
	+ ', rows returned: ' + CONVERT(varchar(10), @RowsReturned) 
	+ ', time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' secs'


exec WriteAuditLogEvent 'HRG - WH HRG47AEInputFileExtract', @Stats, @StartTime

--print @Stats

