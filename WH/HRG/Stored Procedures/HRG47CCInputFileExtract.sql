﻿
CREATE  procedure [HRG].[HRG47CCInputFileExtract] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsReturned Int
declare @Stats varchar(255)
declare @Created datetime
declare @InputStartDate datetime

select @StartTime = getdate()
select @InputStartDate =  CONVERT(DATETIME, MIN(startdate), 112) from CC.AICU

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[HRG].[HRG47CCInputFileExtractDump]') AND type in (N'U'))
DROP TABLE [HRG].[HRG47CCInputFileExtractDump]




select
	 CriticalCareUnitFunctionCode
	,BasicCardiovascularSupportDays
	,AdvancedCardiovascularSupportDays
	,BasicRespiratorySupportDays
	,AdvancedRespiratorySupportDays
	,RenalSupportDays
	,NeurologicalSupportDays
	,DermatologicalSupportDays
	,LiverSupportDays
	,Level2SupportDays
	,Level3SupportDays
	,StartDate
	,EndDate
	,CriticalCareLocalIdentifier
	,SourceSpellNo
	,DischargeDate
into HRG.HRG47CCInputFileExtractDump
from
	CC.AICU

select @RowsReturned = @@rowcount

select @Elapsed = DATEDIFF(second, @StartTime, getdate())

select @Stats = 
	'InputStartDate = ' + CONVERT(varchar(11), @InputStartDate)
	+ ', rows returned: ' + CONVERT(varchar(10), @RowsReturned) 
	+ ', time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' secs'

exec WriteAuditLogEvent 'HRG - WH HRG47CCInputFileExtract', @Stats, @StartTime

--print @Stats
