﻿CREATE TABLE [HRG].[HRG46APCSpellFlags] (
    [EncounterRecno] INT          NOT NULL,
    [SequenceNo]     INT          NOT NULL,
    [SpellFlagCode]  VARCHAR (10) NULL
);

