﻿CREATE TABLE [HRG].[HRG46OPEncounter] (
    [EncounterRecno]        INT          NOT NULL,
    [HRGCode]               VARCHAR (10) NULL,
    [GroupingMethodFlag]    VARCHAR (10) NULL,
    [DominantOperationCode] VARCHAR (10) NULL,
    [Created]               DATETIME     NULL,
    CONSTRAINT [PK_HRG46OPEncounter] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);

