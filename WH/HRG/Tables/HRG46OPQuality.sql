﻿CREATE TABLE [HRG].[HRG46OPQuality] (
    [EncounterRecno]  INT           NOT NULL,
    [SequenceNo]      SMALLINT      NOT NULL,
    [QualityTypeCode] VARCHAR (10)  NULL,
    [QualityCode]     VARCHAR (10)  NULL,
    [QualityMessage]  VARCHAR (255) NULL,
    CONSTRAINT [PK_HRG46OPQuality] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC, [SequenceNo] ASC)
);

