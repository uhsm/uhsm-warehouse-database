﻿CREATE TABLE [HRG].[HRG46OPUnbundled] (
    [EncounterRecno] INT          NOT NULL,
    [SequenceNo]     INT          NOT NULL,
    [HRGCode]        VARCHAR (10) NOT NULL,
    CONSTRAINT [PK_HRG46OPUnbundled] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC, [SequenceNo] ASC)
);

