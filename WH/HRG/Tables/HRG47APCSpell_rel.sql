﻿CREATE TABLE [HRG].[HRG47APCSpell_rel] (
    [EncounterRecno]         INT          NOT NULL,
    [ProviderSpellNo]        VARCHAR (20) NULL,
    [HRGCode]                VARCHAR (10) NULL,
    [DominantOperationCode]  VARCHAR (20) NULL,
    [PBCCode]                VARCHAR (10) NULL,
    [PrimaryDiagnosisCode]   VARCHAR (10) NULL,
    [SecondaryDiagnosisCode] VARCHAR (10) NULL,
    [EpisodeCount]           SMALLINT     NULL,
    [LOS]                    SMALLINT     NULL,
    [ReportingSpellLOS]      SMALLINT     NULL,
    [Trimpoint]              SMALLINT     NULL,
    [ExcessBeddays]          SMALLINT     NULL,
    [CCDays]                 SMALLINT     NULL
);

