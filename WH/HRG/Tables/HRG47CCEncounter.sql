﻿CREATE TABLE [HRG].[HRG47CCEncounter] (
    [EncounterRecno]   INT          NOT NULL,
    [HRGCode]          VARCHAR (10) NULL,
    [CriticalCareDays] SMALLINT     NULL,
    [WarningCode]      VARCHAR (10) NULL,
    [Created]          DATETIME     NULL,
    [SourceSpellNo]    VARCHAR (20) NULL
);

