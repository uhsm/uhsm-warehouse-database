﻿CREATE TABLE [HRG].[HRG47CCInputFileExtractDump_2011_2014] (
    [CriticalCareUnitFunctionCode]      VARCHAR (25)   NULL,
    [BasicCardiovascularSupportDays]    INT            NULL,
    [AdvancedCardiovascularSupportDays] INT            NULL,
    [BasicRespiratorySupportDays]       INT            NULL,
    [AdvancedRespiratorySupportDays]    INT            NULL,
    [RenalSupportDays]                  INT            NULL,
    [NeurologicalSupportDays]           INT            NULL,
    [DermatologicalSupportDays]         INT            NULL,
    [LiverSupportDays]                  INT            NULL,
    [Level2SupportDays]                 NUMERIC (18)   NOT NULL,
    [Level3SupportDays]                 NUMERIC (18)   NOT NULL,
    [StartDate]                         VARCHAR (8000) NULL,
    [EndDate]                           VARCHAR (8000) NULL,
    [CriticalCareLocalIdentifier]       INT            NOT NULL,
    [SourceSpellNo]                     NUMERIC (18)   NULL,
    [DischargeDate]                     VARCHAR (8000) NULL
);

