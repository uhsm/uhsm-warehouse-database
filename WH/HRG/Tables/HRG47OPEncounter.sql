﻿CREATE TABLE [HRG].[HRG47OPEncounter] (
    [EncounterRecno]        INT          NOT NULL,
    [HRGCode]               VARCHAR (10) NULL,
    [GroupingMethodFlag]    VARCHAR (10) NULL,
    [DominantOperationCode] VARCHAR (10) NULL,
    [Created]               DATETIME     NULL,
    CONSTRAINT [PK_HRG47OPEncounter] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);

