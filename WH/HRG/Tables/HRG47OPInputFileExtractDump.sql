﻿CREATE TABLE [HRG].[HRG47OPInputFileExtractDump] (
    [AgeOnAppointment]      NUMERIC (18)  NULL,
    [SexCode]               CHAR (1)      NULL,
    [MainSpecialtyCode]     VARCHAR (3)   NULL,
    [TreatmentFunctionCode] VARCHAR (3)   NULL,
    [FirstAttendanceCode]   VARCHAR (50)  NULL,
    [Operation1]            VARCHAR (4)   NULL,
    [Operation2]            VARCHAR (4)   NULL,
    [Operation3]            VARCHAR (4)   NULL,
    [Operation4]            VARCHAR (4)   NULL,
    [Operation5]            VARCHAR (4)   NULL,
    [Operation6]            VARCHAR (4)   NULL,
    [Operation7]            VARCHAR (4)   NULL,
    [Operation8]            VARCHAR (4)   NULL,
    [Operation9]            VARCHAR (4)   NULL,
    [Operation10]           VARCHAR (4)   NULL,
    [Operation11]           VARCHAR (4)   NULL,
    [Operation12]           VARCHAR (4)   NULL,
    [EncounterRecno]        INT           NOT NULL,
    [AppointmentDateTime]   SMALLDATETIME NULL,
    [SourceUniqueID]        INT           NOT NULL
);

