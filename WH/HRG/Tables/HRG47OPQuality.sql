﻿CREATE TABLE [HRG].[HRG47OPQuality] (
    [EncounterRecno]  INT           NOT NULL,
    [SequenceNo]      SMALLINT      NOT NULL,
    [QualityTypeCode] VARCHAR (10)  NULL,
    [QualityCode]     VARCHAR (10)  NULL,
    [QualityMessage]  VARCHAR (255) NULL
);

