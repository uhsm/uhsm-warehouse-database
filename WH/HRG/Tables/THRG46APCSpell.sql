﻿CREATE TABLE [HRG].[THRG46APCSpell] (
    [RowNo]                  VARCHAR (50) NULL,
    [HRGCode]                VARCHAR (50) NULL,
    [DominantOperationCode]  VARCHAR (50) NULL,
    [PrimaryDiagnosisCode]   VARCHAR (50) NULL,
    [SecondaryDiagnosisCode] VARCHAR (50) NULL,
    [EpisodeCount]           VARCHAR (50) NULL,
    [LOS]                    VARCHAR (50) NULL,
    [ReportingSpellLOS]      VARCHAR (50) NULL,
    [Trimpoint]              VARCHAR (50) NULL,
    [ExcessBeddays]          VARCHAR (50) NULL,
    [CCDays]                 VARCHAR (50) NULL,
    [ProviderSpellNo]        VARCHAR (50) NULL
);

