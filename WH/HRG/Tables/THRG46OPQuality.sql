﻿CREATE TABLE [HRG].[THRG46OPQuality] (
    [RowNo]           VARCHAR (50)  NULL,
    [SequenceNo]      VARCHAR (50)  NULL,
    [QualityTypeCode] VARCHAR (50)  NULL,
    [QualityCode]     VARCHAR (50)  NULL,
    [QualityMessage]  VARCHAR (255) NULL
);

