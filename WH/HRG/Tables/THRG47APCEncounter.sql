﻿CREATE TABLE [HRG].[THRG47APCEncounter] (
    [RowNo]                             VARCHAR (50)  NULL,
    [EncounterRecNo]                    VARCHAR (50)  NULL,
    [HRGCode]                           VARCHAR (50)  NULL,
    [GroupingMethodFlag]                VARCHAR (50)  NULL,
    [DominantOperationCode]             VARCHAR (50)  NULL,
    [PBCCode]                           VARCHAR (50)  NULL,
    [CalculatedEpisodeDuration]         VARCHAR (50)  NULL,
    [ReportingEpisodeDuration]          VARCHAR (50)  NULL,
    [Trimpoint]                         VARCHAR (50)  NULL,
    [ExcessBeddays]                     VARCHAR (50)  NULL,
    [SpellReportFlag]                   VARCHAR (50)  NULL,
    [ProviderSpellNo]                   VARCHAR (50)  NULL,
    [SourceUniqueID]                    VARCHAR (50)  NULL,
    [CriticalCareDaysInput]             INT           NULL,
    [RehabilitationDaysInput]           INT           NULL,
    [SpecialistPalliativeCareDaysInput] INT           NULL,
    [UnbundledHRGs]                     VARCHAR (250) NULL
);

