﻿CREATE TABLE [ICE].[TImportEvent] (
    [TransactionID]     UNIQUEIDENTIFIER NOT NULL,
    [EventType]         TINYINT          NOT NULL,
    [EventCode]         INT              NOT NULL,
    [Timestamp]         DATETIME         NOT NULL,
    [server]            VARCHAR (20)     NOT NULL,
    [ID]                INT              NOT NULL,
    [Recovery_DateTime] DATETIME         NULL
);

