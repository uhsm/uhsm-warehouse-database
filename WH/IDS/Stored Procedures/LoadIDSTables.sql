﻿-- =============================================
-- Author:		CMan
-- Create date: 17/06/2014
-- Description:	Store procedure to be used in daily job to extract the IDS data from ECC database on [v-uhsm-epr].
-- =============================================
CREATE PROCEDURE [IDS].[LoadIDSTables]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
--IDS document status reference table
Truncate table IDS.ECC_Doc_Status
Insert into IDS.ECC_Doc_Status
Select Doc_Status,
		Doc_Status_Description,
		HubStatusCheck
FROM  [v-uhsm-epr].[ECC].[dbo].[tblECC_Doc_Status]



--IDS records
Truncate table IDS.ECCDischargeSummaries
Insert into  IDS.ECCDischargeSummaries
Select  [Id]
      ,[Date_of_Admission]
      ,[LPI_discharge_datetime]
      ,[discharge_date]
      ,[discharge_time]
      ,[external_spell]
      ,[RM2Number]
      ,[NHSNumber]
      ,[Patient_Surname]
      ,[Patient_Forename]
      ,[Patient_DOB]
      ,[lnkpid]
      ,[lnkspell]
      ,[SectionCnt]
      ,[Reg_GP_code]
      ,[GP_Name]
      ,[Practice_code]
      ,[Referral_code]
      ,[Clinician_Code]
      ,[Clinician_Name]
      ,[Consultant_Code]
      ,[Consultant_Name]
      ,[Consultant_Speciality]
      ,[Discharge_Disposition]
      ,[Discharge_Location]
      ,[Discharge_Ward]
      ,[DischargeLounge]
      ,[Report_Code]
      ,[Status]
      ,[Doc_Created]
      ,[Doc_last_changed]
      ,[IDS_complete]
      ,[Drugs_verified]
      ,[Drugs_verified_datetime]
      ,[Print_date]
      ,[Print_time]
      ,[Doc_type]
      ,[ECC_Checked]
      ,[ECC_CheckedDateTime]
      ,[Last_Update]
      ,[Doc_status]
      ,[EDTHub_DocId]
      ,[EDT_status]
      ,[EDTHub_RejectType]
      ,[EDTHub_RejectReason]
      ,[EDTHub_Status_LastUpdated]
      ,[DocInDb]
      ,Record_date
      ,Record_time
      ,Admit_date
      ,Admit_time
      ,InsertDateTime

from [v-uhsm-epr].ECC.dbo.tblECC_docs


END
