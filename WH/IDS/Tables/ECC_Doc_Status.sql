﻿CREATE TABLE [IDS].[ECC_Doc_Status] (
    [Doc_Status]             INT            NOT NULL,
    [Doc_Status_Description] NVARCHAR (100) COLLATE Latin1_General_CI_AS NULL,
    [HubStatusCheck]         BIT            NOT NULL
);

