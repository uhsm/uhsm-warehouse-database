﻿CREATE TABLE [INFOSYS].[RTTDatasetSnapshot] (
    [ref_ref]           VARCHAR (20) COLLATE Latin1_General_CI_AS NULL,
    [Ref_date]          DATETIME     NULL,
    [ClockStopType]     VARCHAR (11) COLLATE Latin1_General_CI_AS NULL,
    [ClockStopDate]     DATETIME     NULL,
    [DQAction]          VARCHAR (30) COLLATE Latin1_General_CI_AS NULL,
    [OrigRefDate]       DATETIME     NULL,
    [ElecAdmitSpellRef] VARCHAR (20) COLLATE Latin1_General_CI_AS NULL
);


GO
CREATE CLUSTERED INDEX [ixRef]
    ON [INFOSYS].[RTTDatasetSnapshot]([ref_ref] ASC);


GO
CREATE NONCLUSTERED INDEX [ixDQAction]
    ON [INFOSYS].[RTTDatasetSnapshot]([DQAction] ASC);


GO
CREATE NONCLUSTERED INDEX [ixClockStopType]
    ON [INFOSYS].[RTTDatasetSnapshot]([ClockStopType] ASC);

