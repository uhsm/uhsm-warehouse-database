﻿




CREATE view [INFOSYS].[AdministrativeClockStart] as

select distinct --duplicate referrals in RTTDataset!
	 SourceCode = 
		case RTTDataset.ClockStopType
		when 'RefComplete' then 'REFRL'
		when 'AHStopClock' then 'REFRL'
		when 'WLremoval' then 'WLIST'
		else 'DECSD'
		end

	,ReferralSourceUniqueID = RTTDataset.Ref_ref
	,ClockStartDate = RTTDataset.Ref_date
from
	--[infosql].RTT_18_WEEKS.dbo.rtt_dataset RTTDataset
	INFOSYS.RTTDatasetSnapshot RTTDataset
where
	RTTDataset.ClockStopType in 
	(
	 'RefComplete'
	,'AHStopClock'
	,'WLremoval'
	,'Deceased'
	)
and	coalesce(RTTDataset.DQAction, '') not like '%NotRTT'
and	RTTDataset.OrigRefDate is not null








