﻿




CREATE view [INFOSYS].[AdmittedClockStart] as

select distinct --duplicate referrals in RTTDataset!
	 ReferralSourceUniqueID = RTTDataset.Ref_ref
	,SourceSpellNo = RTTDataset.ElecAdmitSpellRef
	,ClockStartDate = RTTDataset.Ref_date
from
	--[infosql].RTT_18_WEEKS.dbo.rtt_dataset RTTDataset
	INFOSYS.RTTDatasetSnapshot RTTDataset
where
	RTTDataset.ClockStopType = 'elecADmit'
and	coalesce(RTTDataset.DQAction, '') not like '%NotRTT'
and	RTTDataset.OrigRefDate is not null
and ElecAdmitSpellRef is not null






