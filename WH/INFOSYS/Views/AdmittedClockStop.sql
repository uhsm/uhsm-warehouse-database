﻿





CREATE view [INFOSYS].[AdmittedClockStop] as

select distinct --duplicate referrals in RTTDataset!
	 ReferralSourceUniqueID = RTTDataset.Ref_ref
	,SourceSpellNo = RTTDataset.ElecAdmitSpellRef
	,ClockStopDate = RTTDataset.ClockStopDate
from
	--[infosql].RTT_18_WEEKS.dbo.rtt_dataset RTTDataset
	INFOSYS.RTTDatasetSnapshot RTTDataset
where
	RTTDataset.ClockStopType = 'elecADmit'
and	coalesce(RTTDataset.DQAction, '') not like '%NotRTT'
and ElecAdmitSpellRef is not null







