﻿



CREATE view [INFOSYS].[NonAdmittedClockStart] as

select distinct --duplicate referrals in RTTDataset!
	 ReferralSourceUniqueID = RTTDataset.Ref_ref
	,OutpatientSourceUniqueID = Encounter.SourceUniqueID
	,ClockStartDate = RTTDataset.Ref_date
from
	--[infosql].RTT_18_WEEKS.dbo.rtt_dataset RTTDataset
	INFOSYS.RTTDatasetSnapshot RTTDataset
inner join OP.Encounter
on	Encounter.ReferralSourceUniqueID = RTTDataset.Ref_ref
and	Encounter.AppointmentDate = RTTDataset.Ref_date
and	not exists
	(
	select
		1
	from
		OP.Encounter Previous
	where
		Previous.ReferralSourceUniqueID = RTTDataset.Ref_ref
	and	Previous.AppointmentDate = RTTDataset.Ref_date
	and	(
			Previous.AppointmentTime > Encounter.AppointmentTime
		or	(
				Previous.AppointmentTime = Encounter.AppointmentTime
			and	Previous.EncounterRecno > Encounter.EncounterRecno
			)
		)
	)

where
	RTTDataset.ClockStopType in 
	(
	 'OPStopClock'
	,'OPDischWW'
	)
and	coalesce(RTTDataset.DQAction, '') not like '%NotRTT'
and	RTTDataset.OrigRefDate is not null






