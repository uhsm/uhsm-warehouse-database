﻿


CREATE view [INFOSYS].[NonAdmittedClockStop] as

select distinct --duplicate referrals in RTTDataset!
	 ReferralSourceUniqueID = RTTDataset.Ref_ref
	,OutpatientSourceUniqueID = Encounter.SourceUniqueID
	,ClockStopDate = RTTDataset.ClockStopDate
from
	--[infosql].RTT_18_WEEKS.dbo.rtt_dataset RTTDataset
	INFOSYS.RTTDatasetSnapshot RTTDataset
inner join OP.Encounter
on	Encounter.ReferralSourceUniqueID = RTTDataset.Ref_ref
and	Encounter.AppointmentDate = RTTDataset.ClockStopDate
and	not exists
	(
	select
		1
	from
		OP.Encounter Previous
	where
		Previous.ReferralSourceUniqueID = RTTDataset.Ref_ref
	and	Previous.AppointmentDate = RTTDataset.ClockStopDate
	and	(
			Previous.AppointmentTime > Encounter.AppointmentTime
		or	(
				Previous.AppointmentTime = Encounter.AppointmentTime
			and	Previous.EncounterRecno > Encounter.EncounterRecno
			)
		)
	)
where
	RTTDataset.ClockStopType in 
	(
	 'OPStopClock'
	,'OPDischWW'
	)
and	coalesce(RTTDataset.DQAction, '') not like '%NotRTT'








