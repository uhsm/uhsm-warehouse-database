﻿CREATE TABLE [LK].[lkNewDiagExclusionCodes] (
    [Code]              NVARCHAR (255) NULL,
    [TYPE]              NVARCHAR (255) NULL,
    [Description]       NVARCHAR (255) NULL,
    [On original list?] NVARCHAR (255) NULL
);

