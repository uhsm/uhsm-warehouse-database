﻿CREATE TABLE [MISYS].[AdditionalMultipliers] (
    [ExamCode]       NVARCHAR (255) NULL,
    [Multiplier]     FLOAT (53)     NULL,
    [Interventional] NVARCHAR (255) NULL
);

