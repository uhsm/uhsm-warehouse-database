﻿CREATE TABLE [MISYS].[CancelledOrder] (
    [OrderNumber] NVARCHAR (10) NOT NULL,
    [Created]     DATETIME      NULL,
    [ByWhom]      VARCHAR (50)  NULL,
    CONSTRAINT [pk_MISYS_CancelledOrder] PRIMARY KEY CLUSTERED ([OrderNumber] ASC)
);

