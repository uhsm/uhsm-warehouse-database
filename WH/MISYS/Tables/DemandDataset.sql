﻿CREATE TABLE [MISYS].[DemandDataset] (
    [OrdersPerformed]    INT            NOT NULL,
    [orderdate]          DATETIME       NOT NULL,
    [Staff]              NVARCHAR (50)  NULL,
    [MonthOrdered]       VARCHAR (8)    NOT NULL,
    [MonthOrderedName]   NVARCHAR (3)   NULL,
    [FinancialYear]      INT            NOT NULL,
    [RequestedCaseType]  NVARCHAR (50)  NULL,
    [OrderExamCode]      NVARCHAR (10)  NULL,
    [Exam]               NVARCHAR (130) NULL,
    [ModalityCode]       NVARCHAR (10)  NULL,
    [Modality]           NVARCHAR (100) NULL,
    [PriorityCode]       NVARCHAR (50)  NULL,
    [PlannedRequest]     NVARCHAR (30)  NULL,
    [CurrentPatientType] NVARCHAR (25)  NULL,
    [Specialty]          NVARCHAR (50)  NULL,
    [MonthSequence]      INT            NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [missing_index_4275_4274_DemandDataset]
    ON [MISYS].[DemandDataset]([orderdate] ASC, [ModalityCode] ASC)
    INCLUDE([Specialty]);


GO
CREATE NONCLUSTERED INDEX [missing_index_4289_4288_DemandDataset]
    ON [MISYS].[DemandDataset]([ModalityCode] ASC, [Specialty] ASC, [orderdate] ASC, [Staff] ASC, [OrderExamCode] ASC)
    INCLUDE([OrdersPerformed], [MonthOrdered], [MonthOrderedName], [FinancialYear], [RequestedCaseType], [Exam], [Modality], [PriorityCode], [CurrentPatientType], [MonthSequence]);


GO
CREATE NONCLUSTERED INDEX [missing_index_4297_4296_DemandDataset]
    ON [MISYS].[DemandDataset]([ModalityCode] ASC, [Specialty] ASC)
    INCLUDE([OrdersPerformed], [orderdate], [MonthOrdered], [MonthOrderedName], [OrderExamCode], [Exam], [Modality]);


GO
CREATE NONCLUSTERED INDEX [missing_index_10050_10049_DemandDataset]
    ON [MISYS].[DemandDataset]([orderdate] ASC)
    INCLUDE([OrdersPerformed], [MonthOrderedName], [FinancialYear], [ModalityCode], [Modality], [MonthSequence]);


GO
CREATE NONCLUSTERED INDEX [missing_index_4203_4202_DemandDataset]
    ON [MISYS].[DemandDataset]([ModalityCode] ASC)
    INCLUDE([OrderExamCode], [Exam]);


GO
CREATE NONCLUSTERED INDEX [missing_index_4284_4283_DemandDataset]
    ON [MISYS].[DemandDataset]([Specialty] ASC, [orderdate] ASC)
    INCLUDE([Staff]);


GO
CREATE NONCLUSTERED INDEX [missing_index_4279_4278_DemandDataset]
    ON [MISYS].[DemandDataset]([orderdate] ASC, [ModalityCode] ASC, [Specialty] ASC)
    INCLUDE([Staff], [OrderExamCode], [Exam]);


GO
CREATE NONCLUSTERED INDEX [missing_index_4277_4276_DemandDataset]
    ON [MISYS].[DemandDataset]([orderdate] ASC, [Specialty] ASC)
    INCLUDE([Staff]);


GO
CREATE NONCLUSTERED INDEX [missing_index_4281_4280_DemandDataset]
    ON [MISYS].[DemandDataset]([ModalityCode] ASC, [orderdate] ASC)
    INCLUDE([Specialty]);


GO
CREATE NONCLUSTERED INDEX [missing_index_10029_10028_DemandDataset]
    ON [MISYS].[DemandDataset]([orderdate] ASC)
    INCLUDE([OrdersPerformed], [MonthOrderedName], [FinancialYear], [Modality], [MonthSequence]);


GO
CREATE NONCLUSTERED INDEX [missing_index_4286_4285_DemandDataset]
    ON [MISYS].[DemandDataset]([ModalityCode] ASC, [Specialty] ASC, [orderdate] ASC, [Staff] ASC)
    INCLUDE([OrderExamCode], [Exam]);


GO
CREATE NONCLUSTERED INDEX [missing_index_4270_4269_DemandDataset]
    ON [MISYS].[DemandDataset]([ModalityCode] ASC)
    INCLUDE([OrderExamCode], [Exam]);


GO
CREATE NONCLUSTERED INDEX [missing_index_4462_4461_DemandDataset]
    ON [MISYS].[DemandDataset]([orderdate] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_4272_4271_DemandDataset]
    ON [MISYS].[DemandDataset]([ModalityCode] ASC, [Specialty] ASC)
    INCLUDE([OrdersPerformed], [orderdate], [MonthOrdered], [MonthOrderedName], [OrderExamCode], [Exam], [Modality]);

