﻿CREATE TABLE [MISYS].[DemandDataset_cp_07032014] (
    [OrdersPerformed]    INT            NOT NULL,
    [orderdate]          DATETIME       NOT NULL,
    [Staff]              NVARCHAR (50)  NULL,
    [MonthOrdered]       VARCHAR (8)    NOT NULL,
    [MonthOrderedName]   NVARCHAR (3)   NULL,
    [FinancialYear]      INT            NOT NULL,
    [RequestedCaseType]  NVARCHAR (50)  NULL,
    [OrderExamCode]      NVARCHAR (10)  NULL,
    [Exam]               NVARCHAR (130) NULL,
    [ModalityCode]       NVARCHAR (10)  NULL,
    [Modality]           NVARCHAR (100) NULL,
    [PriorityCode]       NVARCHAR (50)  NULL,
    [PlannedRequest]     NVARCHAR (30)  NULL,
    [CurrentPatientType] NVARCHAR (25)  NULL,
    [Specialty]          NVARCHAR (50)  NULL,
    [MonthSequence]      INT            NOT NULL
);

