﻿CREATE TABLE [MISYS].[DemandModelExamRef] (
    [Case Type]   NVARCHAR (255) NULL,
    [Exam Code]   NVARCHAR (255) NULL,
    [Description] NVARCHAR (255) NULL,
    [Anat]        NVARCHAR (255) NULL,
    [Mod]         NVARCHAR (255) NULL,
    [F6]          NVARCHAR (255) NULL
);

