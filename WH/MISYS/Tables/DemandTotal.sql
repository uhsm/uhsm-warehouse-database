﻿CREATE TABLE [MISYS].[DemandTotal] (
    [Year]             VARCHAR (4)   NULL,
    [MonthOrdered]     INT           NULL,
    [MonthOrderedName] VARCHAR (3)   NULL,
    [ModalityCode]     VARCHAR (10)  NULL,
    [Modality]         VARCHAR (100) NULL,
    [Specialty]        VARCHAR (100) NULL,
    [Orders]           INT           NULL,
    [OrdersLastYr]     INT           NULL
);

