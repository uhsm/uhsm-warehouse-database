﻿CREATE TABLE [MISYS].[DictCaseType] (
    [auto_can_days]               FLOAT (53)     NULL,
    [auto_can_rloc]               NVARCHAR (255) NULL,
    [auto_cancel]                 NVARCHAR (255) NULL,
    [block_encoding]              NVARCHAR (255) NULL,
    [block_purge]                 NVARCHAR (255) NULL,
    [case_type_active]            NVARCHAR (255) NULL,
    [case_type_code]              NVARCHAR (255) NOT NULL,
    [case_type_description]       NVARCHAR (255) NULL,
    [case_type_hosp_num]          FLOAT (53)     NULL,
    [def_quest]                   NVARCHAR (255) NULL,
    [dicom_type]                  NVARCHAR (255) NULL,
    [dict_case_type]              NVARCHAR (255) NULL,
    [link_master_folder_location] NVARCHAR (255) NULL,
    [loc_film_created_ordering]   NVARCHAR (255) NULL,
    [master_folder_location]      NVARCHAR (255) NULL,
    [order_entry_screen]          NVARCHAR (255) NULL,
    [order_mask]                  NVARCHAR (255) NULL,
    [pt_flow_pattern]             NVARCHAR (255) NULL,
    [s_signer]                    NVARCHAR (255) NULL,
    [vol_name]                    NVARCHAR (255) NULL,
    [work_type]                   NVARCHAR (255) NULL,
    [wp_template]                 NVARCHAR (255) NULL,
    CONSTRAINT [PK_DictCaseType] PRIMARY KEY CLUSTERED ([case_type_code] ASC)
);

