﻿CREATE TABLE [MISYS].[DictDepartment] (
    [department_codes] NVARCHAR (255) NOT NULL,
    [dept_active]      NVARCHAR (255) NULL,
    [dept_name]        NVARCHAR (255) NULL,
    CONSTRAINT [PK_DictDepartment] PRIMARY KEY CLUSTERED ([department_codes] ASC)
);

