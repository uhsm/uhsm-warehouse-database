﻿CREATE TABLE [MISYS].[DictExam] (
    [LinkFeeExamB]          NVARCHAR (255) NULL,
    [allow_follow_up]       NVARCHAR (255) NULL,
    [anatomic_region]       NVARCHAR (255) NULL,
    [case_type]             NVARCHAR (255) NULL,
    [department]            NVARCHAR (255) NULL,
    [dict_exam_code]        NVARCHAR (255) NULL,
    [exam_code]             NVARCHAR (255) NOT NULL,
    [exam_code_cpt4]        NVARCHAR (255) NULL,
    [exam_code_hosp_num]    FLOAT (53)     NULL,
    [exam_code_orderable]   NVARCHAR (255) NULL,
    [exam_description]      NVARCHAR (255) NULL,
    [exams_per_sub]         NVARCHAR (255) NULL,
    [exposures]             NVARCHAR (255) NULL,
    [film_codes]            NVARCHAR (255) NULL,
    [film_codes_disp]       NVARCHAR (255) NULL,
    [follow_up_code]        NVARCHAR (255) NULL,
    [follow_up_code_wo_HID] NVARCHAR (255) NULL,
    [group_code]            NVARCHAR (255) NULL,
    [left_right_indicator]  NVARCHAR (255) NULL,
    [link_case_type]        NVARCHAR (255) NULL,
    [link_department]       NVARCHAR (255) NULL,
    [link_proto_seq]        NVARCHAR (255) NULL,
    [num_films_disp]        NVARCHAR (255) NULL,
    [number_of_films]       NVARCHAR (255) NULL,
    [order_time_message]    NVARCHAR (255) NULL,
    [protocol_seq]          NVARCHAR (255) NULL,
    [schedulable]           NVARCHAR (255) NULL,
    [subfolder]             NVARCHAR (255) NULL,
    [time_type]             NVARCHAR (255) NULL,
    [usual_exam_time]       NVARCHAR (255) NULL,
    [wkld_mult]             FLOAT (53)     NULL,
    CONSTRAINT [PK_DictExam] PRIMARY KEY CLUSTERED ([exam_code] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_DictExam]
    ON [MISYS].[DictExam]([exam_description] ASC);

