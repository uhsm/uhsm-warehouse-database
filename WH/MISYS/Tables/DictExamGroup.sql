﻿CREATE TABLE [MISYS].[DictExamGroup] (
    [active_flag]            NVARCHAR (255) NULL,
    [dict_exam_group]        NVARCHAR (255) NOT NULL,
    [exam_group_translation] NVARCHAR (255) NULL,
    CONSTRAINT [PK_DictExamGroup] PRIMARY KEY CLUSTERED ([dict_exam_group] ASC)
);

