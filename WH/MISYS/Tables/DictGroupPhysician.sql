﻿CREATE TABLE [MISYS].[DictGroupPhysician] (
    [dict_group_physician] NVARCHAR (255) NOT NULL,
    [group_phys]           NVARCHAR (255) NULL,
    [group_phys_hosp_num]  FLOAT (53)     NULL,
    [group_phys_name]      NVARCHAR (255) NULL,
    [group_phys_with_mh]   NVARCHAR (255) NULL,
    [link_phys_code_wmh]   NVARCHAR (255) NULL,
    [phys_code]            NVARCHAR (255) NULL,
    [phys_code_hosp_num]   FLOAT (53)     NULL,
    [phys_code_with_mh]    NVARCHAR (255) NULL,
    CONSTRAINT [PK_DictPhysicianGroup] PRIMARY KEY CLUSTERED ([dict_group_physician] ASC)
);

