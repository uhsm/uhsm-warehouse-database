﻿CREATE TABLE [MISYS].[DictPatientLocation] (
    [dict_patient_locations] NVARCHAR (8)   NULL,
    [pat_loc_code]           NVARCHAR (15)  NULL,
    [loc_desc]               NVARCHAR (255) NULL,
    [loc_type]               NVARCHAR (3)   NULL,
    [loc_type_trans]         NVARCHAR (2)   NULL,
    [active_flag]            NVARCHAR (3)   NULL
);

