﻿CREATE TABLE [MISYS].[DictPhysician] (
    [active]                NVARCHAR (255) NULL,
    [auth_sign]             NVARCHAR (255) NULL,
    [borrow_priv]           NVARCHAR (255) NULL,
    [created_date_ODBC]     DATETIME       NULL,
    [created_dj]            FLOAT (53)     NULL,
    [days_keep_inpt_film]   NVARCHAR (255) NULL,
    [days_keep_outpt_film]  NVARCHAR (255) NULL,
    [dict_physicians]       NVARCHAR (255) NULL,
    [intext]                NVARCHAR (255) NULL,
    [link_hosp]             NVARCHAR (255) NULL,
    [phys_add1]             NVARCHAR (255) NULL,
    [phys_add2]             NVARCHAR (255) NULL,
    [phys_add3]             NVARCHAR (255) NULL,
    [phys_add4]             NVARCHAR (255) NULL,
    [phys_code]             NVARCHAR (255) NOT NULL,
    [phys_fax]              NVARCHAR (255) NULL,
    [phys_hosp_num]         FLOAT (53)     NULL,
    [phys_name]             NVARCHAR (255) NULL,
    [phys_phone]            NVARCHAR (255) NULL,
    [phys_printer]          NVARCHAR (255) NULL,
    [phys_userdef]          NVARCHAR (255) NULL,
    [rad_flag]              NVARCHAR (255) NULL,
    [scheduling_privileges] NVARCHAR (255) NULL,
    [sign_code]             NVARCHAR (255) NULL,
    [updated_date_ODBC]     DATETIME       NULL,
    [updated_dj]            FLOAT (53)     NULL,
    [npi]                   NVARCHAR (255) NULL,
    CONSTRAINT [PK_DictPhysician] PRIMARY KEY CLUSTERED ([phys_code] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DictPhysician]
    ON [MISYS].[DictPhysician]([dict_physicians] ASC);

