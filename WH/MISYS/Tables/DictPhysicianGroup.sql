﻿CREATE TABLE [MISYS].[DictPhysicianGroup] (
    [dict_physicians]        NVARCHAR (255) NULL,
    [dict_physicians_groups] NVARCHAR (255) NULL,
    [group_code]             NVARCHAR (255) NULL,
    [link_group_code]        NVARCHAR (255) NULL
);

