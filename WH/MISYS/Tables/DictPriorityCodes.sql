﻿CREATE TABLE [MISYS].[DictPriorityCodes] (
    [priority_code] NVARCHAR (6)  NULL,
    [description]   NVARCHAR (30) NULL,
    [active_flag]   NVARCHAR (1)  NULL
);

