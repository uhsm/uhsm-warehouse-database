﻿CREATE TABLE [MISYS].[DictRadiologistType] (
    [case_types]             NVARCHAR (255) NULL,
    [dict_rad_types]         NVARCHAR (255) NOT NULL,
    [rad_type_description]   NVARCHAR (255) NULL,
    [radiologist_type]       NVARCHAR (255) NULL,
    [signature_capabilities] NVARCHAR (255) NULL,
    [timeout]                FLOAT (53)     NULL,
    [weight]                 FLOAT (53)     NULL,
    CONSTRAINT [PK_DictRadiologistType] PRIMARY KEY CLUSTERED ([dict_rad_types] ASC)
);

