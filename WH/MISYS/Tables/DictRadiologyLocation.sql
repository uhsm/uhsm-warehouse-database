﻿CREATE TABLE [MISYS].[DictRadiologyLocation] (
    [AD_hdr_screen]            NVARCHAR (255) NULL,
    [AD_screen]                NVARCHAR (255) NULL,
    [OE_hdr_screen]            NVARCHAR (255) NULL,
    [OE_screen]                NVARCHAR (255) NULL,
    [aetitle]                  NVARCHAR (255) NULL,
    [campus]                   NVARCHAR (255) NULL,
    [datefrom]                 NVARCHAR (255) NULL,
    [dateto]                   NVARCHAR (255) NULL,
    [dict_radiology_locations] NVARCHAR (255) NULL,
    [disc]                     NVARCHAR (255) NULL,
    [hosp_id]                  NVARCHAR (255) NULL,
    [location_type]            NVARCHAR (255) NULL,
    [rad_loc_desc]             NVARCHAR (255) NULL,
    [rad_location_code]        NVARCHAR (255) NOT NULL,
    [rep_passwd]               NVARCHAR (255) NULL,
    [sched]                    NVARCHAR (255) NULL,
    CONSTRAINT [PK_DictRadiologyLocation] PRIMARY KEY CLUSTERED ([rad_location_code] ASC)
);

