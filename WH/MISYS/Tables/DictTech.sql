﻿CREATE TABLE [MISYS].[DictTech] (
    [active]          NVARCHAR (255) NULL,
    [dict_tech_codes] FLOAT (53)     NOT NULL,
    [tech_name]       NVARCHAR (255) NULL,
    CONSTRAINT [PK_DictTech] PRIMARY KEY CLUSTERED ([dict_tech_codes] ASC)
);

