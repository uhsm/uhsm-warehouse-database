﻿CREATE TABLE [MISYS].[HRGExclusions] (
    [Primary Procedure Code]        NVARCHAR (255) NULL,
    [Primary Procedure Description] NVARCHAR (255) NULL,
    [ModGrp]                        NVARCHAR (255) NULL
);

