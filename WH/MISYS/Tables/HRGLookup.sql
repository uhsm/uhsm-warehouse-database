﻿CREATE TABLE [MISYS].[HRGLookup] (
    [Primary Procedure Code]          NVARCHAR (255) NULL,
    [Primary Procedure Description]   NVARCHAR (255) NULL,
    [ModGrp]                          NVARCHAR (255) NULL,
    [Value]                           FLOAT (53)     NULL,
    [Contrast]                        NVARCHAR (255) NULL,
    [Proposed Code]                   NVARCHAR (255) NULL,
    [Tariff]                          FLOAT (53)     NULL,
    [Body part multiplication factor] FLOAT (53)     NULL
);

