﻿CREATE TABLE [MISYS].[HRGLookup_13052014] (
    [Primary Procedure Code]          NVARCHAR (255) NULL,
    [Primary Procedure Description]   NVARCHAR (255) NULL,
    [ModGrp]                          NVARCHAR (255) NULL,
    [Value]                           FLOAT (53)     NULL,
    [Contrast]                        NVARCHAR (255) NULL,
    [Proposed Code]                   NVARCHAR (255) NULL,
    [Tariff]                          FLOAT (53)     NULL,
    [Body part multiplication factor] FLOAT (53)     NULL,
    [F9]                              NVARCHAR (255) NULL,
    [F10]                             NVARCHAR (255) NULL,
    [F11]                             NVARCHAR (255) NULL
);

