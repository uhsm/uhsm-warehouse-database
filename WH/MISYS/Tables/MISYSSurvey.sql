﻿CREATE TABLE [MISYS].[MISYSSurvey] (
    [Patient Record Number]       FLOAT (53)     NULL,
    [NHS Organisation Code]       NVARCHAR (255) NULL,
    [NHS Number]                  BIGINT         NULL,
    [Gender]                      FLOAT (53)     NULL,
    [Title]                       NVARCHAR (255) NULL,
    [Firstname]                   NVARCHAR (255) NULL,
    [Surname]                     NVARCHAR (255) NULL,
    [Address 1]                   NVARCHAR (255) NULL,
    [Address 2]                   NVARCHAR (255) NULL,
    [Address 3]                   NVARCHAR (255) NULL,
    [Address 4]                   NVARCHAR (255) NULL,
    [Address 5]                   NVARCHAR (255) NULL,
    [Postcode]                    NVARCHAR (255) NULL,
    [Date of Birth]               DATETIME       NULL,
    [Ethnicity]                   NVARCHAR (255) NULL,
    [Day of Attendance]           FLOAT (53)     NULL,
    [Month of Attendance]         FLOAT (53)     NULL,
    [Year of Attendance]          FLOAT (53)     NULL,
    [Patient Source Setting Type] FLOAT (53)     NULL
);

