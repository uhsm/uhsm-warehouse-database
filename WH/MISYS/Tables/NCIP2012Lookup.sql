﻿CREATE TABLE [MISYS].[NCIP2012Lookup] (
    [ID]                                                             FLOAT (53)     NULL,
    [Short Code]                                                     NVARCHAR (255) NULL,
    [Body part multiplication factor]                                FLOAT (53)     NULL,
    [Status]                                                         NVARCHAR (255) NULL,
    [Reason if deprecated]                                           NVARCHAR (255) NULL,
    [Diagnostic procedure]                                           NVARCHAR (255) NULL,
    [Interventional Procedure]                                       NVARCHAR (255) NULL,
    [Recommended substitute procedure]                               NVARCHAR (255) NULL,
    [Preferred representation display term_(Max string length = 40)] NVARCHAR (255) NULL,
    [Synonym _(Max string length = 40)]                              NVARCHAR (255) NULL,
    [Synonym 2 _(Max string length = 40)]                            NVARCHAR (255) NULL,
    [Synonym 3 _(Max string length = 40)]                            NVARCHAR (255) NULL
);

