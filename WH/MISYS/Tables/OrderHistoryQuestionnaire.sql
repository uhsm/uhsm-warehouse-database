﻿CREATE TABLE [MISYS].[OrderHistoryQuestionnaire] (
    [EncounterRecno]            INT           IDENTITY (1, 1) NOT NULL,
    [OrderNumber]               VARCHAR (30)  NOT NULL,
    [OrderHistory]              VARCHAR (16)  NOT NULL,
    [ModifiedJulianDate]        INT           NULL,
    [ModifiedSeconds]           INT           NULL,
    [ModifiedTime]              SMALLDATETIME NULL,
    [TechnicianCode]            FLOAT (53)    NULL,
    [LinkTechnicianCode]        FLOAT (53)    NULL,
    [ModifyingLocationCode]     VARCHAR (30)  NULL,
    [LinkModifyingLocationCode] VARCHAR (30)  NULL,
    [Created]                   DATETIME      NULL,
    [ByWhom]                    VARCHAR (50)  NULL,
    CONSTRAINT [PK_OrderHistoryQuestionnaire] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);

