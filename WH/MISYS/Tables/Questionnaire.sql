﻿CREATE TABLE [MISYS].[Questionnaire] (
    [OrderNumber]      NVARCHAR (10)  NOT NULL,
    [Comments]         NVARCHAR (150) NULL,
    [DateVetted]       NVARCHAR (150) NULL,
    [DelayedByPatient] NVARCHAR (150) NULL,
    [PatientContact]   NVARCHAR (150) NULL,
    [PlannedRequest]   NVARCHAR (150) NULL,
    [RequestDate]      NVARCHAR (150) NULL,
    [Created]          DATETIME       NULL,
    [ByWhom]           VARCHAR (50)   NULL,
    CONSTRAINT [pk_MISYS_Questionnaire] PRIMARY KEY CLUSTERED ([OrderNumber] ASC)
);

