﻿CREATE TABLE [MISYS].[Specialty] (
    [OrderNumber]   NVARCHAR (10) NOT NULL,
    [SpecialtyName] NVARCHAR (25) NULL,
    [MonthOrdered]  INT           NULL,
    [FiscalYear]    INT           NULL
);

