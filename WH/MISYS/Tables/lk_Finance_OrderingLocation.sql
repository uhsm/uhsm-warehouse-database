﻿CREATE TABLE [MISYS].[lk_Finance_OrderingLocation] (
    [Location_code]                         NVARCHAR (255) NULL,
    [Location_Type]                         NVARCHAR (255) NULL,
    [Location_type_translation_description] NVARCHAR (255) NULL,
    [Location_description]                  NVARCHAR (255) NULL
);

