﻿CREATE TABLE [MISYS].[lk_Finance_Outside_Clinicians] (
    [phys_code]        NVARCHAR (255) NULL,
    [Outside Referral] NVARCHAR (255) NULL,
    [Which Hosp]       NVARCHAR (255) NULL,
    [group_phys]       NVARCHAR (255) NULL,
    [phys_name]        NVARCHAR (255) NULL,
    [group_phys_name]  NVARCHAR (255) NULL,
    [phys_add1]        NVARCHAR (255) NULL,
    [phys_add2]        NVARCHAR (255) NULL,
    [phys_add3]        NVARCHAR (255) NULL,
    [phys_add4]        NVARCHAR (255) NULL,
    [Account]          NVARCHAR (255) NULL,
    [Note]             NVARCHAR (255) NULL
);

