﻿CREATE TABLE [MISYS].[lk_Finance_Snowmed] (
    [Short Code]                                                     NVARCHAR (255) NULL,
    [Körner  Band]                                                   NVARCHAR (255) NULL,
    [Body part multiplication factor]                                FLOAT (53)     NULL,
    [Status]                                                         NVARCHAR (255) NULL,
    [Reason if deprecated]                                           NVARCHAR (255) NULL,
    [Diagnostic procedure]                                           NVARCHAR (255) NULL,
    [Interventional Procedure]                                       NVARCHAR (255) NULL,
    [Recommended substitute procedure]                               NVARCHAR (255) NULL,
    [Preferred representation display term (Max string length = 40)] NVARCHAR (255) NULL,
    [Synonym (Max string length = 40)]                               NVARCHAR (255) NULL,
    [Synonym 2 (Max string length = 40)]                             NVARCHAR (255) NULL,
    [Synonym 3 (Max string length = 40)]                             NVARCHAR (255) NULL,
    [HRG (JP)]                                                       NVARCHAR (255) NULL
);

