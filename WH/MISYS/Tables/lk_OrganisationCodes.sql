﻿CREATE TABLE [MISYS].[lk_OrganisationCodes] (
    [Organisation_Code] VARCHAR (50) NULL,
    [Organisation_Name] VARCHAR (50) NULL,
    [PAN_SHA_Code]      VARCHAR (50) NULL,
    [SHA_Code]          VARCHAR (50) NULL,
    [Address_Line_1]    VARCHAR (50) NULL,
    [Address_Line_2]    VARCHAR (50) NULL,
    [Address_Line_3]    VARCHAR (50) NULL,
    [Address_Line_4]    VARCHAR (50) NULL,
    [Address_Line_5]    VARCHAR (50) NULL,
    [PostCode]          VARCHAR (50) NULL,
    [OpenDate]          VARCHAR (50) NULL,
    [Column 11]         VARCHAR (50) NULL,
    [Column 12]         VARCHAR (50) NULL,
    [Column 13]         VARCHAR (50) NULL
);

