﻿CREATE TABLE [MISYS].[lk_finance_price] (
    [ExamCode]        NVARCHAR (10)  NULL,
    [ExamDescription] NVARCHAR (130) NULL,
    [Price]           FLOAT (53)     NULL,
    [FinancialYear]   INT            NULL,
    [casetypecode]    NVARCHAR (10)  NULL
);

