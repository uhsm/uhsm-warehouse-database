﻿create view [MISYS].CaseType as

select
	 AutoCancelDays = [auto_can_days]
	,LocationCode = [auto_can_rloc]
	,AutoCancelState = [auto_cancel]
	,AutoEncodeEnabled =  [block_encoding]
	,BlockPurge = [block_purge]
	,Active = [case_type_active]
	,CaseTypeCode = [case_type_code]
	,CaseType = [case_type_description]
	,HospitalCode = [case_type_hosp_num]
	,DefaultQuestionnaire = [def_quest]
	,DicomTypeCode = [dicom_type]
	,CaseTypeRowID = [dict_case_type]
	,MasterFolderLocationCode = [link_master_folder_location]
	,OrderingFilmLocationCode = [loc_film_created_ordering]
	,MasterFolderLocation = [master_folder_location]
	,OrderEntryScreen = [order_entry_screen]
	,OrderMask = [order_mask]
	,PatientTrackingFlow = [pt_flow_pattern]
	,SuperSignerCode = [s_signer]
	,VolumeName = [vol_name]
	,DigitalDictationWorkTypeCode = [work_type]
	,WordProcessingTemplate = [wp_template]
from
	[MISYS].[DictCaseType]




