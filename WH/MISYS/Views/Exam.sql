﻿create view [MISYS].Exam as

select
	 FeeScheduleExamCode = [LinkFeeExamB]
	,AllowFollowUpCode =[allow_follow_up]
	,AnatomicRegionCode = [anatomic_region]
	,CaseTypeCode = [case_type]
	,Department = [department]
	,ExamRowID = [dict_exam_code]
	,ExamCode = [exam_code]
	,CPT4Code = [exam_code_cpt4]
	,HospitalCode = [exam_code_hosp_num]
	,OrderableCode = [exam_code_orderable]
	,Exam = [exam_description]
	,ExamsPerSubfolder = [exams_per_sub]
	,PromptForExposures = [exposures]
	,FilmCodes = [film_codes]
	,FilmCodesDisplay = [film_codes_disp]
	,FollowUpCode = [follow_up_code]
	,WithoutHIDFollowUpCode = [follow_up_code_wo_HID]
	,ExamGroupCode = [group_code]
	,LeftRightIndicator = [left_right_indicator]
	,CaseTypeRowID = [link_case_type]
	,DepartmentCode = [link_department]
	,ProtocolSequenceRowID = [link_proto_seq]
	,DisplayFilmsCount = [num_films_disp]
	,FilmsCount = [number_of_films]
	,OrderTimeMessage = [order_time_message]
	,ProtocolSequenceCode = [protocol_seq]
	,Schedulable = [schedulable]
	,Subfolder = [subfolder]
	,TimeType = [time_type]
	,UsualDuration = [usual_exam_time]
	,WorkloadMultiplier = [wkld_mult]
from
	[MISYS].[DictExam]




