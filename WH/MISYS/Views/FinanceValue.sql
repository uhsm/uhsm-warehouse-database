﻿








CREATE view [MISYS].[FinanceValue] as

select distinct cast(replace(SocialSecurityNo,' ','') as varchar)+cast(casetypecode as varchar)+cast(PerformanceDate as varchar) as RadKey,
casetypecode+
cast(isnull(sum(hrg.[value]),0) as varchar)+ISNULL(c.ContrastValue,'') as ExamValue
from MISYS.[Order] b
left join MISYS.hrglookup hrg
on b.OrderExamCode=HRG.[Primary Procedure Code]
left join 
(select cast(replace(SocialSecurityNo,' ','') as varchar)+cast(casetypecode as varchar)+cast(PerformanceDate as varchar) as RadKey,
Contrast as ContrastValue
from MISYS.[Order] b
INNER join MISYS.hrglookup Contrast
on b.OrderExamCode=Contrast.[Primary Procedure Code]
where PerformanceDate >'2013-03-31'
and CaseTypeCode NOT IN 
 ('H','T','Z','O','R','X','Q','V')
AND
OrderExamCode NOT IN (
										'IFACJJ',
										'FINJTJ',
										'FHIPLJ',
										'FHIPRJ',
										'FJOINJ',
										'FABDO',
										'FDIAP',
										'FLOLB',
										'FLOLL',
										'FLOLR',
										'FLSPN',
										'FUPLB',
										'FUPLL',
										'FUPLR',
										'FHIPLM',
										'FHIPRM',
										'FLSPNM',								
										'MACOA',
										'MCVIA',
										'MCCMS',
										'MCORP',
										'MCRPS',
										'MCSFS',
										'MCVFS',
										'MCVVS',
										'MCORV',
										'MCARD',
										'NSENT'
										,'NSENTL'
										,'NSENTR'
										,'BSPE',
										'CORR',
										'MAGL',
'MAL1',
'MAL2',
'MAR2',
'MCCL',
'MCCR',
'MTOR',
'XBSPER',
'XMACR',
'XMAMLO',
'XMMAL',
'XMPVLB',
'XMULR',
'XPATHL',
'BRMDT',
'CORL',
'MAGR',
'MTOL',
'XMACL',
'XMAML',
'XMAMRI',
'XMUBBB',
'XMULB',
'MAR1',
'MNLR',
'XBSPEL',
'XMACB',
'XMAMB',
'XMAMRO',
'XMMAB',
'XMMAR',
'XMPVBB',
'XMUBLB',
'XMULL',
'MAB2',
'MNLL',
'XMAMBO',
'XMAMLI',
'XMAMR',
'XMPVRB',
'XMUBRB',
'XPATHR',
'FCSPNM',
'EVCAP'
,'MAAOT'
,'FBAEN'
,'YSBCE'
,'FPAC'
										)
and contrast is not null
group by
 cast(replace(SocialSecurityNo,' ','') as varchar)+cast(casetypecode as varchar)+cast(PerformanceDate as varchar),
Contrast) as c

on c.RadKey=cast(replace(b.SocialSecurityNo,' ','') as varchar)+cast(b.casetypecode as varchar)+cast(b.PerformanceDate as varchar) 

inner join MISYS.LenCodeValue lcv on lcv.RadKey=cast(replace(SocialSecurityNo,' ','') as varchar)+cast(casetypecode as varchar)+cast(PerformanceDate as varchar)
and lcv.LenValue=LEN(casetypecode+
'1'+ISNULL(c.ContrastValue,''))



where PerformanceDate > '2012-03-31' and
CaseTypeCode NOT IN 
 ('H','T','Z','O','R','X','Q','V')
AND
OrderExamCode NOT IN (
									'IFACJJ',
										'FINJTJ',
										'FHIPLJ',
										'FHIPRJ',
										'FJOINJ',
										'FABDO',
										'FDIAP',
										'FLOLB',
										'FLOLL',
										'FLOLR',
										'FLSPN',
										'FUPLB',
										'FUPLL',
										'FUPLR',
										'FHIPLM',
										'FHIPRM',
										'FLSPNM',								
										'MACOA',
										'MCVIA',
										'MCCMS',
										'MCORP',
										'MCRPS',
										'MCSFS',
										'MCVFS',
										'MCVVS',
										'MCORV',
										'MCARD',
										'NSENT'
										,'NSENTL'
										,'NSENTR'
										,'BSPE',
										'CORR',
										'MAGL',
'MAL1',
'MAL2',
'MAR2',
'MCCL',
'MCCR',
'MTOR',
'XBSPER',
'XMACR',
'XMAMLO',
'XMMAL',
'XMPVLB',
'XMULR',
'XPATHL',
'BRMDT',
'CORL',
'MAGR',
'MTOL',
'XMACL',
'XMAML',
'XMAMRI',
'XMUBBB',
'XMULB',
'MAR1',
'MNLR',
'XBSPEL',
'XMACB',
'XMAMB',
'XMAMRO',
'XMMAB',
'XMMAR',
'XMPVBB',
'XMUBLB',
'XMULL',
'MAB2',
'MNLL',
'XMAMBO',
'XMAMLI',
'XMAMR',
'XMPVRB',
'XMUBRB',
'XPATHR',
'FCSPNM',
'EVCAP'
,'MAAOT'
,'FBAEN'
,'YSBCE'
,'FPAC'										)
										
								
group by
cast(replace(SocialSecurityNo,' ','') as varchar)+cast(casetypecode as varchar)+cast(PerformanceDate as varchar),
casetypecode,
ISNULL(c.ContrastValue,'')







