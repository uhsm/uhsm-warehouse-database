﻿create view [MISYS].GroupPhysician as

select
	 GroupPhysicianRowID = [dict_group_physician]
	,GroupPhysicianCode = [group_phys]
	,HospitalCode = [group_phys_hosp_num]
	,GroupPhysician = [group_phys_name]
	,WithMHGroupPhysicianCode = [group_phys_with_mh]
	,MHGroupPhysicianCode = [link_phys_code_wmh]
	,PhysicianCode = [phys_code]
	,PhysicianHospitalCode = [phys_code_hosp_num]
	,MHPhysicianCode = [phys_code_with_mh]
from
	[MISYS].[DictGroupPhysician]




