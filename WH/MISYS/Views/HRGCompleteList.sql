﻿




CREATE view [MISYS].[HRGCompleteList] as
/*
--	Author:  J Cleworth K Oakden
--	Date: 04/08/2013
--	To summarise all tests on MISYS according to their status on the HRGLookup table
--  Used in Radiology HRG Lookup report
Included - Exam HRG data confirmed by Radiology
Excluded - Exam that doesn't need to link to HRG, confirmed by Radiology
New - Exams that have appeared on MISYS since 31/03/2013 that have not been added to HRGLookup 
or HRGExclusion table - requiring action from Radiology
*/
select 
	ExamStatus = case 
		when ExamSummary.HRGLookupCode is not null
		then 'Included'
		when ExamSummary.HRGExclusionCode  is not null
		then 'Excluded'
		when ExamSummary.LastPerformed > '2013-03-31' 
		then 'New - ACTION REQUIRED'
		end
	,ExamSummary.OrderExamCode
	,ExamSummary.ExamDescription
	,ExamSummary.ModGrp
	,ExamSummary.Value
	,ExamSummary.Contrast
	,ExamSummary.ProposedCode
	,ExamSummary.Tariff
	,LastPerformed = convert(varchar(10), ExamSummary.LastPerformed, 121)
	,ExamSummary.ActivityTotal
	,ExamSummary.ActivityThisYear
	,ExamSummary.HRGLookupCode 
	,ExamSummary.HRGExclusionCode 
	--,ExamSummary.CaseTypeCode
from 
(
	select 
		AllExams.OrderExamCode
		,ExamDescription = coalesce(ExamDescriptions.OrderExamDescription, HRGLookUp.[Primary Procedure Description])
		,HRGLookUp.ModGrp
		,HRGLookUp.Value
		,HRGLookUp.Contrast
		,ProposedCode = HRGLookUp.[Proposed Code]
		,HRGLookUp.Tariff
		,LastPerformed = max(BaseOrder.performancedate)
		,ActivityTotal = count(BaseOrder.ordernumber)
		,ActivityThisYear = sum(
			case when BaseOrder.performancedate > '2013-03-31' then 1 else 0 end)
		,HRGLookupCode = HRGLookUp.[Primary Procedure Code]
		,HRGExclusionCode = HRGExclusions.[Primary Procedure Code]
		--,BaseOrder.CaseTypeCode
	from
	
	(
		select distinct OrderExamCode from WHOLAP.dbo.BaseOrder
		union 
		select [Primary Procedure Code] from MISYS.HRGLookUp  
		union 
		select [Primary Procedure Code] from MISYS.HRGExclusions  
	)AllExams
	
	left join WHOLAP.dbo.BaseOrder BaseOrder
		on BaseOrder.OrderExamCode = AllExams.OrderExamCode
		and BaseOrder.PerformanceLocation IS NOT NULL
		and not(BaseOrder.PatientSurname in 
			('TEST','TESTER','TEST2','TESTPATIENT','XXTEST PATIENT','XXTESTER'))
		and not (BaseOrder.PatientForename ='Fred' and BaseOrder.PatientSurname='Flintstone')
		and not (BaseOrder.PatientForename ='Barney' and BaseOrder.PatientSurname='Rubble')
		and not (BaseOrder.PatientForename ='Fred' and BaseOrder.PatientSurname='Flinstone')
		and not (BaseOrder.PatientForename ='Dame' and BaseOrder.PatientSurname='Doody')
		and not (BaseOrder.PatientForename ='Betty' and BaseOrder.PatientSurname='Rubble')
		and not (BaseOrder.PatientForename ='Wilma' and BaseOrder.PatientSurname='Flintstone')

		and BaseOrder.OrderingPhysician not like ('Assess%')

		AND BaseOrder.SocialSecurityNo not in ('0110110110','011 011 0110','5653249999''565 324 9999')

		--AND bOrder.ExamCode1 NOT IN ('HBIOL','XBSPER','HBIOM','HCACR','BSPE','XPATHL','HFLUC','XBSPEL','XPATHR','BRMDT')

		and BaseOrder.CaseTypeCode NOT IN 
		 ('H','T','Z','O','R','X','Q','V')
		 
	left join MISYS.HRGLookUp  
		on HRGLookUp.[Primary Procedure Code]=AllExams.OrderExamCode
	
	left join MISYS.HRGExclusions 
		on HRGExclusions.[Primary Procedure Code]=AllExams.OrderExamCode
	
	left join 
	(
		select  OrderExamCode, max(ExamDescription) as OrderExamDescription 
		from WHOLAP.dbo.BaseOrder 
		group by OrderExamCode
	)ExamDescriptions
		on ExamDescriptions.OrderExamCode = AllExams.OrderExamCode
	 
	group by 
		AllExams.OrderExamCode 
		,coalesce(ExamDescriptions.OrderExamDescription, HRGLookUp.[Primary Procedure Description])
		,HRGLookUp.ModGrp
		,HRGLookUp.Value
		,HRGLookUp.Contrast
		,HRGLookUp.[Proposed Code]
		,HRGLookUp.Tariff
		,HRGExclusions.[Primary Procedure Code]
		,HRGLookUp.[Primary Procedure Code]
		--,BaseOrder.CaseTypeCode
)ExamSummary
--where case 
--		when ExamSummary.HRGLookupCode is not null
--		then 'Included'
--		when ExamSummary.HRGExclusionCode  is not null
--		then 'Excluded'
--		when ExamSummary.LastPerformed > '2013-03-31' 
--		then 'New'
--		end is not null

--order by 	ExamSummary.OrderExamCode

--order by case 
--		when ExamSummary.HRGLookupCode is not null
--		then 'Included'
--		when ExamSummary.HRGExclusionCode  is not null
--		then 'Excluded'
--		when ExamSummary.LastPerformed > '2013-03-31' 
--		then 'New'
--		end


