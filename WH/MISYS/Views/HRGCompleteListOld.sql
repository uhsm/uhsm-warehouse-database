﻿
CREATE view [MISYS].[HRGCompleteListOld] as
/*
--	Author:  J Cleworth K Oakden
--	Date: 04/08/2013
--	To summarise all tests on MISYS according to their status on the HRGLookup table
--  Used in Radiology HRG Lookup report
Included - Exam HRG data confirmed by Radiology
Excluded - Exam that doesn't need to link to HRG, confirmed by Radiology
New - Exams that have appeared on MISYS since 31/03/2013 that have not been added to HRGLookup 
or HRGExclusion table - requiring action from Radiology
*/
select 'Included' as [Exam Status], * from MISYS.HRGLookUp

union all

select 'Excluded', *, null, null, null, null from MISYS.HRGExclusions

union all

select distinct 
	'New'
	,OrderExamCode
	,ExamDescription
	,null, null, null, null, null
	--,count(ordernumber) as Activity 
from WHOLAP.dbo.BaseOrder border

where not exists (select 1 from MISYS.HRGLookUp where HRGLookUp.[Primary Procedure Code]=border.OrderExamCode)

and
(bOrder.PerformanceLocation IS NOT NULL)
and NOT(bOrder.PatientSurname in 
('TEST','TESTER','TEST2','TESTPATIENT','XXTEST PATIENT','XXTESTER'))

and not (bOrder.PatientForename ='Fred' and bOrder.PatientSurname='Flintstone')
and not (bOrder.PatientForename ='Barney' and bOrder.PatientSurname='Rubble')
and not (bOrder.PatientForename ='Fred' and bOrder.PatientSurname='Flinstone')
and not (bOrder.PatientForename ='Dame' and bOrder.PatientSurname='Doody')
and not (bOrder.PatientForename ='Betty' and bOrder.PatientSurname='Rubble')
and not (bOrder.PatientForename ='Wilma' and bOrder.PatientSurname='Flintstone')

and border.OrderingPhysician not like ('Assess%')

AND border.SocialSecurityNo NOT IN ('0110110110','011 011 0110','5653249999''565 324 9999')

--AND bOrder.ExamCode1 NOT IN ('HBIOL','XBSPER','HBIOM','HCACR','BSPE','XPATHL','HFLUC','XBSPEL','XPATHR','BRMDT')

and border.CaseTypeCode NOT IN 
 ('H','T','Z','O','R','X','Q','V')
AND
border.OrderExamCode NOT IN ( select [Primary Procedure Code] from MISYS.HRGExclusions
					
)
    
and border.performancedate > '2013-03-31' 
