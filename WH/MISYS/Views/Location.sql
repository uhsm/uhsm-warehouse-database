﻿

CREATE view [MISYS].[Location] as

select
	 ADHeaderScreen = [AD_hdr_screen]
	,ADScreen = [AD_screen]
	,OrderEntryHeaderScreen = [OE_hdr_screen]
	,OrderEntryScreen = [OE_screen]
	,AETitle = [aetitle]
	,CampusCode = [campus]
	,DateForm = [datefrom]
	,DateTo = [dateto]
	,LocationCode = [dict_radiology_locations]
	,Disc = [disc]
	,HospitalID = [hosp_id]
	,LocationTypeCode = [location_type]
	,Location = [rad_loc_desc]
	,RadiologyLocationCode = [rad_location_code]
	,RepeatPasswordSetting = [rep_passwd]
	,Schedule = [sched]
	,LocationCategoryCode = LocationCategoryMap.XrefEntityCode
from
	[MISYS].[DictRadiologyLocation] Location

left join dbo.EntityXref LocationCategoryMap
on	LocationCategoryMap.EntityCode = Location.dict_radiology_locations
and	LocationCategoryMap.EntityTypeCode = 'MISYSORDERINGLOCATION'
and	LocationCategoryMap.XrefEntityTypeCode = 'MISYSCATEGORY'




