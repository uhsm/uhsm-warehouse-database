﻿create view [MISYS].Physician as

select
	 Active = [active]
	,AuthorisedToSignFor = [auth_sign]
	,BorrowerPrivilages = [borrow_priv]
	,Created = [created_date_ODBC]
	,CreatedJulianDate = [created_dj]
	,KeepInpatientFileDays = [days_keep_inpt_film]
	,KeepOutpatientFileDays = [days_keep_outpt_film]
	,PhysicianRowID = [dict_physicians]
	,InternalExternalCode = [intext]
	,HospitalRowID = [link_hosp]
	,Adddress1 = [phys_add1]
	,Adddress2 = [phys_add2]
	,Adddress3 = [phys_add3]
	,Adddress4 = [phys_add4]
	,PhysicianCode = [phys_code]
	,FaxNumber = [phys_fax]
	,HospitalCode = [phys_hosp_num]
	,Physician = [phys_name]
	,TelephoneNumber = [phys_phone]
	,Printer = [phys_printer]
	,UserDefinedField = [phys_userdef]
	,RadiologistFlag = [rad_flag]
	,SchedulingPrivileges = [scheduling_privileges]
	,SignatureCode = [sign_code]
	,Updated = [updated_date_ODBC]
	,UpdatedJulianDate = [updated_dj]
	,NPI = [npi]
from
	[MISYS].[DictPhysician]




