﻿

CREATE view [MISYS].[RadDirRpt] as

select CaseTypeCode,


FinancialMonthKey as themonth,


COUNT(*) as CountExams from wholap.dbo.baseorder b

INNER JOIN WHREPORTING.LK.Calendar cal
on cal.TheDate=PerformanceDate


where PerformanceLocation is not null


and cal.FinancialMonthKey IN
               (select distinct top 24 financialmonthkey
      from WHREPORTING.lk.calendar
      
      where

       FinancialMonthKey<
      
      (select FinancialMonthKey from WHREPORTING.LK.Calendar
      where TheDate=CAST(GETDATE() as DATE)
      
      
      )order by FinancialMonthKey desc)
group by CaseTypeCode,FinancialMonthKey
