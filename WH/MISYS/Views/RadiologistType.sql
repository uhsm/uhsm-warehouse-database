﻿create view [MISYS].[RadiologistType] as

select
	 CaseTypeCodes = [case_types]
	,RadiologistTypeRowID = [dict_rad_types]
	,RadiologistType = [rad_type_description]
	,RadiologistTypeCode = [radiologist_type]
	,SignatureCapabilities = [signature_capabilities]
	,TimeoutHours = [timeout]
	,PriorityWeight = [weight]
from
	[MISYS].[DictRadiologistType]




