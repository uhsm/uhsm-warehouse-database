﻿
CREATE view [MISYS].[ReportPbRValidation] as
/*
--	Author:  J Cleworth K Oakden
--	Date: 11/09/2013
--	To present Radiology PbR input to allow validation by Radiology Team
*/
SELECT DISTINCT
	DatasetCode = 'RADIOLOGY',
	SourceUniqueID = max(BOrder.EncounterRecno),
	SourcePatientNo = BOrder.PatientNo,
	SourceEncounterRecno = max(BOrder.EncounterRecno),
	InterfaceCode = 'ICE',
	EncounterTypeCode=
		CASE 
			WHEN LEFT((SUBSTRING(OrderingPhysician.Staff, PATINDEX('%([a-z]%)', OrderingPhysician.Staff) + 1, 
						  LEN(OrderingPhysician.Staff) - CASE patindex('%([a-z]%)', OrderingPhysician.Staff) WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', OrderingPhysician.Staff) 
						  + 1 END)),2)='GP' 
			then 'RADDA' 
			WHEN    (MISYS.lkSpecTrue.SpecialtyTrue='A&E'AND DATEDIFF(hour,border.OrderTime,border.performancetime)<=24 )
				or (border.OrderingLocationCode='AE' AND  DATEDIFF(hour,border.OrderTime,border.performancetime)<=24)
			THEN 'RADAE' 
			WHEN spell.SourceSpellNo IS NOT NULL 
			THEN 'RADIP' 
			else 'RADOP' 
		end
	,EncounterStartDate = cast(BOrder.PerformanceDate as date)
	,EncounterEndDate = cast(BOrder.PerformanceDate as date) 
	,Cases=1
	,Age =
			DATEDIFF(
				year
				,cast(border.DateOfBirth as date)
				,cast(BOrder.PerformanceDate as date)
			)
	,Excluded = cast(0 as bit)
	,CategoryCode = 'ATT'
	,ConsultantCode = ''
	,DateOfBirth = cast(border.DateOfBirth as date)
	,DistrictNo =
			cast(
				replace(border.PatientNo1,';1','')
				as varchar(50)
			)
	,EndSiteCode = 'WYT'
	,HRGCOde=lkhrg.[Proposed Code]
	,LocationTypeCode=
		CASE 
			WHEN LEFT((SUBSTRING(OrderingPhysician.Staff, PATINDEX('%([a-z]%)', OrderingPhysician.Staff) + 1, 
						  LEN(OrderingPhysician.Staff) - CASE patindex('%([a-z]%)', OrderingPhysician.Staff) WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', OrderingPhysician.Staff) 
						  + 1 END)),2)='GP' 
			then 'GP' 
			WHEN    (MISYS.lkSpecTrue.SpecialtyTrue='A&E'AND DATEDIFF(hour,border.OrderTime,border.performancetime)<=24 )
				or (border.OrderingLocationCode='AE' AND  DATEDIFF(hour,border.OrderTime,border.performancetime)<=24)
			THEN 'AE' 
			WHEN spell.SourceSpellNo IS NOT NULL 
			THEN 'IP' 
			else 'OP' 
		end
	,NHSNumber = Replace(cast(BOrder.SocialSecurityNo as varchar(17)),' ','')
	,PCTCode = olapborder.PCTCode
	,CCGCode = Coalesce(PracticeCCG.OrganisationCode, ResidenceCCG.OrganisationCode,'01N')
	,PatientAddress1 = BOrder.PatientAddress1
	,PatientAddress2 = BOrder.PatientAddress2 
	,PatientAddress3 = BOrder.PatientAddress3
	,PatientForename = BOrder.PatientForename
	,PatientSurname = BOrder.PatientSurname
	,Postcode = cast(BOrder.Postcode as varchar(10)) 
	,PrimaryProcedureCode =fin.ExamValue
	,ProviderCode = 'RM2'
	,ReferrerCode = BOrder.OrderingPhysicianCode
	,border.RegisteredGpPracticeCode
	,SexCode = BOrder.PatientGenderCode
	,SpecialtyCode = '810'
	,TreatmentFunctionCode = '810'
	,ServiceCode=lkhrg.[Proposed Code]
	,SLACode = olapborder.PCTCode
	,PODCode = null
	,FrozenDate=Null

FROM MISYS.[Order] AS bOrder 
left join
	(select NHSNo,EthnicGroupNHSCode 
		from WHREPORTING.APC.Patient 
		where EncounterRecno IN 
			(select MAX(encounterrecno) from WHREPORTING.APC.Patient group by NHSNo)
	)ip 
	ON ip.NHSNo = CAST(RTRIM(LEFT(bOrder.SocialSecurityNo, 4)) AS varchar) 
		+ CAST(RTRIM(SUBSTRING(bOrder.SocialSecurityNo, 5, 4)) AS varchar) 
		+ CAST(SUBSTRING(bOrder.SocialSecurityNo, 9, 4) AS varchar)

left join
	(select NHSNo,EthnicGroupNHSCode 
		from WHREPORTING.OP.Patient 
		where EncounterRecno in
			(select MAX(encounterrecno) from WHREPORTING.APC.Patient group by NHSNo)
	)op
	ON op.NHSNo = CAST(RTRIM(LEFT(bOrder.SocialSecurityNo, 4)) AS varchar) 
		+ CAST(RTRIM(SUBSTRING(bOrder.SocialSecurityNo, 5, 4)) AS varchar) 
		+ CAST(SUBSTRING(bOrder.SocialSecurityNo, 9, 4) AS varchar)   
--left outer join MISYS.lk_LocationDescription d on d.pat_loc_code=bOrder.OrderingLocationCode
--left outer join dbo.MaxOrganisationCodes o on o.Organisation_name=d.loc_desc

left join 
	(select lpt.PATNT_REFNO_NHS_IDENTIFIER,lpt.NNNTS_CODE
		from Lorenzo.dbo.Patient lpt
		where lpt.[PATNT_REFNO]in
			(select MAX([PATNT_REFNO]) from Lorenzo.dbo.Patient group by PATNT_REFNO_NHS_IDENTIFIER)
	)lp
	on lp.PATNT_REFNO_NHS_IDENTIFIER=
		rtrim(left(bOrder.SocialSecurityNo,4))
		+rtrim(SUBSTRING(bOrder.SocialSecurityNo,5,4))
		+rtrim(SUBSTRING(bOrder.SocialSecurityNo,9,4))and lp.NNNTS_CODE IS NOT NULL

left join [WHREPORTING].[dbo].[vwPASNHSNoStatus] vpas 
	on vpas.NHSNoStatuslocalCode=lp.NNNTS_CODE

--left outer join Lorenzo.dbo.Patient lptest on lptest.SURNAME=bOrder.PatientSurname and lptest.DTTM_OF_BIRTH=bOrder.DateOfBirth
left join MISYS.ReferrerLookup   rfl 
	on rfl.referrercode=bOrder.OrderingPhysicianCode  
	
left join MISYS.ExamCodeMap cm 
	on cm.Original_exam_code =bOrder.ExamCode1       
	
left join WHOLAP.dbo.OlapMISYSStaff AS OrderingPhysician WITH (nolock) 
	ON OrderingPhysician.StaffCode = BOrder.OrderingPhysicianCode

left join MISYS.lkSpecTrue 
	ON MISYS.lkSpecTrue.SpecialtyOrig = SUBSTRING(OrderingPhysician.Staff, PATINDEX('%([a-z]%)', OrderingPhysician.Staff) + 1, 
                      LEN(OrderingPhysician.Staff) - CASE patindex('%([a-z]%)', OrderingPhysician.Staff) WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', OrderingPhysician.Staff) 
                      + 1 END)   
  
left join APC.Encounter spell
	on spell.NHSNumber=replace(SocialSecurityNo,' ','')
	and PerformanceTime between AdmissionTime and 
		case 
			when performancedate between '2013-05-01' and '2013-05-31' 
			then 
				Case 
					when DischargeTime>'2013-06-10' 
					then null 
					else dischargetime 
				end
			when performancedate between '2013-04-01' and '2013-04-30' 
				then 
					Case 
						when DischargeTime>'2013-06-05' 
						then null 
						else dischargetime 
					end
			else coalesce(dischargetime,getdate()) 
		end

--coalesce(dischargetime,getdate())    

left join [MISYS].[FinanceValue] fin 
	on cast(replace(bOrder.SocialSecurityNo,' ','') as varchar)+cast(Border.casetypecode as varchar)+cast(BOrder.performancedate as varchar)=Fin.RadKey

left join MISYS.hrglookup lkhrg 
	on lkhrg.[Primary Procedure Code]=fin.ExamValue
 --CCG
left join OrganisationCCG.dbo.Practice ODSPractice
	on BOrder.RegisteredGpPracticeCode = ODSPractice.OrganisationCode

left join OrganisationCCG.dbo.CCG PracticeCCG
	on ODSPractice.ParentOrganisationCode = PracticeCCG.OrganisationCode

left join OrganisationCCG.dbo.Postcode Postcode
	on BOrder.Postcode = Postcode.Postcode

left join OrganisationCCG.dbo.CCG ResidenceCCG
	on Postcode.CCGCode = ResidenceCCG.OrganisationCode      

left join WHOLAP.dbo.BaseOrder olapborder 
	on olapborder.OrderNumber=bOrder.OrderNumber
    WHERE (bOrder.PerformanceLocation IS NOT NULL)
	and NOT(bOrder.PatientSurname in 
	('TEST','TESTER','TEST2','TESTPATIENT','XXTEST PATIENT','XXTESTER'))

	and not (bOrder.PatientForename ='Fred' and bOrder.PatientSurname='Flintstone')
	and not (bOrder.PatientForename ='Barney' and bOrder.PatientSurname='Rubble')
	and not (bOrder.PatientForename ='Fred' and bOrder.PatientSurname='Flinstone')
	and not (bOrder.PatientForename ='Dame' and bOrder.PatientSurname='Doody')
	and not (bOrder.PatientForename ='Betty' and bOrder.PatientSurname='Rubble')
	and not (bOrder.PatientForename ='Wilma' and bOrder.PatientSurname='Flintstone')

	and border.OrderingPhysician not like ('Assess%')

	AND border.SocialSecurityNo NOT IN ('0110110110','011 011 0110','5653249999''565 324 9999')

--AND bOrder.ExamCode1 NOT IN ('HBIOL','XBSPER','HBIOM','HCACR','BSPE','XPATHL','HFLUC','XBSPEL','XPATHR','BRMDT')

and border.CaseTypeCode NOT IN 
 ('H','T','Z','O','R','X','Q','V')
AND
border.OrderExamCode NOT IN (
	select [Primary Procedure Code] from MISYS.HRGExclusions
--										'IFACJJ',
--										'FINJTJ',
--										'FHIPLJ',
--										'FHIPRJ',
--										'FJOINJ',
--										'FABDO',
--										'FDIAP',
--										'FLOLB',
--										'FLOLL',
--										'FLOLR',
--										'FLSPN',
--										'FUPLB',
--										'FUPLL',
--										'FUPLR',
--										'FHIPLM',
--										'FHIPRM',
--										'FLSPNM',								
--										'MACOA',
--										'MCVIA',
--										'MCCMS',
--										'MCORP',
--										'MCRPS',
--										'MCSFS',
--										'MCVFS',
--										'MCVVS',
--										'MCORV',
--										'MCARD',
--										'NSENT'
--										,'NSENTL'
--										,'NSENTR'
--										,'BSPE',
--										'CORR',
--										'MAGL',
--'MAL1',
--'MAL2',
--'MAR2',
--'MCCL',
--'MCCR',
--'MTOR',
--'XBSPER',
--'XMACR',
--'XMAMLO',
--'XMMAL',
--'XMPVLB',
--'XMULR',
--'XPATHL',
--'BRMDT',
--'CORL',
--'MAGR',
--'MTOL',
--'XMACL',
--'XMAML',
--'XMAMRI',
--'XMUBBB',
--'XMULB',
--'MAR1',
--'MNLR',
--'XBSPEL',
--'XMACB',
--'XMAMB',
--'XMAMRO',
--'XMMAB',
--'XMMAR',
--'XMPVBB',
--'XMUBLB',
--'XMULL',
--'MAB2',
--'MNLL',
--'XMAMBO',
--'XMAMLI',
--'XMAMR',
--'XMPVRB',
--'XMUBRB',
--'XPATHR',
--'FCSPNM',
--'EVCAP'
--,'MAAOT'
--,'FBAEN'
--,'YSBCE'
--,'FPAC'
--,'FTSPNM'
)

					
--AND

--CASE WHEN LEFT((SUBSTRING(OrderingPhysician.Staff, PATINDEX('%([a-z]%)', OrderingPhysician.Staff) + 1, 
--                      LEN(OrderingPhysician.Staff) - CASE patindex('%([a-z]%)', OrderingPhysician.Staff) WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', OrderingPhysician.Staff) 
--                      + 1 END)),2)='GP' then 'RADDA' 
--  WHEN    (MISYS.lkSpecTrue.SpecialtyTrue='A&E'AND DATEDIFF(hour,border.OrderTime,border.performancetime)<=24 )
-- or (border.OrderingLocationCode='AE' AND  DATEDIFF(hour,border.OrderTime,border.performancetime)<=24) THEN 'RADAE'                 
                      
--  WHEN spell.SourceSpellNo IS NOT NULL THEN 'RADIP'                        
                      
--            else 'RADOP' 
--            end in (@EncounterType)
            


--and CAST(bOrder.PerformanceDate AS DATE)>=@StartDate
--and CAST(bOrder.PerformanceDate AS DATE)<=@EndDate
 and cast(border.performancedate as date) >(select max(encounterstartdate) from pbr2013.dbo.RadiologyFixed)

group by
	LKHRG.[Proposed Code],
	fin.ExamValue,
	border.socialsecurityno,
	border.casetypecode,
	border.performancedate,
	BOrder.PatientNo,
		CASE 
			WHEN LEFT((SUBSTRING(OrderingPhysician.Staff, PATINDEX('%([a-z]%)', OrderingPhysician.Staff) + 1, 
						  LEN(OrderingPhysician.Staff) - CASE patindex('%([a-z]%)', OrderingPhysician.Staff) WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', OrderingPhysician.Staff) 
						  + 1 END)),2)='GP' 
			then 'RADDA' 
			WHEN   (MISYS.lkSpecTrue.SpecialtyTrue='A&E'AND DATEDIFF(hour,border.OrderTime,border.performancetime)<=24 )
				or (border.OrderingLocationCode='AE' AND  DATEDIFF(hour,border.OrderTime,border.performancetime)<=24)
			THEN 'RADAE'                 
			WHEN spell.SourceSpellNo IS NOT NULL 
			THEN 'RADIP'                        
			else 'RADOP' 
		end,
	orderingphysician.Staff,
		CASE 
			WHEN LEFT((SUBSTRING(OrderingPhysician.Staff, PATINDEX('%([a-z]%)', OrderingPhysician.Staff) + 1, 
                      LEN(OrderingPhysician.Staff) - CASE patindex('%([a-z]%)', OrderingPhysician.Staff) WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', OrderingPhysician.Staff) 
                      + 1 END)),2)='GP' 
            then 'GP' else
                      
                

MISYS.lkSpecTrue.SpecialtyTrue end 
,DATEDIFF(
			year
			,cast(border.DateOfBirth as date)
			,cast(BOrder.PerformanceDate as date)
		)
,cast(border.DateOfBirth as date)
,cast(
			replace(border.PatientNo1,';1','')
			as varchar(50)
		)
,Replace(cast(BOrder.SocialSecurityNo as varchar(17)),' ','')
,Coalesce(PracticeCCG.OrganisationCode, ResidenceCCG.OrganisationCode,'01N')
,BOrder.PatientAddress1
,BOrder.PatientAddress2 
,BOrder.PatientAddress3
,BOrder.PatientForename
,BOrder.PatientSurname
,cast(BOrder.Postcode as varchar(10)) 
,BOrder.OrderingPhysicianCode
,BOrder.PatientGenderCode
,cast(BOrder.PerformanceDate as date)
,cast(BOrder.PerformanceDate as date) 
,olapborder.PCTCode
,bOrder.RegisteredGpPracticeCode
,CASE WHEN LEFT((SUBSTRING(OrderingPhysician.Staff, PATINDEX('%([a-z]%)', OrderingPhysician.Staff) + 1, 
                      LEN(OrderingPhysician.Staff) - CASE patindex('%([a-z]%)', OrderingPhysician.Staff) WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', OrderingPhysician.Staff) 
                      + 1 END)),2)='GP' then 'GP' 
  WHEN   (MISYS.lkSpecTrue.SpecialtyTrue='A&E'AND DATEDIFF(hour,border.OrderTime,border.performancetime)<=24 )
 or (border.OrderingLocationCode='AE' AND  DATEDIFF(hour,border.OrderTime,border.performancetime)<=24)
  THEN 'AE' WHEN spell.SourceSpellNo IS NOT NULL THEN 'IP' else 'OP' end



union select * from pbr2013.dbo.RadiologyFixed
