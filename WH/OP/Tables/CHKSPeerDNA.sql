﻿CREATE TABLE [OP].[CHKSPeerDNA] (
    [ActivityDate]    DATETIME     NOT NULL,
    [SpecialtyCode]   VARCHAR (5)  NOT NULL,
    [AppointmentType] VARCHAR (20) NOT NULL,
    [PeerAttendances] INT          NULL,
    [PeerDNA]         INT          NULL,
    CONSTRAINT [pk_OP_CHKSPeerDNA] PRIMARY KEY CLUSTERED ([ActivityDate] ASC, [SpecialtyCode] ASC, [AppointmentType] ASC)
);

