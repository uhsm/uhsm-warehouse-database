﻿CREATE TABLE [OP].[CHKSPeerNewFup] (
    [ActivityDate]            DATETIME    NOT NULL,
    [SpecialtyCode]           VARCHAR (5) NOT NULL,
    [PeerFollowUpAttendances] INT         NULL,
    [PeerNewAttendances]      INT         NULL,
    CONSTRAINT [pk_OP_CHKSPeerNewFup] PRIMARY KEY CLUSTERED ([ActivityDate] ASC, [SpecialtyCode] ASC)
);

