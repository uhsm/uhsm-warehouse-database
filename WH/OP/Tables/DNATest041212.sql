﻿CREATE TABLE [OP].[DNATest041212] (
    [AppointmentTime]           SMALLDATETIME NULL,
    [DistrictNo]                VARCHAR (20)  NULL,
    [AppointmentStatusCode]     INT           NULL,
    [AppointmentStatusValue]    VARCHAR (80)  NULL,
    [ScheduledCancelReasonCode] INT           NULL,
    [MappedCode]                VARCHAR (50)  NULL,
    [ReferenceValue]            VARCHAR (80)  NULL,
    [DNACodeOld]                CHAR (1)      NULL,
    [DNACode]                   VARCHAR (1)   NOT NULL
);

