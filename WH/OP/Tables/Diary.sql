﻿CREATE TABLE [OP].[Diary] (
    [SourceUniqueID]           VARCHAR (254) NOT NULL,
    [ClinicCode]               VARCHAR (8)   NOT NULL,
    [SessionCode]              VARCHAR (8)   NOT NULL,
    [SessionDescription]       VARCHAR (255) NULL,
    [SessionDate]              SMALLDATETIME NOT NULL,
    [SessionStartTime]         SMALLDATETIME NULL,
    [SessionEndTime]           SMALLDATETIME NULL,
    [ReasonForCancellation]    VARCHAR (30)  NULL,
    [SessionPeriod]            VARCHAR (3)   NULL,
    [DoctorCode]               VARCHAR (10)  NULL,
    [Units]                    INT           NULL,
    [UsedUnits]                INT           NULL,
    [FreeUnits]                INT           NULL,
    [ValidAppointmentTypeCode] VARCHAR (23)  NULL,
    [InterfaceCode]            VARCHAR (5)   NULL,
    [Created]                  DATETIME      NULL,
    [Updated]                  DATETIME      NULL,
    [ByWhom]                   VARCHAR (50)  NULL,
    CONSTRAINT [PK_Diary] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);

