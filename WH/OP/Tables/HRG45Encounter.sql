﻿CREATE TABLE [OP].[HRG45Encounter] (
    [EncounterRecno]        INT          NOT NULL,
    [HRGCode]               VARCHAR (10) NULL,
    [GroupingMethodFlag]    VARCHAR (10) NULL,
    [DominantOperationCode] VARCHAR (10) NULL,
    [Created]               DATETIME     NULL,
    CONSTRAINT [PK__HRG45Attendance] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);

