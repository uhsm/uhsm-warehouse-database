﻿CREATE TABLE [OP].[HRG45Unbundled] (
    [EncounterRecno] INT          NOT NULL,
    [SequenceNo]     INT          NOT NULL,
    [HRGCode]        VARCHAR (10) NOT NULL,
    CONSTRAINT [PK__HRG45Unbundled] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC, [SequenceNo] ASC)
);

