﻿CREATE TABLE [OP].[Rule] (
    [RuleRecno]           INT          IDENTITY (1, 1) NOT NULL,
    [RuleUniqueID]        INT          NOT NULL,
    [AppliedTo]           VARCHAR (5)  NULL,
    [AppliedToUniqueID]   INT          NULL,
    [EnforcedFlag]        CHAR (1)     NULL,
    [RuleAppliedUniqueID] INT          NULL,
    [RuleValueCode]       VARCHAR (20) NULL,
    [ArchiveFlag]         BIT          NULL,
    [PASCreated]          DATETIME     NULL,
    [PASUpdated]          DATETIME     NULL,
    [PASCreatedByWhom]    VARCHAR (30) NULL,
    [PASUpdatedByWhom]    VARCHAR (30) NULL,
    [Created]             DATETIME     NULL,
    [updated]             DATETIME     NULL,
    [ByWhom]              VARCHAR (50) NULL,
    CONSTRAINT [pk_OP_Rules] PRIMARY KEY NONCLUSTERED ([RuleUniqueID] ASC)
);


GO
CREATE CLUSTERED INDEX [IX_1_OP_Rule]
    ON [OP].[Rule]([AppliedTo] ASC, [EnforcedFlag] ASC, [RuleAppliedUniqueID] ASC, [ArchiveFlag] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_OP_Rule_1]
    ON [OP].[Rule]([AppliedTo] ASC, [AppliedToUniqueID] ASC);


GO
CREATE NONCLUSTERED INDEX [ixOPRule]
    ON [OP].[Rule]([AppliedTo] ASC, [RuleAppliedUniqueID] ASC)
    INCLUDE([RuleUniqueID], [AppliedToUniqueID], [RuleValueCode]);

