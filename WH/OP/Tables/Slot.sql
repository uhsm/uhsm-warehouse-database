﻿CREATE TABLE [OP].[Slot] (
    [SlotRecNo]               INT           IDENTITY (1, 1) NOT NULL,
    [SlotUniqueID]            INT           NOT NULL,
    [SessionUniqueID]         INT           NULL,
    [ServicePointUniqueID]    INT           NULL,
    [SlotDate]                DATE          NULL,
    [SlotStartTime]           TIME (7)      NULL,
    [SlotStartDateTime]       SMALLDATETIME NULL,
    [SlotEndTime]             TIME (7)      NULL,
    [SlotEndDateTime]         SMALLDATETIME NULL,
    [SlotDuration]            INT           NULL,
    [SlotStatusCode]          INT           NULL,
    [SlotChangeReasonCode]    INT           NULL,
    [SlotChangeRequestByCode] INT           NULL,
    [SlotPublishedToEBS]      CHAR (1)      NULL,
    [SlotMaxBookings]         INT           NULL,
    [SlotNumberofBookings]    INT           NULL,
    [SlotTemplateUnqiueID]    INT           NULL,
    [IsTemplate]              CHAR (1)      NULL,
    [SlotCancelledFlag]       BIT           NULL,
    [SlotCancelledDate]       DATETIME      NULL,
    [SlotInstructions]        VARCHAR (255) NULL,
    [ArchiveFlag]             BIT           NULL,
    [PASCreated]              DATETIME      NULL,
    [PASUpdated]              DATETIME      NULL,
    [PASCreatedByWhom]        VARCHAR (30)  NULL,
    [PASUpdatedByWhom]        VARCHAR (30)  NULL,
    [Created]                 DATETIME      NOT NULL,
    [Updated]                 DATETIME      NOT NULL,
    [ByWhom]                  VARCHAR (50)  NOT NULL,
    CONSTRAINT [pk_SlotReference] PRIMARY KEY CLUSTERED ([SlotUniqueID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_SLotReferenceDate]
    ON [OP].[Slot]([SlotDate] ASC);


GO
CREATE NONCLUSTERED INDEX [ixOPSlotArchive]
    ON [OP].[Slot]([IsTemplate] ASC, [ArchiveFlag] ASC)
    INCLUDE([SlotUniqueID], [SessionUniqueID], [ServicePointUniqueID], [SlotDate], [SlotStartTime], [SlotEndTime], [SlotDuration], [SlotStatusCode], [SlotChangeReasonCode], [SlotChangeRequestByCode], [SlotMaxBookings], [SlotNumberofBookings], [SlotCancelledFlag], [SlotCancelledDate]);


GO
CREATE NONCLUSTERED INDEX [ixOPSlotStatus]
    ON [OP].[Slot]([SlotCancelledFlag] ASC, [ArchiveFlag] ASC, [SlotStatusCode] ASC)
    INCLUDE([SlotUniqueID], [SessionUniqueID], [SlotMaxBookings], [SlotNumberofBookings]);


GO
CREATE NONCLUSTERED INDEX [IX1_OP_Slot]
    ON [OP].[Slot]([SlotStatusCode] ASC)
    INCLUDE([SlotUniqueID], [SessionUniqueID], [ServicePointUniqueID], [SlotMaxBookings], [SlotNumberofBookings]);

