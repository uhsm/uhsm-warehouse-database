﻿CREATE TABLE [OP].[Snapshot] (
    [CensusDate] SMALLDATETIME NOT NULL,
    CONSTRAINT [PK_OP_Snapshot] PRIMARY KEY CLUSTERED ([CensusDate] ASC)
);

