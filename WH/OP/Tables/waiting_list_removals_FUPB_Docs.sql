﻿CREATE TABLE [OP].[waiting_list_removals_FUPB_Docs] (
    [PatientRefno]     INT           NULL,
    [TemplateLocation] VARCHAR (255) NULL,
    [TemplateName]     VARCHAR (255) NULL,
    [Queued]           VARCHAR (17)  NOT NULL,
    [Printed]          VARCHAR (1)   NOT NULL,
    [UserCreateCode]   VARCHAR (30)  NOT NULL,
    [UserModifCode]    VARCHAR (30)  NOT NULL,
    [UserCreate]       VARCHAR (35)  NULL,
    [UserModif]        VARCHAR (35)  NULL
);

