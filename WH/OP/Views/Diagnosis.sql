﻿create view OP.[Diagnosis] as

select
	 SourceUniqueID = DGPRO_REFNO
	,SequenceNo = SORT_ORDER
	,SourceDiagnosisCode = ODPCD_REFNO
	,DiagnosisCode = CODE
	,DiagnosisDate = DGPRO_DTTM
	,OPSourceUniqueID = SORCE_REFNO
from
	PAS.ClinicalCodingBase
where
	SORCE_CODE = 'SCHDL'
and	DPTYP_CODE = 'DIAGN'
and	ARCHV_FLAG = 'N'

