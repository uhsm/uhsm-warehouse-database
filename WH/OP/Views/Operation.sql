﻿CREATE view [OP].[Operation] as

select
	 SourceUniqueID = DGPRO_REFNO
	,SequenceNo = SORT_ORDER
	,SourceOperationCode = ODPCD_REFNO
	,OperationCode = CODE
	,OperationDate = DGPRO_DTTM
	,OPSourceUniqueID = SORCE_REFNO
from
	PAS.ClinicalCodingBase
where
	SORCE_CODE = 'SCHDL'
and	DPTYP_CODE = 'PROCE'
and	ARCHV_FLAG = 'N'
