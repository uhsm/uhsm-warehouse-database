﻿Create View OP.SessionOverbooking as 
Select
	
	 SessionUniqueID = OPRule.AppliedToUniqueID
	,Overbooking = RuleValue.DESCRIPTION

from OP.[Rule] OPRule
Left Outer Join PAS.RuleBase RuleBase
on OPRule.RuleAppliedUniqueID = RuleBase.RULES_REFNO
left outer join PAS.ReferenceValueBase RuleValue
on OPRule.RuleValueCode = RuleValue.RFVAL_REFNO
Where AppliedTo = 'SPSSN'
and OPRule.RuleAppliedUniqueID = 9
--Group BY AppliedToUniqueID
