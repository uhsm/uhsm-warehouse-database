﻿
CREATE View [OP].[SlotMaxAppts] as 
Select
	
	 SlotUniqueID = OPRule.AppliedToUniqueID
	,MaxAppts = RuleValue.DESCRIPTION
from OP.[Rule] OPRule
Left Outer Join PAS.RuleBase RuleBase
on OPRule.RuleAppliedUniqueID = RuleBase.RULES_REFNO
left outer join PAS.ReferenceValueBase RuleValue
on OPRule.RuleValueCode = RuleValue.RFVAL_REFNO
Where AppliedTo = 'SPSLT'
and OPRule.RuleAppliedUniqueID = 6
and not exists
	(Select 1 from OP.[Rule] NextRule
	Where 
	OPRule.AppliedTo = NextRule.AppliedTo
	and OPRule.AppliedToUniqueID = NextRule.AppliedToUniqueID
	and OPRule.RuleAppliedUniqueID = NextRule.RuleAppliedUniqueID
	and OPRule.RuleUniqueID < NextRule.RuleUniqueID
	)

