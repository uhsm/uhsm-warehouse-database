﻿

CREATE View [OP].[SlotPriorityAndVisitType] as 
Select
	
	SlotUniqueID = OPRule.AppliedToUniqueID
	--Priority
	,[2 Week Rule] = Count(Case When OPRule.RuleValueCode = 5478 Then 1 End)
	,[Accident & Emergency] = Count(Case When OPRule.RuleValueCode = 2000632 Then 1 End)
	,[Priority Not Specified] = Count(Case When OPRule.RuleValueCode = 812 Then 1 End)
	,[Routine] = Count(Case When OPRule.RuleValueCode = 3127 Then 1 End)
	,[Urgent] = Count(Case When OPRule.RuleValueCode = 3128 Then 1 End)
	--Visit Type
	--,[A&E attendance] = Count(Case When OPRule.RuleValueCode = 2002180 Then 1 End)
	--,[Follow Up] = Count(Case When OPRule.RuleValueCode = 8911 Then 1 End)
	--,[New] = Count(Case When OPRule.RuleValueCode = 9268 Then 1 End)
	--,[Visit Type Not Specified] = Count(Case When OPRule.RuleValueCode = 1288 Then 1 End)
	--,[Panel] = Count(Case When OPRule.RuleValueCode = 2003648 Then 1 End)
	--,[Walk-in] = Count(Case When OPRule.RuleValueCode = 2003439 Then 1 End)
	--,[Walk-in A&E] = Count(Case When OPRule.RuleValueCode = 2003440 Then 1 End)
	--,[Walk-in Follow Up] = Count(Case When OPRule.RuleValueCode = 5809 Then 1 End)
	--,[Walk-in New] = Count(Case When OPRule.RuleValueCode = 5808 Then 1 End)
	,[New] = SUM(
				Case When 
				(OPRule.RuleValueCode = 9268			-- New
				Or OPRule.RuleValueCode = 5808			-- Walk In New
				Or OPRule.RuleValueCode = 2002180		-- A&E Attendance
				Or OPRule.RuleValueCode = 2003439		-- Walk In
				Or OPRule.RuleValueCode = 2003440)		-- Walk In A&E
				Then 1 
				Else 0
				End
				)
	,Review = SUM(
				Case When 
				(OPRule.RuleValueCode =  8911			-- Follow Up
				Or OPRule.RuleValueCode = 2003648		-- Panel 
				Or OPRule.RuleValueCode = 5809)			-- Walk In Follow Up
				Then 1 
				Else 0
				End
				)
	,[Not Specified] = SUM(
				Case When 
				OPRule.RuleValueCode =  1288			-- Not Specified
				Then 1 
				Else 0
				End
				)

from OP.[Rule] OPRule
Left Outer Join PAS.RuleBase RuleBase
on OPRule.RuleAppliedUniqueID = RuleBase.RULES_REFNO
left outer join PAS.ReferenceValueBase RuleValue
on OPRule.RuleValueCode = RuleValue.RFVAL_REFNO
Where AppliedTo = 'SPSLT'
and OPRule.RuleAppliedUniqueID in (14,10)
Group By 
AppliedToUniqueID



