﻿CREATE procedure [PAS].[BuildReferenceValueCheck] as

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PAS].[ReferenceValueCheck]') AND type in (N'U'))
DROP TABLE [PAS].[ReferenceValueCheck]

CREATE TABLE [PAS].[ReferenceValueCheck](
	[rfval] [int] NULL,
	[domain] [varchar](10) NOT NULL,
	[src] [varchar](20) NOT NULL
) 

	insert into PAS.ReferenceValueCheck 
	select distinct SourceOfReferralCode as rfval, 'SORRF' as domain, 'RF.Encounter' as src
	from RF.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct SourceOfReferralCode, 'SORRF', 'OP.Encounter'
	from OP.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct AdminCategoryCode, 'ADCAT', 'OP.Encounter'
	from OP.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct AdmissionMethodCode, 'ADMET', 'APC.Encounter'
	from APC.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct AdmissionSourceCode, 'ADSOR', 'APC.Encounter'
	from APC.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct DischargeDestinationCode, 'DISDE', 'APC.Encounter'
	from APC.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct DischargeMethodCode, 'DISMT', 'APC.Encounter'
	from APC.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct EthnicOriginCode, 'ETHGR', 'APC.Encounter'
	from APC.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct EthnicOriginCode, 'ETHGR', 'OP.Encounter'
	from OP.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct EthnicOriginCode, 'ETHGR', 'RF.Encounter'
	from RF.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct ManagementIntentionCode, 'INMGT', 'APC.Encounter'
	from APC.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct MaritalStatusCode, 'MARRY', 'APC.Encounter'
	from APC.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct MaritalStatusCode, 'MARRY', 'OP.Encounter'
	from OP.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct MaritalStatusCode, 'MARRY', 'RF.Encounter'
	from RF.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct refval.ReferenceValueCode, 'NNNTS', 'APC.Encounter'
	--,enc.NHSNumberStatusCode
	from APC.Encounter enc
	inner join PAS.ReferenceValue refval 
	on refval.MainCode = enc.NHSNumberStatusCode
	and refval.ReferenceDomainCode = 'NNNTS'

	insert into PAS.ReferenceValueCheck 
	select distinct refval.ReferenceValueCode, 'NNNTS', 'OP.Encounter'
	--,enc.NHSNumberStatusCode
	from OP.Encounter enc
	inner join PAS.ReferenceValue refval 
	on refval.MainCode = enc.NHSNumberStatusCode
	and refval.ReferenceDomainCode = 'NNNTS'

	insert into PAS.ReferenceValueCheck 
	select distinct OverseasStatusFlag, 'OVSVS', 'APC.Encounter'
	from APC.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct OverseasStatusFlag, 'OVSVS', 'OP.Encounter'
	from OP.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct CANRS_REFNO, 'CANRS', 'Lorenzo.dbo.Referral'
	from Lorenzo.dbo.Referral

	insert into PAS.ReferenceValueCheck 
	select distinct CLORS_REFNO, 'CLORS', 'Lorenzo.dbo.Referral'
	from Lorenzo.dbo.Referral

	insert into PAS.ReferenceValueCheck 
	select distinct RFMED_REFNO, 'RFMED', 'Lorenzo.dbo.Referral'
	from Lorenzo.dbo.Referral

	insert into PAS.ReferenceValueCheck 
	select distinct PriorityCode, 'PRITY', 'RF.Encounter'
	from RF.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct REASN_REFNO, 'REASN', 'Lorenzo.dbo.Referral'
	from Lorenzo.dbo.Referral

	insert into PAS.ReferenceValueCheck 
	select distinct ACTYP_REFNO, 'ACTYP', 'Lorenzo.dbo.Referral'
	from Lorenzo.dbo.Referral


	insert into PAS.ReferenceValueCheck 
	select distinct ReligionCode, 'RELIG', 'APC.Encounter'
	from APC.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct ReligionCode, 'RELIG', 'OP.Encounter'
	from OP.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct ReligionCode, 'RELIG', 'RF.Encounter'
	from RF.Encounter

	--delete from PAS.ReferenceValueCheck where domain = 'RTTST'
	insert into PAS.ReferenceValueCheck 
	select distinct RTTCurrentStatusCode, 'RTTST', 'APC.Encounter'
	from APC.Encounter
	union select distinct RTTPeriodStatusCode, 'RTTST', 'APC.Encounter'
	from APC.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct RTTCurrentStatusCode, 'RTTST', 'OP.Encounter'
	from OP.Encounter
	union select distinct RTTPeriodStatusCode, 'RTTST', 'OP.Encounter'
	from OP.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct RTTCurrentStatusCode, 'RTTST', 'RF.Encounter'
	from RF.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct AppointmentStatusCode, 'ATTND', 'OP.Encounter'
	from OP.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct CancelledByCode, 'CANCB', 'OP.Encounter'
	from OP.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct DisposalCode, 'SCOCM', 'OP.Encounter'
	from OP.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct ClinicType, 'PRTYP', 'OP.Encounter'
	from OP.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct FirstAttendanceFlag, 'VISIT', 'OP.Encounter'
	from OP.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct SexCode, 'SEXXX', 'APC.Encounter'
	from APC.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct SexCode, 'SEXXX', 'OP.Encounter'
	from OP.Encounter

	insert into PAS.ReferenceValueCheck 
	select distinct SexCode, 'SEXXX', 'RF.Encounter'
	from RF.Encounter
