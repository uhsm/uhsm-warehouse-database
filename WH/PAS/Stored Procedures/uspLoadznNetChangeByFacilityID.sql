﻿/*
-------------------------------------------------------------------------------------
Purpose:		Load znNetChangeByFacilityID

Notes:			Stored in PAS

Versions:		
				1.0.0.2 - 29/01/2016 - MT
					Guarding against this sproc failing.
					Inserting GETDATE() into LastUpdateDateTime rather then
					LastUpdatedDateTime from znPatients as this will ensure
					that even if thsi sproc fails the next time it runs
					all changes will be picked up and passed to downstream
					loads.
					Also removed the section that updates delted patients -
					not necessary as deleted patients will already have had
					their LastUpdatedDateTime updated in znPatients so the
					update existing code will take care of them.

				1.0.0.1 - 19/01/2016 - MT
					Leave deleted and excluded patients in the net change table
					so that any changes to them are picked up in downstream loads
					and records removed but the downstream loads will not re-load them.

				1.0.0.0 - 18/01/2016 - MT
					Created sproc.
-------------------------------------------------------------------------------------
*/
CREATE Procedure PAS.uspLoadznNetChangeByFacilityID
As

Begin Try

-- Create and load a temp table with patient details at FacilityID level.
-- Using a temp table rather than a CTE because it's used 3 times.
Declare @Patients Table (
	FacilityID varchar(20) Not Null Primary Key,
	LastUpdatedDateTime datetime Not Null
	)

;With MyPID As (
	Select	PATNT_REFNO = src.PATNT_REFNO
			,LocalPatientIdentifier = src.IDENTIFIER
			,ROW_NUMBER() Over (Partition By src.PATNT_REFNO Order By src.START_DTTM Desc,src.PATID_REFNO Desc) As RowNo
	From	Lorenzo.dbo.PatientIdentifier src With (NoLock)
	Where	src.IDENTIFIER Like 'RM2%'
			And src.PITYP_REFNO = 2001232 -- Patient ID (Facility)
	)
Insert Into @Patients(FacilityID,LastUpdatedDateTime)
Select	pid.LocalPatientIdentifier,
		Max(src.LastUpdatedDateTime)
From	PAS.znNetChangeByPatient src

		Inner Join MyPID pid
			On src.PATNT_REFNO = pid.PATNT_REFNO
			And pid.RowNo = 1

Group By pid.LocalPatientIdentifier

-- Add any new patients
Insert Into PAS.znNetChangeByFacilityID(FacilityID,LastUpdatedDateTime)
Select	src.FacilityID,GETDATE()
From	@Patients src

		Left Join PAS.znNetChangeByFacilityID zn
			On src.FacilityID = zn.FacilityID
			
Where	zn.FacilityID Is Null

-- Update existing
Update	src
Set		LastUpdatedDateTime = GETDATE()
From	PAS.znNetChangeByFacilityID src

		Inner Join @Patients pat
			On src.FacilityID = pat.FacilityID
			
Where	src.LastUpdatedDateTime < pat.LastUpdatedDateTime

End Try

Begin Catch
	Exec msdb.dbo.sp_send_dbmail
	@profile_name = 'Business Intelligence',
	@recipients = 'Business.Intelligence@uhsm.nhs.uk',
	@subject = 'Load znNetChangeByFacilityID',
	@body_format = 'HTML',
	@body = 'PAS.uspLoadznNetChangeByFacilityID failed.<br><br>';
End Catch
