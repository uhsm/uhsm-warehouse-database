﻿/*
-------------------------------------------------------------------------------------
Purpose:		Load znNetChangeByPatient

Notes:			Stored in PAS

Versions:		
				1.0.0.2 - 29/01/2016 - MT
					There is the possibility that this sproc fails and in that instance
					downstream loads that use this sproc will not pick up all the possible
					changes.
					Modifying so that this sproc as follows:
					1.	Add a LastCreatedDateTime field and populate  using the same logic as
						previously used for LastUpdatedDateTime.
					2.	Populate LastUpdatedDateTime with GETDATE() whenever LastCreatedDateTime
						is changed.
					In this way, if the load fails, it will update the LastUpdateDateTime
					the next time around and downstream loads will still pick up the changes.
					
					Also added a DeletedFlag and a TestFlag so that I can track changes to these
					patients and update the table as necessary.

				1.0.0.1 - 19/01/2016 - MT
					Leave deleted and excluded patients in the net change table
					so that any changes to them are picked up in downstream loads
					and records removed but the downstream loads will not re-load them.
					
				1.0.0.0 - 13/01/2016 - MT
					Created sproc.
-------------------------------------------------------------------------------------
*/
CREATE Procedure PAS.uspLoadznNetChangeByPatient
As

Begin Try

Declare @RecordID int
Declare @TableName varchar(100)
Declare @ParentTable varchar(100)
Declare @PrimaryKey varchar(100)
Declare @SQL varchar(MAX)

Declare @Tables Table (
	RecordID int Identity(1,1) Not Null,
	TableName varchar(100) Not Null
	)

-- Add any new patients
Insert Into PAS.znNetChangeByPatient(PATNT_REFNO,LastCreatedDateTime,LastUpdatedDateTime,LatestActivity)
Select	src.PATNT_REFNO,src.Created,GETDATE(),'Patient registration'
From	Lorenzo.dbo.Patient src

		Left Join PAS.znNetChangeByPatient zn
			On src.PATNT_REFNO = zn.PATNT_REFNO
			
Where	zn.PATNT_REFNO Is Null

-- Update any deleted patients
If (Select COUNT(*) From Lorenzo.dbo.Patient) > 1000000
Begin
	Update	src
	Set		DeletedFlag = 'Y',
			LastUpdatedDateTime = GETDATE()
	From	PAS.znNetChangeByPatient src

			Left Join Lorenzo.dbo.Patient pat
				On src.PATNT_REFNO = pat.PATNT_REFNO
				And pat.ARCHV_FLAG = 'N'

	Where	pat.PATNT_REFNO Is Null
			And src.DeletedFlag = 'N'
End

-- Update any un-deleted patients
If (Select COUNT(*) From Lorenzo.dbo.Patient) > 1000000
Begin
	Update	src
	Set		DeletedFlag = 'N',
			LastUpdatedDateTime = GETDATE()
	From	PAS.znNetChangeByPatient src

			Inner Join Lorenzo.dbo.Patient pat
				On src.PATNT_REFNO = pat.PATNT_REFNO
				And pat.ARCHV_FLAG = 'N'

	Where	src.DeletedFlag = 'Y'
End

-- Update any test patients
Update	src
Set		TestFlag = 'Y',
		LastUpdatedDateTime = GETDATE()
From	PAS.znNetChangeByPatient src

		Inner Join Lorenzo.dbo.ExcludedPatient ex
			On src.PATNT_REFNO = ex.SourcePatientNo

Where	src.TestFlag = 'N'

-- Update any test patients that are now not test patients
If (Select COUNT(*) From Lorenzo.dbo.ExcludedPatient) > 100
Begin
	Update	src
	Set		TestFlag = 'N',
			LastUpdatedDateTime = GETDATE()
	From	PAS.znNetChangeByPatient src

			Left Join Lorenzo.dbo.ExcludedPatient ex
				On src.PATNT_REFNO = ex.SourcePatientNo

	Where	src.TestFlag = 'Y'
			And ex.SourcePatientNo Is Null
End

-- Loop through each relevant Lorenzo table and update the net change
-- field LastUpdatedDateTime accordingly.

	-- Populate a temp table with tables that contain PATNT_REFNO
	Insert Into @Tables(TableName)
	Select	src.TableName
	From	PAS.LorenzoTables src
	Where	src.IncludeForNetChangeByPatient = 'Y'
			And src.HasPATNT_REFNO = 'Y'
	
	Set @RecordID = 1
	While @RecordID <= (Select Max(RecordID) From @Tables)
	Begin

		Set @TableName = (Select TableName From @Tables Where RecordID = @RecordID)

		Set @SQL =
			'With MaxCreated As (' +
			' Select src.PATNT_REFNO,Max(src.Created) As MaxCreated' +
			' From Lorenzo.dbo.' + @TableName + ' src' +
			' Group By src.PATNT_REFNO)' +
			' Update src' +
			' Set LastCreatedDateTime = mc.MaxCreated,' +
			' LastUpdatedDateTime = GETDATE(),' +
			' LatestActivity = ' + '''' + @TableName + '''' +
			' From PAS.znNetChangeByPatient src' +
			' Inner Join MaxCreated mc' +
			' On src.PATNT_REFNO = mc.PATNT_REFNO' +
			' Where mc.MaxCreated > src.LastCreatedDateTime'
			
		Exec(@SQL)
	
		Set @RecordID = @RecordID + 1
	End
	
	-- Now for the tables that do not have PATNT_REFNO but have a parent that does.
	Delete From @Tables
	
	Insert Into @Tables(TableName)
	Select	src.TableName
	From	PAS.LorenzoTables src
			Inner Join PAS.LorenzoTables t
				On src.ParentTable = t.TableName
				And t.HasPATNT_REFNO = 'Y'
	Where	src.IncludeForNetChangeByPatient = 'Y'

	Set @RecordID = (Select Min(RecordID) From @Tables)
	While @RecordID <= (Select Max(RecordID) From @Tables)
	Begin

		Set @TableName = (Select TableName From @Tables Where RecordID = @RecordID)
		Set @ParentTable = (Select ParentTable From PAS.LorenzoTables Where TableName = @TableName)
		Set @PrimaryKey = (Select PrimaryKey From PAS.LorenzoTables Where TableName = @ParentTable)
		
		Set @SQL =
			'With MaxCreated As (' +
			' Select pt.PATNT_REFNO,Max(src.Created) As MaxCreated' +
			' From Lorenzo.dbo.' + @TableName + ' src' +
			' Inner Join Lorenzo.dbo.' + @ParentTable + ' pt' +
			' On src.' + @PrimaryKey + ' = pt.' + @PrimaryKey +
			' Group By pt.PATNT_REFNO)' +
			' Update src' +
			' Set LastCreatedDateTime = mc.MaxCreated,' +
			' LastUpdatedDateTime = GETDATE(),' +
			' LatestActivity = ' + '''' + @TableName + '''' +
			' From PAS.znNetChangeByPatient src' +
			' Inner Join MaxCreated mc' +
			' On src.PATNT_REFNO = mc.PATNT_REFNO' +
			' Where mc.MaxCreated > src.LastCreatedDateTime'
			
		Exec(@SQL)
	
		Set @RecordID = @RecordID + 1
	End

End Try

Begin Catch
	Exec msdb.dbo.sp_send_dbmail
	@profile_name = 'Business Intelligence',
	@recipients = 'Business.Intelligence@uhsm.nhs.uk',
	@subject = 'Load znNetChangeByPatient',
	@body_format = 'HTML',
	@body = 'PAS.uspLoadznNetChangeByPatient failed.<br><br>';
End Catch
