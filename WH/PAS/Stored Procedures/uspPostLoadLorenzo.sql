﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		To run all post load Lorenzo sprocs.

Notes:			Stored in PAS

Versions:		
				1.0.0.0 - 21/01/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure PAS.uspPostLoadLorenzo
As

Begin Try

Declare @LastLorenzoRunDateTime datetime
Declare @LastRunDateTime datetime
Declare @StartTime datetime

Set @LastLorenzoRunDateTime = (Select MAX(EventTime) From AuditLog Where ProcessCode = 'PAS - Load Lorenzo Data - Rows Affected')
Set @LastRunDateTime = (Select MAX(EventTime) From AuditLog Where ProcessCode = 'PAS - Lorenzo Post Load')
Set @StartTime = GETDATE()

-- Only run if the Lorenzo data load has run after this sproc last ran and it has affected some rows.
If @LastLorenzoRunDateTime > @LastRunDateTime
Begin

	Exec PAS.uspPostLoadLorenzo_Patient
	Exec Pseudonomisation.dbo.PseudonomiseIPM
	Exec PAS.uspLoadznNetChangeByPatient
	Exec PAS.uspLoadznNetChangeByFacilityID

End

Exec WriteAuditLogEvent 'PAS - Lorenzo Post Load','',@StartTime

End Try

Begin Catch
	Exec msdb.dbo.sp_send_dbmail
	@profile_name = 'Business Intelligence',
	@recipients = 'Business.Intelligence@uhsm.nhs.uk',
	@subject = 'Lorenzo Post Load',
	@body_format = 'HTML',
	@body = 'PAS.uspPostLoadLorenzo failed.<br><br>';
End Catch
