﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Post Load Lorenzo Patient

Notes:			Stored in PAS

Versions:		
				1.0.0.0 - 21/01/2016 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure PAS.uspPostLoadLorenzo_Patient
As

Begin Try

Declare @RowsUpdated int = 0
Declare @Stats varchar(200) = ''
Declare @StartTime datetime
Declare @LastRunDateTime datetime

Set @StartTime = GETDATE()
Set @LastRunDateTime = (Select MAX(EventTime) From AuditLog Where ProcessCode = 'PAS - Post Load Lorenzo - Patient')

Update	src

Set		PATNT_REFNO_NHS_IDENTIFIER = src.NHS_IDENTIFIER
		,ETHGR_REFNO_DESCRIPTION = ethgr.[DESCRIPTION]
		,MARRY_REFNO_DESCRIPTION = marry.[DESCRIPTION]
		,SEXXX_REFNO_DESCRIPTION = sexxx.[DESCRIPTION]
		,TITLE_REFNO_DESCRIPTION = title.[DESCRIPTION]

From	Lorenzo.dbo.Patient src

		Left Join Lorenzo.dbo.ReferenceValue title
			on src.TITLE_REFNO = title.RFVAL_REFNO

		Left Join Lorenzo.dbo.ReferenceValue ethgr
			on src.ETHGR_REFNO = ethgr.RFVAL_REFNO

		Left Join Lorenzo.dbo.ReferenceValue marry
			on src.MARRY_REFNO = marry.RFVAL_REFNO

		Left Join Lorenzo.dbo.ReferenceValue sexxx
			on src.SEXXX_REFNO = sexxx.RFVAL_REFNO
			
Where	src.Created > @LastRunDateTime

Set @RowsUpdated = @@ROWCOUNT

Set @Stats = 'Rows update: ' + CONVERT(varchar(10),@RowsUpdated)

Exec WriteAuditLogEvent 'PAS - Post Load Lorenzo - Patient',@Stats,@StartTime

End Try

Begin Catch
	Exec msdb.dbo.sp_send_dbmail
	@profile_name = 'Business Intelligence',
	@recipients = 'Business.Intelligence@uhsm.nhs.uk',
	@subject = 'Post Load Lorenzo Patient',
	@body_format = 'HTML',
	@body = 'PAS.uspPostLoadLorenzo_Patient failed.<br><br>';
End Catch
