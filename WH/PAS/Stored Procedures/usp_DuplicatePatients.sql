﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Duplicate patient registrations

Notes:			Stored in PAS

Versions:		
				1.0.0.0 - 16/12/2015 - MT - Job 192
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure PAS.usp_DuplicatePatients
As

;With MyPatients As (
	Select	src.PATNT_REFNO As PatientID,
			src.DATE_OF_BIRTH As DOB,
			src.DATE_OF_DEATH As DOD,
			src.SURNAME,
			src.FORENAME,
			src.SECOND_FORENAME,
			src.THIRD_FORENAME,
			src.SNDEX_SURNAME,
			src.SNDEX_FORENAME,
			src.SEXXX_REFNO As SexID,
			ad.Address1,
			Replace(ad.Postcode,' ','') As Postcode
	From	Lorenzo.dbo.Patient src
	
			Left Join Lorenzo.dbo.MergePatient mp
				On src.PATNT_REFNO = mp.PREV_PATNT_REFNO
				
			Left Join PAS.PatientAddressHistory ad
				On src.PATNT_REFNO = ad.PatientNo
				And ad.CurrentFlag = 'Y'
				
	Where	mp.PREV_PATNT_REFNO Is Null -- Not already merged
	)
	
Select	
		src.FORENAME + 
		Case When src.SECOND_FORENAME Is Not Null Then ' ' + src.SECOND_FORENAME Else '' End +
		Case When src.THIRD_FORENAME Is Not Null Then ' ' + src.THIRD_FORENAME Else '' End +
		' ' + src.SURNAME As PatientName1,
		pat.FORENAME + 
		Case When pat.SECOND_FORENAME Is Not Null Then ' ' + pat.SECOND_FORENAME Else '' End +
		Case When pat.THIRD_FORENAME Is Not Null Then ' ' + pat.THIRD_FORENAME Else '' End +
		' ' + pat.SURNAME As PatientName2,
		src.Address1,
		pat.Address1 As Address2
From	MyPatients src

		Inner Join MyPatients pat
			On src.PatientID < pat.PatientID
			And src.DOB = pat.DOB
			And src.SexID = pat.SexID
			And src.Postcode = pat.Postcode
			And (
			src.SNDEX_FORENAME = pat.SNDEX_FORENAME
			Or (src.SNDEX_SURNAME = pat.SNDEX_FORENAME And src.SNDEX_FORENAME = pat.SNDEX_SURNAME)
			)

Order By 
		src.FORENAME + 
		Case When src.SECOND_FORENAME Is Not Null Then ' ' + src.SECOND_FORENAME Else '' End +
		Case When src.THIRD_FORENAME Is Not Null Then ' ' + src.THIRD_FORENAME Else '' End +
		' ' + src.SURNAME,
		pat.FORENAME + 
		Case When pat.SECOND_FORENAME Is Not Null Then ' ' + pat.SECOND_FORENAME Else '' End +
		Case When pat.THIRD_FORENAME Is Not Null Then ' ' + pat.THIRD_FORENAME Else '' End +
		' ' + pat.SURNAME
