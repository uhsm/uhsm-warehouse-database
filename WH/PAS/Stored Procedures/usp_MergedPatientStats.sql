﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Merged patient stats

Notes:			Stored in PAS

Versions:		
				1.0.0.0 - 16/12/2015 - MT - Job 192
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure PAS.usp_MergedPatientStats
As

Declare @Stats table (
	Item int not null,
	[Description] varchar(250) not null,
	Value int
	)

Insert Into @Stats(Item,[Description],Value)
Select 1,'Total patient registrations',COUNT(*)
From	Lorenzo.dbo.Patient src
Where	src.ARCHV_FLAG = 'N'

Insert Into @Stats(Item,[Description],Value)
Select 2,'Total patient identifiers',COUNT(*)
From	Lorenzo.dbo.PatientIdentifier src
Where	src.ARCHV_FLAG = 'N'

;With MyDistinctMergePatient As (
	Select	Distinct src.PATNT_REFNO,src.PREV_PATNT_REFNO
	From	Lorenzo.dbo.MergePatient src
	Where	src.ARCHV_FLAG = 'N'
	)
Insert Into @Stats(Item,[Description],Value)
Select 3,'Merged patients',COUNT(*)
From	MyDistinctMergePatient src

Select	*
From	@Stats src
Order By src.Item
