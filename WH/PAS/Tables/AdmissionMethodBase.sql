﻿CREATE TABLE [PAS].[AdmissionMethodBase] (
    [MOAID]             VARCHAR (2)  NOT NULL,
    [Description]       VARCHAR (50) NULL,
    [FullDescription]   VARCHAR (50) NULL,
    [HaaCode]           VARCHAR (2)  NULL,
    [InternalValue]     VARCHAR (50) NULL,
    [MfRecStsInd]       VARCHAR (2)  NULL,
    [ScottishIntValue]  VARCHAR (2)  NULL,
    [MethodOfAdmission] VARCHAR (2)  NULL,
    CONSTRAINT [PK_AdmissionMethod] PRIMARY KEY CLUSTERED ([MOAID] ASC),
    CONSTRAINT [FK_AdmissionMethodBase_AdmissionMethodBase] FOREIGN KEY ([HaaCode]) REFERENCES [AdmissionMethod] ([AdmissionMethodCode])
);

