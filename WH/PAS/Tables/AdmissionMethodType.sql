﻿CREATE TABLE [PAS].[AdmissionMethodType] (
    [AdmissionMethodTypeCode] VARCHAR (10) NOT NULL,
    [AdmissionMethodType]     VARCHAR (50) NULL,
    CONSTRAINT [PK_AdmissionMethodType] PRIMARY KEY CLUSTERED ([AdmissionMethodTypeCode] ASC)
);

