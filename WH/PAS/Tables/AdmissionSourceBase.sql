﻿CREATE TABLE [PAS].[AdmissionSourceBase] (
    [SOADID]           VARCHAR (2)  NOT NULL,
    [Description]      VARCHAR (50) NULL,
    [FullDescription]  VARCHAR (50) NULL,
    [HaaCode]          VARCHAR (2)  NULL,
    [InternalValue]    VARCHAR (50) NULL,
    [MfRecStsInd]      VARCHAR (2)  NULL,
    [ScottishIntValue] VARCHAR (2)  NULL,
    [SourceOfAdm]      VARCHAR (2)  NULL,
    CONSTRAINT [PK_AdmissionSource] PRIMARY KEY CLUSTERED ([SOADID] ASC),
    CONSTRAINT [FK_AdmissionSourceBase_AdmissionSourceBase] FOREIGN KEY ([HaaCode]) REFERENCES [AdmissionSource] ([AdmissionSourceCode])
);

