﻿CREATE TABLE [PAS].[ClinicBase] (
    [OUTCLNCID]            VARCHAR (8)   NOT NULL,
    [AppTypesAvlblGpBook]  VARCHAR (3)   NULL,
    [AppTypesAvlblGpInt]   VARCHAR (1)   NULL,
    [Comment]              VARCHAR (20)  NULL,
    [ConsCode]             VARCHAR (6)   NULL,
    [Description]          VARCHAR (20)  NULL,
    [DestForGpAppLetters]  VARCHAR (10)  NULL,
    [DestinationHospCd]    VARCHAR (4)   NULL,
    [FunctionCode]         VARCHAR (4)   NULL,
    [GpAduAppLtrDocument]  VARCHAR (10)  NULL,
    [GpAppConfirmPara]     VARCHAR (10)  NULL,
    [GpBroMaxNoDaysFuture] VARCHAR (4)   NULL,
    [GpChiAppLtrDocument]  VARCHAR (10)  NULL,
    [GpLnkDftBookTypeDesc] VARCHAR (50)  NULL,
    [GpReasonRefrlText]    VARCHAR (70)  NULL,
    [GpRefrlCriteriaPara]  VARCHAR (48)  NULL,
    [GplClinMfRtnAppTypes] VARCHAR (23)  NULL,
    [GplClinMfUrgAppTypes] VARCHAR (23)  NULL,
    [GplClinicMfUrgent]    VARCHAR (3)   NULL,
    [GplClinicMfUrgentInt] VARCHAR (1)   NULL,
    [GplinkDftBookType]    VARCHAR (4)   NULL,
    [MaxAgeThatMeansChild] VARCHAR (2)   NULL,
    [MfRecStsInd]          VARCHAR (255) NULL,
    [ProviderCode]         VARCHAR (4)   NULL,
    [ResultLocationCode]   VARCHAR (6)   NULL,
    [ResultLocationDesc]   VARCHAR (30)  NULL,
    [RptToLocCode]         VARCHAR (4)   NULL,
    [SchedResGrpMfCd]      VARCHAR (8)   NOT NULL,
    [ServiceGroup]         VARCHAR (6)   NULL,
    [Specialty]            VARCHAR (4)   NULL,
    CONSTRAINT [PK_ClinicBase] PRIMARY KEY CLUSTERED ([OUTCLNCID] ASC)
);

