﻿CREATE TABLE [PAS].[CodingError] (
    [sorce_refno] NUMERIC (18)  NULL,
    [SORT_ORDER]  NUMERIC (18)  NULL,
    [CODE]        VARCHAR (20)  NULL,
    [DESCRIPTION] VARCHAR (255) NULL,
    [COMMENTS]    VARCHAR (255) NULL,
    [DPTYP_CODE]  VARCHAR (5)   NULL,
    [CREATE_DTTM] DATETIME      NULL,
    [MODIF_DTTM]  DATETIME      NULL,
    [ARCHV_FLAG]  CHAR (1)      NULL,
    [DGPRO_REFNO] INT           NOT NULL,
    [ODPCD_REFNO] INT           NULL,
    [SORCE_CODE]  VARCHAR (5)   NULL,
    [Created]     DATETIME      NULL
);

