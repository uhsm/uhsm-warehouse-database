﻿CREATE TABLE [PAS].[DischargeDestinationBase] (
    [DODID]            VARCHAR (2)  NOT NULL,
    [Description]      VARCHAR (50) NULL,
    [FullDescription]  VARCHAR (50) NULL,
    [HaaCode]          VARCHAR (2)  NULL,
    [InternalValue]    VARCHAR (50) NULL,
    [MfRecStsInd]      VARCHAR (2)  NULL,
    [ScottishIntValue] VARCHAR (2)  NULL,
    [DestinationCode]  VARCHAR (2)  NULL,
    CONSTRAINT [PK_DischargeDestination] PRIMARY KEY CLUSTERED ([DODID] ASC),
    CONSTRAINT [FK_DischargeDestinationBase_DischargeDestinationBase] FOREIGN KEY ([HaaCode]) REFERENCES [DischargeDestination] ([DischargeDestinationCode])
);

