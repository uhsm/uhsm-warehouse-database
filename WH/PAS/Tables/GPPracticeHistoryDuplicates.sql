﻿CREATE TABLE [PAS].[GPPracticeHistoryDuplicates] (
    [FacilityID]        VARCHAR (20) NULL,
    [NHSNo]             VARCHAR (20) NULL,
    [AdmissionDateTime] DATETIME     NULL,
    [EncounterType]     VARCHAR (3)  NOT NULL,
    [cnt]               INT          NULL
);

