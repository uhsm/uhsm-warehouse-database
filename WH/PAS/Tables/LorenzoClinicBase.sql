﻿CREATE TABLE [PAS].[LorenzoClinicBase] (
    [ClinicCode]  VARCHAR (20) NOT NULL,
    [Clinic Type] VARCHAR (80) NULL,
    CONSTRAINT [PK_LorenzoClinicBase] PRIMARY KEY CLUSTERED ([ClinicCode] ASC)
);

