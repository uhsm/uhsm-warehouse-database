﻿CREATE TABLE [PAS].[LorenzoTables] (
    [RecordID]                     INT           IDENTITY (1, 1) NOT NULL,
    [TableName]                    VARCHAR (100) NOT NULL,
    [PrimaryKey]                   VARCHAR (50)  NOT NULL,
    [HasPATNT_REFNO]               VARCHAR (1)   NOT NULL,
    [ParentTable]                  VARCHAR (100) NULL,
    [IncludeForNetChangeByPatient] VARCHAR (1)   NOT NULL,
    [LastLoadedDateTime]           DATETIME      NOT NULL,
    CONSTRAINT [PK_LorenzoTables] PRIMARY KEY CLUSTERED ([RecordID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_LorenzoTables_TableName]
    ON [PAS].[LorenzoTables]([TableName] ASC);

