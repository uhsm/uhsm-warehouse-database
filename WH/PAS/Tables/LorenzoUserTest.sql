﻿CREATE TABLE [PAS].[LorenzoUserTest] (
    [UserRefno]    NUMERIC (18) NOT NULL,
    [UserUniqueID] VARCHAR (20) NULL,
    [UserName]     VARCHAR (35) NULL,
    [UserForename] VARCHAR (35) NULL,
    [UserSurname]  VARCHAR (35) NULL
);

