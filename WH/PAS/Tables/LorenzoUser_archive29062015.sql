﻿CREATE TABLE [PAS].[LorenzoUser_archive29062015] (
    [SourceUniqueID] INT          IDENTITY (1, 1) NOT NULL,
    [UserRefno]      INT          NULL,
    [UserUniqueID]   VARCHAR (50) NULL,
    [UserName]       VARCHAR (35) NULL,
    [UserForename]   VARCHAR (35) NULL,
    [UserSurname]    VARCHAR (35) NULL
);

