﻿CREATE TABLE [PAS].[MissingPatient] (
    [SourcePatientNo]     INT            NOT NULL,
    [LocalPatientID]      NVARCHAR (15)  NULL,
    [PatientForename]     VARCHAR (8000) NULL,
    [PatientSurname]      VARCHAR (8000) NULL,
    [NHSNumber]           NVARCHAR (50)  NULL,
    [NHSNumberStatusCode] NVARCHAR (2)   NULL,
    [Postcode]            NVARCHAR (20)  NULL,
    [Sex]                 NVARCHAR (20)  NULL,
    [DOB]                 NVARCHAR (10)  NULL,
    [GPCode]              NVARCHAR (20)  NULL,
    [PracticeCode]        NVARCHAR (20)  NULL,
    [InterfaceCode]       VARCHAR (3)    NULL,
    [PatientRecno]        INT            IDENTITY (1, 1) NOT NULL,
    [Archived]            NVARCHAR (1)   NULL,
    [StartDate]           DATETIME       NULL,
    [EndDate]             DATETIME       NULL
);

