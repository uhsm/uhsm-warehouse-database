﻿CREATE TABLE [PAS].[OperationBase] (
    [OPCS4ID]                   VARCHAR (5)   NOT NULL,
    [Opcs4Dx3]                  VARCHAR (55)  NULL,
    [Opcs4Dx4]                  VARCHAR (60)  NULL,
    [Opcs4GpfhChargeable]       VARCHAR (1)   NULL,
    [Opcs4GpfhEffDate]          VARCHAR (8)   NULL,
    [Opcs4Groupcode]            VARCHAR (4)   NULL,
    [Opcs4Ldf]                  VARCHAR (255) NULL,
    [Opcs4Procpricegroup]       VARCHAR (4)   NULL,
    [RevisionFourOperationCode] VARCHAR (5)   NOT NULL,
    CONSTRAINT [PK_Operation_1] PRIMARY KEY CLUSTERED ([OPCS4ID] ASC)
);

