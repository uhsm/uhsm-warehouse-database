﻿CREATE TABLE [PAS].[OverseasVisitorHistory] (
    [PatientNo]                  INT          NOT NULL,
    [DistrictNo]                 VARCHAR (20) NULL,
    [SeqNo]                      INT          NOT NULL,
    [StartDate]                  DATE         NOT NULL,
    [EndDate]                    DATE         NULL,
    [OverseasVisitorStatusRefno] INT          NULL,
    [OverseasVisitorStatusCode]  VARCHAR (25) NULL,
    [CurrentFlag]                VARCHAR (1)  NOT NULL,
    [OVSEA_REFNO]                INT          NULL,
    [LastLoadedDateTime]         DATETIME     NOT NULL,
    CONSTRAINT [PK_PAS_OverseasVisitorHistory] PRIMARY KEY NONCLUSTERED ([PatientNo] ASC, [StartDate] ASC)
);

