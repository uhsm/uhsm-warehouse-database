﻿CREATE TABLE [PAS].[Patient] (
    [PATNT_REFNO]            INT          NOT NULL,
    [LocalPatientIdentifier] VARCHAR (20) NOT NULL,
    [NHSNo]                  VARCHAR (10) NULL,
    [MasterPATNT_REFNO]      INT          NOT NULL,
    [LastLoadedDateTime]     DATETIME     NOT NULL,
    CONSTRAINT [PK_Patient_1] PRIMARY KEY CLUSTERED ([PATNT_REFNO] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Patient_LocalPatientIdentifier]
    ON [PAS].[Patient]([LocalPatientIdentifier] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Patient_NHSNo]
    ON [PAS].[Patient]([NHSNo] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Patient_MasterPATNT_REFNO]
    ON [PAS].[Patient]([MasterPATNT_REFNO] ASC);

