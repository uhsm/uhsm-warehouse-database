﻿CREATE TABLE [PAS].[PatientAddressHistory] (
    [PatientNo]          INT          NOT NULL,
    [DistrictNo]         VARCHAR (20) NULL,
    [SeqNo]              INT          NOT NULL,
    [StartDate]          DATE         NOT NULL,
    [EndDate]            DATE         NULL,
    [Address1]           VARCHAR (50) NULL,
    [Address2]           VARCHAR (50) NULL,
    [Address3]           VARCHAR (50) NULL,
    [Address4]           VARCHAR (50) NULL,
    [Postcode]           VARCHAR (25) NULL,
    [CountryCode]        VARCHAR (25) NULL,
    [CountryDescription] VARCHAR (80) NULL,
    [DHACode]            VARCHAR (5)  NULL,
    [CCGCode]            VARCHAR (5)  NULL,
    [CurrentFlag]        VARCHAR (1)  NOT NULL,
    [ADDSS_REFNO]        INT          NOT NULL,
    [LastLoadedDateTime] DATETIME     NOT NULL,
    CONSTRAINT [PK_PAS_PatientAddressHistory] PRIMARY KEY NONCLUSTERED ([PatientNo] ASC, [StartDate] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_PAS_PatientAddressHistory_DistricNo]
    ON [PAS].[PatientAddressHistory]([DistrictNo] ASC);

