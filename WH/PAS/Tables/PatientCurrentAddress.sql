﻿CREATE TABLE [PAS].[PatientCurrentAddress] (
    [SourcePatientNo]    INT           NOT NULL,
    [PatientTitle]       VARCHAR (80)  NULL,
    [PatientForename]    VARCHAR (30)  NULL,
    [PatientSurname]     VARCHAR (30)  NULL,
    [DistrictNo]         VARCHAR (20)  NULL,
    [Postcode]           VARCHAR (25)  NULL,
    [PatientAddress1]    VARCHAR (50)  NULL,
    [PatientAddress2]    VARCHAR (50)  NULL,
    [PatientAddress3]    VARCHAR (50)  NULL,
    [PatientAddress4]    VARCHAR (50)  NULL,
    [CountryDescription] VARCHAR (80)  NULL,
    [CountryCode]        VARCHAR (25)  NULL,
    [StartDate]          DATETIME      NULL,
    [EndDate]            DATETIME2 (7) NULL,
    [DateOfBirth]        DATETIME      NULL,
    [DateOfDeath]        DATETIME      NULL,
    CONSTRAINT [PK_PatientCurrentAddress] PRIMARY KEY CLUSTERED ([SourcePatientNo] ASC)
);

