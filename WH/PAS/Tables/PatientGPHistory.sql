﻿CREATE TABLE [PAS].[PatientGPHistory] (
    [PatientNo]          INT          NOT NULL,
    [DistrictNo]         VARCHAR (20) NULL,
    [SeqNo]              INT          NOT NULL,
    [StartDate]          DATE         NOT NULL,
    [EndDate]            DATE         NULL,
    [GPRefno]            INT          NULL,
    [GPCode]             VARCHAR (25) NULL,
    [PracticeRefno]      INT          NULL,
    [PracticeCode]       VARCHAR (25) NULL,
    [CurrentFlag]        VARCHAR (1)  NOT NULL,
    [PATPC_REFNO]        INT          NULL,
    [LastLoadedDateTime] DATETIME     NOT NULL,
    CONSTRAINT [PK_PAS_PatientGPHistory] PRIMARY KEY NONCLUSTERED ([PatientNo] ASC, [StartDate] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_PAS_PatientGPHistory_DistricNo]
    ON [PAS].[PatientGPHistory]([DistrictNo] ASC);

