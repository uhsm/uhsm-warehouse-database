﻿CREATE TABLE [PAS].[PatientPhoneNumber] (
    [PatientPhoneRecno] INT          IDENTITY (1, 1) NOT NULL,
    [SourcePatientNo]   INT          NOT NULL,
    [HomePhone]         VARCHAR (50) NULL,
    [MobilePhone]       VARCHAR (50) NULL,
    CONSTRAINT [PK_PASPatientPhone] PRIMARY KEY CLUSTERED ([PatientPhoneRecno] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Patno_PatientPhoneNumber]
    ON [PAS].[PatientPhoneNumber]([SourcePatientNo] ASC);

