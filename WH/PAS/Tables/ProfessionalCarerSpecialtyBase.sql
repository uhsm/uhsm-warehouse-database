﻿CREATE TABLE [PAS].[ProfessionalCarerSpecialtyBase] (
    [PRCAS_REFNO]         INT           NOT NULL,
    [SPECT_REFNO]         INT           NULL,
    [CSTYP_REFNO]         INT           NULL,
    [PROCA_REFNO]         INT           NULL,
    [START_DTTM]          SMALLDATETIME NULL,
    [END_DTTM]            SMALLDATETIME NULL,
    [CREATE_DTTM]         DATETIME      NULL,
    [MODIF_DTTM]          DATETIME      NULL,
    [USER_CREATE]         VARCHAR (30)  NULL,
    [USER_MODIF]          VARCHAR (30)  NULL,
    [ARCHV_FLAG]          CHAR (1)      NULL,
    [STRAN_REFNO]         NUMERIC (18)  NULL,
    [PRIOR_POINTER]       INT           NULL,
    [EXTERNAL_KEY]        VARCHAR (20)  NULL,
    [PBK_LEAD_TIME]       INT           NULL,
    [PBK_LEAD_TIME_UNITS] CHAR (1)      NULL,
    [OWNER_HEORG_REFNO]   INT           NULL,
    [Created]             DATETIME      NULL,
    CONSTRAINT [PK_ProfessionalCarerSpecialtyBase] PRIMARY KEY CLUSTERED ([PRCAS_REFNO] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_ProfessionalCarerSpecialtyBase_1]
    ON [PAS].[ProfessionalCarerSpecialtyBase]([CSTYP_REFNO] ASC, [PROCA_REFNO] ASC, [ARCHV_FLAG] ASC, [PRCAS_REFNO] ASC);

