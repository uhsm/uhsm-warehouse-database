﻿CREATE TABLE [PAS].[ReferenceValueIdentifierBase20130920] (
    [RFVLI_REFNO]       INT          NOT NULL,
    [RFVAL_REFNO]       INT          NULL,
    [IDENTIFIER]        VARCHAR (50) NULL,
    [CREATE_DTTM]       DATETIME     NULL,
    [MODIF_DTTM]        DATETIME     NULL,
    [USER_CREATE]       VARCHAR (30) NULL,
    [USER_MODIF]        VARCHAR (30) NULL,
    [RITYP_CODE]        VARCHAR (5)  NULL,
    [ARCHV_FLAG]        CHAR (1)     NULL,
    [STRAN_REFNO]       NUMERIC (18) NULL,
    [PRIOR_POINTER]     INT          NULL,
    [EXTERNAL_KEY]      VARCHAR (20) NULL,
    [OWNER_HEORG_REFNO] INT          NULL,
    [Created]           DATETIME     NULL
);

