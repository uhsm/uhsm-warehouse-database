﻿CREATE TABLE [PAS].[SpecialtyBase] (
    [SPECT_REFNO]                  NUMERIC (18)  NOT NULL,
    [SPECT_REFNO_MAIN_IDENT]       VARCHAR (25)  NULL,
    [MAIN_IDENT]                   VARCHAR (20)  NULL,
    [DESCRIPTION]                  VARCHAR (255) NULL,
    [PARNT_REFNO]                  NUMERIC (18)  NULL,
    [DIVSN_REFNO]                  NUMERIC (18)  NULL,
    [DIVSN_REFNO_MAIN_CODE]        VARCHAR (25)  NULL,
    [DIVSN_REFNO_DESCRIPTION]      VARCHAR (80)  NULL,
    [CREATE_DTTM]                  DATETIME      NULL,
    [MODIF_DTTM]                   DATETIME      NULL,
    [USER_CREATE]                  VARCHAR (30)  NULL,
    [USER_MODIF]                   VARCHAR (30)  NULL,
    [ARCHV_FLAG]                   CHAR (1)      NULL,
    [START_DTTM]                   DATETIME      NULL,
    [END_DTTM]                     DATETIME      NULL,
    [OWNER_HEORG_REFNO]            NUMERIC (18)  NULL,
    [OWNER_HEORG_REFNO_MAIN_IDENT] VARCHAR (25)  NULL,
    [Created]                      DATETIME      NULL,
    CONSTRAINT [PK_SpecialtyBase] PRIMARY KEY CLUSTERED ([SPECT_REFNO] ASC)
);

