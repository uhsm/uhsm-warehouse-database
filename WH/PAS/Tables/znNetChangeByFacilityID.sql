﻿CREATE TABLE [PAS].[znNetChangeByFacilityID] (
    [FacilityID]          VARCHAR (20) NOT NULL,
    [LastUpdatedDateTime] DATETIME     NOT NULL,
    CONSTRAINT [PK_PAS_znNetChangeByFacilityID] PRIMARY KEY CLUSTERED ([FacilityID] ASC)
);

