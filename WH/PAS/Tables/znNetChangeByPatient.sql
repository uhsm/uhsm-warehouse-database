﻿CREATE TABLE [PAS].[znNetChangeByPatient] (
    [PATNT_REFNO]         INT          NOT NULL,
    [LastUpdatedDateTime] DATETIME     NOT NULL,
    [LatestActivity]      VARCHAR (50) NOT NULL,
    [LastCreatedDateTime] DATETIME     NOT NULL,
    [DeletedFlag]         VARCHAR (1)  CONSTRAINT [DF_znNetChangeByPatient_DeletedFlag] DEFAULT ('N') NOT NULL,
    [TestFlag]            VARCHAR (1)  CONSTRAINT [DF_znNetChangeByPatient_TestFlag] DEFAULT ('N') NOT NULL,
    CONSTRAINT [PK_PAS_znNetChangeByPatient] PRIMARY KEY CLUSTERED ([PATNT_REFNO] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_PAS_znNetChangeByPatient_LastUpdatedDateTime]
    ON [PAS].[znNetChangeByPatient]([LastUpdatedDateTime] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PAS_znNetChangeByPatient_LastCreatedDateTime]
    ON [PAS].[znNetChangeByPatient]([LastCreatedDateTime] ASC);

