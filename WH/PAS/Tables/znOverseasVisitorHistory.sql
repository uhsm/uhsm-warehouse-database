﻿CREATE TABLE [PAS].[znOverseasVisitorHistory] (
    [PatientNo]  INT          NOT NULL,
    [SourceType] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_PAS_znOverseasVisitorHistory] PRIMARY KEY CLUSTERED ([PatientNo] ASC)
);

