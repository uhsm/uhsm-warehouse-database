﻿CREATE TABLE [PAS].[znPatientAddressHistory] (
    [PatientNo]  INT          NOT NULL,
    [SourceType] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_PAS_znPatientAddressHistory] PRIMARY KEY CLUSTERED ([PatientNo] ASC)
);

