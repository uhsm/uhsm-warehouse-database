﻿CREATE TABLE [PAS].[znPatientGPHistory] (
    [PatientNo]  INT          NOT NULL,
    [SourceType] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_PAS_znPatientGPHistory] PRIMARY KEY CLUSTERED ([PatientNo] ASC)
);

