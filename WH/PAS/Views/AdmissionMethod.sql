﻿





CREATE view [PAS].[AdmissionMethod] as

select
	 AdmissionMethodCode = ReferenceValueCode
	,AdmissionMethod = ReferenceValue
	,AdmissionMethodLocalCode = 
		case
		when MainCode = 'BHOSP' then '82'
		when MainCode = 'BOHOSP' then '83'
		else MainCode
		end

	,AdmissionMethodTypeCode =
		case

		when MainCode in ('11', '12', '13') --elective
		then 1

		when MainCode in ('21', '22', '23', '24', '25', '28') --emergency
		then 2

		when MainCode in ('31', '32') --maternity
		then 3

		when MainCode in ('81', '82', '83') --other
		then 4

		else
			coalesce(
				AdmissionMethodType.AdmissionMethodTypeCode
				,0
			)
		end

	,AdmissionMethodType =
		case

		when MainCode in ('11', '12', '13') --elective
		then 'Elective'

		when MainCode in ('21', '22', '23', '24', '25', '28') --emergency
		then 'Emergency'

		when MainCode in ('31', '32') --maternity
		then 'Maternity'

		when MainCode in ('81', '82', '83') --other
		then 'Other'

		else
			coalesce(
				AdmissionMethodType.AdmissionMethodType
				,'N/A'
			)
		end

from
	PAS.ReferenceValue

left join
	(
	select
		 AdmissionMethodTypeCode = ReferenceValueCode
		,AdmissionMethodType = ReferenceValue
		,AdmissionMethodTypeLocalCode = MainCode
	from
		PAS.ReferenceValue
	where
		ReferenceDomainCode = 'ADMET'
	) AdmissionMethodType
on	AdmissionMethodType.AdmissionMethodTypeCode = ReferenceValue.ParentReferenceValueCode

where
	ReferenceDomainCode = 'ADMET'
--and	DisplayValue = 'Y'







