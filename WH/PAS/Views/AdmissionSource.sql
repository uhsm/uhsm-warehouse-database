﻿create view [PAS].[AdmissionSource] as

select
	 AdmissionSourceCode = SOADID
	,AdmissionSource = Description
	,NationalAdmissionSourceCode = HaaCode
	,DeletedFlag = MfRecStsInd
from
	PAS.AdmissionSourceBase
