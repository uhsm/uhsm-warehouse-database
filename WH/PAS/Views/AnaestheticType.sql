﻿create  view [PAS].[AnaestheticType] as

select
	 AnaestheticTypeCode = ReferenceValue.ReferenceValueCode
	,AnaestheticType = ReferenceValue.ReferenceValue
	,NationalAnaestheticTypeCode = ReferenceValue.MainCode
from
	PAS.ReferenceValue
where
	ReferenceValue.ReferenceDomainCode = 'ANTYP'


