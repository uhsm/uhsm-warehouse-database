﻿
CREATE view [PAS].[BookingType] as

select
	 BookingTypeCode = ReferenceValueCode
	,BookingType = ReferenceValue
	,BookingTypeLocalCode = MainCode
from
	PAS.ReferenceValue
where
	ReferenceDomainCode = 'BKTYP'


