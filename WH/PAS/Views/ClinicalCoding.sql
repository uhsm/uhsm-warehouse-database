﻿



CREATE view [PAS].[ClinicalCoding] as

select
	 SourceUniqueID = DGPRO_REFNO
	,SourceCode = SORCE_CODE
	,SourceRecno = SORCE_REFNO
	,SortOrder = SORT_ORDER
	,ClinicalCodingTime = DGPRO_DTTM
	,ClinicalCodingTypeCode = DPTYP_CODE
	,ClinicalCodingCode = ODPCD_REFNO
	,ClinicalCodingMappedCode = CODE
	,CancelDate = CANCEL_DTTM
	,PASCreatedDate = CREATE_DTTM
	,PASModifiedDate = MODIF_DTTM
	,PASCreatedByWhom = USER_CREATE
	,PASUpdatedByWhom = USER_MODIF

	,ArchiveFlag =
		convert(
			bit
			,case
			when ARCHV_FLAG = 'N'
			then 0
			else 1
			end
		)

	,ClinicalCoding =
		ClinicalCodingBase.DESCRIPTION

from
	PAS.ClinicalCodingBase




