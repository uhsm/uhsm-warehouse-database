﻿
CREATE view [PAS].[Coding] as

select
	 CodingCode = CodeBase.ODPCD_REFNO
	,Coding = CodeBase.DESCRIPTION
	,CodingTypeCode = CodeBase.CCSXT_CODE
	,ParentCodingCode = CodeBase.PARNT_REFNO
	,CodingLocalCode = CodeBase.CODE
	,Retired = Case When CodeBase.END_DTTM is null Then
		CONVERT(bit,1)
	Else
		CONVERT(bit,0)
	end
	
	,ArchiveFlag =
		convert(
			bit
			,case
			when CodeBase.ARCHV_FLAG = 'N'
			then 0
			else 1
			end
		)



	

from
	PAS.CodingBase CodeBase
/*	
Where not exists
(
	Select 1 from PAS.CodingBase oldcode
	where CodeBase.CODE = OldCode.Code
	and CodeBase.CCSXT_CODE = OldCode.CCSXT_CODE
	and codebase.CREATE_DTTM > OldCode.CREATE_DTTM
	and OldCode.ARCHV_FLAG = 'N'
)
*/
