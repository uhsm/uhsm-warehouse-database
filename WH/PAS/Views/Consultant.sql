﻿




CREATE view [PAS].[Consultant] as

select
	 ConsultantCode = ProfessionalCarerBase.PROCA_REFNO
	,Deleted = ProfessionalCarerBase.ARCHV_FLAG

	,Title = 
		case
		when ProfessionalCarerBase.TITLE_REFNO_DESCRIPTION = 'Not Known'
		then ''
		else coalesce(ProfessionalCarerBase.TITLE_REFNO_DESCRIPTION, '')
		end

	,NationalConsultantCode = coalesce(ProfessionalCarerBase.MAIN_IDENT, '')
	,Initials = coalesce(ProfessionalCarerBase.FORENAME, '')
	,Forename = coalesce(ProfessionalCarerBase.FORENAME, '')

	,Consultant =
		coalesce(ProfessionalCarerBase.SURNAME, '') + 
		case
		when ProfessionalCarerBase.FORENAME = ''
		then ''
		else coalesce(', ' + ProfessionalCarerBase.FORENAME, '')
		end

	,MainSpecialtyCode = ProfessionalCarerSpecialtyBase.SPECT_REFNO --current main specialty!
	,Surname = coalesce(ProfessionalCarerBase.SURNAME, '')
	,ProfessionalCarerTypeCode = ProfessionalCarerBase.PRTYP_REFNO
	,ProfessionalCarerTypeLocalCode = ProfessionalCarerBase.PRTYP_REFNO_MAIN_CODE
	,ProfessionalCarerType = ProfessionalCarerBase.PRTYP_REFNO_DESCRIPTION

	,ArchiveFlag =
		convert(
			bit
			,case
			when ProfessionalCarerBase.ARCHV_FLAG = 'Y'
			then 1
			else 0
			end
		)
	,ParentHealthOrganisation = OWNER_HEORG_REFNO_MAIN_IDENT
	,LocalFlag = ProfessionalCarerBase.LOCAL_FLAG
	,StartDate = ProfessionalCarerBase.START_DTTM
	,EndDate = ProfessionalCarerBase.END_DTTM

from
	PAS.ProfessionalCarerBase

left join PAS.ProfessionalCarerSpecialtyBase
on	ProfessionalCarerSpecialtyBase.PROCA_REFNO = ProfessionalCarerBase.PROCA_REFNO
and	ProfessionalCarerSpecialtyBase.CSTYP_REFNO = 457 -- Primary Specialty
and	ProfessionalCarerSpecialtyBase.ARCHV_FLAG = 'N'
and	not exists
	(
	select
		1
	from
		PAS.ProfessionalCarerSpecialtyBase Previous
	where
		Previous.PROCA_REFNO = ProfessionalCarerBase.PROCA_REFNO
	and	Previous.CSTYP_REFNO = 457 -- Primary Specialty
	and	Previous.ARCHV_FLAG = 'N'
	and	Previous.PRCAS_REFNO > ProfessionalCarerSpecialtyBase.PRCAS_REFNO
	)




