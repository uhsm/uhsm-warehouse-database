﻿CREATE view [PAS].[ConsultantSpecialty] as

select
	 ConsultantSpecialtyCode = ProfessionalCarerSpecialty.PRCAS_REFNO

	,ConsultantCode = ProfessionalCarerSpecialty.PROCA_REFNO

	,CarerSpecialtyTypeCode = ProfessionalCarerSpecialty.CSTYP_REFNO --Main or sub

	,SpecialtyCode = ProfessionalCarerSpecialty.SPECT_REFNO

	,StartDate = ProfessionalCarerSpecialty.START_DTTM
	,EndDate = ProfessionalCarerSpecialty.END_DTTM

	,ArchiveFlag =
		convert(
			bit
			,case
			when ProfessionalCarerSpecialty.ARCHV_FLAG = 'Y'
			then 1
			else 0
			end
		)

from
	PAS.ProfessionalCarerSpecialtyBase ProfessionalCarerSpecialty

