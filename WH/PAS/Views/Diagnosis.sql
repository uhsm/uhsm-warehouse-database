﻿
CREATE view [PAS].[Diagnosis] as

select
	 DiagnosisCode = ICD10ID
	,Diagnosis = Description
	,DiagnosisChapterCode = left(ICD10ID, 3)
	,DiagnosisChapter = ThreeDigitName
from
	PAS.DiagnosisBase

