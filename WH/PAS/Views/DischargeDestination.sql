﻿
CREATE view [PAS].[DischargeDestination] as

select
	 DischargeDestinationCode = DODID
	,DischargeDestination = Description
	,NationalDischargeDestinationCode = HaaCode
	,DeletedFlag = MfRecStsInd
from
	PAS.DischargeDestinationBase

