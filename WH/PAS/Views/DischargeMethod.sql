﻿

CREATE view [PAS].[DischargeMethod] as

select
	 DischargeMethodCode = ReferenceValueCode
	,DischargeMethod = ReferenceValue
	,DischargeMethodLocalCode = MainCode
from
	PAS.ReferenceValue
where
	ReferenceDomainCode = 'DISMT'



