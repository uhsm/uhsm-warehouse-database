﻿create view PAS.Disposal as

select
	 DisposalCode = ReferenceValueCode
	,Disposal = ReferenceValue
	,DisposalLocalCode = MainCode
from
	PAS.ReferenceValue
where
	ReferenceDomainCode = 'SCOCM'

