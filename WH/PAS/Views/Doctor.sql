﻿create view [PAS].[Doctor] as

select
	 DoctorCode = OPDOCTORID
	,Doctor = Name
	,Comment
from
	PAS.OPDoctorBase
