﻿



CREATE view [PAS].[FirstAttendance] as

select
	 FirstAttendanceCode = ReferenceValueCode
	,FirstAttendance = ReferenceValue
	,FirstAttendanceLocalCode = MainCode
	,FirstAttendanceTypeCode = IsNull(FirstAttendanceType.FirstAttendanceTypeCode,PAS.ReferenceValue.ReferenceValueCode)
	,FirstAttendanceType = IsNull(FirstAttendanceType.FirstAttendanceType,PAS.ReferenceValue.ReferenceValue)
from
	PAS.ReferenceValue

left join
	(
	select
		 FirstAttendanceTypeCode = ReferenceValueCode
		,FirstAttendanceType = ReferenceValue
		,FirstAttendanceTypeLocalCode = MainCode
	from
		PAS.ReferenceValue
	where
		ReferenceDomainCode = 'VISIT'
	) FirstAttendanceType
on	FirstAttendanceType.FirstAttendanceTypeCode = ReferenceValue.ParentReferenceValueCode

where
	ReferenceDomainCode = 'VISIT'
--and	DisplayValue = 'Y'




