﻿CREATE view [PAS].[Gp] as

select
	 GpCode = PROCA_REFNO
	,Deleted = ARCHV_FLAG

	,Title = 
		case
		when TITLE_REFNO_DESCRIPTION = 'Not Known'
		then ''
		else coalesce(TITLE_REFNO_DESCRIPTION, '')
		end

	,NationalGpCode = coalesce(MAIN_IDENT, '')
	,Initials = coalesce(FORENAME, '')
	,Forename = coalesce(FORENAME, '')

	,Gp =
		coalesce(SURNAME, '') + 
		case
		when FORENAME = ''
		then ''
		else coalesce(', ' + FORENAME, '')
		end

	,MainSpecialtyCode = null
	,Surname = coalesce(SURNAME, '')
	,ProfessionalCarerTypeCode = PRTYP_REFNO
	,ProfessionalCarerTypeLocalCode = PRTYP_REFNO_MAIN_CODE
	,ProfessionalCarerType = PRTYP_REFNO_DESCRIPTION

	,ArchiveFlag =
		convert(
			bit
			,case
			when ARCHV_FLAG = 'N'
			then 0
			else 1
			end
		)

from
	PAS.ProfessionalCarerBase

