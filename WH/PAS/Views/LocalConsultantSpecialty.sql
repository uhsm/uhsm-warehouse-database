﻿

create view [PAS].[LocalConsultantSpecialty] as
select 
	cons.ConsultantCode
	,cons.NationalConsultantCode
	,cons.ProfessionalCarerType
	,cons.Consultant
	,cons.Title
	,cons.StartDate
	,cons.EndDate
	,cons.Deleted
	--,cons.MainSpecialtyCode
	,MainSpecialtyCode = spec.MAIN_IDENT
	,MainSpecialtyDescription = spec.DESCRIPTION
	,APCActivity =
		convert(
			bit
			,case
			when apcspec.ConsultantAPC is null
			then 0
			else 1
			end
		)
		
	,OPActivity =
		convert(
			bit
			,case
			when opspec.ConsultantOP is null
			then 0
			else 1
			end
		)
		
	,RFActivity =
		convert(
			bit
			,case
			when rfspec.ConsultantRF is null
			then 0
			else 1
			end
		)
from PAS.Consultant	cons
left join PAS.SpecialtyBase spec
	on spec.SPECT_REFNO = cons.MainSpecialtyCode
left join	
	(
		select distinct ConsultantCode as ConsultantAPC from APC.Encounter
	) apcspec
	on apcspec.ConsultantAPC = cons.ConsultantCode
	
left join	
	(
		select distinct ConsultantCode as ConsultantOP from OP.Encounter
	) opspec
	on opspec.ConsultantOP = cons.ConsultantCode
	
left join	
	(
		select distinct ConsultantCode as ConsultantRF from RF.Encounter
	) rfspec
	on rfspec.ConsultantRF = cons.ConsultantCode
	
where cons.ParentHealthOrganisation = 'RM2'
and cons.LocalFlag = 'Y'
and cons.NationalConsultantCode <> ''

