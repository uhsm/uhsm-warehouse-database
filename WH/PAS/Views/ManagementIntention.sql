﻿create  view [PAS].[ManagementIntention] as

select
	 ManagementIntentionCode = ReferenceValue.ReferenceValueCode
	,ManagementIntention = ReferenceValue.ReferenceValue
	,NationalManagementIntentionCode = ReferenceValue.MainCode
from
	PAS.ReferenceValue
where
	ReferenceValue.ReferenceDomainCode = 'INMGT'


