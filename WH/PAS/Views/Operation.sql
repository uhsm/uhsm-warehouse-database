﻿create view [PAS].[Operation] as

select
	 OperationCode = OPCS4ID
	,Operation = Opcs4Dx4
	,OperationChapter3 = Opcs4Dx3
from
	PAS.OperationBase
