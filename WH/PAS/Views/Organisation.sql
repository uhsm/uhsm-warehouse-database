﻿CREATE view [PAS].[Organisation] as

select
	 OrganisationCode = HEORG_REFNO
	,Organisation = DESCRIPTION
	,OrganisationTypeCode = HOTYP_REFNO
	,ParentOrganisationCode = PARNT_REFNO
	,OrganisationLocalCode = MAIN_IDENT
from
	PAS.OrganisationBase

