﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		View PatientPracticeHistoryGrouped.

Notes:			Stored in 
				Format cascade data into format required by AE reports

Versions:
				1.0.0.2 - 21/10/2015 - MT
					Use the revised table PatientGPHistory.

				1.0.0.1 - 26/09/2012 - KO
					Removed:
					StartDate = min(RegisteredGp.START_DTTM)
					EndDate = max(RegisteredGp.END_DTTM)

				1.0.0.0
					Original view.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE View [PAS].[PatientPracticeHistoryGrouped]
As

Select
	 PatientNo
	,DistrictNo
	,PracticeRefno
	,PracticeCode
	,PracticeStartDate = min(StartDate)
	,PracticeEndDate = max(EndDate)
From
	PAS.PatientGPHistory

Group By
	 PatientNo
	,DistrictNo
	,PracticeRefno
	,PracticeCode
