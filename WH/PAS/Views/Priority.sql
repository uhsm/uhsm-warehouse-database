﻿create  view [PAS].[Priority] as

select
	 PriorityCode = ReferenceValue.ReferenceValueCode
	,Priority = ReferenceValue.ReferenceValue
	,NationalPriorityCode = ReferenceValue.MainCode
from
	PAS.ReferenceValue
where
	ReferenceValue.ReferenceDomainCode = 'PRITY'


