﻿


CREATE view [PAS].[Purchaser] as

select
	 PurchaserCode = PurchaserBase.PURCH_REFNO
	,PurchaserMainCode = PurchaserBase.MAIN_IDENT
	,Purchaser = PurchaserBase.DESCRIPTION
	,PurchaserTypeCode = PurchaserBase.PUTYP_MAIN_CODE
	,PurchaserType = PurchaserBase.PUTYP_DESCRIPTION
	,StartDate = PurchaserBase.START_DATE
	,EndDate = PurchaserBase.END_DATE
	,ArchiveFlag = PurchaserBase.ARCHV_FLAG
from
	PAS.PurchaserBase



