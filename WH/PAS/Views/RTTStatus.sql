﻿create  view [PAS].[RTTStatus] as

select
	 RTTStatusCode = ReferenceValue.ReferenceValueCode
	,RTTStatus = ReferenceValue.ReferenceValue
	,NationalRTTStatusCode = ReferenceValue.MainCode
from
	PAS.ReferenceValue
where
	ReferenceValue.ReferenceDomainCode = 'RTTST'


