﻿



CREATE view [PAS].[ReferenceValue] as

select
	 ReferenceValueCode = RFVAL_REFNO
	,ReferenceDomainCode = RFVDM_CODE
	,ReferenceValue = DESCRIPTION
	,MainCode = MAIN_CODE
	,ParentReferenceValueCode = PARNT_REFNO
	,SystemModeCode = SMODE_VALUE
	,DefaultValue = DEFAULT_VALUE
	,SortOrder = SORT_ORDER
	,SelectValue = SELECT_VALUE
	,DisplayValue = DISPLAY_VALUE
	,PASCreatedDate = CREATE_DTTM
	,PASModifiedDate = MODIF_DTTM
	,UserCreated = USER_CREATE
	,UserModified = USER_MODIF
	,ReferenceValueTypeCode = RFTYP_CODE
	,ArchiveFlag = ARCHV_FLAG
	,SessionTransaction = STRAN_REFNO
--	,PRIOR_POINTER
	,ExternalKey = EXTERNAL_KEY
	,StartDate = START_DTTM
	,EndDate = END_DTTM
	,LowValue = LOW_VALUE
	,HighValue = HIGH_VALUE
	,ProcessModuleID = PROCESS_MODULE_ID
	,SecurityLevelCode = SCLVL_REFNO
	,SynchronisationCode = SYN_CODE
	,OwnerOrganisationCode = OWNER_HEORG_REFNO
	,MappedCode = ReferenceValueIdentifier.MappedCode

from
	PAS.ReferenceValueBase ReferenceValue

left join PAS.ReferenceValueIdentifier
on	ReferenceValueIdentifier.ReferenceValueCode = ReferenceValue.RFVAL_REFNO
and	ReferenceValueIdentifier.ReferenceValueIdentifierTypeCode in ('NHS', 'NAT')
and	ReferenceValueIdentifier.ArchiveFlag = 'N'
and	not exists
	(
	select
		1
	from
		PAS.ReferenceValueIdentifier Previous
	where
		Previous.ReferenceValueCode = ReferenceValue.RFVAL_REFNO
	and	Previous.ReferenceValueIdentifierTypeCode in ('NHS', 'NAT')
	and	Previous.ArchiveFlag = 'N'
	and	(
			Previous.MappedCode > ReferenceValueIdentifier.MappedCode
		or	(
				Previous.MappedCode = ReferenceValueIdentifier.MappedCode
			and	Previous.ReferenceValueCode > ReferenceValueIdentifier.ReferenceValueCode
			)
		)
	)



