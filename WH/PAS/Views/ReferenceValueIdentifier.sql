﻿
create view [PAS].[ReferenceValueIdentifier] as

select
	 ReferenceValueIdentifierCode = RFVLI_REFNO
	,ReferenceValueCode = RFVAL_REFNO
	,MappedCode = IDENTIFIER
	,PASCreatedDate = CREATE_DTTM
	,PASModifiedDate = MODIF_DTTM
	,UserCreated = USER_CREATE
	,UserModified = USER_MODIF
	,ReferenceValueIdentifierTypeCode = RITYP_CODE
	,ArchiveFlag = ARCHV_FLAG
	,SessionTransaction = STRAN_REFNO
--	,PRIOR_POINTER
	,ExternalKey = EXTERNAL_KEY
	,OwnerOrganisationCode = OWNER_HEORG_REFNO
from
	PAS.ReferenceValueIdentifierBase

