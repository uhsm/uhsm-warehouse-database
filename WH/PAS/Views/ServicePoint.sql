﻿


CREATE view [PAS].[ServicePoint] as

select
	 ServicePointCode = sp.SPONT_REFNO
	,ServicePoint = sp.SPONT_REFNO_NAME
	,SpecialtyCode = sp.SPECT_REFNO
	,ConsultantCode = sp.PROCA_REFNO
	,SiteCode = sp.HEORG_REFNO
	,Location = org.[DESCRIPTION]
	--,ParentLocation = parent.[DESCRIPTION]
	,ServicePointTypeCode = sp.SPTYP_REFNO
	,ServicePointType = sp.SPTYP_REFNO_DESCRIPTION
	,ServicePointLocalCode = sp.SPONT_REFNO_CODE
	,ArchiveFlag = sp.ARCHV_FLAG
from
	PAS.ServicePointBase sp
left join PAS.OrganisationBase org
ON org.HEORG_REFNO = sp.HEORG_REFNO

--left join PAS.OrganisationBase parent
--ON parent.HEORG_REFNO = org.PARNT_REFNO


