﻿
CREATE  view [PAS].[SourceOfReferral] as

select
	 SourceOfReferralCode = ReferenceValue.ReferenceValueCode
	,SourceOfReferral = ReferenceValue.ReferenceValue
	,NationalSourceOfReferralCode = ReferenceValue.MainCode
	,ReportableFlag = 1
	,NewFlag = null
from
	PAS.ReferenceValue
where
	ReferenceValue.ReferenceDomainCode = 'SORRF'



