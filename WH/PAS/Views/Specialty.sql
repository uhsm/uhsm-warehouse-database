﻿

CREATE view [PAS].[Specialty] as

select
	 SpecialtyCode = SpecialtyBase.SPECT_REFNO
	,Specialty = SpecialtyBase.DESCRIPTION
	,LocalSpecialtyCode = SpecialtyBase.MAIN_IDENT
	,NationalSpecialtyCode = 
		coalesce(
			 SpecialtyBase.MAIN_IDENT
			,Parent.MAIN_IDENT
			,SpecialtyBase.MAIN_IDENT
		)

	,DivisionCode = SpecialtyBase.DIVSN_REFNO
	,Division = SpecialtyBase.DIVSN_REFNO_DESCRIPTION

	,TreatmentFunctionFlag = 
		convert(bit, 1)

	,ArchiveFlag =
		convert(
			bit
			,case
			when SpecialtyBase.ARCHV_FLAG = 'N'
			then 0
			else 1
			end
		)

from
	PAS.SpecialtyBase

left join PAS.SpecialtyBase Parent
on	Parent.SPECT_REFNO = SpecialtyBase.PARNT_REFNO
and	Parent.DIVSN_REFNO = 3061 --specialty






