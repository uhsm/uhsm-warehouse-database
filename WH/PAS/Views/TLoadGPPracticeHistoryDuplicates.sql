﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		View TLoadGPPracticeHistoryDuplicates.

Notes:			Stored in 
				TLoadPatientPracticeHistoryDuplicates identifies patient activity where there are 
				duplicate or overlapping values in the PAS.PatientPracticeHistory table (used
				to populate episodic practice data for deriving PCT / CCG code.

Versions:
				1.0.0.1 - 21/10/2015 - MT
					Use the revised table PatientGPHistory.

				1.0.0.0 - 01/05/2013 - KO
					Created view.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE View [PAS].[TLoadGPPracticeHistoryDuplicates]
As

Select 
	FacilityID, 
	NHSNo,
	AdmissionDateTime, 
	EncounterType, 
	COUNT(1) as cnt
from 
	(select * from (
			select FacilityID, NHSNo, AdmissionDateTime, 'APC' as EncounterType
			from WHREPORTING.APC.Episode

		union
			select FacilityID, NHSNo, AppointmentDateTime, 'OP'
			from WHREPORTING.OP.Schedule
			where AppointmentDateTime < getdate()

		union
			select FacilityID, NHSNo, ReferralReceivedDate, 'RF'
			from WHREPORTING.RF.Referral
			where ReferralReceivedDate < getdate()

		union
			select DistrictNo, NHSNumber, DateOnList, 'IW'
			from PTL.IPWL

		union
			select ltrim(rtrim(Patient.rm2number)), ltrim(rtrim(nhsno)), ArrivalDate, 'ED'
			from [CASCADE].TLoadAEAttend Encounter
			left join [CASCADE].AEPatMas Patient
			on	Patient.aepatno = Encounter.aepatno
			where ltrim(rtrim(Patient.rm2number)) <> ''
		) Encounter
		
	left join PAS.PatientGPHistory PracticeHistory
		on PracticeHistory.DistrictNo = Encounter.FacilityID
		and DW_REPORTING.LIB.fn_GetJustDate(Encounter.AdmissionDateTime) >= PracticeHistory.StartDate
		and (DW_REPORTING.LIB.fn_GetJustDate(Encounter.AdmissionDateTime) < PracticeHistory.EndDate Or PracticeHistory.EndDate Is Null)
	)EncounterPractice

group by FacilityID, NHSNo, AdmissionDateTime, EncounterType
having COUNT(1) > 1
