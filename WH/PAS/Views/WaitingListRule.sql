﻿create view PAS.WaitingListRule as

select
	 WaitingListRuleCode = WLRUL_REFNO
	--,SPECT_REFNO
	--,PROCA_REFNO
	--,HEORG_REFNO
	--,MWAIT_DAYCASE
	--,MWAIT_ORDINARY
	--,CREATE_DTTM
	--,MODIF_DTTM
	--,USER_CREATE
	--,USER_MODIF
	,MainCode = CODE
	,WaitingListRule = NAME
	--,COMMENTS
	--,STRAN_REFNO
	--,PRIOR_POINTER
	,ArchiveFlag = ARCHV_FLAG
	--,EXTERNAL_KEY
	--,SVTYP_REFNO
	--,SPONT_REFNO
	--,LOCAL_FLAG
	--,END_DTTM
	--,OWNER_HEORG_REFNO
	--,Created
from
	PAS.WaitingListRuleBase

