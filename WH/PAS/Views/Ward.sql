﻿create view [PAS].[Ward] as

select
	 WardCode = WARDID
	,Ward = WardName
	,SiteCode = HospitalCode
from
	PAS.WardBase
