﻿

CREATE procedure [PTL].[BuildActiveWaitingList] as

/* 
19/6/2013
List of all waiting list rules with active entries
*/


declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
--declare @PasDate datetime

select @StartTime = getdate()

truncate table PTL.ActiveWaitingList
insert into PTL.ActiveWaitingList
select * from PTL.TLoadActiveWaitingList 

select @RowsInserted = @@Rowcount

select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WH BuildActiveWaitingList', @Stats, @StartTime
--print @Stats






