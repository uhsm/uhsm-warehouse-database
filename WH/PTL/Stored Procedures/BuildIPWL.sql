﻿

CREATE procedure [PTL].[BuildIPWL] 
	--@Bulk int
as

/* 
24/07/2013
KO recreate IPWL logic from InfoSQL

Code needs tidying up
*/


declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()


--if(@Bulk = 0)
--	begin
--		--remove archived records dataset
--		delete from PTL.IPWL
--		--select *
--		from PTL.IPWL IPWL
--		inner join Lorenzo.dbo.TLoadWaitingList delta 
--			on IPWL.SourceUniqueID = CAST(delta.WLIST_REFNO AS VARCHAR(20))
--		where delta.ARCHV_FLAG = 'Y'

--		print 'BuildIPWLPreRequisites...'
--		exec PTL.BuildIPWLPreRequisites 0

--		print 'BuildIPWLWaitingStatus...'
--		exec PTL.BuildIPWLWaitingStatus

--		print 'BuildIPWLSuspensions...'
--		exec PTL.BuildIPWLSuspensions

--		print 'ExtractIPWL...'
--		exec PTL.ExtractIPWLCCG

--		print 'LoadIPWL...'
--		exec PTL.LoadIPWLArchive
--		--select @RowsInserted = @@Rowcount
	

--	end
--	--End if(@Bulk = 0)
--if(@Bulk = 1)
--	begin

		print 'BuildIPWLPreRequisites...'
		exec PTL.BuildIPWLPreRequisites 1

		print 'BuildIPWLWaitingStatus...'
		exec PTL.BuildIPWLWaitingStatus

		print 'BuildIPWLSuspensions...'
		exec PTL.BuildIPWLSuspensions

		print 'ExtractIPWLCCG...'
		exec PTL.ExtractIPWLCCGBulk
		
		print 'LoadIPWL...'
		exec PTL.LoadIPWL
		--select @RowsInserted = @@Rowcount

	--end
	--End if(@Bulk = 1)
	
	print 'UpdateIPWLSuspensions...'
	exec PTL.UpdateIPWLSuspensions

	print 'UpdateIPWLWithPreOPDetails...'
	exec PTL.UpdateIPWLWithPreOPDetails

	print 'UpdateIPWLDiagnosticFlag...'
	exec PTL.UpdateIPWLDiagnosticFlag

	print 'UpdateIPWLSchedule6...'
	exec PTL.UpdateIPWLSchedule6
		
	print 'LoadIPWLAdmissionOffers...'
	exec PTL.LoadIPWLAdmissionOffers
		
	select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
	select @Stats = 
		
		--'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
		--'Bulk load = ' + CONVERT(varchar(1), @Bulk) + 
		' Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'
	
	exec WriteAuditLogEvent 'PAS - WH BuildIPWL', @Stats, @StartTime
	print @Stats






