﻿



CREATE PROCEDURE [PTL].[BuildIPWLPreRequisites] 
 	@Bulk int
AS

declare @RowCount Int
declare @StartTime datetime

select @StartTime = getdate()

PRINT 'PTL.BuildIPWLPreRequisites started at '	+ CONVERT(varchar(17), @StartTime)
----------------------------------------------------------------------------------------
--GET WLREFNOS THAT NEED UPDATING
----------------------------------------------------------------------------------------
	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PTL].[IPWLNetChange]') AND type in (N'U'))
	DROP TABLE [PTL].[IPWLNetChange]
	--truncate table PTL.IPWLNetChange
	if(@Bulk = 0)
		begin
		
		--GET ALL IPWLS THAT NEED UPDATING
		--insert into PTL.IPWLNetChange
			select distinct 
				WLIST_REFNO 
			into PTL.IPWLNetChange
			from 
			(
				--Didn't work, had to replace
				--select 
				--	WLIST_REFNO--, SVTYP_REFNO
				--from Lorenzo.dbo.TLoadWaitingList
				--where ARCHV_FLAG = 'N' 
				--and SVTYP_REFNO = 1579
				select
					 WLIST_REFNO = convert(int,Column5)
				from
					Lorenzo.dbo.TImportCSVParsed
				where
					Column0 like 'RM2_GE_OA_WLIST_%'
					and	Valid = 1
					and  isnumeric(Column29) = 1
					and Column57 = 'N'					--ARCHV_FLAG
					and convert(int,Column29) = 1579	--SVTYP_REFNO
			
				union select 
					WLIST_REFNO
				from Lorenzo.dbo.TLoadAdmissionOffer

				union select
					wlist.WLIST_REFNO
				from Lorenzo.dbo.TLoadAdmissionDecision admdec 
				inner join Lorenzo.dbo.WaitingList wlist 
					on wlist.ADMDC_REFNO = admdec.ADMDC_REFNO
				where admdec.ARCHV_FLAG = 'N' 
				and wlist.ARCHV_FLAG = 'N'
				
				--Didn't work, had to replace
				--union select 
				--	wlist_refno
				--from Lorenzo.dbo.TLoadProfessionalCareEpisode
				--where wlist_refno is not null
				union select 
					convert(numeric,Column6)
				from Lorenzo.dbo.TImportCSVParsed
				where Column0 like 'RM2_GE_IP_PRCAE_%'
				and	Valid = 1
				and  ISNUMERIC(Column6) = 1

				union select 
					WLIST_REFNO
				from Lorenzo.dbo.TLoadWaitingListSuspension

				union select 
					SORCE_REFNO
				from Lorenzo.dbo.TLoadClinicalCoding coding
				inner join Lorenzo.dbo.WaitingList wlist 
					on coding.SORCE_REFNO = wlist.WLIST_REFNO
				where 
					coding.SORCE_CODE = 'WLIST'
					and coding.DPTYP_CODE = 'PROCE'
					and wlist.SVTYP_REFNO = 1579

				union select
					SORCE_REFNO 
				from Lorenzo.dbo.TLoadNoteRole note
				where SORCE_CODE in (
					'WLCMT',
					'WLCNF',
					'WLOUT',
					'WLPHN',
					'WLPRP')

				union select
					cast(SourceUniqueID as int) as wlist_refno
				from PTL.IPWL
				where NextResetDate = (cast(floor(cast(getdate() as float)) as datetime) -1)
			) wlsource

			select @RowCount = @@rowcount
		end
	--End non-bulk

	if(@Bulk = 1)
		begin
			--insert into PTL.IPWLNetChange
			select distinct WLIST_REFNO
			into PTL.IPWLNetChange
			from Lorenzo.dbo.WaitingList
			where ARCHV_FLAG = 'N' and SVTYP_REFNO = 1579
			--and MODIF_DTTM > '2013-09-01 00:00:00.000' --date swapped to DW build from InfoSQL build

			select @RowCount = @@rowcount
		end
	--End bulk

alter table PTL.IPWLNetChange alter column [WLIST_REFNO] [numeric](18, 0) NOT NULL
alter table PTL.IPWLNetChange add PRIMARY KEY CLUSTERED ([WLIST_REFNO] ASC)

PRINT CONVERT(varchar(17), getdate())
	+ ': ' + 'IPWLNetChange completed. ' 
	+ CONVERT(varchar(10), @RowCount)  + ' records inserted.'
		
------------------------------------------------------------------------------------------
----TCI DATES
------------------------------------------------------------------------------------------
--drop table PTL.IPWLTCIDates
truncate table PTL.IPWLTCIDates

insert into PTL.IPWLTCIDates
	select 
		admoff.WLIST_REFNO
		,admoff.ADMOF_REFNO
		,admoff.ADMOF_DTTM
		,TCIDate = cast(admoff.TCI_DTTM as datetime)
	--into PTL.IPWLTCIDates
	from Lorenzo.dbo.AdmissionOffer admoff 
	inner join 
		(
		select 
			ao.WLIST_REFNO
			,LastOffer = max(ao.ADMOF_REFNO)
		from Lorenzo.dbo.AdmissionOffer ao
		inner join PTL.IPWLNetChange changed 
			on changed.WLIST_REFNO = ao.WLIST_REFNO
		where ao.ARCHV_FLAG = 'N'
		and ao.OFOCM_REFNO not in (
			'2003599',
			'2000657',
			'2003600',
			'985',
			'2003601',
			'2003604',
			'982',
			'2003597',
			'2003605',
			'2005636',
			'4423',
			'2003598', --added 24-07-2008 admitted - treatment deferred
			'3663'
			)   
		group by ao.wlist_refno
		)
	lastoff 
	on admoff.admof_refno = lastoff.lastoffer

select @RowCount = @@rowcount
PRINT CONVERT(varchar(17), getdate())
	+ ': ' + 'IPWLTCIDates completed. ' 
	+ CONVERT(varchar(10), @RowCount)  + ' records inserted.'
		
------------------------------------------------------------------------------------------
----RESCHEDULES
------------------------------------------------------------------------------------------
--drop table PTL.IPWLReschedules
truncate table PTL.IPWLReschedules

insert into PTL.IPWLReschedules
	select
		admoff.WLIST_REFNO
		,ResetDate = max(DATEADD(dd, 0, DATEDIFF(dd, 0, admoff.TCI_DTTM)) )
		--,ResetDate = max(admoff.TCI_DTTM)
		--into PTL.IPWLReschedules
	from Lorenzo.dbo.AdmissionOffer admoff
	inner join PTL.IPWLNetChange changed 
		on changed.WLIST_REFNO = admoff.WLIST_REFNO
	where admoff.OFOCM_REFNO IN (
		'2003599',	--Patient Death
		'2000657', 	--Cancelled By Patient
		'2003600',	--DNA
		'985',		--DNA
		'2003601')	--NO Contact
	and admoff.ARCHV_FLAG = 'N' 
	and DATEADD(dd, 0, DATEDIFF(dd, 0, admoff.TCI_DTTM)) < DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())) 
	--and admoff.TCI_DTTM < GETDATE()
	group by admoff.WLIST_REFNO 

select @RowCount = @@rowcount
PRINT CONVERT(varchar(17), getdate())
	+ ': ' + 'IPWLReschedules completed. ' 
	+ CONVERT(varchar(10), @RowCount)  + ' records inserted.'

------------------------------------------------------------------------------------------
----START DATES
------------------------------------------------------------------------------------------
--drop table PTL.IPWLStartDates
truncate table PTL.IPWLStartDates

insert into PTL.IPWLStartDates
	select 
		wl.WLIST_REFNO
		,wl.WLIST_DTTM
		,resc.ResetDate
		--,wl.WAITING_START_DTTM
		,WaitingStartDate = DATEADD(dd, 0, DATEDIFF(dd, 0, 
			CAST(ISNULL(resc.ResetDate,wl.WAITING_START_DTTM) AS DATETIME) 
		))
		
	--INTO PTL.IPWLStartDates
	from Lorenzo.dbo.WaitingList wl
	inner join PTL.IPWLNetChange changed 
			on changed.WLIST_REFNO = wl.WLIST_REFNO
	left join PTL.IPWLReschedules resc 
		on wl.WLIST_REFNO = resc.WLIST_REFNO
	where wl.SVTYP_REFNO = '1579' 
	and wl.ARCHV_FLAG = 'N'

select @RowCount = @@rowcount
PRINT CONVERT(varchar(17), getdate())
	+ ': ' + 'IPWLStartDates completed. ' 
	+ CONVERT(varchar(10), @RowCount)  + ' records inserted.'


----GET WAITING_START_DATES FOR CURRENT WAITERS (FOR USE IN SUSPENSIONS DATASET)

insert into PTL.IPWLStartDates (WLIST_REFNO, WaitingStartDate)
select 
	WLIST_REFNO = CAST(SourceUniqueID AS INT)
	,WaitingStartDate
from PTL.IPWL ipwl
--where DATA_SOURCE = 'IPM'
where not exists (
SELECT 1 FROM PTL.IPWLStartDates IPS where ipwl.SourceUniqueID = IPS.WLIST_REFNO)









