﻿

CREATE PROCEDURE [PTL].[BuildIPWLSuspensions] AS 

declare @RowCount Int
declare @StartTime datetime

select @StartTime = getdate()

PRINT 'PTL.BuildIPWLSuspensions started at '	+ CONVERT(varchar(17), @StartTime)
/*
--------------------------------------------------------------------------------
CREATE IPM DATASET
--------------------------------------------------------------------------------
*/
--drop table PTL.IPWLSuspensions
truncate table PTL.IPWLSuspensions

insert into PTL.IPWLSuspensions
select
	--'IPM' AS DATA_SOURCE,
	WaitingListSuspension.WLIST_REFNO
	,WaitingListSuspension.WSUSP_REFNO
	,WaitingListSuspension.SUSRS_REFNO
	,WaitingListSuspension.SUSRS_REFNO_MAIN_CODE
	,WaitingListSuspension.SUSRS_REFNO_DESCRIPTION
	,WaitingListSuspension.START_DTTM
	,WaitingListSuspension.end_DTTM

	--TOTAL SUSP DAYS TO DATE AFTER WAITING START DATE
	,SUSP_TO_DATE = case 
		when WaitingListSuspension.end_DTTM 
			< IPWLStartDates.WaitingStartDate 
		then 0
		else
			case 
				when WaitingListSuspension.START_DTTM > GETDATE() 
				then	0
				else 
					case 
						when WaitingListSuspension.end_DTTM > GETDATE() 
						then 
							case 
								when WaitingListSuspension.START_DTTM 
									> IPWLStartDates.WaitingStartDate 
								then DATEDIFF(dd,WaitingListSuspension.[START_DTTM],GETDATE())
								else DATEDIFF(dd,IPWLStartDates.WaitingStartDate,GETDATE()) 
							end
						else 
							case 
								when WaitingListSuspension.START_DTTM > IPWLStartDates.WaitingStartDate 
								then DATEDIFF(dd,WaitingListSuspension.[START_DTTM],WaitingListSuspension.end_dttm)
								else DATEDIFF(dd,IPWLStartDates.WaitingStartDate,WaitingListSuspension.[end_DTTM])
							end				

					end
			end
		end 

	--TOTAL SUSP DAYS TO DATE AFTER WAITING START DATE EXCLUDING FUTURE SUSPENSIONS
	,SUSPENSION_PERIOD = case 
		when CAST(WaitingListSuspension.end_DTTM AS DATETIME) < IPWLStartDates.WaitingStartDate 
		then 0
		else
			case 
				when WaitingListSuspension.START_DTTM > GETDATE() 
				then	0
				else 
					case 
						when WaitingListSuspension.START_DTTM > IPWLStartDates.WaitingStartDate 
						then DATEDIFF(dd,WaitingListSuspension.[START_DTTM],WaitingListSuspension.end_dttm)
						else DATEDIFF(dd,IPWLStartDates.WaitingStartDate,WaitingListSuspension.end_dttm) 
					end
			end
	end
	
	,CURRENT_SUSPENSION = case 
		when GETDATE() >= WaitingListSuspension.START_DTTM AND GETDATE() <= WaitingListSuspension.end_DTTM 
		then 'Y'
		else 'N'
	end 


	--RTT SUSPENSIONS DO NOT TAKE INTO ACCOUNT ANY IP WAITING 'RESTART DATES' THEREFORE NEED TO INCLUDE ALL SUSPendED DAYS
	--REGARDLESS OF IP WAITING START DATE
	--RTT SUSP DAYS TO DATE
	,RTT_SUSP_TO_DATE = case 
		when WaitingListSuspension.START_DTTM > GETDATE() 
		then	0
		else 
			case 
				when WaitingListSuspension.end_DTTM > GETDATE() 
				then DATEDIFF(dd,WaitingListSuspension.[START_DTTM],Cast(Floor(Cast(GETDATE()AS float)) as Datetime))
				else DATEDIFF(dd,WaitingListSuspension.[START_DTTM],WaitingListSuspension.end_dttm)
			end
	end 

--RTT TOTAL SUSP DAYS TO DATE EXCLUDING FUTURE SUSPENSIONS
	,RTT_SUSPENSION_PERIOD = case 
		when WaitingListSuspension.START_DTTM > GETDATE() 
		then	0
		else DATEDIFF(dd,WaitingListSuspension.[START_DTTM],WaitingListSuspension.end_dttm)
	end  
	,WaitingListSuspension.COMMENTS
--INTO PTL.IPWLSuspensions

from Lorenzo.dbo.WaitingListSuspension WaitingListSuspension
left join PTL.IPWLStartDates IPWLStartDates 
on IPWLStartDates.WLIST_REFNO = WaitingListSuspension.WLIST_REFNO
where WaitingListSuspension.ARCHV_FLAG = 'N' 

select @RowCount = @@rowcount
PRINT CONVERT(varchar(17), getdate())
	+ ': ' + 'IPWLSuspensions built. ' 
	+ CONVERT(varchar(10), @RowCount)  + ' records inserted.'

