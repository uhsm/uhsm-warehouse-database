﻿

CREATE procedure [PTL].[BuildIPWLWaitingStatus] AS 

declare @RowCount Int
declare @StartTime datetime

select @StartTime = getdate()

PRINT 'PTL.BuildIPWLWaitingStatus started at '	+ CONVERT(varchar(17), @StartTime)

--drop table PTL.IPWLWaitingStatus
truncate table PTL.IPWLWaitingStatus

insert into PTL.IPWLWaitingStatus
select
	SourceUniqueID = WaitingList.WLIST_REFNO
	,RemovalReasonCode = WaitingList.REMVL_REFNO
	,RemovalReason = Outcome.ReferenceValue
	,WLRemovalStatus = Outcome.MappedCode
	,LastTCIOfferRef = LastTCIOffer.ADMOF_REFNO
	,OfferOutcome = LastTCIOffer.OUTCOME
	,OfferRemovalStatus = LastTCIOffer.RemovalStatus
	,LastTCIOffer.LastTCIOffer
	,LastPCEWaitingListRef = LastProfessionalCareEpisode.WLIST_REFNO 
	,LastPCEStartDate = LastProfessionalCareEpisode.START_DTTM
	,LastPCEEndDate = LastProfessionalCareEpisode.END_DTTM
	--,LastProfessionalCareEpisode.CEOCM_REFNO
	,LastPCEOfferRef = LastProfessionalCareEpisode.ADMOF_REFNO
	--,LastProfessionalCareEpisode.PRCAE_REFNO
	--,LastProfessionalCareEpisode.PRVSP_REFNO
	,PCEOutcome = PCEOutcome.ReferenceValue
	,PCERemovalStatus = PCEOutcome.MappedCode
	--,EPI_NO_ADMOFF_WAITSTAT = I.MappedCode
	--EPI WL STAT ARRIVES AT THE STATUS VIA ADMOFFERS, IF EPISODE HAS NO ADM OFFERS T
	--THEN LOOK FOR WLIST_REFNO IN PROFCARER EPI
	--,WaitingStatus = case 
	--	when not PCEOutcome.MappedCode is null then PCEOutcome.MappedCode
	--	else
	--		case when not LastTCIOffer.RemovalStatus is null then LastTCIOffer.RemovalStatus
	--		else Outcome.MappedCode
	--		end
	--	end

	,EntryStatus = CASE WHEN NOT LastProfessionalCareEpisode.START_DTTM IS NULL 
		AND LastProfessionalCareEpisode.END_DTTM IS NULL THEN 'CURR-IP'
		ELSE
			CASE WHEN NOT WaitingList.REMVL_DTTM  IS NULL AND
				(
				CASE WHEN Outcome.MappedCode = 'REMOVED' THEN 
					CASE WHEN NOT PCEOutcome.MappedCode IS NULL THEN PCEOutcome.MappedCode
					ELSE
					Outcome.MappedCode
					END
				ELSE
					CASE WHEN PCEOutcome.MappedCode IS NULL THEN
						CASE WHEN LastTCIOffer.RemovalStatus IS NULL THEN Outcome.MappedCode
						ELSE
						LastTCIOffer.RemovalStatus
						END
					ELSE
					PCEOutcome.MappedCode
					END
				END
				) = 'WAITING' THEN 'REMOVED'
			ELSE
				CASE WHEN Outcome.MappedCode = 'REMOVED' THEN 
					CASE WHEN NOT PCEOutcome.MappedCode IS NULL THEN PCEOutcome.MappedCode
					ELSE
					Outcome.MappedCode
					END
				ELSE
					CASE WHEN PCEOutcome.MappedCode IS NULL THEN
						CASE WHEN LastTCIOffer.RemovalStatus IS NULL THEN Outcome.MappedCode
						ELSE
						LastTCIOffer.RemovalStatus
						END
					ELSE
					PCEOutcome.MappedCode
					END
				END
			END 

	END 

--into PTL.IPWLWaitingStatus
from Lorenzo.dbo.WaitingList WaitingList 
inner join PTL.IPWLNetChange NetChange 
	on NetChange.wlist_refno = WaitingList.WLIST_REFNO 
	
left join PTL.IPWLLookups Outcome
	on Outcome.ReferenceValueCode = WaitingList.REMVL_REFNO

left join 
	(
	select
		AdmissionOffer.WLIST_REFNO
		,AdmissionOffer.ADMOF_REFNO
		,AdmissionOffer.OFOCM_REFNO
		,Outcome = Outcome.ReferenceValue
		,RemovalStatus = Outcome.MappedCode
		,LastTCIOffer = CAST(AdmissionOffer.TCI_DTTM AS DATETIME) 
	from Lorenzo.dbo.AdmissionOffer AdmissionOffer 
	inner join 
		(
		select 
			WLIST_REFNO
			,LatestOffer = max(ADMOF_REFNO)
		from Lorenzo.dbo.AdmissionOffer 
		where ARCHV_FLAG = 'N' --AND CAST(TCI_DTTM AS DATETIME) <= (GETDATE()-1)
		group by WLIST_REFNO
		) LastAdmissionOffer 
		on AdmissionOffer.ADMOF_REFNO = LastAdmissionOffer.LatestOffer
	left join PTL.IPWLLookups Outcome
		on Outcome.ReferenceValueCode = AdmissionOffer.OFOCM_REFNO
		where ARCHV_FLAG = 'N'
	) LastTCIOffer 
	on LastTCIOffer.WLIST_REFNO = WaitingList.WLIST_REFNO
	
--LEFT OUTER JOIN PROFCAREREPI D ON C.ADMOF_REFNO = D.ADMOF_REFNO AND D.ARCHV_FLAG = 'N' AND D.PRVSN_FLAG = 'N'
left join
	(
	select 
		ProfessionalCareEpisode.PRCAE_REFNO,
		ProfessionalCareEpisode.WLIST_REFNO,
		ProfessionalCareEpisode.CEOCM_REFNO,
		ProfessionalCareEpisode.PRVSP_REFNO,
		left(ProfessionalCareEpisode.START_DTTM,16) as START_DTTM,
		left(ProfessionalCareEpisode.END_DTTM,16) AS END_DTTM,
		ProfessionalCareEpisode.ADMOF_REFNO,
		ProfessionalCareEpisode.PRVSN_FLAG,
		ProfessionalCareEpisode.ARCHV_FLAG
	from Lorenzo.dbo.ProfessionalCareEpisode ProfessionalCareEpisode 
	inner join
		(
		select 
			WLIST_REFNO
			,LatestPCE = max(PRCAE_REFNO)
		from Lorenzo.dbo.ProfessionalCareEpisode 
		where ARCHV_FLAG = 'N' 
		and PRVSN_FLAG = 'N' 
		and not WLIST_REFNO is null
		group by WLIST_REFNO
		)LastPCE 
		on LastPCE.LatestPCE = ProfessionalCareEpisode.PRCAE_REFNO
	) LastProfessionalCareEpisode 
	ON LastProfessionalCareEpisode.ADMOF_REFNO = LastTCIOffer.ADMOF_REFNO  
	AND LastProfessionalCareEpisode.ARCHV_FLAG = 'N' 
	AND LastProfessionalCareEpisode.PRVSN_FLAG = 'N'

left join PTL.IPWLLookups PCEOutcome
	on PCEOutcome.ReferenceValueCode = LastProfessionalCareEpisode.CEOCM_REFNO	


--LEFT OUTER JOIN PATIENTS F ON A.PATNT_REFNO = F.PATNT_REFNO AND F.ARCHV_FLAG = 'N'
--left join
--	(
--	select 
--		A.PRCAE_REFNO,
--		A.WLIST_REFNO,
--		A.CEOCM_REFNO,
--		A.PRVSP_REFNO,
--		A.START_DTTM,
--		A.END_DTTM,
--		A.ADMOF_REFNO
--	FROM Lorenzo.dbo.ProfessionalCareEpisode A
--	INNER JOIN
--		(
--		SELECT 
--		WLIST_REFNO,
--		MAX(PRCAE_REFNO) AS LATEST_EPI
--		FROM Lorenzo.dbo.ProfessionalCareEpisode WHERE ARCHV_FLAG = 'N' AND PRVSN_FLAG = 'N' AND NOT WLIST_REFNO IS NULL
--		GROUP BY WLIST_REFNO
--		) B ON A.PRCAE_REFNO = B.LATEST_EPI
--	) G 
--	ON  G.WLIST_REFNO = WaitingList.WLIST_REFNO

--left join Lorenzo.dbo.ReferenceValue H 
--	ON G.CEOCM_REFNO = H.RFVAL_REFNO

--left join PTL.IPWLLookups I 
--	ON H.RFVAL_REFNO = I.ReferenceValueCode
	
WHERE WaitingList.SVTYP_REFNO = '1579' 
AND WaitingList.ARCHV_FLAG = 'N' 
select @RowCount = @@rowcount
PRINT CONVERT(varchar(17), getdate())
	+ ': ' + 'IPWLWaitingStatus built. ' 
	+ CONVERT(varchar(10), @RowCount)  + ' records inserted.'









