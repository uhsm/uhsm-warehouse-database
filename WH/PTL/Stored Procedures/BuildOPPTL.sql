﻿
/*
--Author: K Oakden
--Date created: 04/11/2012
Build the OP PTL 

--Dependencies
PTL.OPPTLInclusions
*/

CREATE procedure [PTL].[BuildOPPTL] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @PasDate datetime

select @StartTime = getdate()
--Get PAS Date as last updated date in referral table 
--Deduct 1 hour in case the last referral update occurred after midnight
--select @PasDate = MAX(pasUpdated) from RF.Encounter
select @pasdate = max(DATEADD(dd, 0, DATEDIFF(dd, 0, dateadd(hour, -1, PASUpdated)))) from RF.Encounter
exec PTL.GetOPRescheduleCount
exec PTL.GetOPPTLInclusions
exec PTL.LoadOPWL
update PTL.OPWL set CensusDate  = @PasDate

select @RowsInserted = @@Rowcount
select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WH BuildOPPTL', @Stats, @StartTime


--Archive on 1st day of month
select @Stats = 'Archive not run'

if(datepart(DAY, GETDATE()) = 1)
	begin

	insert into PTL.OPWLArchive
	select 
		CensusDate
		,ReferralSourceUniqueID
		,ReferredToSpecialty
		,ReferredToSpecialtyCode
		,ReferredToConsultant
		,ReferredToConsultantCode
		,SourcePatientNo
		,PatientId
		,NHSNumber
		,PatientSurname
		,PatientPostcode
		,ReferralPriority
		,DateOnReferrersWL
		,DateOfReferral
		,WaitingListStartDate
		,CABAdjustedReferralDate
		,RTTReferralDate
		,RTTReferralDateFlag
		,RTTBreachDate
		,RTTWeeksWait
		,FirstAppointmentStatus
		,AppointmentSourceUniqueID
		,CurrentAppointmentType
		,CurrentClinic
		,CurrentAppointmentDate
		,ActivityType
		,ReferralSource
		,ReferralStatus
		,ReferringPCT
		,ReferringPracticeCode
		,NumberOfPatientCancellations
		,NumberOfHospitalCancellations
		,NumberOfPatientReschedules
		,NumberOfDNAs
		,WLSourceUniqueID
		,Contract_DateOfBirth
		,Contract_Age
		,Contract_Sex
		,Contract_ReferringGpCode
		,Contract_ReferringPracticeCode
		,Contract_ReferringCCGCode
		,Contract_ReferringCCG
		,Contract_SourceOfReferralCode
		,Contract_Site
		,Contract_PPID
		,Contract_Ethnicity
		,Contract_UBRN
		,Contract_RTTCurrentStatusCode
		,Contract_ConsultantCode
		,Contract_SpecialtyCode
		,Contract_Provider
		,Contract_RegisteredGpCode
		,Contract_RegisteredGpPracticeCode
		,Contract_RegisteredPCTCode
		,Contract_ReferralPriority
		,Contract_LastDNAPatCancDate
		,Archivedate = CONVERT(VARCHAR(8), GETDATE(), 112)
	--into PTL.OPWLArchive
	from PTL.OPWL

	select @RowsInserted = @@rowcount
	select @Stats = 'Rows archived to OPWLArchive = ' + CONVERT(varchar(10), @RowsInserted) 
	exec WriteAuditLogEvent 'PAS - WH BuildOPPTL: ', @Stats, @StartTime
end

--Archive every Sunday

if(datepart(DW, GETDATE()) = 1) --Sunday
	begin

	insert into PTL.OPWLWeeklyArchive
	select 
		CensusDate
		,ReferralSourceUniqueID
		,ReferredToSpecialty
		,ReferredToSpecialtyCode
		,ReferredToConsultant
		,ReferredToConsultantCode
		,SourcePatientNo
		,PatientId
		,NHSNumber
		,PatientSurname
		,PatientPostcode
		,ReferralPriority
		,DateOnReferrersWL
		,DateOfReferral
		,WaitingListStartDate
		,CABAdjustedReferralDate
		,RTTReferralDate
		,RTTReferralDateFlag
		,RTTBreachDate
		,RTTWeeksWait
		,FirstAppointmentStatus
		,AppointmentSourceUniqueID
		,CurrentAppointmentType
		,CurrentClinic
		,CurrentAppointmentDate
		,ActivityType
		,ReferralSource
		,ReferralStatus
		,ReferringPCT
		,ReferringPracticeCode
		,NumberOfPatientCancellations
		,NumberOfHospitalCancellations
		,NumberOfPatientReschedules
		,NumberOfDNAs
		,WLSourceUniqueID
		,Contract_DateOfBirth
		,Contract_Age
		,Contract_Sex
		,Contract_ReferringGpCode
		,Contract_ReferringPracticeCode
		,Contract_ReferringCCGCode
		,Contract_ReferringCCG
		,Contract_SourceOfReferralCode
		,Contract_Site
		,Contract_PPID
		,Contract_Ethnicity
		,Contract_UBRN
		,Contract_RTTCurrentStatusCode
		,Contract_ConsultantCode
		,Contract_SpecialtyCode
		,Contract_Provider
		,Contract_RegisteredGpCode
		,Contract_RegisteredGpPracticeCode
		,Contract_RegisteredPCTCode
		,Contract_ReferralPriority
		,Contract_LastDNAPatCancDate
		,Archivedate = CONVERT(VARCHAR(8), GETDATE(), 112)
	--into PTL.OPWLArchive
	from PTL.OPWL

	select @RowsInserted = @@rowcount
	select @Stats = 'Rows archived to OPWLWeeklyArchive = ' + CONVERT(varchar(10), @RowsInserted) 
	exec WriteAuditLogEvent 'PAS - WH BuildOPPTL: ', @Stats, @StartTime
end

--print @Stats

