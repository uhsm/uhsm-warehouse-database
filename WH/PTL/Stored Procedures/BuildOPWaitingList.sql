﻿CREATE procedure [PTL].[BuildOPWaitingList] as

/**----------------------------------------------------------------------------
KO 
Date		Author		Action		Description
-------------------------------------------------------------------------------
17/10/2014	KO			Create		To replicate InfoSQL waiting_listv1 table

Not to be confused with PartialBooking waiting list, 
or OPWL (PTL) which is based on referrals rather than waiting list entries

------------------------------------------------------------------------------**/
declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

exec PTL.GetOPWaitingListResets

if Object_id('tempdb..#wl', 'U') is not null
	drop table #wl

select 	
	WLRefNo = wl.wlist_refno, 
	ConsultantCode = wlConsultant.proca_refno_main_ident,
	ConsultantSurname = wlConsultant.surname,
	DateOnList = wl.WLIST_DTTM,
	WLSpecialty = wlSpecialty.[DESCRIPTION],
	WLSpecialtyCode = wlSpecialty.MAIN_IDENT,
	wl.waiting_start_dttm,
	appt.ClinicCode,
	ref.ReferralDate,
	appt.AppointmentDate,
	WLPriority = wlPriority.[DESCRIPTION],
	VisitType = wlVisitType.[DESCRIPTION],
	DaysWaitToAppt = 
		case 
			when resets.LastResetDate is null 
			then datediff (day, wl.waiting_start_dttm, appt.AppointmentDate)
			else datediff(day,resets.LastResetDate, appt.AppointmentDate)
		end,
	ref.DistrictNo,
	ref.PatientSurname,
	ReferralSourceUniqueID = ref.SourceUniqueID
into #WL
from Lorenzo.dbo.WaitingList wl

left join RF.Encounter ref 
	on ref.SourceUniqueID = wl.REFRL_REFNO
	
left join OP.Encounter appt
	on appt.WaitingListSourceUniqueID = wl.WLIST_REFNO
	and appt.FirstAttendanceFlag = '9268' --New appointemnt
	and appt.AppointmentCancelDate is null 
	and appt.AppointmentStatusCode <> '358' --DNA

left join Lorenzo.dbo.ProfessionalCarer wlConsultant 
	on wl.proca_refno = wlConsultant.proca_refno

left join Lorenzo.dbo.Specialty wlSpecialty 
	on wl.SPECT_REFNO = wlSpecialty.spect_refno 
	and wlSpecialty.ARCHV_FLAG = 'N'

left join PAS.ReferenceValueBase wlPriority 
	on wl.prity_refno = wlPriority.RFVAL_REFNO
	
left join PAS.ReferenceValueBase wlVisitType 
	on wl.VISIT_REFNO = wlVisitType.RFVAL_REFNO

left join PTL.OPWaitingListResets resets 
	on wl.WLIST_REFNO = resets.wlist_refno

where wl.archv_flag = 'N' 
and wl.svtyp_refno = '4223'
and wl.remvl_dttm is null 
and isnull(wl.WLRUL_REFNO, 150000536) <> 150000536 --Follow up OPWL



-------------------------------------------------------
--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PTL].[OPWaitingList]') AND type in (N'U'))
--DROP TABLE [PTL].[OPWaitingList]
truncate table PTL.OPWaitingList
insert into PTL.OPWaitingList
select distinct 
	wl.AppointmentDate
	,wl.ClinicCode
	,wl.ConsultantCode
	,wl.ConsultantSurname
	,wl.ReferralDate
	,wl.VisitType
	,wl.DaysWaitToAppt
	,wl.WLPriority
	,wl.WLRefNo
	,wl.WLSpecialty
	,wl.WLSpecialtyCode
	,wl.DistrictNo
	,wl.PatientSurname
	,WaitingStartDate = 
		case 
			when resets.LastResetDate is null
			then cast(wl.DateOnList as datetime)
			else dateadd(day, 0, datediff(day, 0, resets.LastResetDate))
		end
		
	,NextReset = resets.NextResetDate
	,CancerBreachDate = 
		case 
			when resets.LastResetDate is null
			then dateadd(day, 14, wl.DateOnList)
			else dateadd(day, 14, resets.LastResetDate)
		end
	,wl.ReferralSourceUniqueID
	,wl.DateOnList
	,CensusDate = dateadd(day, 0, datediff(day, 0,GETDATE()))

--into PTL.OPWaitingList
from #WL wl

left join PTL.OPWaitingListResets resets
	on wl.wlrefno = resets.wlist_refno

select @RowsInserted = @@rowcount
	
drop table #WL

select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WH BuildOPWaitingList', @Stats, @StartTime
