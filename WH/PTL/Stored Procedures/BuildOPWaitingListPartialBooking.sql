﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Build OP Waiting List Partial Booking

Notes:			Stored in PTL

Versions:
				1.0.0.5 - 07/12/2015 - MT
					Add Stage field (derived from STAGE_REFNO on Lorenzo waiting list).
					Requested by Kelly McLellan to be added to the PBFU_FullwaitingList SSRS report.

				1.0.0.4 - 16/01/2015 - JB
					Update table to show patients who have ever had a cancer diagnosis.

				1.0.0.3 - 17/10/2014 - KO
					Moved build of reset table to a seperate sp because it will also be used by another sp.

				1.0.0.2 - 29/01//2014 - CM
					Change to Weeks Wait Calculation - details commmented in script below.

				1.0.0.1 - 27/01/2014 - CM
					Change to Lead Time Calculation - details commmented in script below.

				1.0.0.0 - 19/02/2013 - KO
					Copy of sp_create_waiting_List_Partial_Booking on InfoSQL
					to check users choosing correct waiting list.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [PTL].[BuildOPWaitingListPartialBooking]
As

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
--declare @PasDate datetime

select @StartTime = getdate()

exec PTL.GetOPWaitingListResets

--Identify PB waiting lists
If exists (Select * from Sysobjects Where name = N'WL_PB')
drop table PTL.WL_PB
select 	
	a.wlist_refno as 'WLRefNo', 
	a.refrl_refno as 'Refrl_ref', 
	a.patnt_refno as 'Pat_ref',
	a.spect_refno as Spec_ref,
	c.surname,
	a.wlist_dttm as 'Date_on_list',
	a.waiting_start_dttm as 'Waiting_start_date',	
	a.user_create as 'creator',
	cast(a.create_dttm as datetime) as 'Create_Date',
	a.USER_MODIF as 'modifier',
	cast(a.MODIF_DTTM as datetime) as 'Modified_Date',
	M.CODE AS WL_Clinic_Code,
	wlr.NAME AS WL_NAME,
	a.visit_refno AS 'WL_visit_type',
	InviteDate = CAST(a.INVIT_DTTM AS DATETIME),
	VisitType = Visit.[DESCRIPTION],
	Stage = Stage.[DESCRIPTION]
Into PTL.WL_PB
From Lorenzo.dbo.WaitingList a 
left join Lorenzo.dbo.ProfessionalCarer c on a.proca_refno = c.proca_refno
LEFT JOIN Lorenzo.dbo.ServicePoint M ON A.SPONT_REFNO = M.SPONT_REFNO
left join Lorenzo.dbo.WaitingListRule wlr on A.WLRUL_REFNO = wlr.WLRUL_REFNO
LEFT JOIN Lorenzo.dbo.ReferenceValue Visit ON a.VISIT_REFNO = Visit.RFVAL_REFNO
Left Join Lorenzo.dbo.ReferenceValue Stage On a.STAGE_REFNO = Stage.RFVAL_REFNO
where 
remvl_dttm is null 
and a.archv_flag = 'N' 
and A.svtyp_refno = '4223'
AND a.WLRUL_REFNO = 150000536 -- this is the old method of identifying Patial Booking waiting list - essentially all patient added to this waiting list is for partial booking
and cast(a.create_dttm as datetime) < '2013-02-20 00:00:00.000'

insert into PTL.WL_PB
select 	
	a.wlist_refno as 'WLRefNo', 
	a.refrl_refno as 'Refrl_ref', 
	a.patnt_refno as 'Pat_ref',
	a.spect_refno as Spec_ref,
	c.surname,
	a.wlist_dttm as 'Date_on_list',
	a.waiting_start_dttm as 'Waiting_start_date',	
	a.user_create as 'creator',
	cast(a.create_dttm as datetime) as 'Create_Date',
	a.USER_MODIF as 'modifier',
	cast(a.MODIF_DTTM as datetime) as 'Modified_Date',
	M.CODE AS WL_Clinic_Code,
	wlr.NAME AS WL_NAME,
	a.visit_refno AS 'WL_visit_type',
	InviteDate = CAST(a.INVIT_DTTM AS DATETIME),
	VisitType = Visit.[DESCRIPTION],
	Stage = Stage.[DESCRIPTION]
from Lorenzo.dbo.WaitingList a 
left join Lorenzo.dbo.ProfessionalCarer c on a.proca_refno = c.proca_refno
LEFT JOIN Lorenzo.dbo.ServicePoint M ON A.SPONT_REFNO = M.SPONT_REFNO
left join Lorenzo.dbo.WaitingListRule wlr on A.WLRUL_REFNO = wlr.WLRUL_REFNO
LEFT JOIN Lorenzo.dbo.ReferenceValue Visit ON a.VISIT_REFNO = Visit.RFVAL_REFNO
Left Join Lorenzo.dbo.ReferenceValue Stage On a.STAGE_REFNO = Stage.RFVAL_REFNO
where 
remvl_dttm is null 
and a.archv_flag = 'N' 
and A.svtyp_refno = '4223'
and a.visit_refno in ('8911', '2003436') --follow up.  This is the new way of identifying partial booking - all WL entries which have been added with the appointment type of follow up.
and cast(a.create_dttm as datetime) > '2013-02-19 23:59:59.000'

If Exists (Select * from Sysobjects Where name = N'wl_pb_appts')
drop table PTL.wl_pb_appts
select 
	WL_PB.*
	,Appt.START_DTTM
	,Appt.SPECT_REFNO
	,Appt.VISIT_REFNO
	,Appt.SPONT_REFNO
	,Appt.PRITY_REFNO
into PTL.wl_PB_appts
from PTL.WL_PB  WL_PB
left outer join Lorenzo.dbo.ScheduleEvent Appt 
on WL_PB.wlrefno = Appt.wlist_refno 
and Appt.archv_flag = 'N'
where Appt.cancr_dttm is null 
and Appt.attnd_refno <> '358' 

Truncate Table PTL.WaitingListPartialBooking

INSERT INTO PTL.WaitingListPartialBooking
          (SourcePatientID
		  ,SourceReferralID
		  ,SourceWaitingListID
		  ,SpecialtyCode
	      ,Specialty
	      ,Directorate
          ,Division
		  ,WLSpecialty
		  ,Consultant
		  ,PatientID
		  ,PatientSurname
		  ,ReferralDate
		  ,DateOnList
		  ,ReferralStatus
		  ,WaitingStartDate
		  ,DaysWait
		  ,WeeksWait
		  ,InviteDate
		  ,ProjectedApptDate
		  ,AppointmentDate
		  ,LeadTimeWeeks
		  ,CreatedByUser
		  ,CreateDate
		  ,ModifiedByUser
		  ,ModifiedDate
		  ,VisitType
		  ,ApptClinicCode
		  ,ApptSpecialty
		  ,WLClinicCode
		  ,WLName
		  ,WLGeneralComment
		  ,NumberOfFUPatientCancellations
		  ,NumberOfFUHospitalCancellations
		  ,NumberOfNewPatientCancellations
		  ,NumberOfNewHospitalCancellations
		  ,NumberOfDNAs
		  ,HSC
		  ,Stage
	  )
           
select 
	  SourcePatientID
	  ,SourceReferralID
	  ,SourceWaitingListID
	  ,SpecialtyCode
	  ,Specialty
	  ,Directorate
      ,Division
	  ,WLSpecialty
      ,Consultant
      ,PatientID = 
		case 
			when SourceReferralID=153169902 then '4088737814'
			when SourceReferralID=153539331 then '4800798116'
			when SourceReferralID=153372509 then '4501589957'
			else isnull(DistrictNo, '')  + ' - ' + isnull(NHSNumber, '')
		end
      ,PatientSurname
      ,ReferralDate
      ,DateOnList
      ,ReferralStatus
      ,WaitingStartDate
      ,DaysWait
      ,WeeksWait
      ,InviteDate
	  ,ProjectedApptDate
      ,AppointmentDate
      ,LeadTimeWeeks = DATEDIFF(Week, DateonList, ProjectedApptDate)--CM amended 27/01/2014 from being calculated as the time between WaitingStartDate( - this is the reset start date) and ProjectedAppointmentDate ( as above) to using the DateonList (the date patient originally added to WList) and ProjectedApptDate - as requested by CBH.
      ,CreatedByUser
      ,CreateDate
	  ,ModifiedByUser
	  ,ModifiedDate
      ,WLVisitType
      ,ApptClinicCode
      ,ApptSpecialty
      ,WLClinicCode
      ,WLName
      ,WLGeneralComment
      ,NumberOfFUPatientCancellations
	  ,NumberOfFUHospitalCancellations
	  ,NumberOfNewPatientCancellations
	  ,NumberOfNewHospitalCancellations
	  ,NumberOfDNAs
	  ,HSC
	  ,Stage
from
(select 	distinct
	SourcePatientID = WLPB.pat_ref,
	SourceReferralID = WLPB.Refrl_ref,
	SourceWaitingListID = WLPB.WLRefNo,
	SpecialtyCode = SpecialtyDivision.[SpecialtyCode],
	Specialty = SpecialtyDivision.Specialty,
	Directorate = SpecialtyDivision.Direcorate,
    Division = SpecialtyDivision.Division,
	WLSpecialty = Specialty.[DESCRIPTION],
	Consultant = WLPB.surname,
	DistrictNo = DistrictNo.IDENTIFIER,
	NHSNumber = Patient.patnt_refno_nhs_identifier, 
	PatientSurname = Patient.surname,
	ReferralDate = cast (Referral.recvd_dttm as datetime),
	DateOnList = cast (WLPB.Date_on_list as datetime),
	ReferralStatus = RefStatus.[Description],
	WaitingStartDate = 
		case 	
			when Resets.LastResetDate is null then cast(WLPB.date_on_list as datetime)
			else Resets.LastResetDate 
		end,
	DaysWait = 
		case	
			when Resets.LastResetDate is null then datediff(day, WLPB.date_on_list, getdate())
			else datediff (day, Resets.LastResetDate, getdate()) 
		end,
/* 29/01/2014 CM: Weeks Wait Calculation amended to number of weeks between Date On List and getdate rather than reset date as per CBH email 28/01/2014.*/
	--WeeksWait = 
	--	case	
	--		when Resets.reset_date is null then datediff(week, WLPB.date_on_list, getdate())
	--		else datediff (week, Resets.reset_date, getdate()) 
	--	end,
	WeeksWait = datediff(week, WLPB.date_on_list, getdate()),
	WLPB.InviteDate,
	ProjectedApptDate =  DateAdd(week,6,WLPB.InviteDate),
	AppointmentDate = DATEADD(dd, 0, DATEDIFF(dd, 0, Appts.start_dttm)),
	CreatedByUser = 
		case 
			when Users.[user_name] is null then WLPB.Creator
			Else Users.[User_name]
		end,
	CreateDate = WLPB.create_date,
	
	ModifiedByUser = 
		case 
			when ModUsers.[user_name] is null then WLPB.Modifier
			Else ModUsers.[User_name]
		end,
	ModifiedDate = WLPB.modified_date,
	WLVisitType = WLPB.VisitType,
	ApptClinicCode = Location.CODE,
	ApptSpecialty = ApptSpecialty.[DESCRIPTION],
	WLClinicCode = WLPB.WL_Clinic_Code,
	WLName = WLPB.WL_NAME,
	WLGeneralComment = Notes.NOTES_REFNO_NOTE,
    NumberOfFUPatientCancellations = PatCancellationsFU.CancellationCount,
	NumberOfFUHospitalCancellations = HospCancellationsFU.CancellationCount,
	NumberOfNewPatientCancellations = PatCancellationsNew.CancellationCount,
	NumberOfNewHospitalCancellations = HospCancellationsNew.CancellationCount,
	NumberOfDNAs = ApptDNACount.DNACount,
	HSC = case when Referral.PRITY_REFNO = 5478 then 'Yes' else 'No' end,
	Stage = WLPB.Stage
from PTL.wl_PB WLPB 
left join PTL.wl_PB_appts Appts on WLPB.wlrefno = Appts.wlrefno
left join PTL.OPWaitingListResets Resets on WLPB.wlrefno = Resets.wlist_refno
left join Lorenzo.dbo.Referral Referral on WLPB.refrl_ref = Referral.refrl_refno
left join Lorenzo.dbo.Patient Patient on WLPB.pat_ref = Patient.patnt_refno
left join Lorenzo.dbo.Specialty Specialty ON WLPB.Spec_ref = Specialty.spect_refno AND Specialty.ARCHV_FLAG = 'N'
left join Lorenzo.dbo.Specialty ApptSpecialty ON Appts.SPECT_REFNO = ApptSpecialty.spect_refno AND ApptSpecialty.ARCHV_FLAG = 'N'
left join Lorenzo.dbo.[User] Users on WLPB.creator = Users.Code and Users.archv_flag = 'N'
left join Lorenzo.dbo.[User] ModUsers on WLPB.modifier = ModUsers.Code and ModUsers.archv_flag = 'N'
left join Lorenzo.dbo.ReferenceValue RefStatus on Referral.rstat_refno = RefStatus.rfval_refno
left join Lorenzo.dbo.NoteRole Notes ON WLPB.WLREFNO = Notes.SORCE_REFNO AND Notes.SORCE_CODE = 'WLCMT' and Notes.archv_flag = 'N'
left join Lorenzo.dbo.ServicePoint Location on Appts.spont_refno = Location.spont_refno
--JB added link to WHReporting Specialty Lookup for Outpatient Qlikview Dashboard
left join [WHREPORTING].[LK].[SpecialtyDivision] SpecialtyDivision
	on Specialty.spect_refno = SpecialtyDivision.SpecialtyRefno
left join PTL.vwCancelledApptCount PatCancellationsFU
	on Referral.refrl_refno = PatCancellationsFU.ReferralSourceUniqueID
	and PatCancellationsFU.CancelledBy = 'P'
	and PatCancellationsFU.FirstAttendanceFlag in (8911, 2003436) --follow up
left join PTL.vwCancelledApptCount HospCancellationsFU
	on Referral.refrl_refno = HospCancellationsFU.ReferralSourceUniqueID
	and HospCancellationsFU.CancelledBy = 'H'
	and HospCancellationsFU.FirstAttendanceFlag in (8911, 2003436) --follow up
left join PTL.vwCancelledApptCount PatCancellationsNew
	on Referral.refrl_refno = PatCancellationsNew.ReferralSourceUniqueID
	and PatCancellationsNew.CancelledBy = 'P'
	and PatCancellationsNew.FirstAttendanceFlag = 9268 --new
left join PTL.vwCancelledApptCount HospCancellationsNew
	on Referral.refrl_refno = HospCancellationsNew.ReferralSourceUniqueID
	and HospCancellationsNew.CancelledBy = 'H'
	and HospCancellationsNew.FirstAttendanceFlag = 9268 --new
left join
	(select 
		ReferralSourceUniqueID
		,COUNT(ReferralSourceUniqueID) as DNACount
	from (
		select 
			Appt.ReferralSourceUniqueID
		from OP.Encounter Appt
		where Appt.AppointmentDate <= GETDATE()
		and Appt.FirstAttendanceFlag in (8911, 2003436) --follow up
		and Appt.DNACode = '3' --DNA
		)ApptDNA
	group by ReferralSourceUniqueID
	)ApptDNACount
	on Referral.refrl_refno = ApptDNACount.ReferralSourceUniqueID
left join Lorenzo.dbo.PatientIdentifier DistrictNo
	on	DistrictNo.PATNT_REFNO = Patient.PATNT_REFNO
	and	DistrictNo.PITYP_REFNO = 2001232 --facility
	and	DistrictNo.IDENTIFIER like 'RM2%'
	and	DistrictNo.ARCHV_FLAG = 'N'
	and	not exists
		(
		select
			1
		from
			Lorenzo.dbo.PatientIdentifier Previous
		where
			Previous.PATNT_REFNO = DistrictNo.PATNT_REFNO
		and	Previous.PITYP_REFNO = DistrictNo.PITYP_REFNO
		and	Previous.IDENTIFIER like 'RM2%'
		and	Previous.ARCHV_FLAG = 'N'
		and	(
				Previous.START_DTTM > DistrictNo.START_DTTM
			or
				(
					Previous.START_DTTM = DistrictNo.START_DTTM
				and	Previous.PATID_REFNO > DistrictNo.PATID_REFNO
				)
			)
		)
where
--remove test patients
	not exists
	(
	select
			1
	from
		Lorenzo.dbo.ExcludedPatient
	where
		ExcludedPatient.SourcePatientNo = WLPB.pat_ref
	)
) wlpb


select @RowsInserted = @@Rowcount

--Update missing patients
update PTL.WaitingListPartialBooking 
set 
	PatientSurname = lpi.PatientSurname
	,PatientID = isnull(lpi.LocalPatientID, '')  + ' - ' + isnull(lpi.NHSNumber, '')
from PTL.WaitingListPartialBooking wlpb
left join PAS.MissingPatient lpi
	on LPI.SourcePatientNo = wlpb.SourcePatientID
	and LPI.Archived = 'N'
where len(wlpb.PatientID) < 4

--Update cancer diagnosis field - patients who have ever had a cancer diagnosis
Update [PTL].[WaitingListPartialBooking]
SET [CancerDiagnosis] = 'Yes'
 FROM [PTL].[WaitingListPartialBooking] P
 LEFT JOIN SCR.Demographic d
 ON left(P.PatientID, CHARINDEX('-', P.PatientID, 0)-2) = d.HospitalNumber
 LEFT JOIN [SCR].[Referral] ref 
 ON ref.DemographicUniqueRecordId = d.UniqueRecordId
 where CHARINDEX('-', PatientID, 0) > 0
 and DiagnosisDate is not null
 
 --Update cancer site
  select distinct ref.UniqueRecordId ,ref.DemographicUniqueRecordId, 
       d.HospitalNumber, ref.DiagnosisDate, ref.CancerSite
       INTO #CancerPatients
        FROM [SCR].[Referral] ref
              LEFT JOIN SCR.Demographic d
              ON ref.DemographicUniqueRecordId = d.UniqueRecordId
              where ref.DiagnosisDate is not null
              order by HospitalNumber

   SELECT final.demographicuniquerecordid,
        LEFT(final.Res, Len(final.Res) - 1) AS CancerSite,
        final.hospitalnumber
        Into #CancerFinal
FROM   (SELECT DISTINCT ST2.demographicuniquerecordid,
                         ST2.[hospitalnumber],
                         (SELECT ST1.cancersite + ',' AS [text()]
                          FROM   #CancerPatients ST1
                          WHERE  ST1.demographicuniquerecordid = ST2.demographicuniquerecordid
                                 --AND demographicuniquerecordid = '56882'
                          ORDER  BY ST1.demographicuniquerecordid
                          FOR XML PATH ('')) Res
         FROM  #CancerPatients ST2
        --WHERE demographicuniquerecordid = '56882'
        )final
 
Update [PTL].[WaitingListPartialBooking]
SET [CancerSite] = d.cancersite
FROM [PTL].[WaitingListPartialBooking] P
LEFT JOIN #CancerFinal d
ON left(P.PatientID, CHARINDEX('-', P.PatientID, 0)-2) = d.HospitalNumber
where CHARINDEX('-', PatientID, 0) > 0

 
--Clean up
drop table PTL.wl_PB_appts
drop table PTL.WL_PB

select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WH BuildOPWaitingListPartialBooking', @Stats, @StartTime
