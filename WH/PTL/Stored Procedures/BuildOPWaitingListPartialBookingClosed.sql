﻿

CREATE     procedure [PTL].[BuildOPWaitingListPartialBookingClosed] as

/* 
27/03/2014
KO based on [BuildOPWaitingListPartialBooking] 
to look at closed waiting lists and details of associated attended appointments


*/
declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

--If exists (Select * from Sysobjects Where name = N'WL_RESCHED')
--DROP TABLE PTL.WL_RESCHED

--SELECT
--WLIST_REFNO,
--MAX(CAST(old_start_dttm AS DATETIME)) AS RESET_DATE
--INTO PTL.WL_RESCHED
--FROM Lorenzo.dbo.ScheduleHistory
--WHERE 

--MOVRN_REFNO IN (
--	'383',
--	'3248',
--	'4520',
--	'2003539',
--	'2003540',
--	'2003541',
--	'2003544',
--	'2004148')

----MOVRN_REFNO_MAIN_CODE = 'PT'
--AND ARCHV_FLAG = 'N' AND (NEW_START_DTTM > OLD_START_DTTM) AND NOT WLIST_REFNO IS NULL
--AND CAST(OLD_START_DTTM AS DATETIME) < GETDATE()
--GROUP BY WLIST_REFNO 


--If exists (Select * from Sysobjects Where name = N'TMP_WL_RESETS')
--DROP TABLE PTL.TMP_WL_RESETS

--select wlist_refno, max(CAST(start_dttm AS DATETIME)) as 'Reset_date'
--into PTL.TMP_wl_resets
--from Lorenzo.dbo.ScheduleEvent
--where attnd_refno 
--in ('2868', '358', '2870', '2004301', '2000724', '358', '2003494', '2003495') 
--and archv_flag = 'n' 
--AND NOT WLIST_REFNO IS NULL
--AND CAST(START_DTTM AS DATETIME) <GETDATE()
--group by wlist_refno

--INSERT INTO PTL.TMP_WL_RESETS
--SELECT *
--FROM PTL.WL_RESCHED

--If exists (Select * from Sysobjects Where name = N'WL_RESETS')
--DROP TABLE PTL.WL_RESETS
--SELECT
--WLIST_REFNO,
--MAX(RESET_DATE) AS RESET_DATE
--INTO PTL.WL_RESETS
--FROM PTL.TMP_WL_RESETS
--GROUP BY WLIST_REFNO


--Identify PB waiting lists
--If exists (Select * from Sysobjects Where name = N'WL_PB')
--drop table PTL.WL_PB
--select 	
--	a.wlist_refno as 'WLRefNo', 
--	a.refrl_refno as 'Refrl_ref', 
--	a.patnt_refno as 'Pat_ref',
--	a.spect_refno as Spec_ref,
--	--c.proca_refno_main_ident as 'Cons_code',
--	c.surname,
--	a.wlist_dttm as 'Date_on_list',
--	a.waiting_start_dttm as 'Waiting_start_date',	
--	--datediff (day, waiting_start_dttm, getdate()) as 'Days_wait',
--	a.user_create as 'creator',
--	cast(a.create_dttm as datetime) as 'Create_Date',
--	a.USER_MODIF as 'modifier',
--	cast(a.MODIF_DTTM as datetime) as 'Modified_Date',
--	M.SPONT_REFNO_CODE AS WL_Clinic_Code,
--	wlr.NAME AS WL_NAME,
--	a.visit_refno AS 'WL_visit_type',
--	InviteDate = CAST(a.INVIT_DTTM AS DATETIME),
--	VisitType = Visit.[DESCRIPTION]
--into PTL.WL_PB
--from Lorenzo.dbo.WaitingList a 
--left join Lorenzo.dbo.ProfessionalCarer c on a.proca_refno = c.proca_refno
--LEFT JOIN Lorenzo.dbo.ServicePoint M ON A.SPONT_REFNO = M.SPONT_REFNO
--left join Lorenzo.dbo.WaitingListRule wlr on A.WLRUL_REFNO = wlr.WLRUL_REFNO
--LEFT JOIN Lorenzo.dbo.ReferenceValue Visit ON a.VISIT_REFNO = Visit.RFVAL_REFNO
--where 
--remvl_dttm is null 
--and a.archv_flag = 'N' 
--and A.svtyp_refno = '4223'
--AND a.WLRUL_REFNO = 150000536 -- this is the old method of identifying Patial Booking waiting list - essentially all patient added to this waiting list is for partial booking
--and cast(a.create_dttm as datetime) < '2013-02-20 00:00:00.000'

If exists (Select * from Sysobjects Where name = N'WL_PB_Closed')
drop table PTL.WL_PB_Closed
select 	
	a.wlist_refno as 'WLRefNo', 
	a.refrl_refno as 'Refrl_ref', 
	a.patnt_refno as 'Pat_ref',
	a.spect_refno as Spec_ref,
	--c.proca_refno_main_ident as 'Cons_code',
	c.surname,
	a.wlist_dttm as 'Date_on_list',
	a.waiting_start_dttm as 'Waiting_start_date',	
	a.remvl_dttm as Removal_Date,
	--datediff (day, waiting_start_dttm, getdate()) as 'Days_wait',
	a.user_create as 'creator',
	cast(a.create_dttm as datetime) as 'Create_Date',
	a.USER_MODIF as 'modifier',
	cast(a.MODIF_DTTM as datetime) as 'Modified_Date',
	M.SPONT_REFNO_CODE AS WL_Clinic_Code,
	wlr.NAME AS WL_NAME,
	a.visit_refno AS 'WL_visit_type',
	InviteDate = CAST(a.INVIT_DTTM AS DATETIME),
	VisitType = Visit.[DESCRIPTION]
into PTL.WL_PB_Closed
from Lorenzo.dbo.WaitingList a 
left join Lorenzo.dbo.ProfessionalCarer c on a.proca_refno = c.proca_refno
LEFT JOIN Lorenzo.dbo.ServicePoint M ON A.SPONT_REFNO = M.SPONT_REFNO
left join Lorenzo.dbo.WaitingListRule wlr on A.WLRUL_REFNO = wlr.WLRUL_REFNO
LEFT JOIN Lorenzo.dbo.ReferenceValue Visit ON a.VISIT_REFNO = Visit.RFVAL_REFNO
where 
remvl_dttm is not null 
and a.archv_flag = 'N' 
and A.svtyp_refno = '4223'
and a.visit_refno in ('8911', '2003436') --follow up.  This is the new way of identifying partial booking - all WL entries which have been added with the appointment type of follow up.

create nonclustered index [idx_WL_PB_Closed_WLRefNo] ON [PTL].[WL_PB_Closed] ([WLRefNo] ASC)


--If Exists (Select * from Sysobjects Where name = N'wl_pb_att_appts')
--drop table PTL.wl_pb_att_appts
--select 
--	WL_PB.*
--	,Appt.START_DTTM
--	,Appt.SPECT_REFNO
--	,Appt.VISIT_REFNO
--	,Appt.SPONT_REFNO
--	,Appt.PRITY_REFNO
--into PTL.wl_pb_att_appts
--from PTL.WL_PB_Closed  WL_PB
--left outer join Lorenzo.dbo.ScheduleEvent Appt 
--on WL_PB.wlrefno = Appt.wlist_refno 
--and Appt.archv_flag = 'N'
--where Appt.cancr_dttm is null 
--and Appt.attnd_refno <> '358' 

If Exists (Select * from Sysobjects Where name = N'WLPB_AttendedAppointment')
drop table PTL.WLPB_AttendedAppointment
select 
	WL_PB.wlrefno
	,Appt.AppointmentDate
	,Appt.SpecialtyCode
	,Appt.ClinicCode
	,appt.DNACode
	,appt.AppointmentStatusCode
into PTL.WLPB_AttendedAppointment
from PTL.WL_PB_Closed  WL_PB
left join OP.Encounter Appt 
on WL_PB.wlrefno = Appt.WaitingListSourceUniqueID 
--and Appt.ar = 'N'
where Appt.AppointmentCancelDate is null 
--and Appt.DNACode <> '358

select top 100 * from OP.Encounter

--If exists (Select * from Sysobjects Where Name = N'WaitingListPartialBooking')
--drop table PTL.WaitingListPartialBookingClosed

truncate table PTL.WaitingListPartialBookingClosed

INSERT INTO PTL.WaitingListPartialBookingClosed
          (SourcePatientID
		  ,SourceReferralID
		  ,SourceWaitingListID
		  ,WLSpecialty
		  ,Consultant
		  ,PatientID
		  ,PatientSurname
		  ,ReferralDate
		  ,DateOnList
		  ,ReferralStatus
		  ,WaitingStartDate
		  ,RemovalDate
		  ,DaysWait
		  ,WeeksWait
		  ,InviteDate
		  ,ProjectedApptDate
		  ,AppointmentDate
		  ,LeadTimeWeeks
		  ,CreatedByUser
		  ,CreateDate
		  ,ModifiedByUser
		  ,ModifiedDate
		  ,VisitType
		  ,ApptClinicCode
		  ,ApptSpecialty
		  ,ApptAttended
		  ,ApptStatus
		  ,WLClinicCode
		  ,WLName
		  ,WLGeneralComment
		  ,NumberOfFUPatientCancellations
		  ,NumberOfFUHospitalCancellations
		  ,NumberOfNewPatientCancellations
		  ,NumberOfNewHospitalCancellations
		  ,NumberOfDNAs
		  ,HSC
	  )
           
select 
	  SourcePatientID
	  ,SourceReferralID
	  ,SourceWaitingListID
	  ,WLSpecialty
      ,Consultant
      ,PatientID = 
		case 
			when SourceReferralID=153169902 then '4088737814'
			when SourceReferralID=153539331 then '4800798116'
			when SourceReferralID=153372509 then '4501589957'
			else isnull(DistrictNo, '')  + ' - ' + isnull(NHSNumber, '')
		end
      ,PatientSurname
      ,ReferralDate
      ,DateOnList
      ,ReferralStatus
      ,WaitingStartDate
      ,RemovalDate
      ,DaysWait
      ,WeeksWait
      ,InviteDate
	  ,ProjectedApptDate
      ,AppointmentDate
      --,LeadTimeWeeks = DATEDIFF(Week, WaitingStartDate, ProjectedApptDate)
      ,LeadTimeWeeks = DATEDIFF(Week, DateonList, ProjectedApptDate)--CM amended 27/01/2014 from being calculated as the time between WaitingStartDate( - this is the reset start date) and ProjectedAppointmentDate ( as above) to using the DateonList (the date patient originally added to WList) and ProjectedApptDate - as requested by CBH.
      ,CreatedByUser
      ,CreateDate
	  ,ModifiedByUser
	  ,ModifiedDate
      ,VisitType = WLVisitType
      ,ApptClinicCode
      ,ApptSpecialty
      ,ApptAttended
      ,ApptStatus
      ,WLClinicCode
      ,WLName
      ,WLGeneralComment
      ,NumberOfFUPatientCancellations
	  ,NumberOfFUHospitalCancellations
	  ,NumberOfNewPatientCancellations
	  ,NumberOfNewHospitalCancellations
	  ,NumberOfDNAs
	  ,HSC
--into PTL.WaitingListPartialBookingClosed
from
(select 	distinct
	SourcePatientID = WLPB.pat_ref,
	SourceReferralID = WLPB.Refrl_ref,
	SourceWaitingListID = WLPB.WLRefNo,
	WLSpecialty = Specialty.[DESCRIPTION],
	Consultant = WLPB.surname,
	DistrictNo = DistrictNo.IDENTIFIER,
	NHSNumber = Patient.patnt_refno_nhs_identifier, 
	PatientSurname = Patient.surname,
	ReferralDate = cast (Referral.recvd_dttm as datetime),
	DateOnList = cast (WLPB.Date_on_list as datetime),
	ReferralStatus = RefStatus.[Description],
	WaitingStartDate = cast(WLPB.date_on_list as datetime),
	RemovalDate = cast(WLPB.Removal_Date as datetime),
		--case 	
		--	when Resets.reset_date is null then cast(WLPB.date_on_list as datetime)
		--	else Resets.reset_date 
		--end,
	DaysWait = datediff(day, WLPB.date_on_list, getdate()),
		--case	
		--	when Resets.reset_date is null then datediff(day, WLPB.date_on_list, getdate())
		--	else datediff (day, Resets.reset_date, getdate()) 
		--end,
/* 29/01/2014 CM: Weeks Wait Calculation amended to number of weeks between Date On List and getdate rather than reset date as per CBH email 28/01/2014.*/
	--WeeksWait = 
	--	case	
	--		when Resets.reset_date is null then datediff(week, WLPB.date_on_list, getdate())
	--		else datediff (week, Resets.reset_date, getdate()) 
	--	end,
	WeeksWait = datediff(week, WLPB.date_on_list, getdate()),
	WLPB.InviteDate,
	ProjectedApptDate =  DateAdd(week,6,WLPB.InviteDate),
	AppointmentDate = DATEADD(dd, 0, DATEDIFF(dd, 0, Appts.AppointmentDate)),
	CreatedByUser = 
		case 
			when Users.[user_name] is null then WLPB.Creator
			Else Users.[User_name]
		end,
	CreateDate = WLPB.create_date,
	
	ModifiedByUser = 
		case 
			when ModUsers.[user_name] is null then WLPB.Modifier
			Else ModUsers.[User_name]
		end,
	ModifiedDate = WLPB.modified_date,
	WLVisitType = WLPB.VisitType,
	ApptClinicCode = Appts.ClinicCode,
	ApptSpecialty = ApptSpecialty.[DESCRIPTION],
	ApptAttended = Appts.DNACode,
	ApptStatus = ApptStatus.[Description],
	WLClinicCode = WLPB.WL_Clinic_Code,
	WLName = WLPB.WL_NAME,
	WLGeneralComment = Notes.NOTES_REFNO_NOTE,
    NumberOfFUPatientCancellations = PatCancellationsFU.CancellationCount,
	NumberOfFUHospitalCancellations = HospCancellationsFU.CancellationCount,
	NumberOfNewPatientCancellations = PatCancellationsNew.CancellationCount,
	NumberOfNewHospitalCancellations = HospCancellationsNew.CancellationCount,
	NumberOfDNAs = ApptDNACount.DNACount,
	HSC = case when Referral.PRITY_REFNO = 5478 then 'Yes' else 'No' end
from PTL.WL_PB_Closed WLPB 
left join PTL.WLPB_AttendedAppointment Appts on WLPB.wlrefno = Appts.wlrefno
--left join PTL.wl_resets Resets on WLPB.wlrefno = Resets.wlist_refno
left join Lorenzo.dbo.Referral Referral on WLPB.refrl_ref = Referral.refrl_refno
left join Lorenzo.dbo.Patient Patient on WLPB.pat_ref = Patient.patnt_refno
left join Lorenzo.dbo.Specialty Specialty ON WLPB.Spec_ref = Specialty.spect_refno AND Specialty.ARCHV_FLAG = 'N'
left join Lorenzo.dbo.Specialty ApptSpecialty ON Appts.SpecialtyCode = ApptSpecialty.spect_refno AND ApptSpecialty.ARCHV_FLAG = 'N'
left join Lorenzo.dbo.[User] Users on WLPB.creator = Users.Code and Users.archv_flag = 'N'
left join Lorenzo.dbo.[User] ModUsers on WLPB.modifier = ModUsers.Code and ModUsers.archv_flag = 'N'
left join Lorenzo.dbo.ReferenceValue RefStatus on Referral.rstat_refno = RefStatus.rfval_refno
left join Lorenzo.dbo.ReferenceValue ApptStatus on ApptStatus.rfval_refno = Appts.AppointmentStatusCode
left join Lorenzo.dbo.NoteRole Notes ON WLPB.WLREFNO = Notes.SORCE_REFNO AND Notes.SORCE_CODE = 'WLCMT' and Notes.archv_flag = 'N'
--left join Lorenzo.dbo.ServicePoint Location on Appts.spont_refno = Location.spont_refno
left join PTL.vwCancelledApptCount PatCancellationsFU
	on Referral.refrl_refno = PatCancellationsFU.ReferralSourceUniqueID
	and PatCancellationsFU.CancelledBy = 'P'
	and PatCancellationsFU.FirstAttendanceFlag in (8911, 2003436) --follow up
left join PTL.vwCancelledApptCount HospCancellationsFU
	on Referral.refrl_refno = HospCancellationsFU.ReferralSourceUniqueID
	and HospCancellationsFU.CancelledBy = 'H'
	and HospCancellationsFU.FirstAttendanceFlag in (8911, 2003436) --follow up
left join PTL.vwCancelledApptCount PatCancellationsNew
	on Referral.refrl_refno = PatCancellationsNew.ReferralSourceUniqueID
	and PatCancellationsNew.CancelledBy = 'P'
	and PatCancellationsNew.FirstAttendanceFlag = 9268 --new
left join PTL.vwCancelledApptCount HospCancellationsNew
	on Referral.refrl_refno = HospCancellationsNew.ReferralSourceUniqueID
	and HospCancellationsNew.CancelledBy = 'H'
	and HospCancellationsNew.FirstAttendanceFlag = 9268 --new
left join
	(select 
		ReferralSourceUniqueID
		,COUNT(ReferralSourceUniqueID) as DNACount
	from (
		select 
			Appt.ReferralSourceUniqueID
		from OP.Encounter Appt
		where Appt.AppointmentDate <= GETDATE()
		and Appt.FirstAttendanceFlag in (8911, 2003436) --follow up
		and Appt.DNACode = '3' --DNA
		)ApptDNA
	group by ReferralSourceUniqueID
	)ApptDNACount
	on Referral.refrl_refno = ApptDNACount.ReferralSourceUniqueID
left join Lorenzo.dbo.PatientIdentifier DistrictNo
	on	DistrictNo.PATNT_REFNO = Patient.PATNT_REFNO
	and	DistrictNo.PITYP_REFNO = 2001232 --facility
	and	DistrictNo.IDENTIFIER like 'RM2%'
	and	DistrictNo.ARCHV_FLAG = 'N'
	and	not exists
		(
		select
			1
		from
			Lorenzo.dbo.PatientIdentifier Previous
		where
			Previous.PATNT_REFNO = DistrictNo.PATNT_REFNO
		and	Previous.PITYP_REFNO = DistrictNo.PITYP_REFNO
		and	Previous.IDENTIFIER like 'RM2%'
		and	Previous.ARCHV_FLAG = 'N'
		and	(
				Previous.START_DTTM > DistrictNo.START_DTTM
			or
				(
					Previous.START_DTTM = DistrictNo.START_DTTM
				and	Previous.PATID_REFNO > DistrictNo.PATID_REFNO
				)
			)
		)
where
--remove test patients
	not exists
	(
	select
			1
	from
		Lorenzo.dbo.ExcludedPatient
	where
		ExcludedPatient.SourcePatientNo = WLPB.pat_ref
	)
) wlpb


select @RowsInserted = @@Rowcount

--Update missing patients
update PTL.WaitingListPartialBookingClosed
set 
	PatientSurname = lpi.PatientSurname
	,PatientID = isnull(lpi.LocalPatientID, '')  + ' - ' + isnull(lpi.NHSNumber, '')
from PTL.WaitingListPartialBookingClosed wlpb
left join PAS.MissingPatient lpi
	on LPI.SourcePatientNo = wlpb.SourcePatientID
	and LPI.Archived = 'N'
where len(wlpb.PatientID) < 4

--clean up
drop table PTL.WLPB_AttendedAppointment
drop table PTL.WL_PB_Closed



select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

--exec WriteAuditLogEvent 'PAS - WH BuildOPWaitingListPartialBookingClosed', @Stats, @StartTime
--print @Stats






