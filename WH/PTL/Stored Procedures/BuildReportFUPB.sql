﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Build PTL.ReportFUPB

Notes:			Stored in PTL

Versions:
				1.0.0.1 - 17/12/2015 - MT - Job 228
					Create a monthly snapshot of ReportFUPB
					on the 1st of every month -> ReportFUPBArchive.

				1.0.0.0 - 07/12/2015 - MT
					Created sproc - previously a view but SSRS reports
					based on the view were running very slowly.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure PTL.BuildReportFUPB
As

Truncate Table PTL.ReportFUPB

;With MyPatientCancs As (
	Select	src.SourceReferralID,
			COUNT(*) As Appts
	From	PTL.WaitingListPartialBooking src With (NoLock)
	
			Inner Join OP.Encounter op With (NoLock)
				On src.SourceReferralID = op.ReferralSourceUniqueID
				And op.AppointmentTime >= src.DateOnList
	
			Left Join PTL.OPLookups lu With (NoLock)
				On op.ScheduledCancelReasonCode = lu.ReferenceValueCode

	Where	op.AppointmentCancelDate Is Not Null
			And (
			(op.ScheduledCancelReasonCode In (61,1210,2001651,2004150,2006150)
			And op.CancelledByCode In (5477,3107255))
			Or lu.MappedCode = 'P'
			)
	Group By src.SourceReferralID
	),

	MyDNAs As (	
	Select	src.SourceReferralID,
			Count(*) As Appts
	From	PTL.WaitingListPartialBooking src With (NoLock)
	
			Inner Join OP.Encounter op With (NoLock)
				On src.SourceReferralID = op.ReferralSourceUniqueID
				And op.AppointmentTime >= src.DateOnList
				And op.DNACode = '3'
				And op.AppointmentDate <= GETDATE()
	
	Group By src.SourceReferralID
	),
	
	MyPatientRescheduled As (
	Select	src.SourceReferralID,
			COUNT(*) As Appts
	From	PTL.WaitingListPartialBooking src With (NoLock)

			Inner Join OP.Encounter op With (NoLock)
				On src.SourceReferralID = op.ReferralSourceUniqueID
	
			Inner Join Lorenzo.dbo.ScheduleHistory his With (NoLock)
				On op.SourceUniqueID = his.SCHDL_REFNO
				And his.MOVRN_REFNO = 4520 -- Patient cancelled move reason
				And his.OLD_START_DTTM >= src.DateOnList
				And his.ARCHV_FLAG = 'N'

	Group By src.SourceReferralID
	),
	
	MyHospitalCancs As (
	Select	src.SourceReferralID,
			COUNT(*) As Appts
	From	PTL.WaitingListPartialBooking src With (NoLock)
	
			Inner Join OP.Encounter op With (NoLock)
				On src.SourceReferralID = op.ReferralSourceUniqueID
				And op.AppointmentTime >= src.DateOnList
	
			Left Join PTL.OPLookups lu With (NoLock)
				On op.ScheduledCancelReasonCode = lu.ReferenceValueCode

	Where	op.AppointmentCancelDate Is Not Null
			And (
			(op.ScheduledCancelReasonCode In (61,1210,2001651,2004150,2006150)
			And op.CancelledByCode Not In (5477,3107255))
			Or lu.MappedCode = 'H'
			)
	Group By src.SourceReferralID
	),
	
	MyHospitalRescheduled As (
	Select	src.SourceReferralID,
			COUNT(*) As Appts
	From	PTL.WaitingListPartialBooking src With (NoLock)

			Inner Join OP.Encounter op With (NoLock)
				On src.SourceReferralID = op.ReferralSourceUniqueID
	
			Inner Join Lorenzo.dbo.ScheduleHistory his With (NoLock)
				On op.SourceUniqueID = his.SCHDL_REFNO
				And his.MOVRN_REFNO In (741,2000639,2001132,2001133) -- Hospital move reasons
				And his.OLD_START_DTTM >= src.DateOnList
				And his.ARCHV_FLAG = 'N'

	Group By src.SourceReferralID
	)

Insert Into PTL.ReportFUPB(
	[WLSpecialty]
	,[Consultant]
	,[PatientID]
	,[PatientSurname]
	,[ReferralSourceUniqueID]
	,[ReferralDate]
	,[WaitingStartDate]
	,[SourceWaitingListID]
	,[SpecialtyCode]
	,[Specialty]
	,[Directorate]
	,[Division]
	,[WLClinicCode]
	,[ApptClinicCode]
	,[VisitType]
	,[DaysWait]
	,[WeeksWait]
	,[InviteDate]
	,[LeadTimeWeeks]
	,[ProjectedApptDate]
	,[AppointmentDate]
	,[WLGeneralComment]
	,[WLName]
	,[ReferralStatus]
	,[CreateDate]
	,[CreatedByUser]
	,[ModifiedDate]
	,[ModifiedByUser]
	,[BookingStatus]
	,[InviteStatus]
	,[OverdueWks]
	,[OverdueWksAtAppointment]
	,[DateOnList]
	,[NumberOfFUPatientCancellations]
	,[NumberOfFUHospitalCancellations]
	,[NumberOfDNAs]
	,[HSC]
	,[BookByBreachDate]
	,[CancerDiagnosis]
	,[CancerSite]
	,[NumberPatientCancellationsAfterDateOnList]
	,[NumberPatientDNAsAfterDateOnList]
	,[NumberHospitalCancellationsAfterDateOnList]
	,[Stage]
	)
Select
	src.WLSpecialty
	,src.Consultant
	,src.PatientID
	,src.PatientSurname
	,ReferralSourceUniqueID = src.SourceReferralID
	,ReferralDate = convert(varchar(11), src.ReferralDate, 106)
	,WaitingStartDate = convert(varchar(11), src.WaitingStartDate, 106)
	,src.SourceWaitingListID
	,src.[SpecialtyCode]
	,src.[Specialty]
	,src.[Directorate]
	,src.[Division]
	,src.WLClinicCode
	,src.ApptClinicCode
	,src.VisitType
	,src.DaysWait
	,src.WeeksWait
	,src.InviteDate
	,src.LeadTimeWeeks
	,ProjectedApptDate = convert(varchar(11), src.ProjectedApptDate, 106)
	,AppointmentDate = convert(varchar(11), src.AppointmentDate, 106)
	,src.WLGeneralComment
	,src.WLName
	,src.ReferralStatus
	,src.CreateDate
	,src.CreatedByUser
	,src.ModifiedDate
	,src.ModifiedByUser
	,BookingStatus =
	Case 
		When src.appointmentdate <> '1998-04-01' Then 'Booked'
		Else 'UnBooked'
	End
	,InviteStatus = 
	Case 
		When InviteDate < getdate() Then 'Overdue'
		Else 'OK'
	End
	,OverdueWks = 
	Case 
		When (DATEDIFF(WW, [ProjectedApptDate], GETDATE())) < 0 Then 0
		Else (DATEDIFF(WW, [ProjectedApptDate], GETDATE()))
	End
	,OverdueWksAtAppointment = 
	Case 
		When AppointmentDate > [ProjectedApptDate] Then (DATEDIFF(WW, [ProjectedApptDate], AppointmentDate))
		Else 0
	End
	,DateOnList = convert(varchar(11), DateOnList, 106)
	,src.NumberOfFUPatientCancellations
	,src.NumberOfFUHospitalCancellations
	,src.NumberOfDNAs
	,src.HSC
	,BookByBreachDate =
	Case 
		When LeadTimeWeeks >= 52 Then convert(varchar(11), DateAdd(week, 12, ProjectedApptDate), 106)
		When LeadTimeWeeks >= 26 And LeadTimeWeeks < 52
			Then convert(varchar(11), DATEADD(week, 4, ProjectedApptDate), 106)
		When LeadTimeWeeks >= 12 And LeadTimeWeeks < 26
			Then convert(varchar(11), DATEADD(week, 3, ProjectedApptDate), 106)
		Else ProjectedApptDate
	End
	,src.CancerDiagnosis
	,src.CancerSite
	,NumberPatientCancellationsAfterDateOnList = Coalesce(pc.Appts,0) + Coalesce(pr.Appts,0)
	,NumberPatientDNAsAfterDateOnList = Coalesce(dna.Appts,0)
	,NumberHospitalCancellationsAfterDateOnList = Coalesce(hc.Appts,0) + Coalesce(hr.Appts,0)
	,src.Stage

From	PTL.WaitingListPartialBooking src

		Left Join MyPatientCancs pc
			On src.SourceReferralID = pc.SourceReferralID

		Left Join MyDNAs dna
			On src.SourceReferralID = dna.SourceReferralID
			
		Left Join MyHospitalCancs hc
			On src.SourceReferralID = hc.SourceReferralID
			
		Left Join MyPatientRescheduled pr
			On src.SourceReferralID = pr.SourceReferralID
			
		Left Join MyHospitalRescheduled hr
			On src.SourceReferralID = hr.SourceReferralID

-- Create monthly snapshot
If (DATEPART(DAY,GETDATE()) = 1)
Begin
	Insert Into PTL.ReportFUPBArchive
	
	Select	src.*,
			DW_REPORTING.LIB.fn_GetJustDate(GETDATE()) -- ArchiveDate
	
	From	PTL.ReportFUPB src
	
End
