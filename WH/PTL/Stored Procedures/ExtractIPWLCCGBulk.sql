﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		PTL - Extract IPWL CCG Bulk

Notes:			Stored in 

				KO recreate IPWL logic from InfoSQL with added fields to generate ResponsibleCommissioner

				Copy of INFORMATION_VER2.uspUPDATE_IPWL_Dataset
				---------------------------------------------------------------------------------------------------
				Prequisites to the script being run are:-
				(In Order)

				1.PTL.BuildIPWLPreRequisites
					Gets list of all WLREFNOs that need updating in table PTL.IPWLNetChange
					Sets the TCI Dates in table PTL.IPWLTCIDates
					Sets the waiting Start Time in table PTL.IPWLStartDates
					Sets the reschedules in table PTL.IPWLReschedules

				2.PTL.BuildIPWLWaitingStatus
					Sets the Waiting status for the waiting list entry in table IPM_IPWL_WAITING_STATUS
					
				3.PTL.BuildIPWLSuspensions
					Creates the Suspension dataset PTL.IPWLSuspensions

Versions:
				1.0.0.1 - 20/10/2015 - MT
					Use the revised table PatientGPHistory.

				1.0.0.0
					Original sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [PTL].[ExtractIPWLCCGBulk]
As

Declare @RowCount Int
Declare @StartTime datetime

Select @StartTime = getdate()

PRINT 'PTL.ExtractIPWLCCGBulk started at '	+ CONVERT(varchar(17), @StartTime)

truncate table PTL.TImportIPWL

insert into PTL.TImportIPWL
select	distinct
	DataSource = 'IPM'
	,SourceUniqueID = WaitingList.WLIST_REFNO
	,SourcePatientNo = WaitingList.PATNT_REFNO
	,ReferralSourceUniqueID = WaitingList.REFRL_REFNO
	,DistrictNo = null --DistrictNo.IDENTIFIER
	,NHSNumber = Patient.PATNT_REFNO_NHS_IDENTIFIER
	,PatientForename = Patient.FORENAME
	,PatientSurname = Patient.SURNAME
	,DateOfBirth = Patient.DTTM_OF_BIRTH
	,SpecialtyCode = Specialty.SPECT_REFNO_MAIN_IDENT
	,Consultant = Carer.PROCA_REFNO_MAIN_IDENT
	,ListType = WaitingListRule.CODE
	,ElectiveAdmissionMethod = AdmissionMethod.MAIN_CODE
	,AdminCategory = AdminCategory.MAIN_CODE
	,IntendedManagement = IntendedManagement.MAIN_CODE
	,Priority = Priority.MAIN_CODE
	,ReferralDate = Referral.RECVD_DTTM
	,OriginalDateOnList = WaitingList.ORDTA_DTTM
	,DateOnList = WaitingList.WLIST_DTTM
	,StartDates.WaitingStartDate
	,SuspendedStartDate = cast(null as datetime) 
	,SuspendedEndDate = cast(null as datetime)
	,SuspToDate = null
	,SuspensionPeriod = null 
	,SuspPatEndDate = cast(null as datetime) 
	,SuspToDatePtReason = null 
	,SuspensionPeriodPtReason = null 
	,CurrentSuspension = cast(null as varchar(1))
	,CurrentSuspensionType = cast(null as varchar(1))
	,RemovalDate = DATEADD(dd, 0, DATEDIFF(dd, 0,WaitingList.REMVL_DTTM))
	,GuaranteedAdmissionDate = WaitingList.GUADM_DTTM
	,AdmissionDate = DATEADD(dd, 0, DATEDIFF(dd, 0,WaitingList.ADMIT_DTTM))
	,PlannedProcedure = WaitingList.PLANNED_PROC 
	,AnaestheticType = AnaestheticType.[MAIN_CODE]
	,ServiceType = ServiceType.MAIN_CODE
	,ListName = WaitingListRule.Name
	,AdmissionWard = AdmissionWard.CODE
	,EstimatedTheatreTime = WaitingList.THEAT_TIME
	,WhoCanOperate = WHOCO.DESCRIPTION
	,GeneralComment =  null --ltrim(rtrim(replace(replace(NotesGeneral.NOTES_REFNO_NOTE,'[PROCEDURE:',''),']','')))
	,PatientPrepComments = null --NotesPrep.NOTES_REFNO_NOTE
	,RemovalReason = RemovalReason.[DESCRIPTION]
	,WaitingStatus.EntryStatus
	,WaitingStatus.OfferOutcome
	,WaitingStatus.PCEOutcome
	,TCIDates.TCIDate
	,DateTCIOffered = DATEADD(dd, 0, DATEDIFF(dd, 0,TCIDates.ADMOF_DTTM))
	,EffectiveGP = null --GPHistory.GPRefno
	,CancerCode = 
		case LOCAL_WLRUL_REFNO
			when '10001223' then 'CD'
			when '10001222' then 'CS'
			when '10003882' then 'SUB'
			else 'NON'
		end 
	,DateRecordCreated = DATEADD(dd, 0, DATEDIFF(dd, 0,WaitingList.CREATE_DTTM))
	,CreatedByUser = --cast(null as varchar(25))
		case 
			when CreatedUsers.[user_name] is null then WaitingList.USER_CREATE
			Else CreatedUsers.[User_name]
		end
		
	,DateRecordModified = DATEADD(dd, 0, DATEDIFF(dd, 0,WaitingList.MODIF_DTTM))
	,ModifiedByUser = --cast(null as varchar(25))
		case 
			when ModifiedUsers.[user_name] is null then WaitingList.USER_MODIF
			Else ModifiedUsers.[User_name]
		end

	,ShortNoticeFlag = WaitingList.SHORT_NOTICE_FLAG
	,AdmitByDate = DATEADD(dd, 0, DATEDIFF(dd, 0,AdmissionDecision.ADMIT_NOTAF_DTTM))
	,NextResetDate = cast(null as datetime) 
	,PPID = Referral.PATNT_PATHWAY_ID
	,RTTCode = RFV.MAIN_CODE
	,RTTDescription = RFV.[DESCRIPTION]
	,IPWLDiagnosticProcedure = cast(null as varchar(25))
	,Sex = Patient.SEXXX_REFNO_DESCRIPTION
	,Ethnicity = Patient.ETHGR_REFNO_DESCRIPTION
	,PreOpClinic = cast(null as varchar(25))
	,PreOpDate = cast(null as datetime)
	,PreOpClinicDescription = cast(null as varchar(25))
	,SecondaryWard = SecondaryWard.CODE
	,OverseasStatusFlag = null --OverseasVisitorStatus.OVSVS_REFNO
	,EpisodicGpPracticeCode = null --PracticeHistory.PracticeRefno
	,EpisodicGpPracticeRefno = null --PracticeHistory.PracticeRefno
	,PurchaserCode = WaitingList.PURCH_REFNO
	,PatientPostcode = null --PatientAddress.PCODE
--into 	PTL.TImportIPWL
from 	Lorenzo.dbo.WaitingList WaitingList  

inner join PTL.IPWLNetChange WLChanges 
	on WLChanges.WLIST_REFNO = WaitingList.WLIST_REFNO
	--from Lorenzo.dbo.WaitingList where ARCHV_FLAG = 'N' and SVTYP_REFNO = 1579 Inpatient Service
left join Lorenzo.dbo.Specialty Specialty 
	on Specialty.spect_refno = WaitingList.SPECT_REFNO 
	and Specialty.ARCHV_FLAG = 'N'
	
left join Lorenzo.dbo.ProfessionalCarer Carer 
	on Carer.PROCA_REFNO = WaitingList.PROCA_REFNO 
	
left join Lorenzo.dbo.Patient Patient 
	on Patient.patnt_refno = WaitingList.PATNT_REFNO
	
left join Lorenzo.dbo.ReferenceValue AdmissionMethod
	on AdmissionMethod.RFVAL_REFNO = WaitingList.ADMET_REFNO

left join Lorenzo.dbo.ReferenceValue AdminCategory
	on AdminCategory.RFVAL_REFNO = WaitingList.ADCAT_REFNO
	
left join Lorenzo.dbo.ReferenceValue IntendedManagement
	on IntendedManagement.RFVAL_REFNO = WaitingList.INMGT_REFNO
	
left join Lorenzo.dbo.ReferenceValue Priority
	on Priority.RFVAL_REFNO = WaitingList.PRITY_REFNO

left join Lorenzo.dbo.ReferenceValue ServiceType
	on ServiceType.RFVAL_REFNO = WaitingList.SVTYP_REFNO

left join Lorenzo.dbo.ReferenceValue AnaestheticType
	on AnaestheticType.RFVAL_REFNO = WaitingList.ANTYP_REFNO
	
left join Lorenzo.dbo.ReferenceValue WHOCO
	on WHOCO.RFVAL_REFNO = WaitingList.WHOCO_REFNO
	
left join Lorenzo.dbo.ReferenceValue RFV 
	ON RFV.RFVAL_REFNO = WaitingList.RTTST_REFNO

left join Lorenzo.dbo.ReferenceValue RemovalReason
	on RemovalReason.RFVAL_REFNO = WaitingList.REMVL_REFNO
	and RemovalReason.ARCHV_FLAG = 'N'

left join Lorenzo.dbo.ServicePoint AdmissionWard 
	on AdmissionWard.SPONT_REFNO = WaitingList.SPONT_REFNO

left join Lorenzo.dbo.ServicePoint SecondaryWard
	on SecondaryWard.Spont_refno = WaitingList.XFER_SPONT_REFNO

left join Lorenzo.dbo.WaitingListRule WaitingListRule 
	on WaitingListRule.WLRUL_REFNO = WaitingList.WLRUL_REFNO
	and WaitingListRule.archv_flag = 'N'
	
left join Lorenzo.dbo.AdmissionDecision	AdmissionDecision
	on AdmissionDecision.ADMDC_REFNO = WaitingList.ADMDC_REFNO
	and AdmissionDecision.archv_flag = 'N'
	
left join PTL.IPWLTCIDates TCIDates 
	on WaitingList.WLIST_REFNO = TCIDates.WLIST_REFNO

left join PTL.IPWLStartDates StartDates 
	on StartDates.WLIST_REFNO = WaitingList.WLIST_REFNO
	
left join PTL.IPWLWaitingStatus WaitingStatus 
	on WaitingStatus.SourceUniqueID = WaitingList.WLIST_REFNO
	
left join Lorenzo.dbo.Referral Referral 
	on Referral.refrl_refno = WaitingList.REFRL_REFNO
	and Referral.archv_flag = 'N'

left join Lorenzo.dbo.[User] CreatedUsers 
	on CreatedUsers.Code = WaitingList.USER_CREATE
	and CreatedUsers.archv_flag = 'N'
	
left join Lorenzo.dbo.[User] ModifiedUsers 
	on ModifiedUsers.Code = WaitingList.USER_MODIF
	and ModifiedUsers.archv_flag = 'N'
	

			


PRINT 'PTL.TImportIPWL initial build completed at '	+ CONVERT(varchar(17), getdate())
----------------------------------------------------------------------------------
--Add demographic data using temp table
----------------------------------------------------------------------------------

IF Object_id('tempdb..#IPWLDemog', 'U') IS NOT NULL
	DROP TABLE #IPWLDemog

select 
	WaitingList.WLIST_REFNO
	,WaitingList.PATNT_REFNO
	,WaitingList.WLIST_DTTM
	,DistrictNo = DistrictNo.IDENTIFIER
	,PatientPostcode = CAST(null as varchar)
	,OverseasStatusFlag = CAST(null as varchar)
into #IPWLDemog
from Lorenzo.dbo.WaitingList WaitingList  
inner join PTL.IPWLNetChange delta
	on delta.wlist_refno = WaitingList.WLIST_REFNO
left join Lorenzo.dbo.PatientIdentifier DistrictNo
	on	DistrictNo.PATNT_REFNO = WaitingList.PATNT_REFNO
	and	DistrictNo.PITYP_REFNO = 2001232 --facility
	and	DistrictNo.ARCHV_FLAG = 'N'
	and	DistrictNo.IDENTIFIER like 'RM2%'
	and	not exists
		(
		select
			1
		from
			Lorenzo.dbo.PatientIdentifier Previous
		where
			Previous.PATNT_REFNO = DistrictNo.PATNT_REFNO
		and	Previous.PITYP_REFNO = DistrictNo.PITYP_REFNO
		and	Previous.ARCHV_FLAG = 'N'
		and	Previous.IDENTIFIER like 'RM2%'
		and	(
				Previous.START_DTTM > DistrictNo.START_DTTM
			or
				(
					Previous.START_DTTM = DistrictNo.START_DTTM
				and	Previous.PATID_REFNO > DistrictNo.PATID_REFNO
				)
			)
		)
		
		
update #IPWLDemog
	set patientPostcode = --PatientAddress.PCODE
		case
			when len(PatientAddress.PCODE) = 6 then left(PatientAddress.PCODE, 2) + '   ' + right(PatientAddress.PCODE, 3)
			when len(PatientAddress.PCODE) = 7 then left(PatientAddress.PCODE, 3) + '  ' + right(PatientAddress.PCODE, 3)
			else PatientAddress.PCODE
		end
from #IPWLDemog dem
left join Lorenzo.dbo.Patient Patient 
	on Patient.patnt_refno = dem.PATNT_REFNO
left join Lorenzo.dbo.PatientAddressRole
	on	PatientAddressRole.PATNT_REFNO = Patient.PATNT_REFNO
	and	PatientAddressRole.ROTYP_CODE = 'HOME'
	and	PatientAddressRole.ARCHV_FLAG = 'N'
	and	exists
		(
		select
			1
		from
			Lorenzo.dbo.PatientAddress
		where
			PatientAddress.ADDSS_REFNO = PatientAddressRole.ADDSS_REFNO
		and	PatientAddress.ADTYP_CODE = 'POSTL'
		and	PatientAddress.ARCHV_FLAG = 'N'
		)
	and	dem.WLIST_DTTM between 
			PatientAddressRole.START_DTTM 
		and coalesce(
				PatientAddressRole.END_DTTM
				,dem.WLIST_DTTM
			)
	and	not exists
		(
		select
			1
		from
			Lorenzo.dbo.PatientAddressRole Previous
		where
			Previous.PATNT_REFNO = PatientAddressRole.PATNT_REFNO
		and	Previous.ROTYP_CODE = PatientAddressRole.ROTYP_CODE
		and	Previous.ARCHV_FLAG = 'N'
		and	exists
			(
			select
				1
			from
				Lorenzo.dbo.PatientAddress
			where
				PatientAddress.ADDSS_REFNO = Previous.ADDSS_REFNO
			and	PatientAddress.ADTYP_CODE = 'POSTL'
			and	PatientAddress.ARCHV_FLAG = 'N'
			)
		and	dem.WLIST_DTTM between 
				Previous.START_DTTM 
			and coalesce(
					Previous.END_DTTM
					,dem.WLIST_DTTM
				)
		and	(
				Previous.START_DTTM > PatientAddressRole.START_DTTM
			or	(
					Previous.START_DTTM = PatientAddressRole.START_DTTM
				and	Previous.ROLES_REFNO > PatientAddressRole.ROLES_REFNO
				)
			)
		)

left join Lorenzo.dbo.PatientAddress
	on	PatientAddress.ADDSS_REFNO = PatientAddressRole.ADDSS_REFNO
	and	PatientAddress.ARCHV_FLAG = 'N'


update #IPWLDemog
	set OverseasStatusFlag = OverseasVisitorStatus.OVSVS_REFNO
from #IPWLDemog dem
left join Lorenzo.dbo.OverseasVisitorStatus OverseasVisitorStatus
on	OverseasVisitorStatus.PATNT_REFNO = dem.PATNT_REFNO
and	OverseasVisitorStatus.ARCHV_FLAG = 'N'
and	dem.WLIST_DTTM between 
		OverseasVisitorStatus.START_DTTM 
	and coalesce(
			OverseasVisitorStatus.END_DTTM
			,dem.WLIST_DTTM
		)
	and	not exists
		(
		select
			1
		from
			Lorenzo.dbo.OverseasVisitorStatus Previous
		where
			Previous.PATNT_REFNO = OverseasVisitorStatus.PATNT_REFNO
		and	Previous.ARCHV_FLAG = 'N'
		and	dem.WLIST_DTTM between 
				Previous.START_DTTM 
			and coalesce(
					Previous.END_DTTM
					,dem.WLIST_DTTM
				)
		and	(
				Previous.START_DTTM > OverseasVisitorStatus.START_DTTM
			or	(
					Previous.START_DTTM = OverseasVisitorStatus.START_DTTM
				and	Previous.OVSEA_REFNO > OverseasVisitorStatus.OVSEA_REFNO
				)
			)
		)
		
PRINT '#IPWLDemog build completed at '	+ CONVERT(varchar(17), getdate())

update PTL.TImportIPWL set
--select 
	DistrictNo = dem.DistrictNo
	,PatientPostcode = dem.PatientPostcode
	,OverseasStatusFlag = dem.OverseasStatusFlag
from  PTL.TImportIPWL ipwl
inner join #IPWLDemog dem
	on dem.WLIST_REFNO = ipwl.SourceUniqueID
	
drop table #IPWLDemog

PRINT 'PTL.TImportIPWL demographic update completed at '	+ CONVERT(varchar(17), getdate())

Update	ipwl
Set		EffectiveGP = GPHistory.GPCode
		,EpisodicGpPracticeCode = GPHistory.PracticeCode
		,EpisodicGPPracticeRefno = GPHistory.PracticeRefno
From	PTL.TImportIPWL ipwl
		Left Join PAS.PatientGPHistory GPHistory
			On ipwl.SourcePatientNo = GPHistory.PatientNo
			And ipwl.DateOnList >= GPHistory.StartDate
			And (ipwl.DateOnList < GPHistory.EndDate Or GPHistory.EndDate Is Null)

PRINT 'PTL.TImportIPWL update effective GP update completed at '	+ CONVERT(varchar(17), getdate())
----------------------------------------------------------------------------------
--UPDATE Notes
----------------------------------------------------------------------------------
IF Object_id('tempdb..#IPWLNotes', 'U') IS NOT NULL
	DROP TABLE #IPWLNotes

select 
	notes.SORCE_REFNO
	,notes.SORCE_CODE
	,notes.NOTES_REFNO_NOTE
into #IPWLNotes
from Lorenzo.dbo.NoteRole notes
inner join PTL.IPWLNetChange delta 
	on delta.WLIST_REFNO = notes.SORCE_REFNO
where notes.SORCE_CODE in ('WLCMT', 'WLPRP')
and notes.archv_flag = 'N'

	
update PTL.TImportIPWL set
	GeneralComment =  ltrim(rtrim(replace(replace(NotesGeneral.NOTES_REFNO_NOTE,'[PROCEDURE:',''),']','')))
	,PatientPrepComments = NotesPrep.NOTES_REFNO_NOTE
from PTL.TImportIPWL ipwl
left join #IPWLNotes NotesGeneral 
	on NotesGeneral.SORCE_REFNO = ipwl.SourceUniqueID
	and NotesGeneral.SORCE_CODE = 'WLCMT' 
left join #IPWLNotes NotesPrep 
	on NotesPrep.SORCE_REFNO = ipwl.SourceUniqueID
	and NotesPrep.SORCE_CODE = 'WLPRP' 

drop table #IPWLNotes
PRINT 'PTL.TImportIPWL notes update completed at '	+ CONVERT(varchar(17), getdate())
----------------------------------------------------------------------------------
--UPDATE NEXT RESET DATE
----------------------------------------------------------------------------------

update PTL.TImportIPWL
	set NextResetDate = resets.ResetDate
from PTL.TImportIPWL ipwl 
inner join
	(
	select
		WLIST_REFNO,
		ResetDate = min(DATEADD(dd, 0, DATEDIFF(dd, 0,TCI_DTTM)))
		--ResetDate = min(cast(TCI_DTTM as datetime))
		
	from Lorenzo.dbo.AdmissionOffer 
	where OFOCM_REFNO IN (
		'2003599',
		'2000657',
		'2003600',
		'985',
		'2003601')
	AND ARCHV_FLAG = 'N' 
	--and  (cast(TCI_DTTM as datetime) > DATEADD(dd, 0, DATEDIFF(dd, 0,GETDATE())))
	and  (cast(TCI_DTTM as datetime) > GETDATE())
	group by WLIST_REFNO 
	) resets 
	on ipwl.SourceUniqueID = resets.WLIST_REFNO
where ipwl.RemovalDate IS NULL

PRINT 'PTL.TImportIPWL NextResetDate completed at '	+ CONVERT(varchar(17), getdate())
----------------------------------------------------------------------------------
--UPDATE WAITING STATUS FOR THOSE THAT HAVE BEEN ADMITTED WITH NO OFFER
----------------------------------------------------------------------------------


update PTL.TImportIPWL
	set EntryStatus = B.NEW_WL_STAT
from PTL.TImportIPWL ipwl 
inner join 
(
	SELECT 
		WaitStatus.SourceUniqueID
		,NEW_WL_STAT = CASE 
			WHEN ProfessionalCareEpisode.CEOCM_REFNO = '70' THEN
				CASE WHEN ProfessionalCareEpisode.END_DTTM IS NULL THEN 'EPI START-NO OUTCOME'
				ELSE
					'EPI END-NO OUTCOME'
				END
			END
		,ProfessionalCareEpisode.CEOCM_REFNO
		,C.DESCRIPTION
		,ProfessionalCareEpisode.START_DTTM
		,ProfessionalCareEpisode.END_DTTM
		,WaitStatus.EntryStatus
	FROM PTL.IPWLWaitingStatus WaitStatus 
	left join Lorenzo.dbo.ProfessionalCareEpisode ProfessionalCareEpisode   
		ON WaitStatus.SourceUniqueID = ProfessionalCareEpisode.WLIST_REFNO 
		AND ProfessionalCareEpisode.ARCHV_FLAG = 'N'
	left join Lorenzo.dbo.ReferenceValue C 
		ON ProfessionalCareEpisode.CEOCM_REFNO = C.RFVAL_REFNO 
		AND C.ARCHV_FLAG = 'N'
	WHERE WaitStatus.EntryStatus = 'WAITING' 
	AND WaitStatus.PCERemovalStatus IS NULL 
	AND NOT ProfessionalCareEpisode.WLIST_REFNO IS NULL 
	AND ProfessionalCareEpisode.PRVSN_FLAG = 'N' 
	AND ProfessionalCareEpisode.CEOCM_REFNO = '70'
) B 
	on ipwl.SourceUniqueID = B.SourceUniqueID
	
PRINT 'PTL.TImportIPWL EntryStatus completed at '	+ CONVERT(varchar(17), getdate())
--/*
----------------------------------------------------------------------------------
--REMOVE PAST TCI_DATES ASSOCIATED WITH MIGRATION
----------------------------------------------------------------------------------
--*/

update PTL.TImportIPWL
	set TCIDate = NULL
where TCIDate < '2006-12-01'

update PTL.TImportIPWL
	set TCIDate = NULL
where SourceUniqueID = 10475567

--/*
----------------------------------------------------------------------------------
--UPDATE PLANNED PROCEDURE WHERE IT HASN'T BEEN CODED FROM THE WAITING LIST
----------------------------------------------------------------------------------
--*/
update PTL.TImportIPWL
	set PlannedProcedure = Coding.CODE + ': ' + DiagCodes.[DESCRIPTION] + ' (UPDATED)'
from PTL.TImportIPWL ipwl  
left join Lorenzo.dbo.ClinicalCoding Coding 
	on ipwl.SourceUniqueID = Coding.SORCE_REFNO
left join Lorenzo.dbo.Diagnosis DiagCodes 
	on Coding.CODE = DiagCodes.CODE 
	and DiagCodes.ARCHV_FLAG = 'N' 
	and DiagCodes.END_DTTM IS NULL
where 
	(ipwl.PlannedProcedure = '' or ipwl.PlannedProcedure is null) 
	and Coding.SORCE_CODE = 'WLIST' 
	and Coding.DPTYP_CODE = 'PROCE'
	and ipwl.EntryStatus = 'WAITING' 
	and DiagCodes.CCSXT_CODE = 'OPCS4' 
	and Coding.SORT_ORDER = '1'
	and Coding.ARCHV_FLAG = 'N' 

--select count(1) from INFOSQL.INFORMATION_VER2.dbo.DGPRO
--select count(1) from Lorenzo.dbo.ClinicalCoding

--select count(1) from INFOSQL.INFORMATION_VER2.dbo.DIAGCODES
--select count(1) from Lorenzo.dbo.Diagnosis
--/*
----------------------------------------------------------------------------------
--UPDATE TCI DATE WHERE WRONG 18 WEEK OUTCOME HAS BEEN ENTERED
----------------------------------------------------------------------------------
--*/
update PTL.TImportIPWL 
set TCIDate = null
where PCEOutcome in (
	'18 DDT Add to WL Treatment Deferred',
	'18 DDT Add to WL Diag T Treat Deferred',
	'Discharged - No Treatment - Return to WL',
	'18 DTT WWait Treatment Deferred') 
and EntryStatus = 'WAITING'
and TCIDate < (getdate() -1)

PRINT 'PTL.[ExtractIPWLCCGBulk] completed at '	+ CONVERT(varchar(17), getdate())
