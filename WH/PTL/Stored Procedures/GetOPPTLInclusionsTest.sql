﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		PTL - Load OPPTLInclusions - Test Version

Notes:			Stored in 
				Use in the load of PTL.OPWL.

Versions:
				1.0.0.1 - 11/02/2016 - MT
					Was missing standard GP referrals in the where clause:
					Changed:
					and (Referral.SourceOfReferralCode = 2002324 or WaitingListLorenzo.WLIST_REFNO is not null)
					To:
					and (Referral.SourceOfReferralCode In (5300,2002324) or WaitingListLorenzo.WLIST_REFNO is not null)
					
				1.0.0.0 - 21/10/2012 - KO
					Created proc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
Create Procedure [PTL].[GetOPPTLInclusionsTest]
As

truncate table PTL.OPPTLInclusionsTest;
insert into PTL.OPPTLInclusionsTest (
	ReferralSourceUniqueID
	,CABAdjustedReferralDate
	,CABAdjustedReferralDateFlag
	)

select distinct
	ReferralSourceUniqueID = Referral.SourceUniqueID
	,CABAdjustedReferralDate =
		Coalesce(
			AttendedCABReferrals.FirstAttendedCABReferralDate
			,DATEADD(dd, 0, DATEDIFF(dd, 0, Referral.ReferralDate)) 
		)
	,CABAdjustedReferralDateFlag =
		case when AttendedCABReferrals.FirstAttendedCABReferralDate is not null
				then 'CABAdjustedReferralDate'
			else 'ReferralDate' 
		end
--into PTL.OPPTLInclusions
from RF.Encounter Referral

--Exclude referrals with attended appointment attached
left join OP.Encounter AttendedAppointment
	on Referral.SourceUniqueID = AttendedAppointment.ReferralSourceUniqueID
	and AttendedAppointment.AppointmentStatusCode in (
		2004151,	--attended
		357,		--attended on time
		2868		--Late - seen
		) 
	--and LEFT(AttendedAppointment.ClinicCode,3) <> 'CAB'

--Exclude referrals with DNA'd CAB appointment attached
left join OP.Encounter AttendedCABAppointment
	on Referral.SourceUniqueID = AttendedCABAppointment.ReferralSourceUniqueID
	and AttendedCABAppointment.AppointmentStatusCode in (
		358,	--DNA
		45		--Not specified
		) 
	and LEFT(AttendedCABAppointment.ClinicCode,3) = 'CAB'
	--and AttendedCABAppointment.ClinicCode <> 'CABPAIN'
	AND AttendedCABAppointment.AppointmentDate < DATEADD(DAY, -2, GETDATE())

--Exclude referrals with DNA'd discharged appointment attached
left join OP.Encounter DNADischargedAppointment
	on Referral.SourceUniqueID = DNADischargedAppointment.ReferralSourceUniqueID
	and DNADischargedAppointment.DisposalCode in (
		3855,	--discharged
		3845	--self-discharged
		)
	and DNADischargedAppointment.AppointmentStatusCode = 358 --dna

left join Lorenzo.dbo.WaitingList WaitingListLorenzo
	on Referral.SourceUniqueID = WaitingListLorenzo.REFRL_REFNO
	and remvl_dttm is null 
	--and (remvl_dttm is null or CONVERT(varchar(8), remvl_dttm, 12) = '121101')
	and WaitingListLorenzo.archv_flag = 'N' 
	and WaitingListLorenzo.svtyp_refno = '4223'
	AND (NOT WaitingListLorenzo.WLRUL_REFNO = 150000536 OR WaitingListLorenzo.WLRUL_REFNO IS NULL)
	
--left join OP.LorenzoWaitingList WaitingListLorenzo
--	on Referral.SourceUniqueID = WaitingListLorenzo.REFRL_REFNO

left join PAS.Specialty Specialty 
		on Referral.SpecialtyCode = Specialty.SpecialtyCode

--Appointments booked during CAB clinic may have referral date set as 'Attendance' date
--of the CAB clinic rather than the original referral date. Substitute the original referral date
--from the CAB referral into the new referral.

--Included here to allow CABAdjustedRefDate to be calculated first
left join
		(select distinct
			Ref.SourcePatientNo
			,FirstAttendedCABReferralDate = DATEADD(dd, 0, DATEDIFF(dd, 0, Ref.ReferralDate)) 
			,FirstAttendedCABAppt.FirstApptDate
		FROM RF.Encounter Ref 
		inner join (
				select 
					Appt.ClinicCode
					,Appt.ReferralSourceUniqueID
					,min(Appt.AppointmentDate) as FirstApptDate
					,Appt.AppointmentStatusCode
				from OP.Encounter Appt
				group by Appt.ClinicCode, Appt.ReferralSourceUniqueID, Appt.AppointmentStatusCode
				having LEFT(Appt.ClinicCode, 3) = 'CAB'
				and Appt.AppointmentStatusCode in (2004151, 357, 2868) --attended, attended on time, Late - seen
				) FirstAttendedCABAppt
			on FirstAttendedCABAppt.ReferralSourceUniqueID = Ref.SourceUniqueID
		where FirstAttendedCABAppt.FirstApptDate is not null
		)AttendedCABReferrals
	on AttendedCABReferrals.SourcePatientNo = Referral.SourcePatientNo
	and DATEADD(dd, 0, DATEDIFF(dd, 0, AttendedCABReferrals.FirstApptDate)) = DATEADD(dd, 0, DATEDIFF(dd, 0, Referral.ReferralDate))

where AttendedAppointment.EncounterRecno is null
and AttendedCABAppointment.EncounterRecno is null
and DNADischargedAppointment.EncounterRecno is null
and  Referral.RequestedService in (
		9,		--Not specified
		232		--232 = Outpatient Activity
		)
and Referral.CancellationDate is null
and Referral.DischargeDate is null --Referral completion date
and (Referral.SourceOfReferralCode In (5300,2002324) or WaitingListLorenzo.WLIST_REFNO is not null)
and Specialty.Specialty NOt In ('ANTE-NATAL','MIDWIFE EPISODE','Obstetrics')
