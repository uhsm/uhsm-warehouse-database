﻿

/*
--Author: K Oakden
--Date created: 21/10/2012
SP used to generate rescheduled appointment list directly from Lorenzo

--Dependencies
Lorenzo

--To do
Implement as part of WH PAS Load
*/
CREATE procedure [PTL].[GetOPRescheduleCount] as

--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PTL].[AppoinmentRescheduleCount]') AND type in (N'U'))
--DROP TABLE [PTL].[AppoinmentRescheduleCount]
--GO

truncate table PTL.AppointmentRescheduleCount;
insert into PTL.AppointmentRescheduleCount (APPT_REF, TOTAL_PAT_CANCELLATIONS)
select distinct
--CAST(ScheduleEvent.PATNT_REFNO AS VARCHAR(20)) AS PAT_REF,
--CAST(ScheduleHistory.SCDHS_REFNO AS VARCHAR(20)) AS HIST_REF,
CAST(ScheduleHistory.SCHDL_REFNO AS VARCHAR(20)) AS APPT_REF,
LastSchdlChange.TOTAL_CANC As TOTAL_PAT_CANCELLATIONS
--CAST(OLD_START_DTTM AS DATETIME) AS OLD_APPT_DATE,
--CAST(ScheduleHistory.NEW_START_DTTM AS DATETIME) AS NEW_APPT_DATE,
--CAST(ScheduleHistory.MOVE_DTTM AS DATETIME) AS DATE_RESCHEDULED,
--ScheduleHistory.COMMENTS,
--CAST(ScheduleHistory.CREATE_DTTM AS DATETIME) AS DATE_CREATED
--into PTL.AppointmentRescheduleCount
FROM Lorenzo.dbo.ScheduleHistory ScheduleHistory

INNER JOIN 
	(SELECT
		SCHDL_REFNO,
		MAX(SCDHS_REFNO) AS LAST_CHANGE,
		COUNT(1) AS TOTAL_CANC
	FROM Lorenzo.dbo.ScheduleHistory
	WHERE MOVRN_REFNO = 4520
		AND ARCHV_FLAG = 'N'
		AND CAST(OLD_START_DTTM AS DATETIME) <= CAST(FLOOR(CAST(GETDATE() AS FLOAT)) AS DATETIME)
	GROUP BY SCHDL_REFNO
	) LastSchdlChange 
	ON ScheduleHistory.SCDHS_REFNO = LastSchdlChange.LAST_CHANGE

INNER JOIN Lorenzo.dbo.ScheduleEvent ScheduleEvent 
	ON ScheduleHistory.SCHDL_REFNO = ScheduleEvent.SCHDL_REFNO

where ScheduleHistory.ARCHV_FLAG = 'N'
AND ScheduleHistory.MOVRN_REFNO = 4520
AND ScheduleEvent.SCTYP_REFNO = 1470
AND CAST(ScheduleHistory.OLD_START_DTTM AS DATETIME) <= CAST(FLOOR(CAST(GETDATE() AS FLOAT)) AS DATETIME)




