﻿
CREATE procedure [PTL].[GetOPWaitingListResets] as

/**----------------------------------------------------------------------------
KO 
Date		Author		Action		Description
-------------------------------------------------------------------------------
17/10/2014	KO			Create		To get latest OP reset date

used by [PTL].[BuildOPWaitingListPartialBooking] 
used by [PTL].[BuildOPWaitingList] 
------------------------------------------------------------------------------**/
if Object_id('tempdb..#wl_resets', 'U') is not null
	drop table #wl_resets
	
--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PTL].[wlReset]') AND type in (N'U'))
--DROP TABLE [PTL].[wlReset]

select * into #wl_resets from (
	select 
		wlist_refno
		,ResetDate = cast(old_start_dttm as datetime)
	from Lorenzo.dbo.ScheduleHistory
	where movrn_refno in (
			'383'
			,'3248'
			,'4520'
			,'2003539'
			,'2003540'
			,'2003541'
			,'2003544'
			,'2004148'
			)
	and archv_flag = 'N'
	and (new_start_dttm > old_start_dttm)
	and not wlist_refno is null
	
	union all
	
	select 
		wlist_refno
		,ResetDate = cast(start_dttm as datetime)
	from Lorenzo.dbo.ScheduleEvent
	where attnd_refno in (
			'2868'
			,'358'
			,'2870'
			,'2004301'
			,'2000724'
			,'358'
			,'2003494'
			,'2003495'
			)
	and archv_flag = 'n'
	and not wlist_refno is null
) AllResets

--drop table PTL.OPWaitingListResets
truncate table PTL.OPWaitingListResets
--Get last reset date
insert into PTL.OPWaitingListResets (wlist_refno, LastResetDate)
select 
	wlist_refno
	,LastResetDate = max(ResetDate)
from #wl_resets
where ResetDate < dateadd(day, 0, datediff(day, 0, GETDATE()))
group by wlist_refno

--Add next reset date for existing records
update PTL.OPWaitingListResets
	set NextResetDate = nextR.NextResetDate
from 
PTL.OPWaitingListResets r
inner join (
	select 
		wlist_refno
		,NextResetDate = min(ResetDate)
	from #wl_resets
	where ResetDate > dateadd(day, 0, datediff(day, 0, GETDATE()))
	group by wlist_refno
	) nextR
on nextR.wlist_refno = r.wlist_refno

--Add next reset date for new records
insert into PTL.OPWaitingListResets (wlist_refno, NextResetDate)
select 
	wlist_refno
	,NextResetDate = min(ResetDate)
from #wl_resets
where ResetDate > dateadd(day, 0, datediff(day, 0, GETDATE()))
and wlist_refno not in (select wlist_refno from PTL.OPWaitingListResets)
group by wlist_refno
	
----ALTER TABLE [PTL].[OPWaitingListResets] ADD  CONSTRAINT [PK_OPWaitingListResets] PRIMARY KEY CLUSTERED 
----(
----	[wlist_refno] ASC
----)

drop table #wl_resets
