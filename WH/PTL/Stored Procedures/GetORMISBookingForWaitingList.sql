﻿
CREATE  Procedure [PTL].[GetORMISBookingForWaitingList] as  
/*
Author: K Oakden
Date: 31/01/2014
Copied from [Information_Reporting].[dbo].[uspGetORMISBookingForWaitingList]
For Admitted PTL

--To do - change to read from WH/WHREPORTING so no clash when ORMIS db restore
*/

--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PTL].[ORMISWaitingListBooking]') AND type in (N'U'))
--DROP TABLE [PTL].[ORMISWaitingListBooking]
declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

truncate table PTL.ORMISWaitingListBooking

insert into PTL.ORMISWaitingListBooking
select
	SRC.SourceUniqueID as WLIST_REFNO,
	SRC.DateOnList as DateOnList,
	SRC.EntryStatus AS WaitingListStatus,
	A.WD_EPISODE_NUM AS OrmisWaitingListRef,
	B.PA_SEQU as OrmisBookingSeq,
	B.PA_WL_SEQU as OrmisWaitingListSeq,
	A.WD_PATIENT_IDENTIFIER AS OrmisPatientID,
	A.WD_LISTING_DATE AS OrmisDateOnList,
	B.PA_OPER_DATE As OrmisOperationDate,
	B.PA_SESSION_NO As OrmisSessionNo,
	CASE B.PA_TRANSF
		WHEN 0 THEN 'BOOKED'
		WHEN 1 THEN 'COMPLETE'
		WHEN 3 THEN 'ON HOLD'
		WHEN 5 THEN 'INP WAITING'
		ELSE 'NOT BOOKED'
	END AS  OrmisBookingStatus,
	C.TH_DESC AS Theatre,
	B.PA_UNPLANNED As Unplanned,
	B.PA_TYPE As OrmisBookingType
--INTO PTL.ORMISWaitingListBooking
from PTL.IPWL SRC 
left join 
	(SELECT ORM1.* 
	FROM 	
	ORMIS.dbo.F_Waiting_Details ORM1 
	INNER JOIN
		(
		SELECT
		WD_EPISODE_NUM,
		MAX(WD_SEQU) AS LASTREC
		FROM ORMIS.dbo.F_Waiting_Details
		GROUP BY WD_EPISODE_NUM
		) ORM2 
		ON ORM1.WD_EPISODE_NUM = ORM2.WD_EPISODE_NUM AND ORM1.WD_SEQU = ORM2.LASTREC
	) A
	ON SRC.SourceUniqueID = CAST(A.WD_EPISODE_NUM AS VARCHAR(20))
	
LEFT OUTER JOIN 
	(
	SELECT ORM1.*
	FROM ORMIS.dbo.FPATS ORM1
	INNER JOIN
		(
		SELECT 
		PA_EPISODE_NUM,
		MAX(PA_SEQU) AS LASTBOOKING
		FROM ORMIS.dbo.FPATS
		GROUP BY
		PA_EPISODE_NUM
		) ORM2 ON ORM1.PA_EPISODE_NUM = ORM2.PA_EPISODE_NUM AND ORM1.PA_SEQU = ORM2.LASTBOOKING
	) b
	ON A.WD_SEQU = B.PA_WL_SEQU and B.PA_TRANSF <> 2
LEFT OUTER JOIN ORMIS.dbo.FTHEAT C ON B.PA_TH_SEQU = C.TH_SEQU
WHERE SRC.DataSource = 'IPM'

select @RowsInserted = @@Rowcount

select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Rows inserted to PTL.ORMISWaitingListBooking '  + CONVERT(varchar(10), @RowsInserted) + 
	'. Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WH GetORMISBookingForWaitingList', @Stats, @StartTime
--print @Stats


