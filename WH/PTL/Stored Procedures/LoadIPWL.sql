﻿

CREATE procedure [PTL].[LoadIPWL] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
--declare @RowsDeleted Int
declare @Stats varchar(255)

select @StartTime = getdate()

PRINT 'PTL.LoadIPWL started at '	+ CONVERT(varchar(17), @StartTime)
--Delete updated records
--delete from PTL.IPWL
--from PTL.IPWL IPWL
--inner join PTL.IPWLNetChange delta 
--ON IPWL.SourceUniqueID = delta.wlist_refno

--select @RowsDeleted = @@Rowcount

truncate table ptl.IPWL

INSERT INTO [PTL].[IPWL]
	(
		[DataSource]
		,[SourceUniqueID]
		,[SourcePatientNo]
		,[ReferralSourceUniqueID]
		,[DistrictNo]
		,[NHSNumber]
		,[PatientForename]
		,[PatientSurname]
		,[DateOfBirth]
		,[SpecialtyCode]
		,[Consultant]
		,[ListType]
		,[ElectiveAdmissionMethod]
		,[AdminCategory]
		,[IntendedManagement]
		,[Priority]
		,[ReferralDate]
		,[OriginalDateOnList]
		,[DateOnList]
		,[WaitingStartDate]
		,[SuspendedStartDate]
		,[SuspendedEndDate]
		,[SuspToDate]
		,[SuspensionPeriod]
		,[SuspPatEndDate]
		,[SuspToDatePtReason]
		,[SuspensionPeriodPtReason]
		,[CurrentSuspension]
		,[CurrentSuspensionType]
		,[RemovalDate]
		,[GuaranteedAdmissionDate]
		,[AdmissionDate]
		,[PlannedProcedure]
		,[AnaestheticType]
		,[ServiceType]
		,[ListName]
		,[AdmissionWard]
		,[EstimatedTheatreTime]
		,[WhoCanOperate]
		,[GeneralComment]
		,[PatientPrepComments]
		,[RemovalReason]
		,[EntryStatus]
		,[OfferOutcome]
		,[PCEOutcome]
		,[TCIDate]
		,[DateTCIOffered]
		,[EffectiveGP]
		,[CancerCode]
		,[DateRecordCreated]
		,[CreatedByUser]
		,[DateRecordModified]
		,[ModifiedByUser]
		,[ShortNoticeFlag]
		,[AdmitByDate]
		,[NextResetDate]
		,[PPID]
		,[RTTCode]
		,[RTTDescription]
		,[IPWLDiagnosticProcedure]
		,[Sex]
		,[Ethnicity]
		,[PreOpClinic]
		,[PreOpDate]
		,[PreOpClinicDescription]
		,[SecondaryWard]
		,[EpisodicGpPracticeCode]
		,[EpisodicGpPracticeRefno]
		,[PurchaserCode]
		,[PatientPostcode]
		,[OverseasStatusFlag]

	)
SELECT 
	[DataSource]
	,[SourceUniqueID]
	,[SourcePatientNo]
	,[ReferralSourceUniqueID]
	,[DistrictNo]
	,[NHSNumber]
	,[PatientForename]
	,[PatientSurname]
	,[DateOfBirth]
	,[SpecialtyCode]
	,[Consultant]
	,[ListType]
	,[ElectiveAdmissionMethod]
	,[AdminCategory]
	,[IntendedManagement]
	,[Priority]
	,[ReferralDate]
	,[OriginalDateOnList]
	,[DateOnList]
	,[WaitingStartDate]
	,[SuspendedStartDate]
	,[SuspendedEndDate]
	,[SuspToDate]
	,[SuspensionPeriod]
	,[SuspPatEndDate]
	,[SuspToDatePtReason]
	,[SuspensionPeriodPtReason]
	,[CurrentSuspension]
	,[CurrentSuspensionType]
	,[RemovalDate]
	,[GuaranteedAdmissionDate]
	,[AdmissionDate]
	,[PlannedProcedure]
	,[AnaestheticType]
	,[ServiceType]
	,[ListName]
	,[AdmissionWard]
	,[EstimatedTheatreTime]
	,[WhoCanOperate]
	,[GeneralComment]
	,[PatientPrepComments]
	,[RemovalReason]
	,[EntryStatus]
	,[OfferOutcome]
	,[PCEOutcome]
	,[TCIDate]
	,[DateTCIOffered]
	,[EffectiveGP]
	,[CancerCode]
	,[DateRecordCreated]
	,[CreatedByUser]
	,[DateRecordModified]
	,[ModifiedByUser]
	,[ShortNoticeFlag]
	,[AdmitByDate]
	,[NextResetDate]
	,[PPID]
	,[RTTCode]
	,[RTTDescription]
	,[IPWLDiagnosticProcedure]
	,[Sex]
	,[Ethnicity]
	,[PreOpClinic]
	,[PreOpDate]
	,[PreOpClinicDescription]
	,[SecondaryWard]
	,[EpisodicGpPracticeCode]
	,[EpisodicGpPracticeRefno]
	,[PurchaserCode]
	,[PatientPostcode]
	,[OverseasStatusFlag]
  FROM [PTL].[TImportIPWL]

select @RowsInserted = @@Rowcount

--KO removed 28/03/2015
--Update MissingPatient
--exec dbo.UpdateMissingPatientIPWL

--Update ResponsibleCommissioner
exec dbo.LoadResponsibleCommissionerIPWL

select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Rows inserted to PTL.IPWL '  + CONVERT(varchar(10), @RowsInserted) + 
	'. Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WH LoadIPWL', @Stats, @StartTime
--print @Stats

--Remove test patients
delete from PTL.IPWL 
from PTL.IPWL ipwltab
--select * from PTL.IPWL ipwltab
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.ExcludedPatient
	where
		ExcludedPatient.SourcePatientNo = ipwltab.SourcePatientNo
	)

