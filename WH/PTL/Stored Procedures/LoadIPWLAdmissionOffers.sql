﻿


CREATE procedure [PTL].[LoadIPWLAdmissionOffers] AS 

--SET DATEFORMAT dmy
declare @RowCount Int
declare @StartTime datetime

select @StartTime = getdate()

PRINT 'PTL.LoadIPWLAdmissionOffers started at '	+ CONVERT(varchar(17), @StartTime)

truncate table PTL.IPWLAdmissionOffers

insert into PTL.IPWLAdmissionOffers

select distinct
	--'IPM' AS DATA_SOURCE,
	SourcePatientNo = WaitingList.PATNT_REFNO
	,SourceUniqueID = AdmissionOffer.ADMOF_REFNO
	,WaitingListID = AdmissionOffer.WLIST_REFNO
	,OfferOutcomeCode = OfferOutcome.MAIN_CODE
	,OfferOutcomeDescription = OfferOutcome.[DESCRIPTION]
	,OfferDate = AdmissionOffer.ADMOF_DTTM
	,OfferOutcomeDate = AdmissionOffer.OFOCM_DTTM
	,TCIDate = AdmissionOffer.TCI_DTTM
	,CancellationDate = AdmissionOffer.CANCEL_DTTM
	,ConfirmationDate = AdmissionOffer.CONFM_DTTM
	,DeferralFlag = AdmissionOffer.DEFER_FLAG
	,DeferralDate = AdmissionOffer.DEFER_DTTM
	,DeferralReasonCode = DeferralReason.MAIN_CODE
	,DeferralReasonDescription = DeferralReason.[DESCRIPTION]
	--,DateRecordCreated = DATEADD(dd, 0, DATEDIFF(dd, 0,AdmissionOffer.CREATE_DTTM)) 
	,DateRecordCreated = AdmissionOffer.CREATE_DTTM
	,DateRecordModified = AdmissionOffer.MODIF_DTTM
	,LatestOffer = NULL
--into PTL.IPWLAdmissionOffers
from Lorenzo.dbo.AdmissionOffer AdmissionOffer
left join Lorenzo.dbo.WaitingList WaitingList 
	on AdmissionOffer.WLIST_REFNO = WaitingList.WLIST_REFNO
	
left join Lorenzo.dbo.ReferenceValue OfferOutcome
	on AdmissionOffer.OFOCM_REFNO = OfferOutcome.RFVAL_REFNO 
	and OfferOutcome.ARCHV_FLAG = 'N'

left join Lorenzo.dbo.ReferenceValue DeferralReason
	on AdmissionOffer.DEFER_REFNO = DeferralReason.RFVAL_REFNO 
	and DeferralReason.ARCHV_FLAG = 'N'

where AdmissionOffer.ARCHV_FLAG = 'N' 
and WaitingList.ARCHV_FLAG = 'N'
--ORDER BY A.WLIST_REFNO, A.ADMOF_REFNO

select @RowCount = @@rowcount
PRINT CONVERT(varchar(17), getdate())
	+ ': ' + 'IPWLAdmissionOffers built. ' 
	+ CONVERT(varchar(10), @RowCount)  + ' records inserted.'
	
--Remove test patients
delete from PTL.IPWLAdmissionOffers 
from PTL.IPWLAdmissionOffers ipwltab
--select * from PTL.IPWLAdmissionOffers ipwltab
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.ExcludedPatient
	where
		ExcludedPatient.SourcePatientNo = ipwltab.SourcePatientNo
	)

update PTL.IPWLAdmissionOffers
set LatestOffer = 'Y'
from PTL.IPWLAdmissionOffers A INNER JOIN
	(
	select
		WaitingListID,
		max(SourceUniqueID) AS lastoffer
	from PTL.IPWLAdmissionOffers
	group by WaitingListID
	) B ON A.SourceUniqueID = B.lastoffer



