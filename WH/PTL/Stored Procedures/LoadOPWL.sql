﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		PTL - Load OPWL

Notes:			Stored in 
				Used to generate master OP PTL table.

Dependencies:	PTL.OPPTLInclusions

Versions:
				1.0.0.3 - 21/10/2015 - MT
					Use the revised table PatientGPHistory.

				1.0.0.2 - 24/02/2014 - KO
					Changes for schedule 6:
					Added NHSNumber, PatientPostcode, RTTBreachDate, RTTWeeksWait
					Amended Contract_ReferralPriority, Contract_Sex, Contract_Ethnicity to national codes

				1.0.0.1 - 04/12/2013 - KO
					KO Hard coded specialty code to 370C (150000185 - breast oncology)
					where ConsultantCode = '10000340' (Prof Howell).

				1.0.0.0 - 20/10/2012 - KO
					Created proc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [PTL].[LoadOPWL]
As

Truncate Table PTL.OPWL;

INSERT INTO [PTL].[OPWL]
           ([CensusDate]
           ,[ReferralSourceUniqueID]
           ,[ReferredToSpecialty]
           ,[ReferredToSpecialtyCode]
           ,[ReferredToConsultant]
           ,[ReferredToConsultantCode]
           ,[SourcePatientNo]
           ,[PatientId]
           ,[NHSNumber]
           ,[PatientSurname]
		   ,[PatientPostcode]
           ,[ReferralPriority]
           ,[DateOnReferrersWL]
           ,[DateOfReferral]
           ,[WaitingListStartDate]
           ,[CABAdjustedReferralDate]
           ,[RTTReferralDate]
           ,[RTTReferralDateFlag]
           ,[RTTBreachDate]
		   ,[RTTWeeksWait]
           ,[FirstAppointmentStatus]
           ,[AppointmentSourceUniqueID]
           ,[CurrentAppointmentType]
           ,[CurrentClinic]
           ,[CurrentAppointmentDate]
           ,[ActivityType]
           ,[ReferralSource]
           ,[ReferralStatus]
           ,[ReferringPCT]
           ,[ReferringPracticeCode]
           ,[NumberOfPatientCancellations]
           ,[NumberOfHospitalCancellations]
           ,[NumberOfPatientReschedules]
           ,[NumberOfDNAs]
           ,[WLSourceUniqueID]
           ,[Contract_DateOfBirth]
           ,[Contract_Age]
           ,[Contract_Sex]
           ,[Contract_ReferringGpCode]
           ,[Contract_ReferringPracticeCode]
           ,[Contract_ReferringCCGCode]
           ,[Contract_ReferringCCG]
           ,[Contract_SourceOfReferralCode]
           ,[Contract_Site]
           ,[Contract_PPID]
           ,[Contract_Ethnicity]
           ,[Contract_UBRN]
           ,[Contract_RTTCurrentStatusCode]
           ,[Contract_ConsultantCode]
           ,[Contract_SpecialtyCode]
           ,[Contract_Provider]
           ,[Contract_RegisteredGpCode]
           ,[Contract_RegisteredGpPracticeCode]
           ,[Contract_RegisteredPCTCode]
           ,[Contract_ReferralPriority]
           ,[Contract_LastDNAPatCancDate])
SELECT distinct
	CensusDate = GETDATE()
	,ReferralSourceUniqueID = Referral.SourceUniqueID
	,ReferredToSpecialty = --RefToSpecialty.Specialty
		case 
			when Referral.ConsultantCode = '10000340' --Prof Howell
			then 'Breast Oncology' 
			else RefToSpecialty.Specialty
		end
	,ReferredToSpecialtyCode = --RefToSpecialty.LocalSpecialtyCode
		case 
			when Referral.ConsultantCode = '10000340' --Prof Howell
			then '370C' --Local code 370C for breast oncology)
			else RefToSpecialty.LocalSpecialtyCode
		end
	--,RefToSpec.SpecialtyFunctionCode
	--,ReferredToConsultant = case when charindex(',',RefToConsultant.Consultant) > 0
	--		then left(RefToConsultant.Consultant,(charindex(',',RefToConsultant.Consultant)-1))
	--		else RefToConsultant.Consultant
	--		end
	,ReferredToConsultant = RefToConsultant.Consultant
	,ReferredToConsultantCode = RefToConsultant.NationalConsultantCode
	,Referral.SourcePatientNo
	,PatientId = 
		Coalesce(
			Referral.DistrictNo,
			'Email Clare Bell-Hartley - ' + cast(Referral.SourceUniqueID as varchar)
		)
	,NHSNumber = Referral.NHSNumber
	,PatientSurname = Referral.PatientSurname
	,PatientPostcode = Referral.Postcode
	,ReferralPriority = ReferralPriority.ReferenceValue
	,DateOnReferrersWL = OriginalProviderReferralDate
	,DateOfReferral = DATEADD(dd, 0, DATEDIFF(dd, 0, Referral.ReferralDate)) 
	--,FirstApptDNADate = FirstApptDNA.FirstApptDNADate
	,WaitingListStartDate = WaitingListLorenzo.WLIST_DTTM
	,Included.CABAdjustedReferralDate
	,RTTReferralDate = 
		--Coalesce(FirstApptDNA.FirstApptDNADate,
			case
				when Referral.OriginalProviderReferralDate < Included.CABAdjustedReferralDate 
					then Referral.OriginalProviderReferralDate
				else Included.CABAdjustedReferralDate
			end
		--)
	,RTTReferralDateFlag = 
		case
			--when FirstApptDNA.FirstApptDNADate is not null
			--	then 'FirstDNAApptDate'
			when Referral.OriginalProviderReferralDate < Included.CABAdjustedReferralDate  
				then 'DateOnReferrersWL'
			else Included.CABAdjustedReferralDateFlag
		end
	----Breach date calculated directly in SSRS report
	,RTTBreachDate = 
			case
				when Referral.OriginalProviderReferralDate < Included.CABAdjustedReferralDate 
					then dateadd(day, 126, Referral.OriginalProviderReferralDate)
				else dateadd(day, 126, Included.CABAdjustedReferralDate)
			end
	,RTTWeeksWait = 
			case
				when Referral.OriginalProviderReferralDate < Included.CABAdjustedReferralDate 
					then datediff(day, Referral.OriginalProviderReferralDate, GETDATE())/7
				else datediff(day, Included.CABAdjustedReferralDate, GETDATE())/7
			end
	,FirstAppointmentStatus = case 
			when ActiveAppointment.SourceUniqueID is null then 'Undated'
			else 'Booked'
		end
	,AppointmentSourceUniqueID = ActiveAppointment.SourceUniqueID
	,ActiveAppointment.CurrentAppointmentType
	,ActiveAppointment.CurrentClinic
	,ActiveAppointment.CurrentAppointmentDate
	,ActivityType =  
		case 
			when Referral.SpecialtyCode = 2000924 then 'Dietetics'
			when Referral.SpecialtyCode = 2000888 then 'Physiotherapist'
			when Referral.SpecialtyCode = 10000217 then 'Voice'
			else RefToConsultant.ProfessionalCarerType
		end 
	,ReferralSource = ReferralSource.ReferenceValue
	,ReferralStatus = ReferralStatus.ReferenceValue
	,ReferringPCT = PCT.OrganisationLocalCode
	,ReferringPracticeCode = isnull(PracticeHistory.PracticeCode, 'V81999')
	,NumberOfPatientCancellations = PatCancellations.CancellationCount
	,NumberOfHospitalCancellations = HospCancellations.CancellationCount
	,NumberOfPatientReschedules = RescheduledAppts.TOTAL_PAT_CANCELLATIONS
	,NumberOfDNAs = NewApptDNACount.DNACount
	,WLSourceUniqueID = WaitingListLorenzo.WLIST_REFNO
	,Contract_DateOfBirth = Referral.DateOfBirth
	,Contract_Age = WHREPORTING.dbo.GetAgeFromBirthDate(Referral.DateOfBirth)
	,Contract_Sex = coalesce(Sex.MappedCode, 0)
	,Contract_ReferringGpCode = 
		case 
			when ReferralSource.MainCode in ('3', 'EBS') --GP referral
			then RefByConsultant.NationalConsultantCode
			else EpisodicGP.NationalConsultantCode
		end
	,Contract_ReferringPracticeCode = 
		case 
			when ReferralSource.MainCode in ('3', 'EBS') --GP referral
			then ReferringPractice.OrganisationLocalCode
			else EpisodicPractice.OrganisationLocalCode
		end

	,Contract_ReferringCCGCode = 
		case 
			when ReferralSource.MainCode in ('3', 'EBS') --GP referral
			then coalesce(
				ODSReferringPractice.ParentOrganisationCode
				,Referral.ResponsibleCommissionerCCGOnly)
			else ODSEpisodicPractice.ParentOrganisationCode
		end
	,Contract_ReferringCCG = cast(null as varchar(50))
	,Contract_SourceOfReferralCode = ReferralSource.MappedCode
	,Contract_Site = cast(null as varchar(20))
	,Contract_PPID = Referral.RTTPathwayID
	,Contract_Ethnicity = case when EthnicGroup.MappedCode = '99' then null else EthnicGroup.MappedCode end
	,Contract_UBRN = ActiveAppointment.CurrentEBRN
	,Contract_RTTCurrentStatusCode = RTTCurrentStatus.MainCode
	,Contract_ConsultantCode = RefToConsultant.NationalConsultantCode
	,Contract_SpecialtyCode = RefToSpecialty.NationalSpecialtyCode
	,Contract_Provider = 'RM200'
	,Contract_RegisteredGpCode = RegisteredGp.NationalConsultantCode
	,Contract_RegisteredGpPracticeCode = RegisteredPractice.OrganisationLocalCode
	,Contract_RegisteredPCTCode = RegisteredCCG.OrganisationLocalCode
	,Contract_ReferralPriority = ReferralPriority.MappedCode
	,Contract_LastDNAPatCancDate = cast(null as datetime)
	--into PTL.OPWL
FROM RF.Encounter Referral

	inner join PTL.OPPTLInclusions Included
	on Referral.SourceUniqueID = Included.ReferralSourceUniqueId
	
	--KO to check that only the first active appt is required
	--PM/HR - all appointments reqd where first appointment status flag is new. 09/11/2012
	left join 
		(select
			Appt.SourceUniqueID
			,Appt.ReferralSourceUniqueID
			,CurrentClinic = Appt.ClinicCode
			,CurrentAppointmentDate = Appt.AppointmentDate
			,CurrentEBRN = Appt.EBookingReferenceNo
			,CurrentAppointmentType = Appt.FirstAttendanceFlag
		from OP.Encounter Appt
		where Appt.AppointmentCancelDate is null
		and Appt.AppointmentStatusCode = 45 --ATTND Not Specified
		--PM 09/11/2012 First attendance flag removed becuase first appts might be set as fu on PAS
		--PM need to write report to identify where first appt (due to subsequent DNAs / cancellations) marked as FU on iPM
		--and Appt.FirstAttendanceFlag = 9268 --new
		--and not exists (
		--	select 1 from OP.Encounter PreviousAppointment
		--	where PreviousAppointment.ReferralSourceUniqueID = Appt.ReferralSourceUniqueID
		--	and PreviousAppointment.AppointmentCancelDate is null
		--	and PreviousAppointment.AppointmentStatusCode = 45 --ATTND Not Specified
		--	and PreviousAppointment.AppointmentTime < Appt.AppointmentTime
		--	)
		)ActiveAppointment
		on Referral.SourceUniqueID = ActiveAppointment.ReferralSourceUniqueID
			
	left join Lorenzo.dbo.WaitingList WaitingListLorenzo
		on Referral.SourceUniqueID = WaitingListLorenzo.REFRL_REFNO
		and remvl_dttm is null 
		and WaitingListLorenzo.archv_flag = 'N' 
		and WaitingListLorenzo.svtyp_refno = '4223'
		AND (NOT WaitingListLorenzo.WLRUL_REFNO = 150000536 OR WaitingListLorenzo.WLRUL_REFNO IS NULL)
		
	left join PAS.Specialty RefToSpecialty 
		on Referral.SpecialtyCode = RefToSpecialty.SpecialtyCode
		
	left join PAS.Consultant RefToConsultant 
		on Referral.ConsultantCode = RefToConsultant.ConsultantCode

	left join PAS.ReferenceValue ReferralPriority 
		on Referral.PriorityCode = ReferralPriority.ReferenceValueCode
	
	left join PAS.ReferenceValue ReferralSource
		on Referral.SourceOfReferralCode = ReferralSource.ReferenceValueCode
		
	left join PAS.ReferenceValue ReferralStatus 
		on Referral.ReferralStatus = ReferralStatus.ReferenceValueCode
		
	left join PAS.PatientGPHistory PracticeHistory
		on Referral.SourcePatientNo = PracticeHistory.PatientNo
		and DW_REPORTING.LIB.fn_GetJustDate(Referral.ReferralDate) >= PracticeHistory.StartDate
		and (DW_REPORTING.LIB.fn_GetJustDate(Referral.ReferralDate) < PracticeHistory.EndDate Or PracticeHistory.EndDate Is Null)
	
	left join PAS.Organisation Practice
		on PracticeHistory.PracticeRefno = Practice.OrganisationCode
		
	left join PAS.Organisation PCT
		on Practice.ParentOrganisationCode = PCT.OrganisationCode
		
	--left join PTL.vwCancelledNewApptCount PatCancellations
	--	on Referral.SourceUniqueID = PatCancellations.ReferralSourceUniqueID
	--	and PatCancellations.CancelledBy = 'P'
		
	--left join PTL.vwCancelledNewApptCount HospCancellations
	--	on Referral.SourceUniqueID = HospCancellations.ReferralSourceUniqueID
	--	and HospCancellations.CancelledBy = 'H'
		
	left join PTL.vwCancelledApptCount PatCancellations
		on Referral.SourceUniqueID = PatCancellations.ReferralSourceUniqueID
		and PatCancellations.CancelledBy = 'P'
		and PatCancellations.FirstAttendanceFlag = 9268
		
	left join PTL.vwCancelledApptCount HospCancellations
		on Referral.SourceUniqueID = HospCancellations.ReferralSourceUniqueID
		and HospCancellations.CancelledBy = 'H'
		and HospCancellations.FirstAttendanceFlag = 9268	
	
	left join
		(select 
			ReferralSourceUniqueID
			,COUNT(ReferralSourceUniqueID) as DNACount
		from (
			select 
				Appt.ReferralSourceUniqueID
			from OP.Encounter Appt
			where Appt.AppointmentDate <= GETDATE()
			and Appt.FirstAttendanceFlag = 9268 --new
			and Appt.DNACode = '3' --DNA
			)NewApptDNA
		group by ReferralSourceUniqueID
		)NewApptDNACount
		on Referral.SourceUniqueID = NewApptDNACount.ReferralSourceUniqueID
	
	--DNA reset not required PM 08/11/2012
	--left join
	--	(select 
	--		Appt.ReferralSourceUniqueID
	--		,FirstApptDNADate = Appt.AppointmentDate
	--	from OP.Encounter Appt
	--	where Appt.DNACode = '3' --DNA
	--	and not exists (
	--		select 1 from OP.Encounter PreviousAppt
	--		where PreviousAppt.ReferralSourceUniqueID = Appt.ReferralSourceUniqueID
	--		and PreviousAppt.SourceUniqueID < Appt.SourceUniqueID
	--		)
	--	)FirstApptDNA
	--	on Referral.SourceUniqueID = FirstApptDNA.ReferralSourceUniqueID

	--Dependency on Lorenzo DB data. Need to run PTL.GetOPRescheduleCount first
	left join PTL.AppointmentRescheduleCount RescheduledAppts
		on ActiveAppointment.SourceUniqueID = RescheduledAppts.APPT_REF

-----------Sched_5------------
	left join PAS.ReferenceValue Sex 
		on Referral.SexCode = Sex.ReferenceValueCode
		
	left join PAS.ReferenceValue EthnicGroup 
		on Referral.EthnicOriginCode = EthnicGroup.ReferenceValueCode 
		
	left join PAS.ReferenceValue RTTCurrentStatus 
		on Referral.RTTCurrentStatusCode = RTTCurrentStatus.ReferenceValueCode

	left join PAS.Consultant RefByConsultant 
		on Referral.ReferredByConsultantCode = RefByConsultant.ConsultantCode
	left join PAS.Organisation ReferringPractice --where referrer is GP
		on Referral.ReferredByOrganisationCode = ReferringPractice.OrganisationCode
	
	left join OrganisationCCG.dbo.Practice ODSReferringPractice --where referrer is GP
		on ReferringPractice.OrganisationLocalCode = ODSReferringPractice.OrganisationCode
		and isnull(ODSReferringPractice.CloseDate, '20130401') > '20130331' 
	
	left join OrganisationCCG.dbo.CCG ODSReferringCCG --where referrer is GP
		on ODSReferringPractice.ParentOrganisationCode = ODSReferringCCG.OrganisationCode
		
	------------	
	left join PAS.Consultant EpisodicGp 
		on Referral.EpisodicGpCode = EpisodicGp.ConsultantCode	
		
	left join PAS.Organisation EpisodicPractice
		on Referral.EpisodicGpPracticeCode = EpisodicPractice.OrganisationCode
		
	left join OrganisationCCG.dbo.Practice ODSEpisodicPractice 
		on EpisodicPractice.OrganisationLocalCode = ODSEpisodicPractice.OrganisationCode
		
	----------
	left join PAS.Consultant RegisteredGp 
		on Referral.RegisteredGpCode = RegisteredGp.ConsultantCode
		
	left join PAS.Organisation RegisteredPractice
		on Referral.RegisteredGpPracticeCode = RegisteredPractice.OrganisationCode

	left join PAS.Organisation RegisteredCCG
		on RegisteredPractice.ParentOrganisationCode = RegisteredCCG.OrganisationCode
	--left outer join WHOLAP.dbo.SpecialtyDivisionBase RefToSpec
	--	on Encounter.SpecialtyCode = RefToSpec.SpecialtyCode
		
--Need to check how RF.Encounter excludes test patients - then remove this where clause
where isnull(PatientSurname, '') not like 'Xxtest%'


--Update CCG
update PTL.OPWL 
	set Contract_ReferringCCG = ccg.Organisation
from PTL.OPWL opwl
left join OrganisationCCG.dbo.CCG ccg
	on ccg.Organisationcode = opwl.Contract_ReferringCCGCode


--Update details of last DNA's or patient cancelled appointment
--update PTL.OPWL set Contract_LastDNAPatCancDate = null
update PTL.OPWL 
	set Contract_LastDNAPatCancDate = canc.LastApptDate
from PTL.OPWL opwl
left join 
(
	select 
		SourcePatientNo
		,ReferralSourceUniqueID
		,ApptCount =  COUNT(1)  
		,LastApptDate = MAX(AppointmentDate)  
	from (
		select 
			Appt.SourcePatientNo
			,Appt.ReferralSourceUniqueID
			,Appt.AppointmentDate
		from OP.Encounter Appt
		left join PTL.OPLookups CancelledReasonLu 
			on Appt.ScheduledCancelReasonCode = CancelledReasonLu.ReferenceValueCode
		where Appt.AppointmentDate <= GETDATE()
		and Appt.FirstAttendanceFlag = 9268 --new appointment
		and 
		(
			(Appt.DNACode = '3') --DNA
				or
			(Appt.AppointmentCancelDate is not null --Patient cancellation
			and CancelledReasonLu.MappedCode = 'P')
		)

	)DNAPatCanc
	group by SourcePatientNo, ReferralSourceUniqueID
)canc
	on canc.ReferralSourceUniqueID = opwl.ReferralSourceUniqueID
	
--Update clinic site
update PTL.OPWL 
	set Contract_Site  = 
		case 
			when CRMLocation.ChronosAreaCode in (1, 3)--WCH, Buccleuch Lodge
			then 'RM201'
			when CRMLocation.ChronosAreaCode = 2 --Wythenshawe
			then 'RM202'
			when CRMLocation.ChronosAreaCode is not null
			then 'RM200'
		end
from PTL.OPWL opwl
left join PAS.ServicePointBase spont
	on spont.code = opwl.CurrentClinic
	and spont.ARCHV_FLAG = 'N'
	and spont.END_DTTM is null
left join Lorenzo.dbo.Organisation ClinicLocation 
	ON spont.HEORG_REFNO = ClinicLocation.HEORG_REFNO
left join CRM.ChronosLocations CRMLocation
	on CRMLocation.LocationCode collate database_default = ClinicLocation.MAIN_IDENT collate database_default


