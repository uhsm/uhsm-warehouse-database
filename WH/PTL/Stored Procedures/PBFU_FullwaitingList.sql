﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		SSRS report: PBFU_FullwaitingList

Notes:			Stored in PTL

Versions:		
				1.0.0.1 - 08/12/2015 - MT
					Change InviteDate parameter from multiple list to
					a From and To.

				1.0.0.0 - 07/12/2015 - MT
					Created sproc.  Previously just code within the SSRS report.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure PTL.PBFU_FullwaitingList
	@Specialty varchar(Max),
	@Consultant varchar(Max),
	@BookingStatus varchar(Max),
	@InviteStatus varchar(Max),
	@StartInviteDate datetime,
	@EndInviteDate datetime,
	@Stage varchar(Max)
As

Select	src.*

From	PTL.ReportFUPB src

		Inner Join DW_REPORTING.LIB.fn_SplitString(@Specialty,',') sp
			On src.WLSpecialty = sp.SplitValue
			
		Inner Join DW_REPORTING.LIB.fn_SplitString(@Consultant,',') con
			On src.Consultant = con.SplitValue
			
		Inner Join DW_REPORTING.LIB.fn_SplitString(@BookingStatus,',') bst
			On src.BookingStatus = bst.SplitValue
			
		Inner Join DW_REPORTING.LIB.fn_SplitString(@InviteStatus,',') ist
			On src.InviteStatus = ist.SplitValue
			
		Inner Join DW_REPORTING.LIB.fn_SplitString(@Stage,',') stg
			On src.Stage = stg.SplitValue

Where	src.InviteDate Between @StartInviteDate And @EndInviteDate

Order By src.PatientSurname
