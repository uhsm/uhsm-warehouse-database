﻿CREATE PROCEDURE [PTL].[UpdateIPWLDiagnosticFlag] AS

--copied from INFOSQL  [Information_Reporting].[dbo].[uspAddDiagnosticFlagToIPWL]

update PTL.IPWL
set IPWLDiagnosticProcedure = B.DiagnosticProcedure
from PTL.IPWL A Inner join
(
select
a.SourceUniqueID,
a.PlannedProcedure,
case when a.PlannedProcedure is null then 
	'NA'
else
	case when b.code is null then
		'N'
	else
		'Y'
	end 
end as DiagnosticProcedure,
b.*
from PTL.IPWL a 
left join PTL.RTTAllDiagnosticCodes b on 
case when PlannedProcedure is null then
	null
else
	case when len
		(
		case when charindex(':', PlannedProcedure) >0 then
			left(PlannedProcedure, charindex(':', PlannedProcedure)-1)
		else
			left(PlannedProcedure, charindex(' ', PlannedProcedure)-1)
		end
		) = 3 then
			(
			case when charindex(':', PlannedProcedure) >0 then
				left(PlannedProcedure, charindex(':', PlannedProcedure)-1)
			else
				left(PlannedProcedure, charindex(' ', PlannedProcedure)-1)
			end
			) + '1'
		else
			(
			case when charindex(':', PlannedProcedure) >0 then
				left(PlannedProcedure, charindex(':', PlannedProcedure)-1)
			else
				left(PlannedProcedure, charindex(' ', PlannedProcedure)-1)
			end
			)
	end
end = b.code
--where 
--a.data_source = 'ipm'
) b on a.SourceUniqueID = b.SourceUniqueID
--where a.data_source = 'ipm'






