﻿CREATE procedure [PTL].[UpdateIPWLSchedule6] as

/*
--Author: K Oakden
--Date created: 30/04/2014
Additions to IPWL required for Schedule 6
*/

update PTL.IPWL set 
	ReferralUBRN = case when left(ipwl.PPID, 3) <> 'RM2' then ipwl.PPID end
	,EthnicGroupNHSCode = case when EthnicGroup.MappedCode = '99' then null else EthnicGroup.MappedCode end
	,SexNHSCode = coalesce(Sex.MappedCode, 0)
	,WaitingListPriorityNHSCode = ListPriority.MappedCode
	,ReferralPriorityNHSCode = ReferralPriority.MappedCode
	,SourceOfReferralNHSCode = ReferralSource.MappedCode
from ptl.IPWL ipwl
left join RF.Encounter referral
	on referral.SourceUniqueID = ipwl.ReferralSourceUniqueID
left join Lorenzo.dbo.WaitingList WaitingList 
	on WaitingList.WLIST_REFNO = ipwl.SourceUniqueID
left join PAS.ReferenceValue ReferralPriority 
	on Referral.PriorityCode = ReferralPriority.ReferenceValueCode
left join PAS.ReferenceValue Sex 
	on Referral.SexCode = Sex.ReferenceValueCode
left join PAS.ReferenceValue EthnicGroup 
	on Referral.EthnicOriginCode = EthnicGroup.ReferenceValueCode 
left join PAS.ReferenceValue ReferralSource
	on Referral.SourceOfReferralCode = ReferralSource.ReferenceValueCode
left join PAS.ReferenceValue ListPriority
	on WaitingList.PRITY_REFNO = ListPriority.ReferenceValueCode 
	
	--select COUNT(1) from ptl.IPWL

--update PTL.IPWL set Contract_LastDNAPatCancDate = null
update PTL.IPWL 
	set LastDNAPatCancelledDate = canc.LastTCIDate
from PTL.IPWL ipwl
left join 
(
	select 
		WaitingListID
		,CancelledOfferCount =  COUNT(1)  
		,LastTCIDate = MAX(TCIDate)  
	from (
		select 
			SourceUniqueID = Offer.ADMOF_REFNO
			,WaitingListID = Offer.WLIST_REFNO
			,OfferOutcomeCode = OfferOutcome.MAIN_CODE
			,OfferOutcomeDescription = OfferOutcome.[DESCRIPTION]
			,OfferOutcomeDate = Offer.OFOCM_DTTM
			,TCIDate = Offer.TCI_DTTM
		from Lorenzo.dbo.AdmissionOffer offer
		inner join ptl.IPWL ipwl
			on ipwl.SourceUniqueID = offer.WLIST_REFNO
		left join Lorenzo.dbo.ReferenceValue OfferOutcome
			on offer.OFOCM_REFNO = OfferOutcome.RFVAL_REFNO 
			and OfferOutcome.ARCHV_FLAG = 'N'
		where offer.ARCHV_FLAG = 'N' 
		and OFOCM_REFNO in (
			2003599,	--DEATH	Cancelled due to Patient Death
			2000657,	--PCAN	Admission Cancelled by or on behalf of Patient
			2003600		--DNA	DNA (Did Not Attend)
			)

	)DNAPatCanc
	group by WaitingListID
) canc
	on canc.WaitingListID = ipwl.SourceUniqueID