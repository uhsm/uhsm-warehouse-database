﻿
CREATE  PROCEDURE [PTL].[UpdateIPWLSuspensions] as

/*
--------------------------------------------------------------------------------
SET SUSPENSION FIELDS TO NULL
--------------------------------------------------------------------------------
*/

update PTL.IPWL
set 
	SuspToDate = NULL,
	SuspensionPeriod = NULL,
	SuspToDatePtReason = NULL,
	SuspensionPeriodPtReason = NULL,
	SuspPatEndDate = NULL,
	CurrentSuspension = NULL,
	CurrentSuspensionType = NULL

/*
--------------------------------------------------------------------------------
UPDATE SUSPENSION DETAILS
--------------------------------------------------------------------------------
*/
update PTL.IPWL
set
	SuspToDate = suspsumm.S_TO_DATE,
	SuspensionPeriod = suspsumm.ALL_SUSP,
	SuspToDatePtReason = suspsumm.PT_REASN,
	SuspensionPeriodPtReason = suspsumm.ALL_PT_REASON
from PTL.IPWL ipwl
inner join 
	(
	select 
		WLIST_REFNO
		,ALL_SUSP = SUM(ALL_SUSP)
		,S_TO_DATE = SUM(S_TO_DATE) 
		,PT_REASN = SUM(PT_REASN)
		,ALL_PT_REASON = SUM(ALL_PT_REASON) 
	from
		(
		select
			susp.WLIST_REFNO
			,susp.SUSRS_REFNO
			,ALL_SUSP = SUM(susp.SUSPENSION_PERIOD)
			,S_TO_DATE = SUM(susp.SUSP_TO_DATE) 
			,PT_REASN = case 
				when susp.susrs_refno in (4398,4397) 
				then sum(susp.RTT_susp_to_date)
				else 0
			end 
			,ALL_PT_REASON = case 
				when susp.SUSRS_REFNO IN (4398,4397) 
				then sum(susp.RTT_SUSPENSION_PERIOD)
				else 0
			end 
		from PTL.IPWLSuspensions susp 
		inner join PTL.IPWL ipwl 
		on susp.WLIST_REFNO = ipwl.SourceUniqueID
		group by susp.WLIST_REFNO, susp.SUSRS_REFNO
		) 
		susptotals
	group by susptotals.WLIST_REFNO
)suspsumm 
on ipwl.SourceUniqueID = suspsumm.WLIST_REFNO



/*
--------------------------------------------------------------------------------
UPDATE MOST RECENT SUSPENSION END DATE WHERE THE REASON IS PATIENT SUSPENSION
--------------------------------------------------------------------------------
*/
update PTL.IPWL
	set SuspPatEndDate = lastsusp.LATESTPATSUSPEND
from PTL.IPWL ipwl 
inner join
	(
	select 
		WLIST_REFNO
		,LATESTPATSUSPEND = MAX(END_DTTM)
	from PTL.IPWLSuspensions  
	where SUSRS_REFNO in ('4397','4398') 
	and CAST(START_DTTM AS DATETIME) < GETDATE()
	group by WLIST_REFNO
	) lastsusp 
on ipwl.SourceUniqueID = lastsusp.WLIST_REFNO


/*
--------------------------------------------------------------------------------
UPDATE CURRENT SUSPENSION FLAGS
--------------------------------------------------------------------------------
*/

update PTL.IPWL set
	SuspendedStartDate = susp.START_DTTM
	,SuspendedEndDate = susp.END_DTTM
	,CurrentSuspension =
		case 
			when susp.CURRENT_SUSPENSION = 'Y' 
			then 'Y'
			else 'N'
		end
	,CurrentSuspensionType =
		case 
			when susp.CURRENT_SUSPENSION = 'Y' 
			then
				case 
					when left(susp.SUSRS_REFNO_MAIN_CODE,2) = 'PT' 
					then 'P'
					else 'M'
				end
			else null
		end 
from PTL.IPWL ipwl 
left join PTL.IPWLSuspensions susp
	on ipwl.SourceUniqueID = susp.WLIST_REFNO
	and susp.CURRENT_SUSPENSION = 'Y'
	and not exists (
		select 1 from PTL.IPWLSuspensions lastsusp
		where lastsusp.WLIST_REFNO = susp.WLIST_REFNO
		and lastsusp.end_DTTM > susp.end_DTTM
		and lastsusp.CURRENT_SUSPENSION = 'Y'
		)



