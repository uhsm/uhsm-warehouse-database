﻿CREATE  PROCEDURE [PTL].[UpdateIPWLWithPreOPDetails] AS

/* 
08/08/2013
KO recreate IPWL logic from InfoSQL

Copy of INFORMATION_VER2.uspUpdateIPWLWithPreOPDetails
*/
update PTL.IPWL
set 
	PreOpDate = NULL,
	PreOpClinic = NULL,
	PreOpClinicDescription = NULL
where PreOpClinicDescription IS NOT NULL;



--INSERT THOSE WHERE THERE IS A LINK TO THE ADMISSION OFFER
update PTL.IPWL
set 
	PreOpDate = offer.PREOP_DATE,
	PreOpClinic = offer.PREOP_CLINIC,
	PreOpClinicDescription = offer.PREOP_CLINIC_DESC
FROM PTL.IPWL ipwl
inner join
(
	select
		ScheduleEvent.ADMOF_REFNO
		,ScheduleEvent.SCHDL_REFNO
		,ScheduleEvent.REFRL_REFNO
		,PREOP_DATE = cast(ScheduleEvent.START_DTTM as datetime)  
		,WLIST_REF = cast(AdmissionOffer.WLIST_REFNO as varchar(20)) 
		,PREOP_CLINIC = ServicePoint.CODE 
		,PREOP_CLINIC_DESC = ServicePoint.[DESCRIPTION] 
	from Lorenzo.dbo.ScheduleEvent ScheduleEvent
	inner join Lorenzo.dbo.AdmissionOffer AdmissionOffer 
		on ScheduleEvent.ADMOF_REFNO = AdmissionOffer.ADMOF_REFNO
	inner join (
		select 
			LastOffer = max(ADMOF_REFNO)
		from Lorenzo.dbo.AdmissionOffer  
		where ARCHV_FLAG = 'N' 
		group by WLIST_REFNO
		) latestoffer 
		on AdmissionOffer.ADMOF_REFNO = latestoffer.LastOffer
	left join Lorenzo.dbo.ServicePoint ServicePoint 
		ON ScheduleEvent.SPONT_REFNO = ServicePoint.SPONT_REFNO
	WHERE ScheduleEvent.ARCHV_FLAG = 'N' 
	AND AdmissionOffer.ARCHV_FLAG = 'N'
) offer 
on ipwl.SourceUniqueID = offer.WLIST_REF;



select
	ipwl.DistrictNo,
	ipwl.WaitingStartDate,
	ipwl.TCIDate,
	OP.REFRL_REFNO,
	OP.WLIST_REFNO,
	OP.SCHDL_REFNO,
	OP.PreOpDate,
	OP.CLINIC_CODE,
	OP.CANCR_DTTM,
	OP.APPT_STATUS,
	OP.APPT_OUTCOME
into #PreOp
from PTL.IPWL ipwl
left join 
	(select
		appt.refrl_refno,
		appt.wlist_refno,
		appt.schdl_refno,
		appt.start_dttm AS PreOpDate,
		clinic_code = spont.CODE,
		appt.cancr_dttm,
		appt_status = stat.[DESCRIPTION],
		appt_outcome = outc.[DESCRIPTION]
	from Lorenzo.dbo.ScheduleEvent appt
	left join Lorenzo.dbo.ServicePoint spont 
		on appt.spont_refno = spont.spont_refno
	left join Lorenzo.dbo.ReferenceValue stat 
		on appt.scocm_refno = stat.rfval_refno
	left join Lorenzo.dbo.ReferenceValue outc 
		on appt.attnd_refno = outc.rfval_refno
	where appt.archv_flag = 'N' 
	and appt.sctyp_refno = '1470'
	)op
	on ipwl.SourceUniqueID = op.WLIST_REFNO
where ipwl.EntryStatus = 'waiting'
and (op.cancr_dttm is null or op.appt_outcome = 'Attended On Time');


select distinct
	P.wlist_refno,
	COALESCE(NextPr.PreOpDate,PrevPr.PreOpDate) AS PreOpDate,
	COALESCE(NextPr.Clinic_Code,PrevPr.Clinic_Code) AS PreOpClinic
into #PreOpDetails
from 
	(select distinct WLIST_REFNO from #PreOp 
	)P
left join 
	(select
		op.wlist_refno,
		schdl_refno,
		PreOpDate,
		clinic_code
	from #PreOp OP
	Inner Join
		(select 
			wlist_refno
			,NextPreOp = min(PreOpDate) 
		from #Preop
		Where PreOpDate >= cast(convert(varchar(8),getdate(),112) as datetime)
		group by wlist_refno
		) NextAppt
	on op.wlist_refno = NextAppt.wlist_refno 
	and OP.PreOpDate = NextAppt.NextPreOp
	) NextPr On P.wlist_refno = NextPr.wlist_refno

left join
	(
	select
		op.wlist_refno,
		schdl_refno,
		PreOpDate,
		clinic_code
	from #PreOp OP
	inner join
		(select 
			wlist_refno
			,LastPreOp = max(PreOpDate) 
		from #Preop
		Where PreOpDate < cast(convert(varchar(8),getdate(),112) as datetime)
		group by wlist_refno
		) LastAppt
	on op.wlist_refno = LastAppt.wlist_refno 
	and op.PreOpDate = LastAppt.LastPreOp
	) PrevPr 
on P.wlist_refno = PrevPr.wlist_refno
Where NextPr.wlist_refno is not null or Prevpr.wlist_refno is not null;



update PTL.IPWL 
set 
	PreOpClinic = Preop.PreopClinic
	,PreOpDate = Preop.PreOpDate
	,PreOpClinicDescription = PreOp.Clinic_name
From PTL.IPWL ipwl 
inner join
	(
	select 
		predet.*
		,clinic_name = clin.NAME 
	from #PreopDetails predet 
	left join Lorenzo.dbo.ServicePoint clin
	on predet.preopclinic = clin.CODE
	--left outer join Information_reporting.dbo.lk_clinic b 
	--on a.preopclinic = b.Clinic_Code
	) PreOp
on ipwl.SourceUniqueID = preop.wlist_refno
where ipwl.PreOpDate IS NULL;


--select * from #PreopDetails;
--select * from #PreOP;

drop table #PreopDetails;
drop Table #PreOP;

