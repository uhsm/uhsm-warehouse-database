﻿CREATE TABLE [PTL].[ActiveWaitingList] (
    [WaitingListRuleRef]  INT           NOT NULL,
    [WaitingListRuleCode] VARCHAR (20)  NULL,
    [WaitingListRuleName] VARCHAR (80)  NULL,
    [TotalActiveOnList]   INT           NULL,
    [MinDateOnList]       SMALLDATETIME NULL,
    [WLSvcType]           VARCHAR (20)  NULL,
    [WLVisitType]         VARCHAR (20)  NULL,
    [WLPatientSurname]    VARCHAR (30)  NULL,
    [WLDistrictNo]        VARCHAR (10)  NULL,
    [WLClinician]         VARCHAR (30)  NULL,
    [WLClinicianCode]     VARCHAR (30)  NULL,
    [WLSpecialty]         VARCHAR (30)  NULL,
    [WLSpecialtyCode]     VARCHAR (30)  NULL,
    [WLDateOnList]        SMALLDATETIME NULL,
    [WLSourceUniqueID]    INT           NOT NULL,
    [WLSourceReferralID]  INT           NULL,
    [WLSourcePatientID]   INT           NULL,
    CONSTRAINT [PK_ActiveWaitingList] PRIMARY KEY CLUSTERED ([WLSourceUniqueID] ASC)
);

