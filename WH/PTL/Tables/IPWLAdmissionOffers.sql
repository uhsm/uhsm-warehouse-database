﻿CREATE TABLE [PTL].[IPWLAdmissionOffers] (
    [SourcePatientNo]           INT           NULL,
    [SourceUniqueID]            INT           NOT NULL,
    [WaitingListID]             NUMERIC (18)  NULL,
    [OfferOutcomeCode]          VARCHAR (25)  NULL,
    [OfferOutcomeDescription]   VARCHAR (80)  NULL,
    [OfferDate]                 SMALLDATETIME NULL,
    [OfferOutcomeDate]          SMALLDATETIME NULL,
    [TCIDate]                   SMALLDATETIME NULL,
    [CancellationDate]          SMALLDATETIME NULL,
    [ConfirmationDate]          SMALLDATETIME NULL,
    [DeferralFlag]              CHAR (1)      NULL,
    [DeferralDate]              SMALLDATETIME NULL,
    [DeferralReasonCode]        VARCHAR (25)  NULL,
    [DeferralReasonDescription] VARCHAR (80)  NULL,
    [DateRecordCreated]         DATETIME      NULL,
    [DateRecordModified]        DATETIME      NULL,
    [LatestOffer]               VARCHAR (1)   NULL
);

