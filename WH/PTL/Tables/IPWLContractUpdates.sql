﻿CREATE TABLE [PTL].[IPWLContractUpdates] (
    [SourceUniqueID]                INT           NOT NULL,
    [ReferralSourceUniqueID]        INT           NULL,
    [Contract_PPID]                 VARCHAR (300) NULL,
    [Contract_ReferringCCG]         VARCHAR (5)   NULL,
    [Contract_Ethnicity]            VARCHAR (50)  NULL,
    [Contract_Sex]                  INT           NULL,
    [Contract_WaitingListPriority]  VARCHAR (50)  NULL,
    [Contract_ReferralPriority]     VARCHAR (50)  NULL,
    [Contract_SourceOfReferralCode] VARCHAR (50)  NULL,
    [Contract_LastDNAPatCancDate]   DATETIME      NULL
);

