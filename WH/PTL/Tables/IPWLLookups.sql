﻿CREATE TABLE [PTL].[IPWLLookups] (
    [ReferenceValueCode]  INT          NOT NULL,
    [ReferenceValue]      VARCHAR (80) COLLATE Latin1_General_CI_AS NULL,
    [MappedCode]          CHAR (20)    COLLATE Latin1_General_CI_AS NULL,
    [ReferenceDomainCode] VARCHAR (5)  COLLATE Latin1_General_CI_AS NULL,
    [LookupField]         VARCHAR (18) NOT NULL,
    CONSTRAINT [PK_IPWLLookups] PRIMARY KEY CLUSTERED ([ReferenceValueCode] ASC)
);

