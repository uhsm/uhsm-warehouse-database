﻿CREATE TABLE [PTL].[IPWLStartDates] (
    [WLIST_REFNO]      INT           NOT NULL,
    [WLIST_DTTM]       SMALLDATETIME NULL,
    [ResetDate]        SMALLDATETIME NULL,
    [WaitingStartDate] DATETIME      NULL
);

