﻿CREATE TABLE [PTL].[IPWLSuspensions] (
    [WLIST_REFNO]             NUMERIC (18)  NULL,
    [WSUSP_REFNO]             NUMERIC (18)  NOT NULL,
    [SUSRS_REFNO]             NUMERIC (18)  NULL,
    [SUSRS_REFNO_MAIN_CODE]   VARCHAR (25)  NULL,
    [SUSRS_REFNO_DESCRIPTION] VARCHAR (80)  NULL,
    [START_DTTM]              DATETIME      NULL,
    [end_DTTM]                DATETIME      NULL,
    [SUSP_TO_DATE]            INT           NULL,
    [SUSPENSION_PERIOD]       INT           NULL,
    [CURRENT_SUSPENSION]      VARCHAR (1)   NOT NULL,
    [RTT_SUSP_TO_DATE]        INT           NULL,
    [RTT_SUSPENSION_PERIOD]   INT           NULL,
    [COMMENTS]                VARCHAR (255) NULL
);

