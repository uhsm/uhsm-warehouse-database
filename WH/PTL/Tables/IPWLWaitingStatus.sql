﻿CREATE TABLE [PTL].[IPWLWaitingStatus] (
    [SourceUniqueID]        INT          NOT NULL,
    [RemovalReasonCode]     INT          NULL,
    [RemovalReason]         VARCHAR (80) COLLATE Latin1_General_CI_AS NULL,
    [WLRemovalStatus]       CHAR (20)    COLLATE Latin1_General_CI_AS NULL,
    [LastTCIOfferRef]       INT          NULL,
    [OfferOutcome]          VARCHAR (80) COLLATE Latin1_General_CI_AS NULL,
    [OfferRemovalStatus]    CHAR (20)    COLLATE Latin1_General_CI_AS NULL,
    [LastTCIOffer]          DATETIME     NULL,
    [LastPCEWaitingListRef] NUMERIC (18) NULL,
    [LastPCEStartDate]      VARCHAR (16) NULL,
    [LastPCEEndDate]        VARCHAR (16) NULL,
    [LastPCEOfferRef]       INT          NULL,
    [PCEOutcome]            VARCHAR (80) COLLATE Latin1_General_CI_AS NULL,
    [PCERemovalStatus]      CHAR (20)    COLLATE Latin1_General_CI_AS NULL,
    [EntryStatus]           VARCHAR (20) COLLATE Latin1_General_CI_AS NULL
);

