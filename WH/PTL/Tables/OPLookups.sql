﻿CREATE TABLE [PTL].[OPLookups] (
    [ReferenceValueCode]  INT          NOT NULL,
    [ReferenceValue]      VARCHAR (80) NULL,
    [MappedCode]          VARCHAR (50) NULL,
    [ReferenceDomainCode] VARCHAR (5)  NULL,
    [LookupField]         VARCHAR (50) NULL,
    CONSTRAINT [PK_OPLookups] PRIMARY KEY CLUSTERED ([ReferenceValueCode] ASC)
);

