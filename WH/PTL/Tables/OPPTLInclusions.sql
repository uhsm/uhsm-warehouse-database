﻿CREATE TABLE [PTL].[OPPTLInclusions] (
    [ReferralSourceUniqueID]      INT          NOT NULL,
    [CABAdjustedReferralDate]     DATETIME     NULL,
    [CABAdjustedReferralDateFlag] VARCHAR (23) NOT NULL
);

