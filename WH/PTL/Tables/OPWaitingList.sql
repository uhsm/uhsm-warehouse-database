﻿CREATE TABLE [PTL].[OPWaitingList] (
    [AppointmentDate]        SMALLDATETIME NULL,
    [ClinicCode]             VARCHAR (25)  NULL,
    [ConsultantCode]         VARCHAR (25)  NULL,
    [ConsultantSurname]      VARCHAR (30)  NULL,
    [ReferralDate]           SMALLDATETIME NULL,
    [VisitType]              VARCHAR (80)  NULL,
    [DaysWaitToAppt]         INT           NULL,
    [WLPriority]             VARCHAR (80)  NULL,
    [WLRefNo]                INT           NOT NULL,
    [WLSpecialty]            VARCHAR (255) NULL,
    [WLSpecialtyCode]        VARCHAR (20)  NULL,
    [DistrictNo]             VARCHAR (20)  NULL,
    [PatientSurname]         VARCHAR (30)  NULL,
    [WaitingStartDate]       DATETIME      NULL,
    [NextReset]              DATETIME      NULL,
    [CancerBreachDate]       DATETIME      NULL,
    [ReferralSourceUniqueID] INT           NULL,
    [DateOnList]             SMALLDATETIME NULL,
    [CensusDate]             DATETIME      NULL
);

