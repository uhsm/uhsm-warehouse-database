﻿CREATE TABLE [PTL].[OPWaitingListResets] (
    [wlist_refno]   NUMERIC (18) NOT NULL,
    [LastResetDate] DATETIME     NULL,
    [NextResetDate] DATETIME     NULL,
    CONSTRAINT [PK_OPWaitingListResets] PRIMARY KEY CLUSTERED ([wlist_refno] ASC)
);

