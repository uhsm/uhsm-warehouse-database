﻿CREATE TABLE [PTL].[ORMISWaitingListBooking] (
    [WLIST_REFNO]         INT          NOT NULL,
    [DateOnList]          DATETIME     NULL,
    [WaitingListStatus]   VARCHAR (20) NULL,
    [OrmisWaitingListRef] VARCHAR (40) NULL,
    [OrmisBookingSeq]     NUMERIC (9)  NULL,
    [OrmisWaitingListSeq] NUMERIC (9)  NULL,
    [OrmisPatientID]      VARCHAR (12) NULL,
    [OrmisDateOnList]     DATETIME     NULL,
    [OrmisOperationDate]  DATETIME     NULL,
    [OrmisSessionNo]      NUMERIC (3)  NULL,
    [OrmisBookingStatus]  VARCHAR (11) NOT NULL,
    [Theatre]             VARCHAR (60) NULL,
    [Unplanned]           NUMERIC (3)  NULL,
    [OrmisBookingType]    VARCHAR (1)  NULL,
    CONSTRAINT [PK_ORMISWaitingListBooking] PRIMARY KEY CLUSTERED ([WLIST_REFNO] ASC)
);

