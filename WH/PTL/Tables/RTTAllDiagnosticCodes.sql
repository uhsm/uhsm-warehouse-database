﻿CREATE TABLE [PTL].[RTTAllDiagnosticCodes] (
    [Code]              NVARCHAR (255) NULL,
    [TYPE]              NVARCHAR (255) NULL,
    [Desc]              NVARCHAR (255) NULL,
    [On original list?] NVARCHAR (255) NULL
);

