﻿CREATE TABLE [PTL].[ReportFUPB] (
    [WLSpecialty]                                VARCHAR (255)  NULL,
    [Consultant]                                 VARCHAR (30)   NULL,
    [PatientID]                                  VARCHAR (43)   NOT NULL,
    [PatientSurname]                             VARCHAR (30)   NULL,
    [ReferralSourceUniqueID]                     INT            NOT NULL,
    [ReferralDate]                               VARCHAR (11)   NULL,
    [WaitingStartDate]                           VARCHAR (11)   NULL,
    [SourceWaitingListID]                        INT            NOT NULL,
    [SpecialtyCode]                              VARCHAR (50)   NULL,
    [Specialty]                                  VARCHAR (255)  NULL,
    [Directorate]                                VARCHAR (255)  NULL,
    [Division]                                   VARCHAR (255)  NULL,
    [WLClinicCode]                               VARCHAR (25)   NULL,
    [ApptClinicCode]                             VARCHAR (25)   NULL,
    [VisitType]                                  VARCHAR (80)   NULL,
    [DaysWait]                                   INT            NULL,
    [WeeksWait]                                  INT            NULL,
    [InviteDate]                                 DATE           NULL,
    [LeadTimeWeeks]                              INT            NULL,
    [ProjectedApptDate]                          VARCHAR (11)   NULL,
    [AppointmentDate]                            VARCHAR (11)   NULL,
    [WLGeneralComment]                           VARCHAR (1200) NULL,
    [WLName]                                     VARCHAR (80)   NULL,
    [ReferralStatus]                             VARCHAR (80)   NULL,
    [CreateDate]                                 DATETIME       NULL,
    [CreatedByUser]                              VARCHAR (35)   NULL,
    [ModifiedDate]                               DATETIME       NULL,
    [ModifiedByUser]                             VARCHAR (35)   NULL,
    [BookingStatus]                              VARCHAR (8)    NOT NULL,
    [InviteStatus]                               VARCHAR (7)    NOT NULL,
    [OverdueWks]                                 INT            NULL,
    [OverdueWksAtAppointment]                    INT            NULL,
    [DateOnList]                                 VARCHAR (11)   NULL,
    [NumberOfFUPatientCancellations]             INT            NULL,
    [NumberOfFUHospitalCancellations]            INT            NULL,
    [NumberOfDNAs]                               INT            NULL,
    [HSC]                                        VARCHAR (3)    NOT NULL,
    [BookByBreachDate]                           DATETIME       NULL,
    [CancerDiagnosis]                            VARCHAR (MAX)  NULL,
    [CancerSite]                                 VARCHAR (MAX)  NULL,
    [NumberPatientCancellationsAfterDateOnList]  INT            NULL,
    [NumberPatientDNAsAfterDateOnList]           INT            NULL,
    [NumberHospitalCancellationsAfterDateOnList] INT            NULL,
    [Stage]                                      VARCHAR (80)   NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ReportFUPB_BookingStatus]
    ON [PTL].[ReportFUPB]([BookingStatus] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ReportFUPB_Consultant]
    ON [PTL].[ReportFUPB]([Consultant] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ReportFUPB_Invite_Status]
    ON [PTL].[ReportFUPB]([InviteStatus] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ReportFUPB_InviteDate]
    ON [PTL].[ReportFUPB]([InviteDate] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ReportFUPB_Stage]
    ON [PTL].[ReportFUPB]([Stage] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ReportFUPB_WLSpecialty]
    ON [PTL].[ReportFUPB]([WLSpecialty] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ReportFUPB_ReferralSourceUniqueID]
    ON [PTL].[ReportFUPB]([ReferralSourceUniqueID] ASC);

