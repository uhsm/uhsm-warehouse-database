﻿CREATE TABLE [PTL].[TempWLRemovals] (
    [SourceUniqueID]         INT           NOT NULL,
    [ReferralSourceUniqueID] INT           NULL,
    [DistrictNo]             VARCHAR (12)  NULL,
    [PatientForename]        VARCHAR (30)  NULL,
    [PatientSurname]         VARCHAR (30)  NULL,
    [ReferralDate]           DATETIME      NULL,
    [DateOnList]             DATETIME      NULL,
    [RemovalDate]            DATETIME      NULL,
    [RemovalReason]          VARCHAR (80)  NULL,
    [EntryStatus]            VARCHAR (20)  NULL,
    [RTTStatusCode]          VARCHAR (25)  NULL,
    [RTTStatusDescription]   VARCHAR (80)  NULL,
    [RTTStatusSource]        VARCHAR (10)  NULL,
    [RTTStatusSourceID]      NUMERIC (18)  NULL,
    [RTTStatusDate]          DATETIME      NULL,
    [COMMENTS]               VARCHAR (255) NULL
);

