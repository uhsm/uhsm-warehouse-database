﻿



CREATE view [PTL].[FUPBRemovals] as
/*
Request by CBH 04/11/2013 for KPMG audit
*/
select 	
	WaitingListID = a.wlist_refno,
	ReferralID = a.refrl_refno,
	WLSpecialty = Specialty.[DESCRIPTION],
	DistrictNo = DistrictNo.IDENTIFIER,
	NHSNumber = Patient.patnt_refno_nhs_identifier, 
	PatientSurname = Patient.surname,
	ConsultantSurname = c.surname,
	DateOnList = a.wlist_dttm,
	WaitingStartDate = a.waiting_start_dttm,
	CreateDate = cast(a.create_dttm as datetime),
	ModifiedDate = cast(a.MODIF_DTTM as datetime),
	Creator = a.user_create,
	Modifier = a.USER_MODIF,
	M.SPONT_REFNO_CODE AS WLClinicCode,
	wlr.NAME AS WLName,
	InviteDate = CAST(a.INVIT_DTTM AS DATETIME),
	VisitType = Visit.[DESCRIPTION],
	DateRemovedFromList = a.remvl_dttm,
	RemovalReason = Removal.[DESCRIPTION]
from Lorenzo.dbo.WaitingList a 
left join Lorenzo.dbo.ProfessionalCarer c on a.proca_refno = c.proca_refno
LEFT JOIN Lorenzo.dbo.ServicePoint M ON A.SPONT_REFNO = M.SPONT_REFNO
left join Lorenzo.dbo.WaitingListRule wlr on A.WLRUL_REFNO = wlr.WLRUL_REFNO
LEFT JOIN Lorenzo.dbo.ReferenceValue Visit ON a.VISIT_REFNO = Visit.RFVAL_REFNO
LEFT JOIN Lorenzo.dbo.ReferenceValue Removal on a.REMVL_REFNO = Removal.RFVAL_REFNO
left join Lorenzo.dbo.Specialty Specialty ON a.spect_refno = Specialty.spect_refno AND Specialty.ARCHV_FLAG = 'N'
left join Lorenzo.dbo.Patient Patient on a.patnt_refno = Patient.patnt_refno
left join Lorenzo.dbo.PatientIdentifier DistrictNo
	on	DistrictNo.PATNT_REFNO = Patient.PATNT_REFNO
	and	DistrictNo.PITYP_REFNO = 2001232 --facility
	and	DistrictNo.IDENTIFIER like 'RM2%'
	and	DistrictNo.ARCHV_FLAG = 'N'
	and	not exists
		(
		select
			1
		from
			Lorenzo.dbo.PatientIdentifier Previous
		where
			Previous.PATNT_REFNO = DistrictNo.PATNT_REFNO
		and	Previous.PITYP_REFNO = DistrictNo.PITYP_REFNO
		and	Previous.IDENTIFIER like 'RM2%'
		and	Previous.ARCHV_FLAG = 'N'
		and	(
				Previous.START_DTTM > DistrictNo.START_DTTM
			or
				(
					Previous.START_DTTM = DistrictNo.START_DTTM
				and	Previous.PATID_REFNO > DistrictNo.PATID_REFNO
				)
			)
		)
where a.remvl_dttm is not null 
and cast(a.remvl_dttm as datetime) > '2013-03-31 00:00:00.000'
and a.archv_flag = 'N' 
and A.svtyp_refno = '4223'
and a.visit_refno in ('8911', '2003436') --follow up


