﻿












CREATE view [PTL].[TLoadActiveWaitingList] as
/*
--	Author:  K Oakden
--	Date: 12/06/2013
--	To show waiting list rules with active waiters
	with summary data and patient level data
--	Changes
	2013-12-04 KO Hard coded specialty code to 370C (150000185 - breast oncology) where ConsultantCode = '10000340' (Prof Howell)
*/

select 
	WaitingListRuleRef = pbwl.WLRUL_REFNO
	,WaitingListRuleCode = pbwl.CODE
	,WaitingListRuleName = pbwl.NAME
	,TotalActiveOnList = pbwl.ActiveOnList
	,pbwl.MinDateOnList
	,WLSvcType = ServiceType.ReferenceValue
	,WLVisitType = Visit.ReferenceValue
	,WLPatientSurname = Patient.surname
	,WLDistrictNo = DistrictNo.IDENTIFIER
	,WLClinician = carer.surname
	,WLClinicianCode = carer.MAIN_IDENT
	--,WLSpecialty = Specialty.[DESCRIPTION]
	--,WLSpecialtyCode = Specialty.MAIN_IDENT
	,WLSpecialty = --Specialty.[DESCRIPTION]
		case 
			when wl.PROCA_REFNO = '10000340' --Prof Howell
			then 'Breast Oncology' 
			else Specialty.[DESCRIPTION]
		end
	,WLSpecialtyCode = --Specialty.MAIN_IDENT
		case 
			when wl.PROCA_REFNO = '10000340' --Prof Howell
			then '370C' 
			else Specialty.MAIN_IDENT
		end
	,WLDateOnList = wl.WLIST_DTTM
	,WLSourceUniqueID = wl.WLIST_REFNO
	,WLSourceReferralID = wl.REFRL_REFNO
	,WLSourcePatientID = wl.PATNT_REFNO

from 
(
	select 
		wlr.WLRUL_REFNO
		,wlr.CREATE_DTTM
		,wlr.MODIF_DTTM
		,wlr.CODE
		,wlr.NAME
		--,Specialty.Specialty
		,ProfessionalCarer.ProfessionalCarerMainCode
		,ProfessionalCarerName = ProfessionalCarer.Surname + ', ' + ProfessionalCarer.Forename
		,RuleActivity.ActiveOnList
		,RuleActivity.MinDateOnList
	from 
	--Lorenzo.dbo.WaitingListRule wlr
	(SELECT [WLRUL_REFNO]
 ,[SPECT_REFNO]
 ,[PROCA_REFNO]
 ,[HEORG_REFNO]
 ,[MWAIT_DAYCASE]
 ,[MWAIT_ORDINARY]
 ,[CREATE_DTTM]
 ,[MODIF_DTTM]
 ,[USER_CREATE]
 ,[USER_MODIF]
 ,[CODE]
 ,[NAME]
 ,[COMMENTS]
 ,[STRAN_REFNO]
 ,[PRIOR_POINTER]
 ,[ARCHV_FLAG]
 ,[EXTERNAL_KEY]
 ,[SVTYP_REFNO]
 ,[SPONT_REFNO]
 ,[LOCAL_FLAG]
 ,[END_DTTM]
 ,[OWNER_HEORG_REFNO]
 ,[Created]
FROM Lorenzo.dbo.WaitingListRule

UNION

SELECT 19999999
 ,NULL
 ,NULL
 ,NULL
 ,NULL
 ,NULL
 ,NULL
 ,NULL
 ,NULL
 ,NULL
 ,'NSP'
 ,'Not Specified'
 ,NULL
 ,NULL
 ,NULL
 ,'N'
 ,NULL
 ,NULL
 ,NULL
 ,NULL
 ,NULL
 ,NULL
 ,NULL) wlr

	left join 
		(select 
			count(active.wlist_refno) as ActiveOnList,
			active.WLRUL_REFNO,
			min(active.wlist_dttm)as MinDateOnList
		from 
			(
			select 	
				wl.wlist_refno,
				WLRUL_REFNO = isnull(wl.WLRUL_REFNO, 19999999),
				wl.wlist_dttm
			from Lorenzo.dbo.WaitingList wl
			where wl.remvl_dttm is null 
			and wl.archv_flag = 'N'
			)active
		group by 
			active.WLRUL_REFNO
		)RuleActivity
	on RuleActivity.WLRUL_REFNO = wlr.WLRUL_REFNO
	
	--left join PAS.Specialty Specialty
	--	on Specialty.SpecialtyCode = wlr.SPECT_REFNO
	
	left join PAS.ProfessionalCarer ProfessionalCarer
		on ProfessionalCarer.ProfessionalCarerCode = wlr.PROCA_REFNO
	


	where wlr.ARCHV_FLAG = 'N'
	and RuleActivity.ActiveOnList is not null
) pbwl

left join Lorenzo.dbo.WaitingList wl
	on isnull(wl.WLRUL_REFNO, 19999999) = pbwl.WLRUL_REFNO
	
left join Lorenzo.dbo.ProfessionalCarer carer 
	on carer.proca_refno = wl.proca_refno

left join Lorenzo.dbo.Specialty Specialty 
	on wl.spect_refno = Specialty.spect_refno 
	and Specialty.ARCHV_FLAG = 'N'
	
left join PAS.ReferenceValue ServiceType
	on ServiceType.ReferenceValueCode = wl.SVTYP_REFNO

left join PAS.ReferenceValue Visit 
	on Visit.ReferenceValueCode = wl.VISIT_REFNO

left join Lorenzo.dbo.Patient Patient 
	on Patient.patnt_refno = wl.PATNT_REFNO

left join Lorenzo.dbo.PatientIdentifier DistrictNo
	on	DistrictNo.PATNT_REFNO = Patient.PATNT_REFNO
	and	DistrictNo.PITYP_REFNO = 2001232 --facility
	and	DistrictNo.IDENTIFIER like 'RM2%'
	and	DistrictNo.ARCHV_FLAG = 'N'
	and	not exists
		(
		select
			1
		from
			Lorenzo.dbo.PatientIdentifier Previous
		where
			Previous.PATNT_REFNO = DistrictNo.PATNT_REFNO
		and	Previous.PITYP_REFNO = DistrictNo.PITYP_REFNO
		and	Previous.IDENTIFIER like 'RM2%'
		and	Previous.ARCHV_FLAG = 'N'
		and	(
				Previous.START_DTTM > DistrictNo.START_DTTM
			or
				(
					Previous.START_DTTM = DistrictNo.START_DTTM
				and	Previous.PATID_REFNO > DistrictNo.PATID_REFNO
				)
			)
		)

where wl.remvl_dttm is null 
and wl.archv_flag = 'N'
and
--remove test patients
	not exists
	(
	select
			1
	from
		Lorenzo.dbo.ExcludedPatient
	where
		ExcludedPatient.SourcePatientNo = wl.PATNT_REFNO
	)

--and DistrictNo.IDENTIFIER = 'RM21185558'
--order by pbwl.WLRUL_REFNO


--select 
--	wlr.* 
--	,Specialty.Specialty
--from Lorenzo.dbo.WaitingListRule wlr
--left join PAS.Specialty Specialty
--on Specialty.SpecialtyCode = wlr.SPECT_REFNO
--where wlr.WLRUL_REFNO = 150000536

--select * from Lorenzo.dbo.Patient where PATNT_REFNO = 12714374













