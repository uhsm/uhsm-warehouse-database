﻿



CREATE view [PTL].[TLoadIPWLBulk] as 
/*
--	Author:  K Oakden
--	Date: 25/07/2013
--	To format WL data from INFOSQL
--Used for testing purposes only
*/
select
	DataSource = DATA_SOURCE
	,SourceUniqueID = WLIST_REFNO
	,SourcePatientNo = case 
			when isnumeric(PATNT_REFNO) = 0
			then null
			else PATNT_REFNO
		end
	,ReferralSourceUniqueID = REF_REF
	,DistrictNo = LOCAL_ID
	,NHSNumber = NHS_NUMBER
	,PatientForename = PATIENT_FORENAME
	,PatientSurname = PATIENT_SURNAME
	,DateOfBirth = DATE_OF_BIRTH
	,SpecialtyCode = SPECIALTY
	,Consultant = CONSULTANT
	,ListType = LIST_TYPE
	,ElectiveAdmissionMethod = ELECTIVE_ADMISSION_METHOD
	,AdminCategory = ADMIN_CAT
	,IntendedManagement = INTENDED_MANAGEMENT
	,Priority = PRIORITY
	,ReferralDate = REF_DATE
	,OriginalDateOnList = ORIGINAL_DATE_ON_LIST
	,DateOnList = DATE_ON_LIST
	,WaitingStartDate = WAITING_START_DATE
	,SuspendedStartDate = SUSPENDED_START_DATE
	,SuspendedEndDate = SUSPENDED_END_DATE
	,SuspToDate = SUSP_TO_DATE
	,SuspensionPeriod = SUSPENSION_PERIOD
	,SuspPatEndDate = SUSP_PAT_END_DATE
	,SuspToDatePtReason = SUSP_TO_DATE_PT_REASON
	,SuspensionPeriodPtReason = SUSPENSION_PERIOD_PT_REASON
	,CurrentSuspension = CURRENT_SUSPENSION
	,CurrentSuspensionType = CURRENT_SUSPENSION_TYPE
	,RemovalDate = REMOVAL_DATE
	,GuaranteedAdmissionDate = GUARANTEED_ADMISSION_DATE
	,AdmissionDate = ADMISSION_DATE
	,PlannedProcedure = PLANNED_PROCEDURE
	,AnaestheticType = ANAESTHETIC_TYPE
	,ServiceType = SERVICE_TYPE
	,ListName = LIST_NAME
	,AdmissionWard = ADMISSION_WARD
	,EstimatedTheatreTime = EST_THEATRE_TIME
	,WhoCanOperate = WHOCO
	,GeneralComment = GENERAL_COMMENT
	,PatientPrepComments = PATIENT_PREP_COMMENTS
	,RemovalReason = REMOVAL_REASON
	,EntryStatus = ENTRY_STATUS COLLATE DATABASE_DEFAULT
	,OfferOutcome = OFFER_OUTCOME
	,PCEOutcome = EPI_OUTCOME
	,TCIDate = TCI_DATE
	,EffectiveGP = EFFECTIVE_GP
	,CancerCode = CANCER_CODE
	,DateRecordCreated = DATE_RECORD_CREATED
	,CreatedByUser = USER_WHO_CREATED_RECORD
	,DateRecordModified = DATE_RECORD_MODIFIED
	,ModifiedByUser = USER_WHO_MODIFIED_RECORD
	,ShortNoticeFlag = SHORT_NOTICE_FLAG
	,AdmitByDate = ADMITBY_DATE
	,NextResetDate = NEXT_RESET_DATE
	,PPID = PPID
	,RTTCode = RTT_Code
	,RTTDescription = RTT_Desc
	,IPWLDiagnosticProcedure = IPWLDiagnosticProcedure
	,Sex = SEX
	,PreOpClinic = PREOP_CLINIC
	,PreOpDate = PREOP_DATE
	,PreOpClinicDescription = PREOP_CLINIC_DESC
	,SecondaryWard = SECONDARY_WARD
from PTL.IPWL_DATASET
where DATA_SOURCE = 'IPM'

























