﻿





/*
--Author: K Oakden
--Date created: 21/09/2013
View used to provide count of OP appointments cancelled by patient or hospital

--Dependencies
PTL.OPLookups
*/





CREATE view [PTL].[vwCancelledApptCount] as (
select 
	SourcePatientNo
	,ReferralSourceUniqueID
	,CancelledBy
	,VisitType
	,FirstAttendanceFlag
	,Count(ReferralSourceUniqueID) as CancellationCount
from (
select
	Appt.SourcePatientNo
	,Appt.ReferralSourceUniqueID
	,Appt.ScheduledCancelReasonCode
	--,CancelledBy = CancelledReasonLu.MappedCode 
	,CancelledBy = 
		case
			when Appt.ScheduledCancelReasonCode in (61, 1210, 2001651, 2004150, 2006150)--Nonspecific
			then 		
				case
					when Appt.CancelledByCode in (5477, 3107255)--Cancelled by patient / patient died
					then 'P'
					else 'H'
				end
			else CancelledReasonLu.MappedCode 
		end
	,CancelledReasonLu.ReferenceValue
	,VisitType = Visit.ReferenceValue
	,Appt.FirstAttendanceFlag
	--,Appt.AppointmentDate
	--,Appt.ClinicCode
	--,Appt.SourceUniqueID
from OP.Encounter Appt
left join PTL.OPLookups CancelledReasonLu 
on Appt.ScheduledCancelReasonCode = CancelledReasonLu.ReferenceValueCode
left join PAS.ReferenceValue Visit
on Visit.ReferenceValueCode = Appt.FirstAttendanceFlag
where Appt.AppointmentCancelDate is not null
and Appt.ReferralSourceUniqueID is not null
--and SourcePatientNo = 11832661
--and Appt.FirstAttendanceFlag = 9268 --new appointment
) CancelledNewAppt

group by SourcePatientNo, ReferralSourceUniqueID, CancelledBy, VisitType, FirstAttendanceFlag
)










