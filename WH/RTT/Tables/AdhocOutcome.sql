﻿CREATE TABLE [RTT].[AdhocOutcome] (
    [ReferralSourceUniqueID] INT         NOT NULL,
    [RTTStatusCode]          VARCHAR (2) NULL,
    [RTTStatusDate]          DATETIME    NULL,
    [Updated]                DATETIME    NULL,
    CONSTRAINT [PK_AdhocOutcome] PRIMARY KEY CLUSTERED ([ReferralSourceUniqueID] ASC)
);

