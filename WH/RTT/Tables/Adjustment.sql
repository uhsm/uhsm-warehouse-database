﻿CREATE TABLE [RTT].[Adjustment] (
    [AdjustmentRecno]      INT            IDENTITY (1, 1) NOT NULL,
    [SourceCode]           VARCHAR (10)   NOT NULL,
    [SourceRecno]          INT            NOT NULL,
    [AdjustmentDays]       INT            NOT NULL,
    [AdjustmentReasonCode] VARCHAR (10)   NULL,
    [AdjustmentComment]    VARCHAR (4000) NULL,
    [Created]              DATETIME       NULL,
    [Updated]              DATETIME       NULL,
    [LastUser]             VARCHAR (50)   NULL,
    CONSTRAINT [PK_Adjustment] PRIMARY KEY CLUSTERED ([AdjustmentRecno] ASC),
    CONSTRAINT [FK_RTTAdjustment_AdjustmentReason] FOREIGN KEY ([AdjustmentReasonCode]) REFERENCES [RTT].[AdjustmentReason] ([AdjustmentReasonCode])
);


GO
CREATE NONCLUSTERED INDEX [IX_Adjustment]
    ON [RTT].[Adjustment]([SourceCode] ASC, [SourceRecno] ASC);

