﻿CREATE TABLE [RTT].[AdjustmentReason] (
    [AdjustmentReasonCode] VARCHAR (10) NOT NULL,
    [AdjustmentReason]     VARCHAR (50) NULL,
    CONSTRAINT [PK_RTTAdjustmentReason] PRIMARY KEY CLUSTERED ([AdjustmentReasonCode] ASC)
);

