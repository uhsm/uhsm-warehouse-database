﻿CREATE TABLE [RTT].[BreachBand] (
    [BreachBandCode] VARCHAR (10) NOT NULL,
    [BreachBand]     VARCHAR (50) NULL,
    CONSTRAINT [PK_BreachBand] PRIMARY KEY CLUSTERED ([BreachBandCode] ASC)
);

