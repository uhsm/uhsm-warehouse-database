﻿CREATE TABLE [RTT].[BreachValidation] (
    [ReferralSourceUniqueID] INT         NOT NULL,
    [RTTStatusCode]          VARCHAR (2) NULL,
    [RTTStatusDate]          DATETIME    NULL,
    [Updated]                DATETIME    NULL,
    CONSTRAINT [PK_BreachValidation] PRIMARY KEY CLUSTERED ([ReferralSourceUniqueID] ASC)
);

