﻿CREATE TABLE [RTT].[ClockStopReason] (
    [ClockStopReasonCode] VARCHAR (10) NOT NULL,
    [ClockStopReason]     VARCHAR (50) NULL,
    CONSTRAINT [PK_RTTClockStopReason] PRIMARY KEY CLUSTERED ([ClockStopReasonCode] ASC)
);

