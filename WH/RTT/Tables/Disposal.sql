﻿CREATE TABLE [RTT].[Disposal] (
    [DisposalCode]      INT          NOT NULL,
    [Disposal]          VARCHAR (80) NULL,
    [DisposalLocalCode] VARCHAR (25) NULL,
    [ClockStopFlag]     BIT          NULL,
    CONSTRAINT [PK_RTT_Disposal] PRIMARY KEY CLUSTERED ([DisposalCode] ASC)
);

