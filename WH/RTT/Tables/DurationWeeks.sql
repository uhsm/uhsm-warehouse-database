﻿CREATE TABLE [RTT].[DurationWeeks] (
    [WaitDurationCode] VARCHAR (50)  NOT NULL,
    [DurationWeek]     VARCHAR (255) NOT NULL,
    [DurationBandCode] VARCHAR (50)  NOT NULL,
    [DurationBand]     VARCHAR (255) NOT NULL,
    [CellColour]       VARCHAR (255) NULL
);

