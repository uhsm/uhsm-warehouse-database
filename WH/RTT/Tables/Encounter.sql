﻿CREATE TABLE [RTT].[Encounter] (
    [EncounterRecno]         INT            IDENTITY (1, 1) NOT NULL,
    [SourceCode]             VARCHAR (10)   NOT NULL,
    [SourceRecno]            INT            NOT NULL,
    [ReviewedFlag]           BIT            NULL,
    [ClockStartDate]         SMALLDATETIME  NULL,
    [ClockStartReasonCode]   VARCHAR (10)   NULL,
    [ClockStopDate]          SMALLDATETIME  NULL,
    [ClockStopReasonCode]    VARCHAR (10)   NULL,
    [ClockStopComment]       VARCHAR (4000) NULL,
    [ClockStopReportingDate] SMALLDATETIME  NULL,
    [WaitReasonCode]         VARCHAR (10)   NULL,
    [WaitReasonDate]         SMALLDATETIME  NULL,
    [UserModeCode]           VARCHAR (10)   NOT NULL,
    [SharedBreachFlag]       BIT            NULL,
    [Created]                DATETIME       NULL,
    [Updated]                DATETIME       NULL,
    [LastUser]               VARCHAR (50)   NULL,
    CONSTRAINT [PK_RTTEncounter] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC),
    CONSTRAINT [FK_RTTEncounter_ClockStartReason] FOREIGN KEY ([ClockStartReasonCode]) REFERENCES [RTT].[ClockStartReason] ([ClockStartReasonCode]),
    CONSTRAINT [FK_RTTEncounter_ClockStopReason] FOREIGN KEY ([ClockStopReasonCode]) REFERENCES [RTT].[ClockStopReason] ([ClockStopReasonCode]),
    CONSTRAINT [FK_RTTEncounter_RTTUserMode] FOREIGN KEY ([UserModeCode]) REFERENCES [RTT].[UserMode] ([UserModeCode]),
    CONSTRAINT [FK_RTTEncounter_WaitReason] FOREIGN KEY ([WaitReasonCode]) REFERENCES [RTT].[WaitReason] ([WaitReasonCode])
);


GO
CREATE NONCLUSTERED INDEX [IX_Encounter_3]
    ON [RTT].[Encounter]([SourceCode] ASC, [SourceRecno] ASC);

