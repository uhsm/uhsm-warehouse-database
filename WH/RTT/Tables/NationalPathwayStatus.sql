﻿CREATE TABLE [RTT].[NationalPathwayStatus] (
    [NationalPathwayStatusCode] VARCHAR (5)  NOT NULL,
    [NationalPathwayStatus]     VARCHAR (50) NULL,
    CONSTRAINT [PK_NationalPathwayStatus] PRIMARY KEY CLUSTERED ([NationalPathwayStatusCode] ASC)
);

