﻿CREATE TABLE [RTT].[OutpatientOutcome] (
    [OutpatientSourceUniqueID] INT          NOT NULL,
    [ReferralSourceUniqueID]   INT          NULL,
    [AppointmentTime]          DATETIME     NULL,
    [RTTStatusCode]            VARCHAR (2)  NULL,
    [Updated]                  DATETIME     NULL,
    [DataSource]               VARCHAR (20) NULL,
    CONSTRAINT [PK_OutpatientOutcome] PRIMARY KEY CLUSTERED ([OutpatientSourceUniqueID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [OutpatientOutcome_IDX_1]
    ON [RTT].[OutpatientOutcome]([Updated] ASC)
    INCLUDE([OutpatientSourceUniqueID], [ReferralSourceUniqueID], [RTTStatusCode], [DataSource]);

