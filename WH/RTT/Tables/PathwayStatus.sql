﻿CREATE TABLE [RTT].[PathwayStatus] (
    [PathwayStatusCode]         VARCHAR (5)  NOT NULL,
    [PathwayStatus]             VARCHAR (50) NULL,
    [NationalPathwayStatusCode] VARCHAR (5)  NULL,
    CONSTRAINT [PK_PathwayStatus] PRIMARY KEY CLUSTERED ([PathwayStatusCode] ASC)
);

