﻿CREATE TABLE [RTT].[RTT] (
    [EncounterRecno]         INT           IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]         NUMERIC (18)  NULL,
    [ReferralSourceUniqueID] NUMERIC (18)  NULL,
    [SourcePatientNo]        NUMERIC (18)  NULL,
    [SourceCode]             VARCHAR (10)  NOT NULL,
    [SourceRecno]            NUMERIC (18)  NOT NULL,
    [StartDate]              DATETIME      NOT NULL,
    [RTTStatusCode]          NUMERIC (18)  NOT NULL,
    [Comment]                VARCHAR (255) NULL,
    [InterfaceCode]          VARCHAR (5)   NOT NULL,
    CONSTRAINT [PK_RTT_1] PRIMARY KEY NONCLUSTERED ([SourceCode] ASC, [SourceRecno] ASC, [StartDate] ASC, [RTTStatusCode] ASC)
);


GO
CREATE CLUSTERED INDEX [RTT_ReferralSourceUniqueID]
    ON [RTT].[RTT]([ReferralSourceUniqueID] ASC, [StartDate] ASC, [EncounterRecno] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RTT]
    ON [RTT].[RTT]([RTTStatusCode] ASC, [ReferralSourceUniqueID] ASC, [StartDate] ASC, [EncounterRecno] ASC);


GO
CREATE NONCLUSTERED INDEX [RTT_Comment]
    ON [RTT].[RTT]([EncounterRecno] ASC, [Comment] ASC);


GO
CREATE STATISTICS [_dta_stat_1707869151_8_7]
    ON [RTT].[RTT]([RTTStatusCode], [StartDate]);


GO
CREATE STATISTICS [_dta_stat_1707869151_3_1]
    ON [RTT].[RTT]([ReferralSourceUniqueID], [EncounterRecno]);


GO
CREATE STATISTICS [_dta_stat_1707869151_1_7_3]
    ON [RTT].[RTT]([EncounterRecno], [StartDate], [ReferralSourceUniqueID]);


GO
CREATE STATISTICS [_dta_stat_1707869151_8_1_7]
    ON [RTT].[RTT]([RTTStatusCode], [EncounterRecno], [StartDate]);

