﻿CREATE TABLE [RTT].[RTTFlag] (
    [EncounterRecno]          INT           NOT NULL,
    [SourceUniqueID]          NUMERIC (18)  NULL,
    [ReferralSourceUniqueID]  NUMERIC (18)  NULL,
    [SourcePatientNo]         NUMERIC (18)  NULL,
    [SourceCode]              VARCHAR (10)  NOT NULL,
    [SourceRecno]             NUMERIC (18)  NOT NULL,
    [StartDate]               DATETIME      NOT NULL,
    [RTTStatusCode]           NUMERIC (18)  NOT NULL,
    [Comment]                 VARCHAR (255) NULL,
    [InterfaceCode]           VARCHAR (5)   NOT NULL,
    [NationalRTTStatusCode]   VARCHAR (10)  NOT NULL,
    [ClockRestartFlag]        BIT           NULL,
    [ClockRestartTriggerFlag] BIT           NULL,
    [ClockStartFlag]          BIT           NULL,
    [ClockStopFlag]           BIT           NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_RTTFlag]
    ON [RTT].[RTTFlag]([ReferralSourceUniqueID] ASC, [StartDate] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ClockStartFlag]
    ON [RTT].[RTTFlag]([ClockStartFlag] ASC)
    INCLUDE([ReferralSourceUniqueID], [StartDate]);

