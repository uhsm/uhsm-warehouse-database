﻿CREATE TABLE [RTT].[RTTPeriod] (
    [ReferralSourceUniqueID]      NUMERIC (18) NOT NULL,
    [StartDate]                   DATETIME     NOT NULL,
    [StartSourceCode]             VARCHAR (10) NOT NULL,
    [StartSourceID]               NUMERIC (18) NULL,
    [StartRTTEntryEncounterRecno] INT          NULL,
    [EndDate]                     DATETIME     NULL,
    [EndSourceCode]               VARCHAR (10) NULL,
    [EndSourceID]                 NUMERIC (18) NULL,
    [EndRTTEntryEncounterRecno]   INT          NULL,
    CONSTRAINT [PK_RTTPeriod] PRIMARY KEY CLUSTERED ([ReferralSourceUniqueID] ASC, [StartDate] ASC)
);

