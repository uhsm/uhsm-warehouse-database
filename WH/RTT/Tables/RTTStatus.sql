﻿CREATE TABLE [RTT].[RTTStatus] (
    [RTTStatusCode]           VARCHAR (10)  NOT NULL,
    [RTTStatus]               VARCHAR (50)  NULL,
    [InternalCode]            VARCHAR (10)  NULL,
    [IsDefaultCode]           VARCHAR (10)  NULL,
    [ClockStopFlag]           BIT           NULL,
    [ClockStartFlag]          BIT           NULL,
    [ClockRestartFlag]        BIT           NULL,
    [ClockRestartTriggerFlag] BIT           NULL,
    [Definition]              VARCHAR (500) NULL,
    [EventClockStatus]        VARCHAR (50)  NULL,
    CONSTRAINT [PK_RTT_RTTStatus] PRIMARY KEY NONCLUSTERED ([RTTStatusCode] ASC)
);


GO
CREATE UNIQUE CLUSTERED INDEX [IX_RTTStatus]
    ON [RTT].[RTTStatus]([InternalCode] ASC);

