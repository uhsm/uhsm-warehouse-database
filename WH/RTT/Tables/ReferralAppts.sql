﻿CREATE TABLE [RTT].[ReferralAppts] (
    [ReferralID]            INT          NULL,
    [FirstApptDate]         DATE         NULL,
    [FirstApptCreatedDate]  DATE         NULL,
    [FirstApptType]         VARCHAR (80) NULL,
    [FirstApptStatus]       VARCHAR (20) NULL,
    [FirstAppOutcomeCode]   VARCHAR (20) NULL,
    [FirstAppOutcome]       VARCHAR (80) NULL,
    [SecondApptDate]        DATE         NULL,
    [SecondApptCreatedDate] DATE         NULL,
    [SecondApptType]        VARCHAR (80) NULL,
    [SecondApptStatus]      VARCHAR (20) NULL,
    [SecondAppOutcomeCode]  VARCHAR (20) NULL,
    [SecondAppOutcome]      VARCHAR (80) NULL,
    [LastApptDate]          DATE         NULL,
    [LastApptCreatedDate]   DATE         NULL,
    [LastApptType]          VARCHAR (80) NULL,
    [LastApptStatus]        VARCHAR (20) NULL,
    [LastAppOutcomeCode]    VARCHAR (20) NULL,
    [LastAppOutcome]        VARCHAR (80) NULL
);

