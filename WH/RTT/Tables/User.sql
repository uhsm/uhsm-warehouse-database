﻿CREATE TABLE [RTT].[User] (
    [Username]     VARCHAR (50) NOT NULL,
    [UserModeCode] VARCHAR (10) NULL,
    CONSTRAINT [PK_RTTUser] PRIMARY KEY CLUSTERED ([Username] ASC),
    CONSTRAINT [FK_RTTUser_RTTUserMode] FOREIGN KEY ([UserModeCode]) REFERENCES [RTT].[UserMode] ([UserModeCode])
);

