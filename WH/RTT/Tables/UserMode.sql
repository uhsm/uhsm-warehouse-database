﻿CREATE TABLE [RTT].[UserMode] (
    [UserModeCode]             VARCHAR (10) NOT NULL,
    [UserMode]                 VARCHAR (50) NULL,
    [UserModeBackgroundColour] VARCHAR (50) NULL,
    CONSTRAINT [PK_RTTUserMode] PRIMARY KEY CLUSTERED ([UserModeCode] ASC)
);

