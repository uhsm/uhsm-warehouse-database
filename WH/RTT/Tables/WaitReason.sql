﻿CREATE TABLE [RTT].[WaitReason] (
    [WaitReasonCode] VARCHAR (10)  NOT NULL,
    [WaitReason]     VARCHAR (255) NULL,
    CONSTRAINT [PK_RTTWaitReason] PRIMARY KEY CLUSTERED ([WaitReasonCode] ASC)
);

