﻿

CREATE procedure [SCR].[Compare2WW] as

/** K Oakden 09-01-14
To compare 2WW referrals on IPM (and routine referrals for breast, cancer not suspected)
to referrals processed by the SCR interface database and referrals records in the SCR system

**/
truncate table WHREPORTING.CAN.InterfaceAudit

select
	 Referral.SourcePatientNo
	,Referral.SourceUniqueID
	,Referral.PriorityCode 
	,PriorityValue = Priority.ReferenceValue
	,CancerTypeCode = 
		case
		when
			Referral.PriorityCode not in 
				(
				 5478 --2 week wait
				,2003614 --2 week wait
				)
			and	Specialty.NationalSpecialtyCode = '103'
		then '16' --Exhibited (non-cancer) breast symptoms - cancer not initially suspected
		else CancerType.MainCode
		end
		
	,CancerType = 
		case
		when
			Referral.PriorityCode not in 
				(
				 5478 --2 week wait
				,2003614 --2 week wait
				)
			and	Specialty.NationalSpecialtyCode = '103'
		then 'Breast symptoms - cancer not initially suspected'
		else CancerType.ReferenceValue
		end
	,DecisionDate = dateadd(day, datediff(day, 0, Referral.ReferralSentDate), 0)
	,ReceiptDate = dateadd(day, datediff(day, 0, Referral.ReferralDate), 0)
	,CreatedOnPAS = Referral.PASCreated
	,UpdatedOnPAS = Referral.PASUpdated
	,Referral.CancellationDate
	,Referral.NHSNumber
	,Referral.DistrictNo
	,CreatedInDW = Referral.Created
	,UpdatedInDW = Referral.Updated
into dbo.#tww
from
	RF.Encounter Referral

left join PAS.Specialty Specialty
on	Specialty.SpecialtyCode = Referral.SpecialtyCode

left join PAS.ReferenceValue CancerType
on	CancerType.ReferenceValueCode = Referral.SuspectedCancerTypeCode

left join PAS.ReferenceValue Priority
on	Priority.ReferenceValueCode = Referral.PriorityCode
where
	(
		(
		Referral.PriorityCode in 
			(
			 5478 --2 week wait
			,2003614 --2 week wait
			)
		and	Referral.ReferralDate >= '1 May 2009' --SCR go-live
		)
	or
		(
		Referral.PriorityCode not in 
			(
			 5478 --2 week wait
			,2003614 --2 week wait
			)
		and	Specialty.NationalSpecialtyCode = '103'
		and	Referral.ReferralDate >= '1 May 2009' --SCR go-live
		)
	)

alter table dbo.#tww add primary key(SourceUniqueID)

insert into WHREPORTING.CAN.InterfaceAudit
select 
	tww.SourceUniqueID
	,tww.NHSNumber
	,tww.DistrictNo
	,tww.CancerTypeCode
	,tww.CancerType
	,tww.PriorityValue
	,tww.DecisionDate
	,tww.ReceiptDate
	,tww.CancellationDate
	,SameDayCancellation = 
		case
			when DATEADD(dd, 0, DATEDIFF(dd, 0, tww.CancellationDate)) 
				= DATEADD(dd, 0, DATEDIFF(dd, 0, tww.CreatedOnPAS)) 
			then 1
			else 0
		end
	,tww.CreatedOnPAS
	,tww.UpdatedOnPAS                                                      
	,tww.CreatedInDW
	,tww.UpdatedInDW
	,SCRInterfaceProcessTime = scrint.ProcessTime
	,SCRReferralUniqueRecordId = scrref.UniqueRecordId
--into WHREPORTING.CAN.InterfaceAudit
from dbo.#tww tww

left join [UHSM-CSERVICES].iPMFeedNew.dbo.Referral scrint
on scrint.SourceUniqueID = tww.SourceUniqueID

left join SCR.Referral scrref
on cast(scrref.SystemId as varchar) = cast(tww.SourceUniqueID as varchar)

--where tww.DistrictNo = 'RM24345003'
--where scrref.UniqueRecordId is not null
--order by tww.SourceUniqueID desc








