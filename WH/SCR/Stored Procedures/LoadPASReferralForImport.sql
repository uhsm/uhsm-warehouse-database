﻿CREATE procedure [SCR].[LoadPASReferralForImport] 
	 @trunc int

as
/*
--Author: K Oakden
--Date created: 09/12/2013
--populate SCR referral table

*/
if(@trunc = 1)
	begin
		truncate table SCR.PASReferralForImport
	end
	--Main referrals
	insert into SCR.PASReferralForImport
	select *, 'ref', GETDATE() 
	from SCR.DailyPASReferral
	
	--Additional referrals for Straight to Test GI patients
	insert into SCR.PASReferralForImport
	select *, 'wl', GETDATE() 
	from SCR.DailyPASReferralFromGIWaitingList


