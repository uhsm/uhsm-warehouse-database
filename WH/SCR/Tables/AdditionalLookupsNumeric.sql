﻿CREATE TABLE [SCR].[AdditionalLookupsNumeric] (
    [LUTableName]   VARCHAR (30)  NOT NULL,
    [LUCodeNumeric] INT           NOT NULL,
    [LUDescription] VARCHAR (150) NULL,
    CONSTRAINT [PK_AdditionalLookupsNumeric] PRIMARY KEY CLUSTERED ([LUTableName] ASC, [LUCodeNumeric] ASC)
);

