﻿CREATE TABLE [SCR].[AdditionalLookupsText] (
    [LUTableName]   VARCHAR (30)  NOT NULL,
    [LUCodeText]    VARCHAR (2)   NOT NULL,
    [LUDescription] VARCHAR (150) NULL,
    CONSTRAINT [PK_AdditionalLookupsText] PRIMARY KEY CLUSTERED ([LUTableName] ASC, [LUCodeText] ASC)
);

