﻿CREATE TABLE [SCR].[AdjustmentReason] (
    [AdjustmentReasonCode] INT           NOT NULL,
    [AdjustmentReason]     VARCHAR (150) NOT NULL,
    [Live]                 INT           NULL
);

