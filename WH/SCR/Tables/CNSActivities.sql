﻿CREATE TABLE [SCR].[CNSActivities] (
    [ACT_CODE]          INT           NOT NULL,
    [ACT_DESC]          VARCHAR (100) NOT NULL,
    [ACT]               VARCHAR (1)   NULL,
    [CA_SITE]           INT           NOT NULL,
    [INFO_ADVICE]       BIT           NOT NULL,
    [CLINICAL_ACTIVITY] BIT           NOT NULL,
    [IS_DELETED]        BIT           NOT NULL,
    [ADVICE_BREAST]     BIT           NOT NULL,
    [ACTIVITY_BREAST]   BIT           NOT NULL
);

