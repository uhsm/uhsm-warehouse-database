﻿CREATE TABLE [SCR].[CNSName] (
    [ID]         INT          NOT NULL,
    [CNS_NAME]   VARCHAR (50) NOT NULL,
    [IS_DELETED] BIT          NOT NULL,
    [USER_ID]    INT          NULL
);

