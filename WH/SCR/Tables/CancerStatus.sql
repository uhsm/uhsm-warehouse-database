﻿CREATE TABLE [SCR].[CancerStatus] (
    [CancerStatusCode]  VARCHAR (2)   NOT NULL,
    [CancerStatus]      VARCHAR (100) NOT NULL,
    [CancerStatusShort] VARCHAR (60)  NULL,
    [OrderBy]           INT           NULL
);

