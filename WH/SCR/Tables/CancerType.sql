﻿CREATE TABLE [SCR].[CancerType] (
    [CancerTypeCode] VARCHAR (2)   NOT NULL,
    [CancerType]     VARCHAR (100) NOT NULL,
    [Urology]        INT           NULL,
    [Haematology]    INT           NULL,
    [Breast]         INT           NULL
);

