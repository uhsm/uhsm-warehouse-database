﻿CREATE TABLE [SCR].[Consultant] (
    [ConsultantId]           INT          NOT NULL,
    [NationalConsultantCode] VARCHAR (8)  NOT NULL,
    [ConsultantCode]         VARCHAR (5)  NOT NULL,
    [Consultant]             VARCHAR (50) NOT NULL,
    [SpecialtyCode]          INT          NOT NULL,
    [Title]                  VARCHAR (5)  NULL,
    [DepartmentCode]         CHAR (1)     NULL,
    [Colorectal]             INT          NULL,
    [Grade]                  VARCHAR (10) NULL,
    [Baus]                   INT          NULL,
    [IsDeleted]              BIT          NOT NULL
);

