﻿CREATE TABLE [SCR].[DefinitiveType] (
    [DefinitiveTypeCode] VARCHAR (2)   NOT NULL,
    [DefinitiveType]     VARCHAR (100) NOT NULL,
    [OrderBy]            INT           NULL
);

