﻿CREATE TABLE [SCR].[Diagnosis] (
    [DiagnosisCode] VARCHAR (50)  NOT NULL,
    [Diagnosis]     VARCHAR (100) NOT NULL,
    [Highlight]     INT           NULL
);

