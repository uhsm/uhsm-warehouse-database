﻿CREATE TABLE [SCR].[FamilyHistory] (
    [FH_ID]                INT           NOT NULL,
    [TEMP_ID]              VARCHAR (255) NULL,
    [CARE_ID]              INT           NULL,
    [L_FAMILY_TAKEN]       INT           NULL,
    [L_FAMILY_DATE]        SMALLDATETIME NULL,
    [L_FAMILY_NAME]        VARCHAR (50)  NULL,
    [L_FAMILY_RELATION]    VARCHAR (3)   NULL,
    [L_FAMILY_DIAGNOSIS]   VARCHAR (5)   NULL,
    [L_FAMILY_DIAGNOSIS_2] VARCHAR (5)   NULL,
    [L_COLORECTAL_ADENOMA] VARCHAR (10)  NULL,
    [L_ULCERATIVE_COLITIS] VARCHAR (10)  NULL,
    [L_FAMILY_POLYPOSIS]   VARCHAR (10)  NULL,
    [L_AGE_DIAGNOSIS]      VARCHAR (15)  NULL,
    [L_GENETIC]            INT           NULL,
    [L_RISK_CALC]          REAL          NULL,
    [L_SKIN_TYPE]          INT           NULL,
    [L_FAMILY_COMMENTS]    TEXT          NULL
);

