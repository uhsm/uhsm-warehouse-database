﻿CREATE TABLE [SCR].[ImagingOutcomes] (
    [OutcomeCode]  INT          NOT NULL,
    [Outcome]      VARCHAR (50) NOT NULL,
    [OutcomeOrder] INT          NULL
);

