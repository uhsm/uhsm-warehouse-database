﻿CREATE TABLE [SCR].[MDTAttendance] (
    [ATTENDANCE_ID] INT           NOT NULL,
    [MDT_ID]        INT           NOT NULL,
    [MEETING_ID]    INT           NULL,
    [MDT_DATE]      SMALLDATETIME NULL,
    [PRESENT]       INT           NULL,
    [COMMENTS]      TEXT          NULL
);

