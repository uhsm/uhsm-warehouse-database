﻿CREATE TABLE [SCR].[MDTAttendees] (
    [MDT_ID]        INT           NOT NULL,
    [MEETING_ID]    INT           NULL,
    [NAME]          VARCHAR (50)  NULL,
    [TYPE]          INT           NULL,
    [LEAD]          INT           NULL,
    [DISPLAY_ORDER] INT           NULL,
    [DELETED]       INT           NULL,
    [DATEDELETED]   SMALLDATETIME NULL,
    [DATEADDED]     SMALLDATETIME NULL,
    [SPECIALTY_ID]  INT           NULL
);

