﻿CREATE TABLE [SCR].[MDTPalliative] (
    [MDT_ID]           INT            NOT NULL,
    [PALLIATIVE_ID]    INT            NOT NULL,
    [TEMP_ID]          VARCHAR (255)  NULL,
    [MEETING_ID]       INT            NULL,
    [UPDATED]          VARCHAR (50)   NULL,
    [L_MDT_DATE]       DATETIME       NULL,
    [L_LOCATION]       VARCHAR (5)    NULL,
    [L_ACTIONS]        TEXT           NULL,
    [L_COMMENTS]       TEXT           NULL,
    [L_PATHOLOGY_TEXT] VARCHAR (8000) NULL
);

