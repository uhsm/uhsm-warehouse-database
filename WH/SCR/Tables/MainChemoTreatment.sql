﻿CREATE TABLE [SCR].[MainChemoTreatment] (
    [ADMIN_ID]              INT           NOT NULL,
    [CHEMO_ID]              INT           NOT NULL,
    [TEMP_ID]               VARCHAR (255) NULL,
    [CYCLE_NUMBER]          INT           NULL,
    [N9_14_CREATININE]      REAL          NULL,
    [N9_15_EVENT_DATE]      SMALLDATETIME NULL,
    [N9_17_DAY_NO]          INT           NULL,
    [N9_18_DRUG_PRESCRIBED] VARCHAR (255) NULL,
    [N9_19_DOSE]            REAL          NULL,
    [L_WEIGHT]              REAL          NULL,
    [L_WHO_STATUS]          INT           NULL,
    [L_OPCS_CODE]           VARCHAR (4)   NULL
);

