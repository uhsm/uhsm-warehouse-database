﻿CREATE TABLE [SCR].[MainPalliative] (
    [PALL_ID]                 INT           NOT NULL,
    [CARE_ID]                 INT           NOT NULL,
    [TEMP_ID]                 VARCHAR (255) NULL,
    [N12_1_DECISION_DATE]     SMALLDATETIME NULL,
    [N12_2_START_DATE]        SMALLDATETIME NULL,
    [N1_3_ORG_CODE_TREATMENT] VARCHAR (5)   NULL,
    [N_SITE_CODE_DTT]         VARCHAR (5)   NULL,
    [N_L39_PROVIDER]          VARCHAR (2)   NULL,
    [N_L40_COMMUNITY]         VARCHAR (2)   NULL,
    [N_L41_TREATMENT]         VARCHAR (1)   NULL,
    [N_L42_TREAT_DATE]        SMALLDATETIME NULL,
    [N_TREATMENT_EVENT]       VARCHAR (2)   NULL,
    [N_TREATMENT_SETTING]     VARCHAR (2)   NULL,
    [N_SPECIALIST]            VARCHAR (2)   NULL,
    [L_TRIAL]                 INT           NULL,
    [DEFINITIVE_TREATMENT]    INT           NULL,
    [CWT_PROFORMA]            INT           NULL,
    [L_COMMENTS]              TEXT          NULL,
    [N7_2_CONSULTANT]         VARCHAR (8)   NULL
);

