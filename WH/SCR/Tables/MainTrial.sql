﻿CREATE TABLE [SCR].[MainTrial] (
    [TRIAL_ID]          INT           NOT NULL,
    [CARE_ID]           INT           NOT NULL,
    [TEMP_ID]           VARCHAR (255) NULL,
    [L_OFFERED]         VARCHAR (50)  NULL,
    [L_NOT_OFFERED]     VARCHAR (255) NULL,
    [N13_1_ENTERED]     VARCHAR (50)  NULL,
    [L_NOT_ENTERED]     VARCHAR (255) NULL,
    [L_TRIAL_NAME]      VARCHAR (255) NULL,
    [N13_2_TREATMENT]   INT           NULL,
    [L_DATE_RANDOMISED] SMALLDATETIME NULL,
    [R_LOC_NAT]         VARCHAR (50)  NULL,
    [L_TRIAL_NO]        VARCHAR (50)  NULL,
    [L_REGIMEN]         VARCHAR (255) NULL,
    [L_CONSENT_DATE]    SMALLDATETIME NULL,
    [L_START_DATE]      SMALLDATETIME NULL,
    [L_CONSULTANT]      VARCHAR (8)   NULL,
    [L_INVEST]          VARCHAR (255) NULL,
    [L_COMMENTS]        TEXT          NULL
);

