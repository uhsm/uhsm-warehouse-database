﻿CREATE TABLE [SCR].[Monitoring] (
    [MONITOR_ID]              INT           NOT NULL,
    [CARE_ID]                 INT           NOT NULL,
    [TEMP_ID]                 VARCHAR (255) NULL,
    [N16_9_DECISION_ACTIVE]   SMALLDATETIME NULL,
    [N16_10_START_ACTIVE]     SMALLDATETIME NULL,
    [N1_3_ORG_CODE_TREATMENT] VARCHAR (5)   NULL,
    [L_MONITORING_INTENT]     VARCHAR (60)  NULL,
    [N_SITE_CODE_DTT]         VARCHAR (5)   NULL,
    [N_TREATMENT_EVENT]       VARCHAR (2)   NULL,
    [N_TREATMENT_SETTING]     VARCHAR (2)   NULL,
    [L_TRIAL]                 INT           NULL,
    [DEFINITIVE_TREATMENT]    INT           NULL,
    [CWT_PROFORMA]            INT           NULL,
    [L_COMMENTS]              TEXT          NULL,
    [N7_2_CONSULTANT]         VARCHAR (8)   NULL
);

