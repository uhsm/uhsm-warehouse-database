﻿CREATE TABLE [SCR].[Organisation] (
    [OrganisationCode] VARCHAR (5)   NOT NULL,
    [Organisation]     VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_Organisation] PRIMARY KEY CLUSTERED ([OrganisationCode] ASC)
);

