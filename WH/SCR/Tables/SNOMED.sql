﻿CREATE TABLE [SCR].[SNOMED] (
    [SNOMEDCode]        VARCHAR (50)  NOT NULL,
    [SNOMEDDescription] VARCHAR (255) NOT NULL,
    [SNOMEDGroup]       INT           NULL,
    [SNOMEDVersion]     VARCHAR (50)  NULL
);

