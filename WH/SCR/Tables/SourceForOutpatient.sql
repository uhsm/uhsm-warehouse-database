﻿CREATE TABLE [SCR].[SourceForOutpatient] (
    [SourceForOutpatientCode] VARCHAR (2)   NOT NULL,
    [SourceForOutpatient]     VARCHAR (100) NOT NULL,
    [OrderBy]                 INT           NOT NULL
);

