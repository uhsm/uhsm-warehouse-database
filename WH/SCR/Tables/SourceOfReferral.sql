﻿CREATE TABLE [SCR].[SourceOfReferral] (
    [SourceOfReferralCode]  VARCHAR (2)   NOT NULL,
    [SourceOfReferral]      VARCHAR (150) NOT NULL,
    [SourceOfReferralShort] VARCHAR (50)  NOT NULL,
    [SourceOfReferralMini]  VARCHAR (50)  NOT NULL,
    [OrderBy]               INT           NOT NULL,
    CONSTRAINT [PK_SourceOfReferral_1] PRIMARY KEY CLUSTERED ([SourceOfReferralCode] ASC)
);

