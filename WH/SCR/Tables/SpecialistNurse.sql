﻿CREATE TABLE [SCR].[SpecialistNurse] (
    [NURSE_ID]                     INT           NOT NULL,
    [CARE_ID]                      INT           NOT NULL,
    [TEMP_ID]                      VARCHAR (255) NULL,
    [FIRST_CONTACT]                INT           NULL,
    [L_DEC_REF_DATE]               SMALLDATETIME NULL,
    [L_REF_DATE]                   SMALLDATETIME NULL,
    [L_SEEN_DATE]                  SMALLDATETIME NULL,
    [L_DISCH_DATE]                 SMALLDATETIME NULL,
    [L_REF_SOURCE]                 VARCHAR (50)  NULL,
    [L_INCLUDED]                   VARCHAR (50)  NULL,
    [L_TOTAL_TIME]                 INT           NULL,
    [L_CONTACT_PLACE]              INT           NULL,
    [L_CONTACT_TYPE]               VARCHAR (50)  NULL,
    [L_CNS]                        INT           NULL,
    [L_NURSE]                      VARCHAR (50)  NULL,
    [L_KEY_WORKER]                 VARCHAR (255) NULL,
    [L_PRESENT_SURGEON]            INT           NULL,
    [L_PRESENT]                    INT           NULL,
    [L_PRESENT_DOC]                INT           NULL,
    [L_PRESENT_NURSE]              INT           NULL,
    [L_PRESENT_GP]                 INT           NULL,
    [L_PRESENT_OTHER]              INT           NULL,
    [L_PRESENT_UNKNOWN]            INT           NULL,
    [L_PRESENT_CONSULTANT]         INT           NULL,
    [L_CONTACT_PAT]                INT           NULL,
    [L_CONTACT_REL]                INT           NULL,
    [L_CONTACT_CARER]              INT           NULL,
    [L_CONTACT_PROF]               INT           NULL,
    [L_PROF_WHO]                   VARCHAR (255) NULL,
    [L_ACTIVITY_1]                 INT           NULL,
    [L_ACTIVITY_2]                 INT           NULL,
    [L_ACTIVITY_3]                 INT           NULL,
    [L_ACTIVITY_4]                 INT           NULL,
    [L_REFERRAL_1]                 INT           NULL,
    [L_REFERRAL_2]                 INT           NULL,
    [L_CONTACT]                    INT           NULL,
    [L_ACCEPTED]                   INT           NULL,
    [L_VERBAL]                     INT           NULL,
    [L_WRITTEN]                    INT           NULL,
    [L_DETAILS]                    INT           NULL,
    [L_PRIVACY]                    INT           NULL,
    [L_DN_INFORMED]                INT           NULL,
    [L_UNDERSTANDING]              INT           NULL,
    [L_INT_ADVICE]                 INT           NULL,
    [L_INT_SHORT]                  INT           NULL,
    [L_INT_REG]                    INT           NULL,
    [L_PT_STATUS]                  INT           NULL,
    [L_ORGANISATION]               VARCHAR (5)   NULL,
    [L_PROGNOSIS]                  VARCHAR (255) NULL,
    [L_NOTES]                      TEXT          NULL,
    [REASON_CNS_NOT_PRESENT]       VARCHAR (50)  NULL,
    [REASON_CNS_NOT_PRESENT_OTHER] VARCHAR (255) NULL,
    [LEVEL_INTERVENTION_1]         INT           NULL,
    [LEVEL_INTERVENTION_2]         INT           NULL,
    [LEVEL_INTERVENTION_3]         INT           NULL,
    [LEVEL_INTERVENTION_4]         INT           NULL,
    [L_CONTACT_OTHER]              INT           NULL,
    [PERFORMANCE_STATUS]           INT           NULL,
    [OTHER_ACTIVITY_1]             VARCHAR (500) NULL,
    [OTHER_ACTIVITY_2]             VARCHAR (500) NULL,
    [OTHER_ACTIVITY_3]             VARCHAR (500) NULL,
    [OTHER_ACTIVITY_4]             VARCHAR (500) NULL,
    [OTHER_ACTIVITY_5]             VARCHAR (500) NULL,
    [OTHER_ACTIVITY_6]             VARCHAR (500) NULL,
    [L_ACTIVITY_5]                 INT           NULL,
    [L_ACTIVITY_6]                 INT           NULL,
    [OTHER_ACTIVITY_7]             VARCHAR (500) NULL,
    [L_REFERRAL_3]                 INT           NULL,
    [L_REFERRAL_OTHER]             VARCHAR (500) NULL,
    [DISTRESS_SCORE]               INT           NULL,
    [HOLISTIC_ASSESSMENT_DECLINED] INT           NULL,
    [HOLISTIC_ASSESSMENT_END]      INT           NULL,
    [L_KEY_WORKER_OTHER]           VARCHAR (255) NULL,
    [L_KEY_WORKER_NEW]             INT           NULL
);

