﻿CREATE TABLE [SCR].[UrologyStatus] (
    [StatusCode] INT           NOT NULL,
    [Status]     VARCHAR (100) NOT NULL,
    [StatusSite] CHAR (1)      NOT NULL
);

