﻿


CREATE view [SCR].[DailyPASReferral] as

select
	 Referral.SourcePatientNo
	,Referral.SourceUniqueID
	,Outpatient.AppointmentDate -- added by JB on 01/09/2015 to push out First Appointment to SCR 
	,Outpatient.AppointmentTime
	,RTTPathwayID = --what about '0001%', '0002%', '0003%' ids?
		case
		when left(Referral.RTTPathwayID, 3) = 'X09'
		then Referral.RTTPathwayID
		else
			left(Referral.RTTPathwayID, 5) + 
			replicate('0', 20 - len(Referral.RTTPathwayID)) + 
			substring(Referral.RTTPathwayID, 6, len(Referral.RTTPathwayID) -5)
		end

	,Referral.SourceOfReferralCode

	,CancerTypeCode = 
		case
		when
			Referral.PriorityCode not in 
				(
				 5478 --2 week wait
				,2003614 --2 week wait
				)
			and	Specialty.NationalSpecialtyCode = '103'
		then '16' --Exhibited (non-cancer) breast symptoms - cancer not initially suspected
		else CancerType.MainCode
		end

	,ReferralSourceCode = ReferralSource.MappedCode
	,PriorityCode = '03'
	,IPMPriorityCode = Referral.PriorityCode
	,IPMWLPriorityCode = CAST (null as int)
	,DecisionDate = dateadd(day, datediff(day, 0, Referral.ReferralSentDate), 0)
	,ReceiptDate = dateadd(day, datediff(day, 0, Referral.ReferralDate), 0)

	,ConsultantCode = Consultant.ProfessionalCarerMainCode
	,SpecialtyCode = Referral.SpecialtyCode
	,MappedSpecialtyCode = left(Specialty.NationalSpecialtyCode, 3)

	,SiteCode = 'RM202'

	,Referral.PASUpdated
	,Referral.PASCreated
	,Specialty.Specialty

	,BreastNonCancer =
		CAST(
			case
			when
				Referral.PriorityCode not in 
					(
					 5478 --2 week wait
					,2003614 --2 week wait
					)
				and	Specialty.NationalSpecialtyCode = '103'
			then 1
			else 0
			end
			as bit
		)

--demographice
	,Referral.NHSNumber

	--KO replaced 14/01/2013
	--,NHSNumberStatusCode = 
	--	case
	--	when Referral.NHSNumber = '05' then 9
	--	when Referral.NHSNumber = '06' then 9
	--	when Referral.NHSNumber = '07' then 9
	--	when Referral.NHSNumber = '08' then 9
	--	when Referral.NHSNumber = 'NSP' then 9
	--	when Referral.NHSNumber = 'RESET' then 9
	--	else 1
	--	end
	,NHSNumberStatusCode =
		case
		when Referral.NHSNumber is null
		then '04' --Trace attempted - No match or multiple match found
		else
			case
			when Referral.NHSNumberStatusCode = 'SUCCS' then '01'
			when Referral.NHSNumberStatusCode = 'RESET' then '02'
			when Referral.NHSNumberStatusCode is null then '01'
			else Referral.NHSNumberStatusCode
			end
		end
		
	,Referral.DistrictNo

	,Referral.PatientTitle
	,PatientForename = upper(Referral.PatientForename)
	,PatientSurname = upper(Referral.PatientSurname)

	,PatientAddress1 =
		case
		when Referral.PatientAddress1 = ''
		then upper(Referral.PatientAddress2)
		else upper(Referral.PatientAddress1)
		end


	,PatientAddress2 =
		case
		when Referral.PatientAddress1 = ''
		then null
		else upper(Referral.PatientAddress2)
		end


	,PatientAddress3 = upper(Referral.PatientAddress3)
	,PatientAddress4 = upper(Referral.PatientAddress4)
	,Postcode = upper(Referral.Postcode)

	,SexCode = Sex.MappedCode

	,Referral.DateOfBirth

	,RegisteredGpCode = left(GP.ProfessionalCarerMainCode, 8)

	,RegisteredGpPracticeCode = Practice.OrganisationLocalCode

	,PCTCode = PCT.OrganisationLocalCode

	,EthnicGroupCode =
		case
		when EthnicOrigin.MappedCode = 'NKN'
		then 'Z'
		else EthnicOrigin.MappedCode
		end

	,Referral.PatientDeceased
	,Referral.DateOfDeath

	,Session.EBSServiceID

from
	RF.Encounter Referral

left join PAS.ReferenceValue ReferralSource
on	ReferralSource.ReferenceValueCode = Referral.SourceOfReferralCode

left join PAS.ProfessionalCarer Consultant
on	Consultant.ProfessionalCarerCode = Referral.ConsultantCode

left join PAS.Specialty Specialty
on	Specialty.SpecialtyCode = Referral.SpecialtyCode

left join PAS.ReferenceValue CancerType
on	CancerType.ReferenceValueCode = Referral.SuspectedCancerTypeCode

left join PAS.ReferenceValue Sex
on	Sex.ReferenceValueCode = Referral.SexCode

left join PAS.ProfessionalCarer GP
on    GP.ProfessionalCarerCode = Referral.RegisteredGpCode

left join PAS.Organisation Practice
on	Practice.OrganisationCode = Referral.RegisteredGpPracticeCode

left join PAS.Organisation PCT
on	PCT.OrganisationCode = Practice.ParentOrganisationCode

left join PAS.ReferenceValue EthnicOrigin
on    EthnicOrigin.ReferenceValueCode = Referral.EthnicOriginCode

left join OP.Encounter Outpatient
on	Outpatient.ReferralSourceUniqueID = Referral.SourceUniqueID
and	not exists
	(
	select
		1
	from
		OP.Encounter Previous
	where
		Previous.ReferralSourceUniqueID = Referral.SourceUniqueID
	and	(
			Previous.AppointmentTime < Outpatient.AppointmentTime
		or	(
				Previous.AppointmentTime = Outpatient.AppointmentTime
			and	Previous.EncounterRecno < Outpatient.EncounterRecno
			)
		)
	)

left join OP.Session
on	Session.SessionUniqueID = Outpatient.SessionUniqueID

where
	(
		(
		Referral.PriorityCode in 
			(
			 5478 --2 week wait
			,2003614 --2 week wait
			)
--		and	Referral.ReferralDate > DATEADD(day, -31, cast(getdate() as date))
		and	Referral.ReferralDate >= '1 May 2009' --SCR go-live
		)
	or
		(
		Referral.PriorityCode not in 
			(
			 5478 --2 week wait
			,2003614 --2 week wait
			)
		--and	Specialty.NationalSpecialtyCode = '103'
		--KO replaced 30/03/2015 because original resulted in error in RTTPathwayID substring calculation
		--No idea why
		and	Referral.SpecialtyCode = 2000780
		and	Referral.ReferralDate >= '1 May 2009' --SCR go-live
		)
	)

and	Referral.CancellationDate is null
and (ClinicCode NOT IN ('GI', 'GULDUM5', 'GIUDUM5') or ClinicCode IS NULL) --added by JB on 21/12/2015 to remove telephone clinics


