﻿








CREATE view [SCR].[PASReferral] as

select distinct 
	ForImport.SourcePatientNo
	,ForImport.SourceUniqueID
	,ForImport.AppointmentDate --added by JB on 01/09/2015 to push out first appointment date to SCR
	,ForImport.AppointmentTime --added by JB on 28/09/2015 to push out first appointment date to SCR
	,ForImport.RTTPathwayID
	,ForImport.SourceOfReferralCode
	,ForImport.CancerTypeCode
	,ForImport.ReferralSourceCode
	,ForImport.PriorityCode
	--,IPMReferralPriorityCode
 --   ,IPMWLPriorityCode
	,ForImport.DecisionDate
	,ForImport.ReceiptDate
	,ForImport.ConsultantCode
	,ForImport.SpecialtyCode
	,ForImport.MappedSpecialtyCode
	,ForImport.SiteCode
	,ForImport.PASUpdated
	,ForImport.PASCreated
	,ForImport.Specialty
	,ForImport.BreastNonCancer
	,ForImport.NHSNumber
	,ForImport.NHSNumberStatusCode
	,ForImport.DistrictNo
	,ForImport.PatientTitle
	,ForImport.PatientForename
	,ForImport.PatientSurname
	,ForImport.PatientAddress1
	,ForImport.PatientAddress2
	,ForImport.PatientAddress3
	,ForImport.PatientAddress4
	,ForImport.Postcode
	,ForImport.SexCode
	,ForImport.DateOfBirth
	,ForImport.RegisteredGpCode
	,ForImport.RegisteredGpPracticeCode
	,ForImport.PCTCode
	,EthnicGroupCode
	,ForImport.PatientDeceased
	,ForImport.DateOfDeath
	,ForImport.EBSServiceID
	--,Source
	--,CensusDate
	,Encounter.ReferralComment
	,Encounter.GeneralComment
	,Encounter.OurActivityFlag
	,Encounter.NotOurActProvCode
	,Encounter.CancellationDate
	,Encounter.DischargeDate
from SCR.PASReferralForImport ForImport
Left outer join RF.Encounter on ForImport.SourceUniqueID =  Encounter.SourceUniqueID

where dateadd(dd, 0, datediff(dd, 0, getdate())) = dateadd(dd, 0, datediff(dd, 0, CensusDate))
and Source = 'ref'













