﻿CREATE ROLE [InformationAnalyst]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'InformationAnalyst', @membername = N'UHSM\isarwar';


GO
EXECUTE sp_addrolemember @rolename = N'InformationAnalyst', @membername = N'UHSM\hransome';


GO
EXECUTE sp_addrolemember @rolename = N'InformationAnalyst', @membername = N'UHSM\PGraham';


GO
EXECUTE sp_addrolemember @rolename = N'InformationAnalyst', @membername = N'UHSM\dbellhartley';


GO
EXECUTE sp_addrolemember @rolename = N'InformationAnalyst', @membername = N'UHSM\aallcutt';


GO
EXECUTE sp_addrolemember @rolename = N'InformationAnalyst', @membername = N'UHSM\AOlivant';


GO
EXECUTE sp_addrolemember @rolename = N'InformationAnalyst', @membername = N'UHSM\ADavenpo';

