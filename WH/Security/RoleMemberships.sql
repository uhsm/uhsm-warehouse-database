﻿EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'UHSM\svc-dw02';


GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'UHSM\svc-dw01';


GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'UHSM\NScatter';


GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'svc-scorecard';


GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'UHSM-DW-1\InformationUser';


GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'UHSM\GFenton';


GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'UHSM\GrRyder';


GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'UHSM\CBeckett';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'svclpi';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'V-UHSM-DW01\InformationUser';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\svc-dw02';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'cservices';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\svc-dw01';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\hransome';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\migray';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'svc-scorecard';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\vmccormack';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'test';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'V-UHSM-DW02\InformationUser';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\jpotluri';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'svc-link';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\cbellhartley';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM-DW-1\InformationUser';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'svc-cservices';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\PGraham';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\dbellhartley';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\DNorring';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\GrRyder';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\aallcutt';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\AOlivant';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\ccripps';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\CBeckett';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\cfisher';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\PPrior';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'svc-reporting';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\ADavenpo';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\pasemota';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\MHamblet';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\abromley';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'UHSM\koakden';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'UHSM\svc-dw01';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'UHSM\hransome';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'UHSM\migray';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'UHSM\jpotluri';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'svc-link';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'UHSM\cbellhartley';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'UHSM-DW-1\InformationUser';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'UHSM\GrRyder';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'UHSM\aallcutt';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'UHSM\AOlivant';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'UHSM\PPrior';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'UHSM\ADavenpo';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'UHSM\pasemota';

