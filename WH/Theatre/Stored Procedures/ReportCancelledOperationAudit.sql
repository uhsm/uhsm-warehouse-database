﻿CREATE        procedure [Theatre].[ReportCancelledOperationAudit] 
	 @fromDate smalldatetime
	,@toDate smalldatetime
	,@trackStatus char(1) = null
as

select
	Episode.ProcedureDate,
	Episode.SourcePatientNo DistrictNo,
	coalesce(Specialty.Specialty, '') Specialty,
	coalesce(Consultant.Consultant, '') Consultant,

	Encounter.Surname PatientName,
	Encounter.DateOfBirth,

	Episode.WardCode,
	left(PatientCategory.PatientCategory, 1) PatientCategory,
	left(Interface.Interface, 1) Site,
	Theatre.Theatre,
	CancelReason.CancelReason,
	left(CancelReason.CancelBy, 1) CancelBy,
	CancelReason.Clinical,
	case when Tracking.TrackingRecno is null then 'No' else 
		case when Tracking.DisableTrackingDate is null then 'Active' else 'Disabled' end
	end TrackedFlag,

	case when Tracking.TrackingRecno is null 
	then '' 
	else (select TextValue from Parameter where Parameter = 'TRACKINGURL')  + convert(varchar, Tracking.TrackingRecno)
	end TrackingURL,

	Tracking.Comment,

	case when Tracking.TrackingRecno is not null and Tracking.DisableTrackingDate is null then dateadd(day, 28, coalesce(Tracking.PASCancelDate, Episode.ProcedureDate)) end BreachDate,

	Tracking.AdmissionDate,
	Tracking.PASCancelDate,
	Tracking.TCIDate,
	Tracking.TreatedDate,

	case
	when Tracking.TCIDate is not null and Tracking.TCIDate <= case when Tracking.TrackingRecno is not null and Tracking.DisableTrackingDate is null then dateadd(day, 28, Episode.ProcedureDate) end then 'white'
	when Tracking.TCIDate is not null and getdate() >= case when Tracking.TrackingRecno is not null and Tracking.DisableTrackingDate is null then dateadd(day, 28, Episode.ProcedureDate) end then 'red'
	when Tracking.TCIDate is not null and Tracking.TCIDate > case when Tracking.TrackingRecno is not null and Tracking.DisableTrackingDate is null then dateadd(day, 28, Episode.ProcedureDate) end then 'pink'
	when Tracking.TCIDate is null and getdate() < case when Tracking.TrackingRecno is not null and Tracking.DisableTrackingDate is null then dateadd(day, 28, Episode.ProcedureDate) end then 'blue'
	when Tracking.TCIDate is null and getdate() >= case when Tracking.TrackingRecno is not null and Tracking.DisableTrackingDate is null then dateadd(day, 28, Episode.ProcedureDate) end then 'red'
	else 'white'
	end TCIBackgroundColour,

	case
	when Tracking.ReportableFlag is null then 'Non-Reportable'
	when Tracking.ReportableFlag = 0 then 'Non-Reportable'
	when Tracking.ReportableFlag = 1 then 'Reportable'
	end Reportable
from
	Theatre.PatientEpisode Episode

left join Theatre.Tracking
on	Episode.SourceUniqueID = Tracking.EpisodeID
and	Episode.InterfaceCode = Tracking.InterfaceCode

left join Theatre.LegacySessions Session
on	Session.ID = Episode.SessionID
and	Session.InterfaceCode = Episode.InterfaceCode

left join Theatre.CancelReason CancelReason
on	CancelReason.CancelReasonCode = Episode.CancelReason

inner join Theatre.PatientCategory PatientCategory
on	PatientCategory.PatientCategoryCode = 
	case	
	when Episode.IntendedManagement is null then 'DC'
	when Episode.IntendedManagement in ('IP', 'DC', 'OP') then Episode.IntendedManagement
	when Episode.IntendedManagement in ('', 'PP', 'SDS') then 'DC'
	when Episode.IntendedManagement = 'PPSDS' then 'DC'
	when Episode.IntendedManagement = 'PPIP' then 'IP'
	when Episode.IntendedManagement = 'PPDC' then 'DC'
	else 'DC'
	end

inner join Interface
on	Interface.InterfaceCode = Episode.InterfaceCode

inner join Theatre.Theatre Theatre
on	Theatre.TheatreCode = Episode.TheatreNumber
and	Theatre.InterfaceCode = Episode.InterfaceCode

left join Theatre.Consultant Consultant
on	Consultant.ConsultantCode = Session.ConsultantCode
and	Consultant.InterfaceCode = Episode.InterfaceCode

left join Theatre.Specialty Specialty
on	Specialty.SpecialtyCode = Consultant.SpecialtyCode

inner join dbo.Parameter CancellationBreachDays
on	CancellationBreachDays.Parameter = 'CANCELLATIONBREACHDAYS'

left join Theatre.CrIndex1 Encounter
on	Encounter.InternalDistrictNumber = Episode.InternalDistrictNumber
and	Encounter.InterfaceCode = Episode.InterfaceCode

where
--	Episode.CancelReason is not null
	coalesce(Episode.Outcome, '6') = '6'
--and	(Episode.TheatreNumber not in ('E3-2', 'E3', 'G3') and Episode.InterfaceCode = 'OTIMS')
and	Episode.ProcedureDate between @fromDate and @toDate
and	
	(
		@trackStatus = '#'
	or	@trackStatus = case when Tracking.TrackingRecno is null then 'N' else 
		case when Tracking.DisableTrackingDate is null then 'A' else 'D' end
		end
	)

order by
	case
	when Tracking.ReportableFlag is null then 'Non-Reportable'
	when Tracking.ReportableFlag = 0 then 'Non-Reportable'
	when Tracking.ReportableFlag = 1 then 'Reportable'
	end desc,
	Interface.Interface,
	Theatre.Theatre,
	Episode.ProcedureDate,
	coalesce(Specialty.Specialty, ''),
	coalesce(Consultant.Consultant, '')
