﻿CREATE       procedure [Theatre].[ReportCancelledOperationSummary]
	 @fromDate smalldatetime
	,@toDate smalldatetime
as

select
	TrackingRecno,
	BreachStatus.BreachStatus BreachStatus,


	case
	when Tracking.TCIDate < dateadd(day, CancellationBreachDays.NumericValue, coalesce(Tracking.PASCancelDate, Episode.ProcedureDate)) then 1 else 0
	end DatedWithinBreachDays,

	case
	when Tracking.TCIDate >= dateadd(day, CancellationBreachDays.NumericValue, coalesce(Tracking.PASCancelDate, Episode.ProcedureDate)) then 1 else 0
	end DatedThatWillBreach,

	case
	when DATEADD(day, DATEDIFF(day,0,GETDATE()), 0) > dateadd(day, CancellationBreachDays.NumericValue, coalesce(Tracking.PASCancelDate, Episode.ProcedureDate)) then 1 else 0
	end DefiniteBreach,

	case
	when Tracking.TCIDate is null then 1 else 0
	end NotDated,

--Tracking.TCIDate, CancellationBreachDays.NumericValue, Tracking.PASCancelDate, Episode.ProcedureDate, dateadd(day, CancellationBreachDays.NumericValue, coalesce(Tracking.PASCancelDate, Episode.ProcedureDate))
	Episode.ProcedureDate,
	Episode.SourcePatientNo DistrictNo,
	coalesce(Specialty.Specialty, '') Specialty,
	coalesce(Consultant.Consultant, '') Consultant,

	Encounter.Surname PatientName,
	Encounter.DateOfBirth,

	Episode.WardCode,
	PatientCategory.PatientCategory,
	Interface.Interface Site,
	Theatre.Theatre,
	CancelReason.CancelReason,
	case when Tracking.TrackingRecno is null then 'No' else 
		case when Tracking.DisableTrackingDate is null then 'Active' else 'Disabled' end
	end + coalesce(' (' + left(convert(varchar, DisableTrackingDate, 113), 11) + ')', '') TrackedFlag,

	case when Tracking.TrackingRecno is null 
	then '' 
	else (select TextValue from Parameter where Parameter = 'TRACKINGURL')  + convert(varchar, Tracking.TrackingRecno)
	end TrackingURL,

	Tracking.AdmissionDate,
	Tracking.PASCancelDate,
	Tracking.TCIDate,
	Tracking.TreatedDate,
	Tracking.Updated,
	Tracking.Created,
	Tracking.ByWhom,
	dateadd(day, CancellationBreachDays.NumericValue, coalesce(Tracking.PASCancelDate, Episode.ProcedureDate)) BreachDate,
	Tracking.Comment
from
	Theatre.PatientEpisode Episode

inner join Theatre.Tracking
on	Episode.SourceUniqueID = Tracking.EpisodeID
and	Episode.InterfaceCode = Tracking.InterfaceCode

left join Theatre.LegacySessions Session
on	Session.ID = Episode.SessionID
and	Session.InterfaceCode = Episode.InterfaceCode

left join Theatre.CancelReason CancelReason
on	CancelReason.CancelReasonCode = Episode.CancelReason

inner join Theatre.PatientCategory PatientCategory
on	PatientCategory.PatientCategoryCode = 
	case	
	when Episode.IntendedManagement is null then 'DC'
	when Episode.IntendedManagement in ('IP', 'DC', 'OP') then Episode.IntendedManagement
	when Episode.IntendedManagement in ('', 'PP', 'SDS') then 'DC'
	when Episode.IntendedManagement = 'PPSDS' then 'DC'
	when Episode.IntendedManagement = 'PPIP' then 'IP'
	when Episode.IntendedManagement = 'PPDC' then 'DC'
	else 'DC'
	end

inner join Interface
on	Interface.InterfaceCode = Episode.InterfaceCode

inner join Theatre.Theatre Theatre
on	Theatre.TheatreCode = Episode.TheatreNumber
and	Theatre.InterfaceCode = Episode.InterfaceCode

left join Theatre.Consultant Consultant
on	Consultant.ConsultantCode = Session.ConsultantCode
and	Consultant.InterfaceCode = Episode.InterfaceCode

left join Theatre.Specialty Specialty
on	Specialty.SpecialtyCode = Consultant.SpecialtyCode

inner join dbo.Parameter CancellationBreachDays
on	CancellationBreachDays.Parameter = 'CANCELLATIONBREACHDAYS'

inner join Theatre.BreachStatus
on	case
	when Tracking.TCIDate < dateadd(day, CancellationBreachDays.NumericValue, coalesce(Tracking.PASCancelDate, Episode.ProcedureDate)) then 1
	when Tracking.TCIDate >= dateadd(day, CancellationBreachDays.NumericValue, coalesce(Tracking.PASCancelDate, Episode.ProcedureDate)) then 2
	when DATEADD(day, DATEDIFF(day,0,GETDATE()), 0) > dateadd(day, CancellationBreachDays.NumericValue, coalesce(Tracking.PASCancelDate, Episode.ProcedureDate)) then 3
	else 4
	end = BreachStatus.BreachStatusCode

left join Theatre.CrIndex1 Encounter
on	Encounter.InternalDistrictNumber = Episode.InternalDistrictNumber
and	Encounter.InterfaceCode = Episode.InterfaceCode

where

	Episode.ProcedureDate between @fromDate and @toDate
and	Tracking.DisableTrackingDate is null

order by
	BreachStatus.BreachStatusCode,
	Interface.Interface,
	Theatre.Theatre,
	Episode.ProcedureDate,
	coalesce(Specialty.Specialty, ''),
	coalesce(Consultant.Consultant, '')
