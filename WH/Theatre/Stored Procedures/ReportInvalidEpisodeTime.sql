﻿CREATE    procedure [Theatre].[ReportInvalidEpisodeTime]
	 @ErrorTypeCode varchar(3)
	,@fromDate varchar(11)
	,@toDate varchar(11)
as


--declare @fromDate smalldatetime

--set @fromDate = '1 jan 2006'

set dateformat dmy

--prepare the temporary table
select
	[EncounterRecno], [SourceUniqueID], [ASACode], [ATO], [ATORelieved], [ActualOperationLine1], [ActualOperationLine2], [ActualOperationLine3], [ActualOperationLine4], [AnaestheticComments], [AnaestheticNurse1], [AnaestheticNurse1Relieved], [AnaestheticNurse2], [AnaestheticNurse2Relieved], [AnaestheticType1], [AnaestheticType2], [AnaestheticType3], [AnaestheticType4], [Anaesthetist1], [Anaesthetist2], [Anaesthetist3], [CEPOD], [CancelReason], [CirculatingNurse], [CirculatingNurseRelieved], [DestinationFromRecovery], [Episode], [EscortName], [EscortNurse], [EscortNurseRelieved], [ExpectedLengthOfOperation], [FirstAssistant], [IntendedManagement], [InternalDistrictNumber], [OtherNurse], [OtherNurseRelieved], [Outcome], [PatientAccompanied], [PatientLocation], [PlannedAnaesthetic], [PortersActivityReception], [ProcedureDate], [ProcedureTime], [ProposedOperationLine1], [ProposedOperationLine2], [ProposedOperationLine3], [ProposedOperationLine4], [ReasonDelayReception1], [ReasonDelayReception2], [ReasonDelayRecovery1], [ReasonDelayRecovery2], [ReceptionComments], [ReceptionNurse1], [ReceptionNurse2], [ReceptionPorter], [RecoveryComments], [RecoveryPorter], [RevoveryNurse], [RevoveryNurseRelieved], [ScrubNurse1], [ScrubNurse1Relieved], [ScrubNurse2], [ScrubNurse2Relieved], [SpecialEquipment1], [SpecialEquipment2], [SpecialEquipment3], [SpecialEquipment4], [Surgeon1], [Surgeon2], [Surgeon3], [TheatreComments], [TheatreNumber], [TimeAnaestheticStart], [TimeArrived], [TimeAwake], [TimeInAnaestheticRoom], [TimeInRecovery], [TimeOffTable], [TimeOnTable], [TimeOutAnaestheticRoom], [TimeOutRecovery], [TimeReadyForDischarge], [TimeSentFor], [WardCode], [SessionID], [SourcePatientNo], [InterfaceCode], [PASEncounterRecno]
into
	#InvalidEpisodeTime
from
	Theatre.PatientEpisode Episode
where
	EncounterRecno = -1

set identity_insert dbo.#InvalidEpisodeTime on

if @ErrorTypeCode in ('MT', 'ALL') --missing key times
begin
	insert into
		#InvalidEpisodeTime
	(
		[EncounterRecno], [SourceUniqueID], [ASACode], [ATO], [ATORelieved], [ActualOperationLine1], [ActualOperationLine2], [ActualOperationLine3], [ActualOperationLine4], [AnaestheticComments], [AnaestheticNurse1], [AnaestheticNurse1Relieved], [AnaestheticNurse2], [AnaestheticNurse2Relieved], [AnaestheticType1], [AnaestheticType2], [AnaestheticType3], [AnaestheticType4], [Anaesthetist1], [Anaesthetist2], [Anaesthetist3], [CEPOD], [CancelReason], [CirculatingNurse], [CirculatingNurseRelieved], [DestinationFromRecovery], [Episode], [EscortName], [EscortNurse], [EscortNurseRelieved], [ExpectedLengthOfOperation], [FirstAssistant], [IntendedManagement], [InternalDistrictNumber], [OtherNurse], [OtherNurseRelieved], [Outcome], [PatientAccompanied], [PatientLocation], [PlannedAnaesthetic], [PortersActivityReception], [ProcedureDate], [ProcedureTime], [ProposedOperationLine1], [ProposedOperationLine2], [ProposedOperationLine3], [ProposedOperationLine4], [ReasonDelayReception1], [ReasonDelayReception2], [ReasonDelayRecovery1], [ReasonDelayRecovery2], [ReceptionComments], [ReceptionNurse1], [ReceptionNurse2], [ReceptionPorter], [RecoveryComments], [RecoveryPorter], [RevoveryNurse], [RevoveryNurseRelieved], [ScrubNurse1], [ScrubNurse1Relieved], [ScrubNurse2], [ScrubNurse2Relieved], [SpecialEquipment1], [SpecialEquipment2], [SpecialEquipment3], [SpecialEquipment4], [Surgeon1], [Surgeon2], [Surgeon3], [TheatreComments], [TheatreNumber], [TimeAnaestheticStart], [TimeArrived], [TimeAwake], [TimeInAnaestheticRoom], [TimeInRecovery], [TimeOffTable], [TimeOnTable], [TimeOutAnaestheticRoom], [TimeOutRecovery], [TimeReadyForDischarge], [TimeSentFor], [WardCode], [SessionID], [SourcePatientNo], [InterfaceCode], [PASEncounterRecno]
	)
	select
		Episode.*
	from
		Theatre.PatientEpisode Episode
	
--need to repoint this to WHOLAP
	inner join WHOLAP.dbo.FactTheatreSession Session
	on	Session.SourceUniqueID = Episode.SessionID
	and	Session.InterfaceCode = Episode.InterfaceCode
	
	where
		Episode.TimeAnaestheticStart is null
	and	Episode.TimeOffTable is null
	and	Episode.CancelReason is null
	and	Session.CancelReasonCode = '##'
	and	coalesce(
			 TimeSentFor
			,TimeArrived
			,TimeInAnaestheticRoom
			,TimeAnaestheticStart
			,TimeOutAnaestheticRoom
			,TimeOnTable
			,TimeOffTable
			,TimeInRecovery
			,TimeReadyForDischarge
			,TimeInRecovery
			) is not null
	and	Episode.ProcedureDate between @fromDate and @toDate
end


if @ErrorTypeCode in ('TO', 'ALL') --invalid time order
begin
	insert into
		#InvalidEpisodeTime
	(
		[EncounterRecno], [SourceUniqueID], [ASACode], [ATO], [ATORelieved], [ActualOperationLine1], [ActualOperationLine2], [ActualOperationLine3], [ActualOperationLine4], [AnaestheticComments], [AnaestheticNurse1], [AnaestheticNurse1Relieved], [AnaestheticNurse2], [AnaestheticNurse2Relieved], [AnaestheticType1], [AnaestheticType2], [AnaestheticType3], [AnaestheticType4], [Anaesthetist1], [Anaesthetist2], [Anaesthetist3], [CEPOD], [CancelReason], [CirculatingNurse], [CirculatingNurseRelieved], [DestinationFromRecovery], [Episode], [EscortName], [EscortNurse], [EscortNurseRelieved], [ExpectedLengthOfOperation], [FirstAssistant], [IntendedManagement], [InternalDistrictNumber], [OtherNurse], [OtherNurseRelieved], [Outcome], [PatientAccompanied], [PatientLocation], [PlannedAnaesthetic], [PortersActivityReception], [ProcedureDate], [ProcedureTime], [ProposedOperationLine1], [ProposedOperationLine2], [ProposedOperationLine3], [ProposedOperationLine4], [ReasonDelayReception1], [ReasonDelayReception2], [ReasonDelayRecovery1], [ReasonDelayRecovery2], [ReceptionComments], [ReceptionNurse1], [ReceptionNurse2], [ReceptionPorter], [RecoveryComments], [RecoveryPorter], [RevoveryNurse], [RevoveryNurseRelieved], [ScrubNurse1], [ScrubNurse1Relieved], [ScrubNurse2], [ScrubNurse2Relieved], [SpecialEquipment1], [SpecialEquipment2], [SpecialEquipment3], [SpecialEquipment4], [Surgeon1], [Surgeon2], [Surgeon3], [TheatreComments], [TheatreNumber], [TimeAnaestheticStart], [TimeArrived], [TimeAwake], [TimeInAnaestheticRoom], [TimeInRecovery], [TimeOffTable], [TimeOnTable], [TimeOutAnaestheticRoom], [TimeOutRecovery], [TimeReadyForDischarge], [TimeSentFor], [WardCode], [SessionID], [SourcePatientNo], [InterfaceCode], [PASEncounterRecno]
	)
	select
		Episode.*
	from
		Theatre.PatientEpisode Episode
	
	inner join dbo.Parameter MaxTheatreActivityMinutes
	on	MaxTheatreActivityMinutes.Parameter = 'MAXTHEATREACTIVITYMINUTES'
	
	where
		coalesce(case when TimeArrived < TimeSentFor or datediff(minute, TimeSentFor, TimeArrived) > MaxTheatreActivityMinutes.NumericValue then 1 else 0 end, 0) +
		coalesce(case when TimeInAnaestheticRoom < TimeArrived or datediff(minute, TimeArrived, TimeInAnaestheticRoom) > MaxTheatreActivityMinutes.NumericValue then 1 else 0 end, 0) +
		coalesce(case when TimeAnaestheticStart < TimeInAnaestheticRoom or datediff(minute, TimeInAnaestheticRoom, TimeAnaestheticStart) > MaxTheatreActivityMinutes.NumericValue then 1 else 0 end, 0) +
		coalesce(case when TimeOutAnaestheticRoom < TimeAnaestheticStart or datediff(minute, TimeAnaestheticStart, TimeOutAnaestheticRoom) > MaxTheatreActivityMinutes.NumericValue then 1 else 0 end, 0) +
		coalesce(case when TimeOnTable < TimeOutAnaestheticRoom or datediff(minute, TimeOutAnaestheticRoom, TimeOnTable) > MaxTheatreActivityMinutes.NumericValue then 1 else 0 end, 0) +
		coalesce(case when TimeOffTable < TimeOnTable or datediff(minute, TimeOnTable, TimeOffTable) > MaxTheatreActivityMinutes.NumericValue then 1 else 0 end, 0) +
		coalesce(case when TimeInRecovery < TimeOffTable or datediff(minute, TimeOffTable, TimeInRecovery) > MaxTheatreActivityMinutes.NumericValue then 1 else 0 end, 0) +
		coalesce(case when TimeReadyForDischarge < TimeInRecovery or datediff(minute, TimeInRecovery, TimeReadyForDischarge) > MaxTheatreActivityMinutes.NumericValue then 1 else 0 end, 0) +
		coalesce(case when TimeOutRecovery < TimeReadyForDischarge or datediff(minute, TimeReadyForDischarge, TimeOutRecovery) > MaxTheatreActivityMinutes.NumericValue then 1 else 0 end, 0)
		> 0
	and	Episode.ProcedureDate between @fromDate and @toDate
end


select
	datediff(minute, 
	coalesce(
	Episode.TimeSentFor,
	Episode.TimeArrived,
	Episode.TimeInAnaestheticRoom,
	Episode.TimeAnaestheticStart,
	Episode.TimeOutAnaestheticRoom,
	Episode.TimeOnTable,
	Episode.TimeOffTable,
	Episode.TimeInRecovery,
	Episode.TimeReadyForDischarge,
	Episode.TimeOutRecovery,
	Episode.ProcedureDate
	),

	coalesce(
	Episode.TimeOutRecovery,
	Episode.TimeReadyForDischarge,
	Episode.TimeInRecovery,
	Episode.TimeOffTable,
	Episode.TimeOnTable,
	Episode.TimeOutAnaestheticRoom,
	Episode.TimeAnaestheticStart,
	Episode.TimeInAnaestheticRoom,
	Episode.TimeArrived,
	Episode.TimeSentFor,
	Episode.ProcedureDate
	)
	) TotalTime,

	Site.Interface Site,
	Theatre.Theatre,
	coalesce(Consultant.SpecialtyCode + ' - ' + Specialty.Specialty, 'Unknown Specialty') Specialty,
	coalesce(Consultant.Consultant, 'Unknown Consultant') Consultant,

	InvalidEpisodeTime.SessionID,
	InvalidEpisodeTime.SourceUniqueID EpisodeID,
	InvalidEpisodeTime.SourcePatientNo DistrictNo,
	InvalidEpisodeTime.InterfaceCode,
	left(convert(varchar, InvalidEpisodeTime.ProcedureDate, 113), 11) ProcedureDate,
	left(convert(varchar, InvalidEpisodeTime.TimeSentFor, 14), 5) TimeSentFor,
	left(convert(varchar, InvalidEpisodeTime.TimeArrived, 14), 5) TimeArrived,
	left(convert(varchar, InvalidEpisodeTime.TimeInAnaestheticRoom, 14), 5) TimeInAnaestheticRoom,
	left(convert(varchar, InvalidEpisodeTime.TimeAnaestheticStart, 14), 5) TimeAnaestheticStart,
	left(convert(varchar, InvalidEpisodeTime.TimeOutAnaestheticRoom, 14), 5) TimeOutAnaestheticRoom,
	left(convert(varchar, InvalidEpisodeTime.TimeOnTable, 14), 5) TimeOnTable,
	left(convert(varchar, InvalidEpisodeTime.TimeOffTable, 14), 5) TimeOffTable,
	left(convert(varchar, InvalidEpisodeTime.TimeInRecovery, 14), 5) TimeInRecovery,
	left(convert(varchar, InvalidEpisodeTime.TimeReadyForDischarge, 14), 5) TimeReadyForDischarge,
	left(convert(varchar, InvalidEpisodeTime.TimeOutRecovery, 14), 5) TimeOutRecovery,

	case when InvalidEpisodeTime.TimeArrived < InvalidEpisodeTime.TimeSentFor then 'LightCoral' else 'LemonChiffon' end TimeArrivedTimeSentForFlag,
	case when InvalidEpisodeTime.TimeInAnaestheticRoom < InvalidEpisodeTime.TimeArrived then 'LightCoral' else 'LemonChiffon' end TimeInAnaestheticRoomTimeArrivedFlag,
	case when InvalidEpisodeTime.TimeAnaestheticStart < InvalidEpisodeTime.TimeInAnaestheticRoom then 'LightCoral' else 'LemonChiffon' end TimeAnaestheticStartTimeInAnaestheticRoomFlag,
	case when InvalidEpisodeTime.TimeOutAnaestheticRoom < InvalidEpisodeTime.TimeAnaestheticStart then 'LightCoral' else 'LemonChiffon' end TimeOutAnaestheticRoomTimeAnaestheticStartFlag,
	case when InvalidEpisodeTime.TimeOnTable < InvalidEpisodeTime.TimeOutAnaestheticRoom then 'LightCoral' else 'LemonChiffon' end TimeOnTableTimeOutAnaestheticRoomFlag,
	case when InvalidEpisodeTime.TimeOffTable < InvalidEpisodeTime.TimeOnTable then 'LightCoral' else 'LemonChiffon' end TimeOffTableTimeOnTableFlag,
	case when InvalidEpisodeTime.TimeInRecovery < InvalidEpisodeTime.TimeOffTable then 'LightCoral' else 'LemonChiffon' end TimeInRecoveryTimeOffTableFlag,
	case when InvalidEpisodeTime.TimeReadyForDischarge < InvalidEpisodeTime.TimeInRecovery then 'LightCoral' else 'LemonChiffon' end TimeReadyForDischargeTimeInRecoveryFlag,
	case when InvalidEpisodeTime.TimeOutRecovery < InvalidEpisodeTime.TimeReadyForDischarge then 'LightCoral' else 'LemonChiffon' end TimeOutRecoveryTimeReadyForDischargeFlag
from
	#InvalidEpisodeTime InvalidEpisodeTime

inner join Theatre.LegacySessions Session
on	Session.ID = InvalidEpisodeTime.SessionID
and	Session.InterfaceCode = InvalidEpisodeTime.InterfaceCode

inner join Theatre.PatientEpisode Episode
on	Episode.SessionID = InvalidEpisodeTime.SessionID
and	Episode.SourceUniqueID = InvalidEpisodeTime.SourceUniqueID
and	Episode.InterfaceCode = InvalidEpisodeTime.InterfaceCode

inner join Theatre.Theatre Theatre
on	Theatre.TheatreCode = Episode.TheatreNumber
and	Theatre.InterfaceCode = Episode.InterfaceCode

left join Interface Site
on	Site.InterfaceCode = Episode.InterfaceCode

left join Theatre.Consultant Consultant
on	Consultant.ConsultantCode = Session.ConsultantCode
and	Consultant.InterfaceCode = Episode.InterfaceCode

left join Theatre.Specialty Specialty
on	Specialty.SpecialtyCode = Consultant.SpecialtyCode

order by
	Site.Interface,
	coalesce(Consultant.SpecialtyCode + ' - ' + Specialty.Specialty, 'Unknown Specialty'),
	coalesce(Consultant.Consultant, 'Unknown Consultant'),
	Episode.ProcedureDate,

	datediff(minute, 
	coalesce(
	Episode.TimeSentFor,
	Episode.TimeArrived,
	Episode.TimeInAnaestheticRoom,
	Episode.TimeAnaestheticStart,
	Episode.TimeOutAnaestheticRoom,
	Episode.TimeOnTable,
	Episode.TimeOffTable,
	Episode.TimeInRecovery,
	Episode.TimeReadyForDischarge,
	Episode.TimeOutRecovery,
	Episode.ProcedureDate
	),

	coalesce(
	Episode.TimeOutRecovery,
	Episode.TimeReadyForDischarge,
	Episode.TimeInRecovery,
	Episode.TimeOffTable,
	Episode.TimeOnTable,
	Episode.TimeOutAnaestheticRoom,
	Episode.TimeAnaestheticStart,
	Episode.TimeInAnaestheticRoom,
	Episode.TimeArrived,
	Episode.TimeSentFor,
	Episode.ProcedureDate
	)
	) desc
