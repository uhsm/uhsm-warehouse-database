﻿CREATE   procedure [Theatre].[ReportWeeklyUtilisation] 
	 @LocationCode varchar(50)
	,@From smalldatetime
	,@To smalldatetime 
as

--declare @LocationCode varchar(50)
--select @LocationCode = 'PH1'

select
	EncounterRecno = FactTheatreSession.SourceUniqueID,
	FactTheatreSession.SessionDate,
	OlapTheatreSessionPeriod.TheatreSessionPeriod,
	Consultant = OlapConsultant.StaffName,
	Specialty = Specialty.Specialty,
	FactTheatreSession.TheatreCode,
	SessionTypeCode = null,
	Session.SessionStartTime,
	Session.SessionEndTime,
	FactTheatreSession.SessionDuration PlannedDuration,
	left(right(convert(varchar, Session.ActualSessionStartTime, 14), 12), 5) ActualSessionStartTime,
	left(right(convert(varchar, Session.ActualSessionEndTime, 14), 12), 5) ActualSessionEndTime,
	FactTheatreSession.ActualDuration,
	FactTheatreSession.ActualDuration * 1.00 / FactTheatreSession.SessionDuration * 1.00 PercentUsed,
	CancelReasonCode = FactTheatreSession.CancelledSession,
	case when FactTheatreSession.CancelledSession = 0 then null else 'Session cancelled' end CancelReason,
	Cases.IP,
	Cases.DC,
	Cases.OP,
	Cases.Cancellation,

	case
	when FactTheatreSession.CancelledSession = 0 then 'Black' -- not cancelled
	else
		case 
		when coalesce(Cases.IP, 0) + coalesce(Cases.DC, 0) + coalesce(Cases.OP, 0) = 0 then 'Purple' -- cancelled
		else 'Blue' -- cancelled but rescheduled and held
		end
	end RowColour,

	case
	when FactTheatreSession.CancelledSession = 0 then '' -- not cancelled
	else
		case 
		when coalesce(Cases.IP, 0) + coalesce(Cases.DC, 0) + coalesce(Cases.OP, 0) = 0 then 'Cancelled' -- cancelled
		else 'Cancelled but rescheduled and held' -- cancelled but rescheduled and held
		end
	end RowHint,

	case
	when FactTheatreSession.CancelledSession = 0 then 'Normal' -- not cancelled
	else 'Bold'
	end RowFontWeight

/*
	Session.StartTime,
	Session.EndTime,
	Session.ActualSessionStartTime,
	Session.ActualSessionEndTime,
*/
from
	WHOLAP.dbo.FactTheatreSession FactTheatreSession
inner join
	(
		select
			Session.SourceUniqueID SessionID,
			Session.StartTime,
			Session.EndTime,
			left(right(convert(varchar, Session.StartTime, 14), 12), 5) SessionStartTime, 
			left(right(convert(varchar, Session.EndTime, 14), 12), 5) SessionEndTime,
			ActualSession.StartTime ActualSessionStartTime,
			ActualSession.EndTime ActualSessionEndTime
			,Session.SpecialtyCode
		from
			Theatre.Session Session

		left join
			(
			select
				min(PatientEpisode.InAnaestheticTime) StartTime,
				max(PatientEpisode.InRecoveryTime) EndTime,
				PatientEpisode.SessionSourceUniqueID SessionID
			from
				Theatre.OperationDetail PatientEpisode
			where
		-- ignore cancellations
				PatientEpisode.OperationCancelledFlag = 0
			and	PatientEpisode.InAnaestheticTime is not null
			and	PatientEpisode.InRecoveryTime is not null
			group by
				PatientEpisode.SessionSourceUniqueID
		
			) ActualSession
		on	ActualSession.SessionID = Session.SourceUniqueID

		where
--			Session.ProcedureDate between '1 apr 2003' and convert(smalldatetime, left(convert(smalldatetime, dateadd(day, -1, getdate()), 113), 11))
			Session.StartTime between '1 apr 2003' and convert(smalldatetime, left(convert(smalldatetime, getdate(), 113), 11))
		--and	Session.TheatreNumber != '0'
		--and	Session.SessionType != 'E'
	) Session
on	Session.SessionID = FactTheatreSession.SourceUniqueID

left join
	(
	select
		SessionID = Cases.SessionSourceUniqueID,
		sum(case when Cases.AdmissionTypeCode in (17, 18, 19, 20, 21, 22, 23) and Cases.CancelReasonCode = 0 then 1 else 0 end) IP,
		sum(case when Cases.AdmissionTypeCode in (16) and Cases.CancelReasonCode = 0 then 1 else 0 end) DC,
		sum(case when Cases.AdmissionTypeCode = 99 and Cases.CancelReasonCode = 0 then 1 else 0 end) OP,
		sum(case when Cases.CancelReasonCode != 0 then 1 else 0 end) Cancellation
	from
		WHOLAP.dbo.FactTheatreOperation Cases
	group by
		Cases.SessionSourceUniqueID
	) Cases
on	Cases.SessionID = FactTheatreSession.SourceUniqueID

inner join WHOLAP.dbo.OlapTheatre Theatre
on	Theatre.TheatreCode = FactTheatreSession.TheatreCode
and	Theatre.OperatingSuiteCode = @LocationCode

inner join WHOLAP.dbo.OlapTheatreSessionPeriod
on	OlapTheatreSessionPeriod.TheatreSessionPeriodCode = FactTheatreSession.TheatreSessionPeriodCode

left join WHOLAP.dbo.OlapTheatreStaff OlapConsultant
on	OlapConsultant.StaffCode = FactTheatreSession.ConsultantCode

left join WHOLAP.dbo.OlapTheatreSpecialty Specialty
on	Specialty.SpecialtyCode = Session.SpecialtyCode

where
	FactTheatreSession.SessionDate between @From and @To

order by
	FactTheatreSession.SessionDate,
	FactTheatreSession.TheatreSessionPeriodCode,
	FactTheatreSession.TheatreCode

