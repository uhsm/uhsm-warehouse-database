﻿CREATE TABLE [Theatre].[AdmissionType] (
    [AdmissionTypeCode]  INT           NOT NULL,
    [AdmissionTypeCode1] VARCHAR (5)   NULL,
    [AdmissionType]      VARCHAR (255) NULL,
    [InactiveFlag]       BIT           NOT NULL,
    CONSTRAINT [PK_TheatreAdmissionType] PRIMARY KEY CLUSTERED ([AdmissionTypeCode] ASC)
);

