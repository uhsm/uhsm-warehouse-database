﻿CREATE TABLE [Theatre].[AnaestheticType] (
    [AnaestheticTypeCode]  INT           NOT NULL,
    [AnaestheticType]      VARCHAR (255) NULL,
    [AnaestheticTypeCode1] VARCHAR (4)   NULL,
    [InactiveFlag]         BIT           NOT NULL,
    CONSTRAINT [PK_Anaesthetic] PRIMARY KEY CLUSTERED ([AnaestheticTypeCode] ASC)
);

