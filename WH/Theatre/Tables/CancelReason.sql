﻿CREATE TABLE [Theatre].[CancelReason] (
    [CancelReasonCode]      INT           NOT NULL,
    [CancelReason]          VARCHAR (255) NULL,
    [CancelReasonCode1]     VARCHAR (5)   NULL,
    [CancelReasonGroupCode] INT           NOT NULL,
    [InactiveFlag]          BIT           NOT NULL,
    CONSTRAINT [PK_TheatreCancelReason] PRIMARY KEY CLUSTERED ([CancelReasonCode] ASC)
);

