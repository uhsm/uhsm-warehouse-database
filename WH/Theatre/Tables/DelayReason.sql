﻿CREATE TABLE [Theatre].[DelayReason] (
    [DelayReasonCode]        NUMERIC (9)  NOT NULL,
    [DelayReasonDescription] VARCHAR (60) NULL,
    [DelayReasonCode1]       VARCHAR (5)  NULL
);

