﻿CREATE TABLE [Theatre].[Delays] (
    [DelaySourceUniqueID]           NUMERIC (9)   NOT NULL,
    [OperationDetailSourceUniqueID] NUMERIC (9)   NOT NULL,
    [DelayStage]                    VARCHAR (100) NULL,
    [DelayMinutes]                  NUMERIC (9)   NOT NULL,
    [DelayComments]                 VARCHAR (400) NULL,
    [DelayReasonCode]               NUMERIC (9)   NOT NULL
);

