﻿CREATE TABLE [Theatre].[LastMinuteCancellations] (
    [SourceUniqueID]           NUMERIC (9)   NOT NULL,
    [TCIToOperationDate]       INT           NULL,
    [KeyCancellationDateFrom]  VARCHAR (22)  NOT NULL,
    [CancellationToOperation]  INT           NULL,
    [LastMinuteCancellations]  VARCHAR (1)   NOT NULL,
    [ProposedOperationDate]    DATETIME      NULL,
    [BookingOperationDate]     DATETIME      NULL,
    [OfferOperationDate]       SMALLDATETIME NULL,
    [TCIDate]                  DATETIME      NULL,
    [TCIDateTime]              SMALLDATETIME NULL,
    [DiffOperDates]            INT           NOT NULL,
    [BookingStatus]            VARCHAR (17)  NOT NULL,
    [OfferRefno]               INT           NULL,
    [OfferOutcome]             VARCHAR (80)  NULL,
    [OfferOutcomeDate]         SMALLDATETIME NULL,
    [OrmisCancellationDate]    DATETIME      NULL,
    [FacilID]                  VARCHAR (12)  NULL,
    [CC_DETAILS]               TEXT          NULL,
    [OrmisCancellationUpdated] DATETIME      NULL,
    [OrmisWLReference]         VARCHAR (40)  NULL,
    [OfferDateUpdated]         DATETIME      NULL
);

