﻿CREATE TABLE [Theatre].[OperatingSuite] (
    [OperatingSuiteCode]  INT           NOT NULL,
    [OperatingSuite]      VARCHAR (255) NULL,
    [OperatingSuiteCode1] VARCHAR (5)   NULL,
    [InactiveFlag]        BIT           NOT NULL,
    CONSTRAINT [PK_OperatingSuite] PRIMARY KEY CLUSTERED ([OperatingSuiteCode] ASC)
);

