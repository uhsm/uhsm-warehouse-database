﻿CREATE TABLE [Theatre].[Operation] (
    [OperationCode]  INT           NOT NULL,
    [Operation]      VARCHAR (255) NULL,
    [OperationCode1] VARCHAR (8)   NULL,
    [InactiveFlag]   BIT           NOT NULL,
    CONSTRAINT [PK_TheatreOperation] PRIMARY KEY CLUSTERED ([OperationCode] ASC)
);

