﻿CREATE TABLE [Theatre].[SessionType] (
    [SessionTypeCode]        VARCHAR (10) NULL,
    [SessionTypeDescription] VARCHAR (60) NULL,
    [InactiveFlag]           NUMERIC (3)  NOT NULL
);

