﻿CREATE TABLE [Theatre].[TimetableDetail] (
    [TimetableDetailCode]   INT           NOT NULL,
    [SessionNumber]         TINYINT       NOT NULL,
    [DayNumber]             TINYINT       NOT NULL,
    [StartDate]             SMALLDATETIME NULL,
    [EndDate]               SMALLDATETIME NULL,
    [TheatreCode]           INT           NULL,
    [SessionStartTime]      DATETIME      NULL,
    [SessionEndTime]        DATETIME      NULL,
    [ConsultantCode]        VARCHAR (10)  NULL,
    [AnaesthetistCode]      VARCHAR (10)  NULL,
    [SpecialtyCode]         VARCHAR (10)  NULL,
    [TimetableTemplateCode] INT           NULL,
    [LogLastUpdated]        DATETIME      NULL,
    [RecordLogDetails]      VARCHAR (255) NULL,
    [SessionMinutes]        INT           NULL,
    CONSTRAINT [PK_TimetableDetail] PRIMARY KEY CLUSTERED ([TimetableDetailCode] ASC, [SessionNumber] ASC, [DayNumber] ASC)
);

