﻿CREATE TABLE [Theatre].[TimetableTemplate] (
    [TimetableTemplateCode] INT           NOT NULL,
    [SessionNumber]         TINYINT       NOT NULL,
    [DayNumber]             TINYINT       NOT NULL,
    [TheatreCode]           INT           NULL,
    [SessionStartTime]      DATETIME      NULL,
    [SessionEndTime]        DATETIME      NULL,
    [ConsultantCode]        VARCHAR (10)  NULL,
    [AnaesthetistCode]      VARCHAR (10)  NULL,
    [SpecialtyCode]         VARCHAR (10)  NULL,
    [LogLastUpdated]        DATETIME      NULL,
    [RecordLogTemplates]    VARCHAR (255) NULL,
    CONSTRAINT [PK_TimetableTemplate] PRIMARY KEY CLUSTERED ([TimetableTemplateCode] ASC, [SessionNumber] ASC, [DayNumber] ASC)
);

