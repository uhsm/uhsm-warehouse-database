﻿CREATE TABLE [Theatre].[Ward] (
    [WardCode]     INT           NOT NULL,
    [Ward]         VARCHAR (255) NULL,
    [WardCode1]    VARCHAR (20)  NULL,
    [InactiveFlag] BIT           NOT NULL,
    CONSTRAINT [PK_TheatreWard] PRIMARY KEY CLUSTERED ([WardCode] ASC)
);

