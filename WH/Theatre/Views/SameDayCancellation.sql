﻿CREATE view [Theatre].[SameDayCancellation] as

select
	PatientBooking.SourceUniqueID

--Tracking.AdmissionDate
--Tracking.PASCancelDate
--Tracking.TCIDate
--Tracking.TreatedDate
--Tracking.Comment

	,BreachDate =
		dateadd(
			 day
			,coalesce(CancellationBreachDays.NumericValue, 28)
			,coalesce(PatientBooking.IntendedProcedureDate, dateadd(day, datediff(day, 0, PatientBooking.SessionStartTime), 0))
			) 
from
	Theatre.PatientBooking PatientBooking

left join dbo.Parameter CancellationBreachDays
on	CancellationBreachDays.Parameter = 'CANCELLATIONBREACHDAYS'





--select
--	Tracking.TrackingRecno,
--	Tracking.EpisodeID,
--	Tracking.InterfaceCode,
--	Tracking.AdmissionDate,
--	Tracking.PASCancelDate,
--	Tracking.TCIDate,
--	Tracking.TreatedDate,
--	Tracking.Comment,
--	BreachDate =
--		dateadd(
--			 day
--			,CancellationBreachDays.NumericValue
--			,coalesce(Tracking.PASCancelDate, Episode.ProcedureDate)
--			) 
--from
--	Theatre.PatientEpisode Episode

--inner join Theatre.Tracking
--on	Episode.ID = Tracking.EpisodeID
--and	Episode.InterfaceCode = Tracking.InterfaceCode

--inner join dbo.Parameter CancellationBreachDays
--on	CancellationBreachDays.Parameter = 'CANCELLATIONBREACHDAYS'

--where
--	Tracking.DisableTrackingDate is not null
