﻿

CREATE view [Theatre].[vwUtilisationDrRpt] as

select
u.Specialty,
u.Theatre,
u.MonthNumber,
u.[Year],
Case when  u.Planned is null then null else u.Planned end as Planned,
u.Productive,
u.Utilisation
--,(case when u.Planned=0 then null else (sum(u.Productive)/sum(u.Planned)) end) as KPIProductivity,
--(case when u.Planned=0 then null else (sum(u.Utilisation)/sum(u.Planned)) end) as KPIUtilisation
,case when utilisation>planned then planned else u.Utilisation end as CappedUtilisation
from

(select
s.NationalSpecialtyCode,
s.Specialty,
t.SourceUniqueID,
th.Theatre,
MONTH(t.sessiondate) as MonthNumber,
c.FinancialYear as [Year],

Planned=sum(Case when t.KPI='Planned Minutes Of Actual Session' then (t.actual) else 0 end),
Productive=sum(Case when t.KPI='Productive Minutes' then (t.actual) else 0 end),
Utilisation=sum(Case when t.KPI='Session Utilisation Minutes' then (t.actual) else 0 end)

from wholap.dbo.FactTheatreKPI t
inner join wholap.dbo.OlapTheatreSpecialty s on s.SpecialtyCode=t.SpecialtyCode
inner join wholap.dbo.OlapTheatre th on th.TheatreCode=t.TheatreCode
inner join wholap.dbo.OlapCalendar c on c.TheDate=t.SessionDate

WHERE (YEAR(t.sessiondate)*100)+MONTH(t.sessiondate)<>(YEAR(GETDATE())*100)+MONTH(GETDATE())
and th.TheatreCode in (1,2,3,6,78,37,38,39,40,14,16,21,13,15,19,20,22,23)
group by
s.NationalSpecialtyCode,
s.Specialty,
t.SourceUniqueID,
th.Theatre,
MONTH(t.sessiondate),
c.FinancialYear) as u

where u.Planned>0

group by
u.Specialty,
u.Theatre,
u.MonthNumber,
u.[Year],
u.Planned,
u.sourceuniqueid,
u.Productive,
u.Utilisation
,case when utilisation>planned then planned else u.Utilisation end
