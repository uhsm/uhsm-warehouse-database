﻿CREATE TABLE [WA].[Operation] (
    [SourceUniqueID]    VARCHAR (50)  NOT NULL,
    [SourcePatientNo]   VARCHAR (20)  NULL,
    [SourceEncounterNo] VARCHAR (20)  NULL,
    [SequenceNo]        SMALLINT      NULL,
    [OperationCode]     VARCHAR (10)  NULL,
    [OperationDate]     SMALLDATETIME NULL,
    [ConsultantCode]    VARCHAR (10)  NULL,
    [WASourceUniqueID]  VARCHAR (50)  NULL,
    [Created]           DATETIME      NOT NULL,
    [Updated]           DATETIME      NULL,
    [ByWhom]            VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Procedure_1] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);

