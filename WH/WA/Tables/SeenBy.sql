﻿CREATE TABLE [WA].[SeenBy] (
    [SeenByCode] VARCHAR (10)  NOT NULL,
    [SeenBy]     VARCHAR (255) NULL,
    CONSTRAINT [PK_SeenBy] PRIMARY KEY CLUSTERED ([SeenByCode] ASC)
);

