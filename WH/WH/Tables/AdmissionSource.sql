﻿CREATE TABLE WH.[AdmissionSource] (
    [AdmissionSourceCode] VARCHAR (2)   NOT NULL,
    [AdmissionSource]     VARCHAR (255) NULL,
    CONSTRAINT [PK_AdmissionSource_1] PRIMARY KEY CLUSTERED ([AdmissionSourceCode] ASC)
);

