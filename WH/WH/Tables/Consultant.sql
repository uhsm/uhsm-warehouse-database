﻿CREATE TABLE WH.[Consultant] (
    [ConsultantCode] VARCHAR (10) NOT NULL,
    [Title]          VARCHAR (10) NULL,
    [Forename]       VARCHAR (20) NULL,
    [Surname]        VARCHAR (30) NULL,
    CONSTRAINT [PK_WH_Consultant] PRIMARY KEY CLUSTERED ([ConsultantCode] ASC)
);

