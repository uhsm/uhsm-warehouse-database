﻿CREATE TABLE WH.[DemandTarget] (
    [TargetTypeCode]     VARCHAR (10)  NOT NULL,
    [SpecialtyCode]      INT           NOT NULL,
    [PCTCode]            VARCHAR (50)  NOT NULL,
    [Period]             SMALLDATETIME NOT NULL,
    [DemandCategoryCode] VARCHAR (50)  NOT NULL,
    [Activity]           FLOAT (53)    NOT NULL,
    CONSTRAINT [PK_DemandTarget] PRIMARY KEY CLUSTERED ([TargetTypeCode] ASC, [SpecialtyCode] ASC, [PCTCode] ASC, [Period] ASC, [DemandCategoryCode] ASC),
    CONSTRAINT [FK_DemandTarget_TargetType] FOREIGN KEY ([TargetTypeCode]) REFERENCES [TargetType] ([TargetTypeCode])
);

