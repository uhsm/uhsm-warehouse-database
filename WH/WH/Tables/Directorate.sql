﻿CREATE TABLE WH.[Directorate] (
    [DirectorateCode] VARCHAR (5)  NOT NULL,
    [Directorate]     VARCHAR (50) NULL,
    [DivisionCode]    VARCHAR (5)  NULL,
    CONSTRAINT [PK_WH_Directorate] PRIMARY KEY CLUSTERED ([DirectorateCode] ASC),
    CONSTRAINT [FK_Directorate_Division] FOREIGN KEY ([DivisionCode]) REFERENCES [Division] ([DivisionCode])
);

