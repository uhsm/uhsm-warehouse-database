﻿CREATE TABLE WH.[DischargeMethod] (
    [DischargeMethodCode] VARCHAR (2)   NOT NULL,
    [DischargeMethod]     VARCHAR (255) NULL,
    CONSTRAINT [PK_DischargeMethod_1] PRIMARY KEY CLUSTERED ([DischargeMethodCode] ASC)
);

