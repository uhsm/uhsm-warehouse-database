﻿CREATE TABLE WH.[Interface] (
    [InterfaceCode]     VARCHAR (10) NOT NULL,
    [Interface]         VARCHAR (50) NULL,
    [LinkedServer]      VARCHAR (50) NULL,
    [InterfaceTypeCode] VARCHAR (10) NULL,
    CONSTRAINT [PK_Interface] PRIMARY KEY CLUSTERED ([InterfaceCode] ASC),
    CONSTRAINT [FK_Interface_InterfaceType] FOREIGN KEY ([InterfaceTypeCode]) REFERENCES [InterfaceType] ([InterfaceTypeCode])
);

