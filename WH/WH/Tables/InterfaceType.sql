﻿CREATE TABLE WH.[InterfaceType] (
    [InterfaceTypeCode] VARCHAR (10) NOT NULL,
    [InterfaceType]     VARCHAR (50) NULL,
    CONSTRAINT [PK_InterfaceType] PRIMARY KEY CLUSTERED ([InterfaceTypeCode] ASC)
);

