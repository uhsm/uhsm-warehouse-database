﻿CREATE TABLE WH.[ManagementIntention] (
    [ManagementIntentionCode] VARCHAR (10) NOT NULL,
    [ManagementIntention]     VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ManagementIntention] PRIMARY KEY CLUSTERED ([ManagementIntentionCode] ASC)
);

