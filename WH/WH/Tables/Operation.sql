﻿CREATE TABLE WH.[Operation] (
    [OperationCode] VARCHAR (10)  NOT NULL,
    [Operation]     VARCHAR (255) NULL,
    CONSTRAINT [PK_Operation] PRIMARY KEY CLUSTERED ([OperationCode] ASC)
);

