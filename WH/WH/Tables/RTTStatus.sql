﻿CREATE TABLE WH.[RTTStatus] (
    [RTTStatusCode] VARCHAR (10) NOT NULL,
    [RTTStatus]     VARCHAR (50) NULL,
    [InternalCode]  VARCHAR (10) NULL,
    [IsDefaultCode] VARCHAR (10) NULL,
    CONSTRAINT [PK_RTTStatus] PRIMARY KEY CLUSTERED ([RTTStatusCode] ASC)
);

