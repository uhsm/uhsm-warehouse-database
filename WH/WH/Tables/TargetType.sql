﻿CREATE TABLE WH.[TargetType] (
    [TargetTypeCode] VARCHAR (10) NOT NULL,
    [TargetType]     VARCHAR (50) NULL,
    CONSTRAINT [PK_TargetType] PRIMARY KEY CLUSTERED ([TargetTypeCode] ASC)
);

