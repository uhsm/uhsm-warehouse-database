﻿CREATE TABLE WH.[TimeBand] (
    [TimeBandCode]      INT          NOT NULL,
    [MinuteBand]        VARCHAR (5)  NOT NULL,
    [FifteenMinuteBand] VARCHAR (11) NOT NULL,
    [ThirtyMinuteBand]  VARCHAR (11) NOT NULL,
    [HourBand]          VARCHAR (11) NOT NULL,
    CONSTRAINT [PK_TimeBand] PRIMARY KEY CLUSTERED ([TimeBandCode] ASC)
);

