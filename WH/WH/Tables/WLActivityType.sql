﻿CREATE TABLE WH.[WLActivityType] (
    [ActivityTypeCode] VARCHAR (10) NOT NULL,
    [ActivityType]     VARCHAR (50) NULL,
    [AgreedMinutes]    SMALLINT     NULL,
    CONSTRAINT [PK_WLActivityType] PRIMARY KEY CLUSTERED ([ActivityTypeCode] ASC)
);

