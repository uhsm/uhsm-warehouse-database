﻿-- =============================================
-- Author:		K Oakden
-- Create date: 27-04-2013
-- Description:	To generate consultant national code using check digit
-- FOr use in with cascade data where check digit is not recorded
-- =============================================
CREATE FUNCTION [dbo].[GetConsultantNationalCode]
(
@Prefix char(1),
@Code varchar(10)
)
RETURNS varchar(10)
AS
BEGIN

declare @output varchar(10) = null
if len(@code) > 0
begin
	select @output = 
		@Prefix + @Code + 
		right(
			convert(varchar, convert(int, SUBSTRING(@Code, 1, 1))
			+ (convert(int, SUBSTRING(@Code, 2, 1)) * 3)
			+ (convert(int, SUBSTRING(@Code, 3, 1)) * 7)
			+ convert(int, SUBSTRING(@Code, 4, 1))
			+ (convert(int, SUBSTRING(@Code, 5, 1)) * 3)
			+ (convert(int, SUBSTRING(@Code, 6, 1)) * 7))
		, 1)
end
return @output

END
