﻿CREATE FUNCTION [dbo].[ufnFormatPhone](@phone NVARCHAR(60))
RETURNS NVARCHAR(60)
AS
  BEGIN
-- Strip @number of extra chracters
	DECLARE @lenPhone INT ,@phoneStr NVARCHAR(30)
	WHILE PATINDEX('%[^0-9]%', @phone) > 0 
        SET @phone = REPLACE(@phone,SUBSTRING(@phone,PATINDEX('%[^0-9]%', @phone),1),'') 

    RETURN @phone
END
