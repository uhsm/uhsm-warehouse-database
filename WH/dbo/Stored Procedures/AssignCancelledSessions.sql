﻿CREATE  procedure [dbo].[AssignCancelledSessions] as

-- update existing sessions that were either cancelled or reassigned
update Theatre.LegacySessions
set
	HistoryRecno = null

update Theatre.LegacySessions
set
	HistoryRecno = History.EncounterRecno
	,Reassigned = History.Reassigned
from
	Theatre.LegacySessions Session

inner join
	(
	-- reassigned
	select
		History.EncounterRecno,
		History.SessionID,
		History.InterfaceCode,
		1 Reassigned
	from
		Theatre.History
	
	inner join Theatre.LegacySessions Session
	on	Session.ID = History.SessionID
	and	Session.InterfaceCode = History.InterfaceCode
	
	where
		not exists
		(
		select
			1
		from
			Theatre.History OriginalSession
		where
			OriginalSession.SessionID = History.SessionID
		and	OriginalSession.InterfaceCode = History.InterfaceCode
		and
			(
				coalesce(OriginalSession.AmendTime, '6/6/2079') > coalesce(History.AmendTime, '6/6/2079')
			or	(
				coalesce(OriginalSession.AmendTime, '6/6/2079') = coalesce(History.AmendTime, '6/6/2079')
			and	OriginalSession.[ID] > History.[ID]
				)
			)
		)
	and	History.ReAssignReason <> '49'

	union all

	--cancellations
	select
		History.EncounterRecno,
		History.SessionID,
		History.InterfaceCode,
		0 Reassigned
	from
		Theatre.History
	where
		not exists
		(
		select
			1
		from
			Theatre.History OriginalSession
		where
			OriginalSession.SessionID = History.SessionID
		and	OriginalSession.InterfaceCode = History.InterfaceCode
		and
			(
				coalesce(OriginalSession.AmendTime, '6/6/2079') > coalesce(History.AmendTime, '6/6/2079')
			or	(
				coalesce(OriginalSession.AmendTime, '6/6/2079') = coalesce(History.AmendTime, '6/6/2079')
			and	OriginalSession.[ID] > History.[ID]
				)
			)
		)
	
	and	History.CancelReason <> '49'

	 ) History

on	History.SessionID = Session.ID
and	History.InterfaceCode = Session.InterfaceCode


--insert missing (cancelled) sessions
insert into Theatre.LegacySessions
(
	[ID],
	AmendTime,
	PlannedAnaesthetist,
	ConsultantCode,
	EndTime,
	ProcedureDate,
	SessionType,
	StartTime,
	TheatreNumber,
	SubTheatre,
	CancelReasonCode,
	InterfaceCode,
	HistoryRecno,
	Reassigned
)

SELECT 
	[SessionID],
	[AmendTime],
	[Anaesthetist],
	[Consultant],
	[EndTime],
	[SessionDate],
	[SessionType],
	[StartTime],
	[Theatre],
	[Theatre],
	[CancelReason],
	InterfaceCode,
	EncounterRecno,
	0 Reassigned
FROM
	(
		select
			*
		from
			Theatre.History
		where
			not exists
			(
			select
				1
			from
				Theatre.History OriginalSession
			where
				OriginalSession.SessionID = History.SessionID
			and	OriginalSession.InterfaceCode = History.InterfaceCode
			and
				(
					coalesce(OriginalSession.AmendTime, '6/6/2079') > coalesce(History.AmendTime, '6/6/2079')
				or	(
					coalesce(OriginalSession.AmendTime, '6/6/2079') = coalesce(History.AmendTime, '6/6/2079')
				and	OriginalSession.[ID] > History.[ID]
					)
				)
			)
		
		and	History.CancelReason <> '49'
	) History

where
	not exists
	(
	select
		1
	from
		Theatre.LegacySessions Session
	where
		Session.ID = History.SessionID
	and	Session.InterfaceCode = History.InterfaceCode
	)
