﻿CREATE procedure [dbo].[BuildAPCBreach] as

/**********************************************************************************
Change Log:

	Current Version Date:	2011-03-15
	Change:
			Updated to add criteria for social suspensions,
			These should not be counted if the start date is
			in the future.
	

**********************************************************************************/



truncate table APC.Breach

insert into APC.Breach
	(
	 SourceUniqueID
	,ClockStartDate
	,BreachDate
	,RTTBreachDate
	,NationalBreachDate
	,NationalDiagnosticBreachDate
	,SocialSuspensionDays
	,BreachTypeCode
	)
select
	 SourceUniqueID

	,ClockStartDate =
		DATEADD(day, datediff(day, 0, ClockStartDate), 0)

	,BreachDate =
		DATEADD(
			 day
			,datediff(
				 day
				,0
				,case
				when BreachTypeCode = 'RTT'
				then RTTBreachDate
				when BreachTypeCode = 'DIAG'
				then NationalDiagnosticBreachDate
				else NationalBreachDate
				end
			)
			,0
		)

	,RTTBreachDate =
		DATEADD(day, datediff(day, 0, RTTBreachDate), 0)

	,NationalBreachDate =
		DATEADD(day, datediff(day, 0, NationalBreachDate), 0)

	,NationalDiagnosticBreachDate =
		DATEADD(day, datediff(day, 0, NationalDiagnosticBreachDate), 0)

	,SocialSuspensionDays
	,BreachTypeCode
from
	--calculate breach dates
	(
	select
		 ClockStart.SourceUniqueID
		,ClockStart.ClockStartDate

		,RTTBreachDate =
			dateadd(
				 day
				,(select NumericValue from dbo.Parameter where Parameter = 'RTTBREACHDAYS')
				 + ClockStart.SocialSuspensionDays
				 + ManualAdjustmentDays

				,ClockStart.ClockStartDate
			)

		,NationalBreachDate =
			dateadd(
				 day
				,(select NumericValue from dbo.Parameter where Parameter = 'NATIONALIPDEFAULTBREACHDAYS')
				 + ClockStart.SocialSuspensionDays
				 + ManualAdjustmentDays

				,ClockStart.ClockStartDate
			)

		,NationalDiagnosticBreachDate =
			dateadd(
				 day
				,(select NumericValue from dbo.Parameter where Parameter = 'NATIONALDIAGNOSTICBREACHDAYS')
				 + ClockStart.SocialSuspensionDays
				 + ManualAdjustmentDays

				,ClockStart.ClockStartDate
			)

		,SocialSuspensionDays =
			ClockStart.SocialSuspensionDays +
			ManualAdjustmentDays

		,BreachTypeCode =
			case
			when
				Diagnostic = 1
			then 'DIAG' --diagnostic breach
			when
				datediff(
					day
					,ClockStart.ClockStartDate
					,ClockStart.AdmissionDate
				) + (ClockStart.SocialSuspensionDays + ManualAdjustmentDays)
				> 
				(select NumericValue from dbo.Parameter where Parameter = 'BREACHTHRESHOLDDAYS')

			then 'NAT' --national breach threshold
			else 'RTT' --RTT breach (18 weeks)
			end

	from
		--ClockStart calculation
		(
		select
			 Encounter.AdmissionDate
			,Encounter.SourceUniqueID

			,ClockStartDate =
				coalesce(
--Added PDO 20120203 - (Log ID 6) Referrals with a status of 98 use date on waiting list
					case
					when exists
						(
						select
							1
						from
							RTT.RTT
						where 
							RTT.ReferralSourceuniqueID = Encounter.ReferralSourceUniqueID
						and RTT.StartDate < Encounter.AdmissionDate
						and RTT.RTTStatusCode = 3007094 --98 Not applicable to RTT
						)
					then OriginalDateOnWaitingList
					end

					,RTTClockStartDate
					,ManualClockStartDate
--Added KD 20111129
					,Case When Exists (
										Select 1 from RTT.RTT ThisRTT
										Inner Join RTT.RTTStatus ThisRTTStatus
										on ThisRTT.RTTStatusCode = ThisRTTStatus.InternalCode
										Where 
											ThisRTT.ReferralSourceuniqueID = Encounter.ReferralSourceUniqueID
										and ThisRTT.StartDate < Encounter.AdmissionDate
										and ThisRTTStatus.ClockStopFlag = 'True'
										)
					Then OriginalDateOnWaitingList
					End
					,OriginalProviderReferralDate
					,ReferralDate
					,OriginalDateOnWaitingList
				)

			,ManualAdjustmentDays

			,Diagnostic

			,SocialSuspensionDays =
				coalesce(
					(
					select
						 sum(datediff(day, SuspensionStartDate, coalesce(SuspensionEndDate, CensusDate))) 
					from
						APC.WaitingListSuspension Suspension
					where
						SuspensionReasonCode in
						(
						 4398	--Patient Away
						,4397	--Patient Request
						)

					and	Suspension.SuspensionStartDate >=
							coalesce(
								 OriginalProviderReferralDate
--Added PDO 20120203 - (Log ID 6) Referrals with a status of 98 use date on waiting list
								,case
								when exists
									(
									select
										1
									from
										RTT.RTT
									where 
										RTT.ReferralSourceuniqueID = Encounter.ReferralSourceUniqueID
									and RTT.StartDate < Encounter.AdmissionDate
									and RTT.RTTStatusCode = 3007094 --98 Not applicable to RTT
									)
								then OriginalDateOnWaitingList
								end

								,RTTClockStartDate
								,ManualClockStartDate
--Added KD 20111129
						,Case When Exists (
											Select 1 from RTT.RTT ThisRTT
											Inner Join RTT.RTTStatus ThisRTTStatus
											on ThisRTT.RTTStatusCode = ThisRTTStatus.InternalCode
											Where 
												ThisRTT.ReferralSourceuniqueID = Encounter.ReferralSourceUniqueID
											and ThisRTT.StartDate < Encounter.AdmissionDate
											and ThisRTTStatus.ClockStopFlag = 'True'
											)
						Then OriginalDateOnWaitingList
						Else Referraldate
						End
								,ReferralDate
								,OriginalDateOnWaitingList
							)
					--Added 2011-03-15 KD
					and Suspension.SuspensionStartDate <= Cast(Convert(varchar(8), getdate(),112) as datetime)
					and	Suspension.WaitingListSourceUniqueID = Encounter.SourceUniqueID
					and	Suspension.CensusDate = 
							(
							select
								max(Snapshot.CensusDate)
							from
								APC.Snapshot Snapshot
							where
								Snapshot.CensusDate <= Encounter.AdmissionDate
							)

					group by
						 Suspension.WaitingListSourceUniqueID

					)
					,0
				)

		from
			(
			select
				 Encounter.AdmissionDate
				,Encounter.SourceUniqueID
				,Encounter.WaitingListSourceUniqueID

		--various potential clock starts

--Any RTT code entered during a waiting list episode (start is WL addition and the 
--end is either an Admission or removal) should be ignored ie any 20 should not 
--restart and any 30 should not stop.

				,RTTClockStartDate =
				--get initial RTT clock start
					(
					select
						 RTTClockStartDate = min(RTT.StartDate)
					from
						RTT.RTT

					inner join RTT.RTTStatus RTTStatus
					on	RTTStatus.InternalCode = RTT.RTTStatusCode

					where
						RTTStatus.ClockStartFlag = 'true'
					and	RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
					)

				,ManualClockStartDate =
					coalesce(
						 APCWLManualClock.ClockStartDate
						,APCManualClock.ClockStartDate
					)

				,OriginalProviderReferralDate =
					case
					when datepart(year, RFEncounter.OriginalProviderReferralDate) <= 1948
					then null
					else RFEncounter.OriginalProviderReferralDate
					end

				,ReferralDate =
--2011-11-21 Added check for RefToCarer
					Case 
					When RefToCarer.ProfessionalCarerCode is null 
					Then null 
					else 
						case
						when datepart(year, RFEncounter.ReferralDate) = 1948
						then null
						else RFEncounter.ReferralDate
						end
					End

				,Encounter.ReferralSourceUniqueID
				,OriginalDateOnWaitingList = Encounter.DateOnWaitingList

				,ManualAdjustmentDays =
					coalesce(APCWLManualAdjustment.AdjustmentDays, 0) +
					coalesce(APCManualAdjustment.AdjustmentDays, 0)

				,Diagnostic =
					case
					when
						DiagnosticProcedure.ProcedureCode is null
			--		and	upper(Encounter.Operation) not like '%$T%'
					then 0
					else 1
					end
			from
				APC.Encounter Encounter

			left join RF.Encounter RFEncounter
			on	RFEncounter.SourceUniqueID = Encounter.ReferralSourceUniqueID

--2011-11-21 KD
			left join PAS.ProfessionalCarer RefToCarer
			on RFEncounter.ConsultantCode = RefToCarer.ProfessionalCarerCode
			and 
				(
				RefToCarer.ProfessionalCarerCode = 10539696  -- Nurse led plastics
				or RefToCarer.ProfessionalCarerTypeCode = 1130 -- Consultant
				)

		--get manual adjustments applied to this wait
			left join
			(
			select
				 SourceUniqueID = SourceRecno
				,AdjustmentDays = sum(AdjustmentDays)
			from
				RTT.Adjustment Adjustment
			where
				Adjustment.SourceCode = 'APCWL'
			group by
				 SourceRecno
			) APCWLManualAdjustment
			on	APCWLManualAdjustment.SourceUniqueID = Encounter.SourceUniqueID

		--get manual adjustments applied to this encounter
			left join 
			(
			select
				 SourceUniqueID = SourceRecno
				,AdjustmentDays = sum(AdjustmentDays)
			from
				RTT.Adjustment Adjustment
			where
				Adjustment.SourceCode = 'APC'
			group by
				 SourceRecno
			) APCManualAdjustment
			on	APCManualAdjustment.SourceUniqueID = Encounter.SourceUniqueID

			left join RTT.Encounter APCManualClock
			on	APCManualClock.SourceRecno = Encounter.SourceUniqueID
			and	APCManualClock.SourceCode = 'APC'

			left join RTT.Encounter APCWLManualClock
			on	APCWLManualClock.SourceRecno = Encounter.SourceUniqueID
			and	APCWLManualClock.SourceCode = 'APCWL'

			left join DiagnosticProcedure DiagnosticProcedure
			on	DiagnosticProcedure.ProcedureCode = Encounter.PrimaryOperationCode

			) Encounter
		) ClockStart
	) Final

