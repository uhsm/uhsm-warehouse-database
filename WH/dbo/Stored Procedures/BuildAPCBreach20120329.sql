﻿create procedure [dbo].[BuildAPCBreach20120329] as

/**********************************************************************************
Change Log:

	Current Version Date:	2011-03-15
	Change:
			Updated to add criteria for social suspensions,
			These should not be counted if the start date is
			in the future.
	

**********************************************************************************/



truncate table APC.Breach

insert into APC.Breach
	(
	 SourceUniqueID
	,ClockStartDate
	,BreachDate
	,RTTBreachDate
	,NationalBreachDate
	,NationalDiagnosticBreachDate
	,SocialSuspensionDays
	,BreachTypeCode
	)
select
	 SourceUniqueID

	,ClockStartDate =
		DATEADD(day, datediff(day, 0, ClockStartDate), 0)

	,BreachDate =
		DATEADD(
			 day
			,datediff(
				 day
				,0
				,case
				when BreachTypeCode = 'RTT'
				then RTTBreachDate
				when BreachTypeCode = 'DIAG'
				then NationalDiagnosticBreachDate
				else NationalBreachDate
				end
			)
			,0
		)

	,RTTBreachDate =
		DATEADD(day, datediff(day, 0, RTTBreachDate), 0)

	,NationalBreachDate =
		DATEADD(day, datediff(day, 0, NationalBreachDate), 0)

	,NationalDiagnosticBreachDate =
		DATEADD(day, datediff(day, 0, NationalDiagnosticBreachDate), 0)

	,SocialSuspensionDays
	,BreachTypeCode
from
	--calculate breach dates
	(
	select
		 ClockStart.SourceUniqueID
		,ClockStart.ClockStartDate

		,RTTBreachDate =
			dateadd(
				 day
				,(select NumericValue from dbo.Parameter where Parameter = 'RTTBREACHDAYS')
				 + ClockStart.SocialSuspensionDays
				 + ManualAdjustmentDays

				,ClockStart.ClockStartDate
			)

		,NationalBreachDate =
			dateadd(
				 day
				,(select NumericValue from dbo.Parameter where Parameter = 'NATIONALIPDEFAULTBREACHDAYS')
				 + ClockStart.SocialSuspensionDays
				 + ManualAdjustmentDays

				,ClockStart.ClockStartDate
			)

		,NationalDiagnosticBreachDate =
			dateadd(
				 day
				,(select NumericValue from dbo.Parameter where Parameter = 'NATIONALDIAGNOSTICBREACHDAYS')
				 + ClockStart.SocialSuspensionDays
				 + ManualAdjustmentDays

				,ClockStart.ClockStartDate
			)

		,SocialSuspensionDays =
			ClockStart.SocialSuspensionDays +
			ManualAdjustmentDays

		,BreachTypeCode =
			case
			when
				Diagnostic = 1
			then 'DIAG' --diagnostic breach
			when
				datediff(
					day
					,ClockStart.ClockStartDate
					,ClockStart.AdmissionDate
				) + (ClockStart.SocialSuspensionDays + ManualAdjustmentDays)
				> 
				(select NumericValue from dbo.Parameter where Parameter = 'BREACHTHRESHOLDDAYS')

			then 'NAT' --national breach threshold
			else 'RTT' --RTT breach (18 weeks)
			end

	from
		--ClockStart calculation
		(
		select
			 Encounter.AdmissionDate
			,Encounter.SourceUniqueID
			,ClockStartDate =
				coalesce(
					 OriginalProviderReferralDate
					,RTTClockStartDate
					,ManualClockStartDate
					,ReferralDate
					,DateOnWaitingList
				)

			,ManualAdjustmentDays

			,Diagnostic

			,SocialSuspensionDays =
				coalesce(
					(
					select
						 sum(datediff(day, SuspensionStartDate, coalesce(SuspensionEndDate, CensusDate))) 
					from
						APC.WaitingListSuspension Suspension
					where
						SuspensionReasonCode in
						(
						 4398	--Patient Away
						,4397	--Patient Request
						)
					and	Suspension.SuspensionStartDate >=
							coalesce(
								 OriginalProviderReferralDate
								,RTTClockStartDate
								,ManualClockStartDate
								,ReferralDate
								,DateOnWaitingList
							)
					--Added 2011-03-15 KD
					and Suspension.SuspensionStartDate <= Cast(Convert(varchar(8), getdate(),112) as datetime)
					and	Suspension.WaitingListSourceUniqueID = Encounter.WaitingListSourceUniqueID
					and	Suspension.CensusDate = 
							(
							select
								max(Snapshot.CensusDate)
							from
								APC.Snapshot Snapshot
							where
								Snapshot.CensusDate <= Encounter.AdmissionDate
							)

					group by
						 Suspension.WaitingListSourceUniqueID
					)
					,0
				)

		from
			(
			select
				 Encounter.AdmissionDate
				,Encounter.SourceUniqueID
				,Encounter.WaitingListSourceUniqueID

		--various potential clock starts

				,RTTClockStartDate =
				--get most recent RTT clock start
					(
					select
						MAX(RTTClockStartDate)
					from
						(
						select
							 RTTClockStartDate = max(RTT.StartDate)
						from
							RTT.RTT

						inner join RTT.RTTStatus RTTStatus
						on	RTTStatus.InternalCode = RTT.RTTStatusCode

						where
							RTTStatus.ClockStartFlag = 'true'
						and	RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
						and	RTT.StartDate < Encounter.AdmissionDate

						union all

					--most recent RTT clock restart
					--if a clock restart (10, 20) follows a clock trigger (3%, 9%(ish)) then use the start date of the earliest restart
						select
							 RTTClockStartDate = min(RTT.StartDate)
						from
							RTT.RTT

						inner join RTT.RTTStatus RTTStatus
						on	RTTStatus.InternalCode = RTT.RTTStatusCode

						where
							RTTStatus.ClockRestartFlag = 'true'
						and	RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
						and	RTT.StartDate <= Encounter.AdmissionDate
						and	exists
							(
							select
								1
							from
								RTT.RTT PreviousClockStop

							inner join RTT.RTTStatus RTTStatus
							on	RTTStatus.InternalCode = PreviousClockStop.RTTStatusCode

							where
								PreviousClockStop.ReferralSourceUniqueID = RTT.ReferralSourceUniqueID
							and	PreviousClockStop.StartDate < RTT.StartDate
							and	RTTStatus.ClockRestartTriggerFlag = 'true'
							--and	not exists
							--	(
							--	select
							--		1
							--	from
							--		RTT.RTT Previous
							--	where
							--		Previous.ReferralSourceUniqueID = PreviousClockStop.ReferralSourceUniqueID
							--	and	Previous.StartDate < RTT.StartDate
							--	and	(
							--			Previous.StartDate < PreviousClockStop.StartDate
							--		or	(
							--				Previous.StartDate = PreviousClockStop.StartDate
							--			and	Previous.EncounterRecno < PreviousClockStop.EncounterRecno
							--			)
							--		)
							--	)
							)

						) ClockStart
					)

				,ManualClockStartDate =
					coalesce(
						 APCWLManualClock.ClockStartDate
						,APCManualClock.ClockStartDate
					)

				,OriginalProviderReferralDate =
					case
					when datepart(year, RFEncounter.OriginalProviderReferralDate) <= 1948
					then null
					else RFEncounter.OriginalProviderReferralDate
					end

				,ReferralDate =
					case
					when datepart(year, RFEncounter.ReferralDate) = 1948
					then null
					else RFEncounter.ReferralDate
					end

				,Encounter.DateOnWaitingList

				,ManualAdjustmentDays =
					coalesce(APCWLManualAdjustment.AdjustmentDays, 0) +
					coalesce(APCManualAdjustment.AdjustmentDays, 0)

				,Diagnostic =
					case
					when
						DiagnosticProcedure.ProcedureCode is null
			--		and	upper(Encounter.Operation) not like '%$T%'
					then 0
					else 1
					end

			from
				APC.Encounter Encounter

			left join RF.Encounter RFEncounter
			on	RFEncounter.SourceUniqueID = Encounter.ReferralSourceUniqueID

		--get manual adjustments applied to this wait
			left join
			(
			select
				 SourceUniqueID = SourceRecno
				,AdjustmentDays = sum(AdjustmentDays)
			from
				RTT.Adjustment Adjustment
			where
				Adjustment.SourceCode = 'APCWL'
			group by
				 SourceRecno
			) APCWLManualAdjustment
			on	APCWLManualAdjustment.SourceUniqueID = Encounter.SourceUniqueID

		--get manual adjustments applied to this encounter
			left join 
			(
			select
				 SourceUniqueID = SourceRecno
				,AdjustmentDays = sum(AdjustmentDays)
			from
				RTT.Adjustment Adjustment
			where
				Adjustment.SourceCode = 'APC'
			group by
				 SourceRecno
			) APCManualAdjustment
			on	APCManualAdjustment.SourceUniqueID = Encounter.SourceUniqueID

			left join RTT.Encounter APCManualClock
			on	APCManualClock.SourceRecno = Encounter.SourceUniqueID
			and	APCManualClock.SourceCode = 'APC'

			left join RTT.Encounter APCWLManualClock
			on	APCWLManualClock.SourceRecno = Encounter.SourceUniqueID
			and	APCWLManualClock.SourceCode = 'APCWL'

			left join DiagnosticProcedure DiagnosticProcedure
			on	DiagnosticProcedure.ProcedureCode = Encounter.PrimaryOperationCode

			) Encounter
		) ClockStart
	) Final

