﻿CREATE procedure [dbo].[BuildAPCWaitingListBreach]
	@CensusDate date
as

/**********************************************************************************
Change Log:
	2011-11-29 KD
	Change:
			Added below to coalesce statement for clockstartdate to pick up waiting start date for 2nd pathways
			Case When (RTTClockStartDate Is null and DateOnWaitingList > ReferralDate) Then DateOnWaitingList Else Null End

	2011-11-21 KD
	Change: 
			Added Link To PAS.ProfessionalCarer on RF.Encounter to get referred to 
			Professional carer type


	2011-03-15
	Change:
			Updated to add criteria for social suspensions,
			These should not be counted if the start date is
			in the future.
	
	2012-05-02 PDO
	Change:
			Broke query into multiple, function specific views to simplify debug
	
	2012-07-23 PDO
	Change:
			Added @CensusDate parameter to allow generation of census specific data

**********************************************************************************/

delete
from
	APC.WaitingListBreach
where
	CensusDate = @CensusDate

insert into APC.WaitingListBreach
	(
	 CensusDate
	,SourceUniqueID
	,ClockStartDate
	,BreachDate
	,RTTBreachDate
	,NationalBreachDate
	,NationalDiagnosticBreachDate
	,SocialSuspensionDays
	,BreachTypeCode
	)
select
	 CensusDate
	,SourceUniqueID

	,ClockStartDate =
		cast(ClockStartDate as date)

	,BreachDate =
		CAST(
			case
			when BreachTypeCode = 'RTT'
			then RTTBreachDate
			when BreachTypeCode = 'DIAG'
			then NationalDiagnosticBreachDate
			else NationalBreachDate
			end

			as date
		)

	,RTTBreachDate =
		cast(RTTBreachDate as date)

	,NationalBreachDate =
		cast(NationalBreachDate as date)

	,NationalDiagnosticBreachDate =
		cast(NationalDiagnosticBreachDate as date)

	,SocialSuspensionDays
	,BreachTypeCode
from
	--calculate breach dates
	(
	select
		 ClockStart.CensusDate
		,ClockStart.SourceUniqueID
		,ClockStart.ClockStartDate

		,RTTBreachDate =
			dateadd(
				 day
				,(select NumericValue from dbo.Parameter where Parameter = 'RTTBREACHDAYS')
				 + ClockStart.SocialSuspensionDays
				 + ManualAdjustmentDays

				,ClockStart.ClockStartDate
			)

		,NationalBreachDate =
			dateadd(
				 day
				,(select NumericValue from dbo.Parameter where Parameter = 'NATIONALIPDEFAULTBREACHDAYS')
				 + ClockStart.SocialSuspensionDays
				 + ManualAdjustmentDays

				,ClockStart.ClockStartDate
			)

		,NationalDiagnosticBreachDate =
			dateadd(
				 day
				,(select NumericValue from dbo.Parameter where Parameter = 'NATIONALDIAGNOSTICBREACHDAYS')
				 + ClockStart.SocialSuspensionDays
				 + ManualAdjustmentDays

				,ClockStart.ClockStartDate
			)

		,SocialSuspensionDays =
			ClockStart.SocialSuspensionDays +
			ManualAdjustmentDays

		,BreachTypeCode =
			case
			when
				Diagnostic = 1
			then 'DIAG' --diagnostic breach
			when
				datediff(
					day
					,ClockStart.ClockStartDate
					,ClockStart.CensusDate
				) + (ClockStart.SocialSuspensionDays + ManualAdjustmentDays)
				> 
				(select NumericValue from dbo.Parameter where Parameter = 'BREACHTHRESHOLDDAYS')

			then 'NAT' --national breach threshold
			else 'RTT' --RTT breach (18 weeks)
			end

	from
		dbo.APCWaitingListBreachPart2 ClockStart
	) Final

where
	Final.CensusDate = @CensusDate
