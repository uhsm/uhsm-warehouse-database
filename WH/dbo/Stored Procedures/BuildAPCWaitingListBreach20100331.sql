﻿create procedure [dbo].[BuildAPCWaitingListBreach20100331] as

truncate table APC.WaitingListBreach

insert into APC.WaitingListBreach
	(
	 CensusDate
	,SourceUniqueID
	,ClockStartDate
	,BreachDate
	,RTTBreachDate
	,NationalBreachDate
	,NationalDiagnosticBreachDate
	,SocialSuspensionDays
	,BreachTypeCode
	)
select
	 CensusDate
	,SourceUniqueID

	,ClockStartDate =
		DATEADD(day, datediff(day, 0, ClockStartDate), 0)

	,BreachDate =
		DATEADD(
			 day
			,datediff(
				 day
				,0
				,case
				when BreachTypeCode = 'RTT'
				then RTTBreachDate
				when BreachTypeCode = 'DIAG'
				then NationalDiagnosticBreachDate
				else NationalBreachDate
				end
			)
			,0
		)

	,RTTBreachDate =
		DATEADD(day, datediff(day, 0, RTTBreachDate), 0)

	,NationalBreachDate =
		DATEADD(day, datediff(day, 0, NationalBreachDate), 0)

	,NationalDiagnosticBreachDate =
		DATEADD(day, datediff(day, 0, NationalDiagnosticBreachDate), 0)

	,SocialSuspensionDays
	,BreachTypeCode
from
	(
	select
		 CensusDate
		,SourceUniqueID
		,ClockStartDate =
			coalesce(
				 OriginalProviderReferralDate
				,RTTClockStartDate
				,ManualClockStartDate
				,ReferralDate
				,DateOnWaitingList
			)


		,RTTBreachDate =
			dateadd(
				 day
				,(select NumericValue from dbo.Parameter where Parameter = 'RTTBREACHDAYS')
				 - SocialSuspensionDays
				 - ManualAdjustmentDays

				,coalesce(
					 OriginalProviderReferralDate
					,RTTClockStartDate
					,ManualClockStartDate
					,ReferralDate
					,DateOnWaitingList
				)
			)

		,NationalBreachDate =
			dateadd(
				 day
				,(select NumericValue from dbo.Parameter where Parameter = 'NATIONALIPDEFAULTBREACHDAYS')
				 - SocialSuspensionDays
				 - ManualAdjustmentDays

				,coalesce(
					 OriginalProviderReferralDate
					,RTTClockStartDate
					,ManualClockStartDate
					,ReferralDate
					,DateOnWaitingList
				)
			)

		,NationalDiagnosticBreachDate =
			dateadd(
				 day
				,(select NumericValue from dbo.Parameter where Parameter = 'NATIONALDIAGNOSTICBREACHDAYS')
				 - SocialSuspensionDays
				 - ManualAdjustmentDays

				,coalesce(
					 OriginalProviderReferralDate
					,RTTClockStartDate
					,ManualClockStartDate
					,ReferralDate
					,DateOnWaitingList
				)
			)

		,SocialSuspensionDays =
			SocialSuspensionDays +
			ManualAdjustmentDays

		,BreachTypeCode =
			case
			when
				Diagnostic = 1
			then 'DIAG' --diagnostic breach
			when
				datediff(
					day
					,coalesce(
						 OriginalProviderReferralDate
						,RTTClockStartDate
						,ManualClockStartDate
						,ReferralDate
						,DateOnWaitingList
					)
					,CensusDate
				) - (SocialSuspensionDays + ManualAdjustmentDays)
				> 
				(select NumericValue from dbo.Parameter where Parameter = 'BREACHTHRESHOLDDAYS')

			then 'NAT' --national breach threshold
			else 'RTT' --RTT breach (18 weeks)
			end

	from
		(
		select
			 Encounter.CensusDate
			,Encounter.SourceUniqueID

	--various potential clock starts

			,RTTClockStartDate =
			--get most recent RTT clock start
				(
				select
					MAX(RTTClockStartDate)
				from
					(
					select
						 RTTClockStartDate = max(RTT.StartDate)
					from
						RTT.RTT

					inner join RTT.RTTStatus RTTStatus
					on	RTTStatus.InternalCode = RTT.RTTStatusCode

					where
						RTTStatus.ClockStartFlag = 'true'
					and	RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
					and	RTT.StartDate < Encounter.CensusDate

					union all

				--most recent RTT clock restart
					select
						 RTTClockStartDate = min(RTT.StartDate)
					from
						RTT.RTT

					inner join RTT.RTTStatus RTTStatus
					on	RTTStatus.InternalCode = RTT.RTTStatusCode

					where
						RTTStatus.ClockRestartFlag = 'true'
					and	RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
					and	RTT.StartDate <= Encounter.CensusDate
					and	exists
						(
						select
							1
						from
							RTT.RTT PreviousClockStop

						inner join RTT.RTTStatus RTTStatus
						on	RTTStatus.InternalCode = PreviousClockStop.RTTStatusCode

						where
							PreviousClockStop.ReferralSourceUniqueID = RTT.ReferralSourceUniqueID
						and	PreviousClockStop.StartDate < RTT.StartDate
						and	RTTStatus.ClockRestartTriggerFlag = 'true'
						and	not exists
							(
							select
								1
							from
								RTT.RTT Previous
							where
								Previous.ReferralSourceUniqueID = PreviousClockStop.ReferralSourceUniqueID
							and	Previous.StartDate < RTT.StartDate
							and	(
									Previous.StartDate < PreviousClockStop.StartDate
								or	(
										Previous.StartDate = PreviousClockStop.StartDate
									and	Previous.EncounterRecno < PreviousClockStop.EncounterRecno
									)
								)
							)
						)

					) ClockStart
				)

			,ManualClockStartDate =
				coalesce(
					 APCWLManualClock.ClockStartDate
					,APCManualClock.ClockStartDate
				)

			,OriginalProviderReferralDate =
				case
				when datepart(year, RFEncounter.OriginalProviderReferralDate) <= 1948
				then null
				else RFEncounter.OriginalProviderReferralDate
				end

			,ReferralDate =
				case
				when datepart(year, RFEncounter.ReferralDate) = 1948
				then null
				else RFEncounter.ReferralDate
				end

			,Encounter.DateOnWaitingList

			,SocialSuspensionDays = 
				coalesce(
					(
					select
						sum(datediff(day, SuspensionStartDate, coalesce(SuspensionEndDate, CensusDate))) DaysSuspended
					from
						APC.WaitingListSuspension Suspension
					where
						SuspensionReasonCode in
						(
						 4398	--Patient Away
						,4397	--Patient Request
						)
					and	Suspension.SuspensionStartDate >= 
						--get most recent RTT clock start
						(
						select
							MAX(RTTClockStartDate)
						from
							(
							select
								 RTTClockStartDate = max(RTT.StartDate)
							from
								RTT.RTT

							inner join RTT.RTTStatus RTTStatus
							on	RTTStatus.InternalCode = RTT.RTTStatusCode

							where
								RTTStatus.ClockStartFlag = 'true'
							and	RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
							and	RTT.StartDate < Encounter.CensusDate

							union all

						--most recent RTT clock restart
							select
								 RTTClockStartDate = min(RTT.StartDate)
							from
								RTT.RTT

							inner join RTT.RTTStatus RTTStatus
							on	RTTStatus.InternalCode = RTT.RTTStatusCode

							where
								RTTStatus.ClockRestartFlag = 'true'
							and	RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
							and	RTT.StartDate <= Encounter.CensusDate
							and	exists
								(
								select
									1
								from
									RTT.RTT PreviousClockStop

								inner join RTT.RTTStatus RTTStatus
								on	RTTStatus.InternalCode = PreviousClockStop.RTTStatusCode

								where
									PreviousClockStop.ReferralSourceUniqueID = RTT.ReferralSourceUniqueID
								and	PreviousClockStop.StartDate < RTT.StartDate
								and	RTTStatus.ClockRestartTriggerFlag = 'true'
								and	not exists
									(
									select
										1
									from
										RTT.RTT Previous
									where
										Previous.ReferralSourceUniqueID = PreviousClockStop.ReferralSourceUniqueID
									and	Previous.StartDate < RTT.StartDate
									and	(
											Previous.StartDate < PreviousClockStop.StartDate
										or	(
												Previous.StartDate = PreviousClockStop.StartDate
											and	Previous.EncounterRecno < PreviousClockStop.EncounterRecno
											)
										)
									)
								)

							) ClockStart
						)
					and	Suspension.WaitingListSourceUniqueID = Encounter.SourceUniqueID
					and	Suspension.CensusDate = 
						(
						select
							max(Snapshot.CensusDate)
						from
							APC.Snapshot Snapshot
						where
							Snapshot.CensusDate <= Encounter.CensusDate
						)

					group by
						 WaitingListSourceUniqueID
						,CensusDate
					)
				, 0
				)

			,ManualAdjustmentDays =
				coalesce(APCWLManualAdjustment.AdjustmentDays, 0) +
				coalesce(APCManualAdjustment.AdjustmentDays, 0)

			,Diagnostic =
				case
				when
					DiagnosticProcedure.ProcedureCode is null
		--		and	upper(Encounter.Operation) not like '%$T%'
				then 0
				else 1
				end

		from
			APC.WaitingList Encounter

		left join RF.Encounter RFEncounter
		on	RFEncounter.SourceUniqueID = Encounter.ReferralSourceUniqueID

	--get manual adjustments applied to this wait
		left join
		(
		select
			 SourceUniqueID = SourceRecno
			,AdjustmentDays = sum(AdjustmentDays)
		from
			RTT.Adjustment Adjustment
		where
			Adjustment.SourceCode = 'APCWL'
		group by
			 SourceRecno
		) APCWLManualAdjustment
		on	APCWLManualAdjustment.SourceUniqueID = Encounter.SourceUniqueID

	--get manual adjustments applied to this encounter
		left join 
		(
		select
			 SourceUniqueID = SourceRecno
			,AdjustmentDays = sum(AdjustmentDays)
		from
			RTT.Adjustment Adjustment
		where
			Adjustment.SourceCode = 'APC'
		group by
			 SourceRecno
		) APCManualAdjustment
		on	APCManualAdjustment.SourceUniqueID = Encounter.SourceUniqueID

		left join RTT.Encounter APCManualClock
		on	APCManualClock.SourceRecno = Encounter.SourceUniqueID
		and	APCManualClock.SourceCode = 'APC'

		left join RTT.Encounter APCWLManualClock
		on	APCWLManualClock.SourceRecno = Encounter.SourceUniqueID
		and	APCWLManualClock.SourceCode = 'APCWL'

		left join DiagnosticProcedure DiagnosticProcedure
		on	DiagnosticProcedure.ProcedureCode = Encounter.IntendedPrimaryOperationCode

		) Detail
	) Final
