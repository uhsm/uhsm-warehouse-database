﻿CREATE procedure [dbo].[BuildAPCWaitingListBreach21120502] as

/**********************************************************************************
Change Log:
	2011-11-29 KD
	Change:
			Added below to coalesce statement for clockstartdate to pick up waiting start date for 2nd pathways
			Case When (RTTClockStartDate Is null and DateOnWaitingList > ReferralDate) Then DateOnWaitingList Else Null End

	2011-11-21 KD
	Change: 
			Added Link To PAS.ProfessionalCarer on RF.Encounter to get referred to 
			Professional carer type


	2011-03-15
	Change:
			Updated to add criteria for social suspensions,
			These should not be counted if the start date is
			in the future.
	

**********************************************************************************/

truncate table APC.WaitingListBreach

insert into APC.WaitingListBreach
	(
	 CensusDate
	,SourceUniqueID
	,ClockStartDate
	,BreachDate
	,RTTBreachDate
	,NationalBreachDate
	,NationalDiagnosticBreachDate
	,SocialSuspensionDays
	,BreachTypeCode
	)
select
	 CensusDate
	,SourceUniqueID

	,ClockStartDate =
		DATEADD(day, datediff(day, 0, ClockStartDate), 0)

	,BreachDate =
		DATEADD(
			 day
			,datediff(
				 day
				,0
				,case
				when BreachTypeCode = 'RTT'
				then RTTBreachDate
				when BreachTypeCode = 'DIAG'
				then NationalDiagnosticBreachDate
				else NationalBreachDate
				end
			)
			,0
		)

	,RTTBreachDate =
		DATEADD(day, datediff(day, 0, RTTBreachDate), 0)

	,NationalBreachDate =
		DATEADD(day, datediff(day, 0, NationalBreachDate), 0)

	,NationalDiagnosticBreachDate =
		DATEADD(day, datediff(day, 0, NationalDiagnosticBreachDate), 0)

	,SocialSuspensionDays
	,BreachTypeCode
from
	--calculate breach dates
	(
	select
		 ClockStart.CensusDate
		,ClockStart.SourceUniqueID
		,ClockStart.ClockStartDate

		,RTTBreachDate =
			dateadd(
				 day
				,(select NumericValue from dbo.Parameter where Parameter = 'RTTBREACHDAYS')
				 + ClockStart.SocialSuspensionDays
				 + ManualAdjustmentDays

				,ClockStart.ClockStartDate
			)

		,NationalBreachDate =
			dateadd(
				 day
				,(select NumericValue from dbo.Parameter where Parameter = 'NATIONALIPDEFAULTBREACHDAYS')
				 + ClockStart.SocialSuspensionDays
				 + ManualAdjustmentDays

				,ClockStart.ClockStartDate
			)

		,NationalDiagnosticBreachDate =
			dateadd(
				 day
				,(select NumericValue from dbo.Parameter where Parameter = 'NATIONALDIAGNOSTICBREACHDAYS')
				 + ClockStart.SocialSuspensionDays
				 + ManualAdjustmentDays

				,ClockStart.ClockStartDate
			)

		,SocialSuspensionDays =
			ClockStart.SocialSuspensionDays +
			ManualAdjustmentDays

		,BreachTypeCode =
			case
			when
				Diagnostic = 1
			then 'DIAG' --diagnostic breach
			when
				datediff(
					day
					,ClockStart.ClockStartDate
					,ClockStart.CensusDate
				) + (ClockStart.SocialSuspensionDays + ManualAdjustmentDays)
				> 
				(select NumericValue from dbo.Parameter where Parameter = 'BREACHTHRESHOLDDAYS')

			then 'NAT' --national breach threshold
			else 'RTT' --RTT breach (18 weeks)
			end

	from
		--ClockStart calculation
		(
		select
			 Encounter.CensusDate
			,Encounter.SourceUniqueID

			,ClockStartDate =
				coalesce(
--Added PDO 20120203 - (Log ID 6) Referrals with a status of 98 use date on waiting list
					case
					when exists
						(
						select
							1
						from
							RTT.RTT
						where 
							RTT.ReferralSourceuniqueID = Encounter.ReferralSourceUniqueID
						and RTT.StartDate < Encounter.CensusDate
						and RTT.RTTStatusCode = 3007094 --98 Not applicable to RTT
						)
					then OriginalDateOnWaitingList
					end

					,RTTClockStartWhereClockStopFollowsRestart
					,Encounter.RTTClockStartDate
					,Encounter.ManualClockStartDate
--Added KD 20111129
					,Case When Exists (
										Select 1 from RTT.RTT ThisRTT
										Inner Join RTT.RTTStatus ThisRTTStatus
										on ThisRTT.RTTStatusCode = ThisRTTStatus.InternalCode
										Where 
											ThisRTT.ReferralSourceuniqueID = Encounter.ReferralSourceUniqueID
										and ThisRTT.StartDate < Encounter.CensusDate
										and ThisRTTStatus.ClockStopFlag = 'True'
										)
					Then Encounter.OriginalDateOnWaitingList
					End
					,Encounter.OriginalProviderReferralDate
					,Encounter.ReferralDate
					,Encounter.OriginalDateOnWaitingList
				)

			,ManualAdjustmentDays

			,Diagnostic

			,SocialSuspensionDays =
				coalesce(
					(
					select
						 sum(datediff(day, SuspensionStartDate, coalesce(SuspensionEndDate, CensusDate))) 
					from
						APC.WaitingListSuspension Suspension
					where
						SuspensionReasonCode in
						(
						 4398	--Patient Away
						,4397	--Patient Request
						)

					and	Suspension.SuspensionStartDate >=
							coalesce(
								 OriginalProviderReferralDate
--Added PDO 20120203 - (Log ID 6) Referrals with a status of 98 use date on waiting list
								,case
								when exists
									(
									select
										1
									from
										RTT.RTT
									where 
										RTT.ReferralSourceuniqueID = Encounter.ReferralSourceUniqueID
									and RTT.StartDate < Encounter.CensusDate
									and RTT.RTTStatusCode = 3007094 --98 Not applicable to RTT
									)
								then OriginalDateOnWaitingList
								end

								,RTTClockStartDate
								,ManualClockStartDate
--Added KD 20111129
						,Case When Exists (
											Select 1 from RTT.RTT ThisRTT
											Inner Join RTT.RTTStatus ThisRTTStatus
											on ThisRTT.RTTStatusCode = ThisRTTStatus.InternalCode
											Where 
												ThisRTT.ReferralSourceuniqueID = Encounter.ReferralSourceUniqueID
											and ThisRTT.StartDate < Encounter.CensusDate
											and ThisRTTStatus.ClockStopFlag = 'True'
											)
						Then OriginalDateOnWaitingList
						Else Referraldate
						End
								,ReferralDate
								,OriginalDateOnWaitingList
							)
					--Added 2011-03-15 KD
					and Suspension.SuspensionStartDate <= Cast(Convert(varchar(8), getdate(),112) as datetime)
					and	Suspension.WaitingListSourceUniqueID = Encounter.SourceUniqueID
					and	Suspension.CensusDate = 
							(
							select
								max(Snapshot.CensusDate)
							from
								APC.Snapshot Snapshot
							where
								Snapshot.CensusDate <= Encounter.CensusDate
							)

					group by
						 Suspension.WaitingListSourceUniqueID
						,Suspension.CensusDate
					)
					,0
				)

		from
			(
			select
				 Encounter.CensusDate
				,Encounter.SourceUniqueID

--PDO 20120203 - Log ID 1
--Any RTT code entered during a waiting list episode (start is WL addition and the 
--end is either an Admission or removal) should be ignored ie any 20 should not 
--restart and any 30 should not stop.

--PDO 20120330
--re-introduced the logic to cope with clock restarts following a clock start trigger!

				,RTTClockStartDate =
					coalesce(
						(
						--get most recent RTT clock start
						select
							 RTTClockStartDate = min(RTT.StartDate)
						from
							RTT.RTT

						inner join RTT.RTTStatus RTTStatus
						on	RTTStatus.InternalCode = RTT.RTTStatusCode

						where
							RTTStatus.ClockStartFlag = 'true'
						and	RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
						)

						,(
						--if a clock restart (10, 20) follows a clock trigger (3%, 9%(ish)) then use the start date of the earliest restart
						select
							 RTTClockStartDate = min(RTT.StartDate)
						from
							RTT.RTT

						inner join RTT.RTTStatus RTTStatus
						on	RTTStatus.InternalCode = RTT.RTTStatusCode

						where
							RTTStatus.ClockRestartFlag = 'true'
						and	RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
						and	RTT.StartDate <= Encounter.CensusDate
						--and not (RTT.SourceCode = 'ADMOF' and RTT.RTTStatusCode = 3007096)
/*--------------------------------Begin Added KD Experimental to remove min issue for multiple stops-------------------------------*/
--If there are multiple restarts we only want to bring the first restart after census date
						and not exists
							(
							Select 1
							From RTT.RTT PreviousRestart
							inner join RTT.RTTStatus PreviousRestartStatus
								on PreviousRestart.RTTStatusCode = PreviousRestartStatus.InternalCode
							Where 
								PreviousRestart.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
							and PreviousRestartStatus.ClockRestartFlag = 'True'
							and	PreviousRestart.StartDate <= Encounter.CensusDate
--PDO 02 Feb 2012 - amended following to bring back first rather than last 
							--and PreviousRestart.StartDate > RTT.StartDate
							and PreviousRestart.StartDate < RTT.StartDate
							)
/*--------------------------------End Added KD Experimental to remove min issue for multiple stops-------------------------------*/
						and	exists
							(
							select
								1
							from
								RTT.RTT PreviousClockRestart

							inner join RTT.RTTStatus RTTStatus
							on	RTTStatus.InternalCode = PreviousClockRestart.RTTStatusCode

							where
								PreviousClockRestart.ReferralSourceUniqueID = RTT.ReferralSourceUniqueID
							and	PreviousClockRestart.StartDate < RTT.StartDate
							and	RTTStatus.ClockRestartTriggerFlag = 'true'
/*--------------------------------Begin Added KD Experimental to remove issue where 90 then 20 not preceeded by 30----------------*/
-- For a clock restart trigger flag to have an effect there must first be a clock stop
-- e.g. if there is a 90 followed by a 20 but there is no 30 then it must not be a restart
-- ? Is this really a DQ issue?
							and Exists 
								(
								Select 1
								from RTT.RTT PreviousClockStop
								Where 
								PreviousClockStop.ReferralSourceUniqueID = RTT.ReferralSourceUniqueID
								and	PreviousClockStop.StartDate < RTT.StartDate
								and	RTTStatus.ClockStopFlag = 'true'
								)
/*--------------------------------End Added KD Experimental to remove issue where 90 then 20 not preceeded by 30----------------*/

							--and	not exists
							--	(
							--	select
							--		1
							--	from
							--		RTT.RTT Previous
							--	where
							--		Previous.ReferralSourceUniqueID = PreviousClockRestart.ReferralSourceUniqueID
							--	and	Previous.StartDate < RTT.StartDate
							--	and	(
							--			Previous.StartDate < PreviousClockRestart.StartDate
							--		or	(
							--				Previous.StartDate = PreviousClockRestart.StartDate
							--			and	Previous.EncounterRecno < PreviousClockRestart.EncounterRecno
							--			)
							--		)
							--	)
							)
						)

					)


				,RTTClockStartWhereClockStopFollowsRestart =
					(
					select
						Encounter.DateOnWaitingList
					from
						RTT.RTT

					inner join RTT.RTTStatus RTTStatus
					on	RTTStatus.InternalCode = RTT.RTTStatusCode
					and	RTTStatus.ClockStopFlag = 1

					where
						RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
					and	RTT.StartDate <= Encounter.CensusDate

					and	not exists
						(
						select
							1
						from
							RTT.RTT Previous

						inner join RTT.RTTStatus PreviousRTTStatus
						on	PreviousRTTStatus.InternalCode = Previous.RTTStatusCode
						and	PreviousRTTStatus.ClockStopFlag = 1

						where
							Previous.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
						and	Previous.StartDate <= Encounter.CensusDate

						and	(
								Previous.StartDate > RTT.StartDate
							or	(
									Previous.StartDate = RTT.StartDate
								and	Previous.EncounterRecno > RTT.EncounterRecno
								)
							)
						)

					and	exists
						(
						select
							1
						from
							RTT.RTT Previous

						inner join RTT.RTTStatus PreviousRTTStatus
						on	PreviousRTTStatus.InternalCode = Previous.RTTStatusCode
						and	PreviousRTTStatus.ClockRestartFlag = 1

						where
							Previous.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
						and	Previous.StartDate <= Encounter.CensusDate

						and	Previous.StartDate < RTT.StartDate
						)
					)

--		--various potential clock starts

--				,RTTClockStartDate =
--				--get most recent RTT clock start
--					(
--					select
--						MAX(RTTClockStartDate)
--					from
--						(
--						select
--							 RTTClockStartDate = max(RTT.StartDate)
--						from
--							RTT.RTT

--						inner join RTT.RTTStatus RTTStatus
--						on	RTTStatus.InternalCode = RTT.RTTStatusCode

--						where
--							RTTStatus.ClockStartFlag = 'true'
--						and	RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
--						and	RTT.StartDate < Encounter.CensusDate
--						--and not (RTT.SourceCode = 'ADMOF' and RTT.RTTStatusCode = 3007096)
--/*--------------------------------Begin Added KD Experimental to remove issue where 90 then 20 not preceeded by 30----------------*/
---- For a clock restart trigger flag to have an effect there must first be a clock stop
---- e.g. if there is a 90 followed by a 20 but there is no 30 then it must not be a restart
---- ? Is this really a DQ issue?
--						and Exists 
--							(
--							Select 1
--							from RTT.RTT PreviousClockStop
--							Where 
--							PreviousClockStop.ReferralSourceUniqueID = RTT.ReferralSourceUniqueID
--							and	PreviousClockStop.StartDate < RTT.StartDate
--							and	RTTStatus.ClockStopFlag = 'true'
--							)
--/*--------------------------------End Added KD Experimental to remove issue where 90 then 20 not preceeded by 30----------------*/

--						union all

--					--most recent RTT clock restart
--					--if a clock restart (10, 20) follows a clock trigger (3%, 9%(ish)) then use the start date of the earliest restart
--						select
--							 RTTClockStartDate = min(RTT.StartDate)
--						from
--							RTT.RTT

--						inner join RTT.RTTStatus RTTStatus
--						on	RTTStatus.InternalCode = RTT.RTTStatusCode

--						where
--							RTTStatus.ClockRestartFlag = 'true'
--						and	RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
--						and	RTT.StartDate <= Encounter.CensusDate
--						--and not (RTT.SourceCode = 'ADMOF' and RTT.RTTStatusCode = 3007096)
--/*--------------------------------Begin Added KD Experimental to remove min issue for multiple stops-------------------------------*/
----If there are multiple restarts we only want to bring the first restart after census date
--						and not exists
--							(
--							Select 1
--							From RTT.RTT PreviousRestart
--							inner join RTT.RTTStatus PreviousRestartStatus
--								on PreviousRestart.RTTStatusCode = PreviousRestartStatus.InternalCode
--							Where 
--								PreviousRestart.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
--							and PreviousRestartStatus.ClockRestartFlag = 'True'
--							and	PreviousRestart.StartDate <= Encounter.CensusDate
----PDO 02 Feb 2012 - amended following to bring back first rather than last 
--							--and PreviousRestart.StartDate > RTT.StartDate
--							and PreviousRestart.StartDate < RTT.StartDate
--							)
--/*--------------------------------End Added KD Experimental to remove min issue for multiple stops-------------------------------*/
--						and	exists
--							(
--							select
--								1
--							from
--								RTT.RTT PreviousClockRestart

--							inner join RTT.RTTStatus RTTStatus
--							on	RTTStatus.InternalCode = PreviousClockRestart.RTTStatusCode

--							where
--								PreviousClockRestart.ReferralSourceUniqueID = RTT.ReferralSourceUniqueID
--							and	PreviousClockRestart.StartDate < RTT.StartDate
--							and	RTTStatus.ClockRestartTriggerFlag = 'true'
--/*--------------------------------Begin Added KD Experimental to remove issue where 90 then 20 not preceeded by 30----------------*/
---- For a clock restart trigger flag to have an effect there must first be a clock stop
---- e.g. if there is a 90 followed by a 20 but there is no 30 then it must not be a restart
---- ? Is this really a DQ issue?
--							and Exists 
--								(
--								Select 1
--								from RTT.RTT PreviousClockStop
--								Where 
--								PreviousClockStop.ReferralSourceUniqueID = RTT.ReferralSourceUniqueID
--								and	PreviousClockStop.StartDate < RTT.StartDate
--								and	RTTStatus.ClockStopFlag = 'true'
--								)
--/*--------------------------------End Added KD Experimental to remove issue where 90 then 20 not preceeded by 30----------------*/

--							and	not exists
--								(
--								select
--									1
--								from
--									RTT.RTT Previous
--								where
--									Previous.ReferralSourceUniqueID = PreviousClockRestart.ReferralSourceUniqueID
--								and	Previous.StartDate < RTT.StartDate
--								and	(
--										Previous.StartDate < PreviousClockRestart.StartDate
--									or	(
--											Previous.StartDate = PreviousClockRestart.StartDate
--										and	Previous.EncounterRecno < PreviousClockRestart.EncounterRecno
--										)
--									)
--								)
--							)

--						) ClockStart
--					)

				,ManualClockStartDate =
					coalesce(
						 APCWLManualClock.ClockStartDate
						,APCManualClock.ClockStartDate
					)

				,OriginalProviderReferralDate =
					case
					when datepart(year, RFEncounter.OriginalProviderReferralDate) <= 1948
					then null
					else RFEncounter.OriginalProviderReferralDate
					end

				,ReferralDate =
--2011-11-21 Added check for RefToCarer
					Case 
					When RefToCarer.ProfessionalCarerCode is null 
					Then null 
					else 
						case
						when datepart(year, RFEncounter.ReferralDate) = 1948
						then null
						else RFEncounter.ReferralDate
						end
					End
				,Encounter.ReferralSourceUniqueID
				,Encounter.OriginalDateOnWaitingList

				,ManualAdjustmentDays =
					coalesce(APCWLManualAdjustment.AdjustmentDays, 0) +
					coalesce(APCManualAdjustment.AdjustmentDays, 0)

				,Diagnostic =
					case
					when
						DiagnosticProcedure.ProcedureCode is null
			--		and	upper(Encounter.Operation) not like '%$T%'
					then 0
					else 1
					end

			from
				APC.WaitingList Encounter

			left join RF.Encounter RFEncounter
			on	RFEncounter.SourceUniqueID = Encounter.ReferralSourceUniqueID
--2011-11-21 KD
			left join PAS.ProfessionalCarer RefToCarer
			on RFEncounter.ConsultantCode = RefToCarer.ProfessionalCarerCode
			and 
				(
				RefToCarer.ProfessionalCarerCode = 10539696  -- Nurse led plastics
				or RefToCarer.ProfessionalCarerTypeCode = 1130 -- Consultant
				)

		--get manual adjustments applied to this wait
			left join
			(
			select
				 SourceUniqueID = SourceRecno
				,AdjustmentDays = sum(AdjustmentDays)
			from
				RTT.Adjustment Adjustment
			where
				Adjustment.SourceCode = 'APCWL'
			group by
				 SourceRecno
			) APCWLManualAdjustment
			on	APCWLManualAdjustment.SourceUniqueID = Encounter.SourceUniqueID

		--get manual adjustments applied to this encounter
			left join 
			(
			select
				 SourceUniqueID = SourceRecno
				,AdjustmentDays = sum(AdjustmentDays)
			from
				RTT.Adjustment Adjustment
			where
				Adjustment.SourceCode = 'APC'
			group by
				 SourceRecno
			) APCManualAdjustment
			on	APCManualAdjustment.SourceUniqueID = Encounter.SourceUniqueID

			left join RTT.Encounter APCManualClock
			on	APCManualClock.SourceRecno = Encounter.SourceUniqueID
			and	APCManualClock.SourceCode = 'APC'

			left join RTT.Encounter APCWLManualClock
			on	APCWLManualClock.SourceRecno = Encounter.SourceUniqueID
			and	APCWLManualClock.SourceCode = 'APCWL'

			left join DiagnosticProcedure DiagnosticProcedure
			on	DiagnosticProcedure.ProcedureCode = Encounter.IntendedPrimaryOperationCode

			) Encounter
		) ClockStart
	) Final




