﻿
CREATE procedure [dbo].[BuildRTT] as


declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

delete
from
	RTT.RTT
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.RTT Archived
	where
		Archived.RTTPR_REFNO = RTT.SourceUniqueID
	and	Archived.ARCHV_FLAG <> 'N'
	)

insert into RTT.RTT
(
	 SourceUniqueID
	,ReferralSourceUniqueID
	,SourcePatientNo
	,SourceCode
	,SourceRecno
	,StartDate
	,RTTStatusCode
	,Comment
	,InterfaceCode
)
select
	 SourceUniqueID = RTTPR_REFNO
	,ReferralSourceUniqueID = REFRL_REFNO
	,SourcePatientNo = PATNT_REFNO
	,SourceCode = SORCE
	,SourceRecno = SORCE_REFNO
	,StartDate = RTTST_DATE
	,RTTStatusCode = RTTST_REFNO
	,Comment = COMMENTS
	,InterfaceCode = 'LOR'
from
	Lorenzo.dbo.RTT
where
	RTT.ARCHV_FLAG = 'N'
and	RTT.RTTST_REFNO is not null
and	not exists
	(
	select
		1
	from
		Lorenzo.dbo.RTT Previous
	where
		Previous.ARCHV_FLAG = 'N'
	and	Previous.RTTST_REFNO is not null
	and	Previous.SORCE = RTT.SORCE
	and	Previous.SORCE_REFNO = RTT.SORCE_REFNO
	and	Previous.RTTST_DATE = RTT.RTTST_DATE
	and	Previous.RTTST_REFNO = RTT.RTTST_REFNO
	and	(
			Previous.CREATE_DTTM > RTT.CREATE_DTTM
		or	(
				Previous.CREATE_DTTM = RTT.CREATE_DTTM
			and	Previous.RTTPR_REFNO > RTT.RTTPR_REFNO
			)
		)
	)

and	not exists
	(
	select
		1
	from
		RTT.RTT DestinationRTT
	where
		DestinationRTT.SourceCode = RTT.SORCE
	and	DestinationRTT.SourceRecno = RTT.SORCE_REFNO
	and	DestinationRTT.StartDate = RTT.RTTST_DATE
	and	DestinationRTT.RTTStatusCode = RTT.RTTST_REFNO
	)

--PDO 8 Mar 2012
--Apply manual validation overrides
and	not exists
	(
	select
		1
	from
		Lorenzo.dbo.RTT Future
	where
		Future.REFRL_REFNO = RTT.REFRL_REFNO
	and	Future.RTTPR_REFNO <> RTT.RTTPR_REFNO
	and	Future.RTTST_DATE = RTT.RTTST_DATE
	and	Future.SORCE = 'REFRL' --manual override
	and	Future.ARCHV_FLAG = 'N'
	and	Future.RTTST_REFNO is not null
	)

--PDO 14 Feb 2012 - above replaces next bit

--truncate table RTT.RTT

--alter index All on RTT.RTT disable

--alter index PK_RTT_1 on RTT.RTT rebuild

----copy the PAS RTT details
--insert into RTT.RTT
--(
--	 SourceUniqueID
--	,ReferralSourceUniqueID
--	,SourcePatientNo
--	,SourceCode
--	,SourceRecno
--	,StartDate
--	,RTTStatusCode
--	,Comment
--	,InterfaceCode
--)
--select
--	 SourceUniqueID = RTTPR_REFNO
--	,ReferralSourceUniqueID = REFRL_REFNO
--	,SourcePatientNo = PATNT_REFNO
--	,SourceCode = SORCE
--	,SourceRecno = SORCE_REFNO
--	,StartDate = RTTST_DATE
--	,RTTStatusCode = RTTST_REFNO
--	,Comment = COMMENTS
--	,InterfaceCode = 'PAS'
--from
--	PAS.RTTBase
--where
--	ARCHV_FLAG = 'N'
--and	RTTST_REFNO is not null
--and	not exists
--	(
--	select
--		1
--	from
--		PAS.RTTBase Previous
--	where
----------------------------------------------------------------------------------------------
----	20111026 KD removed while investigating duplicate refrl_refno for same sorce_refno
----  Which was creating duplicate entries here
----------------------------------------------------------------------------------------------
----		Previous.REFRL_REFNO = RTTBase.REFRL_REFNO
----	and	Previous.SORCE = RTTBase.SORCE
----------------------------------------------------------------------------------------------
--		Previous.SORCE = RTTBase.SORCE
--	and	Previous.SORCE_REFNO = RTTBase.SORCE_REFNO
--	and	Previous.RTTST_DATE = RTTBase.RTTST_DATE
--	and	Previous.RTTST_REFNO = RTTBase.RTTST_REFNO
--	and	Previous.ARCHV_FLAG = 'N'
--	and	Previous.RTTST_REFNO is not null
--	and	(
--			Previous.CREATE_DTTM > RTTBase.CREATE_DTTM
--		or	(
--				Previous.CREATE_DTTM = RTTBase.CREATE_DTTM
--			and	Previous.RTTPR_REFNO > RTTBase.RTTPR_REFNO
--			)
--		)
--	)

--alter index All on RTT.RTT rebuild


--copy the outpatient manual RTT details
insert into RTT.RTT
(
	 ReferralSourceUniqueID
	,SourceCode
	,SourceRecno
	,StartDate
	,RTTStatusCode
	,Comment
	,InterfaceCode
)
select
	 OutpatientOutcome.ReferralSourceUniqueID
	,SourceCode = 'SCHDL'
	,SourceRecno = OutpatientOutcome.OutpatientSourceUniqueID
	,StartDate = dateadd(day, datediff(day, 0, OutpatientOutcome.Updated), 0)
	,RTTStatusCode = RTTStatus.InternalCode
	,Comment = OutpatientOutcome.DataSource
	,InterfaceCode = 'MAN'
from
	RTT.OutpatientOutcome

inner join RTT.RTTStatus
on	RTTStatus.RTTStatusCode = OutpatientOutcome.RTTStatusCode

where
	OutpatientOutcome.Updated is not null
--and	not exists
--	(
--	select
--		1
--	from
--		RTT.RTT
--	where
--		RTT.ReferralSourceUniqueID = OutpatientOutcome.ReferralSourceUniqueID
--	and	RTT.SourceRecno = OutpatientOutcome.OutpatientSourceUniqueID
--	and	RTT.SourceCode = 'SCHDL'
--	)
and	not exists
	(
	select
		1
	from
		RTT.RTT DestinationRTT
	where
		DestinationRTT.SourceCode = 'SCHDL'
	and	DestinationRTT.SourceRecno = OutpatientOutcome.OutpatientSourceUniqueID
	and	DestinationRTT.StartDate = dateadd(day, datediff(day, 0, OutpatientOutcome.Updated), 0)
	and	DestinationRTT.RTTStatusCode = RTTStatus.InternalCode
	)

--copy the referral manual RTT details
insert into RTT.RTT
(
	 ReferralSourceUniqueID
	,SourceCode
	,SourceRecno
	,StartDate
	,RTTStatusCode
	,InterfaceCode
)
select
	 AdhocOutcome.ReferralSourceUniqueID
	,SourceCode = 'REFRL'
	,SourceRecno = AdhocOutcome.ReferralSourceUniqueID
	,StartDate = dateadd(day, datediff(day, 0, AdhocOutcome.RTTStatusDate), 0)
	,RTTStatusCode = RTTStatus.InternalCode
	,InterfaceCode = 'MAN'
from
	RTT.AdhocOutcome

inner join RTT.RTTStatus
on	RTTStatus.RTTStatusCode = AdhocOutcome.RTTStatusCode

where
	--not exists
	--(
	--select
	--	1
	--from
	--	RTT.RTT
	--where
	--	RTT.SourceRecno = AdhocOutcome.ReferralSourceUniqueID
	--and	RTT.SourceCode = 'REFRL'
	--)
	not exists
	(
	select
		1
	from
		RTT.RTT DestinationRTT
	where
		DestinationRTT.SourceCode = 'REFRL'
	and	DestinationRTT.SourceRecno = AdhocOutcome.ReferralSourceUniqueID
	and	DestinationRTT.StartDate = dateadd(day, datediff(day, 0, AdhocOutcome.RTTStatusDate), 0)
	and	DestinationRTT.RTTStatusCode = RTTStatus.InternalCode
	)

--PDO 14 Feb 2012 - Removed as this data is already present in the Lorenzo.dbo.RTT table.

----copy the legacy manual RTT details - Admitted Clock Stops
--insert into RTT.RTT
--(
--	 ReferralSourceUniqueID
--	,SourceCode
--	,SourceRecno
--	,StartDate
--	,RTTStatusCode
--	,InterfaceCode
--)
--select
--	 Legacy.ReferralSourceUniqueID
--	,SourceCode = 'PRVSP'
--	,SourceRecno = Legacy.SourceSpellNo
--	,StartDate = dateadd(day, datediff(day, 0, Legacy.ClockStopDate), 0)
--	,RTTStatusCode = 3007098 --30
--	,InterfaceCode = 'LEG'
--from
--	INFOSYS.AdmittedClockStop Legacy

--where
--	not exists
--	(
--	select
--		1
--	from
--		RTT.RTT
--	where
--		RTT.SourceRecno = Legacy.SourceSpellNo
--	and	RTT.StartDate = Legacy.ClockStopDate
--	and	RTT.RTTStatusCode = 3007098 --30
--	and	RTT.SourceCode = 'PRVSP'
--	)



----copy the legacy manual RTT details - Non-Admitted Clock Stops
--insert into RTT.RTT
--(
--	 ReferralSourceUniqueID
--	,SourceCode
--	,SourceRecno
--	,StartDate
--	,RTTStatusCode
--	,InterfaceCode
--)
--select
--	 Legacy.ReferralSourceUniqueID
--	,SourceCode = 'SCHDL'
--	,SourceRecno = Legacy.OutpatientSourceUniqueID
--	,StartDate = dateadd(day, datediff(day, 0, Legacy.ClockStopDate), 0)
--	,RTTStatusCode = 3007098 --30
--	,InterfaceCode = 'LEG'
--from
--	INFOSYS.NonAdmittedClockStop Legacy

--where
--	not exists
--	(
--	select
--		1
--	from
--		RTT.RTT
--	where
--		RTT.SourceRecno = Legacy.OutpatientSourceUniqueID
--	and	RTT.StartDate = Legacy.ClockStopDate
--	and	RTT.RTTStatusCode = 3007098 --30
--	and	RTT.SourceCode = 'SCHDL'
--	)


----copy the legacy manual RTT details - Administrative (Non-Admitted) Clock Stops
--insert into RTT.RTT
--(
--	 ReferralSourceUniqueID
--	,SourceCode
--	,SourceRecno
--	,StartDate
--	,RTTStatusCode
--	,InterfaceCode
--)
--select
--	 Legacy.ReferralSourceUniqueID
--	,SourceCode = Legacy.SourceCode
--	,SourceRecno = Legacy.ReferralSourceUniqueID
--	,StartDate = dateadd(day, datediff(day, 0, Legacy.ClockStopDate), 0)
--	,RTTStatusCode = 3007098 --30
--	,InterfaceCode = 'LEG'
--from
--	INFOSYS.AdministrativeClockStop Legacy

--where
--	not exists
--	(
--	select
--		1
--	from
--		RTT.RTT
--	where
--		RTT.SourceRecno = Legacy.ReferralSourceUniqueID
--	and	RTT.StartDate = Legacy.ClockStopDate
--	and	RTT.RTTStatusCode = 3007098 --30
--	and	RTT.SourceCode = Legacy.SourceCode
--	)


----copy the legacy manual RTT details - Admitted Clock Starts
--insert into RTT.RTT
--(
--	 ReferralSourceUniqueID
--	,SourceCode
--	,SourceRecno
--	,StartDate
--	,RTTStatusCode
--	,InterfaceCode
--)
--select
--	 Legacy.ReferralSourceUniqueID
--	,SourceCode = 'PRVSP'
--	,SourceRecno = Legacy.SourceSpellNo
--	,StartDate = dateadd(day, datediff(day, 0, Legacy.ClockStartDate), 0)
--	,RTTStatusCode = 3007096 --20
--	,InterfaceCode = 'LEG'
--from
--	INFOSYS.AdmittedClockStart Legacy

--where
--	not exists
--	(
--	select
--		1
--	from
--		RTT.RTT
--	where
--		RTT.SourceRecno = Legacy.SourceSpellNo
--	and	RTT.StartDate = Legacy.ClockStartDate
--	and	RTT.RTTStatusCode = 3007096 --20
--	and	RTT.SourceCode = 'PRVSP'
--	)



----copy the legacy manual RTT details - Non-Admitted Clock Starts
--insert into RTT.RTT
--(
--	 ReferralSourceUniqueID
--	,SourceCode
--	,SourceRecno
--	,StartDate
--	,RTTStatusCode
--	,InterfaceCode
--)
--select
--	 Legacy.ReferralSourceUniqueID
--	,SourceCode = 'SCHDL'
--	,SourceRecno = Legacy.OutpatientSourceUniqueID
--	,StartDate = dateadd(day, datediff(day, 0, Legacy.ClockStartDate), 0)
--	,RTTStatusCode = 3007096 --20
--	,InterfaceCode = 'LEG'
--from
--	INFOSYS.NonAdmittedClockStart Legacy

--where
--	not exists
--	(
--	select
--		1
--	from
--		RTT.RTT
--	where
--		RTT.SourceRecno = Legacy.OutpatientSourceUniqueID
--	and	RTT.StartDate = Legacy.ClockStartDate
--	and	RTT.RTTStatusCode = 3007096 --20
--	and	RTT.SourceCode = 'SCHDL'
--	)


----copy the legacy manual RTT details - Administrative (Non-Admitted) Clock Stops
--insert into RTT.RTT
--(
--	 ReferralSourceUniqueID
--	,SourceCode
--	,SourceRecno
--	,StartDate
--	,RTTStatusCode
--	,InterfaceCode
--)
--select
--	 Legacy.ReferralSourceUniqueID
--	,SourceCode = 'REFRL'
--	,SourceRecno = Legacy.ReferralSourceUniqueID
--	,StartDate = dateadd(day, datediff(day, 0, Legacy.ClockStartDate), 0)
--	,RTTStatusCode = 3007096 --20
--	,InterfaceCode = 'LEG'
--from
--	INFOSYS.AdministrativeClockStart Legacy

--where
--	not exists
--	(
--	select
--		1
--	from
--		RTT.RTT
--	where
--		RTT.SourceRecno = Legacy.ReferralSourceUniqueID
--	and	RTT.StartDate = Legacy.ClockStartDate
--	and	RTT.RTTStatusCode = 3007096 --20
--	and	RTT.SourceCode = 'REFRL'
--	)



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WH BuildRTT', @Stats, @StartTime
