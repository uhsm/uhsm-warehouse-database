﻿CREATE procedure [dbo].[BuildRTTFlag] as

--used to simplify RTT queries

truncate table RTT.RTTFlag

insert
into
	RTT.RTTFlag
(
	 EncounterRecno
	,SourceUniqueID
	,ReferralSourceUniqueID
	,SourcePatientNo
	,SourceCode
	,SourceRecno
	,StartDate
	,RTTStatusCode
	,Comment
	,InterfaceCode
	,NationalRTTStatusCode
	,ClockRestartFlag
	,ClockRestartTriggerFlag
	,ClockStartFlag
	,ClockStopFlag
)
select
	 RTT.EncounterRecno
	,RTT.RTT.SourceUniqueID
	,RTT.ReferralSourceUniqueID
	,RTT.SourcePatientNo
	,RTT.SourceCode
	,RTT.SourceRecno
	,RTT.StartDate
	,RTT.RTTStatusCode
	,RTT.Comment
	,RTT.InterfaceCode
	,NationalRTTStatusCode = RTTStatus.RTTStatusCode
	,RTTStatus.ClockRestartFlag
	,RTTStatus.ClockRestartTriggerFlag
	,RTTStatus.ClockStartFlag
	,RTTStatus.ClockStopFlag
from
	RTT.RTT

inner join RTT.RTTStatus RTTStatus
on	RTTStatus.InternalCode = RTT.RTTStatusCode

where
	(
		exists
			(
			select
				1
			from
				RTT.RTT ManualValidation
			where
				ManualValidation.Comment not in
					(
					 'This Referral'
					,'OP Waiting List'
					,''
					,'IP Waiting List'
					,'Admission Offer'
					,'Outpatient Forms'
					,'Patient Discharge'
					,'Outpatient Appointment'
					,'Contacts'
					,'Patient Depart'
					)
			and	ManualValidation.Comment is not null
			and	ManualValidation.SourceCode = 'REFRL'
			and	ManualValidation.EncounterRecno = RTT.EncounterRecno
			)

	or	not exists
			(
			select
				1
			from
				RTT.RTT ManualValidation
			where
				ManualValidation.Comment not in
					(
					 'This Referral'
					,'OP Waiting List'
					,''
					,'IP Waiting List'
					,'Admission Offer'
					,'Outpatient Forms'
					,'Patient Discharge'
					,'Outpatient Appointment'
					,'Contacts'
					,'Patient Depart'
					)
			and	ManualValidation.Comment is not null
			and	ManualValidation.SourceCode = 'REFRL'
			and	ManualValidation.ReferralSourceUniqueID = RTT.ReferralSourceUniqueID
			and	ManualValidation.StartDate = RTT.StartDate
			)
	)



