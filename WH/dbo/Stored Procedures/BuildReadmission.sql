﻿CREATE procedure [dbo].[BuildReadmission] as

create table #spell
(
	 Seq int IDENTITY(0,1)
	,DistrictNo varchar(50) NOT NULL
	,EncounterRecno int not NULL

	constraint pktempspell primary key clustered (DistrictNo, Seq)
)

insert into #spell
(
	 DistrictNo
	,EncounterRecno
)
select
	 DistrictNo
	,EncounterRecno
from
	(
	select
		 DistrictNo
		,EncounterRecno
		,AdmissionDate
		,DischargeDate = coalesce(DischargeDate, '6/6/2079')
		,ProviderSpellNo
	from
		APC.Encounter Spell
	where
		Spell.AdmissionTime = Spell.EpisodeStartTime
	and	DistrictNo is not null
	) Spell

order by
	 DistrictNo
	,AdmissionDate
	,coalesce(DischargeDate, '6/6/2079')
	,ProviderSpellNo

truncate table APC.Readmission

insert into APC.Readmission
(
	 ReadmissionRecno
	,PreviousAdmissionRecno
)
select
	 S.EncounterRecno ReadmissionRecno
	,P.EncounterRecno PreviousAdmissionRecno
from
	#spell as S

inner join #spell as P
on	P.DistrictNo = S.DistrictNo
and P.Seq = (S.Seq - 1)


drop table #spell
