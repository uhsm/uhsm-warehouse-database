﻿create procedure [dbo].[BuildTherapiesInpatients]

as


/*create table dbo.TherapiesContactPathway
(
episodes int not null
,referrals int
,contacts int
,EndWardCode varchar(20)
,ConsultantCode varchar(20),
EpisodeStartDateTime datetime,
EpisodeEndDateTime datetime,
ReferralReceivedDate date,
ReceivingProfCarercode varchar(80),
ReceivingProfCarerType varchar(80),
[ReceivingSpecialtyCode(Function)] varchar(25),
cont_date datetime,
arrival_time varchar(8),
depart_time varchar(8),
outcome varchar(80),
themonth varchar(20),
financialmonthkey int,
prof_carer varchar(25))*/

truncate table dbo.TherapiesContactPathway

insert into dbo.TherapiesContactPathway

(episodes
,referrals
,contacts
,EndWardCode
,ConsultantCode,
EpisodeStartDateTime,
EpisodeEndDateTime,
ReferralReceivedDate,
ReceivingProfCarercode,
ReceivingProfCarerType,
[ReceivingSpecialtyCode(Function)],
cont_date,
arrival_time,
depart_time,
outcome,
themonth,
financialmonthkey,
prof_carer)





select
count(distinct ep.EncounterRecno) as episodes,
count(distinct ref.ReferralSourceUniqueID) as referrals,
count(distinct cont_ref) as contacts,
ep.EndWardCode,
ep.ConsultantCode,
ep.EpisodeStartDateTime,
ep.EpisodeEndDateTime,
ref.ReferralReceivedDate,
ref.ReceivingProfCarercode,
ref.ReceivingProfCarerType,
ref.[ReceivingSpecialtycode(Function)],
cont.cont_date,
cont.arrival_time,
cont.depart_time,
cont.outcome,
cal.themonth,
cal.financialmonthkey,
cont.prof_carer

from 

whreporting.APC.Episode ep

left join whreporting.RF.Referral ref on ref.FacilityID=ep.FacilityID
and ref.ReferralCreated between ep. AdmissionDateTime and ep.DischargeDateTime
left join infosql.information_reporting.dbo.contacts_dataset cont on cont.ref_ref=ref.ReferralSourceUniqueID
left join whreporting.lk.calendar cal on cal.TheDate=ep.EpisodeStartDate
where ep.episodestartdate >'2011-03-31'

group by

ep.EndWardCode,
ep.ConsultantCode,
ep.EpisodeStartDateTime,
ep.EpisodeEndDateTime,
ref.ReferralReceivedDate,
ref.ReceivingProfCarercode,
ref.ReceivingProfCarerType,
ref.[ReceivingSpecialtycode(Function)],
cont.cont_date,
cont.arrival_time,
cont.depart_time,
cont.outcome,
cal.themonth,
cal.financialmonthkey,
cont.prof_carer