﻿CREATE procedure [dbo].[CompareColumnData] 
	@FullTableNameNew varchar(50)
	,@FullTableNameOld varchar(50)
	,@UIDNew varchar(50)
	,@UIDOld varchar(50)
	,@FieldName varchar(255) 
	,@Operator varchar(2) 
	,@DataType varchar(20) 
as

/*
31/07/2013 K Oakden

To automate the code below used to compare the values of columns in seperate datasets 
that can be linked using a common UID.
For testing new datasets.

--todo - Parametise table names and common UIDs

select match.fieldName, match.oldField, match.newField, count(match.SourceUniqueID) as cnt 
from (
	select 
		'PreOpClinic' as fieldName,
		old.PreOpClinic as oldField, 
		new.PreOpClinic as newField, 
		new.SourceUniqueID
	from PTL.IPWL new
	inner join PTL.TLoadIPWLBulk old
		on new.SourceUniqueID = old.SourceUniqueID
	where  old.PreOpClinic = new.PreOpClinic
--and new.PRACREG = 'H81019'
) match
group by match.fieldName, match.oldField, match.newField
order by newField

select 
	'exec dbo.CompareColumnData '''
	+ column_name +
	''',''<>'''
from information_schema.columns
where table_name = 'IPWL' and table_schema = 'PTL' 
*/


declare @cmd varchar(2000)
print @FieldName

--delete existing values
set @cmd = 
	'delete from dbo.ColumnComparison 
	where FieldName = '''
	+ @FieldName +
	''''
--print @cmd
exec (@cmd)

--re-run comparison and insert into table
set @cmd =
case 
	when @DataType in ('int', 'float', 'binary')
		then
			'insert into dbo.ColumnComparison 
			select match.CensusDate, match.fieldName, match.oldField, match.newField, count(match.'
			+ @UIDNew +
			') as cnt 
			from (
				select 
					'''
					+ @FieldName +
					''' as FieldName,
					old. '
					+ @FieldName +
					' as OldField, 
					new.'
					+ @FieldName +
					' as NewField, 
					new.'
					+ @UIDNew +
					',
					getdate() as CensusDate
				from '
				+ @FullTableNameNew +
				' new
				inner join '
				+ @FullTableNameOld +
				' old
					on new.'
					+ @UIDNew +
					' = old.'
					+ @UIDOld +
				' where isnull(old.'
				+ @FieldName + ',0) '
				+ @Operator +
				' isnull(new.'
				+ @FieldName +
			',0)) match
			group by  match.CensusDate, match.fieldName, match.oldField, match.newField
			order by newField'
			
	when @DataType in ('varchar', 'nvarchar')
		then
						'insert into dbo.ColumnComparison 
			select match.CensusDate, match.fieldName, match.oldField, match.newField, count(match.'
			+ @UIDNew +
			') as cnt 
			from (
				select 
					'''
					+ @FieldName +
					''' as FieldName,
					old. '
					+ @FieldName +
					' as OldField, 
					new.'
					+ @FieldName +
					' as NewField, 
					new.'
					+ @UIDNew +
					',
					getdate() as CensusDate
				from '
				+ @FullTableNameNew +
				' new
				inner join '
				+ @FullTableNameOld +
				' old
					on new.'
					+ @UIDNew +
					' = old.'
					+ @UIDOld +
				' where isnull(old.'
				+ @FieldName + ',''xx'') COLLATE DATABASE_DEFAULT '
				+ @Operator +
				' isnull(new.'
				+ @FieldName + ',''xx'') COLLATE DATABASE_DEFAULT 
			) match
			group by  match.CensusDate, match.fieldName, match.oldField, match.newField
			order by newField'
	
	when @DataType in ('datetime', 'smalldatetime')
		then
						'insert into dbo.ColumnComparison 
			select match.CensusDate, match.fieldName, match.oldField, match.newField, count(match.'
			+ @UIDNew +
			') as cnt 
			from (
				select 
					'''
					+ @FieldName +
					''' as FieldName,
					old. '
					+ @FieldName +
					' as OldField, 
					new.'
					+ @FieldName +
					' as NewField, 
					new.'
					+ @UIDNew +
					',
					getdate() as CensusDate
				from '
				+ @FullTableNameNew +
				' new
				inner join '
				+ @FullTableNameOld +
				' old
					on new.'
					+ @UIDNew +
					' = old.'
					+ @UIDOld +
				' where isnull(old.'
				+ @FieldName + ',''2013-01-01'') '
				+ @Operator +
				' isnull(new.'
				+ @FieldName +
			',''2013-01-01'')) match
			group by  match.CensusDate, match.fieldName, match.oldField, match.newField
			order by newField'
		
end
--print @cmd
exec (@cmd)



