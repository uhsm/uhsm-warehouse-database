﻿CREATE procedure [dbo].[CreateICECSDataset]
AS

/*create table whreporting.ICE.CSDataset
(
--Result_Code varchar (10),
--Sample_Index int,
ClinicianSurname varchar (35)
,ClinicianKey int
,ClinicianSpecialtyCode varchar (10)
,ProviderName varchar (65)
,ProviderType varchar (35)
,ProviderTypeKey int
,providerkey int
,RequestDate date
--,date_added datetime
,MonthAdded int
,ClinicianSpecialty varchar (100)
,InvestigationRequested varchar (100)
,InvestigationIndex int
,RequestIndex int
,SetRequested varchar (255)
,Cases smallint
,DateInvestigationAdded datetime)

*/






delete from  whreporting.ICE.CSDataset
where requestdate>DATEADD(day,-300,getdate())



insert into whreporting.ICE.CSDataset
(
--Sample_Index,
ClinicianSurname
,ClinicianKey
,ClinicianSpecialtyCode
,ProviderName
,ProviderType
,ProviderTypeKey
,providerkey
,RequestDate
--,date_added
,MonthAdded
,ClinicianSpecialty
,InvestigationRequested
,InvestigationIndex
,RequestIndex
,SetRequested
,Cases
,DateInvestigationAdded)



select distinct

--rs.Sample_Index,
cl.ClinicianSurname
,ClinicianKey
,isnull(cast(cl.ClinicianSpecialtyCode as varchar),'Unk')
,coalesce(prov.ProviderDiscipline,p.ProviderDiscipline)
,coalesce(prov.ProviderType,p.ProviderType)
,prov.ProviderTypeKey
,prov.providerspecialtycode
,rq.RequestDate
--,rs.Date_Added
,year(rq.RequestDate)*100+MONTH(rq.RequestDate) as MonthAdded
,case when cl.clinicianspecialtycode is null then 'Unknown' else isnull(s.Specialty,'Unknown') end as Specialty
,irq.InvestigationRequested
,i.InvestigationIndex
,rq.RequestIndex
,SetRequested
,1
,i.DateInvestigationAdded



 from --whreporting.ICE.factresults rs inner join
 whreporting.ICE.FactInvestigation i inner join --on i.InvestigationIndex=rs.Investigation_Index inner join
 whreporting.ICE.DimInvestigationRequested irq on irq.InvestigationKey=i.InvestigationDescriptionKey
 
 right outer join
 whreporting.ICE.FactRequest rq on i.RequestIndex=rq.RequestIndex 

 
 
 inner join
 whreporting.ICE.DimClinician cl on cl.Cliniciankey=rq.MasterClinicianCode 
 
 left join 
 whreporting.ICE.DimProvider p on p.ProviderKey=rq.ServiceProviderCode
 
 left outer join WHREPORTING.ICE.FactReport rp on rp.RequestIndex=rq.RequestIndex
 
 left outer join 
 
 (select distinct iceprov.ProviderSpecialtyCode,providertypekey,providertype,
case when iceprov.ProviderSpecialtyCode='821' then 'Blood Transfusion' else iceprov.ProviderDiscipline end 
as ProviderDiscipline from whreporting.ICE.DimProvider iceprov
where iceprov.ProviderSpecialtyCode is not null) as prov on prov.ProviderSpecialtyCode=coalesce(rp.Specialty,p.providerspecialtycode)
 
 
/* Left Outer Join whreporting.ICE.DimProvider pr

	On rp.Specialty = Pr.ProviderSpecialtyCode*/
 
  left outer join whreporting.LK.SpecialtyDivision s on s.SpecialtyCode=cast(cl.ClinicianSpecialtyCode as varchar)

where rq.requestdate>DATEADD(day,-300,getdate())

and cl.ClinicianSpecialtyCode not in ('999','9999')
and s.MainSpecialtyCode not in ('DQ')

 
 group by 
--rs.Sample_Index,
cl.ClinicianSurname
,ClinicianKey
,isnull(cast(cl.ClinicianSpecialtyCode as varchar),'Unk')
,coalesce(prov.ProviderDiscipline,p.ProviderDiscipline)
,coalesce(prov.ProviderType,p.ProviderType)
,prov.ProviderTypeKey
,prov.providerspecialtycode
,rq.RequestDate
--,rs.Date_Added
,year(rq.RequestDate)*100+MONTH(rq.RequestDate)
,case when cl.clinicianspecialtycode is null then 'Unknown' else isnull(s.Specialty,'Unknown') end
,irq.InvestigationRequested,
i.InvestigationIndex
,rq.RequestIndex
,SetRequested
,i.DateInvestigationAdded


