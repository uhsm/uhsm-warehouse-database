﻿CREATE procedure [dbo].[CreateMISYSDemandDataset]
/*create TABLE

MISYS.DemandDataset

(OrdersPerformed int not null,
orderdate datetime not null,
Staff nvarchar (50) null,
MonthOrdered varchar (8) not null,
MonthOrderedName nvarchar (3) null,
FinancialYear int not null,
RequestedCaseType nvarchar (50) null,
OrderExamCode nvarchar (10) null,
Exam nvarchar (130) null,
ModalityCode nvarchar(10) null,
Modality nvarchar (100) null,
PriorityCode nvarchar(50) null,
PlannedRequest nvarchar (30) null,
CurrentPatientType nvarchar(25) null,
Specialty nvarchar (50) null,
MonthSequence int not null)*/

AS

delete from MISYS.DemandDataset
where orderdate>DATEADD(day,-180,GETDATE())




insert into 

MISYS.DemandDataset
(OrdersPerformed,
OrderDate,
Staff,
MonthOrdered,
MonthOrderedName,
FinancialYear,
RequestedCaseType,
OrderExamCode,
Exam,
ModalityCode,
Modality,
PriorityCode,
PlannedRequest,
CurrentPatientType,
Specialty ,                                                         
MonthSequence)

SELECT    
count(MISYSOrder.OrderNumber) as OrdersPerformed,
MISYSOrder.OrderDate,
OrderingPhysician.Staff as staff,
CAST(MONTH(MISYSOrder.OrderDate) AS int) AS MonthOrdered, 
CASE WHEN month(MISYSOrder.OrderDate) = 1 THEN ('Jan')
     WHEN month(MISYSOrder.OrderDate) = 2 THEN ('Feb')
     WHEN month(MISYSOrder.OrderDate) = 3 THEN ('Mar') 
     WHEN month(MISYSOrder.OrderDate) = 4 THEN ('Apr')
     WHEN month(MISYSOrder.OrderDate) = 5 THEN ('May')
     WHEN month(MISYSOrder.OrderDate) = 6 THEN ('Jun')
     WHEN month(MISYSOrder.OrderDate) = 7 THEN ('Jul') 
     WHEN month(MISYSOrder.OrderDate) = 8 THEN ('Aug')
     WHEN month(MISYSOrder.OrderDate) = 9 THEN ('Sep')
     WHEN month(MISYSOrder.OrderDate) = 10 THEN ('Oct')
     WHEN month(MISYSOrder.OrderDate) = 11 THEN ('Nov')
     WHEN month(MISYSOrder.OrderDate) = 12 THEN ('Dec')
     ELSE ('Other') END AS MonthOrderedName,
CASE WHEN month(MISYSOrder.OrderDate) BETWEEN 1 AND 
                      3 THEN YEAR(MISYSOrder.OrderDate) ELSE year(MISYSOrder.OrderDate) + 1 END AS FiscalYear, 
CASE WHEN MISYSOrder.CaseTypeCode = 'X' THEN 'Other' 
ELSE CASE WHEN MISYSOrder.CaseGroupCode = 'S' THEN 'Specialist' 
WHEN MISYSOrder.CaseGroupCode = 'G' THEN 'General' 
WHEN MISYSOrder.CaseGroupCode = 'O' THEN 'Other' 
ELSE 'Unknown' END END AS RequestedCaseType,
MISYSOrder.OrderExamCode, 
MISYSExam.Exam,
CASE WHEN ORDEREXAMCODE IN ('FBAEN', 'FBAME', 'FBAMF', 'FBASW', 'FPRCT', 'FVIDS', 
                      'FWASW', 'YSBCE') THEN ('Ba')  ELSE MISYSOrder.CaseTypeCode END AS ModalityCode, 
Modality.Modality, 
ISNULL(MISYSOrder.PriorityCode, N'Attention') AS PriorityCode, 
MISYSOrder.PlannedRequest,  
MISYSOrder.PatientTypeCode AS CurrentPatientType,
case when OrderingPhysician.Staff like ('A&E%') THEN ('A&E')
when OrderingPhysician.Staff like ('ASSESS%') THEN ('Assessment')
when
MISYS.lkSpecTrue.SpecialtyTrue is null 
and rtrim(ltrim(left((SUBSTRING(OrderingPhysician.Staff, PATINDEX('%([a-z]%)', OrderingPhysician.Staff) + 1, 
                      LEN(OrderingPhysician.Staff) - CASE patindex('%([a-z]%)', OrderingPhysician.Staff) WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', OrderingPhysician.Staff) 
                      + 1 END)),2)))='GP'
                      THEN 'GP'
                      
                    
when MISYS.lkSpecTrue.SpecialtyTrue is null 
and rtrim(ltrim(left((SUBSTRING(OrderingPhysician.Staff, PATINDEX('%([a-z]%)', OrderingPhysician.Staff) + 1, 
                      LEN(OrderingPhysician.Staff) - CASE patindex('%([a-z]%)', OrderingPhysician.Staff) WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', OrderingPhysician.Staff) 
                      + 1 END)),2)))<>'GP'
                      THEN 'Unknown/External'   
                      
 else MISYS.lkSpecTrue.SpecialtyTrue end as Specialty,
CASE WHEN CAST(MONTH(MISYSOrder.OrderDate) AS INT) = 1 THEN 13 WHEN CAST(MONTH(MISYSOrder.OrderDate) AS INT) 
                      = 2 THEN 14 WHEN CAST(MONTH(MISYSOrder.OrderDate) AS INT) = 3 THEN 15 ELSE CAST(MONTH(MISYSOrder.OrderDate) AS INT) END as MonthSequence
FROM         
WHOLAP.dbo.BaseOrder AS MISYSOrder WITH (nolock) INNER JOIN
                      WHOLAP.dbo.OlapMISYSExam AS MISYSExam WITH (nolock) ON MISYSExam.ExamCode = MISYSOrder.OrderExamCode INNER JOIN
                      WHOLAP.dbo.OlapMISYSModality AS Modality WITH (nolock) ON Modality.ModalityCode = MISYSOrder.CaseTypeCode INNER JOIN
                      WHOLAP.dbo.OlapMISYSLocation AS OrderingLocation WITH (nolock) ON 
                      OrderingLocation.LocationCode = MISYSOrder.OrderingLocationCode INNER JOIN
                      WHOLAP.dbo.OlapMISYSStaff AS OrderingPhysician WITH (nolock) ON OrderingPhysician.StaffCode = MISYSOrder.OrderingPhysicianCode 
                      LEFT OUTER JOIN
                      MISYS.lkSpecTrue ON MISYS.lkSpecTrue.SpecialtyOrig = SUBSTRING(OrderingPhysician.Staff, PATINDEX('%([a-z]%)', OrderingPhysician.Staff) + 1, 
                      LEN(OrderingPhysician.Staff) - CASE patindex('%([a-z]%)', OrderingPhysician.Staff) WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', OrderingPhysician.Staff) 
                      + 1 END)
WHERE    

orderdate>DATEADD(day,-180,GETDATE()) and

 (MISYSOrder.CaseTypeCode IN ('M', 'C', 'U', 'X', 'D', 'O','F','I', 'V')) AND (NOT EXISTS
                          (SELECT     1 AS Expr1
                            FROM          MISYS.CancelledOrder AS canc
                            WHERE      (MISYSOrder.OrderNumber = OrderNumber))) AND 
                       (MISYSOrder.DictationDate IS NOT NULL) AND (MISYSOrder.PerformanceLocation IS NOT NULL) 
                      AND (MISYSOrder.OrderDate > '2008-12-31') 
                      
GROUP BY
CAST(MONTH(MISYSOrder.OrderDate) AS int), 
MISYSOrder.OrderDate,
CASE WHEN month(MISYSOrder.OrderDate) = 1 THEN ('Jan')
     WHEN month(MISYSOrder.OrderDate) = 2 THEN ('Feb')
     WHEN month(MISYSOrder.OrderDate) = 3 THEN ('Mar')
     WHEN month(MISYSOrder.OrderDate) = 4 THEN ('Apr')
     WHEN month(MISYSOrder.OrderDate) = 5 THEN ('May')
     WHEN month(MISYSOrder.OrderDate) = 6 THEN ('Jun')
     WHEN month(MISYSOrder.OrderDate) = 7 THEN ('Jul')
     WHEN month(MISYSOrder.OrderDate) = 8 THEN ('Aug')
     WHEN month(MISYSOrder.OrderDate) = 9 THEN ('Sep')
     WHEN month(MISYSOrder.OrderDate) = 10 THEN ('Oct')
     WHEN month(MISYSOrder.OrderDate) = 11 THEN ('Nov')
     WHEN month(MISYSOrder.OrderDate) = 12 THEN ('Dec')
     ELSE ('Other') END,
CASE WHEN month(MISYSOrder.OrderDate) BETWEEN 1 AND 
                      3 THEN YEAR(MISYSOrder.OrderDate) ELSE year(MISYSOrder.OrderDate) + 1 END, 
CASE WHEN MISYSOrder.CaseTypeCode = 'X' THEN 'Other' 
ELSE CASE WHEN MISYSOrder.CaseGroupCode = 'S' THEN 'Specialist' 
WHEN MISYSOrder.CaseGroupCode = 'G' THEN 'General' 
WHEN MISYSOrder.CaseGroupCode = 'O' THEN 'Other' 
ELSE 'Unknown' END END,
MISYSOrder.OrderExamCode, 
MISYSExam.Exam,
CASE WHEN ORDEREXAMCODE IN ('FBAEN', 'FBAME', 'FBAMF', 'FBASW', 'FPRCT', 'FVIDS', 
                      'FWASW', 'YSBCE') THEN ('Ba')  ELSE MISYSOrder.CaseTypeCode END, 
Modality.Modality, 
ISNULL(MISYSOrder.PriorityCode, N'Attention'), 
MISYSOrder.PlannedRequest,  
MISYSOrder.PatientTypeCode,
MISYS.lkSpecTrue.SpecialtyTrue,
CASE WHEN CAST(MONTH(MISYSOrder.OrderDate) AS INT) = 1 THEN 13 WHEN CAST(MONTH(MISYSOrder.OrderDate) AS INT) 
                      = 2 THEN 14 WHEN CAST(MONTH(MISYSOrder.OrderDate) AS INT) = 3 THEN 15 ELSE CAST(MONTH(MISYSOrder.OrderDate) AS INT) END 
                      ,OrderingPhysician.Staff
                      
                      



/*create table [MISYS].[DemandTotal]
([Year] varchar (4) null,
MonthOrdered int,
MonthOrderedName varchar (3) null,
ModalityCode varchar (10) null,
Modality varchar (100) null,
Specialty varchar (100) null,
Orders int,
OrdersLastYr int)*/


truncate table [MISYS].[DemandTotal]
          
insert into [MISYS].[DemandTotal]
(
	[Year],
	MonthOrdered,
	MonthOrderedName,
	ModalityCode,
	Modality,
	Specialty,
	Orders,
	OrdersLastYr
) 


select 

cast(Year(orderdate) as varchar)
,MonthOrdered
,MonthOrderedName
,ModalityCode
,Modality
,Specialty
,isnull(sum(OrdersPerformed),0)
,(select sum(OrdersPerformed)

from MISYS.DemandDataset b
where cast((YEAR(b.orderdate)+1)as varchar)=cast(Year(a.orderdate) as varchar)
and b.MonthOrdered=a.MonthOrdered
and b.ModalityCode=a.ModalityCode
and isnull(b.Specialty,'Unknown/External')=isnull(a.Specialty,'Unknown/External')

group by
cast((YEAR(b.orderdate)+1)as varchar)
,MonthOrdered
,MonthOrderedName
,ModalityCode
,Modality
,Specialty

)
from MISYS.DemandDataset a

group by
cast(Year(orderdate) as varchar)
,MonthOrderedName
,MonthOrdered
,ModalityCode
,Modality
,Specialty


