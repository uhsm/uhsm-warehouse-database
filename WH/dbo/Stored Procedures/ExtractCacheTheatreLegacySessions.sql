﻿CREATE procedure [dbo].[ExtractCacheTheatreLegacySessions]
	 @interfaceCode varchar(10)
	,@fromDate smalldatetime
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @sql1 varchar(8000)
declare @from varchar(10)
declare @LinkedServer varchar(50)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = 
	convert(varchar, @fromDate, 120)


select
	@LinkedServer = LinkedServer
from
	Interface
where
	InterfaceCode = @interfaceCode

select
	@sql1 = '
insert into dbo.TImportTheatreLegacySessions
(
	 ID
	,AmendDate
	,AmendTime
	,Comment
	,ConsultantCode
	,Description
	,PlannedAnaesthetist
	,PrintDate
	,PrintTime
	,ProcedureDate
	,Registrar
	,SessionStatus
	,SessionType
	,StartAndEnd
	,SubTheatre
	,TheatreNumber
	,InterfaceCode
)
select
	 ID
	,AmendDate
	,AmendTime
	,Comment
	,ConsultantCode
	,Description
	,PlannedAnaesthetist
	,PrintDate
	,PrintTime
	,ProcedureDate
	,Registrar
	,SessionStatus
	,SessionType
	,StartAndEnd
	,SubTheatre
	,TheatreNumber
	,''' + @interfaceCode + '''
from
	openquery(' + @LinkedServer + ', ''
select
	 ID
	,AmendDate
	,AmendTime
	,Comment
	,ConsultantCode
	,Description
	,PlannedAnaesthetist
	,PrintDate
	,PrintTime
	,ProcedureDate
	,Registrar
	,SessionStatus
	,SessionType
	,StartAndEnd
	,SubTheatre
	,TheatreNumber
from
	Theatre.LegacySessions
where
	ProcedureDate >= dateadd(month, 0, ''''' + @from + ''''')
AND	ID NOT LIKE ''''%||DUMP||%''''
''
)
'

if @debug = 1 
begin
	print @sql1
end
else
begin
	exec (@sql1)

	select
		@RowsInserted = @RowsInserted + @@ROWCOUNT

	SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

	SELECT @Stats = 
		'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
		'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

	EXEC WriteAuditLogEvent 'ExtractCacheTheatreLegacySessions', @Stats, @StartTime

end
