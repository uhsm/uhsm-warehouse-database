﻿CREATE procedure [dbo].[ExtractCacheTheatrePatientEpisode]
	 @interfaceCode varchar(10)
	,@fromDate smalldatetime
	,@debug bit = 0
as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @sql1 varchar(8000)
declare @from varchar(10)
declare @LinkedServer varchar(50)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = 
	convert(varchar, @fromDate, 120)


select
	@LinkedServer = LinkedServer
from
	Interface
where
	InterfaceCode = @interfaceCode

select
	@sql1 = '
insert into dbo.TImportTheatrePatientEpisode
(
	 SourceUniqueID
	,ASACode
	,ATO
	,ATORelieved
	,ActualOperationLine1
	,ActualOperationLine2
	,ActualOperationLine3
	,ActualOperationLine4
	,AnaestheticComments
	,AnaestheticNurse1
	,AnaestheticNurse1Relieved
	,AnaestheticNurse2
	,AnaestheticNurse2Relieved
	,AnaestheticType1
	,AnaestheticType2
	,AnaestheticType3
	,AnaestheticType4
	,Anaesthetist1
	,Anaesthetist2
	,Anaesthetist3
	,CancelReason
	,CEPOD
	,CirculatingNurse
	,CirculatingNurseRelieved
	,DestinationFromRecovery
	,Episode
	,EscortName
	,EscortNurse
	,EscortNurseRelieved
	,ExpectedLengthOfOperation
	,FirstAssistant
	,IntendedManagement
	,InternalDistrictNumber
	,OtherNurse
	,OtherNurseRelieved
	,Outcome
	,PatientAccompanied
	,PatientLocation
	,PlannedAnaesthetic
	,PortersActivityReception
	,ProcedureDate
	,ProcedureTime
	,ProposedOperationLine1
	,ProposedOperationLine2
	,ProposedOperationLine3
	,ProposedOperationLine4
	,ReasonDelayReception1
	,ReasonDelayReception2
	,ReasonDelayRecovery1
	,ReasonDelayRecovery2
	,ReceptionComments
	,ReceptionNurse1
	,ReceptionNurse2
	,ReceptionPorter
	,RecoveryComments
	,RecoveryPorter
	,RevoveryNurse
	,RevoveryNurseRelieved
	,ScrubNurse1
	,ScrubNurse1Relieved
	,ScrubNurse2
	,ScrubNurse2Relieved
	,SpecialEquipment1
	,SpecialEquipment2
	,SpecialEquipment3
	,SpecialEquipment4
	,Surgeon1
	,Surgeon2
	,Surgeon3
	,TheatreComments
	,TheatreNumber
	,TimeAnaestheticStart
	,TimeArrived
	,TimeAwake
	,TimeInAnaestheticRoom
	,TimeInRecovery
	,TimeOffTable
	,TimeOnTable
	,TimeOutAnaestheticRoom
	,TimeOutRecovery
	,TimeReadyForDischarge
	,TimeSentFor
	,WardCode
	,SessionID
	,SourcePatientNo
	,InterfaceCode
)
select
	 SourceUniqueID = ID
	,ASACode
	,ATO
	,ATORelieved
	,ActualOperationLine1
	,ActualOperationLine2
	,ActualOperationLine3
	,ActualOperationLine4
	,AnaestheticComments
	,AnaestheticNurse1
	,AnaestheticNurse1Relieved
	,AnaestheticNurse2
	,AnaestheticNurse2Relieved
	,AnaestheticType1
	,AnaestheticType2
	,AnaestheticType3
	,AnaestheticType4
	,Anaesthetist1
	,Anaesthetist2
	,Anaesthetist3
	,CancelReason
	,CEPOD
	,CirculatingNurse
	,CirculatingNurseRelieved
	,DestinationFromRecovery
	,Episode
	,EscortName
	,EscortNurse
	,EscortNurseRelieved
	,ExpectedLengthOfOperation
	,FirstAssistant
	,IntendedManagement
	,InternalDistrictNumber
	,OtherNurse
	,OtherNurseRelieved
	,Outcome
	,PatientAccompanied
	,PatientLocation
	,PlannedAnaesthetic
	,PortersActivityReception
	,ProcedureDate
	,ProcedureTime
	,ProposedOperationLine1
	,ProposedOperationLine2
	,ProposedOperationLine3
	,ProposedOperationLine4
	,ReasonDelayReception1
	,ReasonDelayReception2
	,ReasonDelayRecovery1
	,ReasonDelayRecovery2
	,ReceptionComments
	,ReceptionNurse1
	,ReceptionNurse2
	,ReceptionPorter
	,RecoveryComments
	,RecoveryPorter
	,RevoveryNurse
	,RevoveryNurseRelieved
	,ScrubNurse1
	,ScrubNurse1Relieved
	,ScrubNurse2
	,ScrubNurse2Relieved
	,SpecialEquipment1
	,SpecialEquipment2
	,SpecialEquipment3
	,SpecialEquipment4
	,Surgeon1
	,Surgeon2
	,Surgeon3
	,TheatreComments
	,TheatreNumber
	,TimeAnaestheticStart
	,TimeArrived
	,TimeAwake
	,TimeInAnaestheticRoom
	,TimeInRecovery
	,TimeOffTable
	,TimeOnTable
	,TimeOutAnaestheticRoom
	,TimeOutRecovery
	,TimeReadyForDischarge
	,TimeSentFor
	,WardCode
	,SessionID
	,SourcePatientNo
	,''' + @interfaceCode + '''
from
	openquery(' + @LinkedServer + ', ''
select
	 Episode.ID
	,Episode.ASAScore as ASACode
	,Episode.ATO
	,Episode.ATORelieved
	,Episode.ActualOperationLine1
	,Episode.ActualOperationLine2
	,Episode.ActualOperationLine3
	,Episode.ActualOperationLine4
	,left(Episode.AnaestheticComments, 50) as AnaestheticComments
	,Episode.AnaestheticNurse1
	,Episode.AnaestheticNurse1Relieved
	,Episode.AnaestheticNurse2
	,Episode.AnaestheticNurse2Relieved
	,Episode.AnaestheticType1
	,Episode.AnaestheticType2
	,Episode.AnaestheticType3
	,Episode.AnaestheticType4
	,Episode.Anaesthetist1
	,Episode.Anaesthetist2
	,Episode.Anaesthetist3
	,Episode.CancelReason
	,Episode.Cepod as CEPOD
	,Episode.CirculatingNurse
	,Episode.CirculatingNurseRelieved
	,Episode.DestinationFromRecovery
	,Episode.Episode
	,Episode.EscortName
	,Episode.EscortNurse
	,Episode.EscortNurseRelieved
	,Episode.ExpectedLengthOfOperation
	,Episode.FirstAssistant
	,Episode.IntendedManagement
	,Episode.InternalDistrictNumber
	,Episode.OtherNurse
	,Episode.OtherNurseRelieved
	,Episode.Outcome
	,Episode.PatientAccompanied
	,Episode.PatientLocation
	,Episode.PlannedAnaesthetic
	,Episode.PortersActivityReception
	,Episode.ProcedureDate
	,Episode.ProcedureTime
	,Episode.ProposedOperationLine1
	,Episode.ProposedOperationLine2
	,Episode.ProposedOperationLine3
	,Episode.ProposedOperationLine4
	,Episode.ReasonDelayReception1
	,Episode.ReasonDelayReception2
	,Episode.ReasonDelayRecovery1
	,Episode.ReasonDelayRecovery2
	,Episode.ReceptionComments
	,Episode.ReceptionNurse1
	,Episode.ReceptionNurse2
	,Episode.ReceptionPorter
	,Episode.RecoveryComments
	,Episode.RecoveryPorter
	,Episode.RevoveryNurse
	,Episode.RevoveryNurseRelieved
	,Episode.ScrubNurse1
	,Episode.ScrubNurse1Relieved
	,Episode.ScrubNurse2
	,Episode.ScrubNurse2Relieved
	,Episode.SpecialEquipment1
	,Episode.SpecialEquipment2
	,Episode.SpecialEquipment3
	,Episode.SpecialEquipment4
	,Episode.Surgeon1
	,Episode.Surgeon2
	,Episode.Surgeon3
	,left(Episode.TheatreComments, 50) as TheatreComments
	,Episode.TheatreNumber
	,Episode.TimeAnaestheticStart
	,Episode.TimeArrived
	,Episode.TimeAwake
	,Episode.TimeInAnaestheticRoom
	,Episode.TimeInRecovery
	,Episode.TimeOffTable
	,Episode.TimeOnTable
	,Episode.TimeOutAnaestheticRoom
	,Episode.TimeOutRecovery
	,Episode.TimeReadyForDischarge
	,Episode.TimeSentFor
	,Episode.WardCode

	,Map.ToLegacySessions as SessionID
	,Patient.GNumber SourcePatientNo
from
	Theatre.PatientEpisode Episode

inner join Theatre.LegacyEpisode Map
on      Map.Episode = Episode.Episode

inner join Indices.CrIndex1 Patient
on Patient.ANumber = Episode.InternalDistrictNumber

where
	Episode.ProcedureDate >= dateadd(month, 0, ''''' + @from + ''''')
AND	Episode.ID NOT LIKE ''''%||DUMP||%''''
''
)
'

if @debug = 1 
begin
	print @sql1
end
else
begin
	exec (@sql1)

	select
		@RowsInserted = @RowsInserted + @@ROWCOUNT

	SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

	SELECT @Stats = 
		'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
		'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

	EXEC WriteAuditLogEvent 'ExtractCacheTheatrePatientEpisode', @Stats, @StartTime

end
