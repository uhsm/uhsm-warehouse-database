﻿CREATE procedure [dbo].[ExtractCacheTheatreReferenceData]
	@InterfaceCode varchar(10)
as

declare @sql varchar(8000)
declare @LinkedServer varchar(50)
declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()


set dateformat dmy


select
	@LinkedServer = LinkedServer
from
	Interface
where
	InterfaceCode = @InterfaceCode



delete from Theatre.SpecialtyBase

select
	@sql = '
insert into Theatre.SpecialtyBase
(
	ID
	,Code
	,Name
)
select
	ID
	,Code
	,Name
from
	openquery(' + @LinkedServer + ', ''
select
	ID
	,Code
	,Name
from
	Theatre.Specialty
''
)
'

exec (@sql)

update Theatre.SpecialtyBase set [Name] = 'GASTROENTEROLOGY' where [Name] = 'ENTEROLOGY'



delete from Theatre.AnaesthetistBase
where
	InterfaceCode = @InterfaceCode

select
	@sql = '
INSERT INTO Theatre.AnaesthetistBase
(
	Active
	,Code
	,Designation
	,FullName
	,Initials
	,SpecialtyCode
	,Surname
	,Title
	,InterfaceCode
)
select
	Active
	,Code
	,Designation
	,FullName
	,Initials
	,SpecialtyCode
	,Surname
	,Title
	,''' + @InterfaceCode + ''' InterfaceCode
from
	openquery(' + @LinkedServer + ', ''
select
	Active
	,Code
	,Designation
	,FullName
	,left(Initials, 5) Initials
	,SpecialtyCode
	,Surname
	,Title
from
	Theatre.Anaesthetist
''
)
'

exec (@sql)


delete from Theatre.Nurse
where
	InterfaceCode = @InterfaceCode

select
	@sql = '
INSERT INTO Theatre.Nurse
(
	Active
	,Code
	,Designation
	,FullName
	,Grade
	,Initials
	,Surname
	,Title
	,InterfaceCode
)
select
	Active
	,Code
	,Designation
	,FullName
	,Grade
	,Initials
	,Surname
	,Title
	,''' + @InterfaceCode + ''' InterfaceCode

from
	openquery(' + @LinkedServer + ', ''
select
	Active
	,Code
	,Designation
	,FullName
	,Grade
	,left(Initials, 5) Initials
	,Surname
	,Title
from
	Theatre.Nurse
''
)
'

exec (@sql)



delete from Theatre.Porter
where
	InterfaceCode = @InterfaceCode

select
	@sql = '
INSERT INTO Theatre.Porter
(
	Active
	,Code
	,Designation
	,FullName
	,Initials
	,Surname
	,Title
	,InterfaceCode
)
select
	Active
	,Code
	,Designation
	,FullName
	,Initials
	,Surname
	,Title
	,''' + @InterfaceCode + ''' InterfaceCode

from
	openquery(' + @LinkedServer + ', ''
select
	Active
	,Code
	,Designation
	,FullName
	,left(Initials, 5) Initials
	,Surname
	,Title
from
	Theatre.Porter
''
)
'

exec (@sql)


delete from Theatre.Surgeon
where
	InterfaceCode = @InterfaceCode


-- always remap Anaesthetics (51) to Pain Management (49) before building Specialty table
select
	@sql = '
INSERT INTO Theatre.Surgeon
(
	Active
	,Code
	,Designation
	,FullName
	,Initials
	,SpecialtyCode
	,Surname
	,Title
	,InterfaceCode
)
select
	Active
	,Code
	,Designation
	,FullName
	,Initials
	,SpecialtyCode = case when SpecialtyCode = ''51'' then ''49'' else SpecialtyCode end
	,Surname
	,Title
	,''' + @InterfaceCode + ''' InterfaceCode

from
	openquery(' + @LinkedServer + ', ''
select
	Active
	,Code
	,Designation
	,FullName
	,left(Initials, 5) Initials
	,SpecialtyCode
	,Surname
	,Title
from
	Theatre.Surgeon
''
)
'

exec (@sql)




delete from Theatre.Theatres
where
	InterfaceCode = @InterfaceCode

select
	@sql = '
INSERT INTO Theatre.Theatres
(
	ID
	,Active
	,Area
	,Code
	,Description
	,Location
	,InterfaceCode
)

select
	ID
	,Active
	,Area
	,Code
	,Description
	,Location
	,''' + @InterfaceCode + ''' InterfaceCode

from
	openquery(' + @LinkedServer + ', ''
select
	ID
	,Active
	,Area
	,Code
	,Description
	,Location
from
	Theatre.Theatres
''
)
'

exec (@sql)


delete from Theatre.ParametersAndReasons

select
	@sql = '
INSERT INTO Theatre.ParametersAndReasons
(
	ID
	,Code
	,Description
	,TopLevel
)

select
	ID
	,Code
	,Description
	,TopLevel

from
	openquery(' + @LinkedServer + ', ''
select
	ID
	,Code
	,Description
	,TopLevel
from
	Theatre.ParametersAndReasons
''
)
'

exec (@sql)


delete from Theatre.CancelReasons

select
	@sql = '
INSERT INTO Theatre.CancelReasons
(
	ID
	,Active
	,CancelledBy
	,Clinical
	,Code
	,Description
	,ReasonType
	,TopLevel
)

select
	ID
	,Active
	,CancelledBy
	,Clinical
	,Code
	,Description
	,ReasonType
	,TopLevel
from
	openquery(' + @LinkedServer + ', ''
select
	ID
	,Active
	,CancelledBy
	,Clinical
	,Code
	,Description
	,ReasonType
	,TopLevel
from
	Theatre.CancelReasons
''
)
'

exec (@sql)


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'ExtractCacheTheatreReferenceData', @Stats, @StartTime
