﻿CREATE procedure [dbo].[ExtractInquireAPC]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @sql1 varchar(8000)
declare @sql2 varchar(8000)
declare @sql3 varchar(8000)
declare @sql4 varchar(8000)
declare @sql5 varchar(8000)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112) + '0000'

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112) + '2400'

select @census = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112)

select
	@sql1 = '
insert into TImportAPCEncounter
(
	 SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,DischargeDate
	,EpisodeStartDate
	,EpisodeEndDate
	,StartSiteCode
	,StartWardTypeCode
	,EndSiteCode
	,EndWardTypeCode
	,RegisteredGpCode
	,SiteCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,AdminCategoryCode
	,ConsultantCode
	,SpecialtyCode
	,LastEpisodeInSpellIndicator
	,FirstRegDayOrNightAdmit
	,NeonatalLevelOfCare
	,PASHRGCode
	,PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode
	,PrimaryOperationCode
	,PrimaryOperationDate
	,ContractSerialNo
	,CodingCompleteDate
	,PurchaserCode
	,ProviderCode
	,EpisodeStartTime
	,EpisodeEndTime
	,RegisteredGdpCode
	,EpisodicGpCode
	,InterfaceCode
	,CasenoteNumber
	,NHSNumberStatusCode
	,AdmissionTime
	,DischargeTime
	,TransferFrom
	,DistrictNo
	,SourceUniqueID
	,ExpectedLOS
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,IntendedPrimaryOperationCode
	,Operation
	,Research1
	,CancelledElectiveAdmissionFlag
)'


	,@sql2 = '
select
	 SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo = SourcePatientNo + ''/'' + SourceSpellNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,PostCode
	,Address1
	,Address2
	,Address3
	,DhaCode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,case when DischargeDate = ''//'' then null else DischargeDate end DischargeDate
	,EpisodeStartDate
	,EpisodeEndDate
	,StartSiteCode
	,StartWardType
	,EndSiteCode
	,EndWardType
	,RegisteredGpCode
	,HospitalCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,rtrim(PatientClassificationCode) PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,SourceAdminCategoryCode
	,ConsultantCode
	,SpecialtyCode
	,LastEpisodeInSpellIndicator
	,left(FirstRegDayOrNightAdmit, 1) FirstRegDayOrNightAdmit
	,NeonatalLevelOfCare
	,SourceHRGCode
	,PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode
	,PrimaryOperationCode
	,PrimaryOperationDate
	,SourceContractSerialNo
	,CodingCompleteDate
	,SourcePurchaserCode
	,SourceProviderCode

	,
	case
	when EpisodeStartTime = '':'' then null
	else EpisodeStartDate + '' '' + EpisodeStartTime
	end EpisodeStartTime

	,
	case
	when EpisodeEndTime = '':'' then null
	else EpisodeEndDate + '' '' + EpisodeEndTime
	end EpisodeEndTime

	,RegisteredGdpCode
	,EpisodicGpCode
	,''INQ'' InterfaceCode
	,CasenoteNumber
	,NHSNumberStatusCode

	,
	case
	when AdmissionTime = '':'' then null
	else AdmissionDate + '' '' + AdmissionTime
	end AdmissionTime

	,
	case
	when DischargeTime = '':'' then null
	else DischargeDate + '' '' + DischargeTime
	end DischargeTime

	,TransferFrom
	,DistrictNo
	,SourceKey
	,ExpectedLOS
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,IntendedPrimaryOperationCode
	,Operation
	,Research1
	,CEA

from
	openquery(INQUIRE, '''

	,@sql3 = '

SELECT 
	FCE.FCEEXTID SourceKey,
	P.DistrictNumber DistrictNo,
	FCE.InternalPatientNumber SourcePatientNo,
	FCE.EpisodeNumber SourceSpellNo,
	FCE.FceSequenceNo SourceEncounterNo,
	P.Title PatientTitle,
	P.Forenames PatientForename,
	P.Surname PatientSurname,
	P.PtDoB DateOfBirth,
	P.PtDateOfDeath DateOfDeath,
	P.Sex SexCode,
	P.NHSNumber,
	P.PtAddrPostCode PostCode,
	P.PtAddrLine1 Address1,
	P.PtAddrLine2 Address2,
	P.PtAddrLine3 Address3,
	P.DistrictOfResidenceCode DhaCode,
	P.EthnicType EthnicOriginCode,
	P.MaritalStatus MaritalStatusCode,
	P.Religion ReligionCode,
	A.WlDateCcyy DateOnWaitingList,
	A.AdmissionDate AdmissionDate,
	A.DischargeDate DischargeDate,
	FCE.FCEEndDate EpisodeEndDate,
	case when FCE.FCEEndTime = "24:00" then "23:59" else FCE.FCEEndTime end EpisodeEndTime,
	FCE.FCEStartDate EpisodeStartDate,
	case when FCE.FCEStartTime = "24:00" then "23:59" else FCE.FCEStartTime end EpisodeStartTime,
	FCE.GpCode RegisteredGpCode,
	A.HospCode HospitalCode,
	A.MethodOfAdmission AdmissionMethodCode,
	A.SourceOfAdm AdmissionSourceCode,
	A.IntdMgmt ManagementIntentionCode,
	A.MethodOfDischarge DischargeMethodCode,
	A.DestinationOnDischarge DischargeDestinationCode,
	FCE.Consultant ConsultantCode,
	FCE.Specialty SpecialtyCode,
	FCE.KornerEpisodePrimaryDiagnosisCode PrimaryDiagnosisCode,
	FCE.Subsid SubsidiaryDiagnosisCode,
	FCE.KornerEpisodePrimaryProcedureCode PrimaryOperationCode,
	FCE.KornerEpisodePrimaryProcedureDateExternal PrimaryOperationDate,
	A.TransFrom TransferFrom,
	P.GdpCode RegisteredGdpCode,
	case
	when FCE.IntdMgmt="I" then "1"
	when FCE.IntdMgmt="B" then "1"
	when FCE.IntdMgmt="D" AND FCE.MethodOfAdmission Not In 
		(
		"WL",
		"EL",
		"BA",
		"BL",
		"BP",
		"PA",
		"PL"
		) then "1"
	when FCE.IntdMgmt="D" AND A.DischargeDate Is Null then "1"
	when FCE.IntdMgmt="D" AND A.AdmissionDate <> A.DischargeDate then "1"
	when FCE.IntdMgmt="D" AND A.AdmissionDate = A.DischargeDate then "2"
	when FCE.IntdMgmt="V" then "1"
	when FCE.IntdMgmt="R" then "3"
	when FCE.IntdMgmt="N" then "4"
	end PatientClassificationCode,
	FCE.EpisodicGP EpisodicGpCode,
	A.CaseNoteNumber CasenoteNumber,
	P.NHSNumberStatus NHSNumberStatusCode,
	A.Category SourceAdminCategoryCode,
	A.A1stRegDaynightAdm FirstRegDayOrNightAdmit,
	case when A.AdmissionTime = "24:00" then "23:59" else A.AdmissionTime end AdmissionTime,
	case when A.DischargeTime = "24:00" then "23:59" else A.DischargeTime end DischargeTime,
	left(SPEC.DohCode, 4) NationalSpecialtyCode,
	Episode.AdtNeonatalLevelOfCareInt NeonatalLevelOfCare,
	FCE.HrgCode SourceHRGCode,
	case when FCE.FCEEndDate = A.DischargeDate then "Y" else "N" end LastEpisodeInSpellIndicator,
	FCE.ProviderCode SourceProviderCode,
	FCE.PurchaserCode SourcePurchaserCode,
	FCE.ContractId SourceContractSerialNo,
	FCE.WardCdadmit StartWardType,
	FCE.WardCdend EndWardType,
	A.ExpectedLos
	,P.MRSA MRSAFlag

	,PW.PathwayNumber RTTPathwayID
	,PW.PathwayCondition RTTPathwayCondition
	,PW.RTTStartDate
	,PW.RTTEndDate
	,PW.RTTSpeciality RTTSpecialtyCode
	,PW.RTTCurProv RTTCurrentProviderCode
	,PW.RTTCurrentStatus RTTCurrentStatusCode
	,PW.RTTCurrentStatusDate
	,PW.RTTPrivatePat RTTCurrentPrivatePatientFlag
	,PW.RTTOSVStatus RTTOverseasStatusFlag

	,A.RTTPeriodStatus RTTPeriodStatusCode
	,WL.IntendedProc IntendedPrimaryOperationCode
	,WL.Operation
	,FCE.HospCdAdmit StartSiteCode
	,FCE.HospCdEnd EndSiteCode

	,WL.CEA
	,Episode.CodingCompleteDt CodingCompleteDate
	,Episode.Research1

FROM
	FCEEXT FCE

INNER JOIN ADMITDISCH A 
ON	FCE.EpisodeNumber = A.EpisodeNumber
AND	FCE.InternalPatientNumber = A.InternalPatientNumber

INNER JOIN PATDATA P 
ON	FCE.InternalPatientNumber = P.InternalPatientNumber

INNER JOIN CONSEPISODE Episode 
ON	FCE.FCEStartDTimeInt = Episode.EpsActvDtimeInt
AND	FCE.EpisodeNumber = Episode.EpisodeNumber
AND	FCE.InternalPatientNumber = Episode.InternalPatientNumber

INNER JOIN SPEC 
ON	FCE.Specialty = SPEC.SPECID

left join RTTPTEPIPATHWAY EPW
on	EPW.InternalPatientNumber = A.InternalPatientNumber
and	EPW.EpisodeNumber = A.EpisodeNumber

left join RTTPTPATHWAYCURRENT PW
on	PW.InternalPatientNumber = EPW.InternalPatientNumber
and	PW.PathwayNumber = EPW.PathwayNumber

left join WLENTRY WL
on	WL.InternalPatientNumber = FCE.InternalPatientNumber
and	WL.EpisodeNumber = FCE.EpisodeNumber

WHERE
	FCE.EpsActvDtimeInt between ' + @from + ' and ' + @to + '
'')
'

-- open episodes
	,@sql4 = '
union all

select
	 SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo = SourcePatientNo + ''/'' + SourceSpellNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,PostCode
	,Address1
	,Address2
	,Address3
	,DhaCode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,null DischargeDate
	,EpisodeStartDate
	,null EpisodeEndDate
	,StartSiteCode
	,StartWardType
	,EndSiteCode
	,EndWardType
	,RegisteredGpCode
	,HospitalCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,rtrim(PatientClassificationCode) PatientClassificationCode
	,ManagementIntentionCode
	,null DischargeMethodCode
	,null DischargeDestinationCode
	,SourceAdminCategoryCode
	,ConsultantCode
	,SpecialtyCode
	,null LastEpisodeInSpellIndicator
	,left(FirstRegDayOrNightAdmit, 1) FirstRegDayOrNightAdmit
	,NeonatalLevelOfCare
	,SourceHRGCode
	,null PrimaryDiagnosisCode
	,null SubsidiaryDiagnosisCode
	,null PrimaryOperationCode
	,null PrimaryOperationDate
	,null SourceContractSerialNo
	,CodingCompleteDate
	,null SourcePurchaserCode
	,null SourceProviderCode

	,
	case
	when EpisodeStartTime = '':'' then null
	else EpisodeStartDate + '' '' + EpisodeStartTime
	end EpisodeStartTime

	,null EpisodeEndTime
	,RegisteredGdpCode
	,EpisodicGpCode
	,''INQ'' InterfaceCode
	,CasenoteNumber
	,NHSNumberStatusCode

	,
	case
	when AdmissionTime = '':'' then null
	else AdmissionDate + '' '' + AdmissionTime
	end AdmissionTime

	,null DischargeTime
	,TransferFrom
	,DistrictNo
	,SourceKey
	,ExpectedLOS
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,IntendedPrimaryOperationCode
	,Operation
	,Research1
	,CEA
from
	openquery(INQUIRE, ''
'

	,@sql5 = '
SELECT  
	Episode.CONSEPISODEID SourceKey,
	P.DistrictNumber DistrictNo,
	M.InternalPatientNumber SourcePatientNo,
	M.EpisodeNumber SourceSpellNo,
	"999" SourceEncounterNo,
	P.Title PatientTitle,
	P.Forenames PatientForename,
	P.Surname PatientSurname,
	P.PtDoB DateOfBirth,
	P.PtDateOfDeath DateOfDeath,
	P.Sex SexCode,
	P.NHSNumber,
	P.PtAddrPostCode PostCode,
	P.PtAddrLine1 Address1,
	P.PtAddrLine2 Address2,
	P.PtAddrLine3 Address3,
	P.HealthAuthorityCode DhaCode,
	P.EthnicType EthnicOriginCode,
	P.MaritalStatus MaritalStatusCode,
	P.Religion ReligionCode,
	A.WlDateCcyy DateOnWaitingList,
	A.AdmissionDate AdmissionDate,
	Episode.EpisodeStartDate,
	case when Episode.EpisodeStartTime = "24:00" then "23:59" else Episode.EpisodeStartTime end EpisodeStartTime,
	P.GpCode RegisteredGpCode,
	A.HospCode HospitalCode,
	A.MethodOfAdmission AdmissionMethodCode,
	A.SourceOfAdm AdmissionSourceCode,
	A.IntdMgmt ManagementIntentionCode,
	A.MethodOfDischarge DischargeMethodCode,
	A.DestinationOnDischarge DischargeDestinationCode,
	M.Consultant ConsultantCode,
	M.Specialty SpecialtyCode,
	A.TransFrom TransferFrom,
	P.GdpCode RegisteredGdpCode,
	"1" PatientClassificationCode,
	A.CaseNoteNumber CasenoteNumber,
	P.NHSNumberStatus NHSNumberStatusCode,
	M.Category SourceAdminCategoryCode,
	A.A1stRegDaynightAdm FirstRegDayOrNightAdmit,
	case when A.AdmissionTime = "24:00" then "23:59" else A.AdmissionTime end AdmissionTime,
	left(SPEC.DohCode, 4) NationalSpecialtyCode,
	Episode.AdtNeonatalLevelOfCareInt NeonatalLevelOfCare,
	Episode.HRGCode SourceHRGCode,
	A.AdmWard StartWardType,
	(
	select
		WardStay.WardCode
	from
		WARDSTAY WardStay
	where
		WardStay.InternalPatientNumber = A.InternalPatientNumber
	and	WardStay.EpisodeNumber = A.EpisodeNumber
	and	not exists
		(
		select
			1
		from
			WARDSTAY LastWardStay
		where
			WardStay.InternalPatientNumber = LastWardStay.InternalPatientNumber
		and	WardStay.EpisodeNumber = LastWardStay.EpisodeNumber
		and	WardStay.EpsActvDtimeInt < LastWardStay.EpsActvDtimeInt
		)
	) EndWardType,
	A.ExpectedLos
	,P.MRSA MRSAFlag

	,PW.PathwayNumber RTTPathwayID
	,PW.PathwayCondition RTTPathwayCondition
	,PW.RTTStartDate
	,PW.RTTEndDate
	,PW.RTTSpeciality RTTSpecialtyCode
	,PW.RTTCurProv RTTCurrentProviderCode
	,PW.RTTCurrentStatus RTTCurrentStatusCode
	,PW.RTTCurrentStatusDate
	,PW.RTTPrivatePat RTTCurrentPrivatePatientFlag
	,PW.RTTOSVStatus RTTOverseasStatusFlag

	,A.RTTPeriodStatus RTTPeriodStatusCode
	,WL.IntendedProc IntendedPrimaryOperationCode
	,WL.Operation
	,A.HospCode StartSiteCode
	,M.HospitalCode EndSiteCode
	,WL.CEA
	,Episode.CodingCompleteDt CodingCompleteDate
	,Episode.Research1
	,A.EpiGPCode EpisodicGpCode

FROM
	MIDNIGHTBEDSTATE M

INNER JOIN PATDATA P 
ON	M.InternalPatientNumber = P.InternalPatientNumber

INNER JOIN ADMITDISCH A 
ON	M.EpisodeNumber = A.EpisodeNumber
AND	M.InternalPatientNumber =  A.InternalPatientNumber

INNER JOIN CONSEPISODE  Episode 
ON	M.EpisodeNumber = Episode.EpisodeNumber
AND	M.InternalPatientNumber = Episode.InternalPatientNumber

LEFT JOIN SPEC 
ON	M.Specialty = SPEC.SPECID

left join RTTPTEPIPATHWAY EPW
on	EPW.InternalPatientNumber = A.InternalPatientNumber
and	EPW.EpisodeNumber = A.EpisodeNumber

left join RTTPTPATHWAYCURRENT PW
on	PW.InternalPatientNumber = EPW.InternalPatientNumber
and	PW.PathwayNumber = EPW.PathwayNumber

left join WLENTRY WL
on	WL.InternalPatientNumber = M.InternalPatientNumber
and	WL.EpisodeNumber = M.EpisodeNumber

WHERE
	Episode.EpsActvDtimeInt <= ' + @to + ' 
AND	(
		Episode.ConsultantEpisodeEndDttm > ' + @to + ' 
	or	Episode.ConsultantEpisodeEndDttm Is Null
	)
AND	M.StatisticsDate = ' + @census + '
'')
'


if @debug = 1 
begin
	print @sql1
	print @sql2
	print @sql3
	print @sql4
	print @sql5
end
else
begin
	exec (@sql1 + @sql2 + @sql3 + @sql4 + @sql5)

	select
		@RowsInserted = @RowsInserted + @@ROWCOUNT

	SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

	SELECT @Stats = 
		'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
		'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

	EXEC WriteAuditLogEvent 'ExtractInquireAPC', @Stats, @StartTime

end
