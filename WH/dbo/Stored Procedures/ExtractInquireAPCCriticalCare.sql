﻿CREATE procedure [dbo].[ExtractInquireAPCCriticalCare]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @sql1 varchar(8000)
declare @from varchar(12)
declare @to varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112) + '0000'

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112) + '2400'

select
	@sql1 = '
insert into TImportAPCCriticalCare
(
	 SourceUniqueID
	,AdvancedCardiovascularSupportDays
	,AdvancedRespiratorySupportDays
	,BasicCardiovascularSupportDays
	,BasicRespiratorySupportDays
	,CriticalCareLevel2Days
	,CriticalCareLevel3Days
	,CreatedByUser
	,CreatedByTime
	,DermatologicalSupportDays
	,EndDate
	,EndTimeInt
	,LiverSupportDays
	,LocalIdentifier
	,LocationCode
	,NeurologicalSupportDays
	,RenalSupportDays
	,StartDate
	,StartDateInt
	,StartTime
	,StatusCode
	,TreatmentFunctionCode
	,SourceSpellNo
	,SourcePatientNo
	,PlannedAcpPeriod
)
select
	 CRICAREPATDETID
	,CcpAdvCarLevDays
	,CcpAdvResLevDays
	,CcpBasCarLevDays
	,CcpBasResLevDays
	,CcpCcLevel2Days
	,CcpCcLevel3Days
	,CcpCreaterevByuser
	,CcpCreaterevDtInt
	,CcpDerSupportDays
	,CcpEndDate
	,CcpEndDtInt
	,CcpLivSupportDays
	,CcpLocalIdentifier
	,CcpLocation
	,CcpNeuSupportDays
	,CcpRenSupportDays
	,CcpStartDate
	,CcpStartDtInt
	,CcpStartTime
	,CcpStatus
	,CcpTreatmentFunc
	,EpisodeNumber
	,InternalPatientNumber
	,PlannedAcpPeriod
from
	openquery(INQUIRE, ''
SELECT 
	 CRICAREPATDETID
	,CcpAdvCarLevDays
	,CcpAdvResLevDays
	,CcpBasCarLevDays
	,CcpBasResLevDays
	,CcpCcLevel2Days
	,CcpCcLevel3Days
	,CcpCreaterevByuser
	,CcpCreaterevDtInt
	,CcpDerSupportDays
	,CcpEndDate
	,CcpEndDtInt
	,CcpLivSupportDays
	,CcpLocalIdentifier
	,CcpLocation
	,CcpNeuSupportDays
	,CcpRenSupportDays
	,CcpStartDate
	,CcpStartDtInt
	,CcpStartTime
	,CcpStatus
	,CcpTreatmentFunc
	,EpisodeNumber
	,InternalPatientNumber
	,PlannedAcpPeriod
FROM
	CRICAREPATDET
WHERE
	CcpStartDtInt between ' + @from + ' and ' + @to + '
'')
'

if @debug = 1 
begin
	print @sql1
end
else
begin
	exec (@sql1)

	select
		@RowsInserted = @RowsInserted + @@ROWCOUNT

	SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

	SELECT @Stats = 
		'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
		'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

	EXEC WriteAuditLogEvent 'ExtractInquireCriticalCare', @Stats, @StartTime

end
