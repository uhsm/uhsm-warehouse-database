﻿CREATE procedure [dbo].[ExtractInquireAPCDiagnosis]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @sql1 varchar(8000)
declare @sql2 varchar(8000)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112) + '0000'

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112) + '2400'

select @census = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112)

select
	@sql1 = '
insert into TImportAPCDiagnosis
(
	 SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,DiagnosisCode
	,APCSourceUniqueID
)
select
	 SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,DiagnosisCode
	,APCSourceUniqueID
from
	openquery(INQUIRE, ''
SELECT 
	 CONSEPISDIAG.CONSEPISDIAGID SourceUniqueID
	,CONSEPISDIAG.EpisodeNumber SourceSpellNo
	,CONSEPISDIAG.InternalPatientNumber SourcePatientNo
	,CONSEPISDIAG.FCENumber SourceEncounterNo
	,CONSEPISDIAG.SecondaryDiagSeqNo SequenceNo
	,CONSEPISDIAG.SecondaryDiagnosis DiagnosisCode
	,FCE.FCEEXTID APCSourceUniqueID
FROM
	CONSEPISDIAG

INNER JOIN FCEEXT FCE 
ON	FCE.EpisodeNumber = CONSEPISDIAG.EpisodeNumber
AND	FCE.InternalPatientNumber = CONSEPISDIAG.InternalPatientNumber
AND	FCE.FCESequenceNo = CONSEPISDIAG.FCENumber

WHERE
	FCE.EpsActvDtimeInt between ' + @from + ' and ' + @to + '
'')
'

	,@sql2 = '
union all

select
	 SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,DiagnosisCode
	,APCSourceUniqueID
from
	openquery(INQUIRE, ''
SELECT 
	 CONSEPISDIAG.CONSEPISDIAGID SourceUniqueID
	,CONSEPISDIAG.EpisodeNumber SourceSpellNo
	,CONSEPISDIAG.InternalPatientNumber SourcePatientNo
	,CONSEPISDIAG.FCENumber SourceEncounterNo
	,CONSEPISDIAG.SecondaryDiagSeqNo SequenceNo
	,CONSEPISDIAG.SecondaryDiagnosis DiagnosisCode
	,Episode.CONSEPISODEID APCSourceUniqueID
FROM
	CONSEPISDIAG

INNER JOIN MIDNIGHTBEDSTATE M 
ON	M.EpisodeNumber = CONSEPISDIAG.EpisodeNumber
AND	M.InternalPatientNumber =  CONSEPISDIAG.InternalPatientNumber

INNER JOIN CONSEPISODE Episode 
ON	M.EpisodeNumber = Episode.EpisodeNumber
AND	M.InternalPatientNumber = Episode.InternalPatientNumber
AND	Episode.FCENumber = CONSEPISDIAG.FCENumber

WHERE
	Episode.EpsActvDtimeInt <= ' + @to + ' 
AND	(
		Episode.ConsultantEpisodeEndDttm > ' + @to + ' 
	or	Episode.ConsultantEpisodeEndDttm Is Null
	)
AND	M.StatisticsDate = ' + @census + '
'')
'

if @debug = 1 
begin
	print @sql1
	print @sql2
end
else
begin
	exec (@sql1 + @sql2)

	select
		@RowsInserted = @RowsInserted + @@ROWCOUNT

	SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

	SELECT @Stats = 
		'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
		'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

	EXEC WriteAuditLogEvent 'ExtractInquireAPCDiagnosis', @Stats, @StartTime

end
