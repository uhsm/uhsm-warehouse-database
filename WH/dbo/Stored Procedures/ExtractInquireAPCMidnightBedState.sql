﻿create procedure [dbo].[ExtractInquireAPCMidnightBedState] 
	 @censusDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @sql1 varchar(8000)
declare @sql2 varchar(8000)
declare @sql3 varchar(8000)
declare @census varchar(8)

select @StartTime = getdate()

select @RowsInserted = 0

select @census = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@censusDate, dateadd(day, -1, getdate()))), 0), 112)

select
	@sql1 = '
insert into TImportAPCMidnightBedState
(
	 SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,ProviderSpellNo
	,SiteCode
	,WardCode
	,ConsultantCode
	,SpecialtyCode
	,SourceAdminCategoryCode
	,ActivityInCode
	,AdmissionDateTimeInt
	,MidnightBedStateDate
	,InterfaceCode
)'


	,@sql2 = '

select
	 SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,ProviderSpellNo = SourcePatientNo + ''/'' + SourceSpellNo
	,SiteCode
	,WardCode
	,ConsultantCode
	,SpecialtyCode
	,SourceAdminCategoryCode
	,ActivityInCode
	,AdmissionDateTimeInt
	,MidnightBedStateDate
	,InterfaceCode = ''INQ''
from
	openquery(INQUIRE, ''
'

	,@sql3 = '
SELECT  
	 MIDNIGHTBEDSTATEID SourceUniqueID
	,InternalPatientNumber SourcePatientNo
	,EpisodeNumber SourceSpellNo
	,Consultant ConsultantCode
	,Specialty SpecialtyCode
	,Category SourceAdminCategoryCode
	,HospitalCode SiteCode
	,ActivityIn ActivityInCode
	,IpAdmDtimeInt AdmissionDateTimeInt
	,StatisticsDate MidnightBedStateDate
	,Ward WardCode
FROM
	MIDNIGHTBEDSTATE
WHERE
	StatisticsDate = ' + @census + '
'')
'


if @debug = 1 
begin
	print @sql1
	print @sql2
	print @sql3
end
else
begin
	exec (@sql1 + @sql2 + @sql3)

	select
		@RowsInserted = @RowsInserted + @@ROWCOUNT

	SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

	SELECT @Stats = 
		'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
		'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

	EXEC WriteAuditLogEvent 'ExtractInquireMidnightBedState', @Stats, @StartTime

end
