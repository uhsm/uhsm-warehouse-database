﻿CREATE procedure [dbo].[ExtractInquireAPCWardStay]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @sql1 varchar(8000)
declare @sql2 varchar(8000)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112) + '0000'

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112) + '2400'

select @census = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112)

select
	@sql1 = '
insert into TImportAPCWardStay
(
	 SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo
	,StartDate
	,EndDate
	,StartTime
	,EndTime
	,SiteCode
	,WardCode
	,StartActivityCode
	,EndActivityCode
)
select
	 SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo
	,StartDate
	,EndDate
	,StartTime
	,EndTime
	,SiteCode
	,WardCode
	,StartActivityCode
	,EndActivityCode
from
	openquery(INQUIRE, ''
SELECT 
	 WARDSTAY.WARDSTAYID SourceUniqueID
	,WARDSTAY.EpisodeNumber SourceSpellNo
	,WARDSTAY.InternalPatientNumber SourcePatientNo
	,WARDSTAY.StartDate
	,WARDSTAY.EndDate
	,WARDSTAY.StartTime
	,WARDSTAY.EndTime
	,WARDSTAY.HospitalCode SiteCode
	,WARDSTAY.WardCode
	,WARDSTAY.StartActivity StartActivityCode
	,WARDSTAY.EndActivity EndActivityCode
FROM
	WARDSTAY

INNER JOIN FCEEXT FCE 
ON	FCE.FCEStartDTimeInt = WARDSTAY.EpsActvDtimeInt
AND	FCE.EpisodeNumber = WARDSTAY.EpisodeNumber
AND	FCE.InternalPatientNumber = WARDSTAY.InternalPatientNumber

WHERE
	FCE.EpsActvDtimeInt between ' + @from + ' and ' + @to + '
'')
'
	,@sql2 = '

union all

select
	 SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo
	,StartDate
	,EndDate
	,StartTime
	,EndTime
	,SiteCode
	,WardCode
	,StartActivityCode
	,EndActivityCode
from
	openquery(INQUIRE, ''
SELECT 
	 WARDSTAY.WARDSTAYID SourceUniqueID
	,WARDSTAY.EpisodeNumber SourceSpellNo
	,WARDSTAY.InternalPatientNumber SourcePatientNo
	,WARDSTAY.StartDate
	,WARDSTAY.EndDate
	,WARDSTAY.StartTime
	,WARDSTAY.EndTime
	,WARDSTAY.HospitalCode SiteCode
	,WARDSTAY.WardCode
	,WARDSTAY.StartActivity StartActivityCode
	,WARDSTAY.EndActivity EndActivityCode
FROM
	WARDSTAY

INNER JOIN MIDNIGHTBEDSTATE M 
ON	M.EpisodeNumber = WARDSTAY.EpisodeNumber
AND	M.InternalPatientNumber =  WARDSTAY.InternalPatientNumber

WHERE
	WARDSTAY.EpsActvDtimeInt <= ' + @to + '  
AND	(
		WARDSTAY.EndDtTimeInt > ' + @to + '  
	or	WARDSTAY.EndDtTimeInt Is Null
	)
AND	M.StatisticsDate = ' + @census + '
'')
'

if @debug = 1 
begin
	print @sql1
	print @sql2
end
else
begin
	exec (@sql1 + @sql2)

	select
		@RowsInserted = @RowsInserted + @@ROWCOUNT

	SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

	SELECT @Stats = 
		'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
		'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

	EXEC WriteAuditLogEvent 'ExtractInquireWardStay', @Stats, @StartTime

end
