﻿CREATE procedure [dbo].[ExtractInquireOP]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @sql1 varchar(8000)
declare @sql2 varchar(8000)
declare @sql3 varchar(8000)
declare @sql4 varchar(8000)
declare @sql5 varchar(8000)
declare @sql6 varchar(8000)
declare @from varchar(12)
declare @to varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112)

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112)

select
	@sql1 = '
insert into TImportOPEncounter
(
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AppointmentDate
	,AppointmentTime
	,ClinicCode
	,AdminCategoryCode
	,SourceOfReferralCode
	,ReasonForReferralCode
	,PriorityCode
	,FirstAttendanceFlag
	,AppointmentStatusCode
	,CancelledByCode
	,TransportRequiredFlag
	,AppointmentTypeCode
	,DisposalCode
	,ConsultantCode
	,SpecialtyCode
	,ReferringConsultantCode
	,ReferringSpecialtyCode
	,BookingTypeCode
	,CasenoteNo
	,AppointmentCreateDate
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,DoctorCode
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,ReferralDate
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,AppointmentCategoryCode
	,AppointmentCreatedBy
	,AppointmentCancelDate
	,LastRevisedDate
	,LastRevisedBy
	,OverseasStatusFlag
	,PatientChoiceCode
	,ScheduledCancelReasonCode
	,PatientCancelReason
	,DischargeDate
	,QM08StartWaitDate
	,QM08EndWaitDate
	,DestinationSiteCode
	,EBookingReferenceNo
	,IsWardAttender
	,InterfaceCode
)
'


	,@sql2 = '
select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AppointmentDate
	,AppointmentTime
	,ClinicCode
	,AdminCategoryCode
	,SourceOfReferralCode
	,ReasonForReferralCode
	,PriorityCode
	,FirstAttendanceFlag
	,AppointmentStatusCode
	,CancelledByCode
	,TransportRequiredFlag
	,AppointmentTypeCode
	,DisposalCode
	,ConsultantCode
	,SpecialtyCode
	,ReferringConsultantCode
	,ReferringSpecialtyCode
	,BookingTypeCode
	,CasenoteNo
	,AppointmentCreateDate
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,DoctorCode
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,ReferralDate
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,AppointmentCategoryCode
	,AppointmentCreatedBy
	,AppointmentCancelDate
	,LastRevisedDate
	,LastRevisedBy
	,OverseasStatusFlag
	,PatientChoiceCode
	,ScheduledCancelReasonCode
	,PatientCancelReason
	,DischargeDate
	,QM08StartWaitDate
	,QM08EndWaitDate
	,DestinationSiteCode
	,EBookingReferenceNo
	,IsWardAttender = 0
	,InterfaceCode = ''INQ''
from
	openquery(INQUIRE, '''

	,@sql3 = '

SELECT
	 OPA.OPAID SourceUniqueID
	,OPA.InternalPatientNumber SourcePatientNo
	,OPA.EpisodeNumber SourceEncounterNo
	,Patient.Title PatientTitle
	,Patient.Forenames PatientForename
	,Patient.Surname PatientSurname
	,Patient.PtDoB DateOfBirth
	,Patient.PtDateOfDeath DateOfDeath
	,Patient.Sex SexCode
	,Patient.NHSNumber NHSNumber
	,OPA.DistrictNumber DistrictNo
	,Patient.PtAddrPostCode Postcode
	,Patient.PtAddrLine1 PatientAddress1
	,Patient.PtAddrLine2 PatientAddress2
	,Patient.PtAddrLine3 PatientAddress3
	,Patient.PtAddrLine4 PatientAddress4
	,OPA.HaCode DHACode
	,Patient.EthnicType EthnicOriginCode
	,Patient.MaritalStatus MaritalStatusCode
	,Patient.Religion ReligionCode
	,Patient.GpCode RegisteredGpCode
	,OPA.RegGpCode RegisteredGpPracticeCode
	,OPREFERRAL.HospitalCode SiteCode
	,OPA.ApptDate AppointmentDate
	,OPA.ApptTime AppointmentTime
	,OPA.ClinicCode ClinicCode
	,OPA.PtCategory AdminCategoryCode
	,OPA.RefBy SourceOfReferralCode
	,OPA.ReasonForRef ReasonForReferralCode
	,OPA.RefPriority PriorityCode
	,OPA.ApptClass FirstAttendanceFlag
	,OPA.ApptStatus AppointmentStatusCode
	,OPA.CancelBy CancelledByCode
	,OPA.Transport TransportRequiredFlag
	,OPA.ApptType AppointmentTypeCode
	,OPA.Disposal DisposalCode
	,OPA.ClinicConsultant ConsultantCode
	,OPA.ClinicSpecialty SpecialtyCode
	,OPA.RefConsultant ReferringConsultantCode
	,OPA.RefSpecialty ReferringSpecialtyCode
	,OPA.BookingType BookingTypeCode
	,OPA.CaseNoteNumber CasenoteNo
	,OPA.ApptBookedDate AppointmentCreateDate
	,OPA.EpiGp EpisodicGpCode
	,OPA.PracticeCode EpisodicGpPracticeCode
	,OPA.ResCode DoctorCode
	,OPA.RefPrimaryDiagnosisCode PrimaryDiagnosisCode
	,OPA.ApptPrimaryProcedureCode PrimaryOperationCode
	,OPA.ApptPurchaser PurchaserCode
	,OUTCLNC.ProviderCode ProviderCode
	,OPA.ApptConractId ContractSerialNo
	,OPA.ReferralDate ReferralDate
	,PW.PathwayNumber RTTPathwayID
	,PW.PathwayCondition RTTPathwayCondition
	,PW.RTTStartDate
	,PW.RTTEndDate
	,PW.RTTSpeciality RTTSpecialtyCode
	,PW.RTTCurProv RTTCurrentProviderCode
	,PW.RTTCurrentStatus RTTCurrentStatusCode
	,PW.RTTCurrentStatusDate
	,PW.RTTPrivatePat RTTCurrentPrivatePatientFlag
	,PW.RTTOSVStatus RTTOverseasStatusFlag
	,OPA.RTTPeriodStatus RTTPeriodStatusCode
	,OPA.ApptCategory AppointmentCategoryCode
	,OPA.ApptEnteredUID AppointmentCreatedBy
	,OPA.ApptCancDate AppointmentCancelDate
	,OPA.LastRevisionDTime LastRevisedDate
	,OPA.LastRevisionUID LastRevisedBy
	,OPA.OSVStatus OverseasStatusFlag
	,OPA.PatChoice PatientChoiceCode
	,Attend.SchReasonForCancellationCodeAppointment ScheduledCancelReasonCode
	,Attend.PatientAppointmentCancellationReason PatientCancelReason
	,OPREFERRAL.DischargeDt DischargeDate
	,OPREFERRAL.QM08StartWaitDate QM08StartWaitDate
	,OPREFERRAL.QM08EndWtDate QM08EndWaitDate
	,OUTCLNC.DestinationHospCd DestinationSiteCode
	,OPA.EbookingReferenceNumber EBookingReferenceNo
FROM
	OPA

INNER JOIN PATDATA Patient
ON	OPA.InternalPatientNumber = Patient.InternalPatientNumber

INNER JOIN OPREFERRAL 
ON	OPA.InternalPatientNumber = OPREFERRAL.InternalPatientNumber
AND	OPA.EpisodeNumber = OPREFERRAL.EpisodeNumber
AND	OPA.ReferralDateInt = OPREFERRAL.OpRegDtimeInt

INNER JOIN OUTCLNC
ON	OPA.ClinicCode = OUTCLNC.OUTCLNCID

left JOIN OPAPPOINTATTEND Attend 
ON	OPA.InternalPatientNumber = Attend.InternalPatientNumber
AND	OPA.EpisodeNumber = Attend.EpisodeNumber
AND	OPA.PtApptStartDtimeInt = Attend.PtApptStartDtimeInt
AND	OPA.ResCode = Attend.ResCode

left join RTTPTEPIPATHWAY EPW
on	EPW.InternalPatientNumber = OPA.InternalPatientNumber
and	EPW.EpisodeNumber = OPA.EpisodeNumber

left join RTTPTPATHWAYCURRENT PW
on	PW.InternalPatientNumber = EPW.InternalPatientNumber
and	PW.PathwayNumber = EPW.PathwayNumber
WHERE
	OPA.PtApptStartDtimeInt between ' + @from + '0000' + ' and ' + @to + '2400' + '
'')
'

	,@sql4 = '
insert into TImportOPEncounter
(
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AppointmentDate
	,AppointmentTime
	,ClinicCode
	,AdminCategoryCode
	,SourceOfReferralCode
	,ReasonForReferralCode
	,PriorityCode
	,FirstAttendanceFlag
	,AppointmentStatusCode
	,CancelledByCode
	,TransportRequiredFlag
	,AppointmentTypeCode
	,DisposalCode
	,ConsultantCode
	,SpecialtyCode
	,ReferringConsultantCode
	,ReferringSpecialtyCode
	,BookingTypeCode
	,CasenoteNo
	,AppointmentCreateDate
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,DoctorCode
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,ReferralDate
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,AppointmentCategoryCode
	,AppointmentCreatedBy
	,AppointmentCancelDate
	,LastRevisedDate
	,LastRevisedBy
	,OverseasStatusFlag
	,PatientChoiceCode
	,ScheduledCancelReasonCode
	,PatientCancelReason
	,DischargeDate
	,QM08StartWaitDate
	,QM08EndWaitDate
	,DestinationSiteCode
	,EBookingReferenceNo
	,IsWardAttender
	,InterfaceCode
)
'
	,@sql5 = '

select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AppointmentDate
	,AppointmentTime
	,ClinicCode
	,AdminCategoryCode
	,SourceOfReferralCode
	,ReasonForReferralCode
	,PriorityCode
	,FirstAttendanceFlag
	,AppointmentStatusCode
	,CancelledByCode
	,TransportRequiredFlag
	,AppointmentTypeCode
	,DisposalCode
	,ConsultantCode
	,SpecialtyCode
	,ReferringConsultantCode
	,ReferringSpecialtyCode
	,BookingTypeCode
	,CasenoteNo
	,AppointmentCreateDate
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,DoctorCode
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,ReferralDate
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,AppointmentCategoryCode
	,AppointmentCreatedBy
	,AppointmentCancelDate
	,LastRevisedDate
	,LastRevisedBy
	,OverseasStatusFlag
	,PatientChoiceCode
	,ScheduledCancelReasonCode
	,PatientCancelReason
	,DischargeDate
	,QM08StartWaitDate
	,QM08EndWaitDate
	,DestinationSiteCode
	,EBookingReferenceNo
	,IsWardAttender = 1
	,InterfaceCode = ''INQ''
from
	openquery(INQUIRE, '''

	,@sql6 = '

SELECT
	 WARDATTENDER.WARDATTENDERID SourceUniqueID
	,WARDATTENDER.InternalPatientNumber SourcePatientNo
	,WARDATTENDER.EpisodeNumber SourceEncounterNo
	,Patient.Title PatientTitle
	,Patient.Forenames PatientForename
	,Patient.Surname PatientSurname
	,Patient.PtDoB DateOfBirth
	,Patient.PtDateOfDeath DateOfDeath
	,Patient.Sex SexCode
	,Patient.NHSNumber NHSNumber
	,WARDATTENDER.DistrictNumber DistrictNo
	,Patient.PtAddrPostCode Postcode
	,Patient.PtAddrLine1 PatientAddress1
	,Patient.PtAddrLine2 PatientAddress2
	,Patient.PtAddrLine3 PatientAddress3
	,Patient.PtAddrLine4 PatientAddress4
	,Patient.DistrictOfResidenceCode DHACode
	,Patient.EthnicType EthnicOriginCode
	,Patient.MaritalStatus MaritalStatusCode
	,Patient.Religion ReligionCode
	,Patient.GpCode RegisteredGpCode
	,null RegisteredGpPracticeCode
	,WARD.HospitalCode SiteCode
	,WARDATTENDER.AttendanceDate AppointmentDate
	,WARDATTENDER.AttendanceDate AppointmentTime
	,WARDATTENDER.Ward ClinicCode
	,WARDATTENDER.Category AdminCategoryCode
	,WARDATTENDER.RefBy SourceOfReferralCode
	,WARDATTENDER.ReasonForReferralCode ReasonForReferralCode
	,WARDATTENDER.PriorityType PriorityCode
	,WARDATTENDER.FirstAttendance FirstAttendanceFlag
	,WARDATTENDER.WaWardAttendersAttendanceStatusInternal AppointmentStatusCode
	,null CancelledByCode
	,null TransportRequiredFlag
	,"WA" AppointmentTypeCode
	,WARDATTENDER.Disposal DisposalCode
	,WARDATTENDER.Consultant ConsultantCode
	,WARDATTENDER.Specialty SpecialtyCode
	,null ReferringConsultantCode
	,null ReferringSpecialtyCode
	,null BookingTypeCode
	,WARDATTENDER.CaseNoteNumber CasenoteNo
	,null AppointmentCreateDate
	,Patient.GpCode EpisodicGpCode
	,null EpisodicGpPracticeCode
	,WARDATTENDER.WaWardAttendersDoctorCodePhysical DoctorCode
	,null PrimaryDiagnosisCode
	,WARDATTENDER.PrimaryProcedureCode PrimaryOperationCode
	,WARDATTENDER.PurchaserCode PurchaserCode
	,null ProviderCode
	,WARDATTENDER.ContractId ContractSerialNo
	,WARDATTENDER.ReferralDate ReferralDate
	,PW.PathwayNumber RTTPathwayID
	,PW.PathwayCondition RTTPathwayCondition
	,PW.RTTStartDate
	,PW.RTTEndDate
	,PW.RTTSpeciality RTTSpecialtyCode
	,PW.RTTCurProv RTTCurrentProviderCode
	,PW.RTTCurrentStatus RTTCurrentStatusCode
	,PW.RTTCurrentStatusDate
	,PW.RTTPrivatePat RTTCurrentPrivatePatientFlag
	,PW.RTTOSVStatus RTTOverseasStatusFlag
	,null RTTPeriodStatusCode
	,WARDATTENDER.Category AppointmentCategoryCode
	,null AppointmentCreatedBy
	,null AppointmentCancelDate
	,null LastRevisedDate
	,null LastRevisedBy
	,null OverseasStatusFlag
	,null PatientChoiceCode
	,null ScheduledCancelReasonCode
	,null PatientCancelReason
	,null DischargeDate
	,null QM08StartWaitDate
	,null QM08EndWaitDate
	,null DestinationSiteCode
	,null EBookingReferenceNo
FROM
	WARDATTENDER

INNER JOIN PATDATA Patient
ON	WARDATTENDER.InternalPatientNumber = Patient.InternalPatientNumber

INNER JOIN WARD
ON	WARDATTENDER.Ward = WARD.WARDID

left join RTTPTEPIPATHWAY EPW
on	EPW.InternalPatientNumber = WARDATTENDER.InternalPatientNumber
and	EPW.EpisodeNumber = WARDATTENDER.EpisodeNumber

left join RTTPTPATHWAYCURRENT PW
on	PW.InternalPatientNumber = EPW.InternalPatientNumber
and	PW.PathwayNumber = EPW.PathwayNumber
WHERE
	WARDATTENDER.AttendanceDateInternal between ' + @from + ' and ' + @to + '
and	WARDATTENDER.SeenBy = "DOCTOR"
'')
'

if @debug = 1 
begin
	print @sql1
	print @sql2
	print @sql3
	print @sql4
	print @sql5
	print @sql6
end
else
begin
	exec (@sql1 + @sql2 + @sql3)

	select
		@RowsInserted = @RowsInserted + @@ROWCOUNT

	exec (@sql4 + @sql5 + @sql6)

	select
		@RowsInserted = @RowsInserted + @@ROWCOUNT

	SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

	SELECT @Stats = 
		'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
		'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

	EXEC WriteAuditLogEvent 'ExtractInquireOP', @Stats, @StartTime

end
