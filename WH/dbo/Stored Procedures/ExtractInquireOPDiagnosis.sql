﻿CREATE procedure [dbo].[ExtractInquireOPDiagnosis]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @sql1 varchar(8000)
declare @sql2 varchar(8000)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112) + '0000'

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112) + '2400'

select @census = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112)

select
	@sql1 = '
insert into TImportOPDiagnosis
(
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,DiagnosisCode
	,OPSourceUniqueID
)
select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,DiagnosisCode
	,OPSourceUniqueID
from
	openquery(INQUIRE, ''
SELECT 
	 OPDIAGNOSIS.OPDIAGNOSISID SourceUniqueID
	,OPDIAGNOSIS.EpisodeNumber SourceEncounterNo
	,OPDIAGNOSIS.InternalPatientNumber SourcePatientNo
	,OPDIAGNOSIS.OpEpisodeSecondaryDiagnosisSequenceNumber SequenceNo
	,OPDIAGNOSIS.Diagnosis DiagnosisCode
	,OPA.OPAID OPSourceUniqueID
FROM
	OPDIAGNOSIS

INNER JOIN OPA 
ON	OPA.EpisodeNumber = OPDIAGNOSIS.EpisodeNumber
AND	OPA.InternalPatientNumber = OPDIAGNOSIS.InternalPatientNumber

WHERE
	OPA.PtApptStartDtimeInt between ' + @from + ' and ' + @to + '
'')
'



if @debug = 1 
begin
	print @sql1
end
else
begin
	exec (@sql1)

	select
		@RowsInserted = @RowsInserted + @@ROWCOUNT

	SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

	SELECT @Stats = 
		'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
		'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

	EXEC WriteAuditLogEvent 'ExtractInquireOPDiagnosis', @Stats, @StartTime

end
