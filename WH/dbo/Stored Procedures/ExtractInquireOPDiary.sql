﻿CREATE procedure [dbo].[ExtractInquireOPDiary]
	 @debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @sql1 varchar(8000)
declare @sql2 varchar(8000)
declare @sql3 varchar(8000)
declare @from varchar(12)
declare @to varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0

select
	@from = convert(varchar, dateadd(year, -1, getdate()), 112)

select
	@sql1 = '

insert into TImportOPDiary
	(
	 SourceUniqueID
	,ClinicCode
	,SessionCode
	,SessionDescription
	,SessionDate
	,SessionTimeRange
	,ReasonForCancellation
	,SessionPeriod
	,Units
	,UsedUnits
	,FreeUnits
	,ValidAppointmentTypeCode
	,InterfaceCode
	,DoctorCode
	)
select
	 SourceUniqueID
	,ClinicCode
	,SessionCode
	,SessionDescription
	,SessionDate
	,SessionTimeRange
	,ReasonForCancellation
	,SessionPeriod
	,Units
	,UsedUnits
	,FreeUnits
	,ValidAppointmentTypeCode
	,InterfaceCode = ''INQ''
	,DoctorCode
from
	openquery(INQUIRE, ''

select
	 DIARYID SourceUniqueID
	,SchedResGrpActvCd ClinicCode
	,ResCode SessionCode
	,Comment SessionDescription
	,ScheduleDateNeutralFormat SessionDate
	,SchResSessTimeRange SessionTimeRange
	,ReasonForCancel ReasonForCancellation
	,SchedulingResourceSessionDescription SessionPeriod
	,Units
	,UsedUnits
	,FreeUnits
	,ValidApptTypes ValidAppointmentTypeCode
	,ResCode DoctorCode
from
	Diary
where
	ScheduleDateNeutralFormat > ' + @from + '
''
)
'

if @debug = 1 
begin
	print @sql1
end
else
begin
	exec (@sql1)

	select
		@RowsInserted = @RowsInserted + @@ROWCOUNT

	SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

	SELECT @Stats = 
		'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
		'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

	EXEC WriteAuditLogEvent 'ExtractInquireClinicDiary', @Stats, @StartTime

end
