﻿CREATE procedure [dbo].[ExtractInquireRF]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @sql1 varchar(8000)
declare @sql2 varchar(8000)
declare @sql3 varchar(8000)
declare @from varchar(12)
declare @to varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112) + '0000'

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112) + '2400'

select
	@sql1 = '
insert into TImportRFEncounter
(
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,EpisodicGdpCode
	,SiteCode
	,ConsultantCode
	,SpecialtyCode
	,SourceOfReferralCode
	,PriorityCode
	,ReferralDate
	,DischargeDate
	,DischargeReasonCode
	,DischargeReason
	,AdminCategoryCode
	,ContractSerialNo
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NextFutureAppointmentDate
	,InterfaceCode
)
'


	,@sql2 = '
select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,EpisodicGdpCode
	,SiteCode
	,ConsultantCode
	,SpecialtyCode
	,SourceOfReferralCode
	,PriorityCode
	,ReferralDate
	,DischargeDate
	,DischargeReasonCode
	,DischargeReason
	,AdminCategoryCode
	,ContractSerialNo
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NextFutureAppointmentDate
	,InterfaceCode = ''INQ''
from
	openquery(INQUIRE, '''

	,@sql3 = '

SELECT
	 OPREFERRAL.OPREFERRALID SourceUniqueID
	,OPREFERRAL.InternalPatientNumber SourcePatientNo
	,OPREFERRAL.EpisodeNumber SourceEncounterNo
	,Patient.Title PatientTitle
	,Patient.Forenames PatientForename
	,Patient.Surname PatientSurname
	,Patient.PtDoB DateOfBirth
	,Patient.PtDateOfDeath DateOfDeath
	,Patient.Sex SexCode
	,Patient.NHSNumber NHSNumber
	,OPREFERRAL.DistrictNumber DistrictNo
	,Patient.PtAddrPostCode Postcode
	,Patient.PtAddrLine1 PatientAddress1
	,Patient.PtAddrLine2 PatientAddress2
	,Patient.PtAddrLine3 PatientAddress3
	,Patient.PtAddrLine4 PatientAddress4
	,Patient.DistrictOfResidenceCode DHACode
	,Patient.EthnicType EthnicOriginCode
	,Patient.MaritalStatus MaritalStatusCode
	,Patient.Religion ReligionCode
	,Patient.GpCode RegisteredGpCode
	,null RegisteredGpPracticeCode
	,OPREFERRAL.EpiGPCode EpisodicGpCode
	,OPREFERRAL.EpiGPPracticeCode EpisodicGpPracticeCode
	,OPREFERRAL.EpiGDPCode EpisodicGdpCode
	,OPREFERRAL.HospitalCode SiteCode
	,OPREFERRAL.ConsCode ConsultantCode
	,OPREFERRAL.Specialty SpecialtyCode
	,OPREFERRAL.RefBy SourceOfReferralCode
	,OPREFERRAL.PriorityType PriorityCode
	,OPREFERRAL.ReferralDate
	,OPDISCHARGE.DischargeDatetime DischargeDate
	,OPDISCHARGE.ReasonCode DischargeReasonCode
	,OPDISCHARGE.Reason DischargeReason
	,OPREFERRAL.Category AdminCategoryCode
	,ALLOCATEDCONTRACT.ContractId ContractSerialNo
	,PW.PathwayNumber RTTPathwayID
	,PW.PathwayCondition RTTPathwayCondition
	,PW.RTTStartDate
	,PW.RTTEndDate
	,PW.RTTSpeciality RTTSpecialtyCode
	,PW.RTTCurProv RTTCurrentProviderCode
	,PW.RTTCurrentStatus RTTCurrentStatusCode
	,PW.RTTCurrentStatusDate
	,PW.RTTPrivatePat RTTCurrentPrivatePatientFlag
	,PW.RTTOSVStatus RTTOverseasStatusFlag

	,(
	select
		min(OPA.PtApptStartDtimeInt)
	from
		OPA
	where
		OPA.EpisodeNumber = OPREFERRAL.EpisodeNumber
	and	OPA.InternalPatientNumber = OPREFERRAL.InternalPatientNumber
	and OPA.CancelBy is null
	and	OPA.PtApptStartDtimeInt > ' + @to + '
	) NextFutureAppointmentDate

FROM
	OPREFERRAL

INNER JOIN PATDATA Patient
ON	OPREFERRAL.InternalPatientNumber = Patient.InternalPatientNumber

left join RTTPTEPIPATHWAY EPW
on	EPW.InternalPatientNumber = OPREFERRAL.InternalPatientNumber
and	EPW.EpisodeNumber = OPREFERRAL.EpisodeNumber

left join RTTPTPATHWAYCURRENT PW
on	PW.InternalPatientNumber = EPW.InternalPatientNumber
and	PW.PathwayNumber = EPW.PathwayNumber

LEFT JOIN OPDISCHARGE 
ON	OPREFERRAL.InternalPatientNumber = OPDISCHARGE.InternalPatientNumber
AND	OPREFERRAL.EpisodeNumber = OPDISCHARGE.EpisodeNumber

LEFT JOIN ALLOCATEDCONTRACT 
ON	ALLOCATEDCONTRACT.InternalPatientNumber = OPREFERRAL.InternalPatientNumber
AND	ALLOCATEDCONTRACT.EpisodeNumber = OPREFERRAL.EpisodeNumber
AND	ALLOCATEDCONTRACT.CmReverseDtTime = 999999999999

WHERE
	OPREFERRAL.OpRegDtimeInt between ' + @from + ' and ' + @to + '
'')
'

if @debug = 1 
begin
	print @sql1
	print @sql2
	print @sql3
end
else
begin
	exec (@sql1 + @sql2 + @sql3)

	select
		@RowsInserted = @RowsInserted + @@ROWCOUNT

	SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

	SELECT @Stats = 
		'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
		'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

	EXEC WriteAuditLogEvent 'ExtractInquireRF', @Stats, @StartTime

end
