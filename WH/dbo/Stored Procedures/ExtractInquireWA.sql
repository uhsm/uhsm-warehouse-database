﻿CREATE procedure [dbo].[ExtractInquireWA]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @sql1 varchar(8000)
declare @sql2 varchar(8000)
declare @sql3 varchar(8000)
declare @from varchar(8)
declare @to varchar(8)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112)

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112)

select
	@sql1 = '
insert into TImportWAEncounter
(
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AppointmentDate
	,AppointmentTime
	,ClinicCode
	,AdminCategoryCode
	,SourceOfReferralCode
	,ReasonForReferralCode
	,PriorityCode
	,FirstAttendanceFlag
	,AppointmentStatusCode
	,CancelledByCode
	,TransportRequiredFlag
	,AppointmentTypeCode
	,DisposalCode
	,ConsultantCode
	,SpecialtyCode
	,ReferringConsultantCode
	,ReferringSpecialtyCode
	,BookingTypeCode
	,CasenoteNo
	,AppointmentCreateDate
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,DoctorCode
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,ReferralDate
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,AppointmentCategoryCode
	,AppointmentCreatedBy
	,AppointmentCancelDate
	,LastRevisedDate
	,LastRevisedBy
	,OverseasStatusFlag
	,PatientChoiceCode
	,ScheduledCancelReasonCode
	,PatientCancelReason
	,DischargeDate
	,QM08StartWaitDate
	,QM08EndWaitDate
	,DestinationSiteCode
	,EBookingReferenceNo
	,SeenByCode
	,InterfaceCode
)
'


	,@sql2 = '
select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AppointmentDate
	,AppointmentTime
	,ClinicCode
	,AdminCategoryCode
	,SourceOfReferralCode
	,ReasonForReferralCode
	,PriorityCode
	,FirstAttendanceFlag
	,AppointmentStatusCode
	,CancelledByCode
	,TransportRequiredFlag
	,AppointmentTypeCode
	,DisposalCode
	,ConsultantCode
	,SpecialtyCode
	,ReferringConsultantCode
	,ReferringSpecialtyCode
	,BookingTypeCode
	,CasenoteNo
	,AppointmentCreateDate
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,DoctorCode
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,ReferralDate
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,AppointmentCategoryCode
	,AppointmentCreatedBy
	,AppointmentCancelDate
	,LastRevisedDate
	,LastRevisedBy
	,OverseasStatusFlag
	,PatientChoiceCode
	,ScheduledCancelReasonCode
	,PatientCancelReason
	,DischargeDate
	,QM08StartWaitDate
	,QM08EndWaitDate
	,DestinationSiteCode
	,EBookingReferenceNo
	,SeenByCode
	,InterfaceCode = ''INQ''
from
	openquery(INQUIRE, '''

	,@sql3 = '

SELECT
	 WARDATTENDER.WARDATTENDERID SourceUniqueID
	,WARDATTENDER.InternalPatientNumber SourcePatientNo
	,WARDATTENDER.EpisodeNumber SourceEncounterNo
	,Patient.Title PatientTitle
	,Patient.Forenames PatientForename
	,Patient.Surname PatientSurname
	,Patient.PtDoB DateOfBirth
	,Patient.PtDateOfDeath DateOfDeath
	,Patient.Sex SexCode
	,Patient.NHSNumber NHSNumber
	,WARDATTENDER.DistrictNumber DistrictNo
	,Patient.PtAddrPostCode Postcode
	,Patient.PtAddrLine1 PatientAddress1
	,Patient.PtAddrLine2 PatientAddress2
	,Patient.PtAddrLine3 PatientAddress3
	,Patient.PtAddrLine4 PatientAddress4
	,Patient.DistrictOfResidenceCode DHACode
	,Patient.EthnicType EthnicOriginCode
	,Patient.MaritalStatus MaritalStatusCode
	,Patient.Religion ReligionCode
	,Patient.GpCode RegisteredGpCode
	,null RegisteredGpPracticeCode
	,WARD.HospitalCode SiteCode
	,WARDATTENDER.AttendanceDate AppointmentDate
	,WARDATTENDER.AttendanceDate AppointmentTime
	,WARDATTENDER.Ward ClinicCode
	,WARDATTENDER.Category AdminCategoryCode
	,WARDATTENDER.RefBy SourceOfReferralCode
	,WARDATTENDER.ReasonForReferralCode ReasonForReferralCode
	,WARDATTENDER.PriorityType PriorityCode
	,WARDATTENDER.FirstAttendance FirstAttendanceFlag
	,WARDATTENDER.WaWardAttendersAttendanceStatusInternal AppointmentStatusCode
	,null CancelledByCode
	,null TransportRequiredFlag
	,"WA" AppointmentTypeCode
	,WARDATTENDER.Disposal DisposalCode
	,WARDATTENDER.Consultant ConsultantCode
	,WARDATTENDER.Specialty SpecialtyCode
	,null ReferringConsultantCode
	,null ReferringSpecialtyCode
	,null BookingTypeCode
	,WARDATTENDER.CaseNoteNumber CasenoteNo
	,null AppointmentCreateDate
	,Patient.GpCode EpisodicGpCode
	,null EpisodicGpPracticeCode
	,WARDATTENDER.WaWardAttendersDoctorCodePhysical DoctorCode
	,null PrimaryDiagnosisCode
	,WARDATTENDER.PrimaryProcedureCode PrimaryOperationCode
	,WARDATTENDER.PurchaserCode PurchaserCode
	,null ProviderCode
	,WARDATTENDER.ContractId ContractSerialNo
	,WARDATTENDER.ReferralDate ReferralDate
	,PW.PathwayNumber RTTPathwayID
	,PW.PathwayCondition RTTPathwayCondition
	,PW.RTTStartDate
	,PW.RTTEndDate
	,PW.RTTSpeciality RTTSpecialtyCode
	,PW.RTTCurProv RTTCurrentProviderCode
	,PW.RTTCurrentStatus RTTCurrentStatusCode
	,PW.RTTCurrentStatusDate
	,PW.RTTPrivatePat RTTCurrentPrivatePatientFlag
	,PW.RTTOSVStatus RTTOverseasStatusFlag
	,null RTTPeriodStatusCode
	,WARDATTENDER.Category AppointmentCategoryCode
	,null AppointmentCreatedBy
	,null AppointmentCancelDate
	,null LastRevisedDate
	,null LastRevisedBy
	,null OverseasStatusFlag
	,null PatientChoiceCode
	,null ScheduledCancelReasonCode
	,null PatientCancelReason
	,null DischargeDate
	,null QM08StartWaitDate
	,null QM08EndWaitDate
	,null DestinationSiteCode
	,null EBookingReferenceNo
	,WARDATTENDER.SeenBy SeenByCode
FROM
	WARDATTENDER

INNER JOIN PATDATA Patient
ON	WARDATTENDER.InternalPatientNumber = Patient.InternalPatientNumber

INNER JOIN WARD
ON	WARDATTENDER.Ward = WARD.WARDID

left join RTTPTEPIPATHWAY EPW
on	EPW.InternalPatientNumber = WARDATTENDER.InternalPatientNumber
and	EPW.EpisodeNumber = WARDATTENDER.EpisodeNumber

left join RTTPTPATHWAYCURRENT PW
on	PW.InternalPatientNumber = EPW.InternalPatientNumber
and	PW.PathwayNumber = EPW.PathwayNumber
WHERE
	WARDATTENDER.AttendanceDateInternal between ' + @from + ' and ' + @to + '
'')
'

if @debug = 1 
begin
	print @sql1
	print @sql2
	print @sql3
end
else
begin
	exec (@sql1 + @sql2 + @sql3)

	select
		@RowsInserted = @RowsInserted + @@ROWCOUNT

	SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

	SELECT @Stats = 
		'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
		'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

	EXEC WriteAuditLogEvent 'ExtractInquireWA', @Stats, @StartTime

end
