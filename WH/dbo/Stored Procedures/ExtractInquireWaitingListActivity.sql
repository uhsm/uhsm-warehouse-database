﻿CREATE procedure [dbo].[ExtractInquireWaitingListActivity]
	 @from smalldatetime
	,@to smalldatetime
	,@debug bit = 0
as

set dateformat dmy
declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @sqlInsert varchar(8000)
declare @sqlSelect varchar(8000)
declare @sqlOpenQuery varchar(8000)
declare @sql varchar(8000)
declare @fromString varchar(12)
declare @toString varchar(12)
declare @census varchar(8)

select @StartTime = getdate()
select @RowsInserted = 0

select
	 @fromString = convert(varchar, @from, 112) + '0000'
	,@toString = convert(varchar, @to, 112) + '2400'
	,@sqlInsert = '
insert into TImportWaitingListActivity
	(
	 SourceUniqueID
	,ActivityTypeCode
	,ActivityTime
	,AdminCategoryCode
	,BookingTypeCode
	,CancelledBy
	,ConsultantCode
	,DeferralEndDate
	,DeferralRevisedEndDate
	,DiagnosticGroupCode
	,SourceEntityRecno
	,SiteCode
	,SourcePatientNo
	,LastRevisionTime
	,LastRevisionUser
	,PreviousActivityId
	,Reason
	,Remark
	,RemovalComment
	,RemovalReasonCode
	,SpecialtyCode
	,SuspensionReasonCode
	,TCIAcceptDate
	,TCITime
	,TCIOfferDate
	,PriorityCode
	,WaitingListCode
	,WardCode
	,CharterCancelCode
	,CharterCancelDeferFlag
	,CharterCancel
	,OpCancelledFlag
	,OpCancelled
	,PatientChoiceFlag
	,PatientChoice
	,WLSuspensionInitiatorCode
	,WLSuspensionInitiator

	)
'
	,@sqlSelect = '
select
	 SourceUniqueID =
		WLACTIVITYID + ''||'' + EpsActvTypeInt

	,ActivityTypeCode = EpsActvTypeInt
	,ActivityTime = EpsActvDtimeInt
	,AdminCategoryCode = Category
	,BookingTypeCode = BookingType
	,CancelledBy
	,ConsultantCode = Consultant
	,DeferralEndDate = DefEndDate
	,DeferralRevisedEndDate = DefRevisedEndDate
	,DiagnosticGroupCode = DiagGroup
	,SourceEntityRecno = EpisodeNumber
	,SiteCode = Hospital
	,SourcePatientNo = InternalPatientNumber
	,LastRevisionTime = LastRevisionDtime
	,LastRevisionUser = LastRevisionUID

	,PreviousActivityId = 
		convert(varchar, InternalPatientNumber) + ''||'' + 
		convert(varchar, EpisodeNumber) + ''||'' + 
		convert(varchar, PrevActivityDTimeInt) + ''||'' + 
		convert(varchar, PrevActivityInt)

	,Reason
	,Remark
	,RemovalComment
	,RemovalReasonCode = RemovalReason
	,SpecialtyCode = Specialty
	,SuspensionReasonCode = SuspReason
	,TCIAcceptDate = TCIAcceptDateInt
	,TCITime = TCIDTimeInt
	,TCIOfferDate = TCIOfferDateInt
	,PriorityCode = Urgency
	,WaitingListCode = WaitingList
	,WardCode = Ward
	,CharterCancelCode
	,CharterCancelDeferFlag = CharterCancelDeferInd
	,CharterCancel = CharterCancelDescription
	,OpCancelledFlag = OpCancelled
	,OpCancelled = OpCancelledDesc
	,PatientChoiceFlag = PatientChoice
	,PatientChoice = PatientChoiceDesc
	,WLSuspensionInitiatorCode = WlSuspensionInitiator
	,WLSuspensionInitiator = WLSuspensionInitiatorDesc

from
'
	,@sqlOpenQuery = '
	openquery(inquire, ''

select
	 Activity.WLACTIVITYID
	,Activity.Activity
	,Activity.ActivityDate
	,Activity.ActivityTime
	,Activity.AdmEROD
	,Activity.AdmERODInt
	,Activity.BookingType
	,Activity.CancelledBy
	,Activity.Category
	,Activity.CharterCancelCode
	,Activity.CharterCancelDeferInd
	,Activity.CharterCancelDescription
	,Activity.Consultant
	,Activity.DefEndDate
	,Activity.DefEndDateInt
	,Activity.DefRevisedEndDate
	,Activity.DefRevisedEndDateInt
	,Activity.DiagGroup
	,Activity.EfgCode
	,Activity.EpisodeNumber
	,Activity.EpsActvDtimeInt
	,Activity.EpsActvTypeInt
	,Activity.FirstPbLetterSent
	,Activity.FirstPbReminderSent
	,Activity.Hospital
	,Activity.HrgRankedProcedureCode
	,Activity.HrgResourceGroup
	,Activity.InternalPatientNumber
	,Activity.LastRevisionDtime
	,Activity.LastRevisionUID
	,Activity.OpCancelled
	,Activity.OpCancelledDesc
	,Activity.PatSelfDeferral
	,Activity.PatSelfDeferralInt
	,Activity.PatientChoice
	,Activity.PatientChoiceDesc
	,Activity.PbResponseDate
	,Activity.PrevActivity
	,Activity.PrevActivityDTimeInt
	,Activity.PrevActivityDate
	,Activity.PrevActivityInt
	,Activity.PrevActivityTime
	,Activity.PrevConsultant
	,Activity.PrevDiagGroup
	,Activity.PrevHospital
	,Activity.PrevSpecialty
	,Activity.PrevWaitingList
	,Activity.Reason
	,Activity.Remark
	,left(Activity.RemovalComment, 25) RemovalComment
	,Activity.RemovalReason
	,Activity.SecondPbReminderSent
	,Activity.Specialty
	,Activity.SuspReason
	,Activity.TCIAcceptDate
	,Activity.TCIAcceptDateInt
	,Activity.TCIDTimeInt
	,Activity.TCIDate
	,Activity.TCILetterComment
	,Activity.TCILetterDate
	,Activity.TCIOfferDateInt
	,Activity.TCITime
	,Activity.Urgency
	,Activity.WLSuspensionInitiatorDesc
	,Activity.WaitingList
	,Activity.Ward
	,Activity.WlSuspensionInitiator
from
	WLACTIVITY Activity
where
	EpsActvDtimeInt between ' + @fromString + ' and ' + @toString + '
and	Activity.EpsActvTypeInt in (1, 2, 3, 4, 5, 6, 19)
and	Activity.Hospital in
	(
	select
		Hospital.HOSPDETID
	from
		HOSPDET Hospital
	)
''
)
'

--in the above where clause we need to use the EpsActvTypeInt and Hospital clauses to force the index to be used!

if @debug = 1
	print @sqlInsert + @sqlSelect + @sqlOpenQuery
else
begin
	exec (@sqlInsert + @sqlSelect + @sqlOpenQuery)

	SELECT @RowsInserted = @RowsInserted + @@ROWCOUNT


	SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

	SELECT @Stats = 
		'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
		'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

	EXEC WriteAuditLogEvent 'ExtractInquireWaitingListActivity', @Stats, @StartTime
end
