﻿

/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		ExtractLorenzoAPC - creates TImportAPCEncounter

Notes:			Stored in 
	

Versions:

				1.0.0.1 - 13/04/2016 - CM
					Amended so that Diagnosis and Procedure Codes are updated using the new table  APC.DiagnosisProcedureRefnos rather than previous method( commented out below) as some episodes have more than one code in the same position
					
				1.0.0.0
					Original store proc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/




CREATE procedure [dbo].[ExtractLorenzoAPC]
	 @fromDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0


select
	@from = 
		coalesce(
			@fromDate
			,(
			select
				DateValue
			from
				dbo.Parameter
			where
				Parameter = 'EXTRACTAPCDATE'
			)
		)


exec Lorenzo.dbo.BuildExtractDatasetEncounterAPC @from

insert into dbo.TImportAPCEncounter
(
	 SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,ReferralSourceUniqueID
	,WaitingListSourceUniqueID
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,DischargeDate
	,EpisodeStartDate
	,EpisodeEndDate
	,StartSiteCode
	,StartWardTypeCode
	,EndSiteCode
	,EndWardTypeCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,AdminCategoryCode
	,ConsultantCode
	,SpecialtyCode
	,LastEpisodeInSpellIndicator
	,FirstRegDayOrNightAdmit
	,NeonatalLevelOfCare
	,PASHRGCode
	,PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,PrimaryOperationCode
	,PrimaryOperationDate
	,SecondaryOperationCode1
	,SecondaryOperationDate1
	,SecondaryOperationCode2
	,SecondaryOperationDate2
	,SecondaryOperationCode3
	,SecondaryOperationDate3
	,SecondaryOperationCode4
	,SecondaryOperationDate4
	,SecondaryOperationCode5
	,SecondaryOperationDate5
	,SecondaryOperationCode6
	,SecondaryOperationDate6
	,SecondaryOperationCode7
	,SecondaryOperationDate7
	,SecondaryOperationCode8
	,SecondaryOperationDate8
	,SecondaryOperationCode9
	,SecondaryOperationDate9
	,SecondaryOperationCode10
	,SecondaryOperationDate10
	,SecondaryOperationCode11
	,SecondaryOperationDate11
	,OperationStatusCode
	,ContractSerialNo
	,CodingCompleteFlag
	,PurchaserCode
	,ProviderCode
	,EpisodeStartTime
	,EpisodeEndTime
	,RegisteredGdpCode
	,EpisodicGpCode
	,InterfaceCode
	,CasenoteNumber
	,NHSNumberStatusCode
	,AdmissionTime
	,DischargeTime
	,TransferFrom
	,DistrictNo
	,SourceUniqueID
	,ExpectedLOS
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,OverseasStatusFlag
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,ArchiveFlag
	,EpisodeOutcomeCode
	,LegalStatusClassificationOnAdmissionCode
	,LegalStatusClassificationOnDischargeCode
	,DecisionToAdmitDate
	,OriginalDecisionToAdmitDate
	,DurationOfElectiveWait
)
select
	 APC.SourcePatientNo
	,APC.SourceSpellNo
	,APC.SourceEncounterNo
	,APC.ProviderSpellNo
	,APC.ReferralSourceUniqueID
	,APC.WaitingListSourceUniqueID
	,APC.PatientTitle
	,APC.PatientForename
	,APC.PatientSurname
	,APC.DateOfBirth
	,APC.DateOfDeath
	,APC.SexCode
	,APC.NHSNumber
	,APC.Postcode
	,APC.PatientAddress1
	,APC.PatientAddress2
	,APC.PatientAddress3
	,APC.PatientAddress4
	,APC.DHACode
	,APC.EthnicOriginCode
	,APC.MaritalStatusCode
	,APC.ReligionCode
	,APC.DateOnWaitingList
	,APC.AdmissionDate
	,APC.DischargeDate
	,APC.EpisodeStartDate
	,APC.EpisodeEndDate
	,APC.StartSiteCode
	,APC.StartWardTypeCode
	,APC.EndSiteCode
	,APC.EndWardTypeCode
	,APC.RegisteredGpCode
	,APC.RegisteredGpPracticeCode
	,APC.SiteCode
	,APC.AdmissionMethodCode
	,APC.AdmissionSourceCode
	,APC.PatientClassificationCode
	,APC.ManagementIntentionCode
	,APC.DischargeMethodCode
	,APC.DischargeDestinationCode
	,APC.AdminCategoryCode
	,APC.ConsultantCode
	,APC.SpecialtyCode
	,APC.LastEpisodeInSpellIndicator
	,APC.FirstRegDayOrNightAdmit
	,APC.NeonatalLevelOfCare
	,APC.PASHRGCode
	,APC.PrimaryDiagnosisCode
	,APC.SubsidiaryDiagnosisCode
	,APC.SecondaryDiagnosisCode1
	,APC.SecondaryDiagnosisCode2
	,APC.SecondaryDiagnosisCode3
	,APC.SecondaryDiagnosisCode4
	,APC.SecondaryDiagnosisCode5
	,APC.SecondaryDiagnosisCode6
	,APC.SecondaryDiagnosisCode7
	,APC.SecondaryDiagnosisCode8
	,APC.SecondaryDiagnosisCode9
	,APC.SecondaryDiagnosisCode10
	,APC.SecondaryDiagnosisCode11
	,APC.SecondaryDiagnosisCode12
	,APC.PrimaryOperationCode
	,APC.PrimaryOperationDate
	,APC.SecondaryOperationCode1
	,APC.SecondaryOperationDate1
	,APC.SecondaryOperationCode2
	,APC.SecondaryOperationDate2
	,APC.SecondaryOperationCode3
	,APC.SecondaryOperationDate3
	,APC.SecondaryOperationCode4
	,APC.SecondaryOperationDate4
	,APC.SecondaryOperationCode5
	,APC.SecondaryOperationDate5
	,APC.SecondaryOperationCode6
	,APC.SecondaryOperationDate6
	,APC.SecondaryOperationCode7
	,APC.SecondaryOperationDate7
	,APC.SecondaryOperationCode8
	,APC.SecondaryOperationDate8
	,APC.SecondaryOperationCode9
	,APC.SecondaryOperationDate9
	,APC.SecondaryOperationCode10
	,APC.SecondaryOperationDate10
	,APC.SecondaryOperationCode11
	,APC.SecondaryOperationDate11
	,APC.OperationStatusCode
	,APC.ContractSerialNo
	,APC.CodingCompleteFlag
	,APC.PurchaserCode
	,APC.ProviderCode
	,APC.EpisodeStartTime
	,APC.EpisodeEndTime
	,APC.RegisteredGdpCode
	,APC.EpisodicGpCode
	,APC.InterfaceCode
	,APC.CasenoteNumber
	,APC.NHSNumberStatusCode
	,APC.AdmissionTime
	,APC.DischargeTime
	,APC.TransferFrom
	,APC.DistrictNo
	,APC.SourceUniqueID
	,APC.ExpectedLOS
	,APC.MRSAFlag
	,APC.RTTPathwayID
	,APC.RTTPathwayCondition
	,APC.RTTStartDate
	,APC.RTTEndDate
	,APC.RTTSpecialtyCode
	,APC.RTTCurrentProviderCode
	,APC.RTTCurrentStatusCode
	,APC.RTTCurrentStatusDate
	,APC.RTTCurrentPrivatePatientFlag
	,APC.RTTOverseasStatusFlag
	,APC.RTTPeriodStatusCode
	,APC.OverseasStatusFlag
	,APC.PASCreated
	,APC.PASUpdated
	,APC.PASCreatedByWhom
	,APC.PASUpdatedByWhom
	,APC.ArchiveFlag
	,APC.EpisodeOutcomeCode
	,APC.LegalStatusClassificationOnAdmissionCode
	,APC.LegalStatusClassificationOnDischargeCode
	,APC.DecisionToAdmitDate
	,APC.OriginalDecisionToAdmitDate
	,APC.DurationOfElectiveWait
from
	Lorenzo.dbo.ExtractAPC APC

inner join Lorenzo.dbo.ExtractDatasetEncounter
on	ExtractDatasetEncounter.SourceUniqueID = APC.SourceUniqueID
and	ExtractDatasetEncounter.DatasetCode = 'APC'


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT




update dbo.TImportAPCEncounter
set
	PrimaryDiagnosisCode = ClinicalCoding.CODE
	
	
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode = 'DIAGN'
AND SortOrder = 1


update dbo.TImportAPCEncounter
set
	SubsidiaryDiagnosisCode = ClinicalCoding.CODE
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode = 'DIAGN'
AND SortOrder = 2


update dbo.TImportAPCEncounter
set
	SecondaryDiagnosisCode1 = ClinicalCoding.CODE
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode = 'DIAGN'
AND SortOrder = 3



update dbo.TImportAPCEncounter
set
	SecondaryDiagnosisCode2 = ClinicalCoding.CODE
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode = 'DIAGN'
AND SortOrder = 4


update dbo.TImportAPCEncounter
set
	SecondaryDiagnosisCode3 = ClinicalCoding.CODE
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode = 'DIAGN'
AND SortOrder = 5





update dbo.TImportAPCEncounter
set
	SecondaryDiagnosisCode4 = ClinicalCoding.CODE

From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode = 'DIAGN'
AND SortOrder = 6


update dbo.TImportAPCEncounter
set
	SecondaryDiagnosisCode5 = ClinicalCoding.CODE
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode = 'DIAGN'
AND SortOrder = 7


update dbo.TImportAPCEncounter
set
	SecondaryDiagnosisCode6 = ClinicalCoding.CODE
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode = 'DIAGN'
AND SortOrder = 8


update dbo.TImportAPCEncounter
set
	SecondaryDiagnosisCode7 = ClinicalCoding.CODE
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode = 'DIAGN'
AND SortOrder = 9



update dbo.TImportAPCEncounter
set
	SecondaryDiagnosisCode8 = ClinicalCoding.CODE
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode = 'DIAGN'
AND SortOrder = 10



update dbo.TImportAPCEncounter
set
	SecondaryDiagnosisCode9 = ClinicalCoding.CODE
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode = 'DIAGN'
AND SortOrder = 11



update dbo.TImportAPCEncounter
set
	SecondaryDiagnosisCode10 = ClinicalCoding.CODE
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode = 'DIAGN'
AND SortOrder = 12



update dbo.TImportAPCEncounter
set
	SecondaryDiagnosisCode11 = ClinicalCoding.CODE
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode = 'DIAGN'
AND SortOrder = 13



update dbo.TImportAPCEncounter
set
	SecondaryDiagnosisCode12 = ClinicalCoding.CODE
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode = 'DIAGN'
AND SortOrder = 14




update dbo.TImportAPCEncounter
set
	PrimaryOperationCode = ClinicalCoding.CODE
	,PrimaryOperationDate = ClinicalCoding.DGPRO_DTTM
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode ='PROCE'
AND SortOrder = 1



update dbo.TImportAPCEncounter
set
	SecondaryOperationCode1 = ClinicalCoding.CODE
	,SecondaryOperationDate1 = ClinicalCoding.DGPRO_DTTM
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode ='PROCE'
AND SortOrder = 2


update dbo.TImportAPCEncounter
set
	SecondaryOperationCode2 = ClinicalCoding.CODE
	,SecondaryOperationDate2 = ClinicalCoding.DGPRO_DTTM
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode ='PROCE'
AND SortOrder = 3


update dbo.TImportAPCEncounter
set
	SecondaryOperationCode3 = ClinicalCoding.CODE
	,SecondaryOperationDate3 = ClinicalCoding.DGPRO_DTTM
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode ='PROCE'
AND SortOrder = 4



update dbo.TImportAPCEncounter
set
	SecondaryOperationCode4 = ClinicalCoding.CODE
	,SecondaryOperationDate4 = ClinicalCoding.DGPRO_DTTM
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode ='PROCE'
AND SortOrder = 5



update dbo.TImportAPCEncounter
set
	SecondaryOperationCode5 = ClinicalCoding.CODE
	,SecondaryOperationDate5 = ClinicalCoding.DGPRO_DTTM
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode ='PROCE'
AND SortOrder = 6



update dbo.TImportAPCEncounter
set
	SecondaryOperationCode6 = ClinicalCoding.CODE
	,SecondaryOperationDate6 = ClinicalCoding.DGPRO_DTTM
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode ='PROCE'
AND SortOrder = 7



update dbo.TImportAPCEncounter
set
	SecondaryOperationCode7 = ClinicalCoding.CODE
	,SecondaryOperationDate7 = ClinicalCoding.DGPRO_DTTM
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode ='PROCE'
AND SortOrder = 8



update dbo.TImportAPCEncounter
set
	SecondaryOperationCode8 = ClinicalCoding.CODE
	,SecondaryOperationDate8 = ClinicalCoding.DGPRO_DTTM
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode ='PROCE'
AND SortOrder = 9



update dbo.TImportAPCEncounter
set
	SecondaryOperationCode9 = ClinicalCoding.CODE
	,SecondaryOperationDate9 = ClinicalCoding.DGPRO_DTTM
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode ='PROCE'
AND SortOrder = 10



update dbo.TImportAPCEncounter
set
	SecondaryOperationCode10 = ClinicalCoding.CODE
	,SecondaryOperationDate10 = ClinicalCoding.DGPRO_DTTM
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode ='PROCE'
AND SortOrder = 11




update dbo.TImportAPCEncounter
set
	SecondaryOperationCode11 = ClinicalCoding.CODE
	,SecondaryOperationDate11 = ClinicalCoding.DGPRO_DTTM
From  TImportAPCEncounter 
Left outer join APC.DiagnosisProcedureRefnos on TImportAPCEncounter.SourceUniqueID = DiagnosisProcedureRefnos.EpisodeUniqueID
Left outer join Lorenzo.dbo.ClinicalCoding  on DiagnosisProcedureRefnos.DGProRefo = ClinicalCoding.DGPRO_REFNO
where DPTypCode ='PROCE'
AND SortOrder = 12



/*******CM 13/04/2016 - replaced the below method for updating diagnosis and procedure codes with the one above toremove the multiple codes in the same position issue********/
----there are sometimes duplicate entries for the same SORT_ORDER so rather than having complex (slow) joins to fetch distinct
----records we use lots of updates

--update dbo.TImportAPCEncounter
--set
--	PrimaryDiagnosisCode = PrimaryDiagnosis.CODE

--from
--	Lorenzo.dbo.ClinicalCoding PrimaryDiagnosis
--where
--	PrimaryDiagnosis.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	PrimaryDiagnosis.SORCE_CODE = 'PRCAE'
--and	PrimaryDiagnosis.SORT_ORDER = 1
--and	PrimaryDiagnosis.DPTYP_CODE = 'DIAGN'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SubsidiaryDiagnosisCode = SubsidiaryDiagnosis.CODE
--from
--	Lorenzo.dbo.ClinicalCoding SubsidiaryDiagnosis
--where
--	SubsidiaryDiagnosis.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SubsidiaryDiagnosis.SORCE_CODE = 'PRCAE'
--and	SubsidiaryDiagnosis.SORT_ORDER = 2
--and	SubsidiaryDiagnosis.DPTYP_CODE = 'DIAGN'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryDiagnosisCode1 = SecondaryDiagnosis1.CODE
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryDiagnosis1
--where
--	SecondaryDiagnosis1.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryDiagnosis1.SORCE_CODE = 'PRCAE'
--and	SecondaryDiagnosis1.SORT_ORDER = 3
--and	SecondaryDiagnosis1.DPTYP_CODE = 'DIAGN'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryDiagnosisCode2 = SecondaryDiagnosis2.CODE
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryDiagnosis2
--where
--	SecondaryDiagnosis2.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryDiagnosis2.SORCE_CODE = 'PRCAE'
--and	SecondaryDiagnosis2.SORT_ORDER = 4
--and	SecondaryDiagnosis2.DPTYP_CODE = 'DIAGN'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryDiagnosisCode3 = SecondaryDiagnosis3.CODE
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryDiagnosis3
--where
--	SecondaryDiagnosis3.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryDiagnosis3.SORCE_CODE = 'PRCAE'
--and	SecondaryDiagnosis3.SORT_ORDER = 5
--and	SecondaryDiagnosis3.DPTYP_CODE = 'DIAGN'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryDiagnosisCode4 = SecondaryDiagnosis4.CODE
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryDiagnosis4
--where
--	SecondaryDiagnosis4.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryDiagnosis4.SORCE_CODE = 'PRCAE'
--and	SecondaryDiagnosis4.SORT_ORDER = 6
--and	SecondaryDiagnosis4.DPTYP_CODE = 'DIAGN'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryDiagnosisCode5 = SecondaryDiagnosis5.CODE
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryDiagnosis5
--where
--	SecondaryDiagnosis5.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryDiagnosis5.SORCE_CODE = 'PRCAE'
--and	SecondaryDiagnosis5.SORT_ORDER = 7
--and	SecondaryDiagnosis5.DPTYP_CODE = 'DIAGN'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryDiagnosisCode6 = SecondaryDiagnosis6.CODE
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryDiagnosis6
--where
--	SecondaryDiagnosis6.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryDiagnosis6.SORCE_CODE = 'PRCAE'
--and	SecondaryDiagnosis6.SORT_ORDER = 8
--and	SecondaryDiagnosis6.DPTYP_CODE = 'DIAGN'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryDiagnosisCode7 = SecondaryDiagnosis7.CODE
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryDiagnosis7
--where
--	SecondaryDiagnosis7.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryDiagnosis7.SORCE_CODE = 'PRCAE'
--and	SecondaryDiagnosis7.SORT_ORDER = 9
--and	SecondaryDiagnosis7.DPTYP_CODE = 'DIAGN'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryDiagnosisCode8 = SecondaryDiagnosis8.CODE
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryDiagnosis8
--where
--	SecondaryDiagnosis8.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryDiagnosis8.SORCE_CODE = 'PRCAE'
--and	SecondaryDiagnosis8.SORT_ORDER = 10
--and	SecondaryDiagnosis8.DPTYP_CODE = 'DIAGN'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryDiagnosisCode9 = SecondaryDiagnosis9.CODE
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryDiagnosis9
--where
--	SecondaryDiagnosis9.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryDiagnosis9.SORCE_CODE = 'PRCAE'
--and	SecondaryDiagnosis9.SORT_ORDER = 11
--and	SecondaryDiagnosis9.DPTYP_CODE = 'DIAGN'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryDiagnosisCode10 = SecondaryDiagnosis10.CODE
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryDiagnosis10
--where
--	SecondaryDiagnosis10.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryDiagnosis10.SORCE_CODE = 'PRCAE'
--and	SecondaryDiagnosis10.SORT_ORDER = 12
--and	SecondaryDiagnosis10.DPTYP_CODE = 'DIAGN'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryDiagnosisCode11 = SecondaryDiagnosis11.CODE
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryDiagnosis11
--where
--	SecondaryDiagnosis11.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryDiagnosis11.SORCE_CODE = 'PRCAE'
--and	SecondaryDiagnosis11.SORT_ORDER = 13
--and	SecondaryDiagnosis11.DPTYP_CODE = 'DIAGN'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryDiagnosisCode12 = SecondaryDiagnosis12.CODE
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryDiagnosis12
--where
--	SecondaryDiagnosis12.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryDiagnosis12.SORCE_CODE = 'PRCAE'
--and	SecondaryDiagnosis12.SORT_ORDER = 14
--and	SecondaryDiagnosis12.DPTYP_CODE = 'DIAGN'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	PrimaryOperationCode = PrimaryOperation.CODE
--	,PrimaryOperationDate = PrimaryOperation.DGPRO_DTTM
--from
--	Lorenzo.dbo.ClinicalCoding PrimaryOperation
--where
--	PrimaryOperation.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	PrimaryOperation.SORCE_CODE = 'PRCAE'
--and	PrimaryOperation.SORT_ORDER = 1
--and	PrimaryOperation.DPTYP_CODE = 'PROCE'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryOperationCode1 = SecondaryOperation1.CODE
--	,SecondaryOperationDate1 = SecondaryOperation1.DGPRO_DTTM
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryOperation1
--where
--	SecondaryOperation1.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryOperation1.SORCE_CODE = 'PRCAE'
--and	SecondaryOperation1.SORT_ORDER = 2
--and	SecondaryOperation1.DPTYP_CODE = 'PROCE'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryOperationCode2 = SecondaryOperation2.CODE
--	,SecondaryOperationDate2 = SecondaryOperation2.DGPRO_DTTM
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryOperation2
--where
--	SecondaryOperation2.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryOperation2.SORCE_CODE = 'PRCAE'
--and	SecondaryOperation2.SORT_ORDER = 3
--and	SecondaryOperation2.DPTYP_CODE = 'PROCE'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryOperationCode3 = SecondaryOperation3.CODE
--	,SecondaryOperationDate3 = SecondaryOperation3.DGPRO_DTTM
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryOperation3
--where
--	SecondaryOperation3.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryOperation3.SORCE_CODE = 'PRCAE'
--and	SecondaryOperation3.SORT_ORDER = 4
--and	SecondaryOperation3.DPTYP_CODE = 'PROCE'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryOperationCode4 = SecondaryOperation4.CODE
--	,SecondaryOperationDate4 = SecondaryOperation4.DGPRO_DTTM
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryOperation4
--where
--	SecondaryOperation4.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryOperation4.SORCE_CODE = 'PRCAE'
--and	SecondaryOperation4.SORT_ORDER = 5
--and	SecondaryOperation4.DPTYP_CODE = 'PROCE'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryOperationCode5 = SecondaryOperation5.CODE
--	,SecondaryOperationDate5 = SecondaryOperation5.DGPRO_DTTM
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryOperation5
--where
--	SecondaryOperation5.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryOperation5.SORCE_CODE = 'PRCAE'
--and	SecondaryOperation5.SORT_ORDER = 6
--and	SecondaryOperation5.DPTYP_CODE = 'PROCE'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryOperationCode6 = SecondaryOperation6.CODE
--	,SecondaryOperationDate6 = SecondaryOperation6.DGPRO_DTTM
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryOperation6
--where
--	SecondaryOperation6.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryOperation6.SORCE_CODE = 'PRCAE'
--and	SecondaryOperation6.SORT_ORDER = 7
--and	SecondaryOperation6.DPTYP_CODE = 'PROCE'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryOperationCode7 = SecondaryOperation7.CODE
--	,SecondaryOperationDate7 = SecondaryOperation7.DGPRO_DTTM
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryOperation7
--where
--	SecondaryOperation7.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryOperation7.SORCE_CODE = 'PRCAE'
--and	SecondaryOperation7.SORT_ORDER = 8
--and	SecondaryOperation7.DPTYP_CODE = 'PROCE'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryOperationCode8 = SecondaryOperation8.CODE
--	,SecondaryOperationDate8 = SecondaryOperation8.DGPRO_DTTM
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryOperation8
--where
--	SecondaryOperation8.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryOperation8.SORCE_CODE = 'PRCAE'
--and	SecondaryOperation8.SORT_ORDER = 9
--and	SecondaryOperation8.DPTYP_CODE = 'PROCE'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryOperationCode9 = SecondaryOperation9.CODE
--	,SecondaryOperationDate9 = SecondaryOperation9.DGPRO_DTTM
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryOperation9
--where
--	SecondaryOperation9.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryOperation9.SORCE_CODE = 'PRCAE'
--and	SecondaryOperation9.SORT_ORDER = 10
--and	SecondaryOperation9.DPTYP_CODE = 'PROCE'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryOperationCode10 = SecondaryOperation10.CODE
--	,SecondaryOperationDate10 = SecondaryOperation10.DGPRO_DTTM
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryOperation10
--where
--	SecondaryOperation10.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryOperation10.SORCE_CODE = 'PRCAE'
--and	SecondaryOperation10.SORT_ORDER = 11
--and	SecondaryOperation10.DPTYP_CODE = 'PROCE'
--and	ARCHV_FLAG = 'N'

--update dbo.TImportAPCEncounter
--set
--	SecondaryOperationCode11 = SecondaryOperation11.CODE
--	,SecondaryOperationDate11 = SecondaryOperation11.DGPRO_DTTM
--from
--	Lorenzo.dbo.ClinicalCoding SecondaryOperation11
--where
--	SecondaryOperation11.SORCE_REFNO = TImportAPCEncounter.SourceUniqueID
--and	SecondaryOperation11.SORCE_CODE = 'PRCAE'
--and	SecondaryOperation11.SORT_ORDER = 12
--and	SecondaryOperation11.DPTYP_CODE = 'PROCE'
--and	ARCHV_FLAG = 'N'



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Extract from ' + CONVERT(varchar(20), @from, 130) + ', '  + 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH ExtractLorenzoAPC', @Stats, @StartTime

print @Stats



