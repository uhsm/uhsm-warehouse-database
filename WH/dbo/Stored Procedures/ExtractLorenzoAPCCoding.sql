﻿CREATE procedure [dbo].[ExtractLorenzoAPCCoding] as

/**
KO. 30/06/214. See issue IDDW0070 - missing asterisk codes

**/

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()
	
--Get base coding data without duplicates
--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[APC].[DiagnosisCoding]') AND type in (N'U'))
--DROP TABLE [APC].[DiagnosisCoding]
truncate table APC.DiagnosisCoding
insert into APC.DiagnosisCoding
select
	 SourceUniqueID = DiagCode.DGPRO_REFNO
	,SequenceNo = DiagCode.SORT_ORDER
	,DiagnosisCode = DiagCode.CODE
	,CodeType = cast(null as varchar)
	,DiagnosisDate = DiagCode.DGPRO_DTTM
	,APCSourceUniqueID = DiagCode.SORCE_REFNO
	,SupplementaryCode = DiagCode.SUPL_CODE
	,PASCreated = DiagCode.CREATE_DTTM
	,PASUpdated = DiagCode.MODIF_DTTM
--into APC.DiagnosisCoding
from PAS.ClinicalCodingBase DiagCode
left join APC.Encounter Encounter 
	on Encounter.SourceUniqueID = DiagCode.SORCE_REFNO
where
	DiagCode.SORCE_CODE = 'PRCAE'
and	DiagCode.DPTYP_CODE = 'DIAGN'
and	DiagCode.ARCHV_FLAG = 'N'
and Encounter.DischargeDate > '2011-03-31 00:00:00'
and not exists(
	select 1 from
		PAS.ClinicalCodingBase DuplicateDiagCode
	where DuplicateDiagCode.SORCE_CODE = 'PRCAE'
	and	DuplicateDiagCode.DPTYP_CODE = 'DIAGN'
	and	DuplicateDiagCode.ARCHV_FLAG = 'N'
	and DuplicateDiagCode.SORCE_REFNO = DiagCode.SORCE_REFNO
	and DuplicateDiagCode.SORT_ORDER = DiagCode.SORT_ORDER
	and DuplicateDiagCode.CODE = DiagCode.CODE
	and DuplicateDiagCode.DGPRO_REFNO > DiagCode.DGPRO_REFNO
)
	
--Identify 'Dagger' codes
update APC.DiagnosisCoding
set CodeType = 'D'
where SupplementaryCode is not null

--Add additional rows as 'Asterisk' Codes
insert into APC.DiagnosisCoding (
	SourceUniqueID
	,SequenceNo
	,DiagnosisCode
	,DiagnosisDate
	,APCSourceUniqueID
	,PASCreated
	,PASUpdated
	,CodeType)
select 
	SourceUniqueID
	,SequenceNo
	,SupplementaryCode --add as main code
	,DiagnosisDate
	,APCSourceUniqueID
	,PASCreated
	,dateadd(MILLISECOND, 2, PASUpdated)
	,'A'
from APC.DiagnosisCoding where SupplementaryCode is not null

--Update sequence number so asterisk code comes directly below dagger code
;with RowNumbers as  
(
select 
	NewSequenceNo = Row_Number() over(partition by APCSourceUniqueID order by SequenceNo, CodeType desc) 
	,APCSourceUniqueID, SequenceNo, CodeType
from APC.DiagnosisCoding
)
update cod  
set cod.SequenceNo = r.NewSequenceNo 
from APC.DiagnosisCoding cod 
inner join RowNumbers r 
	on cod.APCSourceUniqueID = r.APCSourceUniqueID
	and cod.SequenceNo = r.SequenceNo
	and isnull(cod.CodeType, 'x') = isnull(r.CodeType, 'x')
	
--Run another CTE to handle other duplicate sequence numbers that exist in the data
--? cause
if Object_id('tempdb..#CodingDupes', 'U') is not null
	drop table #CodingDupes
	
select distinct cod.*
into #CodingDupes
from (
	select APCSourceUniqueID, SequenceNo, COUNT(1) as cnt
	from APC.DiagnosisCoding
	group by APCSourceUniqueID, SequenceNo
	having COUNT(1) > 1
) dupes 
inner join APC.DiagnosisCoding cod
on cod.APCSourceUniqueID = dupes.APCSourceUniqueID
order by cod.APCSourceUniqueID, cod.SequenceNo

;with RowNumbers2 as  
(
select 
	NewSequenceNo = Row_Number() over(
		partition by APCSourceUniqueID order by SequenceNo, SourceUniqueID
	) 
	,SourceUniqueID, SequenceNo, CodeType
from #CodingDupes
)
update cod  
set cod.SequenceNo = r.NewSequenceNo 
from APC.DiagnosisCoding cod 
inner join RowNumbers2 r 
	on cod.SourceUniqueID = r.SourceUniqueID

	
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH ExtractLorenzoAPCCoding', @Stats, @StartTime

--print @Stats