﻿/*
--------------------------------------------------------------------------------------------------------------------------
Purpose:		Load APC.DiagnosisCoding TEST
			
Notes:			dbo

Versions:
				1.0.0.1 - 10/03/2016 - MT
					Add indexing.
					This table had no indexes whatsoever and was causing poor
					query performance.
					Further mods to improve the load time - pending ...

				1.0.0.0 - 30/06/2014 - KO
					See issue IDDW0070 - missing asterisk codes.
--------------------------------------------------------------------------------------------------------------------------
*/
Create Procedure dbo.ExtractLorenzoAPCCodingTEST
As
	
-- Drop PK and indexes before doing the load.
-- PK can not exist in the early stages of this load because there are duplicates.
-- Duplicates are eradicated further on in the load so the PK can be re-created at the end.
-- Dropping and re-creating other indexes for speed.
If Exists (Select * From sys.objects Where object_id = OBJECT_ID(N'PK_DiagnosisCodingTEST') And type in (N'U'))
Alter Table APC.DiagnosisCodingTEST Drop Constraint PK_DiagnosisCodingTEST

If Exists (Select * From sys.indexes Where object_id = object_id('APC.DiagnosisCodingTEST') And NAME = 'IX_DiagnosisCodingTEST_APCSourceUniqueID')
    Drop Index IX_DiagnosisCodingTEST_APCSourceUniqueID On APC.DiagnosisCodingTEST

If Exists (Select * From sys.indexes Where object_id = object_id('APC.DiagnosisCodingTEST') And NAME = 'IX_DiagnosisCodingTEST_DiagnosisCode')
    Drop Index IX_DiagnosisCodingTEST_DiagnosisCode On APC.DiagnosisCodingTEST

If Exists (Select * From sys.indexes Where object_id = object_id('APC.DiagnosisCodingTEST') And NAME = 'IX_DiagnosisCodingTEST_SequenceNo')
    Drop Index IX_DiagnosisCodingTEST_SequenceNo On APC.DiagnosisCodingTEST

-- Truncate existing
Truncate Table APC.DiagnosisCodingTEST

-- Load
;With LastDiagnosisCTE As (
	Select
		 SourceUniqueID = src.DGPRO_REFNO
		,SequenceNo = src.SORT_ORDER
		,DiagnosisCode = src.CODE
		,DiagnosisDate = src.DGPRO_DTTM
		,APCSourceUniqueID = src.SORCE_REFNO
		,SupplementaryCode = src.SUPL_CODE
		,PASCreated = src.CREATE_DTTM
		,PASUpdated = src.MODIF_DTTM
		,ROW_NUMBER() Over (Partition By src.SORCE_REFNO,src.SORT_ORDER,src.CODE Order By src.DGPRO_REFNO Desc) As RowNo
	From	PAS.ClinicalCodingBase src With (NoLock)

			-- Episode
			Left Join APC.Encounter ep  With (NoLock)
				On src.SORCE_REFNO = ep.SourceUniqueID
	Where
		src.SORCE_CODE = 'PRCAE'
		and	src.DPTYP_CODE = 'DIAGN'
		and	src.ARCHV_FLAG = 'N'
		and ep.DischargeDate > '2011-03-31'
	)

Insert Into APC.DiagnosisCodingTEST(
	 SourceUniqueID
	,SequenceNo
	,DiagnosisCode
	,DiagnosisDate
	,APCSourceUniqueID
	,SupplementaryCode
	,PASCreated
	,PASUpdated
	)
Select
	 SourceUniqueID
	,SequenceNo
	,DiagnosisCode
	,DiagnosisDate
	,APCSourceUniqueID
	,SupplementaryCode
	,PASCreated
	,PASUpdated

From	LastDiagnosisCTE src With (NoLock)

Where	src.RowNo = 1
	
-- Identify 'Dagger' codes
Update	src
Set		CodeType = 'D'
From	APC.DiagnosisCodingTEST src
Where	SupplementaryCode Is Not Null

-- Add additional rows as 'Asterisk' Codes
Insert Into APC.DiagnosisCodingTEST(
	SourceUniqueID
	,SequenceNo
	,DiagnosisCode
	,DiagnosisDate
	,APCSourceUniqueID
	,PASCreated
	,PASUpdated
	,CodeType
	)
Select 
	src.SourceUniqueID
	,src.SequenceNo
	,src.SupplementaryCode --add as main code
	,src.DiagnosisDate
	,src.APCSourceUniqueID
	,src.PASCreated
	,DATEADD(MILLISECOND, 2, src.PASUpdated)
	,'A'
From	APC.DiagnosisCodingTEST src With (NoLock)
Where	src.SupplementaryCode Is Not Null

-- Update sequence number so asterisk code comes directly below dagger code
;With RowNumbersCTE As (
	Select	NewSequenceNo = Row_Number() over(partition by APCSourceUniqueID order by SequenceNo, CodeType desc) 
			,APCSourceUniqueID
			,SequenceNo
			,CodeType
	From	APC.DiagnosisCodingTEST src With (NoLock)
	)
Update	src
Set		src.SequenceNo = r.NewSequenceNo
From	APC.DiagnosisCodingTEST src
		
		Inner Join RowNumbersCTE r With (NoLock)
			On src.APCSourceUniqueID = r.APCSourceUniqueID
			And src.SequenceNo = r.SequenceNo
			And Coalesce(src.CodeType,'x') = Coalesce(r.CodeType,'x')
	
--Run another CTE to handle other duplicate sequence numbers that exist in the data
--? cause
;With CodingDupes1CTE As (
	Select	APCSourceUniqueID,SequenceNo
	From	APC.DiagnosisCodingTEST src
	Group By APCSourceUniqueID,SequenceNo
	Having COUNT(1) > 1
	),
	CodingDupes2CTE As (
	Select	cod.APCSourceUniqueID,
			cod.SequenceNo,
			cod.SourceUniqueID,
			cod.CodeType
	From	CodingDupes1CTE src
			Inner Join APC.DiagnosisCodingTEST cod
				On src.APCSourceUniqueID = cod.APCSourceUniqueID
				And src.SequenceNo = cod.SequenceNo
	),
	RowNumbersCTE As (
	Select	NewSequenceNo = ROW_NUMBER() Over (Partition By src.APCSourceUniqueID Order By src.SequenceNo,src.SourceUniqueID)
			,src.SourceUniqueID
			,src.SequenceNo
			,src.CodeType
	From	CodingDupes2CTE src
	)

Update	src 
Set		SequenceNo = r.NewSequenceNo
From	APC.DiagnosisCodingTEST src

		Inner Join RowNumbersCTE r 
			On src.SourceUniqueID = r.SourceUniqueID

--Re-create PK and indexes
If Not Exists (Select * From sys.objects Where object_id = OBJECT_ID(N'PK_DiagnosisCodingTEST') And type In (N'U'))
	Alter Table APC.DiagnosisCodingTEST Add Constraint PK_DiagnosisCodingTEST Primary Key (SourceUniqueID Asc,SequenceNo Asc)

If Not Exists (Select * From sys.indexes Where object_id = object_id('APC.DiagnosisCodingTEST') And NAME = 'IX_DiagnosisCodingTEST_APCSourceUniqueID')
	Create Index IX_DiagnosisCodingTEST_APCSourceUniqueID On APC.DiagnosisCodingTEST(APCSourceUniqueID Asc)

If Not Exists (Select * From sys.indexes Where object_id = object_id('APC.DiagnosisCodingTEST') And NAME = 'IX_DiagnosisCodingTEST_DiagnosisCode')
	Create Index IX_DiagnosisCodingTEST_DiagnosisCode On APC.DiagnosisCodingTEST(DiagnosisCode Asc)

If Not Exists (Select * From sys.indexes Where object_id = object_id('APC.DiagnosisCodingTEST') And NAME = 'IX_DiagnosisCodingTEST_SequenceNo')
	Create Index IX_DiagnosisCodingTEST_SequenceNo On APC.DiagnosisCodingTEST(SequenceNo Asc)
