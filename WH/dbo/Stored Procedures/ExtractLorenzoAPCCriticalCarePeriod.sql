﻿CREATE procedure [dbo].[ExtractLorenzoAPCCriticalCarePeriod]
	 
	 @fromDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0


select
	@from = 
		coalesce(
			@fromDate
			,(
			select
				DateValue
			from
				dbo.Parameter
			where
				Parameter = 'EXTRACTCRITICALCAREDATE'
			)
		)


--exec Lorenzo.dbo.BuildExtractDatasetEncounterAPC @from

truncate table TImportAPCCriticalCarePeriod



INSERT INTO TImportAPCCriticalCarePeriod
(
	SourceCCPStayNo
	,SourceCCPNo
	,CCPSequenceNo
	,SourceEncounterNo
	,SourceWardStayNo
	,SourceSpellNo
	,CCPIndicator
	,CCPStartDate
	,CCPEndDate
	,CCPReadyDate
	,CCPNoOfOrgansSupported
	,CCPICUDays
	,CCPHDUDays
	,CCPAdmissionSource
	,CCPAdmissionType
	,CCPAdmissionSourceLocation
	,CCPUnitFunction
	,CCPUnitBedFunction
	,CCPDischargeStatus
	,CCPDischargeDestination
	,CCPDischargeLocation
	,CCPStayOrganSupported
	,CCPStayCareLevel
	,CCPStayWard
	,CCPStayDate
	,CCPStay
	)
(
SELECT
	 SourceCCPStayNo
	,SourceCCPNo
	,CCPSequenceNo
	,SourceEncounterNo
	,SourceWardStayNo
	,SourceSpellNo
	,CCPIndicator
	,CCPStartDate
	,CCPEndDate
	,CCPReadyDate
	,CCPNoOfOrgansSupported
	,CCPICUDays
	,CCPHDUDays
	,CCPAdmissionSource
	,CCPAdmissionType
	,CCPAdmissionSourceLocation
	,CCPUnitFunction
	,CCPUnitBedFunction
	,CCPDischargeStatus
	,CCPDischargeDestination
	,CCPDischargeLocation
	,CCPStayOrganSupported
	,CCPStayCareLevel
	,CCPStayWard
	,CCPStayDate
	,CCPStay
	FROM Lorenzo.dbo.ExtractACPCCP
	)


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Extract from ' + CONVERT(varchar(20), @from, 130) + ', '  + 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH ExtractLorenzoCriticalCarePeriod', @Stats, @StartTime

print @Stats

