﻿CREATE procedure [dbo].[ExtractLorenzoAPCWaitingList]
	 @census smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

select @RowsInserted = 0

select
	@census = 
		coalesce(
			@census
			,dateadd(day, datediff(day, 0, getdate()), 0)
		)


update
	Lorenzo.dbo.Parameter
set
	DateValue = @census
where
	Parameter = 'APCWLCENSUSDATE'

if @@rowcount = 0
	insert into Lorenzo.dbo.Parameter
		(
		Parameter
		,DateValue
		)
	values
		(
		'APCWLCENSUSDATE'
		,@census
		)


insert into dbo.TImportAPCWaitingList
(
	 CensusDate
	,SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,ReferralSourceUniqueID
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,ConsultantCode
	,SpecialtyCode
	,PASSpecialtyCode
	,ManagementIntentionCode
	,AdmissionMethodCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,IntendedPrimaryOperationCode
	,Operation
	,SiteCode
	,WardCode
	,WLStatus
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,AdminCategoryCode
	,CancelledBy
	,BookingTypeCode
	,CasenoteNumber
	,OriginalDateOnWaitingList
	,DateOnWaitingList
	,TCIDate
	,KornerWait
	,CountOfDaysSuspended
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,SuspensionReason
	,InterfaceCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,BreachDate
	,ExpectedAdmissionDate
	,ExpectedDischargeDate
	,ReferralDate
	,FutureCancelDate
	,ProcedureTime
	,TheatreCode
	,AnaestheticTypeCode
	,AdmissionReason
	,EpisodeNo
	,BreachDays
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NationalBreachDate
	,NationalBreachDays
	,DerivedBreachDays
	,DerivedClockStartDate
	,DerivedBreachDate
	,RTTBreachDate
	,RTTDiagnosticBreachDate
	,NationalDiagnosticBreachDate
	,SocialSuspensionDays
	,BreachTypeCode
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,ArchiveFlag
	,CancelReasonCode
	,GeneralComment
	,PatientPreparation
	,AdmissionOfferOutcomeCode
	,EstimatedTheatreTime
	,WhoCanOperateCode
	,WaitingListRuleCode
	,ShortNoticeCode
	,PreOpDate
	,PreOpClinicCode
	,AdmissionOfferSourceUniqueID
	,LocalCategoryCode
)
select
	 @census
	,Encounter.SourceUniqueID
	,Encounter.SourcePatientNo
	,Encounter.SourceEncounterNo
	,Encounter.ReferralSourceUniqueID
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath
	,Encounter.SexCode
	,Encounter.NHSNumber
	,Encounter.DistrictNo
	,Encounter.Postcode
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.DHACode
	,Encounter.HomePhone
	,Encounter.WorkPhone
	,Encounter.EthnicOriginCode
	,Encounter.MaritalStatusCode
	,Encounter.ReligionCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.PASSpecialtyCode
	,Encounter.ManagementIntentionCode
	,Encounter.AdmissionMethodCode
	,Encounter.PriorityCode
	,Encounter.WaitingListCode
	,Encounter.CommentClinical
	,Encounter.CommentNonClinical
	,Encounter.IntendedPrimaryOperationCode
	,Encounter.Operation
	,Encounter.SiteCode
	,Encounter.WardCode
	,Encounter.WLStatus
	,Encounter.PurchaserCode
	,Encounter.ProviderCode
	,Encounter.ContractSerialNo
	,Encounter.AdminCategoryCode
	,Encounter.CancelledBy
	,Encounter.BookingTypeCode
	,Encounter.CasenoteNumber
	,Encounter.OriginalDateOnWaitingList
	,Encounter.DateOnWaitingList
	,Encounter.TCIDate

	,KornerWait =
		datediff(
			day
			,Encounter.DateOnWaitingList
			,@census
		) - 
		coalesce(
			Encounter.CountOfDaysSuspended
			,0
		)

	,Encounter.CountOfDaysSuspended
	,Encounter.SuspensionStartDate
	,Encounter.SuspensionEndDate
	,Encounter.SuspensionReasonCode
	,Encounter.SuspensionReason
	,Encounter.InterfaceCode
	,Encounter.RegisteredGpCode
	,Encounter.RegisteredGpPracticeCode
	,Encounter.EpisodicGpCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.SourceTreatmentFunctionCode
	,Encounter.TreatmentFunctionCode
	,Encounter.NationalSpecialtyCode
	,Encounter.PCTCode
	,Encounter.BreachDate
	,Encounter.ExpectedAdmissionDate
	,Encounter.ExpectedDischargeDate
	,Encounter.ReferralDate
	,Encounter.FutureCancelDate
	,Encounter.ProcedureTime
	,Encounter.TheatreCode
	,Encounter.AnaestheticTypeCode
	,Encounter.AdmissionReason
	,Encounter.EpisodeNo
	,Encounter.BreachDays
	,Encounter.MRSAFlag
	,Encounter.RTTPathwayID
	,Encounter.RTTPathwayCondition
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.RTTSpecialtyCode
	,Encounter.RTTCurrentProviderCode
	,Encounter.RTTCurrentStatusCode
	,Encounter.RTTCurrentStatusDate
	,Encounter.RTTCurrentPrivatePatientFlag
	,Encounter.RTTOverseasStatusFlag
	,Encounter.NationalBreachDate
	,Encounter.NationalBreachDays
	,Encounter.DerivedBreachDays
	,Encounter.DerivedClockStartDate
	,Encounter.DerivedBreachDate
	,Encounter.RTTBreachDate
	,Encounter.RTTDiagnosticBreachDate
	,Encounter.NationalDiagnosticBreachDate
	,Encounter.SocialSuspensionDays
	,Encounter.BreachTypeCode
	,Encounter.PASCreated
	,Encounter.PASUpdated
	,Encounter.PASCreatedByWhom
	,Encounter.PASUpdatedByWhom
	,Encounter.ArchiveFlag
	,Encounter.CancelReasonCode
	,Encounter.GeneralComment
	,Encounter.PatientPreparation
	,Encounter.AdmissionOfferOutcomeCode
	,Encounter.EstimatedTheatreTime
	,Encounter.WhoCanOperateCode
	,Encounter.WaitingListRuleCode
	,Encounter.ShortNoticeCode
	,Encounter.PreOpDate
	,Encounter.PreOpClinicCode
	,Encounter.AdmissionOfferSourceUniqueID
	,Encounter.LocalCategoryCode
from
	Lorenzo.dbo.ExtractAPCWL Encounter

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Census ' + CONVERT(varchar(20), @census) + ', '  + 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH ExtractLorenzoAPCWaitingList', @Stats, @StartTime

print @Stats


