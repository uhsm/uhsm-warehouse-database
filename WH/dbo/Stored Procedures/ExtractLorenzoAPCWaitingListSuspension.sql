﻿CREATE procedure [dbo].[ExtractLorenzoAPCWaitingListSuspension]
	 @census smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

select @RowsInserted = 0

select
	@census = 
		coalesce(
			@census
			,dateadd(day, datediff(day, 0, getdate()), 0)
		)

update
	Lorenzo.dbo.Parameter
set
	DateValue = @census
where
	Parameter = 'APCWLCENSUSDATE'

if @@rowcount = 0
	insert into Lorenzo.dbo.Parameter
		(
		Parameter
		,DateValue
		)
	values
		(
		'APCWLCENSUSDATE'
		,@census
		)


insert into dbo.TImportAPCWaitingListSuspension
(
	 CensusDate
	,SourceUniqueID
	,WaitingListSourceUniqueID
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,ArchiveFlag
)
select
	 @census
	,SourceUniqueID
	,WaitingListSourceUniqueID
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,ArchiveFlag
from
	Lorenzo.dbo.ExtractAPCWLSuspension Encounter


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Census ' + CONVERT(varchar(20), @census) + ', '  + 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH ExtractLorenzoAPCSuspension', @Stats, @StartTime

print @Stats


