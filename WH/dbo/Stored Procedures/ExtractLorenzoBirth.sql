﻿CREATE procedure [dbo].[ExtractLorenzoBirth]
	 @fromDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0


select
	@from = 
		coalesce(
			@fromDate
			,(
			select
				DateValue
			from
				dbo.Parameter
			where
				Parameter = 'EXTRACTAPCDATE'
			)
		)


insert into dbo.TImportAPCBirth
(
	 SourceUniqueID
	,MaternitySpellSourceUniqueID
	,SourcePatientNo
	,ReferralSourceUniqueID
	,ProfessionalCarerCode
	,MotherSourcePatientNo
	,BirthOrder
	,DateOfBirth
	,SexCode
	,StatusOfPersonConductingDeliveryCode
	,LiveOrStillBirthCode
	,DeliveryPlaceCode
	,ResuscitationMethodByPressureCode
	,ResuscitationMethodByDrugCode
	,BirthWeight
	,ApgarScoreAt1Minute
	,ApgarScoreAt5Minutes
	,BCGAdministeredCode
	,CircumferenceOfBabysHead
	,LengthOfBaby
	,ExaminationOfHipsCode
	,FollowUpCareCode
	,FoetalPresentationCode
	,MetabolicScreeningCode
	,JaundiceCode
	,FeedingTypeCode
	,DeliveryMethodCode
	,MaternityDrugsCode
	,GestationLength
	,ApgarScoreAt10Minutes
	,NeonatalLevelOfCareCode
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,ArchiveFlag
)
select
	 SourceUniqueID
	,MaternitySpellSourceUniqueID
	,SourcePatientNo
	,ReferralSourceUniqueID
	,ProfessionalCarerCode
	,MotherSourcePatientNo
	,BirthOrder
	,DateOfBirth
	,SexCode
	,StatusOfPersonConductingDeliveryCode
	,LiveOrStillBirthCode
	,DeliveryPlaceCode
	,ResuscitationMethodByPressureCode
	,ResuscitationMethodByDrugCode
	,BirthWeight
	,ApgarScoreAt1Minute
	,ApgarScoreAt5Minutes
	,BCGAdministeredCode
	,CircumferenceOfBabysHead
	,LengthOfBaby
	,ExaminationOfHipsCode
	,FollowUpCareCode
	,FoetalPresentationCode
	,MetabolicScreeningCode
	,JaundiceCode
	,FeedingTypeCode
	,DeliveryMethodCode
	,MaternityDrugsCode
	,GestationLength
	,ApgarScoreAt10Minutes
	,NeonatalLevelOfCareCode
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,ArchiveFlag
from
	Lorenzo.dbo.ExtractBirth Birth
where
	Birth.Created > @from


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Extract from ' + CONVERT(varchar(20), @from, 130) + ', '  + 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH ExtractLorenzoBirth', @Stats, @StartTime

print @Stats


