﻿create procedure dbo.ExtractLorenzoClinicalCoding
	 @fromDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0


select
	@from = 
		coalesce(
			@fromDate
			,(
			select
				DateValue
			from
				dbo.Parameter
			where
				Parameter = 'EXTRACTCLINICALCODINGDATE'
			)
		)


--exec Lorenzo.dbo.BuildExtractDatasetEncounterAPC @from

insert into dbo.TImportClinicalCoding
(
	 SourceUniqueID
	,SourceCode
	,SourceRecno
	,SequenceNo
	,ClinicalCodingTime
	,ClinicalCodingTypeCode
	,ClinicalCodingCode
	,CancelDate
	,PASCreatedDate
	,PASModifiedDate
	,PASCreatedByWhom
	,PASUpdatedByWhom
)
select
	 SourceUniqueID = ClinicalCoding.DGPRO_REFNO
	,SourceCode = ClinicalCoding.SORCE_CODE
	,SourceRecno = ClinicalCoding.SORCE_REFNO
	,SequenceNo = ClinicalCoding.SORT_ORDER
	,ClinicalCodingTime = ClinicalCoding.DGPRO_DTTM
	,ClinicalCodingTypeCode = ClinicalCoding.DPTYP_CODE
	,ClinicalCodingCode = ClinicalCoding.ODPCD_REFNO
	,CancelDate = ClinicalCoding.CANCEL_DTTM
	,PASCreatedDate = ClinicalCoding.CREATE_DTTM
	,PASModifiedDate = ClinicalCoding.MODIF_DTTM
	,PASCreatedByWhom = ClinicalCoding.USER_CREATE
	,PASUpdatedByWhom = ClinicalCoding.USER_MODIF
from
	Lorenzo.dbo.ClinicalCoding ClinicalCoding
where
	ClinicalCoding.SORCE_CODE in ('PRCAE', 'SCHDL', 'WLIST')
and	ClinicalCoding.ARCHV_FLAG = 'N'
and	ClinicalCoding.Created > @from


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Extract from ' + CONVERT(varchar(20), @from, 130) + ', '  + 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'ExtractLorenzoClinicalCoding', @Stats, @StartTime

print @Stats


