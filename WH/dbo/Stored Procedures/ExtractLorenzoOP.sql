﻿

/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		ExtractLorenzoOP - creates TImportOPEncounter

Notes:			Stored in 
	

Versions:

				1.0.0.1 - 27/01/2016 - CM
					Amended so that EpisodicGPCode and EpisodicGPPracticeCode so that these fields are not populated into TImportOPEncounter at this stage. 
					Fields to be populated in TLoadAPCEncounter further down stream to use PAS.PatientGPHistory so that it is the same as the A&E and APC method of generating the Episodic GP data.
					
				1.0.0.0
					Original store proc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/



CREATE procedure [dbo].[ExtractLorenzoOP]
	 @fromDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0


select
	@from = 
		coalesce(
			@fromDate
			,(
			select
				DateValue
			from
				dbo.Parameter
			where
				Parameter = 'EXTRACTOPDATE'
			)
		)


exec Lorenzo.dbo.BuildExtractDatasetEncounterOP @from

insert into dbo.TImportOPEncounter
(
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,ReferralSourceUniqueID
	,WaitingListSourceUniqueID
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,NHSNumberStatusCode
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AppointmentDate
	,AppointmentTime
	,ClinicCode
	,AdminCategoryCode
	,SourceOfReferralCode
	,ReasonForReferralCode
	,PriorityCode
	,FirstAttendanceFlag
	,AppointmentStatusCode
	,CancelledByCode
	,TransportRequiredFlag
	,AppointmentTypeCode
	,DisposalCode
	,ConsultantCode
	,SpecialtyCode
	,ReferringConsultantCode
	,ReferringSpecialtyCode
	,BookingTypeCode
	,CasenoteNo
	,AppointmentCreateDate
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,DoctorCode
	,PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,PrimaryOperationCode
	,PrimaryOperationDate
	,SecondaryOperationCode1
	,SecondaryOperationDate1
	,SecondaryOperationCode2
	,SecondaryOperationDate2
	,SecondaryOperationCode3
	,SecondaryOperationDate3
	,SecondaryOperationCode4
	,SecondaryOperationDate4
	,SecondaryOperationCode5
	,SecondaryOperationDate5
	,SecondaryOperationCode6
	,SecondaryOperationDate6
	,SecondaryOperationCode7
	,SecondaryOperationDate7
	,SecondaryOperationCode8
	,SecondaryOperationDate8
	,SecondaryOperationCode9
	,SecondaryOperationDate9
	,SecondaryOperationCode10
	,SecondaryOperationDate10
	,SecondaryOperationCode11
	,SecondaryOperationDate11
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,ReferralDate
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,AppointmentCategoryCode
	,AppointmentCreatedBy
	,AppointmentCancelDate
	,LastRevisedDate
	,LastRevisedBy
	,OverseasStatusFlag
	,OverseasStatusStartDate
	,OverseasStatusEndDate
	,PatientChoiceCode
	,ScheduledCancelReasonCode
	,PatientCancelReason
	,DischargeDate
	,QM08StartWaitDate
	,QM08EndWaitDate
	,DestinationSiteCode
	,EBookingReferenceNo
	,InterfaceCode
	,IsWardAttender
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,ArchiveFlag
	,ClinicType
	,SessionUniqueID
)
select
	 OP.SourceUniqueID
	,OP.SourcePatientNo
	,OP.SourceEncounterNo
	,OP.ReferralSourceUniqueID
	,OP.WaitingListSourceUniqueID
	,OP.PatientTitle
	,OP.PatientForename
	,OP.PatientSurname
	,OP.DateOfBirth
	,OP.DateOfDeath
	,OP.SexCode
	,OP.NHSNumber
	,OP.NHSNumberStatusCode
	,OP.DistrictNo
	,OP.Postcode
	,OP.PatientAddress1
	,OP.PatientAddress2
	,OP.PatientAddress3
	,OP.PatientAddress4
	,OP.DHACode
	,OP.EthnicOriginCode
	,OP.MaritalStatusCode
	,OP.ReligionCode
	,OP.RegisteredGpCode
	,OP.RegisteredGpPracticeCode
	,OP.SiteCode
	,OP.AppointmentDate
	,OP.AppointmentTime
	,OP.ClinicCode
	,OP.AdminCategoryCode
	,OP.SourceOfReferralCode
	,OP.ReasonForReferralCode
	,OP.PriorityCode
	,OP.FirstAttendanceFlag
	,OP.AppointmentStatusCode
	,OP.CancelledByCode
	,OP.TransportRequiredFlag
	,OP.AppointmentTypeCode
	,OP.DisposalCode
	,OP.ConsultantCode
	,OP.SpecialtyCode
	,OP.ReferringConsultantCode
	,OP.ReferringSpecialtyCode
	,OP.BookingTypeCode
	,OP.CasenoteNo
	,OP.AppointmentCreateDate
	,NULL--OP.EpisodicGpCode CM 27/01/2016 - amendment
	,NULL--OP.EpisodicGpPracticeCode CM 27/01/2016 - amendment
	,OP.DoctorCode
	,OP.PrimaryDiagnosisCode
	,OP.SubsidiaryDiagnosisCode
	,OP.SecondaryDiagnosisCode1
	,OP.SecondaryDiagnosisCode2
	,OP.SecondaryDiagnosisCode3
	,OP.SecondaryDiagnosisCode4
	,OP.SecondaryDiagnosisCode5
	,OP.SecondaryDiagnosisCode6
	,OP.SecondaryDiagnosisCode7
	,OP.SecondaryDiagnosisCode8
	,OP.SecondaryDiagnosisCode9
	,OP.SecondaryDiagnosisCode10
	,OP.SecondaryDiagnosisCode11
	,OP.SecondaryDiagnosisCode12
	,OP.PrimaryOperationCode
	,OP.PrimaryOperationDate
	,OP.SecondaryOperationCode1
	,OP.SecondaryOperationDate1
	,OP.SecondaryOperationCode2
	,OP.SecondaryOperationDate2
	,OP.SecondaryOperationCode3
	,OP.SecondaryOperationDate3
	,OP.SecondaryOperationCode4
	,OP.SecondaryOperationDate4
	,OP.SecondaryOperationCode5
	,OP.SecondaryOperationDate5
	,OP.SecondaryOperationCode6
	,OP.SecondaryOperationDate6
	,OP.SecondaryOperationCode7
	,OP.SecondaryOperationDate7
	,OP.SecondaryOperationCode8
	,OP.SecondaryOperationDate8
	,OP.SecondaryOperationCode9
	,OP.SecondaryOperationDate9
	,OP.SecondaryOperationCode10
	,OP.SecondaryOperationDate10
	,OP.SecondaryOperationCode11
	,OP.SecondaryOperationDate11
	,OP.PurchaserCode
	,OP.ProviderCode
	,OP.ContractSerialNo
	,OP.ReferralDate
	,OP.RTTPathwayID
	,OP.RTTPathwayCondition
	,OP.RTTStartDate
	,OP.RTTEndDate
	,OP.RTTSpecialtyCode
	,OP.RTTCurrentProviderCode
	,OP.RTTCurrentStatusCode
	,OP.RTTCurrentStatusDate
	,OP.RTTCurrentPrivatePatientFlag
	,OP.RTTOverseasStatusFlag
	,OP.RTTPeriodStatusCode
	,OP.AppointmentCategoryCode
	,OP.AppointmentCreatedBy
	,OP.AppointmentCancelDate
	,OP.LastRevisedDate
	,OP.LastRevisedBy
	,OP.OverseasStatusFlag
	,OP.OverseasStatusStartDate
	,OP.OverseasStatusEndDate
	,OP.PatientChoiceCode
	,OP.ScheduledCancelReasonCode
	,OP.PatientCancelReason
	,OP.DischargeDate
	,OP.QM08StartWaitDate
	,OP.QM08EndWaitDate
	,OP.DestinationSiteCode
	,OP.EBookingReferenceNo
	,OP.InterfaceCode
	,OP.IsWardAttender
	,OP.PASCreated
	,OP.PASUpdated
	,OP.PASCreatedByWhom
	,OP.PASUpdatedByWhom
	,OP.ArchiveFlag
	,OP.ClinicType
	,OP.SessionUniqueID
from
	Lorenzo.dbo.ExtractOP OP

inner join Lorenzo.dbo.ExtractDatasetEncounter
on	ExtractDatasetEncounter.SourceUniqueID = OP.SourceUniqueID
and	ExtractDatasetEncounter.DatasetCode = 'OUTPATIENT'



select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

update dbo.TImportOPEncounter
set
	PrimaryOperationCode = PrimaryOperation.CODE
	,PrimaryOperationDate = PrimaryOperation.DGPRO_DTTM
from
	Lorenzo.dbo.ClinicalCoding PrimaryOperation
where
	PrimaryOperation.SORCE_REFNO = TImportOPEncounter.SourceUniqueID
and	PrimaryOperation.SORCE_CODE = 'SCHDL'
and	PrimaryOperation.SORT_ORDER = 1
and	PrimaryOperation.DPTYP_CODE = 'PROCE'
and	ARCHV_FLAG = 'N'

update dbo.TImportOPEncounter
set
	SecondaryOperationCode1 = SecondaryOperation1.CODE
	,SecondaryOperationDate1 = SecondaryOperation1.DGPRO_DTTM
from
	Lorenzo.dbo.ClinicalCoding SecondaryOperation1
where
	SecondaryOperation1.SORCE_REFNO = TImportOPEncounter.SourceUniqueID
and	SecondaryOperation1.SORCE_CODE = 'SCHDL'
and	SecondaryOperation1.SORT_ORDER = 2
and	SecondaryOperation1.DPTYP_CODE = 'PROCE'
and	ARCHV_FLAG = 'N'

update dbo.TImportOPEncounter
set
	SecondaryOperationCode2 = SecondaryOperation2.CODE
	,SecondaryOperationDate2 = SecondaryOperation2.DGPRO_DTTM
from
	Lorenzo.dbo.ClinicalCoding SecondaryOperation2
where
	SecondaryOperation2.SORCE_REFNO = TImportOPEncounter.SourceUniqueID
and	SecondaryOperation2.SORCE_CODE = 'SCHDL'
and	SecondaryOperation2.SORT_ORDER = 3
and	SecondaryOperation2.DPTYP_CODE = 'PROCE'
and	ARCHV_FLAG = 'N'

update dbo.TImportOPEncounter
set
	SecondaryOperationCode3 = SecondaryOperation3.CODE
	,SecondaryOperationDate3 = SecondaryOperation3.DGPRO_DTTM
from
	Lorenzo.dbo.ClinicalCoding SecondaryOperation3
where
	SecondaryOperation3.SORCE_REFNO = TImportOPEncounter.SourceUniqueID
and	SecondaryOperation3.SORCE_CODE = 'SCHDL'
and	SecondaryOperation3.SORT_ORDER = 4
and	SecondaryOperation3.DPTYP_CODE = 'PROCE'
and	ARCHV_FLAG = 'N'

update dbo.TImportOPEncounter
set
	SecondaryOperationCode4 = SecondaryOperation4.CODE
	,SecondaryOperationDate4 = SecondaryOperation4.DGPRO_DTTM
from
	Lorenzo.dbo.ClinicalCoding SecondaryOperation4
where
	SecondaryOperation4.SORCE_REFNO = TImportOPEncounter.SourceUniqueID
and	SecondaryOperation4.SORCE_CODE = 'SCHDL'
and	SecondaryOperation4.SORT_ORDER = 5
and	SecondaryOperation4.DPTYP_CODE = 'PROCE'
and	ARCHV_FLAG = 'N'

update dbo.TImportOPEncounter
set
	SecondaryOperationCode5 = SecondaryOperation5.CODE
	,SecondaryOperationDate5 = SecondaryOperation5.DGPRO_DTTM
from
	Lorenzo.dbo.ClinicalCoding SecondaryOperation5
where
	SecondaryOperation5.SORCE_REFNO = TImportOPEncounter.SourceUniqueID
and	SecondaryOperation5.SORCE_CODE = 'SCHDL'
and	SecondaryOperation5.SORT_ORDER = 6
and	SecondaryOperation5.DPTYP_CODE = 'PROCE'
and	ARCHV_FLAG = 'N'

update dbo.TImportOPEncounter
set
	SecondaryOperationCode6 = SecondaryOperation6.CODE
	,SecondaryOperationDate6 = SecondaryOperation6.DGPRO_DTTM
from
	Lorenzo.dbo.ClinicalCoding SecondaryOperation6
where
	SecondaryOperation6.SORCE_REFNO = TImportOPEncounter.SourceUniqueID
and	SecondaryOperation6.SORCE_CODE = 'SCHDL'
and	SecondaryOperation6.SORT_ORDER = 7
and	SecondaryOperation6.DPTYP_CODE = 'PROCE'
and	ARCHV_FLAG = 'N'

update dbo.TImportOPEncounter
set
	SecondaryOperationCode7 = SecondaryOperation7.CODE
	,SecondaryOperationDate7 = SecondaryOperation7.DGPRO_DTTM
from
	Lorenzo.dbo.ClinicalCoding SecondaryOperation7
where
	SecondaryOperation7.SORCE_REFNO = TImportOPEncounter.SourceUniqueID
and	SecondaryOperation7.SORCE_CODE = 'SCHDL'
and	SecondaryOperation7.SORT_ORDER = 8
and	SecondaryOperation7.DPTYP_CODE = 'PROCE'
and	ARCHV_FLAG = 'N'

update dbo.TImportOPEncounter
set
	SecondaryOperationCode8 = SecondaryOperation8.CODE
	,SecondaryOperationDate8 = SecondaryOperation8.DGPRO_DTTM
from
	Lorenzo.dbo.ClinicalCoding SecondaryOperation8
where
	SecondaryOperation8.SORCE_REFNO = TImportOPEncounter.SourceUniqueID
and	SecondaryOperation8.SORCE_CODE = 'SCHDL'
and	SecondaryOperation8.SORT_ORDER = 9
and	SecondaryOperation8.DPTYP_CODE = 'PROCE'
and	ARCHV_FLAG = 'N'

update dbo.TImportOPEncounter
set
	SecondaryOperationCode9 = SecondaryOperation9.CODE
	,SecondaryOperationDate9 = SecondaryOperation9.DGPRO_DTTM
from
	Lorenzo.dbo.ClinicalCoding SecondaryOperation9
where
	SecondaryOperation9.SORCE_REFNO = TImportOPEncounter.SourceUniqueID
and	SecondaryOperation9.SORCE_CODE = 'SCHDL'
and	SecondaryOperation9.SORT_ORDER = 10
and	SecondaryOperation9.DPTYP_CODE = 'PROCE'
and	ARCHV_FLAG = 'N'

update dbo.TImportOPEncounter
set
	SecondaryOperationCode10 = SecondaryOperation10.CODE
	,SecondaryOperationDate10 = SecondaryOperation10.DGPRO_DTTM
from
	Lorenzo.dbo.ClinicalCoding SecondaryOperation10
where
	SecondaryOperation10.SORCE_REFNO = TImportOPEncounter.SourceUniqueID
and	SecondaryOperation10.SORCE_CODE = 'SCHDL'
and	SecondaryOperation10.SORT_ORDER = 11
and	SecondaryOperation10.DPTYP_CODE = 'PROCE'
and	ARCHV_FLAG = 'N'

update dbo.TImportOPEncounter
set
	SecondaryOperationCode11 = SecondaryOperation11.CODE
	,SecondaryOperationDate11 = SecondaryOperation11.DGPRO_DTTM
from
	Lorenzo.dbo.ClinicalCoding SecondaryOperation11
where
	SecondaryOperation11.SORCE_REFNO = TImportOPEncounter.SourceUniqueID
and	SecondaryOperation11.SORCE_CODE = 'SCHDL'
and	SecondaryOperation11.SORT_ORDER = 12
and	SecondaryOperation11.DPTYP_CODE = 'PROCE'
and	ARCHV_FLAG = 'N'



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Extract from ' + CONVERT(varchar(20), @from, 130) + ', '  + 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH ExtractLorenzoOP', @Stats, @StartTime

print @Stats


