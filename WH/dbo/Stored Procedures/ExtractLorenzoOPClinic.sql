﻿CREATE procedure [dbo].[ExtractLorenzoOPClinic]
	 @fromDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0


select
	@from = 
		coalesce(
			@fromDate
			,(
			select
				DateValue
			from
				dbo.Parameter
			where
				Parameter = 'EXTRACTOPREFERENCEDATE'
			)
		)



insert into dbo.TImportOPClinic
(
	 [ServicePointUniqueID]
	,[ClinicCode]
	,[ClinicName]
	,[ClinicDescription]
	,[ClinicLocation]
	,[ClinicEffectiveFromDate]
	,[ClinicEffectiveToDate]
	,[ClinicSpecialtyCode]
	,[ClinicProfessionalCarerCode]
	,[ClinicPurposeCode]
	,[Horizon]
	,[PartialBookingFlag]
	,[PartialBookingLeadTime]
	,[ArchiveFlag]
	,[PASCreated]
	,[PASUpdated]
	,[PASCreatedByWhom]
	,[PASUpdatedByWhom]
	 	)
Select 
	 [ServicePointUniqueID]
	,[ClinicCode]
	,[ClinicName]
	,[ClinicDescription]
	,[ClinicLocation]
	,[ClinicEffectiveFromDate]
	,[ClinicEffectiveToDate]
	,[ClinicSpecialtyCode]
	,[ClinicProfessionalCarerCode]
	,[ClinicPurposeCode]
	,[Horizon]
	,[PartialBookingFlag]
	,[PartialBookingLeadTime]
	,[ArchiveFlag]
	,[PASCreated]
	,[PASUpdated]
	,[PASCreatedByWhom]
	,[PASUpdatedByWhom]
from Lorenzo.dbo.ExtractOPClinic

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Extract from ' + CONVERT(varchar(20), @from, 130) + ', '  + 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH ExtractLorenzoOPClinic', @Stats, @StartTime

print @Stats
