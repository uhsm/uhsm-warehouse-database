﻿CREATE procedure [dbo].[ExtractLorenzoOPRule]
	 @fromDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0


select
	@from = 
		coalesce(
			@fromDate
			,(
			select
				DateValue
			from
				dbo.Parameter
			where
				Parameter = 'EXTRACTOPREFERENCEDATE'
			)
		)



insert into dbo.TImportOPRule
(
	 [RuleUniqueID]
	,[AppliedTo]
	,[AppliedToUniqueID]
	,[EnforcedFlag]
	,[RuleAppliedUniqueID]
	,[RuleValueCode]
	,[ArchiveFlag]
	,[PASCreated]
	,[PASUpdated]
	,[PASCreatedByWhom]
	,[PASUpdatedByWhom]
	)
Select 
	Encounter.[RuleUniqueID]
	,Encounter.[AppliedTo]
	,Encounter.[AppliedToUniqueID]
	,Encounter.[EnforcedFlag]
	,Encounter.[RuleAppliedUniqueID]
	,Encounter.[RuleValueCode]
	,Encounter.[ArchiveFlag]
	,Encounter.[PASCreated]
	,Encounter.[PASUpdated]
	,Encounter.[PASCreatedByWhom]
	,Encounter.[PASUpdatedByWhom]
from
	Lorenzo.dbo.ExtractOPRule Encounter
where
	Encounter.Created > @from



select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Extract from ' + CONVERT(varchar(20), @from, 130) + ', '  + 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH ExtractLorenzoOPRule', @Stats, @StartTime

print @Stats
