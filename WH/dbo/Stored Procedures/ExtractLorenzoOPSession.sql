﻿CREATE procedure [dbo].[ExtractLorenzoOPSession]
	 @fromDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0


select
	@from = 
		coalesce(
			@fromDate
			,(
			select
				DateValue
			from
				dbo.Parameter
			where
				Parameter = 'EXTRACTOPREFERENCEDATE'
			)
		)



insert into dbo.TImportOPSession
(
	 [SessionUniqueID]
	,[ServicePointUniqueID]
	,[SessionCode]
	,[SessionName]
	,[SessionDate]
	,[SessionStartTime]
	,[SessionStartDateTime]
	,[SessionEndTime]
	,[SessionEndDateTime]
	,[SessionDuration]
	,[SlotDuration]
	,[SessionStatusCode]
	,[SessionStatusChangeReasonCode]
	,[SessionStatusChangeRequestByCode]
	,[SessionSpecialtyCode]
	,[SessionProfessionalCarerCode]
	,[ActualSessionProfessionalCarerCode]
	,[SessionStartStatusCode]
	,[SessionStartStatusReasonCode]
	,[SessionEndStatusCode]
	,[SessionEndStatusReasonCode]
	,[VariableSlotsFlag]
	,[AllowOverbookedSlots]
	,[PartialBookingFlag]
	,[PartialBookingLeadTime]
	,[EBSPublishedFlag]
	,[EBSPublishAllSlots]
	,[EBSServiceID]
	,[NewVisits]
	,[SessionTemplateUnqiueID]
	,[SessionCancelledFlag]
	,[SessionCancelledDate]
	,[SessionInstructions]
	,[SessionComments]
	,[IsTemplate]
	,[FrequencyCode]
	,[FrequencyOffsetCode]
	,[SessionFrequencyCode]
	,[SessionFrequencyOffsetCode]
	,[SessionWeeks]
	,[SessionDays]
	,[DaysInWeek1]
	,[DaysInWeek2]
	,[DaysInWeek3]
	,[DaysInWeek4]
	,[DaysInWeek5]
	,[DaysInWeekLast]
	,[ArchiveFlag]
	,[PASCreated]
	,[PASUpdated]
	,[PASCreatedByWhom]
	,[PASUpdatedByWhom]
	 	)
Select 
	 [SessionUniqueID]
	,[ServicePointUniqueID]
	,[SessionCode]
	,[SessionName]
	,[SessionDate]
	,[SessionStartTime]
	,[SessionStartDateTime]
	,[SessionEndTime]
	,[SessionEndDateTime]
	,[SessionDuration]
	,[SlotDuration]
	,[SessionStatusCode]
	,[SessionStatusChangeReasonCode]
	,[SessionStatusChangeRequestByCode]
	,[SessionSpecialtyCode]
	,[SessionProfessionalCarerCode]
	,[ActualSessionProfessionalCarerCode]
	,[SessionStartStatusCode]
	,[SessionStartStatusReasonCode]
	,[SessionEndStatusCode]
	,[SessionEndStatusReasonCode]
	,[VariableSlotsFlag]
	,[AllowOverbookedSlots]
	,[PartialBookingFlag]
	,[PartialBookingLeadTime]
	,[EBSPublishedFlag]
	,[EBSPublishAllSlots]
	,[EBSServiceID]
	,[NewVisits]
	,[SessionTemplateUnqiueID]
	,[SessionCancelledFlag]
	,[SessionCancelledDate]
	,[SessionInstructions]
	,[SessionComments]
	,[IsTemplate]
	,[FrequencyCode]
	,[FrequencyOffsetCode]
	,[SessionFrequencyCode]
	,[SessionFrequencyOffsetCode]
	,[SessionWeeks]
	,[SessionDays]
	,[DaysInWeek1]
	,[DaysInWeek2]
	,[DaysInWeek3]
	,[DaysInWeek4]
	,[DaysInWeek5]
	,[DaysInWeekLast]
	,[ArchiveFlag]
	,[PASCreated]
	,[PASUpdated]
	,[PASCreatedByWhom]
	,[PASUpdatedByWhom]
from
	Lorenzo.dbo.ExtractOPSession
where
	Created > @from
 


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Extract from ' + CONVERT(varchar(20), @from, 130) + ', '  + 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH ExtractLorenzoOPSession', @Stats, @StartTime

print @Stats
