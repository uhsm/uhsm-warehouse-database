﻿CREATE procedure [dbo].[ExtractLorenzoOPSlot]
	 @fromDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0


select
	@from = 
		coalesce(
			@fromDate
			,(
			select
				DateValue
			from
				dbo.Parameter
			where
				Parameter = 'EXTRACTOPREFERENCEDATE'
			)
		)



insert into dbo.TImportOPSlot
(
	 SlotUniqueID
	,SessionUniqueID
	,ServicePointUniqueID
	,SlotDate
	,SlotStartTime
	,SlotStartDateTime
	,SlotEndTime
	,SlotEndDateTime
	,SlotDuration
	,SlotStatusCode
	,SlotChangeReasonCode
	,SlotChangeRequestByCode
	,SlotPublishedToEBS
	,SlotMaxBookings
	,SlotNumberofBookings
	,SlotTemplateUnqiueID
	,IsTemplate
	,SlotCancelledFlag
	,SlotCancelledDate
	,SlotInstructions
	,ArchiveFlag
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	)
Select 
	 OPSlot.SlotUniqueID
	,OPSlot.SessionUniqueID
	,OPSlot.ServicePointUniqueID
	,OPSlot.SlotDate
	,OPSlot.SlotStartTime
	,OPSlot.SlotStartDateTime
	,OPSlot.SlotEndTime
	,OPSlot.SlotEndDateTime
	,OPSlot.SlotDuration
	,OPSlot.SlotStatusCode
	,OPSlot.SlotChangeReasonCode
	,OPSlot.SlotChangeRequestByCode
	,OPSlot.SlotPublishedToEBS
	,OPSlot.SlotMaxBookings
	,OPSlot.SlotNumberofBookings
	,OPSlot.SlotTemplateUnqiueID
	,OPSlot.IsTemplate
	,OPSlot.SlotCancelledFlag
	,OPSlot.SlotCancelledDate
	,OPSlot.SlotInstructions
	,OPSlot.ArchiveFlag
	,OPSlot.PASCreated
	,OPSlot.PASUpdated
	,OPSlot.PASCreatedByWhom
	,OPSlot.PASUpdatedByWhom
from
	Lorenzo.dbo.ExtractOPSlot OPSlot
where
	OPSlot.Created > @from


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Extract from ' + CONVERT(varchar(20), @from, 130) + ', '  + 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH ExtractLorenzoOPSlot', @Stats, @StartTime

print @Stats
