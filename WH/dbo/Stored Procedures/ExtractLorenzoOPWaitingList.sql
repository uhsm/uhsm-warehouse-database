﻿CREATE procedure [dbo].[ExtractLorenzoOPWaitingList]
	 @census smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

select @RowsInserted = 0

select
	@census = 
		coalesce(
			@census
			,dateadd(day, datediff(day, 0, getdate()), 0)
		)

update
	Lorenzo.dbo.Parameter
set
	DateValue = @census
where
	Parameter = 'OPWLCENSUSDATE'

if @@rowcount = 0
	insert into Lorenzo.dbo.Parameter
		(
		Parameter
		,DateValue
		)
	values
		(
		'OPWLCENSUSDATE'
		,@census
		)


insert into dbo.TImportOPWaitingList
(
	 CensusDate
	,SourceUniqueID
	,SourceUniqueIDTypeCode
	,SourcePatientNo
	,ReferralSourceUniqueID
	,AppointmentSourceUniqueID
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,ConsultantCode
	,SpecialtyCode
	,PASSpecialtyCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,SiteCode
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,AdminCategoryCode
	,CancelledBy
	,DoctorCode
	,BookingTypeCode
	,CasenoteNumber
	,InterfaceCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,ReferralDate
	,BookedDate
	,BookedTime
	,AppointmentDate
	,AppointmentTypeCode
	,AppointmentStatusCode
	,AppointmentCategoryCode
	,QM08StartWaitDate
	,QM08EndWaitDate
	,AppointmentTime
	,ClinicCode
	,SourceOfReferralCode
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,ArchiveFlag
	,FuturePatientCancelDate
	,AdditionFlag
	,CountOfDNAs
	,CountOfHospitalCancels
	,CountOfPatientCancels
	,DateOnWaitingList
	,IntendedPrimaryOperationCode
	,Operation
	,WaitingListRule
	,VisitCode
	,InviteDate
	,WaitingListClinicCode
	,GeneralComment
)
select
	 @census
	,Encounter.SourceUniqueID
	,Encounter.SourceUniqueIDTypeCode
	,Encounter.SourcePatientNo
	,Encounter.ReferralSourceUniqueID
	,Encounter.AppointmentSourceUniqueID
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath
	,Encounter.SexCode
	,Encounter.NHSNumber
	,Encounter.DistrictNo
	,Encounter.Postcode
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.DHACode
	,Encounter.HomePhone
	,Encounter.WorkPhone
	,Encounter.EthnicOriginCode
	,Encounter.MaritalStatusCode
	,Encounter.ReligionCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.PASSpecialtyCode
	,Encounter.PriorityCode
	,Encounter.WaitingListCode
	,Encounter.CommentClinical
	,Encounter.CommentNonClinical
	,Encounter.SiteCode
	,Encounter.PurchaserCode
	,Encounter.ProviderCode
	,Encounter.ContractSerialNo
	,Encounter.AdminCategoryCode
	,Encounter.CancelledBy
	,Encounter.DoctorCode
	,Encounter.BookingTypeCode
	,Encounter.CasenoteNumber
	,Encounter.InterfaceCode
	,Encounter.RegisteredGpCode
	,Encounter.RegisteredGpPracticeCode
	,Encounter.EpisodicGpCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.SourceTreatmentFunctionCode
	,Encounter.TreatmentFunctionCode
	,Encounter.NationalSpecialtyCode
	,Encounter.PCTCode
	,Encounter.ReferralDate
	,Encounter.BookedDate
	,Encounter.BookedTime
	,Encounter.AppointmentDate
	,Encounter.AppointmentTypeCode
	,Encounter.AppointmentStatusCode
	,Encounter.AppointmentCategoryCode
	,Encounter.QM08StartWaitDate
	,Encounter.QM08EndWaitDate
	,Encounter.AppointmentTime
	,Encounter.ClinicCode
	,Encounter.SourceOfReferralCode
	,Encounter.MRSAFlag
	,Encounter.RTTPathwayID
	,Encounter.RTTPathwayCondition
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.RTTSpecialtyCode
	,Encounter.RTTCurrentProviderCode
	,Encounter.RTTCurrentStatusCode
	,Encounter.RTTCurrentStatusDate
	,Encounter.RTTCurrentPrivatePatientFlag
	,Encounter.RTTOverseasStatusFlag
	,Encounter.PASCreated
	,Encounter.PASUpdated
	,Encounter.PASCreatedByWhom
	,Encounter.PASUpdatedByWhom
	,Encounter.ArchiveFlag
	,Encounter.FuturePatientCancelDate
	,Encounter.AdditionFlag
	,Encounter.CountOfDNAs
	,Encounter.CountOfHospitalCancels
	,Encounter.CountOfPatientCancels
	,Encounter.DateOnWaitingList
	,Encounter.IntendedPrimaryOperationCode
	,Encounter.Operation
	,Encounter.WaitingListRule
	,Encounter.VisitCode
	,Encounter.InviteDate
	,Encounter.WaitingListClinicCode
	,Encounter.GeneralComment
from
	Lorenzo.dbo.ExtractOPWL Encounter

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Census ' + CONVERT(varchar(20), @census) + ', '  + 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH ExtractLorenzoOPWaitingList', @Stats, @StartTime

print @Stats





