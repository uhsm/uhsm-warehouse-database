﻿

CREATE procedure [dbo].[ExtractLorenzoRF]
	 @fromDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0


select
	@from = 
		coalesce(
			@fromDate
			,(
			select
				DateValue
			from
				dbo.Parameter
			where
				Parameter = 'EXTRACTRFDATE'
			)
		)


exec Lorenzo.dbo.BuildExtractDatasetEncounterRF @from

insert into dbo.TImportRFEncounter
(
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,NHSNumberStatusCode
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,EpisodicGdpCode
	,SiteCode
	,ConsultantCode
	,SpecialtyCode
	,SourceOfReferralCode
	,PriorityCode
	,ReferralDate
	,DischargeDate
	,DischargeReasonCode
	,DischargeReason
	,AdminCategoryCode
	,ContractSerialNo
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NextFutureAppointmentDate
	,InterfaceCode
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,ArchiveFlag
	,OriginalProviderReferralDate
	,ParentSourceUniqueID
	,RequestedService
	,ReferralStatus
	,CancellationDate
	,CancellationReason
	,ReferralMedium
	,ReferredByConsultantCode
	,ReferredByOrganisationCode 
	,ReferredBySpecialtyCode
	,SuspectedCancerTypeCode
	,ReferralSentDate
	,PatientDeceased
	,GeneralComment
	,ReferralComment
)
select
	 Referral.SourceUniqueID
	,Referral.SourcePatientNo
	,Referral.SourceEncounterNo
	,Referral.PatientTitle
	,Referral.PatientForename
	,Referral.PatientSurname
	,Referral.DateOfBirth
	,Referral.DateOfDeath
	,Referral.SexCode
	,Referral.NHSNumber
	,Referral.NHSNumberStatusCode
	,Referral.DistrictNo
	,Referral.Postcode
	,Referral.PatientAddress1
	,Referral.PatientAddress2
	,Referral.PatientAddress3
	,Referral.PatientAddress4
	,Referral.DHACode
	,Referral.EthnicOriginCode
	,Referral.MaritalStatusCode
	,Referral.ReligionCode
	,Referral.RegisteredGpCode
	,Referral.RegisteredGpPracticeCode
	,Referral.EpisodicGpCode
	,Referral.EpisodicGpPracticeCode
	,Referral.EpisodicGdpCode
	,Referral.SiteCode
	,Referral.ConsultantCode
	,Referral.SpecialtyCode
	,Referral.SourceOfReferralCode
	,Referral.PriorityCode
	,Referral.ReferralDate
	,Referral.DischargeDate
	,Referral.DischargeReasonCode
	,Referral.DischargeReason
	,Referral.AdminCategoryCode
	,Referral.ContractSerialNo
	,Referral.RTTPathwayID
	,Referral.RTTPathwayCondition
	,Referral.RTTStartDate
	,Referral.RTTEndDate
	,Referral.RTTSpecialtyCode
	,Referral.RTTCurrentProviderCode
	,Referral.RTTCurrentStatusCode
	,Referral.RTTCurrentStatusDate
	,Referral.RTTCurrentPrivatePatientFlag
	,Referral.RTTOverseasStatusFlag
	,Referral.NextFutureAppointmentDate
	,Referral.InterfaceCode
	,Referral.PASCreated
	,Referral.PASUpdated
	,Referral.PASCreatedByWhom
	,Referral.PASUpdatedByWhom
	,Referral.ArchiveFlag
	,Referral.OriginalProviderReferralDate
	,Referral.ParentSourceUniqueID
	,Referral.RequestedService
	,Referral.ReferralStatus
	,Referral.CancellationDate
	,Referral.CancellationReason
	,Referral.ReferralMedium
	,Referral.ReferredByConsultantCode
	,Referral.ReferredByOrganisationCode 
	,Referral.ReferredBySpecialtyCode
	,Referral.SuspectedCancerTypeCode
	,Referral.ReferralSentDate
	,Referral.PatientDeceased
	,Referral.GeneralComment
	,Referral.ReferralComment
from
	Lorenzo.dbo.ExtractRF Referral

inner join Lorenzo.dbo.ExtractDatasetEncounter
on	ExtractDatasetEncounter.SourceUniqueID = Referral.SourceUniqueID
and	ExtractDatasetEncounter.DatasetCode = 'REFERRAL'


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Extract from ' + CONVERT(varchar(20), @from, 130) + ', '  + 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH ExtractLorenzoRF', @Stats, @StartTime

print @Stats






