﻿
/*
--Author: K Oakden
--Date created: 05/09/2012
--ExtractMISYSAntenatalSQL SP generates the SQL required by the MISYS SSIS package to pull the antenatal dataset.
--Changes to the extract SQL WHERE clause can be made here, not in the SSIS package, but any change required to the columns returned 
--will need SSIS redevelopment, and alteration of the staging and load tables and views.
*/
CREATE procedure [dbo].[ExtractMISYSAntenatalSQL]

	--@from smalldatetime = null
	--,@query varchar(50) = 'U_ONX_Questionnair_View'

as

declare @jfrom int
declare @sql varchar(4000)



select
	@jfrom = 
		DATEDIFF(
			day ,
			'31 dec 1975', 
			'30 jun 2012'
		)

select
	AntenatalSQL =
		'SELECT
			SYSTEM.U_ONX_Questionnair_View.order_number, 
			SYSTEM.U_ONX_Questionnair_View.CG_MAS_KV, 
			SYSTEM.U_ONX_Questionnair_View.LMP_CHECK, 
			SYSTEM.U_ONX_Questionnair_View.EDD, 
			SYSTEM.U_ONX_Questionnair_View.Comment, 
			SYSTEM.U_ONX_Questionnair_View.SCREENING_TIME, 
			SYSTEM.U_ONX_Questionnair_View.CONTRAST, 
			SYSTEM.U_ONX_Questionnair_View.COMPLETE, 
			SYSTEM.U_ONX_Questionnair_View.TOO_EARLY, 
			SYSTEM.U_ONX_Questionnair_View.TOO_LATE, 
			SYSTEM.U_ONX_Questionnair_View.VIABLE, 
			SYSTEM.U_ONX_Questionnair_View.POSSIBLE, 
			SYSTEM.U_ONX_Questionnair_View.AUDITABLE_ANOMOLY,
			SYSTEM.U_ONX_Questionnair_View.exam_code_no_mh
		FROM  SYSTEM.U_ONX_Questionnair_View 
		WHERE  (U_ONX_Questionnair_View.exam_code_no_mh=''UO2T'' 
			OR U_ONX_Questionnair_View.exam_code_no_mh=''UODA'' 
            OR U_ONX_Questionnair_View.exam_code_no_mh=''UODAT'' 
            OR U_ONX_Questionnair_View.exam_code_no_mh=''UODNU'')
		AND  EDD IS NOT NULL
		AND  perf_dj > ' + CONVERT(varchar, @jfrom)

