﻿CREATE procedure [dbo].[ExtractMISYSCancellationSQL]

	@from smalldatetime = null


as

declare @jfrom int
declare @sql varchar(4000)
declare @days int

--KO added 07/02/2015 to standardise refresh period across all ExtractMISYSSQL queries
select @days = CAST(NumericValue as int) from dbo.Parameter where Parameter =  'MISYSREFRESHPERIODDAYS'


select
	@jfrom = 
		DATEDIFF(
			day
			,'31 dec 1975'
			,coalesce(
				@from
				,dateadd(
					day
					,-@days
					,getdate()
				)
			)
		)



select
	CancelledOrderSQL =
		'
		SELECT     
		SYSTEM.order_modification_main.link_patient, 
		SYSTEM.order_modification_main.order_canceled, 
		SYSTEM.order_modification_main.order_modification_main, 
		SYSTEM.order_modification_main.order_number, 
		SYSTEM.order_modification_main.patient_hosp_num, 
		SYSTEM.order_modification_main.patient_number,                       
		SYSTEM.order_modification_time.modification_dj, 
		SYSTEM.order_modification_time.modification_tm
		FROM SYSTEM.order_modification_main, SYSTEM.order_modification_time
		WHERE 
		SYSTEM.order_modification_main.order_modification_main = 
		SYSTEM.order_modification_time.order_modification_main 
		AND (SYSTEM.order_modification_time.modification_dj >
				' +
				CONVERT(
					varchar
					,@jfrom
				)
				+ ')'




