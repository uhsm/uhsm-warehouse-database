﻿CREATE procedure [dbo].[ExtractMISYSDeletedScheduleSQL]

as

declare @sql varchar(4000)
, @LastDeletedDate datetime

Select @LastDeletedDate = MAX(deletiondateodbc) from MISYS.DeletedScheduleEncounter



select
	DeletedScheduleSQL =
		'Select * 
		from System.STDR_DLS
		Where deletion_date_odbc > 
				"' +
				CONVERT(
					varchar(10)
--					,GETDATE() 
					,@LastDeletedDate
					,120
				) + '"'
				




