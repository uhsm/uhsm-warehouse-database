﻿CREATE procedure [dbo].[ExtractMISYSOrderHistoryQuestionnaireSQL]

	@from smalldatetime = null


as

declare @jfrom int
declare @sql varchar(4000)
declare @days int

--KO added 07/02/2015 to standardise refresh period across all ExtractMISYSSQL queries
select @days = CAST(NumericValue as int) from dbo.Parameter where Parameter =  'MISYSREFRESHPERIODDAYS'

select
	@jfrom = 
		DATEDIFF(
			day
			,'31 dec 1975'
			,coalesce(
				@from
				,dateadd(
					day
					,-@days
					,getdate()
				)
			)
		)



select
	OrderHistoryQuestionnaireSQL =
		'Select * FROM SYSTEM.Order_History_quest WHERE SYSTEM.Order_history_quest.dj >' +
				CONVERT(
					varchar
					,@jfrom
				)




