﻿


CREATE procedure [dbo].[ExtractMISYSQuestionnaireSQL]

	@from smalldatetime = null


as

declare @jfrom int
declare @sql varchar(4000)
declare @days int

--KO added 07/02/2015 to standardise refresh period across all ExtractMISYSSQL queries
select @days = CAST(NumericValue as int) from dbo.Parameter where Parameter =  'MISYSREFRESHPERIODDAYS'


select
	@jfrom = 
		DATEDIFF(
			day
			,'31 dec 1975'
			,coalesce(
				@from
				,dateadd(
					day
					,-@days
					,getdate()
				)
			)
		)



select
	QuestionnaireSQL =
		'SELECT
		SYSTEM.onx_questionnaire_data.order_number, 
		SYSTEM.onx_questionnaire_data.exam_var50, 
		SYSTEM.onx_questionnaire_data.exam_var51,
		SYSTEM.onx_questionnaire_data.exam_var52, 
		SYSTEM.onx_questionnaire_data.exam_var53, 
		SYSTEM.onx_questionnaire_data.exam_var54, 
		SYSTEM.onx_questionnaire_data.exam_var55
		FROM SYSTEM.onx_questionnaire_data, SYSTEM.U_fastrak_Sched_View
		WHERE SYSTEM.onx_questionnaire_data.link_onx_questionnaire_data = SYSTEM.U_fastrak_Sched_View.link_onx_questionnaire_data
		AND (SYSTEM.U_fastrak_Sched_View.perf_dj >

				' +
				CONVERT(
					varchar
					,@jfrom
				)
				+ ')'




