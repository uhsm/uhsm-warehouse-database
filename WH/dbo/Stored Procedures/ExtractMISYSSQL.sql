﻿CREATE procedure [dbo].[ExtractMISYSSQL]

	@from smalldatetime = null
	,@query varchar(50) = 'U_fastrak_Sched_View'

as

declare @jfrom int
declare @sql varchar(4000)
declare @days int

--KO added 07/02/2015 to standardise refresh period across all ExtractMISYSSQL queries
select @days = CAST(NumericValue as int) from dbo.Parameter where Parameter =  'MISYSREFRESHPERIODDAYS'

select
	@jfrom = 
		DATEDIFF(
			day
			,'31 dec 1975'
			,coalesce(
				@from
				,dateadd(
					day
					,-@days
					,getdate()
				)
			)
		)

select
	MISYSSQL =
		case

		when @query = 'dict_group_physician'
		then
			'
			select
				 dict_group_physician
				,trim(group_phys)
				,group_phys_hosp_num
				,group_phys_name
				,group_phys_with_mh
				,link_phys_code_wmh
				,phys_code_hosp_num
				,phys_code_with_mh
				,phys_code
			from
				SYSTEM.dict_group_physician
			'

		when @query = 'U_FASTTRACT1_View'
		then
			'
			SELECT
				 creation_date_ODBC
				,episode_number
				,ordering_location
				,patient_number
				,Patient_Gender
				,perf_date_ODBC
				,priority
				,doc_status
				,dictation_date_ODBC
				,final_date_ODBC
				,Speciality
				,patient_dob_ODBC
				,patient_name
				,patient_name_first
				,patient_name_last
				,patient_sex
				,soc_sec_num
				,xdata1
				,xdata2
				,xdata3
				,POST_Code
				,order_number
				,ordering_phys
				,link_onx_questionnaire_data
				,perf_loc
				,series_number
				,tech_code
				,address_line_1
				,address_line_2
				,''''
				,attending_physician_1
				,attending_physician_2
				,patient_type
				,case_type
				,exam_code
				,exam_description
				,group_code
				,trim(radiologist_code)
				,primary_phys
				,primary_phys_name
				,Date_of_death
				,PRACTICE_CODE
				,NHS_N0_Verified
				,GP_PRACTICE
			FROM SYSTEM.U_FASTTRACT1_View 
			'

		else

			'
			SELECT
				 order_number
				,perf_date_ODBC
				,creation_date_ODBC
				,episode_number
				,perf_time
				,perf_time_ODBC
				,patient_name
				,department
				,exam_description
				,group_code
				,doc_status
				,film_ent_status
				,pat_trk_status
				,close_date_ODBC
				,open_date_ODBC
				,perf_dj
				,order_date_ODBC
				,order_time_ODBC
				,ordering_location_no_mh
				,patient_age
				,patient_type
				,patient_dob_ODBC
				,patient_sex
				,order_exam_code
				,perf_date
				,case_type
				,order_status
				,AttendingPhys1
				,attending_physician_2
				,case_status
				,case_status_date_ODBC
				,dictation_status
				,order_scheduled
				,ordering_physician
				,priority
				,address_line_1
				,address_line_2
				,address_line_3
				,attending_physcian_1
				,loc_code
				,patient_name_first
				,patient_name_last
				,soc_sec_num
				,xdata1
				,xdata2
				,xdata3
				,POST_Code
				,Date_of_death
				,GP_Practice
				,GP_Practice_Code
				,xgen3
				,xgen4
				,NHS_N0_Verified
				,dept_code
				,exam_code_no_mh
				,his_order_number
				,exam_var4
				,aetitle_loc
				,primary_phys
				,primary_phys_name
				,Specialty
				,SUSPENSION_DAYS
				,REQUEST_RECD_DATE
				,VETTED_DATE
				,DELAYED_BY_PT
				,FAILED_PT_CONTACT
				,PLANNED_REQUEST
				,REQUEST_DATE
				,film_data_flag
				,ordering_physician_no_mh
				,patient_number_no_mh
				,Patient_Gender
				,patient_number
				,attending_physcian_2
				,sch_status
				,ordering_location
				,aHISTCAN_cancel_date_ODBC
				,dictation_date_ODBC
				,final_date_ODBC
				,patient_type_for_linking
				,case_type_code
				,case_type_description
				,HISTMOD_exam_code
				,HISTMOD_link_exam_code
				,HISTMOD_link_location
				,HISTMOD_link_schedule_room
				,HISTMOD_link_tech
				,HISTMOD_location
				,HISTMOD_modification_date_ODBC
				,HISTMOD_modification_time_ODBC
				,HISTMOD_schedule_date_ODBC
				,schedule_end_time
				,HISTMOD_schedule_room
				,schedule_start_time
				,HISTMOD_tech_code
				,HISTCR_create_date_ODBC
				,HISTCR_create_time_ODBC
				,HISTCR_link_exam
				,HISTCR_link_location
				,HISTCR_link_schedule_room
				,HISTCR_link_tech
				,HISTCR_location
				,HISTCR_schedule_date_ODBC
				,HISTCR_schedule_room
				,HISTCR_tech_code
				,HISTCAN_cancel_date_ODBC
				,HISTCAN_cancel_time_ODBC
				,HISTCAN_link_tech
				,HISTCAN_location
				,HISTCAN_tech_code
				,ordering_phys
				,link_onx_questionnaire_data
				,perf_loc
				,radiologist_code
				,series_number
				,film_tech_code 
				,upper(perf_time_ODBC) 
				,upper(order_time_ODBC) 
				,upper(HISTMOD_modification_time_ODBC)
				,upper(schedule_end_time)
				,upper(schedule_start_time)
				,upper(HISTCR_create_time_ODBC)
				,upper(HISTCAN_cancel_time_ODBC) 
				,filed_date_odbc
				,filed_time
			FROM  SYSTEM.U_fastrak_Sched_View  WHERE   perf_dj > 
				' +
				CONVERT(
					varchar
					,@jfrom
				)
		end

				--,filed_date_odbc
				--,filed_time