﻿
/*
--Author: K Oakden
--Date created: 10/03/2014
--Copy of ExtractLPIMissingPatient to point to SQL Server LPI on TIE-SQL-CLUSTER
--Called by dbo.LoadMissingPatient
*/
CREATE procedure [dbo].[ExtractMSLPIMissingPatient]
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

select @RowsInserted = 0

--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TImportLPIMissingPatient]') AND type in (N'U'))
--DROP TABLE [dbo].[TImportMSLPIMissingPatient]
--GO
truncate table dbo.TImportMSLPIMissingPatient
insert into TImportMSLPIMissingPatient
           ([SourcePatientNo]
           ,[SourceUniqueID]
           ,[PatientClass]
		   ,[EncounterLocation]
		   ,[EncounterTime]
           ,[LocalPatientID]
           ,[Forename]
           ,[Surname]
           ,[NHSNumber]
           ,[NNNStatusCode]
           ,[PostcodeUnformatted]
           ,[Sex]
           ,[DOB]
           ,[GPCode]
           ,[PracticeCode]
           --,[CensusDate]
           )
select 
	mp.SourcePatientNo
	,mp.SourceUniqueID
	,mp.PatientClass
	,mp.EncounterLocation
	,mp.EncounterTime
	,LocalPatientID = lpivisit.FACIL_ID
	,Forename = lpidata.GIVEN_NAME
	,Surname = lpidata.SURNAME
	,NHSNumber = lpidata.NHS_NUMBER
	,NNNStatusCode = lpidata.IDENTITY_RELIABILITY_CODE
	,PostcodeUnformatted = lpidata.POSTCODE
	,Sex = lpidata.SEX
	,DOB = lpidata.DOB
	,GPCode = lpidata.GP_CODE
	,PracticeCode = lpidata.PRACTICE_CODE
	--,CensusDate = DATEADD(dd, 0, DATEDIFF(dd, 0, @StartTime)) 
--into dbo.TImportMSLPIMissingPatient
from dbo.vwMissingPatients mp
left join [TIE-SQL-CLUSTER\TIESQL].TIEUSERPROD.dbo.LPI_PATIENT_VISIT lpivisit
on mp.SourceUniqueID = lpivisit.VISIT_NUMBER
and mp.PatientClass collate database_default = lpivisit.PATIENT_CLASS collate database_default
left join [TIE-SQL-CLUSTER\TIESQL].TIEUSERPROD.dbo.LPI_PATIENT_DATA lpidata
on lpivisit.FACIL_ID = lpidata.FACIL_ID

SELECT @RowsInserted = @@Rowcount

--WaitingList
insert into TImportMSLPIMissingPatient
           ([SourcePatientNo]
           ,[SourceUniqueID]
           ,[PatientClass]
		   ,[EncounterLocation]
		   ,[EncounterTime]
           ,[LocalPatientID]
           ,[Forename]
           ,[Surname]
           ,[NHSNumber]
           ,[NNNStatusCode]
           ,[PostcodeUnformatted]
           ,[Sex]
           ,[DOB]
           ,[GPCode]
           ,[PracticeCode]
           )
select 
	mp.PATNT_REFNO
	,mp.WLIST_REFNO
	,'W'
	,'WL'
	,mp.WLIST_DTTM
	,LocalPatientID = lpiwl.FACIL_ID
	,Forename = lpidata.GIVEN_NAME
	,Surname = lpidata.SURNAME
	,NHSNumber = lpidata.NHS_NUMBER
	,NNNStatusCode = lpidata.IDENTITY_RELIABILITY_CODE
	,PostcodeUnformatted = lpidata.POSTCODE
	,Sex = lpidata.SEX
	,DOB = lpidata.DOB
	,GPCode = lpidata.GP_CODE
	,PracticeCode = lpidata.PRACTICE_CODE
from 
	(select distinct PATNT_REFNO, WLIST_REFNO, WLIST_DTTM
	from Lorenzo.dbo.WaitingList
	where PATNT_REFNO not in
		(select patnt_refno from Lorenzo.dbo.Patient)
	)mp
left join [TIE-SQL-CLUSTER\TIESQL].TIEUSERPROD.dbo.LPI_PATIENT_WAITING_LIST lpiwl
on mp.WLIST_REFNO = lpiwl.VISIT_NUMBER
left join [TIE-SQL-CLUSTER\TIESQL].TIEUSERPROD.dbo.LPI_PATIENT_DATA lpidata
on lpiwl.FACIL_ID = lpidata.FACIL_ID

SELECT @RowsInserted = @RowsInserted + @@Rowcount

--Referrals
insert into TImportMSLPIMissingPatient
           ([SourcePatientNo]
           ,[SourceUniqueID]
           ,[PatientClass]
		   ,[EncounterLocation]
		   ,[EncounterTime]
           ,[LocalPatientID]
           ,[Forename]
           ,[Surname]
           ,[NHSNumber]
           ,[NNNStatusCode]
           ,[PostcodeUnformatted]
           ,[Sex]
           ,[DOB]
           ,[GPCode]
           ,[PracticeCode]
           )
select 
	mp.SourcePatientNo
	,mp.SourceUniqueID
	,mp.PatientClass
	,mp.EncounterLocation
	,mp.EncounterTime
	,LocalPatientID = lpireferral.FACIL_ID
	,Forename = lpidata.GIVEN_NAME
	,Surname = lpidata.SURNAME
	,NHSNumber = lpidata.NHS_NUMBER
	,NNNStatusCode = lpidata.IDENTITY_RELIABILITY_CODE
	,PostcodeUnformatted = lpidata.POSTCODE
	,Sex = lpidata.SEX
	,DOB = lpidata.DOB
	,GPCode = lpidata.GP_CODE
	,PracticeCode = lpidata.PRACTICE_CODE
from dbo.vwMissingPatients mp
left join [TIE-SQL-CLUSTER\TIESQL].TIEUSERPROD.dbo.LPI_PATIENT_REFERRAL lpireferral
on mp.SourceUniqueID = lpireferral.REFERRAL_ID
and mp.PatientClass = 'R'
left join [TIE-SQL-CLUSTER\TIESQL].TIEUSERPROD.dbo.LPI_PATIENT_DATA lpidata
on lpireferral.FACIL_ID = lpidata.FACIL_ID

SELECT @RowsInserted = @RowsInserted + @@Rowcount
    
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH ExtractMSLPIMissingPatient', @Stats, @StartTime

--print @Stats



