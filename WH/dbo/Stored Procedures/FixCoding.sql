﻿CREATE procedure [dbo].[FixCoding] 
	@type varchar(5) --'DIAGN' or 'PROCE'
as

/**
KO. 06/02/214. Run multiple times
**/
--drop table PAS.CodingError
--select 
--	sorce_refno
--	,SORT_ORDER
--	,CODE
--	--,DESCRIPTION
--	--,COMMENTS
--	,DPTYP_CODE
--	,CREATE_DTTM
--	,MODIF_DTTM
--	,ARCHV_FLAG
--	,DGPRO_REFNO
--	,ODPCD_REFNO
--	,SORCE_CODE
--	,Created 
--into PAS.CodingError
--from PAS.ClinicalCodingBase Coding
--where Coding.ARCHV_FLAG = 'N'
--and Coding.SORCE_CODE in ('PRCAE', 'SCHDL', 'WLIST')
--and exists 
--	(
--	Select 1 
--	From PAS.ClinicalCodingBase PrevCoding
--	Where PrevCoding.ARCHV_FLAG = 'N'
--	and Coding.SORCE_REFNO = PrevCoding.SORCE_REFNO
--	and Coding.SORCE_CODE = PrevCoding.SORCE_CODE
--	and Coding.SORT_ORDER = PrevCoding.SORT_ORDER
--	and Coding.DPTYP_CODE = PrevCoding.DPTYP_CODE
--	and Coding.ODPCD_REFNO <> PrevCoding.ODPCD_REFNO
--	--and Coding.DGPRO_REFNO < PrevCoding.DGPRO_REFNO
--	)
	


declare @suid int
declare @order int
declare @txt varchar(max)
declare @duid int

declare DiagnosisDupes cursor for 
	select distinct sorce_refno, sort_order
	from PAS.CodingError
	where dptyp_code = @type
	order by sorce_refno, sort_order

open DiagnosisDupes

fetch next from DiagnosisDupes into @suid, @order
while @@FETCH_STATUS = 0 
	begin
	
		select @duid = Coding.DGPRO_REFNO
		from PAS.ClinicalCodingBase Coding
		where SORCE_REFNO = @suid
		and SORT_ORDER = @order
		and DPTYP_CODE = @type
		and exists
			(
			Select 1 
			from PAS.ClinicalCodingBase PrevCoding
			where PrevCoding.SORCE_REFNO = @suid
			and PrevCoding.SORT_ORDER = @order
			and PrevCoding.DPTYP_CODE = @type
			and Coding.DGPRO_REFNO > PrevCoding.DGPRO_REFNO
			)
		select @txt = 'Update PAS.ClinicalCodingBase set SORT_ORDER = '
		 + cast((@order + 1) as varchar)
		 + ' where DGPRO_REFNO = ' + cast(@duid as varchar)
		 
		print @txt
		fetch next from DiagnosisDupes into @suid, @order
	end

close DiagnosisDupes
deallocate DiagnosisDupes



