﻿
/*
--Author: K Oakden
--Date created: 02/05/2014
--To get size of new objects
*/
CREATE PROCEDURE [dbo].[GetAllObjectSize_] 
	@from datetime

AS
	SET NOCOUNT ON;
	
--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TableDetails]') AND type in (N'U'))
--DROP TABLE [dbo].[TableDetails]

--create table dbo.TableDetails(
--	DatabaseName varchar(30) NOT NULL,
--	SchemaName varchar(30) NULL,
--	ObjectType char(2) NULL,
--	ObjectID int NULL,
--	ObjectName varchar(80) NULL,
--	CreateDate datetime NULL,
--	RowCounts int NULL,
--	TotalSpaceKB int NULL,
--	DropCmd varchar(400) NULL
--) 
	CREATE TABLE #databases(
	    database_id int, 
	    database_name varchar(30)
	);

	-- ignore systems / report databases
	INSERT INTO #databases(database_id, database_name)
	SELECT database_id, name FROM sys.databases
	WHERE database_id > 6
 
	--select * from #databases
	DECLARE 
	    @dbid int, 
		@dbname varchar(30),
	    @sql varchar(max);
	    
	truncate table dbo.TableDetails
	
	WHILE (SELECT COUNT(*) FROM #databases) > 0 BEGIN
		SELECT TOP 1 @dbid = database_id, 
					 @dbname = database_name 
		FROM #databases;

		select @sql = 
			'insert into dbo.TableDetails
			select 
				DatabaseName = ''||db||''
				,SchemaName = s.name
				,ObjectType = o.type
				,ObjectID = o.object_id
				,ObjectName = o.name
				,CreateDate = o.create_date
				,ModifiedDate = o.modify_date
				,size.RowCounts
				,size.TotalSpaceKB
				,DropCmd = ''drop '' 
					+ case o.type  
						when ''U'' then ''table''
						when ''P'' then ''procedure''
						when ''V'' then ''view''
						else ''function''
					end
					+ '' '' + s.name + ''.['' + o.name + '']''
			FROM ||db||.sys.objects o
			join ||db||.sys.schemas s
			on o.schema_id = s.schema_id
			left join (
				SELECT 
					t.object_id AS ObjectID,
					p.rows AS RowCounts,
					SUM(a.total_pages) * 8 AS TotalSpaceKB
				FROM 
					||db||.sys.tables t
				INNER JOIN      
					||db||.sys.indexes i ON t.OBJECT_ID = i.object_id
				INNER JOIN 
					||db||.sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
				INNER JOIN 
					||db||.sys.allocation_units a ON p.partition_id = a.container_id
				LEFT OUTER JOIN 
					||db||.sys.schemas s ON t.schema_id = s.schema_id
				WHERE 
					t.is_ms_shipped = 0
				GROUP BY 
					t.Name, s.Name, p.Rows, t.object_id
				)size
			   on size.ObjectID = o.object_id
			WHERE o.create_date > ''||date||''
			and o.type in (''U'',''P'', ''V'', ''FN'', ''TF'')'

		select @sql = replace(@sql, '||date||', @from)
		select @sql = replace(@sql, '||db||', @dbname)
		--print @sql
		exec(@sql);

		delete from #databases where database_id = @dbid;
	end;


	
	;

select * from dbo.TableDetails