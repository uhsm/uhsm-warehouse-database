﻿

CREATE procedure [dbo].[GetColumnDependencies_] 
as

/*
--Author: K Oakden
--Date created: 12/05/2014
--Identify code containing the search string from Crossdatabase Dependencies.
--Note that dynamic SQL will not be included.
--Assumes that exec SSMS.GetCrossdatabaseDependencies null, null, null has been run first

--KO updated 29/01/2015 to allow refernced table parameter and option to search from a table
*/
declare
	@SearchString varchar(400) --Column being searched for
	,@TableName varchar(400) --The table containing the column / search string
	,@SchemaName varchar(400)--The schema containing the column / search string
	,@DatabaseName varchar(400)--The DB containing the column / search string
	
	select @TableName = 'Patient'
	select @SchemaName = 'dbo'
	select @DatabaseName = 'Lorenzo'
	--select @SearchString = 'Directorate'

	truncate table dbo.CrossdatabaseColumnDependencies


--1 Get Crossdatabase dependencies for specified table / object
-----------------------------------------------------------------------
	exec dbo.GetCrossdatabaseDependencies null, null, null

	if Object_id('tempdb..#ccd', 'U') is not null
		drop table #ccd
		
	CREATE TABLE #ccd(
		[databaseId] [smallint] NULL,
		[databaseName] [varchar](50) NULL,
		[objectId] [int] NULL,
		[schemaName] [varchar](50) NULL,
		[objectName] [varchar](100) NULL,
		[objectType] [varchar](50) NULL,
		[objectDef] [varchar](max) NULL
	) ON [PRIMARY]

	insert into #ccd (
		databaseId, 
		databaseName, 
		objectId,
		schemaName,
		objectName,
		objectType
		)
	select distinct 
		database_id = DB_ID(referencing_database)
		,database_name = referencing_database
		,ob_id = referencing_object_id
		,referencing_schema
		,referencing_object_name
		,referencing_object_type
	from dbo.CrossdatabaseDependencies
	where referenced_object_name = @TableName
	and referenced_schema = @SchemaName
	and referenced_database = @DatabaseName
	--and referencing_object_type = 'SQL_STORED_PROCEDURE'
	--and upper(OBJECT_DEFINITION (referencing_object_id))  LIKE '%AttendanceOutcomeCode%' -- column name

--2 Cycle through all databases to get the object definitions of the objects identified in step 1
----------------------------------------------------------------------------------------------------
	if Object_id('tempdb..#databases', 'U') is not null
		drop table #databases
		
	CREATE TABLE #databases(
		database_id int, 
		database_name sysname
	);

	-- ignore systems databases
	INSERT INTO #databases(database_id, database_name)
	SELECT database_id, name FROM sys.databases
	WHERE database_id > 4
	and state = 0


	declare 
		@database_id int, 
		@database_name sysname, 
		@sql varchar(max),
		@linebreak AS varchar(2)

	set @linebreak = char(13) + char(10)
	    

	WHILE (SELECT COUNT(*) FROM #databases) > 0 BEGIN
		SELECT TOP 1 @database_id = database_id, 
					 @database_name = database_name 
		FROM #databases;

			select @sql = 
				'use ' + @database_name + @linebreak
				--+ 'go ' + @linebreak 
				+ 'update #ccd
			set objectDef = 
				upper(OBJECT_DEFINITION (objectId))
			from #ccd
			where databaseName = '''+ @database_name + ''''


			print @sql
			EXEC(@sql);
	    
			DELETE FROM #databases WHERE database_id = @database_id;
		END;


/**
--3 populate table where any object definition contains the search string
-------------------------------------------------------------------------------
	--drop table dbo.CrossdatabaseColumnDependencies
	insert into dbo.CrossdatabaseColumnDependencies
	select distinct ColDatabaseName = @DatabaseName, ColSchemaName = @SchemaName, ColTableName = @TableName, ColSearchString = @SearchString, col.*
	--into dbo.CrossdatabaseColumnDependencies
	from #ccd col
	where col.objectDef like '%' + @SearchString + '%'
**/

--/**
--4 Or optionally cycle through a table of search strings
----------------------------------------------------------------
	declare @name varchar(50) -- column name 
	declare colNames cursor
	for
		select COLUMN_NAME from [uhsm-dw-1].Lorenzo.dbo.PatientExtractObsolete
	open colNames

	fetch next from colNames into @name

	while @@FETCH_STATUS = 0
	begin
		print @name

		insert into dbo.CrossdatabaseColumnDependencies
		select distinct 
			ColDatabaseName = @DatabaseName, 
			ColSchemaName = @SchemaName, 
			ColTableName = @TableName, 
			ColSearchString = @name,
			col.*
		from #ccd col
		where col.objectDef like '%' + @name + '%'

		fetch next from colNames into @name

	end

	close colNames
	deallocate colNames
	-----------------------------------------------------------------
--**/
	--drop table Lorenzo.dbo.PatDependencies
	select * 
	--into Lorenzo.dbo.PatDependencies
	from dbo.CrossdatabaseColumnDependencies
	order by databaseName, schemaname

