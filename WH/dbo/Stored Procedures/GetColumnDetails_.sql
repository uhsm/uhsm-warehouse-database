﻿
create procedure dbo.[GetColumnDetails_] as

--Author: K Oakden
--Date created: 10/09/2014
--To list all column names


if Object_id('tempdb..#databases', 'U') is not null
	drop table #databases
	
--SET NOCOUNT ON;
create table #databases(
	database_id int, 
	database_name sysname
);

-- ignore systems databases
insert into #databases(database_id, database_name)
select database_id, name 
from sys.databases
where database_id > 4
and [state] = 0 --online

declare @database_id int
declare @database_name sysname
declare @sql varchar(max)

truncate table dbo.ColumnDetails

while (
	select COUNT(*)
	from #databases
	) > 0
begin
	select top 1 @database_id = database_id
		,@database_name = database_name
	from #databases;

	set @sql = 
	'insert into dbo.ColumnDetails
	select 
		DatabaseName = TABLE_CATALOG
		,SchemaName = TABLE_SCHEMA
		,ObjectName = TABLE_NAME
		,ColumnName = COLUMN_NAME
		,ColumnPosition = ORDINAL_POSITION
		,DataType = DATA_TYPE
		,CollationType = COLLATION_NAME
	from ' + @database_name + '.information_schema.columns'


	print @sql
	exec (@sql);

	delete from #databases
	where database_id = @database_id;
end;



