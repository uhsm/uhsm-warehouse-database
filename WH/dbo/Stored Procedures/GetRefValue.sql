﻿CREATE procedure [dbo].[GetRefValue]
	@ReferenceValueCode int
as

select
	*
from
	PAS.ReferenceValue

left join PAS.ReferenceValueIdentifier
on	ReferenceValueIdentifier.ReferenceValueCode = ReferenceValue.ReferenceValueCode
and	ReferenceValueIdentifier.ReferenceValueIdentifierTypeCode = 'NHS'
and	ReferenceValueIdentifier.ArchiveFlag = 'N'

where
	exists
	(
	select
		1
	from
		PAS.ReferenceValue a
	where
		a.ReferenceDomainCode = ReferenceValue.ReferenceDomainCode
	and	a.ReferenceValueCode = @ReferenceValueCode
	)

order by
	1
