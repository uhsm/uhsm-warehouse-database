﻿create procedure [dbo].[GetSiteName] as

select
	SiteName = TextValue
from
	dbo.Parameter
where
	Parameter = 'SITENAME'
