﻿
/*
--Author: K Oakden
--Date created: 20/05/2014
--To get size of tables
*/
CREATE PROCEDURE [dbo].[GetTableSizeFOrAudit] as

	--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TableAudit]') AND type in (N'U'))
	--DROP TABLE [dbo].[TableAudit]

	--CREATE TABLE [dbo].[TableAudit](
	--	[ObjectID] [int] NOT NULL,
	--	[DatabaseName] [varchar](30) NOT NULL,
	--	[SchemaName] [varchar](30) NOT NULL,
	--	[ObjectName] [varchar](80) NOT NULL,
	--	[CreateDate] [datetime] NOT NULL,
	--	[RowCounts] [int] NULL,
	--	[TotalSpaceKB] [int] NULL,
	--	[CensusDate] [datetime] NOT NULL
	--) 

	CREATE TABLE #databases(
	    database_id int, 
	    database_name varchar(30)
	);

	-- ignore systems / report databases
	INSERT INTO #databases(database_id, database_name)
	SELECT database_id,  name FROM sys.databases
	WHERE database_id > 6
	and [state] = 0 --online
 
	--select * from #databases
	DECLARE 
	    @dbid int, 
		@dbname varchar(30),
	    @sql varchar(max);
	    
	
	WHILE (SELECT COUNT(*) FROM #databases) > 0 BEGIN
		SELECT TOP 1 @dbid = database_id, 
					 @dbname = database_name 
		FROM #databases;

		select @sql = 
			'insert into dbo.TableAudit
			select 
				ObjectID = o.object_id
				,DatabaseName = ''||db||''
				,SchemaName = s.name
				,ObjectName = o.name
				,CreateDate = o.create_date
				,size.RowCounts
				,size.TotalSpaceKB
				,CensusDate = dateadd(day, 1, datediff(day, 1, GETDATE()))
			from ||db||.sys.objects o
			join ||db||.sys.schemas s
			on o.schema_id = s.schema_id
			left join (
				select 
					t.object_id AS ObjectID,
					p.rows AS RowCounts,
					SUM(a.total_pages) * 8 AS TotalSpaceKB
				from 
					||db||.sys.tables t
				inner join      
					||db||.sys.indexes i ON t.OBJECT_ID = i.object_id
				inner join 
					||db||.sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
				inner join 
					||db||.sys.allocation_units a ON p.partition_id = a.container_id
				left join 
					||db||.sys.schemas s ON t.schema_id = s.schema_id
				where 
					t.is_ms_shipped = 0
				group by 
					t.Name, s.Name, p.Rows, t.object_id
				)size
			   on size.ObjectID = o.object_id
			where o.type = ''U'''


		select @sql = replace(@sql, '||db||', @dbname)
		--print @sql
		exec(@sql);

		delete from #databases where database_id = @dbid;
	end;
	;

--select * from dbo.TableAudit