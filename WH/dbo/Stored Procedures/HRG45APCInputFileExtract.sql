﻿CREATE procedure [dbo].[HRG45APCInputFileExtract] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsReturned Int
declare @Stats varchar(255)
declare @Created datetime
declare @InputStartDate datetime

select @StartTime = getdate()

--Optional start date parameter for bulk loading
select @InputStartDate =  
	case 
		when DateValue is null then '2001-01-01 00:00:00.000'
		else DateValue
	end
from dbo.Parameter 
where Parameter = 'HRGAPCSTARTDATE'

select

	 ProviderCode = 'RM200'
	,Encounter.ProviderSpellNo

	,Encounter.EpisodeNo

	,EpisodeAge =
		floor(
			datediff(
				 day
				,Encounter.DateOfBirth
				,Encounter.EpisodeStartDate
			) / 365.25
		)

	,SexCode =
		cast(
			Sex.MappedCode
			as CHAR(1)
		)

	,PatientClassificationCode =
		case
		when
			Encounter.ManagementIntentionCode in
			(
			 2003683	--Not Applicable	8
			,2003684	--Not Known	9
			,2005563	--Not Specified	NSP
			)
		then
			case
			when datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
			then 2
			else 1
			end

		when 
			Encounter.ManagementIntentionCode in
				(
				 8850	--No Overnight Stay	2
				,8851	--Planned Sequence - No Overnight Stays	4
				,9487	--Planned Sequence - Inc. Over Night Stays	3
				)
		and
			Encounter.AdmissionMethodCode not in
			(
			 2003470	--Elective
			,8811		--Elective - Booked
			,8810		--Elective - Waiting List
			,8812		--Elective - Planned
			,13			--Not Specified
			,2003472	--OTHER
			)
		then 1

		when
			Encounter.ManagementIntentionCode in
			(
			 8851	--Planned Sequence - No Overnight Stays	4
			)
		and	datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
		then 3

		when
			Encounter.ManagementIntentionCode in
			(
			 9487	--Planned Sequence - Inc. Over Night Stays	3
			)
		and	datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
		then 4

		when datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
		then 2

		else 1
		end

	,AdmissionSourceCode =
		AdmissionSource.MappedCode

	,AdmissionMethodCode =
		AdmissionMethod.MappedCode

	,DischargeDestinationCode =
		DischargeDestination.MappedCode

	,DischargeMethodCode =
		DischargeMethod.MappedCode

	,EpisodeDuration =
		datediff(day, EpisodeStartDate, EpisodeEndDate)

	,MainSpecialtyCode = 
		LEFT(
			case
			when
				left(
					coalesce(
						 TreatmentFunctionSpecialtyMap.XrefEntityCode
						,TreatmentFunction.NationalSpecialtyCode
					)
					,3
				) = '500'
			then MainSpecialty.NationalSpecialtyCode

			else 
				coalesce(
					 TreatmentFunctionSpecialtyMap.XrefEntityCode
					,TreatmentFunction.NationalSpecialtyCode
				)
			end
			,3
		)

	,NeonatalLevelOfCareCode =
		coalesce(NeonatalLevelOfCare.MappedCode, '8')

	,TreatmentFunctionCode =
		left(
			coalesce(
				 SpecialtyTreatmentFunctionMap.XrefEntityCode
				,TreatmentFunction.NationalSpecialtyCode
			)
			,3
		)

	,Diagnosis1 =
	left(
		replace(replace(Encounter.PrimaryDiagnosisCode, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis2 =
	left(
		replace(replace(Encounter.SubsidiaryDiagnosisCode, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis3 = left(replace(replace(Encounter.SecondaryDiagnosisCode1,'.', ''), ',', ''), 5)
	,Diagnosis4 = left(replace(replace(Encounter.SecondaryDiagnosisCode2,'.', ''), ',', ''), 5)
	,Diagnosis5 = left(replace(replace(Encounter.SecondaryDiagnosisCode3,'.', ''), ',', ''), 5)
	,Diagnosis6 = left(replace(replace(Encounter.SecondaryDiagnosisCode4,'.', ''), ',', ''), 5)
	,Diagnosis7 = left(replace(replace(Encounter.SecondaryDiagnosisCode5,'.', ''), ',', ''), 5)
	,Diagnosis8 = left(replace(replace(Encounter.SecondaryDiagnosisCode6,'.', ''), ',', ''), 5)
	,Diagnosis9 = left(replace(replace(Encounter.SecondaryDiagnosisCode7,'.', ''), ',', ''), 5)
	,Diagnosis10 = left(replace(replace(Encounter.SecondaryDiagnosisCode8,'.', ''), ',', ''), 5)
	,Diagnosis11 = left(replace(replace(Encounter.SecondaryDiagnosisCode9,'.', ''), ',', ''), 5)
	,Diagnosis12 = left(replace(replace(Encounter.SecondaryDiagnosisCode10,'.', ''), ',', ''), 5)
	,Diagnosis13 = left(replace(replace(Encounter.SecondaryDiagnosisCode11,'.', ''), ',', ''), 5)
	,Diagnosis14 = left(replace(replace(Encounter.SecondaryDiagnosisCode12,'.', ''), ',', ''), 5)

	,Operation1 =
		left(
			replace(replace(Encounter.PrimaryOperationCode, '.', ''), ',', '')
			, 4
		) 

	,Operation2 = left(replace(replace(Encounter.SecondaryOperationCode1,'.', ''), ',', ''), 4)
	,Operation3 = left(replace(replace(Encounter.SecondaryOperationCode2,'.', ''), ',', ''), 4)
	,Operation4 = left(replace(replace(Encounter.SecondaryOperationCode3,'.', ''), ',', ''), 4)
	,Operation5 = left(replace(replace(Encounter.SecondaryOperationCode4,'.', ''), ',', ''), 4)
	,Operation6 = left(replace(replace(Encounter.SecondaryOperationCode5,'.', ''), ',', ''), 4)
	,Operation7 = left(replace(replace(Encounter.SecondaryOperationCode6,'.', ''), ',', ''), 4)
	,Operation8 = left(replace(replace(Encounter.SecondaryOperationCode7,'.', ''), ',', ''), 4)
	,Operation9 = left(replace(replace(Encounter.SecondaryOperationCode8,'.', ''), ',', ''), 4)
	,Operation10 = left(replace(replace(Encounter.SecondaryOperationCode9,'.', ''), ',', ''), 4)
	,Operation11 = left(replace(replace(Encounter.SecondaryOperationCode10,'.', ''), ',', ''), 4)
	,Operation12 = left(replace(replace(Encounter.SecondaryOperationCode11,'.', ''), ',', ''), 4)

	--,Diagnosis1 =
	--left(
	--	replace(replace(Encounter.PrimaryDiagnosisCode, '.', ''), ',', '')
	--	, 5
	--) 

	--,Diagnosis2 =
	--left(
	--	replace(replace(Diagnosis2.DiagnosisCode, '.', ''), ',', '')
	--	, 5
	--) 

	--,Diagnosis3 =
	--left(
	--	replace(replace(Diagnosis3.DiagnosisCode, '.', ''), ',', '')
	--	, 5
	--) 

	--,Diagnosis4 =
	--left(
	--	replace(replace(Diagnosis4.DiagnosisCode, '.', ''), ',', '')
	--	, 5
	--) 

	--,Diagnosis5 =
	--left(
	--	replace(replace(Diagnosis5.DiagnosisCode, '.', ''), ',', '')
	--	, 5
	--) 

	--,Diagnosis6 =
	--left(
	--	replace(replace(Diagnosis6.DiagnosisCode, '.', ''), ',', '')
	--	, 5
	--) 

	--,Diagnosis7 =
	--left(
	--	replace(replace(Diagnosis7.DiagnosisCode, '.', ''), ',', '')
	--	, 5
	--) 

	--,Diagnosis8 =
	--left(
	--	replace(replace(Diagnosis8.DiagnosisCode, '.', ''), ',', '')
	--	, 5
	--) 

	--,Diagnosis9 =
	--left(
	--	replace(replace(Diagnosis9.DiagnosisCode, '.', ''), ',', '')
	--	, 5
	--) 

	--,Diagnosis10 =
	--left(
	--	replace(replace(Diagnosis10.DiagnosisCode, '.', ''), ',', '')
	--	, 5
	--) 

	--,Diagnosis11 =
	--left(
	--	replace(replace(Diagnosis11.DiagnosisCode, '.', ''), ',', '')
	--	, 5
	--) 

	--,Diagnosis12 =
	--left(
	--	replace(replace(Diagnosis12.DiagnosisCode, '.', ''), ',', '')
	--	, 5
	--) 

	--,Diagnosis13 =
	--left(
	--	replace(replace(Diagnosis13.DiagnosisCode, '.', ''), ',', '')
	--	, 5
	--) 

	--,Diagnosis14 =
	--left(
	--	replace(replace(Diagnosis14.DiagnosisCode, '.', ''), ',', '')
	--	, 5
	--) 

	--,Operation1 =
	--left(
	--	replace(replace(Encounter.PrimaryOperationCode, '.', ''), ',', '')
	--	, 4
	--) 
	--,Operation2 =
	--left(
	--	replace(replace(Operation2.OperationCode, '.', ''), ',', '')
	--	, 4
	--) 
	--,Operation3 =
	--left(
	--	replace(replace(Operation3.OperationCode, '.', ''), ',', '')
	--	, 4
	--) 
	--,Operation4 =
	--left(
	--	replace(replace(Operation4.OperationCode, '.', ''), ',', '')
	--	, 4
	--) 
	--,Operation5 =
	--left(
	--	replace(replace(Operation5.OperationCode, '.', ''), ',', '')
	--	, 4
	--) 
	--,Operation6 =
	--left(
	--	replace(replace(Operation6.OperationCode, '.', ''), ',', '')
	--	, 4
	--) 
	--,Operation7 =
	--left(
	--	replace(replace(Operation7.OperationCode, '.', ''), ',', '')
	--	, 4
	--) 
	--,Operation8 =
	--left(
	--	replace(replace(Operation8.OperationCode, '.', ''), ',', '')
	--	, 4
	--) 
	--,Operation9 =
	--left(
	--	replace(replace(Operation9.OperationCode, '.', ''), ',', '')
	--	, 4
	--) 
	--,Operation10 =
	--left(
	--	replace(replace(Operation10.OperationCode, '.', ''), ',', '')
	--	, 4
	--) 
	--,Operation11 =
	--left(
	--	replace(replace(Operation11.OperationCode, '.', ''), ',', '')
	--	, 4
	--) 
	--,Operation12 =
	--left(
	--	replace(replace(Operation12.OperationCode, '.', ''), ',', '')
	--	, 4
	--) 

	,CriticalCareDays = 0

	--19-03-2013: KO changed to 0. Not used at UHSM
	,RehabilitationDays = 0
		--(
		--select
		--	SUM(
		--		DATEDIFF(
		--			DAY

		--			,case
		--			when WardStay.StartTime < Episode.EpisodeStartTime
		--			then Episode.EpisodeStartDate

		--			else cast(WardStay.StartTime as date)
		--			end

		--			,case
		--			when WardStay.EndTime < Episode.EpisodeEndTime
		--			then cast(WardStay.EndTime as date)

		--			else Episode.EpisodeEndDate
		--			end

		--		)
		--	)
		--from
		--	APC.WardStay

		--inner join APC.Encounter Episode
		--on	Episode.SourceSpellNo = WardStay.SourceSpellNo

		--where
		--	WardStay.WardCode = 150002661	--F10 DISCHARGE
		--and	WardStay.SourceSpellNo = Encounter.SourceSpellNo
		--and	Episode.EncounterRecno = Encounter.EncounterRecno
		--and	(
		--		WardStay.StartTime between Episode.EpisodeStartTime and Episode.EpisodeEndTime
		--	or	WardStay.EndTime between Episode.EpisodeStartTime and Episode.EpisodeEndTime
		--	)
		--)

	,SpecialistPalliativeCareDays = 0

--	,CriticalCareDays =
--	case
--	when Encounter.InterfaceCode = 'PAS'
--	then
--		case
--		when Encounter.StartWardTypeCode = 'CCU'
--		then datediff(day, Encounter.EpisodeStartDate, Encounter.EpisodeEndDate)
--		else 0
--		end
--	when CriticalCare.CriticalCareDays is null then 0
--	when CriticalCare.CriticalCareDays = 0 then 1 --if there is CC < 1 whole day then set to 1 (see guidance)
--	else CriticalCare.CriticalCareDays
--	end
--
--	,RehabilitationDays =
--	case
--	when
--	Encounter.InterfaceCode = 'PAS'
--	and	Encounter.StartWardTypeCode = 'SEYM'
--	then datediff(day, Encounter.EpisodeStartDate, Encounter.EpisodeEndDate)
--	else 0
--	end
--
--	,SpecialistPalliativeCareDays = 0

	,Encounter.EncounterRecno
	,Encounter.EpisodeEndDate
	,Encounter.SourceUniqueID

from
	APC.Encounter Encounter

--left join APC.Operation Operation2
--on	Operation2.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation2.SequenceNo = 2

--left join APC.Operation Operation3
--on	Operation3.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation3.SequenceNo = 3

--left join APC.Operation Operation4
--on	Operation4.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation4.SequenceNo = 4

--left join APC.Operation Operation5
--on	Operation5.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation5.SequenceNo = 5

--left join APC.Operation Operation6
--on	Operation6.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation6.SequenceNo = 6

--left join APC.Operation Operation7
--on	Operation7.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation7.SequenceNo = 7

--left join APC.Operation Operation8
--on	Operation8.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation8.SequenceNo = 8

--left join APC.Operation Operation9
--on	Operation9.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation9.SequenceNo = 9

--left join APC.Operation Operation10
--on	Operation10.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation10.SequenceNo = 10

--left join APC.Operation Operation11
--on	Operation11.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation11.SequenceNo = 11

--left join APC.Operation Operation12
--on	Operation12.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation12.SequenceNo = 12

--left join APC.Diagnosis Diagnosis2
--on	Diagnosis2.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis2.SequenceNo = 2

--left join APC.Diagnosis Diagnosis3
--on	Diagnosis3.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis3.SequenceNo = 3

--left join APC.Diagnosis Diagnosis4
--on	Diagnosis4.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis4.SequenceNo = 4

--left join APC.Diagnosis Diagnosis5
--on	Diagnosis5.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis5.SequenceNo = 5

--left join APC.Diagnosis Diagnosis6
--on	Diagnosis6.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis6.SequenceNo = 6

--left join APC.Diagnosis Diagnosis7
--on	Diagnosis7.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis7.SequenceNo = 7

--left join APC.Diagnosis Diagnosis8
--on	Diagnosis8.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis8.SequenceNo = 8

--left join APC.Diagnosis Diagnosis9
--on	Diagnosis9.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis9.SequenceNo = 9

--left join APC.Diagnosis Diagnosis10
--on	Diagnosis10.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis10.SequenceNo = 10

--left join APC.Diagnosis Diagnosis11
--on	Diagnosis11.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis11.SequenceNo = 11

--left join APC.Diagnosis Diagnosis12
--on	Diagnosis12.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis12.SequenceNo = 12

--left join APC.Diagnosis Diagnosis13
--on	Diagnosis13.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis13.SequenceNo = 13

--left join APC.Diagnosis Diagnosis14
--on	Diagnosis14.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis14.SequenceNo = 14

left join PAS.Specialty TreatmentFunction
on	TreatmentFunction.SpecialtyCode = Encounter.SpecialtyCode

left join PAS.Consultant
on	Consultant.ConsultantCode = Encounter.ConsultantCode

left join PAS.Specialty MainSpecialty
on	MainSpecialty.SpecialtyCode = Consultant.MainSpecialtyCode

left join dbo.EntityXref TreatmentFunctionSpecialtyMap
on	TreatmentFunctionSpecialtyMap.EntityTypeCode = 'TREATMENTFUNCTIONCODE'
and	TreatmentFunctionSpecialtyMap.XrefEntityTypeCode = 'SPECIALTYCODE'
and	TreatmentFunctionSpecialtyMap.EntityCode =
		coalesce(
			 left(TreatmentFunction.NationalSpecialtyCode, 3)
			,MainSpecialty.NationalSpecialtyCode
		)

left join dbo.EntityXref SpecialtyTreatmentFunctionMap
on	SpecialtyTreatmentFunctionMap.EntityTypeCode = 'SPECIALTYCODE'
and	SpecialtyTreatmentFunctionMap.XrefEntityTypeCode like 'TREATMENTFUNCTIONCODE%'
and	SpecialtyTreatmentFunctionMap.EntityCode =
		coalesce(
			 left(TreatmentFunction.NationalSpecialtyCode, 3)
			,MainSpecialty.NationalSpecialtyCode
		)

left join PAS.ReferenceValue AdmissionSource
on	AdmissionSource.ReferenceValueCode = Encounter.AdmissionSourceCode

left join PAS.ReferenceValue AdmissionMethod
on	AdmissionMethod.ReferenceValueCode = Encounter.AdmissionMethodCode

left join PAS.ReferenceValue DischargeDestination
on	DischargeDestination.ReferenceValueCode = Encounter.DischargeDestinationCode

left join PAS.ReferenceValue DischargeMethod
on	DischargeMethod.ReferenceValueCode = Encounter.DischargeMethodCode

left join PAS.ReferenceValue NeonatalLevelOfCare
on	NeonatalLevelOfCare.ReferenceValueCode = Encounter.NeonatalLevelOfCare

--left join 
--	(
--	select
--		WardStay.EncounterRecno
--		,sum(
--			datediff(day,
--				case
--				when WardStay.EpisodeStartDate > WardStay.WardStayStartDate
--				then WardStay.EpisodeStartDate
--				else WardStay.WardStayStartDate
--				end
--				,case
--				when WardStay.EpisodeEndDate > WardStay.WardStayEndDate
--				then WardStay.WardStayEndDate
--				else WardStay.EpisodeEndDate
--				end
--			)
--		) CriticalCareDays
--	from
--		APC.WardStay WardStay
--
--	inner join APC.CriticalCareWard
--	on	CriticalCareWard.WardCode = WardStay.WardCode
--
--	group by
--		WardStay.EncounterRecno
--
--	) CriticalCare
--on	CriticalCare.EncounterRecno = Encounter.EncounterRecno


left join PAS.ReferenceValue Sex
on    Sex.ReferenceValueCode = Encounter.SexCode

where
	Encounter.DischargeDate is not null
--Use StartDate if present
and Encounter.DischargeDate > @InputStartDate
--	Encounter.DischargeDate > COALESCE(@InputStartDate, Encounter.DischargeDate)

--re-group U coded data

	--and exists
	--(
	--select
	--	1
	--from
	--	APC.HRG45Encounter

	--inner join APC.Encounter Spell
	--on	Spell.EncounterRecno = HRG45Encounter.EncounterRecno

	--where
	--	HRG45Encounter.HRGCode = 'UZ01Z'
	--and	Spell.SourceSpellNo = Encounter.SourceSpellNo
	--)

--net change group
and	exists
	(
	select
		1
	from
		APC.Encounter Spell
	where
		coalesce(
			 Encounter.Updated
			,Encounter.Created
		)
		 > 
		(
		select
			DateValue
		from
			dbo.Parameter
		where
			Parameter = 'HRGAPCDATE'
		)
	and	Spell.SourceSpellNo = Encounter.SourceSpellNo
	)

--for testing individual spells
--and Encounter.ProviderSpellNo = 150413270

select @RowsReturned = @@rowcount

select @Elapsed = DATEDIFF(second, @StartTime, getdate())

select @Stats = 
	'InputStartDate = ' + CONVERT(varchar(11), @InputStartDate)
	+ ', rows returned: ' + CONVERT(varchar(10), @RowsReturned) 
	+ ', time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' secs'

exec WriteAuditLogEvent 'HRG - WH HRG45APCInputFileExtract', @Stats, @StartTime

--print @Stats