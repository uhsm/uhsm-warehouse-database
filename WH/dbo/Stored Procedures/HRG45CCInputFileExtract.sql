﻿CREATE procedure [dbo].[HRG45CCInputFileExtract] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsReturned Int
declare @Stats varchar(255)
declare @Created datetime

select @StartTime = getdate()

select
	 CriticalCareUnitFunctionCode
	,BasicCardiovascularSupportDays
	,AdvancedCardiovascularSupportDays
	,BasicRespiratorySupportDays
	,AdvancedRespiratorySupportDays
	,RenalSupportDays
	,NeurologicalSupportDays
	,DermatologicalSupportDays
	,LiverSupportDays
	,Level2SupportDays
	,Level3SupportDays
	,StartDate
	,EndDate
	,CriticalCareLocalIdentifier
	,SourceSpellNo
	,DischargeDate
from
	CC.AICU

select @RowsReturned = @@rowcount

select @Elapsed = DATEDIFF(second, @StartTime, getdate())

select @Stats = 
	'Rows returned: ' + CONVERT(varchar(10), @RowsReturned) 
	+ ', time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' secs'

exec WriteAuditLogEvent 'HRG - WH HRG45CCInputFileExtract', @Stats, @StartTime

--print @Stats