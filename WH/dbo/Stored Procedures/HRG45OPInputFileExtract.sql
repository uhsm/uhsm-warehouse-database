﻿CREATE procedure [dbo].[HRG45OPInputFileExtract] AS

declare @StartTime datetime
declare @Elapsed int
declare @RowsReturned Int
declare @Stats varchar(255)
declare @Created datetime
declare @InputStartDate datetime

select @StartTime = getdate()
--select @InputStartDate =  '2012-03-01 00:00:00.000'
select @InputStartDate =  '2013-03-31 00:00:00.000'

select
	 AgeOnAppointment =
		floor(
			datediff(
				 day
				,Encounter.DateOfBirth
				,Encounter.AppointmentDate
			) / 365.25
		)

	,SexCode =
		cast(
			Sex.MappedCode
			as CHAR(1)
		)


	,MainSpecialtyCode = 
		LEFT(
			case
			when
				left(
					coalesce(
						 TreatmentFunctionSpecialtyMap.XrefEntityCode
						,TreatmentFunction.NationalSpecialtyCode
					)
					,3
				) = '500'
			then MainSpecialty.NationalSpecialtyCode

			else 
				coalesce(
					 TreatmentFunctionSpecialtyMap.XrefEntityCode
					,TreatmentFunction.NationalSpecialtyCode
				)
			end
			,3
		)


	,TreatmentFunctionCode =
		left(
			coalesce(
				 SpecialtyTreatmentFunctionMap.XrefEntityCode
				,TreatmentFunction.NationalSpecialtyCode
			)
			,3
		)

	,FirstAttendanceCode =
		FirstAttendance.MappedCode

	,Operation1 =
	left(
		replace(replace(Encounter.PrimaryOperationCode, '.', ''), ',', '')
		, 4
	) 
	,Operation2 =
	left(
		replace(replace(Encounter.SecondaryOperationCode1, '.', ''), ',', '')
		, 4
	) 
	,Operation3 =
	left(
		replace(replace(Encounter.SecondaryOperationCode2, '.', ''), ',', '')
		, 4
	) 
	,Operation4 =
	left(
		replace(replace(Encounter.SecondaryOperationCode3, '.', ''), ',', '')
		, 4
	) 
	,Operation5 =
	left(
		replace(replace(Encounter.SecondaryOperationCode4, '.', ''), ',', '')
		, 4
	) 
	,Operation6 =
	left(
		replace(replace(Encounter.SecondaryOperationCode5, '.', ''), ',', '')
		, 4
	) 
	,Operation7 =
	left(
		replace(replace(Encounter.SecondaryOperationCode6, '.', ''), ',', '')
		, 4
	) 
	,Operation8 =
	left(
		replace(replace(Encounter.SecondaryOperationCode7, '.', ''), ',', '')
		, 4
	) 
	,Operation9 =
	left(
		replace(replace(Encounter.SecondaryOperationCode8, '.', ''), ',', '')
		, 4
	) 
	,Operation10 =
	left(
		replace(replace(Encounter.SecondaryOperationCode9, '.', ''), ',', '')
		, 4
	) 
	,Operation11 =
	left(
		replace(replace(Encounter.SecondaryOperationCode10, '.', ''), ',', '')
		, 4
	) 
	,Operation12 =
	left(
		replace(replace(Encounter.SecondaryOperationCode11, '.', ''), ',', '')
		, 4
	) 

	,Encounter.EncounterRecno
from
	OP.Encounter Encounter

left join PAS.ReferenceValue Sex
on    Sex.ReferenceValueCode = Encounter.SexCode

left join PAS.ReferenceValue FirstAttendance
on    FirstAttendance.ReferenceValueCode = Encounter.FirstAttendanceFlag

left join PAS.Specialty TreatmentFunction
on	TreatmentFunction.SpecialtyCode = Encounter.SpecialtyCode

left join PAS.Consultant
on	Consultant.ConsultantCode = Encounter.ConsultantCode

left join PAS.Specialty MainSpecialty
on	MainSpecialty.SpecialtyCode = Consultant.MainSpecialtyCode

left join dbo.EntityXref TreatmentFunctionSpecialtyMap
on	TreatmentFunctionSpecialtyMap.EntityTypeCode = 'TREATMENTFUNCTIONCODE'
and	TreatmentFunctionSpecialtyMap.XrefEntityTypeCode = 'SPECIALTYCODE'
and	TreatmentFunctionSpecialtyMap.EntityCode =
		coalesce(
			 left(TreatmentFunction.NationalSpecialtyCode, 3)
			,MainSpecialty.NationalSpecialtyCode
		)

left join dbo.EntityXref SpecialtyTreatmentFunctionMap
on	SpecialtyTreatmentFunctionMap.EntityTypeCode = 'SPECIALTYCODE'
and	SpecialtyTreatmentFunctionMap.XrefEntityTypeCode like 'TREATMENTFUNCTIONCODE%'
and	SpecialtyTreatmentFunctionMap.EntityCode =
		coalesce(
			 left(TreatmentFunction.NationalSpecialtyCode, 3)
			,MainSpecialty.NationalSpecialtyCode
		)

where
	coalesce(
		 Encounter.Updated
		,Encounter.Created
	)
	 > 
	(
	select
		DateValue
	from
		dbo.Parameter
	where
		Parameter = 'HRGOPDATE'
	)
and Encounter.AppointmentDate > @InputStartDate
and Encounter.AppointmentDate < GETDATE()

select @RowsReturned = @@rowcount

select @Elapsed = DATEDIFF(second, @StartTime, getdate())

select @Stats = 
	'InputStartDate = ' + CONVERT(varchar(11), @InputStartDate)
	+ ', rows returned: ' + CONVERT(varchar(10), @RowsReturned) 
	+ ', time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' secs'

exec WriteAuditLogEvent 'HRG - WH HRG45OPInputFileExtract', @Stats, @StartTime

--print @Stats