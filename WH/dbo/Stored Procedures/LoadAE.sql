﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		LoadAE

Notes:			Stored in 

Versions:
				1.0.0.3 - 20/10/2015 - MT
					Revised table PatientGPHistory does not have duplicates therefore
					removed the sections of code about duplicates.

				1.0.0.2 - 07/10/2014 - KO
					Added condition to check whether data has been extracted from source tables 
					Added exec WHREPORTING.QLIK.UpdateAEForecastsTable instead of as seperate step in job.

				1.0.0.1 - 28/05/2014 - KO
					Added year parameter to limit how many years to run.

				1.0.0.0
					Original sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [dbo].[LoadAE]
	@yr int
As

set dateformat mdy

if (
	(select DateValue from dbo.Parameter where Parameter = 'LOADAEDATE')
		< (select DateValue from dbo.Parameter where Parameter = 'EXTRACTAEDATE')
	)

	begin
		declare @StartTime datetime
		declare @Elapsed int
		declare @Stats varchar(255)
		declare @calfrom smalldatetime
		declare @calto smalldatetime 

		select @calfrom = '1 Jan 1990'
		select @calto = convert(smalldatetime,GETDATE())

		--Build CalendarBase instead of waiting for WHOLAP to build...
		exec WHOLAP.dbo.BuildCalendarBase @calfrom, @calto

		select @StartTime = getdate()

			--update lookups. Todo - move to seperate sp
				truncate table AE.ManchesterTriageLookup
				insert into AE.ManchesterTriageLookup
				select 
					ManTriageCode = mtg_code
					,ManTriageDescription = mtg_item 
				--into AE.ManchesterTriageLookup
				from  [CASCADE].[AEMtgcat] 
				where mtg_categ = 'M'
				
				truncate table AE.DrinkLocationLookup
				insert into AE.DrinkLocationLookup
				select 
					LocationCode = ltrim(rtrim(code))
					,LocationDescription = ltrim(rtrim(descript))
				--into AE.DrinkLocationLookup
				from [CASCADE].AEDrinks
				where code <> ''
				
				--KO added 23/5/2014 Load triage step puts calculated data into a seperate table
				--to improve performance of view dbo.TLoadAEEncounter
				exec LoadAETriage @yr
				exec LoadAEEncounter
				exec LoadAEDiagnosis
				exec LoadAEInvestigation
				exec LoadAEProcedure
				exec LoadAECodingUser

				exec Pseudonomisation.dbo.PseudonomiseCascade
				--25/02/2015
				--exec dbo.LoadAEGPTest

				update dbo.Parameter
				set
					DateValue = getdate()
				where
					Parameter = 'LOADAEDATE'

				if @@rowcount = 0

				insert into dbo.Parameter
					(
					 Parameter
					,DateValue
					)
				select
					 Parameter = 'LOADAEDATE'
					,DateValue = getdate()


				select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

				select @Stats = 
					'Time Elapsed ' + 
					CONVERT(varchar(6), @Elapsed) + ' Mins, period = ' 	
					+ CONVERT(varchar(10), @yr) + ' years.' 
					--CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
					--CONVERT(varchar(11), coalesce(@to, ''))

				exec WriteAuditLogEvent 'AE - WH LoadAE', @Stats, @StartTime
				
				exec WHREPORTING.QLIK.UpdateAEForecastsTable
	end
