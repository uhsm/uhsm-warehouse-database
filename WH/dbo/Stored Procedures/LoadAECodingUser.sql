﻿

CREATE procedure [dbo].[LoadAECodingUser] as

/*
--Author: K Oakden
--Date created: 08/10/2014
--To get datetime and user who completed Cascade coding.
--Data from AESaves - but only data relating to the coding screen is imported from the dbf file
--AESAVES.dbf (where module = 'DISP FULL')
*/

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()
truncate table  AE.CodingUser

insert into AE.CodingUser (SourceUniqueID, ModifiedDateTime, ModifiedByUser)
(select  SourceUniqueID, ModifiedDateTime, ModifiedByUser
from [dbo].[TLoadAECodingUser]
)

select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(SECOND, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' secs'

exec WriteAuditLogEvent 'AE - WH LoadAECodingUser', @Stats, @StartTime