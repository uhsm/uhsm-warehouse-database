﻿
/*
--Author: K Oakden
--Date created: 08/02/2013
--To get current cascade data. 
*/

CREATE procedure [dbo].[LoadAECurrentData]
	--@interfaceCode varchar(5) = 'CASC'
as

SET DATEFORMAT dmy

declare @StartTime datetime
declare @Elapsed int
--declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

truncate table AE.CurrentData

INSERT INTO [AE].[CurrentData]
           ([SourceUniqueID]
           ,[DistrictNo]
           ,[NHSNumber]
           ,[PatientTitle]
           ,[PatientForename]
           ,[PatientSurname]
           ,[Postcode]
           ,[DateOfBirth]
           ,[SexCode]
           ,[RegisteredGpCode]
           ,[RegisteredGpPracticeCode]
           ,[PracticePCTCode]
           --,[PCTCCGCode]
           ,[AttendanceNumber]
           ,[SourceOfReferralCode]
           ,[ArrivalModeCode]
           ,[AttendanceCategoryCode]
           ,[AttendanceDisposalCode]
           ,[ArrivalDate]
           ,[ArrivalTime]
           ,[AgeOnArrival]
           ,[TriageCategoryCode]
           ,[ReferredToSpecialtyCode]
           ,[InitialAssessmentTime]
           ,[SeenForTreatmentTime]
           ,[SeenBySpecialtyTime]
           ,[AttendanceConclusionTime]
           ,[ToSpecialtyTime]
           ,[DecisionToAdmitTime]
           ,[RegisteredTime]
           ,[WhereSeen]
           ,[EncounterDurationMinutes]
           ,[STATInterventionDone]
           ,[STATInterventionTime]
           ,[BreachReason]
           ,[AdmitToWard]
           --,[InterfaceCode]
           )
     (select
           [SourceUniqueID]
           ,[DistrictNo]
           ,[NHSNumber]
           ,[PatientTitle]
           ,[PatientForename]
           ,[PatientSurname]
           ,[Postcode]
           ,[DateOfBirth]
           ,[SexCode]
           ,[RegisteredGpCode]
           ,[RegisteredGpPracticeCode]
           ,[PracticePCTCode]
           --,[PCTCCGCode]
           ,[AttendanceNumber]
           ,[SourceOfReferralCode]
           ,[ArrivalModeCode]
           ,[AttendanceCategoryCode]
           ,[AttendanceDisposalCode]
           ,[ArrivalDate]
           ,[ArrivalTime]
           ,[AgeOnArrival]
           ,[TriageCategoryCode]
           ,[ReferredToSpecialtyCode]
           ,[InitialAssessmentTime]
           ,[SeenForTreatmentTime]
           ,[SeenBySpecialtyTime]
           ,[AttendanceConclusionTime]
           ,[ToSpecialtyTime]
           ,[DecisionToAdmitTime]
           ,[RegisteredTime]
           ,[WhereSeen]
           ,[EncounterDurationMinutes]
           ,[STATInterventionDone]
           ,[STATInterventionTime]
		   ,[BreachReason]
		   ,[AdmitToWard]
	from [dbo].[TLoadAECurrentData]
	)

select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(SECOND, @StartTime, getdate())

select @Stats = 
	--'Rows deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', ' +
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Secs'

exec WriteAuditLogEvent 'AE - WH LoadAECurrentData', @Stats, @StartTime
--print @Stats