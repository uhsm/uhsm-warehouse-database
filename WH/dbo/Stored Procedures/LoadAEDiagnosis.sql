﻿CREATE procedure [dbo].[LoadAEDiagnosis] as

/*************************************************************************************
 Description: Get AE diagnosis data and update AE.Encounter
  
 Modification History ----------------------------------------------------------------

 Date		Author			Change
05/09/2014	KO				Added references to [CASCADE].AEDiagnsPart to identify X1, X2 and X3 local codes.
							Note that for national reporting these will have to be mapped back to the national code
							(SourceDiagnosisCode).
*************************************************************************************/
declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

truncate table AE.Diagnosis

insert into AE.Diagnosis
(
	 SourceUniqueID
	,SequenceNo
	,DiagnosticSchemeInUse
	,DiagnosisCode
	,AESourceUniqueID
	,SourceDiagnosisCode
	,SourceSequenceNo
	,DiagnosisSiteCode
	,DiagnosisSideCode
	--,CascadeDiagnosisCode
)
select
	 SourceUniqueID
	,SequenceNo
	,DiagnosticSchemeInUse
	,DiagnosisCode= 
		case
			when DiagnosisCode = 22 and substring(DiagnosisExtra.vascular, 2, 1) = 'T' --DVT
			then 'X1'
			when DiagnosisCode = 18 and substring(DiagnosisExtra.infections, 2, 1) = 'T'  --Cellulitis
			then 'X2'
			when DiagnosisCode = 21 and substring(DiagnosisExtra.cns, 4, 1) = 'T' --TIA
			then 'X3'
			else DiagnosisCode
		end
	,AESourceUniqueID
	,SourceDiagnosisCode
	,SourceSequenceNo
	,DiagnosisSiteCode
	,DiagnosisSideCode
	--,CascadeDiagnosisCode
from
	dbo.TLoadAEDiagnosis diag
left join [CASCADE].AEDiagnsPart DiagnosisExtra
on DiagnosisExtra.diagref = diag.AESourceUniqueID

select @RowsInserted = @@rowcount

--populate the AEEncounter diagnosis codes
update
	AE.Encounter
set
	 DiagnosisCodeFirst = AEDiagnosis.DiagnosisCodeFirst
	,DiagnosisCodeSecond = AEDiagnosis.DiagnosisCodeSecond
	,CascadeDiagnosisCode = AEDiagnosis.CascadeDiagnosisCode
from
	(
	select
		Diagnosis.AESourceUniqueID

		,DiagnosisCodeFirst = 
		(
		select
			AEDiagnosis.DiagnosisCode
		from
			AE.Diagnosis AEDiagnosis
		where
			AEDiagnosis.SequenceNo = 1
		and	AEDiagnosis.AESourceUniqueID = Diagnosis.AESourceUniqueID
		)

		,DiagnosisCodeSecond = 
		(
		select
			AEDiagnosis.DiagnosisCode
		from
			AE.Diagnosis AEDiagnosis
		where
			AEDiagnosis.SequenceNo = 2
		and	AEDiagnosis.AESourceUniqueID = Diagnosis.AESourceUniqueID
		)
		,CascadeDiagnosisCode
	from
		AE.Diagnosis
	) AEDiagnosis

where
	AEDiagnosis.AESourceUniqueID = Encounter.SourceUniqueID

--and	(
--		AEDiagnosis.DiagnosisCodeFirst <> Encounter.DiagnosisCodeFirst
--	or
--		AEDiagnosis.DiagnosisCodeSecond <> Encounter.DiagnosisCodeSecond
--	)


select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'AE - WH LoadAEDiagnosis', @Stats, @StartTime
