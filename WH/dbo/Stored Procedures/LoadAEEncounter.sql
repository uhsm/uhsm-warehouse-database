﻿


CREATE procedure [dbo].[LoadAEEncounter]
	--@interfaceCode varchar(5) = 'CASC'
as

/*************************************************************************************
 Description: Load AE.Encounter from TLoadAEEncounter 
  
 Modification History ----------------------------------------------------------------

 Date		Author			Change
28/05/2014	KO				Changed various delete statments to truncate
							Number of yrs to build based on inner join in dbo.TLoadAEEncounter
							
15/09/2014	KO				Added Episodic GP Practice Code and Name
12/08/2015  TJD				Added Telephone number
18/08/2015	CM				Added Fractclin , Revclin, opd, refothsite for CDS
*************************************************************************************/

declare @StartTime datetime
declare @Elapsed int
--declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
--declare @from datetime
--declare @to datetime

select @StartTime = getdate()

--select
--	@from = min(convert(smalldatetime, ArrivalDate))
--	,@to = max(convert(smalldatetime, ArrivalDate))
--from
--	dbo.TLoadAEEncounter

--delete from AE.Encounter
--where ArrivalDate > DATEADD(year, -5, getdate())

--delete from AE.Encounter
--where
--	coalesce(ArrivalDate, @from) between @from and @to
--and	InterfaceCode = @interfaceCode

--select @RowsDeleted = @@rowcount

--delete from AE.Encounter
--where
--	exists
--	(
--	select
--		1
--	from
--		dbo.TLoadAEEncounter
--	where
--		TLoadAEEncounter.SourceUniqueID = Encounter.SourceUniqueID
--	and	TLoadAEEncounter.InterfaceCode = Encounter.InterfaceCode
--	)
--and	Encounter.InterfaceCode = @interfaceCode


--select @RowsDeleted = @@rowcount + @RowsDeleted

truncate table AE.Encounter

insert into AE.Encounter
(
	 SourceUniqueID
	,UniqueBookingReferenceNo
	,PathwayId
	,PathwayIdIssuerCode
	,RTTStatusCode
	,RTTStartDate
	,RTTEndDate
	,DistrictNo
	,TrustNo
	,CasenoteNo
	,DistrictNoOrganisationCode
	,NHSNumber
	,NHSNumberStatusId
	,LocalPatientID
	,PatientTitle
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,Postcode
	,Telephone --{ADDED TJD 12/08/2015}
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,CarerSupportIndicator
	,RegisteredGpCode
	,RegisteredGpName
	,RegisteredGpPracticeCode
	,RegisteredGpPracticeName
	,RegisteredGpPracticePostcode
	,EpisodicGpPracticeCode
	,EpisodicGpPracticeName
	,Consultant
	,AttendanceNumber
	,ArrivalModeCode
	,AttendanceCategoryCode
	,AttendanceDisposalCode
	,IncidentLocationTypeCode
	,PatientGroupCode
	,SourceOfReferralCode
	,ArrivalDate
	,ArrivalTime
	,AgeOnArrival
	,InitialAssessmentTime
	,SeenForTreatmentTime
	,AttendanceConclusionTime
	,DepartureTime
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,ProviderReferenceNo
	,CommissionerReferenceNo
	,ProviderCode
	,CommissionerCode
	,CommissionerCodeCCG
	,SeenByCode
	,SeenBy
	,SeenByStaffType
	,InvestigationCodeFirst
	,InvestigationCodeSecond
	,DiagnosisCodeFirst
	,DiagnosisCodeSecond
	,TreatmentCodeFirst
	,TreatmentCodeSecond
	,PASHRGCode
	,HRGVersionCode
	,PASDGVPCode
	,SiteCode
	,Created
	,Updated
	,ByWhom
	,InterfaceCode
	,PCTCode
	,CCGCode
	,ResidencePCTCode
	,ResidenceCCGCode
	,SourceOfCCGCode
	,InvestigationCodeList
	,TriageCategoryCode
	,PresentingProblem
	,ToXrayTime
	,FromXrayTime
	,ToSpecialtyTime
	,SeenBySpecialtyTime
	,EthnicCategoryCode
	,ReferredToSpecialtyCode
	,DecisionToAdmitTime
	,DischargeDestinationCode
	,RegisteredTime
	,WhereSeen
	,EncounterDurationMinutes
	,STATInterventionDone
	,STATInterventionTime
	,BreachReason
	,AdmitToWard
	,AdviceTime
	,StandbyTime
	,TrolleyDurationMinutes
	,ManchesterTriageCode
	,AlcoholAuditParticipation
	,AlcoholAuditFrequency
	,AlcoholAuditTypicalDay
	,AlcoholAuditSingleOccasion
	,AlcoholAuditLocation
	,FFTextStatus
	,ReferGP--Added 07/01/2014 to enable PbR reporting of deflected AEAttendances
	,Assault --Added 13/02/2014 for FOI!
	,OpalReferral --Added10/12/2014
	,NonTrauma --ADDED CM 01/09/2015
	,MtgDiscr --ADDED CM 01/09/2015
	,Revclin --added CM 18/08/2015 for CDS
	,Fractclin --added CM 18/08/2015 for CDS
	,OPD --added CM 18/08/2015 for CDS
	,RefOthSite  --added CM 18/08/2015 for CDS
	,RfOutTrust  --added CM 18/08/2015 for CDS
	,WhereDied-- ADDED CM 18/08/2015
	 ,[mtg_categ]-- ADDED CM 19/08/2015
      ,[trclomech]-- ADDED CM 19/08/2015
      ,[fbmech]-- ADDED CM 19/08/2015
      ,[trhedmech]-- ADDED CM 19/08/2015
      ,[treyemech]-- ADDED CM 19/08/2015
      ,[trnosmech]-- ADDED CM 19/08/2015
      ,[trnecmech]-- ADDED CM 19/08/2015
      ,[trbacmech]-- ADDED CM 19/08/2015
      ,[PerceivedComplaint] --ADDED CM 26/08/2015
      ,SeenByStaffID --ADDED CM 26/08/2015 rq for CDS
      ,Setting --ADDED CM 10/09/2015 rq for CDS
      ,TriageTrauma --ADDED CM 10/09/2015 rq for CDS
	  ,AmbulanceIncidentNo--ADDED CM 10/09/2015 rq for CDS

)
select distinct
	 SourceUniqueID
	,UniqueBookingReferenceNo
	,PathwayId
	,PathwayIdIssuerCode
	,RTTStatusCode
	,RTTStartDate
	,RTTEndDate
	,DistrictNo
	,TrustNo
	,CasenoteNo
	,DistrictNoOrganisationCode
	,NHSNumber
	,NHSNumberStatusId
	,LocalPatientID
	,PatientTitle
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,ResidencePostcode
	,Telephone --{ADDED TJD 12/08/2015}
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,CarerSupportIndicator
	,RegisteredGpCode
	,RegisteredGpName
	,RegisteredGpPracticeCode
	,RegisteredGpPracticeName
	,RegisteredGpPracticePostcode
	,EpisodicGpPracticeCode
	,EpisodicGpPracticeName
	,Consultant
	,AttendanceNumber
	,ArrivalModeCode
	,AttendanceCategoryCode
	,AttendanceDisposalCode
	,IncidentLocationTypeCode
	,PatientGroupCode
	,SourceOfReferralCode
	,ArrivalDate
	,ArrivalTime
	,AgeOnArrival
	,InitialAssessmentTime
	,SeenForTreatmentTime
	,AttendanceConclusionTime
	,DepartureTime
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,ProviderReferenceNo
	,CommissionerReferenceNo
	,ProviderCode
	,CommissionerCode
	,CommissionerCodeCCG
	,SeenByCode
	,SeenBy
	,SeenByStaffType
	,InvestigationCodeFirst
	,InvestigationCodeSecond
	,DiagnosisCodeFirst
	,DiagnosisCodeSecond
	,TreatmentCodeFirst
	,TreatmentCodeSecond
	,PASHRGCode
	,HRGVersionCode
	,PASDGVPCode
	,SiteCode
	,Created = GETDATE()
	,Updated
	,ByWhom
	,InterfaceCode
	,EpisodicPCTCode
	,EpisodicCCGCode
	,ResidencePCTCode
	,ResidenceCCGCode
	,SourceOfCCG
	,InvestigationCodeList
	,TriageCategoryCode
	,PresentingProblem
	,ToXrayTime
	,FromXrayTime
	,ToSpecialtyTime
	,SeenBySpecialtyTime
	,EthnicCategoryCode
	,ReferredToSpecialtyCode
	,DecisionToAdmitTime
	,DischargeDestinationCode
	,RegisteredTime
	,WhereSeen
	,EncounterDurationMinutes
	,STATInterventionDone
	,STATInterventionTime
	,BreachReason
	,AdmitToWard
	,AdviceTime
	,StandbyTime
	,TrolleyDurationMinutes
	,ManchesterTriageCode
	,AlcoholAuditParticipation
	,AlcoholAuditFrequency
	,AlcoholAuditTypicalDay
	,AlcoholAuditSingleOccasion
	,AlcoholAuditLocation
	,FFTextStatus
	,ReferGP--Added 07/01/2014 to enable PbR reporting of deflected AEAttendances
	,Assault
	,OpalReferral
	,NonTrauma --ADDED CM 01/09/2015
	,MtgDiscr --ADDED CM 01/09/2015
	,Revclin --added CM 18/08/2015 for CDS
	,Fractclin --added CM 18/08/2015 for CDS
	,opd  --added CM 18/08/2015 for CDS
	,refothsite  --added CM 18/08/2015 for CDS
	,rfouttrust  --added CM 18/08/2015 for CDS
	,wheredied-- ADDED CM 18/08/2015
	 ,[mtg_categ]-- ADDED CM 19/08/2015
      ,[trclomech]-- ADDED CM 19/08/2015
      ,[fbmech]-- ADDED CM 19/08/2015
      ,[trhedmech]-- ADDED CM 19/08/2015
      ,[treyemech]-- ADDED CM 19/08/2015
      ,[trnosmech]-- ADDED CM 19/08/2015
      ,[trnecmech]-- ADDED CM 19/08/2015
      ,[trbacmech]-- ADDED CM 19/08/2015
      ,PerceivedComplaint --ADDED CM 26/08/2015
      ,SeenByStaffID --ADDED CM 26/08/2015
      ,setting  --ADDED CM 10/09/2015
	  ,TriageTrauma --ADDED CM 10/09/2015
	  ,AmbulanceIncidentNo--ADDED CM 10/09/2015 rq for CDS

from
	dbo.TLoadAEEncounter

select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
		'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) 
	+ ', Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins.' 


exec WriteAuditLogEvent 'AE - WH LoadAEEncounter', @Stats, @StartTime

