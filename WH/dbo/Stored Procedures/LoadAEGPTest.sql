﻿
CREATE procedure [dbo].[LoadAEGPTest] as 
--select COUNT(1) from dbo.AEEncounterGP
--select COUNT(1) from dbo.AEEncounterGPPASOnly

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

truncate table dbo.AEEncounterGP
insert into dbo.AEEncounterGP
select * from dbo.TLoadAEEncounterGP

select @Elapsed = DATEDIFF(SECOND, @StartTime,getdate())
select @Stats = 'AEEncounterGP: Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' secs'
exec WriteAuditLogEvent 'PAS - WH LoadAEGPTest', @Stats, @StartTime
------------------------------------------------------------------------------
select @StartTime = getdate()

truncate table dbo.AEEncounterGPPASOnly
insert into dbo.AEEncounterGPPASOnly
select * from dbo.TLoadAEEncounterGPPASOnly

select @Elapsed = DATEDIFF(SECOND, @StartTime,getdate())
select @Stats = 'AEEncounterGPPASOnly: Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' secs'
exec WriteAuditLogEvent 'PAS - WH LoadAEGPTest', @Stats, @StartTime
--select SourceUniqueID,  COUNT(1) from dbo.AEEncounterGPPASOnly
--group by SourceUniqueID having COUNT(1) > 1

--select top 1000 * from dbo.AEEncounterGPPASOnly

--select * from dbo.AEEncounterGP where SourceUniqueID = '0398938G002'
--select * from dbo.AEEncounterGPPASOnly where SourceUniqueID = '0398938G002'

--select * from WHREPORTING.LK.PatientCurrentDetails where FacilityID = 'RM24113997'