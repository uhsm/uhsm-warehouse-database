﻿CREATE procedure [dbo].[LoadAEInvestigation]
as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

truncate table AE.Investigation

insert into AE.Investigation
(
	 SourceUniqueID
	,InvestigationDate
	,SequenceNo
	,InvestigationCode
	,AESourceUniqueID
	,SourceInvestigationCode
	,ResultDate
)
select
	 SourceUniqueID
	,InvestigationDate
	,SequenceNo
	,InvestigationCode
	,AESourceUniqueID
	,SourceInvestigationCode
	,ResultDate
from
	dbo.TLoadAEInvestigation

select @RowsInserted = @@rowcount

--populate the AEEncounter Investigation codes
update
	AE.Encounter
set
	 InvestigationCodeFirst = AEInvestigation.InvestigationCodeFirst

	,InvestigationCodeSecond = AEInvestigation.InvestigationCodeSecond
from
	(
	select
		Investigation.AESourceUniqueID

		,InvestigationCodeFirst = 
		(
		select
			AEInvestigation.InvestigationCode
		from
			AE.Investigation AEInvestigation
		where
			AEInvestigation.SequenceNo = 1
		and	AEInvestigation.AESourceUniqueID = Investigation.AESourceUniqueID
		)

		,InvestigationCodeSecond = 
		(
		select
			AEInvestigation.InvestigationCode
		from
			AE.Investigation AEInvestigation
		where
			AEInvestigation.SequenceNo = 2
		and	AEInvestigation.AESourceUniqueID = Investigation.AESourceUniqueID
		)
	from
		AE.Investigation
	) AEInvestigation

where
	AEInvestigation.AESourceUniqueID = Encounter.SourceUniqueID

--and	(
--		AEInvestigation.InvestigationCodeFirst <> Encounter.InvestigationCodeFirst
--	or
--		AEInvestigation.InvestigationCodeSecond <> Encounter.InvestigationCodeSecond
--	)


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'AE - WH LoadAEInvestigation', @Stats, @StartTime
