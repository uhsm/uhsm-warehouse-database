﻿create procedure [dbo].[LoadAEMCH]
as

--Manchester Community Health Data Load

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)


select @StartTime = getdate()

delete from AE.MCH
where
	Period between 
		(
		select
			MIN(Period)
		from
			TLoadAEMCH
		)
		and
		(
		select
			MAX(Period)
		from
			TLoadAEMCH
		)

select @RowsDeleted = @@rowcount

insert into AE.MCH
(
	 SourceUniqueID
	,Period
	,Days
	,FirstAttends
	,ReviewAttends
	,Attends
	,OtherAttends
	,SiteCode
	,Created
	,Updated
	,ByWhom
)
select
	 SourceUniqueID = SiteCode + CONVERT(varchar, Period, 112)
	,Period
	,Days
	,FirstAttends
	,ReviewAttends
	,Attends
	,OtherAttends
	,SiteCode
	,Created = getdate()
	,Updated = null
	,ByWhom = system_user
from
	dbo.TLoadAEMCH

select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', ' + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'LoadAEMCH', @Stats, @StartTime
