﻿CREATE procedure [dbo].[LoadAEMCHEncounter]
	@interfaceCode varchar(5) = 'MCH'
as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)


select @StartTime = getdate()

delete from AE.Encounter
where
	InterfaceCode = @interfaceCode

select @RowsDeleted = @@rowcount

insert into AE.Encounter
(
	 SourceUniqueID
	,AttendanceCategoryCode
	,AttendanceDisposalCode
	,ArrivalDate
	,ArrivalTime
	,InitialAssessmentTime
	,SeenForTreatmentTime
	,AttendanceConclusionTime
	,DepartureTime
	,SiteCode
	,Created
	,Updated
	,ByWhom
	,InterfaceCode
	,ToXrayTime
	,FromXrayTime
	,ToSpecialtyTime
	,SeenBySpecialtyTime
	,DecisionToAdmitTime
	,RegisteredTime
	,Attends
)
select
	 SourceUniqueID
	,AttendanceCategoryCode = 'W' --Walk-In
	,AttendanceDisposalCode = 'DA'
	,ArrivalDate
	,ArrivalTime = ArrivalDate
	,InitialAssessmentTime = ArrivalDate
	,SeenForTreatmentTime = ArrivalDate
	,AttendanceConclusionTime = ArrivalDate
	,DepartureTime = ArrivalDate
	,SiteCode
	,Created
	,Updated
	,ByWhom
	,InterfaceCode = 'MCH'
	,ToXrayTime = ArrivalDate
	,FromXrayTime = ArrivalDate
	,ToSpecialtyTime = ArrivalDate
	,SeenBySpecialtyTime = ArrivalDate
	,DecisionToAdmitTime = ArrivalDate
	,RegisteredTime = ArrivalDate
	,Attends
from
	(
	select
		 SourceUniqueID = Activity.SiteCode + CONVERT(varchar, Calendar.TheDate, 112)
		,ArrivalDate = Calendar.TheDate

		,Attends =
			(Activity.Attends / Activity.Days) * (select NumericValue from dbo.Parameter where Parameter = 'MCHAPPORTIONMENT')

		,SiteCode
		,Created
		,Updated
		,ByWhom
	from
		AE.MCH Activity

	inner join WHOLAP.dbo.OlapCalendar Calendar
	on	(
			Activity.Period = Calendar.LastDayOfWeek
		and	Activity.Days = 7
		)
	or	(
			Calendar.TheDate between DATEADD(day, (Activity.Days -1) * -1, Activity.Period) and Activity.Period
		and	Activity.Days <> 7
		)
	) Activity

select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', ' + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'LoadAEMCHEncounter', @Stats, @StartTime
