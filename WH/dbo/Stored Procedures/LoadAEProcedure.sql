﻿CREATE procedure [dbo].[LoadAEProcedure]as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

truncate table AE.[Procedure]

insert into AE.[Procedure]
(
	 SourceUniqueID
	,ProcedureDate
	,SequenceNo
	,ProcedureCode
	,AESourceUniqueID
	,SourceProcedureCode
)
select
	 SourceUniqueID
	,ProcedureDate
	,SequenceNo
	,ProcedureCode
	,AESourceUniqueID
	,SourceProcedureCode
from
	dbo.TLoadAEProcedure

select @RowsInserted = @@rowcount

--populate the AEEncounter Procedure codes
update
	AE.Encounter
set
	 TreatmentCodeFirst = AEProcedure.TreatmentCodeFirst

	,TreatmentCodeSecond = AEProcedure.TreatmentCodeSecond
from
	(
	select
		[Procedure].AESourceUniqueID

		,TreatmentCodeFirst = 
		(
		select
			AEProcedure.ProcedureCode
		from
			AE.[Procedure] AEProcedure
		where
			AEProcedure.SequenceNo = 1
		and	AEProcedure.AESourceUniqueID = [Procedure].AESourceUniqueID
		)

		,TreatmentCodeSecond = 
		(
		select
			AEProcedure.ProcedureCode
		from
			AE.[Procedure] AEProcedure
		where
			AEProcedure.SequenceNo = 2
		and	AEProcedure.AESourceUniqueID = [Procedure].AESourceUniqueID
		)
	from
		AE.[Procedure]
	) AEProcedure

where
	AEProcedure.AESourceUniqueID = Encounter.SourceUniqueID

--and	(
--		AEProcedure.TreatmentCodeFirst <> Encounter.TreatmentCodeFirst
--	or
--		AEProcedure.TreatmentCodeSecond <> Encounter.TreatmentCodeSecond
--	)


select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'AE - WH LoadAEProcedure', @Stats, @StartTime
