﻿

CREATE procedure [dbo].[LoadAETriage] (
	@yr int
)as
/*
--Author: K Oakden
--Date created: 23/05/2014
--To build triage data required by view dbo.TLoadAEEncounter
--Created seperate table to improve performance and replace view CASCADE.TLoadAEAttend
*/

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

--drop table [CASCADE].Triage

--create base triage data
truncate table [CASCADE].Triage
insert into [CASCADE].Triage

select
	SourceUniqueID = Encounter.aepatno + Encounter.attno
	--Arrival date is the earlier of regdate and triagedate
	,ArrivalDate =
		case
			when isdate(Triage.triagedate) <> 1
			then Encounter.regdate
			when isdate(Encounter.regdate) <> 1
			then Triage.triagedate
			when convert(datetime, Encounter.regdate) < convert(datetime, Triage.triagedate)
			--then Encounter.regdate
			then convert(datetime, Encounter.regdate)
			--else Triage.triagedate
			else convert(datetime, Triage.triagedate)
		end

	,ArrivalTime =
		case
			when isdate(Triage.triagedate) <> 1
			then Encounter.regtime
			when isdate(Encounter.regdate) <> 1
			then Triage.triagetime
			when convert(datetime, Encounter.regdate) = convert(datetime, Triage.triagedate)
				and	Encounter.regtime < Triage.triagetime
			then Encounter.regtime
			when convert(datetime, Encounter.regdate) = convert(datetime, Triage.triagedate)
				and	Encounter.regtime > Triage.triagetime
			then Triage.triagetime
			when convert(datetime, Encounter.regdate) < convert(datetime, Triage.triagedate)
			then Encounter.regtime
			else Triage.triagetime
		end

	,Triage.triagedate
	,Triage.triagetime
	,triage = 
		case
			when Triage.triage = ''
			then null
			else Triage.triage
		end

	,ManchesterTriageCode = Triage.mtg_categ
	,TriageCategoryCode = null
	,Nontrauma = Triage.nontrauma -- {ADDED TJD 15/08/2015}
	,MtgDiscr = Triage.mtg_discr -- {ADDED TJD 15/08/2015}
--into [CASCADE].Triage
from [CASCADE].AEAttend Encounter

left join [CASCADE].AETriage Triage
	on	Triage.attendref = Encounter.aepatno + Encounter.attno
	and	not exists
	(
		select 	1
		from [CASCADE].AETriage Previous
		where Previous.attendref = Triage.attendref
		and	(
			Previous.triagedate > Triage.triagedate
		or	(
			Previous.triagedate = Triage.triagedate
			and	Previous.triagetime < Triage.triagetime
			)
		or	(
			Previous.triagedate = Triage.triagedate
			and	Previous.triagetime = Triage.triagetime
			and	Previous.EncounterRecno < Triage.EncounterRecno
			)
		)
	)
where	--limit to ArrivalDate within last 3 years
		case
			when isdate(Triage.triagedate) <> 1
			then Encounter.regdate
			when isdate(Encounter.regdate) <> 1
			then Triage.triagedate
			when convert(datetime, Encounter.regdate) < convert(datetime, Triage.triagedate)
			--then Encounter.regdate
			then convert(datetime, Encounter.regdate)
			--else Triage.triagedate
			else convert(datetime, Triage.triagedate)
		end
		>  dateadd(year, -@yr, GETDATE())
--alter table [CASCADE].Triage alter column 	[SourceUniqueID] [varchar](15) NOT NULL
--alter table [CASCADE].Triage add PRIMARY KEY CLUSTERED (SourceUniqueID ASC)

select @RowsInserted = @@rowcount

update [CASCADE].Triage
set TriageCategoryCode =
			case
			when Encounter.triage is null
			then
				case
				when --if in standby table then must be Red
					exists
					(
					select
						1
					from
						[CASCADE].AEStndBy
					where
						AEStndBy.stattendrf = SourceUniqueID
					)
				then 'R'
				else null
				end
			else Encounter.triage
			end
from [CASCADE].Triage Encounter


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) 
	+ ', Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins, ' 
	+ CONVERT(varchar(10), @yr) + ' years.' 

exec WriteAuditLogEvent 'AE - WH LoadAETriage', @Stats, @StartTime


