﻿CREATE       procedure [dbo].[LoadAPC] 
	 @from smalldatetime = null
	,@to smalldatetime = null
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

select @to = coalesce(@to, dateadd(day, datediff(day, 0, getdate()), 0))

select
	@from = 
		coalesce(
			@from
			,(
			select
				DateValue
			from
				dbo.Parameter
			where
				Parameter = 'EXTRACTAPCDATE'
			)
		)


--05/04/2016 CM Added this to build a table from which we can build a diagnosis and procedures table which the build of APC.Encounter can use, which does not have duplicated codes in the same position
exec LoadAPCDiagnosisProcedureRefnos


truncate table TImportAPCEncounter
exec ExtractLorenzoAPC @from
exec LoadAPCEncounter


truncate table TImportAPCWardStay
exec ExtractLorenzoAPCWardStay @from
exec LoadAPCWardStay


truncate table TImportAPCMaternitySpell
exec ExtractLorenzoMaternitySpell @from
exec LoadAPCMaternitySpell


truncate table TImportAPCBirth
exec ExtractLorenzoBirth @from
exec LoadAPCBirth


exec dbo.ExtractLorenzoAPCCriticalCarePeriod
exec dbo.LoadAPCCriticalCarePeriod

--update theatre details
--reset all references
update
	APC.Encounter
set
	 Encounter.TheatrePatientBookingKey = null
	,Encounter.ProcedureTime = null
	,Encounter.TheatreCode = null

--update all references
update
	APC.Encounter
set
	 Encounter.TheatrePatientBookingKey = OperationDetail.SourceUniqueID
	,Encounter.ProcedureTime = coalesce(OperationDetail.OperationStartDate, OperationDetail.OperationDate)
	,Encounter.TheatreCode = OperationDetail.TheatreCode
from
	APC.Encounter Encounter

inner join Theatre.OperationDetail OperationDetail
on	OperationDetail.DistrictNo = Encounter.DistrictNo
and	coalesce(OperationDetail.OperationStartDate, OperationDetail.OperationDate) between Encounter.EpisodeStartTime and	Encounter.EpisodeEndTime

where
	coalesce(OperationDetail.OperationStartDate, OperationDetail.OperationDate) between '1 Jan 1900' and '6 Jun 2079'


--remove duplicate assignments where Encounter.PrimaryOperationCode is null
update
	APC.Encounter
set
	 Encounter.TheatrePatientBookingKey = null
	,Encounter.ProcedureTime = null
	,Encounter.TheatreCode = null
from
	APC.Encounter Encounter
where
	Encounter.PrimaryOperationCode is null
and	Encounter.TheatrePatientBookingKey in
	(
	select
		Duplicate.TheatrePatientBookingKey
	from
		APC.Encounter Duplicate
	group by
		Duplicate.TheatrePatientBookingKey
	having
		count(*) > 1
	)

--remove other duplicate assignments
update
	APC.Encounter
set
	 Encounter.TheatrePatientBookingKey = null
	,Encounter.ProcedureTime = null
	,Encounter.TheatreCode = null
from
	APC.Encounter Encounter
where
	Encounter.TheatrePatientBookingKey in
	(
	select
		Duplicate.TheatrePatientBookingKey
	from
		APC.Encounter Duplicate
	group by
		Duplicate.TheatrePatientBookingKey
	having
		count(*) > 1
	)


--Build breach details
exec BuildAPCBreach

--Build Readmission table
exec dbo.BuildReadmission

----Build BedStatus table
--exec UHSMApplications.SB.ExtractBedStatus
--Moved to seperate schedule


update dbo.Parameter
set
	DateValue = @StartTime
where
	Parameter = 'EXTRACTAPCDATE'

if @@rowcount = 0

insert into dbo.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'EXTRACTAPCDATE'
	,DateValue = @StartTime


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'PAS - WH LoadAPC', @Stats, @StartTime
