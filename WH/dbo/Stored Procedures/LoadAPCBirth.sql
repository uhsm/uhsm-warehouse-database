﻿CREATE procedure [dbo].[LoadAPCBirth] 
as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select @StartTime = getdate()


--delete archived activity
delete from APC.Birth 
where	
	exists
	(
	select
		1
	from
		dbo.TLoadAPCBirth
	where
		TLoadAPCBirth.SourceUniqueID = Birth.SourceUniqueID
	and	TLoadAPCBirth.ArchiveFlag = 1
	)


SELECT @RowsDeleted = @@Rowcount


update
	APC.Birth
set
	 MaternitySpellSourceUniqueID = TImport.MaternitySpellSourceUniqueID
	,SourcePatientNo = TImport.SourcePatientNo
	,ReferralSourceUniqueID = TImport.ReferralSourceUniqueID
	,ProfessionalCarerCode = TImport.ProfessionalCarerCode
	,MotherSourcePatientNo = TImport.MotherSourcePatientNo
	,BirthOrder = TImport.BirthOrder
	,DateOfBirth = TImport.DateOfBirth
	,SexCode = TImport.SexCode
	,StatusOfPersonConductingDeliveryCode = TImport.StatusOfPersonConductingDeliveryCode
	,LiveOrStillBirthCode = TImport.LiveOrStillBirthCode
	,DeliveryPlaceCode = TImport.DeliveryPlaceCode
	,ResuscitationMethodByPressureCode = TImport.ResuscitationMethodByPressureCode
	,ResuscitationMethodByDrugCode = TImport.ResuscitationMethodByDrugCode
	,BirthWeight = TImport.BirthWeight
	,ApgarScoreAt1Minute = TImport.ApgarScoreAt1Minute
	,ApgarScoreAt5Minutes = TImport.ApgarScoreAt5Minutes
	,BCGAdministeredCode = TImport.BCGAdministeredCode
	,CircumferenceOfBabysHead = TImport.CircumferenceOfBabysHead
	,LengthOfBaby = TImport.LengthOfBaby
	,ExaminationOfHipsCode = TImport.ExaminationOfHipsCode
	,FollowUpCareCode = TImport.FollowUpCareCode
	,FoetalPresentationCode = TImport.FoetalPresentationCode
	,MetabolicScreeningCode = TImport.MetabolicScreeningCode
	,JaundiceCode = TImport.JaundiceCode
	,FeedingTypeCode = TImport.FeedingTypeCode
	,DeliveryMethodCode = TImport.DeliveryMethodCode
	,MaternityDrugsCode = TImport.MaternityDrugsCode
	,GestationLength = TImport.GestationLength
	,ApgarScoreAt10Minutes = TImport.ApgarScoreAt10Minutes
	,NeonatalLevelOfCareCode = TImport.NeonatalLevelOfCareCode
	,PASCreated = TImport.PASCreated
	,PASUpdated = TImport.PASUpdated
	,PASCreatedByWhom = TImport.PASCreatedByWhom
	,PASUpdatedByWhom = TImport.PASUpdatedByWhom

	,Updated = getdate()
	,ByWhom = system_user
from
	dbo.TLoadAPCBirth TImport
where
	APC.Birth.SourceUniqueID = TImport.SourceUniqueID


select @RowsUpdated = @@rowcount


insert into APC.Birth
(
	 SourceUniqueID
	,MaternitySpellSourceUniqueID
	,SourcePatientNo
	,ReferralSourceUniqueID
	,ProfessionalCarerCode
	,MotherSourcePatientNo
	,BirthOrder
	,DateOfBirth
	,SexCode
	,StatusOfPersonConductingDeliveryCode
	,LiveOrStillBirthCode
	,DeliveryPlaceCode
	,ResuscitationMethodByPressureCode
	,ResuscitationMethodByDrugCode
	,BirthWeight
	,ApgarScoreAt1Minute
	,ApgarScoreAt5Minutes
	,BCGAdministeredCode
	,CircumferenceOfBabysHead
	,LengthOfBaby
	,ExaminationOfHipsCode
	,FollowUpCareCode
	,FoetalPresentationCode
	,MetabolicScreeningCode
	,JaundiceCode
	,FeedingTypeCode
	,DeliveryMethodCode
	,MaternityDrugsCode
	,GestationLength
	,ApgarScoreAt10Minutes
	,NeonatalLevelOfCareCode
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,Created
	,ByWhom
)
select
	 SourceUniqueID
	,MaternitySpellSourceUniqueID
	,SourcePatientNo
	,ReferralSourceUniqueID
	,ProfessionalCarerCode
	,MotherSourcePatientNo
	,BirthOrder
	,DateOfBirth
	,SexCode
	,StatusOfPersonConductingDeliveryCode
	,LiveOrStillBirthCode
	,DeliveryPlaceCode
	,ResuscitationMethodByPressureCode
	,ResuscitationMethodByDrugCode
	,BirthWeight
	,ApgarScoreAt1Minute
	,ApgarScoreAt5Minutes
	,BCGAdministeredCode
	,CircumferenceOfBabysHead
	,LengthOfBaby
	,ExaminationOfHipsCode
	,FollowUpCareCode
	,FoetalPresentationCode
	,MetabolicScreeningCode
	,JaundiceCode
	,FeedingTypeCode
	,DeliveryMethodCode
	,MaternityDrugsCode
	,GestationLength
	,ApgarScoreAt10Minutes
	,NeonatalLevelOfCareCode
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom

	,Created = getdate()
	,ByWhom = system_user
from
	dbo.TLoadAPCBirth TImport
where
	not exists
	(
	select
		1
	from
		APC.Birth
	where
		Birth.SourceUniqueID = TImport.SourceUniqueID
	)
and	TImport.ArchiveFlag = 0


SELECT @RowsInserted = @@Rowcount


SELECT @Elapsed = DATEDIFF(minute, @StartTime,getdate())

SELECT @Stats = 
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Updated '  + CONVERT(varchar(10), @RowsUpdated) +  
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH LoadAPCBirth', @Stats, @StartTime

print @Stats

