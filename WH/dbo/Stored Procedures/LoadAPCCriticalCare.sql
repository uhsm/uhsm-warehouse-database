﻿CREATE PROCEDURE [dbo].[LoadAPCCriticalCare]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)


select
	@StartTime = getdate()


delete from APC.CriticalCare
where
	StartDate between 
	(
	select
		min(convert(smalldatetime, StartDate))
	from
			dbo.TLoadAPCCriticalCare
	)
and
	(
	select
		max(convert(smalldatetime,StartDate))
	from
			dbo.TLoadAPCCriticalCare
	)


SELECT @RowsDeleted = @@Rowcount


INSERT INTO APC.CriticalCare
	(
	 SourceUniqueID
	,AdvancedCardiovascularSupportDays
	,AdvancedRespiratorySupportDays
	,BasicCardiovascularSupportDays
	,BasicRespiratorySupportDays
	,CriticalCareLevel2Days
	,CriticalCareLevel3Days
	,CreatedByUser
	,CreatedByTime
	,DermatologicalSupportDays
	,EndDate
	,LiverSupportDays
	,LocalIdentifier
	,LocationCode
	,NeurologicalSupportDays
	,RenalSupportDays
	,StartDate
	,StartTime
	,StatusCode
	,TreatmentFunctionCode
	,SourceSpellNo
	,SourcePatientNo
	,PlannedAcpPeriod
	,Created
	,ByWhom
	) 
select
	 SourceUniqueID
	,AdvancedCardiovascularSupportDays
	,AdvancedRespiratorySupportDays
	,BasicCardiovascularSupportDays
	,BasicRespiratorySupportDays
	,CriticalCareLevel2Days
	,CriticalCareLevel3Days
	,CreatedByUser
	,CreatedByTime
	,DermatologicalSupportDays
	,EndDate
	,LiverSupportDays
	,LocalIdentifier
	,LocationCode
	,NeurologicalSupportDays
	,RenalSupportDays
	,StartDate
	,StartTime
	,StatusCode
	,TreatmentFunctionCode
	,SourceSpellNo
	,SourcePatientNo
	,PlannedAcpPeriod

	,Created = getdate()
	,ByWhom = system_user
from
	dbo.TLoadAPCCriticalCare TEncounter

SELECT @RowsInserted = @@Rowcount


SELECT @Elapsed = DATEDIFF(minute, @StartTime,getdate())

SELECT @Stats = 'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadAPCCriticalCare', @Stats, @StartTime

print @Stats
