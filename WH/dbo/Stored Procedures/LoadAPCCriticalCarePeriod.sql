﻿CREATE Procedure [dbo].[LoadAPCCriticalCarePeriod] AS

TRUNCATE TABLE APC.CriticalCarePeriod

INSERT INTO APC.CriticalCarePeriod (

	 SourceCCPStayNo
	,SourceCCPNo
	,CCPSequenceNo
	,SourceEncounterNo
	,SourceWardStayNo
	,SourceSpellNo
	,CCPIndicator
	,CCPStartDate
	,CCPEndDate
	,CCPReadyDate
	,CCPNoOfOrgansSupported
	,CCPICUDays
	,CCPHDUDays
	,CCPAdmissionSource
	,CCPAdmissionType
	,CCPAdmissionSourceLocation
	,CCPUnitFunction
	,CCPUnitBedFunction
	,CCPDischargeStatus
	,CCPDischargeDestination
	,CCPDischargeLocation
	,CCPStayOrganSupported
	,CCPStayCareLevel
	,CCPStayNoOfOrgansSupported
	,CCPStayWard
	,CCPStayDate
	,CCPStay
	,CCPStayType
	,CCPBedStay

	)
(
SELECT
	 SourceCCPStayNo
	,SourceCCPNo
	,CCPSequenceNo
	,SourceEncounterNo
	,SourceWardStayNo
	,SourceSpellNo
	,CCPIndicator
	,CCPStartDate
	,CCPEndDate
	,CCPReadyDate
	,CCPNoOfOrgansSupported
	,CCPICUDays
	,CCPHDUDays
	,CCPAdmissionSource
	,CCPAdmissionType
	,CCPAdmissionSourceLocation
	,CCPUnitFunction
	,CCPUnitBedFunction
	,CCPDischargeStatus
	,CCPDischargeDestination
	,CCPDischargeLocation
	,CCPStayOrganSupported
	,CCPStayCareLevel
	,CCPStayNoOfOrgansSupported
	,CCPStayWard
	,CCPStayDate
	,CCPStay
	,CCPStayType
	,CCPBedStay
	FROM TLoadAPCCriticalCarePeriod
	
	)
	
	
	
	
	