﻿

CREATE Procedure dbo.LoadAPCDiagnosisProcedureRefnos

AS


/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Create a table with Clinical Coding re-ordered to get the max (DGPro_Refno) per episode and sort order.
				There are somes episode which have duplicated codes or have more than one code in the same position.
				the build of this table will allow us to extract the latest code for that position based on the sequential order of the DGPro_Refno
				Procedure needs to be run before the build of APC.Encounter
Notes:			Stored in 

Versions:		CM - 1.0.0.0
					Original sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/


Truncate Table  APC.DiagnosisProcedureRefnos



Insert Into APC.DiagnosisProcedureRefnos
Select Diagnosis.SORCE_REFNO
		,Diagnosis.DPTYP_CODE
		,Diagnosis.SORT_ORDER
		,DGPRO_REFNO = MAX(Diagnosis.DGPRO_REFNO)
		, GETDATE()
		
from
	Lorenzo.dbo.ClinicalCoding Diagnosis
where
Diagnosis.SORCE_CODE = 'PRCAE'
and	ARCHV_FLAG = 'N'

Group By Diagnosis.SORCE_REFNO
		,Diagnosis.DPTYP_CODE
		,Diagnosis.SORT_ORDER
