﻿
CREATE PROCEDURE [dbo].[LoadAPCEncounter]

as

/******************************************

--CM 06/01/2016 - added EpisodicGPPracticeCode field in to be loaded from the TLOADAPCEncounter view load as we can now populate this from the view.  Previously this field was updated via a post load update statement in LoadPatientGPPracticeHistory

******************************************/

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


--delete archived activity
delete from APC.Encounter 
where	
	exists
	(
	select
		1
	from
		dbo.TLoadAPCEncounter
	where
		TLoadAPCEncounter.SourceUniqueID = Encounter.SourceUniqueID
	and	TLoadAPCEncounter.ArchiveFlag = 1
	)


SELECT @RowsDeleted = @@Rowcount

update APC.Encounter
set
	 SourcePatientNo = TEncounter.SourcePatientNo 
	,SourceSpellNo = TEncounter.SourceSpellNo 
	,SourceEncounterNo = TEncounter.SourceEncounterNo 
	,ProviderSpellNo = TEncounter.ProviderSpellNo
	,ReferralSourceUniqueID = TEncounter.ReferralSourceUniqueID
	,WaitingListSourceUniqueID = TEncounter.WaitingListSourceUniqueID
	,PatientTitle = TEncounter.PatientTitle 
	,PatientForename = TEncounter.PatientForename 
	,PatientSurname = TEncounter.PatientSurname 
	,DateOfBirth = TEncounter.DateOfBirth 
	,DateOfDeath = TEncounter.DateOfDeath 
	,SexCode = TEncounter.SexCode 
	,NHSNumber = TEncounter.NHSNumber 
	,Postcode = TEncounter.Postcode 
	,PatientAddress1 = TEncounter.PatientAddress1 
	,PatientAddress2 = TEncounter.PatientAddress2 
	,PatientAddress3 = TEncounter.PatientAddress3 
	,PatientAddress4 = TEncounter.PatientAddress4 
	,DHACode = TEncounter.DHACode 
	,EthnicOriginCode = TEncounter.EthnicOriginCode 
	,MaritalStatusCode = TEncounter.MaritalStatusCode 
	,ReligionCode = TEncounter.ReligionCode 
	,DateOnWaitingList = TEncounter.DateOnWaitingList 
	,AdmissionDate = TEncounter.AdmissionDate 
	,DischargeDate = TEncounter.DischargeDate 
	,EpisodeStartDate = TEncounter.EpisodeStartDate 
	,EpisodeEndDate = TEncounter.EpisodeEndDate 
	,StartSiteCode = TEncounter.StartSiteCode 
	,StartWardTypeCode = TEncounter.StartWardTypeCode 
	,EndSiteCode = TEncounter.EndSiteCode 
	,EndWardTypeCode = TEncounter.EndWardTypeCode 
	,RegisteredGpCode = TEncounter.RegisteredGpCode 
	,RegisteredGpPracticeCode = TEncounter.RegisteredGpPracticeCode 
	,SiteCode = TEncounter.SiteCode 
	,AdmissionMethodCode = TEncounter.AdmissionMethodCode 
	,AdmissionSourceCode = TEncounter.AdmissionSourceCode 
	,PatientClassificationCode = TEncounter.PatientClassificationCode 
	,ManagementIntentionCode = TEncounter.ManagementIntentionCode 
	,DischargeMethodCode = TEncounter.DischargeMethodCode 
	,DischargeDestinationCode = TEncounter.DischargeDestinationCode 
	,AdminCategoryCode = TEncounter.AdminCategoryCode 
	,ConsultantCode = TEncounter.ConsultantCode 
	,SpecialtyCode = TEncounter.SpecialtyCode 
	,LastEpisodeInSpellIndicator = TEncounter.LastEpisodeInSpellIndicator 
	,FirstRegDayOrNightAdmit = TEncounter.FirstRegDayOrNightAdmit 
	,NeonatalLevelOfCare = TEncounter.NeonatalLevelOfCare 
	,PASHRGCode = TEncounter.PASHRGCode 
	,PrimaryDiagnosisCode = TEncounter.PrimaryDiagnosisCode 
	,SubsidiaryDiagnosisCode = TEncounter.SubsidiaryDiagnosisCode 
	,SecondaryDiagnosisCode1 = TEncounter.SecondaryDiagnosisCode1 
	,SecondaryDiagnosisCode2 = TEncounter.SecondaryDiagnosisCode2 
	,SecondaryDiagnosisCode3 = TEncounter.SecondaryDiagnosisCode3 
	,SecondaryDiagnosisCode4 = TEncounter.SecondaryDiagnosisCode4 
	,SecondaryDiagnosisCode5 = TEncounter.SecondaryDiagnosisCode5 
	,SecondaryDiagnosisCode6 = TEncounter.SecondaryDiagnosisCode6 
	,SecondaryDiagnosisCode7 = TEncounter.SecondaryDiagnosisCode7 
	,SecondaryDiagnosisCode8 = TEncounter.SecondaryDiagnosisCode8 
	,SecondaryDiagnosisCode9 = TEncounter.SecondaryDiagnosisCode9 
	,SecondaryDiagnosisCode10 = TEncounter.SecondaryDiagnosisCode10 
	,SecondaryDiagnosisCode11 = TEncounter.SecondaryDiagnosisCode11 
	,SecondaryDiagnosisCode12 = TEncounter.SecondaryDiagnosisCode12 
	,PrimaryOperationCode = TEncounter.PrimaryOperationCode 
	,PrimaryOperationDate = TEncounter.PrimaryOperationDate 
	,SecondaryOperationCode1 = TEncounter.SecondaryOperationCode1 
	,SecondaryOperationDate1 = TEncounter.SecondaryOperationDate1 
	,SecondaryOperationCode2 = TEncounter.SecondaryOperationCode2 
	,SecondaryOperationDate2 = TEncounter.SecondaryOperationDate2 
	,SecondaryOperationCode3 = TEncounter.SecondaryOperationCode3 
	,SecondaryOperationDate3 = TEncounter.SecondaryOperationDate3 
	,SecondaryOperationCode4 = TEncounter.SecondaryOperationCode4 
	,SecondaryOperationDate4 = TEncounter.SecondaryOperationDate4 
	,SecondaryOperationCode5 = TEncounter.SecondaryOperationCode5 
	,SecondaryOperationDate5 = TEncounter.SecondaryOperationDate5 
	,SecondaryOperationCode6 = TEncounter.SecondaryOperationCode6 
	,SecondaryOperationDate6 = TEncounter.SecondaryOperationDate6 
	,SecondaryOperationCode7 = TEncounter.SecondaryOperationCode7 
	,SecondaryOperationDate7 = TEncounter.SecondaryOperationDate7 
	,SecondaryOperationCode8 = TEncounter.SecondaryOperationCode8 
	,SecondaryOperationDate8 = TEncounter.SecondaryOperationDate8 
	,SecondaryOperationCode9 = TEncounter.SecondaryOperationCode9 
	,SecondaryOperationDate9 = TEncounter.SecondaryOperationDate9 
	,SecondaryOperationCode10 = TEncounter.SecondaryOperationCode10 
	,SecondaryOperationDate10 = TEncounter.SecondaryOperationDate10 
	,SecondaryOperationCode11 = TEncounter.SecondaryOperationCode11 
	,SecondaryOperationDate11 = TEncounter.SecondaryOperationDate11 
	,OperationStatusCode = TEncounter.OperationStatusCode 
	,ContractSerialNo = TEncounter.ContractSerialNo 
	,CodingCompleteFlag = TEncounter.CodingCompleteFlag 
	,PurchaserCode = TEncounter.PurchaserCode 
	,ProviderCode = TEncounter.ProviderCode 
	,EpisodeStartTime = TEncounter.EpisodeStartTime 
	,EpisodeEndTime = TEncounter.EpisodeEndTime 
	,RegisteredGdpCode = TEncounter.RegisteredGdpCode 
	,EpisodicGpCode = TEncounter.EpisodicGpCode 
	,InterfaceCode = TEncounter.InterfaceCode 
	,CasenoteNumber = TEncounter.CasenoteNumber 
	,NHSNumberStatusCode = TEncounter.NHSNumberStatusCode 
	,AdmissionTime = TEncounter.AdmissionTime 
	,DischargeTime = TEncounter.DischargeTime 
	,TransferFrom = TEncounter.TransferFrom 
	,DistrictNo = TEncounter.DistrictNo 
	,SourceUniqueID = TEncounter.SourceUniqueID 
	,ExpectedLOS = TEncounter.ExpectedLOS 
	,MRSAFlag = TEncounter.MRSAFlag 
	,RTTPathwayID = TEncounter.RTTPathwayID 
	,RTTPathwayCondition = TEncounter.RTTPathwayCondition 
	,RTTStartDate = TEncounter.RTTStartDate 
	,RTTEndDate = TEncounter.RTTEndDate 
	,RTTSpecialtyCode = TEncounter.RTTSpecialtyCode 
	,RTTCurrentProviderCode = TEncounter.RTTCurrentProviderCode 
	,RTTCurrentStatusCode = TEncounter.RTTCurrentStatusCode 
	,RTTCurrentStatusDate = TEncounter.RTTCurrentStatusDate 
	,RTTCurrentPrivatePatientFlag = TEncounter.RTTCurrentPrivatePatientFlag 
	,RTTOverseasStatusFlag = TEncounter.RTTOverseasStatusFlag 
	,RTTPeriodStatusCode = TEncounter.RTTPeriodStatusCode 
	,OverseasStatusFlag = TEncounter.OverseasStatusFlag 
	,PASCreated = TEncounter.PASCreated 
	,PASUpdated = TEncounter.PASUpdated 
	,PASCreatedByWhom = TEncounter.PASCreatedByWhom 
	,PASUpdatedByWhom = TEncounter.PASUpdatedByWhom 
	,EpisodeOutcomeCode = TEncounter.EpisodeOutcomeCode
	,LegalStatusClassificationOnAdmissionCode = TEncounter.LegalStatusClassificationOnAdmissionCode
	,LegalStatusClassificationOnDischargeCode = TEncounter.LegalStatusClassificationOnDischargeCode
	,DecisionToAdmitDate = TEncounter.DecisionToAdmitDate
	,OriginalDecisionToAdmitDate = TEncounter.OriginalDecisionToAdmitDate
	,DurationOfElectiveWait = TEncounter.DurationOfElectiveWait

	,Updated = getdate()
	,ByWhom = system_user
	,EpisodicGpPracticeCode =  TEncounter.EpisodicGpPracticeCode --CM 06/01/2016 - added this field in the load as we can now populate this from the view.  Previously this field was updated via a post load update statement in LoadPatientGPPracticeHistory
from
	dbo.TLoadAPCEncounter TEncounter
where
	TEncounter.SourceUniqueID = APC.Encounter.SourceUniqueID

select @RowsUpdated = @@rowcount


INSERT INTO APC.Encounter
	(
	 SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,ReferralSourceUniqueID
	,WaitingListSourceUniqueID
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,DischargeDate
	,EpisodeStartDate
	,EpisodeEndDate
	,StartSiteCode
	,StartWardTypeCode
	,EndSiteCode
	,EndWardTypeCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,AdminCategoryCode
	,ConsultantCode
	,SpecialtyCode
	,LastEpisodeInSpellIndicator
	,FirstRegDayOrNightAdmit
	,NeonatalLevelOfCare
	,PASHRGCode
	,PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,PrimaryOperationCode
	,PrimaryOperationDate
	,SecondaryOperationCode1
	,SecondaryOperationDate1
	,SecondaryOperationCode2
	,SecondaryOperationDate2
	,SecondaryOperationCode3
	,SecondaryOperationDate3
	,SecondaryOperationCode4
	,SecondaryOperationDate4
	,SecondaryOperationCode5
	,SecondaryOperationDate5
	,SecondaryOperationCode6
	,SecondaryOperationDate6
	,SecondaryOperationCode7
	,SecondaryOperationDate7
	,SecondaryOperationCode8
	,SecondaryOperationDate8
	,SecondaryOperationCode9
	,SecondaryOperationDate9
	,SecondaryOperationCode10
	,SecondaryOperationDate10
	,SecondaryOperationCode11
	,SecondaryOperationDate11
	,OperationStatusCode
	,ContractSerialNo
	,CodingCompleteFlag
	,PurchaserCode
	,ProviderCode
	,EpisodeStartTime
	,EpisodeEndTime
	,RegisteredGdpCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode  
	,InterfaceCode
	,CasenoteNumber
	,NHSNumberStatusCode
	,AdmissionTime
	,DischargeTime
	,TransferFrom
	,DistrictNo
	,SourceUniqueID
	,ExpectedLOS
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,OverseasStatusFlag
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,EpisodeOutcomeCode
	,LegalStatusClassificationOnAdmissionCode
	,LegalStatusClassificationOnDischargeCode
	,DecisionToAdmitDate
	,OriginalDecisionToAdmitDate
	,DurationOfElectiveWait
	,Created
	,ByWhom
	) 
select
	 SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,ReferralSourceUniqueID
	,WaitingListSourceUniqueID
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,DischargeDate
	,EpisodeStartDate
	,EpisodeEndDate
	,StartSiteCode
	,StartWardTypeCode
	,EndSiteCode
	,EndWardTypeCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,AdminCategoryCode
	,ConsultantCode
	,SpecialtyCode
	,LastEpisodeInSpellIndicator
	,FirstRegDayOrNightAdmit
	,NeonatalLevelOfCare
	,PASHRGCode
	,PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,PrimaryOperationCode
	,PrimaryOperationDate
	,SecondaryOperationCode1
	,SecondaryOperationDate1
	,SecondaryOperationCode2
	,SecondaryOperationDate2
	,SecondaryOperationCode3
	,SecondaryOperationDate3
	,SecondaryOperationCode4
	,SecondaryOperationDate4
	,SecondaryOperationCode5
	,SecondaryOperationDate5
	,SecondaryOperationCode6
	,SecondaryOperationDate6
	,SecondaryOperationCode7
	,SecondaryOperationDate7
	,SecondaryOperationCode8
	,SecondaryOperationDate8
	,SecondaryOperationCode9
	,SecondaryOperationDate9
	,SecondaryOperationCode10
	,SecondaryOperationDate10
	,SecondaryOperationCode11
	,SecondaryOperationDate11
	,OperationStatusCode
	,ContractSerialNo
	,CodingCompleteFlag
	,PurchaserCode
	,ProviderCode
	,EpisodeStartTime
	,EpisodeEndTime
	,RegisteredGdpCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode 
	,InterfaceCode
	,CasenoteNumber
	,NHSNumberStatusCode
	,AdmissionTime
	,DischargeTime
	,TransferFrom
	,DistrictNo
	,SourceUniqueID
	,ExpectedLOS
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,OverseasStatusFlag
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,EpisodeOutcomeCode
	,LegalStatusClassificationOnAdmissionCode
	,LegalStatusClassificationOnDischargeCode
	,DecisionToAdmitDate
	,OriginalDecisionToAdmitDate
	,DurationOfElectiveWait
	,Created = getdate()
	,ByWhom = system_user
from
	dbo.TLoadAPCEncounter
where
	not exists
	(
	select
		1
	from
		APC.Encounter
	where
		Encounter.SourceUniqueID = TLoadAPCEncounter.SourceUniqueID
	)
and	TLoadAPCEncounter.ArchiveFlag = 0


SELECT @RowsInserted = @@Rowcount

update
	APC.Encounter
set
	EpisodeNo = 
		Episode.EpisodeNo
from
	(
	select
		EncounterRecno

		,EpisodeNo = 
			ROW_NUMBER() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartTime)
	from
		APC.Encounter
	) Episode
where
	Episode.EncounterRecno = Encounter.EncounterRecno


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Updated '  + CONVERT(varchar(10), @RowsUpdated) +  
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH LoadAPCEncounter', @Stats, @StartTime

print @Stats

