﻿CREATE procedure [dbo].[LoadAPCMaternitySpell] 
as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select @StartTime = getdate()


--delete archived activity
delete from APC.MaternitySpell 
where	
	exists
	(
	select
		1
	from
		dbo.TLoadAPCMaternitySpell
	where
		TLoadAPCMaternitySpell.SourceUniqueID = MaternitySpell.SourceUniqueID
	and	TLoadAPCMaternitySpell.ArchiveFlag = 1
	)


SELECT @RowsDeleted = @@Rowcount


update
	APC.MaternitySpell
set
	 EpisodeSourceUniqueID = TImport.EpisodeSourceUniqueID
	,SourcePatientNo = TImport.SourcePatientNo
	,SourceSpellNo = TImport.SourceSpellNo
	,ReferralSourceUniqueID = TImport.ReferralSourceUniqueID
	,ProfessionalCarerCode = TImport.ProfessionalCarerCode
	,ServicePointStayCode = TImport.ServicePointStayCode
	,ServicePointCode = TImport.ServicePointCode
	,DeliveryPlaceCode = TImport.DeliveryPlaceCode
	,DeliveryPlaceChangeReasonCode = TImport.DeliveryPlaceChangeReasonCode
	,PregnancyNumber = TImport.PregnancyNumber
	,Parity = TImport.Parity
	,FirstAnteNatalAssessmentDate = TImport.FirstAnteNatalAssessmentDate
	,MaternalHeight = TImport.MaternalHeight
	,NumberOfPreviousCaesareanDeliveries = TImport.NumberOfPreviousCaesareanDeliveries
	,NumberOfPreviousInducedAbortions = TImport.NumberOfPreviousInducedAbortions
	,TotalLiveBirths = TImport.TotalLiveBirths
	,TotalStillBirths = TImport.TotalStillBirths
	,TotalNeonatalDeaths = TImport.TotalNeonatalDeaths
	,PreviousPregnancies = TImport.PreviousPregnancies
	,NumberOfPreviousNonInducedAbortions = TImport.NumberOfPreviousNonInducedAbortions
	,StartTime = TImport.StartTime
	,EndTime = TImport.EndTime
	,PreviousBloodTransfusions = TImport.PreviousBloodTransfusions
	,RubellaImmunity = TImport.RubellaImmunity
	,RubellaTested = TImport.RubellaTested
	,RubellaPositive = TImport.RubellaPositive
	,RubellaImmunised = TImport.RubellaImmunised
	,DeliveryTime = TImport.DeliveryTime
	,LabourOnsetMethodCode = TImport.LabourOnsetMethodCode
	,StatusOfPersonConductingDeliveryCode = TImport.StatusOfPersonConductingDeliveryCode
	,GestationLength = TImport.GestationLength
	,FirstStageOfPregnancyWeeks = TImport.FirstStageOfPregnancyWeeks
	,SecondStageOfPregnancyWeeks = TImport.SecondStageOfPregnancyWeeks
	,NumberOfBabies = TImport.NumberOfBabies
	,AnaestheticGivenDuringCode = TImport.AnaestheticGivenDuringCode
	,AnaestheticGivenPostCode = TImport.AnaestheticGivenPostCode
	,Stage1Time = TImport.Stage1Time
	,Stage2Time = TImport.Stage2Time
	,IntendedDeliveryPlaceTypeCode = TImport.IntendedDeliveryPlaceTypeCode
	,DelivererCode = TImport.DelivererCode
	,AdditionalDelivererCode = TImport.AdditionalDelivererCode
	,PriorProfessionalCarer = TImport.PriorProfessionalCarer
	,AnaestheticGivenDuringReasonCode = TImport.AnaestheticGivenDuringReasonCode
	,AnaestheticGivenPostReasonCode = TImport.AnaestheticGivenPostReasonCode
	,MaternityPeriodCode = TImport.MaternityPeriodCode
	,AnaestheticReasonCode = TImport.AnaestheticReasonCode
	,SelfAdministeredInhalationCode = TImport.SelfAdministeredInhalationCode
	,NarcoticsCode = TImport.NarcoticsCode
	,EpiduralOrCaudalCode = TImport.EpiduralOrCaudalCode
	,SpinalCode = TImport.SpinalCode
	,LocationInfiltrationCode = TImport.LocationInfiltrationCode
	,GeneralCode = TImport.GeneralCode
	,OtherCode = TImport.OtherCode
	,PreviousBloodTransfusionComments = TImport.PreviousBloodTransfusionComments
	,PASCreated = TImport.PASCreated
	,PASUpdated = TImport.PASUpdated
	,PASCreatedByWhom = TImport.PASCreatedByWhom
	,PASUpdatedByWhom = TImport.PASUpdatedByWhom

	,Updated = getdate()
	,ByWhom = system_user
from
	dbo.TLoadAPCMaternitySpell TImport
where
	APC.MaternitySpell.SourceUniqueID = TImport.SourceUniqueID


select @RowsUpdated = @@rowcount


insert into APC.MaternitySpell
(
	 SourceUniqueID
	,EpisodeSourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,ReferralSourceUniqueID
	,ProfessionalCarerCode
	,ServicePointStayCode
	,ServicePointCode
	,DeliveryPlaceCode
	,DeliveryPlaceChangeReasonCode
	,PregnancyNumber
	,Parity
	,FirstAnteNatalAssessmentDate
	,MaternalHeight
	,NumberOfPreviousCaesareanDeliveries
	,NumberOfPreviousInducedAbortions
	,TotalLiveBirths
	,TotalStillBirths
	,TotalNeonatalDeaths
	,PreviousPregnancies
	,NumberOfPreviousNonInducedAbortions
	,StartTime
	,EndTime
	,PreviousBloodTransfusions
	,RubellaImmunity
	,RubellaTested
	,RubellaPositive
	,RubellaImmunised
	,DeliveryTime
	,LabourOnsetMethodCode
	,StatusOfPersonConductingDeliveryCode
	,GestationLength
	,FirstStageOfPregnancyWeeks
	,SecondStageOfPregnancyWeeks
	,NumberOfBabies
	,AnaestheticGivenDuringCode
	,AnaestheticGivenPostCode
	,Stage1Time
	,Stage2Time
	,IntendedDeliveryPlaceTypeCode
	,DelivererCode
	,AdditionalDelivererCode
	,PriorProfessionalCarer
	,AnaestheticGivenDuringReasonCode
	,AnaestheticGivenPostReasonCode
	,MaternityPeriodCode
	,AnaestheticReasonCode
	,SelfAdministeredInhalationCode
	,NarcoticsCode
	,EpiduralOrCaudalCode
	,SpinalCode
	,LocationInfiltrationCode
	,GeneralCode
	,OtherCode
	,PreviousBloodTransfusionComments
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,Created
	,ByWhom
)
select
	 SourceUniqueID
	,EpisodeSourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,ReferralSourceUniqueID
	,ProfessionalCarerCode
	,ServicePointStayCode
	,ServicePointCode
	,DeliveryPlaceCode
	,DeliveryPlaceChangeReasonCode
	,PregnancyNumber
	,Parity
	,FirstAnteNatalAssessmentDate
	,MaternalHeight
	,NumberOfPreviousCaesareanDeliveries
	,NumberOfPreviousInducedAbortions
	,TotalLiveBirths
	,TotalStillBirths
	,TotalNeonatalDeaths
	,PreviousPregnancies
	,NumberOfPreviousNonInducedAbortions
	,StartTime
	,EndTime
	,PreviousBloodTransfusions
	,RubellaImmunity
	,RubellaTested
	,RubellaPositive
	,RubellaImmunised
	,DeliveryTime
	,LabourOnsetMethodCode
	,StatusOfPersonConductingDeliveryCode
	,GestationLength
	,FirstStageOfPregnancyWeeks
	,SecondStageOfPregnancyWeeks
	,NumberOfBabies
	,AnaestheticGivenDuringCode
	,AnaestheticGivenPostCode
	,Stage1Time
	,Stage2Time
	,IntendedDeliveryPlaceTypeCode
	,DelivererCode
	,AdditionalDelivererCode
	,PriorProfessionalCarer
	,AnaestheticGivenDuringReasonCode
	,AnaestheticGivenPostReasonCode
	,MaternityPeriodCode
	,AnaestheticReasonCode
	,SelfAdministeredInhalationCode
	,NarcoticsCode
	,EpiduralOrCaudalCode
	,SpinalCode
	,LocationInfiltrationCode
	,GeneralCode
	,OtherCode
	,PreviousBloodTransfusionComments
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom

	,Created = getdate()
	,ByWhom = system_user
from
	dbo.TLoadAPCMaternitySpell TImport
where
	not exists
	(
	select
		1
	from
		APC.MaternitySpell
	where
		MaternitySpell.SourceUniqueID = TImport.SourceUniqueID
	)
and	TImport.ArchiveFlag = 0


SELECT @RowsInserted = @@Rowcount


SELECT @Elapsed = DATEDIFF(minute, @StartTime,getdate())

SELECT @Stats = 
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Updated '  + CONVERT(varchar(10), @RowsUpdated) +  
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH LoadAPCMaternitySpell', @Stats, @StartTime

print @Stats

