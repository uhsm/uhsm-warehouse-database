﻿CREATE Procedure [dbo].[LoadAPCPBR] AS



--drop Table APC.PBRSpell


/*
Create Table APC.PBRSpell (
	 HRGEncounterRecNo int identity(1,1)
	,SourceSpellNo int null
	,SpellHRGCode varchar(20) null
	,SpellSSCCode varchar(3) null
	,AdjustedExcessBedDays int null
	,FinalExcessBedDays int null
	,POD2 varchar(10) null
	,POD3 varchar(10) null
	,PBRSpellActivityIndicator bit
	--,PBRRowNo int
	,DateLoaded datetime
	,
	CONSTRAINT [PK_HRG] PRIMARY KEY CLUSTERED
		(HRGEncounterRecNo)
	,
	CONSTRAINT [PKHRG_SPELL] UNIQUE
		(SourceSpellNo)
	)
	
*/

----Needs updating for data refreshes

	
Insert into APC.PBRSpell
	(
	 SourceSpellNo
	,SpellHRGCode
	,SpellSSCCode
	,AdjustedExcessBedDays
	,FinalExcessBedDays
	,POD2
	,POD3
	,PBRSpellActivityIndicator
	--,PBRRowNo
	,DateLoaded
	)
	(
 Select distinct
	 SourceSpellNo = pbr.PROVSPNO
	,SpellHRGCode = pbr.SpellHRG
	,SpellSSCCode = 
			Case When charindex('/',HRG2) <=0 Then	
				'NA'
			else
				substring(hrg2,charindex('/',HRG2)+1,Len(HRG2)-charindex('/',HRG2))
			end
	,AdjustedExcessBedDays =  [ADJ XBD] 
	,FinalExcessBedDays = [ADJ XBD2] 
	,POD2
	,POD3
	,PBRSpellActivityIndicator =
		case when (PBR = 'PBR' and [SPELL EXCLUSION] = 'NA') Then CONVERT(bit,1) 
		else CONVERT(bit,0)
		end
	--,RowNo
	,GETDATE() as DateLoaded
	FROM TImportPBR pbr
	where 
	PBR  != 'UNB'
	)




/*-----------------------------------------------------------------------------------------
Unbundeled
-----------------------------------------------------------------------------------------*/

