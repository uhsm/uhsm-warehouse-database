﻿CREATE       procedure [dbo].[LoadAPCWL] 
	 @census smalldatetime = null
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

select
	@census = 
		coalesce(
			@census
			,dateadd(day, datediff(day, 0, getdate()), 0)
		)


truncate table TImportAPCWaitingList
exec ExtractLorenzoAPCWaitingList @census
exec LoadAPCWaitingList

truncate table TImportAPCWaitingListSuspension
exec ExtractLorenzoAPCWaitingListSuspension @census
exec LoadAPCWaitingListSuspension

--update theatre details
update
	APC.WaitingList
set
	 WaitingList.TheatrePatientBookingKey = TheatreSlot.TheatrePatientBookingKey
	,WaitingList.ProcedureTime = 
		case
		when TheatreSlot.ProcedureTime > '6 Jun 2079'
		then null
		else TheatreSlot.ProcedureTime
		end

	,WaitingList.TheatreCode = TheatreSlot.TheatreCode
from
	APC.WaitingList WL

inner join
	(
	select distinct
		 WL.EncounterRecno
		,TheatrePatientBookingKey = PatientBooking.SourceUniqueID
		,ProcedureTime = coalesce(PatientBooking.EstimatedStartTime, PatientBooking.IntendedProcedureDate)
		,TheatreCode = PatientBooking.TheatreCode
	from
		Theatre.PatientBooking PatientBooking

	inner join APC.WaitingList WL
	on	WL.DistrictNo = PatientBooking.DistrictNo
	and	PatientBooking.IntendedProcedureDate >= WL.CensusDate
	and	WL.CensusDate = @census
--	and	PatientBooking.CancelReasonCode is null
	and	not exists
		(
		select
			1
		from
			APC.WaitingList WL1
		where
			WL1.CensusDate = WL.CensusDate
		and	WL1.DistrictNo = WL.DistrictNo 
		and	WL1.DistrictNo in 
			(
			select
				DistrictNo
			from
				(
				select
					WLCount.DistrictNo,
					count(*) Value
				from
					APC.WaitingList WLCount
				where
					WLCount.CensusDate = WL1.CensusDate
				group by
					WLCount.DistrictNo
				having
					count(*) > 1
				) DistrictNumbers
			)
		)
	) TheatreSlot
on	TheatreSlot.EncounterRecno = WL.EncounterRecno

-- Clear down old snapshots
declare @CensusCutOffDate smalldatetime

select @CensusCutOffDate =
	dateadd(
		 day
		,(
		select
			NumericValue * -1
		from
			dbo.Parameter
		where
			Parameter = 'KEEPSNAPSHOTDAYS'
		)
		,@census
	)

select
	CensusDate = max(CensusDate) 
into
	#OldSnapshot
from
	APC.WaitingList 
where
	CensusDate < @CensusCutOffDate
group by
	 datepart(year, CensusDate)
	,datepart(month, CensusDate)

--retain the latest snapshot this week
insert into
	#OldSnapshot
select
	CensusDate = max(CensusDate) 
from
	APC.WaitingList 
where
	CensusDate < @CensusCutOffDate
group by
	 datepart(year, CensusDate)
	,datepart(wk, CensusDate)


delete from APC.WaitingList
where
	not exists
	(
	select
		1
	from
		#OldSnapshot OldSnapshot
	where
		OldSnapshot.CensusDate = WaitingList.CensusDate
	)
and	CensusDate < @CensusCutOffDate

delete from APC.WaitingListSuspension
where
	not exists
	(
	select
		1
	from
		#OldSnapshot OldSnapshot
	where
		OldSnapshot.CensusDate = WaitingListSuspension.CensusDate
	)
and	CensusDate < @CensusCutOffDate


-- reset snapshot table
delete from APC.Snapshot

insert into APC.Snapshot 
	(
	CensusDate
	)
select distinct 
	CensusDate
from 
	APC.WaitingList

--work out derived breach dates etc.
exec BuildAPCWaitingListBreach @census

update dbo.Parameter
set
	DateValue = @StartTime
where
	Parameter = 'EXTRACTAPCWLDATE'

if @@rowcount = 0

insert into dbo.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'EXTRACTAPCWLDATE'
	,DateValue = @StartTime


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Census ' + 
	CONVERT(varchar(11), coalesce(@census, ''))

exec WriteAuditLogEvent 'PAS - WH LoadAPCWL', @Stats, @StartTime
