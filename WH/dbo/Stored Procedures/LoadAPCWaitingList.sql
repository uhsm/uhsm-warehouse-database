﻿CREATE PROCEDURE [dbo].[LoadAPCWaitingList]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


--delete archived activity
delete from APC.WaitingList 
where	
	not exists
	(
	select
		1
	from
		dbo.TLoadAPCWaitingList
	where
		TLoadAPCWaitingList.SourceUniqueID = WaitingList.SourceUniqueID
	and	TLoadAPCWaitingList.CensusDate = WaitingList.CensusDate
	)

and	WaitingList.CensusDate =
	(
	select top 1
		TLoadAPCWaitingList.CensusDate
	from
		dbo.TLoadAPCWaitingList
	)


SELECT @RowsDeleted = @@Rowcount

update APC.WaitingList
set
	 SourcePatientNo = TEncounter.SourcePatientNo
	,SourceEncounterNo = TEncounter.SourceEncounterNo
	,ReferralSourceUniqueID = TEncounter.ReferralSourceUniqueID
	,PatientTitle = TEncounter.PatientTitle
	,PatientForename = TEncounter.PatientForename
	,PatientSurname = TEncounter.PatientSurname
	,DateOfBirth = TEncounter.DateOfBirth
	,DateOfDeath = TEncounter.DateOfDeath
	,SexCode = TEncounter.SexCode
	,NHSNumber = TEncounter.NHSNumber
	,DistrictNo = TEncounter.DistrictNo
	,Postcode = TEncounter.Postcode
	,PatientAddress1 = TEncounter.PatientAddress1
	,PatientAddress2 = TEncounter.PatientAddress2
	,PatientAddress3 = TEncounter.PatientAddress3
	,PatientAddress4 = TEncounter.PatientAddress4
	,DHACode = TEncounter.DHACode
	,HomePhone = TEncounter.HomePhone
	,WorkPhone = TEncounter.WorkPhone
	,EthnicOriginCode = TEncounter.EthnicOriginCode
	,MaritalStatusCode = TEncounter.MaritalStatusCode
	,ReligionCode = TEncounter.ReligionCode
	,ConsultantCode = TEncounter.ConsultantCode
	,SpecialtyCode = TEncounter.SpecialtyCode
	,PASSpecialtyCode = TEncounter.PASSpecialtyCode
	,ManagementIntentionCode = TEncounter.ManagementIntentionCode
	,AdmissionMethodCode = TEncounter.AdmissionMethodCode
	,PriorityCode = TEncounter.PriorityCode
	,WaitingListCode = TEncounter.WaitingListCode
	,CommentClinical = TEncounter.CommentClinical
	,CommentNonClinical = TEncounter.CommentNonClinical
	,IntendedPrimaryOperationCode = TEncounter.IntendedPrimaryOperationCode
	,Operation = TEncounter.Operation
	,SiteCode = TEncounter.SiteCode
	,WardCode = TEncounter.WardCode
	,WLStatus = TEncounter.WLStatus
	,PurchaserCode = TEncounter.PurchaserCode
	,ProviderCode = TEncounter.ProviderCode
	,ContractSerialNo = TEncounter.ContractSerialNo
	,AdminCategoryCode = TEncounter.AdminCategoryCode
	,CancelledBy = TEncounter.CancelledBy
	,BookingTypeCode = TEncounter.BookingTypeCode
	,CasenoteNumber = TEncounter.CasenoteNumber
	,OriginalDateOnWaitingList = TEncounter.OriginalDateOnWaitingList
	,DateOnWaitingList = TEncounter.DateOnWaitingList
	,TCIDate = TEncounter.TCIDate
	,KornerWait = TEncounter.KornerWait
	,CountOfDaysSuspended = TEncounter.CountOfDaysSuspended
	,SuspensionStartDate = TEncounter.SuspensionStartDate
	,SuspensionEndDate = TEncounter.SuspensionEndDate
	,SuspensionReasonCode = TEncounter.SuspensionReasonCode
	,SuspensionReason = TEncounter.SuspensionReason
	,InterfaceCode = TEncounter.InterfaceCode
	,RegisteredGpCode = TEncounter.RegisteredGpCode
	,RegisteredGpPracticeCode = TEncounter.RegisteredGpPracticeCode
	,EpisodicGpCode = TEncounter.EpisodicGpCode
	,EpisodicGpPracticeCode = TEncounter.EpisodicGpPracticeCode
	,SourceTreatmentFunctionCode = TEncounter.SourceTreatmentFunctionCode
	,TreatmentFunctionCode = TEncounter.TreatmentFunctionCode
	,NationalSpecialtyCode = TEncounter.NationalSpecialtyCode
	,PCTCode = TEncounter.PCTCode
	,BreachDate = TEncounter.BreachDate
	,ExpectedAdmissionDate = TEncounter.ExpectedAdmissionDate
	,ExpectedDischargeDate = TEncounter.ExpectedDischargeDate
	,ReferralDate = TEncounter.ReferralDate
	,FuturePatientCancelDate = TEncounter.FuturePatientCancelDate
	,FutureCancelDate = TEncounter.FutureCancelDate
	,ProcedureTime = TEncounter.ProcedureTime
	,TheatreCode = TEncounter.TheatreCode
	,AnaestheticTypeCode = TEncounter.AnaestheticTypeCode
	,AdmissionReason = TEncounter.AdmissionReason
	,EpisodeNo = TEncounter.EpisodeNo
	,BreachDays = TEncounter.BreachDays
	,MRSAFlag = TEncounter.MRSAFlag
	,RTTPathwayID = TEncounter.RTTPathwayID
	,RTTPathwayCondition = TEncounter.RTTPathwayCondition
	,RTTStartDate = TEncounter.RTTStartDate
	,RTTEndDate = TEncounter.RTTEndDate
	,RTTSpecialtyCode = TEncounter.RTTSpecialtyCode
	,RTTCurrentProviderCode = TEncounter.RTTCurrentProviderCode
	,RTTCurrentStatusCode = TEncounter.RTTCurrentStatusCode
	,RTTCurrentStatusDate = TEncounter.RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag = TEncounter.RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag = TEncounter.RTTOverseasStatusFlag
	,NationalBreachDate = TEncounter.NationalBreachDate
	,NationalBreachDays = TEncounter.NationalBreachDays
	,DerivedBreachDays = TEncounter.DerivedBreachDays
	,DerivedClockStartDate = TEncounter.DerivedClockStartDate
	,DerivedBreachDate = TEncounter.DerivedBreachDate
	,RTTBreachDate = TEncounter.RTTBreachDate
	,RTTDiagnosticBreachDate = TEncounter.RTTDiagnosticBreachDate
	,NationalDiagnosticBreachDate = TEncounter.NationalDiagnosticBreachDate
	,SocialSuspensionDays = TEncounter.SocialSuspensionDays
	,BreachTypeCode = TEncounter.BreachTypeCode
	,PASCreated = TEncounter.PASCreated
	,PASUpdated = TEncounter.PASUpdated
	,PASCreatedByWhom = TEncounter.PASCreatedByWhom
	,PASUpdatedByWhom = TEncounter.PASUpdatedByWhom
	,CancelReasonCode = TEncounter.CancelReasonCode
	,GeneralComment = TEncounter.GeneralComment
	,PatientPreparation = TEncounter.PatientPreparation
	,AdmissionOfferOutcomeCode = TEncounter.AdmissionOfferOutcomeCode
	,EstimatedTheatreTime = TEncounter.EstimatedTheatreTime
	,WhoCanOperateCode = TEncounter.WhoCanOperateCode
	,WaitingListRuleCode = TEncounter.WaitingListRuleCode
	,ShortNoticeCode = TEncounter.ShortNoticeCode
	,PreOpDate = TEncounter.PreOpDate
	,PreOpClinicCode = TEncounter.PreOpClinicCode
	,AdmissionOfferSourceUniqueID = TEncounter.AdmissionOfferSourceUniqueID
	,LocalCategoryCode = TEncounter.LocalCategoryCode

	,Updated = getdate()
	,ByWhom = system_user
from
	dbo.TLoadAPCWaitingList TEncounter
where
	TEncounter.SourceUniqueID = APC.WaitingList.SourceUniqueID
and	TEncounter.CensusDate = APC.WaitingList.CensusDate

select @RowsUpdated = @@rowcount


INSERT INTO APC.WaitingList
	(
	 CensusDate
	,SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,ReferralSourceUniqueID
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,ConsultantCode
	,SpecialtyCode
	,PASSpecialtyCode
	,ManagementIntentionCode
	,AdmissionMethodCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,IntendedPrimaryOperationCode
	,Operation
	,SiteCode
	,WardCode
	,WLStatus
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,AdminCategoryCode
	,CancelledBy
	,BookingTypeCode
	,CasenoteNumber
	,OriginalDateOnWaitingList
	,DateOnWaitingList
	,TCIDate
	,KornerWait
	,CountOfDaysSuspended
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,SuspensionReason
	,InterfaceCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,BreachDate
	,ExpectedAdmissionDate
	,ExpectedDischargeDate
	,ReferralDate
	,FuturePatientCancelDate
	,FutureCancelDate
	,ProcedureTime
	,TheatreCode
	,AnaestheticTypeCode
	,AdmissionReason
	,EpisodeNo
	,BreachDays
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NationalBreachDate
	,NationalBreachDays
	,DerivedBreachDays
	,DerivedClockStartDate
	,DerivedBreachDate
	,RTTBreachDate
	,RTTDiagnosticBreachDate
	,NationalDiagnosticBreachDate
	,SocialSuspensionDays
	,BreachTypeCode
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,CancelReasonCode
	,GeneralComment
	,PatientPreparation
	,AdmissionOfferOutcomeCode
	,EstimatedTheatreTime
	,WhoCanOperateCode
	,WaitingListRuleCode
	,ShortNoticeCode
	,PreOpDate
	,PreOpClinicCode
	,AdmissionOfferSourceUniqueID
	,LocalCategoryCode
	,Created
	,ByWhom
	) 
select
	 CensusDate
	,SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,ReferralSourceUniqueID
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,ConsultantCode
	,SpecialtyCode
	,PASSpecialtyCode
	,ManagementIntentionCode
	,AdmissionMethodCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,IntendedPrimaryOperationCode
	,Operation
	,SiteCode
	,WardCode
	,WLStatus
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,AdminCategoryCode
	,CancelledBy
	,BookingTypeCode
	,CasenoteNumber
	,OriginalDateOnWaitingList
	,DateOnWaitingList
	,TCIDate
	,KornerWait
	,CountOfDaysSuspended
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,SuspensionReason
	,InterfaceCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,BreachDate
	,ExpectedAdmissionDate
	,ExpectedDischargeDate
	,ReferralDate
	,FuturePatientCancelDate
	,FutureCancelDate
	,ProcedureTime
	,TheatreCode
	,AnaestheticTypeCode
	,AdmissionReason
	,EpisodeNo
	,BreachDays
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NationalBreachDate
	,NationalBreachDays
	,DerivedBreachDays
	,DerivedClockStartDate
	,DerivedBreachDate
	,RTTBreachDate
	,RTTDiagnosticBreachDate
	,NationalDiagnosticBreachDate
	,SocialSuspensionDays
	,BreachTypeCode
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,CancelReasonCode
	,GeneralComment
	,PatientPreparation
	,AdmissionOfferOutcomeCode
	,EstimatedTheatreTime
	,WhoCanOperateCode
	,WaitingListRuleCode
	,ShortNoticeCode
	,PreOpDate
	,PreOpClinicCode
	,AdmissionOfferSourceUniqueID
	,LocalCategoryCode

	,Created = getdate()
	,ByWhom = system_user
from
	dbo.TLoadAPCWaitingList
where
	not exists
	(
	select
		1
	from
		APC.WaitingList
	where
		WaitingList.SourceUniqueID = TLoadAPCWaitingList.SourceUniqueID
	and	WaitingList.CensusDate = TLoadAPCWaitingList.CensusDate
	)
and	TLoadAPCWaitingList.ArchiveFlag = 0


SELECT @RowsInserted = @@Rowcount



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Updated '  + CONVERT(varchar(10), @RowsUpdated) +  
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH LoadAPCWaitingList', @Stats, @StartTime

print @Stats

