﻿CREATE PROCEDURE [dbo].[LoadAPCWaitingListSuspension]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


--delete archived activity
delete from APC.WaitingListSuspension
where	
	exists
	(
	select
		1
	from
		dbo.TLoadAPCWaitingListSuspension
	where
		TLoadAPCWaitingListSuspension.SourceUniqueID = WaitingListSuspension.SourceUniqueID
	and	TLoadAPCWaitingListSuspension.CensusDate = WaitingListSuspension.CensusDate
	and	TLoadAPCWaitingListSuspension.ArchiveFlag = 1
	)


SELECT @RowsDeleted = @@Rowcount

update APC.WaitingListSuspension
set
	 WaitingListSourceUniqueID = TEncounter.WaitingListSourceUniqueID
	,SuspensionStartDate = TEncounter.SuspensionStartDate
	,SuspensionEndDate = TEncounter.SuspensionEndDate
	,SuspensionReasonCode = TEncounter.SuspensionReasonCode
	,PASCreated = TEncounter.PASCreated
	,PASUpdated = TEncounter.PASUpdated
	,PASCreatedByWhom = TEncounter.PASCreatedByWhom
	,PASUpdatedByWhom = TEncounter.PASUpdatedByWhom

	,Updated = getdate()
	,ByWhom = system_user
from
	dbo.TLoadAPCWaitingListSuspension TEncounter
where
	TEncounter.SourceUniqueID = APC.WaitingListSuspension.SourceUniqueID
and	TEncounter.CensusDate = APC.WaitingListSuspension.CensusDate

select @RowsUpdated = @@rowcount


INSERT INTO APC.WaitingListSuspension
	(
	 CensusDate
	,SourceUniqueID
	,WaitingListSourceUniqueID
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,Created
	,ByWhom
	) 
select
	 CensusDate
	,SourceUniqueID
	,WaitingListSourceUniqueID
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom

	,Created = getdate()
	,ByWhom = system_user
from
	dbo.TLoadAPCWaitingListSuspension
where
	not exists
	(
	select
		1
	from
		APC.WaitingListSuspension
	where
		WaitingListSuspension.SourceUniqueID = TLoadAPCWaitingListSuspension.SourceUniqueID
	and	WaitingListSuspension.CensusDate = TLoadAPCWaitingListSuspension.CensusDate
	)
and	TLoadAPCWaitingListSuspension.ArchiveFlag = 0


SELECT @RowsInserted = @@Rowcount



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Updated '  + CONVERT(varchar(10), @RowsUpdated) +  
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH LoadAPCWaitingListSuspension', @Stats, @StartTime

print @Stats
