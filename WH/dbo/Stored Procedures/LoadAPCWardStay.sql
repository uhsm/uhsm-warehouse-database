﻿CREATE procedure [dbo].[LoadAPCWardStay] 
as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select @StartTime = getdate()


--delete archived activity
delete from APC.WardStay 
where	
	exists
	(
	select
		1
	from
		dbo.TLoadAPCWardStay
	where
		TLoadAPCWardStay.SourceUniqueID = WardStay.SourceUniqueID
	and	TLoadAPCWardStay.ArchiveFlag = 1
	)


SELECT @RowsDeleted = @@Rowcount


update
	APC.WardStay
set
	 SourceSpellNo = TEncounter.SourceSpellNo
	,SourcePatientNo = TEncounter.SourcePatientNo
	,StartTime = TEncounter.StartTime
	,EndTime = TEncounter.EndTime
	,WardCode = TEncounter.WardCode
	,ProvisionalFlag = TEncounter.ProvisionalFlag
	,PASCreated = TEncounter.PASCreated
	,PASUpdated = TEncounter.PASUpdated
	,PASCreatedByWhom = TEncounter.PASCreatedByWhom
	,PASUpdatedByWhom = TEncounter.PASUpdatedByWhom

	,Updated = getdate()
	,ByWhom = system_user
from
	dbo.TLoadAPCWardStay TEncounter
where
	APC.WardStay.SourceUniqueID = TEncounter.SourceUniqueID


select @RowsUpdated = @@rowcount


insert into APC.WardStay
(
	 SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo
	,StartTime
	,EndTime
	,WardCode
	,ProvisionalFlag
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,Created
	,ByWhom
)
select
	 SourceUniqueID = TEncounter.SourceUniqueID
	,SourceSpellNo = TEncounter.SourceSpellNo
	,SourcePatientNo = TEncounter.SourcePatientNo
	,StartTime = TEncounter.StartTime
	,EndTime = TEncounter.EndTime
	,WardCode = TEncounter.WardCode
	,ProvisionalFlag = TEncounter.ProvisionalFlag
	,PASCreated = TEncounter.PASCreated
	,PASUpdated = TEncounter.PASUpdated
	,PASCreatedByWhom = TEncounter.PASCreatedByWhom
	,PASUpdatedByWhom = TEncounter.PASUpdatedByWhom

	,Created = getdate()
	,ByWhom = system_user
from
	dbo.TLoadAPCWardStay TEncounter
where
	not exists
	(
	select
		1
	from
		APC.WardStay
	where
		WardStay.SourceUniqueID = TEncounter.SourceUniqueID
	)
and	TEncounter.ArchiveFlag = 0


SELECT @RowsInserted = @@Rowcount

--KO added 08/05/2013
--A couple of Ward Stays still appearing when spell has been archived
--delete from APC.WardStay where SourceSpellNo in (
--select PRVSP_REFNO from Lorenzo.dbo.ProviderSpell where ARCHV_FLAG = 'Y')

delete from APC.WardStay where SourceSpellNo not in (
select SourceSpellNo from APC.Encounter)

SELECT @RowsDeleted = @RowsDeleted + @@Rowcount

SELECT @Elapsed = DATEDIFF(minute, @StartTime,getdate())

SELECT @Stats = 
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Updated '  + CONVERT(varchar(10), @RowsUpdated) +  
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH LoadAPCWardStay', @Stats, @StartTime

print @Stats

