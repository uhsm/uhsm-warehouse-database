﻿CREATE procedure [dbo].[LoadASTrace] as

insert into Audit.ASTrace
(
	 RowNumber
	,EventClass
	,EventSubclass
	,TextData
	,ConnectionID
	,NTUserName
	,ApplicationName
	,StartTime
	,CurrentTime
	,Duration
	,DatabaseName
	,Error
	,ClientProcessID
	,SPID
	,CPUTime
	,NTDomainName
	,BinaryData
)
select
	 RowNumber
	,EventClass
	,EventSubclass
	,TextData
	,ConnectionID
	,NTUserName
	,ApplicationName
	,StartTime = coalesce(ASTraceCurrent.StartTime, getdate())
	,CurrentTime
	,Duration
	,DatabaseName
	,Error
	,ClientProcessID
	,SPID
	,CPUTime
	,NTDomainName
	,BinaryData
from
	dbo.ASTraceCurrent
where
	not exists
	(
	select
		1
	from
		Audit.ASTrace
	where
		ASTrace.RowNumber = ASTraceCurrent.RowNumber
	and	ASTrace.StartTime = coalesce(ASTraceCurrent.StartTime, getdate())
	)
and	ASTraceCurrent.EventClass <> 65528 --trace start event


