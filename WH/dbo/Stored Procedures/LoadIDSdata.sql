﻿CREATE procedure [dbo].[LoadIDSdata]
as

-- =============================================
-- Author:		CMan
-- Create date: 11/02/2014
-- Description:	Stored Procedure to take the Inpatient Discharge Summaries data from the 'ECC.dbo.tblECC_docs' on the v-uhsm-epr linked server and load into the table in WH
-- =============================================




declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0



truncate table IDS.ECCDischargeSummaries

INSERT INTO IDS.ECCDischargeSummaries
            ([Id],
             [Date_of_Admission],
             [LPI_discharge_datetime],
             [discharge_date],
             [discharge_time],
             [external_spell],
             [RM2Number],
             [NHSNumber],
             [Patient_Surname],
             [Patient_Forename],
             [Patient_DOB],
             [lnkpid],
             [lnkspell],
             [SectionCnt],
             [Reg_GP_code],
             [GP_Name],
             [Practice_code],
             [Referral_code],
             [Clinician_Code],
             [Clinician_Name],
             [Consultant_Code],
             [Consultant_Name],
             [Consultant_Speciality],
             [Discharge_Disposition],
             [Discharge_Location],
             [Discharge_Ward],
             [DischargeLounge],
             [Report_Code],
             [Status],
             [Doc_Created],
             [Doc_last_changed],
             [IDS_complete],
             [Drugs_verified],
             [Drugs_verified_datetime],
             [Print_date],
             [Print_time],
             [Doc_type],
             [ECC_Checked],
             [ECC_CheckedDateTime],
             [Last_Update],
             [Doc_status],
             [EDTHub_DocId],
             [EDT_status],
             [EDTHub_RejectType],
             [EDTHub_RejectReason],
             [EDTHub_Status_LastUpdated],
             [DocInDb])
SELECT [Id],
       [Date_of_Admission],
       [LPI_discharge_datetime],
       [discharge_date],
       [discharge_time],
       [external_spell],
       [RM2Number],
       [NHSNumber],
       [Patient_Surname],
       [Patient_Forename],
       [Patient_DOB],
       [lnkpid],
       [lnkspell],
       [SectionCnt],
       [Reg_GP_code],
       [GP_Name],
       [Practice_code],
       [Referral_code],
       [Clinician_Code],
       [Clinician_Name],
       [Consultant_Code],
       [Consultant_Name],
       [Consultant_Speciality],
       [Discharge_Disposition],
       [Discharge_Location],
       [Discharge_Ward],
       [DischargeLounge],
       [Report_Code],
       [Status],
       [Doc_Created],
       [Doc_last_changed],
       [IDS_complete],
       [Drugs_verified],
       [Drugs_verified_datetime],
       [Print_date],
       [Print_time],
       [Doc_type],
       [ECC_Checked],
       [ECC_CheckedDateTime],
       [Last_Update],
       [Doc_status],
       [EDTHub_DocId],
       [EDT_status],
       [EDTHub_RejectType],
       [EDTHub_RejectReason],
       [EDTHub_Status_LastUpdated],
       [DocInDb]
FROM   [v-uhsm-epr].ECC.dbo.tblECC_docs 

select @RowsInserted = @@rowcount


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadIDSdata', @Stats, @StartTime

print @Stats

