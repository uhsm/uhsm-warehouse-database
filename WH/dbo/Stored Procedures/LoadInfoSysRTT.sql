﻿


CREATE Procedure [dbo].[LoadInfoSysRTT] as 

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


IF Exists (Select * from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA  = 'INFOSYS' and TABLE_NAME = 'RTTDatasetSnapshot')
BEGIN
	DROP Table INFOSYS.RTTDatasetSnapshot
END
Select Distinct
isrtt.ref_ref,
isrtt.Ref_date,
isrtt.ClockStopType,
isrtt.ClockStopDate,
isrtt.DQAction,
isrtt.OrigRefDate,
isrtt.ElecAdmitSpellRef
into INFOSYS.RTTDatasetSnapshot
From infosql.rtt_18_weeks.dbo.rtt_Dataset isrtt



Create Clustered Index ixRef on INFOSYS.RTTDatasetSnapShot(Ref_Ref)
Create Index ixDQAction on INFOSYS.RTTDatasetSnapShot(DQACtion)
Create Index ixClockStopType on INFOSYS.RTTDatasetSnapShot(ClockStopType)

SELECT @RowsInserted = @@Rowcount


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'ImportINFOSYSRTT', @Stats, @StartTime;

--2011-10-24 Some Duplicates (17) in RTTDataset need investigating but need removing here so loading
--can continue
Delete From INFOSYS.RTTDatasetSnapshot
Where Ref_Ref in (
Select Ref_Ref
from INFOSYS.RTTDatasetSnapshot
Group By Ref_Ref
having count(*) >1
)

