﻿CREATE Procedure [dbo].[LoadLorenzoUser] as

If exists 
	(
	select * 
	from INFORMATION_SCHEMA.TABLES 
	Where TABLE_SCHEMA = 'PAS' 
	and TABLE_NAME = 'LorenzoUser'
	)
Truncate table PAS.LorenzoUser

--Insert Records from User Table

insert into PAS.LorenzoUser (
	 UserRefno
	,UserUniqueID
	,UserName
	,UserForename
	,UserSurname
	)
Select
	 USERS_REFNO
	,UserUniqueID
	,UserName
	,UserForename
	,UserSurname
From 
Lorenzo.dbo.ExtractLorenzoUser


--Insert Remaining Users where they don't exist in the iPM Users table


insert into PAS.LorenzoUser (
	 UserRefno
	,UserUniqueID
	,UserName
	,UserForename
	,UserSurname
	)

Select Distinct
	 UserRefno = '999999'
	,UserUniqueID = UsID
	,Username = UsID
	,UserForename = UsID
	,UserSurname = usID
		
From 
(
select
UsID = PASCreatedByWhom
From APC.Encounter
union
select
UsID = PASCreatedByWhom
From APC.WardStay
union
select
UsID = PASUpdatedByWhom
From APC.WardStay
union
select
UsID = PASCreatedByWhom
From OP.Encounter
union
select
UsID = PASCreatedByWhom
From APC.WaitingList
union
select
UsID = PASCreatedByWhom
From PAS.ClinicalCoding
union
select
UsID = PASCreatedByWhom
From OP.WaitingList
union
select
UsID = PASCreatedByWhom
From OP.Encounter
------------
union
select
UsID = PASUpdatedByWhom
From APC.Encounter
union
select
UsID = PASUpdatedByWhom
From OP.Encounter
union
select
UsID = PASUpdatedByWhom
From APC.WaitingList
union
select
UsID = PASUpdatedByWhom
From PAS.ClinicalCoding
union
select
UsID = PASUpdatedByWhom
From OP.WaitingList
union
select
UsID = PASUpdatedByWhom
From OP.Encounter



) UserBase
Where Not Exists
	(Select * from PAS.LorenzoUser lus
	Where UserBAse.UsID = lus.UserUniqueID)