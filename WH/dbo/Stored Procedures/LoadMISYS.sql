﻿CREATE       procedure [dbo].[LoadMISYS] 
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

declare @from smalldatetime

select @StartTime = getdate()

--MISYS data is processed through a SSIS package as linked server does not work against the Cache ODBC driver
--so this procedure is called from the package after the data is imported into SQL

exec LoadMISYSEncounter

exec Pseudonomisation.dbo.PseudonomiseMisys


--now split into normalised structure
--this may be extended to things other than orders, over time, depending on the reporting requirement.

select
	@from = min(TLoadMISYSEncounter.PerformanceDate)
from
	dbo.TLoadMISYSEncounter

exec LoadMISYSOrder @from


--this is a simpler extract without duplicates (in theory)

exec LoadMISYSPerformed



--Added 27-03-2011 KD 
--This gets the questionnaire data

exec [dbo].[LoadMISYSQuestionnaire]

--Added 27-03-2011 KD 
--This gets Cancelled Order Numbers

exec [dbo].[LoadMISYSCancelledOrder]

--Added 31-03-2011 KD 
--This gets Deleted Schedules

exec [dbo].[LoadMISYSDeletedSchedule]

--Added 05-05-2011 KD 
--This gets Deleted Schedules

Exec [dbo].[LoadMISYSOrderHistoryQuestionnaire]

----Added 05-09-2012 KO
----This gets Antenatal Scan data

--Exec [dbo].[LoadMISYSAntenatal]

update dbo.Parameter
set
	DateValue = @StartTime
where
	Parameter = 'EXTRACTMISYSDATE'

if @@rowcount = 0

insert into dbo.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'EXTRACTMISYSDATE'
	,DateValue = @StartTime


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'LoadMISYS', @Stats, @StartTime
