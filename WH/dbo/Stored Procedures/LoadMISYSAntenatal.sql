﻿
CREATE PROCEDURE [dbo].[LoadMISYSAntenatal]

as

/*
--Author: K Oakden
--Date created: 05/09/2012
--LoadMISYSAntenatal loads Misys Antenatal Data from staging table 
*/
set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


--delete activity
delete from MISYS.Antenatal 
SELECT @RowsDeleted = @@Rowcount


INSERT INTO MISYS.Antenatal
	(
	OrderNumber
	,PerformanceDate
	,PatientNo
	,NHSNumber
	,CGMASKV
	,LMPCheck
	,SuspensionDaysCode
	,RequestReceivedDate
	,EDD
	,DelayedByPatient
	,FailedPatientContact
	,PlannedRequest
	,Comment
	,ScreeningTime
	,Contrast
	,Complete
	,TooEarly
	,TooLate
	,Viable
	,Possible
	,AuditableAnomoly
	,ExamCode
	,Created
	,ByWhom
	) 
select
	order_number
	,PerformanceDate
	,PatientNo
	,SocialSecurityNo
	,CG_MAS_KV
	,LMP_CHECK
	,SuspensionDaysCode
	,RequestReceivedDate
	,EDD
	,DelayedByPatient
	,FailedPatientContact
	,PlannedRequest
	,Comment
	,SCREENING_TIME
	,CONTRAST
	,COMPLETE
	,TOO_EARLY
	,TOO_LATE
	,VIABLE
	,POSSIBLE
	,AUDITABLE_ANOMOLY
	,exam_code_no_mh
	,getdate()
	,system_user
from
	[dbo].[TLoadMISYSAntenatal]
	
SELECT @RowsInserted = @@Rowcount

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadMISYSAntenatal', @Stats, @StartTime

print @Stats

