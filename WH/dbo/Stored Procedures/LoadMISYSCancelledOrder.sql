﻿

CREATE PROCEDURE [dbo].[LoadMISYSCancelledOrder] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


--delete activity
delete 
from MISYS.CancelledOrder
where
Exists (
		Select 1 
		from TImportMISYSOrderModification
		Where 
		MISYS.CancelledOrder.OrderNumber = dbo.TImportMISYSOrderModification.order_number
		)
		
SELECT @RowsDeleted = @@Rowcount



INSERT INTO MISYS.CancelledOrder
	(
	 OrderNumber
	,Created
	,ByWhom
	) 
	
Select Distinct 
	order_number
	,Created = getdate()
	,ByWhom = system_user

From dbo.TImportMISYSOrderModification
Where order_canceled = '1'


SELECT @RowsInserted = @@Rowcount



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadMISYSCancelledOrder', @Stats, @StartTime

print @Stats





