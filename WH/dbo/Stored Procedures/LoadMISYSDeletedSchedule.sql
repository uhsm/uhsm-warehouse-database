﻿

CREATE PROCEDURE [dbo].[LoadMISYSDeletedSchedule] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


--delete activity
delete 
from MISYS.DeletedScheduleEncounter
where
MISYS.DeletedScheduleEncounter.DeletiondateODBC >= (
		Select Min(deletion_date_ODBC) as ImportDate
		from dbo.TImportMisysDeletedSchedule
		)
		
SELECT @RowsDeleted = @@Rowcount


INSERT INTO MISYS.DeletedScheduleEncounter
	(
	 [DeletiondateODBC]
	,[DeletionDescription]
	,[DeletionReason]
	,[DeletionTechCode]
	,[DeletionTechLocationDescription]
	,[DeletionTime]
	,[Diagnosis]
	,[ExamCodeWomh]
	,[ExamDescription]
	,[HospMnemonic]
	,[HospName]
	,[LeftRightIndicator]
	,[OrderNumber]
	,[PatientHospitalNumber]
	,[PatientName]
	,[PatientNumber]
	,[PatientType]
	,[PatientNumberWomh]
	,[PreAuthCode]
	,[SchedulingComment]
	,[SchedulingPhyiscian]
	,[SchedulingPhysicianCode]
	,[SchedulingPhysicianName]
	,[SchedulingReason]
	,[SchedulingTechName]
	,[SegmentDate]
	,[SegmentEndTime]
	,[SegmentEndTimeInt]
	,[SegmentName]
	,[SegmentRoom]
	,[SegmentStartTime]
	,[SegmentStartTimeInt]
	,[TechCodeScheduled]
	,[TechDeletedName]
	,[TechLocation]
	 ,Created
	,ByWhom
	) 
select
	 [DeletiondateODBC]
	,[DeletionDescription]
	,[DeletionReason]
	,[DeletionTechCode]
	,[DeletionTechLocationDescription]
	,[DeletionTime]
	,[Diagnosis]
	,[ExamCodeWomh]
	,[ExamDescription]
	,[HospMnemonic]
	,[HospName]
	,[LeftRightIndicator]
	,[OrderNumber]
	,[PatientHospitalNumber]
	,[PatientName]
	,[PatientNumber]
	,[PatientNumberWomh]
	,[PatientType]
	,[PreAuthCode]
	,[SchedulingComment]
	,[SchedulingPhyiscian]
	,[SchedulingPhysicianCode]
	,[SchedulingPhysicianName]
	,[SchedulingReason]
	,[SchedulingTechName]
	,[SegmentDate]
	,[SegmentEndTime]
	,[SegmentEndTimeInt]
	,[SegmentName]
	,[SegmentRoom]
	,[SegmentStartTime]
	,[SegmentStartTimeint]
	,[TechCodeScheduled]
	,[TechDeletedName]
	,[TechLocation]
	,Created = getdate()
	,ByWhom = system_user
from
	dbo.TLoadMysisDeletedScheduleEncounter
	

SELECT @RowsInserted = @@Rowcount



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadMISYSDeletedSchedule', @Stats, @StartTime

print @Stats





