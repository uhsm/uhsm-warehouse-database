﻿CREATE PROCEDURE [dbo].[LoadMISYSEncounter]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


--delete activity
delete from MISYS.Encounter 
where
	PerformanceDate >=

	(
	select
		min(TLoadMISYSEncounter.PerformanceDate)
	from
		dbo.TLoadMISYSEncounter
	)


SELECT @RowsDeleted = @@Rowcount


INSERT INTO MISYS.Encounter
	(
	 OrderNumber
	,PerformanceDate
	,CreationDate
	,EpisodeNumber
	,PerformanceTimeInt
	,PerformanceTime
	,PatientName
	,Department
	,ExamDescription
	,ExamGroupCode
	,DocumentStatusCode
	,FilmEntStatusCode
	,PatientTrackingStatusCode
	,CloseDate
	,OpenDate
	,PerformanceJulianDate
	,OrderDate
	,OrderTime
	,OrderingLocationCode
	,PatientAge
	,PatientTypeCode
	,DateOfBirth
	,SexCode
	,OrderExamCode
	,PerformanceDateInt
	,CaseTypeCode
	,OrderStatusCode
	,AttendingPhysicianCode1
	,AttendingPhysicianCode2
	,CaseStatusCode
	,CaseStatusDate
	,DictationStatusCode
	,OrderScheduledFlag
	,OrderingPhysicianCode
	,PriorityCode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,AttendingPhysician1
	,LocationCode
	,PatientForename
	,PatientSurname
	,SocialSecurityNo
	,ExtraData1
	,ExtraData2
	,ExtraData3
	,Postcode
	,DateOfDeath
	,RegisteredGpPractice
	,RegisteredGpPracticeCode
	,xgen3
	,xgen4
	,NHSNumberVerifiedFlag
	,DepartmentCode
	,ExamCode1
	,HISOrderNumber
	,ExamVar4
	,AETitleLocationCode
	,PrimaryPhysicianCode
	,PrimaryPhysician
	,SpecialtyCode
	,SuspensionDaysCode
	,RequestReceivedDate
	,VettedDate
	,DelayedByPatient
	,FailedPatientContact
	,PlannedRequest
	,RequestDate
	,FilmDataFlag
	,OrderingPhysicianCode1
	,PatientNo
	,PatientGenderCode
	,PatientNo1
	,AttendingPhysician2
	,ScheduleStatusCode
	,OrderingLocationCode1
	,CancelDate
	,DictationDate
	,FinalDate
	,PatientTypeForLinkingCode
	,CaseTypeCode1
	,CaseType
	,ModifiedExamCode
	,LinkExamCode
	,LinkLocationCode
	,LinkScheduleRoomCode
	,LinkTechCode
	,LinkLocation
	,ModificationDate
	,ModificationTime
	,ScheduleDate
	,ScheduleEndTime
	,ScheduleRoomCode
	,ScheduleStartTime
	,ModifiedTechCode
	,ScheduleCreateDate
	,ScheduleCreateTime
	,CreateLinkExamCode
	,CreateLinkLocationCode
	,CreateLinkScheduleRoomCode
	,CreateLinkTechCode
	,CreateLinkLocation
	,CreateLinkScheduleDate
	,CreateLinkScheduleRoom
	,CreateLinkTech
	,CancelDate1
	,CancelTime
	,CancelLinkTech
	,CancelLocationCode
	,CancelTechCode
	,OrderingPhysician
	,ONXQuestionnaireDataCode
	,PerformanceLocation
	,RadiologistCode
	,SeriesNo
	,FilmTechCode
	,Created
	,ByWhom
	,FiledDate
	,FiledTime
	) 
select
	 OrderNumber
	,PerformanceDate
	,CreationDate
	,EpisodeNumber
	,PerformanceTimeInt
	,PerformanceTime
	,PatientName
	,Department
	,ExamDescription
	,ExamGroupCode
	,DocumentStatusCode
	,FilmEntStatusCode
	,PatientTrackingStatusCode
	,CloseDate
	,OpenDate
	,PerformanceJulianDate
	,OrderDate
	,OrderTime
	,OrderingLocationCode
	,PatientAge
	,PatientTypeCode
	,DateOfBirth
	,SexCode
	,OrderExamCode
	,PerformanceDateInt
	,CaseTypeCode
	,OrderStatusCode
	,AttendingPhysicianCode1
	,AttendingPhysicianCode2
	,CaseStatusCode
	,CaseStatusDate
	,DictationStatusCode
	,OrderScheduledFlag
	,OrderingPhysicianCode
	,PriorityCode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,AttendingPhysician1
	,LocationCode
	,PatientForename
	,PatientSurname
	,SocialSecurityNo
	,ExtraData1
	,ExtraData2
	,ExtraData3
	,Postcode
	,DateOfDeath
	,RegisteredGpPractice
	,RegisteredGpPracticeCode
	,xgen3
	,xgen4
	,NHSNumberVerifiedFlag
	,DepartmentCode
	,ExamCode1
	,HISOrderNumber
	,ExamVar4
	,AETitleLocationCode
	,PrimaryPhysicianCode
	,PrimaryPhysician
	,SpecialtyCode
	,SuspensionDaysCode
	,RequestReceivedDate
	,VettedDate
	,DelayedByPatient
	,FailedPatientContact
	,PlannedRequest
	,RequestDate
	,FilmDataFlag
	,OrderingPhysicianCode1
	,PatientNo
	,PatientGenderCode
	,PatientNo1
	,AttendingPhysician2
	,ScheduleStatusCode
	,OrderingLocationCode1
	,CancelDate
	,DictationDate
	,FinalDate
	,PatientTypeForLinkingCode
	,CaseTypeCode1
	,CaseType
	,ModifiedExamCode
	,LinkExamCode
	,LinkLocationCode
	,LinkScheduleRoomCode
	,LinkTechCode
	,LinkLocation
	,ModificationDate
	,ModificationTime
	,ScheduleDate
	,ScheduleEndTime
	,ScheduleRoomCode
	,ScheduleStartTime
	,ModifiedTechCode
	,ScheduleCreateDate
	,ScheduleCreateTime
	,CreateLinkExamCode
	,CreateLinkLocationCode
	,CreateLinkScheduleRoomCode
	,CreateLinkTechCode
	,CreateLinkLocation
	,CreateLinkScheduleDate
	,CreateLinkScheduleRoom
	,CreateLinkTech
	,CancelDate1
	,CancelTime
	,CancelLinkTech
	,CancelLocationCode
	,CancelTechCode
	,OrderingPhysician
	,ONXQuestionnaireDataCode
	,PerformanceLocation
	,RadiologistCode
	,SeriesNo
	,FilmTechCode

	,Created = getdate()
	,ByWhom = system_user
	,FiledDate
	,FiledTime
from
	dbo.TLoadMISYSEncounter


SELECT @RowsInserted = @@Rowcount


--fix important corrupted columns
update
	MISYS.Encounter
set
	SocialSecurityNo = GoodNHSNumber.SocialSecurityNo
from
	MISYS.Encounter

inner join 
	(
	select
		 Encounter.PatientNo
		,Encounter.SocialSecurityNo
	from
		MISYS.Encounter
	where
		ISNUMERIC(
			replace(
				Encounter.SocialSecurityNo
				,' '
				,''
			)
		) = 1
	and	Encounter.SocialSecurityNo is not null
	and	not exists
		(
		select
			1
		from
			MISYS.Encounter Previous
		where
			ISNUMERIC(
				replace(
					Previous.SocialSecurityNo
					,' '
					,''
				)
			) = 1
		and	Previous.SocialSecurityNo is not null
		and	Previous.PatientNo = Encounter.PatientNo
		and	Previous.EncounterRecno > Encounter.EncounterRecno
		)
	) GoodNHSNumber
on	GoodNHSNumber.PatientNo = Encounter.PatientNo

where
	ISNUMERIC(
		replace(
			Encounter.SocialSecurityNo
			,' '
			,''
		)
	) = 0



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadMISYSEncounter', @Stats, @StartTime

print @Stats
