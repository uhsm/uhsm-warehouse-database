﻿CREATE procedure [dbo].[LoadMISYSOrder]
	@from smalldatetime
as

--this generates a unique order based on:

--	latest schedule create time
--	Latest Dictation Date -- 2011.08.11 KD Changed from earliest dictation date as was missing some final dates

--	latest modification time
--	EncounterRecno

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()

--@from varaiable declared locally for debugging
--declare @from smalldatetime
--select @from = min(TLoadMISYSEncounter.PerformanceDate) from dbo.TLoadMISYSEncounter

--remove records relating to data just imported
delete from MISYS.[Order] 
where
	PerformanceDate >= @from

SELECT @RowsDeleted = @@Rowcount

--remove any records with the same order number
delete from MISYS.[Order] 
where
	exists
	(
	select
		1
	from
		MISYS.Encounter
	where
		Encounter.OrderNumber = MISYS.[Order].OrderNumber
	and	Encounter.PerformanceDate >= @from
	)

SELECT @RowsDeleted = @RowsDeleted + @@Rowcount


insert into MISYS.[Order]
(
	 EncounterRecno
	,OrderNumber
	,PerformanceDate
	,CreationDate
	,EpisodeNumber
	,PerformanceTimeInt
	,PerformanceTime
	,PatientName
	,Department
	,ExamDescription
	,ExamGroupCode
	,DocumentStatusCode
	,FilmEntStatusCode
	,PatientTrackingStatusCode
	,CloseDate
	,OpenDate
	,PerformanceJulianDate
	,OrderDate
	,OrderTime
	,OrderingLocationCode
	,PatientAge
	,PatientTypeCode
	,DateOfBirth
	,SexCode
	,OrderExamCode
	,PerformanceDateInt
	,CaseTypeCode
	,OrderStatusCode
	,AttendingPhysicianCode1
	,AttendingPhysicianCode2
	,CaseStatusCode
	,CaseStatusDate
	,DictationStatusCode
	,OrderScheduledFlag
	,OrderingPhysicianCode
	,PriorityCode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,AttendingPhysician1
	,LocationCode
	,PatientForename
	,PatientSurname
	,SocialSecurityNo
	,ExtraData1
	,ExtraData2
	,ExtraData3
	,Postcode
	,DateOfDeath
	,RegisteredGpPractice
	,RegisteredGpPracticeCode
	,xgen3
	,xgen4
	,NHSNumberVerifiedFlag
	,DepartmentCode
	,ExamCode1
	,HISOrderNumber
	,ExamVar4
	,AETitleLocationCode
	,PrimaryPhysicianCode
	,PrimaryPhysician
	,SpecialtyCode
	,SuspensionDaysCode
	,RequestReceivedDate
	,VettedDate
	,DelayedByPatient
	,FailedPatientContact
	,PlannedRequest
	,RequestDate
	,FilmDataFlag
	,OrderingPhysicianCode1
	,PatientNo
	,PatientGenderCode
	,PatientNo1
	,AttendingPhysician2
	,ScheduleStatusCode
	,OrderingLocationCode1
	,CancelDate
	,DictationDate
	,FinalDate
	,PatientTypeForLinkingCode
	,CaseTypeCode1
	,CaseType
	,ModifiedExamCode
	,LinkExamCode
	,LinkLocationCode
	,LinkScheduleRoomCode
	,LinkTechCode
	,LinkLocation
	,ModificationDate
	,ModificationTime
	,ScheduleDate
	,ScheduleEndTime
	,ScheduleRoomCode
	,ScheduleStartTime
	,ModifiedTechCode
	,ScheduleCreateDate
	,ScheduleCreateTime
	,CreateLinkExamCode
	,CreateLinkLocationCode
	,CreateLinkScheduleRoomCode
	,CreateLinkTechCode
	,CreateLinkLocation
	,CreateLinkScheduleDate
	,CreateLinkScheduleRoom
	,CreateLinkTech
	,CancelDate1
	,CancelTime
	,CancelLinkTech
	,CancelLocationCode
	,CancelTechCode
	,OrderingPhysician
	,ONXQuestionnaireDataCode
	,PerformanceLocation
	,RadiologistCode
	,SeriesNo
	,FilmTechCode
	,FiledDate
	,FiledTime
)
select
	 EncounterRecno
	,OrderNumber
	,PerformanceDate
	,CreationDate
	,EpisodeNumber
	,PerformanceTimeInt
	,PerformanceTime
	,PatientName
	,Department
	,ExamDescription
	,ExamGroupCode
	,DocumentStatusCode
	,FilmEntStatusCode
	,PatientTrackingStatusCode
	,CloseDate
	,OpenDate
	,PerformanceJulianDate
	,OrderDate
	,OrderTime
	,OrderingLocationCode
	,PatientAge
	,PatientTypeCode
	,DateOfBirth
	,SexCode

	,OrderExamCode =
		case
		when not exists
			(
			select
				1
			from
				MISYS.Exam
			where
				Exam.ExamCode = Encounter.OrderExamCode
			)
		then
			case
			when not exists
				(
				select
					1
				from
					MISYS.Exam
				where
					Exam.ExamCode = Encounter.ExamCode1
				)
			then
				coalesce(
					(
					select
						ExamCode
					from
						MISYS.Exam
					where
						Exam.Exam = Encounter.ExamDescription
					)
					,'-1'
				)
			else Encounter.ExamCode1
			end

		else Encounter.OrderExamCode
		end

	,PerformanceDateInt
--CM distinction added by JC on 19/08/2013 as Cardiac MR's were being grouped into case type C (CT)
	,CaseTypeCode =case when MISYS.Encounter.CaseTypeCode like ('CM%') then 'CM' else
		left(
			case
			when not exists
				(
				select
					1
				from
					MISYS.CaseType
				where
					CaseType.CaseTypeRowID = Encounter.CaseTypeCode
				)
			then
				case
				when not exists
					(
					select
						1
					from
						MISYS.CaseType
					where
						CaseType.CaseTypeCode = Encounter.CaseTypeCode1
					)
				then
					(
					select
						CaseTypeCode
					from
						MISYS.Exam
					where
						Exam.ExamCode = Encounter.ExamCode1
					)
				else Encounter.CaseTypeCode1
				end

			else Encounter.CaseTypeCode
			end
			,1
		) end

	,OrderStatusCode
	,AttendingPhysicianCode1
	,AttendingPhysicianCode2
	,CaseStatusCode
	,CaseStatusDate
	,DictationStatusCode
	,OrderScheduledFlag

	,OrderingPhysicianCode =
		case
		when not exists
			(
			select
				1
			from
				MISYS.Physician
			where
				Physician.PhysicianRowID = Encounter.OrderingPhysician
			)
		then '-1'
		when Encounter.OrderingPhysician =''
		then '-1'
		--KO added 14/01/2013 to account for dodgy RadiologistCode in both MISYS.Physician and MISYS.Encounter
		--eg ''
		when CHARINDEX(';',Encounter.OrderingPhysician) = 0
		then '-1'
		else
			left(
				Encounter.OrderingPhysician
				,CHARINDEX(
					 ';'
					,Encounter.OrderingPhysician
				) - 1
			)

		end

	,PriorityCode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,AttendingPhysician1
	,LocationCode
	,PatientForename
	,PatientSurname
	,SocialSecurityNo
	,ExtraData1
	,ExtraData2
	,ExtraData3
	,Postcode
	,DateOfDeath
	,RegisteredGpPractice
	,RegisteredGpPracticeCode = Case when Encounter.RegisteredGpPracticeCode like '%%' then '-1' Else  Encounter.RegisteredGpPracticeCode End  --CMan 18/06/2014 added this to handle and repoint all odd characters like  '' to -1.  Same method of handling as used above for OrderingPhysicianCode.
	,xgen3
	,xgen4
	,NHSNumberVerifiedFlag
	,DepartmentCode
	,ExamCode1
	,HISOrderNumber
	,ExamVar4
	,AETitleLocationCode
	,PrimaryPhysicianCode = Case when  encounter.PrimaryPhysicianCode  like '%%' then '-1' Else  encounter.PrimaryPhysicianCode End--CMan 18/06/2014 added this to handle and repoint all odd characters like  '' to -1.  Same method of handling as used above for OrderingPhysicianCode.
	,PrimaryPhysician
	,SpecialtyCode
	,SuspensionDaysCode
	,RequestReceivedDate
	,VettedDate
	,DelayedByPatient
	,FailedPatientContact
	,PlannedRequest
	,RequestDate
	,FilmDataFlag
	,OrderingPhysicianCode1
	,PatientNo
	,PatientGenderCode
	,PatientNo1
	,AttendingPhysician2
	,ScheduleStatusCode
	,OrderingLocationCode1
	,CancelDate
	,DictationDate
	,FinalDate
	,PatientTypeForLinkingCode
	,CaseTypeCode1
	,CaseType
	,ModifiedExamCode
	,LinkExamCode
	,LinkLocationCode
	,LinkScheduleRoomCode
	,LinkTechCode
	,LinkLocation
	,ModificationDate
	,ModificationTime
	,ScheduleDate
	,ScheduleEndTime
	,ScheduleRoomCode
	,ScheduleStartTime
	,ModifiedTechCode
	,ScheduleCreateDate
	,ScheduleCreateTime
	,CreateLinkExamCode
	,CreateLinkLocationCode
	,CreateLinkScheduleRoomCode
	,CreateLinkTechCode
	,CreateLinkLocation
	,CreateLinkScheduleDate
	,CreateLinkScheduleRoom
	,CreateLinkTech
	,CancelDate1
	,CancelTime
	,CancelLinkTech
	,CancelLocationCode
	,CancelTechCode
	,OrderingPhysician= Case when encounter.OrderingPhysician  like '%%' then '-1' Else encounter.OrderingPhysician end  --CMan 18/06/2014 added this to handle and repoint all odd characters like  '' to -1.  Same method of handling as used above for OrderingPhysicianCode.
	,ONXQuestionnaireDataCode
	,PerformanceLocation

	,RadiologistCode =
		case
		when not exists
			(
			select
				1
			from
				MISYS.Physician
			where
				Physician.PhysicianRowID = Encounter.RadiologistCode
			)
		then '-1'
		when Encounter.RadiologistCode =''
		then '-1'
		--KO added 01/10/2013 to account for dodgy RadiologistCode in both MISYS.Physician and MISYS.Encounter
		--eg ''
		when CHARINDEX(';',Encounter.RadiologistCode) = 0
		then '-1'
		else
			left(
				Encounter.RadiologistCode
				,CHARINDEX(
					 ';'
					,Encounter.RadiologistCode
				) - 1
			)

		end

	,SeriesNo
	,FilmTechCode
	,FiledDate
	,FiledTime
from
	MISYS.Encounter
where
	not exists
	(
	select
		1
	from
		MISYS.Encounter Previous
	where
		Previous.OrderNumber = MISYS.Encounter.OrderNumber
	and	(
			coalesce(Previous.ScheduleCreateTime, getdate()) > coalesce(MISYS.Encounter.ScheduleCreateTime, getdate())
		or	(
				coalesce(Previous.ScheduleCreateTime, getdate()) = coalesce(MISYS.Encounter.ScheduleCreateTime, getdate())
			and	coalesce(Previous.DictationDate, getdate()) > coalesce(MISYS.Encounter.DictationDate, getdate())
			or	(
					coalesce(Previous.ScheduleCreateTime, getdate()) = coalesce(MISYS.Encounter.ScheduleCreateTime, getdate())
				and	coalesce(Previous.DictationDate, getdate()) = coalesce(MISYS.Encounter.DictationDate, getdate())
				and	coalesce(Previous.ModificationTime, getdate()) > coalesce(MISYS.Encounter.ModificationTime, getdate())
				or	(
						coalesce(Previous.ScheduleCreateTime, getdate()) = coalesce(MISYS.Encounter.ScheduleCreateTime, getdate())
					and	coalesce(Previous.DictationDate, getdate()) = coalesce(MISYS.Encounter.DictationDate, getdate())
					and	coalesce(Previous.ModificationTime, getdate()) = coalesce(MISYS.Encounter.ModificationTime, getdate())
					and	Previous.EncounterRecno > MISYS.Encounter.EncounterRecno
					)
				)
			)
		)
	)

and	PerformanceDate >= @from


SELECT @RowsInserted = @@Rowcount



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadMISYSOrder', @Stats, @StartTime

print @Stats
