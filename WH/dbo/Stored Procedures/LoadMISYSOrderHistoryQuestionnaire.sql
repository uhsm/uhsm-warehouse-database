﻿Create PROCEDURE [dbo].[LoadMISYSOrderHistoryQuestionnaire] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


--delete activity
delete 
from MISYS.OrderHistoryQuestionnaire
where
MISYS.OrderHistoryQuestionnaire.ModifiedJulianDate >= (
		Select Min(dj) as ImportDate
		from dbo.TimportMISYSOrderHistoryQuestionnaire
		)
		
SELECT @RowsDeleted = @@Rowcount


Insert Into MISYS.OrderHistoryQuestionnaire 
(
	 OrderNumber
	,OrderHistory
	,ModifiedJulianDate
	,ModifiedSeconds
	,ModifiedTime
	,TechnicianCode
	,LinkTechnicianCode
	,ModifyingLocationCode
	,LinkModifyingLocationCode
	,Created
	,ByWhom
)
(
Select 
	 OrderNumber = ohq.order_history
	,OrderHistory = ohq.order_history_quest
	,ModifiedJulianDate = ohq.dj
	,ModifiedSeconds = ohq.tm
	,ModifiedTime = Cast(DATEADD(ss,ohq.tm,DateAdd(dd,ohq.dj,'19751231 00:00:00')) as smallDatetime)
	,TechnicianCode = ohq.tech_code
	,LinkTechnicianCode = link_ohq_tech
	,ModifyingLocationCode = ohq.modifying_location
	,LinkModifyingLocationCode = ohq.link_ohq_location
	,Created = getdate()
	,ByWhom = system_user
From dbo.TimportMISYSOrderHistoryQuestionnaire ohq
	)


SELECT @RowsInserted = @@Rowcount
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadMISYSOrderHistoryQuestionnaire', @Stats, @StartTime

print @Stats





