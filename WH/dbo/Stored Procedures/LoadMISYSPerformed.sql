﻿create PROCEDURE [dbo].[LoadMISYSPerformed]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


--delete activity
delete from MISYS.Performed 
where
	PerformanceDate >=

	(
	select
		min(TLoadMISYSPerformed.PerformanceDate)
	from
		dbo.TLoadMISYSPerformed
	)


SELECT @RowsDeleted = @@Rowcount


INSERT INTO MISYS.Performed
	(
	 CreationDate
	,EpisodeNumber
	,OrderingLocationCode
	,PatientNo
	,Gender
	,PerformanceDate
	,PriorityCode
	,DocumentStatusCode
	,DictationDate
	,FinalDate
	,SpecialtyCode
	,DateOfBirth
	,PatientName
	,PatientForename
	,PatientSurname
	,SexCode
	,SocialSecurityNo
	,ExtraData1
	,ExtraData2
	,ExtraData3
	,Postcode
	,OrderNumber
	,OrderingPhysicianCode
	,ONXQuestionnaireDataCode
	,LocationCode
	,SeriesNo
	,FilmTechCode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,AttendingPhysicianCode1
	,AttendingPhysicianCode2
	,PatientTypeCode
	,CaseTypeCode
	,ExamCode
	,ExamDescription
	,ExamGroupCode
	,RadiologistCode
	,PrimaryPhysicianCode
	,PrimaryPhysician
	,DateOfDeath
	,RegisteredGpPracticeCode
	,NHSNumberVerifiedFlag
	,RegisteredGpPractice
	,Created
	,ByWhom
	)
select
	 CreationDate
	,EpisodeNumber
	,OrderingLocationCode
	,PatientNo
	,Gender
	,PerformanceDate
	,PriorityCode
	,DocumentStatusCode
	,DictationDate
	,FinalDate
	,SpecialtyCode
	,DateOfBirth
	,PatientName
	,PatientForename
	,PatientSurname
	,SexCode
	,SocialSecurityNo
	,ExtraData1
	,ExtraData2
	,ExtraData3
	,Postcode
	,OrderNumber
	,OrderingPhysicianCode
	,ONXQuestionnaireDataCode
	,LocationCode
	,SeriesNo
	,FilmTechCode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,AttendingPhysicianCode1
	,AttendingPhysicianCode2
	,PatientTypeCode
	,CaseTypeCode
	,ExamCode
	,ExamDescription
	,ExamGroupCode
	,RadiologistCode
	,PrimaryPhysicianCode
	,PrimaryPhysician
	,DateOfDeath
	,RegisteredGpPracticeCode
	,NHSNumberVerifiedFlag
	,RegisteredGpPractice

	,Created = getdate()
	,ByWhom = system_user
from
	dbo.TLoadMISYSPerformed


SELECT @RowsInserted = @@Rowcount



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadMISYSPerformed', @Stats, @StartTime

print @Stats
