﻿

CREATE PROCEDURE [dbo].[LoadMISYSQuestionnaire] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


--delete activity
delete 
from MISYS.Questionnaire
where
Exists (
		Select 1 
		from TImportMisysQuestionnnaire
		Where 
		MISYS.Questionnaire.OrderNumber = dbo.TImportMisysQuestionnnaire.order_number
		)
		
SELECT @RowsDeleted = @@Rowcount


INSERT INTO MISYS.Questionnaire
	(
	 OrderNumber
	,Comments
	,DateVetted
	,DelayedByPatient
	,PatientContact
	,PlannedRequest
	,RequestDate
	,Created
	,ByWhom
	) 
select

	order_number
	,exam_var50
	,exam_var51
	,exam_var52
	,exam_var53
	,exam_var54
	,exam_var55
	,Created = getdate()
	,ByWhom = system_user
from
	dbo.TImportMisysQuestionnnaire
where 
	order_number not in(
	(select order_number from dbo.TImportMisysQuestionnnaire with (nolock) group by order_number having COUNT(*) >1))

SELECT @RowsInserted = @@Rowcount



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadMISYSQuestionnaire', @Stats, @StartTime

print @Stats


/*
Delete From dbo.TImportMisysQuestionnnaireDuplicates 
WHere exists (Select 1 from dbo.TImportMisysQuestionnnaire qnx where dbo.TImportMisysQuestionnnaireDuplicates.order_number = qnx.order_number)


Insert Into dbo.TImportMisysQuestionnnaireDuplicates
Select 
qnx.* 
from dbo.TImportMisysQuestionnnaire qnx
Inner Join (
select order_number, COUNT(*) as recs  from dbo.TImportMisysQuestionnnaire with (nolock) group by order_number having COUNT(*) >1
) qnxDupe on qnx.order_number = qnxDupe.order_number
*/