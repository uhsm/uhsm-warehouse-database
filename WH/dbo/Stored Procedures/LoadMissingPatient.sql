﻿
/*
--Author: K Oakden
--Date created: 28/03/2014
Existing IPM extracts have  a small number of missing patient demographics
for patients to 'belonging' to RM2. THis cannot be resolved by CSC until we upgrade
to the LE2.2 extracts.

As a workaround, the missing patient demographics are pulled from the LPI.
In order to keep a historical record of changes to demographics (which will be relevent for 
producing non-current datasets such as CDS) dbo.LoadMissingPatientFromLPI checks for any changes 
in a given record and archives the old record if an update is detected.

--Copy of LoadMissingPatient to point to SQL Server LPI on TIE-SQL-CLUSTER

*/
CREATE procedure [dbo].[LoadMissingPatient]
as

exec dbo.ExtractMSLPIMissingPatient
exec dbo.LoadMissingPatientFromMSLPI
exec dbo.UpdateMissingPatientOPEncounter
exec dbo.UpdateMissingPatientAPCEncounter
exec dbo.UpdateMissingPatientRFEncounter