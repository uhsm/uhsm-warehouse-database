﻿
/*
--Author: K Oakden
--Date created: 27/03/2014
--Copy of LoadMissingPatientFromLPI to point to SQL Server LPI on TIE-SQL-CLUSTER
--Called by dbo.LoadMissingPatientNew

*/
CREATE procedure [dbo].[LoadMissingPatientFromMSLPI]
as
	declare @StartTime datetime
	declare @Elapsed int
	declare @RowsNewPatient Int
	declare @RowsArchivedPatient Int
	declare @RowsUpdatedPatient Int
	declare @Stats varchar(255)

	select @StartTime = getdate()

	--Add new patients
	INSERT INTO PAS.MissingPatient
       (SourcePatientNo
       ,LocalPatientID
       ,PatientForename
       ,PatientSurname
       ,NHSNumber
       ,NHSNumberStatusCode
       ,Postcode
       ,Sex
       ,DOB
       ,GPCode
       ,PracticeCode
       ,InterfaceCode
       ,Archived
       ,StartDate
	)
	select
		SourcePatientNo
       ,LocalPatientID
       ,PatientForename
       ,PatientSurname
       ,NHSNumber
       ,NHSNumberStatusCode
       ,Postcode
       ,Sex
       ,DOB
       ,GPCode
       ,PracticeCode
       ,InterfaceCode
       ,'N'
       ,GETDATE()
	from TLoadMSLPIMissingPatient
	where SourcePatientNo NOT IN (SELECT SourcePatientNo FROM PAS.MissingPatient) 
       
    select @RowsNewPatient = @@Rowcount
    
    
    --Archive patients that no longer have missing data??
	--delete from PAS.MissingPatient
	--where SourcePatientNo NOT IN (SELECT SourcePatientNo FROM dbo.TLoadLPIMissingPatient) 
	update PAS.MissingPatient set 
		Archived = 'Y'
		,EndDate = DATEADD(MINUTE, -1, GETDATE())
	where SourcePatientNo NOT IN (SELECT SourcePatientNo FROM dbo.TLoadMSLPIMissingPatient) 
	and Archived = 'N'
	and InterfaceCode = 'LPI';
    select @RowsArchivedPatient = @@Rowcount
    
    --Check if any details updated for existing patients, if so archive old record and insert new record   
	declare @SourcePatientNo int
	declare @LocalPatientID varchar(15)
	declare @PatientForename varchar(8000)
	declare @PatientSurname varchar(8000)
	declare @NHSNumber varchar(50)
	declare @NHSNumberStatusCode varchar(5)
	declare @Postcode varchar(20)
	declare @Sex varchar(20)
	declare @DOB varchar(10)
	declare @GPCode varchar(20)
	declare @PracticeCode varchar(20)
	declare @InterfaceCode varchar(10)
	
	declare NewData cursor for  
		SELECT SourcePatientNo, LocalPatientID, PatientForename, PatientSurname, NHSNumber, NHSNumberStatusCode
		,Postcode, Sex, DOB, GPCode , PracticeCode, InterfaceCode
		FROM dbo.TLoadMSLPIMissingPatient WHERE SourcePatientNo IN 
			(SELECT SourcePatientNo FROM PAS.MissingPatient) 
		EXCEPT 
		SELECT SourcePatientNo, LocalPatientID, PatientForename, PatientSurname, NHSNumber, NHSNumberStatusCode
		,Postcode, Sex, DOB, GPCode , PracticeCode, InterfaceCode
		FROM PAS.MissingPatient

	select @RowsUpdatedPatient = 0
	
	open NewData   
		fetch next from NewData into @SourcePatientNo,@LocalPatientID,@PatientForename,@PatientSurname,@NHSNumber,@NHSNumberStatusCode,@Postcode,@Sex,@DOB,@GPCode,@PracticeCode,@InterfaceCode   
		while @@fetch_status = 0   
		begin   
			--Archive existing data
			update PAS.MissingPatient set 
				Archived = 'Y'
				,EndDate = DATEADD(MINUTE, -1, GETDATE())
			where SourcePatientNo = @SourcePatientNo
			and Archived = 'N';
			
			--Then insert new data
			INSERT INTO PAS.MissingPatient
           (SourcePatientNo
           ,LocalPatientID
           ,PatientForename
           ,PatientSurname
           ,NHSNumber
           ,NHSNumberStatusCode
           ,Postcode
           ,Sex
           ,DOB
           ,GPCode
           ,PracticeCode
           ,InterfaceCode
           ,Archived
           ,StartDate
	)
     VALUES
           (@SourcePatientNo
           ,@LocalPatientID
           ,@PatientForename
           ,@PatientSurname
           ,@NHSNumber
           ,@NHSNumberStatusCode
           ,@Postcode
           ,@Sex
           ,@DOB
           ,@GPCode
           ,@PracticeCode
           ,@InterfaceCode
           ,'N'
           ,getdate()
	)
		select @RowsUpdatedPatient = @RowsUpdatedPatient + 1
		fetch next from NewData into @SourcePatientNo,@LocalPatientID,@PatientForename,@PatientSurname,@NHSNumber,@NHSNumberStatusCode,@Postcode,@Sex,@DOB,@GPCode,@PracticeCode,@InterfaceCode   
		end   
	close NewData   
	deallocate NewData
	
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Archived ' + CONVERT(varchar(10), @RowsArchivedPatient)  + 
	', Updated '  + CONVERT(varchar(10), @RowsUpdatedPatient) +  
	', Inserted '  + CONVERT(varchar(10), @RowsNewPatient) + 
	', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

--EXEC WriteAuditLogEvent 'PAS - WH LoadMissingPatientFromMSLPI', @Stats, @StartTime

print @Stats