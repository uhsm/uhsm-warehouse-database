﻿CREATE       procedure [dbo].[LoadOP] 
	 @from smalldatetime = null
	,@to smalldatetime = null
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

select @to = coalesce(@to, dateadd(day, datediff(day, 0, getdate()), 0))

select
	@from = 
		coalesce(
			@from
			,(
			select
				DateValue
			from
				dbo.Parameter
			where
				Parameter = 'EXTRACTOPDATE'
			)
		)


truncate table TImportOPEncounter
exec ExtractLorenzoOP @from
exec LoadOPEncounter

--load the MAU clinic into AE.Encounter
--exec LoadAEMAUEncounter


update dbo.Parameter
set
	DateValue = @StartTime
where
	Parameter = 'EXTRACTOPDATE'

if @@rowcount = 0

insert into dbo.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'EXTRACTOPDATE'
	,DateValue = @StartTime


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'PAS - WH LoadOP', @Stats, @StartTime
