﻿CREATE PROCEDURE [dbo].[LoadOPClinic]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


--delete archived activity
delete from OP.[Clinic]


where	
	exists
	(
	select
		1
	from
		dbo.TloadOPClinic
	where
		TLoadOPClinic.ServicePointUniqueID = [Clinic].ServicePointUniqueID
	and	TloadOPClinic.ArchiveFlag = 1
	)


SELECT @RowsDeleted = @@Rowcount

update OP.[Clinic]
set
	 [ServicePointUniqueID] = TEncounter.[ServicePointUniqueID]
	,[ClinicCode] = TEncounter.[ClinicCode]
	,[ClinicName] = TEncounter.[ClinicName]
	,[ClinicDescription] = TEncounter.[ClinicDescription]
	,[ClinicLocation] = TEncounter.[ClinicLocation]
	,[ClinicEffectiveFromDate] = TEncounter.[ClinicEffectiveFromDate]
	,[ClinicEffectiveToDate] = TEncounter.[ClinicEffectiveToDate]
	,[ClinicSpecialtyCode] = TEncounter.[ClinicSpecialtyCode]
	,[ClinicProfessionalCarerCode] = TEncounter.[ClinicProfessionalCarerCode]
	,[ClinicPurposeCode] = TEncounter.[ClinicPurposeCode]
	,[Horizon] = TEncounter.[Horizon]
	,[PartialBookingFlag] = TEncounter.[PartialBookingFlag]
	,[PartialBookingLeadTime] = TEncounter.[PartialBookingLeadTime]
	,[ArchiveFlag] = TEncounter.[ArchiveFlag]
	,[PASCreated] = TEncounter.[PASCreated]
	,[PASUpdated] = TEncounter.[PASUpdated]
	,[PASCreatedByWhom] = TEncounter.[PASCreatedByWhom]
	,[PASUpdatedByWhom] = TEncounter.[PASUpdatedByWhom]
	,Updated = getdate()
	,ByWhom = system_user

from
	dbo.TloadOPClinic TEncounter
where
	TEncounter.ServicePointUniqueID = OP.[Clinic].ServicePointUniqueID

select @RowsUpdated = @@rowcount


INSERT INTO OP.[Clinic]

	(
	 [ServicePointUniqueID]
	,[ClinicCode]
	,[ClinicName]
	,[ClinicDescription]
	,[ClinicLocation]
	,[ClinicEffectiveFromDate]
	,[ClinicEffectiveToDate]
	,[ClinicSpecialtyCode]
	,[ClinicProfessionalCarerCode]
	,[ClinicPurposeCode]
	,[Horizon]
	,[PartialBookingFlag]
	,[PartialBookingLeadTime]
	,[ArchiveFlag]
	,[PASCreated]
	,[PASUpdated]
	,[PASCreatedByWhom]
	,[PASUpdatedByWhom]
	,[Created]
	,[Updated]
	,[ByWhom]
	) 
select
	 [ServicePointUniqueID]
	,[ClinicCode]
	,[ClinicName]
	,[ClinicDescription]
	,[ClinicLocation]
	,[ClinicEffectiveFromDate]
	,[ClinicEffectiveToDate]
	,[ClinicSpecialtyCode]
	,[ClinicProfessionalCarerCode]
	,[ClinicPurposeCode]
	,[Horizon]
	,[PartialBookingFlag]
	,[PartialBookingLeadTime]
	,[ArchiveFlag]
	,[PASCreated]
	,[PASUpdated]
	,[PASCreatedByWhom]
	,[PASUpdatedByWhom]
	,Created = getdate()
	,Updated = GETDATE()
	,ByWhom = system_user
from
	dbo.TloadOPClinic
where
	not exists
	(
	select
		1
	from
		OP.[Clinic]
		
	where
		[Clinic].ServicePointUniqueID = TLoadOPClinic.ServicePointUniqueID
	)
and	TloadOPClinic.ArchiveFlag = 0


SELECT @RowsInserted = @@Rowcount



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Updated '  + CONVERT(varchar(10), @RowsUpdated) +  
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH LoadOPClinic', @Stats, @StartTime

print @Stats
