﻿CREATE PROCEDURE [dbo].[LoadOPDiary]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)
declare @LoadStartDate datetime
declare @LoadEndDate datetime

select @StartTime = getdate()

select
	 @LoadStartDate = MIN(CONVERT(datetime, SessionDate)) 
	,@LoadEndDate = MAX(CONVERT(datetime, SessionDate)) 
from
	dbo.TLoadOPDiary

delete from OP.Diary 
where	
	SessionDate between @LoadStartDate and @LoadEndDate


SELECT @RowsDeleted = @@Rowcount


INSERT INTO OP.Diary
	(
	 SourceUniqueID
	,ClinicCode
	,SessionCode
	,SessionDescription
	,SessionDate
	,SessionStartTime
	,SessionEndTime
	,ReasonForCancellation
	,SessionPeriod
	,Units
	,UsedUnits
	,FreeUnits
	,ValidAppointmentTypeCode
	,InterfaceCode
	,Created
	,ByWhom
	) 
select
	 SourceUniqueID
	,ClinicCode
	,SessionCode
	,SessionDescription
	,SessionDate
	,SessionStartTime
	,SessionEndTime
	,ReasonForCancellation
	,SessionPeriod
	,Units
	,UsedUnits
	,FreeUnits
	,ValidAppointmentTypeCode
	,InterfaceCode

	,Created = getdate()
	,ByWhom = system_user
from
	dbo.TLoadOPDiary
where
	not exists
	(
	select
		1
	from
		OP.Diary
	where
		Diary.SourceUniqueID = TLoadOPDiary.SourceUniqueID
	)

SELECT @RowsInserted = @@Rowcount

--update the last run date
delete from dbo.Parameter where Parameter = 'LASTDIARYLOADDATE'
insert into dbo.Parameter (Parameter, DateValue) values ('LASTDIARYLOADDATE', getdate())


SELECT @Elapsed = DATEDIFF(minute, @StartTime, getdate())

SELECT @Stats = 'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins, Period from ' + 
	CONVERT(varchar(11), @LoadStartDate) + ' to ' + CONVERT(varchar(11), @LoadEndDate)

EXEC WriteAuditLogEvent 'LoadOPDiary', @Stats, @StartTime

print @Stats
