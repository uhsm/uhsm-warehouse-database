﻿CREATE PROCEDURE [dbo].[LoadOPOperation]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


--delete for all related OPs
delete from OP.Operation
where
	exists
	(
	select
		1
	from
		dbo.TLoadOPOperation
	where
		TLoadOPOperation.SourcePatientNo = OP.Operation.SourcePatientNo
	and	TLoadOPOperation.SourceEncounterNo = OP.Operation.SourceEncounterNo
	)


SELECT @RowsDeleted = @@Rowcount


INSERT INTO OP.Operation
	(
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,OperationCode
	,OperationDate
	,DoctorCode
	,OPSourceUniqueID
	,Created
	,ByWhom
	) 
select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,OperationCode
	,OperationDate
	,DoctorCode
	,OPSourceUniqueID

	,Created = getdate()
	,ByWhom = system_user
from
	dbo.TLoadOPOperation TEncounter
where
	not exists
	(
	select
		1
	from
		OP.Operation
	where
		Operation.SourceUniqueID = TEncounter.SourceUniqueID
	)

SELECT @RowsInserted = @@Rowcount


--update appropriate columns in OP.Encounter
update
	OP.Encounter
set
	 SecondaryOperationCode1 = Operation1.OperationCode
	,SecondaryOperationCode2 = Operation2.OperationCode
	,SecondaryOperationCode3 = Operation3.OperationCode
	,SecondaryOperationCode4 = Operation4.OperationCode
	,SecondaryOperationCode5 = Operation5.OperationCode
	,SecondaryOperationCode6 = Operation6.OperationCode
	,SecondaryOperationCode7 = Operation7.OperationCode
	,SecondaryOperationCode8 = Operation8.OperationCode
	,SecondaryOperationCode9 = Operation9.OperationCode
	,SecondaryOperationCode10 = Operation10.OperationCode
	,SecondaryOperationCode11 = Operation11.OperationCode

	,SecondaryOperationDate1 = Operation1.OperationDate
	,SecondaryOperationDate2 = Operation2.OperationDate
	,SecondaryOperationDate3 = Operation3.OperationDate
	,SecondaryOperationDate4 = Operation4.OperationDate
	,SecondaryOperationDate5 = Operation5.OperationDate
	,SecondaryOperationDate6 = Operation6.OperationDate
	,SecondaryOperationDate7 = Operation7.OperationDate
	,SecondaryOperationDate8 = Operation8.OperationDate
	,SecondaryOperationDate9 = Operation9.OperationDate
	,SecondaryOperationDate10 = Operation10.OperationDate
	,SecondaryOperationDate11 = Operation11.OperationDate
from
	OP.Encounter

inner join OP.Operation Operation1
on	Operation1.OPSourceUniqueID = Encounter.SourceUniqueID
and	Operation1.SequenceNo = 1

left join OP.Operation Operation2
on	Operation2.OPSourceUniqueID = Encounter.SourceUniqueID
and	Operation2.SequenceNo = 2

left join OP.Operation Operation3
on	Operation3.OPSourceUniqueID = Encounter.SourceUniqueID
and	Operation3.SequenceNo = 3

left join OP.Operation Operation4
on	Operation4.OPSourceUniqueID = Encounter.SourceUniqueID
and	Operation4.SequenceNo = 4

left join OP.Operation Operation5
on	Operation5.OPSourceUniqueID = Encounter.SourceUniqueID
and	Operation5.SequenceNo = 5

left join OP.Operation Operation6
on	Operation6.OPSourceUniqueID = Encounter.SourceUniqueID
and	Operation6.SequenceNo = 6

left join OP.Operation Operation7
on	Operation7.OPSourceUniqueID = Encounter.SourceUniqueID
and	Operation7.SequenceNo = 7

left join OP.Operation Operation8
on	Operation8.OPSourceUniqueID = Encounter.SourceUniqueID
and	Operation8.SequenceNo = 8

left join OP.Operation Operation9
on	Operation9.OPSourceUniqueID = Encounter.SourceUniqueID
and	Operation9.SequenceNo = 9

left join OP.Operation Operation10
on	Operation10.OPSourceUniqueID = Encounter.SourceUniqueID
and	Operation10.SequenceNo = 10

left join OP.Operation Operation11
on	Operation11.OPSourceUniqueID = Encounter.SourceUniqueID
and	Operation11.SequenceNo = 11



SELECT @Elapsed = DATEDIFF(minute, @StartTime,getdate())

SELECT @Stats = 'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadOPOperation', @Stats, @StartTime

print @Stats
