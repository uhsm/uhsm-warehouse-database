﻿

CREATE procedure [dbo].[LoadOPReferenceData] 
	 @from smalldatetime = null
	,@to smalldatetime = null
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

select @to = coalesce(@to, dateadd(day, datediff(day, 0, getdate()), 0))

select
	@from = 
		coalesce(
			@from
			,(
			select
				DateValue
			from
				dbo.Parameter
			where
				Parameter = 'EXTRACTOPREFERENCEDATE'
			)
		)



--Slots
truncate table TImportOPSlot;
exec ExtractLorenzoOPSlot @from;
exec LoadOPSlot;

--Rules
truncate table TImportOPRule;
exec ExtractLorenzoOPRule @from;
exec LoadOPRule;

--Sessions
truncate table dbo.TimportOPSession;
exec dbo.ExtractLorenzoOPSession @from;
exec dbo.LoadOPSession;

--Clinics
truncate table dbo.TimportOPClinic;
exec dbo.ExtractLorenzoOPClinic @from;
exec dbo.LoadOPClinic;


update dbo.Parameter
set
	DateValue = @StartTime
where
	Parameter = 'EXTRACTOPREFERENCEDATE'

if @@rowcount = 0

insert into dbo.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'EXTRACTOPREFERENCEDATE'
	,DateValue = @StartTime


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'PAS - WH LoadOPReferenceData', @Stats, @StartTime



