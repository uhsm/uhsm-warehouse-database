﻿
CREATE PROCEDURE [dbo].[LoadOPRule]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


--delete archived activity
delete from OP.[Rule]

where	
	exists
	(
	select
		1
	from
		dbo.TLoadOPRule
	where
		TLoadOPRule.RuleUniqueID = [Rule].RuleUniqueID
	and	TLoadOPRule.ArchiveFlag = 1
	)


SELECT @RowsDeleted = @@Rowcount

update OP.[Rule]
set
	[RuleUniqueID] = TEncounter.[RuleUniqueID]
	,[AppliedTo] = TEncounter.[AppliedTo]
	,[AppliedToUniqueID] = TEncounter.[AppliedToUniqueID]
	,[EnforcedFlag] = TEncounter.[EnforcedFlag]
	,[RuleAppliedUniqueID] = TEncounter.[RuleAppliedUniqueID]
	,[RuleValueCode] = TEncounter.[RuleValueCode]
	,[ArchiveFlag] = TEncounter.[ArchiveFlag]
	,[PASCreated] = TEncounter.[PASCreated]
	,[PASUpdated] = TEncounter.[PASUpdated]
	,[PASCreatedByWhom] = TEncounter.[PASCreatedByWhom]
	,[PASUpdatedByWhom] = TEncounter.[PASUpdatedByWhom]
	,Updated = getdate()
	,ByWhom = system_user

from
	dbo.TLoadOPRule TEncounter
where
	TEncounter.RuleUniqueID = OP.[Rule].RuleUniqueID

select @RowsUpdated = @@rowcount


INSERT INTO OP.[Rule]

	(
	[RuleUniqueID]
	,[AppliedTo]
	,[AppliedToUniqueID]
	,[EnforcedFlag]
	,[RuleAppliedUniqueID]
	,[RuleValueCode]
	,[ArchiveFlag]
	,[PASCreated]
	,[PASUpdated]
	,[PASCreatedByWhom]
	,[PASUpdatedByWhom]
	,Created
	,Updated
	,ByWhom
	) 
select
	[RuleUniqueID]
	,[AppliedTo]
	,[AppliedToUniqueID]
	,[EnforcedFlag]
	,[RuleAppliedUniqueID]
	,[RuleValueCode]
	,[ArchiveFlag]
	,[PASCreated]
	,[PASUpdated]
	,[PASCreatedByWhom]
	,[PASUpdatedByWhom]
	,Created = getdate()
	,Updated = GETDATE()
	,ByWhom = system_user
from
	dbo.TLoadOPRule
where
	not exists
	(
	select
		1
	from
		OP.[Rule]
		
	where
		[Rule].RuleUniqueID = TLoadOPRule.RuleUniqueID
	)
and	TLoadOPRule.ArchiveFlag = 0


SELECT @RowsInserted = @@Rowcount



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Updated '  + CONVERT(varchar(10), @RowsUpdated) +  
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH LoadOPRule', @Stats, @StartTime

print @Stats
