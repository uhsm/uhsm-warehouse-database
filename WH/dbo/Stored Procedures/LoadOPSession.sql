﻿CREATE PROCEDURE [dbo].[LoadOPSession]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


--delete archived activity
delete from OP.[Session]


where	
	exists
	(
	select
		1
	from
		dbo.TloadOPSession
	where
		TLoadOPSession.SessionUniqueID = [Session].SessionUniqueID
	and	TloadOPSession.ArchiveFlag = 1
	)


SELECT @RowsDeleted = @@Rowcount

update OP.[Session]
set
	 [SessionUniqueID] = TEncounter.[SessionUniqueID]
	,[ServicePointUniqueID] = TEncounter.[ServicePointUniqueID]
	,[SessionCode] = TEncounter.[SessionCode]
	,[SessionName] = TEncounter.[SessionName]
	,[SessionDate] = TEncounter.[SessionDate]
	,[SessionStartTime] = TEncounter.[SessionStartTime]
	,[SessionStartDateTime] = TEncounter.[SessionStartDateTime]
	,[SessionEndTime] = TEncounter.[SessionEndTime]
	,[SessionEndDateTime] = TEncounter.[SessionEndDateTime]
	,[SessionDuration] = TEncounter.[SessionDuration]
	,[SlotDuration] = TEncounter.[SlotDuration]
	,[SessionStatusCode] = TEncounter.[SessionStatusCode]
	,[SessionStatusChangeReasonCode] = TEncounter.[SessionStatusChangeReasonCode]
	,[SessionStatusChangeRequestByCode] = TEncounter.[SessionStatusChangeRequestByCode]
	,[SessionSpecialtyCode] = TEncounter.[SessionSpecialtyCode]
	,[SessionProfessionalCarerCode] = TEncounter.[SessionProfessionalCarerCode]
	,[ActualSessionProfessionalCarerCode] = TEncounter.[ActualSessionProfessionalCarerCode]
	,[SessionStartStatusCode] = TEncounter.[SessionStartStatusCode]
	,[SessionStartStatusReasonCode] = TEncounter.[SessionStartStatusReasonCode]
	,[SessionEndStatusCode] = TEncounter.[SessionEndStatusCode]
	,[SessionEndStatusReasonCode] = TEncounter.[SessionEndStatusReasonCode]
	,[VariableSlotsFlag] = TEncounter.[VariableSlotsFlag]
	,[AllowOverbookedSlots] = TEncounter.[AllowOverbookedSlots]
	,[PartialBookingFlag] = TEncounter.[PartialBookingFlag]
	,[PartialBookingLeadTime] = TEncounter.[PartialBookingLeadTime]
	,[EBSPublishedFlag] = TEncounter.[EBSPublishedFlag]
	,[EBSPublishAllSlots] = TEncounter.[EBSPublishAllSlots]
	,[EBSServiceID] = TEncounter.[EBSServiceID]
	,[NewVisits] = TEncounter.[NewVisits]
	,[SessionTemplateUnqiueID] = TEncounter.[SessionTemplateUnqiueID]
	,[SessionCancelledFlag] = TEncounter.[SessionCancelledFlag]
	,[SessionCancelledDate] = TEncounter.[SessionCancelledDate]
	,[SessionInstructions] = TEncounter.[SessionInstructions]
	,[SessionComments] = TEncounter.[SessionComments]
	,[IsTemplate] = TEncounter.[IsTemplate]
	,[FrequencyCode] = TEncounter.[FrequencyCode]
	,[FrequencyOffsetCode] = TEncounter.[FrequencyOffsetCode]
	,[SessionFrequencyCode] = TEncounter.[SessionFrequencyCode]
	,[SessionFrequencyOffsetCode] = TEncounter.[SessionFrequencyOffsetCode]
	,[SessionWeeks] = TEncounter.[SessionWeeks]
	,[SessionDays] = TEncounter.[SessionDays]
	,[DaysInWeek1] = TEncounter.[DaysInWeek1]
	,[DaysInWeek2] = TEncounter.[DaysInWeek2]
	,[DaysInWeek3] = TEncounter.[DaysInWeek3]
	,[DaysInWeek4] = TEncounter.[DaysInWeek4]
	,[DaysInWeek5] = TEncounter.[DaysInWeek5]
	,[DaysInWeekLast] = TEncounter.[DaysInWeekLast]
	,[ArchiveFlag] = TEncounter.[ArchiveFlag]
	,[PASCreated] = TEncounter.[PASCreated]
	,[PASUpdated] = TEncounter.[PASUpdated]
	,[PASCreatedByWhom] = TEncounter.[PASCreatedByWhom]
	,[PASUpdatedByWhom] = TEncounter.[PASUpdatedByWhom]
	,Updated = getdate()
	,ByWhom = system_user

from
	dbo.TloadOPSession TEncounter
where
	TEncounter.SessionUniqueID = OP.[Session].SessionUniqueID

select @RowsUpdated = @@rowcount


INSERT INTO OP.[Session]

	(
	 [SessionUniqueID]
	,[ServicePointUniqueID]
	,[SessionCode]
	,[SessionName]
	,[SessionDate]
	,[SessionStartTime]
	,[SessionStartDateTime]
	,[SessionEndTime]
	,[SessionEndDateTime]
	,[SessionDuration]
	,[SlotDuration]
	,[SessionStatusCode]
	,[SessionStatusChangeReasonCode]
	,[SessionStatusChangeRequestByCode]
	,[SessionSpecialtyCode]
	,[SessionProfessionalCarerCode]
	,[ActualSessionProfessionalCarerCode]
	,[SessionStartStatusCode]
	,[SessionStartStatusReasonCode]
	,[SessionEndStatusCode]
	,[SessionEndStatusReasonCode]
	,[VariableSlotsFlag]
	,[AllowOverbookedSlots]
	,[PartialBookingFlag]
	,[PartialBookingLeadTime]
	,[EBSPublishedFlag]
	,[EBSPublishAllSlots]
	,[EBSServiceID]
	,[NewVisits]
	,[SessionTemplateUnqiueID]
	,[SessionCancelledFlag]
	,[SessionCancelledDate]
	,[SessionInstructions]
	,[SessionComments]
	,[IsTemplate]
	,[FrequencyCode]
	,[FrequencyOffsetCode]
	,[SessionFrequencyCode]
	,[SessionFrequencyOffsetCode]
	,[SessionWeeks]
	,[SessionDays]
	,[DaysInWeek1]
	,[DaysInWeek2]
	,[DaysInWeek3]
	,[DaysInWeek4]
	,[DaysInWeek5]
	,[DaysInWeekLast]
	,[ArchiveFlag]
	,[PASCreated]
	,[PASUpdated]
	,[PASCreatedByWhom]
	,[PASUpdatedByWhom]
	,[Created]
	,[updated]
	,[ByWhom]
	) 
select
	 [SessionUniqueID]
	,[ServicePointUniqueID]
	,[SessionCode]
	,[SessionName]
	,[SessionDate]
	,[SessionStartTime]
	,[SessionStartDateTime]
	,[SessionEndTime]
	,[SessionEndDateTime]
	,[SessionDuration]
	,[SlotDuration]
	,[SessionStatusCode]
	,[SessionStatusChangeReasonCode]
	,[SessionStatusChangeRequestByCode]
	,[SessionSpecialtyCode]
	,[SessionProfessionalCarerCode]
	,[ActualSessionProfessionalCarerCode]
	,[SessionStartStatusCode]
	,[SessionStartStatusReasonCode]
	,[SessionEndStatusCode]
	,[SessionEndStatusReasonCode]
	,[VariableSlotsFlag]
	,[AllowOverbookedSlots]
	,[PartialBookingFlag]
	,[PartialBookingLeadTime]
	,[EBSPublishedFlag]
	,[EBSPublishAllSlots]
	,[EBSServiceID]
	,[NewVisits]
	,[SessionTemplateUnqiueID]
	,[SessionCancelledFlag]
	,[SessionCancelledDate]
	,[SessionInstructions]
	,[SessionComments]
	,[IsTemplate]
	,[FrequencyCode]
	,[FrequencyOffsetCode]
	,[SessionFrequencyCode]
	,[SessionFrequencyOffsetCode]
	,[SessionWeeks]
	,[SessionDays]
	,[DaysInWeek1]
	,[DaysInWeek2]
	,[DaysInWeek3]
	,[DaysInWeek4]
	,[DaysInWeek5]
	,[DaysInWeekLast]
	,[ArchiveFlag]
	,[PASCreated]
	,[PASUpdated]
	,[PASCreatedByWhom]
	,[PASUpdatedByWhom]
	,Created = getdate()
	,Updated = GETDATE()
	,ByWhom = system_user
from
	dbo.TloadOPSession
where
	not exists
	(
	select
		1
	from
		OP.[Session]
		
	where
		[Session].SessionUniqueID = TLoadOPSession.SessionUniqueID
	)
and	TloadOPSession.ArchiveFlag = 0


SELECT @RowsInserted = @@Rowcount



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Updated '  + CONVERT(varchar(10), @RowsUpdated) +  
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH LoadOPSession', @Stats, @StartTime

print @Stats
