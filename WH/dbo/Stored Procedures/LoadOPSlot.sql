﻿CREATE PROCEDURE [dbo].[LoadOPSlot]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


--delete archived activity
delete from OP.Slot 
where	
	exists
	(
	select
		1
	from
		dbo.TLoadOPSlot
	where
		TLoadOPSlot.SlotUniqueID = Slot.SlotUniqueID
	and	TLoadOPSlot.ArchiveFlag = 1
	)


SELECT @RowsDeleted = @@Rowcount

update OP.Slot
set
	 [SlotUniqueID] = TEncounter.[SlotUniqueID]
	,[SessionUniqueID] = TEncounter.[SessionUniqueID]
	,[ServicePointUniqueID] = TEncounter.[ServicePointUniqueID]
	,[SlotDate] = TEncounter.[SlotDate]
	,[SlotStartTime] = TEncounter.[SlotStartTime]
	,[SlotStartDateTime] = TEncounter.[SlotStartDateTime]
	,[SlotEndTime] = TEncounter.[SlotEndTime]
	,[SlotEndDateTime] = TEncounter.[SlotEndDateTime]
	,[SlotDuration] = TEncounter.[SlotDuration]
	,[SlotStatusCode] = TEncounter.[SlotStatusCode]
	,[SlotChangeReasonCode] = TEncounter.[SlotChangeReasonCode]
	,[SlotChangeRequestByCode] = TEncounter.[SlotChangeRequestByCode]
	,[SlotPublishedToEBS] = TEncounter.[SlotPublishedToEBS]
	,[SlotMaxBookings] = TEncounter.[SlotMaxBookings]
	,[SlotNumberofBookings] = TEncounter.[SlotNumberofBookings]
	,[SlotTemplateUnqiueID] = TEncounter.[SlotTemplateUnqiueID]
	,[IsTemplate] = TEncounter.[IsTemplate]
	,[SlotCancelledFlag] = TEncounter.[SlotCancelledFlag]
	,[SlotCancelledDate] = TEncounter.[SlotCancelledDate]
	,[SlotInstructions] = TEncounter.[SlotInstructions]
	,[ArchiveFlag] = TEncounter.[ArchiveFlag]
	,[PASCreated] = TEncounter.[PASCreated]
	,[PASUpdated] = TEncounter.[PASUpdated]
	,[PASCreatedByWhom] = TEncounter.[PASCreatedByWhom]
	,[PASUpdatedByWhom] = TEncounter.[PASUpdatedByWhom]
	,Updated = getdate()
	,ByWhom = system_user

from
	dbo.TLoadOPSlot TEncounter
where
	TEncounter.SlotUniqueID = OP.Slot.SlotUniqueID

select @RowsUpdated = @@rowcount


INSERT INTO OP.Slot
	(
	 [SlotUniqueID]
	,[SessionUniqueID]
	,[ServicePointUniqueID]
	,[SlotDate]
	,[SlotStartTime]
	,[SlotStartDateTime]
	,[SlotEndTime]
	,[SlotEndDateTime]
	,[SlotDuration]
	,[SlotStatusCode]
	,[SlotChangeReasonCode]
	,[SlotChangeRequestByCode]
	,[SlotPublishedToEBS]
	,[SlotMaxBookings]
	,[SlotNumberofBookings]
	,[SlotTemplateUnqiueID]
	,[IsTemplate]
	,[SlotCancelledFlag]
	,[SlotCancelledDate]
	,[SlotInstructions]
	,[ArchiveFlag]
	,[PASCreated]
	,[PASUpdated]
	,[PASCreatedByWhom]
	,[PASUpdatedByWhom]
	,Created
	,Updated
	,ByWhom
	) 
select
	 [SlotUniqueID]
	,[SessionUniqueID]
	,[ServicePointUniqueID]
	,[SlotDate]
	,[SlotStartTime]
	,[SlotStartDateTime]
	,[SlotEndTime]
	,[SlotEndDateTime]
	,[SlotDuration]
	,[SlotStatusCode]
	,[SlotChangeReasonCode]
	,[SlotChangeRequestByCode]
	,[SlotPublishedToEBS]
	,[SlotMaxBookings]
	,[SlotNumberofBookings]
	,[SlotTemplateUnqiueID]
	,[IsTemplate]
	,[SlotCancelledFlag]
	,[SlotCancelledDate]
	,[SlotInstructions]
	,[ArchiveFlag]
	,[PASCreated]
	,[PASUpdated]
	,[PASCreatedByWhom]
	,[PASUpdatedByWhom]
	,Created = getdate()
	,Updated = GETDATE()
	,ByWhom = system_user
from
	dbo.TLoadOPSlot
where
	not exists
	(
	select
		1
	from
		OP.Slot
	where
		Slot.SlotUniqueID = TLoadOPSlot.SlotUniqueID
	)
and	TLoadOPSlot.ArchiveFlag = 0


SELECT @RowsInserted = @@Rowcount



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Updated '  + CONVERT(varchar(10), @RowsUpdated) +  
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH LoadOPSlot', @Stats, @StartTime

print @Stats
