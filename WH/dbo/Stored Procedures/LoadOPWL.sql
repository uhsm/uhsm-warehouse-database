﻿CREATE       procedure [dbo].[LoadOPWL] 
	 @census smalldatetime = null
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

select
	@census = 
		coalesce(
			@census
			,dateadd(day, datediff(day, 0, getdate()), 0)
		)


truncate table TImportOPWaitingList
exec ExtractLorenzoOPWaitingList @census
exec LoadOPWaitingList


-- Clear down old snapshots
declare @CensusCutOffDate smalldatetime

select @CensusCutOffDate =
	dateadd(
		 day
		,(
		select
			NumericValue * -1
		from
			dbo.Parameter
		where
			Parameter = 'KEEPSNAPSHOTDAYS'
		)
		,@census
	)

select
	CensusDate = max(CensusDate) 
into
	#OldSnapshot
from
	OP.WaitingList 
where
	CensusDate < @CensusCutOffDate
group by
	 datepart(year, CensusDate)
	,datepart(month, CensusDate)

--retain the latest snapshot this week
insert into
	#OldSnapshot
select
	CensusDate = max(CensusDate) 
from
	OP.WaitingList
where
	CensusDate < @CensusCutOffDate
group by
	 datepart(year, CensusDate)
	,datepart(wk, CensusDate)


delete from OP.WaitingList
where
	not exists
	(
	select
		1
	from
		#OldSnapshot OldSnapshot
	where
		OldSnapshot.CensusDate = WaitingList.CensusDate
	)
and	CensusDate < @CensusCutOffDate



-- reset snapshot table
delete from OP.Snapshot

insert into OP.Snapshot 
	(
	CensusDate
	)
select distinct 
	CensusDate
from 
	OP.WaitingList


update dbo.Parameter
set
	DateValue = @StartTime
where
	Parameter = 'EXTRACTOPWLDATE'

if @@rowcount = 0

insert into dbo.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'EXTRACTOPWLDATE'
	,DateValue = @StartTime


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Census ' + 
	CONVERT(varchar(11), coalesce(@census, ''))

exec WriteAuditLogEvent 'PAS - WH LoadOPWL', @Stats, @StartTime
