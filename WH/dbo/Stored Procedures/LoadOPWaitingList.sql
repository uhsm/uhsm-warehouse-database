﻿CREATE PROCEDURE [dbo].[LoadOPWaitingList]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


--delete archived activity
delete from OP.WaitingList 
where	
	not exists
	(
	select
		1
	from
		dbo.TLoadOPWaitingList
	where
		TLoadOPWaitingList.SourceUniqueID = WaitingList.SourceUniqueID
	and	TLoadOPWaitingList.SourceUniqueIDTypeCode = WaitingList.SourceUniqueIDTypeCode
	and	TLoadOPWaitingList.CensusDate = WaitingList.CensusDate
	)

and	WaitingList.CensusDate =
	(
	select top 1
		TLoadOPWaitingList.CensusDate
	from
		dbo.TLoadOPWaitingList
	)


SELECT @RowsDeleted = @@Rowcount

update OP.WaitingList
set
	 SourcePatientNo = TEncounter.SourcePatientNo
	,ReferralSourceUniqueID = TEncounter.ReferralSourceUniqueID
	,AppointmentSourceUniqueID = TEncounter.AppointmentSourceUniqueID
	,PatientTitle = TEncounter.PatientTitle
	,PatientForename = TEncounter.PatientForename
	,PatientSurname = TEncounter.PatientSurname
	,DateOfBirth = TEncounter.DateOfBirth
	,DateOfDeath = TEncounter.DateOfDeath
	,SexCode = TEncounter.SexCode
	,NHSNumber = TEncounter.NHSNumber
	,DistrictNo = TEncounter.DistrictNo
	,Postcode = TEncounter.Postcode
	,PatientAddress1 = TEncounter.PatientAddress1
	,PatientAddress2 = TEncounter.PatientAddress2
	,PatientAddress3 = TEncounter.PatientAddress3
	,PatientAddress4 = TEncounter.PatientAddress4
	,DHACode = TEncounter.DHACode
	,HomePhone = TEncounter.HomePhone
	,WorkPhone = TEncounter.WorkPhone
	,EthnicOriginCode = TEncounter.EthnicOriginCode
	,MaritalStatusCode = TEncounter.MaritalStatusCode
	,ReligionCode = TEncounter.ReligionCode
	,ConsultantCode = TEncounter.ConsultantCode
	,SpecialtyCode = TEncounter.SpecialtyCode
	,PASSpecialtyCode = TEncounter.PASSpecialtyCode
	,PriorityCode = TEncounter.PriorityCode
	,WaitingListCode = TEncounter.WaitingListCode
	,CommentClinical = TEncounter.CommentClinical
	,CommentNonClinical = TEncounter.CommentNonClinical
	,SiteCode = TEncounter.SiteCode
	,PurchaserCode = TEncounter.PurchaserCode
	,ProviderCode = TEncounter.ProviderCode
	,ContractSerialNo = TEncounter.ContractSerialNo
	,AdminCategoryCode = TEncounter.AdminCategoryCode
	,CancelledBy = TEncounter.CancelledBy
	,DoctorCode = TEncounter.DoctorCode
	,BookingTypeCode = TEncounter.BookingTypeCode
	,CasenoteNumber = TEncounter.CasenoteNumber
	,InterfaceCode = TEncounter.InterfaceCode
	,RegisteredGpCode = TEncounter.RegisteredGpCode
	,RegisteredGpPracticeCode = TEncounter.RegisteredGpPracticeCode
	,EpisodicGpCode = TEncounter.EpisodicGpCode
	,EpisodicGpPracticeCode = TEncounter.EpisodicGpPracticeCode
	,SourceTreatmentFunctionCode = TEncounter.SourceTreatmentFunctionCode
	,TreatmentFunctionCode = TEncounter.TreatmentFunctionCode
	,NationalSpecialtyCode = TEncounter.NationalSpecialtyCode
	,PCTCode = TEncounter.PCTCode
	,ReferralDate = TEncounter.ReferralDate
	,BookedDate = TEncounter.BookedDate
	,BookedTime = TEncounter.BookedTime
	,AppointmentDate = TEncounter.AppointmentDate
	,AppointmentTypeCode = TEncounter.AppointmentTypeCode
	,AppointmentStatusCode = TEncounter.AppointmentStatusCode
	,AppointmentCategoryCode = TEncounter.AppointmentCategoryCode
	,QM08StartWaitDate = TEncounter.QM08StartWaitDate
	,QM08EndWaitDate = TEncounter.QM08EndWaitDate
	,AppointmentTime = TEncounter.AppointmentTime
	,ClinicCode = TEncounter.ClinicCode
	,SourceOfReferralCode = TEncounter.SourceOfReferralCode
	,MRSAFlag = TEncounter.MRSAFlag
	,RTTPathwayID = TEncounter.RTTPathwayID
	,RTTPathwayCondition = TEncounter.RTTPathwayCondition
	,RTTStartDate = TEncounter.RTTStartDate
	,RTTEndDate = TEncounter.RTTEndDate
	,RTTSpecialtyCode = TEncounter.RTTSpecialtyCode
	,RTTCurrentProviderCode = TEncounter.RTTCurrentProviderCode
	,RTTCurrentStatusCode = TEncounter.RTTCurrentStatusCode
	,RTTCurrentStatusDate = TEncounter.RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag = TEncounter.RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag = TEncounter.RTTOverseasStatusFlag
	,PASCreated = TEncounter.PASCreated
	,PASUpdated = TEncounter.PASUpdated
	,PASCreatedByWhom = TEncounter.PASCreatedByWhom
	,PASUpdatedByWhom = TEncounter.PASUpdatedByWhom
	,ArchiveFlag = TEncounter.ArchiveFlag
	,FuturePatientCancelDate = TEncounter.FuturePatientCancelDate
	,AdditionFlag = TEncounter.AdditionFlag
	,CountOfDNAs = TEncounter.CountOfDNAs
	,CountOfHospitalCancels = TEncounter.CountOfHospitalCancels
	,CountOfPatientCancels = TEncounter.CountOfPatientCancels
	,KornerWait = TEncounter.KornerWait
	,BreachDate = TEncounter.BreachDate
	,BreachDays = TEncounter.BreachDays
	,NationalBreachDate = TEncounter.NationalBreachDate
	,NationalBreachDays = TEncounter.NationalBreachDays
	,DateOnWaitingList = TEncounter.DateOnWaitingList
	,IntendedPrimaryOperationCode = TEncounter.IntendedPrimaryOperationCode
	,Operation = TEncounter.Operation

	,Updated = getdate()
	,ByWhom = system_user

	,WaitingListRule = TEncounter.WaitingListRule
	,VisitCode = TEncounter.VisitCode
	,InviteDate = TEncounter.InviteDate
	,WaitingListClinicCode = TEncounter.WaitingListClinicCode
	,GeneralComment = TEncounter.GeneralComment
from
	dbo.TLoadOPWaitingList TEncounter
where
	TEncounter.SourceUniqueID = OP.WaitingList.SourceUniqueID
and	TEncounter.SourceUniqueIDTypeCode = OP.WaitingList.SourceUniqueIDTypeCode
and	TEncounter.CensusDate = OP.WaitingList.CensusDate

select @RowsUpdated = @@rowcount


INSERT INTO OP.WaitingList
	(
	 CensusDate
	,SourceUniqueID
	,SourceUniqueIDTypeCode
	,SourcePatientNo
	,ReferralSourceUniqueID
	,AppointmentSourceUniqueID
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,ConsultantCode
	,SpecialtyCode
	,PASSpecialtyCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,SiteCode
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,AdminCategoryCode
	,CancelledBy
	,DoctorCode
	,BookingTypeCode
	,CasenoteNumber
	,InterfaceCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,ReferralDate
	,BookedDate
	,BookedTime
	,AppointmentDate
	,AppointmentTypeCode
	,AppointmentStatusCode
	,AppointmentCategoryCode
	,QM08StartWaitDate
	,QM08EndWaitDate
	,AppointmentTime
	,ClinicCode
	,SourceOfReferralCode
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,ArchiveFlag
	,FuturePatientCancelDate
	,AdditionFlag
	,CountOfDNAs
	,CountOfHospitalCancels
	,CountOfPatientCancels
	,KornerWait
	,BreachDate
	,BreachDays
	,NationalBreachDate
	,NationalBreachDays
	,DateOnWaitingList
	,IntendedPrimaryOperationCode
	,Operation
	,Created
	,ByWhom
	,WaitingListRule
	,VisitCode
	,InviteDate
	,WaitingListClinicCode
	,GeneralComment
	) 
select
	 CensusDate
	,SourceUniqueID
	,SourceUniqueIDTypeCode
	,SourcePatientNo
	,ReferralSourceUniqueID
	,AppointmentSourceUniqueID
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,ConsultantCode
	,SpecialtyCode
	,PASSpecialtyCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,SiteCode
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,AdminCategoryCode
	,CancelledBy
	,DoctorCode
	,BookingTypeCode
	,CasenoteNumber
	,InterfaceCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,ReferralDate
	,BookedDate
	,BookedTime
	,AppointmentDate
	,AppointmentTypeCode
	,AppointmentStatusCode
	,AppointmentCategoryCode
	,QM08StartWaitDate
	,QM08EndWaitDate
	,AppointmentTime
	,ClinicCode
	,SourceOfReferralCode
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,ArchiveFlag
	,FuturePatientCancelDate
	,AdditionFlag
	,CountOfDNAs
	,CountOfHospitalCancels
	,CountOfPatientCancels
	,KornerWait
	,BreachDate
	,BreachDays
	,NationalBreachDate
	,NationalBreachDays
	,DateOnWaitingList
	,IntendedPrimaryOperationCode
	,Operation

	,Created = getdate()
	,ByWhom = system_user
	,WaitingListRule
	,VisitCode
	,InviteDate
	,WaitingListClinicCode
	,GeneralComment
from
	dbo.TLoadOPWaitingList
where
	not exists
	(
	select
		1
	from
		OP.WaitingList
	where
		WaitingList.SourceUniqueID = TLoadOPWaitingList.SourceUniqueID
	and	WaitingList.SourceUniqueIDTypeCode = TLoadOPWaitingList.SourceUniqueIDTypeCode
	and	WaitingList.CensusDate = TLoadOPWaitingList.CensusDate
	)
and	TLoadOPWaitingList.ArchiveFlag = 0


SELECT @RowsInserted = @@Rowcount



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Updated '  + CONVERT(varchar(10), @RowsUpdated) +  
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH LoadOPWaitingList', @Stats, @StartTime

print @Stats
