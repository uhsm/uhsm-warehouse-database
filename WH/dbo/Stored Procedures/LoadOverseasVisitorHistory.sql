﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Load Overseas Visitor History

Notes:			Stored in 

Versions:
				1.0.0.1 - 20/01/2016 - MT
					Switched to using the new master net change
					table PAS.znNetChangeByPatient.

				1.0.0.0 - 26/10/2015 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure dbo.LoadOverseasVisitorHistory
As

Begin Try

Declare @StartTime datetime
Declare @Elapsed int
Declare @RowsInserted Int
Declare @Stats varchar(255) = ''
Declare @LastRunDateTime datetime

Select @StartTime = getdate()

--Set @LastRunDateTime = '1900-01-01'  -- to perform the initial load.

Set @LastRunDateTime = (
	Select	Max(src.EventTime)
	From	dbo.AuditLog src With (NoLock)
	Where	src.ProcessCode = 'PAS - WH LoadOverseasVisitorHistory'
	)

-- Load net change table

	Truncate Table PAS.znOverseasVisitorHistory

	Insert Into PAS.znOverseasVisitorHistory(PatientNo,SourceType)
	Select	Distinct src.PATNT_REFNO,'znNetChangeByPatient'
	From	PAS.znNetChangeByPatient src With (NoLock)
	Where	src.LastUpdatedDateTime > @LastRunDateTime

-- Delete existing records identified by net change
Delete	src
From	PAS.OverseasVisitorHistory src

		Inner Join PAS.znOverseasVisitorHistory zn With (NoLock)
			On src.PatientNo = zn.PatientNo

-- Load

-- Get the latest DistictNo (casenote)
;With MyDistrictNo As (
	Select	PatientNo = src.PATNT_REFNO
			,DistrictNo = src.IDENTIFIER
			,ROW_NUMBER() Over (Partition By src.PATNT_REFNO Order By src.START_DTTM Desc,src.PATID_REFNO Desc) As RowNo
	From	Lorenzo.dbo.PatientIdentifier src With (NoLock)
	
			Inner Join PAS.znOverseasVisitorHistory zn With (NoLock)
				On src.PATNT_REFNO = zn.PatientNo

	Where	src.ARCHV_FLAG = 'N'
			And src.IDENTIFIER Like 'RM2%'
			And src.PITYP_REFNO = 2001232 -- Patient ID (Facility)
	),
	-- Get all overseas visitor statuses in order
	MyOVS1 As (
	Select	PatientNo = src.PATNT_REFNO
			,ROW_NUMBER() Over (
				Partition By src.PATNT_REFNO,DW_REPORTING.LIB.fn_GetJustDate(src.START_DTTM) 
				Order By src.OVSEA_REFNO Desc) As RowNum
			,StartDate = DW_REPORTING.LIB.fn_GetJustDate(src.START_DTTM) 
			,EndDate = DW_REPORTING.LIB.fn_GetJustDate(src.END_DTTM)
			,OverseasVisitorStatusRefNo = src.OVSVS_REFNO
			,OverseasVisitorStatusCode = src.OVSVS_REFNO_MAIN_CODE
			,src.OVSEA_REFNO
	From	Lorenzo.dbo.OverseasVisitorStatus src With (NoLock)

			Inner Join Lorenzo.dbo.Patient pat With (NoLock)
				On src.PATNT_REFNO = pat.PATNT_REFNO
				And pat.ARCHV_FLAG = 'N'

			Inner Join PAS.znOverseasVisitorHistory zn With (NoLock)
				On src.PATNT_REFNO = zn.PatientNo
			
	Where	src.ARCHV_FLAG = 'N'
	),
	-- Only select the latest of any records with the same start date
	MyOVS2 As (
	Select	src.PatientNo
			,ROW_NUMBER() Over (
				Partition By src.PatientNo 
				Order By src.StartDate Asc,
				Case When src.EndDate Is Null Then '2099-12-31' Else src.EndDate End Asc,
				src.OVSEA_REFNO Asc) As RowNoAsc
			,ROW_NUMBER() Over (
				Partition By src.PatientNo 
				Order By src.StartDate Desc,
				Case When src.EndDate Is Null Then '2099-12-31' Else src.EndDate End Desc,
				src.OVSEA_REFNO Desc) As RowNoDesc
			,src.StartDate
			,src.EndDate
			,src.OverseasVisitorStatusRefNo
			,src.OverseasVisitorStatusCode
			,src.OVSEA_REFNO
	From	MyOVS1 src With (NoLock)
	Where	src.RowNum = 1
	),
	-- Only select records where start date <> end date unless it's the first or last for the patient
	MyOVS3 As (
	Select	src.PatientNo
			,ROW_NUMBER() Over (
				Partition By src.PatientNo 
				Order By src.StartDate Asc,
				Case When src.EndDate Is Null Then '2099-12-31' Else src.EndDate End Asc,
				src.OVSEA_REFNO Asc) As RowNoAsc
			,src.StartDate
			,src.EndDate
			,src.OverseasVisitorStatusRefNo
			,src.OverseasVisitorStatusCode
			,src.OVSEA_REFNO
	From	MyOVS2 src With (NoLock)
	
			Left Join MyOVS2 previous With (NoLock)
				On src.PatientNo = previous.PatientNo
				And src.RowNoAsc - 1 = previous.RowNoAsc
	
	Where	src.StartDate <> src.EndDate
			Or src.EndDate Is Null
			Or src.RowNoAsc = 1
			Or src.RowNoDesc = 1
	),
	-- Only select records where the status has changed
	MyOVS4 As (
	Select	src.PatientNo
			,ROW_NUMBER() Over (
				Partition By src.PatientNo 
				Order By src.StartDate Asc,
				Case When src.EndDate Is Null Then '2099-12-31' Else src.EndDate End Asc,
				src.OVSEA_REFNO Asc) As RowNoAsc
			,ROW_NUMBER() Over (
				Partition By src.PatientNo 
				Order By src.StartDate Desc,
				Case When src.EndDate Is Null Then '2099-12-31' Else src.EndDate End Desc,
				src.OVSEA_REFNO Desc) As RowNoDesc
			,src.StartDate
			,src.EndDate
			,src.OverseasVisitorStatusRefNo
			,src.OverseasVisitorStatusCode
			,src.OVSEA_REFNO
	From	MyOVS3 src With (NoLock)
	
			Left Join MyOVS3 previous With (NoLock)
				On src.PatientNo = previous.PatientNo
				And src.RowNoAsc - 1 = previous.RowNoAsc
	
	Where	previous.PatientNo Is Null
			 Or Coalesce(previous.OverseasVisitorStatusRefno,'') <> Coalesce(src.OverseasVisitorStatusRefno,'')
	)
	
Insert Into PAS.OverseasVisitorHistory(
	PatientNo
	,DistrictNo
	,SeqNo
	,StartDate
	,EndDate
	,OverseasVisitorStatusRefno
	,OverseasVisitorStatusCode
	,CurrentFlag
	,OVSEA_REFNO
	,LastLoadedDateTime
	)
Select
	src.PatientNo
	,dis.DistrictNo
	,src.RowNoAsc
	,src.StartDate
	,Case
		-- Set the end date to the start date of the next record if there is one
 		When nextovs.StartDate Is Not Null Then nextovs.StartDate
 		-- Otherwise set the end date to null
		Else src.EndDate
	End
	,src.OverseasVisitorStatusRefNo
	,src.OverseasVisitorStatusCode
	-- Only set CurrentFlag to Y on the last record for each patient if it does not have an end date.
	,Case When nextovs.StartDate Is Null And src.EndDate Is Null Then 'Y' Else 'N' End
	,src.OVSEA_REFNO
	,GETDATE()

From	MyOVS4 src With (NoLock)
	
		Left Join Lorenzo.dbo.ExcludedPatient ex With (NoLock)
			On src.PatientNo = ex.SourcePatientNo

		Left Join MyOVS4 nextovs With (NoLock)
			On src.PatientNo = nextovs.PatientNo
			And src.RowNoAsc + 1 = nextovs.RowNoAsc

		Left Join MyDistrictNo dis With (NoLock)
			On src.PatientNo = dis.PatientNo
			And dis.RowNo = 1	

Where	ex.SourcePatientNo Is Null

Select @RowsInserted = @@rowcount

Select @Stats = @Stats + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted)

Select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

Select @Stats = @Stats + 
	'. Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

Exec WriteAuditLogEvent 'PAS - WH LoadOverseasVisitorHistory', @Stats, @StartTime

Update	src
Set		DateValue = GETDATE()
From	dbo.Parameter src
Where	src.Parameter = 'EXTRACTOVERSEASVISITORHISTORY'

End Try

Begin Catch
	Exec msdb.dbo.sp_send_dbmail
	@profile_name = 'Business Intelligence',
	@recipients = 'Business.Intelligence@uhsm.nhs.uk',
	@subject = 'Load Overseas Visitor History failed',
	@body_format = 'HTML',
	@body = 'dbo.LoadOverseasVisitorHistory failed.<br><br>';
End Catch
