﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		LoadPASData

Notes:			Stored in 

Versions:		1.0.1.1 - 20/04/2016 - CM
					Added stored procedure to build a physical table in DW_Reporting.CRIS to list all RM3 referrals
					and added stored procedure to update the OurActivityFlag and NotOurActProvCode fields in DW_REporting.CRIS.FactEvents and DW_Reporting.CRIS.FactOrders

				1.0.1.0 - 11/04/2016 - CM 
					Added stored procedure to update the non RM2 Activity flags post build of RF.Encounter, OP.Encounter and APc.Encounter tables


				1.0.0.9 - 26/01/2016 - MT
					Added debugging to find out where exactly this sproc is failing.

				1.0.0.8 - 21/01/2016 - MT - Job 239
					Removed calls to:
					uspLoadznNetChangeByPatient
					uspLoadznNetChangeByFacilityID
					Now part of the new sproc: PAS.uspPostLoadLorenzo.

				1.0.0.7 - 18/01/2016 - MT - Job 239
					Added calls to:
					uspLoadznNetChangeByPatient
					uspLoadznNetChangeByFacilityID

				1.0.0.6 - 25/11/2015 - MT
					Create a record in [V-UHSM-DW02].Admin.dbo.Parameter
					to indicate that the PAS load has completed so that
					the update of V-UHSM-DW02.Lorenzo can begin.

				1.0.0.5 - 13/11/2015 - MT
					Added call to LoadPatient.

				1.0.0.4 - 26/10/2015 - MT
					Added call to LoadOverseasVisitorHistory.

				1.0.0.3 - 26/10/2015 - MT
					Removed call to LoadPatientCurrentAddress - redundant.

				1.0.0.2 - 22/10/2015 - MT
					Added call to LoadPatientAddressHistory

				1.0.0.1 - 20/10/2015 - MT
					Replaced call to LoadPatientGPPracticeHistory
					with LoadPatientGPHistory and moved the call
					earlier in the running order i.e. before the Encounter
					loads so that its data can be used in the Encounter load
					rather than being applied as a post load update.

					Moved call to LoadResponsibleCommissioner procs to where
					the LoadPatientGPHistory was before.  This will ulitmately
					become redundant when the commissioning fields in the
					Encounter tables are populated as part of their load.

				1.0.0.0
					Original sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[LoadPASData]
AS

Truncate Table Debug
Insert Into Debug(Details) Values('LoadPASData: started')

DECLARE @dataLoadDate DATETIME
DECLARE @StartTime DATETIME
DECLARE @Elapsed INT
DECLARE @Stats VARCHAR(255)
DECLARE @writeToLog BIT

SELECT @StartTime = getdate()

SELECT @writeToLog = 0

-- Check the time of the last Lorenzo data load
-- then load each dataset that has not been loaded since.
SELECT @dataLoadDate =  DateValue
FROM Lorenzo.dbo.Parameter
WHERE Parameter = 'DATALOADDATE'

IF (
		SELECT DateValue
		FROM dbo.Parameter
		WHERE Parameter = 'EXTRACTOPREFERENCEDATE'
		) < @dataLoadDate
BEGIN
	Insert Into Debug(Details) Values('LoadPASData: LoadOPReferenceData started')
	EXEC dbo.LoadOPReferenceData

	SELECT @writeToLog = 1
END

-- Need to run this before the WL extracts to ensure the RTT.RTT table is populated with the latest details
IF (
		SELECT DateValue
		FROM dbo.Parameter
		WHERE Parameter = 'EXTRACTREFERENCEDATADATE'
		) < @dataLoadDate
BEGIN
	Insert Into Debug(Details) Values('LoadPASData: LoadReferenceData started')
	EXEC dbo.LoadReferenceData

	SELECT @writeToLog = 1
END

IF (
		SELECT DateValue
		FROM dbo.Parameter
		WHERE Parameter = 'EXTRACTPATIENT'
		) < @dataLoadDate
BEGIN
	Insert Into Debug(Details) Values('LoadPASData: LoadPatient started')
	EXEC dbo.LoadPatient
	SET @writeToLog = 1
END

IF (
		SELECT DateValue
		FROM dbo.Parameter
		WHERE Parameter = 'EXTRACTPATIENTGPHISTORY'
		) < @dataLoadDate
BEGIN
	Insert Into Debug(Details) Values('LoadPASData: LoadPatientGPHistory started')
	EXEC dbo.LoadPatientGPHistory
	SET @writeToLog = 1
END

IF (
		SELECT DateValue
		FROM dbo.Parameter
		WHERE Parameter = 'EXTRACTPATIENTADDRESSHISTORY'
		) < @dataLoadDate
BEGIN
	Insert Into Debug(Details) Values('LoadPASData: LoadPatientAddressHistory started')
	EXEC dbo.LoadPatientAddressHistory
	SET @writeToLog = 1
END

IF (
		SELECT DateValue
		FROM dbo.Parameter
		WHERE Parameter = 'EXTRACTOVERSEASVISITORHISTORY'
		) < @dataLoadDate
BEGIN
	Insert Into Debug(Details) Values('LoadPASData: LoadOverseasVisitorHistory started')
	EXEC dbo.LoadOverseasVisitorHistory
	SET @writeToLog = 1
END

IF (
		SELECT DateValue
		FROM dbo.Parameter
		WHERE Parameter = 'EXTRACTAPCDATE'
		) < @dataLoadDate
BEGIN
	Insert Into Debug(Details) Values('LoadPASData: LoadAPC started')
	EXEC dbo.LoadAPC

	SELECT @writeToLog = 1
END

IF (
		SELECT DateValue
		FROM dbo.Parameter
		WHERE Parameter = 'EXTRACTAPCWLDATE'
		) < @dataLoadDate
BEGIN
	Insert Into Debug(Details) Values('LoadPASData: LoadAPCWL started')
	EXEC dbo.LoadAPCWL

	SELECT @writeToLog = 1
END

IF (
		SELECT DateValue
		FROM dbo.Parameter
		WHERE Parameter = 'EXTRACTOPDATE'
		) < @dataLoadDate
BEGIN
	Insert Into Debug(Details) Values('LoadPASData: LoadOP started')
	EXEC dbo.LoadOP

	SELECT @writeToLog = 1
END

IF (
		SELECT DateValue
		FROM dbo.Parameter
		WHERE Parameter = 'EXTRACTOPWLDATE'
		) < @dataLoadDate
BEGIN
	Insert Into Debug(Details) Values('LoadPASData: LoadOPWL started')
	EXEC dbo.LoadOPWL

	SELECT @writeToLog = 1
END

IF (
		SELECT DateValue
		FROM dbo.Parameter
		WHERE Parameter = 'EXTRACTRFDATE'
		) < @dataLoadDate
BEGIN
	Insert Into Debug(Details) Values('LoadPASData: LoadRF started')
	EXEC dbo.LoadRF

	SELECT @writeToLog = 1
END



IF (	SELECT Max(EventTime)
		FROM dbo.AuditLog
		WHERE ProcessCode = 'PAS - WH NonRM2FlagsALLUpdated'
		) <  @dataLoadDate

BEGIN 
	Insert Into Debug(Details) Values('LoadPASData: UpdateFlagsForNonRM2Activity started')
	EXEC dbo.UpdateFlagsForNonRM2Activity
	
	SELECT @writeToLog = 1
END





--KO 29/03/2015 - move additional processing
IF (
		SELECT DateValue
		FROM dbo.Parameter
		WHERE Parameter = 'BUILDWHDATE'
		) < (
		SELECT DateValue
		FROM dbo.Parameter
		WHERE Parameter = 'EXTRACTRFDATE'
		)
BEGIN
	----------------------------------------------------------------------------------
	--Add extra processing here that should be completed once per day after successful 
	--completion of WH data load
	--Remove any activity related to test patients
	Insert Into Debug(Details) Values('LoadPASData: RemoveTestPatients started')
	EXEC dbo.RemoveTestPatients

	Insert Into Debug(Details) Values('LoadPASData: LoadResponsibleCommissionerAPC started')
	Exec dbo.LoadResponsibleCommissionerAPC
	Insert Into Debug(Details) Values('LoadPASData: LoadResponsibleCommissionerOP started')
	Exec dbo.LoadResponsibleCommissionerOP
	Insert Into Debug(Details) Values('LoadPASData: LoadResponsibleCommissionerRF started')
	Exec dbo.LoadResponsibleCommissionerRF

	--Get SCR referral data to send to SCR
	Insert Into Debug(Details) Values('LoadPASData: SCR.LoadPASReferralForImport started')
	EXEC SCR.LoadPASReferralForImport 1 --set truncate flag
		--Get possible purchaser overwrites once at end of pas load job
		--exec CDS.LoadPurchaserCheck

	--build ptls once at end of pas load job
	Insert Into Debug(Details) Values('LoadPASData: PTL.BuildOPPTL started')
	EXEC PTL.BuildOPPTL

	Insert Into Debug(Details) Values('LoadPASData: PTL.BuildOPWaitingListPartialBooking started')
	EXEC PTL.BuildOPWaitingListPartialBooking

	Insert Into Debug(Details) Values('LoadPASData: PTL.BuildActiveWaitingList started')
	EXEC PTL.BuildActiveWaitingList

	Insert Into Debug(Details) Values('LoadPASData: PTL.BuildIPWL started')
	EXEC PTL.BuildIPWL --1

	Insert Into Debug(Details) Values('LoadPASData: PTL.BuildOPWaitingList started')
	EXEC PTL.BuildOPWaitingList

	--KO 26/01/2015: BuildFirst90Code
	Insert Into Debug(Details) Values('LoadPASData: BuildRTTFirst90Code started')
	EXEC Lorenzo.dbo.BuildRTTFirst90Code

	--fix inpatient coding
	Insert Into Debug(Details) Values('LoadPASData: ExtractLorenzoAPCCoding started')
	EXEC dbo.ExtractLorenzoAPCCoding

	Insert Into Debug(Details) Values('LoadPASData: Update BUILDWHDATE parameter started')
	UPDATE dbo.Parameter
	SET DateValue = getdate()
	WHERE Parameter = 'BUILDWHDATE'
	
	
	--20/04/2016 CM
	--add stored procedure to build a physical table of all RM3 referrals in the DW_Reporting database to help flag RM3 CRIS activity
	Insert Into Debug(Details) Values('LoadPASData: LoadNotOurActReferrals into CRIS started')
	Exec DW_REPORTING.CRIS.LoadNotOurActReferrals
	
	--20/04/2016 CM
	--add stored procedure to update DW_Reporting.CRIS.FactEvents and DW_Reporting.CRIS.FactOrders tables with the not our activity flags
	Insert Into Debug(Details) Values('LoadPASData: UpdateNotOurActivity flags in CRIS events and Orders started')
	Exec DW_REPORTING.CRIS.UpdateNotOurActFlags
	



	SELECT @writeToLog = 1
END

IF @writeToLog = 1
BEGIN
	SELECT @Elapsed = DATEDIFF(minute, @StartTime, getdate())

	SELECT @Stats = 'Time Elapsed ' + CONVERT(VARCHAR(6), @Elapsed) + ' Mins'

	EXEC WriteAuditLogEvent 'PAS - WH LoadPASData Total'
		,@Stats
		,@StartTime

	Update	src
	Set		DateValue = GETDATE()
	From	[V-UHSM-DW02].Admin.dbo.Parameter src
	Where	src.Parameter = 'BUILDWHDATE'
	
END
