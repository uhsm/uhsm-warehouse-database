﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Load PAS Data No Parameters

Notes:			Stored in 
				Use this sp to run the WH PAS load without affecting parameters.

Versions:
				1.0.0.3 - 26/10/2015 - MT
					Added call to LoadOverseasVisitorHistory.

				1.0.0.2 - 26/10/2015 - MT
					Removed call to LoadPatientCurrentAddress - redundant.
					Added call to LoadPatientAddressHistory

				1.0.0.1 - 20/10/2015 - MT
					Replaced call to LoadPatientGPPracticeHistory
					with LoadPatientGPHistory.

				1.0.0.0 - 06/11/2014 - KO
					Original sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE procedure [dbo].[LoadPASDataNoParameters] as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @writeToLog bit

select @StartTime = getdate()
select @writeToLog = 0

declare @Param varchar(255)
	select @Param = DateValue from dbo.Parameter where Parameter = 'EXTRACTOPREFERENCEDATE'
	exec dbo.LoadOPReferenceData
	update dbo.Parameter set DateValue = @Param where Parameter = 'EXTRACTOPREFERENCEDATE'
	
	select @Param = DateValue from dbo.Parameter where Parameter = 'EXTRACTREFERENCEDATADATE'
	exec dbo.LoadReferenceData
	update dbo.Parameter set DateValue = @Param where Parameter = 'EXTRACTREFERENCEDATADATE'
	
	select @Param = DateValue from dbo.Parameter where Parameter = 'EXTRACTAPCDATE'
	exec dbo.LoadAPC
	update dbo.Parameter set DateValue = @Param where Parameter = 'EXTRACTAPCDATE'
		
	select @Param = DateValue from dbo.Parameter where Parameter = 'EXTRACTAPCWLDATE'
	exec dbo.LoadAPCWL
	update dbo.Parameter set DateValue = @Param where Parameter = 'EXTRACTAPCWLDATE'
	
	select @Param = DateValue from dbo.Parameter where Parameter = 'EXTRACTOPDATE'
	exec dbo.LoadOP
	update dbo.Parameter set DateValue = @Param where Parameter = 'EXTRACTOPDATE'
	
	select @Param = DateValue from dbo.Parameter where Parameter = 'EXTRACTOPWLDATE'
	exec dbo.LoadOPWL
	update dbo.Parameter set DateValue = @Param where Parameter = 'EXTRACTOPWLDATE'
	
	select @Param = DateValue from dbo.Parameter where Parameter = 'EXTRACTRFDATE'
	exec dbo.LoadRF
	update dbo.Parameter set DateValue = @Param where Parameter = 'EXTRACTRFDATE'

	--Remove any activity related to test patients
	exec dbo.RemoveTestPatients
	
	--Add missing patient data (from LPI) once at end of pas load job
	exec dbo.LoadMissingPatient
	
	--Get sequential practice history
	exec dbo.LoadPatientGPHistory;
	
	--Get sequential address history
	exec dbo.LoadPatientAddressHistory;

	--Get sequential overseas visitor history
	exec dbo.LoadOverseasVisitorHistory

	--Get SCR referral data to send to SCR
	exec SCR.LoadPASReferralForImport 1		--set truncate flag
	
	--build ptls once at end of pas load job
	exec PTL.BuildOPPTL
	exec PTL.BuildOPWaitingListPartialBooking
	exec PTL.BuildActiveWaitingList
	exec PTL.BuildIPWL --1
	exec PTL.BuildOPWaitingList
	
	--fix inpatient coding
	exec dbo.ExtractLorenzoAPCCoding

	select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

	select @Stats = 
		'Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

	exec WriteAuditLogEvent 'PAS - WH LoadPASDataNoParameters', @Stats, @StartTime
