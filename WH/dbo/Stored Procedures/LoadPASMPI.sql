﻿
CREATE PROCEDURE [dbo].LoadPASMPI AS

--DELETE FROM INFORMATION_REPORTING WHERE ARCHV_FLAG = 'Y'
--IN PATIENTS01

--DELETE FROM INFORMATION_REPORTING.DBO.TBL_MPI
--FROM INFORMATION_REPORTING.DBO.TBL_MPI A 
--INNER JOIN PATIENTS01 B ON A.PT_ID = CAST(B.PATNT_REFNO AS VARCHAR(20))
--WHERE B.ARCHV_FLAG = 'Y'

----GET ALL PATNT_REFNOS FOR PATIENTS THAT NEED UPDATING:

--IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME = N'IPM_UPDATE_MPI')

--DROP TABLE IPM_UPDATE_MPI

----GET PATIENT UPDATES
--SELECT DISTINCT
--PATNT_REFNO
--INTO IPM_UPDATE_MPI
--FROM 
--PATIENTS01
--WHERE ARCHV_FLAG = 'N'

----GET ADDRESS UPDATES
--UNION SELECT DISTINCT
--PATNT_REFNO
--FROM ADDSSROLES01 
--WHERE ARCHV_FLAG = 'N'

----GET GP UPDATES
--UNION SELECT DISTINCT
--PATNT_REFNO
--FROM PATPROFCARERS01
--WHERE ARCHV_FLAG = 'N' 
--AND PRTYP_REFNO = 1132


----GET MRSA UPDATES FROM ALERTS (DGPRO)
--UNION SELECT DISTINCT
--PATNT_REFNO
--FROM DGPRO01
--WHERE ARCHV_FLAG = 'N'
--AND CODE IN ('009','INFC006')


----DELETE FROM INFORMATION_REPORTING WHERE THE PATIENT EXISTS IN
----IPM_UPDATE_MPI

--DELETE FROM INFORMATION_REPORTING.DBO.TBL_MPI
--FROM INFORMATION_REPORTING.DBO.TBL_MPI A 
--INNER JOIN IPM_UPDATE_MPI B ON A.PT_ID = CAST(B.PATNT_REFNO AS VARCHAR(20))




----Empty tmp MPI
--TRUNCATE TABLE tbl_MPI

--Print 'Adding Patients....'
----Insert Rows from tmpPAT_MPI_Pat--
--INSERT INTO
--tbl_MPI
--(
--PT_ID,
--PT_PASNo,
--PT_NHSNo,
--PT_Sex,
--PT_Title,
--PT_Forename,
--PT_Surname,
--PT_EthGroup,
--PT_DOB,
--PT_DOB_Estimated,
--PT_Deceased,
--PT_DOD,
--PT_DOD_Estimated,
--PT_Religion,
--PT_Nationality,
--PT_NHSNo_Status,
--PT_Spoken_Language,
--PT_Marital_Status,
--PT_DateCreated,
--PT_Creator,
--PT_DateModified,
--PT_Modifier)

--SELECT
--PATNT_REFNO,
--PT_PASNo,
--PT_NHSNo,
--PT_Sex,
--PT_Title,
--PT_Forename,
--PT_Surname,
--PT_EthGroup,
----Convert(Datetime,PT_DOB,103),
--PT_DOB,
--PT_DOB_Estimated,
--PT_Deceased,
----Convert(Datetime, PT_DOD, 103),
--PT_DOD,
--PT_DOD_Estimated,
--PT_Religion,
--PT_Nationality,
--PT_NHSNo_Status,
--PT_Spoken_Language,
--PT_Marital_Status,
--PT_DateCreated,
--PT_Creator,
--PT_DateModified,
--PT_Modifier
--FROM 
--(
SELECT A.PATNT_REFNO, CREATE_DTTM,MODIF_DTTM, ARCHV_FLAG,
/*
CASE ARCHV_FLAG 
	WHEN 'N' THEN 'LIVE'
	ELSE 'ARCHIVED'
END AS PatStatus,
*/
PATNT_REFNO,
PATNT_REFNO_PASID AS PT_PASNo,
PATNT_REFNO_NHS_IDENTIFIER AS PT_NHSNo,
SEXXX_REFNO_DESCRIPTION AS PT_Sex, 
TITLE_REFNO_DESCRIPTION AS PT_Title, 
FORENAME AS PT_Forename, 
SURNAME AS PT_Surname, 
ETHGR_REFNO_DESCRIPTION AS PT_EthGroup, 
DTTM_OF_BIRTH as PT_DOB, 
BIRTH_DTTM_ESTIMATE_FLAG AS PT_DOB_Estimated, 
DECSD_FLAG AS PT_Deceased, 
DTTM_OF_DEATH AS PT_DOD, 
DEATH_DTTM_ESTIMATE_FLAG AS PT_DOD_Estimated, 
RELIG_REFNO_DESCRIPTION AS PT_Religion, 
NATNL_REFNO_DESCRIPTION AS PT_Nationality, 
NNNTS_CODE AS PT_NHSNo_Status, 
SPOKL_REFNO_DESCRIPTION AS PT_Spoken_Language, 
MARRY_REFNO_DESCRIPTION AS PT_Marital_Status,
CREATE_DTTM AS PT_DateCreated,
USER_CREATE AS PT_Creator,
MODIF_DTTM AS PT_DateModified,
USER_MODIF AS PT_Modifier

FROM Lorenzo.dbo.Patient A
--INNER JOIN IPM_UPDATE_MPI B ON A.PATNT_REFNO = B.PATNT_REFNO
WHERE ARCHV_FLAG = 'N'
--ORDER BY PATNT_REFNO atmpPAT_MPI_Pat
--)pats


--Print 'Updating GP Details....'
--UPDATE tbl_MPI
--Set
--GP_Code = tmpPAT_MPI_GP.GPCode,
--GP_Name = tmpPAT_MPI_GP.GPName,
--GP_AD_Line1 = tmpPAT_MPI_GP.Address1,
--GP_AD_Line2 = tmpPAT_MPI_GP.Address2,
--GP_AD_PostCode = tmpPAT_MPI_GP.PostCode,
--GP_Practice_Code = tmpPAT_MPI_GP.PracticeCode,
--GP_PCT_Code_of_GP = tmpPAT_MPI_GP.PCTCode,
--GP_PCT_Name_of_GP = GP_PCTName
--FROM tbl_MPI, tmpPAT_MPI_GP
--WHERE tbl_MPI.PT_ID = tmpPAT_MPI_GP.PATNT_REFNO
--AND tmpPAT_MPI_GP.GPStatus = 'Live'

--Print 'Updating Address Details....'
--UPDATE tbl_MPI
--SET
--AD_Location_Code = B.LOCAT_CODE,
--AD_Address_TYPE = B.ADTYP_CODE,
--AD_Line1 = B.LINE1,
--AD_Line2 = B.LINE2,
--AD_Line3 = B.LINE3,
--AD_Line4 = B.LINE4,
--AD_PostCode = B.PCODE,
--AD_PCT_Code_of_Residence = B.HDIST_CODE
--From 
--tbl_MPI A 
--INNER JOIN tmpPAT_MPI_ADDSS B ON PT_ID = B.PATNT_REFNO
--INNER JOIN tmpLiveAddss C ON B.PATNT_REFNO = C.PATNT_REFNO AND B.START_DTTM = C.LASTDATE



--Print 'Updating Facil ID'
--UPDATE TBL_MPI
--SET PT_FACIL_ID = B.IDENTIFIER
--FROM TBL_MPI A INNER JOIN 
--	(
--	SELECT
--	A.PATNT_REFNO,
--	A.IDENTIFIER
--	FROM PATIDS A INNER JOIN REFVALUES B ON A.PITYP_REFNO = B.RFVAL_REFNO
--	WHERE A.ARCHV_FLAG = 'N' AND B.MAIN_CODE = 'FACIL' AND B.ARCHV_FLAG = 'N' AND A.HEORG_REFNO = '2002118'
--	AND LEFT(A.IDENTIFIER,3) = 'RM2'
--	) B ON A.PT_ID = B.PATNT_REFNO




----UPDATE MRSA INFORMATION
--Print 'Updating MRSA Informatino'
--UPDATE TBL_MPI
--SET MRSA = 'Y',
--MRSA_START_DATE = CAST(B.START_DTTM AS DATETIME),
--MRSA_END_DATE = CAST(B.END_DTTM AS DATETIME)
--FROM TBL_MPI A INNER JOIN DGPRO B ON A.PT_ID = B.PATNT_REFNO AND B.ARCHV_FLAG = 'N'
--WHERE 
--B.CODE = '009' 
--OR 
--B.CODE = 'INFC006'

----UPADTE HOME PHONE NUMBER
--Update tbl_MPI
--set HomePhone = B.Line1
--From tbl_MPI A Inner Join vwPatientPhoneHome B on a.pt_id = Cast(b.PATNT_REFNO as Varchar(20))


----UPDATE MOBILE PHONE NUMBER
--Update tbl_MPI
--set MobilePhone = B.Line1
--From tbl_MPI A Inner Join vwPatientPhoneMobile B on a.pt_id = Cast(b.PATNT_REFNO as Varchar(20))




--INSERT RECORDS TO INFORMATION REPORTING

--INSERT INTO INFORMATION_REPORTING.DBO.TBL_MPI
--SELECT
--*,
--'IPM'
--FROM TBL_MPI




