﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Load Patient

Notes:			Stored in 
				Primarily to provide a simple lookup for LocalPatientIdentifier.
				Could be used for other patient-based attributes like address etc
				if required.

Versions:
				1.0.0.1 - 20/01/2016 - MT
					Switched to using the new master net change
					table PAS.znNetChangeByPatient.

				1.0.0.2 - 12/01/2016 - MT
					PATNT_REFNO 10652579 has 2 master PATNT_REFNOs in the MergePatient table.
					This caused an error in the load.
					Modified the MyMerges CTE to select the latest master PATNT_REFNO to avoid
					this error.

				1.0.0.1 - 23/11/2015 - MT
					Added MasterPATNT_REFNO field.
					This stored the PATNT_REFNO that the patient
					has been merged to if there is a merge otherwise
					the original PATNT_REFNO.

				1.0.0.0 - 13/11/2015 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure dbo.LoadPatient
As

Begin Try

Declare @StartTime datetime
Declare @Elapsed int
Declare @RowsInserted Int
Declare @Stats varchar(255) = ''
Declare @LastRunDateTime datetime

Select @StartTime = getdate()

Set @LastRunDateTime = (
	Select	Max(src.EventTime)
	From	dbo.AuditLog src With (NoLock)
	Where	src.ProcessCode = 'PAS - WH LoadPatient'
	)

--Set @LastRunDateTime = '1900-01-01'  -- to perform the initial load.
--Set @LastRunDateTime = '2016-01-10'  -- to perform a partial refresh.

-- Load net change table

	Truncate Table PAS.znPatient

	Insert Into PAS.znPatient(PATNT_REFNO,SourceType)
	Select	Distinct src.PATNT_REFNO,'znNetChangeByPatient'
	From	PAS.znNetChangeByPatient src With (NoLock)
	Where	src.LastUpdatedDateTime > @LastRunDateTime

-- Delete existing records identified by net change
Delete	src
From	PAS.Patient src

		Inner Join PAS.znPatient zn With (NoLock)
			On src.PATNT_REFNO = zn.PATNT_REFNO

-- Load

-- Get the latest local patient identifier
;With MyPID As (
	Select	PATNT_REFNO = src.PATNT_REFNO
			,LocalPatientIdentifier = src.IDENTIFIER
			,ROW_NUMBER() Over (Partition By src.PATNT_REFNO Order By src.START_DTTM Desc,src.PATID_REFNO Desc) As RowNo
	From	Lorenzo.dbo.PatientIdentifier src With (NoLock)
	
			Inner Join PAS.znPatient zn With (NoLock)
				On src.PATNT_REFNO = zn.PATNT_REFNO

	Where	src.ARCHV_FLAG = 'N'
			And src.IDENTIFIER Like 'RM2%'
			And src.PITYP_REFNO = 2001232 -- Patient ID (Facility)
	),
	-- NHSNo
	MyNHSNo As (
	Select	PATNT_REFNO = src.PATNT_REFNO
			,NHSNo = src.IDENTIFIER
			,ROW_NUMBER() Over (Partition By src.PATNT_REFNO Order By src.START_DTTM Desc,src.PATID_REFNO Desc) As RowNo
	From	Lorenzo.dbo.PatientIdentifier src With (NoLock)
	
			Inner Join PAS.znPatient zn With (NoLock)
				On src.PATNT_REFNO = zn.PATNT_REFNO

	Where	src.ARCHV_FLAG = 'N'
			And src.PITYP_REFNO = 1062 -- NHSNo
	),
	-- Merged patients	
	MyMerges As (
	Select	src.PREV_PATNT_REFNO,
			src.PATNT_REFNO,
			src.CREATE_DTTM,
			ROW_NUMBER() Over (Partition By src.PREV_PATNT_REFNO Order By src.CREATE_DTTM Desc) As RowNo
	From	Lorenzo.dbo.MergePatient src With (NoLock)
	Where	src.ARCHV_FLAG = 'N'
	)

Insert Into PAS.Patient(
	PATNT_REFNO
	,LocalPatientIdentifier
	,NHSNo
	,MasterPATNT_REFNO
	,LastLoadedDateTime
	)
Select
	PATNT_REFNO = src.PATNT_REFNO
	,LocalPatientIdentifier = pid.LocalPatientIdentifier
	,NHSNo = nhs.NHSNo
	,MasterPATNT_REFNO = Coalesce(m.PATNT_REFNO,src.PATNT_REFNO)
	,LastLoadedDateTime = GETDATE()

From	Lorenzo.dbo.Patient src With (NoLock)
	
		Inner Join PAS.znPatient zn With (NoLock)
			On src.PATNT_REFNO = zn.PATNT_REFNO

		Left Join Lorenzo.dbo.ExcludedPatient ex With (NoLock)
			On src.PATNT_REFNO = ex.SourcePatientNo

		Inner Join MyPID pid With (NoLock)
			On src.PATNT_REFNO = pid.PATNT_REFNO
			And pid.RowNo = 1

		Left Join MyNHSNo nhs With (NoLock)
			On src.PATNT_REFNO = nhs.PATNT_REFNO
			And nhs.RowNo = 1

		Left Join MyMerges m With (NoLock)
			On src.PATNT_REFNO = m.PREV_PATNT_REFNO
			And m.RowNo = 1

Where	ex.SourcePatientNo Is Null

Select @RowsInserted = @@rowcount

Select @Stats = @Stats + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted)

Select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

Select @Stats = @Stats + 
	'. Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

Exec WriteAuditLogEvent 'PAS - WH LoadPatient', @Stats, @StartTime

Update	src
Set		DateValue = GETDATE()
From	dbo.Parameter src
Where	src.Parameter = 'EXTRACTPATIENT'

End Try

Begin Catch
	Exec msdb.dbo.sp_send_dbmail
	@profile_name = 'Business Intelligence',
	@recipients = 'Business.Intelligence@uhsm.nhs.uk',
	@subject = 'Load Patient failed',
	@body_format = 'HTML',
	@body = 'dbo.LoadPatient failed.<br><br>';
End Catch
