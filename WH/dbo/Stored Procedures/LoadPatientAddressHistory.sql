﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Load Patient Address History

Notes:			Stored in 

				Note: Addresses can be entered by staff into Lorenzo as well as coming from the spine.

Versions:
				1.0.0.2 - 20/01/2016 - MT
					Switched to using the new master net change
					table PAS.znNetChangeByPatient.

				1.0.0.1 - 13/11/2015 - MT
					Use the new PAS.Patient table to obtain
					LocalPatientIdentifier rather than a CTE.

				1.0.0.0 - 22/10/2015 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE procedure [dbo].[LoadPatientAddressHistory]
As

Begin Try

Declare @StartTime datetime
Declare @Elapsed int
Declare @RowsInserted Int
Declare @Stats varchar(255) = ''
Declare @LastRunDateTime datetime

Select @StartTime = getdate()

--Set @LastRunDateTime = '1900-01-01'  -- to perform the initial load.

Set @LastRunDateTime = (
	Select	Max(src.EventTime)
	From	dbo.AuditLog src With (NoLock)
	Where	src.ProcessCode = 'PAS - WH LoadPatientAddressHistory'
	)

-- Load net change table

	Truncate Table PAS.znPatientAddressHistory

	-- Patients
	Insert Into PAS.znPatientAddressHistory(PatientNo,SourceType)
	Select	Distinct src.PATNT_REFNO,'znNetChangeByPatient'
	From	PAS.znNetChangeByPatient src With (NoLock)
	Where	src.LastUpdatedDateTime > @LastRunDateTime

-- Delete existing records identified by net change
Delete	src
From	PAS.PatientAddressHistory src

		Inner Join PAS.znPatientAddressHistory zn With (NoLock)
			On src.PatientNo = zn.PatientNo

-- Load

-- Get all addresses and attach a RowNo to those with the same startdate
;With MyAddress1 As (
	Select	PatientNo = rol.PATNT_REFNO
			,ROW_NUMBER() Over (
				Partition By rol.PATNT_REFNO,DW_REPORTING.LIB.fn_GetJustDate(src.START_DTTM)
				Order By src.ADDSS_REFNO Desc) As RowNo
			,StartDate = DW_REPORTING.LIB.fn_GetJustDate(src.START_DTTM) 
			,EndDate = DW_REPORTING.LIB.fn_GetJustDate(src.END_DTTM)
			,Address1 = src.LINE1
			,Address2 = src.LINE2
			,Address3 = src.LINE3
			,Address4 = src.LINE4
			,Postcode = src.PCODE
			,CountryCode = rv.MAIN_CODE
			,DHACode = HDIST_CODE
			,src.ADDSS_REFNO
	From	Lorenzo.dbo.PatientAddress src With (NoLock)

			Inner Join Lorenzo.dbo.PatientAddressRole rol
				On src.ADDSS_REFNO = rol.ADDSS_REFNO
				And rol.ROTYP_CODE = 'HOME'
				And rol.ARCHV_FLAG = 'N'

			Inner Join PAS.znPatientAddressHistory zn With (NoLock)
				On rol.PATNT_REFNO = zn.PatientNo
			
			Left Join Lorenzo.dbo.ReferenceValue rv
				On src.CNTRY_REFNO = rv.RFVAL_REFNO

	Where	src.ARCHV_FLAG = 'N'
			And src.ADTYP_CODE = 'POSTL'
	),
	-- Only select the latest of any records with the same start date
	MyAddress2 As (
	Select	src.PatientNo
			,ROW_NUMBER() Over (
				Partition By src.PatientNo 
				Order By src.StartDate Asc,
				Case When src.EndDate Is Null Then '2099-12-31' Else src.EndDate End Asc,
				src.ADDSS_REFNO Asc) As RowNoAsc
			,ROW_NUMBER() Over (
				Partition By src.PatientNo 
				Order By src.StartDate Desc,
				Case When src.EndDate Is Null Then '2099-12-31' Else src.EndDate End Desc,
				src.ADDSS_REFNO Desc) As RowNoDesc
			,src.StartDate
			,src.EndDate
			,src.Address1
			,src.Address2
			,src.Address3
			,src.Address4
			,src.Postcode
			,src.CountryCode
			,src.DHACode
			,src.ADDSS_REFNO
	From	MyAddress1 src With (NoLock)
	Where	src.RowNo = 1
	),
	-- Only select records where start date <> end date unless it's the first or last for the patient
	MyAddress3 As (
	Select	src.PatientNo
			,ROW_NUMBER() Over (
				Partition By src.PatientNo 
				Order By src.StartDate Asc,
				Case When src.EndDate Is Null Then '2099-12-31' Else src.EndDate End Asc,
				src.ADDSS_REFNO Asc) As RowNoAsc
			,src.StartDate
			,src.EndDate
			,src.Address1
			,src.Address2
			,src.Address3
			,src.Address4
			,src.Postcode
			,src.CountryCode
			,src.DHACode
			,src.ADDSS_REFNO
	From	MyAddress2 src With (NoLock)
	
			Left Join MyAddress2 previous With (NoLock)
				On src.PatientNo = previous.PatientNo
				And src.RowNoAsc - 1 = previous.RowNoAsc
	
	Where	src.StartDate <> src.EndDate
			Or src.EndDate Is Null
			Or src.RowNoAsc = 1
			Or src.RowNoDesc = 1
	),
	-- Only select records where the address has changed
	MyAddress4 As (
	Select	src.PatientNo
			,ROW_NUMBER() Over (
				Partition By src.PatientNo 
				Order By src.StartDate Asc,
				Case When src.EndDate Is Null Then '2099-12-31' Else src.EndDate End Asc,
				src.ADDSS_REFNO Asc) As RowNoAsc
			,ROW_NUMBER() Over (
				Partition By src.PatientNo 
				Order By src.StartDate Desc,
				Case When src.EndDate Is Null Then '2099-12-31' Else src.EndDate End Desc,
				src.ADDSS_REFNO Desc) As RowNoDesc
			,src.StartDate
			,src.EndDate
			,src.Address1
			,src.Address2
			,src.Address3
			,src.Address4
			,src.Postcode
			,src.CountryCode
			,src.DHACode
			,src.ADDSS_REFNO
	From	MyAddress3 src With (NoLock)
	
			Left Join MyAddress3 previous With (NoLock)
				On src.PatientNo = previous.PatientNo
				And src.RowNoAsc - 1 = previous.RowNoAsc
	
	Where	previous.PatientNo Is Null
			 Or Coalesce(previous.Address1,'') <> Coalesce(src.Address1,'')
			 Or Coalesce(previous.Address2,'') <> Coalesce(src.Address2,'')
			 Or Coalesce(previous.Address3,'') <> Coalesce(src.Address3,'')
			 Or Coalesce(previous.Address4,'') <> Coalesce(src.Address4,'')
			 Or Coalesce(previous.Postcode,'') <> Coalesce(src.Postcode,'')
			 Or Coalesce(previous.CountryCode,'') <> Coalesce(src.CountryCode,'')
	)
	
Insert Into PAS.PatientAddressHistory(
	PatientNo
	,DistrictNo
	,SeqNo
	,StartDate
	,EndDate
	,Address1
	,Address2
	,Address3
	,Address4
	,Postcode
	,CountryCode
	,CountryDescription
	,DHACode
	,CCGCode
	,CurrentFlag
	,ADDSS_REFNO
	,LastLoadedDateTime
	)
Select Distinct 
	src.PatientNo
	,pat.LocalPatientIdentifier
	,SeqNo = src.RowNoAsc
	,src.StartDate
	,EndDate = Case
		-- Set the end date to the start date of the next record if there is one
 		When nextadd.StartDate Is Not Null Then nextadd.StartDate
 		-- Otherwise set the end date to null
		Else Null
	End
	,DW_REPORTING.LIB.fn_ShuffleAddressLines(src.Address1,src.Address2,src.Address3,src.Address4,1)
	,DW_REPORTING.LIB.fn_ShuffleAddressLines(src.Address1,src.Address2,src.Address3,src.Address4,2)
	,DW_REPORTING.LIB.fn_ShuffleAddressLines(src.Address1,src.Address2,src.Address3,src.Address4,3)
	,DW_REPORTING.LIB.fn_ShuffleAddressLines(src.Address1,src.Address2,src.Address3,src.Address4,4)
	,src.Postcode
	,src.CountryCode
	,CountryDescription = rv.[DESCRIPTION]
	,src.DHACode
	,pc.CCGCode
	,CurrentFlag = Case When nextadd.StartDate Is Null Then 'Y' Else 'N' End
	,src.ADDSS_REFNO
	,LastLoadedDateTime = GETDATE()

From	MyAddress4 src With (NoLock)
	
		Left Join Lorenzo.dbo.ExcludedPatient ex With (NoLock)
			On src.PatientNo = ex.SourcePatientNo

		Left Join MyAddress4 nextadd With (NoLock)
			On src.PatientNo = nextadd.PatientNo
			And src.RowNoAsc + 1 = nextadd.RowNoAsc

		Left Join Lorenzo.dbo.ReferenceValue rv With (NoLock)
			On rv.RFVDM_CODE = 'CNTRY'
			And src.CountryCode = rv.MAIN_CODE

		Left Join OrganisationCCG.dbo.Postcode pc With (NoLock)
			On DW_REPORTING.LIB.fn_StandardisePostCode(src.Postcode) = pc.Postcode
			
		Left Join PAS.Patient pat With (NoLock)
			On src.PatientNo = pat.PATNT_REFNO

Where	ex.SourcePatientNo Is Null

Select @RowsInserted = @@rowcount

Select @Stats = @Stats + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted)

Select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

Select @Stats = @Stats + 
	'. Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

Exec WriteAuditLogEvent 'PAS - WH LoadPatientAddressHistory', @Stats, @StartTime

Update	src
Set		DateValue = GETDATE()
From	dbo.Parameter src
Where	src.Parameter = 'EXTRACTPATIENTADDRESSHISTORY'

End Try

Begin Catch
	Exec msdb.dbo.sp_send_dbmail
	@profile_name = 'Business Intelligence',
	@recipients = 'Business.Intelligence@uhsm.nhs.uk',
	@subject = 'Load Patient Address History failed',
	@body_format = 'HTML',
	@body = 'dbo.LoadPatientAddressHistory failed.<br><br>';
End Catch
