﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Load Patient GP History

Notes:			Stored in 
				Previous version took 14 mins to run on average and sometimes more than 50 mins.
				This net change version takes about 30 secs.

				Note: GPs cannot be entered by staff into Lorenzo - they can only come from the spine. 

Versions:
				1.0.0.2 - 20/01/2016 - MT
					Switched to using the new master net change
					table PAS.znNetChangeByPatient.

				1.0.0.1 - 13/11/2015 - MT
					Use the new PAS.Patient table to obtain
					LocalPatientIdentifier rather than a CTE.

				1.0.0.0 - 19/10/2015 - MT
					To replace the LoadPatientGPPracticeHistory proc.
					Also do away with the tables PatientGPPracticeHistory And PatientPracticeHistory
					and retain PatientGPHistory.
					Only storing the date part of the start and end date times from Lorenzo, the
					vast majority are only dates anyway.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE procedure dbo.LoadPatientGPHistory
As

Begin Try

Declare @StartTime datetime
Declare @Elapsed int
Declare @RowsInserted Int
Declare @Stats varchar(255) = ''
Declare @LastRunDateTime datetime

Select @StartTime = getdate()

--Set @LastRunDateTime = '1900-01-01'  -- to perform the initial load.

Set @LastRunDateTime = (
	Select	Max(src.EventTime)
	From	dbo.AuditLog src With (NoLock)
	Where	src.ProcessCode = 'PAS - WH LoadPatientGPHistory'
	)

-- Load net change table

	Truncate Table PAS.znPatientGPHistory

	-- Patients
	Insert Into PAS.znPatientGPHistory(PatientNo,SourceType)
	Select	Distinct src.PATNT_REFNO,'znNetChangeByPatient'
	From	PAS.znNetChangeByPatient src With (NoLock)
	Where	src.LastUpdatedDateTime > @LastRunDateTime

-- Delete existing records identified by net change
Delete	src
From	PAS.PatientGPHistory src

		Inner Join PAS.znPatientGPHistory zn With (NoLock)
			On src.PatientNo = zn.PatientNo

-- Load

-- Get all patient GPs in order
;With MyGP0 As (
	Select	PatientNo = src.PATNT_REFNO
			,ROW_NUMBER() Over (
				Partition By src.PATNT_REFNO,DW_REPORTING.LIB.fn_GetJustDate(src.START_DTTM) 
				Order By src.PATPC_REFNO Desc) As RowNum
			,StartDate = DW_REPORTING.LIB.fn_GetJustDate(src.START_DTTM) 
			,EndDate = DW_REPORTING.LIB.fn_GetJustDate(src.END_DTTM)
			,GPRefNo = src.PROCA_REFNO
			,GPCode = src.PROCA_MAIN_IDENT
			,PracticeRefNo = src.HEORG_REFNO
			,PracticeCode = src.HEORG_MAIN_IDENT
			,src.PATPC_REFNO
	From	Lorenzo.dbo.PatientProfessionalCarer src With (NoLock)

			Inner Join Lorenzo.dbo.Patient pat With (NoLock)
				On src.PATNT_REFNO = pat.PATNT_REFNO
				And pat.ARCHV_FLAG = 'N'

			Inner Join PAS.znPatientGPHistory zn With (NoLock)
				On src.PATNT_REFNO = zn.PatientNo
			
	Where	src.ARCHV_FLAG = 'N'
			And src.PRTYP_MAIN_CODE = 'GMPRC'
	),
	-- Only select the latest of any records with the same start date
	MyGP1 As (
	Select	src.PatientNo
			,ROW_NUMBER() Over (
				Partition By src.PatientNo 
				Order By src.StartDate Asc,
				Case When src.EndDate Is Null Then '2099-12-31' Else src.EndDate End Asc,
				src.PATPC_REFNO Asc) As RowNoAsc
			,ROW_NUMBER() Over (
				Partition By src.PatientNo 
				Order By src.StartDate Desc,
				Case When src.EndDate Is Null Then '2099-12-31' Else src.EndDate End Desc,
				src.PATPC_REFNO Desc) As RowNoDesc
			,src.StartDate
			,src.EndDate
			,src.GPRefNo
			,src.GPCode
			,src.PracticeRefNo
			,src.PracticeCode
			,src.PATPC_REFNO
	From	MyGP0 src With (NoLock)
	Where	src.RowNum = 1
	),
	-- Only select records where start date <> end date unless it's the first or last for the patient
	MyGP2 As (
	Select	src.PatientNo
			,ROW_NUMBER() Over (
				Partition By src.PatientNo 
				Order By src.StartDate Asc,
				Case When src.EndDate Is Null Then '2099-12-31' Else src.EndDate End Asc,
				src.PATPC_REFNO Asc) As RowNoAsc
			,src.StartDate
			,src.EndDate
			,src.GPRefNo
			,src.GPCode
			,src.PracticeRefNo
			,src.PracticeCode
			,src.PATPC_REFNO
	From	MyGP1 src With (NoLock)
	
			Left Join MyGP1 previous With (NoLock)
				On src.PatientNo = previous.PatientNo
				And src.RowNoAsc - 1 = previous.RowNoAsc
	
	Where	src.StartDate <> src.EndDate
			Or src.EndDate Is Null
			Or src.RowNoAsc = 1
			Or src.RowNoDesc = 1
	),
	-- Only select records where the GP or Practice has changed
	MyGP3 As (
	Select	src.PatientNo
			,ROW_NUMBER() Over (
				Partition By src.PatientNo 
				Order By src.StartDate Asc,
				Case When src.EndDate Is Null Then '2099-12-31' Else src.EndDate End Asc,
				src.PATPC_REFNO Asc) As RowNoAsc
			,ROW_NUMBER() Over (
				Partition By src.PatientNo 
				Order By src.StartDate Desc,
				Case When src.EndDate Is Null Then '2099-12-31' Else src.EndDate End Desc,
				src.PATPC_REFNO Desc) As RowNoDesc
			,src.StartDate
			,src.EndDate
			,src.GPRefNo
			,src.GPCode
			,src.PracticeRefNo
			,src.PracticeCode
			,src.PATPC_REFNO
	From	MyGP2 src With (NoLock)
	
			Left Join MyGP2 previous With (NoLock)
				On src.PatientNo = previous.PatientNo
				And src.RowNoAsc - 1 = previous.RowNoAsc
	
	Where	previous.PatientNo Is Null
			 Or Coalesce(previous.GPCode,'') <> Coalesce(src.GPCode,'')
			 Or Coalesce(previous.PracticeCode,'') <> Coalesce(src.PracticeCode,'')
	)
	
Insert Into PAS.PatientGPHistory(
	PatientNo
	,DistrictNo
	,SeqNo
	,StartDate
	,EndDate
	,GPRefno
	,GPCode
	,PracticeRefno
	,PracticeCode
	,CurrentFlag
	,PATPC_REFNO
	,LastLoadedDateTime
	)
Select
	src.PatientNo
	,pat.LocalPatientIdentifier
	,src.RowNoAsc
	,src.StartDate
	,Case
		-- Set the end date to the start date of the next record if there is one
 		When gp3.StartDate Is Not Null Then gp3.StartDate
 		-- Otherwise set the end date to null
		Else Null
	End
	,src.GPRefno
	,src.GPCode
	,src.PracticeRefNo
	,src.PracticeCode
	,Case When gp3.StartDate Is Null Then 'Y' Else 'N' End
	,src.PATPC_REFNO
	,GETDATE()

From	MyGP3 src With (NoLock)
	
		Left Join Lorenzo.dbo.ExcludedPatient ex With (NoLock)
			On src.PatientNo = ex.SourcePatientNo

		Left Join MyGP3 gp3 With (NoLock)
			On src.PatientNo = gp3.PatientNo
			And src.RowNoAsc + 1 = gp3.RowNoAsc

		Left Join PAS.Patient pat With (NoLock)
			On src.PatientNo = pat.PATNT_REFNO

Where	ex.SourcePatientNo Is Null

Select @RowsInserted = @@rowcount

Select @Stats = @Stats + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted)

Select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

Select @Stats = @Stats + 
	'. Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

Exec WriteAuditLogEvent 'PAS - WH LoadPatientGPHistory', @Stats, @StartTime

Update	src
Set		DateValue = GETDATE()
From	dbo.Parameter src
Where	src.Parameter = 'EXTRACTPATIENTGPHISTORY'

End Try

Begin Catch
	Exec msdb.dbo.sp_send_dbmail
	@profile_name = 'Business Intelligence',
	@recipients = 'Business.Intelligence@uhsm.nhs.uk',
	@subject = 'Load Patient GP History failed',
	@body_format = 'HTML',
	@body = 'dbo.LoadPatientGPHistory failed.<br><br>';
End Catch
