﻿
CREATE PROCEDURE [dbo].[LoadRFEncounter]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


--delete archived activity
delete from RF.Encounter 
where	
	exists
	(
	select
		1
	from
		dbo.TLoadRFEncounter
	where
		TLoadRFEncounter.SourceUniqueID = Encounter.SourceUniqueID
	and	TLoadRFEncounter.ArchiveFlag = 1
	)


SELECT @RowsDeleted = @@Rowcount

update RF.Encounter
set
	 SourceUniqueID = TEncounter.SourceUniqueID
	,SourcePatientNo = TEncounter.SourcePatientNo
	,SourceEncounterNo = TEncounter.SourceEncounterNo
	,PatientTitle = TEncounter.PatientTitle
	,PatientForename = TEncounter.PatientForename
	,PatientSurname = TEncounter.PatientSurname
	,DateOfBirth = TEncounter.DateOfBirth
	,DateOfDeath = TEncounter.DateOfDeath
	,SexCode = TEncounter.SexCode
	,NHSNumber = TEncounter.NHSNumber
	,NHSNumberStatusCode = TEncounter.NHSNumberStatusCode
	,DistrictNo = TEncounter.DistrictNo
	,Postcode = TEncounter.Postcode
	,PatientAddress1 = TEncounter.PatientAddress1
	,PatientAddress2 = TEncounter.PatientAddress2
	,PatientAddress3 = TEncounter.PatientAddress3
	,PatientAddress4 = TEncounter.PatientAddress4
	,DHACode = TEncounter.DHACode
	,EthnicOriginCode = TEncounter.EthnicOriginCode
	,MaritalStatusCode = TEncounter.MaritalStatusCode
	,ReligionCode = TEncounter.ReligionCode
	,RegisteredGpCode = TEncounter.RegisteredGpCode
	,RegisteredGpPracticeCode = TEncounter.RegisteredGpPracticeCode
	,EpisodicGpCode = TEncounter.EpisodicGpCode
	,EpisodicGpPracticeCode = TEncounter.EpisodicGpPracticeCode
	,EpisodicGdpCode = TEncounter.EpisodicGdpCode
	,SiteCode = TEncounter.SiteCode
	,ConsultantCode = TEncounter.ConsultantCode
	,SpecialtyCode = TEncounter.SpecialtyCode
	,SourceOfReferralCode = TEncounter.SourceOfReferralCode
	,PriorityCode = TEncounter.PriorityCode
	,ReferralDate = TEncounter.ReferralDate
	,DischargeDate = TEncounter.DischargeDate
	,DischargeTime = TEncounter.DischargeTime
	,DischargeReasonCode = TEncounter.DischargeReasonCode
	,DischargeReason = TEncounter.DischargeReason
	,AdminCategoryCode = TEncounter.AdminCategoryCode
	,ContractSerialNo = TEncounter.ContractSerialNo
	,RTTPathwayID = TEncounter.RTTPathwayID
	,RTTPathwayCondition = TEncounter.RTTPathwayCondition
	,RTTStartDate = TEncounter.RTTStartDate
	,RTTEndDate = TEncounter.RTTEndDate
	,RTTSpecialtyCode = TEncounter.RTTSpecialtyCode
	,RTTCurrentProviderCode = TEncounter.RTTCurrentProviderCode
	,RTTCurrentStatusCode = TEncounter.RTTCurrentStatusCode
	,RTTCurrentStatusDate = TEncounter.RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag = TEncounter.RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag = TEncounter.RTTOverseasStatusFlag
	,NextFutureAppointmentDate = TEncounter.NextFutureAppointmentDate
	,InterfaceCode = TEncounter.InterfaceCode
	,PASCreated = TEncounter.PASCreated
	,PASUpdated = TEncounter.PASUpdated
	,PASCreatedByWhom = TEncounter.PASCreatedByWhom
	,PASUpdatedByWhom = TEncounter.PASUpdatedByWhom
	,OriginalProviderReferralDate = TEncounter.OriginalProviderReferralDate
	,ParentSourceUniqueID = TEncounter.ParentSourceUniqueID

	,Updated = getdate()
	,ByWhom = system_user
	,RequestedService = TEncounter.RequestedService
	,ReferralStatus = TEncounter.ReferralStatus
	,CancellationDate = TEncounter.CancellationDate
	,CancellationReason = TEncounter.CancellationReason
	,ReferralMedium = TEncounter.ReferralMedium
	,ReferredByConsultantCode = TEncounter.referredByConsultantCode
	,ReferredByOrganisationCode = TEncounter.ReferredByOrganisationCode
	,ReferredBySpecialtyCode = TEncounter.ReferredBySpecialtyCode
	,SuspectedCancerTypeCode = TEncounter.SuspectedCancerTypeCode
	,ReferralSentDate = TEncounter.ReferralSentDate
	,PatientDeceased = TEncounter.PatientDeceased
	,GeneralComment = TEncounter.GeneralComment
	,ReferralComment= TEncounter.ReferralComment
from
	dbo.TLoadRFEncounter TEncounter
where
	TEncounter.SourceUniqueID = RF.Encounter.SourceUniqueID

select @RowsUpdated = @@rowcount


INSERT INTO RF.Encounter
	(
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,NHSNumberStatusCode
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,EpisodicGdpCode
	,SiteCode
	,ConsultantCode
	,SpecialtyCode
	,SourceOfReferralCode
	,PriorityCode
	,ReferralDate
	,DischargeDate
	,DischargeTime
	,DischargeReasonCode
	,DischargeReason
	,AdminCategoryCode
	,ContractSerialNo
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NextFutureAppointmentDate
	,InterfaceCode
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,Created
	,ByWhom
	,OriginalProviderReferralDate
	,ParentSourceUniqueID
	,RequestedService
	,ReferralStatus
	,CancellationDate
	,CancellationReason
	,ReferralMedium
	,ReferredByConsultantCode
	,ReferredByOrganisationCode
	,ReferredBySpecialtyCode
	,SuspectedCancerTypeCode
	,ReferralSentDate
	,PatientDeceased
	,GeneralComment
	,ReferralComment
	) 
select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,NHSNumberStatusCode
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,EpisodicGdpCode
	,SiteCode
	,ConsultantCode
	,SpecialtyCode
	,SourceOfReferralCode
	,PriorityCode
	,ReferralDate
	,DischargeDate
	,DischargeTime
	,DischargeReasonCode
	,DischargeReason
	,AdminCategoryCode
	,ContractSerialNo
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NextFutureAppointmentDate
	,InterfaceCode
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom

	,Created = getdate()
	,ByWhom = system_user

	,OriginalProviderReferralDate
	,ParentSourceUniqueID
	,RequestedService
	,ReferralStatus
	,CancellationDate
	,CancellationReason
	,ReferralMedium
	,ReferredByConsultantCode
	,ReferredByOrganisationCode
	,ReferredBySpecialtyCode
	,SuspectedCancerTypeCode
	,ReferralSentDate
	,PatientDeceased
	,GeneralComment
	,ReferralComment
from
	dbo.TLoadRFEncounter
where
	not exists
	(
	select
		1
	from
		RF.Encounter
	where
		Encounter.SourceUniqueID = TLoadRFEncounter.SourceUniqueID
	)
and	TLoadRFEncounter.ArchiveFlag = 0


SELECT @RowsInserted = @@Rowcount

--remove remaining test patients
delete from rf.Encounter
--select * 
from RF.Encounter encounter
inner join Lorenzo.dbo.ExcludedPatient ex
	on ex.SourcePatientNo = encounter.SourcePatientNo


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Updated '  + CONVERT(varchar(10), @RowsUpdated) +  
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'PAS - WH LoadRFEncounter', @Stats, @StartTime

print @Stats






