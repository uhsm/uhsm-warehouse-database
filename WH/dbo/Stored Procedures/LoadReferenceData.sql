﻿CREATE       procedure [dbo].[LoadReferenceData] 
	 @from smalldatetime = null
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

select
	@from = 
		coalesce(
			@from
			,(
			select
				DateValue
			from
				dbo.Parameter
			where
				Parameter = 'EXTRACTREFERENCEDATADATE'
			)
		)

--ReferenceValue
delete
	PAS.ReferenceValueBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.ReferenceValue ReferenceValue
	where
		ReferenceValue.RFVAL_REFNO = ReferenceValueBase.RFVAL_REFNO
	--and	ReferenceValue.Created >= @from
	)

insert into
	PAS.ReferenceValueBase
select
	*
from
	Lorenzo.dbo.ReferenceValue ReferenceValue
--where
--	ReferenceValue.Created >= @from


--ReferenceValueIdentifier
delete
	PAS.ReferenceValueIdentifierBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.ReferenceValueIdentifier ReferenceValueIdentifier
	where
		ReferenceValueIdentifier.RFVAL_REFNO = ReferenceValueIdentifierBase.RFVAL_REFNO
	--and	ReferenceValueIdentifier.Created >= @from
	)

insert into
	PAS.ReferenceValueIdentifierBase
select
	*
from
	Lorenzo.dbo.ReferenceValueIdentifier ReferenceValueIdentifier
--where
--	ReferenceValueIdentifier.Created >= @from



--RTT
delete
	PAS.RTTBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.RTT RTT
	where
		RTT.RTTPR_REFNO = RTTBase.RTTPR_REFNO
	and	RTT.Created >= @from
	)

insert into
	PAS.RTTBase
select
	*
from
	Lorenzo.dbo.RTT RTT
where
	RTT.Created >= @from

--merge PAS RTT details with manual details
exec BuildRTT

--used to simplify and optimise RTT queries
exec BuildRTTFlag

--now done in extract
----update the RTT status columns

----APC Spells
--update
--	APC.Encounter
--set
--	 RTTPeriodStatusCode = PeriodStatus.RTTStatusCode
--	,RTTCurrentStatusCode = CurrentStatus.RTTStatusCode
--	,RTTCurrentStatusDate = CurrentStatus.StartDate
--from
--	APC.Encounter

--inner join PAS.RTT PeriodStatus
--on	PeriodStatus.SourceCode = 'PRVSP'
--and	PeriodStatus.SourceRecno = Encounter.SourceSpellNo

--inner join PAS.RTT CurrentStatus
--on	CurrentStatus.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
--and	not exists
--	(
--	select
--		1
--	from
--		PAS.RTT Previous
--	where
--		Previous.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
--	and	(
--			Previous.StartDate > CurrentStatus.StartDate
--		or	(
--				Previous.StartDate = CurrentStatus.StartDate
--			and	Previous.SourceUniqueID > CurrentStatus.SourceUniqueID
--			)
--		)
--	)

----APC Episode
--update
--	APC.Encounter
--set
--	 RTTPeriodStatusCode = PeriodStatus.RTTStatusCode
--	,RTTCurrentStatusCode = CurrentStatus.RTTStatusCode
--	,RTTCurrentStatusDate = CurrentStatus.StartDate
--from
--	APC.Encounter

--inner join PAS.RTT PeriodStatus
--on	PeriodStatus.SourceCode = 'PRCAE'
--and	PeriodStatus.SourceRecno = Encounter.SourceUniqueID

--inner join PAS.RTT CurrentStatus
--on	CurrentStatus.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
--and	not exists
--	(
--	select
--		1
--	from
--		PAS.RTT Previous
--	where
--		Previous.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
--	and	(
--			Previous.StartDate > CurrentStatus.StartDate
--		or	(
--				Previous.StartDate = CurrentStatus.StartDate
--			and	Previous.SourceUniqueID > CurrentStatus.SourceUniqueID
--			)
--		)
--	)

----Outpatients
--update
--	OP.Encounter
--set
--	 RTTPeriodStatusCode = PeriodStatus.RTTStatusCode
--	,RTTCurrentStatusCode = CurrentStatus.RTTStatusCode
--	,RTTCurrentStatusDate = CurrentStatus.StartDate
--from
--	OP.Encounter

--inner join PAS.RTT PeriodStatus
--on	PeriodStatus.SourceCode = 'SCHDL'
--and	PeriodStatus.SourceRecno = Encounter.SourceUniqueID

--inner join PAS.RTT CurrentStatus
--on	CurrentStatus.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
--and	not exists
--	(
--	select
--		1
--	from
--		PAS.RTT Previous
--	where
--		Previous.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
--	and	(
--			Previous.StartDate > CurrentStatus.StartDate
--		or	(
--				Previous.StartDate = CurrentStatus.StartDate
--			and	Previous.SourceUniqueID > CurrentStatus.SourceUniqueID
--			)
--		)
--	)


--ProfessionalCarer
delete
	PAS.ProfessionalCarerBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.ProfessionalCarer ProfessionalCarer
	where
		ProfessionalCarer.PROCA_REFNO = ProfessionalCarerBase.PROCA_REFNO
	and	ProfessionalCarer.Created >= @from
	)

insert into
	PAS.ProfessionalCarerBase
select
	*
from
	Lorenzo.dbo.ProfessionalCarer ProfessionalCarer
where
	ProfessionalCarer.Created >= @from

--declare @tmpfrom smalldatetime;
--set @tmpfrom = '2009-10-01'
--ProfessionalCarerSpecialty
delete
	PAS.ProfessionalCarerSpecialtyBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.ProfessionalCarerSpecialty ProfessionalCarerSpecialty
	where
		ProfessionalCarerSpecialty.PROCA_REFNO = ProfessionalCarerSpecialtyBase.PROCA_REFNO
	and	ProfessionalCarerSpecialty.Created >= @from
	)

insert into
	PAS.ProfessionalCarerSpecialtyBase
select
	*
from
	Lorenzo.dbo.ProfessionalCarerSpecialty ProfessionalCarerSpecialty
where
	ProfessionalCarerSpecialty.Created >= @from

--Specialty
delete
	PAS.SpecialtyBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.Specialty Specialty
	where
		Specialty.SPECT_REFNO = SpecialtyBase.SPECT_REFNO
	and	Specialty.Created >= @from
	)

insert into
	PAS.SpecialtyBase
select
	*
from
	Lorenzo.dbo.Specialty Specialty
where
	Specialty.Created >= @from



--Organisation
delete
	PAS.OrganisationBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.Organisation Organisation
	where
		Organisation.HEORG_REFNO = OrganisationBase.HEORG_REFNO
	and	Organisation.Created >= @from
	)

insert into
	PAS.OrganisationBase
select
	*
from
	Lorenzo.dbo.Organisation Organisation
where
	Organisation.Created >= @from




--ServicePoint
delete
	PAS.ServicePointBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.ServicePoint ServicePoint
	where
		ServicePoint.SPONT_REFNO = ServicePointBase.SPONT_REFNO
	and	ServicePoint.Created >= @from
	)

insert into
	PAS.ServicePointBase
SELECT [SPONT_REFNO]
      ,[CODE]
      ,[NAME]
      ,ServicePoint.[SPECT_REFNO]
      ,Specialty.MAIN_IDENT As [SPECT_REFNO_MAIN_IDENT]
      ,[STEAM_REFNO]
      ,NULL As [STEAM_REFNO_CODE]
      ,NULL As [STEAM_REFNO_NAME]
      ,[PROCA_REFNO]
      ,NULL AS [PROCA_REFNO_MAIN_IDENT]
      ,ServicePoint.[HEORG_REFNO]
      ,Organisation.MAIN_IDENT AS [HEORG_REFNO_MAIN_IDENT]--CM 20/06/2016 need to look this up from Organisation reference table as the new extracts no longer supply this column
      ,[CODE]
      ,[NOMINAL_DEPT_CODE]
      ,ServicePoint.[DESCRIPTION]
      ,[NAME]
      ,ServicePoint.[START_DTTM]
      ,ServicePoint.[END_DTTM]
      ,[PURPS_REFNO]
      ,NULL AS [PURPS_REFNO_MAIN_CODE]
      ,NULL AS[PURPS_REFNO_DESCRIPTION]
      ,[SPTYP_REFNO]
      ,SPTYP.MAIN_CODE AS [SPTYP_REFNO_MAIN_CODE] --CM 20/06/2016 need to look this up from  reference table as the new extracts no longer supply this column
      ,SPTYP.[DESCRIPTION] AS[SPTYP_REFNO_DESCRIPTION]--CM 20/06/2016 need to look this up from reference table as the new extracts no longer supply this column
      ,[AE_FLAG]
      ,ServicePoint.[PDTYP_REFNO]
      ,PDTYP.MAIN_CODE AS[PDTYP_REFNO_MAIN_CODE]
      ,PDTYP.[DESCRIPTION] AS[PDTYP_REFNO_DESCRIPTION]
      ,ServicePoint.[CREATE_DTTM]
      ,ServicePoint.[MODIF_DTTM]
      ,ServicePoint.[USER_CREATE]
      ,ServicePoint.[USER_MODIF]
      ,ServicePoint.[ARCHV_FLAG]
      ,[USE_BED_MANAGEMENT]
      ,[HORIZ_VALUE]
      ,[AGE_TYPE]
      ,[AGE_QUALIFIER]
      ,[WARN_LEVEL]
      ,[MAX_LEVEL]
      ,[FCPUR_REFNO]
      ,FCPUR.MAIN_CODE AS[FCPUR_REFNO_MAIN_CODE]--CM 20/06/2016 need to look this up from reference table as the new extracts no longer supply this column
      ,FCPUR.[DESCRIPTION] AS[FCPUR_REFNO_DESCRIPTION]--CM 20/06/2016 need to look this up from reference table as the new extracts no longer supply this column
      ,[EXCLUDE_HOLS]
      ,[RESCH_MAX_DIFF]
      ,ServicePoint.[OWNER_HEORG_REFNO]
      ,OwnerHeorg.MAIN_IDENT AS[OWNER_HEORG_REFNO_MAIN_IDENT] --CM 20/06/2016 need to look this up from Organisation reference table as the new extracts no longer supply this column
      ,[INSTRUCTIONS]
      ,[CHG_AC_AUTO]
      ,[PBK_FLAG]
      ,[PBK_LEAD_TIME]
      ,[PBK_LEAD_TIME_UNITS]
      ,[CONTACT_FLAG]
      ,[LEAD_TIME_CLIN_FLAG]
      ,ServicePoint.[Created]
from
 Lorenzo.dbo.ServicePoint ServicePoint
 Left outer join (Select * from Lorenzo.dbo.Organisation where ARCHV_FLAG = 'N') Organisation on ServicePoint.HEORG_REFNO = Organisation.HEORG_REFNO
 Left outer join (Select * from Lorenzo.dbo.ReferenceValue where RFVDM_CODE = 'SPTYP' AND ARCHV_FLAG = 'N')  SPTYP on ServicePoint.SPTYP_REFNO = SPTYP.RFVAL_REFNO
 Left outer join (Select * from Lorenzo.dbo.ReferenceValue where RFVDM_CODE ='PDTYP' AND ARCHV_FLAG = 'N') PDTYP on ServicePoint.PDTYP_REFNO = PDTYP.RFVAL_REFNO
 Left outer join (Select * from Lorenzo.dbo.Organisation where ARCHV_FLAG = 'N') as  OwnerHeorg on ServicePoint.HEORG_REFNO = OwnerHeorg.HEORG_REFNO
 Left outer join (Select * from Lorenzo.dbo.ReferenceValue where RFVDM_CODE ='FCPUR' AND ARCHV_FLAG = 'N')  FCPUR on ServicePoint.FCPUR_REFNO = FCPUR.RFVAL_REFNO
 Left outer join (Select * from Lorenzo.dbo.Specialty where ARCHV_FLAG = 'N') Specialty on ServicePoint.SPECT_REFNO = Specialty.SPECT_REFNO
 where
	ServicePoint.Created >=  @from


--ClinicalCoding
delete
	PAS.ClinicalCodingBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.ClinicalCoding ClinicalCoding
	where
		ClinicalCoding.DGPRO_REFNO = ClinicalCodingBase.DGPRO_REFNO
	and	ClinicalCoding.Created >= @from
	)

insert into
	PAS.ClinicalCodingBase
select
	*
from
	Lorenzo.dbo.ClinicalCoding ClinicalCoding
where
	ClinicalCoding.Created >= @from


--Coding reference data
delete
	PAS.CodingBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.Diagnosis Diagnosis
	where
		Diagnosis.ODPCD_REFNO = CodingBase.ODPCD_REFNO
	and	Diagnosis.Created >= @from
	)

insert into
	PAS.CodingBase
select [ODPCD_REFNO]
      ,[CODE]
      ,[CCSXT_CODE]
      ,[DESCRIPTION]
      ,[SUPL_CODE]
      ,[SUPL_CCSXT_CODE]
      ,[PARNT_REFNO]
      ,[PRICE]
      ,[CREATE_DTTM]
      ,[MODIF_DTTM]
      ,[USER_CREATE]
      ,[USER_MODIF]
      ,[SUPL_DESCRIPTION]
      ,[ARCHV_FLAG]
      ,[STRAN_REFNO]
      ,[PRIOR_POINTER]
      ,[EXTERNAL_KEY]
      ,[TRAVERSE_ONLY_FLAG]
      ,[DURATION]
      ,[CITEM_REFNO]
      ,[BLOCK_NUMBER]
      ,[CDTYP_REFNO]
      ,[START_DTTM]
      ,[END_DTTM]
      ,[SCLVL_REFNO]
      ,[SYN_CODE]
      ,[OWNER_HEORG_REFNO]
      ,[Created]
from
 Lorenzo.dbo.Diagnosis Diagnosis
where
	Diagnosis.Created >= @from


--Rules
delete
	PAS.RuleBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.[rule]
	where
		Lorenzo.dbo.[rule].RULES_REFNO = RuleBase.RULES_REFNO
	and	RuleBase.Created >= @from
	)


insert into
	PAS.RuleBase
select
	*
from
	Lorenzo.dbo.[Rule] [Rule]
where
	[Rule].Created >= @from


--Waiting List Rules
delete
	PAS.WaitingListRuleBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.WaitingListRule
	where
		WaitingListRule.WLRUL_REFNO = WaitingListRuleBase.WLRUL_REFNO
	--and	WaitingListRuleBase.Created >= @from
	)


insert into
	PAS.WaitingListRuleBase
Select  [WLRUL_REFNO]
      ,[SPECT_REFNO]
      ,[PROCA_REFNO]
      ,[HEORG_REFNO]
      ,[MWAIT_DAYCASE]
      ,[MWAIT_ORDINARY]
      ,[CREATE_DTTM]
      ,[MODIF_DTTM]
      ,[USER_CREATE]
      ,[USER_MODIF]
      ,[CODE]
      ,[NAME]
      ,[COMMENTS]
      ,[STRAN_REFNO]
      ,[PRIOR_POINTER]
      ,[ARCHV_FLAG]
      ,[EXTERNAL_KEY]
      ,[SVTYP_REFNO]
      ,[SPONT_REFNO]
      ,[LOCAL_FLAG]
      ,[END_DTTM]
      ,[OWNER_HEORG_REFNO]
      ,[Created]
from
 Lorenzo.dbo.WaitingListRule
--where
--	WaitingListRule.Created >= @from


--Purchaser
delete
	PAS.PurchaserBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.Purchaser Purchaser
	where
		Purchaser.PURCH_REFNO = PurchaserBase.PURCH_REFNO
	and	Purchaser.Created >= @from
	)

insert into
	PAS.PurchaserBase
select
	*
from
	Lorenzo.dbo.Purchaser Purchaser
where
	Purchaser.Created >= @from


update dbo.Parameter
set
	DateValue = @StartTime
where
	Parameter = 'EXTRACTREFERENCEDATADATE'

if @@rowcount = 0

insert into dbo.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'EXTRACTREFERENCEDATADATE'
	,DateValue = @StartTime


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, ''))

exec WriteAuditLogEvent 'PAS - WH LoadReferenceData', @Stats, @StartTime
