﻿CREATE       procedure [dbo].[LoadReferenceDataAll] 
	 @from smalldatetime = null
as
--KO 20/2/2013: Copy of LoadReferenceData with all date parameters removed. Required to do full refresh
-- of reference data periodically.
--Not run daily because of performance (takes ~30 mins to run)
declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

select
	@from = 
		coalesce(
			@from
			,(
			select
				DateValue
			from
				dbo.Parameter
			where
				Parameter = 'EXTRACTREFERENCEDATADATE'
			)
		)

--ReferenceValue
delete
	PAS.ReferenceValueBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.ReferenceValue ReferenceValue
	where
		ReferenceValue.RFVAL_REFNO = ReferenceValueBase.RFVAL_REFNO
	--and	ReferenceValue.Created >= @from
	)

insert into
	PAS.ReferenceValueBase
select
	*
from
	Lorenzo.dbo.ReferenceValue ReferenceValue
--where
--	ReferenceValue.Created >= @from


--ReferenceValueIdentifier
delete
	PAS.ReferenceValueIdentifierBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.ReferenceValueIdentifier ReferenceValueIdentifier
	where
		ReferenceValueIdentifier.RFVAL_REFNO = ReferenceValueIdentifierBase.RFVAL_REFNO
	--and	ReferenceValueIdentifier.Created >= @from
	)

insert into
	PAS.ReferenceValueIdentifierBase
select
	*
from
	Lorenzo.dbo.ReferenceValueIdentifier ReferenceValueIdentifier
--where
--	ReferenceValueIdentifier.Created >= @from



--RTT
delete
	PAS.RTTBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.RTT RTT
	where
		RTT.RTTPR_REFNO = RTTBase.RTTPR_REFNO
	--and	RTT.Created >= @from
	)

insert into
	PAS.RTTBase
select
	*
from
	Lorenzo.dbo.RTT RTT
--where
--	RTT.Created >= @from

--merge PAS RTT details with manual details
exec BuildRTT

--used to simplify and optimise RTT queries
exec BuildRTTFlag

--now done in extract
----update the RTT status columns

----APC Spells
--update
--	APC.Encounter
--set
--	 RTTPeriodStatusCode = PeriodStatus.RTTStatusCode
--	,RTTCurrentStatusCode = CurrentStatus.RTTStatusCode
--	,RTTCurrentStatusDate = CurrentStatus.StartDate
--from
--	APC.Encounter

--inner join PAS.RTT PeriodStatus
--on	PeriodStatus.SourceCode = 'PRVSP'
--and	PeriodStatus.SourceRecno = Encounter.SourceSpellNo

--inner join PAS.RTT CurrentStatus
--on	CurrentStatus.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
--and	not exists
--	(
--	select
--		1
--	from
--		PAS.RTT Previous
--	where
--		Previous.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
--	and	(
--			Previous.StartDate > CurrentStatus.StartDate
--		or	(
--				Previous.StartDate = CurrentStatus.StartDate
--			and	Previous.SourceUniqueID > CurrentStatus.SourceUniqueID
--			)
--		)
--	)

----APC Episode
--update
--	APC.Encounter
--set
--	 RTTPeriodStatusCode = PeriodStatus.RTTStatusCode
--	,RTTCurrentStatusCode = CurrentStatus.RTTStatusCode
--	,RTTCurrentStatusDate = CurrentStatus.StartDate
--from
--	APC.Encounter

--inner join PAS.RTT PeriodStatus
--on	PeriodStatus.SourceCode = 'PRCAE'
--and	PeriodStatus.SourceRecno = Encounter.SourceUniqueID

--inner join PAS.RTT CurrentStatus
--on	CurrentStatus.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
--and	not exists
--	(
--	select
--		1
--	from
--		PAS.RTT Previous
--	where
--		Previous.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
--	and	(
--			Previous.StartDate > CurrentStatus.StartDate
--		or	(
--				Previous.StartDate = CurrentStatus.StartDate
--			and	Previous.SourceUniqueID > CurrentStatus.SourceUniqueID
--			)
--		)
--	)

----Outpatients
--update
--	OP.Encounter
--set
--	 RTTPeriodStatusCode = PeriodStatus.RTTStatusCode
--	,RTTCurrentStatusCode = CurrentStatus.RTTStatusCode
--	,RTTCurrentStatusDate = CurrentStatus.StartDate
--from
--	OP.Encounter

--inner join PAS.RTT PeriodStatus
--on	PeriodStatus.SourceCode = 'SCHDL'
--and	PeriodStatus.SourceRecno = Encounter.SourceUniqueID

--inner join PAS.RTT CurrentStatus
--on	CurrentStatus.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
--and	not exists
--	(
--	select
--		1
--	from
--		PAS.RTT Previous
--	where
--		Previous.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
--	and	(
--			Previous.StartDate > CurrentStatus.StartDate
--		or	(
--				Previous.StartDate = CurrentStatus.StartDate
--			and	Previous.SourceUniqueID > CurrentStatus.SourceUniqueID
--			)
--		)
--	)


--ProfessionalCarer
delete
	PAS.ProfessionalCarerBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.ProfessionalCarer ProfessionalCarer
	where
		ProfessionalCarer.PROCA_REFNO = ProfessionalCarerBase.PROCA_REFNO
	--and	ProfessionalCarer.Created >= @from
	)

insert into
	PAS.ProfessionalCarerBase
select
	*
from
	Lorenzo.dbo.ProfessionalCarer ProfessionalCarer
--where
--	ProfessionalCarer.Created >= @from

--declare @tmpfrom smalldatetime;
--set @tmpfrom = '2009-10-01'
--ProfessionalCarerSpecialty
delete
	PAS.ProfessionalCarerSpecialtyBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.ProfessionalCarerSpecialty ProfessionalCarerSpecialty
	where
		ProfessionalCarerSpecialty.PROCA_REFNO = ProfessionalCarerSpecialtyBase.PROCA_REFNO
	--and	ProfessionalCarerSpecialty.Created >= @from
	)

insert into
	PAS.ProfessionalCarerSpecialtyBase
select
	*
from
	Lorenzo.dbo.ProfessionalCarerSpecialty ProfessionalCarerSpecialty
--where
--	ProfessionalCarerSpecialty.Created >= @from

--Specialty
delete
	PAS.SpecialtyBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.Specialty Specialty
	where
		Specialty.SPECT_REFNO = SpecialtyBase.SPECT_REFNO
	--and	Specialty.Created >= @from
	)

insert into
	PAS.SpecialtyBase
select
	*
from
	Lorenzo.dbo.Specialty Specialty
--where
--	Specialty.Created >= @from


--ServicePoint
delete
	PAS.ServicePointBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.ServicePoint ServicePoint
	where
		ServicePoint.SPONT_REFNO = ServicePointBase.SPONT_REFNO
	--and	ServicePoint.Created >= @from
	)

insert into
	PAS.ServicePointBase
select
	*
from
	Lorenzo.dbo.ServicePoint ServicePoint
--where
--	ServicePoint.Created >= @from



--Organisation
delete
	PAS.OrganisationBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.Organisation Organisation
	where
		Organisation.HEORG_REFNO = OrganisationBase.HEORG_REFNO
	--and	Organisation.Created >= @from
	)

insert into
	PAS.OrganisationBase
select
	*
from
	Lorenzo.dbo.Organisation Organisation
--where
--	Organisation.Created >= @from


--ClinicalCoding
delete
	PAS.ClinicalCodingBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.ClinicalCoding ClinicalCoding
	where
		ClinicalCoding.DGPRO_REFNO = ClinicalCodingBase.DGPRO_REFNO
	--and	ClinicalCoding.Created >= @from
	)

insert into
	PAS.ClinicalCodingBase
select
	*
from
	Lorenzo.dbo.ClinicalCoding ClinicalCoding
--where
--	ClinicalCoding.Created >= @from


--Coding reference data
delete
	PAS.CodingBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.Diagnosis Diagnosis
	where
		Diagnosis.ODPCD_REFNO = CodingBase.ODPCD_REFNO
	--and	Diagnosis.Created >= @from
	)

insert into
	PAS.CodingBase
select
	*
from
	Lorenzo.dbo.Diagnosis Diagnosis
--where
--	Diagnosis.Created >= @from


--Rules
delete
	PAS.RuleBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.[rule]
	where
		Lorenzo.dbo.[rule].RULES_REFNO = RuleBase.RULES_REFNO
	--and	RuleBase.Created >= @from
	)


insert into
	PAS.RuleBase
select
	*
from
	Lorenzo.dbo.[Rule] [Rule]
--where
--	[Rule].Created >= @from


--Waiting List Rules
delete
	PAS.WaitingListRuleBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.WaitingListRule
	where
		WaitingListRule.WLRUL_REFNO = WaitingListRuleBase.WLRUL_REFNO
	--and	WaitingListRuleBase.Created >= @from
	)


insert into
	PAS.WaitingListRuleBase
select
	*
from
	Lorenzo.dbo.WaitingListRule
--where
--	WaitingListRule.Created >= @from


--Purchaser
delete
	PAS.PurchaserBase
where
	exists
	(
	select
		1
	from
		Lorenzo.dbo.Purchaser Purchaser
	where
		Purchaser.PURCH_REFNO = PurchaserBase.PURCH_REFNO
	--and	Purchaser.Created >= @from
	)

insert into
	PAS.PurchaserBase
select
	*
from
	Lorenzo.dbo.Purchaser Purchaser
--where
--	Purchaser.Created >= @from


update dbo.Parameter
set
	DateValue = @StartTime
where
	Parameter = 'EXTRACTREFERENCEDATADATE'

if @@rowcount = 0

insert into dbo.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'EXTRACTREFERENCEDATADATE'
	,DateValue = @StartTime


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, ''))

exec WriteAuditLogEvent 'PAS - WH LoadReferenceData', @Stats, @StartTime

/* Output 20/02/2013
select count(1) from Lorenzo.dbo.ReferenceValue
(2820980 row(s) affected)
(2820980 row(s) affected)

select count(1) from Lorenzo.dbo.ReferenceValueIdentifier
(24769 row(s) affected)
(24769 row(s) affected)

select count(1) from Lorenzo.dbo.RTT
(4615692 row(s) affected)
(4619968 row(s) affected)

(0 row(s) affected)

(0 row(s) affected)

(0 row(s) affected)

(0 row(s) affected)

(1 row(s) affected)

(0 row(s) affected)

(3714666 row(s) affected)

select count(1) from Lorenzo.dbo.ProfessionalCarer 
(682505 row(s) affected)
(682505 row(s) affected)

select count(1) from Lorenzo.dbo.ProfessionalCarerSpecialty
(32401 row(s) affected)
(32460 row(s) affected)

select count(1) from Lorenzo.dbo.Specialty
(955 row(s) affected)
(955 row(s) affected)

select count(1) from Lorenzo.dbo.ServicePoint
(4613 row(s) affected)
(4613 row(s) affected)

select count(1) from Lorenzo.dbo.Organisation
(227193 row(s) affected)
(227193 row(s) affected)

select count(1) from Lorenzo.dbo.ClinicalCoding
(4370780 row(s) affected)
(4370780 row(s) affected)

select count(1) from Lorenzo.dbo.Diagnosis
(61166 row(s) affected)
(61166 row(s) affected)

select count(1) from Lorenzo.dbo.[rule]
(52 row(s) affected)
(52 row(s) affected)

select count(1) from Lorenzo.dbo.WaitingListRule
(1898 row(s) affected)
(1898 row(s) affected)

select count(1) from Lorenzo.dbo.Purchaser
(856 row(s) affected)
(856 row(s) affected)

(1 row(s) affected)

(1 row(s) affected)

(0 row(s) affected)

(1 row(s) affected)
*/
