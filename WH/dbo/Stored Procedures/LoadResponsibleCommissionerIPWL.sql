﻿
CREATE procedure [dbo].[LoadResponsibleCommissionerIPWL] as

/*
--Author:	K Oakden
--Date:		06/09/2013

--Run after Episodic GP Practice data updated in WH
--To Assign responsible commissioner

*/
declare @StartTime datetime
declare @Elapsed int
declare @RowsUpdated Int
declare @Stats varchar(255)

select @StartTime = getdate()

update PTL.IPWL set ResponsibleCommissioner = null
update PTL.IPWL set ResponsibleCommissionerCCGOnly = null

--drop table dbo.APCCommissioner
select 
	ipwl.SourceUniqueID
	,EncounterDate = ipwl.DateOnList
	,PCTofPracticeCode = PCT.OrganisationLocalCode
	,CCGofPracticeCode = coalesce(
		ODSPractice.ParentOrganisationCode
		,PCT.OrganisationLocalCode)
	,PCTofResidenceCode = 
		case when OverseasStatus.MappedCode in
			(
			 1	-- Exempt (reciprocal agreement)
			,2	-- Exempt from payment - other
			,3	-- To pay hotel fees only
			,4	-- To pay all fees
			,9	-- Charging rate not known
			)
		then 'X98'

		--For Scotland, NI and CI (NOT WALES) use DistrictOfResidence as lookup instead of PCTCode
		when left(Postcode.DistrictOfResidence,1) in ('S','Z','Y') then Postcode.DistrictOfResidence
		
		else
			--Lookup to ODS postcode_to_PCT table
			Postcode.PCTCode
		end

	,CCGofResidenceCode = 
		case when OverseasStatus.MappedCode in
			(
			 1	-- Exempt (reciprocal agreement)
			,2	-- Exempt from payment - other
			,3	-- To pay hotel fees only
			,4	-- To pay all fees
			,9	-- Charging rate not known
			)
		then 'X98'

		--For Scotland, NI and CI (NOT WALES) use DistrictOfResidence as lookup instead of PCTCode
		when left(PostcodeCCG.DistrictOfResidence,1) in ('S','Z','Y') then PostcodeCCG.DistrictOfResidence
		
		else
			--Lookup to ODS postcode_to_PCT table
			PostcodeCCG.CCGCode
		end
	
	--NOTE THAT NULL GPPRACTICE RETURNS 'SHA'
	,PurchaserCode =
		case 
			when OverseasStatus.MappedCode in (1,2)
				then '5NT'	--OS - Exempt from charges
			when OverseasStatus.MappedCode in (3,4)
				then 'VPP'	--OS - Liable for charges
			when ipwl.PatientPostcode like 'IM%'
				then 'YAC'	--IOM patients don't seem to have overseas status code, so hard-code YAC here to
								--avoid purchaser being pulled from iPM Purchaser (usually TDH00)
		else
			coalesce(
				left(Purchaser.PurchaserMainCode, 3)
				,left(PCT.OrganisationLocalCode, 3)
			)
		end
		
	,PurchaserCodeCCG =
		case 
			when OverseasStatus.MappedCode in (1,2)
				then '01N'	--OS - Exempt from charges
			when OverseasStatus.MappedCode in (3,4)
				then 'VPP'	--OS - Liable for charges
			when ipwl.PatientPostcode like 'IM%'
				then 'YAC'	--IOM patients don't seem to have overseas status code, so hard-code YAC here to
								--avoid purchaser being pulled from iPM Purchaser (usually TDH00)
			--IPM purchaser data is not correct, so use CCG of practice, unless
			--non English, private or overseas patient
			when 
				(left(
					coalesce(
						Purchaser.PurchaserMainCode
						,ODSPractice.ParentOrganisationCode
						,PCT.OrganisationLocalCode)
					, 1) in ('Z','Y','V','S','7') or
				left(Purchaser.PurchaserMainCode, 2) = 'TD')
				then 
					coalesce(
						left(Purchaser.PurchaserMainCode, 3)
						,left(ODSPractice.ParentOrganisationCode, 3)
						,left(PCT.OrganisationLocalCode, 3)
					)
		else
			left(ODSPractice.ParentOrganisationCode, 3)
		end

into dbo.#Commissioner
--into dbo.APCCommissioner
from PTL.IPWL ipwl

left join PAS.ReferenceValue OverseasStatus
	on OverseasStatus.ReferenceValueCode = ipwl.OverseasStatusFlag

left join Organisation.dbo.Postcode Postcode
	on	Postcode.Postcode = ipwl.PatientPostcode
		--case
		--when len(enc.PatientPostcode) = 8 then enc.PatientPostcode
		--else left(enc.PatientPostcode, 3) + ' ' + right(enc.PatientPostcode, 4) 
		--end

left join OrganisationCCG.dbo.Postcode PostcodeCCG
	on	PostcodeCCG.Postcode = ipwl.PatientPostcode
		--case
		--when len(enc.Postcode) = 8 then enc.Postcode
		--else left(enc.Postcode, 3) + ' ' + right(enc.Postcode, 4) 
		--end
		
left join PAS.Purchaser
	on    Purchaser.PurchaserCode = ipwl.PurchaserCode

left join PAS.Organisation IPMPractice
	on	IPMPractice.OrganisationCode = ipwl.EpisodicGpPracticeRefno
	
left join PAS.Organisation PCT
	on	PCT.OrganisationCode = IPMPractice.ParentOrganisationCode
	
left join OrganisationCCG.dbo.Practice ODSPractice
	on ODSPractice.OrganisationCode = IPMPractice.OrganisationLocalCode
	and isnull(ODSPractice.CloseDate, '20130401') > '20130331' 
	


update PTL.IPWL set
	ResponsibleCommissioner = 
		case when comm.EncounterDate > '2013-03-31 00:00:00'
			then
				case when comm.PurchaserCodeCCG = 'SHA'
				then Coalesce(
					case when comm.CCGofResidenceCode = 'X98' then '01N' end
					,left(comm.CCGofResidenceCode, 3)
					,'01N')
				else Coalesce(
					comm.PurchaserCodeCCG
					,case when comm.CCGofResidenceCode = 'X98' then '01N' end
					,left(comm.CCGofResidenceCode, 3)
					,'01N')
				end
			else
				case when comm.PurchaserCode = 'SHA'
					then Coalesce(
					case when comm.PCTofResidenceCode = 'X98' then '5NT' end
					,left(comm.PCTofResidenceCode, 3)
					,'5NT')
				else Coalesce(
					comm.PurchaserCode
					,case when comm.PCTofResidenceCode = 'X98' then '5NT' end
					,left(comm.PCTofResidenceCode, 3)
					,'5NT')
				end
			end
	,ResponsibleCommissionerCCGOnly = 
		case when comm.PurchaserCodeCCG = 'SHA'
		then Coalesce(
			case when comm.CCGofResidenceCode = 'X98' then '01N' end
			,left(comm.CCGofResidenceCode, 3)
			,'01N')
		else Coalesce(
			comm.PurchaserCodeCCG
			,case when comm.CCGofResidenceCode = 'X98' then '01N' end
			,left(comm.CCGofResidenceCode, 3)
			,'01N')
		end
	,CCGOfResidenceCode = comm.CCGofResidenceCode
	
from PTL.IPWL ipwl
inner join dbo.#Commissioner comm
on comm.SourceUniqueID = ipwl.SourceUniqueID

select @RowsUpdated = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'PTL.IPWL updated ' + CONVERT(varchar(10), @RowsUpdated) 
	 + '. Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WH LoadResponsibleCommissionerIPWL', @Stats, @StartTime
--print @Stats


