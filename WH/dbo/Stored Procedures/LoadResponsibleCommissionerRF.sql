﻿CREATE procedure [dbo].[LoadResponsibleCommissionerRF] as

/*
--Author:	K Oakden
--Date:		18/06/2013

--Run after Episodic GP Practice data updated in WH
--To Assign responsible commissioner
--Although referrals don't have a cost attached this value is
--needed for the CSC CCG workaround

*/
declare @StartTime datetime
declare @Elapsed int
declare @RowsUpdated Int
declare @Stats varchar(255)

select @StartTime = getdate()

update RF.Encounter set ResponsibleCommissionerCCGOnly = null

select 
	enc.SourceUniqueID
	,CCGofResidenceCode = 
		case 
			--For Scotland, NI and CI (NOT WALES) use DistrictOfResidence as lookup instead of PCTCode
			when left(PostcodeCCG.DistrictOfResidence,1) in ('S','Z','Y') then PostcodeCCG.DistrictOfResidence
			
			else
				--Lookup to ODS postcode_to_PCT table
				PostcodeCCG.CCGCode
		end
	
	--NOTE THAT NULL GPPRACTICE RETURNS 'SHA'
	,PurchaserCodeCCG =
		case 
			when enc.Postcode like 'IM%'
				then 'YAC'	--IOM patients don't seem to have overseas status code, so hard-code YAC here to
								--avoid purchaser being pulled from iPM Purchaser (usually TDH00)
			--IPM purchaser data is not correct, so use CCG of practice, unless
			--non English, private or overseas patient
			when 
				--(
				left(
					coalesce(
						--Purchaser.PurchaserMainCode
						ODSPractice.ParentOrganisationCode
						,PCT.OrganisationLocalCode)
					, 1) in ('Z','Y','V','S','7') 
					
				--or left(Purchaser.PurchaserMainCode, 2) = 'TD')
				then 
					coalesce(
						--left(Purchaser.PurchaserMainCode, 3)
						left(ODSPractice.ParentOrganisationCode, 3)
						,left(PCT.OrganisationLocalCode, 3)
					)
		else
			left(ODSPractice.ParentOrganisationCode, 3)
		end

into dbo.#Commissioner

from RF.Encounter enc

left join Organisation.dbo.Postcode Postcode
	on	Postcode.Postcode =
		case
		when len(enc.Postcode) = 8 then enc.Postcode
		else left(enc.Postcode, 3) + ' ' + right(enc.Postcode, 4) 
		end

left join OrganisationCCG.dbo.Postcode PostcodeCCG
	on	PostcodeCCG.Postcode =
		case
		when len(enc.Postcode) = 8 then enc.Postcode
		else left(enc.Postcode, 3) + ' ' + right(enc.Postcode, 4) 
		end
		
left join PAS.Organisation IPMPractice
	on	IPMPractice.OrganisationCode = enc.EpisodicGpPracticeCode
	
left join PAS.Organisation PCT
	on	PCT.OrganisationCode = IPMPractice.ParentOrganisationCode
	
left join OrganisationCCG.dbo.Practice ODSPractice
	on ODSPractice.OrganisationCode = IPMPractice.OrganisationLocalCode
	and isnull(ODSPractice.CloseDate, '20130401') > '20130331' 

update RF.Encounter set
	ResponsibleCommissionerCCGOnly = 
		case when comm.PurchaserCodeCCG = 'SHA'
		then Coalesce(
			case when comm.CCGofResidenceCode = 'X98' then '01N' end
			,left(comm.CCGofResidenceCode, 3)
			,'01N')
		else Coalesce(
			comm.PurchaserCodeCCG
			,case when comm.CCGofResidenceCode = 'X98' then '01N' end
			,left(comm.CCGofResidenceCode, 3)
			,'01N')
		end		
from RF.Encounter enc
inner join dbo.#Commissioner comm
on comm.SourceUniqueID = enc.SourceUniqueID

select @RowsUpdated = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'RF.Encounter updated ' + CONVERT(varchar(10), @RowsUpdated) 
	 + '. Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WH LoadResponsibleCommissionerRF', @Stats, @StartTime
--print @Stats

