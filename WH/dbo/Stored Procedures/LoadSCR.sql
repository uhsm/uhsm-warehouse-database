﻿CREATE   procedure [dbo].[LoadSCR] 
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

exec dbo.LoadSCRReferral
exec dbo.LoadSCRDefinitiveTreatment
exec dbo.LoadSCRDemographic
exec dbo.LoadSCRCarePlan
exec dbo.LoadSCRInitialAssessment
exec dbo.LoadSCRChemotherapy
exec dbo.LoadSCRSurgery
exec dbo.LoadSCRReferenceData
exec dbo.LoadSCRTrackingComment

--KO added 03/03/2015
exec dbo.LoadSCRMDTBreast
exec dbo.LoadSCRMDTColorectal
exec dbo.LoadSCRMDTGynaecology
exec dbo.LoadSCRMDTLung
exec dbo.LoadSCRMDTPalliative
exec dbo.LoadSCRMDTUpperGI
exec dbo.LoadSCRMDTUrology

exec dbo.LoadSCRBreastPathology

--KO added 23/03/2015
exec dbo.LoadSCRMDTSkin
exec dbo.LoadSCRSpecialistNurse
exec dbo.LoadSCRCNSActivities
exec dbo.LoadSCRCNSName

exec WHOLAP.dbo.BuildBaseSCRReferralTreatment 


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins' 


exec WriteAuditLogEvent 'LoadSCR', @Stats, @StartTime
