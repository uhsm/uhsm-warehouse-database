﻿


CREATE procedure [dbo].[LoadSCRAdditionalReferenceData] as

--CREATE TABLE SCR.AdditionalLookupsText(
--	[LUTableName] [varchar](30) NOT NULL,
--	[LUCodeText] [varchar](2) NULL,
--	[LUDescription] [varchar](150) NULL
--) ON [PRIMARY]

--CREATE TABLE SCR.AdditionalLookupsNumeric(
--	[LUTableName] [varchar](30) NOT NULL,
--	[LUCodeNumeric] [int] NULL,
--	[LUDescription] [varchar](150) NULL
--) ON [PRIMARY]

truncate table SCR.AdditionalLookupsText
truncate table SCR.AdditionalLookupsNumeric

insert into SCR.AdditionalLookupsText select 'ltblDIFFERENTIATION', GRADE_CODE, GRADE_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblDIFFERENTIATION
insert into SCR.AdditionalLookupsText select 'ltblINVESTIGATION_TYPE', TYPE_CODE, TYPE_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblINVESTIGATION_TYPE
insert into SCR.AdditionalLookupsText select 'ltblLATERALITY', LAT_CODE, LAT_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblLATERALITY
insert into SCR.AdditionalLookupsText select 'ltblMARGINS', MARGIN_CODE, MARGIN_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblMARGINS
insert into SCR.AdditionalLookupsText select 'ltblPC_COMMUNITY', TYPE_CODE, TYPE_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblPC_COMMUNITY
insert into SCR.AdditionalLookupsText select 'ltblPC_PROVIDER', TYPE_CODE, TYPE_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblPC_PROVIDER
insert into SCR.AdditionalLookupsText select 'ltblPLAN_FAILURE', FAIL_CODE, FAIL_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblPLAN_FAILURE
insert into SCR.AdditionalLookupsText select 'ltblRELATIONSHIP', RELATION_ID, RELATION_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblRELATIONSHIP
insert into SCR.AdditionalLookupsText select 'ltblTREATMENT_SETTING', SET_CODE, SET_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblTREATMENT_SETTING
insert into SCR.AdditionalLookupsText select 'ltblTRIAL_STATUS', STATUS_CODE, STATUS_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblTRIAL_STATUS
insert into SCR.AdditionalLookupsText select 'ltblCNS_INDICATION_CODES', INDICATION_CODE, INDICATION_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblCNS_INDICATION_CODES


insert into SCR.AdditionalLookupsNumeric select 'ltblBREAST_HER2', SCORE_CODE, SCORE_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblBREAST_HER2
insert into SCR.AdditionalLookupsNumeric select 'ltblCOLO_CT_OUTCOME', OUTCOME_CODE, OUTCOME_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblCOLO_CT_OUTCOME
insert into SCR.AdditionalLookupsNumeric select 'ltblCOLO_FOLLOWUP', MODE_CODE, MODE_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblCOLO_FOLLOWUP
insert into SCR.AdditionalLookupsNumeric select 'ltblCOLO_MRI_RESULT', CHANGE_CODE, CHANGE_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblCOLO_MRI_RESULT
insert into SCR.AdditionalLookupsNumeric select 'ltblCOLO_REC_BY', REC_CODE, REC_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblCOLO_REC_BY
insert into SCR.AdditionalLookupsNumeric select 'ltblCOLORECTAL_REC', REC_CODE, REC_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblCOLORECTAL_REC
insert into SCR.AdditionalLookupsNumeric select 'ltblCONTINENCE', CONT_CODE, CONT_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblCONTINENCE
insert into SCR.AdditionalLookupsNumeric select 'ltblCYTOLOGY_TYPE', TYPE_CODE, TYPE_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblCYTOLOGY_TYPE
insert into SCR.AdditionalLookupsNumeric select 'ltblDERM_STATUS', STATUS_CODE, STATUS_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblDERM_STATUS
insert into SCR.AdditionalLookupsNumeric select 'ltblFOLLOW_UP', FOLLOWUP_CODE, FOLLOWUP_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblFOLLOW_UP
insert into SCR.AdditionalLookupsNumeric select 'ltblFU_LOCATION', LOC_CODE, LOC_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblFU_LOCATION
insert into SCR.AdditionalLookupsNumeric select 'ltblGLEASON', GLEASON_ID, GLEASON_CODE from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblGLEASON
insert into SCR.AdditionalLookupsNumeric select 'ltblHAEM_BULK', BULK_CODE, BULK_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblHAEM_BULK
insert into SCR.AdditionalLookupsNumeric select 'ltblHAEM_EVIDENCE', EVIDENCE_CODE, EVIDENCE_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblHAEM_EVIDENCE
insert into SCR.AdditionalLookupsNumeric select 'ltblHAEM_MASS', MASS_CODE, MASS_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblHAEM_MASS
insert into SCR.AdditionalLookupsNumeric select 'ltblHAEM_SURVEY', SURVEY_CODE, SURVEY_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblHAEM_SURVEY
insert into SCR.AdditionalLookupsNumeric select 'ltblIMPOTENCE', IMP_CODE, IMP_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblIMPOTENCE
insert into SCR.AdditionalLookupsNumeric select 'ltblINCOMPLETE_REASON', REASON_CODE, REASON_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblINCOMPLETE_REASON
insert into SCR.AdditionalLookupsNumeric select 'ltblMARGIN_ADEQUACY', MARGIN_CODE, MARGIN_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblMARGIN_ADEQUACY
insert into SCR.AdditionalLookupsNumeric select 'ltblMARKER_RESPONSE', MARKER_CODE, MARKER_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblMARKER_RESPONSE
insert into SCR.AdditionalLookupsNumeric select 'ltblMETS_STATUS', METS_CODE, METS_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblMETS_STATUS
insert into SCR.AdditionalLookupsNumeric select 'ltblMOD_TYPE', MOD_CODE, MOD_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblMOD_TYPE
insert into SCR.AdditionalLookupsNumeric select 'ltblMORBIDITY_CODE', MORBIDITY_CODE, MORBIDITY_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblMORBIDITY_CODE
insert into SCR.AdditionalLookupsNumeric select 'ltblNODAL_STATUS', NODAL_CODE, NODAL_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblNODAL_STATUS
insert into SCR.AdditionalLookupsNumeric select 'ltblREPORT_STATUS', STATUS_CODE, STATUS_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblREPORT_STATUS
insert into SCR.AdditionalLookupsNumeric select 'ltblSKIN_TYPE', TYPE_CODE, TYPE_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblSKIN_TYPE
insert into SCR.AdditionalLookupsNumeric select 'ltblSNOMED_HAEMATOLOGY', M_CODE, M_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblSNOMED_HAEMATOLOGY
insert into SCR.AdditionalLookupsNumeric select 'ltblSPECIMEN', SPECIMEN_CODE, SPECIMEN_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblSPECIMEN
insert into SCR.AdditionalLookupsNumeric select 'ltblTREATMENT_MORBID', TYPE_CODE, TYPE_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblTREATMENT_MORBID
insert into SCR.AdditionalLookupsNumeric select 'ltblTRIAL_TYPE', TRIAL_TYPE_CODE, TRIAL_TYPE_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblTRIAL_TYPE
insert into SCR.AdditionalLookupsNumeric select 'ltblTUMOUR_STATUS', TUMOUR_CODE, TUMOUR_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblTUMOUR_STATUS
insert into SCR.AdditionalLookupsNumeric select 'ltblUGI_FOLLOWUP', FUP_CODE, FUP_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblUGI_FOLLOWUP
insert into SCR.AdditionalLookupsNumeric select 'ltblUROLOGY_COMPLICATIONS', COMP_CODE, COMP_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblUROLOGY_COMPLICATIONS
insert into SCR.AdditionalLookupsNumeric select 'ltblUROLOGY_DISCHARGE', DISCHARGE_CODE, DISCHARGE_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblUROLOGY_DISCHARGE
insert into SCR.AdditionalLookupsNumeric select 'ltblUROLOGY_INTERVENTION', INT_CODE, INT_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblUROLOGY_INTERVENTION
insert into SCR.AdditionalLookupsNumeric select 'ltblUROLOGY_OUTCOME', OUT_CODE, OUT_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblUROLOGY_OUTCOME
insert into SCR.AdditionalLookupsNumeric select 'ltblWBC', WBC_CODE, WBC_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblWBC
insert into SCR.AdditionalLookupsNumeric select 'ltblWHO', WHO_CODE, WHO_DESC from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblWHO


