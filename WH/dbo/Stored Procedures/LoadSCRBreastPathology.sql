﻿
create procedure [dbo].[LoadSCRBreastPathology] as

/**
K Oakden 03/03/2015 Additional SCR data
**/
declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0


truncate table SCR.BreastPathology

insert into SCR.BreastPathology
	(
	[BREAST_PATH_ID]
	,[PATHOLOGY_ID]
	,[L_SPECIMEN]
	,[L_WEIGHT]
	,[L_NON_INV]
	,[L_DCIS_SIZE]
	,[L_MICROINV]
	,[L_INVASIVE]
	,[L_OTHER_PRIMARY]
	,[L_OTHER_MALIGNANT]
	,[L_MIXED]
	,[L_EXTENT]
	,[L_PROLIFERATION]
	,[L_AX_NODES]
	,[L_AX_POS]
	,[L_DISTANCE]
	,[L_ER]
	,[L_ER_SCORE]
	,[L_ER_DETAILS]
	,[L_PR]
	,[L_PR_SCORE]
	,[L_PR_DETAILS]
	,[L_CERBB2]
	,[L_CERBB2_SCORE]
	,[L_CERBB2_DETAILS]
	,[L_BCL2]
	,[L_BCL2_SCORE]
	,[L_BCL2_DETAILS]
	,[L_pS2]
	,[L_pS2_SCORE]
	,[L_pS2_DETAILS]
	,[L_HER2]
	,[L_HER2_DETAILS]
	,[L_FISH]
	,[L_FISH_DETAILS]
	,[R_RADIOGRAPH]
	,[R_MAMMOGRAM]
	,[R_SITES]
	,[L_TM_DATE]
	,[L_WHOLE_SIZE]
	,[L_SOLID]
	,[L_CRIBRIFORM]
	,[L_MICROPAPILLARY]
	,[L_PAPILLARY]
	,[L_APOCRINE]
	,[L_FLAT]
	,[L_COMEDO]
	,[L_OTHER]
	,[R_GROWTH]
	,[L_POSITIVITY]
	)
select
	[BREAST_PATH_ID]
	,[PATHOLOGY_ID]
	,[L_SPECIMEN]
	,[L_WEIGHT]
	,[L_NON_INV]
	,[L_DCIS_SIZE]
	,[L_MICROINV]
	,[L_INVASIVE]
	,[L_OTHER_PRIMARY]
	,[L_OTHER_MALIGNANT]
	,[L_MIXED]
	,[L_EXTENT]
	,[L_PROLIFERATION]
	,[L_AX_NODES]
	,[L_AX_POS]
	,[L_DISTANCE]
	,[L_ER]
	,[L_ER_SCORE]
	,[L_ER_DETAILS]
	,[L_PR]
	,[L_PR_SCORE]
	,[L_PR_DETAILS]
	,[L_CERBB2]
	,[L_CERBB2_SCORE]
	,[L_CERBB2_DETAILS]
	,[L_BCL2]
	,[L_BCL2_SCORE]
	,[L_BCL2_DETAILS]
	,[L_pS2]
	,[L_pS2_SCORE]
	,[L_pS2_DETAILS]
	,[L_HER2]
	,[L_HER2_DETAILS]
	,[L_FISH]
	,[L_FISH_DETAILS]
	,[R_RADIOGRAPH]
	,[R_MAMMOGRAM]
	,[R_SITES]
	,[L_TM_DATE]
	,[L_WHOLE_SIZE]
	,[L_SOLID]
	,[L_CRIBRIFORM]
	,[L_MICROPAPILLARY]
	,[L_PAPILLARY]
	,[L_APOCRINE]
	,[L_FLAT]
	,[L_COMEDO]
	,[L_OTHER]
	,[R_GROWTH]
	,[L_POSITIVITY]
from
	[UHSM-CSERVICES].CancerRegister.dbo.tblPATHOLOGY_BREAST

select @RowsInserted = @@rowcount


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadSCRBreastPathology', @Stats, @StartTime

--print @Stats


