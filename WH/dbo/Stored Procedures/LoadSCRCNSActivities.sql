﻿

CREATE procedure [dbo].[LoadSCRCNSActivities]
as

/**
K Oakden 23/03/2015 Additional SCR data
**/

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0



truncate table SCR.CNSActivities

insert into SCR.CNSActivities
	(
	[ACT_CODE]
	,[ACT_DESC]
	,[ACT]
	,[CA_SITE]
	,[INFO_ADVICE]
	,[CLINICAL_ACTIVITY]
	,[IS_DELETED]
	,[ADVICE_BREAST]
	,[ACTIVITY_BREAST]
)
select
	[ACT_CODE]
	,[ACT_DESC]
	,[ACT]
	,[CA_SITE]
	,[INFO_ADVICE]
	,[CLINICAL_ACTIVITY]
	,[IS_DELETED]
	,[ADVICE_BREAST]
	,[ACTIVITY_BREAST]
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.ltblCNS_ACTIVITIES


select @RowsInserted = @@rowcount


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadSCRCNSActivities', @Stats, @StartTime

print @Stats



