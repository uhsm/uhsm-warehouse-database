﻿

CREATE procedure [dbo].[LoadSCRCNSName]
as

/**
K Oakden 23/03/2015 Additional SCR data
**/

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0



truncate table SCR.CNSName

insert into SCR.CNSName
	(
	[ID]
	,[CNS_NAME]
	,[IS_DELETED]
	,[USER_ID]
	)
select
	[ID]
	,[CNS_NAME]
	,[IS_DELETED]
	,[USER_ID]
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.ltblCNS_NAME


select @RowsInserted = @@rowcount


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadSCRCNSName', @Stats, @StartTime

print @Stats



