﻿CREATE procedure [dbo].[LoadSCRCarePlanTest]
as


--declare @StartTime datetime
--declare @Elapsed int
--declare @RowsInserted Int
--declare @Stats varchar(255)
--declare @from varchar(12)

--select @StartTime = getdate()

--select @RowsInserted = 0
--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SCR].[CarePlanTest]') AND type in (N'U'))
--DROP TABLE [SCR].[CarePlanTest]
--GO


truncate table SCR.CarePlan

insert into SCR.CarePlan
	(
	 UniqueRecordId
	,ReferralUniqueRecordId
	,SystemId
	
	,WasPatientDiscussedAtMDT
	,MDTDate
	,CarePlanAgreedDate
	,DecisionOrganisationCode
	--,Recurrence
	
	,CancerCarePlanIntentCode
	,FirstPlannedTreatmentTypeCode
	,SecondPlannedTreatmentTypeCode
	,ThirdPlannedTreatmentTypeCode
	,FourthPlannedTreatmentTypeCode
	,NoTreatmentReasonCode
	,CoMorbidityIndexCode
	,PerformanceStatusCode
	,PrimaryCareCommunicationDate
	
	,CarePlanAgreed
	,PlanTypeCode
	,NotFirstChoiceOfTreatmentReasonCode
	,LocationCode
	,PrePostTreatment
	,OncologistPresent
	,MDTActionedBy
	,ReviewedByPathologistPriorToMDT
	,ReviewedBy
	,ReferredTo
	,WhoReferredToCode
	,CopyLetterTo
	,TrustReferredToCode
	,ReasonForReferalCode
	,WasPatientDiscussedAtNetworkMDT
	,NetworkMeetingDate
	,NetworkActionedBy
	,PlannedTransplantTypeCode
	,RecurrenceTypeCode
	,RecurrenceDiagnosedBy
	,ReasonNotCurative
	,Seizures
	,Surgery
	,CarePlanAgreedAtMDT
	,MDTComments
	,NetworkDecisionCode
	,NetworkComments
	,ReasonNotDiscussedCode
)
select
	 UniqueRecordId = PLAN_ID
	,ReferralUniqueRecordId = CARE_ID
	,SystemId = TEMP_ID
	
	,WasPatientDiscussedAtMDT = N5_1_MDT_MEETING_YN
	,MDTDate = N5_2_MDT_DATE
	,CarePlanAgreedDate = N5_3_PLAN_AGREE_DATE
	,DecisionOrganisationCode = N1_3_ORG_CODE_DECISION
	--,Recurrence = N5_4_RECURRENCE
	
	,CancerCarePlanIntentCode = N5_5_CARE_INTENT
	,FirstPlannedTreatmentTypeCode = N5_6_TREATMENT_TYPE_1
	,SecondPlannedTreatmentTypeCode = N5_6_TREATMENT_TYPE_2
	,ThirdPlannedTreatmentTypeCode = N5_6_TREATMENT_TYPE_3
	,FourthPlannedTreatmentTypeCode = N5_6_TREATMENT_TYPE_4
	,NoTreatmentReasonCode = N5_8_NO_TREATMENT
	,CoMorbidityIndexCode = N5_9_CO_MORBIDITY
	,PerformanceStatusCode = N5_10_WHO_STATUS
	,PrimaryCareCommunicationDate = N_HN20_PC_DATE
	
	,CarePlanAgreed = N_L19_PCI
	,PlanTypeCode = N_L28_PLAN_TYPE
	,NotFirstChoiceOfTreatmentReasonCode = N_L32_CHOICE
	,LocationCode = L_LOCATION
	,PrePostTreatment = L_PRE_POST_OP
	,OncologistPresent = L_ONCOLOGIST
	,MDTActionedBy = L_MDT_ACTION_BY
	,ReviewedByPathologistPriorToMDT = L_PATH_REVIEW
	,ReviewedBy = L_REVIEW_WHO
	,ReferredTo = L_REFERRED_TO
	,WhoReferredToCode = L_WHO_REFERRED_TO
	,CopyLetterTo = L_LETTER_CC
	,TrustReferredToCode = L_OTHER_TRUST
	,ReasonForReferalCode = L_TERTIARY_REASON
	,WasPatientDiscussedAtNetworkMDT = L_NETWORK
	,NetworkMeetingDate = L_DATE_NETWORK_MEETING
	,NetworkActionedBy = L_NETWORK_ACTIONED
	,PlannedTransplantTypeCode = L_PLANNED_TRANSPLANT
	,RecurrenceTypeCode = R_TYPE_REC
	,RecurrenceDiagnosedBy = R_DIAG_BY
	,ReasonNotCurative = R_NOT_CUR
	,Seizures = L_SEIZURES
	,Surgery = L_SURGERY
	,CarePlanAgreedAtMDT = L_CARE_PLAN_AGREED
	,MDTComments = L_MDT_COMMENTS
	,NetworkDecisionCode = L_NETWORK_FEEDBACK
	,NetworkComments = L_NETWORK_COMMENTS
	,ReasonNotDiscussedCode = R_NOT_DISCUSSED_COMMENTS --AT 29/02/2012 fieldname was changed
	
--	PLAN_ID
--	,CARE_ID
--	,TEMP_ID
--	,N5_1_MDT_MEETING_YN
--	,N5_2_MDT_DATE
--	,N5_3_PLAN_AGREE_DATE
--	,N1_3_ORG_CODE_DECISION
--	,N5_4_RECURRENCE
--	,N5_5_CARE_INTENT
--	,N5_6_TREATMENT_TYPE_1
--	,N5_6_TREATMENT_TYPE_2
--	,N5_6_TREATMENT_TYPE_3
--	,N5_6_TREATMENT_TYPE_4
--	,N5_8_NO_TREATMENT
--	,N5_9_CO_MORBIDITY
--	,N5_10_WHO_STATUS
--	,N_HN20_PC_DATE
--	,N_L19_PCI
--	,N_L28_PLAN_TYPE
--	,N_L32_CHOICE
--	,L_LOCATION
--	,L_PRE_POST_OP
--	,L_ONCOLOGIST
--	,L_MDT_ACTION_BY
--	,L_PATH_REVIEW
--	,L_REVIEW_WHO
--	,L_REFERRED_TO
--	,L_WHO_REFERRED_TO
--	,L_LETTER_CC
--	,L_OTHER_TRUST
--	,L_TERTIARY_REASON
--	,L_NETWORK
--	,L_DATE_NETWORK_MEETING
--	,L_NETWORK_ACTIONED
--	,L_PLANNED_TRANSPLANT
--	,R_TYPE_REC
--	,R_DIAG_BY
--	,R_NOT_CUR
--	,L_SEIZURES
--	,L_SURGERY
--	,L_CARE_PLAN_AGREED
--	,L_MDT_COMMENTS
--	,L_NETWORK_FEEDBACK
--	,L_NETWORK_COMMENTS
--	,R_NOT_DISCUSSED_COMMENTS --AT 29/02/2012 fieldname was changed
--into SCR.CarePlanTest
from
	SCR.CarePlanTest
	--[UHSM-CSERVICES].CancerRegister.dbo.tblMAIN_CARE_PLAN


--select @RowsInserted = @@rowcount


--SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

--SELECT @Stats = 
--	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
--	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

--EXEC WriteAuditLogEvent 'LoadSCRCarePlan', @Stats, @StartTime

--print @Stats

