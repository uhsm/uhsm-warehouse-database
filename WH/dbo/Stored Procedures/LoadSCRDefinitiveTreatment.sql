﻿

CREATE procedure [dbo].[LoadSCRDefinitiveTreatment]
as

/**
KO updated 05/12 to delete 'empty' definitive treatment records
as a result of error with PTL after SWCR upgraded to version 15.0
**/
declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0



truncate table SCR.DefinitiveTreatment


insert into SCR.DefinitiveTreatment
	(
	 UniqueRecordId
	,ReferralUniqueRecordId
	,PatientPathwayId
	,DecisionToTreatDate
	,DTTOrganisationCode
	,FirstDefinitiveTreatmentDate
	,InitialTreatmentCode
	,TreatmentOrganisationCode
	,TreatmentEventTypeCode
	,TreatmentSettingCode
	,PriorityCode
	,TreatmentIntentCode
	,SupportTypeCode
	,ClinicalTrial
	,WaitingTimeAdjustment
	,AdjustmentReasonCode
	,DelayReasonCode
	,TreatmentNo
	,TreatmentNoId
	,ValidatedForUpload
	,DelayReasonComments
	,TrackingNotes
	,AllTrackingNotes
	)
select
	 UniqueRecordId = TREATMENT_ID
	,ReferralUniqueRecordId = CARE_ID
	,PatientPathwayId = PATHWAY_ID
	,DecisionToTreatDate = DECISION_DATE
	,DTTOrganisationCode = case when ORG_CODE_DTT = '' then null else ORG_CODE_DTT end
	,FirstDefinitiveTreatmentDate = START_DATE
	,InitialTreatmentCode = TREATMENT
	,TreatmentOrganisationCode = ORG_CODE
	,TreatmentEventTypeCode = TREATMENT_EVENT
	,TreatmentSettingCode = TREATMENT_SETTING
	,PriorityCode = RT_PRIORITY
	,TreatmentIntentCode= RT_INTENT
	,SupportTypeCode = SPECIALIST
	,ClinicalTrial = TRIAL
	,WaitingTimeAdjustment = ADJ_DAYS
	,AdjustmentReasonCode = ADJ_CODE
	,DelayReasonCode = DELAY_CODE
	,TreatmentNo = TREAT_NO
	,TreatmentNoId = TREAT_ID
	,ValidatedForUpload = VALIDATED
	,DelayReasonComments = DELAY_COMMENTS
	,TrackingNotes = COMMENTS
	,AllTrackingNotes = ALL_COMMENTS
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.tblDEFINITIVE_TREATMENT


select @RowsInserted = @@rowcount

delete from SCR.DefinitiveTreatment
where [PatientPathwayId] is null
and [DecisionToTreatDate] is null
and [DTTOrganisationCode] is null
and [FirstDefinitiveTreatmentDate] is null
and [InitialTreatmentCode] is null
and [TreatmentOrganisationCode] is null
and [TreatmentEventTypeCode] is null
and [TreatmentSettingCode] is null
and [PriorityCode] is null
and [TreatmentIntentCode] is null
and [SupportTypeCode] is null
and [ClinicalTrial] is null
and [WaitingTimeAdjustment] is null
and [AdjustmentReasonCode] is null
and [DelayReasonCode] is null
--and [TreatmentNo] is null
and [TreatmentNoId] is null
and [ValidatedForUpload] is null
and [DelayReasonComments] is null
and [TrackingNotes] is null
and [AllTrackingNotes] is null

select @RowsInserted = @RowsInserted - @@rowcount


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadSCRDefinitiveTreatment', @Stats, @StartTime

print @Stats



