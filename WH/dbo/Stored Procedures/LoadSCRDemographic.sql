﻿

CREATE procedure [dbo].[LoadSCRDemographic]
as


declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0



truncate table SCR.Demographic


insert into SCR.Demographic
	(
	 UniqueRecordId
	,NHSNumber
	,RidRA3
	,RidRA7
	,RidRVJ01
	,SystemId
	,NHSNumberStatus
	,HospitalNumber
	,TitleCode
	,Surname
	,Forename
	,AddressLine1
	,AddressLine2
	,AddressLine3
	,AddressLine4
	,AddressLine5
	,Postcode
	,GenderCode
	,DateOfBirth
	,GpCode
	,PracticeCode
	,PCTCode
	,SurnameAtBirth
	,EthnicityCode
	,PreferredName
	,Occupation
	,SocialClassCode
	,LivesAloneCode
	,MaritalStatusCode
	,PreferredLanguage
	,PreferredContactMethodCode
	,PatientDeathStatusCode
	,DateOfDeath
	,PlaceOfDeathCode
	,CauseOfDeathIdentificationMethodCode
	,CancerRelatedDeathCode
	,CauseOfDeathIaCode
	,CauseOfDeathIbCode
	,CauseOfDeathIcCode
	,CauseOfDeathIiCode
	,DeathIdentificationDiscrepancy
	,TownBirth
	,CountryBirth
	,SurnameMother
	,ClassificationMotherCode
	,ForenameMother
	,DateOfBirthMother
	,BirthTownMother
	,BirthCountryMother
	,OccupationBirthMother
	,OccupationDiagnosisMother
	,SurnameFather
	,ClassificationFather
	,ForenameFather
	,DateOfBirthFather
	,BirthTownFather
	,BirthCountryFather
	,OccupationBirthFather
	,OccupationDiagnosisFather
	,PartOfMultipleBirth
	,PostMortemUndertakenCode
	,DayTimePhoneNumber
	,EveningPhoneNumber
	,DeathRelatedToTreatmentCode
	,PostMortemDetails
	,IatrogenicDeath
	,DeathDueToInfection
	,DeathComments
	,ReligionCode
	,PreferredContactMethod
	,NokName
	,NokAddressLine1
	,NokAddressLine2
	,NokAddressLine3
	,NokAddressLine4
	,NokAddressLine5
	,NokPostcode
	,NokContactDetails
	,NokRelationshipCode
	,Dependants
	,Carer1Name
	,Carer1AddressLine1
	,Carer1AddressLine2
	,Carer1AddressLine3
	,Carer1AddressLine4
	,Carer1AddressLine5
	,Carer1Postcode
	,Carer1ContactDetails
	,Carer1RelationshipCode
	,Carer1TypeCode
	,Carer2Name
	,Carer2AddressLine1
	,Carer2AddressLine2
	,Carer2AddressLine3
	,Carer2AddressLine4
	,Carer2AddressLine5
	,Carer2Postcode
	,Carer2ContactDetails
	,Carer2RelationshipCode
	,Carer2TypeCode
	,PatientAtRiskCode
	,ReasonAtRisk
	,GestationBirthWeight
)
select
	 UniqueRecordId = PATIENT_ID
	,NHSNumber = N1_1_NHS_NUMBER
	,RidRA3 = L_RA3_RID
	,RidRA7 = L_RA7_RID
	,RidRVJ01 = L_RVJ01_RID
	,SystemId = TEMP_ID
	,NHSNumberStatus = L_NSTS_STATUS
	,HospitalNumber = N1_2_HOSPITAL_NUMBER
	,TitleCode = L_TITLE
	,Surname = N1_5_SURNAME
	,Forename = N1_6_FORENAME
	,AddressLine1 = N1_7_ADDRESS_1
	,AddressLine2 = N1_7_ADDRESS_2
	,AddressLine3 = N1_7_ADDRESS_3
	,AddressLine4 = N1_7_ADDRESS_4
	,AddressLine5 = N1_7_ADDRESS_5
	,Postcode = N1_8_POSTCODE
	,GenderCode = N1_9_SEX
	,DateOfBirth = N1_10_DATE_BIRTH
	,GpCode = N1_11_GP_CODE
	,PracticeCode = case when N1_12_GP_PRACTICE_CODE = '' then null else N1_12_GP_PRACTICE_CODE end
	,PCTCode = case when N1_13_PCT = '' then null else N1_13_PCT end
	,SurnameAtBirth = N1_14_SURNAME_BIRTH
	,EthnicityCode = N1_15_ETHNICITY
	,PreferredName = PAT_PREF_NAME
	,Occupation = PAT_OCCUPATION
	,SocialClassCode = PAT_SOCIAL_CLASS
	,LivesAloneCode = PAT_LIVES_ALONE
	,MaritalStatusCode = MARITAL_STATUS
	,PreferredLanguage = PAT_PREF_LANGUAGE
	,PreferredContactMethodCode = PAT_PREF_CONTACT
	,PatientDeathStatusCode = L_DEATH_STATUS
	,DateOfDeath = N15_1_DATE_DEATH
	,PlaceOfDeathCode = N15_2_DEATH_LOCATION
	,CauseOfDeathIdentificationMethodCode = N15_3_DEATH_CAUSE
	,CancerRelatedDeathCode = N15_4_DEATH_CANCER
	,CauseOfDeathIaCode = N15_5_DEATH_CODE_1
	,CauseOfDeathIbCode = N15_6_DEATH_CODE_2
	,CauseOfDeathIcCode = N15_7_DEATH_CODE_3
	,CauseOfDeathIiCode = N15_8_DEATH_CODE_4
	,DeathIdentificationDiscrepancy = N15_9_DEATH_DISCREPANCY
	,TownBirth = N_CC4_TOWN
	,CountryBirth = N_CC5_COUNTRY
	,SurnameMother = N_CC6_M_SURNAME
	,ClassificationMotherCode = N_CC7_M_CLASS
	,ForenameMother = N_CC8_M_FORENAME
	,DateOfBirthMother = N_CC9_M_DOB
	,BirthTownMother = N_CC10_M_TOWN
	,BirthCountryMother = N_CC11_M_COUNTRY
	,OccupationBirthMother = N_CC12_M_OCC
	,OccupationDiagnosisMother = N_CC13_M_OCC_DIAG
	,SurnameFather = N_CC6_F_SURNAME
	,ClassificationFather = N_CC7_F_CLASS
	,ForenameFather = N_CC8_F_FORENAME
	,DateOfBirthFather = N_CC9_F_DOB
	,BirthTownFather = N_CC10_F_TOWN
	,BirthCountryFather = N_CC11_F_COUNTRY
	,OccupationBirthFather = N_CC12_F_OCC
	,OccupationDiagnosisFather = N_CC13_F_OCC_DIAG
	,PartOfMultipleBirth = N_CC14_MULTI_BIRTH
	,PostMortemUndertakenCode = R_POST_MORTEM
	,DayTimePhoneNumber = R_DAY_PHONE
	,EveningPhoneNumber = R_EVE_PHONE
	,DeathRelatedToTreatmentCode = R_DEATH_TREATMENT
	,PostMortemDetails = R_PM_DETAILS
	,IatrogenicDeath = L_IATROGENIC_DEATH
	,DeathDueToInfection = L_INFECTION_DEATH
	,DeathComments = L_DEATH_COMMENTS
	,ReligionCode = RELIGION
	,PreferredContactMethod = CONTACT_DETAILS
	,NokName = NOK_NAME
	,NokAddressLine1 = NOK_ADDRESS_1
	,NokAddressLine2 = NOK_ADDRESS_2
	,NokAddressLine3 = NOK_ADDRESS_3
	,NokAddressLine4 = NOK_ADDRESS_4
	,NokAddressLine5 = NOK_ADDRESS_5
	,NokPostcode = NOK_POSTCODE
	,NokContactDetails = NOK_CONTACT
	,NokRelationshipCode = NOK_RELATIONSHIP
	,Dependants = PAT_DEPENDANTS
	,Carer1Name = CARER_NAME
	,Carer1AddressLine1 = CARER_ADDRESS_1
	,Carer1AddressLine2 = CARER_ADDRESS_2
	,Carer1AddressLine3 = CARER_ADDRESS_3
	,Carer1AddressLine4 = CARER_ADDRESS_4
	,Carer1AddressLine5 = CARER_ADDRESS_5
	,Carer1Postcode = CARER_POSTCODE
	,Carer1ContactDetails = CARER_CONTACT
	,Carer1RelationshipCode = CARER_RELATIONSHIP
	,Carer1TypeCode = CARER1_TYPE
	,Carer2Name = CARER2_NAME
	,Carer2AddressLine1 = CARER2_ADDRESS_1
	,Carer2AddressLine2 = CARER2_ADDRESS_2
	,Carer2AddressLine3 = CARER2_ADDRESS_3
	,Carer2AddressLine4 = CARER2_ADDRESS_4
	,Carer2AddressLine5 = CARER2_ADDRESS_5
	,Carer2Postcode = CARER2_POSTCODE
	,Carer2ContactDetails = CARER2_CONTACT
	,Carer2RelationshipCode = CARER2_RELATIONSHIP
	,Carer2TypeCode = CARER2_TYPE
	,PatientAtRiskCode = PT_AT_RISK
	,ReasonAtRisk = REASON_RISK
	,GestationBirthWeight = GESTATION
FROM
	[V-UHSM-SCR\SCR].CancerRegister.dbo.tblDEMOGRAPHICS


select @RowsInserted = @@rowcount


exec Pseudonomisation.dbo.PseudonomiseSCR


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadSCRDemographic', @Stats, @StartTime

print @Stats



