﻿


CREATE procedure [dbo].[LoadSCRMDTPalliative] as

/**
K Oakden 03/03/2015 Additional SCR data
**/
declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0


truncate table SCR.MDTPalliative

insert into SCR.MDTPalliative
	(
	[MDT_ID]
	,[PALLIATIVE_ID]
	,[TEMP_ID]
	,[MEETING_ID]
	,[UPDATED]
	,[L_MDT_DATE]
	,[L_LOCATION]
	,[L_ACTIONS]
	,[L_COMMENTS]
	,[L_PATHOLOGY_TEXT]
	)
select
	[MDT_ID]
	,[PALLIATIVE_ID]
	,[TEMP_ID]
	,[MEETING_ID]
	,[UPDATED]
	,[L_MDT_DATE]
	,[L_LOCATION]
	,[L_ACTIONS]
	,[L_COMMENTS]
	,[L_PATHOLOGY_TEXT]
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.tblPALLIATIVE_MDT

select @RowsInserted = @@rowcount


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadSCRMDTPalliative', @Stats, @StartTime

--print @Stats




