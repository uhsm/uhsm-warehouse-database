﻿

CREATE procedure [dbo].[LoadSCRReferenceData] as

truncate table SCR.Priority

insert into SCR.Priority
select
	 PriorityCode = PRIORITY_CODE
	,Priority = PRIORITY_DESC
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.ltblPRIORITY_TYPE


truncate table SCR.CancerStatus

insert into SCR.CancerStatus
select
	 CancerStatusCode = Status_CODE
	,CancerStatus = Status_DESC
	,CancerStatusShort = SHORT_DESC
	,OrderBy = ORDER_BY
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.ltblSTATUS


truncate table SCR.SourceForOutpatient

insert into SCR.SourceForOutpatient
select
	 SourceForOutpatientCode = REF_CODE
	,SourceForOutpatient = REF_DESC
	,OrderBy = ORDER_BY
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.ltblOUT_PATIENT_REFERRAL


truncate table SCR.Cancellation

insert into SCR.Cancellation
select
	 CancellationCode = CANCELLED_CODE
	,Cancellation = CANCELLED_DESC
	,Live = LIVE
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.ltblCANCELLATION


truncate table SCR.DefinitiveType

insert into SCR.DefinitiveType
select
	 DefinitiveTypeCode = TREAT_CODE
	,DefinitiveType = TREAT_DESC
	,OrderBy = ORDER_BY
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.ltblDEFINITIVE_TYPE


truncate table SCR.TreatmentSetting

insert into SCR.TreatmentSetting
select
	 TreatmentSettingCode = SET_CODE
	,TreatmentSetting = SET_DESC
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.ltblTREATMENT_SETTING


truncate table SCR.AdjustmentReason

insert into SCR.AdjustmentReason
select
	 AdjustmentReasonCode = ADJ_REASON_CODE
	,AdjustmentReason = ADJ_REASON_DESC
	,Live = LIVE
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.ltblADJ_TREATMENT


truncate table SCR.Consultant

insert into SCR.Consultant
select
	 ConsultantId = CON_ID
	,NationalConsultantCode = NATIONAL_CODE
	,ConsultantCode = CON_CODE
	,Consultant = CON_DESC
	,SpecialtyCode = SPEC_CODE
	,Title = TITLE
	,DepartmentCode = Dept
	,Colorectal = COLORECTAL
	,Grade = GRADE
	,Baus = BAUS
	,IsDeleted = IS_DELETED
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.ltblCONSULTANTS


truncate table SCR.Diagnosis

insert into SCR.Diagnosis
select
	 DiagnosisCode = DIAG_CODE
	,Diagnosis = DIAG_DESC
	,Highlight = HIGHLIGHT
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.ltblDIAGNOSIS


truncate table SCR.TumorStatus

insert into SCR.TumorStatus
select
	 TumorStatusCode = STATUS_CODE
	,TumorStatus = STATUS_DESC
	,OrderBy = ORDER_BY
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.ltblCA_STATUS


truncate table SCR.Specialty

insert into SCR.Specialty
select
	 SpecialtyCode = SPECIALTY_CODE
	,Specialty = SPECIALTY_DESC
	,Division = SPECIALIES
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.ltblSPECIALTIES



truncate table SCR.CancerSite

insert into SCR.CancerSite
select
	 CancerSiteId = CA_ID
	,CancerSite = CA_SITE
	,CancerSiteRole = ROLE_NAME
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.ltblCANCER_SITES


truncate table SCR.Gender

insert into SCR.Gender
select
	 GenderCode = GENDER_CODE
	,Gender = GENDER_DESC
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.ltblGENDER



truncate table SCR.CancerType

insert into SCR.CancerType
select
	 CancerTypeCode = CANCER_TYPE_CODE
	,CancerType = CANCER_TYPE_DESC
	,Urology = UROLOGY
	,Haematology = HAEMATOLOGY
	,Breast = BREAST
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.ltblCANCER_TYPE



truncate table SCR.OPCS4

insert into SCR.OPCS4
select
	 OPCS4Code = PROC_CODE
	,OPCS4 = PROC_DESC
	,OPCSVersion = PROC_V
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.ltblPROCEDURES



truncate table SCR.Organisation

insert into SCR.Organisation
select
	 OrganisationCode = ORG_CODE
	,Organisation = ORG_DESC

from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.ltblORG_CODES


truncate table SCR.SourceOfReferral

insert into SCR.SourceOfReferral
select
	 SourceOfReferralCode = SOURCE_CODE
	,SourceOfReferral = SOURCE_DESC
	,SourceOfReferralShort = SHORT_DESC
	,SourceOfReferralMini = MINI_DESC
	,OrderBy = ORDER_BY
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.ltblSOURCE_REFERRAL


--KO added 28/05/2013

truncate table SCR.ErectileFunction
insert into SCR.ErectileFunction
select 
	FunctionID = Function_ID
	,Gender = M_F
	,ErectileDescription = ERECTILE_DESC
--into SCR.ErectileFunction
from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblERECTILE_FUNCTION


truncate table SCR.HCP
insert into SCR.HCP
select 
	HCPCode = HCP_CODE
	,HCP = HCP_DESC
	,HCPOrder = HCP_ORDER
--into SCR.HCP
from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblHCP


truncate table SCR.ImagingOutcomes
insert into SCR.ImagingOutcomes
select 
	OutcomeCode = OUTCOME_CODE
	,Outcome = OUTCOME_DESC
	,OutcomeOrder = OUTCOME_ORDER
--into SCR.ImagingOutcomes
from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblIMAGING_OUTCOMES


truncate table SCR.SNOMED
insert into SCR.SNOMED
select 
	SNOMEDCode = M_CODE
	,SNOMEDDescription = M_DESC
	,SNOMEDGroup = M_Group
	,SNOMEDVersion = [Version]
--into SCR.SNOMED
from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblSNOMED


truncate table SCR.TreatmentEvent
insert into SCR.TreatmentEvent
select 
	EventCode = EVENT_CODE
	,[Event] = EVENT_DESC
	,OrderBy = ORDER_BY
--into SCR.TreatmentEvent
from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblTREATMENT_EVENT


truncate table SCR.UrologyStatus
insert into SCR.UrologyStatus
select 
	StatusCode = STATUS_CODE
	,[Status] = STATUS_DESC
	,StatusSite = STATUS_SITE
--into SCR.UrologyStatus
from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblUROLOGY_STATUS


truncate table SCR.UrologyTract
insert into SCR.UrologyTract
select 
	StatusCode = STATUS_CODE
	,[Status] = STATUS_DESC
	,StatusOrder = STATUS_ORDER
--into SCR.UrologyTract
from [V-UHSM-SCR\SCR].CancerRegister.dbo.ltblUROLOGY_TRACT

exec dbo.LoadSCRAdditionalReferenceData

