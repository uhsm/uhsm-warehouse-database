﻿


CREATE procedure [dbo].[LoadSCRReferral]
as


declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0



truncate table SCR.Referral


insert into SCR.Referral
	(
	UniqueRecordId
	,DemographicUniqueRecordId
	,SystemId
	,CancerSite
	,SourceOfReferralCode
	,ReferringOrganisationCode
	,ReferrerCode
	,PriorityTypeCode
	,DecisionToReferDate
	,ReceiptOfReferralDate
	,ConsultantCode
	,SpecialtyCode
	,FirstAppointmentDate
	,FirstAppointmentOrganisationCode
	,ReferralToFirstAppointmentDelayReasonCode
	,CancerTypeCode
	,CancerStatusCode
	,FirstAppointmentOfferedCode
	,CancelledAppointmentDate
	,FirstAppointmentWaitingTimeAdjusted
	,FirstAppointmentAdjustmentReasonCode
	,MethodOfReferralCode
	,SourceForOutpatientCode
	,ReferredToSpecialistDate
	,ReferredFromOrganisationCode
	,FirstAppointmentWithSpecialistDate
	,FirstAppointmentSpecialistOrganisationCode
	,ConsultantUpgradeDate
	,OrganisationUpgradeCode
	,WhenPatientUpgradedCode
	,PatientUpgradedByCode
	,DiagnosisDate
	,PrimaryDiagnosisCode
	,PrimaryDiagnosisCode1
	,DiagnosisOrganisationCode
	,PatientInformedDate
	,OtherDiagnosisDate
	,LateralityCode
	,BasisOfDiagnosisCode
	,HistologyGroupCode
	,HistologyCode
	,GradeOfDifferentiationCode
	,PreTreatmentTStageCode
	,PreTreatmentTStageCertaintyCode
	,PreTreatmentNStageCode
	,PreTreatmentNStageCertaintyCode
	,PreTreatmentMStageCode
	,PreTreatmentMStageCertaintyCode
	,PreTreatmentOverallCertaintyCode
	,SiteSpecificClassificationCode
	,FinalOverallCertaintyCode
	,FinalTStageCertaintyCode
	,FinalTStageCode
	,FinalNStageCertaintyCode
	,FinalNStageCode
	,FinalMStageCertaintyCode
	,FinalMStageCode
	,GpInformedOfDiagnosis
	,GpInformedOfDiagnosisDate
	,ReasonNotInformed
	,RelativeCarerInformedOfDiagnosis
	,ReferredToSpecialistNurseDate
	,FirstSeenBySpecialistNurseDate
	,DecisionToReferAdjustment
	,DecisionToTreatAdjustment
	,DecisionToReferAdjustmentReasonCode
	,DecisionToTreatAdjustmentReasonCode
	,DecisionToReferAdjustmentDelayReasonCode
	,DecisionToTreatAdjustmentDelayReasonCode
	,ReferringSymptoms1
	,ReferringSymptoms2
	,ReferringSymptoms3
	,SpecialistNursePresent
	,FinalTNMDate
	,PreTreatmentTNMDate
	,FirstAppointmentConsultantCode
	,ReferralToAppropriateSpecialistCode
	,TertiaryCentreReferredToDate
	,TertiaryCentreOrganisationCode
	,ReasonForReferralTertiaryCentreCode
	,MakeNewReferralCode
	,NewTumourSite
	,AutoReferralFlag
	,OtherTumourSiteDiagnosisCode
	,OtherTumourSiteDiagnosis
	,InappropriateReferralCode
	,InappropriateReason
	,TumourStatusCode
	,NonCancerDetails
	,FirstAppointmentTypeCode
	,ReasonNoAppointmentCode
	,AssessedByCode
	,OtherSymptoms
	,AdditionalCommentsReferral
	,ReferralToFirstAppointmentDelayReasonComments
	,DecisionToReferDelayReasonComments
	,DecisionToTreatDelayReasonComments
	,DiagnosisComments
	,CNSSeenByIndicationCode
	,TransferReason 
	,DateNewReferral
	,TumourSiteNew
	,DateTransferActioned 
	,SourcesCareID 
	,ActionID 
	,DiagnosisActionID
	,OriginalSourceCareID 
)
select
	UniqueRecordId = CARE_ID
	,DemographicUniqueRecordId = PATIENT_ID
	,SystemId = TEMP_ID
	,CancerSite = L_CANCER_SITE
	,SourceOfReferralCode = case when N2_1_REFERRAL_SOURCE = '' then null else N2_1_REFERRAL_SOURCE end
	,ReferringOrganisationCode = case when N2_2_ORG_CODE_REF = '' then null else  SUBSTRING(N2_2_ORG_CODE_REF, 0,6)  end
	,ReferrerCode = case when N2_3_REFERRER_CODE = '' then null else N2_3_REFERRER_CODE end
	,PriorityTypeCode = N2_4_PRIORITY_TYPE
	,DecisionToReferDate = N2_5_DECISION_DATE
	,ReceiptOfReferralDate = N2_6_RECEIPT_DATE
	,ConsultantCode = case when N2_7_CONSULTANT = '' then null else N2_7_CONSULTANT end
	,SpecialtyCode = N2_8_SPECIALTY
	,FirstAppointmentDate = N2_9_FIRST_SEEN_DATE
	,FirstAppointmentOrganisationCode = N1_3_ORG_CODE_SEEN
	,ReferralToFirstAppointmentDelayReasonCode = N2_10_FIRST_SEEN_DELAY
	,CancerTypeCode = N2_12_CANCER_TYPE
	,CancerStatusCode = N2_13_CANCER_STATUS
	,FirstAppointmentOfferedCode = L_FIRST_APPOINTMENT
	,CancelledAppointmentDate = L_CANCELLED_DATE
	,FirstAppointmentWaitingTimeAdjusted = N2_14_ADJ_TIME
	,FirstAppointmentAdjustmentReasonCode = N2_15_ADJ_REASON
	,MethodOfReferralCode = L_REFERRAL_METHOD
	,SourceForOutpatientCode = case when N2_16_OP_REFERRAL = '' then null else N2_16_OP_REFERRAL end
	,ReferredToSpecialistDate = L_SPECIALIST_DATE
	,ReferredFromOrganisationCode = L_ORG_CODE_SPECIALIST
	,FirstAppointmentWithSpecialistDate = L_SPECIALIST_SEEN_DATE
	,FirstAppointmentSpecialistOrganisationCode = N1_3_ORG_CODE_SPEC_SEEN
	,ConsultantUpgradeDate = N_UPGRADE_DATE
	,OrganisationUpgradeCode = N_UPGRADE_ORG_CODE
	,WhenPatientUpgradedCode = L_UPGRADE_WHEN
	,PatientUpgradedByCode = L_UPGRADE_WHO
	,DiagnosisDate = N4_1_DIAGNOSIS_DATE
	,PrimaryDiagnosisCode = case when L_DIAGNOSIS = '' then null else L_DIAGNOSIS end
	,PrimaryDiagnosisCode1 = case when N4_2_DIAGNOSIS_CODE = '' then null else N4_2_DIAGNOSIS_CODE end
	,DiagnosisOrganisationCode = L_ORG_CODE_DIAGNOSIS
	,PatientInformedDate = L_PT_INFORMED_DATE
	,OtherDiagnosisDate = L_OTHER_DIAG_DATE
	,LateralityCode = N4_3_LATERALITY
	,BasisOfDiagnosisCode = N4_4_BASIS_DIAGNOSIS
	,HistologyGroupCode = L_HISTOLOGY_GROUP
	,HistologyCode = N4_5_HISTOLOGY
	,GradeOfDifferentiationCode = N4_6_DIFFERENTIATION
	,PreTreatmentTStageCode = N6_1_PRE_T_STAGE
	,PreTreatmentTStageCertaintyCode = N6_2_PRE_T_CERTAIN
	,PreTreatmentNStageCode = N6_3_PRE_N_STAGE
	,PreTreatmentNStageCertaintyCode = N6_4_PRE_N_CERTAIN
	,PreTreatmentMStageCode = N6_5_PRE_M_STAGE
	,PreTreatmentMStageCertaintyCode = N6_6_PRE_M_CERTAIN
	,PreTreatmentOverallCertaintyCode = N6_8_PRE_TNM_CERTAIN
	,SiteSpecificClassificationCode = N6_9_SITE_CLASSIFICATION
	,FinalOverallCertaintyCode = N_L_PATH_TNM_CERTAIN
	,FinalTStageCertaintyCode = N_L16_PATH_T_CERTAIN
	,FinalTStageCode = N6_11_PATH_T_STAGE
	,FinalNStageCertaintyCode = N_L17_PATH_N_CERTAIN
	,FinalNStageCode = N6_12_PATH_N_STAGE
	,FinalMStageCertaintyCode = N_L18_PATH_M_CERTAIN
	,FinalMStageCode = N6_13_PATH_M_STAGE
	,GpInformedOfDiagnosis = L_GP_INFORMED
	,GpInformedOfDiagnosisDate = L_GP_INFORMED_DATE
	,ReasonNotInformed = L_GP_NOT
	,RelativeCarerInformedOfDiagnosis = L_REL_INFORMED
	,ReferredToSpecialistNurseDate = L_SPEC_NURSE_DATE
	,FirstSeenBySpecialistNurseDate = L_SEEN_NURSE_DATE
	,DecisionToReferAdjustment = N16_1_ADJ_DAYS
	,DecisionToTreatAdjustment = N16_2_ADJ_DAYS
	,DecisionToReferAdjustmentReasonCode = N16_3_ADJ_DECISION_CODE
	,DecisionToTreatAdjustmentReasonCode = N16_4_ADJ_TREAT_CODE
	,DecisionToReferAdjustmentDelayReasonCode = N16_5_DECISION_REASON_CODE
	,DecisionToTreatAdjustmentDelayReasonCode = N16_6_TREATMENT_REASON_CODE
	,ReferringSymptoms1 = L_SYMPTOMS
	,ReferringSymptoms2 = L_SYMPTOMS2
	,ReferringSymptoms3 = L_SYMPTOMS3
	,SpecialistNursePresent = L_NURSE_PRESENT
	,FinalTNMDate = L_PATH_TNM_DATE
	,PreTreatmentTNMDate = L_CLINICAL_TNM_DATE
	,FirstAppointmentConsultantCode = case when L_FIRST_CONSULTANT = '' then null else L_FIRST_CONSULTANT end
	,ReferralToAppropriateSpecialistCode = L_APPROPRIATE
	,TertiaryCentreReferredToDate = L_TERTIARY_DATE
	,TertiaryCentreOrganisationCode = L_TERTIARY_TRUST
	,ReasonForReferralTertiaryCentreCode = L_TERTIARY_REASON
	,MakeNewReferralCode = L_INAP_REF
	,NewTumourSite = L_NEW_CA_SITE
	,AutoReferralFlag = L_AUTO_REF
	,OtherTumourSiteDiagnosisCode = L_SEC_DIAGNOSIS_G
	,OtherTumourSiteDiagnosis = L_SEC_DIAGNOSIS
	,InappropriateReferralCode = L_WRONG_REF
	,InappropriateReason = L_WRONG_REASON
	,TumourStatusCode = L_TUMOUR_STATUS
	,NonCancerDetails = L_NON_CANCER
	,FirstAppointmentTypeCode = L_FIRST_APP
	,ReasonNoAppointmentCode = L_NO_APP
	,AssessedByCode = L_DIAG_WHO
	,OtherSymptoms = L_OTHER_SYMPS
	,AdditionalCommentsReferral = L_COMMENTS
	,ReferralToFirstAppointmentDelayReasonComments = N2_11_FIRST_SEEN_REASON
	,DecisionToReferDelayReasonComments = N16_7_DECISION_REASON
	,DecisionToTreatDelayReasonComments = N16_8_TREATMENT_REASON
	,DiagnosisComments = L_DIAGNOSIS_COMMENTS
	,CNSSeenByIndicationCode = L_INDICATOR_CODE
	,TransferReason = [TRANSFER_REASON] 
	,DateNewReferral = [DATE_NEW_REFERRAL]
	,TumourSiteNew = [TUMOUR_SITE_NEW]
	,DateTransferActioned = [DATE_TRANSFER_ACTIONED]
	,SourcesCareID = [SOURCE_CARE_ID]
	,Action_ID = [ACTION_ID]
	,DiagnosisActionID = [DIAGNOSIS_ACTION_ID]
	,OriginalSourceCareID = [ORIGINAL_SOURCE_CARE_ID] 
	
	
	
	
	
	
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.tblMAIN_REFERRALS


select @RowsInserted = @@rowcount


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadSCRReferral', @Stats, @StartTime

print @Stats




