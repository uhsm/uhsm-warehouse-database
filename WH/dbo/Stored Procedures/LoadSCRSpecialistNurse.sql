﻿

CREATE procedure [dbo].[LoadSCRSpecialistNurse] as

/**
K Oakden 23/03/2015 Additional SCR data
**/
declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0


truncate table SCR.SpecialistNurse

insert into SCR.SpecialistNurse
	(
	[NURSE_ID]
	,[CARE_ID]
	,[TEMP_ID]
	,[FIRST_CONTACT]
	,[L_DEC_REF_DATE]
	,[L_REF_DATE]
	,[L_SEEN_DATE]
	,[L_DISCH_DATE]
	,[L_REF_SOURCE]
	,[L_INCLUDED]
	,[L_TOTAL_TIME]
	,[L_CONTACT_PLACE]
	,[L_CONTACT_TYPE]
	,[L_CNS]
	,[L_NURSE]
	,[L_KEY_WORKER]
	,[L_PRESENT_SURGEON]
	,[L_PRESENT]
	,[L_PRESENT_DOC]
	,[L_PRESENT_NURSE]
	,[L_PRESENT_GP]
	,[L_PRESENT_OTHER]
	,[L_PRESENT_UNKNOWN]
	,[L_PRESENT_CONSULTANT]
	,[L_CONTACT_PAT]
	,[L_CONTACT_REL]
	,[L_CONTACT_CARER]
	,[L_CONTACT_PROF]
	,[L_PROF_WHO]
	,[L_ACTIVITY_1]
	,[L_ACTIVITY_2]
	,[L_ACTIVITY_3]
	,[L_ACTIVITY_4]
	,[L_REFERRAL_1]
	,[L_REFERRAL_2]
	,[L_CONTACT]
	,[L_ACCEPTED]
	,[L_VERBAL]
	,[L_WRITTEN]
	,[L_DETAILS]
	,[L_PRIVACY]
	,[L_DN_INFORMED]
	,[L_UNDERSTANDING]
	,[L_INT_ADVICE]
	,[L_INT_SHORT]
	,[L_INT_REG]
	,[L_PT_STATUS]
	,[L_ORGANISATION]
	,[L_PROGNOSIS]
	,[L_NOTES]
	,[REASON_CNS_NOT_PRESENT]
	,[REASON_CNS_NOT_PRESENT_OTHER]
	,[LEVEL_INTERVENTION_1]
	,[LEVEL_INTERVENTION_2]
	,[LEVEL_INTERVENTION_3]
	,[LEVEL_INTERVENTION_4]
	,[L_CONTACT_OTHER]
	,[PERFORMANCE_STATUS]
	,[OTHER_ACTIVITY_1]
	,[OTHER_ACTIVITY_2]
	,[OTHER_ACTIVITY_3]
	,[OTHER_ACTIVITY_4]
	,[OTHER_ACTIVITY_5]
	,[OTHER_ACTIVITY_6]
	,[L_ACTIVITY_5]
	,[L_ACTIVITY_6]
	,[OTHER_ACTIVITY_7]
	,[L_REFERRAL_3]
	,[L_REFERRAL_OTHER]
	,[DISTRESS_SCORE]
	,[HOLISTIC_ASSESSMENT_DECLINED]
	,[HOLISTIC_ASSESSMENT_END]
	,[L_KEY_WORKER_OTHER]
	,[L_KEY_WORKER_NEW])
select
	[NURSE_ID]
	,[CARE_ID]
	,[TEMP_ID]
	,[FIRST_CONTACT]
	,[L_DEC_REF_DATE]
	,[L_REF_DATE]
	,[L_SEEN_DATE]
	,[L_DISCH_DATE]
	,[L_REF_SOURCE]
	,[L_INCLUDED]
	,[L_TOTAL_TIME]
	,[L_CONTACT_PLACE]
	,[L_CONTACT_TYPE]
	,[L_CNS]
	,[L_NURSE]
	,[L_KEY_WORKER]
	,[L_PRESENT_SURGEON]
	,[L_PRESENT]
	,[L_PRESENT_DOC]
	,[L_PRESENT_NURSE]
	,[L_PRESENT_GP]
	,[L_PRESENT_OTHER]
	,[L_PRESENT_UNKNOWN]
	,[L_PRESENT_CONSULTANT]
	,[L_CONTACT_PAT]
	,[L_CONTACT_REL]
	,[L_CONTACT_CARER]
	,[L_CONTACT_PROF]
	,[L_PROF_WHO]
	,[L_ACTIVITY_1]
	,[L_ACTIVITY_2]
	,[L_ACTIVITY_3]
	,[L_ACTIVITY_4]
	,[L_REFERRAL_1]
	,[L_REFERRAL_2]
	,[L_CONTACT]
	,[L_ACCEPTED]
	,[L_VERBAL]
	,[L_WRITTEN]
	,[L_DETAILS]
	,[L_PRIVACY]
	,[L_DN_INFORMED]
	,[L_UNDERSTANDING]
	,[L_INT_ADVICE]
	,[L_INT_SHORT]
	,[L_INT_REG]
	,[L_PT_STATUS]
	,[L_ORGANISATION]
	,[L_PROGNOSIS]
	,[L_NOTES]
	,[REASON_CNS_NOT_PRESENT]
	,[REASON_CNS_NOT_PRESENT_OTHER]
	,[LEVEL_INTERVENTION_1]
	,[LEVEL_INTERVENTION_2]
	,[LEVEL_INTERVENTION_3]
	,[LEVEL_INTERVENTION_4]
	,[L_CONTACT_OTHER]
	,[PERFORMANCE_STATUS]
	,[OTHER_ACTIVITY_1]
	,[OTHER_ACTIVITY_2]
	,[OTHER_ACTIVITY_3]
	,[OTHER_ACTIVITY_4]
	,[OTHER_ACTIVITY_5]
	,[OTHER_ACTIVITY_6]
	,[L_ACTIVITY_5]
	,[L_ACTIVITY_6]
	,[OTHER_ACTIVITY_7]
	,[L_REFERRAL_3]
	,[L_REFERRAL_OTHER]
	,[DISTRESS_SCORE]
	,[HOLISTIC_ASSESSMENT_DECLINED]
	,[HOLISTIC_ASSESSMENT_END]
	,[L_KEY_WORKER_OTHER]
	,[L_KEY_WORKER_NEW]
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.tblSPECIALIST_NURSE



select @RowsInserted = @@rowcount


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadSCRSpecialistNurse', @Stats, @StartTime

--print @Stats



