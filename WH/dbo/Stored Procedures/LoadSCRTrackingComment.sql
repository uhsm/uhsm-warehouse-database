﻿

CREATE procedure [dbo].[LoadSCRTrackingComment]
as


declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0



truncate table SCR.TrackingComment


insert into SCR.TrackingComment
	(
	 UniqueRecordId
	,ReferralUniqueRecordId
	,UserID
	,TrackingCommentTime
	,TrackingComment
	)
select
	 UniqueRecordId = COM_ID
	,ReferralUniqueRecordId= CARE_ID
	,UserID = [USER_ID]
	,TrackingCommentTime = DATE_TIME
	,TrackingComment = COMMENTS
from
	[V-UHSM-SCR\SCR].CancerRegister.dbo.tblTRACKING_COMMENTS


select @RowsInserted = @@rowcount


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadSCRTrackingComment', @Stats, @StartTime

print @Stats



