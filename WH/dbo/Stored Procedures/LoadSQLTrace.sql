﻿CREATE PROCEDURE [dbo].[LoadSQLTrace]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)
declare @LogFile varchar(max)
declare @LogFilePath varchar(max)
declare @InitialFileName varchar(max)
declare @AuditFileOffset bigint

select
	@StartTime = getdate()

--get the name of the log file
select
	@LogFile = TextValue
from
	dbo.Parameter
where
	Parameter = 'AUDITLOG'

--get the location of the log files
select
	@LogFilePath = log_file_path
from
	sys.server_file_audits
where
	name = 'Audit Patient Detail Access'


--what was the offset of the last data loaded
if exists
	(
	select
		1
	from
		Audit.SQLTrace
	)
	select
		 @AuditFileOffset = max(audit_file_offset)

		,@InitialFileName = 
			@LogFilePath + @LogFile
	from
		Audit.SQLTrace
	else
	select
		 @AuditFileOffset = null
		,@InitialFileName = null


--import SQL audit data
insert into Audit.SQLTrace
(
	 event_time
	,sequence_number
	,action_id
	,succeeded
	,permission_bitmask
	,is_column_permission
	,session_id
	,server_principal_id
	,database_principal_id
	,target_server_principal_id
	,target_database_principal_id
	,object_id
	,class_type
	,session_server_principal_name
	,server_principal_name
	,server_principal_sid
	,database_principal_name
	,target_server_principal_name
	,target_server_principal_sid
	,target_database_principal_name
	,server_instance_name
	,database_name
	,schema_name
	,object_name
	,statement
	,additional_information
	,file_name
	,audit_file_offset
)
select
	 event_time
	,sequence_number
	,action_id
	,succeeded
	,permission_bitmask
	,is_column_permission
	,session_id
	,server_principal_id
	,database_principal_id
	,target_server_principal_id
	,target_database_principal_id
	,object_id
	,class_type
	,session_server_principal_name
	,server_principal_name
	,server_principal_sid
	,database_principal_name
	,target_server_principal_name
	,target_server_principal_sid
	,target_database_principal_name
	,server_instance_name
	,database_name
	,schema_name
	,object_name
	,statement
	,additional_information
	,file_name
	,audit_file_offset
from
	fn_get_audit_file(
		 @LogFilePath + '*'
		,@InitialFileName
		,@AuditFileOffset
	)

SELECT @RowsInserted = @@Rowcount



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadSQLTrace', @Stats, @StartTime

print @Stats
