﻿CREATE Procedure LoadTheatreLastMinuteCancellations as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0

Truncate table Theatre.LastMinuteCancellations

insert into Theatre.LastMinuteCancellations (
	 [SourceUniqueID]
	,[TCIToOperationDate]
	,[KeyCancellationDateFrom]
	,[CancellationToOperation]
	,[LastMinuteCancellations]
	,[ProposedOperationDate]
	,[BookingOperationDate]
	,[OfferOperationDate]
	,[TCIDate]
	,[TCIDateTime]
	,[DiffOperDates]
	,[BookingStatus]
	,[OfferRefno]
	,[OfferOutcome]
	,[OfferOutcomeDate]
	,[OrmisCancellationDate]
	,[FacilID]
	,[CC_DETAILS]
	,[OrmisCancellationUpdated]
	,[OrmisWLReference]
	,[OfferDateUpdated]
)
	SELECT
	SourceUniqueID = canc.cc_sequ,
	TCIToOperationDate = 
		DATEDIFF(D,cast(convert(varchar(8), offer.TCI_DTTM,112) as datetime),coalesce(booking.PA_OPER_DATE,canc.CC_DATE)),
	KeyCancellationDateFrom = 
		Case When offer.ADMOF_REFNO IS NULL Then
			'Not Applicable'
		Else
			Case When offer.OFOCM_DTTM < canc.CC_DATE_TIME_CANCELLED Then
				'Offer Outcome'
			Else
				'Ormis CancellationDate'
			End
		End,
	CancellationToOperation =	
		DATEDIFF(D,
		(Case When Offer.OFOCM_DTTM < canc.CC_DATE_TIME_CANCELLED Then
			ofocm_dttm
		Else
			canc.CC_DATE_TIME_CANCELLED
		End
		)
		,
		coalesce(booking.PA_OPER_DATE,canc.CC_DATE)
		),
	LastMinuteCancellations =
		Case When offer.ADMOF_REFNO IS NUll Then
			'N'
		Else 
			Case When DATEDIFF(D,
			(Case When Ofocm_dttm < canc.CC_DATE_TIME_CANCELLED Then
				ofocm_dttm
			Else
				canc.CC_DATE_TIME_CANCELLED
			End
			) 	,
			coalesce(booking.PA_OPER_DATE,canc.CC_DATE)
			)
			<= 0 Then
				'Y'
			Else
				'N'
			End
		End,
	
	ProposedOperationDate = canc.CC_DATE,
	BookingOperationDate = booking.PA_OPER_DATE,
	OfferOperationDate = offer.operation_dttm,
	TCIDate = cast(convert(varchar(8), offer.TCI_DTTM,112) as datetime),
	TCIDateTime = offer.TCI_DTTM,
	Case when Canc.CC_DATE <> booking.PA_OPER_DATE then
		1
	else
		0
	end as DiffOperDates,
	BookingStatus =
	case booking.pa_transf
		when 0 then 'Booked'
		when 1 then 'Complete'
		when 2 then 'cancelled'
		when 3 then 'On Hold'
		when 5 then 'Inpatient Waiting'
	Else 'Other'
	end,
	OfferRefno = offer.ADMOF_REFNO,
	OfferOutcome = outcome.DESCRIPTION,
	OfferOutcomeDate = offer.OFOCM_DTTM ,
	OrmisCancellationDate = canc.CC_DATE_TIME_CANCELLED,
	FacilID = canc.CC_MRN,
	canc.CC_DETAILS,
	OrmisCancellationUpdated = canc.CC_LOG_DATE,
	OrmisWLReference = booking.PA_EPISODE_NUM,
	OfferDateUpdated = offer.MODIF_DTTM
	--into Theatre.LastMinuteCancellations
	FROM ORMIS.dbo.FCCITEMS Canc
	inner join ORMIS.dbo.FPATS booking
		on Canc.CC_PA_SEQU = booking.PA_SEQU
	left outer join Lorenzo.dbo.AdmissionOffer offer
		on cast(booking.PA_EPISODE_NUM as int) = offer.WLIST_REFNO
		and cast(convert(varchar(8), offer.TCI_DTTM,112) as datetime) <= Canc.CC_DATE
		and offer.ARCHV_FLAG = 'N'
		and not exists (select 1 from Lorenzo.dbo.AdmissionOffer prev
						where offer.WLIST_REFNO = prev.WLIST_REFNO 
						and offer.TCI_DTTM < prev.TCI_DTTM
						)
		and not exists (select 1 from Lorenzo.dbo.AdmissionOffer prev
						where offer.WLIST_REFNO = prev.WLIST_REFNO 
						and offer.MODIF_DTTM < prev.MODIF_DTTM
						)
	left outer join Lorenzo.dbo.ReferenceValue outcome
		on offer.OFOCM_REFNO = outcome.RFVAL_REFNO

select @RowsInserted = @@rowcount


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadTheatreLastMinuteCancellation', @Stats, @StartTime

print @Stats


