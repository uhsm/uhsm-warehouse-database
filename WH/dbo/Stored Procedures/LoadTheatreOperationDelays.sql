﻿CREATE procedure [dbo].[LoadTheatreOperationDelays]
as
/*
 Modified Date				Modified By			Modification
-----------------			------------		------------
15/07/2014					CMan				Created table as new feed
*/



declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0



truncate table Theatre.Delays


insert into Theatre.Delays
(
	 DelaySourceUniqueID
	,OperationDetailSourceUniqueID
	,DelayStage
	,DelayMinutes
	,DelayComments
	,DelayReasonCode
	
)

Select  DelaySourceUniqueID = DIT_SEQU
		, OperationDetailSourceUniqueID = DIT_OP_SEQU 
		, DelayStage = DIT_SOURCE
		, DelayMinutes = DIT_MINUTES
		, DelayComments = DIT_FREE_TEXT
		, DelayReasonCode = DIT_PR_SEQU
from ORMIS.dbo.F_Delay_Items


select @RowsInserted = @@rowcount


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadTheatreOperationDelays', @Stats, @StartTime

print @Stats

