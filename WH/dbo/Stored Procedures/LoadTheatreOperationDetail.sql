﻿CREATE procedure [dbo].[LoadTheatreOperationDetail]
as




/*
 Modified Date				Modified By			Modification
-----------------			------------		------------
17/11/2014					CMan				Amended Feed to include the details of the Anaesthetic Type use for the oper - as per AA request
*/

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0



truncate table Theatre.OperationDetail

--create temp table of the anaesthetic type used per operation - 17/11/2014 CM Added this section get get anaesthetic type codes into one line per operation 
If OBJECT_ID ('tempdb..#ana') > 0
Drop table #ana
SELECT final.PANL_OP_SEQU as OperationDetailSourceUniqueID,
        LEFT(final.Res, Len(final.Res) - 1) AS AnaestheticTypeCodeSet,-- concatenate the anaesthetic codes into one field
        LEFT(final.Res2, Len(final.Res2) - 1) AS AnaestheticTypeSet--concatenate the anaesthetic descriptions into one field
into #ana
FROM   (SELECT DISTINCT st2.PANL_OP_SEQU,
                      (SELECT 	AnaesT1.ANA_CODE  + ',' AS [text()]								
                            from ORMIS.dbo.F_Pat_Anaes_Link st1
								left join ORMIS.dbo.F_Anaesthetic AnaesT1   on ANA_SEQU = PANL_ANA_SEQU
								where st1.PANL_OP_SEQU = st2.PANL_OP_SEQU 
								Order by PANL_OP_SEQU
                         FOR XML PATH ('')) Res,
                             (SELECT AnaesT1.ANA_DESCRIPTION  + ',' AS [text()]								
                            from ORMIS.dbo.F_Pat_Anaes_Link st1
								left join ORMIS.dbo.F_Anaesthetic AnaesT1   on ANA_SEQU = PANL_ANA_SEQU
								where st1.PANL_OP_SEQU = st2.PANL_OP_SEQU 
								Order by PANL_OP_SEQU
                         FOR XML PATH ('')) Res2
									 from ORMIS.dbo.F_Pat_Anaes_Link st2

								)final




insert into Theatre.OperationDetail
	(
	 DistrictNo
	,SourceUniqueID
	,PatientAgeInYears
	,OperationDay
	,OperationStartDate
	,TheatreCode
	,OperationDate
	,OperationEndDate
	,DateOfBirth
	,OperationTypeCode
	,OperationOrder
	,Forename
	,InSuiteTime
	,Surname
	,InAnaestheticTime
	,InRecoveryTime
	,BiohazardFlag
	,ASAScoreCode
	,UnplannedReturnReasonCode
	,ReviewFlag
	,OperationDetailSourceUniqueID
	,Operation
	,PatientDiedFlag
	,CauseOfDeath
	,DiagnosisCode
	,PatientAgeInYearsMonths
	,PatientAgeInYearsDays
	,SexCode
	,TransferredToWardCode
	,PatientSourceUniqueNo
	,PatientClassificationCode
	,PatientBookingSourceUniqueID
	,SpecialtyCode
	,FromWardCode
	,ToWardCode
	,ConsultantCode
	,Address1
	,Address3
	,Postcode
	,HomeTelephone
	,BusinessTelephone
	,Surgeon1Code
	,Surgeon2Code
	,Surgeon3Code
	,Anaesthetist1Code
	,Anaesthetist2Code
	,Anaesthetist3Code
	,ScoutNurseCode
	,AnaesthetistNurseCode
	,InstrumentNurseCode
	,Technician1Code
	,Technician2Code
	,Technician3Code
	,SessionNumber
	,CampusCode
	,SpecimenCode
	,DischargeTime
	,StaffInChargeCode
	,EscortReturnedTime
	,OperationCancelledFlag
	,DelayCode
	,AnaestheticInductionTime
	,ReadyToDepartTime
	,OtherCommentCode
	,PACUClassificationCode
	,PostOperationInstructionCode
	,ClinicalPriorityCode
	,EmergencyOperationSurgeonCode
	,SecondaryStaffCode
	,HospitalDischargeDate
	,HospitalEpisodeNo
	,UnplannedReturnToTheatreFlag
	,AdmissionTime
	,WaitingListSourceUniqueNo
	,AdmissionTypeCode
	,CalfCompressorUsedFlag
	,OperatingRoomReadyTime
	,DressingTime
	,NursingSetupCompleteTime
	,LastUpdated
	,ProceduralStatusCode
	,ACCNumber
	,ACCIndicatorCode
	,ACCCode
	,UpdatedFlag
	,PatientAddressStatus
	,OperationKey
	,ExtubationTime
	,Address2
	,Address4
	,SentForTime
	,PorterLeftTime
	,PorterCode
	,SecondaryTheatreCode
	,SecondaryConsultantCode
	,SecondaryOrderNumberCode
	,SecondarySessionNumber
	,AdditionalFlag
	,DictatedSurgeonNoteCode
	,AnaestheticReadyTime
	,PreparationReadyTime
	,SessionSourceUniqueID
	,CombinedCaseSessionSourceUniqueID
	,IncisionDetailCode
	,AnaestheticTypeCodeSet
	,AnaestheticTypeSet
)


select
	 DistrictNo = OP_MRN
	,SourceUniqueID = OP_SEQU
	,PatientAgeInYears = OP_AGE_YRS
	,OperationDay = OP_DAY
	,OperationStartDate = OP_START_DATE_TIME
	,TheatreCode = OP_TH_SEQU
	,OperationDate = OP_OPERATION_DATE
	,OperationEndDate = OP_FINISH_DATE_TIME
	,DateOfBirth = OP_DOB
	,OperationTypeCode = OP_OPERAT_TYPE
	,OperationOrder = OP_ORDER
	,Forename = OP_FNAME
	,InSuiteTime = OP_IN_SUITE_DATE_TIME
	,Surname = OP_LNAME
	,InAnaestheticTime = OP_IN_ANAES_DATE_TIME
	,InRecoveryTime = OP_IN_REC_DATE_TIME
	,BiohazardFlag = OP_BIOHAZARD
	,ASAScoreCode = OP_ASA_SEQU
	,UnplannedReturnReasonCode = OP_UP_SEQU
	,ReviewFlag = OP_REVIEW
	,OperationDetailSourceUniqueID = OP_DETAILS_SEQU
	,Operation = OP_DESCRIPTION
	,PatientDiedFlag = OP_DEATH
	,CauseOfDeath = OP_CAUSE
	,DiagnosisCode = OP_DIAGNOSIS_SEQU
	,PatientAgeInYearsMonths = OP_AGE_MONTH
	,PatientAgeInYearsDays = OP_AGE_DAYS
	,SexCode = OP_GENDER
	,TransferredToWardCode = OP_WARD_TO
	,PatientSourceUniqueNo = OP_PIN_SEQU
	,PatientClassificationCode = OP_CL_SEQU
	,PatientBookingSourceUniqueID = OP_PA_SEQU
	,SpecialtyCode = OP_S1_SEQU
	,FromWardCode = OP_WA_FR_SEQU
	,ToWardCode = OP_WA_TO_SEQU
	,ConsultantCode = OP_CONS_SEQU
	,Address1 = OP_STREET
	,Address3 = OP_SUBURB
	,Postcode = OP_POSTCODE
	,HomeTelephone = OP_HOME_PHONE
	,BusinessTelephone = OP_BUS_PHONE
	,Surgeon1Code = OP_SU1_SEQU
	,Surgeon2Code = OP_SU2_SEQU
	,Surgeon3Code = OP_SU3_SEQU
	,Anaesthetist1Code = OP_AN1_SEQU
	,Anaesthetist2Code = OP_AN2_SEQU
	,Anaesthetist3Code = OP_AN3_SEQU
	,ScoutNurseCode = OP_SC_NUR_SEQU
	,AnaesthetistNurseCode = OP_AN_NUR_SEQU
	,InstrumentNurseCode = OP_IN_NUR_SEQU
	,Technician1Code = OP_TECH1_SEQU
	,Technician2Code = OP_TECH2_SEQU
	,Technician3Code = OP_TECH3_SEQU
	,SessionNumber = OP_SESSION_NO
	,CampusCode = OP_CA_SEQU
	,SpecimenCode = OP_SP_SEQU
	,DischargeTime = OP_DISCH_DATE_TIME
	,StaffInChargeCode = OP_IC_SEQU
	,EscortReturnedTime = OP_ESCORT_RET
	,OperationCancelledFlag = OP_CANCELLED
	,DelayCode = OP_DE_SEQU
	,AnaestheticInductionTime = OP_ANAES_IND_DATE_TIME
	,ReadyToDepartTime = OP_READY_DEPART_DATE_TIME
	,OtherCommentCode = OP_OTHER_COMMENTS_SEQU
	,PACUClassificationCode = OP_PC_SEQU
	,PostOperationInstructionCode = OP_POST_OP_COMMENTS_SEQU
	,ClinicalPriorityCode = OP_CLP_SEQU
	,EmergencyOperationSurgeonCode = OP_ADMIT_SU_SEQU
	,SecondaryStaffCode = OP_SN_SEQU
	,HospitalDischargeDate = OP_HOSP_DIS_DATE_TIME
	,HospitalEpisodeNo = OP_EPISODE_NUM
	,UnplannedReturnToTheatreFlag = OP_RETURN_TO_THEATRE
	,AdmissionTime = OP_ADMIT_DATE_TIME
	,WaitingListSourceUniqueNo = OP_WD_SEQU
	,AdmissionTypeCode = OP_TA_SEQU
	,CalfCompressorUsedFlag = OP_CALF_COMP
	,OperatingRoomReadyTime = OP_OR_READY_DATE_TIME
	,DressingTime = OP_DRESSING_DATE_TIME
	,NursingSetupCompleteTime = OP_SETUP_COMP_DATE_TIME
	,LastUpdated = OP_UPD_DATE_TIME
	,ProceduralStatusCode = OP_STATUS
	,ACCNumber = OP_ACC_NUMBER
	,ACCIndicatorCode = OP_ACC_INDICATOR
	,ACCCode = OP_ACC_SEQU
	,UpdatedFlag = OP_UPDATED
	,PatientAddressStatus = OP_STATE
	,OperationKey = OP_OPER_NO
	,ExtubationTime = OP_EXTUBATION_DATE_TIME
	,Address2 = OP_STREET_2
	,Address4 = OP_CITY
	,SentForTime = OP_SENT_FOR_DATE_TIME
	,PorterLeftTime = OP_PORTER_LEFT_DATE_TIME
	,PorterCode = OP_PORTER_SEQU
	,SecondaryTheatreCode = OP_TH_CC_SEQU
	,SecondaryConsultantCode = OP_CONS_CC_SEQU
	,SecondaryOrderNumberCode = OP_ORDER_CC
	,SecondarySessionNumber = OP_SESSION_NO_CC
	,AdditionalFlag = OP_ADDITIONAL
	,DictatedSurgeonNoteCode = OP_DICTATED_NOTE
	,AnaestheticReadyTime = OP_ANAES_READY_DATE_TIME
	,PreparationReadyTime = OP_PREP_READY_DATE_TIME
	,SessionSourceUniqueID = OP_SS_SEQU
	,CombinedCaseSessionSourceUniqueID = OP_CC_SS_SEQU
	,IncisionDetailCode = OP_INCISION_DETAILS_SEQU
	,AnaestheticTypeCodeSet = #ana.AnaestheticTypeCodeSet
	,AnaestheticTypeSet = #ana.AnaestheticTypeSet
from
	ORMIS.dbo.FOPERAT OperationDetail
Left outer join #ana on OperationDetail.OP_SEQU = #ana.OperationDetailSourceUniqueID


select @RowsInserted = @@rowcount


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadTheatreOperationDetail', @Stats, @StartTime

print @Stats

