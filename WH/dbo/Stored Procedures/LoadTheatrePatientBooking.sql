﻿CREATE procedure [dbo].[LoadTheatrePatientBooking]
as


declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0



truncate table Theatre.PatientBooking


insert into Theatre.PatientBooking
	(
	 SequenceNo
	,SourceUniqueID
	,DistrictNo
	,IntendedProcedureDate
	,IntendedTheatreCode
	,CustodianDetail
	,IntendedSessionTypeCode
	,Surname
	,Forename
	,DateOfBirth
	,PatientSourceUniqueID
	,TheatreCode
	,ConsultantCode
	,ConsultantCode2
	,BiohazardFlag
	,BloodProductsRequiredFlag
	,SpecialEquipmentFlag
	,PatientAgeInYears
	,PatientAgeInMonths
	,AdmissionWardCode
	,SessionStartTime
	,PatientEpisodeNumber
	,Operation
	,PreferredOrder
	,PatientBookingStatusCode
	,PatientStatusCode
	,PatientClassificationCode2
	,AnaestheticCode
	,AnaestheticTypeCode
	,SurgeonComment
	,SurgeonSpecialtyCode
	,BedNumber
	,WaitingListSourceUniqueID
	,SessionNo
	,OperationDuration
	,ChangeOverDuration
	,SexCode
	,SurgeonCode
	,BookingEventAdmittedTime
	,SurgeryRequestedTime
	,ReadyForSurgeryTime
	,VerifiedFlag
	,PlannedReturnFlag
	,EmergencyFlag
	,TheatreSessionSort
	,EstimatedStartTime
	,IntendedSurgeonCode
	,PatientClassificationCode
	,WardCode
	,Surgeon1SpecialtyCode
	,LogLastUpdated
	,RecordLogDetails
	,WaitingDetailsSourceUniqueID
	,AnaesthetistCode
	,Address1
	,Address3
	,Postcode
	,HomeTelephone
	,BusinessTelephone
	,AgeInDays
	,AdmissionTypeCode
	,ChangedSinceLastDataTransferFlag
	,ASAScoreCode
	,Unused4
	,Unused5
	,Unused6
	,Unused7
	,Unused8
	,PathologyRequiredFlag
	,XrayRequiredFlag
	,PathologyComment
	,XrayComment
	,BloodProductsRequiredComment
	,UnitsOfBloodProductRequired
	,WardAdvisedTime
	,WardAdvisedTime2
	,ModeOfTransportCode
	,NurseRequestingTransferCode
	,NurseReceivingNotificationCode
	,OxygenRequiredFlag
	,IVFlag
	,TransferredToWardCode
	,Surgeon2Code
	,Surgeon3Code
	,Anaesthetist2Code
	,Anaesthetist3Code
	,ScoutNurseCode
	,AnaesthetisNurseCode
	,InstrumentNurseCode
	,Technician1Code
	,Technician2Code
	,Unused1
	,InTheatreTime
	,CampusCode
	,ClinicalPriorityCode
	,StaffCode
	,AdditionalCaseFlag
	,UnplannedReturnCode
	,UnplannedReturnComment
	,AdmittingStaffCode
	,ReviewFlag
	,ExternalReferenceNo
	,UserFlag
	,AdmittedTime
	,ACCFlag
	,ACCNumber
	,ACCCode
	,MajorMinorCaseFlag
	,EstimatedStartTimeFixedFlag
	,PatientLanguageCode
	,LMOCode
	,ReferrerCode
	,MaritalStatusCode
	,PatientAddressStatus
	,LastUpdated
	,OldMRN
	,EstimatedArrivalTime
	,Address2
	,Address4
	,SentForTime
	,PorterLeftTime
	,PorterCode
	,CombinedCaseOrderNumberF
	,CombinedCaseConsultantCode
	,CombinedCaseTheatreCode
	,CombinedCaseSessionNo
	,CombinedCaseEstimateStartTime
	,CombinedCaseSpecialtyCode
	,CombinedCaseSurgeonCode
	,CombinedCaseAnaesthetistCode
	,CombinedCaseFlag
	,CombinedCaseFixedTimeFlag
	,PicklistPrintedFlag
	,PicklistMergedFlag
	,BookingInformationReceivedTime
	,InitiatedByStaffName
	,RequestingSurgerySpecialtyCode
	,HSCComment
	,SessionSourceUniqueID
	,CombinedCaseSessionSourceUniqueID
	,ReasonForOrderCode
	,PrintedFlag
	,PorterDelayCode
)
select
	 SequenceNo = PA_SEQN_FILLER
	,SourceUniqueID = PA_SEQU
	,DistrictNo = PA_MRN
	,IntendedProcedureDate = PA_OPER_DATE
	,IntendedTheatreCode = PA_TH_CODE
	,CustodianDetail = PA_CUSTODIAN
	,IntendedSessionTypeCode = PA_SESS_TYPE
	,Surname = PA_LAST_NAME
	,Forename = PA_FIRST_NAME
	,DateOfBirth = PA_DOB
	,PatientSourceUniqueID = PA_PIN_SEQU
	,TheatreCode = PA_TH_SEQU
	,ConsultantCode = PA_CONS_SEQU
	,ConsultantCode2 = PA_SUCODE
	,BiohazardFlag = PA_BIOHAZARD
	,BloodProductsRequiredFlag = PA_BLOOD
	,SpecialEquipmentFlag = PA_EQUIPMENT
	,PatientAgeInYears = PA_AGEYR
	,PatientAgeInMonths = PA_AGEMNTH
	,AdmissionWardCode = PA_WARD
	,SessionStartTime = PA_TIME
	,PatientEpisodeNumber = PA_EPISODE_NUM
	,Operation = PA_OPCOMM
	,PreferredOrder = PA_ORDER
	,PatientBookingStatusCode = PA_TRANSF
	,PatientStatusCode = PA_STAT
	,PatientClassificationCode2 = PA_CLCODE
	,AnaestheticCode = PA_ANA_SEQU
	,AnaestheticTypeCode = case when PA_ANAES_TYPE = '' then null else PA_ANAES_TYPE end
	,SurgeonComment = PA_SURG_COMM
	,SurgeonSpecialtyCode = PA_SURG_SPEC
	,BedNumber = PA_BED
	,WaitingListSourceUniqueID = PA_WLSEQ
	,SessionNo = PA_SESSION_NO
	,OperationDuration = PA_TOTAL_MINS
	,ChangeOverDuration = PA_CHANGE_OVER
	,SexCode = PA_GENDER
	,SurgeonCode = PA_SURGEON
	,BookingEventAdmittedTime = PA_FILLER_01
	,SurgeryRequestedTime = PA_REQ_TIME
	,ReadyForSurgeryTime = PA_TIME_READY
	,VerifiedFlag = PA_VERIFIED
	,PlannedReturnFlag = PA_UNPLANNED
	,EmergencyFlag = PA_TYPE
	,TheatreSessionSort = PA_TH_SESS_SORT
	,EstimatedStartTime = PA_EST_START
	,IntendedSurgeonCode = PA_SU_SEQU
	,PatientClassificationCode = PA_CL_SEQU
	,WardCode = PA_WA_SEQU
	,Surgeon1SpecialtyCode = PA_S1_SEQU
	,LogLastUpdated = PA_LOG_DATE
	,RecordLogDetails = PA_LOG_DETAILS_FILLER
	,WaitingDetailsSourceUniqueID = PA_WL_SEQU
	,AnaesthetistCode = PA_AN_SEQU
	,Address1 = PA_STREET
	,Address3 = PA_SUBURB
	,Postcode = PA_POSTCODE
	,HomeTelephone = PA_HOME_PHONE
	,BusinessTelephone = PA_BUS_PHONE
	,AgeInDays = PA_AGDAYS
	,AdmissionTypeCode = PA_TA_SEQU
	,ChangedSinceLastDataTransferFlag = PA_CHANGE_FLAG
	,ASAScoreCode = PA_ASA_SEQU
	,Unused4 = PA_UNUSED_4
	,Unused5 = PA_UNUSED_5
	,Unused6 = PA_UNUSED_6
	,Unused7 = PA_UNUSED_7
	,Unused8 = PA_UNUSED_8
	,PathologyRequiredFlag = PA_PATHOLOGY
	,XrayRequiredFlag = PA_XRAY
	,PathologyComment = PA_PATH_COMMENT
	,XrayComment = PA_XRAY_COMMENT
	,BloodProductsRequiredComment = PA_BLOOD_COMMENT
	,UnitsOfBloodProductRequired = PA_BLOOD_NO_OF_UNITS
	,WardAdvisedTime = PA_WARD_ADV_DATE_TIME
	,WardAdvisedTime2 = PA_FILLER_02
	,ModeOfTransportCode = PA_TRANS
	,NurseRequestingTransferCode = PA_NURSE_REQ_SEQU
	,NurseReceivingNotificationCode = PA_NURSE_REC_NOTIF
	,OxygenRequiredFlag = PA_OXYGEN
	,IVFlag = PA_IV
	,TransferredToWardCode = PA_WA_TO_SEQU
	,Surgeon2Code = PA_SU2_SEQU
	,Surgeon3Code = PA_SU3_SEQU
	,Anaesthetist2Code = PA_AN2_SEQU
	,Anaesthetist3Code = PA_AN3_SEQU
	,ScoutNurseCode = PA_SC_NUR_SEQU
	,AnaesthetisNurseCode = PA_AN_NUR_SEQU
	,InstrumentNurseCode = PA_IN_NUR_SEQU
	,Technician1Code = PA_TECH1_SEQU
	,Technician2Code = PA_TECH2_SEQU
	,Unused1 = PA_UNUSED_1
	,InTheatreTime = PA_TIME_THEATRE
	,CampusCode = PA_CA_SEQU
	,ClinicalPriorityCode = PA_CLP_SEQU
	,StaffCode = PA_BOOK_SU_SEQU
	,AdditionalCaseFlag = PA_ADDITIONAL
	,UnplannedReturnCode = PA_CAM_SEQU
	,UnplannedReturnComment = PA_CAM_COMMENT
	,AdmittingStaffCode = PA_ADMIT_SU_SEQU
	,ReviewFlag = PA_FLAG
	,ExternalReferenceNo = PA_EXT_NO_FIELD
	,UserFlag = PA_FLAG_2
	,AdmittedTime = PA_ADMIT_DATE_TIME
	,ACCFlag = PA_ACC_INDICATOR
	,ACCNumber = PA_ACC_NUMBER
	,ACCCode = PA_ACC_SEQU
	,MajorMinorCaseFlag = PA_MAJOR_MINOR
	,EstimatedStartTimeFixedFlag = PA_FIXED_TIME
	,PatientLanguageCode = PA_LA_SEQU
	,LMOCode = PA_LMO_SEQU
	,ReferrerCode = PA_REF_SEQU
	,MaritalStatusCode = PA_MS_SEQU
	,PatientAddressStatus = PA_STATE
	,LastUpdated = PA_UPD_DATE
	,OldMRN = PA_OLD_MRN
	,EstimatedArrivalTime = PA_EST_ARR_TIME
	,Address2 = PA_STREET_2
	,Address4 = PA_CITY
	,SentForTime = PA_SENT_FOR_DATE_TIME
	,PorterLeftTime = PA_PORTER_LEFT_DATE_TIME
	,PorterCode = PA_PORTER_SEQU
	,CombinedCaseOrderNumberF = PA_ORDER_CC
	,CombinedCaseConsultantCode = PA_CONS_CC_SEQU
	,CombinedCaseTheatreCode = PA_TH_CC_SEQU
	,CombinedCaseSessionNo = PA_SESSION_NO_CC
	,CombinedCaseEstimateStartTime = PA_EST_START_CC
	,CombinedCaseSpecialtyCode = PA_S1_CC_SEQU
	,CombinedCaseSurgeonCode = PA_SU_CC_SEQU
	,CombinedCaseAnaesthetistCode = PA_AN_CC_SEQU
	,CombinedCaseFlag = PA_COMBINED_FLAG
	,CombinedCaseFixedTimeFlag = PA_FIXED_TIME_CC
	,PicklistPrintedFlag = PA_PICKLIST_PRINTED
	,PicklistMergedFlag = PA_PICKLIST_MERGED
	,BookingInformationReceivedTime = PA_BOOKING_RECEIVED_DATE_TIME
	,InitiatedByStaffName = PA_INITIATED_BY_NAME
	,RequestingSurgerySpecialtyCode = PA_REQ_S1_SEQU
	,HSCComment = PA_HSC_COMMENTS
	,SessionSourceUniqueID = PA_SS_SEQU
	,CombinedCaseSessionSourceUniqueID = PA_CC_SS_SEQU
	,ReasonForOrderCode = PA_ORDER_REASON_SEQU
	,PrintedFlag = PA_PRINTED
	,PorterDelayCode = PA_PR_SEQU
from
	ORMIS.dbo.FPATS Patient

select @RowsInserted = @@rowcount


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadTheatrePatientBooking', @Stats, @StartTime

print @Stats

