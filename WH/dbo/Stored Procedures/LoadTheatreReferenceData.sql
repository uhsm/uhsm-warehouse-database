﻿CREATE procedure [dbo].[LoadTheatreReferenceData] as

/*
 Modified Date				Modified By			Modification
-----------------			------------		------------
15/07/2014					CMan				Added SessionType Table  and Delay ReasonTable as a new feeds
*/




truncate table Theatre.Theatre

insert into Theatre.Theatre
(
	 TheatreCode
	,Theatre
	,OperatingSuiteCode
	,TheatreCode1
	,InactiveFlag
)
select
	 TheatreCode = TH_SEQU
	,Theatre = TH_DESC
	,OperatingSuiteCode = TH_UN_SEQU
	,TheatreCode1 = TH_CODE
	,InactiveFlag = TH_INACTIVE
from
	ORMIS.dbo.FTHEAT Theatre


truncate table Theatre.OperatingSuite

insert into Theatre.OperatingSuite
(
	 OperatingSuiteCode
	,OperatingSuite
	,OperatingSuiteCode1
	,InactiveFlag
)
select
	 OperatingSuiteCode = UN_SEQU
	,OperatingSuite = UN_DESC
	,OperatingSuiteCode1 = UN_CODE
	,InactiveFlag = UN_INACTIVE
from
	ORMIS.dbo.FUNIT OperatingSuite



truncate table Theatre.Staff

insert into Theatre.Staff
(
	 StaffCode
	,Surname
	,Forename
	,Initial
	,StaffCode1
	,StaffCategoryCode
	,SpecialtyCode
)
select
	 StaffCode = SU_SEQU
	,Surname = SU_LNAME
	,Forename = SU_FNAME
	,Initial = SU_INITIAL
	,StaffCode1 = SU_CODE
	,StaffCategoryCode = SU_SC_SEQU
	,SpecialtyCode = SU_S1_SEQU
from
	ORMIS.dbo.FSURGN Staff



truncate table Theatre.StaffCategory

insert into Theatre.StaffCategory
(
	 StaffCategoryCode
	,StaffCategoryCode1
	,StaffCategory
	,SurgeonFlag
	,InactiveFlag
	,ConsultantFlag
	,SupervisionFlag
	,AnaesthetistFlag
	,OtherFlag
	,NurseFlag
	,PorterFlag
	,Technician1Flag
	,Technician2Flag
	,ClerkFlag
)
select
	 StaffCategoryCode = SC_SEQU
	,StaffCategoryCode1 = SC_CODE
	,StaffCategory = SC_DESC
	,SurgeonFlag = SC_SURGEON
	,InactiveFlag = SC_INACTIVE
	,ConsultantFlag = SC_IS_CONSULT
	,SupervisionFlag = SC_SUPERVISION
	,AnaesthetistFlag = SC_ANAESTHETIST
	,OtherFlag = SC_OTHER
	,NurseFlag = SC_NURSE
	,PorterFlag = SC_PORTER
	,Technician1Flag = SC_TECHNICIAN_1
	,Technician2Flag = SC_TECHNICIAN_2
	,ClerkFlag = SC_CLERK
from
	ORMIS.dbo.FSCAT StaffCategory



truncate table Theatre.Specialty

insert into Theatre.Specialty
(
	 SpecialtyCode
	,Specialty
	,SpecialtyCode1
	,InactiveFlag
)
select
	 SpecialtyCode = S1_SEQU
	,Specialty = S1_DESC
	,SpecialtyCode1 = S1_CODE
	,InactiveFlag = S1_INACTIVE
from
	ORMIS.dbo.FS1SPEC Specialty



truncate table Theatre.AnaestheticType

insert into Theatre.AnaestheticType
(
	 AnaestheticTypeCode
	,AnaestheticType
	,AnaestheticTypeCode1
	,InactiveFlag
)
select
	 AnaestheticTypeCode = ANA_SEQU
	,AnaestheticType = ANA_DESCRIPTION
	,AnaestheticTypeCode1 = ANA_CODE
	,InactiveFlag = ANA_INACTIVE
from
	ORMIS.dbo.F_Anaesthetic AnaestheticType



truncate table Theatre.AdmissionType

insert into Theatre.AdmissionType
(
	 AdmissionTypeCode
	,AdmissionType
	,AdmissionTypeCode1
	,InactiveFlag
)
select
       AdmissionTypeCode = TA_SEQU
      ,AdmissionType = TA_DESCRIPTION
      ,AdmissionTypeCode1 = TA_CODE
      ,InactiveFlag = TA_INACTIVE
from
	ORMIS.dbo.F_Type_of_Admission AdmissionType


truncate table Theatre.ASAScore

insert into Theatre.ASAScore
(
	 ASAScoreCode
	,ASAScore
	,ASAScoreCode1
	,InactiveFlag
)
select
       ASAScoreCode = ASA_SEQU
      ,ASAScore = ASA_DESCRIPTION
      ,ASAScoreCode1 = ASA_CODE
      ,InactiveFlag = ASA_INACTIVE
from
	ORMIS.dbo.F_Asa ASAScore


truncate table Theatre.Ward

insert into Theatre.Ward
(
	 WardCode
	,Ward
	,WardCode1
	,InactiveFlag
)
select
	 WardCode = WA_SEQU
	,Ward = WA_DESC
	,WardCode1 = WA_CODE
	,InactiveFlag = WA_INACTIVE
from
	ORMIS.dbo.FWARD Ward



truncate table Theatre.Operation

insert into Theatre.Operation
(
	 OperationCode
	,Operation
	,OperationCode1
	,InactiveFlag
)
select
	 OperationCode = CD_SEQU
	,Operation = CD_DESCRIPTION
	,OperationCode1 = CD_CODE
	,InactiveFlag = CD_INACTIVE
from
	ORMIS.dbo.FCDOPER Operation


truncate table Theatre.CancelReason

insert into Theatre.CancelReason
(
	 CancelReasonCode
	,CancelReason
	,CancelReasonCode1
	,CancelReasonGroupCode
	,InactiveFlag
)
select
	 CancelReasonCode = CN_SEQU
	,CancelReason = CN_DESC
	,CancelReasonCode1 = CN_CODE
	,CancelReasonGroupCode = CN_CG_SEQU
	,InactiveFlag = CN_INACTIVE
from
	ORMIS.dbo.FCANCEL CancelReason



truncate table Theatre.CancelReasonGroup

insert into Theatre.CancelReasonGroup
(
	 CancelReasonGroupCode
	,CancelReasonGroup
	,CancelReasonGroupCode1
	,InactiveFlag
)
select
	 CancelReasonGroupCode = CG_SEQU
	,CancelReasonGroup = CG_DESC
	,CancelReasonGroupCode1 = CG_CODE
	,InactiveFlag = CG_INACTIVE
from
	ORMIS.dbo.F_Cancel_Group CancelReasonGroup




Truncate Table Theatre.SessionType
Insert Into Theatre.SessionType

SELECT SessionTypeCode = [SET_CODE]
      ,SessionTypeDescription = [SET_DESC]
      ,InactiveFlag = [SET_INACTIVE]
FROM [ORMIS].[dbo].[A_Session_Type]


Truncate table  Theatre.DelayReason
Insert into Theatre.DelayReason 
Select DelayReasonCode = PR_SEQU
		,DelayReasonDescription = PR_DESC
		,DelayReasonCode1 = PR_CODE
from ORMIS.dbo.FPROCDY
