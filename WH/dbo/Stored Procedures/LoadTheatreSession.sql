﻿CREATE procedure [dbo].[LoadTheatreSession]
as


declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0



truncate table Theatre.Session


insert into Theatre.Session
(
	 SourceUniqueID
	,ActualSessionMinutes
	,PlannedSessionMinutes
	,StartTime
	,EndTime
	,SessionNumber
	,TheatreCode
	,ConsultantCode
	,SpecialtyCode
	,SurgeonCode1
	,SurgeonCode2
	,AnaesthetistCode1
	,AnaesthetistCode2
	,AnaesthetistCode3
	,ScoutNurseCode
	,InstrumentNurseCode
	,AnaestheticNurseCode
	,LastUpdated
	,TimetableDetailCode
	,Comment
	,SessionOrder
	,CancelledFlag
	,CancelledMinutes
	,OverrunReason
	,OverrunReasonDate
	,OverrunReasonStaffCode
	,FirstNonCancelledOPID
	,SessionType
	,OriginalSessionConsultantCode--CM 09/12/2014 added additional column to table to be populated with the original session consultant code
	
)
select
	 SourceUniqueID = Session.SS_SEQU
	,ActualSessionMinutes = Session.SS_TOT_ACTUAL
	,PlannedSessionMinutes = Session.SS_TOT_AVAIL
	,StartTime = Session.SS_START_DATE_TIME
	,EndTime = Session.SS_FINISH_DATE_TIME
	,SessionNumber = Session.SS_SESSION
	,TheatreCode = Session.SS_THEATRE
	,ConsultantCode = Session.SS_CONS_SEQU --CM 09/12/2014 :Changed it to pull from SS_CONS_SEQU instead of SS_Consultant as in ORMIS Sessions are set up with a consultant recorded. It is then possible for the Theatre listing staff to change the consultant to a different clinician for an adhoc session.  This is what this field now shows. 
	,SpecialtyCode = Session.SS_S1_SEQU
	,SurgeonCode1 = Session.SS_SURG1_SEQU
	,SurgeonCode2 = Session.SS_SURG2_SEQU
	,AnaesthetistCode1 = Session.SS_ANAES1_SEQU
	,AnaesthetistCode2 = Session.SS_ANAES2_SEQU
	,AnaesthetistCode3 = Session.SS_ANAES3_SEQU
	,ScoutNurseCode = Session.SS_SCOUT_SEQU
	,InstrumentNurseCode = Session.SS_INST_SEQU
	,AnaestheticNurseCode = Session.SS_ANAESN_SEQU
	,LastUpdated = Session.SS_LOG_DATE
	,TimetableDetailCode = Session.SS_TI_SEQU
	,Comment = Session.SS_COMMENTS
	,SessionOrder = Session.SS_ORDER

	,CancelledFlag =
		case
		when
			not exists
			(
			select
				1
			from
				ORMIS.dbo.FOPERAT OperationDetail
			where
				OperationDetail.OP_SS_SEQU = Session.SS_SEQU
			)
		then 1
		else 0
		end

	,CancelledMinutes = Session.SS_CANCELLED
	,OverrunReason = Session.SS_REASON
	,OverrunReasonDate = Session.SS_REASON_DATE_TIME
	,OverrunReasonStaffCode = Session.SS_REASON_STAFF_SEQU
	,FirstNonCancelledOPID = tminop.minop
	,SessionType = Session.SS_SET_CODE
	,OriginalSessionConsultantCode = Session.SS_CONSULTANT--CM 09/12/2014 This is the original consultant listed for the session

from
	ORMIS.dbo.F_SessionSumm Session
left join

(select MIN(ODetail.OP_SEQU) as minop,ODetail.OP_SS_SEQU
from ORMIS.dbo.FOPERAT ODetail
--where OP_CANCELLED<>1
group by ODetail.OP_SS_SEQU) as tminop
on tminop.OP_SS_SEQU=Session.SS_SEQU

--inner join ORMIS.dbo.FTHEAT Theatre
--on	Theatre.TH_SEQU = Session.SS_THEATRE
--
--left join ORMIS.dbo.FSURGN Consultant
--on	Consultant.SU_SEQU = Session.SS_CONS_SEQU
--
--left join ORMIS.dbo.FS1SPEC Specialty
--on	Specialty.S1_SEQU = Session.SS_S1_SEQU
--
--left join ORMIS.dbo.FSURGN Surgeon1
--on	Surgeon1.SU_SEQU = Session.SS_SURG1_SEQU
--
--left join ORMIS.dbo.FSURGN Surgeon2
--on	Surgeon2.SU_SEQU = Session.SS_SURG2_SEQU
--
--left join ORMIS.dbo.FSURGN Anaesthetist1
--on	Anaesthetist1.SU_SEQU = Session.SS_ANAES1_SEQU
--
--left join ORMIS.dbo.FSURGN Anaesthetist2
--on	Anaesthetist2.SU_SEQU = Session.SS_ANAES2_SEQU
--
--left join ORMIS.dbo.FSURGN Anaesthetist3
--on	Anaesthetist3.SU_SEQU = Session.SS_ANAES3_SEQU
--
--left join ORMIS.dbo.FSURGN ScoutNurse
--on	ScoutNurse.SU_SEQU = Session.SS_SCOUT_SEQU
--
--left join ORMIS.dbo.FSURGN InstrumentNurse
--on	InstrumentNurse.SU_SEQU = Session.SS_INST_SEQU

--where
----	Specialty.S1_SEQU is null
--	SS_TOT_AVAIL <> SS_CANCELLED
--and	SS_CANCELLED <> 0


select @RowsInserted = @@rowcount


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadTheatreSession', @Stats, @StartTime

print @Stats

