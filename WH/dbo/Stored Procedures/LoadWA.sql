﻿CREATE       procedure [dbo].[LoadWA] 
	 @from smalldatetime = null
	,@to smalldatetime = null
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

select @to = coalesce(@to, dateadd(day, datediff(day, 0, getdate()), 0))
select @from = 
	coalesce(
		 @from
		,(select dateadd(day, 1, DateValue) from dbo.Parameter where Parameter = 'WAENCOUNTERFREEZEDATE')
		,dateadd(month, -2, @to)
	)

truncate table TImportWAEncounter
exec ExtractInquireWA @from, @to
exec LoadWAEncounter

truncate table TImportWAOperation
exec ExtractInquireWAOperation @from, @to
exec LoadWAOperation


update dbo.Parameter
set
	DateValue = getdate()
where
	Parameter = 'LOADWADATE'

if @@rowcount = 0

insert into dbo.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'LOADWADATE'
	,DateValue = getdate()


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'LoadWA', @Stats, @StartTime
