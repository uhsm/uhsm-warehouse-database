﻿create       procedure [dbo].[LoadWaitingListActivity] 
	 @from smalldatetime = null
	,@to smalldatetime = null
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()


select @to = coalesce(@to, dateadd(day, datediff(day, 0, getdate()), 0))
select @from = 
	coalesce(
		 @from
		,(select dateadd(day, 1, DateValue) from dbo.Parameter where Parameter = 'WLACTIVITYFREEZEDATE')
		,dateadd(month, -1, @to)
	)

truncate table TImportWaitingListActivity
exec ExtractInquireWaitingListActivity @from, @to
exec LoadWaitingListActivityEncounter

update dbo.Parameter
set
	DateValue = getdate()
where
	Parameter = 'LOADWLACTIVITYDATE'

if @@rowcount = 0

insert into dbo.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'LOADWLACTIVITYDATE'
	,DateValue = getdate()


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())
select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

EXEC WriteAuditLogEvent 'LoadWaitingListActivity', @Stats, @StartTime
