﻿CREATE procedure [dbo].[LoadWaitingListActivityEncounter] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

select @from = (select min(convert(smalldatetime, ActivityDate)) from TLoadWaitingListActivity)
select @to = (select max(convert(smalldatetime, ActivityDate)) from TLoadWaitingListActivity)

delete from APC.WaitingListActivity
where
	ActivityDate between @from and @to
or	exists
	(
	select
		1
	from
		TLoadWaitingListActivity
	where
		TLoadWaitingListActivity.SourceUniqueID = WaitingListActivity.SourceUniqueID
	)

select @RowsDeleted = @@rowcount

insert into APC.WaitingListActivity
(
	 SourceUniqueID
	,ActivityTypeCode
	,ActivityDate
	,ActivityTime
	,AdminCategoryCode
	,BookingTypeCode
	,CancelledBy
	,ConsultantCode
	,DeferralEndDate
	,DeferralRevisedEndDate
	,DiagnosticGroupCode
	,SourceEntityRecno
	,SiteCode
	,SourcePatientNo
	,LastRevisionTime
	,LastRevisionUser
	,PreviousActivityId
	,Reason
	,Remark
	,RemovalComment
	,RemovalReasonCode
	,SpecialtyCode
	,SuspensionReasonCode
	,TCIAcceptDate
	,TCITime
	,TCIOfferDate
	,PriorityCode
	,WaitingListCode
	,WardCode
	,CharterCancelCode
	,CharterCancelDeferFlag
	,CharterCancel
	,OpCancelledFlag
	,OpCancelled
	,PatientChoiceFlag
	,PatientChoice
	,WLSuspensionInitiatorCode
	,WLSuspensionInitiator
	,Created
	,Updated
	,ByWhom
)
select
	 SourceUniqueID
	,ActivityTypeCode
	,ActivityDate
	,ActivityTime
	,AdminCategoryCode
	,BookingTypeCode
	,CancelledBy
	,ConsultantCode
	,DeferralEndDate
	,DeferralRevisedEndDate
	,DiagnosticGroupCode
	,SourceEntityRecno
	,SiteCode
	,SourcePatientNo
	,LastRevisionTime
	,LastRevisionUser
	,PreviousActivityId
	,Reason
	,Remark
	,RemovalComment
	,RemovalReasonCode
	,SpecialtyCode
	,SuspensionReasonCode
	,TCIAcceptDate
	,TCITime
	,TCIOfferDate
	,PriorityCode
	,WaitingListCode
	,WardCode
	,CharterCancelCode
	,CharterCancelDeferFlag
	,CharterCancel
	,OpCancelledFlag
	,OpCancelled
	,PatientChoiceFlag
	,PatientChoice
	,WLSuspensionInitiatorCode
	,WLSuspensionInitiator
	,Created = getdate()
	,Updated = null
	,ByWhom = system_user
from
	dbo.TLoadWaitingListActivity

select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', ' + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'LoadWaitingListActivityEncounter', @Stats, @StartTime
