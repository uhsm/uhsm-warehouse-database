﻿
CREATE PROCEDURE dbo.MISYSSpecialty

as
BEGIN
	
truncate table MISYS.Specialty

insert into MISYS.Specialty

(OrderNumber, 
SpecialtyName,
MonthOrdered,
FiscalYear)

select distinct baseorder.OrderNumber,SpecialtyName=
((CASE WHEN MISYS.lkSpecTrue.SpecialtyTrue IS NULL AND (SUBSTRING(OrderingPhysician.Staff, PATINDEX('%([a-z]%)', 
                      OrderingPhysician.Staff) + 1, LEN(OrderingPhysician.Staff) - CASE patindex('%([a-z]%)', OrderingPhysician.Staff) 
                      WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', OrderingPhysician.Staff) + 1 END)) LIKE ('GP%') THEN ('GP') 
                      WHEN MISYS.lkSpecTrue.SpecialtyTrue IS NULL AND (SUBSTRING(OrderingPhysician.Staff, PATINDEX('%([a-z]%)', OrderingPhysician.Staff) + 1, 
                      LEN(OrderingPhysician.Staff) - CASE patindex('%([a-z]%)', OrderingPhysician.Staff) WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', OrderingPhysician.Staff) 
                      + 1 END)) NOT LIKE ('GP%') THEN ('Unknown') ELSE MISYS.lkSpecTrue.SpecialtyTrue END)),

  (CASE WHEN CAST(MONTH(BASEOrder.OrderDate) AS INT) = 1 THEN 13 WHEN CAST(MONTH(BaseOrder.OrderDate) AS INT) 
                      = 2 THEN 14 WHEN CAST(MONTH(BaseOrder.OrderDate) AS INT) = 3 THEN 15 ELSE CAST(MONTH(BaseOrder.OrderDate) AS INT) END),

 (CASE WHEN month(BaseOrder.OrderDate) BETWEEN 1 AND 
                      3 THEN YEAR(BaseOrder.OrderDate) ELSE year(BaseOrder.OrderDate) + 1 END)
from WHOLAP.dbo.BaseOrder
INNER JOIN
                      WHOLAP.dbo.OlapMISYSStaff AS OrderingPhysician WITH (nolock) ON OrderingPhysician.StaffCode = baseOrder.OrderingPhysicianCode
LEFT OUTER JOIN
                      MISYS.lkSpecTrue ON MISYS.lkSpecTrue.SpecialtyOrig = SUBSTRING(OrderingPhysician.Staff, PATINDEX('%([a-z]%)', OrderingPhysician.Staff) + 1, 
                      LEN(OrderingPhysician.Staff) - CASE patindex('%([a-z]%)', OrderingPhysician.Staff) WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', OrderingPhysician.Staff) 
                      + 1 END)
                      
                      
                      
                      
          WHERE BaseOrder.OrderDate>'2009-03-31'

END

