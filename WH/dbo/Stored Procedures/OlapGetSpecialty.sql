﻿CREATE procedure [dbo].[OlapGetSpecialty]
	@type varchar(10) = null
as

--simple select until OLAP database is in place

if @type = 'OPWL'
	select
		 SpecialtyCode
		,Specialty
		,SpecialtyUniqueName
	from
		(
		select
			 SpecialtyCode = null
			,Specialty = 'All'
			,SpecialtyUniqueName = null

		union all

		select
			 SpecialtyCode
			,Specialty = coalesce(NationalSpecialtyCode + ' - ', '') + Specialty
			,SpecialtyUniqueName = SpecialtyCode
		from
			PAS.Specialty
		where
			exists
			(
			select
				1
			from
				OP.WaitingList
			where
				WaitingList.SpecialtyCode = Specialty.SpecialtyCode
			)
		) Specialty
	order by
		SpecialtyCode

if @type = 'APCWL'
	select
		 SpecialtyCode
		,Specialty
		,SpecialtyUniqueName
	from
		(
		select
			 SpecialtyCode = null
			,Specialty = 'All'
			,SpecialtyUniqueName = null

		union all

		select
			 SpecialtyCode
			,Specialty = coalesce(NationalSpecialtyCode + ' - ', '') + Specialty
			,SpecialtyUniqueName = SpecialtyCode
		from
			PAS.Specialty
		where
			exists
			(
			select
				1
			from
				APC.WaitingList
			where
				WaitingList.SpecialtyCode = Specialty.SpecialtyCode
			)
		) Specialty
	order by
		SpecialtyCode

else
	select
		 SpecialtyCode
		,Specialty = coalesce(NationalSpecialtyCode + ' - ', '') + Specialty
		,SpecialtyUniqueName = SpecialtyCode
	from
		PAS.Specialty
	order by
		Specialty

--declare @openquery as varchar(4000)
--declare @select as varchar(4000)
--
--select
--	@openquery =
--		'
--		with 
--
--		member [Measures].[Name] as 
--			[PAS Specialty].[Specialty].currentmember.name 
--
--		member [Measures].[Unique Name] as 
--			[PAS Specialty].[Specialty].currentmember.uniquename 
--
--		member [Measures].[Unique Key] as 
--			[PAS Specialty].[Specialty].currentmember.properties("KEY") 
--
--		set [Measures] as 
--			{
--			 [Measures].[Name]
--			,[Measures].[Unique Name]
--			,[Measures].[Unique Key]
--			} 
--
--		set [Set] as 
--			order(
--				filter(
--					 [PAS Specialty].[Specialty].members
--					,[Measures].[APC Waiters] > 0
--				)
--				,[PAS Specialty].[Specialty].currentmember.name
--				,asc
--			) 
--
--		select 
--			 [Measures] on 0
--			,[Set] on 1 
--		 from 
--			[Warehouse]
--		where
--			(
--			[Census].[Census].members
--			)
--		'
--
--select
--	@select =
--		'
--		select
--			 SpecialtyCode = case when "[Measures].[Unique Key]" = ''0'' then null else "[Measures].[Unique Key]" end
--			,Specialty = "[Measures].[Name]"
--			,SpecialtyUniqueName = "[Measures].[Unique Name]"
--		from
--			openquery(Warehouse, ''
--		'
--
--exec (@select + @openquery + ''')')
