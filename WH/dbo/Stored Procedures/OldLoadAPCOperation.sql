﻿CREATE PROCEDURE [dbo].[OldLoadAPCOperation]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


--delete for all related FCEs
delete from APC.Operation
where
	exists
	(
	select
		1
	from
		dbo.TLoadAPCOperation
	where
		TLoadAPCOperation.SourcePatientNo = APC.Operation.SourcePatientNo
	and	TLoadAPCOperation.SourceSpellNo = APC.Operation.SourceSpellNo
	and	TLoadAPCOperation.SourceEncounterNo = APC.Operation.SourceEncounterNo
	)


SELECT @RowsDeleted = @@Rowcount


INSERT INTO APC.Operation
	(
	 SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,ProviderSpellNo
	,SourceEncounterNo
	,SequenceNo
	,OperationCode
	,OperationDate
	,ConsultantCode
	,APCSourceUniqueID
	,Created
	,ByWhom
	) 
select
	 SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,ProviderSpellNo
	,SourceEncounterNo
	,SequenceNo
	,OperationCode
	,OperationDate
	,ConsultantCode
	,APCSourceUniqueID

	,Created = getdate()
	,ByWhom = system_user
from
	dbo.TLoadAPCOperation TEncounter
where
	not exists
	(
	select
		1
	from
		APC.Operation
	where
		Operation.SourceUniqueID = TEncounter.SourceUniqueID
	)

SELECT @RowsInserted = @@Rowcount


--update appropriate columns in APC.Encounter
update
	APC.Encounter
set
	 SecondaryOperationCode1 = Operation1.OperationCode
	,SecondaryOperationCode2 = Operation2.OperationCode
	,SecondaryOperationCode3 = Operation3.OperationCode
	,SecondaryOperationCode4 = Operation4.OperationCode
	,SecondaryOperationCode5 = Operation5.OperationCode
	,SecondaryOperationCode6 = Operation6.OperationCode
	,SecondaryOperationCode7 = Operation7.OperationCode
	,SecondaryOperationCode8 = Operation8.OperationCode
	,SecondaryOperationCode9 = Operation9.OperationCode
	,SecondaryOperationCode10 = Operation10.OperationCode
	,SecondaryOperationCode11 = Operation11.OperationCode

	,SecondaryOperationDate1 = Operation1.OperationDate
	,SecondaryOperationDate2 = Operation2.OperationDate
	,SecondaryOperationDate3 = Operation3.OperationDate
	,SecondaryOperationDate4 = Operation4.OperationDate
	,SecondaryOperationDate5 = Operation5.OperationDate
	,SecondaryOperationDate6 = Operation6.OperationDate
	,SecondaryOperationDate7 = Operation7.OperationDate
	,SecondaryOperationDate8 = Operation8.OperationDate
	,SecondaryOperationDate9 = Operation9.OperationDate
	,SecondaryOperationDate10 = Operation10.OperationDate
	,SecondaryOperationDate11 = Operation11.OperationDate
from
	APC.Encounter

inner join APC.Operation Operation1
on	Operation1.ProviderSpellNo = Encounter.ProviderSpellNo
and	Operation1.SourceEncounterNo = Encounter.SourceEncounterNo
and	Operation1.SequenceNo = 1

left join APC.Operation Operation2
on	Operation2.ProviderSpellNo = Encounter.ProviderSpellNo
and	Operation2.SourceEncounterNo = Encounter.SourceEncounterNo
and	Operation2.SequenceNo = 2

left join APC.Operation Operation3
on	Operation3.ProviderSpellNo = Encounter.ProviderSpellNo
and	Operation3.SourceEncounterNo = Encounter.SourceEncounterNo
and	Operation3.SequenceNo = 3

left join APC.Operation Operation4
on	Operation4.ProviderSpellNo = Encounter.ProviderSpellNo
and	Operation4.SourceEncounterNo = Encounter.SourceEncounterNo
and	Operation4.SequenceNo = 4

left join APC.Operation Operation5
on	Operation5.ProviderSpellNo = Encounter.ProviderSpellNo
and	Operation5.SourceEncounterNo = Encounter.SourceEncounterNo
and	Operation5.SequenceNo = 5

left join APC.Operation Operation6
on	Operation6.ProviderSpellNo = Encounter.ProviderSpellNo
and	Operation6.SourceEncounterNo = Encounter.SourceEncounterNo
and	Operation6.SequenceNo = 6

left join APC.Operation Operation7
on	Operation7.ProviderSpellNo = Encounter.ProviderSpellNo
and	Operation7.SourceEncounterNo = Encounter.SourceEncounterNo
and	Operation7.SequenceNo = 7

left join APC.Operation Operation8
on	Operation8.ProviderSpellNo = Encounter.ProviderSpellNo
and	Operation8.SourceEncounterNo = Encounter.SourceEncounterNo
and	Operation8.SequenceNo = 8

left join APC.Operation Operation9
on	Operation9.ProviderSpellNo = Encounter.ProviderSpellNo
and	Operation9.SourceEncounterNo = Encounter.SourceEncounterNo
and	Operation9.SequenceNo = 9

left join APC.Operation Operation10
on	Operation10.ProviderSpellNo = Encounter.ProviderSpellNo
and	Operation10.SourceEncounterNo = Encounter.SourceEncounterNo
and	Operation10.SequenceNo = 10

left join APC.Operation Operation11
on	Operation11.ProviderSpellNo = Encounter.ProviderSpellNo
and	Operation11.SourceEncounterNo = Encounter.SourceEncounterNo
and	Operation11.SequenceNo = 11



SELECT @Elapsed = DATEDIFF(minute, @StartTime,getdate())

SELECT @Stats = 'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadAPCOperation', @Stats, @StartTime

print @Stats
