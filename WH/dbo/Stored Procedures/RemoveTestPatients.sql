﻿CREATE procedure [dbo].[RemoveTestPatients] as

/**
KO. 07/05/2014
Remove test patients identfied n Lorenzo.

Note that other datasets built directly from Lorenzo source data will also need
Test patients excluding - handled in build
PTL.OPWL
PTL.IPWL
PTL.ActiveWaitingList
PTL.WaitingListPartialBooking
**/

------------------------------------------------------------------------------------
--OP
------------------------------------------------------------------------------------

--select 
--	SourceUniqueID
--	,PatientForename
--	,PatientSurname
--	,NHSNumber
--	,DistrictNo
--	,ClinicCode
--	,AppointmentTime
--	,PASCreated
--	,PASUpdated
delete
from OP.Encounter 
where SourcePatientNo in (select SourcePatientNo from Lorenzo.dbo.ExcludedPatient)


--select distinct
--	SourceUniqueID
--	,PatientForename
--	,PatientSurname
--	,NHSNumber
--	,DistrictNo
--	,DateOnWaitingList
--	,WaitingListClinicCode
--	,PASCreated
--	,PASUpdated
--	,ArchiveFlag
delete
from OP.WaitingList 
where SourcePatientNo in (select SourcePatientNo from Lorenzo.dbo.ExcludedPatient)

------------------------------------------------------------------------------------
--APC
------------------------------------------------------------------------------------
--select 
--	SourceUniqueID
--	,PatientForename
--	,PatientSurname
--	,NHSNumber
--	,DistrictNo
--	,AdmissionTime
--	,PASCreated
--	,PASUpdated
delete
from APC.Encounter 
where SourcePatientNo in (select SourcePatientNo from Lorenzo.dbo.ExcludedPatient)

--select --distinct
--	SourceUniqueID
--	,PatientForename
--	,PatientSurname
--	,NHSNumber
--	,DistrictNo
--	,DateOnWaitingList
--	,WLStatus
--	,PASCreated
--	,PASUpdated
delete
from APC.WaitingList 
where SourcePatientNo in (select SourcePatientNo from Lorenzo.dbo.ExcludedPatient)
	

------------------------------------------------------------------------------------
--RF
------------------------------------------------------------------------------------

--select 
--	SourceUniqueID
--	,PatientForename
--	,PatientSurname
--	,NHSNumber
--	,DistrictNo
--	,ReferralDate
--	,DischargeDate
--	,CancellationDate
--	,PASCreated
--	,PASUpdated
delete
from RF.Encounter 
where SourcePatientNo in (select SourcePatientNo from Lorenzo.dbo.ExcludedPatient)