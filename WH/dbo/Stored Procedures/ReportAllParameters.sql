﻿

CREATE procedure [dbo].[ReportAllParameters] as (
/*
--Author: K Oakden
--Date created: 23/02/2015
--Return data load completion times for report
*/

select 1 as [Order], 'Lorenzo' as Dataset, DateValue as DateCompleted from Lorenzo.dbo.Parameter where Parameter = 'DATALOADDATE'
union
select 2, 'Cascade', DateValue from dbo.Parameter where Parameter = 'LOADAEDATE'
union
select 3, 'WH (Incl. OPWL and IPWL)', DateValue from dbo.Parameter where Parameter = 'BUILDWHDATE'
union
select 4, 'WHOLAP', DateValue from dbo.Parameter where Parameter = 'BUILDMODELDATE'
union
select 5, 'WHREPORTING', DateValue from dbo.Parameter where Parameter = 'BUILDWHREPORTINGDATE'
union
select 6, 'SSAS CUBE', DateValue from dbo.Parameter where Parameter = 'WAREHOUSECUBEPROCESSEDDATE'
union
select 7, 'RTT Daily', DateValue from RTT_DW.dbo.Parameter where Parameter = 'RTTDWBUILDCOMPLETE'
union
select 8, 'RTT Weekly', DateValue from RTT_DW.dbo.Parameter where Parameter = 'RTTDWWEEKLYBUILDCOMPLETE'
union
select 9, 'RTT Monthly', DateValue from RTT_DW.dbo.Parameter where Parameter = 'RTTDWMONTHLYBUILDCOMPLETE'
union
select 10, 'Community PAS', DateValue from dbo.Parameter where Parameter = 'LOADCOMMUNITYPASDATE'

)








