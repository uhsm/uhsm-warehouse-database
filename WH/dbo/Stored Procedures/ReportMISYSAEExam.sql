﻿CREATE procedure [dbo].[ReportMISYSAEExam]
  @DateFrom datetime
     ,@DateTo datetime
 ,@Modality varchar (100)
  
   
as  
select distinct 
	
		MISYSOrder.ExamCode1
		,MISYSOrder.ExamDescription

		
	from
		misys.[Order] MISYSOrder with (nolock)
		
	where MISYSOrder.OrderDate between @DateFrom and @DateTo
and MISYSOrder.CaseTypeCode IN (@Modality)
	order by MISYSOrder.ExamDescription
