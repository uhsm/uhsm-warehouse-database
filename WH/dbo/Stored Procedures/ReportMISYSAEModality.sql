﻿CREATE procedure [dbo].[ReportMISYSAEModality]
  @DateFrom datetime
     ,@DateTo datetime
 ,@Consultant varchar(50)
  
   
as  
select distinct 
		--MISYSOrder.CaseTypeCode as ModalityCode
		--,MISYSOrder.CaseType as Modality
		--,
		--MISYSOrder.ExamCode1
		--,MISYSOrder.ExamDescription
		--,
		MISYSOrder.CaseTypeCode as ModalityCode
		,Modality.Modality as Modality
		
		
	from
		misys.[order] MISYSOrder with (nolock)
		iNNER JOIN
                      WHOLAP.dbo.OlapMISYSModality AS Modality WITH (nolock) ON Modality.ModalityCode = MISYSOrder.CaseTypeCode

 

	where MISYSOrder.OrderDate between @DateFrom and @DateTo
	and OrderingPhysicianCode in (@Consultant)
	
