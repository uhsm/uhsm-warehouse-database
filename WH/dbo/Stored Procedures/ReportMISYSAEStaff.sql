﻿CREATE procedure [dbo].[ReportMISYSAEStaff]
  @DateFrom datetime
     ,@DateTo datetime
  
as  
select distinct 

		r.OrderingPhysicianCode
		,OrderingPhysician.Staff as OrderingPhysicianName
		

from 
MISYS.[Order] r

inner JOIN wholap.dbo.BaseAE a on a.NHSNumber=(rtrim(left(r.SocialSecurityNo,4))+
rtrim(SUBSTRING(r.SocialSecurityNo,5,4))+
SUBSTRING(r.SocialSecurityNo,9,4))

and r.ordertime between a.arrivaltime and dateadd(hour,4,a.ArrivalTime)
INNER JOIN Calendar c on c.TheDate=a.ArrivalDate
 INNER JOIN
                WHOLAP.dbo.OlapMISYSStaff AS OrderingPhysician WITH (nolock) ON OrderingPhysician.StaffCode = r.OrderingPhysicianCode

where r.OrderDate between @DateFrom and @DateTo and r.OrderingLocationCode in ('A+E','AE','ED')
	order by OrderingPhysician.Staff

