﻿CREATE procedure [dbo].[ReportMISYSAETurnaround]

    @DateFrom datetime
     ,@DateTo datetime
   ,@Modality varchar (100)
    --,@Consultant varchar (100)
    --,@Exam varchar (25)


as


select 
COUNT(r.OrderNumber) as CountOfOrders,
r.OrderDate
,DAY(r.OrderDate)
,r.CaseTypeCode
,r.CaseType
,r.OrderingPhysicianCode
,r.OrderingPhysician
,Case when datediff(hour,r.OrderTime,r.PerformanceTime)<1 then '<1' 
when datediff(hour,r.OrderTime,r.PerformanceTime)>=1 then '>=1' end as HoursTurnaround
,r.ExamDescription 
,a.ArrivalTime
,TimeToOrder=DATEDIFF(MINUTE,a.ArrivalTime,R.OrderTime)
,TimeToScan=DATEDIFF(MINUTE,r.OrderTime,PerformanceTime)

--dictation date does not have time element recorded currently so this is not a true turnaround
,TimeToReport=DATEDIFF(MINUTE,r.PerformanceTime,r.DictationDate)
,TotalTurnaroundTime=DATEDIFF(MINUTE,a.arrivaltime,r.dictationdate)
,r.dictationdate
,c.[DayOfWeek]
,c.FinancialYear
,c.TheMonth
,c.DayOfWeekKey
,r.examcode1
,t.HourBand

/*,

(select distinct icedata.reporttime from 

(select reporttime,ice.ReportDate,
((select distinct icep.nhsnumber from whreporting.ICE.Patient icep where icep.PatientRecno=ice.MasterPatientCode)+cast(ice.ReportDate as varchar)) as identifier

 from whreporting.ICE.FactReport ice) as icedata
 where icedata.identifier=
 
 
 (
 
 
 rtrim(left(r.socialsecurityno,4))+rtrim(SUBSTRING(r.socialsecurityno,5,4))+rtrim(substring(r.socialsecurityno,9,5))
 
 
 
 +cast(r.orderdate as varchar)))


as Dictation_Time*/

from 
MISYS.[Order] r

inner JOIN wholap.dbo.BaseAE a on a.NHSNumber=(rtrim(left(r.SocialSecurityNo,4))+
rtrim(SUBSTRING(r.SocialSecurityNo,5,4))+
SUBSTRING(r.SocialSecurityNo,9,4))

and r.ordertime between a.arrivaltime and dateadd(hour,4,a.ArrivalTime)
INNER JOIN Calendar c on c.TheDate=a.ArrivalDate


inner join WHOLAP.dbo.OlapTimeBand t on t.TimeBandCode= left((convert(varchar(10), OrderTime, 108)),5)




where 
a.ArrivalDate between @DateFrom and @DateTo and
r.OrderingLocationCode in ('A+E','AE','ED')

--Assumes that exam will be performed within 4 hours of order
AND r.PERFORMANCETIME BETWEEN r.OrderTime AND DATEADD(hour,4,r.ORDERTIME)
--and r.OrderingPhysicianCode in (@Consultant)
and casetypecode in (@Modality)
--and ExamCode1 in (@Exam)
group by

r.OrderDate
,DAY(r.OrderDate)
,r.CaseTypeCode
,r.CaseType
,r.OrderingPhysicianCode
,r.OrderingPhysician
,Case when datediff(hour,r.OrderTime,r.PerformanceTime)<1 then '<1' 
when datediff(hour,r.OrderTime,r.PerformanceTime)>=1 then '>=1' end
,r.ExamDescription 
,a.ArrivalTime
,DATEDIFF(MINUTE,a.ArrivalTime,r.OrderTime)
,DATEDIFF(MINUTE,r.OrderTime,PerformanceTime)
,DATEDIFF(MINUTE,r.PerformanceTime,r.DictationDate)
,DATEDIFF(MINUTE,a.arrivaltime,r.dictationdate)
,r.dictationdate
,c.[DayOfWeek]
,c.FinancialYear
,c.TheMonth
,c.DayOfWeekKey
,r.SocialSecurityNo
,r.examcode1
,t.HourBand

