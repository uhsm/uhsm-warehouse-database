﻿
create procedure [dbo].[ReportMISYSDIDDatasetBackup]

@DateFrom datetime,
@DateTo datetime

as

SELECT DISTINCT 
--replace incorrect or unknown NHS Numbers with NULL
	case when bOrder.SocialSecurityNo in ('ZZZZ999') 
	or bOrder.SocialSecurityNo like ('1234%')
	THEN NULL ELSE 
	--manipulate the 3,3,4 format of the NHS Number to one string
	replace((isnull(RTRIM(LEFT(bOrder.SocialSecurityNo,4))+RTRIM(SUBSTRING(bOrder.SocialSecurityNo,5,4))+RTRIM(SUBSTRING(bOrder.SocialSecurityNo,9,4)),'')),' ','') 
	END
	AS [NHS NUMBER]

	,RIGHT('0'+convert(varchar,
	(CASE WHEN vpas.NHSNoStatusNationalCode IS NULL AND bOrder.SocialSecurityNo IS NULL THEN 07
	WHEN vpas.NHSNoStatusNationalCode IS NULL AND bOrder.SocialSecurityNo IS NOT NULL THEN 02
	ELSE vpas.NHSNoStatusNationalCode END)),2) AS [NHS NUMBER STATUS INDICATOR CODE]

	,isnull(CAST(bOrder.DateOfBirth AS DATE),'') AS [PERSON BIRTH DATE]
	,CASE WHEN coalesce(op.EthnicGroupNHSCode,ip.EthnicGroupNHSCode,'Z')='99' THEN 'Z' ELSE coalesce(op.EthnicGroupNHSCode,ip.EthnicGroupNHSCode,'Z') END AS [ETHNIC CATEGORY]
	,CASE WHEN bOrder.PatientGenderCode IN ('U','0') THEN 0
	WHEN bOrder.PatientGenderCode IN ('M','1') THEN 1
	WHEN bOrder.PatientGenderCode IN ('F','2') THEN 2
	ELSE 9 END
	AS [PERSON GENDER CODE CURRENT], 
	border.postcode,


REPLACE(
(case when dbo.fnValidatePostCodeUK 
(replace(bOrder.Postcode,'  ',' '))=1 then bOrder.Postcode else '' end ),'  ',' ')



AS [POSTCODE OF USUAL ADDRESS]
,

CASE WHEN 
isnull(left(bOrder.RegisteredGpPracticeCode,6),'') 

IN ('7','166') THEN NULL ELSE isnull(left(bOrder.RegisteredGpPracticeCode,6),'') END AS [GENERAL MEDICAL PRACTICE CODE (PATIENT REGISTRATION)]


,RIGHT
('0'+CONVERT(VARCHAR,case when bOrder.PatientTypeCode='IP' THEN 1 WHEN bOrder.PatientTypeCode='OP' THEN 3 WHEN bOrder.PatientTypeCode='ER' THEN 5
ELSE 7 END),2) AS [PATIENT SOURCE SETTING TYPE (PATIENT REGISTRATION)]
,

CASE WHEN LEN(
(case when bOrder.OrderingPhysicianCode IS NULL OR bOrder.OrderingPhysicianCode='-1' THEN ('C9999998')
WHEN rfl.TrueCode IS NULL THEN bOrder.OrderingPhysicianCode

ELSE rfl.TrueCode END))<>8 THEN 'C9999998' ELSE
(case when bOrder.OrderingPhysicianCode IS NULL OR bOrder.OrderingPhysicianCode='-1' THEN ('C9999998')
WHEN rfl.TrueCode IS NULL THEN bOrder.OrderingPhysicianCode

ELSE rfl.TrueCode END) END

AS [REFERRER CODE]






,isnull(case when bOrder.OrderingLocationCode ='GP' THEN LEFT(bOrder.RegisteredGpPracticeCode,6) 
ELSE LEFT((
CASE WHEN loc_desc='WITHINGTON COMMUNITY HOSPITAL' THEN 'RM3' ELSE
o.organisation_code END),3) END,'RM2') AS [REFERRING ORGANISATION CODE]--Workaround as not available in MISYS DW


,isnull(CAST(


bOrder.OrderDate




 AS DATE),'') AS [DIAGNOSTIC TEST REQUEST DATE]--cannot link to Request Date From ICE?


,isnull(CAST(bOrder.OrderDate AS DATE),'') AS [DIAGNOSTIC TEST REQUEST RECEIVED DATE]--cannot link to Request Date From ICE?


,isnull(CAST(

case when 


bOrder.PerformanceDate>bOrder.FinalDate then bOrder.FinalDate
when bOrder.PerformanceDate<bOrder.OrderDate then bOrder.OrderDate

else bOrder.PerformanceDate end




 AS DATE),'') AS [DIAGNOSTIC TEST DATE]



,isnull((case when 
New_Exam_Code is null then bOrder.ExamCode1 else New_Exam_Code END)



,'') AS [IMAGING CODE (NICIP)]
,'' as [IMAGING CODE (SNOWMED-CT)]
,


(CASE WHEN 
bOrder.FinalDate='1900-01-01' OR bOrder.FinalDate IS NULL THEN NULL ELSE cast(bOrder.FinalDate as DATE) END)

 AS [SERVICE REPORT ISSUE DATE]
 
 
,Case when border.PerformanceLocation like ('WCH%') then 'RM201' else 'RM202' end  AS [SITE CODE (OF IMAGING)]--presumed that WCH and UHSM are the only sites
,isnull(bOrder.OrderNumber,'') AS [RADIOLOGICAL ACCESSION NUMBER]

FROM         
MISYS.[Order] AS bOrder LEFT OUTER JOIN

(select NHSNo,EthnicGroupNHSCode from
WHREPORTING.APC.Patient where EncounterRecno IN (select MAX(encounterrecno) from WHREPORTING.APC.Patient group by NHSNo))
AS ip 
ON ip.NHSNo = CAST(RTRIM(LEFT(bOrder.SocialSecurityNo, 4)) AS varchar) 
+ CAST(RTRIM(SUBSTRING(bOrder.SocialSecurityNo, 5, 4)) AS varchar) 
+ CAST(SUBSTRING(bOrder.SocialSecurityNo, 9, 4) AS varchar)




LEFT OUTER JOIN

(select NHSNo,EthnicGroupNHSCode from
WHREPORTING.OP.Patient where EncounterRecno in(select MAX(encounterrecno) from WHREPORTING.APC.Patient group by NHSNo))


AS op ON op.NHSNo = CAST(RTRIM(LEFT(bOrder.SocialSecurityNo, 4)) AS varchar) 
+ CAST(RTRIM(SUBSTRING(bOrder.SocialSecurityNo, 5, 4)) AS varchar) 
+ CAST(SUBSTRING(bOrder.SocialSecurityNo, 9, 4) AS varchar)   
left outer join MISYS.lk_LocationDescription d on d.pat_loc_code=bOrder.OrderingLocationCode
left outer join dbo.MaxOrganisationCodes o on o.Organisation_name=d.loc_desc
left outer join 

(select lpt.PATNT_REFNO_NHS_IDENTIFIER,lpt.NNNTS_CODE
from

Lorenzo.dbo.Patient lpt

where lpt.[PATNT_REFNO]in(select MAX([PATNT_REFNO]) from Lorenzo.dbo.Patient group by PATNT_REFNO_NHS_IDENTIFIER))

as lp



 on lp.PATNT_REFNO_NHS_IDENTIFIER=


rtrim(left(bOrder.SocialSecurityNo,4))
+rtrim(SUBSTRING(bOrder.SocialSecurityNo,5,4))
+rtrim(SUBSTRING(bOrder.SocialSecurityNo,9,4))and lp.NNNTS_CODE IS NOT NULL
LEFT OUTER JOIN [WHREPORTING].[dbo].[vwPASNHSNoStatus] vpas on vpas.NHSNoStatuslocalCode=lp.NNNTS_CODE

--left outer join Lorenzo.dbo.Patient lptest on lptest.SURNAME=bOrder.PatientSurname and lptest.DTTM_OF_BIRTH=bOrder.DateOfBirth
left outer join  MISYS.ReferrerLookup   rfl on rfl.referrercode=bOrder.OrderingPhysicianCode  
 left outer join MISYS.ExamCodeMap cm on cm.Original_exam_code =bOrder.ExamCode1          
WHERE     

--border.SocialSecurityNo is null and 



(isnull(CAST(

case when 


bOrder.PerformanceDate>bOrder.FinalDate then bOrder.FinalDate
when bOrder.PerformanceDate<bOrder.OrderDate then bOrder.OrderDate

else bOrder.PerformanceDate end




 AS DATE),'')
 BETWEEN @DateFrom AND @DateTo) 
 
 
 and
(bOrder.PerformanceLocation IS NOT NULL)
and NOT(bOrder.PatientSurname in 
('TEST','TESTER','TEST2','TESTPATIENT','XXTEST PATIENT','XXTESTER'))

and not (bOrder.PatientForename ='Fred' and bOrder.PatientSurname='Flintstone')
and not (bOrder.PatientForename ='Barney' and bOrder.PatientSurname='Rubble')
and not (bOrder.PatientForename ='Fred' and bOrder.PatientSurname='Flinstone')
and not (bOrder.PatientForename ='Dame' and bOrder.PatientSurname='Doody')
and not (bOrder.PatientForename ='Betty' and bOrder.PatientSurname='Rubble')
and not (bOrder.PatientForename ='Wilma' and bOrder.PatientSurname='Flintstone')

and border.OrderingPhysician not like ('Assess%')

AND SocialSecurityNo NOT IN ('0110110110','011 011 0110','5653249999''565 324 9999')

AND bOrder.ExamCode1 NOT IN ('HBIOL','XBSPER','HBIOM','HCACR','BSPE','XPATHL','HFLUC','XBSPEL','XPATHR','BRMDT')

and CaseTypeCode<>'Z'





