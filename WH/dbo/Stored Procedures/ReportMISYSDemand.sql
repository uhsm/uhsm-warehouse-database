﻿CREATE procedure [dbo].[ReportMISYSDemand]

    @MonthNumberFrom int
     ,@FiscalYearFrom int
    ,@MonthNumberTo int
    ,@FiscalYearTo int
    ,@CaseTypeCode as varchar(10)
    ,@ExamCode as varchar (10)
  ,@Specialty as varchar (100)
  ,@Consultant as varchar (100) 


as


SELECT    
count(MISYSOrder.OrderNumber) as OrdersPerformed,
OrderingPhysician.Staff as staff,
calendar.TheMonth AS MonthOrderedName, 
CASE WHEN month(MISYSOrder.OrderDate) BETWEEN 1 AND 
                      3 THEN YEAR(MISYSOrder.OrderDate) ELSE year(MISYSOrder.OrderDate) + 1 END AS FiscalYear, 
CASE WHEN MISYSOrder.CaseTypeCode = 'X' THEN 'Other' 
ELSE CASE WHEN MISYSOrder.CaseGroupCode = 'S' THEN 'Specialist' 
WHEN MISYSOrder.CaseGroupCode = 'G' THEN 'General' 
WHEN MISYSOrder.CaseGroupCode = 'O' THEN 'Other' 
ELSE 'Unknown' END END AS RequestedCaseType,
MISYSOrder.OrderExamCode, 
MISYSExam.Exam,
MISYSOrder.CaseTypeCode AS ModalityCode, 
Modality.Modality, 
ISNULL(MISYSOrder.PriorityCode, N'Attention') AS PriorityCode, 
MISYSOrder.PlannedRequest,  
MISYSOrder.PatientTypeCode AS CurrentPatientType,
MISYS.lkSpecTrue.SpecialtyTrue as Specialty,
CASE WHEN CAST(MONTH(MISYSOrder.OrderDate) AS INT) = 1 THEN 13 WHEN CAST(MONTH(MISYSOrder.OrderDate) AS INT) 
                      = 2 THEN 14 WHEN CAST(MONTH(MISYSOrder.OrderDate) AS INT) = 3 THEN 15 ELSE CAST(MONTH(MISYSOrder.OrderDate) AS INT) END as MonthSequence
FROM         
WHOLAP.dbo.BaseOrder AS MISYSOrder WITH (nolock) INNER JOIN
                      WHOLAP.dbo.OlapMISYSExam AS MISYSExam WITH (nolock) ON MISYSExam.ExamCode = MISYSOrder.OrderExamCode INNER JOIN
                      WHOLAP.dbo.OlapMISYSModality AS Modality WITH (nolock) ON Modality.ModalityCode = MISYSOrder.CaseTypeCode INNER JOIN
                      WHOLAP.dbo.OlapMISYSLocation AS OrderingLocation WITH (nolock) ON 
                      OrderingLocation.LocationCode = MISYSOrder.OrderingLocationCode INNER JOIN
                      WHOLAP.dbo.OlapMISYSStaff AS OrderingPhysician WITH (nolock) ON OrderingPhysician.StaffCode = MISYSOrder.OrderingPhysicianCode INNER JOIN
                      WHOLAP.dbo.OlapMISYSStaff AS Radiologist WITH (nolock) ON Radiologist.StaffCode = MISYSOrder.RadiologistCode INNER JOIN
                      WHOLAP.dbo.OlapMISYSTechnician AS Technician WITH (nolock) ON Technician.TechnicianCode = COALESCE (MISYSOrder.FilmTechCode, - 1) 
                      LEFT OUTER JOIN
                      WHOLAP.dbo.BaseOrder AS OtherOrder ON MISYSOrder.PatientNo = OtherOrder.PatientNo AND 
                      MISYSOrder.CaseTypeCode <> OtherOrder.CaseTypeCode AND NOT (OtherOrder.CaseTypeCode IN ('O', 'R', 'X', 'B', 'D', 'H', 'T', 'Z')) AND 
                      OtherOrder.OrderTime = OtherOrder.PerformanceTime AND OtherOrder.DictationDate IS NULL AND OtherOrder.FinalDate IS NULL AND 
                      OtherOrder.PerformanceLocation IS NULL OR
                      OtherOrder.PerformanceTime < OtherOrder.OrderTime AND OtherOrder.DictationDate IS NULL AND OtherOrder.FinalDate IS NULL AND 
                      OtherOrder.PerformanceLocation IS NULL AND OtherOrder.PerformanceTime > '2011-01-01' LEFT OUTER JOIN
                      MISYS.lkSpecTrue ON MISYS.lkSpecTrue.SpecialtyOrig = SUBSTRING(OrderingPhysician.Staff, PATINDEX('%([a-z]%)', OrderingPhysician.Staff) + 1, 
                      LEN(OrderingPhysician.Staff) - CASE patindex('%([a-z]%)', OrderingPhysician.Staff) WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', OrderingPhysician.Staff) 
                      + 1 END)
                      left join WHREPORTING.LK.Calendar calendar on calendar.TheDate=MISYSOrder.OrderDate
WHERE     
 (MISYSOrder.CaseTypeCode IN (@CaseTypeCode)) AND (CASE WHEN CAST(MONTH(MISYSOrder.OrderDate) AS INT) = 1 THEN 13 WHEN CAST(MONTH(MISYSOrder.OrderDate) AS INT) 
                      = 2 THEN 14 WHEN CAST(MONTH(MISYSOrder.OrderDate) AS INT) = 3 THEN 15 ELSE CAST(MONTH(MISYSOrder.OrderDate) AS INT) END BETWEEN @MonthNumberFrom AND @MonthNumberTo) AND (MISYS.lkSpecTrue.SpecialtyTrue IN (@Specialty)) 
                      AND (CASE WHEN month(MISYSOrder.OrderDate) BETWEEN 1 AND 
                      3 THEN YEAR(MISYSOrder.OrderDate) ELSE year(MISYSOrder.OrderDate) + 1 END BETWEEN @FiscalYearfrom AND @FiscalYearTo) AND (OrderingPhysician.Staff IN (@Consultant))





and

(MISYSOrder.CaseTypeCode IN ('M', 'C', 'U', 'X', 'D', 'O')) AND (NOT EXISTS
                          (SELECT     1 AS Expr1
                            FROM          MISYS.CancelledOrder AS canc
                            WHERE      (MISYSOrder.OrderNumber = OrderNumber))) AND 
                       (MISYSOrder.DictationDate IS NOT NULL) AND (MISYSOrder.PerformanceLocation IS NOT NULL) 
                
                      
                      
                      
                      
GROUP BY
CAST(MONTH(MISYSOrder.OrderDate) AS int), 
calendar.TheMonth, 
CASE WHEN month(MISYSOrder.OrderDate) BETWEEN 1 AND 
                      3 THEN YEAR(MISYSOrder.OrderDate) ELSE year(MISYSOrder.OrderDate) + 1 END, 
CASE WHEN MISYSOrder.CaseTypeCode = 'X' THEN 'Other' 
ELSE CASE WHEN MISYSOrder.CaseGroupCode = 'S' THEN 'Specialist' 
WHEN MISYSOrder.CaseGroupCode = 'G' THEN 'General' 
WHEN MISYSOrder.CaseGroupCode = 'O' THEN 'Other' 
ELSE 'Unknown' END END,
MISYSOrder.OrderExamCode, 
MISYSExam.Exam,
MISYSOrder.CaseTypeCode, 
Modality.Modality, 
ISNULL(MISYSOrder.PriorityCode, N'Attention'), 
MISYSOrder.PlannedRequest,  
MISYSOrder.PatientTypeCode,
MISYS.lkSpecTrue.SpecialtyTrue,
CASE WHEN CAST(MONTH(MISYSOrder.OrderDate) AS INT) = 1 THEN 13 WHEN CAST(MONTH(MISYSOrder.OrderDate) AS INT) 
                      = 2 THEN 14 WHEN CAST(MONTH(MISYSOrder.OrderDate) AS INT) = 3 THEN 15 ELSE CAST(MONTH(MISYSOrder.OrderDate) AS INT) END 
                      ,OrderingPhysician.Staff