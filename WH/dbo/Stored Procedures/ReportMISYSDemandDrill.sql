﻿CREATE procedure [dbo].[ReportMISYSDemandDrill]
--declare
	@CaseTypeCode as varchar(10)
	--,@CaseType as varchar(50)
 --   ,@OtherModality as varchar(5)
    --,@Month as varchar (3)
    --,@DateFrom as datetime
    --,@DateTo as datetime
    --,
    ,@MonthNumberFrom int
    ,@MonthNumberTo int
    ,@FiscalYearFrom int
    ,@FiscalYearTo int
  --  ,@ExamCode varchar (25)
-- ,@Specialty as varchar (25)



as


/*
	Set @CaseTypeCode = 'C'
	Set @Month = 'Jan'
    Set @orderdate = '2010-01-10'
*/

select distinct --distinct because there can be multiple other orders per patient
		 CountOrders=count(MISYSOrder.OrderNumber)
		,MISYSOrder.OrderTime
		,MISYSOrder.OrderDate
		,MonthOrdered=cast(month(MISYSOrder.OrderDate)as int)
		,MonthOrderedName=case when month(MISYSOrder.OrderDate)=1 then ('Jan')
		when month(MISYSOrder.OrderDate)=2 then ('Feb')
		when month(MISYSOrder.OrderDate)=3 then ('Mar')
		when month(MISYSOrder.OrderDate)=4 then ('Apr')
		when month(MISYSOrder.OrderDate)=5 then ('May')
		when month(MISYSOrder.OrderDate)=6 then ('Jun')
		when month(MISYSOrder.OrderDate)=7 then ('Jul')
		when month(MISYSOrder.OrderDate)=8 then ('Aug')
		when month(MISYSOrder.OrderDate)=9 then ('Sep')
		when month(MISYSOrder.OrderDate)=10 then ('Oct')
		when month(MISYSOrder.OrderDate)=11 then ('Nov')
		when month(MISYSOrder.OrderDate)=12 then ('Dec')
		else ('Other') end
		,c.FinancialYear
		,MISYSOrder.OrderingLocationCode
		,OrderingLocation = OrderingLocation.Location
		,OrderingLocationCategory = OrderingLocation.LocationCategory
		,MISYSOrder.CaseGroupCode
	--	,Specialty=SUBSTRING(MISYSOrder.ExamVar4, PATINDEX('%([a-z]%)', MISYSOrder.ExamVar4) + 1, LEN(MISYSOrder.ExamVar4) - CASE patindex('%([a-z]%)', MISYSOrder.ExamVar4) 
                  --    WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', MISYSOrder.ExamVar4) + 1 END)

--Updated KD 2011-08-11 uses CaseGroupCode from BaseOrder
		,RequestedCaseType =
				Case When MISYSOrder.CaseTypeCode = 'X' Then 'Other'
				Else
					Case 
						When MISYSOrder.CaseGroupCode = 'S' Then 'Specialist'
						When MISYSOrder.CaseGroupCode = 'G' Then 'General'	
						When MISYSOrder.CaseGroupCode = 'O' Then 'Other'
						Else 'Unknown'
					End
				End
/*
--Updated KD 2011-03-11
		,RequestedCaseType =
				Case When (
				MISYSOrder.CaseTypeCode IN ('B','H','T','Z','O','R')
				OR
				MISYSOrder.ExamCode1 IN (
										'IFACJJ',
										'FINJTJ',
										'FHIPLJ',
										'FHIPRJ',
										'FJOINJ',
										'FABDO',
										'FDIAP',
										'FLOLB',
										'FLOLL',
										'FLOLR',
										'FLSPN',
										'FUPLB',
										'FUPLL',
										'FUPLR',
										'FHIPLM',
										'FHIPRM',
										'FLSPNM',
										'FCYST',
										'MAAOT',
										'MACOA',
										'MCVIA',
										'MCCMS',
										'MCORP',
										'MCRPS',
										'MCSFS',
										'MCVFS',
										'MCVVS',
										'MCORV'
										)
				OR 
				(MISYSOrder.ExamCode1 = 'MAAOT' AND (OrderingPhysician.Staff like '%(CARDIO)%' OR OrderingPhysician.Staff like '%(THOR)%'))
				) Then 'Specialist'
			Else
				'General'
			End
*/
		--Updated KD 2011-03-11
	/*	,BookingStatus = 
			Case When MISYSOrder.ExamDescription = 'US TRANSRECTAL PROSTATE AND BIOPSY' Then
				'Attention'
			Else
				Case When MISYSOrder.OrderScheduledFlag = 'Y' Then
					'Booked'
				Else
					'Not Booked'
				End
			End*/




/*
-- 2011-08-19 KD Depricated as OrderScheduledFlag seems to make more sense
		,BookingStatus = 
			Case When MISYSOrder.PerformanceTime = MISYSOrder.OrderTime Then
				'Not Booked'
			Else
				Case When
					(
					MISYSOrder.OrderDate < @censusDate
					and (MISYSOrder.PerformanceTime < MISYSOrder.OrderTime)
					and MISYSOrder.PerformanceTime > '2011-01-01'
					)
					OR
					(
					MISYSOrder.ExamDescription = 'US TRANSRECTAL PROSTATE AND BIOPSY'
					)
					Then
				'Attention'
				Else
					Case when MISYSOrder.PerformanceTime > MISYSOrder.OrderTime then
						'Booked'
					Else 
						'Other'
					End
				End
			End
*/
	--	,MISYSOrder.OrderScheduledFlag
	--	,MISYSOrder.OrderingPhysicianCode
	--	,OrderingPhysician = OrderingPhysician.Staff
		,MISYSOrder.OrderExamCode
		,MISYSExam.Exam
		,MISYSOrder.ExamGroupCode
		,MISYSExam.ExamGroup
		,MISYSOrder.DepartmentCode
		,MISYSExam.Department
	--	,MISYSOrder.PatientNo
	--	,MISYSOrder.PatientName
	--	,MISYSOrder.DateOfBirth
	--	,MISYSOrder.RegisteredGpPracticeCode
		,ModalityCode = MISYSOrder.CaseTypeCode
		,Modality.Modality
		,MISYSOrder.PerformanceTime
		,MISYSOrder.CreationDate
		,MISYSOrder.RadiologistCode
	--	,Radiologist = Radiologist.Staff
		,TechnicianCode = MISYSOrder.FilmTechCode
	--	,Technician.Technician
		--Added 2011-03-11 KD
		,PriorityCode = IsNUll(MISYSOrder.PriorityCode,'Attention')
		,MISYSOrder.PlannedRequest
		--Added 2011-03-21 KD
	/*	,AgeAtPerformanceDate =
	Case When datediff(WEEK,MISYSOrder.DateOfBirth, MISYSOrder.PerformanceDate) < 53 
		Then cast(datediff(WEEK,MISYSOrder.DateOfBirth, MISYSOrder.PerformanceDate) as varchar(5))
		+ 'w'
		Else
		cast(datediff(YY, MISYSOrder.DateOfBirth, MISYSOrder.PerformanceDate) 
		-
		case 
		when (
			month(MISYSOrder.DateOfBirth) = month(MISYSOrder.PerformanceDate) 
			AND day(MISYSOrder.DateOfBirth) > day(MISYSOrder.PerformanceDate)
			OR month(MISYSOrder.DateOfBirth) > month(MISYSOrder.PerformanceDate)
			)
		then 1 else 0 end as varchar(5))
		End
		--Added 2011-03-27 KD*/
	/*,TargetDate = 
	Case 
		When MISYSOrder.PriorityCode = 'HSC205' then 
			case 
			when MISYSOrder.OrderDate <'2011-04-01' then 
				MISYSOrder.OrderDate + 14
			else
				MISYSOrder.OrderDate + 5
			End
		Else MISYSOrder.OrderDate + 28
	End*/
--Updated 2011-03-21 KD	
	/*,BreachDate = 
	Case 
		When MISYSOrder.PriorityCode = 'HSC205' then 
			case 
			when MISYSOrder.OrderDate <'2011-04-01' then 
				MISYSOrder.OrderDate + 14
			else
				MISYSOrder.OrderDate + 5
			End
		Else MISYSOrder.OrderDate + 42
	End*/

--2011-08-04 KD Breach Value ([Wait(Weeks)] on report) redefined as PerformanceTime - OrderTime
	/*,BreachValue = 
			Case When (cast(DATEDIFF(day,MISYSOrder.OrderDate, MISYSOrder.PerformanceDate) as decimal(10,2)) / 7) <= 0 
			Then  0
			Else
			(cast(DATEDIFF(day,MISYSOrder.OrderDate, MISYSOrder.PerformanceDate) as decimal(10,2)) / 7)
			End*/
-- Superceded 2011-08-04,BreachValue = (cast(DATEDIFF(day,MISYSOrder.OrderDate, @censusDate) as decimal(10,2)) / 7)
--	,Comments = Questionnaire.Comments
--Added 2011-04-01 IS
	,PCT = MISYSOrder.PCTCode
--Added 2011-03-27 KD	




	/*,BreachStatus = 
--Pending
		Case when MISYSOrder.PlannedRequest like '%Y%' AND MISYSOrder.PriorityCode = 'PEND' Then 
			'Pending'
		Else
--Attention
		Case When (MISYSOrder.PerformanceTime < MISYSOrder.ApprovedTime and MISYSOrder.ApprovedTime >= '2011-01-01') Then
			'Attention'
		Else
		
--Unbooked. Breach
			Case When (MISYSOrder.OrderScheduledFlag = 'N') Then
				Case When 
							Case 
								When MISYSOrder.PriorityCode = 'HSC205' then 
									MISYSOrder.OrderDate + 6
								Else 
									MISYSOrder.OrderDate + 43
							End <= CAST(getdate() as DATE)

				Then 'Unbooked Breach'
				Else
					'Unbooked'
				End

		Else
--Booked Breach
			Case When (MISYSOrder.OrderScheduledFlag = 'Y') Then
				Case When 
							Case 
								When MISYSOrder.PriorityCode = 'HSC205' then 
									MISYSOrder.OrderDate + 6
								Else 
									MISYSOrder.OrderDate + 43
							End <= CAST(getdate() as DATE) 	Then 

						'Booked Breach'
						Else 
						
						'Booking Okay'
						
						End
				End
	
		End
	
	End
End*/





--,HasOtherModality = Case When OtherOrder.OrderNumber Is Null Then 'N' Else 'Y' End
/*,DM01Indicator = 
	Case
		When MISYSOrder.ExamDescription = 'Barium Enema' Then 1
		When MISYSOrder.CaseTypeCode in ('C','M','U','D') Then 1
		Else 0
	End*/
,CurrentPatientType = MISYSOrder.PatientTypeCode
	from
		WHOLAP.dbo.BaseOrder MISYSOrder with (nolock)

	inner join WHOLAP.dbo.OlapMISYSExam MISYSExam with (nolock)
	on	MISYSExam.ExamCode = MISYSOrder.OrderExamCode

	inner join WHOLAP.dbo.OlapMISYSModality Modality with (nolock)
	on	Modality.ModalityCode = MISYSOrder.CaseTypeCode

	inner join WHOLAP.dbo.OlapMISYSLocation OrderingLocation with (nolock)
	on	OrderingLocation.LocationCode = MISYSOrder.OrderingLocationCode

	inner join WHOLAP.dbo.OlapMISYSStaff OrderingPhysician with (nolock)
	on	OrderingPhysician.StaffCode = MISYSOrder.OrderingPhysicianCode

	inner join WHOLAP.dbo.OlapMISYSStaff Radiologist with (nolock)
	on	Radiologist.StaffCode = MISYSOrder.RadiologistCode

	inner join WHOLAP.dbo.OlapMISYSTechnician Technician with (nolock)
	on	Technician.TechnicianCode = coalesce(MISYSOrder.FilmTechCode, -1)

	left join MISYS.Questionnaire Questionnaire with (nolock)
	on MISYSOrder.OrderNumber = Questionnaire.OrderNumber
/*	
	left outer join dbo.EntityLookup SpecialistExam with (nolock)
	on MISYSOrder.ExamCode1 = SpecialistExam.EntityCode
	and SpecialistExam.EntityTypeCode = 'MISYSSPECIALISTEXAMCODE'
*/

left join WHOLAP.dbo.BaseOrder OtherOrder
on						
						MisysOrder.PatientNo = OtherOrder.PatientNo
						and MisysOrder.CaseTypeCode <> OtherOrder.CaseTypeCode
						and (not OtherOrder.CaseTypeCode in 	('O','R','X','B','D','H','T','Z'))
						and
						(
						--(
							--OtherOrder.OrderDate < @censusDate
						--and	OtherOrder.PerformanceDate >= @censusDate
						--)
						--Or 
						(
						--OtherOrder.OrderDate < @censusDate
						--and 
						OtherOrder.OrderTime = OtherOrder.PerformanceTime
						and (OtherOrder.DictationDate is null and OtherOrder.FinalDate is null and OtherOrder.PerformanceLocation is null)
						)
						Or 
						(
						--OtherOrder.OrderDate < @censusDate
						--and 
						(OtherOrder.PerformanceTime < OtherOrder.OrderTime)
						and (OtherOrder.DictationDate is null and OtherOrder.FinalDate is null and OtherOrder.PerformanceLocation is null)
						and OtherOrder.PerformanceTime > '2011-01-01'
						)
						)
 INNER JOIN
                      Calendar AS c ON c.TheDate = MISYSOrder.OrderDate
                      
                  
                      

	where 
	--MISYSOrder.CaseTypeCode in ('M','C','U','X','D','O') AND
	--MISYSOrder.OrderDate >= @DateFrom
	--and MISYSOrder.OrderDate <= @DateTo AND
	
	(CAST(month(MISYSOrder.OrderDate)AS INT)between @MonthNumberFrom and @MonthNumberTo)
	
	and 
	
	
	(Case when month(MISYSOrder.OrderDate)between 1 and 3 then YEAR(MISYSOrder.OrderDate)
	else 
	year(MISYSOrder.OrderDate)+1 end between @FiscalYearfrom and @FiscalYearTo)
	
/*and


Case when
(SUBSTRING(MISYSOrder.ExamVar4, PATINDEX('%([a-z]%)', MISYSOrder.ExamVar4) + 1, LEN(MISYSOrder.ExamVar4) - CASE patindex('%([a-z]%)', MISYSOrder.ExamVar4) 
            WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', MISYSOrder.ExamVar4) + 1 END)) is null
            
            and 
   
 LEFT((SUBSTRING(MISYSOrder.PrimaryPhysician, PATINDEX('%([a-z]%)', MISYSOrder.PrimaryPhysician) + 1, LEN(MISYSOrder.PrimaryPhysician) - CASE patindex('%([a-z]%)', MISYSOrder.PrimaryPhysician) 
            WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', MISYSOrder.PrimaryPhysician) + 1 END)),2) in ('GP')           
 
 Then ('GP')    
 
       
 
WHEN  (SUBSTRING(MISYSOrder.ExamVar4, PATINDEX('%([a-z]%)', MISYSOrder.ExamVar4) + 1, LEN(MISYSOrder.ExamVar4) - CASE patindex('%([a-z]%)', MISYSOrder.ExamVar4) 
            WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', MISYSOrder.ExamVar4) + 1 END)) IS NOT NULL
            THEN 
            (SUBSTRING(MISYSOrder.ExamVar4, PATINDEX('%([a-z]%)', MISYSOrder.ExamVar4) + 1, LEN(MISYSOrder.ExamVar4) - CASE patindex('%([a-z]%)', MISYSOrder.ExamVar4) 
            WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', MISYSOrder.ExamVar4) + 1 END))
            
            ELSE ('Other') end
            
            
             in (@Specialty)*/

--and
	/*(
	(
		case when month(MISYSOrder.OrderDate)=1 then ('Jan')
		when month(MISYSOrder.OrderDate)=2 then ('Feb')
		when month(MISYSOrder.OrderDate)=3 then ('Mar')
		when month(MISYSOrder.OrderDate)=4 then ('Apr')
		when month(MISYSOrder.OrderDate)=5 then ('May')
		when month(MISYSOrder.OrderDate)=6 then ('Jun')
		when month(MISYSOrder.OrderDate)=7 then ('Jul')
		when month(MISYSOrder.OrderDate)=8 then ('Aug')
		when month(MISYSOrder.OrderDate)=9 then ('Sep')
		when month(MISYSOrder.OrderDate)=10 then ('Oct')
		when month(MISYSOrder.OrderDate)=11 then ('Nov')
		when month(MISYSOrder.OrderDate)=12 then ('Dec')
		else ('Other') end in (@Month)
		

*/
	and	
	(
			MISYSOrder.CaseTypeCode in (@CaseTypeCode)
	
		)
		
		--and (MISYSExam.ExamCode in (@ExamCode))

	
	And not exists 
		(
		Select 1 
		from MISYS.CancelledOrder canc
		where MISYSOrder.OrderNumber = canc.OrderNumber
		)
		
		
		
		
Group By

c.FinancialYear
		,MISYSOrder.OrderTime
		,MISYSOrder.OrderDate
		,month(MISYSOrder.OrderDate)
	     ,c.FinancialYear,MISYSOrder.OrderingLocationCode
		,OrderingLocation.Location
		,OrderingLocation.LocationCategory
		,MISYSOrder.CaseGroupCode
		,SUBSTRING(MISYSOrder.ExamVar4, PATINDEX('%([a-z]%)', MISYSOrder.ExamVar4) + 1, LEN(MISYSOrder.ExamVar4) - CASE patindex('%([a-z]%)', MISYSOrder.ExamVar4) 
                      WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', MISYSOrder.ExamVar4) + 1 END)

--Updated KD 2011-08-11 uses CaseGroupCode from BaseOrder
		,
				Case When MISYSOrder.CaseTypeCode = 'X' Then 'Other'
				Else
					Case 
						When MISYSOrder.CaseGroupCode = 'S' Then 'Specialist'
						When MISYSOrder.CaseGroupCode = 'G' Then 'General'	
						When MISYSOrder.CaseGroupCode = 'O' Then 'Other'
						Else 'Unknown'
					End
				End

		,MISYSOrder.OrderExamCode
		,MISYSExam.Exam
		,MISYSOrder.ExamGroupCode
		,MISYSExam.ExamGroup
		,MISYSOrder.DepartmentCode
		,MISYSExam.Department
		,MISYSOrder.CaseTypeCode
		,Modality.Modality
		,MISYSOrder.PerformanceTime
		,MISYSOrder.CreationDate
		,MISYSOrder.RadiologistCode
		,MISYSOrder.FilmTechCode
		,IsNUll(MISYSOrder.PriorityCode,'Attention')
		,MISYSOrder.PlannedRequest
		, Questionnaire.Comments

	,MISYSOrder.PCTCode
,MISYSOrder.PatientTypeCode	