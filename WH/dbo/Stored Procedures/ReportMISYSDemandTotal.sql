﻿CREATE procedure [dbo].[ReportMISYSDemandTotal]

    @MonthNumberFrom int
     ,@FiscalYearFrom int
    ,@MonthNumberTo int
    ,@FiscalYearTo int
    ,@CaseTypeCode as varchar(10)
    ,@ExamCode as varchar (10)
  ,@Specialty as varchar (100)
  ,@Priority as varchar (10)
  ,@PatientType as varchar (10)


as


select 
e.FinancialYear
,e.MonthOrderedName
,e.MonthSequence
,ThisYrOrders=sum(e.OrdersPerformed)
,LastYrOrders=
(select sum(d.OrdersPerformed)
from MISYS.DemandDataset d
where d.monthsequence=e.monthsequence
and d.MonthOrderedName=e.MonthOrderedName
and d.Modalitycode=e.modalitycode
and d.OrderExamCode=e.orderexamcode
and case when d.Specialty is null then 'Unknown' else d.Specialty end=case when e.Specialty is null then 'Unknown' else e.Specialty end
and d.PriorityCode=e.prioritycode
and d.CurrentPatientType=e.currentpatienttype
and d.FinancialYear=e.financialyear-1)
,e.ModalityCode
,e.OrderExamCode
,e.Specialty
,e.PriorityCode
,e.CurrentPatientType


from MISYS.DemandDataset e



where e.MonthSequence between @MonthNumberFrom and @MonthNumberTo
and e.FinancialYear between @FiscalYearFrom and @FiscalYearTo
and e.ModalityCode in (@Casetypecode)
and e.specialty in (@Specialty)
and e.currentpatienttype in (@patienttype)
and e.orderexamcode in (@examcode)
and e.prioritycode in (@priority)


group by 
e.FinancialYear
,e.MonthOrderedName
,e.MonthSequence
,e.ModalityCode
,e.OrderExamCode
,e.Specialty
,e.PriorityCode
,e.CurrentPatientType