﻿CREATE procedure [dbo].[ReportMISYSDirectorate]


as


select
(dt.MonthOrderedName+' '+cast(dt.[year] as varchar)) as TheMonth
,case when dt.MonthOrdered in ('1','2','3') then cast(dt.[Year]-1 as varchar)+'/'+cast(dt.[YEAR] as varchar) else cast(dt.[year] as varchar)+'/'+cast(dt.[Year]+1 as varchar) end as FinancialYear
,dt.[Year]
,dt.MonthOrdered
,case when MonthOrdered=1 then 13 when MonthOrdered=2 then 14 when MonthOrdered=3 then 15 else MonthOrdered end as MonthSequence
,dt.MonthOrderedName
,dt.ModalityCode
,case when dt.Modalitycode ='X' then 'PLAIN RADIOGRAPHY AND DIAGNOSTICS' else dt.Modality end as Modality
,dt.Specialty
,dt.Orders AS Exams
,dt.OrdersLastYr as ExamsLastYear





 from MISYS.DemandTotal dt
 
 
 where cast((cast(dt.Year as varchar)+'-'+cast(dt.MonthOrdered as varchar)+'-'+'01') as datetime)>
 
 
 (case when month(getdate()) in ('1','2','3') then 
  cast((cast((datepart(year,GETDATE())-2)as varchar)+'03'+'31') as DATE)
else cast((cast((datepart(year,GETDATE())-1)as varchar)+'03'+'31') as DATE) end)
 
 
 
 and
 
 
 cast((cast(dt.Year as varchar)+'-'+cast(dt.MonthOrdered as varchar)+'-'+'01') as datetime)<
  cast(DATEADD(day,(datepart(day,getdate())*-1)+1,GETDATE()) as DATE)
 
 
 


