﻿CREATE procedure [dbo].[ReportMISYSDirectorateReport]

    @DateFrom datetime
     ,@DateTo datetime
    


as

/*create table
MISYS.HSC205Dataset

(Modality nvarchar (100),
Department nvarchar (10),
OrderDate date,
PerformanceDate date,
PatientNo nvarchar (15),
PatientTypeCode nvarchar (2),
OrderNumber nvarchar (10),
ExamDescription nvarchar (130),
OrderingPhysician nvarchar (30),
DictationDate date,
MinIceRptDate date,
RadiologistCode nvarchar (10),
DateOfReport date,
FiveDayTarget int,
TwoDayTarget int,
SevenDayTarget int,
CountFiveDayBreaches int,
FiveDayBreachesIndicator int,
CountTwoDayBreaches int,
CountSevenDayBreaches int,
Unreported int,
UnreportedFlag int,
NotYetPerformed int,
MonthOrdered date,
FiveDayBreachFlag int,
TwoDayBreachFlag int,
SevenDayBreachFlag int,
CaseTypeCode nvarchar (10))*/
/*truncate table MISYS.HSC205Dataset

insert into MISYS.HSC205Dataset
(Modality,
Department,
OrderDate,
PerformanceDate,
PatientNo,
PatientTypeCode,
OrderNumber,
ExamDescription,
OrderingPhysician,
DictationDate,
MinIceRptDate,
RadiologistCode,
DateOfReport,
FiveDayTarget,
TwoDayTarget,
SevenDayTarget,
CountFiveDayBreaches,
FiveDayBreachesIndicator,
CountTwoDayBreaches,
CountSevenDayBreaches,
Unreported,
UnreportedFlag,
NotYetPerformed,
MonthOrdered,
FiveDayBreachFlag,
TwoDayBreachFlag,
SevenDayBreachFlag,
CaseTypeCode)*/



SELECT     

CASE	WHEN casetypecode = ('C') THEN ('CT') 
		WHEN casetypecode = ('M') THEN ('MRI') 
		WHEN casetypecode = ('U') THEN ('US') 
        WHEN casetypecode = ('I') THEN ('Nm') 
        WHEN casetypecode = ('F') THEN ('Ba') 
        ELSE ('Other') END AS Modality, 
b.Department, 
CAST(b.OrderDate AS Date) AS OrderDate, 
CAST(b.PerformanceDate AS Date) AS PerformanceDate, 
b.PatientNo, 
b.PatientTypeCode, 
b.OrderNumber, 
b.ExamDescription, 
REPLACE(b.OrderingPhysician, ';1', '') AS OrderingPhysician, 
CAST(b.DictationDate AS Date) AS DictationDate, 
min(r.ReportDate)as ICEMinRptDate,
b.RadiologistCode, 
CAST((CASE WHEN (DictationDate IS NULL OR DictationDate = '1975-12-31 00:00:00.000') 
				AND PerformanceDate IS NOT NULL THEN getdate() 
		ELSE DictationDate END) AS Date) AS DateOfReport, 
		
DATEDIFF(d, b.OrderDate, b.PerformanceDate) AS DaysToScan, 


CASE WHEN (DATEDIFF(d, b.PerformanceDate, 
(CASE WHEN (DictationDate IS NULL OR DictationDate = '1975-12-31 00:00:00.000') 
AND PerformanceDate IS NOT NULL THEN getdate() ELSE DictationDate END))) < 0 
AND performancedate = orderdate THEN NULL 


WHEN (DATEDIFF(d, b.PerformanceDate, 
(CASE WHEN (DictationDate IS NULL OR DictationDate = '1975-12-31 00:00:00.000') 
AND PerformanceDate IS NOT NULL THEN getdate() ELSE DictationDate END))) < 0 
AND performancedate <> orderdate THEN 0 ELSE 
(DATEDIFF(d, b.PerformanceDate, (CASE WHEN (DictationDate IS NULL OR DictationDate = '1975-12-31 00:00:00.000') 
AND PerformanceDate IS NOT NULL THEN getdate() ELSE DictationDate END))) END AS DaysToReport, 
                      
                      
                      
                      DATEDIFF(d, b.OrderDate, (CASE WHEN (DictationDate IS NULL OR
                      DictationDate = '1975-12-31 00:00:00.000') AND PerformanceDate IS NOT NULL THEN getdate() ELSE DictationDate END)) AS DaysFromOrderToReport, 
                      
                      CASE WHEN (Datediff(d, OrderDate, PerformanceDate)) > 27 THEN 1 ELSE 0 END AS FourWeekBreachFlag, 
                     
                      CASE WHEN RADIOLOGISTCODE IN ('-1') AND performancedate < getdate() THEN 1 ELSE 0 END AS UnreportedFlag, 
                      CASE WHEN PerformanceDate > GETDATE() THEN COUNT(OrderNumber) ELSE 0 END AS NotYetPerformed, c.TheMonth AS MonthOrdered,  
                      b.CaseTypeCode
FROM         MISYS.[Order] AS b INNER JOIN
                      Calendar AS c ON c.TheDate = b.OrderDate
                      
                      
                      left outer join WHREPORTING.ICE.Patient p on b.patientno=p.ICEHospitalNumber
                      left outer join WHREPORTING.ICE.FactReport r on r.MasterPatientCode=p.PatientKey
                      and datepart(day,b.creationdate)=datepart(day,r.[RequestDateTime])
and datepart(month,b.creationdate)=datepart(month,r.[RequestDateTime])
and datepart(year,b.creationdate)=datepart(year,r.[RequestDateTime])
                      
      
WHERE     

--(b.PriorityCode = 'HSC205') AND 

(b.OrderDate >'2011-03-31') 

/*AND 
                      (b.ExamCode1 NOT IN ('CMUGA', 'MCCMS', 'MCORP', 'MCRPS', 'MCSFS', 'MCVIA', 'MCVVS', 'MMAMB', 'NSENT', 'XDEXA', 'MMAMC', 'FPAC'))*/


and OrderDate between @Datefrom and @DateTo

GROUP BY b.Department, b.OrderDate, b.PerformanceDate, b.PatientNo, b.PatientTypeCode, b.OrderNumber, b.ExamDescription, b.OrderingPhysician, 
                      b.DictationDate, b.Department, b.RadiologistCode, c.TheMonth, b.CaseTypeCode,b.creationdate
