﻿CREATE procedure [dbo].[ReportMISYSExam]

   @MonthNumberFrom int
    ,@MonthNumberTo int
    ,@FiscalYearFrom int
    ,@FiscalYearTo int
    ,@CaseTypeCode varchar (25)
as

select distinct --distinct because there can be multiple other orders per patient
		
		MISYSOrder.OrderExamCode
		,MISYSExam.Exam
		,MISYSOrder.ExamGroupCode
		,MISYSExam.ExamGroup
		
	from
		WHOLAP.dbo.BaseOrder MISYSOrder with (nolock)

	inner join WHOLAP.dbo.OlapMISYSExam MISYSExam with (nolock)
	on	MISYSExam.ExamCode = MISYSOrder.OrderExamCode

	inner join WHOLAP.dbo.OlapMISYSModality Modality with (nolock)
	on	Modality.ModalityCode = MISYSOrder.CaseTypeCode

	inner join WHOLAP.dbo.OlapMISYSLocation OrderingLocation with (nolock)
	on	OrderingLocation.LocationCode = MISYSOrder.OrderingLocationCode

	inner join WHOLAP.dbo.OlapMISYSStaff OrderingPhysician with (nolock)
	on	OrderingPhysician.StaffCode = MISYSOrder.OrderingPhysicianCode

	inner join WHOLAP.dbo.OlapMISYSStaff Radiologist with (nolock)
	on	Radiologist.StaffCode = MISYSOrder.RadiologistCode

	inner join WHOLAP.dbo.OlapMISYSTechnician Technician with (nolock)
	on	Technician.TechnicianCode = coalesce(MISYSOrder.FilmTechCode, -1)

	left join MISYS.Questionnaire Questionnaire with (nolock)
	on MISYSOrder.OrderNumber = Questionnaire.OrderNumber
/*	
	left outer join dbo.EntityLookup SpecialistExam with (nolock)
	on MISYSOrder.ExamCode1 = SpecialistExam.EntityCode
	and SpecialistExam.EntityTypeCode = 'MISYSSPECIALISTEXAMCODE'
*/

left join WHOLAP.dbo.BaseOrder OtherOrder
on						
						MisysOrder.PatientNo = OtherOrder.PatientNo
						and MisysOrder.CaseTypeCode <> OtherOrder.CaseTypeCode
						and (not OtherOrder.CaseTypeCode in 	('O','R','X','B','D','H','T','Z'))
						and
						(
						--(
							--OtherOrder.OrderDate < @censusDate
						--and	OtherOrder.PerformanceDate >= @censusDate
						--)
						--Or 
						(
						--OtherOrder.OrderDate < @censusDate
						--and 
						OtherOrder.OrderTime = OtherOrder.PerformanceTime
						and (OtherOrder.DictationDate is null and OtherOrder.FinalDate is null and OtherOrder.PerformanceLocation is null)
						)
						Or 
						(
						--OtherOrder.OrderDate < @censusDate
						--and 
						(OtherOrder.PerformanceTime < OtherOrder.OrderTime)
						and (OtherOrder.DictationDate is null and OtherOrder.FinalDate is null and OtherOrder.PerformanceLocation is null)
						and OtherOrder.PerformanceTime > '2011-01-01'
						)
						)
 INNER JOIN
                      Calendar AS c ON c.TheDate = MISYSOrder.OrderDate
                      
                  
                      

	where 
	--MISYSOrder.CaseTypeCode in ('M','C','U','X','D','O') AND
	--MISYSOrder.OrderDate >= @DateFrom
	--and MISYSOrder.OrderDate <= @DateTo AND
	
	(CAST(month(MISYSOrder.OrderDate)AS INT)between @MonthNumberFrom and @MonthNumberTo)
	
	and 
	
	
	(Case when month(MISYSOrder.OrderDate)between 1 and 3 then YEAR(MISYSOrder.OrderDate)
	else 
	year(MISYSOrder.OrderDate)+1 end between @FiscalYearfrom and @FiscalYearTo)
	
/*and


Case when
(SUBSTRING(MISYSOrder.ExamVar4, PATINDEX('%([a-z]%)', MISYSOrder.ExamVar4) + 1, LEN(MISYSOrder.ExamVar4) - CASE patindex('%([a-z]%)', MISYSOrder.ExamVar4) 
            WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', MISYSOrder.ExamVar4) + 1 END)) is null
            
            and 
   
 LEFT((SUBSTRING(MISYSOrder.PrimaryPhysician, PATINDEX('%([a-z]%)', MISYSOrder.PrimaryPhysician) + 1, LEN(MISYSOrder.PrimaryPhysician) - CASE patindex('%([a-z]%)', MISYSOrder.PrimaryPhysician) 
            WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', MISYSOrder.PrimaryPhysician) + 1 END)),2) in ('GP')           
 
 Then ('GP')    
 
       
 
WHEN  (SUBSTRING(MISYSOrder.ExamVar4, PATINDEX('%([a-z]%)', MISYSOrder.ExamVar4) + 1, LEN(MISYSOrder.ExamVar4) - CASE patindex('%([a-z]%)', MISYSOrder.ExamVar4) 
            WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', MISYSOrder.ExamVar4) + 1 END)) IS NOT NULL
            THEN 
            (SUBSTRING(MISYSOrder.ExamVar4, PATINDEX('%([a-z]%)', MISYSOrder.ExamVar4) + 1, LEN(MISYSOrder.ExamVar4) - CASE patindex('%([a-z]%)', MISYSOrder.ExamVar4) 
            WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', MISYSOrder.ExamVar4) + 1 END))
            
            ELSE ('Other') end
            
            
             in (@Specialty)*/

--and
	/*(
	(
		case when month(MISYSOrder.OrderDate)=1 then ('Jan')
		when month(MISYSOrder.OrderDate)=2 then ('Feb')
		when month(MISYSOrder.OrderDate)=3 then ('Mar')
		when month(MISYSOrder.OrderDate)=4 then ('Apr')
		when month(MISYSOrder.OrderDate)=5 then ('May')
		when month(MISYSOrder.OrderDate)=6 then ('Jun')
		when month(MISYSOrder.OrderDate)=7 then ('Jul')
		when month(MISYSOrder.OrderDate)=8 then ('Aug')
		when month(MISYSOrder.OrderDate)=9 then ('Sep')
		when month(MISYSOrder.OrderDate)=10 then ('Oct')
		when month(MISYSOrder.OrderDate)=11 then ('Nov')
		when month(MISYSOrder.OrderDate)=12 then ('Dec')
		else ('Other') end in (@Month)
		

*/
	and	
	(
			Modality.ModalityCode in (@CaseTypeCode)
	
		)
		

	
	And not exists 
		(
		Select 1 
		from MISYS.CancelledOrder canc
		where MISYSOrder.OrderNumber = canc.OrderNumber
		)
		
		
		
		
Group By

		
		MISYSOrder.OrderExamCode
		,MISYSExam.Exam
		,MISYSOrder.ExamGroupCode
		,MISYSExam.ExamGroup
		