﻿CREATE procedure [dbo].[ReportMISYSFiscalYear]

 


as



select distinct 
	
	FiscalYear=(Case when month(MISYSOrder.OrderDate)between 1 and 3 then YEAR(MISYSOrder.OrderDate)
	else 
	year(MISYSOrder.OrderDate)+1 end),
	FiscalYearLookup=Case when month(MISYSOrder.OrderDate)between 1 and 3 then cast((YEAR(MISYSOrder.OrderDate)-1)AS varchar)+'/'+cast((YEAR(MISYSOrder.OrderDate)) AS varchar)
	else 
	cast((YEAR(MISYSOrder.OrderDate))AS varchar)+'/'+cast((YEAR(MISYSOrder.OrderDate)+1) AS varchar) end

	from
		WHOLAP.dbo.BaseOrder MISYSOrder with (nolock)
		   WHERE(MISYSOrder.OrderDate > '2009-03-31')
group by

	(Case when month(MISYSOrder.OrderDate)between 1 and 3 then YEAR(MISYSOrder.OrderDate)
	else 
	year(MISYSOrder.OrderDate)+1 end),
	Case when month(MISYSOrder.OrderDate)between 1 and 3 then cast((YEAR(MISYSOrder.OrderDate)-1)AS varchar)+'/'+cast((YEAR(MISYSOrder.OrderDate)) AS varchar)
	else 
	cast((YEAR(MISYSOrder.OrderDate))AS varchar)+'/'+cast((YEAR(MISYSOrder.OrderDate)+1) AS varchar) end
	order by
	(Case when month(MISYSOrder.OrderDate)between 1 and 3 then YEAR(MISYSOrder.OrderDate)
	else 
	year(MISYSOrder.OrderDate)+1 end)
	