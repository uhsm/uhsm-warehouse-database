﻿CREATE procedure [dbo].[ReportMISYSGPUnreported]

   @DateFrom datetime,
   @DateTo datetime


as




SELECT 
misysoRDER.PatientNo,   
MISYSOrder.OrderNumber,
MISYSOrder.ExamDescription,
MISYSOrder.OrderDate,
MISYSOrder.PerformanceDate,
MISYSOrder.performancelocation,
MISYSOrder.DictationDate,
MISYSOrder.OrderingLocationCode,
OrderingPhysician.Staff as OrderingstaffName,
CASE WHEN MISYSOrder.CaseTypeCode = 'X' THEN 'Other' 
ELSE CASE WHEN MISYSOrder.CaseGroupCode = 'S' THEN 'Specialist' 
WHEN MISYSOrder.CaseGroupCode = 'G' THEN 'General' 
WHEN MISYSOrder.CaseGroupCode = 'O' THEN 'Other' 
ELSE 'Unknown' END END AS RequestedCaseType,
MISYSOrder.OrderExamCode, 
MISYSExam.Exam,
CASE WHEN ORDEREXAMCODE IN ('FBAEN', 'FBAME', 'FBAMF', 'FBASW', 'FPRCT', 'FVIDS', 
                      'FWASW', 'YSBCE') THEN ('Ba')  ELSE MISYSOrder.CaseTypeCode END AS ModalityCode, 
Modality.Modality, 
ISNULL(MISYSOrder.PriorityCode, N'Attention') AS PriorityCode, 
MISYSOrder.PlannedRequest,  
MISYSOrder.PatientTypeCode AS CurrentPatientType,
case when OrderingPhysician.Staff like ('A&E%') THEN ('A&E')
when OrderingPhysician.Staff like ('ASSESS%') THEN ('Assessment')
when MISYS.lkSpecTrue.SpecialtyTrue is null 
and rtrim(ltrim(left((SUBSTRING(OrderingPhysician.Staff, PATINDEX('%([a-z]%)', OrderingPhysician.Staff) + 1, 
                      LEN(OrderingPhysician.Staff) - CASE patindex('%([a-z]%)', OrderingPhysician.Staff) WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', OrderingPhysician.Staff) 
                      + 1 END)),2)))='GP'
                      THEN 'GP'
                      when MISYS.lkSpecTrue.SpecialtyTrue is null 
						and rtrim(ltrim(left((SUBSTRING(OrderingPhysician.Staff, PATINDEX('%([a-z]%)', OrderingPhysician.Staff) + 1, 
                      LEN(OrderingPhysician.Staff) - CASE patindex('%([a-z]%)', OrderingPhysician.Staff) WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', OrderingPhysician.Staff) 
                      + 1 END)),2)))<>'GP'
                      THEN 'Unknown/External'   
                      else MISYS.lkSpecTrue.SpecialtyTrue end as RequestingSource
FROM         
WHOLAP.dbo.BaseOrder AS MISYSOrder WITH (nolock) INNER JOIN
                      WHOLAP.dbo.OlapMISYSExam AS MISYSExam WITH (nolock) ON MISYSExam.ExamCode = MISYSOrder.OrderExamCode INNER JOIN
                      WHOLAP.dbo.OlapMISYSModality AS Modality WITH (nolock) ON Modality.ModalityCode = MISYSOrder.CaseTypeCode INNER JOIN
                      WHOLAP.dbo.OlapMISYSLocation AS OrderingLocation WITH (nolock) ON 
                      OrderingLocation.LocationCode = MISYSOrder.OrderingLocationCode INNER JOIN
                      WHOLAP.dbo.OlapMISYSStaff AS OrderingPhysician WITH (nolock) ON OrderingPhysician.StaffCode = MISYSOrder.OrderingPhysicianCode 
                      LEFT OUTER JOIN
                      MISYS.lkSpecTrue ON MISYS.lkSpecTrue.SpecialtyOrig = SUBSTRING(OrderingPhysician.Staff, PATINDEX('%([a-z]%)', OrderingPhysician.Staff) + 1, 
                      LEN(OrderingPhysician.Staff) - CASE patindex('%([a-z]%)', OrderingPhysician.Staff) WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', OrderingPhysician.Staff) 
                      + 1 END)
WHERE    

(MISYSOrder.PerformanceDate >= @DateFrom and MISYSOrder.performancedate<=@DateTo) and
MISYSOrder.PerformanceDate>'2011-12-31'
and (MISYSOrder.CaseTypeCode IN ('M', 'C', 'U', 'X','F','I')) 
 
 and OrderNumber not in 
 ('07C4066',
'07C4476',
'07C5410',
'07C5411',
'07C6384',
'07C6431',
'07C6768',
'07C6769',
'07C6770',
'07C6771',
'07C6772',
'07C7108',
'07C7631',
'07C7632',
'07C7633',
'07C7646',
'07C7650',
'07C7651',
'07C7655',
'07C7873',
'07C7874',
'07C8032',
'07C8356',
'07C8364',
'07C9088',
'07C9124',
'07C9125',
'07C9200',
'07C9202',
'07C9203',
'07C9292',
'07C9839',
'07C9840',
'07C9841',
'07C9845',
'07M2013',
'07M2931',
'07M2932')

 
 
  
 
 AND (NOT EXISTS
                          (SELECT     1 AS Expr1
                            FROM          MISYS.CancelledOrder AS canc
                            WHERE      (MISYSOrder.OrderNumber = OrderNumber))) AND 
           (MISYSOrder.PerformanceLocation IS NOT NULL) 
                      AND (MISYSOrder.OrderDate > '2008-12-31') 
                      
             
                    --AND MISYSOrder.OrderingLocationCode='GP'
                      AND MISYSOrder.DictationDate IS NULL
                      
                      
                      AND
                      
                      case when OrderingPhysician.Staff like ('A&E%') THEN ('A&E')
when OrderingPhysician.Staff like ('ASSESS%') THEN ('Assessment')
when
MISYS.lkSpecTrue.SpecialtyTrue is null 
and rtrim(ltrim(left((SUBSTRING(OrderingPhysician.Staff, PATINDEX('%([a-z]%)', OrderingPhysician.Staff) + 1, 
                      LEN(OrderingPhysician.Staff) - CASE patindex('%([a-z]%)', OrderingPhysician.Staff) WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', OrderingPhysician.Staff) 
                      + 1 END)),2)))='GP'
                      THEN 'GP'
                      
                    
when MISYS.lkSpecTrue.SpecialtyTrue is null 
and rtrim(ltrim(left((SUBSTRING(OrderingPhysician.Staff, PATINDEX('%([a-z]%)', OrderingPhysician.Staff) + 1, 
                      LEN(OrderingPhysician.Staff) - CASE patindex('%([a-z]%)', OrderingPhysician.Staff) WHEN 0 THEN 0 ELSE patindex('%([a-z]%)', OrderingPhysician.Staff) 
                      + 1 END)),2)))<>'GP'
                      THEN 'Unknown/External'   
                      
 else MISYS.lkSpecTrue.SpecialtyTrue end='GP'
 
 order by MISYSOrder.performanceDate asc
                      
