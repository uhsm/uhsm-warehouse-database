﻿CREATE procedure [dbo].[ReportMISYSMonth]

 


as



select distinct 
MonthNo=month(MISYSOrder.OrderDate),
	Monthsequence=case when month(MISYSOrder.OrderDate)=1 then 13
		when month(MISYSOrder.OrderDate)=2 then 14
		when month(MISYSOrder.OrderDate)=3 then 15
		else month(MISYSOrder.OrderDate) end,
MonthOrderedName=case when month(MISYSOrder.OrderDate)=1 then ('Jan')
		when month(MISYSOrder.OrderDate)=2 then ('Feb')
		when month(MISYSOrder.OrderDate)=3 then ('Mar')
		when month(MISYSOrder.OrderDate)=4 then ('Apr')
		when month(MISYSOrder.OrderDate)=5 then ('May')
		when month(MISYSOrder.OrderDate)=6 then ('Jun')
		when month(MISYSOrder.OrderDate)=7 then ('Jul')
		when month(MISYSOrder.OrderDate)=8 then ('Aug')
		when month(MISYSOrder.OrderDate)=9 then ('Sep')
		when month(MISYSOrder.OrderDate)=10 then ('Oct')
		when month(MISYSOrder.OrderDate)=11 then ('Nov')
		when month(MISYSOrder.OrderDate)=12 then ('Dec')
		else ('Other') end
	from
		WHOLAP.dbo.BaseOrder MISYSOrder with (nolock)
		
		WHERE (MISYSOrder.OrderDate > '2009-03-31')
group by
month(MISYSOrder.OrderDate),
	case when month(MISYSOrder.OrderDate)=13 then ('Jan')
		when month(MISYSOrder.OrderDate)=14 then ('Feb')
		when month(MISYSOrder.OrderDate)=15 then ('Mar')
		when month(MISYSOrder.OrderDate)=4 then ('Apr')
		when month(MISYSOrder.OrderDate)=5 then ('May')
		when month(MISYSOrder.OrderDate)=6 then ('Jun')
		when month(MISYSOrder.OrderDate)=7 then ('Jul')
		when month(MISYSOrder.OrderDate)=8 then ('Aug')
		when month(MISYSOrder.OrderDate)=9 then ('Sep')
		when month(MISYSOrder.OrderDate)=10 then ('Oct')
		when month(MISYSOrder.OrderDate)=11 then ('Nov')
		when month(MISYSOrder.OrderDate)=12 then ('Dec')
		else ('Other') end
		
		order by 
		case when month(MISYSOrder.OrderDate)=1 then 13
		when month(MISYSOrder.OrderDate)=2 then 14
		when month(MISYSOrder.OrderDate)=3 then 15
		else month(MISYSOrder.OrderDate) end asc
		
		
		
		
	