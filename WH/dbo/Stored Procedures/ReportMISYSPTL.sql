﻿
CREATE procedure [dbo].[ReportMISYSPTL]

/**********************************************************************************
Change Log:

	
	
	
	Current Version Date: 2011-08-19
			Changed Booking Status and Breach Status definitions
	


	2011-08-15
			Changed the definition of to any order received that has no
				DictationDate
				and no FinalDate 
				and no PerformanceLocation


	2011-08-12
			Changed Criteria to define wait as <= @CensusDate rather than <@CensusDate
	
	2011-08-04
			Changed Definition of BreachValue, (WaitTime on PTL Report).
			Added some more exam codes into Specialist Group

	2011-04-01
			Added PCT Code

	2011-03-27
			Added Indicator for Patients with Orders in other Modality
			Added Target Date
			Updated Rules for Breach Date
	
	2011-03-21
			Added AgeAtPerformanceDate
			Updated Criteria for waiting where PerformanceTime < OrderTime
			Added BreachDate
			Added BreachValue
			Removed Parameter Exam Group
			Location Category Parameter (Added by IS on 23-03-2011)
	
	2011-03-14
			Added Planned Request Date
	
	2011-03-11
	Change:
			Updated Requested Case Type
			and Booking Status
			Added Priority
	
	
	2011-02-15
	Change:
			Added Fields:
				Requested Case Type
				Booking Status
			Changed Selection Criteria:
			Include Where Ordertime = PeformanceTime
			Peformancetime is less than ordered time 
			(For data quality purposed where orderedTime is less than 2011-01-01)

**********************************************************************************/


	@censusDate as smalldatetime
	,@modalityCode as varchar(10)
	,@orderExamCode as varchar(10)
	,@orderPhysicianCode as varchar(10)
	,@BookingType as varchar(15)
	,@Priority as varchar(10)
	,@LocationCat as varchar(10)
	,@CaseType as varchar(50)
	,@BreachStat as varchar(50)
    ,@OtherModality as varchar(5)

as




/*
	set @censusDate = '2011-08-15'

	Set @modalityCode = '0'
	Set @orderExamCode = '0'
	Set @orderPhysicianCode ='0'
	Set @BookingType ='0'
	Set	@Priority ='0'
	Set @LocationCat = '0'
	Set @CaseType = 'General'
	Set @BreachStat = '0'
    Set @OtherModality = '0'
*/


select distinct --distinct because there can be multiple other orders per patient
		 MISYSOrder.OrderNumber
		,MISYSOrder.OrderTime
		,MISYSOrder.OrderDate
		,MISYSOrder.OrderingLocationCode
		,OrderingLocation = OrderingLocation.Location
		,OrderingLocationCategory = OrderingLocation.LocationCategory
		,MISYSOrder.CaseGroupCode

--Updated KD 2011-08-11 uses CaseGroupCode from BaseOrder
		,RequestedCaseType =
				Case When MISYSOrder.CaseTypeCode = 'X' Then 'Other'
				Else
					Case 
						When MISYSOrder.CaseGroupCode = 'S' Then 'Specialist'
						When MISYSOrder.CaseGroupCode = 'G' Then 'General'	
						When MISYSOrder.CaseGroupCode = 'O' Then 'Other'
						Else 'Unknown'
					End
				End
/*
--Updated KD 2011-03-11
		,RequestedCaseType =
				Case When (
				MISYSOrder.CaseTypeCode IN ('B','H','T','Z','O','R')
				OR
				MISYSOrder.ExamCode1 IN (
										'IFACJJ',
										'FINJTJ',
										'FHIPLJ',
										'FHIPRJ',
										'FJOINJ',
										'FABDO',
										'FDIAP',
										'FLOLB',
										'FLOLL',
										'FLOLR',
										'FLSPN',
										'FUPLB',
										'FUPLL',
										'FUPLR',
										'FHIPLM',
										'FHIPRM',
										'FLSPNM',
										'FCYST',
										'MAAOT',
										'MACOA',
										'MCVIA',
										'MCCMS',
										'MCORP',
										'MCRPS',
										'MCSFS',
										'MCVFS',
										'MCVVS',
										'MCORV'
										)
				OR 
				(MISYSOrder.ExamCode1 = 'MAAOT' AND (OrderingPhysician.Staff like '%(CARDIO)%' OR OrderingPhysician.Staff like '%(THOR)%'))
				) Then 'Specialist'
			Else
				'General'
			End
*/
		--Updated KD 2011-03-11
		/*,BookingStatus = 
			Case When MISYSOrder.ExamDescription = 'US TRANSRECTAL PROSTATE AND BIOPSY' Then
				'Attention'
			Else
				Case When MISYSOrder.OrderScheduledFlag = 'Y' Then
					'Booked'
				Else
					'Not Booked'
				End
			End*/





-- 2011-08-19 KD Depricated as OrderScheduledFlag seems to make more sense
		/*,BookingStatus = 
			Case When MISYSOrder.PerformanceTime = MISYSOrder.OrderTime*/
			
-- 2013-03-27 JC Replaced above with the following as there were Performance times coming through 1 minute after order time
,BookingStatus = 
			Case When MISYSOrder.PerformanceTime between MISYSOrder.OrderTime and DATEADD(minute,5,MISYSOrder.OrderTime)

			
			
-- 2012-12-02 JC Added below to show Pends where not genuinely booked as unbooked			
			OR (MISYSOrder.PriorityCode='PEND' AND MISYSOrder.CreateLinkScheduleDate is null)
			or MISYSOrder.OrderScheduledFlag='N'--jc added in 30/08/2013 
			 Then
				'Not Booked'
			Else
				Case When
					(
					MISYSOrder.OrderDate < @censusDate
					and (MISYSOrder.PerformanceTime < MISYSOrder.OrderTime)
					and MISYSOrder.PerformanceTime > '2011-01-01'
					)
					OR
					(
					MISYSOrder.ExamDescription = 'US TRANSRECTAL PROSTATE AND BIOPSY'
					)
					Then
				'Attention'
				Else
					Case when MISYSOrder.PerformanceTime > MISYSOrder.OrderTime then
						'Booked'
					Else 
						'Other'
					End
				End
			End
		,PatientContact
		,MISYSOrder.OrderScheduledFlag
		,MISYSOrder.OrderingPhysicianCode
		,OrderingPhysician = OrderingPhysician.Staff
		,MISYSOrder.OrderExamCode
		,MISYSExam.Exam
		,MISYSOrder.ExamGroupCode
		,MISYSExam.ExamGroup
		,MISYSOrder.DepartmentCode
		,MISYSExam.Department
		,MISYSOrder.PatientNo
		,MISYSOrder.PatientName
		,MISYSOrder.DateOfBirth
		,MISYSOrder.RegisteredGpPracticeCode
		,ModalityCode = MISYSOrder.CaseTypeCode
		,Modality.Modality
		,MISYSOrder.PerformanceTime
		,DATEADD(SECOND, CASE
                 WHEN Isnumeric(MISYSOrder.PerformanceTimeInt) = 0 THEN 0
                 ELSE CONVERT(INT, LEFT(RIGHT('0000' + MISYSOrder.PerformanceTimeInt, 4), 2)) * 3600 + CONVERT(INT, RIGHT(RIGHT('0000' + MISYSOrder.PerformanceTimeInt, 4), 2)) * 60
               END, MISYSOrder.PerformanceDate) AS PerformanceTimeNew
		,MISYSOrder.ScheduleDate
		,MISYSOrder.CreationDate
		,MISYSOrder.RadiologistCode
		,Radiologist = Radiologist.Staff
		,TechnicianCode = MISYSOrder.FilmTechCode
		,Technician.Technician
		--Added 2011-03-11 KD
		,PriorityCode = IsNUll(MISYSOrder.PriorityCode,'Attention')
		,MISYSOrder.PlannedRequest
		--Added 2011-03-21 KD
		,AgeAtPerformanceDate =
		Case When datediff(WEEK,MISYSOrder.DateOfBirth, MISYSOrder.PerformanceDate) < 53 
		Then cast(datediff(WEEK,MISYSOrder.DateOfBirth, MISYSOrder.PerformanceDate) as varchar(5))
		+ 'w'
		Else
		cast(datediff(YY, MISYSOrder.DateOfBirth, MISYSOrder.PerformanceDate) 
		-
		case 
		when (
			month(MISYSOrder.DateOfBirth) = month(MISYSOrder.PerformanceDate) 
			AND day(MISYSOrder.DateOfBirth) > day(MISYSOrder.PerformanceDate)
			OR month(MISYSOrder.DateOfBirth) > month(MISYSOrder.PerformanceDate)
			)
		then 1 else 0 end as varchar(5))
		End
		--Added 2011-03-27 KD
	,TargetDate = 
	Case 
		When MISYSOrder.PriorityCode = 'HSC205' then 
			case 
			when MISYSOrder.OrderDate <'2011-04-01' then 
				MISYSOrder.OrderDate + 14
			else
				MISYSOrder.OrderDate + 5
			End
		Else MISYSOrder.OrderDate + 28
	End
--Updated 2011-03-21 KD	
	,BreachDate = 
	Case 
		When MISYSOrder.PriorityCode = 'HSC205' then 
			case 
			when MISYSOrder.OrderDate <'2011-04-01' then 
				MISYSOrder.OrderDate + 14
			else
				MISYSOrder.OrderDate + 5
			End
		Else MISYSOrder.OrderDate + 42
	End

--2011-08-04 KD Breach Value ([Wait(Weeks)] on report) redefined as PerformanceTime - OrderTime
	,BreachValue = 
			Case When (cast(DATEDIFF(day,MISYSOrder.OrderDate, MISYSOrder.PerformanceDate) as decimal(10,2)) / 7) <= 0 
			Then  0
			
			when misysorder.OrderScheduledFlag='N' 
			then (cast(DATEDIFF(day,MISYSOrder.OrderDate, cast(GETDATE() as date)) as decimal(10,2)) / 7)--jc added 30/08/2013 
			Else
			(cast(DATEDIFF(day,MISYSOrder.OrderDate, MISYSOrder.PerformanceDate) as decimal(10,2)) / 7)
			End
-- Superceded 2011-08-04,BreachValue = (cast(DATEDIFF(day,MISYSOrder.OrderDate, @censusDate) as decimal(10,2)) / 7)
	,Comments = Questionnaire.Comments
--Added 2011-04-01 IS
	,PCT = MISYSOrder.PCTCode
--Added 2011-03-27 KD	




	,BreachStatus = 
--Pending
		Case when MISYSOrder.PlannedRequest like '%Y%' AND MISYSOrder.PriorityCode = 'PEND' Then 
			'Pending'
		Else
--Attention
		Case When (MISYSOrder.PerformanceTime < MISYSOrder.ApprovedTime and MISYSOrder.ApprovedTime >= '2011-01-01') Then
			'Attention'
		Else
		
--Unbooked. Breach
			Case When (MISYSOrder.PerformanceTime = MISYSOrder.OrderTime) Then
				Case When 
							Case 
								When MISYSOrder.PriorityCode = 'HSC205' then 
									MISYSOrder.OrderDate + 6
								Else 
									MISYSOrder.OrderDate + 43
							End <= CAST(getdate() as DATE)

				Then 'Unbooked Breach'
				Else
					'Unbooked'
				End

		Else
--Booked Breach
			Case When MISYSOrder.PerformanceTime not between MISYSOrder.OrderTime and DATEADD(minute,5,MISYSOrder.OrderTime) Then
				Case When 
							Case 
								When MISYSOrder.PriorityCode = 'HSC205' then 
									MISYSOrder.OrderDate + 6
								Else 
									MISYSOrder.OrderDate + 43
							End <= CAST(getdate() as DATE) 	Then 

						'Booked Breach'
						Else 
						
						'Booking Okay'
						
						End
				End
	
		End
	
	End
End





,HasOtherModality = Case When OtherOrder.OrderNumber Is Null Then 'N' Else 'Y' End
,DM01Indicator = 
	Case
		When MISYSOrder.ExamDescription = 'Barium Enema' Then 1
		When MISYSOrder.CaseTypeCode in ('C','M','U','D','B','T','H','V') Then 1
		Else 0
	End
,CurrentPatientType = MISYSOrder.PatientTypeCode
	from
		WHOLAP.dbo.BaseOrder MISYSOrder with (nolock)

	inner join WHOLAP.dbo.OlapMISYSExam MISYSExam with (nolock)
	on	MISYSExam.ExamCode = MISYSOrder.OrderExamCode

	inner join WHOLAP.dbo.OlapMISYSModality Modality with (nolock)
	on	Modality.ModalityCode = MISYSOrder.CaseTypeCode

	inner join WHOLAP.dbo.OlapMISYSLocation OrderingLocation with (nolock)
	on	OrderingLocation.LocationCode = MISYSOrder.OrderingLocationCode

	inner join WHOLAP.dbo.OlapMISYSStaff OrderingPhysician with (nolock)
	on	OrderingPhysician.StaffCode = MISYSOrder.OrderingPhysicianCode

	inner join WHOLAP.dbo.OlapMISYSStaff Radiologist with (nolock)
	on	Radiologist.StaffCode = MISYSOrder.RadiologistCode

	inner join WHOLAP.dbo.OlapMISYSTechnician Technician with (nolock)
	on	Technician.TechnicianCode = coalesce(MISYSOrder.FilmTechCode, -1)

	left join MISYS.Questionnaire Questionnaire with (nolock)
	on MISYSOrder.OrderNumber = Questionnaire.OrderNumber
/*	
	left outer join dbo.EntityLookup SpecialistExam with (nolock)
	on MISYSOrder.ExamCode1 = SpecialistExam.EntityCode
	and SpecialistExam.EntityTypeCode = 'MISYSSPECIALISTEXAMCODE'
*/

left join WHOLAP.dbo.BaseOrder OtherOrder
on						
						MisysOrder.PatientNo = OtherOrder.PatientNo
						and MisysOrder.CaseTypeCode <> OtherOrder.CaseTypeCode
						and (not OtherOrder.CaseTypeCode in 	('O','R','X','B','D','H','T','Z'))
						and
						(
						(
							OtherOrder.OrderDate < @censusDate
						and	OtherOrder.PerformanceDate >= @censusDate
						)
						Or 
						(
						OtherOrder.OrderDate < @censusDate
						and OtherOrder.OrderTime = OtherOrder.PerformanceTime
						and (OtherOrder.DictationDate is null and OtherOrder.FinalDate is null and OtherOrder.PerformanceLocation is null)
						)
						Or 
						(
						OtherOrder.OrderDate < @censusDate
						and (OtherOrder.PerformanceTime < OtherOrder.OrderTime)
						and (OtherOrder.DictationDate is null and OtherOrder.FinalDate is null and OtherOrder.PerformanceLocation is null)
						and OtherOrder.PerformanceTime > '2011-01-01'
						)
						)

	where

	(
	(
		MISYSOrder.OrderDate <= @censusDate
	and	MISYSOrder.PerformanceDate >= @censusDate
	
	or 
	(
	MISYSOrder.OrderDate <= @censusDate
	and MISYSOrder.DictationDate is null
	and MISYSOrder.FinalDate is null
	and MISYSOrder.PerformanceLocation is null

	)

	)
/*  Depricated KD 2011-08-15
	Or 
	--Added KD 2011-02-15
	(
	MISYSOrder.OrderDate < @censusDate
	and MISYSOrder.OrderTime = MISYSOrder.PerformanceTime
	and (MISYSOrder.DictationDate is null and MISYSOrder.FinalDate is null and MISYSOrder.PerformanceLocation is null)
	)
	Or 
	--Added KD 2011-02-15
	(
	MISYSOrder.OrderDate < @censusDate
	and (MISYSOrder.PerformanceTime < MISYSOrder.OrderTime)
	--Added KD 2011-03-22
	and (MISYSOrder.DictationDate is null and MISYSOrder.FinalDate is null and MISYSOrder.PerformanceLocation is null)
	and MISYSOrder.PerformanceTime > '2011-01-01'
	)
*/
	)

	and	
	(
			whreporting.dbo.RemoveNonDisplayChars(MISYSOrder.CaseTypeCode) = @modalityCode
		or	@modalityCode = '0'
		)

	/* Removed KD 2011-03-22
	and	(
			MISYSOrder.ExamGroupCode = @examGroupCode
		or	@examGroupCode = '0'
		)
	*/
	and	(
			whreporting.dbo.RemoveNonDisplayChars(MISYSOrder.OrderExamCode) = @orderExamCode
		or	@orderExamCode = '0'
		)


	and	(
			whreporting.dbo.RemoveNonDisplayChars(MISYSOrder.OrderingPhysicianCode) = @orderPhysicianCode
		or	@orderPhysicianCode = '0'
		)
	and	(
			isnull(whreporting.dbo.RemoveNonDisplayChars(MISYSOrder.PriorityCode),'Attention') = @Priority
		or	@Priority = '0'
		)
	and	(
			@BookingType = 
			
	--2012-12-02 JC Changed the below so it is in line with the changed in the select
			/*Case When MISYSOrder.ExamDescription = 'US TRANSRECTAL PROSTATE AND BIOPSY' Then
				'Attention'
			Else
				Case When MISYSOrder.OrderScheduledFlag = 'Y' Then
					'Booked'
				Else
					'Not Booked'
				End
			End*/
			
			Case When MISYSOrder.PerformanceTime = MISYSOrder.OrderTime
			
-- 2012-12-02 JC Added below to show Pends where not genuinely booked as unbooked			
			OR (MISYSOrder.PriorityCode='PEND' AND MISYSOrder.CreateLinkScheduleDate is null)
			
			 Then
				'Not Booked'
			Else
				Case When
					(
					MISYSOrder.OrderDate < @censusDate
					and (MISYSOrder.PerformanceTime < MISYSOrder.OrderTime)
					and MISYSOrder.PerformanceTime > '2011-01-01'
					)
					OR
					(
					MISYSOrder.ExamDescription = 'US TRANSRECTAL PROSTATE AND BIOPSY'
					)
					Then
				'Attention'
				Else
					Case when MISYSOrder.PerformanceTime > MISYSOrder.OrderTime then
						'Booked'
					Else 
						'Other'
					End
				End
			End


		or	@BookingType = '0'
		)
	--Added 23-03-2011 IS	
	and	(
	--Altered 03-06-2011 KD changed from Ordering Location to PatientLocation
--			OrderingLocation.LocationCategory = @LocationCat
		whreporting.dbo.RemoveNonDisplayChars(MISYSOrder.PatientTypeCode) = @LocationCat
		or	@LocationCat = '0'
		)	

	and 
		(@CaseType = 
				Case When MISYSOrder.CaseTypeCode = 'X' Then 'Other'
				Else
					Case 
						When MISYSOrder.CaseGroupCode = 'S' Then 'Specialist'
						When MISYSOrder.CaseGroupCode = 'G' Then 'General'	
						When MISYSOrder.CaseGroupCode = 'O' Then 'Other'
						Else 'Unknown'
					End
				End
			
			or @CaseType = '0'
		)
		
		--Added 28-03-2011 IS      
	and 
--523
		(@BreachStat = 

		--Pending
				Case when MISYSOrder.PlannedRequest like '%Y%' AND MISYSOrder.PriorityCode = 'PEND' Then 
					'Pending'
				Else
		--Attention
				Case When (MISYSOrder.PerformanceTime < MISYSOrder.ApprovedTime and MISYSOrder.ApprovedTime >= '2011-01-01') Then
					'Attention'
				Else
				
		--Unbooked. Breach
					Case When (MISYSOrder.OrderScheduledFlag = 'N') Then
						Case When 
									Case 
										When MISYSOrder.PriorityCode = 'HSC205' then 
											MISYSOrder.OrderDate + 6
										Else 
											MISYSOrder.OrderDate + 43
									End <= CAST(getdate() as DATE)

						Then 'Unbooked Breach'
						Else
							'Unbooked'
						End

				Else
		--Booked Breach
					Case When (MISYSOrder.OrderScheduledFlag = 'Y') Then
						Case When 
									Case 
										When MISYSOrder.PriorityCode = 'HSC205' then 
											MISYSOrder.OrderDate + 6
										Else 
											MISYSOrder.OrderDate + 43
									End <= CAST(getdate() as DATE) 	Then 

								'Booked Breach'
								Else 
								
								'Booking Okay'
								
								End
						End
			
				End
			
			End
		End

	or @BreachStat = '0'
      )

--Added 28-03-2011 IS      
	and
		(@OtherModality = Case When OtherOrder.OrderNumber Is Null Then 'N' Else 'Y' End
			or @OtherModality = '0'
		)     
--Added 25/10/2012 JC - Requested by Claire Cook to remove Review exams from PTL		
	and MISYSOrder.CaseTypeCode<>'R'
	and MISYSOrder.OrderExamCode NOT in ('CREV','RMRI','REVM','RIMR','RICT')
	And not exists 
		(
		Select 1 
		from MISYS.CancelledOrder canc
		where MISYSOrder.OrderNumber = canc.OrderNumber
		)

