﻿
CREATE procedure [dbo].[ReportMISYSPTLSummary]

/**********************************************************************************
Change Log:
	
	Current Version Date:	2011-03-29 IS

**********************************************************************************/
--DECLARE
	@censusDate as smalldatetime
	,@modalityCode as varchar(10)
	,@orderExamCode as varchar(10)
	,@orderPhysicianCode as varchar(10)
	,@BookingType as varchar(15)
	,@Priority as varchar(10)
	,@LocationCat as varchar(10)
	,@CaseType as varchar(50)
	,@BreachStat as varchar(50)
    ,@OtherModality as varchar(5)

AS

/*
Set @censusDate = '2011-03-28'
Set @modalityCode = '0'
Set @orderExamCode = '0'
set @orderPhysicianCode = '0'
set @Priority = '0'
set @LocationCat = '0'
set @CaseType = '0'
set @BookingType = '0'
set @BreachStat = '0'
set @OtherModality = '0'
*/
	select distinct --distinct because there can be multiple other orders per patient
		 1 AS ForCount,
		 MISYSOrder.OrderNumber
		,MISYSOrder.OrderTime
		,MISYSOrder.OrderDate
		,MISYSOrder.OrderingLocationCode
		,OrderingLocation = OrderingLocation.Location
		,OrderingLocationCategory = OrderingLocation.LocationCategory
		--Updated KD 2011-03-11
		,RequestedCaseType =
				Case When (
				MISYSOrder.CaseTypeCode IN ('B','H','T','Z')
				OR
				MISYSOrder.ExamCode1 IN ('MCORP','MCRPS','MCSFS','MCVIA','MCVVS', 'MMAMB','NSENT','XDEXA')
				OR 
				(MISYSOrder.ExamCode1 = 'MAAOT' AND (OrderingPhysician.Staff like '%(CARDIO)%' OR OrderingPhysician.Staff like '%(THOR)%'))
				) Then 'Specialist'
			Else
				'General'
			End
		--Updated KD 2011-03-11
		,BookingStatus = 
			Case When MISYSOrder.PerformanceTime = MISYSOrder.OrderTime Then
				'Not Booked'
			Else
				Case When
					(
					MISYSOrder.OrderDate < @censusDate
					and (MISYSOrder.PerformanceTime < MISYSOrder.OrderTime)
					and MISYSOrder.PerformanceTime > '2011-01-01'
					)
					OR
					(
					MISYSOrder.ExamDescription = 'US TRANSRECTAL PROSTATE AND BIOPSY'
					)
					Then
				'Attention'
				Else
					Case when MISYSOrder.PerformanceTime > MISYSOrder.OrderTime then
						'Booked'
					Else 
						'Other'
					End
				End
			End
		,MISYSOrder.OrderingPhysicianCode
		,OrderingPhysician = OrderingPhysician.Staff
		,MISYSOrder.OrderExamCode
		,MISYSExam.Exam
		,MISYSOrder.ExamGroupCode
		,MISYSExam.ExamGroup
		,MISYSOrder.DepartmentCode
		,MISYSExam.Department
		,MISYSOrder.PatientNo
		,MISYSOrder.PatientName
		,MISYSOrder.DateOfBirth
		,MISYSOrder.RegisteredGpPracticeCode
		,ModalityCode = MISYSOrder.CaseTypeCode
		,Modality.Modality
		,MISYSOrder.PerformanceTime
		,MISYSOrder.CreationDate
		,MISYSOrder.RadiologistCode
		,Radiologist = Radiologist.Staff
		,TechnicianCode = MISYSOrder.FilmTechCode
		,Technician.Technician
		--Added 2011-03-11 KD
		,PriorityCode = IsNUll(MISYSOrder.PriorityCode,'Attention')
		,MISYSOrder.PlannedRequest
		--Added 2011-03-21 KD
		,AgeAtPerformanceDate =
		Case When datediff(WEEK,MISYSOrder.DateOfBirth, MISYSOrder.PerformanceDate) < 53 
		Then cast(datediff(WEEK,MISYSOrder.DateOfBirth, MISYSOrder.PerformanceDate) as varchar(5))
		+ 'w'
		Else
		cast(datediff(YY, MISYSOrder.DateOfBirth, MISYSOrder.PerformanceDate) 
		-
		case 
		when (
			month(MISYSOrder.DateOfBirth) = month(MISYSOrder.PerformanceDate) 
			AND day(MISYSOrder.DateOfBirth) > day(MISYSOrder.PerformanceDate)
			OR month(MISYSOrder.DateOfBirth) > month(MISYSOrder.PerformanceDate)
			)
		then 1 else 0 end as varchar(5))
		End
		--Added 2011-03-27 KD
	,TargetDate = 
	Case 
		When MISYSOrder.PriorityCode = 'HSC205' then 
			case 
			when MISYSOrder.OrderDate <'2011-04-01' then 
				MISYSOrder.OrderDate + 14
			else
				MISYSOrder.OrderDate + 5
			End
		Else MISYSOrder.OrderDate + 28
	End
--Updated 2011-03-21 KD	
	,BreachDate = 
	Case 
		When MISYSOrder.PriorityCode = 'HSC205' then 
			case 
			when MISYSOrder.OrderDate <'2011-04-01' then 
				MISYSOrder.OrderDate + 14
			else
				MISYSOrder.OrderDate + 5
			End
		Else MISYSOrder.OrderDate + 42
	End
	,BreachValue = (cast(DATEDIFF(day,MISYSOrder.OrderDate, @censusDate) as decimal(10,2)) / 7)
	,BreachCat4Weeks = Case
		When (cast(DATEDIFF(day,MISYSOrder.OrderDate, @censusDate) as decimal(10,2)) / 7) >=4 Then 1
		else 0
		End
	,BreachCat6Weeks = Case 
		When (cast(DATEDIFF(day,MISYSOrder.OrderDate, @censusDate) as decimal(10,2)) / 7) >=6 Then 1
		Else 0
		End
		 
	,Comments = Questionnaire.Comments
--Added 2011-03-27 KD	
	,BreachStatus = 
--Pending
		Case when MISYSOrder.PlannedRequest like '%Y%' Then 
			'Pending'
		Else
--Attention
		Case When (MISYSOrder.PerformanceTime < MISYSOrder.OrderTime and MISYSOrder.OrderDate >= '2011-01-01') Then
			'Attention'
		Else
		
--Unbooked. Breach
			Case When (MISYSOrder.PerformanceTime = MISYSOrder.OrderTime) Then
				Case When 
							Case 
								When MISYSOrder.PriorityCode = 'HSC205' then 
										case 
											when MISYSOrder.OrderDate <'2011-04-01' then 
											MISYSOrder.OrderDate + 14
										else
											MISYSOrder.OrderDate + 5
										End
								Else 
									MISYSOrder.OrderDate + 42
							End <= CAST(getdate() as DATE)
				Then 'Unbooked Breach'
--Unbooked. Breach Approaching
				Else
						Case When
							DateDiff(Day,CAST(getdate() as DATE),(Case 
								When MISYSOrder.PriorityCode = 'HSC205' then 
										case 
											when MISYSOrder.OrderDate <'2011-04-01' then 
											MISYSOrder.OrderDate + 14
										else
											MISYSOrder.OrderDate + 5
										End
								Else 
									MISYSOrder.OrderDate + 42
							End)) < 8
						Then 'Breach Approaching'
						
						Else
							
							'Unbooked'
							
						End
				End
		Else
--Booked Breach
				Case when MISYSOrder.PerformanceTime > MISYSOrder.OrderTime then
						Case When 
							Case
								When MISYSOrder.PriorityCode = 'HSC205' then 
										case 
											when MISYSOrder.OrderDate <'2011-04-01' then 
											MISYSOrder.OrderDate + 14
										else
											MISYSOrder.OrderDate + 5
										End
								Else 
									MISYSOrder.OrderDate + 42
								End < MISYSOrder.PerformanceTime 
							Then 'Booked Breach'
						Else 
						
						'Booking Okay'
						
						End
				End
	
		End
	
	End
End

,HasOtherModality = Case When OtherOrder.OrderNumber Is Null Then 'N' Else 'Y' End
/*
					Case When Exists (
					Select 1 
					From WHOLAP.dbo.BaseOrder OtherOrder
						Where
						MisysOrder.PatientNo = OtherOrder.PatientNo
						and MisysOrder.CaseTypeCode <> OtherOrder.CaseTypeCode
						and
						(
						(
							OtherOrder.OrderDate < @censusDate
						and	OtherOrder.PerformanceDate >= @censusDate
						)
						Or 
						(
						OtherOrder.OrderDate < @censusDate
						and OtherOrder.OrderTime = OtherOrder.PerformanceTime
						)
						Or 
						(
						OtherOrder.OrderDate < @censusDate
						and (OtherOrder.PerformanceTime < OtherOrder.OrderTime)
						and (OtherOrder.DictationDate is null or OtherOrder.FinalDate is null)
						and PerformanceTime > '2011-01-01'
						)
						)
					) Then 'Y'
					Else 'N'
					End
					


*/
		
	from
		WHOLAP.dbo.BaseOrder MISYSOrder

	inner join WHOLAP.dbo.OlapMISYSExam MISYSExam
	on	MISYSExam.ExamCode = MISYSOrder.OrderExamCode

	inner join WHOLAP.dbo.OlapMISYSModality Modality
	on	Modality.ModalityCode = MISYSOrder.CaseTypeCode

	inner join WHOLAP.dbo.OlapMISYSLocation OrderingLocation
	on	OrderingLocation.LocationCode = MISYSOrder.OrderingLocationCode

	inner join WHOLAP.dbo.OlapMISYSStaff OrderingPhysician
	on	OrderingPhysician.StaffCode = MISYSOrder.OrderingPhysicianCode

	inner join WHOLAP.dbo.OlapMISYSStaff Radiologist
	on	Radiologist.StaffCode = MISYSOrder.RadiologistCode

	inner join WHOLAP.dbo.OlapMISYSTechnician Technician
	on	Technician.TechnicianCode = coalesce(MISYSOrder.FilmTechCode, -1)

	left join MISYS.Questionnaire Questionnaire
	on MISYSOrder.OrderNumber = Questionnaire.OrderNumber
left join WHOLAP.dbo.BaseOrder OtherOrder
on						
						MisysOrder.PatientNo = OtherOrder.PatientNo
						and MisysOrder.CaseTypeCode <> OtherOrder.CaseTypeCode
						and
						(
						(
							OtherOrder.OrderDate < @censusDate
						and	OtherOrder.PerformanceDate >= @censusDate
						)
						Or 
						(
						OtherOrder.OrderDate < @censusDate
						and OtherOrder.OrderTime = OtherOrder.PerformanceTime
						and (OtherOrder.DictationDate is null and OtherOrder.FinalDate is null and OtherOrder.PerformanceLocation is null)
						)
						Or 
						(
						OtherOrder.OrderDate < @censusDate
						and (OtherOrder.PerformanceTime < OtherOrder.OrderTime)
						and (OtherOrder.DictationDate is null and OtherOrder.FinalDate is null and OtherOrder.PerformanceLocation is null)
						and OtherOrder.PerformanceTime > '2011-01-01'
						)
						)

	where

	(
	(
		MISYSOrder.OrderDate < @censusDate
	and	MISYSOrder.PerformanceDate >= @censusDate
	)
	Or 
	--Added KD 2011-02-15
	(
	MISYSOrder.OrderDate < @censusDate
	and MISYSOrder.OrderTime = MISYSOrder.PerformanceTime
	and (MISYSOrder.DictationDate is null and MISYSOrder.FinalDate is null and MISYSOrder.PerformanceLocation is null)
	)
	Or 
	--Added KD 2011-02-15
	(
	MISYSOrder.OrderDate < @censusDate
	and (MISYSOrder.PerformanceTime < MISYSOrder.OrderTime)
	--Added KD 2011-03-22
	and (MISYSOrder.DictationDate is null and MISYSOrder.FinalDate is null and MISYSOrder.PerformanceLocation is null)
	and MISYSOrder.PerformanceTime > '2011-01-01'
	)
	)

	and	
	(
			MISYSOrder.CaseTypeCode = @modalityCode
		or	@modalityCode = '0'
		)

	/* Removed KD 2011-03-22
	and	(
			MISYSOrder.ExamGroupCode = @examGroupCode
		or	@examGroupCode = '0'
		)
	*/
	and	(
			MISYSOrder.OrderExamCode = @orderExamCode
		or	@orderExamCode = '0'
		)


	and	(
			MISYSOrder.OrderingPhysicianCode = @orderPhysicianCode
		or	@orderPhysicianCode = '0'
		)
	and	(
			isnull(MISYSOrder.PriorityCode,'Attention') = @Priority
		or	@Priority = '0'
		)
	and	(
			@BookingType = 
					Case When MISYSOrder.PerformanceTime = MISYSOrder.OrderTime Then
				'Not Booked'
			Else
				Case When
					(
					MISYSOrder.OrderDate < @censusDate
					and (MISYSOrder.PerformanceTime < MISYSOrder.OrderTime)
					and MISYSOrder.PerformanceTime > '2011-01-01'
					)
					OR
					(
					MISYSOrder.ExamDescription = 'US TRANSRECTAL PROSTATE AND BIOPSY'
					)
					Then
				'Attention'
				Else
					Case when MISYSOrder.PerformanceTime > MISYSOrder.OrderTime then
						'Booked'
					Else 
						'Other'
					End
				End
			End
		or	@BookingType = '0'
		)
	--Added 23-03-2011 IS	
	and	(
			OrderingLocation.LocationCategory = @LocationCat
		or	@LocationCat = '0'
		)	

	and 
		(@CaseType = 
			Case 
			When (
				MISYSOrder.CaseTypeCode IN ('B','H','T','Z')
				OR
				MISYSOrder.ExamCode1 IN ('MCORP','MCRPS','MCSFS','MCVIA','MCVVS', 'MMAMB','NSENT','XDEXA')
				OR 
				(MISYSOrder.ExamCode1 = 'MAAOT' AND (OrderingPhysician.Staff like '%(CARDIO)%' OR OrderingPhysician.Staff like '%(THOR)%'))
				) Then 'Specialist'
			Else
				'General'
			End
			
			or @CaseType = '0'
		)
		
		--Added 28-03-2011 IS      
	and 
		(@BreachStat =
--Pending
            Case when MISYSOrder.PlannedRequest like '%Y%' Then 
                  'Pending'
            Else
--Attention
            Case When (MISYSOrder.PerformanceTime < MISYSOrder.OrderTime and MISYSOrder.OrderDate >= '2011-01-01') Then
                  'Attention'
            Else
            
--Unbooked. Breach
                  Case When (MISYSOrder.PerformanceTime = MISYSOrder.OrderTime) Then
                        Case When 
                                          Case 
                                                When MISYSOrder.PriorityCode = 'HSC205' then 
                                                            case 
                                                                  when MISYSOrder.OrderDate <'2011-04-01' then 
                                                                  MISYSOrder.OrderDate + 14
                                                            else
                                                                  MISYSOrder.OrderDate + 5
                                                            End
                                                Else 
                                                      MISYSOrder.OrderDate + 42
                                          End <= CAST(getdate() as DATE)
                        Then 'Unbooked Breach'
--Unbooked. Breach Approaching
                        Else
                                    Case When
                                          DateDiff(Day,CAST(getdate() as DATE),(Case 
                                                When MISYSOrder.PriorityCode = 'HSC205' then 
                                                            case 
                                                                  when MISYSOrder.OrderDate <'2011-04-01' then 
                                                                  MISYSOrder.OrderDate + 14
                                                            else
                                                                  MISYSOrder.OrderDate + 5
                                                            End
                                                Else 
                                                      MISYSOrder.OrderDate + 42
                                          End)) < 8
                                    Then 'Breach Approaching'
                                    
                                    Else
                                          
                                          'Unbooked'
                                          
                                    End
                        End
            Else
--Booked Breach
                        Case when MISYSOrder.PerformanceTime > MISYSOrder.OrderTime then
                                    Case When 
                                          Case
                                                When MISYSOrder.PriorityCode = 'HSC205' then 
                                                            case 
                                                                  when MISYSOrder.OrderDate <'2011-04-01' then 
                                                                  MISYSOrder.OrderDate + 14
                                                            else
                                                                  MISYSOrder.OrderDate + 5
                                                            End
                                                Else 
                                                      MISYSOrder.OrderDate + 42
                                                End < MISYSOrder.PerformanceTime 
                                          Then 'Booked Breach'
                                    Else 
                                    
                                    'Booking Okay'
                                    
                                    End
                        End
      
            End
      
      End
End


	or @BreachStat = '0'
      )

--Added 28-03-2011 IS      
	and
		(@OtherModality = Case When OtherOrder.OrderNumber Is Null Then 'N' Else 'Y' End
			or @OtherModality = '0'
		)     
		
	And not exists 
		(
		Select 1 
		from MISYS.CancelledOrder canc
		where MISYSOrder.OrderNumber = canc.OrderNumber
		)

	order by
		MISYSOrder.OrderDate




