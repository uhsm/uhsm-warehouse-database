﻿CREATE procedure [dbo].[ReportMISYSRefOtherHosp]
  @DateFrom datetime
 ,@DateTo datetime
  
   
as  

select distinct
cast(CreationDate as date) as CreationDate
,cast(PerformanceDate as date) as PerformanceDate
,PatientName
,cast(DateOfBirth as date) as DateOfBirth
,SexCode
,replace(PatientNo1,';1','') as PatientNumber
,PatientAddress1
,Postcode
,SocialSecurityNo
,RegisteredGpPracticeCode
,replace(AttendingPhysician1,';1','') as AttendingPhysician
,OrderNumber
,ExamCode1
,baseorder.ExamDescription
,CaseGroupCode
,baseorder.CaseTypeCode
,PerformanceLocation
,OrderingLocationCode
,OrderingPhysicianCode
,PatientTypeCode
,DocumentStatusCode
,cast(DictationDate as date)as DictationDate
,cast(FinalDate as date)as FinalDate
,RadiologistCode
,l.Location_type_translation_description as [where]
,c.phys_name as [who]
,l.Location_description as [where2]
,c.[Which Hosp]
,s.[Interventional Procedure] as Interventional
,s.[HRG (JP)] as HRG
,c.phys_add1
,c.phys_add2
,c.phys_add3
,c.phys_add4
,ISNULL(c.Account,'Unknown') as Account
,p.price

 from WHOLAP.dbo.baseorder
 left outer join MISYS.lk_Finance_OrderingLocation l
 on ltrim(rtrim(BaseOrder.OrderingLocationCode))=ltrim(rtrim(l.location_code))
 left outer join MISYS.lk_Finance_Outside_Clinicians c
 on ltrim(rtrim(c.phys_code))=ltrim(rtrim(BaseOrder.OrderingPhysiciancode))
 left outer join MISYS.lk_Finance_Snowmed s
 on ltrim(rtrim(BaseOrder.ExamCode1))=ltrim(rtrim(s.[short code]))
 left outer join MISYS.lk_Finance_price p 
 on ltrim(rtrim(p.Examcode))=ltrim(rtrim(BASEOrder.Examcode1)) AND BaseOrder.CaseTypeCode=p.casetypecode
 where PerformanceDate between @DateFrom and @DateTo
 and c.[Outside Referral]='Yes'
 
and 
baseorder.ExamDescription not like ('%ERPC%')
and baseorder.ExamDescription not like ('%STENT%')
and baseorder.ExamDescription not like ('%TUBE%')
and baseorder.ExamDescription not like ('%INSERT%')
and baseorder.examdescription not like ('%GASTRO%')
and baseorder.examdescription not like ('%MDT%')
and baseorder.examdescription not like ('%CARDIAC%')
and PerformanceLocation is not null
 and baseorder.CaseTypeCode<>'Z'
 AND LTRIM(RTRIM(PatientSurname)) NOT IN ('TEST','TEST2','TESTER','TESTPATIENT','TEST-PATIENT','XXTEST PATIENT','XXTESTER')
  
 and OrderNumber not in( 
 
 
 select distinct ordernumber 
 
 from WHOLAP.dbo.baseorder left outer join MISYS.lk_Finance_OrderingLocation l
 on ltrim(rtrim(BaseOrder.OrderingLocationCode))=ltrim(rtrim(l.location_code))
 left outer join MISYS.lk_Finance_Outside_Clinicians c
 on ltrim(rtrim(c.phys_code))=ltrim(rtrim(BaseOrder.OrderingPhysiciancode))
 left outer join MISYS.lk_Finance_Snowmed s
 on ltrim(rtrim(BaseOrder.ExamCode1))=ltrim(rtrim(s.[short code]))
 left outer join MISYS.lk_Finance_price p 
 on ltrim(rtrim(p.Examcode))=ltrim(rtrim(BASEOrder.Examcode1)) AND BaseOrder.CaseTypeCode=p.casetypecode
 where PerformanceDate between @DateFrom and @DateTo
 and c.[Outside Referral]='Yes'
 
and
 
 (
 
 ([Which Hosp]='CMMC' AND phys_add1 like ('%Renal%'))
 
 
 or (phys_add1 like ('%Family History%'))
 
or ([Which Hosp]='TRAFFORD' AND phys_add1 like ('%Obs%'))
 
 or (l.Location_type_translation_description='GP')
 or (OrderingLocationCode='AE' and [Which Hosp]='CHRISTIE'))
 
 
 
 
 
 
 
 
 
 
 
 
 
 )
group by
 cast(CreationDate as date) 
,cast(PerformanceDate as date)
,PatientName
,cast(DateOfBirth as date)
,SexCode
,replace(PatientNo1,';1','')
,PatientAddress1
,Postcode
,SocialSecurityNo
,RegisteredGpPracticeCode
,replace(AttendingPhysician1,';1','')
,OrderNumber
,ExamCode1
,baseorder.ExamDescription
,CaseGroupCode
,baseorder.CaseTypeCode
,PerformanceLocation
,OrderingLocationCode
,OrderingPhysicianCode
,PatientTypeCode
,DocumentStatusCode
,cast(DictationDate as date)
,cast(FinalDate as date)
,RadiologistCode
,l.Location_type_translation_description 
,c.phys_name
,l.Location_description 
,c.[Which Hosp]
,s.[Interventional Procedure] 
,s.[HRG (JP)] 
,c.phys_add1
,c.phys_add2
,c.phys_add3
,c.phys_add4
,c.Account
,p.price
 

 
 order by PerformanceDate,replace(PatientNo1,';1',''),CaseTypeCode
 
 