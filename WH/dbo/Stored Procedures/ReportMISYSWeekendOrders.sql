﻿CREATE procedure [dbo].[ReportMISYSWeekendOrders]
  @DateFrom datetime
 ,@DateTo datetime
  ,@Modality varchar(25)
  ,@OrderingLocation varchar(50)
  ,@Location varchar(50)
   
as  



select 
PatientSurname,
PatientForename,
OrderingLocationCode,
LocationCode,
CaseType,
OrderNumber,
Created,
OrderDate,
OrderTime,
OrderExamCode,
PerformanceDate,
DictationDate,
1 as Orders,
PerformanceLocation
from
MISYS.Encounter Rad
inner join whreporting.lk.calendar cal on cal.TheDate=cast(Rad.OrderDate as DATE)
where cal.DayOfWeekKey in (5,6,7,1)
and (DayOfWeekKey=5 and OrderTime>cast(OrderDate+'16:59:59.000' as datetime)
or DayOfWeekKey=1 and OrderTime<cast(OrderDate+'09:00:00.000' as datetime)
or DayOfWeekKey=6
or DayOfWeekKey=7)
and orderdate between @DateFrom and @DateTo
and CaseType in (@Modality)
and OrderingLocationCode in (@OrderingLocation)
and LocationCode in (@Location)
