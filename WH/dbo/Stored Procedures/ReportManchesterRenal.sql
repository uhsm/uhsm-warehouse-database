﻿CREATE procedure [dbo].[ReportManchesterRenal] as

--this will be converted into a report at some point

select
	RadiologyOrder.PerformanceDate
	,RadiologyOrder.OrderingPhysicianCode
	,OrderingPhysician = Physician.Physician
	,RadiologyOrder.OrderingLocationCode
	,Location.Location
	,RadiologyOrder.PatientName
	,RadiologyOrder.DateOfBirth
	,RadiologyOrder.PatientNo
	,NHSNumber = RadiologyOrder.SocialSecurityNo
	,ExamCode = RadiologyOrder.ExamCode1
	,RadiologyOrder.ExamDescription
	,RadiologyOrder.CaseType
	,RadiologyOrder.ExamGroupCode
from
	MISYS.[Order] RadiologyOrder

left join MISYS.Physician
on	Physician.PhysicianCode = RadiologyOrder.OrderingPhysicianCode

left join MISYS.Location
on	Location.LocationCode = RadiologyOrder.OrderingLocationCode

where
	RadiologyOrder.PerformanceDate between '1 mar 2010' and '31 mar 2010'
and	RadiologyOrder.OrderingPhysicianCode in 
		(
		'C4404536'
		,'C4498533'
		,'MCVE'
		)

and	RadiologyOrder.PatientTypeCode not in ('1', 'IP')
and	RadiologyOrder.OrderingLocationCode not in ('EYE', 'GP')
and	RadiologyOrder.CaseTypeCode not in ('W', 'Z')

order by
	PatientName
