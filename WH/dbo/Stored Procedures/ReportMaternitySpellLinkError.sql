﻿create procedure ReportMaternitySpellLinkError as

select
	 MotherDistrictNo = Encounter.DistrictNo
	,MotherPatientForename = Encounter.PatientForename
	,MotherPatientSurname = Encounter.PatientSurname
	,MotherDateOfBirth = Encounter.DateOfBirth
	,MotherEpisodeStartDate = ProfessionalCareEpisode.START_DTTM

	,BirthDistrictNo = BirthEncounter.DistrictNo
	,BirthPatientForename = BirthEncounter.PatientForename
	,BirthPatientSurname = BirthEncounter.PatientSurname
	,BirthDateOfBirth = BirthEncounter.DateOfBirth
from
	APC.MaternitySpell

inner join RF.Encounter
on	Encounter.SourceUniqueID = MaternitySpell.ReferralSourceUniqueID

inner join Lorenzo.dbo.ProfessionalCareEpisode
on	ProfessionalCareEpisode.PRCAE_REFNO = MaternitySpell.EpisodeSourceUniqueID

inner join APC.Birth
on	Birth.MaternitySpellSourceUniqueID = MaternitySpell.SourceUniqueID

inner join APC.Encounter BirthEncounter
on	BirthEncounter.SourcePatientNo = Birth.SourcePatientNo
and	not exists
	(
	select
		1
	from
		APC.Encounter Previous
	where
		Previous.SourcePatientNo = BirthEncounter.SourcePatientNo
	and	Previous.AdmissionTime < BirthEncounter.AdmissionTime
	)

where
	not exists
	(
	select
		1
	from
		APC.Encounter
	where
		Encounter.SourceUniqueID = MaternitySpell.EpisodeSourceUniqueID
	)
order by
	EpisodeSourceUniqueID
