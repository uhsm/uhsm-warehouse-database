﻿CREATE procedure [dbo].[ReportPTLHotlistOP] 
	 @SpecialtyCode int = null
	,@CensusDate smalldatetime = null
	,@ConsultantCode int = null
	,@PCTCode varchar (5) = null
	,@QuarterStartDate as varchar(50) = null 
as

select 
	WL.BreachWeekNo

	,RowColour =
		case WL.BreachWeekNo
		when 0 then 'Red'
		when 1 then 'Orange'
		when 2 then 'Orange'
		when 3 then 'Orange'
		else 'White'
		end

	,WL.EncounterRecno

	,WL.SpecialtyCode

	,Specialty.Specialty

	,CensusDate = left(convert(varchar, WL.CensusDate, 113), 11)

	,Consultant =
		case 
		when upper(Operation.Coding) like '%PELVIC SERVICE' then '*' 
		when upper(Operation.Coding) like '%TGL' then '#'
		else '' 
		end +
		case 
			when WL.ConsultantCode is null then 'No Consultant'
			when WL.ConsultantCode = '' then 'No Consultant'
			else
				case 
					when Consultant.Surname is null  then convert(varchar, WL.ConsultantCode) + ' - No Description'
					else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
				end
		end

	,WL.DistrictNo

	,WL.PatientSurname

	,Operation.Coding


	,WL.DateOnWaitingList

	,WL.KornerWait

	,convert(int, coalesce(WL.KornerWait, 0) / 7) WeeksWaiting

-- add the number of days 'til the end of the month
	,WeeksWaitingAtMonthEnd =
		convert(int, (
			coalesce(WL.KornerWait, 0) + 
			datediff(day, WL.CensusDate, 
				'01 ' + datename(month, dateadd(month, 1, WL.CensusDate)) + ' ' + datename(year, dateadd(month, 1, WL.CensusDate))
			) -1) / 7)

	,AppointmentDate = WL.DerivedAppointmentDate

	,WL.BreachDate
	,EligibleStatusCode = null

	,BookedBeyondBreach =
		case when WL.BreachDate < WL.AppointmentDate then 1 else 0 end

	,NearBreach =
		case when datediff(month, WL.AppointmentDate, WL.BreachDate) < 2 then 1 else 0 end

	,Comment = ''

	,TCIBackgroundColour =
		case
	-- breach
		when WL.CensusDate > WL.BreachDate then 'red'
	-- booked beyond breach
		when WL.BreachDate < 
		case
		when	WL.FuturePatientCancelDate = WL.AppointmentDate then null
		when	WL.CancelledBy is not null then null
		when	WL.AppointmentStatusCode in ('DNA', 'CND') then null
		else	WL.AppointmentDate
		end then 'pink'
	-- urgent
		when 
		case
		when	WL.FuturePatientCancelDate = WL.AppointmentDate then null
		when	WL.CancelledBy is not null then null
		when	WL.AppointmentStatusCode in ('DNA', 'CND') then null
		else	WL.AppointmentDate
		end is null and datediff(day, WL.CensusDate, WL.BreachDate) <= 28 then 'blue'
	-- no tci
		when 
		case
		when	WL.FuturePatientCancelDate = WL.AppointmentDate then null
		when	WL.CancelledBy is not null then null
		when	WL.AppointmentStatusCode in ('DNA', 'CND') then null
		else	WL.AppointmentDate
		end is null then 'deepskyblue'
	--	DN CANCEL is black with white text
	-- default
		else ''
		end

	,ChoiceBackgroundColour = null

	,WL.PCTCode

	,WL.ReferralDate

	,WL.QM08StartWaitDate

	,WL.CancelledBy

	,WL.FuturePatientCancelDate

	,WL.SourceOfReferral

	,WL.DateOfBirth

	,SourceOfReferralTypeCode

	,SourceOfReferralType =
		case WL.SourceOfReferralTypeCode
		when 'GP' then 'GP'
		else 'Other'
		end 

	,WL.RTTPathwayID
	,WL.RTTStartDate
	,WL.RTTEndDate
	,RTTCurrentStatusCode = RTTStatus.NationalRTTStatusCode
	,WL.RTTCurrentStatusDate

	,RTTStatus.RTTStatus

from
	(
	select
		BreachPeriodDate =
			Calendar.LastDayOfWeek

		,BreachWeekNo =
			case
			when convert(int, Calendar.WeekNoKey) - (datepart(year, WL.CensusDate) * 100 + datepart(week, WL.CensusDate)) < 1 then 0 
			else convert(int, Calendar.WeekNoKey) - (datepart(year, WL.CensusDate) * 100 + datepart(week, WL.CensusDate))
			end

		,FuturePatientCancellationDate =
			case
			when WL.CancelledBy = 'P'
			then WL.FuturePatientCancelDate 
			else null
			end

		,DerivedAppointmentDate =
			case
			when	WL.FuturePatientCancelDate = WL.AppointmentDate then null
			when	WL.CancelledBy is not null then null
	--DNA or Patient Cancel
			when
				WL.AppointmentStatusCode in
				(
				 358		--Did Not Attend	DNA
				,2000724	--Patient Late / Not Seen	7
				,2003495	--Refused To Wait	RW
				,2004301	--Cancelled by Patient	CANBP
				,2870		--Cancelled By Patient	2
				)
			then null
			else WL.AppointmentDate
			end

		,AppointmentStatusCode =
			case
	--DNA or Patient Cancel
			when
				WL.AppointmentStatusCode in
				(
				 358		--Did Not Attend	DNA
				,2000724	--Patient Late / Not Seen	7
				,2003495	--Refused To Wait	RW
				)
			then 'DNA'
			when
				WL.AppointmentStatusCode in
				(
				 2004301	--Cancelled by Patient	CANBP
				,2870		--Cancelled By Patient	2
				)
			then 'CND'
			end


		,SourceOfReferralTypeCode = 
			case
			when SourceOfReferral.SourceOfReferralCode is not null
			then 'GP'
			else 'OTHER'
			end

		,SourceOfReferral =
			coalesce(
				 SourceOfReferral.SourceOfReferral
				,convert(varchar, WL.SourceOfReferralCode) + ' - No Description'
			)

		,DerivedBreachDate = WL.BreachDate

		,WL.EncounterRecno

		,WL.SpecialtyCode

		,WL.CensusDate

		,WL.ConsultantCode

		,WL.DistrictNo

		,WL.PatientSurname

		,WL.IntendedPrimaryOperationCode

		,WL.DateOnWaitingList

		,WL.KornerWait

		,WeeksWaiting = convert(int, coalesce(WL.KornerWait, 0) / 7.0)

		,WL.CancelledBy

		,WL.PCTCode

		,WL.SexCode

		,WL.DateOfBirth

		,WL.FuturePatientCancelDate

		,WL.RTTPathwayID

		,WL.RTTCurrentStatusCode
		,WL.RTTCurrentStatusDate

		--,RTTKornerWait = datediff(day, Breach.ClockStartDate, WL.CensusDate) - WL.SocialSuspensionDays 
		--,RTTWeeksWaiting = (datediff(day, Breach.ClockStartDate, WL.CensusDate) - Breach.SocialSuspensionDays) / 7 
		--,RTTTotalWeeksWaiting = datediff(day, dateadd(day, Breach.SocialSuspensionDays, Breach.ClockStartDate), WL.CensusDate) / 7 

		,WL.NationalBreachDate

		,WL.BreachDate

		,WL.AppointmentDate

		,WL.ReferralDate

		,WL.QM08StartWaitDate

		,WL.RTTStartDate

		,WL.RTTEndDate
	
	from
		OP.WaitingList WL

	inner join WH.Calendar Calendar
	on	Calendar.TheDate = WL.BreachDate

	left join PAS.SourceOfReferral SourceOfReferral
	on	SourceOfReferral.SourceOfReferralCode = WL.SourceOfReferralCode
	and	SourceOfReferral.NationalSourceOfReferralCode in ('3', '03')

	where
		WL.CensusDate = @CensusDate

	-- remove non-trust specialties
	and	not exists
		(
		select
			1
		from
			dbo.EntityLookup NonTrustSpecialty
		where
			NonTrustSpecialty.EntityCode = WL.SpecialtyCode
		and	NonTrustSpecialty.EntityTypeCode = 'NONTRUSTSPECIALTY'
		)

	) WL

left join PAS.Consultant Consultant 
on	Consultant.ConsultantCode = WL.ConsultantCode

left join PAS.Coding Operation
on	Operation.CodingLocalCode = WL.IntendedPrimaryOperationCode
and	Operation.CodingTypeCode = 'OPCS4'
and	Operation.ArchiveFlag = 0
and	not exists
	(
	select
		1
	from
		PAS.Coding Previous
	where
		Previous.CodingLocalCode = WL.IntendedPrimaryOperationCode
	and	Previous.CodingTypeCode = 'OPCS4'
	and	Previous.ArchiveFlag = 0
	and	Previous.CodingCode > Operation.CodingCode
	)

left join PAS.Specialty Specialty
on	Specialty.SpecialtyCode = WL.SpecialtyCode

left join PAS.RTTStatus RTTStatus
on	RTTStatus.RTTStatusCode = WL.RTTCurrentStatusCode

where
	(
		WL.FuturePatientCancelDate is null
	or	WL.FuturePatientCancelDate > WL.BreachPeriodDate
	)

and
	(
		WL.DerivedAppointmentDate is null
	or	(
			(
				WL.DerivedAppointmentDate > WL.BreachPeriodDate
			or	WL.DerivedAppointmentDate < dateadd(day, -4, WL.CensusDate)
			or
				(
					WL.DerivedAppointmentDate < dateadd(day, -4, WL.BreachPeriodDate)
				and	datepart(week, WL.DerivedAppointmentDate) = datepart(week, WL.BreachPeriodDate)
				and	datepart(year, WL.DerivedAppointmentDate) = datepart(year, WL.BreachPeriodDate)
				)
			)
		and	datepart(year, WL.DerivedAppointmentDate) * 100 + datepart(week, WL.DerivedAppointmentDate) != datepart(year, WL.CensusDate) * 100 + datepart(week, WL.CensusDate)
		)
	)

and	(
		WL.SpecialtyCode = @SpecialtyCode
	or	@SpecialtyCode is null
	)


and	(
		WL.ConsultantCode = @ConsultantCode
	or	@ConsultantCode is null
	)

and	(
		left(WL.PCTCode, 3) = @PCTCode
	or	@PCTCode is null
	)


order by
	Specialty.Specialty

	,case 
	when upper(Operation.Coding) like '%PELVIC SERVICE' then '*' 
	when upper(Operation.Coding) like '%TGL' then '#'
	else '' 
	end +
	case 
		when WL.ConsultantCode is null then 'No Consultant'
		when WL.ConsultantCode = '' then 'No Consultant'
		else
			case 
				when Consultant.Surname is null  then convert(varchar, WL.ConsultantCode) + ' - No Description'
				else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
			end
	end

	,WL.BreachDate
	,WL.PatientSurname
