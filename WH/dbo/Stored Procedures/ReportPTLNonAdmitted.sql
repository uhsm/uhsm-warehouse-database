﻿CREATE procedure [dbo].[ReportPTLNonAdmitted]
	 @SpecialtyCode varchar(10) = null
	,@CensusDate smalldatetime = null
	,@ConsultantCode varchar(20) = null
	,@PCTCode varchar(5) = null
	,@SourceOfReferralCode varchar(20) = null
	,@HospitalCancellationFilter bit = 0
as

--formerly known as: ReportPTLNewReviewOP

set dateformat dmy

declare @localCensusDate smalldatetime

select
	@localCensusDate = @CensusDate

select
	WL.EncounterRecno,

	WL.SpecialtyCode SpecialtyCode,

	Specialty.Specialty,

	WL.ConsultantCode,

	ServicePoint.ServicePointLocalCode SiteCode,

	left(convert(varchar, WL.CensusDate, 113), 11) CensusDate,

	case 
	when WL.SiteCode is null then 'Unknown Site' 
	when ServicePoint.ServicePoint is null then convert(varchar, WL.SiteCode) + ' - No Description' 
	else ServicePoint.ServicePoint
	end Site,

	case 
	when upper(WL.Operation) like '%PELVIC SERVICE' then '*' 
	when upper(WL.Operation) like '%TGL' then '#'
	else '' 
	end +
	case 
		when WL.ConsultantCode is null then 'No Consultant'
		when WL.ConsultantCode = '' then 'No Consultant'
		else
			case 
				when Consultant.Surname is null  then convert(varchar, WL.ConsultantCode) + ' - No Description'
				else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
			end
	end Consultant, 

	WL.DistrictNo,

	WL.PatientSurname,

	case
	when WL.IntendedPrimaryOperationCode is null then ''
	when WL.IntendedPrimaryOperationCode = '' then ''
	when Operation.Operation is null then convert(varchar, WL.IntendedPrimaryOperationCode) + ' - No Description'
	else convert(varchar, WL.IntendedPrimaryOperationCode) + ' - ' + Operation.Operation
	end + ' (' + coalesce(WL.Operation, '') + ')' Operation,

	convert(varchar, WL.IntendedPrimaryOperationCode) OperationCode,

	ManagementIntentionCode = null,

	PriorityCode = Priority.NationalPriorityCode,

	WL.DateOnWaitingList,

	WL.KornerWait,

	convert(int, coalesce(WL.KornerWait, 0) / 7) WeeksWaiting,

-- add the number of days 'til the end of the month
	convert(int, (
		coalesce(WL.KornerWait, 0) + 
		datediff(day, WL.CensusDate, 
			'01 ' + datename(month, dateadd(month, 1, WL.CensusDate)) + ' ' + datename(year, dateadd(month, 1, WL.CensusDate))
		) -1) / 7) WeeksWaitingAtMonthEnd,

-- 26 Oct 2005 - PN
-- when the FuturePatientCancelDate = AppointmentDate then blank the AppointmentDate
	case
	when	WL.FuturePatientCancelDate = WL.AppointmentDate then null
	when	WL.CancelledBy is not null then null
	when	WL.AppointmentStatusCode in (358) then null --Did Not Attend
	else	WL.AppointmentDate
	end AppointmentDate,

	WL.BreachDate,

	case when WL.BreachDate < WL.AppointmentDate then 1 else 0 end BookedBeyondBreach,

	case when datediff(month, WL.AppointmentDate, WL.BreachDate) < 2 then 1 else 0 end NearBreach,

	'' Comment,

	case
-- breach
	when WL.CensusDate > WL.BreachDate then 'red'
-- booked beyond breach
	when WL.BreachDate < 
	case
	when	WL.FuturePatientCancelDate = WL.AppointmentDate then null
	when	WL.CancelledBy is not null then null
	when	WL.AppointmentStatusCode in (358) then null --Did Not Attend
	else	WL.AppointmentDate
	end then 'pink'
-- urgent
	when 
	case
	when	WL.FuturePatientCancelDate = WL.AppointmentDate then null
	when	WL.CancelledBy is not null then null
	when	WL.AppointmentStatusCode in (358) then null --Did Not Attend
	else	WL.AppointmentDate
	end is null and datediff(day, WL.CensusDate, WL.BreachDate) <= 28 then 'blue'
-- no tci
	when 
	case
	when	WL.FuturePatientCancelDate = WL.AppointmentDate then null
	when	WL.CancelledBy is not null then null
	when	WL.AppointmentStatusCode in (358) then null --Did Not Attend
	else	WL.AppointmentDate
	end is null then 'deepskyblue'
--	DN CANCEL is black with white text
-- default
	else ''
	end TCIBackgroundColour,

	ChoiceBackgroundColour = null,

	WL.PCTCode,

	WL.ReferralDate,

	WL.QM08StartWaitDate,

	WL.CancelledBy,

	WL.FuturePatientCancelDate,

	coalesce(SourceOfReferral.SourceOfReferral, convert(varchar, WL.SourceOfReferralCode) + ' - No Description') SourceOfReferral,

	WL.DateOfBirth,

	case when NonTrustSpecialty.EntityCode is null then 'N' else 'Y' end NonTrustSpecialty,

	coalesce(WL.EpisodicGpPracticeCode, RegisteredGpPracticeCode) GpPracticeCode,

	WL.BreachDays

	,WL.RTTPathwayID
	,WL.RTTPathwayCondition
	,WL.RTTStartDate
	,WL.RTTEndDate
	,WL.RTTSpecialtyCode
	,WL.RTTCurrentProviderCode
	,RTTCurrentStatusCode = RTTStatus.NationalRTTStatusCode
	,RTTCurrentStatusDate = RTT.StartDate
	,WL.RTTCurrentPrivatePatientFlag
	,WL.RTTOverseasStatusFlag

	,RTTStatus.RTTStatus

	,CountOfDNAs
	,CountOfHospitalCancels
	,CountOfPatientCancels

from
	OP.WL WL

left join PAS.Consultant Consultant 
on	Consultant.ConsultantCode = WL.ConsultantCode

left join PAS.ServicePoint ServicePoint
on	ServicePoint.ServicePointCode = WL.SiteCode

left join Operation Operation
on	Operation.OperationCode = left(WL.IntendedPrimaryOperationCode, 3)

left join PAS.Specialty Specialty
on	Specialty.SpecialtyCode = WL.SpecialtyCode

left join PAS.SourceOfReferral SourceOfReferral
on	SourceOfReferral.SourceOfReferralCode = WL.SourceOfReferralCode

left join dbo.EntityLookup NonTrustSpecialty
on	NonTrustSpecialty.EntityCode = WL.SpecialtyCode
and	NonTrustSpecialty.EntityTypeCode = 'NONTRUSTSPECIALTY'

left join RTT.RTT RTT
on	RTT.SourceUniqueID = WL.SourceUniqueID

left join PAS.RTTStatus RTTStatus
on	RTTStatus.RTTStatusCode = RTT.RTTStatusCode

left join PAS.Priority Priority
on	Priority.PriorityCode = WL.PriorityCode

where
	WL.CensusDate = @localCensusDate
and	(
		WL.SpecialtyCode = @SpecialtyCode
	or	@SpecialtyCode is null
	)

and	(
		Consultant.ConsultantCode = @ConsultantCode
	or	@ConsultantCode is null
	)

and	(
		left(WL.PCTCode, 3) = @PCTCode
	or	@PCTCode is null
	)

and	(
		WL.SourceOfReferralCode = @SourceOfReferralCode
	or	@SourceOfReferralCode is null
	)

and (
		coalesce(WL.CountOfHospitalCancels, 0) > 2
	or	@HospitalCancellationFilter = 0
	)

order by
	WL.KornerWait desc,
	Specialty.Specialty,
	WL.SiteCode,
	case 
	when upper(WL.Operation) like '%PELVIC SERVICE' then '*' 
	when upper(WL.Operation) like '%TGL' then '#'
	else '' 
	end +
	case 
		when WL.ConsultantCode is null then 'No Consultant'
		when WL.ConsultantCode = '' then 'No Consultant'
		else
			case 
				when Consultant.Surname is null  then convert(varchar, WL.ConsultantCode) + ' - No Description'
				else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
			end
	end, 
	WL.PatientSurname
