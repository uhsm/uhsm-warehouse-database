﻿CREATE procedure [dbo].[ReportRTTDrillthrough] 
	@DrillThroughMDX varchar(8000) = null
	,@PeriodType varchar(10) = 'WEEKLY'
as

declare @sql varchar(8000)

select
	@DrillThroughMDX =
		coalesce(
			@DrillThroughMDX
	 
			,'drillthrough maxrows 10 select from [RTT]
			where
				(
				 [Adjusted].[Yes]
				,[Census].[Census].&[2010-01-03T00:00:00]
				)
			'
		)

select
	@sql =
'
select
	 OlapRTTDrillThrough.*
	,MaintenanceURL = null --populate this when required
	,DrillThrough.PathwayStatus
	,DrillThrough.DurationBand
	,DrillThrough.BreachBand
	,DrillThrough.ThisMonthsCases
	,DrillThrough.LastWeeksCases
	,DrillThrough.Specialty
	,DrillThrough.Consultant
	,DrillThrough.WeekBandCode
	,DrillThrough.Organisation
	,DurationWeek.DurationWeek
from
	WHOLAP.dbo.OlapRTTDrillThrough OlapRTTDrillThrough

inner join 
	(
	select

		 CensusDate = 
			convert(smalldatetime, convert(varchar, "[$RTT].[Census Date]"))

		,SourceEncounterRecno = 
			convert(int, convert(varchar, "[$RTT].[SourceEncounterRecno]"))

		,SourceCode =
			convert(varchar, "[$RTT].[SourceCode]")

		,PathwayStatus = 
			"[$Pathway Status].[Pathway Status]"

		,DurationBand =
			"[$Duration Weeks].[Duration Band]"

		,BreachBand =
			"[$Breach Band].[Breach Band]"

		,ThisMonthsCases =
			"[RTT].[This Months Cases]"

		,LastWeeksCases =
			"[RTT].[Last Weeks Cases]"

		,Specialty =
			"[$PAS Specialty].[Specialty]"

		,NationalSpecialty =
			"[$PAS Specialty].[Specialty Function]"

		,Consultant =
			"[$PAS Consultant].[Consultant]"

		,WeekBandCode =
			convert(varchar, "[$Duration Weeks].[Duration Weeks]")

		,Organisation =
			"[$PAS Practice].[PCT]"

		,KeyDate = 
			convert(smalldatetime, convert(varchar, "[$RTT].[Key Date]"))

	from
		openquery(WAREHOUSE
		,''
		' + @DrillThroughMDX + ' 


	return
		 key([$RTT].[Census Date])
		,key([$RTT].[SourceEncounterRecno])
		,key([$RTT].[SourceCode])
		,[$Pathway Status].[Pathway Status]
		,[$Duration Weeks].[Duration Band]
		,[$Breach Band].[Breach Band]
		,[$Measures].[Last Weeks Cases]
		,[$Measures].[This Months Cases]
		,[$PAS Specialty].[Specialty]
		,[$PAS Specialty].[Specialty Function]
		,[$PAS Consultant].[Consultant]
		,key([$Duration Weeks].[Duration Weeks])
		,[$PAS Practice].[PCT]
		,key([$RTT].[Key Date])

		''
		)
	) DrillThrough
on	DrillThrough.SourceEncounterRecno = OlapRTTDrillThrough.SourceEncounterRecno
and	DrillThrough.SourceCode = OlapRTTDrillThrough.SourceCode
and	DrillThrough.CensusDate = OlapRTTDrillThrough.CensusDate


left join WHOLAP.dbo.OlapDurationWeeks DurationWeek
on	DurationWeek.WaitDurationCode = DrillThrough.WeekBandCode

where 
	' + 
	case
	when @PeriodType = 'WEEKLY'
	then 'LastWeeksCases'
	else 'ThisMonthsCases'
	end +
	' > 0
'

exec (@sql)
