﻿CREATE procedure [dbo].[ReportRTTNonAdmitted]
	 @CensusDate smalldatetime
	,@SpecialtyCode int = null
	,@ConsultantCode int = null
	,@PCTCode varchar (5) = null
	,@RTTStatusCode varchar (10) = null
	,@ReviewedFlag bit = null
	,@Filter varchar(20) = null

as

--shows pending non-admitted patients
select
	 FactRTT.CensusDate
	,Referral.EncounterRecno ReferralRecno
	,Referral.RTTPathwayID
	,Referral.DistrictNo
	,FactRTT.SpecialtyCode
	,Specialty.Specialty
	,FactRTT.ConsultantCode
	,Consultant.Consultant
	,Referral.PatientSurname
	,Referral.DateOfBirth
	,Practice.PCTCode
	,Practice.PCT PCT
	,Referral.ReferralDate
	,Referral.RTTStartDate
	,RTTCurrentStatusCode = RTTStatus.RTTStatusCode
	,RTTStatus.RTTStatus
	,Referral.RTTCurrentStatusDate
	,FactRTT.DaysWaiting
	,FactRTT.DaysWaiting / 7 WeeksWaiting
	,null FuturePatientCancellationDate

	,BreachDate =
		DATEADD(
			DAY
			,(FactRTT.DaysWaiting * -1) + 126
			,FactRTT.KeyDate
		)

	,FirstAppointmentDate = FirstOutpatient.AppointmentDate

	--,FirstAppointmentDate =
	--(
	--select top 1
	--	Outpatient.AppointmentDate
	--from
	--	OP.Encounter Outpatient
	--where
	--	Outpatient.ReferralSourceUniqueID = Referral.SourceUniqueID
	--order by
	--	Outpatient.AppointmentDate asc
	--) 

	,FirstOutcomeCode = FirstOutpatient.AttendanceOutcomeCode

	--,FirstOutcomeCode =
	--(
	--select top 1
	--	Outpatient.AttendanceOutcomeCode
	--from
	--	OP.Encounter Outpatient
	--where
	--	Outpatient.ReferralSourceUniqueID = Referral.SourceUniqueID
	--order by
	--	Outpatient.AppointmentDate asc
	--) 

	,PreviousAppointmentDate = LastOutpatient.AppointmentDate

	--,PreviousAppointmentDate =
	--(
	--select top 1
	--	Outpatient.AppointmentDate
	--from
	--	OP.Encounter Outpatient
	--where
	--	Outpatient.ReferralSourceUniqueID = Referral.SourceUniqueID
	--and	Outpatient.AppointmentDate <= FactRTT.CensusDate
	--order by
	--	Outpatient.AppointmentDate desc
	--) 

	,PreviousOutcomeCode = LastOutpatient.AttendanceOutcomeCode

	--,PreviousOutcomeCode =
	--(
	--select top 1
	--	Outpatient.AttendanceOutcomeCode
	--from
	--	OP.Encounter Outpatient
	--where
	--	Outpatient.ReferralSourceUniqueID = Referral.SourceUniqueID
	--and	Outpatient.AppointmentDate <= FactRTT.CensusDate
	--order by
	--	Outpatient.AppointmentDate desc
	--)

	,Referral.NextFutureAppointmentDate

	,RTTBreachDate =
		DATEADD(
			DAY
			,(FactRTT.DaysWaiting * -1) + 126
			,FactRTT.KeyDate
		)

	,MaintenanceURL =
	(
	select
		TextValue
	from
		dbo.Parameter
	where
		Parameter = 'OPMAINTENANCEURL'
	)
	+ '?SourceUniqueID=' + convert(varchar, Referral.SourceUniqueID)
	+ '&CensusDate=' + convert(varchar, FactRTT.CensusDate)

	,WaitReason.WaitReason

	,RTTEncounter.WaitReasonDate

	,RTTEncounter.ClockStopComment

from
	WHOLAP.dbo.FactRTT FactRTT

inner join RF.Encounter Referral
on	Referral.EncounterRecno = FactRTT.SourceEncounterRecno

left join WHOLAP.dbo.OlapPASPractice Practice
on	Practice.PracticeCode = coalesce(Referral.EpisodicGpPracticeCode, Referral.RegisteredGpPracticeCode)

left join WHOLAP.dbo.OlapPASConsultant Consultant
on	Consultant.ConsultantCode = FactRTT.ConsultantCode

left join WHOLAP.dbo.OlapPASSpecialty Specialty
on	Specialty.SpecialtyCode = FactRTT.SpecialtyCode

--left join WHOLAP.dbo.OlapPCT PCT
--on	PCT.PCTCode = Practice.PCTCode

left join RTT.RTTStatus RTTStatus
on	RTTStatus.InternalCode = Referral.RTTCurrentStatusCode

left join RTT.Encounter RTTEncounter
on	RTTEncounter.SourceCode = 'REFERRAL'
and	RTTEncounter.SourceRecno = Referral.SourceUniqueID

left join RTT.WaitReason WaitReason
on	WaitReason.WaitReasonCode = RTTEncounter.WaitReasonCode

left join OP.Encounter FirstOutpatient
on	FirstOutpatient.ReferralSourceUniqueID = Referral.SourceUniqueID
and	not exists
	(
	select
		1
	from
		OP.Encounter Previous
	where
		Previous.ReferralSourceUniqueID = Referral.SourceUniqueID
	and	(
			Previous.AppointmentTime > FirstOutpatient.AppointmentTime
		or	(
				Previous.AppointmentTime = FirstOutpatient.AppointmentTime
			and	Previous.EncounterRecno > FirstOutpatient.EncounterRecno
			)
		)
	)

left join OP.Encounter LastOutpatient
on	LastOutpatient.ReferralSourceUniqueID = Referral.SourceUniqueID
and	LastOutpatient.AppointmentDate <= FactRTT.CensusDate
and	not exists
	(
	select
		1
	from
		OP.Encounter Previous
	where
		Previous.ReferralSourceUniqueID = Referral.SourceUniqueID
	and	Previous.AppointmentDate <= FactRTT.CensusDate
	and	(
			Previous.AppointmentTime < LastOutpatient.AppointmentTime
		or	(
				Previous.AppointmentTime = LastOutpatient.AppointmentTime
			and	Previous.EncounterRecno < LastOutpatient.EncounterRecno
			)
		)
	)

where
	FactRTT.PathwayStatusCode = 'P'
and	FactRTT.AdjustedFlag = 'true'
and	RTTStatus.ClockStopFlag = 'false'

and	FactRTT.CensusDate = @CensusDate

and	(
		FactRTT.SpecialtyCode = @SpecialtyCode
	or	@SpecialtyCode = 0
	)


and	(
		FactRTT.ConsultantCode = @ConsultantCode
	or	@ConsultantCode = 0
	)

and	(
		Practice.PCTCode = @PCTCode
	or	@PCTCode = '0'
	)

and	(
		Referral.RTTCurrentStatusCode = @RTTStatusCode
	or	@RTTStatusCode is null
	)

and	(
		coalesce(RTTEncounter.ReviewedFlag, convert(bit, 0)) = @ReviewedFlag
	or	@ReviewedFlag is null
	)

and	(
		(
			FactRTT.DaysWaiting > 126
		and	@Filter = 'LONGWAITER'
		)
		or	@Filter is null
	)

--and	Referral.ReferralDate >= '1 jan 2007'

order by
	DATEADD(
		DAY
		,(FactRTT.DaysWaiting * -1) + 126
		,FactRTT.KeyDate
	)

