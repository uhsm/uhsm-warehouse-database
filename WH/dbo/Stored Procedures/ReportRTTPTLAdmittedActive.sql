﻿CREATE procedure [dbo].[ReportRTTPTLAdmittedActive]
	 @SpecialtyCode int
	,@CensusDate smalldatetime
	,@ConsultantCode int
	,@PCTCode varchar (5)
	,@PriorityCode int
	,@ManagementIntentionCode int
as


--formerly ReportPTLActiveIP

set @CensusDate = coalesce(@CensusDate, (select max(CensusDate) from APC.Snapshot))

select
	WL.EncounterRecno,

	WL.SpecialtyCode,

	Specialty.Specialty,

	left(convert(varchar, WL.CensusDate, 113), 11) CensusDate,

	case 
	when upper(WL.Operation) like '%PELVIC SERVICE' then '*' 
	when upper(WL.Operation) like '%TGL' then '#'
	else '' 
	end +
	case 
		when WL.ConsultantCode is null then 'No Consultant'
		when WL.ConsultantCode = '' then 'No Consultant'
		else
			case 
				when Consultant.Surname is null  then convert(varchar, WL.ConsultantCode) + ' - No Description'
				else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
			end
	end Consultant, 

	WL.DistrictNo,

	WL.PatientSurname,

	case
	when WL.IntendedPrimaryOperationCode is null then ''
	when WL.IntendedPrimaryOperationCode = '' then ''
	when Operation.Operation is null then left(WL.IntendedPrimaryOperationCode, 3) + ' - No Description'
	else Operation.Operation
	end + ' (' + coalesce(WL.Operation, '') + ')' Operation,

	left(WL.IntendedPrimaryOperationCode, 3) OperationCode,

	ManagementIntentionCode = ManagementIntention.NationalManagementIntentionCode,

	PriorityCode = Priority.NationalPriorityCode,

	WL.DateOnWaitingList,

	WL.OriginalDateOnWaitingList,

	WL.KornerWait,

	convert(int, coalesce(WL.KornerWait, 0) / 7.0) WeeksWaiting,

	convert(int, datediff(day, WL.OriginalDateOnWaitingList, WL.CensusDate) / 7.0) TotalWeeksWaiting,

	case
	when WL.TCIDate = WL.FuturePatientCancelDate then null
	else coalesce(WL.TCIDate, DATEADD(day, DATEDIFF(day, 0, WL.ProcedureTime), 0), WL.ExpectedAdmissionDate)
	end TCIDate,

	BreachDate = WaitingListBreach.BreachDate,

	EligibleStatusCode = null,

	case when WL.BreachDate < WL.TCIDate then 1 else 0 end BookedBeyondBreach,

	case when datediff(week, WL.TCIDate, WL.BreachDate) < 2 then 1 else 0 end NearBreach,

	'' Comment,

	case
-- breach
	when WL.CensusDate > WL.BreachDate then 'red'
-- booked beyond breach
	when WL.BreachDate < WL.TCIDate then 'pink'
-- procedure date
--	when WL.TCIDate is null and WL.ProcedureDate is not null then 'yellow'
-- urgent
	when WL.TCIDate is null and datediff(day, WL.CensusDate, WL.BreachDate) <= 28 then 'blue'
-- no tci
	when WL.TCIDate is null then 'deepskyblue'
--	DN CANCEL is black with white text
-- default
	else ''
	end TCIBackgroundColour,

	ChoiceBackgroundColour = null,

	WL.PCTCode,

	SexCode = Sex.MainCode,

	WL.DateOfBirth,

	ExpectedLOS =
		DATEDIFF(
			day
			,WL.ExpectedAdmissionDate
			,WL.ExpectedDischargeDate
		)
	,

	ProcedureDate =
		DATEADD(day, DATEDIFF(day, 0, WL.ProcedureTime), 0)
	,

	WL.TCIDate OriginalTCIDate,

	FuturePatientCancelDate FutureCancellationDate,


	PlannedAnaesthetic =
		case
		when AnaestheticType.NationalAnaestheticTypeCode = 'NSP'
		then null
		else AnaestheticType.NationalAnaestheticTypeCode
		end,

	WL.RegisteredGpPracticeCode GpPracticeCode,

	WL.BreachDays
from
	APC.WL WITH (NOLOCK)

inner join APC.WaitingListBreach
on	WaitingListBreach.SourceUniqueID = WL.SourceUniqueID
and	WaitingListBreach.CensusDate = WL.CensusDate

left join PAS.Consultant Consultant 
on	Consultant.ConsultantCode = WL.ConsultantCode

left join PAS.ServicePoint ServicePoint
on	ServicePoint.ServicePointCode = WL.SiteCode

left join Operation Operation
on	Operation.OperationCode = left(WL.IntendedPrimaryOperationCode, 3)

left join PAS.Specialty Specialty
on	Specialty.SpecialtyCode = WL.SpecialtyCode

left join PAS.AnaestheticType
on	AnaestheticType.AnaestheticTypeCode = WL.AnaestheticTypeCode

left join APC.SocialSuspension
on    SocialSuspension.WaitingListSourceUniqueID = WL.SourceUniqueID
and   SocialSuspension.CensusDate = WL.CensusDate

left join PAS.ManagementIntention
on	ManagementIntention.ManagementIntentionCode = WL.ManagementIntentionCode

left join PAS.Priority Priority
on	Priority.PriorityCode = WL.PriorityCode

left join PAS.ReferenceValue Sex
on	Sex.ReferenceValueCode = WL.SexCode

where
--			WL.AdmissionMethodCode in 
--			(
--			 8811	--Elective - Booked	12
--			,8814	--Maternity ante-partum	31
--			,8812	--Elective - Planned	13
--			,13		--Not Specified	NSP
--			,8810	--Elective - Waiting List	11
--			)

--allow not suspended or clinical suspensions
	(
		WL.WLStatus != 'Suspended'
	or	SocialSuspension.WaitingListSourceUniqueID is null
	)

and	WL.CensusDate = @CensusDate
and	(
		WL.SpecialtyCode = @SpecialtyCode
	or	@SpecialtyCode is null
	)

and	(
		Consultant.ConsultantCode = @ConsultantCode
	or	@ConsultantCode is null
	)

and	(
		left(WL.PCTCode, 3) = @PCTCode
	or	@PCTCode is null
	)

and	(
		WL.PriorityCode = @PriorityCode
	or	@PriorityCode is null
	)

and	(
		WL.ManagementIntentionCode = @ManagementIntentionCode
	or	@ManagementIntentionCode is null
	)

order by
	WL.KornerWait desc,
	Specialty.Specialty,
	WL.SiteCode,

	case 
		when WL.ConsultantCode is null then 'No Consultant'
		when WL.ConsultantCode = '' then 'No Consultant'
		else
			case 
				when Consultant.Surname is null  then convert(varchar, WL.ConsultantCode) + ' - No Description'
				else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
			end
	end,

	WL.PatientSurname
