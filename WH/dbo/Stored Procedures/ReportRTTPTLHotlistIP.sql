﻿CREATE	procedure [dbo].[ReportRTTPTLHotlistIP] 
	 @SpecialtyCode int = null
	,@CensusDate smalldatetime = null
	,@ConsultantCode int = null
	,@ManagementIntentionCode int = null
	,@QuarterStartDate as varchar(50) = null
as

--formerly ReportRTTPTLHotlistIP

declare @LocalCensusDate smalldatetime

select
	 @LocalCensusDate = coalesce(@CensusDate, (select max(CensusDate) from APC.Snapshot))

select
	WL.BreachWeekNo,

	case WL.BreachWeekNo
	when 0 then 'Red'
	when 1 then 'Orange'
	when 2 then 'Orange'
	when 3 then 'Orange'
	else 'White'
	end RowColour,

	WL.EncounterRecno,

	WL.SpecialtyCode,

	Specialty.Specialty,

	left(convert(varchar, WL.CensusDate, 113), 11) CensusDate,

	case 
	when upper(Operation.Coding) like '%PELVIC SERVICE' then '*' 
	when upper(Operation.Coding) like '%TGL' then '#'
	else '' 
	end +
	case 
		when WL.ConsultantCode is null then 'No Consultant'
		when WL.ConsultantCode = '' then 'No Consultant'
		else
			case 
				when Consultant.Surname is null  then convert(varchar, WL.ConsultantCode) + ' - No Description'
				else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
			end
	end Consultant,  

	WL.DistrictNo,

	WL.PatientSurname,

	Operation.Coding,

	WL.IntendedPrimaryOperationCode OperationCode,

	WL.DateOnWaitingList,

	WL.OriginalDateOnWaitingList,

	WL.KornerWait,

	convert(int, coalesce(WL.KornerWait, 0) / 7.0) WeeksWaiting,

	convert(int, datediff(day, WL.OriginalDateOnWaitingList, WL.CensusDate) / 7.0) TotalWeeksWaiting,

	case
	when WL.TCIDate = WL.FutureCancellationDate and WL.CancelledBy = 'P' then null
	when WL.FutureCancellationDate is not null and coalesce(WL.CancelledBy, 'X') != 'P' then null
	else coalesce(WL.TCIDate, WL.ProcedureTime)
	end TCIDate,

	DerivedBreachDate = DerivedBreachDate,

	case when WL.DerivedBreachDate < WL.TCIDate then 1 else 0 end BookedBeyondBreach,

	case when datediff(week, WL.TCIDate, DerivedBreachDate) < 2 then 1 else 0 end NearBreach,

	'' Comment,

	case
-- breach
	when WL.CensusDate > DerivedBreachDate then 'red'
-- booked beyond breach
	when DerivedBreachDate < WL.TCIDate then 'pink'
-- urgent
	when WL.TCIDate is null and datediff(day, WL.CensusDate, DerivedBreachDate) <= 28 then 'blue'
-- no tci
	when WL.TCIDate is null then 'deepskyblue'
--	DN CANCEL is black with white text
-- default
	else ''
	end TCIBackgroundColour,

	null ChoiceBackgroundColour,

	WL.PCTCode,

	WL.SexCode,
	WL.DateOfBirth,

	ExpectedLOS =
		DATEDIFF(day, WL.ExpectedAdmissionDate, WL.ExpectedDischargeDate)
	,

	WL.ProcedureTime,
	WL.TCIDate OriginalTCIDate,

	WL.FuturePatientCancelDate FutureCancellationDate,

	WL.SuspensionEndDate,

	WL.BreachDate28Day

	,WL.RTTPathwayID
	,WL.DerivedClockStartDate
	,WL.RTTCurrentStatusCode
	,WL.RTTCurrentStatusDate

	,WL.RTTKornerWait 
	,WL.RTTWeeksWaiting
	,WL.RTTTotalWeeksWaiting

	,WL.NationalBreachDate
	,WL.NationalBreachDateVisibleFlag
	,WL.RTTBreachDate
	,WL.DerivedDiagnosticBreachDate

from
	(
	select
		BreachPeriodDate =
			Calendar.LastDayOfWeek

		,BreachWeekNo =
			case
			when convert(int, Calendar.WeekNoKey) - (datepart(year, WL.CensusDate) * 100 + datepart(week, WL.CensusDate)) < 1 then 0 
			else convert(int, Calendar.WeekNoKey) - (datepart(year, WL.CensusDate) * 100 + datepart(week, WL.CensusDate))
			end

		,FutureCancellationDate =
			WL.FuturePatientCancelDate

		,BreachDate28Day = null
	--	,BreachDate28Day = TheatreSameDayCancellation.BreachDate

	-- 19 Mar 2009 - always show the national breach date
		,NationalBreachDateVisibleFlag = 1

	--	,NationalBreachDateVisibleFlag =
	--		case
	--		when WL.RTTBreachDate < WL.CensusDate
	--		then 1
	--		else 0
	--		end

		,DerivedDiagnosticBreachDate =
		case
		when WL.BreachTypeCode = 2 then
			case
			when WL.RTTDiagnosticBreachDate < WL.NationalDiagnosticBreachDate
			then WL.RTTDiagnosticBreachDate
			else WL.NationalDiagnosticBreachDate
			end
		else null
		end

		,DerivedBreachDate = Breach.BreachDate

		,WL.EncounterRecno

		,WL.SpecialtyCode

		,WL.CensusDate

		,WL.ConsultantCode

		,WL.DistrictNo

		,WL.PatientSurname

		,WL.IntendedPrimaryOperationCode

		,WL.DateOnWaitingList

		,WL.OriginalDateOnWaitingList

		,WL.KornerWait

		,WL.TCIDate

		,WL.CancelledBy

		,WL.ProcedureTime

		,WL.PCTCode

		,WL.SexCode

		,WL.DateOfBirth

		,WL.ExpectedAdmissionDate

		,WL.ExpectedDischargeDate

		,WL.FuturePatientCancelDate

		,WL.SuspensionEndDate

		,WL.RTTPathwayID

		,DerivedClockStartDate = Breach.ClockStartDate

		,WL.RTTCurrentStatusCode
		,WL.RTTCurrentStatusDate

		,RTTKornerWait = datediff(day, Breach.ClockStartDate, WL.CensusDate) - WL.SocialSuspensionDays 
		,RTTWeeksWaiting = (datediff(day, Breach.ClockStartDate, WL.CensusDate) - Breach.SocialSuspensionDays) / 7 
		,RTTTotalWeeksWaiting = datediff(day, dateadd(day, Breach.SocialSuspensionDays, Breach.ClockStartDate), WL.CensusDate) / 7 

		,Breach.NationalBreachDate
		,Breach.RTTBreachDate
		,Breach.RTTDiagnosticBreachDate
		,Breach.SocialSuspensionDays

	from
		APC.WaitingList WL WITH (NOLOCK)

	inner join APC.WaitingListBreach Breach
	on	Breach.SourceUniqueID = WL.SourceUniqueID
	and	Breach.CensusDate = WL.CensusDate

	--left join Theatre.SameDayCancellation TheatreSameDayCancellation
	--on	TheatreSameDayCancellation.SourceUniqueID = WL.TheatrePatientBookingKey

	--left join Calendar Calendar
	--on	Calendar.TheDate = 
	--	case
	--	when TheatreSameDayCancellation.BreachDate is null then Breach.BreachDate
	--	when TheatreSameDayCancellation.BreachDate < Breach.BreachDate
	--	then TheatreSameDayCancellation.BreachDate
	--	else Breach.BreachDate
	--	end

	left join Calendar Calendar
	on	Calendar.TheDate = Breach.BreachDate

	where
		WL.AdmissionMethodCode in 
		(
		 8811	--Elective - Booked	12
		,8814	--Maternity ante-partum	31
		,8812	--Elective - Planned	13
		,13		--Not Specified	NSP
		,8810	--Elective - Waiting List	11
		)

	and	WL.CensusDate = @LocalCensusDate

	and	(
			(
				@QuarterStartDate is null
			and	Breach.BreachDate <= dateadd(week, 8, WL.CensusDate)
			)
		)

	and	(
			WL.SpecialtyCode = @SpecialtyCode
		or	@SpecialtyCode is null
		)


	and	(
			WL.ConsultantCode = @ConsultantCode
		or	@ConsultantCode is null
		)

	and	(
			WL.ManagementIntentionCode = @ManagementIntentionCode
		or	@ManagementIntentionCode is null
		)

	) WL

left join PAS.Consultant Consultant 
on	Consultant.ConsultantCode = WL.ConsultantCode

left join PAS.Coding Operation
on	Operation.CodingLocalCode = WL.IntendedPrimaryOperationCode
and	Operation.CodingTypeCode = 'OPCS4'
and	Operation.ArchiveFlag = 0
and	not exists
	(
	select
		1
	from
		PAS.Coding Previous
	where
		Previous.CodingLocalCode = WL.IntendedPrimaryOperationCode
	and	Previous.CodingTypeCode = 'OPCS4'
	and	Previous.ArchiveFlag = 0
	and	Previous.CodingCode > Operation.CodingCode
	)

left join PAS.Specialty Specialty
on	Specialty.SpecialtyCode = WL.SpecialtyCode

where
	(
		WL.FuturePatientCancelDate is null
	or	WL.FuturePatientCancelDate > WL.BreachPeriodDate
	)
and
	(
		WL.TCIDate is null
	or	(
			(
				WL.TCIDate > WL.BreachPeriodDate
			or	WL.TCIDate < dateadd(day, -4, WL.CensusDate)
			or
				(
					WL.TCIDate < dateadd(day, -4, WL.BreachPeriodDate)
				and	datepart(week, WL.TCIDate) = datepart(week, WL.BreachPeriodDate)
				and	datepart(year, WL.TCIDate) = datepart(year, WL.BreachPeriodDate)
				)
			)
		and	datepart(year, WL.TCIDate) * 100 + datepart(week, WL.TCIDate) != datepart(year, WL.CensusDate) * 100 + datepart(week, WL.CensusDate)
		)
	)

and
	(
		WL.ProcedureTime is null
	or	WL.ProcedureTime > WL.BreachPeriodDate
	or	(
			WL.ProcedureTime < dateadd(day, 4, WL.CensusDate) 
		and WL.TCIDate is null
		)
	)
and
	(
		WL.SuspensionEndDate is null
	or	WL.SuspensionEndDate <= WL.BreachPeriodDate
	)

order by
	Specialty.Specialty,

	case 
	when upper(Operation.Coding) like '%PELVIC SERVICE' then '*' 
	when upper(Operation.Coding) like '%TGL' then '#'
	else '' 
	end +
	case 
		when WL.ConsultantCode is null then 'No Consultant'
		when WL.ConsultantCode = '' then 'No Consultant'
		else
			case 
				when Consultant.Surname is null  then convert(varchar, WL.ConsultantCode) + ' - No Description'
				else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
			end
	end, 

	DerivedBreachDate,
	WL.PatientSurname
