﻿
CREATE procedure [dbo].[ReportRTTPTLInpatient]
	 @SpecialtyCode int = null
	,@CensusDate smalldatetime
	,@ConsultantCode int = null
	,@PCTCode varchar (5) = null
	,@PriorityCode int = null
	,@ManagementIntentionCode int = null
	,@PASCreated smalldatetime = null
	,@Report varchar(20) = 'ADMITTED'
as

declare @localCensusDate smalldatetime

set @localCensusDate = coalesce(@CensusDate, (select max(CensusDate) from APC.Snapshot))

select
	 WL.EncounterRecno
	,WL.SpecialtyCode
	,WL.Specialty
	,WL.SiteCode
	,WL.CensusDate
	,WL.Site
	,WL.Consultant
	,WL.DistrictNo
	,WL.PatientSurname
	,WL.Operation
	,WL.OperationCode
	,WL.ManagementIntentionCode
	,WL.PriorityCode
	,WL.OriginalDateOnWaitingList
	,WL.KornerWait
	,WL.WeeksWaiting
	,WL.TotalWeeksWaiting
	,WL.TCIDate
	,WL.Comment
	,WL.ChoiceBackgroundColour
	,WL.PCTCode
	,WL.SexCode
	,WL.DateOfBirth
	,WL.ExpectedLOS
	,WL.ProcedureDate
	,WL.OriginalTCIDate
	,WL.FutureCancellationDate
	,WL.PlannedAnaesthetic
	,WL.GpPracticeCode
	,WL.DerivedBreachDays
	,WL.RTTPathwayID
	,WL.RTTPathwayCondition
	,WL.RTTStartDate
	,WL.RTTEndDate
	,WL.RTTSpecialtyCode
	,WL.RTTCurrentProviderCode
	,WL.RTTCurrentStatusCode
	,WL.RTTCurrentStatusDate
	,WL.RTTCurrentPrivatePatientFlag
	,WL.RTTOverseasStatusFlag
	,WL.DerivedClockStartDate
	,WL.RTTKornerWait
	,WL.RTTWeeksWaiting
	,WL.RTTTotalWeeksWaiting
	,WL.RTTBreachDate
	,WL.NationalBreachDate
	,WL.RTTDiagnosticBreachDate
	,WL.NationalBreachBackgroundColour
	,WL.DerivedBreachDate
	,WL.DerivedDiagnosticBreachDate
	,WL.SocialSuspensionDays
	,WL.DateOnWaitingList
	,WL.MaintenanceURL
	,WL.BreachTypeCode
	,WL.ClinicalSuspensionFlag
	,WL.SuspensionEndDate
	,WL.ExpectedAdmissionDate
	,WL.WLStatus
	,WL.SuspensionStartDate
	,WL.SuspensionReason
	,WL.PlannedSuspended
	,WL.PASCreated
	,WL.ReferralCompComment
	,WL.ListType
	,WL.AdmissionWard
	,WL.SecondaryWard
	,WL.CurrentSuspensionType
	,WL.FutureSuspensionDate
	,WL.FutureSuspensionDays
	,WL.PreOpDate

	,case when WL.DerivedBreachDate < WL.TCIDate then 1 else 0 end RTTBookedBeyondBreach
	,case when datediff(week, WL.TCIDate, WL.DerivedBreachDate) < 2 then 1 else 0 end RTTNearBreach

	,RTTTCIBackgroundColour = ''

	,RTTDiagnosticBackgroundColour =
	case
	when WL.BreachTypeCode = '2' then
		case
	-- breach
		when WL.CensusDate > WL.DerivedDiagnosticBreachDate then 'red'
	-- booked beyond breach
		when WL.DerivedDiagnosticBreachDate < WL.TCIDate then 'pink'
	-- procedure date
	--	when WL.TCIDate is null and WL.ProcedureDate is not null then 'yellow'
	-- urgent
		when WL.TCIDate is null and datediff(day, WL.CensusDate, WL.DerivedDiagnosticBreachDate) <= 14 then 'blue'
	-- no tci
		when WL.TCIDate is null then 'deepskyblue'
	--	DN CANCEL is black with white text
	-- default
		else ''
		end 
	else ''
	end 

	,RTTBackgroundColour =
	case
	when WL.BreachTypeCode != '2' then
		case
	-- breach
		when WL.CensusDate > WL.RTTBreachDate then 'red'
	-- booked beyond breach
		when WL.RTTBreachDate < WL.TCIDate then 'pink'
	-- procedure date
	--	when WL.TCIDate is null and WL.ProcedureDate is not null then 'yellow'
	-- urgent
		when WL.TCIDate is null and datediff(day, WL.CensusDate, WL.RTTBreachDate) <= 28 then 'blue'
	-- no tci
		when WL.TCIDate is null then 'deepskyblue'
	--	DN CANCEL is black with white text
	-- default
		else ''
		end 
	else ''
	end 

	,NewRTTBreachDate = RTTBreachDate

	,NationalBreachDateVisibleFlag =
		case
		when RTTBreachDate < CensusDate
		then 1
		else 0
		end

	,TCIBackgroundColour =
	case
	when WL.TCIDate is null then 'deepskyblue'
	else ''
	end

	,PTLComments =
		case
		when
			WL.RTTBreachDate < WL.CensusDate
		and	WL.TCIDate is null
		then 'Backlog no TCI'

		when
			WL.RTTBreachDate < WL.CensusDate
		then 'Backlog with TCI'

		when
			WL.TCIDate > WL.RTTBreachDate
		and	WL.RTTBreachDate > WL.CensusDate
		then 'Bring forward'

		when
			WL.TCIDate is null
		and	datepart(year, WL.RTTBreachDate) * 100 + datepart(month, WL.RTTBreachDate) <= datepart(year, WL.CensusDate) * 100 + datepart(month, WL.CensusDate)
		then 'No TCI current month'

		when
			WL.TCIDate is null
		and	datepart(year, WL.RTTBreachDate) * 100 + datepart(month, WL.RTTBreachDate) <= datepart(year, dateadd(month, 1, WL.CensusDate)) * 100 + datepart(month, WL.CensusDate) + 1
		then 'No TCI next month'

		when WL.TCIDate < WL.CensusDate
		then '? Outcome'

		end

from
	(
	select
		WL.EncounterRecno,
		WL.SpecialtyCode,
		WL.Specialty,
		WL.SiteCode,
		WL.CensusDate,
		WL.Site,
		WL.Consultant, 
		WL.DistrictNo,
		WL.PatientSurname,
		WL.Operation,
		WL.OperationCode,
		WL.ManagementIntentionCode,
		WL.PriorityCode,
		WL.OriginalDateOnWaitingList,
		WL.KornerWait,
		WL.WeeksWaiting,
		WL.TotalWeeksWaiting,
		WL.TCIDate,
		WL.Comment,
		WL.ChoiceBackgroundColour,
		WL.PCTCode,
		WL.SexCode,
		WL.DateOfBirth,
		WL.ExpectedLOS,
		WL.ProcedureDate,
		WL.OriginalTCIDate,
		WL.FutureCancellationDate,
		WL.PlannedAnaesthetic,
		WL.GpPracticeCode

		,WL.DerivedBreachDays
		,WL.RTTPathwayID
		,WL.RTTPathwayCondition
		,WL.RTTStartDate
		,WL.RTTEndDate
		,WL.RTTSpecialtyCode
		,WL.RTTCurrentProviderCode
		,WL.RTTCurrentStatusCode
		,WL.RTTCurrentStatusDate
		,WL.RTTCurrentPrivatePatientFlag
		,WL.RTTOverseasStatusFlag

		,WL.DerivedClockStartDate

		,datediff(day, WL.DerivedClockStartDate, WL.CensusDate) - WL.SocialSuspensionDays RTTKornerWait

		,(datediff(day, WL.DerivedClockStartDate, WL.CensusDate) - WL.SocialSuspensionDays) / 7 RTTWeeksWaiting

		,datediff(day, dateadd(day, WL.SocialSuspensionDays, WL.DerivedClockStartDate), WL.CensusDate) / 7 RTTTotalWeeksWaiting

		,WL.RTTBreachDate

		,WL.NationalBreachDate

		,WL.RTTDiagnosticBreachDate

		,NationalBreachBackgroundColour =
		case
		when datediff(day, WL.DerivedClockStartDate, WL.CensusDate) > 
			(select NumericValue from dbo.Parameter where Parameter = 'BREACHTHRESHOLDDAYS')
		then 'yellow'
		else 'white'
		end

		,WL.DerivedBreachDate

		,DerivedDiagnosticBreachDate =
			case
			when WL.BreachTypeCode = 'DIAG'
			then
				case
				when WL.RTTDiagnosticBreachDate < WL.NationalDiagnosticBreachDate
				then WL.RTTDiagnosticBreachDate
				else WL.NationalDiagnosticBreachDate
				end
			end

		,WL.SocialSuspensionDays
		,WL.DateOnWaitingList

		,MaintenanceURL =
		(
		select
			TextValue
		from
			dbo.Parameter
		where
			Parameter = 'MAINTENANCEURL'
		)
		+ '?SourcePatientNo=' + convert(varchar, WL.SourcePatientNo)
		+ '&SourceEncounterNo=' + convert(varchar, WL.SourceEncounterNo)

		,BreachTypeCode

		,ClinicalSuspensionFlag

		,SuspensionEndDate

		,WL.ExpectedAdmissionDate
		,WL.WLStatus
		,WL.SuspensionStartDate
		,WL.SuspensionReason
		,WL.PlannedSuspended
		,WL.PASCreated

		,WL.ReferralCompComment
		,WL.ListType
		,WL.AdmissionWard
		,WL.SecondaryWard
		,WL.CurrentSuspensionType
		,WL.FutureSuspensionDate
		,WL.FutureSuspensionDays
		,WL.PreOpDate

	from
		(
		select
			WL.EncounterRecno,

			WL.SpecialtyCode SpecialtyCode,

			Specialty.Specialty,

			WL.ConsultantCode,

			ServicePoint.ServicePointLocalCode SiteCode,

			left(convert(varchar, WL.CensusDate, 113), 11) CensusDate,

			case 
			when WL.SiteCode is null then 'Unknown Site' 
			when ServicePoint.ServicePoint is null then convert(varchar, WL.SiteCode) + ' - No Description' 
			else ServicePoint.ServicePoint
			end Site,

			case 
			when upper(WL.Operation) like '%PELVIC SERVICE' then '*' 
			when upper(WL.Operation) like '%TGL' then '#'
			else '' 
			end +
			case 
				when WL.ConsultantCode is null then 'No Consultant'
				when WL.ConsultantCode = '' then 'No Consultant'
				else
					case 
						when Consultant.Surname is null  then convert(varchar, WL.ConsultantCode) + ' - No Description'
						else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
					end
			end Consultant, 

			WL.DistrictNo,

			WL.PatientSurname,

--			case
--			when WL.IntendedPrimaryOperationCode is null then ''
--			when WL.IntendedPrimaryOperationCode = '' then ''
--			when Operation.Operation = '' then left(WL.IntendedPrimaryOperationCode, 3) + ' - No Description'
--			else Operation.Operation
--			end + ' (' + coalesce(WL.Operation, '') + ')' Operation,
			WL.Operation,

			WL.IntendedPrimaryOperationCode OperationCode,

			ManagementIntentionCode = ManagementIntention.NationalManagementIntentionCode,

			PriorityCode = Priority.NationalPriorityCode,

			WL.OriginalDateOnWaitingList,

			WL.KornerWait,

			convert(int, coalesce(WL.KornerWait, 0) / 7.0) WeeksWaiting,

			convert(int, datediff(day, WL.OriginalDateOnWaitingList, WL.CensusDate) / 7.0) TotalWeeksWaiting,

			--PDO 24 Feb 2012
			--if TCI is cancelled then make it null
			TCIDate =
				case
				when WL.CancelledBy is null
				then coalesce(WL.TCIDate, WL.ProcedureTime)
				else null
				end
			,

			--case
			--when WL.TCIDate = WL.FuturePatientCancelDate and WL.CancelledBy = 'P' then null
			--when WL.FuturePatientCancelDate is not null and coalesce(WL.CancelledBy, 'X') != 'P' then null
			--else coalesce(WL.TCIDate, WL.ProcedureTime)
			--end TCIDate,

			WL.DerivedBreachDate,

			Comment = WL.GeneralComment,

			'white' ChoiceBackgroundColour,

			PCTCode = left(WL.PCTCode, 3),

			SexCode = Sex.MainCode,
					
			WL.DateOfBirth,
			ExpectedLOS = datediff(day, WL.ExpectedAdmissionDate, WL.ExpectedDischargeDate),

			--PDO 24 Feb 2012
			--if TCI is cancelled then make it null
			ProcedureDate =
				case
				when WL.CancelledBy is null
				then dateadd(day, datediff(day, 0, WL.ProcedureTime), 0)
				else null
				end
			,

			--ProcedureDate = dateadd(day, datediff(day, 0, WL.ProcedureTime), 0),


			--PDO 24 Feb 2012
			--if TCI is cancelled then make it null
			OriginalTCIDate =
				case
				when WL.CancelledBy is null
				then coalesce(WL.TCIDate, WL.ProcedureTime)
				else null
				end
			,

			--WL.TCIDate OriginalTCIDate,

			WL.FuturePatientCancelDate FutureCancellationDate,

			PlannedAnaesthetic =
				case
				when AnaestheticType.NationalAnaestheticTypeCode = 'NSP'
				then null
				else AnaestheticType.NationalAnaestheticTypeCode
				end

			,coalesce(WL.EpisodicGpPracticeCode, WL.RegisteredGpPracticeCode) GpPracticeCode,

			WL.DerivedBreachDays

			,WL.RTTPathwayID
			,WL.RTTPathwayCondition
			,WL.RTTStartDate
			,WL.RTTEndDate
			,WL.RTTSpecialtyCode
			,WL.RTTCurrentProviderCode
--			,WL.RTTCurrentStatusCode
			,WL.RTTCurrentStatusDate
			,WL.RTTCurrentPrivatePatientFlag
			,WL.RTTOverseasStatusFlag


			,DerivedClockStartDate = WaitingListBreach.ClockStartDate
			,RTTBreachDate = WaitingListBreach.RTTBreachDate
			,RTTDiagnosticBreachDate = WaitingListBreach.RTTDiagnosticBreachDate
			,NationalDiagnosticBreachDate = WaitingListBreach.NationalDiagnosticBreachDate
			,NationalBreachDate = WaitingListBreach.NationalBreachDate
			,SocialSuspensionDays = WaitingListBreach.SocialSuspensionDays
			,WL.DateOnWaitingList

			,WL.SourcePatientNo
			,WL.SourceEncounterNo

			,WL.BreachTypeCode

			,ClinicalSuspensionFlag =
			case when WL.WLStatus = 'Suspended' then 1 else 0 end

			,WL.SuspensionEndDate

			,RTTCurrentStatusCode =
				RTTStatus.NationalRTTStatusCode

			,WL.ExpectedAdmissionDate
			,WL.WLStatus
			,WL.SuspensionStartDate
			,WL.SuspensionReason

			,PlannedSuspended =
				case
				when WL.AdmissionMethodCode = 8812 then 'Y'
				else 'N'
				end

			,WL.PASCreated

			,ReferralCompComment = Referral.GeneralComment
			,ListType = ListType.MainCode
			,AdmissionWard = ServicePoint.ServicePointLocalCode
			,SecondaryWard = SecondaryWard.ServicePointLocalCode

			,CurrentSuspensionType =
				case
				when WL.SuspensionReasonCode is null
				then null
				when WL.SuspensionReasonCode in
					(
					 4398	--Patient Away
					,4397	--Patient Request
					)
				then 'P'
				else 'M'
				end

			,FutureSuspensionDate = FutureWaitingListSuspension.SuspensionStartDate

			,FutureSuspensionDays =
				DATEDIFF(
					 day
					,FutureWaitingListSuspension.SuspensionStartDate
					,FutureWaitingListSuspension.SuspensionEndDate
				)

			,PreOpDate = WL.PreOpDate
		from
			APC.WaitingList WL WITH (NOLOCK)

		inner join APC.WaitingListBreach
		on	WaitingListBreach.SourceUniqueID = WL.SourceUniqueID
		and	WaitingListBreach.CensusDate = WL.CensusDate

		left join PAS.Consultant Consultant 
		on	Consultant.ConsultantCode = WL.ConsultantCode

		left join PAS.ServicePoint ServicePoint
		on	ServicePoint.ServicePointCode = WL.SiteCode

		left join PAS.ServicePoint SecondaryWard
		on	SecondaryWard.ServicePointCode = WL.WardCode

		left join Operation Operation
		on	Operation.OperationCode = left(WL.IntendedPrimaryOperationCode, 3)

		left join PAS.Specialty Specialty
		on	Specialty.SpecialtyCode = WL.SpecialtyCode

		left join APC.SocialSuspension
		on    SocialSuspension.WaitingListSourceUniqueID = WL.SourceUniqueID
		and   SocialSuspension.CensusDate = WL.CensusDate

		left join PAS.AnaestheticType
		on	AnaestheticType.AnaestheticTypeCode = WL.AnaestheticTypeCode

		left join PAS.ManagementIntention
		on	ManagementIntention.ManagementIntentionCode = WL.ManagementIntentionCode

		left join PAS.Priority Priority
		on	Priority.PriorityCode = WL.PriorityCode

		left join PAS.ReferenceValue Sex
		on	Sex.ReferenceValueCode = WL.SexCode
		
		left join PAS.RTTStatus RTTStatus
		on	RTTStatus.RTTStatusCode = WL.RTTCurrentStatusCode

		left join RF.Encounter Referral
		on	Referral.SourceUniqueID = WL.ReferralSourceUniqueID

		left join PAS.WaitingListRule ListType
		on	ListType.WaitingListRuleCode = WL.WaitingListRuleCode

		left join
			(
			select
				 WaitingListSuspension.WaitingListSourceUniqueID
				,WaitingListSuspension.CensusDate
				,WaitingListSuspension.SuspensionStartDate
				,WaitingListSuspension.SuspensionEndDate
			from
				APC.WaitingListSuspension
			where
				WaitingListSuspension.SuspensionStartDate > WaitingListSuspension.CensusDate

			and	not exists
				(
				select
					1
				from
					APC.WaitingListSuspension Previous
				where
					Previous.WaitingListSourceUniqueID = WaitingListSuspension.WaitingListSourceUniqueID
				and	Previous.CensusDate = WaitingListSuspension.CensusDate
				and	Previous.SuspensionStartDate > WaitingListSuspension.CensusDate
				and	Previous.SourceUniqueID < WaitingListSuspension.SourceUniqueID
				)
			) FutureWaitingListSuspension
		on	FutureWaitingListSuspension.WaitingListSourceUniqueID = WL.SourceUniqueID
		and	FutureWaitingListSuspension.CensusDate = WL.CensusDate

		where

--PDO 9 Feb 2012
--removed this to allow all waiters including suspended to show
--as per CBH request

----allow not suspended or clinical suspensions
--			(
--				(
--					@Report != 'SUSPENDED'
--				and	(
--						WL.WLStatus != 'Suspended'
--					or	SocialSuspension.WaitingListSourceUniqueID is null
--					)
--				)
--			or
--				(
--					@Report = 'SUSPENDED'
--				and	WL.WLStatus = 'Suspended'
--				and	SocialSuspension.WaitingListSourceUniqueID is not null
--				)
--			)

			(
				(
					WL.AdmissionMethodCode in 
						(
						-- 8811	--Elective - Booked	12
						--,8814	--Maternity ante-partum	31
						8812	--Elective - Planned	13
						--,13		--Not Specified	NSP
						--,8810	--Elective - Waiting List	11
						)
				and	@Report = 'PLANNED'
				)
			or	(
					WL.AdmissionMethodCode not in 
						(
						-- 8811	--Elective - Booked	12
						--,8814	--Maternity ante-partum	31
						8812	--Elective - Planned	13
						--,13		--Not Specified	NSP
						--,8810	--Elective - Waiting List	11
						)

				and	@Report != 'PLANNED'
				)
			)

		and	WL.CensusDate = @localCensusDate
		and	(
				WL.SpecialtyCode = @SpecialtyCode
			or	@SpecialtyCode is null
			)

		and	(
				Consultant.ConsultantCode = @ConsultantCode
			or	@ConsultantCode is null
			)

		and	(
				left(WL.PCTCode, 3) = @PCTCode
			or	@PCTCode is null
			)

		and	(
				WL.PriorityCode = @PriorityCode
			or	@PriorityCode is null
			)

		and	(
				WL.ManagementIntentionCode = @ManagementIntentionCode
			or	@ManagementIntentionCode is null
			)

		and	(
				cast(WL.PASCreated as date) = @PASCreated
			or	@PASCreated is null
			)

		) WL

) WL

order by
	datediff(day, WL.DerivedClockStartDate, WL.CensusDate) - WL.SocialSuspensionDays desc,
	WL.Specialty,
	WL.SiteCode,
	WL.Consultant,
	WL.PatientSurname
