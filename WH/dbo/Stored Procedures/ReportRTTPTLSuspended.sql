﻿CREATE     procedure [dbo].[ReportRTTPTLSuspended] 
	 @SpecialtyCode varchar (10)
	,@CensusDate smalldatetime
	,@InterfaceCode varchar (5)
	,@ConsultantCode varchar (20)
	,@PCTCode varchar (5)
	,@PriorityCode varchar(2)
	,@ManagementIntentionCode varchar(2)
as


--formally ReportPTLSuspendedIPRTT

set @CensusDate = coalesce(@CensusDate, (select max(CensusDate) from APC.Snapshot))

select
	WL.EncounterRecno,

	WL.SpecialtyCode,

	Specialty.Specialty,

	Site.Abbreviation SiteCode,

	left(convert(varchar, WL.CensusDate, 113), 11) CensusDate,

	case 
	when WL.SiteCode is null then 'Unknown Site' 
	when Site.Site is null then WL.SiteCode + ' - No Description' 
	else Site.Site
	end Site,

	case 
	when upper(WL.Operation) like '%PELVIC SERVICE' then '*' 
	when upper(WL.Operation) like '%TGL' then '#'
	else '' 
	end +
		case 
			when WL.ConsultantCode is null then 'No Consultant'
			when WL.ConsultantCode = '' then 'No Consultant'
			else
				case 
					when Consultant.Surname is null  then WL.ConsultantCode + ' - No Description'
					else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
				end
		end Consultant, 

	WL.DistrictNo,

	WL.PatientSurname,

	case
	when WL.IntendedPrimaryOperationCode is null then ''
	when WL.IntendedPrimaryOperationCode = '' then ''
	when Operation.Operation is null then left(WL.IntendedPrimaryOperationCode, 3) + ' - No Description'
	else Operation.Operation
	end + ' (' + coalesce(WL.Operation, '') + ')' Operation,

	left(WL.IntendedPrimaryOperationCode, 3) OperationCode,

	WL.ManagementIntentionCode,

	WL.PriorityCode,

	WL.OriginalDateOnWaitingList OriginalDateOnWaitingList,
--
--	KornerWait =
--		WL.KornerWait - SocialSuspension.DaysSuspended,

	KornerWait =
		datediff
			(
				 day
				,coalesce(WL.RTTStartDate, WL.OriginalDateOnWaitingList)
				,coalesce(Suspension.SuspensionEndDate, WL.CensusDate)
			)
		 - SocialSuspension.DaysSuspended

	,WeeksWaiting =
		convert(int, coalesce(
			datediff
				(
					 day
					,coalesce(WL.RTTStartDate, WL.OriginalDateOnWaitingList)
					,coalesce(Suspension.SuspensionEndDate, WL.CensusDate)
				)
			 - SocialSuspension.DaysSuspended
			,0) / 7.0
		) ,

	convert(int, datediff(day, coalesce(WL.RTTStartDate, WL.OriginalDateOnWaitingList), WL.CensusDate) / 7.0) TotalWeeksWaiting,

	WL.TCIDate,

	BreachDate = 
		WL.RTTBreachDate,

--	BreachDate = 
--		dateadd(day
--			,SocialSuspension.DaysSuspended
--			,WL.RTTBreachDate
--		),

	null EligibleStatusCode,

	case when 
		dateadd(day
			,SocialSuspension.DaysSuspended
			,WL.RTTBreachDate
		) < WL.TCIDate then 1 else 0 end BookedBeyondBreach,

	case when datediff(week, WL.TCIDate, 
		dateadd(day
			,SocialSuspension.DaysSuspended
			,WL.RTTBreachDate
		)
	) < 2 then 1 else 0 end NearBreach,

	'' Comment,

	WL.ExpectedAdmissionDate,

	WL.WLStatus,

	WL.AdmissionMethodCode,

	left(WL.IntendedPrimaryOperationCode, 3) IntendedPrimaryOperationCode,

	WL.SuspensionEndDate,

	WL.PCTCode,

	WL.NHSNumber,

	WL.SuspensionStartDate,

	case
-- breach
	when WL.CensusDate > 
		dateadd(day
			,SocialSuspension.DaysSuspended
			,WL.RTTBreachDate
		) then 'red'
-- booked beyond breach
	when 
		dateadd(day
			,SocialSuspension.DaysSuspended
			,WL.RTTBreachDate
		) < WL.TCIDate then 'pink'
-- urgent
	when WL.TCIDate is null and datediff(day, WL.CensusDate, 
		dateadd(day
			,SocialSuspension.DaysSuspended
			,WL.RTTBreachDate
		)
	) <= 28 then 'blue'
-- no tci
	when WL.TCIDate is null then 'deepskyblue'
--	DN CANCEL is black with white text
-- default
	else ''
	end TCIBackgroundColour,

	null ChoiceBackgroundColour,

/*
	case
	when WL.AdmissionMethodCode in ('BP', 'PA', 'PL') then 'gray'
	else ''
	end RowBackgroundColour,
*/

	'' RowBackgroundColour,

	WL.SexCode,
	WL.DateOfBirth,
	WL.ExpectedLOS,


	case
	when WL.AdmissionMethodCode in ('BP', 'PA', 'PL') then 'Y'
	else 'N'
	end PlannedSuspended

	,WL.RTTPathwayID
	,WL.RTTPathwayCondition
	,WL.RTTStartDate
	,WL.RTTEndDate
	,WL.RTTSpecialtyCode
	,WL.RTTCurrentProviderCode
	,WL.RTTCurrentStatusCode
	,WL.RTTCurrentStatusDate
	,WL.RTTCurrentPrivatePatientFlag
	,WL.RTTOverseasStatusFlag

	,RTTStatus.RTTStatus
	,WL.DerivedClockStartDate

	,Suspension.SuspensionReason

	,WL.NationalBreachDate

	,NationalBreachDateVisibleFlag =
		case
		when WL.RTTBreachDate < WL.CensusDate
		then 1
		else 0
		end

	,RTTDiagnosticBreachDate =
	case
	when WL.BreachTypeCode = 2 then
		case
		when WL.RTTDiagnosticBreachDate < WL.NationalDiagnosticBreachDate
		then WL.RTTDiagnosticBreachDate
		else WL.NationalDiagnosticBreachDate
		end
	else null
	end

from
	APC.WL

left join PAS.Consultant PASConsultant 
on	PASConsultant.ConsultantCode = WL.ConsultantCode

left join Consultant Consultant 
on	PASConsultant.NationalConsultantCode = Consultant.ConsultantCode

left join PAS.Site PASSite
on	PASSite.SiteCode = WL.SiteCode

left join Site Site
on	Site.SiteCode = PASSite.MappedSiteCode

left join Operation Operation
on	Operation.OperationCode = left(WL.IntendedPrimaryOperationCode, 3)

left join dbo.DiagnosticProcedure
on	DiagnosticProcedure.ProcedureCode = WL.IntendedPrimaryOperationCode

left join SpecialtyPennine Specialty
on	Specialty.SpecialtyCode = WL.SpecialtyCode

--Social suspensions only
inner join APC.SocialSuspension SocialSuspension
on    SocialSuspension.SourcePatientNo = WL.SourcePatientNo
and   SocialSuspension.SourceEncounterNo = WL.SourceEncounterNo
and   SocialSuspension.CensusDate = WL.CensusDate

left join RTTStatus
on	RTTStatus.RTTStatusCode = WL.RTTCurrentStatusCode

inner join 
	(
	select
		*
	from
		APC.WaitingListSuspension Suspension
	where
		upper(Suspension.SuspensionReason) like '%S*%'
	and	not exists
		(
		select
			1
		from
			APC.WaitingListSuspension LastSuspension
		where
			LastSuspension.SourcePatientNo = Suspension.SourcePatientNo
		and	upper(LastSuspension.SuspensionReason) like '%S*%'
		and	LastSuspension.SourceEncounterNo = Suspension.SourceEncounterNo
		and	LastSuspension.CensusDate = Suspension.CensusDate
		and
			(
				LastSuspension.SuspensionStartDate > Suspension.SuspensionStartDate
			or
				(
					LastSuspension.SuspensionStartDate = Suspension.SuspensionStartDate
				and	LastSuspension.SuspensionRecno > Suspension.SuspensionRecno
				)
			)
		)
	) Suspension
on	Suspension.SourcePatientNo = WL.SourcePatientNo
and	Suspension.SourceEncounterNo = WL.SourceEncounterNo
and	Suspension.CensusDate = WL.CensusDate

where
	WL.CensusDate = @CensusDate
and	WL.WLStatus = 'WL Suspend'
and	(
		WL.SpecialtyCode = @SpecialtyCode
	or	@SpecialtyCode is null
	)

and	(
		Site.SiteCode = @InterfaceCode
	or	@InterfaceCode is null
	)

and	(
		Consultant.ConsultantCode = @ConsultantCode
	or	@ConsultantCode is null
	)

and	(
		left(WL.PCTCode, 3) = @PCTCode
	or	@PCTCode is null
	)

and	(
		WL.PriorityCode = @PriorityCode
	or	@PriorityCode is null
	)

and	(
		WL.ManagementIntentionCode = @ManagementIntentionCode
	or	@ManagementIntentionCode is null
	)

order by
	datediff(day, coalesce(WL.RTTStartDate, WL.OriginalDateOnWaitingList), WL.CensusDate)
	 - SocialSuspension.DaysSuspended desc,

--	WL.KornerWait desc,
	WL.SiteCode,

	case 
		when WL.ConsultantCode is null then 'No Consultant'
		when WL.ConsultantCode = '' then 'No Consultant'
		else
			case 
				when Consultant.Surname is null  then WL.ConsultantCode + ' - No Description'
				else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
			end
	end,

	WL.PatientSurname
