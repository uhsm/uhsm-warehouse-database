﻿CREATE procedure [dbo].[ReportRTTTCIIP]

	 @SpecialtyCode varchar (10) = null
	,@HospitalCode varchar (5) = null
	,@ConsultantCode varchar (20) = null
	,@PCTCode varchar (5) = null
	,@ReviewedFlag bit = null
	,@UnknownClockStartFlag bit = null
as


declare @CensusDate smalldatetime
declare @LatestCensusDate smalldatetime
declare @nextMonday smalldatetime


set @CensusDate = (select max(CensusDate) from WHOLAP.dbo.OlapCensus where IsRTT = 'true')


set @LatestCensusDate = (select max(CensusDate) from APC.Snapshot)


set @nextMonday = dateadd(day, ((datepart(dw, dateadd(day, datediff(day, 0, getdate()), 0)) - 2) * -1) + 7, dateadd(day, datediff(day, 0, getdate()), 0) )


---- Patients admitted for which we have no referral link 


--select
--	dateadd(day,
--	coalesce(RTTAdjustmentIP.AdjustmentDays, 0)
--	,RTTClockStartIP.ClockStartDate
--	) AdjustedClockStartDate




--	,Inpatient.SpecialtyCode SpecialtyCode


--	,Specialty.Specialty


--	,HospitalCode =
--	Site.Abbreviation


--	,Hospital =
--	case 
--	when Inpatient.SiteCode is null then 'Unknown Site' 
--	when Site.Site is null then Inpatient.SiteCode + ' - No Description' 
--	else Site.Site
--	end


--	,Consultant =
--	case 
--	when upper(Inpatient.Operation) like '%PELVIC SERVICE' then '*' 
--	when upper(Inpatient.Operation) like '%TGL' then '#'
--	else '' 
--	end +
--	case 
--	when Inpatient.ConsultantCode is null then 'No Consultant'
--	when Inpatient.ConsultantCode = '' then 'No Consultant'
--	else
--	case 
--	when Consultant.Surname is null  then Inpatient.ConsultantCode + ' - No Description'
--	else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
--	end
--	end


--	,Inpatient.DistrictNo DistrictNo


--	,Inpatient.PatientSurname PatientSurname


--	,Inpatient.IntendedPrimaryOperationCode PrimaryOperationCode


--	,left(Inpatient.IntendedPrimaryOperationCode, 3) OperationCode


--	,Inpatient.ManagementIntentionCode ManagementIntentionCode
--	,Inpatient.AdmissionMethodCode AdmissionMethodCode
--	,Inpatient.PriorityCode PriorityCode
--	,Inpatient.DateOnWaitingList
--	,coalesce(InpatientSinceRTTBuild.TCIDate, Inpatient.TCIDate) TCIDate
--	,Inpatient.PCTCode PCTCode
--	,PCT.Organisation PCT
--	,Inpatient.SexCode SexCode
--	,Inpatient.DateOfBirth
--	,coalesce(Inpatient.EpisodicGpPracticeCode, Inpatient.RegisteredPracticeCode) GpPracticeCode


--	,case coalesce(RTTEncounter.ReviewedFlag, convert(bit, 0))
--	when 0 then 'N'
--	else 'Y'
--	end ReviewedFlag


--	,MaintenanceURL =
--	(
--	select
--	TextValue
--	from
--	dbo.Parameter
--	where
--	Parameter = 'MAINTENANCEURL'
--	)
--	+ '?SourcePatientNo=' + Inpatient.SourcePatientNo
--	+ '&SourceEntityRecno=' + Inpatient.SourceEntityRecno


--	,FactRTT.WeekBandCode


--	,WeekBand.DurationBand WeekBand


--	,FactRTT.BreachBandCode


--	,BreachBand.BreachBand


--	,FactRTT.TCIBeyondBreach
--from
--	WHOLAP.dbo.FactRTT FactRTT

--inner join WHOLAP.dbo.BaseRTT Inpatient
--on Inpatient.SourceEncounterRecno = FactRTT.SourceEncounterRecno
--and	Inpatient.SourceCode = FactRTT.SourceCode


--left join APC.WaitingList InpatientSinceRTTBuild
--on InpatientSinceRTTBuild.SourceUniqueID = Inpatient.WaitingListSourceUniqueID
--and InpatientSinceRTTBuild.CensusDate = @LatestCensusDate


----left join dbo.RTTEncounter
----on RTTEncounter.SourcePatientNo = Inpatient.SourcePatientNo
----and RTTEncounter.SourceEntityRecno = Inpatient.SourceEntityRecno


----left join dbo.RTTClockStart RTTClockStartIP
----on RTTClockStartIP.SourcePatientNo = Inpatient.SourcePatientNo
----and RTTClockStartIP.SourceEntityRecno = Inpatient.SourceEntityRecno
----and not exists
----(
----select
----1
----from
----dbo.RTTClockStart
----where
----RTTClockStart.SourcePatientNo = RTTClockStartIP.SourcePatientNo
----and RTTClockStart.SourceEntityRecno = RTTClockStartIP.SourceEntityRecno
----and (
----RTTClockStart.ClockStartDate > RTTClockStartIP.ClockStartDate
----or
----(
----RTTClockStart.ClockStartDate = RTTClockStartIP.ClockStartDate
----and RTTClockStart.RTTClockStartRecno > RTTClockStartIP.RTTClockStartRecno
----)
----)
----)


----left join 
----(
----select
----RTTAdjustment.SourcePatientNo
----,RTTAdjustment.SourceEntityRecno
----,sum(RTTAdjustment.AdjustmentDays) AdjustmentDays
----from
----dbo.RTTAdjustment
----group by
----RTTAdjustment.SourcePatientNo
----,RTTAdjustment.SourceEntityRecno
----) RTTAdjustmentIP
----on RTTAdjustmentIP.SourcePatientNo = Inpatient.SourcePatientNo
----and RTTAdjustmentIP.SourceEntityRecno = Inpatient.SourceEntityRecno


--left join PAS.Consultant ConsultantLocal
--on ConsultantLocal.ConsultantCode = Inpatient.ConsultantCode


--left join Consultant Consultant 
--on Consultant.ConsultantCode = ConsultantLocal.NationalConsultantCode

--left join SpecialtyPennine Specialty
--on Specialty.SpecialtyCode = Inpatient.SpecialtyCode

--left join Organisation.dbo.PCT PCT
--on PCT.OrganisationCode = left(Inpatient.PCTCode, 3)


--left join WHOLAP.dbo.OlapDurationWeeks WeekBand
--on WeekBand.WaitDurationCode = FactRTT.WeekBandCode


--left join WHOLAP.dbo.OlapBreachBand BreachBand
--on BreachBand.BreachBandCode = FactRTT.BreachBandCode


--where
--	FactRTT.CensusDate = @CensusDate
--and	FactRTT.SourceCode = 'APCWL'
--and FactRTT.AdjustedFlag = 'N'


--and FactRTT.EncounterTypeCode = 'UNKIPWL'
--and FactRTT.BreachBandCode = 'UK'


--and (
--FactRTT.SpecialtyCode = @SpecialtyCode
--or coalesce(@SpecialtyCode, '0') = '0'
--)


--and (
--FactRTT.SiteCode = @HospitalCode
--or coalesce(@HospitalCode, '0') = '0'
--)


--and (
--FactRTT.ConsultantCode = @ConsultantCode
--or coalesce(@ConsultantCode, '0') = '0'
--)


--and (
--FactRTT.PCTCode = @PCTCode
--or coalesce(@PCTCode, '0') = '0'
--)


--and (
--coalesce(RTTEncounter.ReviewedFlag, convert(bit, 0)) = @ReviewedFlag
--or @ReviewedFlag is null
--)


--and coalesce(@UnknownClockStartFlag, 1) = 1


--and coalesce(InpatientSinceRTTBuild.TCIDate, Inpatient.TCIDate) between @nextMonday and dateadd(day, 6, @nextMonday)



 
 
