﻿CREATE procedure [dbo].[ReportReadmissions]

   @Specialty varchar(10),
   @MonthYear varchar(10)


as


SELECT     c.TheMonth, c.FinancialMonthKey, fd.SpecialtyCode, fd.EndWardCode, COUNT(fd.EncounterRecno) AS discharges,
                          (SELECT     COUNT(r.ReadmissionRecno) AS Expr1
                            FROM          WHOLAP.dbo.FactReadmission AS r INNER JOIN
                                                   whreporting.APC.Episode AS ep ON ep.EncounterRecno = r.PreviousAdmissionRecno LEFT OUTER JOIN
                                                   WHOLAP.dbo.OlapAdmissionMethod AS am ON am.AdmissionMethodCode = r.ReadmissionAdmissionMethodCode LEFT OUTER JOIN
                                                   WHOLAP.dbo.OlapCalendar AS cal ON cal.TheDate = ep.DischargeDate
                            WHERE      (ep.EndWardCode = fd.EndWardCode) AND (ep.SpecialtyCode = fd.SpecialtyCode) AND (am.AdmissionMethodTypeCode = 2003471) AND 
                                                   (r.ReadmissionIntervalDays < 31) AND (c.TheMonth = cal.TheMonth)) AS readmissions
FROM         WHOLAP.dbo.FactDischarge AS fd INNER JOIN
                      WHOLAP.dbo.OlapCalendar AS c ON c.TheDate = CAST(fd.EpisodeEndDateTime AS DATE)
WHERE     (fd.SpecialtyCode IN (@Specialty)) AND (c.FinancialMonthKey IN (@MonthYear))
GROUP BY fd.SpecialtyCode, fd.EndWardCode, c.TheMonth, c.FinancialMonthKey