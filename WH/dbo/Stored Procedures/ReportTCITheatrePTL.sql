﻿CREATE       procedure [dbo].[ReportTCITheatrePTL] 

	 @FromDate smalldatetime
	,@ToDate smalldatetime = null
	,@ManagementIntentionCode int = null

as

declare @CensusDate smalldatetime

set @CensusDate = (select coalesce(@CensusDate, max(CensusDate)) from APC.Snapshot)

set @ToDate = coalesce(@ToDate, dateadd(day, 6, @FromDate))

select
	 WL.ProcedureTime
	,EpisodeID = PatientBooking.SourceUniqueID

	,WL.EncounterRecno
	,WL.InterfaceCode
	,WL.DistrictNo
	,SexCode = Sex.MainCode
	,WL.PatientSurname
	,WL.DateOfBirth

	,WL.TCIDate

	,DayOfWeek =
		datename(dw, coalesce(WL.TCIDate, dateadd(day, datediff(day, 0, WL.ProcedureTime), 0)))

	,SessionDate = PatientBooking.SessionStartTime

	,Specialty.Specialty

	,Consultant =
		case 
		when upper(WL.Operation) like '%PELVIC SERVICE' then '*' 
		when upper(WL.Operation) like '%TGL' then '#'
		else '' 
		end +
		case 
			when WL.ConsultantCode is null then 'No Consultant'
			when WL.ConsultantCode = '' then 'No Consultant'
			else
				case 
					when Consultant.Surname is null  then convert(varchar, WL.ConsultantCode) + ' - No Description'
					else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
				end
		end

	,WL.Operation

	,OperationCode = WL.IntendedPrimaryOperationCode 

	,ManagementIntentionCode = ManagementIntention.NationalManagementIntentionCode

	,WL.DateOnWaitingList

	,WL.OriginalDateOnWaitingList

	,WL.KornerWait

	,MonthsWaiting =
		convert(int, coalesce(WL.KornerWait, 0) / 30.4)

	,TotalMonthsWaiting =
		convert(int, datediff(day, WL.OriginalDateOnWaitingList, WL.CensusDate) / 30.4)

	,ActivePlannedFlag =
		case
		when WL.AdmissionMethodCode = 8812 then 'P'
		else 'A'
		end

	,WaitingListBreach.BreachDate

	,SameDayCancellationBreachDate =
		(
		select
			max(SameDayCancellation.BreachDate) BreachDate
		from
			Theatre.SameDayCancellation SameDayCancellation
		where
			SameDayCancellation.SourceUniqueID = WL.TheatrePatientBookingKey
		and	WL.ManagementIntentionCode = 2000619 --At Least One Night
		and	WL.AdmissionMethodCode <> 8812 --Planned
		)

	,TheatreSessionPeriod =
		case
		when WL.ProcedureTime is null then 'N/R'
		when datediff(minute, Session.StartTime, WL.ProcedureTime) < 720  then 'AM'
		else 'PM'
		end

	,WL.WardCode

--	Episode.SourceUniqueID EpisodeEpisodeID,

	,PriorityCode =
		Priority.NationalPriorityCode

	,TCIBackgroundColour =
		case
	-- breach
		when WL.CensusDate > WaitingListBreach.BreachDate then 'red'
	-- booked beyond breach
		when WaitingListBreach.BreachDate < WL.TCIDate then 'pink'
	-- procedure date
	--	when WL.TCIDate is null and dateadd(day, datediff(day, 0, WL.ProcedureTime), 0) is not null then 'yellow'
	-- urgent
		when WL.TCIDate is null and datediff(day, WL.CensusDate, WaitingListBreach.BreachDate) <= 28 then 'blue'
	-- no tci
		when WL.TCIDate is null then 'deepskyblue'
	--	DN CANCEL is black with white text
	-- default
		else ''
		end

	,ExpectedLOS = datediff(day, WL.ExpectedAdmissionDate, WL.ExpectedDischargeDate)

	,WL.AdmissionReason

	,MRSA =
		case when WL.MRSAFlag = 'true' then 'Y' else '' end
from
	APC.WaitingList WL WITH (NOLOCK)

inner join APC.WaitingListBreach
on	WaitingListBreach.SourceUniqueID = WL.SourceUniqueID
and	WaitingListBreach.CensusDate = WL.CensusDate

left join Theatre.PatientBooking
on	PatientBooking.SourceUniqueID = WL.TheatrePatientBookingKey

left join Theatre.Session
on	Session.SourceUniqueID = PatientBooking.PatientSourceUniqueID

left join PAS.Consultant Consultant 
on	Consultant.ConsultantCode = WL.ConsultantCode

left join Operation Operation
on	Operation.OperationCode = left(WL.IntendedPrimaryOperationCode, 3)

left join PAS.Specialty
on	Specialty.SpecialtyCode = WL.SpecialtyCode

left join PAS.ManagementIntention
on	ManagementIntention.ManagementIntentionCode = WL.ManagementIntentionCode

left join PAS.Priority Priority
on	Priority.PriorityCode = WL.PriorityCode

left join PAS.ReferenceValue Sex
on	Sex.ReferenceValueCode = WL.SexCode

where
	WL.CensusDate = @CensusDate

and	coalesce(WL.TCIDate, dateadd(day, datediff(day, 0, WL.ProcedureTime), 0)) between @FromDate and @ToDate

and	WL.AdmissionMethodCode in 
		(
		 8811	--Elective - Booked	12
		,8814	--Maternity ante-partum	31
		,8812	--Elective - Planned	13
		,13		--Not Specified	NSP
		,8810	--Elective - Waiting List	11
		)

and	WL.WLStatus != 'Suspended'

and	(
		WL.ManagementIntentionCode = @ManagementIntentionCode
	or	@ManagementIntentionCode is null
	)

and	(
	WL.FutureCancelDate != WL.TCIDate
or	WL.FutureCancelDate is null
	)

order by
	coalesce(
		WL.TCIDate
		,dateadd(day, datediff(day, 0, WL.ProcedureTime), 0)
	)

	,coalesce(
		 WL.ProcedureTime
		,WaitingListBreach.BreachDate
		,dateadd(minute, 1439, WL.TCIDate)
		,dateadd(minute, 1439, dateadd(day, datediff(day, 0, WL.ProcedureTime), 0))
	)

	,case 
	when upper(WL.Operation) like '%PELVIC SERVICE' then '*' 
	when upper(WL.Operation) like '%TGL' then '#'
	else '' 
	end +
	case 
		when WL.ConsultantCode is null then 'No Consultant'
		when WL.ConsultantCode = '' then 'No Consultant'
		else
			case 
				when Consultant.Surname is null  then convert(varchar, WL.ConsultantCode) + ' - No Description'
				else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
			end
	end

	,WL.PriorityCode
	,WaitingListBreach.BreachDate
	,Session.StartTime
