﻿CREATE procedure [dbo].[ReportTheatreKPIbyTheatre]

    @Period varchar (10)
     ,@Day varchar (15)  
  ,@Specialty as int



as

select
s.NationalSpecialtyCode,
s.Specialty,
th.Theatre,
c.TheMonth,
t.KPI,
SUM(Actual)as Value

from WHOLAP.dbo.FactTheatreKPI t
inner join WHOLAP.dbo.OlapTheatreSpecialty s on s.SpecialtyCode=t.SpecialtyCode
inner join WHOLAP.dbo.OlapTheatre th on th.TheatreCode=t.TheatreCode
inner join WHOLAP.dbo.OlapCalendar c on c.TheDate=t.SessionDate

where c.TheMonth=@Period
and c.[DayOfWeek]in(@Day)
and s.SpecialtyCode in (@Specialty)

group by
s.NationalSpecialtyCode,
s.Specialty,
th.Theatre,
c.TheMonth,
t.KPI