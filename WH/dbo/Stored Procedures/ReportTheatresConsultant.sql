﻿CREATE procedure [dbo].[ReportTheatresConsultant]
  @DateFrom datetime
 ,@DateTo datetime
  
   
as  

select 
Th.OperatingSuite,
st.StaffName,
avg(PlannedMinutes) as AveragePlanned
,avg(UtilisedMinutes) as AverageUtilised
,COUNT(sourceuniqueid) as CountSessions
,COUNT(opid) as CountOperations

from


(SELECT     Session.SourceUniqueID,TheatreCode=Session.TheatreCode, ConsultantCode = Session.ConsultantCode, PlannedMinutes = datediff(minute, Session.StartTime, 
                      Session.EndTime), UtilisedMinutes=datediff(minute, Session.ActualSessionStartTime,Session.EndTime)
,op.SourceUniqueID as opid
FROM         WHOLAP.dbo.BaseTheatreSession Session
right outer join wholap.dbo.BaseTheatreOperation op on op.SessionSourceUniqueID=Session.SourceUniqueID 

where CAST(StartTime AS DATE)BETWEEN @DateFrom AND @DateTo
and    Session.CancelledFlag = 0) as s

left outer join WHOLAP.dbo.OlapTheatreStaff st on st.StaffCode=s.ConsultantCode
left outer join WHOLAP.dbo.OlapTheatre th on th.TheatreCode=s.TheatreCode
group by Th.OperatingSuite,
st.StaffName

order by Th.OperatingSuite




