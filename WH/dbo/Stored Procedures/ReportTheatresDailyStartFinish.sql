﻿CREATE procedure [dbo].[ReportTheatresDailyStartFinish]
  @DateFrom datetime
 ,@DateTo datetime
 ,@OperatingSuite varchar(1)
 ,@Session int
  -- ,@starttimerange varchar (3)
as  

select 

ts.SessionNumber,
Th.OperatingSuite,
th.Theatre,
cast(actualsessionstarttime as DATE) as SessionDate
,actualsessionstarttime
,actualsessionendtime
,ts.StartTime
,ts.EndTime
,DATEDIFF(minute,actualsessionstarttime,actualsessionendtime) as Utilised
,DATEDIFF(minute,ts.starttime,ts.endtime) as Planned
,c.DayOfWeek
,st.StaffName
,sp.Specialty
,
count(distinct case when (actualsessionendtime<ts.endtime) then ts.SourceUniqueID else null end) as Underruns
,count(distinct ts.SourceUniqueID) as CountSessions
,count(distinct case when (actualsessionendtime>ts.endtime) then ts.SourceUniqueID else null end) as Overruns
,count(distinct case when (actualsessionstarttime>ts.starttime) then ts.SourceUniqueID else null end) as LateStarts
,count(distinct case when (actualsessionstarttime>ts.starttime and actualsessionendtime>ts.endtime) then ts.SourceUniqueID else null end) as LateStartLateFinish
,count(distinct case when (actualsessionstarttime>ts.starttime and actualsessionendtime<ts.endtime) then ts.SourceUniqueID else null end) as LateStartEarlyFinish
,
case when 
DATEDIFF(minute,ts.starttime,actualsessionstarttime)<1
then 'OnTime/Early'
when DATEDIFF(minute,ts.starttime,actualsessionstarttime) between 1 and 15 then '1-15'
when DATEDIFF(minute,ts.starttime,actualsessionstarttime) between 16 and 30 then '16-30'
when DATEDIFF(minute,ts.starttime,actualsessionstarttime) between 31 and 45 then '31-45'
when DATEDIFF(minute,ts.starttime,actualsessionstarttime) between 46 and 60 then '46-60'
else '>60' end as starttimerange,

DATEDIFF(minute,ts.starttime,actualsessionstarttime) as delay

,OperatingSuiteCode
,DIT_SOURCE as DelayStage
,PR_DESC as DelayDescription


from 

WHOLAP.dbo.BaseTheatreSession ts
inner join WHOLAP.dbo.OlapTheatre th on th.TheatreCode=ts.TheatreCode
inner join WHOLAP.dbo.OlapCalendar c on c.TheDate=CAST(starttime as DATE)
left outer join WHOLAP.dbo.OlapTheatreStaff st on st.StaffCode=ts.ConsultantCode
left outer join WHOLAP.dbo.OlapTheatreSpecialty sp on sp.SpecialtyCode=ts.SpecialtyCode

INNER JOIN
                          (SELECT     TheatreOperation.SessionSourceUniqueID, OperationProductiveTime = SUM(TheatreOperation.OperationProductiveTime)
                            FROM          wholap.dbo.FactTheatreOperation TheatreOperation
                            GROUP BY TheatreOperation.SessionSourceUniqueID) TheatreOperation ON 
                      TheatreOperation.SessionSourceUniqueID = ts.SourceUniqueID

LEFT OUTER JOIN Theatre.[Session] WHSession on WHSession.SourceUniqueID=ts.SourceUniqueID
LEFT JOIN ORMIS.DBO.f_delay_items fi (nolock) 
         ON WHSession.SourceUniqueID = fi.dit_op_sequ 
       left JOIN ormis.dbo.fprocdy fp (nolock) 
         ON fp.pr_sequ = fi.dit_pr_sequ  


where 
cast(actualsessionstarttime as DATE) between @DateFrom and @DateTo
and cast(OperatingSuiteCode as varchar) in (@OperatingSuite)

and ts.SessionNumber in (@Session)



and ts.CancelledFlag <>1 and ts.TheatreCode not in (4,5)
/*and case when DATEDIFF(minute,starttime,actualsessionstarttime) between 1 and 15 then '15'
when DATEDIFF(minute,starttime,actualsessionstarttime) between 16 and 30 then '30'
when DATEDIFF(minute,starttime,actualsessionstarttime) between 31 and 45 then '45'
when DATEDIFF(minute,starttime,actualsessionstarttime) between 46 and 60 then '60'
else '>60' end in (@starttimerange)*/
group by

ts.SessionNumber,
Th.OperatingSuite,
th.Theatre,
cast(actualsessionstarttime as DATE)
,actualsessionstarttime
,actualsessionendtime
,ts.StartTime
,ts.EndTime
,DATEDIFF(minute,actualsessionstarttime,actualsessionendtime)
,DATEDIFF(minute,ts.starttime,ts.endtime) 
,c.DayOfWeek
,st.StaffName
,sp.Specialty
,ts.SourceUniqueID
,OperatingSuiteCode
,DIT_SOURCE
,PR_DESC