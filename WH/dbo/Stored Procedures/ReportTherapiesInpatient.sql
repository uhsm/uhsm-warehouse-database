﻿CREATE procedure [dbo].[ReportTherapiesInpatient]
  @DateFrom datetime
     ,@DateTo datetime
     ,@ReferringWard varchar (50)
     ,@ReferringConsultant varchar (25)
  
   
as  




select
ep.EncounterRecno,
ep.EndWard,
ep.ConsultantName,
ep.EpisodeStartDateTime,
ep.EpisodeEndDateTime,
ref.ReferralReceivedDate,
ref.ReceivingProfCarerName,
ref.ReceivingProfCarerType,
ref.[ReceivingSpecialty(Function)],
cont.cont_date,
cont.arrival_time,
cont.depart_time,
cont.outcome 







from whreporting.dbo.TherapiesContactPathway t

inner join 
whreporting.APC.Episode ep on ep.EncounterRecno=t.IPEpisodeEncounterRecNo
inner join whreporting.RF.Referral ref on ref.ReferralSourceUniqueID=t.ReferralSourceUniqueID
inner join infosql.information_reporting.dbo.contacts_dataset cont on cont.cont_ref=t.cont_ref

where t.ContactOrder=1
and ref.ReferralReceivedDate between @DateFrom and @DateTo
and EndWardCode in (@ReferringWard)
and ConsultantCode in (@ReferringConsultant)


