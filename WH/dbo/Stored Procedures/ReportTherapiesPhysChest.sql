﻿create procedure [dbo].[ReportTherapiesPhysChest]


as




select
Clinic
,appointmenttype
,status
,c.TheMonth
,op.Outcome
,c.FinancialMonthKey
,opc.PrimaryDiagnosis
,COUNT(op.encounterrecno) as CountAppointments


 from whreporting.OP.Schedule op
 inner join whreporting.LK.Calendar c on c.TheDate=op.AppointmentDate
 left outer join whreporting.OP.ClinicalCoding opc on opc.EncounterRecno=op.EncounterRecno

where Clinic like 'Physchest%'
and AppointmentDate between '2012-06-01' and '2012-06-30'

group by
Clinic
,appointmenttype
,status
,c.TheMonth
,op.Outcome
,c.FinancialMonthKey
,opc.PrimaryDiagnosis