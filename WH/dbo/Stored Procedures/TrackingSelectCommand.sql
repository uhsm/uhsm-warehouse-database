﻿create     PROCEDURE [dbo].[TrackingSelectCommand] @TrackingRecno int

AS
	SET NOCOUNT ON;

SELECT
	Tracking.TrackingRecno, 
	Tracking.EpisodeID,
	Tracking.InterfaceCode, 
	Tracking.AdmissionDate, 
	Tracking.PASCancelDate, 
	Tracking.TCIDate, 
	Tracking.TreatedDate, 
	convert(bit, case when Tracking.DisableTrackingDate is null then 0 else 1 end) DisableTrackingFlag, 
	Tracking.DisableTrackingDate, 
	Tracking.Created, 
	Tracking.Updated, 
	Tracking.ByWhom,
	coalesce(Episode.SourcePatientNo, '') DistrictNo,
	coalesce(Episode.ProcedureDate, '') ProcedureDate,
	Site.Interface Site,
	coalesce(Theatre.Theatre, '') Theatre,
	coalesce(Specialty.Specialty, '') Specialty,
	coalesce(Consultant.Consultant, '') Consultant,
	coalesce(Tracking.Comment, '') Comment,
	convert(bit, coalesce(Tracking.ReportableFlag, 0)) ReportableFlag 
FROM
	Theatre.Tracking

left join Theatre.PatientEpisode Episode
on	Episode.SourceUniqueID = Tracking.EpisodeID
and	Episode.InterfaceCode = Tracking.InterfaceCode

left join Theatre.LegacySessions Session
on	Session.ID = Episode.SessionID
and	Session.InterfaceCode = Episode.InterfaceCode

inner join Theatre.Theatre Theatre
on	Theatre.TheatreCode = Episode.TheatreNumber
and	Theatre.InterfaceCode = Episode.InterfaceCode

left join Interface Site
on	Site.InterfaceCode = Episode.InterfaceCode

left join Theatre.Consultant Consultant
on	Consultant.ConsultantCode = Session.ConsultantCode
and	Consultant.InterfaceCode = Episode.InterfaceCode

left join Theatre.Specialty Specialty
on	Specialty.SpecialtyCode = Consultant.SpecialtyCode

where
	TrackingRecno = @TrackingRecno
