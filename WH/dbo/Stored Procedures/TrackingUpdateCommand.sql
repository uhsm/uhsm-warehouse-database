﻿CREATE    PROCEDURE [dbo].[TrackingUpdateCommand]
(
	@TrackingRecno int,
	@AdmissionDate smalldatetime,
	@PASCancelDate smalldatetime,
	@TCIDate smalldatetime,
	@TreatedDate smalldatetime,
	@DisableTrackingFlag bit,
	@Comment varchar(4000),
	@ReportableFlag bit
)
AS

SET NOCOUNT OFF;

UPDATE Theatre.Tracking 
SET 
	AdmissionDate = @AdmissionDate, 
	PASCancelDate = @PASCancelDate, 
	TCIDate = @TCIDate, 
	TreatedDate = @TreatedDate, 
	DisableTrackingDate = case when @DisableTrackingFlag = 1 then coalesce(DisableTrackingDate, getdate()) else null end, 
	Comment = @Comment,
	Updated  = getdate() ,
	ByWhom = suser_sname(),
	ReportableFlag = @ReportableFlag
WHERE
	TrackingRecno = @TrackingRecno
