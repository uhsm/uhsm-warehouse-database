﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		To update Referral, Op and APC tables with Flag to distinguish RM2 as provider activity and non RM2 provider activity
				Set up initially for purposes of SRFT Breast Surgery activity

Notes:			Stored in PAS

Versions:		
				1.0.0.0 - 11/04/2016 - CM
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/

CREATE Procedure  [dbo].[UpdateFlagsForNonRM2Activity]

As


Begin Try
Declare @StartTime datetime
Declare @Elapsed int
Declare @Stats varchar(255)

Set @StartTime = GETDATE()


--Update RF.Encounter Table 
Update RF.Encounter

SET OurActivityFlag = CASE WHEN SpecialtyCode = '2000780'--this is code for Breast Surgery
						AND(ReferralComment LIKE 'RM3%' OR GeneralComment LIKE 'RM3%') --this is how Salford Refs are being flagged by booking and scheduling team
						THEN 'N'
						ELSE 'Y' 
						END ,
	NotOurActProvCode =  CASE WHEN SpecialtyCode = '2000780'
						AND (ReferralComment LIKE 'RM3%' OR GeneralComment LIKE 'RM3%') THEN 'RM3'
						ELSE 'NA'
						END
					
FROM RF.Encounter
inner join (SELECT * 
			FROM Lorenzo.dbo.ExtractDatasetEncounter
			WHERE DatasetCode = 'Referral') Refs on Encounter.SourceUniqueID = Refs.SourceUniqueID--join back to Lorenzo.dbo.ExtractDatasetEncounter to find only the net change referrals that need updating for the day



Exec WriteAuditLogEvent 'PAS - WH NonRM2Flags-ReferralsUpdated','',@StartTime



--Update OP.Encounter Table 

UPDATE OP.Encounter
SET OurActivityFlag = CASE WHEN RF.OurActivityFlag = 'N' 
							AND RF.NotOurActProvCode = 'RM3'
							AND OP.SpecialtyCode = '2000780'
						THEN 'N'
						ELSE 'Y'
						END ,
	NotOurActProvCode = CASE WHEN RF.OurActivityFlag = 'N' 
							AND RF.NotOurActProvCode = 'RM3'
							AND OP.SpecialtyCode = '2000780'
						THEN 'RM3'
						ELSE 'NA'
						END 
	
FROM OP.Encounter OP
inner join (SELECT * 
			FROM Lorenzo.dbo.ExtractDatasetEncounter
			WHERE DatasetCode = 'OUTPATIENT') Refs on OP.SourceUniqueID = Refs.SourceUniqueID--join back to Lorenzo.dbo.ExtractDatasetEncounter to find only the net change appointments that have been updated today

left outer join RF.Encounter RF on OP.ReferralSourceUniqueID = RF.SourceUniqueID --join to RF.Referrals to get the OurActivityFlag and NotOurActProvCode that has already been set for the referral



Exec WriteAuditLogEvent 'PAS - WH NonRM2Flags-OPUpdated','',@StartTime



--Update APC.Encounter Table 

UPDATE APC.Encounter
SET OurActivityFlag = CASE WHEN RF.OurActivityFlag = 'N' 
							AND RF.NotOurActProvCode = 'RM3'
							AND APC.SpecialtyCode = '2000780'
						THEN 'N'
						ELSE 'Y'
						END ,
	NotOurActProvCode = CASE WHEN RF.OurActivityFlag = 'N' 
							AND RF.NotOurActProvCode = 'RM3'
							AND APC.SpecialtyCode = '2000780'
						THEN 'RM3'
						ELSE 'NA'
						END 
	
FROM APC.Encounter APC
inner join (SELECT * 
			FROM Lorenzo.dbo.ExtractDatasetEncounter
			WHERE DatasetCode = 'APC') Refs on APC.SourceUniqueID = Refs.SourceUniqueID--join back to Lorenzo.dbo.ExtractDatasetEncounter to find only the net change episodes that have been updated today

left outer join RF.Encounter RF on APC.ReferralSourceUniqueID = RF.SourceUniqueID --join to RF.Referrals to get the OurActivityFlag and NotOurActProvCode that has already been set for the referral


Exec WriteAuditLogEvent 'PAS - WH NonRM2Flags-APCUpdated','',@StartTime






Exec WriteAuditLogEvent 'PAS - WH NonRM2FlagsALLUpdated', '', @StartTime



End Try


Begin Catch
	Exec msdb.dbo.sp_send_dbmail
	@profile_name = 'Business Intelligence',
	@recipients = 'Business.Intelligence@uhsm.nhs.uk',
	@subject = 'WH - Update NonRM2ActivityFlags',
	@body_format = 'HTML',
	@body = 'dbo.UpdateFlagsForNonRM2Activity failed.<br><br>';
End Catch
