﻿

/*
--Author: K Oakden
--Date created: 07/03/2013
Existing IPM extracts have  a small number of missing patient demographics
for patients to 'belonging' to RM2. THis cannot be resolved by CSC until we upgrade
to the LE2.2 extracts.

Part 4 of [dbo].[LoadMissingPatient]. Update encounter details with missing patient data
*/
CREATE procedure [dbo].[UpdateMissingPatientAPCEncounter]
as

declare @StartTime datetime
declare @Elapsed int
declare @RowsUpdated Int
declare @Stats varchar(255)

select @StartTime = getdate()

select @RowsUpdated = 0

--select 
--	Encounter.SourceUniqueID
--	,Encounter.SourceSpellNo
--	,Encounter.SourcePatientNo
--	,Encounter.AdmissionTime
update APC.Encounter
set 
	PatientForename = LPI.PatientForename
	,PatientSurname = LPI.PatientSurname
	,NHSNumber = LPI.NHSNumber
	,NHSNumberStatusCode = 
		case
			when LPI.NHSNumberStatusCode = '01' then 'SUCCS' 
			when LPI.NHSNumberStatusCode = '02' then 'RESET' 
			else LPI.NHSNumberStatusCode
		end
	,DistrictNo = LPI.LocalPatientID
	,Postcode = LPI.Postcode
	,SexCode = 
		case 
			when LPI.Sex = 'M' then 9270 
			when LPI.Sex = 'F' then 9269 
			when LPI.Sex is not null then 3006530
		end
    ,DateOfBirth = 
		case 
			when (LPI.dob is null or LPI.dob = '') then null 
			else convert(datetime, right(LPI.dob,2) + '/' + substring(LPI.dob,5,2) + '/' + left(LPI.dob,4) , 103)
		end
    ,RegisteredGpCode = GP.ProfessionalCarerCode
    ,RegisteredGpPracticeCode = Practice.OrganisationCode

from APC.Encounter Encounter

inner join dbo.vwMissingPatients Missing
	on Missing.SourceUniqueID = Encounter.SourceSpellNo
	and Missing.PatientClass = 'I'
	
left join PAS.MissingPatient LPI
	on LPI.SourcePatientNo = Encounter.SourcePatientNo
	and LPI.Archived = 'N'
	
left join PAS.ProfessionalCarer GP
	on GP.ProfessionalCarerMainCode = LPI.GPCode
	and GP.ArchiveFlag = 'N'
	and GP.EndDate is null
	and not exists 
		(select 1 from PAS.ProfessionalCarer Subsequent
		where GP.ProfessionalCarerMainCode = Subsequent.ProfessionalCarerMainCode
		and Subsequent.StartDate > GP.StartDate)
		
left join PAS.Organisation Practice
	on Practice.OrganisationLocalCode = LPI.PracticeCode
	and Practice.ParentOrganisationCode is not null
	
where LPI.LocalPatientID is not null
and Encounter.DistrictNo is null
--order by Encounter.SourceSpellNo,	Encounter.SourceUniqueID

select @RowsUpdated = @@rowcount

SELECT @Elapsed = DATEDIFF(SECOND,@StartTime,getdate())

SELECT @Stats = 
	'Rows Updated ' + CONVERT(varchar(10), @RowsUpdated) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Secs'

EXEC WriteAuditLogEvent 'PAS - WH UpdateMissingPatientAPCEncounter', @Stats, @StartTime

print @Stats
