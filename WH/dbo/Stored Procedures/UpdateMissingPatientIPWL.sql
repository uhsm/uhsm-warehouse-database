﻿

/*
--Author: K Oakden
--Date created: 13/01/2014
Existing IPM extracts have  a small number of missing patient demographics
for patients to 'belonging' to RM2. THis cannot be resolved by CSC until we upgrade
to the LE2.2 extracts.

Part 6 of [dbo].[LoadMissingPatient]. Update waiting list details with missing patient data
*/
CREATE procedure [dbo].[UpdateMissingPatientIPWL]
as

declare @StartTime datetime
declare @Elapsed int
declare @RowsUpdated Int
declare @Stats varchar(255)

select @StartTime = getdate()

select @RowsUpdated = 0


update PTL.IPWL
set 

--select
--	encounter.SourceUniqueID
	PatientForename = LPI.PatientForename
	,PatientSurname = LPI.PatientSurname
	,NHSNumber = LPI.NHSNumber
	,DistrictNo = LPI.LocalPatientID
	,Sex = 
		case 
			when LPI.Sex = 'M' then 'Male' 
			when LPI.Sex = 'F' then 'Female' 
		end
    ,DateOfBirth = 
		case 
			when (LPI.dob is null or LPI.dob = '') then null 
			else convert(datetime, right(LPI.dob,2) + '/' + substring(LPI.dob,5,2) + '/' + left(LPI.dob,4) , 103)
		end

from PTL.IPWL Encounter

inner join dbo.vwMissingPatients Missing
	on Missing.SourceUniqueID = Encounter.SourceUniqueID
	and Missing.PatientClass = 'W'
	
left join PAS.MissingPatient LPI
	on LPI.SourcePatientNo = Encounter.SourcePatientNo
	and LPI.Archived = 'N'

where LPI.LocalPatientID is not null
and Encounter.DistrictNo is null
--order by LPI.PatientForename

select @RowsUpdated = @@rowcount

SELECT @Elapsed = DATEDIFF(SECOND,@StartTime,getdate())

SELECT @Stats = 
	'Rows Updated ' + CONVERT(varchar(10), @RowsUpdated) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Secs'

EXEC WriteAuditLogEvent 'PAS - WH UpdateMissingPatientIPWL', @Stats, @StartTime

--print @Stats
