﻿CREATE procedure WIPSlot as

select
	 VisitPriority.ReferenceValue
	,VisitType.ReferenceValue
	,*
from
(
select
	VisitPriorityCode =
		coalesce(
			(
			select
				VisitTypeRule.RuleValueCode
			from
				OP.[Rule] VisitTypeRule
			where
				VisitTypeRule.AppliedToUniqueID = Slot.SlotUniqueID
			and	VisitTypeRule.ArchiveFlag = 0
			and	VisitTypeRule.AppliedTo = 'SPSLT'
			and	VisitTypeRule.RuleAppliedUniqueID in (10) --Priority
			and	VisitTypeRule.EnforcedFlag = 'Y'
			)

			,(
			select
				VisitTypeRule.RuleValueCode
			from
				OP.[Rule] VisitTypeRule
			where
				VisitTypeRule.AppliedToUniqueID = Slot.SessionUniqueID
			and	VisitTypeRule.ArchiveFlag = 0
			and	VisitTypeRule.AppliedTo = 'SPSSN'
			and	VisitTypeRule.RuleAppliedUniqueID in (10) --Priority
			and	VisitTypeRule.EnforcedFlag = 'Y'
			)
			,(
			select
				VisitTypeRule.RuleValueCode
			from
				OP.[Rule] VisitTypeRule
			where
				VisitTypeRule.AppliedToUniqueID = Slot.ServicePointUniqueID
			and	VisitTypeRule.ArchiveFlag = 0
			and	VisitTypeRule.AppliedTo = 'SPONT'
			and	VisitTypeRule.RuleAppliedUniqueID in (10) --Priority
			and	VisitTypeRule.EnforcedFlag = 'Y'
			)

			,3127 --routine
		)

	,VisitTypeCode =
		coalesce(
			(
			select
				VisitTypeRule.RuleValueCode
			from
				OP.[Rule] VisitTypeRule
			where
				VisitTypeRule.AppliedToUniqueID = Slot.SlotUniqueID
			and	VisitTypeRule.ArchiveFlag = 0
			and	VisitTypeRule.AppliedTo = 'SPSLT'
			and	VisitTypeRule.RuleAppliedUniqueID in (14) --Visit Type
			and	VisitTypeRule.EnforcedFlag = 'Y'
			)

			,(
			select
				VisitTypeRule.RuleValueCode
			from
				OP.[Rule] VisitTypeRule
			where
				VisitTypeRule.AppliedToUniqueID = Slot.SessionUniqueID
			and	VisitTypeRule.ArchiveFlag = 0
			and	VisitTypeRule.AppliedTo = 'SPSSN'
			and	VisitTypeRule.RuleAppliedUniqueID in (14) --Visit Type
			and	VisitTypeRule.EnforcedFlag = 'Y'
			)
			,(
			select
				VisitTypeRule.RuleValueCode
			from
				OP.[Rule] VisitTypeRule
			where
				VisitTypeRule.AppliedToUniqueID = Slot.ServicePointUniqueID
			and	VisitTypeRule.ArchiveFlag = 0
			and	VisitTypeRule.AppliedTo = 'SPONT'
			and	VisitTypeRule.RuleAppliedUniqueID in (14) --Visit Type
			and	VisitTypeRule.EnforcedFlag = 'Y'
			)

			,8911 --Follow-Up
		)

	,Slot.*

from
	OP.Slot Slot
where
	Slot.SessionUniqueID = 151655808
and	CAST(Slot.SlotDate as date) = '29 feb 2012'
) Activity

left join PAS.ReferenceValue VisitPriority
on	VisitPriority.ReferenceValueCode = Activity.VisitPriorityCode

left join PAS.ReferenceValue VisitType
on	VisitType.ReferenceValueCode = Activity.VisitTypeCode

--select
--	*
--from
--	[Rule]
--where
--	RULES_REFNO in (10, 14)

--select
--	*
--from
--	OP.[Rule]
--where
--	AppliedToUniqueID = 169442412

