﻿CREATE   PROCEDURE [dbo].[WriteAuditLogEvent] 
	 @ProcessCode varchar(255)
	,@Event varchar(255)
	,@StartTime datetime = null
as

INSERT INTO AuditLog 
	(
	 EventTime
	,UserId
	,ProcessCode
	,Event
	,StartTime
	)
VALUES 
	(
	 getdate()
	,Suser_SName()
	,@ProcessCode
	,@Event
	,@StartTime
	)


--insert any unknown process
insert into dbo.AuditProcess
(
	 ProcessCode
	,Process
	,ParentProcessCode
	,ProcessSourceCode
)
select
	 ProcessCode = @ProcessCode
	,Process = @ProcessCode
	,ParentProcessCode = 'OTH'
	,ProcessSourceCode = 'OTH'
where
	not exists
	(
	select
		1
	from
		dbo.AuditProcess
	where
		ProcessCode = @ProcessCode
	)
