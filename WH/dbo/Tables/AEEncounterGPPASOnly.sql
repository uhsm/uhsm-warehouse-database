﻿CREATE TABLE [dbo].[AEEncounterGPPASOnly] (
    [SourceUniqueID]           VARCHAR (11)   NULL,
    [LocalPatientID]           VARCHAR (12)   NULL,
    [ArrivalDate]              DATETIME       NULL,
    [RegisteredGpCode]         VARCHAR (25)   NULL,
    [RegisteredGPName]         NVARCHAR (100) NULL,
    [RegisteredGpPracticeCode] VARCHAR (25)   NULL,
    [RegisteredGPPracticeName] NVARCHAR (100) NULL,
    [RegisteredCCGCode]        NVARCHAR (6)   NULL,
    [EpisodicGpPracticeCode]   VARCHAR (20)   NULL,
    [EpisodicGpPracticeName]   NVARCHAR (100) NULL,
    [EpisodicCCGCode]          NVARCHAR (6)   NULL,
    [CommissionerCodeCCG]      NVARCHAR (7)   NULL,
    [ReferGP]                  BIT            NULL
);

