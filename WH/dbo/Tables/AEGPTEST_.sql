﻿CREATE TABLE [dbo].[AEGPTEST_] (
    [SourceUniqueID]         VARCHAR (11)  NULL,
    [CascPracEncPracCode]    VARCHAR (6)   NULL,
    [CascPracPatPracCode]    VARCHAR (6)   NULL,
    [LocalPatientID]         VARCHAR (12)  NULL,
    [EpisodicGpPracticeCode] NVARCHAR (20) NULL,
    [ArrivalDate]            DATETIME      NULL,
    [EpisodicCCGCode]        NVARCHAR (6)  NULL,
    [EpisodicCCGCode2]       NVARCHAR (6)  NULL,
    [DefaultCCG]             VARCHAR (7)   NULL,
    [RegisteredPCTCode]      NVARCHAR (6)  NULL,
    [EpisodicPCTCode]        NVARCHAR (6)  NULL,
    [TPracCloseDate]         NVARCHAR (8)  NULL,
    [TArrDate]               VARCHAR (8)   NULL,
    [PASPracticeCode]        VARCHAR (20)  NULL,
    [CascPostcode]           VARCHAR (10)  NULL,
    [ResidencePostcode]      VARCHAR (10)  NULL,
    [ResidenceCCGCode]       NVARCHAR (5)  NULL
);

