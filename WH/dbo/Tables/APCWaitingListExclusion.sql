﻿CREATE TABLE [dbo].[APCWaitingListExclusion] (
    [SourcePatientNo]   VARCHAR (50) NOT NULL,
    [SourceEncounterNo] VARCHAR (50) NOT NULL,
    [DistrictNo]        VARCHAR (50) NULL,
    [LastAction]        VARCHAR (50) NULL,
    [PADDate]           VARCHAR (50) NULL,
    CONSTRAINT [PK_APCWaitingListExclusion] PRIMARY KEY CLUSTERED ([SourcePatientNo] ASC, [SourceEncounterNo] ASC)
);

