﻿CREATE TABLE [dbo].[ASTraceCurrent] (
    [RowNumber]       INT            IDENTITY (0, 1) NOT NULL,
    [EventClass]      INT            NULL,
    [EventSubclass]   INT            NULL,
    [TextData]        NTEXT          NULL,
    [ConnectionID]    INT            NULL,
    [NTUserName]      NVARCHAR (128) NULL,
    [ApplicationName] NVARCHAR (128) NULL,
    [StartTime]       DATETIME       NULL,
    [CurrentTime]     DATETIME       NULL,
    [Duration]        BIGINT         NULL,
    [DatabaseName]    NVARCHAR (128) NULL,
    [Error]           INT            NULL,
    [ClientProcessID] INT            NULL,
    [SPID]            INT            NULL,
    [CPUTime]         BIGINT         NULL,
    [NTDomainName]    NVARCHAR (128) NULL,
    [BinaryData]      IMAGE          NULL,
    PRIMARY KEY CLUSTERED ([RowNumber] ASC)
);

