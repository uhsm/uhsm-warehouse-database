﻿CREATE TABLE [dbo].[CSC_CCGCheck] (
    [SourceUniqueID]     FLOAT (53)     NULL,
    [ProviderSpellNo]    FLOAT (53)     NULL,
    [EpisodeStartTime]   DATETIME       NULL,
    [SourcePatientNo]    FLOAT (53)     NULL,
    [OverseasStatusCode] NVARCHAR (255) NULL,
    [OverseasStatus]     NVARCHAR (255) NULL,
    [PostcodeStart]      NVARCHAR (255) NULL,
    [PurchaserMainCode]  NVARCHAR (255) NULL,
    [Comment]            NVARCHAR (255) NULL
);

