﻿CREATE TABLE [dbo].[CalendarBase] (
    [TheDate] SMALLDATETIME NOT NULL,
    CONSTRAINT [PK_CalendarBase] PRIMARY KEY CLUSTERED ([TheDate] ASC)
);

