﻿CREATE TABLE [dbo].[ColumnComparison] (
    [CensusDate] DATETIME      NOT NULL,
    [fieldName]  VARCHAR (50)  NOT NULL,
    [oldField]   VARCHAR (MAX) NULL,
    [newField]   VARCHAR (MAX) NULL,
    [cnt]        INT           NULL
);

