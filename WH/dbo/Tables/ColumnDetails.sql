﻿CREATE TABLE [dbo].[ColumnDetails] (
    [DatabaseName]   VARCHAR (50)  NULL,
    [SchemaName]     VARCHAR (50)  NULL,
    [ObjectName]     VARCHAR (150) NULL,
    [ColumnName]     VARCHAR (150) NULL,
    [ColumnPosition] INT           NULL,
    [DataType]       VARCHAR (50)  NULL,
    [CollationType]  VARCHAR (100) NULL
);

