﻿CREATE TABLE [dbo].[CrossdatabaseColumnDependencies] (
    [ColDatabaseName] VARCHAR (400) NULL,
    [ColSchemaName]   VARCHAR (400) NULL,
    [ColTableName]    VARCHAR (400) NULL,
    [ColSearchString] VARCHAR (400) NULL,
    [databaseId]      SMALLINT      NULL,
    [databaseName]    VARCHAR (50)  NULL,
    [objectId]        INT           NULL,
    [schemaName]      VARCHAR (50)  NULL,
    [objectName]      VARCHAR (100) NULL,
    [objectType]      VARCHAR (50)  NULL,
    [objectDef]       VARCHAR (MAX) NULL
);

