﻿CREATE TABLE [dbo].[CrossdatabaseDependencies] (
    [referencing_database]           VARCHAR (MAX) NULL,
    [referencing_schema]             VARCHAR (MAX) NULL,
    [referencing_object_name]        VARCHAR (MAX) NULL,
    [referencing_object_type]        VARCHAR (MAX) NULL,
    [referencing_object_id]          INT           NULL,
    [referencing_object_create_date] VARCHAR (MAX) NULL,
    [referenced_server]              VARCHAR (MAX) NULL,
    [referenced_database]            VARCHAR (MAX) NULL,
    [referenced_schema]              VARCHAR (MAX) NULL,
    [referenced_object_name]         VARCHAR (MAX) NULL,
    [referenced_object_type]         VARCHAR (MAX) NULL,
    [referenced_object_id]           INT           NULL,
    [referenced_object_create_date]  VARCHAR (MAX) NULL
);

