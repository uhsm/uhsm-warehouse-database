﻿CREATE TABLE [dbo].[Debug] (
    [DebugID]       INT           IDENTITY (1, 1) NOT NULL,
    [EventDateTime] DATETIME      CONSTRAINT [DF_Debug_EventDateTime] DEFAULT (getdate()) NOT NULL,
    [Details]       VARCHAR (500) NOT NULL,
    CONSTRAINT [PK_Debug] PRIMARY KEY CLUSTERED ([DebugID] ASC)
);

