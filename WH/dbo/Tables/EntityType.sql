﻿CREATE TABLE [dbo].[EntityType] (
    [EntityTypeCode] VARCHAR (50)  NOT NULL,
    [EntityType]     VARCHAR (255) NULL,
    CONSTRAINT [PK_EntityType] PRIMARY KEY CLUSTERED ([EntityTypeCode] ASC)
);

