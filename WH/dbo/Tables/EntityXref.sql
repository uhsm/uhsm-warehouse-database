﻿CREATE TABLE [dbo].[EntityXref] (
    [EntityXrefRecno]    INT          IDENTITY (1, 1) NOT NULL,
    [EntityTypeCode]     VARCHAR (50) NOT NULL,
    [EntityCode]         VARCHAR (50) NOT NULL,
    [XrefEntityTypeCode] VARCHAR (50) NOT NULL,
    [XrefEntityCode]     VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_EntityXref] PRIMARY KEY CLUSTERED ([EntityTypeCode] ASC, [EntityCode] ASC, [XrefEntityTypeCode] ASC, [XrefEntityCode] ASC),
    CONSTRAINT [FK_EntityXref_EntityType] FOREIGN KEY ([EntityTypeCode]) REFERENCES [dbo].[EntityType] ([EntityTypeCode]),
    CONSTRAINT [FK_EntityXref_EntityType1] FOREIGN KEY ([XrefEntityTypeCode]) REFERENCES [dbo].[EntityType] ([EntityTypeCode])
);

