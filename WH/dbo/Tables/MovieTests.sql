﻿CREATE TABLE [dbo].[MovieTests] (
    [ID]          INT           NULL,
    [Title]       VARCHAR (100) NULL,
    [ReleaseDate] DATETIME      NULL,
    [Genre]       VARCHAR (50)  NULL,
    [Price]       DECIMAL (18)  NULL
);

