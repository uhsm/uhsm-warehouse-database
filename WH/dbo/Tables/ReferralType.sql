﻿CREATE TABLE [dbo].[ReferralType] (
    [ReferralTypeCode] VARCHAR (10) NOT NULL,
    [ReferralType]     VARCHAR (50) NULL,
    CONSTRAINT [PK_ReferralType] PRIMARY KEY CLUSTERED ([ReferralTypeCode] ASC)
);

