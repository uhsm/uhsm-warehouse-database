﻿CREATE TABLE [dbo].[ReportMISYSAETurnaroundDataset] (
    [CountOfOrders]         INT           NULL,
    [OrderDate]             DATETIME      NULL,
    [OrderDay]              VARCHAR (10)  NULL,
    [CaseTypeCode]          VARCHAR (10)  NULL,
    [CaseType]              VARCHAR (100) NULL,
    [OrderingPhysicianCode] VARCHAR (25)  NULL,
    [OrderingPhysician]     VARCHAR (100) NULL,
    [HoursTurnaround]       VARCHAR (10)  NULL,
    [ExamDescription]       VARCHAR (100) NULL,
    [ArrivalDate]           DATETIME      NULL,
    [ArrivalTime]           DATETIME      NULL,
    [TimeToOrder]           INT           NULL,
    [TimeToScan]            INT           NULL,
    [TimeToReport]          INT           NULL,
    [TotalTurnaroundTime]   INT           NULL,
    [dictationdate]         DATETIME      NULL,
    [DayOfWeek]             VARCHAR (10)  NULL,
    [FinancialYear]         VARCHAR (9)   NULL,
    [TheMonth]              VARCHAR (10)  NULL,
    [DayOfWeekKey]          VARCHAR (25)  NULL,
    [examcode1]             VARCHAR (10)  NULL
);

