﻿CREATE TABLE [dbo].[THRG45AEEncounter] (
    [EncounterRecno]  VARCHAR (50) NOT NULL,
    [RowNo]           VARCHAR (50) NULL,
    [HRGCode]         VARCHAR (50) NULL,
    [ProviderSpellNo] VARCHAR (50) NULL,
    [SourceUniqueID]  VARCHAR (50) NULL,
    CONSTRAINT [PK_THRG4AEEncounter] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);

