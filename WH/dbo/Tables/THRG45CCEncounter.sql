﻿CREATE TABLE [dbo].[THRG45CCEncounter] (
    [RowNo]                       VARCHAR (50) NULL,
    [HRGCode]                     VARCHAR (50) NULL,
    [CriticalCareDays]            VARCHAR (50) NULL,
    [WarningFlag]                 VARCHAR (50) NULL,
    [CriticalCareLocalIdentifier] VARCHAR (50) NOT NULL,
    [SourceSpellNo]               VARCHAR (50) NULL,
    CONSTRAINT [PK_THRG45CCEncounter] PRIMARY KEY CLUSTERED ([CriticalCareLocalIdentifier] ASC)
);

