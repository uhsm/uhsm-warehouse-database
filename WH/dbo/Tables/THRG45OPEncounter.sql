﻿CREATE TABLE [dbo].[THRG45OPEncounter] (
    [EncounterRecno]        VARCHAR (50) NULL,
    [RowNo]                 VARCHAR (50) NULL,
    [HRGCode]               VARCHAR (50) NULL,
    [GroupingMethodFlag]    VARCHAR (50) NULL,
    [DominantOperationCode] VARCHAR (50) NULL
);

