﻿CREATE TABLE [dbo].[TImportAEDiagnosis] (
    [SourceUniqueID]      NUMERIC (9)  NOT NULL,
    [SequenceNo]          SMALLINT     NULL,
    [DiagnosisCode]       VARCHAR (6)  NULL,
    [AESourceUniqueID]    NUMERIC (9)  NOT NULL,
    [SourceDiagnosisCode] VARCHAR (50) NULL,
    [DiagnosisSiteCode]   VARCHAR (10) NULL,
    [DiagnosisSideCode]   VARCHAR (10) NULL
);

