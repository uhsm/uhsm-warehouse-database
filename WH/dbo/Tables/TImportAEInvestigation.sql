﻿CREATE TABLE [dbo].[TImportAEInvestigation] (
    [SourceUniqueID]          VARCHAR (50)  NOT NULL,
    [InvestigationDate]       SMALLDATETIME NULL,
    [SequenceNo]              SMALLINT      NULL,
    [InvestigationCode]       VARCHAR (6)   NULL,
    [AESourceUniqueID]        VARCHAR (50)  NOT NULL,
    [SourceInvestigationCode] VARCHAR (6)   NULL,
    [ResultDate]              SMALLDATETIME NULL
);

