﻿CREATE TABLE [dbo].[TImportAPCMidnightBedState] (
    [SourceUniqueID]          VARCHAR (50) NULL,
    [SourcePatientNo]         VARCHAR (20) NULL,
    [SourceSpellNo]           VARCHAR (50) NULL,
    [ProviderSpellNo]         VARCHAR (20) NULL,
    [SiteCode]                VARCHAR (10) NULL,
    [WardCode]                VARCHAR (10) NULL,
    [ConsultantCode]          VARCHAR (10) NULL,
    [SpecialtyCode]           VARCHAR (10) NULL,
    [SourceAdminCategoryCode] VARCHAR (10) NULL,
    [ActivityInCode]          VARCHAR (50) NULL,
    [AdmissionDateTimeInt]    VARCHAR (20) NULL,
    [MidnightBedStateDate]    VARCHAR (20) NULL,
    [InterfaceCode]           VARCHAR (3)  NULL
);

