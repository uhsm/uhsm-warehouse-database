﻿CREATE TABLE [dbo].[TImportAPCOperation] (
    [SourceUniqueID]    VARCHAR (50) NULL,
    [SourceSpellNo]     VARCHAR (20) NULL,
    [SourcePatientNo]   VARCHAR (20) NULL,
    [SourceEncounterNo] VARCHAR (20) NULL,
    [SequenceNo]        VARCHAR (10) NULL,
    [OperationCode]     VARCHAR (10) NULL,
    [OperationDate]     VARCHAR (19) NULL,
    [ConsultantCode]    VARCHAR (10) NULL,
    [APCSourceUniqueID] VARCHAR (50) NULL
);

