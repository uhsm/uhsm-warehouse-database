﻿CREATE TABLE [dbo].[TImportAPCWaitingListSuspension] (
    [CensusDate]                SMALLDATETIME NOT NULL,
    [SourceUniqueID]            INT           NOT NULL,
    [WaitingListSourceUniqueID] INT           NOT NULL,
    [SuspensionStartDate]       SMALLDATETIME NULL,
    [SuspensionEndDate]         SMALLDATETIME NULL,
    [SuspensionReasonCode]      INT           NULL,
    [PASCreated]                DATETIME      NULL,
    [PASUpdated]                DATETIME      NULL,
    [PASCreatedByWhom]          VARCHAR (30)  NULL,
    [PASUpdatedByWhom]          VARCHAR (30)  NULL,
    [ArchiveFlag]               BIT           NULL,
    CONSTRAINT [PK_Suspension] PRIMARY KEY CLUSTERED ([CensusDate] ASC, [SourceUniqueID] ASC) WITH (FILLFACTOR = 90)
);

