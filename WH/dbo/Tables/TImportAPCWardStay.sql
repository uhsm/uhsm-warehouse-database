﻿CREATE TABLE [dbo].[TImportAPCWardStay] (
    [SourceUniqueID]   INT           NOT NULL,
    [SourceSpellNo]    INT           NULL,
    [SourcePatientNo]  INT           NULL,
    [StartTime]        SMALLDATETIME NULL,
    [EndTime]          SMALLDATETIME NULL,
    [WardCode]         INT           NULL,
    [ProvisionalFlag]  BIT           NULL,
    [PASCreated]       DATETIME      NULL,
    [PASUpdated]       DATETIME      NULL,
    [PASCreatedByWhom] VARCHAR (30)  NULL,
    [PASUpdatedByWhom] VARCHAR (30)  NULL,
    [ArchiveFlag]      BIT           NULL,
    CONSTRAINT [PK_TImportAPCWardStay] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);

