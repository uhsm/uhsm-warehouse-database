﻿CREATE TABLE [dbo].[TImportMISYSAntenatal] (
    [order_number]      NVARCHAR (15)  NULL,
    [CG_MAS_KV]         NVARCHAR (30)  NULL,
    [LMP_CHECK]         NVARCHAR (300) NULL,
    [EDD]               NVARCHAR (30)  NULL,
    [Comment]           NVARCHAR (150) NULL,
    [SCREENING_TIME]    NVARCHAR (30)  NULL,
    [CONTRAST]          NVARCHAR (30)  NULL,
    [COMPLETE]          NVARCHAR (30)  NULL,
    [TOO_EARLY]         NVARCHAR (30)  NULL,
    [TOO_LATE]          NVARCHAR (30)  NULL,
    [VIABLE]            NVARCHAR (30)  NULL,
    [POSSIBLE]          NVARCHAR (30)  NULL,
    [AUDITABLE_ANOMOLY] NVARCHAR (30)  NULL,
    [exam_code_no_mh]   NVARCHAR (30)  NULL
);

