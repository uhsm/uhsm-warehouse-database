﻿CREATE TABLE [dbo].[TImportMISYSOrderModification] (
    [link_patient]            NVARCHAR (20) NULL,
    [order_canceled]          NVARCHAR (1)  NULL,
    [order_modification_main] NVARCHAR (16) NULL,
    [order_number]            NVARCHAR (15) NULL,
    [patient_hosp_num]        FLOAT (53)    NULL,
    [patient_number]          NVARCHAR (20) NULL,
    [modification_dj]         FLOAT (53)    NULL,
    [modification_tm]         FLOAT (53)    NULL
);

