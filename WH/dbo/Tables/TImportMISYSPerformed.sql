﻿CREATE TABLE [dbo].[TImportMISYSPerformed] (
    [creation_date_ODBC]          DATETIME       NULL,
    [episode_number]              NVARCHAR (50)  NULL,
    [ordering_location]           NVARCHAR (50)  NULL,
    [patient_number]              NVARCHAR (50)  NULL,
    [Patient_Gender]              NVARCHAR (50)  NULL,
    [perf_date_ODBC]              DATETIME       NULL,
    [priority]                    NVARCHAR (50)  NULL,
    [doc_status]                  NVARCHAR (50)  NULL,
    [dictation_date_ODBC]         DATETIME       NULL,
    [final_date_ODBC]             DATETIME       NULL,
    [Speciality]                  NVARCHAR (50)  NULL,
    [patient_dob_ODBC]            DATETIME       NULL,
    [patient_name]                VARCHAR (50)   NULL,
    [patient_name_first]          VARCHAR (50)   NULL,
    [patient_name_last]           NVARCHAR (50)  NULL,
    [patient_sex]                 NVARCHAR (50)  NULL,
    [soc_sec_num]                 NVARCHAR (122) NULL,
    [xdata1]                      NVARCHAR (50)  NULL,
    [xdata2]                      NVARCHAR (50)  NULL,
    [xdata3]                      NVARCHAR (50)  NULL,
    [POST_Code]                   NVARCHAR (50)  NULL,
    [order_number]                NVARCHAR (50)  NULL,
    [ordering_phys]               NVARCHAR (50)  NULL,
    [link_onx_questionnaire_data] NVARCHAR (50)  NULL,
    [perf_loc]                    NVARCHAR (50)  NULL,
    [series_number]               VARCHAR (50)   NULL,
    [tech_code]                   VARCHAR (50)   NULL,
    [address_line_1]              VARCHAR (50)   NULL,
    [address_line_2]              VARCHAR (50)   NULL,
    [address_line_3]              VARCHAR (50)   NULL,
    [attending_physician_1]       NVARCHAR (50)  NULL,
    [attending_physician_2]       NVARCHAR (50)  NULL,
    [patient_type]                NVARCHAR (50)  NULL,
    [case_type]                   NVARCHAR (50)  NULL,
    [exam_code]                   NVARCHAR (50)  NULL,
    [exam_description]            NVARCHAR (130) NULL,
    [group_code]                  NVARCHAR (50)  NULL,
    [radiologist_code]            VARCHAR (50)   NULL,
    [primary_phys]                NVARCHAR (50)  NULL,
    [primary_phys_name]           NVARCHAR (50)  NULL,
    [Date_of_death]               NVARCHAR (50)  NULL,
    [PRACTICE_CODE]               VARCHAR (50)   NULL,
    [NHS_N0_Verified]             NVARCHAR (50)  NULL,
    [GP_PRACTICE]                 NVARCHAR (50)  NULL
);

