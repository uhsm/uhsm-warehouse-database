﻿CREATE TABLE [dbo].[TImportMISYSPtStatusLog] (
    [film_flag]            NVARCHAR (1)   NULL,
    [link_location]        NVARCHAR (6)   NULL,
    [link_patient]         NVARCHAR (20)  NULL,
    [link_status]          NVARCHAR (10)  NULL,
    [location_code]        NVARCHAR (30)  NULL,
    [patient_number]       NVARCHAR (30)  NULL,
    [status_code]          NVARCHAR (10)  NULL,
    [pt_status_log]        NVARCHAR (16)  NULL,
    [tech_list]            NVARCHAR (20)  NULL,
    [time_in_status]       FLOAT (53)     NULL,
    [transition_date]      NVARCHAR (10)  NULL,
    [transition_date_ODBC] DATETIME       NULL,
    [transition_dj]        INT            NULL,
    [transition_time]      NVARCHAR (4)   NULL,
    [transition_time_ODBC] DATETIME       NULL,
    [transition_time_SAM]  FLOAT (53)     NULL,
    [pt_comment]           NVARCHAR (255) NULL,
    [status_log_number]    FLOAT (53)     NULL,
    [Order_number]         NVARCHAR (50)  NULL
);

