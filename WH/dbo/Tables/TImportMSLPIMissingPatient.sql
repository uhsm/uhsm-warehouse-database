﻿CREATE TABLE [dbo].[TImportMSLPIMissingPatient] (
    [SourcePatientNo]     INT           NULL,
    [SourceUniqueID]      INT           NOT NULL,
    [PatientClass]        VARCHAR (1)   NOT NULL,
    [EncounterLocation]   VARCHAR (25)  NULL,
    [EncounterTime]       SMALLDATETIME NULL,
    [LocalPatientID]      VARCHAR (15)  NULL,
    [Forename]            VARCHAR (50)  NULL,
    [Surname]             VARCHAR (50)  NULL,
    [NHSNumber]           VARCHAR (50)  NULL,
    [NNNStatusCode]       VARCHAR (50)  NULL,
    [PostcodeUnformatted] VARCHAR (20)  NULL,
    [Sex]                 VARCHAR (20)  NULL,
    [DOB]                 VARCHAR (10)  NULL,
    [GPCode]              VARCHAR (20)  NULL,
    [PracticeCode]        VARCHAR (20)  NULL
);

