﻿CREATE TABLE [dbo].[TImportMissingPatientsLpi] (
    [SourcePatientNo]           INT           NOT NULL,
    [LastVisitNum]              INT           NULL,
    [FACIL_ID]                  NVARCHAR (15) NULL,
    [GIVEN_NAME]                NVARCHAR (50) NULL,
    [SURNAME]                   NVARCHAR (50) NULL,
    [NHS_NUMBER]                NVARCHAR (50) NULL,
    [IDENTITY_RELIABILITY_CODE] NVARCHAR (50) NULL,
    [POSTCODE]                  NVARCHAR (20) NULL,
    [SEX]                       NVARCHAR (20) NULL,
    [DOB]                       NVARCHAR (10) NULL,
    [GP_CODE]                   NVARCHAR (20) NULL,
    [PRACTICE_CODE]             NVARCHAR (20) NULL
);

