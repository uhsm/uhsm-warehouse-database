﻿CREATE TABLE [dbo].[TImportMisysQuestionnnaireDuplicates] (
    [order_number]                NVARCHAR (30) NULL,
    [onx_questionnaire_data]      NVARCHAR (16) NULL,
    [link_onx_questionnaire_data] NVARCHAR (16) NULL,
    [exam_var50]                  NVARCHAR (50) NULL,
    [exam_var51]                  NVARCHAR (50) NULL,
    [exam_var52]                  NVARCHAR (50) NULL,
    [exam_var53]                  NVARCHAR (50) NULL,
    [exam_var54]                  NVARCHAR (50) NULL,
    [exam_var55]                  NVARCHAR (50) NULL,
    [perf_dj]                     FLOAT (53)    NULL,
    [case_status]                 NVARCHAR (5)  NULL
);

