﻿CREATE TABLE [dbo].[TImportMisysUnreported] (
    [OrderNumber]     NVARCHAR (255) NULL,
    [PatientNo]       FLOAT (53)     NULL,
    [PerformanceDate] DATETIME       NULL,
    [ExamDescription] NVARCHAR (255) NULL,
    [Findings]        NVARCHAR (255) NULL
);

