﻿CREATE TABLE [dbo].[TImportOPClinic] (
    [ServicePointUniqueID]        NUMERIC (18)  NOT NULL,
    [ClinicCode]                  VARCHAR (20)  NULL,
    [ClinicName]                  VARCHAR (80)  NULL,
    [ClinicDescription]           VARCHAR (255) NULL,
    [ClinicLocation]              NUMERIC (18)  NULL,
    [ClinicEffectiveFromDate]     DATE          NULL,
    [ClinicEffectiveToDate]       DATE          NULL,
    [ClinicSpecialtyCode]         NUMERIC (18)  NULL,
    [ClinicProfessionalCarerCode] NUMERIC (18)  NULL,
    [ClinicPurposeCode]           NUMERIC (18)  NULL,
    [Horizon]                     NUMERIC (18)  NULL,
    [PartialBookingFlag]          CHAR (1)      NULL,
    [PartialBookingLeadTime]      VARCHAR (32)  NULL,
    [ArchiveFlag]                 BIT           NULL,
    [PASCreated]                  DATETIME      NULL,
    [PASUpdated]                  DATETIME      NULL,
    [PASCreatedByWhom]            VARCHAR (30)  NULL,
    [PASUpdatedByWhom]            VARCHAR (30)  NULL
);

