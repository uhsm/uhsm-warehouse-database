﻿CREATE TABLE [dbo].[TImportOPDiary] (
    [SourceUniqueID]           VARCHAR (254) NOT NULL,
    [ClinicCode]               VARCHAR (8)   NULL,
    [SessionCode]              VARCHAR (8)   NULL,
    [SessionDescription]       VARCHAR (65)  NULL,
    [SessionDate]              VARCHAR (19)  NULL,
    [SessionTimeRange]         VARCHAR (19)  NULL,
    [ReasonForCancellation]    VARCHAR (30)  NULL,
    [SessionPeriod]            VARCHAR (3)   NULL,
    [Units]                    INT           NULL,
    [UsedUnits]                INT           NULL,
    [FreeUnits]                INT           NULL,
    [ValidAppointmentTypeCode] VARCHAR (23)  NULL,
    [InterfaceCOde]            VARCHAR (10)  NULL,
    [DoctorCode]               VARCHAR (10)  NULL
);

