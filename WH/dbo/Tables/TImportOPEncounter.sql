﻿CREATE TABLE [dbo].[TImportOPEncounter] (
    [SourceUniqueID]               INT           NOT NULL,
    [SourcePatientNo]              INT           NOT NULL,
    [SourceEncounterNo]            INT           NOT NULL,
    [ReferralSourceUniqueID]       INT           NULL,
    [WaitingListSourceUniqueID]    INT           NULL,
    [PatientTitle]                 VARCHAR (80)  NULL,
    [PatientForename]              VARCHAR (30)  NULL,
    [PatientSurname]               VARCHAR (30)  NULL,
    [DateOfBirth]                  DATETIME      NULL,
    [DateOfDeath]                  SMALLDATETIME NULL,
    [SexCode]                      INT           NULL,
    [NHSNumber]                    VARCHAR (20)  NULL,
    [DistrictNo]                   VARCHAR (20)  NULL,
    [Postcode]                     VARCHAR (25)  NULL,
    [PatientAddress1]              VARCHAR (50)  NULL,
    [PatientAddress2]              VARCHAR (50)  NULL,
    [PatientAddress3]              VARCHAR (50)  NULL,
    [PatientAddress4]              VARCHAR (50)  NULL,
    [DHACode]                      VARCHAR (10)  NULL,
    [EthnicOriginCode]             INT           NULL,
    [MaritalStatusCode]            INT           NULL,
    [ReligionCode]                 INT           NULL,
    [RegisteredGpCode]             VARCHAR (25)  NULL,
    [RegisteredGpPracticeCode]     VARCHAR (25)  NULL,
    [SiteCode]                     INT           NULL,
    [AppointmentDate]              SMALLDATETIME NULL,
    [AppointmentTime]              SMALLDATETIME NULL,
    [ClinicCode]                   VARCHAR (25)  NULL,
    [AdminCategoryCode]            INT           NULL,
    [SourceOfReferralCode]         INT           NULL,
    [ReasonForReferralCode]        INT           NULL,
    [PriorityCode]                 INT           NULL,
    [FirstAttendanceFlag]          INT           NULL,
    [AppointmentStatusCode]        INT           NULL,
    [CancelledByCode]              INT           NULL,
    [TransportRequiredFlag]        INT           NULL,
    [AppointmentTypeCode]          INT           NULL,
    [DisposalCode]                 INT           NULL,
    [ConsultantCode]               INT           NULL,
    [SpecialtyCode]                INT           NULL,
    [ReferringConsultantCode]      INT           NULL,
    [ReferringSpecialtyCode]       INT           NULL,
    [BookingTypeCode]              INT           NULL,
    [CasenoteNo]                   VARCHAR (50)  NULL,
    [AppointmentCreateDate]        SMALLDATETIME NULL,
    [EpisodicGpCode]               VARCHAR (25)  NULL,
    [EpisodicGpPracticeCode]       VARCHAR (25)  NULL,
    [DoctorCode]                   INT           NULL,
    [PrimaryDiagnosisCode]         VARCHAR (10)  NULL,
    [SubsidiaryDiagnosisCode]      VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode1]      VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode2]      VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode3]      VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode4]      VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode5]      VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode6]      VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode7]      VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode8]      VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode9]      VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode10]     VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode11]     VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode12]     VARCHAR (10)  NULL,
    [PrimaryOperationCode]         VARCHAR (10)  NULL,
    [PrimaryOperationDate]         VARCHAR (19)  NULL,
    [SecondaryOperationCode1]      VARCHAR (10)  NULL,
    [SecondaryOperationDate1]      VARCHAR (19)  NULL,
    [SecondaryOperationCode2]      VARCHAR (10)  NULL,
    [SecondaryOperationDate2]      VARCHAR (19)  NULL,
    [SecondaryOperationCode3]      VARCHAR (10)  NULL,
    [SecondaryOperationDate3]      VARCHAR (19)  NULL,
    [SecondaryOperationCode4]      VARCHAR (10)  NULL,
    [SecondaryOperationDate4]      VARCHAR (19)  NULL,
    [SecondaryOperationCode5]      VARCHAR (10)  NULL,
    [SecondaryOperationDate5]      VARCHAR (19)  NULL,
    [SecondaryOperationCode6]      VARCHAR (10)  NULL,
    [SecondaryOperationDate6]      VARCHAR (19)  NULL,
    [SecondaryOperationCode7]      VARCHAR (10)  NULL,
    [SecondaryOperationDate7]      VARCHAR (19)  NULL,
    [SecondaryOperationCode8]      VARCHAR (10)  NULL,
    [SecondaryOperationDate8]      VARCHAR (19)  NULL,
    [SecondaryOperationCode9]      VARCHAR (10)  NULL,
    [SecondaryOperationDate9]      VARCHAR (19)  NULL,
    [SecondaryOperationCode10]     VARCHAR (10)  NULL,
    [SecondaryOperationDate10]     VARCHAR (19)  NULL,
    [SecondaryOperationCode11]     VARCHAR (10)  NULL,
    [SecondaryOperationDate11]     VARCHAR (19)  NULL,
    [PurchaserCode]                INT           NULL,
    [ProviderCode]                 INT           NULL,
    [ContractSerialNo]             VARCHAR (20)  NULL,
    [ReferralDate]                 SMALLDATETIME NULL,
    [RTTPathwayID]                 VARCHAR (300) NULL,
    [RTTPathwayCondition]          VARCHAR (30)  NULL,
    [RTTStartDate]                 SMALLDATETIME NULL,
    [RTTEndDate]                   SMALLDATETIME NULL,
    [RTTSpecialtyCode]             VARCHAR (10)  NULL,
    [RTTCurrentProviderCode]       VARCHAR (10)  NULL,
    [RTTCurrentStatusCode]         INT           NULL,
    [RTTCurrentStatusDate]         SMALLDATETIME NULL,
    [RTTCurrentPrivatePatientFlag] BIT           NULL,
    [RTTOverseasStatusFlag]        BIT           NULL,
    [RTTPeriodStatusCode]          INT           NULL,
    [AppointmentCategoryCode]      INT           NULL,
    [AppointmentCreatedBy]         VARCHAR (35)  NULL,
    [AppointmentCancelDate]        SMALLDATETIME NULL,
    [LastRevisedDate]              DATETIME      NULL,
    [LastRevisedBy]                VARCHAR (35)  NULL,
    [OverseasStatusFlag]           INT           NULL,
    [OverseasStatusStartDate]      SMALLDATETIME NULL,
    [OverseasStatusEndDate]        SMALLDATETIME NULL,
    [PatientChoiceCode]            INT           NULL,
    [ScheduledCancelReasonCode]    INT           NULL,
    [PatientCancelReason]          INT           NULL,
    [DischargeDate]                SMALLDATETIME NULL,
    [QM08StartWaitDate]            SMALLDATETIME NULL,
    [QM08EndWaitDate]              SMALLDATETIME NULL,
    [DestinationSiteCode]          INT           NULL,
    [EBookingReferenceNo]          VARCHAR (255) NULL,
    [InterfaceCode]                VARCHAR (5)   NULL,
    [IsWardAttender]               BIT           NULL,
    [PASCreated]                   DATETIME      NULL,
    [PASUpdated]                   DATETIME      NULL,
    [PASCreatedByWhom]             VARCHAR (50)  NULL,
    [PASUpdatedByWhom]             VARCHAR (50)  NULL,
    [ArchiveFlag]                  BIT           NULL,
    [ClinicType]                   INT           NULL,
    [SessionUniqueID]              INT           NULL,
    [NHSNumberStatusCode]          VARCHAR (5)   NULL
);

