﻿CREATE TABLE [dbo].[TImportOPRule] (
    [RuleUniqueID]        INT          NOT NULL,
    [AppliedTo]           VARCHAR (5)  NULL,
    [AppliedToUniqueID]   NUMERIC (18) NULL,
    [EnforcedFlag]        CHAR (1)     NULL,
    [RuleAppliedUniqueID] INT          NULL,
    [RuleValueCode]       VARCHAR (20) NULL,
    [ArchiveFlag]         BIT          NULL,
    [PASCreated]          DATETIME     NULL,
    [PASUpdated]          DATETIME     NULL,
    [PASCreatedByWhom]    VARCHAR (30) NULL,
    [PASUpdatedByWhom]    VARCHAR (30) NULL
);

