﻿CREATE TABLE [dbo].[TImportPasPatientMobile] (
    [SourcePatientNo] INT           NULL,
    [PhoneNumber]     NVARCHAR (60) NULL
);


GO
CREATE UNIQUE CLUSTERED INDEX [IX_TImportPasPatientMobile_Patno]
    ON [dbo].[TImportPasPatientMobile]([SourcePatientNo] ASC);

