﻿CREATE TABLE [dbo].[TImportPasPatientPhone] (
    [SourcePatientNo] INT          NULL,
    [PhoneNumber]     VARCHAR (50) NULL
);


GO
CREATE UNIQUE CLUSTERED INDEX [IX_TImportPasPatientPhone_Patno]
    ON [dbo].[TImportPasPatientPhone]([SourcePatientNo] ASC);

