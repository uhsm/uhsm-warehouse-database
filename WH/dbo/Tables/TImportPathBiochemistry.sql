﻿CREATE TABLE [dbo].[TImportPathBiochemistry] (
    [CRN (Hospital) Number] NVARCHAR (50) NULL,
    [NHS Number]            NVARCHAR (50) NULL,
    [Surname]               NVARCHAR (50) NULL,
    [Forename]              NVARCHAR (50) NULL,
    [Date of Birth]         NVARCHAR (50) NULL,
    [Post Code]             NVARCHAR (50) NULL,
    [Sex]                   NVARCHAR (50) NULL,
    [Clinician Code]        NVARCHAR (50) NULL,
    [Clinician Desc]        NVARCHAR (50) NULL,
    [Source Code]           NVARCHAR (50) NULL,
    [Source Desc]           NVARCHAR (50) NULL,
    [Departmental Number]   NVARCHAR (50) NULL,
    [Date of Receipt]       NVARCHAR (50) NULL,
    [Test Code]             NVARCHAR (50) NULL,
    [Test Desc]             NVARCHAR (50) NULL,
    [Department Code]       NVARCHAR (50) NULL
);

