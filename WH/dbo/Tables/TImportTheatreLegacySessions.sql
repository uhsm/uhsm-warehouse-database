﻿CREATE TABLE [dbo].[TImportTheatreLegacySessions] (
    [ID]                  VARCHAR (254) NOT NULL,
    [AmendDate]           DATETIME      NULL,
    [AmendTime]           DATETIME      NULL,
    [Comment]             VARCHAR (60)  NULL,
    [ConsultantCode]      VARCHAR (10)  NULL,
    [Description]         VARCHAR (50)  NULL,
    [PlannedAnaesthetist] VARCHAR (10)  NULL,
    [PrintDate]           DATETIME      NULL,
    [PrintTime]           DATETIME      NULL,
    [ProcedureDate]       DATETIME      NOT NULL,
    [Registrar]           VARCHAR (10)  NULL,
    [SessionStatus]       VARCHAR (50)  NULL,
    [SessionType]         VARCHAR (1)   NULL,
    [StartAndEnd]         VARCHAR (50)  NULL,
    [SubTheatre]          VARCHAR (50)  NOT NULL,
    [TheatreNumber]       VARCHAR (10)  NULL,
    [InterfaceCode]       VARCHAR (10)  NULL
);

