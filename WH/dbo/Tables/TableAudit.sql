﻿CREATE TABLE [dbo].[TableAudit] (
    [ObjectID]     INT          NOT NULL,
    [DatabaseName] VARCHAR (30) NOT NULL,
    [SchemaName]   VARCHAR (30) NOT NULL,
    [ObjectName]   VARCHAR (80) NOT NULL,
    [CreateDate]   DATETIME     NOT NULL,
    [RowCounts]    INT          NULL,
    [TotalSpaceKB] INT          NULL,
    [CensusDate]   DATETIME     NOT NULL
);

