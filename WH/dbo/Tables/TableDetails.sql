﻿CREATE TABLE [dbo].[TableDetails] (
    [DatabaseName] VARCHAR (30)  NOT NULL,
    [SchemaName]   VARCHAR (30)  NULL,
    [ObjectType]   CHAR (2)      NULL,
    [ObjectID]     INT           NULL,
    [ObjectName]   VARCHAR (80)  NULL,
    [CreateDate]   DATETIME      NULL,
    [ModifiedDate] DATETIME      NULL,
    [RowCounts]    INT           NULL,
    [TotalSpaceKB] INT           NULL,
    [DropCmd]      VARCHAR (400) NULL
);

