﻿CREATE TABLE [dbo].[TherapiesContactPathway] (
    [episodes]                         INT          NOT NULL,
    [referrals]                        INT          NULL,
    [contacts]                         INT          NULL,
    [EndWardCode]                      VARCHAR (20) NULL,
    [ConsultantCode]                   VARCHAR (20) NULL,
    [EpisodeStartDateTime]             DATETIME     NULL,
    [EpisodeEndDateTime]               DATETIME     NULL,
    [ReferralReceivedDate]             DATE         NULL,
    [ReceivingProfCarercode]           VARCHAR (80) NULL,
    [ReceivingProfCarerType]           VARCHAR (80) NULL,
    [ReceivingSpecialtyCode(Function)] VARCHAR (25) NULL,
    [cont_date]                        DATETIME     NULL,
    [arrival_time]                     VARCHAR (8)  NULL,
    [depart_time]                      VARCHAR (8)  NULL,
    [outcome]                          VARCHAR (80) NULL,
    [themonth]                         VARCHAR (20) NULL,
    [financialmonthkey]                INT          NULL,
    [prof_carer]                       VARCHAR (25) NULL
);

