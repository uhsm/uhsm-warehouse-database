﻿CREATE TABLE [dbo].[TimportMISYSOrderHistoryQuestionnaire] (
    [order_history]       NVARCHAR (25) NULL,
    [order_history_quest] NVARCHAR (16) NULL,
    [dj]                  FLOAT (53)    NULL,
    [tm]                  FLOAT (53)    NULL,
    [tech_code]           FLOAT (53)    NULL,
    [link_ohq_tech]       FLOAT (53)    NULL,
    [modifying_location]  NVARCHAR (30) NULL,
    [link_ohq_location]   NVARCHAR (6)  NULL
);

