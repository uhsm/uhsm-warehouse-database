﻿CREATE TABLE [dbo].[UserObjectsCopy] (
    [ObjectID]   INT           NOT NULL,
    [DB]         VARCHAR (11)  NULL,
    [SchemaName] NVARCHAR (60) NULL,
    [ObjectName] NVARCHAR (60) NULL,
    [ObjectType] NVARCHAR (60) NULL,
    [CreateDate] DATETIME      NULL,
    [Owner]      NVARCHAR (30) NULL,
    [Status]     NVARCHAR (30) NULL,
    [CensusDate] DATETIME      NULL
);

