﻿CREATE TABLE [dbo].[WardDischargeTarget] (
    [ServicePointCode] INT      NOT NULL,
    [PeriodTypeCode]   CHAR (2) NOT NULL,
    [Discharges]       TINYINT  NULL,
    CONSTRAINT [PK_WardDischargeTarget] PRIMARY KEY CLUSTERED ([ServicePointCode] ASC, [PeriodTypeCode] ASC)
);

