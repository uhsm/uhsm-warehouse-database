﻿CREATE TABLE [dbo].[qryDeltaNew] (
    [SPELL_REFNO]           NVARCHAR (20)  NULL,
    [PAT_REF]               NVARCHAR (20)  NULL,
    [INT_MGT_CODE]          NVARCHAR (25)  NULL,
    [PAT_CLASS]             NVARCHAR (25)  NULL,
    [WARD_CODE]             NVARCHAR (20)  NULL,
    [ADMISSION_DATE]        DATETIME       NULL,
    [DISCHAGE_DATE]         DATETIME       NULL,
    [PT_Facil_ID]           NVARCHAR (50)  NULL,
    [Description]           NVARCHAR (255) NULL,
    [OPCS 44]               NVARCHAR (255) NULL,
    [ADMISSION_METHOD_CODE] NVARCHAR (25)  NULL,
    [WLIST_REFNO]           NVARCHAR (20)  NULL,
    [SPEC_CODE]             NVARCHAR (50)  NULL,
    [age]                   FLOAT (53)     NULL,
    [MRSA]                  NVARCHAR (1)   NULL,
    [MRSA_Start_Date]       DATETIME       NULL,
    [MRSA_End_Date]         DATETIME       NULL,
    [HighRisk]              BINARY (510)   NULL,
    [PT_NHSNo]              NVARCHAR (50)  NULL,
    [MaxOfPastAdm]          DATETIME       NULL,
    [MaxOfPastDis]          DATETIME       NULL
);

