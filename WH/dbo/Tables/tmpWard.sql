﻿CREATE TABLE [dbo].[tmpWard] (
    [EncounterRecno]    INT IDENTITY (1, 1) NOT NULL,
    [StartWardTypeCode] INT NULL,
    [EndWardTypeCode]   INT NULL
);

