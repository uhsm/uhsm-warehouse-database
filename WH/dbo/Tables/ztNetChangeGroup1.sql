﻿CREATE TABLE [dbo].[ztNetChangeGroup1] (
    [GroupField] VARCHAR (100) NOT NULL,
    [Records]    INT           NULL,
    CONSTRAINT [PK_ztNetChangeGroup1] PRIMARY KEY CLUSTERED ([GroupField] ASC)
);

