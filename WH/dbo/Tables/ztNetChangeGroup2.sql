﻿CREATE TABLE [dbo].[ztNetChangeGroup2] (
    [GroupField] VARCHAR (100) NOT NULL,
    [Records]    INT           NULL,
    CONSTRAINT [PK_ztNetChangeGroup2] PRIMARY KEY CLUSTERED ([GroupField] ASC)
);

