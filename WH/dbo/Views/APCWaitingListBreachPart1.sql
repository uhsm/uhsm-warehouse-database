﻿
CREATE view [dbo].[APCWaitingListBreachPart1] as

select
	 Encounter.CensusDate
	,Encounter.SourceUniqueID

--PDO 20120203 - Log ID 1
--Any RTT code entered during a waiting list episode (start is WL addition and the 
--end is either an Admission or removal) should be ignored ie any 20 should not 
--restart and any 30 should not stop.

--PDO 20120330
--re-introduced the logic to cope with clock restarts following a clock start trigger!

--PDO 21120509
--Various reformatting of code

	,MostRecentRTTClockStartDate =
		(
		--get most recent RTT clock start
		select
			 RTTClockStartDate = min(RTT.StartDate)
		from
			RTT.RTTFlag RTT
		where
			RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
		and	RTT.ClockStartFlag = 'true'
		)

	,OldRTTClockStartDate =
		(
--if a clock restart (10, 20) follows a clock trigger (3%, 9%(ish)) then use the start date of the earliest restart
		select
			 RTTClockStartDate = min(RTT.StartDate)
		from
			RTT.RTTFlag RTT
		where
			RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
		and	RTT.ClockRestartFlag = 'true'
		and	RTT.StartDate <= Encounter.CensusDate
		--and not (RTT.SourceCode = 'ADMOF' and RTT.RTTStatusCode = 3007096)
/*--------------------------------Begin Added KD Experimental to remove min issue for multiple stops-------------------------------*/
--If there are multiple restarts we only want to bring the first restart after census date
		and not exists
			(
			Select 1
			From RTT.RTTFlag PreviousRestart
			Where 
				PreviousRestart.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
			and PreviousRestart.ClockRestartFlag = 'True'
			and	PreviousRestart.StartDate <= Encounter.CensusDate
--PDO 02 Feb 2012 - amended following to bring back first rather than last 
			--and PreviousRestart.StartDate > RTT.StartDate
			and PreviousRestart.StartDate < RTT.StartDate
			)
/*--------------------------------End Added KD Experimental to remove min issue for multiple stops-------------------------------*/
		and	exists
			(
			select
				1
			from
				RTT.RTTFlag PreviousClockRestart
			where
				PreviousClockRestart.ReferralSourceUniqueID = RTT.ReferralSourceUniqueID
			and	PreviousClockRestart.StartDate < RTT.StartDate
			and	PreviousClockRestart.ClockRestartTriggerFlag = 'true'
/*--------------------------------Begin Added KD Experimental to remove issue where 90 then 20 not preceeded by 30----------------*/
-- For a clock restart trigger flag to have an effect there must first be a clock stop
-- e.g. if there is a 90 followed by a 20 but there is no 30 then it must not be a restart
-- ? Is this really a DQ issue?
			and Exists 
				(
				Select 1
				from RTT.RTTFlag PreviousClockStop
				Where 
				PreviousClockStop.ReferralSourceUniqueID = RTT.ReferralSourceUniqueID
				and	PreviousClockStop.StartDate < RTT.StartDate
				and	PreviousClockStop.ClockStopFlag = 'true'
				)
/*--------------------------------End Added KD Experimental to remove issue where 90 then 20 not preceeded by 30----------------*/
			)
		)


	,RTTClockStartDate =

--if a clock restart (10, 20) follows a clock stop trigger (3%, 9%(ish)) 
--then use the start date of the earliest restart
		(
		select top 1
			 RTTClockStartDate = RTTRestart.StartDate
		from
			RTT.RTTFlag RTTRestart

		inner join
			(
			select
				 PreviousRestartTrigger.ReferralSourceUniqueID
				,PreviousRestartTrigger.EncounterRecno
				,PreviousRestartTrigger.StartDate
				,LatestPreviousClockStopDate = LatestPreviousClockStop.StartDate
			from
				RTT.RTTFlag PreviousRestartTrigger

			inner join RTT.RTTFlag LatestPreviousClockStop
			on	LatestPreviousClockStop.ReferralSourceUniqueID = PreviousRestartTrigger.ReferralSourceUniqueID
			and	LatestPreviousClockStop.StartDate <= PreviousRestartTrigger.StartDate
			and	LatestPreviousClockStop.ClockStopFlag = 'true'
			and	not exists
				(
				select
					1
				from
					RTT.RTTFlag Previous
				where
					Previous.ReferralSourceUniqueID = PreviousRestartTrigger.ReferralSourceUniqueID
				and	Previous.StartDate <= PreviousRestartTrigger.StartDate
				and	Previous.ClockStopFlag = 'true'

				and	(
						Previous.StartDate > LatestPreviousClockStop.StartDate
					or	(
							Previous.StartDate = LatestPreviousClockStop.StartDate
						and	Previous.EncounterRecno > LatestPreviousClockStop.EncounterRecno
						)
					)
				)

			where
				PreviousRestartTrigger.ClockRestartTriggerFlag = 'true'
			and	PreviousRestartTrigger.StartDate >= LatestPreviousClockStop.StartDate

			) PreviousRestartTrigger
		on	PreviousRestartTrigger.ReferralSourceUniqueID = RTTRestart.ReferralSourceUniqueID
		and	PreviousRestartTrigger.StartDate <= RTTRestart.StartDate
		and	not exists
			(
			select
				1
			from
				RTT.RTTFlag Previous

			inner join RTT.RTTFlag LatestPreviousClockStop
			on	LatestPreviousClockStop.ReferralSourceUniqueID = PreviousRestartTrigger.ReferralSourceUniqueID
			and	LatestPreviousClockStop.StartDate <= PreviousRestartTrigger.StartDate
			and	LatestPreviousClockStop.ClockStopFlag = 'true'
			and	not exists
				(
				select
					1
				from
					RTT.RTTFlag Previous
				where
					Previous.ReferralSourceUniqueID = PreviousRestartTrigger.ReferralSourceUniqueID
				and	Previous.StartDate <= PreviousRestartTrigger.StartDate
				and	Previous.ClockStopFlag = 'true'

				and	(
						Previous.StartDate > LatestPreviousClockStop.StartDate
					or	(
							Previous.StartDate = LatestPreviousClockStop.StartDate
						and	Previous.EncounterRecno > LatestPreviousClockStop.EncounterRecno
						)
					)
				)

			where
				Previous.ClockRestartTriggerFlag = 'true'
			and	Previous.StartDate >= LatestPreviousClockStop.StartDate
			and	Previous.ReferralSourceUniqueID = RTTRestart.ReferralSourceUniqueID
			and	Previous.StartDate <= RTTRestart.StartDate

			and	(
					Previous.StartDate > PreviousRestartTrigger.StartDate
				or	(
						Previous.StartDate = PreviousRestartTrigger.StartDate
					and	Previous.EncounterRecno > PreviousRestartTrigger.EncounterRecno
					)
				)
			)

		where
			RTTRestart.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
		and	RTTRestart.StartDate <= Encounter.CensusDate
		and	RTTRestart.ClockRestartFlag = 'true'

		order by
			 PreviousRestartTrigger.LatestPreviousClockStopDate desc
			,PreviousRestartTrigger.StartDate asc

		)

--use the OriginalDateOnWaitingList where the waiter
--has a clock stop following a restart
--and does not have subsequent restart.
	,RTTClockStartWhereClockStopFollowsRestart =
		(
		select
			Encounter.OriginalDateOnWaitingList
		from
			RTT.RTTFlag RTT
		where
			RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
		and	RTT.ClockStopFlag = 1
		and	RTT.StartDate <= Encounter.CensusDate

		and	not exists
			(
			select
				1
			from
				RTT.RTTFlag Previous
			where
				Previous.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
			and	Previous.ClockStopFlag = 1
			and	Previous.StartDate <= Encounter.CensusDate

			and	(
					Previous.StartDate < RTT.StartDate
				or	(
						Previous.StartDate = RTT.StartDate
					and	Previous.EncounterRecno < RTT.EncounterRecno
					)
				)
			)

--has a previous restart
		and	exists
			(
			select
				1
			from
				RTT.RTTFlag HasRestart
			where
				HasRestart.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
			and	HasRestart.ClockRestartFlag = 1
			and	HasRestart.StartDate <= Encounter.CensusDate

			and	HasRestart.StartDate < RTT.StartDate
			)

--and no subsequent restart
		and	not exists
			(
			select
				1
			from
				RTT.RTTFlag HasRestart
			where
				HasRestart.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
			and	HasRestart.ClockRestartFlag = 1
			and	HasRestart.StartDate <= Encounter.CensusDate

			and	HasRestart.StartDate > RTT.StartDate
			)
		)

	,RTTClockStartWithStatus98 =
--Added PDO 20120203 - (Log ID 6) Referrals with a status of 98 use date on waiting list
		case
		when exists
			(
			select
				1
			from
				RTT.RTT
			where 
				RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
			and RTT.StartDate < Encounter.CensusDate
			and RTT.RTTStatusCode = 3007094 --98 Not applicable to RTT
			)
		then OriginalDateOnWaitingList
		end


	,RTTClockStartWithClockStopFlag =
--Added KD 20111129 --Reformatted PDO 21120502
		case
		when exists
			(
			select
				1
			from
				RTT.RTTFlag RTT
			where 
				RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
			and	RTT.ClockStopFlag = 1
			and RTT.StartDate < Encounter.CensusDate
			)
		then Encounter.OriginalDateOnWaitingList
		end

	,ManualClockStartDate =
		coalesce(
			 APCWLManualClock.ClockStartDate
			,APCManualClock.ClockStartDate
		)

	,OriginalProviderReferralDate =
		case
		when datepart(year, RFEncounter.OriginalProviderReferralDate) <= 1948
		then null
		else RFEncounter.OriginalProviderReferralDate
		end

	,ReferralDate =
--2011-11-21 Added check for RefToCarer
		case
		when ReferralToCarer.ProfessionalCarerCode is null 
		then null 
		when datepart(year, RFEncounter.ReferralDate) = 1948
		then null
		else RFEncounter.ReferralDate
		end

	,Encounter.ReferralSourceUniqueID
	,Encounter.OriginalDateOnWaitingList

	,ManualAdjustmentDays =
		coalesce(APCWLManualAdjustment.AdjustmentDays, 0) +
		coalesce(APCManualAdjustment.AdjustmentDays, 0)

	,Diagnostic =
		case
		when
			DiagnosticProcedure.ProcedureCode is null
--		and	upper(Encounter.Operation) not like '%$T%'
		then 0
		else 1
		end

from
	APC.WaitingList Encounter

left join RF.Encounter RFEncounter
on	RFEncounter.SourceUniqueID = Encounter.ReferralSourceUniqueID

--2011-11-21 KD
left join PAS.ProfessionalCarer ReferralToCarer
on	ReferralToCarer.ProfessionalCarerCode = RFEncounter.ConsultantCode
and 
	(
		ReferralToCarer.ProfessionalCarerCode = 10539696 -- Nurse led plastics
	or	ReferralToCarer.ProfessionalCarerTypeCode = 1130 -- Consultant
	)

--get manual adjustments applied to this wait
left join
	(
	select
		 SourceUniqueID = SourceRecno
		,AdjustmentDays = sum(AdjustmentDays)
	from
		RTT.Adjustment Adjustment
	where
		Adjustment.SourceCode = 'APCWL'
	group by
		 SourceRecno
	) APCWLManualAdjustment
on	APCWLManualAdjustment.SourceUniqueID = Encounter.SourceUniqueID

--get manual adjustments applied to this encounter
left join 
	(
	select
		 SourceUniqueID = SourceRecno
		,AdjustmentDays = sum(AdjustmentDays)
	from
		RTT.Adjustment Adjustment
	where
		Adjustment.SourceCode = 'APC'
	group by
		 SourceRecno
	) APCManualAdjustment
on	APCManualAdjustment.SourceUniqueID = Encounter.SourceUniqueID

left join RTT.Encounter APCManualClock
on	APCManualClock.SourceRecno = Encounter.SourceUniqueID
and	APCManualClock.SourceCode = 'APC'

left join RTT.Encounter APCWLManualClock
on	APCWLManualClock.SourceRecno = Encounter.SourceUniqueID
and	APCWLManualClock.SourceCode = 'APCWL'

left join WH.DiagnosticProcedure DiagnosticProcedure
on	DiagnosticProcedure.ProcedureCode = Encounter.IntendedPrimaryOperationCode


