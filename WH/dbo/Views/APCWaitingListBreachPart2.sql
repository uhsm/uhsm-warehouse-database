﻿

CREATE view [dbo].[APCWaitingListBreachPart2] as

select
	 Encounter.CensusDate
	,Encounter.SourceUniqueID
	,Encounter.ReferralSourceUniqueID

	,ClockStartDate =
		CAST(
			coalesce(
				 Encounter.RTTClockStartWithStatus98
				,Encounter.RTTClockStartWhereClockStopFollowsRestart
				,Encounter.MostRecentRTTClockStartDate
				,Encounter.RTTClockStartDate
				,Encounter.ManualClockStartDate
				,Encounter.RTTClockStartWithClockStopFlag
				,Encounter.OriginalProviderReferralDate
				,Encounter.ReferralDate
				,Encounter.OriginalDateOnWaitingList
			)
			as date
		)

	,ManualAdjustmentDays

	,Diagnostic

	,SocialSuspensionDays =
		coalesce(
			(
			select
				 sum(
					datediff(
						 day
						,SuspensionStartDate
						,case
						when SuspensionEndDate is null
						then CensusDate
						when CensusDate < SuspensionEndDate
						then CensusDate
						else SuspensionEndDate
						end
					)
				)

--PDO 26 Jun 2012 - only count suspension days up to the census date
				-- sum(
				--	datediff(
				--		 day
				--		,SuspensionStartDate
				--		,coalesce(
				--			 SuspensionEndDate, CensusDate
				--		)
				--	)
				--) 

			from
				APC.WaitingListSuspension Suspension
			where
				SuspensionReasonCode in
				(
				 4398	--Patient Away
				,4397	--Patient Request
				)

			and	Suspension.SuspensionStartDate >=
				CAST(
					coalesce(
						 Encounter.RTTClockStartWithStatus98
						,Encounter.RTTClockStartWhereClockStopFollowsRestart
						,Encounter.MostRecentRTTClockStartDate
						,Encounter.RTTClockStartDate
						,Encounter.ManualClockStartDate
						,Encounter.RTTClockStartWithClockStopFlag
						,Encounter.OriginalProviderReferralDate
						,Encounter.ReferralDate
						,Encounter.OriginalDateOnWaitingList
					)
					as date
				)

			--Added 2011-03-15 KD
			and Suspension.SuspensionStartDate <= Encounter.CensusDate
			and	Suspension.WaitingListSourceUniqueID = Encounter.SourceUniqueID
			and	Suspension.CensusDate = 
					(
					select
						max(Snapshot.CensusDate)
					from
						APC.Snapshot Snapshot
					where
						Snapshot.CensusDate <= Encounter.CensusDate
					)

			group by
				 Suspension.WaitingListSourceUniqueID
				,Suspension.CensusDate
			)
			,0
		)

from
	dbo.APCWaitingListBreachPart1 Encounter



