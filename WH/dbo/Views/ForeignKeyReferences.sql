﻿
CREATE VIEW [dbo].[ForeignKeyReferences]
--http://bytes.com/topic/sql-server/answers/82772-determining-if-foreign-key-constraint-exists

(fk_name, fk_table_name, fk_column_name,
key_name, key_table_name, key_column_name, ordinal_position)
AS
SELECT FK.constraint_name, FK.table_name, FKU.column_name,
UK.constraint_name, UK.table_name, UKU.column_name,
FKU.ordinal_position
FROM Information_Schema.Table_Constraints AS FK
INNER JOIN
Information_Schema.Key_Column_Usage AS FKU
ON FK.constraint_type = 'FOREIGN KEY' AND
FKU.constraint_name = FK.constraint_name
INNER JOIN
Information_Schema.Referential_Constraints AS RC
ON RC.constraint_name = FK.constraint_name
INNER JOIN
Information_Schema.Table_Constraints AS UK
ON UK.constraint_name = RC.unique_constraint_name
INNER JOIN
Information_Schema.Key_Column_Usage AS UKU
ON UKU.constraint_name = UK.constraint_name AND
UKU.ordinal_position =FKU.ordinal_position
