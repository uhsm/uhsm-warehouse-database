﻿CREATE view [dbo].[HRG4AEInputFile] as

select
	Investigation1 =
	left(
		replace(replace(Investigation1.InvestigationCode, '.', ''), ',', '')
		, 4
	) 
	,Investigation2 =
	left(
		replace(replace(Investigation2.InvestigationCode, '.', ''), ',', '')
		, 4
	)

	,Disposal =
		Encounter.AttendanceDisposalCode

	,Encounter.EncounterRecno

from
	AE.Encounter Encounter

left join AE.Investigation Investigation1
on	Investigation1.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation1.SequenceNo = 1

left join AE.Investigation Investigation2
on	Investigation2.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation2.SequenceNo = 2

--where
--	Encounter.ArrivalDate between
--	(
--	select
--		min(ArrivalDate)
--	from
--		dbo.TLoadAEEncounter
--	)
--and
--	(
--	select
--		max(ArrivalDate)
--	from
--		dbo.TLoadAEEncounter
--	)
