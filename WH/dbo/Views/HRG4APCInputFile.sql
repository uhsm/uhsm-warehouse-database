﻿

CREATE view [dbo].[HRG4APCInputFile] as

select
	 ProviderCode = 'RM200'
	,Encounter.ProviderSpellNo

	,Encounter.EpisodeNo

	,EpisodeAge =
		datediff(
			 year
			,Encounter.DateOfBirth
			,Encounter.EpisodeStartDate
		)

	,SexCode = Sex.MappedCode

	,PatientClassificationCode =
		case
		when
			Encounter.ManagementIntentionCode in
			(
			 2003683	--Not Applicable	8
			,2003684	--Not Known	9
			,2005563	--Not Specified	NSP
			)
		then
			case
			when datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
			then 2
			else 1
			end

		when 
			Encounter.ManagementIntentionCode in
				(
				 8850	--No Overnight Stay	2
				,8851	--Planned Sequence - No Overnight Stays	4
				,9487	--Planned Sequence - Inc. Over Night Stays	3
				)
		and
			Encounter.AdmissionMethodCode not in
			(
			 2003470	--Elective
			,8811		--Elective - Booked
			,8810		--Elective - Waiting List
			,8812		--Elective - Planned
			,13			--Not Specified
			,2003472	--OTHER
			)
		then 1

		when
			Encounter.ManagementIntentionCode in
			(
			 8851	--Planned Sequence - No Overnight Stays	4
			)
		and	datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
		then 3

		when
			Encounter.ManagementIntentionCode in
			(
			 9487	--Planned Sequence - Inc. Over Night Stays	3
			)
		and	datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
		then 4

		when datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
		then 2

		else 1
		end

	,AdmissionSourceCode =
		AdmissionSource.MappedCode

	,AdmissionMethodCode =
		AdmissionMethod.MappedCode

	,DischargeDestinationCode =
		DischargeDestination.MappedCode

	,DischargeMethodCode =
		DischargeMethod.MappedCode

	,EpisodeDuration =
		case
		when datediff(day, EpisodeStartDate, EpisodeEndDate) <= 0 then 1
		else datediff(day, EpisodeStartDate, EpisodeEndDate)
		end

	,MainSpecialtyCode =
		Specialty.NationalSpecialtyCode

	,NeonatalLevelOfCareCode =
		coalesce(NeonatalLevelOfCare.MappedCode, '0')

	,TreatmentFunctionCode =
		Specialty.NationalSpecialtyCode

	,Operation1 =
	left(
		replace(replace(Encounter.PrimaryOperationCode, '.', ''), ',', '')
		, 4
	) 
	,Operation2 =
	left(
		replace(replace(Operation2.OperationCode, '.', ''), ',', '')
		, 4
	) 
	,Operation3 =
	left(
		replace(replace(Operation3.OperationCode, '.', ''), ',', '')
		, 4
	) 
	,Operation4 =
	left(
		replace(replace(Operation4.OperationCode, '.', ''), ',', '')
		, 4
	) 
	,Operation5 =
	left(
		replace(replace(Operation5.OperationCode, '.', ''), ',', '')
		, 4
	) 
	,Operation6 =
	left(
		replace(replace(Operation6.OperationCode, '.', ''), ',', '')
		, 4
	) 
	,Operation7 =
	left(
		replace(replace(Operation7.OperationCode, '.', ''), ',', '')
		, 4
	) 
	,Operation8 =
	left(
		replace(replace(Operation8.OperationCode, '.', ''), ',', '')
		, 4
	) 
	,Operation9 =
	left(
		replace(replace(Operation9.OperationCode, '.', ''), ',', '')
		, 4
	) 
	,Operation10 =
	left(
		replace(replace(Operation10.OperationCode, '.', ''), ',', '')
		, 4
	) 
	,Operation11 =
	left(
		replace(replace(Operation11.OperationCode, '.', ''), ',', '')
		, 4
	) 
	,Operation12 =
	left(
		replace(replace(Operation12.OperationCode, '.', ''), ',', '')
		, 4
	) 

	,Diagnosis1 =
	left(
		replace(replace(Encounter.PrimaryDiagnosisCode, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis2 =
	left(
		replace(replace(Diagnosis2.DiagnosisCode, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis3 =
	left(
		replace(replace(Diagnosis3.DiagnosisCode, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis4 =
	left(
		replace(replace(Diagnosis4.DiagnosisCode, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis5 =
	left(
		replace(replace(Diagnosis5.DiagnosisCode, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis6 =
	left(
		replace(replace(Diagnosis6.DiagnosisCode, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis7 =
	left(
		replace(replace(Diagnosis7.DiagnosisCode, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis8 =
	left(
		replace(replace(Diagnosis8.DiagnosisCode, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis9 =
	left(
		replace(replace(Diagnosis9.DiagnosisCode, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis10 =
	left(
		replace(replace(Diagnosis10.DiagnosisCode, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis11 =
	left(
		replace(replace(Diagnosis11.DiagnosisCode, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis12 =
	left(
		replace(replace(Diagnosis12.DiagnosisCode, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis13 =
	left(
		replace(replace(Diagnosis13.DiagnosisCode, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis14 =
	left(
		replace(replace(Diagnosis14.DiagnosisCode, '.', ''), ',', '')
		, 5
	) 

	,CriticalCareDays = 0

	,RehabilitationDays = 0

	,SpecialistPalliativeCareDays = 0

--	,CriticalCareDays =
--	case
--	when Encounter.InterfaceCode = 'PAS'
--	then
--		case
--		when Encounter.StartWardTypeCode = 'CCU'
--		then datediff(day, Encounter.EpisodeStartDate, Encounter.EpisodeEndDate)
--		else 0
--		end
--	when CriticalCare.CriticalCareDays is null then 0
--	when CriticalCare.CriticalCareDays = 0 then 1 --if there is CC < 1 whole day then set to 1 (see guidance)
--	else CriticalCare.CriticalCareDays
--	end
--
--	,RehabilitationDays =
--	case
--	when
--	Encounter.InterfaceCode = 'PAS'
--	and	Encounter.StartWardTypeCode = 'SEYM'
--	then datediff(day, Encounter.EpisodeStartDate, Encounter.EpisodeEndDate)
--	else 0
--	end
--
--	,SpecialistPalliativeCareDays = 0

	,Encounter.EncounterRecno
	,Encounter.EpisodeEndDate

from
	APC.Encounter Encounter

left join APC.Operation Operation2
on	Operation2.APCSourceUniqueID = Encounter.SourceUniqueID
and	Operation2.SequenceNo = 1

left join APC.Operation Operation3
on	Operation3.APCSourceUniqueID = Encounter.SourceUniqueID
and	Operation3.SequenceNo = 2

left join APC.Operation Operation4
on	Operation4.APCSourceUniqueID = Encounter.SourceUniqueID
and	Operation4.SequenceNo = 3

left join APC.Operation Operation5
on	Operation5.APCSourceUniqueID = Encounter.SourceUniqueID
and	Operation5.SequenceNo = 4

left join APC.Operation Operation6
on	Operation6.APCSourceUniqueID = Encounter.SourceUniqueID
and	Operation6.SequenceNo = 5

left join APC.Operation Operation7
on	Operation7.APCSourceUniqueID = Encounter.SourceUniqueID
and	Operation7.SequenceNo = 6

left join APC.Operation Operation8
on	Operation8.APCSourceUniqueID = Encounter.SourceUniqueID
and	Operation8.SequenceNo = 7

left join APC.Operation Operation9
on	Operation9.APCSourceUniqueID = Encounter.SourceUniqueID
and	Operation9.SequenceNo = 8

left join APC.Operation Operation10
on	Operation10.APCSourceUniqueID = Encounter.SourceUniqueID
and	Operation10.SequenceNo = 9

left join APC.Operation Operation11
on	Operation11.APCSourceUniqueID = Encounter.SourceUniqueID
and	Operation11.SequenceNo = 10

left join APC.Operation Operation12
on	Operation12.APCSourceUniqueID = Encounter.SourceUniqueID
and	Operation12.SequenceNo = 11

left join APC.Diagnosis Diagnosis2
on	Diagnosis2.APCSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis2.SequenceNo = 1

left join APC.Diagnosis Diagnosis3
on	Diagnosis3.APCSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis3.SequenceNo = 2

left join APC.Diagnosis Diagnosis4
on	Diagnosis4.APCSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis4.SequenceNo = 3

left join APC.Diagnosis Diagnosis5
on	Diagnosis5.APCSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis5.SequenceNo = 4

left join APC.Diagnosis Diagnosis6
on	Diagnosis6.APCSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis6.SequenceNo = 5

left join APC.Diagnosis Diagnosis7
on	Diagnosis7.APCSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis7.SequenceNo = 6

left join APC.Diagnosis Diagnosis8
on	Diagnosis8.APCSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis8.SequenceNo = 7

left join APC.Diagnosis Diagnosis9
on	Diagnosis9.APCSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis9.SequenceNo = 8

left join APC.Diagnosis Diagnosis10
on	Diagnosis10.APCSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis10.SequenceNo = 9

left join APC.Diagnosis Diagnosis11
on	Diagnosis11.APCSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis11.SequenceNo = 10

left join APC.Diagnosis Diagnosis12
on	Diagnosis12.APCSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis12.SequenceNo = 11

left join APC.Diagnosis Diagnosis13
on	Diagnosis13.APCSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis13.SequenceNo = 12

left join APC.Diagnosis Diagnosis14
on	Diagnosis14.APCSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis14.SequenceNo = 13

left join PAS.Specialty Specialty
on	Specialty.SpecialtyCode = Encounter.SpecialtyCode

left join PAS.ReferenceValue AdmissionSource
on	AdmissionSource.ReferenceValueCode = Encounter.AdmissionSourceCode

left join PAS.ReferenceValue AdmissionMethod
on	AdmissionMethod.ReferenceValueCode = Encounter.AdmissionMethodCode

left join PAS.ReferenceValue DischargeDestination
on	DischargeDestination.ReferenceValueCode = Encounter.DischargeDestinationCode

left join PAS.ReferenceValue DischargeMethod
on	DischargeMethod.ReferenceValueCode = Encounter.DischargeMethodCode

left join PAS.ReferenceValue NeonatalLevelOfCare
on	NeonatalLevelOfCare.ReferenceValueCode = Encounter.NeonatalLevelOfCare

--left join 
--	(
--	select
--		WardStay.EncounterRecno
--		,sum(
--			datediff(day,
--				case
--				when WardStay.EpisodeStartDate > WardStay.WardStayStartDate
--				then WardStay.EpisodeStartDate
--				else WardStay.WardStayStartDate
--				end
--				,case
--				when WardStay.EpisodeEndDate > WardStay.WardStayEndDate
--				then WardStay.WardStayEndDate
--				else WardStay.EpisodeEndDate
--				end
--			)
--		) CriticalCareDays
--	from
--		APC.WardStay WardStay
--
--	inner join APC.CriticalCareWard
--	on	CriticalCareWard.WardCode = WardStay.WardCode
--
--	group by
--		WardStay.EncounterRecno
--
--	) CriticalCare
--on	CriticalCare.EncounterRecno = Encounter.EncounterRecno


left join PAS.ReferenceValue Sex
on    Sex.ReferenceValueCode = Encounter.SexCode

where
	Encounter.DischargeDate > '31 Mar 2011'


