﻿CREATE VIEW dbo.ReportRadDirectorate
AS
SELECT     MonthOrderedName + ' ' + CAST(Year AS varchar) AS TheMonth, CASE WHEN dt.MonthOrdered IN ('1', '2', '3') THEN CAST(dt.[Year] - 1 AS varchar) 
                      + '/' + CAST(dt.[YEAR] AS varchar) ELSE CAST(dt.[year] AS varchar) + '/' + CAST(dt.[Year] + 1 AS varchar) END AS FinancialYear, Year, MonthOrdered, 
                      CASE WHEN MonthOrdered = 1 THEN 13 WHEN MonthOrdered = 2 THEN 14 WHEN MonthOrdered = 3 THEN 15 ELSE MonthOrdered END AS MonthSequence,
                       MonthOrderedName, ModalityCode, 
                      CASE WHEN dt.Modalitycode = 'X' THEN 'PLAIN RADIOGRAPHY AND DIAGNOSTICS' ELSE dt.Modality END AS Modality, Specialty, Orders AS Exams, 
                      OrdersLastYr AS ExamsLastYear
FROM         MISYS.DemandTotal AS dt
WHERE     (CAST(CAST(Year AS varchar) + '-' + CAST(MonthOrdered AS varchar) + '-' + '01' AS datetime) > (CASE WHEN month(getdate()) IN ('1', '2', '3') 
                      THEN CAST((CAST((datepart(year, GETDATE()) - 2) AS varchar) + '03' + '31') AS DATE) ELSE CAST((CAST((datepart(year, GETDATE()) - 1) AS varchar) 
                      + '03' + '31') AS DATE) END)) AND (CAST(CAST(Year AS varchar) + '-' + CAST(MonthOrdered AS varchar) + '-' + '01' AS datetime) < CAST(DATEADD(day, 
                      DATEPART(day, GETDATE()) * - 1 + 1, GETDATE()) AS DATE))

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "dt"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 215
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'ReportRadDirectorate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'ReportRadDirectorate';

