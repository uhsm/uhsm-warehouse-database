﻿

CREATE view [dbo].[TLoadAECodingUser] as

/*
--Author: K Oakden
--Date created: 08/10/2014
--To get datetime and user who completed Cascade coding.
--Data from AESaves - but only data relating to the coding screen is imported from the dbf file
--AESAVES.dbf (where module = 'DISP FULL')
*/
select 
	SourceUniqueID = ref,
	ModifiedDateTime = DATEADD(
	minute 	
		,case
			when savetime like '[012][0123456789]:[012345][0123456789]%'
			then convert(int, left(savetime, 2)) * 60 + convert(int, right(savetime, 2))
			else 0
		end
		,case
		when isdate(savedate) = 1
		then convert(datetime, savedate)
		else null
		end
	), 
	ModifiedByUser = saveuser
from [CASCADE].AESAVES
--order by savetime desc
