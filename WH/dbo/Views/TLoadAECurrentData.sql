﻿









/*
--Author: K Oakden
--Date created: 08/02/2013
--To get current cascade data. Majority of code pulled from dbo.TLoadAEEncounter
--KO updated 13/08/2014 added SeenBySpecialtyTime, SourceOfReferralCode
*/


CREATE view [dbo].[TLoadAECurrentData] as

select
	 SourceUniqueID
	,DistrictNo
	,NHSNumber
	,PatientTitle
	,PatientForename
	,PatientSurname
	,Encounter.Postcode
	,DateOfBirth
	,SexCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,PracticePCTCode = PracticePCT.ParentOrganisationCode
	--,PCTCCGCode =
	--	case when ArrivalDate > '2013-03-31 00:00:00' 
	--		then PracticeCCG.ParentOrganisationCode
	--		else PracticePCT.ParentOrganisationCode
	--	end
	,AttendanceNumber
	,SourceOfReferralCode
	,ArrivalModeCode
	,AttendanceCategoryCode
	,AttendanceDisposalCode
	,ArrivalDate
	,ArrivalTime
	,AgeOnArrival =
		case
		when Encounter.DateOfBirth >= Encounter.ArrivalDate
		then 0
		else
			datediff(year, Encounter.DateOfBirth, Encounter.ArrivalDate) -
			case
			when Encounter.DateOfBirth >= Encounter.ArrivalDate
			then 0
			when datepart(day, Encounter.DateOfBirth) = datepart(day, Encounter.ArrivalDate)
			and	datepart(month, Encounter.DateOfBirth) = datepart(month,Encounter.ArrivalDate)
			and datepart(year, Encounter.DateOfBirth) <> datepart(year,Encounter.ArrivalDate)
			then 0 
			else 
				case
				when Encounter.DateOfBirth > Encounter.ArrivalDate then 0
				when datepart(dy, Encounter.DateOfBirth) > datepart(dy, Encounter.ArrivalDate) 
				then 1
				else 0 
				end 
			end
		end


	,TriageCategoryCode
	,ReferredToSpecialtyCode

	,InitialAssessmentTime
	,SeenForTreatmentTime
	,SeenBySpecialtyTime
	,AttendanceConclusionTime
	,ToSpecialtyTime
	,DecisionToAdmitTime
	,RegisteredTime

	,WhereSeen
	--,TimeInDepartment
	--KO Added 23/11/2012 to calculate Time in Dept
	--Uses WHOLAP.dbo.BSTAdjustment function to adjust for differences due to clocks going back/forward
	--Function references WHOLAP.dbo.Calendar table
	,EncounterDurationMinutes =
		datediff(
			 minute
			,ArrivalTime
			,AttendanceConclusionTime
		)
		+ dbo.BSTAdjustment(ArrivalTime, AttendanceConclusionTime)
	,STATInterventionDone
	,STATInterventionTime
	,BreachReason
	,AdmitToWard
	--,InterfaceCode
from
	(
	select --top 50
		 SourceUniqueID = uniqueid
		,DistrictNo = Encounter.cas_no
		,NHSNumber = 
			case
			when Encounter.nhs_no = ''
			then null
			else Encounter.nhs_no
			end

		,PatientTitle = 
			case
			when Encounter.title = ''
			then null
			else Encounter.title
			end

		,PatientForename =
			case
			when Encounter.forename = ''
			then null
			else Encounter.forename
			end

		,PatientSurname =
			case
			when Encounter.surname = ''
			then null
			else Encounter.surname
			end

		,Postcode =
			case
			when Encounter.postcode = ''
			then null
			else Encounter.postcode
			end

		,DateOfBirth =
			case
			when isdate(Encounter.birthdate) = 1
			then convert(datetime, Encounter.birthdate)
			else null
			end

		,SexCode = Encounter.gender

		,RegisteredGpCode = Encounter.gp_code

		,RegisteredGpPracticeCode = Encounter.nat_prac

		,PCTCode = Encounter.pct_pcode
		
		,AttendanceNumber = Encounter.att_no

		,SourceOfReferralCode =
			case
				when Encounter.att_source = ''
				then null
				else ltrim(rtrim(Encounter.att_source)) 
			end
			
		,ArrivalModeCode =
			case
				when Encounter.att_source = ''
				then null
				when ltrim(rtrim(Encounter.att_source)) in ( '1', '16', '21')
				then '1'
				else '2'
			end

		,AttendanceCategoryCode =
			case
			when Encounter.att_type = ''
			then null
			else Encounter.att_type
			end

		,AttendanceDisposalCode =
			case
			when Encounter.disposal = ''
			then null
			else Encounter.disposal
			end
		--,ArrivalDateSrc = Encounter.ArrivalDate
		--,ArrivalTimeSrc = Encounter.ArrivalTime
		,ArrivalDate = 
			case
			when isdate(Encounter.ArrivalDate) = 1
			then 
			convert(datetime, Encounter.ArrivalDate)
			else null
			end
		
		,ArrivalTime = 
			DATEADD(
				minute

				,case
				when Encounter.ArrivalTime like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.ArrivalTime, 2)) * 60 + convert(int, right(Encounter.ArrivalTime, 2))
				else 0
				end
				,case
				when isdate(Encounter.ArrivalDate) = 1
				then convert(datetime, Encounter.ArrivalDate)
				else null
				end
			)
		--,Encounter.reg_date
		--,Encounter.reg_time
		,RegisteredTime =
			DATEADD(
				minute

				,case
				when Encounter.reg_time like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.reg_time, 2)) * 60 + convert(int, right(Encounter.reg_time, 2))
				else 0
				end
				,case
				when isdate(Encounter.reg_date) = 1
				then convert(datetime, Encounter.reg_date)
				else null
				end
			)
		,AgeOnArrival = Encounter.pat_age
		--,Encounter.tri_date
		--,Encounter.tri_time
		,InitialAssessmentTime =
			DATEADD(
				minute

				,case
				when Encounter.tri_time like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.tri_time, 2)) * 60 + convert(int, right(Encounter.tri_time, 2))
				else 0
				end
				,case
				when isdate(Encounter.tri_date) = 1
				then --check this date is within a couple of days of arrival date. if not then use arrival date.
					case
					when
						abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Encounter.tri_date))) > 2
					then convert(datetime, Encounter.ArrivalDate)
					else convert(datetime, Encounter.tri_date)
					end
				else null
				end
			)
		,TriageCategoryCode = 
			case
			when Encounter.triage = ''
			then null
			else Encounter.triage
			end
		--,Encounter.dr_date
		--,Encounter.dr_time
		--,Encounter.sp_dr_date
		--,Encounter.sp_dr_time
		--,Encounter.SeenDate
		--,Encounter.SeenTime
		,SeenForTreatmentTime =
			DATEADD(
				minute
				,case
				when Encounter.SeenTime like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.SeenTime, 2)) * 60 + convert(int, right(Encounter.SeenTime, 2))
				else 0
				end
				,case
				when isdate(Encounter.SeenDate) = 1
				then --check this date is within a couple of days of arrival date. if not then use arrival date.
					case
					when
						abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Encounter.SeenDate))) > 2
					then convert(datetime, Encounter.ArrivalDate)
					else convert(datetime, Encounter.SeenDate)
					end
				else null
				end
			)	
	
		--,Encounter.end_date
		--,Encounter.end_time
		,AttendanceConclusionTime = 
			DATEADD(
				minute
				,case
				when Encounter.end_time like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.end_time, 2)) * 60 + convert(int, right(Encounter.end_time, 2))
				else 0
				end
				,case
				when isdate(Encounter.end_date) = 1
				then --check this date is within a couple of days of arrival date. if not then use arrival date.
					case
					when
						abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Encounter.end_date))) > 2
					then convert(datetime, Encounter.ArrivalDate)
					else convert(datetime, Encounter.end_date)
					end
				else null
				end
			)

		--,Encounter.refer_date
		--,Encounter.refer_time
		,ToSpecialtyTime = 
			DATEADD(
				minute

				,case
				when Encounter.refer_time like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.refer_time, 2)) * 60 + convert(int, right(Encounter.refer_time, 2))
				else 0
				end
				,case
				when isdate(Encounter.refer_date) = 1
				then --check this date is within a couple of days of arrival date. if not then use arrival date.
					case
					when
						abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Encounter.refer_date))) > 2
					then convert(datetime, Encounter.ArrivalDate)
					else convert(datetime, Encounter.refer_date)
					end
				else null
				end
			)

		,ReferredToSpecialtyCode = 
			case
			when Encounter.specialty = ''
			then null
			else Encounter.specialty
			end
			
		--,Encounter.admit_date
		--,Encounter.admit_time
		,DecisionToAdmitTime =
			DATEADD(
				minute

				,case
				when Encounter.admit_time like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.admit_time, 2)) * 60 + convert(int, right(Encounter.admit_time, 2))
				else 0
				end
				,case
				when isdate(Encounter.admit_date) = 1
				then --check this date is within a couple of days of arrival date. if not then use arrival date.
					case
					when
						abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Encounter.admit_date))) > 2
					then convert(datetime, Encounter.ArrivalDate)
					else convert(datetime, Encounter.admit_date)
					end
				else null
				end
			)
		,SeenBySpecialtyTime = --null
			DATEADD(
				minute

				,case
				when Encounter.sp_dr_time like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.sp_dr_time, 2)) * 60 + convert(int, right(Encounter.sp_dr_time, 2))
				else 0
				end
				,case
				when isdate(Encounter.sp_dr_date) = 1
				then --check this date is within a couple of days of arrival date. if not then use arrival date.
					case
					when
						abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Encounter.sp_dr_date))) > 2
					then convert(datetime, Encounter.ArrivalDate)
					else convert(datetime, Encounter.sp_dr_date)
					end
				else null
				end
			)
		,STATInterventionDone = 
			case
				when Encounter.statdone = 'T'
				then 1
				else 0
			end
		,STATInterventionTime = --null
			case when Encounter.stattime like '[012][0123456789]:[012345][0123456789]'
			then
			DATEADD(
				minute

				,case
				when Encounter.stattime like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.stattime, 2)) * 60 + convert(int, right(Encounter.stattime, 2))
				else 0
				end
				,case
				when isdate(Encounter.statdate) = 1
				then --check this date is within a couple of days of arrival date. if not then use arrival date.
					case
					when
						abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Encounter.statdate))) > 2
					then convert(datetime, Encounter.ArrivalDate)
					else convert(datetime, Encounter.statdate)
					end
				else null
				end
			)
			end
		--,Encounter.reg_date
		--,Encounter.reg_time
		,WhereSeen = Encounter.where_seen
		,TimeInDepartment = Encounter.time_in_ae
		,BreachReason = Encounter.br_reason
		,AdmitToWard = 
			case
			when Encounter.admit_ward = ''
			then null
			else Encounter.admit_ward
			end
		--,InterfaceCode = 'CASC'

	from
		(
		select
			Encounter.uniqueid
			,Encounter.cas_no
			,Encounter.att_no
			,Encounter.nhs_no
			,Encounter.title
			,Encounter.forename
			,Encounter.surname
			,Encounter.postcode
			,Encounter.birthdate
			,Encounter.pat_age
			,Encounter.gender
--Arrival date is the earlier of regdate and triagedate
			,ArrivalDate =
				case
				when isdate(Encounter.tri_date) <> 1
				then Encounter.reg_date

				when isdate(Encounter.reg_date) <> 1
				then Encounter.tri_date

				when convert(datetime, Encounter.reg_date) < convert(datetime, Encounter.tri_date)
				then Encounter.reg_date

				else Encounter.tri_date
				end

			,ArrivalTime =
				case
				when isdate(Encounter.tri_date) <> 1
				then Encounter.reg_time

				when isdate(Encounter.reg_date) <> 1
				then Encounter.tri_time

				when convert(datetime, Encounter.reg_date) = convert(datetime, Encounter.tri_date)
				and	Encounter.reg_time < Encounter.tri_time
				then Encounter.reg_time

				when convert(datetime, Encounter.reg_date) = convert(datetime, Encounter.tri_date)
				and	Encounter.reg_time > Encounter.tri_time
				then Encounter.tri_time

				when convert(datetime, Encounter.reg_date) < convert(datetime, Encounter.tri_date)
				then Encounter.reg_time

				else Encounter.tri_time
				end
			
			--,Encounter.arr_time		
			--,Encounter.arr_date
			,Encounter.att_type
			,Encounter.att_source
			,Encounter.specialty
			,Encounter.gp_code
			,Encounter.nat_prac
			,Encounter.pct_pcode
			,Encounter.disposal
			,Encounter.tri_date
			,Encounter.tri_time
			--Seen date is the earlier of dr_date and sp_dr_date
			,SeenDate =
				case
				when isdate(Encounter.dr_date) <> 1
				then Encounter.sp_dr_date

				when isdate(Encounter.sp_dr_date) <> 1
				then Encounter.dr_date

				when convert(datetime, Encounter.sp_dr_date) < convert(datetime, Encounter.dr_date)
				then Encounter.sp_dr_date

				else Encounter.dr_date
				end
			,SeenTime =
				case
					when isdate(Encounter.dr_date) <> 1
					then Encounter.sp_dr_time

					when isdate(Encounter.sp_dr_date) <> 1
					then Encounter.dr_time

					when convert(datetime, Encounter.sp_dr_date) = convert(datetime, Encounter.dr_date)
					and	Encounter.dr_time < Encounter.sp_dr_time
					then Encounter.dr_time

					when convert(datetime, Encounter.sp_dr_date) = convert(datetime, Encounter.dr_date)
					and	Encounter.dr_time > Encounter.sp_dr_time
					then Encounter.sp_dr_time

					when convert(datetime, Encounter.sp_dr_date) < convert(datetime, Encounter.dr_date)
					then Encounter.sp_dr_time

					else Encounter.dr_time
				end

			,Encounter.dr_date
			,Encounter.dr_time
			,Encounter.sp_dr_date
			,Encounter.sp_dr_time
			,Encounter.end_date
			,Encounter.end_time
			,Encounter.refer_date
			,Encounter.refer_time
			,Encounter.admit_date
			,Encounter.admit_time
			,Encounter.reg_date
			,Encounter.reg_time
			,Encounter.triage
			,Encounter.where_seen
			,Encounter.time_in_ae
			,Encounter.br_reason
			,Encounter.admit_ward
			,Encounter.statdate
			,Encounter.stattime
			,Encounter.statdone	
		from
			[CASCADE].[CurrentData] Encounter
	) Encounter
)Encounter
left join Organisation.dbo.Practice PracticePCT
on	PracticePCT.OrganisationCode = Encounter.RegisteredGpPracticeCode

--left join OrganisationCCG.dbo.Practice PracticeCCG
--on	PracticeCCG.OrganisationCode = Encounter.RegisteredGpPracticeCode


















