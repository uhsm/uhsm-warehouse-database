﻿








CREATE view [dbo].[TLoadAEDiagnosis] as

/*************************************************************************************
 Description: Format AE diagnosis data 
  
 Modification History ----------------------------------------------------------------

 Date		Author			Change
28/05/2014	KO				Additional inner join to AE.Encounter 
							to include only last 5 years of data
03/09/2014	KO				TIA's wrongly identified as 2nd char of DiagnosisExtra.cns.
							Changed to 4th char.
05/09/2014	KO				Removed references to [CASCADE].AEDiagnsPart. 
							This is now handled in [dbo].[LoadAEDiagnosis]
*************************************************************************************/
select
	 SourceUniqueID = null

	,SequenceNo =
		ROW_NUMBER()
		over(
			partition by
				AECarCod.diagref
			order by
				AECarCod.weighting desc
		)

	,DiagnosticSchemeInUse = '704'

	,DiagnosisCode =
		case
		when AECarCod.codefound = '' then null
		else AECarCod.codefound
		end

	,AESourceUniqueID = AECarCod.diagref

	,SourceDiagnosisCode =
		case
		when AECarCod.codefound = '' then null
		else AECarCod.codefound
		end

	,SourceSequenceNo = null
	,DiagnosisSiteCode = null
	,DiagnosisSideCode = null
	--,CascadeDiagnosisCode = 
	--	case 
	--		when substring(DiagnosisExtra.vascular, 2, 1) = 'T' then 'X1' --DVT
	--		when substring(DiagnosisExtra.infections, 2, 1) = 'T' then 'X2'  --Cellulitis
	--		when substring(DiagnosisExtra.cns, 4, 1) = 'T' then 'X3'  --TIA
	--	end
	--,DiagnosisX1 = case when substring(DiagnosisExtra.vascular, 2, 1) = 'T' then 'X1' else null end --DVT
	--,DiagnosisX2 = case when substring(DiagnosisExtra.infections, 2, 1) = 'T' then 'X2' else null end --Cellulitis
	--,DiagnosisX3 = case when substring(DiagnosisExtra.cns, 4, 1) = 'T' then 'X3' else null end --TIA
from
	[CASCADE].AECarCod AECarCod
inner join AE.Encounter Encounter
on Encounter.SourceUniqueID = AECarCod.diagref
--left join [CASCADE].AEDiagnsPart DiagnosisExtra
--on DiagnosisExtra.diagref = AECarCod.diagref

where
	AECarCod.codetype = 'D'
	










