﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		View TLoadAEEncounter.

Notes:			Stored in 
				Format cascade data into format required by AE reports.

Versions:
				1.0.0.9 - 21/10/2015 - MT
					Use the revised table PatientGPHistory.

				1.0.0.8 - 26/08/2015 - CM
					Added fields from Cascade - PerceivedComplaint (req by GRyder)
					and SeenByStaffId - req for CDS

				1.0.0.7 - 19/08/2015 - CM
					Added fields from Cascade.AETriage table required for deriving CDS

				1.0.0.6 - 18/08/2015 - CM
					Added Revclin, Fractclin, OPD ,refothsite for CDS build

				1.0.0.5 - 15/08/2015 - TJD
					Added NonTrauma and MtgDiscr
					
				1.0.0.4 - 12/08/2015 - TJD
					Added telephone number

				1.0.0.3 - 25/02/2015 - KO
					Removed cascade GP data

				1.0.0.2 - 15/09/2014 - KO
					Added EpisodicGpPracticeName 

				1.0.0.1 - 28/05/2014 - KO
					Inner join [CASCADE].Triage to limit data to last 3 years.
					Added SeenBySpecialtyTime
					
				1.0.0.0
					Original view.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE View [dbo].[TLoadAEEncounter]
As

Select
	 SourceUniqueID
	,UniqueBookingReferenceNo
	,PathwayId
	,PathwayIdIssuerCode
	,RTTStatusCode
	,RTTStartDate
	,RTTEndDate
	,DistrictNo
	,TrustNo
	,CasenoteNo
	,DistrictNoOrganisationCode
	,NHSNumber
	,NHSNumberStatusId
	,LocalPatientID
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.ResidencePostcode
	,Encounter.Telephone --{ADDED TJD 12/08/2015}
	,ResidenceCCGCode = 
		case 
		--For Scotland, NI and CI (NOT WALES) use DistrictOfResidence as lookup instead of PCTCode
		when left(PostcodeCCG.DistrictOfResidence,1) in ('S','Z','Y') then PostcodeCCG.DistrictOfResidence
		
		else
			--Lookup to ODS postcode_to_PCT table
			PostcodeCCG.CCGCode
		end
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath
	,SexCode
	,CarerSupportIndicator
	,Consultant
	,AttendanceNumber
	,ArrivalModeCode
	,AttendanceCategoryCode
	,AttendanceDisposalCode
	,IncidentLocationTypeCode
	,PatientGroupCode
	,SourceOfReferralCode
	,ArrivalDate

	,AgeOnArrival =
		case
		when Encounter.DateOfBirth >= Encounter.ArrivalDate
		then 0
		else
			datediff(year, Encounter.DateOfBirth, Encounter.ArrivalDate) -
			case
			when Encounter.DateOfBirth >= Encounter.ArrivalDate
			then 0
			when datepart(day, Encounter.DateOfBirth) = datepart(day, Encounter.ArrivalDate)
			and	datepart(month, Encounter.DateOfBirth) = datepart(month,Encounter.ArrivalDate)
			and datepart(year, Encounter.DateOfBirth) <> datepart(year,Encounter.ArrivalDate)
			then 0 
			else 
				case
				when Encounter.DateOfBirth > Encounter.ArrivalDate then 0
				when datepart(dy, Encounter.DateOfBirth) > datepart(dy, Encounter.ArrivalDate) 
				then 1
				else 0 
				end 
			end
		end

	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,ProviderReferenceNo
	,CommissionerReferenceNo
	,ProviderCode
	,CommissionerCode
	,SeenByCode
	,SeenBy
	,SeenByStaffType
	,InvestigationCodeFirst
	,InvestigationCodeSecond
	,DiagnosisCodeFirst
	,DiagnosisCodeSecond
	,TreatmentCodeFirst
	,TreatmentCodeSecond
	,PASHRGCode
	,HRGVersionCode
	,PASDGVPCode
	,SiteCode
	,Created
	,Updated
	,ByWhom
	,InterfaceCode
	--,RegisteredGpCode
	--,RegisteredGPName 
	--,RegisteredGpPracticeCode
	--,RegisteredGPPracticeName 
	--,RegisteredGPPracticePostcode 
	--,RegisteredPCTCode = RegisteredPCT.ParentOrganisationCode
	--,RegisteredCCGCode = RegisteredCCG.ParentOrganisationCode
	------------------------------------------------------------------------------
	--Current GP details from PAS
	,RegisteredGpCode = CurrentGP.CurrentRegisteredGPCode
	,RegisteredGPName = CurrentGP.CurrentRegisteredGPName
	,RegisteredGpPracticeCode = CurrentGP.CurrentRegisteredPracticeCode
	,RegisteredGPPracticeName = CurrentGP.CurrentRegisteredGPPractice
	,RegisteredGPPracticePostcode = ''
	--,RegisteredGPPracticePostcode --[CASCADE].AEGpPrac
	,RegisteredCCGCode = CurrentGP.CCGCodeOfGPPractice
	------------------------------------------------------------------------------
	,EpisodicGpPracticeCode --CascadeSrc or PracticeHistory.practiceCode
	,EpisodicGpPracticeName = EpisodicCCG.Organisation
	,EpisodicPCTCode = EpisodicPCT.ParentOrganisationCode
	,EpisodicCCGCode = 
		coalesce(
			EpisodicCCG.ParentOrganisationCode, 
			case 
			--For Scotland, NI and CI (NOT WALES) use DistrictOfResidence as lookup instead of PCTCode
				when left(PostcodeCCG.DistrictOfResidence,1) in ('S','Z','Y') 
				then PostcodeCCG.DistrictOfResidence
				else PostcodeCCG.CCGCode
			end,
			'01N'
		)
	,CommissionerCodeCCG = 
		LEFT(
			coalesce(
				EpisodicCCG.ParentOrganisationCode, 
				case when Encounter.ResidencePostcode like 'ZZ%' then null else
					case 
					--For Scotland, NI and CI (NOT WALES) use DistrictOfResidence as lookup instead of PCTCode
						when left(PostcodeCCG.DistrictOfResidence,1) in ('S','Z','Y') 
						then PostcodeCCG.DistrictOfResidence
						else PostcodeCCG.CCGCode
					end
				end,
				'01N'
			)
			,5) + '00'
	,SourceOfCCG = 'PAS'
	--	coalesce(
	--		case when EpisodicCCG.ParentOrganisationCode is not null then Def2 end,
	--		case when 
	--			case 
	--			--For Scotland, NI and CI (NOT WALES) use DistrictOfResidence as lookup instead of PCTCode
	--				when left(PostcodeCCG.DistrictOfResidence,1) in ('S','Z','Y') 
	--				then PostcodeCCG.DistrictOfResidence
	--				else PostcodeCCG.CCGCode
	--			end is not null then 'Post' end,
	--		'01N'
	--	)
	-------------for testing	
	--,PASPracticeCode
	--,TPracCloseDate = isnull(EpisodicCCG.CloseDate, convert(varchar(8), getdate(), 112))
	--,TArrDate = convert(varchar(8), convert(datetime, Encounter.ArrivalDate), 112) 
	--,CascPracEncPracCode
	--,CascPracPatPracCode
	--,CascPracCode
	-------------
	,ResidencePCTCode
	,InvestigationCodeList
	,TriageCategoryCode
	,PresentingProblem
	,EthnicCategoryCode
	,ReferredToSpecialtyCode
	,DischargeDestinationCode
	--KO added FFTextStatus 20/12/2013 for Friends and Family text authorisation
	,FFTextStatus
	,Assault
	,ArrivalTime
	,InitialAssessmentTime
	,SeenForTreatmentTime
	,AttendanceConclusionTime
	,DepartureTime
	,ToXrayTime
	,FromXrayTime
	,ToSpecialtyTime
	,SeenBySpecialtyTime
	,DecisionToAdmitTime
	,RegisteredTime
	,CardTime
	,ReviewClinicTime
	,ClinicAppointmentTime
	,AdviceTime
	,StaffTime
	,WhereSeen
	--KO Added 23/11/2012 to calculate Time in Dept
	--Uses WHOLAP.dbo.BSTAdjustment function to adjust for differences due to clocks going back/forward
	--Function references WHOLAP.dbo.Calendar table
	,EncounterDurationMinutes =
		datediff(
			 minute
			,ArrivalTime
			,AttendanceConclusionTime
		)
		+ WHREPORTING.dbo.BSTAdjustment(ArrivalTime, AttendanceConclusionTime)
	,STATInterventionDone
	,STATInterventionTime
	,BreachReason
	,AdmitToWard
	,StandbyTime
	,TrolleyDurationMinutes =
		datediff(
			 minute
			,DecisionToAdmitTime
			,AttendanceConclusionTime
		)
		+ WHREPORTING.dbo.BSTAdjustment(DecisionToAdmitTime, AttendanceConclusionTime)
	,ManchesterTriageCode
	,AlcoholAuditParticipation
	,AlcoholAuditFrequency 
	,AlcoholAuditTypicalDay 
	,AlcoholAuditSingleOccasion 
	,AlcoholAuditLocation 
	,ReferGP--added 07/01/2014 to enable reporting for PbR of those patients deflected back to GP from A&E 
	,OpalReferral--added 10/12/14
	,Encounter.NonTrauma --{ADDED TJD 15/08/2015}
	,Encounter.MtgDiscr --{ADDED TJD 15/08/2015}
	,Encounter.Revclin --ADDED CM 18/08/2015
	,Encounter.Fractclin --ADDED CM 18/08/2015
	, Encounter.opd -- ADDED CM 18/08/2015
	, Encounter.refothsite -- ADDED CM 18/08/2015
	,Encounter.rfouttrust-- ADDED CM 18/08/2015
	,Encounter.wheredied-- ADDED CM 18/08/2015
	, Encounter.mtg_categ -- ADDED CM 19/08/2015
	, Encounter.trclomech -- ADDED CM 19/08/2015
	, Encounter.fbmech -- ADDED CM 19/08/2015
	, Encounter.trhedmech -- ADDED CM 19/08/2015
	, Encounter.treyemech -- ADDED CM 19/08/2015
	, Encounter.trnosmech -- ADDED CM 19/08/2015
	, Encounter.trnecmech -- ADDED CM 19/08/2015
	, Encounter.trbacmech -- ADDED CM 19/08/2015
	, Encounter.PerceivedComplaint-- ADDED CM 26/08/2015
	, Encounter.SeenByStaffID -- ADDED CM 26/08/2015 - required for CDS
	, Encounter.setting  -- ADDED CM 10/09/2015
	, Encounter.TriageTrauma -- ADDED CM 10/09/2015
	, Encounter.AmbulanceIncidentNo  -- ADDED CM 10/09/2015 for CDS	

from
	(
	select --top 50
		 SourceUniqueID = Encounter.aepatno + Encounter.attno
		,UniqueBookingReferenceNo = null
		,PathwayId = null
		,PathwayIdIssuerCode = null
		,RTTStatusCode = null
		,RTTStartDate = null
		,RTTEndDate = null

		,DistrictNo = Encounter.aepatno

		,TrustNo = null

		,CasenoteNo =
			case
			when PatUnitNo = ''
			then null
			else PatUnitNo
			end

		,DistrictNoOrganisationCode = null

		,NHSNumber =
			case
			when PatNhsNo = ''
			then null
			else PatNhsNo
			end

		,NHSNumberStatusId = null
		
		,LocalPatientID = 
			case
			when PatRm2Number = ''
			then null
			else PatRm2Number
			end
			
		,PatientTitle = 
			case
			when PatTitle = ''
			then null
			else PatTitle
			end

		,PatientForename =
			case
			when PatForename = ''
			then null
			else PatForename
			end

		,PatientSurname =
			case
			when PatSurname = ''
			then null
			else PatSurname
			end

		,PatientAddress1 =
			case
			when PatAddress1 = ''
			then null
			else PatAddress1
			end

		,PatientAddress2 =
			case
			when PatAddress2 = ''
			then null
			else PatAddress2
			end

		,PatientAddress3 =
			case
			when PatAddress3 = ''
			then null
			else PatAddress3
			end

		,PatientAddress4 =
			case
			when PatAddress4 = ''
			then null
			else PatAddress4
			end


		,ResidencePostcode =
			case
				when PatPostcode = '' then null
				when len(PatPostcode) = 6 then left(PatPostcode, 2) + '   ' + right(PatPostcode, 3)
				when len(PatPostcode) = 7 then left(PatPostcode, 3) + '  ' + right(PatPostcode, 3)
				else PatPostcode
			end
		
		,Telephone = Telephone --{ADDED TJD 12/08/2015}
			
		,DateOfBirth =
			case
			when isdate(PatDob) = 1
			then convert(datetime, PatDob)
			when isdate('01/01/' + PatApproxYear) = 1
			then convert(datetime, '01/01/' + PatApproxYear)
			else null
			end

		,DateOfDeath = null
		,SexCode = PatSex
		,CarerSupportIndicator = null

		--,RegisteredGpCode =
		--		coalesce(
		--			 dbo.GetConsultantNationalCode('G', GpPatGpNatCode)
		--			,dbo.GetConsultantNationalCode('G', GpEncGpNatCode)
		--		)
				
		--,RegisteredGPName = 
		--	case 
		--		when isnull(GpPatGpNatCode, '') = ''
		--		then case 
		--			when isnull(GpEncGpNatCode, '') <> ''
		--			then rtrim(GpEncGpSurname) + coalesce(', ' + GpEncGpInit, '')
		--		end
		--		else rtrim(GpPatGpSurname) + coalesce(', ' + GpPatGpInit, '')
		--	end
			
		--,RegisteredGpPracticeCode =
		--	coalesce(
		--		 PracPatPracCode
		--		,PracEncPracCode
		--	)
			
		--,RegisteredGPPracticeName = 
		--	case 
		--		when isnull(PracPatPracName, '') = ''
		--		then case 
		--			when isnull(PracEncPracName, '') <> ''
		--			then PracEncPracName
		--		end
		--		else PracPatPracName
		--	end
			
		--,RegisteredGPPracticePostcode = 
		--	case 
		--		when isnull(PracPatPracPostcode, '') = ''
		--		then case 
		--			when isnull(PracEncPracPostcode, '') <> ''
		--			then PracEncPracPostcode
		--		end
		--		else PracPatPracPostcode
		--	end
			
		------For Testing------------
		--,PASPracticeCode = Encounter.PASPracticeCode
		--,CascPracEncPracCode = PracEncPracCode
		--,CascPracPatPracCode = PracPatPracCode
		--,CascPracCode = Encounter.EpisodicPracCode
		------------------------------

		,EpisodicGpPracticeCode = Encounter.PASPracticeCode
			--coalesce(
			--	EpisodicPracticeCCG.OrganisationCode
			--	,Encounter.PASPracticeCode
			--)

		--,Def2 = 
		--	case
		--		when EpisodicPracticeCCG.OrganisationCode is not null then Def1 
		--		when Encounter.PASPracticeCode is not null then 'PasHist' 
		--		end

		,Consultant = rtrim(Consultant.conname) + coalesce(', ' + Consultant.coninits, '')
		
		,AttendanceNumber = Encounter.attno

		,ArrivalDate 

		,ArrivalModeCode =
			case
			when Encounter.[source] = ''
			then null
			when ltrim(rtrim(Encounter.[source])) in ( '1', '16', '21')
			then '1'
			else '2'
			end

		,AttendanceCategoryCode =
			case
			when Encounter.atttype = ''
			then null
			else Encounter.atttype
			end

		,AttendanceDisposalCode =
			case
			when Encounter.disposal = ''
			then null
			else Encounter.disposal
			end

		,IncidentLocationTypeCode =
			case
			when Encounter.setting = ''
			then null
			else Encounter.setting
			end

		,PatientGroupCode = null
		
		,SourceOfReferralCode =
			case
			when Encounter.[source] = ''
			then null
			else ltrim(rtrim(Encounter.[source])) 
			end
		
		--,SourceOfReferral =
		--	case
		--	when ReferralSource.label = ''
		--	then null
		--	else ReferralSource.label
		--	end
			
		,RegisteredDate = 
			case
			when isdate(Encounter.regdate) = 1
			then convert(datetime, Encounter.regdate)
			else null
			end

		,InitialAssessmentDate =
			case
			when isdate(Encounter.triagedate) = 1
			then convert(datetime, Encounter.triagedate)
			else null
			end

		,AgeOnArrival = null
		,CommissioningSerialNo = null
		,NHSServiceAgreementLineNo = null
		,ProviderReferenceNo = null
		,CommissionerReferenceNo = null
		,ProviderCode = null
		,CommissionerCode = null
		,SeenByCode = Encounter.doctor		
		,SeenBy= ltrim(rtrim(Staff.staffforen)) + ' ' + ltrim(rtrim(Staff.staffsurn))
		,SeenByStaffType= ltrim(rtrim(Staff.stafftype)) 
		,InvestigationCodeFirst = null
		,InvestigationCodeSecond = null
		,DiagnosisCodeFirst = null
		,DiagnosisCodeSecond = null
		,TreatmentCodeFirst = null
		,TreatmentCodeSecond = null
		,PASHRGCode = null
		,HRGVersionCode = null
		,PASDGVPCode = null
		,SiteCode = null
		,Created = null
		,Updated = null
		,ByWhom = null
		,InterfaceCode = 'CASC'
		,PCTCode = null
		,ResidencePCTCode = null
		,InvestigationCodeList = null
		,Encounter.TriageCategoryCode
		,PresentingProblem = null
		
		,EthnicCategoryCode = 
			case
			when PatEthnicity = ''
			then null
			else PatEthnicity
			end

		,ReferredToSpecialtyCode = 
			case
			when Encounter.specialty = ''
			then null
			else Encounter.specialty
			end

		,DischargeDestinationCode = null
		
		,FFTextStatus = 
			case
			when Encounter.fftextok = ''
			then null
			else Encounter.fftextok
			end
			
		,Assault = 
			case
			when Encounter.assault = ''
			then null
			else Encounter.assault
			end

	--various key times
		,ArrivalTime = 
			DATEADD(
				minute

				,case
				when Encounter.ArrivalTime like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.ArrivalTime, 2)) * 60 + convert(int, right(Encounter.ArrivalTime, 2))
				else 0
				end
				,case
				when isdate(Encounter.ArrivalDate) = 1
				then convert(datetime, Encounter.ArrivalDate)
				else null
				end
			)

		,InitialAssessmentTime =
			DATEADD(
				minute

				,case
				when Encounter.triagetime like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.triagetime, 2)) * 60 + convert(int, right(Encounter.triagetime, 2))
				else 0
				end
				,case
				when isdate(Encounter.triagedate) = 1
				then --check this date is within a couple of days of arrival date. if not then use arrival date.
					case
					when
						abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Encounter.triagedate))) > 2
					then convert(datetime, Encounter.ArrivalDate)
					else convert(datetime, Encounter.triagedate)
					end
				else null
				end
			)


		,SeenForTreatmentTime =
			DATEADD(
				minute

				,case
				when Encounter.drseentime like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.drseentime, 2)) * 60 + convert(int, right(Encounter.drseentime, 2))
				else 0
				end
				,case
				when isdate(Encounter.drseendate) = 1
				then --check this date is within a couple of days of arrival date. if not then use arrival date.
					case
					when
						abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Encounter.drseendate))) > 2
					then convert(datetime, Encounter.ArrivalDate)
					else convert(datetime, Encounter.drseendate)
					end
				else null
				end
			)

		,AttendanceConclusionTime = 
			DATEADD(
				minute

				,case
				when Encounter.trtcomptim like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.trtcomptim, 2)) * 60 + convert(int, right(Encounter.trtcomptim, 2))
				else 0
				end
				,case
				when isdate(Encounter.trtcompdat) = 1
				then --check this date is within a couple of days of arrival date. if not then use arrival date.
					case
					when
						abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Encounter.trtcompdat))) > 2
					then convert(datetime, Encounter.ArrivalDate)
					else convert(datetime, Encounter.trtcompdat)
					end
				else null
				end
			)

		,DepartureTime = null

		,ToXrayTime =
			DATEADD(
				minute

				,case
				when Encounter.xrgonetime like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.xrgonetime, 2)) * 60 + convert(int, right(Encounter.xrgonetime, 2))
				else 0
				end
				,case
				when isdate(Encounter.xrgonedate) = 1
				then --check this date is within a couple of days of arrival date. if not then use arrival date.
					case
					when
						abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Encounter.xrgonedate))) > 2
					then convert(datetime, Encounter.ArrivalDate)
					else convert(datetime, Encounter.xrgonedate)
					end
				else null
				end
			)

		,FromXrayTime =
			DATEADD(
				minute

				,case
				when Encounter.xrbacktime like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.xrbacktime, 2)) * 60 + convert(int, right(Encounter.xrbacktime, 2))
				else 0
				end
				,case
				when isdate(Encounter.xrbackdate) = 1
				then --check this date is within a couple of days of arrival date. if not then use arrival date.
					case
					when
						abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Encounter.xrbackdate))) > 2
					then convert(datetime, Encounter.ArrivalDate)
					else convert(datetime, Encounter.xrbackdate)
					end
				else null
				end
			)

		,ToSpecialtyTime = 
			DATEADD(
				minute

				,case
				when Encounter.referontim like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.referontim, 2)) * 60 + convert(int, right(Encounter.referontim, 2))
				else 0
				end
				,case
				when isdate(Encounter.referondat) = 1
				then --check this date is within a couple of days of arrival date. if not then use arrival date.
					case
					when
						abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Encounter.referondat))) > 2
					then convert(datetime, Encounter.ArrivalDate)
					else convert(datetime, Encounter.referondat)
					end
				else null
				end
			)

		,SeenBySpecialtyTime = --null
			DATEADD(
				minute

				,case
				when Specialty.specdrtime like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Specialty.specdrtime, 2)) * 60 + convert(int, right(Specialty.specdrtime, 2))
				else 0
				end
				,case
				when isdate(Specialty.specdrdate) = 1
				then --check this date is within a couple of days of arrival date. if not then use arrival date.
					case
					when
						abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Specialty.specdrdate))) > 2
					then convert(datetime, Encounter.ArrivalDate)
					else convert(datetime, Specialty.specdrdate)
					end
				else null
				end
			)
		,Specialty.specdrdate
		,Specialty.specdrtime
		,OpalReferral = 
			case
			when Opal.opal = ''
			then null
			else Opal.opal
			end
			
		,DecisionToAdmitTime =
			DATEADD(
				minute

				,case
				when Encounter.toadmittim like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.toadmittim, 2)) * 60 + convert(int, right(Encounter.toadmittim, 2))
				else 0
				end
				,case
				when isdate(Encounter.toadmitdat) = 1
				then --check this date is within a couple of days of arrival date. if not then use arrival date.
					case
					when
						abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Encounter.toadmitdat))) > 2
					then convert(datetime, Encounter.ArrivalDate)
					else convert(datetime, Encounter.toadmitdat)
					end
				else null
				end
			)

		,RegisteredTime =
			DATEADD(
				minute

				,case
				when Encounter.regtime like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.regtime, 2)) * 60 + convert(int, right(Encounter.regtime, 2))
				else 0
				end
				,case
				when isdate(Encounter.regdate) = 1
				then convert(datetime, Encounter.regdate)
				else null
				end
			)

	--[CASCADE] times not aleady recorded above

		,CardTime =
			DATEADD(
				minute

				,case
				when Encounter.cardtime like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.cardtime, 2)) * 60 + convert(int, right(Encounter.cardtime, 2))
				else 0
				end
				,case
				when isdate(Encounter.carddate) = 1
				then --check this date is within a couple of days of arrival date. if not then use arrival date.
					case
					when
						abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Encounter.carddate))) > 2
					then convert(datetime, Encounter.ArrivalDate)
					else convert(datetime, Encounter.carddate)
					end
				else null
				end
			)

		,ReviewClinicTime =
			DATEADD(
				minute

				,case
				when Encounter.revcltime like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.revcltime, 2)) * 60 + convert(int, right(Encounter.revcltime, 2))
				else 0
				end
				,case
				when isdate(Encounter.revcldate) = 1
				then --check this date is within a couple of days of arrival date. if not then use arrival date.
					case
					when
						abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Encounter.revcldate))) > 2
					then convert(datetime, Encounter.ArrivalDate)
					else convert(datetime, Encounter.revcldate)
					end
				else null
				end
			)

		,ClinicAppointmentTime =
			DATEADD(
				minute

				,case
				when Encounter.clappttime like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.clappttime, 2)) * 60 + convert(int, right(Encounter.clappttime, 2))
				else 0
				end
				,case
				when isdate(Encounter.clapptdate) = 1
				then --check this date is within a couple of days of arrival date. if not then use arrival date.
					case
					when
						abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Encounter.clapptdate))) > 2
					then convert(datetime, Encounter.ArrivalDate)
					else convert(datetime, Encounter.clapptdate)
					end
				else null
				end
			)

		,AdviceTime =
			case when Encounter.advicetime like '[012][0123456789]:[012345][0123456789]'
			then
			DATEADD(
				minute

				,case
				when Encounter.advicetime like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.advicetime, 2)) * 60 + convert(int, right(Encounter.advicetime, 2))
				else 0
				end
				,case
				when isdate(Encounter.advicedate) = 1
				then --check this date is within a couple of days of arrival date. if not then use arrival date.
					case
					when
						abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Encounter.advicedate))) > 2
					then convert(datetime, Encounter.ArrivalDate)
					else convert(datetime, Encounter.advicedate)
					end
				else null
				end
			)
			end
			
		,StaffTime =
			DATEADD(
				minute

				,case
				when Encounter.stafftime like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.stafftime, 2)) * 60 + convert(int, right(Encounter.stafftime, 2))
				else 0
				end
				,case
				when isdate(Encounter.staffdate) = 1
				then --check this date is within a couple of days of arrival date. if not then use arrival date.
					case
					when
						abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Encounter.staffdate))) > 2
					then convert(datetime, Encounter.ArrivalDate)
					else convert(datetime, Encounter.staffdate)
					end
				else null
				end
			)
		,STATInterventionDone = 
			case
				when Encounter.statdone = 'True'
				then 1
				else 0
			end
		,STATInterventionTime = --null
			case when Encounter.stattime like '[012][0123456789]:[012345][0123456789]'
			then
			DATEADD(
				minute

				,case
				when Encounter.stattime like '[012][0123456789]:[012345][0123456789]'
				then convert(int, left(Encounter.stattime, 2)) * 60 + convert(int, right(Encounter.stattime, 2))
				else 0
				end
				,case
				when isdate(Encounter.statdate) = 1
				then --check this date is within a couple of days of arrival date. if not then use arrival date.
					case
					when
						abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Encounter.statdate))) > 2
					then convert(datetime, Encounter.ArrivalDate)
					else convert(datetime, Encounter.statdate)
					end
				else null
				end
			)
			end
		,WhereSeen = Encounter.pathway
		,BreachReason = isnull(BreachReason.br_reason, 'Not Selected')
		,AdmitToWard =
			case
			when Encounter.admitward = ''
			then null
			else Encounter.admitward
			end
		,StandbyTime = null
		--,StandbyTime = 
		--	DATEADD(
		--		minute
		--		,case
		--		when Stndby.stcalltime like '[012][0123456789]:[012345][0123456789]'
		--		then convert(int, left(Stndby.stcalltime, 2)) * 60 + convert(int, right(Stndby.stcalltime, 2))
		--		else 0
		--		end
		--		,case
		--		when isdate(Stndby.stcalldate) = 1
		--		then --check this date is within a couple of days of arrival date. if not then use arrival date.
		--			case
		--			when
		--				abs(datediff(day, convert(datetime, Encounter.ArrivalDate), convert(datetime, Stndby.stcalldate))) > 2
		--			then convert(datetime, Encounter.ArrivalDate)
		--			else convert(datetime, Stndby.stcalldate)
		--			end
		--		else null
		--		end
		--	)
			--,StandbyDate = 
			--	case
			--	when Stndby.stcalldate = ''
			--	then null
			--	else Stndby.stcalldate
			--	end
			--,StandbyTime = 
			--	case
			--	when Stndby.stcalltime = ''
			--	then null
			--	else Stndby.stcalltime
			--	end
		,ManchesterTriageCode
		,AlcoholAuditParticipation =
			case
				when AlcoholAudit.partaking = '' then null
				when AlcoholAudit.partaking = '1' then 'Agreed'
				when AlcoholAudit.partaking = '2' then 'Refused'
				when AlcoholAudit.partaking = '3' then 'Impaired'
				when AlcoholAudit.partaking = '4' then 'Blank'
			end
		,AlcoholAuditFrequency =
			case
				when AlcoholAudit.alcohfreq = '' then null
				else convert(int, AlcoholAudit.alcohfreq)
			end
		,AlcoholAuditTypicalDay =
			case
				when AlcoholAudit.alcoquant = '' then null
				else convert(int,AlcoholAudit.alcoquant)
			end
		,AlcoholAuditSingleOccasion =
			case
				when AlcoholAudit.alcoexcess = '' then null
				else convert(int, AlcoholAudit.alcoexcess)
			end
		,AlcoholAuditLocation = 
			case
				when AlcoholAudit.location12 = '' then null
				else AlcoholLocation.LocationDescription
			end
		,refergp--added 07/01/2014 to enable reporting for PbR of those patients deflected back to GP from A&E 
	    ,Encounter.NonTrauma --{ADDED TJD 15/08/2015}
	    ,Encounter.MtgDiscr --{ADDED TJD 15/08/2015}
	    , Encounter.Revclin -- ADDED CM 18/08/2015
	    , Encounter.Fractclin -- ADDED CM 18/08/2015
	    , Encounter.opd -- ADDED CM 18/08/2015
	    , Encounter.refothsite -- ADDED CM 18/08/2015
		,Encounter.rfouttrust-- ADDED CM 18/08/2015
		,Encounter.wheredied-- ADDED CM 18/08/2015
		, AETriage.mtg_categ -- ADDED CM 19/08/2015
		, AETriage.trclomech -- ADDED CM 19/08/2015
		, AETriage.fbmech -- ADDED CM 19/08/2015
		, AETriage.trhedmech -- ADDED CM 19/08/2015
		, AETriage.treyemech -- ADDED CM 19/08/2015
		, AETriage.trnosmech -- ADDED CM 19/08/2015
		, AETriage.trnecmech -- ADDED CM 19/08/2015
		, AETriage.trbacmech -- ADDED CM 19/08/2015
		, Encounter.PerceivedComplaint -- ADDED CM 26/08/2015
		, SeenByStaffID = staff.staffid -- ADDED CM 26/08/2015 - required for CDS
		, Encounter.setting  -- ADDED CM 10/09/2015
		, ISNULL(AEStndBy.sttrauma, AETriage.trauma)  as TriageTrauma -- ADDED CM 10/09/2015
		, AmbulanceIncidentNo = Encounter.AmbulanceIncidentNo  -- ADDED CM 10/09/2015 for CDS	
	from
	
		(
		select
			 Encounter.aepatno
			,Encounter.attno
			,Encounter.regdate
			,Encounter.regtime
			,Triage.ArrivalDate 
			,Triage.ArrivalTime 
			,Encounter.child
			,Encounter.atttype
			,Encounter.[source]
			,Encounter.specialty
			,Encounter.carddate
			,Encounter.cardtime
			,Encounter.drseendate
			,Encounter.drseentime
			,Encounter.endcomment
			,Encounter.dispdate
			,Encounter.disptime
			,Encounter.gpref
			,Encounter.practice
			,Encounter.setting
			,Encounter.rtaprinted
			,Encounter.disposal
			,Encounter.admitward
			,Encounter.consultant
			,Encounter.hospname
			,Encounter.othhosward
			,Encounter.wheredied
			,Encounter.attstatus
			,Encounter.quickmajor
			,Encounter.cubicle
			,Encounter.amincident
			,Encounter.clapptdate
			,Encounter.clappttime
			,Encounter.trtcompdat
			,Encounter.trtcomptim
			,Encounter.dishome
			,Encounter.aesos
			,Encounter.refusedtrt
			,Encounter.policecust
			,Encounter.prisoncust
			,Encounter.sservices
			,Encounter.disother
			,Encounter.disothspec
			,Encounter.revclin
			,Encounter.revcldate
			,Encounter.revcltime
			,Encounter.refphysio
			,Encounter.refergp
			,Encounter.refergpdat
			,Encounter.refowndent
			,Encounter.refereph
			,Encounter.refercpn
			,Encounter.referbsv
			,Encounter.refchiropo
			,Encounter.refother
			,Encounter.refothspec
			,Encounter.refotherae
			,Encounter.fractclin
			,Encounter.opd
			,Encounter.opdspec
			,Encounter.refothsite
			,Encounter.rfouttrust
			,Encounter.doctor
			,Encounter.clinicflag
			,Encounter.referondat
			,Encounter.referontim
			,Encounter.toadmitdat
			,Encounter.toadmittim
			,Encounter.research1
			,Encounter.research2
			,Encounter.research3
			,Encounter.xrgonedate
			,Encounter.xrgonetime
			,Encounter.xrbackdate
			,Encounter.xrbacktime
			,Encounter.xrnstosend
			,Encounter.advicetime
			,Encounter.advicedate
			,Encounter.staff
			,Encounter.stafftime
			,Encounter.staffdate
			,Encounter.fasttrack
			,Encounter.specialchk
			,Encounter.prescpay
			,Encounter.recpresent
			,Encounter.EncounterRecno
			,Triage.triagedate
			,Triage.triagetime
			,Triage.triage 
			,Encounter.pathway
			,Encounter.fftextok
			,Encounter.assault
			,Encounter.statdone
			,Encounter.statdate
			,Encounter.stattime
			,Triage.ManchesterTriageCode
			,Triage.TriageCategoryCode
			,PASPracticeCode = PracticeHistory.practiceCode
			,PatUnitNo = Patient.unitno
			,PatNhsNo = Patient.nhsno
			,PatRm2Number = Patient.rm2number
			,PatTitle = Patient.title
			,PatForename = Patient.forename
			,PatSurname = Patient.surname
			,PatAddress1 = Patient.address1
			,PatAddress2 = Patient.address2
			,PatAddress3 = Patient.address3
			,PatAddress4 = Patient.address4
			,PatPostcode = ltrim(rtrim(Patient.postcode))
			,Telephone = Patient.telephone -- {ADDED TJD 12/08/2015}
			,PatDob = Patient.dob
			,PatApproxYear = Patient.approxyear
			,PatSex = Patient.sex
			,PatEthnicity = Patient.ethnicity
			,NonTrauma = Triage.NonTrauma -- {ADDED TJD 15/08/2015}
			,MtgDiscr = Triage.MtgDiscr -- {ADDED TJD 15/08/2015}
			,PerceivedComplaint = AEPercvd.label  -- ADDED CM 26/08/2015 as per GR Request
			,AmbulanceIncidentNo = Encounter.amincident  -- ADDED CM 10/09/2015 for CDS		
			--,GpPatGpNatCode = GpPatient.gpnatcode
			--,GpPatGpSurname = GpPatient.gpsurname
			--,GpPatGpInit = GpPatient.gpini
			--,GpEncGpNatCode = GpEncounter.gpnatcode
			--,GpEncGpSurname = GpEncounter.gpsurname
			--,GpEncGpInit = GpEncounter.gpini
			
			--,PracPatPracCode = PracticePatient.pracncode
			--,PracPatPracName = PracticePatient.prname
			--,PracPatPracPostcode = PracticePatient.prpostcode
			--,PracEncPracCode = PracticeEncounter.pracncode
			--,PracEncPracName = PracticeEncounter.prname
			--,PracEncPracPostcode = PracticeEncounter.prpostcode
			
			--,EpisodicPracCode =
			--	case
			--		when coalesce(
			--				PracticeEncounter.pracncode
			--				,PracticePatient.pracncode
			--			) = ''
			--		then null
			--		else coalesce(
			--				PracticeEncounter.pracncode
			--				,PracticePatient.pracncode
			--			)
			--	end
				
			--,Def1 = 
			--	case
			--		when coalesce(
			--				PracticeEncounter.pracncode
			--				,PracticePatient.pracncode
			--			) = ''
			--		then null
			--		when PracticeEncounter.pracncode is not null then 'CasEnc' 
			--		when PracticePatient.pracncode is not null then 'CasPat' 
			--	end

			
		from [CASCADE].[AEAttend] Encounter
		
		inner join [CASCADE].Triage Triage
			on Triage.SourceUniqueID = Encounter.aepatno + Encounter.attno
		
		left join [CASCADE].AEPatMas Patient
			on	Patient.aepatno = Encounter.aepatno

		--left join [CASCADE].AEGpFile GpEncounter
		--	on	GpEncounter.gpref = Encounter.gpref
		--	and	Encounter.practice = 'false'

		--left join [CASCADE].AEGpFile GpPatient
		--	on	GpPatient.gpref = Patient.gpref
		--	and	Patient.practice = 'false'

		--left join [CASCADE].AEGpPrac PracticeEncounter
		--	on	PracticeEncounter.prcode = GpEncounter.prcode

		--left join [CASCADE].AEGpPrac PracticePatient
		--	on	PracticePatient.prcode = GpPatient.prcode

		left join PAS.PatientGPHistory PracticeHistory
			on PracticeHistory.DistrictNo = Patient.rm2number
			and Triage.ArrivalDate >= PracticeHistory.StartDate
			and (Triage.ArrivalDate < PracticeHistory.EndDate Or PracticeHistory.EndDate Is Null)
		--where
		--	Encounter.atttype = '1' --Initial attendance. not sure if this is required
			left join  [CASCADE].AEPERCVD
		on Encounter.recpresent = AEPERCVD.itemcode
	
		) Encounter
	
	left join [CASCADE].AEBreach BreachEvent
	on BreachEvent.aepatno = Encounter.aepatno
	and BreachEvent.attno = Encounter.attno
	
	left join [CASCADE].AEBreachLU BreachReason
	on BreachReason.br_code = BreachEvent.br_cause
	
	left join [CASCADE].AEConsul Consultant
    on Consultant.concode = Encounter.consultant
    
	left join 
	(select distinct staffcode, staffforen, staffsurn, stafftype, staffid from [CASCADE] .AEStaff) Staff
	on Staff.staffcode = Encounter.doctor
	
	left join [CASCADE].AEVDRoomPart Specialty
		on Specialty.aepatno = Encounter.aepatno
		and Specialty.attno = Encounter.attno
		
	left join [CASCADE].AEVDRoomPartOPAL Opal
		on Opal.aepatno = Encounter.aepatno
		and Opal.attno = Encounter.attno
		
	--left join OrganisationCCG.dbo.Practice EpisodicPracticeCCG
	--on	EpisodicPracticeCCG.OrganisationCode = Encounter.EpisodicPracCode
	--and  convert(varchar(8), Encounter.ArrivalDate, 112) 
	--	< isnull(EpisodicPracticeCCG.CloseDate, convert(varchar(8), getdate(), 112)) 
	
	left join OrganisationCCG.dbo.Practice PASPracticeCCG
	on	PASPracticeCCG.OrganisationCode = ISNULL(Encounter.PASPracticeCode, 'V81999')

	left join [CASCADE].AEExtras AlcoholAudit
	on ltrim(rtrim(AlcoholAudit.attendref)) = ltrim(rtrim(Encounter.aepatno)) + ltrim(rtrim(Encounter.attno))
	
	left join AE.DrinkLocationLookup AlcoholLocation
	on AlcoholLocation.LocationCode = ltrim(rtrim(AlcoholAudit.location12))
	
	left join  [CASCADE].AETriage 
	on Encounter.aepatno + Encounter.attno = AETriage.attendref
	
	left join  [CASCADE].AEStndBy 
	on Encounter.aepatno + Encounter.attno = AEStndBy.stattendrf
	

	--left join [CASCADE].AEStndBy Stndby
	--on ltrim(rtrim(Stndby.stattendrf)) = ltrim(rtrim(Encounter.aepatno)) + ltrim(rtrim(Encounter.attno))
	--and not exists 		
	--	(select 1
	--	from [CASCADE].AEStndBy Previous
	--	where
	--		Previous.stattendrf = Stndby.stattendrf
	--		and	
	--		DATEADD(
	--			minute
	--			,case
	--			when Previous.stcalltime like '[012][0123456789]:[012345][0123456789]'
	--			then convert(int, left(Previous.stcalltime, 2)) * 60 + convert(int, right(Previous.stcalltime, 2))
	--			else 0
	--			end
	--			,convert(datetime, Previous.stcalldate)) 
	--		< 
	--		DATEADD(
	--			minute
	--			,case
	--			when Stndby.stcalltime like '[012][0123456789]:[012345][0123456789]'
	--			then convert(int, left(Stndby.stcalltime, 2)) * 60 + convert(int, right(Stndby.stcalltime, 2))
	--			else 0
	--			end
	--			,convert(datetime, Stndby.stcalldate)) 
	--		)
	) Encounter

--left join Organisation.dbo.Practice RegisteredPCT
--on	RegisteredPCT.OrganisationCode = Encounter.RegisteredGpPracticeCode

--left join OrganisationCCG.dbo.Practice RegisteredCCG
--on	RegisteredCCG.OrganisationCode = Encounter.RegisteredGpPracticeCode

left join Organisation.dbo.Practice EpisodicPCT
on	EpisodicPCT.OrganisationCode = Encounter.EpisodicGpPracticeCode

left join OrganisationCCG.dbo.Practice EpisodicCCG
on	EpisodicCCG.OrganisationCode = Encounter.EpisodicGpPracticeCode

left join OrganisationCCG.dbo.Postcode PostcodeCCG
	on	PostcodeCCG.Postcode =
		case
		when len(Encounter.ResidencePostcode) = 8 then Encounter.ResidencePostcode
		else left(Encounter.ResidencePostcode, 3) + ' ' + right(Encounter.ResidencePostcode, 4) 
		end

left join WHREPORTING.LK.PatientCurrentDetails CurrentGP
	on CurrentGP.FacilityID = Encounter.LocalPatientID
