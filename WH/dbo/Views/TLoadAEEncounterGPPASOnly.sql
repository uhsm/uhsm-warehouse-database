﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		View TLoadAEEncounterGPPASOnly.

Notes:			Stored in 

Versions:
				1.0.0.1 - 21/10/2015 - MT
					Use the revised table PatientGPHistory.

				1.0.0.0 - 17/07/2014 - KO
					Created view.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE View [dbo].[TLoadAEEncounterGPPASOnly]
As

Select Distinct
	 SourceUniqueID
	,LocalPatientID
	,ArrivalDate 
	------------------------------------------------------------------------------
	--GP details from cascade
	--,RegisteredGpCode --[CASCADE].AEGpFile
	--,RegisteredGPName --[CASCADE].AEGpFile
	--,RegisteredGpPracticeCode --[CASCADE].AEGpPrac
	--,RegisteredGPPracticeName --[CASCADE].AEGpPrac
	--,RegisteredGPPracticePostcode --[CASCADE].AEGpPrac
	--,RegisteredCCGCode = RegisteredCCG.ParentOrganisationCode --[CASCADE].AEGpPrac
	------------------------------------------------------------------------------
	------------------------------------------------------------------------------
	--Current GP details from PAS
	,RegisteredGpCode = CurrentGP.CurrentRegisteredGPCode
	,RegisteredGPName = CurrentGP.CurrentRegisteredGPName
	,RegisteredGpPracticeCode = CurrentGP.CurrentRegisteredPracticeCode
	,RegisteredGPPracticeName = CurrentGP.CurrentRegisteredGPPractice
	--,RegisteredGPPracticePostcode --[CASCADE].AEGpPrac
	,RegisteredCCGCode = CurrentGP.CCGCodeOfGPPractice
	------------------------------------------------------------------------------
	--GP details from cascade is exists otherwise from PAS history
	,EpisodicGpPracticeCode --CascadeSrc or PracticeHistory.practiceCode
	,EpisodicGpPracticeName = EpisodicCCG.Organisation
	,EpisodicCCGCode = 
		coalesce(
			EpisodicCCG.ParentOrganisationCode, --CascadeSrc or PracticeHistory.practiceCode
			case 
			--For Scotland, NI and CI (NOT WALES) use DistrictOfResidence as lookup instead of PCTCode
				when left(PostcodeCCG.DistrictOfResidence,1) in ('S','Z','Y') 
				then PostcodeCCG.DistrictOfResidence
				else PostcodeCCG.CCGCode
			end,
			'01N'
		)
	------------------------------------------------------------------------------
	,CommissionerCodeCCG = 
		LEFT(
			coalesce(
				EpisodicCCG.ParentOrganisationCode, --CascadeSrc or PracticeHistory.practiceCode
				case when Encounter.ResidencePostcode like 'ZZ%' then null else
					case 
					--For Scotland, NI and CI (NOT WALES) use DistrictOfResidence as lookup instead of PCTCode
						when left(PostcodeCCG.DistrictOfResidence,1) in ('S','Z','Y') 
						then PostcodeCCG.DistrictOfResidence
						else PostcodeCCG.CCGCode
					end
				end,
				'01N'
			)
			,5) + '00'
	--,SourceOfCCG = 
	--	coalesce(
	--		case when EpisodicCCG.ParentOrganisationCode is not null then Def2 end,
	--		case when 
	--			case 
	--			--For Scotland, NI and CI (NOT WALES) use DistrictOfResidence as lookup instead of PCTCode
	--				when left(PostcodeCCG.DistrictOfResidence,1) in ('S','Z','Y') 
	--				then PostcodeCCG.DistrictOfResidence
	--				else PostcodeCCG.CCGCode
	--			end is not null then 'Post' end,
	--		'01N'
	--	)
	-------------for testing	
	--,Def2 
	--,CascadeSrc
	--,PASPracticeCode
	--,TPracCloseDate = isnull(EpisodicCCG.CloseDate, convert(varchar(8), getdate(), 112))
	--,TArrDate = convert(varchar(8), convert(datetime, Encounter.ArrivalDate), 112) 
	--,CascPracEncPracCode
	--,CascPracPatPracCode
	--,CascPracCode
	--,PASCCG = PasHistoryCCG.ParentOrganisationCode
	-------------
	,ReferGP--added 07/01/2014 to enable reporting for PbR of those patients deflected back to GP from A&E 
	
from
	(
	select --top 50
		 SourceUniqueID = Encounter.aepatno + Encounter.attno
		 ,LocalPatientID = 
			case
			when PatRm2Number = ''
			then null
			else PatRm2Number
			end
		,ResidencePostcode =
			case
				when PatPostcode = '' then null
				when len(PatPostcode) = 6 then left(PatPostcode, 2) + '   ' + right(PatPostcode, 3)
				when len(PatPostcode) = 7 then left(PatPostcode, 3) + '  ' + right(PatPostcode, 3)
				else PatPostcode
			end
		
		--,RegisteredGpCode =
		--		coalesce(
		--			 dbo.GetConsultantNationalCode('G', GpPatGpNatCode)
		--			,dbo.GetConsultantNationalCode('G', GpEncGpNatCode)
		--		)
				
		--,RegisteredGPName = 
		--	case 
		--		when isnull(GpPatGpNatCode, '') = ''
		--		then case 
		--			when isnull(GpEncGpNatCode, '') <> ''
		--			then rtrim(GpEncGpSurname) + coalesce(', ' + GpEncGpInit, '')
		--		end
		--		else rtrim(GpPatGpSurname) + coalesce(', ' + GpPatGpInit, '')
		--	end
			
		--,RegisteredGpPracticeCode =
		--	coalesce(
		--		 PracPatPracCode
		--		,PracEncPracCode
		--	)
			
		--,RegisteredGPPracticeName = 
		--	case 
		--		when isnull(PracPatPracName, '') = ''
		--		then case 
		--			when isnull(PracEncPracName, '') <> ''
		--			then PracEncPracName
		--		end
		--		else PracPatPracName
		--	end
			
		--,RegisteredGPPracticePostcode = 
		--	case 
		--		when isnull(PracPatPracPostcode, '') = ''
		--		then case 
		--			when isnull(PracEncPracPostcode, '') <> ''
		--			then PracEncPracPostcode
		--		end
		--		else PracPatPracPostcode
		--	end
			
		------For Testing------------
		--,PASPracticeCode = Encounter.PASPracticeCode
		--,CascPracEncPracCode = PracEncPracCode
		--,CascPracPatPracCode = PracPatPracCode
		--,CascPracCode = Encounter.EpisodicPracCode
		------------------------------

		,EpisodicGpPracticeCode = Encounter.PASPracticeCode --PASHist
			--coalesce(
			--	EpisodicPracticeCCG.OrganisationCode --CascadeSrc
			--	,Encounter.PASPracticeCode --PASHist
			--)
			

		--,Def2 = 
		--	case
		--		when EpisodicPracticeCCG.OrganisationCode is not null then Def1 
		--		when Encounter.PASPracticeCode is not null then 'PasHist' 
		--		end
		--,CascadeSrc = Def1
		,ArrivalDate 
		,refergp--added 07/01/2014 to enable reporting for PbR of those patients deflected back to GP from A&E 
	from
		(
		select
			 Encounter.aepatno
			,Encounter.attno
			,Triage.ArrivalDate 
			,Encounter.refergp
			,PASPracticeCode = PracticeHistory.practiceCode
			,PatPostcode = ltrim(rtrim(Patient.postcode))
		
			--,GpPatGpNatCode = GpPatient.gpnatcode
			--,GpPatGpSurname = GpPatient.gpsurname
			--,GpPatGpInit = GpPatient.gpini
			--,GpEncGpNatCode = GpEncounter.gpnatcode
			--,GpEncGpSurname = GpEncounter.gpsurname
			--,GpEncGpInit = GpEncounter.gpini
			
			--,PracPatPracCode = PracticePatient.pracncode
			--,PracPatPracName = PracticePatient.prname
			--,PracPatPracPostcode = PracticePatient.prpostcode
			--,PracEncPracCode = PracticeEncounter.pracncode
			--,PracEncPracName = PracticeEncounter.prname
			--,PracEncPracPostcode = PracticeEncounter.prpostcode
			,PatRm2Number = Patient.rm2number
			--,EpisodicPracCode = --Cascade src see Def1
			--	case
			--		when coalesce(
			--				PracticeEncounter.pracncode
			--				,PracticePatient.pracncode
			--			) = ''
			--		then null
			--		else coalesce(
			--				PracticeEncounter.pracncode
			--				,PracticePatient.pracncode
			--			)
			--	end
				
			--,Def1 = 
			--	case
			--		when coalesce(
			--				PracticeEncounter.pracncode
			--				,PracticePatient.pracncode
			--			) = ''
			--		then null
			--		when PracticeEncounter.pracncode is not null then 'CasEnc' 
			--		when PracticePatient.pracncode is not null then 'CasPat' 
			--	end

			
		from [CASCADE].[AEAttend] Encounter
		
		inner join [CASCADE].Triage Triage
			on Triage.SourceUniqueID = Encounter.aepatno + Encounter.attno
		
		left join [CASCADE].AEPatMas Patient
			on	Patient.aepatno = Encounter.aepatno

		--left join [CASCADE].AEGpFile GpEncounter
		--	on	GpEncounter.gpref = Encounter.gpref
		--	and	Encounter.practice = 'false'

		--left join [CASCADE].AEGpFile GpPatient
		--	on	GpPatient.gpref = Patient.gpref
		--	and	Patient.practice = 'false'

		--left join [CASCADE].AEGpPrac PracticeEncounter
		--	on	PracticeEncounter.prcode = GpEncounter.prcode

		--left join [CASCADE].AEGpPrac PracticePatient
		--	on	PracticePatient.prcode = GpPatient.prcode

		left join PAS.PatientGPHistory PracticeHistory
			on PracticeHistory.DistrictNo = Patient.rm2number
			and Triage.ArrivalDate >= PracticeHistory.StartDate
			and (Triage.ArrivalDate < PracticeHistory.EndDate Or PracticeHistory.EndDate Is Null)

		) Encounter
			
	--left join OrganisationCCG.dbo.Practice EpisodicPracticeCCG --Cascade src
	--on	EpisodicPracticeCCG.OrganisationCode = Encounter.EpisodicPracCode
	--and  convert(varchar(8), Encounter.ArrivalDate, 112) 
	--	< isnull(EpisodicPracticeCCG.CloseDate, convert(varchar(8), getdate(), 112)) 
	
	left join OrganisationCCG.dbo.Practice PASPracticeCCG
	on	PASPracticeCCG.OrganisationCode = ISNULL(Encounter.PASPracticeCode, 'V81999')

	
	) Encounter

--left join OrganisationCCG.dbo.Practice RegisteredCCG
--on	RegisteredCCG.OrganisationCode = Encounter.RegisteredGpPracticeCode

left join OrganisationCCG.dbo.Practice EpisodicCCG
on	EpisodicCCG.OrganisationCode = Encounter.EpisodicGpPracticeCode

--left join OrganisationCCG.dbo.Practice PasHistoryCCG
--on	PasHistoryCCG.OrganisationCode = Encounter.PASPracticeCode

left join OrganisationCCG.dbo.Postcode PostcodeCCG
	on	PostcodeCCG.Postcode =
		case
		when len(Encounter.ResidencePostcode) = 8 then Encounter.ResidencePostcode
		else left(Encounter.ResidencePostcode, 3) + ' ' + right(Encounter.ResidencePostcode, 4) 
		end

left join LK.PatientCurrentDetails CurrentGP
	on CurrentGP.FacilityID = Encounter.LocalPatientID


