﻿


CREATE view [dbo].[TLoadAEInvestigation] as

select
	 SourceUniqueID = null
	,InvestigationDate = null

	,SequenceNo =
		ROW_NUMBER()
		over(
			partition by
				diagref
			order by
				weighting desc
		)


	,InvestigationCode =
		case
		when codefound = '' then null
		else codefound
		end

	,AESourceUniqueID = diagref

	,SourceInvestigationCode =
		case
		when codefound = '' then null
		else codefound
		end

	,ResultDate = null
	--,codetype
from
	[CASCADE].AECarCod
where
	codetype = 'I'



