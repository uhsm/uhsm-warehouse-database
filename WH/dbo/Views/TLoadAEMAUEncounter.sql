﻿



CREATE view [dbo].[TLoadAEMAUEncounter] as

select
	 SourceUniqueID = Encounter.SourceUniqueID
	,UniqueBookingReferenceNo = null
	,PathwayId = null
	,PathwayIdIssuerCode = null
	,RTTStatusCode = null
	,RTTStartDate = null
	,RTTEndDate = null

	,DistrictNo = Encounter.DistrictNo

	,TrustNo = null

	,CasenoteNo = Encounter.SourcePatientNo

	,DistrictNoOrganisationCode = null

	,NHSNumber = Encounter.NHSNumber

	,NHSNumberStatusId = null

	,PatientTitle = Encounter.PatientTitle
	,PatientForename = Encounter.PatientForename
	,PatientSurname = Encounter.PatientSurname
	,PatientAddress1 = Encounter.PatientAddress1
	,PatientAddress2 = Encounter.PatientAddress2
	,PatientAddress3 = Encounter.PatientAddress3
	,PatientAddress4 = Encounter.PatientAddress4
	,Postcode = Encounter.Postcode
	,DateOfBirth = Encounter.DateOfBirth
	,DateOfDeath = Encounter.DateOfDeath

	,SexCode =
		case Encounter.SexCode
		when 9269 --Female
		then 'F'
		when 9270 --Male
		then 'M'
		else null
		end

	,CarerSupportIndicator = null

	,RegisteredGpCode = null

	,RegisteredGpPracticeCode = null

	,AttendanceNumber = '001'
	,ArrivalModeCode = '4' --GP
	,AttendanceCategoryCode = 'WA' -- initial

	,AttendanceDisposalCode =
		case
		when Encounter.DisposalCode = 1461 --Admitted from Clinic
		then 'AD'
		else 'DA'
		end

	,IncidentLocationTypeCode = null
	,PatientGroupCode = null

	,SourceOfReferralCode = null

	,ArrivalDate = Encounter.AppointmentDate

	,AgeOnArrival =
	case
	when Encounter.DateOfBirth >= Encounter.AppointmentDate
	then 0
	else
		datediff(year, Encounter.DateOfBirth, Encounter.AppointmentDate) -
		case
		when Encounter.DateOfBirth >= Encounter.AppointmentDate
		then 0
		when datepart(day, Encounter.DateOfBirth) = datepart(day, Encounter.AppointmentDate)
		and	datepart(month, Encounter.DateOfBirth) = datepart(month,Encounter.AppointmentDate)
		and datepart(year, Encounter.DateOfBirth) <> datepart(year,Encounter.AppointmentDate)
		then 0 
		else 
			case
			when Encounter.DateOfBirth > Encounter.AppointmentDate then 0
			when datepart(dy, Encounter.DateOfBirth) > datepart(dy, Encounter.AppointmentDate) 
			then 1
			else 0 
			end 
		end
	end

	,CommissioningSerialNo = null
	,NHSServiceAgreementLineNo = null
	,ProviderReferenceNo = null
	,CommissionerReferenceNo = null
	,ProviderCode = null
	,CommissionerCode = null
	,StaffMemberCode = null
	,InvestigationCodeFirst = null
	,InvestigationCodeSecond = null
	,DiagnosisCodeFirst = null
	,DiagnosisCodeSecond = null
	,TreatmentCodeFirst = null
	,TreatmentCodeSecond = null
	,PASHRGCode = null
	,HRGVersionCode = null
	,PASDGVPCode = null
	,SiteCode = null
	,Created = null
	,Updated = null
	,ByWhom = null
	,InterfaceCode = Encounter.InterfaceCode
	,PCTCode = null
	,ResidencePCTCode = null
	,InvestigationCodeList = null
	,TriageCategoryCode = 'G' --green
	,PresentingProblem = null
	,EthnicCategoryCode = null

	,ReferredToSpecialtyCode = null

	,DischargeDestinationCode = null

--various key times
	,ArrivalTime = Encounter.AppointmentTime

	,InitialAssessmentTime = Encounter.AppointmentTime

	,SeenForTreatmentTime = Encounter.AppointmentTime

	,AttendanceConclusionTime = Encounter.AppointmentTime

	,DepartureTime = null

	,ToXrayTime = null

	,FromXrayTime = null

	,ToSpecialtyTime =
		case
		when Encounter.DisposalCode = 1461 --Admitted from Clinic
		then Encounter.AppointmentTime
		else null
		end

	,SeenBySpecialtyTime = null

	,DecisionToAdmitTime = 
		case
		when Encounter.DisposalCode = 1461 --Admitted from Clinic
		then Encounter.AppointmentTime
		else null
		end

	,RegisteredTime = Encounter.AppointmentTime

--CASCADE times not aleady recorded above

	,CardTime = null

	,ReviewClinicTime = null

	,ReferOnTime = null

	,ClinicAppointmentTime = null

	,StaffTime = null

from
	OP.Encounter Encounter
where
	Encounter.ClinicCode = 'A10'






