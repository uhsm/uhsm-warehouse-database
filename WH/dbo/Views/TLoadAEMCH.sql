﻿CREATE view TLoadAEMCH as

select
	Period = CONVERT(smalldatetime, F1)
	,Days = convert(float, F2)
	,FirstAttends = CONVERT(int, F3)
	,ReviewAttends = CONVERT(int, F4)
	,Attends = CONVERT(int, F5)
	,OtherAttends = CONVERT(int, F6)
	,SiteCode
from
	TImportAEMCH
where
	isdate(F1) = 1
