﻿



CREATE view [dbo].[TLoadAEProcedure] as

select
	 SourceUniqueID = null
	,ProcedureDate = null

	,SequenceNo =
		ROW_NUMBER()
		over(
			partition by
				diagref
			order by
				weighting desc
		)


	,ProcedureCode =
		case
		when codefound = '' then null
		else codefound
		end

	,AESourceUniqueID = diagref

	,SourceProcedureCode =
		case
		when codefound = '' then null
		else codefound
		end

from
	[CASCADE].AECarCod
	inner join AE.Encounter Encounter
on Encounter.SourceUniqueID = AECarCod.diagref
where
	codetype = 'T'
and	diagref != ''



