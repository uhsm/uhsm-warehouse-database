﻿create view [dbo].[TLoadAPCBirth] as

select
	 SourceUniqueID
	,MaternitySpellSourceUniqueID
	,SourcePatientNo
	,ReferralSourceUniqueID
	,ProfessionalCarerCode
	,MotherSourcePatientNo
	,BirthOrder
	,DateOfBirth
	,SexCode
	,StatusOfPersonConductingDeliveryCode
	,LiveOrStillBirthCode
	,DeliveryPlaceCode
	,ResuscitationMethodByPressureCode
	,ResuscitationMethodByDrugCode
	,BirthWeight
	,ApgarScoreAt1Minute
	,ApgarScoreAt5Minutes
	,BCGAdministeredCode
	,CircumferenceOfBabysHead
	,LengthOfBaby
	,ExaminationOfHipsCode
	,FollowUpCareCode
	,FoetalPresentationCode
	,MetabolicScreeningCode
	,JaundiceCode
	,FeedingTypeCode
	,DeliveryMethodCode
	,MaternityDrugsCode
	,GestationLength
	,ApgarScoreAt10Minutes
	,NeonatalLevelOfCareCode
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,ArchiveFlag
from
	dbo.TImportAPCBirth


