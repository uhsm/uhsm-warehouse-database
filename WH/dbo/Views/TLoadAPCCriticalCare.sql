﻿CREATE view [dbo].[TLoadAPCCriticalCare] as

select
	 SourceUniqueID
	,AdvancedCardiovascularSupportDays
	,AdvancedRespiratorySupportDays
	,BasicCardiovascularSupportDays
	,BasicRespiratorySupportDays
	,CriticalCareLevel2Days
	,CriticalCareLevel3Days
	,CreatedByUser
	,CreatedByTime
	,DermatologicalSupportDays
	,EndDate

	,EndTime =
		EndDate + ' ' +
		case
		when right(EndTimeInt, 4) = '2400'
		then '23:59'
		else left(right(EndTimeInt, 4), 2) + ':' + right(EndTimeInt, 2)
		end

	,LiverSupportDays
	,LocalIdentifier
	,LocationCode
	,NeurologicalSupportDays
	,RenalSupportDays
	,StartDate

	,StartTime =
		case
		when StartTime = ':'
		then null
		else StartDate + ' ' + case when StartTime = '24:00' then '23:59' else StartTime end
		end

	,StatusCode
	,TreatmentFunctionCode
	,SourceSpellNo
	,SourcePatientNo
	,PlannedAcpPeriod

from
	dbo.TImportAPCCriticalCare
where
	SourceUniqueID <> '517821||20||200710181649' --dodgy end date
