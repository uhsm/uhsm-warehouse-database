﻿CREATE VIEW TLoadAPCCriticalCarePeriod AS

SELECT
	 ccp.SourceCCPStayNo
	,ccp.SourceCCPNo
	,CcpSequenceNo
	,SourceEncounterNo
	,SourceWardStayNo
	,SourceSpellNo
	,CcpIndicator
	,CcpStartDate
	,CcpEndDate
	,CcpReadyDate
	,CcpNoOfOrgansSupported
	,CcpICUDays
	,CcpHDUDays
	,CcpAdmissionSource
	,CcpAdmissionType
	,CcpAdmissionSourceLocation
	,CcpUnitFunction
	,CcpUnitBedFunction
	,CcpDischargeStatus
	,CcpDischargeDestination
	,CcpDischargeLocation
	,CcpStayOrganSupported = ISNULL(Ccp.CcpstayOrganSupported,-1)
	,CcpStayCareLevel
	,CCPStayNoOfOrgansSupported
	,CcpStayWard
	,ccp.CCPStayDate
	,CcpStay
	,CcpStayType = 
		case Ccp.CcpStayCareLevel 
			when 1 then 'Level 1'
			when 2 then 'HDU'
			when 3 then 'ICU'
			Else 'Unknown'
		end
	,CcpBedStay = 
				case when Bedstay.SourceCCPStayNo is null then 
					Case When Ccp.CCPStayOrganSupported is null then
						1
					else
						0
					end
				else 
					1 
				end
	
	FROM TImportAPCCriticalCarePeriod Ccp
	left outer join (
						SELECT 
							 SourceCCPStayNo
							 ,Min(CCPStayOrganSupported) as FStay 
						from TImportAPCCriticalCarePeriod
						GROUP BY
							 SourceCCPStayNo
					) BedStay 
	On Ccp.SourceCCPStayNo = BedStay.SourceCCPStayNo and Ccp.CCPStayOrganSupported = BedStay.FStay
	
	
	
	
	
	
