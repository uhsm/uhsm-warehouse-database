﻿CREATE view [dbo].[TLoadAPCDiagnosis] as

select
	 SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo

	,ProviderSpellNo =
		SourcePatientNo + '/' + SourceSpellNo

	,SourceEncounterNo
	,SequenceNo
	,DiagnosisCode
	,APCSourceUniqueID
from
	dbo.TImportAPCDiagnosis
