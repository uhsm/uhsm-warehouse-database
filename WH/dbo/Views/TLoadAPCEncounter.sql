﻿
/*********************************

06/01/2016 CM - Amended the view so that the Episodic GP Practice is now populated with the new  PAS.PatientGPHistory created on 20/10/2015



**********************************/


CREATE  view [dbo].[TLoadAPCEncounter] as

select
	 Encounter.SourcePatientNo
	,Encounter.SourceSpellNo
	,Encounter.SourceEncounterNo
	,Encounter.ProviderSpellNo
	,Encounter.ReferralSourceUniqueID
	,Encounter.WaitingListSourceUniqueID
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath
	,Encounter.SexCode
	,Encounter.NHSNumber

	,Postcode =
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end

	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.DHACode
	,Encounter.EthnicOriginCode
	,Encounter.MaritalStatusCode
	,Encounter.ReligionCode
	,Encounter.DateOnWaitingList
	,Encounter.AdmissionDate
	,Encounter.DischargeDate
	,Encounter.EpisodeStartDate
	,Encounter.EpisodeEndDate
	,Encounter.StartSiteCode
	,Encounter.StartWardTypeCode
	,Encounter.EndSiteCode
	,Encounter.EndWardTypeCode
	,Encounter.RegisteredGpCode
	,Encounter.RegisteredGpPracticeCode
	,Encounter.SiteCode
	,Encounter.AdmissionMethodCode
	,Encounter.AdmissionSourceCode
	,Encounter.PatientClassificationCode
	,Encounter.ManagementIntentionCode
	,Encounter.DischargeMethodCode
	,Encounter.DischargeDestinationCode
	,Encounter.AdminCategoryCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.LastEpisodeInSpellIndicator
	,Encounter.FirstRegDayOrNightAdmit
	,Encounter.NeonatalLevelOfCare
	,Encounter.PASHRGCode
	,Encounter.PrimaryDiagnosisCode
	,Encounter.SubsidiaryDiagnosisCode
	,Encounter.SecondaryDiagnosisCode1
	,Encounter.SecondaryDiagnosisCode2
	,Encounter.SecondaryDiagnosisCode3
	,Encounter.SecondaryDiagnosisCode4
	,Encounter.SecondaryDiagnosisCode5
	,Encounter.SecondaryDiagnosisCode6
	,Encounter.SecondaryDiagnosisCode7
	,Encounter.SecondaryDiagnosisCode8
	,Encounter.SecondaryDiagnosisCode9
	,Encounter.SecondaryDiagnosisCode10
	,Encounter.SecondaryDiagnosisCode11
	,Encounter.SecondaryDiagnosisCode12
	,Encounter.PrimaryOperationCode
	,Encounter.PrimaryOperationDate
	,Encounter.SecondaryOperationCode1
	,Encounter.SecondaryOperationDate1
	,Encounter.SecondaryOperationCode2
	,Encounter.SecondaryOperationDate2
	,Encounter.SecondaryOperationCode3
	,Encounter.SecondaryOperationDate3
	,Encounter.SecondaryOperationCode4
	,Encounter.SecondaryOperationDate4
	,Encounter.SecondaryOperationCode5
	,Encounter.SecondaryOperationDate5
	,Encounter.SecondaryOperationCode6
	,Encounter.SecondaryOperationDate6
	,Encounter.SecondaryOperationCode7
	,Encounter.SecondaryOperationDate7
	,Encounter.SecondaryOperationCode8
	,Encounter.SecondaryOperationDate8
	,Encounter.SecondaryOperationCode9
	,Encounter.SecondaryOperationDate9
	,Encounter.SecondaryOperationCode10
	,Encounter.SecondaryOperationDate10
	,Encounter.SecondaryOperationCode11
	,Encounter.SecondaryOperationDate11
	,Encounter.OperationStatusCode
	,Encounter.ContractSerialNo
	,Encounter.CodingCompleteFlag
	,Encounter.PurchaserCode
	,Encounter.ProviderCode
	,Encounter.EpisodeStartTime
	,Encounter.EpisodeEndTime
	,Encounter.RegisteredGdpCode
	,Encounter.EpisodicGpCode
	,Encounter.InterfaceCode
	,Encounter.CasenoteNumber
	,Encounter.NHSNumberStatusCode
	,Encounter.AdmissionTime
	,Encounter.DischargeTime
	,Encounter.TransferFrom
	,Encounter.DistrictNo
	,Encounter.SourceUniqueID
	,Encounter.ExpectedLOS
	,Encounter.MRSAFlag
	,Encounter.RTTPathwayID
	,Encounter.RTTPathwayCondition
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.RTTSpecialtyCode
	,Encounter.RTTCurrentProviderCode
	,Encounter.RTTCurrentStatusCode
	,Encounter.RTTCurrentStatusDate
	,Encounter.RTTCurrentPrivatePatientFlag
	,Encounter.RTTOverseasStatusFlag
	,Encounter.RTTPeriodStatusCode
	,Encounter.OverseasStatusFlag
	,EpisodicGpPracticeCode = PracticeHistory.PracticeRefno--Encounter.RegisteredGpPracticeCode

	,PCTCode = null
--		coalesce(
--			 Practice.ParentOrganisationCode
--			,Postcode.PCTCode
--			,'X98'
--		)

	,LocalityCode = null
--	,LocalityCode = LocalAuthority.LocalAuthorityCode


	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom

	,Encounter.ArchiveFlag

	,Encounter.EpisodeOutcomeCode
	,Encounter.LegalStatusClassificationOnAdmissionCode
	,Encounter.LegalStatusClassificationOnDischargeCode
	,Encounter.DecisionToAdmitDate
	,Encounter.OriginalDecisionToAdmitDate
	,Encounter.DurationOfElectiveWait
from
	dbo.TImportAPCEncounter Encounter
	
left join PAS.PatientGPHistory PracticeHistory
on Encounter.SourcePatientNo =  PracticeHistory.PatientNo
			and Encounter.AdmissionDate >= PracticeHistory.StartDate
			and (Encounter.AdmissionDate < PracticeHistory.EndDate Or PracticeHistory.EndDate Is Null)

--LEFT JOIN PAS.Gp EpisodicGp 
--ON	Encounter.EpisodicGpCode = EpisodicGp.GpCode
--
--LEFT JOIN PAS.Gp RegisteredGp 
--ON	Encounter.RegisteredGpCode = RegisteredGp.GpCode
--
--left join Organisation.dbo.Practice Practice
--on	Practice.OrganisationCode = coalesce(EpisodicGp.PracticeCode, RegisteredGp.PracticeCode)
--
--left join Organisation.dbo.Postcode Postcode
--on	Postcode.Postcode =
--		case
--		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
--		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
--		else Encounter.Postcode
--		end
--
--left join Organisation.dbo.LocalAuthority LocalAuthority
--on	LocalAuthority.LocalAuthorityCode = Postcode.LocalAuthorityCode




