﻿CREATE  view [dbo].[TLoadAPCMidnightBedState] as

select
	 SourceUniqueID
    ,SourcePatientNo
    ,SourceSpellNo
    ,ProviderSpellNo
    ,SiteCode
    ,WardCode
    ,ConsultantCode
    ,SpecialtyCode
    ,SourceAdminCategoryCode
    ,ActivityInCode

    ,AdmissionTime = 
		convert(
			 smalldatetime
			,replace(
				left(
					convert(
						 varchar
						,convert(
							 smalldatetime
							,left(AdmissionDateTimeInt, 8)
							,112
						)
					)
					,11
				) + ' ' +
				substring(AdmissionDateTimeInt, 9, 2) + ':' + substring(AdmissionDateTimeInt, 11, 2)
				,'24:00'
				,'23:59'
			)
		)

	,AdmissionDate = 
		convert(
			 varchar
			,convert(
				 smalldatetime
				,left(AdmissionDateTimeInt, 8)
				,112
			)
		)

    ,CensusDate = 
		convert(
			 varchar
			,convert(
				 smalldatetime
				,left(MidnightBedStateDate, 8)
				,112
			)
		)
		

    ,InterfaceCode
from
	TImportAPCMidnightBedState
