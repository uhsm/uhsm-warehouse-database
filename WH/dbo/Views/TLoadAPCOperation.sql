﻿CREATE view [dbo].[TLoadAPCOperation] as

select
	 SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo

	,ProviderSpellNo =
		SourcePatientNo + '/' + SourceSpellNo

	,SourceEncounterNo
	,SequenceNo
	,OperationCode
	,OperationDate
	,ConsultantCode
	,APCSourceUniqueID
from
	dbo.TImportAPCOperation
