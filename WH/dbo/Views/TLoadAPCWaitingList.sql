﻿












CREATE  view [dbo].[TLoadAPCWaitingList] as

select
	 Encounter.CensusDate
	,Encounter.SourceUniqueID
	,Encounter.SourcePatientNo
	,Encounter.SourceEncounterNo
	,Encounter.ReferralSourceUniqueID
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath
	,Encounter.SexCode
	,Encounter.NHSNumber
	,Encounter.DistrictNo

	,Postcode =
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end

	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.DHACode
	,Encounter.HomePhone
	,Encounter.WorkPhone
	,Encounter.EthnicOriginCode
	,Encounter.MaritalStatusCode
	,Encounter.ReligionCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.PASSpecialtyCode
	,Encounter.ManagementIntentionCode
	,Encounter.AdmissionMethodCode
	,Encounter.PriorityCode
	,Encounter.WaitingListCode
	,Encounter.CommentClinical
	,Encounter.CommentNonClinical
	,Encounter.IntendedPrimaryOperationCode
	,Encounter.Operation
	,Encounter.SiteCode
	,Encounter.WardCode
	,Encounter.WLStatus
	,Encounter.PurchaserCode
	,Encounter.ProviderCode
	,Encounter.ContractSerialNo
	,Encounter.AdminCategoryCode
	,Cancelled.CancelledBy
	,Encounter.BookingTypeCode
	,Encounter.CasenoteNumber
	,Encounter.OriginalDateOnWaitingList
	,Encounter.DateOnWaitingList
	,Encounter.TCIDate
	,Encounter.KornerWait
	,Encounter.CountOfDaysSuspended
	,Encounter.SuspensionStartDate
	,Encounter.SuspensionEndDate
	,Encounter.SuspensionReasonCode
	,Encounter.SuspensionReason
	,Encounter.InterfaceCode
	,Encounter.RegisteredGpCode
	,Encounter.RegisteredGpPracticeCode
	,Encounter.EpisodicGpCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.SourceTreatmentFunctionCode
	,Encounter.TreatmentFunctionCode
	,Encounter.NationalSpecialtyCode

	,PCTCode =
		coalesce(
			 PCT.OrganisationLocalCode
			,Postcode.PCTCode
			,'X98'
		)
		
	,PCTCCGCode =
		case when Encounter.DateOnWaitingList > '2013-03-31 00:00:00' then
			coalesce(
				 ODSPractice.ParentOrganisationCode
				,PostcodeCCG.CCGCode
				,'X98'
			)
		else 
			coalesce(
			 PCT.OrganisationLocalCode
			,Postcode.PCTCode
			,'X98'
		)
		end
	,Encounter.BreachDate
	,Encounter.ExpectedAdmissionDate
	,Encounter.ExpectedDischargeDate
	,Encounter.ReferralDate

	,FuturePatientCancelDate =
		case
		when Cancelled.CancelledBy = 'P'
		then Encounter.FutureCancelDate
		end

	,Encounter.FutureCancelDate
	,Encounter.ProcedureTime
	,Encounter.TheatreCode
	,Encounter.AnaestheticTypeCode
	,Encounter.AdmissionReason
	,Encounter.EpisodeNo
	,Encounter.BreachDays
	,Encounter.MRSAFlag
	,Encounter.RTTPathwayID
	,Encounter.RTTPathwayCondition
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.RTTSpecialtyCode
	,Encounter.RTTCurrentProviderCode
	,Encounter.RTTCurrentStatusCode
	,Encounter.RTTCurrentStatusDate
	,Encounter.RTTCurrentPrivatePatientFlag
	,Encounter.RTTOverseasStatusFlag
	,Encounter.NationalBreachDate
	,Encounter.NationalBreachDays
	,Encounter.DerivedBreachDays
	,Encounter.DerivedClockStartDate
	,Encounter.DerivedBreachDate
	,Encounter.RTTBreachDate
	,Encounter.RTTDiagnosticBreachDate
	,Encounter.NationalDiagnosticBreachDate
	,Encounter.SocialSuspensionDays
	,Encounter.BreachTypeCode
	,Encounter.PASCreated
	,Encounter.PASUpdated
	,Encounter.PASCreatedByWhom
	,Encounter.PASUpdatedByWhom
	,Encounter.ArchiveFlag
	,Encounter.CancelReasonCode
	,Encounter.GeneralComment
	,Encounter.PatientPreparation
	,Encounter.AdmissionOfferOutcomeCode
	,Encounter.EstimatedTheatreTime
	,Encounter.WhoCanOperateCode
	,Encounter.WaitingListRuleCode
	,Encounter.ShortNoticeCode
	,Encounter.PreOpDate
	,Encounter.PreOpClinicCode
	,Encounter.AdmissionOfferSourceUniqueID
	,Encounter.LocalcategoryCode
from
	dbo.TImportAPCWaitingList Encounter

left join PAS.Organisation IPMPractice
on	IPMPractice.OrganisationCode = Encounter.RegisteredGpPracticeCode

left join PAS.Organisation PCT
on	PCT.OrganisationCode = IPMPractice.ParentOrganisationCode

left join Organisation.dbo.Postcode Postcode
on	Postcode.Postcode =
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end

left join OrganisationCCG.dbo.Postcode PostcodeCCG
on	PostcodeCCG.Postcode =
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end

left join OrganisationCCG.dbo.Practice ODSPractice
	on ODSPractice.OrganisationCode = IPMPractice.OrganisationLocalCode
	
	
----PDO 24 Feb 2012
----Use the admission offer outcome to determine a cancellation

left join
	(
	select AdmissionOfferOutcomeCode = 985, CancelledBy = 'P' union all --Did Not Attend (DNA)
	select AdmissionOfferOutcomeCode = 2003605, CancelledBy = 'H' union all --Cancelled
	--select AdmissionOfferOutcomeCode = 2005609, CancelledBy = 'H' union all --18 DTT WWait Treatment Completed
	--select AdmissionOfferOutcomeCode = 2005613, CancelledBy = 'H' union all --18 DTT Add to WL Treatment Commenced
	--select AdmissionOfferOutcomeCode = 2005617, CancelledBy = 'H' union all --18 DTT Add to WL Diag Test Treatment Complete
	--select AdmissionOfferOutcomeCode = 981, CancelledBy = 'H' union all --Admitted - Treatment Completed
	--select AdmissionOfferOutcomeCode = 2003601, CancelledBy = 'H' union all --No Contact
	--select AdmissionOfferOutcomeCode = 2003604, CancelledBy = 'H' union all --PIMS USER
	--select AdmissionOfferOutcomeCode = 2005610, CancelledBy = 'H' union all --18 DTT WWait Treatment Deferred
	--select AdmissionOfferOutcomeCode = 2005612, CancelledBy = 'H' union all --18 Ref to External Cons - Treatment Deferred
	--select AdmissionOfferOutcomeCode = 2003602, CancelledBy = 'H' union all --Patient Admitted
	--select AdmissionOfferOutcomeCode = 2005618, CancelledBy = 'H' union all --18 DTT Add to WL Diag Test Treatment Deferred
	select AdmissionOfferOutcomeCode = 4423, CancelledBy = 'H' union all --Cancelled by hosp. on Day of Admission
	--select AdmissionOfferOutcomeCode = 2003603, CancelledBy = 'H' union all --Patient Admitted - Died
	--select AdmissionOfferOutcomeCode = 2005611, CancelledBy = 'H' union all --18 Ref to External Cons - Treatment Complete
	select AdmissionOfferOutcomeCode = 2000657, CancelledBy = 'P' union all --Admission Cancelled by or on behalf of Patient
	select AdmissionOfferOutcomeCode = 2003597, CancelledBy = 'H' union all --Admission Cancelled by Hospital
	select AdmissionOfferOutcomeCode = 3663, CancelledBy = 'H' union all --Admitted - Treatment Deferred
	--select AdmissionOfferOutcomeCode = 2005615, CancelledBy = 'H' union all --18 DTT Add to WL Treatment Deferred
	select AdmissionOfferOutcomeCode = 982, CancelledBy = 'H' union all --Cancelled by hosp. before Day of Admission
	--select AdmissionOfferOutcomeCode = 2005616, CancelledBy = 'H' union all --18 DTT Add to WL Diag Test Treatment Commenced
	--select AdmissionOfferOutcomeCode = 761, CancelledBy = 'H' union all --Not Specified
	--select AdmissionOfferOutcomeCode = 2005614, CancelledBy = 'H' union all --18 DTT Add to WL Treatment Complete
	select AdmissionOfferOutcomeCode = 2003599, CancelledBy = 'P' union all --Cancelled due to Patient Death
	select AdmissionOfferOutcomeCode = 2005636, CancelledBy = 'H' union all --Admission Cancelled by Hospital before Day of Admission
	--select AdmissionOfferOutcomeCode = 2003600, CancelledBy = 'H' union all --DNA (Did Not Attend)
	--select AdmissionOfferOutcomeCode = 2005608, CancelledBy = 'H' union all --18 DTT WWait Treatment Commenced
	--select AdmissionOfferOutcomeCode = 2003598, CancelledBy = 'H' union all --Patient Admitted - Treatment Deferred
	--select AdmissionOfferOutcomeCode = 2005620, CancelledBy = 'H'	union all --Admitted - Treatment Commenced
	select AdmissionOfferOutcomeCode = null, CancelledBy = null
	) Cancelled
on	Cancelled.AdmissionOfferOutcomeCode = Encounter.AdmissionOfferOutcomeCode


--left join
--	(
--	select CancelReasonCode = 61, CancelledBy = 'H' union all --Not Specified
--	select CancelReasonCode = 383, CancelledBy = 'H' union all --Patient died
--	select CancelReasonCode = 3248, CancelledBy = 'P' union all --Other - Patient cancellation
--	select CancelReasonCode = 3249, CancelledBy = 'H' union all --Slot cancelled
--	select CancelReasonCode = 2002068, CancelledBy = 'P' union all --C&B Cancellation
--	select CancelReasonCode = 2003537, CancelledBy = 'H' union all --Clinic overrun
--	select CancelReasonCode = 2003538, CancelledBy = 'H' union all --Other - Provider cancellation
--	select CancelReasonCode = 2003539, CancelledBy = 'P' union all --Patient ill
--	select CancelReasonCode = 2003540, CancelledBy = 'P' union all --Patient - On holiday
--	select CancelReasonCode = 2003541, CancelledBy = 'P' union all --Patient - Other more pressing engagement
--	select CancelReasonCode = 2003542, CancelledBy = 'H' union all --Clashes with another appointment
--	select CancelReasonCode = 2003543, CancelledBy = 'H' union all --Appt. made with wrong provider
--	select CancelReasonCode = 2003544, CancelledBy = 'P' union all --Other - Patient cancellation
--	select CancelReasonCode = 2003545, CancelledBy = 'H' union all --Cancelled
--	select CancelReasonCode = 2003546, CancelledBy = 'H' union all --Amalgamation
--	select CancelReasonCode = 2003547, CancelledBy = 'H' union all --Daycare Episode discharged
--	select CancelReasonCode = 2003548, CancelledBy = 'P' union all --Did Not Attend
--	select CancelReasonCode = 2004147, CancelledBy = 'H' union all --GP Request
--	select CancelReasonCode = 2004148, CancelledBy = 'H' union all --Patient is current Inpatient
--	select CancelReasonCode = 2004149, CancelledBy = 'H' union all --Appt. made in error
--	select CancelReasonCode = 2004150, CancelledBy = 'H' union all --Other
--	select CancelReasonCode = 2004338, CancelledBy = 'P' union all --C&B timed out
--	select CancelReasonCode = 2005554, CancelledBy = 'H' union all --Clinic cancelled
--	select CancelReasonCode = 2005621, CancelledBy = 'P' union all --Patient Declined
--	select CancelReasonCode = 2006150, CancelledBy = 'H' union all --Other reason
--	select CancelReasonCode = 2006151, CancelledBy = 'P' union all --Intend to go private
--	select CancelReasonCode = 2006152, CancelledBy = 'H' union all --Addional req unavailable for this appt
--	select CancelReasonCode = 2006153, CancelledBy = 'H' union all --Addional req not provided at location
--	select CancelReasonCode = 2006154, CancelledBy = 'H' union all --Not eligible for additional requirement
--	select CancelReasonCode = 2006155, CancelledBy = 'H' union all --Inappropriate priority
--	select CancelReasonCode = 2006156, CancelledBy = 'H' union all --Priority change for other reasons
--	select CancelReasonCode = 2006157, CancelledBy = 'H' union all --Treatment no longer required
--	select CancelReasonCode = 2006158, CancelledBy = 'H' union all --Further information required
--	select CancelReasonCode = 2006159, CancelledBy = 'H' union all --Inappropriate service
--	select CancelReasonCode = 2006160, CancelledBy = 'H' union all --Outpatient appointment not required
--	select CancelReasonCode = 2006161, CancelledBy = 'H' union all --Pre-requisites for service not complete
--	select CancelReasonCode = 2006162, CancelledBy = 'H' union all --Redirected due to confidentiality
--	select CancelReasonCode = 2006163, CancelledBy = 'H' union all --Redirected due to other reason
--	select CancelReasonCode = 2006164, CancelledBy = 'H' union all --Redirected due to patient age
--	select CancelReasonCode = 2006165, CancelledBy = 'H' union all --Redirected due to patient gender
--	select CancelReasonCode = 2006166, CancelledBy = 'H' union all --Redirected due to priority
--	select CancelReasonCode = 2006167, CancelledBy = 'H' union all --Redirected due to staff vacation
--	select CancelReasonCode = 2006168, CancelledBy = 'H' union all --Redirected to more appropriate clinic
--	select CancelReasonCode = 2006169, CancelledBy = 'H' --Redirected to more appropriate specialty
--	) Cancelled
--on	Cancelled.CancelReasonCode = Encounter.CancelReasonCode



where Encounter.SourceUniqueID
not in (select SourceUniqueID
from dbo.TImportAPCWaitingList
where PatientForename like 'xx%'
or PatientSurname like 'xx%'
)







