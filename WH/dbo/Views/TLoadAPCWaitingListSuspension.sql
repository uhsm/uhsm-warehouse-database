﻿CREATE view [dbo].[TLoadAPCWaitingListSuspension]
as

select
	 CensusDate
	,SourceUniqueID
	,WaitingListSourceUniqueID
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,ArchiveFlag
from
	dbo.TImportAPCWaitingListSuspension
