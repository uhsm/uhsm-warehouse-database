﻿
CREATE view [dbo].[TLoadAPCWardStay] as

select
	 SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo
	,StartTime
	,EndTime
	,WardCode
	,ProvisionalFlag
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,ArchiveFlag
from
	dbo.TImportAPCWardStay

