﻿
/*
--Author: K Oakden
--Date created: 05/09/2012
--TLoadMISYSAntenatal used to format antenatal data for load into MISYS.Antenatal table
*/
create view [dbo].[TLoadMISYSAntenatal] as (
select
	ante.order_number
	,ord.PerformanceDate
	,ord.PatientNo
	,ord.SocialSecurityNo
	,ante.CG_MAS_KV
	,ante.LMP_CHECK
	,ord.SuspensionDaysCode
	,ord.RequestReceivedDate
	,ante.EDD
	,ord.DelayedByPatient
	,ord.FailedPatientContact
	,ord.PlannedRequest
	,ante.Comment
	,ante.SCREENING_TIME
	,ante.CONTRAST
	,ante.COMPLETE
	,ante.TOO_EARLY
	,ante.TOO_LATE
	,ante.VIABLE
	,ante.POSSIBLE
	,ante.AUDITABLE_ANOMOLY
	,ante.exam_code_no_mh
from
	dbo.TImportMISYSAntenatal ante
		inner join MISYS.Encounter ord
		on ante.order_number = ord.OrderNumber
where ante.EDD IS NOT NULL
and ord.PerformanceJulianDate > 13331
)
