﻿



CREATE view [dbo].[TLoadMISYSEncounter] as

select
	 EncounterRecno

	,OrderNumber =
	case
	when OrderNumber like '[0-9][0-9][A-Z][0-9]%' then OrderNumber
	else OrderStatusCode
	end

	,PerformanceDate
	,CreationDate
	,EpisodeNumber
	,PerformanceTimeInt

	,PerformanceTime =
		DATEADD(
			SECOND
			,case
			when isnumeric(PerformanceTimeSeconds) = 0
			then
				case
				when isnumeric(PerformanceTimeInt) = 0
				then 0
				else convert(int, left(RIGHT('0000' + PerformanceTimeInt, 4), 2)) * 3600 + convert(int, right(RIGHT('0000' + PerformanceTimeInt, 4), 2)) * 60
				end
			when PerformanceTimeSeconds IS NULL then 0
			else convert(float, PerformanceTimeSeconds)
			end
			,PerformanceDate
		)

	,PatientName
	,Department
	,ExamDescription
	,ExamGroupCode
	,DocumentStatusCode
	,FilmEntStatusCode
	,PatientTrackingStatusCode
	,CloseDate
	,OpenDate
	,PerformanceJulianDate
	,OrderDate

	,OrderTime =
		DATEADD(
			SECOND
			,case
			when isnumeric(OrderTimeSeconds) = 0 then 0
			when OrderTimeSeconds IS NULL then 0
			else convert(float, OrderTimeSeconds)
			end
			,OrderDate
		)

	,OrderingLocationCode =
	case
	when OrderingLocationCode1 like '%;%'
	then LEFT(OrderingLocationCode1, charindex(';', OrderingLocationCode1) -1)
	else OrderingLocationCode
	end

	,PatientAge
	,PatientTypeCode = 
		Case When TImportMISYSEncounter.PatientTypeCode In ('IP','OP','ER') Then
			TImportMISYSEncounter.PatientTypeCode
		Else
			'AT'
		End
	,DateOfBirth
	,SexCode
	,OrderExamCode
	,PerformanceDateInt
	,CaseTypeCode

	,OrderStatusCode =
	case
	when OrderStatusCode like '[0-9][0-9][A-Z][0-9]%' then OrderStatusCode
	else OrderNumber
	end

	,AttendingPhysicianCode1
	,AttendingPhysicianCode2
	,CaseStatusCode
	,CaseStatusDate
	,DictationStatusCode
	,OrderScheduledFlag
	,OrderingPhysicianCode
	,PriorityCode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,AttendingPhysician1
	,LocationCode
	,PatientForename
	,PatientSurname
	,SocialSecurityNo
	,ExtraData1
	,ExtraData2
	,ExtraData3
	,Postcode
	,DateOfDeath
	,RegisteredGpPractice
	,RegisteredGpPracticeCode
	,xgen3
	,xgen4
	,NHSNumberVerifiedFlag
	,DepartmentCode
	,ExamCode1
	,HISOrderNumber
	,ExamVar4
	,AETitleLocationCode
	,PrimaryPhysicianCode
	,PrimaryPhysician
	,SpecialtyCode
	,SuspensionDaysCode
	,RequestReceivedDate
	,VettedDate
	,DelayedByPatient
	,FailedPatientContact
	,PlannedRequest
	,RequestDate
	,FilmDataFlag
	,OrderingPhysicianCode1
	,PatientNo
	,PatientGenderCode
	,PatientNo1
	,AttendingPhysician2
	,ScheduleStatusCode

	,OrderingLocationCode1 =
	case
	when OrderingLocationCode1 like '%;%'
	then OrderingLocationCode1
	else OrderingLocationCode + ';1'
	end

	,CancelDate
	,DictationDate
	,FinalDate
	,PatientTypeForLinkingCode
	,CaseTypeCode1
	,CaseType
	,ModifiedExamCode
	,LinkExamCode
	,LinkLocationCode
	,LinkScheduleRoomCode
	,LinkTechCode
	,LinkLocation
	,ModificationDate

	,ModificationTime =
		DATEADD(
			SECOND
			,case
			when isnumeric(ModificationTimeSeconds) = 0 then 0
			when ModificationTimeSeconds IS NULL then 0
			else convert(float, ModificationTimeSeconds)
			end
			,ModificationDate
		)


	,ScheduleDate

	,ScheduleEndTime =
		DATEADD(
			SECOND
			,case
			when isnumeric(ScheduleEndTimeSeconds) = 0 then 0
			when ScheduleEndTimeSeconds IS NULL then 0
			else convert(int, left(RIGHT('0000' + ScheduleEndTimeSeconds, 4), 2)) * 3600 + convert(int, right(RIGHT('0000' + ScheduleEndTimeSeconds, 4), 2)) * 60
			end
			,ScheduleDate
		)

	,ScheduleRoomCode

	,ScheduleStartTime =
		DATEADD(
			SECOND
			,case
			when isnumeric(ScheduleStartTimeSeconds) = 0 then 0
			when ScheduleStartTimeSeconds IS NULL then 0
			else convert(int, left(RIGHT('0000' + ScheduleStartTimeSeconds, 4), 2)) * 3600 + convert(int, right(RIGHT('0000' + ScheduleStartTimeSeconds, 4), 2)) * 60
			end
			,ScheduleDate
		)


	,ModifiedTechCode
	,ScheduleCreateDate

	,ScheduleCreateTime =
		DATEADD(
			SECOND
			,case
			when isnumeric(ScheduleCreateTimeSeconds) = 0 then 0
			when ScheduleCreateTimeSeconds IS NULL then 0
			else convert(float, ScheduleCreateTimeSeconds)
			end
			,ScheduleCreateDate
		)

	,CreateLinkExamCode
	,CreateLinkLocationCode
	,CreateLinkScheduleRoomCode
	,CreateLinkTechCode
	,CreateLinkLocation
	,CreateLinkScheduleDate
	,CreateLinkScheduleRoom
	,CreateLinkTech
	,CancelDate1

	,CancelTime =
		DATEADD(
			SECOND
			,case
			when isnumeric(CancelTimeSeconds) = 0 then 0
			when CancelTimeSeconds IS NULL then 0
			else convert(float, CancelTimeSeconds)
			end
			,CancelDate
		)

	,CancelLinkTech
	,CancelLocationCode
	,CancelTechCode
	,OrderingPhysician
	,ONXQuestionnaireDataCode
	,PerformanceLocation
	,RadiologistCode
	,SeriesNo
	,FilmTechCode
	,FiledDate
	,FiledTime
from
	dbo.TImportMISYSEncounter




