﻿






CREATE view [dbo].[TLoadMSLPIMissingPatient] as
(
--KO 26/03/2014. Copy of TLoadLPIMissingPatient for MSSQL version of LPI
SELECT DISTINCT SourcePatientNo
	--,SourceUniqueID
	,LocalPatientID
	,PatientForename = dbo.ConvertToProperCase(Forename)
	,PatientSurname = dbo.ConvertToProperCase(Surname)
	,NHSNumber
	,NHSNumberStatusCode = 
		case 
			when NNNStatusCode like 'NSTS%'
			then RIGHT(NNNStatusCode, 2)
		end
	,Postcode =
		case
			when len(rtrim(ltrim(replace(PostcodeUnformatted, ' ','')))) = 6 then left(PostcodeUnformatted, 3) + '  ' + right(PostcodeUnformatted, 3)
			when len(rtrim(ltrim(replace(PostcodeUnformatted, ' ','')))) = 7 then left(PostcodeUnformatted, 4) + ' ' + right(PostcodeUnformatted, 3)
			else PostcodeUnformatted
		end
	--,PostcodeUnformatted
	,Sex
	,DOB
	,GPCode
	,PracticeCode
	--,CensusDate 
	,InterfaceCode = 'LPI'
  FROM dbo.TImportMSLPIMissingPatient
  where LocalPatientID is not null
)










