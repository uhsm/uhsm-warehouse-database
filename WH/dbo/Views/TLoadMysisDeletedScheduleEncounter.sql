﻿


CREATE View [dbo].[TLoadMysisDeletedScheduleEncounter] as 

Select
	 [DeletiondateODBC] = [deletion_date_ODBC]
	,[DeletionDescription] = [deletion_description]
	,[DeletionReason] = [deletion_reason]
	,[DeletionTechCode] = [deletion_tech_code]
	,[DeletionTechLocationDescription] = [deletion_tech_loc_desc]
	,[DeletionTime] = [deletion_time_ODBC]
	,[Diagnosis] = [diagnosis]
	,[ExamCodeWomh] = [exam_code_womh]
	,[ExamDescription] = [exam_description]
	,[HospMnemonic] = [hosp_mnemonic]
	,[HospName] = [hosp_name]
	,[LeftRightIndicator] = [lri]
	,[OrderNumber] = [order_number]
	,[PatientHospitalNumber] = [patient_hosp_num]
	,[PatientName] = [patient_name]
	,[PatientNumber] = [patient_number]
	,[PatientNumberWomh] = [pat_number_womh]
	,[PatientType] = [patient_type]
	,[PreAuthCode] = [preauth_code]
	,[SchedulingComment] = [scheduling_comment]
	,[SchedulingPhyiscian] = [scheduling_phy]
	,[SchedulingPhysicianCode] = [scheduling_phys_code]
	,[SchedulingPhysicianName] = [scheduling_phys_name]
	,[SchedulingReason] = [scheduling_reason]
	,[SchedulingTechName] = [scheduling_tech_name]
	,[SegmentDate] = [segment_date_ODBC]
	,SegmentEndTime = 
			Case When isdate(
						Left(RIGHT('0' + segment_end_time,4),2) 
						+ ':'
						+RIGHT(RIGHT('0' + segment_end_time,4),2)
						+ ':00'
						) = 1
			Then 
				CAST(
					segment_date_ODBC +
						CAST(
						Left(RIGHT('0' + segment_end_time,4),2) 
						+ ':'
						+RIGHT(RIGHT('0' + segment_end_time,4),2)
						+ ':00' as time(0))
					as smalldatetime)
			Else
				cast(segment_date_ODBC as smalldatetime)
			End
	,[SegmentEndTimeInt] = [segment_end_time]
	,[SegmentName] = [segment_name]
	,[SegmentRoom] = [segment_room]
	,SegmentStartTime = 
		Case When isdate(
				Left(RIGHT('0' + segment_start_time,4),2) 
				+ ':'
				+RIGHT(RIGHT('0' + segment_start_time,4),2)
				+ ':00'
				) = 1
	Then 
		CAST(
			segment_date_ODBC +
				CAST(
				Left(RIGHT('0' + segment_start_time,4),2) 
				+ ':'
				+RIGHT(RIGHT('0' + segment_start_time,4),2)
				+ ':00' as time(0))
			as smalldatetime)
	Else
		cast(segment_date_ODBC as smalldatetime)
	End
	,[SegmentStartTimeint] = [segment_start_time]
	,[TechCodeScheduled] = [tech_code_scheduled]
	,[TechDeletedName] = [tech_deleted_name]
	,[TechLocation] = [tech_location]
From TImportMisysDeletedSchedule





