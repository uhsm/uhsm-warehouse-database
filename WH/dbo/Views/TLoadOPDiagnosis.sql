﻿create view [dbo].[TLoadOPDiagnosis] as

select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,DiagnosisCode
	,OPSourceUniqueID
from
	dbo.TImportOPDiagnosis
