﻿CREATE view [dbo].[TLoadOPDiary] as

select
	 SourceUniqueID
	,ClinicCode
	,SessionCode
	,SessionDescription

	,SessionDate =
		convert(smalldatetime, right(SessionDate, 2) + '/' + substring(SessionDate, 5, 2) + '/' + left(SessionDate, 4))

	,SessionStartTime =
		convert(smalldatetime, right(SessionDate, 2) + '/' + substring(SessionDate, 5, 2) + '/' + left(SessionDate, 4)  + ' ' + 
			left(SessionTimeRange, 5)
		)

	,SessionEndTime =
		convert(smalldatetime, right(SessionDate, 2) + '/' + substring(SessionDate, 5, 2) + '/' + left(SessionDate, 4)  + ' ' + 
			right(SessionTimeRange, 5)
		)

	,ReasonForCancellation
	,SessionPeriod
	,Units
	,UsedUnits
	,FreeUnits
	,ValidAppointmentTypeCode
	,InterfaceCode
	,DoctorCode
from
	dbo.TImportOPDiary
