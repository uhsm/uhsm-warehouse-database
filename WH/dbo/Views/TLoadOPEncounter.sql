﻿
/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		View TLoadOPEncounter

Notes:			Stored in 
	

Versions:

				1.0.0.1 - 27/01/2016 - CM
					Amended so that EpisodicGPCode and EpisodicGPPracticeCode are populated using PAS.PatientGPHistory  to mirror the way fields are generated for the APC and A&E data
					
				1.0.0.0
					Original view.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/







CREATE  view [dbo].[TLoadOPEncounter] as

select
	 Encounter.SourceUniqueID
	,Encounter.SourcePatientNo
	,Encounter.SourceEncounterNo
	,Encounter.ReferralSourceUniqueID
	,Encounter.WaitingListSourceUniqueID
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath
	,Encounter.SexCode
	,Encounter.NHSNumber
	,Encounter.NHSNumberStatusCode

	,Encounter.DistrictNo

	,Postcode =
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end

	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.DHACode
	,Encounter.EthnicOriginCode
	,Encounter.MaritalStatusCode
	,Encounter.ReligionCode
	,Encounter.RegisteredGpCode
	,Encounter.RegisteredGpPracticeCode
	,Encounter.SiteCode
	,Encounter.AppointmentDate
	,Encounter.AppointmentTime
	,Encounter.ClinicCode
	,Encounter.AdminCategoryCode
	,Encounter.SourceOfReferralCode
	,Encounter.ReasonForReferralCode
	,Encounter.PriorityCode
	,Encounter.FirstAttendanceFlag
	,Encounter.AppointmentStatusCode
	,Encounter.CancelledByCode
	,Encounter.TransportRequiredFlag
	,Encounter.AppointmentTypeCode
	,Encounter.DisposalCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.ReferringConsultantCode
	,Encounter.ReferringSpecialtyCode
	,Encounter.BookingTypeCode
	,Encounter.CasenoteNo
	,Encounter.AppointmentCreateDate
	,EpisodicGpCode = PracticeHistory.GPRefno --CM 27/01/2016 amended
	,EpisodicGpPracticeCode = PracticeHistory.PracticeRefno--CM 27/01/2016 amended
	,Encounter.DoctorCode
	,Encounter.PrimaryDiagnosisCode
	,Encounter.SubsidiaryDiagnosisCode
	,Encounter.SecondaryDiagnosisCode1
	,Encounter.SecondaryDiagnosisCode2
	,Encounter.SecondaryDiagnosisCode3
	,Encounter.SecondaryDiagnosisCode4
	,Encounter.SecondaryDiagnosisCode5
	,Encounter.SecondaryDiagnosisCode6
	,Encounter.SecondaryDiagnosisCode7
	,Encounter.SecondaryDiagnosisCode8
	,Encounter.SecondaryDiagnosisCode9
	,Encounter.SecondaryDiagnosisCode10
	,Encounter.SecondaryDiagnosisCode11
	,Encounter.SecondaryDiagnosisCode12
	,Encounter.PrimaryOperationCode
	,Encounter.PrimaryOperationDate
	,Encounter.SecondaryOperationCode1
	,Encounter.SecondaryOperationDate1
	,Encounter.SecondaryOperationCode2
	,Encounter.SecondaryOperationDate2
	,Encounter.SecondaryOperationCode3
	,Encounter.SecondaryOperationDate3
	,Encounter.SecondaryOperationCode4
	,Encounter.SecondaryOperationDate4
	,Encounter.SecondaryOperationCode5
	,Encounter.SecondaryOperationDate5
	,Encounter.SecondaryOperationCode6
	,Encounter.SecondaryOperationDate6
	,Encounter.SecondaryOperationCode7
	,Encounter.SecondaryOperationDate7
	,Encounter.SecondaryOperationCode8
	,Encounter.SecondaryOperationDate8
	,Encounter.SecondaryOperationCode9
	,Encounter.SecondaryOperationDate9
	,Encounter.SecondaryOperationCode10
	,Encounter.SecondaryOperationDate10
	,Encounter.SecondaryOperationCode11
	,Encounter.SecondaryOperationDate11
	,Encounter.PurchaserCode
	,Encounter.ProviderCode
	,Encounter.ContractSerialNo
	,Encounter.ReferralDate
	,Encounter.RTTPathwayID
	,Encounter.RTTPathwayCondition
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.RTTSpecialtyCode
	,Encounter.RTTCurrentProviderCode
	,Encounter.RTTCurrentStatusCode
	,Encounter.RTTCurrentStatusDate
	,Encounter.RTTCurrentPrivatePatientFlag
	,Encounter.RTTOverseasStatusFlag
	,Encounter.RTTPeriodStatusCode
	,Encounter.AppointmentCategoryCode
	,Encounter.AppointmentCreatedBy
	,Encounter.AppointmentCancelDate
	,Encounter.LastRevisedDate
	,Encounter.LastRevisedBy
	,Encounter.OverseasStatusFlag
	,Encounter.OverseasStatusStartDate
	,Encounter.OverseasStatusEndDate
	,Encounter.PatientChoiceCode
	,Encounter.ScheduledCancelReasonCode
	,Encounter.PatientCancelReason
	,Encounter.DischargeDate
	,Encounter.QM08StartWaitDate
	,Encounter.QM08EndWaitDate
	,Encounter.DestinationSiteCode
	,Encounter.EBookingReferenceNo
	,Encounter.InterfaceCode
	,Encounter.IsWardAttender
	,Encounter.PASCreated
	,Encounter.PASUpdated
	,Encounter.PASCreatedByWhom
	,Encounter.PASUpdatedByWhom

	,PCTCode = null
--		coalesce(
--			 Practice.ParentOrganisationCode
--			,Postcode.PCTCode
--			,'X98'
--		)

	,LocalityCode = null
--	,LocalityCode = LocalAuthority.LocalAuthorityCode

--KO changed 19/02/2013
	,DNACode =
			case 
				when Encounter.AppointmentStatusCode = 45 then '0'   --Not Specified
				when Encounter.AppointmentStatusCode = 2870 then '2'   --Cancelled By Patient
				when Encounter.AppointmentStatusCode = 2004301 then '2'   --Cancelled by Patient
				when Encounter.AppointmentStatusCode = 5477 then '2'   --Patient/Carer
				when Encounter.AppointmentStatusCode = 358 then '3'   --Did Not Attend
				when Encounter.AppointmentStatusCode = 2003494 then '3'   --Patient Left / Not seen
				when Encounter.AppointmentStatusCode = 2003495 then '3'   --Refused To Wait
				when Encounter.AppointmentStatusCode = 2004300 then '4'   --Cancelled by Hospital
				when Encounter.AppointmentStatusCode = 357 then '5'   --Attended On Time
				when Encounter.AppointmentStatusCode = 2004151 then '5'   --Attended
				when Encounter.AppointmentStatusCode = 2868 then '6'   --Patient Late / Seen
				when Encounter.AppointmentStatusCode = 2000724 then '7'   --Patient Late / Not Seen
				--when Encounter.AppointmentStatusCode = 0 then ''   --NULL
				when Encounter.AppointmentStatusCode in ( --Ambiguous
					2003532 --Cancelled
					,2004528 --Cancelled by EBS
					,3006508 --Referral Cancelled
					) 
				then
					case 
						when CancelledReasonLu.MappedCode = 'P' 
						then '2' 
						else '4'
					end
				when Encounter.AppointmentStatusCode = 2003534 --Unable to Attend, probably deceased
				then
					case 
						when Encounter.CancelledByCode = 3107255 --deceased
						then '2'
						when CancelledReasonLu.MappedCode = 'H' 
						then '4' 
						else '2'
					end
				else '0'
			end
		,AttendanceOutcomeCode = Encounter.AppointmentStatusCode--not used
			--case
			--	when coalesce(Encounter.AppointmentStatusCode, 45) = 45
			--	then
			--		case
			--			when Encounter.CancelledByCode is null
			--			then Encounter.AppointmentStatusCode --KO!!only if not null??
			--			else 
			--				case 
			--					when Encounter.CancelledByCode = 5477 --Patient/Carer
			--					then 2004301 --Cancelled by Patient
			--					when Encounter.CancelledByCode  = 3647 --Hospital/Trust
			--					then 2004300 --Cancelled by Hospital
			--					when Encounter.CancelledByCode  = 2002060 --C&B
			--					then 2004528 --Cancelled by EBS
			--					else coalesce(Encounter.AppointmentStatusCode, 45)
			--				end
			--		end
			--	else coalesce(Encounter.AppointmentStatusCode, 45)
			--end
	--,DNACode =
	--	case 
	--		when Encounter.AppointmentStatusCode = 45 then '0'   --Not Specified
	--		when Encounter.AppointmentStatusCode = 2870 then '2'   --Cancelled By Patient
	--		when Encounter.AppointmentStatusCode = 2004301 then '2'   --Cancelled by Patient
	--		when Encounter.AppointmentStatusCode = 358 then '3'   --Did Not Attend
	--		when Encounter.AppointmentStatusCode = 2003494 then '3'   --Patient Left / Not seen
	--		when Encounter.AppointmentStatusCode = 2003495 then '3'   --Refused To Wait
	--		when Encounter.AppointmentStatusCode = 2004300 then '4'   --Cancelled by Hospital
	--		when Encounter.AppointmentStatusCode = 357 then '5'   --Attended On Time
	--		when Encounter.AppointmentStatusCode = 2004151 then '5'   --Attended
	--		when Encounter.AppointmentStatusCode = 2868 then '6'   --Patient Late / Seen
	--		when Encounter.AppointmentStatusCode = 2000724 then '7'   --Patient Late / Not Seen
	--		--when Encounter.AppointmentStatusCode = 0 then ''   --NULL
	--		when Encounter.AppointmentStatusCode in (
	--			2003532 --Cancelled
	--			,2004528 --Cancelled by EBS
	--			,3006508 --Referral Cancelled
	--			) then
	--			case when CancelledReasonLu.MappedCode = 'P' then '2' 
	--				else '4'
	--			end
	--		when Encounter.AppointmentStatusCode = 2003534 --Unable to Attend
	--			then
	--			case when CancelledReasonLu.MappedCode = 'H' then '4' 
	--				else '2'
	--			end
	--		else '9'
	--	end
	--,AttendanceOutcomeCode =
	--case
	--when coalesce(Encounter.AppointmentStatusCode, 45) = 45
	--then
	--	case
	--	when Encounter.CancelledByCode is null
	--	then Encounter.AppointmentStatusCode
	--	else Encounter.CancelledByCode
	--	end
	--else coalesce(Encounter.AppointmentStatusCode, 45)
	--end

	,Encounter.ArchiveFlag
	,Encounter.ClinicType
	,ExcludedClinic = 
	case
	when ExcludedClinic.EntityCode is Null
	then
		convert(Bit,0)
	Else
		convert(Bit,1)
	End
	,Encounter.SessionUniqueID
	/*
		CONVERT(bit, 
				Case 
				When Exists 
						(
						Select 1 
						from dbo.EntityLookUp Entity
						Where 
						Entity.EntityTypeCode = 'EXCLUDEDCLINIC'
						and Encounter.SiteCode = Entity.EntityCode
						)
				Then 1
				Else 0
				End
				)
*/				
	,IsWalkIn 	=
			Case When FirstAttendance.FirstAttendanceType = 'Walk-in' Then 
				1
			Else
				0
			End
			
from
	dbo.TImportOPEncounter Encounter
left join 
	(
	Select * 
	From
	EntityLookup 
	Where EntityTypeCode = 'EXCLUDEDCLINIC'
	) ExcludedClinic
	on Encounter.SiteCode = ExcludedClinic.EntityCode
left join PAS.FirstAttendance FirstAttendance
	on Encounter.FirstAttendanceFlag = FirstAttendance.FirstAttendanceCode
left join PTL.OPLookups CancelledReasonLu 
      on Encounter.ScheduledCancelReasonCode = CancelledReasonLu.ReferenceValueCode
left join PAS.PatientGPHistory PracticeHistory
			on PracticeHistory.DistrictNo = Encounter.DistrictNo
			and Encounter.AppointmentDate >= PracticeHistory.StartDate
			and (Encounter.AppointmentDate < PracticeHistory.EndDate Or PracticeHistory.EndDate Is Null)

	

--LEFT JOIN PAS.Gp EpisodicGp 
--ON	Encounter.EpisodicGpCode = EpisodicGp.GpCode
--
--LEFT JOIN PAS.Gp RegisteredGp 
--ON	Encounter.RegisteredGpCode = RegisteredGp.GpCode
--
--left join Organisation.dbo.Practice Practice
--on	Practice.OrganisationCode = coalesce(EpisodicGp.PracticeCode, RegisteredGp.PracticeCode)
--
--left join Organisation.dbo.Postcode Postcode
--on	Postcode.Postcode =
--		case
--		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
--		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
--		else Encounter.Postcode
--		end
--
--left join Organisation.dbo.LocalAuthority LocalAuthority
--on	LocalAuthority.LocalAuthorityCode = Postcode.LocalAuthorityCode












