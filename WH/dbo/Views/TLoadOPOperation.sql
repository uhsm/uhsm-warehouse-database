﻿CREATE view [dbo].[TLoadOPOperation] as

select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,OperationCode
	,OperationDate
	,DoctorCode
	,OPSourceUniqueID
from
	dbo.TImportOPOperation
