﻿Create View dbo.TLoadOPRule AS
Select
	 [RuleUniqueID]
	,[AppliedTo]
	,[AppliedToUniqueID]
	,[EnforcedFlag]
	,[RuleAppliedUniqueID]
	,[RuleValueCode]
	,[ArchiveFlag]
	,[PASCreated]
	,[PASUpdated]
	,[PASCreatedByWhom]
	,[PASUpdatedByWhom]
From dbo.TImportOPRule