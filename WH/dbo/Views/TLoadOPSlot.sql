﻿
create view [dbo].[TLoadOPSlot] as

select
	Encounter.SlotUniqueID
	,Encounter.SessionUniqueID
	,Encounter.ServicePointUniqueID
	,Encounter.SlotDate
	,Encounter.SlotStartTime
	,Encounter.SlotStartDateTime
	,Encounter.SlotEndTime
	,Encounter.SlotEndDateTime
	,Encounter.SlotDuration
	,Encounter.SlotStatusCode
	,Encounter.SlotChangeReasonCode
	,Encounter.SlotChangeRequestByCode
	,Encounter.SlotPublishedToEBS
	,Encounter.SlotMaxBookings
	,Encounter.SlotNumberofBookings
	,Encounter.SlotTemplateUnqiueID
	,Encounter.IsTemplate
	,Encounter.SlotCancelledFlag
	,Encounter.SlotCancelledDate
	,Encounter.SlotInstructions
	,Encounter.ArchiveFlag
	,Encounter.PASCreated
	,Encounter.PASUpdated
	,Encounter.PASCreatedByWhom
	,Encounter.PASUpdatedByWhom

from
	dbo.TImportOPSlot Encounter


