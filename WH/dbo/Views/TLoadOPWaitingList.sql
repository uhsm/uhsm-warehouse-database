﻿




CREATE  view [dbo].[TLoadOPWaitingList] as

select
	 Encounter.CensusDate
	,Encounter.SourceUniqueID
	,Encounter.SourceUniqueIDTypeCode
	,Encounter.SourcePatientNo
	,Encounter.ReferralSourceUniqueID
	,Encounter.AppointmentSourceUniqueID
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath
	,Encounter.SexCode
	,Encounter.NHSNumber
	,Encounter.DistrictNo

	,Postcode =
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end

	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.DHACode
	,Encounter.HomePhone
	,Encounter.WorkPhone
	,Encounter.EthnicOriginCode
	,Encounter.MaritalStatusCode
	,Encounter.ReligionCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.PASSpecialtyCode
	,Encounter.PriorityCode
	,Encounter.WaitingListCode
	,Encounter.CommentClinical
	,Encounter.CommentNonClinical
	,Encounter.SiteCode
	,Encounter.PurchaserCode
	,Encounter.ProviderCode
	,Encounter.ContractSerialNo
	,Encounter.AdminCategoryCode
	,Encounter.CancelledBy
	,Encounter.DoctorCode
	,Encounter.BookingTypeCode
	,Encounter.CasenoteNumber
	,Encounter.InterfaceCode
	,Encounter.RegisteredGpCode
	,Encounter.RegisteredGpPracticeCode
	,Encounter.EpisodicGpCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.SourceTreatmentFunctionCode
	,Encounter.TreatmentFunctionCode
	,Encounter.NationalSpecialtyCode

	,PCTCode =
		coalesce(
			 PCT.OrganisationLocalCode
			,Postcode.PCTCode
			,'X98'
		)

	,PCTCCGCode =
		case when Encounter.DateOnWaitingList > '2013-03-31 00:00:00' then
			coalesce(
				 ODSPractice.ParentOrganisationCode
				,PostcodeCCG.CCGCode
				,'X98'
			)
		else 
			coalesce(
			 PCT.OrganisationLocalCode
			,Postcode.PCTCode
			,'X98'
		)
		end
		
	,Encounter.ReferralDate
	,Encounter.BookedDate
	,Encounter.BookedTime
	,Encounter.AppointmentDate
	,Encounter.AppointmentTypeCode
	,Encounter.AppointmentStatusCode
	,Encounter.AppointmentCategoryCode
	,Encounter.QM08StartWaitDate
	,Encounter.QM08EndWaitDate
	,Encounter.AppointmentTime
	,Encounter.ClinicCode
	,Encounter.SourceOfReferralCode
	,Encounter.MRSAFlag
	,Encounter.RTTPathwayID
	,Encounter.RTTPathwayCondition
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.RTTSpecialtyCode
	,Encounter.RTTCurrentProviderCode
	,Encounter.RTTCurrentStatusCode
	,Encounter.RTTCurrentStatusDate
	,Encounter.RTTCurrentPrivatePatientFlag
	,Encounter.RTTOverseasStatusFlag
	,Encounter.PASCreated
	,Encounter.PASUpdated
	,Encounter.PASCreatedByWhom
	,Encounter.PASUpdatedByWhom
	,Encounter.ArchiveFlag

	,KornerWait =
		datediff(
			 day
			,Encounter.QM08StartWaitDate
			,Encounter.CensusDate
		)

-- Set the BreachDate to CensusDate + (breach days - Days waiting)
	,BreachDate =
		dateadd(day,
			coalesce(
				convert(int, ConsultantBreachDays.Description), 
				convert(int, SpecialtyBreachDays.Description), 
				(select NumericValue from dbo.Parameter where Parameter = 'OPDEFAULTBREACHDAYS')
			)
			-	
			datediff(
				 day
				,Encounter.QM08StartWaitDate
				,Encounter.CensusDate
			)
			,Encounter.CensusDate
		)

-- Set the BreachDate to CensusDate + (breach days - Days waiting)
	,NationalBreachDate =
		dateadd(day,
			coalesce(
				convert(int, NationalConsultantBreachDays.Description), 
				convert(int, NationalSpecialtyBreachDays.Description), 
				(select NumericValue from dbo.Parameter where Parameter = 'NATIONALOPDEFAULTBREACHDAYS')
			)
			-	
			datediff(
				 day
				,Encounter.QM08StartWaitDate
				,Encounter.CensusDate
			)
			,Encounter.CensusDate
		)

	,BreachDays =
		coalesce(
			 convert(int, ConsultantBreachDays.Description)
			,convert(int, SpecialtyBreachDays.Description)
			,(select NumericValue from dbo.Parameter where Parameter = 'OPDEFAULTBREACHDAYS')
		)

	,NationalBreachDays =
		coalesce(
			 convert(int, ConsultantBreachDays.Description)
			,convert(int, SpecialtyBreachDays.Description)
			,(select NumericValue from dbo.Parameter where Parameter = 'NATIONALOPDEFAULTBREACHDAYS')
		)

	,Encounter.FuturePatientCancelDate
	,Encounter.AdditionFlag
	,Encounter.CountOfDNAs
	,Encounter.CountOfHospitalCancels
	,Encounter.CountOfPatientCancels
	,Encounter.DateOnWaitingList
	,Encounter.IntendedPrimaryOperationCode
	,Encounter.Operation
	,Encounter.WaitingListRule
	,Encounter.VisitCode
	,Encounter.InviteDate
	,Encounter.WaitingListClinicCode
	,Encounter.GeneralComment

from
	dbo.TImportOPWaitingList Encounter

left join PAS.Organisation IPMPractice
on	IPMPractice.OrganisationCode = Encounter.RegisteredGpPracticeCode

left join PAS.Organisation PCT
on	PCT.OrganisationCode = IPMPractice.ParentOrganisationCode

left join Organisation.dbo.Postcode Postcode
on	Postcode.Postcode =
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end

left join OrganisationCCG.dbo.Postcode PostcodeCCG
on	PostcodeCCG.Postcode =
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end

left join OrganisationCCG.dbo.Practice ODSPractice
	on ODSPractice.OrganisationCode = IPMPractice.OrganisationLocalCode
	
--left join Organisation.dbo.LocalAuthority LocalAuthority
--on	LocalAuthority.LocalAuthorityCode = Postcode.LocalAuthorityCode

left join dbo.EntityLookup ConsultantBreachDays
on	ConsultantBreachDays.EntityTypeCode = 'OPCONSULTANTBREACHDAYS'
and	ConsultantBreachDays.EntityCode = Encounter.ConsultantCode

left join dbo.EntityLookup SpecialtyBreachDays
on	SpecialtyBreachDays.EntityTypeCode = 'OPSPECIALTYBREACHDAYS'
and	SpecialtyBreachDays.EntityCode = Encounter.SpecialtyCode

left join dbo.EntityLookup NationalConsultantBreachDays
on	NationalConsultantBreachDays.EntityTypeCode = 'NATIONALOPCONSULTANTBREACHDAYS'
and	NationalConsultantBreachDays.EntityCode = Encounter.ConsultantCode

left join dbo.EntityLookup NationalSpecialtyBreachDays
on	NationalSpecialtyBreachDays.EntityTypeCode = 'NATIONALOPSPECIALTYBREACHDAYS'
and	NationalSpecialtyBreachDays.EntityCode = Encounter.SpecialtyCode





