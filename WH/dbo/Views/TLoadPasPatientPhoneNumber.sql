﻿Create View TLoadPasPatientPhoneNumber as 


Select
	 SourcePatientNo = PAR.PATNT_REFNO
	,PhoneNumber = PA.LINE1

From Lorenzo.dbo.PatientAddressRole PAR
Inner Join Lorenzo.dbo.PatientAddress PA 
	on PAR.ADDSS_REFNO = PA.ADDSS_REFNO
Where
	PAR.ARCHV_FLAG = 'N'
	and PA.ARCHV_FLAG = 'N'
	and PAR.ROTYP_CODE = 'HOME'
	and PAR.CURNT_FLAG = 'Y'
	and PA.ADTYP_CODE = 'PHONE'
	and Not Exists
		(
		Select 1 
		From Lorenzo.dbo.PatientAddressRole LastPAR
		Inner Join Lorenzo.dbo.PatientAddress LastPA 
			on LastPAR.ADDSS_REFNO = LastPA.ADDSS_REFNO
		Where
			LastPAR.ARCHV_FLAG = 'N'
			and LastPA.ARCHV_FLAG = 'N'
			and LastPAR.ROTYP_CODE = 'HOME'
			and LastPAR.CURNT_FLAG = 'Y'
			and LastPA.ADTYP_CODE = 'PHONE'
			and LastPAR.PATNT_REFNO = PAR.PATNT_REFNO
			and LastPAR.ROLES_REFNO > PAR.ROLES_REFNO
		)



