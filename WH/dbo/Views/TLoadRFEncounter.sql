﻿











CREATE  view [dbo].[TLoadRFEncounter] as

select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,NHSNumberStatusCode
	,DistrictNo

	,Postcode =
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end

	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,EpisodicGdpCode
	,SiteCode
	,ConsultantCode
	,SpecialtyCode
	,SourceOfReferralCode
	,PriorityCode
	,ReferralDate
	,DischargeDate = dateadd(day, datediff(day, 0, DischargeDate), 0)
	,DischargeTime = DischargeDate
	,DischargeReasonCode
	,DischargeReason
	,AdminCategoryCode
	,ContractSerialNo
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NextFutureAppointmentDate
	,InterfaceCode

	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,ArchiveFlag

	,PCTCode = null
--		coalesce(
--			 Practice.ParentOrganisationCode
--			,Postcode.PCTCode
--			,'X98'
--		)

	,LocalityCode = null
--	,LocalityCode = LocalAuthority.LocalAuthorityCode

	,OriginalProviderReferralDate

	,ParentSourceUniqueID
	,RequestedService
	,ReferralStatus
	,CancellationDate
	,CancellationReason
	,ReferralMedium
	,ReferredByConsultantCode 
	,ReferredByOrganisationCode
	,ReferredBySpecialtyCode
	,SuspectedCancerTypeCode
	,ReferralSentDate
	,PatientDeceased
	,GeneralComment
	,ReferralComment
from
	dbo.TImportRFEncounter Encounter

--left join PAS.Gp RegisteredGp 
--ON	Encounter.RegisteredGpCode = RegisteredGp.GpCode
--
--left join Organisation.dbo.Practice Practice
--on	Practice.OrganisationCode = RegisteredGp.PracticeCode
--
--left join Organisation.dbo.Postcode Postcode
--on	Postcode.Postcode =
--		case
--		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
--		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
--		else Encounter.Postcode
--		end
--
--left join Organisation.dbo.LocalAuthority LocalAuthority
--on	LocalAuthority.LocalAuthorityCode = Postcode.LocalAuthorityCode












