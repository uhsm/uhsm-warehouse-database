﻿CREATE view [dbo].[TLoadTheatreHistory]
as

select
	 ID

	,AmendTime =
		cast(cast(AmendDate as varchar(11)) + ' ' + right(convert(varchar, AmendTime, 113), 12) as smalldatetime)

	,AmendUser
	,Anaesthetist
	,CancelReason
	,Consultant

	,EndTime =
		cast(cast(SessionDate as varchar(11)) + ' ' + right(convert(varchar, EndTime, 113), 12) as smalldatetime)

	,ReAssignReason
	,SessionDate
	,SessionType

	,StartTime =
		cast(cast(SessionDate as varchar(11)) + ' ' + replace(StartTime, '/', '0') as smalldatetime)

	,Theatre
	,InterfaceCode

	,SessionID =
		Theatre + '||' + convert(varchar, datediff(day, '31 dec 1840', SessionDate)) + '||' + convert(varchar, convert(int, left(replace(right('0' + convert(varchar, convert(smalldatetime, left(convert(varchar, SessionDate, 113), 12) + StartTime), 108), 8), ':', ''), 4)))

from
	dbo.TImportTheatreHistory
