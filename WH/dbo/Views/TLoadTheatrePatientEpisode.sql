﻿CREATE view [dbo].[TLoadTheatrePatientEpisode] as

SELECT
	SourceUniqueID,
	ASACode,
	ATO,
	ATORelieved,
	ActualOperationLine1,
	ActualOperationLine2,
	ActualOperationLine3,
	ActualOperationLine4,
	AnaestheticComments,
	AnaestheticNurse1,
	AnaestheticNurse1Relieved,
	AnaestheticNurse2,
	AnaestheticNurse2Relieved,
	AnaestheticType1,
	AnaestheticType2,
	AnaestheticType3,
	AnaestheticType4,
	Anaesthetist1,
	Anaesthetist2,
	Anaesthetist3,

	CancelReason = convert(varchar, convert(int, CancelReason)),

	case
	when CEPOD is null then
		case
		when TheatreNumber = '0' then '1' --emergency
		else '4' -- elective
		end
	else CEPOD
	end CEPOD,

	CirculatingNurse,
	CirculatingNurseRelieved,
	DestinationFromRecovery,
	Episode,
	EscortName,
	EscortNurse,
	EscortNurseRelieved,
	ExpectedLengthOfOperation,
	FirstAssistant,	
	IntendedManagement,
	InternalDistrictNumber,
	OtherNurse,
	OtherNurseRelieved,
	Outcome,
	PatientAccompanied,
	PatientLocation,
	PlannedAnaesthetic,
	PortersActivityReception,
	ProcedureDate,

	dateadd(minute, 
		case charindex('.', cast([ProcedureTime] as varchar(7)))
		when 4 then cast(left(cast([ProcedureTime] as varchar(7)), 1) as int) * 60 + cast(substring(cast([ProcedureTime] as varchar(7)), 2, 2) as int)
		when 5 then cast(left(cast([ProcedureTime] as varchar(7)), 2) as int) * 60 + cast(substring(cast([ProcedureTime] as varchar(7)), 3, 2) as int)
		else 0
		end
		,ProcedureDate
	) ProcedureTime,

	ProposedOperationLine1,
	ProposedOperationLine2,
	ProposedOperationLine3,
	ProposedOperationLine4,
	ReasonDelayReception1,
	ReasonDelayReception2,
	ReasonDelayRecovery1,
	ReasonDelayRecovery2,
	ReceptionComments,
	ReceptionNurse1,
	ReceptionNurse2,
	ReceptionPorter,
	RecoveryComments,
	RecoveryPorter,
	RevoveryNurse,
	RevoveryNurseRelieved,
	ScrubNurse1,
	ScrubNurse1Relieved,
	ScrubNurse2,
	ScrubNurse2Relieved,
	SpecialEquipment1,
	SpecialEquipment2,
	SpecialEquipment3,
	SpecialEquipment4,
	Surgeon1,
	Surgeon2,
	Surgeon3,
	TheatreComments,
	TheatreNumber,
	TimeSentFor,
	dateadd(day, case when NewDay < 2 then 1 else 0 end, TimeArrived) TimeArrived,
	dateadd(day, case when NewDay < 3 then 1 else 0 end, TimeInAnaestheticRoom) TimeInAnaestheticRoom,
	dateadd(day, case when NewDay < 4 then 1 else 0 end, TimeAnaestheticStart) TimeAnaestheticStart,
	dateadd(day, case when NewDay < 5 then 1 else 0 end, TimeOutAnaestheticRoom) TimeOutAnaestheticRoom,
	dateadd(day, case when NewDay < 6 then 1 else 0 end, TimeOnTable) TimeOnTable,
	dateadd(day, case when NewDay < 7 then 1 else 0 end, TimeOffTable) TimeOffTable,
	dateadd(day, case when NewDay < 8 then 1 else 0 end, TimeInRecovery) TimeInRecovery,
	dateadd(day, case when NewDay < 9 then 1 else 0 end, TimeReadyForDischarge) TimeReadyForDischarge,
	dateadd(day, case when NewDay < 10 then 1 else 0 end, TimeOutRecovery) TimeOutRecovery,

	null TimeAwake,

/* order of events...
 TimeSentFor
,TimeArrived
,TimeInAnaestheticRoom
,TimeAnaestheticStart
,TimeOutAnaestheticRoom
,TimeOnTable
,TimeOffTable
,TimeInRecovery
,TimeReadyForDischarge
,TimeOutRecovery
,
*/
	WardCode,
	SessionID,
	SourcePatientNo,
	InterfaceCode
FROM
	(
	select
		*,
	
		case
		when TimeArrived < TimeSentFor and datediff(minute, TimeSentFor, TimeArrived) * -1  > MaxSessionMinutes.NumericValue then 1
		when TimeInAnaestheticRoom < TimeArrived and datediff(minute, TimeArrived, TimeInAnaestheticRoom) * -1  > MaxSessionMinutes.NumericValue then 2
		when TimeAnaestheticStart < TimeInAnaestheticRoom and datediff(minute, TimeInAnaestheticRoom, TimeAnaestheticStart) * -1  > MaxSessionMinutes.NumericValue then 3
		when TimeOutAnaestheticRoom < TimeAnaestheticStart and datediff(minute, TimeAnaestheticStart, TimeOutAnaestheticRoom) * -1  > MaxSessionMinutes.NumericValue then 4
		when TimeOnTable < TimeOutAnaestheticRoom and datediff(minute, TimeOutAnaestheticRoom, TimeOnTable) * -1  > MaxSessionMinutes.NumericValue then 5
		when TimeOffTable < TimeOnTable and datediff(minute, TimeOnTable, TimeOffTable) * -1 > MaxSessionMinutes.NumericValue then 6
		when TimeInRecovery < TimeOffTable and datediff(minute, TimeOffTable, TimeInRecovery) * -1  > MaxSessionMinutes.NumericValue then 7
		when TimeReadyForDischarge < TimeInRecovery and datediff(minute, TimeInRecovery, TimeReadyForDischarge) * -1  > MaxSessionMinutes.NumericValue then 8
		when TimeOutRecovery < TimeReadyForDischarge and datediff(minute, TimeReadyForDischarge, TimeOutRecovery) * -1  > MaxSessionMinutes.NumericValue then 9
		else null
		end NewDay
	FROM
		(
		select
			SourceUniqueID, [ASACode], [ATO], [ATORelieved], [ActualOperationLine1], [ActualOperationLine2], [ActualOperationLine3], [ActualOperationLine4], [AnaestheticComments], [AnaestheticNurse1], [AnaestheticNurse1Relieved], [AnaestheticNurse2], [AnaestheticNurse2Relieved], [AnaestheticType1], [AnaestheticType2], [AnaestheticType3], [AnaestheticType4], [Anaesthetist1], [Anaesthetist2], [Anaesthetist3], [CancelReason], [CEPOD], [CirculatingNurse], [CirculatingNurseRelieved], [DestinationFromRecovery], [Episode], [EscortName], [EscortNurse], [EscortNurseRelieved], [ExpectedLengthOfOperation], [FirstAssistant], [IntendedManagement], [InternalDistrictNumber], [OtherNurse], [OtherNurseRelieved], [Outcome], [PatientAccompanied], [PatientLocation], [PlannedAnaesthetic], [PortersActivityReception], [ProcedureDate], [ProcedureTime], [ProposedOperationLine1], [ProposedOperationLine2], [ProposedOperationLine3], [ProposedOperationLine4], [ReasonDelayReception1], [ReasonDelayReception2], [ReasonDelayRecovery1], [ReasonDelayRecovery2], [ReceptionComments], [ReceptionNurse1], [ReceptionNurse2], [ReceptionPorter], [RecoveryComments], [RecoveryPorter], [RevoveryNurse], [RevoveryNurseRelieved], [ScrubNurse1], [ScrubNurse1Relieved], [ScrubNurse2], [ScrubNurse2Relieved], [SpecialEquipment1], [SpecialEquipment2], [SpecialEquipment3], [SpecialEquipment4], [Surgeon1], [Surgeon2], [Surgeon3], [TheatreComments], [TheatreNumber], [WardCode], [SessionID], [SourcePatientNo],
			dateadd(minute, left(TimeSentFor, 2) * 60 + right(TimeSentFor, 2), ProcedureDate) TimeSentFor,
			dateadd(minute, left(TimeArrived, 2) * 60 + right(TimeArrived, 2), ProcedureDate) TimeArrived,
			dateadd(minute, left(TimeInAnaestheticRoom, 2) * 60 + right(TimeInAnaestheticRoom, 2), ProcedureDate) TimeInAnaestheticRoom,
			dateadd(minute, left(TimeAnaestheticStart, 2) * 60 + right(TimeAnaestheticStart, 2), ProcedureDate) TimeAnaestheticStart,
			dateadd(minute, left(TimeOutAnaestheticRoom, 2) * 60 + right(TimeOutAnaestheticRoom, 2), ProcedureDate) TimeOutAnaestheticRoom,
			dateadd(minute, left(TimeOnTable, 2) * 60 + right(TimeOnTable, 2), ProcedureDate) TimeOnTable,
			dateadd(minute, left(TimeOffTable, 2) * 60 + right(TimeOffTable, 2), ProcedureDate) TimeOffTable,
			dateadd(minute, left(TimeInRecovery, 2) * 60 + right(TimeInRecovery, 2), ProcedureDate) TimeInRecovery,
			dateadd(minute, left(TimeReadyForDischarge, 2) * 60 + right(TimeReadyForDischarge, 2), ProcedureDate) TimeReadyForDischarge,
			dateadd(minute, left(TimeOutRecovery, 2) * 60 + right(TimeOutRecovery, 2), ProcedureDate) TimeOutRecovery
			,InterfaceCode
		from
			dbo.TImportTheatrePatientEpisode
		) Session

	inner join dbo.Parameter MaxSessionMinutes
	on	MaxSessionMinutes.Parameter = 'MAXSESSIONMINUTES'
	) Session
