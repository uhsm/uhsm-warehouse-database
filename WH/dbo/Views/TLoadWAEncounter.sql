﻿CREATE  view [dbo].[TLoadWAEncounter] as

select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo

	,Postcode =
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end

	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode

	,RegisteredGpPracticeCode =
		RegisteredGp.PracticeCode

	,SiteCode
	,AppointmentDate
	,AppointmentTime
	,ClinicCode

	,AdminCategoryCode =
	case
	when Encounter.AdminCategoryCode = 'NHS' then '01'
	when Encounter.AdminCategoryCode = 'PAY' then '02'
	when Encounter.AdminCategoryCode = 'IND' then '02'
	when Encounter.AdminCategoryCode = 'OV' then '02'
	when Encounter.AdminCategoryCode = 'AME' then '03'
	when Encounter.AdminCategoryCode = 'CAT' then '04'
	else '01'
	end

	,SourceOfReferralCode
	,ReasonForReferralCode
	,PriorityCode

	,FirstAttendanceFlag =
		case
		when Encounter.FirstAttendanceFlag = '0'
		then '2'
		else '1'
		end

	,AppointmentStatusCode
	,CancelledByCode
	,TransportRequiredFlag
	,AppointmentTypeCode
	,DisposalCode
	,ConsultantCode
	,SpecialtyCode
	,ReferringConsultantCode
	,ReferringSpecialtyCode
	,BookingTypeCode
	,CasenoteNo
	,AppointmentCreateDate
	,EpisodicGpCode

	,EpisodicGpPracticeCode =
		RegisteredGp.PracticeCode

	,DoctorCode
	,Encounter.PrimaryDiagnosisCode
	,Encounter.SubsidiaryDiagnosisCode
	,Encounter.SecondaryDiagnosisCode1
	,Encounter.SecondaryDiagnosisCode2
	,Encounter.SecondaryDiagnosisCode3
	,Encounter.SecondaryDiagnosisCode4
	,Encounter.SecondaryDiagnosisCode5
	,Encounter.SecondaryDiagnosisCode6
	,Encounter.SecondaryDiagnosisCode7
	,Encounter.SecondaryDiagnosisCode8
	,Encounter.SecondaryDiagnosisCode9
	,Encounter.SecondaryDiagnosisCode10
	,Encounter.SecondaryDiagnosisCode11
	,Encounter.SecondaryDiagnosisCode12
	,Encounter.PrimaryOperationCode
	,Encounter.PrimaryOperationDate
	,Encounter.SecondaryOperationCode1
	,Encounter.SecondaryOperationDate1
	,Encounter.SecondaryOperationCode2
	,Encounter.SecondaryOperationDate2
	,Encounter.SecondaryOperationCode3
	,Encounter.SecondaryOperationDate3
	,Encounter.SecondaryOperationCode4
	,Encounter.SecondaryOperationDate4
	,Encounter.SecondaryOperationCode5
	,Encounter.SecondaryOperationDate5
	,Encounter.SecondaryOperationCode6
	,Encounter.SecondaryOperationDate6
	,Encounter.SecondaryOperationCode7
	,Encounter.SecondaryOperationDate7
	,Encounter.SecondaryOperationCode8
	,Encounter.SecondaryOperationDate8
	,Encounter.SecondaryOperationCode9
	,Encounter.SecondaryOperationDate9
	,Encounter.SecondaryOperationCode10
	,Encounter.SecondaryOperationDate10
	,Encounter.SecondaryOperationCode11
	,Encounter.SecondaryOperationDate11
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,ReferralDate
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,AppointmentCategoryCode
	,AppointmentCreatedBy
	,AppointmentCancelDate
	,LastRevisedDate
	,LastRevisedBy
	,OverseasStatusFlag
	,PatientChoiceCode
	,ScheduledCancelReasonCode
	,PatientCancelReason
	,DischargeDate
	,QM08StartWaitDate
	,QM08EndWaitDate
	,DestinationSiteCode
	,EBookingReferenceNo
	,InterfaceCode = 'INQ'

	,DNACode =
	case
	when Encounter.AppointmentStatusCode = '1' then '5'
	when Encounter.AppointmentStatusCode = '2' then '3'
	when Encounter.AppointmentStatusCode = '3' then '5'
	else '3'
	end

	,AttendanceOutcomeCode =
	case
	when Encounter.AppointmentStatusCode = '1' then 'ATT'
	when Encounter.AppointmentStatusCode = '2' then 'DNA'
	when Encounter.AppointmentStatusCode = '3' then 'WLK'
	else 'DNA'
	end

	,LocalAdminCategoryCode = Encounter.AdminCategoryCode

	,PCTCode =
		coalesce(
			 Practice.ParentOrganisationCode
			,Postcode.PCTCode
			,'X98'
		)

	,LocalityCode = LocalAuthority.LocalAuthorityCode

	,Encounter.SeenByCode
from
	dbo.TImportWAEncounter Encounter

left join PAS.Gp RegisteredGp 
ON	Encounter.RegisteredGpCode = RegisteredGp.GpCode

left join Organisation.dbo.Practice Practice
on	Practice.OrganisationCode = RegisteredGp.PracticeCode

left join Organisation.dbo.Postcode Postcode
on	Postcode.Postcode =
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end

left join Organisation.dbo.LocalAuthority LocalAuthority
on	LocalAuthority.LocalAuthorityCode = Postcode.LocalAuthorityCode
