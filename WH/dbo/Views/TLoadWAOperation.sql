﻿create view [dbo].[TLoadWAOperation] as

select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,OperationCode
	,OperationDate
	,ConsultantCode
	,WASourceUniqueID
from
	dbo.TImportWAOperation
