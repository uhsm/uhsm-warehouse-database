﻿create  view [dbo].[TLoadWaitingListActivity] as

select
	 SourceUniqueID

	,ActivityTypeCode

	,ActivityDate = 
		substring(ActivityTime, 7, 2) + '/' +
		substring(ActivityTime, 5, 2) + '/' +
		substring(ActivityTime, 1, 4)

	,ActivityTime = 
		substring(ActivityTime, 7, 2) + '/' +
		substring(ActivityTime, 5, 2) + '/' +
		substring(ActivityTime, 1, 4) + ' ' +

		case
		when substring(ActivityTime, 9, 2) = '24' then '23:59:00'
		else 
		substring(ActivityTime, 9, 2) + ':' +
		substring(ActivityTime, 11, 2) + ':00' 
		end

	,AdminCategoryCode =
		case
		when AdminCategoryCode = 'NHS' then '01'
		when AdminCategoryCode = 'PAY' then '02'
		when AdminCategoryCode = 'IND' then '02'
		when AdminCategoryCode = 'OV' then '02'
		when AdminCategoryCode = 'AME' then '03'
		when AdminCategoryCode = 'CAT' then '04'
		else '01'
		end

	,BookingTypeCode
	,CancelledBy
	,ConsultantCode
	,DeferralEndDate
	,DeferralRevisedEndDate
	,DiagnosticGroupCode
	,SourceEntityRecno
	,SiteCode
	,SourcePatientNo

	,LastRevisionTime =
		substring(LastRevisionTime, 7, 2) + '/' +
		substring(LastRevisionTime, 5, 2) + '/' +
		substring(LastRevisionTime, 1, 4) + ' ' +

		case
		when substring(LastRevisionTime, 9, 2) = '24' then '23:59:00'
		else 
		substring(LastRevisionTime, 9, 2) + ':' +
		substring(LastRevisionTime, 11, 2) + ':00' 
		end

	,LastRevisionUser
	,PreviousActivityID
	,Reason
	,Remark
	,RemovalComment
	,RemovalReasonCode
	,SpecialtyCode
	,SuspensionReasonCode

	,TCIAcceptDate =
		substring(TCIAcceptDate, 7, 2) + '/' +
		substring(TCIAcceptDate, 5, 2) + '/' +
		substring(TCIAcceptDate, 1, 4)

	,TCITime =
		substring(TCITime, 7, 2) + '/' +
		substring(TCITime, 5, 2) + '/' +
		substring(TCITime, 1, 4) + ' ' +

		case
		when substring(TCITime, 9, 2) = '24' then '23:59:00'
		else 
		substring(TCITime, 9, 2) + ':' +
		substring(TCITime, 11, 2) + ':00' 
		end

	,TCIOfferDate =
		substring(TCIOfferDate, 7, 2) + '/' +
		substring(TCIOfferDate, 5, 2) + '/' +
		substring(TCIOfferDate, 1, 4) + ' '

	,PriorityCode
	,WaitingListCode
	,WardCode
	,CharterCancelCode
	,CharterCancelDeferFlag
	,CharterCancel
	,OpCancelledFlag
	,OpCancelled
	,PatientChoiceFlag
	,PatientChoice
	,WLSuspensionInitiatorCode
	,WLSuspensionInitiator
from
	dbo.TImportWaitingListActivity
