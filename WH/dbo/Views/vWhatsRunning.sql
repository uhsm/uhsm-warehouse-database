﻿Create View dbo.vWhatsRunning as 
Select
SQLText.text, 
req.*
From sys.dm_exec_requests req
Cross Apply sys.dm_exec_sql_text(sql_handle) as SQLText
