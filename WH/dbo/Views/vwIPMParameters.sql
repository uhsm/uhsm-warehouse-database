﻿








CREATE view [dbo].[vwIPMParameters] as (
select 1 as [Order], 'Lorenzo' as DB, Parameter as Parameter, DateValue as DateSet from Lorenzo.dbo.Parameter where Parameter = 'DATALOADDATE'
union
select 2, 'WH', Parameter, DateValue from dbo.Parameter where Parameter = 'BUILDCONTACTS'
union
select 3, 'Lorenzo', Parameter, DateValue from Lorenzo.dbo.Parameter where Parameter = 'UncodedSpellCensusDate'
union
select 4, 'WH', Parameter, DateValue from dbo.Parameter where Parameter = 'EXTRACTOPREFERENCEDATE'
union
select 5, 'WH', Parameter, DateValue from dbo.Parameter where Parameter = 'EXTRACTREFERENCEDATADATE'
union
select 6, 'WH', Parameter, DateValue from dbo.Parameter where Parameter = 'EXTRACTAPCDATE'
union
select 7, 'WH', Parameter, DateValue from dbo.Parameter where Parameter = 'EXTRACTAPCWLDATE'
union
select 8, 'WH', Parameter, DateValue from dbo.Parameter where Parameter = 'EXTRACTOPDATE'
union
select 9, 'WH', Parameter, DateValue from dbo.Parameter where Parameter = 'EXTRACTOPWLDATE'
union
select 10, 'WH', Parameter, DateValue from dbo.Parameter where Parameter = 'EXTRACTRFDATE'
union
select 11, 'WH', Parameter, DateValue from dbo.Parameter where Parameter = 'BUILDWHDATE'
union
select 12, 'WHOLAP', Parameter, DateValue from dbo.Parameter where Parameter = 'BUILDMODELDATE'
union
select 13, 'WHREPORTING', Parameter, DateValue from dbo.Parameter where Parameter = 'BUILDWHREPORTINGDATE'
union
select 14, 'CUBE', Parameter, DateValue from dbo.Parameter where Parameter = 'WAREHOUSECUBEPROCESSEDDATE'
union
select 15, 'WH', Parameter, DateValue from dbo.Parameter where Parameter = 'LOADAEDATE'
--union
--select 15, 'WHREPORTING', Parameter, DateValue from dbo.Parameter where Parameter = 'BUILDINFOSQLCOPIES'
)








