﻿


CREATE    VIEW [dbo].[vwMaternityVMSRClinicPatients] AS

SELECT
Encounter.DistrictNo,
Encounter.PatientSurname,
Encounter.PatientForename,
Encounter.DateOfBirth,
Encounter.NHSNumber,
Encounter.SourceUniqueID,
Encounter.AppointmentDate,
Encounter.ClinicCode,
ClinicName = 'ToDo',
Encounter.AttendanceOutcomeCode AS AttendStatus,
Encounter.DNACode,
Encounter.Postcode,
Encounter.EpisodicGpPracticeCode
FROM OP.Encounter Encounter
--LEFT OUTER JOIN LK_CLINIC C ON A.CLINIC_CODE = C.CLINIC_CODE
WHERE 
/*
	(
	APPT_OUTCOME = 'Attended'
	OR APPT_OUTCOME = 'Attended On Time'
	OR APPT_OUTCOME = 'Patient Late / Seen'
	)
*/
Encounter.ClinicCode IN 
	(
	'MLCWCH',
	'MLCWHP',
	'MLCWHP2',
	'MLCWITH',
	'MLCWYTH',
	'MLC2P2',
	'MLCAM2',
	'MLCBAGULEY',
	'MLCBENCHILL',
	'MLCBROADHEATH',
	'MLCBROOKLAND',
	'MLCBROOMWOOD',
	'MLCBURNAGE',
	'MLCDAVY',
	'MLCDUCK',
	'MLCKELLY',
	'MLCKELLY2',
	'MLCLIME',
	'MLCLIME2',
	'MLCMPM',
	'MLCONWAY',
	'MLCPART',
	'MLCPM1',
	'MLCROYLE',
	'MLCSALE',
	'MLCSAND',
	'MLCTRAFFORD',
	'MLC2P2',
	'MLCPM1',
	'MLC1P2',
	'MLCSAND',
	'MLCBAGULEY',
	'MLCBURNAGE',
	'MLCWCH',
	'MLCBENCHILL',
	'MLCWYTH',
	'MLCWHP',
	'MID1ABK',
	'MID2ABK',
	'MID3ABK',
	'MID5ABK',
	'MID1PBK',
	'MID2PBK',
	'MID3PBK',
	'MID4PBK',
	'MID5PBK',
	'MIDTABK',
	'MIDWABK',
	'MLC2ABK',
	'MIDTPBK',
	'MIDWPBK',
	'MLCROYLE',
	'MLCSALE',
	'MID4ABK',
	'MLCWHP2',
	'MLCWITH',
	'MLCDUCK',
	'MLCKELLY',
	'MLCKELLY2',
	'BROOKCC',
	'MLCBROOKLAND',
	'MIDBK',
	'MLCLIME',
	'MLCCOPPICE',
	'MLCTRAFFORD',
	'MIDBKTRAFFORD',
	'MLCBROADHEATH',
	'MLCBROOMWOOD',
	'MLCPART',
	'MLCONWAY',
	'MLCLIME2',
	'MLCDAVY',
	'MLCTIMPERLEY',
	'MLCFLIXTON',
	'MLCNEWALL',
	'MLCSTYAL',
	'MLCCONWAY'
	)

and Encounter.AppointmentDate >='20100701 00:00:00' 
and Encounter.AppointmentDate < cast(convert(varchar(8), getdate(),112) as datetime)



