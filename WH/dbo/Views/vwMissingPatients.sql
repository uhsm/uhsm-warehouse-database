﻿


CREATE view [dbo].[vwMissingPatients] as (
--select mp.SourcePatientNo, MAX(mp.SourceUniqueID) as LastVisitNum from (
select 
	SourcePatientNo
	,SourceUniqueID
	,PatientClass = 'O'
	,EncounterLocation = ClinicCode 
	,EncounterTime = AppointmentTime
from OP.Encounter
where SourcePatientNo not in
	(select patnt_refno from Lorenzo.dbo.Patient)
union

select distinct 
	SourcePatientNo
	,SourceSpellNo
	,'I'
	,''
	,AdmissionTime
from APC.Encounter
where SourcePatientNo not in
	(select patnt_refno from Lorenzo.dbo.Patient)
union

select 
	SourcePatientNo
	,SourceUniqueID
	,'R' 
	,'RF'
	,ReferralDate
from RF.Encounter
where SourcePatientNo not in
	(select patnt_refno from Lorenzo.dbo.Patient)

union

select 
	SourcePatientNo = PATNT_REFNO
	,SourceUniqueID = WLIST_REFNO
	,'W' 
	,'WL'
	,WaitingStartDate = WLIST_DTTM
from Lorenzo.dbo.WaitingList
where PATNT_REFNO not in
	(select patnt_refno from Lorenzo.dbo.Patient)
----) mp
----group by mp.SourcePatientNo
)

----select SourceUniqueID , count(1) as cnt from
----(
--	--select 
--	--	SourcePatientNo
--	--	,SourceUniqueID
--	--	,'R' as col1
--	--	,'' as col2
--	--	,ReferralDate
--	--from RF.Encounter
--	--where SourcePatientNo not in
--	--	(select patnt_refno from Lorenzo.dbo.Patient)
----	)
----	sq
	
----group by SourceUniqueID having count(1) > 1









