﻿


CREATE view [dbo].[vwNewObjects] as
SELECT 
	DatabaseName = 'WH'
	,SchemaName = s.name
	,o.type
	,o.object_id
	,ObjectName = o.name
	,CreateDate = o.create_date
FROM sys.objects o
join sys.schemas s
on o.schema_id = s.schema_id
WHERE o.create_date > '2013-02-10 00:00:00.000'
and o.type in ('U','P', 'V', 'FN', 'TF')
--and s.name = 'CRM'
--order by o.create_date desc

union SELECT 
	'WHREPORTING'
	,SchemaName = s.name
	,o.type
	,o.object_id
	,ObjectName = o.name
	,CreateDate = o.create_date
FROM WHREPORTING.sys.objects o
join WHREPORTING.sys.schemas s
on o.schema_id = s.schema_id
WHERE o.create_date > '2013-02-10 00:00:00.000'
and o.type in ('U','P', 'V', 'FN', 'TF')

union SELECT 
	'Lorenzo'
	,SchemaName = s.name
	,o.type
	,o.object_id
	,ObjectName = o.name
	,CreateDate = o.create_date
FROM Lorenzo.sys.objects o
join Lorenzo.sys.schemas s
on o.schema_id = s.schema_id
WHERE o.create_date > '2013-02-10 00:00:00.000'
and o.type in ('U','P', 'V', 'FN', 'TF')

union SELECT 
	'UHSMApplications'
	,SchemaName = s.name
	,o.type
	,o.object_id
	,ObjectName = o.name
	,CreateDate = o.create_date
FROM UHSMApplications.sys.objects o
join UHSMApplications.sys.schemas s
on o.schema_id = s.schema_id
WHERE o.create_date > '2013-02-10 00:00:00.000'
and o.type in ('U','P', 'V', 'FN', 'TF')

union SELECT 
	'WHOLAP'
	,SchemaName = s.name
	,o.type
	,o.object_id
	,ObjectName = o.name
	,CreateDate = o.create_date
FROM WHOLAP.sys.objects o
join WHOLAP.sys.schemas s
on o.schema_id = s.schema_id
WHERE o.create_date > '2013-02-10 00:00:00.000'
and o.type in ('U','P', 'V', 'FN', 'TF')

