﻿CREATE procedure [dbo].[BuildAEModel] as

/**********************************************************************************
Change Log:
	
	Current Version Date: Currently not in use
	2012-09-13 KO
			Created SP [BuildAEModel] to remove AE model processing from the iPM
			Model sp dbo.BuildModel
			
**********************************************************************************/
		

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

begin
	exec dbo.BuildBaseAE
	exec dbo.BuildFactAE

	----store build time
	--update WH.dbo.Parameter
	--set
	--	DateValue = @StartTime
	--where
	--	Parameter = 'BUILDMODELDATE'

	--if @@rowcount = 0

	--insert into WH.dbo.Parameter
	--	(
	--	 Parameter
	--	,DateValue
	--	)
	--select
	--	 Parameter = 'BUILDMODELDATE'
	--	,DateValue = @StartTime



	select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

	select @Stats = 
		'Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

	exec WriteAuditLogEvent 'BuildAEModel', @Stats, @StartTime

end
