﻿CREATE procedure [dbo].[BuildBaseAPC] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()






truncate table dbo.BaseAPC

insert into dbo.BaseAPC
	(
	 EncounterRecno
	,SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,ReferralSourceUniqueID
	,WaitingListSourceUniqueID
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,DischargeDate
	,EpisodeStartDate
	,EpisodeEndDate
	,StartSiteCode
	,StartWardTypeCode
	,EndSiteCode
	,EndWardTypeCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpPracticeCode
	,SiteCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,AdminCategoryCode
	,ConsultantCode
	,SpecialtyCode
	,LastEpisodeInSpellIndicator
	,FirstRegDayOrNightAdmit
	,NeonatalLevelOfCare
	,PASHRGCode
	,PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,PrimaryOperationCode
	,PrimaryOperationDate
	,SecondaryOperationCode1
	,SecondaryOperationDate1
	,SecondaryOperationCode2
	,SecondaryOperationDate2
	,SecondaryOperationCode3
	,SecondaryOperationDate3
	,SecondaryOperationCode4
	,SecondaryOperationDate4
	,SecondaryOperationCode5
	,SecondaryOperationDate5
	,SecondaryOperationCode6
	,SecondaryOperationDate6
	,SecondaryOperationCode7
	,SecondaryOperationDate7
	,SecondaryOperationCode8
	,SecondaryOperationDate8
	,SecondaryOperationCode9
	,SecondaryOperationDate9
	,SecondaryOperationCode10
	,SecondaryOperationDate10
	,SecondaryOperationCode11
	,SecondaryOperationDate11
	,OperationStatusCode
	,ContractSerialNo
	,CodingCompleteFlag
	,PurchaserCode
	,ProviderCode
	,EpisodeStartTime
	,EpisodeEndTime
	,RegisteredGdpCode
	,EpisodicGpCode
	,InterfaceCode
	,CasenoteNumber
	,NHSNumberStatusCode
	,AdmissionTime
	,DischargeTime
	,TransferFrom
	,DistrictNo
	,SourceUniqueID
	,ExpectedLOS
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,OverseasStatusFlag
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,Created
	,Updated
	,ByWhom
	,TheatrePatientBookingKey
	,ProcedureTime
	,TheatreCode
	,BreachDays
	,ClockStartDate
	,BreachDate
	,RTTBreachDate
	,RTTDiagnosticBreachDate
	,NationalBreachDate
	,NationalDiagnosticBreachDate
	,SocialSuspensionDays
	,BreachTypeCode
	,UnadjustedClockStartDate
	,ResponsibleCommissioner
	,ResponsibleCommissionerCCGOnly
	,OurActivityFlag
	,NotOurActProvCode
	)
select
	 Encounter.EncounterRecno
	,Encounter.SourcePatientNo
	,Encounter.SourceSpellNo
	,Encounter.SourceEncounterNo
	,Encounter.ProviderSpellNo
	,Encounter.ReferralSourceUniqueID
	,Encounter.WaitingListSourceUniqueID
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath
	,Encounter.SexCode
	,Encounter.NHSNumber
	,Encounter.Postcode
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.DHACode
	,Encounter.EthnicOriginCode
	,Encounter.MaritalStatusCode
	,Encounter.ReligionCode
	,Encounter.DateOnWaitingList
	,Encounter.AdmissionDate
	,Encounter.DischargeDate
	,Encounter.EpisodeStartDate
	,Encounter.EpisodeEndDate
	,Encounter.StartSiteCode
	,Encounter.StartWardTypeCode
	,Encounter.EndSiteCode
	,Encounter.EndWardTypeCode
	,Encounter.RegisteredGpCode
	,Encounter.RegisteredGpPracticeCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.SiteCode
	,Encounter.AdmissionMethodCode
	,Encounter.AdmissionSourceCode
	,Encounter.PatientClassificationCode
	,Encounter.ManagementIntentionCode
	,Encounter.DischargeMethodCode
	,Encounter.DischargeDestinationCode
	,Encounter.AdminCategoryCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.LastEpisodeInSpellIndicator
	,Encounter.FirstRegDayOrNightAdmit
	,Encounter.NeonatalLevelOfCare
	,Encounter.PASHRGCode
	,Encounter.PrimaryDiagnosisCode
	,Encounter.SubsidiaryDiagnosisCode
	,Encounter.SecondaryDiagnosisCode1
	,Encounter.SecondaryDiagnosisCode2
	,Encounter.SecondaryDiagnosisCode3
	,Encounter.SecondaryDiagnosisCode4
	,Encounter.SecondaryDiagnosisCode5
	,Encounter.SecondaryDiagnosisCode6
	,Encounter.SecondaryDiagnosisCode7
	,Encounter.SecondaryDiagnosisCode8
	,Encounter.SecondaryDiagnosisCode9
	,Encounter.SecondaryDiagnosisCode10
	,Encounter.SecondaryDiagnosisCode11
	,Encounter.SecondaryDiagnosisCode12
	,Encounter.PrimaryOperationCode
	,Encounter.PrimaryOperationDate
	,Encounter.SecondaryOperationCode1
	,Encounter.SecondaryOperationDate1
	,Encounter.SecondaryOperationCode2
	,Encounter.SecondaryOperationDate2
	,Encounter.SecondaryOperationCode3
	,Encounter.SecondaryOperationDate3
	,Encounter.SecondaryOperationCode4
	,Encounter.SecondaryOperationDate4
	,Encounter.SecondaryOperationCode5
	,Encounter.SecondaryOperationDate5
	,Encounter.SecondaryOperationCode6
	,Encounter.SecondaryOperationDate6
	,Encounter.SecondaryOperationCode7
	,Encounter.SecondaryOperationDate7
	,Encounter.SecondaryOperationCode8
	,Encounter.SecondaryOperationDate8
	,Encounter.SecondaryOperationCode9
	,Encounter.SecondaryOperationDate9
	,Encounter.SecondaryOperationCode10
	,Encounter.SecondaryOperationDate10
	,Encounter.SecondaryOperationCode11
	,Encounter.SecondaryOperationDate11
	,Encounter.OperationStatusCode
	,Encounter.ContractSerialNo
	,Encounter.CodingCompleteFlag
	,Encounter.PurchaserCode
	,Encounter.ProviderCode
	,Encounter.EpisodeStartTime
	,Encounter.EpisodeEndTime
	,Encounter.RegisteredGdpCode
	,Encounter.EpisodicGpCode
	,Encounter.InterfaceCode
	,Encounter.CasenoteNumber
	,Encounter.NHSNumberStatusCode
	,Encounter.AdmissionTime
	,Encounter.DischargeTime
	,Encounter.TransferFrom
	,Encounter.DistrictNo
	,Encounter.SourceUniqueID
	,Encounter.ExpectedLOS
	,Encounter.MRSAFlag
	,Encounter.RTTPathwayID
	,Encounter.RTTPathwayCondition
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.RTTSpecialtyCode
	,Encounter.RTTCurrentProviderCode
	,Encounter.RTTCurrentStatusCode
	,Encounter.RTTCurrentStatusDate
	,Encounter.RTTCurrentPrivatePatientFlag
	,Encounter.RTTOverseasStatusFlag
	,Encounter.RTTPeriodStatusCode
	,Encounter.OverseasStatusFlag
	,Encounter.PASCreated
	,Encounter.PASUpdated
	,Encounter.PASCreatedByWhom
	,Encounter.PASUpdatedByWhom
	,Encounter.Created
	,Encounter.Updated
	,Encounter.ByWhom
	,Encounter.TheatrePatientBookingKey
	,Encounter.ProcedureTime
	,Encounter.TheatreCode
	,Breach.BreachDays
	,Breach.ClockStartDate
	,Breach.BreachDate
	,Breach.RTTBreachDate
	,Breach.RTTDiagnosticBreachDate
	,Breach.NationalBreachDate
	,Breach.NationalDiagnosticBreachDate
	,Breach.SocialSuspensionDays
	,Breach.BreachTypeCode
	,Breach.UnadjustedClockStartDate
	,ResponsibleCommissioner
	,ResponsibleCommissionerCCGOnly
	,OurActivityFlag
	,NotOurActProvCode
from
	WH.APC.Encounter Encounter

inner join WH.APC.Breach Breach
on	Breach.SourceUniqueID = Encounter.SourceUniqueID


select @RowsInserted = @@rowcount



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WHOLAP BuildBaseAPC', @Stats, @StartTime
