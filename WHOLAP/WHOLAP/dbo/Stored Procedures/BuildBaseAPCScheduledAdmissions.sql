﻿CREATE Procedure [dbo].[BuildBaseAPCScheduledAdmissions] as 


If (Select COUNT(*) From Lorenzo.dbo.vwScheduledAdmissions) > 0

BEGIN
SET NOCOUNT ON
	Truncate Table dbo.BaseAPCScheduledAdmission

	Insert into dbo.BaseAPCScheduledAdmission
	(
		 [DataAsOf]
		,[RunTime]
		,[WaitingListUniqueID]
		,[Forename]
		,[Surname]
		,[FacilityID]
		,[NHSNo]
		,[TCIDate]
		,[CancerCode]
		,[ProfessionalCarer]
		,[Specialty]
		,[DateOnList]
		,[IntendedManagment]
		,[AdmissionMethod]
		,[Priority]
		,[AdmitingWardCode]
		,[AdmittingWard]
		,[SecondaryWardCode]
		,[SecondaryWard]
		,[PlannedProcedure]
		,[GeneralComment]
	)

	Select 
		 [DataAsOf]
		,[RunTime]
		,[WaitingListUniqueID]
		,[Forename]
		,[Surname]
		,[FacilityID]
		,[NHSNo]
		,[TCIDate]
		,[CancerCode]
		,[ProfessionalCarer]
		,[Specialty]
		,[DateOnList]
		,[IntendedManagment]
		,[AdmissionMethod]
		,[Priority]
		,[AdmitingWardCode]
		,[AdmittingWard]
		,[SecondaryWardCode]
		,[SecondaryWard]
		,[PlannedProcedure]
		,[GeneralComment]
	From Lorenzo.dbo.vwScheduledAdmissions
END



