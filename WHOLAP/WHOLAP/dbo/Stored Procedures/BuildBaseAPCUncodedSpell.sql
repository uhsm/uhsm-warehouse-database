﻿CREATE Procedure [dbo].[BuildBaseAPCUncodedSpell] as 

Declare 
@LastRun DateTime 
,@dataLoadDate datetime

Select @LastRun = DateValue From Lorenzo.dbo.Parameter Where Parameter = 'UncodedSpellCensusDate'
select @dataLoadDate = DateValue from Lorenzo.dbo.Parameter Where Parameter = 'DATALOADDATE'

--Take parameter value back if need to re-run
--update dbo.Parameter set DateValue = CAST(CONVERT(varchar(8), DateAdd(DAY,-1,getdate()),112) as datetime) where Parameter = 'UncodedSpellCensusDate';
IF @LastRun < @dataLoadDate
BEGIN

	--Make temp table for last casenote move
	Select
		 SourcePatientNo = Casenote.PATNT_REFNO
		,CasenoteUniqueID = Casenote.CASNT_REFNO
		,CasenoteNo = Casenote.IDENTIFIER
		,CurrentLocationUniqueID = Volume.CURNT_SPONT_REFNO
		,CurrentCasenoteLocationCode = ServicePoint.CODE
		,CurrentCasenoteLocation = ServicePoint.DESCRIPTION
		,CasenoteActivityUniqueID = Activity.CASAC_REFNO
		,RequestStatusUniqueID = Activity.RQSTA_REFNO
		,Received = 
			Case When Activity.RQSTA_REFNO = 1426 Then
				'Y'
			Else
				'N'
			End
		,CasenoteMovementCommets = COMMENTS
	into #CurrentCasenoteLocationLastMoved
	from
		Lorenzo.dbo.CasenoteActivity Activity

	Inner Join Lorenzo.dbo.CasenoteVolume Volume
	on Activity.CASVL_REFNO = Volume.CASVL_REFNO

	Inner Join Lorenzo.dbo.Casenote
	on Volume.CASNT_REFNO = Casenote.CASNT_REFNO

	inner join Lorenzo.dbo.ServicePoint 
	on Volume.CURNT_SPONT_REFNO = ServicePoint.SPONT_REFNO

	Where 
	Activity.ARCHV_FLAG = 'N'
	and Casenote.PDTYP_REFNO = 3444
	and Not Exists 
		(
		Select 1 
		From CasenoteActivity PreviousActivity
		Where 
		PreviousActivity.ARCHV_FLAG = 'N'
		And Activity.PATNT_REFNO = PreviousActivity.PATNT_REFNO
		And Activity.CASAC_REFNO < PreviousActivity.CASAC_REFNO
		)
		and Casenote.PATNT_REFNO <> 13208206
		
		
		
	/*-------CM 04/11/2014	
	Delete duplicate problem record from the temp table before insert statement below
	----------------*/
	
	Delete from #CurrentCasenoteLocationLastMoved
	where CasenoteNo = 'HN/RM2148688/01'

	Delete from dbo.BaseAPCUncodedSpell
	Where CensusDate = CAST(Getdate() as DATE)


	Insert into dbo.BaseAPCUncodedSpell
	(
		 CensusDate
		,DistrictNo
		,SourcePatientNo
		,SourceSpellNo
		,AdmissionTime
		,DischargeTime
		,DischargingConsultant
		,SpecialtyCode
		,Specialty
		,[Lapse (Days)]
		,CurrentCasenoteLocationCode
		,CurrentCasenoteLocation
		,IntendedManagement
		,ReceivedInCoding
		,Cases
		,PatientName
		,CasenoteMovementComment
		,CurrentWard
		,BacklogFlag
	)

	Select 
		 CensusDate = Cast(GETDATE() as Date)
		,DistrictNo = DistrictNo.IDENTIFIER
		,SourcePatientNo = Episode.PATNT_REFNO
		,SourceSpellNo = Episode.PRVSP_REFNO
		,AdmissionTime = Spell.ADMIT_DTTM
		,DischargeTime = Spell.DISCH_DTTM
		,DischargingConsultant = Consultant.SURNAME + ', ' + Consultant.FORENAME
		,SpecialtyCode = Specialty.MAIN_IDENT
		,Specialty = Specialty.DESCRIPTION
		,[Lapse (Days)] = DATEDIFF(dd,Spell.DISCH_DTTM,cast(getdate() as DATE))
		,CurrentCasenoteLocation.CurrentCasenoteLocationCode
		,CurrentCasenoteLocation.CurrentCasenoteLocation
		,IntendedManagement = 
				Case IntendedManagement.MAIN_CODE 
					When '1' then 'IP'
					When '2' then 'DC'
					When '3' then 'IP'
					When '4' then 'DC'
					When '5' then 'IP'
					When '8' then 'N/A'
					When '9' then 'NK'
					When 'NSP' then 'NSP'
					Else 'OTHER'
				End
		,ReceivedInCoding = 
			Case When 
				(
				CurrentCasenoteLocation.CurrentLocationUniqueID = 10000381 
				Or CurrentCasenoteLocation.CurrentLocationUniqueID = 10000793
				)
				and CurrentCasenoteLocation.Received = 'Y'
			Then 
				'Y'
			Else
				'N'
			End
		,Cases = 1
		,PatientName = Patient.SURNAME + ', ' + Patient.FORENAME
		,CurrentCasenoteLocation.CasenoteMovementCommets
		,CurrentWard.CODE
		,BacklogFlag = 
			Case When 
				(
					(
					CurrentCasenoteLocation.CurrentLocationUniqueID = 10000381 
					Or CurrentCasenoteLocation.CurrentLocationUniqueID = 10000793
					)
				Or
					(Len(CurrentWard.CODE) >0)
				Or
					(DATEDIFF(dd,Spell.DISCH_DTTM,cast(getdate() as DATE)) < 12)
				)
			Then 'N'
			Else 'Y'
			End
	from
		Lorenzo.dbo.ProfessionalCareEpisode Episode

	Inner Join Lorenzo.dbo.ProviderSpell Spell
	on Episode.PRVSP_REFNO = Spell.PRVSP_REFNO

	--vwCurrentCasenoteLocationLastMoved

	left outer join #CurrentCasenoteLocationLastMoved CurrentCasenoteLocation
	on Episode.PATNT_REFNO = CurrentCasenoteLocation.SourcePatientNo

	--left outer join vwCurrentCasenoteLocation CurrentCasenoteLocation
	--on Episode.PATNT_REFNO = CurrentCasenoteLocation.SourcePatientNo

	left outer join ProfessionalCarer Consultant
	on Episode.PROCA_REFNO = Consultant.PROCA_REFNO
	
	left outer join Lorenzo.dbo.Specialty
	on Episode.SPECT_REFNO = Specialty.SPECT_REFNO

	left outer join Lorenzo.dbo.ReferenceValue IntendedManagement
	on Spell.INMGT_REFNO = IntendedManagement.RFVAL_REFNO

	left outer join Lorenzo.dbo.Patient
	on Episode.PATNT_REFNO = Patient.PATNT_REFNO

	left outer join Lorenzo.dbo.PatientIdentifier DistrictNo
	on	DistrictNo.PATNT_REFNO = Episode.PATNT_REFNO
	and	DistrictNo.PITYP_REFNO = 2001232 --facility
	and	DistrictNo.IDENTIFIER like 'RM2%'
	and DistrictNo.ARCHV_FLAG = 'N'
	and	not exists
		(
		select
			1
		from
			Lorenzo.dbo.PatientIdentifier Previous
		where
			Previous.PATNT_REFNO = DistrictNo.PATNT_REFNO
		and	Previous.PITYP_REFNO = DistrictNo.PITYP_REFNO
		and	Previous.IDENTIFIER like 'RM2%'
		and Previous.ARCHV_FLAG = 'N'
		and	(
				Previous.START_DTTM > DistrictNo.START_DTTM
			or
				(
					Previous.START_DTTM = DistrictNo.START_DTTM
				and	Previous.PATID_REFNO > DistrictNo.PATID_REFNO
				)
			)
		)

	Left Outer Join Lorenzo.dbo.ServicePointStay Stay
	on Spell.PATNT_REFNO = Stay.PATNT_REFNO
	and Stay.ARCHV_FLAG = 'N'
	AND Stay.PRVSN_FLAG = 'N'
	And Not Exists (
		Select
			1
		From
			Lorenzo.dbo.ServicePointStay PreviousStay
		Where 
			Stay.PATNT_REFNO = PreviousStay.PATNT_REFNO
		and Stay.START_DTTM < PreviousStay.START_DTTM
		)
	and Stay.END_DTTM is null

	left outer Join Lorenzo.dbo.ServicePoint CurrentWard
	on Stay.SPONT_REFNO = CurrentWard.SPONT_REFNO
	
	where
		Episode.ARCHV_FLAG = 'N'
	and Spell.ARCHV_FLAG = 'N'
	and Spell.PRVSN_END_FLAG = 'N'
	and Spell.PRVSN_START_FLAG = 'N'
	and Spell.DISCH_DTTM is not null
	and not exists
		(
		Select 1 From Lorenzo.dbo.ClinicalCoding Coding
		Where 
			Coding.SORCE_CODE = 'PRCAE'
		and Coding.ARCHV_FLAG = 'N'
		and Coding.SORCE_REFNO = Episode.PRCAE_REFNO
		and Coding.DPTYP_CODE = 'DIAGN'
		)
	and Spell.DISCH_DTTM = Episode.END_DTTM	
	and Not Exists 
		(
		Select 1
		From Lorenzo.dbo.ProfessionalCareEpisode PreviousEpisode
		Where 
			Episode.PRVSP_REFNO = PreviousEpisode.PRVSP_REFNO
		and PreviousEpisode.END_DTTM = Spell.DISCH_DTTM
		and PreviousEpisode.PRCAE_REFNO > Episode.PRCAE_REFNO
		and PreviousEpisode.ARCHV_FLAG = 'N'
		and PreviousEpisode.PRVSN_FLAG = 'N'
		)
	and Not Exists 
		(
		Select 1
		From Lorenzo.dbo.ProfessionalCareEpisode PreviousEpisode
		Where 
			Episode.PRVSP_REFNO = PreviousEpisode.PRVSP_REFNO
		and PreviousEpisode.END_DTTM = Spell.DISCH_DTTM
		and PreviousEpisode.PRCAE_REFNO > Episode.PRCAE_REFNO
		and PreviousEpisode.ARCHV_FLAG = 'N'
		and PreviousEpisode.PRVSN_FLAG = 'N'
		)
--	and Spell.ADMET_REFNO not in (8816,8817)


UPdate Lorenzo.dbo.Parameter
Set DateValue = GETDATE()
Where Parameter = 'UncodedSpellCensusDate'

END