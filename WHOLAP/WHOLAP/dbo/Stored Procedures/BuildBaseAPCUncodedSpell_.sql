﻿CREATE Procedure [dbo].[BuildBaseAPCUncodedSpell_] as 
/**
KO 07/02/2015 Don't think this is still needed, there is a another version in Lorenzo 
which is called from WH - Load PAS job
**/
Delete from BaseAPCUncodedSpell
Where CensusDate = CAST(Getdate() as DATE)

Insert into [BaseAPCUncodedSpell]
(
	 CensusDate
	,DistrictNo
	,SourcePatientNo
	,SourceSpellNo
	,AdmissionTime
	,DischargeTime
	,DischargingConsultant
	,SpecialtyCode
	,Specialty
	,[Lapse (Days)]
	,CurrentCasenoteLocationCode
	,CurrentCasenoteLocation
	,IntendedManagement
	,ReceivedInCoding
	,Cases
	,PatientName
	,CasenoteMovementComment
)
	




Select 
	 CensusDate = Cast(GETDATE() as Date)
	,DistrictNo
	,APCUncoded.SourcePatientNo
	,APCUncoded.SourceSpellNo
	,AdmissionTime
	,DischargeTime
	,DischargingConsultant = Substring(Cons.Consultant,0,CHARINDEX('(',Cons.Consultant)-1)
	,SpecialtyCode = Spec.LocalSpecialtyCode
	,Specialty = Spec.Specialty
	,[Lapse (Days)] = DATEDIFF(dd,APCUncoded.DischargeDate,cast(getdate() as DATE))
	,CurrentCasenoteLocationCode
	,CurrentCasenoteLocation
	,IntendedManagement = 
			Case IntendedManagement.MAIN_CODE 
				When '1' then 'IP'
				When '2' then 'DC'
				When '3' then 'IP'
				When '4' then 'DC'
				When '5' then 'IP'
				When '8' then 'N/A'
				When '9' then 'NK'
				When 'NSP' then 'NSP'
				Else 'OTHER'
			End
	,ReceivedInCoding = 
		Case When 
			(
			Casenote.CurrentLocationUniqueID = 10000381 
			Or Casenote.CurrentLocationUniqueID = 10000793
			)
			and Casenote.Received = 'Y'
		Then 
			'Y'
		Else
			'N'
		End
	,Cases = 1
	,PatientName = APCUncoded.PatientSurname + ', ' + APCUncoded.PatientForename
	,CasenoteMovementComment = Casenote.CasenoteMovementCommets
From BaseAPC APCUncoded
Left outer join OlapPASConsultant Cons
on cast(APCUncoded.ConsultantCode as varchar(20))= cast(Cons.ConsultantCode as varchar(20))
left outer join OlapPASSpecialty Spec
	on APCUncoded.SpecialtyCode = spec.SpecialtyCode
left outer join WH.PAS.ReferenceValueBase IntendedManagement
	on APCUncoded.ManagementIntentionCode = IntendedManagement.RFVAL_REFNO
left outer join dbo.CasenoteCurrentLocation Casenote
	on APCUncoded.SourcePatientNo = Casenote.SourcePatientNo

Where 



APCUncoded.SourceSpellNo in 
	(
	Select 
	SourceSpellNo
	from BaseAPC APC
	where 
	Exists 
		(
		Select 1 
		from BaseAPC Uncoded
		Where 
		APC.SourceUniqueID = Uncoded.SourceUniqueID
		and PrimaryDiagnosisCode is null
		and DischargeTime is not null
		and EpisodeEndTime is not null
		and AdmissionMethodCode not in (8816,8817)
		)
	)	
and APCUncoded.DischargeTime = APCUncoded.EpisodeEndTime 
