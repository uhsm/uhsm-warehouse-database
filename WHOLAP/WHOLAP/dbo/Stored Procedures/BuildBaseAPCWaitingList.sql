﻿
CREATE procedure [dbo].[BuildBaseAPCWaitingList] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table dbo.BaseAPCWaitingList

insert into dbo.BaseAPCWaitingList
	(
	 EncounterRecno
	,CensusDate
	,SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,ConsultantCode
	,SpecialtyCode
	,PASSpecialtyCode
	,ManagementIntentionCode
	,AdmissionMethodCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,IntendedPrimaryOperationCode
	,Operation
	,SiteCode
	,WardCode
	,WLStatus
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,AdminCategoryCode
	,CancelledBy
	,BookingTypeCode
	,CasenoteNumber
	,OriginalDateOnWaitingList
	,DateOnWaitingList
	,TCIDate
	,KornerWait
	,CountOfDaysSuspended
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,SuspensionReason
	,InterfaceCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,BreachDate
	,ExpectedAdmissionDate
	,ReferralDate
	,FuturePatientCancelDate
	,TheatrePatientBookingKey
	,ProcedureTime
	,TheatreCode
	,AdmissionReason
	,EpisodeNo
	,BreachDays
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NationalBreachDate
	,NationalBreachDays
	,DerivedBreachDays
	,DerivedClockStartDate
	,DerivedBreachDate
	,RTTBreachDate
	,RTTDiagnosticBreachDate
	,NationalDiagnosticBreachDate
	,SocialSuspensionDays
	,BreachTypeCode
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,Created
	,Updated
	,ByWhom
	,ExpectedDischargeDate
	,AnaestheticTypeCode
	,ReferralSourceUniqueID
	,EstimatedTheatreTime
	,WhoCanOperateCode
	,GeneralComment
	,PatientPreparation
	,WaitingListRuleCode
	,ShortNoticeCode
	)
select
	 Encounter.EncounterRecno
	,Encounter.CensusDate
	,Encounter.SourceUniqueID
	,Encounter.SourcePatientNo
	,Encounter.SourceEncounterNo
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath
	,Encounter.SexCode
	,Encounter.NHSNumber
	,Encounter.DistrictNo
	,Encounter.Postcode
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.DHACode
	,Encounter.HomePhone
	,Encounter.WorkPhone
	,Encounter.EthnicOriginCode
	,Encounter.MaritalStatusCode
	,Encounter.ReligionCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.PASSpecialtyCode
	,Encounter.ManagementIntentionCode
	,Encounter.AdmissionMethodCode
	,Encounter.PriorityCode
	,Encounter.WaitingListCode
	,Encounter.CommentClinical
	,Encounter.CommentNonClinical
	,Encounter.IntendedPrimaryOperationCode
	,Encounter.Operation
	,Encounter.SiteCode
	,Encounter.WardCode
	,Encounter.WLStatus
	,Encounter.PurchaserCode
	,Encounter.ProviderCode
	,Encounter.ContractSerialNo
	,Encounter.AdminCategoryCode
	,Encounter.CancelledBy
	,Encounter.BookingTypeCode
	,Encounter.CasenoteNumber
	,Encounter.OriginalDateOnWaitingList
	,Encounter.DateOnWaitingList
	,Encounter.TCIDate
	,Encounter.KornerWait
	,Encounter.CountOfDaysSuspended
	,Encounter.SuspensionStartDate
	,Encounter.SuspensionEndDate
	,Encounter.SuspensionReasonCode
	,Encounter.SuspensionReason
	,Encounter.InterfaceCode
	,Encounter.RegisteredGpCode
	,Encounter.RegisteredGpPracticeCode
	,Encounter.EpisodicGpCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.SourceTreatmentFunctionCode
	,Encounter.TreatmentFunctionCode
	,Encounter.NationalSpecialtyCode
	,Encounter.PCTCode
	,Breach.BreachDate
	,Encounter.ExpectedAdmissionDate
	,Encounter.ReferralDate
	,Encounter.FuturePatientCancelDate
	,Encounter.TheatrePatientBookingKey
	,Encounter.ProcedureTime
	,Encounter.TheatreCode
	,Encounter.AdmissionReason
	,Encounter.EpisodeNo
	,Breach.BreachDays
	,Encounter.MRSAFlag
	,Encounter.RTTPathwayID
	,Encounter.RTTPathwayCondition
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.RTTSpecialtyCode
	,Encounter.RTTCurrentProviderCode
	,Encounter.RTTCurrentStatusCode
	,Encounter.RTTCurrentStatusDate
	,Encounter.RTTCurrentPrivatePatientFlag
	,Encounter.RTTOverseasStatusFlag
	,Breach.NationalBreachDate
	,Encounter.NationalBreachDays
	,Encounter.DerivedBreachDays
	,Encounter.DerivedClockStartDate
	,Encounter.DerivedBreachDate
	,Breach.RTTBreachDate
	,Breach.RTTDiagnosticBreachDate
	,Breach.NationalDiagnosticBreachDate
	,Breach.SocialSuspensionDays
	,Breach.BreachTypeCode
	,Encounter.PASCreated
	,Encounter.PASUpdated
	,Encounter.PASCreatedByWhom
	,Encounter.PASUpdatedByWhom
	,Encounter.Created
	,Encounter.Updated
	,Encounter.ByWhom
	,Encounter.ExpectedDischargeDate
	,Encounter.AnaestheticTypeCode
	,Encounter.ReferralSourceUniqueID
	,Encounter.EstimatedTheatreTime
	,Encounter.WhoCanOperateCode
	,LEFT(Encounter.GeneralComment,1200) -- TJD 08/11/2014
	,LEFT(Encounter.PatientPreparation,1200) -- TJD 08/11/2014
	,Encounter.WaitingListRuleCode
	,Encounter.ShortNoticeCode
from
	WH.APC.WaitingList Encounter

inner join WH.APC.WaitingListBreach Breach
on	Breach.SourceUniqueID = Encounter.SourceUniqueID
and	Breach.CensusDate = Encounter.CensusDate


select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WHOLAP BuildBaseAPCWaitingList', @Stats, @StartTime



