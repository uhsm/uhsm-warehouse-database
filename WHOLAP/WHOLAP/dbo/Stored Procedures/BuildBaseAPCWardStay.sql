﻿CREATE procedure [dbo].[BuildBaseAPCWardStay] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table dbo.BaseAPCWardStay

insert into dbo.BaseAPCWardStay
	(
	 SourceUniqueID
	,SequenceNo
	,SourceSpellNo
	,SourcePatientNo
	,StartTime
	,EndTime
	,WardCode
	,ProvisionalFlag
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,Created
	,Updated
	,ByWhom
	,FCESourceUniqueID
	)
select
	 WardStay.SourceUniqueID

	,SequenceNo =
		row_number() over (partition by WardStay.SourceSpellNo order by WardStay.StartTime, WardStay.SourceUniqueID)

	,WardStay.SourceSpellNo
	,WardStay.SourcePatientNo
	,WardStay.StartTime
	,WardStay.EndTime
	,WardStay.WardCode
	,WardStay.ProvisionalFlag
	,WardStay.PASCreated
	,WardStay.PASUpdated
	,WardStay.PASCreatedByWhom
	,WardStay.PASUpdatedByWhom
	,WardStay.Created
	,WardStay.Updated
	,WardStay.ByWhom
	,FCESourceUniqueID = Encounter.SourceUniqueID
from
	WH.APC.WardStay

left join WH.APC.Encounter
on	Encounter.SourceSpellNo = WardStay.SourceSpellNo
and	Encounter.EpisodeStartTime <= coalesce(WardStay.EndTime, getdate())
and	coalesce(Encounter.EpisodeEndTime, getdate()) >= WardStay.StartTime
and	not exists
	(
	select
		1
	from
		WH.APC.Encounter Previous
	where
		Previous.SourceSpellNo = WardStay.SourceSpellNo
	and	Previous.EpisodeStartTime <= coalesce(WardStay.EndTime, getdate())
	and	coalesce(Previous.EpisodeEndTime, getdate()) >= WardStay.StartTime
	and	(
			Previous.EpisodeStartTime < Encounter.EpisodeStartTime
		or	(
				Previous.EpisodeStartTime = Encounter.EpisodeStartTime
			and	Previous.EncounterRecno < Encounter.EncounterRecno
			)
		)
	)



select @RowsInserted = @@rowcount



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WHOLAP BuildBaseAPCWardStay', @Stats, @StartTime
