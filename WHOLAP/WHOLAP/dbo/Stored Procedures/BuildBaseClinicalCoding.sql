﻿CREATE procedure [dbo].[BuildBaseClinicalCoding] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table dbo.BaseClinicalCoding

insert into dbo.BaseClinicalCoding
	(
	 SourceUniqueID
	,SourceCode
	,SourceRecno
	,SortOrder
	,ClinicalCodingTime
	,ClinicalCodingTypeCode
	,ClinicalCodingCode
	,PASCreatedDate
	,PASModifiedDate
	,PASCreatedByWhom
	,PASUpdatedByWhom
	)
select
	 Coding.SourceUniqueID
	,Coding.SourceCode
	,Coding.SourceRecno
	,Coding.SortOrder
	,Coding.ClinicalCodingTime
	,Coding.ClinicalCodingTypeCode
	,Coding.ClinicalCodingCode
	,Coding.PASCreatedDate
	,Coding.PASModifiedDate
	,Coding.PASCreatedByWhom
	,Coding.PASUpdatedByWhom
from
	WH.PAS.ClinicalCoding Coding
where
	SourceCode in ('PRCAE', 'SCHDL', 'WLIST')
and	ArchiveFlag = 'false'
and	CancelDate is null
and not exists 
	(
	Select 1 
	From WH.PAS.ClinicalCoding PrevCoding
	Where 
		Coding.SourceRecno = PrevCoding.SourceRecno
	and PrevCoding.ArchiveFlag = 'False'
	and Coding.SourceCode = PrevCoding.SourceCode
	and Coding.SortOrder = PrevCoding.SortOrder
	and Coding.ClinicalCodingTypeCode = PrevCoding.ClinicalCodingTypeCode
	and Coding.SourceUniqueID < PrevCoding.SourceUniqueID
	)



select @RowsInserted = @@rowcount



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WHOLAP BuildBaseClinicalCoding', @Stats, @StartTime


