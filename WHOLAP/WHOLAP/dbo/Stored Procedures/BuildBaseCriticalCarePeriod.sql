﻿CREATE procedure [dbo].[BuildBaseCriticalCarePeriod] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table dbo.BaseCriticalCarePeriod

INSERT INTO dbo.BaseCriticalCarePeriod (
	EncounterRecno
	,SourceCCPStayNo
	,SourceCCPNo
	,CCPSequenceNo
	,SourceEncounterNo
	,SourceWardStayNo
	,SourceSpellNo
	,CCPIndicator
	,CCPStartDate
	,CCPEndDate
	,CCPReadyDate
	,CCPNoOfOrgansSupported
	,CCPICUDays
	,CCPHDUDays
	,CCPAdmissionSource
	,CCPAdmissionType
	,CCPAdmissionSourceLocation
	,CCPUnitFunction
	,CCPUnitBedFunction
	,CCPDischargeStatus
	,CCPDischargeDestination
	,CCPDischargeLocation
	,CCPStayOrganSupported
	,CCPStayCareLevel
	,CCPStayNoOfOrgansSupported
	,CCPStayWard
	,CCPStayDate
	,CCPStay
	,CCPStayType
	,CCPBedStay
	)
(
SELECT
	EncounterRecno
	,SourceCCPStayNo
	,SourceCCPNo
	,CCPSequenceNo
	,SourceEncounterNo
	,SourceWardStayNo
	,SourceSpellNo
	,CCPIndicator
	,CCPStartDate
	,CCPEndDate
	,CCPReadyDate
	,CCPNoOfOrgansSupported
	,CCPICUDays
	,CCPHDUDays
	,CCPAdmissionSource
	,CCPAdmissionType
	,CCPAdmissionSourceLocation
	,CCPUnitFunction
	,CCPUnitBedFunction
	,CCPDischargeStatus
	,CCPDischargeDestination
	,CCPDischargeLocation
	,CCPStayOrganSupported
	,CCPStayCareLevel
	,CCPStayNoOfOrgansSupported
	,CCPStayWard
	,CCPStayDate
	,CCPStay
	,CCPStayType
	,CCPBedStay
	FROM WH.APC.CriticalCarePeriod
	)
	
	
	
	
	




select @RowsInserted = @@rowcount



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'BuildBaseCriticalCarePeriod', @Stats, @StartTime

