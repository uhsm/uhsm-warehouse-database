﻿CREATE procedure [BuildBaseOPDiary] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table dbo.BaseOPDiary

insert into dbo.BaseOPDiary
	(
	 SourceUniqueID
	,ClinicCode
	,SessionCode
	,SessionDescription
	,SessionDate
	,SessionStartTime
	,SessionEndTime
	,ReasonForCancellation
	,SessionPeriod
	,DoctorCode
	,Units
	,UsedUnits
	,FreeUnits
	,ValidAppointmentTypeCode
	,InterfaceCode
	,Created
	,ByWhom
	,Cases
	)
SELECT --top 100
	 SourceUniqueID
	,ClinicCode
	,SessionCode
	,SessionDescription
	,SessionDate
	,SessionStartTime
	,SessionEndTime
	,ReasonForCancellation
	,SessionPeriod

	,DoctorCode =
		coalesce(
			 DoctorCode
			,'N/A'
		)

	,Units
	,UsedUnits
	,FreeUnits
	,ValidAppointmentTypeCode
	,InterfaceCode
	,Created
	,ByWhom

	,Cases = 1

from
	WH.OP.Diary


select @RowsInserted = @@rowcount



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'BuildBaseOPDiary', @Stats, @StartTime
