﻿CREATE procedure [dbo].[BuildBaseOPWaitingList] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()


truncate table BaseOPWaitingList

insert into dbo.BaseOPWaitingList
	(
	 EncounterRecno
	,CensusDate
	,SourceUniqueID
	,SourceUniqueIDTypeCode
	,SourcePatientNo
	,ReferralSourceUniqueID
	,AppointmentSourceUniqueID
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,ConsultantCode
	,SpecialtyCode
	,PASSpecialtyCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,SiteCode
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,AdminCategoryCode
	,CancelledBy
	,DoctorCode
	,BookingTypeCode
	,CasenoteNumber
	,InterfaceCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,ReferralDate
	,BookedDate
	,BookedTime
	,AppointmentDate
	,AppointmentTypeCode
	,AppointmentStatusCode
	,AppointmentCategoryCode
	,QM08StartWaitDate
	,QM08EndWaitDate
	,AppointmentTime
	,ClinicCode
	,SourceOfReferralCode
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,ArchiveFlag
	,FuturePatientCancelDate
	,AdditionFlag
	,CountOfDNAs
	,CountOfHospitalCancels
	,CountOfPatientCancels
	,KornerWait
	,BreachDate
	,BreachDays
	,NationalBreachDate
	,NationalBreachDays
	,Created
	,Updated
	,ByWhom
	,DateOnWaitingList
	,IntendedPrimaryOperationCode
	,Operation
	,WaitingListRule
	,VisitCode
	,InviteDate
	,WaitingListClinicCode
	,GeneralComment
	)
SELECT
	 EncounterRecno
	,CensusDate
	,SourceUniqueID
	,SourceUniqueIDTypeCode
	,SourcePatientNo
	,ReferralSourceUniqueID
	,AppointmentSourceUniqueID
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,ConsultantCode
	,SpecialtyCode
	,PASSpecialtyCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,SiteCode
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,AdminCategoryCode
	,CancelledBy
	,DoctorCode
	,BookingTypeCode
	,CasenoteNumber
	,InterfaceCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,ReferralDate
	,BookedDate
	,BookedTime
	,AppointmentDate
	,AppointmentTypeCode
	,AppointmentStatusCode
	,AppointmentCategoryCode
	,QM08StartWaitDate
	,QM08EndWaitDate
	,AppointmentTime
	,ClinicCode
	,SourceOfReferralCode
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,ArchiveFlag
	,FuturePatientCancelDate
	,AdditionFlag
	,CountOfDNAs
	,CountOfHospitalCancels
	,CountOfPatientCancels
	,KornerWait
	,BreachDate
	,BreachDays
	,NationalBreachDate
	,NationalBreachDays
	,Created
	,Updated
	,ByWhom
	,DateOnWaitingList
	,IntendedPrimaryOperationCode
	,Operation
	,WaitingListRule
	,VisitCode
	,InviteDate
	,WaitingListClinicCode
	,GeneralComment
from
	WH.OP.WaitingList Encounter


select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WHOLAP BuildBaseOPWaitingList', @Stats, @StartTime
