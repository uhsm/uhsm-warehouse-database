﻿CREATE procedure [dbo].[BuildBaseOrder] as

/********************************************************************************
	2012.07.03 - KO
	Added select distinct becuase getting duplicate rows from 	WH.MISYS.[Order] Encounter
	
	2011.11.23 - KD
	Added join and criteria to remove cancelled orders
	?only reduced difference by about 600 where are others removed


	2011.08.17 - KD
	Added ApprovedTime column to BaseOrder.  This is to be populated with the 
	Orignal 'OrderTime' from the U_FASTRAK view.  

	2011.08.11 - KD
	Changed CaseGroup 

	2011.05.05 - KD
	Changed Order Date and Order Time to look at Order History Questionnaire
	Date.  This is where an order has been placed via Anglia ICE.  in Misys the
	order date resolves to the date the order was approved.

	2011.04.14 - KD
	Added CaseGroupCode - (EntityLookUp MISYSCaseGroup)
		Still Needs the ExamCode variations added to CaseStatement

	2011.04.01 - KD
	Added: 
	PCTCode 
	PCTName


	2011.03.04 - KD
	Updated:
		CaseTypeCode
		CaseTypeCode1
		CaseType
	Changes requested by Service any Ultrasound General Case Type should be
	Obsetric Ultrasound where the requesting physician is Obs or Gynae
	
	
	2012.10.17 - JC
	added function whreporting.dbo.RemoveNonDisplayChars to various fields to eliminate non-printable characters

	
********************************************************************************/

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table dbo.BaseOrder

insert into dbo.BaseOrder
	(
	 EncounterRecno
	,OrderNumber
	,PerformanceDate
	,CreationDate
	,EpisodeNumber
	,PerformanceTimeInt
	,PerformanceTime
	,PatientName
	,Department
	,ExamDescription
	,ExamGroupCode
	,DocumentStatusCode
	,FilmEntStatusCode
	,PatientTrackingStatusCode
	,CloseDate
	,OpenDate
	,PerformanceJulianDate
	,OrderDate
	,OrderTime
	,OrderingLocationCode
	,PatientAge
	,PatientTypeCode
	,DateOfBirth
	,SexCode
	,OrderExamCode
	,PerformanceDateInt
	,CaseTypeCode
	,OrderStatusCode
	,AttendingPhysicianCode1
	,AttendingPhysicianCode2
	,CaseStatusCode
	,CaseStatusDate
	,DictationStatusCode
	,OrderScheduledFlag
	,OrderingPhysicianCode
	,PriorityCode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,AttendingPhysician1
	,LocationCode
	,PatientForename
	,PatientSurname
	,SocialSecurityNo
	,ExtraData1
	,ExtraData2
	,ExtraData3
	,Postcode
	,DateOfDeath
	,RegisteredGpPractice
	,RegisteredGpPracticeCode
	,xgen3
	,xgen4
	,NHSNumberVerifiedFlag
	,DepartmentCode
	,ExamCode1
	,HISOrderNumber
	,ExamVar4
	,AETitleLocationCode
	,PrimaryPhysicianCode
	,PrimaryPhysician
	,SpecialtyCode
	,SuspensionDaysCode
	,RequestReceivedDate
	,VettedDate
	,DelayedByPatient
	,FailedPatientContact
	,PlannedRequest
	,RequestDate
	,FilmDataFlag
	,OrderingPhysicianCode1
	,PatientNo
	,PatientGenderCode
	,PatientNo1
	,AttendingPhysician2
	,ScheduleStatusCode
	,OrderingLocationCode1
	,CancelDate
	,DictationDate
	,FinalDate
	,PatientTypeForLinkingCode
	,CaseTypeCode1
	,CaseType
	,ModifiedExamCode
	,LinkExamCode
	,LinkLocationCode
	,LinkScheduleRoomCode
	,LinkTechCode
	,LinkLocation
	,ModificationDate
	,ModificationTime
	,ScheduleDate
	,ScheduleEndTime
	,ScheduleRoomCode
	,ScheduleStartTime
	,ModifiedTechCode
	,ScheduleCreateDate
	,ScheduleCreateTime
	,CreateLinkExamCode
	,CreateLinkLocationCode
	,CreateLinkScheduleRoomCode
	,CreateLinkTechCode
	,CreateLinkLocation
	,CreateLinkScheduleDate
	,CreateLinkScheduleRoom
	,CreateLinkTech
	,CancelDate1
	,CancelTime
	,CancelLinkTech
	,CancelLocationCode
	,CancelTechCode
	,OrderingPhysician
	,ONXQuestionnaireDataCode
	,PerformanceLocation
	,RadiologistCode
	,SeriesNo
	,FilmTechCode
	,PCTCode
	,PCTName
	,HasDeletedSchedule
	,CaseGroupCode
	,ApprovedTime
	,FiledDate
	,FiledTime
	)
select distinct
	 Encounter.EncounterRecno
	,OrderNumber=whreporting.dbo.RemoveNonDisplayChars(Encounter.OrderNumber)
	,PerformanceDate = 
			Case When DeletedSchedule.OrderNumber is null then
					Encounter.PerformanceDate
			Else
--2011.08.17 Updated KD	OrderDate replaced with order rule
--Encounter.OrderDate
				Case When Encounter.OrderDate < Coalesce(CAST(Convert(varchar(8), OQH.OQHTime,112) as SmallDatetime),'20300101 00:00:00') Then
					Encounter.OrderDate
				Else
					CAST(Convert(varchar(8), OQH.OQHTime,112) as SmallDatetime)
				End
			End
	,CreationDate
	,EpisodeNumber
	,PerformanceTimeInt
	,PerformanceTime =
			Case When DeletedSchedule.OrderNumber is null then
					Encounter.PerformanceTime
			Else
--2011.08.17 Updated KD	OrderTime replaced with order time rule
--Encounter.OrderTime
				Case When Encounter.OrderTime < Coalesce(OQH.OQHTime,'20300101 00:00:00') Then
					Encounter.OrderTime
				Else
					OQH.OQHTime
				End
			End
	,PatientName=whreporting.dbo.RemoveNonDisplayChars(Encounter.PatientName)
	,Department=whreporting.dbo.RemoveNonDisplayChars(Department)
	,ExamDescription=whreporting.dbo.RemoveNonDisplayChars(Encounter.ExamDescription)
	,ExamGroupCode=whreporting.dbo.RemoveNonDisplayChars(ExamGroupCode)
	,DocumentStatusCode
	,FilmEntStatusCode
	,PatientTrackingStatusCode
	,Encounter.CloseDate
	,Encounter.OpenDate
	,PerformanceJulianDate
	,OrderDate =
		Case When Encounter.OrderDate < Coalesce(CAST(Convert(varchar(8), OQH.OQHTime,112) as SmallDatetime),'20300101 00:00:00') Then
			Encounter.OrderDate
		Else
			CAST(Convert(varchar(8), OQH.OQHTime,112) as SmallDatetime)
		End
		
	,OrderTime = 
		Case When Encounter.OrderTime < Coalesce(OQH.OQHTime,'20300101 00:00:00') Then
			Encounter.OrderTime
		Else
			OQH.OQHTime
		End
		
	,whreporting.dbo.RemoveNonDisplayChars(OrderingLocationCode)
	,PatientAge
	,PatientTypeCode=whreporting.dbo.RemoveNonDisplayChars(PatientTypeCode)
	,DateOfBirth
	,SexCode
	,whreporting.dbo.RemoveNonDisplayChars(OrderExamCode)
	,PerformanceDateInt
--KD 2011.03.04
	,CaseTypeCode = 
			Case When 
				Encounter.CaseTypeCode = 'U' 
				and
				(OrderPhysician.Physician like '%(%obs%)%' or OrderPhysician.Physician like '%(%gyn%)%')
				Then 'O'
				Else encounter.CaseTypeCode
				End

	,OrderStatusCode
	,AttendingPhysicianCode1=whreporting.dbo.RemoveNonDisplayChars(AttendingPhysicianCode1)
	,AttendingPhysicianCode2=whreporting.dbo.RemoveNonDisplayChars(AttendingPhysicianCode2)
	,CaseStatusCode
	,CaseStatusDate
	,DictationStatusCode
	,OrderScheduledFlag
	,OrderingPhysicianCode=whreporting.dbo.RemoveNonDisplayChars(OrderingPhysicianCode)
	,PriorityCode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,AttendingPhysician1
	,LocationCode=whreporting.dbo.RemoveNonDisplayChars(LocationCode)
	,PatientForename
	,PatientSurname
	,SocialSecurityNo
	,ExtraData1
	,ExtraData2
	,ExtraData3
	,Encounter.Postcode
	,DateOfDeath
	,RegisteredGpPractice=whreporting.dbo.RemoveNonDisplayChars(RegisteredGpPractice)
	,RegisteredGpPracticeCode=whreporting.dbo.RemoveNonDisplayChars(RegisteredGpPracticeCode)
	,xgen3
	,xgen4
	,NHSNumberVerifiedFlag
	,DepartmentCode=whreporting.dbo.RemoveNonDisplayChars(DepartmentCode)
	,ExamCode1=whreporting.dbo.RemoveNonDisplayChars(ExamCode1)
	,HISOrderNumber
	,ExamVar4
	,AETitleLocationCode
	,PrimaryPhysicianCode=whreporting.dbo.RemoveNonDisplayChars(PrimaryPhysicianCode)
	,PrimaryPhysician=whreporting.dbo.RemoveNonDisplayChars(PrimaryPhysician)
	,SpecialtyCode
	,SuspensionDaysCode
	,RequestReceivedDate
	,VettedDate
	,DelayedByPatient
	,FailedPatientContact
	,PlannedRequest
	,RequestDate
	,FilmDataFlag
	,OrderingPhysicianCode1
	,PatientNo
	,PatientGenderCode
	,PatientNo1
	,AttendingPhysician2
	,ScheduleStatusCode
	,OrderingLocationCode1
	,CancelDate
	,DictationDate
	,FinalDate
	,PatientTypeForLinkingCode
--KD 2011.03.04
	,CaseTypeCode1 = 
			Case When 
				Encounter.CaseTypeCode = 'u' 
				and
				(OrderPhysician.Physician like '%(%obs%)%' or OrderPhysician.Physician like '%(%gyn%)%')
				Then 'O'
				Else encounter.CaseTypeCode
				End
--KD 2011.03.04
	,CaseType =
			Case When 
				Encounter.CaseTypeCode = 'u' 
				and
				(OrderPhysician.Physician like '%(%obs%)%' or OrderPhysician.Physician like '%(%gyn%)%')
				Then 'OBSTETRIC ULTRASOUND'
				Else whreporting.dbo.RemoveNonDisplayChars(encounter.CaseType)
				End
	,whreporting.dbo.RemoveNonDisplayChars(ModifiedExamCode)
	,LinkExamCode
	,LinkLocationCode
	,LinkScheduleRoomCode
	,LinkTechCode
	,LinkLocation
	,ModificationDate
	,ModificationTime
	,ScheduleDate
	,ScheduleEndTime
	,ScheduleRoomCode
	,ScheduleStartTime
	,ModifiedTechCode
	,ScheduleCreateDate
	,ScheduleCreateTime
	,CreateLinkExamCode
	,CreateLinkLocationCode
	,CreateLinkScheduleRoomCode
	,CreateLinkTechCode
	,CreateLinkLocation
	,CreateLinkScheduleDate
	,CreateLinkScheduleRoom
	,CreateLinkTech
	,CancelDate1
	,CancelTime
	,CancelLinkTech
	,CancelLocationCode
	,CancelTechCode
	,whreporting.dbo.RemoveNonDisplayChars(OrderingPhysician)
	,ONXQuestionnaireDataCode
	,whreporting.dbo.RemoveNonDisplayChars(PerformanceLocation)
	,RadiologistCode
	,SeriesNo
	,FilmTechCode
	,PCTCode = Coalesce(PCT.OrganisationCode, ResidencePCT.OrganisationCode,'5NT')
	,PCTName = Coalesce(PCT.Organisation, ResidencePCT.Organisation,'MANCHESTER PCT')
	,HasDeletedSchedule =
			CONVERT(
					bit,
					
						Case When DeletedSchedule.OrderNumber is null then
							0
						Else
							1
						End
					)
	,CaseGroupCode = 
	Case 
		When (
				Encounter.CaseTypeCode IN ('B','H','T','CM')
				OR
				Encounter.ExamCode1 IN 
				(
                       --'FABDO', 
                        --'FCYST',
                        --'FDIAP',
                        --'FHIPLJ',
                        --'FHIPLM',
                        --'FHIPRJ',
                        --'FHIPRM',
                        --'FINJTJ',
                        --'FJOINJ',
                        --'FLOLB',
                        --'FLOLL',
                        --'FLOLR',
                        --'FLSPN',
                        --'FLSPNM',
                        --'FUPLB',
                        --'FUPLL',
                        --'FUPLR',
                        --'FPAC',
                        --'IFACJJ',

				'MAAOT',
				'MACOA',
				'MCARD',
				'MCCMS',
				'MCORP',
				'MCORV',
				'MCRPS',
				'MCSFS',
				'MCVFS',
				'MCVIA',
				'MCVVS',
				'NSENT',
				'UMAMB',
				'UMAML',
				'UMAMR'
				)
				OR 
				(Encounter.ExamCode1 = 'MAAOT' AND (OrderPhysician.Physician like '%(CARDIO)%' OR OrderPhysician.Physician like '%(THOR)%'))
				OR
				(OrderDate < '2011-08-01' and CaseTypeCode = 'D')
				) Then 'S'
		When CaseTypeCode in ('Z','O','V','R') Then 'O'
		When CaseTypeCode IN ('C', 'M','F','I','P','U','D','X') Then 'G'
	Else 'U'
	End
	,ApprovedTime = Encounter.OrderTime
	,FiledDate
	,FiledTime=case when whreporting.dbo.RemoveNonDisplayChars(FiledTime) like ('%[A-Z%]%') THEN null else whreporting.dbo.RemoveNonDisplayChars(FiledTime) end
from
	WH.MISYS.[Order] Encounter
	
	left outer join WH.MISYS.Physician OrderPhysician
	on encounter.OrderingPhysicianCode = OrderPhysician.PhysicianCode
	
	left outer join WH.MISYS.DeletedScheduleEncounter DeletedSchedule
	on Encounter.OrderNumber = DeletedSchedule.OrderNumber
	and Encounter.PerformanceTime = DeletedSchedule.SegmentStartTime
	
	left outer join Organisation.dbo.Practice Practice
	on Encounter.RegisteredGpPracticeCode = Practice.OrganisationCode
	
	left outer join Organisation.dbo.PCT PCT
	on Practice.ParentOrganisationCode = PCT.OrganisationCode
	
	left outer join Organisation.dbo.Postcode Postcode
	on Encounter.Postcode = Postcode.Postcode
	
	left outer join Organisation.dbo.PCT ResidencePCT
	on Postcode.PCTCode = ResidencePCT.OrganisationCode
	
	--CCG
	--left join OrganisationCCG.dbo.Practice PracticeCCG
	--on Encounter.RegisteredGpPracticeCode = PracticeCCG.OrganisationCode
	
	--left join OrganisationCCG.dbo.CCG CCG
	--on PracticeCCG.ParentOrganisationCode = CCG.OrganisationCode
	
	left outer join
		(
		Select 
		OrderNumber, 
		MIN(ModifiedTime) as OQHTime
		From WH.MISYS.OrderHistoryQuestionnaire
		Group By
		OrderNumber
		) OQH
	on Encounter.OrderNumber = OQH.OrderNumber
	left outer join WH.MISYS.CancelledOrder CancOrder
	on Encounter.OrderNumber = CancOrder.OrderNumber
Where CancOrder.OrderNumber is null
and OrderExamCode<>'-1'

select @RowsInserted = @@rowcount

--KO added 18/12/2014 due to SSAS error
--CDUP and other vascular codes removed from MISYS
--delete from dbo.BaseOrder where ExamCode1 in ('LLVDB','CDUP', 'FFA', 'ULLVB', 'UCARB', 
--	'UULAB', 'ULLAB', 'BAPWA', 'UULVB', 'POVMB', 'UVMAP')

select @RowsInserted = @RowsInserted - @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'BuildBaseOrder', @Stats, @StartTime


