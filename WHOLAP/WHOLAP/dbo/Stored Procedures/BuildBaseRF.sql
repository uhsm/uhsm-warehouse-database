﻿CREATE procedure [dbo].[BuildBaseRF] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table dbo.BaseRF

insert into dbo.BaseRF
	(
	 EncounterRecno
	,SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,EpisodicGdpCode
	,SiteCode
	,ConsultantCode
	,SpecialtyCode
	,SourceOfReferralCode
	,PriorityCode
	,ReferralDate
	,OriginalProviderReferralDate
	,DischargeDate
	,DischargeTime
	,DischargeReasonCode
	,DischargeReason
	,AdminCategoryCode
	,ContractSerialNo
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NextFutureAppointmentDate
	,InterfaceCode
	,Created
	,Updated
	,ByWhom
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom

	,AgeCode
	,Cases
	,Duplicate
	,RequestedService
	,CancellationDate
	,CancellationReason
	,SelfReferringBaby
	,ReferralMediumCode
	,ResponsibleOrganisationCode
	,ResponsibleOrganisationSource
	,ReferredByConsultantCode
	,ReferredByOrganisationCode
	,ReferredBySpecialtyCode
	,ReferralTypeCode
	,ResponsibleCommissionerCCGOnly
	,GeneralComment
	,ReferralComment
	,OurActivityFlag
	,NotOurActProvCode


	)
select --top 100
	 EncounterRecno
	,SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Encounter.Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode

	,RegisteredGpPracticeCode =
		coalesce(Encounter.RegisteredGpPracticeCode, Encounter.EpisodicGpPracticeCode, -1)

	,EpisodicGpCode

	,EpisodicGpPracticeCode =
		coalesce(Encounter.EpisodicGpPracticeCode, Encounter.RegisteredGpPracticeCode, -1)

	,EpisodicGdpCode

	,SiteCode =
		coalesce(Encounter.SiteCode, -1)

	,ConsultantCode =
		coalesce(Encounter.ConsultantCode, -1)

	,SpecialtyCode =
		coalesce(Encounter.SpecialtyCode, -1)

	,SourceOfReferralCode
	,PriorityCode
	,ReferralDate
	,OriginalProviderReferralDate
	,DischargeDate
	,DischargeTime
	,DischargeReasonCode
	,DischargeReason
	,AdminCategoryCode
	,ContractSerialNo
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NextFutureAppointmentDate
	,InterfaceCode
	,Created
	,Updated
	,ByWhom
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom

	,AgeCode =
	case
	when datediff(day, DateOfBirth, Encounter.ReferralDate) is null then 'Age Unknown'
	when datediff(day, DateOfBirth, Encounter.ReferralDate) <= 0 then '00 Days'
	when datediff(day, DateOfBirth, Encounter.ReferralDate) = 1 then '01 Day'
	when datediff(day, DateOfBirth, Encounter.ReferralDate) > 1 and
	datediff(day, DateOfBirth, Encounter.ReferralDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, DateOfBirth, Encounter.ReferralDate)), 2) + ' Days'
	
	when datediff(day, DateOfBirth, Encounter.ReferralDate) > 28 and
	datediff(month, DateOfBirth, Encounter.ReferralDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.ReferralDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, DateOfBirth, Encounter.ReferralDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.ReferralDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, DateOfBirth, Encounter.ReferralDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.ReferralDate) then 1 else 0 end > 1 and
	 datediff(month, DateOfBirth, Encounter.ReferralDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.ReferralDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, DateOfBirth,Encounter.ReferralDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.ReferralDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, DateOfBirth, Encounter.ReferralDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.ReferralDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.ReferralDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.ReferralDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, DateOfBirth, Encounter.ReferralDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.ReferralDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.ReferralDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.ReferralDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end

	,Cases = 1
	,Duplicate =
	Case When Exists (
					Select 1 from WH.RF.Encounter Previous
					Where 
						Encounter.SourcePatientNo = Previous.SourcePatientNo 
					and Encounter.SpecialtyCode = Previous.SpecialtyCode
					and Encounter.ConsultantCode = Previous.ConsultantCode
					and Previous.ReferralDate < Encounter.ReferralDate
					and DATEDIFF(d,previous.referraldate,Encounter.referralDate) <10
					) Then
				Convert(Bit,1)
	Else
				Convert(Bit,0)
	End 
	,RequestedService = 
		Case When (SourceOfReferralCode = 2002324 or Encounter.ReferralMedium IN (2004336,2004345)) Then 232
		Else RequestedService
		end
	,CancellationDate
	,CancellationReason
	,SelfReferringBaby =
		Case When Exists 
			(
			Select 1 from WH.APC.Encounter apc
			where 
			(
			Encounter.SourceUniqueID = apc.ReferralSourceUniqueID
			and Cast(Convert(varchar(8), Encounter.ReferralDate,112) as datetime) = apc.AdmissionDate
			and apc.AdmissionMethodCode in (8816,8817) -- 8816 = BHOSP; 8817 = BOHOSP
			)
			)
			and not exists
				(
				Select 1 from WH.APC.Encounter NextAdm
				Where 
				Encounter.SourceUniqueID = NextAdm.ReferralSourceUniqueID
				and Cast(Convert(varchar(8), Encounter.ReferralDate,112) as datetime) < NextAdm.AdmissionDate
				)
			and not exists
				(
				Select 1 from WH.OP.Encounter OP
				Where Encounter.SourceUniqueID = OP.ReferralSourceUniqueID
				)
			and not exists
				(
				Select 1 from Lorenzo.dbo.WaitingList wait
				where Encounter.SourceUniqueID = wait.refrl_refno 
				and wait.ARCHV_FLAG = 'n'
				and wait.CANCL_DTTM IS NULL
				)
			 Then convert(bit,1)
		Else
			convert(bit,0)
		End
	,ReferralMedium = Encounter.ReferralMedium
	,ResponsibleOrganisationCode = 
			Coalesce(
				Case When EpisodePCT.OrganisationTypeCode = 2001715 Then
					Null
				Else
					EpisodePCT.OrganisationLocalCode
				End,
				Residence.PCTCode,
				'5NT'
			)
	,ResponsibleOrganisationSource = 
			Coalesce(
				Case When (EpisodePCT.OrganisationTypeCode = 2001715 or EpisodePCT.OrganisationCode is null) Then
					Null
				Else
					'EpisodeGPPractice'
				End,
				Case When Residence.PCTCode Is Null Then
					Null
				Else
					'Residence'
				End,
				'HostPCT'
			)
	,ReferredByConsultantCode = IsNull(Encounter.ReferredByConsultantCode,-2)
	,ReferredByOrganisationCode = IsNull(Encounter.ReferredByOrganisationCode,-2)
	,ReferredBySpecialtyCode = IsNull(Encounter.ReferredBySpecialtyCode,-2)
	,ReferralTypeCode = 
	
			Case
			--Cancelled = 1
			When (Not CancellationDate Is null )Then 1
			When (Not CancellationReason In (62,0)) Then 1
			--SelfReferringBaby = 2
			When Exists 
				(
				Select 1 from WH.APC.Encounter apc
				where 
				(
				Encounter.SourceUniqueID = apc.ReferralSourceUniqueID
				and Cast(Convert(varchar(8), Encounter.ReferralDate,112) as datetime) = apc.AdmissionDate
				and apc.AdmissionMethodCode in (8816,8817) -- 8816 = BHOSP; 8817 = BOHOSP
				)
				)
				and not exists
					(
					Select 1 from WH.APC.Encounter NextAdm
					Where 
					Encounter.SourceUniqueID = NextAdm.ReferralSourceUniqueID
					and Cast(Convert(varchar(8), Encounter.ReferralDate,112) as datetime) < NextAdm.AdmissionDate
					)
				and not exists
					(
					Select 1 from WH.OP.Encounter OP
					Where Encounter.SourceUniqueID = OP.ReferralSourceUniqueID
					)
				and not exists
					(
					Select 1 from Lorenzo.dbo.WaitingList wait
					where Encounter.SourceUniqueID = wait.refrl_refno 
					and wait.ARCHV_FLAG = 'n'
					and wait.CANCL_DTTM IS NULL
					)
				 Then 2
			--Duplicate = 3
			When Exists (
					Select 1 from WH.RF.Encounter Previous
					Where 
						Encounter.SourcePatientNo = Previous.SourcePatientNo 
					and Encounter.SpecialtyCode = Previous.SpecialtyCode
					and Encounter.ConsultantCode = Previous.ConsultantCode
					and Previous.ReferralDate < Encounter.ReferralDate
					and DATEDIFF(d,previous.referraldate,Encounter.referralDate) <10
					) Then 3
			--Choose and Book = 4
			When (SourceOfReferralCode = 2002324 or Encounter.ReferralMedium IN (2004336,2004345)) Then 4
			-- Paper Referral =5
			When Encounter.ReferralMedium NOT IN (2004336,2004345) Then 5
			--Other 
			Else 99
		End
	,ResponsibleCommissionerCCGOnly
	,GeneralComment
	,ReferralComment
	,OurActivityFlag
	,NotOurActProvCode


from
	WH.RF.Encounter Encounter
	
Left outer join Organisation.dbo.Postcode Residence
--	On Replace(Encounter.Postcode,' ','') = Residence.SlimPostcode
	On Encounter.Postcode = Residence.Postcode
left outer join WH.PAS.Organisation EpisodePractice
	on Encounter.EpisodicGpPracticeCode = EpisodePractice.OrganisationCode
left outer join WH.PAS.Organisation EpisodePCT
	on EpisodePractice.ParentOrganisationCode = EpisodePCT.OrganisationCode
	
	

select @RowsInserted = @@rowcount



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WHOLAP BuildBaseRF', @Stats, @StartTime













