﻿CREATE procedure [dbo].[BuildBaseRTT]
	@censusDate smalldatetime
as

/************************************************************************************************
	2011-07-29
	Added Criteria for Preop Clinic Discharges, (they should not effect a clockstop)
	Clinic codes are WH.dbo.EntityLookup

	2011-06-28
	Reversed Decision to Add IPW to criteria for Legacy rules to Open Pathways
	(? Secondary Pathways not in RTT dataset possible for initial decision)
	
	2011-01-19
	Added IPW to criteria for Legacy rules to Open Pathways
	Added Criteria to exclude Legacy Referrals
	
	2011-01-18
	Updated Outpatient Waiting List portion to exclude Physio and Midwives
	
	2011-01-17 KD
	Updated Outpatient Waiting List portion to exclude Follow Up Outpatient Waitiers
		WaitingListRule code = 150000536

************************************************************************************************/


declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime
declare @census smalldatetime

select @StartTime = getdate()


--for performance reasons
select
	@census = @censusDate

--@from contains the earliest of the date one week ago and the 1st of the month
select
	@from =
		min(FromDate)
from
	(
	select
		FromDate =
			dateadd(
				day
				,-6
				,@census
			)

	union all

	select
		FromDate =
			'01' +
		substring(
			convert(
				varchar
				,@census
				,113
			)
			,3
			,9
		)
	) Dates

select
	@to = @census


delete from dbo.BaseRTT
where
	CensusDate = @to


--delete everything >3 months old --PDO 24 Aug 2011
delete from dbo.BaseRTT
where
	CensusDate <= dateadd(month, -3, @to)


select @RowsInserted = 0

--treated admitted

--RTT Rules:
--must be elective non-planned
--exclude maternity
--consultant led
--non-diagnostic

insert into dbo.BaseRTT
(
	 CensusDate
	,SourceEncounterRecno
	,SourceCode
	,PathwayStatusCode
	,KeyDate
	,ClockStartDate
	,ClockStopDate
	,BreachDate
	,RTTCurrentStatusCode
	,RTTPeriodStatusCode
	,RTTCurrentStatusDate
	,Cases
	,ReferralDate
	,OriginalProviderReferralDate
	,SocialSuspensionDays
	,MappedSpecialtyCode
	,ReferralSourceUniqueID
	,WaitingListSourceUniqueID
	,SourcePatientNo
	,DistrictNo
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DateOnWaitingList
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,ConsultantCode
	,SpecialtyCode
	,AdminCategoryCode
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,PrimaryOperationDate
	,RTTPathwayID
	,AdmissionDate
	,DischargeDate
	,StartSiteCode
	,StartWardTypeCode
	,EndSiteCode
	,EndWardTypeCode
	,AdmissionMethodCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,PriorityCode
	,ClinicCode
	,SourceOfReferralCode
	,AppointmentDate
	,FirstAttendanceFlag
	,DNACode
	,AttendanceOutcomeCode
	,DisposalCode
	,PreviousAppointmentDate
	,PreviousAppointmentStatusCode
	,FuturePatientCancelDate
	,CountOfDNAs
	,CountOfPatientCancels
	,NextAppointmentDate
	,LastDnaOrPatientCancelledDate
	,DateOnFutureOPWaitingList
	,PASPrimaryOperationCode
)
select
	 CensusDate = @census
	,SourceEncounterRecno = Encounter.EncounterRecno
	,SourceCode = 'APC'

	,PathwayStatusCode = 'AP' --admitted pathway

	,KeyDate = Encounter.AdmissionDate

	,ClockStartDate = Breach.ClockStartDate

	,ClockStopDate = Encounter.AdmissionDate

	,Breach.RTTBreachDate

	,RTTCurrentStatusCode = CurrentRTTStatus.RTTStatusCode
	,RTTPeriodStatusCode = PeriodRTTStatus.RTTStatusCode
	,Encounter.RTTCurrentStatusDate

	,Cases = 1

	,ReferralDate =
		case
		when datepart(year, RFEncounter.ReferralDate) = 1948
		then null
		else RFEncounter.ReferralDate
		end

	,OriginalProviderReferralDate =
		case
		when datepart(year, RFEncounter.OriginalProviderReferralDate) <= 1948
		then null
		else RFEncounter.OriginalProviderReferralDate
		end

	,SocialSuspensionDays = Breach.SocialSuspensionDays

	,MappedSpecialtyCode = ConsultantSpecialtyMap.XrefEntityCode

	,Encounter.ReferralSourceUniqueID
	,Encounter.WaitingListSourceUniqueID
	,Encounter.SourcePatientNo
	,Encounter.DistrictNo
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath
	,SexCode = Sex.MainCode
	,Encounter.NHSNumber
	,Encounter.Postcode
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.DateOnWaitingList
	,RegisteredGpCode = Gp.NationalGpCode
	,Encounter.RegisteredGpPracticeCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,AdminCategoryCode = AdminCategory.MainCode
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.PrimaryOperationDate
	,Encounter.RTTPathwayID

--IP columns
	,Encounter.AdmissionDate
	,Encounter.DischargeDate
	,StartSiteCode = StartSite.OrganisationLocalCode
	,StartWardTypeCode = StartWard.ServicePointLocalCode
	,EndSiteCode = EndSite.OrganisationLocalCode
	,EndWardTypeCode = EndWard.ServicePointLocalCode
	,AdmissionMethodCode =AdmissionMethod.MainCode
	,PatientClassificationCode = PatientClassification.MainCode
	,ManagementIntentionCode = ManagementIntention.MainCode
	,DischargeMethodCode = DischargeMethod.MainCode
	,DischargeDestinationCode = DischargeDestination.MainCode

--OP columns
	,PriorityCode = null
	,ClinicCode = null
	,SourceOfReferralCode = null
	,AppointmentDate = null
	,FirstAttendanceFlag = null
	,DNACode = null
	,AttendanceOutcomeCode = null
	,DisposalCode = null
	,PreviousAppointmentDate = null
	,PreviousAppointmentStatusCode = null

--IP Waiting List columns
	,FuturePatientCancelDate = null

--OP Waiting List columns
	,CountOfDNAs = null
	,CountOfPatientCancels = null
	,NextAppointmentDate = null
	,LastDnaOrPatientCancelledDate = null
	,DateOnFutureOPWaitingList = null

	,PASPrimaryOperationCode = 
		Operation.ClinicalCodingCode

from
	WH.APC.Encounter Encounter

left join WH.RF.Encounter RFEncounter
on	RFEncounter.SourceUniqueID = Encounter.ReferralSourceUniqueID

left join WH.PAS.Consultant Consultant
on	Consultant.ConsultantCode = Encounter.ConsultantCode

left join WH.dbo.EntityXref ConsultantSpecialtyMap
on	ConsultantSpecialtyMap.EntityCode = Consultant.NationalConsultantCode
and	ConsultantSpecialtyMap.EntityTypeCode = 'CONSULTANTSPECIALTYMAP'
and	ConsultantSpecialtyMap.XrefEntityTypeCode = 'CONSULTANTSPECIALTYMAP'

inner join WH.APC.Breach Breach
on	Breach.SourceUniqueID = Encounter.SourceUniqueID

left join WH.PAS.ReferenceValue Sex
on	Sex.ReferenceValueCode = Encounter.SexCode

left join WH.PAS.Gp Gp
on	Gp.GpCode = Encounter.RegisteredGpCode

left join WH.PAS.ReferenceValue AdminCategory
on	AdminCategory.ReferenceValueCode = Encounter.AdminCategoryCode

left join WH.PAS.Organisation StartSite
on	StartSite.OrganisationCode = Encounter.StartSiteCode

left join WH.PAS.ServicePoint StartWard
on	StartWard.ServicePointCode = Encounter.StartWardTypeCode

left join WH.PAS.Organisation EndSite
on	EndSite.OrganisationCode = Encounter.EndSiteCode

left join WH.PAS.ServicePoint EndWard
on	EndWard.ServicePointCode = Encounter.EndWardTypeCode

left join WH.PAS.ReferenceValue AdmissionMethod
on	AdmissionMethod.ReferenceValueCode = Encounter.AdmissionMethodCode

left join WH.PAS.ReferenceValue PatientClassification
on	PatientClassification.ReferenceValueCode = Encounter.PatientClassificationCode

left join WH.PAS.ReferenceValue ManagementIntention
on	ManagementIntention.ReferenceValueCode = Encounter.ManagementIntentionCode

left join WH.PAS.ReferenceValue DischargeMethod
on	DischargeMethod.ReferenceValueCode = Encounter.DischargeMethodCode

left join WH.PAS.ReferenceValue DischargeDestination
on	DischargeDestination.ReferenceValueCode = Encounter.DischargeDestinationCode

left join WH.PAS.ClinicalCoding Operation
on	Operation.SourceRecno = Encounter.SourceUniqueID
and	Operation.SourceCode = 'PRCAE'
and	Operation.ClinicalCodingTypeCode = 'PROCE'
and	Operation.SortOrder = 1
and	not exists
	(
	select
		1
	from
		WH.PAS.ClinicalCoding Previous
	where
		Previous.SourceRecno = Encounter.SourceUniqueID
	and	Previous.SourceCode = 'PRCAE'
	and	Previous.ClinicalCodingTypeCode = 'PROCE'
	and	Previous.SortOrder = 1
	and	Previous.SourceUniqueID > Operation.SourceUniqueID
	)

left join WH.RTT.RTTStatus CurrentRTTStatus
on	CurrentRTTStatus.InternalCode = Encounter.RTTCurrentStatusCode

left join WH.RTT.RTTStatus PeriodRTTStatus
on	PeriodRTTStatus.InternalCode = Encounter.RTTPeriodStatusCode

where
	Encounter.AdmissionTime = Encounter.EpisodeStartTime

and	Encounter.AdmissionMethodCode in 
	(
	 2003470	--Elective
	,8811		--Elective - Booked
	,8810		--Elective - Waiting List
	,13			--Not Specified
	,2003472	--OTHER
	)

and	not exists
	(
	select
		1
	from
		WH.WH.DiagnosticProcedure DiagnosticProcedure
	where
		DiagnosticProcedure.ProcedureCode = Encounter.PrimaryOperationCode
	)

and	coalesce(
		 ConsultantSpecialtyMap.XrefEntityCode
		,Encounter.SpecialtyCode
	) not in
	(
	 48			--Obstetrics	501
	,2000852	--Obstetrics	501
	,10000170	--ANTE-NATAL	501B
	,10000171	--POST-NATAL	501C
	,2000855	--Midwife Episode	560
	,2000661	--Midwife Episode	560
	,10000175	--MIDWIFE-EPI WELL BAB	560W
	)

and	(
		Consultant.ProfessionalCarerTypeCode = 1130	--Consultant
	or	(
			Consultant.ProfessionalCarerTypeCode = 1136	--Nurse
		and	Consultant.ConsultantCode = 10539696	--Nurse Led Plastics
		)
	)

--added 5 Feb 2010 - CBH / PN
and	coalesce(
		PeriodRTTStatus.ClockStopFlag

		,case
		when
			Encounter.RTTPeriodStatusCode is null
		and	Encounter.EpisodeOutcomeCode <> 409	--Discharged - No Treatment - Return to WL
		and	Encounter.DischargeDate is not null
		then 'true'
		else 'false'
		end

		,'true'
	) = 'true'


--remove internal referrals
and	RFEncounter.ParentSourceUniqueID is null

--remove 98's
and	not exists
	(
	select
		1
	from
		WH.RTT.RTT RTT
	where
		RTT.ReferralSourceUniqueID = RFEncounter.SourceUniqueID
	and	RTT.StartDate < @to
	and	RTT.RTTStatusCode = 3007094 --98 Not applicable to RTT
	and	not exists
		(
		select
			1
		from
			WH.RTT.RTT Previous
		where
			Previous.ReferralSourceUniqueID = RFEncounter.SourceUniqueID
		and	Previous.StartDate < @to
		and	(
				Previous.StartDate < RTT.StartDate
			or	(
					Previous.StartDate = RTT.StartDate
				and	Previous.SourceUniqueID < RTT.SourceUniqueID
				)
			)
		)
	)

and	Encounter.AdmissionDate between @from and @to

select @RowsInserted = @RowsInserted + @@rowcount

print 'Treated admitted rows inserted ' + CONVERT(varchar(10), @RowsInserted)

--treated non-admitted

--RTT Rules:
--disposal causes a clock stop
--attended

--non-RTT patients to be added - Clare to identify
--non-RTT specialties to be added - Clare to identify

insert into dbo.BaseRTT
(
	 CensusDate
	,SourceEncounterRecno
	,SourceCode
	,PathwayStatusCode
	,KeyDate
	,ClockStartDate
	,ClockStopDate
	,BreachDate
	,RTTCurrentStatusCode
	,RTTPeriodStatusCode
	,RTTCurrentStatusDate
	,Cases
	,ReferralDate
	,OriginalProviderReferralDate
	,SocialSuspensionDays
	,MappedSpecialtyCode
	,ReferralSourceUniqueID
	,WaitingListSourceUniqueID
	,SourcePatientNo
	,DistrictNo
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DateOnWaitingList
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,ConsultantCode
	,SpecialtyCode
	,AdminCategoryCode
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,PrimaryOperationDate
	,RTTPathwayID
	,AdmissionDate
	,DischargeDate
	,StartSiteCode
	,StartWardTypeCode
	,EndSiteCode
	,EndWardTypeCode
	,AdmissionMethodCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,PriorityCode
	,ClinicCode
	,SourceOfReferralCode
	,AppointmentDate
	,FirstAttendanceFlag
	,DNACode
	,AttendanceOutcomeCode
	,DisposalCode
	,PreviousAppointmentDate
	,PreviousAppointmentStatusCode
	,FuturePatientCancelDate
	,CountOfDNAs
	,CountOfPatientCancels
	,NextAppointmentDate
	,LastDnaOrPatientCancelledDate
	,DateOnFutureOPWaitingList
	,PASPrimaryOperationCode
)
select
	 CensusDate = @census
	,SourceEncounterRecno = Encounter.EncounterRecno
	,SourceCode = 'OP'

	,PathwayStatusCode =
		case
		when Encounter.DisposalCode = 3855 --discharged
		then 'OPD' --Non-Admitted Discharged
		else 'OPT' --Non-Admitted Treated
		end

	,KeyDate = Encounter.AppointmentDate

	,ClockStartDate =
		coalesce(
--most recent DNA appointment date
			(
			select
				 ClockStartDate = max(LastDNA.AppointmentDate)
			from
				WH.OP.Encounter LastDNA
			where
				LastDNA.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
			and	LastDNA.AppointmentDate <= Encounter.AppointmentDate
			and	LastDNA.DNACode = '3'
			)

--get most recent RTT clock start
			,(
			select
				 ClockStartDate = max(RTT.StartDate)
			from
				WH.RTT.RTT

			inner join WH.RTT.RTTStatus RTTStatus
			on	RTTStatus.InternalCode = RTT.RTTStatusCode

			where
				RTTStatus.ClockStartFlag = 'true'
			and	RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
			and	RTT.StartDate < Encounter.AppointmentDate
			)

			,case
			when datepart(year, Encounter.ReferralDate) = 1948
			then null
			else Encounter.ReferralDate
			end
		)

	,ClockStopDate = Encounter.AppointmentDate

	,BreachDate =
		dateadd(
			day
			,(select NumericValue from WH.dbo.Parameter where Parameter = 'RTTBREACHDAYS')
			,coalesce(
--most recent DNA appointment date
				(
				select
					 ClockStartDate = max(LastDNA.AppointmentDate)
				from
					WH.OP.Encounter LastDNA
				where
					LastDNA.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
				and	LastDNA.AppointmentDate <= Encounter.AppointmentDate
				and	LastDNA.DNACode = '3'
				)

	--get most recent RTT clock start
				,(
				select
					 ClockStartDate = max(RTT.StartDate)
				from
					WH.RTT.RTT

				inner join WH.RTT.RTTStatus RTTStatus
				on	RTTStatus.InternalCode = RTT.RTTStatusCode

				where
					RTTStatus.ClockStartFlag = 'true'
				and	RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
				and	RTT.StartDate < Encounter.AppointmentDate
				)

				,case
				when datepart(year, Encounter.ReferralDate) = 1948
				then null
				else Encounter.ReferralDate
				end
			)
		)

	,RTTCurrentStatusCode = CurrentRTTStatus.NationalRTTStatusCode
	,RTTPeriodStatusCode = PeriodRTTStatus.NationalRTTStatusCode

	,Encounter.RTTCurrentStatusDate

	,Cases = 1

	,ReferralDate =
		case
		when datepart(year, Encounter.ReferralDate) = 1948
		then null
		else Encounter.ReferralDate
		end

	,OriginalProviderReferralDate =
		case
		when datepart(year, RFEncounter.OriginalProviderReferralDate) <= 1948
		then null
		else RFEncounter.OriginalProviderReferralDate
		end

	,SocialSuspensionDays = 0

	,MappedSpecialtyCode = ConsultantSpecialtyMap.XrefEntityCode

	,Encounter.ReferralSourceUniqueID
	,Encounter.WaitingListSourceUniqueID
	,Encounter.SourcePatientNo
	,Encounter.DistrictNo
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath
	,SexCode = Sex.MainCode
	,Encounter.NHSNumber
	,Encounter.Postcode
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,DateOnWaitingList = null
	,RegisteredGpCode = Gp.NationalGpCode
	,Encounter.RegisteredGpPracticeCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,AdminCategoryCode = AdminCategory.MainCode
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.PrimaryOperationDate
	,Encounter.RTTPathwayID

--IP columns
	,AdmissionDate = null
	,DischargeDate = null
	,StartSiteCode = Site.OrganisationLocalCode
	,StartWardTypeCode = null
	,EndSiteCode = null
	,EndWardTypeCode = null
	,AdmissionMethodCode = null
	,PatientClassificationCode = null
	,ManagementIntentionCode = null
	,DischargeMethodCode = null
	,DischargeDestinationCode = null

--OP columns
	,PriorityCode = Priority.MainCode
	,ClinicCode = Encounter.ClinicCode
	,SourceOfReferralCode = SourceOfReferral.MainCode
	,AppointmentDate = Encounter.AppointmentDate
	,FirstAttendanceFlag = FirstAttendanceFlag.MainCode
	,DNACode = Encounter.DNACode
	,AttendanceOutcomeCode = AttendanceOutcome.MainCode
	,DisposalCode = Disposal.DisposalLocalCode
	,PreviousAppointmentDate = null
	,PreviousAppointmentStatusCode = null

--IP Waiting List columns
	,FuturePatientCancelDate = null

--OP Waiting List columns
	,CountOfDNAs = null
	,CountOfPatientCancels = null
	,NextAppointmentDate = null
	,LastDnaOrPatientCancelledDate = null
	,DateOnFutureOPWaitingList = null

	,PASPrimaryOperationCode = 
		Operation.ClinicalCodingCode
from
	WH.OP.Encounter Encounter

left join WH.RF.Encounter RFEncounter
on	RFEncounter.SourceUniqueID = Encounter.ReferralSourceUniqueID

inner join WH.RTT.Disposal
on	Disposal.DisposalCode = Encounter.DisposalCode

left join WH.PAS.Consultant Consultant
on	Consultant.ConsultantCode = Encounter.ConsultantCode

left join WH.dbo.EntityXref ConsultantSpecialtyMap
on	ConsultantSpecialtyMap.EntityCode = Consultant.NationalConsultantCode
and	ConsultantSpecialtyMap.EntityTypeCode = 'CONSULTANTSPECIALTYMAP'
and	ConsultantSpecialtyMap.XrefEntityTypeCode = 'CONSULTANTSPECIALTYMAP'

left join WH.PAS.ReferenceValue Sex
on	Sex.ReferenceValueCode = Encounter.SexCode

left join WH.PAS.Gp Gp
on	Gp.GpCode = Encounter.RegisteredGpCode

left join WH.PAS.ReferenceValue AdminCategory
on	AdminCategory.ReferenceValueCode = Encounter.AdminCategoryCode

left join WH.PAS.Organisation Site
on	Site.OrganisationCode = Encounter.SiteCode

left join WH.PAS.ReferenceValue SourceOfReferral
on	SourceOfReferral.ReferenceValueCode = Encounter.SourceOfReferralCode

left join WH.PAS.ReferenceValue Priority
on	Priority.ReferenceValueCode = Encounter.PriorityCode

left join WH.PAS.ReferenceValue FirstAttendanceFlag
on	FirstAttendanceFlag.ReferenceValueCode = Encounter.FirstAttendanceFlag

left join WH.PAS.ReferenceValue AttendanceOutcome
on	AttendanceOutcome.ReferenceValueCode = Encounter.AttendanceOutcomeCode

--left join WH.RTT.RTT LastClockStart
--on	LastClockStart.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
--and	LastClockStart.StartDate < Encounter.AppointmentDate
--and	LastClockStart.RTTStatusCode in
--	(
--	select
--		RTTStatus.InternalCode
--	from
--		WH.RTT.RTTStatus
--	where
--		RTTStatus.ClockStartFlag = 'true'
--	)
--and	not exists
--	(
--	select
--		1
--	from
--		WH.RTT.RTT Previous
--	where
--		Previous.ReferralSourceUniqueID = LastClockStart.ReferralSourceUniqueID
--	and	Previous.RTTStatusCode in
--		(
--		select
--			RTTStatus.InternalCode
--		from
--			WH.RTT.RTTStatus
--		where
--			RTTStatus.ClockStartFlag = 'true'
--		)
--	and	(
--			Previous.StartDate > LastClockStart.StartDate
--		or	(
--				Previous.StartDate = LastClockStart.StartDate
--			and	Previous.SourceUniqueID > LastClockStart.SourceUniqueID
--			)
--		)
--	)

left join WH.RTT.RTT LastClockStop
on	LastClockStop.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
and	LastClockStop.StartDate < Encounter.AppointmentDate
and	LastClockStop.RTTStatusCode in
	(
	select
		RTTStatus.InternalCode
	from
		WH.RTT.RTTStatus
	where
		RTTStatus.ClockStopFlag = 'true'
	)
and	not exists
	(
	select
		1
	from
		WH.RTT.RTT Previous
	where
		Previous.ReferralSourceUniqueID = LastClockStop.ReferralSourceUniqueID
	and	Previous.RTTStatusCode in
		(
		select
			RTTStatus.InternalCode
		from
			WH.RTT.RTTStatus
		where
			RTTStatus.ClockStopFlag = 'true'
		)
	and	(
			Previous.StartDate > LastClockStop.StartDate
		or	(
				Previous.StartDate = LastClockStop.StartDate
			and	Previous.SourceUniqueID > LastClockStop.SourceUniqueID
			)
		)
	)

left join WH.PAS.ClinicalCoding Operation
on	Operation.SourceRecno = Encounter.SourceUniqueID
and	Operation.SourceCode = 'SCHDL'
and	Operation.ClinicalCodingTypeCode = 'PROCE'
and	Operation.SortOrder = 1
and	not exists
	(
	select
		1
	from
		WH.PAS.ClinicalCoding Previous
	where
		Previous.SourceRecno = Encounter.SourceUniqueID
	and	Previous.SourceCode = 'SCHDL'
	and	Previous.ClinicalCodingTypeCode = 'PROCE'
	and	Previous.SortOrder = 1
	and	Previous.SourceUniqueID > Operation.SourceUniqueID
	)

left join WH.PAS.RTTStatus CurrentRTTStatus
on	CurrentRTTStatus.RTTStatusCode = Encounter.RTTCurrentStatusCode

left join WH.PAS.RTTStatus PeriodRTTStatus
on	PeriodRTTStatus.RTTStatusCode = Encounter.RTTPeriodStatusCode

where
	Encounter.AppointmentDate between @from and @to
and	Encounter.AppointmentStatusCode in
	(
	 357		--Attended On Time
	,2868		--Patient Late / Seen
	,2004151	--Attended
	)
and	(
	not exists 
		(
		select
			1
		from
			EntityLookup ExcludedClinic
		where
			Encounter.ClinicCode = ExcludedClinic.Description
		and ExcludedClinic.EntityTypeCode = 'RTTPREOPCLINIC'
		and Encounter.DisposalCode in (3855,2003638)
		)
	and
		(
			Disposal.ClockStopFlag = 'true'
		or	exists
			(
			select
				1
			from
				WH.RTT.RTT RTT
			where
				RTT.SourceRecno = Encounter.SourceUniqueID
			and	RTT.SourceCode = 'SCHDL'
			and	RTT.RTTStatusCode in
				(
				select
					RTTStatus.InternalCode
				from
					WH.RTT.RTTStatus
				where
					RTTStatus.ClockStopFlag = 'true'
				)
			)
		or	Encounter.SpecialtyCode in --always treated on 1st appointment
			(
			 2000950	--Anticoagulant Service (324)
			,10000159	--HAEM-ANTICOAGULANT (303B)
			)
		)	
	)

--and the date of the latest clock start was after the date of the latest clock stop (where recorded)
--and	(
--		LastClockStart.StartDate > LastClockStop.StartDate
--	or	LastClockStart.StartDate is null
--	or	LastClockStop.StartDate is null
--	)

--no previous clock stop
and	LastClockStop.StartDate is null

--remove 98's
and	not exists
	(
	select
		1
	from
		WH.RTT.RTT RTT
	where
		RTT.ReferralSourceUniqueID = RFEncounter.SourceUniqueID
	and	RTT.StartDate < @to
	and	RTT.RTTStatusCode = 3007094 --98 Not applicable to RTT
	and	not exists
		(
		select
			1
		from
			WH.RTT.RTT Previous
		where
			Previous.ReferralSourceUniqueID = RFEncounter.SourceUniqueID
		and	Previous.StartDate < @to
		and	(
				Previous.StartDate < RTT.StartDate
			or	(
					Previous.StartDate = RTT.StartDate
				and	Previous.SourceUniqueID < RTT.SourceUniqueID
				)
			)
		)
	)

--remove internal referrals
and	RFEncounter.ParentSourceUniqueID is null


select @RowsInserted = @RowsInserted + @@rowcount
print 'Treated non admitted rows inserted ' + CONVERT(varchar(10), @@rowcount)

--I/P waiters

insert into dbo.BaseRTT
(
	 CensusDate
	,SourceEncounterRecno
	,SourceCode
	,PathwayStatusCode
	,KeyDate
	,ClockStartDate
	,ClockStopDate
	,BreachDate
	,RTTCurrentStatusCode
	,RTTPeriodStatusCode
	,RTTCurrentStatusDate
	,Cases
	,ReferralDate
	,OriginalProviderReferralDate
	,SocialSuspensionDays
	,MappedSpecialtyCode
	,ReferralSourceUniqueID
	,WaitingListSourceUniqueID
	,SourcePatientNo
	,DistrictNo
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DateOnWaitingList
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,ConsultantCode
	,SpecialtyCode
	,AdminCategoryCode
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,PrimaryOperationDate
	,RTTPathwayID
	,AdmissionDate
	,DischargeDate
	,StartSiteCode
	,StartWardTypeCode
	,EndSiteCode
	,EndWardTypeCode
	,AdmissionMethodCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,PriorityCode
	,ClinicCode
	,SourceOfReferralCode
	,AppointmentDate
	,FirstAttendanceFlag
	,DNACode
	,AttendanceOutcomeCode
	,DisposalCode
	,PreviousAppointmentDate
	,PreviousAppointmentStatusCode
	,FuturePatientCancelDate
	,CountOfDNAs
	,CountOfPatientCancels
	,NextAppointmentDate
	,LastDnaOrPatientCancelledDate
	,DateOnFutureOPWaitingList
	,PASPrimaryOperationCode
)
select
	 CensusDate = @census
	,SourceEncounterRecno = Encounter.EncounterRecno
	,SourceCode = 'APCWL'

	,PathwayStatusCode = 'IPW' --inpatient waiter

	,KeyDate = Encounter.CensusDate

	,ClockStartDate = Breach.ClockStartDate

	,ClockStopDate = null

	,Breach.RTTBreachDate

	,RTTCurrentStatusCode = CurrentRTTStatus.NationalRTTStatusCode
	,RTTPeriodStatusCode = null
	,Encounter.RTTCurrentStatusDate

	,Cases = 1

	,ReferralDate =
		case
		when datepart(year, Encounter.ReferralDate) = 1948
		then null
		else Encounter.ReferralDate
		end

	,OriginalProviderReferralDate =
		case
		when datepart(year, RFEncounter.OriginalProviderReferralDate) <= 1948
		then null
		else RFEncounter.OriginalProviderReferralDate
		end

	,SocialSuspensionDays = Breach.SocialSuspensionDays

	,MappedSpecialtyCode = ConsultantSpecialtyMap.XrefEntityCode

	,Encounter.ReferralSourceUniqueID
	,WaitingListSourceUniqueID = Encounter.SourceUniqueID
	,Encounter.SourcePatientNo
	,Encounter.DistrictNo
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath
	,SexCode = Sex.MainCode
	,Encounter.NHSNumber
	,Encounter.Postcode
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.DateOnWaitingList
	,RegisteredGpCode = Gp.NationalGpCode
	,Encounter.RegisteredGpPracticeCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,AdminCategoryCode = AdminCategory.MainCode
	,PrimaryDiagnosisCode = null
	,Encounter.IntendedPrimaryOperationCode
	,PrimaryOperationDate = Encounter.ProcedureTime
	,Encounter.RTTPathwayID

--IP columns
	,AdmissionDate = null
	,DischargeDate = null
	,StartSiteCode = Site.OrganisationLocalCode
	,StartWardTypeCode = null
	,EndSiteCode = null
	,EndWardTypeCode = null
	,AdmissionMethodCode = null
	,PatientClassificationCode = null
	,ManagementIntentionCode = null
	,DischargeMethodCode = null
	,DischargeDestinationCode = null

--OP columns
	,PriorityCode = null
	,ClinicCode = null
	,SourceOfReferralCode = null
	,AppointmentDate = null
	,FirstAttendanceFlag = null
	,DNACode = null
	,AttendanceOutcomeCode = null
	,DisposalCode = null
	,PreviousAppointmentDate = null
	,PreviousAppointmentStatusCode = null

--IP Waiting List columns
	,Encounter.FuturePatientCancelDate

--OP Waiting List columns
	,CountOfDNAs = null
	,CountOfPatientCancels = null
	,NextAppointmentDate = null
	,LastDnaOrPatientCancelledDate = null
	,DateOnFutureOPWaitingList = null

	,PASPrimaryOperationCode = 
		Operation.ClinicalCodingCode

from
	WH.APC.WaitingList Encounter

inner join WH.APC.WaitingListBreach Breach
on	Breach.SourceUniqueID = Encounter.SourceUniqueID
and	Breach.CensusDate = Encounter.CensusDate

LEFT join WH.RF.Encounter RFEncounter
on	RFEncounter.SourceUniqueID = Encounter.ReferralSourceUniqueID

left join WH.PAS.Consultant Consultant
on	Consultant.ConsultantCode = Encounter.ConsultantCode

left join WH.dbo.EntityXref ConsultantSpecialtyMap
on	ConsultantSpecialtyMap.EntityCode = Consultant.NationalConsultantCode
and	ConsultantSpecialtyMap.EntityTypeCode = 'CONSULTANTSPECIALTYMAP'
and	ConsultantSpecialtyMap.XrefEntityTypeCode = 'CONSULTANTSPECIALTYMAP'

left join WH.PAS.ReferenceValue Sex
on	Sex.ReferenceValueCode = Encounter.SexCode

left join WH.PAS.Gp Gp
on	Gp.GpCode = Encounter.RegisteredGpCode

left join WH.PAS.ReferenceValue AdminCategory
on	AdminCategory.ReferenceValueCode = Encounter.AdminCategoryCode

left join WH.PAS.Organisation Site
on	Site.OrganisationCode = Encounter.SiteCode

left join WH.PAS.ClinicalCoding Operation
on	Operation.SourceRecno = Encounter.SourceUniqueID
and	Operation.SourceCode = 'WLIST'
and	Operation.ClinicalCodingTypeCode = 'PROCE'
and	Operation.SortOrder = 1
and	not exists
	(
	select
		1
	from
		WH.PAS.ClinicalCoding Previous
	where
		Previous.SourceRecno = Encounter.SourceUniqueID
	and	Previous.SourceCode = 'WLIST'
	and	Previous.ClinicalCodingTypeCode = 'PROCE'
	and	Previous.SortOrder = 1
	and	Previous.SourceUniqueID > Operation.SourceUniqueID
	)

left join WH.PAS.RTTStatus CurrentRTTStatus
on	CurrentRTTStatus.RTTStatusCode = Encounter.RTTCurrentStatusCode

where
	Encounter.CensusDate =
		(
		select
			max(CensusDate)
		from
			WH.APC.WaitingList WaitingList
		where
			WaitingList.CensusDate <= @to
		)

and	not exists
	(
	select
		1
	from
		WH.WH.DiagnosticProcedure DiagnosticProcedure
	where
		DiagnosticProcedure.ProcedureCode = Encounter.IntendedPrimaryOperationCode
	and	upper(Encounter.GeneralComment) not like '%$T%'
	)

and Encounter.AdmissionMethodCode <> 8812 --remove planned

--remove internal referrals
and	RFEncounter.ParentSourceUniqueID is null

--remove 98's
and	not exists
	(
	select
		1
	from
		WH.RTT.RTT RTT
	where
		RTT.ReferralSourceUniqueID = RFEncounter.SourceUniqueID
	and	RTT.StartDate < @to
	and	RTT.RTTStatusCode = 3007094 --98 Not applicable to RTT
	and	not exists
		(
		select
			1
		from
			WH.RTT.RTT Previous
		where
			Previous.ReferralSourceUniqueID = RFEncounter.SourceUniqueID
		and	Previous.StartDate < @to
		and	(
				Previous.StartDate < RTT.StartDate
			or	(
					Previous.StartDate = RTT.StartDate
				and	Previous.SourceUniqueID < RTT.SourceUniqueID
				)
			)
		)
	)


select @RowsInserted = @RowsInserted + @@rowcount
print 'IP waiters rows inserted ' + CONVERT(varchar(10), @@rowcount)

--O/P waiters

insert into dbo.BaseRTT
(
	 CensusDate
	,SourceEncounterRecno
	,SourceCode
	,PathwayStatusCode
	,KeyDate
	,ClockStartDate
	,ClockStopDate
	,BreachDate
	,RTTCurrentStatusCode
	,RTTPeriodStatusCode
	,RTTCurrentStatusDate
	,Cases
	,ReferralDate
	,OriginalProviderReferralDate
	,SocialSuspensionDays
	,MappedSpecialtyCode
	,ReferralSourceUniqueID
	,WaitingListSourceUniqueID
	,SourcePatientNo
	,DistrictNo
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DateOnWaitingList
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,ConsultantCode
	,SpecialtyCode
	,AdminCategoryCode
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,PrimaryOperationDate
	,RTTPathwayID
	,AdmissionDate
	,DischargeDate
	,StartSiteCode
	,StartWardTypeCode
	,EndSiteCode
	,EndWardTypeCode
	,AdmissionMethodCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,PriorityCode
	,ClinicCode
	,SourceOfReferralCode
	,AppointmentDate
	,FirstAttendanceFlag
	,DNACode
	,AttendanceOutcomeCode
	,DisposalCode
	,PreviousAppointmentDate
	,PreviousAppointmentStatusCode
	,FuturePatientCancelDate
	,CountOfDNAs
	,CountOfPatientCancels
	,NextAppointmentDate
	,LastDnaOrPatientCancelledDate
	,DateOnFutureOPWaitingList
	,PASPrimaryOperationCode
)
select
	 CensusDate = @census
	,SourceEncounterRecno = Encounter.EncounterRecno
	,SourceCode = 'OPWL'

	,PathwayStatusCode = 'OPW' --outpatient waiter

	,KeyDate = Encounter.CensusDate

	,ClockStartDate =
--get most recent RTT clock start
		coalesce(
			(
			select
				 ClockStartDate = max(RTT.StartDate)
			from
				WH.RTT.RTT

			inner join WH.RTT.RTTStatus RTTStatus
			on	RTTStatus.InternalCode = RTT.RTTStatusCode

			where
				RTTStatus.ClockStartFlag = 'true'
			and	RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
			and	RTT.StartDate < Encounter.CensusDate
			)

			,case
			when datepart(year, Encounter.ReferralDate) = 1948
			then null
			else Encounter.ReferralDate
			end
		)

	,ClockStopDate = null

	,BreachDate =
		dateadd(
			day
			,(select NumericValue from WH.dbo.Parameter where Parameter = 'RTTBREACHDAYS')
	--get most recent RTT clock start
			,coalesce(
				(
				select
					 ClockStartDate = max(RTT.StartDate)
				from
					WH.RTT.RTT

				inner join WH.RTT.RTTStatus RTTStatus
				on	RTTStatus.InternalCode = RTT.RTTStatusCode

				where
					RTTStatus.ClockStartFlag = 'true'
				and	RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
				and	RTT.StartDate < Encounter.CensusDate
				)

				,case
				when datepart(year, Encounter.ReferralDate) = 1948
				then null
				else Encounter.ReferralDate
				end
			)
		)

	,RTTCurrentStatusCode = CurrentRTTStatus.NationalRTTStatusCode
	,RTTPeriodStatusCode = null
	,Encounter.RTTCurrentStatusDate

	,Cases = 1

	,ReferralDate =
		case
		when datepart(year, Encounter.ReferralDate) = 1948
		then null
		else Encounter.ReferralDate
		end

	,OriginalProviderReferralDate =
		case
		when datepart(year, RFEncounter.OriginalProviderReferralDate) <= 1948
		then null
		else RFEncounter.OriginalProviderReferralDate
		end

	,SocialSuspensionDays = 0

	,MappedSpecialtyCode = ConsultantSpecialtyMap.XrefEntityCode

	,Encounter.ReferralSourceUniqueID
	,WaitingListSourceUniqueID = Encounter.SourceUniqueID
	,Encounter.SourcePatientNo
	,Encounter.DistrictNo
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath
	,SexCode = Sex.MainCode
	,Encounter.NHSNumber
	,Encounter.Postcode
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,DateOnWaitingList = null
	,RegisteredGpCode = Gp.NationalGpCode
	,Encounter.RegisteredGpPracticeCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,AdminCategoryCode = AdminCategory.MainCode
	,PrimaryDiagnosisCode = null
	,PrimaryOperationCode = Encounter.IntendedPrimaryOperationCode
	,PrimaryOperationDate = Encounter.AppointmentTime
	,Encounter.RTTPathwayID

--IP columns
	,AdmissionDate = null
	,DischargeDate = null
	,StartSiteCode = Site.OrganisationLocalCode
	,StartWardTypeCode = null
	,EndSiteCode = null
	,EndWardTypeCode = null
	,AdmissionMethodCode = null
	,PatientClassificationCode = null
	,ManagementIntentionCode = null
	,DischargeMethodCode = null
	,DischargeDestinationCode = null

--OP columns
	,PriorityCode = Encounter.PriorityCode
	,ClinicCode = Encounter.ClinicCode
	,SourceOfReferralCode = Encounter.SourceOfReferralCode
	,AppointmentDate = Encounter.AppointmentDate
	,FirstAttendanceFlag = null
	,DNACode = null
	,AttendanceOutcomeCode = null
	,DisposalCode = null
	,PreviousAppointmentDate = null
	,PreviousAppointmentStatusCode = null

--IP Waiting List columns
	,FuturePatientCancelDate = null

--OP Waiting List columns
	,CountOfDNAs = Encounter.CountOfDNAs
	,CountOfPatientCancels = Encounter.CountOfPatientCancels
	,NextAppointmentDate = null
	,LastDnaOrPatientCancelledDate = null
	,DateOnFutureOPWaitingList = null

	,PASPrimaryOperationCode = 
		Operation.ClinicalCodingCode

from
	WH.OP.WaitingList Encounter

left join WH.RF.Encounter RFEncounter
on	RFEncounter.SourceUniqueID = Encounter.ReferralSourceUniqueID

left join WH.PAS.Consultant Consultant
on	Consultant.ConsultantCode = Encounter.ConsultantCode

--only done for admitted??
left join WH.dbo.EntityXref ConsultantSpecialtyMap
on	ConsultantSpecialtyMap.EntityCode = Consultant.NationalConsultantCode
and	ConsultantSpecialtyMap.EntityTypeCode = 'CONSULTANTSPECIALTYMAP'
and	ConsultantSpecialtyMap.XrefEntityTypeCode = 'CONSULTANTSPECIALTYMAP'

left join WH.PAS.ReferenceValue Sex
on	Sex.ReferenceValueCode = Encounter.SexCode

left join WH.PAS.Gp Gp
on	Gp.GpCode = Encounter.RegisteredGpCode

left join WH.PAS.ReferenceValue AdminCategory
on	AdminCategory.ReferenceValueCode = Encounter.AdminCategoryCode

left join WH.PAS.Organisation Site
on	Site.OrganisationCode = Encounter.SiteCode

left join WH.PAS.ClinicalCoding Operation
on	Operation.SourceRecno = Encounter.SourceUniqueID
and	Operation.SourceCode = 'WLIST'
and	Operation.ClinicalCodingTypeCode = 'PROCE'
and	Operation.SortOrder = 1
and	not exists
	(
	select
		1
	from
		WH.PAS.ClinicalCoding Previous
	where
		Previous.SourceRecno = Encounter.SourceUniqueID
	and	Previous.SourceCode = 'WLIST'
	and	Previous.ClinicalCodingTypeCode = 'PROCE'
	and	Previous.SortOrder = 1
	and	Previous.SourceUniqueID > Operation.SourceUniqueID
	)

left join WH.PAS.RTTStatus CurrentRTTStatus
on	CurrentRTTStatus.RTTStatusCode = Encounter.RTTCurrentStatusCode

where
	Encounter.CensusDate =
		(
		select
			max(CensusDate)
		from
			WH.OP.WaitingList WaitingList
		where
			WaitingList.CensusDate <= @to
		)


----remove any OPs with referrals already accounted for in previous steps
----PDO 20 Mar 2010
--and	not exists
--	(
--	select
--		1
--	from
--		dbo.BaseRTT
--	where
--		BaseRTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
--	and	BaseRTT.CensusDate = @census
--	)

----no clock stops in RTT
--and	not exists
--	(
--	select
--		1
--	from
--		WH.RTT.RTT RTT

--	inner join WH.RTT.RTTStatus RTTStatus
--	on	RTTStatus.InternalCode = RTT.RTTStatusCode
--	and	RTTStatus.ClockStopFlag = 'true'

--	where
--		RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
--	and	RTT.StartDate < @to
--	)

--remove internal referrals
and	RFEncounter.ParentSourceUniqueID is null


--not been treated as an IP ever
and	not exists
	(
	select
		1
	from
		WH.APC.Encounter Inpatient

	left join WH.RTT.RTTStatus PeriodRTTStatus
	on	PeriodRTTStatus.InternalCode = Inpatient.RTTPeriodStatusCode

	where
		Inpatient.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
	and	Inpatient.AdmissionTime = Inpatient.EpisodeStartTime

	and	Inpatient.AdmissionMethodCode in 
		(
		 2003470	--Elective
		,8811		--Elective - Booked
		,8810		--Elective - Waiting List
		,13			--Not Specified
		,2003472	--OTHER
		)

	and	not exists
		(
		select
			1
		from
			WH.WH.DiagnosticProcedure DiagnosticProcedure
		where
			DiagnosticProcedure.ProcedureCode = Inpatient.PrimaryOperationCode
		)

--added 5 Feb 2010 - CBH / PN
	and	coalesce(
			PeriodRTTStatus.ClockStopFlag

			,case
			when
				Inpatient.RTTPeriodStatusCode is null
			and	Inpatient.EpisodeOutcomeCode <> 409	--Discharged - No Treatment - Return to WL
			and	Inpatient.DischargeDate is not null
			then 'true'
			else 'false'
			end

			,'true'
		) = 'true'

	)

--not been treated as an OP ever
and	not exists
	(
	select
		1
	from
		WH.OP.Encounter Outpatient

	inner join WH.RTT.Disposal
	on	Disposal.DisposalCode = Outpatient.DisposalCode

	where
		Outpatient.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
	and	Outpatient.AppointmentStatusCode in
		(
		 357		--Attended On Time
		,2868		--Patient Late / Seen
		,2004151	--Attended
		)
	and	(
		not exists 
			(
			select
				1
			from
				EntityLookup ExcludedClinic
			where
				Outpatient.ClinicCode = ExcludedClinic.Description
			and ExcludedClinic.EntityTypeCode = 'RTTPREOPCLINIC'
			and Disposal.DisposalCode in (3855, 2003638)
			)
		and
			(
				Disposal.ClockStopFlag = 'true'
			or	exists
				(
				select
					1
				from
					WH.RTT.RTT RTT
				where
					RTT.SourceRecno = Encounter.SourceUniqueID
				and	RTT.SourceCode = 'SCHDL'
				and	RTT.RTTStatusCode in
					(
					select
						RTTStatus.InternalCode
					from
						WH.RTT.RTTStatus
					where
						RTTStatus.ClockStopFlag = 'true'
					)
				)
			or	Encounter.SpecialtyCode in --always treated on 1st appointment
				(
				 2000950	--Anticoagulant Service (324)
				,10000159	--HAEM-ANTICOAGULANT (303B)
				)
			)	
		)
	)


--no clock stops in RTT
and	not exists
	(
	select
		1
	from
		WH.RTT.RTT RTT

	inner join WH.RTT.RTTStatus RTTStatus
	on	RTTStatus.InternalCode = RTT.RTTStatusCode
	and	RTTStatus.ClockStopFlag = 'true'

	where
		RTT.ReferralSourceUniqueID = Encounter.ReferralSourceUniqueID
	and	RTT.StartDate < @to
	)


--Waiting List is not Follow Up (added by KD 2011-01-17)
and ISNULL(encounter.WaitingListRule,-1) <> 150000536

-- Specialty not Physio or Midwives (added by KD 2011-01-18)
and not exists 
	(
	Select 
		1 
	from WH.PAS.SpecialtyBase specs
	where Encounter.SpecialtyCode = specs.SPECT_REFNO
	and (Left(specs.MAIN_IDENT,2) ='65' or specs.MAIN_IDENT = '560')
	)

--remove 98's
and	not exists
	(
	select
		1
	from
		WH.RTT.RTT RTT
	where
		RTT.ReferralSourceUniqueID = RFEncounter.SourceUniqueID
	and	RTT.StartDate < @to
	and	RTT.RTTStatusCode = 3007094 --98 Not applicable to RTT
	and	not exists
		(
		select
			1
		from
			WH.RTT.RTT Previous
		where
			Previous.ReferralSourceUniqueID = RFEncounter.SourceUniqueID
		and	Previous.StartDate < @to
		and	(
				Previous.StartDate < RTT.StartDate
			or	(
					Previous.StartDate = RTT.StartDate
				and	Previous.SourceUniqueID < RTT.SourceUniqueID
				)
			)
		)
	)


select @RowsInserted = @RowsInserted + @@rowcount
print 'OP waiters rows inserted ' + CONVERT(varchar(10), @@rowcount)

--All other referrals that we haven't got yet and are still open

insert into dbo.BaseRTT
(
	 CensusDate
	,SourceEncounterRecno
	,SourceCode
	,PathwayStatusCode
	,KeyDate
	,ClockStartDate
	,ClockStopDate
	,BreachDate
	,RTTCurrentStatusCode
	,RTTPeriodStatusCode
	,RTTCurrentStatusDate
	,Cases
	,ReferralDate
	,OriginalProviderReferralDate
	,SocialSuspensionDays
	,MappedSpecialtyCode
	,ReferralSourceUniqueID
	,WaitingListSourceUniqueID
	,SourcePatientNo
	,DistrictNo
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DateOnWaitingList
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,ConsultantCode
	,SpecialtyCode
	,AdminCategoryCode
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,PrimaryOperationDate
	,RTTPathwayID
	,AdmissionDate
	,DischargeDate
	,StartSiteCode
	,StartWardTypeCode
	,EndSiteCode
	,EndWardTypeCode
	,AdmissionMethodCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,PriorityCode
	,ClinicCode
	,SourceOfReferralCode
	,AppointmentDate
	,FirstAttendanceFlag
	,DNACode
	,AttendanceOutcomeCode
	,DisposalCode
	,PreviousAppointmentDate
	,PreviousAppointmentStatusCode
	,FuturePatientCancelDate
	,CountOfDNAs
	,CountOfPatientCancels
	,NextAppointmentDate
	,LastDnaOrPatientCancelledDate
	,DateOnFutureOPWaitingList
	,PASPrimaryOperationCode
)
select
	 CensusDate = @census
	,SourceEncounterRecno = Encounter.EncounterRecno
	,SourceCode = 'RF'

	,PathwayStatusCode = 'P' --Pending

	,KeyDate = @to

	,ClockStartDate =
		coalesce(
			 Encounter.RTTStartDate
			,Encounter.ReferralDate
		)

	,ClockStopDate = null

	,BreachDate =
		dateadd(
			day
			,(select NumericValue from WH.dbo.Parameter where Parameter = 'RTTBREACHDAYS')
	--get most recent RTT clock start
			,coalesce(
				 Encounter.RTTStartDate
				,Encounter.ReferralDate
			)
		)

	,RTTCurrentStatusCode = CurrentRTTStatus.NationalRTTStatusCode
	,RTTPeriodStatusCode = null
	,Encounter.RTTCurrentStatusDate

	,Cases = 1

	,ReferralDate =
		case
		when datepart(year, Encounter.ReferralDate) = 1948
		then null
		else Encounter.ReferralDate
		end

	,OriginalProviderReferralDate =
		case
		when datepart(year, Encounter.OriginalProviderReferralDate) <= 1948
		then null
		else Encounter.OriginalProviderReferralDate
		end

	,SocialSuspensionDays = 0

	,MappedSpecialtyCode = ConsultantSpecialtyMap.XrefEntityCode

	,ReferralSourceUniqueID = Encounter.SourceUniqueID
	,WaitingListSourceUniqueID = null
	,Encounter.SourcePatientNo
	,Encounter.DistrictNo
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath
	,SexCode = Sex.MainCode
	,Encounter.NHSNumber
	,Encounter.Postcode
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,DateOnWaitingList = null
	,RegisteredGpCode = Gp.NationalGpCode
	,Encounter.RegisteredGpPracticeCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,AdminCategoryCode = AdminCategory.MainCode
	,PrimaryDiagnosisCode = null
	,PrimaryOperationCode = null
	,PrimaryOperationDate = null
	,Encounter.RTTPathwayID

--IP columns
	,AdmissionDate = null
	,DischargeDate = null
	,StartSiteCode = Site.OrganisationLocalCode
	,StartWardTypeCode = null
	,EndSiteCode = null
	,EndWardTypeCode = null
	,AdmissionMethodCode = null
	,PatientClassificationCode = null
	,ManagementIntentionCode = null
	,DischargeMethodCode = null
	,DischargeDestinationCode = null

--OP columns
	,PriorityCode = Priority.MainCode
	,ClinicCode = null
	,SourceOfReferralCode = SourceOfReferral.MainCode
	,AppointmentDate = null
	,FirstAttendanceFlag = null
	,DNACode = null
	,AttendanceOutcomeCode = null
	,DisposalCode = null
	,PreviousAppointmentDate = null
	,PreviousAppointmentStatusCode = null

--IP Waiting List columns
	,FuturePatientCancelDate = null

--OP Waiting List columns
	,CountOfDNAs = null
	,CountOfPatientCancels = null
	,NextAppointmentDate = null
	,LastDnaOrPatientCancelledDate = null
	,DateOnFutureOPWaitingList = null

	,PASPrimaryOperationCode = 
		Operation.ClinicalCodingCode

from
	WH.RF.Encounter Encounter

left join WH.PAS.Consultant Consultant
on	Consultant.ConsultantCode = Encounter.ConsultantCode

left join WH.dbo.EntityXref ConsultantSpecialtyMap
on	ConsultantSpecialtyMap.EntityCode = Consultant.NationalConsultantCode
and	ConsultantSpecialtyMap.EntityTypeCode = 'CONSULTANTSPECIALTYMAP'
and	ConsultantSpecialtyMap.XrefEntityTypeCode = 'CONSULTANTSPECIALTYMAP'

left join WH.PAS.ReferenceValue Sex
on	Sex.ReferenceValueCode = Encounter.SexCode

left join WH.PAS.Gp Gp
on	Gp.GpCode = Encounter.RegisteredGpCode

left join WH.PAS.ReferenceValue AdminCategory
on	AdminCategory.ReferenceValueCode = Encounter.AdminCategoryCode

left join WH.PAS.Organisation Site
on	Site.OrganisationCode = Encounter.SiteCode

left join WH.PAS.ReferenceValue SourceOfReferral
on	SourceOfReferral.ReferenceValueCode = Encounter.SourceOfReferralCode

left join WH.PAS.ReferenceValue Priority
on	Priority.ReferenceValueCode = Encounter.PriorityCode

left join WH.PAS.ClinicalCoding Operation
on	Operation.SourceRecno = Encounter.SourceUniqueID
and	Operation.SourceCode = 'REFRL'
and	Operation.ClinicalCodingTypeCode = 'PROCE'
and	Operation.SortOrder = 1
and	not exists
	(
	select
		1
	from
		WH.PAS.ClinicalCoding Previous
	where
		Previous.SourceRecno = Encounter.SourceUniqueID
	and	Previous.SourceCode = 'REFRL'
	and	Previous.ClinicalCodingTypeCode = 'PROCE'
	and	Previous.SortOrder = 1
	and	Previous.SourceUniqueID > Operation.SourceUniqueID
	)

left join WH.PAS.RTTStatus CurrentRTTStatus
on	CurrentRTTStatus.RTTStatusCode = Encounter.RTTCurrentStatusCode

where
--not been treated as an IP ever
	not exists
	(
	select
		1
	from
		WH.APC.Encounter Inpatient

	left join WH.RTT.RTTStatus PeriodRTTStatus
	on	PeriodRTTStatus.InternalCode = Inpatient.RTTPeriodStatusCode

	where
		Inpatient.ReferralSourceUniqueID = Encounter.SourceUniqueID
	and	Inpatient.AdmissionTime = Inpatient.EpisodeStartTime

	and	Inpatient.AdmissionMethodCode in 
		(
		 2003470	--Elective
		,8811		--Elective - Booked
		,8810		--Elective - Waiting List
		,13			--Not Specified
		,2003472	--OTHER
		)

	and	not exists
		(
		select
			1
		from
			WH.WH.DiagnosticProcedure DiagnosticProcedure
		where
			DiagnosticProcedure.ProcedureCode = Inpatient.PrimaryOperationCode
		)

--added 5 Feb 2010 - CBH / PN
	and	coalesce(
			PeriodRTTStatus.ClockStopFlag

			,case
			when
				Inpatient.RTTPeriodStatusCode is null
			and	Inpatient.EpisodeOutcomeCode <> 409	--Discharged - No Treatment - Return to WL
			and	Inpatient.DischargeDate is not null
			then 'true'
			else 'false'
			end

			,'true'
		) = 'true'

	)

--not been treated as an OP ever
and	not exists
	(
	select
		1
	from
		WH.OP.Encounter Outpatient

	inner join WH.RTT.Disposal
	on	Disposal.DisposalCode = Outpatient.DisposalCode

	where
		Outpatient.ReferralSourceUniqueID = Encounter.SourceUniqueID
	and	Outpatient.AppointmentStatusCode in
		(
		 357		--Attended On Time
		,2868		--Patient Late / Seen
		,2004151	--Attended
		)
	and	(
		not exists 
			(
			select
				1
			from
				EntityLookup ExcludedClinic
			where
				Outpatient.ClinicCode = ExcludedClinic.Description
			and ExcludedClinic.EntityTypeCode = 'RTTPREOPCLINIC'
			and Disposal.DisposalCode in (3855, 2003638)
			)
		and
			(
				Disposal.ClockStopFlag = 'true'
			or	exists
				(
				select
					1
				from
					WH.RTT.RTT RTT
				where
					RTT.SourceRecno = Encounter.SourceUniqueID
				and	RTT.SourceCode = 'SCHDL'
				and	RTT.RTTStatusCode in
					(
					select
						RTTStatus.InternalCode
					from
						WH.RTT.RTTStatus
					where
						RTTStatus.ClockStopFlag = 'true'
					)
				)
			or	Encounter.SpecialtyCode in --always treated on 1st appointment
				(
				 2000950	--Anticoagulant Service (324)
				,10000159	--HAEM-ANTICOAGULANT (303B)
				)
			)	
		)
	)


--no clock stops in RTT
and	not exists
	(
	select
		1
	from
		WH.RTT.RTT RTT

	inner join WH.RTT.RTTStatus RTTStatus
	on	RTTStatus.InternalCode = RTT.RTTStatusCode
	and	RTTStatus.ClockStopFlag = 'true'

	where
		RTT.ReferralSourceUniqueID = Encounter.SourceUniqueID
	and	RTT.StartDate < @to
	)

--not on outpatient waiting list
and	not exists
	(
	select
		1
	from
		WH.OP.WaitingList WaitingList
	where
		WaitingList.ReferralSourceUniqueID = Encounter.SourceUniqueID
	and	WaitingList.CensusDate = 
		(
		select
			max(CensusDate)
		from
			WH.OP.WaitingList WaitingList
		where
			WaitingList.CensusDate <= @to
		)
	)

--not on inpatient waiting list
and	not exists
	(
	select
		1
	from
		WH.APC.WaitingList WaitingList
	where
		WaitingList.ReferralSourceUniqueID = Encounter.SourceUniqueID
	and	WaitingList.CensusDate = 
		(
		select
			max(CensusDate)
		from
			WH.APC.WaitingList WaitingList
		where
			WaitingList.CensusDate <= @to
		)
	and	not exists
		(
		select
			1
		from
			WH.WH.DiagnosticProcedure DiagnosticProcedure
		where
			DiagnosticProcedure.ProcedureCode = WaitingList.IntendedPrimaryOperationCode
		)
	)

and	coalesce(
		 ConsultantSpecialtyMap.XrefEntityCode
		,Encounter.SpecialtyCode
	) not in
	(
	 48			--Obstetrics	501
	,2000852	--Obstetrics	501
	,10000170	--ANTE-NATAL	501B
	,10000171	--POST-NATAL	501C
	,2000855	--Midwife Episode	560
	,2000661	--Midwife Episode	560
	,10000175	--MIDWIFE-EPI WELL BAB	560W
	)

and	Encounter.DischargeDate is null

and	(
		Consultant.ProfessionalCarerTypeCode = 1130	--Consultant
	or	(
			Consultant.ProfessionalCarerTypeCode = 1136	--Nurse
		and	Consultant.ConsultantCode = 10539696	--Nurse Led Plastics
		)
	)

--remove internal referrals
and	Encounter.ParentSourceUniqueID is null


--remove 98's
and	not exists
	(
	select
		1
	from
		WH.RTT.RTT RTT
	where
		RTT.ReferralSourceUniqueID = Encounter.SourceUniqueID
	and	RTT.StartDate < @to
	and	RTT.RTTStatusCode = 3007094 --98 Not applicable to RTT
	and	not exists
		(
		select
			1
		from
			WH.RTT.RTT Previous
		where
			Previous.ReferralSourceUniqueID = Encounter.SourceUniqueID
		and	Previous.StartDate < @to
		and	(
				Previous.StartDate < RTT.StartDate
			or	(
					Previous.StartDate = RTT.StartDate
				and	Previous.SourceUniqueID < RTT.SourceUniqueID
				)
			)
		)
	)

--remove referrals with A&E, maternity or non-elective admissions
and	not exists
	(
	select
		1
	from
		WH.APC.Encounter Admission
	where
		Admission.ReferralSourceUniqueID = Encounter.SourceUniqueID
	and	Admission.AdmissionMethodCode not in
			(
			 2003470	--Elective
			,8811		--Elective - Booked
			,8810		--Elective - Waiting List
			,13			--Not Specified
			,2003472	--OTHER
			)
	)

--remove referrals over 365 days old and with no activity
and	not exists
	(
	select
		1
	from
		WH.RF.Encounter OldReferral
	where
		OldReferral.SourceUniqueID = Encounter.SourceUniqueID
	and	datediff(day, OldReferral.ReferralDate, @to) >= 365
	and	not exists
		(
		select
			1
		from
			WH.OP.Encounter Encounter
		where
			Encounter.ReferralSourceUniqueID = OldReferral.SourceUniqueID
		)
	and	not exists
		(
		select
			1
		from
			WH.APC.Encounter Encounter
		where
			Encounter.ReferralSourceUniqueID = OldReferral.SourceUniqueID
		)
	and	not exists
		(
		select
			1
		from
			WH.OP.WaitingList Encounter
		where
			Encounter.ReferralSourceUniqueID = OldReferral.SourceUniqueID
		and	Encounter.CensusDate =
			(
			select
				max(CensusDate)
			from
				WH.OP.WaitingList WaitingList
			where
				WaitingList.CensusDate <= @to
			)
		)
	and	not exists
		(
		select
			1
		from
			WH.APC.WaitingList Encounter
		where
			Encounter.ReferralSourceUniqueID = OldReferral.SourceUniqueID
		and	Encounter.CensusDate =
			(
			select
				max(CensusDate)
			from
				WH.APC.WaitingList WaitingList
			where
				WaitingList.CensusDate <= @to
			)
		)
	)

and	Encounter.ReferralDate between '31 dec 2006 23:59:59' and @to


select @RowsInserted = @RowsInserted + @@rowcount
print 'Other referrals rows inserted ' + CONVERT(varchar(10), @@rowcount)


--apply legacy rules

delete
from
	dbo.BaseRTT
where
	BaseRTT.CensusDate = @census

and	BaseRTT.PathwayStatusCode in ('OPW', 'P') 

--365 days without an appointment
and
	(
		exists
		(
		select
			1
		from
			WH.OP.Encounter Outpatient
		where
			Outpatient.ReferralSourceUniqueID = BaseRTT.ReferralSourceUniqueID
		and	datediff(day, Outpatient.AppointmentDate, @to) >= 365
		and	not exists
			(
			select	1
			from
				WH.OP.Encounter Previous
			where
				Previous.ReferralSourceUniqueID = BaseRTT.ReferralSourceUniqueID
			and	(
					Previous.AppointmentTime > Outpatient.AppointmentTime
				or	(
						Previous.AppointmentTime = Outpatient.AppointmentTime
					and	Previous.EncounterRecno > Outpatient.EncounterRecno
					)
				)
			)
		)

	--duplicate referral i.e. referred for the same specialty within 10 days
	or	exists
		(
		select
			1
		from
			WH.RF.Encounter Previous
		where
			Previous.SourcePatientNo = BaseRTT.SourcePatientNo
		and	Previous.SpecialtyCode = BaseRTT.SpecialtyCode
		and	Previous.SourceUniqueID <> BaseRTT.ReferralSourceUniqueID
		and	datediff(day, Previous.ReferralDate, BaseRTT.ReferralDate) < 11
		)

	--subsequent clock stop for the same specialty
	or	exists
		(
		select
			1
		from
			WH.RF.Encounter Treated
		where
			Treated.SourcePatientNo = BaseRTT.SourcePatientNo
		and	Treated.SpecialtyCode = BaseRTT.SpecialtyCode
		and	Treated.SourceUniqueID <> BaseRTT.ReferralSourceUniqueID
		and	(
	--op treated
				exists
				(
				select
					1
				from
					WH.OP.Encounter Outpatient

				inner join WH.RTT.Disposal
				on	Disposal.DisposalCode = Outpatient.DisposalCode
				and	Disposal.ClockStopFlag = 'true'

				where
					Outpatient.ReferralSourceUniqueID = Treated.SourceUniqueID
				and	Outpatient.AppointmentStatusCode in
					(
					 357		--Attended On Time
					,2868		--Patient Late / Seen
					,2004151	--Attended
					)
				)
	--ip treated
			or	exists
				(
				select
					1
				from
					WH.APC.Encounter Inpatient
				where
					Inpatient.ReferralSourceUniqueID = Treated.SourceUniqueID
				)
			)
		)

	--last appointment was over 90 days ago and they are not on either the ip or op waiting list
	or	exists
		(
		select
			1
		from
			WH.OP.Encounter Outpatient
		where
			Outpatient.ReferralSourceUniqueID = BaseRTT.ReferralSourceUniqueID
		and	DATEDIFF(day, Outpatient.AppointmentDate, @to) > 90
		and	not exists
			(
			select
				1
			from
				WH.OP.Encounter Previous
			where
				Previous.ReferralSourceUniqueID = Outpatient.ReferralSourceUniqueID
			and	Previous.AppointmentTime < Outpatient.AppointmentTime
			)
		and	not exists
			(
			select
				1
			from
				WH.OP.WaitingList WaitingList
			where
				WaitingList.ReferralSourceUniqueID = Outpatient.ReferralSourceUniqueID
			and	WaitingList.CensusDate = 
				(
				select
					max(CensusDate)
				from
					WH.OP.WaitingList WaitingList
				where
					WaitingList.CensusDate <= @to
				)
			)
		and	not exists
			(
			select
				1
			from
				WH.APC.WaitingList WaitingList
			where
				WaitingList.ReferralSourceUniqueID = Outpatient.ReferralSourceUniqueID
			and	WaitingList.CensusDate = 
				(
				select
					max(CensusDate)
				from
					WH.APC.WaitingList WaitingList
				where
					WaitingList.CensusDate <= @to
				)
			)
		)
	--ClockStart before 1st Jan 2007 or Clockstart is null
	--The Null ClockStarts are those which come from 1948 Referrals
	or (ClockStartDate <'2007-01-01' or ClockStartDate is null)
	)

--adjust inserted row count
select @RowsInserted = @RowsInserted - @@rowcount
print 'Legacy rows deleted ' + CONVERT(varchar(10), @@rowcount)


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WHOLAP BuildBaseRTT', @Stats, @StartTime
