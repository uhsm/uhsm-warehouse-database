﻿CREATE Procedure [dbo].[BuildBaseTheatreCancelledOperation] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table BaseTheatreCancelledOperation

Insert into BaseTheatreCancelledOperation
(
	 [SourceUniqueID]
	,[CancellationDate]
	,[CancelReasonCode]
	,[CancelReasonCode1]
	,[CancellationComment]
	,[OperationDetailSourceUniqueID]
	,[ConsultantCode]
	,[SpecialtyCode]
	,[SurgeonCode]
	,[SurgeonSpecialtyCode]
	,[ProposedOperationDate]
	,[DistrictNo]
	,[Forename]
	,[Surname]
	,[WardCode]
	,[WardCode1]
	,[PreMedGivenFlag]
	,[FastedFlag]
	,[LastUpdated]
	,[PatientClassificationCode]
	,[PatientSourceUniqueID]
	,[CampusCode]
	,[TheatreCode]
	,[TheatreCode1]
	,[CancellationSurgeonCode]
	,[InitiatorCode]
	,[PriorityCode]
	,[AdmissionDate]
	,[LastMinute]
)
	Select
	 Cancel.[SourceUniqueID]
	,Cancel.[CancellationDate]
	,Cancel.[CancelReasonCode]
	,Cancel.[CancelReasonCode1]
	,Cancel.[CancellationComment]
	,Cancel.[OperationDetailSourceUniqueID]
	,Cancel.[ConsultantCode]
	,Cancel.[SpecialtyCode]
	,Cancel.[SurgeonCode]
	,Cancel.[SurgeonSpecialtyCode]
	,Cancel.[ProposedOperationDate]
	,Cancel.[DistrictNo]
	,Cancel.[Forename]
	,Cancel.[Surname]
	,Cancel.[WardCode]
	,Cancel.[WardCode1]
	,Cancel.[PreMedGivenFlag]
	,Cancel.[FastedFlag]
	,Cancel.[LastUpdated]
	,Cancel.[PatientClassificationCode]
	,Cancel.[PatientSourceUniqueID]
	,Cancel.[CampusCode]
	,Cancel.[TheatreCode]
	,Cancel.[TheatreCode1]
	,Cancel.[CancellationSurgeonCode]
	,Cancel.[InitiatorCode]
	,Cancel.[PriorityCode]
	,Cancel.[AdmissionDate]
	,lastmin.LastMinuteCancellations
	
	from WH.Theatre.Cancellation cancel
	left outer join WH.Theatre.LastMinuteCancellations lastmin
	on cancel.SourceUniqueID = lastmin.SourceUniqueID




select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

--exec WriteAuditLogEvent 'BuildBaseTheatreCancelledOp', @Stats, @StartTime






