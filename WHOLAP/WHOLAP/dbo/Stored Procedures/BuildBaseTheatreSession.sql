﻿CREATE procedure [dbo].[BuildBaseTheatreSession] as


/************************************************************************************************
	2011-04-27 KD
		Changed Session Start Time from
		InAnaestheticTime to
		AnaestheticInductionTime

************************************************************************************************/







declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table dbo.BaseTheatreSession

insert into dbo.BaseTheatreSession
(
	 SourceUniqueID
	,ActualSessionMinutes
	,PlannedSessionMinutes
	,StartTime
	,EndTime
	,SessionNumber
	,TheatreCode
	,ConsultantCode
	,SpecialtyCode
	,SurgeonCode1
	,SurgeonCode2
	,AnaesthetistCode1
	,AnaesthetistCode2
	,AnaesthetistCode3
	,ScoutNurseCode
	,InstrumentNurseCode
	,AnaestheticNurseCode
	,LastUpdated
	,TimetableDetailCode
	,Comment
	,SessionOrder
	,CancelledFlag
	,CancelledMinutes
	,OverrunReason
	,OverrunReasonDate
	,OverrunReasonStaffCode
	,TimetableConsultantCode
	,TemplateConsultantCode
	,TimetableAnaesthetistCode
	,TemplateAnaesthetistCode
	,TimetableSpecialtyCode
	,TemplateSpecialtyCode
	,ActualSessionStartTime
	,ActualSessionEndTime
)
select
	 Session.SourceUniqueID
	,Session.ActualSessionMinutes
	,Session.PlannedSessionMinutes
	,Session.StartTime
	,Session.EndTime
	,Session.SessionNumber
	,Session.TheatreCode
	,Session.ConsultantCode
	,Session.SpecialtyCode
	,Session.SurgeonCode1
	,Session.SurgeonCode2
	,Session.AnaesthetistCode1
	,Session.AnaesthetistCode2
	,Session.AnaesthetistCode3
	,Session.ScoutNurseCode
	,Session.InstrumentNurseCode
	,Session.AnaestheticNurseCode
	,Session.LastUpdated
	,Session.TimetableDetailCode
	,Session.Comment
	,Session.SessionOrder
	,Session.CancelledFlag
	,Session.CancelledMinutes
	,Session.OverrunReason
	,Session.OverrunReasonDate
	,Session.OverrunReasonStaffCode

	,TimetableConsultantCode = TimetableConsultant.StaffCode

	,TemplateConsultantCode = TimetableTemplateConsultant.StaffCode

	,TimetableAnaesthetistCode = TimetableAnaesthetist.StaffCode

	,TemplateAnaesthetistCode = TimetableTemplateAnaesthetist.StaffCode

	,TimetableSpecialtyCode = TimetableSpecialty.SpecialtyCode

	,TemplateSpecialtyCode = TimetableTemplateSpecialty.SpecialtyCode

	,ActualSessionStartTime = ActualSession.StartTime
	,ActualSessionEndTime = ActualSession.EndTime

from
	WH.Theatre.Session Session

left join WH.Theatre.TimetableDetail TimetableDetail
on	TimetableDetail.TimetableDetailCode = Session.TimetableDetailCode
and	TimetableDetail.SessionNumber = Session.SessionNumber
and	TimetableDetail.DayNumber = datepart(dw, Session.StartTime)

left join WH.Theatre.TimetableTemplate TimetableTemplate
on	TimetableTemplate.TimetableTemplateCode = TimetableDetail.TimetableTemplateCode
and	TimetableTemplate.SessionNumber = Session.SessionNumber
and	TimetableTemplate.DayNumber = datepart(dw, Session.StartTime)

left join WH.Theatre.Staff TimetableConsultant
on	TimetableConsultant.StaffCode1 = TimetableDetail.ConsultantCode

left join WH.Theatre.Staff TimetableTemplateConsultant
on	TimetableTemplateConsultant.StaffCode1 = TimetableTemplate.ConsultantCode

left join WH.Theatre.Staff TimetableAnaesthetist
on	TimetableAnaesthetist.StaffCode1 = TimetableDetail.AnaesthetistCode

left join WH.Theatre.Staff TimetableTemplateAnaesthetist
on	TimetableTemplateAnaesthetist.StaffCode1 = TimetableTemplate.AnaesthetistCode

left join WH.Theatre.Specialty TimetableSpecialty
on	TimetableSpecialty.SpecialtyCode1 = TimetableDetail.SpecialtyCode

left join WH.Theatre.Specialty TimetableTemplateSpecialty
on	TimetableTemplateSpecialty.SpecialtyCode1 = TimetableTemplate.SpecialtyCode

left join 
	(
	select
		 SessionSourceUniqueID
		--,StartTime = min(InAnaestheticTime) Changed by KD 27/04/2011
		-- On instruction from Tract Iles
		,StartTime = MIN(coalesce(AnaestheticInductionTime, InAnaestheticTime))
		,EndTime = max(coalesce(OperationEndDate, InRecoveryTime))
	from
		WH.Theatre.OperationDetail
	group by
		SessionSourceUniqueID
	) ActualSession
on	ActualSession.SessionSourceUniqueID = Session.SourceUniqueID


select @RowsInserted = @@rowcount



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'BuildBaseTheatreSession', @Stats, @StartTime
