﻿CREATE procedure [BuildBaseTheatreSessionOld] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table dbo.BaseTheatreSession

insert into dbo.BaseTheatreSession
	(
	 SourceUniqueID
	,InterfaceCode
	,EncounterRecno
	,TheatreCode
	,SessionDate
	,ConsultantCode
	,OriginalConsultantCode
	,ConsultantAllocationCode
	,ReassignedSession
	,SessionTypeCode
	,TheatreSessionPeriodCode
	,CancelReasonCode
	,CancelledSession
	,Session
	,SessionDuration
	,TheoreticalSessionDuration
	,ActualDuration
	,OverUtilisation
	,UnderUtilisation
	,AmendTime
	,Comment
	,SessionDescription
	,PlannedAnaesthetist
	,PrintTime
	,Registrar
	,SessionStatus
	,StartTime
	,EndTime
	,TheatreNumber
	,HistoryRecno
	,HistoryID
	,HistoryAmendTime
	,HistoryAmendUser
	,HistoryAnaesthetistCode
	,HistoryCancelReasonCode
	,HistoryConsultantCode
	,HistoryReAssignReasonCode
	,HistoryStartTime
	,HistoryEndTime
	,HistorySessionDate
	,HistorySessionTypeCode
	,HistoryTheatreCode	)
select
	Session.ID SourceUniqueID,
	Session.InterfaceCode,
	Session.EncounterRecno,

	case	
	when Session.SubTheatre is null then '##'
	else Session.SubTheatre
	end TheatreCode, 

	Session.ProcedureDate SessionDate, 

--better to have some kind of consultant in there than nothing!
	coalesce(Session.ConsultantCode, '##') + 
	case
	when Session.DerivedSessionTypeCode = 'T' then '|T' 
	else ''
	end ConsultantCode, 

	coalesce(History.Consultant, Session.ConsultantCode, '##') +
	case
	when Session.DerivedSessionTypeCode = 'T' then '|T' 
	else ''
	end OriginalConsultantCode, 

	case	
	when History.EncounterRecno is null then 'NA'
	when coalesce(History.Consultant, Session.ConsultantCode, '##') = coalesce(Session.ConsultantCode, '##') then 'SC'
	when coalesce(OriginalConsultant.SpecialtyCode, Consultant.SpecialtyCode, '##') = coalesce(Consultant.SpecialtyCode, '##') then 'SS'
	else 'DS'
	end ConsultantAllocationCode,

	Session.Reassigned ReassignedSession,

	DerivedSessionTypeCode SessionTypeCode, 

	case
	when Session.SessionStartTime between  '00:00' and '12:29' and Session.SessionEndTime > '16:29' then 'AD'
	when Session.SessionStartTime between  '00:00' and '12:29' then 'AM'
	when Session.SessionStartTime between  '12:30' and '16:29' then 'PM'
	when Session.SessionStartTime between  '16:30' and '23:59' then 'VN'
	else '##'
	end TheatreSessionPeriodCode,

	case when Session.HistoryRecno is not null then convert(varchar, coalesce(History.CancelReason, History.ReAssignReason))
	else '##'
	end CancelReasonCode,

	case when Session.HistoryRecno is not null then 1 else 0 end CancelledSession,

	1 Session,

	datediff(minute, Session.StartTime, Session.EndTime) -
		case
		when Session.SessionStartTime between  '00:00' and '12:29' and Session.SessionEndTime > '16:29' then 60 else 0
		end
	SessionDuration,

	case
	when Session.ActualSessionStartTime is null or Session.ActualSessionEndTime is null then null
	else datediff(minute, Session.StartTime, Session.EndTime) -
		case
		when Session.SessionStartTime between  '00:00' and '12:29' and Session.SessionEndTime > '16:29' then 60 else 0
		end
	end TheoreticalSessionDuration,

	datediff(minute, Session.ActualSessionStartTime, Session.ActualSessionEndTime) -
		case
--discrepency here however, although all day session duration loses 60 minutes, actual duration does not
		when Session.ActualSessionStartTime between  '00:00' and '12:29' and Session.ActualSessionEndTime > '16:29' then 0 else 0
		end
	ActualDuration,

	case
	when datediff(minute, Session.StartTime, Session.EndTime) > datediff(minute, Session.ActualSessionStartTime, Session.ActualSessionEndTime) then null
	else datediff(minute, Session.ActualSessionStartTime, Session.ActualSessionEndTime) - datediff(minute, Session.StartTime, Session.EndTime)
	end OverUtilisation,

	case
	when datediff(minute, Session.StartTime, Session.EndTime) < datediff(minute, Session.ActualSessionStartTime, Session.ActualSessionEndTime)then null
	else datediff(minute, Session.StartTime, Session.EndTime) - datediff(minute, Session.ActualSessionStartTime, Session.ActualSessionEndTime)
	end UnderUtilisation

	,Session.AmendTime
	,Session.Comment
	,SessionDescription = Session.Description
	,Session.PlannedAnaesthetist
	,Session.PrintTime
	,Session.Registrar
	,Session.SessionStatus
	,Session.StartTime
	,Session.EndTime
	,Session.TheatreNumber
	,Session.HistoryRecno
	,HistoryID = History.ID
	,HistoryAmendTime = History.AmendTime
	,HistoryAmendUser = History.AmendUser
	,HistoryAnaesthetistCode =History.Anaesthetist
	,HistoryCancelReasonCode = History.CancelReason
	,HistoryConsultantCode = History.Consultant
	,HistoryReAssignReasonCode = History.ReAssignReason
	,HistoryStartTime = History.StartTime
	,HistoryEndTime = History.EndTime
	,HistorySessionDate = History.SessionDate
	,HistorySessionTypeCode = History.SessionType
	,HistoryTheatreCode = History.Theatre

from
	(
		select
			Session.*,
			left(right(convert(varchar, Session.StartTime, 14), 12), 5) SessionStartTime, 
			left(right(convert(varchar, Session.EndTime, 14), 12), 5) SessionEndTime,
			ActualSession.StartTime ActualSessionStartTime,
			ActualSession.EndTime ActualSessionEndTime,

			case
			when Session.SessionType = 'W' then Session.SessionType
			when TheatreTemplate.SessionTypeCode is not null then TheatreTemplate.SessionTypeCode
			when Session.SubTheatre = '2' and Session.InterfaceCode = 'OTIMS' then 'E'
			when Session.SubTheatre = '3' and Session.InterfaceCode = 'OTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'MON' and left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '12:30' and '16:29' then 'T'
			when Session.SubTheatre = '3' and Session.InterfaceCode = 'OTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'THU' and left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '12:30' and '16:29' then 'T'
			when Session.SubTheatre = '3' and Session.InterfaceCode = 'OTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'FRI' and left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '12:30' and '16:29' then 'T'
			when Session.SubTheatre = '7' and Session.InterfaceCode = 'OTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'TUE' and left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '12:30' and '16:29' then 'T'
			when Session.SubTheatre = '7' and Session.InterfaceCode = 'OTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'WED' and left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '12:30' and '16:29' then 'T'
		
			when Session.SubTheatre = '6' and Session.InterfaceCode = 'RTIMS' and left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '00:00' and '12:29' then 'E'
			when Session.SubTheatre = '6' and Session.InterfaceCode = 'RTIMS' and left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '12:30' and '16:29' then 'T'
		
			when Session.SubTheatre = '1' and Session.InterfaceCode = 'BTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'MON' and left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '12:30' and '16:29' then 'T'
			when Session.SubTheatre = '1' and Session.InterfaceCode = 'BTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'WED' and left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '12:30' and '16:29' then 'T'
			when Session.SubTheatre = '1' and Session.InterfaceCode = 'BTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'THU' and left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '12:30' and '16:29' then 'T'
			when Session.SubTheatre = '1' and Session.InterfaceCode = 'BTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'FRI' and left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '12:30' and '16:29' then 'T'
			when Session.SubTheatre = '2' and Session.InterfaceCode = 'BTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'TUE' then 'T'
		
			when Session.SubTheatre = '4' and Session.InterfaceCode = 'NTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'THU' and left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '12:30' and '16:29' then 'T'
			when Session.SubTheatre = '5' and Session.InterfaceCode = 'NTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'MON' then 'E'
			when Session.SubTheatre = '5' and Session.InterfaceCode = 'NTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'WED' then 'E'
			when Session.SubTheatre = '5' and Session.InterfaceCode = 'NTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'THU' then 'E'
			when Session.SubTheatre = '5' and Session.InterfaceCode = 'NTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'FRI' then 'E'
			when Session.SubTheatre = '6' and Session.InterfaceCode = 'NTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'TUE' and left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '12:30' and '16:29' then 'E'
			when Session.SubTheatre = '7' and Session.InterfaceCode = 'NTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'WED' and left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '00:00' and '12:29' then 'T'
			when Session.SubTheatre = '8' and Session.InterfaceCode = 'NTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'TUE' and left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '00:00' and '12:29' then 'T'
			when Session.SubTheatre = '8' and Session.InterfaceCode = 'NTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'WED' and left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '00:00' and '12:29' then 'T'
			when Session.SubTheatre = '8' and Session.InterfaceCode = 'NTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'FRI' and left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '00:00' and '12:29' then 'T'
			when Session.SubTheatre = '8' and Session.InterfaceCode = 'NTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'MON' and left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '12:30' and '16:29' then 'T'
			when Session.SubTheatre = '9' and Session.InterfaceCode = 'NTIMS' and upper(left(datename(DW, Session.StartTime), 3)) = 'THU' and left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '00:00' and '12:29' then 'T'
		
			when Session.SessionType is null then 'E'
			when Session.SessionType = '' then 'E'
			when Session.SessionType in ('E', 'R', 'W') then Session.SessionType
			else 'R'
			end DerivedSessionTypeCode

		from
			WH.Theatre.LegacySessions Session

		left join
			(
			select
				min(PatientEpisode.TimeAnaestheticStart) StartTime,
				max(PatientEpisode.TimeOffTable) EndTime,
				PatientEpisode.SessionID,
				PatientEpisode.InterfaceCode
			from
				WH.Theatre.PatientEpisode
			where
				PatientEpisode.TheatreNumber != '0'
		-- ignore cancellations
			and	PatientEpisode.Outcome < 3
			and	PatientEpisode.TimeAnaestheticStart is not null
			and	PatientEpisode.TimeOffTable is not null
			group by
				PatientEpisode.SessionID,
				PatientEpisode.InterfaceCode
		
			) ActualSession
		on	ActualSession.SessionID = Session.ID
		and	ActualSession.InterfaceCode = Session.InterfaceCode

		left join 
			(
			select
				 TheatreTemplate.ActiveDate FromDate
				,
				(
				select
					coalesce(min(ActiveDate), dateadd(day, 3650 + datediff(day, 0, getdate()), 0))
				from
					WH.Theatre.TheatreTemplate NextTheatreTemplate
				where
					NextTheatreTemplate.ActiveDate > TheatreTemplate.ActiveDate
				and	NextTheatreTemplate.InterfaceCode = TheatreTemplate.InterfaceCode
				and	NextTheatreTemplate.TheatreCode = TheatreTemplate.TheatreCode
				and	NextTheatreTemplate.TheatreSessionPeriodCode = TheatreTemplate.TheatreSessionPeriodCode
				and	NextTheatreTemplate.DayOfWeekId = TheatreTemplate.DayOfWeekId
				and	NextTheatreTemplate.TheatreRotaCode = TheatreTemplate.TheatreRotaCode
				) ToDate

				,TheatreTemplate.InterfaceCode
				,TheatreTemplate.TheatreCode
				,TheatreTemplate.TheatreSessionPeriodCode
				,TheatreTemplate.DayOfWeekId
				,TheatreTemplate.SessionTypeCode
				,TheatreTemplate.ConsultantCode
				,TheatreTemplate.SpecialtyCode
				,TheatreRota.WeeklyFrequency
			from
				WH.Theatre.TheatreTemplate

			inner join WH.Theatre.TheatreRota
			on	TheatreRota.TheatreRotaCode = TheatreTemplate.TheatreRotaCode

			) TheatreTemplate
		on	TheatreTemplate.InterfaceCode = Session.InterfaceCode
		and	TheatreTemplate.TheatreCode = Session.SubTheatre
		and	TheatreTemplate.TheatreSessionPeriodCode = 
			case
			when left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '00:00' and '12:29' and left(right(convert(varchar, Session.EndTime, 14), 12), 5) > '16:29' then 'AD'
			when left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '00:00' and '12:29' then 'AM'
			when left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '12:30' and '16:29' then 'PM'
			when left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '16:30' and '23:59' then 'VN'
			else '##'
			end
		and	Session.ProcedureDate between TheatreTemplate.FromDate and TheatreTemplate.ToDate
		and	convert(tinyint, datepart(dw, Session.ProcedureDate)) = TheatreTemplate.DayOfWeekId
		and	datediff(week, TheatreTemplate.FromDate, Session.ProcedureDate) % TheatreTemplate.WeeklyFrequency = 0

		where
			Session.ProcedureDate >= '1 jan 2006'
		and	charindex('|', Session.ID) > 0

		and	not
		(
			Session.SubTheatre = '3'
		and	Session.InterfaceCode = 'RTIMS'
		and	upper(left(datename(DW, Session.StartTime), 3)) = 'MON'
		and	left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '12:30' and '16:29' 
		)
		and	not
		(
			Session.SubTheatre = '4'
		and	Session.InterfaceCode = 'RTIMS'
		and	upper(left(datename(DW, Session.StartTime), 3)) = 'TUE'
		and	left(right(convert(varchar, Session.StartTime, 14), 12), 5) between  '12:30' and '16:29' 
		)

		and	not
		(
			Session.SubTheatre = '6'
		and	Session.InterfaceCode = 'BTIMS'
		and	upper(left(datename(DW, Session.StartTime), 3)) = 'FRI'
		)


--			Session.ProcedureDate between '1 apr 2003' and convert(smalldatetime, left(convert(smalldatetime, dateadd(day, -1, getdate()), 113), 11))
	) Session

left join WH.Theatre.History
on	History.EncounterRecno = Session.HistoryRecno

left join WH.Theatre.Consultant Consultant
on	Consultant.ConsultantCode = Session.ConsultantCode
and	Consultant.InterfaceCode = Session.InterfaceCode

left join WH.Theatre.Consultant OriginalConsultant
on	OriginalConsultant.ConsultantCode = History.Consultant
and	OriginalConsultant.InterfaceCode = History.InterfaceCode


select @RowsInserted = @@rowcount



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'BuildBaseTheatreSession', @Stats, @StartTime
