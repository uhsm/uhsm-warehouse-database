﻿CREATE procedure [BuildBaseTheatreVisitOld] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table dbo.BaseTheatreVisit

insert into dbo.BaseTheatreVisit
	(
	 EncounterRecno
	,SessionID
	,SourceUniqueID
	,InterfaceCode
	,TheatreCode
	,SessionTypeCode
	,CaseTypeCode
	,TheatreSessionPeriodCode
	,SessionDate
	,ConsultantCode
	,CaseConsultantCode
	,OriginalConsultantCode
	,ConsultantAllocationCode
	,CancelReasonCode
	,EpisodicConsultantCode
	,SurgeonCode
	,AnaesthetistCode
	,PrimaryProcedureCode
	,AgeCode
	,SexCode
	,PatientCategoryCode
	,SeverityCode
	,Visit
	,ReceptionTime
	,ReceptionWaitTime
	,AnaestheticWaitTime
	,AnaestheticTime
	,OperationTime
	,RecoveryTime
	,ReturnTime
	,PatientTime
	,ReportableFlag
	,ASACode
	,ATO
	,ATORelieved
	,ActualOperationLine1
	,ActualOperationLine2
	,ActualOperationLine3
	,ActualOperationLine4
	,AnaestheticComments
	,AnaestheticNurse1
	,AnaestheticNurse1Relieved
	,AnaestheticNurse2
	,AnaestheticNurse2Relieved
	,AnaestheticType1
	,AnaestheticType2
	,AnaestheticType3
	,AnaestheticType4
	,Anaesthetist1
	,Anaesthetist2
	,Anaesthetist3
	,CEPOD
	,CancelReason
	,CirculatingNurse
	,CirculatingNurseRelieved
	,DestinationFromRecovery
	,Episode
	,EscortName
	,EscortNurse
	,EscortNurseRelieved
	,ExpectedLengthOfOperation
	,FirstAssistant
	,IntendedManagement
	,InternalDistrictNumber
	,OtherNurse
	,OtherNurseRelieved
	,Outcome
	,PatientAccompanied
	,PatientLocation
	,PlannedAnaesthetic
	,PortersActivityReception
	,ProcedureDate
	,ProcedureTime
	,ProposedOperationLine1
	,ProposedOperationLine2
	,ProposedOperationLine3
	,ProposedOperationLine4
	,ReasonDelayReception1
	,ReasonDelayReception2
	,ReasonDelayRecovery1
	,ReasonDelayRecovery2
	,ReceptionComments
	,ReceptionNurse1
	,ReceptionNurse2
	,ReceptionPorter
	,RecoveryComments
	,RecoveryPorter
	,RevoveryNurse
	,RevoveryNurseRelieved
	,ScrubNurse1
	,ScrubNurse1Relieved
	,ScrubNurse2
	,ScrubNurse2Relieved
	,SpecialEquipment1
	,SpecialEquipment2
	,SpecialEquipment3
	,SpecialEquipment4
	,Surgeon1
	,Surgeon2
	,Surgeon3
	,TheatreComments
	,TheatreNumber
	,TimeAnaestheticStart
	,TimeArrived
	,TimeAwake
	,TimeInAnaestheticRoom
	,TimeInRecovery
	,TimeOffTable
	,TimeOnTable
	,TimeOutAnaestheticRoom
	,TimeOutRecovery
	,TimeReadyForDischarge
	,TimeSentFor
	,WardCode
	,SourcePatientNo
	,PASEncounterRecno
	)
select
	Visit.EncounterRecno, 
	Visit.SessionID,
	Visit.SourceUniqueID,

	Session.InterfaceCode,
	Session.TheatreCode,
	Session.SessionTypeCode,

	case	
	when Visit.CEPOD = '1' then 'E'
	else Session.SessionTypeCode
	end CaseTypeCode, 

	Session.TheatreSessionPeriodCode,
	Session.SessionDate,
	Session.ConsultantCode,

	--if this is an emergency case then the consultant is the lead surgeon
	case	
	when Visit.CEPOD = '1' then coalesce(Visit.Surgeon1, Session.ConsultantCode)
	when Session.ConsultantCode = '##' then coalesce(Visit.Surgeon1, Session.ConsultantCode)
	else Session.ConsultantCode
	end CaseConsultantCode,

	Session.OriginalConsultantCode,

	Session.ConsultantAllocationCode,

	coalesce(Visit.CancelReason, '##') CancelReasonCode,

	coalesce(Encounter.ConsultantCode, 'N/A') EpisodicConsultantCode,

	coalesce(Visit.Surgeon1, '##') SurgeonCode,

	coalesce(Visit.Anaesthetist1, '##') AnaesthetistCode,

	coalesce(Encounter.PrimaryOperationCode, '##') PrimaryProcedureCode

	,AgeCode =
	case
	when datediff(day, Encounter.DateOfBirth, Visit.ProcedureDate) is null then 'Age Unknown'
	when datediff(day, Encounter.DateOfBirth, Visit.ProcedureDate) <= 0 then '00 Days'
	when datediff(day, Encounter.DateOfBirth, Visit.ProcedureDate) = 1 then '01 Day'
	when datediff(day, Encounter.DateOfBirth, Visit.ProcedureDate) > 1 and
	datediff(day, Encounter.DateOfBirth, Visit.ProcedureDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, Encounter.DateOfBirth, Visit.ProcedureDate)), 2) + ' Days'
	
	when datediff(day, Encounter.DateOfBirth, Visit.ProcedureDate) > 28 and
	datediff(month, Encounter.DateOfBirth, Visit.ProcedureDate) -
	case  when datepart(day, Encounter.DateOfBirth) > datepart(day, Visit.ProcedureDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, Encounter.DateOfBirth, Visit.ProcedureDate) -
	case  when datepart(day, Encounter.DateOfBirth) > datepart(day, Visit.ProcedureDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, Encounter.DateOfBirth, Visit.ProcedureDate) -
	case  when datepart(day, Encounter.DateOfBirth) > datepart(day, Visit.ProcedureDate) then 1 else 0 end > 1 and
	 datediff(month, Encounter.DateOfBirth, Visit.ProcedureDate) -
	case  when datepart(day, Encounter.DateOfBirth) > datepart(day, Visit.ProcedureDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, Encounter.DateOfBirth,Visit.ProcedureDate) -
	case  when datepart(day, Encounter.DateOfBirth) > datepart(day, Visit.ProcedureDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, Encounter.DateOfBirth, Visit.ProcedureDate) - 
	(
	case 
	when	(datepart(m, Encounter.DateOfBirth) > datepart(m, Visit.ProcedureDate)) 
	or
		(
			datepart(m, Encounter.DateOfBirth) = datepart(m, Visit.ProcedureDate) 
		And	datepart(d, Encounter.DateOfBirth) > datepart(d, Visit.ProcedureDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, Encounter.DateOfBirth, Visit.ProcedureDate) - 
	(
	case 
	when	(datepart(m, Encounter.DateOfBirth) > datepart(m, Visit.ProcedureDate)) 
	or
		(
			datepart(m, Encounter.DateOfBirth) = datepart(m, Visit.ProcedureDate) 
		And	datepart(d, Encounter.DateOfBirth) > datepart(d, Visit.ProcedureDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end

	,case
	when Patient.SexCode is null then '##'
	when Patient.SexCode not in ('M', 'F') then '##'
	else Patient.SexCode
	end SexCode,

	case	
	when Visit.IntendedManagement is null then '20'
	when Visit.IntendedManagement = 'DC' then '20'
	when Visit.IntendedManagement = 'IP' then '10'
	when Visit.IntendedManagement = 'OP' then '200'
	when Visit.IntendedManagement in ('', 'PP', 'SDS') then '20'
	when Visit.IntendedManagement = 'PPSDS' then '20'
	when Visit.IntendedManagement = 'PPIP' then '10'
	when Visit.IntendedManagement = 'PPDC' then '20'
	else '20'
	end PatientCategoryCode,

	'##' SeverityCode,

	1 Visit,

	--	case when Visit.TheatreNumber = '0' then 1 else 0 end UnscheduledCase,

	--	case when Visit.CancelReason is not null then 1 else 0 end Cancelled,

	datediff(minute, TimeSentFor, TimeArrived) ReceptionTime,
	datediff(minute, TimeArrived, TimeInAnaestheticRoom) ReceptionWaitTime,
	datediff(minute, TimeInAnaestheticRoom, TimeAnaestheticStart) AnaestheticWaitTime,
	datediff(minute, TimeAnaestheticStart, TimeOutAnaestheticRoom) AnaestheticTime,
	datediff(minute, TimeOnTable, TimeOffTable) OperationTime,
	datediff(minute, TimeInRecovery, TimeOutRecovery) RecoveryTime,
	datediff(minute, TimeReadyForDischarge, TimeOutRecovery) ReturnTime,
	datediff(minute, TimeAnaestheticStart, TimeOffTable) PatientTime


	,coalesce(Tracking.ReportableFlag, 0) ReportableFlag

	,Visit.ASACode
	,Visit.ATO
	,Visit.ATORelieved
	,Visit.ActualOperationLine1
	,Visit.ActualOperationLine2
	,Visit.ActualOperationLine3
	,Visit.ActualOperationLine4
	,Visit.AnaestheticComments
	,Visit.AnaestheticNurse1
	,Visit.AnaestheticNurse1Relieved
	,Visit.AnaestheticNurse2
	,Visit.AnaestheticNurse2Relieved
	,Visit.AnaestheticType1
	,Visit.AnaestheticType2
	,Visit.AnaestheticType3
	,Visit.AnaestheticType4
	,Visit.Anaesthetist1
	,Visit.Anaesthetist2
	,Visit.Anaesthetist3
	,Visit.CEPOD
	,Visit.CancelReason
	,Visit.CirculatingNurse
	,Visit.CirculatingNurseRelieved
	,Visit.DestinationFromRecovery
	,Visit.Episode
	,Visit.EscortName
	,Visit.EscortNurse
	,Visit.EscortNurseRelieved
	,Visit.ExpectedLengthOfOperation
	,Visit.FirstAssistant
	,Visit.IntendedManagement
	,Visit.InternalDistrictNumber
	,Visit.OtherNurse
	,Visit.OtherNurseRelieved
	,Visit.Outcome
	,Visit.PatientAccompanied
	,Visit.PatientLocation
	,Visit.PlannedAnaesthetic
	,Visit.PortersActivityReception
	,Visit.ProcedureDate
	,Visit.ProcedureTime
	,Visit.ProposedOperationLine1
	,Visit.ProposedOperationLine2
	,Visit.ProposedOperationLine3
	,Visit.ProposedOperationLine4
	,Visit.ReasonDelayReception1
	,Visit.ReasonDelayReception2
	,Visit.ReasonDelayRecovery1
	,Visit.ReasonDelayRecovery2
	,Visit.ReceptionComments
	,Visit.ReceptionNurse1
	,Visit.ReceptionNurse2
	,Visit.ReceptionPorter
	,Visit.RecoveryComments
	,Visit.RecoveryPorter
	,Visit.RevoveryNurse
	,Visit.RevoveryNurseRelieved
	,Visit.ScrubNurse1
	,Visit.ScrubNurse1Relieved
	,Visit.ScrubNurse2
	,Visit.ScrubNurse2Relieved
	,Visit.SpecialEquipment1
	,Visit.SpecialEquipment2
	,Visit.SpecialEquipment3
	,Visit.SpecialEquipment4
	,Visit.Surgeon1
	,Visit.Surgeon2
	,Visit.Surgeon3
	,Visit.TheatreComments
	,Visit.TheatreNumber
	,Visit.TimeAnaestheticStart
	,Visit.TimeArrived
	,Visit.TimeAwake
	,Visit.TimeInAnaestheticRoom
	,Visit.TimeInRecovery
	,Visit.TimeOffTable
	,Visit.TimeOnTable
	,Visit.TimeOutAnaestheticRoom
	,Visit.TimeOutRecovery
	,Visit.TimeReadyForDischarge
	,Visit.TimeSentFor
	,Visit.WardCode
	,Visit.SourcePatientNo
	,Visit.PASEncounterRecno
from
	WH.Theatre.PatientEpisode Visit

inner join dbo.FactTheatreSession Session
on	Session.SourceUniqueID = Visit.SessionID
and	Session.InterfaceCode = Visit.InterfaceCode

-- consultant episode
left join WH.APC.Encounter Encounter
on	Encounter.EncounterRecno = Visit.PASEncounterRecno

left join WH.Theatre.CrIndex1 Patient
on	Patient.InternalDistrictNumber = Visit.InternalDistrictNumber
and	Patient.InterfaceCode = Visit.InterfaceCode

left join WH.Theatre.Tracking
on	Tracking.EpisodeID = Visit.SourceUniqueID
and	Tracking.InterfaceCode = Visit.InterfaceCode

where
	Session.SessionDate >= '1 jan 2006'


select @RowsInserted = @@rowcount



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'BuildBaseTheatreVisit', @Stats, @StartTime
