﻿CREATE procedure [BuildBaseWaitingListActivity] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table dbo.BaseWaitingListActivity

insert into dbo.BaseWaitingListActivity
	(
	 SourceUniqueID
	,ActivityTypeCode
	,ActivityDate
	,ActivityTime
	,AdminCategoryCode
	,BookingTypeCode
	,CancelledBy
	,ConsultantCode
	,DeferralEndDate
	,DeferralRevisedEndDate
	,DiagnosticGroupCode
	,SourceEntityRecno
	,SiteCode
	,SourcePatientNo
	,LastRevisionTime
	,LastRevisionUser
	,PreviousActivityID
	,Reason
	,Remark
	,RemovalComment
	,RemovalReasonCode
	,SpecialtyCode
	,SuspensionReasonCode
	,TCIAcceptDate
	,TCITime
	,TCIOfferDate
	,PriorityCode
	,WaitingListCode
	,WardCode
	,Created
	,Updated
	,ByWhom
	,CharterCancelCode
	,CharterCancelDeferFlag
	,CharterCancel
	,OpCancelledFlag
	,OpCancelled
	,PatientChoiceFlag
	,PatientChoice
	,WLSuspensionInitiatorCode
	,WLSuspensionInitiator
	)

select
	 SourceUniqueID
	,ActivityTypeCode
	,ActivityDate
	,ActivityTime
	,AdminCategoryCode
	,BookingTypeCode
	,CancelledBy

	,ConsultantCode =
		coalesce(ConsultantCode, 'N/A')

	,DeferralEndDate
	,DeferralRevisedEndDate
	,DiagnosticGroupCode
	,SourceEntityRecno

	,SiteCode =
		coalesce(SiteCode, 'N/A')

	,SourcePatientNo
	,LastRevisionTime
	,LastRevisionUser
	,PreviousActivityID
	,Reason
	,Remark
	,RemovalComment
	,RemovalReasonCode

	,SpecialtyCode =
		coalesce(SpecialtyCode, 'N/A')

	,SuspensionReasonCode
	,TCIAcceptDate
	,TCITime
	,TCIOfferDate
	,PriorityCode
	,WaitingListCode
	,WardCode
	,Created
	,Updated
	,ByWhom
	,CharterCancelCode
	,CharterCancelDeferFlag
	,CharterCancel
	,OpCancelledFlag
	,OpCancelled
	,PatientChoiceFlag
	,PatientChoice
	,WLSuspensionInitiatorCode
	,WLSuspensionInitiator
from
	WH.APC.WaitingListActivity



select @RowsInserted = @@rowcount



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'BuildBaseWaitingListActivity', @Stats, @StartTime
