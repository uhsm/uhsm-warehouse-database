﻿-----------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd, 2001 --
-----------------------------------------------------------------------
CREATE procedure [dbo].[BuildCalendarBase]
(
    @FromDate varchar (20),
    @ToDate varchar (20)
)
as

declare @CurrentDate smalldatetime   /* upper limit as smalldatetime */

--if convert(smalldatetime,@FromDate)<'1 Jan 1950' select @FromDate='1 Jan 1950'
--if convert(smalldatetime,@ToDate)>'31 Dec 2020' select @ToDate='31 Dec 2020'

select @CurrentDate = @FromDate

while @CurrentDate <= @ToDate
   begin
        if not exists (select * from CalendarBase where TheDate = @CurrentDate)
            insert into CalendarBase
			(
			[TheDate]
			)
			values
			(
			@CurrentDate
			)

        select @CurrentDate=dateadd(day, 1, @CurrentDate)

    end

--force in catch-all date
select @CurrentDate = '1 Jan 1900'

if not exists (select * from CalendarBase where TheDate = @CurrentDate)
    insert into CalendarBase
	(
	[TheDate]
	)
	values
	(
	@CurrentDate
	)

