﻿CREATE procedure [dbo].[BuildFactAE] as

declare @StartTime datetime
declare @Elapsed int
--declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table dbo.FactAE

insert into dbo.FactAE
(
	 SourceUniqueID
	,StageCode
	,ArrivalModeCode
	,AttendanceDisposalCode
	,SiteCode
	,RegisteredPracticeCode
	,AttendanceCategoryCode
	,ReferredToSpecialtyCode
	,LevelOfCareCode
	,EncounterBreachStatusCode
	,EncounterStartTimeOfDay
	,EncounterEndTimeOfDay
	,EncounterStartDate
	,EncounterEndDate
	,StageBreachStatusCode
	,StageDurationMinutes
	,StageBreachMinutes
	,StageStartTimeOfDay
	,StageEndTimeOfDay
	,StageStartDate
	,StageEndDate
	,Cases
	,BreachValue
	,DelayCode
	,EncounterDurationMinutes
	,InterfaceCode
	,DurationBandCode
	,WhereSeen
)
select
	 AEEncounter.SourceUniqueID

	,AEEncounterStage.StageCode

	,ArrivalModeCode =
		coalesce(
			AEEncounter.ArrivalModeCode
			,-1
		)

	,AttendanceDisposalCode =
		coalesce(
			AEEncounter.AttendanceDisposalCode
			,'##'
		)

	,AEEncounter.SiteCode

	,RegisteredPracticeCode =
		coalesce(
			right(AEEncounter.RegisteredGpPracticeCode, 4)
			,'X99999'
		)

	,AttendanceCategoryCode =
		coalesce(
			AEEncounter.AttendanceCategoryCode
			,'#'
		)

	,ReferredToSpecialtyCode =
		coalesce(
			 AEEncounter.ReferredToSpecialtyCode
			,'##'
		)

	,LevelOfCareCode =
		coalesce(
			AEEncounter.TriageCategoryCode
			,'##'
		)

	,EncounterBreachStatusCode = 
	case
	when AEEncounter.AttendanceConclusionTime is null then 'X'
	when EncounterBreach.BreachValue is null
	then 'N'
	else 'B'
	end

	,EncounterStartTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime), 0)
				,AEEncounter.ArrivalTime
			)
			,-1
		)

	,EncounterEndTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, AEEncounter.AttendanceConclusionTime), 0)
				,AEEncounter.AttendanceConclusionTime
			)
			,-1
		)

	,EncounterStartDate = dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime), 0)

	,EncounterEndDate = 
		coalesce(
			 dateadd(day, datediff(day, 0, AEEncounter.AttendanceConclusionTime), 0)
			,dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime), 0)
		)

	,StageBreachStatusCode = 
	case
	when AEEncounterStage.StageEndTime is null then 'X'
	when
		datediff(
			 minute
			,AEEncounterStage.StageStartTime
			,AEEncounterStage.StageEndTime
		)
		+ AEEncounterStage.BSTAdjustment
		 > StageBreach.BreachValue
	then 'B'
	else 'N'
	end

	,StageDurationMinutes =
		datediff(
			 minute
			,AEEncounterStage.StageStartTime
			,AEEncounterStage.StageEndTime
		)
		+ AEEncounterStage.BSTAdjustment

	,StageBreachMinutes =
		datediff(
			 minute
			,AEEncounterStage.StageStartTime
			,AEEncounterStage.StageEndTime
		) 
		+ AEEncounterStage.BSTAdjustment
		- StageBreach.BreachValue


	,StageStartTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, AEEncounterStage.StageStartTime), 0)
				,AEEncounterStage.StageStartTime
			)
			,-1
		)

	,StageEndTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, AEEncounterStage.StageEndTime), 0)
				,AEEncounterStage.StageEndTime
			)
			,-1
		)

	,StageStartDate =
		coalesce(
			 dateadd(day, datediff(day, 0, AEEncounterStage.StageStartTime), 0)
			,dateadd(day, datediff(day, 0, AEEncounterStage.StageEndTime), 0)
		)

	,StageEndDate =
		coalesce(
			 dateadd(day, datediff(day, 0, AEEncounterStage.StageEndTime), 0)
			,dateadd(day, datediff(day, 0, AEEncounterStage.StageStartTime), 0)
		)

	,Cases = AEEncounter.Attends

	,BreachValue =
		case
		when AEEncounterStage.StageEndTime is null 
		then null
		else StageBreach.BreachValue
		end

	,DelayCode =
		row_number() over(
		partition by AEEncounter.SourceUniqueID
		order by
			coalesce(
				(
					datediff(
						 minute
						,AEEncounterStage.StageStartTime
						,AEEncounterStage.StageEndTime
					)
					+ AEEncounterStage.BSTAdjustment * 1.0
				)
				 /
				StageBreach.BreachValue * 1.0
				,1.0
			) desc
		)

	,EncounterDurationMinutes =
		datediff(
			 minute
			,AEEncounter.ArrivalTime
			,AEEncounter.AttendanceConclusionTime
		)
		+ WHReporting.dbo.BSTAdjustment(AEEncounter.ArrivalTime, AEEncounter.AttendanceConclusionTime)

	,AEEncounter.InterfaceCode

	,DurationBandCode =
		case
		when
		datediff(
			 minute
			,AEEncounterStage.StageStartTime
			,AEEncounterStage.StageEndTime
		) is null
		then '000'

		when
		datediff(
			 minute
			,AEEncounterStage.StageStartTime
			,AEEncounterStage.StageEndTime
		)
		+ AEEncounterStage.BSTAdjustment < 16
		then '015'

		when
		datediff(
			 minute
			,AEEncounterStage.StageStartTime
			,AEEncounterStage.StageEndTime
		)
		+ AEEncounterStage.BSTAdjustment between 16 and 30
		then '030'

		when
		datediff(
			 minute
			,AEEncounterStage.StageStartTime
			,AEEncounterStage.StageEndTime
		)
		+ AEEncounterStage.BSTAdjustment between 31 and 60
		then '060'

		when
		datediff(
			 minute
			,AEEncounterStage.StageStartTime
			,AEEncounterStage.StageEndTime
		)
		+ AEEncounterStage.BSTAdjustment between 61 and 90
		then '090'

		when
		datediff(
			 minute
			,AEEncounterStage.StageStartTime
			,AEEncounterStage.StageEndTime
		)
		+ AEEncounterStage.BSTAdjustment between 91 and 120
		then '120'

		when
		datediff(
			 minute
			,AEEncounterStage.StageStartTime
			,AEEncounterStage.StageEndTime
		)
		+ AEEncounterStage.BSTAdjustment between 121 and 150
		then '150'

		when
		datediff(
			 minute
			,AEEncounterStage.StageStartTime
			,AEEncounterStage.StageEndTime
		)
		+ AEEncounterStage.BSTAdjustment between 151 and 180
		then '180'

		when
		datediff(
			 minute
			,AEEncounterStage.StageStartTime
			,AEEncounterStage.StageEndTime
		)
		+ AEEncounterStage.BSTAdjustment between 181 and 210
		then '210'

		when
		datediff(
			 minute
			,AEEncounterStage.StageStartTime
			,AEEncounterStage.StageEndTime
		)
		+ AEEncounterStage.BSTAdjustment between 211 and 240
		then '240'

		else '999'

		end
	,AEEncounter.WhereSeen
from
	dbo.BaseAE AEEncounter

inner join
	(
	--in department cases
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'INDEPARTMENT'
		,StageStartTime = AEEncounter.ArrivalTime
		,StageEndTime = AEEncounter.AttendanceConclusionTime
		,BSTAdjustment = WHReporting.dbo.BSTAdjustment(AEEncounter.ArrivalTime, AEEncounter.AttendanceConclusionTime)
	from
		dbo.BaseAE AEEncounter

	union all

	--Triage cases
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'TRIAGE'
		,StageStartTime = AEEncounter.ArrivalTime
		,StageEndTime = AEEncounter.InitialAssessmentTime
		,BSTAdjustment =  WHReporting.dbo.BSTAdjustment(AEEncounter.ArrivalTime, AEEncounter.InitialAssessmentTime)
	from
		dbo.BaseAE AEEncounter
	where
		AEEncounter.InitialAssessmentTime is not null

	union all

	--Triage to Doctor cases
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'TRIAGETODOCTOR'
		,StageStartTime = AEEncounter.InitialAssessmentTime
		,StageEndTime = AEEncounter.SeenForTreatmentTime
		,BSTAdjustment =  WHReporting.dbo.BSTAdjustment(AEEncounter.InitialAssessmentTime, AEEncounter.SeenForTreatmentTime)
	from
		dbo.BaseAE AEEncounter
	where
		AEEncounter.SeenForTreatmentTime is not null

	union all

	--Doctor to Referral
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'DOCTORTOREFERRAL'
		,StageStartTime = AEEncounter.SeenForTreatmentTime
		,StageEndTime = AEEncounter.ToSpecialtyTime
		,BSTAdjustment =  WHReporting.dbo.BSTAdjustment(AEEncounter.SeenForTreatmentTime, AEEncounter.ToSpecialtyTime)
	from
		dbo.BaseAE AEEncounter
	where
		AEEncounter.ToSpecialtyTime is not null

	--union all

	----Referral to Consultant
	--select
	--	 SourceUniqueID = AEEncounter.SourceUniqueID
	--	,StageCode = 'REFERRALTOCONSULTANT'
	--	,StageStartTime = AEEncounter.ToSpecialtyTime
	--	,StageEndTime = AEEncounter.
	--from
	--	dbo.BaseAE AEEncounter
	--where
	--	AEEncounter.InitialAssessmentTime is not null

	--union all

	----Consultant to DTA
	--select
	--	 SourceUniqueID = AEEncounter.SourceUniqueID
	--	,StageCode = 'CONSULTANTTODTA'
	--	,StageStartTime = AEEncounter.
	--	,StageEndTime = AEEncounter.DecisionToAdmitTime
	--from
	--	dbo.BaseAE AEEncounter
	--where
	--	AEEncounter.InitialAssessmentTime is not null

	union all

	--DTA to Treatment Complete
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'DTATOTREATMENTCOMPLETE'
		,StageStartTime = AEEncounter.DecisionToAdmitTime
		,StageEndTime = AEEncounter.AttendanceConclusionTime
		,BSTAdjustment =  WHReporting.dbo.BSTAdjustment(AEEncounter.DecisionToAdmitTime, AEEncounter.AttendanceConclusionTime)
	from
		dbo.BaseAE AEEncounter
	where
		AEEncounter.DecisionToAdmitTime is not null

	--union all

	----Seen For Treatment cases
	--select
	--	 SourceUniqueID = AEEncounter.SourceUniqueID
	--	,StageCode = 'SEENFORTREATMENT'
	--	,StageStartTime = AEEncounter.ArrivalTime
	--	,StageEndTime = AEEncounter.SeenForTreatmentTime
	--from
	--	dbo.BaseAE AEEncounter
	--where
	--	AEEncounter.SeenForTreatmentTime is not null

	--union all

	----Awaiting Diagnostic cases
	--select
	--	 SourceUniqueID = AEEncounter.SourceUniqueID
	--	,StageCode = 'AWAITINGDIAGNOSTIC'
	--	,StageStartTime = AEInvestigation.InvestigationDate
	--	,StageEndTime = AEInvestigation.ResultDate
	--from
	--	dbo.BaseAE AEEncounter

	--inner join WH.AE.Investigation AEInvestigation
	--on	AEInvestigation.AESourceUniqueID = AEEncounter.SourceUniqueID
	--and	AEInvestigation.SequenceNo = 1

	--union all

	----Awaiting Specialty cases
	--select
	--	 SourceUniqueID = AEEncounter.SourceUniqueID
	--	,StageCode = 'AWAITINGSPECIALTY'
	--	,StageStartTime = AEEncounter.ToSpecialtyTime
	--	,StageEndTime = AEEncounter.SeenBySpecialtyTime
	--from
	--	dbo.BaseAE AEEncounter
	--where
	--	AEEncounter.ToSpecialtyTime is not null

	--union all

	----Awaiting Specialty Conclusion cases
	--select
	--	 SourceUniqueID = AEEncounter.SourceUniqueID
	--	,StageCode = 'AWAITINGSPECIALTYCONCLUSION'
	--	,StageStartTime = AEEncounter.SeenBySpecialtyTime
	--	,StageEndTime = AEEncounter.AttendanceConclusionTime
	--from
	--	dbo.BaseAE AEEncounter
	--where
	--	AEEncounter.SeenBySpecialtyTime is not null

	--union all

	----Awaiting Bed
	--select
	--	 SourceUniqueID = AEEncounter.SourceUniqueID
	--	,StageCode = 'AWAITINGBED'
	--	,StageStartTime = AEEncounter.DecisionToAdmitTime
	--	,StageEndTime = AEEncounter.AttendanceConclusionTime
	--from
	--	dbo.BaseAE AEEncounter
	--where
	--	AEEncounter.AttendanceDisposalCode = '01' --admitted
	--and	AEEncounter.DecisionToAdmitTime is not null

	--union all

	----registration to departure
	--select
	--	 SourceUniqueID = AEEncounter.SourceUniqueID
	--	,StageCode = 'REGISTRATIONTODEPARTURE'
	--	,StageStartTime =
	--		coalesce(
	--			 AEEncounter.RegisteredTime
	--			,AEEncounter.ArrivalTime
	--		)
	--	,StageEndTime = AEEncounter.AttendanceConclusionTime
	--from
	--	dbo.BaseAE AEEncounter

	) AEEncounterStage
on	AEEncounterStage.SourceUniqueID = AEEncounter.SourceUniqueID

left join WH.AE.StageBreach EncounterBreach
on	EncounterBreach.StageCode = 'INDEPARTMENT'
and
	datediff(
		 minute
		,AEEncounter.ArrivalTime
		,AEEncounter.AttendanceConclusionTime
	)
	+  WHReporting.dbo.BSTAdjustment(AEEncounter.ArrivalTime, AEEncounter.AttendanceConclusionTime)
	 > EncounterBreach.BreachValue


inner join WH.AE.StageBreach StageBreach
on	StageBreach.StageCode = AEEncounterStage.StageCode

--left join WH.AE.Investigation AEInvestigation
--on	AEInvestigation.AESourceUniqueID = AEEncounter.SourceUniqueID
--and	AEInvestigation.SequenceNo = 1


select @RowsInserted = @@rowcount


select
	 @from = min(EncounterFact.EncounterStartDate)
	,@to = max(EncounterFact.EncounterEndDate)
from
	dbo.FactAE EncounterFact
where
	EncounterFact.EncounterEndDate is not null


--exec BuildOlapPractice 'AE'
--exec BuildCalendarBase @from, @to


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
		CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'AE - WHOLAP BuildFactAE', @Stats, @StartTime
