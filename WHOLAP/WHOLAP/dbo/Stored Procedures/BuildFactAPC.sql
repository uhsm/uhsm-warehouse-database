﻿CREATE procedure [dbo].[BuildFactAPC] as

declare @StartTime datetime
declare @Elapsed int
--declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table dbo.FactAPC


insert into dbo.FactAPC
(
	 EncounterRecno
	,EncounterDate
	,MetricCode
	,ConsultantCode
	,SpecialtyCode
	,PracticeCode
	,SiteCode
	,AgeCode
	,Cases
	,LengthOfEncounter
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,AdmissionMethodCode
	,StartServicePointCode
	,EndServicePointCode
	,PreOpBedDays
)


--FCEs
SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'CHKFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	EpisodeEndDate is not null

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'IEFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	Encounter.AdmissionMethodTypeCode = 'EL'
and	Encounter.InpatientStayCode = 'I'
and	Encounter.EpisodeEndDate is not null

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'DCFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0 
from
	dbo.OlapAPC Encounter
where
	Encounter.AdmissionMethodTypeCode = 'EL'
and	Encounter.InpatientStayCode = 'D'
and	Encounter.EpisodeEndDate is not null

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'RDFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	Encounter.AdmissionMethodTypeCode = 'EL'
and	Encounter.InpatientStayCode = 'RD'
and	Encounter.EpisodeEndDate is not null

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'RNFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	Encounter.AdmissionMethodTypeCode = 'EL'
and	Encounter.InpatientStayCode = 'RN'
and	Encounter.EpisodeEndDate is not null

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'NEOTHFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	Encounter.AdmissionMethodTypeCode = 'NE'
and	EpisodeEndDate is not null

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'MATFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	Encounter.AdmissionMethodTypeCode = 'MAT'
and	EpisodeEndDate is not null

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'EMFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	Encounter.AdmissionMethodTypeCode = 'EM'
and	EpisodeEndDate is not null

union all

-- Admissions
SELECT
	 Encounter.EncounterRecno
	,Encounter.AdmissionDate EncounterDate
	,'CHKADM' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,Case When isnull(datediff(day,AdmissionDate,PrimaryOperationDate),0) < 0 Then 0
	 WHEN 
	 DischargeDate < PrimaryOperationDate THEN 0
     Else
     isnull(datediff(day,AdmissionDate,PrimaryOperationDate),0)
     End 
from
	dbo.OlapAPC Encounter
where
	Encounter.EpisodeStartTime = Encounter.AdmissionTime

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.AdmissionDate EncounterDate
	,'IEADM' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,Case When isnull(datediff(day,AdmissionDate,PrimaryOperationDate),0) < 0 Then 0
	 WHEN 
	 DischargeDate < PrimaryOperationDate THEN 0
     Else
     isnull(datediff(day,AdmissionDate,PrimaryOperationDate),0)
     End 
from
	dbo.OlapAPC Encounter
where
	Encounter.AdmissionMethodTypeCode = 'EL'
and	Encounter.InpatientStayCode = 'I'
and	Encounter.EpisodeStartTime = Encounter.AdmissionTime


union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.AdmissionDate EncounterDate
	,'DCADM' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	Encounter.AdmissionMethodTypeCode = 'EL'
and	Encounter.InpatientStayCode = 'D'
and	Encounter.EpisodeStartTime = Encounter.AdmissionTime

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.AdmissionDate EncounterDate
	,'RDADM' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0 
from
	dbo.OlapAPC Encounter
where
	Encounter.AdmissionMethodTypeCode = 'EL'
and	Encounter.InpatientStayCode = 'RD'
and	Encounter.EpisodeStartTime = Encounter.AdmissionTime

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.AdmissionDate EncounterDate
	,'RNADM' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,Case When isnull(datediff(day,AdmissionDate,PrimaryOperationDate),0) < 0 Then 0
	 WHEN 
	 DischargeDate < PrimaryOperationDate THEN 0
     Else
     isnull(datediff(day,AdmissionDate,PrimaryOperationDate),0)
     End
from
	dbo.OlapAPC Encounter
where
	Encounter.AdmissionMethodTypeCode = 'EL'
and	Encounter.InpatientStayCode = 'RN'
and	Encounter.EpisodeStartTime = Encounter.AdmissionTime

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.AdmissionDate EncounterDate
	,'NEOTHADM' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,Case When isnull(datediff(day,AdmissionDate,PrimaryOperationDate),0) < 0 Then 0
	 WHEN 
	 DischargeDate < PrimaryOperationDate THEN 0
     Else
     isnull(datediff(day,AdmissionDate,PrimaryOperationDate),0)
     End
from
	dbo.OlapAPC Encounter
where
	Encounter.AdmissionMethodTypeCode = 'NE'
and	Encounter.EpisodeStartTime = Encounter.AdmissionTime

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.AdmissionDate EncounterDate
	,'MATADM' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	Encounter.AdmissionMethodTypeCode = 'MAT'
and	Encounter.EpisodeStartTime = Encounter.AdmissionTime

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.AdmissionDate EncounterDate
	,'EMADM' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,Case When isnull(datediff(day,AdmissionDate,PrimaryOperationDate),0) < 0 Then 0
	 WHEN 
	 DischargeDate < PrimaryOperationDate THEN 0
     Else
     isnull(datediff(day,AdmissionDate,PrimaryOperationDate),0)
     End 
from
	dbo.OlapAPC Encounter
where
	Encounter.AdmissionMethodTypeCode = 'EM'
and	Encounter.EpisodeStartTime = Encounter.AdmissionTime

union all

--Discharges
SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'CHKDIS' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	LastEpisodeInSpellIndicator = 1
and	Encounter.DischargeDate is not null

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'IEDIS' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0 
from
	dbo.OlapAPC Encounter
where
	LastEpisodeInSpellIndicator = 1
and	DischargeDate is not null
and	Encounter.InpatientStayCode = 'I'
and	Encounter.AdmissionMethodTypeCode = 'EL'

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'DCDIS' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	LastEpisodeInSpellIndicator = 1
and	DischargeDate is not null
and	Encounter.InpatientStayCode = 'D'
and	Encounter.AdmissionMethodTypeCode = 'EL'

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'RDDIS' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	LastEpisodeInSpellIndicator = 1
and	DischargeDate is not null
and	Encounter.InpatientStayCode = 'RD'
and	Encounter.AdmissionMethodTypeCode = 'EL'

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'RNDIS' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0 
from
	dbo.OlapAPC Encounter
where
	LastEpisodeInSpellIndicator = 1
and	DischargeDate is not null
and	Encounter.InpatientStayCode = 'RN'
and	Encounter.AdmissionMethodTypeCode = 'EL'

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'NEOTHDIS' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	LastEpisodeInSpellIndicator = 1
and	DischargeDate is not null
and	Encounter.AdmissionMethodTypeCode = 'NE'

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'MATDIS' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	LastEpisodeInSpellIndicator = 1
and	DischargeDate is not null
and	Encounter.AdmissionMethodTypeCode = 'MAT'

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'EMDIS' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	LastEpisodeInSpellIndicator = 1
and	DischargeDate is not null
and	Encounter.AdmissionMethodTypeCode = 'EM'

union all

--Births
SELECT
	 Encounter.EncounterRecno
	,Encounter.AdmissionDate EncounterDate
	,'WELLBABY' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0 
from
	dbo.OlapAPC Encounter
where
	NeonatalLevelOfCareFlag = 0
and	(
		PrimaryDiagnosisCode like 'Z38%'
	or	exists
		(
		select
			1
		from
			WH.APC.Diagnosis Diagnosis
		where
			Diagnosis.APCSourceUniqueID = Encounter.SourceUniqueID
		and	Diagnosis.DiagnosisCode like 'Z38%'
		)
	)

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.AdmissionDate EncounterDate
	,'ILLBABY' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0 
from
	dbo.OlapAPC Encounter
where
	coalesce(NeonatalLevelOfCareFlag, 1) <> 0
and	(
		PrimaryDiagnosisCode like 'Z38%'
	or	exists
		(
		select
			1
		from
			WH.APC.Diagnosis Diagnosis
		where
			Diagnosis.APCSourceUniqueID = Encounter.SourceUniqueID
		and	Diagnosis.DiagnosisCode like 'Z38%'
		)
	)

union all

--Deaths
SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'DIED' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0 
from
	dbo.OlapAPC Encounter
where
	LastEpisodeInSpellIndicator = 1
and	DischargeDate is not null
and	DischargeMethodCode = 8836	--Patient Died	4

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'STILL' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	LastEpisodeInSpellIndicator = 1
and	DischargeDate is not null
and	DischargeMethodCode = 8837	--Still birth	5

union all

--First FCEs
SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'CHKFFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0 
from
	dbo.OlapAPC Encounter
where
	EpisodeEndDate is not null
and	Encounter.EpisodeStartTime = Encounter.AdmissionTime

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'IEFFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	EpisodeEndDate is not null
and	Encounter.EpisodeStartTime = Encounter.AdmissionTime
and	Encounter.AdmissionMethodTypeCode = 'EL'
and	Encounter.InpatientStayCode = 'I'

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'DCFFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	EpisodeEndDate is not null
and	Encounter.EpisodeStartTime = Encounter.AdmissionTime
and	Encounter.AdmissionMethodTypeCode = 'EL'
and	Encounter.InpatientStayCode = 'D'

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'RDFFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	EpisodeEndDate is not null
and	Encounter.EpisodeStartTime = Encounter.AdmissionTime
and	Encounter.AdmissionMethodTypeCode = 'EL'
and	Encounter.InpatientStayCode = 'RD'

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'RNFFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	EpisodeEndDate is not null
and	Encounter.EpisodeStartTime = Encounter.AdmissionTime
and	Encounter.AdmissionMethodTypeCode = 'EL'
and	Encounter.InpatientStayCode = 'RN'

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'NEOTHFFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	EpisodeEndDate is not null
and	Encounter.EpisodeStartTime = Encounter.AdmissionTime
and	Encounter.AdmissionMethodTypeCode = 'NE'

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'MATFFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	EpisodeEndDate is not null
and	Encounter.EpisodeStartTime = Encounter.AdmissionTime
and	Encounter.AdmissionMethodTypeCode = 'MAT'

union all

SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'EMFFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.RegisteredGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardTypeCode
	,EndWardTypeCode
	,0
from
	dbo.OlapAPC Encounter
where
	EpisodeEndDate is not null
and	Encounter.EpisodeStartTime = Encounter.AdmissionTime
and	Encounter.AdmissionMethodTypeCode = 'EM'


select @RowsInserted = @@rowcount


select
	 @from = min(EncounterFact.EncounterDate)
	,@to = max(EncounterFact.EncounterDate)
from
	dbo.FactAPC EncounterFact
where
	EncounterFact.EncounterDate is not null


exec BuildOlapPASSpecialty 'APC'
exec BuildOlapPASConsultant 'APC'
exec BuildOlapPASPractice 'APC'
exec BuildCalendarBase @from, @to


select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
		CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'PAS - WHOLAP BuildFactAPC', @Stats, @StartTime
