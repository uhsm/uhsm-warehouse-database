﻿CREATE procedure [dbo].[BuildFactAPCWaitingList] as

declare @StartTime datetime
declare @Elapsed int
--declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

truncate table dbo.FactAPCWaitingList

insert into dbo.FactAPCWaitingList
(
	 EncounterRecno
	,Cases
	,LengthOfWait
	,AgeCode
	,CensusDate
	,ConsultantCode
	,SpecialtyCode
	,DurationCode
	,PracticeCode
	,SiteCode
	,WaitTypeCode
	,StatusCode
	,CategoryCode
	,AddedLastWeek
	,WaitingListAddMinutes
	,OPCSCoded
	,WithRTTStartDate
	,WithRTTStatusCode
	,WithExpectedAdmissionDate
)
SELECT
	 Encounter.EncounterRecno
	,Encounter.Cases
	,Encounter.LengthOfWait
	,Encounter.AgeCode
	,Encounter.CensusDate
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.DurationCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.SiteCode
	,Encounter.WaitTypeCode
	,Encounter.StatusCode
	,Encounter.CategoryCode
	,Encounter.AddedLastWeek
	,Encounter.WaitingListAddMinutes
	,Encounter.OPCSCoded
	,Encounter.WithRTTStartDate
	,Encounter.WithRTTStatusCode
	,WithExpectedAdmissionDate
FROM
	dbo.OlapAPCWaitingList Encounter

select @RowsInserted = @@rowcount

exec BuildOlapPASSpecialty 'APCWL'
exec BuildOlapPASConsultant 'APCWL'
exec BuildOlapPASPractice 'APCWL'
exec BuildOlapCensus


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WHOLAP BuildFactAPCWaitingList', @Stats, @StartTime
