﻿CREATE procedure [BuildFactAudit] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

truncate table dbo.FactAudit

insert into	dbo.FactAudit
	(
	 AuditLogRecno
	,AuditDate
	,AuditStartTimeOfDay
	,AuditEndTimeOfDay
	,StartTime
	,EndTime
	,UserId
	,ProcessCode
	,Event
	,ProcessSeconds
	)
select
	 AuditLog.AuditLogRecno

	,AuditDate =
		dateadd(day, datediff(day, 0, AuditLog.EventTime), 0)

	,AuditStartTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, AuditLog.StartTime), 0)
				,AuditLog.StartTime
			)
			,-1
		)

	,AuditEndTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, AuditLog.EventTime), 0)
				,AuditLog.EventTime
			)
			,-1
		)

	,AuditLog.StartTime
	,EndTime = AuditLog.EventTime

	,AuditLog.UserId
	,AuditLog.ProcessCode
	,AuditLog.Event

	,ProcessSeconds =
		datediff(second, AuditLog.StartTime, AuditLog.EventTime)

from
	WH.dbo.AuditLog


insert into WH.dbo.AuditProcess
	(
	 ProcessCode
	,Process
	,ParentProcessCode
	,ProcessSourceCode
	)
select distinct
	 ProcessCode
	,Process = ProcessCode
	,ParentProcessCode = 'OTH'
	,ProcessSourceCode = 'OTH'
from
	WH.dbo.AuditLog
where
	not exists
	(
	select
		1
	from
		WH.dbo.AuditProcess AuditProcess
	where
		AuditProcess.ProcessCode = AuditLog.ProcessCode
	)

select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'BuildFactAudit', @Stats, @StartTime
