﻿CREATE procedure [dbo].[BuildFactClinicalCoding] as

declare @StartTime datetime
declare @Elapsed int
--declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table dbo.FactClinicalCoding

insert into dbo.FactClinicalCoding
(
	 ClinicalCodingDate
	,ClinicalCodingTypeCode
	,ClinicalCodingCode
	,SequenceNo
	,SourceCode
	,SourceRecno
	,PASCreatedByWhom
	,PASCreatedDate
)
SELECT
	 ClinicalCodingDate =
		coalesce(
			convert(
				 date
				,ClinicalCodingTime
			)
			,'1 Jan 1900'
		)

	,ClinicalCodingTypeCode
	,ClinicalCodingCode
	,SequenceNo = SortOrder
	,SourceCode
	,SourceRecno

	,PASCreatedByWhom =
			coalesce(
				olapLorenzoUser.UserUniqueID
				,'-1'
			)

	,PasCreatedDate = CONVERT(Date,PasCreatedDate)
FROM
	dbo.BaseClinicalCoding

left join dbo.olapLorenzoUser
on	olapLorenzoUser.UserUniqueID = cast(BaseClinicalCoding.PASCreatedByWhom as varchar(30))

select @RowsInserted = @@rowcount


select
	 @from = min(EncounterFact.ClinicalCodingDate)
	,@to = max(EncounterFact.ClinicalCodingDate)
from
	dbo.FactClinicalCoding EncounterFact
where
	EncounterFact.ClinicalCodingDate is not null

--exec BuildOlapPASSpecialty 'ClinicalCoding'
--exec BuildOlapPASConsultant 'ClinicalCoding'
--exec BuildOlapPASPractice 'ClinicalCoding'

exec BuildCalendarBase @from, @to


select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
		CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'PAS - WHOLAP BuildFactClinicalCoding', @Stats, @StartTime





