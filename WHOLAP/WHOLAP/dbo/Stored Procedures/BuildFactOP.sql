﻿CREATE procedure [dbo].[BuildFactOP] as

declare @StartTime datetime
declare @Elapsed int
--declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table dbo.FactOP

insert into dbo.FactOP
(
	 EncounterRecno
	,EncounterDate
	,MetricCode
	,ConsultantCode
	,SpecialtyCode
	,PracticeCode
	,AgeCode
	,ClinicCode
	,Cases
	,SiteCode
	,SourceOfReferralCode
	,BookingTypeCode
	,LengthOfWait
	,IsWardAttender
	,ClinicType
	,ExludedClinic
	,IsWalkIn
)
SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'FATT' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.EpisodicGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.AgeCode
	,Encounter.ClinicCode
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.ClinicType
	,Encounter.ExcludedClinic
	,Encounter.IsWalkIn
FROM
	dbo.OlapOP Encounter
where
	AttendanceStatusCode = 'ATTEND'
and	FirstAttendanceCode = 'NEW'

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'RATT' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.EpisodicGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.AgeCode
	,Encounter.ClinicCode
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.ClinicType
	,Encounter.ExcludedClinic
	,Encounter.IsWalkIn
FROM
	dbo.OlapOP Encounter
where
	AttendanceStatusCode = 'ATTEND'
and	FirstAttendanceCode = 'REVIEW'


union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'FDNA' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.EpisodicGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.AgeCode
	,Encounter.ClinicCode
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.ClinicType
	,Encounter.ExcludedClinic
	,Encounter.IsWalkIn
FROM
	dbo.OlapOP Encounter
where
	AttendanceStatusCode = 'DNA'
and	FirstAttendanceCode = 'NEW'

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'RDNA' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.EpisodicGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.AgeCode
	,Encounter.ClinicCode
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.ClinicType
	,Encounter.ExcludedClinic
	,Encounter.IsWalkIn
FROM
	dbo.OlapOP Encounter
where
	AttendanceStatusCode = 'DNA'
and	FirstAttendanceCode = 'REVIEW'

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'FCANPAT' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.EpisodicGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.AgeCode
	,Encounter.ClinicCode
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.ClinicType
	,Encounter.ExcludedClinic
	,Encounter.IsWalkIn
FROM
	dbo.OlapOP Encounter
where
	AttendanceStatusCode = 'PATCAN'
and	FirstAttendanceCode = 'NEW'

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'RCANPAT' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.EpisodicGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.AgeCode
	,Encounter.ClinicCode
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.ClinicType
	,Encounter.ExcludedClinic
	,Encounter.IsWalkIn
FROM
	dbo.OlapOP Encounter
where
	AttendanceStatusCode = 'PATCAN'
and	FirstAttendanceCode = 'REVIEW'

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'FCANPROV' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.EpisodicGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.AgeCode
	,Encounter.ClinicCode
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.ClinicType
	,Encounter.ExcludedClinic
	,Encounter.IsWalkIn
FROM
	dbo.OlapOP Encounter
where
	AttendanceStatusCode = 'HOSPCAN'
and	FirstAttendanceCode = 'NEW'

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'RCANPROV' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.EpisodicGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.AgeCode
	,Encounter.ClinicCode
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.ClinicType
	,Encounter.ExcludedClinic
	,Encounter.IsWalkIn
FROM
	dbo.OlapOP Encounter
where
	AttendanceStatusCode = 'HOSPCAN'
and	FirstAttendanceCode = 'REVIEW'

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'FCANCEL' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.EpisodicGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.AgeCode
	,Encounter.ClinicCode
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.ClinicType
	,Encounter.ExcludedClinic
	,Encounter.IsWalkIn
FROM
	dbo.OlapOP Encounter
where
	AttendanceStatusCode in
		(
		 'PATCAN'
		,'HOSPCAN'
		)
and	FirstAttendanceCode = 'NEW'

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'RCANCEL' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.EpisodicGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.AgeCode
	,Encounter.ClinicCode
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.ClinicType
	,Encounter.ExcludedClinic
	,Encounter.IsWalkIn
FROM
	dbo.OlapOP Encounter
where
	AttendanceStatusCode in
		(
		 'PATCAN'
		,'HOSPCAN'
		)
and	FirstAttendanceCode = 'REVIEW'

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'FUNKATT' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.EpisodicGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.AgeCode
	,Encounter.ClinicCode
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.ClinicType
	,Encounter.ExcludedClinic
	,Encounter.IsWalkIn
FROM
	dbo.OlapOP Encounter
where
	AttendanceStatusCode = 'OTHER'
and	FirstAttendanceCode = 'NEW'

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'RUNKATT' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.EpisodicGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.AgeCode
	,Encounter.ClinicCode
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.ClinicType
	,Encounter.ExcludedClinic
	,Encounter.IsWalkIn
FROM
	dbo.OlapOP Encounter
where
	AttendanceStatusCode = 'OTHER'
and	FirstAttendanceCode = 'REVIEW'

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'CHKAPP' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.EpisodicGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.AgeCode
	,Encounter.ClinicCode
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.ClinicType
	,Encounter.ExcludedClinic
	,Encounter.IsWalkIn
FROM
	dbo.OlapOP Encounter

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'FAPP' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.EpisodicGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.AgeCode
	,Encounter.ClinicCode
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.ClinicType
	,Encounter.ExcludedClinic
	,Encounter.IsWalkIn
FROM
	dbo.OlapOP Encounter
where
	FirstAttendanceCode = 'NEW'

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'RAPP' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	--,Encounter.EpisodicGpPracticeCode
	,case 
		when Encounter.EpisodicGpPracticeCode = -1
		then Encounter.RegisteredGpPracticeCode
		else Encounter.EpisodicGpPracticeCode
	end
	,Encounter.AgeCode
	,Encounter.ClinicCode
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.ClinicType
	,Encounter.ExcludedClinic
	,Encounter.IsWalkIn
FROM
	dbo.OlapOP Encounter
where
	FirstAttendanceCode = 'REVIEW'


select @RowsInserted = @@rowcount


select
	 @from = min(EncounterFact.EncounterDate)
	,@to = max(EncounterFact.EncounterDate)
from
	dbo.FactOP EncounterFact
where
	EncounterFact.EncounterDate is not null


exec BuildOlapPASSpecialty 'OP'
exec BuildOlapPASConsultant 'OP'
exec BuildOlapPASPractice 'OP'
exec BuildCalendarBase @from, @to


select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
		CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'PAS - WHOLAP BuildFactOP', @Stats, @StartTime
