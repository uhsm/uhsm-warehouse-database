﻿CREATE procedure [BuildFactOPDiary] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table dbo.FactOPDiary

insert into dbo.FactOPDiary
(
	 SourceUniqueID
	,ClinicCode
	,SessionCode
	,SessionDescription
	,SessionDate
	,SessionStartTime
	,SessionEndTime
	,ReasonForCancellation
	,SessionPeriod
	,DoctorCode
	,ValidAppointmentTypeCode
	,Slots
	,UsedSlots
	,FreeSlots
	,Sessions
	,CancelledSessions
	,CancelledSlots
	,SessionMinutesDuration
)
SELECT
	 SourceUniqueID
	,ClinicCode
	,SessionCode
	,SessionDescription
	,SessionDate
	,SessionStartTime
	,SessionEndTime
	,ReasonForCancellation
	,SessionPeriod
	,DoctorCode
	,ValidAppointmentTypeCode

	,Slots = Units

	,UsedSlots = UsedUnits

	,FreeSlots = FreeUnits

	,Sessions = Cases

	,CancelledSessions =
		case when ReasonForCancellation is null then 0 else 1 end

	,CancelledSlots =
		case when ReasonForCancellation is null then 0 else Units end

	,SessionMinutesDuration =
		datediff(minute, SessionStartTime, SessionEndTime)

FROM
	dbo.BaseOPDiary



select @RowsInserted = @@rowcount


select
	 @from = min(EncounterFact.SessionDate)
	,@to = max(EncounterFact.SessionDate)
from
	dbo.FactOPDiary EncounterFact


exec BuildCalendarBase @from, @to


select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
		CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'BuildFactOPDiary', @Stats, @StartTime
