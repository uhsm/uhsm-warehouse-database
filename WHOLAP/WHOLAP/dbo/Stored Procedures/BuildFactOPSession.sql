﻿create procedure [dbo].[BuildFactOPSession] as

truncate table dbo.FactOPSession

insert into dbo.FactOPSession
(
	 SessionUniqueID
	,ServicePointUniqueID
	,SessionSpecialtyCode
	,SessionDate
	,Sessions
	,MaxNumberOfAppointments
)
select
	 Session.SessionUniqueID
	,Session.ServicePointUniqueID
	,Session.SessionSpecialtyCode
	,Session.SessionDate
	,Sessions = 1

	,MaxNumberOfAppointments =
		CAST(
			coalesce(
				 MaxNumberOfAppointments.ReferenceValue
				,MaxNumberOfAppointmentsBySP.ReferenceValue
			)
			as int
		)

from
	WH.OP.Session

left join 
	(
	select
		 SessionUniqueID = VisitTypeRule.AppliedToUniqueID
		,VisitTypeRuleCode = VisitTypeRule.RuleValueCode
	from
		WH.OP.[Rule] VisitTypeRule
	where
		VisitTypeRule.ArchiveFlag = 0
	and	VisitTypeRule.AppliedTo = 'SPSSN'
	and	VisitTypeRule.RuleAppliedUniqueID = 5 --max appointments
	and	VisitTypeRule.EnforcedFlag = 'Y'
	and	not exists
		(
		select
			1
		from
			WH.OP.[Rule] Previous
		where
			Previous.AppliedToUniqueID = VisitTypeRule.AppliedToUniqueID
		and	Previous.ArchiveFlag = 0
		and	Previous.AppliedTo = 'SPSSN'
		and	Previous.RuleAppliedUniqueID = 5 --max appointments
		and	Previous.EnforcedFlag = 'Y'
		and	Previous.RuleRecno > VisitTypeRule.RuleRecno
		)
	) MaxNumberOfAppointmentsBySession
on	MaxNumberOfAppointmentsBySession.SessionUniqueID = Session.SessionUniqueID

left join WH.PAS.ReferenceValue MaxNumberOfAppointments
on	MaxNumberOfAppointments.ReferenceValueCode = MaxNumberOfAppointmentsBySession.VisitTypeRuleCode

left join 
	(
	select
		 ServicePointUniqueID = VisitTypeRule.AppliedToUniqueID
		,VisitTypeRuleCode = VisitTypeRule.RuleValueCode
	from
		WH.OP.[Rule] VisitTypeRule
	where
		VisitTypeRule.ArchiveFlag = 0
	and	VisitTypeRule.AppliedTo = 'SPONT'
	and	VisitTypeRule.RuleAppliedUniqueID = 5 --max appointments
	and	VisitTypeRule.EnforcedFlag = 'Y'
	and	not exists
		(
		select
			1
		from
			WH.OP.[Rule] Previous
		where
			Previous.AppliedToUniqueID = VisitTypeRule.AppliedToUniqueID
		and	Previous.ArchiveFlag = 0
		and	Previous.AppliedTo = 'SPONT'
		and	Previous.RuleAppliedUniqueID = 5 --max appointments
		and	Previous.EnforcedFlag = 'Y'
		and	Previous.RuleRecno > VisitTypeRule.RuleRecno
		)
	) MaxNumberOfAppointmentsByServicePoint
on	MaxNumberOfAppointmentsByServicePoint.ServicePointUniqueID = Session.ServicePointUniqueID

left join WH.PAS.ReferenceValue MaxNumberOfAppointmentsBySP
on	MaxNumberOfAppointmentsBySP.ReferenceValueCode = MaxNumberOfAppointmentsByServicePoint.VisitTypeRuleCode

where
	Session.ArchiveFlag = 0
and	Session.IsTemplate = 0

and	Session.SessionDate <=
		coalesce(
			 Session.SessionEndDateTime
			,Session.SessionDate
		)

and	Session.SessionCancelledFlag = 0

and	Session.SessionStatusCode in
		(
		 1563	--	Session Held
		,1565	--	Session Scheduled
		,1566	--	Session Initiated
		)
