﻿create procedure BuildFactOPSessionSlot as

truncate table dbo.FactOPSessionSlot

insert into dbo.FactOPSessionSlot
(
	 SessionSpecialtyCode
	,SessionDate
	,BookedSlots
	,BookingsAllowed
	,BookingsFree
)
select
	 Session.SessionSpecialtyCode
	,Session.SessionDate

	,BookedSlots =
		isnull(
			 SlotBookings.Booked
			,0
		)
		
	,BookingsAllowed =
		coalesce(
			 SlotBookings.SlotMaxBookings
			,SessionMaxApptsPerSession.MaxAppts
			,ClinicMaxApptsPerSession.MaxAppts
			,0
		)

	,BookingsFree = 
		case
		when
			coalesce(
				 SlotBookings.SlotMaxBookings
				,SessionMaxApptsPerSession.MaxAppts
				,ClinicMaxApptsPerSession.MaxAppts
				,0
			) -
			
			isnull(
				SlotBookings.Booked
				,0
			)
			 < 0
		
		then 0
		else 
			coalesce(
				 SlotBookings.SlotMaxBookings
				,SessionMaxApptsPerSession.MaxAppts
				,ClinicMaxApptsPerSession.MaxAppts
				,0
			) -
			
			isnull(
				SlotBookings.Booked
				,0
			)
		end

from
	WH.OP.Session

--Number of Bookings
left join
	(
	select 
		 Slot.SessionUniqueID
		 
		,Booked =
			sum(Slot.SlotNumberofBookings)
			
		,SlotMaxBookings =
			sum(
				coalesce(
					 SlotMaxAppts.MaxAppts
					,SessionMaxApptsPerSlot.MaxAppts
					,ClinicMaxApptsPerSlot.MaxAppts
					,Slot.SlotMaxBookings
				)
			)
		from
			WH.OP.Slot Slot
			
		left join WH.OP.SlotMaxAppts SlotMaxAppts
		on	Slot.SlotUniqueID = SlotMaxAppts.SlotUniqueID

		left join WH.OP.SessionMaxApptsPerSlot SessionMaxApptsPerSlot
		on	Slot.SessionUniqueID = SessionMaxApptsPerSlot.SessionUniqueID

		left join WH.OP.ClinicMaxApptsPerSlot ClinicMaxApptsPerSlot
		on	Slot.ServicePointUniqueID = ClinicMaxApptsPerSlot.ServicePointUniqueID
		
		where
			Slot.SlotStatusCode not in (1624,1625)
			
		group by
			Slot.SessionUniqueID

	) SlotBookings
on Session.SessionUniqueID = SlotBookings.SessionUniqueID

--Allowed Bookings
left join WH.OP.SessionMaxApptsPerSession SessionMaxApptsPerSession
on Session.SessionUniqueID = SessionMaxApptsPerSession.SessionUniqueID

left join WH.OP.ClinicMaxApptsPerSession ClinicMaxApptsPerSession
on	Session.ServicePointUniqueID = ClinicMaxApptsPerSession.ServicePointUniqueID

where
	Session.ArchiveFlag = 0
and	Session.IsTemplate = 0

and	Session.SessionDate <=
		coalesce(
			 Session.SessionEndDateTime
			,Session.SessionDate
		)

and	Session.SessionCancelledFlag = 0

and	Session.SessionStatusCode in
		(
		 1563	--	Session Held
		,1565	--	Session Scheduled
		,1566	--	Session Initiated
		)
