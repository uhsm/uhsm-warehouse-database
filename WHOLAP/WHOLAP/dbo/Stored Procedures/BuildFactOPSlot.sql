﻿create procedure [dbo].[BuildFactOPSlot] as

truncate table dbo.FactOPSlot

insert into dbo.FactOPSlot
(
	 SessionUniqueID
	,ServicePointUniqueID
	,SessionSpecialtyCode
	,SessionDate
	,SlotVisitTypeCode
	,SlotPriorityCode
	,BookedSlots
	,MaxNumberOfBookings
)
select
	 SessionUniqueID
	,ServicePointUniqueID
	,SessionSpecialtyCode
	,SessionDate
	,SlotVisitTypeCode
	,SlotPriorityCode
	,BookedSlots

	,MaxNumberOfBookings =
		cast(
			coalesce(
				 MaxNumberOfBookings.ReferenceValue
				,Activity.SlotMaxBookings
			)
			 as int
		)

from
	(
	select
		 Session.SessionUniqueID
		,Session.ServicePointUniqueID
		,Session.SessionSpecialtyCode
		,Session.SessionDate


		,SlotVisitTypeCode =
			coalesce(
				 VisitTypeBySlot.VisitTypeRuleCode
				,VisitTypeBySession.VisitTypeRuleCode
				,VisitTypeByServicePoint.VisitTypeRuleCode
				,8911 --Follow-Up
			)

		,SlotPriorityCode =
			coalesce(
				 PriorityBySlot.PriorityRuleCode
				,PriorityBySession.PriorityRuleCode
				,PriorityByServicePoint.PriorityRuleCode
				,3127 --routine
			)

		,BookedSlots =
			isnull(
				 Slot.SlotNumberofBookings
				,0
			)

		,MaxNumberOfBookingsCode =
			coalesce(
				 MaxNumberOfBookingsBySlot.VisitTypeRuleCode
				,MaxNumberOfBookingsBySession.VisitTypeRuleCode
				,MaxNumberOfBookingsByServicePoint.VisitTypeRuleCode
			)

		,Slot.SlotMaxBookings

	from
		WH.OP.Session


	--Number of Bookings
	left join WH.OP.Slot Slot
	on Slot.SessionUniqueID = Session.SessionUniqueID
	and	Slot.SlotStatusCode not in 
		(
		 1625	--Reserved
		,1624	--No Longer Available
		)


	--slot visit type
	left join 
		(
		select
			 SlotUniqueID = VisitTypeRule.AppliedToUniqueID
			,VisitTypeRuleCode = VisitTypeRule.RuleValueCode
		from
			WH.OP.[Rule] VisitTypeRule
		where
			VisitTypeRule.ArchiveFlag = 0
		and	VisitTypeRule.AppliedTo = 'SPSLT'
		and	VisitTypeRule.RuleAppliedUniqueID = 14 --visit type
		and	VisitTypeRule.EnforcedFlag = 'Y'
		and	not exists
			(
			select
				1
			from
				WH.OP.[Rule] Previous
			where
				Previous.AppliedToUniqueID = VisitTypeRule.AppliedToUniqueID
			and	Previous.ArchiveFlag = 0
			and	Previous.AppliedTo = 'SPSLT'
			and	Previous.RuleAppliedUniqueID = 14 --visit type
			and	Previous.EnforcedFlag = 'Y'
			and	Previous.RuleRecno > VisitTypeRule.RuleRecno
			)
		) VisitTypeBySlot
	on	VisitTypeBySlot.SlotUniqueID = Slot.SlotUniqueID

	left join 
		(
		select
			 SessionUniqueID = VisitTypeRule.AppliedToUniqueID
			,VisitTypeRuleCode = VisitTypeRule.RuleValueCode
		from
			WH.OP.[Rule] VisitTypeRule
		where
			VisitTypeRule.ArchiveFlag = 0
		and	VisitTypeRule.AppliedTo = 'SPSSN'
		and	VisitTypeRule.RuleAppliedUniqueID = 14 --visit type
		and	VisitTypeRule.EnforcedFlag = 'Y'
		and	not exists
			(
			select
				1
			from
				WH.OP.[Rule] Previous
			where
				Previous.AppliedToUniqueID = VisitTypeRule.AppliedToUniqueID
			and	Previous.ArchiveFlag = 0
			and	Previous.AppliedTo = 'SPSSN'
			and	Previous.RuleAppliedUniqueID = 14 --visit type
			and	Previous.EnforcedFlag = 'Y'
			and	Previous.RuleRecno > VisitTypeRule.RuleRecno
			)
		) VisitTypeBySession
	on	VisitTypeBySession.SessionUniqueID = Slot.SessionUniqueID

	left join 
		(
		select
			 ServicePointUniqueID = VisitTypeRule.AppliedToUniqueID
			,VisitTypeRuleCode = VisitTypeRule.RuleValueCode
		from
			WH.OP.[Rule] VisitTypeRule
		where
			VisitTypeRule.ArchiveFlag = 0
		and	VisitTypeRule.AppliedTo = 'SPONT'
		and	VisitTypeRule.RuleAppliedUniqueID = 14 --visit type
		and	VisitTypeRule.EnforcedFlag = 'Y'
		and	not exists
			(
			select
				1
			from
				WH.OP.[Rule] Previous
			where
				Previous.AppliedToUniqueID = VisitTypeRule.AppliedToUniqueID
			and	Previous.ArchiveFlag = 0
			and	Previous.AppliedTo = 'SPONT'
			and	Previous.RuleAppliedUniqueID = 14 --visit type
			and	Previous.EnforcedFlag = 'Y'
			and	Previous.RuleRecno > VisitTypeRule.RuleRecno
			)
		) VisitTypeByServicePoint
	on	VisitTypeByServicePoint.ServicePointUniqueID = Slot.ServicePointUniqueID


	--slot priority
	left join 
		(
		select
			 SlotUniqueID = PriorityRule.AppliedToUniqueID
			,PriorityRuleCode = PriorityRule.RuleValueCode
		from
			WH.OP.[Rule] PriorityRule
		where
			PriorityRule.ArchiveFlag = 0
		and	PriorityRule.AppliedTo = 'SPSLT'
		and	PriorityRule.RuleAppliedUniqueID = 10 --visit type
		and	PriorityRule.EnforcedFlag = 'Y'
		and	not exists
			(
			select
				1
			from
				WH.OP.[Rule] Previous
			where
				Previous.AppliedToUniqueID = PriorityRule.AppliedToUniqueID
			and	Previous.ArchiveFlag = 0
			and	Previous.AppliedTo = 'SPSLT'
			and	Previous.RuleAppliedUniqueID = 10 --visit type
			and	Previous.EnforcedFlag = 'Y'
			and	Previous.RuleRecno > PriorityRule.RuleRecno
			)
		) PriorityBySlot
	on	PriorityBySlot.SlotUniqueID = Slot.SlotUniqueID

	left join 
		(
		select
			 SessionUniqueID = PriorityRule.AppliedToUniqueID
			,PriorityRuleCode = PriorityRule.RuleValueCode
		from
			WH.OP.[Rule] PriorityRule
		where
			PriorityRule.ArchiveFlag = 0
		and	PriorityRule.AppliedTo = 'SPSSN'
		and	PriorityRule.RuleAppliedUniqueID = 10 --visit type
		and	PriorityRule.EnforcedFlag = 'Y'
		and	not exists
			(
			select
				1
			from
				WH.OP.[Rule] Previous
			where
				Previous.AppliedToUniqueID = PriorityRule.AppliedToUniqueID
			and	Previous.ArchiveFlag = 0
			and	Previous.AppliedTo = 'SPSSN'
			and	Previous.RuleAppliedUniqueID = 10 --visit type
			and	Previous.EnforcedFlag = 'Y'
			and	Previous.RuleRecno > PriorityRule.RuleRecno
			)
		) PriorityBySession
	on	PriorityBySession.SessionUniqueID = Slot.SessionUniqueID

	left join 
		(
		select
			 ServicePointUniqueID = PriorityRule.AppliedToUniqueID
			,PriorityRuleCode = PriorityRule.RuleValueCode
		from
			WH.OP.[Rule] PriorityRule
		where
			PriorityRule.ArchiveFlag = 0
		and	PriorityRule.AppliedTo = 'SPONT'
		and	PriorityRule.RuleAppliedUniqueID = 10 --visit type
		and	PriorityRule.EnforcedFlag = 'Y'
		and	not exists
			(
			select
				1
			from
				WH.OP.[Rule] Previous
			where
				Previous.AppliedToUniqueID = PriorityRule.AppliedToUniqueID
			and	Previous.ArchiveFlag = 0
			and	Previous.AppliedTo = 'SPONT'
			and	Previous.RuleAppliedUniqueID = 10 --visit type
			and	Previous.EnforcedFlag = 'Y'
			and	Previous.RuleRecno > PriorityRule.RuleRecno
			)
		) PriorityByServicePoint
	on	PriorityByServicePoint.ServicePointUniqueID = Slot.ServicePointUniqueID



	--max bookings per slot
	left join 
		(
		select
			 SlotUniqueID = VisitTypeRule.AppliedToUniqueID
			,VisitTypeRuleCode = VisitTypeRule.RuleValueCode
		from
			WH.OP.[Rule] VisitTypeRule
		where
			VisitTypeRule.ArchiveFlag = 0
		and	VisitTypeRule.AppliedTo = 'SPSLT'
		and	VisitTypeRule.RuleAppliedUniqueID = 6 --max bookings
		and	VisitTypeRule.EnforcedFlag = 'Y'
		and	not exists
			(
			select
				1
			from
				WH.OP.[Rule] Previous
			where
				Previous.AppliedToUniqueID = VisitTypeRule.AppliedToUniqueID
			and	Previous.ArchiveFlag = 0
			and	Previous.AppliedTo = 'SPSLT'
			and	Previous.RuleAppliedUniqueID = 6 --max bookings
			and	Previous.EnforcedFlag = 'Y'
			and	Previous.RuleRecno > VisitTypeRule.RuleRecno
			)
		) MaxNumberOfBookingsBySlot
	on	MaxNumberOfBookingsBySlot.SlotUniqueID = Slot.SlotUniqueID

	left join 
		(
		select
			 SessionUniqueID = VisitTypeRule.AppliedToUniqueID
			,VisitTypeRuleCode = VisitTypeRule.RuleValueCode
		from
			WH.OP.[Rule] VisitTypeRule
		where
			VisitTypeRule.ArchiveFlag = 0
		and	VisitTypeRule.AppliedTo = 'SPSSN'
		and	VisitTypeRule.RuleAppliedUniqueID = 6 --max bookings
		and	VisitTypeRule.EnforcedFlag = 'Y'
		and	not exists
			(
			select
				1
			from
				WH.OP.[Rule] Previous
			where
				Previous.AppliedToUniqueID = VisitTypeRule.AppliedToUniqueID
			and	Previous.ArchiveFlag = 0
			and	Previous.AppliedTo = 'SPSSN'
			and	Previous.RuleAppliedUniqueID = 6 --max bookings
			and	Previous.EnforcedFlag = 'Y'
			and	Previous.RuleRecno > VisitTypeRule.RuleRecno
			)
		) MaxNumberOfBookingsBySession
	on	MaxNumberOfBookingsBySession.SessionUniqueID = Slot.SessionUniqueID

	left join 
		(
		select
			 ServicePointUniqueID = VisitTypeRule.AppliedToUniqueID
			,VisitTypeRuleCode = VisitTypeRule.RuleValueCode
		from
			WH.OP.[Rule] VisitTypeRule
		where
			VisitTypeRule.ArchiveFlag = 0
		and	VisitTypeRule.AppliedTo = 'SPONT'
		and	VisitTypeRule.RuleAppliedUniqueID = 6 --max bookings
		and	VisitTypeRule.EnforcedFlag = 'Y'
		and	not exists
			(
			select
				1
			from
				WH.OP.[Rule] Previous
			where
				Previous.AppliedToUniqueID = VisitTypeRule.AppliedToUniqueID
			and	Previous.ArchiveFlag = 0
			and	Previous.AppliedTo = 'SPONT'
			and	Previous.RuleAppliedUniqueID = 6 --max bookings
			and	Previous.EnforcedFlag = 'Y'
			and	Previous.RuleRecno > VisitTypeRule.RuleRecno
			)
		) MaxNumberOfBookingsByServicePoint
	on	MaxNumberOfBookingsByServicePoint.ServicePointUniqueID = Slot.ServicePointUniqueID

	where
		Session.ArchiveFlag = 0
	and	Session.IsTemplate = 0

	and	Session.SessionDate <=
			coalesce(
				 Session.SessionEndDateTime
				,Session.SessionDate
			)

	and	Session.SessionCancelledFlag = 0

	and	Session.SessionStatusCode in
			(
			 1563	--	Session Held
			,1565	--	Session Scheduled
			,1566	--	Session Initiated
			)

--	and	Session.SessionDate = '29 feb 2012'
	) Activity

left join WH.PAS.ReferenceValue MaxNumberOfBookings
on	MaxNumberOfBookings.ReferenceValueCode = Activity.MaxNumberOfBookingsCode
