﻿Create Procedure BuildFactOPSlotUtilisation as
Select 
slot.SlotUniqueID 
,MaxApptsPerSlot = Coalesce(slMaxAppts.MaxAppts,ssMaxAppts.MaxAppts,cmaxappts.maxappts,1),
COALESCE(slVP.New, ssVP.New, cVP.New, 0) As [New],
COALESCE(slVP.[Follow UP], ssVP.[Follow UP], cVP.[Follow UP], 1) As [FollowUp],
COALESCE(slVP.[Walk-in New], ssVP.[Walk-in New], cVP.[Walk-in New], 0) As [WalkInNew],
COALESCE(slVP.[Walk-in Follow Up], ssVP.[Walk-in Follow Up], cVP.[Walk-in Follow Up], 0) As [WalkInFollowUp],
COALESCE(slVP.[Walk-in], ssVP.[Walk-in], cVP.[Walk-in], 0) As [WalkIn],
COALESCE(slVP.[Walk-in A&E], ssVP.[Walk-in A&E], cVP.[Walk-in A&E], 0) As [WalkInAE],
COALESCE(slVP.[A&E attendance], ssVP.[A&E attendance], cVP.[A&E attendance], 0) As [AEAttendance],
COALESCE(slVP.[Panel], ssVP.[Panel], cVP.[Panel], 0) As [Panel],
COALESCE(slVP.[Visit Type Not Specified], ssVP.[Visit Type Not Specified], cVP.[Visit Type Not Specified], 0) As [Visit Type Not Specified],
COALESCE(slVP.[Urgent], ssVP.[Urgent], cVP.[Urgent], 0) As [Urgent],
COALESCE(slVP.[Routine], ssVP.[Routine], cVP.[Routine], 1) As [Routine],
COALESCE(slVP.[2 Week Rule], ssVP.[2 Week Rule], cVP.[2 Week Rule], 0) As [2 Week Rule],
COALESCE(slVP.[Accident & Emergency], ssVP.[Accident & Emergency], cVP.[Accident & Emergency], 0) As [Accident & Emergency],
COALESCE(slVP.[Priority Not Specified], ssVP.[Priority Not Specified], cVP.[Priority Not Specified], 0) As [Priority Not Specified]


from WH.OP.Slot slot
left outer join WH.OP.SlotMaxAppts slMAxAppts
on slot.SlotUniqueID = slMAxAppts.SlotUniqueID
left outer join WH.OP.SessionMaxApptsPerSlot ssMaxAppts
on slot.SessionUniqueID = ssMaxAppts.SessionUniqueID
left outer join WH.OP.ClinicMaxApptsPerSlot cmaxappts
on slot.ServicePointUniqueID = cmaxappts.ServicePointUniqueID
left outer join WH.OP.SlotPriorityAndVisitType slVP
on slot.SlotUniqueID = slVP.SlotUniqueID
left outer join WH.OP.SessionPriorityAndVisitType ssVP
on slot.SessionUniqueID = ssVP.SessionUniqueID
left outer join WH.OP.ClinicPriorityAndVisitType cVP
on slot.ServicePointUniqueID = cVP.ServicePointUniqueID
