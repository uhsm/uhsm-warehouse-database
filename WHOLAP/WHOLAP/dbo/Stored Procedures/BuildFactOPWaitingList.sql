﻿CREATE procedure [dbo].[BuildFactOPWaitingList] as

declare @StartTime datetime
declare @Elapsed int
--declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

truncate table dbo.FactOPWaitingList

insert into dbo.FactOPWaitingList
(
	 EncounterRecno
	,Cases
	,LengthOfWait
	,AgeCode
	,CensusDate
	,ConsultantCode
	,SpecialtyCode
	,DurationCode
	,PracticeCode
	,SiteCode
	,SourceOfReferralCode
	,WithAppointment
)
SELECT
	 Encounter.EncounterRecno
	,Encounter.Cases
	,Encounter.LengthOfWait
	,Encounter.AgeCode
	,Encounter.CensusDate
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.DurationCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.WithAppointment
FROM
	dbo.OlapOPWaitingList Encounter


select @RowsInserted = @@rowcount


exec BuildOlapPASSpecialty 'OPWL'
exec BuildOlapPASConsultant 'OPWL'
exec BuildOlapPASPractice 'OPWL'
exec BuildOlapCensus


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WHOLAP BuildFactOPWaitingList', @Stats, @StartTime
