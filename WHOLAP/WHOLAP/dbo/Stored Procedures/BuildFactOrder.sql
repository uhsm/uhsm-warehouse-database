﻿CREATE procedure [dbo].[BuildFactOrder] as
/*************************************************************************************************
	2011-01-25 KD 
		Added Criteria to Performed for Non Null Dictation Dates
	2014-12-22 KO
		Added update statement at end to handle dodgy radiology exam codes

*************************************************************************************************/
declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table dbo.FactOrder

insert into dbo.FactOrder
(
	 EncounterRecno
	,ModalityCode
	,ExamCode
	,OrderStaffCode
	,OrderLocationCode
	,OrderDate
	,PerformStaffCode
	,PerformLocationCode
	,PerformDate
	,PerformTimeBandCode
	,ReportStaffCode
	,ReportLocationCode
	,ReportDate
	,ReportTimeBandCode
	,ReportBreachFlag
	,Ordered
	,Performed
	,Reported
	,PerformanceDays
	,ReportingDays
	,DictationDate
	,TechnicianCode
	,CaseGroupCode
)

select
	 RadiologyOrder.EncounterRecno
	,ModalityCode = case when casetypecode like ('CM%') then left (RadiologyOrder.CaseTypeCode, 2) else left (RadiologyOrder.CaseTypeCode, 1) end
	,ExamCode = RadiologyOrder.OrderExamCode

	,OrderStaffCode =
		coalesce(
			RadiologyOrder.OrderingPhysicianCode
			,'-1'
		)

	,OrderLocationCode =
		coalesce(
			RadiologyOrder.OrderingLocationCode
			,'-1'
		)

	,OrderDate = 
		coalesce(
			RadiologyOrder.CreationDate
			,'1 Jan 1900'
		)



	,PerformStaffCode =
		coalesce(
			RadiologyOrder.RadiologistCode
			,'-1'
		)

	,PerformLocationCode =
		coalesce(
			RadiologyOrder.PerformanceLocation
			,'-1'
		)

	,PerformDate = 
		coalesce(
			RadiologyOrder.PerformanceDate
			,'1 Jan 1900'
		)


	,PerformTimeBandCode =
		case
		when 
			DATEDIFF(
				day
				,RadiologyOrder.CreationDate
				,RadiologyOrder.PerformanceDate
			)
			 is null
		then -1
		when 
			DATEDIFF(
				day
				,RadiologyOrder.CreationDate
				,RadiologyOrder.PerformanceDate
			)
			 < 0
		then -1
		when 
			DATEDIFF(
				day
				,RadiologyOrder.CreationDate
				,RadiologyOrder.PerformanceDate
			)
			 > 30
		then 31
		else
			DATEDIFF(
				day
				,RadiologyOrder.CreationDate
				,RadiologyOrder.PerformanceDate
			)
		end



	,ReportStaffCode =
		coalesce(
			RadiologyOrder.RadiologistCode
			,'-1'
		)

	,ReportLocationCode =
		coalesce(
			RadiologyOrder.PerformanceLocation
			,'-1'
		)

	,ReportDate = 
		coalesce(
			RadiologyOrder.FinalDate
			,'1 Jan 1900'
		)

	,ReportTimeBandCode =
		case
		when
			RadiologyOrder.CaseTypeCode = 'X'
		and	RadiologyOrder.OrderingLocationCode = 'FRA'
		then 0
		when 
			DATEDIFF(
				day
				,RadiologyOrder.PerformanceDate
				,RadiologyOrder.FinalDate
			)
			 is null
		then -1
		when 
			DATEDIFF(
				day
				,RadiologyOrder.PerformanceDate
				,RadiologyOrder.FinalDate
			)
			 < 0
		then -1
		when 
			DATEDIFF(
				day
				,RadiologyOrder.PerformanceDate
				,RadiologyOrder.FinalDate
			)
			 > 30
		then 31
		else
			DATEDIFF(
				day
				,RadiologyOrder.PerformanceDate
				,RadiologyOrder.FinalDate
			)
		end


	,ReportBreachFlag =
		CONVERT(
			bit
			,case
			when RadiologyOrder.FinalDate is null
			then 0

			when
				RadiologyOrder.CaseTypeCode = 'X'
			and	RadiologyOrder.OrderingLocationCode = 'FRA'
			then 0

			when
				DATEDIFF(
					day
					,RadiologyOrder.PerformanceDate
					,RadiologyOrder.FinalDate
				) >
				(
				select
					Breach.BreachValue
				from
					dbo.EntityBreach Breach

				inner join WH.MISYS.Location
				on	Location.LocationCode = RadiologyOrder.OrderingLocationCode

				where
					Breach.EntityTypeCode = 'MISYSREPORTBREACH'
				and	Breach.EntityCode = Location.LocationCategoryCode
				)
			then 1
			else 0
			end
		)
		

	,Ordered = 1
--KD 2011-01-25 Added Criteria for Dictation Date
	,Performed =
		case
		when (RadiologyOrder.PerformanceLocation is null and RadiologyOrder.DictationDate Is Null)
		then 
			case when RadiologyOrder.CaseTypeCode = 'R' then 1
			Else 0
			end
		else 
		1	
		end

	,Reported =
		case
		when RadiologyOrder.FinalDate is null
		then 0
		else 1
		end

	,PerformanceDays =
		case
		when
			RadiologyOrder.CaseTypeCode = 'X'
		and	RadiologyOrder.OrderingLocationCode = 'FRA'
		then 0

		when 
			DATEDIFF(
				day
				,RadiologyOrder.CreationDate
				,RadiologyOrder.PerformanceDate
			)
			 < 0
		then null
		else
			DATEDIFF(
				day
				,RadiologyOrder.CreationDate
				,RadiologyOrder.PerformanceDate
			)
		end


	,ReportingDays =
		case
		when
			RadiologyOrder.CaseTypeCode = 'X'
		and	RadiologyOrder.OrderingLocationCode = 'FRA'
		then 0

		when 
			DATEDIFF(
				day
				,RadiologyOrder.PerformanceDate
				,RadiologyOrder.FinalDate
			)
			 < 0
		then null
		else
			DATEDIFF(
				day
				,RadiologyOrder.PerformanceDate
				,RadiologyOrder.FinalDate
			)
		end

	,DictationDate = 
		coalesce(
			RadiologyOrder.DictationDate
			,'1 Jan 1900'
		)

	,TechnicianCode =
		coalesce(
			RadiologyOrder.FilmTechCode
			,-1
		)
	,RadiologyOrder.CaseGroupCode
from
	dbo.BaseOrder RadiologyOrder


select @RowsInserted = @@rowcount

--KO added 22/12/2014
--to handle obsolete exam codes
update WHOLAP.dbo.FactOrder set ExamCode = 'VASC'
where ExamCode not in (
select ExamCode from [WHOLAP].[dbo].[OlapMISYSExam])

select
	 @from = min(FromDate)
	,@to = max(ToDate)
from
	(
	select
		 FromDate = min(EncounterFact.OrderDate)
		,ToDate = max(EncounterFact.OrderDate)
	from
		dbo.FactOrder EncounterFact
	where
		EncounterFact.OrderDate <> '1 Jan 1900'

	union all

	select
		 min(EncounterFact.PerformDate)
		,max(EncounterFact.PerformDate)
	from
		dbo.FactOrder EncounterFact
	where
		EncounterFact.PerformDate <> '1 Jan 1900'

	union all

	select
		 min(EncounterFact.ReportDate)
		,max(EncounterFact.ReportDate)
	from
		dbo.FactOrder EncounterFact
	where
		EncounterFact.ReportDate <> '1 Jan 1900'
	) Dates



exec BuildCalendarBase @from, @to


select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
		CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'BuildFactOrder', @Stats, @StartTime




