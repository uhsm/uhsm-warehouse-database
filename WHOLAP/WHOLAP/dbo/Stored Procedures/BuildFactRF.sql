﻿CREATE procedure [dbo].[BuildFactRF] as

declare @StartTime datetime
declare @Elapsed int
--declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

/********************************************************************************
	2011.02.08 - KD
	Added:
		ReferralPriorityCode
	2011.01.10 - KD
	Added:
		ReferralType
		Responsible Organisation Code
********************************************************************************/

		

select @StartTime = getdate()

truncate table dbo.FactRF

insert into dbo.FactRF
(
	 EncounterRecno
	,EncounterDate
	,ConsultantCode
	,SpecialtyCode
	,PracticeCode
--	,PCTCode
	,AgeCode
	,SiteCode
	,SourceOfReferralCode
	,Cases
	,ReferralTypeCode
	,ResponsibleOrganisationCode
	,ReferralPriorityCode
	,ReferredByConsultantCode 
	,ReferredByOrganisationCode 
	,ReferredBySpecialtyCode 
	,ReferralCategoryCode
	
)
SELECT
	 Encounter.EncounterRecno
	,EncounterDate = dateadd(day, datediff(day, 0, ReferralDate), 0)
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
--	,Encounter.PCTCode
	,Encounter.AgeCode
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.Cases
	,ReferralTypeCode = 
	--1	Outpatient - EBS
	--2	Outpatient - Other
	--3	Other  (referral for any other Service than outpatient)
	--4	Cancelled
	--5	Duplicate
	--6	Self Referring Baby
	--7	N/A
		
		Case
			When (Not CancellationDate Is null )Then 4
			When (Not CancellationReason In (62,0)) Then 4
			When SelfReferringBaby = 1 then 6
			When Duplicate = 1 Then 5
			When Encounter.RequestedService = 232 and not ReferralMediumCode IN (2004336,2004336) Then 2
			When (SourceOfReferralCode = 2002324 or ReferralMediumCode IN (2004336,2004336)) Then 1
			Else 3
		End
	,Encounter.ResponsibleOrganisationCode
	,Encounter.PriorityCode
	,Encounter.ReferredByConsultantCode 
	,Encounter.ReferredByOrganisationCode 
	,Encounter.ReferredBySpecialtyCode 
	,RefCat.ReferralCategoryCode
FROM
	BaseRF Encounter
Left outer join dbo.olapPASReferralCategory RefCat
on Encounter.RequestedService = RefCat.RequestedServiceCode
and Encounter.ReferralTypeCode = RefCat.ReferralTypeCode


select @RowsInserted = @@rowcount


select
	 @from = min(EncounterFact.EncounterDate)
	,@to = max(EncounterFact.EncounterDate)
from
	dbo.FactRF EncounterFact
where
	EncounterFact.EncounterDate is not null


exec BuildOlapPASSpecialty 'RF'
exec BuildOlapPASConsultant 'RF'
exec BuildOlapPASPractice 'RF'
exec BuildCalendarBase @from, @to

--Add Codes into Responsible Organisation Where they don't exist
	Insert Into OlapiPMResponsibleOrganisation
	Select Distinct
	Ref.ResponsibleOrganisationCode
	,'Unknown'
	,'Source - ' + Ref.ResponsibleOrganisationSource
	from BaseRF Ref
	left outer join OlapiPMResponsibleOrganisation Org
	on Ref.ResponsibleOrganisationCode = Org.OrganisationCode
	where Org.OrganisationCode is null

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
		CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'PAS - WHOLAP BuildFactRF', @Stats, @StartTime



