﻿CREATE procedure [dbo].[BuildFactRTT]
	@censusDate smalldatetime
as


declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()


delete from dbo.FactRTT
where
	CensusDate = @censusDate
SELECT @RowsDeleted = @@Rowcount

--delete everything >3 months old --PDO 24 Aug 2011
delete from dbo.FactRTT
where
	CensusDate <= dateadd(month, -3, @censusDate)
SELECT @RowsDeleted = @RowsDeleted + @@Rowcount

insert into dbo.FactRTT
(
	 CensusDate
	,SourceEncounterRecno
	,SourceCode
	,AdjustedFlag
	,ConsultantCode
	,SpecialtyCode
	,PracticeCode
	,PathwayStatusCode
	,WeekBandCode
	,BreachBandCode
	,Cases
	,LastWeeksCases
	,KeyDate
	,OperationCode
	,ReferringProviderCode
	,DaysWaiting
)
select
	 CensusDate
	,SourceEncounterRecno
	,SourceCode

	,AdjustedFlag =
		convert(bit, 0)

	,ConsultantCode =
		coalesce(
			ConsultantCode
			,-1
		)

	,SpecialtyCode =
		coalesce(
			MappedSpecialtyCode
			,SpecialtyCode
			,-1
		)

	,PracticeCode =
		coalesce(
			RegisteredGpPracticeCode
			,-1
		)

	,PathwayStatusCode

	,WeekBandCode =
		case
		when Encounter.ClockStartDate is null then '53' -- patients with unknown clock start date
		when datediff(day, Encounter.ClockStartDate, Encounter.KeyDate) < 0 then '00'
		when datediff(day, Encounter.ClockStartDate, Encounter.KeyDate) = 126 then '17'
		when datediff(day, Encounter.ClockStartDate, Encounter.KeyDate) / 7 > 51 then '52'
		else right('0' + convert(varchar, datediff(day, Encounter.ClockStartDate, Encounter.KeyDate) / 7), 2)
		end

	,BreachBandCode =
		case
		when Encounter.ClockStartDate is null then 'UK' -- patients with unknown clock start date
		when datediff(day, dateadd(day, 126, Encounter.ClockStartDate), Encounter.KeyDate) between 1 and 7 then 'BR01'
		when datediff(day, dateadd(day, 126, Encounter.ClockStartDate), Encounter.KeyDate) > 7 then 'BR02'
		when datediff(day, dateadd(day, 126, Encounter.ClockStartDate), Encounter.KeyDate) between -7 and 0 then 'F01'
		when datediff(day, dateadd(day, 126, Encounter.ClockStartDate), Encounter.KeyDate) between -14 and -8 then 'F12'
		when datediff(day, dateadd(day, 126, Encounter.ClockStartDate), Encounter.KeyDate) between -28 and -15 then 'F24'
		when datediff(day, dateadd(day, 126, Encounter.ClockStartDate), Encounter.KeyDate) between -42 and -29 then 'F46'
		when datediff(day, dateadd(day, 126, Encounter.ClockStartDate), Encounter.KeyDate) between -56 and -43 then 'F68'
		else 'F99'
		end

	,Cases =
		case
		when
			datediff(
				 month
				,Encounter.KeyDate
				,Encounter.CensusDate
			) = 0
		then Encounter.Cases
		else 0
		end

	,LastWeeksCases =
		case
		when
			datediff(
				 day
				,Encounter.KeyDate
				,Encounter.CensusDate
			) < 8
		then Encounter.Cases
		else 0
		end

	,Encounter.KeyDate

	,OperationCode =
		coalesce(
			 Encounter.PASPrimaryOperationCode
			,-1
		)

	,ReferringProviderCode =
		coalesce(
			 left(Encounter.RTTPathwayID, 3)
			,'##'
		)

	,DaysWaiting =
		datediff(
			 day
			,Encounter.ClockStartDate
			,Encounter.KeyDate
		)

from
	dbo.BaseRTT Encounter
where
	Encounter.CensusDate = @censusDate

union all

select
	 CensusDate
	,SourceEncounterRecno
	,SourceCode

	,AdjustedFlag =
		convert(bit, 1)

	,ConsultantCode =
		coalesce(
			ConsultantCode
			,-1
		)

	,SpecialtyCode =
		coalesce(
			MappedSpecialtyCode
			,SpecialtyCode
			,-1
		)

	,PracticeCode =
		coalesce(
			RegisteredGpPracticeCode
			,-1
		)

	,PathwayStatusCode

	,WeekBandCode =
		case
		when Encounter.ClockStartDate is null then '53' -- patients with unknown clock start date
		when datediff(day, dateadd(day, Encounter.SocialSuspensionDays, Encounter.ClockStartDate), Encounter.KeyDate) < 0 then '00'
		when datediff(day, dateadd(day, Encounter.SocialSuspensionDays, Encounter.ClockStartDate), Encounter.KeyDate) = 126 then '17'
		when datediff(day, dateadd(day, Encounter.SocialSuspensionDays, Encounter.ClockStartDate), Encounter.KeyDate) / 7 > 51 then '52'
		else right('0' + convert(varchar, datediff(day, dateadd(day, Encounter.SocialSuspensionDays, Encounter.ClockStartDate), Encounter.KeyDate) / 7), 2)
		end

	,BreachBandCode =
		case
		when Encounter.ClockStartDate is null then 'UK' -- patients with unknown clock start date
		when datediff(day, dateadd(day, 126, dateadd(day, Encounter.SocialSuspensionDays, Encounter.ClockStartDate)), Encounter.KeyDate) between 1 and 7 then 'BR01'
		when datediff(day, dateadd(day, 126, dateadd(day, Encounter.SocialSuspensionDays, Encounter.ClockStartDate)), Encounter.KeyDate) > 7 then 'BR02'
		when datediff(day, dateadd(day, 126, dateadd(day, Encounter.SocialSuspensionDays, Encounter.ClockStartDate)), Encounter.KeyDate) between -7 and 0 then 'F01'
		when datediff(day, dateadd(day, 126, dateadd(day, Encounter.SocialSuspensionDays, Encounter.ClockStartDate)), Encounter.KeyDate) between -14 and -8 then 'F12'
		when datediff(day, dateadd(day, 126, dateadd(day, Encounter.SocialSuspensionDays, Encounter.ClockStartDate)), Encounter.KeyDate) between -28 and -15 then 'F24'
		when datediff(day, dateadd(day, 126, dateadd(day, Encounter.SocialSuspensionDays, Encounter.ClockStartDate)), Encounter.KeyDate) between -42 and -29 then 'F46'
		when datediff(day, dateadd(day, 126, dateadd(day, Encounter.SocialSuspensionDays, Encounter.ClockStartDate)), Encounter.KeyDate) between -56 and -43 then 'F68'
		else 'F99'
		end

	,Cases =
		case
		when
			datediff(
				 month
				,Encounter.KeyDate
				,Encounter.CensusDate
			) = 0
		then Encounter.Cases
		else 0
		end

	,LastWeeksCases =
		case
		when
			datediff(
				 day
				,Encounter.KeyDate
				,Encounter.CensusDate
			) < 8
		then Encounter.Cases
		else 0
		end

	,Encounter.KeyDate

	,OperationCode =
		coalesce(
			 Encounter.PASPrimaryOperationCode
			,-1
		)

	,ReferringProviderCode =
		coalesce(
			 left(Encounter.RTTPathwayID, 3)
			,'##'
		)

	,DaysWaiting =
		datediff(
			 day
			,dateadd(day, Encounter.SocialSuspensionDays, Encounter.ClockStartDate)
			,Encounter.KeyDate
		)

from
	dbo.BaseRTT Encounter
where
	Encounter.CensusDate = @censusDate

select @RowsInserted = @@rowcount


exec BuildOlapPASOperation 'RTT'
exec BuildOlapPASSpecialty 'RTT'
exec BuildOlapPASConsultant 'RTT'
exec BuildOlapPASPractice 'RTT'
exec BuildOlapCensus


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins' + ', ' + 
	'Period ' + CONVERT(varchar(10), @censusDate)

exec WriteAuditLogEvent 'PAS - WHOLAP BuildFactRTT', @Stats, @StartTime
