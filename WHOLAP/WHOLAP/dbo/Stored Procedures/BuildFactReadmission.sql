﻿CREATE procedure [dbo].[BuildFactReadmission] as

declare @StartTime datetime
declare @Elapsed int
--declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table dbo.FactReadmission

insert into dbo.FactReadmission
(
	 ReadmissionRecno
	,ReadmissionAdmissionDate
	,ReadmissionConsultantCode
	,ReadmissionSpecialtyCode
	,ReadmissionPracticeCode
	,ReadmissionAdmissionMethodCode
	,Cases
	,PreviousAdmissionRecno
	,PreviousAdmissionDischargeDate
	,PreviousAdmissionConsultantCode
	,PreviousAdmissionSpecialtyCode
	,PreviousAdmissionPracticeCode
	,PreviousAdmissionAdmissionMethodCode
	,ReadmissionIntervalCode
	,ReadmissionIntervalDays
	,MetricCode
)
--Readmissions
SELECT
	 ReadmissionRecno
	,ReadmissionAdmissionDate
	,ReadmissionConsultantCode
	,ReadmissionSpecialtyCode
	,ReadmissionRegisteredGpPracticeCode
	,ReadmissionAdmissionMethodCode
	,Cases
	,PreviousAdmissionRecno
	,PreviousAdmissionDischargeDate
	,PreviousAdmissionConsultantCode
	,PreviousAdmissionSpecialtyCode
	,PreviousAdmissionRegisteredGpPracticeCode
	,PreviousAdmissionAdmissionMethodCode
	,ReadmissionIntervalCode
	,ReadmissionIntervalDays

	,MetricCode =
		case
		when ReadmissionSpecialtyCode = PreviousAdmissionSpecialtyCode 
		then 'SAMESPEC'
		else 'OTHERSPEC'
		end 
from
	dbo.OlapReadmissionEncounter Encounter


select @RowsInserted = @@rowcount


select
	 @from = min(EncounterFact.ReadmissionAdmissionDate)
	,@to = max(EncounterFact.ReadmissionAdmissionDate)
from
	dbo.FactReadmission EncounterFact
where
	EncounterFact.ReadmissionAdmissionDate is not null


exec BuildCalendarBase @from, @to


select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
		CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'PAS - WHOLAP BuildFactReadmission', @Stats, @StartTime

