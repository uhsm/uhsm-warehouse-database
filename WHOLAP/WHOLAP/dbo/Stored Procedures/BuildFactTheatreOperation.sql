﻿CREATE procedure [dbo].[BuildFactTheatreOperation] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table dbo.FactTheatreOperation

insert into dbo.FactTheatreOperation
	(
	 SessionSourceUniqueID
	,SourceUniqueID
	,TheatreCode
	,CancelReasonCode
	,ConsultantCode
	,EpisodicConsultantCode
	,SurgeonCode
	,AnaesthetistCode
	,PrimaryProcedureCode
	,AgeCode
	,SexCode
	,AdmissionTypeCode
	,OperationDate
	,OperationTypeCode
	,TheatreSessionPeriodCode
	,SessionConsultantCode
	,TimetableConsultantCode
	,TemplateConsultantCode
	,SessionAnaesthetistCode
	,TimetableAnaesthetistCode
	,TemplateAnaesthetistCode
	,SessionSpecialtyCode
	,TimetableSpecialtyCode
	,TemplateSpecialtyCode
	,Biohazard
	,ASAScoreCode
	,ReceptionTime
	,ReceptionWaitTime
	,AnaestheticWaitTime
	,AnaestheticTime
	,InORTime
	,OperationTime
	,OutOfORTime
	,RecoveryTime
	,ReturnTime
	,PatientTime
	,Operation
	,CancelledOperation
	,CancelledOperationOnDay
	,OperationProductiveTime
	,AnaestheticTypeCode
	,OperationProductiveTimeCalc2
	,OperationOrder
	)
select
	 SessionSourceUniqueID = convert(int, TheatreOperation.SessionSourceUniqueID)
	,TheatreOperation.SourceUniqueID
	,TheatreCode = coalesce(TheatreOperation.TheatreCode, 0)
	,CancelReasonCode = coalesce(TheatreOperation.CancelReasonCode, 0)
	,ConsultantCode = coalesce(TheatreOperation.ConsultantCode, 0)
	,EpisodicConsultantCode = coalesce(TheatreOperation.EpisodicConsultantCode, -1)
	,SurgeonCode = TheatreOperation.Surgeon1Code
	,AnaesthetistCode = TheatreOperation.Anaesthetist1Code
	,PrimaryProcedureCode = coalesce(TheatreOperation.PrimaryProcedureCode, '##')
	,TheatreOperation.AgeCode

	,SexCode =
		case
		when TheatreOperation.SexCode = ''
		then 'N' --N/A
		else TheatreOperation.SexCode
		end

	,TheatreOperation.AdmissionTypeCode

	,OperationDate =
		case
		when TheatreOperation.OperationDate > '6 Jun 2079' -- beyond smalldatetime
		then '1 Jan 1900' -- smallest smalldatetime
		else TheatreOperation.OperationDate
		end

	,OperationTypeCode = 
		case coalesce(TheatreOperation.OperationTypeCode, 'N/A')
		when ''
		then 'N/A'
		else TheatreOperation.OperationTypeCode
		end

	,TheatreSessionPeriodCode =
		case
		--when
		--	left(convert(varchar, Session.StartTime, 8), 5) between '00:00' and '12:29'
		--and	left(convert(varchar, Session.EndTime, 8), 5) > '16:29' then 'AD' --All Day

		when left(convert(varchar, Session.StartTime, 8), 5) between '00:00' and '12:29' then 'AM' --Morning
		when left(convert(varchar, Session.StartTime, 8), 5) between '12:30' and '16:29' then 'PM' --Afternoon
		when left(convert(varchar, Session.StartTime, 8), 5) between '16:30' and '23:59' then 'VN' --Evening
		else 'NA'
		end

	,SessionConsultantCode = coalesce(Session.ConsultantCode, 0)

	,TimetableConsultantCode = coalesce(Session.TimetableConsultantCode, 0)

	,TemplateConsultantCode = coalesce(Session.TemplateConsultantCode, 0)

	,SessionAnaesthetistCode = coalesce(Session.AnaesthetistCode1, 0)

	,TimetableAnaesthetistCode = coalesce(Session.TimetableAnaesthetistCode, 0)

	,TemplateAnaesthetistCode = coalesce(Session.TemplateAnaesthetistCode, 0)

	,SessionSpecialtyCode = coalesce(Session.SpecialtyCode, 0)

	,TimetableSpecialtyCode = coalesce(Session.TimetableSpecialtyCode, 0)

	,TemplateSpecialtyCode = coalesce(Session.TemplateSpecialtyCode, 0)

	,Biohazard = convert(bit, TheatreOperation.BiohazardFlag)

	,ASAScoreCode = coalesce(TheatreOperation.ASAScoreCode, 0)

	,ReceptionTime =
		datediff(minute, TheatreOperation.SentForTime, TheatreOperation.InSuiteTime)

	,ReceptionWaitTime =
		datediff(minute, TheatreOperation.InSuiteTime, TheatreOperation.InAnaestheticTime)

	,AnaestheticWaitTime =
		datediff(minute, TheatreOperation.InAnaestheticTime, TheatreOperation.AnaestheticInductionTime)

	,AnaestheticTime =
		datediff(minute, TheatreOperation.AnaestheticInductionTime, TheatreOperation.OperationStartDate)

	,InORTime =
		datediff(minute, TheatreOperation.OperationStartDate, TheatreOperation.ProcedureStartTime)

	,OperationTime =
		datediff(minute, TheatreOperation.ProcedureStartTime, TheatreOperation.ProcedureEndTime)

	,OutOfORTime =
		datediff(minute, TheatreOperation.ProcedureEndTime, TheatreOperation.InRecoveryTime)

	,RecoveryTime =
		datediff(minute, TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime)

	,ReturnTime =
		datediff(minute, TheatreOperation.ReadyToDepartTime, TheatreOperation.DischargeTime)

	,PatientTime =
		datediff(minute, TheatreOperation.AnaestheticInductionTime, TheatreOperation.ProcedureEndTime)

	,Operation = 1

	,CancelledOperation =
		case
		when TheatreOperation.OperationCancelledFlag = 0
		then 0
		else 1
		end

	,CancelledOperationOnDay =
		case
		when TheatreOperation.OperationCancelledFlag = 0
		then 0
		when cast(TheatreOperation.CancellationDate as date) = cast(TheatreOperation.OperationDate as date)
		then 1
		else 0
		end

	,OperationProductiveTime =
		case
		when
			datediff(minute, TheatreOperation.AnaestheticInductionTime, TheatreOperation.AnaestheticReadyTime) +
			datediff(minute, TheatreOperation.ProcedureStartTime, TheatreOperation.ProcedureEndTime)
			<
			datediff(minute, TheatreOperation.AnaestheticInductionTime, TheatreOperation.ProcedureEndTime)
		and	datediff(minute, TheatreOperation.AnaestheticInductionTime, TheatreOperation.AnaestheticReadyTime) +
			datediff(minute, TheatreOperation.ProcedureStartTime, TheatreOperation.ProcedureEndTime)
			> 0
		then
			datediff(minute, TheatreOperation.AnaestheticInductionTime, TheatreOperation.AnaestheticReadyTime) +
			datediff(minute, TheatreOperation.ProcedureStartTime, TheatreOperation.ProcedureEndTime)
		else
			datediff(minute, TheatreOperation.AnaestheticInductionTime, TheatreOperation.ProcedureEndTime)
		end
		--18/12/2013 amended productive time calculation which includes recovery time 
	--,OperationProductiveTime = 
	--	case
	--	when
	--		datediff(minute, TheatreOperation.AnaestheticInductionTime, TheatreOperation.AnaestheticReadyTime) +
	--		datediff(minute, TheatreOperation.ProcedureStartTime, TheatreOperation.ProcedureEndTime)
	--		<
	--		datediff(minute, TheatreOperation.AnaestheticInductionTime, TheatreOperation.ProcedureEndTime)
	--	and	datediff(minute, TheatreOperation.AnaestheticInductionTime, TheatreOperation.AnaestheticReadyTime) +
	--		datediff(minute, TheatreOperation.ProcedureStartTime, TheatreOperation.ProcedureEndTime)
	--		> 0
	--	then
	--		datediff(minute, TheatreOperation.AnaestheticInductionTime, TheatreOperation.AnaestheticReadyTime) +
	--		datediff(minute, TheatreOperation.ProcedureStartTime, TheatreOperation.ProcedureEndTime) +
	--		case when TheatreSpecialty.NationalSpecialtyCode = '110' and (DATEDIFF(minute,TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime) > 20 Or DATEDIFF(minute,TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime) IS NULL ) then '20'
	--			when TheatreSpecialty.NationalSpecialtyCode <> '110' and (DATEDIFF(minute,TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime) > 10 or DATEDIFF(minute,TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime) IS NULL) then '10'
	--			Else  DATEDIFF(minute,TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime)
	--			End
			
	--	else
	--		datediff(minute, TheatreOperation.AnaestheticInductionTime, TheatreOperation.ProcedureEndTime)+
	--		case when TheatreSpecialty.NationalSpecialtyCode = '110' and (DATEDIFF(minute,TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime) > 20 Or DATEDIFF(minute,TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime) IS NULL ) then '20'
	--			when TheatreSpecialty.NationalSpecialtyCode <> '110' and (DATEDIFF(minute,TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime) > 10 or DATEDIFF(minute,TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime) IS NULL) then '10'
	--			Else  DATEDIFF(minute,TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime)
	--			End
	--	end
	,AnaestheticTypeCode =
		coalesce(TheatreOperation.AnaestheticTypeCode, 'N/A')
	,OperationProductiveTimeCalc2 =	
	case
		when
			datediff(minute, TheatreOperation.AnaestheticInductionTime, TheatreOperation.AnaestheticReadyTime) +
			datediff(minute, TheatreOperation.OperationStartDate, TheatreOperation.ProcedureEndTime)
			<
			datediff(minute, TheatreOperation.AnaestheticInductionTime, TheatreOperation.ProcedureEndTime)
		and	datediff(minute, TheatreOperation.AnaestheticInductionTime, TheatreOperation.AnaestheticReadyTime) +
			datediff(minute, TheatreOperation.OperationStartDate, TheatreOperation.ProcedureEndTime)
			> 0
		then
			datediff(minute, TheatreOperation.AnaestheticInductionTime, TheatreOperation.AnaestheticReadyTime) +
			datediff(minute, TheatreOperation.OperationStartDate, TheatreOperation.ProcedureEndTime) +
			case when TheatreSpecialty.NationalSpecialtyCode = '110' and (DATEDIFF(minute,TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime) > 20 Or DATEDIFF(minute,TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime) IS NULL ) then '20'
				when TheatreSpecialty.NationalSpecialtyCode <> '110' and (DATEDIFF(minute,TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime) > 10 or DATEDIFF(minute,TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime) IS NULL) then '10'
				Else  DATEDIFF(minute,TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime)
				End
			
		else
			datediff(minute, TheatreOperation.AnaestheticInductionTime, TheatreOperation.ProcedureEndTime)+
			case when TheatreSpecialty.NationalSpecialtyCode = '110' and (DATEDIFF(minute,TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime) > 20 Or DATEDIFF(minute,TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime) IS NULL ) then '20'
				when TheatreSpecialty.NationalSpecialtyCode <> '110' and (DATEDIFF(minute,TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime) > 10 or DATEDIFF(minute,TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime) IS NULL) then '10'
				Else  DATEDIFF(minute,TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime)
				End
		end
		,OperationOrder= TheatreOperation.OperationOrder
from
	dbo.BaseTheatreOperation TheatreOperation

left join dbo.BaseTheatreSession Session
on	Session.SourceUniqueID = TheatreOperation.SessionSourceUniqueID
Left outer join [WHOLAP].[dbo].[OlapTheatreSpecialty] TheatreSpecialty 
on TheatreOperation.SpecialtyCode = TheatreSpecialty.SpecialtyCode


select @RowsInserted = @@rowcount


select
	 @from = min(EncounterFact.OperationDate)
	,@to = max(EncounterFact.OperationDate)
from
	dbo.FactTheatreOperation EncounterFact
where
	EncounterFact.OperationDate > '1 Jan 1900'


exec BuildCalendarBase @from, @to


select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
		CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'BuildFactTheatreOperation', @Stats, @StartTime
