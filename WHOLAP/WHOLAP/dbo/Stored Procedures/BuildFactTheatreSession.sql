﻿CREATE procedure [dbo].[BuildFactTheatreSession] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table dbo.FactTheatreSession

insert into dbo.FactTheatreSession
(
	 SourceUniqueID
	,TheatreCode
	,SessionDate
	,ConsultantCode
	,TimetableConsultantCode
	,TemplateConsultantCode
	,AnaesthetistCode
	,TimetableAnaesthetistCode
	,TemplateAnaesthetistCode
	,SpecialtyCode
	,TimetableSpecialtyCode
	,TemplateSpecialtyCode
	,SessionAllocationCode
	,TheatreSessionPeriodCode
	,CancelledSession
	,Session
	,SessionDuration
	,PlannedSessionDuration
	,TheoreticalSessionDuration
	,ActualDuration
	,OverUtilisation
	,UnderUtilisation
	,PlannedStartTimeOfDay
	,PlannedEndTimeOfDay
	,LateStart
	,LateFinish
	,ProductiveTime
	,TheatreSessionTypeCode
	,StartPunctualityMinutes
	,FinishPunctualityMinutes
)
select
	 Session.SourceUniqueID

	,TheatreCode = Session.TheatreCode

	,SessionDate = dateadd(day, datediff(day, 0, Session.StartTime), 0)

	,ConsultantCode = coalesce(Session.ConsultantCode, 0)

	,TimetableConsultantCode = coalesce(Session.TimetableConsultantCode, 0)

	,TemplateConsultantCode = coalesce(Session.TemplateConsultantCode, 0)

	,AnaesthetistCode = coalesce(Session.AnaesthetistCode1, 0)

	,TimetableAnaesthetistCode = coalesce(Session.TimetableAnaesthetistCode, 0)

	,TemplateAnaesthetistCode = coalesce(Session.TemplateAnaesthetistCode, 0)

	,SpecialtyCode = coalesce(Session.SpecialtyCode, 0)

	,TimetableSpecialtyCode = coalesce(Session.TimetableSpecialtyCode, 0)

	,TemplateSpecialtyCode = coalesce(Session.TemplateSpecialtyCode, 0)

	,SessionAllocationCode =
		case	
		when 
			coalesce(
				 Session.TemplateConsultantCode
				,Session.ConsultantCode
				,'##'
			) = 
			coalesce(
				 Session.ConsultantCode
				,'##'
			)
		then 'SC' --Same Consultant

		when 
			coalesce(
				 Session.TemplateSpecialtyCode
				,Session.SpecialtyCode
				,'##'
			) = 
			coalesce(
				 Session.SpecialtyCode
				,'##'
			)
		then 'SS' --Same Specialty

		else 'DS' --Different Specialty
		end

	,TheatreSessionPeriodCode =
		case
		--when
		--	left(convert(varchar, Session.StartTime, 8), 5) between '00:00' and '12:29'
		--and	left(convert(varchar, Session.EndTime, 8), 5) > '16:29' then 'AD' --All Day

		when left(convert(varchar, Session.StartTime, 8), 5) between '00:00' and '12:29' then 'AM' --Morning
		when left(convert(varchar, Session.StartTime, 8), 5) between '12:30' and '16:29' then 'PM' --Afternoon
		when left(convert(varchar, Session.StartTime, 8), 5) between '16:30' and '23:59' then 'VN' --Evening
		else 'NA'
		end

	,CancelledSession = Session.CancelledFlag

	,Session = 1

	,SessionDuration =
		datediff(minute, Session.StartTime, Session.EndTime)

--this takes 60 minutes off for lunch on an all day session
		--datediff(minute, Session.StartTime, Session.EndTime) -
		--case
		--when
		--	left(convert(varchar, Session.StartTime, 8), 5) between '00:00' and '12:29'
		--and left(convert(varchar, Session.EndTime, 8), 5) > '16:29' 
		--then 60
		--else 0
		--end

	,PlannedSessionDuration = Session.PlannedSessionMinutes

	,TheoreticalSessionDuration =
		case
		when
			Session.ActualSessionStartTime is null 
		or	Session.ActualSessionEndTime is null
		then null
		else
			Session.PlannedSessionMinutes
 -- -
	--		case
	--		when
	--			left(convert(varchar, Session.StartTime, 8), 5) between '00:00' and '12:29'
	--		and left(convert(varchar, Session.EndTime, 8), 5) > '16:29' 
	--		then 60
	--		else 0
	--		end
		end

	,ActualDuration =
		datediff(
			 minute
			,Session.ActualSessionStartTime
			,Session.ActualSessionEndTime
		) -
--discrepency here however, although all day session duration loses 60 minutes, actual duration does not
		case
		when
			left(convert(varchar, Session.ActualSessionStartTime, 8), 5) between '00:00' and '12:29'
		and	left(convert(varchar, Session.ActualSessionEndTime, 8), 5) > '16:29'
		then 0
		else 0
		end

	,OverUtilisation =
		case
		when datediff(minute, Session.StartTime, Session.EndTime) > datediff(minute, Session.ActualSessionStartTime, Session.ActualSessionEndTime)
		then null
		else datediff(minute, Session.ActualSessionStartTime, Session.ActualSessionEndTime) - datediff(minute, Session.StartTime, Session.EndTime)
		end

	,UnderUtilisation =
		case
		when datediff(minute, Session.StartTime, Session.EndTime) < datediff(minute, Session.ActualSessionStartTime, Session.ActualSessionEndTime)
		then null
		else  datediff(minute, Session.StartTime, Session.EndTime) - datediff(minute, Session.ActualSessionStartTime, Session.ActualSessionEndTime)
		end

	,PlannedStartTimeOfDay = left(convert(varchar, Session.StartTime, 8), 5)

	,PlannedEndTimeOfDay = left(convert(varchar, Session.EndTime, 8), 5)

	,LateStart =
		case
		when datediff(minute, Session.ActualSessionStartTime, Session.StartTime) > 15
		then 1
		else 0
		end

	,LateFinish =
		case
		when datediff(minute, Session.EndTime, Session.ActualSessionEndTime) > 15
		then 1
		else 0
		end

	,ProductiveTime =
		FactTheatreOperation.ProductiveTime

	,TheatreSessionTypeCode =
		coalesce(
			case
			when
				(
				select
					COUNT(distinct FactTheatreOperation.OperationTypeCode)
				from
					dbo.FactTheatreOperation
				where
					FactTheatreOperation.SessionSourceUniqueID = Session.SourceUniqueID
				and	FactTheatreOperation.OperationTypeCode is not null
				)
				> 1
			then 'MIXED'
			else
				(
				select top 1
					FactTheatreOperation.OperationTypeCode
				from
					dbo.FactTheatreOperation
				where
					FactTheatreOperation.SessionSourceUniqueID = Session.SourceUniqueID
				and	FactTheatreOperation.OperationTypeCode is not null
				)
			end
			,'N/A'
		)


	,StartPunctualityMinutes =
		datediff(minute, Session.ActualSessionStartTime, Session.StartTime)

	,FinishPunctualityMinutes =
		datediff(minute, Session.EndTime, Session.ActualSessionEndTime)

from
	dbo.BaseTheatreSession Session

left join 
	(
	select
		 FactTheatreOperation.SessionSourceUniqueID
		,ProductiveTime =
			SUM(FactTheatreOperation.OperationProductiveTime)
	from
		dbo.FactTheatreOperation
	group by
		 FactTheatreOperation.SessionSourceUniqueID
	) FactTheatreOperation
on	FactTheatreOperation.SessionSourceUniqueID = Session.SourceUniqueID


select @RowsInserted = @@rowcount

select
	 @from = min(FactTheatreSession.SessionDate)
	,@to = max(FactTheatreSession.SessionDate)
from
	dbo.FactTheatreSession
where
	FactTheatreSession.SessionDate is not null


exec BuildCalendarBase @from, @to


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'BuildFactTheatreSession', @Stats, @StartTime
