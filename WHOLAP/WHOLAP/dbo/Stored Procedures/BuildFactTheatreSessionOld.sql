﻿CREATE procedure [BuildFactTheatreSessionOld] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table dbo.FactTheatreSession

insert into dbo.FactTheatreSession
	(
	 SourceUniqueID
	,InterfaceCode
	,EncounterRecno
	,TheatreCode
	,SessionDate
	,ConsultantCode
	,OriginalConsultantCode
	,ConsultantAllocationCode
	,ReassignedSession
	,SessionTypeCode
	,TheatreSessionPeriodCode
	,CancelReasonCode
	,CancelledSession
	,Session
	,SessionDuration
	,TheoreticalSessionDuration
	,ActualDuration
	,OverUtilisation
	,UnderUtilisation
	,StartTime
	,EndTime
)
select
	 SourceUniqueID
	,InterfaceCode
	,EncounterRecno
	,TheatreCode
	,SessionDate
	,ConsultantCode
	,OriginalConsultantCode
	,ConsultantAllocationCode
	,ReassignedSession
	,SessionTypeCode
	,TheatreSessionPeriodCode
	,CancelReasonCode
	,CancelledSession
	,Session
	,SessionDuration
	,TheoreticalSessionDuration
	,ActualDuration
	,OverUtilisation
	,UnderUtilisation
	,StartTime
	,EndTime
from
	dbo.BaseTheatreSession

select @RowsInserted = @@rowcount


select
	 @from = min(EncounterFact.SessionDate)
	,@to = max(EncounterFact.SessionDate)
from
	dbo.FactTheatreSession EncounterFact
where
	EncounterFact.SessionDate is not null


exec BuildCalendarBase @from, @to


select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
		CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'BuildFactTheatreSession', @Stats, @StartTime
