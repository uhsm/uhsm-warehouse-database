﻿CREATE procedure [BuildFactTheatreVisitOld] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table dbo.FactTheatreVisit

insert into dbo.FactTheatreVisit
	(
	 EncounterRecno
	,SessionID
	,SourceUniqueID
	,InterfaceCode
	,TheatreCode
	,SessionTypeCode
	,CaseTypeCode
	,TheatreSessionPeriodCode
	,SessionDate
	,CancelReasonCode
	,ConsultantCode
	,CaseConsultantCode
	,EpisodicConsultantCode
	,SurgeonCode
	,AnaesthetistCode
	,PrimaryProcedureCode
	,AgeCode
	,SexCode
	,PatientCategoryCode
	,SeverityCode
	,Visit
	,ReceptionTime
	,ReceptionWaitTime
	,AnaestheticWaitTime
	,AnaestheticTime
	,OperationTime
	,RecoveryTime
	,ReturnTime
	,PatientTime
	,OriginalConsultantCode
	,ConsultantAllocationCode
	,ProcedureTime
	,ReportableFlag
)
select
	 EncounterRecno
	,SessionID
	,SourceUniqueID
	,InterfaceCode
	,TheatreCode
	,SessionTypeCode
	,CaseTypeCode
	,TheatreSessionPeriodCode
	,SessionDate
	,CancelReasonCode
	,ConsultantCode
	,CaseConsultantCode
	,EpisodicConsultantCode
	,SurgeonCode
	,AnaesthetistCode
	,PrimaryProcedureCode
	,AgeCode
	,SexCode
	,PatientCategoryCode
	,SeverityCode
	,Visit
	,ReceptionTime
	,ReceptionWaitTime
	,AnaestheticWaitTime
	,AnaestheticTime
	,OperationTime
	,RecoveryTime
	,ReturnTime
	,PatientTime
	,OriginalConsultantCode
	,ConsultantAllocationCode
	,ProcedureTime
	,ReportableFlag
from
	dbo.BaseTheatreVisit

select @RowsInserted = @@rowcount


select
	 @from = min(EncounterFact.SessionDate)
	,@to = max(EncounterFact.SessionDate)
from
	dbo.FactTheatreVisit EncounterFact
where
	EncounterFact.SessionDate is not null


exec BuildCalendarBase @from, @to


--update the Tracking table

INSERT INTO WH.Theatre.Tracking
	(
	EpisodeID, 
	InterfaceCode, 
	Created, 
	ByWhom,
	DisableTrackingDate
	)
select distinct
	Episode.SourceUniqueID, 
	Episode.InterfaceCode, 
	getdate() Created, 
	suser_sname() ByWhom,
	case 
	when PatientEpisode.CancelReason is null and coalesce(PatientEpisode.Outcome, '6') = '6' then getdate() 
	when CancelReason.CancelByCode != '2' then getdate() 
	else null
	end DisableTrackingDate
from
	dbo.FactTheatreVisit Episode

left join dbo.OlapTheatreCancelReason CancelReason
on	CancelReason.CancelReasonCode = Episode.CancelReasonCode

inner join WH.Theatre.PatientEpisode PatientEpisode
on	PatientEpisode.EncounterRecno = Episode.EncounterRecno

where
	(
		coalesce(PatientEpisode.Outcome, '6') = '6'
	or	(
			CancelReason.CancelByCode = '2'
		and	CancelReason.ClinicalCode = '2'
		)
	)

and	Episode.SessionDate < getdate()

and	not exists
	(
	select
		1
	from
		WH.Theatre.Tracking Tracking
	where
		Tracking.EpisodeID = Episode.SourceUniqueID
	and	Tracking.InterfaceCode = Episode.InterfaceCode
	)


select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
		CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'BuildFactTheatreVisit', @Stats, @StartTime
