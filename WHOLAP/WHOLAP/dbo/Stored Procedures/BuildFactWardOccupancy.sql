﻿create procedure [dbo].[BuildFactWardOccupancy] as

--KO recreated 24/11/2014 to replace view dbo.vwFactWardOccupancy to improve performance of cube build.
declare @StartTime datetime
declare @Elapsed int
--declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

truncate table dbo.FactWardOccupancy

--Midday values
insert into WHOLAP.dbo.FactWardOccupancy
	(SourceUniqueID
	,CensusDate
	,StartTime
	,EndTime
	,WardCode
	,PatientCategoryCode
	,AuditPeriod
	,SpecialtyCode
	,ConsultantCode
	,Cases)
select
	 WardStay.SourceUniqueID
	,CensusDate = Calendar.TheDate
	,WardStay.StartTime
	,WardStay.EndTime
	,WardStay.WardCode

	,PatientCategoryCode = 
		case
		when Spell.AdmissionMethodTypeCode = 'NE'
		then 'NE'
		else Spell.InpatientStayCode
		end

	,AuditPeriod = 0 --midday

	,FCE.SpecialtyCode

	,FCE.ConsultantCode

	,Cases = 1
from
	BaseAPCWardStay WardStay

inner join dbo.CalendarBase Calendar
on	dateadd(hour, 12, Calendar.TheDate) between WardStay.StartTime and coalesce(WardStay.EndTime, getdate())
and Calendar.TheDate > '2011-03-31'

inner join OlapAPC Spell
on	Spell.SourceSpellNo = WardStay.SourceSpellNo
and	Spell.AdmissionTime = Spell.EpisodeStartTime

inner join dbo.BaseAPC FCE
on	FCE.SourceUniqueID = WardStay.FCESourceUniqueID

select @RowsInserted = @@rowcount

--Midnight values
insert into WHOLAP.dbo.FactWardOccupancy
	(SourceUniqueID
	,CensusDate
	,StartTime
	,EndTime
	,WardCode
	,PatientCategoryCode
	,AuditPeriod
	,SpecialtyCode
	,ConsultantCode
	,Cases)
select
	 WardStay.SourceUniqueID
	,CensusDate = Calendar.TheDate
	,WardStay.StartTime
	,WardStay.EndTime
	,WardStay.WardCode

	,PatientCategoryCode = 
		case
		when Spell.AdmissionMethodTypeCode = 'NE'
		then 'NE'
		else Spell.InpatientStayCode
		end

	,AuditPeriod = 1 --midnight

	,FCE.SpecialtyCode

	,FCE.ConsultantCode

	,Cases = 1
from
	BaseAPCWardStay WardStay

inner join dbo.CalendarBase Calendar
on	Calendar.TheDate between WardStay.StartTime and coalesce(WardStay.EndTime, getdate())
and Calendar.TheDate > '2011-03-31'

inner join OlapAPC Spell
on	Spell.SourceSpellNo = WardStay.SourceSpellNo
and	Spell.AdmissionTime = Spell.EpisodeStartTime

inner join dbo.BaseAPC FCE
on	FCE.SourceUniqueID = WardStay.FCESourceUniqueID

select @RowsInserted = @RowsInserted + @@rowcount

exec BuildOlapCensus


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WHOLAP BuildFactFactWardOccupancy', @Stats, @StartTime
--print @Stats
