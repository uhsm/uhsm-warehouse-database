﻿CREATE procedure [dbo].[BuildModel] as

/**********************************************************************************
Change Log:
	Current Version Date: 2014-11-13
	2014-11-13 KO 
			Commented out RTT and waiting list builds
			
	2014-06-02 KO 
			Inserted if clause to control when SP is run (ie after WH build). 
			Removed this step from SSIS package

	2012-09-13 KO
			Comnmented out BuildBaseAE and BuildFactAE because these are not iPM load jobs. 
			They have been moved to the separate procedure BuildAEModel.
			
	2012-07-26 KO
			Comnmented out BuildBaseOrder and BuildFactOrder because these are covered by the 
			LoadMIsys job
			
	2011-07-01 KD
			Removed execution of procedure dbo.BuildBaseAPCUncodedSpell
			This has been superceded by another procedure run straight from the
			data in Lorenzo database.
	
	2011-06-21 KD
			Added execution of procedure dbo.BuildBaseAPCUncodedSpell
			for UncodedSpellReport	

**********************************************************************************/
		

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()


if 
	(
		select DateValue from WH.dbo.Parameter where Parameter = 'BUILDMODELDATE'
		) < (
		select DateValue from WH.dbo.Parameter where Parameter = 'BUILDWHDATE' 
	)
	--(
	--select
	--	max(DateValue)
	--from
	--	WH.dbo.Parameter
	--where
	--	Parameter like 'EXTRACT%'
	--) > 
	--(
	--select
	--	DateValue
	--from
	--	WH.dbo.Parameter
	--where
	--	Parameter = 'BUILDMODELDATE'
	--)
begin

	exec dbo.BuildBaseAPC

	exec dbo.BuildOlapAPC

	exec dbo.BuildFactReadmission
	
	--exec dbo.BuildBaseAPCUncodedSpell --KD Added 2011-06-21

	exec dbo.BuildBaseOP

	exec dbo.BuildBaseRF
	--KO 13/11/2014
	--exec dbo.BuildBaseAPCWaitingList
	--KO 13/11/2014
	--exec dbo.BuildBaseOPWaitingList

	--exec dbo.BuildBaseOrder

	--exec dbo.BuildBaseAE

	exec dbo.BuildBaseClinicalCoding

	exec dbo.BuildFactAPC

	exec dbo.BuildFactOP

	exec dbo.BuildFactRF
	
	--KO 13/11/2014
	--exec dbo.BuildFactAPCWaitingList
	--KO 13/11/2014
	--exec dbo.BuildFactOPWaitingList

	--exec dbo.BuildFactOrder

	--exec dbo.BuildFactAE

	exec dbo.BuildFactClinicalCoding

	exec dbo.BuildBaseAPCWardStay
	
	--KO 13/11/2014
	--exec dbo.BuildRTTModel

	exec dbo.BuildOlapCalendar
	
	--KO added 24/11/2014
	exec dbo.BuildFactWardOccupancy

	--store build time
	update WH.dbo.Parameter
	set
		DateValue = GETDATE()
	where
		Parameter = 'BUILDMODELDATE'

	if @@rowcount = 0

	insert into WH.dbo.Parameter
		(
		 Parameter
		,DateValue
		)
	select
		 Parameter = 'BUILDMODELDATE'
		,DateValue = GETDATE()



	select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

	select @Stats = 
		'Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

	exec WriteAuditLogEvent 'PAS - WHOLAP BuildModel Total', @Stats, @StartTime

end