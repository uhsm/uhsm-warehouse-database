﻿CREATE procedure [dbo].[BuildOlapAPC] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table dbo.OlapAPC

insert into dbo.OlapAPC
(
	 EncounterRecno
	,SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,ReferralSourceUniqueID
	,WaitingListSourceUniqueID
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,DischargeDate
	,EpisodeStartDate
	,EpisodeEndDate
	,StartSiteCode
	,StartWardTypeCode
	,EndSiteCode
	,EndWardTypeCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpPracticeCode
	,SiteCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,AdminCategoryCode
	,ConsultantCode
	,SpecialtyCode
	,LastEpisodeInSpellIndicator
	,FirstRegDayOrNightAdmit
	,NeonatalLevelOfCare
	,PASHRGCode
	,PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,PrimaryOperationCode
	,PrimaryOperationDate
	,SecondaryOperationCode1
	,SecondaryOperationDate1
	,SecondaryOperationCode2
	,SecondaryOperationDate2
	,SecondaryOperationCode3
	,SecondaryOperationDate3
	,SecondaryOperationCode4
	,SecondaryOperationDate4
	,SecondaryOperationCode5
	,SecondaryOperationDate5
	,SecondaryOperationCode6
	,SecondaryOperationDate6
	,SecondaryOperationCode7
	,SecondaryOperationDate7
	,SecondaryOperationCode8
	,SecondaryOperationDate8
	,SecondaryOperationCode9
	,SecondaryOperationDate9
	,SecondaryOperationCode10
	,SecondaryOperationDate10
	,SecondaryOperationCode11
	,SecondaryOperationDate11
	,OperationStatusCode
	,ContractSerialNo
	,CodingCompleteFlag
	,PurchaserCode
	,ProviderCode
	,EpisodeStartTime
	,EpisodeEndTime
	,RegisteredGdpCode
	,EpisodicGpCode
	,InterfaceCode
	,CasenoteNumber
	,NHSNumberStatusCode
	,AdmissionTime
	,DischargeTime
	,TransferFrom
	,DistrictNo
	,SourceUniqueID
	,ExpectedLOS
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,OverseasStatusFlag
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,Created
	,Updated
	,ByWhom
	,TheatrePatientBookingKey
	,ProcedureTime
	,TheatreCode
	,AgeCode
	,CategoryCode
	,LOE
	,LOS
	,AdmissionMethodTypeCode
	,NeonatalLevelOfCareFlag
	,InpatientStayCode
	,Cases
	,ResponsibleCommissioner
	,ResponsibleCommissionerCCGOnly
	,OurActivityFlag
	,NotOurActProvCode
)
select
	 EncounterRecno
	,SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,ReferralSourceUniqueID
	,WaitingListSourceUniqueID
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,DischargeDate
	,EpisodeStartDate
	,EpisodeEndDate
	,StartSiteCode = coalesce(Encounter.StartSiteCode, -1)
	,StartWardTypeCode
	,EndSiteCode = coalesce(Encounter.EndSiteCode, -1)
	,EndWardTypeCode
	,RegisteredGpCode

	,RegisteredGpPracticeCode =
		coalesce(Encounter.RegisteredGpPracticeCode, -1)

	,EpisodicGpPracticeCode =
		coalesce(Encounter.EpisodicGpPracticeCode, -1)
		
	,SiteCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,AdminCategoryCode
	,ConsultantCode = coalesce(Encounter.ConsultantCode, -1)
	,SpecialtyCode = coalesce(Encounter.SpecialtyCode, -1)
	,LastEpisodeInSpellIndicator
	,FirstRegDayOrNightAdmit
	,NeonatalLevelOfCare

	,PASHRGCode

	,PrimaryDiagnosisCode =
		coalesce(left(Encounter.PrimaryDiagnosisCode, 5), '##')

	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12

	,PrimaryOperationCode =
		coalesce(left(Encounter.PrimaryOperationCode, 5), '##')
	
	,PrimaryOperationDate
	,SecondaryOperationCode1
	,SecondaryOperationDate1
	,SecondaryOperationCode2
	,SecondaryOperationDate2
	,SecondaryOperationCode3
	,SecondaryOperationDate3
	,SecondaryOperationCode4
	,SecondaryOperationDate4
	,SecondaryOperationCode5
	,SecondaryOperationDate5
	,SecondaryOperationCode6
	,SecondaryOperationDate6
	,SecondaryOperationCode7
	,SecondaryOperationDate7
	,SecondaryOperationCode8
	,SecondaryOperationDate8
	,SecondaryOperationCode9
	,SecondaryOperationDate9
	,SecondaryOperationCode10
	,SecondaryOperationDate10
	,SecondaryOperationCode11
	,SecondaryOperationDate11
	,OperationStatusCode
	,ContractSerialNo
	,CodingCompleteFlag
	,PurchaserCode
	,ProviderCode
	,EpisodeStartTime
	,EpisodeEndTime
	,RegisteredGdpCode
	,EpisodicGpCode
	,InterfaceCode
	,CasenoteNumber
	,NHSNumberStatusCode
	,AdmissionTime
	,DischargeTime
	,TransferFrom
	,DistrictNo
	,SourceUniqueID
	,ExpectedLOS
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,OverseasStatusFlag
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,Created
	,Updated
	,ByWhom
	,TheatrePatientBookingKey
	,ProcedureTime
	,TheatreCode

	,AgeCode =
	case
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) is null then 'Age Unknown'
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) <= 0 then '00 Days'
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) = 1 then '01 Day'
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) > 1 and
	datediff(day, DateOfBirth, Encounter.EpisodeStartDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, DateOfBirth, Encounter.EpisodeStartDate)), 2) + ' Days'
	
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) > 28 and
	datediff(month, DateOfBirth, Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, DateOfBirth, Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, DateOfBirth, Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end > 1 and
	 datediff(month, DateOfBirth, Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, DateOfBirth,Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, DateOfBirth, Encounter.EpisodeStartDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.EpisodeStartDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.EpisodeStartDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.EpisodeStartDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, DateOfBirth, Encounter.EpisodeStartDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.EpisodeStartDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.EpisodeStartDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.EpisodeStartDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end 

	,CategoryCode =
		case
		when Encounter.ManagementIntentionCode = 2000619 then '10'
		when Encounter.ManagementIntentionCode = 9487 then '10'
		when Encounter.ManagementIntentionCode = 8850 then '20'
		when Encounter.ManagementIntentionCode = 8851 then '20'
		else '28' --other inpatient
		end

	,LOE =
		datediff(day, Encounter.EpisodeStartDate, Encounter.EpisodeEndDate)

	,LOS =
	datediff(
		 day
		,Encounter.AdmissionDate
		,coalesce(
			 Encounter.DischargeDate
			,(
			select
				max(DischargeDate)
			from
				WH.APC.Encounter
			)
		)
	)

	,AdmissionMethodTypeCode =
		case
		when Encounter.AdmissionMethodCode in
			(
			 2003470	--Elective
			,8811		--Elective - Booked
			,8810		--Elective - Waiting List
			,8812		--Elective - Planned
			,13			--Not Specified
			,2003472	--OTHER
			)
		then 'EL' --elective
		when Encounter.AdmissionMethodCode in 
			(
			 2000685	--Emergency - Bed Bureau	23
			,2000616	--Emergency - Clinic	24
			,2000615	--Emergency - GP	22
			,2000614	--Emergency - Local A&E	21
			,2000617	--Emergency - Other (inc other provider A&E)	28
			)
		then 'EM'
		when Encounter.AdmissionMethodCode in 
			(
			 2003473	--Maternity	MATER
			,8814		--Maternity ante-partum	31
			,8815		--Maternity post-partum	32
			)
		then 'MAT'

		else 'NE'
		end

	,NeonatalLevelOfCareFlag =
		case
		when NeonatalLevelOfCare = 2004631	--Not Applicable	8
		then null
		when NeonatalLevelOfCare in
			(
			 2004627	--Special Care	1
			,2004628	--Level 2 Intensive Care (High Dependency Intensive Care)	2
			,2004629	--Level 1 Intensive Care (Maximal Intensive Care)	3
			)
		then 1
		else 0
		end

	,InpatientStayCode =
		case
		when AdmissionMethod.MainCode not in ('11','12','13')
		then
			case
			when AdmissionMethod.MainCode in ('BHOSP','BOHOSP')
			then 'B'
			else 'I' --'N' Nicholas Scattergood 20 Mar 2012 - we don’t want Non-Elective splitting out on DW01 like it used to be on Info-Sql. So everything that’s an ‘N’ needs to be an ‘I’. This is because we have existing reports based on this and we don’t want it to change.
			end
		else
			case
			when
				ManagementIntention.MainCode in ('2','4','9','8')
			and	DATEDIFF(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, getdate())) >= 1

			then 'I'

			else
				case
				when ManagementIntention.MainCode in ('1','3','5')
				then 'I'
				else 'D'
				end
			end
		end

	--,InpatientStayCode =
	--	case
	--	when
	--		(
	--			Encounter.ManagementIntentionCode in
	--			(
	--			 2000619	--At Least One Night	1
	--			,2003683	--Not Applicable	8
	--			,2003684	--Not Known	9
	--			,2005563	--Not Specified	NSP
	--			)

	--		or	(
	--				Encounter.ManagementIntentionCode in
	--				(
	--				 8850	--No Overnight Stay	2
	--				,8851	--Planned Sequence - No Overnight Stays	4
	--				,9487	--Planned Sequence - Inc. Over Night Stays	3
	--				)

	--			and	datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) <> 0
	--			)
	--		)
	--	then 'I'

	--	when
	--		Encounter.ManagementIntentionCode in
	--		(
	--		 8850	--No Overnight Stay	2
	--		)
	--	and	datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
	--	then 'D'

	--	when
	--		Encounter.ManagementIntentionCode in
	--		(
	--		 8851	--Planned Sequence - No Overnight Stays	4
	--		)
	--	and	datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
	--	then 'RD'

	--	when
	--		Encounter.ManagementIntentionCode in
	--		(
	--		 9487	--Planned Sequence - Inc. Over Night Stays	3
	--		)
	--	and	datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
	--	then 'RN'

	--	else 'OTHER'
	--	end


	,1 Cases
	,ResponsibleCommissioner
	,ResponsibleCommissionerCCGOnly
	,OurActivityFlag
	,NotOurActProvCode

from
	dbo.BaseAPC Encounter

left join WH.PAS.ReferenceValue AdmissionMethod
on	AdmissionMethod.ReferenceValueCode = Encounter.AdmissionMethodCode

left join WH.PAS.ReferenceValue ManagementIntention
on	ManagementIntention.ReferenceValueCode = Encounter.ManagementIntentionCode


select @RowsInserted = @@rowcount



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WHOLAP BuildOlapAPC', @Stats, @StartTime



