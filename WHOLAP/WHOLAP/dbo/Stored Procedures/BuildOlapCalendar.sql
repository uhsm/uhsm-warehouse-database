﻿CREATE procedure [dbo].[BuildOlapCalendar] as

declare @StartTime datetime
declare @Elapsed int
--declare @RowsInserted Int
declare @Stats varchar(255)

--declare @from smalldatetime
--declare @to smalldatetime

select @StartTime = getdate()

set datefirst 1 --monday


--truncate table OlapCalendar

insert into OlapCalendar
	(
	 TheDate
	,DayOfWeek
	,DayOfWeekKey
	,LongDate
	,TheMonth
	,FinancialQuarter
	,FinancialYear
	,FinancialMonthKey
	,FinancialQuarterKey
	,CalendarQuarter
	,CalendarSemester
	,CalendarYear
	,LastCompleteMonth
	,WeekNoKey
	,WeekNo
	,FirstDayOfWeek
	,LastDayOfWeek
	,FirstDateOfMonth
	)
select
	 TheDate

	,TheDay =
		datename(weekday, TheDate)

	,DayOfWeekKey =
		datepart(weekday, TheDate)

	,LongDate =
		left(convert(varchar, TheDate, 113), 11)

	,TheMonth =
		left(datename(month, TheDate), 3) + ' ' + datename(year, TheDate) 

	,FinancialQuarter =
		'Qtr ' + 
		case datepart(quarter, TheDate)
		when 1 then '4'
		else convert(varchar, datepart(quarter, TheDate) - 1)
		end + ' ' + FinancialYear
	

	,FinancialYear
	,FinancialMonthKey
	,FinancialQuarterKey

	,CalendarQuarter
	,CalendarSemester
	,CalendarYear

--derived as 1 if either last day of month or 
	,LastCompleteMonth =
		case
		when
			datepart(day, dateadd(day, 1, getdate())) = 1 
		and	TheDate = dateadd(day, -1, datepart(day, dateadd(day, 1, getdate())))
		then 1 --it is the last day of month
		when
			TheDate = dateadd(month, -1, dateadd(day, datepart(day, getdate()) * -1 + 1, dateadd(day, datediff(day, 0, getdate()), 0))) --mid month so go back to start of last month
		then 1
		else 0
		end

	,WeekNoKey =
		datename(year, TheDate) + right('00' + datename(week, TheDate), 2) 

	,WeekNo =
		'Week ' + left(convert(varchar, dateadd(day, 6, FirstDayOfWeek), 113), 11) 

	,FirstDayOfWeek =
		case
		when TheDate = '1 Jan 1900'
		then '1 jan 1900'
		else FirstDayOfWeek
		end

	,LastDayOfWeek =
		case
		when TheDate = '1 Jan 1900'
		then '1 jan 1900'
		else dateadd(day, 6, FirstDayOfWeek)
		end
	,FirstDateOfMonth

from
	(
	select
		TheDate

		,FinancialYear =
			case datepart(quarter, TheDate)
			when 1 then convert(char(4),datepart(year,dateadd(year,-1, TheDate))) + '/' + datename(year, TheDate)
			else datename(year, TheDate) + '/' + convert(char(4),datepart(year,dateadd(year,1, TheDate)))
			end

		,FinancialMonthKey =
			case datepart(quarter, TheDate)
			when 1 then datepart(year, TheDate) - 1
			else datepart(year, TheDate)
			end * 100 + 
			case datepart(quarter, TheDate)
			when 1 then 9 else -3 
			end + datepart(month, TheDate)

		,FinancialQuarterKey =
			case datepart(quarter, TheDate)
			when 1 then datepart(year, TheDate) - 1
			else datepart(year, TheDate)
			end * 100 + 
			case datepart(quarter, TheDate)
			when 1 then 4
			else datepart(quarter, TheDate) - 1
			end

		,FirstDateOfMonth =
			convert(datetime, '01 ' + datename(month, TheDate) + ' ' + datename(year, TheDate)) 

		,FirstDayOfWeek =
			(
			select
				min(StartOfWeek.TheDate)
			from
				CalendarBase StartOfWeek
			where
				datepart(week, StartOfWeek.TheDate) = datepart(week, CalendarBase.TheDate)
			and	datepart(year, StartOfWeek.TheDate) = datepart(year, CalendarBase.TheDate)
			)


	--calendar hierarchy
		,CalendarQuarter =
			'Qtr ' + convert(char,datepart(quarter,TheDate))
 
		,CalendarSemester =
			case when datepart(quarter, TheDate) < 3 then '1st Semester' else '2nd Semester' end

		,CalendarYear =
			datename(year,TheDate) 

	from
		dbo.CalendarBase
	where
		not exists
		(
		select
			1
		from
			dbo.OlapCalendar
		where
			OlapCalendar.TheDate = CalendarBase.TheDate
		)
	) Calendar

update
	OlapCalendar
set
	--derived as 1 if either last day of month or 
	LastCompleteMonth =
		case
		when
			datepart(day, dateadd(day, 1, getdate())) = 1 
		and	TheDate = dateadd(day, -1, datepart(day, dateadd(day, 1, getdate())))
		then 1 --it is the last day of month
		when
			TheDate = 
				dateadd(
					month
					,-1
					,dateadd(
						day
						,datepart(
							day
							,getdate()
						) * -1 + 1
						,dateadd(
							day
							,datediff(
								day
								,0
								,getdate()
							)
							,0
						)
					)
				) --mid month so go back to start of last month
		then 1
		else 0
		end

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

--select @Stats = 
--	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
--		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
--		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
--		CONVERT(varchar(11), coalesce(@to, ''))
		
select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WHOLAP BuildOlapCalendar', @Stats, @StartTime

/**
K Oakden
08/05/2014
Required to fix discrepancies in week related values in Calendar lookup
Similar fix applied to WH.LK.Calendar


declare 
	@firstday datetime,
	@wknum varchar(6),
	@NewWeekNoKey varchar(6),
	@NewWeekNo varchar(20),
	@NewFirstDayOfWeek datetime,
	@NewLastDayOfWeek datetime

--get all WeekNoKeys that fall at the end of the year --eg 201453
declare lastweek cursor for
	select
		WeekNoKey =  cast(DATEPART(YEAR, TheDate)as varchar) +  max(RIGHT(WeekNoKey, 2))
	from dbo.OlapCalendar
	group by DATEPART(YEAR, TheDate)
	order by DATEPART(YEAR, TheDate)

open lastweek
	fetch next from lastweek into @wknum
	while @@fetch_status = 0   
	begin  
		--Get first date for a given week 
		--plus the value of the WeekNo, FirstDayOfWeekand and LastDayOfWeek
		--parameters to apply to the whole week (correct at start of week)
		select 
			@firstday = TheDate,
			@NewWeekNo = WeekNo,
			@NewFirstDayOfWeek = FirstDayOfWeek,
			@NewLastDayOfWeek = LastDayOfWeek
		from dbo.OlapCalendar
		where WeekNoKey = @wknum and DayOfWeekKey = 1
		
		--Get the value of the WeekNoKey parameter to apply to the whole week
		--(correct at end of week)
		select 
			@NewWeekNoKey = WeekNoKey
		from dbo.OlapCalendar 
		where TheDate = DATEADD (day, 6, @firstday)
		
		--update full week
		update dbo.OlapCalendar set
		--select
			WeekNo = @NewWeekNo,
			FirstDayOfWeek = @NewFirstDayOfWeek,
			LastDayOfWeek =@NewLastDayOfWeek,
			WeekNoKey = @NewWeekNoKey
		from dbo.OlapCalendar 
		where TheDate between @firstday and DATEADD (day, 6, @firstday)
		
	fetch next from lastweek into @wknum
	end   
	
close lastweek   
deallocate lastweek

--select * into dbo.OlapCalendarCopy from dbo.OlapCalendar
**/