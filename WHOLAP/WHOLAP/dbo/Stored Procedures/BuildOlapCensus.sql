﻿CREATE procedure [dbo].[BuildOlapCensus] as

delete from 	dbo.OlapCensus

insert
into
	dbo.OlapCensus
(
	Snapshot.CensusDate
	,Census
	,LastInMonth
	,IsRTT
)
select
	Snapshot.CensusDate

	,Census = left(convert(varchar, Snapshot.CensusDate, 113), 11) 

	,LastInMonth =
	case
	when CensusDate in 
		(
		select 
			max(CensusDate) 
		from 
			WH.APC.Snapshot LatestSnapshot 
		group by 
			 datepart(year, CensusDate)
			,datepart(month, CensusDate)
		)
	then 1
	else 0
	end

	,IsRTT = 0
from
	(
	--select distinct
	--	CensusDate
	--from
	--	dbo.BaseAPCWaitingList

	--union

	--select distinct
	--	CensusDate
	--from
	--	dbo.BaseOPWaitingList

	--union

	--select distinct
	--	CensusDate
	--from
	--	dbo.BaseRTT

	--union

	select distinct
		CensusDate
	from
		dbo.FactWardOccupancy
		
	union
	--KO updated 20/11/2014 to keep table up to date whilst testing
	select distinct
		TheDate
  	from
		dbo.CalendarBase
	where
		not exists
		(
		select
			1
		from
			dbo.OlapCensus
		where
			OlapCensus.CensusDate = CalendarBase.TheDate
			

		)
	and CalendarBase.TheDate < DATEADD(day, 1, getdate())
	and CalendarBase.TheDate > '2006-04-01 00:00:00.000'
	

	) Snapshot