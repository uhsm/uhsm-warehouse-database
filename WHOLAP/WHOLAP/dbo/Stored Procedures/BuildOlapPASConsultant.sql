﻿CREATE      procedure [dbo].[BuildOlapPASConsultant]
	@dataset varchar(50) = null
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

----------------------------------------------------
-- PASConsultant Reference Data Build
----------------------------------------------------

create table #tPASConsultant 
	(
	ConsultantCode int not null
	)

create clustered index #tPASConsultantIX on #tPASConsultant
	(
	ConsultantCode
	)

-- prepare distinct list

if @dataset = 'APC' or @dataset is null

	insert into #tPASConsultant
	select distinct
		 coalesce(Encounter.ConsultantCode, -1)
	from
		dbo.FactAPC Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASConsultant
		where
			#tPASConsultant.ConsultantCode = coalesce(Encounter.ConsultantCode, -1)
		)
	--KO added 20/01/2015 because new consultant with unfinished episode was not appearing in dbo.FactAPC
	insert into #tPASConsultant
	select distinct
		 coalesce(Encounter.ConsultantCode, -1)
	from
		WH.APC.Encounter Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASConsultant
		where
			#tPASConsultant.ConsultantCode = coalesce(Encounter.ConsultantCode, -1)
		)

if @dataset = 'APCTheatre' or @dataset is null

	insert into #tPASConsultant
	select distinct
		 coalesce(Encounter.EpisodicConsultantCode, -1)
	from
		dbo.FactTheatreOperation Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASConsultant
		where
			#tPASConsultant.ConsultantCode = coalesce(Encounter.EpisodicConsultantCode, -1)
		)

if @dataset = 'OP' or @dataset is null

	insert into #tPASConsultant
	select distinct
		 coalesce(Encounter.ConsultantCode, -1)
	from
		dbo.FactOP Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASConsultant
		where
			#tPASConsultant.ConsultantCode = coalesce(Encounter.ConsultantCode, -1)
		)

if @dataset = 'RF' or @dataset is null

	insert into #tPASConsultant
	select distinct
		 coalesce(Encounter.ConsultantCode, -1)
	from
		dbo.FactRF Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASConsultant
		where
			#tPASConsultant.ConsultantCode = coalesce(Encounter.ConsultantCode, -1)
		)

	insert into #tPASConsultant
	select distinct
		 coalesce(Encounter.ReferredByConsultantCode, -1)
	from
		dbo.FactRF Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASConsultant
		where
			#tPASConsultant.ConsultantCode = coalesce(Encounter.ReferredByConsultantCode, -1)
		)


if @dataset = 'APCWL' or @dataset is null

	insert into #tPASConsultant
	select distinct
		 coalesce(Encounter.ConsultantCode, -1)
	from
		dbo.FactAPCWaitingList Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASConsultant
		where
			#tPASConsultant.ConsultantCode = coalesce(Encounter.ConsultantCode, -1)
		)

if @dataset = 'OPWL' or @dataset is null

	insert into #tPASConsultant
	select distinct
		 coalesce(Encounter.ConsultantCode, -1)
	from
		dbo.FactOPWaitingList Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASConsultant
		where
			#tPASConsultant.ConsultantCode = coalesce(Encounter.ConsultantCode, -1)
		)

if @dataset = 'RTT' or @dataset is null

	insert into #tPASConsultant
	select distinct
		 coalesce(Encounter.ConsultantCode, -1)
	from
		dbo.FactRTT Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASConsultant
		where
			#tPASConsultant.ConsultantCode = coalesce(Encounter.ConsultantCode, -1)
		)


--delete all entries if this is ALL datasets
if @dataset is null

	delete from dbo.EntityXref
	where
		EntityTypeCode = 'OLAPPASCONSULTANT'
	and	XrefEntityTypeCode = 'OLAPPASCONSULTANT'
	and	not exists
		(
		select
			1
		from
			#tPASConsultant
		where
			EntityCode = ConsultantCode
		)


insert into dbo.EntityXref
(
	 EntityTypeCode
	,EntityCode
	,XrefEntityTypeCode
	,XrefEntityCode
)
select
	 EntityTypeCode
	,EntityCode
	,XrefEntityTypeCode
	,XrefEntityCode
from
	(
	select distinct
		 EntityTypeCode = 'OLAPPASCONSULTANT'
		,EntityCode = ConsultantCode
		,XrefEntityTypeCode = 'OLAPPASCONSULTANT'
		,XrefEntityCode = ConsultantCode
	from
		#tPASConsultant

	union

	select
		 EntityTypeCode = 'OLAPPASCONSULTANT'
		,EntityCode = '-1'
		,XrefEntityTypeCode = 'OLAPPASCONSULTANT'
		,XrefEntityCode = '-1'
	) PASConsultant

where
	not exists
	(
	select
		1
	from
		dbo.EntityXref
	where
		EntityXref.EntityTypeCode = 'OLAPPASCONSULTANT'
	and	EntityXref.XrefEntityTypeCode = 'OLAPPASCONSULTANT'
	and	EntityXref.EntityCode = PASConsultant.EntityCode
	and	EntityXref.XrefEntityCode = PASConsultant.XrefEntityCode
	)


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins, ' +
	'Dataset ' + coalesce(@dataset, 'All')

exec WriteAuditLogEvent 'PAS - WHOLAP BuildOlapPASConsultant', @Stats, @StartTime
