﻿CREATE      procedure [dbo].[BuildOlapPASOperation]
	@dataset varchar(50) = null
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

----------------------------------------------------
-- PASOperation Reference Data Build
----------------------------------------------------

create table #tPASOperation 
	(
	OperationCode int not null
	)

create clustered index #tPASOperationIX on #tPASOperation
	(
	OperationCode
	)

-- prepare distinct list

--if @dataset = 'APC' or @dataset is null

--	insert into #tPASOperation
--	select distinct
--		 coalesce(Encounter.OperationCode, -1)
--	from
--		dbo.FactAPC Encounter
--	where
--		not exists
--		(
--		select
--			1
--		from
--			#tPASOperation
--		where
--			#tPASOperation.OperationCode = coalesce(Encounter.OperationCode, -1)
--		)

--if @dataset = 'OP' or @dataset is null

--	insert into #tPASOperation
--	select distinct
--		 coalesce(Encounter.OperationCode, -1)
--	from
--		dbo.FactOP Encounter
--	where
--		not exists
--		(
--		select
--			1
--		from
--			#tPASOperation
--		where
--			#tPASOperation.OperationCode = coalesce(Encounter.OperationCode, -1)
--		)

--if @dataset = 'RF' or @dataset is null

--	insert into #tPASOperation
--	select distinct
--		 coalesce(Encounter.OperationCode, -1)
--	from
--		dbo.FactRF Encounter
--	where
--		not exists
--		(
--		select
--			1
--		from
--			#tPASOperation
--		where
--			#tPASOperation.OperationCode = coalesce(Encounter.OperationCode, -1)
--		)


--if @dataset = 'APCWL' or @dataset is null

--	insert into #tPASOperation
--	select distinct
--		 coalesce(Encounter.OperationCode, -1)
--	from
--		dbo.FactAPCWaitingList Encounter
--	where
--		not exists
--		(
--		select
--			1
--		from
--			#tPASOperation
--		where
--			#tPASOperation.OperationCode = coalesce(Encounter.OperationCode, -1)
--		)

--if @dataset = 'OPWL' or @dataset is null

--	insert into #tPASOperation
--	select distinct
--		 coalesce(Encounter.OperationCode, -1)
--	from
--		dbo.FactOPWaitingList Encounter
--	where
--		not exists
--		(
--		select
--			1
--		from
--			#tPASOperation
--		where
--			#tPASOperation.OperationCode = coalesce(Encounter.OperationCode, -1)
--		)

if @dataset = 'RTT' or @dataset is null

	insert into #tPASOperation
	select distinct
		 coalesce(Encounter.OperationCode, -1)
	from
		dbo.FactRTT Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASOperation
		where
			#tPASOperation.OperationCode = coalesce(Encounter.OperationCode, -1)
		)


--delete all entries if this is ALL datasets
if @dataset is null

	delete from dbo.EntityXref
	where
		EntityTypeCode = 'OLAPPASOPERATION'
	and	XrefEntityTypeCode = 'OLAPPASOPERATION'
	and	not exists
		(
		select
			1
		from
			#tPASOperation
		where
			EntityCode = OperationCode
		)


insert into dbo.EntityXref
(
	 EntityTypeCode
	,EntityCode
	,XrefEntityTypeCode
	,XrefEntityCode
)
select
	 EntityTypeCode
	,EntityCode
	,XrefEntityTypeCode
	,XrefEntityCode
from
	(
	select distinct
		 EntityTypeCode = 'OLAPPASOPERATION'
		,EntityCode = OperationCode
		,XrefEntityTypeCode = 'OLAPPASOPERATION'
		,XrefEntityCode = OperationCode
	from
		#tPASOperation

	union

	select
		 EntityTypeCode = 'OLAPPASOPERATION'
		,EntityCode = '-1'
		,XrefEntityTypeCode = 'OLAPPASOPERATION'
		,XrefEntityCode = '-1'
	) PASOperation

where
	not exists
	(
	select
		1
	from
		dbo.EntityXref
	where
		EntityXref.EntityTypeCode = 'OLAPPASOPERATION'
	and	EntityXref.XrefEntityTypeCode = 'OLAPPASOPERATION'
	and	EntityXref.EntityCode = PASOperation.EntityCode
	and	EntityXref.XrefEntityCode = PASOperation.XrefEntityCode
	)


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins, ' +
	'Dataset ' + coalesce(@dataset, 'All')

exec WriteAuditLogEvent 'PAS - WHOLAP BuildOlapPASOperation', @Stats, @StartTime
