﻿CREATE      procedure [dbo].[BuildOlapPASPractice]
	@dataset varchar(50) = null
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

----------------------------------------------------
-- Practice Reference Data Build
----------------------------------------------------

create table #tPractice 
	(
	PracticeCode varchar(20) not null
	)

create clustered index #tPracticeIX on #tPractice
	(
	PracticeCode
	)

-- prepare distinct list

if @dataset = 'AE' or @dataset is null

	insert into #tPractice
	select distinct
		 coalesce(Encounter.RegisteredPracticeCode, -1)
	from
		dbo.FactAE Encounter
	where
		not exists
		(
		select
			1
		from
			#tPractice
		where
			#tPractice.PracticeCode = coalesce(Encounter.RegisteredPracticeCode, -1)
		)

if @dataset = 'APC' or @dataset is null

	insert into #tPractice
	select distinct
		 coalesce(Encounter.PracticeCode, -1)
	from
		dbo.FactAPC Encounter
	where
		not exists
		(
		select
			1
		from
			#tPractice
		where
			#tPractice.PracticeCode = coalesce(Encounter.PracticeCode, -1)
		)

if @dataset = 'OP' or @dataset is null

	insert into #tPractice
	select distinct
		 coalesce(Encounter.PracticeCode, -1)
	from
		dbo.FactOP Encounter
	where
		not exists
		(
		select
			1
		from
			#tPractice
		where
			#tPractice.PracticeCode = coalesce(Encounter.PracticeCode, -1)
		)

if @dataset = 'RF' or @dataset is null

	insert into #tPractice
	select distinct
		 coalesce(Encounter.PracticeCode, -1)
	from
		dbo.FactRF Encounter
	where
		not exists
		(
		select
			1
		from
			#tPractice
		where
			#tPractice.PracticeCode = coalesce(Encounter.PracticeCode, -1)
		)


if @dataset = 'APCWL' or @dataset is null

	insert into #tPractice
	select distinct
		 coalesce(Encounter.PracticeCode, -1)
	from
		dbo.FactAPCWaitingList Encounter
	where
		not exists
		(
		select
			1
		from
			#tPractice
		where
			#tPractice.PracticeCode = coalesce(Encounter.PracticeCode, -1)
		)

if @dataset = 'OPWL' or @dataset is null

	insert into #tPractice
	select distinct
		 coalesce(Encounter.PracticeCode, -1)
	from
		dbo.FactOPWaitingList Encounter
	where
		not exists
		(
		select
			1
		from
			#tPractice
		where
			#tPractice.PracticeCode = coalesce(Encounter.PracticeCode, -1)
		)

if @dataset = 'RTT' or @dataset is null

	insert into #tPractice
	select distinct
		 coalesce(Encounter.PracticeCode, -1)
	from
		dbo.FactRTT Encounter
	where
		not exists
		(
		select
			1
		from
			#tPractice
		where
			#tPractice.PracticeCode = coalesce(Encounter.PracticeCode, -1)
		)


--delete all entries if this is ALL datasets
if @dataset is null

	delete from dbo.EntityXref
	where
		EntityTypeCode = 'OLAPPRACTICE'
	and	XrefEntityTypeCode = 'OLAPPRACTICE'
	and	not exists
		(
		select
			1
		from
			#tPractice
		where
			EntityCode = PracticeCode
		)


insert into dbo.EntityXref
(
	 EntityTypeCode
	,EntityCode
	,XrefEntityTypeCode
	,XrefEntityCode
)
select
	 EntityTypeCode
	,EntityCode
	,XrefEntityTypeCode
	,XrefEntityCode
from
	(
	select distinct
		 EntityTypeCode = 'OLAPPASPRACTICE'
		,EntityCode = PracticeCode
		,XrefEntityTypeCode = 'OLAPPASPRACTICE'
		,XrefEntityCode = PracticeCode
	from
		#tPractice

	union

	select
		 EntityTypeCode = 'OLAPPASPRACTICE'
		,EntityCode = '-1'
		,XrefEntityTypeCode = 'OLAPPASPRACTICE'
		,XrefEntityCode = '-1'
	) Practice

where
	not exists
	(
	select
		1
	from
		dbo.EntityXref
	where
		EntityXref.EntityTypeCode = 'OLAPPASPRACTICE'
	and	EntityXref.XrefEntityTypeCode = 'OLAPPASPRACTICE'
	and	EntityXref.EntityCode = Practice.EntityCode
	and	EntityXref.XrefEntityCode = Practice.XrefEntityCode
	)


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins, ' +
	'Dataset ' + coalesce(@dataset, 'All')

exec WriteAuditLogEvent 'PAS - WHOLAP BuildOlapPASPractice', @Stats, @StartTime
