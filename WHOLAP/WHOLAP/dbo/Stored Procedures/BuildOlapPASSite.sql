﻿CREATE      procedure [dbo].[BuildOlapPASSite]
	@dataset varchar(50) = null
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

----------------------------------------------------
-- PASSite Reference Data Build
----------------------------------------------------

create table #tPASSite 
	(
	SiteCode int not null
	)

create clustered index #tPASSiteIX on #tPASSite
	(
	SiteCode
	)

-- prepare distinct list

if @dataset = 'APC' or @dataset is null

	insert into #tPASSite
	select distinct
		 coalesce(Encounter.SiteCode, -1)
	from
		dbo.FactAPC Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASSite
		where
			#tPASSite.SiteCode = coalesce(Encounter.SiteCode, -1)
		)

if @dataset = 'OP' or @dataset is null

	insert into #tPASSite
	select distinct
		 coalesce(Encounter.SiteCode, -1)
	from
		dbo.FactOP Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASSite
		where
			#tPASSite.SiteCode = coalesce(Encounter.SiteCode, -1)
		)

if @dataset = 'RF' or @dataset is null

	insert into #tPASSite
	select distinct
		 coalesce(Encounter.SiteCode, -1)
	from
		dbo.FactRF Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASSite
		where
			#tPASSite.SiteCode = coalesce(Encounter.SiteCode, -1)
		)


if @dataset = 'APCWL' or @dataset is null

	insert into #tPASSite
	select distinct
		 coalesce(Encounter.SiteCode, -1)
	from
		dbo.FactAPCWaitingList Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASSite
		where
			#tPASSite.SiteCode = coalesce(Encounter.SiteCode, -1)
		)

if @dataset = 'OPWL' or @dataset is null

	insert into #tPASSite
	select distinct
		 coalesce(Encounter.SiteCode, -1)
	from
		dbo.FactOPWaitingList Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASSite
		where
			#tPASSite.SiteCode = coalesce(Encounter.SiteCode, -1)
		)


--delete all entries if this is ALL datasets
if @dataset is null

	delete from dbo.EntityXref
	where
		EntityTypeCode = 'OLAPPASSITE'
	and	XrefEntityTypeCode = 'OLAPPASSITE'
	and	not exists
		(
		select
			1
		from
			#tPASSite
		where
			EntityCode = SiteCode
		)


insert into dbo.EntityXref
(
	 EntityTypeCode
	,EntityCode
	,XrefEntityTypeCode
	,XrefEntityCode
)
select
	 EntityTypeCode
	,EntityCode
	,XrefEntityTypeCode
	,XrefEntityCode
from
	(
	select distinct
		 EntityTypeCode = 'OLAPPASSITE'
		,EntityCode = SiteCode
		,XrefEntityTypeCode = 'OLAPPASSITE'
		,XrefEntityCode = SiteCode
	from
		#tPASSite

	union

	select
		 EntityTypeCode = 'OLAPPASSITE'
		,EntityCode = '-1'
		,XrefEntityTypeCode = 'OLAPPASSITE'
		,XrefEntityCode = '-1'
	) PASSite

where
	not exists
	(
	select
		1
	from
		dbo.EntityXref
	where
		EntityXref.EntityTypeCode = 'OLAPPASSITE'
	and	EntityXref.XrefEntityTypeCode = 'OLAPPASSITE'
	and	EntityXref.EntityCode = PASSite.EntityCode
	and	EntityXref.XrefEntityCode = PASSite.XrefEntityCode
	)


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins, ' +
	'Dataset ' + coalesce(@dataset, 'All')

exec WriteAuditLogEvent 'PAS - WHOLAP BuildOlapPASSite', @Stats, @StartTime
