﻿CREATE      procedure [dbo].[BuildOlapPASSpecialty]
	@dataset varchar(50) = null
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		stored procedure: OlapSpecialtyDivision

Notes:			Stored in WHOLAP

Versions:		
				1.0.0.1 - 19/04/2016 - CM - Job 451
					Added field: RTTExcludeFlag to the Insert statement at the bottom of the script

				1.0.0.0 - 20.10.2010 - ??
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/




/**************************************************************************
20.10.2010 KD
			- Added Insert to SpecialtyDivisionBase to add specialty codes 
			that haven't previously been used
**************************************************************************/

----------------------------------------------------
-- PASSpecialty Reference Data Build
----------------------------------------------------

create table #tPASSpecialty 
	(
	SpecialtyCode int not null
	)

create clustered index #tPASSpecialtyIX on #tPASSpecialty
	(
	SpecialtyCode
	)

-- prepare distinct list

if @dataset = 'APC' or @dataset is null

	insert into #tPASSpecialty
	select distinct
		 coalesce(Encounter.SpecialtyCode, -1)
	from
		dbo.FactAPC Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASSpecialty
		where
			#tPASSpecialty.SpecialtyCode = coalesce(Encounter.SpecialtyCode, -1)
		)

if @dataset = 'OP' or @dataset is null

	insert into #tPASSpecialty
	select distinct
		 coalesce(Encounter.SpecialtyCode, -1)
	from
		dbo.FactOP Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASSpecialty
		where
			#tPASSpecialty.SpecialtyCode = coalesce(Encounter.SpecialtyCode, -1)
		)

if @dataset = 'RF' or @dataset is null

	insert into #tPASSpecialty
	select distinct
		 coalesce(Encounter.SpecialtyCode, -1)
	from
		dbo.FactRF Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASSpecialty
		where
			#tPASSpecialty.SpecialtyCode = coalesce(Encounter.SpecialtyCode, -1)
		)

	insert into #tPASSpecialty
	select distinct
		 coalesce(Encounter.ReferredBySpecialtyCode, -1)
	from
		dbo.FactRF Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASSpecialty
		where
			#tPASSpecialty.SpecialtyCode = coalesce(Encounter.ReferredBySpecialtyCode, -1)
		)


if @dataset = 'APCWL' or @dataset is null

	insert into #tPASSpecialty
	select distinct
		 coalesce(Encounter.SpecialtyCode, -1)
	from
		dbo.FactAPCWaitingList Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASSpecialty
		where
			#tPASSpecialty.SpecialtyCode = coalesce(Encounter.SpecialtyCode, -1)
		)

if @dataset = 'OPWL' or @dataset is null

	insert into #tPASSpecialty
	select distinct
		 coalesce(Encounter.SpecialtyCode, -1)
	from
		dbo.FactOPWaitingList Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASSpecialty
		where
			#tPASSpecialty.SpecialtyCode = coalesce(Encounter.SpecialtyCode, -1)
		)

if @dataset = 'RTT' or @dataset is null

	insert into #tPASSpecialty
	select distinct
		 coalesce(Encounter.SpecialtyCode, -1)
	from
		dbo.FactRTT Encounter
	where
		not exists
		(
		select
			1
		from
			#tPASSpecialty
		where
			#tPASSpecialty.SpecialtyCode = coalesce(Encounter.SpecialtyCode, -1)
		)


--delete all entries if this is ALL datasets
if @dataset is null

	delete from dbo.EntityXref
	where
		EntityTypeCode = 'OLAPPASSPECIALTY'
	and	XrefEntityTypeCode = 'OLAPPASSPECIALTY'
	and	not exists
		(
		select
			1
		from
			#tPASSpecialty
		where
			EntityCode = SpecialtyCode
		)


insert into dbo.EntityXref
(
	 EntityTypeCode
	,EntityCode
	,XrefEntityTypeCode
	,XrefEntityCode
)
select
	 EntityTypeCode
	,EntityCode
	,XrefEntityTypeCode
	,XrefEntityCode
from
	(
	select distinct
		 EntityTypeCode = 'OLAPPASSPECIALTY'
		,EntityCode = SpecialtyCode
		,XrefEntityTypeCode = 'OLAPPASSPECIALTY'
		,XrefEntityCode = SpecialtyCode
	from
		#tPASSpecialty

	union

	select
		 EntityTypeCode = 'OLAPPASSPECIALTY'
		,EntityCode = '-1'
		,XrefEntityTypeCode = 'OLAPPASSPECIALTY'
		,XrefEntityCode = '-1'
	) PASSpecialty

where
	not exists
	(
	select
		1
	from
		dbo.EntityXref
	where
		EntityXref.EntityTypeCode = 'OLAPPASSPECIALTY'
	and	EntityXref.XrefEntityTypeCode = 'OLAPPASSPECIALTY'
	and	EntityXref.EntityCode = PASSpecialty.EntityCode
	and	EntityXref.XrefEntityCode = PASSpecialty.XrefEntityCode
	)


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins, ' +
	'Dataset ' + coalesce(@dataset, 'All')

exec WriteAuditLogEvent 'PAS - WHOLAP BuildOlapPASSpecialty', @Stats, @StartTime



insert into dbo.SpecialtyDivisionBase (
	 SpecialtyCode
	,SubSpecialtyCode
	,SubSpecialty
	,SpecialtyFunctionCode
	,SpecialtyFunction
	,MainSpecialtyCode
	,MainSpecialty
	,RTTSpecialtyCode
	,Direcorate
	,Division
	,RTTExcludeFlag --19/04/2016 CM - Added
	,LastLoadedDateTime
	)
(	Select
	 SpecialtyCode
	,LocalSpecialtyCode
	,Specialty
	,'Unk'
	,'Unknown'
	,'Unk'
	,'Unknown'
	,'Unk'
	,'Unknown'
	,'Unknown'
	,'Y' --19/04/2016 CM - Added
	,GETDATE()
from OlapPASSpecialty
where Specialtycode 
	Not In (select specialtycode from SpecialtyDivisionBase)
)
