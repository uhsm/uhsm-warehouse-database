﻿
CREATE  procedure [dbo].[BuildRTTModel]
	 @censusDate smalldatetime = null
as

--this builds the reporting model for a given census date
--or if no census date supplied rebuilds the latest weekly
declare @StartTime datetime
declare @Elapsed int
--declare @RowsInserted Int
declare @Stats varchar(255)

--declare @from smalldatetime
--declare @to smalldatetime

select @StartTime = getdate()

declare @census smalldatetime

select
	@census = 
		coalesce(
			@censusDate
			,(
			select
				MAX(TheDate)
			from
				dbo.CalendarBase
			where
				datename(dw, TheDate) = 'Sunday'
			and	TheDate <= GETDATE()
			)
		)

exec dbo.BuildBaseRTT @census
exec dbo.BuildFactRTT @census

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

--select @Stats = 
--	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
--		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
--		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
--		CONVERT(varchar(11), coalesce(@to, ''))
		
select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WHOLAP BuildRTTModel', @Stats, @StartTime

