﻿
CREATE PROCEDURE HSCReport
	
AS
BEGIN

	SET NOCOUNT ON
declare @DateFrom as datetime
,@DateTo as datetime

create table TempMedianRow
(RowNumber numeric not null)

insert into TempMedianRow
select 
round(

((count ((DATEDIFF(d, BaseOrder.OrderDate, (CASE WHEN (DictationDate IS NULL OR
                      DictationDate = '1975-12-31 00:00:00.000') AND PerformanceDate IS NOT NULL THEN getdate() ELSE DictationDate END)))))/2),0)
FROM         BaseOrder INNER JOIN
                      OlapCalendar ON OlapCalendar.TheDate = BaseOrder.OrderDate
WHERE     (BaseOrder.PriorityCode = 'HSC205') AND (BaseOrder.OrderDate BETWEEN @DateFrom AND @DateTo)



--main select query

SELECT     
CASE WHEN Department = ('C') THEN ('CT') WHEN Department = ('M') THEN ('MRI') WHEN Department = ('U') THEN ('US') WHEN Department = ('I') 
                      THEN ('Nm') WHEN Department = ('F') THEN ('Ba') ELSE ('Other') END AS Modality
, BaseOrder.Department, CAST(BaseOrder.OrderDate AS Date) 
                      AS OrderDate, CAST(BaseOrder.PerformanceDate AS Date) AS PerformanceDate
, BaseOrder.PatientNo
, BaseOrder.PatientTypeCode
, BaseOrder.OrderNumber
, BaseOrder.ExamDescription
, REPLACE(BaseOrder.OrderingPhysician, ';1', '') AS OrderingPhysician
, CAST(BaseOrder.DictationDate AS Date) AS DictationDate
, BaseOrder.Department AS Department
, BaseOrder.RadiologistCode
, CAST((CASE WHEN (DictationDate IS NULL OR
                      DictationDate = '1975-12-31 00:00:00.000') AND PerformanceDate IS NOT NULL THEN getdate() ELSE DictationDate END) AS Date) AS DateOfReport
, DATEDIFF(d, BaseOrder.OrderDate, BaseOrder.PerformanceDate) AS FiveDayTarget, DATEDIFF(d, BaseOrder.PerformanceDate, 
                      (CASE WHEN (DictationDate IS NULL OR
                      DictationDate = '1975-12-31 00:00:00.000') AND PerformanceDate IS NOT NULL THEN getdate() ELSE DictationDate END)) AS TwoDayTarget
, DATEDIFF(d, BaseOrder.OrderDate, (CASE WHEN (DictationDate IS NULL OR
                      DictationDate = '1975-12-31 00:00:00.000') AND PerformanceDate IS NOT NULL THEN getdate() ELSE DictationDate END)) AS SevenDayTarget
, CASE WHEN (Datediff(d, OrderDate, PerformanceDate)) > 5 THEN COUNT(OrderNumber) ELSE 0 END AS CountFiveDayBreaches
, CASE WHEN (Datediff(d, PerformanceDate, (CASE WHEN DictationDate IS NULL AND PerformanceDate IS NOT NULL THEN getdate() 
                      ELSE DictationDate END))) > 2 THEN COUNT(OrderNumber) ELSE 0 END AS CountTwoDayBreaches, CASE WHEN (Datediff(d, OrderDate
,(CASE WHEN DictationDate IS NULL AND PerformanceDate IS NOT NULL THEN getdate() ELSE DictationDate END))) > 7 THEN COUNT(OrderNumber) 
                      ELSE 0 END AS CountSevenDayBreaches, CASE WHEN DictationDate IS NULL OR
                      DictationDate = '1975-12-31 00:00:00.000' THEN COUNT(OrderNumber) ELSE 0 END AS Unreported, CASE WHEN PerformanceDate IS NULL OR
                      PerformanceDate = '1975-12-31 00:00:00.000' THEN COUNT(OrderNumber) ELSE 0 END AS NotYetPerformed
, OlapCalendar.TheMonth AS MonthOrdered

,MedianSevenDays = (select (DATEDIFF(d, BaseOrder.OrderDate, (CASE WHEN (DictationDate IS NULL OR
                      DictationDate = '1975-12-31 00:00:00.000') AND PerformanceDate IS NOT NULL THEN getdate() ELSE DictationDate END)))
from BaseOrder inner join TempMedianRow

on
RANK() OVER (ORDER BY (DATEDIFF(d, BaseOrder.OrderDate, (CASE WHEN (DictationDate IS NULL OR
                      DictationDate = '1975-12-31 00:00:00.000') AND PerformanceDate IS NOT NULL THEN getdate() ELSE DictationDate END))))
=Rownumber
where (BaseOrder.PriorityCode = 'HSC205') AND (BaseOrder.OrderDate BETWEEN @DateFrom AND @DateTo)

)

FROM BaseOrder INNER JOIN
                      OlapCalendar ON OlapCalendar.TheDate = BaseOrder.OrderDate
WHERE     (BaseOrder.PriorityCode = 'HSC205') AND (BaseOrder.OrderDate BETWEEN @DateFrom AND @DateTo)
GROUP BY BaseOrder.Department, BaseOrder.OrderDate, BaseOrder.PerformanceDate, BaseOrder.PatientNo, BaseOrder.PatientTypeCode, 
                      BaseOrder.OrderNumber, BaseOrder.ExamDescription, BaseOrder.OrderingPhysician, BaseOrder.DictationDate, BaseOrder.Department, 
                      BaseOrder.RadiologistCode, OlapCalendar.TheMonth




drop table TempMedianRow
END

