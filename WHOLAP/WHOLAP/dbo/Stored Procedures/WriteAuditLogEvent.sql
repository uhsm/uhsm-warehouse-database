﻿CREATE   PROCEDURE [WriteAuditLogEvent] 
	 @ProcessCode varchar(255)
	,@Event varchar(255)
	,@StartTime datetime = null
as

exec WH.dbo.WriteAuditLogEvent
	 @ProcessCode
	,@Event
	,@StartTime
