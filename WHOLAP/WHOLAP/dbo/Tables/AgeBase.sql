﻿CREATE TABLE [dbo].[AgeBase] (
    [AgeCode]       VARCHAR (50) NOT NULL,
    [Age]           VARCHAR (50) NULL,
    [AgeParentCode] VARCHAR (50) NULL,
    CONSTRAINT [PK_AgeBase] PRIMARY KEY CLUSTERED ([AgeCode] ASC),
    CONSTRAINT [FK_AgeBase_AgeBase] FOREIGN KEY ([AgeParentCode]) REFERENCES [dbo].[AgeBase] ([AgeCode])
);

