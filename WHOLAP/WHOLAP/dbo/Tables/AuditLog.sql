﻿CREATE TABLE [dbo].[AuditLog] (
    [EventTime]   DATETIME      NOT NULL,
    [UserId]      VARCHAR (30)  NOT NULL,
    [Source]      CHAR (30)     NOT NULL,
    [Event]       VARCHAR (255) NOT NULL,
    [Processed]   INT           NULL,
    [Inserted]    INT           NULL,
    [Deleted]     INT           NULL,
    [Updated]     INT           NULL,
    [Allocated]   INT           NULL,
    [StartDate]   SMALLDATETIME NULL,
    [EndDate]     SMALLDATETIME NULL,
    [ElapsedTime] INT           NULL,
    CONSTRAINT [PK_AuditLog] PRIMARY KEY CLUSTERED ([EventTime] ASC)
);

