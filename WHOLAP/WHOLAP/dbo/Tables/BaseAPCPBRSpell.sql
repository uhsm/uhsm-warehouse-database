﻿CREATE TABLE [dbo].[BaseAPCPBRSpell] (
    [SourceEncounterRecNo]  INT          NOT NULL,
    [SourceSpellNo]         INT          NULL,
    [SpellHRGCode]          VARCHAR (20) NULL,
    [SpellCCSCode]          VARCHAR (3)  NULL,
    [AdjustedExcessBedDays] INT          NULL,
    [FinalExcessBedDays]    INT          NULL,
    [POD2]                  VARCHAR (10) NULL,
    [POD3]                  VARCHAR (10) NULL,
    [PBRstatus]             VARCHAR (20) NULL,
    [DateLoaded]            DATETIME     NULL,
    CONSTRAINT [PK_HRG] PRIMARY KEY CLUSTERED ([SourceEncounterRecNo] ASC)
);

