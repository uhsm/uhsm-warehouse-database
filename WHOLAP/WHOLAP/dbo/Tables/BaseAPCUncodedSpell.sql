﻿CREATE TABLE [dbo].[BaseAPCUncodedSpell] (
    [CensusDate]                  DATE          NOT NULL,
    [DistrictNo]                  VARCHAR (20)  NULL,
    [SourcePatientNo]             INT           NOT NULL,
    [SourceSpellNo]               INT           NOT NULL,
    [AdmissionTime]               DATETIME      NOT NULL,
    [DischargeTime]               DATETIME      NULL,
    [DischargingConsultant]       VARCHAR (100) NULL,
    [SpecialtyCode]               VARCHAR (20)  NULL,
    [Specialty]                   VARCHAR (255) NULL,
    [Lapse (Days)]                INT           NULL,
    [CurrentCasenoteLocationCode] VARCHAR (20)  NULL,
    [CurrentCasenoteLocation]     VARCHAR (255) NULL,
    [IntendedManagement]          VARCHAR (5)   NULL,
    [ReceivedInCoding]            CHAR (1)      NULL,
    [Cases]                       INT           NULL,
    [PatientName]                 VARCHAR (255) NULL,
    [CasenoteMovementComment]     VARCHAR (255) NULL,
    [CurrentWard]                 VARCHAR (20)  NULL,
    [BacklogFlag]                 CHAR (1)      NULL,
    CONSTRAINT [pk_APCUncoded] PRIMARY KEY CLUSTERED ([CensusDate] ASC, [SourcePatientNo] ASC, [SourceSpellNo] ASC)
);

