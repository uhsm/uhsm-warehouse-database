﻿CREATE TABLE [dbo].[BaseAPCWardStay] (
    [SourceUniqueID]    INT           NOT NULL,
    [SequenceNo]        SMALLINT      NOT NULL,
    [SourceSpellNo]     INT           NOT NULL,
    [SourcePatientNo]   INT           NOT NULL,
    [StartTime]         SMALLDATETIME NULL,
    [EndTime]           SMALLDATETIME NULL,
    [WardCode]          INT           NULL,
    [ProvisionalFlag]   BIT           NULL,
    [PASCreated]        DATETIME      NULL,
    [PASUpdated]        DATETIME      NULL,
    [PASCreatedByWhom]  VARCHAR (30)  NULL,
    [PASUpdatedByWhom]  VARCHAR (30)  NULL,
    [Created]           DATETIME      NOT NULL,
    [Updated]           DATETIME      NULL,
    [ByWhom]            VARCHAR (50)  NOT NULL,
    [FCESourceUniqueID] INT           NULL,
    CONSTRAINT [PK_BaseAPCWardStay] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);

