﻿CREATE TABLE [dbo].[BaseClinicalCoding] (
    [SourceUniqueID]         INT           NOT NULL,
    [SourceCode]             VARCHAR (5)   NULL,
    [SourceRecno]            NUMERIC (18)  NULL,
    [SortOrder]              NUMERIC (18)  NULL,
    [ClinicalCodingTime]     SMALLDATETIME NULL,
    [ClinicalCodingTypeCode] VARCHAR (5)   NULL,
    [ClinicalCodingCode]     INT           NULL,
    [PASCreatedDate]         DATETIME      NULL,
    [PASModifiedDate]        DATETIME      NULL,
    [PASCreatedByWhom]       VARCHAR (30)  NULL,
    [PASUpdatedByWhom]       VARCHAR (30)  NULL
);

