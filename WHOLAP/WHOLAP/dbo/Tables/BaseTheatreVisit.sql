﻿CREATE TABLE [dbo].[BaseTheatreVisit] (
    [EncounterRecno]            INT             NOT NULL,
    [SessionID]                 VARCHAR (254)   NULL,
    [SourceUniqueID]            INT             NOT NULL,
    [InterfaceCode]             CHAR (5)        NOT NULL,
    [TheatreCode]               VARCHAR (50)    NOT NULL,
    [SessionTypeCode]           VARCHAR (1)     NULL,
    [CaseTypeCode]              VARCHAR (1)     NULL,
    [TheatreSessionPeriodCode]  VARCHAR (2)     NOT NULL,
    [SessionDate]               SMALLDATETIME   NOT NULL,
    [ConsultantCode]            VARCHAR (10)    NULL,
    [CaseConsultantCode]        VARCHAR (50)    NULL,
    [OriginalConsultantCode]    VARCHAR (10)    NULL,
    [ConsultantAllocationCode]  VARCHAR (2)     NULL,
    [CancelReasonCode]          VARCHAR (50)    NULL,
    [EpisodicConsultantCode]    VARCHAR (10)    NULL,
    [SurgeonCode]               VARCHAR (50)    NULL,
    [AnaesthetistCode]          VARCHAR (50)    NULL,
    [PrimaryProcedureCode]      VARCHAR (10)    NULL,
    [AgeCode]                   VARCHAR (27)    NULL,
    [SexCode]                   VARCHAR (2)     NULL,
    [PatientCategoryCode]       VARCHAR (50)    NULL,
    [SeverityCode]              VARCHAR (2)     NOT NULL,
    [Visit]                     INT             NOT NULL,
    [ReceptionTime]             INT             NULL,
    [ReceptionWaitTime]         INT             NULL,
    [AnaestheticWaitTime]       INT             NULL,
    [AnaestheticTime]           INT             NULL,
    [OperationTime]             INT             NULL,
    [RecoveryTime]              INT             NULL,
    [ReturnTime]                INT             NULL,
    [PatientTime]               INT             NULL,
    [ReportableFlag]            INT             NULL,
    [ASACode]                   VARCHAR (50)    NULL,
    [ATO]                       VARCHAR (50)    NULL,
    [ATORelieved]               VARCHAR (50)    NULL,
    [ActualOperationLine1]      VARCHAR (50)    NULL,
    [ActualOperationLine2]      VARCHAR (50)    NULL,
    [ActualOperationLine3]      VARCHAR (50)    NULL,
    [ActualOperationLine4]      VARCHAR (50)    NULL,
    [AnaestheticComments]       VARCHAR (50)    NULL,
    [AnaestheticNurse1]         VARCHAR (50)    NULL,
    [AnaestheticNurse1Relieved] VARCHAR (50)    NULL,
    [AnaestheticNurse2]         VARCHAR (50)    NULL,
    [AnaestheticNurse2Relieved] VARCHAR (50)    NULL,
    [AnaestheticType1]          NUMERIC (15, 2) NULL,
    [AnaestheticType2]          NUMERIC (15, 2) NULL,
    [AnaestheticType3]          NUMERIC (15, 2) NULL,
    [AnaestheticType4]          NUMERIC (15, 2) NULL,
    [Anaesthetist1]             VARCHAR (50)    NULL,
    [Anaesthetist2]             VARCHAR (50)    NULL,
    [Anaesthetist3]             VARCHAR (50)    NULL,
    [CEPOD]                     VARCHAR (50)    NULL,
    [CancelReason]              VARCHAR (50)    NULL,
    [CirculatingNurse]          VARCHAR (50)    NULL,
    [CirculatingNurseRelieved]  VARCHAR (50)    NULL,
    [DestinationFromRecovery]   NUMERIC (15, 2) NULL,
    [Episode]                   INT             NOT NULL,
    [EscortName]                VARCHAR (50)    NULL,
    [EscortNurse]               VARCHAR (50)    NULL,
    [EscortNurseRelieved]       VARCHAR (50)    NULL,
    [ExpectedLengthOfOperation] NUMERIC (15, 2) NULL,
    [FirstAssistant]            VARCHAR (50)    NULL,
    [IntendedManagement]        VARCHAR (50)    NULL,
    [InternalDistrictNumber]    VARCHAR (50)    NULL,
    [OtherNurse]                VARCHAR (50)    NULL,
    [OtherNurseRelieved]        VARCHAR (50)    NULL,
    [Outcome]                   NUMERIC (15, 2) NULL,
    [PatientAccompanied]        VARCHAR (50)    NULL,
    [PatientLocation]           VARCHAR (50)    NULL,
    [PlannedAnaesthetic]        VARCHAR (50)    NULL,
    [PortersActivityReception]  VARCHAR (50)    NULL,
    [ProcedureDate]             SMALLDATETIME   NULL,
    [ProcedureTime]             SMALLDATETIME   NULL,
    [ProposedOperationLine1]    VARCHAR (50)    NULL,
    [ProposedOperationLine2]    VARCHAR (50)    NULL,
    [ProposedOperationLine3]    VARCHAR (50)    NULL,
    [ProposedOperationLine4]    VARCHAR (50)    NULL,
    [ReasonDelayReception1]     NUMERIC (15, 2) NULL,
    [ReasonDelayReception2]     NUMERIC (15, 2) NULL,
    [ReasonDelayRecovery1]      NUMERIC (15, 2) NULL,
    [ReasonDelayRecovery2]      NUMERIC (15, 2) NULL,
    [ReceptionComments]         VARCHAR (50)    NULL,
    [ReceptionNurse1]           VARCHAR (50)    NULL,
    [ReceptionNurse2]           VARCHAR (50)    NULL,
    [ReceptionPorter]           VARCHAR (50)    NULL,
    [RecoveryComments]          VARCHAR (50)    NULL,
    [RecoveryPorter]            VARCHAR (50)    NULL,
    [RevoveryNurse]             VARCHAR (50)    NULL,
    [RevoveryNurseRelieved]     VARCHAR (50)    NULL,
    [ScrubNurse1]               VARCHAR (50)    NULL,
    [ScrubNurse1Relieved]       VARCHAR (50)    NULL,
    [ScrubNurse2]               VARCHAR (50)    NULL,
    [ScrubNurse2Relieved]       VARCHAR (50)    NULL,
    [SpecialEquipment1]         NUMERIC (15, 2) NULL,
    [SpecialEquipment2]         NUMERIC (15, 2) NULL,
    [SpecialEquipment3]         NUMERIC (15, 2) NULL,
    [SpecialEquipment4]         NUMERIC (15, 2) NULL,
    [Surgeon1]                  VARCHAR (50)    NULL,
    [Surgeon2]                  VARCHAR (50)    NULL,
    [Surgeon3]                  VARCHAR (50)    NULL,
    [TheatreComments]           VARCHAR (50)    NULL,
    [TheatreNumber]             VARCHAR (50)    NULL,
    [TimeAnaestheticStart]      SMALLDATETIME   NULL,
    [TimeArrived]               SMALLDATETIME   NULL,
    [TimeAwake]                 SMALLDATETIME   NULL,
    [TimeInAnaestheticRoom]     SMALLDATETIME   NULL,
    [TimeInRecovery]            SMALLDATETIME   NULL,
    [TimeOffTable]              SMALLDATETIME   NULL,
    [TimeOnTable]               SMALLDATETIME   NULL,
    [TimeOutAnaestheticRoom]    SMALLDATETIME   NULL,
    [TimeOutRecovery]           SMALLDATETIME   NULL,
    [TimeReadyForDischarge]     SMALLDATETIME   NULL,
    [TimeSentFor]               SMALLDATETIME   NULL,
    [WardCode]                  VARCHAR (50)    NULL,
    [SourcePatientNo]           VARCHAR (50)    NULL,
    [PASEncounterRecno]         INT             NULL,
    CONSTRAINT [PK_BaseTheatreVisit] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);

