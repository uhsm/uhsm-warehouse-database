﻿CREATE TABLE [dbo].[CalendarBase] (
    [TheDate] DATETIME NOT NULL,
    CONSTRAINT [PK_CalendarBase] PRIMARY KEY CLUSTERED ([TheDate] ASC) WITH (FILLFACTOR = 90)
);

