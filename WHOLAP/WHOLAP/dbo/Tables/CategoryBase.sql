﻿CREATE TABLE [dbo].[CategoryBase] (
    [CategoryCode]       VARCHAR (50)  NOT NULL,
    [Category]           VARCHAR (255) NOT NULL,
    [ParentCategoryCode] VARCHAR (50)  NULL,
    CONSTRAINT [PK_CategoryBase] PRIMARY KEY CLUSTERED ([CategoryCode] ASC),
    CONSTRAINT [FK_CategoryBase_CategoryBase] FOREIGN KEY ([ParentCategoryCode]) REFERENCES [dbo].[CategoryBase] ([CategoryCode])
);

