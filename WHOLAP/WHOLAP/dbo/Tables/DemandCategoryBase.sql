﻿CREATE TABLE [dbo].[DemandCategoryBase] (
    [DemandCategoryCode]       VARCHAR (50)  NOT NULL,
    [DemandCategory]           VARCHAR (255) NOT NULL,
    [DemandCategoryParentCode] VARCHAR (50)  NULL,
    CONSTRAINT [PK_DemandCategoryBase] PRIMARY KEY CLUSTERED ([DemandCategoryCode] ASC)
);

