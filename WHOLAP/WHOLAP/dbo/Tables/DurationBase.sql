﻿CREATE TABLE [dbo].[DurationBase] (
    [DurationCode]       VARCHAR (10) NOT NULL,
    [Duration]           VARCHAR (50) NULL,
    [DurationParentCode] VARCHAR (10) NULL,
    CONSTRAINT [PK_DurationBase] PRIMARY KEY CLUSTERED ([DurationCode] ASC),
    CONSTRAINT [FK_DurationBase_DurationBase] FOREIGN KEY ([DurationParentCode]) REFERENCES [dbo].[DurationBase] ([DurationCode])
);

