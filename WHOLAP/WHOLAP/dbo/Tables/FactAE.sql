﻿CREATE TABLE [dbo].[FactAE] (
    [SourceUniqueID]            VARCHAR (50) NOT NULL,
    [StageCode]                 VARCHAR (27) NOT NULL,
    [ArrivalModeCode]           VARCHAR (10) NULL,
    [AttendanceDisposalCode]    VARCHAR (10) NULL,
    [SiteCode]                  VARCHAR (10) NULL,
    [RegisteredPracticeCode]    VARCHAR (10) NULL,
    [AttendanceCategoryCode]    VARCHAR (10) NULL,
    [ReferredToSpecialtyCode]   VARCHAR (10) NULL,
    [LevelOfCareCode]           VARCHAR (10) NOT NULL,
    [EncounterBreachStatusCode] VARCHAR (1)  NOT NULL,
    [EncounterStartTimeOfDay]   INT          NULL,
    [EncounterEndTimeOfDay]     INT          NULL,
    [EncounterStartDate]        DATETIME     NULL,
    [EncounterEndDate]          DATETIME     NULL,
    [StageBreachStatusCode]     VARCHAR (1)  NOT NULL,
    [StageDurationMinutes]      INT          NULL,
    [StageBreachMinutes]        INT          NULL,
    [StageStartTimeOfDay]       INT          NULL,
    [StageEndTimeOfDay]         INT          NULL,
    [StageStartDate]            DATETIME     NULL,
    [StageEndDate]              DATETIME     NULL,
    [Cases]                     FLOAT (53)   NOT NULL,
    [BreachValue]               INT          NULL,
    [DelayCode]                 INT          NULL,
    [EncounterDurationMinutes]  INT          NULL,
    [InterfaceCode]             VARCHAR (10) NULL,
    [DurationBandCode]          VARCHAR (10) NULL,
    [WhereSeen]                 INT          NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_FactAE]
    ON [dbo].[FactAE]([ReferredToSpecialtyCode] ASC);

