﻿CREATE TABLE [dbo].[FactAPC] (
    [EncounterRecno]        INT           NOT NULL,
    [MetricCode]            VARCHAR (10)  NOT NULL,
    [EncounterDate]         SMALLDATETIME NOT NULL,
    [ConsultantCode]        INT           NOT NULL,
    [SpecialtyCode]         INT           NOT NULL,
    [PracticeCode]          INT           NOT NULL,
    [SiteCode]              INT           NOT NULL,
    [AgeCode]               VARCHAR (50)  NOT NULL,
    [Cases]                 INT           NOT NULL,
    [LengthOfEncounter]     INT           NULL,
    [PrimaryDiagnosisCode]  VARCHAR (10)  NULL,
    [PrimaryOperationCode]  VARCHAR (10)  NULL,
    [AdmissionMethodCode]   INT           NULL,
    [StartServicePointCode] INT           NULL,
    [EndServicePointCode]   INT           NULL,
    [PreOpBedDays]          INT           NULL,
    CONSTRAINT [PK_FactAPC] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC, [MetricCode] ASC)
);

