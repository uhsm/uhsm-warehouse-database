﻿CREATE TABLE [dbo].[FactAPCWaitingList] (
    [EncounterRecno]            INT           NOT NULL,
    [Cases]                     INT           NOT NULL,
    [LengthOfWait]              INT           NULL,
    [AgeCode]                   VARCHAR (50)  NOT NULL,
    [CensusDate]                SMALLDATETIME NOT NULL,
    [ConsultantCode]            INT           NOT NULL,
    [SpecialtyCode]             INT           NOT NULL,
    [DurationCode]              VARCHAR (2)   NOT NULL,
    [PracticeCode]              INT           NOT NULL,
    [SiteCode]                  INT           NOT NULL,
    [WaitTypeCode]              VARCHAR (10)  NOT NULL,
    [StatusCode]                VARCHAR (10)  NOT NULL,
    [CategoryCode]              VARCHAR (10)  NOT NULL,
    [AddedLastWeek]             INT           NULL,
    [WaitingListAddMinutes]     INT           NULL,
    [OPCSCoded]                 INT           NULL,
    [WithRTTStartDate]          INT           NULL,
    [WithRTTStatusCode]         INT           NULL,
    [WithExpectedAdmissionDate] INT           NULL,
    CONSTRAINT [PK_APCWaitingListFact] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);

