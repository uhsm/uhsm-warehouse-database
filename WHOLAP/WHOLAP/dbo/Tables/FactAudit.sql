﻿CREATE TABLE [dbo].[FactAudit] (
    [AuditLogRecno]       INT           NOT NULL,
    [AuditDate]           DATETIME      NULL,
    [AuditStartTimeOfDay] INT           NULL,
    [AuditEndTimeOfDay]   INT           NULL,
    [StartTime]           DATETIME      NULL,
    [EndTime]             DATETIME      NOT NULL,
    [UserId]              VARCHAR (30)  NOT NULL,
    [ProcessCode]         VARCHAR (255) NOT NULL,
    [Event]               VARCHAR (255) NOT NULL,
    [ProcessSeconds]      INT           NULL,
    CONSTRAINT [PK_FactAudit] PRIMARY KEY CLUSTERED ([AuditLogRecno] ASC)
);

