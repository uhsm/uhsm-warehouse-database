﻿CREATE TABLE [dbo].[FactClinicalCoding] (
    [ClinicalCodingDate]     SMALLDATETIME NULL,
    [ClinicalCodingTypeCode] VARCHAR (5)   NULL,
    [ClinicalCodingCode]     INT           NULL,
    [SequenceNo]             NUMERIC (18)  NULL,
    [SourceCode]             VARCHAR (5)   NULL,
    [SourceRecno]            NUMERIC (18)  NULL,
    [PASCreatedByWhom]       VARCHAR (30)  NULL,
    [PASCreatedDate]         DATE          NULL
);

