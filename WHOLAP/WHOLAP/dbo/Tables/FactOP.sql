﻿CREATE TABLE [dbo].[FactOP] (
    [EncounterRecno]       INT           NOT NULL,
    [MetricCode]           VARCHAR (10)  NOT NULL,
    [EncounterDate]        SMALLDATETIME NULL,
    [ConsultantCode]       INT           NULL,
    [SpecialtyCode]        INT           NULL,
    [PracticeCode]         INT           NULL,
    [SiteCode]             INT           NULL,
    [AgeCode]              VARCHAR (50)  NULL,
    [ClinicCode]           VARCHAR (20)  NULL,
    [SourceOfReferralCode] INT           NULL,
    [BookingTypeCode]      INT           NULL,
    [Cases]                INT           NOT NULL,
    [LengthOfWait]         INT           NULL,
    [IsWardAttender]       BIT           NULL,
    [ClinicType]           INT           NULL,
    [ExludedClinic]        BIT           NULL,
    [IsWalkIn]             BIT           NULL,
    CONSTRAINT [PK_FactOP] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC, [MetricCode] ASC)
);

