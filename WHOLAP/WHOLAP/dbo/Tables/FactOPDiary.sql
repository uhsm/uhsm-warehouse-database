﻿CREATE TABLE [dbo].[FactOPDiary] (
    [SourceUniqueID]           VARCHAR (254) NOT NULL,
    [ClinicCode]               VARCHAR (8)   NOT NULL,
    [SessionCode]              VARCHAR (8)   NOT NULL,
    [SessionDescription]       VARCHAR (255) NULL,
    [SessionDate]              SMALLDATETIME NOT NULL,
    [SessionStartTime]         SMALLDATETIME NULL,
    [SessionEndTime]           SMALLDATETIME NULL,
    [ReasonForCancellation]    VARCHAR (30)  NULL,
    [SessionPeriod]            VARCHAR (3)   NULL,
    [DoctorCode]               VARCHAR (10)  NULL,
    [ValidAppointmentTypeCode] VARCHAR (23)  NULL,
    [Slots]                    INT           NULL,
    [UsedSlots]                INT           NULL,
    [FreeSlots]                INT           NULL,
    [Sessions]                 INT           NOT NULL,
    [CancelledSessions]        INT           NOT NULL,
    [CancelledSlots]           INT           NULL,
    [SessionMinutesDuration]   INT           NULL,
    CONSTRAINT [PK_FactOPDiary] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);

