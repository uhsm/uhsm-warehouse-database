﻿CREATE TABLE [dbo].[FactOPSession] (
    [SessionUniqueID]         INT  NOT NULL,
    [ServicePointUniqueID]    INT  NULL,
    [SessionSpecialtyCode]    INT  NULL,
    [SessionDate]             DATE NULL,
    [Sessions]                INT  NOT NULL,
    [MaxNumberOfAppointments] INT  NULL,
    CONSTRAINT [PK_FactOPSession] PRIMARY KEY CLUSTERED ([SessionUniqueID] ASC)
);

