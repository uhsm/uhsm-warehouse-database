﻿CREATE TABLE [dbo].[FactOPSessionSlot] (
    [SessionSpecialtyCode] INT  NULL,
    [SessionDate]          DATE NULL,
    [BookedSlots]          INT  NOT NULL,
    [BookingsAllowed]      INT  NULL,
    [BookingsFree]         INT  NULL
);

