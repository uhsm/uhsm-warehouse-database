﻿CREATE TABLE [dbo].[FactOPSlot] (
    [SessionUniqueID]      INT  NOT NULL,
    [ServicePointUniqueID] INT  NULL,
    [SessionSpecialtyCode] INT  NULL,
    [SessionDate]          DATE NULL,
    [SlotVisitTypeCode]    INT  NULL,
    [SlotPriorityCode]     INT  NULL,
    [BookedSlots]          INT  NOT NULL,
    [MaxNumberOfBookings]  INT  NULL
);

