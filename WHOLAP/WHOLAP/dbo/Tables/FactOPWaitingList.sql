﻿CREATE TABLE [dbo].[FactOPWaitingList] (
    [EncounterRecno]       INT           NOT NULL,
    [Cases]                INT           NOT NULL,
    [LengthOfWait]         INT           NOT NULL,
    [AgeCode]              VARCHAR (50)  NOT NULL,
    [CensusDate]           SMALLDATETIME NOT NULL,
    [ConsultantCode]       INT           NOT NULL,
    [SpecialtyCode]        INT           NOT NULL,
    [DurationCode]         VARCHAR (2)   NOT NULL,
    [PracticeCode]         INT           NOT NULL,
    [SiteCode]             INT           NOT NULL,
    [SourceOfReferralCode] INT           NOT NULL,
    [WithAppointment]      INT           NOT NULL,
    CONSTRAINT [PK_OPWaitingListFact] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);

