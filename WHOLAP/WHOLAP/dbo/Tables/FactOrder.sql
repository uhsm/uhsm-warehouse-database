﻿CREATE TABLE [dbo].[FactOrder] (
    [EncounterRecno]      INT           NOT NULL,
    [ModalityCode]        NVARCHAR (10) NULL,
    [ExamCode]            NVARCHAR (10) NULL,
    [OrderStaffCode]      NVARCHAR (10) NULL,
    [OrderLocationCode]   NVARCHAR (10) NULL,
    [OrderDate]           DATETIME      NULL,
    [PerformStaffCode]    NVARCHAR (10) NULL,
    [PerformLocationCode] NVARCHAR (10) NULL,
    [PerformDate]         DATETIME      NULL,
    [PerformTimeBandCode] INT           NULL,
    [ReportStaffCode]     NVARCHAR (10) NULL,
    [ReportLocationCode]  NVARCHAR (10) NULL,
    [ReportDate]          DATETIME      NULL,
    [ReportTimeBandCode]  INT           NULL,
    [ReportBreachFlag]    BIT           NULL,
    [Ordered]             INT           NOT NULL,
    [Performed]           INT           NOT NULL,
    [Reported]            INT           NOT NULL,
    [PerformanceDays]     INT           NULL,
    [ReportingDays]       INT           NULL,
    [DictationDate]       DATETIME      NULL,
    [TechnicianCode]      FLOAT (53)    NULL,
    [CaseGroupCode]       VARCHAR (5)   NULL,
    CONSTRAINT [PK_FactOrder] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_FactOrder]
    ON [dbo].[FactOrder]([OrderLocationCode] ASC);

