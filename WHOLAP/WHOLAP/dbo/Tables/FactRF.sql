﻿CREATE TABLE [dbo].[FactRF] (
    [EncounterRecno]              INT           NOT NULL,
    [EncounterDate]               SMALLDATETIME NULL,
    [ConsultantCode]              INT           NULL,
    [SpecialtyCode]               INT           NULL,
    [PracticeCode]                INT           NULL,
    [SiteCode]                    INT           NULL,
    [AgeCode]                     VARCHAR (50)  NULL,
    [SourceOfReferralCode]        INT           NULL,
    [Cases]                       INT           NOT NULL,
    [ReferralTypeCode]            INT           NULL,
    [ResponsibleOrganisationCode] VARCHAR (50)  NULL,
    [ReferralPriorityCode]        INT           NULL,
    [ReferredByConsultantCode]    INT           NULL,
    [ReferredByOrganisationCode]  INT           NULL,
    [ReferredBySpecialtyCode]     INT           NULL,
    [ReferralCategoryCode]        INT           NULL,
    CONSTRAINT [PK_FactRF] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);

