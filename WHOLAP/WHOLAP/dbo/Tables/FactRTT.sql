﻿CREATE TABLE [dbo].[FactRTT] (
    [CensusDate]            SMALLDATETIME NOT NULL,
    [SourceEncounterRecno]  INT           NOT NULL,
    [SourceCode]            VARCHAR (10)  NOT NULL,
    [AdjustedFlag]          BIT           NOT NULL,
    [ConsultantCode]        INT           NOT NULL,
    [SpecialtyCode]         INT           NOT NULL,
    [PracticeCode]          INT           NOT NULL,
    [PathwayStatusCode]     VARCHAR (10)  NOT NULL,
    [WeekBandCode]          VARCHAR (2)   NOT NULL,
    [BreachBandCode]        VARCHAR (4)   NOT NULL,
    [Cases]                 INT           NOT NULL,
    [LastWeeksCases]        INT           NOT NULL,
    [KeyDate]               SMALLDATETIME NOT NULL,
    [OperationCode]         INT           NOT NULL,
    [ReferringProviderCode] VARCHAR (3)   NULL,
    [DaysWaiting]           INT           NULL,
    CONSTRAINT [PK_FactRTT] PRIMARY KEY CLUSTERED ([CensusDate] ASC, [SourceEncounterRecno] ASC, [SourceCode] ASC, [AdjustedFlag] ASC)
);

