﻿CREATE TABLE [dbo].[FactReadmission] (
    [ReadmissionRecno]                     INT           NOT NULL,
    [ReadmissionAdmissionDate]             SMALLDATETIME NULL,
    [ReadmissionConsultantCode]            INT           NULL,
    [ReadmissionSpecialtyCode]             INT           NULL,
    [ReadmissionPracticeCode]              INT           NULL,
    [ReadmissionAdmissionMethodCode]       INT           NULL,
    [Cases]                                INT           NOT NULL,
    [PreviousAdmissionRecno]               INT           NOT NULL,
    [PreviousAdmissionDischargeDate]       SMALLDATETIME NULL,
    [PreviousAdmissionConsultantCode]      INT           NULL,
    [PreviousAdmissionSpecialtyCode]       INT           NULL,
    [PreviousAdmissionPracticeCode]        INT           NULL,
    [PreviousAdmissionAdmissionMethodCode] INT           NULL,
    [ReadmissionIntervalCode]              VARCHAR (2)   NULL,
    [ReadmissionIntervalDays]              INT           NULL,
    [MetricCode]                           VARCHAR (9)   NOT NULL,
    CONSTRAINT [PK_FactReadmission_1] PRIMARY KEY CLUSTERED ([ReadmissionRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [ix_PrevReadRecno]
    ON [dbo].[FactReadmission]([PreviousAdmissionRecno] ASC);


GO
CREATE NONCLUSTERED INDEX [ixReadmissionMethodCode]
    ON [dbo].[FactReadmission]([ReadmissionAdmissionMethodCode] ASC)
    INCLUDE([ReadmissionRecno], [PreviousAdmissionRecno], [ReadmissionIntervalDays]);

