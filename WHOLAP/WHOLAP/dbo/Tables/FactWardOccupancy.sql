﻿CREATE TABLE [dbo].[FactWardOccupancy] (
    [SourceUniqueID]      INT           NOT NULL,
    [CensusDate]          DATETIME      NOT NULL,
    [StartTime]           SMALLDATETIME NULL,
    [EndTime]             SMALLDATETIME NULL,
    [WardCode]            INT           NULL,
    [PatientCategoryCode] VARCHAR (5)   NOT NULL,
    [AuditPeriod]         INT           NOT NULL,
    [SpecialtyCode]       INT           NULL,
    [ConsultantCode]      INT           NULL,
    [Cases]               INT           NOT NULL
);

