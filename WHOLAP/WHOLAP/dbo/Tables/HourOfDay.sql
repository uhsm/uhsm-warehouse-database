﻿CREATE TABLE [dbo].[HourOfDay] (
    [HourOfDayCode] TINYINT      NOT NULL,
    [HourOfDay]     VARCHAR (50) NULL,
    [Morning]       BIT          NULL,
    CONSTRAINT [PK_HourOfDay] PRIMARY KEY CLUSTERED ([HourOfDayCode] ASC)
);

