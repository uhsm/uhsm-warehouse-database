﻿CREATE TABLE [dbo].[MetricIPBase] (
    [MetricCode]       VARCHAR (10) NOT NULL,
    [Metric]           VARCHAR (50) NULL,
    [MetricParentCode] VARCHAR (10) NULL,
    CONSTRAINT [PK_MetricIPBase] PRIMARY KEY CLUSTERED ([MetricCode] ASC),
    CONSTRAINT [FK_MetricIPBase_MetricIPBase] FOREIGN KEY ([MetricParentCode]) REFERENCES [dbo].[MetricIPBase] ([MetricCode])
);

