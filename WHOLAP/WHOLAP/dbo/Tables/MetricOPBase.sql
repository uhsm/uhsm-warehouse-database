﻿CREATE TABLE [dbo].[MetricOPBase] (
    [MetricCode]       VARCHAR (10) NOT NULL,
    [Metric]           VARCHAR (50) NULL,
    [MetricParentCode] VARCHAR (10) NULL,
    CONSTRAINT [PK_MetricOPBase] PRIMARY KEY CLUSTERED ([MetricCode] ASC),
    CONSTRAINT [FK_MetricOPBase_MetricOPBase] FOREIGN KEY ([MetricParentCode]) REFERENCES [dbo].[MetricOPBase] ([MetricCode])
);

