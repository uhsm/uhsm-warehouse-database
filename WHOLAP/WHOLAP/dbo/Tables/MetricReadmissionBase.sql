﻿CREATE TABLE [dbo].[MetricReadmissionBase] (
    [MetricCode]       VARCHAR (10) NOT NULL,
    [Metric]           VARCHAR (50) NULL,
    [MetricParentCode] VARCHAR (10) NULL,
    CONSTRAINT [PK_MetricReadmissionBase] PRIMARY KEY CLUSTERED ([MetricCode] ASC),
    CONSTRAINT [FK_MetricReadmissionBase_MetricReadmissionBase] FOREIGN KEY ([MetricParentCode]) REFERENCES [dbo].[MetricReadmissionBase] ([MetricCode])
);

