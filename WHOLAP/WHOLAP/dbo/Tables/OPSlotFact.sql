﻿CREATE TABLE [dbo].[OPSlotFact] (
    [SlotFactRecno]        INT           IDENTITY (1, 1) NOT NULL,
    [SlotUniqueID]         INT           NOT NULL,
    [SessionUniqueID]      INT           NULL,
    [ServicePointUniqueID] INT           NULL,
    [SessionDate]          DATE          NULL,
    [SessionCancelledDate] SMALLDATETIME NULL,
    [SessionCancelledFlag] BIT           NULL,
    [SlotDate]             DATE          NULL,
    [Overbooking]          CHAR (1)      NULL,
    [Available]            INT           NULL,
    [Booked]               INT           NULL,
    [NewYN]                BIT           NULL,
    [ReviewYN]             BIT           NULL
);

