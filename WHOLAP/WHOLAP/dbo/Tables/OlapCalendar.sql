﻿CREATE TABLE [dbo].[OlapCalendar] (
    [TheDate]             DATETIME     NOT NULL,
    [DayOfWeek]           VARCHAR (20) NULL,
    [DayOfWeekKey]        INT          NULL,
    [LongDate]            VARCHAR (20) NULL,
    [TheMonth]            VARCHAR (20) NULL,
    [FinancialQuarter]    VARCHAR (20) NULL,
    [FinancialYear]       VARCHAR (20) NULL,
    [FinancialMonthKey]   INT          NULL,
    [FinancialQuarterKey] INT          NULL,
    [CalendarQuarter]     VARCHAR (20) NULL,
    [CalendarSemester]    VARCHAR (20) NOT NULL,
    [CalendarYear]        VARCHAR (20) NULL,
    [LastCompleteMonth]   INT          NOT NULL,
    [WeekNoKey]           VARCHAR (20) NULL,
    [WeekNo]              VARCHAR (20) NULL,
    [FirstDayOfWeek]      DATETIME     NULL,
    [LastDayOfWeek]       DATETIME     NULL,
    [FirstDateOfMonth]    DATETIME     NULL
);

