﻿CREATE TABLE [dbo].[OlapCensus] (
    [CensusDate]  SMALLDATETIME NOT NULL,
    [Census]      VARCHAR (11)  NULL,
    [LastInMonth] INT           NOT NULL,
    [IsRTT]       BIT           NULL,
    CONSTRAINT [PK_OlapCensus] PRIMARY KEY CLUSTERED ([CensusDate] ASC)
);

