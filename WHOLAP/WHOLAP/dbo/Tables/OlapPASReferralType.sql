﻿CREATE TABLE [dbo].[OlapPASReferralType] (
    [ReferralTypeCode] INT          IDENTITY (1, 1) NOT NULL,
    [ReferralType]     VARCHAR (50) NULL,
    CONSTRAINT [PK_ReferralType] PRIMARY KEY CLUSTERED ([ReferralTypeCode] ASC)
);

