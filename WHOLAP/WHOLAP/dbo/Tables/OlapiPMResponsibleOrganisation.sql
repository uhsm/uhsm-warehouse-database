﻿CREATE TABLE [dbo].[OlapiPMResponsibleOrganisation] (
    [OrganisationCode] VARCHAR (20)  NOT NULL,
    [OrganisationName] VARCHAR (255) NULL,
    [OrganisationType] VARCHAR (255) NULL
);

