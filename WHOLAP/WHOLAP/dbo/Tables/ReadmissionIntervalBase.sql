﻿CREATE TABLE [dbo].[ReadmissionIntervalBase] (
    [ReadmissionIntervalCode] VARCHAR (2)  NOT NULL,
    [ReadmissionInterval]     VARCHAR (50) NULL,
    CONSTRAINT [PK_ReadmissionIntervalBase] PRIMARY KEY CLUSTERED ([ReadmissionIntervalCode] ASC)
);

