﻿CREATE TABLE [dbo].[SpecialtyDivisionBase] (
    [SpecialtyCode]         INT          NOT NULL,
    [SubSpecialtyCode]      VARCHAR (20) NULL,
    [SubSpecialty]          VARCHAR (80) NULL,
    [SpecialtyFunctionCode] VARCHAR (20) NULL,
    [SpecialtyFunction]     VARCHAR (80) NULL,
    [MainSpecialtyCode]     VARCHAR (20) NULL,
    [MainSpecialty]         VARCHAR (80) NULL,
    [RTTSpecialtyCode]      VARCHAR (20) NULL,
    [RTTExcludeFlag]        VARCHAR (1)  NOT NULL,
    [Direcorate]            VARCHAR (80) NULL,
    [Division]              VARCHAR (80) NULL,
    [LastLoadedDateTime]    DATETIME     NOT NULL,
    CONSTRAINT [PK_SpecialtyDivisionBase] PRIMARY KEY CLUSTERED ([SpecialtyCode] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_SpecialtyDivisionBase_SubSpecialtyCode]
    ON [dbo].[SpecialtyDivisionBase]([SubSpecialtyCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SpecialtyDivisionBase_SpecialtyFunctionCode]
    ON [dbo].[SpecialtyDivisionBase]([SpecialtyFunctionCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SpecialtyDivisionBase_MainSpecialtyCode]
    ON [dbo].[SpecialtyDivisionBase]([MainSpecialtyCode] ASC);

