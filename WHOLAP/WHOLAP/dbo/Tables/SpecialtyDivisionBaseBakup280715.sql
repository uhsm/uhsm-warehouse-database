﻿CREATE TABLE [dbo].[SpecialtyDivisionBaseBakup280715] (
    [SpecialtyCode]         INT          NOT NULL,
    [SubSpecialtyCode]      VARCHAR (20) NULL,
    [SubSpecialty]          VARCHAR (80) NULL,
    [SpecialtyFunctionCode] VARCHAR (20) NULL,
    [SpecialtyFunction]     VARCHAR (80) NULL,
    [MainSpecialtyCode]     VARCHAR (20) NULL,
    [MainSpecialty]         VARCHAR (80) NULL,
    [RTTSpecialtyCode]      VARCHAR (20) NULL,
    [Direcorate]            VARCHAR (80) NULL,
    [Division]              VARCHAR (80) NULL
);

