﻿CREATE TABLE [dbo].[olapPASReferralCategory] (
    [ReferralCategoryCode] INT           IDENTITY (1, 1) NOT NULL,
    [RequestedServiceCode] INT           NULL,
    [RequestedService]     VARCHAR (100) NULL,
    [ReferralTypeCode]     INT           NULL,
    [ReferralType]         VARCHAR (100) NULL,
    CONSTRAINT [pk_OlapPASReferralCategory] PRIMARY KEY CLUSTERED ([ReferralCategoryCode] ASC)
);

