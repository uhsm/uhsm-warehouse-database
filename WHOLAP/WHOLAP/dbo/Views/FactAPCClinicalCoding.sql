﻿create view FactAPCClinicalCoding as

select
	 FactAPC.AdmissionMethodCode
	,FactAPC.AgeCode
	,FactAPC.Cases
	,FactAPC.ConsultantCode
	,FactAPC.EncounterDate
	,FactAPC.LengthOfEncounter
	,FactAPC.MetricCode
	,FactAPC.PracticeCode
	,FactAPC.SiteCode
	,FactAPC.SpecialtyCode
	,FactClinicalCoding.ClinicalCodingCode
	,FactClinicalCoding.ClinicalCodingDate
	,FactClinicalCoding.ClinicalCodingTypeCode
	,FactClinicalCoding.SequenceNo
from
	dbo.FactAPC

inner join dbo.BaseAPC
on	BaseAPC.EncounterRecno = FactAPC.EncounterRecno

inner join dbo.FactClinicalCoding
on	FactClinicalCoding.SourceCode = 'PRCAE'
and	FactClinicalCoding.SourceRecno = BaseAPC.SourceUniqueID


