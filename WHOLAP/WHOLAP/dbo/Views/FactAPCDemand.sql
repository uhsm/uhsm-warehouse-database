﻿
CREATE view [dbo].[FactAPCDemand] as

select
	 EncounterFact.EncounterRecno
	,EncounterFact.EncounterDate
	,EncounterFact.ConsultantCode

	,SpecialtyCode =
		EncounterFact.SpecialtyCode
	--case
	--when EncounterFact.ConsultantCode in ('RAM', 'CON', 'MJG', 'RCP', 'NHB', 'DNF', 'RG', 'LJQ', 'RKS', 'RHAM', 'HJKL', 'XMCF', 'RGG', 'DNFG', 'RKSM', 'GOOD', 'HAMM', 'AMAB')
	--then '301'
	--else coalesce(
	--		 SpecialtyMerge.XrefEntityCode
	--		,EncounterFact.NationalSpecialtyCode
	--	)
	--end

	,EncounterFact.AdmissionMethodCode
	,EncounterFact.PracticeCode

	--,PCTCode =
	--case
	--when Encounter.ContractSerialNo = 'DOH000'
	--then '5J5'
	--else EncounterFact.PCTCode
	--end

	,EncounterFact.AgeCode
	,EncounterFact.Cases
	,EncounterFact.LengthOfEncounter
	,EncounterFact.SiteCode

	,DemandCategoryCode =
		case EncounterFact.MetricCode
		when 'IEFFCE' then '100'
		when 'DCFFCE' then '200'
		when 'RDFFCE' then '200'
		when 'RNFFCE' then '200'
		else '300'
		end

	,Breached =
		convert(
			bit
			,case
			when Encounter.BreachDate <= Encounter.AdmissionDate
			then 1
			else 0
			end
		)

FROM
	dbo.FactAPC EncounterFact

inner join dbo.BaseAPC Encounter
on	Encounter.EncounterRecno = EncounterFact.EncounterRecno

--inner join dbo.OlapPASSpecialty Specialty
--on	Specialty.SpecialtyCode = EncounterFact.SpecialtyCode

--left join WH.dbo.EntityXref SpecialtyMerge
--on	SpecialtyMerge.EntityCode = Specialty.NationalSpecialtyCode
--and	SpecialtyMerge.EntityTypeCode = 'DEMANDSPECIALTYMERGE'
--and	SpecialtyMerge.XrefEntityTypeCode = 'DEMANDSPECIALTYMERGE'

where
	EncounterFact.MetricCode in 
		(
		 'IEFFCE'
		,'DCFFCE'
		--,'RDFFCE'
		--,'RNFFCE'
		,'EMFFCE'
		,'NEFFCE'
		)

----exclude well babies
--and not
--	(
--		left(Encounter.PrimaryDiagnosisCode, 3) = 'Z38'
--	and	Encounter.NeonatalLevelOfCare = '0'
--	)

----exclude paying patients
--and	Encounter.ContractSerialNo <> 'PAY000'

----exclude HIV
--and	left(Encounter.PrimaryDiagnosisCode, 3) not between 'B20' and 'B25'

----exclude regular day/night
--and	Encounter.ManagementIntentionCode not in ('R', 'N')

----exclude iCATS
--and	EncounterFact.SiteCode != 'FMSK'

----excluded specialties
--and	not exists
--	(
--	select
--		1
--	from
--		WH.dbo.EntityLookup ExcludedSpecialty
--	where
--		ExcludedSpecialty.EntityCode = Specialty.NationalSpecialtyCode
--	and	ExcludedSpecialty.EntityTypeCode = 'DEMANDEXCLUDEDSPECIALTY'
--	)

