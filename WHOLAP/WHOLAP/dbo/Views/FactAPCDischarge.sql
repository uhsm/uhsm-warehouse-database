﻿CREATE view FactAPCDischarge as

SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.RegisteredGpPracticeCode
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,StartWardCode = Encounter.StartWardTypeCode
	,ActualEndWardCode = Encounter.EndWardTypeCode

	,EndWardCode =
		case
--if the last ward was F2 and was occupied <24 hours
--or the last ward was the Discharge Lounge then use the previous ward
		when 
			Encounter.EndWardTypeCode = 10009072	--DISCHARGE UNIT
		or	(
				Encounter.EndWardTypeCode = 10009080 --F2
			and	(
				select
					DATEDIFF(
						HOUR
						,WardStay.StartTime
						,WardStay.EndTime
					)
				from
					dbo.BaseAPCWardStay WardStay
				where
					WardStay.SourceSpellNo = Encounter.SourceSpellNo
				and	not exists
					(
					select
						1
					from
						dbo.BaseAPCWardStay Previous
					where
						Previous.SourceSpellNo = Encounter.SourceSpellNo
					and	(
							Previous.StartTime > WardStay.StartTime
						or	(
								Previous.StartTime = WardStay.StartTime
							and	Previous.SourceUniqueID > WardStay.SourceUniqueID
							)
						)
					)
				)
			< 24
			)
		then
			(
			select
				WardStay.WardCode
			from
				dbo.BaseAPCWardStay WardStay
			where
				WardStay.SourceSpellNo = Encounter.SourceSpellNo
			and	WardStay.SequenceNo =
				(
				select
					case
					when MAX(Previous.SequenceNo) = 1
					then MAX(Previous.SequenceNo)
					else MAX(Previous.SequenceNo) - 1
					end
				from
					dbo.BaseAPCWardStay Previous
				where
					Previous.SourceSpellNo = Encounter.SourceSpellNo
				)
			)

		else Encounter.EndWardTypeCode
		end

	,DischargeHourOfDay =
		CAST(
			DATEPART(
				 hour
				,Encounter.DischargeTime
			)
			as tinyint
		)
from
	dbo.OlapAPC Encounter
where
	LastEpisodeInSpellIndicator = 1
and	Encounter.DischargeDate is not null

