﻿
CREATE view [dbo].[FactAPCWaitingListDemand] as

select
	 EncounterFact.EncounterRecno
	,EncounterFact.AgeCode
	,EncounterFact.ConsultantCode

	,SpecialtyCode =
		EncounterFact.SpecialtyCode
	--case
	--when EncounterFact.ConsultantCode in ('RAM', 'CON', 'MJG', 'RCP', 'NHB', 'DNF', 'RG', 'LJQ', 'RKS', 'RHAM', 'HJKL', 'XMCF', 'RGG', 'DNFG', 'RKSM', 'GOOD', 'HAMM', 'AMAB')
	--then '301'
	--else coalesce(
	--		 SpecialtyMerge.XrefEntityCode
	--		,Specialty.NationalSpecialtyCode
	--	)
	--end

	,EncounterFact.DurationCode
	,EncounterFact.PracticeCode
	,EncounterFact.SiteCode
	,EncounterFact.WaitTypeCode
	,EncounterFact.StatusCode
	,EncounterFact.CategoryCode

	,DemandCategoryCode =
		case EncounterFact.CategoryCode
		when '10' then '100'
		when '20' then '200'
		else '300'
		end

	,EncounterFact.Cases

	,EncounterFact.CensusDate

	,Breached =
		convert(
			bit
			,case
			when BaseAPCWaitingList.BreachDate <= EncounterFact.CensusDate
			then 1
			else 0
			end
		)

from
	dbo.FactAPCWaitingList EncounterFact

inner join dbo.BaseAPCWaitingList
on	BaseAPCWaitingList.EncounterRecno = EncounterFact.EncounterRecno

--inner join dbo.OlapPASSpecialty Specialty
--on	Specialty.SpecialtyCode = EncounterFact.SpecialtyCode

--left join WH.dbo.EntityXref SpecialtyMerge
--on	SpecialtyMerge.EntityCode = Specialty.NationalSpecialtyCode
--and	SpecialtyMerge.EntityTypeCode = 'DEMANDSPECIALTYMERGE'
--and	SpecialtyMerge.XrefEntityTypeCode = 'DEMANDSPECIALTYMERGE'

inner join dbo.OlapCensus Census
on	Census.CensusDate = EncounterFact.CensusDate
and	Census.LastInMonth = 1

--where
----excluded specialties
--	not exists
--	(
--	select
--		1
--	from
--		WH.dbo.EntityLookup ExcludedSpecialty
--	where
--		ExcludedSpecialty.EntityCode = Specialty.NationalSpecialtyCode
--	and	ExcludedSpecialty.EntityTypeCode = 'DEMANDEXCLUDEDSPECIALTY'
--	)

