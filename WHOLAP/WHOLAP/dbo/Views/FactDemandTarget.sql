﻿
CREATE view [dbo].[FactDemandTarget] as

select
	 TargetTypeCode
	,SpecialtyCode
	,PCTCode
	,Period
	,DemandCategoryCode
	,Activity
from
	WH.WH.DemandTarget

