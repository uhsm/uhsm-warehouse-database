﻿
CREATE view [dbo].[FactOPDemand] as

select
	 EncounterFact.EncounterRecno
	,EncounterFact.EncounterDate
	,EncounterFact.ConsultantCode

	,SpecialtyCode =
		EncounterFact.SpecialtyCode
	--case
	--when Encounter.ConsultantCode in ('RAM', 'CON', 'MJG', 'RCP', 'NHB', 'DNF', 'RG', 'LJQ', 'RKS', 'RHAM', 'HJKL', 'XMCF', 'RGG', 'DNFG', 'RKSM', 'GOOD', 'HAMM', 'AMAB')
	--then '301'
	--else coalesce(
	--		 SpecialtyMerge.XrefEntityCode
	--		,Specialty.NationalSpecialtyCode
	--	)
	--end

	,EncounterFact.PracticeCode

	--,PCTCode =
	--case
	--when Encounter.ContractSerialNo = 'DOH000'
	--then '5J5'
	--else EncounterFact.PCTCode
	--end

	,EncounterFact.AgeCode
	,EncounterFact.Cases
	,EncounterFact.LengthOfWait
	,EncounterFact.SiteCode

	,DemandCategoryCode = '400'
from
	dbo.FactOP EncounterFact

--inner join dbo.BaseOP Encounter
--on	Encounter.EncounterRecno = EncounterFact.EncounterRecno

--inner join dbo.OlapPASSpecialty Specialty
--on	Specialty.SpecialtyCode = EncounterFact.SpecialtyCode

--left join WH.dbo.EntityXref SpecialtyMerge
--on	SpecialtyMerge.EntityCode = Specialty.NationalSpecialtyCode
--and	SpecialtyMerge.EntityTypeCode = 'DEMANDSPECIALTYMERGE'
--and	SpecialtyMerge.XrefEntityTypeCode = 'DEMANDSPECIALTYMERGE'

where
	EncounterFact.MetricCode = 'FATT'

----exclude iCATS
--and	EncounterFact.SiteCode != 'FMSK'

----excluded specialties
--and	not exists
--	(
--	select
--		1
--	from
--		WH.dbo.EntityLookup ExcludedSpecialty
--	where
--		ExcludedSpecialty.EntityCode = Specialty.NationalSpecialtyCode
--	and	ExcludedSpecialty.EntityTypeCode = 'DEMANDEXCLUDEDSPECIALTY'
--	)

--exclude ward attenders
and	EncounterFact.IsWardAttender = 0

