﻿
CREATE view [dbo].[FactOPWaitingListDemand] as

select
	 EncounterFact.EncounterRecno
	,EncounterFact.AgeCode
	,EncounterFact.ConsultantCode

	,SpecialtyCode =
		EncounterFact.SpecialtyCode
	--case
	--when EncounterFact.ConsultantCode in ('RAM', 'CON', 'MJG', 'RCP', 'NHB', 'DNF', 'RG', 'LJQ', 'RKS', 'RHAM', 'HJKL', 'XMCF', 'RGG', 'DNFG', 'RKSM', 'GOOD', 'HAMM', 'AMAB')
	--then '301'
	--else coalesce(
	--		 SpecialtyMerge.XrefEntityCode
	--		,Specialty.NationalSpecialtyCode
	--	)
	--end

	,EncounterFact.DurationCode
	,EncounterFact.PracticeCode
	,EncounterFact.SiteCode
	,EncounterFact.SourceOfReferralCode
	,EncounterFact.Cases
	,DemandCategoryCode = '400'
	,EncounterFact.CensusDate

	,Breached =
		convert(
			bit
			,case
			when BaseOPWaitingList.BreachDate <= EncounterFact.CensusDate
			then 1
			else 0
			end
		)

from
	dbo.FactOPWaitingList EncounterFact

inner join dbo.BaseOPWaitingList
on	BaseOPWaitingList.EncounterRecno = EncounterFact.EncounterRecno

--inner join dbo.OlapPASSpecialty Specialty
--on	Specialty.SpecialtyCode = EncounterFact.SpecialtyCode

--left join WH.dbo.EntityXref SpecialtyMerge
--on	SpecialtyMerge.EntityCode = Specialty.NationalSpecialtyCode
--and	SpecialtyMerge.EntityTypeCode = 'DEMANDSPECIALTYMERGE'
--and	SpecialtyMerge.XrefEntityTypeCode = 'DEMANDSPECIALTYMERGE'

inner join dbo.OlapCensus Census
on	Census.CensusDate = EncounterFact.CensusDate
and	Census.LastInMonth = 1

--where
----exclude iCATS
--	EncounterFact.SiteCode != 'FMSK'

----excluded specialties
--and	not exists
--	(
--	select
--		1
--	from
--		WH.dbo.EntityLookup ExcludedSpecialty
--	where
--		ExcludedSpecialty.EntityCode = Specialty.NationalSpecialtyCode
--	and	ExcludedSpecialty.EntityTypeCode = 'DEMANDEXCLUDEDSPECIALTY'
--	)

