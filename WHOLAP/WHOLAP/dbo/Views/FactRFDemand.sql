﻿
CREATE view [dbo].[FactRFDemand] as

select
	 EncounterFact.EncounterRecno
	,EncounterFact.EncounterDate
	,EncounterFact.ConsultantCode

	,SpecialtyCode =
		EncounterFact.SpecialtyCode
	--case
	--when EncounterFact.ConsultantCode in ('RAM', 'CON', 'MJG', 'RCP', 'NHB', 'DNF', 'RG', 'LJQ', 'RKS', 'RHAM', 'HJKL', 'XMCF', 'RGG', 'DNFG', 'RKSM', 'GOOD', 'HAMM', 'AMAB')
	--then '301'
	--else coalesce(
	--		 SpecialtyMerge.XrefEntityCode
	--		,Specialty.NationalSpecialtyCode
	--	)
	--end

	,EncounterFact.PracticeCode
	,EncounterFact.AgeCode
	,EncounterFact.SiteCode

	,EncounterFact.SourceOfReferralCode
	,DemandCategoryCode = '500'
	,EncounterFact.Cases
from
	dbo.FactRF EncounterFact

--inner join dbo.BaseRF Encounter
--on	Encounter.EncounterRecno = EncounterFact.EncounterRecno

--inner join dbo.OlapPASSpecialty Specialty
--on	Specialty.SpecialtyCode = EncounterFact.SpecialtyCode

--left join WH.dbo.EntityXref SpecialtyMerge
--on	SpecialtyMerge.EntityCode = Specialty.NationalSpecialtyCode
--and	SpecialtyMerge.EntityTypeCode = 'DEMANDSPECIALTYMERGE'
--and	SpecialtyMerge.XrefEntityTypeCode = 'DEMANDSPECIALTYMERGE'

--where
----exclude iCATS
--	EncounterFact.SiteCode != 'FMSK'

----excluded specialties
--and	not exists
--	(
--	select
--		1
--	from
--		WH.dbo.EntityLookup ExcludedSpecialty
--	where
--		ExcludedSpecialty.EntityCode = Specialty.NationalSpecialtyCode
--	and	ExcludedSpecialty.EntityTypeCode = 'DEMANDEXCLUDEDSPECIALTY'
--	)

