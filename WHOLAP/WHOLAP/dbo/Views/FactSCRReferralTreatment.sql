﻿



CREATE view [dbo].[FactSCRReferralTreatment] as


select
	 PTLCode

	,EncounterRecno
	,CensusDate
	,GenderCode
	,PracticeCode = coalesce(PracticeCode, 'N/A')
	,PCTCode = coalesce(PCTCode, 'N/A')
	,AgeAtDiagnosisCode = coalesce(AgeAtDiagnosis, -1)
	,PriorityTypeCode = coalesce(PriorityTypeCode, 'N/A')
	,CancerTypeCode = coalesce(CancerTypeCode, 'N/A')
	,CancerStatusCode = coalesce(CancerStatusCode, 'N/A')
	,SourceForOutpatientCode = coalesce(SourceForOutpatientCode, 'N/A')
	,InitialTreatmentCode = coalesce(InitialTreatmentCode, 'N/A')
	,TreatmentSettingCode = coalesce(TreatmentSettingCode, 'N/A')
	,ConsultantCode = coalesce(ConsultantCode, 'N/A')
	,SpecialtyCode = coalesce(SpecialtyCode, 999)
	,PrimaryDiagnosisCode = coalesce(PrimaryDiagnosisCode, 'N/A')
	,IntendedProcedureCode = coalesce(IntendedProcedureCode, 'N/A')
	,TumourStatusCode
	,FirstAppointmentConsultantCode = coalesce(FirstAppointmentConsultantCode, 'N/A')
	,CancerSite
	,PathwayTypeCode
	,DTTAdjustmentDays
	,SevenDayWait
	,BreachDate62Days
	,CurrentWait62Days
	,BreachDate31Days
	,CurrentWait31Days
	,AgeAtDiagnosis
	,ReferringOrganisationCode = coalesce(ReferringOrganisationCode, 'N/A')
	,TreatmentOrganisationCode = coalesce(TreatmentOrganisationCode, 'N/A')
	,SourceOfReferralCode = coalesce(SourceOfReferralCode, 'N/A')
from
(
select
	 PTLCode = 'Full'

	,EncounterRecno
	,CensusDate
	,GenderCode
	,PracticeCode
	,PCTCode
	,AgeAtDiagnosis
	,PriorityTypeCode
	,CancerTypeCode
	,CancerStatusCode
	,SourceForOutpatientCode
	,InitialTreatmentCode
	,TreatmentSettingCode
	,ConsultantCode
	,SpecialtyCode
	,PrimaryDiagnosisCode
	,IntendedProcedureCode
	,TumourStatusCode
	,FirstAppointmentConsultantCode
	,CancerSite
	,PathwayTypeCode
	,DTTAdjustmentDays
	,SevenDayWait
	,BreachDate62Days
	,CurrentWait62Days
	,BreachDate31Days
	,CurrentWait31Days
	,ReferringOrganisationCode
	,TreatmentOrganisationCode
	,SourceOfReferralCode
from
	dbo.BaseSCRReferralTreatment

--PDO 26 Jun 2012 - As requested by Karen Blackburn
where
	(
		DTT = 1
	or	RTT = 1
	or	Upgrade = 1
	or	Subsequent = 1
	or	Screening = 1
	)

union all

select
	 PTLCode = '62 Day'

	,EncounterRecno
	,CensusDate
	,GenderCode
	,PracticeCode
	,PCTCode
	,AgeAtDiagnosis
	,PriorityTypeCode
	,CancerTypeCode
	,CancerStatusCode
	,SourceForOutpatientCode
	,InitialTreatmentCode
	,TreatmentSettingCode
	,ConsultantCode
	,SpecialtyCode
	,PrimaryDiagnosisCode
	,IntendedProcedureCode
	,TumourStatusCode
	,FirstAppointmentConsultantCode
	,CancerSite
	,PathwayTypeCode
	,DTTAdjustmentDays
	,SevenDayWait
	,BreachDate62Days
	,CurrentWait62Days
	,BreachDate31Days
	,CurrentWait31Days
	,ReferringOrganisationCode
	,TreatmentOrganisationCode
	,SourceOfReferralCode
from
	dbo.BaseSCRReferralTreatment
where
	RTT = 1

union all

select
	 PTLCode = 'Screening'

	,EncounterRecno
	,CensusDate
	,GenderCode
	,PracticeCode
	,PCTCode
	,AgeAtDiagnosis
	,PriorityTypeCode
	,CancerTypeCode
	,CancerStatusCode
	,SourceForOutpatientCode
	,InitialTreatmentCode
	,TreatmentSettingCode
	,ConsultantCode
	,SpecialtyCode
	,PrimaryDiagnosisCode
	,IntendedProcedureCode
	,TumourStatusCode
	,FirstAppointmentConsultantCode
	,CancerSite
	,PathwayTypeCode
	,DTTAdjustmentDays
	,SevenDayWait
	,BreachDate62Days
	,CurrentWait62Days
	,BreachDate31Days
	,CurrentWait31Days
	,ReferringOrganisationCode
	,TreatmentOrganisationCode
	,SourceOfReferralCode
from
	dbo.BaseSCRReferralTreatment
where
	Screening = 1

union all

select
	 PTLCode = 'Upgrade'

	,EncounterRecno
	,CensusDate
	,GenderCode
	,PracticeCode
	,PCTCode
	,AgeAtDiagnosis
	,PriorityTypeCode
	,CancerTypeCode
	,CancerStatusCode
	,SourceForOutpatientCode
	,InitialTreatmentCode
	,TreatmentSettingCode
	,ConsultantCode
	,SpecialtyCode
	,PrimaryDiagnosisCode
	,IntendedProcedureCode
	,TumourStatusCode
	,FirstAppointmentConsultantCode
	,CancerSite
	,PathwayTypeCode
	,DTTAdjustmentDays
	,SevenDayWait
	,BreachDate62Days
	,CurrentWait62Days
	,BreachDate31Days
	,CurrentWait31Days
	,ReferringOrganisationCode
	,TreatmentOrganisationCode
	,SourceOfReferralCode
from
	dbo.BaseSCRReferralTreatment
where
	Upgrade = 1


--this is the equivalent of the Full PTL
union all

select
	 PTLCode = '31 Day'

	,EncounterRecno
	,CensusDate
	,GenderCode
	,PracticeCode
	,PCTCode
	,AgeAtDiagnosis
	,PriorityTypeCode
	,CancerTypeCode
	,CancerStatusCode
	,SourceForOutpatientCode
	,InitialTreatmentCode
	,TreatmentSettingCode
	,ConsultantCode
	,SpecialtyCode
	,PrimaryDiagnosisCode
	,IntendedProcedureCode
	,TumourStatusCode
	,FirstAppointmentConsultantCode
	,CancerSite
	,PathwayTypeCode
	,DTTAdjustmentDays
	,SevenDayWait
	,BreachDate62Days
	,CurrentWait62Days
	,BreachDate31Days
	,CurrentWait31Days
	,ReferringOrganisationCode
	,TreatmentOrganisationCode
	,SourceOfReferralCode
from
	dbo.BaseSCRReferralTreatment
where
	DTT = 1

union all

select
	 PTLCode = 'Subsequent'

	,EncounterRecno
	,CensusDate
	,GenderCode
	,PracticeCode
	,PCTCode
	,AgeAtDiagnosis
	,PriorityTypeCode
	,CancerTypeCode
	,CancerStatusCode
	,SourceForOutpatientCode
	,InitialTreatmentCode
	,TreatmentSettingCode
	,ConsultantCode
	,SpecialtyCode
	,PrimaryDiagnosisCode
	,IntendedProcedureCode
	,TumourStatusCode
	,FirstAppointmentConsultantCode
	,CancerSite
	,PathwayTypeCode
	,DTTAdjustmentDays
	,SevenDayWait
	,BreachDate62Days
	,CurrentWait62Days
	,BreachDate31Days
	,CurrentWait31Days
	,ReferringOrganisationCode
	,TreatmentOrganisationCode
	,SourceOfReferralCode
from
	dbo.BaseSCRReferralTreatment
where
	Subsequent = 1
) Activity
where CensusDate > DATEADD(year, -1, getdate())
and CancerStatusCode <> '69' --added by Jubeda on 08/06/2015. removing patients that have the cancerstatuscode 'Other'








