﻿CREATE view FactTheatreActivity as

--planned (template) sessions

select
	 Session.SourceUniqueID

	,TheatreCode = Session.TheatreCode

	,SessionDate = dateadd(day, datediff(day, 0, Session.StartTime), 0)

	,SpecialtyCode = coalesce(Session.TemplateSpecialtyCode, 0)

	,PlannedSession = 1

	,ActualSession = null

	,ReallocatedSession =
		case	
		when 
			coalesce(
				 Session.TemplateSpecialtyCode
				,Session.SpecialtyCode
				,'##'
			) = 
			coalesce(
				 Session.SpecialtyCode
				,'##'
			)
		then 0 --Same Specialty

		else 1 --Different Specialty
		end

	,UnusedSession =
		case
		when Session.CancelledFlag = 1
		then 1
		else 0
		end

	,AdditionalSession = null

	,CancelledOperationOnDay = null

	,CancelledOperation = null

	,Operation = null

	,PlannedMinutes =
		datediff(minute, Session.StartTime, Session.EndTime)

	,PlannedMinutesOfActualSession = null

	,SessionUtilisationMinutes = null

	,ProductiveTime = null

	,LateStart = null

	,LateFinish = null

from
	dbo.BaseTheatreSession Session

union all

--Actual (theatre) sessions
select
	 Session.SourceUniqueID

	,TheatreCode = Session.TheatreCode

	,SessionDate = dateadd(day, datediff(day, 0, Session.StartTime), 0)

	,SpecialtyCode = coalesce(Session.SpecialtyCode, 0)

	,PlannedSession = null

	,ActualSession = 1

	,ReallocatedSession = null

	,UnusedSession = null

	,AdditionalSession = 
		case	
		when 
			coalesce(
				 Session.TemplateSpecialtyCode
				,Session.SpecialtyCode
				,'##'
			) = 
			coalesce(
				 Session.SpecialtyCode
				,'##'
			)
		then 0 --Same Specialty

		else 1 --Different Specialty
		end

	,CancelledOperationOnDay =
		TheatreOperation.CancelledOperationOnDay

	,CancelledOperation =
		TheatreOperation.CancelledOperation

	,Operation =
		TheatreOperation.Operation

	,PlannedMinutes = null

	,PlannedMinutesOfActualSession =
		datediff(minute, Session.StartTime, Session.EndTime)

	,SessionUtilisationMinutes =
		datediff(
			 minute
			,Session.ActualSessionStartTime
			,Session.ActualSessionEndTime
		)

	,ProductiveTime =
		TheatreOperation.ProductiveTime

	,LateStart =
		case
		when datediff(minute, Session.ActualSessionStartTime, Session.StartTime) > 15
		then 1
		else 0
		end

	,LateFinish =
		case
		when datediff(minute, Session.EndTime, Session.ActualSessionEndTime) > 15
		then 1
		else 0
		end

from
	dbo.BaseTheatreSession Session

left join 
	(
	select
		 TheatreOperation.SessionSourceUniqueID

		,ProductiveTime =
			SUM(TheatreOperation.OperationProductiveTime)

		,CancelledOperationOnDay =
			SUM(TheatreOperation.CancelledOperationOnDay)

		,CancelledOperation =
			SUM(TheatreOperation.CancelledOperation)

		,Operation = COUNT(*)
	from
		dbo.FactTheatreOperation TheatreOperation
	group by
		 TheatreOperation.SessionSourceUniqueID
	) TheatreOperation
on	TheatreOperation.SessionSourceUniqueID = Session.SourceUniqueID

where
	Session.CancelledFlag = 0
