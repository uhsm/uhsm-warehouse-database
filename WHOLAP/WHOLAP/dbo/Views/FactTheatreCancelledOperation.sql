﻿CREATE View FactTheatreCancelledOperation as



Select
	CancOp.SourceUniqueID
	,CancelDate =Cast(Convert(varchar(8), CancOp.CancellationDate,112) as SmallDatetime)
	,CancOp.CancelReasonCode
	,CancOp.SpecialtyCode
	,CancOp.SurgeonCode
	,cancOp.TheatreCode
	,CancOp.CancellationSurgeonCode
	,ProposedOperationDate = Cast(Convert(Varchar(8), IsNull(CancOp.ProposedOperationDate,'19000101'), 112) as smalldatetime)
	,LastMinuteIndicator = 
		Case When CancOp.LastMinute = 'Y' Then
			Convert(Bit,1)
		Else
			Convert(Bit,0)
		End
	,Cases = 1
from BaseTheatreCancelledOperation CancOp