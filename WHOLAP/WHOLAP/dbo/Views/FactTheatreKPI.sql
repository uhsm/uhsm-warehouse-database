﻿
/*
Modified Date			Modified By			Modification
-------------			------------		-----------------------------------
29/01/2014				CMan				CBH - had received details regards to canellations not being picked up.
											Amendment to Cancelled Operation and Cancelled Operation on Day calculations - details as per comments in script below






*/





CREATE VIEW [dbo].[FactTheatreKPI]
AS
/*planned (template) sessions*/ SELECT Session.SourceUniqueID, TheatreCode = Session.TheatreCode, SessionDate = dateadd(day, datediff(day, 0, 
                      Session.StartTime), 0), SpecialtyCode = COALESCE (Session.TemplateSpecialtyCode, 0), KPI = 'Planned Session', 
                      ---nscatter - Amended Session Count on request of Andrea Myerscough 04/03/2013
                      case	when Session.PlannedSessionMinutes >= 600 then 3
							when Session.PlannedSessionMinutes > 360  then 2
							else 1
							end as Actual
FROM         dbo.BaseTheatreSession Session
UNION ALL
SELECT     Session.SourceUniqueID, TheatreCode = Session.TheatreCode, SessionDate = dateadd(day, datediff(day, 0, Session.StartTime), 0), 
                      SpecialtyCode = COALESCE (Session.TemplateSpecialtyCode, 0), KPI = 'Reallocated Session', 
                      ---nscatter - Amended Session Count on request of Andrea Myerscough 04/03/2013
                      case	when Session.PlannedSessionMinutes >= 600 then 3
							when Session.PlannedSessionMinutes > 360  then 2
							else 1
							end as Actual
FROM         dbo.BaseTheatreSession Session
WHERE     COALESCE (Session.TemplateSpecialtyCode, Session.SpecialtyCode, '##') != COALESCE (Session.SpecialtyCode, '##')
UNION ALL
SELECT     Session.SourceUniqueID, TheatreCode = Session.TheatreCode, SessionDate = dateadd(day, datediff(day, 0, Session.StartTime), 0), 
                      SpecialtyCode = COALESCE (Session.TemplateSpecialtyCode, 0), KPI = 'Unused Session', 
                      ---nscatter - Amended Session Count on request of Andrea Myerscough 04/03/2013
                      case	when Session.PlannedSessionMinutes >= 600 then 3
							when Session.PlannedSessionMinutes > 360  then 2
							else 1
							end as Actual
FROM         dbo.BaseTheatreSession Session
WHERE     Session.CancelledFlag = 1
UNION ALL
SELECT     Session.SourceUniqueID, TheatreCode = Session.TheatreCode, SessionDate = dateadd(day, datediff(day, 0, Session.StartTime), 0), 
                      SpecialtyCode = COALESCE (Session.TemplateSpecialtyCode, 0), KPI = 'Planned Minutes', Actual = datediff(minute, Session.StartTime, 
                      Session.EndTime)
FROM         dbo.BaseTheatreSession Session
UNION ALL
/*Actual (theatre) sessions*/ SELECT Session.SourceUniqueID, TheatreCode = Session.TheatreCode, SessionDate = dateadd(day, datediff(day, 0, 
                      Session.StartTime), 0), SpecialtyCode = COALESCE (Session.SpecialtyCode, 0), KPI = 'Actual Session', 
                      ---nscatter - Amended Session Count on request of Andrea Myerscough 04/03/2013
                      case	when Session.PlannedSessionMinutes >= 600 then 3
							when Session.PlannedSessionMinutes > 360  then 2
							else 1
							end as Actual
FROM         dbo.BaseTheatreSession Session
WHERE     Session.CancelledFlag = 0
UNION ALL
SELECT     Session.SourceUniqueID, TheatreCode = Session.TheatreCode, SessionDate = dateadd(day, datediff(day, 0, Session.StartTime), 0), 
                      SpecialtyCode = COALESCE (Session.SpecialtyCode, 0), KPI = 'Additional Session', 
                      ---nscatter - Amended Session Count on request of Andrea Myerscough 04/03/2013
                      case	when Session.PlannedSessionMinutes >= 600 then 3
							when Session.PlannedSessionMinutes > 360  then 2
							else 1
							end as Actual
FROM         dbo.BaseTheatreSession Session
WHERE     Session.CancelledFlag = 0 AND COALESCE (Session.TemplateSpecialtyCode, Session.SpecialtyCode, '##') != COALESCE (Session.SpecialtyCode, '##')
UNION ALL
SELECT     Session.SourceUniqueID, TheatreCode = Session.TheatreCode, SessionDate = dateadd(day, datediff(day, 0, Session.StartTime), 0), 
                      SpecialtyCode = COALESCE (Session.SpecialtyCode, 0), KPI = 'Planned Minutes Of Actual Session', Actual = datediff(minute, Session.StartTime, 
                      Session.EndTime)
FROM         dbo.BaseTheatreSession Session
WHERE     Session.CancelledFlag = 0
UNION ALL
SELECT     Session.SourceUniqueID, TheatreCode = Session.TheatreCode, SessionDate = dateadd(day, datediff(day, 0, Session.StartTime), 0), 
                      SpecialtyCode = COALESCE (Session.SpecialtyCode, 0), KPI = 'Session Utilisation Minutes', Actual = datediff(minute, Session.ActualSessionStartTime, 
                      Session.ActualSessionEndTime)
FROM         dbo.BaseTheatreSession Session
WHERE     Session.CancelledFlag = 0

/*--Scripted 18/12/2013 as per CBH and PN discussions
 --New Session Utilisation logic to include recovery time of the last op onto the utilisation calcultion but capped at 10 if recovery time is greater than 10 for non 110 spec ops and 20 if recovery time is greater than 20 for 110 spec ops
--Uncomment and replace the above logic for'Session Utilisation Minutes' with the section below once confirmed in meeting that that is what is required
 SELECT Session.SourceUniqueID, TheatreCode = Session.TheatreCode,SessionDate = Dateadd(day, Datediff(day, 0, Session.StartTime), 0),
       SpecialtyCode = COALESCE (Session.SpecialtyCode, 0),KPI = 'Session Utilisation Minutes',
       Actual = Datediff(minute, Session.ActualSessionStartTime, Session.ActualSessionEndTime)+
        Case when TSp.NationalSpecialtyCode = '110' and (RecoveryTime > '20' or RecoveryTime IS NULL)  then '20'
			when  TSp.NationalSpecialtyCode <> '110' and (RecoveryTime > '10'or RecoveryTime IS NULL) then '10'
			Else RecoveryTime End 
 FROM   WHOLAP.dbo.BaseTheatreSession Session
       LEFT OUTER JOIN (SELECT TheatreSession.SessionSourceUniqueID,RecoveryTime 
						FROM   WHOLAP.dbo.FactTheatreOperation TheatreSession
                               INNER JOIN (SELECT SessionSourceUniqueID, Max(operationorder) MAxOpOrder
                                           FROM   WHOLAP.dbo.FactTheatreOperation
                                          GROUP  BY SessionSourceUniqueID
                                          ) MaxOp
                                       ON TheatreSession.SessionSourceUniqueID = MaxOp.SessionSourceUniqueID
                                          AND TheatreSession.OperationOrder = MaxOp.MaxOpOrder) OpDetails
                    ON Session.SourceUniqueID = OpDetails.SessionSourceUniqueID
        Left outer join WHOLAP.dbo.OlapTheatreSpecialty TSp on Session.SpecialtyCode = TSp.SpecialtyCode
WHERE  Session.CancelledFlag = 0
*/
UNION ALL
SELECT     Session.SourceUniqueID, TheatreCode = Session.TheatreCode, SessionDate = dateadd(day, datediff(day, 0, Session.StartTime), 0), 
                      SpecialtyCode = COALESCE (Session.SpecialtyCode, 0), KPI = 'Capped Session Utilisation', Actual = CASE WHEN datediff(minute, 
                      Session.ActualSessionStartTime, Session.ActualSessionEndTime) > datediff(minute, Session.StartTime, Session.EndTime) THEN datediff(minute, 
                      Session.StartTime, Session.EndTime) ELSE datediff(minute, Session.ActualSessionStartTime, Session.ActualSessionEndTime) END
FROM         dbo.BaseTheatreSession Session
WHERE     Session.CancelledFlag = 0
/*
--SCripted 18/12/2013 as per CBH and PN discussions
--  Capped Session Utilisation logic to include recovery time of last op in the session
--Uncomment this section and replace the 'Capped Session Utilisation' above with the one below once confirmed with CBH and Theatre leads that it is ok.
SELECT     Session.SourceUniqueID, TheatreCode = Session.TheatreCode, SessionDate = dateadd(day, datediff(day, 0, Session.StartTime), 0), 
                      SpecialtyCode = COALESCE (Session.SpecialtyCode, 0), KPI = 'Capped Session Utilisation', Actual = CASE WHEN datediff(minute, 
                      Session.ActualSessionStartTime, Session.ActualSessionEndTime) > datediff(minute, Session.StartTime, Session.EndTime) THEN datediff(minute, 
                      Session.StartTime, Session.EndTime) +  
						       Case when TSp.NationalSpecialtyCode = '110' and (RecoveryTime > '20' or RecoveryTime IS NULL)  then '20'
								when  TSp.NationalSpecialtyCode <> '110' and (RecoveryTime > '10'or RecoveryTime IS NULL) then '10'
								Else RecoveryTime End
					ELSE datediff(minute, Session.ActualSessionStartTime, Session.ActualSessionEndTime) +
						        Case when TSp.NationalSpecialtyCode = '110' and (RecoveryTime > '20' or RecoveryTime IS NULL)  then '20'
								when  TSp.NationalSpecialtyCode <> '110' and (RecoveryTime > '10'or RecoveryTime IS NULL) then '10'
								Else RecoveryTime End
					END
FROM         dbo.BaseTheatreSession Session
LEFT OUTER JOIN (SELECT TheatreSession.SessionSourceUniqueID,RecoveryTime 
					FROM   WHOLAP.dbo.FactTheatreOperation TheatreSession
                           INNER JOIN (SELECT SessionSourceUniqueID, Max(operationorder) MAxOpOrder--,*-- DATEDIFF(minutes,InRecoveryTime,--RecoveryTime = SUM(Datediff(minutes,TheatreOperation.OperationProductiveTime)
                                       FROM   WHOLAP.dbo.FactTheatreOperation
                                       GROUP  BY SessionSourceUniqueID
                                      ) MaxOp
                                   ON TheatreSession.SessionSourceUniqueID = MaxOp.SessionSourceUniqueID
                                      AND TheatreSession.OperationOrder = MaxOp.MaxOpOrder) OpDetails
                ON Session.SourceUniqueID = OpDetails.SessionSourceUniqueID
    Left outer join WHOLAP.dbo.OlapTheatreSpecialty TSp on Session.SpecialtyCode = TSp.SpecialtyCode
WHERE     Session.CancelledFlag = 0
*/
UNION ALL
SELECT     Session.SourceUniqueID, TheatreCode = Session.TheatreCode, SessionDate = dateadd(day, datediff(day, 0, Session.StartTime), 0), 
                      SpecialtyCode = COALESCE (Session.SpecialtyCode, 0), KPI = 'Late Start', Actual = 1
FROM         dbo.BaseTheatreSession Session
WHERE     Session.CancelledFlag = 0 AND datediff(minute,Session.StartTime, Session.ActualSessionStartTime) > 15
UNION ALL
SELECT     Session.SourceUniqueID, TheatreCode = Session.TheatreCode, SessionDate = dateadd(day, datediff(day, 0, Session.StartTime), 0), 
                      SpecialtyCode = COALESCE (Session.SpecialtyCode, 0), KPI = 'Late Finish', Actual = 1
FROM         dbo.BaseTheatreSession Session
WHERE     Session.CancelledFlag = 0 AND datediff(minute, Session.EndTime, Session.ActualSessionEndTime) > 15
UNION ALL
SELECT     Session.SourceUniqueID, TheatreCode = Session.TheatreCode, SessionDate = dateadd(day, datediff(day, 0, Session.StartTime), 0), 
                      SpecialtyCode = COALESCE (Session.SpecialtyCode, 0), KPI = 'Early Finish', Actual = 1
FROM         dbo.BaseTheatreSession Session
WHERE     Session.CancelledFlag = 0 AND datediff(minute, Session.ActualSessionEndTime, Session.EndTime) > 15
UNION ALL
--29/01/2014 CM: Added this new method of calculating cancelled ops on day
SELECT Session.SourceUniqueID, Session.TheatreCode, dateadd(day, datediff(day, 0, Session.StartTime), 0) as SessionDate, IsNull (Session.SpecialtyCode, 0) as SpecialtyCode, 'Cancelled Operation On Day' as KPI, Isnull(canc_op.CancelledOperationOnDay,0) as Actual
FROM   WHOLAP.dbo.BaseTheatreSession session
       LEFT OUTER JOIN (SELECT TheatreCode,
                               ProposedOperationDate,
                               Count(DISTINCT SourceUniqueID) as CancelledOperationOnDay
                        FROM   WH.Theatre.Cancellation
                        WHERE  dateadd(day, datediff(day, 0,CancellationDate), 0) = ProposedOperationDate
						GROUP  BY TheatreCode,
                                  ProposedOperationDate) canc_op
                    ON session.TheatreCode = canc_op.TheatreCode
                       AND dateadd(day, datediff(day, 0, Session.StartTime), 0) = canc_op.ProposedOperationDate
WHERE   Session.CancelledFlag = 0

----29/01/2014 CM : Cancelled Operation on Day Calculation changed as the method commented out below is not picking up all cancellations as the cancelled ops don't all appear to be in the FactTheatreOperation table. 
--SELECT     Session.SourceUniqueID, TheatreCode = Session.TheatreCode, SessionDate = dateadd(day, datediff(day, 0, Session.StartTime), 0), 
--                      SpecialtyCode = COALESCE (Session.SpecialtyCode, 0), KPI = 'Cancelled Operation On Day', 
--                      Actual = TheatreOperation.CancelledOperationOnDay
--FROM         dbo.BaseTheatreSession Session INNER JOIN
--                          (SELECT     TheatreOperation.SessionSourceUniqueID, CancelledOperationOnDay = SUM(TheatreOperation.CancelledOperationOnDay)
--                            FROM          dbo.FactTheatreOperation TheatreOperation
--                            WHERE      TheatreOperation.CancelledOperationOnDay = 1
--                            GROUP BY TheatreOperation.SessionSourceUniqueID) TheatreOperation ON 
--                      TheatreOperation.SessionSourceUniqueID = Session.SourceUniqueID
--WHERE     Session.CancelledFlag = 0

UNION ALL
--29/01/2014 CM: Added this new method of calculating cancelled ops
SELECT Session.SourceUniqueID,Session.TheatreCode,Dateadd(day, Datediff(day, 0, Session.StartTime), 0) AS SessionDate,Isnull (Session.SpecialtyCode, 0)  AS SpecialtyCode,
       'Cancelled Operation' AS KPI,Isnull(canc_op.CancelledOperation, 0) AS Actual
FROM   WHOLAP.dbo.BaseTheatreSession session
       LEFT OUTER JOIN (SELECT TheatreCode, ProposedOperationDate,Count(DISTINCT SourceUniqueID) CancelledOperation
                        FROM   WH.Theatre.Cancellation
                        GROUP  BY TheatreCode,
                                  ProposedOperationDate) canc_op
                    ON session.TheatreCode = canc_op.TheatreCode
                       AND Dateadd(day, Datediff(day, 0, Session.StartTime), 0) = canc_op.ProposedOperationDate
WHERE  Session.CancelledFlag = 0
----29/01/2014 CM : Cancelled Operations Calculation changed as the method commented out below is not picking up all cancellations as the cancelled ops don't all appear to be in the FactTheatreOperation table. 
--SELECT     Session.SourceUniqueID, TheatreCode = Session.TheatreCode, SessionDate = dateadd(day, datediff(day, 0, Session.StartTime), 0), 
--                      SpecialtyCode = COALESCE (Session.SpecialtyCode, 0), KPI = 'Cancelled Operation', Actual = TheatreOperation.CancelledOperation
--FROM         dbo.BaseTheatreSession Session INNER JOIN
--                          (SELECT     TheatreOperation.SessionSourceUniqueID, CancelledOperation = SUM(TheatreOperation.CancelledOperation)
--                            FROM          dbo.FactTheatreOperation TheatreOperation
--                            WHERE      TheatreOperation.CancelledOperation = 1
--                            GROUP BY TheatreOperation.SessionSourceUniqueID) TheatreOperation ON 
--                      TheatreOperation.SessionSourceUniqueID = Session.SourceUniqueID
--WHERE     Session.CancelledFlag = 0

UNION ALL
SELECT     Session.SourceUniqueID, TheatreCode = Session.TheatreCode, SessionDate = dateadd(day, datediff(day, 0, Session.StartTime), 0), 
                      SpecialtyCode = COALESCE (Session.SpecialtyCode, 0), KPI = 'Number of Patients Operated On', Actual = TheatreOperation.Operation
FROM         dbo.BaseTheatreSession Session INNER JOIN
                          (SELECT     TheatreOperation.SessionSourceUniqueID, Operation = COUNT(*)
                            FROM          dbo.FactTheatreOperation TheatreOperation
                            WHERE      TheatreOperation.Operation = 1 
                            and TheatreOperation.CancelledOperation <> 1 ---amended 08/03/2013 to exclude cancelled patients
                            GROUP BY TheatreOperation.SessionSourceUniqueID) TheatreOperation ON 
                      TheatreOperation.SessionSourceUniqueID = Session.SourceUniqueID
WHERE     Session.CancelledFlag = 0
UNION ALL
SELECT     Session.SourceUniqueID, TheatreCode = Session.TheatreCode, SessionDate = dateadd(day, datediff(day, 0, Session.StartTime), 0), 
                      SpecialtyCode = COALESCE (Session.SpecialtyCode, 0), KPI = 'Productive Minutes', Actual = TheatreOperation.OperationProductiveTime
FROM         dbo.BaseTheatreSession Session INNER JOIN
                          (SELECT     TheatreOperation.SessionSourceUniqueID, OperationProductiveTime = SUM(TheatreOperation.OperationProductiveTime)
                            FROM          dbo.FactTheatreOperation TheatreOperation
                            GROUP BY TheatreOperation.SessionSourceUniqueID) TheatreOperation ON 
                      TheatreOperation.SessionSourceUniqueID = Session.SourceUniqueID
WHERE     Session.CancelledFlag = 0
UNION ALL 
SELECT     Session.SourceUniqueID, TheatreCode = Session.TheatreCode, SessionDate = dateadd(day, datediff(day, 0, Session.StartTime), 0), 
                      SpecialtyCode = COALESCE (Session.SpecialtyCode, 0), KPI = 'Productive Minutes Calculation 2', Actual = TheatreOperation.OperationProductiveTimeCalc2
FROM         dbo.BaseTheatreSession Session INNER JOIN
                          (SELECT     TheatreOperation.SessionSourceUniqueID, OperationProductiveTimeCalc2 = SUM(TheatreOperation.OperationProductiveTimeCalc2)
                            FROM          dbo.FactTheatreOperation TheatreOperation
                            GROUP BY TheatreOperation.SessionSourceUniqueID) TheatreOperation ON 
                      TheatreOperation.SessionSourceUniqueID = Session.SourceUniqueID
WHERE     Session.CancelledFlag = 0










GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[12] 4[10] 2[59] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'FactTheatreKPI';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'FactTheatreKPI';

