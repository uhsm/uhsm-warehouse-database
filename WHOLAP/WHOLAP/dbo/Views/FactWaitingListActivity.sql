﻿CREATE view [FactWaitingListActivity] as

select
	 Activity.SourceUniqueID
	,Activity.ActivityTypeCode
	,Activity.ActivityDate
	,Activity.SiteCode
	,Activity.ConsultantCode
	,Activity.SpecialtyCode

	,AgreedMinutes =
		coalesce(
			case
			when Activity.ActivityTypeCode = '1' --WL Add
			then WaitingListAddMinutes.AgreedMinutes
			when Activity.ActivityTypeCode = '5' --Preadmission
			then PreadmissionMinutes.AgreedMinutes
			end
			,ActivityType.AgreedMinutes
			,10
		)

from
	dbo.BaseWaitingListActivity Activity

left join dbo.OlapWLActivityType ActivityType
on	ActivityType.ActivityTypeCode = Activity.ActivityTypeCode

left join 
	(
	select
		 SpecialtyCode = EntityCode
		,AgreedMinutes = convert(int, Description)
	from
		WH.dbo.EntityLookup
	where
		EntityTypeCode = 'WLADDMINUTES'
	) WaitingListAddMinutes
on	WaitingListAddMinutes.SpecialtyCode = Activity.SpecialtyCode

left join 
	(
	select
		 SpecialtyCode = EntityCode
		,AgreedMinutes = convert(int, Description)
	from
		WH.dbo.EntityLookup
	where
		EntityTypeCode = 'WLPREADMITMINUTES'
	) PreadmissionMinutes
on	PreadmissionMinutes.SpecialtyCode = Activity.SpecialtyCode
