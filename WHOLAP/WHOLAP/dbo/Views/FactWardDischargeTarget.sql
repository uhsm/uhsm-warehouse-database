﻿CREATE view FactWardDischargeTarget as

select
	 WardDischargeTarget.ServicePointCode

	,DischargeDate = Calendar.TheDate

	,DischargeHourOfDay =
		CAST(
			case
			when WardDischargeTarget.PeriodTypeCode = 'AM'
			then 0
			else 12
			end
			as tinyint
		)

	,TargetDischarges = WardDischargeTarget.Discharges
from
	WH.dbo.WardDischargeTarget

inner join dbo.OlapCalendar Calendar
on	Calendar.DayOfWeek not in ('Saturday', 'Sunday')
and	exists
	(
	select
		1
	from
		dbo.FactAPCDischarge
	where
		FactAPCDischarge.DischargeDate = Calendar.TheDate
	)
