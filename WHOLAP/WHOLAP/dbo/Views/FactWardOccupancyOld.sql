﻿


CREATE view [dbo].[FactWardOccupancyOld] as

select
	 WardStay.SourceUniqueID
	,CensusDate = Calendar.TheDate
	,WardStay.StartTime
	,WardStay.EndTime
	,WardStay.WardCode

	,PatientCategoryCode = 
		case
		when Spell.AdmissionMethodTypeCode = 'NE'
		then 'NE'
		else Spell.InpatientStayCode
		end

	,AuditPeriod = 0 --midday

	,FCE.SpecialtyCode

	,FCE.ConsultantCode

	,Cases = 1
from
	BaseAPCWardStay WardStay

inner join dbo.CalendarBase Calendar
on	dateadd(hour, 12, Calendar.TheDate) between WardStay.StartTime and coalesce(WardStay.EndTime, getdate())

inner join OlapAPC Spell
on	Spell.SourceSpellNo = WardStay.SourceSpellNo
and	Spell.AdmissionTime = Spell.EpisodeStartTime

inner join dbo.BaseAPC FCE
on	FCE.SourceUniqueID = WardStay.FCESourceUniqueID

union all

select
	 WardStay.SourceUniqueID
	,CensusDate = Calendar.TheDate
	,WardStay.StartTime
	,WardStay.EndTime
	,WardStay.WardCode

	,PatientCategoryCode = 
		case
		when Spell.AdmissionMethodTypeCode = 'NE'
		then 'NE'
		else Spell.InpatientStayCode
		end

	,AuditPeriod = 1 --midnight

	,FCE.SpecialtyCode

	,FCE.ConsultantCode

	,Cases = 1
from
	BaseAPCWardStay WardStay

inner join dbo.CalendarBase Calendar
on	Calendar.TheDate between WardStay.StartTime and coalesce(WardStay.EndTime, getdate())

inner join OlapAPC Spell
on	Spell.SourceSpellNo = WardStay.SourceSpellNo
and	Spell.AdmissionTime = Spell.EpisodeStartTime

inner join dbo.BaseAPC FCE
on	FCE.SourceUniqueID = WardStay.FCESourceUniqueID







