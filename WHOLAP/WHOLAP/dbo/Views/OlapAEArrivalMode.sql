﻿
CREATE view [dbo].[OlapAEArrivalMode] as

select
	 ArrivalMode.ArrivalModeCode
	,ArrivalMode.ArrivalMode
from
	WH.AE.ArrivalMode ArrivalMode
where
	exists
	(
	select
		1
	from
		dbo.FactAE FactAE
	where
		FactAE.ArrivalModeCode = ArrivalMode.ArrivalModeCode
	)

union all

select distinct
	 ArrivalModeCode
	,ArrivalMode = 
		case
		when FactAE.ArrivalModeCode = -1
		then 'N/A'
		else convert(varchar, FactAE.ArrivalModeCode) + ' - No Description'
		end
from
	dbo.FactAE
where
	not exists
	(
	select
		1
	from
		WH.AE.ArrivalMode ArrivalMode
	where
		ArrivalMode.ArrivalModeCode = FactAE.ArrivalModeCode
	)

