﻿



CREATE view [dbo].[OlapAEAttendanceCategory] as

select
	 AttendanceCategoryCode
	,AttendanceCategory
	,AttendanceCategoryGroup
from
	WH.AE.AttendanceCategory


union all

select distinct
	 AttendanceCategoryCode
	,AttendanceCategory = 
		case
		when FactAE.AttendanceCategoryCode = '#'
		then 'N/A'
		else convert(varchar, FactAE.AttendanceCategoryCode) + ' - No Description'
		end

	,AttendanceCategoryGroup = 'Other'

from
	dbo.FactAE
where
	not exists
	(
	select
		1
	from
		WH.AE.AttendanceCategory AttendanceCategory
	where
		AttendanceCategory.AttendanceCategoryCode = FactAE.AttendanceCategoryCode
	)



