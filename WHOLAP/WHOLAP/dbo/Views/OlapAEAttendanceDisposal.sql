﻿create view [OlapAEAttendanceDisposal]
as

select
	 AttendanceDisposalCode
	,AttendanceDisposal
from
	WH.AE.AttendanceDisposal

union all

select distinct
	 AttendanceDisposalCode =
		coalesce(
			AttendanceDisposalCode
			,'##'
		)
	,AttendanceDisposal =
		coalesce(
			AttendanceDisposalCode + ' - No Description'
			,'N/A'
		)
from
	WH.AE.Encounter AEEncounter
where
	not exists
	(
	select
		1
	from
		WH.AE.AttendanceDisposal AttendanceDisposal
	where
		AttendanceDisposal.AttendanceDisposalCode = AEEncounter.AttendanceDisposalCode
	)
