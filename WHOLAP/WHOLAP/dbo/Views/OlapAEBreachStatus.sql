﻿create view [OlapAEBreachStatus] as

select
	 BreachStatusCode
	,BreachStatus
from
	WH.AE.BreachStatus
