﻿
CREATE view [dbo].[OlapAEInterface] as

select
	InterfaceCode = 'CASC'
	,Interface = 'Trust A&E'

union all

select
	InterfaceCode = 'LOR'
	,Interface = 'MAU'

union all

select
	InterfaceCode = 'MCH'
	,Interface = 'Manchester Community Health'


