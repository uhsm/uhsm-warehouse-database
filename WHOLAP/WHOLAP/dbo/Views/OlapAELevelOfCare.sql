﻿
CREATE view [dbo].[OlapAELevelOfCare] as

select
	 LevelOfCareCode
	,LevelOfCare
	,LevelOfCareGroup
from
	WH.AE.LevelOfCare

union all

select
	 LevelOfCareCode = '##'
	,LevelOfCare = 'N/A'
	,LevelOfCareGroup = 'Minor'


