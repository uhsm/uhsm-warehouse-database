﻿



CREATE view [dbo].[OlapAEPractice] as

select
	 Practice.PracticeCode
	,Practice.Practice

	,PCTCode =
		coalesce(
			Practice.PCTCode
			,NationalPCT.OrganisationCode
			,'N/A'
		)

	,PCT =
		coalesce(
			Practice.PCT
			,NationalPCT.Organisation
			,'N/A'
		) + ' (' +
		coalesce(
			Practice.PCTCode
			,NationalPCT.OrganisationCode
			,'N/A'
		) + ')'

from
	WH.AE.Practice Practice

left join Organisation.dbo.Practice NationalPractice
on	NationalPractice.OrganisationCode = Practice.NationalPracticeCode

left join Organisation.dbo.PCT NationalPCT
on	NationalPCT.OrganisationCode = NationalPractice.ParentOrganisationCode

where
	exists
	(
	select
		1
	from
		dbo.FactAE FactAE
	where
		FactAE.RegisteredPracticeCode = Practice.PracticeCode
	)

union all

select distinct
	 PracticeCode = RegisteredPracticeCode

	,Practice = 
		case
		when FactAE.RegisteredPracticeCode = 'X99999'
		then 'N/A'
		else convert(varchar, FactAE.RegisteredPracticeCode) + ' - No Description'
		end

	,PCTCode = 'N/A'
	,PCT = 'N/A'

from
	dbo.FactAE
where
	not exists
	(
	select
		1
	from
		WH.AE.Practice Practice
	where
		Practice.PracticeCode = FactAE.RegisteredPracticeCode
	)




