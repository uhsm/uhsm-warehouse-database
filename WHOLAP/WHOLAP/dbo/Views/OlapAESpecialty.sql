﻿
CREATE view [dbo].[OlapAESpecialty] as

select
	 AESpecialty.SpecialtyCode

	,Specialty =
		AESpecialty.Specialty +
			case
			when exists
				(
				select
					1
				from
					WH.AE.Specialty DuplicateAESpecialty
				where
					DuplicateAESpecialty.Specialty in
					(
					select
						AESpecialty.Specialty
					from
						WH.AE.Specialty AESpecialty
					where
						exists
						(
						select
							1
						from
							dbo.FactAE FactAE
						where
							FactAE.ReferredToSpecialtyCode = AESpecialty.SpecialtyCode
						)

					group by
						AESpecialty.Specialty
					having
						count(*) > 1
					)
				and	DuplicateAESpecialty.SpecialtyCode = AESpecialty.SpecialtyCode
				)
			then ' (' + convert(varchar, AESpecialty.SpecialtyCode) + ')'
			else ''
			end

	,IsMentalHealth

	,DivisionCode =
		coalesce(
			DivisionCode
			,'##'
		)

from
	WH.AE.Specialty AESpecialty
where
	exists
	(
	select
		1
	from
		dbo.FactAE FactAE
	where
		FactAE.ReferredToSpecialtyCode = AESpecialty.SpecialtyCode
	)

union all

select distinct
	 FactAE.ReferredToSpecialtyCode
	,Specialty = 
		case
		when FactAE.ReferredToSpecialtyCode = '##'
		then 'N/A'
		else convert(varchar, FactAE.ReferredToSpecialtyCode) + ' - No Description'
		end
	,IsMentalHealth = 0
	,DivisionCode = '##'
from
	dbo.FactAE
where
	not exists
	(
	select
		1
	from
		WH.AE.Specialty AESpecialty
	where
		AESpecialty.SpecialtyCode = FactAE.ReferredToSpecialtyCode
	)

