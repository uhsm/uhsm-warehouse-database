﻿

CREATE View [dbo].[OlapAPCEncounter] AS
SELECT
	 EncounterRecno
	 ,NHSNumber = ISNULL(NHSNumber,'')
	 ,FacilityID = APC.DistrictNo
	 ,EthnicOrigin = ISNULL(EthBase.ReferenceValue,'Not Specified')
	 ,Postcode = ISNULL(Postcode,'ZZ99 3AZ')
	 ,DateOfBirth = ISNULL(DateOfBirth,'19000101 00:00:00')
	 ,DateOfDeath = ISNULL(DateOfDeath, '19000101 00:00:00')
	 ,Sex = ISNULL(SexBase.ReferenceValue, 'Not Specified')
	
from BaseAPC APC
left join WH.PAS.ReferenceValue SexBase
	on APC.SexCode = SexBase.ReferenceValueCode and SexBase.ReferenceDomainCode = 'SEXXX'
left join WH.PAS.ReferenceValue EthBase
	on APC.EthnicOriginCode = EthBase.ReferenceValueCode and EthBase.ReferenceDomainCode = 'ETHGR'





