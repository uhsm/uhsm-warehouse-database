﻿CREATE view [dbo].[OlapAPCWaitingList] as

select
	 Encounter.EncounterRecno
	,Encounter.CensusDate
	,Encounter.SourceUniqueID
	,Encounter.SourcePatientNo
	,Encounter.SourceEncounterNo
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath
	,Encounter.SexCode
	,Encounter.NHSNumber
	,Encounter.DistrictNo
	,Encounter.Postcode
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.DHACode
	,Encounter.HomePhone
	,Encounter.WorkPhone
	,Encounter.EthnicOriginCode
	,Encounter.MaritalStatusCode
	,Encounter.ReligionCode
	,ConsultantCode = coalesce(Encounter.ConsultantCode, -1)
	,SpecialtyCode = coalesce(Encounter.SpecialtyCode, -1)
	,Encounter.PASSpecialtyCode
	,Encounter.ManagementIntentionCode
	,Encounter.AdmissionMethodCode
	,Encounter.PriorityCode
	,Encounter.WaitingListCode
	,Encounter.CommentClinical
	,Encounter.CommentNonClinical
	,Encounter.IntendedPrimaryOperationCode
	,Encounter.Operation
	,SiteCode = coalesce(Encounter.SiteCode, -1)
	,Encounter.WardCode
	,Encounter.WLStatus
	,Encounter.PurchaserCode
	,Encounter.ProviderCode
	,Encounter.ContractSerialNo
	,Encounter.AdminCategoryCode
	,Encounter.CancelledBy
	,Encounter.BookingTypeCode
	,Encounter.CasenoteNumber
	,Encounter.OriginalDateOnWaitingList
	,Encounter.DateOnWaitingList
	,Encounter.TCIDate
	,Encounter.KornerWait
	,Encounter.CountOfDaysSuspended
	,Encounter.SuspensionStartDate
	,Encounter.SuspensionEndDate
	,Encounter.SuspensionReasonCode
	,Encounter.SuspensionReason
	,Encounter.InterfaceCode
	,Encounter.RegisteredGpCode
	,RegisteredGpPracticeCode = coalesce(Encounter.RegisteredGpPracticeCode, -1)
	,Encounter.EpisodicGpCode
	,EpisodicGpPracticeCode = coalesce(Encounter.EpisodicGpPracticeCode, -1)
	,Encounter.SourceTreatmentFunctionCode
	,Encounter.TreatmentFunctionCode
	,Encounter.NationalSpecialtyCode
	,Encounter.PCTCode
	,Encounter.BreachDate
	,Encounter.ExpectedAdmissionDate
	,Encounter.ReferralDate
	,Encounter.FuturePatientCancelDate
	,Encounter.ProcedureTime
	,Encounter.TheatreCode
	,Encounter.AdmissionReason
	,Encounter.EpisodeNo
	,Encounter.BreachDays
	,Encounter.MRSAFlag
	,Encounter.RTTPathwayID
	,Encounter.RTTPathwayCondition
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.RTTSpecialtyCode
	,Encounter.RTTCurrentProviderCode
	,Encounter.RTTCurrentStatusCode
	,Encounter.RTTCurrentStatusDate
	,Encounter.RTTCurrentPrivatePatientFlag
	,Encounter.RTTOverseasStatusFlag
	,Encounter.NationalBreachDate
	,Encounter.NationalBreachDays
	,Encounter.DerivedBreachDays
	,Encounter.DerivedClockStartDate
	,Encounter.DerivedBreachDate
	,Encounter.RTTBreachDate
	,Encounter.RTTDiagnosticBreachDate
	,Encounter.NationalDiagnosticBreachDate
	,Encounter.SocialSuspensionDays
	,Encounter.BreachTypeCode
	,Encounter.PASCreated
	,Encounter.PASUpdated
	,Encounter.PASCreatedByWhom
	,Encounter.PASUpdatedByWhom
	,Encounter.Created
	,Encounter.Updated
	,Encounter.ByWhom
	,Encounter.ExpectedDischargeDate
	,Encounter.AnaestheticTypeCode

	,AgeCode =
	case
	when datediff(day, DateOfBirth, Encounter.CensusDate) is null then 'Age Unknown'
	when datediff(day, DateOfBirth, Encounter.CensusDate) <= 0 then '00 Days'
	when datediff(day, DateOfBirth, Encounter.CensusDate) = 1 then '01 Day'
	when datediff(day, DateOfBirth, Encounter.CensusDate) > 1 and
	datediff(day, DateOfBirth, Encounter.CensusDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, DateOfBirth, Encounter.CensusDate)), 2) + ' Days'
	
	when datediff(day, DateOfBirth, Encounter.CensusDate) > 28 and
	datediff(month, DateOfBirth, Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, DateOfBirth, Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, DateOfBirth, Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end > 1 and
	 datediff(month, DateOfBirth, Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, DateOfBirth,Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, DateOfBirth, Encounter.CensusDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.CensusDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.CensusDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.CensusDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, DateOfBirth, Encounter.CensusDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.CensusDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.CensusDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.CensusDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end 

	,1 Cases

	,convert(int, Encounter.KornerWait) LengthOfWait

	,DurationCode =
		case
		when Encounter.KornerWait < 0 then 'NA'
		else
			coalesce(
				right('0' +
				convert(varchar, 
				case
				when convert(int, Encounter.KornerWait /7 ) > 51 then 52
				else convert(int, Encounter.KornerWait /7 )
				end), 2)
				,'NA'
			)
		end

	,WaitTypeCode =
		case
		when Encounter.AdmissionMethodCode = 13 then '40' --not specified
		when Encounter.AdmissionMethodCode = 8811 then '20' --booked
		when Encounter.AdmissionMethodCode = 8812 then '30' --planned
		when Encounter.AdmissionMethodCode = 8814 then '30' --planned
		else '10' --waiting list
		end

--			 8811	--Elective - Booked	12
--			,8814	--Maternity ante-partum	31
--			,8812	--Elective - Planned	13
--			,13		--Not Specified	NSP
--			,8810	--Elective - Waiting List	11

	,StatusCode =
		case
		when Encounter.WLStatus = 'Suspended' then '20'
		else '10'
		end

	,CategoryCode =
		case
		when Encounter.ManagementIntentionCode = 2000619 then '10'
		when Encounter.ManagementIntentionCode = 9487 then '10'
		when Encounter.ManagementIntentionCode = 8850 then '20'
		when Encounter.ManagementIntentionCode = 8851 then '20'
		else '28' --other inpatient
		end

--8851	INMGT	Planned Sequence - No Overnight Stays	4
--9487	INMGT	Planned Sequence - Inc. Over Night Stays	3
--2003683	INMGT	Not Applicable	8
--2003684	INMGT	Not Known	9
--2000619	INMGT	At Least One Night	1
--8850	INMGT	No Overnight Stay	2

	,AddedToWaitingListTime = Encounter.PASCreated

	,AddedLastWeek =
	case
	when
		datediff(
			 day
			,dateadd(day, datediff(day, 0, Encounter.PASCreated), 0)
			,Encounter.CensusDate
		) < 8 then 1
	else 0
	end

	,WaitingListAddMinutes =
	case
	when
		datediff(
			 day
			,dateadd(day, datediff(day, 0, Encounter.PASCreated), 0)
			,Encounter.CensusDate
		) < 8
	then
		coalesce(
			 WaitingListAddMinutes.WaitingListAddMinutes
			,(
			select
				NumericValue
			from
				WH.dbo.Parameter
			where
				Parameter = 'DEFAULTWLADDMINUTES'
			)
			,10
		)
	else null
	end

	,OPCSCoded =
	case
	when Encounter.IntendedPrimaryOperationCode is null
	then 0
	else 1
	end

	,WithRTTStartDate =
	case
	when Encounter.RTTStartDate is null
	then 0
	else 1
	end

	,WithRTTStatusCode =
	case
	when RTTStatus.NationalRTTStatusCode in ('10', '20') then 1
	else 0
	end

	,WithExpectedAdmissionDate =
	case
	when Encounter.ExpectedAdmissionDate is null
	then 0
	else 1
	end

from
	dbo.BaseAPCWaitingList Encounter

left join WH.PAS.RTTStatus RTTStatus
on	RTTStatus.RTTStatusCode = Encounter.RTTCurrentStatusCode

left join 
	(
	select
		 SpecialtyCode = EntityCode
		,WaitingListAddMinutes = convert(int, Description)
	from
		WH.dbo.EntityLookup
	where
		EntityTypeCode = 'WLADDMINUTES'
	) WaitingListAddMinutes
on	WaitingListAddMinutes.SpecialtyCode = Encounter.SpecialtyCode
