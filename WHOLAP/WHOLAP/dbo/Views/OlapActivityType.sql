﻿
create view [dbo].[OlapActivityType] as

select
	 ActivityTypeCode = ActivityType.MainCode
	,ActivityType = ActivityType.ReferenceValue
from
	WH.PAS.ReferenceValue ActivityType
where
	ReferenceDomainCode = 'SORCE'
and	exists
	(
	select
		1
	from
		dbo.FactClinicalCoding
	where
		FactClinicalCoding.SourceCode = ActivityType.MainCode
	)

