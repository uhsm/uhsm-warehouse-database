﻿
CREATE view [dbo].[OlapAdmissionMethod] as

SELECT
	 AdmissionMethod.AdmissionMethodCode
	,AdmissionMethod.AdmissionMethod AdmissionMethod
	,AdmissionMethod.AdmissionMethodLocalCode
	,AdmissionMethod.AdmissionMethodTypeCode
	,AdmissionMethod.AdmissionMethodType
FROM 
	WH.PAS.AdmissionMethod AdmissionMethod


