﻿create view [dbo].[OlapAdmissionType] as

SELECT
	 AdmissionType.AdmissionTypeCode
	,AdmissionType.AdmissionType
	,AdmissionType.AdmissionTypeCode1
FROM 
	WH.Theatre.AdmissionType AdmissionType
