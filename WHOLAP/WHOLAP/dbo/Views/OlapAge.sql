﻿create view [OlapAge] as

select
	 AgeBase.AgeCode
	,AgeBase.Age
	,AgeBase.AgeParentCode
from
	dbo.AgeBase
