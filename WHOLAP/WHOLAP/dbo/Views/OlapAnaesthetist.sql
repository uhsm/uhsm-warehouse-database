﻿create view [OlapAnaesthetist] as

select
	 Anaesthetist.AnaesthetistCode
	,Anaesthetist.InterfaceCode

	,Anaesthetist =
		coalesce(
			Anaesthetist.Surname +
			coalesce(
				 ', ' + Anaesthetist.Initials
				,''
			)
			,Anaesthetist.AnaesthetistCode +  ' - No Description'
		) + 
		' (' + Anaesthetist.AnaesthetistCode + 
		', ' + Interface.Interface + ')'
from
	WH.Theatre.Anaesthetist Anaesthetist

inner join WH.WH.Interface
on	Interface.InterfaceCode = Anaesthetist.InterfaceCode

union all

select distinct
	 AnaesthetistCode = '##'
	,Interface.InterfaceCode

	,Anaesthetist =
		'N/A (' + Interface.Interface + ')'
from
	WH.WH.Interface Interface
where
	Interface.InterfaceTypeCode = 'T'
