﻿create view OlapAuditPeriod as

select
	 AuditPeriodCode = 0
	,AuditPeriod = 'Midday'

union all

select
	 AuditPeriodCode = 1
	,AuditPeriod = 'Midnight'

