﻿
CREATE view [dbo].[OlapCCG] as

select
	 CCGCode = rtrim(CCG.OrganisationCode)
	,CCG = rtrim(CCG.OrganisationCode) + ' - ' + CCG.Organisation
from
	OrganisationCCG.dbo.CCG

union all

select
	'N/A'
	,'Unknown'

