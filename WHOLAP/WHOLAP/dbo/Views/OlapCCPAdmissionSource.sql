﻿CREATE view [dbo].[OlapCCPAdmissionSource] as

select
	 CCPAdmissionSourceCode = RefValue.ReferenceValueCode
	,CCPAdmissionSourceCode1 = RefValue.MainCode
	,CCPAdmissionSource = RefValue.ReferenceValue
from
	WH.PAS.ReferenceValue RefValue
where
	ReferenceDomainCode = 'CCASO'
and	exists
	(
	select
		1
	from
		dbo.FactCriticalCarePeriod
	where
		FactCriticalCarePeriod.CCPAdmissionSource = RefValue.ReferenceValueCode
	)
