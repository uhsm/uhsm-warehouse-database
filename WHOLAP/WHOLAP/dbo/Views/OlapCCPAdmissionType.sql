﻿CREATE view [dbo].[OlapCCPAdmissionType] as

select
	 CCPAdmissionTypeCode = RefValue.ReferenceValueCode
	,CCPAdmissionTypeCode1 = RefValue.MainCode
	,CCPAdmissionType = RefValue.ReferenceValue
from
	WH.PAS.ReferenceValue RefValue
where
	ReferenceDomainCode = 'CCATY'
and	exists
	(
	select
		1
	from
		dbo.FactCriticalCarePeriod
	where
		FactCriticalCarePeriod.CCPAdmissionType = RefValue.ReferenceValueCode
	)

