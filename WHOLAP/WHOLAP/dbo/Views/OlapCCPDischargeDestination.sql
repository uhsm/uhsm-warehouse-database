﻿CREATE view [dbo].[OlapCCPDischargeDestination] as

select
	 CCPDischargeDestinationCode = RefValue.ReferenceValueCode
	,CCPDischargeDestinationCode1 = RefValue.MainCode
	,CCPDischargeDestination = RefValue.ReferenceValue
from
	WH.PAS.ReferenceValue RefValue
where
	ReferenceDomainCode = 'CCDDE'
and	exists
	(
	select
		1
	from
		dbo.FactCriticalCarePeriod
	where
		FactCriticalCarePeriod.CCPDischargeDestination = RefValue.ReferenceValueCode
	)
