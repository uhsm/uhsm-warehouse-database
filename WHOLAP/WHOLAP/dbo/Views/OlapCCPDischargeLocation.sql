﻿CREATE view [dbo].[OlapCCPDischargeLocation] as

select
	 CCPDischargeLocationCode = RefValue.ReferenceValueCode
	,CCPDischargeLocationCode1 = RefValue.MainCode
	,CCPDischargeLocation = RefValue.ReferenceValue
from
	WH.PAS.ReferenceValue RefValue
where
	ReferenceDomainCode = 'CCDLO'
and	exists
	(
	select
		1
	from
		dbo.FactCriticalCarePeriod
	where
		FactCriticalCarePeriod.CCPDischargeLocation = RefValue.ReferenceValueCode
	)
