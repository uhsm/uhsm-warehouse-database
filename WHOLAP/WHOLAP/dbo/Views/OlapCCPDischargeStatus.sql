﻿CREATE view [dbo].[OlapCCPDischargeStatus] as

select
	 CCPDischargeStatusCode = RefValue.ReferenceValueCode
	,CCPDischargeStatusCode1 = RefValue.MainCode
	,CCPDischargeStatus = RefValue.ReferenceValue
from
	WH.PAS.ReferenceValue RefValue
where
	ReferenceDomainCode = 'CCDST'
and	exists
	(
	select
		1
	from
		dbo.FactCriticalCarePeriod
	where
		FactCriticalCarePeriod.CCPDischargeStatus = RefValue.ReferenceValueCode
	)
