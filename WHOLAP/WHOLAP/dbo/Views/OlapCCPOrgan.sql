﻿
CREATE view [dbo].[OlapCCPOrgan] as

select
	 CCPOrganCode = RefValue.ReferenceValueCode
	,CCPOrganCode1 = RefValue.MainCode
	,CCPOrgan = RefValue.ReferenceValue
from
	WH.PAS.ReferenceValue RefValue
where
	ReferenceDomainCode = 'ORGAN'
and	exists
	(
	select
		1
	from
		dbo.FactCriticalCarePeriod
	where
		FactCriticalCarePeriod.CCPStayOrganSupported = RefValue.ReferenceValueCode
	)

Union All
select
	 CCPOrganCode = -1
	,CCPOrganCode1 = 'NSP'
	,CCPOrgan = 'Not Specified'




