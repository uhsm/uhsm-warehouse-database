﻿CREATE view [dbo].[OlapCCPSourceLocation] as

select
	 CCPSourceLocationCode = RefValue.ReferenceValueCode
	,CCPSourceLocationCode1 = RefValue.MainCode
	,CCPSourceLocation = RefValue.ReferenceValue
from
	WH.PAS.ReferenceValue RefValue
where
	ReferenceDomainCode = 'CCSLO'
and	exists
	(
	select
		1
	from
		dbo.FactCriticalCarePeriod
	where
		FactCriticalCarePeriod.CCPAdmissionSourceLocation = RefValue.ReferenceValueCode
	)
