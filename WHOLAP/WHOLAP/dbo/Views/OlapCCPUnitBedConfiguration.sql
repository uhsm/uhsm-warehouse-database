﻿CREATE view [dbo].[OlapCCPUnitBedConfiguration] as

select
	 CCPUnitBedConfigCode = RefValue.ReferenceValueCode
	,CCPUnitBedConfigCode1 = RefValue.MainCode
	,CCPUnitBedConfig = RefValue.ReferenceValue
from
	WH.PAS.ReferenceValue RefValue
where
	ReferenceDomainCode = 'CCUBC'
and	exists
	(
	select
		1
	from
		dbo.FactCriticalCarePeriod
	where
		FactCriticalCarePeriod.CCPUnitBedFunction = RefValue.ReferenceValueCode
	)
