﻿CREATE view [dbo].[OlapCCPUnitFunction] as

select
	 CCPUnitFunctionCode = RefValue.ReferenceValueCode
	,CCPUnitFunctionCode1 = RefValue.MainCode
	,CCPUnitFunction = RefValue.ReferenceValue
from
	WH.PAS.ReferenceValue RefValue
where
	ReferenceDomainCode = 'CCUFU'
and	exists
	(
	select
		1
	from
		dbo.FactCriticalCarePeriod
	where
		FactCriticalCarePeriod.CCPUnitFunction = RefValue.ReferenceValueCode
	)
