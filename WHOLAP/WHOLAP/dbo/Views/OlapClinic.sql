﻿CREATE view [OlapClinic] as

select
	*
from
	(
	select
		 Clinic.ClinicCode
		,Comment
		,ConsultantCode = coalesce(Clinic.ConsultantCode, 'N/A')
		,SpecialtyCode = coalesce(Clinic.SpecialtyCode, 'N/A')
		,Clinic.Clinic
		,Clinic.SiteCode
		,Clinic.FunctionCode
		,Clinic.ProviderCode
		,Clinic.VirtualClinicFlag
		,Clinic.POAClinicFlag
	from
		WH.PAS.Clinic Clinic

	union all

	select
		 Ward.WardCode
		,Comment = null
		,ConsultantCode = 'N/A'
		,SpecialtyCode = 'N/A'
		,Ward.Ward
		,Ward.SiteCode
		,FunctionCode = null
		,ProviderCode = null
		,VirtualWardFlag = 0
		,POAWardFlag = 0
	from
		WH.PAS.Ward Ward
	where
	--	exists
	--	(
	--	select
	--		1
	--	from
	--		dbo.OlapDiaryFact
	--	where
	--		OlapDiaryFact.WardCode = Ward.WardCode
	--	)
	--
	--or
		exists
		(
		select
			1
		from
			dbo.FactOP
		where
			FactOP.ClinicCode = Ward.WardCode
		)
	) Clinic

where
--	exists
--	(
--	select
--		1
--	from
--		dbo.OlapDiaryFact
--	where
--		OlapDiaryFact.ClinicCode = Clinic.ClinicCode
--	)
--
--or
	exists
	(
	select
		1
	from
		dbo.FactOP
	where
		FactOP.ClinicCode = Clinic.ClinicCode
	)
