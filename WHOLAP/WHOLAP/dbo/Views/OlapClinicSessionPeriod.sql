﻿create view [OlapClinicSessionPeriod] as

select
	 ClinicSessionPeriod.ClinicSessionPeriodCode
	,ClinicSessionPeriod.ClinicSessionPeriod
from
	WH.OP.ClinicSessionPeriod ClinicSessionPeriod
