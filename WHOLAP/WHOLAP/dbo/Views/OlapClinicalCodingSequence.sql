﻿
create view OlapClinicalCodingSequence as

select distinct
	 SequenceNo
	,Sequence = SequenceNo
from
	dbo.FactClinicalCoding

