﻿create view OlapClinicalCodingType as

select
	 ClinicalCodingTypeCode = ClinicalCodingType.MainCode
	,ClinicalCodingType = ClinicalCodingType.ReferenceValue
from
	WH.PAS.ReferenceValue ClinicalCodingType
where
	ReferenceDomainCode = 'DPTYP'
and	exists
	(
	select
		1
	from
		dbo.FactClinicalCoding
	where
		FactClinicalCoding.ClinicalCodingTypeCode = ClinicalCodingType.MainCode
	)
