﻿
create view [dbo].[OlapCoding] as

select
	 Coding.CodingCode

	,CodingLocalCode

	,Coding = 
		Coding.CodingLocalCode + ' - ' + Coding.Coding

	,ParentCodingCode

	,CodingTypeCode
from
	WH.PAS.Coding Coding


