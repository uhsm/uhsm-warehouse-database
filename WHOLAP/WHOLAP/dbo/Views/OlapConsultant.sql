﻿CREATE view [OlapConsultant]
as

select
	 Consultant.ConsultantCode
	,Consultant =
		Consultant.Surname + 
		coalesce(', ' + Consultant.Forename, '') +
		coalesce(', ' + Consultant.Title, '')
from
	WH.WH.Consultant Consultant

union all

select
	'N/A'
	,'Unknown'
