﻿
CREATE VIEW [dbo].[OlapCriticalCarePeriod] as 

select
	CCP.EncounterRecno
	,CCP.SourceCCPStayNo
	,CCP.SourceCCPNo
	,CCP.CCPSequenceNo
	,CCP.SourceEncounterNo
	,CCP.SourceWardStayNo
	,CCP.SourceSpellNo
	,CCP.CCPIndicator
	,CCPStartDate = CAST(CONVERT(VARCHAR(8),CCP.CCPStartDate,112) AS DATETIME)
	,CCPEndDate = CAST(CONVERT(VARCHAR(8),CCP.CCPEndDate,112) AS DATETIME)
	,CCP.CCPReadyDate
	,CCP.CCPNoOfOrgansSupported
	,CCP.CCPICUDays
	,CCP.CCPHDUDays
	,CCP.CCPAdmissionSource
	,CCP.CCPAdmissionType
	,CCP.CCPAdmissionSourceLocation
	,CCP.CCPUnitFunction
	,CCP.CCPUnitBedFunction
	,CCP.CCPDischargeStatus
	,CCP.CCPDischargeDestination
	,CCP.CCPDischargeLocation
	,CCP.CCPStayOrganSupported
	,CCP.CCPStayCareLevel
	,CCPStayWard = ISNULL(CCP.CCPStayWard,-1)
	,CCPStayDate = CAST(CONVERT(VARCHAR(8),CCP.CCPStayDate,112) AS SMALLDATETIME)
	,CCPOrganSupportDays = CCP.CCPStay
	,CCP.CCPBedStay
	,CCP.CCPStayNoOfOrgansSupported
	,CCP.CCPStayType
	,SpecialtyCOde = IsNUll(APC.SpecialtyCode,-1)
	,APC.PurchaserCode
	,ConsultantCode = ISNULL(APC.ConsultantCode,-1)
	,APC.RegisteredGpPracticeCode
from BaseCriticalCarePeriod CCP
	LEFT JOIN BaseAPC APC ON CCP.SourceEncounterNo = APC.SourceUniqueID
WHERE CCPStayDate IS NOT NULL

