﻿create view [OlapDemandCategory] as

select
	 DemandCategory.DemandCategoryCode
	,DemandCategory.DemandCategory
	,DemandCategory.DemandCategoryParentCode
from
	dbo.DemandCategoryBase DemandCategory
