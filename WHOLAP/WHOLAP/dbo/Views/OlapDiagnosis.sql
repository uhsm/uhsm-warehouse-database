﻿create view [OlapDiagnosis] as

select distinct
	 Diagnosis.DiagnosisCode

	,Diagnosis = 
		Diagnosis.DiagnosisCode + ' - ' + Diagnosis.Diagnosis

	,DiagnosisChapterCode

	,DiagnosisChapter =
		Diagnosis.DiagnosisChapterCode + ' - ' + Diagnosis.DiagnosisChapter
from
	WH.PAS.Diagnosis Diagnosis

union all

select
	 '##'
	,'N/A'	,'##'
	,'N/A'
