﻿CREATE view [OlapDoctor] as

select
	DoctorCode
	,Doctor
	,Comment
from
	WH.PAS.Doctor Doctor
where
	exists
	(
	select
		1
	from
		dbo.FactOPDiary
	where
		FactOPDiary.DoctorCode = Doctor.DoctorCode
	)

union all

select
	 'N/A'
	,'N/A'
	,'N/A'
