﻿create view [OlapDuration] as

select
	 DurationCode
	,Duration
	,DurationParentCode
from
	dbo.DurationBase
