﻿
CREATE view [dbo].[OlapDurationWeeks] as

select
	 WaitDurationCode
	,DurationWeek
	,DurationBandCode
	,DurationBand
	,CellColour
from
	WH.RTT.DurationWeeks

