﻿CREATE view [OlapHRG] as

select
	 HRG.HRGCode
	,HRG =
		HRG.HRGCode + ' - ' + HRG.HRG

	,HRGChapterCode =
		case
		when AE.HRGCode is not null
		then 'ZAE'
		else left(HRG.HRGCode, 1)
		end 

	,HRGChapter =
		case
		when AE.HRGCode is not null
		then 'A&E'
		else left(HRG.HRGCode, 1)
		end 

from
	WH.WH.HRG HRG

left join
	(
	select HRGCode = 'V01' union all
	select HRGCode = 'V02' union all
	select HRGCode = 'V03' union all
	select HRGCode = 'V04' union all
	select HRGCode = 'V05' union all
	select HRGCode = 'V06' union all
	select HRGCode = 'V07' union all
	select HRGCode = 'V08' union all
	select HRGCode = 'U06'
	) AE
on	AE.HRGCode = HRG.HRGCode

union all

select
	'ZZZ',
	'N/A',
	'Z',
	'N/A'
