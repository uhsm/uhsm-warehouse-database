﻿Create View OlapMISYSCaseGroup AS

Select
CaseGroupCode = EntityCode
,CaseGroup = Description
From EntityLookup 
Where EntityTypeCode = 'MISYSCaseGroup'
