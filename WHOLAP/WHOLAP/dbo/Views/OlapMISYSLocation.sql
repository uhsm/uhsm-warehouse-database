﻿


CREATE view [dbo].[OlapMISYSLocation] as

select
	 LocationCode = Location.LocationCode
	,Location = Location.Location
	,LocationTypeCode = Location.LocationTypeCode
	,LocationCategoryCode =
		coalesce(
			Location.LocationCategoryCode
			,'-1'
		)
	,LocationCategory =
		coalesce(
			LocationCategory.Description
			,'N/A'
		)
from
	WH.MISYS.Location

left join WH.dbo.EntityLookup LocationCategory
on	LocationCategory.EntityTypeCode = 'MISYSCATEGORY'
and	LocationCategory.EntityCode = Location.LocationCategoryCode

where
	exists
		(
		select
			1
		from
			(
			select
				 LocationCode = Encounter.OrderLocationCode
			from
				dbo.FactOrder Encounter

			union all

			select
				 LocationCode = Encounter.PerformLocationCode
			from
				dbo.FactOrder Encounter

			union all

			select
				 LocationCode = Encounter.ReportLocationCode
			from
				dbo.FactOrder Encounter

			) FactOrder
		where
			FactOrder.LocationCode = Location.LocationCode
		)

union all

select distinct
	 LocationCode = FactOrder.LocationCode
	,Location =
		FactOrder.LocationCode + ' - No Description'

	,LocationTypeCode = '?'

	,LocationCategoryCode =
		coalesce(
			LocationCategory.EntityCode
			,'-1'
		)

	,LocationCategory =
		coalesce(
			LocationCategory.Description
			,'N/A'
		)
from
	(
	select distinct
		 LocationCode
	from
		(
		select distinct
			 LocationCode = Encounter.OrderLocationCode
		from
			dbo.FactOrder Encounter

		union all

		select distinct
			 LocationCode = Encounter.PerformLocationCode
		from
			dbo.FactOrder Encounter

		union all

		select distinct
			 LocationCode = Encounter.ReportLocationCode
		from
			dbo.FactOrder Encounter

		) Location
	) FactOrder

left join WH.dbo.EntityXref LocationCategoryMap
on	LocationCategoryMap.EntityTypeCode = 'MISYSORDERINGLOCATION'
and	LocationCategoryMap.XrefEntityTypeCode = 'MISYSCATEGORY'
and	LocationCategoryMap.EntityCode = FactOrder.LocationCode

left join WH.dbo.EntityLookup LocationCategory
on	LocationCategory.EntityTypeCode = 'MISYSCATEGORY'
and	LocationCategory.EntityCode = LocationCategoryMap.XrefEntityCode

where
	not exists
		(
		select
			1
		from
			WH.MISYS.Location
		where
			Location.LocationCode = FactOrder.LocationCode
		)






