﻿create view OlapMISYSMetric as

select
	 MetricCode = 'O'
	,Metric = 'Ordered'

union all

select
	 MetricCode = 'P'
	,Metric = 'Performed'

union all

select
	 MetricCode = 'R'
	,Metric = 'Reported'




