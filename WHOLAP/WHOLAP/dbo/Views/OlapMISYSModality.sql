﻿create view OlapMISYSModality as

select
	ModalityCode = CaseType.CaseTypeCode
	,Modality = CaseType.CaseType
from
	WH.MISYS.CaseType
where
	exists
		(
		select
			1
		from
			dbo.FactOrder
		where
			FactOrder.ModalityCode = CaseType.CaseTypeCode
		)

union all

select
	 '-1'
	,'N/A'

