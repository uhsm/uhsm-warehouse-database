﻿

CREATE view [dbo].[OlapMISYSStaff] as

select
	 StaffCode = Physician.PhysicianCode
	,Staff = Physician.Physician

	,Radiologist =
		CONVERT(
			bit
			,case
			when Physician.RadiologistFlag = 'Y'
			then 1
			else 0
			end
		)

	,Active =
		CONVERT(
			bit
			,case
			when Physician.Active = 'Y'
			then 1
			else 0
			end
		)

from
	WH.MISYS.Physician

where
	exists
		(
		select
			1
		from
			(
			select
				 StaffCode = Encounter.OrderStaffCode
			from
				dbo.FactOrder Encounter

			union all

			select
				 StaffCode = Encounter.PerformStaffCode
			from
				dbo.FactOrder Encounter

			union all

			select
				 StaffCode = Encounter.ReportStaffCode
			from
				dbo.FactOrder Encounter

			) FactOrder
		where
			FactOrder.StaffCode = Physician.PhysicianCode
		)

union all

select distinct
	 FactOrder.StaffCode
	,Staff = FactOrder.StaffCode + ' - No Description'

	,Radiologist = 0
	,Active = 1
from
	(
	select
		 StaffCode = Encounter.OrderStaffCode
	from
		dbo.FactOrder Encounter

	union all

	select
		 StaffCode = Encounter.PerformStaffCode
	from
		dbo.FactOrder Encounter

	union all

	select
		 StaffCode = Encounter.ReportStaffCode
	from
		dbo.FactOrder Encounter

	) FactOrder

where
	not exists
		(
		select
			1
		from
			WH.MISYS.Physician
		where
			Physician.PhysicianCode = FactOrder.StaffCode
		)
and	FactOrder.StaffCode <> '-1'

union all

select
	 '-1'
	,'N/A'
	,0
	,1


