﻿
CREATE view [dbo].[OlapMISYSTechnician] as

select
	 TechnicianCode = Technician.TechCode
	,Technician = Technician.Tech
from
	WH.MISYS.Tech Technician
where
	exists
		(
		select
			1
		from
			dbo.FactOrder Encounter
		where
			Encounter.TechnicianCode = Technician.TechCode
		)

union all

select distinct
	 Encounter.TechnicianCode
	,convert(varchar, Encounter.TechnicianCode) + '- No Description'
from
	dbo.FactOrder Encounter
where
	not exists
		(
		select
			1
		from
			WH.MISYS.Tech Technician
		where
			Technician.TechCode = Encounter.TechnicianCode
		)
and	Encounter.TechnicianCode <> -1

union all

select
	 TechnicianCode = -1
	,Technician = 'N/A'
