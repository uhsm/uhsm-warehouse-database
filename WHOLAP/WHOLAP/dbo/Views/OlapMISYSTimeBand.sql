﻿CREATE view [dbo].[OlapMISYSTimeBand] as

select
	 TimeBandCode = 
		convert(
			int
			,TimeBand.EntityCode
		)

	,TimeBand = TimeBand.Description
from
	dbo.EntityLookup TimeBand
where
	TimeBand.EntityTypeCode = 'MISYSTIMEBAND'


