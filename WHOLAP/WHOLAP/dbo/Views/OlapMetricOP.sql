﻿create view [OlapMetricOP] as

select
	 MetricBase.MetricCode
	,MetricBase.Metric
	,MetricBase.MetricParentCode
from
	dbo.MetricOPBase MetricBase
