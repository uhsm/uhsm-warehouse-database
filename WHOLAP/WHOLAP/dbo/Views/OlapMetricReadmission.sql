﻿create view [dbo].[OlapMetricReadmission] as

select
	 MetricBase.MetricCode
	,MetricBase.Metric
	,MetricBase.MetricParentCode
from
	dbo.MetricReadmissionBase MetricBase

