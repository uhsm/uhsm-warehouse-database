﻿










CREATE view [dbo].[OlapOP] as

/*
2011-08-17 KD Changed First Attendance so that walk ins become either new or follow up
2013-12-04 KO Hard coded specialty code to 370C (150000185 - breast oncology) where ConsultantCode = '10000340' (Prof Howell)
2014-05-16 KO Updated derivation of AttendanceStatusCode to use DNA code already derived in WH
*/

select
	 EncounterRecno
	,SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,ReferralSourceUniqueID
	,WaitingListSourceUniqueID
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode

	,RegisteredGpPracticeCode =
		coalesce(Encounter.RegisteredGpPracticeCode, -1)

	,SiteCode = coalesce(Encounter.SiteCode, -1)
	,AppointmentDate
	,AppointmentTime
	,ClinicCode
	,AdminCategoryCode
	,SourceOfReferralCode = coalesce(Encounter.SourceOfReferralCode, -1)
	,ReasonForReferralCode
	,PriorityCode
	,FirstAttendanceFlag
	,DNACode
	,AppointmentStatusCode
	,CancelledByCode
	,TransportRequiredFlag
	,AttendanceOutcomeCode
	,AppointmentTypeCode
	,DisposalCode
	,ConsultantCode = coalesce(Encounter.ConsultantCode, -1)
	,SpecialtyCode = coalesce(
		case 
			when Encounter.ConsultantCode = '10000340' --Prof Howell
			then 150000185 --Local code 370C for breast oncology)
			else Encounter.SpecialtyCode
		end
	, -1)
	,ReferringConsultantCode
	,ReferringSpecialtyCode
	,BookingTypeCode = coalesce(Encounter.BookingTypeCode, -1)
	,CasenoteNo
	,AppointmentCreateDate
	,EpisodicGpCode
	,EpisodicGpPracticeCode = coalesce(Encounter.EpisodicGpPracticeCode, -1)
	,DoctorCode

	,PrimaryDiagnosisCode =
		coalesce(left(Encounter.PrimaryDiagnosisCode, 5), '##')

	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12

	,PrimaryOperationCode =
		coalesce(left(Encounter.PrimaryOperationCode, 5), '##')
	
	,PrimaryOperationDate
	,SecondaryOperationCode1
	,SecondaryOperationDate1
	,SecondaryOperationCode2
	,SecondaryOperationDate2
	,SecondaryOperationCode3
	,SecondaryOperationDate3
	,SecondaryOperationCode4
	,SecondaryOperationDate4
	,SecondaryOperationCode5
	,SecondaryOperationDate5
	,SecondaryOperationCode6
	,SecondaryOperationDate6
	,SecondaryOperationCode7
	,SecondaryOperationDate7
	,SecondaryOperationCode8
	,SecondaryOperationDate8
	,SecondaryOperationCode9
	,SecondaryOperationDate9
	,SecondaryOperationCode10
	,SecondaryOperationDate10
	,SecondaryOperationCode11
	,SecondaryOperationDate11
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,ReferralDate
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,AppointmentCategoryCode
	,AppointmentCreatedBy
	,AppointmentCancelDate
	,LastRevisedDate
	,LastRevisedBy
	,OverseasStatusFlag
	,PatientChoiceCode
	,ScheduledCancelReasonCode
	,PatientCancelReason
	,DischargeDate
	,QM08StartWaitDate
	,QM08EndWaitDate
	,DestinationSiteCode
	,EBookingReferenceNo
	,InterfaceCode
	,IsWardAttender
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,Created
	,Updated
	,ByWhom
	,LocalAdminCategoryCode
	,PCTCode
	,LocalityCode

	,AgeCode =
	case
	when datediff(day, DateOfBirth, Encounter.AppointmentDate) is null then 'Age Unknown'
	when datediff(day, DateOfBirth, Encounter.AppointmentDate) <= 0 then '00 Days'
	when datediff(day, DateOfBirth, Encounter.AppointmentDate) = 1 then '01 Day'
	when datediff(day, DateOfBirth, Encounter.AppointmentDate) > 1 and
	datediff(day, DateOfBirth, Encounter.AppointmentDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, DateOfBirth, Encounter.AppointmentDate)), 2) + ' Days'
	
	when datediff(day, DateOfBirth, Encounter.AppointmentDate) > 28 and
	datediff(month, DateOfBirth, Encounter.AppointmentDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.AppointmentDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, DateOfBirth, Encounter.AppointmentDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.AppointmentDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, DateOfBirth, Encounter.AppointmentDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.AppointmentDate) then 1 else 0 end > 1 and
	 datediff(month, DateOfBirth, Encounter.AppointmentDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.AppointmentDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, DateOfBirth,Encounter.AppointmentDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.AppointmentDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, DateOfBirth, Encounter.AppointmentDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.AppointmentDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.AppointmentDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.AppointmentDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, DateOfBirth, Encounter.AppointmentDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.AppointmentDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.AppointmentDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.AppointmentDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end 

	,FirstAttendanceCode =
		case
		when
			Encounter.FirstAttendanceFlag in
				(
				 9268		--New	N
				,5808		--Walk-in New	WIN
				,2003439	--Walk-in	WI
				,2003440	--Walk-in A&E	WIAE				)
				,2002180	--A&E Attendance AANDE
				)
		then 'NEW'
		else 'REVIEW'
		end

	,AttendanceStatusCode =
		case
		when
			Encounter.AppointmentStatusCode in
			(
			 357		--Attended On Time	5
			,2868		--Patient Late / Seen	6
			,2004151	--Attended	1
			)
		then 'ATTEND'

		when
			Encounter.AppointmentStatusCode in
			(
			 358		--Did Not Attend	DNA
			,2000724	--Patient Late / Not Seen	7
			,2003495	--Refused To Wait	RW
			,2003494	--Patient Left / Not seen	LNS
			)
		then 'DNA'

		when
			Encounter.AppointmentStatusCode in
			(
			 2004301	--Cancelled by Patient	CANBP
			,2870		--Cancelled By Patient	2
			)
		then 'PATCAN'

		when
			Encounter.AppointmentStatusCode in
			(
			 2004528	--Cancelled by EBS	CANBE
			,2004300	--Cancelled by Hospital	CANBH
			,2003532	--Cancelled	CANCL
			,3006508	--Referral Cancelled	REFCN
			)
		then 'HOSPCAN'

		else 'OTHER'

		end
		,AttendanceStatusCode2 =
			case
				when Encounter.DNACode in (
					5		--Attended On Time	5
					,6		--Patient Late / Seen	6
					
				)then 'ATTEND'
				when Encounter.DNACode in (
					 3		--Did Not Attend	DNA
					,7		--Patient Late / Not Seen	7
				)then 'DNA'
				when Encounter.DNACode = 2		--Cancelled By Patient	2
				then 'PATCAN'
				when Encounter.DNACode = 4		--Cancelled by Hospital	4
				then 'HOSPCAN'
				else 'OTHER'
			end
	,1 Cases

	,LengthOfWait =
		datediff(
			 day
			,Encounter.ReferralDate
			,Encounter.AppointmentDate
		)
	,Encounter.ClinicType
	,Encounter.ExcludedClinic
	,Encounter.SessionUniqueID
	,Encounter.IsWalkIn
	,ResponsibleCommissioner
	,ResponsibleCommissionerCCGOnly
	,OurActivityFlag
	,NotOurActProvCode
from
	dbo.BaseOP Encounter

--inner join WH.PAS.FirstAttendance FirstAttendance
--on	FirstAttendance.FirstAttendanceCode = Encounter.FirstAttendanceFlag
--and	FirstAttendance.FirstAttendanceType = 'Normal Booking'



















