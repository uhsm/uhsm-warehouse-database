﻿CREATE view [dbo].[OlapOPClinicType] as

select
	 ClinicTypeCode = RefValue.ReferenceValueCode
	,CliniTypeCode1 = RefValue.MainCode
	,ClinicType = RefValue.ReferenceValue
from
	WH.PAS.ReferenceValue RefValue
where
	ReferenceDomainCode = 'PRTYP'
--and	exists
--	(
--	select
--		1
--	from
--		dbo.FactOP
--	where
--		FactOP.ClinicType = RefValue.ReferenceValueCode
--	)



