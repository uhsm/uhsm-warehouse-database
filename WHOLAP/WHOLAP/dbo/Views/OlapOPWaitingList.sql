﻿
CREATE view [dbo].[OlapOPWaitingList] as

select
	 EncounterRecno
	,CensusDate
	,SourceUniqueID
	,SourceUniqueIDTypeCode
	,SourcePatientNo
	,ReferralSourceUniqueID
	,AppointmentSourceUniqueID
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,ConsultantCode = coalesce(ConsultantCode, -1)
	,SpecialtyCode = coalesce(SpecialtyCode, -1)
	,PASSpecialtyCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,SiteCode = coalesce(SiteCode, -1)
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,AdminCategoryCode
	,CancelledBy
	,DoctorCode
	,BookingTypeCode
	,CasenoteNumber
	,InterfaceCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode = coalesce(RegisteredGpPracticeCode, -1)
	,EpisodicGpCode
	,EpisodicGpPracticeCode = coalesce(EpisodicGpPracticeCode, -1)
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,ReferralDate
	,BookedDate
	,BookedTime
	,AppointmentDate
	,AppointmentTypeCode
	,AppointmentStatusCode
	,AppointmentCategoryCode
	,QM08StartWaitDate
	,QM08EndWaitDate
	,AppointmentTime
	,ClinicCode = coalesce(ClinicCode, 'N/A')
	,SourceOfReferralCode = coalesce(SourceOfReferralCode, -1)
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,ArchiveFlag
	,FuturePatientCancelDate
	,AdditionFlag
	,CountOfDNAs
	,CountOfHospitalCancels
	,CountOfPatientCancels
	,KornerWait
	,BreachDate
	,BreachDays
	,NationalBreachDate
	,NationalBreachDays
	,Created
	,Updated
	,ByWhom
	,DateOnWaitingList
	,IntendedPrimaryOperationCode
	,Operation

	,AgeCode =
		case
		when datediff(day, DateOfBirth, Encounter.CensusDate) is null then 'Age Unknown'
		when datediff(day, DateOfBirth, Encounter.CensusDate) <= 0 then '00 Days'
		when datediff(day, DateOfBirth, Encounter.CensusDate) = 1 then '01 Day'
		when datediff(day, DateOfBirth, Encounter.CensusDate) > 1 and
		datediff(day, DateOfBirth, Encounter.CensusDate) <= 28 then
			right('0' + Convert(Varchar,datediff(day, DateOfBirth, Encounter.CensusDate)), 2) + ' Days'
		
		when datediff(day, DateOfBirth, Encounter.CensusDate) > 28 and
		datediff(month, DateOfBirth, Encounter.CensusDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end
			 < 1 then 'Between 28 Days and 1 Month'
		when datediff(month, DateOfBirth, Encounter.CensusDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end = 1
			then '01 Month'
		when datediff(month, DateOfBirth, Encounter.CensusDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end > 1 and
		 datediff(month, DateOfBirth, Encounter.CensusDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end <= 23
		then
		right('0' + Convert(varchar,datediff(month, DateOfBirth,Encounter.CensusDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end), 2) + ' Months'
		when datediff(yy, DateOfBirth, Encounter.CensusDate) - 
		(
		case 
		when	(datepart(m, DateOfBirth) > datepart(m, Encounter.CensusDate)) 
		or
			(
				datepart(m, DateOfBirth) = datepart(m, Encounter.CensusDate) 
			And	datepart(d, DateOfBirth) > datepart(d, Encounter.CensusDate)
			) then 1 else 0 end
		) > 99 then '99+'
		else right('0' + convert(varchar, datediff(yy, DateOfBirth, Encounter.CensusDate) - 
		(
		case 
		when	(datepart(m, DateOfBirth) > datepart(m, Encounter.CensusDate)) 
		or
			(
				datepart(m, DateOfBirth) = datepart(m, Encounter.CensusDate) 
			And	datepart(d, DateOfBirth) > datepart(d, Encounter.CensusDate)
			) then 1 else 0 end
		)), 2) + ' Years'

		end 

	,Cases = 1

	,convert(int, Encounter.KornerWait) LengthOfWait

	,WeeksWaiting = convert(int, Encounter.KornerWait /7 )

	,WithAppointment =
		case
		when Encounter.AppointmentDate is null 
		then 0
		else 1
		end

	,DurationCode =
		case
		when Encounter.KornerWait < 0 then 'NA'
		else
			coalesce(
				right('0' +
				convert(varchar, 
				case
				when convert(int, Encounter.KornerWait /7 ) > 51 then 52
				else convert(int, Encounter.KornerWait /7 )
				end), 2)
				,'NA'
			)
		end

	,WaitTypeCode = '10' --waiter

	,StatusCode = '40' --O/P Active

	,CategoryCode = '30' --reportable outpatient


from
	dbo.BaseOPWaitingList Encounter

