﻿CREATE view [dbo].[OlapOperation] as

select distinct
	 Operation.OperationCode

	,Operation = 
		Operation.OperationCode + ' - ' + Operation.Operation
from
	WH.PAS.Operation Operation

union allselect distinct
	 FactTheatreOperation.PrimaryProcedureCode

	,Operation = 
		FactTheatreOperation.PrimaryProcedureCode + ' - No Description'
from
	dbo.FactTheatreOperation
where
	not exists
	(
	select
		1
	from
		WH.PAS.Operation Operation
	where
		Operation.OperationCode = FactTheatreOperation.PrimaryProcedureCode
	)
and	FactTheatreOperation.PrimaryProcedureCode <> '##'

union all

select
	'##',
	'N/A'
