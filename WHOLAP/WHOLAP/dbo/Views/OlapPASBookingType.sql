﻿


create view [dbo].[OlapPASBookingType] as

select
	 BookingTypeCode
	,BookingType
	,BookingTypeLocalCode
from
	WH.PAS.BookingType

union all

select
	 BookingTypeCode = -1
	,BookingType = 'Unknown'
	,BookingTypeLocalCode = 'N/A'




