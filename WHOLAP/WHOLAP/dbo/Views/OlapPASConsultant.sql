﻿
CREATE view [dbo].[OlapPASConsultant] as

select
	 ConsultantCode =
		convert(
			 int
			,PASConsultant.ConsultantCode
		)

	,Consultant = 
		PASConsultant.Consultant +
		' (' + 
		coalesce(
			 PASConsultant.NationalConsultantCode
			,convert(varchar, PASConsultant.ConsultantCode)
		) + ')'

	,NationalConsultantCode =
		coalesce(
			 PASConsultant.NationalConsultantCode
			,'N/A'
		)

from
	WH.PAS.Consultant PASConsultant

inner join dbo.EntityXref ActiveConsultant
on	ActiveConsultant.EntityCode = PASConsultant.ConsultantCode
and	ActiveConsultant.EntityTypeCode = 'OLAPPASCONSULTANT'
and	ActiveConsultant.XrefEntityTypeCode = 'OLAPPASCONSULTANT'

union all

select
	 ActiveConsultant.EntityCode
	,convert(varchar, ActiveConsultant.EntityCode) + ' - No Description'
	,'N/A'
from
	dbo.EntityXref ActiveConsultant
where
	ActiveConsultant.EntityTypeCode = 'OLAPPASCONSULTANT'
and	ActiveConsultant.XrefEntityTypeCode = 'OLAPPASCONSULTANT'
and	not exists
	(
	select
		1
	from
		WH.PAS.Consultant PASConsultant
	where
		PASConsultant.ConsultantCode = ActiveConsultant.EntityCode	
	)
and	ActiveConsultant.EntityCode <> '-1'

union all

select
	 -1
	,'Not Specified'
	,'N/A'

