﻿

CREATE view [dbo].[OlapPASOperation] as

select
	 OperationCode = Operation.CodingCode

	,Operation = 
		Operation.CodingLocalCode + ' - ' + Operation.Coding
from
	WH.PAS.Coding Operation

inner join dbo.EntityXref Active
on	Active.EntityCode = Operation.CodingCode
and	Active.EntityTypeCode = 'OLAPPASOPERATION'
and	Active.XrefEntityTypeCode = 'OLAPPASOPERATION'

union all

select
	 Active.EntityCode
	,case
	when Active.EntityCode = -1
	then 'Unknown'
	else Active.EntityCode + ' - No Description'
	end
from
	dbo.EntityXref Active
where
	Active.EntityTypeCode = 'OLAPPASOPERATION'
and	Active.XrefEntityTypeCode = 'OLAPPASOPERATION'
and	not exists
	(
	select
		1
	from
		WH.PAS.Coding Operation
	where
		Active.EntityCode = Operation.CodingCode
	)





