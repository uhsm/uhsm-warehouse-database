﻿
CREATE view [dbo].[OlapPASOrganisation]
as

select
	 PASOrganisation.OrganisationCode
	,PASOrganisation.Organisation
	,PASOrganisation.ParentOrganisationCode
	,PASOrganisation.OrganisationLocalCode

	,OrganisationTypeCode = 
		coalesce(
			PASOrganisation.OrganisationTypeCode
			,-1
		)

	,OrganisationType =
		coalesce(
			 OrganisationType.ReferenceValue
			,'Unknown'
		)
from
	WH.PAS.Organisation PASOrganisation

left join WH.PAS.ReferenceValue OrganisationType
on	OrganisationType.ReferenceValueCode = PASOrganisation.OrganisationTypeCode

union all

select
	-1
	,'Not Specified'
	,null
	,'N/S'
	,-1
	,'Unknown'


union all

select
	-2
	,'Not Applicable'
	,null
	,'N/A'
	,-2
	,'Not Applicable'


