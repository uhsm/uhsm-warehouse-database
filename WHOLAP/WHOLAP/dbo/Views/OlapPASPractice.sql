﻿






CREATE view [dbo].[OlapPASPractice] as

select
	 PracticeCode = 
		IPMPractice.OrganisationCode

	,Practice =
		IPMPractice.OrganisationLocalCode + ' - ' + coalesce(IPMPractice.Organisation, 'Unknown')

	,PCTCode =
		coalesce(PCT.OrganisationLocalCode,'N/A')

	,PCT =
		coalesce(PCT.OrganisationLocalCode + ' - ' + PCT.Organisation, 'Unknown')
		
	,HealthAuthorityCode =
		coalesce(
			 HealthAuthority.OrganisationLocalCode
			,'N/A'
		)

	,HealthAuthority =
		coalesce(HealthAuthority.OrganisationLocalCode + ' - ' + HealthAuthority.Organisation, 'Unknown')


	,LocalPCT =
		convert(varchar(255), 
			case
			when exists
				(
				select
					1
				from
					WH.dbo.EntityLookup LocalPCT
				where
					LocalPCT.EntityCode = PCT.OrganisationLocalCode
				and	LocalPCT.EntityTypeCode = 'LOCALPCT'
				)
			then coalesce(PCT.OrganisationLocalCode + ' - ' + PCT.Organisation, 'Unknown')
			else 'Other'
			end
		)

	,RTTPCTCode =
		coalesce(
			 RTTPCT.XrefEntityCode
			,PCT.OrganisationLocalCode
			,'X98'
		)
from
	WH.PAS.Organisation IPMPractice

inner join dbo.EntityXref ActivePractice
on	ActivePractice.EntityCode = IPMPractice.OrganisationCode
and	ActivePractice.EntityTypeCode = 'OLAPPASPRACTICE'
and	ActivePractice.XrefEntityTypeCode = 'OLAPPASPRACTICE'

left join WH.PAS.Organisation PCT
on	PCT.OrganisationCode = IPMPractice.ParentOrganisationCode

left join WH.PAS.Organisation HealthAuthority
on	HealthAuthority.OrganisationCode = PCT.ParentOrganisationCode

left join WH.dbo.EntityXref RTTPCT
on	RTTPCT.EntityCode = PCT.OrganisationCode
and	RTTPCT.EntityTypeCode = 'PCT'
and	RTTPCT.XrefEntityTypeCode = 'RTTPCT'

union

select
	 PracticeCode = -1
	,Practice = 'Not Specified'
	,PCTCode = 'N/A'
	,PCT = 'Unknown'
	,HealthAuthorityCode = 'N/A'
	,HealthAuthority = 'Unknown'
	,LocalPCT = 'Other'
	,RTTPCTCode ='X98'

union

select
	 PracticeCode = convert(int, ActivePractice.EntityCode)
	,Practice = ActivePractice.EntityCode + ' - No Description'
	,PCTCode = 'N/A'
	,PCT = 'Unknown'
	,HealthAuthorityCode = 'N/A'
	,HealthAuthority = 'Unknown'
	,LocalPCT = 'Other'
	,RTTPCTCode ='X98'
from
	dbo.EntityXref ActivePractice
where
	ActivePractice.EntityTypeCode = 'OLAPPASPRACTICE'
and	ActivePractice.XrefEntityTypeCode = 'OLAPPASPRACTICE'
and	not exists
	(
	select
		1
	from
		WH.PAS.Organisation Practice
	where
		ActivePractice.EntityCode = convert(varchar, Practice.OrganisationCode)
	)
and	ActivePractice.EntityCode <> -1








