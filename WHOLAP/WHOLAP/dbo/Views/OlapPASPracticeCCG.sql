﻿







CREATE view [dbo].[OlapPASPracticeCCG] as

--K Oakden updated 21-05-2013 to include CCG
select
	 PracticeCode = 
		IPMPractice.OrganisationCode

	,Practice =
		IPMPractice.OrganisationLocalCode + ' - ' + coalesce(IPMPractice.Organisation, 'Unknown')

	,PracticeCloseDate = coalesce(ODSPractice.CloseDate, '20990101')

	,PCTCode =
		coalesce(PCT.OrganisationLocalCode,'N/A')

	,PCT =
		coalesce(PCT.OrganisationLocalCode + ' - ' + PCT.Organisation, 'Unknown')

	,PCTName =
		coalesce(PCT.Organisation, 'Unknown')
		
	,CCGCode =
		case --Non-English code so not present in ODS data
			when left(PCT.OrganisationLocalCode, 1) in ('Z','Y','S','Q') 
				then coalesce(PCT.OrganisationLocalCode,'N/A')
			--Otherwise update where practice not closed prior to CCG existence
			when coalesce(ODSPractice.CloseDate, '20990101') > '20130331'
				then coalesce(CCG.OrganisationCode,'N/A')
			else 'N/A'
		end
		
	,CCG =
		case --Non-English code so not present in ODS data
			when left(PCT.OrganisationLocalCode, 1) in ('Z','Y','S','Q') 
				then coalesce(PCT.OrganisationLocalCode + ' - ' + PCT.Organisation, 'Unknown')
			when coalesce(ODSPractice.CloseDate, '20990101') > '20130331'
				then coalesce(CCG.OrganisationCode + ' - ' + CCG.Organisation,'Unknown')
			else 'Practice Closed'
		end

	,CCGName =
		case --Non-English code so not present in ODS data
			when left(PCT.OrganisationLocalCode, 1) in ('Z','Y','S','Q') 
				then coalesce(PCT.Organisation, 'Unknown')
			when coalesce(ODSPractice.CloseDate, '20990101') > '20130331'
				then coalesce(CCG.Organisation,'Unknown')
			else 'Practice Closed'
		end
		
	,HealthAuthorityCode =
		coalesce(
			 HealthAuthority.OrganisationLocalCode
			,'N/A'
		)

	,HealthAuthority =
		coalesce(HealthAuthority.OrganisationLocalCode + ' - ' + HealthAuthority.Organisation, 'Unknown')


	,LocalPCT =
		convert(varchar(255), 
			case
			when exists
				(
				select
					1
				from
					WH.dbo.EntityLookup LocalPCT
				where
					LocalPCT.EntityCode = PCT.OrganisationLocalCode
				and	LocalPCT.EntityTypeCode = 'LOCALPCT'
				)
			then coalesce(PCT.OrganisationLocalCode + ' - ' + PCT.Organisation, 'Unknown')
			else 'Other'
			end
		)

	,RTTPCTCode =
		coalesce(
			 RTTPCT.XrefEntityCode
			,PCT.OrganisationLocalCode
			,'X98'
		)
from
	WH.PAS.Organisation IPMPractice

inner join dbo.EntityXref ActivePractice
on	ActivePractice.EntityCode = IPMPractice.OrganisationCode
and	ActivePractice.EntityTypeCode = 'OLAPPASPRACTICE'
and	ActivePractice.XrefEntityTypeCode = 'OLAPPASPRACTICE'

left join WH.PAS.Organisation PCT
on	PCT.OrganisationCode = IPMPractice.ParentOrganisationCode

left join OrganisationCCG.dbo.Practice ODSPractice
on ODSPractice.OrganisationCode = IPMPractice.OrganisationLocalCode

left join OrganisationCCG.dbo.CCG CCG
on CCG.OrganisationCode = ODSPractice.ParentOrganisationCode

left join WH.PAS.Organisation HealthAuthority
on	HealthAuthority.OrganisationCode = PCT.ParentOrganisationCode

left join WH.dbo.EntityXref RTTPCT
on	RTTPCT.EntityCode = PCT.OrganisationCode
and	RTTPCT.EntityTypeCode = 'PCT'
and	RTTPCT.XrefEntityTypeCode = 'RTTPCT'

union

select
	 PracticeCode = -1
	,Practice = 'Not Specified'
	,PracticeCloseDate = '20990101'
	,PCTCode = 'N/A'
	,PCT = 'Unknown'
	,PCTName = 'Unknown'
	,CCGCode = 'N/A'
	,CCG = 'Unknown'
	,CCGName = 'Unknown'
	,HealthAuthorityCode = 'N/A'
	,HealthAuthority = 'Unknown'
	,LocalPCT = 'Other'
	,RTTPCTCode ='X98'

union

select
	 PracticeCode = convert(int, ActivePractice.EntityCode)
	,Practice = ActivePractice.EntityCode + ' - No Description'
	,PracticeCloseDate = '20990101'
	,PCTCode = 'N/A'
	,PCT = 'Unknown'
	,PCTName = 'Unknown'
	,CCGCode = 'N/A'
	,CCG = 'Unknown'
	,CCGName = 'Unknown'
	,HealthAuthorityCode = 'N/A'
	,HealthAuthority = 'Unknown'
	,LocalPCT = 'Other'
	,RTTPCTCode ='X98'
from
	dbo.EntityXref ActivePractice
where
	ActivePractice.EntityTypeCode = 'OLAPPASPRACTICE'
and	ActivePractice.XrefEntityTypeCode = 'OLAPPASPRACTICE'
and	not exists
	(
	select
		1
	from
		WH.PAS.Organisation Practice
	where
		ActivePractice.EntityCode = convert(varchar, Practice.OrganisationCode)
	)
and	ActivePractice.EntityCode <> -1









