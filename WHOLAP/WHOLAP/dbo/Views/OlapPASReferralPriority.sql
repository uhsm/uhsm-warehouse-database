﻿
Create view [dbo].[OlapPASReferralPriority] as

select
	 ReferralPriorityCode = RefValue.ReferenceValueCode
	,ReferralPriorityCode1 = RefValue.MainCode
	,ReferralPriority = RefValue.ReferenceValue
from
	WH.PAS.ReferenceValue RefValue
where
	ReferenceDomainCode = 'PRITY'
and	exists
	(
	select
		1
	from
		dbo.FactRF
	where
		FactRF.ReferralPriorityCode = RefValue.ReferenceValueCode
	)

