﻿



CREATE view [dbo].[OlapPASServicePoint]
as

select
	 ServicePointCode

	,ServicePoint

	,ServicePointLocalCode

	,OrganisationCode

	,WardTypeCode =
		coalesce(
			 WardType.XrefEntityCode
			,'X'
		)

	,WardType =
		case WardType.XrefEntityCode
		when 'S' then 'Scheduled'
		when 'U' then 'Unscheduled'
		else 'N/A'
		end
from
(
select
	 ServicePointCode =
		convert(
			 int
			,PASServicePoint.ServicePointCode
		)

	,PASServicePoint.ServicePoint

	,ServicePointLocalCode

	,OrganisationCode =
		convert(
			 int
			,PASServicePoint.SiteCode
		)

from
	WH.PAS.ServicePoint PASServicePoint


union all

--this needs to be fixed properly with an "ActiveServicePoint" table
select distinct
	 FactOP.SiteCode
	,convert(varchar, FactOP.SiteCode) + ' - No Description'
	,'N/A'
	,-1
from
	dbo.FactOP
where
	not exists
	(
	select
		1
	from
		WH.PAS.ServicePoint ServicePoint
	where
		ServicePoint.ServicePointCode = FactOP.SiteCode	
	)

union all

--this needs to be fixed properly with an "ActiveServicePoint" table
select distinct
	 BaseAPCWardStay.WardCode
	,convert(varchar, BaseAPCWardStay.WardCode) + ' - No Description'
	,'N/A'
	,-1
from
	dbo.BaseAPCWardStay
where
	not exists
	(
	select
		1
	from
		WH.PAS.ServicePoint ServicePoint
	where
		ServicePoint.ServicePointCode = BaseAPCWardStay.WardCode	
	)


union all

--this needs to be fixed properly with an "ActiveServicePoint" table
select distinct
	 FactOPSession.ServicePointUniqueID
	,convert(varchar, FactOPSession.ServicePointUniqueID) + ' - No Description'
	,'N/A'
	,-1
from
	dbo.FactOPSession
where
	not exists
	(
	select
		1
	from
		WH.PAS.ServicePoint ServicePoint
	where
		ServicePoint.ServicePointCode = FactOPSession.ServicePointUniqueID	
	)

union all

select
	-1
	,'Not Specified'
	,'N/A'
	,-1

) ServicePoint

left join dbo.EntityXref WardType
on	WardType.EntityCode = ServicePoint.ServicePointCode
and	WardType.EntityTypeCode = 'WARD'
and	WardType.XrefEntityTypeCode = 'WARDTYPE'


