﻿CREATE view [OlapPASSite]
as

select
	 PASSite.SiteCode
	,PASSite.Site
	,WHSite.Colour
	,WHSite.Sequence
	,PASSite.MappedSiteCode
from
	WH.PAS.Site PASSite

inner join WH.WH.Site WHSite
on	WHSite.SiteCode = PASSite.MappedSiteCode
