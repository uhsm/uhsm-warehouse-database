﻿


CREATE view [dbo].[OlapPASSourceOfReferral] as

select
	 SourceOfReferralCode
	,SourceOfReferral
	,NationalSourceOfReferralCode
	,ReferralType
	,ReportableFlag
	,Reportable
from
	(
	select
		 SourceOfReferral.SourceOfReferralCode
		,SourceOfReferral.SourceOfReferral
		,SourceOfReferral.NationalSourceOfReferralCode

		,ReferralType =
		case
		when SourceOfReferral.NationalSourceOfReferralCode in ('3', '03')
		then 'Gp Referral'
		else 'Other Referral'
		end 

		,ReportableFlag = coalesce(SourceOfReferral.ReportableFlag, 0)

		,Reportable =
		case coalesce(SourceOfReferral.ReportableFlag, 0)
		when 0
		then 'Non-reportable'
		else 'Reportable'
		end 
	from
		WH.PAS.SourceOfReferral SourceOfReferral


	union all

	select
		-1
		,'Unknown'
		,'N/A'
		,'Unknown'
		,1
		,'Reportable'
	) SourceOfReferral
where
	exists
	(
	select
		1
	from
		dbo.FactOPWaitingList Encounter
	where
		Encounter.SourceOfReferralCode = SourceOfReferral.SourceOfReferralCode
	)

or	exists
	(
	select
		1
	from
		dbo.FactOP Encounter
	where
		Encounter.SourceOfReferralCode = SourceOfReferral.SourceOfReferralCode
	)


or	exists
	(
	select
		1
	from
		dbo.FactRF Encounter
	where
		Encounter.SourceOfReferralCode = SourceOfReferral.SourceOfReferralCode
	)

