﻿


CREATE view [dbo].[OlapPASSpecialty]
as

select
	 SpecialtyCode =
		convert(
			 int
			,PASSpecialty.SpecialtyCode
		)

	,PASSpecialty.Specialty

	,PASSpecialty.LocalSpecialtyCode

	,NationalSpecialtyCode = coalesce(PASSpecialty.NationalSpecialtyCode, PASSpecialty.LocalSpecialtyCode)

	,NationalSpecialty =
		coalesce(
			 Specialty.Specialty
			,coalesce(PASSpecialty.NationalSpecialtyCode, PASSpecialty.LocalSpecialtyCode) + ' - No Description'
			,'Unknown'
		)

	,PASSpecialty.TreatmentFunctionFlag

	,RTTSpecialtyCode =
		coalesce(
			 RTTSpecialty.XrefEntityCode
			,'X01'
		)

	,RTTSpecialty =
		case
		when RTTSpecialty.XrefEntityCode is null
		then 'Other'
		else
			coalesce(
				 Specialty.Specialty
				,coalesce(PASSpecialty.NationalSpecialtyCode, PASSpecialty.LocalSpecialtyCode) + ' - No Description'
				,'Unknown'
			)
		end

	,OutpatientMaximumWeeksWait =
		cast(
			OutpatientMaximumWeeksWait.Description
			as int
		)

from
	WH.PAS.Specialty PASSpecialty

inner join dbo.EntityXref ActiveSpecialty
on	ActiveSpecialty.EntityCode = PASSpecialty.SpecialtyCode
and	ActiveSpecialty.EntityTypeCode = 'OLAPPASSPECIALTY'
and	ActiveSpecialty.XrefEntityTypeCode = 'OLAPPASSPECIALTY'

left join WH.WH.Specialty Specialty
on	Specialty.SpecialtyCode = coalesce(PASSpecialty.NationalSpecialtyCode, PASSpecialty.LocalSpecialtyCode)

left join dbo.EntityXref RTTSpecialty
on	RTTSpecialty.EntityCode = PASSpecialty.NationalSpecialtyCode
and	RTTSpecialty.EntityTypeCode = 'NATIONALSPECIALTY'
and	RTTSpecialty.XrefEntityTypeCode = 'RTTSPECIALTY'

left join dbo.EntityLookup OutpatientMaximumWeeksWait
on	OutpatientMaximumWeeksWait.EntityCode = PASSpecialty.SpecialtyCode
and	OutpatientMaximumWeeksWait.EntityTypeCode = 'OUTPATIENTMAXIMUMWAITWEEKS'


union all

select
	 ActiveSpecialty.EntityCode
	,case
	when ActiveSpecialty.EntityCode = -1
	then 'Unknown'
	else ActiveSpecialty.EntityCode + ' - No Description'
	end

	,'999'
	,'999'
	,'Unknown'
	,convert(bit, 0)
	,'X01'
	,RTTSpecialty = 'Other'
	,OutpatientMaximumWeeksWait = null
from
	dbo.EntityXref ActiveSpecialty
where
	ActiveSpecialty.EntityTypeCode = 'OLAPPASSPECIALTY'
and	ActiveSpecialty.XrefEntityTypeCode = 'OLAPPASSPECIALTY'
and	not exists
	(
	select
		1
	from
		WH.PAS.Specialty Specialty
	where
		ActiveSpecialty.EntityCode = Specialty.SpecialtyCode
	)




