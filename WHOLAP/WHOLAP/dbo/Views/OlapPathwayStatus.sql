﻿
CREATE view [dbo].[OlapPathwayStatus] as

select
	 PathwayStatus.PathwayStatusCode
	,PathwayStatus.PathwayStatus
	,PathwayStatus.NationalPathwayStatusCode
	,NationalPathwayStatus.NationalPathwayStatus
from
	WH.RTT.PathwayStatus PathwayStatus

inner join WH.RTT.NationalPathwayStatus NationalPathwayStatus
on	NationalPathwayStatus.NationalPathwayStatusCode = PathwayStatus.NationalPathwayStatusCode

