﻿
CREATE view [dbo].[OlapPatientCategory] as

select
	 PatientCategoryCode = EntityCode
	,PatientCategory = Description

	--,PatientCategoryTypeCode =
	--	case
	--	when EntityCode = 'NE'
	--	then 'U'
	--	else 'S'
	--	end

	--,PatientCategoryType =
	--	case
	--	when EntityCode = 'NE'
	--	then 'Unscheduled'
	--	else 'Scheduled'
	--	end
from
	dbo.EntityLookup
where
	EntityTypeCode = 'OLAPPATIENTCATEGORY'

