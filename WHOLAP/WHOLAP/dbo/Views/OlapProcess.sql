﻿CREATE view [OlapProcess] as

select
	 Process.ProcessCode
	,Process.Process
	,Process.ParentProcessCode
	,ProcessSource.ProcessSourceCode
	,ProcessSource.ProcessSource
from
	WH.dbo.AuditProcess Process

inner join WH.dbo.AuditProcessSource ProcessSource
on	ProcessSource.ProcessSourceCode = Process.ProcessSourceCode
