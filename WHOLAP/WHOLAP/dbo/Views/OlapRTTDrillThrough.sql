﻿

CREATE  view [dbo].[OlapRTTDrillThrough] as

select
	 BaseRTT.CensusDate
	,BaseRTT.SourceEncounterRecno
	,BaseRTT.SourceCode
	,BaseRTT.PathwayStatusCode
	,BaseRTT.KeyDate
	,BaseRTT.ClockStartDate
	,BaseRTT.ClockStopDate
	,BaseRTT.BreachDate
	,BaseRTT.Cases
	,BaseRTT.ReferralDate
	,BaseRTT.SocialSuspensionDays
	,BaseRTT.MappedSpecialtyCode
	,BaseRTT.ReferralSourceUniqueID
	,BaseRTT.WaitingListSourceUniqueID
	,BaseRTT.SourcePatientNo
	,BaseRTT.DistrictNo
	,BaseRTT.PatientForename
	,BaseRTT.PatientSurname
	,BaseRTT.DateOfBirth
	,BaseRTT.DateOfDeath
	,BaseRTT.SexCode
	,BaseRTT.NHSNumber
	,BaseRTT.Postcode
	,BaseRTT.PatientAddress1
	,BaseRTT.PatientAddress2
	,BaseRTT.PatientAddress3
	,BaseRTT.PatientAddress4
	,BaseRTT.DateOnWaitingList
	,BaseRTT.RegisteredGpCode
	,BaseRTT.RegisteredGpPracticeCode
	,BaseRTT.ConsultantCode
	,BaseRTT.SpecialtyCode
	,BaseRTT.AdminCategoryCode
	,BaseRTT.PrimaryDiagnosisCode
	,BaseRTT.PrimaryOperationCode
	,BaseRTT.PrimaryOperationDate
	,BaseRTT.RTTPathwayID
	,BaseRTT.AdmissionDate
	,BaseRTT.DischargeDate
	,BaseRTT.StartSiteCode
	,BaseRTT.StartWardTypeCode
	,BaseRTT.EndSiteCode
	,BaseRTT.EndWardTypeCode
	,BaseRTT.AdmissionMethodCode
	,BaseRTT.PatientClassificationCode
	,BaseRTT.ManagementIntentionCode
	,BaseRTT.DischargeMethodCode
	,BaseRTT.DischargeDestinationCode
	,BaseRTT.PriorityCode
	,BaseRTT.ClinicCode
	,BaseRTT.SourceOfReferralCode
	,BaseRTT.AppointmentDate
	,BaseRTT.FirstAttendanceFlag
	,BaseRTT.DNACode
	,BaseRTT.AttendanceOutcomeCode
	,BaseRTT.DisposalCode
	,BaseRTT.PreviousAppointmentDate
	,BaseRTT.PreviousAppointmentStatusCode
	,BaseRTT.FuturePatientCancelDate
	,BaseRTT.CountOfDNAs
	,BaseRTT.CountOfPatientCancels
	,BaseRTT.NextAppointmentDate
	,BaseRTT.LastDnaOrPatientCancelledDate
	,BaseRTT.DateOnFutureOPWaitingList

	,ReviewedFlag = coalesce(RTTEncounter.ReviewedFlag, 0)

	,ReviewedBy = 
		replace(
			RTTEncounter.LastUser
			,'UHSM\'
			,''
		)

	,ReviewedTime = 
		coalesce(
			 RTTEncounter.Updated
			,RTTEncounter.Created
		)

	,SharedBreachFlag =  
		coalesce(
			 RTTEncounter.SharedBreachFlag
			,0
		)
from
	dbo.BaseRTT

left join WH.RTT.Encounter RTTEncounter
on	RTTEncounter.SourceCode = BaseRTT.SourceCode
and	RTTEncounter.SourceRecno = BaseRTT.SourceEncounterRecno



