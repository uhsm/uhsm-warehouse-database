﻿



CREATE view [dbo].[OlapReadmissionEncounter] as

SELECT
	 Readmission.ReadmissionRecno
	,ReadmissionEncounter.AdmissionDate ReadmissionAdmissionDate
	,ReadmissionEncounter.ConsultantCode ReadmissionConsultantCode
	,ReadmissionEncounter.SpecialtyCode ReadmissionSpecialtyCode
	,ReadmissionEncounter.RegisteredGpPracticeCode ReadmissionRegisteredGpPracticeCode
	,ReadmissionEncounter.AdmissionMethodCode ReadmissionAdmissionMethodCode
	,Cases = 1

	,Readmission.PreviousAdmissionRecno
	,PreviousAdmissionEncounter.DischargeDate PreviousAdmissionDischargeDate
	,PreviousAdmissionEncounter.ConsultantCode PreviousAdmissionConsultantCode
	,PreviousAdmissionEncounter.SpecialtyCode PreviousAdmissionSpecialtyCode
	,PreviousAdmissionEncounter.RegisteredGpPracticeCode PreviousAdmissionRegisteredGpPracticeCode
	,PreviousAdmissionEncounter.AdmissionMethodCode PreviousAdmissionAdmissionMethodCode

	,ReadmissionIntervalCode =
		case
		when
			datediff(
				 day
				,PreviousAdmissionEncounter.DischargeDate
				,ReadmissionEncounter.AdmissionDate
			) > 90
		then '91'
		else
			right(
				'0' + 
				convert(
					 varchar
					,datediff(
						 day
						,PreviousAdmissionEncounter.DischargeDate
						,ReadmissionEncounter.AdmissionDate
					)
				)
				,2
			)
		end 

	,ReadmissionIntervalDays =
		datediff(
			 day
			,PreviousAdmissionEncounter.DischargeDate
			,ReadmissionEncounter.AdmissionDate
		)
FROM
	dbo.Readmission Readmission

inner join dbo.BaseAPC ReadmissionEncounter
on	ReadmissionEncounter.EncounterRecno = Readmission.ReadmissionRecno

inner join dbo.BaseAPC AdmittingPreviousAdmissionEncounter
on	AdmittingPreviousAdmissionEncounter.EncounterRecno = Readmission.PreviousAdmissionRecno

inner join dbo.BaseAPC PreviousAdmissionEncounter
on	PreviousAdmissionEncounter.ProviderSpellNo = AdmittingPreviousAdmissionEncounter.ProviderSpellNo
and	not exists
		(
		select
			1
		from
			dbo.BaseAPC Previous
		where
			Previous.ProviderSpellNo = PreviousAdmissionEncounter.ProviderSpellNo
		and	(
				Previous.EpisodeStartTime > PreviousAdmissionEncounter.EpisodeStartTime
			or	(
					Previous.EpisodeStartTime = PreviousAdmissionEncounter.EpisodeStartTime
				and	Previous.EncounterRecno > PreviousAdmissionEncounter.EncounterRecno
				)
			)
		)

where
	datediff(day, PreviousAdmissionEncounter.DischargeDate, ReadmissionEncounter.AdmissionDate) between 0 and 365




