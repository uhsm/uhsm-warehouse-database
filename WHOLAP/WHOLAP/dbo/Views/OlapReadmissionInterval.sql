﻿

CREATE view [dbo].[OlapReadmissionInterval] as

select
	ReadmissionIntervalCode
	,ReadmissionInterval

--	,case
--	when ReadmissionIntervalCode < '07' then '06'
--	when ReadmissionIntervalCode < '14' then '13'
--	when ReadmissionIntervalCode < '21' then '20'
--	when ReadmissionIntervalCode < '28' then '27'
--	else '28'
--	end ZeroToCurrentCode
--
--	,case
--	when ReadmissionIntervalCode < '07' then '0 - 6 Days'
--	when ReadmissionIntervalCode < '14' then '0 - 13 Days'
--	when ReadmissionIntervalCode < '21' then '0 - 20 Days'
--	when ReadmissionIntervalCode < '28' then '0 - 27 Days'
--	else '28+ Days'
--	end ZeroToCurrent

	,ZeroToCurrentCode = 1
	,ZeroToCurrent = 'Current'

	,case
	when ReadmissionIntervalCode < '07' then '06'
	when ReadmissionIntervalCode between '07' and '13' then '13'
	when ReadmissionIntervalCode between '14' and '20' then '20'
	when ReadmissionIntervalCode between '21' and '27' then '27'
	else '28'
	end WeekBandCode

	,case
	when ReadmissionIntervalCode < '07' then '0 - 6 Days'
	when ReadmissionIntervalCode between '07' and '13' then '7 - 13 Days'
	when ReadmissionIntervalCode between '14' and '20' then '14 - 20 Days'
	when ReadmissionIntervalCode between '21' and '27' then '21 - 27 Days'
	else '28+ Days'
	end WeekBand

from
	ReadmissionIntervalBase



