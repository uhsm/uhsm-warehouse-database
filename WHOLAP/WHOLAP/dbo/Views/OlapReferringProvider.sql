﻿
CREATE view [dbo].[OlapReferringProvider] as

select distinct
	 FactRTT.ReferringProviderCode

	,ReferringProvider = 
		case
		when FactRTT.ReferringProviderCode = '##' then 'Unknown'
		when FactRTT.ReferringProviderCode = 'X09' then 'Choose and Book'
		else coalesce(Provider.Organisation, FactRTT.ReferringProviderCode + ' - No Description')
		end
from
	dbo.FactRTT

left join WH.PAS.Organisation Provider
on	Provider.OrganisationLocalCode = FactRTT.ReferringProviderCode

