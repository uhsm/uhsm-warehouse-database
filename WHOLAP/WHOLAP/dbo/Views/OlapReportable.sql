﻿create view [OlapReportable] as

select
	 ReportableFlag = convert(bit, 1)
	,Reportable = 'Reportable'

union all

select
	 ReportableFlag = convert(bit, 0)
	,Reportable = 'Non-Reportable'
