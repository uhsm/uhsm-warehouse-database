﻿
CREATE view [dbo].[OlapSCRAge] as

select distinct
	 AgeCode = AgeAtDiagnosis
	,AgeAtDiagnosis = cast(AgeAtDiagnosis as varchar)
from
	dbo.BaseSCRReferralTreatment

union all

select
	-1
	,'N/A'

