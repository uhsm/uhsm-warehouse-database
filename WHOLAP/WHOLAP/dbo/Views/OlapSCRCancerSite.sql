﻿create view OlapSCRCancerSite as

select
	 CancerSiteCode = CancerSite.CancerSiteId
	,CancerSite = CancerSite.CancerSite
from
	WH.SCR.CancerSite
