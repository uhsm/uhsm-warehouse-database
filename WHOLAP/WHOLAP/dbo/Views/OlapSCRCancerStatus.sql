﻿

CREATE view [dbo].[OlapSCRCancerStatus] as

select
	 CancerStatusCode = CancerStatus.CancerStatusCode
	,CancerStatus = CancerStatus.CancerStatusShort
	,OrderBy
from
	WH.SCR.CancerStatus
where CancerStatusCode <> '69'	

union all

select distinct
	 CancerStatusCode = BaseSCRReferralTreatment.CancerStatusCode
	,CancerStatus = BaseSCRReferralTreatment.CancerStatusCode + ' - No Description'
	,OrderBy = 999
from
	dbo.BaseSCRReferralTreatment
where
	not exists
	(
	select
		1
	from
		WH.SCR.CancerStatus
	where
		CancerStatus.CancerStatusCode = BaseSCRReferralTreatment.CancerStatusCode
	)
and	BaseSCRReferralTreatment.CancerStatusCode is not null

union all

select
	 'N/A'
	,'N/A'
	,999


