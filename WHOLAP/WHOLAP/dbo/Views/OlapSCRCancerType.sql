﻿
CREATE view [dbo].[OlapSCRCancerType] as

select
	 CancerTypeCode = CancerType.CancerTypeCode
	,CancerType = CancerType.CancerType
from
	WH.SCR.CancerType

union all

select distinct
	 CancerTypeCode = BaseSCRReferralTreatment.CancerTypeCode
	,CancerType = BaseSCRReferralTreatment.CancerTypeCode + ' - No Description'
from
	dbo.BaseSCRReferralTreatment
where
	not exists
	(
	select
		1
	from
		WH.SCR.CancerType
	where
		CancerType.CancerTypeCode = BaseSCRReferralTreatment.CancerTypeCode
	)
and	BaseSCRReferralTreatment.CancerTypeCode is not null

union all

select
	 'N/A'
	,'N/A'

