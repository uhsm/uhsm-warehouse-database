﻿
CREATE view [dbo].[OlapSCRConsultant] as

select
	 ConsultantCode = Consultant.NationalConsultantCode
	,Consultant = Consultant.Consultant + ' (' + Consultant.NationalConsultantCode + ')'
from
	WH.SCR.Consultant

union all

select
	 'N/A'
	,'N/A'

union all

select distinct
	 BaseSCRReferralTreatment.ConsultantCode
	,BaseSCRReferralTreatment.ConsultantCode + ' - No Description'
from
	BaseSCRReferralTreatment
where
	not exists
	(
	select
		1
	from
		WH.SCR.Consultant
	where
		Consultant.NationalConsultantCode = BaseSCRReferralTreatment.ConsultantCode
	)
and	BaseSCRReferralTreatment.ConsultantCode is not null


