﻿create view OlapSCRDefinitiveType as

select
	 DefinitiveTypeCode = DefinitiveType.DefinitiveTypeCode
	,DefinitiveType = DefinitiveType.DefinitiveType
	,OrderBy
from
	WH.SCR.DefinitiveType

union all

select
	 'N/A'
	,'N/A'
	,999
