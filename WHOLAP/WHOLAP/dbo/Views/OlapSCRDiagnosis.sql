﻿create view OlapSCRDiagnosis as

select
	 DiagnosisCode = Diagnosis.DiagnosisCode
	,Diagnosis = Diagnosis.Diagnosis
from
	WH.SCR.Diagnosis

union all

select
	 'N/A'
	,'N/A'
