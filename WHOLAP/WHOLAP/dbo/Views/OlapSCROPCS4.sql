﻿

CREATE view [dbo].[OlapSCROPCS4] as

select
	 OPCS4Code
	,OPCS4 = OPCS4Code + ' - ' + OPCS4
from
	WH.SCR.OPCS4
where
	exists
		(
		select
			1
		from
			BaseSCRReferralTreatment
		where
			BaseSCRReferralTreatment.IntendedProcedureCode = OPCS4.OPCS4Code
		)

union all

select distinct

	 IntendedProcedureCode = coalesce(BaseSCRReferralTreatment.IntendedProcedureCode, 'N/A')
 	,coalesce(BaseSCRReferralTreatment.IntendedProcedureCode + ' - Unknown', 'N/A')

from
	BaseSCRReferralTreatment

where
	not exists
		(
		select
			1
		from
			WH.SCR.OPCS4
		where
			OPCS4.OPCS4Code = BaseSCRReferralTreatment.IntendedProcedureCode
		)

