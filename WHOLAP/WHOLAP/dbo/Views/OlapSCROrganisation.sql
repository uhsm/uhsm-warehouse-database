﻿

CREATE view [dbo].[OlapSCROrganisation] as


select
	 OrganisationCode
	,Organisation
	,LocalOrganisation =
		case
		when LocalOrganisation.EntityCode is null
		then 'Other'
		else 'Local'
		end
from
(
select
	 OrganisationCode
	,Organisation
from
	WH.SCR.Organisation

union all

select distinct
	 TreatmentOrganisationCode
	,TreatmentOrganisationCode + ' - No Description'
from
	dbo.FactSCRReferralTreatment
where
	not exists
	(
	select
		1
	from
		WH.SCR.Organisation
	where
		Organisation.OrganisationCode = FactSCRReferralTreatment.TreatmentOrganisationCode
	)
and	FactSCRReferralTreatment.TreatmentOrganisationCode <> 'N/A'

union

select distinct
	 ReferringOrganisationCode
	,ReferringOrganisationCode + ' - No Description'
from
	dbo.FactSCRReferralTreatment
where
	not exists
	(
	select
		1
	from
		WH.SCR.Organisation
	where
		Organisation.OrganisationCode = FactSCRReferralTreatment.ReferringOrganisationCode
	)
and	FactSCRReferralTreatment.ReferringOrganisationCode <> 'N/A'

union all

select
	 'N/A'
	,'N/A'
) Organisation

left join dbo.EntityLookup LocalOrganisation
on	LocalOrganisation.EntityTypeCode = 'LOCALORGANISATION'
and	LocalOrganisation.EntityCode = Organisation.OrganisationCode


