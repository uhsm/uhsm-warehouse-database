﻿create view OlapSCRPathwayType as

select
	 PathwayTypeCode = 'A'
	,PathwayType = 'Admitted'

union all

select
	 PathwayTypeCode = 'N'
	,PathwayType = 'Non-Admitted'