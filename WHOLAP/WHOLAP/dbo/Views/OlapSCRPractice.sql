﻿create view [dbo].[OlapSCRPractice] as

select

	 PracticeCode = convert(varchar(10), Practice.OrganisationCode)
 
	,PCTCode = convert(varchar(10), coalesce(PCT.OrganisationCode, 'N/A'))

	,HealthAuthorityCode = convert(varchar(10), coalesce(PCT.HACode, 'N/A'))
 
	,Practice =
		convert(varchar(255), rtrim(Practice.OrganisationCode) + ' - ' + coalesce(Practice.Organisation, 'Unknown'))

	,PCT =
		convert(varchar(255), coalesce(rtrim(PCT.OrganisationCode), 'N/A') + ' - ' + coalesce(PCT.Organisation, 'Unknown'))

	,HealthAuthority =
		convert(varchar(255),
			coalesce(
				HealthAuthority.Organisation
				,case
				when PCT.HACode is null
				then 'Unknown'
				else rtrim(PCT.HACode) + ' - Unknown'
				end
			)
		)

	,LocalPCT =
		convert(varchar(255), 
			case
			when exists
				(
				select
					1
				from
					WH.dbo.EntityLookup LocalPCT
				where
					LocalPCT.EntityCode = Practice.ParentOrganisationCode
				and	LocalPCT.EntityTypeCode = 'LOCALPCT'
				)
			then coalesce(rtrim(PCT.OrganisationCode), 'N/A') + ' - ' + coalesce(PCT.Organisation, 'Unknown')
			else 'Other'
			end
		)

from
	Organisation.dbo.Practice Practice

left join Organisation.dbo.PCT PCT
on	PCT.OrganisationCode = Practice.ParentOrganisationCode

left join Organisation.dbo.HealthAuthority HealthAuthority
on	HealthAuthority.OrganisationCode = PCT.HACode

where
	exists
		(
		select
			1
		from
			BaseSCRReferralTreatment
		where
			BaseSCRReferralTreatment.PracticeCode = Practice.OrganisationCode
		)

union all

select distinct

	 PracticeCode = coalesce(PracticeCode, 'N/A')
 
	,PCTCode = 'N/A'

	,HealthAuthorityCode = 'N/A'
 
	,Practice =
		coalesce(PracticeCode + ' - Unknown', 'Unknown')

	,PCT =
		'Unknown'

	,HealthAuthority =
		'Unknown'

	,LocalPCT =
		'Other'

from
	BaseSCRReferralTreatment

where
	not exists
		(
		select
			1
		from
			Organisation.dbo.Practice
		where
			BaseSCRReferralTreatment.PracticeCode = Practice.OrganisationCode
		)