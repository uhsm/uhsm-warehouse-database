﻿
CREATE view [dbo].[OlapSCRPriorityType] as

select
	 Priority.PriorityCode
	,Priority.Priority
from
	WH.SCR.Priority

union all

select distinct
	 PriorityCode = BaseSCRReferralTreatment.PriorityTypeCode
	,Priority = BaseSCRReferralTreatment.PriorityTypeCode + ' - No Description'
from
	dbo.BaseSCRReferralTreatment
where
	not exists
	(
	select
		1
	from
		WH.SCR.Priority
	where
		Priority.PriorityCode = BaseSCRReferralTreatment.PriorityTypeCode
	)
and	BaseSCRReferralTreatment.PriorityTypeCode is not null

union all

select
	 'N/A'
	,'N/A'

