﻿
CREATE view [dbo].[OlapSCRSourceForOutpatient] as

select
	 SourceForOutpatientCode = SourceForOutpatient.SourceForOutpatientCode
	,SourceForOutpatient = SourceForOutpatient.SourceForOutpatient
	,OrderBy
from
	WH.SCR.SourceForOutpatient

union all

select distinct
	 SourceForOutpatientCode = BaseSCRReferralTreatment.SourceForOutpatientCode
	,SourceForOutpatient = BaseSCRReferralTreatment.SourceForOutpatientCode + ' - No Description'
	,OrderBy = 999
from
	dbo.BaseSCRReferralTreatment
where
	not exists
	(
	select
		1
	from
		WH.SCR.SourceForOutpatient
	where
		SourceForOutpatient.SourceForOutpatientCode = BaseSCRReferralTreatment.SourceForOutpatientCode
	)
and	BaseSCRReferralTreatment.SourceForOutpatientCode is not null

union all

select
	 'N/A'
	,'N/A'
	,999

