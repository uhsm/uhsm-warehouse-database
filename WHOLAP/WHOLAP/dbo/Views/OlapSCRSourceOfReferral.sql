﻿
CREATE view [dbo].[OlapSCRSourceOfReferral] as

select
	 SourceOfReferralCode
	,SourceOfReferral
	,SourceOfReferralShort
	,SourceOfReferralMini
	,OrderBy
from
	WH.SCR.SourceOfReferral

union all

select distinct
	 SourceOfReferralCode = BaseSCRReferralTreatment.SourceOfReferralCode
	,SourceOfReferral = BaseSCRReferralTreatment.SourceOfReferralCode + ' - No Description'
	,SourceOfReferralShort = BaseSCRReferralTreatment.SourceOfReferralCode + ' - No Description'
	,SourceOfReferralMini = BaseSCRReferralTreatment.SourceOfReferralCode + ' - No Description'
	,OrderBy = 999
from
	dbo.BaseSCRReferralTreatment
where
	not exists
	(
	select
		1
	from
		WH.SCR.SourceOfReferral
	where
		SourceOfReferral.SourceOfReferralCode = BaseSCRReferralTreatment.SourceOfReferralCode
	)
and	BaseSCRReferralTreatment.SourceOfReferralCode is not null

union all

select
	 'N/A'
	,'N/A'
	,'N/A'
	,'N/A'
	,99


