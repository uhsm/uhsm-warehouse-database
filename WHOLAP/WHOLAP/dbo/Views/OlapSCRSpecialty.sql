﻿
CREATE view [dbo].[OlapSCRSpecialty] as

select
	 SpecialtyCode = Specialty.SpecialtyCode
	,Specialty = Specialty.Specialty
	,Division
from
	WH.SCR.Specialty

union all

select
	 999
	,'N/A'
	,'N/A'
	
--KO workaraound
union all
select
	 651
	,'Occupational Therapy'
	,'Medical'
