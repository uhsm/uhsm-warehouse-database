﻿create view OlapSCRTreatmentSetting as

select
	 TreatmentSettingCode = TreatmentSetting.TreatmentSettingCode
	,TreatmentSetting = TreatmentSetting.TreatmentSetting
from
	WH.SCR.TreatmentSetting

union all

select
	 'N/A'
	,'N/A'
