﻿create view OlapSCRTumorStatus as

select
	 TumorStatusCode = TumorStatus.TumorStatusCode
	,TumorStatus = TumorStatus.TumorStatus
	,OrderBy
from
	WH.SCR.TumorStatus
