﻿create view [OlapSite]
as

select
	 SiteCode
	,Site
	,Colour
	,Sequence
from
	WH.WH.Site
