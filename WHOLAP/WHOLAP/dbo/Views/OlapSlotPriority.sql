﻿create view [dbo].[OlapSlotPriority] as

select
	 SlotPriority.SlotPriorityCode
	,SlotPriority.SlotPriority
from
	WH.PAS.SlotPriority SlotPriority
where
	exists
	(
	select
		1
	from
		dbo.FactOPSlot Encounter
	where
		Encounter.SlotPriorityCode = SlotPriority.SlotPriorityCode
	)
