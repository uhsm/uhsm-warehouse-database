﻿create view [dbo].[OlapSlotVisitType] as

select
	 SlotVisitType.SlotVisitTypeCode
	,SlotVisitType.SlotVisitType
from
	WH.PAS.SlotVisitType SlotVisitType
where
	exists
	(
	select
		1
	from
		dbo.FactOPSlot Encounter
	where
		Encounter.SlotVisitTypeCode = SlotVisitType.SlotVisitTypeCode
	)
