﻿CREATE view [OlapSourceOfReferral] as

select
	 SourceOfReferral.SourceOfReferralCode
	,SourceOfReferral.SourceOfReferral

	,ReferralType =
	case when SourceOfReferral.SourceOfReferralCode in('03','92') then 'Gp Referral'
	else 'Other Referral'
	end 
from
	WH.WH.SourceOfReferral SourceOfReferral

union all

select distinct
	 NationalSourceOfReferralCode
	,NationalSourceOfReferralCode + ' - No Description'
	,'Other Referral'
from
	dbo.OlapPASSourceOfReferral
where
	not exists
	(
	select
		1
	from
		WH.WH.SourceOfReferral SourceOfReferral
	where
		SourceOfReferral.SourceOfReferralCode = OlapPASSourceOfReferral.NationalSourceOfReferralCode
	)
