﻿CREATE view [OlapSpecialty]
as

select
	 Specialty.SpecialtyCode
	,Specialty.Specialty
	,Directorate.DirectorateCode
	,Directorate.Directorate
	,Division.DivisionCode
	,Division.Division

	,DemandSpecialty =
		convert(
			bit
			,case
			when ExcludedSpecialty.EntityCode is null
			then 1
			else 0
			end
		)
from
	WH.WH.Specialty Specialty

inner join WH.WH.Directorate Directorate
on	Directorate.DirectorateCode = Specialty.DirectorateCode

inner join WH.WH.Division Division
on	Division.DivisionCode = Directorate.DivisionCode

left join WH.dbo.EntityLookup ExcludedSpecialty
on	ExcludedSpecialty.EntityCode = Specialty.SpecialtyCode
and	ExcludedSpecialty.EntityTypeCode = 'DEMANDEXCLUDEDSPECIALTY'
