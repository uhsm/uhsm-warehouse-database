﻿create view [OlapTargetType] as

select
	TargetTypeCode
	,TargetType
from
	WH.WH.TargetType
