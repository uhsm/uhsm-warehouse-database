﻿CREATE view [dbo].[OlapTheatre] as

select
	 Theatre.TheatreCode
	,Theatre.Theatre
	,Theatre.OperatingSuiteCode

	,OperatingSuite = 
		coalesce(
			 OperatingSuite.OperatingSuite
			,convert(varchar, Theatre.OperatingSuiteCode) + ' - No Description'
		)

	,Theatre.TheatreCode1
from
	WH.Theatre.Theatre Theatre

left join WH.Theatre.OperatingSuite
on	OperatingSuite.OperatingSuiteCode = Theatre.OperatingSuiteCode

union all

select
	0
	,'N/A'
	,0
	,'N/A'
	,'N/A'
