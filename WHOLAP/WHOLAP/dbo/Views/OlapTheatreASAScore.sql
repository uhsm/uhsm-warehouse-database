﻿CREATE view [dbo].[OlapTheatreASAScore] as

select
	 TheatreASAScore.ASAScoreCode
	,TheatreASAScore.ASAScore
from
	WH.Theatre.ASAScore TheatreASAScore

union all

select
	0
	,'N/A'
