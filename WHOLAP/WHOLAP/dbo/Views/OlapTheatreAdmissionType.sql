﻿create view [dbo].[OlapTheatreAdmissionType] as

select
	 TheatreAdmissionType.AdmissionTypeCode
	,TheatreAdmissionType.AdmissionType
from
	WH.Theatre.AdmissionType TheatreAdmissionType

