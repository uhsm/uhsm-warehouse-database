﻿


CREATE view [dbo].[OlapTheatreAnaestheticType] as

select
	 AnaestheticTypeCode = left(TheatreAnaestheticType.AnaestheticTypeCode1, 3) --this field is 3 characters on the FPATS table but 4 in the ref table so make it 3.
	,AnaestheticType = TheatreAnaestheticType.AnaestheticType
from
	WH.Theatre.AnaestheticType TheatreAnaestheticType

union all

select
	'N/A'
	,'N/A'


