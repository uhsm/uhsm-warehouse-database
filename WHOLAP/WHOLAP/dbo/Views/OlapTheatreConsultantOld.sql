﻿create view [OlapTheatreConsultantOld] as

select
	*
from
	(
	select
		 Consultant.ConsultantCode
		,Consultant.Consultant

		,SpecialtyCode =
			coalesce(
				 Consultant.SpecialtyCode
				,'##'
			)

		,Specialty =
			coalesce(
				 Specialty.Specialty
				,Consultant.SpecialtyCode + ' - No Description'
				,'N/A'
			)
	from
		WH.Theatre.Consultant Consultant

	left join WH.Theatre.Specialty Specialty
	on	Consultant.SpecialtyCode = Specialty.SpecialtyCode

	union all

	select
		 ConsultantCode = '##'
		,Consultant = 'N/A'
		,SpecialtyCode = '##'
		,Specialty = 'N/A'

	) Consultant

where
	exists
	(
	select
		1
	from
		dbo.FactTheatreSession
	where
		FactTheatreSession.ConsultantCode = Consultant.ConsultantCode
	)
