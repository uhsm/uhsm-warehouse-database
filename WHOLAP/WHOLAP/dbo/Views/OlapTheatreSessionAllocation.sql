﻿CREATE  view [dbo].[OlapTheatreSessionAllocation] as

select
	 SessionAllocationCode = EntityCode
	,SessionAllocation = Description
from
	dbo.EntityLookup
where
	EntityTypeCode = 'THEATRESESSIONALLOCATION'

