﻿create view [OlapTheatreSite] as

select
	SiteCode = Interface.InterfaceCode
	,Site = Interface.Interface
	,MappedSiteCode = left(Interface.InterfaceCode, 1)
from
	WH.WH.Interface Interface
where
	Interface.InterfaceTypeCode = 'T'
