﻿

CREATE view [dbo].[OlapTheatreStaff] as


select
	 Staff.StaffCode

	,StaffName =
		coalesce(
			coalesce(
				Staff.Forename
				,''
			) + ' ' +
			coalesce(
				Staff.Surname
				,''
			)
			,StaffCode1 + ' - No Description'
		)

	,StaffCategory.StaffCategoryCode
	,StaffCategory.StaffCategory
	,Staff.SpecialtyCode

	,StaffType =
		case
		when coalesce(StaffCode1, '') = ''
		then 'Unknown'
		when StaffCode1 like 'C[0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
		then
			case
			when AnaesthetistFlag = 1
			then 'Anaesthetist'
			when SurgeonFlag = 1
			then 'Surgeon'
			else 'Other Consultant'
			end
		else 'Other'
		end

	,LEFT(StaffCode1,10) AS StaffCode1

from
	WH.Theatre.Staff Staff

left join WH.Theatre.StaffCategory StaffCategory
on	StaffCategory.StaffCategoryCode = Staff.StaffCategoryCode

union all

select
	 0
	,'N/A'
	,0
	,'N/A'
	,0
	,'Unknown'
	,'N/A'


