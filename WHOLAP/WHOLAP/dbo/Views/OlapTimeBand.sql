﻿create view [OlapTimeBand] as

select
	 TimeBandCode
	,MinuteBand
	,FifteenMinuteBand
	,ThirtyMinuteBand
	,HourBand
from
	WH.WH.TimeBand
