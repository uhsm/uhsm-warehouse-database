﻿CREATE view [OlapWLActivityType] as

select
	 ActivityTypeCode
	,ActivityType
	,AgreedMinutes
from
	WH.WH.WLActivityType
