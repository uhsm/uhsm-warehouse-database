﻿create view [OlapWaitType] as

select
	 WaitType.WaitTypeCode
	,WaitType.WaitType
	,WaitType.ParentWaitTypeCode
from
	dbo.WaitTypeBase WaitType
