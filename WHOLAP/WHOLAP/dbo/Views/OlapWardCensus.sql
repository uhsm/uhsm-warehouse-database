﻿



CREATE view [dbo].[OlapWardCensus] as

select
	 CensusDate = Calendar.TheDate
	,Calendar.LongDate
	,WeekKey = Calendar.FirstDayOfWeek
	,LongWeek = 'w/c ' + left(convert(varchar, Calendar.FirstDayOfWeek, 113), 11)
	,MonthKey = Calendar.FinancialMonthKey
	,LongMonth = Calendar.TheMonth
from
	OlapCalendar Calendar
where
	Calendar.TheDate between 
	(
	select
		MIN(BaseAPCWardStay.StartTime)
	from
		dbo.BaseAPCWardStay
	)
and getdate()
	--(
	--select
	--	MAX(BaseAPCWardStay.EndTime) + 365
	--from
	--	dbo.BaseAPCWardStay
	--)





