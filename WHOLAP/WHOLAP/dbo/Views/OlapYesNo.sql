﻿CREATE view [OlapYesNo] as

select
	 YesNoFlag = convert(bit, 1)
	,YesNo = 'Yes'

union all

select
	 YesNoFlag = convert(bit, 0)
	,YesNo = 'No'
