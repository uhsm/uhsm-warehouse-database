﻿CREATE View [dbo].[OlapiPMSourceOfReferral] as 

SELECT
	 ReferralSourceCode = RefVal.RFVAL_REFNO
	,ReferralSourceLocalCode = RefVal.MAIN_CODE
	,ReferralSourceDescription = RefVal.DESCRIPTION
	,ReferralSourceNationalCode = 
								Case When refval.RFVAL_REFNO = 1226 Then --1226 is Not Specified
									'97'
								Else
									ISNUll(refvalid.IDENTIFIER,-1)
								End
	,ReferralGroup = 
		Case 
		When RefVal.RFVAL_REFNO in (
				 3247
				,1510
				,4305
				,6422
				,2005592
				,3224
				,3693
				,2001354
				,2004330
				,2004202 ) Then 'Consultant'
		When RefVal.RFVAL_REFNO in (
				 5300
				,2002324
				,2003641
				,2005590
				,6428) Then 'GP/GDP'
		When RefVal.RFVAL_REFNO in (
				 1509
				,3692
				,3007115
				,2004201
				,2004203
				,2005591
				,2005596
				,3007082
				,3007081
				,3007080
				,3007079
				,2005595
				,2003485
				,2003486
				,2003487
				,3362
				,1511
				,3364
				,6421
				,6424
				,6427
				,2001353
				,2003818
				,2003819
				,2003820
				,2003821
				,2003822
				,2003823
				,2003824
				,2003825
				,2004204
				,2004205
				,2004206
				,2004207
				,2004318
				,2004329
				,2005415
				,2005593
				,3007116
				,3007117
				,3007118
				,3007119) Then 'Other'
		Else 
				'Unknown'
		End

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
		and refvalid.RITYP_CODE = 'NHS'
		and refvalid.ARCHV_FLAG = 'N'

WHERE 
refval.rfvdm_code = 'SORRF'
and RefVal.ARCHV_FLAG = 'N'

Union All
	Select
		 -1
		,'-1'
		,'Unknown'
		,97
		,'Unknown'
/*
Union All
	Select
		 1226
		,'NSP'
		,'Not Specified'
		,97
		,'Unknown'
*/		

