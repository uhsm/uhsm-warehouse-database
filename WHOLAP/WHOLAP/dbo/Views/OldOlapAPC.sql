﻿




CREATE view [dbo].[OldOlapAPC] as

select
	 EncounterRecno
	,SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,ReferralSourceUniqueID
	,WaitingListSourceUniqueID
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,DischargeDate
	,EpisodeStartDate
	,EpisodeEndDate
	,StartSiteCode = coalesce(Encounter.StartSiteCode, -1)
	,StartWardTypeCode
	,EndSiteCode = coalesce(Encounter.EndSiteCode, -1)
	,EndWardTypeCode
	,RegisteredGpCode

	,RegisteredGpPracticeCode =
		coalesce(Encounter.RegisteredGpPracticeCode, -1)

	,SiteCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,AdminCategoryCode
	,ConsultantCode = coalesce(Encounter.ConsultantCode, -1)
	,SpecialtyCode = coalesce(Encounter.SpecialtyCode, -1)
	,LastEpisodeInSpellIndicator
	,FirstRegDayOrNightAdmit
	,NeonatalLevelOfCare

	,PASHRGCode

	,PrimaryDiagnosisCode =
		coalesce(left(Encounter.PrimaryDiagnosisCode, 5), '##')

	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12

	,PrimaryOperationCode =
		coalesce(left(Encounter.PrimaryOperationCode, 5), '##')
	
	,PrimaryOperationDate
	,SecondaryOperationCode1
	,SecondaryOperationDate1
	,SecondaryOperationCode2
	,SecondaryOperationDate2
	,SecondaryOperationCode3
	,SecondaryOperationDate3
	,SecondaryOperationCode4
	,SecondaryOperationDate4
	,SecondaryOperationCode5
	,SecondaryOperationDate5
	,SecondaryOperationCode6
	,SecondaryOperationDate6
	,SecondaryOperationCode7
	,SecondaryOperationDate7
	,SecondaryOperationCode8
	,SecondaryOperationDate8
	,SecondaryOperationCode9
	,SecondaryOperationDate9
	,SecondaryOperationCode10
	,SecondaryOperationDate10
	,SecondaryOperationCode11
	,SecondaryOperationDate11
	,OperationStatusCode
	,ContractSerialNo
	,CodingCompleteFlag
	,PurchaserCode
	,ProviderCode
	,EpisodeStartTime
	,EpisodeEndTime
	,RegisteredGdpCode
	,EpisodicGpCode
	,InterfaceCode
	,CasenoteNumber
	,NHSNumberStatusCode
	,AdmissionTime
	,DischargeTime
	,TransferFrom
	,DistrictNo
	,SourceUniqueID
	,ExpectedLOS
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,OverseasStatusFlag
	,PASCreated
	,PASUpdated
	,PASCreatedByWhom
	,PASUpdatedByWhom
	,Created
	,Updated
	,ByWhom
	,TheatrePatientBookingKey
	,ProcedureTime
	,TheatreCode

	,AgeCode =
	case
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) is null then 'Age Unknown'
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) <= 0 then '00 Days'
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) = 1 then '01 Day'
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) > 1 and
	datediff(day, DateOfBirth, Encounter.EpisodeStartDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, DateOfBirth, Encounter.EpisodeStartDate)), 2) + ' Days'
	
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) > 28 and
	datediff(month, DateOfBirth, Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, DateOfBirth, Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, DateOfBirth, Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end > 1 and
	 datediff(month, DateOfBirth, Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, DateOfBirth,Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, DateOfBirth, Encounter.EpisodeStartDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.EpisodeStartDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.EpisodeStartDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.EpisodeStartDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, DateOfBirth, Encounter.EpisodeStartDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.EpisodeStartDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.EpisodeStartDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.EpisodeStartDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end 

	,CategoryCode =
		case
		when Encounter.ManagementIntentionCode = 2000619 then '10'
		when Encounter.ManagementIntentionCode = 9487 then '10'
		when Encounter.ManagementIntentionCode = 8850 then '20'
		when Encounter.ManagementIntentionCode = 8851 then '20'
		else '28' --other inpatient
		end

	,LOE =
		datediff(day, Encounter.EpisodeStartDate, Encounter.EpisodeEndDate)

	,LOS =
	datediff(
		 day
		,Encounter.AdmissionDate
		,coalesce(
			 Encounter.DischargeDate
			,(
			select
				max(DischargeDate)
			from
				WH.APC.Encounter
			)
		)
	)

	,AdmissionMethodTypeCode =
		case
		when Encounter.AdmissionMethodCode in
			(
			 2003470	--Elective
			,8811		--Elective - Booked
			,8810		--Elective - Waiting List
			,8812		--Elective - Planned
			,13			--Not Specified
			,2003472	--OTHER
			)
		then 'EL' --elective
		when Encounter.AdmissionMethodCode in 
			(
			 2000685	--Emergency - Bed Bureau	23
			,2000616	--Emergency - Clinic	24
			,2000615	--Emergency - GP	22
			,2000614	--Emergency - Local A&E	21
			,2000617	--Emergency - Other (inc other provider A&E)	28
			)
		then 'EM'
		when Encounter.AdmissionMethodCode in 
			(
			 2003473	--Maternity	MATER
			,8814		--Maternity ante-partum	31
			,8815		--Maternity post-partum	32
			)
		then 'MAT'

		else 'NE'
		end

	,NeonatalLevelOfCareFlag =
		case
		when NeonatalLevelOfCare = 2004631	--Not Applicable	8
		then null
		when NeonatalLevelOfCare in
			(
			 2004627	--Special Care	1
			,2004628	--Level 2 Intensive Care (High Dependency Intensive Care)	2
			,2004629	--Level 1 Intensive Care (Maximal Intensive Care)	3
			)
		then 1
		else 0
		end

	,InpatientStayCode =
		case
		when
			(
				Encounter.ManagementIntentionCode in
				(
				 2000619	--At Least One Night	1
				,2003683	--Not Applicable	8
				,2003684	--Not Known	9
				,2005563	--Not Specified	NSP
				)

			or	(
					Encounter.ManagementIntentionCode in
					(
					 8850	--No Overnight Stay	2
					,8851	--Planned Sequence - No Overnight Stays	4
					,9487	--Planned Sequence - Inc. Over Night Stays	3
					)

				and	datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) <> 0
				)
			)
		then 'I'

		when
			Encounter.ManagementIntentionCode in
			(
			 8850	--No Overnight Stay	2
			)
		and	datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
		then 'D'

		when
			Encounter.ManagementIntentionCode in
			(
			 8851	--Planned Sequence - No Overnight Stays	4
			)
		and	datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
		then 'RD'

		when
			Encounter.ManagementIntentionCode in
			(
			 9487	--Planned Sequence - Inc. Over Night Stays	3
			)
		and	datediff(day, Encounter.AdmissionDate, coalesce(Encounter.DischargeDate, Encounter.EpisodeEndDate, getdate())) = 0
		then 'RN'

		else 'OTHER'
		end


	,1 Cases

from
	dbo.BaseAPC Encounter






