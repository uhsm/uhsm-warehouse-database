﻿


CREATE VIEW [dbo].[vwCriticalCareDays]
AS


select 
CCFinanceDays.TheMonth,
/*case when CCFinanceDays.SourceSpellNo in 
(
SELECT distinct SourceSpellNo FROM WHReporting.APC.Episode EPI2
LEFT OUTER JOIN WHReporting.APC.ClinicalCoding CC2
ON EPI2.EncounterRecno = CC2.EncounterRecno  
AND EPI2.ConsultantCode = 'C2386780' 
AND CC2.PriamryProcedureCode LIKE 'K%'
AND EPI2.[SpecialtyCode(Function)] = '173'
) then '320' else */
CCFinanceDays.[SpecialtyCode(Function)] /*end as SpecialtyFunction*/,
CCFinanceDays.SourceSpellNo as SpellNumber,
CCFinanceDays.ProcCode,
CCFinanceDays.SPONT_REFNO_CODE as Ward,
CCFinanceDays.L2FinanceBedDays,
CCFinanceDays.L3FinanceBedDays,
sum(Case when CCActualDays.Care_Level=3 then CCActualDays.BedDays else 0 end) as L3ActualBedDays,
sum(Case when CCActualDays.Care_Level=2 then CCActualDays.BedDays else 0 end) as L2ActualBedDays from 

(Select Distinct 
cal.TheMonth,
CCPeriod.SourceSpellNo
,SPONT_REFNO_CODE
,EPI.[SpecialtyCode(Function)]
,Case when TXTYPE='SL' Then 1
when TXTYPE='DL' Then 2
when TXTYPE='H' Then 3
when CCPeriod.SourceSpellNo in (

'150082579',
'150118810',
'150119575',
'150169207',
'150169892',
'150189458',
'150199225',
'150204123',
'150217673',
'150232045',
'150288186',
'150305885',
'150315650',
'150342401',
'150324280',
'150335131',
'150342407',
'150343130',
'150361421',
'150385499',
'150394035',
'150400193',
'150401854',
'150404513',
'150409385',
'150416332',
'150416829') then 4 
when CVE.TypeOfProc IS NOT NULL THEN CVE.TypeOfProc

when CCPeriod.SourceSpellNo in

(
'150328358'
,'150229905'
,'150241005'
,'150249911'
,'150250700'
,'150261339'
,'150261342'
,'150267033'
,'150267013'
,'150261324'
,'150267029'
,'150274659'
,'150274654'
,'150268697'
,'150268685'
,'150274660'
,'150280836'
,'150276860'
,'150276834'
,'150278828'
,'150278818'
,'150275212'
,'150280824'
,'150290995'
,'150286772'
,'150300813'
,'150297097'
,'150301121'
,'150304034'
,'150303659'
,'150304011'
,'150286786'
,'150303195'
,'150314305'
,'150314306'
,'150308256'
,'150308257'
,'150320173'
,'150318250'
,'150314010'
,'150318251'
,'150320207'
,'150322146'
,'150322319'
,'150234708'
,'150149651'
,'150157050'
,'150146036'
,'150157079'
,'150160028'
,'150166755'
,'150179480'
,'150179383'
,'150199166'
,'150229905'
)
Then 10


else 13
end as ProcCode

,
sum(Case when STAY.Care_Level=3 then STAY.CCPStay else 0 end) as L3FinanceBedDays,
sum(Case when STAY.Care_Level=2 then STAY.CCPStay else 0 end) as L2FinanceBedDays


from 


Lorenzo.dbo.ExtractCriticalCarePeriodStay stay
inner join WHReporting.APC.CriticalCarePeriod CCPeriod on CCPeriod.SourceCCPNo=stay.SourceCCPNo
LEFT JOIN WHReporting.APC.Episode EPI
ON CCPeriod.SourceEncounterNo = EPI.EpisodeUniqueID
lEFT JOIN WHReporting.dbo.TXPatients Transplants ON Transplants.Spell2=CCPeriod.SourceSpellNo
left join WHREPORTING.LK.Calendar cal on cal.TheDate=cast(stay.StayDate as date)
LEFT JOIN
(SELECT DISTINCT (EPI.SourceSpellNo), 
case when 
(
PriamryProcedureCode >= 'K401' AND PriamryProcedureCode <= 'K469'
) 
AND
(
SecondaryProcedureCode1 >= 'K241' AND SecondaryProcedureCode1 <= 'K369'
) 
OR 
(
SecondaryProcedureCode2 >= 'K241' AND SecondaryProcedureCode2 <= 'K369'
) 
OR
(
SecondaryProcedureCode3 >= 'K241' AND SecondaryProcedureCode3 <= 'K369'
) 
OR
(
SecondaryProcedureCode4 >= 'K241' AND SecondaryProcedureCode4 <= 'K369'
) 
OR
(
SecondaryProcedureCode5 >= 'K241' AND SecondaryProcedureCode5 <= 'K369'
) and

EPI.SourceSpellNo

IN
(
'150328358'
,'150229905'
,'150241005'
,'150249911'
,'150250700'
,'150261339'
,'150261342'
,'150267033'
,'150267013'
,'150261324'
,'150267029'
,'150274659'
,'150274654'
,'150268697'
,'150268685'
,'150274660'
,'150280836'
,'150276860'
,'150276834'
,'150278828'
,'150278818'
,'150275212'
,'150280824'
,'150290995'
,'150286772'
,'150300813'
,'150297097'
,'150301121'
,'150304034'
,'150303659'
,'150304011'
,'150286786'
,'150303195'
,'150314305'
,'150314306'
,'150308256'
,'150308257'
,'150320173'
,'150318250'
,'150314010'
,'150318251'
,'150320207'
,'150322146'
,'150322319'
,'150234708'
,'150149651'
,'150157050'
,'150146036'
,'150157079'
,'150160028'
,'150166755'
,'150179480'
,'150179383'
,'150199166'
,'150229905'
) 

and

(
PriamryProcedureCode >= 'K401' AND PriamryProcedureCode <= 'K469'
) 
OR
(
SecondaryProcedureCode1 >= 'K401' AND SecondaryProcedureCode1 <= 'K469'
) 
OR 
(
SecondaryProcedureCode2 >= 'K401' AND SecondaryProcedureCode2 <= 'K469'
) 
OR
(
SecondaryProcedureCode3 >= 'K401' AND SecondaryProcedureCode3 <= 'K469'
) 
OR
(
SecondaryProcedureCode4 >= 'K401' AND SecondaryProcedureCode4 <= 'K469'
) 
OR
(
SecondaryProcedureCode5 >= 'K401' AND SecondaryProcedureCode5 <= 'K469'
) then 14


WHEN 
(
PriamryProcedureCode >= 'K401' AND PriamryProcedureCode <= 'K469'
) 
AND
(
SecondaryProcedureCode1 >= 'K241' AND SecondaryProcedureCode1 <= 'K369'
) 
OR 
(
SecondaryProcedureCode2 >= 'K241' AND SecondaryProcedureCode2 <= 'K369'
) 
OR
(
SecondaryProcedureCode3 >= 'K241' AND SecondaryProcedureCode3 <= 'K369'
) 
OR
(
SecondaryProcedureCode4 >= 'K241' AND SecondaryProcedureCode4 <= 'K369'
) 
OR
(
SecondaryProcedureCode5 >= 'K241' AND SecondaryProcedureCode5 <= 'K369'
) and

EPI.SourceSpellNo

IN
(
'150328358'
,'150229905'
,'150241005'
,'150249911'
,'150250700'
,'150261339'
,'150261342'
,'150267033'
,'150267013'
,'150261324'
,'150267029'
,'150274659'
,'150274654'
,'150268697'
,'150268685'
,'150274660'
,'150280836'
,'150276860'
,'150276834'
,'150278828'
,'150278818'
,'150275212'
,'150280824'
,'150290995'
,'150286772'
,'150300813'
,'150297097'
,'150301121'
,'150304034'
,'150303659'
,'150304011'
,'150286786'
,'150303195'
,'150314305'
,'150314306'
,'150308256'
,'150308257'
,'150320173'
,'150318250'
,'150314010'
,'150318251'
,'150320207'
,'150322146'
,'150322319'
,'150234708'
,'150149651'
,'150157050'
,'150146036'
,'150157079'
,'150160028'
,'150166755'
,'150179480'
,'150179383'
,'150199166'
,'150229905'
) THEN 12

WHEN (
PriamryProcedureCode >= 'K401' AND PriamryProcedureCode <= 'K469'
) 
AND
(
SecondaryProcedureCode1 >= 'K241' AND SecondaryProcedureCode1 <= 'K369'
) 
OR 
(
SecondaryProcedureCode2 >= 'K241' AND SecondaryProcedureCode2 <= 'K369'
) 
OR
(
SecondaryProcedureCode3 >= 'K241' AND SecondaryProcedureCode3 <= 'K369'
) 
OR
(
SecondaryProcedureCode4 >= 'K241' AND SecondaryProcedureCode4 <= 'K369'
) 
OR
(
SecondaryProcedureCode5 >= 'K241' AND SecondaryProcedureCode5 <= 'K369'
)


THEN 7

WHEN

(
PriamryProcedureCode >= 'K401' AND PriamryProcedureCode <= 'K469'
) 
OR
(
SecondaryProcedureCode1 >= 'K401' AND SecondaryProcedureCode1 <= 'K469'
) 
OR 
(
SecondaryProcedureCode2 >= 'K401' AND SecondaryProcedureCode2 <= 'K469'
) 
OR
(
SecondaryProcedureCode3 >= 'K401' AND SecondaryProcedureCode3 <= 'K469'
) 
OR
(
SecondaryProcedureCode4 >= 'K401' AND SecondaryProcedureCode4 <= 'K469'
) 
OR
(
SecondaryProcedureCode5 >= 'K401' AND SecondaryProcedureCode5 <= 'K469'
)
THEN 5

WHEN

(
PriamryProcedureCode >= 'K241' AND PriamryProcedureCode <= 'K369'
) 
OR
(
SecondaryProcedureCode1 >= 'K241' AND SecondaryProcedureCode1 <= 'K369'
) 
OR 
(
SecondaryProcedureCode2 >= 'K241' AND SecondaryProcedureCode2 <= 'K369'
) 
OR
(
SecondaryProcedureCode3 >= 'K241' AND SecondaryProcedureCode3 <= 'K369'
) 
OR
(
SecondaryProcedureCode4 >= 'K241' AND SecondaryProcedureCode4 <= 'K369'
) 
OR
(
SecondaryProcedureCode5 >= 'K241' AND SecondaryProcedureCode5 <= 'K369'
)
THEN 6

When PriamryProcedureCode='X581'
OR SecondaryProcedureCode1='X581'
OR SecondaryProcedureCode2='X581'
OR SecondaryProcedureCode3='X581'
OR SecondaryProcedureCode4='X581'
OR SecondaryProcedureCode5='X581'
OR SecondaryProcedureCode6='X581'
OR SecondaryProcedureCode7='X581'
OR SecondaryProcedureCode8='X581'

THEN 9



END AS TypeOfProc

FROM WHReporting.APC.Episode EPI
LEFT OUTER JOIN WHReporting.APC.ClinicalCoding CC
ON EPI.EncounterRecno = CC.EncounterRecno

WHERE
--AND EPI.[SpecialtyCode(Function)] <> '320'  
 (
(
PriamryProcedureCode >= 'K401' AND PriamryProcedureCode <= 'K469'
OR PriamryProcedureCode >= 'K241' AND PriamryProcedureCode <= 'K369'
) 
OR
(
SecondaryProcedureCode1 >= 'K401' AND SecondaryProcedureCode1 <= 'K469'
OR SecondaryProcedureCode1 >= 'K241' AND SecondaryProcedureCode1 <= 'K369'
) 
OR
(SecondaryProcedureCode2 >= 'K401' AND SecondaryProcedureCode2 <= 'K469'
OR SecondaryProcedureCode2 >= 'K241' AND SecondaryProcedureCode2 <= 'K369'
) 
OR
(
SecondaryProcedureCode3 >= 'K401' AND SecondaryProcedureCode3 <= 'K469'
OR SecondaryProcedureCode3 >= 'K241' AND SecondaryProcedureCode3 <= 'K369'
) 
OR
(
SecondaryProcedureCode4 >= 'K401' AND SecondaryProcedureCode4 <= 'K469'
OR SecondaryProcedureCode4 >= 'K241' AND SecondaryProcedureCode4 <= 'K369'
) 
OR
(
SecondaryProcedureCode5 >= 'K401' AND SecondaryProcedureCode5 <= 'K469'
OR SecondaryProcedureCode5 >= 'K241' AND SecondaryProcedureCode5 <= 'K369')
)

OR PriamryProcedureCode='X581'
OR SecondaryProcedureCode1='X581'
OR SecondaryProcedureCode2='X581'
OR SecondaryProcedureCode3='X581'
OR SecondaryProcedureCode4='X581'
OR SecondaryProcedureCode5='X581'
OR SecondaryProcedureCode6='X581'
OR SecondaryProcedureCode7='X581'
OR SecondaryProcedureCode8='X581'
OR
EPI.SourceSpellNo

IN
(
'150328358'
,'150229905'
,'150241005'
,'150249911'
,'150250700'
,'150261339'
,'150261342'
,'150267033'
,'150267013'
,'150261324'
,'150267029'
,'150274659'
,'150274654'
,'150268697'
,'150268685'
,'150274660'
,'150280836'
,'150276860'
,'150276834'
,'150278828'
,'150278818'
,'150275212'
,'150280824'
,'150290995'
,'150286772'
,'150300813'
,'150297097'
,'150301121'
,'150304034'
,'150303659'
,'150304011'
,'150286786'
,'150303195'
,'150314305'
,'150314306'
,'150308256'
,'150308257'
,'150320173'
,'150318250'
,'150314010'
,'150318251'
,'150320207'
,'150322146'
,'150322319'
,'150234708'
,'150149651'
,'150157050'
,'150146036'
,'150157079'
,'150160028'
,'150166755'
,'150179480'
,'150179383'
,'150199166'
,'150229905'
)




GROUP BY EPI.SourceSpellNo,
case when 
(
PriamryProcedureCode >= 'K401' AND PriamryProcedureCode <= 'K469'
) 
AND
(
SecondaryProcedureCode1 >= 'K241' AND SecondaryProcedureCode1 <= 'K369'
) 
OR 
(
SecondaryProcedureCode2 >= 'K241' AND SecondaryProcedureCode2 <= 'K369'
) 
OR
(
SecondaryProcedureCode3 >= 'K241' AND SecondaryProcedureCode3 <= 'K369'
) 
OR
(
SecondaryProcedureCode4 >= 'K241' AND SecondaryProcedureCode4 <= 'K369'
) 
OR
(
SecondaryProcedureCode5 >= 'K241' AND SecondaryProcedureCode5 <= 'K369'
) and

EPI.SourceSpellNo

IN
(
'150328358'
,'150229905'
,'150241005'
,'150249911'
,'150250700'
,'150261339'
,'150261342'
,'150267033'
,'150267013'
,'150261324'
,'150267029'
,'150274659'
,'150274654'
,'150268697'
,'150268685'
,'150274660'
,'150280836'
,'150276860'
,'150276834'
,'150278828'
,'150278818'
,'150275212'
,'150280824'
,'150290995'
,'150286772'
,'150300813'
,'150297097'
,'150301121'
,'150304034'
,'150303659'
,'150304011'
,'150286786'
,'150303195'
,'150314305'
,'150314306'
,'150308256'
,'150308257'
,'150320173'
,'150318250'
,'150314010'
,'150318251'
,'150320207'
,'150322146'
,'150322319'
,'150234708'
,'150149651'
,'150157050'
,'150146036'
,'150157079'
,'150160028'
,'150166755'
,'150179480'
,'150179383'
,'150199166'
,'150229905'
) 

and

(
PriamryProcedureCode >= 'K401' AND PriamryProcedureCode <= 'K469'
) 
OR
(
SecondaryProcedureCode1 >= 'K401' AND SecondaryProcedureCode1 <= 'K469'
) 
OR 
(
SecondaryProcedureCode2 >= 'K401' AND SecondaryProcedureCode2 <= 'K469'
) 
OR
(
SecondaryProcedureCode3 >= 'K401' AND SecondaryProcedureCode3 <= 'K469'
) 
OR
(
SecondaryProcedureCode4 >= 'K401' AND SecondaryProcedureCode4 <= 'K469'
) 
OR
(
SecondaryProcedureCode5 >= 'K401' AND SecondaryProcedureCode5 <= 'K469'
) then 14


WHEN 
(
PriamryProcedureCode >= 'K401' AND PriamryProcedureCode <= 'K469'
) 
AND
(
SecondaryProcedureCode1 >= 'K241' AND SecondaryProcedureCode1 <= 'K369'
) 
OR 
(
SecondaryProcedureCode2 >= 'K241' AND SecondaryProcedureCode2 <= 'K369'
) 
OR
(
SecondaryProcedureCode3 >= 'K241' AND SecondaryProcedureCode3 <= 'K369'
) 
OR
(
SecondaryProcedureCode4 >= 'K241' AND SecondaryProcedureCode4 <= 'K369'
) 
OR
(
SecondaryProcedureCode5 >= 'K241' AND SecondaryProcedureCode5 <= 'K369'
) and

EPI.SourceSpellNo

IN
(
'150328358'
,'150229905'
,'150241005'
,'150249911'
,'150250700'
,'150261339'
,'150261342'
,'150267033'
,'150267013'
,'150261324'
,'150267029'
,'150274659'
,'150274654'
,'150268697'
,'150268685'
,'150274660'
,'150280836'
,'150276860'
,'150276834'
,'150278828'
,'150278818'
,'150275212'
,'150280824'
,'150290995'
,'150286772'
,'150300813'
,'150297097'
,'150301121'
,'150304034'
,'150303659'
,'150304011'
,'150286786'
,'150303195'
,'150314305'
,'150314306'
,'150308256'
,'150308257'
,'150320173'
,'150318250'
,'150314010'
,'150318251'
,'150320207'
,'150322146'
,'150322319'
,'150234708'
,'150149651'
,'150157050'
,'150146036'
,'150157079'
,'150160028'
,'150166755'
,'150179480'
,'150179383'
,'150199166'
,'150229905'
) THEN 12

WHEN (
PriamryProcedureCode >= 'K401' AND PriamryProcedureCode <= 'K469'
) 
AND
(
SecondaryProcedureCode1 >= 'K241' AND SecondaryProcedureCode1 <= 'K369'
) 
OR 
(
SecondaryProcedureCode2 >= 'K241' AND SecondaryProcedureCode2 <= 'K369'
) 
OR
(
SecondaryProcedureCode3 >= 'K241' AND SecondaryProcedureCode3 <= 'K369'
) 
OR
(
SecondaryProcedureCode4 >= 'K241' AND SecondaryProcedureCode4 <= 'K369'
) 
OR
(
SecondaryProcedureCode5 >= 'K241' AND SecondaryProcedureCode5 <= 'K369'
)


THEN 7

WHEN

(
PriamryProcedureCode >= 'K401' AND PriamryProcedureCode <= 'K469'
) 
OR
(
SecondaryProcedureCode1 >= 'K401' AND SecondaryProcedureCode1 <= 'K469'
) 
OR 
(
SecondaryProcedureCode2 >= 'K401' AND SecondaryProcedureCode2 <= 'K469'
) 
OR
(
SecondaryProcedureCode3 >= 'K401' AND SecondaryProcedureCode3 <= 'K469'
) 
OR
(
SecondaryProcedureCode4 >= 'K401' AND SecondaryProcedureCode4 <= 'K469'
) 
OR
(
SecondaryProcedureCode5 >= 'K401' AND SecondaryProcedureCode5 <= 'K469'
)
THEN 5

WHEN

(
PriamryProcedureCode >= 'K241' AND PriamryProcedureCode <= 'K369'
) 
OR
(
SecondaryProcedureCode1 >= 'K241' AND SecondaryProcedureCode1 <= 'K369'
) 
OR 
(
SecondaryProcedureCode2 >= 'K241' AND SecondaryProcedureCode2 <= 'K369'
) 
OR
(
SecondaryProcedureCode3 >= 'K241' AND SecondaryProcedureCode3 <= 'K369'
) 
OR
(
SecondaryProcedureCode4 >= 'K241' AND SecondaryProcedureCode4 <= 'K369'
) 
OR
(
SecondaryProcedureCode5 >= 'K241' AND SecondaryProcedureCode5 <= 'K369'
)
THEN 6

When PriamryProcedureCode='X581'
OR SecondaryProcedureCode1='X581'
OR SecondaryProcedureCode2='X581'
OR SecondaryProcedureCode3='X581'
OR SecondaryProcedureCode4='X581'
OR SecondaryProcedureCode5='X581'
OR SecondaryProcedureCode6='X581'
OR SecondaryProcedureCode7='X581'
OR SecondaryProcedureCode8='X581'

THEN 9



END

) AS CVE ON CVE.SourceSpellNo=CCPeriod.SourceSpellNo

Where SPONT_REFNO_CODE in ('CTCU', 'CT Transplant', 'ICA')

GROUP BY
cal.TheMonth,
CCPeriod.SourceSpellNo
,SPONT_REFNO_CODE
,EPI.[SpecialtyCode(Function)]
,Case when TXTYPE='SL' Then 1
when TXTYPE='DL' Then 2
when TXTYPE='H' Then 3
when CCPeriod.SourceSpellNo in (

'150082579',
'150118810',
'150119575',
'150169207',
'150169892',
'150189458',
'150199225',
'150204123',
'150217673',
'150232045',
'150288186',
'150305885',
'150315650',
'150342401',
'150324280',
'150335131',
'150342407',
'150343130',
'150361421',
'150385499',
'150394035',
'150400193',
'150401854',
'150404513',
'150409385',
'150416332',
'150416829') then 4 
when CVE.TypeOfProc IS NOT NULL THEN CVE.TypeOfProc

when CCPeriod.SourceSpellNo in

(
'150328358'
,'150229905'
,'150241005'
,'150249911'
,'150250700'
,'150261339'
,'150261342'
,'150267033'
,'150267013'
,'150261324'
,'150267029'
,'150274659'
,'150274654'
,'150268697'
,'150268685'
,'150274660'
,'150280836'
,'150276860'
,'150276834'
,'150278828'
,'150278818'
,'150275212'
,'150280824'
,'150290995'
,'150286772'
,'150300813'
,'150297097'
,'150301121'
,'150304034'
,'150303659'
,'150304011'
,'150286786'
,'150303195'
,'150314305'
,'150314306'
,'150308256'
,'150308257'
,'150320173'
,'150318250'
,'150314010'
,'150318251'
,'150320207'
,'150322146'
,'150322319'
,'150234708'
,'150149651'
,'150157050'
,'150146036'
,'150157079'
,'150160028'
,'150166755'
,'150179480'
,'150179383'
,'150199166'
,'150229905'
)
Then 10


else 13
end) as CCFinanceDays


left join whreporting.dbo.vwCriticalCareActualDays CCActualDays



on CCActualDays.sourcespellno=CCFinanceDays.SourceSpellNo
and CCActualDays.TheMonth=CCFinanceDays.TheMonth
and CCActualDays.ActualWard=CCFinanceDays.SPONT_REFNO_CODE

group by

CCFinanceDays.TheMonth,
CCFinanceDays.SourceSpellNo,
CCFinanceDays.[SpecialtyCode(Function)],
CCFinanceDays.ProcCode,
CCFinanceDays.SPONT_REFNO_CODE,
CCFinanceDays.L2FinanceBedDays,
CCFinanceDays.L3FinanceBedDays

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CCActualDays"
            Begin Extent = 
               Top = 6
               Left = 289
               Bottom = 114
               Right = 456
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CCFinanceDays"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 251
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwCriticalCareDays';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwCriticalCareDays';

