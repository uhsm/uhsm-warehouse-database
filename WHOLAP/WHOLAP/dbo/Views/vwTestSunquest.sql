﻿CREATE View [dbo].[vwTestSunquest] AS 
select 
	 BaseOrder.OrderNumber
	,FactOrder.PerformDate
	,FactOrder.ExamCode
	,BaseOrder.OrderingLocationCode1
	,BaseOrder.OrderingLocationCode
	,BaseOrder.PatientSurname
	,BaseOrder.PatientNo1
	,FactOrder.ModalityCode
	,BaseOrder.PerformanceLocation
	,BaseOrder.ScheduleStatusCode
	,RequestType = 
		Case When (
			BaseOrder.CaseTypeCode IN ('B','H','T','Z')
			OR
			BaseOrder.ExamCode1 IN ('MCORP','MCRPS','MCSFS','MCVIA','MCVVS', 'MMAMB','NSENT','XDEXA')
			) Then 'Specialist'
		Else
			'General'
		End
From BaseOrder Left outer join FactOrder
	on BaseOrder.EncounterRecno = FactOrder.EncounterRecno
Where 
(FactOrder.PerformDate >='2011-01-10' and FactOrder.PerformDate<'2011-01-17')
And FactOrder.Performed = 1 






