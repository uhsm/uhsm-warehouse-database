﻿CREATE VIEW dbo.[vwTheatreUtilDirectorateReport]
AS
SELECT     Specialty, Theatre, MonthNumber, Year, CASE WHEN u.Planned IS NULL THEN NULL ELSE u.Planned END AS Planned, Productive, Utilisation
FROM         (SELECT     s.NationalSpecialtyCode, s.Specialty, th.Theatre, MONTH(t.SessionDate) AS MonthNumber, c.FinancialYear AS Year, 
                                              SUM(CASE WHEN t .KPI = 'Planned Minutes Of Actual Session' THEN (t .actual) ELSE 0 END) AS Planned, 
                                              SUM(CASE WHEN t .KPI = 'Productive Minutes' THEN (t .actual) ELSE 0 END) AS Productive, 
                                              SUM(CASE WHEN t .KPI = 'Session Utilisation Minutes' THEN (t .actual) ELSE 0 END) AS Utilisation
                       FROM          dbo.FactTheatreKPI AS t INNER JOIN
                                              dbo.OlapTheatreSpecialty AS s ON s.SpecialtyCode = t.SpecialtyCode INNER JOIN
                                              dbo.OlapTheatre AS th ON th.TheatreCode = t.TheatreCode INNER JOIN
                                              dbo.OlapCalendar AS c ON c.TheDate = t.SessionDate
                       GROUP BY s.NationalSpecialtyCode, s.Specialty, th.Theatre, MONTH(t.SessionDate), c.FinancialYear) AS u
WHERE     (Planned > 0)
GROUP BY Specialty, Theatre, MonthNumber, Year, Planned, Productive, Utilisation

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "u"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 241
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwTheatreUtilDirectorateReport';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwTheatreUtilDirectorateReport';

