﻿/******************************************************************************
 Description: A&E Attendances export for the New Health Deal Trafford.  Data
              used by the Information Sub-Group for activity analysis. 
  
 Parameters:  @StartDate - Start date for data extract
              @EndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author       Change
 13/11/13 Tim Dean	   Created
 01/05/14 Graham Ryder Modified - added "and ae.AttendanceCategoryCode in('1','2')" in-line with other reports

******************************************************************************/

CREATE Procedure [AE].[AEAttendancesNewHealthDealTrafford]
 @StartDate as Date
,@EndDate as Date
as
select
  d.OrganisationCode [Organisation Code (Code of Provider]
 ,d.SiteCode [Provider Site Code]
 ,d.DepartmentType [A and E Department Type]
 ,d.CCGCode [Organisation Code (Code of Commissioner)]
 ,d.ArrivalDate [Arrival Date]
 ,d.ArrivalTimeBand [Arrival Time Band]
 ,d.ArrivalMode [A and E Arrival Mode]
 ,d.TriageCategoryCode [Triage Category Code]
 ,cast(d.AgeAtArrivalBand as varchar(10)) [Age at Arrival Band]
 ,count(*) [Cases]
from (
      select
        'RM2' OrganisationCode
        ,'RM203' SiteCode
        ,'01' DepartmentType
		,ae.CCGCode
		,ae.ArrivalDate
		,case
		   when datepart(HH,ae.ArrivalTime) < 8 then '00:00 to 07:59'
           else '08:00 to 23:59'
		 end ArrivalTimeBand
		,case ae.SourceOfReferralCode
			when ' 1' then 1 -- 999 Ambulance
			when '1' then 1 -- 999 Ambulance
			when '16' then 1 -- Air Ambulance
			when '21' then 1 -- 111 Ambulance
			else 2
		 end ArrivalMode
		,case
		   when ae.TriageCategoryCode in ('R','O','Y') then 1 -- Majors
		   else 2 -- Minors
		 end TriageCategoryCode
		,case
		   when ae.AgeOnArrival < 5 then '00-04'
		   when ae.AgeOnArrival >= 5 then '05+'
		 end AgeAtArrivalBand
	  from  WH.AE.Encounter ae
	  where ae.ArrivalDate between @StartDate and @EndDate
	  and ae.AttendanceCategoryCode in('1','2')
	) d
group by
	d.OrganisationCode
	,d.SiteCode
	,d.DepartmentType
	,d.CCGCode
	,d.ArrivalDate
	,d.ArrivalTimeBand
	,d.ArrivalMode
	,d.TriageCategoryCode
	,d.AgeAtArrivalBand