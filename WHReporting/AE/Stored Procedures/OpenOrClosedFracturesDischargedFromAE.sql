﻿/******************************************************************************
 Description: Lists patients for SSRS report: A and E - Open Or Closed Fractures Discharged From AE

 
 Modification History --------------------------------------------------------

 Date     Author		Change
 25/08/15 Graham Ryder  Created

******************************************************************************/

CREATE Procedure [AE].[OpenOrClosedFracturesDischargedFromAE]
 
as

select C.FirstDateOfMonth
,SourceUniqueID
,ArrivalTime
,LocalPatientID as RM2Number
,PatientForename
,PatientSurname
,DateOfBirth
,1 as OpenOrClosedFractureDischargedFromAE
from WH.AE.Encounter E
left join WHREPORTING.LK.Calendar C
on E.ArrivalDate=C.TheDate
where (DiagnosisCodeFirst in('052','053') or DiagnosisCodeSecond in('052','053'))
and AttendanceCategoryCode in('1','2')
and ArrivalDate>='11 aug 2015'
and AttendanceDisposalCode='DA'