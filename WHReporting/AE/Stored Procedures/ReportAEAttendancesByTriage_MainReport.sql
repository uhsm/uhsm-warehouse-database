﻿/*
--Author: N Scattergood	
--Date created: 21/06/2013
--Stored Procedure Built for SRSS Report on:
--A&E Attendances by Triage Decription
*/

CREATE Procedure [AE].[ReportAEAttendancesByTriage_MainReport]

@StartDate as Date
,@EndDate as Date
as
SELECT 


      Cal.TheMonth as ArrivalMonth
      ,case 
      when T.ManTriageDescription is null
      then '~ Left Blank ~'
      else rtrim(T.ManTriageDescription) 
      end as ManTriageDescription
     ,COUNT(*) as Attendances
	
      --,AE.[SourceUniqueID]
      --,AE.[UniqueBookingReferenceNo]
      --,AE.[PathwayId]
      --,AE.[PathwayIdIssuerCode]
  
      --,AE.[TrustNo]
      --,AE.[CasenoteNo]
      --,AE.[DistrictNoOrganisationCode]
      --,AE.[NHSNumber]
      --,AE.[NHSNumberStatusId]
      --,AE.[LocalPatientID]
      --,AE.[PatientTitle]
      --,AE.[PatientForename]
      --,AE.[PatientSurname]
      --,AE.[PatientAddress1]
      --,AE.[PatientAddress2]
      --,AE.[PatientAddress3]
      --,AE.[PatientAddress4]
      --,AE.[Postcode]
      --,AE.[DateOfBirth]
      --,AE.[DateOfDeath]
      --,AE.[SexCode]
      --,AE.[CarerSupportIndicator]
      --,AE.[RegisteredGpCode]
      --,AE.[RegisteredGpName]
      --,AE.[RegisteredGpPracticeCode]
      --,AE.[RegisteredGpPracticeName]
      --,AE.[RegisteredGpPracticePostcode]
      --,AE.[Consultant]
      --,AE.[AttendanceNumber]
      --,AE.[ArrivalModeCode]
      --,AE.[AttendanceCategoryCode]
      --,AE.[AttendanceDisposalCode]
      --,AE.[IncidentLocationTypeCode]
      --,AE.[PatientGroupCode]
      --,AE.[SourceOfReferralCode]
      --,AE.[ArrivalDate]
      --,AE.[ArrivalTime]
      --,AE.[AgeOnArrival]
      --,AE.[InitialAssessmentTime]
      --,AE.[SeenForTreatmentTime]
      --,AE.[AttendanceConclusionTime]
      --,AE.[DepartureTime]
      --,AE.[CommissioningSerialNo]
      --,AE.[NHSServiceAgreementLineNo]
      --,AE.[ProviderReferenceNo]
      --,AE.[CommissionerReferenceNo]
      --,AE.[ProviderCode]
      --,AE.[CommissionerCode]
      --,AE.[CommissionerCodeCCG]
      --,AE.[StaffMemberCode]
      --,AE.[InvestigationCodeFirst]
      --,AE.[InvestigationCodeSecond]
      --,AE.[DiagnosisCodeFirst]
      --,AE.[DiagnosisCodeSecond]
      --,AE.[CascadeDiagnosisCode]
      --,AE.[TreatmentCodeFirst]
      --,AE.[TreatmentCodeSecond]
      --,AE.[PASHRGCode]
      --,AE.[HRGVersionCode]
      --,AE.[PASDGVPCode]
      --,AE.[SiteCode]
      --,AE.[Created]
      --,AE.[Updated]
      --,AE.[ByWhom]
      --,AE.[InterfaceCode]
      --,AE.[PCTCode]
      --,AE.[CCGCode]
      --,AE.[ResidencePCTCode]
      --,AE.[ResidenceCCGCode]
      --,AE.[SourceOfCCGCode]
      --,AE.[InvestigationCodeList]
      --,AE.[TriageCategoryCode]
      --,AE.[PresentingProblem]
      --,AE.[ToXrayTime]
      --,AE.[FromXrayTime]
      --,AE.[ToSpecialtyTime]
      --,AE.[SeenBySpecialtyTime]
      --,AE.[EthnicCategoryCode]
      --,AE.[ReferredToSpecialtyCode]
      --,AE.[DecisionToAdmitTime]
      --,AE.[DischargeDestinationCode]
      --,AE.[RegisteredTime]
      --,AE.[Attends]
      --,AE.[WhereSeen]
      --,AE.[EncounterDurationMinutes]
      --,AE.[BreachReason]
      --,AE.[AdmitToWard]
      --,AE.[AdviceTime]
      --,AE.[StandbyTime]
      --,AE.[TrolleyDurationMinutes]
      --,AE.[ManchesterTriageCode]
      --,AE.[AlcoholAuditParticipation]
      --,AE.[AlcoholAuditFrequency]
      --,AE.[AlcoholAuditTypicalDay]
      --,AE.[AlcoholAuditSingleOccasion]
      --,AE.[AlcoholAuditLocation]
      
  FROM [WH].[AE].[Encounter] AE
  
	left join [WH].[AE].[ManchesterTriageLookup] T
	on AE.ManchesterTriageCode = T.ManTriageCode
	
		--left join [WH].[AE].[AttendanceCategory] AC
		--on AC.AttendanceCategoryCode = AE.AttendanceCategoryCode
		
		--	left join [WH].[AE].[AttendanceDisposal] AD
		--	on AE.AttendanceDisposalCode = AD.AttendanceDisposalCode
			
				left join WH.WH.Calendar Cal
				on AE.ArrivalDate = Cal.TheDate
  
  where 
	AE.ArrivalDate between @StartDate and @EndDate
and
	AE.AttendanceCategoryCode in ('1','2')



Group by 
     Cal.TheMonth
     ,Cal.FinancialMonthKey
     ,case 
      when T.ManTriageDescription is null
      then '~ Left Blank ~'
      else rtrim(T.ManTriageDescription) 
      end 
      
Order by
    case 
      when T.ManTriageDescription is null
      then '~ Left Blank ~'
      else rtrim(T.ManTriageDescription) 
      end 
--,Cal.FinancialMonthKey