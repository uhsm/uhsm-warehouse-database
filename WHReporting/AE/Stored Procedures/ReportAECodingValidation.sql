﻿/******************************************************************************
 Description: Used to extract data for the A&E Coding Validation SSRS report.
  
 Parameters:  @StartDate - Start date for data extract
              @EndDate - End date for data extract
              @CheckColumnName - Name of the column in table to check if populated
              @WhereSeenStatus - Either Included or Excluded where seen
 
 Modification History --------------------------------------------------------

 Date     Author      Change
 14/11/13 Tim Dean	  Created

******************************************************************************/

CREATE Procedure [AE].[ReportAECodingValidation]
 @StartDate as Date
,@EndDate as Date
,@CheckColumnName as varchar(50)
,@WhereSeenStatus as varchar(10)
as

Declare @Query as varchar(1100)

Set @Query = '
select
  ae1.AttendanceMonth
 ,ae1.WhereSeenName
 ,ae1.CodedStatus
 ,count(*) Attendances
from (
      select
        dateadd(d,-1 * (day(ae.ArrivalDate)-1),ae.ArrivalDate) AttendanceMonth
       ,case ae.WhereSeen
          when 0 then ''0 Main Dept''
          when 1 then ''1 Main Dept''
          when 2 then ''2 UCC Minor''
          when 3 then ''3 Deflection''
          when 4 then ''4 GP Out of Hrs''
          when 5 then ''5 GPAU''
          else ''Unknown Location''
        end WhereSeenName 
       ,case
          when ae.WhereSeen = 3 then ''Excluded''
          when ae.WhereSeen = 4 then ''Excluded''
          else ''Included''
        end WhereSeenStatus
       ,case
          when ae.' + @CheckColumnName + ' is not null then ''Coded''
          else ''NOT Coded''
        end CodedStatus
      from WH.AE.Encounter ae
      where ae.ArrivalDate between ''' + convert(varchar(10),@StartDate,101) + ''' and ''' + convert(varchar(10),@EndDate,101) + '''
     ) ae1
where ae1.WhereSeenStatus = ''' + @WhereSeenStatus + '''
group by ae1.AttendanceMonth
        ,ae1.WhereSeenName
        ,ae1.CodedStatus
order by ae1.AttendanceMonth' 

Exec (@Query)