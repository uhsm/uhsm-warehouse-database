﻿/******************************************************************************
 Description: Used to provide drill through data for the A&E Coding Validation 
              SSRS report.
  
 Parameters:  @StartDate - Current selected month on report (1st of Month)
              @CheckColumnName - Current selected check item on report
              @WhereSeenName - Current selected where seen name item on report
              @CodedStatus - Current selected coded status item on report
 
 Modification History --------------------------------------------------------

 Date     Author      Change
 15/11/13 Tim Dean	  Created

******************************************************************************/

CREATE Procedure [AE].[ReportAECodingValidation_DrillThrough]
 @StartDate as Date
,@CheckColumnName as varchar(50)
,@WhereSeenName as varchar(50)
,@CodedStatus as varchar(10)
as

declare @EndDate as date
declare @Query as varchar(2000)

set @EndDate = dateadd(d,-1,dateadd(m,1,@StartDate))

Set @Query = '
select
  ae1.AttendanceMonth
 ,ae1.DistrictNo
 ,ae1.CasenoteNo
 ,ae1.NHSNumber
 ,ae1.LocalPatientID
 ,ae1.ArrivalDate
 ,ae1.SeenBy
 ,ae1.InvestigationCodeFirst
 ,ae1.DiagnosisCodeFirst
 ,ae1.TreatmentCodeFirst
 ,ae1.WhereSeenName
 ,''' + @CheckColumnName + ''' CheckItem
 ,ae1.CodedStatus
from (
      select
        dateadd(d,-1 * (day(ae.ArrivalDate)-1),ae.ArrivalDate) AttendanceMonth
       ,ae.DistrictNo
       ,ae.CasenoteNo
       ,ae.NHSNumber
       ,ae.LocalPatientID
       ,ae.ArrivalDate
       ,ae.SeenBy
       ,ae.InvestigationCodeFirst
       ,ae.DiagnosisCodeFirst
       ,ae.TreatmentCodeFirst
       ,case ae.WhereSeen
          when 0 then ''0 Main Dept''
          when 1 then ''1 Main Dept''
          when 2 then ''2 UCC Minor''
          when 3 then ''3 Deflection''
          when 4 then ''4 GP Out of Hrs''
          when 5 then ''5 GPAU''
          else ''Unknown Location''
        end WhereSeenName 
       ,case
          when ae.WhereSeen = 3 then ''Excluded''
          when ae.WhereSeen = 4 then ''Excluded''
          else ''Included''
        end WhereSeenStatus
       ,case
          when ae.' + @CheckColumnName + ' is not null then ''Coded''
          else ''NOT Coded''
        end CodedStatus
      from WH.AE.Encounter ae
      where ae.ArrivalDate between ''' + convert(varchar(10),@StartDate,101) + ''' and ''' + convert(varchar(10),@EndDate,101) + '''
     ) ae1
where ae1.WhereSeenName = ''' + @WhereSeenName + '''
      and ae1.CodedStatus = ''' + @CodedStatus + '''
order by ae1.ArrivalDate'

Exec (@Query)