﻿/*
--Author: J Cleworth
--Date created: 20/05/2013
--AE Alcohol Report Consultants
--Amended by Graham Ryder on 10/06/2014 to change ArrivalModeCode to SourceOfReferralCode due to
--change in population of the ArrivalModeCode field so that it no longer matches SourceOfReferralCode
--AE Alcohol Report
*/

CREATE Procedure [AE].[ReportAlcoholConsultants]

@DateFrom date,
@DateTo date
as

select
distinct
C.financialyear,
c.TheMonth,
c.FinancialMonthKey,
coalesce(ae.seenBy,'Unknown') as Consultant
,

      sum(case when [AlcoholAuditParticipation]='Impaired' then 1 else 0 end) as Impairments
      ,sum(Case when alcoholauditparticipation is not null then 1 else 0 end)-sum(case when [AlcoholAuditParticipation]='Impaired' then 1 else 0 end) as Screened
      ,Sum(Case when alcoholauditparticipation IN ('Agreed') then 1 else 0 end) as Agreed
      ,Sum(Case when alcoholauditparticipation IN ('Refused') then 1 else 0 end) as Refused
     ,count(SourceUniqueID) as EligiblePatients

  FROM [WH].[AE].[Encounter] ae
  inner join whreporting.lk.calendar c on c.TheDate=ae.ArrivalDate
  
  where ArrivalDate > '2012-03-31'
  AND FinancialMonthKey<=(SELECT FinancialMonthKey FROM WHREPORTING.LK.Calendar where TheDate=CAST(getdate() as DATE))
  
 -- and alcoholauditparticipation in('Agreed')
  

AND AttendanceCategoryCode in ('1','2') --Only New Pts (Excludes Review Pts)
AND AttendanceDisposalCode not in ('DI','X') --Exclude Pts who died or Pts who Did Not Wait
AND (AgeOnArrival >=16 OR AgeOnArrival is null)--Excludes Pts under 16 
AND WhereSeen not in ('4','3')--Exclude Pts seen in GPOOH & Deflections
AND TriageCategoryCode not in ('R','O')--Excludes Pts with Red & Orange Triage
--AND ArrivalModeCode not in (' 3','19')-- Exclude Bed Bureau Attends (& Bed Bur RR)
AND SourceOfReferralCode not in ('3','19')-- Exclude Bed Bureau Attends (& Bed Bur RR)
and ArrivalDate between @DateFrom and @DateTo
group by C.financialyear,c.TheMonth,
c.FinancialMonthKey,
coalesce(ae.seenby,'Unknown')
order by c.FinancialMonthKey