﻿/*****************************************
Name: Peter Asemota
Date: 18/03/2016
Description : Script for populating KPI for the OP KPI report at Consultant level.

***************************************/

CREATE PROCEDURE [AE].[ReportAttendance_Breaches]
 (  
   @StartDate As Datetime
  ,@EndDate As Datetime 
  
 
 )
	
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED


set  Dateformat   DMY

SET  @EndDate  =DATEADD(day, -1,getdate())

SELECT  CONVERT(VARCHAR(10), ArrivalDate, 103) AS [Arrival Date]
             , ArrivalDate
              , DATENAME(DW,ArrivalDate) As [WeekDay]
              ,COUNT([CurrentEncounter].[ArrivalDate]) AS Attendances
	,SUM(CASE 
			WHEN DATEDIFF(MINUTE, [CurrentEncounter].[ArrivalTime], [CurrentEncounter].[AttendanceConclusionTime]) > '240'
				THEN 1
			ELSE 0
			END) + SUM(CASE 
			WHEN [CurrentEncounter].[AttendanceConclusionTime] IS NULL
				THEN (
						CASE 
							WHEN DATEDIFF(MINUTE, [CurrentEncounter].[ArrivalTime], GETDATE()) > '240'
								THEN 1
							ELSE 0
							END
						)
			ELSE 0
			END) AS Breaches
	,SUM(CASE 
			WHEN (DATEDIFF(MINUTE, [CurrentEncounter].[DecisionToAdmitTime], [CurrentEncounter].[AttendanceConclusionTime])) > '720'
				THEN 1
			ELSE 0
			END) + SUM(CASE 
			WHEN [CurrentEncounter].[AttendanceConclusionTime] IS NULL
				THEN (
						CASE 
							WHEN DATEDIFF(MINUTE, [CurrentEncounter].[DecisionToAdmitTime], GETDATE()) > '720'
								THEN 1
							ELSE 0
							END
						)
			ELSE 0
			END) AS [TrolleyWaits>12hrs]
FROM [WH].[AE].[CurrentEncounter]
WHERE [CurrentEncounter].[ArrivalDate] >= @StartDate
      and [CurrentEncounter].[ArrivalDate] <= @EndDate
--'01 FEB 2016'
	AND AttendanceCategoryCode IN (
		'1'   -- returning only initial and self return attendance categories
		,'2'
		)
GROUP BY [CurrentEncounter].[ArrivalDate]
ORDER BY [CurrentEncounter].[ArrivalDate] DESC

END