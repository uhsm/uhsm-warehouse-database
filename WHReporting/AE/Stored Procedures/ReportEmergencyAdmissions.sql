﻿/*****************************************
Name: Peter Asemota
Date: 18/03/2016
Description : Script for populating KPI for the OP KPI report at Consultant level.

***************************************/

CREATE PROCEDURE [AE].[ReportEmergencyAdmissions]
 (  
   @StartDate As Datetime
  ,@EndDate As Datetime 
  
 )
	
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED


set  Dateformat   DMY



SET @EndDate  =DATEADD(day, -1,getdate())

SELECT CONVERT(VARCHAR(10), S.[AdmissionDate], 103) AS [Admission Date]
        ,S.[AdmissionDate] as AdmissionDate
       ,DATENAME(DW,S.[AdmissionDate]) As [WeekDay]
	  ,SUM(CASE 
			WHEN S.[AdmissionMethodNHSCode] IN (
					 '21' -- emergency admission codes
					,'22'
					,'23'
					,'24'
					,'28'
					)
				THEN 1
			ELSE 0
			END) AS [All Emergency Admissions]
   ,AES.[AE Emergency Admissions] as [AE Emergency Admissions]
FROM [WHREPORTING].[APC].[Spell] S
LEFT JOIN (
	SELECT CONVERT(VARCHAR(10), [AttendanceConclusionTime], 103) AS [AttendanceConclusionTime]
	     ,DATENAME(DW,[AttendanceConclusionTime]) As [WeekDay]
		,COUNT(*) AS [AE Emergency Admissions]
	FROM WH.AE.CurrentEncounter AE
	WHERE AE.AttendanceDisposalCode = 'AD'
		 AND cast([AttendanceConclusionTime] AS DATE) >=  @StartDate --'01 mar 2016'
		 AND cast([AttendanceConclusionTime] AS DATE) <=  @EndDate  -- (dateadd(day, - 1, cast(getdate() AS DATE)))
	GROUP BY CONVERT(VARCHAR(10), [AttendanceConclusionTime], 103)
	        ,DATENAME(DW,[AttendanceConclusionTime])
	) AES ON AES.AttendanceConclusionTime = CONVERT(VARCHAR(10), [AdmissionDate], 103)
WHERE S.[AdmissionDate] >= @StartDate
	AND S.[AdmissionDate] <= @EndDate
	--AND  S.[AdmissionMethodNHSCode] BETWEEN '11'AND '28'

GROUP BY S.[AdmissionDate]
	    ,AES.[AE Emergency Admissions]
	    
ORDER BY S.[AdmissionDate]

END