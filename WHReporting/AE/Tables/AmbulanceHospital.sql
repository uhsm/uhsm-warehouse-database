﻿CREATE TABLE [AE].[AmbulanceHospital](
	[Hospital Key] [tinyint] IDENTITY(1,1) NOT NULL,
	[Hospital Name] [varchar](100) NOT NULL,
	[Clinic Attended] [varchar](100) NOT NULL,
	[NWAS Area] [varchar](50) NOT NULL,
 CONSTRAINT [PK_AmbulanceHospital] PRIMARY KEY CLUSTERED 
(
	[Hospital Key] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]