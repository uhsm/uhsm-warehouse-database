﻿CREATE TABLE [AE].[AmbulanceLateTurnaroundReason](
	[Late Turnaround Reason Key] [tinyint] IDENTITY(1,1) NOT NULL,
	[Late Turnaround Reason] [varchar](300) NOT NULL,
 CONSTRAINT [PK_AmbulanceLateTurnaroundReason] PRIMARY KEY CLUSTERED 
(
	[Late Turnaround Reason Key] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]