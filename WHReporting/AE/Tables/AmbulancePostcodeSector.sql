﻿CREATE TABLE [AE].[AmbulancePostcodeSector](
	[Postcode Sector Key] [smallint] IDENTITY(1,1) NOT NULL,
	[Postcode Sector] [varchar](10) NOT NULL,
 CONSTRAINT [PK_AmbulancePostcodeSector] PRIMARY KEY CLUSTERED 
(
	[Postcode Sector Key] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]