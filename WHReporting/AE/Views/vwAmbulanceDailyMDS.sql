﻿CREATE view [AE].[vwAmbulanceDailyMDS] as

SELECT h.[Hospital Name]
	,h.[Clinic Attended]
	,d.[NWAS Call Number]
	,d.[Date Of Call]
	,d.[Date At Hospital]
	,p.[Postcode Sector]
	,c.[Call Category]
	,n.[Call Nature]
	,d.[Vehicle Callsign]
	,h.[NWAS Area]
	,d.[Time At Hospital]
	,d.[Time Hospital Notified]
	,d.[Time Of Handover]
	,d.[Time Vehicle Clear]
	,d.[Overall Turnaround Time Mins]
	,d.[HAS Penalty Candidate]
	,d.[NWAS Time Stamp Present]
	,d.[Acute Time Stamp Present]
	,d.[Rapid Handover Count]
	,d.[ARRV To NTFY Mins]
	,d.[NTFY To HOVR Mins]
	,d.[HOVR To CLR Mins]
	,d.[NWAS Breach]
	,d.[Acute Breach]
	,d.[N2H 30m 60m]
	,d.[N2H 60m 120m]
	,d.[N2H Over 120m]
	,d.[Extract From Date]
	,d.[Extract To Date]
	,d.[Extract Run Date]
	,l.[Late Turnaround Reason]
FROM WHREPORTING.AE.AmbulanceDailyMDS AS d
LEFT JOIN WHREPORTING.AE.AmbulanceHospital AS h ON (d.[Hospital Key] = h.[Hospital Key])
LEFT JOIN WHREPORTING.AE.AmbulanceCallCategory AS c ON (d.[Call Category Key] = c.[Call Category Key])
LEFT JOIN WHREPORTING.AE.AmbulanceCallNature AS n ON (d.[Call Nature Key] = n.[Call Nature Key])
LEFT JOIN WHREPORTING.AE.AmbulancePostcodeSector AS p ON (d.[Postcode Sector Key] = p.[Postcode Sector Key])
LEFT JOIN WHREPORTING.AE.AmbulanceLateTurnaroundReason AS l ON (d.[Late Turnaround Reason Key] = l.[Late Turnaround Reason Key]);