﻿CREATE Procedure [APC].[BuildClinicalCoding] As

Declare @SQL varchar(8000)
Declare @Sequence varchar(2)
Declare @Order varchar(2)
declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

If Exists (Select * from INFORMATION_SCHEMA.TABLES Where Table_Schema = 'APC' and TABLE_NAME = 'ClinicalCoding')
DROP TABLE APC.ClinicalCoding;

CREATE TABLE APC.ClinicalCoding
(
	  EncounterRecno int not null
	 ,SourceUniqueID int not null
	,CodingComplete char(1) not null
	,PrimaryDiagnosisCode  varchar(10)
	,PrimaryDiagnosis varchar(280)
	,SubsidiaryDiagnosisCode varchar(10)
	,SubsidiaryDiagnosis varchar(280)
	,SecondaryDiagnosisCode1 varchar(10)
	,SecondaryDiagnosis1 varchar(280)
	,SecondaryDiagnosisCode2 varchar(10)
	,SecondaryDiagnosis2 varchar(280)
	,SecondaryDiagnosisCode3 varchar(10)
	,SecondaryDiagnosis3 varchar(280)
	,SecondaryDiagnosisCode4 varchar(10)
	,SecondaryDiagnosis4 varchar(280)
	,SecondaryDiagnosisCode5 varchar(10)
	,SecondaryDiagnosis5 varchar(280)
	,SecondaryDiagnosisCode6 varchar(10)
	,SecondaryDiagnosis6 varchar(280)
	,SecondaryDiagnosisCode7 varchar(10)
	,SecondaryDiagnosis7 varchar(280)
	,SecondaryDiagnosisCode8 varchar(10)
	,SecondaryDiagnosis8 varchar(280)
	,SecondaryDiagnosisCode9 varchar(10)
	,SecondaryDiagnosis9 varchar(280)
	,SecondaryDiagnosisCode10 varchar(10)
	,SecondaryDiagnosis10 varchar(280)
	,SecondaryDiagnosisCode11 varchar(10)
	,SecondaryDiagnosis11 varchar(280)
	,SecondaryDiagnosisCode12 varchar(10)
	,SecondaryDiagnosis12 varchar(280)
	--IS Added the following extra Diagnosis Code 04/01/2012
	,SecondaryDiagnosisCode13 varchar(10)
	,SecondaryDiagnosis13 varchar(280)
	
	,SecondaryDiagnosisCode14 varchar(10)
	,SecondaryDiagnosis14 varchar(280)
	
	,SecondaryDiagnosisCode15 varchar(10)
	,SecondaryDiagnosis15 varchar(280)
	
	,SecondaryDiagnosisCode16 varchar(10)
	,SecondaryDiagnosis16 varchar(280)
		
	,SecondaryDiagnosisCode17 varchar(10)
	,SecondaryDiagnosis17 varchar(280)
	
	,SecondaryDiagnosisCode18 varchar(10)
	,SecondaryDiagnosis18 varchar(280)
	
	,SecondaryDiagnosisCode19 varchar(10)
	,SecondaryDiagnosis19 varchar(280)
	
	,SecondaryDiagnosisCode20 varchar(10)
	,SecondaryDiagnosis20 varchar(280)
	
	,PriamryProcedureCode varchar(10)
	,PriamryProcedure varchar(280)
	,PrimaryProcedureDate smallDateTime
	,SecondaryProcedureCode1 varchar(10)
	,SecondaryProcedure1 varchar(280)
	,SecondaryProcedure1Date smalldatetime
	,SecondaryProcedureCode2 varchar(10)
	,SecondaryProcedure2 varchar(280)
	,SecondaryProcedure2Date smalldatetime
	,SecondaryProcedureCode3 varchar(10)
	,SecondaryProcedure3 varchar(280)
	,SecondaryProcedure3Date smalldatetime
	,SecondaryProcedureCode4 varchar(10)
	,SecondaryProcedure4 varchar(280)
	,SecondaryProcedure4Date smalldatetime
	,SecondaryProcedureCode5 varchar(10)
	,SecondaryProcedure5 varchar(280)
	,SecondaryProcedure5Date smalldatetime
	,SecondaryProcedureCode6 varchar(10)
	,SecondaryProcedure6 varchar(280)
	,SecondaryProcedure6Date smalldatetime
	,SecondaryProcedureCode7 varchar(10)
	,SecondaryProcedure7 varchar(280)
	,SecondaryProcedure7Date smalldatetime
	,SecondaryProcedureCode8 varchar(10)
	,SecondaryProcedure8 varchar(280)
	,SecondaryProcedure8Date smalldatetime
	,SecondaryProcedureCode9 varchar(10)
	,SecondaryProcedure9 varchar(280)
	,SecondaryProcedure9Date smalldatetime
	,SecondaryProcedureCode10 varchar(10)
	,SecondaryProcedure10 varchar(280)
	,SecondaryProcedure10Date smalldatetime
	,SecondaryProcedureCode11 varchar(10)
	,SecondaryProcedure11 varchar(280)
	,SecondaryProcedure11Date smalldatetime
	);

Create Unique Clustered Index ixcClinicalCoding on APC.ClinicalCoding(EncounterRecno)
Create Unique Index ixSourceUniqueID on APC.ClinicalCoding(SourceUniqueID)


Insert into APC.ClinicalCoding
	(
	 [EncounterRecno]
	,[SourceUniqueID]
	,[CodingComplete]
	 )
	(
	Select 
		 EncounterRecno
		,SourceUniqueID
		,CodingCompleteFlag
	FROM WHOLAP.dbo.OlapAPC Encounter
	);

--UPDATE DIAGNOSIS CODES
--Primary Diagnosis
Update APC.ClinicalCoding
Set 
	 PrimaryDiagnosisCode = ClinCod.CodingLocalCode
	,PrimaryDiagnosis = ClinCod.Coding
From APC.ClinicalCoding Encounter
	left outer join WHOLAP.dbo.FactClinicalCoding PatCod
		on Encounter.SourceUniqueID = PatCod.SourceRecno
			and PatCod.SourceCode = 'PRCAE'
			and patcod.ClinicalCodingTypeCode = 'DIAGN'
			and PatCod.SequenceNo = 1
	left outer join WHOLAP.dbo.OlapCoding ClinCod
		on PatCod.ClinicalCodingCode = ClinCod.CodingCode;

--Subsidiary Diagnosis
Update APC.ClinicalCoding
Set 
	 SubsidiaryDiagnosisCode = ClinCod.CodingLocalCode
	,SubsidiaryDiagnosis = ClinCod.Coding
From APC.ClinicalCoding Encounter
	left outer join WHOLAP.dbo.FactClinicalCoding PatCod
		on Encounter.SourceUniqueID = PatCod.SourceRecno
			and PatCod.SourceCode = 'PRCAE'
			and patcod.ClinicalCodingTypeCode = 'DIAGN'
			and PatCod.SequenceNo = 2
	left outer join WHOLAP.dbo.OlapCoding ClinCod
		on PatCod.ClinicalCodingCode = ClinCod.CodingCode;
		
--Secondary Diagnsosis 1 through 12 

Set @Sequence = 3
Set @Order = 1

WHILE @Order <21
BEGIN
	Set @SQL = 
			'Update APC.ClinicalCoding
			Set 
				 SecondaryDiagnosisCode' + @Order + '  = ClinCod.CodingLocalCode
				,SecondaryDiagnosis' + @Order +'  = ClinCod.Coding
				
			From APC.ClinicalCoding Encounter
				inner join WHOLAP.dbo.FactClinicalCoding PatCod
					on Encounter.SourceUniqueID = PatCod.SourceRecno
						and PatCod.SourceCode = ''PRCAE''
						and patcod.ClinicalCodingTypeCode = ''DIAGN''
						and PatCod.SequenceNo = ' + @Sequence +'
				inner join WHOLAP.dbo.OlapCoding ClinCod
					on PatCod.ClinicalCodingCode = ClinCod.CodingCode '
	exec(@SQL)
		set @Order = @Order + 1
		set @Sequence = @Sequence +1
END;


--PROCEDURE CODING
--Primary Procedure
Update APC.ClinicalCoding
Set 
	 PriamryProcedureCode = ClinCod.CodingLocalCode
	,PriamryProcedure = ClinCod.Coding
	,PrimaryProcedureDate = PatCod.ClinicalCodingDate
From APC.ClinicalCoding Encounter
	left outer join WHOLAP.dbo.FactClinicalCoding PatCod
		on Encounter.SourceUniqueID = PatCod.SourceRecno
			and PatCod.SourceCode = 'PRCAE'
			and patcod.ClinicalCodingTypeCode = 'PROCE'
			and PatCod.SequenceNo = 1
	left outer join WHOLAP.dbo.OlapCoding ClinCod
		on PatCod.ClinicalCodingCode = ClinCod.CodingCode;


--Secondary Procedures 1 through 11
Set @Sequence = 2
Set @Order = 1

WHILE @Order <12
BEGIN
	Set @SQL = 
			'Update APC.ClinicalCoding
			Set 
				 SecondaryProcedureCode' + @Order + '  = ClinCod.CodingLocalCode
				,SecondaryProcedure' + @Order +'  = ClinCod.Coding
				,SecondaryProcedure' + @Order +'Date = PatCod.ClinicalCodingDate
			From APC.ClinicalCoding Encounter
				inner join WHOLAP.dbo.FactClinicalCoding PatCod
					on Encounter.SourceUniqueID = PatCod.SourceRecno
						and PatCod.SourceCode = ''PRCAE''
						and patcod.ClinicalCodingTypeCode = ''PROCE''
						and PatCod.SequenceNo = ' + @Sequence +'
				inner join WHOLAP.dbo.OlapCoding ClinCod
					on PatCod.ClinicalCodingCode = ClinCod.CodingCode '
	exec(@SQL)
		set @Order = @Order + 1
		set @Sequence = @Sequence +1
END;


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WHREPORTING APC.BuildClinicalCoding', @Stats, @StartTime