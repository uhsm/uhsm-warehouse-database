﻿CREATE Procedure [APC].[BuildCriticalCarePeriod] as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

If Exists (Select * from INFORMATION_SCHEMA.TABLES Where Table_Schema = 'APC' and TABLE_NAME = 'CriticalCarePeriod')
DROP TABLE APC.CriticalCarePeriod;


Select 
	 ccp.SourceCCPNo 
	,ccp.CCPSequenceNo
	,ccp.SourceEncounterNo
	,ccp.SourceSpellNo
	,ccp.CCPIndicator
	,ccp.CCPDischargeStatus
	,ccp.CCPDischargeStatusDescription
	,CriticalCareStartDatetime = ccp.CCPStartDate
	,CriticalCareEndDatetime = ccp.CCPEndDate
	,ReadyDate = ccp.CCPReadyDate
	,AdmissionSourceCode = ads.CCPAdmissionSourceCode1
	,AdmissionSource = ads.CCPAdmissionSource
	,AdmissionTypeCode = adt.CCPAdmissionTypeCode1
	,AdmissionType = adt.CCPAdmissionType
	,AdmissionSourceLocationCode = sl.CCPSourceLocationCode1
	,AdmissionSourceLocation = sl.CCPSourceLocation
	,UnitFunctionCode = uf.CCPUnitFunctionCode1
	,UnitFunction = uf.CCPUnitFunction
	,UnitBedFunctionCode = ubf.CCPUnitBedConfigCode1
	,UnitBedFunction = ubf.CCPUnitBedConfig
	,DischargeStatusCode = ds.CCPDischargeStatusCode1
	,DischargeStatus = ds.CCPDischargeStatus
	,DischargeDestinationCode = dd.CCPDischargeDestinationCode1
	,DischargeDestination = dd.CCPDischargeDestination
	,DischargeLocationCode = dl.CCPDischargeLocationCode1
	,DischargeLocation = dl.CCPDischargeLocation
	,cdays.[Advanced Cardiovascular Support Days]
	,cdays.[Advanced Respiratory Support Days]
	,cdays.[Basic Cardiovascular Support Days]
	,cdays.[Basic Respiratory Support Days]
	,cdays.[Circulatory Support Days]
	,cdays.[Dermatological Support Days]
	,cdays.[Enhanced Nursing Care Days]
	,cdays.[Gastrointestinal Support Days]
	,cdays.[Hepatic Days]
	,cdays.[Liver Support Days]
	,cdays.[Neurological Support Days]
	,cdays.[Not Specified Support Days]
	,cdays.[Renal Support Days]
	,cdays.AllSupported
	,[HDU Days] = ccp.CCPHDUDays
	,[ICU Days] = ccp.CCPICUDays
INTO APC.CriticalCarePeriod	
from Lorenzo.dbo.ExtractCriticalCarePeriod ccp
left outer join Lorenzo.dbo.vwCriticalCareOrganSupportDays cdays
	on ccp.SourceCCPNo = cdays.APCDT_REFNO
left outer join wholap.dbo.OlapCCPAdmissionSource ads
	on ccp.CCPAdmissionSource = ads.CCPAdmissionSourceCode
left outer join wholap.dbo.OlapCCPAdmissionType adt
	on ccp.CCPAdmissionType = adt.CCPAdmissionTypeCode
left outer join wholap.dbo.OlapCCPDischargeDestination dd
	on ccp.CCPDischargeDestination = dd.CCPDischargeDestinationCode
left outer join wholap.dbo.OlapCCPDischargeLocation dl
	on ccp.CCPDischargeLocation = dl.CCPDischargeLocationCode
left outer join wholap.dbo.OlapCCPDischargeStatus ds
	on ccp.CCPDischargeStatus = ds.CCPDischargeStatusCode
left outer join wholap.dbo.OlapCCPSourceLocation sl
	on ccp.CCPAdmissionSourceLocation = sl.CCPSourceLocationCode
left outer join wholap.dbo.OlapCCPUnitBedConfiguration ubf
	on ccp.CCPUnitBedFunction = ubf.CCPUnitBedConfigCode
left outer join wholap.dbo.OlapCCPUnitFunction uf
	on ccp.CCPUnitFunction = uf.CCPUnitFunctionCode


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WHREPORTING BuildCriticalCarePeriod', @Stats, @StartTime