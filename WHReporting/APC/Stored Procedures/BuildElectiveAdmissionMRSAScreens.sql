﻿CREATE Procedure [APC].[BuildElectiveAdmissionMRSAScreens]
AS 

/*****************************************************************************
* Procedure Name:		APC.BuildElectiveAdmissionMRSAScreens
* Procedure Desc:		Builds a table which combines Elective Admissions joined to MRSA screens which have been recorded on ICE.
						Table is to support the developement of an SSRS report which is aimed at replacing a legacy MRSA screening database 
						REquested by Jay Turner-Garnder via Alison Olivant / David Bell-Hartley
* Parameters:			N/A
* Returns:				N/A
* Calling Mechanism:	Stored procedure incorporated into the build of the WHReporting process - dbo.BuildReportingModel
* Tables and Alias		WHReporting.APC.ElectiveAdmissionMRSAScreens
* Definitions:			
*
* Modification History --------------------------------------------------------
Versions:	1.0.0.0 - 24/12/2015 - CM
				Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/




Truncate table  WHReporting.APC.ElectiveAdmissionMRSAScreens
;


--this logic follows the Endoscopy Exclusions in the Access Database
--note in the access database the endoscopy lookup table has additional OPCS codes like L661 + Z... - not sure that these we being excluded in the old process
WITH EndoExcl
AS (
	SELECT DISTINCT Episode.SourceSpellNo
		,Episode.EpisodeUniqueID
		,PrimaryProcedureCode
		,Spell.PatientClass
	FROM WHREPORTING.APC.Episode
	INNER JOIN WHREPORTING.APC.Spell ON Episode.SourcespellNo = Spell.SourceSpellNo
	WHERE PrimaryProcedureCode IN (
			SELECT OPCSCode
			FROM WHREPORTING.dbo.MRSAExclusions
			WHERE EndoscopyFlag = 1
			) --this is the list of Endocscopy procedures detailed in the Access lookup table. 
		--Logic in Access replication - looks only at the prim procedure position for these
		AND Episode.FirstEpisodeInSpell = 1 --only want the first episode in the spell - as per logic in the access db
		AND Spell.PatientClass = 'DAY CASE' --only looking for endoscopy OPCS codes for Day case spells to be excluded ? unsure why - same as logic in access db
	)
	,
	--Elective admissions for the OPCS codes  which are not flagged as 1 in the 'IncludeForMRSA' table need to be excluded 
	--logic as per the access database but also similarly mirrored in the policy doc.
OtherExcl
AS (
	SELECT Episode.SourceSpellNo
		,Episode.EpisodeUniqueID
		,Episode.PrimaryProcedureCode
		,Spell.PatientClass
	FROM WHREPORTING.APC.Episode
	INNER JOIN WHREPORTING.APC.Spell ON Episode.SourcespellNo = Spell.SourceSpellNo
	INNER JOIN (
		SELECT *
		FROM WHREPORTING.dbo.MRSAExclusions
		WHERE IncludeForMRSA = 0
		) excl --These are the procedures which have been marked as to be excluded in the access database lookup table
		ON Episode.PrimaryProcedureCode = excl.OPCSCode
	WHERE Episode.FirstEpisodeInSpell = 1
		AND Spell.PatientClass IN (
			'DAY CASE'
			,'ELECTIVE INPATIENT'
			)
	)
	,WardExcl --list of spells admitted onto wards which are not subject to MRSA screening
AS (
	SELECT SourceSpellNo
		,AdmissiontWardCode
	FROM WHREPORTING.APC.Spell
	WHERE AdmissiontWardCode IN
		--this is the list of wards left that do not fall into the list specified in the Table of UHSM wards & MRSA Screen Compliance appendix in the MRSA policy
		--listing these wards so that any new ones can be automatically included until we are specifically told to exclude.
		(
			'A7'
			,'ADMLOU'
			,'ALE'
			,'ALEX PCT'
			,'ALEXHDU'
			,'ALEXICU'
			,'ANT'
			,'ASPIRE'
			,'BROOMWOOD'
			,'BS'
			,'BUP'
			,'BWH'
			,'C1'
			,'CEC'
			,'CF15'
			,'CF15'
			,'CHBL'
			,'CLR'
			,'DCAU'
			,'DEN'
			,'F11'
			,'F11A DO NOT USE'
			,'F12'
			,'F14'
			,'F2'
			,'F2M'
			,'F2N'
			,'F5'
			,'GIU'
			,'HAEM'
			,'HCAWILM'
			,'HOM'
			,'ISC'
			,'KDU'
			,'LAVC'
			,'MT'
			,'MUU'
			,'NH-WEST'
			,'NIHR'
			,'OAKLAND'
			,'PIU'
			,'RAD'
			,'RCU'
			,'REGENCY'
			,'SEXTON'
			,'SPIREW'
			,'TAMESIDE HOSPITAL'
			,'TCU'
			,'TCW'
			,'TRAFF'
			,'TRLO'
			,'WEL'
			,'WPU'
			)
	)
	--NOte : in the access database, there is an additional criteria of spells where the specialty is not one of the following : ("211","420","421","171","501","502","503","560","140","130")
	--Do we need to include this when generating the cohort of admissions for the denominator?
	--CTE for cohort of spells - derive the exclusion field to determine those which dont need to be included in the cohort
	,Spells
AS (
	SELECT DISTINCT Spell.FaciltyID
		,Spell.NHSNo
		,Spell.AdmissionSpecialty
		,Spell.AdmissionType
		,Spell.PatientClass
		,Spell.AdmissionDateTime
		,Spell.DischargeDateTime
		,Spell.AdmissiontWardCode
		,Spell.AdmissionWard
		,Spell.AdmissionConsultantName
		,CASE 
			WHEN EndoExcl.SourceSpellNo IS NOT NULL
				THEN 'Endo Primary Proc Exclusion'
			WHEN OtherExcl.SourceSpellNo IS NOT NULL
				THEN 'Other Prim Proc Exclusion'
			WHEN WardExcl.SourceSpellNo IS NOT NULL
				THEN 'Admission Ward Not Included'
			ELSE NULL
			END AS Exclusion
		,cc.PriamryProcedure PrimaryProcedureOfFirstEpisode
		,ISNULL(cc.PriamryProcedureCode, '') + ',' + ISNULL(cc.SecondaryProcedureCode1, '') + ',' + ISNULL(cc.SecondaryProcedureCode2, '') + ',' + ISNULL(cc.SecondaryProcedureCode3, '') + ',' + ISNULL(cc.SecondaryProcedureCode4, '') + ',' + ISNULL(cc.SecondaryProcedureCode5, '') + ',' + ISNULL(cc.SecondaryProcedureCode6, '') + ',' + ISNULL(cc.SecondaryProcedureCode7, '') + ',' + ISNULL(cc.SecondaryProcedureCode8, '') + ',' + ISNULL(cc.SecondaryProcedureCode9, '') + ',' + ISNULL(cc.SecondaryProcedureCode10, '') + ',' + ISNULL(cc.SecondaryProcedureCode11, '') AS AllProceduresOfFirstEpisode
		,Spell.SourceSpellNo
		,Episode.EpisodeUniqueID
	FROM WHREPORTING.APC.Spell
	LEFT OUTER JOIN (
		SELECT *
		FROM WHREPORTING.APC.Episode
		WHERE FirstEpisodeInSpell = 1
		) Episode ON Spell.SourceSpellNo = Episode.SourceSpellNo
	LEFT OUTER JOIN EndoExcl ON Spell.SourceSpellNo = EndoExcl.SourceSpellNo
	LEFT OUTER JOIN OtherExcl ON Spell.SourceSpellNo = OtherExcl.SourceSpellNo
	LEFT OUTER JOIN WardExcl ON Spell.SourceSpellNo = WardExcl.SourceSpellNo
	LEFT OUTER JOIN WHREPORTING.APC.ClinicalCoding cc ON Episode.EpisodeUniqueID = cc.SourceUniqueID
	WHERE Spell.PatientClass IN (
			'DAY CASE'
			,'ELECTIVE INPATIENT' --this logic in the MRSA database included both Day case and Elective Inpatient Patient Class types
			) --Check with Alison if we want to include Admission Type of Maternity as the Delivery suite etc are included in the criteria sheet
		--AO response from meeting is that we should be using Patient Class rather than Admission type to determine the elective admissions.
		AND Spell.AdmissionDate >= '20150401'
	)
	,Result -- identify the Max Report and Abnormal result for each MRSA request as sometimes there is more than one report for each request - ? unsure why?
AS (
	SELECT Max(FactInvestigation.InvestigationIndex) AS InvestigationIndex --should be ok to use Max investigation as we are only pulling out investigations where the name is 'MRSA Screen'
		,DimInvestigationRequested.InvestigationRequested
		,Max(FactInvestigation.ReportIndex) AS ReportIndex --should be ok to use Max Report as we are only pulling out investigations where the name is 'MRSA Screen'
		,FactReport.RequestIndex
		,FactReport.MasterPatientCode
		,Patient.ICEHospitalNumber
		,Patient.NHSNumber
		,sum(cast(ServiceResult.Abnormal_Flag AS INT)) AS AbnormalFlag --Sum so we can flag any reports which relate to a MRSA screen investigation which has been marked as abnormal
	FROM WHREPORTING.ICE.FactInvestigation WITH (NOLOCK)
	LEFT OUTER JOIN WHREPORTING.ICE.FactReport WITH (NOLOCK) ON FactInvestigation.ReportIndex = FactReport.ReportIndex
	LEFT OUTER JOIN WHREPORTING.ICE.DimInvestigationRequested WITH (NOLOCK) ON FactInvestigation.InvestigationDescriptionKey = DimInvestigationRequested.InvestigationKey
	LEFT OUTER JOIN ICE_DB.dbo.ServiceResult WITH (NOLOCK) ON FactInvestigation.InvestigationIndex = ServiceResult.Investigation_Index
	LEFT OUTER JOIN WHREPORTING.ICE.Patient ON FactReport.MasterPatientCode = Patient.PatientKey
	WHERE InvestigationRequested LIKE '%mrsa screen%'
		AND FactReport.ReportDate >= '20110401'
		AND FactReport.Category IS NULL
	GROUP BY DimInvestigationRequested.InvestigationRequested
		,FactReport.RequestIndex
		,FactReport.MasterPatientCode
		,Patient.ICEHospitalNumber
		,Patient.NHSNumber
	)
	,MRSAScreen --combined CTE of the latest MRSA report for each request from above linked back to the ICE.FactReport and ICE.FactRequest table to get the details such as Request Date and Report Date 
AS (
	SELECT Result.*
		,FactReport.ReportDate + + cast(ReportTime AS DATETIME) AS ReportDateTime
		,FactRequest.RequestDate + cast(RequestTime AS DATETIME) AS RequestDateTime
		,RequestDate
		,RequestTime
	FROM Result
	LEFT OUTER JOIN WHReporting.ICE.FactReport ON Result.ReportIndex = FactReport.ReportIndex
	LEFT OUTER JOIN WHReporting.ICE.FactRequest ON Result.RequestIndex = FactRequest.RequestIndex
	WHERE FactReport.Category IS NULL
	)
	
	,-- The brief for the MRSA report is simply to pick up the first MRSA screen associated with the admission
	--The MRSA screen can be done on admission or in a pre-op up to 6 weeks prior to admission
	--CTE below finds the first MRSA screen requested between the Admission Date - 42 days  and the discharge date
	--Then it just picks the first date on which an MRSA screen was requested
SpellMRSA
AS (
	SELECT Spells.FaciltyID
		,Spells.NHSNo
		,Spells.AdmissionSpecialty
		,Spells.AdmissionType
		,Spells.PatientClass
		,Spells.AdmissionDateTime
		,Spells.DischargeDateTime
		,Spells.AdmissiontWardCode
		,Spells.AdmissionWard
		,Spells.AdmissionConsultantName
		,Spells.Exclusion
		,Spells.PrimaryProcedureOfFirstEpisode
		,Spells.AllProceduresOfFirstEpisode
		,Spells.SourceSpellNo
		,Spells.EpisodeUniqueID
		,Max(RequestDateTime) AS FirstRequestDate
	FROM Spells
	LEFT OUTER JOIN MRSAScreen ON Spells.NHSNo = MRSAScreen.NHSNumber
		AND RequestDate >= DateAdd(day, - 42, Spells.AdmissionDateTime)
		AND RequestDate < DateAdd(day, 2, Spells.AdmissionDateTime)--in the MRSA database there was similar logic but the period in there between admission date -90 days and admission date +1 day
	GROUP BY Spells.FaciltyID
		,Spells.NHSNo
		,Spells.AdmissionSpecialty
		,Spells.AdmissionType
		,Spells.PatientClass
		,Spells.AdmissionDateTime
		,Spells.DischargeDateTime
		,Spells.AdmissiontWardCode
		,Spells.AdmissionWard
		,Spells.AdmissionConsultantName
		,Spells.Exclusion
		,Spells.PrimaryProcedureOfFirstEpisode
		,Spells.AllProceduresOfFirstEpisode
		,Spells.SourceSpellNo
		,Spells.EpisodeUniqueID
	)
	--Max report CTE to get the latest report associated with that spell as some patients have more than one MRSA screens requested on the same day - both of which can be reported on.
	--As long as there is a report for one of them then this should indicate the creening has been carried out so it should be fine to take the Max(reportIndex) provided the request date is linked to the MinREquestDate for that spell.
	,MaxReport
AS (
	SELECT SpellMRSA.SourceSpellNo
		,Max(MRSAScreen.ReportIndex) AS MaxReportIndex --should be ok to use the Max report index as some patients have more than one MRSA screens requested on the same day. As long as there is a report for one of them then this should indecate the creening has been carried out?
	FROM SpellMRSA
	LEFT OUTER JOIN MRSAScreen ON SpellMRSA.NHSNo = MRSAScreen.NHSNumber
		AND SpellMRSA.FirstRequestDate = MRSAScreen.RequestDateTime
	GROUP BY SpellMRSA.SourceSpellNO
	)

	

--final query to get the details of the spells joined to the details of the max report index

Insert into  WHReporting.APC.ElectiveAdmissionMRSAScreens
(		FaciltyID
      ,NHSNo
      ,AdmissionSpecialty
      ,AdmissionType
      ,PatientClass
      ,AdmissionDateTime
      ,DischargeDateTime
      ,AdmissiontWardCode
      ,AdmissionWard
      ,AdmissionConsultantName
      ,Exclusion
      ,PrimaryProcedureOfFirstEpisode
      ,AllProceduresOfFirstEpisode
      ,SourceSpellNo
      ,EpisodeUniqueID
      ,FirstRequestDate
      ,InvestigationRequested
      ,RequestDate
      ,ReportDateTime
      ,AbnormalFlag
      ,ReportIndex	
      ,BuildDate
)
SELECT SpellMRSA.FaciltyID
	,SpellMRSA.NHSNo
	,SpellMRSA.AdmissionSpecialty
	,SpellMRSA.AdmissionType
	,SpellMRSA.PatientClass
	,SpellMRSA.AdmissionDateTime
	,SpellMRSA.DischargeDateTime
	,SpellMRSA.AdmissiontWardCode
	,SpellMRSA.AdmissionWard
	,SpellMRSA.AdmissionConsultantName
	,SpellMRSA.Exclusion
	,SpellMRSA.PrimaryProcedureOfFirstEpisode
	,SpellMRSA.AllProceduresOfFirstEpisode
	,SpellMRSA.SourceSpellNo
	,SpellMRSA.EpisodeUniqueID
	,SpellMRSA.FirstRequestDate
	,MaxRepDetails.InvestigationRequested
	,MaxRepDetails.RequestDate
	,MaxRepDetails.ReportDateTime
	,MaxRepDetails.AbnormalFlag
	,MaxRepDetails.ReportIndex
	,GETDATE()
FROM SpellMRSA
LEFT OUTER JOIN MaxReport ON SpellMRSA.SourceSpellNo = MaxReport.SourceSpellNo
LEFT OUTER JOIN MRSAScreen MaxRepDetails ON MaxReport.MaxReportIndex = MaxRepDetails.ReportIndex


;



Update APC.ElectiveAdmissionMRSAScreens
Set AdmissionFlag = 1,
MRSAScreenExclFlag = CASE WHEN Exclusion IS NOT NULL THEN 1	ELSE 0	END,
MRSAScreenRequiredFlag =  CASE WHEN Exclusion IS  NULL THEN 1	ELSE 0	END,
MRSAScreenCompletedFlag = CASE 	WHEN Exclusion IS NULL AND ReportDateTime IS NOT NULL THEN 1 ELSE 0	END--only flagging the ones that are required for MRSA screening and completed.  If MRSA screening not required as indicated by exclusion rules, but the MRSA screening has been carried out anyway, these are not flagged as completed


; 


--SELECT SpellMRSA.FaciltyID
--	,SpellMRSA.NHSNo
--	,SpellMRSA.AdmissionSpecialty
--	,SpellMRSA.AdmissionType
--	,SpellMRSA.PatientClass
--	,SpellMRSA.AdmissionDateTime
--	,SpellMRSA.DischargeDateTime
--	,SpellMRSA.AdmissiontWardCode
--	,SpellMRSA.AdmissionWard
--	,SpellMRSA.AdmissionConsultantName
--	,SpellMRSA.Exclusion
--	,SpellMRSA.PrimaryProcedureOfFirstEpisode
--	,SpellMRSA.AllProceduresOfFirstEpisode
--	,SpellMRSA.SourceSpellNo
--	,SpellMRSA.EpisodeUniqueID
--	,SpellMRSA.FirstRequestDate
--	,MaxRepDetails.InvestigationRequested
--	,MaxRepDetails.RequestDate
--	,MaxRepDetails.ReportDate
--	,MaxRepDetails.AbnormalFlag
--	,MaxRepDetails.ReportIndex
--FROM SpellMRSA
--LEFT OUTER JOIN (
--	SELECT SpellMRSA.SourceSpellNo
--		,Max(MRSAScreen.ReportIndex) AS MaxReportIndex --should be ok to use the Max report index as some patients have more than one MRSA screens requested on the same day. As long as there is a report for one of them then this should indecate the creening has been carried out?
--	FROM SpellMRSA
--	LEFT OUTER JOIN MRSAScreen ON SpellMRSA.NHSNo = MRSAScreen.NHSNumber
--		AND SpellMRSA.FirstRequestDate = MRSAScreen.RequestDateTime
--	GROUP BY SpellMRSA.SourceSpellNO --, MRSAScreen.RequestDateTime
--	) MaxReport --sub query here to get the latest report for that request as some requests have more than one report 
--	ON SpellMRSA.SourceSpellNo = MaxReport.SourceSpellNo
--LEFT OUTER JOIN MRSAScreen MaxRepDetails ON MaxReport.MaxReportIndex = MaxRepDetails.ReportIndex
 



/*
--this is the list of wards we care currently specifying to include spells from 
SElect WardCode , ServicePoint.ServicePoint, Max(StartTime) from WHREPORTING.APC.WardStay
Left outer join WH.PAS.ServicePoint
 on WardStay.WardCode = ServicePoint.ServicePointLocalCode

where WardCode  not in  
('BIC',
'CTCU',
'HDU',
'ICA',
'ICU',
'REC',
'CT TRANSPLANT',--do we need to include this
'TDC',
'F1',
'F6',
'JIM',
'CCA',--is this ACCU on sheet from AO
'F16',
'C3',
'DS',
'C2',
'CC2',
'F15 OBS',
'F15S',--Is it both this and the F15Obs above?
'BC',
'SCB',
'NIGHTINGALE',
'WHS',
'F8M',
'POAU',
'A3',
'A4',
'A5',
'A6',
'SAU',
'F1', --F1S
'F3',--Is it both F3 and F3PLAS?
'F3PLAS',--Is it both F3 and F3PLAS?
'PIUF2', --IS this the correct PIU
'UAUWYT',--Is this Urolgy as per the list of Wards or do we need to include below
'ATR',--WCH UROLOGY TRUSS UNIT
'AUR',--WCH UROL VIDEO D/C
'AUS',--WCH UROL SURG UNIT
'UAU', --WCH UAU UROL ASSESS  UNIT
'LIT', 
'A2',
'BUR',
'F9',
'UCC',--Urgent Care Centre -Is this what they mean by A&E
'A10',
'ACU',--AMRU
'EAU',-- Clinical Decision Unit
'A8',
'AMU',-- This is not in the list but didn't A8 and A10 become AMU last year 
'A1',
'A9',
'F4N',
'F4S',--Is this to be included as well as F4N as the F4 listed in the list of Wards
'F7',
'F10',
'DL', -- Is this the Dis ward as per the liust of wards?
'DU',--Sleep Services
'PEA',-- Pearce
'BRO',--BRONCHOSCOPY UNIT
'DYL',
'WIL',
'POU',
'PITU')
Group by WardCode,ServicePoint.ServicePoint
order by 1
*/