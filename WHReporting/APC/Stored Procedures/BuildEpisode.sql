﻿CREATE Procedure [APC].[BuildEpisode] As

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()




If Exists (Select * from INFORMATION_SCHEMA.TABLES Where Table_Schema = 'APC' and TABLE_NAME = 'Episode')
DROP TABLE APC.Episode;



CREATE TABLE APC.Episode 
(
	 EncounterRecno int not null --Primary Key Clustered
	,SourcePatientNo int not null
	,SourceSpellNo int not null
	,EpisodeUniqueID int not null
	,ReferralSourceUniqueID int null
	,WaitingListSourceUniqueID int null
	,FacilityID varchar(20) null
	,NHSNo varchar(20) null
	,AdmissionDate Date null
	,AdmissionTime Time(0) null
	,AdmissionDateTime SmallDatetime null
	,DischargeDate Date Null
	,DischargeTime Time(0) null
	,DischargeDateTime SmallDatetime
	,EpisodeStartDate Date null
	,EpisodeStartTime Time(0) null
	,EpisodeStartDateTime SmallDatetime null
	,EpisodeEndDate Date null
	,EpisodeEndTime Time(0) null
	,EpisodeEndDateTime SmallDatetime null
	,StartWardCode varchar(20)
	,StartWard varchar(255) null
	,EndWardCode varchar(20)
	,EndWard varchar(255) null
	,ConsultantCode varchar(20) null
	,ConsultantName varchar(80) null
	,Division varchar(80) null
	,Directorate varchar(80) null
	,[SpecialtyCode] varchar(20) null
	,[Specialty] varchar(255) null
	,[SpecialtyCode(Function)] varchar(20) null
	,[Specialty(Function)] varchar(255) null
	,[SpecialtyCode(Main)] varchar(20) null
	,[Specialty(Main)] varchar(255) null
	,LengthOfEpisode int null
	,PrimaryDiagnosisCode varchar(20) null
	--,PrimaryDiagnosis varchar(255) null
	,PrimaryProcedureCode varchar(20) null
	--,PriamryProcedure varchar(255) null
	,PrimaryProcedureDateTime smalldatetime null
	,EpisodeGPCode varchar(20) null
	,EpisodeGP varchar(500) null
	,EpisodicPracticeCode varchar(20) null
	,EpisodePractice varchar(500) null
	,EpisodePCTCode varchar(10) null
	,EpisodePCT varchar(255) null
	,EpisodeCCGCode varchar(10) null
	,EpisodeCCG varchar(255) null
	,EpisodePCTCCGCode varchar(10) null
	,EpisodePCTCCG varchar(255) null
	,PostCode varchar(20)
	,ResidencePCTCode varchar(10)
	,ResidencePCT varchar(255)
	--,ResidenceCCGCode varchar(10)
	--,ResidenceCCG varchar(255)
	,ResidenceLocalAuthorityCode varchar(10)
	,ResidenceLocalAuthority varchar(255)
	,PurchaserCode varchar(10) null
	,Purchaser varchar(255) null
	,ResponsibleCommissionerCode varchar(10) null
	,ResponsibleCommissioner varchar(255)
	,ResponsibleCommissionerCCGOnlyCode varchar(10) null
	,ResponsibleCommissionerCCGOnly varchar(255)
	,TheatrePatientBookingKey int null
	,TheatreCode varchar(20) null
	,Theatre varchar(255) null
	,AgeCode varchar(50) null
	,AdmissionType varchar(20)
	,NeoNatalLevelOfCare int null
	,FirstEpisodeInSpell int null
	,LastEpisodeInSpell int null
	,EpisodeOutcome varchar(50) null
	,OurActivityFlag varchar(1)
	,NotOurActProvCode varchar(10)

	);
	


	
--select top 200 * from WHOLAP.dbo.OlapAPC

Insert into APC.Episode
	(
	 [EncounterRecno]
	,[SourcePatientNo]
	,[SourceSpellNo]
	,[EpisodeUniqueID]
	,[ReferralSourceUniqueID]
	,[WaitingListSourceUniqueID]
	,[FacilityID]
	,[NHSNo]
	,[AdmissionDate]
	,[AdmissionTime]
	,[AdmissionDateTime]
	,[DischargeDate]
	,[DischargeTime]
	,[DischargeDateTime]
	,[EpisodeStartDate]
	,[EpisodeStartTime]
	,[EpisodeStartDateTime]
	,[EpisodeEndDate]
	,[EpisodeEndTime]
	,[EpisodeEndDateTime]
	,[StartWardCode]
	,[StartWard]
	,[EndWardCode]
	,[EndWard]
	,[ConsultantCode]
	,[ConsultantName]
	,[Division]
	,[Directorate]
	,[SpecialtyCode]
	,[Specialty]
	,[SpecialtyCode(Function)]
	,[Specialty(Function)]
	,[SpecialtyCode(Main)]
	,[Specialty(Main)]
	,[LengthOfEpisode]
	,[PrimaryDiagnosisCode]
	--,[PrimaryDiagnosis]
	,[PrimaryProcedureCode]
	--,[PriamryProcedure]
	,[PrimaryProcedureDateTime]
	,[EpisodeGPCode]
	,[EpisodeGP]
	,[EpisodicPracticeCode]
	,[EpisodePractice]
	,[EpisodePCTCode]
	,[EpisodePCT]
	,[EpisodeCCGCode]
	,[EpisodeCCG]
	,[EpisodePCTCCGCode]
	,[EpisodePCTCCG]
	,[PostCode]
	,[ResidencePCTCode]
	,[ResidencePCT]
	--,[ResidenceCCGCode]
	--,[ResidenceCCG]
	,[ResidenceLocalAuthorityCode]
	,[ResidenceLocalAuthority]
	,[PurchaserCode]
	,[Purchaser]
	,[ResponsibleCommissionerCode]
	,[ResponsibleCommissioner]
	,[ResponsibleCommissionerCCGOnlyCode]
	,[ResponsibleCommissionerCCGOnly]
	,[TheatrePatientBookingKey]
	,[TheatreCode]
	,[Theatre]
	,[AgeCode]
	,[AdmissionType]
	,[NeoNatalLevelOfCare]
	,[FirstEpisodeInSpell]
	,[LastEpisodeInSpell]
	,[EpisodeOutcome]
	,OurActivityFlag 
	,NotOurActProvCode
	)
	(
	Select 
	 Encounter.EncounterRecno
	,Encounter.SourcePatientNo
	,Encounter.SourceSpellNo
	,Encounter.SourceUniqueID
	,Encounter.ReferralSourceUniqueID
	,Encounter.WaitingListSourceUniqueID
	,Encounter.DistrictNo
	,Encounter.NHSNumber
	,AdmissionDate = CAST(Encounter.AdmissionDate as Date)
	,AdmissionTime = CAST(Encounter.AdmissionTime as Time(0))
	,AdmissionDateTime = CAST(Encounter.AdmissionTime as Datetime)
	,DischargeDate = CAST(Encounter.DischargeDate as Date)
	,DischargeTime = CAST(Encounter.DischargeTime as Time(0))
	,DischargeDateTime = CAST(Encounter.DischargeTime as Datetime)
	,EpisodeStartDate = CAST(Encounter.EpisodeStartDate as Date)
	,EpisodeStartTime = CAST(Encounter.EpisodeStartTime as Time(0))
	,EpisodrStartDateTime = CAST(Encounter.EpisodeStartTime as Datetime)
	,EpisodeEndDate = CAST(Encounter.EpisodeEndDate as Date)
	,EpisodeEndTime = CAST(Encounter.EpisodeEndTime as Time(0))
	,EpisodeEndDateTime = CAST(Encounter.EpisodeEndTime as Datetime)
	,SWard.CODE
	,SWard.NAME
	,EWard.CODE
	,EWard.NAME
	,Cons.NationalConsultantCode
	,Cons.Consultant
	,SpecBase.Division
	,SpecBase.Direcorate
	,SpecBase.SubSpecialtyCode
	,SpecBase.SubSpecialty
	,SpecBase.SpecialtyFunctionCode
	,SpecBase.SpecialtyFunction
	,SpecBase.MainSpecialtyCode
	,SpecBase.MainSpecialty
	,Encounter.LOE
	--KO updated 01/03/2013 as per issues log
	--,Encounter.PrimaryDiagnosisCode
	,case when Encounter.PrimaryDiagnosisCode = '##' then null 
		else Encounter.PrimaryDiagnosisCode
		end
	--,PDiag.Coding
	--,Encounter.PrimaryOperationCode
	,case when Encounter.PrimaryOperationCode = '##' then null 
		else Encounter.PrimaryOperationCode
		end
	--,PProc.Coding
	,Encounter.PrimaryOperationDate
	,GP.PROCA_REFNO_MAIN_IDENT
	,GPName = GP.SURNAME + ' ' + GP.FORENAME
	,EpisodicPracticeCode = 
					Case When (charindex('-',Practice.Practice) <1) Then
						cast(Practice.PracticeCode AS varchar)
					Else
						LTRIM(RTRIM(LEFT(Practice.Practice,CHARINDEX('-',Practice.Practice) -2)))
					End
	,EpisodicPractice = 
					Case When (charindex('-',Practice.Practice) <1) Then
						cast(Practice.Practice AS varchar)
					Else
						LTRIM(RTRIM(RIGHT(Practice.Practice,CHARINDEX('-',REVERSE(Practice.Practice)) -2)))
					End
	,Practice.PCTCode
	,Practice.PCT
	,Practice.CCGCode
	,Practice.CCGName
	,EpisodePCTCCGCode = 
		case 
			when Encounter.AdmissionDate > '2013-03-31 00:00:00'
			then Practice.CCGCode
			else Practice.PCTCode
		end
	,EpisodePCTCCG = 
		case 
			when Encounter.AdmissionDate > '2013-03-31 00:00:00'
			then Practice.CCGName
			else Practice.PCTName
		end
	,Encounter.Postcode
	,PostC.PCTCode
	,PCT.Organisation
	--,PostCCCG.CCGCode
	--,CCG.Organisation
	,PostC.LocalAuthorityCode
	,LA.[Local Authority Name]
	,Purchaser.MAIN_IDENT
	,Purchaser.DESCRIPTION
	,Encounter.ResponsibleCommissioner
	,ResponsibleCommissioner.PCTCCG
	,Encounter.ResponsibleCommissionerCCGOnly
	,ResponsibleCommissionerCCGOnly.PCTCCG
	,Encounter.TheatrePatientBookingKey
	,Theatre.TheatreCode1
	,Theatre.Theatre
	,Encounter.AgeCode
	,AdmissionType = case Encounter.AdmissionMethodTypeCode
		When 'EL' Then 'Elective'
		When 'EM' Then 'Emergency'
		When 'MAT' Then 'Maternity'
		When 'NE' Then 'Non-Elective'
		Else 'Other'
		End
	,Encounter.NeonatalLevelOfCare
	,FirstEpisodeInSpell = 
			Case When Encounter.EpisodeStartTime = Encounter.AdmissionTime Then 
				1
			Else
				0
			End
	,LastEpisodeInSpellIndicator = Encounter.LastEpisodeInSpellIndicator
	,EpisodeOutcome = ceocm.[DESCRIPTION]
	,OurActivityFlag =Encounter.OurActivityFlag
	,NotOurActProvCode = Encounter.NotOurActProvCode

	FROM WHOLAP.dbo.OlapAPC Encounter
	left join WH.PAS.ServicePointBase SWard
		on Encounter.StartWardTypeCode = SWard.SPONT_REFNO
		
	left join WH.PAS.ServicePointBase EWard
		on Encounter.EndWardTypeCode = EWard.SPONT_REFNO
		
	left join WHOLAP.dbo.OlapPASConsultant Cons
		on Encounter.ConsultantCode = Cons.ConsultantCode
		
	left join WHOLAP.dbo.SpecialtyDivisionBase SpecBase
		on Encounter.SpecialtyCode = SpecBase.SpecialtyCode
		
	left join WH.PAS.ProfessionalCarerBase GP 
		on Encounter.EpisodicGpCode = GP.PROCA_REFNO	
		
	--left join WHOLAP.dbo.OlapPASPractice Pract
	--	on Encounter.RegisteredGpPracticeCode = Pract.PracticeCode
		
	left join WHOLAP.dbo.OlapPASPracticeCCG Practice
		--on Encounter.RegisteredGpPracticeCode = Pract.PracticeCode
		on case 
			when Encounter.EpisodicGpPracticeCode = -1
			then Encounter.RegisteredGpPracticeCode
			else Encounter.EpisodicGpPracticeCode
		end = Practice.PracticeCode
		
	left join Lorenzo.dbo.Purchaser Purchaser
		on Encounter.PurchaserCode = Purchaser.PURCH_REFNO	
		
	left join WHOLAP.dbo.OlapTheatre Theatre
		on Encounter.TheatreCode = Theatre.TheatreCode
		
	left join Organisation.dbo.Postcode PostC
		on REPLACE(Encounter.PostCode,' ','') = PostC.SlimPostcode
		
	left join Organisation.dbo.[Local Authority] LA
		on PostC.LocalAuthorityCode = LA.[Local Authority Code]
		
	left join Organisation.dbo.PCT PCT
		on PostC.PCTCode = PCT.OrganisationCode
		
	left join LK.PasPCTOdsCCG ResponsibleCommissioner
		on ResponsibleCommissioner.PCTCCGCode = Encounter.ResponsibleCommissioner
		
	left join LK.PasPCTOdsCCG ResponsibleCommissionerCCGOnly
		on ResponsibleCommissionerCCGOnly.PCTCCGCode = Encounter.ResponsibleCommissionerCCGOnly
		
	left join WH.APC.Encounter WHEncounter
		on WHEncounter.SourceUniqueID = Encounter.SourceUniqueID
		
	left join WH.PAS.ReferenceValueBase ceocm
		on WHEncounter.EpisodeOutcomeCode = ceocm.RFVAL_REFNO
);




Create Unique Clustered Index ixcEpisode on APC.Episode(EncounterRecno);
Create Index ixEpisodeUNiqueID on APC.Episode(EpisodeUniqueID);
Create Index ixEpisodeStartDate on APC.Episode(EpisodeStartDate);
Create Index ixEpisodeEndDate on APC.Episode(EpisodeEndDate);
Create Index ixAdmitDate on APC.Episode(AdmissionDate);
Create Index ixDischDate on APC.Episode(DischargeDate);
Create Index ixDivision on APC.Episode(Division);
Create Index ixDirectorate on APC.Episode(Directorate);
Create Index ixSpecActual on APC.Episode([SpecialtyCode]);
Create Index ixSpecFunc on APC.Episode([SpecialtyCode(Function)]);
Create Index ixSpecMain on APC.Episode([SpecialtyCode(Main)]);
Create Index ixPCT on APC.Episode(EpisodePCTCode);
Create Index ixResPCT on APC.Episode(ResidencePCTCode);
--Create Index ixResCCG on APC.Episode(ResidenceCCGCode);
Create Index ixFacilityID on APC.Episode(FacilityID);
Create Index ixNHSNo on APC.Episode(NHSNo);

--CM 13/10/2015 Added indexes to improve performance of view WHReporting.APC.Readmissions
CREATE NONCLUSTERED INDEX [ixLastEpisode] ON [APC].[Episode] ([LastEpisodeInSpell]) INCLUDE ([SourceSpellNo],[Division],[Directorate],[SpecialtyCode(Function)],[Specialty(Function)])
CREATE NONCLUSTERED INDEX [ixSpellNo] ON [APC].[Episode] ([SourceSpellNo])


--update APC.Episode set
--	ResidenceCCGCode = PostCCCG.CCGCode
--	,ResidenceCCG = CCG.Organisation
--from APC.Episode Episode
--left join OrganisationCCG.dbo.Postcode PostCCCG
--	on REPLACE(Episode.PostCode,' ','') = PostCCCG.SlimPostcode
--left join OrganisationCCG.dbo.CCG CCG
--	on PostCCCG.CCGCode = CCG.OrganisationCode

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WHREPORTING APC.BuildEpisode', @Stats, @StartTime