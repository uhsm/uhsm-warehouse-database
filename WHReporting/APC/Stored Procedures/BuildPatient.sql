﻿--exec APC.BuildReferralToTreatment
--Select top 100 * from WHOLAP.dbo.OlapAPC


CREATE Procedure [APC].[BuildPatient] As

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @RowsInserted Int

select @StartTime = getdate()

If Exists (Select * from INFORMATION_SCHEMA.TABLES Where Table_Schema = 'APC' and TABLE_NAME = 'Patient')
DROP TABLE APC.Patient;


CREATE TABLE APC.Patient
	(
	 EncounterRecno int not null
	,SourcePatientNo int
    ,EpisodeUniqueID int
	,FacilityID varchar(20)
	,NHSNo varchar(20)
	,NHSNoStatusCode varchar(20)
	,NHSNoStatus varchar(80)
	,NHSNoStatusNHSCode varchar(20)
	,PatientTitle varchar(20)
	,PatientForename varchar(30)
	,PatientSurname varchar(30)
	,DateOfBirth smalldatetime
	,DateOfDeath smallDatetime
	,PatientAddress1 varchar(50)
	,PatientAddress2 varchar(50)
	,PatientAddress3 varchar(50)
	,PatientAddress4 varchar(50)
	,PostCode varchar(25)
	,SexCode varchar(20) 
	,Sex varchar(80)
	,SexNHSCode varchar(5)
	,EthnicGroupCode varchar(20)
	,EthnicGroup varchar(80)
	,EthnicGroupNHSCode varchar(5)
	,ReligionCode varchar(20)
	,Religion varchar(80)
	,ReligionNHSCode varchar(5)
	,MRSA char(1)
	,MRSAStartDate smalldatetime
	,MRSAEndDate smalldatetime
	);

Create Unique Clustered Index ixcPatient on APC.Patient(EncounterRecno);



Insert into APC.Patient
	(
	[EncounterRecno]
	,[SourcePatientNo]
	,[EpisodeUniqueID]
	,[FacilityID]
	,[NHSNo]
	,[NHSNoStatusCode]
	,[NHSNoStatus]
	,[NHSNoStatusNHSCode]
	,[PatientTitle]
	,[PatientForename]
	,[PatientSurname]
	,[DateOfBirth]
	,[DateOfDeath]
	,[PatientAddress1]
	,[PatientAddress2]
	,[PatientAddress3]
	,[PatientAddress4]
	,[PostCode]
	,[SexCode]
	,[Sex]
	,[SexNHSCode]
	,[EthnicGroupCode]
	,[EthnicGroup]
	,[EthnicGroupNHSCode]
	,[ReligionCode]
	,[Religion]
	,[ReligionNHSCode]
	)
 
	(
	Select 
		 Encounter.EncounterRecno
		,Encounter.SourcePatientNo
		,Encounter.SourceUniqueID
		,Encounter.DistrictNo
		,Encounter.NHSNumber
		,NHSN.NHSNoStatusLocalCode
		,NHSN.NHSNoStatusDescription
		,NHSN.NHSNoStatusNationalCode
		,Encounter.PatientTitle
		,Encounter.PatientForename
		,Encounter.PatientSurname
		,Encounter.DateOfBirth
		,Encounter.DateOfDeath
		,Encounter.PatientAddress1
		,Encounter.PatientAddress2
		,Encounter.PatientAddress3
		,Encounter.PatientAddress4
		,Encounter.Postcode
		,Sex.SexLocalCode
		,Sex.SexDescription
		,Sex.SexNationalCode
		,Eth.EthnicOriginLocalCode
		,Eth.EthnicOriginDescription
		,Eth.EthnicOriginNationalCode
		--,Relgn.ReligionLocalCode
		--,Relgn.ReligionDescription
		--,Relgn.ReligionNationalCode
		,Relgn.MainCode
		,Relgn.ReferenceValue
		,Relgn.MappedCode
	
	FROM WHOLAP.dbo.OlapAPC Encounter
	left outer join dbo.vwPASNHSNoStatus NHSN
		on Encounter.NHSNumberStatusCode = NHSN.NHSNoStatusLocalCode
	left outer join dbo.vwPASEthnicOrigin Eth
		on Encounter.EthnicOriginCode = Eth.EthnicOriginCode
		
	--KO amended 04/03/2014 - reference views in WHREPORTING have missing data
	--left outer join dbo.vwPASReligion Relgn
	--	on Encounter.ReligionCode = Relgn.ReligionCode
	left join WH.PAS.ReferenceValue Relgn
		on Encounter.ReligionCode = Relgn.ReferenceValueCode
		
	left outer join vwPASSex Sex
		on Encounter.SexCode = Sex.SexCode
	);

select @RowsInserted = @@rowcount
--KO added 05/11/2013 Add MRSA status
--select 
--	diag.PATNT_REFNO
update APC.Patient set
	MRSA = 'Y'
	,MRSAStartDate  = cast(diag.START_DTTM as smalldatetime)
	,MRSAEndDate  = cast(diag.END_DTTM as smalldatetime)
from APC.Patient pat
inner join Lorenzo.dbo.ClinicalCoding diag
on diag.PATNT_REFNO = pat.SourcePatientNo
and diag.ARCHV_FLAG = 'N'
where (diag.CODE = '009' OR diag.CODE = 'INFC006')


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) +
	'. Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WHREPORTING APC.BuildPatient', @Stats, @StartTime