﻿--exec APC.BuildClinicalCoding
--Select top 100 * from WHOLAP.dbo.OlapAPC


CREATE Procedure [APC].[BuildReferralToTreatment] As

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

If Exists (Select * from INFORMATION_SCHEMA.TABLES Where Table_Schema = 'APC' and TABLE_NAME = 'ReferralToTreatment')
DROP TABLE APC.ReferralToTreatment;




CREATE TABLE APC.ReferralToTreatment
	(
	 EncounterRecno int not null
	,RTTPathwayID varchar(200)
	,RTTStartDate smalldatetime
	,RTTEndDate smallDatetime
	,RTTSpecialtyCode varchar(5)
	,RTTCurrentStatusCode varchar(20)
	,RTTCurrentStatus varchar(80)
	,RTTCurrentStatusDate Smalldatetime
	,RTTPeriodStatusCode varchar(20)
	,RTTPeriodStatus varchar(80)
	,OurActivityFlag varchar(1)
	, NotOurActProvCode varchar(10)
	);

Create Unique Clustered Index ixcReferralToTreatmemt on APC.ReferralToTreatment(EncounterRecno);



Insert into APC.ReferralToTreatment
	(
	 EncounterRecno
	,RTTPathwayID
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentStatusCode
	,RTTCurrentStatus
	,RTTCurrentStatusDate
	,RTTPeriodStatusCode
	,RTTPeriodStatus
	,OurActivityFlag
	,NotOurActProvCode
	)
 
	(
	Select 
		 Encounter.EncounterRecno
		,Encounter.RTTPathwayID
		,Encounter.RTTStartDate
		,Encounter.RTTEndDate
		,Spec.RTTSpecialtyCode
		,CurrRTT.RTTStatusLocalCode
		,CurrRTT.RTTStatusDescription
		,Encounter.RTTCurrentStatusDate
		,PerRTT.RTTStatusLocalCode
		,PerRTT.RTTStatusDescription
		,Encounter.OurActivityFlag
		,Encounter.NotOurActProvCode
	FROM WHOLAP.dbo.OlapAPC Encounter
	left outer join dbo.vwPASRTTStatus CurrRTT
		on Encounter.RTTCurrentStatusCode = CurrRTT.RTTStatusCode
	left outer join dbo.vwPASRTTStatus PerRTT
		on Encounter.RTTPeriodStatusCode = PerRTT.RTTStatusCode
	left outer join WHOLAP.dbo.SpecialtyDivisionBase Spec
		on Encounter.SpecialtyCode = Spec.SpecialtyCode	
	);




	select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

	select @Stats = 
		'Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

	exec WriteAuditLogEvent 'PAS - WHREPORTING BuildReferralToTreatment', @Stats, @StartTime