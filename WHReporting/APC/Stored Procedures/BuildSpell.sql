﻿CREATE Procedure [APC].[BuildSpell] As

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @RowsInserted Int

select @StartTime = getdate()

--refresh Smartboard data
exec UHSMApplications.SB.ExtractPatientSpellData 'vuhsmdw01'

If Exists (Select * from INFORMATION_SCHEMA.TABLES Where Table_Schema = 'APC' and TABLE_NAME = 'Spell')
DROP TABLE APC.Spell;
/*
CREATE TABLE APC.Spell 
(
	 EncounterRecno int not null
	,SourcePatientNo int not null
	,SourceSpellNo int not null
	,ReferralSourceUniqueID int null
	,WaitingListSourceUniqueID int null
	,AdmissionOfferSourceUniqueID int null
	,FaciltyID varchar(20) null
	,NHSNo varchar(20) null
	,AdmissionDate Date null
	,AdmissionTime Time(0) null
	,AdmissionDateTime SmallDatetime null
	,DischargeDate Date Null
	,DischargeTime Time(0) null
	,DischargeDateTime SmallDatetime
	,AdmissionConsultantCode varchar(20) null
	,AdmissionConsultantName varchar(80) null
	,AdmissionDivision varchar(80) null
	,AdmissionDirectorate varchar(80) null
	,[AdmissionSpecialtyCode] varchar(20) null
	,[AdmissionSpecialty] varchar(255) null
	,[AdmissionSpecialtyCode(Function)] varchar(20) null
	,[AdmissionSpecialty(Function)] varchar(255) null
	,[ADmissionSpecialtyCode(Main)] varchar(20) null
	,[AdmissionSpecialty(Main)] varchar(255) null
	,[AdmissionSpecialtyCode(RTT)] varchar(20) null
	,AdmissiontWardCode varchar(20) null
	,AdmissionWard varchar(255) null
	,AdmissionMethodCode varchar(25)
	,AdmissionMethod varchar(255)
	,AdmissionMethodNHSCode varchar(50)
	,AdmissionSourceCode varchar(25)
	,AdmissionSource varchar(255)
	,AdmissionSourceNHSCode varchar(50)
	,IntendedManagementCode varchar(20)
	,IntendedManagement varchar(80)
	,IntendedManagementNHSCode varchar(50)
	,FirstRegularDayOrNightAdmission char(1)
	,InpatientStayCode varchar(2)
	,DischargeWardCode varchar(20) null
	,DischargeWard varchar(255)
	,DischargeMethodCode varchar(20)
	,DischargeMethod varchar(80)
	,DischargeMethodNHSCode varchar(50)
	,DischargeDestinationCode varchar(20)
	,DischargeDestination varchar(80)
	,DischargeDestinationNHSCode varchar(50)
	,LengthOfSpell int null
	,GPCode varchar(20) null
	,GP varchar(255) null
	,PracticeCode varchar(500) null
	,Practice varchar(500) null
	,PCTCode varchar(10) null
	,PCT varchar(500) null
	,PurchaserCode varchar(20) null
	,Purchaser varchar(255) null
	,TheatrePatientBookingKey int null
	,TheatreCode varchar(20) null
	,Theatre varchar(255) null
	,AgeCode varchar(50) null
	,AdmissionType varchar(20)
	,AdmissionCreated datetime
	,AdmissionCreatedByWhom varchar(80)
	,AdmissionUpdated datetime
	,AdmissionUpdatedByWhom varchar(80)
	);



	

Insert into APC.Spell
	(
	 [EncounterRecno]
	,[SourcePatientNo]
	,[SourceSpellNo]
	,[ReferralSourceUniqueID]
	,[WaitingListSourceUniqueID]
	,[AdmissionOfferSourceUniqueID]
	,[FaciltyID]
	,[NHSNo]
	,[AdmissionDate]
	,[AdmissionTime]
	,[AdmissionDateTime]
	,[DischargeDate]
	,[DischargeTime]
	,[DischargeDateTime]
	,[AdmissionConsultantCode]
	,[AdmissionConsultantName]
	,[AdmissionDivision]
	,[AdmissionDirectorate]
	,[AdmissionSpecialtyCode]
	,[AdmissionSpecialty]
	,[AdmissionSpecialtyCode(Function)]
	,[AdmissionSpecialty(Function)]
	,[ADmissionSpecialtyCode(Main)]
	,[AdmissionSpecialty(Main)]
	,[AdmissionSpecialtyCode(RTT)]
	,[AdmissiontWardCode]
	,[AdmissionWard]
	,[AdmissionMethodCode]
	,[AdmissionMethod]
	,[AdmissionMethodNHSCode]
	,[AdmissionSourceCode]
	,[AdmissionSource]
	,[AdmissionSourceNHSCode]
	,[IntendedManagementCode]
	,[IntendedManagement]
	,[IntendedManagementNHSCode]
	,[FirstRegularDayOrNightAdmission]
	,[InpatientStayCode]
	,[DischargeWardCode]
	,[DischargeWard]
	,[DischargeMethodCode]
	,[DischargeMethod]
	,[DischargeMethodNHSCode]
	,[DischargeDestinationCode]
	,[DischargeDestination]
	,[DischargeDestinationNHSCode]
	,[LengthOfSpell]
	,[GPCode]
	,[GP]
	,[PracticeCode]
	,[Practice]
	,[PCTCode]
	,[PCT]
	,[PurchaserCode]
	,[Purchaser]
	,[TheatrePatientBookingKey]
	,[TheatreCode]
	,[Theatre]
	,[AgeCode]
	,[AdmissionType]
	,[AdmissionCreated]
	,[AdmissionCreatedByWhom]
	,[AdmissionUpdated]
	,[AdmissionUpdatedByWhom]
	)
	(
*/

	Select
	distinct 
	 [EncounterRecno] = Encounter.EncounterRecno
	,[SourcePatientNo] = Encounter.SourcePatientNo
	,[SourceSpellNo] = Encounter.SourceSpellNo
	,[ReferralSourceUniqueID] = Encounter.ReferralSourceUniqueID
	,[WaitingListSourceUniqueID] = Encounter.WaitingListSourceUniqueID
	,[AdmissionOfferSourceUniqueID] = PSpell.ADMOF_REFNO
	,[FaciltyID] = Encounter.DistrictNo
	,[NHSNo] = Encounter.NHSNumber
	,[AdmissionDate] = CAST(Encounter.AdmissionDate as Date)
	,[AdmissionTime] = CAST(Encounter.AdmissionTime as Time(0))
	,[AdmissionDateTime] = CAST(Encounter.AdmissionTime as Datetime)
	--KO updated 06/11/2013 to handle provisional end flag
	,[DischargeDate] = --CAST(Encounter.DischargeDate as Date)
		case 
			when PSpell.PRVSN_END_FLAG = 'N' 
			then CAST(Encounter.DischargeDate as Date)
			else null
		end
	,[DischargeTime] = --CAST(Encounter.DischargeTime as Time(0))
		case 
			when PSpell.PRVSN_END_FLAG = 'N' 
			then CAST(Encounter.DischargeTime as Time(0))
			else null
		end
	,[DischargeDateTime] = --CAST(Encounter.DischargeTime as Datetime)
		case 
			when PSpell.PRVSN_END_FLAG = 'N' 
			then CAST(Encounter.DischargeTime as Datetime)
			else null
		end
	,[AdmissionConsultantCode] = Cons.NationalConsultantCode
	,[AdmissionConsultantName] = Cons.Consultant
	,[AdmissionDivision] = SpecBase.Division
	,[AdmissionDirectorate] = SpecBase.Direcorate
	,[AdmissionSpecialtyCode] = SpecBase.SubSpecialtyCode
	,[AdmissionSpecialty] = SpecBase.SubSpecialty
	,[AdmissionSpecialtyCode(Function)] = SpecBase.SpecialtyFunctionCode
	,[AdmissionSpecialty(Function)] = SpecBase.SpecialtyFunction
	,[ADmissionSpecialtyCode(Main)] = SpecBase.MainSpecialtyCode
	,[AdmissionSpecialty(Main)] = SpecBase.MainSpecialty
	,[AdmissionSpecialtyCode(RTT)]	= SpecBase.RTTSpecialtyCode
	,[AdmissiontWardCode] = SWard.CODE
	,[AdmissionWard] = SWard.NAME
	,[AdmissionMethodCode] = Admet.AdmissionMethodLocalCode
	,[AdmissionMethod] = Admet.AdmissionMethodDescription
	,[AdmissionMethodNHSCode] = Admet.AdmissionMethodNationalCode
	,[AdmissionSourceCode] = Adsor.AdmissionSourceLocalCode
	,[AdmissionSource] = Adsor.AdmissionSourceDescription
	,[AdmissionSourceNHSCode] = Adsor.AdmissionSourceNationalCode
	,[AdmissionCategoryCode] = Adcat.AdministrativeCategoryLocalCode
	,[AdmissionCategory] = Adcat.AdministrativeCategoryDescription
	,[AdmissionCategoryNHSCode] = Adcat.AdministrativeCategoryNationalCode
	,[IntendedManagementCode] = Inmgt.IntendedMangementLocalCode
	,[IntendedManagement] = Inmgt.IntendedMangementDescription
	,[IntendedManagementNHSCode] = Inmgt.IntendedMangementNationalCode
	,[FirstRegularDayOrNightAdmission] = Encounter.FirstRegDayOrNightAdmit
	,[InpatientStayCode] = Encounter.InpatientStayCode
	--,[DischargeWardCode] = Disch.DischWardCode
	--,[DischargeWard] = Disch.DischWardName
	,[DischargeWardCode] = 
		case when Encounter.DischargeDate is not null then Disch.DischWardCode end
	,[DischargeWard] = 
		case when Encounter.DischargeDate is not null then Disch.DischWardName end
	,DischargeDivision =  
		case when Encounter.DischargeDate is not null then Disch.DischargeDivision end
	,DischargeDirectorate =  
		case when Encounter.DischargeDate is not null then Disch.DischargeDirectorate end
	,DischargeSpecialtyCode =  
		case when Encounter.DischargeDate is not null then Disch.DischargeSpecialtyCode end
	,DischargeSpecialty =  
		case when Encounter.DischargeDate is not null then Disch.DischargeSpecialty end
	,DischargeConsultantCode =  
		case when Encounter.DischargeDate is not null then Disch.DischargeConsultantCode end
	,DischargeConsultantName =  
		case when Encounter.DischargeDate is not null then Disch.DischargeConsultantName end
	,[DischargeMethodCode] = Dismet.DischargeMethodLocalCode
	,[DischargeMethod] = Dismet.DishcargeMethodDescription
	,[DischargeMethodNHSCode] = Dismet.DischargeMethodNationalCode
	,[DischargeDestinationCode] = disde.DischargeDestinationLocalCode
	,[DischargeDestination] = disde.DischargeDestinationDescription
	,[DischargeDestinationNHSCode] = disde.DischargeDestinationNationalCode
	,[LengthOfSpell] = --Encounter.LOS
	--KO updated 06/11/2013 to handle provisional end flag
	--,LOS =
		datediff(
			 day
			,Encounter.AdmissionDate
			,case 
				when PSpell.PRVSN_END_FLAG = 'N' 
				then CAST(coalesce(Encounter.DischargeTime, dateadd(DAY, -1, getdate())) as Datetime)
				else CAST(dateadd(DAY, -1, getdate()) as Datetime)
			end
		)
	,[GPCode] = GP.PROCA_REFNO_MAIN_IDENT
	,[GP] = GP.SURNAME + ' ' + GP.FORENAME
	--,[PracticeCode] = 
	--				Case When (charindex('-',Pract.Practice) <1) Then
	--					cast(Pract.PracticeCode AS varchar)
	--				Else
	--					LTRIM(RTRIM(LEFT(Pract.Practice,CHARINDEX('-',Pract.Practice) -2)))
	--				End
	--,[Practice] = 
	--				Case When (charindex('-',Pract.Practice) <1) Then
	--					cast(Pract.Practice AS varchar)
	--				Else
	--					LTRIM(RTRIM(RIGHT(Pract.Practice,CHARINDEX('-',REVERSE(Pract.Practice)) -2)))
	--				End
	,[PracticeCode] = 
					Case When (charindex('-',Practice.Practice) <1) Then
						cast(Practice.PracticeCode AS varchar)
					Else
						LTRIM(RTRIM(LEFT(Practice.Practice,CHARINDEX('-',Practice.Practice) -2)))
					End
	,[Practice] = 
					Case When (charindex('-',Practice.Practice) <1) Then
						cast(Practice.Practice AS varchar)
					Else
						LTRIM(RTRIM(RIGHT(Practice.Practice,CHARINDEX('-',REVERSE(Practice.Practice)) -2)))
					End
	,[PCTCode] = Practice.PCTCode
	,[PCT] = Practice.PCT
	--KO updated 06/11/2013 to handle provisional end flag
	,[ProvisionalEndIndicator] = PSpell.PRVSN_END_FLAG
	,[SpellCCGCode] = Practice.CCGCode
	,[SpellCCG] = Practice.CCG
	,[SpellPCTCCGCode] = 
		case 
			when Encounter.AdmissionDate > '2013-03-31 00:00:00'
			then Practice.CCGCode
			else Practice.PCTCode
		end
	,[SpellPCTCCG] = 
		case 
			when Encounter.AdmissionDate > '2013-03-31 00:00:00'
			then Practice.CCG
			else Practice.PCT
		end
	,[PurchaserCode] = Purchaser.MAIN_IDENT
	,[Purchaser] = Purchaser.DESCRIPTION
	,[ResponsibleCommissionerCode] = [Encounter].ResponsibleCommissioner
	,[ResponsibleCommissioner] = ResponsibleCommissioner.PCTCCG
	,[ResponsibleCommissionerCCGOnlyCode] = [Encounter].ResponsibleCommissionerCCGOnly
	,[ResponsibleCommissionerCCGOnly] = ResponsibleCommissionerCCGOnly.PCTCCG
	,[TheatrePatientBookingKey] = TheatrePatientBookingKey
	,[TheatreCode] = Theatre.TheatreCode1
	,[Theatre] = Theatre.Theatre
	,[AgeCode] = Encounter.AgeCode
	,Age = WHREPORTING.dbo.GetAgeOnSpecifiedDate(pat.DTTM_OF_BIRTH, Encounter.AdmissionDate)
	,[AdmissionType] = case Encounter.AdmissionMethodTypeCode
						When 'EL' Then 'Elective'
						When 'EM' Then 'Emergency'
						When 'MAT' Then 'Maternity'
						When 'NE' Then 'Non-Elective'
						Else 'Other'
						End
	,[AdmissionCreated] = PSpell.CREATE_DTTM
	,[AdmissionCreatedByWhom] = 
							Case When UserCreate.Code is null Then
								PSpell.USER_CREATE
							Else
								UserCreate.[USER_NAME]
							End
	,[AdmissionUpdated] = PSpell.MODIF_DTTM
	,[AdmissionUpdatedByWhom] = 
							Case When UserModif.Code is null Then
								PSpell.USER_MODIF
							Else
								UserModif.[USER_NAME]
							End
	,[SmartboardEDD] = Smartboard.EDDValue
	,[SmartboardEDDSetOnDate] = Smartboard.EDDDate
	,[SmartboardEDDSetOnWard] = Smartboard.EDDWard
	--PAT_CLASS code copied from INFOSQL build ADM_DISCH_DATASET
	,PatientClass = 
	case 
		when NOT Admet.AdmissionMethodLocalCode IN ('11','12','13') 
		then
			case 
				when Admet.AdmissionMethodLocalCode IN ('BHOSP','BOHOSP') 
				then 'BABY'
				else 'NON ELECTIVE'
			end
		else
			case	
				when (Inmgt.IntendedMangementLocalCode IN ('2','4','9','8') 
				and case 
						when PSpell.PRVSN_END_FLAG = 'N' 
						then 
							case 
								when Encounter.DischargeDate IS NULL 
								then DATEDIFF(dd,CAST(Encounter.AdmissionDate AS DATETIME),GETDATE()) 
								else DATEDIFF(dd,CAST(Encounter.AdmissionDate AS DATETIME),CAST(Encounter.DischargeDate AS DATETIME)) 
							end
						else null
					end >=1) 
				then 'ELECTIVE INPATIENT'
				else
					case 
						when Inmgt.IntendedMangementLocalCode IN ('1','3','5') 
						then 'ELECTIVE INPATIENT'
						else 'DAY CASE'
					end
			end
	end
	,RTTCode = RTTST.MAIN_CODE
	--,FirstEDD = CAST (null as varchar(30))
	--,FirstEDDSetOnDate = CAST (null as datetime)
	--,FirstEDDSetOnWard = CAST (null as varchar(30))
	--,MinutesToFirstEDD = CAST (null as int)
	,OurActivityFlag = Encounter.OurActivityFlag
	,NotOurActProvCode = Encounter.NotOurActProvCode
	INTO APC.Spell
	FROM WHOLAP.dbo.OlapAPC Encounter
	left outer join WH.PAS.ServicePointBase SWard
		on Encounter.StartWardTypeCode = SWard.SPONT_REFNO
	left outer join WHOLAP.dbo.OlapPASConsultant Cons
		on Encounter.ConsultantCode = Cons.ConsultantCode
	left outer join WHOLAP.dbo.SpecialtyDivisionBase SpecBase
		on Encounter.SpecialtyCode = SpecBase.SpecialtyCode
	left outer join WH.PAS.ProfessionalCarerBase GP 
		on Encounter.EpisodicGpCode = GP.PROCA_REFNO	
		
	--left outer join WHOLAP.dbo.OlapPASPractice Pract
	--	on Encounter.RegisteredGpPracticeCode = Pract.PracticeCode
		
	left join WHOLAP.dbo.OlapPASPracticeCCG Practice
		--on Encounter.RegisteredGpPracticeCode = Pract.PracticeCode
		on case 
			when Encounter.EpisodicGpPracticeCode = -1
			then Encounter.RegisteredGpPracticeCode
			else Encounter.EpisodicGpPracticeCode
		end = Practice.PracticeCode

	left join LK.PasPCTOdsCCG ResponsibleCommissioner
		on ResponsibleCommissioner.PCTCCGCode = Encounter.ResponsibleCommissioner
	left join LK.PasPCTOdsCCG ResponsibleCommissionerCCGOnly
		on ResponsibleCommissionerCCGOnly.PCTCCGCode = Encounter.ResponsibleCommissionerCCGOnly
		
	left outer join Lorenzo.dbo.Purchaser Purchaser
		on Encounter.PurchaserCode = Purchaser.PURCH_REFNO	
	left outer join WHOLAP.dbo.OlapTheatre Theatre
		on Encounter.TheatreCode = Theatre.TheatreCode
	left outer join dbo.vwPASAdmissionMethod Admet 
		on Encounter.AdmissionMethodCode = Admet.AdmissionMethodCode
	left outer join dbo.vwPASAdmissionSource Adsor
		on Encounter.AdmissionSourceCode = Adsor.AdmissionSourceCode
	left join dbo.vwPASAdministrativeCategory Adcat
		on Encounter.AdminCategoryCode = Adcat.AdministrativeCategoryCode
	left outer join dbo.vwPASIntendedManagement Inmgt
		on Encounter.ManagementIntentionCode = Inmgt.IntendedMangementCode
	left outer join 
			(
			Select
				SourceSpellNo
				,EndWardTypeCode
				,DischWardCode = EndWard.SPONT_REFNO_CODE 
				,DischWardName = EndWard.NAME  
				,DischargeDivision = DisSpecBase.Division
				,DischargeDirectorate = DisSpecBase.Direcorate
				,DischargeSpecialtyCode = DisSpecBase.SubSpecialtyCode
				,DischargeSpecialty = DisSpecBase.SubSpecialty
				,DischargeConsultantCode = DisCons.NationalConsultantCode
				,DischargeConsultantName = DisCons.Consultant
			from WHOLAP.dbo.OlapAPC LastEncounter
			left join WH.PAS.ServicePointBase EndWard
				on LastEncounter.EndWardTypeCode = EndWard.SPONT_REFNO
			left join WHOLAP.dbo.SpecialtyDivisionBase DisSpecBase
				on LastEncounter.SpecialtyCode = DisSpecBase.SpecialtyCode
			left outer join WHOLAP.dbo.OlapPASConsultant DisCons
				on LastEncounter.ConsultantCode = DisCons.ConsultantCode
			Where LastEpisodeInSpellIndicator = 1
			) Disch
		on Encounter.SourceSpellNo = Disch.SourceSpellNo
	left outer join dbo.vwPASDischargeMethod Dismet
		on Encounter.DischargeMethodCode = Dismet.DischargeMethodCode
	left outer join dbo.vwPASDischargeDestination disde
		on Encounter.DischargeDestinationCode = disde.DischargeDestinationCode
	left outer join Lorenzo.dbo.ProviderSpell PSpell
		on Encounter.SourceSpellNo = PSpell.PRVSP_REFNO
	left join Lorenzo.dbo.Patient pat
		on Encounter.SourcePatientNo = pat.PATNT_REFNO 
	left outer join Lorenzo.dbo.[User] UserCreate
	on	PSpell.USER_CREATE = UserCreate.CODE
	and	not exists
		(
		select
			1
		from
			Lorenzo.dbo.[User] Previous
		where
			Previous.CODE = UserCreate.CODE
		and	Previous.USERS_REFNO > UserCreate.USERS_REFNO
		)

	left outer join Lorenzo.dbo.[User] UserModif
	on PSpell.USER_MODIF = UserModif.CODE
	and	not exists
		(
		select
			1
		from
			Lorenzo.dbo.[User] Previous
		where
			Previous.CODE = UserModif.CODE
		and	Previous.USERS_REFNO > UserModif.USERS_REFNO
		)
     --add Smartboard data
	left join UHSMApplications.SB.PatientSpellData Smartboard
	on Smartboard.SpellNumber = Encounter.SourceSpellNo
	
	left join WH.PAS.ReferenceValueBase RTTST 
		on PSpell.rttst_refno = RTTST.rfval_refno
--ccg
--left join OrganisationCCG.dbo.Postcode PostcodeCCG
--	on	PostcodeCCG.Postcode =
--		case
--		when len(enc.Postcode) = 8 then enc.Postcode
--		else left(enc.Postcode, 3) + ' ' + right(enc.Postcode, 4) 
--		end
WHERE
	Encounter.AdmissionTime = Encounter.EpisodeStartTime
;
select @RowsInserted = @@rowcount

Create Unique Clustered Index ixcSpell on APC.Spell(EncounterRecno);
Create Index ixFacilityID on APC.Spell(FaciltyID);
Create Index ixNHSNo on APC.Spell(NHSNo);
Create Index ixAdmitSpec on APC.Spell(AdmissionSpecialtyCode);
Create Index ixDivision on APC.Spell(AdmissionDivision);
Create Index ixDirectorate on APC.Spell(AdmissionDirectorate);
Create Index ixConsultant on APC.Spell(AdmissionConsultantCode);
Create Index ixAdmitSpecFunc on APC.Spell([AdmissionSpecialtyCode(Function)]);
Create Index ixAdmissionMethod on APC.Spell(AdmissionMethodCode);
Create Index ixGPCode on APC.Spell(GPCode)
Create Index ixPCTCode on APC.Spell(PCTCode)

CREATE NONCLUSTERED INDEX [ixDischargeDate]ON [APC].[Spell] ([DischargeDate])INCLUDE ([EncounterRecno],[SourceSpellNo])
CREATE NONCLUSTERED INDEX [ixSourceSpellNo] ON [APC].[Spell] ([SourceSpellNo] ) 

alter table WHREPORTING.APC.spell add [FirstEDD] varchar(20) NULL
alter table WHREPORTING.APC.spell add [FirstEDDSetOnDate] datetime NULL
alter table WHREPORTING.APC.spell add [FirstEDDSetOnWard] varchar(20) NULL
alter table WHREPORTING.APC.spell add [MinutesToFirstEDD] int NULL

--KO 18/09/2014 Add more smartboard data
update APC.Spell set 
	FirstEDD = firstEDD.EDDValue
	,FirstEDDSetOnDate = firstEDD.EDDDate
	,FirstEDDSetOnWard = firstEDD.EDDWard
	,MinutesToFirstEDD = datediff(MINUTE,  spell.AdmissionDateTime, firstEDD.EDDDate)
from WHREPORTING.APC.spell spell
left join UHSMApplications.SB.EDDAuditTrail firstEDD
	on firstEDD.SpellNo = spell.SourceSpellNo
	and firstEDD.FirstEDD = 1
	
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) +
	'. Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WHREPORTING APC.BuildSpell', @Stats, @StartTime