﻿CREATE procedure [APC].[BuildWaitingList] as

select
	 WaitingList.InterfaceCode
	,WaitingList.SourceUniqueID
	,WaitingList.SourcePatientNo
	,WaitingList.ReferralSourceUniqueID
	,WaitingList.DistrictNo
	,WaitingList.NHSNumber
	,WaitingList.PatientForename
	,WaitingList.PatientSurname
	,WaitingList.DateOfBirth
	,SpecialtyCode = Specialty.MainSpecialtyCode
	,ConsultantCode = Consultant.NationalConsultantCode
	,ListType = '?'
	,AdmissionMethodCode = AdmissionMethod.MainCode
	,AdminCategoryCode = AdminCategory.MainCode
	,ManagementIntentionCode = ManagementIntention.MainCode
	,Priority = Priority.MainCode
	,WaitingList.ReferralDate
	,WaitingList.OriginalDateOnWaitingList
	,WaitingList.DateOnWaitingList
	,WaitingList.SuspensionStartDate
	,WaitingList.SuspensionEndDate
	,SuspensionToDate = '?'
	,SuspensionPeriod = datediff(day, WaitingList.SuspensionStartDate, WaitingList.SuspensionEndDate)
	,PatientSuspensionEndDate = '?'
	,ReasonPatientSuspensionToDate = '?'
	,PatientReasonSuspensionPeriod = '?' --WaitingList.SocialSuspensionDays

	,Suspended =
		cast(
			case
			when WaitingList.SuspensionEndDate >= WaitingList.CensusDate
			then 1
			else 0
			end
			as bit
		)

	,SuspensionReason =
		case
		when WaitingList.SuspensionEndDate >= WaitingList.CensusDate
		then WaitingList.SuspensionReason
		else null
		end

	,RemovalDate = '?' --this is a waiting list so not applicable
	,WaitingList.ExpectedAdmissionDate
	,AdmissionDate = '?'
	,WaitingList.IntendedPrimaryOperationCode
	,BookingTypeCode = BookingType.MainCode
	,AnaestheticTypeCode = AnaestheticType.MainCode
	,ServiceTypeCode = ServiceType.MainCode
	,ListName = '?'
	,AdmissionWardCode = AdmissionWard.ServicePointLocalCode
	,WaitingList.EstimatedTheatreTime
	,WhoCanOperateCode = WhoCanOperate.MainCode
	,WaitingList.GeneralComment
	,WaitingList.PatientPreparation
	,Purchaser.PurchaserMainCode
	,RemovalReason = '?' --this is a waiting list so not applicable
	,WaitingList.WLStatus
	,NewStatus = '?'
	,OfferOutcome = '?'
	,EpisodeOutcome = '?'
	,WaitingList.TCIDate
	,RegisteredGpCode = GP.ProfessionalCarerMainCode
	,RegisteredGpStartDate = GP.StartDate
	,RegisteredGpEndDate = GP.EndDate

	,CancerCode =
		case WaitingList.WaitingListRuleCode
		when '10001223'
		then 'CD'
		when '10001222'
		then 'CS'
		else  'NON'
		end

	,WaitingList.PASCreated
	,WaitingList.PASCreatedByWhom
	,WaitingList.PASUpdated
	,WaitingList.PASUpdatedByWhom
	,WaitingList.ShortNoticeCode
	,AdmittedByDate = '?'
	,NextResetDate = '?'
	,PPID = '?'
	,RTTStatus.NationalRTTStatusCode
	,RTTStatus.RTTStatus
	,DiagnosticProcedure = '?'
	,SexCode = Sex.MainCode
	,PreOpClinicCode = '?'
	,PreOpDate = '?'
	,PreOpClinic = '?'
	,SecondaryWard = '?'

from
	WHOLAP.dbo.BaseAPCWaitingList WaitingList

left join WHOLAP.dbo.OlapPASConsultant Consultant
on Consultant.ConsultantCode = WaitingList.ConsultantCode

left join WHOLAP.dbo.SpecialtyDivisionBase Specialty
on Specialty.SpecialtyCode = WaitingList.SpecialtyCode

left join WH.PAS.ReferenceValue AdmissionMethod
on	AdmissionMethod.ReferenceValueCode = WaitingList.AdmissionMethodCode

left join WH.PAS.ReferenceValue AdminCategory
on	AdminCategory.ReferenceValueCode = WaitingList.AdminCategoryCode

left join WH.PAS.ReferenceValue ManagementIntention
on	ManagementIntention.ReferenceValueCode = WaitingList.ManagementIntentionCode

left join WH.PAS.ReferenceValue Priority
on	Priority.ReferenceValueCode = WaitingList.PriorityCode

left join WH.PAS.ReferenceValue BookingType
on	BookingType.ReferenceValueCode = WaitingList.BookingTypeCode

left join WH.PAS.ReferenceValue AnaestheticType
on	AnaestheticType.ReferenceValueCode = WaitingList.AnaestheticTypeCode

left join WH.PAS.ReferenceValue ServiceType
on	ServiceType.ReferenceValueCode = 1579

left join WH.PAS.ServicePoint AdmissionWard
on	AdmissionWard.ServicePointCode = WaitingList.SiteCode

left join WH.PAS.ReferenceValue WhoCanOperate
on	WhoCanOperate.ReferenceValueCode = WaitingList.WhoCanOperateCode

left join WH.PAS.Purchaser
on	Purchaser.PurchaserCode = WaitingList.PurchaserCode

left join WH.PAS.ProfessionalCarer GP
on	GP.ProfessionalCarerCode = WaitingList.RegisteredGpCode

left join WH.PAS.RTTStatus
on	RTTStatus.RTTStatusCode = WaitingList.RTTCurrentStatusCode

left join WH.PAS.ReferenceValue Sex
on	Sex.ReferenceValueCode = WaitingList.SexCode