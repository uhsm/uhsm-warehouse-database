﻿CREATE Procedure [APC].[BuildWardStay] As

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

If Exists (Select * from INFORMATION_SCHEMA.TABLES Where Table_Schema = 'APC' and TABLE_NAME = 'WardStay')
DROP TABLE APC.WardStay;

select
	 WardStay.SourceUniqueID
	,WardStay.SourceSpellNo
	,WardStay.SourcePatientNo
	,WardStay.StartTime
	,WardStay.EndTime
	,WardCode = Ward.ServicePointLocalCode
	,WardStay.ProvisionalFlag
	,WardStay.PASCreated
	,WardStay.PASUpdated
	,PASCreatedByWhom = PASCreatedBy.UserName
	,PASUpdatedByWhom = PASUpdatedBy.UserName
into
	APC.WardStay
from
	WH.APC.WardStay

left join WH.PAS.ServicePoint Ward
on Ward.ServicePointCode = WardStay.WardCode

left join WH.PAS.LorenzoUser PASCreatedBy
on	PASCreatedBy.UserUniqueID = WardStay.PASCreatedByWhom

left join WH.PAS.LorenzoUser PASUpdatedBy
on	PASUpdatedBy.UserUniqueID = WardStay.PASUpdatedByWhom


ALTER TABLE APC.WardStay ADD CONSTRAINT
	PK_WardStay PRIMARY KEY CLUSTERED 
	(
	SourceUniqueID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


CREATE NONCLUSTERED INDEX IX_WardStay ON APC.WardStay
	(
	SourceSpellNo
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


CREATE NONCLUSTERED INDEX IX_WardStay_1 ON APC.WardStay
	(
	SourcePatientNo
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WHREPORTING APC.BuildWardStay', @Stats, @StartTime