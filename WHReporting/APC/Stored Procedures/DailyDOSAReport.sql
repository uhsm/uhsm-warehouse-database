﻿/******************************************************************************
 Description: Lists patients for SSRS report: Daily DOSA Report

 
 Modification History --------------------------------------------------------

 Date     Author		Change
 25/08/15 Graham Ryder  Created

******************************************************************************/

CREATE Procedure [APC].[DailyDOSAReport]
 
as

Declare @StartDate	as Datetime	= DATEADD(day,DATEDIFF(day,0,GETDATE())-1,0)
Declare @EndDate	as Datetime	= DATEADD(day,DATEDIFF(day,0,GETDATE())-1,0)

Select 
E.FacilityID
,S.AdmissionSpecialty
,E.Specialty as LorenzoEpisodeSpecialty
,case when DATEDIFF(day,S.AdmissionDate,th.operationdate)=0 then'SameDay' else 'After' end as DayOfOp
,E.AdmissionDate 
,S.DischargeDate
,TH.OperationDate
,E.EpisodeStartDate
,E.EpisodeEndDate
,book.IntendedProcedureDate
,S.AdmissionWard
,S.AdmissionMethod
,S.AdmissionType
,s.PatientClass
,E.ConsultantName as LorenzoEpisodeConsultant
,Case when book.IntendedProcedureDate > S.AdmissionDate then 'Planned Wait' when book.IntendedProcedureDate = S.AdmissionDate then 'Planned same day' else 'Check' end as 'Planned DOSA'
,C.CancellationDate
,C.CancellationComment
,book.PatientSourceUniqueID
,book.TheatreCode
,book.Operation

from
WHREPORTING.APC.Episode E
		
	left join [WHREPORTING].[APC].[Spell] S
	on E.SourceSpellNo = S.SourceSpellNo

left join WH.Theatre.PatientBooking book
      on book.DistrictNo = S.FaciltyID	
      and IntendedProcedureDate 	>= E.AdmissionDate		

left join 	WH.Theatre.Cancellation C
		on C.PatientSourceUniqueID = book.SourceUniqueID
		
		left join [WH].[Theatre].[OperationDetail] TH
		  on TH.DistrictNo = S.FaciltyID	
			and
			TH.[OperationDate] >= E.AdmissionDate



where 
s.AdmissionMethodNHSCode in ('11','12')
and
E.AdmissionDate between @StartDate and @EndDate
and book.IntendedProcedureDate is not null

and book.TheatreCode not in ('0','24','25','28','30','32','33','34','35','36','41','42','43','44','45','50','51','52','53','55','56','58','59','68','69','70','73','74','79','80','81','82','83','84','85','86','87','88','89','90','91','92','93','94','95')
and (case when DATEDIFF(day,S.AdmissionDate,th.operationdate)=0 then'SameDay' else 'After' end)='After'
order by
E.FacilityID