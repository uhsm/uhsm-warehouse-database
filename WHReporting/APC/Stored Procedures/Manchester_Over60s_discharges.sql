﻿CREATE Procedure [APC].[Manchester_Over60s_discharges]
(
	@StartDate datetime
	,@EndDate datetime 
)

AS 

Set NoCount on

/*
Declare @StartDate datetime = '20150809'
Declare @EndDate datetime = '20150809'
*/

/*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
/*Query below creates list of all Day Case, Elective and Non-Elective discharges in selected 
period*/

SELECT distinct
      s.[FaciltyID]
      ,s.[NHSNo]
      ,p.[Sex]
      ,p.[PatientTitle]
      ,p.[PatientForename]
      ,p.[PatientSurname]
      ,p.[PatientAddress1]
      ,p.[PatientAddress2]
      ,p.[PatientAddress3]
      ,p.[PatientAddress4]
      ,mp.[Postcode]
      ,p.[HomePhone]
      ,p.[MobilePhone]
      ,s.AdmissionDateTime
      ,s.[DischargeDateTime]
      ,s.[Age]
      ,s.PatientClass                   as Admission_Attendance_Type
  into #admissions
  FROM [WHREPORTING].[APC].[Spell] s
       Left Join [WHREPORTING].[LK].[PatientCurrentDetails] p 
       on s.[FaciltyID] = p.FacilityID  
          and s.[DischargeDate] between @StartDate and @EndDate 
          and s.Age >= 60 
          and s.DischargeDestination = 'Usual Place of residence'
          and s.PatientClass in ('ELECTIVE INPATIENT', 'NON ELECTIVE')
       inner join [WHREPORTING].LK.APCMancPostcodes mp  
       on REPLACE(p.[Postcode],' ','') = mp.SlimPostcode
          and p.[DeceasedFlag] = 'N'

/*xxxxxxxxxxxxxxxxxxxxxxxxx*/
/*Query below pulls out all AandE attendances and then compares this to those
on the admitted data in temp table created above.
  This pulls through those AE attends that haven't appeared in the list above*/
SELECT distinct
      LocalPatientID                as FacilityID
      ,e.[NHSNumber]
      ,p.[Sex]
      ,p.[PatientTitle]
      ,p.[PatientForename]
      ,p.[PatientSurname]
      ,p.[PatientAddress1]
      ,p.[PatientAddress2]
      ,p.[PatientAddress3]
      ,p.[PatientAddress4]
      ,mp.[Postcode]
      ,p.[HomePhone]
      ,p.[MobilePhone]
      ,e.ArrivalTime                   as AdmissionDateTime
      ,e.AttendanceConclusionTime      as DischargeDateTime
	  ,e.AgeOnArrival                  as Age
	  ,'AandE_Attendance'              as Admission_Attendance_Type
into #AEattends    
FROM [WH].[AE].[Encounter] e 
       Left Join [WHREPORTING].[LK].[PatientCurrentDetails] p 
        on e.LocalPatientID = p.FacilityID  
     
      Left join #admissions ss
	    on E.LocalPatientID = ss.FaciltyID and ss.AdmissionDateTime 
	  between e.ArrivalTime and dateadd(minute,240,e.AttendanceConclusionTime)
	   /*pulls through any patients from the admissions temp table where 
	   that has an admission between the start of the AE encounter 
	   and up to 4 hours after discharge*/
	inner join [WHREPORTING].LK.APCMancPostcodes mp  
       on REPLACE(e.[Postcode],' ','') = mp.SlimPostcode
  
where     cast(e.AttendanceConclusionTime as date) between @StartDate and @EndDate 
          and e.AgeOnArrival >= 60
          and p.[DeceasedFlag] = 'N'
          and ss.FaciltyID is null 
          and e.AttendanceDisposalCode in ('DA')

/*xxxxxxxxxxxxxxxxxxxxxxxxx*/
/*Compile results*/

Select distinct
	* 
From #admissions

union all

Select distinct
	* 
From #AEattends 

/*xxxxxxxxxxxxxxxxxxxxxxxxx*/
Drop table #admissions,#AEattends