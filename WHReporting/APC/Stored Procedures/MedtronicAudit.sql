﻿CREATE procedure [APC].[MedtronicAudit] 
	@ReportDate datetime
as
/**
----------------------------------------------------------------------------------------------------------------
Action		Date		Author		Comments
----------------------------------------------------------------------------------------------------------------
Created		11/09/2014	KO			To replace InfoSQL code S:\CO-HI-Information\New File Management - Jan 2011\Scheduled Care\1_Cardiothoracic\
									Medtronic Hospital Solutions\NS_Cardiology_AdditionstoWL_INFOSQL.sql
----------------------------------------------------------------------------------------------------------------

to use: exec APC.MedtronicAudit '2014-08-31'
*/
--declare @ReportDate as datetime
--set @ReportDate = '31 Aug 2014' --Change the date to reflect the last date of the month

select MonthYear = Datename(month, ipwl.DateOnList) + '-' + Datename(year, ipwl.DateOnList)
	,ipwl.ReferralSourceUniqueID
	,ipwl.SpecialtyCode
	,ipwl.Consultant
	,ConsultantName = cons.Forename + ' ' + cons.Surname
	,ipwl.ListType
	,ipwl.ElectiveAdmissionMethod
	,ipwl.AdminCategory
	,ipwl.IntendedManagement
	,man.IntendedMangementDescription
	,ipwl.Priority
	,ipwl.PlannedProcedure
	,ipwl.ServiceType
	,ipwl.ListName
	,ipwl.AdmissionWard
	,ipwl.RemovalReason
	,ipwl.EntryStatus
	,ipwl.OfferOutcome
	,PCEOutcome
	,ipwl.CancerCode
from WH.PTL.IPWL ipwl
left join dbo.vwPASIntendedManagement man
	on man.IntendedMangementLocalCode = ipwl.IntendedManagement
left join 
	(
	select distinct ConsultantCode, Forename, Surname
	from LK.ConsultantSpecialty 
	)cons
	on cons.ConsultantCode = ipwl.Consultant

where ipwl.DateOnList between '2011-04-01'
		and @ReportDate
	and ipwl.SpecialtyCode = '320'
--order by ipwl.ReferralSourceUniqueID desc