﻿CREATE procedure [APC].[PADSPatientDetail]

@PadsYesVar as varchar,
@PadsYes as varchar,
@SForOlder as varchar,
@AOI as varchar,
@AdmissionMonth as varchar,
@LOS15Days as varchar,
@Mortality as varchar,
@Readmissions1Day as varchar,
@Readmissions7Days as varchar,
@Readmissions30Days as varchar,
@WardMovesZero as varchar,
@WardMovesAAZero as varchar,
@WardMovesOne as varchar,
@WardMovesAAOne as varchar,
@WardMovesTwo as varchar,
@WardMovesAATwo as varchar,
@WardMovesOverTwo as varchar,
@WardMovesAAOverTwo as varchar,
@DischargedEDD as varchar,
@Discharged as varchar,
@SForOlderVar as varchar,
@AOIVar as varchar,
@AdmissionMonthVar as varchar,
@LOS15DaysVar as varchar,
@MortalityVar as varchar,
@Readmissions1DayVar as varchar,
@Readmissions7DaysVar as varchar,
@Readmissions30DaysVar as varchar,
@WardMovesZeroVar as varchar,
@WardMovesAAZeroVar as varchar,
@WardMovesOneVar as varchar,
@WardMovesAAOneVar as varchar,
@WardMovesTwoVar as varchar,
@WardMovesAATwoVar as varchar,
@WardMovesOverTwoVar as varchar,
@WardMovesAAOverTwoVar as varchar,
@DischargedEDDVar as varchar,
@DischargedVar as varchar

as

set @PadsYesVar=@PadsYes
set @SForOlderVar=@SForOlder
set @AOIVar=@AOI
set @AdmissionMonthVar=@AdmissionMonth
set @LOS15DaysVar=@LOS15Days
set @MortalityVar=@Mortality
set @Readmissions1DayVar=@Readmissions1Day
set @Readmissions7DaysVar=@Readmissions7Days
set @Readmissions30DaysVar=@Readmissions30Days
set @WardMovesZeroVar=@WardMovesZero
set @WardMovesAAZeroVar=@WardMovesAAZero
set @WardMovesOneVar=@WardMovesOne
set @WardMovesAAOneVar=@WardMovesAAOne
set @WardMovesTwoVar=@WardMovesTwo
set @WardMovesAATwoVar=@WardMovesAATwo
set @WardMovesOverTwoVar=@WardMovesOverTwo
set @WardMovesAAOverTwoVar=@WardMovesAAOverTwo
set @DischargedEDDVar=@DischargedEDD
set @DischargedVar=@Discharged



select *

from

PADS2.dbo.vwPatientDetail
WHERE  
PadsYes in (@PadsYes)
and AssessedOnInterventionalWard in (@AOIVar)
and AdmissionMonthKey IN (@AdmissionMonthVar)
and LOS15Days IN (@LOS15DaysVar)
and OutcomePatientDied in (@MortalityVar)
and Readmission24Hours in (@Readmissions1DayVar)
and Readmission7Days in (@Readmissions7DaysVar)
and Readmission30Days in (@Readmissions30DaysVar)
and WardMovesZero in (@WardMovesZeroVar)
and WardMovesAAZero in (@WardMovesAAZeroVar)
and WardMoves1 in (@WardMovesOneVar)
and WardMovesAA1 in (@WardMovesAAOneVar)
and WardMoves2 in (@WardMovesTwoVar)
and WardMovesAA2 in (@WardMovesAATwoVar)
and WardMovesOver2 in (@WardMovesOverTwoVar)
and WardMovesAAOver2 in (@WardMovesAAOverTwoVar)
and DischargedOnOrBeforePADSEDD in (@DischargedEDDVar)
and Discharged in (@DischargedVar)