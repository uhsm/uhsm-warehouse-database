﻿CREATE procedure [APC].[PivotAdmissionsByPatientClass] 
	@EndDate datetime
as
/**
----------------------------------------------------------------------------------------------------------------
Action		Date		Author		Comments
----------------------------------------------------------------------------------------------------------------
Created		11/09/2014	KO			To replace InfoSQL code S:\CO-HI-Information\Ijaz Sarwar\AdhocReporting.mdb
									--QryTotalElectiveandNonElecAdmissionsbyMonthforMaresaJohnson
----------------------------------------------------------------------------------------------------------------

to use: exec APC.PivotAdmissionsByPatientClass '2014-08-31'
*/

declare @StartDate datetime
--declare @EndDate datetime
declare @cols varchar(500)   --dynamically define column headers
declare @sql varchar(4000)   

select @StartDate = '2013-04-01'
--select @EndDate = '2014-08-31'

--Get dynamic header names
select @cols = isnull(@cols + ',', '') + '[' + AdmissionMonth + ']'
from 
	(
	select distinct AdmissionMonth = convert(varchar(6), AdmissionDate, 112)
	from APC.Spell
	where AdmissionDate between @StartDate and @EndDate
	)mon
order by AdmissionMonth
--print @cols

set @sql = 
	'select * from (
		select 
			PatientClass,
			AdmissionMonth = convert(varchar(6), AdmissionDate, 112),
			cnt = count(1)
		from APC.Spell
		where AdmissionMethodNHSCode in (''11'',''12'',''13'',''21'',''22'',''24'',''28'',''81'')
			and AdmissionDate between ''' + 
			convert(varchar(10), @StartDate, 120) 
			+ ''' and ''' + 
			convert(varchar(10), @EndDate, 120) +
		''' group by PatientClass, convert(varchar(6), AdmissionDate, 112)
	)grp
	PIVOT (SUM(cnt) FOR AdmissionMonth IN ('
	+ @cols +
	')) AS pvt'

print @sql
execute(@sql)

/*
Resulting code:
select * from (
		select 
			PatientClass,
			AdmissionMonth = convert(varchar(6), AdmissionDate, 112),
			cnt = count(1)
		from APC.Spell
		where AdmissionMethodNHSCode in ('11','12','13','21','22','24','28','81')
			and AdmissionDate between '2013-04-01' and '2014-08-31' group by PatientClass, convert(varchar(6), AdmissionDate, 112)
	)grp
	PIVOT (SUM(cnt) FOR AdmissionMonth IN ([201304],[201305],[201306],[201307],[201308],[201309],[201310],[201311],[201312],[201401],[201402],[201403],[201404],[201405],[201406],[201407],[201408])) AS pvt
*/