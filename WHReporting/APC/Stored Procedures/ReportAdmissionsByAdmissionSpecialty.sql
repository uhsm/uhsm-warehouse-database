﻿-- =============================================
-- Author:		CMan
-- Create date: 11/02/2014
-- Description:	Stored Procedure for SSRS report requested by PMcConnell to extract the number of admissiona per month for Gastroenterology and Endocrinology.  Have incorporated 
--				all specialties with a dropdown to filter on specialty so that the report can be used for other speciaties

-- =============================================
CREATE PROCEDURE [APC].[ReportAdmissionsByAdmissionSpecialty]
	-- Add the parameters for the stored procedure here
@month as varchar(400), @spec as varchar(100), @AdmMethod varchar(200)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


SELECT cal.TheMonth AS AdmissionMonth,
       cal.FirstDateOfMonth AdmissionMonthStart,
       [AdmissionSpecialty(Main)] as AdmissionSpecialtyMain,
       sp.SourceSpellNo,
       FaciltyID PatientID,
       AdmissionDateTime,
       DischargeDateTime,
       AdmissionSource SourceOfAdmission,
       DischargeDestination,
       AdmissionMethod,
       DischargeMethod,
       AdmissionConsultantName 
FROM   WHREPORTING.APC.Spell sp
       LEFT OUTER JOIN WHREPORTING.LK.Calendar cal
                    ON sp.AdmissionDate = cal.TheDate 
Where FirstDateOfMonth IN (SELECT Item
                         FROM   dbo.Split ((@month), ',')) 

and [AdmissionSpecialty(Main)] IN (SELECT Item
                         FROM   dbo.Split ((@spec), ',')) 
and AdmissionMethodCode  IN (SELECT Item
                         FROM   dbo.Split ((@AdmMethod), ',')) 

Group by cal.TheMonth,
       cal.FirstDateOfMonth,
       [AdmissionSpecialty(Main)] ,
       FaciltyID ,
       sp.SourceSpellNo,
       AdmissionDateTime,
       DischargeDateTime,
       AdmissionSource ,
       DischargeDestination,
       AdmissionMethod,
       DischargeMethod,
       AdmissionConsultantName 



END