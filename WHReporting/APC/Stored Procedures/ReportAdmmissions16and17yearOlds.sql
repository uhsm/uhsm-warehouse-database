﻿/*
==========================================================================================================================    
Purpose :    [APC].[ReportAdmmissions16and17yearOlds]   -  data for Admission list for 16 and 17 year Olds SSRS Report     

Notes :      Stored in WHREPORTING.APC
             This Procedure has been created for use in the Admission list for 16 and 17 year Olds SSRS Report in the location below
             S:\CO-HI-Information\New File Management - Jan 2011\Data Warehouse Project\SSRS Report Code\Admissions_Discharges\Admissions\Admissions
           
             This has been designed to populate the data for a list of all Admissions for 16 and 17 year Olds with the option to to select a date period.
           
Author  :     Peter Asemota
Store Proc Create Date :  05/05/2016 
Version :     1.0.0.1  
Note : commentted out the join to the [APC].[Clinicalcoding] as this join is not required in the report 

Original version :   UHSM\GFenton 
Create date : 11/02/2016   
Versioon : 1.0.0.0 embedded script in SSRS report   
========================================================================================================================
*/
CREATE PROCEDURE [APC].[ReportAdmmissions16and17yearOlds]
   (
     @StartDate DATETIME
	,@EndDate DATETIME
	)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET DATEFORMAT DMY
/*  -- test purposes only---
    Declare @StartDate AS DATETIME = '20160401'
           ,@EndDate AS DATETIME = '20160430'
          */
	SELECT CONVERT(VARCHAR(10), S.AdmissionDate, 105) AS 'AdmissionDate'
		,S.FaciltyID AS 'RM2_Number'
		,P.Sex
		,P.DateOfDeath
		,CASE 
			WHEN P.DateOfDeath IS NULL
				THEN 'N'
			ELSE 'Y'
			END AS Deceased
		,P.PatientSurname
		,P.PatientAddress1
		,P.PatientAddress2
		,P.PatientAddress3
		,P.PatientAddress4
		,P.PostCode
		,S.AdmissionConsultantName
		,S.AdmissionMethod
		,DATEDIFF("d", S.AdmissionDate, CAST(GETDATE() AS DATE)) AS LengthOfSpell
		,year(S.[AdmissionDate]) AS AdmYear
		,month(S.[AdmissionDate]) AS AdmMonth
		,CONVERT(VARCHAR(10), S.[AdmissionDateTime], 103) + ' ' + CONVERT(VARCHAR(10), S.[AdmissionDateTime], 8) AS [AdmissionDateTime]
		,[DischargeDateTime]
		,S.Age
		,S.[AdmissionSpecialty(Function)]
		,[PatientClass]
		,S.AdmissiontWardCode
		,CONVERT(VARCHAR(10), DW.StartTime, 103) + ' ' + CONVERT(VARCHAR(10), DW.StartTime, 8) AS 'Ward StartTime'
		,DW.WardCode AS CurrentWard
		,S.[DischargeDate]
		,CONVERT(VARCHAR(10), S.AdmissionDate, 105) + ' ( ' + S.FaciltyID + ' ) ' AS [Distinct_Admissions]
	FROM [WHREPORTING].[APC].[Spell] S
	--LEFT JOIN [WHREPORTING].[APC].[ClinicalCoding] cc ON s.EncounterRecno = cc.EncounterRecno  -- PA not required
	LEFT JOIN WHREPORTING.APC.Patient P ON P.EncounterRecno = S.EncounterRecno
	LEFT JOIN [WHREPORTING].[APC].[WardStay] DW ON S.SourceSpellNo = DW.SourceSpellNo
		AND DW.ProvisionalFlag <> 1
	WHERE S.[AdmissionDate] >= @StartDate
		AND S.[AdmissionDate] <= @EndDate
		-- and [DischargeDate] <= @Discharged)
		AND P.PatientSurname <> 'Xxtest' -- excluded test patients
		-- excludes children wards i.e. babies and private wards
		AND DW.WardCode NOT IN (
			 'SCB' 
			,'ASPIRE'
			,'ALE'
			,'ALEX PCT'
			,'SPIREW'
			,'BWH'
			)
		AND S.Age BETWEEN 16
			AND 17
		order by 1,2
			
END