﻿CREATE procedure [APC].[ReportAlertsReport]

@Division as varchar(50),
@Admitting_Specialty as varchar(100),
@Ward as varchar(50)

AS
	
SELECT 
		p.NHSNo
	,	apc.FaciltyID as 'RM2 Number'
	,	p.PatientSurname + ', ' + p.PatientForename as 'Patient Name'
	,	CONVERT(varchar(10),p.DateOfBirth,20) as 'Date of Birth'
	,	CONVERT(varchar(10),apc.AdmissionDate,20)  as 'Admission Date'
	,	w.WardCode as 'Ward Code'
	,	d.DESCRIPTION as 'Alert'
	,	apc.AdmissionSpecialty as 'Admitting Specialty'
	,	apc.AdmissionDivision as 'Division'


FROM	APC.wardstay w
		
		left join APC.Spell apc
			on w.SourceSpellNo = apc.SourceSpellNo
		left join lorenzo.dbo.ClinicalCoding c
			on apc.SourcePatientNo = c.PATNT_REFNO
		left join lorenzo.dbo.diagnosis d
			on c.CODE = d.code  
		left join APC.Patient p
			on apc.SourcePatientNo = p.SourcePatientNo

			
		inner join 
        (
			select max(w1.StartTime) maxdate, w1.SourceSpellNo
						from APC.wardstay w1
						group by w1.SourceSpellNo
		) mx
				  On w.SourceSpellNo = mx.SourceSpellNo
				  And w.StartTime = mx.maxdate
			

WHERE  

		c.DPTYP_CODE = 'alert'
	AND
		 cast(apc.admissiondate as date)=
			DATEADD(DAY,-1, cast(GETDATE() as date))
	AND
		c.ARCHV_FLAG = 'N'
	AND
		d.ARCHV_FLAG = 'N'
	AND
		c.code in ('009' -- MRSA and Ill-defined intestinal Infections
				,  'INFC003' -- ESBL
				,  'INFC005' -- VRE
				,  'INFC006' -- MRSA
				,  'INFC001') -- C.diff
	AND
		apc.AdmissionDivision in (@Division)
	AND
		apc.AdmissionSpecialty in (@Admitting_Specialty)
	AND
		w.WardCode in (@Ward)
		
		
GROUP BY 
			p.NHSNo
	, apc.FaciltyID
	,	p.PatientSurname + ', ' + p.PatientForename
	,	CONVERT(varchar(10),p.DateOfBirth,20)
	,	CONVERT(varchar(10),apc.AdmissionDate,20)
	,	w.WardCode
	,	d.DESCRIPTION
	,	apc.AdmissionSpecialty
	,	apc.AdmissionDivision

ORDER BY 
		p.NHSNo