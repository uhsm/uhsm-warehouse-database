﻿/*
---------------------------------------------------------------------------------------------------------------------------------
  
   Purpose :    Coded Activity Report By Consultant - data for SSRS Report on Coded admitted activity by consultant
   
   Notes  :    Stored in WHREPORTING.OP as [APC].[CodedAdmittedActivityByConsultant]
           
                This procedure has been created for use in the Coded Admitted Activity by Consultant report in the location below
                \\uhsm-sa2\shared\CO-HI-Information\New File Management - Jan 2011\Data Warehouse Project\SSRS Report Code\Admissions_Discharges\Admissions
                This has been designed to populate DNA rate for outpatients by Consultant on by weekly and monthly basis 
                
   Versions		 1.0.0.0 - 21/06/2016 - Peter Asemota   
                 Create Stored Procedure detailing coded activity of patients that have been seen  by different consultant
                 The report defaults to the previous week admitted activity with the option to select a time period
 --------------------------------------------------------------------------------------------------------------------------------- 
*/
CREATE PROCEDURE [APC].[ReportCodedActivityByConsultant] (
	 @StartDate AS DATETIME
	,@EndDate AS DATETIME
	,@Specialty AS VARCHAR(MAX)
	,@Consultant AS VARCHAR(MAX)
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET DATEFORMAT DMY

	/* **********Testing purposes only*******************	

Declare @StartDate as Datetime = '20160613'
        ,@EndDate as Datetime = '20160619'
	    ,@Specialty AS VARCHAR(MAX)  = '180,317,103,161,320,170,192,307,654'
	    ,@Consultant AS VARCHAR(MAX) = 'C6106757,C7399661,C4188434,C3558238,C4312686,C4629256,C5203575,C6099877,C3564341'
	    
	   -- */
	SELECT EPI.FacilityID
		,PAT.PatientSurname
		,EPI.AdmissionDate
		,EPI.DischargeDate
		,EPI.EpisodeEndDate
		,EPI.AdmissionType
		,EPI.Division
		,EPI.Directorate
		,EPI.[SpecialtyCode(Function)]
		,EPI.[Specialty(Function)]
		,EPI.ConsultantCode
		,EPI.ConsultantName
		,CC.PrimaryDiagnosis
		,CC.SubsidiaryDiagnosis
		,CC.SecondaryDiagnosis1
		,CC.SecondaryDiagnosis2
		,CC.SecondaryDiagnosis3
		,CC.SecondaryDiagnosis4
		,CC.SecondaryDiagnosis5
		,CC.SecondaryDiagnosis6
		,CC.SecondaryDiagnosis7
		,CC.SecondaryDiagnosis8
		,CC.SecondaryDiagnosis9
		,CC.SecondaryDiagnosis10
		,CC.PriamryProcedure
		,CC.PrimaryProcedureDate
		,CC.SecondaryProcedure1
		,CC.SecondaryProcedure2
		,CC.SecondaryProcedure3
		,CC.SecondaryProcedure4
		,CC.SecondaryProcedure5
		,CC.SecondaryProcedure6
		,SPE.SourceSpellNo
		,DATEDIFF(D, EPI.AdmissionDate, EPI.DischargeDate) AS LoS
	FROM APC.Episode AS EPI
	LEFT OUTER JOIN APC.Patient AS PAT ON EPI.EncounterRecno = PAT.EncounterRecno
	LEFT OUTER JOIN APC.ClinicalCoding AS CC ON EPI.EncounterRecno = CC.EncounterRecno
	LEFT OUTER JOIN APC.Spell AS SPE ON EPI.SourceSpellNo = SPE.SourceSpellNo
	INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Specialty, ',') sp ON EPI.[SpecialtyCode(Function)] = sp.SplitValue
	INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Consultant, ',') Con ON EPI.ConsultantCode = Con.SplitValue
	WHERE (EPI.DischargeDate >= @StartDate)
		AND (EPI.DischargeDate <= @EndDate)
		AND (EPI.LastEpisodeInSpell = 1)
	ORDER BY EPI.FacilityID
END