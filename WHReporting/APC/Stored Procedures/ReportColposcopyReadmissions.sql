﻿/*
--Author: J Cleworth
--Date created: 10/09/2013
--Stored Procedure Built for SRSS Report on:

*/

CREATE Procedure [APC].[ReportColposcopyReadmissions]


@Quarter as int

AS

select originalspell.FaciltyID,
originalspell.NHSNo,
originalspell.SpellOrder,
originalspell.SourceSpellNo,
originalspell.PrimaryDiagnosisCode,
originalspell.PrimaryProcedureCode,
originalspell.AdmissionDate,
originalspell.DischargeDate,
originalspell.AdmissiontWardCode,
originalspell.pod,
0 as ReadmissionIntervalDays


 from 

(select distinct
episode.EncounterRecno,
FaciltyID,
spell.NHSNo,
'Original' as SpellOrder,
spell.SourceSpellNo,
episode.PrimaryDiagnosisCode,
episode.PrimaryProcedureCode,
spell.AdmissionDate,
spell.DischargeDate,
spell.AdmissiontWardCode,
CASE WHEN IntendedManagementCode=2 AND spell.admissionDate=spell.DischargeDate then 'DC'
WHEN AdmissionMethodNHSCode between 21 and 28 then 'NEL'
else 'EL' end as POD



from apc.Spell spell
inner join apc.Episode episode on episode.SourceSpellNo=spell.SourceSpellNo
left join LK.Calendar c on c.thedate=spell.AdmissionDate
where 
c.FinancialQuarterKey in (@Quarter)
and PrimaryProcedureCode in ('Q014','Q033') 
--LastEpisodeInSpell=1 AND
/*spell.SourceSpellNo in (

select episode.SourceSpellNo from apc.ClinicalCoding coding inner join apc.Episode episode2 on episode2.EncounterRecno=coding.EncounterRecno
where PriamryProcedureCode IN ('Q014','Q033') 
OR SecondaryProcedureCode1 IN ('Q014','Q033')
OR SecondaryProcedureCode2 IN ('Q014','Q033')
OR SecondaryProcedureCode3 IN ('Q014','Q033')
OR SecondaryProcedureCode4 IN ('Q014','Q033')
OR SecondaryProcedureCode5 IN ('Q014','Q033')
OR SecondaryProcedureCode6 IN ('Q014','Q033')
OR SecondaryProcedureCode7 IN ('Q014','Q033')
OR SecondaryProcedureCode8 IN ('Q014','Q033')
OR SecondaryProcedureCode9 IN ('Q014','Q033')
OR SecondaryProcedureCode10 IN ('Q014','Q033')
OR SecondaryProcedureCode11 IN ('Q014','Q033')
)*/
) as originalspell

inner join 


(select PreviousAdmissionRecno,ReadmissionRecno,ReadmissionIntervalDays from WHOLAP.dbo.FactReadmission readm
where ReadmissionIntervalDays<22
and readm.ReadmissionAdmissionMethodCode in(2000614,
2000615,
2000616,
2000617,
2000685,
2003471)

)fr


on fr.PreviousAdmissionRecno=originalspell.EncounterRecno

inner join 

(select distinct
episode3.EncounterRecno,
FaciltyID,
spell3.NHSNo,
'Readmission' as SpellOrder,
spell3.SourceSpellNo,
episode3.PrimaryDiagnosisCode,
episode3.PrimaryProcedureCode,
spell3.AdmissionDate,
spell3.DischargeDate,
spell3.AdmissiontWardCode,
'NEL' AS POD

from apc.Spell spell3
inner join apc.Episode episode3
on episode3.SourceSpellNo=spell3.SourceSpellNo
) as readmission on readmission.encounterrecno=fr.ReadmissionRecno

union

select readmission.FaciltyID,
readmission.NHSNo,
readmission.SpellOrder,
readmission.SourceSpellNo,
readmission.PrimaryDiagnosisCode,
readmission.PrimaryProcedureCode,
readmission.AdmissionDate,
readmission.DischargeDate,
readmission.AdmissiontWardCode,
readmission.POD,
ReadmissionIntervalDays



 from 

(select distinct
episode.EncounterRecno,
FaciltyID,
spell.NHSNo,
'Original' as SpellOrder,
spell.SourceSpellNo,
episode.PrimaryDiagnosisCode,
episode.PrimaryProcedureCode,
spell.AdmissionDate,
spell.DischargeDate,
spell.AdmissiontWardCode,
CASE WHEN IntendedManagementCode=2 AND spell.admissionDate=spell.DischargeDate then 'DC'
WHEN AdmissionMethodNHSCode between 21 and 28 then 'NEL'
else 'EL' end as POD



from apc.Spell spell
inner join apc.Episode episode on episode.SourceSpellNo=spell.SourceSpellNo
inner join LK.Calendar c2 on c2.TheDate=spell.AdmissionDate
where 
c2.FinancialQuarterKey in (@Quarter) and PrimaryProcedureCode in ('Q014','Q033') 
--LastEpisodeInSpell=1 AND
/*spell.SourceSpellNo in (

select episode.SourceSpellNo from apc.ClinicalCoding coding inner join apc.Episode episode2 on episode2.EncounterRecno=coding.EncounterRecno
where PriamryProcedureCode IN ('Q014','Q033') 
OR SecondaryProcedureCode1 IN ('Q014','Q033')
OR SecondaryProcedureCode2 IN ('Q014','Q033')
OR SecondaryProcedureCode3 IN ('Q014','Q033')
OR SecondaryProcedureCode4 IN ('Q014','Q033')
OR SecondaryProcedureCode5 IN ('Q014','Q033')
OR SecondaryProcedureCode6 IN ('Q014','Q033')
OR SecondaryProcedureCode7 IN ('Q014','Q033')
OR SecondaryProcedureCode8 IN ('Q014','Q033')
OR SecondaryProcedureCode9 IN ('Q014','Q033')
OR SecondaryProcedureCode10 IN ('Q014','Q033')
OR SecondaryProcedureCode11 IN ('Q014','Q033')
)*/
) as originalspell

inner join 


(select PreviousAdmissionRecno,ReadmissionRecno,ReadmissionIntervalDays from WHOLAP.dbo.FactReadmission readm
where ReadmissionIntervalDays<22
and readm.ReadmissionAdmissionMethodCode in(2000614,
2000615,
2000616,
2000617,
2000685,
2003471)

)fr


on fr.PreviousAdmissionRecno=originalspell.EncounterRecno

inner join 

(select distinct
episode3.EncounterRecno,
FaciltyID,
spell3.NHSNo,
'Readmission' as SpellOrder,
spell3.SourceSpellNo,
episode3.PrimaryDiagnosisCode,
episode3.PrimaryProcedureCode,
spell3.AdmissionDate,
spell3.DischargeDate,
spell3.AdmissiontWardCode,
'NEL' AS POD

from apc.Spell spell3
inner join apc.Episode episode3
on episode3.SourceSpellNo=spell3.SourceSpellNo
) as readmission on readmission.encounterrecno=fr.ReadmissionRecno