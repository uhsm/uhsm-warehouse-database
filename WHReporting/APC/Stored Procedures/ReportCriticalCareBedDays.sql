﻿/*
--Author: J Cleworth
--Date created: 13/02/2013
--Stored Procedure Built for SRSS Report on:
--Bed Days Actual and Finance

*/


CREATE Procedure [APC].[ReportCriticalCareBedDays]


@Month AS int,
@Ward AS VARCHAR (25),
@TransplantSLT AS INT,
@TransplantDLT AS INT,
@TransplantHT AS INT,
@TransplantVAD as int,
@CABG as int,
@TAVI as int,
@Valve as int,
@ECMO as int


as

select 
FDays.FinancialMonthKey,
--FDays.TheMonth,
FDays.[SpecialtyCode(Function)],
FDays.SourceSpellNo
,FDays.SPONT_REFNO_CODE
,FDays.TransplantSLT
,FDays.TransplantDLT
,FDays.TransplantHT
,FDays.TransplantVAD
,FDays.CABG
,FDays.TAVI
,FDays.Valve
,FDays.ECMO
,sum(case when FDays.CARE_LEVEL=2 then FDays.ActualBedDays else 0 end) AS L2FinanceBedDays,
sum(case when FDays.CARE_LEVEL=3 then FDays.ActualBedDays else 0 end) AS L3FinanceBedDays
,isnull(sum(case when FDays.CARE_LEVEL=2 then ADays.BedDays else 0 end),0) AS L2ActualBedDays,
isnull(sum(case when FDays.CARE_LEVEL=3 then ADays.BedDays else 0 end),0) AS L3ActualBedDays

from


(Select Distinct 
cal.FinancialMonthKey,
cal.TheMonth,
CCPeriod.SourceSpellNo
,SPONT_REFNO_CODE
,CARE_LEVEL
,EPI.[SpecialtyCode(Function)]
,Case when TXTYPE='SL' Then 1 else 0 end as TransplantSLT,
Case when TXTYPE='DL' Then 1 else 0 end as TransplantDLT,
Case when TXTYPE='H' Then 1 else 0 end as TransplantHT,
case when CCPeriod.SourceSpellNo in (

'150082579',
'150118810',
'150119575',
'150169207',
'150169892',
'150189458',
'150199225',
'150204123',
'150217673',
'150232045',
'150288186',
'150305885',
'150315650',
'150342401',
'150324280',
'150335131',
'150342407',
'150343130',
'150361421',
'150385499',
'150394035',
'150400193',
'150401854',
'150404513',
'150409385',
'150416332',
'150416829') then 1 ELSE 0 END AS TransplantVAD,
CASE when CCPeriod.SourceSpellNo in

(
'150328358'
,'150229905'
,'150241005'
,'150249911'
,'150250700'
,'150261339'
,'150261342'
,'150267033'
,'150267013'
,'150261324'
,'150267029'
,'150274659'
,'150274654'
,'150268697'
,'150268685'
,'150274660'
,'150280836'
,'150276860'
,'150276834'
,'150278828'
,'150278818'
,'150275212'
,'150280824'
,'150290995'
,'150286772'
,'150300813'
,'150297097'
,'150301121'
,'150304034'
,'150303659'
,'150304011'
,'150286786'
,'150303195'
,'150314305'
,'150314306'
,'150308256'
,'150308257'
,'150320173'
,'150318250'
,'150314010'
,'150318251'
,'150320207'
,'150322146'
,'150322319'
,'150234708'
,'150149651'
,'150157050'
,'150146036'
,'150157079'
,'150160028'
,'150166755'
,'150179480'
,'150179383'
,'150199166'
,'150229905'
)
Then 1 else 0 END as TAVI,


Case WHEN

(
PriamryProcedureCode >= 'K401' AND PriamryProcedureCode <= 'K469'
) 
OR
(
SecondaryProcedureCode1 >= 'K401' AND SecondaryProcedureCode1 <= 'K469'
) 
OR 
(
SecondaryProcedureCode2 >= 'K401' AND SecondaryProcedureCode2 <= 'K469'
) 
OR
(
SecondaryProcedureCode3 >= 'K401' AND SecondaryProcedureCode3 <= 'K469'
) 
OR
(
SecondaryProcedureCode4 >= 'K401' AND SecondaryProcedureCode4 <= 'K469'
) 
OR
(
SecondaryProcedureCode5 >= 'K401' AND SecondaryProcedureCode5 <= 'K469'
)
THEN 1 else 0 end as CABG,

Case WHEN

(
PriamryProcedureCode >= 'K241' AND PriamryProcedureCode <= 'K369'
) 
OR
(
SecondaryProcedureCode1 >= 'K241' AND SecondaryProcedureCode1 <= 'K369'
) 
OR 
(
SecondaryProcedureCode2 >= 'K241' AND SecondaryProcedureCode2 <= 'K369'
) 
OR
(
SecondaryProcedureCode3 >= 'K241' AND SecondaryProcedureCode3 <= 'K369'
) 
OR
(
SecondaryProcedureCode4 >= 'K241' AND SecondaryProcedureCode4 <= 'K369'
) 
OR
(
SecondaryProcedureCode5 >= 'K241' AND SecondaryProcedureCode5 <= 'K369'
)
THEN 1 else 0 end as Valve,

Case When PriamryProcedureCode='X581'
OR SecondaryProcedureCode1='X581'
OR SecondaryProcedureCode2='X581'
OR SecondaryProcedureCode3='X581'
OR SecondaryProcedureCode4='X581'
OR SecondaryProcedureCode5='X581'
OR SecondaryProcedureCode6='X581'
OR SecondaryProcedureCode7='X581'
OR SecondaryProcedureCode8='X581'

THEN 1 else 0 end as ECMO,
sum(Stay.CCPStay) as ActualBedDays


from 

Lorenzo.dbo.ExtractCriticalCarePeriodStay stay
inner join WHReporting.APC.CriticalCarePeriod CCPeriod on CCPeriod.SourceCCPNo=stay.SourceCCPNo
LEFT JOIN WHReporting.APC.Episode EPI
ON CCPeriod.SourceEncounterNo = EPI.EpisodeUniqueID
lEFT JOIN WHReporting.dbo.TXPatients Transplants ON Transplants.Spell2=CCPeriod.SourceSpellNo
left join WHREPORTING.LK.Calendar cal on cal.TheDate=cast(stay.StayDate as date)
LEFT OUTER JOIN WHReporting.APC.ClinicalCoding CC
ON EPI.EncounterRecno = CC.EncounterRecno

where SPONT_REFNO_CODE in ('CTCU', 'CT Transplant', 'ICA')

Group by

cal.TheMonth,
CCPeriod.SourceSpellNo
,SPONT_REFNO_CODE
,EPI.[SpecialtyCode(Function)]
,Case when TXTYPE='SL' Then 1 else 0 end,
Case when TXTYPE='DL' Then 1 else 0 end,
Case when TXTYPE='H' Then 1 else 0 end,
case when CCPeriod.SourceSpellNo in (

'150082579',
'150118810',
'150119575',
'150169207',
'150169892',
'150189458',
'150199225',
'150204123',
'150217673',
'150232045',
'150288186',
'150305885',
'150315650',
'150342401',
'150324280',
'150335131',
'150342407',
'150343130',
'150361421',
'150385499',
'150394035',
'150400193',
'150401854',
'150404513',
'150409385',
'150416332',
'150416829') then 1 ELSE 0 END ,
CASE when CCPeriod.SourceSpellNo in

(
'150328358'
,'150229905'
,'150241005'
,'150249911'
,'150250700'
,'150261339'
,'150261342'
,'150267033'
,'150267013'
,'150261324'
,'150267029'
,'150274659'
,'150274654'
,'150268697'
,'150268685'
,'150274660'
,'150280836'
,'150276860'
,'150276834'
,'150278828'
,'150278818'
,'150275212'
,'150280824'
,'150290995'
,'150286772'
,'150300813'
,'150297097'
,'150301121'
,'150304034'
,'150303659'
,'150304011'
,'150286786'
,'150303195'
,'150314305'
,'150314306'
,'150308256'
,'150308257'
,'150320173'
,'150318250'
,'150314010'
,'150318251'
,'150320207'
,'150322146'
,'150322319'
,'150234708'
,'150149651'
,'150157050'
,'150146036'
,'150157079'
,'150160028'
,'150166755'
,'150179480'
,'150179383'
,'150199166'
,'150229905'
)
Then 1 else 0 END ,


Case WHEN

(
PriamryProcedureCode >= 'K401' AND PriamryProcedureCode <= 'K469'
) 
OR
(
SecondaryProcedureCode1 >= 'K401' AND SecondaryProcedureCode1 <= 'K469'
) 
OR 
(
SecondaryProcedureCode2 >= 'K401' AND SecondaryProcedureCode2 <= 'K469'
) 
OR
(
SecondaryProcedureCode3 >= 'K401' AND SecondaryProcedureCode3 <= 'K469'
) 
OR
(
SecondaryProcedureCode4 >= 'K401' AND SecondaryProcedureCode4 <= 'K469'
) 
OR
(
SecondaryProcedureCode5 >= 'K401' AND SecondaryProcedureCode5 <= 'K469'
)
THEN 1 else 0 end ,

Case WHEN

(
PriamryProcedureCode >= 'K241' AND PriamryProcedureCode <= 'K369'
) 
OR
(
SecondaryProcedureCode1 >= 'K241' AND SecondaryProcedureCode1 <= 'K369'
) 
OR 
(
SecondaryProcedureCode2 >= 'K241' AND SecondaryProcedureCode2 <= 'K369'
) 
OR
(
SecondaryProcedureCode3 >= 'K241' AND SecondaryProcedureCode3 <= 'K369'
) 
OR
(
SecondaryProcedureCode4 >= 'K241' AND SecondaryProcedureCode4 <= 'K369'
) 
OR
(
SecondaryProcedureCode5 >= 'K241' AND SecondaryProcedureCode5 <= 'K369'
)
THEN 1 else 0 end ,

Case When PriamryProcedureCode='X581'
OR SecondaryProcedureCode1='X581'
OR SecondaryProcedureCode2='X581'
OR SecondaryProcedureCode3='X581'
OR SecondaryProcedureCode4='X581'
OR SecondaryProcedureCode5='X581'
OR SecondaryProcedureCode6='X581'
OR SecondaryProcedureCode7='X581'
OR SecondaryProcedureCode8='X581'

THEN 1 else 0 end,
CARE_LEVEL,
cal.FinancialMonthKey) as FDays

left join WHReporting.APC.vwCriticalCareActualDays ADays
on ADays.FinancialMonthKey=FDays.FinancialMonthKey
and ADays.SourceSpellNo=FDays.SourceSpellNo
and ADays.Ward=FDays.SPONT_REFNO_CODE
and isnull(ADays.CARE_LEVEL,4)=isnull(FDays.CARE_LEVEL,4)


where cast(FDays.FinancialMonthKey as int) in (@Month)
and FDays.SPONT_REFNO_CODE in (@Ward)
and FDays.TransplantSLT in (@TransplantSLT)
AND FDays.TransplantDLT IN (@TransplantDLT)
AND FDays.TransplantHT IN (@TransplantHT)
AND FDays.TransplantVAD IN (@TransplantVAD)
AND FDays.CABG IN (@CABG)
AND FDays.TAVI IN (@TAVI)
AND FDays.Valve IN (@Valve)
AND FDays.ECMO in (@ECMO)

group by

FDays.[SpecialtyCode(Function)],
FDays.FinancialMonthKey,
FDays.TheMonth,
FDays.SourceSpellNo
,FDays.SPONT_REFNO_CODE
,FDays.TransplantSLT
,FDays.TransplantDLT
,FDays.TransplantHT
,FDays.TransplantVAD
,FDays.CABG
,FDays.TAVI
,FDays.Valve
,FDays.ECMO