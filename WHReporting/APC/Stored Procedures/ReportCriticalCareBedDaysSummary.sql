﻿/*
--Author: J Cleworth
--Date created: 26/02/2013
--Stored Procedure Built for SRSS Report on:
--Bed Days Actual and Finance

*/


CREATE Procedure [APC].[ReportCriticalCareBedDaysSummary]


@Month AS int,
@Ward AS VARCHAR (25)


as

SELECT     
TheMonth
,FinancialMonthKey, 
[SpecialtyCode(Function)], 
SourceSpellNo, 
SPONT_REFNO_CODE, 
Case when Patienttype like 'Trans%' then 1
when PatientType like 'ECMO%' Then 2
WHEN PatientType like 'VAD%' then 3
When PatientType like 'TAVI%' then 4
when PatientType like 'CABG%' THEN 5
WHEN PatientType like 'Valve%' then 6
when PatientType = 'Other' then 8
else 7 end as PatientOrder
,
case when PatientType='Other' and OtherGroup<>'Other' then OtherGroup
else PatientType end as PatientType,
L2FinanceBedDays,
L3FinanceBedDays,
L2ActualBedDays,
L3ActualBedDays,
L0FinanceBedDays
from


(SELECT     
TheMonth
,FDays.FinancialMonthKey, 
FDays.[SpecialtyCode(Function)], 
FDays.SourceSpellNo, 
FDays.SPONT_REFNO_CODE, 

LEFT(
(case when FDays.TransplantSLT=1 then 'TransplantSLT+' else '' end +
case when FDays.TransplantDLT=1 then 'TransplantDLT+'else '' end +
case when FDays.TransplantHT=1 then 'TransplantHT+' else '' end +
case when FDays.TransplantVAD=1 then 'TransplantVAD+'else '' end +
case when FDays.CABG=1 then 'CABG+'else ''  end +
case when FDays.TAVI=1 then 'TAVI+'else '' end +
case when FDays.Valve=1 then 'Valve+'else '' end +
case when FDays.ECMO=1 then 'ECMO+'else '' end +
--case when FDays.PCICardiology=1 then 'Cardiology+'else '' end +
--case when FDays.Thoracic=1 then 'Thoracic+'else  '' end +

case when FDays.TransplantSLT=0 and FDays.TransplantDLT=0 and FDays.TransplantHT=0 and FDays.TransplantVAD=0 and FDays.CABG=0 
and FDays.TAVI=0 and FDays.Valve=0 and FDays.ECMO=0 --and FDays.PCICardiology=0 and FDays.Thoracic=0 
then 'Other' else '' end),
(len(case when FDays.TransplantSLT=1 then 'TransplantSLT+' else '' end +
case when FDays.TransplantDLT=1 then 'TransplantDLT+'else '' end +
case when FDays.TransplantHT=1 then 'TransplantHT+' else '' end +
case when FDays.TransplantVAD=1 then 'TransplantVAD+'else '' end +
case when FDays.CABG=1 then 'CABG+'else ''  end +
case when FDays.TAVI=1 then 'TAVI+'else '' end +
case when FDays.Valve=1 then 'Valve+'else '' end +
case when FDays.ECMO=1 then 'ECMO+'else '' end +
--case when FDays.PCICardiology=1 then 'Cardiology+'else '' end +
--case when FDays.Thoracic=1 then 'Thoracic+'else  '' end +

case when FDays.TransplantSLT=0 and FDays.TransplantDLT=0 and FDays.TransplantHT=0 and FDays.TransplantVAD=0 and FDays.CABG=0 
and FDays.TAVI=0 and FDays.Valve=0 and FDays.ECMO=0 --and FDays.PCICardiology=0 and FDays.Thoracic=0 
then 'Other+' else '' end))-1)



as PatientType, 
case when FDays.PCICardiology=1 then 'Cardiology'
When FDays.Thoracic=1 then 'Thoracic'

else 'Other' end 


AS OtherGroup,
SUM(CASE WHEN FDays.CARE_LEVEL = 2 THEN FDays.ActualBedDays ELSE 0 END) AS L2FinanceBedDays, 
SUM(CASE WHEN FDays.CARE_LEVEL = 3 THEN FDays.ActualBedDays ELSE 0 END) AS L3FinanceBedDays, 
sum(CASE WHEN FDays.CARE_LEVEL is null THEN FDays.ActualBedDays ELSE 0 END) AS L0FinanceBedDays,
ISNULL(SUM(CASE WHEN FDays.CARE_LEVEL = 2 THEN ADays.BedDays ELSE 0 END), 0) AS L2ActualBedDays, 
ISNULL(SUM(CASE WHEN FDays.CARE_LEVEL = 3 THEN ADays.BedDays ELSE 0 END), 0) AS L3ActualBedDays
FROM        (Select Distinct 
cal.FinancialMonthKey,
cal.TheMonth,
CCPeriod.SourceSpellNo
,SPONT_REFNO_CODE
,CARE_LEVEL
,EPI.[SpecialtyCode(Function)]
,case when [SpecialtyCode(Function)]=173 
/*and (ConsultantCode <> 'C2386780' 
AND PriamryProcedureCode not like 'K%')*/ then 1 else 0 end as Thoracic
,case when [SpecialtyCode(Function)]='320' Then 1 ELSE 0 END AS PCICardiology

,Case when TXTYPE='SL' Then 1 else 0 end as TransplantSLT,
Case when TXTYPE='DL' Then 1 else 0 end as TransplantDLT,
Case when TXTYPE='H' Then 1 else 0 end as TransplantHT,
case when CCPeriod.SourceSpellNo in (

'150082579',
'150118810',
'150119575',
'150169207',
'150169892',
'150189458',
'150199225',
'150204123',
'150217673',
'150232045',
'150288186',
'150305885',
'150315650',
'150342401',
'150324280',
'150335131',
'150342407',
'150343130',
'150361421',
'150385499',
'150394035',
'150400193',
'150401854',
'150404513',
'150409385',
'150416332',
'150416829') then 1 ELSE 0 END AS TransplantVAD,
CASE when CCPeriod.SourceSpellNo in

(
'150328358'
,'150229905'
,'150241005'
,'150249911'
,'150250700'
,'150261339'
,'150261342'
,'150267033'
,'150267013'
,'150261324'
,'150267029'
,'150274659'
,'150274654'
,'150268697'
,'150268685'
,'150274660'
,'150280836'
,'150276860'
,'150276834'
,'150278828'
,'150278818'
,'150275212'
,'150280824'
,'150290995'
,'150286772'
,'150300813'
,'150297097'
,'150301121'
,'150304034'
,'150303659'
,'150304011'
,'150286786'
,'150303195'
,'150314305'
,'150314306'
,'150308256'
,'150308257'
,'150320173'
,'150318250'
,'150314010'
,'150318251'
,'150320207'
,'150322146'
,'150322319'
,'150234708'
,'150149651'
,'150157050'
,'150146036'
,'150157079'
,'150160028'
,'150166755'
,'150179480'
,'150179383'
,'150199166'
,'150229905'
)
Then 1 else 0 END as TAVI,


Case WHEN ccperiod.SourceSpellNo in 


(select distinct sourcespellno

from whreporting.APC.Episode episode
inner join whreporting.APC.ClinicalCoding coding
on coding.EncounterRecno=episode.EncounterRecno


where  (
PriamryProcedureCode >= 'K401' AND PriamryProcedureCode <= 'K469') 
OR (SecondaryProcedureCode1 >= 'K401' AND SecondaryProcedureCode1 <= 'K469') 
OR (SecondaryProcedureCode2 >= 'K401' AND SecondaryProcedureCode2 <= 'K469') 
OR(SecondaryProcedureCode3 >= 'K401' AND SecondaryProcedureCode3 <= 'K469') 
OR(SecondaryProcedureCode4 >= 'K401' AND SecondaryProcedureCode4 <= 'K469') 
OR(SecondaryProcedureCode5 >= 'K401' AND SecondaryProcedureCode5 <= 'K469'))
THEN 1 else 0 end as CABG,

Case WHEN ccperiod.SourceSpellNo in 


(select distinct sourcespellno

from whreporting.APC.Episode episode
inner join whreporting.APC.ClinicalCoding coding
on coding.EncounterRecno=episode.EncounterRecno


where  (PriamryProcedureCode >= 'K241' AND PriamryProcedureCode <= 'K369') OR
(SecondaryProcedureCode1 >= 'K241' AND SecondaryProcedureCode1 <= 'K369') 
OR (SecondaryProcedureCode2 >= 'K241' AND SecondaryProcedureCode2 <= 'K369') 
OR(SecondaryProcedureCode3 >= 'K241' AND SecondaryProcedureCode3 <= 'K369') 
OR(SecondaryProcedureCode4 >= 'K241' AND SecondaryProcedureCode4 <= 'K369') 
OR(SecondaryProcedureCode5 >= 'K241' AND SecondaryProcedureCode5 <= 'K369'
))
THEN 1 else 0 end as Valve,

Case When CCPeriod.SourceSpellNo in 


 (select sourcespellno

from whreporting.APC.Episode episode
inner join whreporting.APC.ClinicalCoding coding
on coding.EncounterRecno=episode.EncounterRecno
where  (PriamryProcedureCode='X581'
OR SecondaryProcedureCode1='X581'
OR SecondaryProcedureCode2='X581'
OR SecondaryProcedureCode3='X581'
OR SecondaryProcedureCode4='X581'
OR SecondaryProcedureCode5='X581'
OR SecondaryProcedureCode6='X581'
OR SecondaryProcedureCode7='X581'
OR SecondaryProcedureCode8='X581'
OR SecondaryProcedureCode9='X581'
OR SecondaryProcedureCode10='X581'
OR SecondaryProcedureCode11='X581'))

THEN 1 else 0 end as ECMO,
sum(Stay.CCPStay) as ActualBedDays


from 

Lorenzo.dbo.ExtractCriticalCarePeriodStay stay
inner join WHReporting.APC.CriticalCarePeriod CCPeriod on CCPeriod.SourceCCPNo=stay.SourceCCPNo
LEFT JOIN WHReporting.APC.Episode EPI
ON CCPeriod.SourceEncounterNo = EPI.EpisodeUniqueID
lEFT JOIN (select distinct txtype,spell2 from WHReporting.dbo.TXPatients Transplants) as Trans ON Trans.Spell2=CCPeriod.SourceSpellNo



left join WHREPORTING.LK.Calendar cal on cal.TheDate=cast(stay.StayDate as date)
LEFT OUTER JOIN WHReporting.APC.ClinicalCoding CC
ON EPI.EncounterRecno = CC.EncounterRecno

where SPONT_REFNO_CODE in ('CTCU', 'CT Transplant', 'ICA')

Group by

cal.TheMonth,
CCPeriod.SourceSpellNo
,SPONT_REFNO_CODE
,EPI.[SpecialtyCode(Function)]
,case when [SpecialtyCode(Function)]=173 and (ConsultantCode <> 'C2386780' 
AND PriamryProcedureCode not like 'K%') then 1 else 0 end
,Case when TXTYPE='SL' Then 1 else 0 end,
Case when TXTYPE='DL' Then 1 else 0 end,
Case when TXTYPE='H' Then 1 else 0 end,
case when CCPeriod.SourceSpellNo in (

'150082579',
'150118810',
'150119575',
'150169207',
'150169892',
'150189458',
'150199225',
'150204123',
'150217673',
'150232045',
'150288186',
'150305885',
'150315650',
'150342401',
'150324280',
'150335131',
'150342407',
'150343130',
'150361421',
'150385499',
'150394035',
'150400193',
'150401854',
'150404513',
'150409385',
'150416332',
'150416829') then 1 ELSE 0 END ,
CASE when CCPeriod.SourceSpellNo in

(
'150328358'
,'150229905'
,'150241005'
,'150249911'
,'150250700'
,'150261339'
,'150261342'
,'150267033'
,'150267013'
,'150261324'
,'150267029'
,'150274659'
,'150274654'
,'150268697'
,'150268685'
,'150274660'
,'150280836'
,'150276860'
,'150276834'
,'150278828'
,'150278818'
,'150275212'
,'150280824'
,'150290995'
,'150286772'
,'150300813'
,'150297097'
,'150301121'
,'150304034'
,'150303659'
,'150304011'
,'150286786'
,'150303195'
,'150314305'
,'150314306'
,'150308256'
,'150308257'
,'150320173'
,'150318250'
,'150314010'
,'150318251'
,'150320207'
,'150322146'
,'150322319'
,'150234708'
,'150149651'
,'150157050'
,'150146036'
,'150157079'
,'150160028'
,'150166755'
,'150179480'
,'150179383'
,'150199166'
,'150229905'
)
Then 1 else 0 END ,


/*Case WHEN ccperiod.SourceSpellNo in 


(select distinct sourcespellno

from whreporting.APC.Episode episode
inner join whreporting.APC.ClinicalCoding coding
on coding.EncounterRecno=episode.EncounterRecno


where  (
PriamryProcedureCode >= 'K401' AND PriamryProcedureCode <= 'K469') 
OR (SecondaryProcedureCode1 >= 'K401' AND SecondaryProcedureCode1 <= 'K469') 
OR (SecondaryProcedureCode2 >= 'K401' AND SecondaryProcedureCode2 <= 'K469') 
OR(SecondaryProcedureCode3 >= 'K401' AND SecondaryProcedureCode3 <= 'K469') 
OR(SecondaryProcedureCode4 >= 'K401' AND SecondaryProcedureCode4 <= 'K469') 
OR(SecondaryProcedureCode5 >= 'K401' AND SecondaryProcedureCode5 <= 'K469'))
THEN 1 else 0 end,

Case WHEN ccperiod.SourceSpellNo in 


(select distinct sourcespellno

from whreporting.APC.Episode episode
inner join whreporting.APC.ClinicalCoding coding
on coding.EncounterRecno=episode.EncounterRecno


where  (PriamryProcedureCode >= 'K241' AND PriamryProcedureCode <= 'K369') OR
(SecondaryProcedureCode1 >= 'K241' AND SecondaryProcedureCode1 <= 'K369') 
OR (SecondaryProcedureCode2 >= 'K241' AND SecondaryProcedureCode2 <= 'K369') 
OR(SecondaryProcedureCode3 >= 'K241' AND SecondaryProcedureCode3 <= 'K369') 
OR(SecondaryProcedureCode4 >= 'K241' AND SecondaryProcedureCode4 <= 'K369') 
OR(SecondaryProcedureCode5 >= 'K241' AND SecondaryProcedureCode5 <= 'K369'
))
THEN 1 else 0 end,

Case When CCPeriod.SourceSpellNo in 


 (select sourcespellno

from whreporting.APC.Episode episode
inner join whreporting.APC.ClinicalCoding coding
on coding.EncounterRecno=episode.EncounterRecno
where  (PriamryProcedureCode='X581'
OR SecondaryProcedureCode1='X581'
OR SecondaryProcedureCode2='X581'
OR SecondaryProcedureCode3='X581'
OR SecondaryProcedureCode4='X581'
OR SecondaryProcedureCode5='X581'
OR SecondaryProcedureCode6='X581'
OR SecondaryProcedureCode7='X581'
OR SecondaryProcedureCode8='X581'
OR SecondaryProcedureCode9='X581'
OR SecondaryProcedureCode10='X581'
OR SecondaryProcedureCode11='X581'))

THEN 1 else 0 end,*/
CARE_LEVEL,
cal.FinancialMonthKey) as FDays

left join WHReporting.APC.vwCriticalCareActualDays ADays
on ADays.FinancialMonthKey=FDays.FinancialMonthKey
and ADays.SourceSpellNo=FDays.SourceSpellNo
and ADays.Ward=FDays.SPONT_REFNO_CODE
and isnull(ADays.CARE_LEVEL,4)=isnull(FDays.CARE_LEVEL,4)

WHERE     (CAST(FDays.FinancialMonthKey AS int) IN (@Month)) AND (FDays.SPONT_REFNO_CODE IN (@Ward)) 
GROUP BY 
TheMonth
,FDays.FinancialMonthKey, 
FDays.[SpecialtyCode(Function)], 
FDays.SourceSpellNo, 
FDays.SPONT_REFNO_CODE, 
LEFT(
(case when FDays.TransplantSLT=1 then 'TransplantSLT+' else '' end +
case when FDays.TransplantDLT=1 then 'TransplantDLT+'else '' end +
case when FDays.TransplantHT=1 then 'TransplantHT+' else '' end +
case when FDays.TransplantVAD=1 then 'TransplantVAD+'else '' end +
case when FDays.CABG=1 then 'CABG+'else ''  end +
case when FDays.TAVI=1 then 'TAVI+'else '' end +
case when FDays.Valve=1 then 'Valve+'else '' end +
case when FDays.ECMO=1 then 'ECMO+'else '' end +
--case when FDays.PCICardiology=1 then 'Cardiology+'else '' end +
--case when FDays.Thoracic=1 then 'Thoracic+'else  '' end +

case when FDays.TransplantSLT=0 and FDays.TransplantDLT=0 and FDays.TransplantHT=0 and FDays.TransplantVAD=0 and FDays.CABG=0 
and FDays.TAVI=0 and FDays.Valve=0 and FDays.ECMO=0 --and FDays.PCICardiology=0 and FDays.Thoracic=0 
then 'Other' else '' end),
(len(case when FDays.TransplantSLT=1 then 'TransplantSLT+' else '' end +
case when FDays.TransplantDLT=1 then 'TransplantDLT+'else '' end +
case when FDays.TransplantHT=1 then 'TransplantHT+' else '' end +
case when FDays.TransplantVAD=1 then 'TransplantVAD+'else '' end +
case when FDays.CABG=1 then 'CABG+'else ''  end +
case when FDays.TAVI=1 then 'TAVI+'else '' end +
case when FDays.Valve=1 then 'Valve+'else '' end +
case when FDays.ECMO=1 then 'ECMO+'else '' end +
--case when FDays.PCICardiology=1 then 'Cardiology+'else '' end +
--case when FDays.Thoracic=1 then 'Thoracic+'else  '' end +

case when FDays.TransplantSLT=0 and FDays.TransplantDLT=0 and FDays.TransplantHT=0 and FDays.TransplantVAD=0 and FDays.CABG=0 
and FDays.TAVI=0 and FDays.Valve=0 and FDays.ECMO=0 --and FDays.PCICardiology=0 and FDays.Thoracic=0 
then 'Other+' else '' end))-1)
,case when FDays.PCICardiology=1 then 'Cardiology'
When FDays.Thoracic=1 then 'Thoracic'
else 'Other' end 
) as GroupedPatients