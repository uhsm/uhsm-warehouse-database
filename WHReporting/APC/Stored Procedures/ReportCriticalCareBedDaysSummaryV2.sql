﻿/*
--Author: A kilcourse
--Date created: 10/04/2013
--Stored Procedure Built for SSRS Report on:
--Bed Days Actual and Finance

*/


CREATE Procedure [APC].[ReportCriticalCareBedDaysSummaryV2]


@Month AS int,
@Ward AS VARCHAR (25),
@ProcedureCategory AS VARCHAR (255)

AS

SELECT     
TheMonth,
FinancialMonthKey, 
[SpecialtyCode(Function)], 
SourceSpellNo, 
SPONT_REFNO_CODE,
Case when Patienttype like 'Trans%' then 1
WHEN PatientType like '%VAD%' then 3
When PatientType like 'TAVI%' then 4
when PatientType like 'CABG%' THEN 5
WHEN PatientType in ('AVR','MVR','Major Aortic Surgery','Double Valve','Other_Cardiac') then 6
when PatientType in ('Pneumonectomy','Bilobectomy','Lobectomy','Decortication','Chest Wall Resection','Other_Thoracic','ECMO') then 8
else 7 end as PatientOrder,
OPCS4_Category as ProcedureCategory,
PatientType,
L2FinanceBedDays,
case when(L3FinanceBedDays-ECMODays)<0 then 0 else (L3FinanceBedDays-ECMODays) end as L3FinanceBedDays,
L2ActualBedDays,
case when (L3ActualBedDays-ECMODays)<0 then 0 else (L3ActualBedDays-ECMODays)end as L3ActualBedDays,
L0FinanceBedDays,
L0ActualDays,
ECMODays

from

(SELECT     
TheMonth
,FDays.FinancialMonthKey, 
FDays.[SpecialtyCode(Function)], 
FDays.SourceSpellNo, 
FDays.SPONT_REFNO_CODE,
isnull(EDays.ECMODays,0) as ECMODays,

LEFT(
(case when FDays.Pneumonectomy=1 then 'Pneumonectomy+' else '' end +
case when FDays.Bilobectomy=1 then 'Bilobectomy+'else '' end +
case when FDays.Lobectomy=1 then 'Lobectomy+' else '' end +
case when FDays.Decortication=1 then 'Decortication+'else '' end +
case when FDays.[Chest Wall Resection]=1 then 'Chest Wall Resection+'else ''  end +
case when FDays.Other_Thoracic=1 then 'Thoracic Other+'else '' end +
case when FDays.[Acute Heart Failure]=1 then 'Acute Heart Failure+'else '' end +
case when FDays.PCICardiology=1 then 'PCICardiology+'else '' end +
case when FDays.Other_Cardiology=1 then 'Cardiology Other+' else '' end +
case when FDays.TransplantSLT=1 then 'TransplantSLT+'else '' end +
case when FDays.TransplantDLT=1 then 'TransplantDLT+'else '' end +
case when FDays.TransplantHT=1 then 'TransplantHT+'else '' end +
case when FDays.TransplantVAD=1 then 'TransplantVAD+'else '' end +
case when FDays.TAVI=1 then 'TAVI+'else '' end +
case when FDays.AVR=1 then 'AVR+'else '' end +
case when FDays.MVR=1 then 'MVR+'else '' end +
case when FDays.CABG=1 then 'CABG+'else '' end +
case when FDays.[Major Aortic Surgery]=1 then 'Major Aortic Surgery+'else '' end +
case when FDays.[Double Valve]=1 then 'Double Valve+'else '' end +
case when FDays.[Other_Cardiac]=1 then 'Cardiac Other+'else '' end +
case when FDays.[ECMO]=1 then 'ECMO+' else '' end +

case when FDays.Pneumonectomy=0 and FDays.Bilobectomy=0 and FDays.Lobectomy=0 and FDays.Decortication=0 and 
FDays.[Chest Wall Resection]=0 and FDays.Other_Thoracic=0 and FDays.[Acute Heart Failure]=0 and FDays.PCICardiology=0 
and FDays.Other_Cardiology=0 and FDays.TransplantSLT=0 and FDays.TransplantDLT=0 and FDays.TransplantHT=0 and FDays.TransplantVAD=0 and FDays.TAVI=0 and 
FDays.AVR=0 and FDays.MVR=0 and FDays.CABG=0 and FDays.[Major Aortic Surgery]=0 and FDays.[Double Valve]=0 and FDays.Other_Cardiac=0 and FDays.[ECMO]=0 
then 'Other+' else '' end),

(len(case when FDays.Pneumonectomy=1 then 'Pneumonectomy+' else '' end +
case when FDays.Bilobectomy=1 then 'Bilobectomy+'else '' end +
case when FDays.Lobectomy=1 then 'Lobectomy+' else '' end +
case when FDays.Decortication=1 then 'Decortication+'else '' end +
case when FDays.[Chest Wall Resection]=1 then 'Chest Wall Resection+'else ''  end +
case when FDays.Other_Thoracic=1 then 'Thoracic Other+'else '' end +
case when FDays.[Acute Heart Failure]=1 then 'Acute Heart Failure+'else '' end +
case when FDays.PCICardiology=1 then 'PCICardiology+'else '' end +
case when FDays.Other_Cardiology=1 then 'Cardiology Other+' else '' end +
case when FDays.TransplantSLT=1 then 'TransplantSLT+'else '' end +
case when FDays.TransplantDLT=1 then 'TransplantDLT+'else '' end +
case when FDays.TransplantHT=1 then 'TransplantHT+'else '' end +
case when FDays.TransplantVAD=1 then 'TransplantVAD+'else '' end +
case when FDays.TAVI=1 then 'TAVI+'else '' end +
case when FDays.AVR=1 then 'AVR+'else '' end +
case when FDays.MVR=1 then 'MVR+'else '' end +
case when FDays.CABG=1 then 'CABG+'else '' end +
case when FDays.[Major Aortic Surgery]=1 then 'Major Aortic Surgery+'else '' end +
case when FDays.[Double Valve]=1 then 'Double Valve+'else '' end +
case when FDays.[Other_Cardiac]=1 then 'Cardiac Other+'else '' end +
case when FDays.[ECMO]=1 then 'ECMO+' else '' end +

case when FDays.Pneumonectomy=0 and FDays.Bilobectomy=0 and FDays.Lobectomy=0 and FDays.Decortication=0 and 
FDays.[Chest Wall Resection]=0 and FDays.Other_Thoracic=0 and FDays.[Acute Heart Failure]=0 and FDays.PCICardiology=0 
and FDays.Other_Cardiology=0 and FDays.TransplantSLT=0 and FDays.TransplantDLT=0 and FDays.TransplantHT=0 and FDays.TransplantVAD=0 and FDays.TAVI=0 and 
FDays.AVR=0 and FDays.MVR=0 and FDays.CABG=0 and FDays.[Major Aortic Surgery]=0 and FDays.[Double Valve]=0 and FDays.Other_Cardiac=0 and FDays.[ECMO]=0 
then 'Other+' else '' end))-1)

as PatientType, 

SUM(CASE WHEN FDays.CARE_LEVEL = 2 THEN FDays.ActualBedDays ELSE 0 END) AS L2FinanceBedDays, 
SUM(CASE WHEN FDays.CARE_LEVEL = 3 THEN FDays.ActualBedDays ELSE 0 END) AS L3FinanceBedDays, 
sum(CASE WHEN FDays.CARE_LEVEL is null THEN FDays.ActualBedDays ELSE 0 END) AS L0FinanceBedDays,
ISNULL(SUM(CASE WHEN FDays.CARE_LEVEL = 2 THEN ADays.BedDays ELSE 0 END), 0) AS L2ActualBedDays, 
ISNULL(SUM(CASE WHEN FDays.CARE_LEVEL = 3 THEN ADays.BedDays ELSE 0 END), 0) AS L3ActualBedDays,
isnull(sum(CASE WHEN FDays.CARE_LEVEL is null THEN ADays.BedDays ELSE 0 END),0) AS L0ActualDays
FROM        APC.vwCriticalCareFact as FDays

left join WHReporting.APC.vwCriticalCareActualDays ADays
on ADays.FinancialMonthKey=FDays.FinancialMonthKey
AND coalesce(ADays.[SpecialtyCode(Function)],'-1')=coalesce(FDays.[SpecialtyCode(Function)],'-1')
and ADays.SourceSpellNo=FDays.SourceSpellNo
and ADays.Ward=FDays.SPONT_REFNO_CODE
and isnull(ADays.CARE_LEVEL,4)=isnull(FDays.CARE_LEVEL,4)

left join 
(select FinancialMonthKey,Ward,SpellNo,LevelOfCare,sum(ECMODays) as ECMODays from WHREPORTING.APC.vwECMODays
Group by FinancialMonthKey,Ward,SpellNo,LevelOfCare) as EDays
on EDays.FinancialMonthKey=ADays.FinancialMonthKey
and EDays.Ward=ADays.Ward
and EDays.SpellNo=ADays.SourceSpellNo
and EDays.LevelOfCare=ADays.CARE_LEVEL

WHERE     (CAST(FDays.FinancialMonthKey AS int) IN (@Month)) AND (FDays.SPONT_REFNO_CODE IN (@Ward)) 

GROUP BY 
TheMonth,
FDays.FinancialMonthKey, 
isnull(EDays.ECMODays,0),
FDays.[SpecialtyCode(Function)], 
FDays.SourceSpellNo, 
FDays.SPONT_REFNO_CODE,

LEFT(
(case when FDays.Pneumonectomy=1 then 'Pneumonectomy+' else '' end +
case when FDays.Bilobectomy=1 then 'Bilobectomy+'else '' end +
case when FDays.Lobectomy=1 then 'Lobectomy+' else '' end +
case when FDays.Decortication=1 then 'Decortication+'else '' end +
case when FDays.[Chest Wall Resection]=1 then 'Chest Wall Resection+'else ''  end +
case when FDays.Other_Thoracic=1 then 'Thoracic Other+'else '' end +
case when FDays.[Acute Heart Failure]=1 then 'Acute Heart Failure+'else '' end +
case when FDays.PCICardiology=1 then 'PCICardiology+'else '' end +
case when FDays.Other_Cardiology=1 then 'Cardiology Other+' else '' end +
case when FDays.TransplantSLT=1 then 'TransplantSLT+'else '' end +
case when FDays.TransplantDLT=1 then 'TransplantDLT+'else '' end +
case when FDays.TransplantHT=1 then 'TransplantHT+'else '' end +
case when FDays.TransplantVAD=1 then 'TransplantVAD+'else '' end +
case when FDays.TAVI=1 then 'TAVI+'else '' end +
case when FDays.AVR=1 then 'AVR+'else '' end +
case when FDays.MVR=1 then 'MVR+'else '' end +
case when FDays.CABG=1 then 'CABG+'else '' end +
case when FDays.[Major Aortic Surgery]=1 then 'Major Aortic Surgery+'else '' end +
case when FDays.[Double Valve]=1 then 'Double Valve+'else '' end +
case when FDays.[Other_Cardiac]=1 then 'Cardiac Other+'else '' end +
case when FDays.[ECMO]=1 then 'ECMO+' else '' end +

case when FDays.Pneumonectomy=0 and FDays.Bilobectomy=0 and FDays.Lobectomy=0 and FDays.Decortication=0 and 
FDays.[Chest Wall Resection]=0 and FDays.Other_Thoracic=0 and FDays.[Acute Heart Failure]=0 and FDays.PCICardiology=0 
and FDays.Other_Cardiology=0 and FDays.TransplantSLT=0 and FDays.TransplantDLT=0 and FDays.TransplantHT=0 and FDays.TransplantVAD=0 and FDays.TAVI=0 and 
FDays.AVR=0 and FDays.MVR=0 and FDays.CABG=0 and FDays.[Major Aortic Surgery]=0 and FDays.[Double Valve]=0 and FDays.Other_Cardiac=0 and FDays.[ECMO]=0 
then 'Other+' else '' end),

(len(case when FDays.Pneumonectomy=1 then 'Pneumonectomy+' else '' end +
case when FDays.Bilobectomy=1 then 'Bilobectomy+'else '' end +
case when FDays.Lobectomy=1 then 'Lobectomy+' else '' end +
case when FDays.Decortication=1 then 'Decortication+'else '' end +
case when FDays.[Chest Wall Resection]=1 then 'Chest Wall Resection+'else ''  end +
case when FDays.Other_Thoracic=1 then 'Thoracic Other+'else '' end +
case when FDays.[Acute Heart Failure]=1 then 'Acute Heart Failure+'else '' end +
case when FDays.PCICardiology=1 then 'PCICardiology+'else '' end +
case when FDays.Other_Cardiology=1 then 'Cardiology Other+' else '' end +
case when FDays.TransplantSLT=1 then 'TransplantSLT+'else '' end +
case when FDays.TransplantDLT=1 then 'TransplantDLT+'else '' end +
case when FDays.TransplantHT=1 then 'TransplantHT+'else '' end +
case when FDays.TransplantVAD=1 then 'TransplantVAD+'else '' end +
case when FDays.TAVI=1 then 'TAVI+'else '' end +
case when FDays.AVR=1 then 'AVR+'else '' end +
case when FDays.MVR=1 then 'MVR+'else '' end +
case when FDays.CABG=1 then 'CABG+'else '' end +
case when FDays.[Major Aortic Surgery]=1 then 'Major Aortic Surgery+'else '' end +
case when FDays.[Double Valve]=1 then 'Double Valve+'else '' end +
case when FDays.[Other_Cardiac]=1 then 'Cardiac Other+'else '' end +
case when FDays.[ECMO]=1 then 'ECMO+' else '' end +

case when FDays.Pneumonectomy=0 and FDays.Bilobectomy=0 and FDays.Lobectomy=0 and FDays.Decortication=0 and 
FDays.[Chest Wall Resection]=0 and FDays.Other_Thoracic=0 and FDays.[Acute Heart Failure]=0 and FDays.PCICardiology=0 
and FDays.Other_Cardiology=0 and FDays.TransplantSLT=0 and FDays.TransplantDLT=0 and FDays.TransplantHT=0 and FDays.TransplantVAD=0 and FDays.TAVI=0 and 
FDays.AVR=0 and FDays.MVR=0 and FDays.CABG=0 and FDays.[Major Aortic Surgery]=0 and FDays.[Double Valve]=0 and FDays.Other_Cardiac=0 and FDays.[ECMO]=0 
then 'Other+' else '' end))-1)
) as GroupedPatients
left join dbo.CC_ProcedureGroups on Patienttype = OPCS4_Description

where OPCS4_Category IN (@ProcedureCategory)