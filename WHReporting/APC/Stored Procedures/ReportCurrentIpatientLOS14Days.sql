﻿/*
==========================================================================================================================    
Purpose :    [APC].[ReportCurrentIpatientLOS14Days]  -  data for Current Inpatient 14 days LOS SSRS Report     

Notes :      Stored in WHREPORTING.APC
             This Procedure has been created for use in the Current Inpatient 14 Days LOS SSRS report in the location below
             S:\CO-HI-Information\New File Management - Jan 2011\Data Warehouse Project\SSRS Report Code\Admissions_Discharges\Admissions\Admissions
           
             This has been designed to populate the data for Current Inpatients with a lenth of spell greater than 14 Days .
           
Author  :     Peter Asemota
Create Date :  04/05/2016 
Version :     1.0.0.0          
========================================================================================================================
*/
CREATE PROCEDURE [APC].[ReportCurrentIpatientLOS14Days] 
      (
	 @SnapshotDate DATETIME
	,@Division VARCHAR(MAX)
	,@Directorate VARCHAR(MAX)
	,@PatientClass VARCHAR (MAX)
	  )
	  
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET DATEFORMAT DMY




/****************Test purposes only **********************
  
SET DATEFORMAT DMY

Declare  @SnapshotDate DATETIME = DATEADD(day, DATEDIFF(day, 0, GETDATE()) - 1, 0)
        ,@Division as Varchar (max) = 'Scheduled Care,Clinical Support,Unscheduled Care'
       ,@Directorate As Varchar(max) = 'Accident & Emergency,Anaesthetics Critical Care & Pain,Cardiothoracic,Complex Health & Social Care,Medical Specialities,Respiratory Medicine,Specialist Surgery,Surgery,Unknown,Urgent Care,Women & Childrens'
       ,@PatientClass As Varchar(max)= 'BABY,ELECTIVE INPATIENT,NON ELECTIVE'
  
   ****************/
     
	
SELECT DATEADD(day, 1, C.TheDate) AS TheDate
	,S.AdmissionDate
	,S.DischargeDate
	,S.ProvisionalEndIndicator
	,W.WardCode
	,S.PatientClass	
	,CASE 
            WHEN DATEDIFF(DAY, S.AdmissionDate, C.TheDate) + 1 >= 100 THEN 'LOS >= 100'
            WHEN DATEDIFF(DAY, S.AdmissionDate, C.TheDate) + 1 >= 50  THEN 'LOS 50-99'
            WHEN DATEDIFF(DAY, S.AdmissionDate, C.TheDate) + 1 >= 28  THEN 'LOS 28-49'
            WHEN DATEDIFF(DAY, S.AdmissionDate, C.TheDate) + 1 >= 14  THEN 'LOS 14-27'
            WHEN DATEDIFF(DAY, S.AdmissionDate, C.TheDate) + 1 >= 7   THEN 'LOS 7-13'
            WHEN DATEDIFF(DAY, S.AdmissionDate, C.TheDate) + 1 >= 0   THEN 'LOS 0-6'
            ELSE 'other'
            END AS LOSAtStartOfDay    
      ,Case 
          WHEN  DATEDIFF(DAY, S.AdmissionDate, C.TheDate) + 1 >= 100   THEN 6
            WHEN  DATEDIFF(DAY, S.AdmissionDate, C.TheDate) + 1 >= 50   THEN 5
            WHEN  DATEDIFF(DAY, S.AdmissionDate, C.TheDate) + 1 >= 28    THEN 4
            WHEN  DATEDIFF(DAY, S.AdmissionDate, C.TheDate) + 1 >= 14   THEN 3
            WHEN  DATEDIFF(DAY, S.AdmissionDate, C.TheDate) + 1 >= 7     THEN 2
            WHEN  DATEDIFF(DAY, S.AdmissionDate, C.TheDate) + 1 >= 0  THEN 1
            ELSE 7
            END AS LOSSort    
	
	,DATEDIFF(DAY, S.AdmissionDate, C.TheDate) + 1 AS CurrentLOS
	,S.AdmissionSpecialty
	,S.[AdmissionSpecialty(Function)]
	,S.[AdmissionSpecialty(Main)]
	,E.Specialty AS CurrentSpecialty
	,E.[Specialty(Function)] AS [CurrentSpecialty(Function)]
	,E.[Specialty(Main)] AS [CurrentSpecialty(Main)]
	,E.Division AS CurrentDivision
	,E.Directorate AS CurrentDirectorate
	,E.ConsultantName AS CurrentConsultantName
	,E.FacilityID AS [PatientNumber]
	,S.Age AS AgeAtAdmission
	,P.DateOfBirth
	,P.PatientForename
	,P.PatientSurname
	,E.SourceSpellNo
	,E.EncounterRecno
	,S.SpellCCGCode
FROM WHREPORTING.APC.Spell S
INNER JOIN WHREPORTING.APC.Episode E ON E.SourceSpellNo = S.SourceSpellNo
INNER JOIN WHREPORTING.LK.Calendar C ON E.EpisodeStartDate <= C.TheDate
	AND (
		E.EpisodeEndDate > C.TheDate
		OR E.EpisodeEndDate IS NULL
		)
	AND (
		S.DischargeDate > C.TheDate
		OR S.DischargeDate IS NULL
		)
LEFT OUTER JOIN WHREPORTING.APC.WardStay W ON S.SourceSpellNo = W.SourceSpellNo
	AND W.StartTime < DATEADD(day, 1, C.TheDate)
	AND (
		W.EndTime >= DATEADD(day, 1, C.TheDate)
		OR W.EndTime IS NULL
		)
LEFT OUTER JOIN WHREPORTING.APC.Patient AS P ON E.EncounterRecno = P.EncounterRecno
INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Division, ',') div ON E.Division = div.SplitValue
INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Directorate, ',') dir ON E.Directorate = dir.SplitValue
INNER JOIN DW_REPORTING.LIB.fn_SplitString(@PatientClass, ',') pc ON S.PatientClass= pc.SplitValue
Where C.TheDate = @SnapshotDate
      AND C.TheDate <= DATEADD(day, DATEDIFF(day, 0, GETDATE()) - 1, 0)
      AND DATEDIFF(DAY, S.AdmissionDate, C.TheDate) + 1 >= 14
	--AND C.TheDate <= DATEADD(day, DATEDIFF(day, 0, GETDATE()) - 1, 0)
	AND W.WardCode NOT IN ( -- excluding private and children wards 
		 'SCB'
		,'ASPIRE'
		,'ICA'
		,'BIC'
		,'CCA'
		,'CTCU'
		)
	AND W.WardCode IS NOT NULL
	AND P.PatientSurname <> 'Xxtest' -- excluding test patients


END