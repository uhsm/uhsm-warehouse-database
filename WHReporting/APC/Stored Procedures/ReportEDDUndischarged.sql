﻿CREATE procedure 
[APC].[ReportEDDUndischarged]
AS
select 
wardcode,
cast((case when left(SmartboardEDD,1) like '%[1-9]%' 
then right(smartboardedd,4)+'-'+SUBSTRING(smartboardedd,4,2)+'-'+LEFT(smartboardedd,2) 
else null end) as DATE) as EDDDate,
/*cast((case when left(SmartboardEDD,1) like '%[1-9]%' 
then right(smartboardedd,4)+'-'+SUBSTRING(smartboardedd,4,2)+'-'+LEFT(smartboardedd,2) 
else 'Unknown' end) as varchar) as EDDDateIncUnk,*/


COUNT(distinct stay.SourceSpellNo) as Patients
 
from 
APC.WardStay stay 
inner join APC.Spell spell on spell.SourceSpellNo=stay.SourceSpellNo
/*and cast((case when left(SmartboardEDD,1) like '%[1-9]%' 
then right(smartboardedd,4)+'-'+SUBSTRING(smartboardedd,4,2)+'-'+LEFT(smartboardedd,2) 
else null end) as DATE) between StartTime and coalesce(endtime,cast(getdate() as date))*/

where spell.DischargeDate is null

and cast((case when left(SmartboardEDD,1) like '%[1-9]%' 
then right(smartboardedd,4)+'-'+SUBSTRING(smartboardedd,4,2)+'-'+LEFT(smartboardedd,2) 
else null end) as DATE) between dateadd(day,-1,cast(GETDATE() as DATE)) and dateadd(day,1,cast(GETDATE() as DATE))


and WardCode not in 
('ADMLOU','ALE','ASPIRE','ATR','AUR','BC','BIC','BRO','BS','CLR','DEN','DS','DU','GIU','HOM','KDU','LIT','NIGHTINGALE','NIHR','PEA','POU','RAD','REC','SCB','TCU','TCW','TDC','UAU','UAUWYT','WHS','ALEX PCT','ALEXHDU','BWH','CC2','PIUF2','REGENCY','SPIREW','TRAFF',
'NGALE')--CM 01/06/2016 added this code - new code for nightingale ward

group by
wardcode,
cast((case when left(SmartboardEDD,1) like '%[1-9]%' 
then right(smartboardedd,4)+'-'+SUBSTRING(smartboardedd,4,2)+'-'+LEFT(smartboardedd,2) 
else null end) as DATE)/*,
cast((case when left(SmartboardEDD,1) like '%[1-9]%' 
then right(smartboardedd,4)+'-'+SUBSTRING(smartboardedd,4,2)+'-'+LEFT(smartboardedd,2) 
else 'Unknown' end) as varchar)*/