﻿CREATE Procedure [APC].[ReportElectiveAdmissionMRSAScreensPatientLevel] @start DATETIME 
	,@end DATETIME
	,@ward varchar(150) 
	,@Adm int
	,@MRSAExcl int
	,@MRSAReq int
	,@MRSAComp int
	,@Abnormal int 
	


AS 

--DECLARE @start DATETIME = '20151201'
--	,@end DATETIME = '20151213'
--	,@ward varchar(150) = 'A1'
--	,@Adm int = NULL
--	,@MRSAExcl int = 0
--	,@MRSAReq int = 1
--	,@MRSAComp int =1
	
SELECT FaciltyID
	,NHSNo
	,AdmissionSpecialty
	,AdmissionType
	,PatientClass
	,AdmissionDateTime
	,DischargeDateTime
	,AdmissiontWardCode
	,AdmissionWard
	,AdmissionConsultantName
	,PrimaryProcedureOfFirstEpisode
	,Exclusion
	,SourceSpellNo
	,FirstRequestDate --date of the first MRSA Screening requested on ICE within the Admission Date range period of 6 weeks prior or on admission
	,InvestigationRequested
	,ReportDateTime --latest report of the first MRSA screening requested
	,AbnormalFlag
FROM [WHREPORTING].APC.[ELectiveAdmissionMRSAScreens]
WHERE AdmissionDateTime >= @start
	AND Cast(AdmissionDateTime AS DATE) <= @end
	AND AdmissionWard = @ward
	AND (
		AdmissionFlag = @Adm
		OR @Adm IS NULL
		)
	AND (
		[MRSAScreenExclFlag] = @MRSAExcl
		OR @MRSAExcl IS NULL
		)
	AND (
		[MRSAScreenRequiredFlag] = @MRSAReq
		OR @MRSAReq IS NULL
		)
	AND (
		[MRSAScreenCompletedFlag] = @MRSAComp
		OR @MRSAComp IS NULL
		)
	AND (
		[AbnormalFlag] = @Abnormal
		OR @Abnormal IS NULL
	)