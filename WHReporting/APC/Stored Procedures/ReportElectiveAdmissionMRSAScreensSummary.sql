﻿/****** Script for SelectTopNRows command from SSMS  ******/


CREATE Procedure [APC].[ReportElectiveAdmissionMRSAScreensSummary] @start DATETIME ,@end DATETIME 

AS 


--DECLARE @start DATETIME = '20151001'
--	,@end DATETIME = '20151031'

SELECT AdmissionWard
	,SUM(AdmissionFlag) AS NumberofAdmissions
	,Sum([MRSAScreenExclFlag]) AS NumberOfAdmissionsMRSAScreenExcl
	,Sum([MRSAScreenRequiredFlag]) AS NumberOfAdmissionsToBeScreened
	,Sum([MRSAScreenCompletedFlag]) AS NumberOfAdmissionsWithMRSAScreen
	,SUM(CASE 
			WHEN [MRSAScreenCompletedFlag] = 1
				THEN [AbnormalFlag]
			ELSE 0
			END) AS NumberOfAbnormalCompleted
FROM [WHREPORTING].APC.[ELectiveAdmissionMRSAScreens]
WHERE AdmissionDateTime >= @start
	AND Cast(AdmissionDateTime AS DATE) <= @end
GROUP BY AdmissionWard