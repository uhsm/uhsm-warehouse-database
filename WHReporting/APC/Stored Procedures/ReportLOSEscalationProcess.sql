﻿/******************************************************************************
 Description: Used to extract data for the LOS Escalation Process SSRS report.
  
 Parameters:  @CensusDate - Required current inpatient snapshot date.
              @LOSThreshold - Required LOS Threshold crossed (7, 14, 21 or 28 days).
              @Reviewer - Required reviewer(s) for the report.

 Modification History --------------------------------------------------------

 Date     Author       Change
 08/05/14 Tim Dean	   Created
 03/06/14 Graham Ryder Change to only include patients who are on 7, 14, 21 or 28 days on Census date

******************************************************************************/

CREATE Procedure [APC].[ReportLOSEscalationProcess]
 @CensusDate as Date,
 @LOSThreshold AS NVARCHAR(2000),
 @Reviewer AS NVARCHAR(3000)
as

SET NOCOUNT ON;

IF Object_id('tempdb..#LOSThreshold', 'U') IS NOT NULL
  DROP TABLE #LOSThreshold;

SELECT Item
INTO   #LOSThreshold
FROM   WHREPORTING.dbo.Splitstrings_cte(@LOSThreshold, N',');

IF Object_id('tempdb..#Reviewer', 'U') IS NOT NULL
  DROP TABLE #Reviewer;

SELECT Item
INTO   #Reviewer
FROM   WHREPORTING.dbo.Splitstrings_cte(@Reviewer, N',');


SELECT
  Data.FacilityID
 ,Data.PatientSurname
 ,Data.EndWardCode
 ,Data.EndWard
 ,Data.ConsultantName
 ,Data.AdmissionMethod
 ,Data.Directorate
 ,Data.Division
 ,Data.SpellLoSThresholdCrossed
 ,Data.Reviewer
FROM (
      SELECT 
        Included.FacilityID
       ,Included.PatientSurname
       ,Included.EndWardCode
       ,Included.EndWard
       ,Included.ConsultantName
       ,Included.AdmissionMethod
       ,Included.Directorate
       ,Included.Division
       ,CASE
          WHEN Included.SpellLoS >= 28 THEN '28'
          WHEN Included.SpellLoS >= 21 THEN '21'
          WHEN Included.SpellLoS >= 14 THEN '14'
          WHEN Included.SpellLoS >= 7 THEN '7'
        END SpellLoSThresholdCrossed
       ,CASE
          WHEN Included.SpellLoS < 14 THEN REPLACE(Included.ConsultantName, ',', '')
          WHEN Included.SpellLoS < 21 THEN CASE Included.Directorate
                                             WHEN 'Medical Specialities' THEN 'Issa Basil'
                                             WHEN 'Urgent Care' THEN 'Watson Lesley'
                                             WHEN 'Complex Health & Social Care' THEN 'Briggs Sally'
                                             WHEN 'Respiratory' THEN 'Barraclough Richard'
                                             WHEN 'Surgery' THEN 'Galloway Simon'
                                             WHEN 'Cardio & Thoracic' THEN 'Shah Rajesh'
                                             WHEN 'Womens & Childrens' THEN 'Ahluwalia Anjali'
                                             ELSE 'Unknown Reviewer'
                                            END
          WHEN Included.SpellLoS < 28 THEN CASE Included.Division
                                             WHEN 'Unscheduled Care' THEN 'Onon Toli'
                                             WHEN 'Scheduled Care' THEN 'Levy Richard'
                                             WHEN 'Clinical Support' THEN 'Beards Sue'
                                             ELSE 'Unknown Reviewer'
                                            END
          WHEN Included.SpellLoS >= 28 THEN 'Crampton John'
        END Reviewer
      FROM (
            SELECT 
              Episode.FacilityID
             ,Episode.EndWard
             ,Episode.EndWardCode
             ,Episode.ConsultantName
             ,Episode.Directorate
             ,Episode.Division
             ,DATEDIFF(DAY,Episode.AdmissionDate,@CensusDate) SpellLoS
             ,Episode.AdmissionDate
             ,Episode.DischargeDate
             ,Spell.AdmissionMethod
             ,Patient.PatientSurname
            FROM WHREPORTING.APC.Episode
              LEFT JOIN WHREPORTING.APC.Spell
                ON (Episode.SourceSpellNo = Spell.SourceSpellNo)
              LEFT JOIN WHREPORTING.APC.Patient
                ON (Episode.EpisodeUniqueID = Patient.EpisodeUniqueID)
            WHERE (Episode.AdmissionDate < @CensusDate
                   AND (Episode.DischargeDate >= @CensusDate
                        OR Episode.DischargeDate IS NULL))
                  AND (Episode.EpisodeStartDate < @CensusDate
                       AND (Episode.EpisodeEndDate >= @CensusDate
                            OR EpisodeEndDate IS NULL)) 
                  AND Episode.[SpecialtyCode(Main)] <> '501'
           ) Included
      WHERE 
            Included.SpellLoS in(7,14,21,28)
--			Included.SpellLoS >= 7 - GR This line has been removed and replaced with the line above to only include patients who are on 7, 14, 21 or 28 days on Census date
            AND Included.EndWardCode NOT IN ( 'SCB'
                                             ,'ASPIRE'
                                             ,'F8M'
                                             ,'CTCU'
                                             ,'ICA'
                                             ,'ALEX HDU'
                                             ,'ALEX ICU'
                                             ,'ALE'
                                             ,'ALE PCT'
                                             ,'BWH'
                                             ,'SPIREW'
                                  )
      ) Data
  INNER JOIN #LOSThreshold t 
        ON Data.SpellLoSThresholdCrossed = t.Item
  INNER JOIN #Reviewer r 
        ON Data.Reviewer = r.Item;        

-- Tidy up temporary parameter tables
IF Object_id('tempdb..#LOSThreshold', 'U') IS NOT NULL
  DROP TABLE #LOSThreshold

IF Object_id('tempdb..#Reviewer', 'U') IS NOT NULL
  DROP TABLE #Reviewer