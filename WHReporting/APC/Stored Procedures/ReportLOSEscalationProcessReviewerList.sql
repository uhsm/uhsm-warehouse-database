﻿/******************************************************************************
 Description: Used to extract list of distinct reviewer for SSRS report parameter
              for the LOS Escalation Process SSRS report.
  
 Parameters:  @CensusDate - Required current inpatient snapshot date.

 Modification History --------------------------------------------------------

 Date     Author      Change
 08/05/14 Tim Dean	  Created
 03/06/14 Graham Ryder Change to only include patients who are on 7, 14, 21 or 28 days on Census date
 
******************************************************************************/

CREATE Procedure [APC].[ReportLOSEscalationProcessReviewerList]
 @CensusDate as Date
as

SET NOCOUNT ON;

SELECT DISTINCT
 Data.Reviewer
FROM (
      SELECT 
        CASE
          WHEN Included.SpellLoS < 14 THEN REPLACE(Included.ConsultantName, ',', '')
          WHEN Included.SpellLoS < 21 THEN CASE Included.Directorate
                                             WHEN 'Medical Specialities' THEN 'Issa Basil'
                                             WHEN 'Urgent Care' THEN 'Watson Lesley'
                                             WHEN 'Complex Health & Social Care' THEN 'Briggs Sally'
                                             WHEN 'Respiratory' THEN 'Barraclough Richard'
                                             WHEN 'Surgery' THEN 'Galloway Simon'
                                             WHEN 'Cardio & Thoracic' THEN 'Shah Rajesh'
                                             WHEN 'Womens & Childrens' THEN 'Ahluwalia Anjali'
                                             ELSE 'Unknown Reviewer'
                                            END
          WHEN Included.SpellLoS < 28 THEN CASE Included.Division
                                             WHEN 'Unscheduled Care' THEN 'Onon Toli'
                                             WHEN 'Scheduled Care' THEN 'Levy Richard'
                                             WHEN 'Clinical Support' THEN 'Beards Sue'
                                             ELSE 'Unknown Reviewer'
                                            END
          WHEN Included.SpellLoS >= 28 THEN 'Crampton John'
        END Reviewer
      FROM (
            SELECT 
              Episode.EndWardCode
             ,Episode.ConsultantName
             ,Episode.Directorate
             ,Episode.Division
             ,DATEDIFF(DAY,Episode.AdmissionDate,@CensusDate) SpellLoS
            FROM WHREPORTING.APC.Episode
              LEFT JOIN WHREPORTING.APC.Spell
                ON (Episode.SourceSpellNo = Spell.SourceSpellNo)
            WHERE (Episode.AdmissionDate < @CensusDate
                   AND (Episode.DischargeDate >= @CensusDate
                        OR Episode.DischargeDate IS NULL))
                  AND (Episode.EpisodeStartDate < @CensusDate
                       AND (Episode.EpisodeEndDate >= @CensusDate
                            OR EpisodeEndDate IS NULL)) 
                  AND Episode.[SpecialtyCode(Main)] <> '501'
           ) Included
      WHERE 
            Included.SpellLoS in(7,14,21,28)
--			Included.SpellLoS >= 7 - GR This line has been removed and replaced with the line above to only include patients who are on 7, 14, 21 or 28 days on Census date

            AND Included.EndWardCode NOT IN ( 'SCB'
                                             ,'ASPIRE'
                                             ,'F8M'
                                             ,'CTCU'
                                             ,'ICA'
                                             ,'ALEX HDU'
                                             ,'ALEX ICU'
                                             ,'ALE'
                                             ,'ALE PCT'
                                             ,'BWH'
                                             ,'SPIREW'
                                  )
      ) Data;