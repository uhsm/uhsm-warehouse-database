﻿-- =============================================
-- Author:		CMan
-- Create date: 17/02/2014
-- Description:	Stored Procedure for NHS Choices Report to calculate day case rate indictors for certain procedures
-- =============================================
CREATE PROCEDURE [APC].[ReportNHSChoicesDaycaseIndicators]
	-- Add the parameters for the stored procedure here

@start as datetime

WITH RECOMPILE 	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
Declare @end as datetime
--Declare @start as datetime 
--Set @start ='20150101'
set @end = DATEADD(month, +12, @start)
; 
--create an episodes base table to work from which contains all the episodes where the discharge is in the selected period 
--add into this table a cancer flag to be used later for excluding any episodes with cancer
With  Episodes 
(DischargeMonth
,DischargeMonthStart
,PatientClass 
,AdmissionType 
,AdmissionMethodCode 
,SourceSpellNo 
,EpisodeSourceUniqueID
,Age 
,CancerFlag 
,SourcePatientNo 
,FaciltyID  
,AdmissionDateTime 
,DischargeDateTime 
,EpisodeNo 
,PrimaryOperationCode
,PrimaryOperationDate
,PrimaryDiagnosisCode  
,SecondaryProcedureCodes 
,LengthOfSpell 
,EpisodeStartTime 
,EpisodeEndTime 
)
 as (
Select cal.TheMonth as DischargeMonth
		, cal.FirstDateOfMonth as DischargeMonthStart
		,PatientClass
		, sp.AdmissionType
		,sp.AdmissionMethodCode
		, sp.SourceSpellNo
		, ep.SourceUniqueID as EpisodeSourceUniqueID
		,sp.Age
		, Case when ep.PrimaryDiagnosisCode IN
			('C000','C001','C002','C003','C004','C005','C006','C008','C009','C01X','C020','C021','C022','C023','C024','C028','C029','C030','C031','C039','
			C040','C041','C048','C049','C050','C051','C052','C058','C059','C060','C061','C062','C068','C069','C07X','C080','C081','C088','C089','C090','
			C091','C098','C099','C100','C101','C102','C103','C104','C108','C109','C110','C111','C112','C113','C118','C119','C12X','C130','C131','C132','
			C138','C139','C140','C148','C150','C151','C152','C153','C154','C155','C158','C159','C160','C161','C162','C163','C164','C165','C166','C168','
			C169','C170','C171','C172','C173','C178','C179','C180','C181','C182','C183','C184','C185','C186','C187','C188','C189','C19X','C20X','C210','
			C211','C212','C218','C220','C221','C222','C223','C224','C227','C229','C23X','C240','C241','C248','C249','C250','C251','C252','C253','C254','
			C257','C258','C259','C260','C261','C268','C269','C300','C301','C310','C311','C312','C313','C318','C319','C320','C321','C322','C323','C328','
			C329','C33X','C340','C341','C342','C343','C348','C349','C37X','C380','C381','C382','C383','C384','C388','C390','C398','C399','C400','C401','
			C402','C403','C408','C409','C410','C411','C412','C413','C414','C418','C419','C450','C451','C452','C457','C459','C461','C462','C467','C469','
			C470','C471','C472','C473','C474','C475','C476','C478','C479','C480','C481','C482','C488','C490','C491','C492','C493','C494','C495','C496','
			C498','C499','C500','C501','C502','C503','C504','C506','C508','C509','C510','C511','C512','C518','C519','C52X','C530','C531','C538','C539','
			C540','C541','C542','C543','C548','C549','C55X','C56X','C570','C571','C572','C573','C574','C577','C578','C579','C58X','C600','C601','C602','
			C608','C609','C61X','C620','C621','C629','C630','C631','C632','C637','C638','C639','C64X','C65X','C66X','C670','C671','C672','C673','C674','
			C675','C676','C677','C678','C679','C680','C688','C689','C690','C691','C692','C693','C694','C695','C696','C698','C699','C700','C701','C709','
			C710','C711','C712','C713','C714','C715','C716','C717','C718','C719','C720','C721','C722','C723','C724','C725','C728','C729','C73X','C740','
			C741','C749','C750','C751','C752','C753','C754','C755','C758','C759','C760','C761','C762','C763','C764','C765','C767','C768','C960','C961','
			C962','C963','C967','C969', 'C968','D000','D001','D002','D010','D011','D012','D013','D014','D015','D017','D020','D021','D022','D023','D024','
			D050','D051','D057','D059','D060','D061','D067','D069','D070','D071','D072','D073','D074','D075','D076','D090','D091','D092','D093','
			D097','D099'
			) then 'Y' Else 'N' End as CancerFlag,--assigns cancer flag so that episodes which have a primary diagnosis of cancer can be excluded in later query
		sp.SourcePatientNo,
		sp.FaciltyID,
		sp.AdmissionDateTime,
		sp.DischargeDateTime,
		ep.EpisodeNo,
		ep.PrimaryOperationCode,
		ep.PrimaryOperationDate,
		ep.PrimaryDiagnosisCode,
		Isnull(SecondaryOperationCode1, '')
       + ',' + Isnull(SecondaryOperationCode2, '')
       + ',' + Isnull(SecondaryOperationCode3, '')
       + ',' + Isnull(SecondaryOperationCode4, '')
       + ',' + Isnull(SecondaryOperationCode5, '')
       + ',' + Isnull(SecondaryOperationCode6, '')
       + ',' + Isnull(SecondaryOperationCode7, '')
       + ',' + Isnull(SecondaryOperationCode8, '')
       + ',' + Isnull(SecondaryOperationCode9, '')
       + ',' + Isnull(SecondaryOperationCode10, '')
       + ',' + Isnull(SecondaryOperationCode11, '') as SecondaryProcedureCodes,
		sp.LengthOfSpell,
		ep.EpisodeStartTime,
		ep.EpisodeEndTime

from WHReporting.APC.Spell sp
Left outer join WH.APC.Encounter ep
on sp.SourceSpellNo = ep.SourceSpellNo
Left outer join WHREPORTING.LK.Calendar cal
on sp.DischargeDate = cal.TheDate
where sp.DischargeDate >= @start
and sp.DischargeDate < @end
)
,
/*-------------------------
Arthroscopy Day Cases
------------------------------*/
--Create second temp table which contains the episodes which follows the two different coding conditions as specified in the indicator construction for Arthroscopy DayCases
ArthrEpisodes
(DischargeMonth
,DischargeMonthStart 
,PatientClass 
,AdmissionType 
,AdmissionMethodCode 
,SourceSpellNo 
,EpisodeSourceUniqueID  
,Age  
,CancerFlag 
,SourcePatientNo 
,FaciltyID  
,AdmissionDateTime 
,DischargeDateTime 
,EpisodeNo 
,PrimaryOperationCode
,PrimaryOperationDate 
,PrimaryDiagnosisCode 
,SecondaryProcedureCodes 
,LengthOfSpell 
,EpisodeStartTime 
,EpisodeEndTime)
 as 
 (
Select *
From 
(--this gets the episodes which have been coded as per the first operation code condition
Select *
from Episodes 
where Episodes.AdmissionMethodCode in ('11','12','13') --only Elective spells to be included in cohort of patients for this indicator
and  (Episodes.PrimaryOperationCode in  ('W821', 'W822', 'W823', 'W828', 'W829', 'W831', 'W832', 'W833', 'W834', 'W835', 'W836', 'W837', 'W838', 'W839', 'W851', 'W852', 'W853', 'W858', 'W859','W871', 'W878', 'W879' )
			and Episodes.CancerFlag = 'N')
and Episodes.Age > 18
union
--this gets the episodes which have been coded as per the second operation code condition
Select *
from Episodes 
where Episodes.AdmissionMethodCode in ('11','12','13') --only Elective spells to be included in cohort of patients for this indicator
and  (Episodes.PrimaryOperationCode in ('W841', 'W842', 'W843', 'W844', 'W845', 'W846', 'W847', 'W848', 'W849', 'W891', 'W892', 'W898','W899')
			and Episodes.SecondaryProcedureCodes like '%Z846%'
			and Episodes.CancerFlag = 'N')
and Episodes.Age > 18
) a
)
,

-- Temp table to get identify first episode number within the spell with Arthroscopy primary procedure
Arth_Min_ep
(SourcespellNo 
, MinEpisodeno 
)
as 
(Select ArthrEpisodes.SourceSpellNo, MIN(ArthrEpisodes.episodeno) as MinEpisodeNo--use min to get the number of the first episode which fulfils the criteria
From ArthrEpisodes 
Group by ArthrEpisodes.SourceSpellNo
)



, 

/*----------------------------------------------------------
Dupuytrens Contracture
------------------------------------------------------------*/
-- Temp table to get identify first episode number within the spell with the Dupuytrens Contracture criteria required
Dup_min_ep
(SourcespellNo 
, MinEpisodeno 
)
 as 
 (
Select Episodes.SourceSpellNo, MIN(Episodes.episodeno) as MinEpisodeNo--use min to get the number of the first episode which fulfils the criteria
From Episodes 
where Episodes.AdmissionMethodCode in ('11','12','13') --only Elective spells to be included in cohort of patients for this indicator
and (Episodes.PrimaryOperationCode in  ('T521', 'T522', 'T541')
 and Episodes.CancerFlag = 'N')--only include episodes where the Primary Procedure is one in the criteria and the Primary Diagnosis of the same episode is not one of the cancer codes is to be included
and Episodes.Age > 18 --only include spells where patient age at spell start is 19 or over
Group by Episodes.SourceSpellNo
)

,

/*----------------------------------------------------------
Repair of Umbilical Hernia
------------------------------------------------------------*/
-- Temp table to get identify first episode number within the spell with the criteria required
UmH_min_ep
(SourcespellNo 
, MinEpisodeno 
)

as 
(
Select Episodes.SourceSpellNo, MIN(Episodes.episodeno) as MinEpisodeNo--use min to get the number of the first episode which fulfils the criteria
From Episodes 
where Episodes.AdmissionMethodCode in ('11','12','13') --only Elective spells to be included in cohort of patients for this indicator
and (Episodes.PrimaryOperationCode in ('T241', 'T242', 'T243', 'T244', 'T248', 'T249', 'T971', 'T972', 'T973', 'T978', 'T979')
 and Episodes.CancerFlag = 'N')--only include episodes where the Primary Procedure is one in the criteria  and the Primary Diagnosis of the same episode is not one of the cancer codes is to be included
and Episodes.Age > 18 --only include spells where patient age at spell start is 19 or over
Group by Episodes.SourceSpellNo
)
,





/*----------------------------------------------------------
Repair of Inguinal Hernia
------------------------------------------------------------*/
-- Temp table to get identify first episode number within the spell with the criteria required
IngH_min_ep
(SourcespellNo 
, MinEpisodeno 
)
 as 
 (
Select Episodes.SourceSpellNo, MIN(Episodes.episodeno) as MinEpisodeNo--use min to get the number of the first episode which fulfils the criteria
From Episodes 
where Episodes.AdmissionMethodCode in ('11','12','13') --only Elective spells to be included in cohort of patients for this indicator
and (Episodes.PrimaryOperationCode in ('T191', 'T192', 'T193', 'T198', 'T199', 'T201', 'T202', 'T203','T204', 'T208', 'T209', 'T211', 'T212', 'T213', 'T218', 'T219')
 and Episodes.CancerFlag = 'N')--only include episodes where the Primary Procedure is  one in the critieria and the Primary Diagnosis of the same episode is not one of the cancer codes is to be included
and Episodes.Age > 18 --only include spells where patient age at spell start is 19 or over
Group by Episodes.SourceSpellNo
)

,




/*----------------------------------------------------------
Bunion Operations
------------------------------------------------------------*/
-- Temp table to get identify first episode number within the spell with the criteria required
Bun_min_ep
(SourcespellNo 
, MinEpisodeno 
)
 as (
Select Episodes.SourceSpellNo, MIN(Episodes.episodeno) as MinEpisodeNo--use min to get the number of the first episode which fulfils the criteria
From Episodes 
where Episodes.AdmissionMethodCode in ('11','12','13') --only Elective spells to be included in cohort of patients for this indicator
and (Episodes.PrimaryOperationCode in ('W151', 'W152', 'W153', 'W791', 'W792', 'W793', 'W798','W799', 'W591', 'W592', 'W593', 'W594', 'W595', 'W596', 'W598', 'W599')
 and Episodes.CancerFlag = 'N')--only include episodes where the Primary Procedure is one in the critieria and the Primary Diagnosis of the same episode is not one of the cancer codes is to be included
and Episodes.Age > 18 --only include spells where patient age at spell start is 19 or over
Group by Episodes.SourceSpellNo

) , 







/*----------------------------------------------------------
Surgery for Haemorrhoids 
------------------------------------------------------------*/
-- Temp table to get identify first episode number within the spell with the criteria required
Haem_min_ep
(SourcespellNo 
, MinEpisodeno 
)
as 
(
Select Episodes.SourceSpellNo, MIN(Episodes.episodeno) as MinEpisodeNo--use min to get the number of the first episode which fulfils the criteria
From Episodes 
where Episodes.AdmissionMethodCode in ('11','12','13') --only Elective spells to be included in cohort of patients for this indicator
and (Episodes.PrimaryOperationCode = 'H511' and Episodes.CancerFlag = 'N')--only include episodes where the Primary Procedure is  one within the criteria and the Primary Diagnosis of the same episode is not one of the cancer codes is to be included
and Episodes.Age > 18 --only include spells where patient age at spell start is 19 or over
Group by Episodes.SourceSpellNo
)

,





/*----------------------------------------------------------
Examination of Uterus
------------------------------------------------------------*/
-- Temp table to get identify first episode number within the spell with the criteria required
Uter_min_ep
(SourcespellNo 
, MinEpisodeno 
)

as 
(
Select Episodes.SourceSpellNo, MIN(Episodes.episodeno) as MinEpisodeNo--use min to get the number of the first episode which fulfils the criteria
From Episodes 
where Episodes.AdmissionMethodCode in ('11','12','13') --only Elective spells to be included in cohort of patients for this indicator
and (Episodes.PrimaryOperationCode in ('Q181', 'Q188', 'Q189')
 and Episodes.CancerFlag = 'N')--only include episodes where the Primary Procedure is H511 and the Primary Diagnosis of the same episode is not one of the cancer codes is to be included
and Episodes.Age > 18 --only include spells where patient age at spell start is 19 or over
Group by Episodes.SourceSpellNo

)



,
/*----------------------------------------------------------
Coronary Angiography
------------------------------------------------------------*/
-- Temp table to get identify first episode number within the spell with the  Coronary Angiography criteria required
Coron_min_ep
(SourcespellNo 
, MinEpisodeno 
)
 as 
 (
Select Episodes.SourceSpellNo, MIN(Episodes.episodeno) as MinEpisodeNo--use min to get the number of the first episode which fulfils the criteria
From Episodes 
where Episodes.AdmissionMethodCode in ('11','12','13') --only Elective spells to be included in cohort of patients for this indicator
and (Episodes.PrimaryOperationCode in ('K631', 'K632', 'K633', 'K634', 'K635', 'K636', 'K638', 'K639','K651', 'K652', 'K653', 'K654', 'K658', 'K659')
			and Episodes.CancerFlag = 'N')--only include episodes where the Primary Procedure is one of those listed in the indicator document and the Primary Diagnosis of the same episode is not one of the cancer codes is to be included
and Episodes.Age > 18 --only include spells where patient age at spell start is 19 or over
Group by Episodes.SourceSpellNo
)


,



/*----------------------------------------------------------
Conservative Surgery for Breast Cancer
------------------------------------------------------------*/
--Get all the episodes within the time period where the primary procedures is one of those listed in the indicator document
BrEpisodes
(DischargeMonth
,DischargeMonthStart 
,PatientClass 
,AdmissionType 
,AdmissionMethodCode 
,SourceSpellNo 
,EpisodeSourceUniqueID  
,Age  
,CancerFlag 
,SourcePatientNo 
,FaciltyID  
,AdmissionDateTime 
,DischargeDateTime 
,EpisodeNo 
,PrimaryOperationCode
,PrimaryOperationDate 
,PrimaryDiagnosisCode 
,SecondaryProcedureCodes 
,LengthOfSpell 
,EpisodeStartTime 
,EpisodeEndTime)
 as 
(Select *
from Episodes
where Episodes.AdmissionMethodCode in ('11','12','13')--only Elective spells to be included in cohort of patients for this indicator
and Episodes.PrimaryOperationCode in ('B281', 'B282', 'B283', 'B285', 'B287', 'B288', 'B289', 'B401','B408', 'B409') 
and Episodes.Age > 18
)

, 

--Get the first episode which has one of the procedure codes, where the primary diagnosis code of the same episode is one of those listed in the indicatior consruction document
Breast_Min_ep
(SourcespellNo 
, MinEpisodeno 
)
 as
(Select SourceSpellNo, MIN(EpisodeNo) as MinEpisodeNo
from BrEpisodes
where PrimaryDiagnosisCode in ('C500', 'C501', 'C502', 'C503', 'C504', 'C505', 'C506', 'C508','C509', 'D050', 'D051', 'D057', 'D059')--only want those episodes where the primary diagnosis of the same episode also has one of the diagnosis codes listed
Group by SourceSpellNo
)


,





--temp table to join #min_ep back to #Episodes to get the details of the spell and that first episode which fulfils the criteria
--this is the base data - essentially forms the denominator 
final 
(Indicator 
,DischargeMonth 
,DischargeMonthStart 
,PatientClass 
,AdmissionType 
,AdmissionMethodCode 
,SourceSpellNo 
,EpisodeSourceUniqueID 
,Age 
,CancerFlag 
,SourcePatientNo 
,FaciltyID  
,AdmissionDateTime 
,DischargeDateTime 
,EpisodeNo 
,PrimaryOperationCode 
,PrimaryOperationDate  
,PrimaryDiagnosisCode  
,SecondaryProcedureCodes 
,LengthOfSpell 
,EpisodeStartTime 
,EpisodeEndTime 
,Spell		
,DayCases	
)
as 
(
Select 'Arthroscopy of Knee Operations' as Indicator, ArthrEpisodes.*, 
1 as Spell, 
case when LengthOfSpell = 0 and PatientClass = 'Day Case' then 1 else 0 end as DayCases -- this is the criteria for identifying day cases as  specified in Indicator construction document
from Arth_min_ep 
left outer join ArthrEpisodes on Arth_min_ep.SourceSpellNo = ArthrEpisodes.SourceSpellNo and Arth_min_ep.MinEpisodeNo = ArthrEpisodes.episodeno

union all

---Insert into #Final table the rows for the Coronary Angiography Indicator

Select 'Coronary Angiography Operations'    ,Episodes.*, 
1 as Spell, 
case when LengthOfSpell = 0 and PatientClass = 'Day Case' then 1 else 0 end as DayCases -- this is the criteria for identifying day cases as  specified in Indicator construction document
from Coron_min_ep 
left outer join Episodes on Coron_min_ep.SourceSpellNo = Episodes.SourceSpellNo and Coron_min_ep.MinEpisodeNo = episodes.episodeno


union all 

Select 'Surgery for Dupuytrens Contracture' as Indicator,  Episodes.*, 
1 as Spell, 
case when LengthOfSpell = 0 and PatientClass = 'Day Case' then 1 else 0 end as DayCases -- this is the criteria for identifying day cases as  specified in Indicator construction document
from Dup_min_ep 
left outer join Episodes on Dup_min_ep.SourceSpellNo = Episodes.SourceSpellNo and Dup_min_ep.MinEpisodeNo = episodes.episodeno


union all 
Select 'Repair of Umbilical Hernia Operations'  as Indicator, Episodes.*, 
1 as Spell, 
case when LengthOfSpell = 0 and PatientClass = 'Day Case' then 1 else 0 end as DayCases -- this is the criteria for identifying day cases as  specified in Indicator construction document
from UmH_min_ep 
left outer join Episodes on UmH_min_ep.SourceSpellNo = Episodes.SourceSpellNo and UmH_min_ep.MinEpisodeNo = episodes.episodeno



union all


Select 'Repair of Inguinal Hernia Operations'  as Indicator, Episodes.*, 
1 as Spell, 
case when LengthOfSpell = 0 and PatientClass = 'Day Case' then 1 else 0 end as DayCases -- this is the criteria for identifying day cases as  specified in Indicator construction document
from IngH_min_ep 
left outer join Episodes on IngH_min_ep.SourceSpellNo = Episodes.SourceSpellNo and IngH_min_ep.MinEpisodeNo = episodes.episodeno

union all

Select  'Bunion Operations'   as Indicator, Episodes.*, 
1 as Spell, 
case when LengthOfSpell = 0 and PatientClass = 'Day Case' then 1 else 0 end as DayCases -- this is the criteria for identifying day cases as  specified in Indicator construction document
from Bun_min_ep 
left outer join Episodes on Bun_min_ep.SourceSpellNo = Episodes.SourceSpellNo and Bun_min_ep.MinEpisodeNo = episodes.episodeno


union all

Select  'Surgery for Haemorrhoids'    as Indicator, Episodes.*, 
1 as Spell, 
case when LengthOfSpell = 0 and PatientClass = 'Day Case' then 1 else 0 end as DayCases -- this is the criteria for identifying day cases as  specified in Indicator construction document
from Haem_min_ep 
left outer join Episodes on Haem_min_ep.SourceSpellNo = Episodes.SourceSpellNo and Haem_min_ep.MinEpisodeNo = episodes.episodeno

 union all
 
 Select  'Examination of Uterus'    as Indicator, Episodes.*, 
1 as Spell, 
case when LengthOfSpell = 0 and PatientClass = 'Day Case' then 1 else 0 end as DayCases -- this is the criteria for identifying day cases as  specified in Indicator construction document
from Uter_min_ep 
left outer join Episodes on Uter_min_ep.SourceSpellNo = Episodes.SourceSpellNo and Uter_min_ep.MinEpisodeNo = episodes.episodeno

union all

Select 'Conservative surgery for breast cancer ' as Indicator, BrEpisodes.*, 
1 as Spell, 
case when LengthOfSpell = 0 and PatientClass = 'Day Case' then 1 else 0 end as DayCases -- this is the criteria for identifying day cases as  specified in Indicator construction document
from Breast_Min_ep 
left outer join BrEpisodes on Breast_Min_ep.SourceSpellNo = BrEpisodes.SourceSpellNo and Breast_Min_ep.MinEpisodeNo = BrEpisodes.episodeno

)

--Truncate table APC.DaycaseIndicators

--Insert into WHReporting.APC.DaycaseIndicators
SELECT (SELECT  Themonth
        FROM   WHReporting.lk.Calendar
        WHERE  FirstDateOfMonth = Dateadd(month, 5,@end)
        Group by TheMonth) AS ReportingMonth,
       Indicator,
       @start                                                          AS PeriodStart,
       Dateadd(day, -1,@end)                                                          AS PeriodEnd,
       Sum(spell)                                                      NumberOfSpells,
       Sum(daycases)                                                   AS NumberOfDaycases,
       Sum(daycases) / CONVERT(FLOAT, Sum(spell))             AS PercentageDaycases

FROM   Final
GROUP  BY Indicator 


END