﻿CREATE procedure [APC].[ReportSmartboardSSABreach]
	@StartDate datetime
	,@EndDate datetime
	,@BreachWard varchar(500)
as
/*
--Author: K Oakden
--Date created: 28/03/2014
--Get Smartboard SSA breach data
--SSRS report code at S:\CO-HI-Information\New File Management - Jan 2011\Data Warehouse Project\SSRS Report Code\Corporate\CorporateReporting
*/

exec UHSMApplications.SB.ExtractBeds
exec UHSMApplications.SB.ExtractSSABreaches

-- To improve query performance create tables from the parameters
-- SSRS passes the parameters a CSV strings, sometimes there can
-- be 30+ entries.  
if Object_id('tempdb..#BreachWard', 'U') is not null
	drop table #BreachWard
	
select ward = rtrim(ltrim(item))
into   #BreachWard
from   WHREPORTING.dbo.Splitstrings_cte(@BreachWard, N',')

select 
	RM2Number = isnull(spell.FaciltyID, breach.VisitNumber),	spell.NHSNo, 	Patient = pat.PatientForename + ' ' + pat.PatientSurname,
	breach.VisitNumber,	breach.BreachBed,	BedName = isnull(bed.BedName, 'Bed no longer exists'),	breach.BedType,	breach.BreachWard,	BreachStart = convert(varchar(17), breach.BreachStartTime, 113),	BreachEnd = 		case 			when breach.BreachEndTime is null			then 'Ongoing'			else convert(varchar(17), breach.BreachEndTime, 113)		end,	Duration = datediff(minute, breach.BreachStartTime, breach.BreachEndTime),	breach.BreachReason,	breach.BreachDetails,	breach.BreachCauseFlag,	--spell.AdmissionDateTime, 	--spell.DischargeDateTime ,	spell.ResponsibleCommissionerCode,	spell.ResponsibleCommissioner,	RecordCount = 1from UHSMApplications.SB.TImportBreachData breach

left join WHREPORTING.APC.Spell spell
	on cast(spell.SourceSpellNo as varchar) = breach.VisitNumber
	
left join 
(
	select 
		LastRecNo = max(encounterRecNo),
		SourceSpellNo
	from WHREPORTING.APC.Episode
	group by SourceSpellNo
) SpellLink
	on SpellLink.SourceSpellNo = spell.SourceSpellNo
	
left join WHREPORTING.APC.Patient pat
	on pat.EncounterRecno = SpellLink.LastRecNo
	
left join UHSMApplications.SB.TImportBeds bed
	on breach.BreachBed = bed.BedID

inner join #BreachWard ward
	on breach.breachward collate database_default = ward.ward collate database_default

where dateadd(day, 0, datediff(day, 0, breach.BreachStartTime)) >= @StartDate 
and dateadd(day, 0, datediff(day, 0, breach.BreachStartTime)) <= @EndDate  

--and breach.breachward in (
--	select item collate database_default from dbo.Split (@BreachWard, ',')
--)

order by breach.BreachStartTime desc;

if Object_id('tempdb..#BreachWard', 'U') is not null
	drop table #BreachWard
/**
--Ward lookup used in report
select BreachWard 
from UHSMApplications.SB.TImportBreachData
group by BreachWard
order by BreachWard
**/