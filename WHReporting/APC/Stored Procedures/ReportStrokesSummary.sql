﻿/*
--Author: J Cleworth
--Date created: 30/08/2013
--Stored Procedure ReportStrokesSummary
--
*/


CREATE Procedure [APC].[ReportStrokesSummary]

@FinancialYear varchar (25)

as

select 

FinancialMonthKey,
TheMonth,
sum(case when strokeunitperc<0.9 and Breach<>'Excluded' then 1 else 0 end) as LessThan90OnStroke,
sum(case when strokeunitperc>=0.9 and Breach<>'Excluded' then 1 else 0 end) as Morethan90OnStroke,
sum(case when TimeOnStrokeWards=0 and Breach<>'Excluded' then 1 else 0 end) as NotOnStrokeWard,
sum(case when FourHrAdmission='Breach' and Admissionward='STROKE UNIT ( F15 )' then 1 else 0 end) as FourHourBreach,
sum(case when FourHrAdmission='No Breach' and Admissionward='STROKE UNIT ( F15 )' then 1 else 0 end) as NoFourHourBreach,
sum(case when FourHrAdmission='No Breach' or FourHrAdmission='Breach' then 1 else 0 end) as PatientsAdmStroke,
sum(case when FourHrAdmission='' or FourHrAdmission is null then 1 else 0 end) as Exclusions




from [WHREPORTING].[APC].[vwStrokeData]

where financialmonthkey>=201301
and Breach<>'Excluded'
and FinancialYear in (@FinancialYear)
group by FinancialMonthKey,
TheMonth
order by financialmonthkey