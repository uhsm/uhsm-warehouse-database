﻿/*
--Author: J Cleworth
--Date created: 22/06/2013
--Stored Procedure Built for SRSS Report on:
--Tarn Reporting

*/


CREATE Procedure [APC].[ReportTarn]


@DateFrom as datetime,
@DateTo as datetime


as

select distinct

episode.FacilityID,
patient.NHSNo,
patient.PatientForename,
patient.PatientSurname,
patient.DateOfBirth,
spell.AdmissionDate,
spell.DischargeDate,
episode.EpisodeStartDateTime,
DATEDIFF(MINUTE,spell.admissiondatetime,coalesce(spell.dischargedatetime,cast(getdate() as date)))/1440.00 as LOS,
isnull(DATEDIFF(MINUTE,AE.ArrivalTime,
AE.AttendanceConclusionTime),0) as TimeInAE,
DATEDIFF(MINUTE,spell.admissiondatetime,coalesce(spell.dischargedatetime,cast(getdate() as date)))/1440.00 +
isnull(DATEDIFF(MINUTE,AE.ArrivalTime,
AE.AttendanceConclusionTime)/1440.00,0) as TotalLOS,

coding.PrimaryDiagnosisCode as PrimaryDiagnosis,
coding.SubsidiaryDiagnosisCode as SubsidiaryDiagnosis,
coding.SecondaryDiagnosisCode1 as SecondaryDiagnosis1,
coding.SecondaryDiagnosisCode2 as SecondaryDiagnosis2,
coding.SecondaryDiagnosisCode3 as SecondaryDiagnosis3,
coding.SecondaryDiagnosisCode4 as SecondaryDiagnosis4,
coding.SecondaryDiagnosisCode5 as SecondaryDiagnosis5,
coding.SecondaryDiagnosisCode6 as SecondaryDiagnosis6,
coding.SecondaryDiagnosisCode7 as SecondaryDiagnosis7,
coding.SecondaryDiagnosisCode8 as SecondaryDiagnosis8,
coding.SecondaryDiagnosisCode9 as SecondaryDiagnosis9,
coding.SecondaryDiagnosisCode10 as SecondaryDiagnosis10,
coding.SecondaryDiagnosisCode11 as SecondaryDiagnosis11, 
coding.SecondaryDiagnosisCode12 as SecondaryDiagnosis12,
coding.PriamryProcedure,
AdmissionSpecialty,
datediff( year, patient.DateOfBirth, spell.AdmissionDate ) - 
                  case when 100 * month( spell.AdmissionDate ) + 
                  day( spell.AdmissionDate ) < 100 * month( patient.DateOfBirth ) + 
                  day( patient.DateOfBirth ) then 1 else 0 end as Age,
Sex,
AdmissionMethod,
DischargeMethod,
DischargeDestination,
spell.SourceSpellNo,
Case when  DATEDIFF(MINUTE,spell.admissiondatetime,coalesce(spell.dischargedatetime,cast(getdate() as date)))/1440.00 >3 
then 'LOS>3 Days'
else '' end+' '
+case when DischargeDestinationNHSCode=4 then 'Patient Died' else '' end+' '+
Case when AdmissionWard in ('CARDIO - ICU','ACUTE - ICU','CT TRANSPLANT','BURNS INTENSIVE CARE','CTCU') 
then 'Admitted to CC Ward' else '' end
+' '+case when AdmissionMethodNHSCode=81 then 'Transfer In' else '' end+' '+Case when DischargeDestinationNHSCode between 49 and 53 then 'Transfer Out' else '' end
as InclusionReason
,spell.AdmissionType,
cast(getdate() as date) as DateReported
from  APC.Spell spell
inner join APC.Episode episode
on episode.SourceSpellNo=spell.SourceSpellNo

inner join APC.Patient patient

on patient.EncounterRecno=episode.EncounterRecno
inner join APC.ClinicalCoding coding on coding.EncounterRecno=episode.EncounterRecno
left JOIN WH.AE.Encounter AE ON AE.NHSNumber=spell.NHSNo
AND spell.AdmissionDateTime between dateadd(minute,-300,AE.AttendanceConclusionTime) and  dateadd(minute,600,AE.AttendanceConclusionTime)

where episode.EncounterRecno in (select distinct episode.EncounterRecno

from APC.Episode episode
inner join apc.Spell spell
on episode.SourceSpellNo=spell.SourceSpellNo
inner join apc.ClinicalCoding coding
on coding.EncounterRecno=episode.EncounterRecno


where
spell.AdmissionDate between @DateFrom and @DateTo
--and LastEpisodeInSpell=1
and

(
coding.PrimaryDiagnosisCode in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3)) or
coding.SubsidiaryDiagnosisCode in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3)) or
coding.SecondaryDiagnosisCode1 in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3)) or
coding.SecondaryDiagnosisCode2 in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3)) or
coding.SecondaryDiagnosisCode3 in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3))or
coding.SecondaryDiagnosisCode4 in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3))or
coding.SecondaryDiagnosisCode5 in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3))or
coding.SecondaryDiagnosisCode6 in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3))or
coding.SecondaryDiagnosisCode7 in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3))or
coding.SecondaryDiagnosisCode8 in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3))or
coding.SecondaryDiagnosisCode9 in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3))or
coding.SecondaryDiagnosisCode10 in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3))or
coding.SecondaryDiagnosisCode11 in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3))or
coding.SecondaryDiagnosisCode12 in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3))or
coding.SecondaryDiagnosisCode13 in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3))or
coding.SecondaryDiagnosisCode14 in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3))or
coding.SecondaryDiagnosisCode15 in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3))or
coding.SecondaryDiagnosisCode16 in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3))or
coding.SecondaryDiagnosisCode17 in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3))or
coding.SecondaryDiagnosisCode18 in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3))or
coding.SecondaryDiagnosisCode19 in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3))or
coding.SecondaryDiagnosisCode20 in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2,3))
)


and (DATEDIFF(Hour,spell.Admissiondatetime,spell.Dischargedatetime)>72
or DischargeMethodNHSCode=4
or AdmissionMethodNHSCode=81
OR DischargeDestinationNHSCode between 49 and 53
or AdmissionWard in ('CARDIO - ICU','ACUTE - ICU','CT TRANSPLANT','BURNS INTENSIVE CARE','CTCU'))
)
--and spell.FaciltyID='RM2253686'
and not exists 

(select 1 from apc.ClinicalCoding coding2 
inner join apc.Episode episode2 on coding2.EncounterRecno=episode2.EncounterRecno
inner join apc.Patient patient2 on patient2.EncounterRecno=episode2.EncounterRecno

where episode2.EncounterRecno=episode.EncounterRecno
and episode2.AdmissionDate between @DateFrom and @DateTo
and 
(
coalesce(coding2.PrimaryDiagnosisCode,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3))or
coalesce(coding.SubsidiaryDiagnosisCode,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3))or
coalesce(coding2.SecondaryDiagnosisCode1,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3))or
coalesce(coding2.SecondaryDiagnosisCode2,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3))or
coalesce(coding2.SecondaryDiagnosisCode3,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3))or
coalesce(coding2.SecondaryDiagnosisCode4,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3))or
coalesce(coding2.SecondaryDiagnosisCode5,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3))or
coalesce(coding2.SecondaryDiagnosisCode6,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3))or
coalesce(coding2.SecondaryDiagnosisCode7,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3))or
coalesce(coding2.SecondaryDiagnosisCode8,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3))or 
coalesce(coding2.SecondaryDiagnosisCode9,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3))or
coalesce(coding2.SecondaryDiagnosisCode10,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3))or
coalesce(coding2.SecondaryDiagnosisCode11,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3))or 
coalesce(coding2.SecondaryDiagnosisCode12,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3))or 
coalesce(coding2.SecondaryDiagnosisCode13,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3))or 
coalesce(coding2.SecondaryDiagnosisCode14,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3))or 
coalesce(coding2.SecondaryDiagnosisCode15,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3))or 
coalesce(coding2.SecondaryDiagnosisCode16,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3))or 
coalesce(coding2.SecondaryDiagnosisCode17,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3))or
coalesce(coding2.SecondaryDiagnosisCode18,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3))or 
coalesce(coding2.SecondaryDiagnosisCode19,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3))or 
coalesce(coding2.SecondaryDiagnosisCode20,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (3)
))

and not exists (select 1 from APC.clinicalcoding coding3 
inner join APC.Episode episode3 on coding3.EncounterRecno=episode3.EncounterRecno

where

episode3.EncounterRecno=episode2.EncounterRecno 
and episode3.AdmissionDate between @DateFrom and @DateTo
and 
(
coalesce(coding3.PrimaryDiagnosisCode,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2)) or 
coalesce(coding.SubsidiaryDiagnosisCode,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2)) or 
coalesce(coding3.SecondaryDiagnosisCode1,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2)) or 
coalesce(coding3.SecondaryDiagnosisCode2,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2)) or 
coalesce(coding3.SecondaryDiagnosisCode3,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2)) or 
coalesce(coding3.SecondaryDiagnosisCode4,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2)) or 
coalesce(coding3.SecondaryDiagnosisCode5,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2)) or 
coalesce(coding3.SecondaryDiagnosisCode6,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2)) or 
coalesce(coding3.SecondaryDiagnosisCode7,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2)) or 
coalesce(coding3.SecondaryDiagnosisCode8,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2)) or  
coalesce(coding3.SecondaryDiagnosisCode9,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2)) or 
coalesce(coding3.SecondaryDiagnosisCode10,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2)) or 
coalesce(coding3.SecondaryDiagnosisCode11,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2)) or  
coalesce(coding3.SecondaryDiagnosisCode12,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2)) or  
coalesce(coding3.SecondaryDiagnosisCode13,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2)) or  
coalesce(coding3.SecondaryDiagnosisCode14,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2)) or  
coalesce(coding3.SecondaryDiagnosisCode15,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2)) or  
coalesce(coding3.SecondaryDiagnosisCode16,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2)) or  
coalesce(coding3.SecondaryDiagnosisCode17,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2)) or 
coalesce(coding3.SecondaryDiagnosisCode18,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2)) or  
coalesce(coding3.SecondaryDiagnosisCode19,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2)) or  
coalesce(coding3.SecondaryDiagnosisCode20,'')in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude in (0,2))
))

and dbo.GetAgeOnSpecifiedDate(DateOfBirth,AdmissionDate)>64)


ORDER BY episode.FacilityID,spell.SourceSpellNo,episode.EpisodeStartDateTime