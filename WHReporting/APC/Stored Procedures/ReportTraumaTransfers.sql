﻿/*
--Author: Helen Ransome
--Date created: 25/08/2015
--Stored Procedure Built for SSRS Report on:
--Cancer Inter Trust Transfer Report
*/

CREATE Procedure [APC].[ReportTraumaTransfers]

 @StartDate as date
 ,@EndDate as date

as





SELECT  

--S.EncounterRecno
      
--      ,[SourceSpellNo]
   
   pat.PatientForename 
   ,pat.PatientSurname 
      ,[FaciltyID]
      ,pat.[NHSNo] 
      --,[AdmissionDate]
      --,[AdmissionTime]
      ,[AdmissionDateTime]
      --,[DischargeDate]
      --,[DischargeTime]
      ,[DischargeDateTime]
      --,[AdmissionConsultantCode]
      ,[AdmissionConsultantName]
      ,[AdmissionDivision]
      ,[AdmissionDirectorate]
      --,[AdmissionSpecialtyCode]
      --,[AdmissionSpecialty]
      ,[AdmissionSpecialtyCode(Function)]
      ,[AdmissionSpecialty(Function)]
      --,[ADmissionSpecialtyCode(Main)]
      --,[AdmissionSpecialty(Main)]
      --,[AdmissionSpecialtyCode(RTT)]
      ,[AdmissiontWardCode]
      ,[AdmissionWard]
      ,[AdmissionMethodCode]
      ,[AdmissionMethod]
      --,[AdmissionMethodNHSCode]
      --,[AdmissionSourceCode]
      ,[AdmissionSource]
      --,[AdmissionSourceNHSCode]
      --,[IntendedManagementCode]
      ,[IntendedManagement]
      --,[IntendedManagementNHSCode]
    
      ,[InpatientStayCode]
      ,[DischargeWardCode]
      ,[DischargeWard]
      --,[DischargeMethodCode]
      ,[DischargeMethod]
      --,[DischargeMethodNHSCode]
      --,[DischargeDestinationCode]
      ,[DischargeDestination]
      --,[DischargeDestinationNHSCode]
      ,[LengthOfSpell]
      --,[GPCode]
      --,[GP]
      --,[PracticeCode]
      --,[Practice]
      --,[PCTCode]
      --,[PCT]
      --,[SpellCCGCode]
      --,[SpellCCG]
      --,[SpellPCTCCGCode]
      --,[SpellPCTCCG]
      --,[PurchaserCode]
      --,[Purchaser]
      --,[ResponsibleCommissionerCode]
      --,[ResponsibleCommissioner]
      --,[ResponsibleCommissionerCCGOnlyCode]
      --,[ResponsibleCommissionerCCGOnly]
      --,[TheatrePatientBookingKey]
      --,[TheatreCode]
      --,[Theatre]
      ,[AgeCode]
      ,[AdmissionType]
      --,P.ProcedureCode 
      ,P.[Procedure]
      
  FROM [WHREPORTING].[APC].[Spell] as S
  
  left join [WHREPORTING].[APC].[AllProcedures] as P
  on P.EncounterRecno = S.EncounterRecno 
  and P.ProcedureSequence = '1'
  
  left join [WHREPORTING].APC.Patient as pat
  on pat.EncounterRecno = s.EncounterRecno   
  
  
  where AdmissionDate between @StartDate and @EndDate
  
  and s.AdmissionMethodCode = '81'
  
  and s.AdmissionWard not in ('CORONARY CARE UNIT','F10','POAU','RADIOLOGY','BURNS CENTRE','CATHETER LAB RECOVERY','NEONATAL BABY UNIT','C2','PULMONARY ONCOLOGY UNIT')
  
  --and s.[AdmissionSpecialtyCode(Function)] = '110'
  --and S.[AdmissionSpecialtyCode(Function)] = '160'
  --and S.AdmissionConsultantCode = 'C4206563'
  --and P.ProcedureCode = 'B393'
  ORDER BY AdmissionDateTime