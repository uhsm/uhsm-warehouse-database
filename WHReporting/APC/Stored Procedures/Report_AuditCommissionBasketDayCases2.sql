﻿-- =============================================
-- Author:		JPotluri
-- Create date: 17/03/2015
-- Description:	This Improved report for Audit Commission Basket Day Cases
-- =============================================
CREATE PROCEDURE [APC].[Report_AuditCommissionBasketDayCases2] 
	-- Add the parameters for the stored procedure here

@DateFrom Datetime, @DateTo Datetime, @Spec Varchar (max), @Consul Varchar (max), @BasketType Varchar (max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	/******************************************************
First Step is to create a temp table with the base data and also additional fields to be updated later with the exclusions logic
*******************************************************/
IF OBJECT_ID('tempdb..#Base') > 0
	DROP TABLE #Base

CREATE TABLE #Base (
	EpisodeUniqueID INT
	,FacilityID VARCHAR(20)
	,AdmissionDateTime DATETIME
	,DischargeDateTime DATETIME
	,DischargeDate DATETIME
	,EpisodeStartDateTime DATETIME
	,EpisodeEndDateTime DATETIME
	,SourceSpellNo INT
	,SpecialtyCode VARCHAR(20)
	,ConsultantCode VARCHAR(20)
	,PatientClass VARCHAR(20)
	,PrimaryProcedureCode VARCHAR(10)
	,PrimaryProcedureCodeGroup VARCHAR(10)
	,SecondaryProcedureCode1 VARCHAR(10)
	,SecondaryProcedureCode1Group VARCHAR(10)
	,CombinedSecondaryProcedures VARCHAR(250)
	,CombinedDiagnoses VARCHAR(250)
	,DaycaseFlag INT --derived flag fields to be used in the final query
	,ElectiveFlag INT --derived flag fields to be used in the final query
	,Exclude INT --update this field after the intial insert of base data into the table
	)

/******************************************************
Insert all fields we need into the temp table with all the episodes for the period selected in the SSRS reports parameters
This will give us base data to work with and update 
*******************************************************/
INSERT INTO #Base (
	EpisodeUniqueID
	,FacilityID
	,AdmissionDateTime
	,DischargeDateTime
	,DischargeDate 
	,EpisodeStartDateTime
	,EpisodeEndDateTime
	,SourceSpellNo
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,PrimaryProcedureCode
	,PrimaryProcedureCodeGroup
	,SecondaryProcedureCode1
	,SecondaryProcedureCode1Group
	,CombinedSecondaryProcedures
	,CombinedDiagnoses
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	)
--?? are we still deriving Daycase and Elective in the same way as old report
SELECT DISTINCT EpisodeUniqueID = ep.EpisodeUniqueID
	,FacilityID = ep.FacilityID
	,AdmissionDateTime = ep.AdmissionDateTime
	,ep.DischargeDateTime
	,ep.DischargeDate
	,EpisodeStartDateTime = ep.EpisodeStartDateTime
	,EpisodeEndDateTime = ep.EpisodeEndDateTime
	,SourceSpellNo = ep.SourceSpellNo
	--,opcs.EpisodeSourceUniqueID 
	,SpecialtyCode = ep.SpecialtyCode
	,ConsultantCode = ep.ConsultantCode
	,PatientClass = sp.PatientClass
	,PrimaryProcedureCode = cc.PriamryProcedureCode
	,PrimaryProcedureCodeGroup = LEFT(cc.PriamryProcedureCode, 3)
	,SecondaryProcedureCode1 = cc.SecondaryProcedureCode1
	,SecondaryProcedureCode1Group = LEFT(cc.SecondaryProcedureCode1, 3)
	,CombinedSecondaryProcedures = ISNULL(cc.SecondaryProcedureCode1, '') + ',' + ISNULL(cc.SecondaryProcedureCode2, '') + ',' + ISNULL(cc.SecondaryProcedureCode3, '') + ',' + ISNULL(cc.SecondaryProcedureCode4, '') + ',' + ISNULL(cc.SecondaryProcedureCode5, '') + ',' + ISNULL(cc.SecondaryProcedureCode6, '') + ',' + ISNULL(cc.SecondaryProcedureCode7, '') + ',' + ISNULL(cc.SecondaryProcedureCode8, '') + ',' + ISNULL(cc.SecondaryProcedureCode9, '') + ',' + ISNULL(cc.SecondaryProcedureCode10, '') + ',' + ISNULL(cc.SecondaryProcedureCode11, '')
	,CombinedDiagnoses = ISNULL(cc.PrimaryDiagnosisCode, '') + ',' + ISNULL(cc.SubsidiaryDiagnosisCode, '') + ',' + ISNULL(cc.SecondaryDiagnosisCode1, '') + ',' + ISNULL(cc.SecondaryDiagnosisCode2, '') + ',' + ISNULL(cc.SecondaryDiagnosisCode3, '') + ',' + ISNULL(cc.SecondaryDiagnosisCode4, '') + ',' + ISNULL(cc.SecondaryDiagnosisCode5, '') + ',' + ISNULL(cc.SecondaryDiagnosisCode6, '') + ',' + ISNULL(cc.SecondaryDiagnosisCode7, '') + ',' + ISNULL(cc.SecondaryDiagnosisCode8, '') + ',' + ISNULL(cc.SecondaryDiagnosisCode9, '') + ',' + ISNULL(cc.SecondaryDiagnosisCode10, '') + ',' + ISNULL(cc.SecondaryDiagnosisCode11, '') + ',' + ISNULL(cc.SecondaryDiagnosisCode12, '') + ',' + ISNULL(cc.SecondaryDiagnosisCode13, '') + ',' + ISNULL(cc.SecondaryDiagnosisCode14, '') + ',' + ISNULL(cc.SecondaryDiagnosisCode15, '') + ',' + ISNULL(cc.SecondaryDiagnosisCode16, '') + ',' + ISNULL(cc.SecondaryDiagnosisCode17, '') + ',' + ISNULL(cc.SecondaryDiagnosisCode18, '') + ',' + ISNULL(cc.SecondaryDiagnosisCode19, '') + ',' + ISNULL(cc.SecondaryDiagnosisCode20, '')
	,DayCaseFlag = CASE 
		WHEN sp.PatientClass = 'Day Case'
			THEN 1
		ELSE 0
		END
	,ElectiveFlag = CASE 
		WHEN sp.PatientClass = 'Elective Inpatient'
			THEN 1
		ELSE 0
		END
	,Exclude = 0
FROM WHREPORTING.APC.Episode ep
LEFT OUTER JOIN WHREPORTING.APC.Spell sp ON ep.SourceSpellNo = sp.SourceSpellNo
LEFT OUTER JOIN WHREPORTING.APC.AllProcedures opcs ON ep.EpisodeUniqueID = opcs.EpisodeSourceUniqueID
LEFT OUTER JOIN WHReporting.APC.AllDiagnosis icd ON ep.EpisodeUniqueID = icd.EpisodeSourceUniqueID
LEFT OUTER JOIN WHREPORTING.APC.ClinicalCoding cc ON ep.EpisodeUniqueID = cc.SourceUniqueID
WHERE ep.DischargeDateTime >= @DateFrom
	AND ep.DischargeDateTime < @DateTo
	AND PatientClass <> 'Non Elective'
ORDER BY ep.SourceSpellNo




/*****************************************************
Update the #base temp table with all Exclusions
******************************************************/
--Transurethral Resection of Bladder Exclusion
UPDATE #Base
SET Exclude = 1
FROM #Base
WHERE PrimaryProcedureCodeGroup = 'M42'
	AND CombinedSecondaryProcedures LIKE '%M65%'

--Arthroscopy Exclusion
UPDATE #Base
SET Exclude = 1
FROM #Base
WHERE PrimaryProcedureCodeGroup IN ('W82','W83','W84','W85','W86','W87','W88')
	AND (
		CombinedSecondaryProcedures LIKE '%W74%'
		OR CombinedSecondaryProcedures LIKE '%W70%'
		OR CombinedSecondaryProcedures LIKE '%W283%'
		OR CombinedSecondaryProcedures LIKE '%W783%'
		OR CombinedSecondaryProcedures LIKE '%W69%'
		OR CombinedSecondaryProcedures LIKE '%W085%'
		)

--Myringotomy Exclusions
UPDATE #Base
SET Exclude = 1
FROM #Base
WHERE PrimaryProcedureCodeGroup IN ('D15')
	AND (
		CombinedSecondaryProcedures LIKE '%E081%'
		OR CombinedSecondaryProcedures LIKE '%E201%'
		OR CombinedSecondaryProcedures LIKE '%F291%'
		OR CombinedSecondaryProcedures LIKE '%F34%'
		OR CombinedSecondaryProcedures LIKE '%D191%'
		)

--Sub Mucous REsection Exclusions
UPDATE #Base
SET Exclude = 1
FROM #Base
WHERE PrimaryProcedureCode IN ('E031','E036','E041','E046')
	AND (
		CombinedSecondaryProcedures LIKE '%E081%'
		OR CombinedSecondaryProcedures LIKE '%E201%'
		OR CombinedSecondaryProcedures LIKE '%F291%'
		OR CombinedSecondaryProcedures LIKE '%F34%'
		OR CombinedSecondaryProcedures LIKE '%E02%'
		OR CombinedSecondaryProcedures LIKE '%E142%'
		OR CombinedSecondaryProcedures LIKE '%F328%'
		)



/********************************************************
Work From the data in the #Base table to create final temp table to be used in the Select query which returns the data for the SSRS report
*********************************************************/
--Create #Final Table
IF OBJECT_ID('tempdb..#Final') > 0
	DROP TABLE #Final

CREATE TABLE #Final (
	EpisodeUniqueID INT
	,DischargeDate DATETIME
	,SpecialtyCode VARCHAR(20)
	,ConsultantCode VARCHAR(20)
	,PatientClass VARCHAR(20)
	,DaycaseFlag INT
	,ElectiveFlag INT
	,Exclude INT
	,BasketType VARCHAR(250)
	)

--Insert Orchidopexy records
INSERT INTO #Final
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Orchidopexy'
FROM #Base
WHERE Exclude <> 1
	AND PrimaryProcedureCodeGroup IN ('N08','N09')
	AND PrimaryProcedureCode NOT IN ('N081','N091')


--Insert Circumcision records
INSERT INTO #Final
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Circumcision'
FROM #Base
WHERE Exclude <> 1
	AND PrimaryProcedureCode IN ('N303','N304')



--Insert Inguinal Hernia records
INSERT INTO #Final
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Inguinal Hernia'
FROM #Base
WHERE Exclude <> 1
	AND (
		PrimaryProcedureCode IN ('T211','T212','T213','T218','T219')
		OR PrimaryProcedureCodeGroup IN ('T19','T20')
		)

--Insert Excision Of Breast Lump records
INSERT INTO #Final
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Excision Of Breast Lump'
FROM #Base
WHERE Exclude <> 1
	AND PrimaryProcedureCode IN ('B283')



--Insert Anal Fissure Dilation or Excision records
INSERT INTO #Final
--these are the records where the primary code is one of the definition codes
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Anal Fissure Dilation or Excision'
FROM #Base
WHERE Exclude <> 1
	AND (
		PrimaryProcedureCode IN ('H562','H564')
		OR PrimaryProcedureCodeGroup IN ('H50','H54')
		)
	AND CombinedDiagnoses LIKE '%K60%'

UNION ALL

--these are the records where the second code is one of the definition codes and the primary code is one of the combinations
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Anal Fissure Dilation or Excision'
FROM #Base
WHERE Exclude <> 1
	AND (
		SecondaryProcedureCode1 IN ('H562','H564')
		OR SecondaryProcedureCode1Group IN ('H50','H54')
		)
	AND (
		PrimaryProcedureCodeGroup IN ('H25','H28','H48','H52')
		OR PrimaryProcedureCode IN ('H443','H444','H412')
		)
	
	AND CombinedDiagnoses LIKE '%K60%'


--Insert Haemorrhoidectomy records
INSERT INTO #Final
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Haemorrhoidectomy'
FROM #Base
WHERE Exclude <> 1
	AND PrimaryProcedureCode IN ('H511')



--Insert Lap cholecystectomy records
INSERT INTO #Final
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Lap cholecystectomy'
FROM #Base
WHERE Exclude <> 1
	AND PrimaryProcedureCode IN ('J183')
	AND CombinedSecondaryProcedures like '%Y508%'
	
--Insert Varicose vein stripping or ligation
INSERT INTO #Final
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Varicose vein stripping or ligation'
FROM #Base
WHERE Exclude <> 1
	AND PrimaryProcedureCodeGroup IN ('L85','L87')
	
--Insert Transurethral resectionof bladder tumour
INSERT INTO #Final
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Transurethral resection of bladder tumour'
FROM #Base
WHERE Exclude <> 1
	AND PrimaryProcedureCodeGroup IN ('M42')
	
	
--Insert Excision of Dupuytren's contracture
INSERT INTO #Final
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Excision of Dupuytrens contracture'
FROM #Base
WHERE Exclude <> 1
	AND PrimaryProcedureCode IN ('T521','T522','T541')
	
--Insert Carpal tunnel decompression
INSERT INTO #Final
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Carpal tunnel decompression'
FROM #Base
WHERE Exclude <> 1
	AND PrimaryProcedureCode IN ('A651')	
	
	
--Insert Excision of ganglion
INSERT INTO #Final
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Excision of ganglion'
FROM #Base
WHERE Exclude <> 1
	AND PrimaryProcedureCodeGroup IN ('T59','T60')
	
--Insert Arthroscopy
INSERT INTO #Final
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Arthroscopy'
FROM #Base
WHERE Exclude <> 1
	AND PrimaryProcedureCodeGroup IN ('W82','W83','W84','W85','W86','W87','W88')
	
	
--Insert Bunion operations
INSERT INTO #Final
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Bunion operations'
FROM #Base
WHERE Exclude <> 1
	
	AND 
	(
	PrimaryProcedureCodeGroup IN ('W79','W59')
	OR PrimaryProcedureCode IN ('W151','W152','W153')	
	)

--Insert Removal of metalware
INSERT INTO #Final
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Removal of metalware'
FROM #Base
WHERE Exclude <> 1
	
	AND PrimaryProcedureCode IN ('W283')
	
/*	
--Insert Extraction of cataract with/without implant
INSERT INTO #Final

SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Extraction of cataract with/without implant'
FROM #Base
WHERE Exclude <> 1
	AND 
		PrimaryProcedureCodeGroup IN ('C71','C72','C73','C74','C75','C77')
	
		
	AND (
	     CombinedDiagnoses LIKE '%H25%'
	     OR CombinedDiagnoses LIKE '%H26%'
         OR CombinedDiagnoses LIKE '%H28%'
         OR CombinedDiagnoses LIKE '%Q120%'
         )
*/

--Insert Correction of squint
INSERT INTO #Final
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Correction of squint'
FROM #Base
WHERE Exclude <> 1
	
	AND PrimaryProcedureCodeGroup IN ('C31','C32','C33','C34','C35')
	
--Insert Myringotomy with/without grommets
INSERT INTO #Final
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Myringotomy with/without grommets'
FROM #Base
WHERE Exclude <> 1
	
	AND PrimaryProcedureCodeGroup IN ('D15')
	
	
--Insert Tonsillectomy
INSERT INTO #Final
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Tonsillectomy'
FROM #Base
WHERE Exclude <> 1
	
	AND PrimaryProcedureCode IN ('F341','F342','F343','F344')
	
	
--Insert Sub mucous resection
INSERT INTO #Final
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Sub mucous resection'
FROM #Base
WHERE Exclude <> 1
	
	AND PrimaryProcedureCode IN ('E031','E036','E041','E046')
	
--Insert Reduction of nasal fracture
INSERT INTO #Final
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Reduction of nasal fracture'
FROM #Base
WHERE Exclude <> 1
	
	AND PrimaryProcedureCode IN ('V091','V092')	
	
	
--Insert Operation for bat ears
INSERT INTO #Final
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Operation for bat ears'
FROM #Base
WHERE Exclude <> 1
	
	AND PrimaryProcedureCode IN ('D033')
	AND CombinedDiagnoses LIKE '%Q175%'
	
--Insert Dilation and curettage hysteroscopy
INSERT INTO #Final	
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Dilation and curettage hysteroscopy'
FROM #Base
WHERE Exclude <> 1
	AND (
		PrimaryProcedureCode IN ('Q103')
		OR PrimaryProcedureCodeGroup IN ('Q18')
		)
	AND CombinedDiagnoses Not LIKE '%O04%'
	
	Union all
	SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Dilation and curettage hysteroscopy'
FROM #Base
WHERE Exclude <> 1
	and
	(SecondaryProcedureCode1 IN ('Q103')
	OR SecondaryProcedureCode1Group IN ('Q18'))
	
	AND (
		PrimaryProcedureCode IN ('P313','Q013','Q413')
		OR PrimaryProcedureCodeGroup IN ('Q02','Q03')
		)
	
	AND CombinedDiagnoses Not LIKE '%O04%'
	

--Insert Laparoscopy
INSERT INTO #Final
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Laparoscopy'
FROM #Base
WHERE Exclude <> 1
	
	AND 
	
	PrimaryProcedureCodeGroup IN ('Q17','Q35','Q36','Q38','Q39','Q50','T43','T42','Q49')
	
Union all
	SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Laparoscopy'
FROM #Base
WHERE Exclude <> 1	
	
	AND (
		PrimaryProcedureCode IN ('P313','Q013','Q413')
		OR PrimaryProcedureCodeGroup IN ('Q02','Q03')
		)
	AND SecondaryProcedureCode1Group IN ('Q17','Q35','Q36','Q38','Q39','Q50','T43','T42','Q49')


--Insert Termination of pregnancy
INSERT INTO #Final	
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Termination of pregnancy'
FROM #Base
WHERE Exclude <> 1
	AND 
		PrimaryProcedureCode IN ('Q101','Q102','Q111','Q112','Q113')
		
	AND 
	     CombinedDiagnoses LIKE ('%O049%')
Union all
SELECT EpisodeUniqueID
	,DischargeDate
	,SpecialtyCode
	,ConsultantCode
	,PatientClass
	,DaycaseFlag
	,ElectiveFlag
	,Exclude
	,BasketType = 'Termination of pregnancy'
FROM #Base
WHERE Exclude <> 1

and PrimaryProcedureCodeGroup IN ('Q14')
and SecondaryProcedureCode1 IN ('Q101','Q102','Q111','Q112','Q113')

AND 
	     CombinedDiagnoses LIKE ('%O049%')


/******************************************
Final SELECT statement for output to SSRS reprot
********************************************/
Select cal.TheMonth
		,cal.FinancialMonthKey
		,#Final.PatientClass
		,#Final.BasketType
		,COUNT(distinct #Final.EpisodeUniqueID) as Total
		,SUM(#Final.DaycaseFlag) as Daycase
		,SUM(#Final.ElectiveFlag) as Elective
from #Final
Left outer join WHREPORTING.LK.Calendar cal on #Final.DischargeDate = cal.TheDate

where [SpecialtyCode] in (SELECT Item
                        FROM   dbo.Split (@Spec, ','))
AND 
[ConsultantCode] in (SELECT Item
                      FROM   dbo.Split (@Consul, ','))

AND BasketType in (SELECT Item
                        FROM   dbo.Split (@BasketType, ','))
Group by cal.TheMonth
		,cal.FinancialMonthKey
		,#Final.PatientClass
		,#Final.BasketType
END