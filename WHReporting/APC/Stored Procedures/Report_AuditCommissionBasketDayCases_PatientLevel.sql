﻿-- =============================================
-- Author:		JPotluri
-- Create date: 28/02/2014
-- Description:	This report for Audit Commission Basket Day Cases at Patient Level
--				Log Number : RD408

-- =============================================
/*
Created Modified Date		Modified By							Comments
---------------------		------------------------		------------------




*/
CREATE PROCEDURE [APC].[Report_AuditCommissionBasketDayCases_PatientLevel] @DateFrom Datetime, @DateTo Datetime, @Spec Varchar (4000), @Patclass Varchar (25), @Consul Varchar (4000), @BasketType Varchar (max),@Month Varchar (20)


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Declare @DateFrom as Datetime
--Declare @DateTo as Datetime


--Set		@DateFrom	=	'01 Apr 2013'			--Amend if Necessary to the Start of the Reporting Period
--Set		@DateTo		=	'31 Jan 2014 23:59:59'	



SELECT 
      S.EncounterRecno
       ,P.FacilityID
      ,P.PatientSurname      
      ,S.AdmissionDateTime
      ,S.DischargeDateTime
      ,DATEDIFF(HOUR,S.AdmissionDateTime,S.DischargeDateTime) as LosHours
      ,month(S.DischargeDate) as DisMonth
      ,year(S.DischargeDate) as DisYear
      ,Cal.FinancialMonthKey
      ,Cal.TheMonth
      ,S.IntendedManagement
      ,S.PatientClass
      ,S.InpatientStayCode
      ,S.LengthOfSpell
      ,CC.PriamryProcedure
      ,CC.SecondaryProcedure1
      ,CC.SecondaryProcedure2
      ,S.AdmissionConsultantName
	  ,S.AdmissionSpecialty
	,Case 
			WHEN left(cc.PriamryProcedure,3) in ('N08','N09') THEN 'Orchidopexy'
			WHEN left(cc.PriamryProcedure,3) in ('L85','L87') THEN 'Varicose Vein stripping or ligation'
			WHEN left(cc.PriamryProcedure,3) in ('M42') THEN 'TURBT'
			WHEN left(cc.PriamryProcedure,3) in ('T59','T60') THEN 'Excision of ganglion'
			WHEN left(cc.PriamryProcedure,3) in ('W82','W83','W84','W85','W86','W87','W88') THEN 'Arthroscopy'
			WHEN left(cc.PriamryProcedure,3) in ('D15') THEN 'Myringotomy with/without grommets'
			WHEN left(cc.PriamryProcedure,3) in ('Q17','Q35','Q36','Q38','Q39','Q50','Q49','T43','T42') THEN 'Laparoscopy'
			WHEN left(cc.PriamryProcedure,3) in ('Q18') then 'Dilation and curettage/hysteroscopy'
			WHEN left(cc.PriamryProcedure,3) in ('W79','W59') then 'Bunion operations'
			WHEN left(cc.PriamryProcedure,3) in ('H50','H54') then 'Anal fissure dilation or excision'
			WHEN left(cc.PriamryProcedure,3) in ('T19','T20') then 'Inguinal Hernia'
			
			WHEN left(cc.PriamryProcedure,4) in ('N303','N304') then 'Circumcision'
			WHEN left(cc.PriamryProcedure,4) in ('B283') then 'Excision of breast lump'
			WHEN left(cc.PriamryProcedure,4) in ('H511') then 'Haemorrhoidectomy'
			WHEN left(cc.PriamryProcedure,4) in ('T521','T522','T541') then 'Excision of Dupuytrens contracture'
			WHEN left(cc.PriamryProcedure,4) in ('A651') then 'Carpal tunnel decompression'
			WHEN left(cc.PriamryProcedure,4) in ('W283') then 'Removal of metalware'
			WHEN left(cc.PriamryProcedure,4) in ('J183') then 'Lap cholecystectomy'
			WHEN left(cc.PriamryProcedure,4) in ('F341','F342','F343','F344') then 'Tonsillectomy'
			WHEN left(cc.PriamryProcedure,4) in ('V091','V092') then 'Reduction of nasal fracture'
			WHEN left(cc.PriamryProcedure,4) in ('D033') then 'Operation for bat ears'
			WHEN left(cc.PriamryProcedure,4) in ('Q101','Q102','Q111','Q112','Q113') then 'Termination of Pregnancy'
			WHEN left(cc.PriamryProcedure,4) in ('E031','E036','E041','E046') then 'Sub mucous resection'
			WHEN left(cc.PriamryProcedure,4) in ('Q103') then 'Dilation and curettage/hsteroscopy'
			WHEN left(cc.PriamryProcedure,4) in ('W151','W152','W153') then 'Bunion operations'
			WHEN left(cc.PriamryProcedure,4) in ('H562','H564') then 'Anal fissure dilation or excision'
			WHEN left(cc.PriamryProcedure,4) in ('T211','T212','T213','T218','T219') then 'Inguinal Hernia'
			
			else null
	end as [BasketType]
	
   
 FROM WHREPORTING.APC.Spell S
		
	left join WHREPORTING.APC.ClinicalCoding cc
	on S.EncounterRecno = cc.EncounterRecno
		
	left join [WHREPORTING].[LK].[Calendar] Cal
	on Cal.TheDate = S.DischargeDate
	
	Left Join APC.Patient P
	ON S.EncounterRecno = P.EncounterRecno
	
  WHERE S.PatientClass in ('Day Case','Elective Inpatient')
	AND S.DischargeDate between @DateFrom and @DateTo
	and 
	(S.PatientClass in (SELECT Item
                         FROM   dbo.Split (isnull(@Patclass,S.PatientClass), ',')) or @Patclass is NULL)
	AND [AdmissionSpecialtyCode] in (SELECT Item
                         FROM   dbo.Split (@Spec, ','))
	AND [AdmissionConsultantCode] in (SELECT Item
                         FROM   dbo.Split (@Consul, ','))
    AND Cal.TheMonth = isnull(@Month, Cal.TheMonth)
	AND (Case 
			WHEN left(cc.PriamryProcedure,3) in ('N08','N09') THEN 'Orchidopexy'
			WHEN left(cc.PriamryProcedure,3) in ('L85','L87') THEN 'Varicose Vein stripping or ligation'
			WHEN left(cc.PriamryProcedure,3) in ('M42') THEN 'TURBT'
			WHEN left(cc.PriamryProcedure,3) in ('T59','T60') THEN 'Excision of ganglion'
			WHEN left(cc.PriamryProcedure,3) in ('W82','W83','W84','W85','W86','W87','W88') THEN 'Arthroscopy'
			WHEN left(cc.PriamryProcedure,3) in ('D15') THEN 'Myringotomy with/without grommets'
			WHEN left(cc.PriamryProcedure,3) in ('Q17','Q35','Q36','Q38','Q39','Q50','Q49','T43','T42') THEN 'Laparoscopy'
			WHEN left(cc.PriamryProcedure,3) in ('Q18') then 'Dilation and curettage/hysteroscopy'
			WHEN left(cc.PriamryProcedure,3) in ('W79','W59') then 'Bunion operations'
			WHEN left(cc.PriamryProcedure,3) in ('H50','H54') then 'Anal fissure dilation or excision'
			WHEN left(cc.PriamryProcedure,3) in ('T19','T20') then 'Inguinal Hernia'
			
			WHEN left(cc.PriamryProcedure,4) in ('N303','N304') then 'Circumcision'
			WHEN left(cc.PriamryProcedure,4) in ('B283') then 'Excision of breast lump'
			WHEN left(cc.PriamryProcedure,4) in ('H511') then 'Haemorrhoidectomy'
			WHEN left(cc.PriamryProcedure,4) in ('T521','T522','T541') then 'Excision of Dupuytrens contracture'
			WHEN left(cc.PriamryProcedure,4) in ('A651') then 'Carpal tunnel decompression'
			WHEN left(cc.PriamryProcedure,4) in ('W283') then 'Removal of metalware'
			WHEN left(cc.PriamryProcedure,4) in ('J183') then 'Lap cholecystectomy'
			WHEN left(cc.PriamryProcedure,4) in ('F341','F342','F343','F344') then 'Tonsillectomy'
			WHEN left(cc.PriamryProcedure,4) in ('V091','V092') then 'Reduction of nasal fracture'
			WHEN left(cc.PriamryProcedure,4) in ('D033') then 'Operation for bat ears'
			WHEN left(cc.PriamryProcedure,4) in ('Q101','Q102','Q111','Q112','Q113') then 'Termination of Pregnancy'
			WHEN left(cc.PriamryProcedure,4) in ('E031','E036','E041','E046') then 'Sub mucous resection'
			WHEN left(cc.PriamryProcedure,4) in ('Q103') then 'Dilation and curettage/hsteroscopy'
			WHEN left(cc.PriamryProcedure,4) in ('W151','W152','W153') then 'Bunion operations'
			WHEN left(cc.PriamryProcedure,4) in ('H562','H564') then 'Anal fissure dilation or excision'
			WHEN left(cc.PriamryProcedure,4) in ('T211','T212','T213','T218','T219') then 'Inguinal Hernia'
			
			else null
	end in (SELECT Item
                         FROM   dbo.Split (@BasketType, ',')) or 
                         Case 
			WHEN left(cc.PriamryProcedure,3) in ('N08','N09') THEN 'Orchidopexy'
			WHEN left(cc.PriamryProcedure,3) in ('L85','L87') THEN 'Varicose Vein stripping or ligation'
			WHEN left(cc.PriamryProcedure,3) in ('M42') THEN 'TURBT'
			WHEN left(cc.PriamryProcedure,3) in ('T59','T60') THEN 'Excision of ganglion'
			WHEN left(cc.PriamryProcedure,3) in ('W82','W83','W84','W85','W86','W87','W88') THEN 'Arthroscopy'
			WHEN left(cc.PriamryProcedure,3) in ('D15') THEN 'Myringotomy with/without grommets'
			WHEN left(cc.PriamryProcedure,3) in ('Q17','Q35','Q36','Q38','Q39','Q50','Q49','T43','T42') THEN 'Laparoscopy'
			WHEN left(cc.PriamryProcedure,3) in ('Q18') then 'Dilation and curettage/hysteroscopy'
			WHEN left(cc.PriamryProcedure,3) in ('W79','W59') then 'Bunion operations'
			WHEN left(cc.PriamryProcedure,3) in ('H50','H54') then 'Anal fissure dilation or excision'
			WHEN left(cc.PriamryProcedure,3) in ('T19','T20') then 'Inguinal Hernia'
			
			WHEN left(cc.PriamryProcedure,4) in ('N303','N304') then 'Circumcision'
			WHEN left(cc.PriamryProcedure,4) in ('B283') then 'Excision of breast lump'
			WHEN left(cc.PriamryProcedure,4) in ('H511') then 'Haemorrhoidectomy'
			WHEN left(cc.PriamryProcedure,4) in ('T521','T522','T541') then 'Excision of Dupuytrens contracture'
			WHEN left(cc.PriamryProcedure,4) in ('A651') then 'Carpal tunnel decompression'
			WHEN left(cc.PriamryProcedure,4) in ('W283') then 'Removal of metalware'
			WHEN left(cc.PriamryProcedure,4) in ('J183') then 'Lap cholecystectomy'
			WHEN left(cc.PriamryProcedure,4) in ('F341','F342','F343','F344') then 'Tonsillectomy'
			WHEN left(cc.PriamryProcedure,4) in ('V091','V092') then 'Reduction of nasal fracture'
			WHEN left(cc.PriamryProcedure,4) in ('D033') then 'Operation for bat ears'
			WHEN left(cc.PriamryProcedure,4) in ('Q101','Q102','Q111','Q112','Q113') then 'Termination of Pregnancy'
			WHEN left(cc.PriamryProcedure,4) in ('E031','E036','E041','E046') then 'Sub mucous resection'
			WHEN left(cc.PriamryProcedure,4) in ('Q103') then 'Dilation and curettage/hsteroscopy'
			WHEN left(cc.PriamryProcedure,4) in ('W151','W152','W153') then 'Bunion operations'
			WHEN left(cc.PriamryProcedure,4) in ('H562','H564') then 'Anal fissure dilation or excision'
			WHEN left(cc.PriamryProcedure,4) in ('T211','T212','T213','T218','T219') then 'Inguinal Hernia'
			
			else null
	end is null)
	and
(
		left(cc.[PriamryProcedure],3) in ('N08','N09','T19','T20','L85','L87','M42','T59','T60','W82','W83',
		'W84','W85','W86','W87','W88','W79','W59','D15') 
	  OR 
		left(cc.[PriamryProcedure],4) in ('N303','N304','T211','T212','T213','T218','T219',
		'B283','H511','T521','T522','T541','A651','W151','W152','W153','W283','F341','F342','F343','F344',
		'E031','E036','E041''E046','V091','V092','D033')
	  OR 
		left(cc.[PriamryProcedure],4) = 'J183' AND left(cc.[SecondaryProcedure1],4) = 'Y508'
	  OR
		left(cc.[PriamryProcedure],3) IN ('H50','H54')  AND left(cc.[SecondaryProcedure1],3) 
		IN( 'H25','H28','H48','H52')
	  OR
		left(cc.[PriamryProcedure],3) IN ('H50','H54')  AND Left(cc.[SecondaryProcedure1],4) 
		IN( 'H412','H443','H443','H444')
	  OR
		left(cc.[PriamryProcedure],4) IN ('H562','H564')  AND left(cc.[SecondaryProcedure1],3) 
		IN( 'H25','H28','H48','H52')
	  OR
		left(cc.[PriamryProcedure],4) IN ('H562','H564')  AND Left(cc.[SecondaryProcedure1],4) 
		IN( 'H412','H443','H443','H444')
	  OR
		left(cc.[PriamryProcedure],3) IN ('Q18')  AND left(cc.[SecondaryProcedure1],3) 
		IN( 'Q02','Q03')
	  OR
		left(cc.[PriamryProcedure],3) IN ('Q18')  AND left(cc.[SecondaryProcedure1],4) 
		IN( 'P313','Q013','Q413')
	  OR
		left(cc.[PriamryProcedure],4) IN ('Q103')  AND left(cc.[SecondaryProcedure1],3) 
		IN( 'Q02','Q03')
	  OR
		left(cc.[PriamryProcedure],4) IN ('Q103')  AND left(cc.[SecondaryProcedure1],4) 
		IN( 'P313','Q013','Q413')
	  OR
		left(cc.[PriamryProcedure],3) IN ('Q17','Q35','Q36','Q38','Q39','Q50','T43','T42','Q49')
		AND left(cc.[SecondaryProcedure1],3) 
		IN( 'Q02','Q03')
	  OR
		left(cc.[PriamryProcedure],3) IN ('Q17','Q35','Q36','Q38','Q39','Q50','T43','T42','Q49')
		AND left(cc.[SecondaryProcedure1],4) 
		IN( 'P313','Q013','Q413')
	  OR
		left(cc.[PriamryProcedure],4) IN ('Q101','Q102','Q111','Q112','Q113')
		AND left(cc.[SecondaryProcedure1],3) 
		IN( 'Q14')		
				
	  		)
	  
	  		
	ORDER BY
	PriamryProcedure
	
	

END