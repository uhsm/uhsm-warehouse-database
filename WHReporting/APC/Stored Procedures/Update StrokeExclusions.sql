﻿/*
--Author: J Cleworth
--Date created: 30/08/2013
--Stored Procedure [APC].[Update StrokeExclusions]
--
*/


CREATE Procedure [APC].[Update StrokeExclusions]

@SourceSpellNo int
     ,@Exclude varchar (1)
      ,@Comments varchar(250)

as
	if exists (select 1 from [WHREPORTING].[APC].[StrokeComments] where sourcespellno in (@SourceSpellNo))
	begin
update [WHREPORTING].[APC].[StrokeComments]

set Exclude=@Exclude


where sourcespellno=@SourceSpellNo


update [WHREPORTING].[APC].[StrokeComments]

set [Comments]=@Comments

where sourcespellno=@SourceSpellNo
end

if not exists (select 1 from [WHREPORTING].[APC].[StrokeComments] where sourcespellno in (@SourceSpellNo))
begin
insert into  [WHREPORTING].[APC].[StrokeComments]

(SourceSpellNo
     ,Exclude
      ,Comments)
      
    select @SourceSpellNo
     ,@Exclude
      ,@Comments
end