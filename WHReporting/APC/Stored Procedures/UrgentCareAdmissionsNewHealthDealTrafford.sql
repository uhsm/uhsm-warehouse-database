﻿/******************************************************************************
 Description: Urgent care admissions export for the New Health Deal Trafford.
              Data used by the Information Sub-Group for activity analysis. 
  
 Parameters:  @StartDate - Start date for data extract
              @EndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author      Change
 13/11/13 Tim Dean	  Created
 14/11/13 Tim Dean    Added method of admission as per v3 of data definition
                      and changed where to include all emergency NEL admissions
 06/12/13 Tim Dean    Changed the column used for the main specialty code as
                      the previous column was incorrect
******************************************************************************/

CREATE Procedure [APC].[UrgentCareAdmissionsNewHealthDealTrafford]
 @StartDate as Date
,@EndDate as Date
as
select
  d.OrganisationCode [Organisation Code (Code of Provider)]
 ,d.SiteCode [Provider Site Code]
 ,d.ResponsibleCommissionerCCGOnlyCode [Organisation Code (Code of Commissioner]
 ,d.AdmissionDate [Date of Admission]
 ,d.AdmissionMethodCode [Admission Method Code] -- added 14/11/13 TJD
 ,d.AdmissionTimeBand [Time of Admission Band]
 ,d.AdmissionSpecialtyCode [Main Specialty Code (Admitting)]
 ,d.AgeAtArrivalBand [Age at Arrival Band]
 ,count(*) [Cases]
from (
	  select
	    'RM2' OrganisationCode
	   ,'RM202' SiteCode
	   ,s.ResponsibleCommissionerCCGOnlyCode
	   ,s.AdmissionDate
	   ,s.AdmissionMethodCode -- added 14/11/13 TJD
	   ,case
		  when datepart(HH,s.AdmissionDateTime) < 8 then '00:00 to 07:59'
		  else '08:00 to 23:59'
		end AdmissionTimeBand
	   --,s.AdmissionSpecialtyCode -- removed 06/12/13 TJD
	   ,s.[ADmissionSpecialtyCode(Main)] AdmissionSpecialtyCode -- added 06/12/13 TJD
	   ,case
		  when s.Age < 5 then '00-04'
		  when s.Age >= 5 then '05+'
		end AgeAtArrivalBand
      from WHREPORTING.APC.Spell s
	  where s.AdmissionDate between @StartDate and @EndDate
	        and s.AdmissionMethodCode in ('21','22','23','24','25','28','2A','2B','2C','2D') -- added 14/11/13 TJD
--		    and s.AdmissionMethodCode = '21'   -- removed 14/11/13 TJD 
      ) d
group by
	d.OrganisationCode
	,d.SiteCode
	,d.ResponsibleCommissionerCCGOnlyCode
	,d.AdmissionDate
	,d.AdmissionMethodCode -- added 14/11/13 TJD
	,d.AdmissionTimeBand
	,d.AdmissionSpecialtyCode
	,d.AgeAtArrivalBand