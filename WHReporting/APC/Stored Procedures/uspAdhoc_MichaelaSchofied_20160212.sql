﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		For general surgery and urology consultants list the top 20 procedures
				together with the specialty.

Notes:			Stored in WHREPORTING.APC

Versions:		
				1.0.0.0 - 24/02/2016 - PP/MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [APC].[uspAdhoc_MichaelaSchofied_20160212]
As

-- Put raw data into a Common Table Expression (CTE) (Subquery)
;With OperationsCTE As (
	Select	
			src.ConsultantName,
			src.PrimaryProcedureCode,
			opcs.[OPCS Description] As OPCSDescription,
			src.[Specialty(Function)],
			COUNT(*) As Records
			
	From	APC.Episode src

			-- Be aware that this does not have a match for all OPCS codes
			-- There should be a simple lookup for OPCS codes somewhere.
			Left Join LK.OPCS_List_v2 opcs
				On src.PrimaryProcedureCode = opcs.[OPCS Code ]

	Where	src.PrimaryProcedureDateTime Between '2015-01-01' And '2016-01-01'
			And src.[SpecialtyCode(Function)] In ('100','101')

	Group By
			src.ConsultantName,
			src.PrimaryProcedureCode,
			opcs.[OPCS Description],
			src.[Specialty(Function)]
	),

	Operations2CTE As (
	Select	
		src.ConsultantName,
		src.PrimaryProcedureCode,
		src.OPCSDescription,
		src.[Specialty(Function)],
		src.Records,
		ROW_NUMBER() Over (Partition By src.ConsultantName Order By Records Desc) As RowNo
	From	OperationsCTE src
	)

Select	*
From	Operations2CTE src
Where	src.RowNo <= 20
Order By src.ConsultantName,src.RowNo
--10565