﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Extract coding data for upload to Easy Audit Software

Notes:			Stored in WHREPORTING.APC

				*** NOTE ***
				As each year passes the joins to the following 2 tables need amending
				to the relevant current year's version, or at least the version
				that has the data for the required date range:
					PbR2015.dbo.PbR
					PbR2015.HRG.HRG48APCEncounter

Versions:		
				1.0.0.1 - 24/02/2016 - MT - Job ?
					Query not running in a reasonable time.
					Mods to change this.

				1.0.0.0 - ??/??/???? - AA
					Created script.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [APC].[uspExtractCodingDataForEasyAudit2016]
	@StartDate date,		-- e.g. '2015-10-01'
	@EndDate date,			-- e.g. '2015-12-31'
	@CoderName varchar(70)	-- e.g. 'Webster Sue P'
As

-- Get the episodes that this coder has coded within the given time period
-- into a Common Table Expression (CTE)
With EpisodesCTE As (
	Select	Distinct src.EpisodeUniqueID
	From	APC.Episode src With (NoLock)

			Inner Join APC.AllDiagnosis diag With (NoLock)
				On src.EpisodeUniqueID = diag.EpisodeSourceUniqueID
				And diag.UserModified = @CoderName

	Where	src.DischargeDate Between @StartDate And @EndDate
	and (src.FacilityID <> '' or src.FacilityID is not null)
	),

	-- Get the diagnoses into a CTE as we're joining to this dataset
	-- 14 times to return all the diagnoses. Putting only
	-- the required data in a CTE is faster than querying the entire
	-- diagnosis table 14 times (WH.APC.DiagnosisCoding).
	DiagnosesCTE As (
	Select	src.EpisodeUniqueID,
			d.SequenceNo,
			d.DiagnosisCode,
			d.CodeType
	From	EpisodesCTE src With (NoLock)
	
			Inner Join WH.APC.DiagnosisCoding d With (NoLock)
				On src.EpisodeUniqueID = d.APCSourceUniqueID
	)

Select	
	PROCOET = 'RM2'
	,PURCODE = ISNULL(sp.PurchaserCode,'')
	,PROVSPNO = ISNULL(sp.SourceSpellNo,'')
	,EPIORDER = ISNULL(whep.EpisodeNo,'')
	,ADMIDATE = CONVERT(varchar,CAST(ep.AdmissionDate as date),112)
	,DISDATE = CONVERT(varchar,CAST(ep.DischargeDate as date),112)
	,EPISTART = CONVERT(varchar,CAST(ep.EpisodeStartDate as date),112)
	,EPIEND = CONVERT(varchar,CAST(ep.EpisodeEndDate as date),112)
	,STARTAGE = ISNULL(sp.Age,'')
	,SEX = ISNULL(pat.SexNHSCode,'')
	,CLASSPAT = ISNULL(pbr.PatientClassificationCode,'')
	,ADMISORC = ISNULL(sp.AdmissionSourceNHSCode,'')
	,ADMIMETH = ISNULL(sp.AdmissionMethodNHSCode,'')
	,DISDEST = ISNULL(sp.DischargeDestinationNHSCode,'')
	,DISMETH = ISNULL(sp.DischargeMethodNHSCode,'')
	,EPIDUR = ISNULL(ep.LengthOfEpisode,'')
	,MAINSPEF = ISNULL(ep.[SpecialtyCode(Main)],'')
	,NEOCARE = ISNULL(ep.NeoNatalLevelOfCare,'')
	,TRETSPEF = ISNULL(ep.[SpecialtyCode(Function)],'')
	,LEGLSTAT = ISNULL(whep.LegalStatusClassificationOnAdmissionCode,'')
	,CONSULT = ISNULL(ep.ConsultantCode,'')
	,DIAG_01 = ISNULL(d1.DiagnosisCode + ISNULL(d1.CodeType,''),'')
	,DIAG_02 = ISNULL(d2.DiagnosisCode + ISNULL(d2.CodeType,''),'')
	,DIAG_03 = ISNULL(d3.DiagnosisCode + ISNULL(d3.CodeType,''),'')
	,DIAG_04 = ISNULL(d4.DiagnosisCode + ISNULL(d4.CodeType,''),'')
	,DIAG_05 = ISNULL(d5.DiagnosisCode + ISNULL(d5.CodeType,''),'')
	,DIAG_06 = ISNULL(d6.DiagnosisCode + ISNULL(d6.CodeType,''),'')
	,DIAG_07 = ISNULL(d7.DiagnosisCode + ISNULL(d7.CodeType,''),'')
	,DIAG_08 = ISNULL(d8.DiagnosisCode + ISNULL(d8.CodeType,''),'')
	,DIAG_09 = ISNULL(d9.DiagnosisCode + ISNULL(d9.CodeType,''),'')
	,DIAG_10 = ISNULL(d10.DiagnosisCode + ISNULL(d10.CodeType,''),'')
	,DIAG_11 = ISNULL(d11.DiagnosisCode + ISNULL(d11.CodeType,''),'')
	,DIAG_12 = ISNULL(d12.DiagnosisCode + ISNULL(d12.CodeType,''),'')
	,DIAG_13 = ISNULL(d13.DiagnosisCode + ISNULL(d13.CodeType,''),'')
	,DIAG_14 = ISNULL(d14.DiagnosisCode + ISNULL(d14.CodeType,''),'')
	,OPER_01 = ISNULL(LEFT(cc.PriamryProcedureCode,4),'')
	,OPER_02 = ISNULL(LEFT(cc.SecondaryProcedure1,4),'')
	,OPER_03 = ISNULL(LEFT(cc.SecondaryProcedure2,4),'')
	,OPER_04 = ISNULL(LEFT(cc.SecondaryProcedure3,4),'')
	,OPER_05 = ISNULL(LEFT(cc.SecondaryProcedure4,4),'')
	,OPER_06 = ISNULL(LEFT(cc.SecondaryProcedure5,4),'')
	,OPER_07 = ISNULL(LEFT(cc.SecondaryProcedure6,4),'')
	,OPER_08 = ISNULL(LEFT(cc.SecondaryProcedure7,4),'')
	,OPER_09 = ISNULL(LEFT(cc.SecondaryProcedure8,4),'')
	,OPER_10 = ISNULL(LEFT(cc.SecondaryProcedure9,4),'')
	,OPER_11 = ISNULL(LEFT(cc.SecondaryProcedure10,4),'')
	,OPER_12 = ISNULL(LEFT(cc.SecondaryProcedure11,4),'')
	,PATIENTID = ISNULL(sp.FaciltyID,'')
	,FCE_HRG = ISNULL(hrg.HRGCode,'')
	,SPELL_HRG = ISNULL(pbr.HRGCode,'')
	,DOB = CONVERT(varchar,CAST(pat.DateOfBirth as date),112)
	,CCDAYS = ISNULL(pbr.CriticalCareDays,'')
	,AREA = '00' -- Must be a number
	,REHABILITATIONDAYS = ISNULL(pbr.SpellTotalRehabilitationDays,'')
	,SPCDAYS = ''
	,CoderID = @CoderName
	,cn.Location
	,cn.Notes

From	EpisodesCTE src With (NoLock)

		Inner Join APC.Episode ep With (NoLock)
			On src.EpisodeUniqueID = ep.EpisodeUniqueID

		-- Only joining to this WH version of episodes to get EpisodeNo and
		-- LegalStatusClassificationOnAdmissionCode which is not available
		-- in WHREPORTING.APC.Episode.
		Left Join WH.APC.Encounter whep With (NoLock)
			On ep.EncounterRecno = whep.EncounterRecno

		Left Join APC.Spell sp With (NoLock)
			On ep.SourceSpellNo = sp.SourceSpellNo

		Left Join APC.Patient pat With (NoLock)
			On sp.EncounterRecno = pat.EncounterRecno

		-- Get current casenote location details
		Left Join CN.CasenoteCurrentLocationDataset cn With (NoLock)
			On ep.SourcePatientNo = cn.Patnt_refno

		Left Join PbR2016.dbo.PbR pbr With (NoLock)
			On sp.SourceSpellNo = pbr.ProviderSpellNo
			And pbr.EncounterTypeCode = 'IP'
			And pbr.DatasetCode = 'ENCOUNTER'
	
		Left Join PbR2016.HRG.HRG49APCEncounter hrg With (NoLock)
			On ep.EncounterRecno = hrg.EncounterRecno

		-- Get the procedure coding
		Left Join APC.ClinicalCoding as cc With (NoLock)
			On ep.EncounterRecno = cc.EncounterRecno

		-- Get the diagnosis coding
		Left Join DiagnosesCTE d1 With (NoLock)
			On src.EpisodeUniqueID = d1.EpisodeUniqueID
			And d1.SequenceNo = 1
	
		Left Join DiagnosesCTE d2 With (NoLock)
			On src.EpisodeUniqueID = d2.EpisodeUniqueID
			And d2.SequenceNo = 2

		Left Join DiagnosesCTE d3 With (NoLock)
			On src.EpisodeUniqueID = d3.EpisodeUniqueID
			And d3.SequenceNo = 3

		Left Join DiagnosesCTE d4 With (NoLock)
			On src.EpisodeUniqueID = d4.EpisodeUniqueID
			And d4.SequenceNo = 4

		Left Join DiagnosesCTE d5 With (NoLock)
			On src.EpisodeUniqueID = d5.EpisodeUniqueID
			And d5.SequenceNo = 5

		Left Join DiagnosesCTE d6 With (NoLock)
			On src.EpisodeUniqueID = d6.EpisodeUniqueID
			And d6.SequenceNo = 6

		Left Join DiagnosesCTE d7 With (NoLock)
			On src.EpisodeUniqueID = d7.EpisodeUniqueID
			And d7.SequenceNo = 7

		Left Join DiagnosesCTE d8 With (NoLock)
			On src.EpisodeUniqueID = d8.EpisodeUniqueID
			And d8.SequenceNo = 8

		Left Join DiagnosesCTE d9 With (NoLock)
			On src.EpisodeUniqueID = d9.EpisodeUniqueID
			And d9.SequenceNo = 9

		Left Join DiagnosesCTE d10 With (NoLock)
			On src.EpisodeUniqueID = d10.EpisodeUniqueID
			And d10.SequenceNo = 10

		Left Join DiagnosesCTE d11 With (NoLock)
			On src.EpisodeUniqueID = d11.EpisodeUniqueID
			And d11.SequenceNo = 11

		Left Join DiagnosesCTE d12 With (NoLock)
			On src.EpisodeUniqueID = d12.EpisodeUniqueID
			And d12.SequenceNo = 12

		Left Join DiagnosesCTE d13 With (NoLock)
			On src.EpisodeUniqueID = d13.EpisodeUniqueID
			And d13.SequenceNo = 13

		Left Join DiagnosesCTE d14 With (NoLock)
			On src.EpisodeUniqueID = d14.EpisodeUniqueID
			And d14.SequenceNo = 14