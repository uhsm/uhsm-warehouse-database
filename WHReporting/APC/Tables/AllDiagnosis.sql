﻿CREATE TABLE [APC].[AllDiagnosis](
	[EncounterRecno] [int] NOT NULL,
	[EpisodeSourceUniqueID] [int] NOT NULL,
	[ClinicalCodingKey] [int] NOT NULL,
	[DiagnosisCode] [varchar](20) NULL,
	[Diagnosis] [varchar](255) NULL,
	[DiagnosisSequence] [int] NOT NULL,
	[DiagnosisDate] [date] NULL,
	[UserCreate] [varchar](70) NULL,
	[CreateDate] [date] NULL,
	[CreateTime] [time](0) NULL,
	[CreateDateTime] [datetime] NULL,
	[UserModified] [varchar](70) NULL,
	[ModifedDate] [date] NULL,
	[ModifiedTime] [time](0) NULL,
	[ModifiedDateTime] [datetime] NULL,
	[Counter] [int] NULL,
 CONSTRAINT [pk_APCAllDiagnosis] PRIMARY KEY CLUSTERED 
(
	[EncounterRecno] ASC,
	[DiagnosisSequence] ASC,
	[ClinicalCodingKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]