﻿CREATE TABLE [APC].[ECMODays](
	[FaciltyID] [nvarchar](255) NULL,
	[NHSNo] [nvarchar](255) NULL,
	[Spell Number] [nvarchar](255) NULL,
	[ECMO Chargeable Days] [int] NULL,
	[ECMOStart] [datetime] NULL,
	[ECMOEnd] [datetime] NULL
) ON [PRIMARY]