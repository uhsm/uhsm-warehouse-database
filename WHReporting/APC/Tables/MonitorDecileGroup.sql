﻿CREATE TABLE [APC].[MonitorDecileGroup](
	[PCD7] [varchar](10) NULL,
	[PCD8] [varchar](10) NULL,
	[LSOA11] [varchar](20) NULL,
	[LSOA01] [varchar](20) NULL,
	[IMD 2004 DECILE] [varchar](50) NULL,
	[CCG CODE] [varchar](10) NULL
) ON [PRIMARY]