﻿CREATE TABLE [APC].[MonitorPatientList](
	[NHSNO] [varchar](17) NOT NULL,
	[PAT_CLASS] [int] NULL,
	[OUTCOME_GROUP] [int] NULL,
	[SIMPLIFIED_OUTCOME_GROUP] [int] NULL,
	[TUMOUR_TYPE] [varchar](15) NULL,
	[DIAG_DATE] [datetime] NULL
) ON [PRIMARY]