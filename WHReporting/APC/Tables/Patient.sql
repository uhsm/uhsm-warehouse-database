﻿CREATE TABLE [APC].[Patient](
	[EncounterRecno] [int] NOT NULL,
	[SourcePatientNo] [int] NULL,
	[EpisodeUniqueID] [int] NULL,
	[FacilityID] [varchar](20) NULL,
	[NHSNo] [varchar](20) NULL,
	[NHSNoStatusCode] [varchar](20) NULL,
	[NHSNoStatus] [varchar](80) NULL,
	[NHSNoStatusNHSCode] [varchar](20) NULL,
	[PatientTitle] [varchar](20) NULL,
	[PatientForename] [varchar](30) NULL,
	[PatientSurname] [varchar](30) NULL,
	[DateOfBirth] [smalldatetime] NULL,
	[DateOfDeath] [smalldatetime] NULL,
	[PatientAddress1] [varchar](50) NULL,
	[PatientAddress2] [varchar](50) NULL,
	[PatientAddress3] [varchar](50) NULL,
	[PatientAddress4] [varchar](50) NULL,
	[PostCode] [varchar](25) NULL,
	[SexCode] [varchar](20) NULL,
	[Sex] [varchar](80) NULL,
	[SexNHSCode] [varchar](5) NULL,
	[EthnicGroupCode] [varchar](20) NULL,
	[EthnicGroup] [varchar](80) NULL,
	[EthnicGroupNHSCode] [varchar](5) NULL,
	[ReligionCode] [varchar](20) NULL,
	[Religion] [varchar](80) NULL,
	[ReligionNHSCode] [varchar](5) NULL,
	[MRSA] [char](1) NULL,
	[MRSAStartDate] [smalldatetime] NULL,
	[MRSAEndDate] [smalldatetime] NULL
) ON [PRIMARY]