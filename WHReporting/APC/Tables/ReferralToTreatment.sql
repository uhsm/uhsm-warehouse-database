﻿CREATE TABLE [APC].[ReferralToTreatment](
	[EncounterRecno] [int] NOT NULL,
	[RTTPathwayID] [varchar](200) NULL,
	[RTTStartDate] [smalldatetime] NULL,
	[RTTEndDate] [smalldatetime] NULL,
	[RTTSpecialtyCode] [varchar](5) NULL,
	[RTTCurrentStatusCode] [varchar](20) NULL,
	[RTTCurrentStatus] [varchar](80) NULL,
	[RTTCurrentStatusDate] [smalldatetime] NULL,
	[RTTPeriodStatusCode] [varchar](20) NULL,
	[RTTPeriodStatus] [varchar](80) NULL,
	[OurActivityFlag] [varchar](1) NULL,
	[NotOurActProvCode] [varchar](10) NULL
) ON [PRIMARY]