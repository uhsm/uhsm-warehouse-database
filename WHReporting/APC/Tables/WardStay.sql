﻿CREATE TABLE [APC].[WardStay](
	[SourceUniqueID] [int] NOT NULL,
	[SourceSpellNo] [int] NOT NULL,
	[SourcePatientNo] [int] NOT NULL,
	[StartTime] [smalldatetime] NULL,
	[EndTime] [smalldatetime] NULL,
	[WardCode] [varchar](25) NULL,
	[ProvisionalFlag] [bit] NULL,
	[PASCreated] [datetime] NULL,
	[PASUpdated] [datetime] NULL,
	[PASCreatedByWhom] [varchar](35) NULL,
	[PASUpdatedByWhom] [varchar](35) NULL,
 CONSTRAINT [PK_WardStay] PRIMARY KEY CLUSTERED 
(
	[SourceUniqueID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]