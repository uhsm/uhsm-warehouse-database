﻿CREATE VIEW [APC].[Readmissions]
AS 



/*****************************************************************************
*		Name:	[APC].[Readmissions]
*		Desc:	Script provided by Nicholas Scattergood following agreement on methodology to be used for Readmissions.
*						
* Parameters:	N/A			
* Returns:	  	N/A			
* Where Used -----------------------------------------------------------------		
* Qlikview Trust Dashboard
*  
*
* Modification History -------------------------------------------------------
*
* Date		Author	Change
* 05/10/2015 CM 		Created	
*
******************************************************************************/



With EmergencyAdmissions
/*
CTE to Look At Emergency Admissions Within 30 Days
*/
AS 
(
SELECT TOP 100 PERCENT Readm.PreviousAdmissionRecno
	,AM.[AdmissionMethodType] AS ReadmissionMethodType
	,RS.AdmissionDateTime AS ReadmissionDate
	,RS.DischargeDateTime AS ReadmissionDischargeDate
	,RS.LengthOfSpell AS ReadmLoS
	,Readm.ReadmissionIntervalDays
	,Readm30DayFlag = CASE 
		WHEN Readm.ReadmissionIntervalDays < '30'
			THEN 1
		ELSE 0
		END
	,Readm30DayOvernightFlag = CASE 
		WHEN Readm.ReadmissionIntervalDays < '30'
			AND RS.LengthOfSpell > 0
			THEN 1
		ELSE 0
		END
FROM [WHOLAP].[dbo].[FactReadmission] Readm
INNER JOIN [WHOLAP].[dbo].[OlapAdmissionMethod] AM ON Readm.ReadmissionAdmissionMethodCode = AM.AdmissionMethodCode --AND AM.[AdmissionMethodTypeCode] = 2 
INNER JOIN [WHREPORTING].[APC].[Spell] RS ON Readm.ReadmissionRecno = RS.EncounterRecno
WHERE AM.[AdmissionMethodTypeCode] = 2 ----Emergency----
Order By Readm.PreviousAdmissionRecno
),
CancerSpells
AS
/* CTE to Identify Any Spells where any Cancer is Recorded
*/
(SELECT 
Distinct Top 100 PERCENT 
E.SourceSpellNo
FROM [WHREPORTING].[APC].[AllDiagnosis] D
    
	left join WHREPORTING.APC.Episode E
	on E.EpisodeUniqueID = D.EpisodeSourceUniqueID
	
	WHERE
	(
	left(D.DiagnosisCode,3) between 'C00' and 'C97'
	or
	left(D.DiagnosisCode,3) between 'D37' and 'D48'
	)			
ORDER BY E.SourceSpellNo)



		
--/* Final query to select the information*/

SELECT DATEADD(m, DATEDIFF(m, 0, s.DischargeDate), 0) AS Per_Month
	,S.FaciltyID
	,S.SourceSpellNo
	,S.AdmissionDate
	,S.DischargeDate
	,S.LengthOfSpell
	,S.[AdmissionSpecialtyCode(Function)]
	,S.[AdmissionSpecialty(Function)]
	,DischargeSpecialtyCode = E.[SpecialtyCode(Function)]
	,DischargeSpecialty = E.[Specialty(Function)]
	,DischargeDirectorate = E.Directorate
	,DischargeDivision = E.Division
	,S.IntendedManagementNHSCode
	,S.IntendedManagement
	,S.AdmissionMethodNHSCode
	,S.AdmissionMethod
	--,S.AdmissionType
	,S.DischargeMethodNHSCode
	,S.DischargeMethod
	--,S.PatientClass
	,[Elective/Non-Elective] = CASE 
		WHEN S.AdmissionMethodNHSCode IN (
				'11'
				,'12'
				,'13'
				)
			THEN 'Elective'
		ELSE 'Non-Elective'
		END
	,[AdmissionType] = CASE 
		WHEN S.AdmissionMethodNHSCode IN (
				'11'
				,'12'
				,'13'
				)
			THEN 'Elective Admission'
		WHEN S.AdmissionMethodNHSCode BETWEEN 21
				AND 28
			THEN 'Emergency Admission'
		WHEN S.AdmissionMethodNHSCode BETWEEN 31
				AND 32
			THEN 'Maternity Admission'
		WHEN S.AdmissionMethodNHSCode BETWEEN 82
				AND 83
			THEN 'Baby Birth'
		WHEN S.AdmissionMethodNHSCode = 81
			THEN 'Transfer'
		END
	,Flag_PatientDied = CASE 
		WHEN S.DischargeMethodNHSCode = 4
			THEN 1
		ELSE 0
		END
	,Flag_RegularAttender = CASE --------Don't really have these at UHSM-----------
		WHEN S.IntendedManagementNHSCode IN (
				'4'
				,'5'
				)
			AND DATEDIFF(mi, S.AdmissionDateTime, S.DischargeDateTime) <= 1440
			THEN 1
		ELSE 0
		END
	,Flag_CancerPatients = CASE 
		WHEN Can.SourceSpellNo IS NOT NULL
			THEN 1
		ELSE 0
		END
	,Readm.ReadmissionDate
	,Readm.ReadmissionDischargeDate
	,Readm.ReadmLoS
	,Readm.ReadmissionIntervalDays
	,IND_Readm30Day = isnull(Readm30DayFlag, 0)
	,Flag_Readm30DayOvernight = isnull(Readm30DayOvernightFlag, 0)
	,IND_Activity = 1
FROM APC.Spell s
--LEFT JOIN WHREPORTING.lk.Calendar Cal ON Cal.TheDate = S.DischargeDate
LEFT JOIN WHREPORTING.APC.Episode E ON E.SourceSpellNo = S.SourceSpellNo
	AND E.LastEpisodeInSpell = 1
LEFT JOIN
	----Identifies the re-admissions---
	EmergencyAdmissions Readm ON Readm.PreviousAdmissionRecno = S.EncounterRecno
LEFT JOIN
	----Identifies the Cancer Spells---
	CancerSpells Can ON Can.SourceSpellNo = S.SourceSpellNo
WHERE S.DischargeDate >= '20130401'
		AND S.DischargeDate < DW_REPORTING.LIB.fn_PreviousMonthStartOfDate (getdate())

		--AND DATEADD(s, - 1, DATEADD(mm, DATEDIFF(m, 0, DateAdd(month, - 2, GETDATE())) + 1, 0)) --last day of the month before last