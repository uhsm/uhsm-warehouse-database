﻿CREATE VIEW [APC].[vwCriticalCareActualDaysJCReport]
AS
SELECT     
c.FinancialMonthKey
,EPI.[SpecialtyCode(Function)],                   
T.SourceSpellNo, 
stay.SPONT_REFNO_CODE AS Ward, 
stay.CARE_LEVEL, 
SUM(T.MinutesInpatient) / (60.00 * 24.00) AS BedDays
FROM         

(SELECT DISTINCT APC.CriticalCarePeriod.SourceSpellNo, 
cal.TheDate, 
APC.CriticalCarePeriod.SourceCCPNo, 
APC.CriticalCarePeriod.CriticalCareStartDatetime, 
APC.CriticalCarePeriod.CriticalCareEndDatetime, 
DATEDIFF(minute, COALESCE ((CASE WHEN CAST(CriticalCarestartDatetime AS date) 
         = thedate THEN CriticalCarestartDatetime ELSE NULL END), cal.TheDate), 
COALESCE ((CASE WHEN CAST(coalesce(CriticalCareEndDatetime,getdate()) AS date) 
         = thedate THEN coalesce(CriticalCareEndDatetime,getdate()) ELSE NULL END), 
DATEADD(day, 1, cal.TheDate))) AS MinutesInpatient                      
FROM  APC.CriticalCarePeriod INNER JOIN
      LK.Calendar AS cal ON cal.TheDate BETWEEN CAST(APC.CriticalCarePeriod.CriticalCareStartDatetime AS DATE) AND 
              CAST(coalesce(APC.CriticalCarePeriod.CriticalCareEndDatetime,getdate()) AS DATE)) AS T 
              
              
      INNER JOIN
      LK.Calendar AS c ON c.TheDate = T.TheDate LEFT OUTER JOIN
      Lorenzo.dbo.ExtractCriticalCarePeriodStay AS stay ON T.SourceCCPNo = stay.SourceCCPNo AND T.TheDate = CAST(stay.StayDate AS date)
    --  left join WHREPORTING.APC.Spell on Spell.SourceSpellNo=t.SourceSpellNo   
       inner join WHReporting.APC.CriticalCarePeriod CCPeriod on CCPeriod.SourceCCPNo=stay.SourceCCPNo        
LEFT JOIN WHReporting.APC.Episode EPI
ON CCPeriod.SourceEncounterNo = EPI.EpisodeUniqueID
WHERE     (stay.SPONT_REFNO_CODE IN ('CTCU', 'CT Transplant', 'ICA','CTTRAN'))--01/06/2016 CM Added CTTRAN as this is the new ward code for CT Transplant as per ward code changes for JAC
--and DischargeDate is not null

GROUP BY stay.SPONT_REFNO_CODE, stay.CARE_LEVEL, c.FinancialMonthKey, T.SourceSpellNo,
 EPI.[SpecialtyCode(Function)]