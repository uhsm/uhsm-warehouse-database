﻿CREATE VIEW [APC].[vwCriticalCareAdmissions]
AS
select

Ward,
[Year],
[Month],
SUM(Counter) as Admissions

from



(SELECT [AdmissionSpecialty(Function)], SPL.AdmissionConsultantName, SPL.FaciltyID, CritP.SourceSpellNo, CritP.AdmissionType, 
CriticalCareStartDatetime,CriticalCareEndDatetime,
  CASE WHEN SPONT_REFNO_NAME IN ('CTCU', 'CT Transplant','CTTRAN')--01/06/2016 CM Added CTTRAN as this is the new ward code for CT Transplant as per ward code changes for JAC
							THEN 'CTCU'
	   WHEN SPONT_REFNO_NAME = 'ACUTE - ICU' THEN 'ICA'
  END AS Ward, 
  
  COUNT(CCPStay) AS LoS,
  
  Counter = 1, 
CAL.FinancialYear AS [Year],
LEFT(CAL.TheMonth,3) as [Month]
  
  FROM [WHREPORTING].[APC].[CriticalCarePeriod] CritP
  
  LEFT OUTER JOIN Lorenzo.dbo.ExtractCriticalCarePeriodStay Stay
  ON CritP.SourceCCPNo = Stay.SourceCCPNo
  
  --LEFT OUTER JOIN Lorenzo.dbo.ServicePoint SP
  --ON Stay.CCPStaywWard = SP.SPONT_REFNO
  
  LEFT OUTER JOIN WH.PAS.ServicePointBase SP
  ON Stay.CCPStaywWard = SP.SPONT_REFNO
  
  LEFT OUTER JOIN WHREPORTING.APC.Spell SPL
  ON CritP.SourceSpellNo = SPL.SourceSpellNo
  LEFT OUTER JOIN LK.Calendar CAL ON CAL.TheDate=CAST(CriticalCareStartDatetime AS DATE)
  --where CritP.SourceCCPNo = '150006952'
  
  WHERE CriticalCareStartDatetime >= '2010-04-01'
  AND CriticalCareStartDatetime < DATEADD(DAY,((DATEPART(DAY,GETDATE()))-1)*-1,CAST(GETDATE()AS DATE))
  
	
  
  /*AND   
  CASE WHEN SPONT_REFNO_NAME IN ('CTCU', 'CT Transplant')THEN 'CTCU'
	   WHEN SPONT_REFNO_NAME = 'ACUTE - ICU' THEN 'ICA'
  END IN (@Ward)*/
  
  GROUP BY [AdmissionSpecialty(Function)], SPL.AdmissionConsultantName, SPL.FaciltyID, CritP.SourceSpellNo, 
  CritP.AdmissionType,CriticalCareStartDatetime, CriticalCareEndDatetime,   
  CASE WHEN SPONT_REFNO_NAME IN ('CTCU', 'CT Transplant','CTTRAN')--01/06/2016 CM Added CTTRAN as this is the new ward code for CT Transplant as per ward code changes for JAC
								THEN 'CTCU'
	   WHEN SPONT_REFNO_NAME = 'ACUTE - ICU' THEN 'ICA'
  END, 
CONVERT(CHAR(4), CriticalCareStartDatetime, 100) + CONVERT(CHAR(4), CriticalCareStartDatetime, 120)
  ,CAL.FinancialYear,
LEFT(CAL.TheMonth,3)
 ) as t
 
 where Ward in ('CTCU','ICA')
 
 group by
 
 Ward,
[Year],
[Month]