﻿CREATE VIEW [APC].[vwCriticalCareFact]
AS

Select Distinct 
case when SPELL.AdmissionMethodCode between 21 and 28 then 1 else 0 end as AdmissionType,
cal.FinancialMonthKey,
cal.TheMonth,
CCPeriod.SourceSpellNo,
SPONT_REFNO_CODE,
CARE_LEVEL,
EPI.Specialty,
EPI.SpecialtyCode,
EPI.[SpecialtyCode(Function)],
EPI.[Specialty(Function)],
Case when [SpecialtyCode(Function)]=173 and not(ConsultantCode = 'C2386780' AND PriamryProcedureCode like 'K%') and (CCProcs.OPCS4_Description = 'Pneumonectomy' or CCProcs1.OPCS4_Description = 'Pneumonectomy' or CCProcs2.OPCS4_Description = 'Pneumonectomy' or CCProcs3.OPCS4_Description = 'Pneumonectomy' or CCProcs4.OPCS4_Description = 'Pneumonectomy' or CCProcs5.OPCS4_Description = 'Pneumonectomy')
 then 1 else 0 end as Pneumonectomy,
Case when [SpecialtyCode(Function)]=173 and not(ConsultantCode = 'C2386780' AND PriamryProcedureCode like 'K%') and (CCProcs.OPCS4_Description = 'Bilobectomy' or CCProcs1.OPCS4_Description = 'Bilobectomy' or CCProcs2.OPCS4_Description = 'Bilobectomy' or CCProcs3.OPCS4_Description = 'Bilobectomy' or CCProcs4.OPCS4_Description = 'Bilobectomy' or CCProcs5.OPCS4_Description = 'Bilobectomy')
 then 1 else 0 end as Bilobectomy,
Case when [SpecialtyCode(Function)]=173 and not(ConsultantCode = 'C2386780' AND PriamryProcedureCode like 'K%') and (CCProcs.OPCS4_Description = 'Lobectomy' or CCProcs1.OPCS4_Description = 'Lobectomy' or CCProcs2.OPCS4_Description = 'Lobectomy' or CCProcs3.OPCS4_Description = 'Lobectomy' or CCProcs4.OPCS4_Description = 'Lobectomy' or CCProcs5.OPCS4_Description = 'Lobectomy')
 then 1 else 0 end as Lobectomy,
Case when [SpecialtyCode(Function)]=173 and not(ConsultantCode = 'C2386780' AND PriamryProcedureCode like 'K%') and (CCProcs.OPCS4_Description = 'Decortication' or CCProcs1.OPCS4_Description = 'Decortication' or CCProcs2.OPCS4_Description = 'Decortication' or CCProcs3.OPCS4_Description = 'Decortication' or CCProcs4.OPCS4_Description = 'Decortication' or CCProcs5.OPCS4_Description = 'Decortication')
 then 1 else 0 end as Decortication,
Case when [SpecialtyCode(Function)]=173 and not(ConsultantCode = 'C2386780' AND PriamryProcedureCode like 'K%') and (CCProcs.OPCS4_Description = 'Chest Wall Resection' or CCProcs1.OPCS4_Description = 'Chest Wall Resection' or CCProcs2.OPCS4_Description = 'Chest Wall Resection' or CCProcs3.OPCS4_Description = 'Chest Wall Resection' or CCProcs4.OPCS4_Description = 'Chest Wall Resection' or CCProcs5.OPCS4_Description = 'Chest Wall Resection')
 then 1 else 0 end as [Chest Wall Resection],
case when [SpecialtyCode(Function)]=173 

and 
		
Case when [SpecialtyCode(Function)]=173 and not(ConsultantCode = 'C2386780' AND PriamryProcedureCode like 'K%') and (CCProcs.OPCS4_Description = 'Pneumonectomy' or CCProcs1.OPCS4_Description = 'Pneumonectomy' or CCProcs2.OPCS4_Description = 'Pneumonectomy' or CCProcs3.OPCS4_Description = 'Pneumonectomy' or CCProcs4.OPCS4_Description = 'Pneumonectomy' or CCProcs5.OPCS4_Description = 'Pneumonectomy')
 then 1 
 when [SpecialtyCode(Function)]=173 and not(ConsultantCode = 'C2386780' AND PriamryProcedureCode like 'K%') and (CCProcs.OPCS4_Description = 'Bilobectomy' or CCProcs1.OPCS4_Description = 'Bilobectomy' or CCProcs2.OPCS4_Description = 'Bilobectomy' or CCProcs3.OPCS4_Description = 'Bilobectomy' or CCProcs4.OPCS4_Description = 'Bilobectomy' or CCProcs5.OPCS4_Description = 'Bilobectomy')
 then 1 
 when [SpecialtyCode(Function)]=173 and not(ConsultantCode = 'C2386780' AND PriamryProcedureCode like 'K%') and (CCProcs.OPCS4_Description = 'Lobectomy' or CCProcs1.OPCS4_Description = 'Lobectomy' or CCProcs2.OPCS4_Description = 'Lobectomy' or CCProcs3.OPCS4_Description = 'Lobectomy' or CCProcs4.OPCS4_Description = 'Lobectomy' or CCProcs5.OPCS4_Description = 'Lobectomy')
 then 1 
when [SpecialtyCode(Function)]=173 and not(ConsultantCode = 'C2386780' AND PriamryProcedureCode like 'K%') and (CCProcs.OPCS4_Description = 'Decortication' or CCProcs1.OPCS4_Description = 'Decortication' or CCProcs2.OPCS4_Description = 'Decortication' or CCProcs3.OPCS4_Description = 'Decortication' or CCProcs4.OPCS4_Description = 'Decortication' or CCProcs5.OPCS4_Description = 'Decortication')
 then 1 
when [SpecialtyCode(Function)]=173 and not(ConsultantCode = 'C2386780' AND PriamryProcedureCode like 'K%') and (CCProcs.OPCS4_Description = 'Chest Wall Resection' or CCProcs1.OPCS4_Description = 'Chest Wall Resection' or CCProcs2.OPCS4_Description = 'Chest Wall Resection' or CCProcs3.OPCS4_Description = 'Chest Wall Resection' or CCProcs4.OPCS4_Description = 'Chest Wall Resection' or CCProcs5.OPCS4_Description = 'Chest Wall Resection')
 then 1 else 0 end=0 then 1 else 0 end as
 Other_Thoracic,
case when [SpecialtyCode(Function)]='320' and (cc.[PrimaryDiagnosisCode] like 'I50%' or [SecondaryDiagnosisCode1] like 'I50%' or [SecondaryDiagnosisCode2]like 'I50%' or [SecondaryDiagnosisCode3]like 'I50%' or [SecondaryDiagnosisCode4]like 'I50%' or [SecondaryDiagnosisCode5] like 'I50%')
 then 1 else 0 end as [Acute Heart Failure],
case when [SpecialtyCode(Function)]='320' and (CCProcs.OPCS4_Description = 'PCICardiology' or CCProcs1.OPCS4_Description = 'PCICardiology' or CCProcs2.OPCS4_Description = 'PCICardiology' or CCProcs3.OPCS4_Description = 'PCICardiology' or CCProcs4.OPCS4_Description = 'PCICardiology' or CCProcs5.OPCS4_Description = 'PCICardiology') 
then 1 else 0 end as [PCICardiology],
case when [SpecialtyCode(Function)]='320' and ((cc.[PrimaryDiagnosisCode] not like 'I50%' or cc.[PrimaryDiagnosisCode] is null) and ([SecondaryDiagnosisCode1] not like 'I50%' or cc.[SecondaryDiagnosisCode1] is null) and ([SecondaryDiagnosisCode2] not like 'I50%' or cc.[SecondaryDiagnosisCode2] is null) and ([SecondaryDiagnosisCode3] not like 'I50%' or cc.[SecondaryDiagnosisCode3] is null) and ([SecondaryDiagnosisCode4] not like 'I50%' or cc.[SecondaryDiagnosisCode4] is null) and ([SecondaryDiagnosisCode5] not like 'I50%' or cc.[SecondaryDiagnosisCode5] is null) and (CCProcs.OPCS4_Description <> 'PCICardiology' or CCProcs.OPCS4_Description is null) and (CCProcs1.OPCS4_Description <> 'PCICardiology' or CCProcs1.OPCS4_Description is null) and (CCProcs2.OPCS4_Description <> 'PCICardiology' or CCProcs2.OPCS4_Description is null) and (CCProcs3.OPCS4_Description <> 'PCICardiology' or CCProcs3.OPCS4_Description is null) and (CCProcs4.OPCS4_Description <> 'PCICardiology' or CCProcs4.OPCS4_Description is null) and (CCProcs5.OPCS4_Description <> 'PCICardiology' or CCProcs5.OPCS4_Description is null))
 Then 1 ELSE 0 END AS [Other_Cardiology],
Case when TXTYPE='SL' Then 1 else 0 end as TransplantSLT,
Case when TXTYPE='DL' Then 1 else 0 end as TransplantDLT,
Case when TXTYPE='H' Then 1 else 0 end as TransplantHT,
Case when TransplantVAD.SourceSpellNo is not null then 1 else 0 end as TransplantVAD,
Case when TAVI.SourceSpellNo is not null then 1 else 0 end as TAVI,
Case when [SpecialtyCode(Function)]=170 and (CCProcs.OPCS4_Description = 'AVR' or CCProcs1.OPCS4_Description = 'AVR' or CCProcs2.OPCS4_Description = 'AVR' or CCProcs3.OPCS4_Description = 'AVR' or CCProcs4.OPCS4_Description = 'AVR' or CCProcs5.OPCS4_Description = 'AVR')
 then 1 else 0 end as AVR,
Case when [SpecialtyCode(Function)]=170 and (((PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 not like 'K34%' and SecondaryProcedurecode2 not like 'K34%' and SecondaryProcedurecode3 not like 'K34%' and SecondaryProcedurecode4 not like 'K34%' and SecondaryProcedurecode5 not like 'K34%')) or (SecondaryProcedurecode1 like 'K25%' and not(PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3  like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%'))) or (SecondaryProcedurecode2 like 'K25%' and not(PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3  like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%'))) or (SecondaryProcedurecode3 like 'K25%' and not(PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3  like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%'))) or (SecondaryProcedurecode4 like 'K25%'  and not(PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3  like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%'))) or (SecondaryProcedurecode5 like 'K25%' and not(PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3  like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%')))))
 then 1 else 0 end as MVR,
Case when [SpecialtyCode(Function)]=170 and (CCProcs.OPCS4_Description = 'CABG' or CCProcs1.OPCS4_Description = 'CABG' or CCProcs2.OPCS4_Description = 'CABG' or CCProcs3.OPCS4_Description = 'CABG' or CCProcs4.OPCS4_Description = 'CABG' or CCProcs5.OPCS4_Description = 'CABG')
 then 1 else 0 end as CABG,
Case when [SpecialtyCode(Function)]=170 and (CCProcs.OPCS4_Description = 'Major Aortic Surgery' or CCProcs1.OPCS4_Description = 'Major Aortic Surgery' or CCProcs2.OPCS4_Description = 'Major Aortic Surgery' or CCProcs3.OPCS4_Description = 'Major Aortic Surgery' or CCProcs4.OPCS4_Description = 'Major Aortic Surgery' or CCProcs5.OPCS4_Description = 'Major Aortic Surgery')
 then 1 else 0 end as [Major Aortic Surgery],
Case when [SpecialtyCode(Function)]=170 and PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3 like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%') 
then 1 else 0 end as [Double Valve],
Case when [SpecialtyCode(Function)]=170 and PriamryProcedureCode like 'K51%' and (SecondaryProcedurecode1 like 'K26%' or SecondaryProcedurecode2 like 'K26%' or SecondaryProcedurecode3 like 'K26%' or SecondaryProcedurecode4 like 'K26%' or SecondaryProcedurecode5 like 'K26%') 
then 1 else 0 end as [AVR and CABG], 
Case when [SpecialtyCode(Function)]=170 and

Case when [SpecialtyCode(Function)]=170 and (CCProcs.OPCS4_Description = 'AVR' or CCProcs1.OPCS4_Description = 'AVR' or CCProcs2.OPCS4_Description = 'AVR' or CCProcs3.OPCS4_Description = 'AVR' or CCProcs4.OPCS4_Description = 'AVR' or CCProcs5.OPCS4_Description = 'AVR')
 then 1 
when [SpecialtyCode(Function)]=170 and (((PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 not like 'K34%' and SecondaryProcedurecode2 not like 'K34%' and SecondaryProcedurecode3 not like 'K34%' and SecondaryProcedurecode4 not like 'K34%' and SecondaryProcedurecode5 not like 'K34%')) or (SecondaryProcedurecode1 like 'K25%' and not(PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3  like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%'))) or (SecondaryProcedurecode2 like 'K25%' and not(PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3  like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%'))) or (SecondaryProcedurecode3 like 'K25%' and not(PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3  like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%'))) or (SecondaryProcedurecode4 like 'K25%'  and not(PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3  like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%'))) or (SecondaryProcedurecode5 like 'K25%' and not(PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3  like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%')))))
 then 1
when [SpecialtyCode(Function)]=170 and (CCProcs.OPCS4_Description = 'CABG' or CCProcs1.OPCS4_Description = 'CABG' or CCProcs2.OPCS4_Description = 'CABG' or CCProcs3.OPCS4_Description = 'CABG' or CCProcs4.OPCS4_Description = 'CABG' or CCProcs5.OPCS4_Description = 'CABG')
 then 1 
 when [SpecialtyCode(Function)]=170 and (CCProcs.OPCS4_Description = 'Major Aortic Surgery' or CCProcs1.OPCS4_Description = 'Major Aortic Surgery' or CCProcs2.OPCS4_Description = 'Major Aortic Surgery' or CCProcs3.OPCS4_Description = 'Major Aortic Surgery' or CCProcs4.OPCS4_Description = 'Major Aortic Surgery' or CCProcs5.OPCS4_Description = 'Major Aortic Surgery')
 then 1 
when [SpecialtyCode(Function)]=170 and PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3 like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%') 
then 1 
when [SpecialtyCode(Function)]=170 and PriamryProcedureCode like 'K51%' and (SecondaryProcedurecode1 like 'K26%' or SecondaryProcedurecode2 like 'K26%' or SecondaryProcedurecode3 like 'K26%' or SecondaryProcedurecode4 like 'K26%' or SecondaryProcedurecode5 like 'K26%') 
then 1 
else 0 End=0 then 1 else 0 end

 as Other_Cardiac,
 
 
--Case When Ecmo.SPELL_REFNO is not null then 1 else 0 end as ECMO,'X581'
case when 
PriamryProcedureCode = 'X581' 
or SecondaryProcedurecode1 = 'X581' 
or SecondaryProcedurecode2 = 'X581' 
or SecondaryProcedurecode3 = 'X581'  
or SecondaryProcedurecode4 = 'X581'  
or SecondaryProcedurecode5 = 'X581' 
 or SecondaryProcedurecode6 = 'X581'  
 or SecondaryProcedurecode7 = 'X581'  
 or SecondaryProcedurecode8 = 'X581'  
 or SecondaryProcedurecode9 = 'X581'  
  or SecondaryProcedurecode10 = 'X581'  
then 1 else 0 end as ECMO, 

sum(Stay.[CCPStay]*1) as ActualBedDays

from 

Lorenzo.dbo.ExtractCriticalCarePeriodStay stay
inner join WHReporting.APC.CriticalCarePeriod CCPeriod on stay.SourceCCPNo=CCPeriod.SourceCCPNo
LEFT join WHReporting.dbo.TransplantVADPatients TransplantVAD on CCPeriod.SourceSpellNo = TransplantVAD.SourceSpellNo
LEFT join WHReporting.dbo.TAVIPatients TAVI on CCPeriod.SourceSpellNo = TAVI.SourceSpellNo
LEFT JOIN WHReporting.APC.Episode EPI ON CCPeriod.SourceEncounterNo = EPI.EpisodeUniqueID
lEFT JOIN (select distinct txtype,spell2 from WHReporting.dbo.TXPatients Transplants) as Trans ON Trans.Spell2=CCPeriod.SourceSpellNo
left join WHREPORTING.LK.Calendar cal on cal.TheDate=cast(stay.StayDate as date)
Left JOIN WHReporting.APC.ClinicalCoding CC ON EPI.EncounterRecno = CC.EncounterRecno
LEFT JOIN WHREPORTING.dbo.CC_Procedures CCProcs on CC.PriamryProcedureCode = CCProcs.OPCS4Code
LEFT JOIN WHREPORTING.dbo.CC_Procedures CCProcs1 on CC.SecondaryProcedureCode1 = CCProcs1.OPCS4Code
LEFT JOIN WHREPORTING.dbo.CC_Procedures CCProcs2 on CC.SecondaryProcedureCode2 = CCProcs2.OPCS4Code
LEFT JOIN WHREPORTING.dbo.CC_Procedures CCProcs3 on CC.SecondaryProcedureCode3 = CCProcs3.OPCS4Code
LEFT JOIN WHREPORTING.dbo.CC_Procedures CCProcs4 on CC.SecondaryProcedureCode4 = CCProcs4.OPCS4Code
LEFT JOIN WHREPORTING.dbo.CC_Procedures CCProcs5 on CC.SecondaryProcedureCode5 = CCProcs5.OPCS4Code
--Left Join WHREPORTING.dbo.[ECMO Patients] Ecmo on CCPeriod.SourceSpellNo = Ecmo.SPELL_REFNO --use apc.ecmodays
lEFT JOIN APC.Spell SPELL ON SPELL.SourceSpellNo=EPI.SourceSpellNo
where SPONT_REFNO_CODE in ('CTCU', 'CT Transplant', 'ICA','CTTRAN')--01/06/2016 CM Added CTTRAN as this is the new ward code for CT Transplant as per ward code changes for JAC
--and DischargeDate is not null
Group by
EPI.Specialty,
EPI.SpecialtyCode,
case when SPELL.AdmissionMethodCode between 21 and 28 then 1 else 0 end,
cal.FinancialMonthKey,
cal.TheMonth,
CCPeriod.SourceSpellNo,
SPONT_REFNO_CODE,
CARE_LEVEL,
EPI.[SpecialtyCode(Function)],
EPI.[Specialty(Function)],
Case when [SpecialtyCode(Function)]=173 and not(ConsultantCode = 'C2386780' AND PriamryProcedureCode like 'K%') and (CCProcs.OPCS4_Description = 'Pneumonectomy' or CCProcs1.OPCS4_Description = 'Pneumonectomy' or CCProcs2.OPCS4_Description = 'Pneumonectomy' or CCProcs3.OPCS4_Description = 'Pneumonectomy' or CCProcs4.OPCS4_Description = 'Pneumonectomy' or CCProcs5.OPCS4_Description = 'Pneumonectomy')
 then 1 else 0 end,
Case when [SpecialtyCode(Function)]=173 and not(ConsultantCode = 'C2386780' AND PriamryProcedureCode like 'K%') and (CCProcs.OPCS4_Description = 'Bilobectomy' or CCProcs1.OPCS4_Description = 'Bilobectomy' or CCProcs2.OPCS4_Description = 'Bilobectomy' or CCProcs3.OPCS4_Description = 'Bilobectomy' or CCProcs4.OPCS4_Description = 'Bilobectomy' or CCProcs5.OPCS4_Description = 'Bilobectomy')
 then 1 else 0 end,
Case when [SpecialtyCode(Function)]=173 and not(ConsultantCode = 'C2386780' AND PriamryProcedureCode like 'K%') and (CCProcs.OPCS4_Description = 'Lobectomy' or CCProcs1.OPCS4_Description = 'Lobectomy' or CCProcs2.OPCS4_Description = 'Lobectomy' or CCProcs3.OPCS4_Description = 'Lobectomy' or CCProcs4.OPCS4_Description = 'Lobectomy' or CCProcs5.OPCS4_Description = 'Lobectomy')
 then 1 else 0 end,
Case when [SpecialtyCode(Function)]=173 and not(ConsultantCode = 'C2386780' AND PriamryProcedureCode like 'K%') and (CCProcs.OPCS4_Description = 'Decortication' or CCProcs1.OPCS4_Description = 'Decortication' or CCProcs2.OPCS4_Description = 'Decortication' or CCProcs3.OPCS4_Description = 'Decortication' or CCProcs4.OPCS4_Description = 'Decortication' or CCProcs5.OPCS4_Description = 'Decortication')
 then 1 else 0 end,
Case when [SpecialtyCode(Function)]=173 and not(ConsultantCode = 'C2386780' AND PriamryProcedureCode like 'K%') and (CCProcs.OPCS4_Description = 'Chest Wall Resection' or CCProcs1.OPCS4_Description = 'Chest Wall Resection' or CCProcs2.OPCS4_Description = 'Chest Wall Resection' or CCProcs3.OPCS4_Description = 'Chest Wall Resection' or CCProcs4.OPCS4_Description = 'Chest Wall Resection' or CCProcs5.OPCS4_Description = 'Chest Wall Resection')
 then 1 else 0 end,
case when [SpecialtyCode(Function)]=173 

and 
		
Case when [SpecialtyCode(Function)]=173 and not(ConsultantCode = 'C2386780' AND PriamryProcedureCode like 'K%') and (CCProcs.OPCS4_Description = 'Pneumonectomy' or CCProcs1.OPCS4_Description = 'Pneumonectomy' or CCProcs2.OPCS4_Description = 'Pneumonectomy' or CCProcs3.OPCS4_Description = 'Pneumonectomy' or CCProcs4.OPCS4_Description = 'Pneumonectomy' or CCProcs5.OPCS4_Description = 'Pneumonectomy')
 then 1 
 when [SpecialtyCode(Function)]=173 and not(ConsultantCode = 'C2386780' AND PriamryProcedureCode like 'K%') and (CCProcs.OPCS4_Description = 'Bilobectomy' or CCProcs1.OPCS4_Description = 'Bilobectomy' or CCProcs2.OPCS4_Description = 'Bilobectomy' or CCProcs3.OPCS4_Description = 'Bilobectomy' or CCProcs4.OPCS4_Description = 'Bilobectomy' or CCProcs5.OPCS4_Description = 'Bilobectomy')
 then 1 
 when [SpecialtyCode(Function)]=173 and not(ConsultantCode = 'C2386780' AND PriamryProcedureCode like 'K%') and (CCProcs.OPCS4_Description = 'Lobectomy' or CCProcs1.OPCS4_Description = 'Lobectomy' or CCProcs2.OPCS4_Description = 'Lobectomy' or CCProcs3.OPCS4_Description = 'Lobectomy' or CCProcs4.OPCS4_Description = 'Lobectomy' or CCProcs5.OPCS4_Description = 'Lobectomy')
 then 1 
when [SpecialtyCode(Function)]=173 and not(ConsultantCode = 'C2386780' AND PriamryProcedureCode like 'K%') and (CCProcs.OPCS4_Description = 'Decortication' or CCProcs1.OPCS4_Description = 'Decortication' or CCProcs2.OPCS4_Description = 'Decortication' or CCProcs3.OPCS4_Description = 'Decortication' or CCProcs4.OPCS4_Description = 'Decortication' or CCProcs5.OPCS4_Description = 'Decortication')
 then 1 
when [SpecialtyCode(Function)]=173 and not(ConsultantCode = 'C2386780' AND PriamryProcedureCode like 'K%') and (CCProcs.OPCS4_Description = 'Chest Wall Resection' or CCProcs1.OPCS4_Description = 'Chest Wall Resection' or CCProcs2.OPCS4_Description = 'Chest Wall Resection' or CCProcs3.OPCS4_Description = 'Chest Wall Resection' or CCProcs4.OPCS4_Description = 'Chest Wall Resection' or CCProcs5.OPCS4_Description = 'Chest Wall Resection')
 then 1 else 0 end=0 then 1 else 0 end,
case when [SpecialtyCode(Function)]='320' and (cc.[PrimaryDiagnosisCode] like 'I50%' or [SecondaryDiagnosisCode1] like 'I50%' or [SecondaryDiagnosisCode2]like 'I50%' or [SecondaryDiagnosisCode3]like 'I50%' or [SecondaryDiagnosisCode4]like 'I50%' or [SecondaryDiagnosisCode5] like 'I50%')
 then 1 else 0 end,
case when [SpecialtyCode(Function)]='320' and (CCProcs.OPCS4_Description = 'PCICardiology' or CCProcs1.OPCS4_Description = 'PCICardiology' or CCProcs2.OPCS4_Description = 'PCICardiology' or CCProcs3.OPCS4_Description = 'PCICardiology' or CCProcs4.OPCS4_Description = 'PCICardiology' or CCProcs5.OPCS4_Description = 'PCICardiology') 
then 1 else 0 end,
case when [SpecialtyCode(Function)]='320' and ((cc.[PrimaryDiagnosisCode] not like 'I50%' or cc.[PrimaryDiagnosisCode] is null) and ([SecondaryDiagnosisCode1] not like 'I50%' or cc.[SecondaryDiagnosisCode1] is null) and ([SecondaryDiagnosisCode2] not like 'I50%' or cc.[SecondaryDiagnosisCode2] is null) and ([SecondaryDiagnosisCode3] not like 'I50%' or cc.[SecondaryDiagnosisCode3] is null) and ([SecondaryDiagnosisCode4] not like 'I50%' or cc.[SecondaryDiagnosisCode4] is null) and ([SecondaryDiagnosisCode5] not like 'I50%' or cc.[SecondaryDiagnosisCode5] is null) and (CCProcs.OPCS4_Description <> 'PCICardiology' or CCProcs.OPCS4_Description is null) and (CCProcs1.OPCS4_Description <> 'PCICardiology' or CCProcs1.OPCS4_Description is null) and (CCProcs2.OPCS4_Description <> 'PCICardiology' or CCProcs2.OPCS4_Description is null) and (CCProcs3.OPCS4_Description <> 'PCICardiology' or CCProcs3.OPCS4_Description is null) and (CCProcs4.OPCS4_Description <> 'PCICardiology' or CCProcs4.OPCS4_Description is null) and (CCProcs5.OPCS4_Description <> 'PCICardiology' or CCProcs5.OPCS4_Description is null))
 Then 1 ELSE 0 END,
Case when TXTYPE='SL' Then 1 else 0 end,
Case when TXTYPE='DL' Then 1 else 0 end,
Case when TXTYPE='H' Then 1 else 0 end,
Case when TransplantVAD.SourceSpellNo is not null then 1 else 0 end,
Case when TAVI.SourceSpellNo is not null then 1 else 0 end,
Case when [SpecialtyCode(Function)]=170 and (CCProcs.OPCS4_Description = 'AVR' or CCProcs1.OPCS4_Description = 'AVR' or CCProcs2.OPCS4_Description = 'AVR' or CCProcs3.OPCS4_Description = 'AVR' or CCProcs4.OPCS4_Description = 'AVR' or CCProcs5.OPCS4_Description = 'AVR')
 then 1 else 0 end,
Case when [SpecialtyCode(Function)]=170 and (((PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 not like 'K34%' and SecondaryProcedurecode2 not like 'K34%' and SecondaryProcedurecode3 not like 'K34%' and SecondaryProcedurecode4 not like 'K34%' and SecondaryProcedurecode5 not like 'K34%')) or (SecondaryProcedurecode1 like 'K25%' and not(PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3  like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%'))) or (SecondaryProcedurecode2 like 'K25%' and not(PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3  like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%'))) or (SecondaryProcedurecode3 like 'K25%' and not(PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3  like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%'))) or (SecondaryProcedurecode4 like 'K25%'  and not(PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3  like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%'))) or (SecondaryProcedurecode5 like 'K25%' and not(PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3  like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%')))))
 then 1 else 0 end,
Case when [SpecialtyCode(Function)]=170 and (CCProcs.OPCS4_Description = 'CABG' or CCProcs1.OPCS4_Description = 'CABG' or CCProcs2.OPCS4_Description = 'CABG' or CCProcs3.OPCS4_Description = 'CABG' or CCProcs4.OPCS4_Description = 'CABG' or CCProcs5.OPCS4_Description = 'CABG')
 then 1 else 0 end,
Case when [SpecialtyCode(Function)]=170 and (CCProcs.OPCS4_Description = 'Major Aortic Surgery' or CCProcs1.OPCS4_Description = 'Major Aortic Surgery' or CCProcs2.OPCS4_Description = 'Major Aortic Surgery' or CCProcs3.OPCS4_Description = 'Major Aortic Surgery' or CCProcs4.OPCS4_Description = 'Major Aortic Surgery' or CCProcs5.OPCS4_Description = 'Major Aortic Surgery')
 then 1 else 0 end,
Case when [SpecialtyCode(Function)]=170 and PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3 like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%') 
then 1 else 0 end,
Case when [SpecialtyCode(Function)]=170 and PriamryProcedureCode like 'K51%' and (SecondaryProcedurecode1 like 'K26%' or SecondaryProcedurecode2 like 'K26%' or SecondaryProcedurecode3 like 'K26%' or SecondaryProcedurecode4 like 'K26%' or SecondaryProcedurecode5 like 'K26%') 
then 1 else 0 end, 
Case when [SpecialtyCode(Function)]=170 and

Case when [SpecialtyCode(Function)]=170 and (CCProcs.OPCS4_Description = 'AVR' or CCProcs1.OPCS4_Description = 'AVR' or CCProcs2.OPCS4_Description = 'AVR' or CCProcs3.OPCS4_Description = 'AVR' or CCProcs4.OPCS4_Description = 'AVR' or CCProcs5.OPCS4_Description = 'AVR')
 then 1 
when [SpecialtyCode(Function)]=170 and (((PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 not like 'K34%' and SecondaryProcedurecode2 not like 'K34%' and SecondaryProcedurecode3 not like 'K34%' and SecondaryProcedurecode4 not like 'K34%' and SecondaryProcedurecode5 not like 'K34%')) or (SecondaryProcedurecode1 like 'K25%' and not(PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3  like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%'))) or (SecondaryProcedurecode2 like 'K25%' and not(PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3  like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%'))) or (SecondaryProcedurecode3 like 'K25%' and not(PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3  like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%'))) or (SecondaryProcedurecode4 like 'K25%'  and not(PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3  like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%'))) or (SecondaryProcedurecode5 like 'K25%' and not(PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3  like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%')))))
 then 1
when [SpecialtyCode(Function)]=170 and (CCProcs.OPCS4_Description = 'CABG' or CCProcs1.OPCS4_Description = 'CABG' or CCProcs2.OPCS4_Description = 'CABG' or CCProcs3.OPCS4_Description = 'CABG' or CCProcs4.OPCS4_Description = 'CABG' or CCProcs5.OPCS4_Description = 'CABG')
 then 1 
 when [SpecialtyCode(Function)]=170 and (CCProcs.OPCS4_Description = 'Major Aortic Surgery' or CCProcs1.OPCS4_Description = 'Major Aortic Surgery' or CCProcs2.OPCS4_Description = 'Major Aortic Surgery' or CCProcs3.OPCS4_Description = 'Major Aortic Surgery' or CCProcs4.OPCS4_Description = 'Major Aortic Surgery' or CCProcs5.OPCS4_Description = 'Major Aortic Surgery')
 then 1 
when [SpecialtyCode(Function)]=170 and PriamryProcedureCode like 'K25%' and (SecondaryProcedurecode1 like 'K34%' or SecondaryProcedurecode2 like 'K34%' or SecondaryProcedurecode3 like 'K34%' or SecondaryProcedurecode4 like 'K34%' or SecondaryProcedurecode5 like 'K34%') 
then 1 
when [SpecialtyCode(Function)]=170 and PriamryProcedureCode like 'K51%' and (SecondaryProcedurecode1 like 'K26%' or SecondaryProcedurecode2 like 'K26%' or SecondaryProcedurecode3 like 'K26%' or SecondaryProcedurecode4 like 'K26%' or SecondaryProcedurecode5 like 'K26%') 
then 1 
else 0 End=0 then 1 else 0 end,
case when 
PriamryProcedureCode = 'X581' 
or SecondaryProcedurecode1 = 'X581' 
or SecondaryProcedurecode2 = 'X581' 
or SecondaryProcedurecode3 = 'X581'  
or SecondaryProcedurecode4 = 'X581'  
or SecondaryProcedurecode5 = 'X581' 
 or SecondaryProcedurecode6 = 'X581'  
 or SecondaryProcedurecode7 = 'X581'  
 or SecondaryProcedurecode8 = 'X581'  
 or SecondaryProcedurecode9 = 'X581'  
  or SecondaryProcedurecode10 = 'X581'  
then 1 else 0 end