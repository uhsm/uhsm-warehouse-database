﻿CREATE VIEW [APC].[vwCriticalCareFactJCReport]
AS
Select Distinct 
cal.FinancialMonthKey,
cal.TheMonth,
CCPeriod.SourceSpellNo
,SPONT_REFNO_CODE
,CARE_LEVEL
,EPI.[SpecialtyCode(Function)]
,case when [SpecialtyCode(Function)]=173 and
not(ConsultantCode = 'C2386780' 
AND PriamryProcedureCode like 'K%') then 1 else 0 end as Thoracic
,case when [SpecialtyCode(Function)]='320' Then 1 ELSE 0 END AS PCICardiology

,Case when TXTYPE='SL' Then 1 else 0 end as TransplantSLT,
Case when TXTYPE='DL' Then 1 else 0 end as TransplantDLT,
Case when TXTYPE='H' Then 1 else 0 end as TransplantHT,
case when CCPeriod.SourceSpellNo in (
--JC - Note that these are up to date as at 31st March 2013
'150082579',
'150118810',
'150119575',
'150169207',
'150169892',
'150189458',
'150199225',
'150204123',
'150217673',
'150232045',
'150288186',
'150305885',
'150315650',
'150342401',
'150324280',
'150335131',
'150342407',
'150343130',
'150361421',
'150385499',
'150394035',
'150400193',
'150401854',
'150404513',
'150409385',
'150416332',
'150416829'
) then 1 ELSE 0 END AS TransplantVAD,
CASE when CCPeriod.SourceSpellNo in

(
--JC - Note that these are up to date as at 31st March 2013
'150328358'
,'150229905'
,'150241005'
,'150249911'
,'150250700'
,'150261339'
,'150261342'
,'150267033'
,'150267013'
,'150261324'
,'150267029'
,'150274659'
,'150274654'
,'150268697'
,'150268685'
,'150274660'
,'150280836'
,'150276860'
,'150276834'
,'150278828'
,'150278818'
,'150275212'
,'150280824'
,'150290995'
,'150286772'
,'150300813'
,'150297097'
,'150301121'
,'150304034'
,'150303659'
,'150304011'
,'150286786'
,'150303195'
,'150314305'
,'150314306'
,'150308256'
,'150308257'
,'150320173'
,'150318250'
,'150314010'
,'150318251'
,'150320207'
,'150322146'
,'150322319'
,'150234708'
,'150149651'
,'150157050'
,'150146036'
,'150157079'
,'150160028'
,'150166755'
,'150179480'
,'150179383'
,'150199166'
,'150229905',
'150350229',
'150328358',
'150323030',
'150331840',
'150334906',
'150338011',
'150340062',
'150350250',
'150352139',
'150336483',
'150358320',
'150358486',
'150360383',
'150360384',
'150364204',
'150370890',
'150370761',
'150372053',
'150378022',
'150378023',
'150380616',
'150382943',
'150384849',
'150392523',
'150397134',
'150399464',
'150403571',
'150403285',
'150405204',
'150403577',
'150405582',
'150412629',
'150408640',
'150412301',
'150408638',
'150377294',
'150408640',
'150408638',
'150412629',
'150412301',
'150377294',
'150416662',
'150416637',
'150414462',
'150420779',
'150420793',
'150424875',
'150430763',
'150426982',
'150422701',
'150429542'

)
Then 1 else 0 END as TAVI,


Case WHEN ccperiod.SourceSpellNo in 


(select distinct sourcespellno

from whreporting.APC.Episode episode
inner join whreporting.APC.ClinicalCoding coding
on coding.EncounterRecno=episode.EncounterRecno


where  (
PriamryProcedureCode >= 'K401' AND PriamryProcedureCode <= 'K469') 
OR (SecondaryProcedureCode1 >= 'K401' AND SecondaryProcedureCode1 <= 'K469') 
OR (SecondaryProcedureCode2 >= 'K401' AND SecondaryProcedureCode2 <= 'K469') 
OR(SecondaryProcedureCode3 >= 'K401' AND SecondaryProcedureCode3 <= 'K469') 
OR(SecondaryProcedureCode4 >= 'K401' AND SecondaryProcedureCode4 <= 'K469') 
OR(SecondaryProcedureCode5 >= 'K401' AND SecondaryProcedureCode5 <= 'K469'))
THEN 1 else 0 end as CABG,

Case WHEN ccperiod.SourceSpellNo in 


(select distinct sourcespellno

from whreporting.APC.Episode episode
inner join whreporting.APC.ClinicalCoding coding
on coding.EncounterRecno=episode.EncounterRecno


where  (PriamryProcedureCode >= 'K241' AND PriamryProcedureCode <= 'K369') OR
(SecondaryProcedureCode1 >= 'K241' AND SecondaryProcedureCode1 <= 'K369') 
OR (SecondaryProcedureCode2 >= 'K241' AND SecondaryProcedureCode2 <= 'K369') 
OR(SecondaryProcedureCode3 >= 'K241' AND SecondaryProcedureCode3 <= 'K369') 
OR(SecondaryProcedureCode4 >= 'K241' AND SecondaryProcedureCode4 <= 'K369') 
OR(SecondaryProcedureCode5 >= 'K241' AND SecondaryProcedureCode5 <= 'K369'
))
THEN 1 else 0 end as Valve,

Case When CCPeriod.SourceSpellNo in 


 (select sourcespellno

from whreporting.APC.Episode episode
inner join whreporting.APC.ClinicalCoding coding
on coding.EncounterRecno=episode.EncounterRecno
where  (PriamryProcedureCode='X581'
OR SecondaryProcedureCode1='X581'
OR SecondaryProcedureCode2='X581'
OR SecondaryProcedureCode3='X581'
OR SecondaryProcedureCode4='X581'
OR SecondaryProcedureCode5='X581'
OR SecondaryProcedureCode6='X581'
OR SecondaryProcedureCode7='X581'
OR SecondaryProcedureCode8='X581'
OR SecondaryProcedureCode9='X581'
OR SecondaryProcedureCode10='X581'
OR SecondaryProcedureCode11='X581'))

THEN 1 else 0 end as ECMO,
sum(Stay.CCPStay) as ActualBedDays


from 

Lorenzo.dbo.ExtractCriticalCarePeriodStay stay
inner join WHReporting.APC.CriticalCarePeriod CCPeriod on CCPeriod.SourceCCPNo=stay.SourceCCPNo
LEFT JOIN WHReporting.APC.Episode EPI
ON CCPeriod.SourceEncounterNo = EPI.EpisodeUniqueID
lEFT JOIN (select distinct txtype,spell2 from WHReporting.dbo.TXPatients Transplants) as Trans ON Trans.Spell2=CCPeriod.SourceSpellNo



left join WHREPORTING.LK.Calendar cal on cal.TheDate=cast(stay.StayDate as date)
LEFT OUTER JOIN WHReporting.APC.ClinicalCoding CC
ON EPI.EncounterRecno = CC.EncounterRecno

where SPONT_REFNO_CODE in ('CTCU', 'CT Transplant', 'ICA','CTTRAN')--01/06/2016 CM Added CTTRAN as this is the new ward code for CT Transplant as per ward code changes for JAC)
--and DischargeDate is not null
Group by

cal.TheMonth,
CCPeriod.SourceSpellNo
,SPONT_REFNO_CODE
,EPI.[SpecialtyCode(Function)]
,case when [SpecialtyCode(Function)]=173 and not(ConsultantCode = 'C2386780' 
AND PriamryProcedureCode like 'K%') then 1 else 0 end
,Case when TXTYPE='SL' Then 1 else 0 end,
Case when TXTYPE='DL' Then 1 else 0 end,
Case when TXTYPE='H' Then 1 else 0 end,
case when CCPeriod.SourceSpellNo in (

'150082579',
'150118810',
'150119575',
'150169207',
'150169892',
'150189458',
'150199225',
'150204123',
'150217673',
'150232045',
'150288186',
'150305885',
'150315650',
'150342401',
'150324280',
'150335131',
'150342407',
'150343130',
'150361421',
'150385499',
'150394035',
'150400193',
'150401854',
'150404513',
'150409385',
'150416332',
'150416829',
'150324280',
'150315650',
'150335131',
'150305885',
'150342407',
'150342401',
'150343130',
'150361421',
'150385499',
'150404513',
'150400193',
'150409385',
'150401854',
'150394035',
'150416332',
'150416829') then 1 ELSE 0 END ,
CASE when CCPeriod.SourceSpellNo in

(
'150328358'
,'150229905'
,'150241005'
,'150249911'
,'150250700'
,'150261339'
,'150261342'
,'150267033'
,'150267013'
,'150261324'
,'150267029'
,'150274659'
,'150274654'
,'150268697'
,'150268685'
,'150274660'
,'150280836'
,'150276860'
,'150276834'
,'150278828'
,'150278818'
,'150275212'
,'150280824'
,'150290995'
,'150286772'
,'150300813'
,'150297097'
,'150301121'
,'150304034'
,'150303659'
,'150304011'
,'150286786'
,'150303195'
,'150314305'
,'150314306'
,'150308256'
,'150308257'
,'150320173'
,'150318250'
,'150314010'
,'150318251'
,'150320207'
,'150322146'
,'150322319'
,'150234708'
,'150149651'
,'150157050'
,'150146036'
,'150157079'
,'150160028'
,'150166755'
,'150179480'
,'150179383'
,'150199166'
,'150229905',
'150350229',
'150328358',
'150323030',
'150331840',
'150334906',
'150338011',
'150340062',
'150350250',
'150352139',
'150336483',
'150358320',
'150358486',
'150360383',
'150360384',
'150364204',
'150370890',
'150370761',
'150372053',
'150378022',
'150378023',
'150380616',
'150382943',
'150384849',
'150392523',
'150397134',
'150399464',
'150403571',
'150403285',
'150405204',
'150403577',
'150405582',
'150412629',
'150408640',
'150412301',
'150408638',
'150377294'
)
Then 1 else 0 END ,


/*Case WHEN ccperiod.SourceSpellNo in 


(select distinct sourcespellno

from whreporting.APC.Episode episode
inner join whreporting.APC.ClinicalCoding coding
on coding.EncounterRecno=episode.EncounterRecno


where  (
PriamryProcedureCode >= 'K401' AND PriamryProcedureCode <= 'K469') 
OR (SecondaryProcedureCode1 >= 'K401' AND SecondaryProcedureCode1 <= 'K469') 
OR (SecondaryProcedureCode2 >= 'K401' AND SecondaryProcedureCode2 <= 'K469') 
OR(SecondaryProcedureCode3 >= 'K401' AND SecondaryProcedureCode3 <= 'K469') 
OR(SecondaryProcedureCode4 >= 'K401' AND SecondaryProcedureCode4 <= 'K469') 
OR(SecondaryProcedureCode5 >= 'K401' AND SecondaryProcedureCode5 <= 'K469'))
THEN 1 else 0 end,

Case WHEN ccperiod.SourceSpellNo in 


(select distinct sourcespellno

from whreporting.APC.Episode episode
inner join whreporting.APC.ClinicalCoding coding
on coding.EncounterRecno=episode.EncounterRecno


where  (PriamryProcedureCode >= 'K241' AND PriamryProcedureCode <= 'K369') OR
(SecondaryProcedureCode1 >= 'K241' AND SecondaryProcedureCode1 <= 'K369') 
OR (SecondaryProcedureCode2 >= 'K241' AND SecondaryProcedureCode2 <= 'K369') 
OR(SecondaryProcedureCode3 >= 'K241' AND SecondaryProcedureCode3 <= 'K369') 
OR(SecondaryProcedureCode4 >= 'K241' AND SecondaryProcedureCode4 <= 'K369') 
OR(SecondaryProcedureCode5 >= 'K241' AND SecondaryProcedureCode5 <= 'K369'
))
THEN 1 else 0 end,

Case When CCPeriod.SourceSpellNo in 


 (select sourcespellno

from whreporting.APC.Episode episode
inner join whreporting.APC.ClinicalCoding coding
on coding.EncounterRecno=episode.EncounterRecno
where  (PriamryProcedureCode='X581'
OR SecondaryProcedureCode1='X581'
OR SecondaryProcedureCode2='X581'
OR SecondaryProcedureCode3='X581'
OR SecondaryProcedureCode4='X581'
OR SecondaryProcedureCode5='X581'
OR SecondaryProcedureCode6='X581'
OR SecondaryProcedureCode7='X581'
OR SecondaryProcedureCode8='X581'
OR SecondaryProcedureCode9='X581'
OR SecondaryProcedureCode10='X581'
OR SecondaryProcedureCode11='X581'))

THEN 1 else 0 end,*/
CARE_LEVEL,
cal.FinancialMonthKey