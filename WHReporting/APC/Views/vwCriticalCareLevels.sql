﻿CREATE VIEW [APC].[vwCriticalCareLevels]
AS
SELECT

CASE WHEN SPONT_REFNO_CODE IN ('CTCU', 'CT Transplant','CTTRAN')--01/06/2016 CM Added CTTRAN as this is the new ward code for CT Transplant as per ward code changes for JAC
 THEN 'CTCU'
WHEN SPONT_REFNO_CODE = 'ICA' THEN 'ACUTE - ICU'
ELSE SPONT_REFNO_CODE END AS Ward,

CARE_LEVEL,
left(cal.themonth,3) as TheMonth,
cal.FinancialYear,

SUM(CCPStay) AS BedDays

FROM Lorenzo.dbo.ExtractCriticalCarePeriodStay cc
inner join LK.Calendar cal on cal.TheDate=cast(cc.StayDate as DATE)
WHERE SPONT_REFNO_CODE IN ('CTCU','CT Transplant', 'ICA','CTTRAN')--01/06/2016 CM Added CTTRAN as this is the new ward code for CT Transplant as per ward code changes for JAC
AND StayDate >= '2010-04-01' and cast(StayDate as date) < DATEADD(DAY,((DATEPART(DAY,GETDATE()))-1)*-1,CAST(GETDATE()AS DATE))
GROUP BY
CASE WHEN SPONT_REFNO_CODE IN ('CTCU', 'CT Transplant','CTTRAN')--01/06/2016 CM Added CTTRAN as this is the new ward code for CT Transplant as per ward code changes for JAC
 THEN 'CTCU'
WHEN SPONT_REFNO_CODE = 'ICA' THEN 'ACUTE - ICU'
ELSE SPONT_REFNO_CODE END,
CARE_LEVEL,

left(cal.themonth,3),
cal.FinancialYear