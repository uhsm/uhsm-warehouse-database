﻿CREATE VIEW [APC].[vwCriticalCareOrgans]
AS
SELECT

CASE WHEN SPONT_REFNO_CODE IN ('CTCU', 'CT Transplant','CTTRAN')--01/06/2016 CM Added CTTRAN as this is the new ward code for CT Transplant as per ward code changes for JAC 
		THEN 'CTCU'
WHEN SPONT_REFNO_CODE = 'ICA' THEN 'ACUTE - ICU'
ELSE SPONT_REFNO_CODE END AS Ward,
OrgansSupported,

CASE WHEN EPI.AdmissionType IN ('Emergency','Non-Elective','Maternity') THEN 'Non-Elective'
ELSE EPI.AdmissionType END AS 'PATCLASS',

MONTH(StayDate) AS TheMonth,


SUM(CCPStay) AS BedDays

FROM Lorenzo.dbo.ExtractCriticalCarePeriodStay Stay
LEFT OUTER JOIN [WHREPORTING].[APC].[CriticalCarePeriod] CritP

ON Stay.SourceCCPNo = CritP.SourceCCPNo

LEFT OUTER JOIN WHREPORTING.APC.Episode EPI
ON EPI.EpisodeUniqueID = CritP.SourceEncounterNo
left join lk.Calendar cal on cal.TheDate=cast(stayDate as DATE)
WHERE SPONT_REFNO_CODE IN ('CTCU','CT Transplant', 'ICA','CTTRAN')--01/06/2016 CM Added CTTRAN as this is the new ward code for CT Transplant as per ward code changes for JAC
AND cast(StayDate as date) between
DATEADD(DAY,((DATEPART(DAY,GETDATE()))-1)*-1,dateadd(month,-12,CAST(GETDATE()AS DATE)))
 and DATEADD(DAY,((DATEPART(DAY,GETDATE())))*-1,CAST(GETDATE()AS DATE))
GROUP BY
CASE WHEN SPONT_REFNO_CODE IN ('CTCU', 'CT Transplant','CTTRAN')--01/06/2016 CM Added CTTRAN as this is the new ward code for CT Transplant as per ward code changes for JAC 
		THEN 'CTCU'
WHEN SPONT_REFNO_CODE = 'ICA' THEN 'ACUTE - ICU'
ELSE SPONT_REFNO_CODE END,
OrgansSupported,
CASE WHEN EPI.AdmissionType IN ('Emergency','Non-Elective','Maternity') THEN 'Non-Elective'
ELSE EPI.AdmissionType END,
MONTH(StayDate)