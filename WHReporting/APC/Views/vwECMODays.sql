﻿CREATE VIEW [APC].[vwECMODays]
AS
select cal.FinancialMonthKey,EDays.[Spell number] as SpellNo,EDays.ECMOStart,EDays.ECMOEnd,1 as ECMODays,'CTCU' as Ward,3 as LevelOfCare

from WHREPORTING.APC.ECMODays AS EDays
inner join  
 
 LK.Calendar AS cal ON cal.TheDate BETWEEN CAST(EDays.ECMOStart AS DATE) AND 
                                              CAST(EDays.ECMOEnd AS DATE)