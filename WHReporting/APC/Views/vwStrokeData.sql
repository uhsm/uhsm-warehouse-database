﻿CREATE VIEW [APC].[vwStrokeData]
AS
Select 
DataRaw.NHSNo,
DataRaw.SourceSpellNo,
RM2No,
PatientForename as Forename ,
PatientSurname as Surname,
SUM(isnull(AEDATA.TimeInAE,0)) as TimeInAE,
SUM(TimeOnCC) as TimeOnCC,
sum(LOS) as LOS,
SUM(TimeOnStrokeWards) AS TimeOnStrokeWards,
sum(TimeOnStrokeWards)/
sum(case when (LOS+isnull(AEDATA.TimeInAE,0)-isnull(TimeOnCC,0))=0 then 1 else (LOS+isnull(AEDATA.TimeInAE,0)-isnull(TimeOnCC,0)) end)*1.0 as StrokeUnitPerc,
case
when  
(sum(LOS+AEDATA.TimeInAE)-sum(TimeOnCC))<1 THEN 'Excluded'
when sc.Exclude='Y' then 'Excluded'
when sum(TimeOnStrokeWards)=0 then 'Not on Stroke Ward'
when 
sum(TimeOnStrokeWards)/
sum(case when (LOS+isnull(AEDATA.TimeInAE,0)-isnull(TimeOnCC,0))=0 then 1 else (LOS+isnull(AEDATA.TimeInAE,0)-isnull(TimeOnCC,0)) end)*1.0 <0.9 then 'Breach'
else 'No Breach' end as Breach,
cal.FinancialYear,
cal.TheMonth,
cal.financialmonthkey,
MONTH(cal.thedate) as [Month],
AdmissionDateTime as AdmissionDate,
DischargeDateTime as DischargeDate,
PCT,
SpellCCG as CCG,
AdmissionMethodCode,
DateAddedToList=cast(GETDATE() as date),
Comments=isnull(SC.comments,''),
AEData.AEArrivalTime,
AEData.FirstWardStartTime,
AEData.AdmissionWard,
AEData.AEArrivaltoWardMinutes,
AEData.AETimeFromArivalToWardBed,
--AEData.AETimeFromArivalToStrokeBed,
case
when  
(sum(LOS+AEDATA.TimeInAE)-sum(TimeOnCC))<1 THEN 'Excluded'
when sc.Exclude='Y' then 'Excluded' else AEData.FourHrAdmission end as FourHrAdmission

from


(select 
pat.PatientSurname,
pat.PatientForename,
s.SpellCCG,
s.pct,
AdmissionWard,
ep.SourceSpellNo,
ep.admissiondate,
s.NHSNo,
s.FaciltyID as RM2No,
ep.AdmissionDateTime,
ep.DischargeDateTime,
s.AdmissionMethodCode,
sum(DATEDIFF(minute,ep.EpisodeStartDateTime,ep.EpisodeEndDateTime))/1440.00 as LOS,
ISNULL(TimeonStroke.MinOnStroke/1440.00,0) as TimeOnStrokeWards,
ISNULL(sum(TimeonCC.MinOnCC)/1440.00,0) as TimeOnCC
,StrokeBedTime
from APC.Episode ep
left join APC.Spell s on s.SourceSpellNo=ep.SourceSpellNo
left join APC.Patient pat on pat.EncounterRecno=ep.EncounterRecno
LEFT JOIN 
(Select SourceSpellNo,min(StartTime) as StrokeBedTime,sum(datediff(minute,starttime,endtime)) AS MinOnStroke
from
APC.WardStay stay where WardCode in ('F15S','F4S')
group by SourceSpellNo
) as TimeonStroke
on TimeonStroke.SourceSpellNo=s.SourceSpellNo

LEFT JOIN 
(Select SourceSpellNo, starttime as starttime,sum(datediff(minute,starttime,endtime)) AS MinOnCC
from
APC.WardStay stay where WardCode in ('CTCU','ICU','ICA','CT Transplant','CTTRAN')--01/06/2016 CM Added CTTRAN as this is the new ward code for CT Transplant as per ward code changes for JAC
group by SourceSpellNo, StartTime) as TimeonCC
on TimeonCC.SourceSpellNo=s.SourceSpellNo
and TimeonCC.StartTime between ep.EpisodeStartDateTime and coalesce(ep.EpisodeEndDateTime,getdate())



where left(PrimaryDiagnosisCode,3) in ('I61','I63','I64')

AND ep.AdmissionDate >'2012-03-31'
and SpecialtyCode<>'420'
and ep.DischargeDate is not null
--and ep.SourceSpellNo=150448298



group by
pat.PatientSurname,
pat.PatientForename,
s.SpellCCG,
s.pct,
s.NHSNo,
ep.admissiondate,
s.FaciltyID,
ep.SourceSpellNo,
ep.AdmissionDateTime,
ep.DischargeDateTime,
s.AdmissionMethodCode,
--AE.ArrivalTime,
--AE.AttendanceConclusionTime,
/*isnull(DATEDIFF(MINUTE,AE.ArrivalTime,
AE.AttendanceConclusionTime)/1440.00,0) ,*/
ISNULL(TimeonStroke.MinOnStroke/1440.00,0)
--ISNULL(TimeonCC.MinOnCC/1440.00,0) 
,AdmissionWard
,StrokeBedTime
) as DataRaw


left join whreporting.lk.calendar cal on cal.thedate=DataRaw.admissiondate

LEFT JOIN 


(Select distinct
cal.TheMonth,
cal.financialmonthkey,
s.NHSNo,
s.SourceSpellNo
,ArrivalTime as AEArrivalTime
,s.AdmissionDateTime as FirstWardStartTime
,AdmissionWard
,cast(StrokeBedTime-ArrivalTime as Time) as AETimeFromArivalToStrokeBed
,datediff(minute,ArrivalTime,s.AdmissionDateTime)as AEArrivalToWardMinutes
,datediff(minute,ArrivalTime,AttendanceConclusionTime) as timeInAEMinutes
,datediff(minute,ArrivalTime,AttendanceConclusionTime)/1440.00 as timeInAE
,cast(s.AdmissionDateTime-ArrivalTime as Time) as AETimeFromArivalToWardBed
,case when
isnull(datediff(minute,arrivaltime,s.admissiondatetime),0)=0 then null when
isnull(datediff(minute,arrivaltime,s.admissiondatetime),0)>240 then 'Breach' else 'No Breach' end as FourHrAdmission

from APC.Episode ep
left join APC.Spell s on s.SourceSpellNo=ep.SourceSpellNo
left JOIN WH.AE.Encounter AE ON AE.NHSNumber=s.NHSNo
AND s.AdmissionDateTime between dateadd(minute,-360,AE.AttendanceConclusionTime) and  dateadd(minute,600,AE.AttendanceConclusionTime)
--and FirstEpisodeInSpell=1
LEFT JOIN 
(Select SourceSpellNo,min(StartTime) as StrokeBedTime,sum(datediff(minute,starttime,endtime)) AS MinOnStroke
from
APC.WardStay stay where WardCode in ('F15S','F4S')
group by SourceSpellNo
) as TimeonStroke
on TimeonStroke.SourceSpellNo=s.SourceSpellNo
left join whreporting.lk.calendar cal on cal.thedate=cast(s.admissiondatetime as date)

where left(PrimaryDiagnosisCode,3) in ('I61','I63','I64')
and FirstEpisodeInSpell=1
AND ep.AdmissionDate >'2012-03-31'
and SpecialtyCode<>'420'
and ep.DischargeDate is not null
and AE.AttendanceDisposalCode='AD'
--and ep.SourceSpellNo=150448298
and ArrivalTime is not null) AS AEData on AEData.SourceSpellNo=DataRaw.SourceSpellNo
left join apc.strokecomments sc on sc.SourceSpellNo=DataRaw.SourceSpellNo



group by
PatientForename,
PatientSurname,
cal.TheMonth,
cal.FinancialYear,
--cal.financialmonthkey,
MONTH(cal.thedate),
cal.financialmonthkey,
DataRaw.NHSNo,
DataRaw.SourceSpellNo,
RM2No,
AdmissionMethodCode,
AdmissionDateTime
,DischargeDateTime,
SpellCCG ,
PCT,
AEData.AEArrivalTime,
AEData.FirstWardStartTime,
AEData.AdmissionWard,
AEData.AEArrivaltoWardMinutes,
AEData.AETimeFromArivalToStrokeBed,
AEData.AETimeFromArivalToWardBed,
AEData.FourHrAdmission,
sc.Exclude,

isnull(SC.comments,'')






--The SQL below is the changes John made on 22nd October 13.  When Mark validated the report, which was based
--on the query below, he said it didn't match and was much worse.  Therefore, I (Ijaz) have put back the original
--SQL back in (which is above
--Select 
--DataRaw.NHSNo,
--DataRaw.SourceSpellNo,
--RM2No,
--PatientForename as Forename ,
--PatientSurname as Surname,
--SUM(isnull(AEDATA.TimeInAE,0)) as TimeInAE,
--SUM(TimeOnCC) as TimeOnCC,
--sum(LOS) as LOS,
--SUM(TimeOnStrokeWards) AS TimeOnStrokeWards,
--sum(TimeOnStrokeWards)/
--sum(case when (LOS+isnull(AEDATA.TimeInAE,0)-isnull(TimeOnCC,0))=0 then 1 else (LOS+isnull(AEDATA.TimeInAE,0)-isnull(TimeOnCC,0)) end)*1.0 as StrokeUnitPerc,
--case
--when  
--(sum(LOS+AEDATA.TimeInAE)-sum(TimeOnCC))<1 THEN 'Excluded'
--when sc.Exclude='Y' then 'Excluded'
--when sum(TimeOnStrokeWards)=0 then 'Not on Stroke Ward'
--when 
--sum(TimeOnStrokeWards)/
--sum(case when (LOS+isnull(AEDATA.TimeInAE,0)-isnull(TimeOnCC,0))=0 then 1 else (LOS+isnull(AEDATA.TimeInAE,0)-isnull(TimeOnCC,0)) end)*1.0 <0.9 then 'Breach'
--else 'No Breach' end as Breach,
--cal.FinancialYear,
--cal.TheMonth,
--cal.financialmonthkey,
--MONTH(cal.thedate) as [Month],
--AdmissionDateTime as AdmissionDate,
--DischargeDateTime as DischargeDate,
--PCT,
--SpellCCG as CCG,
--AdmissionMethodCode,
--DateAddedToList=cast(GETDATE() as date),
--Comments=isnull(SC.comments,''),
--AEData.AEArrivalTime,
--AEData.FirstWardStartTime,
--AEData.AdmissionWard,
--AEData.AEArrivaltoWardMinutes,
--AEData.AETimeFromArivalToWardBed,
----AEData.AETimeFromArivalToStrokeBed,
--case
--when  
--(sum(LOS+AEDATA.TimeInAE)-sum(TimeOnCC))<1 THEN 'Excluded'
--when sc.Exclude='Y' then 'Excluded' else AEData.FourHrAdmission end as FourHrAdmission

--from


--(select 
--pat.PatientSurname,
--pat.PatientForename,
--s.SpellCCG,
--s.pct,
--AdmissionWard,
--ep.SourceSpellNo,
--ep.admissiondate,
--s.NHSNo,
--s.FaciltyID as RM2No,
--ep.AdmissionDateTime,
--ep.DischargeDateTime,
--s.AdmissionMethodCode,
--sum(DATEDIFF(minute,ep.EpisodeStartDateTime,ep.EpisodeEndDateTime))/1440.00 as LOS,
--ISNULL(sum(TimeonStroke.MinOnStroke)/1440.00,0) as TimeOnStrokeWards,
--ISNULL(sum(TimeonCC.MinOnCC)/1440.00,0) as TimeOnCC

--from APC.Episode ep
--left join APC.Spell s on s.SourceSpellNo=ep.SourceSpellNo
--left join APC.Patient pat on pat.EncounterRecno=ep.EncounterRecno
--LEFT JOIN 
--(Select episode.EpisodeUniqueID,sum(datediff(minute,starttime,endtime)) AS MinOnStroke
--from
--APC.WardStay stay 
--Inner join apc.episode episode on episode.SourceSpellNo = stay.SourceSpellNo
--And stay.starttime between episode.EpisodeStartDateTime and coalesce(episode.episodeenddatetime,getdate())
--where WardCode in ('F15S','F4S')
--group by episode.EpisodeUniqueID
--) as TimeonStroke
--on TimeonStroke.EpisodeUniqueID=ep.EpisodeUniqueID

--LEFT JOIN 
--(Select episode.EpisodeUniqueID,sum(datediff(minute,starttime,endtime)) AS MinOnCC
--from
--APC.WardStay stay 
--Inner join apc.episode episode on episode.SourceSpellNo = stay.SourceSpellNo
--And stay.starttime between episode.EpisodeStartDateTime and coalesce(episode.episodeenddatetime,getdate())

--where WardCode in ('CTCU','ICU','ICA','CT Transplant')
--group by episode.EpisodeUniqueID) as TimeonCC
--on TimeonCC.EpisodeUniqueID=ep.EpisodeUniqueID



--where left(PrimaryDiagnosisCode,3) in ('I61','I63','I64')

--AND ep.AdmissionDate >'2012-03-31'
--and SpecialtyCode<>'420'
--and ep.DischargeDate is not null
----and ep.SourceSpellNo=150450825



--group by
--pat.PatientSurname,
--pat.PatientForename,
--s.SpellCCG,
--s.pct,
--s.NHSNo,
--ep.admissiondate,
--s.FaciltyID,
--ep.SourceSpellNo,
--ep.AdmissionDateTime,
--ep.DischargeDateTime,
--s.AdmissionMethodCode,
----AE.ArrivalTime,
----AE.AttendanceConclusionTime,
--/*isnull(DATEDIFF(MINUTE,AE.ArrivalTime,
--AE.AttendanceConclusionTime)/1440.00,0) ,*/
--ISNULL(TimeonStroke.MinOnStroke/1440.00,0)
----ISNULL(TimeonCC.MinOnCC/1440.00,0) 
--,AdmissionWard
--) as DataRaw


--left join whreporting.lk.calendar cal on cal.thedate=DataRaw.admissiondate

--LEFT JOIN 


--(Select distinct
--cal.TheMonth,
--cal.financialmonthkey,
--s.NHSNo,
--s.SourceSpellNo
--,ArrivalTime as AEArrivalTime
--,s.AdmissionDateTime as FirstWardStartTime
--,AdmissionWard
--,cast(StrokeBedTime-ArrivalTime as Time) as AETimeFromArivalToStrokeBed
--,datediff(minute,ArrivalTime,s.AdmissionDateTime)as AEArrivalToWardMinutes
--,datediff(minute,ArrivalTime,AttendanceConclusionTime) as timeInAEMinutes
--,datediff(minute,ArrivalTime,AttendanceConclusionTime)/1440.00 as timeInAE
--,cast(s.AdmissionDateTime-ArrivalTime as Time) as AETimeFromArivalToWardBed
--,case when
--isnull(datediff(minute,arrivaltime,s.admissiondatetime),0)=0 then null when
--isnull(datediff(minute,arrivaltime,s.admissiondatetime),0)>240 then 'Breach' else 'No Breach' end as FourHrAdmission

--from APC.Episode ep
--left join APC.Spell s on s.SourceSpellNo=ep.SourceSpellNo
--left JOIN WH.AE.Encounter AE ON AE.NHSNumber=s.NHSNo
--AND s.AdmissionDateTime between dateadd(minute,-360,AE.AttendanceConclusionTime) and  dateadd(minute,600,AE.AttendanceConclusionTime)
----and FirstEpisodeInSpell=1
--LEFT JOIN 
--(Select SourceSpellNo,min(StartTime) as StrokeBedTime,sum(datediff(minute,starttime,endtime)) AS MinOnStroke
--from
--APC.WardStay stay where WardCode in ('F15S','F4S')
--group by SourceSpellNo
--) as TimeonStroke
--on TimeonStroke.SourceSpellNo=s.SourceSpellNo
--left join whreporting.lk.calendar cal on cal.thedate=cast(s.admissiondatetime as date)

--where left(PrimaryDiagnosisCode,3) in ('I61','I63','I64')
--and FirstEpisodeInSpell=1
--AND ep.AdmissionDate >'2012-03-31'
--and SpecialtyCode<>'420'
--and ep.DischargeDate is not null
--and AE.AttendanceDisposalCode='AD'
----and ep.SourceSpellNo=150448298
--and ArrivalTime is not null) AS AEData on AEData.SourceSpellNo=DataRaw.SourceSpellNo
--left join apc.strokecomments sc on sc.SourceSpellNo=DataRaw.SourceSpellNo



--group by
--PatientForename,
--PatientSurname,
--cal.TheMonth,
--cal.FinancialYear,
----cal.financialmonthkey,
--MONTH(cal.thedate),
--cal.financialmonthkey,
--DataRaw.NHSNo,
--DataRaw.SourceSpellNo,
--RM2No,
--AdmissionMethodCode,
--AdmissionDateTime
--,DischargeDateTime,
--SpellCCG ,
--PCT,
--AEData.AEArrivalTime,
--AEData.FirstWardStartTime,
--AEData.AdmissionWard,
--AEData.AEArrivaltoWardMinutes,
--AEData.AETimeFromArivalToStrokeBed,
--AEData.AETimeFromArivalToWardBed,
--AEData.FourHrAdmission,
--sc.Exclude,

--isnull(SC.comments,'')