﻿CREATE VIEW [APC].[vwTarnExclusionsbySpell]
AS
SELECT distinct spell.sourcespellno,
/*isnull(coding.PrimaryDiagnosisCode,'')+'-'+
+isnull(SecondaryDiagnosisCode1,'')+'-'+
+isnull(SecondaryDiagnosisCode2,'')+'-'+
+isnull(SecondaryDiagnosisCode3,'')+'-'+
+isnull(SecondaryDiagnosisCode4,'')+'-'+
+isnull(SecondaryDiagnosisCode5,'')+'-'+
+isnull(SecondaryDiagnosisCode6,'')+'-'+
+isnull(SecondaryDiagnosisCode7,'')+'-'+
+isnull(SecondaryDiagnosisCode8,'')+'-'+
+isnull(SecondaryDiagnosisCode9,'')+'-'+
+isnull(SecondaryDiagnosisCode10,'')+'-'+
+isnull(SecondaryDiagnosisCode11,'')+'-'+
+isnull(SecondaryDiagnosisCode12,'')+'-'+
+isnull(SecondaryDiagnosisCode13,'')+'-'+
+isnull(SecondaryDiagnosisCode14,'')+'-'+
+isnull(SecondaryDiagnosisCode15,'')+'-'+
+isnull(SecondaryDiagnosisCode16,'')+'-'+
+isnull(SecondaryDiagnosisCode17,'')+'-'+
+isnull(SecondaryDiagnosisCode18,'')+'-'+
+isnull(SecondaryDiagnosisCode19,'') as DiagnosisString,*/


case when isnull(coding.PrimaryDiagnosisCode,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=1)
or isnull(SecondaryDiagnosisCode1,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=1)
or isnull(SecondaryDiagnosisCode2,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=1)
or isnull(SecondaryDiagnosisCode3,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=1)
or isnull(SecondaryDiagnosisCode4,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=1)
or isnull(SecondaryDiagnosisCode5,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=1)
or isnull(SecondaryDiagnosisCode6,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=1)
or isnull(SecondaryDiagnosisCode7,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=1)
or isnull(SecondaryDiagnosisCode8,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=1)
or isnull(SecondaryDiagnosisCode9,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=1)
or isnull(SecondaryDiagnosisCode10,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=1)
or isnull(SecondaryDiagnosisCode11,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=1)
or isnull(SecondaryDiagnosisCode12,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=1)
or isnull(SecondaryDiagnosisCode13,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=1)
or isnull(SecondaryDiagnosisCode14,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=1)
or isnull(SecondaryDiagnosisCode15,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=1)
or isnull(SecondaryDiagnosisCode16,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=1)
or isnull(SecondaryDiagnosisCode17,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=1)
or isnull(SecondaryDiagnosisCode18,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=1)
or isnull(SecondaryDiagnosisCode19,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=1)
then 1 else 0 end as Blue

,
case when isnull(coding.PrimaryDiagnosisCode,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=3)
or isnull(SecondaryDiagnosisCode1,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=3)
or isnull(SecondaryDiagnosisCode2,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=3)
or isnull(SecondaryDiagnosisCode3,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=3)
or isnull(SecondaryDiagnosisCode4,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=3)
or isnull(SecondaryDiagnosisCode5,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=3)
or isnull(SecondaryDiagnosisCode6,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=3)
or isnull(SecondaryDiagnosisCode7,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=3)
or isnull(SecondaryDiagnosisCode8,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=3)
or isnull(SecondaryDiagnosisCode9,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=3)
or isnull(SecondaryDiagnosisCode10,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=3)
or isnull(SecondaryDiagnosisCode11,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=3)
or isnull(SecondaryDiagnosisCode12,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=3)
or isnull(SecondaryDiagnosisCode13,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=3)
or isnull(SecondaryDiagnosisCode14,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=3)
or isnull(SecondaryDiagnosisCode15,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=3)
or isnull(SecondaryDiagnosisCode16,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=3)
or isnull(SecondaryDiagnosisCode17,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=3)
or isnull(SecondaryDiagnosisCode18,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=3)
or isnull(SecondaryDiagnosisCode19,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=3)

and datediff( year, DateOfBirth, spell.AdmissionDate ) - 
                  case when 100 * month( spell.AdmissionDate ) + 
                  day( spell.AdmissionDate ) < 100 * month( DateOfBirth ) + 
                  day( DateOfBirth ) then 1 else 0 end>=65 then 1 else 0 end as Red


,case when isnull(coding.PrimaryDiagnosisCode,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=2)
or isnull(SecondaryDiagnosisCode1,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=2)
or isnull(SecondaryDiagnosisCode2,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=2)
or isnull(SecondaryDiagnosisCode3,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=2)
or isnull(SecondaryDiagnosisCode4,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=2)
or isnull(SecondaryDiagnosisCode5,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=2)
or isnull(SecondaryDiagnosisCode6,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=2)
or isnull(SecondaryDiagnosisCode7,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=2)
or isnull(SecondaryDiagnosisCode8,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=2)
or isnull(SecondaryDiagnosisCode9,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=2)
or isnull(SecondaryDiagnosisCode10,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=2)
or isnull(SecondaryDiagnosisCode11,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=2)
or isnull(SecondaryDiagnosisCode12,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=2)
or isnull(SecondaryDiagnosisCode13,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=2)
or isnull(SecondaryDiagnosisCode14,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=2)
or isnull(SecondaryDiagnosisCode15,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=2)
or isnull(SecondaryDiagnosisCode16,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=2)
or isnull(SecondaryDiagnosisCode17,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=2)
or isnull(SecondaryDiagnosisCode18,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=2)
or isnull(SecondaryDiagnosisCode19,'') in (select ICD10Code from AE.TARNExclusionDiagnoses where Exclude=2)
then 1 else 0 end as Yellow



FROM APC.Spell spell

inner join APC.Episode ep
on spell.EncounterRecno=ep.EncounterRecno
inner join APC.Patient p on p.EncounterRecno=ep.EncounterRecno
inner join APC.ClinicalCoding coding on coding.EncounterRecno=ep.EncounterRecno
--left join AE.TARNExclusionDiagnoses tarn on tarn.ICD10Code=coding.PrimaryDiagnosisCode




WHERE (DATEDIFF(Hour,spell.Admissiondatetime,spell.Dischargedatetime)>72
or DischargeMethodNHSCode=4
or AdmissionMethodNHSCode=81
OR DischargeDestinationNHSCode between 49 and 53
or AdmissionWard in ('CARDIO - ICU','ACUTE - ICU','CT TRANSPLANT','BURNS INTENSIVE CARE','CTCU','CTTRAN')--01/06/2016 CM Added CTTRAN as this is the new ward code for CT Transplant as per ward code changes for JAC 

)
and spell.AdmissionDate>'2012-01-01'

--and FirstEpisodeInSpell=1