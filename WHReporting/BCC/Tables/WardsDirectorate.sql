﻿CREATE TABLE [BCC].[WardsDirectorate](
	[Code] [nvarchar](255) NULL,
	[Desc] [nvarchar](255) NULL,
	[WardCode] [nvarchar](255) NULL,
	[WardName] [nvarchar](255) NULL,
	[Directorate] [nvarchar](255) NULL
) ON [PRIMARY]