﻿-- =============================================
-- Author:		CMan 
-- Create date: 27/12/2013
-- Description:	Reporting on AutoFate data.  Stored Procedure for Patient Level Drill Through SSRS report 'Fated Transfusions by Location - Patient Level'.
--				Requested by MGerrard. Spec discussed with Emma Milser.
--				RD438
-- =============================================
CREATE PROCEDURE [BLOODTRACK].[ReportFatedTransfusionsByLocation_PatientLevel]
@Loc varchar(50),
@fated varchar(50),
@Yearmonth varchar(50)
AS
BEGIN






 
-- --------------------------
----Fated List Temp table 
----------------------------
----Drop temp table if already exists 
--IF Object_id('tempdb..#FT') > 0
--  DROP TABLE #FT

----create temp table from the data provided  with an RM2 number column split out from the patient's name and a unique id to use
--CREATE TABLE #FT
--  (
--     ID               INT IDENTITY (1, 1),
--     patientno        VARCHAR(20),
--     NHSno            VARCHAR(10),
--     OriginalLocation VARCHAR(50),--this is the location which comes from the Blood Track Report
--     Location         VARCHAR(50),--this is the mapped location from the  WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping table
--     LocationCode     VARCHAR(50),--this is the mapped location code from the  WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping table
--     Division         VARCHAR(50),
--     BloodUnit        VARCHAR(50),
--     Product          VARCHAR(100),
--     Unit             VARCHAR(50),
--     Date             DATETIME,
--     Patient          VARCHAR(100)
--  )

--INSERT INTO #FT
--SELECT CASE
--         WHEN Len(CONVERT(VARCHAR(20), CONVERT(BIGINT, CASE
--                                                         WHEN Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '') LIKE '%[a-z]%' THEN NULL
--                                                         ELSE Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '')
--                                                       END))) = 10 THEN NULL
--         ELSE 'RM2' + CONVERT(VARCHAR(20), CONVERT(BIGINT, CASE WHEN Replace(Substring(patient, Charindex('(', patient)+1, Len(patient)), ')', '') LIKE '%[a-z]%' THEN NULL ELSE Replace(Substring(patient, Charindex('(', patient)+1, Len(patient)), ')', '') END))
--       END AS patientno,
--       CASE
--         WHEN Len(CONVERT(VARCHAR(20), CONVERT(BIGINT, CASE
--                                                         WHEN Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '') LIKE '%[a-z]%' THEN NULL
--                                                         ELSE Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '')
--                                                       END))) = 10 THEN CONVERT(VARCHAR(20), CONVERT(BIGINT, CASE
--                                                                                                               WHEN Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '') LIKE '%[a-z]%' THEN NULL
--                                                                                                               ELSE Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '')
--                                                                                                             END))
--         ELSE NULL
--       END AS NHSNo,
--       a.Location,
--       b.wardname,--this is the mapped wardname from the PAS to Blood Track Location mapping table
--       b.wardcode,--this is the mapped wardcode from the PAS to Blood Track Location mapping table
--       b.division,--this is the division from the PAS to Blood Track Location mapping table - mappings approved by Emma Milser
--       a.[Blood Unit],
--       a.Product,
--       a.[Unit Fate],
--       a.Date,
--       a.Patient
--FROM   WHREPORTING.dbo.TransfusionFatedUnits a
--       LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping b
--                    ON Isnull(a.Location, 'x') = b.MappedAutoTrackWard 


------------------------------------
----Temp table for the last ward per spell where the ward is not Discharge Lounge
------------------------------------
----To be used in the final query to identify the last ward for the patient's transfusion where no ward can be identified over the period of the transfusion date

----Drop temp table if already exists 
--IF Object_id('tempdb..#ws') > 0
--  DROP TABLE #ws

--CREATE TABLE #ws
--  (
--     SourcePatientNo   VARCHAR(20),
--     NHSNo             VARCHAR(20),
--     SourceSpellNo     VARCHAR(20),
--     FaciltyID         VARCHAR(20),
--     StartTime         DATETIME,
--     EndTime           DATETIME,
--     AdmissionDateTime DATETIME,
--     DischargeDateTime DATETIME,
--     WardCode          VARCHAR(20),
--     WardName          VARCHAR(50)
--  )

--INSERT INTO #ws
--SELECT ws.SourcePatientNo,
--       sp.NHSNo,
--       ws.SourceSpellNo,
--       sp.FaciltyID,
--       ws.StartTime,
--       ws.EndTime,
--       sp.AdmissionDateTime,
--       sp.DischargeDateTime,
--       ws.WardCode,
--       mp.WardName
--FROM   (SELECT SourceSpellNo,
--               Max(EndTime) MaxEndTime
--        FROM   WHREPORTING.APC.WardStay
--        WHERE  Isnull(WardCode, 'x') <> 'DL'
--        GROUP  BY SourceSpellNo) a
--       LEFT OUTER JOIN WHREPORTING.APC.WardStay ws
--                    ON a.SourceSpellNo = ws.SourceSpellNo
--                       AND a.MaxEndTime = ws.EndTime
--       LEFT OUTER JOIN WHREPORTING.APC.Spell sp
--                    ON ws.SourceSpellNo = sp.SourceSpellNo
--       LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping mp
--                    ON ws.WardCode = mp.PasWardCode



-------------------------------
----Fated data - next ward after transfusion date
------------------------------
----Temp table identifying the admission ward for the next spell admission after the transfusion date
----to be used to identify a ward for those transfusion records where no location can be identified even after trying to match to a wardstay which covers the transfusion date
----and looking back to last spell ward
----Only looks at those records in the fated data which have an admission date which is after the tranfusion date to grab the date, time and ward of the next spell
----then takes the admission ward details of the first admission which is after the transfusion date

----Drop temp table if already exists 
--IF Object_id('tempdb..#FT_next_sp') > 0
--  DROP TABLE #FT_next_sp

--CREATE TABLE #FT_next_sp
--  (
--     ID                INT,
--     patientno         VARCHAR(20),
--     NHSno             VARCHAR(20),
--     Location          VARCHAR(50),
--     BloodUnit         VARCHAR(50),
--     Product           VARCHAR(100),
--     Unit              VARCHAR(50),
--     Date              DATETIME,
--     Patient           VARCHAR(50),
--     SourcePatientNo   VARCHAR(20),
--     AdmissionDateTime DATETIME,
--     DischargeDateTime DATETIME,
--     AdmissionWardCode VARCHAR(20),
--     AdmissionWardName VARCHAR(50)
--  )

--INSERT INTO #FT_next_sp
--SELECT min_sp.ID,
--       min_sp.patientno,
--       min_sp.NHSno,
--       min_sp.Location,
--       min_sp.BloodUnit,
--       min_sp.Product,
--       min_sp.Unit,
--       min_sp.Date,
--       min_sp.Patient,
--       min_sp.SourcePatientNo,
--       Spell.AdmissionDateTime,
--       Spell.DischargeDateTime,
--       Spell.AdmissiontWardCode,
--       Wdmap.wardname AS AdmissionWardName
--FROM   (SELECT #FT.ID,
--               #FT.patientno,
--               #FT.NHSno,
--               #FT.Location,
--               #FT.BloodUnit,
--               #FT.Product,
--               #FT.Unit,
--               #FT.Date,
--               #FT.Patient,
--               sp.SourcePatientNo,
--               Min(AdmissionDateTime) min_admdate
--        FROM   #FT
--               LEFT OUTER JOIN WHREPORTING.APC.spell sp
--                            ON Isnull(#FT.patientno, 'x') = sp.faciltyID
--                               AND sp.AdmissionDateTime > #FT.Date
--        WHERE  #FT.NHSno IS NULL
--        GROUP  BY #FT.ID,
--                  #FT.patientno,
--                  #FT.NHSno,
--                  #FT.Location,
--                  #FT.BloodUnit,
--                  #FT.Product,
--                  #FT.Unit,
--                  #FT.Date,
--                  #FT.Patient,
--                  sp.SourcePatientNo
--        UNION ALL
--        SELECT #FT.ID,
--               #FT.patientno,
--               #FT.NHSno,
--               #FT.Location,
--               #FT.BloodUnit,
--               #FT.Product,
--               #FT.Unit,
--               #FT.Date,
--               #FT.Patient,
--               sp.SourcePatientNo,
--               Min(AdmissionDateTime) min_admdate
--        FROM   #FT
--               LEFT OUTER JOIN WHREPORTING.APC.spell sp
--                            ON #FT.NHSno = sp.NHSNo
--                               AND sp.AdmissionDateTime > #FT.Date
--        WHERE  #FT.NHSno IS NOT NULL
--        GROUP  BY #FT.ID,
--                  #FT.patientno,
--                  #FT.NHSno,
--                  #FT.Location,
--                  #FT.BloodUnit,
--                  #FT.Product,
--                  #FT.Unit,
--                  #FT.Date,
--                  #FT.Patient,
--                  sp.SourcePatientNo) min_sp
--       LEFT OUTER JOIN WHREPORTING.APC.spell
--                    ON min_sp.SourcePatientNo = Spell.SourcePatientNo
--                       AND min_sp.min_admdate = Spell.AdmissionDateTime
--       LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping WdMap --maps back to the ward mapping table here so that the final location which comes through is already mapped
--                    ON Spell.AdmissiontWardCode = Wdmap.PASWardCode
--WHERE  min_sp.SourcePatientNo IS NOT NULL 



--------------------------------
----Fated Data Final Query
--------------------------------
----The records in the data which we want to find a location for are thos which are coming through as 'Blood Bank', 'Blood Bank FR', 'ERC 2nd Floor', 'Default Desktop Location'  or where the location has a 'Fridge' in the description.
----All other locations are deemed to have been correctly fated and therefore the location which has been recorded in the AutoFate Blood Track data is the one which is used. 


----Drop temp table if already exists 
--IF Object_id('tempdb..#FinalFT') > 0
--  DROP TABLE #FinalFT

--CREATE TABLE #FinalFT
--  (
--     YearMonth         VARCHAR(50),
--     ID					INT,
--     NHSno             VARCHAR(20),
--     PatientNo			VARCHAR(20),
--     OriginalLocation   VARCHAR(50),
--     Location			 VARCHAR(50),--this is the mapped location
--     BloodUnit         VARCHAR(50),
--     Product           VARCHAR(100),
--     Unit              VARCHAR(50),
--     Date              DATETIME,
--     Patient           VARCHAR(50),
--     SourcePatientNo   VARCHAR(50),
--     AdmissionDate DATETIME,
--     DischargeDate DATETIME,
--     WardStartTime DATETIME,
--     WardEndTime DATETIME,
--     FinalLocation		VARCHAR(50),
--     MappedWard			VARCHAR(50),
--	 Fated				VARCHAR(50)
--	  )
--INSERT INTO #FinalFT
--SELECT DISTINCT Datename(yy, MaxWS.Date) + ' '
--                        + Datename(mm, MaxWS.Date)                                                          AS YearMonth,
--                        MaxWS.ID,
--                        MaxWS.NHSno,
--                        MaxWS.patientno,
--                        MaxWS.OriginalLocation                                                              AS OriginalLocationDetails,--this is the original location which comes through in the Blood Track Report
--                        MaxWS.Location                                                                      AS OriginalMappedLocationDetails,--this is the location which the original location which comes through in the Blood Track Report has been mapped to
--                        MaxWS.BloodUnit,
--                        MaxWS.Product,
--                        MaxWS.Unit,
--                        MaxWS.Date,
--                        MaxWS.Patient,
--                        MaxWS.SourceSpellNo,
--                        COALESCE(MaxWS.AdmissionDate, #ws.AdmissionDatetime, #FT_next_sp.AdmissionDateTime) AS AdmissionDate,
--                        COALESCE(MaxWS.DischargeDate, #ws.dischargeDatetime, #FT_next_sp.DischargeDateTime) AS DischargeDate,
--                        COALESCE(MaxWS.WardStartTime, #ws.startTime)                                        AS WardStartTime,
--                        COALESCE(MaxWS.WardEndTime, #ws.EndTime)                                            AS WardEndTime,
--                        CASE
--                          WHEN MaxWS.OriginalLocation IN ( 'Blood Bank', 'Blood Bank FR', 'ERC 2nd Floor', 'Default Desktop Location' ) THEN COALESCE (MaxWS.Ward, #ws.WardName, #FT_next_sp.AdmissionWardName) 
--                          WHEN MaxWS.OriginalLocation LIKE '%Fridge%' THEN COALESCE (MaxWS.Ward, #ws.WardName, #FT_next_sp.AdmissionWardName)
--                          ELSE MaxWS.Location
--                        END                                                                                 AS FinalLocation,--this is the final location identiofied for the transfusion based on taking the location from Auto Track where the location has been identified (i.e. not blood bank or ERC), then it looks to get the mapped to ward, then looks at getting the ward of the last spell prior to the transfusion date where it is not DL, then looks to get the admission ward in the next spell after the transfusion date
--                        MaxWS.Ward                                                                          AS MappedWard,--this is the ward from the wardstays data whichn the transfusion has been mapped to based on the transfusiondate being within ward start and end
--                        CASE
--                          WHEN MaxWS.OriginalLocation NOT IN ( 'Blood Bank', 'Blood Bank FR', 'ERC 2nd Floor', 'Default Desktop Location' ) THEN 'Fated Correctly'
--                          ELSE
--                            CASE
--                              WHEN MaxWS.OriginalLocation IN ( 'Blood Bank', 'Blood Bank FR' ) THEN 'Blood Bank'
--                              ELSE MaxWS.OriginalLocation
--                            END
--                        END                                                                                 AS Fated
--        FROM   (SELECT DISTINCT #ft.*,
--                                sp.SourceSpellNo,
--                                sp.AdmissionDate,
--                                sp.DischargeDate,
--                                wd.StartTime     AS WardStartTime,
--                                wd.EndTime       AS WardEndTime,
--                                mp.Wardname      AS Ward,
--                                Max(#ws.endtime) MaxWdEnd
--                FROM   #FT
--                       LEFT OUTER JOIN WHREPORTING.APC.spell sp
--                                    ON Isnull(#FT.patientno, 'x') = sp.faciltyID
--                                       AND #FT.Date >= sp.AdmissionDateTime
--                                       AND #FT.date <= Isnull(sp.DischargeDateTime, Getdate())
--                       LEFT OUTER JOIN WHReporting.APC.WardStay wd
--                                    ON sp.SourceSpellNo = wd.SourceSpellNo
--                                       AND #FT.Date > wd.StartTime
--                                       AND #FT.date <= Isnull(wd.EndTime, Getdate())
--                       LEFT OUTER JOIN #ws
--                                    ON #FT.patientno = #ws.FaciltyID
--                                       AND #ws.DischargeDateTime < #FT.Date
--                       LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping mp
--                                    ON wd.WardCode = mp.paswardcode
--                WHERE  #FT.NHSno IS NULL
--                GROUP  BY ID,
--                          patientno,
--                          #FT.NHSno,
--                          Location,
--                          LocationCode,
--                          BloodUnit,
--                          Product,
--                          Unit,
--                          Date,
--                          Patient,
--                          #FT.OriginalLocation,
--                          #FT.Division,
--                          sp.SourceSpellNo,
--                          AdmissionDate,
--                          DischargeDate,
--                          wd.StartTime,
--                          wd.EndTime,
--                          mp.Wardname) MaxWS
--               LEFT OUTER JOIN #ws
--                            ON maxWS.patientno = #ws.FaciltyID
--                               AND MaxWS.MaxWdEnd = #ws.EndTime
--               LEFT OUTER JOIN #FT_next_sp
--                            ON maxWs.patientno = #FT_next_sp.patientno
--                               AND maxWs.Date = #FT_next_sp.date
--        UNION ALL
--        SELECT DISTINCT Datename(yy, MaxWS.Date) + ' '
--                        + Datename(mm, MaxWS.Date)                                                          AS YearMonth,
--                        MaxWS.ID,
--                        MaxWS.NHSno,
--                        MaxWS.patientno,
--                        MaxWS.OriginalLocation                                                              AS OriginalLocationDetails,--this is the original location which comes through in the Blood Track Report
--                        MaxWS.Location                                                                      AS OriginalMappedLocationDetails,--this is the location which the original location which comes through in the Blood Track Report has been mapped to
--                        MaxWS.BloodUnit,
--                        MaxWS.Product,
--                        MaxWS.Unit,
--                        MaxWS.Date,
--                        MaxWS.Patient,
--                        MaxWS.SourceSpellNo,
--                        COALESCE(MaxWS.AdmissionDate, #ws.AdmissionDatetime, #FT_next_sp.AdmissionDateTime) AS AdmissionDate,
--                        COALESCE(MaxWS.DischargeDate, #ws.dischargeDatetime, #FT_next_sp.DischargeDateTime) AS DischargeDate,
--                        COALESCE(MaxWS.WardStartTime, #ws.startTime)                                        AS WardStartTime,
--                        COALESCE(MaxWS.WardEndTime, #ws.EndTime)                                            AS WardEndTime,
--                        CASE
--                          WHEN MaxWS.OriginalLocation IN ( 'Blood Bank', 'Blood Bank FR', 'ERC 2nd Floor', 'Default Desktop Location' ) THEN COALESCE (MaxWS.Ward, #ws.WardName, #FT_next_sp.AdmissionWardName)
--                          WHEN MaxWS.OriginalLocation LIKE '%Fridge%' THEN COALESCE (MaxWS.Ward, #ws.WardName, #FT_next_sp.AdmissionWardName)
--                          ELSE MaxWS.Location
--                        END                                                                                 AS FinalLocation,--this is the final location identiofied for the transfusion based on taking the location from Auto Track where the location has been identified (i.e. not blood bank or ERC), then it looks to get the mapped to ward, then looks at getting the ward of the last spell prior to the transfusion date where it is not DL, then looks to get the admission ward in the next spell after the transfusion date
--                        MaxWS.Ward                                                                          AS MappedWard,--this is the ward from the wardstays data whichn the transfusion has been mapped to based on the transfusiondate being within ward start and end
--                        CASE
--                          WHEN MaxWS.OriginalLocation NOT IN ( 'Blood Bank', 'Blood Bank FR', 'ERC 2nd Floor', 'Default Desktop Location' ) THEN 'Fated Correctly'
--                          ELSE
--                            CASE
--                              WHEN MaxWS.OriginalLocation IN ( 'Blood Bank', 'Blood Bank FR' ) THEN 'Blood Bank'
--                              ELSE MaxWS.Location
--                            END
--                        END                                                                                 AS Fated
--        FROM   (SELECT DISTINCT #ft.*,
--                                sp.SourceSpellNo,
--                                sp.AdmissionDate,
--                                sp.DischargeDate,
--                                wd.StartTime     AS WardStartTime,
--                                wd.EndTime       AS WardEndTime,
--                                mp.wardname      AS Ward,
--                                Max(#ws.endtime) MaxWdEnd
--                FROM   #FT
--                       LEFT OUTER JOIN WHREPORTING.APC.spell sp
--                                    ON #FT.NHSno = sp.NHSNo
--                                       AND #FT.Date >= sp.AdmissionDateTime
--                                       AND #FT.date <= Isnull(sp.DischargeDateTime, Getdate())
--                       LEFT OUTER JOIN WHReporting.APC.WardStay wd
--                                    ON sp.SourceSpellNo = wd.SourceSpellNo
--                                       AND #FT.Date > wd.StartTime
--                                       AND #FT.date <= Isnull(wd.EndTime, Getdate())
--                       LEFT OUTER JOIN #ws
--                                    ON #FT.NHSno = #ws.NHSNo
--                                       AND #ws.DischargeDateTime < #FT.Date
--                       LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping mp
--                                    ON wd.WardCode = mp.paswardcode
--                WHERE  #FT.NHSno IS NOT NULL
--                GROUP  BY ID,
--                          patientno,
--                          #FT.NHSno,
--                          Location,
--                          LocationCode,
--                          BloodUnit,
--                          Product,
--                          Unit,
--                          Date,
--                          Patient,
--                          #FT.OriginalLocation,
--                          #FT.Division,
--                          sp.SourceSpellNo,
--                          AdmissionDate,
--                          DischargeDate,
--                          wd.StartTime,
--                          wd.EndTime,
--                          mp.Wardname) MaxWS
--               LEFT OUTER JOIN #ws
--                            ON maxWS.NHSno = #ws.NHSNo
--                               AND MaxWS.MaxWdEnd = #ws.EndTime
--               LEFT OUTER JOIN #FT_next_sp
--                            ON maxWS.NHSno = #FT_next_sp.NHSno
--                               AND maxWs.Date = #FT_next_sp.date
                               
--SELECT DISTINCT Isnull(mp.Division, 'Unknown')                          AS Division,
--                Isnull(#FinalFT.FinalLocation, 'Unknown')               AS FinalLocationIdentifiedAsTransfusionLocation,
--                #FinalFT.YearMonth,
--                Cast('01/'
--                     + CONVERT(VARCHAR, Datename(month, #FinalFT.Date))
--                     + '/' + Datename(year, #FinalFT.Date) AS DATETIME) AS FirstDayOfMonth,
--                #FinalFT.ID,
--                #FinalFT.PatientNo,
--                #FinalFT.OriginalLocation                               AS LocationRecordedOnBloodTrack,
--                #FinalFT.BloodUnit,
--                #FinalFT.Product,
--                #FinalFT.Unit,
--                #FinalFT.Date                                           AS TransfusionDate,
--                #FinalFT.Patient,
--                #FinalFT.FinalLocation                                  AS FinalLocationUnmapped,
--                #FinalFT.Fated                                          AS FatedStatus
--FROM   #FinalFT
--       --join back to the mapping table to get the division of the final location that has been identified after the processing above 
--       LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping mp
--                    ON #FinalFT.FinalLocation = mp.wardname 
--where Isnull(#FinalFT.FinalLocation, 'Unknown') = @Loc
--and  #FinalFT.Fated    = @fated
--and  #FinalFT.YearMonth = @yearmonth





SELECT Division
	,FinalLocation  AS FinalLocationIdentifiedAsTransfusionLocation
	,ID
	,PatientNo
	,OriginalLocation  AS LocationRecordedOnBloodTrack
	,BloodUnit
	,Product
	,DATE  AS TransfusionDate
	,Patient
	,FinalLocation
	,YearMonth
	,FirstDayOfMonth
	,CASE 
		WHEN OriginalLocation NOT IN (
				'Blood Bank'
				,'Blood Bank FR'
				,'ERC 2nd Floor'
				,'Default Desktop Location'
				)
			THEN 'Fated Correctly'
		ELSE CASE 
				WHEN OriginalLocation IN (
						'Blood Bank'
						,'Blood Bank FR'
						)
					THEN 'Blood Bank'
				WHEN OriginalLocation = 'ERC 2nd Floor'
					THEN 'ERC 2nd Floor'
				WHEN OriginalLocation = 'Default Desktop Location'
					THEN 'Default Desktop Location'
				ELSE OriginalLocation
				END
		END AS Fated
FROM WHREPORTING.BLOODTRACK.ReportTransfusionsFatedUnfatedCombined
WHERE (UnfatedTransfusionsFirst <> 1
	OR UnfatedTransfusionsRefresh <> 1)
	AND FirstImport = 1
	AND Isnull(FinalLocation, 'Unknown') = @Loc
	AND  CASE 
		WHEN OriginalLocation NOT IN (
				'Blood Bank'
				,'Blood Bank FR'
				,'ERC 2nd Floor'
				,'Default Desktop Location'
				)
			THEN 'Fated Correctly'
		ELSE CASE 
				WHEN OriginalLocation IN (
						'Blood Bank'
						,'Blood Bank FR'
						)
					THEN 'Blood Bank'
				WHEN OriginalLocation = 'ERC 2nd Floor'
					THEN 'ERC 2nd Floor'
				WHEN OriginalLocation = 'Default Desktop Location'
					THEN 'Default Desktop Location'
				ELSE OriginalLocation
				END
		END    = @fated
	AND  YearMonth = @yearmonth

--GROUP BY Division
--	,FinalLocation
--	,YearMonth
--	,FirstDayOfMonth
--	,CASE 
--		WHEN OriginalLocation NOT IN (
--				'Blood Bank'
--				,'Blood Bank FR'
--				,'ERC 2nd Floor'
--				,'Default Desktop Location'
--				)
--			THEN 'Fated Correctly'
--		ELSE CASE 
--				WHEN OriginalLocation IN (
--						'Blood Bank'
--						,'Blood Bank FR'
--						)
--					THEN 'Blood Bank'
--				WHEN OriginalLocation = 'ERC 2nd Floor'
--					THEN 'ERC 2nd Floor'
--				WHEN OriginalLocation = 'Default Desktop Location'
--					THEN 'Default Desktop Location'
--				ELSE OriginalLocation
--				END
--		END






END