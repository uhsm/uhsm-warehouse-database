﻿-- =============================================
-- Author:		CMan 
-- Create date: 08/01/2014
-- Description:	Reporting on AutoFate data.  Stored Procedure for SSRS report 'Summary report - Fated  and Unfated Transfusions'.
--				Requested by EMilser via email on 07/01/2014. 
--				Linked to RD438
-- =============================================
CREATE PROCEDURE [BLOODTRACK].[ReportSummaryFated_UnfatedTransfusions]
@monthstart varchar(max)
	-- Add the parameters for the stored procedure here
AS
BEGIN



--Declare @monthstart datetime = '20151001'


SELECT	YearMonth, 
		FirstDayOfMonth,
		Division,
		WardName,
		WardCode,
		SUM(UnfatedTransfusionsFirst) as UnfatedTransfusions,
		SUM(FatedCorrectlyFirst) as FatedCorrectly,
		SUM(FatedinNonClinicalAreaFirst) as FatedinNonClinicalArea,
		SUM(FatedCorrectlyFirst) + SUM(FatedinNonClinicalAreaFirst) as FatedTransfusions,
		SUM(FatedCorrectlyFirst) + SUM(FatedinNonClinicalAreaFirst) + SUM(UnfatedTransfusionsFirst) as Total,
		SUM(UnfatedTransfusionsRefresh) as UnfatedTransfusionsRefresh,
		SUM(FatedCorrectlyRefresh) as FatedCorrectlyRefresh,
		SUM(FatedinNonClinicalAreaRefresh) as FatedinNonClinicalAreaRefresh,
		SUM(FatedCorrectlyRefresh) + SUM(FatedinNonClinicalAreaRefresh) as FatedTransfusionsRefresh,
		SUM(FatedCorrectlyRefresh) + SUM(FatedinNonClinicalAreaRefresh)  +SUM(UnfatedTransfusionsRefresh) as TotalRefresh
		
FROM WHREPORTING.BLOODTRACK.ReportTransfusionsFatedUnfatedCombined
where FirstDayOfMonth in (SELECT Item
                      FROM   dbo.Split (@monthstart, ','))
Group By YearMonth,
		FirstDayOfMonth,
		Division,
		WardName,
		WardCode




----The below is no longer needed as the logic is now stored in a stored proc which builds an actual table to report from 
-- --------------------------
----Fated List Temp table 
----------------------------
----Drop temp table if already exists 
--IF Object_id('tempdb..#FT') > 0
--  DROP TABLE #FT

----create temp table from the data provided  with an RM2 number column split out from the patient's name and a unique id to use
--CREATE TABLE #FT
--  (
--     ID               INT IDENTITY (1, 1),
--     patientno        VARCHAR(20),
--     NHSno            VARCHAR(10),
--     OriginalLocation VARCHAR(50),--this is the location which comes from the Blood Track Report
--     Location         VARCHAR(50),--this is the mapped location from the  WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping table
--     LocationCode     VARCHAR(50),--this is the mapped location code from the  WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping table
--     Division         VARCHAR(50),
--     BloodUnit        VARCHAR(50),
--     Product          VARCHAR(100),
--     Unit             VARCHAR(50),
--     Date             DATETIME,
--     Patient          VARCHAR(100)
--  )

--INSERT INTO #FT
--SELECT CASE
--         WHEN Len(CONVERT(VARCHAR(20), CONVERT(BIGINT, CASE
--                                                         WHEN Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '') LIKE '%[a-z]%' THEN NULL
--                                                         ELSE Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '')
--                                                       END))) = 10 THEN NULL
--         ELSE 'RM2' + CONVERT(VARCHAR(20), CONVERT(BIGINT, CASE WHEN Replace(Substring(patient, Charindex('(', patient)+1, Len(patient)), ')', '') LIKE '%[a-z]%' THEN NULL ELSE Replace(Substring(patient, Charindex('(', patient)+1, Len(patient)), ')', '') END))
--       END AS patientno,
--       CASE
--         WHEN Len(CONVERT(VARCHAR(20), CONVERT(BIGINT, CASE
--                                                         WHEN Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '') LIKE '%[a-z]%' THEN NULL
--                                                         ELSE Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '')
--                                                       END))) = 10 THEN CONVERT(VARCHAR(20), CONVERT(BIGINT, CASE
--                                                                                                               WHEN Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '') LIKE '%[a-z]%' THEN NULL
--                                                                                                               ELSE Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '')
--                                                                                                             END))
--         ELSE NULL
--       END AS NHSNo,
--       a.Location,
--       b.wardname,--this is the mapped wardname from the PAS to Blood Track Location mapping table
--       b.wardcode,--this is the mapped wardcode from the PAS to Blood Track Location mapping table
--       b.division,--this is the division from the PAS to Blood Track Location mapping table - mappings approved by Emma Milser
--       a.[Blood Unit],
--       a.Product,
--       a.[Unit Fate],
--       a.Date,
--       a.Patient
--FROM   WHREPORTING.dbo.TransfusionFatedUnits a
--       LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping b
--                    ON Isnull(a.Location, 'x') = b.MappedAutoTrackWard 
                    
                    
                    
-- --------------------------
----UnFated List Temp table 
----------------------------
----Drop temp table if already exists 
--IF Object_id('tempdb..#UF') > 0
--  DROP TABLE #UF

----create temp table from original data with an RM2 number split out from patients name column and a unique id to use
--CREATE TABLE #UF
--  (
--     ID                   INT IDENTITY (1, 1),
--     patientno            VARCHAR(20),
--     NHSNo                VARCHAR(10),
--     BloodUnit            VARCHAR(50),
--     Product              VARCHAR(100),
--     --ProductCode varchar(10),
--     OriginalLastLocation VARCHAR(50),--this is the location which comes from the Blood Track Report
--     LastLocation         VARCHAR(50),--this is the mapped location from the  WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping table
--     LastLocationCode     VARCHAR(50),--this is the mapped location code from the  WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping table
--     Division             VARCHAR(50),
--     LastActivity         DATETIME,
--     LastTransaction      VARCHAR(50),
--     Patient              VARCHAR(100)
--  )

--INSERT INTO #UF
--SELECT CASE
--         WHEN Len(CONVERT(VARCHAR(20), CONVERT(BIGINT, CASE
--                                                         WHEN Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '') LIKE '%[a-z]%' THEN NULL
--                                                         ELSE Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '')
--                                                       END))) = 10 THEN NULL
--         ELSE 'RM2' + CONVERT(VARCHAR(20), CONVERT(BIGINT, CASE WHEN Replace(Substring(patient, Charindex('(', patient)+1, Len(patient)), ')', '') LIKE '%[a-z]%' THEN NULL ELSE Replace(Substring(patient, Charindex('(', patient)+1, Len(patient)), ')', '') END))
--       END AS patientno,
--       CASE
--         WHEN Len(CONVERT(VARCHAR(20), CONVERT(BIGINT, CASE
--                                                         WHEN Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '') LIKE '%[a-z]%' THEN NULL
--                                                         ELSE Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '')
--                                                       END))) = 10 THEN CONVERT(VARCHAR(20), CONVERT(BIGINT, CASE
--                                                                                                               WHEN Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '') LIKE '%[a-z]%' THEN NULL
--                                                                                                               ELSE Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '')
--                                                                                                             END))
--         ELSE NULL
--       END AS NHSNo,
--       [Blood Unit],
--       [Product],
--       [Last Location],
--       b.wardname,--this is the mapped wardname from the PAS to Blood Track Location mapping table
--       b.wardcode,--this is the mapped wardcode from the PAS to Blood Track Location mapping table
--       b.division,--this is the division from the PAS to Blood Track Location mapping table - mappings approved by Emma Milser
--       [Last Activity],
--       [Last Transaction],
--       [Patient]
--FROM   WHREPORTING.dbo.TransfusionUnfatedUnits a
--       LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping b
--                    ON Isnull(a.[Last Location], 'x') = b.MappedAutoTrackWard 



------------------------------------
----Temp table for the last ward per spell where the ward is not Discharge Lounge
------------------------------------
----To be used in the final query to identify the last ward for the patient's transfusion where no ward can be identified over the period of the transfusion date

----Drop temp table if already exists 
--IF Object_id('tempdb..#ws') > 0
--  DROP TABLE #ws

--CREATE TABLE #ws
--  (
--     SourcePatientNo   VARCHAR(20),
--     NHSNo             VARCHAR(20),
--     SourceSpellNo     VARCHAR(20),
--     FaciltyID         VARCHAR(20),
--     StartTime         DATETIME,
--     EndTime           DATETIME,
--     AdmissionDateTime DATETIME,
--     DischargeDateTime DATETIME,
--     WardCode          VARCHAR(20),
--     WardName          VARCHAR(50)
--  )

--INSERT INTO #ws
--SELECT ws.SourcePatientNo,
--       sp.NHSNo,
--       ws.SourceSpellNo,
--       sp.FaciltyID,
--       ws.StartTime,
--       ws.EndTime,
--       sp.AdmissionDateTime,
--       sp.DischargeDateTime,
--       ws.WardCode,
--       mp.WardName
--FROM   (SELECT SourceSpellNo,
--               Max(EndTime) MaxEndTime
--        FROM   WHREPORTING.APC.WardStay
--        WHERE  Isnull(WardCode, 'x') <> 'DL'
--        GROUP  BY SourceSpellNo) a
--       LEFT OUTER JOIN WHREPORTING.APC.WardStay ws
--                    ON a.SourceSpellNo = ws.SourceSpellNo
--                       AND a.MaxEndTime = ws.EndTime
--       LEFT OUTER JOIN WHREPORTING.APC.Spell sp
--                    ON ws.SourceSpellNo = sp.SourceSpellNo
--       LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping mp
--                    ON ws.WardCode = mp.PasWardCode



-------------------------------
----Fated data - next ward after transfusion date
------------------------------
----Temp table identifying the admission ward for the next spell admission after the transfusion date
----to be used to identify a ward for those transfusion records where no location can be identified even after trying to match to a wardstay which covers the transfusion date
----and looking back to last spell ward
----Only looks at those records in the fated data which have an admission date which is after the tranfusion date to grab the date, time and ward of the next spell
----then takes the admission ward details of the first admission which is after the transfusion date

----Drop temp table if already exists 
--IF Object_id('tempdb..#FT_next_sp') > 0
--  DROP TABLE #FT_next_sp

--CREATE TABLE #FT_next_sp
--  (
--     ID                INT,
--     patientno         VARCHAR(20),
--     NHSno             VARCHAR(20),
--     Location          VARCHAR(50),
--     BloodUnit         VARCHAR(50),
--     Product           VARCHAR(100),
--     Unit              VARCHAR(50),
--     Date              DATETIME,
--     Patient           VARCHAR(50),
--     SourcePatientNo   VARCHAR(20),
--     AdmissionDateTime DATETIME,
--     DischargeDateTime DATETIME,
--     AdmissionWardCode VARCHAR(20),
--     AdmissionWardName VARCHAR(50)
--  )

--INSERT INTO #FT_next_sp
--SELECT min_sp.ID,
--       min_sp.patientno,
--       min_sp.NHSno,
--       min_sp.Location,
--       min_sp.BloodUnit,
--       min_sp.Product,
--       min_sp.Unit,
--       min_sp.Date,
--       min_sp.Patient,
--       min_sp.SourcePatientNo,
--       Spell.AdmissionDateTime,
--       Spell.DischargeDateTime,
--       Spell.AdmissiontWardCode,
--       Wdmap.wardname AS AdmissionWardName
--FROM   (SELECT #FT.ID,
--               #FT.patientno,
--               #FT.NHSno,
--               #FT.Location,
--               #FT.BloodUnit,
--               #FT.Product,
--               #FT.Unit,
--               #FT.Date,
--               #FT.Patient,
--               sp.SourcePatientNo,
--               Min(AdmissionDateTime) min_admdate
--        FROM   #FT
--               LEFT OUTER JOIN WHREPORTING.APC.spell sp
--                            ON Isnull(#FT.patientno, 'x') = sp.faciltyID
--                               AND sp.AdmissionDateTime > #FT.Date
--        WHERE  #FT.NHSno IS NULL
--        GROUP  BY #FT.ID,
--                  #FT.patientno,
--                  #FT.NHSno,
--                  #FT.Location,
--                  #FT.BloodUnit,
--                  #FT.Product,
--                  #FT.Unit,
--                  #FT.Date,
--                  #FT.Patient,
--                  sp.SourcePatientNo
--        UNION ALL
--        SELECT #FT.ID,
--               #FT.patientno,
--               #FT.NHSno,
--               #FT.Location,
--               #FT.BloodUnit,
--               #FT.Product,
--               #FT.Unit,
--               #FT.Date,
--               #FT.Patient,
--               sp.SourcePatientNo,
--               Min(AdmissionDateTime) min_admdate
--        FROM   #FT
--               LEFT OUTER JOIN WHREPORTING.APC.spell sp
--                            ON #FT.NHSno = sp.NHSNo
--                               AND sp.AdmissionDateTime > #FT.Date
--        WHERE  #FT.NHSno IS NOT NULL
--        GROUP  BY #FT.ID,
--                  #FT.patientno,
--                  #FT.NHSno,
--                  #FT.Location,
--                  #FT.BloodUnit,
--                  #FT.Product,
--                  #FT.Unit,
--                  #FT.Date,
--                  #FT.Patient,
--                  sp.SourcePatientNo) min_sp
--       LEFT OUTER JOIN WHREPORTING.APC.spell
--                    ON min_sp.SourcePatientNo = Spell.SourcePatientNo
--                       AND min_sp.min_admdate = Spell.AdmissionDateTime
--       LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping WdMap --maps back to the ward mapping table here so that the final location which comes through is already mapped
--                    ON Spell.AdmissiontWardCode = Wdmap.PASWardCode
--WHERE  min_sp.SourcePatientNo IS NOT NULL 





-------------------------------
----UnFated data - next ward after transfusion date
------------------------------
----Temp table identifying the admission ward for the next spell admission after the transfusion date for unfated data
----to be used to identify a ward for those transfusion records where no location can be identified even after trying to match to a wardstay which covers the transfusion date
----and looking back to last spell ward
----Only looks at those records in the unfated data which have an admission date which is after the tranfusion date to grab the date, time and ward of the next spell
----then takes the admission ward details of the first admission which is after the transfusion date


----Drop temp table if already exists 
--IF Object_id('tempdb..#UF_next_sp') > 0
--  DROP TABLE #UF_next_sp

--CREATE TABLE #UF_next_sp
--  (
--     ID                 INT,
--     patientno          VARCHAR(20),
--     NHSno              VARCHAR(20),
--     LastLocation       VARCHAR(50),
--     BloodUnit          VARCHAR(50),
--     Product            VARCHAR(100),
--     LastActivity       DATETIME,
--     LastTransaction    VARCHAR(50),
--     Patient            VARCHAR(50),
--     SourcePatientNo    VARCHAR(20),
--     AdmissionDateTime  DATETIME,
--     DischargeDateTime  DATETIME,
--     AdmissiontWardCode VARCHAR(20),
--     AdmissionWardName  VARCHAR(50)
--  )

--INSERT INTO #UF_next_sp
--SELECT min_sp.ID,
--       min_sp.patientno,
--       min_sp.NHSno,
--       min_sp.LastLocation,
--       min_sp.BloodUnit,
--       min_sp.Product,
--       min_sp.LastActivity,
--       min_sp.LastTransaction,
--       min_sp.Patient,
--       min_sp.SourcePatientNo,
--       Spell.AdmissionDateTime,
--       Spell.DischargeDateTime,
--       Spell.AdmissiontWardCode,
--       wdmap.WardName AS AdmissionWardName
--FROM   (SELECT #UF.ID,
--               #UF.patientno,
--               #UF.NHSno,
--               #UF.LastLocation,
--               #UF.BloodUnit,
--               #UF.Product,
--               #UF.LastActivity,
--               #UF.LastTransaction,
--               #UF.Patient,
--               sp.SourcePatientNo,
--               Min(AdmissionDateTime) min_admdate
--        FROM   #UF
--               LEFT OUTER JOIN WHREPORTING.APC.spell sp
--                            ON Isnull(#UF.patientno, 'x') = sp.faciltyID
--                               AND sp.AdmissionDateTime > #UF.LastActivity
--        WHERE  #UF.NHSno IS NULL
--        GROUP  BY #UF.ID,
--                  #UF.patientno,
--                  #UF.NHSno,
--                  #UF.LastLocation,
--                  #UF.BloodUnit,
--                  #UF.Product,
--                  #UF.LastActivity,
--                  #UF.LastTransaction,
--                  #UF.Patient,
--                  sp.SourcePatientNo
--        UNION ALL
--        SELECT #UF.ID,
--               #UF.patientno,
--               #UF.NHSno,
--               #UF.LastLocation,
--               #UF.BloodUnit,
--               #UF.Product,
--               #UF.LastActivity,
--               #UF.LastTransaction,
--               #UF.Patient,
--               sp.SourcePatientNo,
--               Min(AdmissionDateTime) min_admdate
--        FROM   #UF
--               LEFT OUTER JOIN WHREPORTING.APC.spell sp
--                            ON #UF.NHSno = sp.NHSNo
--                               AND sp.AdmissionDateTime > #UF.LastActivity
--        WHERE  #UF.NHSno IS NOT NULL
--        GROUP  BY #UF.ID,
--                  #UF.patientno,
--                  #UF.NHSno,
--                  #UF.LastLocation,
--                  #UF.BloodUnit,
--                  #UF.Product,
--                  #UF.LastActivity,
--                  #UF.LastTransaction,
--                  #UF.Patient,
--                  sp.SourcePatientNo) min_sp
--       LEFT OUTER JOIN WHREPORTING.APC.spell
--                    ON min_sp.SourcePatientNo = Spell.SourcePatientNo
--                       AND min_sp.min_admdate = Spell.AdmissionDateTime
--       LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping WdMap
--                    ON Spell.AdmissiontWardCode = Wdmap.PASWardCode
--WHERE  min_sp.SourcePatientNo IS NOT NULL 






--------------------------------
----Fated Data Final Query
--------------------------------
----The records in the data which we want to find a location for are thos which are coming through as 'Blood Bank', 'Blood Bank FR', 'ERC 2nd Floor', 'Default Desktop Location'  or where the location has a 'Fridge' in the description.
----All other locations are deemed to have been correctly fated and therefore the location which has been recorded in the AutoFate Blood Track data is the one which is used. 


----Drop temp table if already exists 
--IF Object_id('tempdb..#FinalFT') > 0
--  DROP TABLE #FinalFT

--CREATE TABLE #FinalFT
--  (
--     YearMonth         VARCHAR(50),
--     ID					INT,
--     NHSno             VARCHAR(20),
--     PatientNo			VARCHAR(20),
--     OriginalLocation   VARCHAR(50),
--     Location			 VARCHAR(50),--this is the mapped location
--     BloodUnit         VARCHAR(50),
--     Product           VARCHAR(100),
--     Unit              VARCHAR(50),
--     Date              DATETIME,
--     Patient           VARCHAR(50),
--     SourcePatientNo   VARCHAR(50),
--     AdmissionDate DATETIME,
--     DischargeDate DATETIME,
--     WardStartTime DATETIME,
--     WardEndTime DATETIME,
--     FinalLocation		VARCHAR(50),
--     MappedWard			VARCHAR(50),
--	 Fated				VARCHAR(50)
--	  )
--INSERT INTO #FinalFT
--SELECT DISTINCT Datename(yy, MaxWS.Date) + ' '
--                        + Datename(mm, MaxWS.Date)                                                          AS YearMonth,
--                        MaxWS.ID,
--                        MaxWS.NHSno,
--                        MaxWS.patientno,
--                        MaxWS.OriginalLocation                                                              AS OriginalLocationDetails,--this is the original location which comes through in the Blood Track Report
--                        MaxWS.Location                                                                      AS OriginalMappedLocationDetails,--this is the location which the original location which comes through in the Blood Track Report has been mapped to
--                        MaxWS.BloodUnit,
--                        MaxWS.Product,
--                        MaxWS.Unit,
--                        MaxWS.Date,
--                        MaxWS.Patient,
--                        MaxWS.SourceSpellNo,
--                        COALESCE(MaxWS.AdmissionDate, #ws.AdmissionDatetime, #FT_next_sp.AdmissionDateTime) AS AdmissionDate,
--                        COALESCE(MaxWS.DischargeDate, #ws.dischargeDatetime, #FT_next_sp.DischargeDateTime) AS DischargeDate,
--                        COALESCE(MaxWS.WardStartTime, #ws.startTime)                                        AS WardStartTime,
--                        COALESCE(MaxWS.WardEndTime, #ws.EndTime)                                            AS WardEndTime,
--                        CASE
--                          WHEN MaxWS.OriginalLocation IN ( 'Blood Bank', 'Blood Bank FR', 'ERC 2nd Floor', 'Default Desktop Location' ) THEN COALESCE (MaxWS.Ward, #ws.WardName, #FT_next_sp.AdmissionWardName) 
--                          WHEN MaxWS.OriginalLocation LIKE '%Fridge%' THEN COALESCE (MaxWS.Ward, #ws.WardName, #FT_next_sp.AdmissionWardName)
--                          ELSE MaxWS.Location
--                        END                                                                                 AS FinalLocation,--this is the final location identiofied for the transfusion based on taking the location from Auto Track where the location has been identified (i.e. not blood bank or ERC), then it looks to get the mapped to ward, then looks at getting the ward of the last spell prior to the transfusion date where it is not DL, then looks to get the admission ward in the next spell after the transfusion date
--                        MaxWS.Ward                                                                          AS MappedWard,--this is the ward from the wardstays data whichn the transfusion has been mapped to based on the transfusiondate being within ward start and end
--                        CASE
--                          WHEN MaxWS.OriginalLocation NOT IN ( 'Blood Bank', 'Blood Bank FR', 'ERC 2nd Floor', 'Default Desktop Location' ) THEN 'Fated Correctly'
--                          ELSE
--                            CASE
--                              WHEN MaxWS.OriginalLocation IN ( 'Blood Bank', 'Blood Bank FR' ) THEN 'Blood Bank'
--                              ELSE MaxWS.OriginalLocation
--                            END
--                        END                                                                                 AS Fated
--        FROM   (SELECT DISTINCT #ft.*,
--                                sp.SourceSpellNo,
--                                sp.AdmissionDate,
--                                sp.DischargeDate,
--                                wd.StartTime     AS WardStartTime,
--                                wd.EndTime       AS WardEndTime,
--                                mp.Wardname      AS Ward,
--                                Max(#ws.endtime) MaxWdEnd
--                FROM   #FT
--                       LEFT OUTER JOIN WHREPORTING.APC.spell sp
--                                    ON Isnull(#FT.patientno, 'x') = sp.faciltyID
--                                       AND #FT.Date >= sp.AdmissionDateTime
--                                       AND #FT.date <= Isnull(sp.DischargeDateTime, Getdate())
--                       LEFT OUTER JOIN WHReporting.APC.WardStay wd
--                                    ON sp.SourceSpellNo = wd.SourceSpellNo
--                                       AND #FT.Date > wd.StartTime
--                                       AND #FT.date <= Isnull(wd.EndTime, Getdate())
--                       LEFT OUTER JOIN #ws
--                                    ON #FT.patientno = #ws.FaciltyID
--                                       AND #ws.DischargeDateTime < #FT.Date
--                       LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping mp
--                                    ON wd.WardCode = mp.paswardcode
--                WHERE  #FT.NHSno IS NULL
--                GROUP  BY ID,
--                          patientno,
--                          #FT.NHSno,
--                          Location,
--                          LocationCode,
--                          BloodUnit,
--                          Product,
--                          Unit,
--                          Date,
--                          Patient,
--                          #FT.OriginalLocation,
--                          #FT.Division,
--                          sp.SourceSpellNo,
--                          AdmissionDate,
--                          DischargeDate,
--                          wd.StartTime,
--                          wd.EndTime,
--                          mp.Wardname) MaxWS
--               LEFT OUTER JOIN #ws
--                            ON maxWS.patientno = #ws.FaciltyID
--                               AND MaxWS.MaxWdEnd = #ws.EndTime
--               LEFT OUTER JOIN #FT_next_sp
--                            ON maxWs.patientno = #FT_next_sp.patientno
--                               AND maxWs.Date = #FT_next_sp.date
--        UNION ALL
--        SELECT DISTINCT Datename(yy, MaxWS.Date) + ' '
--                        + Datename(mm, MaxWS.Date)                                                          AS YearMonth,
--                        MaxWS.ID,
--                        MaxWS.NHSno,
--                        MaxWS.patientno,
--                        MaxWS.OriginalLocation                                                              AS OriginalLocationDetails,--this is the original location which comes through in the Blood Track Report
--                        MaxWS.Location                                                                      AS OriginalMappedLocationDetails,--this is the location which the original location which comes through in the Blood Track Report has been mapped to
--                        MaxWS.BloodUnit,
--                        MaxWS.Product,
--                        MaxWS.Unit,
--                        MaxWS.Date,
--                        MaxWS.Patient,
--                        MaxWS.SourceSpellNo,
--                        COALESCE(MaxWS.AdmissionDate, #ws.AdmissionDatetime, #FT_next_sp.AdmissionDateTime) AS AdmissionDate,
--                        COALESCE(MaxWS.DischargeDate, #ws.dischargeDatetime, #FT_next_sp.DischargeDateTime) AS DischargeDate,
--                        COALESCE(MaxWS.WardStartTime, #ws.startTime)                                        AS WardStartTime,
--                        COALESCE(MaxWS.WardEndTime, #ws.EndTime)                                            AS WardEndTime,
--                        CASE
--                          WHEN MaxWS.OriginalLocation IN ( 'Blood Bank', 'Blood Bank FR', 'ERC 2nd Floor', 'Default Desktop Location' ) THEN COALESCE (MaxWS.Ward, #ws.WardName, #FT_next_sp.AdmissionWardName)
--                          WHEN MaxWS.OriginalLocation LIKE '%Fridge%' THEN COALESCE (MaxWS.Ward, #ws.WardName, #FT_next_sp.AdmissionWardName)
--                          ELSE MaxWS.Location
--                        END                                                                                 AS FinalLocation,--this is the final location identiofied for the transfusion based on taking the location from Auto Track where the location has been identified (i.e. not blood bank or ERC), then it looks to get the mapped to ward, then looks at getting the ward of the last spell prior to the transfusion date where it is not DL, then looks to get the admission ward in the next spell after the transfusion date
--                        MaxWS.Ward                                                                          AS MappedWard,--this is the ward from the wardstays data whichn the transfusion has been mapped to based on the transfusiondate being within ward start and end
--                        CASE
--                          WHEN MaxWS.OriginalLocation NOT IN ( 'Blood Bank', 'Blood Bank FR', 'ERC 2nd Floor', 'Default Desktop Location' ) THEN 'Fated Correctly'
--                          ELSE
--                            CASE
--                              WHEN MaxWS.OriginalLocation IN ( 'Blood Bank', 'Blood Bank FR' ) THEN 'Blood Bank'
--                              ELSE MaxWS.Location
--                            END
--                        END                                                                                 AS Fated
--        FROM   (SELECT DISTINCT #ft.*,
--                                sp.SourceSpellNo,
--                                sp.AdmissionDate,
--                                sp.DischargeDate,
--                                wd.StartTime     AS WardStartTime,
--                                wd.EndTime       AS WardEndTime,
--                                mp.wardname      AS Ward,
--                                Max(#ws.endtime) MaxWdEnd
--                FROM   #FT
--                       LEFT OUTER JOIN WHREPORTING.APC.spell sp
--                                    ON #FT.NHSno = sp.NHSNo
--                                       AND #FT.Date >= sp.AdmissionDateTime
--                                       AND #FT.date <= Isnull(sp.DischargeDateTime, Getdate())
--                       LEFT OUTER JOIN WHReporting.APC.WardStay wd
--                                    ON sp.SourceSpellNo = wd.SourceSpellNo
--                                       AND #FT.Date > wd.StartTime
--                                       AND #FT.date <= Isnull(wd.EndTime, Getdate())
--                       LEFT OUTER JOIN #ws
--                                    ON #FT.NHSno = #ws.NHSNo
--                                       AND #ws.DischargeDateTime < #FT.Date
--                       LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping mp
--                                    ON wd.WardCode = mp.paswardcode
--                WHERE  #FT.NHSno IS NOT NULL
--                GROUP  BY ID,
--                          patientno,
--                          #FT.NHSno,
--                          Location,
--                          LocationCode,
--                          BloodUnit,
--                          Product,
--                          Unit,
--                          Date,
--                          Patient,
--                          #FT.OriginalLocation,
--                          #FT.Division,
--                          sp.SourceSpellNo,
--                          AdmissionDate,
--                          DischargeDate,
--                          wd.StartTime,
--                          wd.EndTime,
--                          mp.Wardname) MaxWS
--               LEFT OUTER JOIN #ws
--                            ON maxWS.NHSno = #ws.NHSNo
--                               AND MaxWS.MaxWdEnd = #ws.EndTime
--               LEFT OUTER JOIN #FT_next_sp
--                            ON maxWS.NHSno = #FT_next_sp.NHSno
--                               AND maxWs.Date = #FT_next_sp.date
                               




--------------------------------
----UnFated Data Final Query
--------------------------------
----The records in the data which we want to find a location for are thos which are coming through as 'Blood Bank', 'Blood Bank FR', 'ERC 2nd Floor', 'Default Desktop Location'  or where the location has a 'Fridge' in the description.
----All other locations are deemed to have been correctly fated and therefore the location which has been recorded in the AutoFate Blood Track data is the one which is used. 


----Drop temp table if already exists 
--IF Object_id('tempdb..#FinalUFT') > 0
--  DROP TABLE #FinalUFT

--CREATE TABLE #FinalUFT
--  (
--     YearMonth         VARCHAR(50),
--     ID					INT,
--     NHSno             VARCHAR(20),
--     PatientNo			VARCHAR(20),
--     OriginalLocationDetails   VARCHAR(50),
--     OriginalMappedLocationDetails			 VARCHAR(50),--this is the original location mapped
--     BloodUnit         VARCHAR(50),
--     Product           VARCHAR(100),
--     LastActivity              DATETIME,
--     Patient           VARCHAR(50),
--     SourcePatientNo   VARCHAR(50),
--     AdmissionDate DATETIME,
--     DischargeDate DATETIME,
--     WardStartTime DATETIME,
--     WardEndTime DATETIME,
--     MappedWard			VARCHAR(50),
--      FinalLocation		VARCHAR(50)
--	  )
--INSERT INTO #FinalUFT

--------------------------
----Unfated Query
--------------------------
----The records in the data which we want to find a location for are those which are coming through as 'Blood Bank', 'Blood Bank FR', 'ERC 2nd Floor', 'Default Desktop Location'  or where the location has a 'Fridge' in the description.
----All other locations are deemed to have been correctly recorded and therefore the location which has been recorded in the AutoFate Blood Track data is the one which is used. 
--SELECT Datename(yy, MaxWS.LastActivity) + ' '
--               + Datename(mm, MaxWS.LastActivity)                                                  AS YearMonth,
--               MaxWS.ID,
--               MaxWS.NHSno,
--               MaxWS.patientno,
--               MaxWS.OriginalLastLocation                                                          AS OriginalLocationDetails,
--               MaxWS.LastLocation                                                                  AS OriginalMappedLocationDetails,
--               MaxWS.BloodUnit,
--               MaxWS.Product,
--               MaxWS.LastActivity,
--               MaxWS.Patient,
--               MaxWS.SourceSpellNo,
--               COALESCE(MaxWS.AdmissionDate, #ws.AdmissionDatetime, #UF_next_sp.AdmissionDateTime) AS AdmissionDate,
--               COALESCE(MaxWS.DischargeDate, #ws.dischargeDatetime, #UF_next_sp.DischargeDateTime) AS DischargeDate,
--               COALESCE(MaxWS.WardStartTime, #ws.startTime)                                        AS WardStartTime,
--               COALESCE(MaxWS.WardEndTime, #ws.EndTime)                                            AS WardEndTime,
--               --       MaxWS.LastLocation             AS OriginalLocationDetailsfromAutoTrackReport,
--               MaxWS.Ward                                                                          AS MappedWard,--this is the ward where the transfusion date falls within ward start and end
--               --case when MaxWS.Ward IS NULL then  #ws.Wardcode Else NULL End as LastKnownWard ,
--               --#UF_next_sp.AdmissiontWardCode as nextWard,
--               CASE
--                 WHEN MaxWS.OriginalLastLocation LIKE '%Fridge%' THEN COALESCE(MaxWS.Ward, #ws.WardName, #UF_next_sp.AdmissiontWardCode)
--                 WHEN MaxWS.OriginalLastLocation = 'Default Desktop Location' THEN COALESCE(MaxWS.Ward, #ws.WardName, #UF_next_sp.AdmissiontWardCode)
--                 ELSE MaxWS.LastLocation
--               END                                                                                 AS FinalLocation --for all records where the original location in autofate data is not like Fridge then the original location is used otherwise the mappped location is ised, if this is null
--        --then the last ward in the last spell before the transfusion date is used, if this is null then the admission ward of the first spell after the transfusion date is used.           
--        FROM   (SELECT DISTINCT #UF.*,
--                                sp.SourceSpellNo,
--                                sp.AdmissionDate,
--                                sp.DischargeDate,
--                                wd.StartTime     AS WardStartTime,
--                                wd.EndTime       AS WardEndTime,
--                                mp.wardname      AS Ward,
--                                Max(#ws.endtime) MaxWdEnd
--                FROM   #UF
--                       LEFT OUTER JOIN WHREPORTING.APC.spell sp
--                                    ON Isnull(#UF.patientno, 'x') = sp.faciltyID
--                                       AND #UF.LastActivity >= sp.AdmissionDateTime
--                                       AND #UF.LastActivity <= Isnull(sp.DischargeDateTime, Getdate())
--                       LEFT OUTER JOIN WHReporting.APC.WardStay wd
--                                    ON sp.SourceSpellNo = wd.SourceSpellNo
--                                       AND #UF.LastActivity > wd.StartTime
--                                       AND #UF.LastActivity <= Isnull(wd.EndTime, Getdate())
--                       LEFT OUTER JOIN #ws
--                                    ON #UF.patientno = #ws.FaciltyID
--                                       AND #ws.DischargeDateTime < #UF.LastActivity
--                       LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping mp
--                                    ON wd.WardCode = mp.paswardcode
--                WHERE  #UF.NHSNo IS NULL
--                GROUP  BY ID,
--                          patientno,
--                          #UF.NHSNo,
--                          LastLocation,
--                          LastLocationCode,
--                          OriginalLastLocation,
--                          #UF.Division,
--                          BloodUnit,
--                          Product,
--                          LastActivity,
--                          LastTransaction,
--                          Patient,
--                          sp.SourceSpellNo,
--                          AdmissionDate,
--                          DischargeDate,
--                          wd.StartTime,
--                          wd.EndTime,
--                          mp.wardname) MaxWS
--               LEFT OUTER JOIN #ws
--                            ON maxWS.patientno = #ws.FaciltyID
--                               AND MaxWS.MaxWdEnd = #ws.EndTime
--               LEFT OUTER JOIN #UF_next_sp
--                            ON maxWs.patientno = #UF_next_sp.patientno
--                               AND maxWs.LastActivity = #UF_next_sp.LastActivity
--        UNION ALL
--        SELECT Datename(yy, MaxWS.LastActivity) + ' '
--               + Datename(mm, MaxWS.LastActivity)                                                  AS YearMonth,
--               MaxWS.ID,
--               MaxWS.NHSno,
--               MaxWS.patientno,
--               MaxWS.OriginalLastLocation                                                          AS OriginalLocationDetails,
--               MaxWS.LastLocation                                                                  AS OriginalMappedLocationDetails,
--               MaxWS.BloodUnit,
--               MaxWS.Product,
--               MaxWS.LastActivity,
--               MaxWS.Patient,
--               MaxWS.SourceSpellNo,
--               COALESCE(MaxWS.AdmissionDate, #ws.AdmissionDatetime, #UF_next_sp.AdmissionDateTime) AS AdmissionDate,
--               COALESCE(MaxWS.DischargeDate, #ws.dischargeDatetime, #UF_next_sp.DischargeDateTime) AS DischargeDate,
--               COALESCE(MaxWS.WardStartTime, #ws.startTime)                                        AS WardStartTime,
--               COALESCE(MaxWS.WardEndTime, #ws.EndTime)                                            AS WardEndTime,
--               --       MaxWS.LastLocation             AS OriginalLocationDetailsfromAutoTrackReport,
--               MaxWS.Ward                                                                          AS MappedWard,--this is the ward where the transfusion date falls within ward start and end
--               --case when MaxWS.Ward IS NULL then  #ws.Wardcode Else NULL End as LastKnownWard ,
--               --#UF_next_sp.AdmissiontWardCode as nextWard,
--               CASE
--                 WHEN MaxWS.OriginalLastLocation LIKE '%Fridge%' THEN COALESCE(MaxWS.Ward, #ws.WardName, #UF_next_sp.AdmissiontWardCode)
--                 WHEN MaxWS.OriginalLastLocation = 'Default Desktop Location' THEN COALESCE(MaxWS.Ward, #ws.WardName, #UF_next_sp.AdmissiontWardCode)
--                 ELSE MaxWS.LastLocation
--               END                                                                                 AS FinalLocation --for all records where the original location in autofate data is not like Fridge then the original location is used otherwise the mappped location is ised, if this is null
--        --then the last ward in the last spell before the transfusion date is used, if this is null then the admission ward of the first spell after the transfusion date is used.           
--        FROM   (SELECT DISTINCT #UF.*,
--                                sp.SourceSpellNo,
--                                sp.AdmissionDate,
--                                sp.DischargeDate,
--                                wd.StartTime     AS WardStartTime,
--                                wd.EndTime       AS WardEndTime,
--                                mp.WardName      AS Ward,
--                                Max(#ws.endtime) MaxWdEnd
--                FROM   #UF
--                       LEFT OUTER JOIN WHREPORTING.APC.spell sp
--                                    ON #UF.NHSNo = sp.NHSNo
--                                       AND #UF.LastActivity >= sp.AdmissionDateTime
--                                       AND #UF.LastActivity <= Isnull(sp.DischargeDateTime, Getdate())
--                       LEFT OUTER JOIN WHReporting.APC.WardStay wd
--                                    ON sp.SourceSpellNo = wd.SourceSpellNo
--                                       AND #UF.LastActivity > wd.StartTime
--                                       AND #UF.LastActivity <= Isnull(wd.EndTime, Getdate())
--                       LEFT OUTER JOIN #ws
--                                    ON #UF.NHSNo = #ws.NHSNo
--                                       AND #ws.DischargeDateTime < #UF.LastActivity
--                       LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping mp
--                                    ON wd.WardCode = mp.paswardcode
--                WHERE  #UF.NHSNo IS NOT NULL
--                GROUP  BY ID,
--                          patientno,
--                          #UF.NHSNo,
--                          LastLocation,
--                          LastLocationCode,
--                          OriginalLastLocation,
--                          #UF.Division,
--                          BloodUnit,
--                          Product,
--                          LastActivity,
--                          LastTransaction,
--                          Patient,
--                          sp.SourceSpellNo,
--                          AdmissionDate,
--                          DischargeDate,
--                          wd.StartTime,
--                          wd.EndTime,
--                          mp.wardname) MaxWS
--               LEFT OUTER JOIN #ws
--                            ON maxWS.NHSNo = #ws.NHSNo
--                               AND MaxWS.MaxWdEnd = #ws.EndTime
--               LEFT OUTER JOIN #UF_next_sp
--                            ON maxWs.patientno = #UF_next_sp.patientno
--                               AND maxWs.LastActivity = #UF_next_sp.LastActivity




------------------------------
----Temp table to get the location and month combination to be uased as a base in the final query 
------------------------------

--IF Object_id('tempdb..#Cal_mp') > 0
--  DROP TABLE #Cal_mp

--CREATE TABLE #Cal_mp
--  (
--     YearMonth         VARCHAR(50),
--     FirstDayOfMonth	DATETIME,
--     Division             VARCHAR(100),
--     Wardcode		VARCHAR(50),
--     WardName       VARCHAR(100)
--	  )
--INSERT INTO #Cal_mp	  
--SELECT DISTINCT Datename(yy, TheDate) + ' '
--                + Datename(mm, TheDate)                            AS YearMonth,
--                CONVERT(DATETIME, '01/'
--                                  + CONVERT(VARCHAR, Datename(month, TheDate))
--                                  + '/' + Datename(year, TheDate)) AS FirstDayOfMonth,
--                Isnull(Division, 'Unknown')                        AS Division,
--                WardCode,
--                WardName
--FROM   WHREPORTING.LK.Calendar
--       CROSS JOIN (SELECT *
--                   FROM   WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping
--                  UNION ALL
--                   SELECT 'Unknown',--union to  a select to give the null values in the Fated and unfated data 
--                          'Unknown',
--                          NULL,
--                          NULL,
--                          NULL,
--                          'Unknown') TransfusionPASBloodTrackLoc_mapping
--WHERE  TheDate >= (SELECT CONVERT(DATETIME, CONVERT(VARCHAR, Min(Date), 101))
--                   FROM   #FinalFT)
--       AND TheDate < (SELECT CONVERT(DATETIME, CONVERT(VARCHAR, Dateadd(day, 1, Max(Date)), 101))
--                      FROM   #FinalFT) 



-----------------------------
----Final Query
-----------------------------
--Select  mp.YearMonth,
--		mp.firstdayofmonth,
--		mp.division,
--		mp.wardcode,
--		mp.wardname,
--		COUNT(distinct UF.ID) UnfatedTransfusions,
--		COUNT(distinct FT.ID) FatedTransfusions,
--		count(distinct case when FT.Fated in ('Fated Correctly') then FT.ID end) as  FatedCorrectly,
--		count(distinct case when FT.Fated not in ('Fated Correctly') then FT.ID end) as  FatedinNonClinicalArea,
--		COUNT(distinct UF.ID)+COUNT(distinct FT.ID) as Total
--from #Cal_mp mp
--Left Outer Join #FinalUFT UF on mp.yearmonth = UF.YearMonth and mp.wardname = isnull(UF.FinalLocation,'Unknown')
--Left outer Join #FinalFT FT on mp.yearmonth = FT.YearMonth and mp.wardname =  isnull(FT.FinalLocation,'Unknown')
--where mp.firstdayofmonth in (SELECT Item
--                       FROM   dbo.Split (@monthstart, ','))
--Group by   mp.YearMonth,
--		mp.firstdayofmonth,
--		mp.division,
--		mp.wardcode,
--		mp.wardname
--having  COUNT(distinct UF.ID)+COUNT(distinct FT.ID) <> 0	
END