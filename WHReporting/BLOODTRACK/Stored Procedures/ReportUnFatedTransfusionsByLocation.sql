﻿-- =============================================
-- Author:		CMan 
-- Create date: 27/12/2013
-- Description:	Reporting on AutoFate data.  Stored Procedure for SSRS report 'UnFated Transfusions by Location'.
--				Requested by MGerrard. Spec discussed with Emma Milser.
--				RD438
-- =============================================
CREATE PROCEDURE [BLOODTRACK].[ReportUnFatedTransfusionsByLocation]
@monthstart varchar(max)
	-- Add the parameters for the stored procedure here
AS
BEGIN

--Declare @monthstart datetime = '20131001'

Select Division,
	FinalLocation,
	YearMonth,
	FirstDayOfMonth,
	SUM(UnfatedTransfusionsFirst) as CountTransfusions,
	SUM(UnfatedTransfusionsRefresh) as CountTransfusionsRefresh

from WHREPORTING.BLOODTRACK.ReportTransfusionsFatedUnfatedCombined
where (UnfatedTransfusionsFirst <> 0
	OR    UnfatedTransfusionsRefresh <> 0)
	AND FirstDayOfMonth  in (SELECT Item
                         FROM   dbo.Split (@MonthStart, ','))

Group By Division,
	FinalLocation,
	YearMonth,
	FirstDayOfMonth




 
-- --------------------------
----UnFated List Temp table 
----------------------------
----Drop temp table if already exists 
--IF Object_id('tempdb..#UF') > 0
--  DROP TABLE #UF

----create temp table from original data with an RM2 number split out from patients name column and a unique id to use
--CREATE TABLE #UF
--  (
--     ID                   INT IDENTITY (1, 1),
--     patientno            VARCHAR(20),
--     NHSNo                VARCHAR(10),
--     BloodUnit            VARCHAR(50),
--     Product              VARCHAR(100),
--     --ProductCode varchar(10),
--     OriginalLastLocation VARCHAR(50),--this is the location which comes from the Blood Track Report
--     LastLocation         VARCHAR(50),--this is the mapped location from the  WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping table
--     LastLocationCode     VARCHAR(50),--this is the mapped location code from the  WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping table
--     Division             VARCHAR(50),
--     LastActivity         DATETIME,
--     LastTransaction      VARCHAR(50),
--     Patient              VARCHAR(100)
--  )

--INSERT INTO #UF
--SELECT CASE
--         WHEN Len(CONVERT(VARCHAR(20), CONVERT(BIGINT, CASE
--                                                         WHEN Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '') LIKE '%[a-z]%' THEN NULL
--                                                         ELSE Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '')
--                                                       END))) = 10 THEN NULL
--         ELSE 'RM2' + CONVERT(VARCHAR(20), CONVERT(BIGINT, CASE WHEN Replace(Substring(patient, Charindex('(', patient)+1, Len(patient)), ')', '') LIKE '%[a-z]%' THEN NULL ELSE Replace(Substring(patient, Charindex('(', patient)+1, Len(patient)), ')', '') END))
--       END AS patientno,
--       CASE
--         WHEN Len(CONVERT(VARCHAR(20), CONVERT(BIGINT, CASE
--                                                         WHEN Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '') LIKE '%[a-z]%' THEN NULL
--                                                         ELSE Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '')
--                                                       END))) = 10 THEN CONVERT(VARCHAR(20), CONVERT(BIGINT, CASE
--                                                                                                               WHEN Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '') LIKE '%[a-z]%' THEN NULL
--                                                                                                               ELSE Replace(Substring(patient, Charindex('(', patient) + 1, Len(patient)), ')', '')
--                                                                                                             END))
--         ELSE NULL
--       END AS NHSNo,
--       [Blood Unit],
--       [Product],
--       [Last Location],
--       b.wardname,--this is the mapped wardname from the PAS to Blood Track Location mapping table
--       b.wardcode,--this is the mapped wardcode from the PAS to Blood Track Location mapping table
--       b.division,--this is the division from the PAS to Blood Track Location mapping table - mappings approved by Emma Milser
--       [Last Activity],
--       [Last Transaction],
--       [Patient]
--FROM   WHREPORTING.dbo.TransfusionUnfatedUnits a
--       LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping b
--                    ON Isnull(a.[Last Location], 'x') = b.MappedAutoTrackWard 

------------------------------------
----Temp table for the last ward per spell where the ward is not Discharge Lounge
------------------------------------
----To be used in the final query to identify the last ward for the patient's transfusion where no ward can be identified over the period of the transfusion date

----Drop temp table if already exists 
--IF Object_id('tempdb..#ws') > 0
--  DROP TABLE #ws

--CREATE TABLE #ws
--  (
--     SourcePatientNo   VARCHAR(20),
--     NHSNo             VARCHAR(20),
--     SourceSpellNo     VARCHAR(20),
--     FaciltyID         VARCHAR(20),
--     StartTime         DATETIME,
--     EndTime           DATETIME,
--     AdmissionDateTime DATETIME,
--     DischargeDateTime DATETIME,
--     WardCode          VARCHAR(20),
--     WardName          VARCHAR(50)
--  )

--INSERT INTO #ws
--SELECT ws.SourcePatientNo,
--       sp.NHSNo,
--       ws.SourceSpellNo,
--       sp.FaciltyID,
--       ws.StartTime,
--       ws.EndTime,
--       sp.AdmissionDateTime,
--       sp.DischargeDateTime,
--       ws.WardCode,
--       mp.WardName
--FROM   (SELECT SourceSpellNo,
--               Max(EndTime) MaxEndTime
--        FROM   WHREPORTING.APC.WardStay
--        WHERE  Isnull(WardCode, 'x') <> 'DL'
--        GROUP  BY SourceSpellNo) a
--       LEFT OUTER JOIN WHREPORTING.APC.WardStay ws
--                    ON a.SourceSpellNo = ws.SourceSpellNo
--                       AND a.MaxEndTime = ws.EndTime
--       LEFT OUTER JOIN WHREPORTING.APC.Spell sp
--                    ON ws.SourceSpellNo = sp.SourceSpellNo
--       LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping mp
--                    ON ws.WardCode = mp.PasWardCode






-------------------------------
----UnFated data - next ward after transfusion date
------------------------------
----Temp table identifying the admission ward for the next spell admission after the transfusion date for unfated data
----to be used to identify a ward for those transfusion records where no location can be identified even after trying to match to a wardstay which covers the transfusion date
----and looking back to last spell ward
----Only looks at those records in the unfated data which have an admission date which is after the tranfusion date to grab the date, time and ward of the next spell
----then takes the admission ward details of the first admission which is after the transfusion date


----Drop temp table if already exists 
--IF Object_id('tempdb..#UF_next_sp') > 0
--  DROP TABLE #UF_next_sp

--CREATE TABLE #UF_next_sp
--  (
--     ID                 INT,
--     patientno          VARCHAR(20),
--     NHSno              VARCHAR(20),
--     LastLocation       VARCHAR(50),
--     BloodUnit          VARCHAR(50),
--     Product            VARCHAR(100),
--     LastActivity       DATETIME,
--     LastTransaction    VARCHAR(50),
--     Patient            VARCHAR(50),
--     SourcePatientNo    VARCHAR(20),
--     AdmissionDateTime  DATETIME,
--     DischargeDateTime  DATETIME,
--     AdmissiontWardCode VARCHAR(20),
--     AdmissionWardName  VARCHAR(50)
--  )

--INSERT INTO #UF_next_sp
--SELECT min_sp.ID,
--       min_sp.patientno,
--       min_sp.NHSno,
--       min_sp.LastLocation,
--       min_sp.BloodUnit,
--       min_sp.Product,
--       min_sp.LastActivity,
--       min_sp.LastTransaction,
--       min_sp.Patient,
--       min_sp.SourcePatientNo,
--       Spell.AdmissionDateTime,
--       Spell.DischargeDateTime,
--       Spell.AdmissiontWardCode,
--       wdmap.WardName AS AdmissionWardName
--FROM   (SELECT #UF.ID,
--               #UF.patientno,
--               #UF.NHSno,
--               #UF.LastLocation,
--               #UF.BloodUnit,
--               #UF.Product,
--               #UF.LastActivity,
--               #UF.LastTransaction,
--               #UF.Patient,
--               sp.SourcePatientNo,
--               Min(AdmissionDateTime) min_admdate
--        FROM   #UF
--               LEFT OUTER JOIN WHREPORTING.APC.spell sp
--                            ON Isnull(#UF.patientno, 'x') = sp.faciltyID
--                               AND sp.AdmissionDateTime > #UF.LastActivity
--        WHERE  #UF.NHSno IS NULL
--        GROUP  BY #UF.ID,
--                  #UF.patientno,
--                  #UF.NHSno,
--                  #UF.LastLocation,
--                  #UF.BloodUnit,
--                  #UF.Product,
--                  #UF.LastActivity,
--                  #UF.LastTransaction,
--                  #UF.Patient,
--                  sp.SourcePatientNo
--        UNION ALL
--        SELECT #UF.ID,
--               #UF.patientno,
--               #UF.NHSno,
--               #UF.LastLocation,
--               #UF.BloodUnit,
--               #UF.Product,
--               #UF.LastActivity,
--               #UF.LastTransaction,
--               #UF.Patient,
--               sp.SourcePatientNo,
--               Min(AdmissionDateTime) min_admdate
--        FROM   #UF
--               LEFT OUTER JOIN WHREPORTING.APC.spell sp
--                            ON #UF.NHSno = sp.NHSNo
--                               AND sp.AdmissionDateTime > #UF.LastActivity
--        WHERE  #UF.NHSno IS NOT NULL
--        GROUP  BY #UF.ID,
--                  #UF.patientno,
--                  #UF.NHSno,
--                  #UF.LastLocation,
--                  #UF.BloodUnit,
--                  #UF.Product,
--                  #UF.LastActivity,
--                  #UF.LastTransaction,
--                  #UF.Patient,
--                  sp.SourcePatientNo) min_sp
--       LEFT OUTER JOIN WHREPORTING.APC.spell
--                    ON min_sp.SourcePatientNo = Spell.SourcePatientNo
--                       AND min_sp.min_admdate = Spell.AdmissionDateTime
--       LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping WdMap
--                    ON Spell.AdmissiontWardCode = Wdmap.PASWardCode
--WHERE  min_sp.SourcePatientNo IS NOT NULL 




--------------------------------
----UnFated Data Final Query
--------------------------------
----The records in the data which we want to find a location for are thos which are coming through as 'Blood Bank', 'Blood Bank FR', 'ERC 2nd Floor', 'Default Desktop Location'  or where the location has a 'Fridge' in the description.
----All other locations are deemed to have been correctly fated and therefore the location which has been recorded in the AutoFate Blood Track data is the one which is used. 


----Drop temp table if already exists 
--IF Object_id('tempdb..#FinalUFT') > 0
--  DROP TABLE #FinalUFT

--CREATE TABLE #FinalUFT
--  (
--     YearMonth         VARCHAR(50),
--     ID					INT,
--     NHSno             VARCHAR(20),
--     PatientNo			VARCHAR(20),
--     OriginalLocationDetails   VARCHAR(50),
--     OriginalMappedLocationDetails			 VARCHAR(50),--this is the original location mapped
--     BloodUnit         VARCHAR(50),
--     Product           VARCHAR(100),
--     LastActivity              DATETIME,
--     Patient           VARCHAR(50),
--     SourcePatientNo   VARCHAR(50),
--     AdmissionDate DATETIME,
--     DischargeDate DATETIME,
--     WardStartTime DATETIME,
--     WardEndTime DATETIME,
--     MappedWard			VARCHAR(50),
--      FinalLocation		VARCHAR(50)
--	  )
--INSERT INTO #FinalUFT

--------------------------
----Unfated Query
--------------------------
----The records in the data which we want to find a location for are those which are coming through as 'Blood Bank', 'Blood Bank FR', 'ERC 2nd Floor', 'Default Desktop Location'  or where the location has a 'Fridge' in the description.
----All other locations are deemed to have been correctly recorded and therefore the location which has been recorded in the AutoFate Blood Track data is the one which is used. 
--SELECT Datename(yy, MaxWS.LastActivity) + ' '
--               + Datename(mm, MaxWS.LastActivity)                                                  AS YearMonth,
--               MaxWS.ID,
--               MaxWS.NHSno,
--               MaxWS.patientno,
--               MaxWS.OriginalLastLocation                                                          AS OriginalLocationDetails,
--               MaxWS.LastLocation                                                                  AS OriginalMappedLocationDetails,
--               MaxWS.BloodUnit,
--               MaxWS.Product,
--               MaxWS.LastActivity,
--               MaxWS.Patient,
--               MaxWS.SourceSpellNo,
--               COALESCE(MaxWS.AdmissionDate, #ws.AdmissionDatetime, #UF_next_sp.AdmissionDateTime) AS AdmissionDate,
--               COALESCE(MaxWS.DischargeDate, #ws.dischargeDatetime, #UF_next_sp.DischargeDateTime) AS DischargeDate,
--               COALESCE(MaxWS.WardStartTime, #ws.startTime)                                        AS WardStartTime,
--               COALESCE(MaxWS.WardEndTime, #ws.EndTime)                                            AS WardEndTime,
--               --       MaxWS.LastLocation             AS OriginalLocationDetailsfromAutoTrackReport,
--               MaxWS.Ward                                                                          AS MappedWard,--this is the ward where the transfusion date falls within ward start and end
--               --case when MaxWS.Ward IS NULL then  #ws.Wardcode Else NULL End as LastKnownWard ,
--               --#UF_next_sp.AdmissiontWardCode as nextWard,
--               CASE
--                 WHEN MaxWS.OriginalLastLocation LIKE '%Fridge%' THEN COALESCE(MaxWS.Ward, #ws.WardName, #UF_next_sp.AdmissiontWardCode)
--                 WHEN MaxWS.OriginalLastLocation = 'Default Desktop Location' THEN COALESCE(MaxWS.Ward, #ws.WardName, #UF_next_sp.AdmissiontWardCode)
--                 ELSE MaxWS.LastLocation
--               END                                                                                 AS FinalLocation --for all records where the original location in autofate data is not like Fridge then the original location is used otherwise the mappped location is ised, if this is null
--        --then the last ward in the last spell before the transfusion date is used, if this is null then the admission ward of the first spell after the transfusion date is used.           
--        FROM   (SELECT DISTINCT #UF.*,
--                                sp.SourceSpellNo,
--                                sp.AdmissionDate,
--                                sp.DischargeDate,
--                                wd.StartTime     AS WardStartTime,
--                                wd.EndTime       AS WardEndTime,
--                                mp.wardname      AS Ward,
--                                Max(#ws.endtime) MaxWdEnd
--                FROM   #UF
--                       LEFT OUTER JOIN WHREPORTING.APC.spell sp
--                                    ON Isnull(#UF.patientno, 'x') = sp.faciltyID
--                                       AND #UF.LastActivity >= sp.AdmissionDateTime
--                                       AND #UF.LastActivity <= Isnull(sp.DischargeDateTime, Getdate())
--                       LEFT OUTER JOIN WHReporting.APC.WardStay wd
--                                    ON sp.SourceSpellNo = wd.SourceSpellNo
--                                       AND #UF.LastActivity > wd.StartTime
--                                       AND #UF.LastActivity <= Isnull(wd.EndTime, Getdate())
--                       LEFT OUTER JOIN #ws
--                                    ON #UF.patientno = #ws.FaciltyID
--                                       AND #ws.DischargeDateTime < #UF.LastActivity
--                       LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping mp
--                                    ON wd.WardCode = mp.paswardcode
--                WHERE  #UF.NHSNo IS NULL
--                GROUP  BY ID,
--                          patientno,
--                          #UF.NHSNo,
--                          LastLocation,
--                          LastLocationCode,
--                          OriginalLastLocation,
--                          #UF.Division,
--                          BloodUnit,
--                          Product,
--                          LastActivity,
--                          LastTransaction,
--                          Patient,
--                          sp.SourceSpellNo,
--                          AdmissionDate,
--                          DischargeDate,
--                          wd.StartTime,
--                          wd.EndTime,
--                          mp.wardname) MaxWS
--               LEFT OUTER JOIN #ws
--                            ON maxWS.patientno = #ws.FaciltyID
--                               AND MaxWS.MaxWdEnd = #ws.EndTime
--               LEFT OUTER JOIN #UF_next_sp
--                            ON maxWs.patientno = #UF_next_sp.patientno
--                               AND maxWs.LastActivity = #UF_next_sp.LastActivity
--        UNION ALL
--        SELECT Datename(yy, MaxWS.LastActivity) + ' '
--               + Datename(mm, MaxWS.LastActivity)                                                  AS YearMonth,
--               MaxWS.ID,
--               MaxWS.NHSno,
--               MaxWS.patientno,
--               MaxWS.OriginalLastLocation                                                          AS OriginalLocationDetails,
--               MaxWS.LastLocation                                                                  AS OriginalMappedLocationDetails,
--               MaxWS.BloodUnit,
--               MaxWS.Product,
--               MaxWS.LastActivity,
--               MaxWS.Patient,
--               MaxWS.SourceSpellNo,
--               COALESCE(MaxWS.AdmissionDate, #ws.AdmissionDatetime, #UF_next_sp.AdmissionDateTime) AS AdmissionDate,
--               COALESCE(MaxWS.DischargeDate, #ws.dischargeDatetime, #UF_next_sp.DischargeDateTime) AS DischargeDate,
--               COALESCE(MaxWS.WardStartTime, #ws.startTime)                                        AS WardStartTime,
--               COALESCE(MaxWS.WardEndTime, #ws.EndTime)                                            AS WardEndTime,
--               --       MaxWS.LastLocation             AS OriginalLocationDetailsfromAutoTrackReport,
--               MaxWS.Ward                                                                          AS MappedWard,--this is the ward where the transfusion date falls within ward start and end
--               --case when MaxWS.Ward IS NULL then  #ws.Wardcode Else NULL End as LastKnownWard ,
--               --#UF_next_sp.AdmissiontWardCode as nextWard,
--               CASE
--                 WHEN MaxWS.OriginalLastLocation LIKE '%Fridge%' THEN COALESCE(MaxWS.Ward, #ws.WardName, #UF_next_sp.AdmissiontWardCode)
--                 WHEN MaxWS.OriginalLastLocation = 'Default Desktop Location' THEN COALESCE(MaxWS.Ward, #ws.WardName, #UF_next_sp.AdmissiontWardCode)
--                 ELSE MaxWS.LastLocation
--               END                                                                                 AS FinalLocation --for all records where the original location in autofate data is not like Fridge then the original location is used otherwise the mappped location is ised, if this is null
--        --then the last ward in the last spell before the transfusion date is used, if this is null then the admission ward of the first spell after the transfusion date is used.           
--        FROM   (SELECT DISTINCT #UF.*,
--                                sp.SourceSpellNo,
--                                sp.AdmissionDate,
--                                sp.DischargeDate,
--                                wd.StartTime     AS WardStartTime,
--                                wd.EndTime       AS WardEndTime,
--                                mp.WardName      AS Ward,
--                                Max(#ws.endtime) MaxWdEnd
--                FROM   #UF
--                       LEFT OUTER JOIN WHREPORTING.APC.spell sp
--                                    ON #UF.NHSNo = sp.NHSNo
--                                       AND #UF.LastActivity >= sp.AdmissionDateTime
--                                       AND #UF.LastActivity <= Isnull(sp.DischargeDateTime, Getdate())
--                       LEFT OUTER JOIN WHReporting.APC.WardStay wd
--                                    ON sp.SourceSpellNo = wd.SourceSpellNo
--                                       AND #UF.LastActivity > wd.StartTime
--                                       AND #UF.LastActivity <= Isnull(wd.EndTime, Getdate())
--                       LEFT OUTER JOIN #ws
--                                    ON #UF.NHSNo = #ws.NHSNo
--                                       AND #ws.DischargeDateTime < #UF.LastActivity
--                       LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping mp
--                                    ON wd.WardCode = mp.paswardcode
--                WHERE  #UF.NHSNo IS NOT NULL
--                GROUP  BY ID,
--                          patientno,
--                          #UF.NHSNo,
--                          LastLocation,
--                          LastLocationCode,
--                          OriginalLastLocation,
--                          #UF.Division,
--                          BloodUnit,
--                          Product,
--                          LastActivity,
--                          LastTransaction,
--                          Patient,
--                          sp.SourceSpellNo,
--                          AdmissionDate,
--                          DischargeDate,
--                          wd.StartTime,
--                          wd.EndTime,
--                          mp.wardname) MaxWS
--               LEFT OUTER JOIN #ws
--                            ON maxWS.NHSNo = #ws.NHSNo
--                               AND MaxWS.MaxWdEnd = #ws.EndTime
--               LEFT OUTER JOIN #UF_next_sp
--                            ON maxWs.patientno = #UF_next_sp.patientno
--                               AND maxWs.LastActivity = #UF_next_sp.LastActivity
                               
                               
--       --                        )FinalUFT
--       --LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping mp
--       --             ON FinalUFT.FinalLocation = mp.wardname 




--SELECT ISNULL(mp.Division , 'Unknown') as Division,
--       ISNULL(#FinalUFT.FinalLocation, 'Unknown') as FinalLocation,
--       #FinalUFT.YearMonth,
--      cast('01/' + convert(varchar,Datename(month,#FinalUFT.LastActivity)) + '/' + DATENAME(year,#FinalUFT.LastActivity) as Date) as  FirstDayOfMonth,
--       Count(DISTINCT #FinalUFT.ID) CountTransfusions
--FROM   #FinalUFT
--       --join back to the mapping table to get the division of the final location that has been identified after the processing above 
--       LEFT OUTER JOIN WHREPORTING.dbo.TransfusionPASBloodTrackLoc_mapping mp
--                    ON #FinalUFT.FinalLocation = mp.wardname
--where      cast('01/' + convert(varchar,Datename(month,#FinalUFT.LastActivity)) + '/' + DATENAME(year,#FinalUFT.LastActivity) as Date) in (SELECT Item
--                         FROM   dbo.Split (@monthstart, ','))
--GROUP  BY ISNULL(mp.Division, 'Unknown'),
--          ISNULL(#FinalUFT.FinalLocation, 'Unknown') ,
--          #FinalUFT.YearMonth,
--            '01/' + convert(varchar,Datename(month,#FinalUFT.LastActivity)) + '/' + DATENAME(year,#FinalUFT.LastActivity)



END