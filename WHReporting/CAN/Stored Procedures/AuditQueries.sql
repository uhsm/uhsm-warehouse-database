﻿CREATE procedure [CAN].[AuditQueries] as

/**
K Oakden 15/01/2014
List of queries used to query SCR interface audit table

The WHREPORTING.CAN.InterfaceAudit is recreated every morning by the 
'WH - Load SCR Audit' job.

Notes
------------

The InterfaceAudit table is based on 2WW referrals (and routine referrals for 
breast, cancer not suspected) in the data warehouse, and the corrersponding 
records in the cancer interface table and cancer application referral table. 
Referrals entered manually into SCR because they weren't based on a 
2WW referral from PAS are not included.

Changes to updated referrals are not processed by the SCR interface, 
so records updated yesterday but originally created previously should have 
an SCRInterfaceProcessTime to correspond with the Created on PAS / Created in DW date.
eg where SourceUniqueID = 155648692

Records with no SCRInterfaceProcessTime were not sent to the interface, 
this should only happen when referral creation and cancellation date are the same 
(this is one of the problems with the endoscopy stuff)
eg where SourceUniqueID = 155746166

Records with no ReferralUniqueRecordId are not present in the SCR database,
but may have been manually deleted if SCRInterfaceProcessTime exists
eg where SourceUniqueID = 155659900 (guessing it was manually deleted)
**/

--To check all 2WW referrals created yesterday
select * from WHREPORTING.CAN.InterfaceAudit
where dateadd(dd, 0, datediff(dd, 0,CreatedOnPAS)) 
      = dateadd(dd, -1, datediff(dd, 0,getdate()))


--To check all 2WW referrals created in the last week 
--that haven't been processed by interface
select * from WHREPORTING.CAN.InterfaceAudit
where dateadd(dd, 0, datediff(dd, 0,CreatedOnPAS)) 
      > dateadd(dd, -8, datediff(dd, 0,getdate()))
and SCRInterfaceProcessTime is null --and SameDayCancellation  = 0
order by CreatedOnPAS desc

--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--To check all 2WW referrals created in the last x days
--that don't have a corresponding automatic record on SCR.
--Automatic SCR records identified where the SystemId is an IPM referral unique 
--identifier.
select * from WHREPORTING.CAN.InterfaceAudit
where dateadd(dd, 0, datediff(dd, 0,CreatedOnPAS)) 
      > dateadd(dd, -100, datediff(dd, 0,getdate()))
and SCRReferralUniqueRecordId is null and SCRInterfaceProcessTime is not null
order by CreatedOnPAS desc
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

--To check daily count of all 2WW referrals created in the last x days
-- that don't have a corresponding automatic record on SCR
select 
      createdate = dateadd(dd, 0, datediff(dd, 0,CreatedOnPAS))  
      ,cnt = count(1)
from WHREPORTING.CAN.InterfaceAudit
where dateadd(dd, 0, datediff(dd, 0,CreatedOnPAS)) 
      > dateadd(dd, -60, datediff(dd, 0,getdate()))
and SCRReferralUniqueRecordId is null --and SameDayCancellation  = 0
group by dateadd(dd, 0, datediff(dd, 0,CreatedOnPAS))  
order by dateadd(dd, 0, datediff(dd, 0,CreatedOnPAS))  desc


--To check daily count of all 2WW referrals created in the last x days
-- that don't have a interface record
select *
      --createdate = dateadd(dd, 0, datediff(dd, 0,CreatedOnPAS))  
      --,cnt = count(1)
from WHREPORTING.CAN.InterfaceAudit
where dateadd(dd, 0, datediff(dd, 0,CreatedOnPAS)) 
      > dateadd(dd, -60, datediff(dd, 0,getdate()))
and SCRInterfaceProcessTime is null and SameDayCancellation  = 0
--group by dateadd(dd, 0, datediff(dd, 0,CreatedOnPAS))  
order by dateadd(dd, 0, datediff(dd, 0,CreatedOnPAS))  


--Or search on hospital number