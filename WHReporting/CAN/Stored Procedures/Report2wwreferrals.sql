﻿/*
--Author: Helen Ransome
--Date created: 23/05/2013
--Stored Procedure Built for SSRS Report on:
--Cancer all 2 week wait referrals report
*/

CREATE Procedure [CAN].[Report2wwreferrals]

 @StartDate as date
 ,@EndDate as date
 ,@CancerTypePAR as nvarchar(100) 
as

select
d.NHSNumber 
,d.HospitalNumber 
,d.Forename 
,d.Surname 
,ct.CancerType 
,r.CancerSite 
,r.DecisionToReferDate 
,r.ReceiptOfReferralDate 
,c.Consultant 
,s.Specialty 
,r.FirstAppointmentDate 




From WH.SCR.Referral as r


left join [WH].SCR.Demographic as d
on d.UniqueRecordId = r.DemographicUniqueRecordId     


 left join [WH].[SCR].CancerType ct
			  on r.CancerTypeCode = ct.CancerTypeCode 
			  
			   left join [WH].[SCR].[Consultant] c
		  on r.ConsultantCode = c.NationalConsultantCode  
		  
		  	  left join [WH].[SCR].[Specialty] s
	  on r.SpecialtyCode = s.SpecialtyCode
	  
	  
Where 

r.ReceiptOfReferralDate between @StartDate and @EndDate 
and PriorityTypeCode = '03'
  and FirstAppointmentOrganisationCode in ('RM202','RM201')
  and r.CancerTypeCode in(SELECT Val from dbo.fn_String_To_Table(@CancerTypePAR,',',1))
  
    order by ct.CancerType