﻿/*
--Author: Helen Ransome
--Date created: 15/05/2013
--Stored Procedure Built for SSRS Report on:
--Cancer all 2 week wait breaches report
*/

CREATE Procedure [CAN].[ReportAll2wwBreaches]

 @StartDate as date
 ,@EndDate as date
 ,@CancerTypePAR as nvarchar(max) 
as

SELECT NHSNumber,
HospitalNumber,
Forename,
Surname,


r. [UniqueRecordId]
      --,[DemographicUniqueRecordId]
      ,[CancerSite]
      ,[SourceOfReferralCode]
      ,[PriorityTypeCode]
      ,r.DecisionToReferDate
      --,CONVERT (varchar(10), r.DecisionToReferDate, 103)as 'DecisionToReferDate'
      ,r.ReceiptOfReferralDate
      --,CONVERT (varchar(10), r.ReceiptOfReferralDate , 103)as 'Receipt of Referral Date'
      ,r.[ConsultantCode]
      ,c.Title + ' ' + c.Consultant as 'ConsultantName' 
      ,r.[SpecialtyCode]
      ,s.Specialty 
    ,r.FirstAppointmentDate
      --,CONVERT (varchar(10), r.FirstAppointmentDate, 103)as 'First Appointment Date'
      ,[FirstAppointmentOrganisationCode]
      ,[ReferralToFirstAppointmentDelayReasonCode]
      ,r.CancerTypeCode 
      ,ct.CancerType 
     ,ReferralToFirstAppointmentDelayReasonComments
    ,1 as DataCount  
  FROM [WH].[SCR].[Referral] r
  
  Left join [WH].SCR.Demographic  d
  on r.DemographicUniqueRecordId = d.UniqueRecordId  
  
	  left join [WH].[SCR].[Specialty] s
	  on r.SpecialtyCode = s.SpecialtyCode
   
		  left join [WH].[SCR].[Consultant] c
		  on r.ConsultantCode = c.NationalConsultantCode  
			  
			  left join [WH].[SCR].CancerType ct
			  on r.CancerTypeCode = ct.CancerTypeCode  
  where  
  FirstAppointmentDate between @StartDate and @EndDate
  and PriorityTypeCode = '03'
  and FirstAppointmentOrganisationCode in ('RM202','RM201')
  and r.CancerTypeCode in(SELECT Val from dbo.fn_String_To_Table(@CancerTypePAR,',',1))
  and ReferralToFirstAppointmentDelayReasonComments is not null
  and r.TumourStatusCode <> '07'
  
  
  order by ct.CancerType