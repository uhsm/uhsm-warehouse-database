﻿/*
--Author: Helen Ransome
--Date created: 25/09/2015
--Stored Procedure Built for SSRS Report on:
--Generic store procedure used for Cancer Organisation Parameter
*/

CREATE Procedure [CAN].[ReportCancerFirstSeenOrganisationcodePAR]

as
SELECT 

DISTINCT FirstAppointmentOrganisationCode  

From [WH].SCR.Referral   

Order by FirstAppointmentOrganisationCode