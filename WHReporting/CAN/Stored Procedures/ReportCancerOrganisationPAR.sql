﻿/*
--Author: Helen Ransome
--Date created: 22/12/2014
--Stored Procedure Built for SSRS Report on:
--Generic store procedure used for Cancer Organisation Parameter
*/

CREATE Procedure [CAN].[ReportCancerOrganisationPAR]

as
SELECT 

DISTINCT (Organisation) 

From [WH].SCR.Organisation  

Order by Organisation