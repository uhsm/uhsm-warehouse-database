﻿/*
--Author: Helen Ransome
--Date created: 11/09/2013
--Stored Procedure Built for SSRS Report on:
--Cancer procedures
*/

CREATE Procedure [CAN].[ReportCancerProcedure]

 @StartDate as date
 ,@EndDate as date

 
  
as

SELECT 
 --d.UniqueRecordId 
d.NHSnumber
,d.HospitalNumber 
,d.Forename 
,d.Surname
      --,[ReferralUniqueRecordId]
 
      ,[OrganisationCode]
      --,S.consultantcode
      --,Con.NationalConsultantCode 
     ,Con.Title + ' '+ Con.Consultant as ConsultantName
      --,S.specialtycode
      --,[TreatmentIntentCode]
      ,[DecisionToOperateDate]
      ,[AdmissionDate]
      ,[SurgeryDate]
      --,[MainProcedureSiteCode]
      --,[MainProcedureTypeCode]
      --,[MainProcedureId]
      --,[SubProcedureSite2]
      --,[SubProcedureType2]
      --,[SubProcedureId2]
      --,[SubProcedureSite3]
      --,[SubProcedureType3]
      --,[SubProcedureId3]
      --,[SubProcedureSite4]
      --,[SubProcedureType4]
      --,[SubProcedureId4]
      ,[MainProcedureCode1]
      ,OPCS4 as 'Procedure Description' 
      ,[SubProcedureCode2]
      ,[SubProcedureCode3]
      ,[SubProcedureCode4]
      ,[DischargeDate]
      --,[DischargeDestinationCode]
      --,[TreatmentEventTypeCode]
      ,TE.Event 
      ,[TreatmentSettingCode]
      ,[ClinicalTrial]
      --,[SurgeonCode]
      --,[Surgeon]
  FROM [WH].[SCR].[Surgery] as S
  
 
 
  
   Left join [WH].SCR.Demographic  d
  on d.UniqueRecordId = S.UniqueRecordId
  
  
  left join [WH].[SCR].[OPCS4] Ops
	  on S.MainProcedureCode1 = Ops.OPCS4Code  
  
     left join
  [WH].[SCR].[Consultant] Con
  on S.consultantcode = Con.NationalConsultantCode
  
  left join [WH].[SCR].[TreatmentEvent] TE
  on TE.EventCode = S.TreatmentEventTypeCode 
  
  
  Where
  
  AdmissionDate between @StartDate and @EndDate
  
  AND OrganisationCode in ('RM202', 'RM201')
   and OPCS4 is not null
 
  
  ORDER BY OPCS4