﻿/*
--Author: Helen Ransome
--Date created: 11/09/2013
--Stored Procedure Built for SSRS Report on:
--Cancer Procedire Parameter report
*/

CREATE Procedure [CAN].[ReportCancerProcedurePAR]

 @StartDate as date
 ,@EndDate as date
 
as

SELECT 
     DISTINCT [OPCS4] 
      
  FROM [WH].[SCR].[OPCS4] 
  
  --where 
  --AdmissionDate between @StartDate and @EndDate
  
  ORDER BY OPCS4