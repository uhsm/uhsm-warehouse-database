﻿/*
--Author: Helen Ransome
--Date created: 23/05/2013
--Stored Procedure Built for SSRS Report on:
--Cancer Site Parameter report
*/

CREATE Procedure [CAN].[ReportCancerSitePAR]

 @StartDate as date
 ,@EndDate as date
 
as

SELECT 
     DISTINCT [CancerSite]
      
  FROM [WH].[SCR].[Referral]
  
  where 
  ReceiptOfReferralDate between @StartDate and @EndDate
  
  ORDER BY CancerSite