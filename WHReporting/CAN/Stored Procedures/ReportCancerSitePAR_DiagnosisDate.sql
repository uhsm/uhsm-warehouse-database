﻿/*
--Author: Helen Ransome
--Date created: 09/07/2013
--Stored Procedure Built for SSRS Report on:
--Cancer Site Parameter report
This is to be used for reports based on diagnosis date
*/

CREATE Procedure [CAN].[ReportCancerSitePAR_DiagnosisDate]

 @StartDate as date
 ,@EndDate as date
 
as

SELECT 
     DISTINCT [CancerSite]
      
  FROM [WH].[SCR].[Referral]
  
  where 
  DiagnosisDate between @StartDate and @EndDate
  
  ORDER BY CancerSite