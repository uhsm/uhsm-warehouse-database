﻿/*
--Author: Helen Ransome
--Date created: 19/06/2013
--Stored Procedure Built for SSRS Report on:
--Surgery Admission Date Report
*/

CREATE Procedure [CAN].[ReportCancerSurgery_CancerSite]

 @StartDate as date
 ,@EndDate as date
 --,@CancerSitePAR as nvarchar(max) 
as

SELECT 
DISTINCT 

--d.NHSNumber
--      ,d.HospitalNumber
--      ,d.Surname
--      ,d.Forename
      Ref.CancerSite 
  --  ,Con.Title + ' '+ Con.Consultant as ConsultantName
  --    ,S.[ConsultantCode]
  --    ,Spec.Specialty as SpecialtyDescription
  --    ,Con.[SpecialtyCode]
      
  --    ,S.[DecisionToOperateDate]
  --    ,S.[AdmissionDate]
  --,Ops.[OPCS4] as ProcedureDescription
      
  --    ,TE.[Event] as TreatmentEvent
  --   ,TS.[TreatmentSetting] as TreatmentSetting
      
     
    
      
  FROM [WH].[SCR].[Surgery] as S 
  
   Left join
  [WH].[SCR].[Referral] Ref
  on S.ReferralUniqueRecordId = Ref.UniqueRecordId
  
  --Left join [WH].SCR.Demographic  d
  --on ref.DemographicUniqueRecordId = d.UniqueRecordId
  
  --  left join
  --[WH].[SCR].[Consultant] Con
  --on S.consultantcode = Con.NationalConsultantCode
  
		--left join [WH].[SCR].[Specialty] spec
	 -- on Con.SpecialtyCode = spec.SpecialtyCode
	  
	 -- left join [WH].[SCR].[OPCS4] Ops
	 -- on S.MainProcedureCode1 = Ops.OPCS4Code  
	  
	 -- left join [WH].[SCR].[TreatmentEvent] TE
	 -- on S.TreatmentEventTypeCode =TE.EventCode 
	  
	 --left join [WH].[SCR].[TreatmentSetting]TS
	 --on S.TreatmentSettingCode = TS.TreatmentSettingCode 
	 
	
  

  
  WHERE   
  LEFT (S.OrganisationCode,3) = 'RM2'
  AND 
  AdmissionDate between @StartDate and @EndDate 
  --AND  
  --Ref.CancerSite in(SELECT Val from dbo.fn_String_To_Table(@CancerSitePAR,',',1))
  ORDER BY CancerSite