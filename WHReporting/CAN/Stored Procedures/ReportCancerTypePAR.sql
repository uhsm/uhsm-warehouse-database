﻿/*
--Author: Helen Ransome
--Date created: 16/05/2013
--Stored Procedure Built for SSRS Report on:
--Generic store procedure used for Cancer Type Parameter
*/

CREATE Procedure [CAN].[ReportCancerTypePAR]

as
SELECT [CancerTypeCode]
      ,[CancerType]
      
  FROM [WH].[SCR].[CancerType]
  Order by CancerType