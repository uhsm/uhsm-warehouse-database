﻿/******************************************************************************
 Description: CANCER - Deaths within 30 days of administration of Chemotherapy
              For SSRS report. 
  
 Parameters:  @StartDate - Start date for data extract
              @EndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author           Change
 12/06/13 Nick Scattergood Created
 10/12/13 Tim Dean         Change date of death from SCR to use date of death
                           from iPM - RD449
******************************************************************************/


CREATE Procedure [CAN].[ReportDeathsWithin30DaysOfChemo]
 
 @StartDate as date
 ,@EndDate as date
 
As

SELECT d.NHSNumber,
       d.HospitalNumber,
       d.Surname,
       d.Forename,
       C.OrganisationCode,
       d.DateOfBirth,
       --d.DateOfDeath, -- removed 10/12/13 TJD
       Cast(p.DTTM_OF_DEATH AS DATE)                          DateOfDeath, -- added 10/12/13 TJD
       c.ConsultantCode,
       con.Consultant,
       s.Specialty,
       ref.CancerSite,
       [DecisionToTreatDate],
       [InitialStartDate],
       CT.LatestDateOfTreatment,
       [EndDate],
       [ActualCyclesCourses],
       [RouteOfAdministration],
       C.[Comments],
       1                                                      AS DataCount,
       --Datediff(day, CT.LatestDateOfTreatment, d.DateOfDeath) AS DaysBetweenLastChemoAndDateOfDeath -- removed 10/12/13 TJD
       Datediff(day, CT.LatestDateOfTreatment, p.DTTM_OF_DEATH) AS DaysBetweenLastChemoAndDateOfDeath -- added 10/12/13 TJD
FROM   [WH].[SCR].[Chemotherapy] C
       LEFT JOIN [WH].[SCR].[Referral] Ref
              ON C.ReferralUniqueRecordId = Ref.UniqueRecordId
       LEFT JOIN [WH].SCR.Demographic d
              ON ref.DemographicUniqueRecordId = d.UniqueRecordId
       LEFT JOIN [WH].[SCR].[Specialty] s
              ON ref.SpecialtyCode = s.SpecialtyCode
       LEFT JOIN [WH].[SCR].[Consultant] Con
              ON C.consultantcode = Con.NationalConsultantCode
       LEFT JOIN (SELECT [CHEMO_ID]              AS JoinKey,
                         Max([N9_15_EVENT_DATE]) AS [LatestDateOfTreatment]
                  FROM   [WH].[SCR].[MainChemoTreatment]
                  GROUP  BY [CHEMO_ID]) CT
              ON CT.JoinKey = C.UniqueRecordId
       -- added 10/12/13 TJD two extra joins to link iPM patient demographics
       LEFT JOIN Lorenzo.dbo.PatientIdentifier pid
              ON ( d.HospitalNumber = pid.IDENTIFIER )
       LEFT JOIN Lorenzo.dbo.Patient p
              ON ( pid.PATNT_REFNO = p.PATNT_REFNO )
WHERE  Cast(p.DTTM_OF_DEATH AS DATE) BETWEEN @StartDate AND @EndDate -- added 10/12/13 TJD
       AND Datediff(day, CT.LatestDateOfTreatment, p.DTTM_OF_DEATH) < 31 -- added 10/12/13 TJD
       --DateOfDeath BETWEEN @StartDate AND @EndDate -- removed 10/12/13 TJD
       --AND Datediff(day, CT.LatestDateOfTreatment, d.DateOfDeath) < 31 -- removed 10/12/13 TJD
       AND LEFT (OrganisationCode, 3) = 'RM2'
ORDER  BY p.DTTM_OF_DEATH ASC -- added 10/12/13 TJD
          --DateOfDeath ASC  -- removed 10/12/13 TJD