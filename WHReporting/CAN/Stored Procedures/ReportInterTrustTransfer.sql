﻿/*
--Author: Helen Ransome
--Date created: 22/12/2014
--Stored Procedure Built for SSRS Report on:
--Cancer Inter Trust Transfer Report
*/

CREATE Procedure [CAN].[ReportInterTrustTransfer]

 @StartDate as date
 ,@EndDate as date
 --,@CancerTypePAR as nvarchar(100) 
as

Select

D.Forename 
,D.Surname 
,D.HospitalNumber 
,D.NHSNumber 
--,D.DateOfBirth 
,r.ConsultantUpgradeDate 
--,PriorityTypeCode 
,P.Priority 
,R.CancerSite 
--,R.ConsultantCode 
,con.Consultant 
,r.ReceiptOfReferralDate 
,R.FirstAppointmentDate
,FirstAppointmentOrganisationCode 
,O.Organisation 
--,R.CancerStatusCode  
,R.DiagnosisDate 
,T.TrackingComment 
,C.CancerStatus 
--,R.TertiaryCentreReferredToDate 
--,R.TertiaryCentreOrganisationCode 
,S.AdmissionDate as 'Surgery Date'
,chem.InitialStartDate as 'Chemotherapy Date'
,rt.BreachDate62Days

From WH.SCR.Referral as r

left join WH.SCR.Demographic as d
on r.DemographicUniqueRecordId = d.UniqueRecordId 

left join WH.SCR.TrackingComment as T
on r.UniqueRecordId = T.ReferralUniqueRecordId
and not exists
(
      select 1 from WH.SCR.TrackingComment PreviousComment
      where T.ReferralUniqueRecordId  = PreviousComment.ReferralUniqueRecordId 
      and  T.UniqueRecordId < PreviousComment.UniqueRecordId 
)
 

left join WH.SCR.CancerStatus as C
on r.CancerStatusCode =C.CancerStatusCode   

left join WH.SCR.Priority as P
on r.PriorityTypeCode = P.PriorityCode 

left join WH.SCR.Surgery as S
on r.UniqueRecordId = S.ReferralUniqueRecordId 

left join WH.SCR.Chemotherapy as chem
on r.UniqueRecordId = chem.ReferralUniqueRecordId 

left join WH.SCR.Consultant as con
on r.ConsultantCode = con.NationalConsultantCode 

left join WH.SCR.Organisation as O
on r.FirstAppointmentOrganisationCode = O.OrganisationCode  

left join WHOLAP.dbo.BaseSCRReferralTreatment rt
on rt.UniqueRecordId = r.UniqueRecordId
and dateadd(day, 0, datediff(day, 0, getdate())) = censusDate
and BreachDate62Days is not null


Where FirstAppointmentOrganisationCode not like 'RM2%'
and R.ReceiptOfReferralDate between @startDate and @enddate
and R.CancerStatusCode  not in ('03','07','08','18','21')