﻿/*
--Author: Helen Ransome
--Date created: 22/12/2014
--Stored Procedure Built for SSRS Report on:
--Cancer Inter Trust Treated Report
*/

CREATE Procedure [CAN].[ReportInterTrustTreated]

 @StartDate as date
 ,@EndDate as date
 --,@CancerTypePAR as nvarchar(100) 
as

select * from (
Select

d.Forename 
,d.Surname
,d.HospitalNumber
,d.NHSNumber 
,d.DateOfBirth 
--,r.PriorityTypeCode 
,P.Priority  
,r.ReceiptOfReferralDate 
,r.FirstAppointmentOrganisationCode 
,O.Organisation 
,r.DiagnosisDate 
,r.PrimaryDiagnosisCode 
,r.CancerSite 
--,r.CancerStatusCode
,can.CancerStatus 
,T.TrackingComment  
--,S.AdmissionDate as 'Surgery Date'
--,S.MainProcedureCode1 as'Surgery Procedure Code'
--,C.InitialStartDate as 'Chemotherapy Date'
,S.AdmissionDate as 'Surgery Date'
            --,S.MainProcedureCode1 as'Surgery Procedure Code'
            ,C.InitialStartDate as 'Chemotherapy Date'
            ,case 
                  when S.AdmissionDate <= C.InitialStartDate
                        or InitialStartDate is null
                        then S.AdmissionDate
                  when S.AdmissionDate >= C.InitialStartDate
                        or AdmissionDate is null
                        then C.InitialStartDate
                  else null
                  end as 'Treatment Date'
            ,case 
                  when S.AdmissionDate <= C.InitialStartDate
                        or InitialStartDate is null
                        then 'Surgery'
                  when S.AdmissionDate >= C.InitialStartDate
                        or AdmissionDate is null
                        then 'Chemotherapy'
                  else null
                  end as 'First Treatment Type'
            ,case 
                  when S.AdmissionDate <= C.InitialStartDate
                        or InitialStartDate is null
                        then s.DecisionToOperateDate
                  when S.AdmissionDate >= C.InitialStartDate
                        or AdmissionDate is null
                        then c.DecisionToTreatDate
                  else null
                  end as 'Decision To Treat'
            ,r.UniqueRecordId

FROM WH.SCR.Referral as r

left join WH.SCR.Demographic as d
on r.DemographicUniqueRecordId = d.UniqueRecordId 


left join WH.SCR.TrackingComment as T
on r.UniqueRecordId = T.ReferralUniqueRecordId 
and not exists
(
      select 1 from WH.SCR.TrackingComment PreviousComment
      where T.ReferralUniqueRecordId  = PreviousComment.ReferralUniqueRecordId 
      and  T.UniqueRecordId < PreviousComment.UniqueRecordId 
)

left join WH.SCR.CancerStatus as can
on r.CancerStatusCode = can.CancerStatusCode 




left join WH.SCR.Surgery as S
            on r.UniqueRecordId = S.ReferralUniqueRecordId
                  and not exists (
                        select 1
                        from WH.SCR.Surgery PreviousSurgery
                        where S.ReferralUniqueRecordId = PreviousSurgery.ReferralUniqueRecordId
                              and S.AdmissionDate > PreviousSurgery.AdmissionDate
)

--(Select 
--ReferralUniqueRecordId
--,MIN(AdmissionDate)as AdmissionDate
--From 

--WH.SCR.Surgery
--Group by ReferralUniqueRecordId ) as S
--on r.UniqueRecordId = S.ReferralUniqueRecordId  

--left join

--(Select
--ReferralUniqueRecordID
--,MIN(InitialStartDate) as InitialStartDate

left join WH.SCR.Chemotherapy as C
            on r.UniqueRecordId = C.ReferralUniqueRecordId
                  and not exists (
                        select 1
                        from WH.SCR.Chemotherapy PreviousChemo
                        where C.ReferralUniqueRecordId = PreviousChemo.ReferralUniqueRecordId
                              and C.InitialStartDate > PreviousChemo.InitialStartDate
                        )


--From

-- WH.SCR.Chemotherapy 
--Group by ReferralUniqueRecordId )as C
--on r.UniqueRecordId = C.ReferralUniqueRecordId 

left join WH.SCR.Priority as P
on r.PriorityTypeCode = P.PriorityCode  

left join WH.SCR.Organisation as O
on r.FirstAppointmentOrganisationCode = O.OrganisationCode 

Where 

 FirstAppointmentOrganisationCode not like 'RM2%'
 and r.CancerStatusCode not like '03'
 
 --and DiagnosisDate between '1 sep 2014' and '30 nov 2014'
 
 )query
where [Treatment Date] between @StartDate and @EndDate