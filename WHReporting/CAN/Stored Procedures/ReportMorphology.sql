﻿/*
--Author: Helen Ransome
--Date created: 09/07/2013
--Stored Procedure Built for SSRS Report on:
--Morphology procedures
*/

CREATE Procedure [CAN].[ReportMorphology]

 @StartDate as date
 ,@EndDate as date
 ,@CancerSitePAR as nvarchar(1000)
 ,@MorphologyPAR as nvarchar(4000)
  
as

SELECT TOP 1000
NHSNumber,
HospitalNumber,
Forename,
Surname,


r. [UniqueRecordId]
      --,[DemographicUniqueRecordId]
      ,[CancerSite]
      --,[SourceOfReferralCode]
      --,[PriorityTypeCode]
      --,r.DecisionToReferDate
      --,CONVERT (varchar(10), r.DecisionToReferDate, 103)as 'DecisionToReferDate'
      --,r.ReceiptOfReferralDate
      --,CONVERT (varchar(10), r.ReceiptOfReferralDate , 103)as 'Receipt of Referral Date'
      ,r.[ConsultantCode]
      ,c.Title + ' ' + c.Consultant as 'ConsultantName' 
      ,r.[SpecialtyCode]
      ,s.Specialty 
      ,r.DiagnosisDate 
      ,r.DiagnosisComments 
      ,Sn.SNOMEDDescription as 'Morphology'
    --,r.FirstAppointmentDate
      --,CONVERT (varchar(10), r.FirstAppointmentDate, 103)as 'First Appointment Date'
      --,[FirstAppointmentOrganisationCode]
      --,[ReferralToFirstAppointmentDelayReasonCode]
      --,r.CancerTypeCode 
      --,ct.CancerType 
     --,ReferralToFirstAppointmentDelayReasonComments
    --,1 as DataCount  
  FROM [WH].[SCR].[Referral] r
  
  Left join [WH].SCR.Demographic  d
  on r.DemographicUniqueRecordId = d.UniqueRecordId  
  
	  left join [WH].[SCR].[Specialty] s
	  on r.SpecialtyCode = s.SpecialtyCode
   
		  left join [WH].[SCR].[Consultant] c
		  on r.ConsultantCode = c.NationalConsultantCode  
			  
			  left join [WH].[SCR].CancerType ct
			  on r.CancerTypeCode = ct.CancerTypeCode  
			  
			  left join [WH].[SCR].[SNOMED] Sn
			  on Sn.SNOMEDCode = r.HistologyCode 
  where  
  DiagnosisDate between @StartDate and @EndDate

  and 
  DiagnosisOrganisationCode in ('RM202', 'RM201')
  and r.CancerSite in(SELECT Val from dbo.fn_String_To_Table(@CancerSitePAR,',',1))
  and r.HistologyCode in(SELECT Val from dbo.fn_String_To_Table(@MorphologyPAR,',',1))  
 
  
  order by r.CancerSite