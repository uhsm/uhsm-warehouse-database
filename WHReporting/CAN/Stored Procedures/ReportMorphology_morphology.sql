﻿/*
--Author: Helen Ransome
--Date created: 09/07/2013
--Stored Procedure Built for SSRS Report on:

Store procedure for Morphology parameters
*/

CREATE Procedure [CAN].[ReportMorphology_morphology]

@StartDate as date
 ,@EndDate as date
 ,@CancerSitePAR as nvarchar(1000)
 
as

SELECT 
     DISTINCT [HistologyCode] as MorphologyCode
     ,Sn.SNOMEDDescription as MorphologyDesc 
      
  FROM [WH].[SCR].[Referral] as r
  
  left join [WH].[SCR].[SNOMED] Sn
			  on Sn.SNOMEDCode = r.HistologyCode 
  
  where 
  
  DiagnosisDate between @StartDate and @EndDate

  and 
  DiagnosisOrganisationCode = 'RM202'
  and HistologyCode is not NULL 
  and HistologyCode <> ''
  
  and r.CancerSite in(SELECT Val from dbo.fn_String_To_Table(@CancerSitePAR,',',1))

  
  ORDER BY Sn.SNOMEDDescription