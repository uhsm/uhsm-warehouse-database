﻿/****** Script for SelectTopNRows command from SSMS  ******/


/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		SCR Referrals - Interim report for Cancer services to help them identify the cancer referrals which are exported to SCR that should be flagged as SRFT referrals as a result of the Salford Breast Surgery project to all for updating on SCR
				Temporary solution until after the SCR upgrade when an automated solution will be developed

Notes:			Stored in WHReporting - data sourced from WH.SCR.PASReferrals view
Versions:		
				1.0.0.0 - 12/04/2016 - CM
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/


CREATE Procedure [CAN].[ReportRM3FlaggedSCRReferrals] @startdate datetime

AS



--Declare @startdate datetime = '20160401'


SELECT [NHSNumber]
      ,[DistrictNo]
      ,[PatientTitle]
      ,PatientName = [PatientForename] + ' ' + [PatientSurname]
      ,PatientAddress = isnull([PatientAddress1],'')+ ',' + isnull([PatientAddress2],'')+','+ isnull([PatientAddress3],'')+ ',' + isnull([PatientAddress4],'')+ ',' + isnull([Postcode],'')
      ,[SexCode]
      ,[DateOfBirth]
      ,[RegisteredGpCode]
      ,[RegisteredGpPracticeCode]
      ,[PCTCode]
      ,[DecisionDate]
      ,[ReceiptDate]
      ,[PriorityCode]
      ,[AppointmentDate]
      ,[AppointmentTime]
      ,[RTTPathwayID]
      ,[ConsultantCode]
      ,[MappedSpecialtyCode]
      ,[Specialty]
      ,[BreastNonCancer]
      ,[CancerTypeCode]      
      ,[PASUpdated]
      ,[PASCreated]
      ,[EthnicGroupCode]
      ,[DateOfDeath]
      ,PASReferralComment =[ReferralComment] 
      ,PASReferralAdditionalComment= [GeneralComment]
      ,CancellationDate
      ,DischargeDate
  FROM [WH].[SCR].[PASReferral]
  where NotOurActProvCode = 'RM3'
  and ReceiptDate >= @startdate
  Order by ReceiptDate