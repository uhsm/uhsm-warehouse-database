﻿CREATE procedure [CAN].[ReportStraightToTest] as

/**
Author: KO
Date: 16/12/2013
Purpose: Temporary SSRS report required to return details of all Straight To Test waiting lists
for Cancer Team.  
*/


 --@StartDate as date
 --,@EndDate as date
 --,@CancerTypePAR as nvarchar(100) 
select	
	DistrictNo = DistrictNo.IDENTIFIER
	,NHSNumber = Patient.PATNT_REFNO_NHS_IDENTIFIER
	,PatientForename = Patient.FORENAME
	,PatientSurname = Patient.SURNAME
	,ReferralPriority = ReferralPriority.DESCRIPTION
	,ReferralDate = convert(varchar(10), Referral.RECVD_DTTM, 103)
	,ReferralCancelDate = convert(varchar(10), Referral.CANCD_DTTM, 103)
	,WLPriority = Priority.DESCRIPTION
	,DateOnList = convert(varchar(10), WaitingList.WLIST_DTTM, 103)
	,Consultant = Carer.SURNAME + ', ' + Carer.FORENAME 
	,Specialty = Specialty.DESCRIPTION
	,ListName = WaitingListRule.Name
	,ListType = WaitingListRule.CODE
	,LocalCategory = LocalRule.Name
	,Removed = case
			when WaitingList.REMVL_DTTM is null
			then 'N'
			else 'Y'
		end
	,RemovalDate = convert(varchar(10), WaitingList.REMVL_DTTM, 103)
	,RemovalReason = RemovalReason.[DESCRIPTION]
	,GeneralComment = ltrim(rtrim(replace(replace(NotesGeneral.NOTES_REFNO_NOTE,'[PROCEDURE:',''),']','')))
	,DateRecordCreated = convert(varchar(10), WaitingList.CREATE_DTTM, 103)
	,CreatedByUser = --cast(null as varchar(25))
		case 
			when CreatedUsers.[user_name] is null then WaitingList.USER_CREATE
			Else CreatedUsers.[User_name]
		end
		
	,DateRecordModified = convert(varchar(10), WaitingList.MODIF_DTTM, 103)
	,ModifiedByUser = --cast(null as varchar(25))
		case 
			when ModifiedUsers.[user_name] is null then WaitingList.USER_MODIF
			Else ModifiedUsers.[User_name]
		end
     ,DataCount = 1
     ,WaitingList.WLIST_REFNO
     ,WaitingList.REFRL_REFNO
from 	Lorenzo.dbo.WaitingList WaitingList  

left join Lorenzo.dbo.Specialty Specialty 
	on Specialty.spect_refno = WaitingList.SPECT_REFNO 
	and Specialty.ARCHV_FLAG = 'N'
	
left join Lorenzo.dbo.ProfessionalCarer Carer 
	on Carer.PROCA_REFNO = WaitingList.PROCA_REFNO 
	
left join Lorenzo.dbo.Patient Patient 
	on Patient.patnt_refno = WaitingList.PATNT_REFNO

left join Lorenzo.dbo.ReferenceValue Priority
	on Priority.RFVAL_REFNO = WaitingList.PRITY_REFNO

left join Lorenzo.dbo.ReferenceValue RemovalReason
	on RemovalReason.RFVAL_REFNO = WaitingList.REMVL_REFNO
	and RemovalReason.ARCHV_FLAG = 'N'

left join Lorenzo.dbo.NoteRole NotesGeneral 
	on NotesGeneral.SORCE_REFNO = WaitingList.WLIST_REFNO
	and NotesGeneral.SORCE_CODE = 'WLCMT' 
	and NotesGeneral.archv_flag = 'N'
	
left join Lorenzo.dbo.WaitingListRule WaitingListRule 
	on WaitingListRule.WLRUL_REFNO = WaitingList.WLRUL_REFNO
	and WaitingListRule.archv_flag = 'N'

left join Lorenzo.dbo.WaitingListRule LocalRule 
	on LocalRule.WLRUL_REFNO = WaitingList.LOCAL_WLRUL_REFNO
	and LocalRule.archv_flag = 'N'
	
--left join WH.PTL.IPWLWaitingStatus WaitingStatus 
--	on WaitingStatus.SourceUniqueID = WaitingList.WLIST_REFNO
	
left join Lorenzo.dbo.Referral Referral 
	on Referral.refrl_refno = WaitingList.REFRL_REFNO
	and Referral.archv_flag = 'N'
	
left join Lorenzo.dbo.ReferenceValue ReferralPriority
	on ReferralPriority.RFVAL_REFNO = Referral.PRITY_REFNO
	
left join Lorenzo.dbo.[User] CreatedUsers 
	on CreatedUsers.Code = WaitingList.USER_CREATE
	and CreatedUsers.archv_flag = 'N'
	
left join Lorenzo.dbo.[User] ModifiedUsers 
	on ModifiedUsers.Code = WaitingList.USER_MODIF
	and ModifiedUsers.archv_flag = 'N'
	
left join Lorenzo.dbo.PatientIdentifier DistrictNo
on	DistrictNo.PATNT_REFNO = WaitingList.PATNT_REFNO
and	DistrictNo.PITYP_REFNO = 2001232 --facility
and	DistrictNo.ARCHV_FLAG = 'N'
and	DistrictNo.IDENTIFIER like 'RM2%'
and	not exists
	(
	select
		1
	from
		Lorenzo.dbo.PatientIdentifier Previous
	where
		Previous.PATNT_REFNO = DistrictNo.PATNT_REFNO
	and	Previous.PITYP_REFNO = DistrictNo.PITYP_REFNO
	and	Previous.ARCHV_FLAG = 'N'
	and	Previous.IDENTIFIER like 'RM2%'
	and	(
			Previous.START_DTTM > DistrictNo.START_DTTM
		or
			(
				Previous.START_DTTM = DistrictNo.START_DTTM
			and	Previous.PATID_REFNO > DistrictNo.PATID_REFNO
			)
		)
	)
	

where WaitingList.PRITY_REFNO = 5478 --2 Week Rule
and WaitingList.LOCAL_WLRUL_REFNO = 150001520	
and WaitingList.ARCHV_FLAG = 'N'
order by WaitingList.WLIST_DTTM desc