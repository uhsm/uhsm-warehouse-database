﻿/*
--Author: Helen Ransome
--Date created: 18/12/2014
--Stored Procedure Built for SSRS Report on:
--Cancer Teenage Young Adult report
*/

CREATE Procedure [CAN].[ReportTYA]


 @StartDate as date 
 ,@EndDate as date
 ,@CancerSite as varchar
 

as

Select
--D.Forename 
D.Surname 
--,D.HospitalNumber 
,D.NHSNumber 
,D.DateOfBirth 
,GETDATE () AS [Today]
--,DATEDIFF (yy,D.DateofBirth,GETDATE()) 
,DATEDIFF (YY,d.dateofbirth,GETDATE())-
Case
When DATEADD (yy,DATEDIFF (yy,d.dateofbirth,GETDATE ()),d.dateofbirth)
>GETDATE () then 1
Else 0
End as Age
--,DATEDIFF (yy,CONVERT(DATETIME,D.DateofBirth),GETDATE()) as AGE, D.DateOfBirth
,con.Consultant 
,CancerSite 
,c.CancerStatus 
,DiagnosisDate 
,PrimaryDiagnosisCode 
,track.TrackingComment 
--,T.TumorStatus 

From [WH].SCR.Referral as R


left join WH.SCR.Demographic as D
on R.DemographicUniqueRecordId = D.UniqueRecordId 

   
left join WH.SCR.TumorStatus as T
on R.TumourStatusCode = T.TumorStatusCode 

left join WH.SCR.CancerStatus as c
on R.CancerStatusCode = c.CancerStatusCode 

left join WH.SCR.Consultant as con
on R.ConsultantCode = con.NationalConsultantCode   

left join WH.SCR.TrackingComment as track
on R.UniqueRecordId = track.ReferralUniqueRecordId  

and not exists
(
      select 1 from WH.SCR.TrackingComment PreviousComment
      where Track.ReferralUniqueRecordId  = PreviousComment.ReferralUniqueRecordId 
      and  Track.UniqueRecordId < PreviousComment.UniqueRecordId 
)

Where

R.DiagnosisDate not like 'NULL'
and R.DiagnosisDate between @StartDate and @EndDate 
and TumorStatus not like 'Benign'


and (DATEDIFF (YY,d.dateofbirth,GETDATE())-
Case
When DATEADD (yy,DATEDIFF (yy,d.dateofbirth,GETDATE ()),d.dateofbirth)
>GETDATE () then 1
Else 0
End) <= 24
and R.CancerSite in (@CancerSite) 
  
  ORDER BY DiagnosisDate DESC