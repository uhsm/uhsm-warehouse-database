﻿CREATE TABLE [CAN].[InterfaceAudit](
	[SourceUniqueID] [int] NOT NULL,
	[NHSNumber] [varchar](20) NULL,
	[DistrictNo] [varchar](20) NULL,
	[CancerTypeCode] [varchar](25) NULL,
	[CancerType] [varchar](80) NULL,
	[PriorityValue] [varchar](80) NULL,
	[DecisionDate] [datetime] NULL,
	[ReceiptDate] [datetime] NULL,
	[CancellationDate] [smalldatetime] NULL,
	[SameDayCancellation] [int] NOT NULL,
	[CreatedOnPAS] [datetime] NULL,
	[UpdatedOnPAS] [datetime] NULL,
	[CreatedInDW] [datetime] NULL,
	[UpdatedInDW] [datetime] NULL,
	[SCRInterfaceProcessTime] [datetime] NULL,
	[SCRReferralUniqueRecordId] [int] NULL
) ON [PRIMARY]