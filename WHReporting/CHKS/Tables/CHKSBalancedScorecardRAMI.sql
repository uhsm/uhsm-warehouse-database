﻿CREATE TABLE [CHKS].[CHKSBalancedScorecardRAMI](
	[UniqueID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityMonth] [datetime] NOT NULL,
	[AdmType] [varchar](50) NULL,
	[DeathFlag] [varchar](10) NULL,
	[DischTreatmentFunc] [varchar](100) NULL,
	[PbRTreatmentFunc] [varchar](100) NULL,
	[RAMI2013IncFlag] [varchar](10) NULL,
	[WellBabyFlag] [varchar](10) NULL,
	[RAMI2013Total] [decimal](15, 9) NULL,
	[SpellTotal] [int] NULL
) ON [PRIMARY]