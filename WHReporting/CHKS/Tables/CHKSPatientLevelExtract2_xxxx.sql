﻿CREATE TABLE [CHKS].[CHKSPatientLevelExtract2_xxxx](
	[Month] [varchar](50) NULL,
	[Financial Year] [varchar](50) NULL,
	[Quarter] [varchar](50) NULL,
	[Adm Group] [varchar](50) NULL,
	[Adm Method] [varchar](50) NULL,
	[Adm Source] [varchar](50) NULL,
	[Adm Type] [varchar](50) NULL,
	[Adm CHKS SubSpec] [varchar](50) NULL,
	[Adm Con] [varchar](50) NULL,
	[Adm HRG35] [varchar](50) NULL,
	[Adm HRG v4] [varchar](50) NULL,
	[Adm Spec] [varchar](50) NULL,
	[Adm Local SubSpec] [varchar](50) NULL,
	[Admit PBC] [varchar](50) NULL,
	[Adm Treatment Func] [varchar](50) NULL,
	[Age] [varchar](50) NULL,
	[Age Group] [varchar](50) NULL,
	[CCG] [varchar](50) NULL,
	[CCS Group] [varchar](50) NULL,
	[Charlson Score] [varchar](50) NULL,
	[Day Case] [varchar](50) NULL,
	[Day of Admission (Spell)] [varchar](50) NULL,
	[Day of Discharge (Spell)] [varchar](50) NULL,
	[Death] [varchar](50) NULL,
	[Diags 2-15] [varchar](50) NULL,
	[Diag1 (3 chars)] [varchar](50) NULL,
	[Diag1 (5 chars)] [varchar](50) NULL,
	[Diag2 (3 chars)] [varchar](50) NULL,
	[Diag2 (5 chars)] [varchar](50) NULL,
	[Disch Dest] [varchar](50) NULL,
	[Disch Method] [varchar](50) NULL,
	[Disch CHKS SubSpec] [varchar](50) NULL,
	[Disch Con] [varchar](50) NULL,
	[Disch HRG35] [varchar](50) NULL,
	[Disch HRG v4] [varchar](50) NULL,
	[Disch Spec] [varchar](50) NULL,
	[Disch Local SubSpec] [varchar](50) NULL,
	[Disch PBC] [varchar](50) NULL,
	[Disch Treatment Func] [varchar](50) NULL,
	[Ethnic Origin] [varchar](50) NULL,
	[GP Practice] [varchar](50) NULL,
	[HRGv4 Excess Days] [varchar](50) NULL,
	[Intend Mgt] [varchar](50) NULL,
	[Operative Proc (Spell)] [varchar](50) NULL,
	[Patient ID] [varchar](50) NULL,
	[PbR (HRG35) CHKS SubSpec] [varchar](50) NULL,
	[Consultant] [varchar](50) NULL,
	[PbR (HRG35) HRG35] [varchar](50) NULL,
	[PbR (HRG35) HRG4] [varchar](50) NULL,
	[PbR (HRG35) Spec] [varchar](50) NULL,
	[PbR (HRG35) Local SubSpec] [varchar](50) NULL,
	[PbR (HRG3 5) PBC] [varchar](50) NULL,
	[PbR (HRG35) Treatment Func] [varchar](50) NULL,
	[PCT] [varchar](50) NULL,
	[Post-Op LoS (Spell)] [varchar](50) NULL,
	[PreOp LoS] [varchar](50) NULL,
	[Proc1 (3 Chars)] [varchar](50) NULL,
	[Proc1 (4 Chars)] [varchar](50) NULL,
	[Procs 2-15] [varchar](50) NULL,
	[Prov ID] [varchar](50) NULL,
	[ProvSpell End Date] [varchar](50) NULL,
	[ProvSpell Start Date] [varchar](50) NULL,
	[RALI 2012 inc] [varchar](50) NULL,
	[RALI 2012] [varchar](50) NULL,
	[RALI 2013 inc] [varchar](50) NULL,
	[RALI 2013] [varchar](50) NULL,
	[RAMI 2011 inc] [varchar](50) NULL,
	[RAMI 2011] [varchar](50) NULL,
	[RAMI 2012 inc] [varchar](50) NULL,
	[RAMI 2012] [varchar](50) NULL,
	[RAMI 2013 inc] [varchar](50) NULL,
	[RAMI 2013] [varchar](50) NULL,
	[Referring GP] [varchar](50) NULL,
	[Regular Attender] [varchar](50) NULL,
	[Renal Dialysis Admission] [varchar](50) NULL,
	[Sex] [varchar](50) NULL,
	[SHMI 2012 inc] [varchar](50) NULL,
	[SHMI 2012] [varchar](50) NULL,
	[SHMI 2013 inc] [varchar](50) NULL,
	[SHMI 2013] [varchar](50) NULL,
	[SHMI Group] [varchar](50) NULL,
	[Site ID] [varchar](50) NULL,
	[Spell LoS] [varchar](50) NULL,
	[Spell Zero LoS] [varchar](50) NULL,
	[Tariff Excluding MFF] [varchar](50) NULL,
	[Tariff Including MFF] [varchar](50) NULL,
	[Well Baby] [varchar](50) NULL
) ON [PRIMARY]