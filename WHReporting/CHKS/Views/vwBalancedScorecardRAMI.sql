﻿CREATE VIEW [CHKS].[vwBalancedScorecardRAMI]

as

SELECT 
  MeasureID
 ,Period
 ,LookupKey
 ,Deaths = ActualDeaths
FROM WHREPORTING.CHKS.vwBalancedScorecardRAMI_N

UNION

SELECT 
  MeasureID
 ,Period
 ,LookupKey
 ,Deaths = ExpectedDeaths
FROM WHREPORTING.CHKS.vwBalancedScorecardRAMI_D