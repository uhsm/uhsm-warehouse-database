﻿CREATE VIEW [CHKS].[vwBalancedScorecardRAMI_D]

as

SELECT
  MeasureID = 'RAMI_D_' + Alias.ScorecardAlias
 ,Period = DATENAME(YEAR,RollingPeriod.PeriodEndDate) + ' ' + DATENAME(MONTH,RollingPeriod.PeriodEndDate)
 ,LookupKey = 'RAMI_D_' + Alias.ScorecardAlias + DATENAME(YEAR,RollingPeriod.PeriodEndDate) + ' ' + DATENAME(MONTH,RollingPeriod.PeriodEndDate)
 ,ExpectedDeaths = SUM(Data.RAMI2013Total)
FROM (
      SELECT DISTINCT 
        PeriodEndDate = Period.ActivityMonth
       ,PeriodStartDate = DATEADD(MONTH,-11,Period.ActivityMonth)
      FROM WHREPORTING.CHKS.CHKSBalancedScorecardRAMI Period
      WHERE Period.ActivityMonth >= '20130401'
     ) RollingPeriod
LEFT JOIN WHREPORTING.CHKS.CHKSBalancedScorecardRAMI Data
  ON (RollingPeriod.PeriodStartDate <= Data.ActivityMonth
      AND RollingPeriod.PeriodEndDate >= Data.ActivityMonth)
LEFT JOIN WHREPORTING.CHKS.CHKSBalancedScorecardAliasLkp Alias
  ON (Data.PbRTreatmentFunc = Alias.PbRTreatmentFunc)
WHERE LEN(Alias.ScorecardAlias) > 0
GROUP BY Alias.ScorecardAlias
        ,RollingPeriod.PeriodEndDate;