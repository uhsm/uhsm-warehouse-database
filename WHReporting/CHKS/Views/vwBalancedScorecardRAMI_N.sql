﻿CREATE VIEW [CHKS].[vwBalancedScorecardRAMI_N]

as

SELECT
  MeasureID = 'RAMI_N_' + Alias.ScorecardAlias
 ,Period = DATENAME(YEAR,RollingPeriod.PeriodEndDate) + ' ' + DATENAME(MONTH,RollingPeriod.PeriodEndDate)
 ,LookupKey = 'RAMI_N_' + Alias.ScorecardAlias + DATENAME(YEAR,RollingPeriod.PeriodEndDate) + ' ' + DATENAME(MONTH,RollingPeriod.PeriodEndDate)

 ,ActualDeaths = SUM(CASE Data.DeathFlag 
                       WHEN 'Yes' THEN Data.SpellTotal
                       ELSE 0
                     END)
FROM (
      SELECT DISTINCT 
        PeriodEndDate = Period.ActivityMonth
       ,PeriodStartDate = DATEADD(MONTH,-11,Period.ActivityMonth)
      FROM WHREPORTING.CHKS.CHKSBalancedScorecardRAMI Period
      WHERE Period.ActivityMonth >= '20130401'
     ) RollingPeriod
LEFT JOIN WHREPORTING.CHKS.CHKSBalancedScorecardRAMI Data
  ON (RollingPeriod.PeriodStartDate <= Data.ActivityMonth
      AND RollingPeriod.PeriodEndDate >= Data.ActivityMonth)
LEFT JOIN WHREPORTING.CHKS.CHKSBalancedScorecardAliasLkp Alias
  ON (Data.PbRTreatmentFunc = Alias.PbRTreatmentFunc)
WHERE LEN(Alias.ScorecardAlias) > 0
      AND Data.RAMI2013IncFlag = 'Yes'
GROUP BY Alias.ScorecardAlias
        ,RollingPeriod.PeriodEndDate;