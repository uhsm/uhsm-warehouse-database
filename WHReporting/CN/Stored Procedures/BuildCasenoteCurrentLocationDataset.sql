﻿-- =============================================
-- Author:	CMan
-- Create date: 13/03/2014
-- Description:	Stored Procedure to build a CasenuteCurrentLocationDataset table to replicate the CaseNote_Locations_Dataset on InfoSQL.
--				Some of the logic has been taken from the stored procedure on INFOSQL - INFORMATION_VER2.dbo.uspGetLatestCaseNoteInformation
--				Logic within the above stored procedure on INFOSQL relating to RIGHT(IDENTIFIER,2) = '01' and a.serial = 1  have been excluded from this new stored procedure as it looks like it may have resulted in some records being excluded 
--				Logic relating to LEFT(CASVL_PASID,3) = 'RM2' is also excluded from this logic as this excludes all casenote volumes which are not preceded with RM2 - e.g casenote volumes which have an identifier like '1/HN/AK0093618' but may still have document id on front end that is RM2 - for example patnt_refno 10459312.  Not sure is this is what the logic should be
--				Also, the logic in INFOSQL gets the notes comments from the Lorenzo NOTEROLE table, but it looks the same information could come from the comments field in the Lorenzo.dbo.CasenoteActivity table so have replaced it with this.
--
-- =============================================
CREATE PROCEDURE [CN].[BuildCasenoteCurrentLocationDataset]
	-- Add the parameters for the stored procedure here




AS
/*
Created Modified Date		Modified By							Comments
---------------------		------------------------		------------------
Modified 20/03/2014			CMan							A Fairclough requested that for patients who have a volume of casenotes with RM2 in the identifier, this is the colume they wish to know about.
															Otherwise if patient does not have a volume of casenotes with RM2 number in then bring through the BB or AE etc notes - only when patient has no RM2 notes.
															Applied this in the logic which builds the #max_cas_vl temp table below



*/




BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @RowsInserted Int

select @StartTime = getdate()




--create temp table using max to get the last transfer activity for each casenote volume per patient
IF Object_id ('tempdb..#max_mov') > 0
  DROP TABLE #max_mov

CREATE TABLE #max_mov
  (
     Patnt_refno INT,
     Casvl_refno INT,
     max_casac   INT
  ) 


Insert into #max_mov
SELECT PATNT_REFNO,
       CASVL_REFNO,
       Max(CASAC_REFNO) AS MAX_CASAC
FROM   Lorenzo.dbo.CasenoteActivity
WHERE  ARCHV_FLAG = 'N'
--and PATNT_REFNO = '10459312'
GROUP  BY CASVL_REFNO,
          PATNT_REFNO 
;


--create index on the above temp table to help it run faster in the join when creating the table below
CREATE INDEX ix_mov_casac_rf
ON #max_mov (max_casac)
;

--temp table joining the #max_mov table back to the Lorenzo.dbo.CasenoteActivity to get the details of that latest/max  action/movement per casenote volume - - to be used in final query to create dataset table further below.
IF Object_id ('tempdb..#cn_activity') > 0
  DROP TABLE #cn_activity

CREATE TABLE #cn_activity
  (
     Patnt_refno INT,
     Casvl_refno INT,
     Comments    VARCHAR(400),
     Casac_Refno INT,
     RQSTA_Refno INT,
     REQBY_DTTM DateTime,
     RQSTA_DTTM DateTime
  ) 


Insert into #cn_activity
SELECT cn.PATNT_REFNO,
       cn.CASVL_REFNO,
       cn.COMMENTS,
       cn.CASAC_REFNO,
       cn.RQSTA_REFNO,
       cn.REQBY_DTTM,
       cn.RQSTA_DTTM   
FROM   Lorenzo.dbo.CasenoteActivity cn 
       INNER JOIN #max_mov
               ON cn.PATNT_REFNO = #max_mov.PATNT_REFNO
                  AND cn.CASVL_REFNO = #max_mov.CASVL_REFNO
                  AND cn.CASAC_REFNO = #max_mov.MAX_CASAC
WHERE  cn.ARCHV_FLAG = 'N'
--and cn.PATNT_REFNO = '10459312'
GROUP  BY cn.PATNT_REFNO,
          cn.CASVL_REFNO,
          cn.END_DTTM,
          cn.COMMENTS,
          cn.CASAC_REFNO,
          cn.RQSTA_REFNO,
          cn.REQBY_DTTM,
		   cn.RQSTA_DTTM   

;








--create temp table for latest/max casenote volume refno as for this dataset we are only interested in finding out the last activity of the latest(active) volume of the patient's casenotes 
--this appears to be what the dataset on infosql was trying to show.
--Group on Patnt_refno to get the max volume for the patient as there are instances where the latest volume does not relate to the latest casnt_refno. e.g patnt_refno, latest casenote volume (casvl_refno) = 150517937, but this relates to the casenote number (casnt_refno) '10298012'
--Therefore, use this to get the latest casenote volume for each patient. 
IF Object_id ('tempdb..#max_cas_vl') > 0
  DROP TABLE #max_cas_vl

CREATE TABLE #max_cas_vl
  (
     patnt_refno     INT,
     --Casnt_Refno int,
     max_casvl_refno INT,
  ) 

--Insert into the temp table the latest volume of RM2 numbers for patients with RM2 numbers
Insert into #max_cas_vl
SELECT PATNT_REFNO,
       --casnt_refno,
       Max(CASVL_REFNO) AS max_casvl_refno --to get the latest volume refno for the patient
FROM   Lorenzo.dbo.CasenoteVolume
WHERE   ARCHV_FLAG = 'N'
AND END_DTTM is NULL
AND LEFT(CASVL_PASID,3) = 'RM2'	
--and PATNT_REFNO  in ( '10650031', '11899420')
GROUP  BY PATNT_REFNO

;

--Insert into the temp table the latest volume for patients who do not have an RM2 volume
--Requested by Angela Fairclough - for patients's who have an volume of casenotes with an RM2 number this is the volume they want to know about, otherwise bring through the identifier of the latest 
--volume of casenotes where the colume identifier does not have RM2
Insert into #max_cas_vl
SELECT PATNT_REFNO,
       --casnt_refno,
       Max(CASVL_REFNO) AS max_casvl_refno --to get the latest volume refno for the patient
FROM   Lorenzo.dbo.CasenoteVolume
WHERE   ARCHV_FLAG = 'N'
AND END_DTTM is NULL
--and PATNT_REFNO  in ( '10650031', '11899420')
and PATNT_REFNO not in (Select PATNT_REFNO from #max_cas_vl)
GROUP  BY PATNT_REFNO 






--Create temp table joining the  #max_cas_vl  temp table above back to  Lorenzo.dbo.CasenoteVolume to get the details of the latest volume - to be used in final query to create dataset table below.
IF Object_id ('tempdb..#cas_vl') > 0
  DROP TABLE #cas_vl

CREATE TABLE #cas_vl
  (
     Patnt_refno       INT,
     Casnt_Refno       INT,
     Casvl_refno       INT,
     Curnt_Spont_refno INT,
     Identifier        VARCHAR(100)
  ) 

INSERT INTO #cas_vl
SELECT Casvl.PATNT_REFNO,
       CasVl.CASNT_REFNO,
       CasVl.CASVL_REFNO,
       CasVl.CURNT_SPONT_REFNO,
       CasVl.IDENTIFIER
FROM   Lorenzo.dbo.CasenoteVolume CasVl
       INNER JOIN #max_cas_vl maxcvl
               ON CasVl.PATNT_REFNO = maxcvl.PATNT_REFNO
                  AND CasVl.CASVL_REFNO = maxcvl.MAX_CASVL_REFNO 
;
/*this section was used for testing

Select * from #max_mov
where Patnt_refno  = '10459312'--'150333077'--'150333040'
Select * from #cn_activity
where Patnt_refno  = '10459312'--'150333077'--'150333040'
Select * from #max_cas_vl
where Patnt_refno  = '10474374'--'150333077'--'150333040'
Select * from #cas_vl
where Patnt_refno  = '10474374'--'150333077'--'150333040'
Select *
from Lorenzo.dbo.Casenote
where PATNT_REFNO = '10474374'

select *
FROM   Lorenzo.dbo.Casenote cn
       INNER JOIN (SELECT PATNT_REFNO,
                          Max(CASNT_REFNO) max_casnt_refno
                   FROM   Lorenzo.dbo.Casenote
                   WHERE  ARCHV_FLAG = 'N'
                          AND PDTYP_REFNO = 3444
                        AND PATNT_REFNO = '10474374'
                   GROUP  BY PATNT_REFNO) max_cn --to get the latest casenote number for the patient
               ON cn.PATNT_REFNO = max_cn.PATNT_REFNO
                  AND cn.CASNT_REFNO = max_cn.max_casnt_refno
SElect *
from Lorenzo.dbo.CasenoteVolume
where PATNT_REFNO = '10474374'

--Select *
--from #cas_vl
--where Patnt_refno  = '10474374'--'150333077'--'150333040'
--Select * from #max_mov
--where Patnt_refno  = '150333040'
Select * from #cn_activity
where Patnt_refno  = '150333040'
--Select * from #max_cas_vl
--where Patnt_refno  = '150333040'

--Select *
--from #cas_vl
--where Patnt_refno  = '150333040'



--create temp table of casenote inner joining to casenote columes table to get rid of any casenotes which do not have an associated columne e.g. patnt '10474374' and casnt_refno '150231224'  which is the max casnote but has no volumes
Select *
from Lorenzo.dbo.Casenote  casent
inner join #cas_vl casvl on casent.PATNT_REFNO = casvl.Patnt_refno and casent.CASNT_REFNO = casvl.Casnt_Refno
--where casent.Patnt_refno = '10474374'
*/



--Query to insert into Casenote_Locations_Dataset

TRUNCATE TABLE WHReporting.CN.CasenoteCurrentLocationDataset;

INSERT INTO WHReporting.CN.CasenoteCurrentLocationDataset
SELECT cn.PATNT_REFNO,
       cn.IDENTIFIER,
       cn.CASVL_COUNT,
       sp.DESCRIPTION as  Location,
       cna.COMMENTS,
       --nr.NOTES_REFNO_NOTE,
       CASE
         WHEN cna.RQSTA_REFNO = 1426 THEN 'Y'
         ELSE 'N'
       END            AS Received,
       cv.IDENTIFIER,
       sp.CODE as LocationCode,
       cna.REQBY_DTTM  as RequestByDate,--CM 26/11/2015 added these additional fields to allow Gordon F to carry out request for new report
       cna.RQSTA_DTTM as RequestStatusDate,  --CM 26/11/2015 added these additional fields to allow Gordon F to carry out request for new report 
       GETDATE() as Datestamp --add date the table was last created/updated - can be used in SSRS report to inform users when the table was last updated.
       
FROM   (SELECT *
        FROM   Lorenzo.dbo.Casenote
        WHERE  ARCHV_FLAG = 'N'
               AND PDTYP_REFNO = 3444 -- this is logic which was in the infosql build
       ) cn
       --join to #cas_vl created above to get the details of the latest casenote volume details for the patient and casenote_refno so that the curnt_location can be used to find latest location of that latest volume in the join further below   
       --inner join is used as the  #cas_vl table is the one which holds the casent_refno of the latest volume  so we only want to get these ones  
       INNER JOIN #cas_vl cv
               ON cn.PATNT_REFNO = cv.PATNT_REFNO
                  AND cn.CASNT_REFNO = cv.CASNT_REFNO --ensure that the patnt_refno is used in the join for patient volume, as there are instances where the same casvl_refno applies to more than one patnt_refno e.g. casvl_refno '150358401' appears aganist patnt_refno '150333040' and '150333077'
       --join below is to find the last action and notes/comments  for the volume identified in the temp table #cn_activity created earlier - this appears to give the exact location of the notes e.g. drawer/shelf/bay etc.
       LEFT OUTER JOIN #cn_activity AS cna
                    ON cv.PATNT_REFNO = cna.PATNT_REFNO
                       AND cv.CASVL_REFNO = cna.CASVL_REFNO --ensure that the patnt_refno is used in the join for patient volume, as there are instances where the same casvl_refno applies to more than one patnt_refno e.g. casvl_refno '150358401' appears aganist patnt_refno '150333040' and '150333077'
       LEFT OUTER JOIN Lorenzo.dbo.ServicePoint sp --join back to Lorenzo.dbo.ServicePoint to get the description of the last location
                    ON cv.CURNT_SPONT_REFNO = sp.SPONT_REFNO
WHERE  cn.ARCHV_FLAG = 'N' 
;



select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) +
	'. Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WHREPORTING CN.BuildCasenoteCurrentLocationDataset', @Stats, @StartTime



--House keeping
DROP INDEX ix_mov_casac_rf on #max_mov;
DROP TABLE #max_mov;
DROP TABLE #cn_activity;
DROP TABLE #cas_vl;
DROP TABLE #max_cas_vl;






END