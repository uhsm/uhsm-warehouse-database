﻿/*
--Author: J Cleworth
--Date created: 21/08/2013

*/

CREATE Procedure [COM].[BuildDataComplianceOld]
AS 
DROP TABLE com.DataCompliance

select scheduleid,
r.ReferralID

/* Failure Indicator 1
 Referral/Activity Specialty Mismatch */

/*,case when r.ReferredToSpecialty<>s.AppointmentSpecialty 
and ActivityTypeMain='Community Contact' 
then 1 else 0 end as Indicator1*/


,case when ActivityTypeMain='Community Contact'and s.ScheduleID is not null and ss.Specialty1 IS NULL -- exclude combinations of PARS/PACE, PARS/ALFA, PARS/SMILE etc
	AND
		s.AppointmentSpecialty is not null
	AND
		(
				(
						ns.[ID] IS NOT NULL -- part of new/old specialty combination
					AND
						(
								r.ReferralCreatedDate > rsv.enddate
							OR
								s.AppointmentDate > csv.enddate
						)
				)
			OR
				(
						ns.[ID] IS NULL -- not part of new/old specialty combination
					AND
						s.AppointmentSpecialty <> r.ReferredToSpecialty
				)
		)
then 1 else 0 end as Indicator1

/* Failure Indicator 2
 Unrecorded or Incorrect Specialty */

/*,case when s.AppointmentSpecialty is null 
and s.ActivityTypeMain='Community Contact' 
then 1 else 0 end as Indicator2*/

,case when ActivityTypeMain='Community Contact' and s.ScheduleID is not null and ssInd2.Specialty1 IS NULL -- exclude combinations of PARS/PACE, PARS/ALFA, PARS/SMILE etc
	AND
		s.AppointmentSpecialty is not null
	AND
		(
				(
						ns.[ID] IS NOT NULL -- part of new/old specialty combination...
											-- ...but outside the mapping period
					AND
						(
								r.ReferralCreatedDate > rsv.EndDate
							OR
								s.AppointmentDate > csv.EndDate
						)
				)
		)

			OR
				s.AppointmentSpecialty is null --no specialty recorded for the schedule
				
Then 1 else 0 end as Indicator2


/* Failure Indicator 3
 Clinic Contact: Unrecorded Attend Status*/

,case when s.attendNationalCode='NSP' 
and s.ScheduleID is not null
and s.CancellationDate is null 
and ActivityTypeMain='Outpatient Schedule' 
then 1 else 0 end as Indicator3

/*Failure Indicator 4
Unrecorded Seen By Professional Carer*/

,case when SeenByProfessionalCarer is null 
and s.ScheduleID is not null
and s.CancellationDate is null
and ActivityTypeMain='Outpatient Schedule'
and AttendNationalCode not in ('DNA','7','LNS','RW','UTA')
 then 1 else 0 end as Indicator4

/*Failure Indicator 5
 Contact Not Actualised*/

,case when ActivityTypeMain='Community Contact' 
and s.ScheduleID is not null
and AppointmentArrivalDateTime is null
and CancellationDate is null
and Outcome not in ('0','DIED','CANCD')
then 1 else 0 end as Indicator5

/*Failure Indicator 6
 Unrecorded Contact Type*/

,case when CareContactSubjectCode is null 
and s.ScheduleID is not null
and ActivityTypeMain='Community Contact' 
and Outcome not in ('0','DIED','CANCD')
and CancellationDate is null 
then 1 else 0 end  as Indicator6

/*Failure Indicator 7
-Unrecorded Referral Status*/

,case when ReferralStatus='NSP' then 1 else 0 end as Indicator7

,Case when ActivityTypeMain='Community Contact' then 1 else 0 end as Contacts

,Case when ActivityTypeMain='Outpatient Schedule' then 1 else 0 end as OutpatientAppointments

,1 as Appointments

,case when AttendedOrDidNotAttendCode=5 then 1 else 0 end as Attendances

into COM.DataCompliance

from com.Referrals r left outer join COM.Schedules s on 
s.ReferralID=r.ReferralID
left join LK.Calendar c on s.AppointmentDate=c.TheDate
LEFT JOIN [COM].lkServices csv
	ON s.AppointmentSpecialty = csv.name
LEFT JOIN [COM].lkServices rsv
	ON r.ReferredToSpecialty = rsv.name
	
LEFT JOIN [COM].lkSameSpecialties ss
	ON
			(
					(r.ReferredToSpecialty = ss.Specialty1)
				OR
					(r.ReferredToSpecialty = ss.Specialty2)
			)
		AND
			(
					(s.AppointmentSpecialty = ss.Specialty1)
				OR
					(s.AppointmentSpecialty = ss.Specialty2)
			)
LEFT JOIN [COM].lkNewSpecialties ns
	ON
			(
					r.ReferredToSpecialty = ns.OldDescription
				AND
					r.ReferredToSpecialty = ns.NewDescription
			)
		OR
			(
					s.AppointmentSpecialty = ns.OldDescription
				AND	
					s.AppointmentSpecialty = ns.NewDescription
			)
			
LEFT JOIN [COM].lkSameSpecialties ssInd2
	ON
			(
					(s.AppointmentSpecialty = ssInd2.Specialty1)
				OR
					(s.AppointmentSpecialty = ssInd2.Specialty2)
			)
LEFT JOIN [COM].lkNewSpecialties nsInd2
	ON
			
			(
					s.AppointmentSpecialty = nsInd2.OldDescription
				or	
					s.AppointmentSpecialty = nsInd2.NewDescription
			)
/*where c.FinancialMonthKey between @MonthFrom and @MonthTo
and professionalCarer is not null*/