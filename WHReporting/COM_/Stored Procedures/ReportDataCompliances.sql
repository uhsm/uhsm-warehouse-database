﻿create PROCEDURE [COM].[ReportDataCompliances] 

@MonthFrom as int,
@MonthTo as int,
@Services as int,
@Carers as int


as

SELECT
	Directorate,
	ServiceName,
	Carer,
	SUM(TotalActivity) as 'TotalActivity',
	SUM(TotalFailed) as 'TotalFailed',
	SUM(Indicator1Total) as 'Indicator1Total',
	
	CAST(
	CAST(SUM(Indicator1Numerator) AS DECIMAL(7,2)) 
	    / CAST(SUM(Indicator1Denominator) AS DECIMAL(7,2)) * 100
	AS DECIMAL(5,2)) AS 'Indicator1Percentage',
	
	SUM(Indicator1Numerator) as 'Indicator1Numerator',
	SUM(Indicator1Denominator) as 'Indicator1Denominator',
	
	SUM(Indicator2Total) as 'Indicator2Total',
	
	CAST(
	CAST(SUM(Indicator2Numerator) AS DECIMAL(7,2)) 
	    / CAST(SUM(Indicator2Denominator) AS DECIMAL(7,2)) * 100
	AS DECIMAL(5,2)) AS 'Indicator2Percentage',
	
	SUM(Indicator2Numerator) as 'Indicator2Numerator',
	SUM(Indicator2Denominator) as 'Indicator2Denominator',
	
	SUM(Indicator3Total) as 'Indicator3Total',
	
	CAST(
	CAST(SUM(Indicator3Numerator) AS DECIMAL(7,2)) 
	    / CAST(SUM(Indicator3Denominator) AS DECIMAL(7,2)) * 100
	AS DECIMAL(5,2)) AS 'Indicator3Percentage',
	
	SUM(Indicator3Numerator) as 'Indicator3Numerator',
	SUM(Indicator3Denominator) as 'Indicator3Denominator',
	
	SUM(Indicator4Total) as 'Indicator4Total',
	
	CAST(
	CAST(SUM(Indicator4Numerator) AS DECIMAL(7,2)) 
	    / CAST(SUM(Indicator4Denominator) AS DECIMAL(7,2)) * 100
	AS DECIMAL(5,2)) AS 'Indicator4Percentage',
	
	SUM(Indicator4Numerator) as 'Indicator4Numerator',
	SUM(Indicator4Denominator) as 'Indicator4Denominator',
	
	SUM(Indicator5Total) as 'Indicator5Total',
	
	CAST(
	CAST(SUM(Indicator5Numerator) AS DECIMAL(7,2)) 
	    / CAST(SUM(Indicator5Denominator) AS DECIMAL(7,2)) * 100
	AS DECIMAL(5,2)) AS 'Indicator5Percentage',
	
	SUM(Indicator5Numerator) as 'Indicator5Numerator',
	SUM(Indicator5Denominator) as 'Indicator5Denominator',
	
	SUM(Indicator6Total) as 'Indicator6Total',
	
	CAST(
	CAST(SUM(Indicator6Numerator) AS DECIMAL(7,2)) 
	    / CAST(SUM(Indicator6Denominator) AS DECIMAL(7,2)) * 100
	AS DECIMAL(5,2)) AS 'Indicator6Percentage',
	
	SUM(Indicator6Numerator) as 'Indicator6Numerator',
	SUM(Indicator6Denominator) as 'Indicator6Denominator',
	
	SUM(Indicator7Total) as 'Indicator7Total',
	
	CAST(
	CAST(SUM(Indicator7Numerator) AS DECIMAL(7,2)) 
	    / CAST(SUM(Indicator7Denominator) AS DECIMAL(7,2)) * 100
	AS DECIMAL(5,2)) AS 'Indicator7Percentage',
	
	SUM(Indicator7Numerator) as 'Indicator7Numerator',
	SUM(Indicator7Denominator) as 'Indicator7Denominator',
	
	SUM(UniqueScheduleTotal) as 'UniqueScheduleTotal',
	
	CAST(
	CAST(SUM(TotalComplianceNumerator) AS DECIMAL(7,2)) 
	    / CAST(SUM(TotalComplianceDenominator) AS DECIMAL(7,2)) * 100
	AS DECIMAL(5,2)) AS 'TotalCompliancePercentage',
	
	SUM(TotalComplianceNumerator) as 'TotalComplianceNumerator',
	SUM(TotalComplianceDenominator) as 'TotalComplianceDenominator',
	ServiceRefNo,
	CarerRefNo
FROM
	[COM].DataCompliances

WHERE
	--PMc start
		MonthOrder >= @MonthFrom
	AND
		MonthOrder <= @MonthTo
	/*AND
	--PMc end
	TCSDestinationId =	CASE @AdminLogin
							WHEN 1 THEN TCSDestinationId
							ELSE @TCSDestinationID
						END*/
	AND
		(
				ServiceRefNo = @Services
			OR
				@Services = -999
		)
	AND
		(
				CarerRefNo = @Carers
			OR
				@Carers = -999
		)
		

GROUP BY
	Directorate,
	ServiceName,
	Carer,
	ServiceRefNo,
	CarerRefNo

ORDER BY
	Directorate,
	ServiceName,
	Carer