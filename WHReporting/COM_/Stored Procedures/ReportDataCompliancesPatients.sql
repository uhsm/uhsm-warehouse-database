﻿CREATE PROCEDURE [COM].[ReportDataCompliancesPatients] 

@FailIndicator int,
@ServiceRefNo int,
@CarerRefNo int,
@MonthFrom int,
@MonthTo int


   
AS

SELECT
	Directorate,
	ServiceName,
	ClinicCode,
	Carer,
	ScheduleUserCreate,
	ReferralUserCreate,
	NHSNumber,
	ReferralDate,
	AppointmentDate,
	FailIndicator,
	FailIndicator2
FROM
	[com].DataCompliancesPatients
WHERE
		
		FailIndicatorNo = @FailIndicator
	AND
		(
				ServiceRefNo in (@ServiceRefNo)
			/*OR
				@ServiceRefNo = -999*/
		)
	AND
		(
				CarerRefNo in (@CarerRefNo)
			/*OR
				@CarerRefNo = -999*/
		)
		

    AND
        (
        
        	year(AppointmentDate)*100+Month(AppointmentDate) >= @MonthFrom

        )
        
    AND
        (
        
        	year(AppointmentDate)*100+Month(AppointmentDate) <= @MonthTo

        )        
	
ORDER BY
	Directorate,
	ServiceName,
	Carer,
	ClinicCode,
	NHSNumber