﻿CREATE procedure [COM].[UpdateCommunityDataWHReporting] as
/**
/*----------------------------------------------------------------------------------------------------------
Created/Modified Date			Created/Modified By			Details
---------------------			-------------------			---------------
Created 02/03/2015					K Oakden				Called from uhsm-is1.COM.UpdateCommunityData
-------------------------------------------------------------------------------------------------------------*/
**/
declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

--declare @Today datetime
--select @Today = DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))

--if (
--	select 1
--	from [Community].[dbo].[AuditLog]
--	where ProcessCode = 'Community - PAS RefreshTables'
--		and CAST(eventtime as date) = CAST(getdate() as date)
--	) is not null
--and (	
--	select dateadd(day, 0, datediff(day, 0, (DateValue))) from WH.dbo.Parameter where parameter = 'LOADCOMMUNITYPASDATE'
--	)
--	< @Today
	
--begin
--print 'run'

	--In Community
	--exec COM.UpdateBaseReferrals

	--exec COM.UpdateBaseGroupSessions

	--exec COM.UpdateBaseIndirectPatientActivity

	--exec COM.UpdateBaseOtherClientLinkedActivity

	--exec COM.UpdateBaseSchedules

	--exec COM.UpdateBaseSpells

	--exec COM.UpdateBaseWardStays

	--exec com.UpdateBaseSecondaryCarer

	--exec COM.UpdateBasePatients

	--exec COM.PopulateDataComplianceTables

	--exec COM.UpdateBaseRTT
	
--/**	
--IN WHReporting

if (
	select DateValue from WH.dbo.Parameter where Parameter = 'LOADCOMMUNITYPASDATE'
	) < (
	select DateValue from WH.dbo.Parameter where Parameter = 'LOADCOMMUNITYPASDATEIS1'
	)
begin
--print 'run'
	exec WHReporting.COM.UpdateIndirectPatientActivity

	exec WHReporting.COM.UpdateGroupSessions

	exec WHReporting.COM.UpdateOtherClientLinkedActivity

	exec WHReporting.COM.UpdatePatients

	exec WHReporting.COM.UpdateReferrals

	exec WHReporting.COM.UpdateRTT

	exec WHReporting.COM.UpdateSchedules

	exec WHReporting.COM.UpdateSecondaryCarer

	exec WHReporting.COM.UpdateSchedulesPatient

	exec WHReporting.COM.UpdateSpells

	exec WHReporting.COM.UpdateWardStay

	exec WHReporting.COM.UpdateDataCompliances
	
	exec WHReporting.COM.UpdateProfessionalCarer -- added by JB on 11/09/2015
	
	update WH.dbo.Parameter
	set DateValue = getdate()
	where parameter = 'LOADCOMMUNITYPASDATE'
	select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

	select @Stats = 
		'Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins' 

	exec dbo.WriteAuditLogEvent 'Community - UpdateCommunityDataWHReporting', @Stats, @StartTime
--**/

end