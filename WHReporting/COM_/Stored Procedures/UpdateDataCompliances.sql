﻿/*
--Author: J Cleworth
--Date created: 23/08/2013

*/

CREATE Procedure [COM].[UpdateDataCompliances]
as

drop table COM.DataCompliances
drop table COM.DataCompliancesPatients

Select [ID]
      ,[MonthOrder]
      ,[Month]
      ,[TCSDestinationId]
      ,[Directorate]
      ,[ServiceName]
      ,[Carer]
      ,[ServiceRefNo]
      ,[CarerRefNo]
      ,[TotalActivity]
      ,[TotalFailed]
      ,[Indicator1Total]
      ,[Indicator1Percentage]
      ,[Indicator1Numerator]
      ,[Indicator1Denominator]
      ,[Indicator2Total]
      ,[Indicator2Percentage]
      ,[Indicator2Numerator]
      ,[Indicator2Denominator]
      ,[Indicator3Total]
      ,[Indicator3Percentage]
      ,[Indicator3Numerator]
      ,[Indicator3Denominator]
      ,[Indicator4Total]
      ,[Indicator4Percentage]
      ,[Indicator4Numerator]
      ,[Indicator4Denominator]
      ,[Indicator5Total]
      ,[Indicator5Percentage]
      ,[Indicator5Numerator]
      ,[Indicator5Denominator]
      ,[Indicator6Total]
      ,[Indicator6Percentage]
      ,[Indicator6Numerator]
      ,[Indicator6Denominator]
      ,[Indicator7Total]
      ,[Indicator7Percentage]
      ,[Indicator7Numerator]
      ,[Indicator7Denominator]
      ,[UniqueScheduleTotal]
      ,[TotalCompliancePercentage]
      ,[TotalComplianceNumerator]
      ,[TotalComplianceDenominator]
      into COM.DataCompliances
  FROM [uhsm-is1].[Community].[COM].[DataCompliances]
  
  
  SELECT [ID]
      ,[MonthOrder]
      ,[Month]
      ,[FailIndicatorNo]
      ,[TCSDestinationId]
      ,[ServiceRefNo]
      ,[CarerRefNo]
      ,[ServiceName]
      ,ClinicCode
      ,[Carer]
      ,[Directorate]
      ,[NHSNumber]
      ,[ReferralDate]
      ,[AppointmentDate]
      ,[FailIndicator]
      ,[FailIndicator2]
      ,[ScheduleId],
	ScheduleUserCreate,
	ReferralUserCreate
      INTO COM.DataCompliancesPatients
  FROM [uhsm-is1].[Community].[COM].[DataCompliancePatients]