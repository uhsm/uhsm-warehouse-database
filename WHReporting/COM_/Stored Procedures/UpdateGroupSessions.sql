﻿/*
--Author: J Cleworth
--Date created: 25/07/2013

*/

CREATE Procedure [COM].[UpdateGroupSessions]
as

/*CREATE TABLE [COM].[GroupSessions](
	[GroupSessionIdentifier] [numeric](10, 0)not NULL,
	[GroupSessionDateTime] [datetime] NULL,
	[GroupSessionDate] [date] NULL,
	[GroupSessionTime] [Time] (7) NULL,
	[ContactDuration] [int] NULL,
	[GroupSessionTypeNationalCode] [float] NULL,
	[NumberOfAttendees] [numeric](5, 0) NULL,
	[ActivityLocationTypeNationalCode] [nvarchar](255) NULL,
	[SiteCodeOfTreatment] [varchar](20) NULL,
	[CareProfessionalStaffGroupCode] [nvarchar](255) NULL,
 CONSTRAINT [pk_CommGroupSessions] PRIMARY KEY CLUSTERED 
(
	[GroupSessionIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO*/
truncate table [COM].[GroupSessions]
insert into [COM].[GroupSessions]

([GroupSessionIdentifier]
      ,[GroupSessionDateTime]
      ,[GroupSessionDate]
      ,[GroupSessionTime]
      ,[ContactDuration]
      ,[GroupSessionTypeNationalCode]
      ,[NumberOfAttendees]
      ,[ActivityLocationTypeNationalCode]
      ,[SiteCodeOfTreatment]
      ,[CareProfessionalStaffGroupCode])

select 

[GroupSessionIdentifier]
      ,[GroupSessionDateTime]
      ,[GroupSessionDate]
      ,[GroupSessionTime]
      ,[ContactDuration]
      ,[GroupSessionTypeNationalCode]
      ,[NumberOfAttendees]
      ,[ActivityLocationTypeNationalCode]
      ,[SiteCodeOfTreatment]
      ,[CareProfessionalStaffGroupCode]

from [uhsm-is1].community.COM.GroupSessions