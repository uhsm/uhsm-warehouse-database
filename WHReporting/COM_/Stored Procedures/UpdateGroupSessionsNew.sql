﻿/*
--Author: J Cleworth
--Date created: 25/07/2013
--KO updated 19/02/2015 to remove intermediate step
*/

CREATE Procedure [COM].[UpdateGroupSessionsNew]
as


declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

truncate table [COM].[GroupSessions]
insert into [COM].[GroupSessions]

([GroupSessionIdentifier]
      ,[GroupSessionDateTime]
      ,[GroupSessionDate]
      ,[GroupSessionTime]
      ,[ContactDuration]
      ,[GroupSessionTypeNationalCode]
      ,[NumberOfAttendees]
      ,[ActivityLocationTypeNationalCode]
      ,[SiteCodeOfTreatment]
      ,[CareProfessionalStaffGroupCode])
select distinct
GroupSessionIdentifier=s.[SCHDL_REFNO],
GroupSessionDateTime=s.START_DTTM,
GroupSessionDate=cast(s.START_DTTM as date),
GroupSessionTime=CAST(s.START_DTTM as time),
ContactDuration=Datediff(Minute,s.START_DTTM,s.END_DTTM),--Check on this
GroupSessionTypeNationalCode=gst.[National Code],--Link to lk.GroupSessionType to get description
NumberOfAttendees=ACTUAL_ATTENDEES,
ActivityLocationTypeNationalCode=alt.National_Code,--Link to ActivityLocationType to Get Description
SiteCodeOfTreatment=he.MAIN_IDENT
,CareProfessionalStaffGroupCode=sg.National_Code--Link to lk.CareProfessionalStaffGroup to Get Description


from Community.PAS.SCHEDULES_VE06_NON_PATIENT_EVENTS s with (nolock)
inner join Community.lk.GroupSessionType gst on gst.satyp_refno=s.satyp_refno
left join Community.lk.ActivityLocationType alt  on alt.LOTYP_REFNO=s.LOTYP_REFNO
left join Community.PAS.HEALTH_ORGANISATIONS_VE01 he with (nolock)
on s.lotyp_refno =he.heorg_refno
left join  Community.PAS.[PROF_CARERS_VE01] p with (nolock)
on npe_proca_refno = p.proca_refno
left join Community.lk.CareProfessionalStaffGroup sg on sg.PRTYP_REFNO=p.PRTYP_REFNO
where
s.ARCHV_FLAG='N'
AND PATNT_REFNO NOT IN (SELECT PATNT_REFNO FROM Community.PAS.IPMTestPatients)

--select 

--[GroupSessionIdentifier]
--      ,[GroupSessionDateTime]
--      ,[GroupSessionDate]
--      ,[GroupSessionTime]
--      ,[ContactDuration]
--      ,[GroupSessionTypeNationalCode]
--      ,[NumberOfAttendees]
--      ,[ActivityLocationTypeNationalCode]
--      ,[SiteCodeOfTreatment]
--      ,[CareProfessionalStaffGroupCode]

--from community.COM.GroupSessions

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins' 

exec dbo.WriteAuditLogEvent 'Community - UpdateGroupSessions', @Stats, @StartTime