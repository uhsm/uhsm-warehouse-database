﻿/*
--Author: J Cleworth
--Date created: 25/07/2013

*/

CREATE Procedure [COM].[UpdateIndirectPatientActivity]
as

/*CREATE TABLE [COM].[IndirectPatientActivity](
	[ActivityIdentifier] [numeric](10, 0) not NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[StartTime] [time](7) NULL,
	[EndTime] [time](7) NULL,
	[StartDateTime] [datetime] NULL,
	[EndDateTime] [datetime] NULL,
	[ActivityDuration] [int] NULL,
	[Comments] [varchar](255) NULL,
	[Location] [varchar](255) NULL,
	[IndirectPatientActivityTypeCodeCIDS] [float] NULL,
	[ActivityTypeLocalCode] [varchar](80) NULL,
	[StaffGroup] [nvarchar](255) NULL,
 CONSTRAINT [pk_CommIndirect] PRIMARY KEY CLUSTERED 
(
	[ActivityIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO*/

truncate table [COM].[IndirectPatientActivity]
insert into [COM].[IndirectPatientActivity]

([ActivityIdentifier]
      ,[StartDate]
      ,[EndDate]
      ,[StartTime]
      ,[EndTime]
      ,[StartDateTime]
      ,[EndDateTime]
      ,[ActivityDuration]
      ,[Comments]
      ,[Location]
      ,[IndirectPatientActivityTypeCodeCIDS]
      ,[ActivityTypeLocalCode]
      ,[StaffGroup])

select 

[ActivityIdentifier]
      ,[StartDate]
      ,[EndDate]
      ,[StartTime]
      ,[EndTime]
      ,[StartDateTime]
      ,[EndDateTime]
      ,[ActivityDuration]
      ,[Comments]
      ,[Location]
      ,[IndirectPatientActivityTypeCodeCIDS]
      ,[ActivityTypeLocalCode]
      ,[StaffGroup]

from [uhsm-is1].Community.COM.IndirectPatientActivity