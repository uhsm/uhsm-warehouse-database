﻿/*
--Author: J Cleworth
--Date created: 25/07/2013
--KO updated 19/02/2015 to remove intermediate step
*/

CREATE Procedure [COM].[UpdateIndirectPatientActivityNew]
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

truncate table [COM].[IndirectPatientActivity]
insert into [COM].[IndirectPatientActivity] (
	[ActivityIdentifier]
	,[StartDate]
	,[EndDate]
	,[StartTime]
	,[EndTime]
	,[StartDateTime]
	,[EndDateTime]
	,[ActivityDuration]
	,[Comments]
	,[Location]
	,[IndirectPatientActivityTypeCodeCIDS]
	,[ActivityTypeLocalCode]
	,[StaffGroup]
	)
select ActivityIdentifier = SCHDL_REFNO
	,StartDate = cast(s.START_DTTM as date)
	,EndDate = cast(s.END_DTTM as date)
	,StartTime = cast(s.START_DTTM as time)
	,EndTime = cast(s.END_DTTM as time)
	,StartDateTime = cast(s.START_DTTM as datetime)
	,EndDateTime = cast(s.END_DTTM as datetime)
	,ActivityDuration = DATEDIFF(minute, s.START_DTTM, s.END_DTTM)
	,Comments
	,Location
	,IndirectPatientActivityTypeCodeCIDS = ipt.[National Code]
	,ActivityTypeLocalCode = v.[DESCRIPTION]
	,StaffGroup = sg.National_Code --Link to lk.CareProfessionalStaffGroup to Get Description

from Community.pas.SCHEDULES_VE06_NON_PATIENT_EVENTS s
left join Community.lk.IndirectPatientActivityType ipt
	on ipt.satyp_refno = s.satyp_refno
left join Community.PAS.REFERENCE_VALUES_VE01 v
	on s.satyp_refno = v.rfval_refno
left join Community.PAS.[PROF_CARERS_VE01] p
	on npe_proca_refno = p.proca_refno
left join Community.lk.CareProfessionalStaffGroup sg
	on sg.PRTYP_REFNO = p.prtyp_refno
where s.ARCHV_FLAG = 'N'
	and SCHDL_REFNO not in (
		select GroupSessionIdentifier
		from com.GroupSessions
		)
	and not exists (
		select 1
		from Community.PAS.IPMTestPatients
		where IPMTestPatients.PATNT_REFNO = s.PATNT_REFNO
		)

--select 

--[ActivityIdentifier]
--      ,[StartDate]
--      ,[EndDate]
--      ,[StartTime]
--      ,[EndTime]
--      ,[StartDateTime]
--      ,[EndDateTime]
--      ,[ActivityDuration]
--      ,[Comments]
--      ,[Location]
--      ,[IndirectPatientActivityTypeCodeCIDS]
--      ,[ActivityTypeLocalCode]
--      ,[StaffGroup]

--from community.COM.IndirectPatientActivity

  select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins' 


exec dbo.WriteAuditLogEvent 'Community - UpdateIndirectPatientActivity', @Stats, @StartTime