﻿/*
--Author: J Cleworth
--Date created: 25/07/2013

*/

CREATE Procedure [COM].[UpdateOtherClientLinkedActivity]
as

/*CREATE TABLE [COM].[OtherClientLinkedActivity](
	[NHSnumber] [varchar](20) NULL,
	[PatientID] [varchar](20) NULL,
	[FacilityID] [varchar](20) NULL,
	[ReferralID] [varchar](50) NULL,
	[ScheduleID] [numeric](10, 0) NOT NULL,
	[Age] [int] NULL,
	[ActivityDate] [date] NULL,
	[ActivityTime] [time](7) NULL,
	[ActivityDateTime] [datetime] NULL,
	[ActivityDuration] [int] NULL,
	[CareProfessionalStaffGroupCommunityCareNationalCode] [nvarchar](255) NULL,
	[ProfessionalCarer] [varchar](61) NULL,
	[ActivitySpecialty] [nvarchar](200) NULL,
	[ActivitySpecialtyDivision] [nvarchar](200) NULL,
	[ActivitySpecialtyDirectorate] [nvarchar](200) NULL,
	[ActivityCreatedBy] [varchar](35) NULL,
	[ActivityCreatedDate] [datetime] NULL,
	[EpisodicGPCode] [varchar](20) NULL,
	[EpisodicGPName] [varchar](62) NULL,
	[EpisodicGPPracticeCode] [varchar](20) NULL,
	[EpisodicGPPracticeName] [varchar](255) NULL,
	[EpisodicCCGCode] [varchar](10) NOT NULL,
	[EpisodicCCGName] [varchar](255) NOT NULL,
	PatientPostCode varchar (20) null,
 CONSTRAINT [PK_OtherClientLinkedActivity] PRIMARY KEY CLUSTERED 
(
	[ScheduleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO*/


Truncate Table [COM].[OtherClientLinkedActivity]


insert into [COM].[OtherClientLinkedActivity]

([NHSnumber]
      ,[PatientID]
      ,[FacilityID]
      ,[ReferralID]
      ,[ScheduleID]
      ,[Age]
      ,[ActivityDate]
      ,[ActivityTime]
      ,[ActivityDateTime]
      ,[ActivityDuration]
      ,[CareProfessionalStaffGroupCommunityCareNationalCode]
      ,[ProfessionalCarer]
      ,[ActivitySpecialty]
      ,[ActivitySpecialtyDivision]
      ,[ActivitySpecialtyDirectorate]
      ,[ActivityCreatedBy]
      ,[ActivityCreatedDate]
      ,[EpisodicGPCode]
      ,[EpisodicGPName]
      ,[EpisodicGPPracticeCode]
      ,[EpisodicGPPracticeName]
      ,[EpisodicCCGCode]
      ,[EpisodicCCGName]
      ,PatientPostCode)
	
	select distinct
	
	[NHSnumber]
      ,[PatientID]
      ,[FacilityID]
      ,[ReferralID]
      ,[ScheduleID]
      ,[Age]
      ,[ActivityDate]
      ,[ActivityTime]
      ,[ActivityDateTime]
      ,[ActivityDuration]
      ,[CareProfessionalStaffGroupCommunityCareNationalCode]
      ,[ProfessionalCarer]
      ,[ActivitySpecialty]
      ,[ActivitySpecialtyDivision]
      ,[ActivitySpecialtyDirectorate]
      ,[ActivityCreatedBy]
      ,[ActivityCreatedDate]
      ,[EpisodicGPCode]
      ,[EpisodicGPName]
      ,[EpisodicGPPracticeCode]
      ,[EpisodicGPPracticeName]
      ,[EpisodicCCGCode]
      ,[EpisodicCCGName]
      ,PatientPostCode
	
	from [uhsm-is1].Community.COM.OtherClientLinkedActivity
	
	

  
update COM.OtherClientLinkedActivity

set EpisodicCCGCode=coalesce((select CCG FROM organisationccg.dbo.[CCG Practice PCT Lookup] CCGPLk where CCGPLk.Practice=COM.OtherClientLinkedActivity.episodicGPPracticeCode),
(select [PCG of Residence] from OrganisationCCG.dbo.[Extended Postcode] ccgpostcode where ccgpostcode.postcode=COM.OtherClientLinkedActivity.PatientPostCode))


update COM.OtherClientLinkedActivity

set EpisodicCCGName=(select distinct [CCG Name] from OrganisationCCG.dbo.[CCG Practice PCT Lookup] ccglk where ccglk.CCG=COM.OtherClientLinkedActivity.EpisodicCCGCode)