﻿/*
--Author: J Cleworth
--Date created: 25/07/2013
--KO updated 19/02/2015 to remove intermediate step
*/

CREATE Procedure [COM].[UpdateOtherClientLinkedActivityNew]
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

Truncate Table [COM].[OtherClientLinkedActivity]
insert into [COM].[OtherClientLinkedActivity]

([NHSnumber]
      ,[PatientID]
      ,[FacilityID]
      ,[ReferralID]
      ,[ScheduleID]
      ,[Age]
      ,[ActivityDate]
      ,[ActivityTime]
      ,[ActivityDateTime]
      ,[ActivityDuration]
      ,[CareProfessionalStaffGroupCommunityCareNationalCode]
      ,[ProfessionalCarer]
      ,[ActivitySpecialty]
      ,[ActivitySpecialtyDivision]
      ,[ActivitySpecialtyDirectorate]
      ,[ActivityCreatedBy]
      ,[ActivityCreatedDate]
      ,[EpisodicGPCode]
      ,[EpisodicGPName]
      ,[EpisodicGPPracticeCode]
      ,[EpisodicGPPracticeName]
      ,[EpisodicCCGCode]
      ,[EpisodicCCGName]
      ,PatientPostCode)

(
		select --Distinct 	
			E.NHS_IDENTIFIER as NHSnumber
			,CAST(A.PATNT_REFNO as varchar(20)) as PatientID
			,DistrictNo.IDENTIFIER as FacilityID
			,cast(A.REFRL_REFNO as varchar(50)) as ReferralID
			,A.SCHDL_REFNO as ScheduleID
			,dbo.GetAgeOnSpecifiedDate(CAST(E.DTTM_OF_BIRTH as datetime), CAST(A.START_DTTM as datetime)) as Age
			--Organisation Code (Code of Commissioner) this can be looked up from referrals??
			,CAST(A.START_DTTM as date) as ActivityDate
			,CAST(A.START_DTTM as time) as ActivityTime
			,CAST(A.START_DTTM as datetime) as ActivityDateTime --Added this in for reporting purposes
			,case a.SCTYP_REFNO
				when 1468 -- Community Contact
					then datediff(minute, a.START_DTTM, a.END_DTTM)
				when 1470 -- Clinic Contact
					then datediff(minute, a.START_DTTM, a.DEPARTED_DTTM)
				end as ActivityDuration
			,ahp.National_Code as CareProfessionalStaffGroupCommunityCareNationalCode
			,isnull(j.[SURNAME], '') + ',' + isnull(j.[FORENAME], '') as ProfessionalCarer
			,lksd.IPMDescription as ActivitySpecialty
			,lksd.Division as ActivitySpecialtyDivision
			,lksd.Directorate as ActivitySpecialtyDirectorate
			,dz.[USER_NAME] as ActivityCreatedBy
			,a.CREATE_DTTM as ActivityCreatedDate
			,EpisodicGPCode = pcarerlk.MAIN_IDENT
			,EpisodicGPName = isnull(pcarerlk.SURNAME, '') + ', ' + isnull(pcarerlk.FORENAME, '')
			,EpisodicGPPracticeCode = episodicGPPrac.MAIN_IDENT
			,EpisodicGPPracticeName = EpisodicGPPrac.[Description]
			,EpisodicCCGCode = '' --coalesce(ccgpractice.CCG,ccgpostcode.[PCG of Residence],'unk')
			,EpisodicCCGName = '' --coalesce(ccgpractice.ccg,lkccgname.ccg,'unk')
			,ptaddress.PCODE as PatientPostCode
		--INTO COM.OtherClientLinkedActivity
		from Community.PAS.SCHEDULES_VE06_RM2 A
		left join Community.PAS.SPECIALTIES_VE01 B
			on A.SPECT_REFNO = B.SPECT_REFNO
		--LEFT OUTER JOIN dbo.[Contract] C ON A.CONTR_REFNO = C.CONTR_REFNO
		--LEFT OUTER JOIN PAS.PURCHASERS_VE01 D ON A.PURCH_REFNO = D.PURCH_REFNO
		left join Community.PAS.PATIENTS_VE05_RM2 E
			on A.PATNT_REFNO = E.PATNT_REFNO
		left join Community.PAS.REFERENCE_VALUES_VE01 F
			on A.VISIT_REFNO = F.RFVAL_REFNO
		left join Community.PAS.REFERENCE_VALUES_VE01 G
			on A.URGNC_REFNO = G.RFVAL_REFNO
		left join Community.PAS.REFERENCE_VALUES_VE01 H
			on A.PRITY_REFNO = H.RFVAL_REFNO
		left join Community.PAS.REFERENCE_VALUES_VE01 I
			on A.CANCR_REFNO = I.RFVAL_REFNO
		left join Community.PAS.PROF_CARERS_VE01 J
			on A.PROCA_REFNO = J.PROCA_REFNO
		left join Community.PAS.REFERENCE_VALUES_VE01 K
			on A.ATTND_REFNO = K.RFVAL_REFNO
		left join Community.PAS.REFERENCE_VALUES_VE01 L
			on A.SCOCM_REFNO = L.RFVAL_REFNO
		left join Community.PAS.REFERENCE_VALUES_VE01 M
			on A.ADCAT_REFNO = M.RFVAL_REFNO
		left join Community.PAS.REFERENCE_VALUES_VE01 N
			on A.CANCB_REFNO = N.RFVAL_REFNO
		left join Community.PAS.REFERENCE_VALUES_VE01 P
			on A.CONTY_REFNO = P.RFVAL_REFNO
		left join Community.PAS.REFERENCE_VALUES_VE01 R
			on A.BKTYP_REFNO = R.RFVAL_REFNO
		left join Community.PAS.REFERENCE_VALUES_VE01 S
			on A.SCTYP_REFNO = S.RFVAL_REFNO
		left join Community.PAS.REFERRALS_VE07_RM2 T
			on A.REFRL_REFNO = T.REFRL_REFNO
		left join Community.PAS.SPECIALTIES_VE01 U
			on T.REFTO_SPECT_REFNO = U.SPECT_REFNO
		left join Community.PAS.REFERENCE_VALUES_VE01 V
			on A.LOTYP_REFNO = V.RFVAL_REFNO
		left join Community.PAS.REFERENCE_VALUES_VE01 W
			on A.CONTP_REFNO = W.RFVAL_REFNO
		left join Community.PAS.REFERENCE_VALUES_VE01 X
			on A.CSTAT_REFNO = X.RFVAL_REFNO
		left join Community.PAS.SERVICE_POINTS_VE03 Y
			on A.SPONT_REFNO = Y.SPONT_REFNO
		left join Community.PAS.REFERENCE_VALUES_VE01 Z
			on A.RESUT_REFNO = Z.RFVAL_REFNO
		left join Community.PAS.REFERENCE_VALUES_VE01 AZ
			on A.DNARS_REFNO = AZ.RFVAL_REFNO
		left join Community.PAS.PROF_CARERS_VE01 AY
			on A.SEENBY_PROCA_REFNO = AY.PROCA_REFNO
		left join (
			select ar.PATNT_REFNO
				,a.PCODE
				,a.LINE1
				,a.LINE2
				,a.LINE3
				,a.line4
			from Community.PAS.ADDRESS_ROLES_VE02 ar
			left join Community.PAS.ADDRESSES_VE02 a
				on ar.ADDSS_REFNO = a.ADDSS_REFNO
			where ar.ARCHV_FLAG = 'N'
				and ar.ROTYP_CODE = 'HOME'
				and ar.END_DTTM is null
				and a.ADTYP_CODE = 'POSTL'
				and a.ARCHV_FLAG = 'N'
				and a.END_DTTM is null
				and ar.ADDSS_REFNO in (
					select MAX(ar2.ADDSS_REFNO)
					from Community.PAS.ADDRESS_ROLES_VE02 ar2
					left join Community.PAS.ADDRESSES_VE02 a2
						on ar2.ADDSS_REFNO = a2.ADDSS_REFNO
					where ar2.ARCHV_FLAG = 'N'
						and ar2.ROTYP_CODE = 'HOME'
						and ar2.END_DTTM is null
						and a2.ADTYP_CODE = 'POSTL'
						and a2.ARCHV_FLAG = 'N'
						and a2.END_DTTM is null
					group by PATNT_REFNO
					)
			) ptaddress
			on E.PATNT_REFNO = ptaddress.PATNT_REFNO
		left join Community.PAS.PATIENT_IDS_VE01 DistrictNo
			on DistrictNo.PATNT_REFNO = E.PATNT_REFNO
				and DistrictNo.PITYP_REFNO = 2001232
				and DistrictNo.IDENTIFIER like '5NT%'
				and DistrictNo.ARCHV_FLAG = 'N'
				and not exists (
					select 1
					from Community.PAS.PATIENT_IDS_VE01 Previous
					where Previous.PATNT_REFNO = DistrictNo.PATNT_REFNO
						and Previous.PITYP_REFNO = 2001232
						and Previous.IDENTIFIER like '5NT%'
						and Previous.ARCHV_FLAG = 'N'
						and (
							Previous.START_DTTM > DistrictNo.START_DTTM
							or (
								Previous.START_DTTM = DistrictNo.START_DTTM
								and Previous.PATID_REFNO > DistrictNo.PATID_REFNO
								)
							)
					)
		left join (
			select MIN(SCHDL_REFNO) as SCHDL_REFNO
				,1 as FirstAttend
			from (
				select REFRL_REFNO
					,SCHDL_REFNO
					,START_DTTM
				from Community.PAS.SCHEDULES_VE06_RM2 sched
				inner join Community.COM.lkAttendStatus ast
					on ast.attnd_refno = sched.ATTND_REFNO
				--and ast.cancr_refno=sched.CANCR_REFNO
				where sched.ATTND_Refno in (
						357
						,2868
						)
					and ast.AttendStatusNationalCode in (
						'5'
						,'6'
						)
				) as Attends
			group by REFRL_REFNO
			) as FirstAttends
			on FirstAttends.SCHDL_REFNO = A.SCHDL_REFNO
		left join (
			select SCHDL_REFNO
				,1 as FirstFutureApt
			from Community.PAS.SCHEDULES_VE06_RM2 sched
			where START_DTTM >= GETDATE()
				and SCHDL_REFNO in (
					select MIN(SCHDL_REFNO)
					from Community.PAS.SCHEDULES_VE06_RM2
					group by REFRL_REFNO
					)
			) as FutureFirstApts
			on FutureFirstApts.SCHDL_REFNO = A.SCHDL_REFNO
		left join Community.lk.ActivityLocationType lkal
			on lkal.LOTYP_REFNO = a.LOTYP_REFNO
		left join Community.PAS.HEALTH_ORGANISATIONS_VE01 he
			on a.lotyp_refno = he.heorg_refno
		left join Community.pas.REFERENCE_VALUES_VE01 bz
			on j.prtyp_refno = bz.rfval_refno
		left join Community.lk.AHP ahp
			on ahp.PRTYP_REFNO = J.PRTYP_REFNO
		left join Community.lk.CancellationReason lkcr
			on lkcr.CANCR_REFNO = a.CANCR_REFNO
		left join Community.PAS.SPECIALTIES_VE01 cz
			on a.spect_refno = cz.spect_refno
		left join Community.lk.SpecialtyDivision lksd
			on lksd.SPECT_REFNO = a.SPECT_REFNO
		left join (
			select user_create
				,[user_name]
			from Community.PAS.[USERS_VE01]
			where ARCHV_FLAG = 'N'
			) as dz
			on a.user_create collate database_default = dz.user_create collate database_default
		left join Community.PAS.PATIENT_PROF_CARERS_VE01 RegisteredGp
			on RegisteredGp.PATNT_REFNO = a.PATNT_REFNO
				and RegisteredGp.PRTYP_REFNO = 1132
				and RegisteredGp.ARCHV_FLAG = 'N'
				and a.START_DTTM between RegisteredGp.START_DTTM
					and coalesce(RegisteredGp.END_DTTM, a.START_DTTM)
				and not exists (
					select 1
					from Community.PAS.PATIENT_PROF_CARERS_VE01 Previous
					where Previous.PATNT_REFNO = RegisteredGp.PATNT_REFNO
						and Previous.PRTYP_REFNO = RegisteredGp.PRTYP_REFNO
						and Previous.ARCHV_FLAG = 'N'
						and a.START_DTTM between Previous.START_DTTM
							and coalesce(Previous.END_DTTM, a.START_DTTM)
						and (
							Previous.START_DTTM > RegisteredGp.START_DTTM
							or (
								Previous.START_DTTM = RegisteredGp.START_DTTM
								and Previous.PATPC_REFNO > RegisteredGp.PATPC_REFNO
								)
							)
					)
		left join Community.PAS.PROF_CARERS_VE01 pcarerlk
			on pcarerlk.PROCA_REFNO = registeredGP.PROCA_REFNO
		left join Community.PAS.HEALTH_ORGANISATIONS_VE01 episodicGPPrac
			on EpisodicGPPrac.HEORG_REFNO = registeredGP.HEORG_REFNO
		left join Community.dbo.vwScheduleType apttype
			on apttype.AppointmentTypeCode = a.VISIT_REFNO
		where A.ARCHV_FLAG = 'N'
			and A.CONTY_REFNO = 2004177
			and not exists (
				select 1
				from Community.PAS.IPMTestPatients
				where IPMTestPatients.PATNT_REFNO = a.PATNT_REFNO
				)
			--and 
			--(A.CREATE_DTTM>='2013-03-31'
			--OR
			--A.MODIF_DTTM >=@MaxModDate
			--)
			--a.MODIF_DTTM>@MaximumModified
		)

	--select distinct
	
	--[NHSnumber]
 --     ,[PatientID]
 --     ,[FacilityID]
 --     ,[ReferralID]
 --     ,[ScheduleID]
 --     ,[Age]
 --     ,[ActivityDate]
 --     ,[ActivityTime]
 --     ,[ActivityDateTime]
 --     ,[ActivityDuration]
 --     ,[CareProfessionalStaffGroupCommunityCareNationalCode]
 --     ,[ProfessionalCarer]
 --     ,[ActivitySpecialty]
 --     ,[ActivitySpecialtyDivision]
 --     ,[ActivitySpecialtyDirectorate]
 --     ,[ActivityCreatedBy]
 --     ,[ActivityCreatedDate]
 --     ,[EpisodicGPCode]
 --     ,[EpisodicGPName]
 --     ,[EpisodicGPPracticeCode]
 --     ,[EpisodicGPPracticeName]
 --     ,[EpisodicCCGCode]
 --     ,[EpisodicCCGName]
 --     ,PatientPostCode
	
	--from community.COM.OtherClientLinkedActivity
	
 
update COM.OtherClientLinkedActivity
set EpisodicCCGCode = coalesce((
			select CCG
			from organisationccg.dbo.[CCG Practice PCT Lookup] CCGPLk
			where CCGPLk.Practice = COM.OtherClientLinkedActivity.episodicGPPracticeCode
			), (
			select [PCG of Residence]
			from OrganisationCCG.dbo.[Extended Postcode] ccgpostcode
			where ccgpostcode.postcode = COM.OtherClientLinkedActivity.PatientPostCode
			))

update COM.OtherClientLinkedActivity
set EpisodicCCGName = (
		select distinct [CCG Name]
		from OrganisationCCG.dbo.[CCG Practice PCT Lookup] ccglk
		where ccglk.CCG = COM.OtherClientLinkedActivity.EpisodicCCGCode
		)


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins' 


exec dbo.WriteAuditLogEvent 'Community - UpdateBaseOtherClientLinkedActivity', @Stats, @StartTime