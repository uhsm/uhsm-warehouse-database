﻿/*
--Author: J Cleworth
--Date created: 25/07/2013

*/

CREATE Procedure [COM].[UpdatePatients]

/*CREATE TABLE [dbo].[Patients](
	[NHSNumber] [varchar](20) NULL,
	[NHSNumberStatus] [varchar](13) NOT NULL,
	[PatientID] [numeric](10, 0) NOT NULL,
	[Forename] [varchar](30) NULL,
	[Surname] [varchar](30) NULL,
	[BirthDate] [datetime] NULL,
	[DeathDate] [datetime] NULL,
	[Postcode] [varchar](25) NULL,
	[GeneralMedicalPracticeCode] [nvarchar](8) NULL,
	[CCGOfPractice] [varchar](10) NULL,
	[Gender] [varchar](80) NULL,
	[EthnicCategory] [varchar](80) NULL,
	[CommunicationLanguage] [varchar](80) NULL,
	[OrganisationCodePCTOfResidence] [char](3) NULL,
	[ActualDeathLocationType] [varchar](80) NULL,
 CONSTRAINT [pk_CommPatients] PRIMARY KEY CLUSTERED 
(
	[PatientID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]*/

as

Truncate Table [COM].[Patients]


insert into [COM].[Patients]

([NHSNumber]
      ,[NHSNumberStatus]
      ,[PatientID]
      ,[Forename]
      ,[Surname]
      ,[BirthDate]
      ,[DeathDate]
      ,[Postcode]
      ,[GeneralMedicalPracticeCode]
      ,[CCGOfPractice]
      ,[Gender]
      ,[EthnicCategory]
      ,[CommunicationLanguage]
      ,[OrganisationCodePCTOfResidence]
      ,[ActualDeathLocationType])
	
	select distinct
	
	[NHSNumber]
      ,[NHSNumberStatus]
      ,[PatientID]
      ,[Forename]
      ,[Surname]
      ,[BirthDate]
      ,[DeathDate]
      ,[Postcode]
      ,[GeneralMedicalPracticeCode]
      ,[CCGOfPractice]
      ,[Gender]
      ,[EthnicCategory]
      ,[CommunicationLanguage]
      ,[OrganisationCodePCTOfResidence]
      ,[ActualDeathLocationType]
	
	from [uhsm-is1].community.COM.Patients
	
	
	update COM.Patients

set [CCGOfPractice]=coalesce((select CCG 
FROM organisationccg.dbo.[CCG Practice PCT Lookup] CCGPLk 
where CCGPLk.Practice=com.Patients.[GeneralMedicalPracticeCode]),'-1')