﻿/*
--Author: J Begum
--Date created: 11/09/2015
--Table brought in as requested by Alison Olivant and Heather Slim on 11/09/2015

*/

CREATE Procedure [COM].[UpdateProfessionalCarer]



as

Truncate Table [COM].[lkProfessionalCarer]


insert into [COM].[lkProfessionalCarer]

([RowIdentifier]
      ,[ProfessionalCarerID]
      ,[ProfessionalCarerTypeCode]
      ,[ProfessionalCarerForename]
      ,[ProfessionalCarerSurname]
      ,[StartDate]
      ,[EndDate]
      ,[Description]
      ,[CreateDate]
      ,[ModifyDate]
      ,[CreatedBy]
      ,[ModifiedBy]
      ,[CurrentFlag]
      ,[LocalFlag]
      ,[MainIdentifier]
      ,[ArchiveFlag]
      ,[PrimaryStaffTeamRefno]
      ,[OwnerHealthOrgRefno])
	
	select 
	
	   [ROW_IDENTIFIER]
      ,[PROCA_REFNO]
      ,[PRTYP_REFNO]
      ,[FORENAME]
      ,[SURNAME]
      ,[START_DTTM]
      ,[END_DTTM]
      ,[DESCRIPTION]
      ,[CREATE_DTTM]
      ,[MODIF_DTTM]
      ,[USER_CREATE]
      ,[USER_MODIF]
      ,[CURNT_FLAG]
      ,[LOCAL_FLAG]
      ,[MAIN_IDENT]
      ,[ARCHV_FLAG]
      ,[PRIMARY_STEAM_REFNO]
      ,[OWNER_HEORG_REFNO]

	
	from [uhsm-is1].[Community].[PAS].[PROF_CARERS_VE01]