﻿/*
--Author: J Cleworth
--Date created: 25/07/2013

*/

CREATE Procedure [COM].[UpdateRTT]
as

/*CREATE TABLE [COM].[RTT](
	[UBRN] [varchar](255) NULL,
	[FacilityID] [varchar](20) NULL,
	[PatientID] [numeric](10, 0) NULL,
	[PatientPathwayIdentifier] [varchar](300) NULL,
	[WaitingTimeMeasurementType] [float] NULL,
	[ReferralID] [numeric](10, 0) NOT NULL,
	[RTTActivityLink] [numeric](10, 0) NULL,
	[RTTEncounterID] [numeric](10, 0) not NULL,
	[RTTStatusType] [varchar](10) NULL,
	[RTTStatusUpdateDate] [date] NULL,
	[RTTStatusCode] [varchar](2) NULL,
	[ReferralToTreatmentPeriodStartDate] [datetime] NULL,
	[ReferralToTreatmentPeriodEndDate] [datetime] NULL,
 CONSTRAINT [pk_Commrtt] PRIMARY KEY CLUSTERED 
(
	[RTTEncounterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO*/
truncate table [COM].[RTT]
insert into [COM].[RTT]

( [UBRN]
      ,[FacilityID]
      ,[PatientID]
      ,[PatientPathwayIdentifier]
      ,[WaitingTimeMeasurementType]
      ,[ReferralID]
      ,[RTTActivityLink]
      ,[RTTEncounterID]
      ,[RTTStatusType]
      ,[RTTStatusUpdateDate]
      ,[RTTStatusCode]
      ,[ReferralToTreatmentPeriodStartDate]
      ,[ReferralToTreatmentPeriodEndDate])

select 

 [UBRN]
      ,[FacilityID]
      ,[PatientID]
      ,[PatientPathwayIdentifier]
      ,[WaitingTimeMeasurementType]
      ,[ReferralID]
      ,[RTTActivityLink]
      ,[RTTEncounterID]
      ,[RTTStatusType]
      ,[RTTStatusUpdateDate]
      ,[RTTStatusCode]
      ,[ReferralToTreatmentPeriodStartDate]
      ,[ReferralToTreatmentPeriodEndDate]
from [uhsm-is1].community.COM.RTT