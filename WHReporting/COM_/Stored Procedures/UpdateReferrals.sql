﻿/*
--Author: J Cleworth
--Date created: 25/07/2013

*/

CREATE Procedure [COM].[UpdateReferrals]
as
/*
DROP TABLE COM.referrals
CREATE TABLE [COM].[referrals](
	[SourcePatientNo] [numeric](10, 0) NULL,
	[NHSNumber] [varchar](20) NULL,
	[FacilityID] [varchar](20) NULL,
	[ReferralID] [numeric](10, 0) NULL,
	[ReferralRequestReceivedDateTime] [datetime] NULL,
	[ReferralRequestReceivedDate] [date] NULL,
	[ReferralRequestReceivedTime] [time](7) NULL,
	[CodeOfCommissioner] [nvarchar](8) NULL,
	[CIDServiceTypeReferredTo] [nvarchar](255) NULL,
	[ReferringOrganisationCode] [varchar](20) NULL,
	[ReferringCareProfessionalStaffGroup] [nvarchar](255) NULL,
	[Priority] [varchar](4) NOT NULL,
	[ReferralClosureDate] [datetime] NULL,
	[ReferralClosureReason] [nvarchar](255) NULL,
	[DischargeDate] [datetime] NULL,
	[ReceivingProfCarer] [varchar](62) NULL,
	[ReferralCreatedDate] [datetime] NULL,
	[ReferredToDivision] [varchar](250) NOT NULL,
	[ReferredToDirectorate] [varchar](250) NOT NULL,
	[ReferringOrganisationName] [varchar](255) NULL,
	[ReferrerCode] [varchar](20) NULL,
	[ReferrerName] [varchar](61) NULL,
	[ReferralCreatedBy] [varchar](30) NULL,
	Authorised nchar (10) null,
	ReferralStatus nchar (10) null,
	[EpisodicGPCode] [varchar](25) NULL,
	[EpisodicGPName] [varchar](62) NULL,
	[EpisodicGPPracticeCode] [varchar](25) NULL,
	[EpisodicGPPracticeName] [varchar](255) NULL,
	[ReferredToSpecialtyCode] [numeric](10, 0) NULL,
	[ReferredToSpecialty] [varchar](250) NOT NULL,
	[SourceOfReferral] [nvarchar](255) NULL,
	SourceOfReferralDescription nvarchar (255 null,
	[EpisodicCCGCode] [varchar](10) NOT NULL,
	[EpisodicCCGName] [varchar](250) NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[PatientPostCode] [varchar](25) NULL,
	PatientForename varchar(30) null,
	PatientSurname varchar(30) null,
	PatientAge int null
	Sex varchar(80) null,
	Title varchar(80) null,
	DateOfBirth date null,
	DateOfDeath date null,
	AddressLine1 varchar(50) null,
	AddressLine2 varchar(50) null,
	AddressLine3 varchar(50) null,
	AddressLine4 varchar(50) null,
	ActivityInLastYear int null,
	AgeOfReferralCode int null,
		
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO*/
truncate table COM.Referrals
insert into COM.Referrals

([PatientID],
	[NHSNumber],
	[FacilityID] ,
	[ReferralID],
	[ReferralRequestReceivedDateTime],
	[ReferralRequestReceivedDate],
	[ReferralRequestReceivedTime],
	[CodeOfCommissioner] ,
	[CIDServiceTypeReferredTo],
	[ReferringOrganisationCode],
	[ReferringCareProfessionalStaffGroup],
	[Priority],
	[ReferralClosureDate],
	[ReferralClosureReason],
	[DischargeDate],
	[ReceivingProfCarer],
	[ReferralCreatedDate],
	[ReferredToDivision],
	[ReferredToDirectorate],
	[ReferringOrganisationName],
	[ReferrerCode],
	[ReferrerName],
	[ReferralCreatedBy],
	Authorised,
	ReferralStatus,
	[EpisodicGPCode],
	[EpisodicGPName],
	[EpisodicGPPracticeCode],
	[EpisodicGPPracticeName],
	[ReferredToSpecialtyCode],
	[ReferredToSpecialty],
	[SourceOfReferral],
	SourceOfReferralDescription,
	[EpisodicCCGCode],
	[EpisodicCCGName],
	[ModifiedDate],
	[PatientPostCode],
	PatientForename,
	PatientSurname,
	PatientAge,
	Sex,
	Title,
	DateOfBirth,
	DateOfDeath,
	AddressLine1,
	AddressLine2,
	AddressLine3,
	AddressLine4,
	ActivityInLastYear,
	AgeOfReferralCode,
	ReferredToServiceTeamReferralNo,
	RStatRefNo,
    AuthorisedDate,
    RjectRefNo
    )
	
	select [SourcePatientNo]
      ,[NHSNumber]
      ,[FacilityID]
      ,[ReferralID]
      ,[ReferralRequestReceivedDateTime]
      ,[ReferralRequestReceivedDate]
      ,[ReferralRequestReceivedTime]
      ,[CodeOfCommissioner]
      ,[CIDServiceTypeReferredTo]
      ,[ReferringOrganisationCode]
      ,[ReferringCareProfessionalStaffGroup]
      ,[Priority]
      ,[ReferralClosureDate]
      ,[ReferralClosureReason]
      ,[DischargeDate]
      ,[ReceivingProfCarer]
      ,[ReferralCreatedDate]
      ,[ReferredToDivision]
      ,[ReferredToDirectorate]
      ,[ReferringOrganisationName]
      ,[ReferrerCode]
      ,[ReferrerName]
      ,[ReferralCreatedBy]
      ,Authorised
      ,ReferralStatus
      ,[EpisodicGPCode]
      ,[EpisodicGPName]
      ,[EpisodicGPPracticeCode]
      ,[EpisodicGPPracticeName]
      ,[ReferredToSpecialtyCode]
      ,[ReferredToSpecialty]
      ,[SourceOfReferral]
      ,SourceOfReferralDescription
      ,[EpisodeicCCGCode]
      ,[EpisodeicCCGName]
      ,[ModifiedDate]
      ,[PatientPostCode]
      ,[PatientForename]
      ,[PatientSurname]
      ,PatientAge
      ,[Sex]
      ,[Title]
      ,[DateOfBirth]
      ,[DateOfDeath]
      ,[AddressLine1]
      ,[AddressLine2]
      ,[AddressLine3]
      ,[AddressLine4]
      ,0
      ,AgeOfReferralCode
      ,ReferredToServiceTeamReferralNo
      ,RStatRefNo
      ,AuthorisedDate
      ,RjectRefNo
       from [uhsm-is1].Community.COM.referrals
	
	update

COM.referrals

set ReferredTospecialty=coalesce((select DISTINCT coalesce(IPMDescription,'Unknown') from com.lkSpecialtyDivision reftosd where reftosd.SPECT_REFNO=COM.referrals.ReferredToSpecialtyCode),'Unknown')

update

COM.Referrals

set ReferredToDivision = coalesce((select DISTINCT coalesce(Division,'Unknown') 
from com.lkSpecialtyDivision reftosd 
where reftosd.SPECT_REFNO=COM.Referrals.ReferredToSpecialtyCode),'Unknown')

update

COM.Referrals

set ReferredToDirectorate=coalesce((select DISTINCT coalesce(Directorate,'Unknown') from com.lkSpecialtyDivision reftosd where reftosd.SPECT_REFNO=COM.Referrals.ReferredToSpecialtyCode),'Unknown')


update COM.Referrals

set EpisodicCCGCode=coalesce((select CCG FROM organisationccg.dbo.[CCG Practice PCT Lookup] CCGPLk where CCGPLk.Practice=COM.Referrals.episodicGPPracticeCode),
(select [PCG of Residence] from OrganisationCCG.dbo.[Extended Postcode] ccgpostcode where ccgpostcode.postcode=COM.Referrals.PatientPostCode))


update COM.Referrals

set EpisodicCCGName=(select distinct [CCG Name] from OrganisationCCG.dbo.[CCG Practice PCT Lookup] ccglk where ccglk.CCG=COM.Referrals.EpisodicCCGCode)