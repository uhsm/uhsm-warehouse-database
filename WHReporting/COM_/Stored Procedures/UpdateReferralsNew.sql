﻿/*
--Author: J Cleworth
--Date created: 25/07/2013
--KO updated 19/02/2015 to remove intermediate step
*/

CREATE Procedure [COM].[UpdateReferralsNew]
as

truncate table COM.Referrals
insert into COM.Referrals

([PatientID],
	[NHSNumber],
	[FacilityID] ,
	[ReferralID],
	[ReferralRequestReceivedDateTime],
	[ReferralRequestReceivedDate],
	[ReferralRequestReceivedTime],
	[CodeOfCommissioner] ,
	[CIDServiceTypeReferredTo],
	[ReferringOrganisationCode],
	[ReferringCareProfessionalStaffGroup],
	[Priority],
	[ReferralClosureDate],
	[ReferralClosureReason],
	[DischargeDate],
	[ReceivingProfCarer],
	[ReferralCreatedDate],
	[ReferredToDivision],
	[ReferredToDirectorate],
	[ReferringOrganisationName],
	[ReferrerCode],
	[ReferrerName],
	[ReferralCreatedBy],
	Authorised,
	ReferralStatus,
	[EpisodicGPCode],
	[EpisodicGPName],
	[EpisodicGPPracticeCode],
	[EpisodicGPPracticeName],
	[ReferredToSpecialtyCode],
	[ReferredToSpecialty],
	[SourceOfReferral],
	SourceOfReferralDescription,
	[EpisodicCCGCode],
	[EpisodicCCGName],
	[ModifiedDate],
	[PatientPostCode],
	PatientForename,
	PatientSurname,
	PatientAge,
	Sex,
	Title,
	DateOfBirth,
	DateOfDeath,
	AddressLine1,
	AddressLine2,
	AddressLine3,
	AddressLine4,
	ActivityInLastYear,
	AgeOfReferralCode
	)
	(select distinct
	SourcePatientNo = Referral.PATNT_REFNO
	,NHSNumber = Patient.NHS_IDENTIFIER
	,FacilityID = DistrictNo.IDENTIFIER
	,ReferralID = Referral.REFRL_REFNO
	,ReferralRequestReceivedDateTime = Referral.RECVD_DTTM
	,ReferralRequestReceivedDate = cast(Referral.RECVD_DTTM as date)
	,ReferralRequestReceivedTime = cast(Referral.RECVD_DTTM as time)
	,CodeOfCommissioner=CASE WHEN ccg.[parent organisation code] is not null
						THEN ccg.[parent organisation code] +'00'
						WHEN ccg.[parent organisation code] is NULL
						THEN rtrim(ng.[ccg]) + '00'
						WHEN ng.[ccg] IS NULL
						THEN '01N00' --South Manchester CCG
						ELSE '01N00' end
	,CIDServiceTypeReferredTo=CID.CID_CODE
	,ReferringOrganisationCode=he.[MAIN_IDENT]
	,ReferringCareProfessionalStaffGroup=ahp.National_Code
	,Priority=CASE WHEN prity_refno in('3127','2000632')
						THEN '1'
						WHEN prity_refno ='3128'
						THEN '2'
						WHEN prity_refno ='5478'
						THEN '3'
						ELSE 'NULL' end
	,ReferralClosureDate=referral.[CLOSR_DATE]		
	,ReferralClosureReason=rclose.national_code
	,DischargeDate=referral.[CLOSR_DATE]
	,ReceivingProfCarer=isnull(profcarer.surname,'')+', '+isnull(profcarer.forename,'')
	,ReferralCreatedDate=referral.CREATE_DTTM	
	,ReferredToDivision=''--reftosd.Division
	,ReferredToDirectorate=''--reftosd.Directorate
	,ReferringOrganisationName=he.[Description]
	,ReferrerCode=refbypcarer.[MAIN_IDENT]
	,ReferrerName=isnull(refbypcarer.[SURNAME],'') + ',' + isnull(refbypcarer.[FORENAME],'')
	,ReferralCreatedBy=Referral.USER_CREATE
	,Authorised=rvauth.MAIN_CODE
	,ReferralStatus=rvstatus.MAIN_CODE
	,EpisodicGPCode=pcarerlk.MAIN_IDENT
	,EpisodicGPName=isnull(pcarerlk.SURNAME,'')+', '+isnull(pcarerlk.FORENAME,'') 
	,EpisodicGPPracticeCode=gporg.MAIN_IDENT
	,EpisodicGPPracticeName=EpisodicGPPrac.[Description]
	,ReferredToSpecialtyCode = Referral.REFTO_SPECT_REFNO
	,ReferredToSpecialty=''--SpecialtyTo.[DESCRIPTION]
	,SourceOfReferral=sorrf.National_code
	,SourceOfReferralDescription=sorrf.[description]
	,EpisodeicCCGCode=''--coalesce(ccgpractice.CCG,ccgpostcode.[PCG of Residence],'unk')
	,EpisodeicCCGName=''--coalesce(ccgpractice.ccg,lkccgname.ccg,'unk')
	,ModifiedDate=Referral.MODIF_DTTM
	,PatientPostCode=ptaddress.pcode
	,PatientForename=Patient.FORENAME
	,PatientSurname=Patient.SURNAME
	,dbo.GetAgeOnSpecifiedDate(CAST(Patient.DTTM_OF_BIRTH AS Datetime),CAST(Referral.RECVD_DTTM AS Datetime)) AS PatientAge
	,Sex=sexrefval.Description
	,Title=titlerefval.Description
	,DateOfBirth=patient.DATE_OF_BIRTH
	,DateOfDeath=DATE_OF_DEATH
	,AddressLine1=ptaddress.LINE1
	,AddressLine2=ptaddress.LINE2
	,AddressLine3=ptaddress.LINE3
	,AddressLine4=ptaddress.LINE4
	--,ArchiveFlag=Referral.ARCHV_FLAG
	,0
	,AgeOfReferralCode=case when cast(RECVD_DTTM as DATE) between DATEADD(year,-1,cast(getdate() as date)) and dateadd(day,-1,cast(getdate()as date))
	then 1
	when cast(RECVD_DTTM as DATE) > DATEADD(year,-2,cast(getdate() as date)) 
	then 2
	when cast(RECVD_DTTM as DATE) > DATEADD(year,-3,cast(getdate() as date)) 
	then 3
	when cast(RECVD_DTTM as DATE) > DATEADD(year,-4,cast(getdate() as date)) 
	then 4
	when cast(RECVD_DTTM as DATE) > DATEADD(year,-5,cast(getdate() as date)) 
	then 5
	when cast(RECVD_DTTM as DATE) > DATEADD(year,-6,cast(getdate() as date)) 
	then 6
	else 7 end
--into dbo.Referrals
from 
	[Community].[PAS].REFERRALS_VE07_RM2 Referral
left join Community.PAS.PATIENTS_VE05_RM2 Patient
on	Patient.PATNT_REFNO = Referral.PATNT_REFNO  
LEFT JOIN (

      SELECT
            ar.PATNT_REFNO,
            a.PCODE,
            a.LINE1,
            a.LINE2,
            a.LINE3,
            a.line4
      FROM
            Community.PAS.ADDRESS_ROLES_VE02 ar
      LEFT JOIN Community.PAS.ADDRESSES_VE02 a
            ON ar.ADDSS_REFNO = a.ADDSS_REFNO
      WHERE
                  ar.ARCHV_FLAG = 'N'
            AND
                  ar.ROTYP_CODE = 'HOME'
            AND
                  ar.END_DTTM IS NULL
            AND
                  a.ADTYP_CODE = 'POSTL'
            AND
                  a.ARCHV_FLAG = 'N'
            AND
                  a.END_DTTM IS NULL
                  
           and ar.ADDSS_REFNO in (Select MAX(ar2.ADDSS_REFNO)FROM Community.PAS.ADDRESS_ROLES_VE02 ar2
      LEFT JOIN Community.PAS.ADDRESSES_VE02 a2
            ON ar2.ADDSS_REFNO = a2.ADDSS_REFNO
      WHERE
                  ar2.ARCHV_FLAG = 'N'
            AND
                  ar2.ROTYP_CODE = 'HOME'
            AND
                  ar2.END_DTTM IS NULL
            AND
                  a2.ADTYP_CODE = 'POSTL'
            AND
                  a2.ARCHV_FLAG = 'N'
            AND
                  a2.END_DTTM IS NULL
          
          
           GROUP BY PATNT_REFNO)       
) ptaddress
      ON patient.PATNT_REFNO = ptaddress.PATNT_REFNO 


    
left join Community.PAS.PATIENT_IDS_VE01 DistrictNo
on	DistrictNo.PATNT_REFNO = Patient.PATNT_REFNO
and	DistrictNo.PITYP_REFNO = 2001232
and	DistrictNo.IDENTIFIER like '5NT%'
and	DistrictNo.ARCHV_FLAG = 'N'
and	not exists
	(
	select
		1
	from
		Community.PAS.PATIENT_IDS_VE01 Previous
	where
		Previous.PATNT_REFNO = DistrictNo.PATNT_REFNO
	and	Previous.PITYP_REFNO = 2001232
	and	Previous.IDENTIFIER like '5NT%'
	and	Previous.ARCHV_FLAG = 'N'
	and	(
			Previous.START_DTTM > DistrictNo.START_DTTM
		or
			(
				Previous.START_DTTM = DistrictNo.START_DTTM
			and	Previous.PATID_REFNO > DistrictNo.PATID_REFNO
			)
		)
	)


left join Community.PAS.PATIENT_PROF_CARERS_VE01 RegisteredGp
on	RegisteredGp.PATNT_REFNO = Referral.PATNT_REFNO
and	RegisteredGp.PRTYP_REFNO = 1132
and	RegisteredGp.ARCHV_FLAG = 'N'
and	Referral.RECVD_DTTM between 
		RegisteredGp.START_DTTM 
	and coalesce(
			RegisteredGp.END_DTTM
			,Referral.RECVD_DTTM
		)
and	not exists
	(
	select
		1
	from
		Community.PAS.PATIENT_PROF_CARERS_VE01 Previous
	where
		Previous.PATNT_REFNO = RegisteredGp.PATNT_REFNO
	and	Previous.PRTYP_REFNO = RegisteredGp.PRTYP_REFNO
	and	Previous.ARCHV_FLAG = 'N'
	and	Referral.RECVD_DTTM between 
			Previous.START_DTTM 
		and coalesce(
				Previous.END_DTTM
				,Referral.RECVD_DTTM
			)
	and	(
			Previous.START_DTTM > RegisteredGp.START_DTTM
		or	(
				Previous.START_DTTM = RegisteredGp.START_DTTM
			and	Previous.PATPC_REFNO > RegisteredGp.PATPC_REFNO
			)
		)
	)
	
left join 
Community.PAS.PROF_CARERS_VE01 pcarerlk on pcarerlk.PROCA_REFNO=registeredGP.PROCA_REFNO


left join  Community.PAS.HEALTH_ORGANISATIONS_VE01 episodicGPPrac on EpisodicGPPrac.HEORG_REFNO =registeredGP.HEORG_REFNO


left join Community.PAS.PATIENT_PROF_CARERS_VE01 DefaultRegisteredGp
on	DefaultRegisteredGp.PATNT_REFNO = Referral.PATNT_REFNO
and	DefaultRegisteredGp.PATNT_REFNO = 1132
and	DefaultRegisteredGp.ARCHV_FLAG = 'N'
and	Referral.RECVD_DTTM < DefaultRegisteredGp.START_DTTM
and	not exists
	(
	select
		1
	from
		Community.PAS.PATIENT_PROF_CARERS_VE01 Previous
	where
		Previous.PATNT_REFNO = DefaultRegisteredGp.PATNT_REFNO
	and	Previous.PRTYP_REFNO = DefaultRegisteredGp.PRTYP_REFNO
	and	Previous.ARCHV_FLAG = 'N'
	and	Referral.RECVD_DTTM < Previous.START_DTTM
	and	(
			Previous.START_DTTM > DefaultRegisteredGp.START_DTTM
		or	(
				Previous.START_DTTM = DefaultRegisteredGp.START_DTTM
			and	Previous.PATPC_REFNO > DefaultRegisteredGp.PATPC_REFNO
			)
		)
	)
				

LEFT JOIN Community.Common.NACSgridall ng
   ON REPLACE(ptaddress.PCODE, ' ', '')COLLATE DATABASE_DEFAULT = REPLACE(ng.Postcode, ' ', '')COLLATE DATABASE_DEFAULT


LEFT JOIN Community.PAS.HEALTH_ORGANISATIONS_VE01 gporg on gporg.HEORG_REFNO=RegisteredGp.HEORG_REFNO


LEFT JOIN Community.Common.[General Medical Practice] gmp                                                                         
      ON gporg.MAIN_IDENT COLLATE DATABASE_DEFAULT = gmp.[Organisation Code]  COLLATE DATABASE_DEFAULT
      
      
      
LEFT JOIN Community.Common.[Clinical Commissioning Group practice] ccg             
      ON gmp.[parent organisation code] = ccg.[organisation code] 


left join Community.lk.CID CID on CID.SPECT_REFNO=Referral.REFTO_SPECT_REFNO

 left join  Community.PAS.SPECIALTIES_VE01 SpecialtyTo
 on referral.refto_spect_refno =SpecialtyTo.spect_refno

 left join  Community.PAS.SPECIALTIES_VE01 SpecialtyFrom
 on referral.refby_spect_refno =SpecialtyTo.spect_refno

left join Community.lk.Sorrf sorrf on sorrf.sorrf_refno=referral.sorrf_refno

left join  Community.PAS.[PROF_CARERS_VE01] pcarer
  on referral.refby_proca_refno = pcarer.proca_refno
left join Community.PAS.REFERENCE_VALUES_VE01 refval
  on pcarer.prtyp_refno = refval.rfval_refno
 left join Community.lk.AHP ahp on ahp.PRTYP_REFNO=refval.rfval_refno
  
 left join Community.PAS.PROF_CARERS_VE01 profcarer
 on refto_proca_refno = profcarer.proca_refno
 
  left join Community.PAS.PROF_CARERS_VE01 refbypcarer
 on referral.refby_proca_refno = refbypcarer.proca_refno
 
  left join Community.PAS.HEALTH_ORGANISATIONS_VE01 he
 on referral.refby_heorg_refno = he.heorg_refno 

 
 left join Community.lk.ReferralClosure rclose on rclose.CLORS_REFNO=referral.CLORS_REFNO
 
 left join Community.PAS.REFERENCE_VALUES_VE01 sexrefval
  on Patient.SEXXX_REFNO = sexrefval.rfval_refno
 
 left join Community.PAS.REFERENCE_VALUES_VE01 titlerefval
  on Patient.TITLE_REFNO = titlerefval.rfval_refno
 
left join Community.PAS.REFERENCE_VALUES_VE01 rvauth

on Referral.AUTHR_REFNO=rvauth.RFVAL_REFNO

left join Community.PAS.REFERENCE_VALUES_VE01 rvstatus

on Referral.RSTAT_REFNO=rvstatus.RFVAL_REFNO


 where Referral.ARCHV_FLAG='N'
 AND not exists (select 1 FROM Community.PAS.IPMTestPatients where IPMTestPatients.PATNT_REFNO=referral.PATNT_REFNO) 
 )
	--select [SourcePatientNo]
 --     ,[NHSNumber]
 --     ,[FacilityID]
 --     ,[ReferralID]
 --     ,[ReferralRequestReceivedDateTime]
 --     ,[ReferralRequestReceivedDate]
 --     ,[ReferralRequestReceivedTime]
 --     ,[CodeOfCommissioner]
 --     ,[CIDServiceTypeReferredTo]
 --     ,[ReferringOrganisationCode]
 --     ,[ReferringCareProfessionalStaffGroup]
 --     ,[Priority]
 --     ,[ReferralClosureDate]
 --     ,[ReferralClosureReason]
 --     ,[DischargeDate]
 --     ,[ReceivingProfCarer]
 --     ,[ReferralCreatedDate]
 --     ,[ReferredToDivision]
 --     ,[ReferredToDirectorate]
 --     ,[ReferringOrganisationName]
 --     ,[ReferrerCode]
 --     ,[ReferrerName]
 --     ,[ReferralCreatedBy]
 --     ,Authorised
 --     ,ReferralStatus
 --     ,[EpisodicGPCode]
 --     ,[EpisodicGPName]
 --     ,[EpisodicGPPracticeCode]
 --     ,[EpisodicGPPracticeName]
 --     ,[ReferredToSpecialtyCode]
 --     ,[ReferredToSpecialty]
 --     ,[SourceOfReferral]
 --     ,SourceOfReferralDescription
 --     ,[EpisodeicCCGCode]
 --     ,[EpisodeicCCGName]
 --     ,[ModifiedDate]
 --     ,[PatientPostCode]
 --     ,[PatientForename]
 --     ,[PatientSurname]
 --     ,PatientAge
 --     ,[Sex]
 --     ,[Title]
 --     ,[DateOfBirth]
 --     ,[DateOfDeath]
 --     ,[AddressLine1]
 --     ,[AddressLine2]
 --     ,[AddressLine3]
 --     ,[AddressLine4]
 --     ,0
 --     ,AgeOfReferralCode
 --      from Community.COM.referrals
	
	update

COM.referrals

set ReferredTospecialty=coalesce((select DISTINCT coalesce(IPMDescription,'Unknown') from com.lkSpecialtyDivision reftosd where reftosd.SPECT_REFNO=COM.referrals.ReferredToSpecialtyCode),'Unknown')

update

COM.Referrals

set ReferredToDivision = coalesce((select DISTINCT coalesce(Division,'Unknown') 
from com.lkSpecialtyDivision reftosd 
where reftosd.SPECT_REFNO=COM.Referrals.ReferredToSpecialtyCode),'Unknown')

update

COM.Referrals

set ReferredToDirectorate=coalesce((select DISTINCT coalesce(Directorate,'Unknown') from com.lkSpecialtyDivision reftosd where reftosd.SPECT_REFNO=COM.Referrals.ReferredToSpecialtyCode),'Unknown')


update COM.Referrals

set EpisodicCCGCode=coalesce((select CCG FROM organisationccg.dbo.[CCG Practice PCT Lookup] CCGPLk where CCGPLk.Practice=COM.Referrals.episodicGPPracticeCode),
(select [PCG of Residence] from OrganisationCCG.dbo.[Extended Postcode] ccgpostcode where ccgpostcode.postcode=COM.Referrals.PatientPostCode))


update COM.Referrals

set EpisodicCCGName=(select distinct [CCG Name] from OrganisationCCG.dbo.[CCG Practice PCT Lookup] ccglk where ccglk.CCG=COM.Referrals.EpisodicCCGCode)