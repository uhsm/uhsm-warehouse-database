﻿/*
--Author: J Cleworth
--Date created: 25/07/2013

*/

CREATE Procedure [COM].[UpdateSchedules]
as

/*CREATE TABLE [dbo].[Schedules](
	[NHSnumber] [varchar](10) NULL,
	[PatientID] [varchar](20) NULL,
	[FacilityID] [varchar](20) NULL,
	[ReferralID] [varchar](20) NULL,
	[ScheduleID] [numeric](10, 0) NOT NULL,
	[Age] [int] NULL,
	[AppointmentDate] [date] NULL,
	[AppointmentTime] [time](7) NULL,
	[AppointmentDateTime] [datetime] NULL,
	[AppointmentDuration] [int] NULL,
	[AppointmentType] [int] NOT NULL,
	[CareContactSubjectCode] [varchar](10) NOT NULL,
	[ConsultantMediumUsedNationalCode] [varchar](10) NULL,
	[ActivityLocationTypeCode] [nvarchar](10) NULL,
	[SiteCodeOfTreatment] [varchar](10) NULL,
	[AttendedOrDidNotAttendCode] [varchar](10) NULL,
	AttendNationalCode varchar (10) null,
	Outcome varchar (10) null
	[CareProfessionalStaffGroupCommunityCareNationalCode] [nvarchar](10) NULL,
	[EarliestReasonableOfferDate] [datetime] NULL,
	[CancellationDate] [datetime] NULL,
	[CancellationReason] [nvarchar](100) NULL,
	[EarliestClinicallyAppropriateDate] datetime null,
	ReplacementAppointmentBookedDateCommunityCare datetime null,
	ReplacementAppointmentDateOfferedCommunityCare datetime null,
	[ActivityType] [varchar](10) NULL,
	[GroupTherapyIndicatorCommunityCare] [varchar](10) NULL,
	[ProfessionalCarer] [varchar](61) NULL,
	SeenByProfessionalCarer varchar (70) null
	[AppointmentSpecialty] [nvarchar](200) NULL,
	[AppointmentSpecialtyDivision] [nvarchar](200) NULL,
	[AppointmentSpecialtyDirectorate] [nvarchar](200) NULL,
	[AppointmentArrivalDateTime] [datetime] NULL,
	[AppointmentSeenDateTime] [datetime] NULL,
	[AppointmentDepartedDateTime] [datetime] NULL,
	[AppointmentCreatedBy] [varchar](35) NULL,
	[AppointmentCreatedDate] [datetime] NULL,
	[ClinicAttendanceStatus] [varchar](80) NULL,
	[AppointmentOutcome] [varchar](80) NULL,
	[ClinicDescription] [varchar](200) NULL,
	[ClinicCode] [varchar](20) NULL,
	[ActivityTypeMain] [varchar](19) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [pk_CommSchedule] PRIMARY KEY CLUSTERED 
(
	[ScheduleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]*/


Truncate Table [COM].[Schedules]


insert into [COM].[Schedules]

([NHSnumber],
	[PatientID],
	[FacilityID],
	[ReferralID],
	[ScheduleID],
	[Age],
	[AppointmentDate],
	[AppointmentTime],
	[AppointmentDateTime],
	[AppointmentDuration],
	[AppointmentType],
	[CareContactSubjectCode],
	[ConsultantMediumUsedNationalCode],
	[ActivityLocationTypeCode],
	[SiteCodeOfTreatment],
	[AttendedOrDidNotAttendCode],
	AttendNationalCode,
	Outcome,
	[CareProfessionalStaffGroupCommunityCareNationalCode],
	[EarliestReasonableOfferDate],
	[CancellationDate],
	[CancellationReason],
	[EarliestClinicallyAppropriateDate],
	ReplacementAppointmentBookedDateCommunityCare,
	ReplacementAppointmentDateOfferedCommunityCare,
	[ActivityType],
	[GroupTherapyIndicatorCommunityCare],
	[ProfessionalCarer],
	SeenByProfessionalCarer,
	OtherCarerPresent,
	[AppointmentSpecialty],
	[AppointmentSpecialtyDivision],
	[AppointmentSpecialtyDirectorate],
	[AppointmentArrivalDateTime],
	[AppointmentSeenDateTime],
	[AppointmentDepartedDateTime],
	[AppointmentCreatedBy],
	[AppointmentCreatedDate],
	[ClinicAttendanceStatus],
	[AppointmentOutcome],
	[ClinicDescription],
	[ClinicCode],
	[ActivityTypeMain],
	[ModifiedDate],
	[ContactType])
	
	select distinct
	
	[NHSnumber],
	[PatientID],
	[FacilityID],
	[ReferralID],
	[ScheduleID],
	[Age],
	[AppointmentDate],
	[AppointmentTime],
	[AppointmentDateTime],
	[AppointmentDuration],
	[AppointmentType],
	[CareContactSubjectCode],
	[ConsultantMediumUsedNationalCode],
	[ActivityLocationTypeCode],
	[SiteCodeOfTreatment],
	[AttendedOrDidNotAttendCode],
	AttendNationalCode,
	Outcome,
	[CareProfessionalStaffGroupCommunityCareNationalCode],
	[EarliestReasonableOfferDate],
	[CancellationDate],
	[CancellationReason],
	[EarliestClinicallyAppropriateDate],
	ReplacementAppointmentBookedDateCommunityCare,
	ReplacementAppointmentDateOfferedCommunityCare,
	[ActivityType],
	[GroupTherapyIndicatorCommunityCare],
	[ProfessionalCarer],
	SeenByProfessionalCarer,
	OtherCarerPresent,
	[AppointmentSpecialty],
	[AppointmentSpecialtyDivision],
	[AppointmentSpecialtyDirectorate],
	[AppointmentArrivalDateTime],
	[AppointmentSeenDateTime],
	[AppointmentDepartedDateTime],
	[AppointmentCreatedBy],
	[AppointmentCreatedDate],
	[ClinicAttendanceStatus],
	[AppointmentOutcome],
	[ClinicDescription],
	[ClinicCode],
	[ActivityTypeMain],
	[ModifiedDate],
	[ContactType]
	
	from [uhsm-is1].community.COM.schedules
	
	
	/*update

dbo.schedules

set AppointmentSpecialty=coalesce((select DISTINCT coalesce(IPMDescription,'Unknown') 
from lk.SpecialtyDivision reftosd 
where reftosd.SPECT_REFNO=dbo.schedules.AppointmentSpecialtyCode),'Unknown')

update

dbo.schedules

set AppointmentSpecialtyDivision = coalesce((select DISTINCT coalesce(Division,'Unknown') 
from lk.SpecialtyDivision reftosd 
where reftosd.SPECT_REFNO=dbo.schedules.AppointmentSpecialtyCode),'Unknown')

update

dbo.Schedules

set AppointmentSpecialtyDirectorate=coalesce((select DISTINCT coalesce(Directorate,'Unknown') 
from lk.SpecialtyDivision reftosd 
where reftosd.SPECT_REFNO=dbo.schedules.AppointmentSpecialtyCode),'Unknown')*/
	
	
update COM.Referrals

set ActivityInLastYear=1 where exists
(Select 1 from WHREPORTING.[COM].[vwActivityInLast12Months] where ReferralID=COM.Referrals.ReferralID)