﻿/*
--Author: J Cleworth
--Date created: 25/07/2013

*/

CREATE Procedure [COM].[UpdateSchedulesPatient]
as

/*CREATE TABLE [dbo].[SchedulesPatient](
	[NHSnumber] [varchar](10) NULL,
	[PatientID] [varchar](20) NULL,
	[FacilityID] [varchar](20) NULL,
	[ScheduleID] [numeric](10, 0) NOT NULL,
	[EpisodicGPCode] [varchar](10) NULL,
	[EpisodicGPName] [varchar](100) NULL,
	[EpisodicGPPracticeCode] [varchar](10) NULL,
	[EpisodicGPPracticeName] [varchar](255) NULL,
	[EpisodicCCGCode] [varchar](5) NOT NULL,
	[EpisodicCCGName] [varchar](100) NOT NULL,
	[NHSNumberStatus] [varchar](13) NOT NULL,
	[PatientForename] [varchar](30) NULL,
	[PatientSurname] [varchar](30) NULL,
	[PatientDateOfBirth] [date] NULL,
	[PatientDeathDate] [date] NULL,
	[PatientPostCode] [varchar](25) NULL,
	[ActualDeathLocationType] [varchar](80) NULL,
 CONSTRAINT [pk_CommSchedulePatient] PRIMARY KEY CLUSTERED 
(
	[ScheduleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO*/


Truncate Table [COM].[SchedulesPatient]


insert into [COM].[SchedulesPatient]

([NHSnumber]
      ,[PatientID]
      ,[FacilityID]
      ,[ScheduleID]
      ,[EpisodicGPCode]
      ,[EpisodicGPName]
      ,[EpisodicGPPracticeCode]
      ,[EpisodicGPPracticeName]
      ,[EpisodicCCGCode]
      ,[EpisodicCCGName]
      ,[NHSNumberStatus]
      ,[PatientForename]
      ,[PatientSurname]
      ,[PatientDateOfBirth]
      ,[PatientDeathDate]
      ,[PatientPostCode]
      ,[ActualDeathLocationType])
	
	select distinct
	
	[NHSnumber]
      ,[PatientID]
      ,[FacilityID]
      ,[ScheduleID]
      ,[EpisodicGPCode]
      ,[EpisodicGPName]
      ,[EpisodicGPPracticeCode]
      ,[EpisodicGPPracticeName]
      ,[EpisodicCCGCode]
      ,[EpisodicCCGName]
      ,[NHSNumberStatus]
      ,[PatientForename]
      ,[PatientSurname]
      ,[PatientDateOfBirth]
      ,[PatientDeathDate]
      ,[PatientPostCode]
      ,[ActualDeathLocationType]
	
	from [uhsm-is1].community.COM.schedules
	
	
	
  
update COM.SchedulesPatient

set EpisodicCCGCode=coalesce((select CCG FROM organisationccg.dbo.[CCG Practice PCT Lookup] CCGPLk where CCGPLk.Practice=COM.SchedulesPatient.episodicGPPracticeCode),
(select [PCG of Residence] from OrganisationCCG.dbo.[Extended Postcode] ccgpostcode where ccgpostcode.postcode=COM.SchedulesPatient.PatientPostCode))


update COM.SchedulesPatient

set EpisodicCCGName=(select distinct [CCG Name] from OrganisationCCG.dbo.[CCG Practice PCT Lookup] ccglk where ccglk.CCG=COM.SchedulesPatient.EpisodicCCGCode)