﻿/*
--Author: J Cleworth
--Date created: 12/08/2013

*/

CREATE Procedure [COM].[UpdateSecondaryCarer]
as

/*CREATE TABLE [COM].[SecondaryCarer](
	[ScheduleID] [numeric](10, 0) NULL,
	[ScheduleChildID] [numeric](10, 0) NOT NULL,
	[OtherCarerPresentName] [varchar](61) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO*/


Truncate Table [COM].[SecondaryCarer]


insert into [COM].[SecondaryCarer]

([ScheduleID]
      ,[ScheduleChildID]
      ,[OtherCarerPresentName]
   )
	
	select distinct
	
	[ScheduleID]
      ,[ScheduleChildID]
      ,[OtherCarerPresentName]
	
	from [uhsm-is1].community.COM.SecondaryCarer