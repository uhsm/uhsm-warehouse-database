﻿/*
--Author: J Cleworth
--Date created: 25/07/2013

*/

CREATE Procedure [COM].[UpdateSpells]
as

/*CREATE TABLE [dbo].[Spells](
	[NHSNumber] [varchar](20) NULL,
	[FacilityID] [varchar](20) NULL,
	[PatientID] [numeric](10, 0) NULL,
	[AgeOnAdmission] [int] NULL,
	[ProviderEncounterID] [numeric](10, 0) NOT NULL,
	[ReferralID] [numeric](10, 0) NULL,
	[Specialty] [varchar](200) NULL,
	[AdmissionMethodCode] [varchar](25) NULL,
	[WardName] [varchar](25) NULL,
	[AdmissionsSource] [varchar](25) NULL,
	[DischargeDestination] [varchar](25) NULL,
	[DischargeMethod] [varchar](25) NULL,
	[AdmissionDate] [datetime] NULL,
	[DischargeDate] [datetime] NULL,
	[SpellLengthOfStay] [int] NULL,
	[SpellCreateDate] [datetime] NULL,
	[SpellCreatedBy] [varchar](35) NULL,
	[EpisodicGPCode] [varchar](25) NULL,
	[EpisodicGPName] [varchar](62) NULL,
	[EpisodicGPPracticeCode] [varchar](25) NULL,
	[EpisodicGPPracticeName] [varchar](255) NULL,
	[EpisodicCCGCode] varchar (10) null,
    [EpisodicCCGName] varchar (255) null,
	Postcode varchar (20) null,
 CONSTRAINT [pk_CommSpells] PRIMARY KEY CLUSTERED 
(
	[ProviderEncounterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO*/
truncate table [COM].[Spells]
insert into [COM].[Spells]

([NHSNumber]
,[FacilityID]
,[PatientID]
,[AgeOnAdmission]
,[ProviderEncounterID]
,[ReferralID]
,[Specialty]
,[AdmissionMethodCode]
,[WardName]
,[AdmissionsSource]
,[DischargeDestination]
,[DischargeMethod]
,[AdmissionDate]
,[AdmissionDateTime]
,[DischargeDate]
,[DischargeDateTime]
,[SpellLengthOfStay]
,[SpellCreateDate]
,[SpellCreatedBy]
,[EpisodicGPCode]
,[EpisodicGPName]
,[EpisodicGPPracticeCode]
,[EpisodicGPPracticeName]
,Postcode)

select 

Spells.[NHSNumber]
,Spells.[FacilityID]
,Spells.[PatientID]
,Spells.[AgeOnAdmission]
,Spells.[ProviderEncounterID]
,Spells.[ReferralID]
,Spells.[Specialty]
,Spells.[AdmissionMethodCode]
,Spells.[WardName]
,Spells.[AdmissionsSource]
,Spells.[DischargeDestination]
,Spells.[DischargeMethod]
,Spells.[AdmissionDate]
,Spells.[AdmissionDateTime]
,Spells.[DischargeDate]
,Spells.[DischargeDateTime]
,Spells.[SpellLengthOfStay]
,Spells.[SpellCreateDate]
,Spells.[SpellCreatedBy]
,Spells.[EpisodicGPCode]
,Spells.[EpisodicGPName]
,Spells.[EpisodicGPPracticeCode]
,Spells.[EpisodicGPPracticeName]
,Spells.Postcode

from [uhsm-is1].community.COM.Spells Spells




update COM.Spells

set EpisodicCCGCode=coalesce((select CCG FROM organisationccg.dbo.[CCG Practice PCT Lookup] CCGPLk where CCGPLk.Practice=COM.spells.episodicGPPracticeCode),
(select [PCG of Residence] from OrganisationCCG.dbo.[Extended Postcode] ccgpostcode where ccgpostcode.postcode=COM.spells.PostCode))


update COM.Spells

set EpisodicCCGName=(select distinct [CCG Name] from OrganisationCCG.dbo.[CCG Practice PCT Lookup] ccglk where ccglk.CCG=COM.spells.EpisodicCCGCode)