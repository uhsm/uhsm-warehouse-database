﻿/*
--Author: K Oakden
--Date created: 08/01/2015

*/

CREATE Procedure [COM].[UpdateWardStay]
as

--drop table COM.WardStay
truncate table COM.WardStay
insert into COM.WardStay
(
	NHSNumber
	,FacilityID
	,PatientID
	,AgeOnAdmission
	,ProviderSpellID
	,WardStayID
	,WardName
	,StartDateTime
	,StartDate
	,StartTime
	,EndDateTime
	,EndDate
	,EndTime
	,LengthOfWardStay
	,CreateDate
	,CreatedBy
	,EpisodicGPCode
	,EpisodicGPName
	,EpisodicGPPracticeCode
	,EpisodicGPPracticeName
	,EpisodicCCGCode
	,EpisodicCCGName
	,ModifiedDate
	,PatientPostcode
)

select 
	NHSNumber
	,FacilityID
	,PatientID
	,AgeOnAdmission
	,ProviderSpellID
	,WardStayID
	,WardName
	,StartDateTime
	,StartDate
	,StartTime
	,EndDateTime
	,EndDate
	,EndTime
	,LengthOfWardStay
	,CreateDate
	,CreatedBy
	,EpisodicGPCode
	,EpisodicGPName
	,EpisodicGPPracticeCode
	,EpisodicGPPracticeName
	,EpisodicCCGCode
	,EpisodicCCGName
	,ModifiedDate
	,PatientPostcode
from [uhsm-is1].Community.COM.WardStay 

--select 
--	ws.* 
--	,pat.Forename
--	,pat.Surname
--from [WHREPORTING].[COM].[WardStay] ws
--left join COM.Patients pat
--on pat.PatientID = ws.PatientID
--order by WardStayID desc