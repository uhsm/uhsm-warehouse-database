﻿CREATE TABLE [COM].[DS_CYP002](
	[PatientID] [varchar](20) NULL,
	[Main_Ident] [varchar](20) NULL,
	[START_DTTM] [date] NULL,
	[END_DTTM] [datetime] NULL,
	[CCG] [nvarchar](255) NULL,
	[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]