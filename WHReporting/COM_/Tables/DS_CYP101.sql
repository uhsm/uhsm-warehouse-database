﻿CREATE TABLE [COM].[DS_CYP101](
	[ServiceRequestID] [varchar](20) NULL,
	[LocalPatientID] [varchar](20) NULL,
	[OrgCodeCommissioner] [nvarchar](3) NULL,
	[ReferralRequestReceivedDate] [varchar](8000) NULL,
	[ReferralRequestRecievedTime] [varchar](8) NULL,
	[NHSServAgreeLineNum] [varchar](1) NOT NULL,
	[SourceOfReferral] [varchar](31) NULL,
	[OrgCodeReferrer] [varchar](20) NULL,
	[RefCareProfStaffGroup] [nvarchar](255) NULL,
	[PriorityType] [varchar](4) NULL,
	[PrimaryReasonForRef] [varchar](1) NOT NULL,
	[ServiceDischargeDate] [varchar](8000) NULL,
	[DischargeLetterIssuedDate] [varchar](1) NOT NULL,
	[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]