﻿CREATE TABLE [COM].[DS_CYP102](
	[ServiceRequestIdentifier] [varchar](20) NULL,
	[CareProfessionalTeamLocalIdentifier] [varchar](30) NULL,
	[CIDServiceTypeReferredTo] [nvarchar](255) NULL,
	[ReferralClosureDate] [varchar](8000) NULL,
	[ReferralRejectionDate] [date] NULL,
	[ReferralClosureReason] [varchar](2) NOT NULL,
	[ReferralRejectionReason] [varchar](1) NOT NULL,
	[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]