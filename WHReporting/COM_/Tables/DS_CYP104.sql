﻿CREATE TABLE [COM].[DS_CYP104](
	[ServiceRequestID] [varchar](20) NULL,
	[UBRNConverted] [varchar](255) NULL,
	[PatientPathwayID] [varchar](300) NULL,
	[OrgCodePPI] [varchar](3) NOT NULL,
	[WaitingTimeMeasureType] [varchar](31) NULL,
	[RTTPeriodStartDate] [varchar](8000) NULL,
	[RTTPeriodEndDate] [varchar](8000) NULL,
	[RTTPeriodStatus] [varchar](2) NULL,
	[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]