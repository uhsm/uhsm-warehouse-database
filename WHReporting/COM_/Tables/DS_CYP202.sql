﻿CREATE TABLE [COM].[DS_CYP202](
	[CareActivityID] [numeric](10, 0) NOT NULL,
	[CareContactID] [numeric](10, 0) NOT NULL,
	[CommunityCareActivityType] [varchar](10) NULL,
	[CareProfLocalID] [numeric](10, 0) NULL,
	[ClinicalContactDurationCare] [varchar](30) NULL,
	[ProcedureSchemeInUse] [varchar](1) NOT NULL,
	[CodedProcedure] [varchar](1) NOT NULL,
	[FindingSchemeInUse] [varchar](1) NOT NULL,
	[CodedFinding] [varchar](1) NOT NULL,
	[ObservationInUse] [varchar](1) NOT NULL,
	[CodedObservation] [varchar](1) NOT NULL,
	[ObservationValue] [varchar](1) NOT NULL,
	[UnitOfMeasurement] [varchar](1) NOT NULL,
	[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]