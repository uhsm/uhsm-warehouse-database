﻿CREATE TABLE [COM].[DS_CYP901](
	[CareProfLocalID] [numeric](10, 0) NULL,
	[ProfRegBody] [varchar](1) NOT NULL,
	[ProfRegEntryID] [varchar](1) NOT NULL,
	[CareProfessionalStaffGroupCommunityCareNationalCode] [nvarchar](10) NULL,
	[OccupationCode] [varchar](1) NOT NULL,
	[CareProfJob] [varchar](1) NOT NULL,
	[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]