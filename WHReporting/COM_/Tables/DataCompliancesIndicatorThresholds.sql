﻿CREATE TABLE [COM].[DataCompliancesIndicatorThresholds](
	[IndicatorID] [char](4) NOT NULL,
	[IndicatorName] [varchar](80) NOT NULL,
	[FinancialYear] [char](9) NOT NULL,
	[Red] [decimal](6, 5) NULL,
	[Amber] [decimal](6, 5) NULL,
	[Green] [decimal](6, 5) NULL
) ON [PRIMARY]