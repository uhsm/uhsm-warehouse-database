﻿CREATE TABLE [COM].[GroupSessions](
	[GroupSessionIdentifier] [numeric](10, 0) NOT NULL,
	[GroupSessionDatetime] [datetime] NULL,
	[GroupSessionDate] [date] NULL,
	[GroupSessionTime] [time](7) NULL,
	[ContactDuration] [int] NULL,
	[GroupSessionTypeNationalCode] [float] NULL,
	[NumberOfAttendees] [numeric](5, 0) NULL,
	[ActivityLocationTypeNationalCode] [nvarchar](255) NULL,
	[SiteCodeOfTreatment] [varchar](20) NULL,
	[CareProfessionalStaffGroupCode] [nvarchar](255) NULL
) ON [PRIMARY]