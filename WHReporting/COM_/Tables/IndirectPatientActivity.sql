﻿CREATE TABLE [COM].[IndirectPatientActivity](
	[ActivityIdentifier] [numeric](10, 0) NOT NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[StartTime] [time](7) NULL,
	[EndTime] [time](7) NULL,
	[StartDateTime] [datetime] NULL,
	[EndDateTime] [datetime] NULL,
	[ActivityDuration] [int] NULL,
	[Comments] [varchar](255) NULL,
	[Location] [varchar](255) NULL,
	[IndirectPatientActivityTypeCodeCIDS] [float] NULL,
	[ActivityTypeLocalCode] [varchar](80) NULL,
	[StaffGroup] [nvarchar](255) NULL
) ON [PRIMARY]