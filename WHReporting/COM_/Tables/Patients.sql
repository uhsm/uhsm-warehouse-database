﻿CREATE TABLE [COM].[Patients](
	[NHSNumber] [varchar](20) NULL,
	[NHSNumberStatus] [varchar](13) NOT NULL,
	[PatientID] [numeric](10, 0) NOT NULL,
	[Forename] [varchar](30) NULL,
	[Surname] [varchar](30) NULL,
	[BirthDate] [datetime] NULL,
	[DeathDate] [datetime] NULL,
	[Postcode] [varchar](25) NULL,
	[GeneralMedicalPracticeCode] [nvarchar](8) NULL,
	[CCGOfPractice] [varchar](10) NULL,
	[Gender] [varchar](80) NULL,
	[EthnicCategory] [varchar](80) NULL,
	[CommunicationLanguage] [varchar](80) NULL,
	[OrganisationCodePCTOfResidence] [char](3) NULL,
	[ActualDeathLocationType] [varchar](80) NULL,
 CONSTRAINT [PK_COM_Patients] PRIMARY KEY CLUSTERED 
(
	[PatientID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]