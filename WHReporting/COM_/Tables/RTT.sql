﻿CREATE TABLE [COM].[RTT](
	[UBRN] [varchar](255) NULL,
	[FacilityID] [varchar](20) NULL,
	[PatientID] [numeric](10, 0) NULL,
	[PatientPathwayIdentifier] [varchar](300) NULL,
	[WaitingTimeMeasurementType] [float] NULL,
	[ReferralID] [numeric](10, 0) NOT NULL,
	[RTTActivityLink] [numeric](10, 0) NULL,
	[RTTEncounterID] [numeric](10, 0) NOT NULL,
	[RTTStatusType] [varchar](10) NULL,
	[RTTStatusUpdateDate] [date] NULL,
	[RTTStatusCode] [varchar](2) NULL,
	[ReferralToTreatmentPeriodStartDate] [datetime] NULL,
	[ReferralToTreatmentPeriodEndDate] [datetime] NULL
) ON [PRIMARY]