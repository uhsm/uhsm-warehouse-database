﻿CREATE TABLE [COM].[SchedulesPatient](
	[NHSnumber] [varchar](10) NULL,
	[PatientID] [varchar](20) NULL,
	[FacilityID] [varchar](20) NULL,
	[ScheduleID] [numeric](10, 0) NOT NULL,
	[EpisodicGPCode] [varchar](10) NULL,
	[EpisodicGPName] [varchar](100) NULL,
	[EpisodicGPPracticeCode] [varchar](10) NULL,
	[EpisodicGPPracticeName] [varchar](255) NULL,
	[EpisodicCCGCode] [varchar](5) NULL,
	[EpisodicCCGName] [varchar](100) NULL,
	[NHSNumberStatus] [varchar](13) NULL,
	[PatientForename] [varchar](30) NULL,
	[PatientSurname] [varchar](30) NULL,
	[PatientDateOfBirth] [date] NULL,
	[PatientDeathDate] [date] NULL,
	[PatientPostCode] [varchar](25) NULL,
	[ActualDeathLocationType] [varchar](80) NULL
) ON [PRIMARY]