﻿CREATE TABLE [COM].[SecondaryCarer](
	[ScheduleID] [numeric](10, 0) NULL,
	[ScheduleChildID] [numeric](10, 0) NOT NULL,
	[OtherCarerPresentName] [varchar](61) NULL
) ON [PRIMARY]