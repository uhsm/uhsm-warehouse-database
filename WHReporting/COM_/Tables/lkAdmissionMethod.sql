﻿CREATE TABLE [COM].[lkAdmissionMethod](
	[AdmissionMethodCode] [int] NOT NULL,
	[AdmissionMethod] [varchar](80) NULL,
	[AdmissionMethodLocalCode] [varchar](25) NULL,
	[AdmissionMethodTypeCode] [int] NULL,
	[AdmissionMethodType] [varchar](80) NULL
) ON [PRIMARY]