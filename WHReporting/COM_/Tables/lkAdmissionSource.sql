﻿CREATE TABLE [COM].[lkAdmissionSource](
	[AdmissionSourceCode] [int] NOT NULL,
	[AdmissionSource] [varchar](50) NULL,
	[NationalAdmissionSourceCode] [varchar](2) NULL
) ON [PRIMARY]