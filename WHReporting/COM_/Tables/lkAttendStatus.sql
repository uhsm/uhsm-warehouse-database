﻿CREATE TABLE [COM].[lkAttendStatus](
	[ATTND_REFNO] [numeric](10, 0) NULL,
	[MAIN_CODE] [varchar](25) NULL,
	[CommCode] [int] NULL,
	[DESCRIPTION] [varchar](80) NULL
) ON [PRIMARY]