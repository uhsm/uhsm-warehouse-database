﻿CREATE TABLE [COM].[lkAttendanceType](
	[AppointmentTypeCode] [numeric](10, 0) NOT NULL,
	[AppointmentTypeLocalCode] [varchar](25) NULL,
	[AppointmentTypeDescription] [varchar](80) NULL,
	[AppointmentTypeNationalCode] [varchar](50) NULL
) ON [PRIMARY]