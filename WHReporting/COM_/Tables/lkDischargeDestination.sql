﻿CREATE TABLE [COM].[lkDischargeDestination](
	[DischargeDestinationCode] [numeric](10, 0) NULL,
	[DischargeDestination] [varchar](80) NULL,
	[NationalDischargeDestinationCode] [varchar](25) NULL
) ON [PRIMARY]