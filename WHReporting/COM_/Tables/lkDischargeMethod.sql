﻿CREATE TABLE [COM].[lkDischargeMethod](
	[DischargeMethodCode] [int] NOT NULL,
	[DischargeMethod] [varchar](80) NULL,
	[DischargeMethodLocalCode] [varchar](25) NULL
) ON [PRIMARY]