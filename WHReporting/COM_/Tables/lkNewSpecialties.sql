﻿CREATE TABLE [COM].[lkNewSpecialties](
	[ID] [int] NOT NULL,
	[OldSpectRefNo] [int] NULL,
	[OldDescription] [varchar](255) NULL,
	[NewSpectRefNo] [int] NULL,
	[NewDescription] [varchar](255) NULL
) ON [PRIMARY]