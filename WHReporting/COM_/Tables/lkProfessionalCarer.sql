﻿CREATE TABLE [COM].[lkProfessionalCarer](
	[RowIdentifier] [numeric](18, 0) NULL,
	[ProfessionalCarerID] [numeric](10, 0) NOT NULL,
	[ProfessionalCarerTypeCode] [numeric](10, 0) NULL,
	[ProfessionalCarerForename] [varchar](30) NULL,
	[ProfessionalCarerSurname] [varchar](30) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Description] [varchar](80) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ModifiedBy] [varchar](30) NULL,
	[CurrentFlag] [varchar](1) NULL,
	[LocalFlag] [varchar](1) NULL,
	[MainIdentifier] [varchar](20) NULL,
	[ArchiveFlag] [varchar](1) NULL,
	[PrimaryStaffTeamRefno] [numeric](10, 0) NULL,
	[OwnerHealthOrgRefno] [numeric](10, 0) NULL,
 CONSTRAINT [PK_lkProfessionalCarer] PRIMARY KEY CLUSTERED 
(
	[ProfessionalCarerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]