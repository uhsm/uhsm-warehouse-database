﻿CREATE TABLE [COM].[lkReferralStatus](
	[RSTAT_REFNO] [numeric](10, 0) NULL,
	[MAIN_CODE] [varchar](25) NULL,
	[DESCRIPTION] [varchar](80) NULL
) ON [PRIMARY]