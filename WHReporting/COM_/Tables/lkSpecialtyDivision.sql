﻿CREATE TABLE [COM].[lkSpecialtyDivision](
	[Service_Name] [nvarchar](255) NULL,
	[SPECT_REFNO] [float] NULL,
	[IPMDescription] [nvarchar](255) NULL,
	[Division] [nvarchar](255) NULL,
	[Directorate] [nvarchar](255) NULL
) ON [PRIMARY]