﻿CREATE VIEW [COM].[vwActivity]
AS

Select	MONTH(appointmentDate) as 'Month'
,		COUNT(*) as 'count'
,		AppointmentType
,		AppointmentSpecialty
,		case when AttendedOrDidNotAttendCode in ('5','6')	then 'Attended'
		when AttendedOrDidNotAttendCode ='2'			then 'Patient Cancellation'
		when AttendedOrDidNotAttendCode = '4'			then 'Hospital Cancellation'
		when AttendedOrDidNotAttendCode in ('3','7')	then 'DNA'
		else 'check'
		end as 'Attendance Status'
,		ActivityTypeMain
,		sp.EpisodicCCGName

from COM.Schedules s
left join COM.SchedulesPatient sp
on s.ScheduleID = sp.ScheduleID
where AppointmentDate between '2013-04-01' and '2013-06-30'
group by	MONTH(appointmentDate),AppointmentType, 
			AppointmentSpecialty, 
			case when AttendedOrDidNotAttendCode in ('5','6')	then 'Attended'
			 when AttendedOrDidNotAttendCode ='2'			then 'Patient Cancellation'
			 when AttendedOrDidNotAttendCode = '4'			then 'Hospital Cancellation'
			 when AttendedOrDidNotAttendCode in ('3','7')	then 'DNA'
			 else 'check'
			 end,
			 ActivityTypeMain,
			 sp.EpisodicCCGName