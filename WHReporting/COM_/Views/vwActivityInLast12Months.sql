﻿CREATE View [COM].[vwActivityInLast12Months] as 


SELECT
	ReferralID from COM.Schedules
where AppointmentDate between dateadd(year,-1,cast(GETDATE() as DATE)) and CAST(getdate() as DATE)
and ReferralID is not null
union
SELECT
	ReferralID from
COM.OtherClientLinkedActivity
where ActivityDate between dateadd(year,-1,cast(GETDATE() as DATE)) and CAST(getdate() as DATE)
and ReferralID is not null