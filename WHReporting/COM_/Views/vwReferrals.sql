﻿CREATE VIEW [COM].[vwReferrals]
AS

Select 
		COUNT (*) as 'Count'
,		EpisodicCCGName
,		ReferredToSpecialty
,		MONTH(ReferralRequestReceivedDate) as 'Month'
,		sor.description

From COM.Referrals r
left join COM.lkSourceOfReferral sor
on r.SourceOfReferral = sor.National_code
Where 
	ReferralRequestReceivedDate between '2013-04-01' and '2013-06-30'

Group By
		EpisodicCCGName
,		ReferredToSpecialty
,		MONTH(ReferralRequestReceivedDate)
,		sor.description