﻿CREATE VIEW [COM].[vwSpells]
AS

Select 
		COUNT (*) as 'Count'
,		EpisodicCCGName
,		WardName
,		MONTH(admissiondate) as 'Month'
,		ad.AdmissionSource

From COM.Spells sp
left join COM.lkAdmissionSource ad
on sp.admissionssource = ad.nationaladmissionsourcecode
Where 
	AdmissionDate between '2013-04-01' and '2013-06-30'

Group By
	EpisodicCCGName
,		WardName
,		MONTH(admissiondate)
,		ad.AdmissionSource