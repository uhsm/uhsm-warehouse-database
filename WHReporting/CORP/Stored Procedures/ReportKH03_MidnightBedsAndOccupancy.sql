﻿/*
--Author: N Scattergood	
--Date created: 04/06/2015
--Stored Procedure Built Bed Days and Occupied 
--The Data is taken from the Input table 
--This is imported from the KH03 Trend file found in:
--S:\CO-HI-Information\New File Management - Jan 2011\Corporate Reporting\KH03
*/

CREATE Procedure  [CORP].[ReportKH03_MidnightBedsAndOccupancy]
as

Select

[Quarter]
      ,[Wards Open (General Acute + Maternity)]
      ,[Beddays in wards open overnight]
      ,[Overall % Occupancy]
      ,[Days in Quarter]
      ,[Average Beds_(General Acute + Maternity)]
      ,[Wards Open (General Acute)]
      ,[General Acute Bed Days]
      ,[General and Acute % Occupancy]
      ,[Average Beds (General Acute Only)]
  FROM [CorporateReports].[KH03].[InputTable]
  
  ORDER BY 
  RIGHT([Quarter],2)