﻿/*
--Author: N Scattergood
--Date created: 04/12/2012
--Stored Procedure for Diabetes Project
--(Might be expanded to other Projects / Specialties)
*/


CREATE Procedure [CSS].[DiabetesProjectPatientLevel]

@Clinic varchar (max)
as 
Select 
distinct
OP.FacilityID
,OP.NHSNo
,PAT.PatientSurname
,PAT.PatientForename
,OP.AgeAtAppointment
,PAT.DateOfBirth
,PAT.Sex
,PAT.SexCode
,PAT.PatientAddress1
,PAT.PatientAddress2
,PAT.PatientAddress3
,PAT.PatientAddress4
,PAT.PostCode
,OP.AppointmentDateTime
,case 
when OP.AttendStatusNHSCode = '2'
then 'Cancelled by Patient'
when OP.AttendStatusNHSCode in ('3','7')
then 'DNA'
when OP.AttendStatusNHSCode = '4'
then 'Cancelled by Hospital'
when OP.AttendStatusNHSCode in ('5','6')
then 'Attendance'
when OP.AppointmentDate >= CAST(getdate() as DATE)
then 'Future Appointment'
else OP.AttendStatus
end as AttendanceCategory
,OP.AppointmentGP
,OP.AppointmentPractice
,OP.AppointmentPracticeCode
,PreviousAtts =
sum(
case 
when 
DATA.Area = 'OP'
and
DATA.StartOfContact  between 
DATEADD(year,-1,OP.AppointmentDatetime) and 
DATEADD(MINUTE,-1,OP.AppointmentDatetime) 
THEN DATA.Activity 
else 0 end
) 
,AttendancesSince =
sum(
case 
when
DATA.Area = 'OP'
and
DATA.StartOfContact  > OP.AppointmentDateTime 
THEN DATA.Activity 
else 0 end
) 
,PreviousIPActivity =
sum(
case 
when
DATA.Area = 'IP'
and
DATA.EndOfContact between 
DATEADD(year,-1,OP.AppointmentDatetime) and 
DATEADD(MINUTE,-1,OP.AppointmentDatetime) 
THEN DATA.Activity
else 0 end
) 
,IPActivitySince =
sum(
case
when
DATA.Area = 'IP'
and
DATA.StartOfContact > OP.AppointmentDateTime 
THEN DATA.Activity
else 0 end
) 
,BedDaysPrevious =
sum(
case 
when
DATA.Area = 'IP'
and
DATA.EndOfContact between 
DATEADD(year,-1,OP.AppointmentDatetime) and 
DATEADD(MINUTE,-1,OP.AppointmentDatetime) 
THEN DATA.BedDays
else 0 end
) 
,BedDaysSince =
sum(
case
when
DATA.Area = 'IP'
and
DATA.StartOfContact > OP.AppointmentDateTime 
THEN DATA.BedDays
else 0 end
) 

,OP.ClinicCode
,OP.Clinic
,OP.AppointmentDate
,OP.AppointmentType
,OP.AttendStatus
,OP.Outcome
,OP.Division
,OP.Directorate
,OP.[Specialty(Function)]
,OP.PurchaserCode


FROM
[WHREPORTING].[OP].Schedule OP
 
Left Join  [WHREPORTING].[OP].[Patient] PAT
ON OP.EncounterRecno = PAT.EncounterRecno     

Left Join
-----Sub Query----
    ( select * from
(
(
      Select 
      1 as Activity,
      'OP' as Area,
      FacilityID, 
      AppointmentDatetime as StartOfContact,
      Null as EndOfContact,
      [SpecialtyCode(Function)],
      Null as BedDays
      FROM [WHREPORTING].[OP].Schedule SCH
      WHERE
      SCH.AttendStatus in ('Attended On Time', 'Attended', 'Patient Late / Seen')
      and SCH.AppointmentDate > '31 Mar 2011'
      ) 
-----End of OP ---
UNION ALL
-----Sub Query for Inpatients --
(
Select Distinct
1 as Activity,
'IP' as Area,
E.FacilityID,
E.AdmissionDateTime as StartOfContact,
E.DischargeDateTime as EndOfContact,
E.[SpecialtyCode(Function)],
S.LengthOfSpell + 1 as BedDays
	FROM
	[WHREPORTING].[APC].Episode E
	Left Join  [WHREPORTING].[APC].[Spell] S
	on E.SourceSpellNo = S.SourceSpellNo
	WHERE
	E.DischargeDate >  '31 Mar 2011'
) 
) as SQ)
-----End of IP ---
-----End of SQ ----
as DATA 
on DATA.[SpecialtyCode(Function)] = OP.[SpecialtyCode(Function)]
and DATA.FacilityID = OP.FacilityID


Where 

--OP.AttendStatus in ('Attended On Time', 'Attended', 'Patient Late / Seen')
--and 
OP.AppointmentType = 'New'
and
OP.AppointmentDate >= '01 Nov 2012'
and 
OP.ClinicCode 
--'NYO4P2'
--'RRO4P2'
--'BGILP2'
--'ANCDIABP2'
in (SELECT Val from dbo.fn_String_To_Table(@Clinic,',',1))



Group By
OP.FacilityID
,OP.NHSNo
,PAT.PatientSurname
,PAT.PatientForename
,PAT.DateOfBirth
,OP.AgeAtAppointment
,PAT.Sex
,PAT.SexCode
,PAT.PatientAddress1
,PAT.PatientAddress2
,PAT.PatientAddress3
,PAT.PatientAddress4
,PAT.PostCode
,OP.AppointmentDateTime
,case 
when OP.AttendStatusNHSCode = '2'
then 'Cancelled by Patient'
when OP.AttendStatusNHSCode in ('3','7')
then 'DNA'
when OP.AttendStatusNHSCode = '4'
then 'Cancelled by Hospital'
when OP.AttendStatusNHSCode in ('5','6')
then 'Attendance'
when OP.AppointmentDate >= CAST(getdate() as DATE)
then 'Future Appointment'
else OP.AttendStatus
end
,OP.AppointmentGP
,OP.AppointmentPractice
,OP.AppointmentPracticeCode
,OP.ClinicCode
,OP.Clinic
,OP.AppointmentDate
,OP.AppointmentType
,OP.AttendStatus
,OP.Outcome
,OP.Division
,OP.Directorate
,OP.[Specialty(Function)]
,OP.PurchaserCode


Order by
PAT.PatientSurname asc
,PAT.PatientForename asc
,OP.AppointmentDate desc