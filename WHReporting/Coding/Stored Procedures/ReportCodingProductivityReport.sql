﻿/*
--Author: N Scattergood
--Date created: 16/03/2016
--Stored Procedure Built for Coding Productivity
--
*/

CREATE Procedure [Coding].[ReportCodingProductivityReport]

@StartDate	as Date 
,@EndDate	as Date	

as
SELECT DISTINCT 
                      AD.Counter
                      , CM.DayOfWeek
                      ,CD.FirstDateOfMonth as DischargeMonth
                      , FCE.FacilityID
                      , FCE.EpisodeUniqueID
                      ,SpellCount = FCE.LastEpisodeInSpell
                      ,FCE.DischargeDate
                      , AD.ModifedDate
                      , CodedBy = isnull(AD.UserModified,'(Unknown Coder)')
                      ,CoderRole =
                      case
                      when AD.UserModified in ('Rebecca Jane Wadsworth','Viollet-Raoui Deborah J' )
                      then 'Training/Audit'
                      when AD.UserModified in ('Michelle Harris', 'Hall Beverley A','Angela Ogden','McLauchlan Jay')
                      then 'Team Leader'
                      else 'Coder'
                      end
                      , ContractedMinutes 
                      = case 
                      when AD.UserModified IS NULL then 450
                      when AD.UserModified = 'Rebecca Jane Wadsworth' then 450
						when AD.UserModified = 'Viollet-Raoui Deborah J' then 450
						when AD.UserModified = 'Michelle Harris' then 450
						when AD.UserModified = 'Hall Beverley A' then 450
						when AD.UserModified = 'Angela Ogden' then 450

						when AD.UserModified = 'Culleton Jackie' then 450
						when AD.UserModified = 'Vivian Hilton' then 364.5
						when AD.UserModified = 'Webster Sue P' then 360
						when AD.UserModified = 'Jones Alison' then 450
						when AD.UserModified = 'Shaikh Corral' then 330
						when AD.UserModified = 'Katy Hannon' then 360
						when AD.UserModified = 'Janet Mcginty' then 450
						when AD.UserModified = 'Preece Cheryl L' then 360
						when AD.UserModified = 'Wright Paula J' then 450
						when AD.UserModified = 'Bowden Dale JP' then 450
						when AD.UserModified = 'Jones Catherine Mrs' then 418.5
						when AD.UserModified = 'Hardy Sarah D' then 450
						when AD.UserModified = 'Merriman Ruth A' then 310.5

						when AD.UserModified = 'Amachree Ton-Opirima Miss' then 450
						when AD.UserModified = 'Barlow Brian Mr' then 450
						when AD.UserModified = 'Patel Mohammed T' then 450
						when AD.UserModified = 'Baker Neil J'				then 420 
						when AD.UserModified = 'Griffey Abigail Miss'		then 450
						
						when AD.UserModified = 'Myers Nigel Mr' then 450---left 16/03


						--Else 'NULL'
						END
						,HRGError = 
						case when UZ.EncounterRecno IS not null 
						then 1 
						else 0
						end
						,CodingDepth = DiagDepth.NumberOfDiags
						,DaysToCoded = datediff(day,FCE.DischargeDate,AD.ModifedDate)
                     
                      
FROM     [APC].[AllDiagnosis] AD

LEFT  JOIN LK.Calendar CM
ON AD.ModifedDate = CM.TheDate 

INNER JOIN APC.Episode FCE
ON AD.EncounterRecno = FCE.EncounterRecno

LEFT JOIN LK.Calendar CD
on FCE.DischargeDate = CD.TheDate

LEFT JOIN [PbR2016].[HRG].[HRG49APCQuality] UZ--will need changing each Fin YEar to the Year in question
  on UZ.EncounterRecno = FCE.EncounterRecno
  AND QualityTypeCode in ('ICD','OPCS')
  
LEFT JOIN ---Sub Query for the Number 
(select   
MAD.EncounterRecno
, MAX(MAD.[DiagnosisSequence]) as NumberOfDiags
FROM
[APC].[AllDiagnosis] MAD
where
MAD.ModifedDate BETWEEN @StartDate AND @EndDate
Group BY
MAD.EncounterRecno
) DiagDepth
on DiagDepth.EncounterRecno = AD.EncounterRecno


WHERE   
AD.DiagnosisSequence = 1 --indicates Primary Diagnosis has been Coded
AND
(  
AD.ModifedDate BETWEEN @StartDate AND @EndDate
--AND 
--(NOT (AD.UserModified IN ('Claudette Maureen Whitby', 'Forbes Margaret', 'Gill Mansell', 'Jackie Brownbill', 'Tina Rogan', 'Colin Owen', 'Lesley Scott', 'Judith Smithson')))
--)
-- OR
-- (
--AD.ModifedDate BETWEEN @StartDate AND @EndDate
--AND
--AD.UserModified IS NULL
)

--ORDER BY 
--AD.UserModified