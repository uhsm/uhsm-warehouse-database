﻿/*
--Author: N Scattergood
--Date created: 14/04/2016
--Stored Procedure Built for Main Drillthrough on Clinical Coding Productivity Report
--
*/

CREATE Procedure [Coding].[ReportCodingProductivityReport_DrillThroughEpisodes]

 @StartDate		as Date = '13 apr 2016'
--, @EndDate		as Date	= '13 apr 2016'
, @Coder		as Varchar(40)

as
Declare @CRLF		as varchar(2) 
SET @CRLF			=  CHAR(13)+CHAR(10)



Select 

	E.FacilityID
	,E.AdmissionDate
	,E.DischargeDate
	,E.EpisodeStartDate
	,E.EpisodeEndDate
	,E.[SpecialtyCode(Function)]
	,E.[Specialty(Function)]
      ,CC.CodingComplete
      ,datediff(day,E.DischargeDate,AD.ModifiedDateTime) as DaysToCoded
      ,AD.UserCreate
      ,AD.UserModified
      ,AD.CreateDateTime
      ,AD.ModifiedDateTime
      , [DiagnosisCodes(12)]
       = isnull(cc.[PrimaryDiagnosis],'-')+@CRLF
             +isnull(cc.[SubsidiaryDiagnosis],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis1],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis2],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis3],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis4],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis5],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis6],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis7],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis8],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis9],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis10],'-')
      
        , [ProcedureCodes(12)]
       = isnull(cc.[PriamryProcedure],'-')+@CRLF
              +isnull(cc.[SecondaryProcedure1],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure2],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure3],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure4],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure5],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure6],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure7],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure8],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure9],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure10],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure11],'-')
 
FROM     [APC].[AllDiagnosis] AD

 left join WHREPORTING.APC.ClinicalCoding CC
  on AD.EpisodeSourceUniqueID = CC.SourceUniqueID
  
   left join WHREPORTING.APC.Episode E
  on E.EpisodeUniqueID = AD.EpisodeSourceUniqueID

WHERE   
AD.DiagnosisSequence = 1 --indicates Primary Diagnosis has been Coded
AND
--AD.ModifedDate BETWEEN @StartDate AND @EndDate
AD.ModifedDate = @StartDate 
AND
AD.UserModified = @Coder 


Order by
AD.UserModified
,ModifiedDateTime