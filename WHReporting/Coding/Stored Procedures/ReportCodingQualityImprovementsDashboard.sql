﻿/*
--Author: N Scattergood
--Date created: 23/09/2015
--Stored Procedure Built for Coding Quality Workstream (In progress)
--
*/

CREATE Procedure [Coding].[ReportCodingQualityImprovementsDashboard]

@StartDate	as Date = '01 apr 2014'
,@EndDate	as Date	= '31 May 2016'

as




	
SELECT 
Distinct

--top 20000
      Diag.[EpisodeSourceUniqueID],
      Diag.[DiagnosisCode]
      ,Diag.[Diagnosis]
      ,Diag.[DiagnosisSequence]
      ,Diag.[UserModified]
      ,Diag.[ModifedDate]
      ,E.PrimaryProcedureCode
      ,E.FacilityID
      ,E.FirstEpisodeInSpell
      ,E.[SpecialtyCode]
      ,E.[Specialty]
      ,E.[SpecialtyCode(Function)]
      ,E.[Specialty(Function)]
      ,E.Directorate
      ,E.Division
      ,S.PatientClass
      ,E.ConsultantName
      ,'Per_FinYear' = Cal.FinancialYear
      ,'Per_Month' = Cal.FirstDateOfMonth
      ,'Per_MonthName' = DATENAME(Month, E.DischargeDate)
      ,'Per_DayOfWeek' = CalA.DayOfWeek
      ,'Per_Weekend'= 
      case 
      when CalA.DayOfWeekKey IN ('6','7')
      then 'WeekEND'
      else 'WeekDAY'
      end
      ,'IND_CodedSpells' = 1
      ,isnull(CI.IND_CharlsonComorbidity,0) as 'IND_CharlsonComorbidity'
      ,ND.IND_NumberOfSecondaryDiags
      ,'IND_UnspecifiedPoint9s'
      = Case 
      WHEN   RIGHT([DiagnosisCode],1) = '9'
        and  Diagnosis like '%unspecified%'
        THEN 1
        ELSE 0
        END
        ,'IND_SignsAndSymptoms'
        = Case
        WHEN LEFT([DiagnosisCode],1) = 'R'
        THEN 1
        ELSE 0
        END
        ,'FLAG_MortalityIndicator'
        = Case 
        WHEN S.DischargeMethodNHSCode = 4
        THEN 'Yes'
        ELSE 'No'
        END


  FROM [WHREPORTING].[APC].[AllDiagnosis] Diag
  
	left join WHREPORTING.APC.Episode E
	on E.EpisodeUniqueID = Diag.EpisodeSourceUniqueID
	
		left join WHREPORTING.APC.Spell S
		on S.SourceSpellNo = E.SourceSpellNo
	
		left join
		----Identifies the CoMorbidities---
		/*
Sub Query 1 Look At Number of Diag Codes recorded
*/
	(
	SELECT
		[EpisodeSourceUniqueID]
		,Diag.EncounterRecno
		,IND_NumberOfDiags = MAX([DiagnosisSequence])
		,IND_NumberOfSecondaryDiags = MAX([DiagnosisSequence]) - 1
				
	
		FROM [WHREPORTING].[APC].[AllDiagnosis] Diag
  
	left join WHREPORTING.APC.Episode E
	on E.EpisodeUniqueID = Diag.EpisodeSourceUniqueID
	
	Where
	E.DischargeDate between @StartDate and @EndDate
  
		Group By 
		[EpisodeSourceUniqueID]
		,Diag.EncounterRecno
		) ND
	-------------------END of Sub Query-------------
		on ND.EpisodeSourceUniqueID = E.EpisodeUniqueID
		
		left join 
		----Identifies ChalsonIndex---
		----Sub Query 2 -----
		(
		SELECT 
	Distinct
		[EpisodeSourceUniqueID]
,IND_CharlsonComorbidity		= case when CI.ConditionName is NOT null then 1 else 0 End
--,[Acute myocardial infarction]	= sum (case when CI.ConditionName = 'Acute myocardial infarction'	then 1 else 0 end)
--,[Cancer]						= sum (case when CI.ConditionName = 'Cancer'						then 1 else 0 end)
--,[Cerebral vascular accident]	= sum (case when CI.ConditionName = 'Cerebral vascular accident'	then 1 else 0 end)
--,[Congestive heart failure]		= sum (case when CI.ConditionName = 'Congestive heart failure'		then 1 else 0 end)
--,[Connective tissue disorder]	= sum (case when CI.ConditionName = 'Connective tissue disorder'	then 1 else 0 end)
--,[Dementia]						= sum (case when CI.ConditionName = 'Dementia'						then 1 else 0 end)
--,[Diabetes]						= sum (case when CI.ConditionName = 'Diabetes'						then 1 else 0 end)
--,[Diabetes complications]		= sum (case when CI.ConditionName = 'Diabetes complications'		then 1 else 0 end)
--,[HIV]							= sum (case when CI.ConditionName = 'HIV'							then 1 else 0 end)
--,[Liver disease]				= sum (case when CI.ConditionName = 'Liver disease'					then 1 else 0 end)
--,[Metastatic cancer]			= sum (case when CI.ConditionName = 'Metastatic cancer'				then 1 else 0 end)
--,[Paraplegia]					= sum (case when CI.ConditionName = 'Paraplegia'					then 1 else 0 end)
--,[Peptic ulcer]					= sum (case when CI.ConditionName = 'Metastatic cancer'				then 1 else 0 end)
--,[Peripheral vascular disease]	= sum (case when CI.ConditionName = 'Peripheral vascular disease'	then 1 else 0 end)
--,[Pulmonary disease]			= sum (case when CI.ConditionName = 'Pulmonary disease'				then 1 else 0 end)
--,[Renal disease]				= sum (case when CI.ConditionName = 'Renal disease'					then 1 else 0 end)
--,[Severe liver disease]			= sum (case when CI.ConditionName = 'Severe liver disease'			then 1 else 0 end)
				
		FROM [WHREPORTING].[APC].[AllDiagnosis] Diag
  
	left join WHREPORTING.APC.Episode E
	on E.EpisodeUniqueID = Diag.EpisodeSourceUniqueID
	
		inner Join [WHREPORTING].[LK].[CharlsonIndex] CI
		on 
		(
		(CI.ICDCoding = Diag.DiagnosisCode 	and CI.[Length] = 4)
		or
		(CI.ICDCoding = left(Diag.DiagnosisCode,3) 	and CI.[Length] = 3)
		)

Where 
[DiagnosisSequence] <> 1
and
E.DischargeDate between @StartDate and @EndDate

		-------------------END of STEP 2 Query-------------
		) CI
		  on CI.EpisodeSourceUniqueID = E.EpisodeUniqueID
		  
		  left join WHREPORTING.LK.Calendar Cal
		  on Cal.TheDate = E.DischargeDate
		  
		  left join WHREPORTING.LK.Calendar CalA
		  on CalA.TheDate = E.AdmissionDate
    
  Where 
  DiagnosisSequence = 1
  and
  E.DischargeDate between @StartDate and @EndDate
  and 
  LEFT([DiagnosisCode],3) <> 'Z38' --i.e. exclude Well Babies
    
  Order by 
  [DiagnosisCode]
  
  
  --Drop Table  #NumberOfDiag, #CharlsonIndex