﻿CREATE Procedure [Coding].[ReportPneumoniaTracker]

@StartDate	as Date = '31 Mar 2014' --(for Week Beginning)
,@EndDate	as Date	= '27 Mar 2016' --(for Week Ending)

AS
Select
S.FaciltyID
,S.AdmissionDate
,S.DischargeDate
,S.AdmissionWard
,S.DischargeWard
,S.DischargeSpecialty        
,'PER_DischargeMonth'	= CalD.FirstDateOfMonth
,'PER_DischargeFinYear' = CalD.FinancialYear
,'PER_DischargeWeek' = CalD.LastDayOfWeek
,'PER_AdmissionWeekDay' = CalA.DayOfWeek
,'PER_AdmissionWeekendAdmission'
=     case 
      when CalA.DayOfWeekKey IN ('6','7')
      then 'WeekEND'
      else 'WeekDAY'
      end
,'IND_MortalityIndicator'
        = Case 
        WHEN S.DischargeMethodNHSCode = 4
        THEN 'Patient Died'
        ELSE 'Patient Discharged'
        END
,'IND_PneumoniaDiagnosis' = 1


FROM
WHReporting.APC.Spell S

inner join ---Sub Query for Episodes with Primary Diagnosis of Pneumonia
(
SELECT Distinct
E.SourceSpellNo
  FROM [WHREPORTING].[APC].Episode E
 
  where
	(
    [PrimaryDiagnosisCode] in ('A202', 'A212', 'A221', 'A310', 'A420', 'A430', 'A481', 'B012', 'B052', 'B250',
								'B583','B671','J170','J171','J172','J173','J178', 'J850', 'J851')
	OR
	left ([PrimaryDiagnosisCode],3) in ('A78','B59','J12','J13','J14','J15','J16','J18')
	)
) SQ --- End of Sub Query
on SQ.SourceSpellNo = S.SourceSpellNo

Left Join
WHREPORTING.LK.Calendar CalD
on S.DischargeDate = CalD.TheDate

Left Join
WHREPORTING.LK.Calendar CalA
on S.DischargeDate = CalA.TheDate

Where
S.DischargeDate between @StartDate and @EndDate