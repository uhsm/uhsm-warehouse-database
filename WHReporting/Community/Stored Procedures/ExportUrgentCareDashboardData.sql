﻿CREATE procedure [Community].[ExportUrgentCareDashboardData]
as

/*
--Author: G Ryder / K Oakden
--Date created: 09/06/2014
--Export urgent care dashboard data to DSCRO as part of Schedule 6 contract agreement
--SSRS report code at S:\CO-HI-Information\New File Management - Jan 2011\Data Warehouse Project\SSRS Report Code\Schedule6\Urgent Care Dashboard Data Flow GR.sql

--KO updated 07/02/2015 to change to local server after v-uhsm-dw01 failure

*/
declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)	
declare @RowsInserted Int

declare @StartDate as Datetime
declare @EndDate as Datetime

set @StartTime = getdate()
set @StartDate  = DATEADD(day,DATEDIFF(day,0,GETDATE())-7,0)        
set @EndDate    = DATEADD(day,DATEDIFF(day,0,GETDATE())-1,0)  --This ensures yesterdays data


-- Urgent Care Inpatient
-- The daily inpatient data feed will contain a single record for each emergency inpatient admission and discharge (following an emergency admission) which occurred in the previous 72 hour period. 
-- Emergency admissions are identified by the admission methods 21 to 28 (NHS Data Dictionary).
delete from [COMMUNITY].[TISData].[RM2c].[UrgentCare_IP]
insert into [COMMUNITY].[TISData].[RM2c].[UrgentCare_IP] (
	[Provider]
	,[ProviderLocalID]
	,[NHSNumber]
	,[DateOfBirth]
	,[PracticeCode]
	,[TreatmentFunction]
	,[AdmissionMethod]
	,[AdmissionDate]
	,[AdmissionTime]
	,[DischargeDate]
	,[DischargeTime]
	,[DischargeDestination]
	,[DischargeMethod]
	,[CCG])

select
	Provider = 'RM200'
	,ProviderLocalID = S.FaciltyID
	,NHSNumber = S.NHSNo
	,DateOfBirth = cast(P.DateOfBirth as datetime)
	,S.PracticeCode
	,TreatmentFunction = S.[AdmissionSpecialtyCode(Function)]
	,AdmissionMethod = cast(S.AdmissionMethodNHSCode as varchar(10))
	,AdmissionDate = cast(S.AdmissionDate as datetime)
	,AdmissionTime = cast(S.AdmissionDateTime as datetime) 
	,DischargeDate = cast(S.DischargeDate as datetime) 
	,DischargeTime = cast(S.DischargeDateTime as datetime)
	,DischargeDestination = cast(S.DischargeDestinationNHSCode as varchar(10))
	,DischargeMethod = cast(S.DischargeMethodNHSCode as varchar(10))
	,CCG = S.SpellCCGCode
from WHREPORTING.APC.Spell S
left join WHREPORTING.APC.Patient P 
	on S.EncounterRecno=P.EncounterRecno
where
S.SpellCCGCode in('02H','00T','00V','00W','01D','01M','00Y','01G','01N','01W','01Y','02A')
and S.AdmissionMethodNHSCode in ('21','22','23','24','25','26','27','28')
and (S.AdmissionDate between @StartDate and @EndDate
or S.DischargeDate between @StartDate and @EndDate)

select @RowsInserted = @@Rowcount

select @Stats = 
	'Rows inserted to TISData.RM2c.UrgentCare_IP '  + CONVERT(varchar(10), @RowsInserted) 


-- Accident & Emergency Attendances (and Walk in Centre)
-- The daily feed will contain a record of all A&E and WIC attendances that have
-- occurred in the previous 72 hour period
delete from [COMMUNITY].[TISData].[RM2c].[UrgentCare_EM]
insert into [COMMUNITY].[TISData].[RM2c].[UrgentCare_EM] (
	[Provider]
	,[ProviderLocalID]
	,[NHSNumber]
	,[DateOfBirth]
	,[PracticeCode]
	,[ArrivalDate]
	,[ArrivalTime]
	,[SourceReferral]
	,[DisposalMethod]
	,[PresentingComplaint]
	,[InitialTriageCat]
	,[DepartureDate]
	,[DepartureTime]
	,[CCG]
	,[WalkInType])
select
	Provider = 'RM200'
	,ProviderLocalID = AE.LocalPatientID
	,NHSNumber
	,DateOfBirth = cast(DateOfBirth as DATETIME)
	,PracticeCode = RegisteredGpPracticeCode
	,ArrivalDate = cast(ArrivalDate as DATETIME)
	,ArrivalTime = cast(ArrivalTime as DATETIME)
	,SourceReferral = AE.SourceOfReferralCode
	,DisposalMethod = AE.AttendanceDisposalCode
	,PresentingComplaint = Triage.ManTriageDescription
	,InitialTriageCat = cast(AE.ManchesterTriageCode as integer)
	,DepartureDate = cast(cast(AE.AttendanceConclusionTime as DATE) as datetime) 
	,DepartureTime = cast(AE.AttendanceConclusionTime as datetime)
	,CCG = AE.CCGCode
	,WalkInType = 
		case 
			when AE.WhereSeen in(0,1) then 'MainDept'
			when AE.WhereSeen=2 then 'UCCMinor'
			when AE.WhereSeen=3 then 'UCCDeflect'
			when AE.WhereSeen=4 then 'GPOOH'
			when AE.WhereSeen=5 then 'GPAU' 
			else 'Unknown' 
		end
from WH.AE.Encounter AE

left join WH.AE.ManchesterTriageLookup Triage
    on Triage.ManTriageCode = AE.ManchesterTriageCode

where AE.CCGCode in('02H','00T','00V','00W','01D','01M','00Y','01G','01N','01W','01Y','02A')
and AE.ArrivalDate between @StartDate and @EndDate

select @RowsInserted = @@Rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = @Stats + 
	'. Rows inserted to TISData.RM2c.UrgentCare_EM '  + CONVERT(varchar(10), @RowsInserted) + 
	'. Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'


exec dbo.WriteAuditLogEvent 'Community - RM2 ExportUrgentCareDashboardData', @Stats, @StartTime

--update flag
update [COMMUNITY].TISData.[RM2c].UrgentCare_Refresh set FLAG = 1, FLAG_SET_DTTM = GETDATE()
/**
-- Initial one-off admitted patient care dataset containing a record of all patients
-- currently within hospital that were admitted as an emergency.
insert into [COMMUNITY].[TISData].[RM2c].[UrgentCare_IP_20140611] (
	[Provider]
	,[ProviderLocalID]
	,[NHSNumber]
	,[DateOfBirth]
	,[PracticeCode]
	,[TreatmentFunction]
	,[AdmissionMethod]
	,[AdmissionDate]
	,[AdmissionTime]
	,[DischargeDate]
	,[DischargeTime]
	,[DischargeDestination]
	,[DischargeMethod]
	,[CCG])
select
	Provider = 'RM200'
	,ProviderLocalID = S.FaciltyID
	,NHSNumber = S.NHSNo
	,DateOfBirth = cast(P.DateOfBirth as datetime)
	,S.PracticeCode
	,TreatmentFunction = S.[AdmissionSpecialtyCode(Function)]
	,AdmissionMethod = cast(S.AdmissionMethodNHSCode as varchar(10))
	,AdmissionDate = cast(S.AdmissionDate as datetime)
	,AdmissionTime = cast(S.AdmissionDateTime as datetime) 
	,DischargeDate = cast(S.DischargeDate as datetime) 
	,DischargeTime = cast(S.DischargeDateTime as datetime)
	,DischargeDestination = cast(S.DischargeDestinationNHSCode as varchar(10))
	,DischargeMethod = cast(S.DischargeMethodNHSCode as varchar(10))
	,CCG = S.SpellCCGCode
from [V-UHSM-DW01].WHREPORTING.APC.Spell S

left join [V-UHSM-DW01].WHREPORTING.APC.Patient P 
	on S.EncounterRecno=P.EncounterRecno

where S.SpellCCGCode in('02H','00T','00V','00W','01D','01M','00Y','01G','01N','01W','01Y','02A')
and [AdmissionMethodNHSCode] in ('21','22','23','24','25','26','27','28')
and DischargeDate is null
**/