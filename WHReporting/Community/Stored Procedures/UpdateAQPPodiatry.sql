﻿CREATE Procedure [Community].[UpdateAQPPodiatry] as



truncate table WHReporting.Community.AQPPodiatry 


insert into WHReporting.Community.AQPPodiatry
(
TISID,
STRAN_REFNO,
PATNT_REFNO,
NHSNumber,
EpisodicGpCode,
EpisodicGpPracticeCode,
ReferralDate,
AppointmentDate,
ApptType,
ReferralSource,
National_code,
DateOfBirth,
ProfessionalCarer,
PathwayType,
PathwayDescription,
ModifiedDate
)

select distinct
OPAppointments.TISID,
OPAppointments.STRAN_REFNO
,Patients.PATNT_REFNO
,NHS_IDENTIFIER AS NHSNumber
,cast(COALESCE (RegisteredGp.PROCA_REFNO, DefaultRegisteredGp.PROCA_REFNO) as varchar) AS EpisodicGpCode, 
 cast(COALESCE (RegisteredGp.HEORG_REFNO, DefaultRegisteredGp.HEORG_REFNO) as varchar) AS EpisodicGpPracticeCode
,Referrals.RECVD_DTTM as ReferralDate
,OPAppointments.start_dttm AS AppointmentDate
,case when NewAppointments.MinSchedule is null then 'Follow-Up' else 'New' end as ApptType
,RSource.[description] as ReferralSource
,SLookup.National_code
,DTTM_OF_BIRTH as DateOfBirth
--,CCG.[Organisation Name] AS CCG
,PCarer.Surname+', '+PCarer.Forename as ProfessionalCarer
,case when dp.CODE='255611003' then 'A'
when dp.CODE='6736007' then 'B1'
when dp.CODE='CC00469' then 'B2'
when dp.CODE='255604002' then 'C'
when dp.CODE='CC00507' then 'D'
when dp.CODE='371923003' then 'D1'
when dp.CODE='371924009' then 'D2' 
when dp.CODE='225399009' then 'E'
when dp.CODE = '308451001' then 'Non-AQP' 
end as PathwayType
,case when dp.CODE='255611003' then 'Wound Care Pathway'
when dp.CODE='6736007' then 'Nail Surgery 1 Toe'
when dp.CODE='CC00469' then 'Nail Surgery 2 Toe'
when dp.CODE='255604002' then 'Routine Consultation and Treatment'
when dp.CODE='CC00507' then 'Biomechanical (all adults and children without concurrent co-morbidities)'
when dp.CODE='371923003' then 'Simple off the shelf standard insoles'
when dp.CODE='371924009' then 'Higher cost standard insoles' 
when dp.CODE='225399009' then 'Pain Assessment'
when dp.CODE ='308451001' then 'Non-AQP Appointment' 
end as PathwayDescription
--,Spec.[Description] as SpecialtyDesc
--,isnull(Div.[Description],'Unknown') as DivDesc
,ModifiedDate=OPAppointments.MODIF_DTTM
--Into WHReporting.Community.AQPPodiatry

from [uhsm-is1].Community.pas.SCHEDULES_VE06_RM2 OPAppointments
inner join [uhsm-is1].Community.pas.PATIENTS_VE05_RM2 Patients
on OPAppointments.PATNT_REFNO=Patients.PATNT_REFNO
inner join [uhsm-is1].Community.pas.REFERRALS_VE07_RM2 Referrals
on Referrals.REFRL_REFNO=OPAppointments.REFRL_REFNO
left join [uhsm-is1].Community.pas.DIAGNOSIS_PROCEDURES_VE01 dp
on OPAppointments.schdl_refno = dp.sorce_refno
LEFT JOIN [uhsm-is1].Community.pas.PROF_CARERS_VE01 PCarer
      ON
            CASE
                  WHEN OPAppointments.SCTYP_REFNO = 1468 THEN OPAppointments.PROCA_REFNO
                  WHEN OPAppointments.SCTYP_REFNO = 1470 THEN ISNULL(OPAppointments.SEENBY_PROCA_REFNO, OPAppointments.PROCA_REFNO)
            END = PCarer.proca_REFNO            
inner join [uhsm-is1].Community.pas.PATIENT_IDS_VE01 PatientFacil on PatientFacil.PATNT_REFNO=OPAppointments.PATNT_REFNO
left join [uhsm-is1].Community.pas.[REFERENCE_VALUES_VE01] RSource on RSource.[rfvAL_refno]=Referrals.[sorrf_refno]
left join [WHREPORTING].com.lksourceofreferral  SLookup on SLookup.sorrf_refno=Referrals.sorrf_refno                
left join
				(select min(OPAppointments2.STRAN_REFNO) as MinSchedule 
				from [uhsm-is1].Community.pas.SCHEDULES_VE06_RM2 OPAppointments2
				inner join [uhsm-is1].Community.pas.REFERRALS_VE07_RM2 Referrals2
				on Referrals2.REFRL_REFNO=OPAppointments2.REFRL_REFNO
				left join [uhsm-is1].Community.pas.DIAGNOSIS_PROCEDURES_VE01 dp2
				on OPAppointments2.schdl_refno = dp2.sorce_refno
				where dp2.CODE in ('255611003','6736007','CC00469','255604002','371923003','371924009','308451001','CC00507','225399009')			
				and CCSXT_CODE = 'CCRFR'
				group by Referrals2.REFRL_REFNO,dp2.CODE) as NewAppointments
				on NewAppointments.MinSchedule=OPAppointments.STRAN_REFNO
LEFT OUTER JOIN [uhsm-is1].Community.pas.PATIENT_PROF_CARERS_VE01 AS DefaultRegisteredGp 
		ON cast(DefaultRegisteredGp.PATNT_REFNO as varchar) = cast(OPAppointments.PATNT_REFNO as varchar) 
		AND DefaultRegisteredGp.PRTYP_REFNO = 1132
		AND DefaultRegisteredGp.ARCHV_FLAG = 'N' 
		AND OPAppointments.START_DTTM < DefaultRegisteredGp.START_DTTM 
		AND NOT EXISTS
                          (SELECT     1 AS Expr1
                            FROM          [uhsm-is1].Community.pas.PATIENT_PROF_CARERS_VE01 AS Previous
                            WHERE      (PATNT_REFNO = DefaultRegisteredGp.PATNT_REFNO) AND (PRTYP_REFNO = DefaultRegisteredGp.PRTYP_REFNO) AND 
                                                   (ARCHV_FLAG = DefaultRegisteredGp.ARCHV_FLAG) AND (OPAppointments.START_DTTM < START_DTTM) AND 
                                                   (START_DTTM > DefaultRegisteredGp.START_DTTM) OR
                                                   (cast(PATNT_REFNO as varchar) = cast(DefaultRegisteredGp.PATNT_REFNO as varchar)) 
                                                   AND (cast(PRTYP_REFNO as varchar) = cast(DefaultRegisteredGp.PRTYP_REFNO as varchar)) AND 
                                                   (ARCHV_FLAG = DefaultRegisteredGp.ARCHV_FLAG) AND (OPAppointments.START_DTTM < START_DTTM) AND 
                                                   (START_DTTM = DefaultRegisteredGp.START_DTTM) AND (PATPC_REFNO > DefaultRegisteredGp.PATPC_REFNO)) 
	
	LEFT OUTER JOIN [uhsm-is1].Community.pas.PATIENT_PROF_CARERS_VE01 AS RegisteredGp 
		ON cast(RegisteredGp.PATNT_REFNO as varchar) = cast(OPAppointments.PATNT_REFNO as varchar) 
		AND RegisteredGp.PRTYP_REFNO = 1132
		AND RegisteredGp.ARCHV_FLAG = 'N' 
		AND OPAppointments.START_DTTM BETWEEN RegisteredGp.START_DTTM 
		AND COALESCE (RegisteredGp.END_DTTM, OPAppointments.START_DTTM) 
		AND NOT EXISTS
                          (SELECT     1 AS Expr1
                            FROM          [uhsm-is1].Community.pas.PATIENT_PROF_CARERS_VE01 AS Previous
                            WHERE      (cast(PATNT_REFNO as varchar) = cast(RegisteredGp.PATNT_REFNO as varchar)) AND (cast(PRTYP_REFNO as varchar) = cast(RegisteredGp.PRTYP_REFNO as varchar)) AND (ARCHV_FLAG = 'N') AND 
                                                   (OPAppointments.START_DTTM BETWEEN START_DTTM AND COALESCE (END_DTTM, OPAppointments.START_DTTM)) AND 
                                                   (START_DTTM > RegisteredGp.START_DTTM) OR
                                                   (cast(PATNT_REFNO as varchar) = cast(RegisteredGp.PATNT_REFNO as varchar)) AND (PRTYP_REFNO = RegisteredGp.PRTYP_REFNO) AND (ARCHV_FLAG = 'N') AND 
                                                   (OPAppointments.START_DTTM BETWEEN START_DTTM AND COALESCE (END_DTTM, OPAppointments.START_DTTM)) AND 
                                                   (START_DTTM = RegisteredGp.START_DTTM) AND (PATPC_REFNO > RegisteredGp.PATPC_REFNO)) 
where 
dp.CODE in ('255611003','6736007','CC00469','255604002','371923003','371924009','308451001','CC00507','225399009')
and OPAppointments.ATTND_REFNO in ('357','2868')
and CCSXT_CODE = 'CCRFR'
and OPAppointments.START_DTTM>'2013-03-31'
--and OPAppointments.MODIF_DTTM>(SELECT MAX(ModifiedDate) FROM Community.AQPPodiatry)
and SCTYP_REFNO ='1470'
and OPAppointments.ARCHV_FLAG ='N'
and DP.ARCHV_FLAG = 'N'
AND Patients.PATNT_REFNO NOT IN (select PATNT_REFNO from
    [uhsm-is1].Community.pas.IPMTestPatients)