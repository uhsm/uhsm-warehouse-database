﻿CREATE TABLE [Community].[AQPPodiatry](
	[TISID] [int] NOT NULL,
	[STRAN_REFNO] [numeric](10, 0) NULL,
	[PATNT_REFNO] [numeric](10, 0) NULL,
	[NHSNumber] [varchar](20) NULL,
	[EpisodicGpCode] [varchar](30) NULL,
	[EpisodicGpPracticeCode] [varchar](30) NULL,
	[ReferralDate] [datetime] NULL,
	[AppointmentDate] [datetime] NULL,
	[ApptType] [varchar](9) NULL,
	[ReferralSource] [varchar](80) NULL,
	[National_code] [float] NULL,
	[DateOfBirth] [datetime] NULL,
	[ProfessionalCarer] [varchar](62) NULL,
	[PathwayType] [varchar](7) NULL,
	[PathwayDescription] [varchar](73) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]