﻿CREATE TABLE [Community].[LearningDisability](
	[NHSNumber] [varchar](20) NULL,
	[DiagnosisDate] [datetime] NULL,
	[PracticeCode] [varchar](10) NULL
) ON [PRIMARY]