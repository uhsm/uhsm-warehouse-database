﻿CREATE Procedure [DM01].[Audiology_NonIPMWL_WL_Breaches]

as 
set nocount on

SELECT 
      [ReportMonth]
      ,[ID]
      ,[patient_number]
      ,[patient_name_first]
      ,[patient_name_last]
      ,[Referred by:]
      ,[POST_CODE]
      ,[Type of Test Requested]
      ,[Date of Referral Request]
      ,[BREACH Date]
      ,[Appointment date]
      ,[DNA date]
      ,[WeekWait]
      ,[Exclude]
      ,[Breach_Validation_Update]
      ,[Breach_Validation_Update_Date]
  FROM [CorporateReports].[DM01].[Audiology_NonIPMWL_Validation]
 where WeekWait>=6