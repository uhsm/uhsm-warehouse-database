﻿CREATE procedure [DM01].[BuildElectiveActivity] as

/**
Author: K Oakden
Date: 10/09/2014
Based on Qry_ElectiveActivity in DM01_csvLoad_Master.accdb

Required for Elective Activity section of monthly DM01
Parameters set to first and last days of last month
Run daily from dbo.BuildReportingModel 
**/

--declare @Today datetime
declare @StartTime datetime
declare @Stats varchar(255)
declare @RowsInserted int
declare @Elapsed int

select @StartTime = getdate()

--declare @startdate datetime
--declare @enddate datetime

--select @startdate = '2014-07-01 00:00:00.000'
--select @enddate = '2014-07-31 23:59:59.000'
--select @startdate = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) --First day of previous month
--select @enddate = dateadd(MINUTE, -1, DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)) --Last Day of previous month 

--print @startdate
--print @enddate

--Create temp table to map all required diagnostic procecedures
if Object_id('tempdb..#DiagnosticProcedures', 'U') is not null
	drop table #DiagnosticProcedures

select distinct
	EpisodeSourceUniqueID
	,DiagnosticProcedure = 
		case 
			when ProcedureCode = 'K582'	and not exists (
				select 1 from APC.AllProcedures K57Primary
				where K57Primary.EpisodeSourceUniqueID = AllProcs.EpisodeSourceUniqueID
				and K57Primary.ProcedureCode like 'K57%'
				and K57Primary.ProcedureSequence = 1
				)then 'Electro'
			when ProcedureCode = 'A847'	then 'Sleep'
			when ProcedureCode = 'M474'	then 'Urodynamics'
			when ProcedureCode = 'U264'	then 'Urodynamics'
			when ProcedureCode = 'U202'	then 'EchoTOE'
			when left(ProcedureCode, 3) = 'G45'	then 'Gastroscopy'
			when left(ProcedureCode, 3) = 'G55'	then 'Gastroscopy'
			when left(ProcedureCode, 3) = 'G65'	then 'Gastroscopy'
			when left(ProcedureCode, 3) = 'G80'	then 'Gastroscopy'
			when left(ProcedureCode, 3) = 'H22'	then 'Colonoscopy'
			when left(ProcedureCode, 3) = 'H28'	then 'Colonoscopy'
			when left(ProcedureCode, 3) = 'H25'	then 'Sigmoidoscopy'
			when left(ProcedureCode, 3) = 'M30'	then 'Cystoscopy'
			when left(ProcedureCode, 3) = 'M45'	then 'Cystoscopy'
			when left(ProcedureCode, 3) = 'M77'	then 'Cystoscopy'
			else null
		end
into #DiagnosticProcedures
from APC.AllProcedures AllProcs
where ProcedureCode in (
	'K582'	--Electro2
	,'A847'	--Sleep
	,'M474'	--Urodynamics
	,'U264'	--Urodynamics
	,'U202'	--'EchoTOE'
	)
or LEFT(ProcedureCode, 3) in (
	'G45'	--Gastroscopy
	,'G55'	--Gastroscopy
	,'G65'	--Gastroscopy
	,'G80'	--Gastroscopy
	,'H22'	--Colonoscopy
	,'H28'	--Colonoscopy
	,'H25'	--Sigmoidoscopy
	,'M30'	--Cystoscopy
	,'M45'	--Cystoscopy
	,'M77'	--Cystoscopy
	)
	
	
--Populate ElectiveActivity
truncate table DM01.ElectiveActivity
insert into DM01.ElectiveActivity

select distinct
	fce.FacilityID
	,patient.PatientSurname
	,patient.PostCode
	,fce.ResponsibleCommissionerCCGOnlyCode
	,fce.Specialty
	,spell.AdmissionMethodNHSCode
	,fce.EpisodeEndDateTime
	,ExamName = 
		case
			when Electro.DiagnosticProcedure is not null
			then Electro.DiagnosticProcedure
			when Sleep.DiagnosticProcedure is not null
			then Sleep.DiagnosticProcedure
			when Urodynamics.DiagnosticProcedure is not null
			then Urodynamics.DiagnosticProcedure
			when Gastroscopy.DiagnosticProcedure is not null
			then Gastroscopy.DiagnosticProcedure
			when Colonoscopy.DiagnosticProcedure is not null
			then Colonoscopy.DiagnosticProcedure
			when Sigmoidoscopy.DiagnosticProcedure is not null
			then Sigmoidoscopy.DiagnosticProcedure
			when Cystoscopy.DiagnosticProcedure is not null
			then Cystoscopy.DiagnosticProcedure
			when EchoTOE.DiagnosticProcedure is not null
			then EchoTOE.DiagnosticProcedure
		end

from APC.Episode fce
inner join #DiagnosticProcedures diag
	on diag.EpisodeSourceUniqueID = fce.EpisodeUniqueID
left join APC.Patient patient
	on patient.EpisodeUniqueID = fce.EpisodeUniqueID
left join APC.Spell spell
	on spell.SourceSpellNo = fce.SourceSpellNo
left join #DiagnosticProcedures Electro
	on Electro.EpisodeSourceUniqueID = fce.EpisodeUniqueID
	and Electro.DiagnosticProcedure = 'Electro'
left join #DiagnosticProcedures Sleep
	on Sleep.EpisodeSourceUniqueID = fce.EpisodeUniqueID
	and Sleep.DiagnosticProcedure = 'Sleep'
left join #DiagnosticProcedures Urodynamics
	on Urodynamics.EpisodeSourceUniqueID = fce.EpisodeUniqueID
	and Urodynamics.DiagnosticProcedure = 'Urodynamics'
left join #DiagnosticProcedures Gastroscopy
	on Gastroscopy.EpisodeSourceUniqueID = fce.EpisodeUniqueID
	and Gastroscopy.DiagnosticProcedure = 'Gastroscopy'
left join #DiagnosticProcedures Colonoscopy
	on Colonoscopy.EpisodeSourceUniqueID = fce.EpisodeUniqueID
	and Colonoscopy.DiagnosticProcedure = 'Colonoscopy'
left join #DiagnosticProcedures Sigmoidoscopy
	on Sigmoidoscopy.EpisodeSourceUniqueID = fce.EpisodeUniqueID
	and Sigmoidoscopy.DiagnosticProcedure = 'Sigmoidoscopy'
left join #DiagnosticProcedures Cystoscopy
	on Cystoscopy.EpisodeSourceUniqueID = fce.EpisodeUniqueID
	and Cystoscopy.DiagnosticProcedure = 'Cystoscopy'
left join #DiagnosticProcedures EchoTOE
	on EchoTOE.EpisodeSourceUniqueID = fce.EpisodeUniqueID
	and EchoTOE.DiagnosticProcedure = 'EchoTOE'

where spell.AdmissionMethodNHSCode in ('11', '12', '13')
--and fce.EpisodeEndDateTime between @startdate and @enddate

select @RowsInserted = @@rowcount

drop table #DiagnosticProcedures

--Write to log
select @Elapsed = DATEDIFF(SECOND, @StartTime, getdate())
select @Stats = 'Rows added to DM01.ElectiveActivity = ' + CONVERT(varchar(10), @RowsInserted) 
		+ '. Time elapsed ' + CONVERT(varchar(6), @Elapsed) + ' seconds'
	
exec WriteAuditLogEvent 'WHREPORTING - DM01 BuildElectiveActivity', @Stats, @StartTime