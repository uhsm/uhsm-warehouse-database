﻿CREATE Procedure [DM01].[EchoToe_WL_Breaches]

as 
set nocount on

SELECT
    ID
    ,[EntryStatus]
   ,[patient_name_last]
   ,[patient_number]
   ,[WeekWait]            
   ,[ElectiveAdmissionMethod]
   ,[FinalPCT]   
   ,[PRACTICE_CODE]
   ,[Specialty]  
   ,[GeneralComment]
   ,[ProcedureCode]
   ,[Date]
   ,[Diagnostic12]
   ,[DM01 LOOKUP]
   ,[SixWeekBreach] 
   ,[DiagnosticTarget] 
   ,[ReportMonth]
   ,[Exclude]
   ,[Breach_Validation_Update]
   ,[Breach_Validation_Update_Date]
  FROM [CorporateReports].[DM01].[EchoToeWL_Validation]
  where WeekWait>=6