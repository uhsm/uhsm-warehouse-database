﻿CREATE Procedure [DM01].[Echo_WL_Breaches]

as 
set nocount on
SELECT 
      [ID]
      ,[patient_number]
      ,[patient_name_first]
      ,[patient_name_last]
      ,[DM01 LOOKUP]
      ,[Consultant]
      ,[Activity]
      ,[Ref RAISED Date]
      ,[Ref RECEIVED Date]
      ,[Weeks Waiting]
      ,[6 Week Date]
      ,[Appt Date]
      ,[POST_CODE]
      ,[FinalPCT]
      ,[LoadType]
      ,[LoadName]
      ,[WeekWait]
      ,[ReportMonth]
      ,[Exclude]
      ,[Breach_Validation_Update]
      ,[Breach_Validation_Update_Date]
  FROM [CorporateReports].[DM01].[EchoWL_Validation]
  where WeekWait>=6