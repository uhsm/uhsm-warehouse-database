﻿CREATE Procedure [DM01].[Elective_WL_Breaches]

as 
set nocount on

SELECT  
      Case when [Exam Name] in ('Urodyn','Cysto')
           then 'Urodyn,Cysto'
           when [Exam Name] in ('Colon', 'Gastro', 'Sigmoid')
           then 'Colon, Gastro, Sigmoid'
           when [Exam Name] in ('Sleep')
           then 'Sleep'
           else 'None'
           end                   as Groupfield
      ,ID
      ,[patient_number]
      ,[patient_name_last]
      ,[PRACTICE_CODE]
      ,[FinalPCT]
      ,[Exam Name]
      ,[DM01 LOOKUP]
      ,[WeekWait]
      ,[Specialty]
      ,[EntryStatus]
      ,[ElectiveAdmissionMethod]
      ,[Treatment]
      ,[GeneralComment]
      ,[IPWLWaitingStartDate]
      ,[DateOnList]
      ,[TCIDate]
      ,[DiagnosticTarget]
      ,[WLUniqueID]
      ,[LoadType]
      ,[LoadName]
      ,[ReportMonth]
      ,[Exclude]
      ,[Breach_Validation_Update]
      ,[Breach_Validation_Update_Date]
  FROM [CorporateReports].[DM01].[ElectiveWL_Validation]
  where WeekWait>=6