﻿CREATE Procedure [DM01].[Forecast_CombinedWL]
	(
    @Level int   /* 0 = overall level, 1 = Patient level If set to 0 resulting parameters 
                  below are ignored in the code*/
	,@DM01Lookup nvarchar(255)
    ,@Status nvarchar(255) /* = Pulls through the status name eg 'No Appt/TCI' 
							when drilling through. At the the overall level this is defaulted to 'na'
							and is ignored*/
    ,@Weekno varchar (20)
    ,@noofStatus int   /* 0 = selected statuts, 1 = all Status */
    ,@Weeklevel int    /* 0 = single week, 1 = all over 6+, 2 all weeks */
	)
as
set nocount on

/*
Declare @Level int = 0
Declare @DM01Lookup nvarchar(255) ='Computed Tomography'
Declare @Status nvarchar(255) = 'na'
Declare @Weekno varchar (20) = '13+'
Declare @noofStatus int = 1
Declare @Weeklevel int = 0 
*/

 /*xxxxxxxxxxxxxxxxxxxxxxxxxxxx*/

/*code below has been added so it can be joined further on to ensure every line will have all the status types, 
regardless of whether they have any data for a particular status type(in which case SSRS will add a 0 for that status type) in SSRS*/ 
Create table #status
(
    [status] nvarchar(255)
)
Insert into #status ([status]) 
values ('Appt/TCI before Month End')
       ,('Appt/TCI after Month End') 
       ,('No Appt/TCI') 
       ,('Appt/TCI date in past')
  /*xxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
/*code below has been added so it can be joined further on to ensure every line will have all the weeknumbers, 
regardless of whether they have any data for a particular weeknumber(in which case SSRS will add a 0 for that weeknumbers) in SSRS*/ 
 
 Create table #weeknumber
(
[Weekno] int
,[Weeknogrp] nvarchar(100)
,[ForecastWeekWait] int
,[ForecastWeekWaitgrp] nvarchar(100)

)
Insert into #weeknumber ([Weekno],[Weeknogrp],[ForecastWeekWait],[ForecastWeekWaitgrp]) 
values (0,'0',0,'0')
       ,(1,'1',1,'1')
    ,(2,'2',2,'2')
    ,(3,'3',3,'3')
    ,(4,'4',4,'4')
    ,(5,'5',5,'5')
    ,(6,'6',6,'6')
    ,(7,'7',7,'7')
    ,(8,'8',8,'8')
    ,(9,'9',9,'9')
    ,(10,'10',10,'10')
    ,(11,'11',11,'11')
    ,(12,'12',12,'12')
    ,(13,'13+',13,'13+')

/*xxxxxxxxxxxxxxxxxxxxxxxxxxxx*/

  
  Create table #consolidated
  (
    CensusDate datetime
    ,patient_number varchar (255)
	,patient_name_last varchar (255)
	,[Exam] varchar (255)
	,[DM01 LOOKUP Group] varchar (255)
	,[DM01 LOOKUP] varchar (255)
	,Consultant nvarchar (255)
	,ConsultantCode nvarchar (255)
	,GeneralComment nvarchar (max)
	,ReferralPriority varchar(255)
	,ReferredToSpecialtyCode varchar(255)
    ,ReferredToSpecialty varchar(255)
    ,PatientPracticeCode varchar(255)
	,WLStartDate  datetime
	,DateOnList datetime
	,TCIDate datetime
	,[WeekWait] int
	,[Status] varchar (255)
	,[ForecastWeekWait] int
	,[Total] int
	,[6wkBreachesPotential] int 
	,[LoadName] varchar (255)
  )
  
 /*xxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
/*AudIPM*/
insert into #consolidated
SELECT
      cast(audipm.CensusDate as Date)                 as CensusDate
      ,audipm.[PatientId]                             as patient_number
      ,audipm.[PatientSurname]                        as patient_name_last
      ,'Audiology Assessments'                        as [Exam]
      ,uuc.DM01Lookupgrp                              as [DM01 LOOKUP Group]
      ,'Audiology - Audiology Assessments'            as [DM01 LOOKUP]
      ,audipm.ReferredToConsultant                    as Consultant
	  ,audipm.ReferredToConsultantCode                as ConsultantCode
      ,Null                                           as GeneralComment
      ,ReferralPriority
      ,ReferredToSpecialtyCode
      ,ReferredToSpecialty
      ,Contract_ReferringPracticeCode                 as PatientPracticeCode
      ,audipm.[RTTReferralDate]                       as WLStartDate
      ,audipm.[WaitingListStartDate]                  as DateonList
      ,audipm.[CurrentAppointmentDate] 
      ,floor(datediff(d,audipm.[RTTReferralDate],GETDATE())/7)        as WeekWait
      ,case when audipm.[CurrentAppointmentDate] < cast(GETDATE() as date) then 'Appt/TCI date in past'
            when audipm.[CurrentAppointmentDate] <= DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)) 
            then 'Appt/TCI before Month End'
            when audipm.[CurrentAppointmentDate] > DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)) 
            then 'Appt/TCI after Month End' 
            when audipm.[CurrentAppointmentDate] is null then 'No Appt/TCI' 
       end                                                     as [Status]
        /* 
    Forecast weeks wait calculation based on Procedure WH.[PTL].[LoadOPWL]
    and portion that creates PTL.AdmittedWaitingListAll table
    	,RTTWeeksWait = 
			case
				when Referral.OriginalProviderReferralDate (DateOnReferrersWL on OPWL) 
				< Included.CABAdjustedReferralDate 
					then datediff(day, Referral.OriginalProviderReferralDate, GETDATE())/7
				else datediff(day, Included.CABAdjustedReferralDate, GETDATE())/7
			end
     */                                            
       ,Case when audipm.DateOnReferrersWL < audipm.CABAdjustedReferralDate  
        then 
        DATEDIFF (d,audipm.DateOnReferrersWL,DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)))/7
        else
        DATEDIFF (d,audipm.CABAdjustedReferralDate,DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)))/7 
        end
        as [ForecastWeekWait]
        
      ,case when audipm.[CurrentAppointmentDate] <= DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)) 
            then 1
            when audipm.[CurrentAppointmentDate] > DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)) 
            then 1 
            when audipm.[CurrentAppointmentDate] is null then  1      
            else 0 
       end                                                     as [Total] 
      ,Case when 
			    case when     
					 floor(datediff(d,audipm.[RTTReferralDate],DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1))/7)<0 
					 then 0
					 else floor(datediff(d,audipm.[RTTReferralDate],DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1))/7)
				end    
		     >=6 then 1 else 0 end                              as [6wkBreachesPotential] 

      ,'AudiologyIPM'                                           as LoadName  

 FROM  [WH].[PTL].[OPWL] audipm left join /*Source table for [WH].[PTL].[OPWLArchive]*/
/* replaced by link to live table [WH].[PTL].[OPWLArchive] audipm left join */
     CorporateReports.DM01.UHSMLUCodes uuc on 'Audiology - Audiology Assessments' = uuc.[DM01 LOOKUP]
  where  [ReferredToSpecialty] like '%Audiology%'
        /*
        For use whn query was looking at Archive table-
        and CensusDate = DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) 
         /*Last day of previous month*/   
         */   
/*xxxxxxxxxxxxxxxxxxxx*/
/*Elective WL*/
Insert into #consolidated
Select 
       ewl.CensusDate 
      ,ewl.patient_number
      ,ewl.patient_name_last
      ,ewl.[Exam]                       
      ,uuc.DM01Lookupgrp                    as [DM01 LOOKUP Group]
      ,uuc.[DM01 LOOKUP]                    as [DM01 LOOKUP]
      ,ewl.Consultant
      ,ewl.ConsultantCode
      ,ewl.GeneralComment
      ,ewl.ReferralPriority
      ,ewl.ReferredToSpecialtyCode
      ,ewl.ReferredToSpecialty     
      ,ewl.PatientPracticeCode
      ,ewl.WLStartDate
      ,ewl.[DateOnList]
      ,ewl.[TCIDate]
      ,ewl.WeekWait
      ,ewl.[Status]
      ,ewl.[ForecastWeekWait]
      ,ewl.[Total] 
      ,ewl.[6wkBreachesPotential] 
      ,ewl.LoadName 
From
(
SELECT
      ewl.CensusDate 
      ,ewl.DistrictNo                       as patient_number
      ,ewl.PatientSurname                   as patient_name_last
      ,case 
              when left(PlannedProcedure1, 4) = 'K582'
              then 'Electro'
              when left(PlannedProcedure1, 4) in ('A847', 'U331')
              then 'Sleep'
              when left(PlannedProcedure1, 3) = 'H25'
              then 'Sigmoid'
              when left(PlannedProcedure1, 3) in ('H22', 'H28', 'H20') --H20 added July 2015 from Mar 15 new guidance
              then 'Colon'
              when left(PlannedProcedure1, 3) in ('M30', 'M45', 'M77', 'M42', 'M65')--M42, M65 added July 2015 from Mar 15 new guidance
              then 'Cysto'
              when left(PlannedProcedure1, 3) in ('G80', 'G65', 'G55', 'G45', 'G14', 'G16', 'G17', 'G19', 'G43')
              --G14, G16, G17, G19, G19, G43 added July 2015 from Mar 15 new guidance
              then 'Gastro'
              when left(PlannedProcedure1, 4) = 'M474'
              then 'Urodyn'
              when left(PlannedProcedure1, 4) = 'U202'
              then 'TOE'
              else null
	    end                                 as [Exam]
      ,ewl.SpecialtyDescription             as Specialty
      ,ewl.Consultant
      ,ewl.ConsultantCode
      ,ewl.GeneralComment
      ,ewl.ReferralPriority
      ,ewl.SpecialtyCode                    as ReferredToSpecialtyCode    
      ,ewl.SpecialtyDescription             as ReferredToSpecialty 
      ,ewl.ReferringPracticeCode            as PatientPracticeCode  
      ,ewl.[IPWLWaitingStartDate]           as WLStartDate
      ,ewl.[DateOnList]
      ,ewl.[TCIDate]
      ,case when     
		ewl.IPWLWaitNowWeeksAdjNoRTT <0 
		 then 0
		 else ewl.IPWLWaitNowWeeksAdjNoRTT
		 end                                  as WeekWait
      ,case when ewl.[TCIDate] < cast(GETDATE() as date) then 'Appt/TCI date in past'
       when ewl.[TCIDate] <= DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)) 
            then 'Appt/TCI before Month End'
            when ewl.[TCIDate] > DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)) 
            then 'Appt/TCI after Month End' 
            when ewl.[TCIDate] is null then 'No Appt/TCI' 
       end    
    /* 
    Forecast weeks wait calculation based on Procedure RTT_DW.[PTL].[BuildAdmittedPTL] 
    and portion that creates PTL.AdmittedWaitingListAll table
    ,IPWLWaitNowWeeksAdjNoRTT = --As used in former IPDC snapshot on InfoSQL for DM01
            case  
                  when SuspToDate is null
                  then (datediff(day, WaitingStartDate, GETDATE()))/7
                  else (datediff(day, WaitingStartDate, GETDATE()) - SuspToDate)/7
     */                                             as [Status]
       ,Case when ewl.SuspToDate is null
        then 
        DATEDIFF (d,ewl.[IPWLWaitingStartDate],DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)))/7
        else
        DATEDIFF (d,ewl.[IPWLWaitingStartDate],DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)) - ewl.SuspToDate)/7 
        end
        as [ForecastWeekWait]
      ,case when ewl.[TCIDate] <= DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)) 
            then 1
            when ewl.[TCIDate] > DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)) 
            then 1 
            when ewl.[TCIDate] is null then  1      
            else 0 
       end                                                     as [Total] 
      ,Case when 
			    case when     
					 ewl.IPWLWaitNowWeeksAdjNoRTT <0 
					 then 0
					 else ewl.IPWLWaitNowWeeksAdjNoRTT
				end    
		     >=6 then 1 else 0 end                              as [6wkBreachesPotential] 
      ,'ElectiveWL'                                             as LoadName 
 /*,ewl.ArchiveDate*/
  FROM RTT_DW.PTL.Admitted ewl 
  /*
  Left join
   replaced by live source data and join moved outside of sub query
  WHREPORTING.DM01.ElectiveWaitingList ewl  Left join 
     CorporateReports.DM01.UHSMLUCodes uuc on ewl.Diagnostic = uuc.UHSMCODE
  where 
  For use whn query was looking at Archive table
  ewl.ArchiveDate = Dateadd(month, Datediff(month, 0, Getdate()), 0)
  and ewl.Diagnostic is not null
  */ 
  ) ewl
  Left join 
     CorporateReports.DM01.UHSMLUCodes uuc on ewl.[Exam]= uuc.UHSMCODE
  Where ewl.[Exam] is not null

/*xxxxxxxxxxxxxxxxx*/
/*EchoToe WL */
Insert into #consolidated
SELECT
	etwl.CensusDate
	,etwl.DistrictNo                     as patient_number
    ,etwl.PatientSurname                 as patient_name_last
    ,'ECHO TOE'                          as [Exam]
    ,uuc.DM01Lookupgrp                   as [DM01 LOOKUP Group]
    ,'Cardiology - echocardiography'     as [DM01 LOOKUP]
    ,etwl.Consultant
    ,etwl.ConsultantCode
    ,etwl.GeneralComment
    ,etwl.ReferralPriority
    ,etwl.SpecialtyCode                  as ReferredToSpecialtyCode    
    ,etwl.SpecialtyDescription           as ReferredToSpecialty
    ,etwl.ReferringPracticeCode          as PatientPracticeCode  
    ,DATEADD(day, -42,(DATEADD(day, 42, etwl.IPWLWaitingStartDate)))   as WLStartDate
    ,null                                as DateonList
	,etwl.TCIDate
	,etwl.IPWLWaitNowWeeksUnAdjNoRTT     as WeekWait
      ,case when etwl.[TCIDate] < cast(GETDATE() as date) then 'Appt/TCI date in past'
       when etwl.[TCIDate] <= DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)) 
            then 'Appt/TCI before Month End'
            when etwl.[TCIDate] > DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)) 
            then 'Appt/TCI after Month End' 
            when etwl.[TCIDate] is null then 'No Appt/TCI' 
       end      as [Status]
    /* 
    Forecast weeks wait calculation based on Procedure RTT_DW.[PTL].[BuildAdmittedPTL] 
    and portion that creates PTL.AdmittedWaitingListAll table
    ,IPWLWaitNowWeeksAdjNoRTT = --As used in former IPDC snapshot on InfoSQL for DM01
            case  
                  when SuspToDate is null
                  then (datediff(day, WaitingStartDate, GETDATE()))/7
                  else (datediff(day, WaitingStartDate, GETDATE()) - SuspToDate)/7
     */                                           
       ,Case when etwl.SuspToDate is null
        then 
        DATEDIFF (d,etwl.[IPWLWaitingStartDate],DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)))/7
        else
        DATEDIFF (d,etwl.[IPWLWaitingStartDate],DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)) - etwl.SuspToDate)/7 
        end
        as [ForecastWeekWait]
      ,case when etwl.TCIDate <= DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)) 
            then 1
            when etwl.TCIDate > DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)) 
            then 1 
            when etwl.TCIDate is null then  1      
            else 0 
       end                                                     as [Total] 
      ,Case when 
			    case when     
					 etwl.IPWLWaitNowWeeksUnAdjNoRTT <0 
					 then 0
					 else etwl.IPWLWaitNowWeeksUnAdjNoRTT
				end    
		     >=6 then 1 else 0 end                              as [6wkBreachesPotential] 
	,'Echo_Toe'                                                 as LoadName  
 /*,ewl.ArchiveDate*/
  FROM RTT_DW.PTL.Admitted etwl Left Join
       CorporateReports.DM01.UHSMLUCodes uuc on 'Cardiology - echocardiography' = uuc.[DM01 LOOKUP]
Where 
etwl.EntryStatus <> 'SUS'
and etwl.PlannedProcedure1 like 'U202%'
and etwl.GeneralComment not like '%$T%'
and etwl.ElectiveAdmissionMethod <> '13'
  /*
   replaced by live source data 
WHREPORTING.DM01.EchoTOEWaitingList etwl  Left join 
*/
/*
where etwl.ArchiveDate = Dateadd(month, Datediff(month, 0, Getdate()), 0) 
  For use whn query was looking at Archive table
  */ 


/*xxxxxxxxxxxxxxxxx*/
/*Radiology WL*/

/*Waiting list filter case statement, used in the DM01 to split out the
data for validation by the various dept, has been moved into exam column.
This has been done to avoid the need to another column just for this field,
which would need to be added as nulls in the DM01 areas. As the exam field is not being
used in radiology it seems the best place to keep this*/
Insert into #consolidated
Select
	CensusDate
	,PatientNumber
	,PatientName
	,Exam
	,[DM01 LOOKUP Group]
	,[DM01 LOOKUP]
	,Consultant
	,ConsultantCode
	,GeneralComment
	,ReferralPriority
	,SpecialtyName
	,ReferringSpecialty
	,RegisteredPracticeCode
	,RequestDate
	,DateBooked
	,EventDate
	,WeeksWait
	,[Status]
	,ForecastWeekWait
	,Total
	,6wkBreachesPotential
	,LoadName
From
(
Select distinct
    GETDATE()                       as CensusDate
    ,pat.HospitalID                 as PatientNumber
    ,a.PatientName
    ,case when w.PTLCategory in ('CARDIAC MR','CARDIAC CT')
	      then 'CardiacMRCT'
	      when a.ExamCode = 'UVOCA'
	      then 'Vocal Chord Ultrasound'
	      when a.ExamCode in ('ZUCHESB', 'ZUCHESA')
	      then 'Bronchoscopy'
	      when w.PTLCategory = 'Non Obs US Not Radiology'
	      then 'Breast'
	      when w.PTLCategory = 'Hycosy'
	      then 'Hycosy'
	      else 'Main Radiology'
	      end                       as Exam
    ,ul.DM01Lookupgrp               as [DM01 LOOKUP Group]
    ,ul.[DM01 LOOKUP]
    ,e.ReferrerName                 as Consultant
    ,e.ReferrerCode                 as ConsultantCode
    ,null                           as GeneralComment
    ,a.EventUrgency                 as ReferralPriority 
    ,e.SpecialtyName       
    ,a.ReferringSpecialty
    ,a.RegisteredPracticeCode
    ,a.RequestDate
    ,e.DateBooked
    ,e.EventDate
    ,floor(datediff(d,case when a.RequestDate = '20990101' then e.DateBooked
                      else a.RequestDate end
                      ,GETDATE())/7) as WeeksWait
    ,case when e.EventDate = '20990101' 
		then 'No Appt/TCI'
		when e.EventDate < cast(GETDATE() as date) then 'Appt/TCI date in past'
		when e.EventDate <= DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)) 
		then 'Appt/TCI before Month End'
		when e.EventDate > DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)) 
		then 'Appt/TCI after Month End' 
		when e.EventDate is null then 'No Appt/TCI' 
       end                                        as [Status]          
    ,DATEDIFF (d,case when a.RequestDate = '20990101' then e.DateBooked
                      else a.RequestDate end,DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)))/7     as [ForecastWeekWait]
      
    ,case when e.EventDate <= DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)) 
            then 1
            when e.EventDate> DATEADD(s, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE())+1, 0)) 
            then 1 
            when e.EventDate is null then  1      
            else 0 
       end                                                     as [Total] 
    ,Case when 
			    case when     
					 floor(datediff(d,case when a.RequestDate = '20990101' then e.DateBooked
                      else a.RequestDate end,DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1))/7)<0 
					 then 0
					 else floor(datediff(d,case when a.RequestDate = '20990101' then e.DateBooked
                      else a.RequestDate end,DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1))/7)
				end    
		     >=6 then 1 else 0 end                              as [6wkBreachesPotential] 

    ,'Radiology'                                           as LoadName  

FROM [DW_REPORTING].[CRIS].[CurrentWaitersPTL] a
	  Left join CorporateReports.DM01.UHSMLUCodes ul 
	  on (
	  case when a.[Modality] = 'Radiology' then 'DEXA'
		 when a.[Modality] = 'Ultrasound'
		 then 'US'
		 when a.[Modality] = 'MRI' then 'MR'
		 else a.[Modality] 
	 end ) 
	 = ul.UHSMCODE
	 Left Join DW_REPORTING.CRIS.Exams ex on a.EventKey = ex.EventKey --for Wasplanned field
     Left Join DW_REPORTING.CRIS.Patient pat on a.PatientKey = pat.PatientKey --for RM2 number
     Left Join DW_REPORTING.CRIS.Waiters w on a.ExamsKey = w.ExamsKey --for PTL category
     Left Join DW_REPORTING.CRIS.[Events] e on a.EventKey = e.EventKey
WHERE a.PatientGroup = 'Outpatient'
	  AND a.DM01Flag = 'Y'
	  and a.ExamCode not in  ('UXRELL' ,'ZRXTI','ZEFOBR4','ZEFOBR3','ZEFOBR5','UMAWL','UMAWR')    --X:  Plain Film
	  and a.ExamName not like '%Doppler%'--VS:  Vascular Studies#
	  and ex.WasPlanned = 'No'
) x where Exam not in ('CardiacMRCT')
 /*xxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
  /*Ensures every DM01 has all the status types, for use in SSRS to ensure better presntation*/
 SELECT distinct
  c.[DM01 LOOKUP Group]
  ,c.[DM01 LOOKUP]
  ,s.[status]
Into #status2
  FROM #consolidated c 
  inner join #status  s on 1=1 
 order by    c.[DM01 LOOKUP Group]
  ,c.[DM01 LOOKUP]
  ,s.[status]

/*xxxxxxxxxxxxxxxxxxxx*/

 Select
      s.[DM01 LOOKUP Group]
      ,s.[DM01 LOOKUP]
      ,s.[Status]
      ,c.CensusDate
      ,c.[patient_number]
      ,c.[patient_name_last]
      ,c.[Exam]
      ,c.Consultant
      ,c.ConsultantCode
      ,c.GeneralComment
	  ,c.ReferralPriority
      ,c.ReferredToSpecialtyCode
      ,c.ReferredToSpecialty
      ,c.PatientPracticeCode
      ,c.[WLStartDate]
      ,c.[DateOnList]
      ,c.[TCIDate]
      ,case when c.WeekWait is null
            then 0
            else c.WeekWait 
            end                                                        as WeekWait
      ,case when c.WeekWait is null
            then '0'
            when c.WeekWait >= 13 
            then '13+' 
            else cast(c.WeekWait as varchar (50)) end                  as Weekwaitgrp
      ,case when c.[ForecastWeekWait] is null
            then 0
            else c.[ForecastWeekWait]
            end                                                        as [ForecastWeekWait]
      ,case when c.[ForecastWeekWait] is null
            then '0'
            when c.[ForecastWeekWait] >= 13 
            then '13+' 
            else cast(c.[ForecastWeekWait] as varchar (50)) end       as ForecastWeekwaitgrp
      ,case when c.[patient_number] is null then 0 else c.[Total] end  as [Total]
      ,Case when c.[6wkBreachesPotential] is null 
			then 0
			else c.[6wkBreachesPotential] 
			end                                                        as [6wkBreachesPotential]
      ,Case when c.[ForecastWeekWait] >=  6 
			then 1
			else 0
			end                                                        as [Forecast6wkBreachesPotential]
      ,c.LoadName 
into #unfilteredresult
from  #status2 s left join #consolidated  c 
      on s.[DM01 LOOKUP] = c.[DM01 LOOKUP] and s.[Status] = c.[Status] 


/*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/

/*This next section ensures that every DM01 lookup has all 4 status within the SSRS report,
with 0 values where it isn't in the main data. Purely for presentational purposes with the SSRS report*/
Insert into #unfilteredresult
 Select
      r.[DM01 LOOKUP Group]
      ,r.[DM01 LOOKUP]
      ,r.[Status]
      ,null as CensusDate
      ,null as [patient_number]
      ,null as [patient_name_last]
      ,null as [Exam]
      ,null as Consultant
      ,null as ConsultantCode
      ,null as GeneralComment
      ,null as ReferralPriority
      ,null as ReferredToSpecialtyCode
      ,null as ReferredToSpecialty
      ,null as PatientPracticeCode
      ,null as [WLStartDate]
      ,null as [DateOnList]
      ,null as [TCIDate]
      ,w.[Weekno]
      ,w.[Weeknogrp]
      ,w.[ForecastWeekWait]
      ,w.ForecastWeekwaitgrp
      ,0    as Total
      ,0    as [6wkBreachesPotential]
      ,0    as [Forecast6wkBreachesPotential]
      ,null as LoadName 
From  #weeknumber w inner join #status2 r on 1=1
where
    @Level = 0  -- Denotes that the SP is at an overall level so every record is returned

/*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
/* The next stage filters the results and also allows for the drill through to be added to the 6+
total potential breaches and total columns */

Select * 
into #Results
from #unfilteredresult c
where 

       @Level = 0  -- Denotes that the SP is at an overall level so every record is returned
      or
   (
      @Level = 1  --Denotes that the SP is at an patient level so only records that fit the parameters below are returned
      and 
      (
      @noofStatus = 0 --Denotes that only certain status's are to be included (parameter for this further below)
      and c.[Status] = @Status
      )
      and c.[DM01 LOOKUP] = @DM01Lookup
      
      and 
      (
      (@Weeklevel = 0  and c.ForecastWeekwaitgrp = @Weekno)
       or 
      (@Weeklevel = 1  and c.ForecastWeekWait >= 6)  --6 week Breach potential
      or
      (@Weeklevel = 2 )                              -- all Forecast waiters regardless week
      )
   )
      or 
  (
      @Level = 1--Denotes that the SP is at an patient level so only records that fit the parameters below are returned
      and @noofStatus = 1 --Denotes that all status's are to be included
      and c.[DM01 LOOKUP] = @DM01Lookup
      and 
      (
      (@Weeklevel = 0  and c.ForecastWeekwaitgrp = @Weekno)
       or 
      (@Weeklevel = 1  and c.ForecastWeekWait >= 6)  --6 week Breach potential
      or
      (@Weeklevel = 2 )                              -- all Forecast waiters regardless week
       )
   )

/*xxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
Select * from #Results Where [DM01 LOOKUP Group] is not null

/*xxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
Drop table #status, #consolidated,#status2, #weeknumber,#unfilteredresult, #Results