﻿CREATE Procedure [DM01].[Neuro_WL_Breaches]

as 
set nocount on

SELECT  
      ID
      ,[patient_number]
      ,[patient_name_first]
      ,[patient_name_last]
      ,[Referring Consultant]
      ,[Dept]
      ,[POST_CODE]
      ,[Inpatient / Outpatient]
      ,[Test Type]
      ,[Complex]
      ,[Date Referral Received]
      ,[Date request for a Diagnostic Procedure is made]
      ,[Clock Reset Date]
      ,[Breach Date]
      ,[Date Patient due for Diagnostic Procedure]
      ,[In Month Breach?]
      ,[True Breach]
      ,[Date Patient Received Diagnostic Procedure]
      ,[Comments]
      ,[FinalPCT]
      ,[DM01 LOOKUP]
      ,[WeekWait]
      ,[LoadType]
      ,[LoadName]
      ,[ReportMonth]
      ,[Exclude]
      ,[Breach_Validation_Update]
      ,[Breach_Validation_Update_Date]
  FROM [CorporateReports].[DM01].[NeuroWL_Validation]
    where WeekWait>=6