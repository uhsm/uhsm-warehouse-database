﻿CREATE Procedure [DM01].[Radiology_WL_Breaches]

as 


/*
As of july 2015 6 seperate breach valifdation fWaitinglist_filteriles are produced so the filters with the dataset of
SSRS will be utilised, on Waitinglist_filter and weeksWait
*/
SELECT  [ID]
      ,[PatientID]
      ,[PatientNumber]
      ,[PatientName]
      ,[AgeAtExamination]
      ,[DateOfBirth]
      ,[NhsNumber]
      ,[RequestDate]
      ,[PatientType]
      ,[PatientGroup]
      ,[EventUrgency]
      ,[BookingMode]
      ,[ExamStatusCategory]
      ,[OrderingLocation]
      ,[ReferringClinician]
      ,[ReferringSpecialty]
      ,[PatientKey]
      ,[Modality]
      ,[TargetDate]
      ,[BreachDate]
      ,[WeeksWait]
      ,[statuscode]
      ,[statuslongdesc]
      ,[BookingStatus]
      ,[BreachStatus]
      ,[CCGCode]
      ,[RegisteredPracticeCode]
      ,[PatientPostcode]
      ,[DM01 LOOKUP]
      ,[PTLCategory]
      ,[Exclude]
      ,[Breach_Validation_Update]
      ,[Breach_Validation_Update_Date]
      ,[LoadType]
      ,[LoadName]
      ,[ReportMonth]
      ,Waitinglist_filter
  FROM [CorporateReports].[DM01].[RadiologyWL_Validation]
 -- where WeekSWait>=6