﻿CREATE TABLE [DM01].[ElectiveActivity](
	[FacilityID] [varchar](20) NULL,
	[PatientSurname] [varchar](30) NULL,
	[PostCode] [varchar](25) NULL,
	[ResponsibleCommissionerCCGOnlyCode] [varchar](10) NULL,
	[Specialty] [varchar](255) NULL,
	[AdmissionMethodNHSCode] [varchar](50) NULL,
	[EpisodeEndDateTime] [smalldatetime] NULL,
	[ExamName] [varchar](20) NULL
) ON [PRIMARY]