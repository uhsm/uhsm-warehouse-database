﻿CREATE view [DM01].[EchoTOEWaitingList] as

/**
--Author: K Oakden
--Date: 19/08/2014
--Based on Qry_ElectiveWL_EchoToe_Validation in DM01_csvLoad_Master.accdb

Required for Echo TOE Waiting List section of monthly DM01
Based on AdmittedArchive table in RTT_DW which contains all Admitted PTL
snapshots. The filter to specify a given month is handled in the 
Access database
**/

select 
	DistrictNo
	,PatientSurname
	,EntryStatus
	,IPWLWaitNowWeeksUnAdjNoRTT
	,ElectiveAdmissionMethod
	,CCG = PracticePCTCode
	,GPPracticeCode
	,SpecialtyDescription
	,GeneralComment = dbo.CleanString(GeneralComment) --use Claen string as some text is being truncated ?? possible CR/LF
	,ProcedureCode = left(PlannedProcedure1, 4) 
	,TCIDate
	--,TCIDate2 = 
	--	case 
	--		when TCIDate is null 
	--		then 'No Date'
	--		when TCIDate > '2007-06-30 00:00:00.000'
	--		then 'No Date Breach'
	--		else 'Dated'
	--	end
	
	,Diagnostic12 = 'ECHO TOE'
		--case	
		--	when PlannedProcedure1 like 'U202%'
		--		then 'ECHO TOE'
		--	when PlannedProcedure1 like 'K499%'
		--		then 'CORONARY ANGIO'
		--	when (PlannedProcedure1 like '%592%')
		--	or (PlannedProcedure1 like '%593%')
		--	or (PlannedProcedure1 like '%594%')
		--	or (PlannedProcedure1 like '%595%')
		--	or (PlannedProcedure1 like '%601%')
		--	or (PlannedProcedure1 like '%603%')
		--	or (PlannedProcedure1 like '%607%')
		--		then 'IMPLANTABLE DEVICES'
		--	else null
		--end

	,DM01Name = 'Cardiology - echocardiography'  
	,SixWeekBreach = DATEADD(day, 42, IPWLWaitingStartDate)
	,DiagnosticTarget
	,CensusDate = CONVERT(VARCHAR(8), GETDATE(), 112)
	,ArchiveDate
from RTT_DW.PTL.AdmittedArchive

where EntryStatus <> 'SUS'
and PlannedProcedure1 like 'U202%'
and GeneralComment not like '%$T%'
and ElectiveAdmissionMethod <> '13'