﻿CREATE view [DM01].[ElectiveWaitingList] as

/**
Author: K Oakden
Date: 01/08/2014
Based on Qry_Validation_ElectiveWL in DM01_csvLoad_Master.accdb

Required for Elective Waiting List section of monthly DM01
Based on AdmittedArchive table in RTT_DW which contains all Admitted PTL
snapshots. The filter to specify a given month is handled in the 
Access database

Additional procedure codes added following new guidance march 2015
**/

--select * from [V-UHSM-DW02].RTT_DW.DM01.ElectiveWaitingList
select * from 
(
	select 
		EntryStatus
		,DistrictNo
		,IPWLWaitNowWeeksAdjNoRTT
		,IPWLWaitNowWeeksUnAdjNoRTT
		,ElectiveAdmissionMethod
		,Diagnostic = 
            case 
                  when left(PlannedProcedure1, 4) = 'K582'
                  then 'Electro'
                  when left(PlannedProcedure1, 4) in ('A847', 'U331')
                  then 'Sleep'
                  when left(PlannedProcedure1, 3) = 'H25'
                  then 'Sigmoid'
                  when left(PlannedProcedure1, 3) in ('H22', 'H28', 'H20') --H20 added July 2015 from Mar 15 new guidance
                  then 'Colon'
                  when left(PlannedProcedure1, 3) in ('M30', 'M45', 'M77', 'M42', 'M65')--M42, M65 added July 2015 from Mar 15 new guidance
                  then 'Cysto'
                  when left(PlannedProcedure1, 3) in ('G80', 'G65', 'G55', 'G45', 'G14', 'G16', 'G17', 'G19', 'G43')
                  --G14, G16, G17, G19, G19, G43 added July 2015 from Mar 15 new guidance
                  then 'Gastro'
                  when left(PlannedProcedure1, 4) = 'M474'
                  then 'Urodyn'
                  when left(PlannedProcedure1, 4) = 'U202'
                  then 'TOE'
                  else null
            end
		,ProcedureCode = left(PlannedProcedure1, 4) 
		,CCG = PracticePCTCode
		,SpecialtyDescription
		,Treatment = case when GeneralComment like '%$T$' then 'Treatment' else 'Diag' end
		,GPPracticeCode
		,GeneralComment = dbo.CleanString(GeneralComment) --use Claen string as some text is being truncated ?? possible CR/LF
		,IPWLWaitingStartDate
		,DateOnList
		--,OriginalDateOnList
		,TCIDate
		,DiagnosticTarget
		,WLUniqueID
		,PatientSurname
		,CensusDate = CONVERT(VARCHAR(8), GETDATE(), 112)
		,ArchiveDate
	from RTT_DW.PTL.AdmittedArchive
	where SuspendedEndDate is null
)admitted

where Diagnostic is not null
and GeneralComment not like '%$T%'
and ElectiveAdmissionMethod <> '13'