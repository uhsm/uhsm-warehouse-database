﻿CREATE view [DM01].[QuarterlyCardiologyWaitingList] as

/**
Author: K Oakden
Date: 16/10/2014
Based on Qry_Cardiology_Biannual NEW in S:\CO-HI-Information\Waiting Lists-1\General\WL CDS\WL CDS 2011_12New.mdb

Required for quarterly diagnostic returns
Based on AdmittedArchive table in RTT_DW which contains all Admitted PTL
snapshots. The filter to specify a given month is handled in the 
Access database
**/

select 
	Diagnostic = 
		case 
			when ProcedureCode like 'U202%' 
				then 'ECHO TOE'
			when ProcedureCode like 'K499%' 
				then 'CORONARY ANGIO'
			when left(ProcedureCode, 4) in  ('K592', 'K593', 'K594', 'K595', 'K601', 'K602','K603', 'K607') 
				then 'IMPLANTABLE DEVICES'
			else ''
		end
	,*
 from 
	(select 
		DistrictNo
		,EntryStatus
		,ListType
		,ProcedureCode = left(
			case 
				when LEFT(PlannedProcedure1, 4) = 'NCP '
				then SUBSTRING(PlannedProcedure1, 5, len(PlannedProcedure1) - 4)
				else PlannedProcedure1
			end
			, 4) 

		,PlannedProcedure = 
			case 
				when LEFT(PlannedProcedure1, 4) = 'NCP '
				then SUBSTRING(PlannedProcedure1, 5, len(PlannedProcedure1) - 4)
				else PlannedProcedure1
			end
		,IPWLWaitingStartDate = convert(varchar, IPWLWaitingStartDate, 103)
		,TCIDate = convert(varchar, TCIDate, 103)
		,IPWLWaitNowWeeksAdjNoRTT
		,SixWeekBreach = convert(varchar, dateadd(day, 42,IPWLWaitingStartDate) , 103)
		--6wkbreach does not match existing logic (probably need to refer back to IPWL)
		--however don't know if this column is even used as these are not diagnostic procedures
		,PatientSurname
		,SpecialtyDescription
		,Consultant
		,ELAdmission
		,GeneralComment
		,ArchiveDate
	from RTT_DW.PTL.AdmittedArchive
	where ElectiveAdmissionMethod <> '13'

)arch
where LEFT(ProcedureCode, 3) in ('U20', 'K59', 'K60', 'K61', 'K49')
and EntryStatus <> 'SUS'
--and Archivedate = '20141001'