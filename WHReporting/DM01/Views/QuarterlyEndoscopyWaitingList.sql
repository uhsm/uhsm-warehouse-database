﻿CREATE view [DM01].[QuarterlyEndoscopyWaitingList] as

/**
Author: K Oakden
Date: 16/10/2014
Based on Qry_Endoscopic_Biannual NEW in S:\CO-HI-Information\Waiting Lists-1\General\WL CDS\WL CDS 2011_12New.mdb

Required for quarterly diagnostic returns
Based on AdmittedArchive table in RTT_DW which contains all Admitted PTL
snapshots. The filter to specify a given month is handled in the 
Access database
**/

select 
	Endo2Code = 
		case 
			when ProcedureCode like 'E25%' then 'Pharynx'
			when ProcedureCode like 'E36%' then 'Layrynx'
			when ProcedureCode like 'J43%' then 'Bile/Pancreatic Duct'
			when ProcedureCode like 'P273%' then 'Colposcopy'
			when ProcedureCode like 'Q18%' then 'Uterus'
			when ProcedureCode like 'Q39%' then 'Fallopian Tube'
			when ProcedureCode like 'Q50%' then 'Ovary'
			when ProcedureCode like 'T43%' then 'Peritoneum'
			when ProcedureCode like 'W87%' then 'Knee Joint'
			when ProcedureCode like 'W88%' then 'Other Joint'
			when ProcedureCode like 'E51%' then 'Lower resp tract'
			when ProcedureCode like 'E63%' then 'Mediastinum'
			when ProcedureCode like 'T11%' then 'Pleura'
			else ''
		end

	,*

 from 
	(select 
		DistrictNo
		,EntryStatus
		,ELAdmission
		,ListType
		,ProcedureCode = left(
			case 
				when LEFT(PlannedProcedure1, 4) = 'NCP '
				then SUBSTRING(PlannedProcedure1, 5, len(PlannedProcedure1) - 4)
				else PlannedProcedure1
			end
			, 4) 

		,PlannedProcedure = 
			case 
				when LEFT(PlannedProcedure1, 4) = 'NCP '
				then SUBSTRING(PlannedProcedure1, 5, len(PlannedProcedure1) - 4)
				else PlannedProcedure1
			end
		,TCIDate = convert(varchar, TCIDate, 103)
		,IPWLWaitNowWeeksAdjNoRTT
		,SpecialtyDescription
		,PatientSurname
		,Consultant
		,DiagnosticTarget = convert(varchar, DiagnosticTarget, 103)
		,IPWLWaitingStartDate = convert(varchar, IPWLWaitingStartDate, 103)
		,GeneralComment
		,ArchiveDate
	from RTT_DW.PTL.AdmittedArchive
	where GeneralComment not like '%$T%'
	and ElectiveAdmissionMethod <> '13'
)arch
where (
	ProcedureCode like 'E25%'
	or ProcedureCode like 'E36%'
	or ProcedureCode like 'J43%'
	or ProcedureCode like 'P273%'
	or ProcedureCode like 'Q18%'
	or ProcedureCode like 'Q39%'
	or ProcedureCode like 'Q50%'
	or ProcedureCode like 'T43%'
	or ProcedureCode like 'W87%'
	or ProcedureCode like 'W88%'
	or ProcedureCode like 'E51%'
	or ProcedureCode like 'E63%'
	or ProcedureCode like 'T11%'
	or ProcedureCode like 'G16%'
	or ProcedureCode like 'G19%'
	or ProcedureCode like 'J09%'
	or ProcedureCode like 'J44%'
	or ProcedureCode like 'J45%'
	or ProcedureCode like 'E49%'
	or ProcedureCode like 'M11%'
	or ProcedureCode like 'A18%'
	or ProcedureCode like 'N343%'
	)
and EntryStatus <> 'SUS'
--and Archivedate = '20141001'