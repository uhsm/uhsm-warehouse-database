﻿CREATE view [DM01].[SleepActivity] as
/**
--Author: K Oakden
--Date: 20/08/2014
--Based on Qry_SleepActivity in New Extracts database

Required for Sleep activity section of monthly DM01

**/
select schedule.SpecialtyCode
	,schedule.NHSNo
	,patient.PatientSurname
	,schedule.AppointmentType
	,AppointmentDate
	--,AppointmentDate = convert(varchar, schedule.AppointmentDate, 103)
	,schedule.AttendStatus
	,schedule.Outcome
	,schedule.ClinicCode
	,referral.ReferralReceivedDate
	,referral.ReferralSource
	,referral.RequestedService
	--,schedule.PurchaserCode
	,schedule.AppointmentGPCode
	,schedule.AppointmentPracticeCode
	,schedule.AppointmentCCGCode
from OP.Schedule schedule
left join OP.Patient patient
	on patient.SourceUniqueID = schedule.SourceUniqueID
left join RF.Referral referral
	on schedule.ReferralSourceUniqueID = referral.ReferralSourceUniqueID
where schedule.AttendStatus in (
	'Attended'
	,'Attended on Time'
	,'Patient Late / Seen'
	)
and schedule.ClinicCode in (
	'ASSTP1'
	,'ASSORS'
	,'ASSOC'
	,'ASSPRN'
	)
and schedule.CancelledDateTime is null
		
--order by schedule.AppointmentDateTime desc