﻿--from NewExtracts.mdb
CREATE view [DM01].[SleepWaitingList] as

/**
Author: K Oakden
Date: 01/08/2014
Based on Qry_SleepWL from NewExtracts.mdb

Required for Pulse Oximetry Waiting List section of monthly DM01

--TO do - once correct convert to stored proc to be run on 1st of month
**/

select 
	schedule.SpecialtyCode
	,schedule.NHSNo
	,patient.PatientSurname
	,schedule.AppointmentDate
	,schedule.AppointmentType
	,schedule.AttendStatus
	,schedule.Outcome
	,schedule.ClinicCode
	,referral.ReferralReceivedDate
	,referral.ReferralSource
	,referral.RequestedService
	,LastSleepAppt.LastAppointmentDate
	,referral.ReferralSourceUniqueID
	,schedule.AppointmentGPCode
	,schedule.AppointmentPracticeCode
	,schedule.AppointmentCCGCode
	,schedule.AppointmentCCG
	,referral.ReferralCompletionComments
from OP.Schedule schedule 
left join OP.Patient patient
	on patient.SourceUniqueID = schedule.SourceUniqueID
left join RF.Referral referral
	on schedule.ReferralSourceUniqueID = referral.ReferralSourceUniqueID
left join (
	select 
		ReferralSourceUniqueID, LastAppointmentDate = max(AppointmentDate)
	from OP.Schedule op 
	where op.SpecialtyCode = '340'
		and op.ClinicCode like 'ASS%'
		and op.AttendStatus in (
					'Attended'
					,'Attended on Time'
					,'patient Late / Seen'
				)
		and op.AppointmentType = 'New'
	group by ReferralSourceUniqueID
)LastSleepAppt
	on LastSleepAppt.ReferralSourceUniqueID = schedule.ReferralSourceUniqueID

where schedule.AttendStatus = 'Not Specified'
and schedule.ClinicCode in (
	'ASSTP1'
	,'ASSTP5'
	,'ASSOC'
	)
			
and referral.ReferralCompletionComments like '%diag%'
and schedule.CancelledDateTime is null

--select top 1000 * from rtt.referral_dataset 
--where REF_COMP_COMMENTS is not null 
--and REF_STATUS_COMMENTS is not null 
--order by Ref_ref desc

--select * from rtt.referral_dataset where Ref_ref =  156306117
--select * from RF.Referral where ReferralSourceUniqueID =  156306117