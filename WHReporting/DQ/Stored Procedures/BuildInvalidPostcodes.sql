﻿/*===============================================================================
Author:     Jubeda Begum
Date:       24 September 2014
Function:   Current Patients without a valid lookup postcode to [OrganisationCCG].[dbo].[Postcode] table                 
Version:    1.0
=================================================================================*/

CREATE proc [DQ].[BuildInvalidPostcodes]

AS

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

Truncate Table dbo.InvalidPostcodes

INSERT INTO dbo.InvalidPostcodes

SELECT 
SourcePatientNo
,FacilityID
,PatientForename
,PatientSurname
,PatientAddress1
,PatientAddress2
,PatientAddress3
,PatientAddress4
,A.Postcode AS Postcode

FROM [WHREPORTING].[LK].[PatientCurrentDetails] A 


LEFT OUTER JOIN [OrganisationCCG].[dbo].[Postcode] B
ON REPLACE(A.POSTCODE, ' ','') = B.SlimPostcode

WHERE B.SlimPostcode IS NULL

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

	select @Stats = 
		'Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

	exec WriteAuditLogEvent 'PAS - WHREPORTING BuildInvalidPostcodes', @Stats, @StartTime