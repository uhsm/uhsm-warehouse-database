﻿/*===============================================================================
Author:     J Smithson
Date:       29 Oct 2012
Function:   Time taken between discharge and date Coded, For SSRS Date Coded Report	
                  
Version:    1.0
=================================================================================*/
--DQ.DateCoded '1 Apr 2012', '29 Oct 2012'

CREATE proc [DQ].[DateCoded]


	 @StartDate Date, 
	 @EndDate Date
	 as 
SELECT 
      MAX(CreateDate)as DateCoded
      ,MAX(DateDiff("D", DischargeDate, CreateDate)) as DaysCodedAfterDischarge
      ,CASE		  WHEN MAX(CreateDate) IS NULL THEN 'Not Coded'
                  WHEN MAX(DateDiff("D", DischargeDate, CreateDate)) <= 0 Then 'Coded Prior To Discharge'
                  WHEN MAX(DateDiff("D", DischargeDate, CreateDate)) < 14 THEN '1 to 13 Days'
                  else 'On or after day 14'                  
                  
            END AS 'Check'
       ,CASE 
             WHEN MAX(DateDiff("D", DischargeDate, CreateDate)) <= 0 Then '0'
             WHEN MAX(DateDiff("D", DischargeDate, CreateDate)) = 1 Then '1'
             WHEN MAX(DateDiff("D", DischargeDate, CreateDate)) = 2 Then '2'
             WHEN MAX(DateDiff("D", DischargeDate, CreateDate)) = 3 Then '3'
             WHEN MAX(DateDiff("D", DischargeDate, CreateDate)) = 4 Then '4'
             WHEN MAX(DateDiff("D", DischargeDate, CreateDate)) = 5 Then '5'
             WHEN MAX(DateDiff("D", DischargeDate, CreateDate)) = 6 Then '6'
             WHEN MAX(DateDiff("D", DischargeDate, CreateDate)) = 7 Then '7'
             WHEN MAX(DateDiff("D", DischargeDate, CreateDate)) = 8 Then '8'
             WHEN MAX(DateDiff("D", DischargeDate, CreateDate)) = 9 Then '9'
             WHEN MAX(DateDiff("D", DischargeDate, CreateDate)) = 10 Then '10'
             WHEN MAX(DateDiff("D", DischargeDate, CreateDate)) = 11 Then '11'
             WHEN MAX(DateDiff("D", DischargeDate, CreateDate)) = 12 Then '12'
             WHEN MAX(DateDiff("D", DischargeDate, CreateDate)) = 13 Then '13'
             else 'On or after day 14'                  
                  
            END AS DaysCodedCheck
      ,SourceSpellNo
      ,FacilityID
      ,AdmissionDate
      ,DischargeDate
      ,Division
      ,[SpecialtyCode]
      ,[Specialty]
      ,CodingComplete
      ,CC.PrimaryDiagnosis
      ,CC.PriamryProcedure
      ,RIGHT (Calendar.FinancialMonthKey,2)as FinMonth
      ,Calendar.FinancialYear
FROM APC.Episode EPI
LEFT JOIN APC.ClinicalCoding CC
on EPI.EncounterRecno = CC.EncounterRecno
LEFT OUTER JOIN APC.AllDiagnosis Diag
ON CC.EncounterRecno = Diag.EncounterRecno
LEFT JOIN LK.Calendar on EPI.DischargeDate = Calendar.TheDate

--WHERE CC.EncounterRecno = '767209'
where DischargeDate between @StartDate and @EndDate
 
GROUP BY SourceSpellNo
                  ,CreateDate
            ,FacilityID
            ,AdmissionDate
            ,DischargeDate
            ,Division
            ,[SpecialtyCode]
            ,[Specialty]
            ,CodingComplete
            ,CC.PrimaryDiagnosis
            ,CC.PriamryProcedure
            ,Calendar.FinancialMonthKey
            ,Calendar.FinancialYear
            Having MAX(CreateDate) IS not NULL
ORDER BY DaysCodedAfterDischarge