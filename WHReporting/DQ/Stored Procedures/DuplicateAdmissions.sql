﻿/*===============================================================================
Author:		J Pitchforth
Date:		21 May 2013
Function:	Identifies patients who have been admitted more than once on the same day,
			to the same specialty.
Version:	1.0
=================================================================================
*/
--DQ.DuplicateAdmissions '01 Apr 2013','30 Apr 2013'

CREATE Procedure [DQ].[DuplicateAdmissions]

@StartDate date,
@EndDate date

as

select 
Spell.FaciltyID, 
Patient.PatientForename,
Patient.PatientSurname,
Spell.SourcePatientNo,
--Episode.EpisodeUniqueID,
Spell.AdmissionDate,
Spell.[AdmissionSpecialtyCode(Function)],
count(0) as Admissions

from WHREPORTING.APC.Spell
--inner join WHReporting.APC.Episode on Spell.SourceSpellNo = Episode.SourceSpellNo
inner join WHREPORTING.APC.Patient on Spell.EncounterRecno = Patient.EncounterRecno

where Spell.AdmissionDate between @StartDate and @EndDate

group by
Spell.FaciltyID, 
Patient.PatientForename,
Patient.PatientSurname,
Spell.SourcePatientNo,
--Episode.EpisodeUniqueID,
Spell.AdmissionDate,
Spell.[AdmissionSpecialtyCode(Function)]
having count(0) > 1

order by Spell.FaciltyID