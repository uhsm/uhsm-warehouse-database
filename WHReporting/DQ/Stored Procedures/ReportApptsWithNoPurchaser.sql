﻿/******************************************************************************
 Description: Data Quality - Outpatient Appointments with no purchaser code
  
 Parameters:  @StartDate - Start date for the report
              @EndDate - End date for the report
 
 Modification History --------------------------------------------------------

 Date     Author		  Change
 22/09/14 Jubeda Begum	  Created

******************************************************************************/
CREATE PROCEDURE [DQ].[ReportApptsWithNoPurchaser]
 @StartDate AS DATE
,@EndDate AS DATE
as

SET nocount ON

SELECT 
      S.[FacilityID]
      ,S.[NHSNo]
      ,P.PatientSurname
      ,P.PatientForename
      ,P.DateOfBirth
      ,P.PatientAddress1
      ,P.PatientAddress2
      ,P.PatientAddress3
      ,P.PatientAddress4
      ,P.PostCode
      ,S.AppointmentGPCode
      ,S.AppointmentPracticeCode
      ,S.AppointmentPractice
      ,S.AppointmentPCT
      ,S.AppointmentCCGCode
      ,S.AppointmentCCG
      ,S.AppointmentDate
      ,S.PurchaserCode
      ,S.Purchaser
      ,S.[ClinicCode]
  FROM [WHREPORTING].[OP].[Schedule] S
  LEFT OUTER JOIN OP.Patient P
  ON S.SourceUniqueID = P.SourceUniqueID
  WHERE AppointmentDate Between @StartDate And @EndDate
  and Purchaser IS NULL
  and AttendStatusCode = '5'
  and IsWardAttender <> 1
  Order by S.AppointmentDate