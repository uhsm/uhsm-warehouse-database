﻿-- =============================================
-- Author:	CMan
-- Create date: 14/11/2013
-- Description:Stored Procedure for report to look at Caasenote tracking per day by user.
--				Coding and logic for this is taken from the view Lorenzo.dbo.vwCasenoteTrackingforSpecificUser
--				Requested by PMcconnell

-- =============================================
CREATE PROCEDURE [DQ].[ReportCasenoteTrackingForSpecficUser] @start datetime, @end datetime, @user bigint
	-- Add the parameters for the stored procedure here

AS
BEGIN

SELECT FaciltyID = DistrictNo.IDENTIFIER,
       CaseNoteID = Casenote.IDENTIFIER,
       StatusDate = Cast(Movement.RQSTA_DTTM AS DATE),
       StatusTime = Cast(Movement.RQSTA_DTTM AS DATETIME),
       [Status] = RequestStatus.[DESCRIPTION]
       --	,StatusDate = Movement.RQSTA_DTTM
       --	,StartDate = Movement.START_DTTM
       --	,iPMCreateDate = Movement.CREATE_DTTM
       ,
       [Location] = ServicePoint.DESCRIPTION,
       ipmUser.[code],
       ipmUser.[USER_NAME],
       UsualLocation = VolUsual.[DESCRIPTION],
       VolNo = Volume.SERIAL,
       TrackingComments = Movement.COMMENTS
FROM   Lorenzo.dbo.CasenoteActivity Movement
       INNER JOIN Lorenzo.dbo.CasenoteVolume Volume
               ON Movement.CASVL_REFNO = Volume.CASVL_REFNO
       INNER JOIN Lorenzo.dbo.Casenote Casenote
               ON Volume.CASNT_REFNO = Casenote.CASNT_REFNO
       LEFT OUTER JOIN Lorenzo.dbo.ReferenceValue RequestStatus
                    ON Movement.RQSTA_REFNO = RequestStatus.RFVAL_REFNO
       LEFT OUTER JOIN Lorenzo.dbo.ServicePoint ServicePoint
                    ON Movement.REQBY_SPONT_REFNO = ServicePoint.SPONT_REFNO
       LEFT OUTER JOIN Lorenzo.dbo.[User] ipmUser
                    ON Movement.Reqby_users_refno = ipmuser.users_refno
       LEFT OUTER JOIN Lorenzo.dbo.ServicePoint VolUsual
                    ON Volume.STORE_SPONT_REFNO = VolUsual.SPONT_REFNO
       LEFT OUTER JOIN Lorenzo.dbo.PatientIdentifier DistrictNo
                    ON DistrictNo.PATNT_REFNO = Casenote.PATNT_REFNO
                       AND DistrictNo.PITYP_REFNO = 2001232 --facility
                       AND DistrictNo.IDENTIFIER LIKE 'RM2%'
                       AND DistrictNo.ARCHV_FLAG = 'N'
                       AND NOT EXISTS (SELECT 1
                                       FROM   Lorenzo.dbo.PatientIdentifier Previous
                                       WHERE  Previous.PATNT_REFNO = DistrictNo.PATNT_REFNO
                                              AND Previous.PITYP_REFNO = DistrictNo.PITYP_REFNO
                                              AND Previous.IDENTIFIER LIKE 'RM2%'
                                              AND Previous.ARCHV_FLAG = 'N'
                                              AND ( Previous.START_DTTM > DistrictNo.START_DTTM
                                                     OR ( Previous.START_DTTM = DistrictNo.START_DTTM
                                                          AND Previous.PATID_REFNO > DistrictNo.PATID_REFNO ) ))
WHERE  ( ( Movement.CREATE_DTTM >= @start
           AND Movement.CREATE_DTTM < @end )
          OR ( Movement.MODIF_DTTM >=  @start
               AND Movement.MODIF_DTTM <  @end )
          OR ( Movement.RQSTA_DTTM >=  @start
               AND Movement.RQSTA_DTTM <@end ) )
       AND REQBY_USERS_REFNO = @user
       AND Movement.ARCHV_FLAG = 'N'


--select * from dbo.[User] where users_refno = 10014718



Order by  DistrictNo.IDENTIFIER


END