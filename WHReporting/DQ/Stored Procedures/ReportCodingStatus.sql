﻿/*
--Author: I Sarwar
--Date created: 30/01/2013
--Stored Procedure Built for SRSS Report where coding status is incomplete or awaiting results
*/


CREATE Procedure [DQ].[ReportCodingStatus]
@CodingStatus AS VARCHAR(1)

as

select DISTINCT 
FaciltyID
--,SPL.SourceSpellNo
,Pat.PatientForename + ' ' + Pat.PatientSurname AS PatName
,CodingComplete 
,SPL.AdmissionDate
,SPL.DischargeDate
,DATENAME(MONTH,SPL.DischargeDate) + ' ' + DATENAME(Year,SPL.DischargeDate) AS MonthYear 
,DischargeMethod
,EPI.ConsultantName
,EPI.[Specialty(Function)]
,CurrentCasenoteLocation
,COALESCE(CurrentCasenoteLocation.CurrentCasenoteLocationCode,'N/A') AS LocationCode
,CasenoteMovementCommets
,UserModified
from APC.Spell SPL
LEFT JOIN APC.ClinicalCoding CC
on SPL.EncounterRecno = CC.EncounterRecno

LEFT JOIN APC.Patient Pat
ON SPL.EncounterRecno = Pat.EncounterRecno

LEFT JOIN APC.Episode EPI
ON SPL.SourceSpellNo = EPI.SourceSpellNo

left outer join Lorenzo.dbo.vwCurrentCasenoteLocationLastMoved CurrentCasenoteLocation
on SPL.SourcePatientNo = CurrentCasenoteLocation.SourcePatientNo

left join APC.AllDiagnosis D
on SPL.EncounterRecno = D.EncounterRecno

WHERE CC.CodingComplete IN (@CodingStatus)
AND SPL.DischargeDate >= '01 jun 2014'
AND EPI.LastEpisodeInSpell = 1
order by SPL.DischargeDate