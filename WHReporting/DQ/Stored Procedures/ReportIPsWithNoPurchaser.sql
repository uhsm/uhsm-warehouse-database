﻿/******************************************************************************
 Description: Data Quality - Epsiodes with no purchaser code
  
 Parameters:  @StartDate - Start date for the report
              @EndDate - End date for the report
 
 Modification History --------------------------------------------------------

 Date     Author		  Change
 22/09/14 Jubeda Begum	  Created

******************************************************************************/
Create PROCEDURE [DQ].[ReportIPsWithNoPurchaser]
 @StartDate AS DATE
,@EndDate AS DATE
as

SET nocount ON

SELECT 
P.PatientSurname
,P.PatientForename
,P.DateOfBirth
,E.NHSNo
,E.FacilityID
,P.PatientAddress1
,P.PatientAddress2
,P.PatientAddress3
,P.PatientAddress4
,P.PostCode
,E.EpisodicPracticeCode
,E.EpisodeStartDate
,E.EpisodeEndDate

FROM APC.Episode E
LEFT OUTER JOIN APC.Patient P
ON E.EncounterRecno = P.EncounterRecno
WHERE EpisodeStartDate BETWEEN @StartDate AND @EndDate
AND PurchaserCode IS NULL


ORDER BY EpisodeStartDate