﻿/******************************************************************************
 Description: Data Quality - ReportInvalidPostcode
  
 Parameters:  @StartDate - Start date for the report
              @EndDate - End date for the report
 
 Modification History --------------------------------------------------------

 Date     Author		  Change
 22/09/14 Jubeda Begum	  Created

******************************************************************************/
CREATE PROCEDURE [DQ].[ReportInvalidPostcode]
 @StartDate AS DATE
,@EndDate AS DATE
,@Activity AS Varchar(20)
as

SET nocount ON


SELECT DISTINCT
IP.SourcePatientNo
,NHSNo
,IP.FacilityID
,PatientForename
,PatientSurname
,PatientAddress1
,PatientAddress2
,PatientAddress3
,PatientAddress4
,IP.Postcode
,'Outpatients' AS Activity
FROM dbo.InvalidPostcodes IP
INNER JOIN WHReporting.OP.Schedule OP
ON IP.SourcePatientNo = OP.SourcePatientNo
WHERE OP.AppointmentDate Between @StartDate And @EndDate
AND 'Outpatients'
IN (SELECT Item
                         FROM   dbo.Split (@Activity, ','))
  
UNION ALL
  
SELECT DISTINCT
IP.SourcePatientNo
,NHSNo
,IP.FacilityID
,PatientForename
,PatientSurname
,PatientAddress1
,PatientAddress2
,PatientAddress3
,PatientAddress4
,IP.Postcode
,'Inpatients' AS Activity
FROM dbo.InvalidPostcodes IP
INNER JOIN WHReporting.APC.Episode EP
ON IP.SourcePatientNo = EP.SourcePatientNo
WHERE EP.DischargeDate Between @StartDate And @EndDate
AND 'Inpatients' IN (SELECT Item
                         FROM   dbo.Split (@Activity, ','))
ORDER BY IP.FacilityID