﻿/******************************************************************************
 Description: Data Quality - Married Minors SSRS Report
   
 Modification History --------------------------------------------------------

 Date     Author      Change
 10/12/13 Tim Dean	  Created

******************************************************************************/
CREATE PROCEDURE [DQ].[ReportMarriedMinors]
as

SET nocount ON

SELECT p.PATNT_REFNO,
       PID.IDENTIFIER,
       p.FORENAME,
       p.SURNAME,
       P.DTTM_OF_BIRTH                                                               DOB,
       Cast(Cast(( Getdate() - p.DTTM_OF_BIRTH ) AS INT) / 365.25 AS DECIMAL(10, 1)) AGE,
       p.MARRY_REFNO_DESCRIPTION                                                     MARTIAL_STATUS,
       CASE
         WHEN c.[USER_NAME] IS NULL THEN p.USER_CREATE
         ELSE c.[USER_NAME]
       END                                                                           CREATOR,
       p.CREATE_DTTM                                                                 CREATE_DATE,
       CASE
         WHEN m.[USER_NAME] IS NULL THEN p.USER_MODIF
         ELSE m.[USER_NAME]
       END                                                                           MODIFIER,
       p.MODIF_DTTM                                                                  MODIF_DATE,
       p.PDS_UPDATE_NEEDED
FROM   Lorenzo.dbo.Patient p
       LEFT JOIN Lorenzo.dbo.PatientIdentifier pid
              ON ( p.PATNT_REFNO = pid.PATNT_REFNO )
       LEFT JOIN Lorenzo.dbo.[User] c
              ON ( p.USER_MODIF = c.CODE )
       LEFT JOIN Lorenzo.dbo.[User] m
              ON ( p.USER_MODIF = m.CODE )
WHERE  p.ARCHV_FLAG = 'N'
       AND Cast(( Getdate() - p.DTTM_OF_BIRTH ) AS INT) / 365.25 < 16
       AND p.MARRY_REFNO_DESCRIPTION IN ( 'Married', 'Divorced', 'Separated', 'Widowed' )
       AND NOT p.SURNAME LIKE '%TEST%'
       AND pid.PITYP_REFNO = 2001232