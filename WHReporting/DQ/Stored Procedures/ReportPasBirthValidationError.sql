﻿/******************************************************************************
 Description: Data Quality - PAS Birth Validation Error SSRS Report
  
 Parameters:  @StartDate - Start date for the report
              @EndDate - End date for the report
 
 Modification History --------------------------------------------------------

 Date     Author      Change
 11/12/13 Tim Dean	  Created

******************************************************************************/
CREATE PROCEDURE [DQ].[ReportPasBirthValidationError]
 @StartDate AS DATE
,@EndDate AS DATE
as

SET nocount ON

SELECT PasDetails.PasBirthEpisodeRecno,
       PasDetails.PasBirthEpisodeNumber,
       PasDetails.PasBirthEpisodeSourceSpellNo,
       PasDetails.PasBabyDateOfBirth,
       PasDetails.PasBabyHospitalNumber,
       PasDetails.PasBabySurname,
       PasDetails.PasBabyFirstname,
       PasDetails.PasBirthDetailsSourceUniqueID,
       PasDetails.PasOutcomeOfBirth,
       PasDetails.PasAdmissionMethod,
       PasDetails.PasAdmissionSource,
       PasDetails.PasBabyBirthOrder,
       PasDetails.PasNumberOfBabies,
       PasDetails.PasPregnancyNumber,
       PasDetails.PasMotherHospitalNumber,
       PasDetails.PasMotherSurname
FROM   (SELECT PasBirthEpisode.EncounterRecno     PasBirthEpisodeRecno,
               PasBirthEpisode.EpisodeNo          PasBirthEpisodeNumber,
               PasBirthEpisode.SourceSpellNo      PasBirthEpisodeSourceSpellNo,
               PasBirthEpisode.DateOfBirth        PasBabyDateOfBirth,
               PasBirthEpisode.DistrictNo         PasBabyHospitalNumber,
               PasBirthEpisode.PatientSurname     PasBabySurname,
               PasBirthEpisode.PatientForename    PasBabyFirstname,
               PasBirthDetails.SourceUniqueID     PasBirthDetailsSourceUniqueID,
               BirthStatus.ReferenceValue         PasOutcomeOfBirth,
               CASE
                 WHEN PasBirthDetails.LiveOrStillBirthCode IS NULL THEN 1
                 ELSE 0
               END                                PasBirthStatusValid,
               AdmissionMethod.MappedCode + ' - '
               + AdmissionMethod.ReferenceValue   PasAdmissionMethod,
               AdmissionSource.MainCode + ' - '
               + AdmissionSource.ReferenceValue   PasAdmissionSource,
               PasBirthDetails.BirthOrder         PasBabyBirthOrder,
               CASE
                 WHEN PasBirthDetails.BirthOrder IS NULL THEN 1
                 ELSE 0
               END                                PasBabyBirthOrderValid,
               PasDeliveryDetails.NumberOfBabies  PasNumberOfBabies,
               CASE
                 WHEN PasDeliveryDetails.NumberOfBabies IS NULL THEN 1
                 ELSE 0
               END                                PasNumberOfBabiesValid,
               PasDeliveryDetails.PregnancyNumber PasPregnancyNumber,
               PasDeliveryEpisode.DistrictNo      PasMotherHospitalNumber,
               PasDeliveryEpisode.NHSNumber       PasMotherNHSNumber,
               PasDeliveryEpisode.PatientSurname  PasMotherSurname
        FROM   WH.APC.Encounter PasBirthEpisode -- iPM baby birth episode
               -- iPM baby birth details =============================================
               LEFT JOIN WH.APC.Birth PasBirthDetails
                      ON ( PasBirthEpisode.SourcePatientNo = PasBirthDetails.SourcePatientNo )
               -- iPM delivery details ===============================================
               LEFT JOIN WH.APC.MaternitySpell PasDeliveryDetails
                      ON ( PasBirthDetails.MaternitySpellSourceUniqueID = PasDeliveryDetails.SourceUniqueID
                           AND PasBirthDetails.MotherSourcePatientNo = PasDeliveryDetails.SourcePatientNo )
               -- iPM mother delivery episode ========================================
               LEFT JOIN WH.APC.Encounter PasDeliveryEpisode
                      ON ( PasDeliveryDetails.EpisodeSourceUniqueID = PasDeliveryEpisode.SourceUniqueID )
               -- iPM Live or still birth code lookup =========================================
               LEFT JOIN WH.PAS.ReferenceValue BirthStatus
                      ON ( PasBirthDetails.LiveOrStillBirthCode = BirthStatus.ReferenceValueCode
                           AND BirthStatus.ReferenceDomainCode = 'BIRST' )
               -- iPM Admission method code lookup =========================================
               LEFT JOIN WH.PAS.ReferenceValue AdmissionMethod
                      ON ( PasBirthEpisode.AdmissionMethodCode = AdmissionMethod.ReferenceValueCode
                           AND AdmissionMethod.ReferenceDomainCode = 'ADMET' )
               -- iPM Admission source code lookup =========================================
               LEFT JOIN WH.PAS.ReferenceValue AdmissionSource
                      ON ( PasBirthEpisode.AdmissionSourceCode = AdmissionSource.ReferenceValueCode
                           AND AdmissionSource.ReferenceDomainCode = 'ADSOR' )
        WHERE  PasBirthEpisode.AdmissionDate BETWEEN @StartDate AND @EndDate
               AND PasBirthEpisode.AdmissionDate = PasBirthEpisode.DateOfBirth -- method to identify a birth
               AND PasBirthEpisode.EpisodeNo = 1 -- make sure we only have first episode
               AND AdmissionMethod.MappedCode = '82' -- only need babies born in hospital
       ) PasDetails
WHERE  ( PasDetails.PasBirthStatusValid
         + PasDetails.PasBabyBirthOrderValid
         + PasDetails.PasNumberOfBabiesValid ) > 0 
ORDER BY PasDetails.PasBabyDateOfBirth