﻿/******************************************************************************
 Description: Data Quality - Unrecorded Attendance and Outcome SSRS Report
  
 Parameters:  @StartDate - Start date for the report
              @EndDate - End date for the report
              @Division -  Divisions required on the report
              @Directorate - Directorates required on the report
              @Specialty - Specialties required on the report
              @AttendOutcomeStatus - Attend outcome statuses required on the report
 
 Modification History --------------------------------------------------------

 Date     Author      Change
 05/12/13 Tim Dean	  Created
 30/05/13 Tim Dean    AttendStatus has been changed in DW from NULL 
                      to 'Not Specified' amended query to match.

******************************************************************************/
CREATE PROCEDURE [DQ].[ReportUnrecordedAttendanceAndOutcome]
 @StartDate AS DATE
,@EndDate AS DATE
,@Division AS NVARCHAR(4000)
,@Directorate AS NVARCHAR(4000)
,@Specialty AS NVARCHAR(4000)
,@AttendOutcomeStatus AS NVARCHAR(50)
as

SET nocount ON

-- To improve query performance create tables from the parameters
-- SSRS passes the parameters a CSV strings, sometimes there can
-- be 30+ entries.  
IF Object_id('tempdb..#Directorate', 'U') IS NOT NULL
  DROP TABLE #Directorate

SELECT Item
INTO   #Directorate
FROM   WHREPORTING.dbo.Splitstrings_cte(@Directorate, N',')

IF Object_id('tempdb..#Division', 'U') IS NOT NULL
  DROP TABLE #Division

SELECT Item
INTO   #Division
FROM   WHREPORTING.dbo.Splitstrings_cte(@Division, N',')

IF Object_id('tempdb..#Specialty', 'U') IS NOT NULL
  DROP TABLE #Specialty

SELECT Item
INTO   #Specialty
FROM   WHREPORTING.dbo.Splitstrings_cte(@Specialty, N',')

--===================================================================

SELECT a.Division,
       a.Directorate,
       a.[Specialty(Function)],
       a.ClinicCode,
       CASE
        -- WHEN a.AttendStatus IS NULL THEN 1 -- 30/05/2014 TJD Removed
          WHEN a.AttendStatus = 'Not Specified' THEN 1 -- 30/05/2014 TJD Added
         ELSE 0
       END MissingAttended,
       CASE
         WHEN a.Outcome IS NULL THEN 1  
         ELSE 0
       END MissingOutcome,
       CASE
         --WHEN a.AttendStatus IS NULL -- 30/05/2014 TJD Removed
         WHEN a.AttendStatus = 'Not Specified'  -- 30/05/2014 TJD Added
              OR a.Outcome IS NULL THEN 1 
         ELSE 0
       END MissingTotal
FROM   WHREPORTING.OP.Schedule a
       LEFT JOIN WHREPORTING.OP.Patient p
              ON a.EncounterRecno = p.EncounterRecno
       LEFT JOIN (SELECT DISTINCT s.AdmissiontWardCode WardCode
                  FROM   WHREPORTING.APC.Spell s
                  WHERE  s.AdmissionDate BETWEEN @StartDate AND @EndDate) wards
              ON a.ClinicCode = wards.WardCode -- identify ward attenders
       -- inner join temporary parameter tables to include correct rows
       INNER JOIN #Directorate dir
               ON a.Directorate = dir.Item
       INNER JOIN #Division div
               ON a.Division = div.Item
       INNER JOIN #Specialty sp
               ON a.[Specialty(Function)] = sp.Item
       --===============================================================
WHERE  a.AppointmentDate BETWEEN @StartDate AND @EndDate
       -- only included appointments with no outcome or attendance status
       AND ( a.Outcome IS NULL 
              -- OR a.AttendStatus IS NULL ) -- 30/05/2014 TJD Removed
              OR a.AttendStatus = 'Not Specified' )  -- 30/05/2014 TJD Added               
       AND a.IsWardAttender = 0 -- only op appointments exclude ward attenders
       AND a.CancelledDateTime IS NULL -- exclude any cancelled appointments
       AND ( a.AppointmentDate <= p.DateOfDeath
              OR p.DateOfDeath IS NULL )-- exclude any appointments after patient death 
       AND wards.WardCode IS NULL -- exclude ward attenders
       -- exclude the following clinc codes
       AND a.ClinicCode NOT IN ( 'CABPAIN', 'CABDERM', 'CABUROL', 'CABCARD',
                                 'CABCAS', 'CABPAED', 'PACTEL', 'JOCJP2',
                                 'JOCMON', 'JEFJOC', 'JEFJP2', 'JFGPP2',
                                 'JFGP2N', 'JFGPSI', 'JEFGP2', 'JOB4AM',
                                 'JOB4P2', 'CABGAST', 'CABGAS', 'PASACAB',
                                 'CABBREA', '4ABJOB', 'BJOB4A', 'JEFJO1',
                                 'JOB4AM', 'JOB4P2', 'DEXA-NG' )
       AND a.ClinicCode NOT LIKE '%Test%'
       AND ( CASE
              -- WHEN a.AttendStatus IS NULL THEN 'Non-Attended' -- 30/05/2014 TJD Removed
               WHEN a.AttendStatus = 'Not Specified' THEN 'Non-Attended'  -- 30/05/2014 TJD Added
             END IN (SELECT Item
                     FROM   WHREPORTING.dbo.Splitstrings_cte(@AttendOutcomeStatus, N','))
              OR CASE
                   WHEN a.Outcome IS NULL THEN 'Un-Outcome' 
                 END IN (SELECT Item
                         FROM   WHREPORTING.dbo.Splitstrings_cte(@AttendOutcomeStatus, N',')) )

-- Tidy up temporary parameter tables
IF Object_id('tempdb..#Directorate', 'U') IS NOT NULL
  DROP TABLE #Directorate

IF Object_id('tempdb..#Division', 'U') IS NOT NULL
  DROP TABLE #Division

IF Object_id('tempdb..#Specialty', 'U') IS NOT NULL
  DROP TABLE #Specialty