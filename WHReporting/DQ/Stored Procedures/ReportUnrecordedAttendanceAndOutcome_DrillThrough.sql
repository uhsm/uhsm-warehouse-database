﻿/******************************************************************************
 Description: Used to provide drill through data for the Unrecorded Attendance
              and Outcome Data Quality SSRS report.
  
 Parameters:  @StartDate - Start date of the parent report
              @EndDate - End date of the parent report
              @Division - Current selected division
              @Directorate - Current selected directorate or null
              @Specialty - Current selected specialty or null
              @ClinicCode - Current selected clinic code or null
              @Status - Current selected status or null
 
 Modification History --------------------------------------------------------

 Date     Author      Change
 05/12/13 Tim Dean	  Created
 30/05/13 Tim Dean    AttendStatus has been changed in DW from NULL 
                      to 'Not Specified' amended query to match.

******************************************************************************/

CREATE Procedure [DQ].[ReportUnrecordedAttendanceAndOutcome_DrillThrough]
 @StartDate AS DATE
,@EndDate AS DATE
,@Division AS VARCHAR(200)
,@Directorate AS VARCHAR(200)
,@Specialty AS VARCHAR(200)
,@ClinicCode AS VARCHAR(50)
,@Status AS VARCHAR(20)
AS

DECLARE @Query AS VARCHAR(6000)

SET @Query = '
SELECT a.Division,
       a.Directorate,
       a.[Specialty(Function)],
       a.ClinicCode,
       a.AppointmentDate,
       a.AppointmentTime,
       a.AppointmentType,
       a.AttendStatus,
       a.Outcome,
       a.FacilityID,
       p.PatientSurname
FROM   WHREPORTING.OP.Schedule a
       LEFT JOIN WHREPORTING.OP.Patient p
              ON a.EncounterRecno = p.EncounterRecno
       LEFT JOIN (SELECT DISTINCT s.AdmissiontWardCode
                  FROM   WHREPORTING.APC.Spell s
                  WHERE  s.AdmissionDate BETWEEN '''
             + CONVERT(VARCHAR(10), @StartDate, 101)
             + ''' AND '''
             + CONVERT(VARCHAR(10), @EndDate, 101)
             + ''') temp
              ON a.ClinicCode = temp.AdmissiontWardCode
WHERE  a.AppointmentDate BETWEEN '''
             + CONVERT(VARCHAR(10), @StartDate, 101)
             + ''' AND '''
             + CONVERT(VARCHAR(10), @EndDate, 101)
             + '''
       AND a.IsWardAttender = 0
       AND a.CancelledDateTime IS NULL 
       AND ( a.AppointmentDate <= p.DateOfDeath
              OR p.DateOfDeath IS NULL )  
       AND temp.AdmissiontWardCode IS NULL         
       AND a.ClinicCode NOT IN ( ''CABPAIN'', ''CABDERM'', ''CABUROL'', ''CABCARD'',
                                         ''CABCAS'', ''CABPAED'', ''PACTEL'', ''JOCJP2'',
                                         ''JOCMON'', ''JEFJOC'', ''JEFJP2'', ''JFGPP2'',
                                         ''JFGP2N'', ''JFGPSI'', ''JEFGP2'', ''JOB4AM'',
                                         ''JOB4P2'', ''CABGAST'', ''CABGAS'', ''PASACAB'',
                                         ''CABBREA'', ''4ABJOB'', ''BJOB4A'', ''JEFJO1'',
                                         ''JOB4AM'', ''JOB4P2'', ''DEXA-NG'' )
       AND a.ClinicCode NOT LIKE ''%Test%''
       '

IF @Division IS NOT NULL
  BEGIN
      SET @Query = @Query + 'AND a.Division = '''
                   + @Division + '''
            '
  END

IF @Directorate IS NOT NULL
  BEGIN
      SET @Query = @Query + 'AND a.Directorate = '''
                   + @Directorate + '''
            '
  END

IF @Specialty IS NOT NULL
  BEGIN
      SET @Query = @Query + 'AND a.[Specialty(Function)] = '''
                   + @Specialty + '''
            '
  END

IF @ClinicCode IS NOT NULL
  BEGIN
      SET @Query = @Query + 'AND a.ClinicCode = ''' + @ClinicCode
                   + '''
            '
  END

-- 30/05/2014 TJD Added
IF @Status IS NULL
  BEGIN
      SET @Query = @Query
                   + 'AND ( a.Outcome IS NULL
              OR a.AttendStatus = ''Not Specified'' ) '
  END
  
IF @Status = 'Both'
  BEGIN
      SET @Query = @Query
                   + 'AND ( a.Outcome IS NULL
              OR a.AttendStatus = ''Not Specified'' ) '
  END  
  
IF @Status = 'Outcome'
  BEGIN
      SET @Query = @Query
                   + 'AND a.Outcome IS NULL '
  END

IF @Status = 'Attended'
  BEGIN
      SET @Query = @Query
                   + 'AND a.AttendStatus = ''Not Specified'' '
  END

-- 30/05/2014 TJD Removed
--IF @Status IS NULL
--  BEGIN
--      SET @Query = @Query
--                   + 'AND ( a.Outcome IS NULL
--              OR a.AttendStatus IS NULL ) '
--  END
  
--IF @Status = 'Both'
--  BEGIN
--      SET @Query = @Query
--                   + 'AND ( a.Outcome IS NULL
--              OR a.AttendStatus IS NULL ) '
--  END  
  
--IF @Status = 'Outcome'
--  BEGIN
--      SET @Query = @Query
--                   + 'AND a.Outcome IS NULL '
--  END

--IF @Status = 'Attended'
--  BEGIN
--      SET @Query = @Query
--                   + 'AND a.AttendStatus IS NULL '
--  END

SET @Query = @Query + '
ORDER  BY a.Division,
          a.Directorate,
          a.Specialty,
          a.ClinicCode,
          a.AppointmentDateTime 
          '

EXEC ( @Query )