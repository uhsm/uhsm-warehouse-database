﻿/*
--Author: I Sarwar
--Date created: 30/01/2013
--Stored Procedure Built for SRSS Report where coding status is incomplete or awaiting results
*/


CREATE Procedure [DQ].[ReportVTEAssessmentsNotComplete]


AS

SELECT DISTINCT
SpellNumber
,AddedToTable
,VTEStatus
,VTEDate
,VTEWard
,FacilityID
,LPI.PATNAME
,AdmissionDateTime
,DischargeDateTime
,DATEDIFF(MI,AdmissionDateTime,GETDATE()) AS Mins
,CAST(DATEDIFF(SECOND,AdmissionDateTime, GETDATE()) / 60 / 60 / 24  AS NVARCHAR(50)) + ' Days, '
+ CAST(DATEDIFF(SECOND,AdmissionDateTime, GETDATE()) / 60 / 60 % 24 AS NVARCHAR(50)) + ' Hours, '
+ + CAST(DATEDIFF(SECOND,AdmissionDateTime, GETDATE()) / 60 % 60 AS NVARCHAR(50)) + ' Minutes' AS DaysHrsMins
,AdmissiontWardCode
,LPI.TYPE
FROM WHREPORTING.APC.Spell SPL
LEFT JOIN UHSMApplications.SB.PatientSpellData VTE 
ON VTE.SpellNumber = SPL.SourceSpellNo

LEFT JOIN CQUIN.dbo.srcCurrentInpatientLpi LPI
ON SPL.SourceSpellNo = LPI.ATTNO

WHERE DischargeDate IS Null
AND VTEStatus IS NULL
AND TYPE IS NOT NULL
AND DATEDIFF(MI,AdmissionDateTime,GETDATE()) > 1440 
order by AdmissionDateTime