﻿/*===============================================================================
Author:		J Pitchforth
Date:		23 May 2013
Function:	Identifies patients with a HMP Styal (or HMP Forest Bank) postcode, so 
			that the Data Quality team can amend the purchaser code to Central and
			Eastern Cheshire PCT / CCG.
Version:	1.0
=================================================================================
*/
--DQ.StyalIP '01 Apr 2012','30 Apr 2013'

CREATE Procedure [DQ].[StyalIP]

	@StartDate date,
	@EndDate date

as

select 
Spell.FaciltyID,
Spell.NHSNo,
Patient.PatientForename,
Patient.PatientSurname,
Patient.DateOfBirth,
Spell.AdmissionDate,
Spell.DischargeDate,
Spell.SpellCCG,
Patient.PostCode,
Prison =
	case	when Patient.PostCode like 'SK9%4HR' then 'Styal'
			when Patient.PostCode like 'm27%8sb' then 'Forest Bank'		
	end

from APC.Spell
inner join APC.Patient on Spell.EncounterRecno = Patient.EncounterRecno

where 
Spell.AdmissionDate between @StartDate and @EndDate
and (Patient.PostCode like 'SK9%4HR' or Patient.PostCode like 'm27%8sb')
and Spell.ResponsibleCommissionerCode not in ('5NP','01C') --Central and Eastern Cheshire CCG/PCT

order by 
Spell.AdmissionDate