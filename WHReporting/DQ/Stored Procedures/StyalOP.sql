﻿/*===============================================================================
Author:		J Pitchforth
Date:		23 May 2013
Function:	Identifies patients with a HMP Styal (or HMP Forest Bank) postcode, so 
			that the Data Quality team can amend the purchaser code to Central and
			Eastern Cheshire PCT / CCG.
Version:	1.0
=================================================================================
*/
--DQ.StyalOP '01 Apr 2012','30 Apr 2013'

Create Procedure [DQ].[StyalOP]

	@StartDate date,
	@EndDate date

as


select 
Schedule.FacilityID, 
Schedule.NHSNo,
Schedule.AppointmentPCTCCG as ResponsibleCommissioner,
Schedule.[SpecialtyCode(Function)],
Schedule.AppointmentDate,
Patient.Postcode,

Prison =
	case	when Patient.PostCode like 'SK9%4HR' then 'Styal'
			when Patient.PostCode like 'm27%8sb' then 'Forest Bank'		
	end

from OP.Schedule
inner join OP.Patient on Schedule.EncounterRecno = Patient.EncounterRecno

where 
Schedule.AppointmentDate between @StartDate and @EndDate
and (Patient.PostCode like 'SK9%4HR' or Patient.PostCode like 'm27%8sb')
and Schedule.AppointmentPCTCCGCode not in ('5NP','01C') --Central and Eastern Cheshire CCG/PCT

order by 
Schedule.AppointmentDate