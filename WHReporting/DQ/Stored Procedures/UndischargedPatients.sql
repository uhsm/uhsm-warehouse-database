﻿/*===============================================================================
Author:		J Pitchforth
Date:		21 May 2013
Function:	Returns spells with no discharge date and LOS > 120 days, so that 
			Data Quality can find out if the patient is still physically in 
			hospital or they have not been discharged correctly on Lorenzo.
Version:	1.0
=================================================================================
*/
--DQ.UndischargedPatients '1 Feb 2013','28 Feb 2013'


CREATE procedure [DQ].[UndischargedPatients]

	@StartDate date,
	@EndDate date

as

select 
	Spell.NHSNo,
	Spell.FaciltyID, 
	Patient.PatientForename,
	Patient.PatientSurname,
	Spell.AdmissionDate,
	Spell.DischargeDate,
	AdmissionType = 
			Case	when Spell.InpatientStayCode = 'D' then 'Day Case' 
					when Spell.AdmissionType in ('Emergency','Non-Elective','Maternity') then 'Non Elective'
					else 'Elective' end,
	Spell.AdmissionConsultantCode,
	Spell.[AdmissionSpecialtyCode(Function)],
	CAST(getdate() as DATE) as Today,
	Spell.LengthOfSpell

from WHREPORTING.APC.Spell
	--inner join WHReporting.APC.Episode on Spell.SourceSpellNo = Episode.SourceSpellNo
	inner join WHREPORTING.APC.Patient on Spell.EncounterRecno = Patient.EncounterRecno

where Spell.LengthOfSpell > 120
	and Spell.DischargeDate is null
	
order by AdmissionDate