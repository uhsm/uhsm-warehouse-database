﻿/*===============================================================================
Author:		J Pitchforth
Date:		22 May 2013
Function:	Identifies patients where the age is not consistent with the method of
			admission (baby born in hospital or baby born in another hospital where
			the age in years is >0)
Version:	1.0
=================================================================================
*/
--DQ.Wellbabies '01 Apr 2013','30 Apr 2013'

CREATE Procedure [DQ].[Wellbabies]

	@StartDate date,
	@EndDate date

as

select 
	Calendar.FinancialMonthKey as FinMonth,
	Patient.SexNHSCode as Gender,
	Spell.AdmissionMethodCode,
	Spell.[AdmissionSpecialtyCode(Function)] as TSpec,
	Case when Spell.AdmissionMethodCode in ('BHOSP','BOHOSP') then 'BABY' else null end as PatClass,
	Spell.SourceSpellNo, 
	Spell.AdmissionDate, 
	Spell.DischargeDate, 
	Spell.PracticeCode,
	Spell.ResponsibleCommissioner as Purchaser,
	Spell.AdmissiontWardCode,
	Patient.NHSNo,
	Patient.FacilityID, 
	Patient.DateOfBirth,
	WHREPORTING.dbo.GetAgeOnSpecifiedDate(Patient.DateOfBirth,Spell.AdmissionDate)as AgeOnAdmission
	
from APC.Spell
	inner join APC.Patient on Spell.EncounterRecno = Patient.EncounterRecno
	inner join WHREPORTING.LK.Calendar on Spell.DischargeDate = Calendar.TheDate
	
where 
	Spell.AdmissionDate between @StartDate and @EndDate
	and Spell.AdmissionMethodCode in ('BHOSP', 'BOHOSP')
	and WHREPORTING.dbo.GetAgeOnSpecifiedDate(Patient.DateOfBirth,Spell.AdmissionDate) >0
	
order by Spell.admissiondate