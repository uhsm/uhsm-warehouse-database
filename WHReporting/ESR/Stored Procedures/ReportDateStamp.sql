﻿Create Procedure [ESR].[ReportDateStamp] As

select 
 CONVERT(varchar(20), CONVERT(date, CONVERT(varchar(8), TextValue), 112),105)as datetime
 from ESR.dbo.Parameter where Parameter = 'ESRFILEDATE'