﻿CREATE Procedure [ESR].[ReportTrustMandatoryTrainingCompliance] 

@Directorate AS Varchar(max)
,@SubDirectorate AS Varchar(max)
,
@Specialty AS Varchar(max)
,@WardDepartment AS Varchar(max)

AS

SELECT
c.CompetenceID 
,cl.CompetenceName
,rl.Requirement
,SUM(CASE WHEN rl.Requirement = ('Meets requirement') THEN 1 ELSE 0 END) AS RequirementMet
,SUM(CASE WHEN rl.Requirement = ('Expires in next 3 months') THEN 1 ELSE 0 END) AS Expires
,SUM(CASE WHEN rl.Requirement = ('Does not meet requirement') THEN 1 ELSE 0 END) AS Outstanding
,REPLACE(ISNULL(SUBSTRING(s.Directorate, 5, LEN(s.Directorate) -4),'Unknown'),'L3','') AS Directorate
,REPLACE(ISNULL(s.Directorate, 'Unknown'), 'L3', '')  AS DirectorateCode
,REPLACE(ISNULL(SUBSTRING(s.SubDirectorate, 5, LEN(s.SubDirectorate) -4),'Unknown'),'L4','') AS SubDirectorate
,REPLACE(ISNULL(s.SubDirectorate, 'Unknown'), 'L4', '') AS SubDirectorateCode
,REPLACE(ISNULL(SUBSTRING(s.Specialty, 5, LEN(s.Specialty) -4),'Unknown'),'L5','') AS Specialty
,REPLACE(ISNULL(s.Specialty, 'Unknown'), 'L5', '') AS SpecialtyCode
,REPLACE(ISNULL(SUBSTRING(s.WardDepartment, 5, LEN(s.WardDepartment) -4),'Unknown'),'L6','') AS WardDepartment
,REPLACE(ISNULL(s.WardDepartment, 'Unknown'), 'L6', '') AS WardDepartmentCode
FROM ESR.ESR.TImportStaff s
LEFT JOIN ESR.ESR.Competence c ON (s.AssignmentNumber = c.Assignment)
LEFT JOIN ESR.ESR.CompetenceLookup cl ON (c.CompetenceID = cl.CompetenceID)
LEFT JOIN ESR.ESR.RequirementLookup rl ON (c.MeetsRequirement = rl.RequirementID)

WHERE (SUBSTRING(s.Directorate, 5, LEN(s.Directorate))) IN ('Charitable Funds L3','Clinical Support Directorate','Corporate Services',
'Education & Training L3','Research & Development L3', 'Scheduled Care Directorates',
'Unscheduled Care Directorates')

AND Cl.CompetenceID IS NOT NULL
AND s.AssignmentCategory <> 'Bank'
AND s.AssignmentStatus not in (
		'Maternity & Adoption',
		'Suspend No Pay',
		'Inactive Not Worked',
		'Career Break',
		'Out on External Secondment - Paid',
		'Suspend With Pay'
	) 
	
AND REPLACE(ISNULL(s.Directorate, 'Unknown'), 'L3', '') IN (SELECT ITEM FROM dbo.Split (@Directorate, ','))
AND REPLACE(ISNULL(s.SubDirectorate, 'Unknown'), 'L4', '') IN (SELECT ITEM FROM dbo.Split (@SubDirectorate, ','))
--AND REPLACE(ISNULL(s.Specialty, 'Unknown'), 'L5', '') IN (SELECT ITEM FROM dbo.Split (@Specialty, ','))
AND REPLACE(ISNULL(s.WardDepartment, 'Unknown'), 'L6', '') IN (SELECT ITEM FROM dbo.Split (@WardDepartment, ','))
	
GROUP BY 
c.CompetenceID 
,cl.CompetenceName
,rl.Requirement
,s.Directorate
,s.SubDirectorate
,s.Specialty
,s.WardDepartment

ORDER BY
cl.CompetenceName