﻿/*
--Author: N Scattergood	
--Date created: 06/03/2014
--Stored Procedure Built for ETM Performance Report on:
--ED Breakdown by Breach Reason
*/

CREATE Procedure [ETM].[ReportEDBreakdownBreachReason]
as


SELECT
    Period.ReportingPeriod 
     ,case
     when rtrim([BreachReason]) = 'Unavailability of Bed'
     then 'Unavailability of Bed'
    when rtrim([BreachReason]) = 'Awaiting Emergency Dept Dr'
	then 'Awaiting ED Doctor'
	when rtrim([BreachReason]) = 'Late Referral from EM Dept'
	then 'Late Referral From ED'
	when rtrim([BreachReason]) = 'Clinical need'
	then 'Clinical Need'
	when rtrim([BreachReason]) = 'Transfer problem'
	then 'Transfer Problem'
	when rtrim([BreachReason]) = 'Bouncing referral'
	then 'Bouncing Referral'
    else rtrim([BreachReason]) 
    end as BreachReason
      ,COUNT(*) as NumberOfBreaches
      
  FROM [WH].[AE].[CurrentEncounter]
  
  	INNER JOIN ---Sub Query for Reporting Period
			(
				 select 
		 Cal.TheDate
		 ,Cal.WeekNoKey
		 ,Cal.LastDayOfWeek as ReportingPeriod
		 FROM WHREPORTING.LK.Calendar Cal
		 
		 where
		Cal.LastDayOfWeek in 
		(select distinct
		SQ.LastDayOfWeek	
		  FROM WHREPORTING.LK.Calendar SQ
		  where SQ.TheDate between  
  			(
		dateadd(week,-13,cast(GETDATE()as DATE))---AMEND HERE IF YOU WANT MORE REPORTING WEEKS
			)
			and
		dateadd(week,-1,cast(GETDATE()as DATE))
			)
			Group by
			 Cal.TheDate
			 ,Cal.WeekNoKey
			 ,Cal.LastDayOfWeek
        ) Period
		 ON ArrivalDate = Period.TheDate
  
  WHERE 
AttendanceCategoryCode in ('1','2')	
AND 
EncounterDurationMinutes > 240

GROUP BY
 Period.ReportingPeriod ,
     case
     when rtrim([BreachReason]) = 'Unavailability of Bed'
     then 'Unavailability of Bed'
    when rtrim([BreachReason]) = 'Awaiting Emergency Dept Dr'
	then 'Awaiting ED Doctor'
	when rtrim([BreachReason]) = 'Late Referral from EM Dept'
	then 'Late Referral From ED'
	when rtrim([BreachReason]) = 'Clinical need'
	then 'Clinical Need'
	when rtrim([BreachReason]) = 'Transfer problem'
	then 'Transfer Problem'
	when rtrim([BreachReason]) = 'Bouncing referral'
	then 'Bouncing Referral'
    else rtrim([BreachReason]) 
    end 

ORDER BY 
 Period.ReportingPeriod ,
     case
     when rtrim([BreachReason]) = 'Unavailability of Bed'
     then 'Unavailability of Bed'
    when rtrim([BreachReason]) = 'Awaiting Emergency Dept Dr'
	then 'Awaiting ED Doctor'
	when rtrim([BreachReason]) = 'Late Referral from EM Dept'
	then 'Late Referral From ED'
	when rtrim([BreachReason]) = 'Clinical need'
	then 'Clinical Need'
	when rtrim([BreachReason]) = 'Transfer problem'
	then 'Transfer Problem'
	when rtrim([BreachReason]) = 'Bouncing referral'
	then 'Bouncing Referral'
    else rtrim([BreachReason]) 
    end