﻿/*
--Author: N Scattergood	
--Date created: 06/03/2014
--Stored Procedure Built for ETM Performance Report on:
--ED Breakdown for previous week By:
--		- Arrival Hour
--		- Arrival Day	
*/

CREATE Procedure [ETM].[ReportEDBreakdownByArrivalTime]
as
SELECT 
	Period.ReportingPeriod
	,Period.TheDate as ArrivalDate
,Period.DayOfWeek
,DATEPART(hour,ArrivalTime) as ArrivalHour
	,SUM(CASE WHEN AttendanceCategoryCode <> 'WA'	
					AND AttendanceCategoryCode in ('1','2')
					THEN 1 ELSE 0
     END) as [Attendances] 
     					,SUM(CASE WHEN AttendanceDisposalCode = 'AD'
					AND AttendanceCategoryCode <> 'WA'	
					AND AttendanceCategoryCode in ('1','2')
						THEN 1 ELSE 0
					END) AS [Admissions]
	,SUM(CASE WHEN EncounterDurationMinutes > 240
				AND AttendanceCategoryCode in ('1','2')
			THEN 1 ELSE 0
	END) as [Over4hours] 
    ,SUM(CASE WHEN EncounterDurationMinutes <= 240
				AND AttendanceCategoryCode in ('1','2')
			THEN 1 ELSE 0
	END) as [Under4hours]  
	--,AE2.Admissions --This uses a temporary table created in join below to ensure All Attends used

FROM 
	WH.AE.Encounter AE
	INNER JOIN ---Sub Query for last week
			(
				 select 
		 Cal.TheDate
		 ,Cal.DayOfWeek
		 ,Cal.WeekNoKey
		 ,Cal.LastDayOfWeek as ReportingPeriod
		 FROM WHREPORTING.LK.Calendar Cal
		 
		 where
		Cal.LastDayOfWeek in 
		(select distinct
		SQ.LastDayOfWeek	
		  FROM WHREPORTING.LK.Calendar SQ
		  where SQ.TheDate =  
  			(
		dateadd(week,-1,cast(GETDATE()as DATE))
			))
			Group by
			 Cal.TheDate
			 ,Cal.DayOfWeek
			 ,Cal.WeekNoKey
			 ,Cal.LastDayOfWeek
        ) Period
		 ON ArrivalDate = Period.TheDate
		  
	

WHERE 
	AttendanceCategoryCode in ('1','2') 


GROUP BY 
	Period.ReportingPeriod
	,Period.TheDate
,Period.DayOfWeek
,DATEPART(hour,ArrivalTime) 

	
ORDER BY 	
Period.TheDate
,DATEPART(hour,ArrivalTime)