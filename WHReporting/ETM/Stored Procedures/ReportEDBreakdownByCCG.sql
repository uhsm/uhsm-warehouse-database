﻿/*
--Author: N Scattergood	
--Date created: 06/03/2014
--Stored Procedure Built for ETM Performance Report on:
--ED Breakdown by CCG
*/

CREATE Procedure [ETM].[ReportEDBreakdownByCCG]
as

SELECT 
	Period.ReportingPeriod
	,Case
	when AE.CCGCode is null 
	then 'South Manchester CCG'
	when AE.CCGCode = '01N' 
	then 'South Manchester CCG'
	when AE.CCGCode = '02A' 
	then 'Trafford CCG'
	when AE.CCGCode = '01W' 
	then 'Stockport CCG'
	Else 'Other CCG'
	end
	as CCGGroup
	,AE.CCGCode
    ,SUM(CASE WHEN EncounterDurationMinutes <= 240
				AND AttendanceCategoryCode in ('1','2')
			THEN 1 ELSE 0
	END) as [Under4hours]  
	,SUM(CASE WHEN EncounterDurationMinutes > 240
				AND AttendanceCategoryCode in ('1','2')
			THEN 1 ELSE 0
	END) as [Over4hours] 
	,SUM(CASE WHEN AttendanceCategoryCode <> 'WA'	
					AND AttendanceCategoryCode in ('1','2')
					THEN 1 ELSE 0
     END) as [Attendances] 
     					,SUM(CASE WHEN AttendanceDisposalCode = 'AD'
					AND AttendanceCategoryCode <> 'WA'	
						THEN 1 ELSE 0
					END) AS [Admissions]
	--,AE2.Admissions --This uses a temporary table created in join below to ensure All Attends used

FROM 
	WH.AE.Encounter AE
	INNER JOIN ---Sub Query for last 13 weeks
			(
				 select 
		 Cal.TheDate
		 ,Cal.WeekNoKey
		 ,Cal.LastDayOfWeek as ReportingPeriod
		 FROM WHREPORTING.LK.Calendar Cal
		 
		 where
		Cal.LastDayOfWeek in 
		(select distinct
		SQ.LastDayOfWeek	
		  FROM WHREPORTING.LK.Calendar SQ
		  where SQ.TheDate between  
  			(
		dateadd(week,-15,cast(GETDATE()as DATE))---AMEND HERE IF YOU WANT MORE REPORTING WEEKS
			)
			and
		dateadd(week,-1,cast(GETDATE()as DATE))
			)
			Group by
			 Cal.TheDate
			 ,Cal.WeekNoKey
			 ,Cal.LastDayOfWeek
        ) Period
		 ON ArrivalDate = Period.TheDate
		  
	--LEFT JOIN 
	--		    (SELECT 
	--				WeekNoKey	
	--				,CCGCode				
	--				,SUM(CASE WHEN AttendanceDisposalCode = 'AD'
	--				AND AttendanceCategoryCode <> 'WA'	
	--					THEN 1 ELSE 0
	--				END) AS [Admissions]
	--				--,SUM(CASE WHEN (DATEDIFF(MINUTE, DecisionToAdmitTime, AttendanceConclusionTime)) > '240' 
	--				--	THEN 1 ELSE 0
	--				--END)AS [TrolleyWaits>4hrs]
	--				--,SUM(CASE WHEN (DATEDIFF(MINUTE, DecisionToAdmitTime, AttendanceConclusionTime)) > '720' 
	--				--	THEN 1 ELSE 0
	--				--END)AS [TrolleyWaits>12hrs]  
	--			FROM WH.AE.Encounter 
	--			INNER JOIN WHREPORTING.LK.Calendar c ON ArrivalDate = c.TheDate 
	--					and c.LastDayOfWeek in 
	--						(select distinct
	--						SQ.LastDayOfWeek	
	--						  FROM WHREPORTING.LK.Calendar SQ
	--						  where SQ.TheDate between  
 -- 								(
	--						dateadd(week,-12,cast(GETDATE()as DATE))---AMEND HERE IF YOU WANT MORE REPORTING WEEKS
	--							)
	--							and
	--						dateadd(week,-1,cast(GETDATE()as DATE))
	--							)
	--			WHERE 
	--			AttendanceDisposalCode = 'AD'
	--			AND 
	--			AttendanceCategoryCode <> 'WA'	
	--				--AND ArrivalDate between @StartDate and @EndDate	
	--				--AND ArrivalDate between '2012-04-01' and '2013-03-31'
	--			GROUP BY c.WeekNoKey
	--			,CCGCode
	--			) AS AE2 
	--			on Period.WeekNoKey = AE2.WeekNoKey
	--			and	AE2.CCGCode = AE.CCGCode

WHERE 
	AttendanceCategoryCode in ('1','2') 


GROUP BY 
	Period.ReportingPeriod
	,AE.CCGCode
	--,AE2.Admissions
	,Case
	when AE.CCGCode is null 
	then 'South Manchester CCG'
	when AE.CCGCode = '01N' 
	then 'South Manchester CCG'
	when AE.CCGCode = '02A' 
	then 'Trafford CCG'
	when AE.CCGCode = '01W' 
	then 'Stockport CCG'
	Else 'Other CCG'
	end

	
ORDER BY 	
	Period.ReportingPeriod
	,Case
	when AE.CCGCode is null 
	then 'South Manchester CCG'
	when AE.CCGCode = '01N' 
	then 'South Manchester CCG'
	when AE.CCGCode = '02A' 
	then 'Trafford CCG'
	when AE.CCGCode = '01W' 
	then 'Stockport CCG'
	Else 'Other CCG'
	end