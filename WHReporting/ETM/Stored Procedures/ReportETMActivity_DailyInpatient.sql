﻿/*
--Author: N Scattergood	
--Date created: 21/10/2015
--Stored Procedure Built for Daily Reporting on ETM Activity:
*/

CREATE Procedure [ETM].[ReportETMActivity_DailyInpatient]
as

Declare @StartDate	as Date = 
(--Sub Query for First day of last Week
 select 
 distinct
  min(Cal.TheDate) as FirstDayOfLastWeek
 --,max(Cal.TheDate) LastDayofLastWeek
 FROM WHREPORTING.LK.Calendar Cal
 where
Cal.FirstDayOfWeek = 
(select distinct
SQ.FirstDayOfWeek
  FROM WHREPORTING.LK.Calendar SQ
  where SQ.TheDate = 
 (
dateadd(WEEK,-1,cast(dateadd(day,-1,GETDATE())as DATE))
)
)
)--End of Sub Query

Declare @EndDate	as Date = 
(--Sub Query for Last day of last Week
 select 
-- distinct
--  --min(Cal.TheDate) as FirstDayOfLastWeek
-- max(Cal.TheDate) LastDayofLastWeek
-- FROM WHREPORTING.LK.Calendar Cal
-- where
--Cal.FirstDayOfWeek = 
--(select distinct
--SQ.FirstDayOfWeek
--  FROM WHREPORTING.LK.Calendar SQ
--  where SQ.TheDate = 
 (
cast(dateadd(day,-1,GETDATE())as DATE)
)
--)
)--End of Sub Query


select 
'Activity' as DataType,
Division
,Directorate
,DischargeDate
,Spec
,DischargeWeekNumber
--, 0			as	DC_Plan
--, 0			as  EL_Plan
--, 0			as	NEL_Plan
, SUM(DC)	as	Act_DC
, SUM(EL)	as  Act_EL
, SUM(NEL)	as	Act_NEL

from PbR2016.ETM.InPatientActivityDetail APC



where DischargeDate between @StartDate and @EndDate
and 
Spec not in ('Anti-Coagulation','Palliative Medicine','Physiotherapy')-- Uncomment if Needed

Group by 
Division
,Directorate
,Spec
,DischargeDate
,DischargeWeekNumber

Order by 
Division
,Directorate
,Spec
,DischargeDate
,DischargeWeekNumber