﻿/*
--Author: N Scattergood	
--Date created: 21/10/2015
--Stored Procedure Built for Daily Reporting on ETM Activity:
*/

CREATE Procedure [ETM].[ReportETMActivity_DailyOutpatient]
as

Declare @StartDate	as Date = 
(--Sub Query for First day of last Week
 select 
 distinct
  min(Cal.TheDate) as FirstDayOfLastWeek
 --,max(Cal.TheDate) LastDayofLastWeek
 FROM WHREPORTING.LK.Calendar Cal
 where
Cal.FirstDayOfWeek = 
(select distinct
SQ.FirstDayOfWeek
  FROM WHREPORTING.LK.Calendar SQ
  where SQ.TheDate = 
 (
dateadd(WEEK,-1,cast(dateadd(day,-1,GETDATE())as DATE))
)
)
)--End of Sub Query

Declare @EndDate	as Date = 
(--Sub Query for Last day of last Week
 select 
-- distinct
--  --min(Cal.TheDate) as FirstDayOfLastWeek
-- max(Cal.TheDate) LastDayofLastWeek
-- FROM WHREPORTING.LK.Calendar Cal
-- where
--Cal.FirstDayOfWeek = 
--(select distinct
--SQ.FirstDayOfWeek
--  FROM WHREPORTING.LK.Calendar SQ
--  where SQ.TheDate = 
 (
cast(dateadd(day,-1,GETDATE())as DATE)
)
--)
)--End of Sub Query


select 
'Activity' as DataType,
Division
,Directorate
,AppointmentDate
,Spec
,WeekNumber,
SUM(New) + SUM(NewProc) as Act_New, 
SUM(FollowUp)+ SUM(FollowUpProc) as Act_FollowUP 
--, SUM(FollowUp) as FollowUP, SUM(FollowUpProc) as  FollowUpProc, SUM(New) as New, SUM(NewProc) as NewProc

from PbR2016.ETM.OutPatientActivityDetail OP

where AppointmentDate between @StartDate and @EndDate
and
case
When OP.Spec is null
then 1
When OP.Spec = 'DQ'
then 1
When OP.Spec = 'Nursing Episode' 
then 1
else 0 end 
=0

Group by
Division
,Directorate
,AppointmentDate
,Spec
,WeekNumber

Order By
Division
,Directorate
,AppointmentDate
,Spec
,WeekNumber

--UNION ALL

--Select
--'Activity'						as DataType
--,'Clinical Support Division'	as Division
--,'Audiology'					as Directorate
--,ApptDate						as AppointmentDate
--,'Audiology'					as Spec
--,WeekNumber,						
--SUM(New) as New, 
--SUM(FollowUp) as FollowUP 
--FROM
--PbR2015.[ETM].[AudiologyOP_PractiseNav]

--Where
--[ApptDate] between @StartDate and @EndDate

--Group by 
--WeekNumber
--,ApptDate