﻿/*
--Author: N Scattergood	
--Date created: 28/10/2015
--Stored Procedure Built for Daily Reporting on ETM Activity:
--This focuses on Appointments in the past where the Attendance Status has not been specified
--Based on 
--		logic from the ETM (OP View in PbR Database)
--		logic from Stored Procedure [DQ].[ReportUnrecordedAttendanceAndOutcome]
*/

CREATE Procedure [ETM].[ReportETMActivity_DailyOutpatient_MissingAttend]
as



Declare @StartDate	as Date = 
(--Sub Query for First day of last Week
 select 
 distinct
  min(Cal.TheDate) as FirstDayOfLastWeek
 --,max(Cal.TheDate) LastDayofLastWeek
 FROM WHREPORTING.LK.Calendar Cal
 where
Cal.FirstDayOfWeek = 
(select distinct
SQ.FirstDayOfWeek
  FROM WHREPORTING.LK.Calendar SQ
  where SQ.TheDate = 
 (
dateadd(WEEK,-1,cast(dateadd(day,-1,GETDATE())as DATE))
)
)
)--End of Sub Query

Declare @EndDate	as Date = 
(--Sub Query for Last day of last Week
 select 
(
cast(dateadd(day,-1,GETDATE())as DATE)
)

)--End of Sub Query


SELECT 
Cal.LastDayOfWeek as WeekEnding,
a.AppointmentDate,
ETMSpecLookup.Division,
ETMSpecLookup.Directorate,
case 
when a.[SpecialtyCode] = '103A'
then 'Breast Surgery - Thyroid'
when a.[SpecialtyCode] = '301B'
then 'Gastroenterology'
when a.[SpecialtyCode] = '370C'
then 'Breast Surgery - Family History'
when a.[SpecialtyCode] = '329'
then 'Complex Care TIA'
--when Schedule.[SpecialtyCode] = '9044'
--then 'MSK Physiotherapy'
--when Schedule.[SpecialtyCode] = '9046'
--then 'Orthopaedic Assessment Service'
when a.[SpecialtyCode] = '656'--Clinical Psychology (amended 28/05/14 at PN's request
then 'Pain Management' 
else
Coalesce(LK_OP_Clinics.SpecDesc,ETMSpecLookup.SpecDescription2)
end
as Spec,
a.[SpecialtyCode(Function)],
a.[SpecialtyCode]
,a.Specialty,
       a.ClinicCode,
       CASE
        -- WHEN a.AttendStatus IS NULL THEN 1 -- 30/05/2014 TJD Removed
          WHEN a.AttendStatus = 'Not Specified' THEN 1 -- 30/05/2014 TJD Added
         ELSE 0
       END MissingAttended,
       CASE
         WHEN a.Outcome IS NULL THEN 1  
         ELSE 0
       END MissingOutcome
       --CASE
       --  --WHEN a.AttendStatus IS NULL -- 30/05/2014 TJD Removed
       --  WHEN a.AttendStatus = 'Not Specified'  -- 30/05/2014 TJD Added
       --       OR a.Outcome IS NULL THEN 1 
       --  ELSE 0
       --END MissingTotal
,a.ProfessionalCarerCode
,a.ProfessionalCarerName
,a.ProfessionalCarerType
,AppointmentType 
= case when a.AppointmentTypeNHSCode = '1'
then 'First'
else 'Follow Up'
End
,IndicativePOD =

						case
			when exists
				(
				select
					1
				from
					PbR2016.Tariff.Outpatient
				where
					Outpatient.TreatmentFunctionCode = a.[SpecialtyCode(Function)]
				)
			then ''
			else 'NP'
			end +	case			
					when a.ProfessionalCarerCode like '[CD][0-9][0-9][0-9][0-9][0-9]%'
					then

						case
						when a.AppointmentTypeNHSCode = '1' --First seen
						then 'OPFASPCL'
						else 'OPFUPSPCL'
						end

					--when PbR.MedicalStaffTypeCode = '04' --Member of Care Professional team
					--then
					else

						case
						when a.AppointmentTypeNHSCode = '1'  --First seen
						then 'OPFASPNCL'
						else 'OPFUPSPNCL'
						end
						end  
						
,[Tariff] as EstimatedIncome    
       
       
       
       
FROM   WHREPORTING.OP.Schedule a
       LEFT JOIN WHREPORTING.OP.Patient p
              ON a.EncounterRecno = p.EncounterRecno
       --LEFT JOIN (SELECT DISTINCT s.AdmissiontWardCode WardCode
       --           FROM   WHREPORTING.APC.Spell s
       --           WHERE  s.AdmissionDate BETWEEN @StartDate AND @EndDate) wards
       --       ON a.ClinicCode = wards.WardCode -- identify ward attenders

		left join PbR2016.ETM.LK_OP_Clinics 
		on a.ClinicCode = LK_OP_Clinics.ClinicName 

		left join PbR2016.ETM.ETMSpecLookup  ETMSpecLookup 
		on Coalesce(LK_OP_Clinics.TreatmentFunctionCode,a.[SpecialtyCode]) = ETMSpecLookup.SpecCode 
		
		left join WHREPORTING.LK.Calendar Cal
		on Cal.TheDate = a.AppointmentDate
		
		left join [PbR2016].[Tariff].[SLAMExport_TariffOPFAFUP] Tar
		on (
		(
		Tar.[Spec_Code] = left(a.[SpecialtyCode],3)
		and 
		left(ETMSpecLookup.Directorate,4)= 'Card'
		)
		OR
		(
		Tar.[Spec_Code] = a.[SpecialtyCode(Function)]
		and 
		left(ETMSpecLookup.Directorate,4)<> 'Card'
		)
		)
		and case
			when exists
				(
				select
					1
				from
					PbR2016.Tariff.Outpatient
				where
					Outpatient.TreatmentFunctionCode = a.[SpecialtyCode(Function)]
				)
			then ''
			else 'NP'
			end +	case			
					when a.ProfessionalCarerCode like '[CD][0-9][0-9][0-9][0-9][0-9]%'
					then

						case
						when a.AppointmentTypeNHSCode = '1' --First seen
						then 'OPFASPCL'
						else 'OPFUPSPCL'
						end

					--when PbR.MedicalStaffTypeCode = '04' --Member of Care Professional team
					--then
					else

						case
						when a.AppointmentTypeNHSCode = '1'  --First seen
						then 'OPFASPNCL'
						else 'OPFUPSPNCL'
						end
						end = Tar.[Pod_Code]

       --===============================================================
WHERE  a.AppointmentDate BETWEEN @StartDate AND @EndDate
       -- only included appointments with no outcome or attendance status
       AND a.AttendStatus = 'Not Specified'
       --OR a.Outcome IS NULL )           
       AND a.IsWardAttender = 0 -- only op appointments exclude ward attenders
       AND a.CancelledDateTime IS NULL -- exclude any cancelled appointments
       AND ( a.AppointmentDate <= p.DateOfDeath
              OR p.DateOfDeath IS NULL )-- exclude any appointments after patient death 
       --AND wards.WardCode IS NULL -- exclude ward attenders
       -- exclude the following clinc codes
       AND a.ClinicCode NOT IN ( 'CABPAIN', 'CABDERM', 'CABUROL', 'CABCARD',
                                 'CABCAS', 'CABPAED', 'PACTEL', 'JOCJP2',
                                 'JOCMON', 'JEFJOC', 'JEFJP2', 'JFGPP2',
                                 'JFGP2N', 'JFGPSI', 'JEFGP2', 'JOB4AM',
                                 'JOB4P2', 'CABGAST', 'CABGAS', 'PASACAB',
                                 'CABBREA', '4ABJOB', 'BJOB4A', 'JEFJO1',
                                 'JOB4AM', 'JOB4P2', 'DEXA-NG' )
       AND a.ClinicCode NOT LIKE '%Test%'
       AND
       not
	exists
			(
			select
				1
			from
				PbR2016.ETM.TELE_OP_Clinics
			where
				a.ClinicCode = TELE_OP_Clinics.ClinicName
			)
			and [SpecialtyCode(Function)] <> '840'
			
			
			Order By
			[SpecialtyCode(Function)]