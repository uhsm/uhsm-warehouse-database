﻿/*
--Author: N Scattergood	
--Date created: 18/03/2014
--Stored Procedure Built for ETM Performance Report on:
--Emergency Admissions and Discharges to Measure Net Change
*/

CREATE Procedure [ETM].[ReportEmergencyAdmissionDischargesNetChange]
as


Select 

Indicator = 'AdmissionsByDay',
TheDate,
DayOfWeek,
LastDayOfWeek,
S.AdmissiontWardCode,
S.AdmissionWard,
S.[AdmissionSpecialtyCode(Function)],
S.[AdmissionSpecialty(Function)],
COUNT(*) as Value

FROM

WHREPORTING.APC.Spell S

 inner join WHREPORTING.LK.Calendar Cal
 On S.AdmissionDate = Cal.TheDate
 and Cal.LastDayOfWeek in 
		(select distinct
		SQ.LastDayOfWeek	
		  FROM WHREPORTING.LK.Calendar SQ
		  where SQ.TheDate between  
  			(
		dateadd(week,-5,cast(GETDATE()as DATE))---AMEND HERE IF YOU WANT MORE REPORTING WEEKS
			)
			and
		dateadd(week,-1,cast(GETDATE()as DATE))
			)
	
  WHERE
AdmissionMethodCode in ('21','22','23','24','28') 		
And 
[ADmissionSpecialtyCode(Main)] <> '420'
And 
[AdmissionSpecialtyCode(Function)] not in ('501','560')
And
S.AdmissiontWardCode not in 
(
'ALE','ALEXHDU','ALEXICU',	--ALEX
'REGENCY',					--Regency Hospital Macclesfield MAXFAX
'BS',						--BAGULEY SUITE OUTPATIENT DEPARTMENT
'BWH',						--Bridgewater Hospital
'ATR','AUR',				--TCW Treatment Centre WCH
--'ADMLOU','DL'				
'UAU',						--WCH Ward
'ASPIRE',					--No Beds - Virtual Ward
'TCW',						--Withington Community Hospital TREATMENT CENTRE
'TRAFF',					--WARD AT TRAFFORD
'SCB',						--SCBU
'HOM'						--HOME BIRTH
)
  
  
  GROUP BY
  TheDate,
DayOfWeek,
LastDayOfWeek,
S.AdmissiontWardCode ,
S.AdmissionWard,    
S.[AdmissionSpecialtyCode(Function)],
S.[AdmissionSpecialty(Function)]



Union All

Select 

Indicator = 'DischargesByDay',
TheDate,
DayOfWeek,
LastDayOfWeek,
S.AdmissiontWardCode,
S.AdmissionWard,
S.[AdmissionSpecialtyCode(Function)],
S.[AdmissionSpecialty(Function)],
-COUNT(*) as Value

FROM

WHREPORTING.APC.Spell S

 inner join WHREPORTING.LK.Calendar Cal
 On S.DischargeDate = Cal.TheDate
 and Cal.LastDayOfWeek in 
		(select distinct
		SQ.LastDayOfWeek	
		  FROM WHREPORTING.LK.Calendar SQ
		  where SQ.TheDate between  
  			(
		dateadd(week,-5,cast(GETDATE()as DATE))---AMEND HERE IF YOU WANT MORE REPORTING WEEKS
			)
			and
		dateadd(week,-1,cast(GETDATE()as DATE))
			)
	
  WHERE
AdmissionMethodCode in ('21','22','23','24','28') 		
And 
[ADmissionSpecialtyCode(Main)] <> '420'
And 
[AdmissionSpecialtyCode(Function)] not in ('501','560')
And
S.AdmissiontWardCode not in 
(
'ALE','ALEXHDU','ALEXICU',	--ALEX
'REGENCY',					--Regency Hospital Macclesfield MAXFAX
'BS',						--BAGULEY SUITE OUTPATIENT DEPARTMENT
'BWH',						--Bridgewater Hospital
'ATR','AUR',				--TCW Treatment Centre WCH
--'ADMLOU','DL'				
'UAU',						--WCH Ward
'ASPIRE',					--No Beds - Virtual Ward
'TCW',						--Withington Community Hospital TREATMENT CENTRE
'TRAFF',					--WARD AT TRAFFORD
'SCB',						--SCBU
'HOM'						--HOME BIRTH
)
  
  
  GROUP BY
  TheDate,
DayOfWeek,
LastDayOfWeek,
S.AdmissiontWardCode ,
S.AdmissionWard,    
S.[AdmissionSpecialtyCode(Function)],
S.[AdmissionSpecialty(Function)]