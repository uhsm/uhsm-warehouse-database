﻿/*
--Author: N Scattergood	
--Date created: 20/03/2014
--Stored Procedure Built for ETM Performance Report on:
--Medical Outliers (i.e. Unscheduled Care Patients in Sceduled Care Beds at Midnight)
*/

CREATE Procedure [ETM].[ReportHospitalMedicalOutliers]
as

SELECT 
Period.ReportingPeriod,
Period.ReportingDate
     --,E.[SourceSpellNo]
      --,[StartTime]
      --,[EndTime]
      ,WS.WardCode
      ,SQ.WardDesc
      ,LK.Division as WardDivision
      --,SQ.WardLocation
      ,S.AdmissionType
      --,WS.WardCode
      ,S.PatientClass
      ,E.Division
      ,E.Directorate
      ,E.[SpecialtyCode(Main)]
      ,E.[Specialty(Main)]
      ,E.[SpecialtyCode(Function)]
      ,E.[Specialty(Function)]
   
      , COUNT(1) as Outliers

  FROM [WHREPORTING].[APC].[Episode] E
  
        inner join ---Limits to last 5 weeks By Specialty At Midnight
				(--Start of Sub Query--
				 select 
		 Cal.TheDate		as ReportingDate
		 ,Cal.LastDayOfWeek as ReportingPeriod
		 FROM WHREPORTING.LK.Calendar Cal
		 
		 where
			Cal.LastDayOfWeek in 
		(select distinct
		SQ.LastDayOfWeek
		  FROM WHREPORTING.LK.Calendar SQ
		  where SQ.TheDate between  
  			(
		dateadd(week,-5,cast(GETDATE()as DATE))
			)
			and
				(
		dateadd(week,-1,cast(GETDATE()as DATE))
			)
			)
			Group by
			Cal.TheDate,
			  Cal.LastDayOfWeek --End of Sub Query--
        ) Period
        
        on E.EpisodeStartDateTime <= Period.ReportingDate	
        and coalesce(E.EpisodeEndDateTime,E.DischargeDateTime,
						(
						select distinct
						SQ.LastDayOfWeek
						  FROM WHREPORTING.LK.Calendar SQ
						  where SQ.TheDate =  
							dateadd(week,-1,cast(GETDATE()as DATE))
						)
        ) >= Period.ReportingDate	
        
        	
			left join [WHREPORTING].[APC].[WardStay] WS
			on E.[SourceSpellNo] = WS.[SourceSpellNo]
		and WS.StartTime <= Period.ReportingDate
        and coalesce(WS.EndTime,
						(
						select distinct
						SQ.LastDayOfWeek
						  FROM WHREPORTING.LK.Calendar SQ
						  where SQ.TheDate =  
							dateadd(week,-1,cast(GETDATE()as DATE))
						)
        ) >= Period.ReportingDate		
			
				left join	
				  (	
				SELECT 
				Distinct
				[Location] as WardLocation
				,[ServicePointLocalCode] as WardCode
					  ,[ServicePoint] as WardDesc
					  --,[SpecialtyCode]
					  ----,[SiteCode]
					  ----,[ServicePointTypeCode]
					  --,[ServicePointType]
					  --,[ArchiveFlag]
				  FROM [WH].[PAS].[ServicePoint]
				  where 
				  ServicePointType = 'WARD'
				  and 
				  [ArchiveFlag] = 'N'
				  )	 SQ
				  on WS.WardCode = SQ.WardCode
			
			left join [WHREPORTING].[APC].[Spell] S
			on S.SourceSpellNo = E.SourceSpellNo
			
			inner join WHReporting.LK.WardtoDivision LK
			on WS.WardCode = LK.Ward
			AND LK.Division = 'Scheduled Care'		--Limits to Scheduled Care Wards
			and Ward not in ('PIU','PIUF2')			--Excluding PIU
			
where 
S.InpatientStayCode <> 'B'
and
(
(E.Division = 'Unscheduled Care'   
AND SpecialtyCode <> '340' )
OR
(E.Division = 'Unscheduled Care'  
AND SpecialtyCode = '340' AND WS.WardCode IN ('F1N', 'F5', 'F6','F2', 'F1'))
)

group by
Period.ReportingPeriod,
Period.ReportingDate
     --,E.[SourceSpellNo]
      --,[StartTime]
      --,[EndTime]
      ,WS.WardCode
      ,SQ.WardDesc
        ,LK.Division 
      --,SQ.WardLocation
      ,S.AdmissionType
      --,WS.WardCode
      ,S.PatientClass
      ,E.Division
      ,E.Directorate
      ,E.[SpecialtyCode(Main)]
      ,E.[Specialty(Main)]
      ,E.[SpecialtyCode(Function)]
      ,E.[Specialty(Function)]
      
      order by
		WS.WardCode
      ,E.Division
      ,E.Directorate
      ,E.[SpecialtyCode(Function)]