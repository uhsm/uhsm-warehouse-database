﻿/*
--Author: N Scattergood	
--Date created: 06/03/2014
--Stored Procedure Built for ETM Performance Report on:
--Occupancy in Hospital at 9am
*/

CREATE Procedure [ETM].[ReportHospitalOccupancy]
as



Declare @Hour		as int  = 8

Declare @StartDate	as Date = 
(--Sub Query for First day of last Week
 select 
 distinct
  min(Cal.TheDate) as FirstDayOfLastWeek
 --,max(Cal.TheDate) LastDayofLastWeek
 FROM WHREPORTING.LK.Calendar Cal
 where
Cal.FirstDayOfWeek = 
(select distinct
SQ.FirstDayOfWeek
  FROM WHREPORTING.LK.Calendar SQ
  where SQ.TheDate = 
 (
dateadd(WEEK,-5,cast(GETDATE()as DATE))
)
)
)--End of Sub Query

Declare @EndDate	as Date = 
(--Sub Query for Last day of last Week
 select 
 distinct
  --min(Cal.TheDate) as FirstDayOfLastWeek
 max(Cal.TheDate) LastDayofLastWeek
 FROM WHREPORTING.LK.Calendar Cal
 where
Cal.FirstDayOfWeek = 
(select distinct
SQ.FirstDayOfWeek
  FROM WHREPORTING.LK.Calendar SQ
  where SQ.TheDate = 
 (
dateadd(day,-7,cast(GETDATE()as DATE))
)
)
)--End of Sub Query



SELECT 
DT.[LastDayOfWeek] as ReportingPeriod
      ,DT.[HOURINT]
      ,DT.TheDate
      ,DT.DateAndHour	
      ,DT.TheMonth
      ,DT.[DayOfWeek]
     --,E.[SourceSpellNo]
      --,[StartTime]
      --,[EndTime]
      ,WS.WardCode
      ,SQ.WardDesc
      --,SQ.WardLocation
      ,S.AdmissionType
      --,WS.WardCode
      ,E.Division
      ,E.Directorate
      ,E.[SpecialtyCode(Main)]
      ,E.[Specialty(Main)]
      ,E.[SpecialtyCode(Function)]
      ,E.[Specialty(Function)]
     
      , COUNT(1) as TotalOccupancy

  FROM [WHREPORTING].[APC].[Episode] E
  
        inner join [WHREPORTING].[LK].[vwDateTime] DT
        on E.EpisodeStartDateTime <= DT.DateAndHour	
        and coalesce(E.EpisodeEndDateTime,E.DischargeDateTime,(WHREPORTING.dbo.GetEndOfDay(@EndDate))) > DT.DateAndHour	
        
        	
			left join [WHREPORTING].[APC].[WardStay] WS
			on E.[SourceSpellNo] = WS.[SourceSpellNo]
		and WS.StartTime <= DT.DateAndHour	
        and coalesce(WS.EndTime,(WHREPORTING.dbo.GetEndOfDay(@EndDate)))
			> DT.DateAndHour		
			
				left join	
				  (	
				SELECT 
				Distinct
				[Location] as WardLocation
				,[ServicePointLocalCode] as WardCode
					  ,[ServicePoint] as WardDesc
					  --,[SpecialtyCode]
					  ----,[SiteCode]
					  ----,[ServicePointTypeCode]
					  --,[ServicePointType]
					  --,[ArchiveFlag]
				  FROM [WH].[PAS].[ServicePoint]
				  where 
				  ServicePointType = 'WARD'
				  and 
				  [ArchiveFlag] = 'N'
				  )	 SQ
				  on WS.WardCode = SQ.WardCode
			
			left join [WHREPORTING].[APC].[Spell] S
			on S.SourceSpellNo = E.SourceSpellNo
where 
DT.TheDate between @StartDate and @EndDate
and
DT.[HOURINT] = @Hour
and
S.InpatientStayCode <> 'B'
and
WS.WardCode not in 
(
'ALE','ALEXHDU','ALEXICU',	--ALEX
'REGENCY',					--Regency Hospital Macclesfield MAXFAX
'BS',						--BAGULEY SUITE OUTPATIENT DEPARTMENT
'BWH',						--Bridgewater Hospital
'ATR','AUR',				--TCW Treatment Centre WCH
--'ADMLOU','DL'				
'UAU',						--WCH Ward
'ASPIRE',					--No Beds - Virtual Ward
'TCW',						--Withington Community Hospital TREATMENT CENTRE
'AUS',						--WCH UROL SURG UNIT (Withington Community Hospital) added 21st March
'TRAFF',					--WARD AT TRAFFORD
'SCB',						--SCBU
'HOM',						--HOME BIRTH
'SPIREW'					--Spire Manchester Hospital
,'HCAWILM'					-- HCA Wilmslow (Private Hospital)

)
and
[SpecialtyCode(Main)] <> '420'
And 
[SpecialtyCode(Function)] not in ('501','560')
And
Division in ('Scheduled Care','Unscheduled Care')


group by
      DT.[LastDayOfWeek] 
      ,DT.[HOURINT]
      ,DT.TheDate
      ,DT.DateAndHour	
      ,DT.TheMonth
      ,DT.[DayOfWeek]
      ,WS.WardCode
      ,SQ.WardDesc
      ,S.AdmissionType
      ,E.Division
      ,E.Directorate
      ,E.[SpecialtyCode(Main)]
      ,E.[Specialty(Main)]
      ,E.[SpecialtyCode(Function)]
      ,E.[Specialty(Function)]
      
      order by
WS.WardCode