﻿/*
--Author: N Scattergood	
--Date created: 06/03/2014
--Stored Procedure Built for ETM Performance Report on:
--Hospital LoS as at Midnight Sunday/Monday
*/

CREATE Procedure [ETM].[ReportHospitalOccupancyMidnightLoS]
as

SELECT 
Period.ReportingPeriod
     --,E.[SourceSpellNo]
      --,[StartTime]
      --,[EndTime]
      ,WS.WardCode
      ,SQ.WardDesc
      --,SQ.WardLocation
      ,S.AdmissionType
      --,WS.WardCode
      ,S.InpatientStayCode
      ,S.PatientClass
      ,E.Division
      ,E.Directorate
      ,E.[SpecialtyCode(Main)]
      ,E.[Specialty(Main)]
      ,E.[SpecialtyCode(Function)]
      ,E.[Specialty(Function)]
      ,DATEDIFF(day,S.AdmissionDate,Period.ReportingPeriod) as LOS
      ,case
      --when
      --DATEDIFF(day,S.AdmissionDate,Period.ReportingPeriod) >= 28
      --then 'LoS 28+'
      when
      DATEDIFF(day,S.AdmissionDate,Period.ReportingPeriod) >= 14 
      then 'LoS 14+'
      else 'LoS 0-13'      
      End as LoSCategory
     
      , COUNT(1) as TotalOccupancy

  FROM [WHREPORTING].[APC].[Episode] E
  
        inner join
				(
				 select 
		 Cal.TheDate as ReportingPeriod
		 FROM WHREPORTING.LK.Calendar Cal
		 
		 where
		 DayOfWeekKey = 1
		 and
		Cal.FirstDayOfWeek in 
		(select distinct
		SQ.FirstDayOfWeek
		  FROM WHREPORTING.LK.Calendar SQ
		  where SQ.TheDate between  
  			(
		dateadd(week,-12,cast(GETDATE()as DATE))
			)
			and
			--dateadd(week,-1,cast(GETDATE()as DATE))
			cast(GETDATE()as DATE)
			
			)
			Group by
			 Cal.TheDate
        ) Period
        
        on E.EpisodeStartDateTime <= Period.ReportingPeriod	
        and coalesce(E.EpisodeEndDateTime,E.DischargeDateTime,
						(
						 select 
						 Cal.TheDate
						 FROM WHREPORTING.LK.Calendar Cal
						 
						 where
						 DayOfWeekKey = 1
						 and
						Cal.FirstDayOfWeek in 
						(select distinct
						SQ.FirstDayOfWeek
						  FROM WHREPORTING.LK.Calendar SQ
						  where SQ.TheDate =  
								cast(GETDATE()as DATE)
							)
							Group by
							 Cal.TheDate    
							)
        ) >= Period.ReportingPeriod	
        
        	
			left join [WHREPORTING].[APC].[WardStay] WS
			on E.[SourceSpellNo] = WS.[SourceSpellNo]
		and WS.StartTime <= Period.ReportingPeriod
        and coalesce(WS.EndTime,
(
						 select 
						 dateadd(Minute,1,Cal.TheDate)
						 FROM WHREPORTING.LK.Calendar Cal
						 
						 where
						 DayOfWeekKey = 1
						 and
						Cal.FirstDayOfWeek in 
						(select distinct
						SQ.FirstDayOfWeek
						  FROM WHREPORTING.LK.Calendar SQ
						  where SQ.TheDate =  
							cast(GETDATE()as DATE)
							)
							Group by
							 Cal.TheDate    
							)
        ) > Period.ReportingPeriod		
			
				left join	
				  (	
				SELECT 
				Distinct
				[Location] as WardLocation
				,[ServicePointLocalCode] as WardCode
					  ,[ServicePoint] as WardDesc
					  --,[SpecialtyCode]
					  ----,[SiteCode]
					  ----,[ServicePointTypeCode]
					  --,[ServicePointType]
					  --,[ArchiveFlag]
				  FROM [WH].[PAS].[ServicePoint]
				  where 
				  ServicePointType = 'WARD'
				  and 
				  [ArchiveFlag] = 'N'
				  )	 SQ
				  on WS.WardCode = SQ.WardCode
			
			left join [WHREPORTING].[APC].[Spell] S
			on S.SourceSpellNo = E.SourceSpellNo
where 
S.InpatientStayCode not in ('B','D')
and
WS.WardCode not in 
(
'ALE','ALEXHDU','ALEXICU',	--ALEX
'REGENCY',					--Regency Hospital Macclesfield MAXFAX
'BS',						--BAGULEY SUITE OUTPATIENT DEPARTMENT
'BWH',						--Bridgewater Hospital
'ATR','AUR',				--TCW Treatment Centre WCH
--'ADMLOU','DL'				
'UAU',						--WCH Ward
'ASPIRE',					--No Beds - Virtual Ward
'TCW',						--Withington Community Hospital TREATMENT CENTRE
'TRAFF',					--WARD AT TRAFFORD
'SPIREW'					--Spire Manchester Hospital

)
and
[SpecialtyCode(Main)] <> '420'
And 
[SpecialtyCode(Function)] not in ('501','560')

group by
Period.ReportingPeriod
     --,E.[SourceSpellNo]
      --,[StartTime]
      --,[EndTime]
      ,WS.WardCode
      ,SQ.WardDesc
      --,SQ.WardLocation
      ,S.AdmissionType
      --,WS.WardCode
        ,S.InpatientStayCode
      ,S.PatientClass
      ,E.Division
      ,E.Directorate
      ,E.[SpecialtyCode(Main)]
      ,E.[Specialty(Main)]
      ,E.[SpecialtyCode(Function)]
      ,E.[Specialty(Function)]
            ,DATEDIFF(day,S.AdmissionDate,Period.ReportingPeriod) 
      ,case
      --when
      --DATEDIFF(day,S.AdmissionDate,Period.ReportingPeriod) >= 28
      --then 'LoS 28+'
      when
      DATEDIFF(day,S.AdmissionDate,Period.ReportingPeriod) >= 14 
      then 'LoS 14+'
      else 'LoS 0-13'      
      End 
      
      order by
      PatientClass,
DATEDIFF(day,S.AdmissionDate,Period.ReportingPeriod)
,Period.ReportingPeriod