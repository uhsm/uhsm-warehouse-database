﻿/*
--Author: N Scattergood	
--Date created: 21/03/2014
--Stored Procedure Built for ETM Performance Report on:
--Percentage Occupancy based on Smart Board Snapshots
--Scheduled and Unscheduled Care Beds not including Paeds/Obs Or Critical Care
*/

CREATE Procedure [ETM].[ReportHospitalSmartBoardOccupancy]
as
select 
cast(SB.CensusDate as DATE) as CensusDate,
convert(varchar,SB.CensusDate,3) as CensusDateFormat,
Datepart(Hour,SB.CensusDate) as CensusHour,
SB.WardCode
,SB.WardName
,WD.Division
,CASE
WHEN SB.SBOpenBeds is NULL
THEN SB.OccupiedOnLPI
ELSE SB.SBOpenBeds
END
AS BedsAvailable
,CASE 
WHEN SB.OccupiedOnLPI > SB.SBOpenBeds
THEN SB.SBOpenBeds
ELSE SB.OccupiedOnLPI 
END
AS BedsOccupied



from
[UHSMApplications].[SB].[BedStatus] SB

 inner join WHREPORTING.LK.Calendar Cal
 On cast(SB.CensusDate as DATE) = Cal.TheDate
 and Cal.LastDayOfWeek in 
		(select distinct
		SQ.LastDayOfWeek	
		  FROM WHREPORTING.LK.Calendar SQ
		  where SQ.TheDate between  
  			(
		dateadd(week,-5,cast(GETDATE()as DATE))---AMEND HERE IF YOU WANT MORE REPORTING WEEKS
			)
			and
		dateadd(week,-1,cast(GETDATE()as DATE))
			)
			
			Left join WHReporting.LK.WardtoDivision WD 
			-- IF A WARD APPEARS WITHOUT A DIVISION YOU WILL HAVE TO ADD IT TO THIS LOOKUP TABLE 
			-- OR TO THE EXCLUSIONS BELOW
			on WD.Ward = SB.WardCode
			

WHERE
WardCode not in 
(
'ADMLOU'  --ADMISSIONS LOUNGE - No Beds
,'ASPIRE'  --ASPIRE - Virtual Ward
,'ATR'  --WCH UROLOGY TRUSS UNIT - Withington
,'AUR'  --WCH UROL VIDEO D/C - Withington
,'AUS'  --WCH UROL SURG UNIT - Withington
,'BC'  --BIRTH CENTRE - OBS
,'BIC'  --BURNS INTENSIVE CARE - Critical Care
,'BRO'  --BRONCHOSCOPY UNIT - no beds, only trollies or chairs
,'BS'  --BAGULEY SUITE - Day Case Unit
,'C2'  --C2 - OBS
,'C3'  --C3 - OBS
,'CC2'  --C2 COTS - OBS
,'CLR'  --CATHETER LAB RECOVERY - Recovery
,'CT TRANSPLANT'  --CT TRANSPLANT - Critical Care
,'CTTRAN'--new code for CT Transplant as per ward changes for JAC email from SWilliam - 01/06/2016 CM added
,'CTCU'  --CTCU - Critical Care
,'DEN'  --MAXILLOFACIAL OP/DC - Dental Chairs - No Beds
,'DL'  --DISCHARGE UNIT - No Beds
,'DS'  --DELIVERY SUITE - OBS
,'DU'  --SLEEP SERVICES - Sleep Service
,'F8M'  --STARLIGHT WARD - Paeds
,'GIU'  --GI Unit - 
,'ICA'  --ACUTE - ICU - Critical Care
,'JIM'  --JIM QUICK TRANSPLANT UNIT - Critical Care
,'KDU'  --STARLIGHT DAY UNIT - Paeds
,'NIGHTINGALE'  --NIGHTINGALE - Day Case Unit
,'NGALE'--new code for Nightingale as per ward changes for JAC email from SWilliam - 01/06/2016 CM added
,'NIHR'  --Allergy Service CRC - no beds, only trollies or chairs
,'RAD'  --RADIOLOGY - no beds
,'REC'  --THEATRE RECOVERY - Recovery
,'SCB'  --NEONATAL BABY UNIT - Paeds
,'SPIREW'--Spire Manchester Hospital
,'TCU'  --TCU UNWELL BABIES - Paeds
,'TCW'  --WCH TREATMENT CENTRE - Withington
,'TDC'  --TDC - DAYCASE UNIT - Day Case Unit
,'UAU'  --WCH UAU UROL ASSESS  UNIT - Withington
,'UAUWYT'  --UROLOGY ASSESSMENT UNIT - No Beds on SB, Minimal Numbers on LPI
,'UCC'  --URGENT CARE CENTRE - No Beds on SB, Minimal Numbers on LPI
,'WHS'  --WOMENS HEALTH SUITE - Women's Health
,'HCAWILM' -- HCA Wilmslow (Private Hospital)


)

--Group By
--SB.WardCode
--,SB.WardName
--,WD.Division

order by

CensusDate desc,
Datepart(Hour,SB.CensusDate) desc,
WD.Division,
SB.WardCode asc


--Select * from
--WHReporting.LK.WardtoDivision WD
--Order by Ward