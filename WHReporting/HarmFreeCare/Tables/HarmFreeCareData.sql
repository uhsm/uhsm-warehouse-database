﻿CREATE TABLE [HarmFreeCare].[HarmFreeCareData](
	[Ward] [nvarchar](255) NULL,
	[Date] [datetime] NULL,
	[Indicator] [nvarchar](255) NULL,
	[Yes] [float] NULL,
	[No] [float] NULL,
	[N/A] [float] NULL
) ON [PRIMARY]