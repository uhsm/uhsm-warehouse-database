﻿-- =============================================
-- Author:	CMan
-- Create date: 29/04/2014
-- Description:	Used in SSRS report  to give RRO prior ICE reports for patients

-- =============================================
CREATE PROCEDURE [ICE].[ExtractICEReportsforRRO]

AS
BEGIN


SELECT [ReportDate]
      ,[ReportIndex]
      ,[RequestIndex]
      ,[RequestDate]
      ,[HospitalNumber]
      ,[FacilID]
      ,[NHSNumber]
      ,[ICEHospitalNumber]
      ,[SetRequested]
      ,[Result_Code]
      ,[exam_description]
      ,[case_type]
      , [Comments] as Comments
      ,[OrderNumber]
      ,[PerformanceDate]
      ,Created
  FROM [WHREPORTING].[ICE].[ICEReportsforRRO]
  where substring(case_type, 1, CHARINDEX(';', Case_type)-1) in ('C','M','X')


END