﻿CREATE Procedure [ICE].[FilingReportWho] 
	@Who varchar(50),
	@SQL varchar(8000)
as	

--Set @Who = 'ResponsibleConsultant'

IF @Who = 'OrderingClinician'
	BEGIN 
		set @SQL = 'Select  Distinct
					ResponsibleClinician =  Clinician.ClinicianSurname
					from ICE.DimClinician Clinician
					Inner Join ICE.FactReport Report 
					on Clinician.ClinicianKey = Report.MasterClinicianCode
					Where 
					Report.ReportDate >= @StartDate
					and Report.ReportDate <=@EndDate
					and Report.MasterLocationCode in (@Location)'
	END

ELSE IF @Who = 'ResponsibleConsultant'
		set @SQL = '
					Select Distinct
					ResponsibleClinician = Coalesce(IP.Consultantname,OP.ProfessionalCarerName,-1)
					from ICE.FactReport Report
					Left Outer Join APC.Episode IP with (nolock) 
						on Report.InpatientEpisodeUniqueID = IP.EpisodeUniqueID
					Left Outer Join OP.Schedule OP with (nolock)
						on Report.OutpatientAppointmentUniqueID = OP.SourceUniqueID
					Where 
					Report.ReportDate >= @StartDate
					and Report.ReportDate <=@EndDate
					and 

						(
						Report.MasterLocationCode in (@Location)
						or @Location = 0
						)'