﻿CREATE Procedure [ICE].[MDTReferralsForLungCancer] 

-- Created by Graham Ryder 24/08/2015 for MDT Referrals For Lung Cancer SSRS report

@WeekEndingOfRequest nvarchar(max)

as

SELECT 
DISTINCT 
--cal.WeekNoKey
--,cal.WeekNo
cal.LastDayOfWeek as WeekEndingOfRequest
,pat.NHSNumber
,prov.ProviderDiscipline
      ,pat.ICEHospitalNumber
      ,pat.FacilID
      ,loc.LocationType
      ,loc.LocationName
      ,RequestIndex
      ,RequestDate + cast(RequestTime AS DATETIME) AS RequestDateTime
      ,cast(RequestDate as DATETIME) as RequestDate
      ,datename(weekday,RequestDate) as RequestDayOfWeek
      ,cl.ClinicianForename + ' ' + cl.ClinicianSurname AS RequestingClinician -- this is the clinician under which the test was ordered
      ,prov.ProviderType 
      ,rq.SetRequested
      ,tests.Service_Request_Test_Index
      ,tests.Test_Code
      ,tests.Test_Name
      ,Case when rq.RequestStatusCode=7 then 'Deleted request' else 'Other' end as [Deleted Request?]
      ,srd.UserName--user who ordered the test
      ,RP.Prompt_Desc--this is the prompt which displays on screen to ask user to enter MDT date
      ,cast(ServiceRequestTestInformation.value AS DateTime) as MDTDate -- this is the MDT Date
      ,datename(weekday,cast(ServiceRequestTestInformation.value AS DateTime)) as MDTDayOfWeek      
      ,Case when DATEDIFF(minute,RequestDate + cast(RequestTime AS DATETIME),cast(ServiceRequestTestInformation.value AS Date) + CAST('09:00' as DATETIME))
      <64*60
      then '< 64 Hrs'
      when DATEDIFF(minute,RequestDate + cast(RequestTime AS DATETIME),cast(ServiceRequestTestInformation.value AS Date) + CAST('09:00' as DATETIME))
      >=64*60
      then '>= 64 Hrs' else null end as TimeBetweenRequestAnd9amOnMDTDate
      
      
--,case when rq.RequestDate between '01 jan 2014' and '31 jul 2014' then '1st Jan 2014 - 31st Jul 2014'
--when rq.RequestDate between '01 jan 2015' and '31 jul 2015' then '1st Jan 2015 - 31st Jul 2015' else null end as RequestPeriod
--INTO #Req

--,srd.*
--,tests.*
FROM WHREPORTING.ICE.FactRequest rq
LEFT JOIN WHREPORTING.ICE.Patient pat ON rq.MasterPatientCode = pat.PatientKey
LEFT JOIN WHREPORTING.ICE.DimLocation loc ON rq.MasterLocationCode = loc.LocationKey
LEFT JOIN WHREPORTING.ICE.DimProvider prov ON rq.ServiceProviderCode = prov.ProviderKey
LEFT JOIN ICE_DB.dbo.OrderTests tests ON rq.RequestIndex = tests.Service_Request_Index
LEFT JOIN ICE_DB.dbo.ServiceRequestDetail srd ON rq.RequestIndex = srd.Request_Index
LEFT JOIN WHREPORTING.ICE.DimClinician cl ON rq.MasterClinicianCode = cl.ClinicianKey
left join WHREPORTING.LK.Calendar cal
on rq.RequestDate=cal.TheDate
/****The next join below allows us to pull through answers relating to questions which relate only to the MDT tests***********/

LEFT Join (Select * from ICE_DB.dbo.ServiceRequestTestInformation -- this is the table where the value of the MDT data is stored
                        where Test_Id = '2465'-- this is the id for the MDTLUNGCREF test
                                                                                                            and remote_key = '1096' --this is the remote key id which will allow us to extract the row that specifically relates to the MDT Date entry
                  )ServiceRequestTestInformation  on rq.RequestIndex = ServiceRequestTestInformation.Service_Request_Id
Left outer join ICE_DB.dbo.Request_Prompt RP  --this is the table detailing the dofferent prompts which appear depending on which tests have been requested
on ServiceRequestTestInformation.remote_key = RP.Prompt_Index

WHERE 
--srd.UserName = 'pkeast'
--      AND 
--      RequestDate >= '20140331' 
--      and RequestDate<='20150208'
--and cal.WeekNoKey<=(select WeekNoKey from WHREPORTING.LK.Calendar where thedate=DATEADD(day,DATEDIFF(day,0,GETDATE())-1,-6))
rq.RequestDate>='01 jan 2015'


--and cal.LastDayOfWeek in(@WeekEndingOfRequest)

and cast(cast(cal.LastDayOfWeek as int) as nvarchar(max)) in(SELECT Item FROM WHREPORTING.dbo.Split (@WeekEndingOfRequest, ','))



--      AND RequestDate < '20140930'
--      AND rq.RequestStatusCode <> 7 -- only those requesst which have not been deleted    
--and tests.Test_Name like '%Ambulatory%'
--and (tests.Test_Name like '%EEG%'
--or (tests.Test_Name like '%ELECTRO MYOGRAPHY%'
--or (tests.Test_Name like '%NERVE CONDUCTION STUDY%'
--or (tests.Test_Name like '%Carpal Tunnel Syndrome%'))))
and tests.Test_Name like '%MDT LUNG CANCER Referral%'
and cl.ClinicianForename + ' ' + cl.ClinicianSurname <> 'X Xxtest'

order by RequestDate