﻿-- =============================================
-- Author:		CMan
-- Create date: 10/06/2014
-- Description:	Stored procedure to support the SSRS report - Breast Radiology Pre-operative procedures requested 
--				Job RD479
--				Report development discussed with Megan Bydder and Alison Allcut
--				To assist Megan Bydder with planning
/*
 Modified Date				Modified By			Modification
-----------------			------------		------------
21/07/2015				CMan				Link from service request test information - test id field no longer works to link to order test table following CRIS go live- having this in the join prevented the new exam codes NSENT and XPATH from 
												coming through into the report.  Removed this link from the join - see comment in script below
											Alsoi added ZNMDT into the list of MDT reports to be displayed - new code on CRIS for MDT reports											
*/






-- =============================================
CREATE PROCEDURE [ICE].[ReportBreastPreoperativeProceduresRequested]
	-- Add the parameters for the stored procedure here
@startdate as datetime, @enddate as datetime 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;




SELECT DISTINCT OT.Test_Code,
                OT.Test_Name,
                OT.Test_Index,
                SR.Service_Request_Index,
                SR.DateTime_Of_Request,
                SR.Patient_id_key,
                SR.Hospital_Number,
                SR.Status,
                Pat.ICEHospitalNumber,
                Pat.Forename,
                Pat.Surname,
                SR.Clinician_Index,
                cl.ClinicianSurname AS RequestingClinician,
                RP.Prompt_Desc,
                SRTI.remote_key,
                case when SRTI.value ='' then null else convert(datetime,SRTI.value) end as OpDate,
                RRO.RequestDate,
                RRO.ReportDate,
                RRO.Result_Code, 
                max(RRO.Comments) as Comments
FROM   ICE_DB.dbo.OrderTests OT --look for all service requests which once of the localisation and sentinal node procedures ordered on ICE
       LEFT OUTER JOIN ICE_DB.dbo.ServiceRequest SR
                    ON OT.Service_Request_Index = SR.Service_Request_Index --join back to service request to get the details of the request - i.e. the request date, requesting clinician etc.
       LEFT OUTER JOIN WHREPORTING.ICE.Patient Pat
                    ON SR.Patient_id_key = Pat.PatientKey
       LEFT OUTER JOIN WHREPORTING.ICE.DimClinician cl
                    ON SR.Clinician_Index = Cl.ClinicianKey
       LEFT outer join ICE_DB.dbo.ServiceRequestTestInformation SRTI -- this table is where the information around what date has been entered as the operation date etc is found
       on SR.Service_Request_Index = SRTI.Service_Request_ID-- and OT.Test_Index = SRTI.Test_Id- 21/07/2015 CM - commented out the link between OT.Test_Index and SRTI.Test_Id as this appeared to be preventing the new test codes of NSENT and XPATH from appearing in the report
      Left outer join ICE_DB.dbo.Request_Prompt RP 
      on SRTI.remote_key = RP.Prompt_Index
      Left outer join (Select * from WHREPORTING.ICE.ICEReportsforRRO
where SetRequested like '%BRMDT%'
or SetRequested  like '%ZNMDT%') RRO --21/07/2015 CM - added new code of 'ZNMDT'
on SR.Hospital_Number = RRO.ICEHospitalNumber--add in the details of any Breast MDT reports that may be related to the Pre op procedure requested.  Only BRMDT reports are required as per MB
WHERE  OT.Test_Code IN ( 'NSENTL', 'NSENTR', 'UMAXL', 'UMAXR',
                      'UMAWL', 'UMAWR', 'XMULL', 'XMULR', 'NSENT', 'XPATH' )--these are the tests we want to extract
                      --CM 18/05/2015 Added NSENT and XPATH to list above as per MBydder email from 17/05/2015
       AND SR.Hospital_Number <> '4033673' --include this criteria to remove any tests relating to this patient id as this is a test patient named Freddy/Fred Test
		AND  SRTI.remote_key = '655' -- this is the ID for the Prompt to get Operation date entry
	and not exists 
			(
			Select 1 
			from ICE_DB.dbo.ServiceRequestInformation RequestInfo
			Where SR.Service_Request_Index = RequestInfo.Service_Request_Id
			and RequestInfo.Information_Type = 'D'
			)--these are the cancelled requests
	and SR.Status <> 'DEL'	--excluded any deleted requests	
	and   case when SRTI.value ='' then null else convert(datetime,SRTI.value) end >= @startdate
	and   case when SRTI.value ='' then null else convert(datetime,SRTI.value) end <= @enddate
 --AND SR.Hospital_Number = '4364891'
 
 
Group by OT.Test_Code,
                OT.Test_Name,
                OT.Test_Index,
                SR.Service_Request_Index,
                SR.DateTime_Of_Request,
                SR.Patient_id_key,
                SR.Hospital_Number,
                SR.Status,
                Pat.ICEHospitalNumber,
                Pat.Forename,
                Pat.Surname,
                SR.Clinician_Index,
                cl.ClinicianSurname,
                RP.Prompt_Desc,
                SRTI.remote_key,
                case when SRTI.value ='' then null else convert(datetime,SRTI.value) end,
                RRO.RequestDate,
                RRO.ReportDate,
                RRO.Result_Code
 
 
 
 
Order by   case when SRTI.value ='' then null else convert(datetime,SRTI.value) end,  SR.Service_Request_Index, RRO.ReportDate






 
END