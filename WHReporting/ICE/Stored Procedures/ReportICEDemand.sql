﻿CREATE procedure [ICE].[ReportICEDemand]

    @DateFrom datetime
     ,@DateTo datetime
    ,@Provider varchar (100)
    ,@Specialty varchar (100)
  


as


select distinct
rq.RequestRecno
,cl.ClinicianSurname
,Case when s.SpecialtyFunction is null then 'Unknown' else s.SpecialtyFunction end as SpecialtyFunction
,1 as Cases
,rs.Result_Code
,c.TheMonth
from 


ICE.FactResults rs inner join 
ICE.FactInvestigation i on i.InvestigationIndex=rs.Investigation_Index inner join
ICE.FactRequest rq on rq.RequestIndex=i.RequestIndex inner join 
ICE.DimClinician cl on cl.ClinicianKey=rq.MasterClinicianCode left outer join 
LK.SpecialtyDivision s on cast(s.SpecialtyFunctionCode as varchar)=cast(cl.ClinicianSpecialtyCode as varchar)
inner join LK.Calendar c on c.TheDate=rq.RequestDate
inner join ICE.DimProvider p on p.ProviderKey=rq.ServiceProviderCode


where rs.Date_Added between @DateFrom and @DateTo
and case when s.SpecialtyFunctionCode is null then ('u') else cast(s.SpecialtyFunctionCode as varchar) end in (@Specialty)
and p.ProviderKey in (@Provider)

group by
cl.ClinicianSurname
,Case when s.SpecialtyFunction is null then 'Unknown' else s.SpecialtyFunction end,
rq.MasterPatientCode
,rq.RequestRecno
,rs.Result_Code
,c.TheMonth