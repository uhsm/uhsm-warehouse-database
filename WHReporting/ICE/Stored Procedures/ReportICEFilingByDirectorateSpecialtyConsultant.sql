﻿-- =============================================
-- Author:		CMan
-- Create date: 19/10/2014
-- Description:	Replacing the script which was embedded in the original SSRS Report 'ICE Filing Report - Aggregare by Directorate, specialty and Consultant
-- =============================================
CREATE PROCEDURE [ICE].[ReportICEFilingByDirectorateSpecialtyConsultant]  @startdate datetime, @enddate datetime,  @Directorate varchar(400), @Location varchar(400) ,
@Clinician varchar(max),  
@Provider varchar(50),-- @ProviderDiscipline varchar(250) ,
@abnormal varchar(5)
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


--Declare @startdate  as datetime, 
--@enddate as datetime

--set @startdate = '20131201'
--set @enddate = '20131231'

--Create a temp table with a smaller reports dataset that we want to work with.
--Only includes reports with the date parameters of those selected within the report
--Also includes a criteria in the where clause which states where the latest is to be different from 0 --this was in the original logic of the ICE filing reports -? query with Deb Rose why?

If object_id ('tempdb..#reports') > 0
Drop table #reports

SELECT ReportIndex,
       RequestIndex,
       RequestDateTime,
       MasterClinicianCode,
       MasterPatientCode,
       MasterLocationCode,
       FilingStatusKey,
       UserFiledKey,
       ReportDate,
       ReportTime,
       InpatientEpisodeUniqueID,
       OutpatientAppointmentUniqueID,
       Specialty,
       Latest,
       ServiceProviderCOde
INTO   #reports
FROM   WHREPORTING.ICE.FactReport
WHERE  ( ReportDate >= @StartDate )
       AND ( ReportDate <= @EndDate )
       AND Latest <> 0 


--Create temp table with all the records which have an abnormal flag  - to be used in the final query 
--Temp table created here as some reports may have both an abnormal flag and a normal flag as the abnormal flag is actually assigned to an abnormal result 
--however a report may contain both normal and abnormal results if more than one investigation was required in the original request - in the front end, it appears that any report that contains even one abnormal result is flagged as such 
--therefore a temp table is created here to get a distinct list of reports which have an abnormal flag.  This can then be used to determing normal and abnormal reports in the final query
If OBJECT_ID('tempdb..#abnormal') > 0
Drop table #abnormal
SELECT Inv.RequestIndex,
       Inv.ReportIndex,
       Result.Abnormal_Flag
INTO   #abnormal
FROM   WHREPORTING.ICE.FactReport Reports
       LEFT OUTER JOIN WHREPORTING.ICE.FactInvestigation Inv
                    ON Reports.ReportIndex = Inv.ReportIndex
       LEFT OUTER JOIN ICE_DB.dbo.Serviceresult Result
                    ON Inv.InvestigationIndex = Result.Investigation_Index
WHERE  result.abnormal_flag <> 0
       AND Reports.ReportDate >= @startdate
       AND Reports.ReportDate <= @enddate
GROUP  BY Inv.RequestIndex,
          Inv.ReportIndex,
          Result.Abnormal_Flag 



--create clinician specialty and directorate temp table to be used in final query
--Linking clinician table to specialty table so that the case statements can be defined here into a temp table to be used in the final query, which will hopefully help to query run faster 
If OBJECT_ID('tempdb..#RequestDoc') > 0
Drop table #RequestDoc
SELECT distinct clin.ClinicianKey,
       clin.ClinicianSurname,
       clin.ClinicianSpecialtyCode,
       Isnull(( CASE
                  WHEN sd.Direcorate = 'Cardio & Thoracic' THEN 'Cardiothoracic'
                  WHEN clin.clinicianspecialtycode IN ( '242', '500' ) THEN 'Womens & Childrens'
                  WHEN clin.clinicianspecialtycode IN ( '172' ) THEN 'Cardiothoracic'
                  WHEN clin.clinicianspecialtycode IN ( '653' ) THEN 'Therapies'
                  ELSE sd.Direcorate
                END ), 'Unknown')          AS Directorate,
       Isnull(sd.MainSpecialty, 'Unknown') AS Specialty
into #RequestDoc
FROM   WHReporting.ICE.DimClinician clin
       LEFT OUTER JOIN lk.specialtydivision sd
                    ON sd.SpecialtyCode = Cast(( CASE
                                                   WHEN clin.ClinicianKey = 3460 THEN 370
                                                   ELSE clin.clinicianspecialtycode
                                                 END ) AS VARCHAR) 




SELECT DISTINCT Location.LocationName,
                LocationCode = Report.MasterLocationCode,
                ProviderTypeCode = Report.ServiceProviderCode,
                Provider.ProviderType,
                RequestDoc.ClinicianSurname,
                report.reportindex,
                Report.MasterClinicianCode,
                FileStatus.FilingStatus,
                NotViewed = CASE
                              WHEN FileStatus.FilingStatusKey = 0 THEN 1
                              ELSE 0
                            END,
                NotFiled = CASE
                             WHEN FileStatus.FilingStatusKey = 1 THEN 1
                             ELSE 0
                           END,
                Filed = CASE
                          WHEN FileStatus.FilingStatusKey = 2 THEN 1
                          ELSE 0
                        END,
                Total = 1,
                CASE
                  WHEN provider.ProviderDisciplineKey = '13' THEN '4'
                  ELSE provider.ProviderDisciplineKey
                END                    AS ProviderDisciplineKey,
                CASE
                  WHEN provider.ProviderDiscipline = 'BLOOD TRANSFUSION Wythenshawe' THEN 'Blood Transfusion'
                  ELSE provider.ProviderDiscipline
                END                    AS ProviderDiscipline,
                RequestDoc.Directorate AS Directorate,
                RequestDoc.Specialty   AS Specialty
FROM   #reports Report
       LEFT OUTER JOIN ICE.DimFilingStatus FileStatus
                    ON Report.FilingStatusKey = FileStatus.FilingStatusKey
       LEFT OUTER JOIN ICE.DimLocation Location
                    ON Report.MasterLocationCode = Location.LocationKey
       LEFT OUTER JOIN #RequestDoc AS RequestDoc WITH (nolock)
                    ON Report.MasterClinicianCode = RequestDoc.ClinicianKey
       LEFT OUTER JOIN ICE.DimProvider Provider
                    ON Report.Specialty = Provider.ProviderSpecialtyCode
       LEFT OUTER JOIN ICE_DB.dbo.ServiceRequest AS Request
                    ON Report.RequestIndex = Request.Service_Request_Index
       LEFT OUTER JOIN #abnormal
                    ON Report.ReportIndex = #abnormal.ReportIndex
WHERE 
 Report.MasterClinicianCode IN (SELECT Item
                                      FROM   dbo.Split (@Clinician, ','))
       AND 
report.MasterLocationCode IN (SELECT Item
                                       FROM   dbo.Split (@Location, ','))
      AND provider.ProviderTypeKey IN (SELECT Item
                                        FROM   dbo.Split (@Provider, ','))
     --AND CASE
     --        WHEN provider.ProviderDisciplineKey = '13' THEN '4'
     --        ELSE provider.ProviderDisciplineKey
     --     END IN (SELECT Item
     --              FROM   dbo.Split (@ProviderDiscipline, ','))
		AND   report.latest = 1
       AND RequestDoc.Directorate IN (SELECT Item
                                      FROM   dbo.Split (@Directorate, ','))
       AND CASE
             WHEN #abnormal.Abnormal_Flag = 1 THEN #abnormal.Abnormal_Flag
             ELSE 0
           END IN (SELECT Item
                   FROM   dbo.Split (@abnormal, ','))
GROUP  BY report.reportindex,
          Location.LocationName,
          Report.MasterLocationCode,
          Report.ServiceProviderCode,
          Provider.ProviderType,
          RequestDoc.ClinicianSurname,
          Report.MasterClinicianCode,
          FileStatus.FilingStatus,
          CASE
            WHEN FileStatus.FilingStatusKey = 0 THEN 1
            ELSE 0
          END,
          CASE
            WHEN FileStatus.FilingStatusKey = 1 THEN 1
            ELSE 0
          END,
          CASE
            WHEN FileStatus.FilingStatusKey = 2 THEN 1
            ELSE 0
          END,
          CASE
            WHEN provider.ProviderDisciplineKey = '13' THEN '4'
            ELSE provider.ProviderDisciplineKey
          END,
          CASE
            WHEN provider.ProviderDiscipline = 'BLOOD TRANSFUSION Wythenshawe' THEN 'Blood Transfusion'
            ELSE provider.ProviderDiscipline
          END,
          RequestDoc.Directorate,
          RequestDoc.Specialty 


END