﻿-- =============================================
-- Author:		CMan
-- Create date: 20/10/2014
-- Description:	Replacing the script which was embedded in the original SSRS Report 'ICE Filing Report - Aggregare by Directorate, specialty and Consultant
-- =============================================
CREATE  PROCEDURE [ICE].[ReportICEFilingByDirectorateSpecialtyConsultant2] 
@startdate datetime, @enddate datetime,  @Directorate varchar(400), @Location varchar(max) ,
@Clinician varchar(max),  
@Provider varchar(50), @ProviderDiscipline varchar(250) ,
@abnormal varchar(5),
@cxr varchar(5)



AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


--Declare @startdate  as datetime, 
--@enddate as datetime

--set @startdate = '20130401'
--set @enddate = '20140701'

If object_id ('tempdb..#reports') > 0
Drop table #reports

SELECT distinct ReportIndex,
       RequestIndex,
       RequestDateTime,
       MasterClinicianCode,
       MasterPatientCode,
       MasterLocationCode,
       FileStatus.FilingStatus,
       CASE
                              WHEN FileStatus.FilingStatusKey = 0 THEN 1
                              ELSE 0
                            END as NotViewed,
        CASE
                             WHEN FileStatus.FilingStatusKey = 1 THEN 1
                             ELSE 0
                           END  as NotFiled,
     CASE
                          WHEN FileStatus.FilingStatusKey = 2 THEN 1
                          ELSE 0
                        END as Filed ,
       Total = 1,
       UserFiledKey,
       ReportDate,
       ReportTime,
       InpatientEpisodeUniqueID,
       OutpatientAppointmentUniqueID,
       Specialty,
       Latest,
       ServiceProviderCOde,
      case when dbo.Ufntransposeinvestigations(FactReport.ReportIndex)  like '%XCHES%' then 1 else 0 end  as IsChestXray

INTO   #reports
FROM   WHREPORTING.ICE.FactReport
  LEFT OUTER JOIN  ICE.DimFilingStatus FileStatus
                    ON FactReport.FilingStatusKey = FileStatus.FilingStatusKey
WHERE  ( ReportDate >= @StartDate )
       AND ( ReportDate <= @EndDate )
       --AND Latest <> 0 --28/07/2014 CM: Commented this out as this is now incorrect logic after investigations with DAhearn.  Correct logic for identifying valid latest reports is via the use of the Category field in the line below
       AND Category is null -- Category 1 (Added), 2 (Updated), 3(Deleted) -- any reports which have an entry in this field should be excluded from the reporting as they will no longer be visible on front end and may have been replaced with a new report index int he back end.
       



--Create temp table with all the records which have an abnormal flag  - to be used in the final query 
--Temp table created here as some reports may have both an abnormal flag and a normal flag as the abnormal flag is actually assigned to an abnormal result 
--however a report may contain both normal and abnormal results if more than one investigation was required in the original request - in the front end, it appears that any report that contains even one abnormal result is flagged as such 
--therefore a temp table is created here to get a distinct list of reports which have an abnormal flag.  This can then be used to determing normal and abnormal reports in the final query
If OBJECT_ID('tempdb..#abnormal') > 0
Drop table #abnormal
SELECT Inv.RequestIndex,
       Inv.ReportIndex,
       Result.Abnormal_Flag
INTO   #abnormal
FROM   WHREPORTING.ICE.FactReport Reports
       LEFT OUTER JOIN WHREPORTING.ICE.FactInvestigation Inv
                    ON Reports.ReportIndex = Inv.ReportIndex
       LEFT OUTER JOIN ICE_DB.dbo.Serviceresult Result
                    ON Inv.InvestigationIndex = Result.Investigation_Index
WHERE  result.abnormal_flag <> 0
       AND Reports.ReportDate >= @startdate
       AND Reports.ReportDate <= @enddate
GROUP  BY Inv.RequestIndex,
          Inv.ReportIndex,
          Result.Abnormal_Flag 



--create clinician specialty and directorate temp table to be used in final query
--Linking clinician table to specialty table so that the case statements can be defined here into a temp table to be used in the final query, which will hopefully help to query run faster 
If OBJECT_ID('tempdb..#RequestDoc') > 0
Drop table #RequestDoc
SELECT distinct clin.ClinicianKey,
       ISNULL(clin.ClinicianForename,'') + ' '  + ISNULL(clin.ClinicianSurname,'') as ClinicianSurname,
       clin.ClinicianSpecialtyCode,
       Isnull(( CASE
				WHEN clin.ClinicianKey in ('293', '605','13099','507') then 'Women & Childrens' --as per A Allcut request
				WHEN clin.ClinicianKey  in ('593',--Dr Kulharni-- as per G Ryder request
										'30204',--Dr Bose
										'30205'--Dr Basu
										) then 'Disablement Services Centre' -- as per G Ryder request
                  WHEN sd.Direcorate = 'Cardio & Thoracic' THEN 'Cardiothoracic'
                  WHEN clin.clinicianspecialtycode IN ( '242', '500' ) THEN 'Women & Childrens'
                  WHEN clin.clinicianspecialtycode IN ( '172' ) THEN 'Cardiothoracic'
                  WHEN clin.clinicianspecialtycode IN ( '653' ) THEN 'Therapies'
                  ELSE sd.Direcorate
                END ), 'Unknown')          AS Directorate,
       CASE	WHEN clin.ClinicianKey in ('293', '605','13099','507') then 'Breast Radiology' Else Isnull(sd.MainSpecialty, 'Unknown') end  AS Specialty,--Breast Radiology as per A Allcut request
        Case when clin.clinicianspecialtycode IN ( '242', '500' )  then 'Scheduled Care' 
        when clin.ClinicianKey  in ('593',--Dr Kulharni-- as per G Ryder request
										'30204',--Dr Bose
										'30205'--Dr Basu
										) then  'Clinical Support'   
        when clin.ClinicianKey in ('293', '605','13099','507') then 'Scheduled Care' --as per A Allcut request
        else  sd.division end as Division
into #RequestDoc
FROM   WHReporting.ICE.DimClinician clin
       LEFT OUTER JOIN lk.specialtydivision sd
                    ON sd.SpecialtyCode = Cast(( CASE
                                                   WHEN clin.ClinicianKey = 3460 THEN 370
                                                   ELSE clin.clinicianspecialtycode
                                                 END ) AS VARCHAR) 




 --temp table to sum the report counts before joining  
 If object_id ('tempdb..#reports_sum') > 0
Drop table #reports_sum  

 Select Request.MasterClinicianCode,
		#reports.MasterLocationCode,
		#reports.ServiceProviderCOde,
		#reports.Specialty,
		FilingStatus,
		case when #abnormal.abnormal_flag = 1 then 1 
		else 0 end as abnormalflag,
		IsChestXray,
		SUM(NotViewed) as NotViewed,
		SUM(NotFiled) as NotFiled,
		SUM(Filed) as Filed,
		SUM(Total) as Total
into #reports_sum
 from 
#reports
  LEFT OUTER JOIN WHREPORTING.ICE.FactRequest AS Request
                    ON #reports.RequestIndex = Request.RequestIndex
       LEFT OUTER JOIN #abnormal
                    ON #reports.ReportIndex = #abnormal.ReportIndex
--Left outer join  #chest on Request.Service_Request_Index = #chest.Service_Request_Index
 Group By  Request.MasterClinicianCode,
		#reports.MasterLocationCode,
		#reports.ServiceProviderCOde,
		#reports.Specialty,
		FilingStatus,
		case when #abnormal.abnormal_flag = 1 then 1 
		else 0 end,
		FilingStatus,
		IsChestXray
       









SELECT DISTINCT Location.LocationName,
                LocationCode = Report.MasterLocationCode,
                ProviderTypeCode = Report.ServiceProviderCode,
                Provider.ProviderType,
                RequestDoc.ClinicianSurname,
                --report.reportindex,
                Report.MasterClinicianCode,
                report.FilingStatus,
                report.NotViewed ,
                report.NotFiled,
                report.Filed,
                report.Total,
                CASE
                  WHEN provider.ProviderDisciplineKey = '13' THEN '4'
                  ELSE provider.ProviderDisciplineKey
                END                    AS ProviderDisciplineKey,
                CASE
                  WHEN provider.ProviderDiscipline = 'BLOOD TRANSFUSION Wythenshawe' THEN 'Blood Transfusion'
                  ELSE provider.ProviderDiscipline
                END                    AS ProviderDiscipline,
                RequestDoc.Directorate AS Directorate,
                RequestDoc.Specialty   AS Specialty,
                RequestDoc.Division as Division,
                Report.abnormalflag,
                Report.IsChestXray
FROM   #reports_sum Report
       LEFT OUTER JOIN ICE.DimLocation Location
                    ON Report.MasterLocationCode = Location.LocationKey
       LEFT OUTER JOIN #RequestDoc AS RequestDoc WITH (nolock)
                    ON Report.MasterClinicianCode = RequestDoc.ClinicianKey
       LEFT OUTER JOIN ICE.DimProvider Provider
                    ON Report.Specialty = Provider.ProviderSpecialtyCode
WHERE 
 Report.MasterClinicianCode IN (SELECT Item
                                    FROM   dbo.Split (@Clinician, ','))
AND 

report.MasterLocationCode IN (SELECT Item
                                       FROM   dbo.Split (@Location, ','))
     AND 
provider.ProviderTypeKey in (SELECT Item
                                   FROM   dbo.Split (@Provider, ','))
     AND CASE
             WHEN provider.ProviderDisciplineKey = '13' THEN '4'
             ELSE provider.ProviderDisciplineKey
          END IN (SELECT Item
                   FROM   dbo.Split (@ProviderDiscipline, ','))
  AND 
RequestDoc.Directorate IN (SELECT Item
                                    FROM   dbo.Split (@Directorate, ','))
       AND report.AbnormalFlag IN (SELECT Item
                   FROM   dbo.Split (@abnormal, ','))
                   and report.IsChestXray in ( SELECT Item
                   FROM   dbo.Split (@cxr, ','))
END