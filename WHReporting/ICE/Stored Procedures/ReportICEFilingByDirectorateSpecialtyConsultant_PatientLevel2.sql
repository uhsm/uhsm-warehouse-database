﻿-- =============================================
-- Author:		CMan
-- Create date: 20/02/2014
-- Description:	 Report for patient level drill through of the ReportICEFilingByDirectorateSpecialtyConsultant SSRS.
			-- script replaces the query which was embedded in the SSRS report
-- =============================================
CREATE PROCEDURE [ICE].[ReportICEFilingByDirectorateSpecialtyConsultant_PatientLevel2]
 @startdate datetime, @enddate datetime,  @Directorate varchar(400),
@Location varchar(max) ,
@Clinician varchar(max),  
@ProviderDiscipline varchar(250) ,
@abnormal varchar(5),
@cxr varchar(5),
@Provider varchar(10),
@FilingStatus varchar(50)



AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here



--Declare @startdate  as datetime, 
--@enddate as datetime

--set @startdate = '20131201'
--set @enddate = '20131210'


--Create a temp table with a smaller reports dataset that we want to work with.
--Only includes reports with the date parameters of those selected within the report
--Also includes a criteria in the where clause which states where the latest is to be different from 0 --this was in the original logic of the ICE filing reports -? query with Deb Rose why?


If object_id ('tempdb..#reports') > 0
Drop table #reports

SELECT distinct ReportIndex,
       RequestIndex,
       RequestDateTime,
       MasterClinicianCode,
       MasterPatientCode,
       MasterLocationCode,
       FactReport.FilingStatusKey,
       FileStatus.FilingStatus,
       CASE
                              WHEN FileStatus.FilingStatusKey = 0 THEN 1
                              ELSE 0
                            END as NotViewed,
        CASE
                             WHEN FileStatus.FilingStatusKey = 1 THEN 1
                             ELSE 0
                           END  as NotFiled,
     CASE
                          WHEN FileStatus.FilingStatusKey = 2 THEN 1
                          ELSE 0
                        END as Filed ,
       Total = 1,
       UserFiledKey,
       ReportDate,
       ReportTime,
       InpatientEpisodeUniqueID,
       OutpatientAppointmentUniqueID,
       Specialty,
       Latest,
       ServiceProviderCOde,
      case when dbo.Ufntransposeinvestigations(FactReport.ReportIndex)  like '%XCHES%' then 1 else 0 end  as IsChestXray,
		dbo.Ufntransposeinvestigations(FactReport.ReportIndex)   as Investigations
INTO   #reports
FROM   WHREPORTING.ICE.FactReport
  LEFT OUTER JOIN  ICE.DimFilingStatus FileStatus
                    ON FactReport.FilingStatusKey = FileStatus.FilingStatusKey
WHERE  ( ReportDate >= @StartDate )
       AND ( ReportDate <= @EndDate )
       --AND Latest <> 0 --28/07/2014 CM: Commented this out as this is now incorrect logic after investigations with DAhearn.  Correct logic for identifying valid latest reports is via the use of the Category field in the line below
       AND Category is null -- Category 1 (Added), 2 (Updated), 3(Deleted) -- any reports which have an entry in this field should be excluded from the reporting as they will no longer be visible on front end and may have been replaced with a new report index int he back end.

       




--Create temp table with all the records which have an abnormal flag  - to be used in the final query 
--Temp table created here as some reports may have both an abnormal flag and a normal flag as the abnormal flag is actually assigned to an abnormal result 
--however a report may contain both normal and abnormal results if more than one investigation was required in the original request - in the front end, it appears that any report that contains even one abnormal result is flagged as such 
--therefore a temp table is created here to get a distinct list of reports which have an abnormal flag.  This can then be used to determing normal and abnormal reports in the final query
If OBJECT_ID('tempdb..#abnormal') > 0
Drop table #abnormal
SELECT Inv.RequestIndex,
       Inv.ReportIndex,
       Result.Abnormal_Flag
INTO   #abnormal
FROM   WHREPORTING.ICE.FactReport Reports
       LEFT OUTER JOIN WHREPORTING.ICE.FactInvestigation Inv
                    ON Reports.ReportIndex = Inv.ReportIndex
       LEFT OUTER JOIN ICE_DB.dbo.Serviceresult Result
                    ON Inv.InvestigationIndex = Result.Investigation_Index
WHERE  result.abnormal_flag <> 0
       AND Reports.ReportDate >= @startdate
       AND Reports.ReportDate <= @enddate
GROUP  BY Inv.RequestIndex,
          Inv.ReportIndex,
          Result.Abnormal_Flag 



--create clinician specialty and directorate temp table to be used in final query
--Linking clinician table to specialty table so that the case statements can be defined here into a temp table to be used in the final query, which will hopefully help to query run faster 
If OBJECT_ID('tempdb..#RequestDoc') > 0
Drop table #RequestDoc
SELECT distinct clin.ClinicianKey,
       clin.ClinicianSurname,
       clin.ClinicianSpecialtyCode,
       Isnull(( CASE
                  WHEN sd.Direcorate = 'Cardio & Thoracic' THEN 'Cardiothoracic'
                  WHEN clin.clinicianspecialtycode IN ( '242', '500' ) THEN 'Womens & Childrens'
                  WHEN clin.clinicianspecialtycode IN ( '172' ) THEN 'Cardiothoracic'
                  WHEN clin.clinicianspecialtycode IN ( '653' ) THEN 'Therapies'
                  ELSE sd.Direcorate
                END ), 'Unknown')          AS Directorate,
       Isnull(sd.MainSpecialty, 'Unknown') AS Specialty
into #RequestDoc
FROM   WHReporting.ICE.DimClinician clin
       LEFT OUTER JOIN lk.specialtydivision sd
                    ON sd.SpecialtyCode = Cast(( CASE
                                                   WHEN clin.ClinicianKey = 3460 THEN 370
                                                   ELSE clin.clinicianspecialtycode
                                                 END ) AS VARCHAR) 




--create temp table to join reports to abnormal

If object_id ('tempdb..#reports2') > 0
Drop table #reports2
Select   MasterClinicianCode,
       MasterPatientCode,
       MasterLocationCode,
       #reports.FilingStatusKey,
       #reports.FilingStatus,
       Specialty, 
       IsChestXray,
       case when #abnormal.abnormal_flag = 1 then #abnormal.abnormal_flag else 0 end as AbnormalFlag,
       SUM(NotViewed) as NotViewed,
       SUM(NotFiled) as NotFiled,
       SUM(Filed) as Filed,
       SUM(Total) as Total
into #reports2
from #reports
left outer join #abnormal
on #reports.Reportindex = #abnormal.reportindex

Group by  MasterClinicianCode,
       MasterPatientCode,
       MasterLocationCode,
       #reports.FilingStatusKey,
       Specialty,
       IsChestXray,
        case when #abnormal.abnormal_flag = 1 then #abnormal.abnormal_flag else 0 end ,
         #reports.FilingStatus




SELECT DISTINCT --Report.ReportIndex,
                --Report.RequestIndex,
               -- Isnull(Report.RequestDateTime, Request.DateTime_Of_Request)       AS RequestDateTime,
                Location.LocationName ,
                Location.LocationTypeKey                                          AS RequestingLocationTypeKey,
                Location.LocationType                                             AS RequestingLocationType,
                Report.MasterClinicianCode                                        AS MasterClinicianCode,
                Report.MasterLocationCode as LocationCode,
                RequestDoc.ClinicianSurname ,
                RequestDoc.Directorate                                            AS Directorate,
                RequestDoc.Specialty                                              AS RequestingClinicianSpecialty,
               -- Report.MasterPatientCode                                          AS UniquePatientNo,
                Report.FilingStatusKey,
                Report.FilingStatus,
               -- Report.UserFiledKey,
                --ServiceUser.UserDescription                                       AS FilingUserType,
                --ServiceUser.UserName                                              AS FilingUserName,
               -- Report.ReportDate,
               -- Report.ReportTime,
                Provider.ProviderType,
                Provider.ProviderTypeKey  as ProviderTypeCode,
                CASE
             WHEN provider.ProviderDisciplineKey = '13' THEN '4'
             ELSE provider.ProviderDisciplineKey
           END as ProviderDisciplineKey,
             case when Provider.ProviderDiscipline = 'BLOOD TRANSFUSION Wythenshawe' then 'Blood Transfusion' else Provider.ProviderDiscipline end as ProviderDiscipline,
                Report.Specialty,
                --CASE
                --  WHEN Report.InpatientEpisodeUniqueID > 0 THEN
                --    CASE
                --      WHEN IP.EpisodeEndDate IS NULL THEN 'IP - Current'
                --      ELSE 'IP - Discharged'
                --    END
                --  WHEN Report.OutpatientAppointmentUniqueID > 0 THEN 'OP'
                --  ELSE ' N/A'
                --END                                                               AS iPMKeyEvent,
                --COALESCE (IP.EndWardCode, OP.ClinicCode, 'N/A')                   AS iPMServicePoint,
                --COALESCE (IP.EpisodeEndDate, OP.AppointmentDate)                  AS iPMKeyDate,
                --IP.AdmissionDate                                                  AS iPMIPAdmissionDate,
                --COALESCE (IP.ConsultantName, OP.ProfessionalCarerName, ' N/A')    AS iPMProfessionalCarer,
                --COALESCE (MPat.FacilID, Pat.ICEHospitalNumber)                    AS HospitalNumber,
                --MPat.FacilID,
                --Request.Hospital_Number                                           AS ICEPatNumber,
                --MPat.Surname + ', ' + MPat.forename                               AS PatientName,
                --srd.UserName                                                      AS RequestingUser,
                --Report.Investigations                                             AS Investigatons,
                --(SELECT DISTINCT ConsultantName
                -- FROM   (SELECT DISTINCT ConsultantCode,
                --                         ConsultantName
                --         FROM   APC.Episode
                --         WHERE  ( EpisodeStartDate >= @StartDate )
                --                AND ( EpisodeStartDate <= @EndDate )
                --         UNION
                --         SELECT 'N/A'                  AS ConsultantCode,
                --                'Consultant Not Known' AS ConsultantName) AS jsjsj
                -- WHERE  ( ConsultantCode = COALESCE (IP.ConsultantCode, 'N/A') )) AS ResponsibleConsultant,
                --CASE
                --  WHEN #abnormal.Abnormal_Flag = 1 THEN #abnormal.Abnormal_Flag
                --  ELSE 0
                --END                                                               AS AbnormalFlag,
                report.IsChestXray,
                Report.AbnormalFlag,
                Report.NotViewed,
                Report.NotFiled,
                Report.Filed,
                Report.Total
FROM   #reports2 AS Report
       LEFT OUTER JOIN #RequestDoc AS RequestDoc WITH (nolock)
                    ON Report.MasterClinicianCode = RequestDoc.ClinicianKey
       --LEFT OUTER JOIN ICE.DimServiceUser AS ServiceUser WITH (nolock)
       --             ON Report.UserFiledKey = ServiceUser.UserKey
       LEFT OUTER JOIN ICE.DimLocation AS Location WITH (nolock)
                    ON Report.MasterLocationCode = Location.LocationKey
       --LEFT OUTER JOIN APC.Episode AS IP WITH (nolock)
       --             ON Report.InpatientEpisodeUniqueID = IP.EpisodeUniqueID
       --LEFT OUTER JOIN OP.Schedule AS OP WITH (nolock)
       --             ON Report.OutpatientAppointmentUniqueID = OP.SourceUniqueID
       --LEFT OUTER JOIN ICE.Patient AS Pat WITH (nolock)
       --             ON Report.MasterPatientCode = Pat.PatientKey
       --LEFT OUTER JOIN ICE.MatchedPatients AS MPat
       --             ON MPat.PatientKey = Pat.PatientKey
       LEFT OUTER JOIN (SELECT DISTINCT ProviderSpecialtyCode,
                                        ProviderTypeKey,
                                        ProviderDisciplineKey,
                                        ProviderDiscipline,
                                        ProviderType
                        FROM   ICE.DimProvider) AS Provider
                    ON Provider.ProviderSpecialtyCode = Report.Specialty
       --LEFT OUTER JOIN ICE.DimFilingStatus AS FilingStatus WITH (nolock)
       --             ON Report.FilingStatusKey = FilingStatus.FilingStatusKey
       --LEFT OUTER JOIN ICE_DB.dbo.ServiceRequest AS Request
       --             ON Report.RequestIndex = Request.Service_Request_Index
       --LEFT OUTER JOIN #abnormal
       --             ON Report.ReportIndex = #abnormal.ReportIndex
       --LEFT OUTER JOIN ICE_DB.dbo.ServiceRequestDetail srd
       --             ON Request.Service_Request_Index = srd.Request_Index
WHERE  Report.MasterClinicianCode IN (SELECT Item
                                      FROM   dbo.Split (@Clinician, ','))
       AND report.MasterLocationCode IN (SELECT Item
                                         FROM   dbo.Split (@Location, ',')) 
       AND  Report.FilingStatusKey IN  (SELECT Item
                                         FROM   dbo.Split (@FilingStatus, ',')) 
           -- AND 
       --provider.ProviderTypeKey in (SELECT Item
       --                                FROM   dbo.Split (@Provider, ','))
       AND Provider.ProviderTypeKey IN (SELECT Item
                                        FROM   dbo.Split (@Provider, ','))
       AND CASE
             WHEN provider.ProviderDisciplineKey = '13' THEN '4'
             ELSE provider.ProviderDisciplineKey
           END IN (SELECT Item
                   FROM   dbo.Split (@ProviderDiscipline, ','))
         AND 
       RequestDoc.Directorate IN (SELECT Item
                                           FROM   dbo.Split (@Directorate, ','))
     AND 
     report.AbnormalFlag IN (SELECT Item
                     FROM   dbo.Split (@abnormal, ',')) 
       AND  report.IsChestXray IN (SELECT Item
                                    FROM   dbo.Split (@cxr, ','))


END