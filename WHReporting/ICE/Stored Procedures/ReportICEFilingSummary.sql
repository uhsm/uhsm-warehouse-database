﻿-- =============================================
-- Author:		CMan
-- Create date: 19/05/2014
-- Description: Stored Procedure for ICE Filing Summary Report
-- =============================================
CREATE PROCEDURE [ICE].[ReportICEFilingSummary]
@startdate as datetime,@enddate as datetime, @Directorate  varchar(400), @Clinician varchar(max)

AS
BEGIN

	SET NOCOUNT ON;


--Declare @startdate  as datetime, 
--@enddate as datetime

--set @startdate = '20150713'
--set @enddate = '20150714'

--Generate temp table for all report indexes relating to chest Xrays - to be referenced later in the script to create IsChestXray flag

If object_id('tempdb..#ches') > 0
Drop table #ches
Select Inv.ReportIndex, InRq.InvestigationCode
into #ches
From WHREPORTING.ICE.FactInvestigation Inv 
Left outer join WHReporting.ICE.DimInvestigationRequested InRq on Inv.InvestigationDescriptionKey = InRq.InvestigationKey
where  InRq.InvestigationCode  like  '%Xches%'

Create nonclustered index IX_RepInx on #ches(ReportIndex)

--Generate temp table for all reports with an abnormal flag - to be referenced later in the script to create IsAbnormal flag
If object_id('tempdb..#abnormal') > 0
Drop table #abnormal
SELECT Inv.RequestIndex,
       Inv.ReportIndex,
       Result.Abnormal_Flag
INTO   #abnormal
FROM   WHREPORTING.ICE.FactReport Reports
       LEFT OUTER JOIN WHREPORTING.ICE.FactInvestigation Inv --have to join to FactInvestigation first as this is the link between the reports table and the results tables - where the abnormal marker is.
                    ON Reports.ReportIndex = Inv.ReportIndex
       LEFT OUTER JOIN ICE_DB.dbo.Serviceresult Result
                    ON Inv.InvestigationIndex = Result.Investigation_Index
WHERE  result.abnormal_flag <> 0
       AND Reports.ReportDate >= @startdate
       AND Reports.ReportDate <= @enddate
GROUP  BY Inv.RequestIndex,
          Inv.ReportIndex,
          Result.Abnormal_Flag 


Create nonclustered index IX_AbRepInx on #abnormal(ReportIndex)



--Temp table of all records within selected parameters - to be used in summary Select script below.
If object_id('tempdb..#reports') > 0
Drop table #reports

SELECT rep.ReportDate,
       rep.ReportTime,
       rep.ReportIndex,
       rep.RequestIndex,
       rep.FilingStatusKey,--this indicates unviewed 0, viewed 1, filed 2
       rep.Specialty,
       CASE
                  WHEN provider.ProviderDisciplineKey = '13' THEN '4'
                  ELSE provider.ProviderDisciplineKey
                END                    AS ProviderDisciplineKey,
                CASE
                  WHEN provider.ProviderDiscipline = 'BLOOD TRANSFUSION Wythenshawe' THEN 'Blood Transfusion'
                  ELSE provider.ProviderDiscipline
                END                    AS ProviderDiscipline,
       Provider.ProviderTypeKey,
       Provider.ProviderType,
       rep.ServiceProviderCode,
       rep.Latest,--Latest flag is logic used in toher ICE scripts - further to conversation with D Rose this may be because reports in ICE can be updated/appended to which can generate a new report index - this latest flag should help determine the most recent report for a request.  28/07/2014 CM: This logic proven to be incorrect after further investigations with DAhearn.  Logic has been updated to use the 'Category' field instead - see the where clause below.
        req.MasterClinicianCode, 
       rep.MasterPatientCode,
       cl.ClinicianSpecialtyCode,
       ISNULL(cl.ClinicianForename,'') + ' '  + ISNULL(cl.ClinicianSurname,'') as ClinicianSurname ,-- renamed this back to cliniciansurname as the query and report which already exists references this field name 
       ab.abnormal_flag,
       CASE    WHEN cl.ClinicianKey in ('293', '605','13099','507') then 'Scheduled Care' --as per A Allcut request
				WHEN cl.ClinicianKey  in ('593',--Dr Kulharni-- as per G Ryder request
										'30204',--Dr Bose
										'30205'--Dr Basu
										)
				 then 'Clinical Support' -- as per G Ryder request then 'Clinical Support' -- as per G Ryder request
                     WHEN cl.clinicianspecialtycode IN ( '242', '500' ) THEN 'Scheduled Care'
         ELSE sd.division
       END                        AS Division,--this is how division is determined in other ICE scripts.
       Isnull(( CASE
				   WHEN cl.ClinicianKey in ('293', '605','13099','507') then 'Women & Childrens' --as per A Allcut request
				WHEN cl.ClinicianKey  in ('593',--Dr Kulharni-- as per G Ryder request
										'30204',--Dr Bose
										'30205'--Dr Basu
				) then 'Disablement Services Centre' -- as per G Ryder request
                 WHEN sd.Direcorate = 'Cardio & Thoracic' THEN 'Cardiothoracic'
                  WHEN cl.clinicianspecialtycode IN ( '242', '500' ) THEN 'Women & Childrens'
                  WHEN cl.clinicianspecialtycode IN ( '172' ) THEN 'Cardiothoracic'
                  WHEN cl.clinicianspecialtycode IN ( '653' ) THEN 'Therapies'
                  ELSE sd.Direcorate
                END ), 'Unknown') AS Directorate,--this is the directorate logic used in other ICE scripts. 
       1 as TotalReports,
       Case when  rep.FilingStatusKey = 0 then 1 Else 0 End  as Unviewed,
       Case when  rep.FilingStatusKey = 1 then 1 Else 0 End  as ViewedAndUnfiled,
       Case when  rep.FilingStatusKey = 2 then 1 Else 0 End  as Filed
       
INTO   #reports
FROM   WHREPORTING.ICE.FactReport rep
       LEFT OUTER JOIN #abnormal ab
                    ON rep.reportindex = ab.reportindex
       LEFT OUTER JOIN #Ches
                    ON rep.ReportIndex = #ches.ReportIndex
       LEFT OUTER JOIN (SELECT DISTINCT ProviderSpecialtyCode,
                                        ProviderTypeKey,
                                        ProviderDisciplineKey,
                                        ProviderDiscipline,
                                        ProviderType
                        FROM   ICE.DimProvider) AS Provider--this will give the Provider of the requests i.e. pathology, radiology etc.
                    ON Provider.ProviderSpecialtyCode = rep.Specialty
       LEFT OUTER JOIN WHREPORTING.ICE.FactRequest req
                    ON rep.requestindex = req.RequestIndex
       LEFT OUTER JOIN WHREPORTING.ICE.DimClinician cl
                    ON req.MasterClinicianCode = cl.ClinicianKey
       LEFT OUTER JOIN lk.specialtydivision sd
                    ON sd.SpecialtyCode = Cast(( CASE
                                                   WHEN cl.ClinicianKey = 3460 THEN 370
                                                   ELSE cl.clinicianspecialtycode
                                                 END ) AS VARCHAR)
       LEFT OUTER JOIN WHREPORTING.ICE.DimLocation loc on rep.MasterLocationCode = loc.LocationKey
WHERE  rep.ReportDate >= @startdate
       AND rep.ReportDate <= @enddate
       --AND rep.Latest --28/07/2014 CM: Commented this out as this is now incorrect logic after investigations with DAhearn.  Correct logic for identifying valid latest reports is via the use of the Category field in the line below
       AND rep.category  is null -- Category 1 (Added), 2 (Updated), 3(Deleted) -- any reports which have an entry in this field should be excluded from the reporting as they will no longer be visible on front end and may have been replaced with a new report index int he back end.
      and loc.LocationTypeKey in ('1','2','3','6')
GROUP  BY rep.ReportDate,
          rep.ReportTime,
          rep.ReportIndex,
          rep.RequestIndex,
          rep.FilingStatusKey,
          rep.ServiceProviderCode,
          rep.Specialty,
          CASE
                  WHEN provider.ProviderDisciplineKey = '13' THEN '4'
                  ELSE provider.ProviderDisciplineKey
                END,
                CASE
                  WHEN provider.ProviderDiscipline = 'BLOOD TRANSFUSION Wythenshawe' THEN 'Blood Transfusion'
                  ELSE provider.ProviderDiscipline
                END,
          Provider.ProviderTypeKey,
          rep.Latest,
          Provider.ProviderType,
           req.MasterClinicianCode,
           rep.MasterPatientCode,
          cl.ClinicianSpecialtyCode,
           ISNULL(cl.ClinicianForename,'') + ' '  + ISNULL(cl.ClinicianSurname,''),
          sd.Direcorate,
          ab.abnormal_flag,
           CASE    WHEN cl.ClinicianKey in ('293', '605','13099','507') then 'Scheduled Care' --as per A Allcut request
				WHEN cl.ClinicianKey  in ('593',--Dr Kulharni-- as per G Ryder request
										'30204',--Dr Bose
										'30205'--Dr Basu
										)
				 then 'Clinical Support' -- as per G Ryder request
                     WHEN cl.clinicianspecialtycode IN ( '242', '500' ) THEN 'Scheduled Care'
         ELSE sd.division
       END  ,
           Isnull(( CASE
				   WHEN cl.ClinicianKey in ('293', '605','13099','507') then 'Women & Childrens' --as per A Allcut request
				WHEN cl.ClinicianKey  in ('593',--Dr Kulharni-- as per G Ryder request
										'30204',--Dr Bose
										'30205'--Dr Basu
										)
				THEN 'Disablement Services Centre' -- as per G Ryder request
                 WHEN sd.Direcorate = 'Cardio & Thoracic' THEN 'Cardiothoracic'
                  WHEN cl.clinicianspecialtycode IN ( '242', '500' ) THEN 'Women & Childrens'
                  WHEN cl.clinicianspecialtycode IN ( '172' ) THEN 'Cardiothoracic'
                  WHEN cl.clinicianspecialtycode IN ( '653' ) THEN 'Therapies'
                  ELSE sd.Direcorate
                END ), 'Unknown') 






--Final Select statement        
SELECT Isnull(Division, 'Unknown') AS Division,
       ProviderDisciplineKey,
       ProviderDiscipline,
       Directorate,
       ClinicianSpecialtyCode,
       Case when ProviderType IS NULL then 'Unknown' Else ProviderType End as ProviderType,
       Case when ProviderTypeKey = 0 then 10
			when ProviderTypeKey IS NULL then 11 
			Else ProviderTypeKey 
	   End as ProviderTypeKey, -- added this logic in for ProviderType Key as we want to use this for the ordering way provider types appear in the SSRS report as per Anne Marie Harris Request
       FilingStatusKey,
       clinicianSurname,
       CASE
         WHEN abnormal_flag = 1 THEN 'Abnormal'
         ELSE 'Normal'
       END                         AS Abnormal_Normal,
       SUM(TotalReports)                    AS NoOfReports,
       SUM(Unviewed)   as NoOfUnviewed,
       SUM(ViewedAndUnfiled) as NoOfUnfiled,
       SUM(Filed) as Filed
FROM   #reports a
WHERE   Case when ProviderType IS NULL then 'Unknown' Else ProviderType End NOT IN ('ICE','Unknown')--CM added 27/07/2015 - as requested by Anne Marie Harris 15/07/2015
		AND Directorate IN (SELECT Item
                       FROM   dbo.Split (@Directorate, ','))
       AND MasterClinicianCode IN (SELECT Item
                                   FROM   dbo.Split (@Clinician, ','))
                                   
GROUP  BY Isnull(Division, 'Unknown'),
          ProviderDisciplineKey,
          ProviderDiscipline,
          Directorate,
          ClinicianSpecialtyCode,
           Case when ProviderType IS NULL then 'Unknown' Else ProviderType End,
            Case when ProviderTypeKey = 0 then 10
			when ProviderTypeKey IS NULL then 11 
			Else ProviderTypeKey 
	   End,
          FilingStatusKey,
          clinicianSurname,
          CASE
            WHEN abnormal_flag = 1 THEN 'Abnormal'
            ELSE 'Normal'
          END 






END