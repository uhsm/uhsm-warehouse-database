﻿-- =============================================
-- Author:	CMan
-- Create date: 29/04/2014
-- Description:	Used in SSRS report  to give RRO prior ICE reports for patients

-- =============================================
CREATE PROCEDURE [ICE].[ReportICEReportsforRRO] @NHSNumber varchar(400), @HospNo varchar(400)

AS
BEGIN


SELECT [ReportDate]
      ,[ReportIndex]
      ,[RequestIndex]
      ,[RequestDate]
      ,ISNULL([HospitalNumber], 'unknown') as HospitalNumber
      ,[FacilID]
      ,ISNULL([NHSNumber], 'unknown') as NHSNumber
      ,[ICEHospitalNumber]
      ,[SetRequested]
      ,[Result_Code]
      ,[exam_description]
      ,[case_type]
      , [Comments] as Comments
      ,[OrderNumber]
      ,[PerformanceDate]
      ,Created
  FROM [WHREPORTING].[ICE].[ICEReportsforRRO]
  where substring(case_type, 1, CHARINDEX(';', Case_type)-1) in ('C','M','X')
  and ((ISNULL([NHSNumber], 'unknown') in (SELECT Item
                         FROM   dbo.Split (@NHSNumber, ',')) or ISNULL([NHSNumber], 'unknown') is NULL)
        or 
        (ISNULL([HospitalNumber], 'unknown')  in (SELECT Item
                         FROM   dbo.Split (@HospNo, ',')) or ISNULL([HospitalNumber], 'unknown') is NULL))


END