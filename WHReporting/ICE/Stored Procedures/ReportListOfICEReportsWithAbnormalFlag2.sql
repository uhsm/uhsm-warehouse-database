﻿-- =============================================
-- Author:		CMan
-- Create date: 19/02/2014
-- Description:	 Report to look at list of reports which have not been filed/not filed with the ability to filter on those with normal or abnormal reaults
--				Only includes reports requested unter the LocationTypes of Inpatient, Outpatient, A&E and ZebraOutpatient - 'Community' location type is not included
--				Report request by Deborah Rose after discussion on 18/02/2014 
-- =============================================
CREATE PROCEDURE [ICE].[ReportListOfICEReportsWithAbnormalFlag2] @startdate datetime, @enddate datetime, @LocType varchar(20), @Directorate varchar(400), @abnormal varchar(10), @Status varchar(20), @ProviderType varchar(20), @Loc varchar(Max), @ProviderDiscipline varchar(250)--,@cxr varchar(5)
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here



--Declare @startdate  as datetime, 
--@enddate as datetime,
--@abnormal as varchar(5),
--@Directorate as varchar(100),
--@Status as varchar(5),
--@ProviderType as varchar(5),
--@LocType as varchar(5)

--set @startdate = '20131201'
--set @enddate = '20131231'
--set @abnormal = 1
--set @Directorate = 'Cardiothoracic'
--set @Status = 1
--set @ProviderType = 1
--set @LocType = 2

--Create a temp table with a smaller reports dataset that we want to work with.
--Only includes reports with the date parameters of those selected within the report
--Also includes a criteria in the where clause which states where the latest is to be different from 0 --this was in the original logic of the ICE filing reports -? query with Deb Rose why?

If object_id ('tempdb..#reports') > 0
Drop table #reports
Create table #reports
(
ReportIndex  int,
       RequestIndex int,
       RequestDateTime datetime,
       MasterClinicianCode int,
       MasterPatientCode int,
       MasterLocationCode int ,
       FilingStatusKey int,
       UserFiledKey int,
       ReportDate date,
       ReportTime time,
       InpatientEpisodeUniqueID int,
       OutpatientAppointmentUniqueID int ,
       Specialty int,
       Latest bit,
       IsChestXray int,
       ServiceReportID varchar(25)
)

Insert into #reports
SELECT ReportIndex,
       RequestIndex,
       RequestDateTime,
       MasterClinicianCode,
       MasterPatientCode,
       MasterLocationCode,
       FilingStatusKey,
       UserFiledKey,
       ReportDate,
       ReportTime,
       InpatientEpisodeUniqueID,
       OutpatientAppointmentUniqueID,
       FactReport.Specialty,
       FactReport.Latest,
       case when dbo.Ufntransposeinvestigations(FactReport.ReportIndex)  like '%XCHES%' then 1 else 0 end  as IsChestXray
       ,ServiceReport.Service_Report_ID



FROM   WHREPORTING.ICE.FactReport
Left outer join ICE_DB.dbo.ServiceReport on FactReport.ReportIndex = ServiceReport.Service_Report_Index--join to this table to get the service report id which is the sample number on the front end
WHERE  ( FactReport.ReportDate >= @startdate )
       AND ( FactReport.ReportDate <= @enddate )
       --AND Latest <> 0 --28/07/2014 CM: Commented this out as this is now incorrect logic after investigations with DAhearn.  Correct logic for identifying valid latest reports is via the use of the Category field in the line below
       AND FactReport.Category is null -- Category 1 (Added), 2 (Updated), 3(Deleted) -- any reports which have an entry in this field should be excluded from the reporting as they will no longer be visible on front end and may have been replaced with a new report index int he back end.
       and FactReport.FilingStatusKey in (SELECT Item
                         FROM   dbo.Split (@Status, ','))



--Create temp table with all the records which have an abnormal flag  - to be used in the final query 
--Temp table created here as some reports may have both an abnormal flag and a normal flag as the abnormal flag is actually assigned to an abnormal result 
--however a report may contain both normal and abnormal results if more than one investigation was required in the original request - in the front end, it appears that any report that contains even one abnormal result is flagged as such 
--therefore a temp table is created here to get a distinct list of reports which have an abnormal flag.  This can then be used to determing normal and abnormal reports in the final query
If OBJECT_ID('tempdb..#abnormal') > 0
Drop table #abnormal
SELECT Inv.RequestIndex,
       Inv.ReportIndex,
       Result.Abnormal_Flag
INTO   #abnormal
FROM   WHREPORTING.ICE.FactReport Reports
       LEFT OUTER JOIN WHREPORTING.ICE.FactInvestigation Inv
                    ON Reports.ReportIndex = Inv.ReportIndex
       LEFT OUTER JOIN ICE_DB.dbo.Serviceresult Result
                    ON Inv.InvestigationIndex = Result.Investigation_Index
WHERE  result.abnormal_flag <> 0
       AND Reports.ReportDate >= @startdate
       AND Reports.ReportDate <= @enddate
GROUP  BY Inv.RequestIndex,
          Inv.ReportIndex,
          Result.Abnormal_Flag 



--create clinician specialty and directorate temp table to be used in final query
--Linking clinician table to specialty table so that the case statements can be defined here into a temp table to be used in the final query, which will hopefully help to query run faster 
If OBJECT_ID('tempdb..#RequestDoc') > 0
Drop table #RequestDoc

Create table #RequestDoc
(ClinicianKey varchar(50),
ClinicianSurname varchar(50),
ClinicianSpecialtyCode varchar(50),
Directorate varchar(100),
Specialty varchar(100),
)
Create nonclustered index IX_dir on #RequestDoc(Directorate)

Insert into #RequestDoc
SELECT distinct clin.ClinicianKey,
       ISNULL(Clin.ClinicianForename,'') + ' ' + ISNULL(clin.ClinicianSurname,'') as ClinicianSurname,
       clin.ClinicianSpecialtyCode,
       Isnull(( CASE
				WHEN clin.ClinicianKey in ('293', '605','13099','507') then 'Women & Childrens' --as per A Allcut request
				WHEN clin.ClinicianKey  in ('593',--Dr Kulharni-- as per G Ryder request
										'30204',--Dr Bose
										'30205'--Dr Basu
										) then 'Disablement Services Centre' -- as per G Ryder request
                 WHEN sd.Direcorate = 'Cardio & Thoracic' THEN 'Cardiothoracic'
                  WHEN clin.clinicianspecialtycode IN ( '242', '500' ) THEN 'Women & Childrens'
                  WHEN clin.clinicianspecialtycode IN ( '172' ) THEN 'Cardiothoracic'
                  WHEN clin.clinicianspecialtycode IN ( '653' ) THEN 'Therapies'
                  ELSE sd.Direcorate
                END ), 'Unknown')          AS Directorate,
       CASE WHEN clin.ClinicianKey  in ('593',--Dr Kulharni-- as per G Ryder request
										'30204',--Dr Bose
										'30205'--Dr Basu
										) then 'Disablement Services Centre' 
       WHEN clin.ClinicianKey in ('293', '605','13099','507') then 'Breast Radiology'
       Else Isnull(sd.MainSpecialty, 'Unknown') End  AS Specialty

FROM   WHReporting.ICE.DimClinician clin
       LEFT OUTER JOIN lk.specialtydivision sd
                    ON sd.SpecialtyCode = Cast(( CASE
                                                   WHEN clin.ClinicianKey = 3460 THEN 370
                                                   ELSE clin.clinicianspecialtycode
                                                 END ) AS VARCHAR) 



SELECT DISTINCT Report.ReportIndex,
                Report.RequestIndex,
                Isnull(Report.RequestDateTime, Request.DateTime_Of_Request)       AS RequestDateTime,
                Location.LocationKey											AS RequestingLocationKey,
                Location.LocationName                                             AS RequestingLocation,
                Location.LocationTypeKey                                          AS RequestingLocationTypeKey,
                Location.LocationType                                             AS RequestingLocationType,
                Report.MasterClinicianCode                                        AS RequestingClinicianCode,
                RequestDoc.ClinicianSurname                                       AS RequestingClinician,
                RequestDoc.Directorate                                            AS RequestingDirectorate,
                RequestDoc.Specialty                                              AS RequestingClinicianSpecialty,
                Report.MasterPatientCode                                          AS UniquePatientNo,
                Report.FilingStatusKey,
                FilingStatus.FilingStatus,
                Report.UserFiledKey,
                ServiceUser.UserDescription                                       AS FilingUserType,
                ServiceUser.UserName                                              AS FilingUserName,
                Report.ReportDate,
                Report.ReportTime,
                Provider.ProviderType,
                Provider.ProviderTypeKey,
                case when Provider.ProviderDiscipline = 'BLOOD TRANSFUSION Wythenshawe' then 'Blood Transfusion' else Provider.ProviderDiscipline END as ProviderDiscipline,
                CASE
                  WHEN Report.InpatientEpisodeUniqueID > 0 THEN
                    CASE
                      WHEN IP.EpisodeEndDate IS NULL THEN 'IP - Current'
                      ELSE 'IP - Discharged'
                    END
                  WHEN Report.OutpatientAppointmentUniqueID > 0 THEN 'OP'
                  ELSE ' N/A'
                END                                                               AS iPMKeyEvent,
                COALESCE (IP.EndWardCode, OP.ClinicCode, 'N/A')                   AS iPMServicePoint,
                COALESCE (IP.EpisodeEndDate, OP.AppointmentDate)                  AS iPMKeyDate,
                IP.AdmissionDate                                                  AS iPMIPAdmissionDate,
                COALESCE (IP.ConsultantName, OP.ProfessionalCarerName, ' N/A')    AS iPMProfessionalCarer,
                COALESCE (MPat.FacilID, Pat.ICEHospitalNumber)                    AS HospitalNumber,
                MPat.FacilID,
                Request.Hospital_Number as ICEPatNumber,
                MPat.Surname + ', ' + MPat.forename                               AS PatientName,
                srd.UserName                                                      AS RequestingUser,
                dbo.Ufntransposeinvestigations(Report.ReportIndex)                AS Investigatons,
                (SELECT DISTINCT ConsultantName
                 FROM   (SELECT DISTINCT ConsultantCode,
                                         ConsultantName
                         FROM   APC.Episode
                         WHERE  ( EpisodeStartDate >= @StartDate )
                                AND ( EpisodeStartDate <= @EndDate )
                         UNION
                         SELECT 'N/A'                  AS ConsultantCode,
                                'Consultant Not Known' AS ConsultantName) AS jsjsj
                 WHERE  ( ConsultantCode = COALESCE (IP.ConsultantCode, 'N/A') )) AS ResponsibleConsultant,
                CASE
                  WHEN #abnormal.Abnormal_Flag = 1 THEN #abnormal.Abnormal_Flag
                  ELSE 0
                END                                                               AS AbnormalFlag,
                IsChestXray,
                Report.ServiceReportID as SampleNo
FROM   #reports AS Report
       LEFT OUTER JOIN #RequestDoc AS RequestDoc WITH (nolock)
                    ON Report.MasterClinicianCode = RequestDoc.ClinicianKey
       LEFT OUTER JOIN ICE.DimServiceUser AS ServiceUser WITH (nolock)
                    ON Report.UserFiledKey = ServiceUser.UserKey
       LEFT OUTER JOIN ICE.DimLocation AS Location WITH (nolock)
                    ON Report.MasterLocationCode = Location.LocationKey
       LEFT OUTER JOIN APC.Episode AS IP WITH (nolock)
                    ON Report.InpatientEpisodeUniqueID = IP.EpisodeUniqueID
       LEFT OUTER JOIN OP.Schedule AS OP WITH (nolock)
                    ON Report.OutpatientAppointmentUniqueID = OP.SourceUniqueID
       LEFT OUTER JOIN ICE.Patient AS Pat WITH (nolock)
                    ON Report.MasterPatientCode = Pat.PatientKey
       LEFT OUTER JOIN ICE.MatchedPatients AS MPat
                    ON MPat.PatientKey = Pat.PatientKey
       LEFT OUTER JOIN (SELECT DISTINCT ProviderSpecialtyCode,
                                        ProviderTypeKey,
                                        ProviderDisciplineKey,
                                        ProviderDiscipline,
                                        ProviderType
                        FROM   ICE.DimProvider) AS Provider
                    ON Provider.ProviderSpecialtyCode = Report.Specialty
       LEFT OUTER JOIN ICE.DimFilingStatus AS FilingStatus WITH (nolock)
                    ON Report.FilingStatusKey = FilingStatus.FilingStatusKey
       LEFT OUTER JOIN ICE_DB.dbo.ServiceRequest AS Request
                    ON Report.RequestIndex = Request.Service_Request_Index
       LEFT OUTER JOIN #abnormal
                    ON Report.ReportIndex = #abnormal.ReportIndex
       LEFT OUTER JOIN ICE_DB.dbo.ServiceRequestDetail srd
                    ON Request.Service_Request_Index = srd.Request_Index

WHERE  case when Location.LocationTypeKey = 6 then 3 else Location.LocationTypeKey end  IN (SELECT Item
                         FROM   dbo.Split (@LocType, ',')) 
      and 
     RequestDoc.Directorate       IN (SELECT Item
                         FROM   dbo.Split (@Directorate, ','))  
       and 
      CASE
                  WHEN #abnormal.Abnormal_Flag = 1 THEN #abnormal.Abnormal_Flag
                  ELSE 0
                END in  (SELECT Item
                         FROM   dbo.Split (@abnormal, ','))        
	--AND Report.FilingStatusKey in (SELECT Item
 --                        FROM   dbo.Split (@Status, ','))
    AND isnull(provider.ProviderTypeKey,'7') in (SELECT Item
                         FROM   dbo.Split (@ProviderType, ','))
    --AND IsChestXray in (SELECT Item
    --                     FROM   dbo.Split (@cxr, ','))
    AND Location.LocationKey in (SELECT Item
                         FROM   dbo.Split (@Loc, ','))
    AND CASE
             WHEN provider.ProviderDisciplineKey = '13' THEN '4'
             ELSE provider.ProviderDisciplineKey
          END IN (SELECT Item
                   FROM   dbo.Split (@ProviderDiscipline, ','))
           
Drop table #reports
Drop table #abnormal
Drop table #RequestDoc


END