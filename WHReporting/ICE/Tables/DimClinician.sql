﻿CREATE TABLE [ICE].[DimClinician](
	[ClinicianRecno] [int] NOT NULL,
	[ClinicianKey] [int] NOT NULL,
	[ClinicianSurname] [varchar](35) NULL,
	[ClinicianForename] [varchar](35) NULL,
	[ClinicianNationalCode] [varchar](8) NULL,
	[ClinicianSpecialtyCode] [smallint] NULL,
	[ClinicianOrganisationID] [varchar](6) NULL,
	[IsGP] [int] NULL,
	[IsActive] [int] NULL,
	[DateAddedAddedToSystem] [datetime] NULL,
	[Created] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ClinicianKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]