﻿CREATE TABLE [ICE].[DimInvestigationRequested](
	[InvestigationKey] [int] NOT NULL,
	[InvestigationCode] [varchar](10) NULL,
	[InvestigationRequested] [varchar](60) NULL,
	[Created] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[InvestigationKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]