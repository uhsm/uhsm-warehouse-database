﻿CREATE TABLE [ICE].[DimLocation](
	[LocationRecno] [int] NOT NULL,
	[LocationKey] [int] NOT NULL,
	[LocationName] [varchar](35) NULL,
	[IsGPPractice] [bit] NULL,
	[LocationTypeKey] [int] NULL,
	[LocationType] [varchar](50) NULL,
	[DateAdded] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[LocationKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]