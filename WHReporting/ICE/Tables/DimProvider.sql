﻿CREATE TABLE [ICE].[DimProvider](
	[ProviderRecno] [int] NOT NULL,
	[ProviderKey] [int] NOT NULL,
	[ProviderShortCode] [varchar](8) NULL,
	[ProviderName] [varchar](65) NULL,
	[ProviderDisciplineKey] [int] NULL,
	[ProviderDiscipline] [varchar](50) NULL,
	[ProviderSpecialtyCode] [varchar](5) NULL,
	[ProviderTypeKey] [int] NULL,
	[ProviderType] [varchar](35) NULL,
PRIMARY KEY CLUSTERED 
(
	[ProviderKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]