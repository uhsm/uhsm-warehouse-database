﻿CREATE TABLE [ICE].[DimRequestStatus](
	[RequestStatusKey] [int] IDENTITY(1,1) NOT NULL,
	[RequestStatusICECode] [varchar](3) NOT NULL,
	[RequestStatus] [varchar](255) NULL
) ON [PRIMARY]