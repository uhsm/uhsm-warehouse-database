﻿CREATE TABLE [ICE].[DimServiceUser](
	[UserRecno] [int] NOT NULL,
	[UserKey] [int] NOT NULL,
	[UserName] [varchar](35) NULL,
	[UserInitials] [varchar](10) NULL,
	[UserDescription] [varchar](50) NULL,
	[UserTypeKey] [tinyint] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]