﻿CREATE TABLE [ICE].[FactInvestigation](
	[InvestigationRecno] [int] NOT NULL,
	[InvestigationIndex] [int] NOT NULL,
	[InvestigationDescriptionKey] [int] NOT NULL,
	[DateInvestigationAddedKey] [int] NOT NULL,
	[DateInvestigationAdded] [date] NULL,
	[DateTimeInvestigationAdded] [datetime] NULL,
	[SampleIndex] [int] NULL,
	[ReportIndex] [int] NULL,
	[RequestIndex] [int] NULL,
	[Created] [datetime] NULL,
	[Cases] [int] NULL,
 CONSTRAINT [pk_FactInvestigation] PRIMARY KEY CLUSTERED 
(
	[InvestigationIndex] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]