﻿CREATE TABLE [ICE].[FactRequest](
	[RequestRecno] [int] NOT NULL,
	[RequestIndex] [int] NOT NULL,
	[RequestDateKey] [int] NULL,
	[RequestDate] [date] NULL,
	[RequestTime] [time](0) NULL,
	[MasterLocationCode] [int] NULL,
	[MasterClinicianCode] [int] NULL,
	[ServiceProviderCode] [int] NULL,
	[MasterPatientCode] [int] NULL,
	[Created] [datetime] NOT NULL,
	[RequestStatusCode] [int] NULL,
	[UserRequestCode] [int] NULL,
	[PriorityCode] [int] NULL,
	[Category] [varchar](30) NULL,
	[Cases] [int] NULL,
	[SetRequested] [nvarchar](200) NULL,
 CONSTRAINT [pk_FactRequest] PRIMARY KEY CLUSTERED 
(
	[RequestIndex] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF__FactReque__Cases__6DD739FB]    Script Date: 07/04/2016 10:14:37 ******/
ALTER TABLE [ICE].[FactRequest] ADD  DEFAULT ((1)) FOR [Cases]