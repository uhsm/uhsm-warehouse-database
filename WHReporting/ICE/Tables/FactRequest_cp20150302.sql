﻿CREATE TABLE [ICE].[FactRequest_cp20150302](
	[RequestRecno] [int] NOT NULL,
	[RequestIndex] [int] NOT NULL,
	[RequestDateKey] [int] NULL,
	[RequestDate] [date] NULL,
	[RequestTime] [time](0) NULL,
	[MasterLocationCode] [int] NULL,
	[MasterClinicianCode] [int] NULL,
	[ServiceProviderCode] [int] NULL,
	[MasterPatientCode] [int] NULL,
	[Created] [datetime] NOT NULL,
	[RequestStatusCode] [int] NULL,
	[UserRequestCode] [int] NULL,
	[PriorityCode] [int] NULL,
	[Category] [varchar](30) NULL,
	[Cases] [int] NULL,
	[SetRequested] [nvarchar](200) NULL
) ON [PRIMARY]