﻿CREATE TABLE [ICE].[FactResults](
	[Investigation_Index] [int] NOT NULL,
	[Sample_Index] [int] NOT NULL,
	[Result_Index] [int] NOT NULL,
	[Date_Added] [datetime] NULL,
	[Created] [datetime] NULL,
	[Result_Type] [varchar](3) NULL,
	[Result_Code] [varchar](10) NULL,
	[Result] [varchar](70) NULL,
 CONSTRAINT [PK_FactResults] PRIMARY KEY CLUSTERED 
(
	[Result_Index] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]