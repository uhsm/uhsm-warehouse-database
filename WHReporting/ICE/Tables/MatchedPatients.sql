﻿CREATE TABLE [ICE].[MatchedPatients](
	[PatientKey] [int] NULL,
	[Surname] [varchar](35) NULL,
	[forename] [varchar](35) NULL,
	[NHSNumber] [varchar](10) NULL,
	[ICEHospitalNumber] [varchar](30) NULL,
	[FacilID] [varchar](20) NULL
) ON [PRIMARY]