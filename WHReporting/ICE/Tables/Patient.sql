﻿CREATE TABLE [ICE].[Patient](
	[PatientRecno] [int] NOT NULL,
	[PatientKey] [int] NULL,
	[Forename] [varchar](35) NULL,
	[MiddleName] [varchar](35) NULL,
	[Surname] [varchar](35) NULL,
	[AlternativeName] [varchar](70) NULL,
	[PreviousName] [varchar](70) NULL,
	[FacilID] [varchar](20) NULL,
	[NHSNumber] [varchar](10) NULL,
	[ICEHospitalNumber] [varchar](30) NULL,
	[DateOfBirth] [datetime] NULL,
	[DeceasedFlag] [bit] NULL,
	[Created] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[PatientRecno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]