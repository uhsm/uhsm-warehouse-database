﻿CREATE VIEW [ICE].[vwICEDirectorateReportDetailsbyLocation]

As

Select distinct
	case when 
Location.LocationName in ('CT TX UNIT','CTCCU TRANSPLANT') THEN 'CTCCU' ELSE Location.LocationName END AS LocationName
	,Location.LocationType as PatientType
	,ProviderTypeCode = Report.ServiceProviderCode
	,CASE WHEN ProviderTypeKey = 2 THEN 'Radiology' WHEN ProviderTypeKey = 1 AND 
                    ProviderDisciplineKey = 2 THEN 'Histopathology' WHEN ProviderTypeKey = 1 AND 
                    ProviderDisciplineKey <> 2 THEN 'Pathology' ELSE 'Other' END AS Dept 
	,Clinician.ClinicianSurname
	,report.reportindex
	,Report.MasterClinicianCode
	,FileStatus.FilingStatus
	,NotViewed = CAse When FileStatus.FilingStatusKey = 0 Then 1 Else 0 End 
	,NotFiled = CAse When FileStatus.FilingStatusKey = 1 Then 1 Else 0 End
	,Filed = CAse When FileStatus.FilingStatusKey = 2 Then 1 Else 0 End
	,Total = 1
	,provider.ProviderDisciplineKey
	,provider.ProviderDiscipline
	,MONTH(cal.TheDate) AS MonthNumber, 
    left(cal.TheMonth,3)+'-'+RIGHT(cal.TheMonth,2) as TheMonth, 
    cal.FinancialMonthKey
from ICE.FactReport Report
left outer join ICE.DimFilingStatus FileStatus
on Report.FilingStatusKey = FileStatus.FilingStatusKey
Left Outer Join ICE.DimLocation Location
	on Report.MasterLocationCode = Location.LocationKey
Left OUter Join ICE.DimClinician Clinician
	On Report.MasterClinicianCode = Clinician.ClinicianKey
Left Outer Join ICE.DimProvider Provider
	On Report.Specialty = Provider.ProviderSpecialtyCode
INNER JOIN LK.Calendar cal ON cal.TheDate = Report.ReportDate
left outer join lk.specialtydivision sd on sd.mainspecialtycode=cast(clinician.clinicianspecialtycode as varchar) 
Where 
 cal.FinancialMonthKey IN
               (select distinct top 12 d.financialmonthkey
      from lk.calendar as d
      
      where

       d.FinancialMonthKey<
      
      (select e.FinancialMonthKey from LK.Calendar e
      where e.TheDate=CAST(GETDATE() as DATE))
      order by d.FinancialMonthKey desc
      
      )
and report.MasterLocationCode IN ('50','51','71','7587')
and provider.ProviderTypeKey IN ('1','2')
--and provider.ProviderDisciplineKey in ('1','2','7')
and report.latest =1
--and LocationTypeKey in ('1','2','3','6')

group by report.reportindex
 ,case when 
Location.LocationName in ('CT TX UNIT','CTCCU TRANSPLANT') THEN 'CTCCU' ELSE Location.LocationName END
	,Location.LocationType
	,Report.ServiceProviderCode
	,Provider.ProviderType
	,CASE WHEN ProviderTypeKey = 2 THEN 'Radiology' WHEN ProviderTypeKey = 1 AND 
                    ProviderDisciplineKey = 2 THEN 'Histopathology' WHEN ProviderTypeKey = 1 AND 
                    ProviderDisciplineKey <> 2 THEN 'Pathology' ELSE 'Other' END
	,Clinician.ClinicianSurname
	,Report.MasterClinicianCode
	,FileStatus.FilingStatus
	,CAse When FileStatus.FilingStatusKey = 0 Then 1 Else 0 End 
	,CAse When FileStatus.FilingStatusKey = 1 Then 1 Else 0 End
	,CAse When FileStatus.FilingStatusKey = 2 Then 1 Else 0 End
	,provider.ProviderDisciplineKey
	,provider.ProviderDiscipline
	,MONTH(cal.TheDate) 
    ,left(cal.TheMonth,3)+'-'+RIGHT(cal.TheMonth,2) 
 ,cal.FinancialMonthKey