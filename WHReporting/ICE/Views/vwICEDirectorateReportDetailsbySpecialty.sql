﻿CREATE VIEW [ICE].[vwICEDirectorateReportDetailsbySpecialty]
AS
SELECT DISTINCT 
case when clinician.clinicianspecialtycode in ('500') then 'Obstetrics&Gynaecology' else sd.SpecialtyFunction end as OPSpecialty,
IP.[Specialty(Function)] as IPSpecialty,
case when 
sd.Direcorate ='Cardio & Thoracic' then 'Cardiothoracic'
when clinician.clinicianspecialtycode in ('242')

then 'Womens & Childrens' 
when clinician.clinicianspecialtycode in ('172','500') then 'Cardiothoracic'
when clinician.clinicianspecialtycode in ('653') then 'Therapies'
else sd.Direcorate end as OPDirectorate,
	MONTH(cal.TheDate) AS MonthNumber, 
    left(cal.TheMonth,3)+'-'+RIGHT(cal.TheMonth,2) as TheMonth, 
    cal.FinancialMonthKey,
    Provider.ProviderTypeKey, 
    CASE WHEN ProviderTypeKey = 2 THEN 'Radiology' WHEN ProviderTypeKey = 1 AND 
                    ProviderDisciplineKey = 2 THEN 'Histopathology' WHEN ProviderTypeKey = 1 AND 
                    ProviderDisciplineKey <> 2 THEN 'Pathology' ELSE 'Other' END AS Dept, 
    case when IP.Directorate='Cardio & Thoracic' then 'Cardiothoracic' else IP.Directorate end AS IPDirectorate, 
    case when IP.Directorate is not null
    then 'Inpatient' 
    when IP.Directorate is null and LocationTypeKey IN (3, 6)
    then 'Outpatient' else 'Other' end as PatientType ,
    
    
    
    
    
    /*case when LocationTypeKey IN (3, 6) then 'Outpatient' 
					WHEN LocationTypeKey IN (1, 2) then 'Inpatient' 
					else 'Other' end as PatientType,*/
    CASE WHEN LocationTypeKey IN (3, 6) 
                    THEN 
                    (case when sd.SpecialtyFunctionCode='191' then '190' 
                    else sd.SpecialtyFunctionCode end)
         ELSE NULL END AS OPSpecialtyFunction, 
    CASE WHEN LocationTypeKey IN (1,2) 
                    THEN 
                    (case when IP.[SpecialtyCode(Function)]='191' then '190' else IP.[SpecialtyCode(Function)]end)
                    ELSE NULL END AS IPSpecialtyFunction, 
    Location.LocationName, 
    Report.MasterLocationCode AS LocationCode, 
    Report.ServiceProviderCode AS ProviderTypeCode, 
    Provider.ProviderType, 
    Clinician.ClinicianSurname, 
    Report.ReportIndex, 
    Report.MasterClinicianCode, 
    FileStatus.FilingStatus, 
    CASE WHEN FileStatus.FilingStatusKey = 0 THEN 1 ELSE 0 END AS NotViewed, 
    CASE WHEN FileStatus.FilingStatusKey = 1  THEN 1 ELSE 0 END AS NotFiled, 
    CASE WHEN FileStatus.FilingStatusKey = 2 THEN 1 ELSE 0 END AS Filed,
    Provider.ProviderDiscipline, Provider.ProviderDisciplineKey, 
    1 AS Total, 
    COALESCE (IP.ConsultantName, 'Consultant Not Known') AS ResponsibleConsultantIP, 
    COALESCE (IP.ConsultantCode, 'N/A') AS ResponsibleConsultantCodeIP
FROM ICE.FactReport AS Report LEFT OUTER JOIN
    ICE.DimFilingStatus AS FileStatus ON Report.FilingStatusKey = FileStatus.FilingStatusKey LEFT OUTER JOIN
    ICE.DimLocation AS Location ON Report.MasterLocationCode = Location.LocationKey LEFT OUTER JOIN
    ICE.DimClinician AS Clinician ON Report.MasterClinicianCode = Clinician.ClinicianKey LEFT OUTER JOIN
		(SELECT DISTINCT ProviderSpecialtyCode,
                                        ProviderTypeKey,
                                        case when ProviderDisciplineKey = '13' then '4' else ProviderDisciplineKey end as ProviderDisciplineKey,
                                        ProviderType,
                                        case when ProviderDiscipline = 'BLOOD TRANSFUSION Wythenshawe' then 'Blood Transfusion' else ProviderDiscipline end as ProviderDiscipline
                     FROM   ICE.DimProvider)  AS Provider 
								  ON Report.Specialty = Provider.ProviderSpecialtyCode LEFT OUTER JOIN
     APC.Episode AS IP WITH (nolock) ON Report.InpatientEpisodeUniqueID = IP.EpisodeUniqueID INNER JOIN
     LK.Calendar AS cal ON cal.TheDate = Report.ReportDate left outer join lk.specialtydivision sd on sd.SpecialtyCode=cast(
(case when cliniciankey=3460 then 370
 else
clinician.clinicianspecialtycode end)

 as varchar)  --02/12/2013 changed the field used in the join from sd.SpecialtyFunctionCode to sd.SpecialtyCode as using the specialtyfunctioncode resulted in duplicates due to there being more than one description for some specialtyfunctioncodes 
WHERE cal.FinancialMonthKey IN
               (select distinct top 12 financialmonthkey
      from lk.calendar
      
      where

       FinancialMonthKey<
      
      (select FinancialMonthKey from LK.Calendar
      where TheDate=CAST(GETDATE() as DATE)
      
      
      )
      
     order by FinancialMonthKey desc)
        AND (Provider.ProviderTypeKey IN (1, 2)) 
        AND (Report.Latest = 1) 
        --AND (Location.LocationTypeKey IN (1,2,3,6))
        and 
        
        
        isnull((case when coalesce((case when LocationTypeKey IN (3, 6) then 
        
        
        
        (case when 
sd.Direcorate ='Cardio & Thoracic' then 'Cardiothoracic'
when clinician.clinicianspecialtycode in ('242')

then 'Womens & Childrens' 
when clinician.clinicianspecialtycode in ('172','500') then 'Cardiothoracic'
when clinician.clinicianspecialtycode in ('653') then 'Therapies'
else sd.Direcorate end)
        
        
        
        
         else null end),
        ip.directorate
        ) ='Cardio & Thoracic' then 'Cardiothoracic' else coalesce((case when LocationTypeKey IN (3, 6) then sd.Direcorate else null end),
        ip.directorate
        ) end),'Unknown')
        not in 
        ('-1','Trust',
         'Not UHSM',
         'Audiology',
         'Data quality',
         'Unknown'
        ,'Therapies')

 --and Report.ReportRecno = '16037201'