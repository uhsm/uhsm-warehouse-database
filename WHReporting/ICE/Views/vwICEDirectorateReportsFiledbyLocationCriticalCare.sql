﻿CREATE VIEW [ICE].[vwICEDirectorateReportsFiledbyLocationCriticalCare]
AS
SELECT     LocationName+Dept+'Reports' as DirKey,FinancialMonthKey,TheMonth, 
Actual=sum(case when themonth in ('Jan-12', 'Feb-12', 'Mar-12') then 0 else Total end)
FROM         ICE.vwICEDirectorateReportDetailsbyLocation
GROUP BY  LocationName+Dept+'Reports',FinancialMonthKey,TheMonth
UNION
SELECT      LocationName+Dept+'Filed' as DirKey,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12', 'Feb-12', 'Mar-12') then 0 else Filed end)
FROM        ICE.vwICEDirectorateReportDetailsbyLocation
GROUP BY   LocationName+Dept+'Filed' ,FinancialMonthKey,TheMonth
UNION
SELECT      LocationName+Dept+'NotFiled' as DirKey,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12', 'Feb-12', 'Mar-12') then 0 else NotFiled end)
FROM       ICE.vwICEDirectorateReportDetailsbyLocation
GROUP BY   LocationName+Dept+'NotFiled' ,FinancialMonthKey,TheMonth
UNION
SELECT      LocationName+Dept+'NotViewed' as DirKey,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12', 'Feb-12', 'Mar-12') then 0 else NotViewed end)
FROM       ICE.vwICEDirectorateReportDetailsbyLocation
GROUP BY   LocationName+Dept+'NotViewed' ,FinancialMonthKey,TheMonth