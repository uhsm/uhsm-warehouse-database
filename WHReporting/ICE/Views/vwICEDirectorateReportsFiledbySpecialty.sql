﻿CREATE VIEW [ICE].[vwICEDirectorateReportsFiledbySpecialty]
AS
SELECT     IPDirectorate+ipSpecialty+PatientType+Dept+'Reports' as DirKey,FinancialMonthKey,TheMonth, 
Actual=sum(case when themonth in ('Jan-12', 'Feb-12', 'Mar-12') then 0 else Total end)
FROM         ice.vwICEDirectorateReportDetailsbySpecialty
WHERE     patienttype = 'Inpatient' 
GROUP BY  IPDirectorate+ipSpecialty+PatientType+Dept+'Reports',FinancialMonthKey,TheMonth
UNION
SELECT      IPDirectorate+ipSpecialty+PatientType+Dept+'Filed' as DirKey,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12', 'Feb-12', 'Mar-12') then 0 else Filed end)
FROM         ice.vwICEDirectorateReportDetailsbySpecialty
WHERE     patienttype = 'Inpatient'
GROUP BY  IPDirectorate+ipSpecialty+PatientType+Dept+'Filed' ,FinancialMonthKey,TheMonth
UNION
SELECT      IPDirectorate+ipSpecialty+PatientType+Dept+'NotFiled' as DirKey,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12', 'Feb-12', 'Mar-12') then 0 else NotFiled end)
FROM         ice.vwICEDirectorateReportDetailsbySpecialty
WHERE     patienttype = 'Inpatient' 
GROUP BY  IPDirectorate+ipSpecialty+PatientType+Dept+'NotFiled' ,FinancialMonthKey,TheMonth
UNION
SELECT     IPDirectorate+ipSpecialty+PatientType+Dept+'NotViewed' as DirKey,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12', 'Feb-12', 'Mar-12') then 0 else NotViewed end)
FROM         ice.vwICEDirectorateReportDetailsbySpecialty
WHERE     patienttype = 'Inpatient' 
GROUP BY  IPDirectorate+ipSpecialty+PatientType+Dept+'NotViewed' ,FinancialMonthKey,TheMonth
UNION
SELECT     OPDirectorate+opSpecialty+PatientType+Dept+'Reports' as DirKey,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12','Feb-12','Mar-12','Apr-12', 'May-12', 'Jun-12', 'Jul-12', 'Aug-12', 'Sep-12', 'Oct-12') then 0 else Total end)
FROM         ice.vwICEDirectorateReportDetailsbySpecialty
WHERE     patienttype = 'Outpatient' 
GROUP BY   OPDirectorate+opSpecialty+PatientType+Dept+'Reports' ,FinancialMonthKey,TheMonth
UNION
SELECT       OPDirectorate+opSpecialty+PatientType+Dept+'Filed' as DirKey,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12','Feb-12','Mar-12','Apr-12', 'May-12', 'Jun-12', 'Jul-12', 'Aug-12', 'Sep-12', 'Oct-12') then 0 else Filed end)
FROM         ice.vwICEDirectorateReportDetailsbySpecialty
WHERE     patienttype = 'Outpatient' 
GROUP BY   OPDirectorate+opSpecialty+PatientType+Dept+'Filed' ,FinancialMonthKey,TheMonth
UNION
SELECT       OPDirectorate+opSpecialty+PatientType+Dept+'NotFiled' as DirKey,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12','Feb-12','Mar-12','Apr-12', 'May-12', 'Jun-12', 'Jul-12', 'Aug-12', 'Sep-12', 'Oct-12') then 0 else NotFiled end)
FROM         ice.vwICEDirectorateReportDetailsbySpecialty
WHERE     patienttype = 'Outpatient' 
GROUP BY   OPDirectorate+opSpecialty+PatientType+Dept+'NotFiled' ,FinancialMonthKey,TheMonth
UNION
SELECT       OPDirectorate+opSpecialty+PatientType+Dept+'NotViewed' as DirKey,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12','Feb-12','Mar-12','Apr-12', 'May-12', 'Jun-12', 'Jul-12', 'Aug-12', 'Sep-12', 'Oct-12') then 0 else NotViewed end)
FROM         ice.vwICEDirectorateReportDetailsbySpecialty
WHERE     patienttype = 'Outpatient'
GROUP BY   OPDirectorate+opSpecialty+PatientType+Dept+'NotViewed' ,FinancialMonthKey,TheMonth