﻿-- =============================================
-- Author:		CMan
-- Create date: 17/06/2014
-- Description:	Stored procedure to build a working table in WHReporting 'IDS.AEDischargeSummaries' with the IDS data linked to AE.Encounter
-- =============================================
CREATE PROCEDURE [IDS].[BuildAEIDSReportingTable]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

----Temp table 
If OBJECT_ID ('WHReporting.IDS.AEDischargeSummaries')> 0
Drop table WHReporting.IDS.AEDischargeSummaries

Create table WHReporting.IDS.AEDischargeSummaries
(Monthstart Datetime,
SourceUniqueID  varchar(20),
IDSId int   null,
IDS_Doc_status_code int null,
EDTHub_DocId int null,
EDTStatus nvarchar(300),
LastUpdate datetime,
IDSComplete nvarchar(5),
FaciltyID varchar(20),
AdmissionDateTime datetime,
DischargeDateTime datetime,
IDS_Practice_code varchar(20),
Spell_Practice_code varchar(20),
DischargeDestination varchar(100),
Age int,
ExcludeFromIDSReporting bit,
ExclusionReason varchar(100),
EDTHub_RejectType int null,
EDTHub_RejectReason varchar(250),
EDTHub_status_LastUpdated datetime,
InsertDateTime datetime,
DataLastLoaded datetime
)




Insert into  WHReporting.IDS.AEDischargeSummaries
Select cal.FirstDateOfMonth as monthstart,
		enc.SourceUniqueID,
		EDAS.ID IDSId , 
EDAS.Doc_status,
EDAS.EDTHub_DocId,
EDAS.EDT_status,
EDAS.Last_Update,
EDAS.IDS_complete,
enc.LocalPatientID, 
enc.ArrivalTime,
enc.AttendanceConclusionTime,
EDAS.Practice_code,
enc.RegisteredGpPracticeCode,
enc.DischargeDestinationCode,
enc.AgeOnArrival,
'0' as ExcludeFromIDSReporting,
NULL as ExclusionReason,
EDAS.EDTHub_RejectType,
EDAS.EDTHub_RejectReason,
EDAS.EDTHub_status_LastUpdated,
EDAS.InsertDateTime,
GETDATE() as DataLastLoaded
from WH.AE.Encounter enc
Left outer join 
(
SELECT ID
	,Doc_Status
	,EDTHub_DocId
	,EDT_status
	,Last_Update
	,IDS_complete
	,Practice_code
	,EDTHub_RejectType
	,EDTHub_RejectReason
	,EDTHub_Status_LastUpdated
	,External_Spell
	,InsertDateTime
FROM WH.IDS.ECCDischargeSummaries
WHERE Report_Code = 'EDAS'
) EDAS 
on enc.SourceUniqueID  COLLATE Latin1_General_CI_AS  = EDAS.external_spell
left outer join WHREPORTING.LK.Calendar cal on  enc.ArrivalDate = cal.TheDate
where ArrivalDate >= '20150301'
Order by ArrivalTime


Select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 'Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'IDS - WHREPORTING BuildAEIDSReportingTable', @Stats, @StartTime

END