﻿-- =============================================
-- Author:		CMan
-- Create date: 17/06/2014
-- Description:	Store procedure to build a working table in WHReporting 'IDS.DischargeSummaries' with the IDS data along with flags to indicate whether or not the record should be include in the the IDS count
-- =============================================
CREATE PROCEDURE [IDS].[BuildIDSReportingTable]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

--Temp table 
If OBJECT_ID ('WHReporting.IDS.DischargeSummaries')> 0
Drop table WHReporting.IDS.DischargeSummaries

Create table WHReporting.IDS.DischargeSummaries
(Monthstart Datetime,
SourceSpellNo  varchar(10),
IDSId int   null,
IDS_Doc_status_code int null,
EDTHub_DocId int null,
EDTStatus nvarchar(300),
LastUpdate datetime,
IDSComplete nvarchar(5),
FaciltyID varchar(20),
AdmissionDateTime datetime,
DischargeDateTime datetime,
IDS_Practice_code varchar(20),
Spell_Practice_code varchar(20),
DischargeWard varchar(50),
DischargeDestination varchar(100),
Age int,
ExcludeFromIDSReporting bit,
ExclusionReason varchar(100),
EDTHub_RejectType int null,
EDTHub_RejectReason varchar(250),
EDTHub_status_LastUpdated datetime,
AdmissionMethod varchar(20),
InsertDateTime datetime, 
DataLastLoaded datetime
)




Insert into  WHReporting.IDS.DischargeSummaries
Select FirstDateOfMonth as monthstart,
SourceSpellNo,
IDS.ID IDSId , 
IDS.Doc_status,
IDS.EDTHub_DocId,
IDS.EDT_status,
Last_Update,
IDS.IDS_complete,
sp.FaciltyID, 
sp.AdmissionDateTime,
sp.DischargeDateTime,
IDS.Practice_code,
Sp.PracticeCode,
Sp.DischargeWard, 
SP.DischargeDestination,
sp.Age,
'0' as ExcludeFromIDSReporting,
NULL as ExclusionReason,
IDS.EDTHub_RejectType,
IDS.EDTHub_RejectReason,
IDS.EDTHub_status_LastUpdated,
Sp.AdmissionMethodNHSCode,
IDS.InsertDateTime,
GETDATE() as DataLastLoaded
 
from WHREPORTING.APC.Spell sp
left outer join (SELECT ID
	,Doc_Status
	,EDTHub_DocId
	,EDT_status
	,Last_Update
	,IDS_complete
	,Practice_code
	,EDTHub_RejectType
	,EDTHub_RejectReason
	,EDTHub_Status_LastUpdated
	,CASE 
		WHEN external_spell LIKE '%[a-z]%'
			THEN NULL
		ELSE external_spell
		END AS External_Spell
	 ,InsertDateTime 
FROM WH.IDS.ECCDischargeSummaries
WHERE Report_Code = 'IDS'
) IDS 
on sp.SourceSpellNo = IDS.external_spell
left outer join WHREPORTING.LK.Calendar cal on  sp.DischargeDate = cal.TheDate

where DischargeDateTime >='20131001'
--and DischargeDateTime < '20140301'


/*-----------------------------------------------
--Exclusion Updates:
Exclusions from being counted in IDS reporting in JAC as per rules detailed by Geoff Courner
-------------------------------------------------*/


--Patient Died:  no IDS are created for patients who are discharged as dead 
Update   WHReporting.IDS.DischargeSummaries

Set ExcludeFromIDSReporting = 1,
ExclusionReason = 'Patient Discharged as Died'
where DischargeDestination  = 'N/A Patient Died'




--Discharge to Other Hospitals
Update   WHReporting.IDS.DischargeSummaries
Set ExcludeFromIDSReporting = 1,
ExclusionReason = 'Patient Discharged to Other Hospital'
where  DischargeDestination in 
('NHS hosp. - Ward neonates/ maternity','NHS hosp. - Ward mentally ill / learning difficulties','NHS hosp. - long term medium secure','NHS hosp. - ward for general patients','NHS hosp. - high security psychiatric accommodation','Non-NHS run hospital','Non-NHS run hosp. - medium secure unit','NHS hosp. - medium secure unit')





--wards Exclusions as per GC's list:

--activity at external hospitals
Update   WHReporting.IDS.DischargeSummaries

Set ExcludeFromIDSReporting = 1,
ExclusionReason = 'Activity at External Hospitals'
where dischargeward in ('WARD AT ALEX',--Alex
'WARD AT ALEX PCT', --Alex PCT
'ALEX HDU',--Alex
'ALEX ICU',--Alex
'WARD AT BUPA',--BUP
'BWH - Bridgewater Hospital',--BWH
'Wellington Pressure Unit',--WPU
'Ward at Oaklands',--Oakland
'WARD AT TRAFFORD',--Traff
'Regency Hospital Macclesfield MAXFAX') --Regency Hospital Macclesfield MAXFAX --added 29/07/2014 as per GC email 


--Sleep Lab
Update   WHReporting.IDS.DischargeSummaries

Set ExcludeFromIDSReporting = 1,
ExclusionReason = 'Sleep Lab-IDS from PAS letter'
where dischargeward in ('SLEEP SERVICES')--DU Sleep Lab


--WCH Urology video DC
Update   WHReporting.IDS.DischargeSummaries

Set ExcludeFromIDSReporting = 1,
ExclusionReason = 'WCH Urology video DC-IDS from PAS letter'
where dischargeward in ('WCH UROL VIDEO D/C')--AUR WCH Urology video DC



--BRONCHOSCOPY UNIT
Update   WHReporting.IDS.DischargeSummaries

Set ExcludeFromIDSReporting = 1,
ExclusionReason = 'BRONCHOSCOPY UNIT -IDS from PAS letter'
where dischargeward in ('BRONCHOSCOPY UNIT')--BRO





--Lithotripsy
Update   WHReporting.IDS.DischargeSummaries

Set ExcludeFromIDSReporting = 1,
ExclusionReason = 'Lithotripsy -IDS from PAS letter'
where dischargeward in ('LITHOTRIPTOR UNIT')--LIT Lithotripsy



--Nightingale
Update   WHReporting.IDS.DischargeSummaries

Set ExcludeFromIDSReporting = 1,
ExclusionReason = 'Nightingale -IDS from PAS letter'
where dischargeward in ('NIGHTINGALE','NGALE' )--Nightingale



--Baguley Suite
Update   WHReporting.IDS.DischargeSummaries

Set ExcludeFromIDSReporting = 1,
ExclusionReason = 'Baguley Suite - paper based system'
where dischargeward in ('BAGULEY SUITE')--BS 


--Endoscopy in TDC
Update   WHReporting.IDS.DischargeSummaries

Set ExcludeFromIDSReporting = 1,
ExclusionReason = 'Endoscopy in TDC - uses unisoft to produce summary of procedures'
where dischargeward in ('GI Unit')--GIU Endoscopy in TDC



--Women's Health Suite - Colposcopy
Update   WHReporting.IDS.DischargeSummaries

Set ExcludeFromIDSReporting = 1,
ExclusionReason = 'Endoscopy in TDC - uses unisoft to produce summary of procedures'
where dischargeward in ('WOMENS HEALTH SUITE')--WHS


--Well Babies
--Note:  also need to exlude well babies from wards C2 and C3  but cannot distinguish between well and unwell babaies discharged from these wards
Update   WHReporting.IDS.DischargeSummaries

Set ExcludeFromIDSReporting = 1,
ExclusionReason = 'Well Babies - Midwives provide summary to GP'
where dischargeward in ('BIRTH CENTRE',--BC
'DELIVERY SUITE',--DS
'HOME BIRTH',--HOM
'C2 COTS')--CC"

select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 'Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'IDS - WHREPORTING BuildIDSReportingTable', @Stats, @StartTime

END