﻿-- =============================================
-- Author:		CMan
-- Create date: 13/05/2015
-- Description:	Stored Procedure to support SSRS report detailing A&E Discharge Summaries sent to GP via EDT to be reported to Commissioners from April 2015
				
-- =============================================
CREATE PROCEDURE [IDS].[ReportAEDischargeSummariesByPractice] @ReportMonthStart datetime, @ReportMonthEnd datetime, @PracticeStatus varchar(250)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


--Declare @ReportMonthStart datetime = '20150401'

Select  cal.TheMonth
	,cal.FirstDateOfMonth
	,ISNULL(ds.IDS_Practice_code, ds.Spell_Practice_code) AS Practice
	,CASE 
		WHEN gp.ActiveFLag = 1
			THEN 'Active Practice'
		WHEN gp.ActiveFlag = 0
			THEN 'Inactive Practice'
		WHEN gp.ActiveFlag IS NULL
			THEN 'Not on HUB'
		END AS PracticeStatus
	,Sum(CASE 
			WHEN (ds.IDS_Doc_status_code = 4002)
				THEN 1
			WHEN (ds.IDS_Doc_status_code = 4001)
				THEN 1
			WHEN (ds.IDS_Doc_status_code = 4003)
				THEN 1
			WHEN (ds.IDS_Doc_status_code = 4006)
				THEN 1
			WHEN (ds.IDS_Doc_status_code = 1003 and ds.EDTStatus like 'Error: 7%') 
				THEN 1
			ELSE 0
			END) AS EDTHub
	,Sum(CASE 
			WHEN (ds.IDS_Doc_status_code = - 1)
				THEN 1
			ELSE 0
			END) AS [Not Processed]
	,Sum(CASE 
			WHEN (ds.EDTStatus = 'Can not find this practice or this practice is inactived on the HUB.')
				THEN 1
			ELSE 0
			END) AS [Managed Print Service]
	,Sum(CASE 
			WHEN (ds.IDS_Doc_status_code = 1003
					AND (ds.EDTStatus <> 'Can not find this practice or this practice is inactived on the HUB.'
						AND 
						ds.EDTStatus NOT LIKE 'Error: 7%'
						)
					)
				THEN 1
			--WHEN (ds.IDS_Doc_status_code = 1003
			--		AND ds.EDTStatus NOT LIKE 'Error: 7%'
			--	)
			--	THEN 1
			WHEN (ds.IDS_Doc_status_code = 4005)
				THEN 1
			WHEN (ds.IDS_Doc_status_code = 1002)
				THEN 1
			WHEN (ds.IDS_Doc_status_code = 1004)
				THEN 1
			WHEN (ds.IDS_Doc_status_code = 5001)
				THEN 1
			WHEN (
					ds.IDS_Doc_status_code = 5002
					AND ds.EDTStatus <> 'Can not find this practice or this practice is inactived on the HUB.'
					)
				THEN 1
			WHEN (ds.IDS_Doc_status_code = 5003)
				THEN 1
			WHEN (ds.IDS_Doc_status_code = 6003)
				THEN 1
			ELSE 0
			END) AS [Manual]
	,SUM(CASE 
			WHEN (ds.IDS_Doc_status_code = 4002)
				THEN 0
			WHEN (ds.IDS_Doc_status_code = 4001)
				THEN 0
			WHEN (ds.IDS_Doc_status_code = 4003)
				THEN 0
			WHEN (ds.IDS_Doc_status_code = 4006)
				THEN 0
			WHEN (ds.IDS_Doc_status_code = - 1)
				THEN 0
			WHEN (ds.EDTStatus = 'Can not find this practice or this practice is inactived on the HUB.')
				THEN 0
			WHEN (
					ds.IDS_Doc_status_code = 1003
					AND ds.EDTStatus <> 'Can not find this practice or this practice is inactived on the HUB.'
					AND ds.EDTStatus  LIKE 'Error: 7%'
					)
				THEN 0
			WHEN (ds.IDS_Doc_status_code = 4005)
				THEN 0
			WHEN (ds.IDS_Doc_status_code = 1002)
				THEN 0
			WHEN (ds.IDS_Doc_status_code = 1004)
				THEN 0
			WHEN (ds.IDS_Doc_status_code = 5001)
				THEN 0
			WHEN (
					ds.IDS_Doc_status_code = 5002
					AND ds.EDTStatus <> 'Can not find this practice or this practice is inactived on the HUB.'
					)
				THEN 0
			WHEN (ds.IDS_Doc_status_code = 5003)
				THEN 0
			WHEN (ds.IDS_Doc_status_code = 6003)
				THEN 0
			WHEN (ds.IDSID IS NULL)
				THEN 0
			ELSE 1
			END) AS UNKNOWN
	,COUNT(DISTINCT SourceUniqueID) AS Discharges
	,Sum(CASE 
			WHEN ds.IDSID IS NOT NULL
				THEN 1
			ELSE 0
			END) AS JAC_DS_created
	,SUM( CASE 
			WHEN (ds.IDS_Doc_status_code = 4002 OR ds.IDS_Doc_status_code = 4001 OR ds.IDS_Doc_status_code = 4003 OR ds.IDS_Doc_status_code = 4006 OR
																					(ds.IDS_Doc_status_code = 1003 and ds.EDTStatus like 'Error: 7%') 
						)
				 AND 
				DATEDIFF(day, DischargeDateTime, ds.InsertDateTime) <= 1--CM 20/06/2016 Amended this to <=1 and to use insert date time from this '--DATEDIFF(day,DischargeDateTime,EDTHub_Status_LastUpdated) <=2' as per NScattergood (re: 1 day) and SFarrall(re: new field to use)

			THEN 1
			ELSE 0
			END
		) AS Within1day

From 
WHREPORTING.IDS.AEDischargeSummaries ds
LEFT OUTER JOIN WH.IDS.ECC_Doc_Status doc_stat ON ds.IDS_Doc_status_code = doc_stat.Doc_Status
LEFT OUTER JOIN (
	SELECT DISTINCT FirstDateOfMonth
		,TheMonth
	FROM WHREPORTING.LK.Calendar
	) cal ON ds.monthstart = cal.FirstDateOfMonth
LEFT OUTER JOIN WHREPORTING.IDS.EDTGPPractices GP ON ISNULL(ds.IDS_Practice_code, ds.Spell_Practice_code) = GP.PracticeCode
WHERE ds.Monthstart >= @ReportMonthStart
and  ds.Monthstart <= @ReportMonthEnd
and CASE 
		WHEN gp.ActiveFLag = 1
			THEN 'Active Practice'
		WHEN gp.ActiveFlag = 0
			THEN 'Inactive Practice'
		WHEN gp.ActiveFlag IS NULL
			THEN 'Not on HUB'
		END =  @PracticeStatus

GROUP BY cal.TheMonth
	,cal.FirstDateOfMonth
	,CASE 
		WHEN gp.ActiveFLag = 1	THEN 'Active Practice'
		WHEN gp.ActiveFlag = 0	THEN 'Inactive Practice'
		WHEN gp.ActiveFlag IS NULL THEN 'Not on HUB'
		END
	,ISNULL(ds.IDS_Practice_code, ds.Spell_Practice_code)

ORDER BY 2
	,4
	,3





END