﻿-- =============================================
-- Author:		CMan
-- Create date: 29/09/2014
-- Description:	SSRS Report to be created following discussion with Geoff Corner/Sean Farrell.
				--Report to give breakdown on number of discharges, number of discharges with JAC created, of those with JAC created - how many were sent via EDT, how many by Print Service, how many by manual intervention
-- =============================================
CREATE PROCEDURE [IDS].[ReportDischargeSummariesBySentCategory]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Declare @start datetime = '20140401'

--Select   cal.TheMonth
--		, cal.FirstDateOfMonth
--		, ds.DischargeWard
--		--, doc_stat.Doc_Status_Description 
--		,Sum(CASE 
--        WHEN (ds.IDS_Doc_status_code = 4002 and ds.EDTHub_RejectType = 0) THEN 1
--        WHEN (ds.IDS_Doc_status_code = 4001 ) THEN  1
--        WHEN (ds.IDS_Doc_status_code = 4003 ) THEN 1
--        WHEN (ds.IDS_Doc_status_code = 4006 ) THEN 1
--       Else 0
--       End) as EDTHub
--       ,Sum(Case WHEN (ds.IDS_Doc_status_code = 1003 and ds.EDTHub_RejectType IS NULL) THEN 1
--              WHEN (ds.IDS_Doc_status_code = 4005 and ds.EDTHub_RejectType = 2) THEN 1
--              Else 0
--              End)as [Managed Print Service]
--           ,Sum(Case WHEN (ds.IDS_Doc_status_code = 1003 and ds.EDTHub_RejectType = 0 )  THEN  1
--            WHEN (ds.IDS_Doc_status_code = 4005 and ds.EDTHub_RejectType = 1) THEN 1
--            WHEN (ds.IDS_Doc_status_code = 1002 ) THEN 1
--            WHEN (ds.IDS_Doc_status_code = 1004 ) THEN 1
--            Else 0 End)  as [Manual]
--      , SUM(CASE 
--        WHEN (ds.IDS_Doc_status_code = 4002 and ds.EDTHub_RejectType = 0) THEN 0
--        WHEN (ds.IDS_Doc_status_code = 4001 ) THEN 0
--        WHEN (ds.IDS_Doc_status_code = 4003 ) THEN 0
--        WHEN (ds.IDS_Doc_status_code = 4006 ) THEN 0
--            WHEN (ds.IDS_Doc_status_code = 1003 and ds.EDTHub_RejectType IS NULL) THEN 0
--            WHEN (ds.IDS_Doc_status_code = 1003 and ds.EDTHub_RejectType = 0 ) THEN 0
--            WHEN (ds.IDS_Doc_status_code = 4005 and ds.EDTHub_RejectType = 2) THEN 0
--            WHEN (ds.IDS_Doc_status_code = 4005 and ds.EDTHub_RejectType = 1) THEN 0
--            WHEN (ds.IDS_Doc_status_code = 1002 ) THEN 0
--            WHEN (ds.IDS_Doc_status_code = 1004 ) THEN 0
--            WHEN (ds.IDSId is null) THEN 0
--            ELSE 1
--      END)as Unknown
--      ,COUNT(Distinct SourceSpellNo) as Discharges
--      , Sum(Case when ds.IDSID is not null then 1
--		 Else 0
--		 End) as JAC_DS_created
--		--, ds.*
--from WHREPORTING.IDS.DischargeSummaries ds 
--Left outer join WH.IDS.ECC_Doc_Status doc_stat on ds.IDS_Doc_status_code = doc_stat.Doc_Status
--  LEFT OUTER JOIN (Select distinct FirstDateOfMonth, TheMonth from WHREPORTING.LK.Calendar )cal
--                    ON ds.monthstart = cal.FirstDateOfMonth
--where DischargeDateTime >= @start
--Group by cal.TheMonth,  ds.DischargeWard, cal.FirstDateOfMonth





Select   cal.TheMonth
		, cal.FirstDateOfMonth
		, ds.DischargeWard
		--, doc_stat.Doc_Status_Description 
		,Sum(CASE 
        WHEN (ds.IDS_Doc_status_code = 4002 )THEN 1
        WHEN (ds.IDS_Doc_status_code = 4001 ) THEN  1
        WHEN (ds.IDS_Doc_status_code = 4003 ) THEN 1
        WHEN (ds.IDS_Doc_status_code = 4006 ) THEN 1
       Else 0
       End) as EDTHub
       ,Sum(Case WHEN (ds.IDS_Doc_status_code = -1) THEN 1 Else 0 End) as [Not Processed]
       ,Sum(Case  WHEN (ds.EDTStatus = 'Can not find this practice or this practice is inactived on the HUB.') THEN 1 Else 0 End) 
       as [Managed Print Service]
           ,Sum(Case WHEN (ds.IDS_Doc_status_code = 1003 and ds.EDTStatus <> 'Can not find this practice or this practice is inactived on the HUB.')   THEN  1
            WHEN (ds.IDS_Doc_status_code = 4005 ) THEN 1
            WHEN (ds.IDS_Doc_status_code = 1002 ) THEN 1
            WHEN (ds.IDS_Doc_status_code = 1004 ) THEN 1
            WHEN (ds.IDS_Doc_status_code = 5001) THEN 1                      
            WHEN (ds.IDS_Doc_status_code = 5002  and ds.EDTStatus <> 'Can not find this practice or this practice is inactived on the HUB.')  THEN 1        
            WHEN (ds.IDS_Doc_status_code = 5003) THEN 1
            WHEN (ds.IDS_Doc_status_code = 6003) THEN  1
            Else 0 End)  as [Manual]
      , SUM(CASE 
        WHEN (ds.IDS_Doc_status_code = 4002 )THEN 0
        WHEN (ds.IDS_Doc_status_code = 4001 ) THEN 0
        WHEN (ds.IDS_Doc_status_code = 4003 ) THEN 0
        WHEN (ds.IDS_Doc_status_code = 4006 ) THEN 0
		WHEN (ds.IDS_Doc_status_code = -1) THEN 0
		WHEN (ds.EDTStatus = 'Can not find this practice or this practice is inactived on the HUB.') THEN 0
		WHEN (ds.IDS_Doc_status_code = 1003 and ds.EDTStatus <> 'Can not find this practice or this practice is inactived on the HUB.')   THEN  0
            WHEN (ds.IDS_Doc_status_code = 4005 ) THEN 0
            WHEN (ds.IDS_Doc_status_code = 1002 ) THEN 0
            WHEN (ds.IDS_Doc_status_code = 1004 ) THEN 0
            WHEN (ds.IDS_Doc_status_code = 5001) THEN 0                      
            WHEN (ds.IDS_Doc_status_code = 5002 and ds.EDTStatus <> 'Can not find this practice or this practice is inactived on the HUB.')  THEN 0       
            WHEN (ds.IDS_Doc_status_code = 5003) THEN 0
            WHEN (ds.IDS_Doc_status_code = 6003) THEN  0
            WHEN (ds.IDSID is null ) Then 0
            ELSE 1
      END)as Unknown
      ,COUNT(Distinct SourceSpellNo) as Discharges
      , Sum(Case when ds.IDSID is not null then 1
		 Else 0
		 End) as JAC_DS_created
	 , Sum(Case when ds.IDSId is null and  ExcludeFromIDSReporting = 1 then 1 Else 0 End) as ExcludedNoIDSCreated--added this to give an indication of those discharges which have not had an IDS but are an exclusion anyway
		--, ds.*

from WHREPORTING.IDS.DischargeSummaries ds 
Left outer join WH.IDS.ECC_Doc_Status doc_stat on ds.IDS_Doc_status_code = doc_stat.Doc_Status
  LEFT OUTER JOIN (Select distinct FirstDateOfMonth, TheMonth from WHREPORTING.LK.Calendar )cal
                    ON ds.monthstart = cal.FirstDateOfMonth
where DischargeDateTime >=@start
Group by cal.TheMonth,  ds.DischargeWard, cal.FirstDateOfMonth





END