﻿-- =============================================
-- Author:		CMan
-- Create date: 29/09/2014
-- Description:	SSRS Report to be created following discussion with Geoff Corner/Sean Farrell.
				--Report to give breakdown on number of discharges, number of discharges with JAC created, of those with JAC created - how many were sent via EDT, how many by Print Service, how many by manual intervention
-- =============================================
CREATE PROCEDURE [IDS].[ReportDischargeSummariesBySentCategory_PatientLevel] @TheMonth varchar(25), @DisWard varchar(max),@Jac varchar(Max), @Action varchar(max), @Title varchar(250), @Exc varchar(max)
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Declare @start datetime = '20140401'

Select  distinct  CASE 
                  WHEN (ds.EDTStatus = 'Can not find this practice or this practice is inactived on the HUB.') THEN 'Managed Print Service'
        WHEN (ds.IDS_Doc_status_code = -1) THEN 'Not Processed'
            WHEN (ds.IDS_Doc_status_code  = 1002 ) THEN 'Manual'
            WHEN (ds.IDS_Doc_status_code  = 1003 ) THEN 'Manual'
            WHEN (ds.IDS_Doc_status_code  = 1004 ) THEN 'Manual'
        WHEN (ds.IDS_Doc_status_code  = 4001 ) THEN 'EDTHub'
        WHEN (ds.IDS_Doc_status_code  = 4002 ) THEN 'EDTHub'
        WHEN (ds.IDS_Doc_status_code  = 4003 ) THEN 'EDTHub'
        WHEN (ds.IDS_Doc_status_code  = 4005 ) THEN 'Manual'
        WHEN (ds.IDS_Doc_status_code  = 4006 ) THEN 'EDTHub'
            WHEN (ds.IDS_Doc_status_code  = 5001) THEN 'Manual'                      
            WHEN (ds.IDS_Doc_status_code  = 5002) THEN 'Manual'          
            WHEN (ds.IDS_Doc_status_code  = 5003) THEN 'Manual'
            WHEN (ds.IDS_Doc_status_code  = 6003) THEN 'Manual'
            WHEN (ds.IDSId is null) THEN 'No JAC Created'
            ELSE 'Unknown'
      END as [Action]
      , doc_stat.Doc_Status_Description 
      --, 1 as Discharges
      , Case when ds.IDSID is not null then 'Yes'
		 when ds.IDSID is null then 'No'
		 Else 'Other'
		 End as JAC_DS_created
		,cal.TheMonth
		,cal.FirstDateOfMonth
		,ds.SourceSpellNo
		,ds.IDSId
		,ds.FaciltyID
		,ds.AdmissionDateTime
		,ds.DischargeDateTime
		,ds.DischargeWard
		,ds.IDS_Practice_code
		,ds.IDS_Doc_status_code
		,ds.EDTHub_DocId
		,ds.EDTStatus
		,ds.EDTHub_RejectType
		,ds.EDTHub_RejectReason
		,case when ds.ExcludeFromIDSReporting = 1 then 'Yes' 
				when ds.ExcludeFromIDSReporting = 0 then 'No'
				Else '-'
				End as ExcludeFromIDSReporting
		,ds.ExclusionReason

from WHREPORTING.IDS.DischargeSummaries ds 
Left outer join WH.IDS.ECC_Doc_Status doc_stat on ds.IDS_Doc_status_code = doc_stat.Doc_Status
  LEFT OUTER JOIN WHREPORTING.LK.Calendar cal
                    ON ds.monthstart = cal.FirstDateOfMonth
where DischargeDateTime >= @start
and TheMonth = @TheMonth
and DischargeWard in (SELECT Item FROM   dbo.Split  (@DisWard,',') )
and Case when ds.IDSID is not null then 'Yes'
		 when ds.IDSID is null then 'No'
		 Else 'Other'
		 End in (SELECT Item FROM   dbo.Split  (@Jac,',') )
		
and   CASE 
                  WHEN (ds.EDTStatus = 'Can not find this practice or this practice is inactived on the HUB.') THEN 'Managed Print Service'
        WHEN (ds.IDS_Doc_status_code = -1) THEN 'Not Processed'
            WHEN (ds.IDS_Doc_status_code  = 1002 ) THEN 'Manual'
            WHEN (ds.IDS_Doc_status_code  = 1003 ) THEN 'Manual'
            WHEN (ds.IDS_Doc_status_code  = 1004 ) THEN 'Manual'
        WHEN (ds.IDS_Doc_status_code  = 4001 ) THEN 'EDTHub'
        WHEN (ds.IDS_Doc_status_code  = 4002 ) THEN 'EDTHub'
        WHEN (ds.IDS_Doc_status_code  = 4003 ) THEN 'EDTHub'
        WHEN (ds.IDS_Doc_status_code  = 4005 ) THEN 'Manual'
        WHEN (ds.IDS_Doc_status_code  = 4006 ) THEN 'EDTHub'
            WHEN (ds.IDS_Doc_status_code  = 5001) THEN 'Manual'                      
            WHEN (ds.IDS_Doc_status_code  = 5002) THEN 'Manual'          
            WHEN (ds.IDS_Doc_status_code  = 5003) THEN 'Manual'
            WHEN (ds.IDS_Doc_status_code  = 6003) THEN 'Manual'
            WHEN (ds.IDSId is null) THEN 'No JAC Created'
            ELSE 'Unknown'
      END in (SELECT Item FROM   dbo.Split  (@Action,',') )
 and case when ds.ExcludeFromIDSReporting = 1 then 'Yes' 
				when ds.ExcludeFromIDSReporting = 0 then 'No'
				Else '-'
				End in (SELECT Item FROM   dbo.Split  (@Exc,',') )



END