﻿-- =============================================
-- Author:		CMan
-- Create date: 12/02/2014
-- Description:	Stored Procedure for SSRS report to look at the percentage of Inpatient Discharge Summaries created on JAC
-- =============================================
CREATE PROCEDURE [IDS].[ReportPercentageofJACDischargeSummariesCreated]   @month varchar(400),
@excl nvarchar(10)


AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT Monthstart,
       TheMonth                      AS [Month],
       DischargeWard,
       Count(DISTINCT SourceSpellNo) AS NumberOfDischarges,
       Count(DISTINCT IDSId)         AS NumberofIDS
FROM   WHReporting.IDS.DischargeSummaries ds
       LEFT OUTER JOIN WH.IDS.ECC_Doc_Status st
                    ON ds.IDS_Doc_status_code = st.doc_status
       LEFT OUTER JOIN WHREPORTING.LK.Calendar cal
                    ON ds.monthstart = cal.FirstDateOfMonth
       where ds.ExcludeFromIDSReporting in  (SELECT Item
                         FROM   dbo.Split (@excl, ',')) 
        and Monthstart in (SELECT Item
                         FROM   dbo.Split (@month, ',')) 
GROUP  BY Monthstart,
          DischargeWard,
          TheMonth 

END