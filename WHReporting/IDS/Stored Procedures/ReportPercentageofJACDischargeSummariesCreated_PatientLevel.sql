﻿-- =============================================
-- Author:		CMan
-- Create date: 12/02/2014
-- Description:	Stored Procedure for SSRS patient level drill through report to look at Inpatient Discharge Summaries created on JAC
-- =============================================
CREATE PROCEDURE [IDS].[ReportPercentageofJACDischargeSummariesCreated_PatientLevel]
 @month varchar(50),
@excl nvarchar(10),
@ward varchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON


SELECT ds.FaciltyID as PatientID,
       AdmissionDateTime,
       DischargeDateTime,
       DischargeWard,
       DischargeDestination,
       CASE
         WHEN IDSID IS NULL THEN 'No IDS record'
         WHEN IDSID IS NOT NULL THEN 'IDS Completed'
         ELSE NULL
       END AS IDSComplete
FROM   WHReporting.IDS.DischargeSummaries ds
       LEFT OUTER JOIN WH.IDS.ECC_Doc_Status st
                    ON ds.IDS_Doc_status_code = st.doc_status
       LEFT OUTER JOIN WHREPORTING.LK.Calendar cal
                    ON ds.monthstart = cal.FirstDateOfMonth
WHERE  ds.ExcludeFromIDSReporting = @excl
       AND Monthstart = @month
       AND DischargeWard = @Ward 
       
Group by FaciltyID,
       AdmissionDateTime,
       DischargeDateTime,
       DischargeWard,
       DischargeDestination,
       CASE
         WHEN IDSID IS NULL THEN 'No IDS record'
         WHEN IDSID IS NOT NULL THEN 'IDS Completed'
         ELSE NULL
       END
Order by CASE
         WHEN IDSID IS NULL THEN 'No IDS record'
         WHEN IDSID IS NOT NULL THEN 'IDS Completed'
         ELSE NULL
       END desc


END