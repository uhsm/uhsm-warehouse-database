﻿CREATE TABLE [IDS].[AEDischargeSummaries](
	[Monthstart] [datetime] NULL,
	[SourceUniqueID] [varchar](20) NULL,
	[IDSId] [int] NULL,
	[IDS_Doc_status_code] [int] NULL,
	[EDTHub_DocId] [int] NULL,
	[EDTStatus] [nvarchar](300) NULL,
	[LastUpdate] [datetime] NULL,
	[IDSComplete] [nvarchar](5) NULL,
	[FaciltyID] [varchar](20) NULL,
	[AdmissionDateTime] [datetime] NULL,
	[DischargeDateTime] [datetime] NULL,
	[IDS_Practice_code] [varchar](20) NULL,
	[Spell_Practice_code] [varchar](20) NULL,
	[DischargeDestination] [varchar](100) NULL,
	[Age] [int] NULL,
	[ExcludeFromIDSReporting] [bit] NULL,
	[ExclusionReason] [varchar](100) NULL,
	[EDTHub_RejectType] [int] NULL,
	[EDTHub_RejectReason] [varchar](250) NULL,
	[EDTHub_status_LastUpdated] [datetime] NULL,
	[InsertDateTime] [datetime] NULL,
	[DataLastLoaded] [datetime] NULL
) ON [PRIMARY]