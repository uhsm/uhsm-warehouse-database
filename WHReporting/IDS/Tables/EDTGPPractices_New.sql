﻿CREATE TABLE [IDS].[EDTGPPractices_New](
	[PracticeCode] [nvarchar](8) NOT NULL,
	[PracticeName] [nvarchar](100) NULL,
	[Address Line 1] [nvarchar](100) NULL,
	[Address Line 2] [nvarchar](35) NULL,
	[Address Line 3] [nvarchar](35) NULL,
	[Address Line 4] [nvarchar](35) NULL,
	[Address Line 5] [nvarchar](35) NULL,
	[Postcode] [nvarchar](8) NULL,
	[CCGCode] [nvarchar](6) NULL,
	[ActiveFlag] [int] NOT NULL,
	[HubStartDate] [datetime] NULL
) ON [PRIMARY]