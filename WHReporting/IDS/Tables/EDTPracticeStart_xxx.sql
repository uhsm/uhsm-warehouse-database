﻿CREATE TABLE [IDS].[EDTPracticeStart_xxx](
	[PracticeCode] [nvarchar](8) NOT NULL,
	[PracticeName] [nvarchar](100) NULL,
	[CCGCode] [nvarchar](6) NULL,
	[StartDate] [datetime] NULL
) ON [PRIMARY]