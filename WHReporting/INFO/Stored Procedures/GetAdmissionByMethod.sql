﻿CREATE Procedure [INFO].[GetAdmissionByMethod] 
@AdmissionMethod varchar(20),
@StartDate Date,
@EndDate Date



AS 

Select 
AdmissionDate,
AdmissionConsultantName,
AdmissionDivision,
AdmissionDirectorate,
AdmissionSpecialtyCode,
AdmissionSpecialty,
[AdmissionSpecialtyCode(Function)],
[ADmissionSpecialtyCode(Main)],
AdmissionMethod,
AdmissionMethodCode,
AdmissionMethodNHSCode,
Adms = COUNT(*)
from
APC.Spell
Where AdmissionMethodCode = @AdmissionMethod
and AdmissionDate >=@StartDate
and AdmissionDate <= @EndDate
Group By
AdmissionDate,
AdmissionConsultantName,
AdmissionDivision,
AdmissionDirectorate,
AdmissionSpecialtyCode,
AdmissionSpecialty,
[AdmissionSpecialtyCode(Function)],
[ADmissionSpecialtyCode(Main)],
AdmissionMethod,
AdmissionMethodCode,
AdmissionMethodNHSCode