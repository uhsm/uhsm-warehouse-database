﻿CREATE View [INFO].[AdmissionByMethod] AS 

Select 
AdmissionDate,
AdmissionConsultantName,
AdmissionDivision,
AdmissionDirectorate,
AdmissionSpecialtyCode,
AdmissionSpecialty,
[AdmissionSpecialtyCode(Function)],
[ADmissionSpecialtyCode(Main)],
AdmissionMethod,
AdmissionMethodCode,
AdmissionMethodNHSCode,
Adms = COUNT(*)
from
APC.Spell

Group By
AdmissionDate,
AdmissionConsultantName,
AdmissionDivision,
AdmissionDirectorate,
AdmissionSpecialtyCode,
AdmissionSpecialty,
[AdmissionSpecialtyCode(Function)],
[ADmissionSpecialtyCode(Main)],
AdmissionMethod,
AdmissionMethodCode,
AdmissionMethodNHSCode