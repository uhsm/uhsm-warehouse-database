﻿CREATE View [INFO].[NHSNo1] AS 

Select
FaciltyID
,SPE.EncounterRecno
,SPE.AdmissionDate
,SPE.NHSNo
,PAT.PatientSurname
,PAT.PatientForename
,PAT.DateOfBirth
,PAT.PostCode
,SPE.DischargeDate
,SPE.AdmissionConsultantName
,SPE.AdmissionDivision
,SPE.AdmissionSpecialty
,SPE.GP
,SPE.GPCode

from APC.Spell SPE
left outer join APC.Patient PAT
on SPE.EncounterRecno = PAT.EncounterRecno

where SPE.EncounterRecno in 
(Select MAX(EncounterRecno)
From APC.Spell
Where AdmissionDate >= '01 April 2012'
Group By
FaciltyID)

--and SPE.NHSNo is NULL

group by
SPE.FaciltyID
,SPE.EncounterRecno
,AdmissionDate
,SPE.NHSNo
,PAT.PatientSurname
,PAT.PatientForename
,PAT.DateOfBirth
,PAT.PostCode
,SPE.DischargeDate
,SPE.AdmissionConsultantName
,SPE.AdmissionDivision
,SPE.AdmissionSpecialty
,SPE.GP
,SPE.GPCode