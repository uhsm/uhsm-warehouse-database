﻿CREATE View [INFO].[Stroke] AS 

SELECT
 EPI.FacilityID
,EPI.NHSNo 
,EPI.SourceSpellNo
,PAT.PatientSurname
,PAT.PatientForename
,EPI.AdmissionDate
,EPI.AdmissionTime
,EPI.AdmissionDateTime
,EPI.DischargeDateTime
,EPI.DischargeDate
,EPI.DischargeTime
,SPE.PCT
,SPE.AdmissionMethodCode
,EPI.AdmissionType
,EPI.EpisodeStartDateTime
,EPI.EpisodeEndDateTime
,PAT.DateOfBirth
,PAT.DateOfDeath
,SPE.DischargeMethodCode
,[StrokeUnitLoS]
,CC.CriticalCareStartDatetime
,CC.CriticalCareEndDatetime
,CC.[ICU Days]
,CC.[HDU Days]
,AE.ArrivalTime
,AE.AttendanceConclusionTime
,CAL.TheMonth
,CAL.FinancialYear
--,AEPAT.rm2number

--,convert(decimal(5,2),(CAST (AE.AttendanceConclusionTime as float)-CAST (AE.ArrivalTime as float))) as TimeinAE
,(convert(decimal(5,2),EncounterDurationMinutes))/(60*24) as TimeinAE

--,DATEDIFF("D",CC.CriticalCareStartDatetime, CC.CriticalCareEndDatetime) AS CCPLOS
--,convert(decimal(5,2),(CAST (CriticalCareEndDatetime as float)-CAST (CriticalCareStartDatetime as float))) as CCP

,CAST((CriticalCareEndDatetime - CriticalCareStartDatetime) AS NUMERIC(10, 2)) AS CCP 

,DATEDIFF("d",EPI.AdmissionDateTime, PAT.DateOfDeath) AS DOD

,convert(decimal(5,2),
(CAST (PAT.DateOfDeath as float)-CAST (EPI.AdmissionDateTime as float))) as DODroundedto2dp

,CASE WHEN SPE.DischargeMethodCode = '4'
AND (DATEDIFF (MINUTE,EPI.AdmissionDateTime, EPI.DischargeDateTime)) < 1440
Then 'Died within 24hrs' Else 'Null'
End as DeathExclusions

,(DATEDIFF ("d",EPI.EpisodeStartDateTime, EPI.EpisodeEndDateTime)) AS LoS

,convert(decimal(5,2)
,(CAST (EPI.EpisodeEndDateTime as float)-CAST (EPI.EpisodeStartDateTime as float))) as LOS2

,CASE WHEN (convert(decimal(5,2)
,(CAST (EPI.DischargeDateTime as float)-CAST (EPI.AdmissionDateTime as float)))) <= 1
THEN 'Exclude' Else 'Non Exclusion'
End as LoSExclusions

FROM APC.Episode EPI
LEFT OUTER JOIN APC.Patient PAT
ON EPI.EncounterRecno = PAT.EncounterRecno
LEFT OUTER JOIN APC.Spell SPE
on EPI.SourceSpellNo = SPE.SourceSpellNo
LEFT OUTER JOIN
      (
            SELECT DISTINCT 
             APC.WardStay.SourceSpellNo
            ,APC.WardStay.WardCode
            --,CAST(SUM(DATEDIFF("D",StartTime,EndTime)) AS FLOAT) AS T
            ,SUM(CAST((EndTime - StartTime) AS NUMERIC(10, 2))) AS [StrokeUnitLoS]
            FROM APC.WardStay
            WHERE WardCode = 'F15S'
            --AND SourceSpellNo ='150391191'
            GROUP BY SourceSpellNo, WardCode 
      ) AS W
ON SPE.SourceSpellNo = W.SourceSpellNo
LEFT OUTER JOIN APC.CriticalCarePeriod CC
ON EPI.SourceSpellNo = CC.SourceSpellNo
LEFT OUTER JOIN WH.[CASCADE].AEPatMas AEPAT
ON EPI.FacilityID = AEPAT.rm2number
LEFT OUTER JOIN WH.AE.Encounter AE
ON AEPAT.aepatno = AE.DistrictNo
AND ArrivalDate >= EPI.AdmissionDate and ArrivalDate < EPI.DischargeDate
LEFT OUTER JOIN WHREPORTING.LK.Calendar CAL
ON EPI.AdmissionDate = CAL.TheDate

WHERE EPI.AdmissionDate >= '01 April 2010'
and EPI.PrimaryDiagnosisCode IN ('I610', 'I611', 'I612', 'I613', 'I614', 'I615', 'I616', 'I618', 'I619', 'I630', 'I631', 'I632', 'I633','I634', 'I635', 'I636', 'I638', 'I639', 'I64X')
and EPI.SpecialtyCode <> '420'
--and WS.WardCode = 'F15S'
--and EPI.LastEpisodeInSpell = '1'
--and EPI.FacilityID = 'RM21890963'

GROUP BY
EPI.FacilityID
,EPI.NHSNo 
,EPI.SourceSpellNo
--,EPI.SourcePatientNo
--,EPI.NHSNo
--,EPI.LengthOfEpisode
,EPI.AdmissionDate
,EPI.AdmissionTime
,EPI.AdmissionDateTime
,EPI.DischargeDateTime
,EPI.DischargeTime
,EPI.DischargeDate
,SPE.PCT
,SPE.AdmissionMethodCode
,SPE.DischargeMethodCode
,EPI.AdmissionType
,EPI.EpisodeStartDateTime
,EPI.EpisodeEndDateTime
--,EPI.PrimaryDiagnosisCode
--,EPI.EpisodePCT
--,EPI.EpisodePCTCode
--,EPI.SpecialtyCode
--,EPI.ConsultantCode
--,EPI.ConsultantName
--,EPI.FirstEpisodeInSpell
--,EPI.LastEpisodeInSpell
,PAT.PatientSurname
,PAT.PatientForename
,PAT.DateOfBirth
,PAT.DateOfDeath
,[StrokeUnitLoS]
,CC.CriticalCareStartDatetime
,CC.CriticalCareEndDatetime
,CC.[ICU Days]
,CC.[HDU Days]
,AE.ArrivalTime
,AE.AttendanceConclusionTime
,CAL.TheMonth
,CAL.FinancialYear
--,AEPAT.rm2number


--,convert(decimal(5,2),
--(CAST (AE.AttendanceConclusionTime as float)-CAST (AE.ArrivalTime as float)))
,(convert(decimal(5,2),EncounterDurationMinutes))/(60*24)

,DATEDIFF("D",CC.CriticalCareStartDatetime, CC.CriticalCareEndDatetime)
,DATEDIFF("d",EPI.AdmissionDateTime, PAT.DateOfDeath) 

,convert(decimal(5,2),
(CAST (PAT.DateOfDeath as float)-CAST (EPI.AdmissionDateTime as float))) 

,CASE WHEN SPE.DischargeMethodCode = '4'
AND (DATEDIFF (MINUTE,EPI.AdmissionDateTime, EPI.DischargeDateTime)) < 1440
Then 'Died within 24hrs' Else 'Null'
End 

--,(DATEDIFF ("d",EPI.AdmissionDateTime, EPI.DischargeDateTime)) 
,(DATEDIFF ("d",EPI.EpisodeStartDateTime, EPI.EpisodeEndDateTime))

,convert(decimal(5,2)
--,(CAST (EPI.DischargeDateTime as float)-CAST (EPI.AdmissionDateTime as float))) 
,(CAST (EPI.EpisodeEndDateTime as float)-CAST (EPI.EpisodeStartDateTime as float)))

,CASE WHEN (convert(decimal(5,2)
,(CAST (EPI.DischargeDateTime as float)-CAST (EPI.AdmissionDateTime as float)))) <= 1
THEN 'Exclude' Else 'Non Exclusion'
End 


--,CASE WHEN (DATEDIFF ("d",EPI.AdmissionDateTime, EPI.DischargeDateTime)) <= 1
--THEN 'Exclude' Else 'Non Exclusion'
--End