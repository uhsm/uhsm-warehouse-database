﻿CREATE View [INFO].[vwAPCElectiveAdmissions201213] 

AS

SELECT 

       [SourceSpellNo]
       ,[AdmissionDateTime]
      ,[DischargeDateTime]
      --,[InpatientStayCode]
      ,[InpatientStayCode]
      ,IntendedManagement
            ,[AdmissionConsultantName]
      ,[AdmissionDivision]
      ,[AdmissionDirectorate]
      --,[AdmissionSpecialtyCode]
      --,[AdmissionSpecialty]
      ,[AdmissionSpecialtyCode(Function)]
      ,[AdmissionSpecialty(Function)]
      
      
        FROM [WHREPORTING].[APC].[Spell] 
  

  
  WHERE
  [DischargeDate] between '01 apr 2012' and '31 mar 2013 23:59:59'
  and
  AdmissionMethodNHSCode between 11 and 13