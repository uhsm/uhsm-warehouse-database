﻿Create View [INFO].[vwAPCElectiveAdmissions2013] 

AS

SELECT 

       [SourceSpellNo]
       ,[AdmissionDateTime]
      ,[DischargeDateTime]
      ,AdmissionMethodNHSCode
            ,[AdmissionConsultantName]
      ,[AdmissionDivision]
      ,[AdmissionDirectorate]
      --,[AdmissionSpecialtyCode]
      --,[AdmissionSpecialty]
      ,[AdmissionSpecialtyCode(Function)]
      ,[AdmissionSpecialty(Function)]
      
      
        FROM [WHREPORTING].[APC].[Spell] 
  

  
  WHERE
  [DischargeDate] between '01 jan 2013' and '31 dec 2013 23:59:59'
  and
  AdmissionMethodNHSCode between 11 and 13