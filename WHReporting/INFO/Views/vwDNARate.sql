﻿CREATE View [INFO].[vwDNARate] AS 
SELECT
CASE 
--WHEN [SpecialtyCode(Function)] IN ('340','340G','352','316','317') THEN '340'
WHEN [SpecialtyCode(Function)] IN ('340','340G','352','316') THEN '340'
WHEN [SpecialtyCode(Function)] IN ('350','350A') THEN '350'
WHEN [SpecialtyCode(Function)] IN ('301','301A') THEN '301'
WHEN [SpecialtyCode(Function)] IN ('211','171') THEN '171'
WHEN [SpecialtyCode(Function)] IN ('144','145') THEN '140'
ELSE [SpecialtyCode(Function)]
END AS [SpecialtyCode(Function)],  

CASE WHEN AppointmentTypeNHSCode = 1 Then 'New'
WHEN AppointmentTypeNHSCode = 2 THEN 'Follow Up'
WHEN AppointmentTypeNHSCode IS NULL THEN 'Follow Up'
END AS ApptType,
SUM(CASE WHEN AttendStatus IN ('Refused to Wait','Patient Left / Not seen',
	'Patient Late / Not Seen','Did Not Attend') 
	OR (AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen') 
	AND Outcome = 'Did Not Attend') 
THEN 1 ELSE 0 END) AS DNAs, 

SUM(CASE WHEN AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen')
	AND (outcome IS NULL 
	OR (Outcome NOT LIKE '%canc%' AND Outcome NOT LIKE '%Did Not Attend%')) 
THEN 1 ELSE 0 END) AS Attends,

Total = SUM(CASE WHEN AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen')
	AND (outcome IS NULL 
	OR (Outcome NOT LIKE '%canc%' AND Outcome NOT LIKE '%Did Not Attend%')) 
THEN 1 ELSE 0 END)
+ 
SUM(CASE WHEN AttendStatus IN ('Refused to Wait','Patient Left / Not seen',
	'Patient Late / Not Seen','Did Not Attend') 
	OR (AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen') 
	AND Outcome = 'Did Not Attend') 
THEN 1 ELSE 0 END), 

CAST(

SUM(CASE WHEN AttendStatus IN ('Refused to Wait','Patient Left / Not seen',
	'Patient Late / Not Seen','Did Not Attend') 
	OR (AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen') 
	AND Outcome = 'Did Not Attend') 
THEN 1 ELSE 0 END)
AS decimal) 

/
 
CAST(

SUM(CASE WHEN AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen')
	AND (outcome IS NULL 
	OR (Outcome NOT LIKE '%canc%' AND Outcome NOT LIKE '%Did Not Attend%')) 
THEN 1 ELSE 0 END)
+ 
SUM(CASE WHEN AttendStatus IN ('Refused to Wait','Patient Left / Not seen',
	'Patient Late / Not Seen','Did Not Attend') 
	OR (AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen') 
	AND Outcome = 'Did Not Attend') 
THEN 1 ELSE 0 END) 
AS decimal) AS Percentage, 

--REPLACE(RIGHT(CONVERT(VARCHAR(9), AppointmentDate, 6), 6), ' ', '-') AS MonthYear
--DATENAME(MM,AppointmentDate) + ' ' + CAST(Year(AppointmentDate) AS VARCHAR(4)) AS MonthYear
CONVERT(CHAR(4), AppointmentDate, 100) + CONVERT(CHAR(4), AppointmentDate, 120) AS MonthYear
,Cal.FinancialYear

FROM OP.Schedule OP
LEFT OUTER JOIN dbo.vwExcludeWAWardsandClinics Exc
ON OP.ClinicCode = Exc.SPONT_REFNO_CODE

left join WHREPORTING.LK.Calendar as Cal
on AppointmentDate = Cal.TheDate

WHERE 
IsWardAttender = 0
AND AppointmentDate >= '2011-04-01'
--AND AppointmentDate <= @EndDate
AND Exc.SPONT_REFNO_CODE IS NULL
AND Status IN ('DNA','ATTEND') 
AND (OP.AppointmentTypeNHSCode IN (1, 2) OR OP.AppointmentTypeNHSCode IS NULL)
AND AdministrativeCategory NOT LIKE '%Private%'

GROUP BY 
CASE 
--WHEN [SpecialtyCode(Function)] IN ('340','340G','352','316','317') THEN '340'
WHEN [SpecialtyCode(Function)] IN ('340','340G','352','316') THEN '340'
WHEN [SpecialtyCode(Function)] IN ('350','350A') THEN '350'
WHEN [SpecialtyCode(Function)] IN ('301','301A') THEN '301'
WHEN [SpecialtyCode(Function)] IN ('211','171') THEN '171'
WHEN [SpecialtyCode(Function)] IN ('144','145') THEN '140'
ELSE [SpecialtyCode(Function)]
END,
CASE WHEN AppointmentTypeNHSCode = 1 Then 'New'
WHEN AppointmentTypeNHSCode = 2 THEN 'Follow Up'
WHEN AppointmentTypeNHSCode IS NULL THEN 'Follow Up'
END,
Cal.FinancialYear,

--REPLACE(RIGHT(CONVERT(VARCHAR(9), AppointmentDate, 6), 6), ' ', '-')
--DATENAME(MM,AppointmentDate) + ' ' + CAST(Year(AppointmentDate) AS VARCHAR(4))
CONVERT(CHAR(4), AppointmentDate, 100) + CONVERT(CHAR(4), AppointmentDate, 120)
--ORDER BY [SpecialtyCode(Function)]