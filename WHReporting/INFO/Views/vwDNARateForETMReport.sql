﻿CREATE View [INFO].[vwDNARateForETMReport] AS 
SELECT


--CASE WHEN [SpecialtyCode(Function)] IN ('340','340G','352','316','317') THEN '340'
--WHEN [SpecialtyCode(Function)] IN ('350','350A') THEN '350'
--WHEN [SpecialtyCode(Function)] IN ('301','301A') THEN '301'
--WHEN [SpecialtyCode(Function)] IN ('211','171') THEN '171'
--WHEN [SpecialtyCode(Function)] IN ('144','145') THEN '140'
--ELSE [SpecialtyCode(Function)]
--END AS [SpecialtyCode(Function)], 
OP.Division 
,OP.Directorate
,OP.[SpecialtyCode(Main)] as 'SpecialtyCode'
,Cal.FinancialYear
,Cal.TheMonth as 'ApptMonth'

,CASE WHEN AppointmentTypeNHSCode = 1 Then 'New'
WHEN AppointmentTypeNHSCode = 2 THEN 'Follow Up'
WHEN AppointmentTypeNHSCode IS NULL THEN 'Follow Up'
END AS ApptType,
SUM(CASE WHEN AttendStatus IN ('Refused to Wait','Patient Left / Not seen',
	'Patient Late / Not Seen','Did Not Attend') 
	OR (AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen') 
	AND Outcome = 'Did Not Attend') 
THEN 1 ELSE 0 END) AS DNAs, 

SUM(CASE WHEN AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen')
	AND (outcome IS NULL 
	OR (Outcome NOT LIKE '%canc%' AND Outcome NOT LIKE '%Did Not Attend%')) 
THEN 1 ELSE 0 END) AS Attends,

Total = SUM(CASE WHEN AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen')
	AND (outcome IS NULL 
	OR (Outcome NOT LIKE '%canc%' AND Outcome NOT LIKE '%Did Not Attend%')) 
THEN 1 ELSE 0 END)
+ 
SUM(CASE WHEN AttendStatus IN ('Refused to Wait','Patient Left / Not seen',
	'Patient Late / Not Seen','Did Not Attend') 
	OR (AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen') 
	AND Outcome = 'Did Not Attend') 
THEN 1 ELSE 0 END) 

--CAST(

--SUM(CASE WHEN AttendStatus IN ('Refused to Wait','Patient Left / Not seen',
--	'Patient Late / Not Seen','Did Not Attend') 
--	OR (AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen') 
--	AND Outcome = 'Did Not Attend') 
--THEN 1 ELSE 0 END)
--AS decimal) 

--/
 
--CAST(

--SUM(CASE WHEN AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen')
--	AND (outcome IS NULL 
--	OR (Outcome NOT LIKE '%canc%' AND Outcome NOT LIKE '%Did Not Attend%')) 
--THEN 1 ELSE 0 END)
--+ 
--SUM(CASE WHEN AttendStatus IN ('Refused to Wait','Patient Left / Not seen',
--	'Patient Late / Not Seen','Did Not Attend') 
--	OR (AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen') 
--	AND Outcome = 'Did Not Attend') 
--THEN 1 ELSE 0 END) 
--AS decimal) AS Percentage, 


FROM [WHREPORTING].[OP].[Schedule] OP
		
		LEFT OUTER JOIN dbo.vwExcludeWAWardsandClinics Exc
		ON OP.ClinicCode = Exc.SPONT_REFNO_CODE
		
				LEFT JOIN [WHREPORTING].[LK].[Calendar] Cal
				on Cal.TheDate = OP.AppointmentDate
WHERE 
IsWardAttender = 0
AND AppointmentDate BETWEEN '01 apr 2012' AND 
		(select  distinct cast(max(Cal.TheDate)as DATE)
		FROM LK.Calendar Cal
			where
			Cal.TheMonth = 
			(
			select distinct
			SQ.TheMonth
			  FROM LK.Calendar SQ
			  where SQ.TheDate = 
			 (
			dateadd(MONTH,-1,cast(GETDATE()as DATE))
			)))
AND Exc.SPONT_REFNO_CODE IS NULL
AND Status IN ('DNA','ATTEND') 
AND (OP.AppointmentTypeNHSCode IN (1, 2) OR OP.AppointmentTypeNHSCode IS NULL)
AND AdministrativeCategory NOT LIKE '%Private%'
AND OP.Division in ('Clinical Support','Scheduled Care','Unscheduled Care')


GROUP BY 
OP.Division 
,OP.Directorate
,OP.[SpecialtyCode(Main)] 
,Cal.FinancialYear
,Cal.TheMonth 
,CASE WHEN AppointmentTypeNHSCode = 1 Then 'New'
WHEN AppointmentTypeNHSCode = 2 THEN 'Follow Up'
WHEN AppointmentTypeNHSCode IS NULL THEN 'Follow Up'
END