﻿CREATE View [INFO].[vwDayCaseRate] AS
SELECT  


datename(mm,[DischargeDateTime]) + '-' + datename(yy,[DischargeDateTime]) as DischargeMonth 
      --,S.[AdmissionConsultantCode]
      --,S.[AdmissionConsultantName]
      ,S.[AdmissionDivision]
      ,S.[AdmissionDirectorate]
      ,S.[AdmissionSpecialtyCode]
      ,S.[AdmissionSpecialty]
      ,S.[AdmissionMethod]
      --,S.[AdmissiontWardCode]
      --,S.[AdmissionWard]
      ,case when S.[InpatientStayCode] = 'D'	then 'DayCase'
			when  S.[InpatientStayCode] = 'I'	then 'Inpatient'
			else [InpatientStayCode] end as InpatientStay
      ,S.[AdmissionType]
,count(1) as 'Number of Discharges'
            
      
      
  FROM [WHREPORTING].[APC].[Spell] S
  

  
  where
  
	DischargeDateTime >= '2011-04-01'
	--and

	
	group by 
	datename(mm,[DischargeDateTime]) + '-' + datename(yy,[DischargeDateTime]) 
      --,S.[AdmissionConsultantCode]
      --,S.[AdmissionConsultantName]
      ,S.[AdmissionDivision]
      ,S.[AdmissionDirectorate]
      ,S.[AdmissionSpecialtyCode]
      ,S.[AdmissionSpecialty]
      --,S.[AdmissiontWardCode]
      --,S.[AdmissionWard]
       ,S.[AdmissionMethod]
      ,S.[InpatientStayCode]
      ,S.[AdmissionType]