﻿CREATE View [INFO].[vwNewReviewRatio] AS
SELECT  

CASE 
--WHEN [SpecialtyCode(Function)] IN ('340','340G','352','316','317') THEN '340'
WHEN [SpecialtyCode(Function)] IN ('340','340G','352','316') THEN '340'
WHEN [SpecialtyCode(Function)] IN ('350','350A') THEN '350'
WHEN [SpecialtyCode(Function)] IN ('301','301A') THEN '301'
WHEN [SpecialtyCode(Function)] IN ('211','171') THEN '171'
WHEN [SpecialtyCode(Function)] IN ('144','145') THEN '140'
ELSE [SpecialtyCode(Function)]
END AS [SpecialtyCode(Function)], 

Ratio =
COALESCE(
(
CAST(
SUM(CASE WHEN Status = 'Attend' AND (AppointmentTypeNHSCode = 2 OR AppointmentTypeNHSCode IS NULL)
THEN 1 ELSE 0
END) AS DECIMAL
)

/
  
NULLIF(
CAST(
SUM(CASE WHEN Status = 'Attend' AND AppointmentTypeNHSCode = 1 
THEN 1 ELSE 0 
END) AS DECIMAL
),0)),0),

CONVERT(CHAR(4), OP.AppointmentDate, 100) + CONVERT(CHAR(4), OP.AppointmentDate, 120) AS MonthYear

,Cal.FinancialYear

FROM OP.Schedule AS OP LEFT OUTER JOIN
vwExcludeWAWardsandClinics AS Exc ON OP.ClinicCode = Exc.SPONT_REFNO_CODE

LEFT JOIN WHREPORTING.LK.Calendar as Cal
on OP.AppointmentDate = Cal.TheDate

WHERE (OP.AppointmentDate >= '2011-04-01') 
--AND (OP.AppointmentDate <= @EndDate) 
AND (OP.IsWardAttender = 0) 
AND (Exc.SPONT_REFNO_CODE IS NULL) 
AND (OP.AttendStatus IN ('Attended', 'Attended on Time', 'Patient Late / Seen')) 
AND (OP.AppointmentTypeNHSCode IN (1, 2) OR OP.AppointmentTypeNHSCode IS NULL)

GROUP BY

CASE 
--WHEN [SpecialtyCode(Function)] IN ('340','340G','352','316','317') THEN '340'
WHEN [SpecialtyCode(Function)] IN ('340','340G','352','316') THEN '340'
WHEN [SpecialtyCode(Function)] IN ('350','350A') THEN '350'
WHEN [SpecialtyCode(Function)] IN ('301','301A') THEN '301'
WHEN [SpecialtyCode(Function)] IN ('211','171') THEN '171'
WHEN [SpecialtyCode(Function)] IN ('144','145') THEN '140'
ELSE [SpecialtyCode(Function)]
END,

Cal.FinancialYear,

CONVERT(CHAR(4), OP.AppointmentDate, 100) + CONVERT(CHAR(4), OP.AppointmentDate, 120)