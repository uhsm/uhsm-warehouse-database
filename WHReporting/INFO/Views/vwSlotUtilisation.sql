﻿CREATE View [INFO].[vwSlotUtilisation] AS

SELECT  

  
CASE 
--WHEN TreatmentFunctionCode IN ('340', '340G', '352', '316', '317') THEN '340' 
WHEN TreatmentFunctionCode IN ('340', '340G', '352', '316') THEN '340' 
WHEN TreatmentFunctionCode IN ('350', '350A') THEN '350' 
WHEN TreatmentFunctionCode IN ('301', '301A') THEN '301'
WHEN TreatmentFunctionCode IN ('211', '171') THEN '171'
WHEN TreatmentFunctionCode IN ('144','145') THEN '140'

ELSE TreatmentFunctionCode 
END AS TreatmentFunctionCode, 



CAST(
SUM(BookingsMade) 
AS DECIMAL)

/ 
CAST(
SUM(case when BookingsAllowed=0 then null else BookingsAllowed end) 
AS DECIMAL) AS Percentage, 


CONVERT(CHAR(4), SessionDate, 100) + CONVERT(CHAR(4), SessionDate, 120) AS MonthYear
,Cal.TheMonth
,Cal.FinancialYear

FROM         OP.SessionUtilisation

LEFT JOIN WHREPORTING.LK.Calendar as Cal
on SessionDate = Cal.TheDate

WHERE     
(SessionDate >= '2012-04-01') 
AND (SessionDate <= '2017-03-31') 
AND (SessionIsActive = 'Y') 
AND (IsCancelled = 'N') 
AND (ExcludedClinic = 'N') 
AND (NOT (ClinicCode LIKE '%TELE'))
AND (NOT (ClinicDescription LIKE '%TELE%')) 
AND (SessionStatus IN ('Session Scheduled', 'Session Initiated', 'Session Held')) 
--AND (ProfessionalCarerCode LIKE 'C[0-9]%' OR
--ProfessionalCarerCode LIKE 'D[0-9]%' OR
--ProfessionalCarerCode LIKE 'Di%' OR
--ProfessionalCarerCode LIKE 'Mi%' OR
--ProfessionalCarerCode LIKE 'NURSE%' OR
--ProfessionalCarerCode LIKE 'MANO%' OR
--ProfessionalCarerCode LIKE 'NLG%' OR
--ProfessionalCarerCode LIKE 'NUR%' OR
--ProfessionalCarerCode LIKE 'CLGM%') OR
--(SessionDate >= '2011-04-01') 
--AND (SessionDate <= '2013-03-31') 
--AND (SessionIsActive = 'Y') 
--AND (IsCancelled = 'N') 
--AND (ExcludedClinic = 'N') 
--AND (NOT (ClinicCode LIKE '%TELE%'))
--AND (NOT (ClinicDescription LIKE '%TELE%')) 
--AND (SessionStatus IN ('Session Scheduled', 'Session Initiated', 'Session Held')) 
--AND (TreatmentFunctionCode IN ('560', '654', '650', '652', '840'))

GROUP BY CASE 
--WHEN TreatmentFunctionCode IN ('340', '340G', '352', '316', '317') THEN '340' 
WHEN TreatmentFunctionCode IN ('340', '340G', '352', '316') THEN '340' 
WHEN TreatmentFunctionCode IN ('350', '350A') THEN '350' 
WHEN TreatmentFunctionCode IN ('301', '301A') THEN '301'
WHEN TreatmentFunctionCode IN ('211', '171') THEN '171' 
WHEN TreatmentFunctionCode IN ('144','145') THEN '140'
ELSE TreatmentFunctionCode 
END,
Cal.TheMonth,
Cal.FinancialYear,


CONVERT(CHAR(4), SessionDate, 100) + CONVERT(CHAR(4), SessionDate, 120)