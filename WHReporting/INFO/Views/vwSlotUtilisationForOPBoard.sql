﻿CREATE View [INFO].[vwSlotUtilisationForOPBoard] AS
SELECT

Cal.FinancialYear as  'Fin Year',
Division,
Directorate,
MainSpecialtyCode,

CASE WHEN TreatmentFunctionCode IN ('340', '340G', '352', '316', '317') THEN '340' 
WHEN TreatmentFunctionCode IN ('350', '350A') THEN '350' 
WHEN TreatmentFunctionCode IN ('301', '301A') THEN '301'
WHEN TreatmentFunctionCode IN ('211', '171') THEN '171'
WHEN TreatmentFunctionCode IN ('144','145') THEN '140'
ELSE TreatmentFunctionCode 
END AS TreatmentFunctionCode, 
TreatmentFunction,    
SUM(BookingsAllowed) AS BookingsAllowed, 
SUM(BookingsMade) AS BookingsMade, 
SUM(BookingsFree) AS BookingsFree, 
Cal.TheMonth as MonthYear

FROM         OP.SessionUtilisation

					left join	[WHREPORTING].[LK].[Calendar] Cal
					on SessionDate = Cal.TheDate
					
WHERE (SessionDate >= '2012-04-01') 
AND (SessionDate <= 		(select  distinct cast(max(Cal.TheDate)as DATE)
		FROM LK.Calendar Cal
			where
			Cal.TheMonth = 
			(
			select distinct
			SQ.TheMonth
			  FROM LK.Calendar SQ
			  where SQ.TheDate = 
			 (
			dateadd(MONTH,-1,cast(GETDATE()as DATE))
			)))) 
AND (SessionIsActive = 'Y') 
AND (IsCancelled = 'N') 
AND (ExcludedClinic = 'N') 
AND (NOT (ClinicCode LIKE '%TELE%')) 
AND (NOT (ClinicDescription LIKE '%TELE%')) 
--AND (Division IN (@Division)) 
AND (SessionStatus IN ('Session Scheduled', 'Session Initiated', 'Session Held')) 
AND TreatmentFunctionCode <> 'DQ'
--AND (Directorate IN (@Directorate)) 
--AND (TreatmentFunctionCode IN (@Specialty)) 
--AND (ProfessionalCarerCode LIKE 'C[0-9]%' OR
--ProfessionalCarerCode LIKE 'D[0-9]%' OR
--ProfessionalCarerCode LIKE 'Di%' OR
--ProfessionalCarerCode LIKE 'Mi%' OR
--ProfessionalCarerCode LIKE 'NURSE%' OR
--ProfessionalCarerCode LIKE 'MANO%' OR
--ProfessionalCarerCode LIKE 'NLG%' OR
--ProfessionalCarerCode LIKE 'NUR%' OR
--ProfessionalCarerCode LIKE 'CLGM%') OR
--(SessionDate >= '2011-05-01')
--AND (SessionDate <= '2012-04-30') 
--AND (SessionIsActive = 'Y') 
--AND (IsCancelled = 'N') 
--AND (ExcludedClinic = 'N') 
--AND (NOT (ClinicCode LIKE '%TELE%')) 
--AND (NOT (ClinicDescription LIKE '%TELE%')) 
--AND (Division IN (@Division)) 
--AND (SessionStatus IN ('Session Scheduled', 'Session Initiated', 'Session Held')) 
--AND (Directorate IN (@Directorate)) 
--AND (TreatmentFunctionCode IN ('560', '654', '650', '652', '840')) 
--AND (NOT (ProfessionalCarerCode LIKE '[a-z]%'))
GROUP BY
Division,
Directorate,
MainSpecialtyCode,
CASE WHEN TreatmentFunctionCode IN ('340', '340G', '352', '316', '317') THEN '340' 
WHEN TreatmentFunctionCode IN ('350', '350A') THEN '350' 
WHEN TreatmentFunctionCode IN ('301', '301A') THEN '301'
WHEN TreatmentFunctionCode IN ('211', '171') THEN '171'
WHEN TreatmentFunctionCode IN ('144','145') THEN '140'
ELSE TreatmentFunctionCode 
END, 

Cal.FinancialYear,
TreatmentFunction,
Cal.TheMonth