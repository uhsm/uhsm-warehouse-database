﻿CREATE View [INFO].[vwTrustCancellations] AS

SELECT     

CASE 
--WHEN [SpecialtyCode(Function)] IN ('340','340G','352','316','317') THEN '340'
WHEN [SpecialtyCode(Function)] IN ('340','340G','352','316') THEN '340'
WHEN [SpecialtyCode(Function)] IN ('350','350A') THEN '350'
WHEN [SpecialtyCode(Function)] IN ('301','301A') THEN '301'
WHEN [SpecialtyCode(Function)] IN ('211','171') THEN '171'
WHEN [SpecialtyCode(Function)] IN ('144','145') THEN '140'
ELSE [SpecialtyCode(Function)]
END AS [SpecialtyCode(Function)],


(
CAST(
      SUM(CASE WHEN [Status] = 'HOSPCAN' THEN 1 ELSE 0 END)
      as decimal
      )

/ 
CAST(
      COUNT(EncounterRecno)
      as decimal
      )
)AS [%of1stApptCancelledbyTheTrust],

CONVERT(CHAR(4), AppointmentDate, 100) + CONVERT(CHAR(4), AppointmentDate, 120) AS MonthYear,
Cal.FinancialYear

FROM OP.Schedule 

LEFT OUTER JOIN dbo.vwExcludeWAWardsandClinics Exc
ON OP.Schedule.ClinicCode = Exc.SPONT_REFNO_CODE

LEFT JOIN WHREPORTING.LK.Calendar as Cal
on Cal.TheDate = OP.Schedule.AppointmentDate

WHERE (OP.Schedule.AppointmentDate >= '2011-04-01') 
AND (OP.Schedule.AppointmentDate <= '2017-03-31')
AND Exc.SPONT_REFNO_CODE IS NULL
AND (OP.Schedule.IsWardAttender = 0) 
AND (OP.Schedule.AppointmentType = 'New')
GROUP BY CASE 
--WHEN [SpecialtyCode(Function)] IN ('340','340G','352','316','317') THEN '340'
WHEN [SpecialtyCode(Function)] IN ('340','340G','352','316') THEN '340'
WHEN [SpecialtyCode(Function)] IN ('350','350A') THEN '350'
WHEN [SpecialtyCode(Function)] IN ('301','301A') THEN '301'
WHEN [SpecialtyCode(Function)] IN ('211','171') THEN '171'
WHEN [SpecialtyCode(Function)] IN ('144','145') THEN '140'
ELSE [SpecialtyCode(Function)]
END,
CONVERT(CHAR(4), AppointmentDate, 100) + CONVERT(CHAR(4), AppointmentDate, 120),
Cal.FinancialYear