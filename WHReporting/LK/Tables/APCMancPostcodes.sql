﻿CREATE TABLE [LK].[APCMancPostcodes](
	[Postcode] [nvarchar](255) NULL,
	[SlimPostcode] [nvarchar](255) NULL,
	[Area] [varchar](100) NULL
) ON [PRIMARY]