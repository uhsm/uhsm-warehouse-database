﻿CREATE TABLE [LK].[Calendar](
	[TheDate] [datetime] NOT NULL,
	[DayOfWeek] [varchar](20) NULL,
	[DayOfWeekKey] [int] NULL,
	[LongDate] [varchar](20) NULL,
	[TheMonth] [varchar](20) NULL,
	[FinancialQuarter] [varchar](20) NULL,
	[FinancialYear] [varchar](20) NULL,
	[FinancialMonthKey] [int] NULL,
	[FinancialQuarterKey] [int] NULL,
	[CalendarQuarter] [varchar](20) NULL,
	[CalendarSemester] [varchar](20) NOT NULL,
	[CalendarYear] [varchar](20) NULL,
	[LastCompleteMonth] [int] NOT NULL,
	[WeekNoKey] [varchar](20) NULL,
	[WeekNo] [varchar](20) NULL,
	[FirstDayOfWeek] [datetime] NULL,
	[LastDayOfWeek] [datetime] NULL,
	[FirstDateOfMonth] [datetime] NULL,
	[MonitorQuarter] [varchar](25) NULL,
	[IsWeekEnd] [bit] NULL,
	[IsBankHoliday] [bit] NULL,
	[FinYearMonth] [varchar](14) NULL,
 CONSTRAINT [PK_Calendar] PRIMARY KEY CLUSTERED 
(
	[TheDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]