﻿CREATE TABLE [LK].[CharlsonIndex](
	[ConditionName] [nvarchar](255) NULL,
	[ICDCoding] [nvarchar](255) NULL,
	[Length] [float] NULL,
	[Weight] [float] NULL
) ON [PRIMARY]