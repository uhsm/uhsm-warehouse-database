﻿CREATE TABLE [LK].[CoderSpecMapping](
	[Division] [nvarchar](255) NULL,
	[SpecCode] [nvarchar](255) NULL,
	[SpecName] [nvarchar](255) NULL,
	[Lead] [nvarchar](255) NULL,
	[LeadID] [varchar](100) NULL
) ON [PRIMARY]