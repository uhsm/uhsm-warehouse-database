﻿CREATE TABLE [LK].[DateTime](
	[TheDate] [datetime] NOT NULL,
	[DateAndHour] [datetime] NULL,
	[HOURTIME] [time](7) NULL,
	[HOURINT] [int] NULL,
	[DayOfWeek] [varchar](20) NULL,
	[DayOfWeekKey] [int] NULL,
	[LastDayOfWeek] [datetime] NULL,
	[LongDate] [varchar](20) NULL,
	[TheMonth] [varchar](20) NULL,
	[CalendarYear] [varchar](20) NULL
) ON [PRIMARY]