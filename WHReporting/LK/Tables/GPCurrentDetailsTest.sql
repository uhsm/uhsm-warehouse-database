﻿CREATE TABLE [LK].[GPCurrentDetailsTest](
	[PatientNo] [int] NOT NULL,
	[DistrictNo] [varchar](20) NULL,
	[GPRefno] [varchar](25) NULL,
	[PracticeRefno] [numeric](18, 0) NULL,
	[PracticeCode] [varchar](25) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL
) ON [PRIMARY]