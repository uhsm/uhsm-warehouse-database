﻿CREATE TABLE [LK].[IBASupportSpecialtyLookup_TLA_NHSCode](
	[ADMIT__SPE] [nvarchar](255) NULL,
	[ADMISSION_] [nvarchar](255) NULL,
	[ADMIT__SP2] [nvarchar](255) NULL,
	[NHS_CODE] [nvarchar](255) NULL,
	[NOTES] [nvarchar](255) NULL
) ON [PRIMARY]