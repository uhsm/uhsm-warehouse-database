﻿CREATE TABLE [LK].[ICD-10CCSCategory](
	[ICD-10 CODE] [nvarchar](255) NULL,
	[ICD3Flag] [float] NULL,
	[CCS_CATEGORY] [float] NULL,
	[SHMI_DIAGNOSIS_GROUP] [float] NULL,
	[Diagnosis_Group] [nvarchar](255) NULL,
	[Diagnosis_Chapter] [nvarchar](255) NULL
) ON [PRIMARY]