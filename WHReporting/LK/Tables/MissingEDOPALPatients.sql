﻿CREATE TABLE [LK].[MissingEDOPALPatients](
	[Date] [nvarchar](255) NULL,
	[PtForename] [nvarchar](255) NULL,
	[PtSurname] [nvarchar](255) NULL,
	[RM2] [nvarchar](255) NULL,
	[SourceUniqueID] [nvarchar](255) NULL
) ON [PRIMARY]