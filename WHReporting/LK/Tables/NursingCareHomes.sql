﻿CREATE TABLE [LK].[NursingCareHomes](
	[Home Name] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Postcode] [nvarchar](255) NULL,
	[Nursing/Care] [nvarchar](255) NULL
) ON [PRIMARY]