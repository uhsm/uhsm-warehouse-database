﻿CREATE TABLE [LK].[OPCS_List_v2](
	[OPCS Code ] [nvarchar](255) NULL,
	[OPCS Description] [nvarchar](255) NULL,
	[Chapter] [nvarchar](255) NULL,
	[OPCS4 Version] [float] NULL,
	[GS 100 Flag] [nvarchar](255) NULL,
	[Southern Sector Flag] [nvarchar](255) NULL,
	[SS Endosc] [nvarchar](255) NULL,
	[HT_Flag] [nvarchar](255) NULL,
	[HT 25 Aug] [nvarchar](255) NULL
) ON [PRIMARY]