﻿CREATE TABLE [LK].[OPClinicCodesResp](
	[Clinic Code] [nvarchar](255) NULL,
	[MabsCode] [nvarchar](255) NULL,
	[NumOfApptsSinceJan2013] [float] NULL,
	[SubSpecialty] [nvarchar](255) NULL
) ON [PRIMARY]