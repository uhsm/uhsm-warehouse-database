﻿CREATE TABLE [LK].[OPSSessionEndStatus](
	[SessionEndStatusID] [int] NOT NULL,
	[SessionEndStatusCode] [varchar](25) NULL,
	[SessionEndStatus] [varchar](80) NULL
) ON [PRIMARY]