﻿CREATE TABLE [LK].[OPSSessionSessionStartStatusReason](
	[SessionStartStatusReasonID] [int] NOT NULL,
	[SessionStartStatusReasonCode] [varchar](25) NULL,
	[SessionStartStatusReason] [varchar](80) NULL
) ON [PRIMARY]