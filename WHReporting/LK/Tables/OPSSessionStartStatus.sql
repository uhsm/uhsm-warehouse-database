﻿CREATE TABLE [LK].[OPSSessionStartStatus](
	[SessionStartStatusID] [int] NOT NULL,
	[SessionStartStatusCode] [varchar](25) NULL,
	[SessionStartStatus] [varchar](80) NULL
) ON [PRIMARY]