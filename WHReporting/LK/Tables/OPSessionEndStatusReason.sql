﻿CREATE TABLE [LK].[OPSessionEndStatusReason](
	[SessionEndStatusReasonID] [int] NOT NULL,
	[SessionEndStatusReasonCode] [varchar](25) NULL,
	[SessionEndStatusReason] [varchar](80) NULL
) ON [PRIMARY]