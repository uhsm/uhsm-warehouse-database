﻿CREATE TABLE [LK].[OPSessionStatus](
	[SessionStatusID] [int] NOT NULL,
	[SessionStatusCode] [varchar](25) NULL,
	[SessionStatus] [varchar](80) NULL
) ON [PRIMARY]