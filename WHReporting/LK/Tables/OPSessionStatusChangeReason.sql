﻿CREATE TABLE [LK].[OPSessionStatusChangeReason](
	[SessionStatusChangeReasonID] [int] NOT NULL,
	[SessionStatusChangeReasonCode] [varchar](25) NULL,
	[SessionStatusChangeReason] [varchar](80) NULL
) ON [PRIMARY]