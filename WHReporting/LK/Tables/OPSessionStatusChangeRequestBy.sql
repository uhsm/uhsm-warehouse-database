﻿CREATE TABLE [LK].[OPSessionStatusChangeRequestBy](
	[SessionStatusChangeRequestByID] [int] NOT NULL,
	[SessionStatusChangeRequestByCode] [varchar](25) NULL,
	[SessionStatusChangeRequestBy] [varchar](80) NULL
) ON [PRIMARY]