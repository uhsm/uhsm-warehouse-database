﻿CREATE TABLE [LK].[PASReferenceValue](
	[ReferenceValueCode] [int] NOT NULL,
	[ReferenceDomainCode] [varchar](5) NULL,
	[ReferenceValue] [varchar](80) NULL,
	[MainCode] [varchar](25) NULL,
	[ArchiveFlag] [char](1) NULL,
	[MappedCode] [varchar](50) NULL,
 CONSTRAINT [PK_PASReferenceValue] PRIMARY KEY CLUSTERED 
(
	[ReferenceValueCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]