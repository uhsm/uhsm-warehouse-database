﻿CREATE TABLE [LK].[Practices_To_CCGs](
	[Practice] [nvarchar](255) NULL,
	[Geography Code] [nvarchar](255) NULL,
	[CCG] [nvarchar](255) NULL,
	[CCG Name] [nvarchar](255) NULL
) ON [PRIMARY]