﻿CREATE TABLE [LK].[PriorApprovalProcedures](
	[Code] [int] NULL,
	[Speciality] [nvarchar](255) NULL,
	[OPCSCode] [nvarchar](255) NOT NULL,
	[ProcedureDescription] [nvarchar](255) NULL,
 CONSTRAINT [pk_PriorApprovalProcedures] PRIMARY KEY CLUSTERED 
(
	[OPCSCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]