﻿CREATE TABLE [LK].[RTTSpecialtyExclusions](
	[SpecialtyCode] [numeric](18, 0) NOT NULL,
	[LocalSpecialtyCode] [varchar](20) NULL,
	[Specialty] [varchar](255) NULL
) ON [PRIMARY]