﻿CREATE TABLE [LK].[RadiologyExamTypes](
	[Specialty] [nvarchar](255) NULL,
	[ExamCode1] [nvarchar](255) NULL,
	[ExamDescription] [nvarchar](255) NULL,
	[CC Type] [nvarchar](255) NULL,
	[Exam Type] [nvarchar](255) NULL,
	[Exam SubType] [nvarchar](255) NULL,
	[F7] [nvarchar](255) NULL
) ON [PRIMARY]