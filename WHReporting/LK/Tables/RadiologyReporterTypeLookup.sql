﻿CREATE TABLE [LK].[RadiologyReporterTypeLookup](
	[ReportedByName] [nvarchar](255) NULL,
	[Reporter Type] [nvarchar](255) NULL
) ON [PRIMARY]