﻿CREATE TABLE [LK].[SpecDivDependencies](
	[ColDatabaseName] [varchar](400) NULL,
	[ColSchemaName] [varchar](400) NULL,
	[ColTableName] [varchar](400) NULL,
	[ColSearchString] [varchar](400) NULL,
	[databaseId] [smallint] NULL,
	[databaseName] [varchar](50) NULL,
	[objectId] [int] NULL,
	[schemaName] [varchar](50) NULL,
	[objectName] [varchar](100) NULL,
	[objectType] [varchar](50) NULL,
	[objectDef] [varchar](max) NULL
) ON [PRIMARY]