﻿CREATE TABLE [LK].[SpecialtyDivision](
	[SpecialtyRefno] [int] NOT NULL,
	[SpecialtyCode] [varchar](20) NULL,
	[Specialty] [varchar](80) NULL,
	[SpecialtyFunctionCode] [varchar](20) NULL,
	[SpecialtyFunction] [varchar](80) NULL,
	[MainSpecialtyCode] [varchar](20) NULL,
	[MainSpecialty] [varchar](80) NULL,
	[RTTSpecialtyCode] [varchar](20) NULL,
	[RTTExcludeFlag] [varchar](1) NULL,
	[Direcorate] [varchar](80) NULL,
	[Division] [varchar](80) NULL
) ON [PRIMARY]