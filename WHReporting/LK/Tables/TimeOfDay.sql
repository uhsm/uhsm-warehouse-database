﻿CREATE TABLE [LK].[TimeOfDay](
	[TimeKey] [int] IDENTITY(1,1) NOT NULL,
	[TimeValue] [time](0) NULL,
	[TimeTextValue] [varchar](8) NULL,
	[HourValue] [char](2) NULL,
	[MinuteValue] [char](2) NULL,
	[HourBand] [varchar](13) NULL,
	[HalfHourBand] [varchar](5) NULL,
	[QuarterHourBand] [varchar](5) NULL,
	[PartOfDay] [varchar](50) NULL,
 CONSTRAINT [pk_TimeOfDay] PRIMARY KEY CLUSTERED 
(
	[TimeKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]