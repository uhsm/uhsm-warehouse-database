﻿CREATE TABLE [LK].[WardDischargeTargets](
	[Ward] [nvarchar](255) NULL,
	[Weekday target] [float] NULL,
	[Before 9am target] [float] NULL,
	[Before 11am target] [float] NULL,
	[Saturday target] [float] NULL,
	[Sunday target] [float] NULL
) ON [PRIMARY]