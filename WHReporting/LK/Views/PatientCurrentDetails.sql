﻿CREATE VIEW [LK].[PatientCurrentDetails] as

select
	SourcePatientNo
	,FacilityID
	,NHSNo
	,Sex
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,CurrentRegisteredGPCode
	,CurrentRegisteredGPName
	,CurrentRegisteredPracticeCode
	,CurrentRegisteredGPPractice
	,GPPracticeAddress1
	,GPPracticeAddress2
	,GPPracticeAddress3
	,GPPracticeAddress4
	,GPPracticeAddress5
	,GPPracticePostcode
	,CCGCodeOfGPPractice
	,CCGNameofPractice
	,NHSNumberStatusCode
from
	WH.LK.PatientCurrentDetails
