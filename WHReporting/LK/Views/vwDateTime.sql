﻿--USE [WHREPORTING]
--GO

--/****** Object:  View [SC].[APCSpecialtyFunctionCorrection]    Script Date: 12/12/2012 15:56:03 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

/*
--Author: N Scattergood
--Date created: 26/06/2013
--View for calendar lookup table with 24 entries for every date (1 for each Hour)
--Includes last 4 years 
--Used in Bed Modelling Report
*/

CREATE VIEW [LK].[vwDateTime]

as


(Select * from
((
SELECT 
[TheDate]
,[TheDate] as DateAndHour
,CAST([TheDate] AS TIME) AS [HOURTIME]
,DATEPART(hour,[TheDate]) AS [HOURINT]
	
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	)) Union All
	-------1AM---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,1,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,1,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,1,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	)) UNION ALL
	-------2AM---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,2,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,2,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,2,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))
	UNION ALL
	-------3AM---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,3,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,3,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,3,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))
	UNION ALL
	-------4AM---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,4,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,4,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,4,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))
	UNION ALL
	-------5AM---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,5,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,5,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,5,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))
	UNION ALL
	-------6AM---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,6,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,6,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,6,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))
	UNION ALL
	-------7AM---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,7,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,7,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,7,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))UNION ALL
	-------8AM---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,8,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,8,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,8,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))
	UNION ALL
	-------9AM---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,9,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,9,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,9,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))UNION ALL
	-------10AM---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,10,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,10,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,10,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))UNION ALL
	-------11AM---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,11,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,11,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,11,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))UNION ALL
	-------12Noon---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,12,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,12,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,12,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))UNION ALL
	-------1PM---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,13,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,13,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,13,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))UNION ALL
	-------2PM---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,14,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,14,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,14,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))UNION ALL
	-------3PM---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,15,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,15,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,15,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))
	UNION ALL
	-------4PM---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,16,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,16,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,16,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))UNION ALL
	-------5PM---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,17,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,17,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,17,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))UNION ALL
	-------6PM---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,18,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,18,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,18,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))UNION ALL
	-------7PM---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,19,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,19,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,19,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))UNION ALL
	-------8PM---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,20,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,20,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,20,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))UNION ALL
	-------9PM---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,21,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,21,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,21,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))UNION ALL
	-------10PM--------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,22,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,22,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,22,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))UNION ALL
	------11PM---------
	(
SELECT 
[TheDate]
,DATEADD(HOUR,23,[TheDate]) as DateAndHour
,CAST((DATEADD(HOUR,23,[TheDate])) AS TIME) AS [HOURTIME]
,DATEPART(hour,(DATEADD(HOUR,23,[TheDate]))) AS [HOURINT]
      ,[DayOfWeek]
      ,[DayOfWeekKey]	,[LastDayOfWeek]
      ,[LongDate]
      ,[TheMonth]

      ,[CalendarYear]

  FROM [WHREPORTING].[LK].[Calendar] Cal
  where  
Cal.CalendarYear in 
(select distinct
SQ.CalendarYear
  FROM [WHREPORTING].LK.Calendar SQ
  where SQ.TheDate between 
	(dateadd(year,-3,cast(GETDATE()as DATE)))
and
	(dateadd(day,-1,cast(GETDATE()as DATE)))
	))
	
	) AS U)