﻿/* =============================================
-- Author:		CMan
-- Create date: 07/11/2014
-- Description:	Stored procedure for generating report which looks to determine SSRS Current Lodgers from Live LPI ADT data 
-- Dependancies:   [UHSMApplications].[LPI].[ADTData]	
					WHReporting.LK.WardtoDivision
					WHReporting.LK.SpecialtyDivision	

-- =============================================*/
CREATE PROCEDURE [LPI].[ReportCurrentLodgers_PatientLevel] @Specialty varchar (max), @Location varchar(max), @Division varchar(100)
AS

/*
 Modified Date				Modified By			Modification
-----------------			------------		------------
07/11/2014					CMan				Created

*/





BEGIN



--Temp table to detmine the latest consultant episode record for any spells which have yet to be discharged using Max(EventDateTime)
If OBJECT_ID('tempdb..#MaxEvent')> 0
Drop table  #MaxEvent
Select  ADT.FACIL_ID
		,ADT.SourceSpellID
		,ADT.AdmissionDateTime
		,ADT.DischargeDateTime
		,ADT.AdmissionMethod
		,ADT.DischargeMethod
		,ADT.SourceOfReferral
		,ADT.Specialty
		,ADT.AssignedPatientLocation
		,ADT.AdmissionType
		,MAX(ADT.EventDateTime) as Max_EventDateTime --to get the latest event entry

into #MaxEvent
From [UHSMApplications].[LPI].[ADTData] ADT
where not exists
(SELECT  SourceSpellID
  FROM [UHSMApplications].[LPI].[ADTData] DIS
  where EventType = 'Discharge'
  and ADT.SourceSpellID = DIS.SourceSpellID)--criteria added here to only include spells which have not yet been discharged
 and EventType  in ('CONS/SPEC TRANSFER', 'ADMISSION')--only include events that are either an admission or a consultant transfer as this will tell us the latest specialty which the patient is under the care of and then we should be able to use the  assigned location to determine the wards which the patient is currently on
 Group by ADT.FACIL_ID
		,ADT.SourceSpellID
		,ADT.AdmissionDateTime
		,ADT.DischargeDateTime
		,ADT.AdmissionMethod
		,ADT.DischargeMethod
		,ADT.SourceOfReferral
		,ADT.Specialty
		,ADT.AssignedPatientLocation
		,ADT.AdmissionType
 Order by ADT.SourceSpellID
 
 
---temp table to match the latest record from the temp table above back to LPI.ADTData to get the details of that latest episode event
 
If OBJECT_ID('tempdb..#Live')> 0
Drop table #Live
 Select distinct ADT.FACIL_ID
		,ADT.SourceSpellID
		,ADT.AdmissionDateTime
		,ADT.DischargeDateTime
		,ADT.AdmissionMethod
		,ADT.DischargeMethod
		,ADT.SourceOfReferral
		,ADT.Specialty--this is the speciality which the patient is currently under
		,ADT.AssignedPatientLocation--this is the ward which the patient is currently on
		,ADT.AdmissionType
		,ADT.EventDateTime
		,ADT.Location--this is the ward which the patient was at time of this event 
		,ADT.EventSpecialty--This is the Specialty of the event row - for the latest episode this should be the same as the Specialty
		,wd.Division as WardDivision
		,sp.Specialty as SpecialtyDesc
		,sp.Division as EventDivision
		, case when sp.Division = 'Unscheduled Care'  AND wd.Division = 'Scheduled Care'  AND ADT.EventSpecialty<> '340' then 'Outlier'
				when sp.Division = 'Unscheduled Care'  AND wd.Division = 'Scheduled Care'  AND ADT.EventSpecialty = '340' AND ADT.Location IN ('F1N', 'F5', 'F6','F2', 'F1') then 'Outlier'
				when  sp.Division = 'Scheduled Care' AND wd.Division  = 'Unscheduled Care' then 'Outlier'
				 when  sp.Division = 'Clinical Support' AND wd.Division  IN ('Unscheduled Care', 'Scheduled Care')then 'Outlier'
		Else 'Not Outlier'
		End as [Outlier]--Logic for determining what is a outlier and what is not an outlier is taken from the Lodgers Report SSRS rpeort
		,ADT.UpdatedDate -- Date which the last pull from LPI was taken
		,cons.ConsultantName 
		,pat.PatientSurname
into #Live
 From [UHSMApplications].[LPI].[ADTData] ADT
inner join #MaxEvent on ADT.SourceSpellID = #MaxEvent.SourceSpellID and ADT.EventDateTime = #MaxEvent.Max_EventDateTime -- join above temp table back to LPI.ADTData to get the details of the latest event from the temp table above
Left outer join WHREPORTING.LK.WardtoDivision wd on ADT.AssignedPatientLocation COLLATE Latin1_General_CI_AS = wd.Ward--join the patient's current location to ward look up to determine the division of the ward
Left outer join WHREPORTING.LK.SpecialtyDivision sp on ADT.Specialty COLLATE Latin1_General_CI_AS = sp.SpecialtyCode --join the patient's current specialty to specialty look to determine the division of the specialty
Left outer join WHREPORTING.LK.ConsultantSpecialty cons on ADT.Consulting_Doctor = cons.ConsultantCode
Left outer join WHREPORTING.LK.PatientCurrentDetails pat on ADT.Facil_ID = pat.FacilityID
where wd.Division <> sp.Division--outliers are determined to be those where the current specialty which the patient is under the care of is sits under a division which is different to the division of the Ward
and 
ADT.AssignedPatientLocation in ('AMU','A7','A9', 'F4N', 'F4', 'F15S', 'F15','F7','F10','F11','F12','F14','CDU', 'EAU','DYL','PEA','POU','WIL','A2','A1','A4','A5','A6','F1S','F3','F9','BUR','SAU','F16','JIM','F1','F2','F5','F6','A3', 'F1N','F3PLAS','PITU')--only interested in patients who are currently sitting on these wards - logic taken from the SSRS report 'Lodgers Report'.  Although I ahve changed A10 and A8 to AMU instead.


--Query for the report
----patient level
Select #Live.FACIL_ID
		,#Live.SourceSpellID
		,#Live.AdmissionDateTime
		,#Live.DischargeDateTime
		,#Live.AdmissionMethod
		,#Live.DischargeMethod
		,#Live.SourceOfReferral
		,#Live.Specialty
		,#Live.AssignedPatientLocation
		,#Live.AdmissionType
		,#Live.EventDateTime
		,#Live.Location
		,#Live.EventSpecialty
		,#Live.WardDivision
		,#Live.SpecialtyDesc
		,#Live.EventDivision
		,#Live.Outlier
		,#Live.UpdatedDate
		,#Live.ConsultantName
		,#Live.PatientSurname
from #Live
where Outlier = 'Outlier'--criteria as only want to view the outliers in the report
and (SpecialtyDesc in (Select Item from dbo.split(@Specialty,','))  or @Specialty is null)
and (AssignedPatientLocation Collate Latin1_General_CI_AS  in (Select Item from  dbo.Split(@Location,','))  or @Location is null)
and (EventDivision in  (Select Item from dbo.split(@Division,',')) or @Division is NULL)

----summary
--Select 	count(#Live.SourceSpellID) as LodgersCount
--		,#Live.AssignedPatientLocation
--		,#Live.WardDivision
--		,#Live.SpecialtyDesc
--		,#Live.EventDivision
--		,#Live.UpdatedDate

--from #Live
--where Outlier = 'Outlier'--criteria as only want to view the outliers in the report
--Group by #Live.WardDivision
--		,#Live.SpecialtyDesc
--		,#Live.EventDivision
--		,#Live.AssignedPatientLocation
--		,#Live.UpdatedDate



END