﻿create view [LRD].[CurrentWard] as 
select 
	SourceUniqueID = SPONT_REFNO
	,WardCode = SPONT_REFNO_CODE
	,WardName = SPONT_REFNO_NAME
	,DateFrom = START_DTTM
from WH.PAS.ServicePointBase 
where SPTYP_REFNO = 1520
and ARCHV_FLAG = 'N'
and END_DTTM is null