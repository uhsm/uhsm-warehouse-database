﻿create view [LRD].[Specialty] as

select 
	SourceUniqueID = spec.SPECT_REFNO
	,SpecialtyCode = spec.MAIN_IDENT
	,SpecialtyName = spec.[DESCRIPTION]
	--,OWNER_HEORG_REFNO_MAIN_IDENT
from WH.PAS.SpecialtyBase spec
where spec.OWNER_HEORG_REFNO in (
	2002118		--RM2
	,2001932	--SHA
	,2001934	--ORG
	)
and spec.ARCHV_FLAG = 'N'
and spec.END_DTTM is null
and spec.DIVSN_REFNO_DESCRIPTION = 'Specialty'
--order by spec.DIVSN_REFNO_DESCRIPTION, MAIN_IDENT