﻿-- =============================================
-- Author:	CMan
-- Create date: 18/08/2014
-- Description:	Used in SSRS report for patient level to give the average waits for the various time spans during a radilogy requests i.e. Request to Accept, Accept to Performance and Performance to Reported.

-- =============================================
CREATE PROCEDURE [MISYS].[ReportRadiologyWaitingTimeSpans] @start datetime, @end datetime, @Modality varchar(max), @Priority varchar(max), @PatCat varchar(100)


WITH RECOMPILE
AS
BEGIN
	SET NOCOUNT ON;

SET DATEFORMAT DMY;

--Declare @start datetime,
--@end datetime
--,@Modality varchar(50)
--,@Priority varchar(50)
--,@PatCat varchar(50)
--Set @start = '20140701'
--Set @end = '20140817'
--Set @Modality = 'X'
--set @Priority = 'Rout'
--set @PatCat = 'IP'



If OBJECT_ID('tempdb..#Orders') > 0
Drop table #Orders

Select  

	--bOrder.PatientNo
	--, 'RM2' + bOrder.PatientNo as RM2Number
	distinct bOrder.OrderNumber as NumberOfRequests
	--, bOrder.OrderingPhysician as RequestingClinicianCode
	--, OrderingPhysician.phys_name as RequestingClinician
	, castyp.CaseTypeCode as ModalityCode
	 , castyp.CaseType as Modality 
	--, bOrder.OrderTime as RequestDate
	--, bOrder.ScheduleCreateTime as AcceptDate
	--, bOrder.PerformanceTime as TestDate
	--, bOrder.FinalDate 
	--, bOrder.FiledDate
	--, bOrder.FiledTime as ReportDate
	, bOrder.PriorityCode as Priority
	,pc.Description as PriorityDescription
	--, bOrder.OrderingLocationCode 
	--, loc.Location as OrderingLocation
	--, loc.LocationCategoryCode
	,Case when loc.loc_type_trans = 'ER' then 'A&E'
		when loc.loc_type_trans = 'IP' and SourceSpellNo is not null then 'IP'
		Else 'OP'
		End as DerivedPatientCategory 
	, bOrder.PatientTypeCode
	, DATEDIFF(HOUR, OrderTime, ISNULL(ScheduleCreateTime, OrderTime))  as RequestToBooking
	, DATEDIFF(HOUR, Isnull(ScheduleCreateDate, OrderTime), PerformanceTime) as BookingToPerform
	, DATEDIFF(HOUR, PerformanceTime, case when Isnumeric(FiledTime) = 0 then FiledDate Else  dateadd(minute, cast(right(filedtime,2) as int),dateadd(hour, cast(left(filedtime,2) as int),FiledDate))  End) as PerformToReport
	, DATEDIFF(HOUR, OrderTime, case when Isnumeric(FiledTime) = 0 then FiledDate Else  dateadd(minute, cast(right(filedtime,2) as int),dateadd(hour, cast(left(filedtime,2) as int),FiledDate))  End)  as RequestToReport
	,DATEDIFF(HOUR, OrderTime, ISNULL(ScheduleCreateTime, OrderTime))/ cast (24 as float)  as DaysRequestToBooking
	, DATEDIFF(HOUR, Isnull(ScheduleCreateDate, OrderTime), PerformanceTime)/ cast (24 as float) as DaysBookingToPerform
	, DATEDIFF(HOUR, PerformanceTime, case when Isnumeric(FiledTime) = 0 then FiledDate Else  dateadd(minute, cast(right(filedtime,2) as int),dateadd(hour, cast(left(filedtime,2) as int),FiledDate))  End)/ cast (24 as float) as DaysPerformToReport
	, DATEDIFF(HOUR, OrderTime, case when Isnumeric(FiledTime) = 0 then FiledDate Else  dateadd(minute, cast(right(filedtime,2) as int),dateadd(hour, cast(left(filedtime,2) as int),FiledDate))  End)/ cast (24 as float) as DaysRequestToReport
	, bOrder.ExamDescription
	, bOrder.ExamCode1
into #Orders
--select case when Isnumeric(FiledTime) = 0 then FiledDate 
-- not like '%[^0-9]%' then FiledDate 

--like '%%' then FiledDate 
--			when FiledTime like '%[a-z]%' then FiledDate
--			when FiledTime like '%;%' then FiledDate 
--				when FiledTime like '%/%' then FiledDate 
--			when FiledTime like	 '%0%' then FiledDate 
			--Else  dateadd(minute, cast(right(filedtime,2) as int),dateadd(hour, cast(left(filedtime,2) as int),FiledDate))  End , fileddate,filedtime 
from WH.MISYS.[Order] as bOrder
Left outer join WH.MISYS.DictPatientLocation  as loc on bOrder.OrderingLocationCode = loc.pat_loc_code
Left outer join WH.MISYS.CaseType castyp on bOrder.CaseTypeCode = castyp.CaseTypeCode
Left outer join WH.MISYS.DictPhysician  as OrderingPhysician on  bOrder.OrderingPhysicianCode = OrderingPhysician.phys_code
Left outer join WHREPORTING.APC.Spell sp on 'RM2' + bOrder.PatientNo = sp.FaciltyID and (PerformanceDate >= sp.AdmissionDate and PerformanceTime < isnull(sp.DischargeDateTime, GETDATE()))-- and (OrderDate >= sp.AdmissionDate and OrderTime < isnull(sp.DischargeDateTime,GETDATE())  )
Left outer join WH.MISYS.DictPriorityCodes pc on bOrder.PriorityCode = pc.priority_code
where OrderDate >= @start
and OrderDate <= @end
and PerformanceDate < GETDATE()

--and   DerivedPatientCategory in (SELECT Item 
--						 FROM   dbo.Split (@PatCat,','))                    



Select COUNT(NumberOfRequests) as NumberOfRequests,
ModalityCode,
Modality,
Priority,
PriorityDescription,
DerivedPatientCategory,
Sum(RequestToBooking) as RequestToBooking,
Sum(BookingToPerform) as BookingToPerform,
Sum(PerformToReport) as PerformToReport,
Sum(RequestToReport) as RequestToReport,
Sum(DaysRequestToBooking) as DaysRequestToBooking,
Sum(DaysBookingToPerform) as DaysBookingToPerform,
Sum(DaysPerformToReport) as DaysPerformToReport,
Sum(DaysRequestToReport) as DaysRequestToReport

from #Orders

where  ModalityCode in (SELECT Item
                         FROM   dbo.Split (@Modality, ','))
and  Priority in  (SELECT Item
                         FROM   dbo.Split (@Priority, ','))
and     DerivedPatientCategory in (SELECT Item 
						 FROM   dbo.Split (@PatCat,','))                     

Group by ModalityCode,
Modality,
Priority,
PriorityDescription,
DerivedPatientCategory



 --bOrder.PriorityCode,  castyp.CaseType , castyp.CaseTypeCode ,Case when loc.loc_type_trans = 'ER' then 'A&E'
	--	when loc.loc_type_trans = 'IP' and SourceSpellNo is not null then 'IP'
	--	Else 'OP'
	--	End, pc.Description

--OPTION (OPTIMIZE FOR UNKNOWN) 



END