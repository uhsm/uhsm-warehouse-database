﻿-- =============================================
-- Author:	CMan
-- Create date: 18/08/2014
-- Description:	Used in SSRS report for patient level to give the average waits for the various time spans during a radilogy requests i.e. Request to Accept, Accept to Performance and Performance to Reported.

-- =============================================
CREATE PROCEDURE [MISYS].[ReportRadiologyWaitingTimeSpans_PatientLevel] @start datetime, @end datetime, @Modality varchar(4000), @Priority varchar(4000), @PatCat varchar(100)

AS
BEGIN


SET NOCOUNT ON;

SET DATEFORMAT DMY; 



--Declare @start datetime,
--@end datetime
--,@Modality varchar(50)
--,@Priority varchar(50)
--,@PatCat varchar(50)
--Set @start = '20140701'
--Set @end = '20140824'
--Set @Modality = 'X'
--set @Priority = 'Rout'
--set @PatCat = 'IP'





--If OBJECT_ID('tempdb..#Orders') > 0
--Drop table #Orders

Select Distinct  
Case when loc.loc_type_trans = 'ER' then 'A&E'
		when loc.loc_type_trans = 'IP' and SourceSpellNo is not null then 'IP'
		Else 'OP'
		End as DerivedPatientCategory
	,sp.AdmissionWard
	,bOrder.PatientNo
	, 'RM2' + bOrder.PatientNo as RM2Number
	, bOrder.OrderNumber
	, bOrder.OrderingPhysician as RequestingClinicianCode
	, OrderingPhysician.phys_name as RequestingClinician
	 , castyp.CaseType as Modality
	, bOrder.OrderTime as RequestDate
	, bOrder.ScheduleCreateTime as AcceptDate
	, bOrder.PerformanceTime as TestDate
	, bOrder.FinalDate 
	, bOrder.FiledDate
	,case when Isnumeric(FiledTime) = 0 then FiledDate Else  dateadd(minute, cast(right(filedtime,2) as int),dateadd(hour, cast(left(filedtime,2) as int),FiledDate))  End as ReportDate
	--, bOrder.FiledTime as ReportDate
	, bOrder.PriorityCode as Priority
	, pc.Description as PriorityDescription
	, bOrder.OrderingLocationCode 
	, loc.loc_desc as OrderingLocation
	,loc_type_trans as OrderingLocationType
	, bOrder.PatientTypeCode
	, DATEDIFF(hour, OrderTime,ISNULL(ScheduleCreateTime, OrderTime))  as RequestToBooking
	, DATEDIFF(hour, Isnull(ScheduleCreateDate, OrderTime), PerformanceTime) as BookingToPerform
	, DATEDIFF(hour, PerformanceTime, case when Isnumeric(FiledTime) = 0 then FiledDate Else  dateadd(minute, cast(right(filedtime,2) as int),dateadd(hour, cast(left(filedtime,2) as int),FiledDate))  End) as PerformToReport
	,DATEDIFF(hour, OrderTime, case when Isnumeric(FiledTime) = 0 then FiledDate Else  dateadd(minute, cast(right(filedtime,2) as int),dateadd(hour, cast(left(filedtime,2) as int),FiledDate))  End)  as RequestToReport
	,DATEDIFF(HOUR, OrderTime, ISNULL(ScheduleCreateTime, OrderTime))/ cast (24 as float)  as DaysRequestToBooking
	, DATEDIFF(HOUR, Isnull(ScheduleCreateDate, OrderTime), PerformanceTime)/ cast (24 as float) as DaysBookingToPerform
	, DATEDIFF(HOUR, PerformanceTime, case when Isnumeric(FiledTime) = 0 then FiledDate Else  dateadd(minute, cast(right(filedtime,2) as int),dateadd(hour, cast(left(filedtime,2) as int),FiledDate))  End)/ cast (24 as float) as DaysPerformToReport
	, DATEDIFF(HOUR, OrderTime, case when Isnumeric(FiledTime) = 0 then FiledDate Else  dateadd(minute, cast(right(filedtime,2) as int),dateadd(hour, cast(left(filedtime,2) as int),FiledDate))  End)/ cast (24 as float) as DaysRequestToReport
	, bOrder.ExamDescription
	, bOrder.ExamCode1

from WH.MISYS.[Order] as bOrder
Left outer join WH.MISYS.DictPatientLocation  as loc on bOrder.OrderingLocationCode = loc.pat_loc_code
Left outer join WH.MISYS.CaseType castyp on bOrder.CaseTypeCode = castyp.CaseTypeCode
Left outer join WH.MISYS.DictPhysician  as OrderingPhysician on  bOrder.OrderingPhysicianCode = OrderingPhysician.phys_code
Left outer join WHREPORTING.APC.Spell sp on 'RM2' + bOrder.PatientNo = sp.FaciltyID and (PerformanceDate >= sp.AdmissionDate and PerformanceTime < isnull(sp.DischargeDateTime, GETDATE()))-- and (OrderDate >= sp.AdmissionDate and OrderTime < isnull(sp.DischargeDateTime,GETDATE())  )
Left outer join WH.MISYS.DictPriorityCodes pc on bOrder.PatientTypeCode = pc.priority_code
where OrderDate >=  @start
and OrderDate <= @end
and PerformanceDate< GETDATE()
--and OrderNumber = '14C17937'
and 
bOrder.CaseTypeCode in (SELECT Item
                         FROM   dbo.Split (@Modality, ','))
and  bOrder.PriorityCode in  (SELECT Item
                         FROM   dbo.Split (@Priority, ','))
and   Case when loc.loc_type_trans = 'ER' then 'A&E'
		when loc.loc_type_trans = 'IP' and SourceSpellNo is not null then 'IP'
		Else 'OP'
		End in (SELECT Item 
						 FROM   dbo.Split (@PatCat,','))                     


Order by bOrder.OrderTime


END