﻿/*
--Author: N Scattergood
--Date created: 20/02/2013
--Stored Procedure Built for SRSS Report on:
--Medical Model for A8 and A10
--Provides drillthrough details on 30 day Emergency Readmissions
*/

CREATE Procedure [MM].[ReportMedicalModelAMUDashboard_DrillThrough30Days]

@StartDate as Date
,@EndDate as Datetime
,@WARDPar as nvarchar(100)
,@MonthPar as nvarchar (8)


as
SELECT 
Distinct
      S.[SourceSpellNo]
      ,S.FaciltyID
      ,S.NHSNo
      ,SQ.WardCohort
      ,Cal.TheMonth		as	DischargeMonth
       ,SQ.FirstStayOnWardStart
      ,SQ.LastStayOnWardEnd
  --    ,DATEDIFF(dd,SQ.FirstStayOnWardStart,SQ.LastStayOnWardEnd) as 'DayDiff(<>WardStay)'

	--,WLOS.WardStayStart
 --     ,WLOS.WardStayEnd
 --     ,DATEDIFF(DAY,WardStayStart,WardStayEnd) AS WardLOS
 
  --    ,case 
  --    when 
  --    S.DischargeMethodNHSCode = 4
  --    and coalesce(SQ.DischargeLoungeEndTime,SQ.LastStayOnWardEnd) = S.DischargeDateTime 
  --    then 'Patient Died on Ward'
  --    when
  --    WLOS.WardStayEnd - WLOS.WardStayStart < '1900-01-01 12:00:00' 
  --    then '0-12 Hours'
  --    when
  --    WLOS.WardStayEnd - WLOS.WardStayStart < '1900-01-02'
  --    then '12-24 Hours'
  --    when
  --    WLOS.WardStayEnd - WLOS.WardStayStart < '1900-01-04'
  --    then '<72Hours'
		--else '72+'
		--end as LoSBand
      
      
      
  --    --,[AdmissionDate]
  --    --,[AdmissionTime]
      ,S.[AdmissionDateTime]
  --    --,[DischargeDate]
  --    --,S.[DischargeTime]
      
      ,S.[DischargeDateTime]
      	,S.LengthOfSpell 
	,E.[Specialty(Function)] as DischargeSpecialty
	,CC.PriamryProcedure as PrimaryProcedureOnDischarge
	,CC.PrimaryDiagnosis as PrimaryDiagnosisOnDischarge
      --,SQ.DischargeLoungeEndTime
      --,case
      --when S.DischargeMethodNHSCode = 4
      --and coalesce(SQ.DischargeLoungeEndTime,SQ.LastStayOnWardEnd) = S.DischargeDateTime 
      --then 1
      --else 0
      --end as 'PatientDied'


      ,1 as DataCount
      --,case
      --when
      --coalesce(SQ.DischargeLoungeEndTime,SQ.LastStayOnWardEnd) = S.DischargeDateTime 
      --then 0
      --else 1
      --end as StayedOnWardLOSDenominator
      
   --   ,case
   --   when S.DischargeMethodNHSCode = 4
   --   and coalesce(SQ.DischargeLoungeEndTime,SQ.LastStayOnWardEnd) = S.DischargeDateTime 
   --   then 0
   --   when coalesce(SQ.DischargeLoungeEndTime,SQ.LastStayOnWardEnd) = S.DischargeDateTime 
	  --then 1
   --   else 0
   --   end as DischargeDenominator
      ,Cal.[DayOfWeek]	as DischargeDay
      --,case
      --when S.DischargeMethodNHSCode = 4
      --then 0
      --when
      --(S.DischargeWardCode in (SELECT Val from dbo.fn_String_To_Table(@WARDPar,',',1)) or SQ.DischargeLoungeEndTime = S.DischargeDateTime)
      --and Cal.DayOfWeekKey	in (6,7)
      --then 1
      --else 0
      --end as WeekendDischarge
      --,case
      --when S.DischargeMethodNHSCode = 4
      --then 0
      --when DATEPART(hour,S.[DischargeDateTime]) < 12
      --then 1
      --when S.[DischargeDateTime] is Null
      --then 0
      --else 0
      --end as AMDischarge

  
      
      --,[AdmissionConsultantCode]
      --,[AdmissionConsultantName]
      --,[AdmissionDivision]
      --,[AdmissionDirectorate]

      --,[AdmissionSpecialtyCode(Function)]
      --,[AdmissionSpecialty(Function)]
      ,S.[AdmissiontWardCode] as AdmissionWardCode
      --,[AdmissionWard]

		,case when S.[AdmissionType] in ('Emergency','Non-Elective') then 'Non-Elective'
		else S.[AdmissionType] end as 'AdmissionMethod' 
		
		      ,case when S.[InpatientStayCode] = 'D'	then 'DayCase'
			when  S.[InpatientStayCode] = 'I'	then 'Inpatient'
			when  S.[InpatientStayCode] = 'B'	then 'Baby'
			else S.[InpatientStayCode] end as InpatientStay   
			 
      ,S.[DischargeWardCode]
      --,[DischargeWard]
      ----,[DischargeMethodCode]
      --,S.[DischargeMethod]
      --,S.[DischargeMethodNHSCode]
      --,[DischargeDestinationCode]
      --,S.[DischargeDestination]
      --,S.[DischargeDestinationNHSCode]
	  --,S.DischargeDate
,RR.*
		--,SQ.BedOutlier
      --,Case
      --when AE.ArrivalDate is not null 
      --then 1
      --else 0
      --end as 'AEReattender'

		

  FROM [WHREPORTING].[APC].[Spell] S
  
  -------Following Join restricts to patients on our Selected Ward------
  -------Where the Ward Start Time is in the Selected period-------
  -------In addition it also looks at whether the patient was a bed outlier at any point------
	inner join 
	(select distinct
	Ward.SourceSpellNo
	,Min(Ward.StartTime)	as FirstStayOnWardStart
	,MAX(Ward.EndTime)		as LastStayOnWardEnd
	,Ward.WardCode			as WardCohort
	,MAX(DL.EndTime)		as DischargeLoungeEndTime
	--,Case 
	--when	SP.DischargeMethodNHSCode  = '4' 
	--then	0
	--when	DL.EndTime = SP.DischargeDateTime
	--then	DATEDIFF(DAY,Ward.StartTime,Ward.EndTime)
	--when	Ward.EndTime = SP.DischargeDateTime
	--then	DATEDIFF(DAY,Ward.StartTime,Ward.EndTime)
	--else	0
	--end as WardLoS
	--,sum
	--(case 
	--when E.Division <> WDiv.Division
	--and DATEDIFF(dd,
	--(case when Ward.StartTime >= E.EpisodeStartDateTime
	--		then Ward.StartTime else E.EpisodeStartDateTime
	--		end)
	--		,(case 
	--			when E.EpisodeEndDateTime >= Ward.EndTime
	--				then Ward.EndTime
	--					else E.EpisodeEndDateTime 
	--						End)
	--						) > 0
	--then 1
	--else 0 
	--end) as BedOutlier
	
	from
	[WHREPORTING].[APC].[WardStay] Ward
	left join	[WHREPORTING].[APC].[Spell] SP
	on Ward.SourceSpellNo = SP.SourceSpellNo
	--left join	[WHREPORTING].[LK].[WardtoDivision] WDiv
	--on Ward.WardCode = WDiv.Ward
	-----following is used (alongside the Case Statement above) to determine whether the patientwas an outlier at any point--
	--left join	[WHREPORTING].[APC].[Episode] E
	--on Ward.SourceSpellNo = E.SourceSpellNo
	--	and 
	--	(E.EpisodeEndDateTime > Ward.StartTime
	--	or
	--	(E.EpisodeStartDateTime >= Ward.StartTime)
	--	)
	--	and 
	--	(E.EpisodeStartDateTime < Ward.EndTime
	--	or
	--	(E.EpisodeStartDateTime >= Ward.StartTime)
	--	)-------end of Outlier--------
		
				left join	[WHREPORTING].[APC].[WardStay]  DL
						on DL.WardCode = 'DL'
						and Ward.EndTime = DL.StartTime
						and Ward.SourceSpellNo = DL.SourceSpellNo
						
	where 
	Ward.WardCode in (SELECT Val from dbo.fn_String_To_Table(@WARDPar,',',1))
	and
	SP.DischargeDate between @StartDate and @EndDate

	group by 
	Ward.SourceSpellNo
	,Ward.WardCode

	 )  
	 SQ
	on SQ.SourceSpellNo = S.SourceSpellNo
	
	---------END OF SUB QUERY-----------
	
	----------SECOND SUB QUERY TO CALCULATE WARD LOS------
	--left join
	
	-- 	(select distinct
	--LoSWard.SourceSpellNo
	--,Min(LoSWard.StartTime)			as WardStayStart
	--,coalesce(LoSWard3.EndTime,LoSWard2.EndTime,	LoSWard.EndTime) as WardStayEnd		
	--,LoSWard.WardCode			as WardCohort
	----,case
	----when coalesce(LoSDL.Endtime,LoSWard3.EndTime,LoSWard2.EndTime,	LoSWard.EndTime) = LS.DischargeDateTime
	----then 1
	----else 0
	----end as Discharge
	----,case
	----when coalesce(LoSDL.Endtime,LoSWard3.EndTime,LoSWard2.EndTime,	LoSWard.EndTime) <> LS.DischargeDateTime
	----then 1
	----else 0
	----end as [Transfer]
	
	--	from
		
	--[WHREPORTING].[APC].[WardStay] LoSWard
	
	--left join	[WHREPORTING].[APC].[Spell] LS
	--on LoSWard.SourceSpellNo = LS.SourceSpellNo
	
	--left join  [WHREPORTING].[APC].[WardStay] LoSWard2
	--on LoSWard.SourceSpellNo = LoSWard2.SourceSpellNo
	--and cast(LoSWard.EndTime as DATE) = CAST(LoSWard2.StartTime as DATE) 
	---- captures patients who left the Ward at returned in the same day e.g. those who went to RAD or GIU and returned in the same day
	--and LoSWard2.WardCode  in (SELECT Val from dbo.fn_String_To_Table(@WARDPar,',',1))
	
	--	left join  [WHREPORTING].[APC].[WardStay] LoSWard3
	--on LoSWard.SourceSpellNo = LoSWard3.SourceSpellNo
	--and cast(LoSWard2.EndTime as DATE) = CAST(LoSWard3.StartTime as DATE) 
	---- captures patients who left the Ward at returned in the same day e.g. those who went to RAD or GIU and returned in the same day
	--and LoSWard3.WardCode in (SELECT Val from dbo.fn_String_To_Table(@WARDPar,',',1))
	
	--left join	[WHREPORTING].[APC].[WardStay]  LoSDL
	--on LoSDL.WardCode = 'DL'
	--	and LoSWard.SourceSpellNo = LoSDL.SourceSpellNo
	--	and coalesce(LoSWard3.EndTime,LoSWard2.EndTime,LoSWard.EndTime) = LoSDL.StartTime

	
	--where
	--	LoSWard.WardCode in (SELECT Val from dbo.fn_String_To_Table(@WARDPar,',',1))
	----and
	----LS.DischargeMethodNHSCode <> '4'
	----and
	----coalesce(LoSDL.EndTime,LoSWard3.EndTime,LoSWard2.EndTime,LoSWard.EndTime) = LS.DischargeDateTime
	----and
	----coalesce(LoSDL.EndTime,LoSWard3.EndTime,LoSWard2.EndTime,LoSWard.EndTime) between @StartDate and @EndDate
	--and
	--LS.DischargeDate between @StartDate and @EndDate
	--group by
	--LoSWard.SourceSpellNo
	--,LoSWard.WardCode


	--,coalesce(LoSWard3.EndTime,LoSWard2.EndTime,LoSWard.EndTime)
	--) WLoS
	--on WLoS.SourceSpellNo = SQ.SourceSpellNo
	--and WLoS.WardCohort = SQ.WardCohort
	--and	WLoS.WardStayStart = SQ.FirstStayOnWardStart
	--and	WLoS.WardStayEnd = SQ.LastStayOnWardEnd
	

	-----------END OF SECOND SUB QUERY--------
	
			--left join  ------Gets the Discharge Day------
			--[WHREPORTING].[LK].[Calendar] Dis
			--on Dis.TheDate = CAST(S.[DischargeDateTime] as DATE)
			--and S.DischargeMethodNHSCode <> '4'
			
				left join------Gets the Date Information for the Discharge Date------
				[WHREPORTING].[LK].[Calendar] Cal 
				on Cal.TheDate = S.DischargeDate
				
				left join [WHREPORTING].[APC].[Episode] E
				on E.SourceSpellNo = S.SourceSpellNo
				and E.LastEpisodeInSpell = 1
				
				left join
				[WHREPORTING].[APC].[ClinicalCoding] CC
				on CC.EncounterRecno = E.EncounterRecno
				
				
				--left join
				------Gets the number of ward moves -from the cohort - between 10pm and 8am---
				--(
				--SELECT 
				--'Wardmoves' as Indicator
				--,WM.[SourceSpellNo]
				--,sum (
				--case 
				--when SX.DischargeMethodNHSCode = 4
				--	and SX.DischargeDateTime = WM.EndTime
				--	then 0
				--	when SX.AdmissionDateTime = WM.StartTime
				--	then 0
				--	else 1
				--	end) as NightWardMoves

				--FROM [WHREPORTING].[APC].[WardStay] WM	
  	
				--		right join [WHREPORTING].[APC].[Spell] SX
				--		on SX.SourceSpellNo = WM.SourceSpellNo
				--		and  
				--		SX.AdmissionDateTime <> WM.StartTime
	 
				--	where 
				--	WM.WardCode = @Ward
				--	and
				--	SX.DischargeDate between @StartDate and @EndDate
				--	and
				--	(
				--	(DATEPART(hour,WM.StartTime) < 8)
				--	or 
				--	(DATEPART(hour,WM.StartTime) >= 22)
				--	or 
				--	(DATEPART(hour,WM.EndTime) < 8)
				--	or 
				--	(DATEPART(hour,WM.EndTime) >= 22)
				--	)
				--	 group by
				--		WM.[SourceSpellNo]
				--) WardMoves
				--on WardMoves.SourceSpellNo = SQ.SourceSpellNo
				
				left join
				----Gets the number of re-admissions within 72 Hours---
				(
select 
				Readm.PreviousAdmissionRecno
				,AM.[AdmissionMethodType] as ReadmissionAdmissionMethod
				,RS.AdmissionDateTime as ReadmissionDate
				,Readm.ReadmissionIntervalDays
				,Readm.ReadmissionAdmissionDate
				,RS.[AdmissionSpecialty(Function)] as ReadmissionSpecialty
				,RS.AdmissionConsultantName as ReadmissionConsultant
				,Readm.MetricCode
				,CC.PriamryProcedure as ReadmissionPrimaryProcedure
				,CC.PrimaryDiagnosis as ReadmissionPrimaryDiagnosis
		
				from
				[WHOLAP].[dbo].[FactReadmission] Readm
				left join [WHOLAP].[dbo].[OlapAdmissionMethod] AM
				on Readm.ReadmissionAdmissionMethodCode = AM.AdmissionMethodCode
				left join [WHREPORTING].[APC].[Spell] RS
				on Readm.ReadmissionRecno = RS.EncounterRecno
				left join [WHREPORTING].[APC].[ClinicalCoding] CC
				on CC.EncounterRecno = Readm.ReadmissionRecno
					
				where AM.[AdmissionMethodTypeCode] = 2 ----Emergency----
				) RR
				on RR.PreviousAdmissionRecno = S.EncounterRecno
				
				--left join [WH].[AE].[Encounter] AE
				------Gets the A&E Reattenders----
				--on AE.NHSNumber = S.NHSNo
				--and 
			 --   AE.[ArrivalTime] > S.[DischargeDateTime]
			 --   and
				--DATEDIFF(dd,S.[DischargeDate],AE.[ArrivalDate]) <= 7

where
Cal.TheMonth = @MonthPar
and
RR.ReadmissionIntervalDays <= '30'
and
coalesce(SQ.DischargeLoungeEndTime,SQ.LastStayOnWardEnd) = S.DischargeDateTime 

order by
RR.ReadmissionIntervalDays 
,S.DischargeDateTime
,S.DischargeWardCode
,S.SourceSpellNo