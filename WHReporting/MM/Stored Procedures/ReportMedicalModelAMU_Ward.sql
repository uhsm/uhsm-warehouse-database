﻿/*
--Author: N Scattergood
--Date created: 20/02/2013
--Stored Procedure Built for SRSS Report on:
--Medical Model for A8 and A10
--Just provides Ward Dropdown
*/

Create Procedure [MM].[ReportMedicalModelAMU_Ward]

as
select Ward from
[WHREPORTING].[LK].[WardtoDivision]
where
Ward in ('A8','A10')

order by Ward desc