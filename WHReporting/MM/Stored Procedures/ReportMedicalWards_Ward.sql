﻿/*
--Author: N Scattergood
--Date created: 18/03/2013
--Stored Procedure Built for SRSS Report on:
--Medical Model Medical Wards
--Just provides Ward Dropdown
*/

CREATE Procedure [MM].[ReportMedicalWards_Ward]

as
select

distinct
S.DischargeWardCode as WardPar
from
[WHREPORTING].APC.Spell S

where 
S.DischargeDate > DATEADD(year,-1,getdate())
and 
S.DischargeWardCode in 
(
--'A8',
--'A10',
'A7',
'F11',
'F12',
'F14',
'F4N',
'F10',
'F15S',
'F7',
'A9',
'POU',
'DYL',
'WIL',
'PEA'
)


order by S.DischargeWardCode