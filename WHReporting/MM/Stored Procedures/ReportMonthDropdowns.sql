﻿/*
--Author: N Scattergood
--Date created: 20/02/2013
--Stored Procedure Built for SRSS Report on:
--Medical Model 
--This Procedure Provides StartDate and EndDate Parameters
*/

Create Procedure [MM].[ReportMonthDropdowns]

as
Select 

Cal.TheMonth as MonthDropDown,
MIN(Cal.TheDate) as ParStartDate,
MAX(Cal.TheDate) as ParEndDate,
dbo.GetEndOfDay(MAX(Cal.TheDate)) as ParEndDateTime

From
	[WHREPORTING].[LK].[Calendar] Cal 
	
	where Cal.TheDate between 
		( select  distinct min(Cal.TheDate)
		FROM LK.Calendar Cal
			where
			Cal.TheMonth = 
			(select distinct
			SQ.TheMonth
			  FROM LK.Calendar SQ
			  where SQ.TheDate = 
			 (
			dateadd(MM,-18,cast(GETDATE()as DATE))
			)))
			and
			DATEADD(day,-1,getdate())
			
			group by
			Cal.TheMonth 
			,Cal.FinancialMonthKey
			
			order by
			Cal.FinancialMonthKey