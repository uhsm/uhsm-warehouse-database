﻿/*
--Author: N Scattergood
--Date created: 13/03/2013
--Stored Procedure Built for Pilot on Weekend Ward Rounds
*/

CREATE Procedure [MM].[ReportWeekendWardRoundPilot]

--Declare @Ward as varchar(25)		=	'F12'

 @StartDate as Datetime			=	'25 Feb 2013'	
 ,@EndDate as Datetime			=	'31 mar 2013 23:59:59'
 as
 Declare @4WeekStart as Datetime		= dateadd(day,-35,@StartDate)
 Declare @12MonthsStart as Datetime		= dateadd(day,-364,@StartDate)   --This is 364 because it's 52 weeks NOT 1 Year
 Declare @12MonthsEnd as Datetime		= dateadd(day,-364,@EndDate)		--This is 364 because it's 52 weeks NOT 1 Year


SELECT 


      S.[SourceSpellNo]
      ,S.FaciltyID
      ,S.NHSNo
      ,S.[AdmissiontWardCode] as AdmissionWardCode
      ,SQ.WardCohort
      ,SQ.WardStartTime
      ,SQ.WardEndTime
      ,SQ.DischargeLoungeStart
      ,SQ.DischargeLoungeEnd 
      ,S.[DischargeWardCode]
      ,S.[DischargeMethod]
      ,S.[DischargeMethodNHSCode]
      ,case when S.[AdmissionType] in ('Emergency','Non-Elective') then 'Non-Elective'
		else S.[AdmissionType] end as 'AdmissionMethod' 
		    ,case when S.[InpatientStayCode] = 'D'	then 'DayCase'
			when  S.[InpatientStayCode] = 'I'	then 'Inpatient'
			when  S.[InpatientStayCode] = 'B'	then 'Baby'
			else S.[InpatientStayCode] 
			end as InpatientStay  
			,Cal.LastDayOfWeek as 'WkEnding' 
			,Cal.[DayOfWeek] as DischargeDay


  FROM [WHREPORTING].[APC].[Spell] S
  
  right join ------------Start of Sub Query Limiting to Specified Wards---------
	(select distinct
	Ward.SourceSpellNo
	,Ward.StartTime			as WardStartTime
	,Ward.EndTime			as WardEndTime
	,Ward.WardCode			as WardCohort
	,DL.StartTime			as DischargeLoungeStart
	,DL.EndTime				as DischargeLoungeEnd

	
	from
	[WHREPORTING].[APC].[WardStay] Ward
	
	left join	[WHREPORTING].[APC].[Spell] SP
	on Ward.SourceSpellNo = SP.SourceSpellNo

		
				left join	[WHREPORTING].[APC].[WardStay]  DL
						on DL.WardCode = 'DL'
						and Ward.EndTime = DL.StartTime
						and Ward.SourceSpellNo = DL.SourceSpellNo
						
	where 
	Ward.WardCode in ('A8','A10','F4N','F7','F14','F11','F12','F15S')
	and
	(
	(SP.DischargeDate between @4WeekStart and @EndDate)
	or
	(SP.DischargeDate between @12MonthsStart and @12MonthsEnd)
	)
	and
	coalesce(DL.Endtime,Ward.Endtime) = SP.DischargeDateTime
	and
	SP.DischargeMethodNHSCode <> '4'
	--group by 
	--Ward.SourceSpellNo
	--,Ward.WardCode

	 )  ----------End of Sub Query-------------
	 SQ
	 on SQ.SourceSpellNo = S.SourceSpellNo
  
  
				left join------Gets the Date Information for the Discharge Date------
				[WHREPORTING].[LK].[Calendar] Cal 
				on Cal.TheDate = S.DischargeDate
				
				
--where
--S.DischargeMethodNHSCode <> '5'



--select 
--Cal.TheDate
--,Cal.DayOfWeek
--,Cal.FirstDayOfWeek
--,Cal.LastDayOfWeek

-- from
--	[WHREPORTING].[LK].[Calendar] Cal 
--	where Cal.TheDate between '25 feb 2013' and '24 mar 2013'