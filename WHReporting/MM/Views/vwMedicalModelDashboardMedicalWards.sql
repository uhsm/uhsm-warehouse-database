﻿/*
--Author: N Scattergood
--Date created: 09/04/2013
--View created for Medical Model Dashboard for Medical Wards 
*/

Create View  [MM].[vwMedicalModelDashboardMedicalWards]
as


SELECT 

Distinct

      S.[SourceSpellNo]
      ,S.FaciltyID
      ,S.NHSNo
      ,S.[AdmissiontWardCode] as AdmissionWardCode
    ,case when S.[AdmissionType] in ('Emergency','Non-Elective') then 'Non-Elective'
else S.[AdmissionType] end as 'AdmissionMethod' 
    ,case when S.[InpatientStayCode] = 'D'	then 'DayCase'
	when  S.[InpatientStayCode] = 'I'	then 'Inpatient'
	when  S.[InpatientStayCode] = 'B'	then 'Baby'
	else S.[InpatientStayCode] 
	end as InpatientStay 
	,S.AdmissionDateTime
	,S.DischargeDateTime
	,S.DischargeDate
	,Cal.TheMonth	 as DischargeMonth
	,Cal.[DayOfWeek] as DischargeDay
	,WCH.WardCohort
	,WCH.FirstStayOnWardCohort
	,WCH.LastStayOnWardCohort
	,WCH.DischargeLoungeStart
	,WCH.DischargeLoungeEnd
	  ,WCH.TotalStayOnWardCohort
      ,S.[DischargeWardCode]
      ,S.[DischargeMethod]
      --,S.[DischargeMethodNHSCode]

	 ,case
      when S.[DischargeMethodNHSCode] = 4
      then 0
      else 1
      end as IndicatorDischargedLive
      ,WCH.TotalStayOnWardCohort as IndicatorWardLoS
		,S.LengthOfSpell as IndicatorTotalLoS
       ,NightWardMoves.IndicatorNumberOfNightWardMoves
       --,RR.ReadmissionIntervalDays
       ,RR.ReadmissionDate
      --,RR.ReadmissionDate - S.[DischargeDateTime]
      ,case 
      when 
      RR.ReadmissionDate - S.[DischargeDateTime] < '1900-01-04' 
      then 1
		else 0
		end as IndicatorReadmission72Hr
	 ,case 
      when RR.ReadmissionIntervalDays <= '30'
      then 1
		else 0
		end as IndicatorReadmission30Day
       ,case
      when S.[DischargeMethodNHSCode] = 4
      then 0
      when Cal.DayOfWeekKey in (6,7)
      then 1 
      else 0
      end as IndicatorWeekendDischarge
      
      ,case
      when S.[DischargeMethodNHSCode] = 4
      then 0
      when DATEPART(hour,S.DischargeDateTime) < 12 
      then 1
      else 0 end as IndicatorAMDischarge
      
      ,NumberOfWards.IndicatorNumberOfWards
      ,Case
		when NumberOfWards.IndicatorNumberOfWards is Null
		then Null
		when NumberOfWards.IndicatorNumberOfWards > 3
		then '>3Wards'
		when NumberOfWards.IndicatorNumberOfWards = 3
		then '3Wards'
		when NumberOfWards.IndicatorNumberOfWards = 2
		then '2Wards'
		else '1Ward' end
		as 'IndicatorNumberOfWardCategory'
      
      ,case
      when S.[DischargeMethodNHSCode] = 4
      then 1
      else 0
      end as IndicatorPatientDiedOnWard
      ,case
      when S.[DischargeMethodNHSCode] = 4
      then 0
      when S.DischargeWardCode = 'DL'
      then 1
      else 0 
      end as IndicatorDischargedViaDL


  FROM [WHREPORTING].[APC].[Spell] S

  inner join ------------Start of Sub Query Limiting to Patients Discharged From Specified Wards---------
	(select 
	Ward.SourceSpellNo
	,Ward.WardCode			as WardCohort
	,MIN(DL.StartTime)		as DischargeLoungeStart
	,MAX(DL.EndTime)		as DischargeLoungeEnd
	,MIN(Ward.StartTime)	as FirstStayOnWardCohort
	,MAX(Ward.EndTime)		as LastStayOnWardCohort
	,SUM(datediff(DAY,Ward.StartTime,Ward.EndTime)) as TotalStayOnWardCohort
	
	from
	[WHREPORTING].[APC].[WardStay] Ward
	
			left join	[WHREPORTING].[APC].[Spell] SP
			on Ward.SourceSpellNo = SP.SourceSpellNo
		
				left join	[WHREPORTING].[APC].[WardStay]  DL
						on DL.WardCode = 'DL'
						and Ward.EndTime = DL.StartTime
						and Ward.SourceSpellNo = DL.SourceSpellNo
						
	where 
	Ward.WardCode in ('A7','F11','F12','F14','F4','F10D','F15S',
						'F7','A9','POU','DYL','WIL','PEA')
	and 
	SP.DischargeDate between (
select 
min(Cal.TheDate) as FirstDayOfRolling13Month
FROM LK.Calendar Cal
 
 where
Cal.TheMonth in 
(select distinct
SQ.TheMonth
  FROM LK.Calendar SQ
  where SQ.TheDate = dateadd(MM,-13,cast(GETDATE()as DATE))
)
)
and
(select
dbo.GetEndOfDay (DATEADD(day,-1,getdate())) 
)

	group by 
	Ward.SourceSpellNo
	,Ward.WardCode
	) WCH
	---------------------------End of Sub Query -----------------------------
	------------Following statements in Join Limit to Discharge Ward---------
	 on WCH.SourceSpellNo = S.SourceSpellNo
	 --and WCH.WardCohort in (SELECT Val from dbo.fn_String_To_Table(@WARDPar,',',1))
	 and 
		(
		WCH.DischargeLoungeEnd = S.DischargeDateTime
		or
		WCH.LastStayOnWardCohort = S.DischargeDateTime
		)-----Does not like coalesce because there are a small number of patients who go to DL and then back to the Ward 
     
				left join------Gets the Date Information for the Discharge Date------
				[WHREPORTING].[LK].[Calendar] Cal 
				on Cal.TheDate = S.DischargeDate
				
				
left join
				------Gets the number of wards a patient has been on------
				(
				Select 
				SP2.SourceSpellNo
				,SP2.DischargeDateTime
				,COUNT(NoWards.WardCode) as IndicatorNumberOfWards
				 FROM [WHREPORTING].[APC].[Spell] SP2
				 
				left join---Sub Query within Sub Query----
				(
				Select 
				Distinct
				W.SourceSpellNo
				,W.WardCode

				  FROM [WHREPORTING].[APC].[WardStay] W
				  left join
				  [WHREPORTING].[APC].[Spell] SW2
				  on SW2.SourceSpellNo = W.SourceSpellNo
				  
				  where
				  SW2.DischargeDate between (
select 
min(Cal.TheDate) as FirstDayOfRolling13Month
FROM LK.Calendar Cal
 
 where
Cal.TheMonth in 
(select distinct
SQ.TheMonth
  FROM LK.Calendar SQ
  where SQ.TheDate = dateadd(MM,-13,cast(GETDATE()as DATE))
)
)
and
(select
dbo.GetEndOfDay (DATEADD(day,-1,getdate())) 
)
				  and  
				  W.[WardCode] not in -----these wards are excluded-----
				  (
				  'ADMLOU',
				  'A8',
				  'A10',
				  'DL',
				  'EAU',
				  'GIU',
				  'RAD',
				  'ICA',
				  'CTCU'
				  )
				 ) NoWards
				 on NoWards.SourceSpellNo = SP2.SourceSpellNo
				  where
				  SP2.DischargeDate between (
select 
min(Cal.TheDate) as FirstDayOfRolling13Month
FROM LK.Calendar Cal
 
 where
Cal.TheMonth in 
(select distinct
SQ.TheMonth
  FROM LK.Calendar SQ
  where SQ.TheDate = dateadd(MM,-13,cast(GETDATE()as DATE))
)
)
and
(select
dbo.GetEndOfDay (DATEADD(day,-1,getdate())) 
)
				  and
				  SP2.DischargeWardCode in ('DL','A7','F11','F12','F14','F4','F10D','F15S',
						'F7','A9','POU','DYL','WIL','PEA')
				  
				  group by
				  SP2.SourceSpellNo
				  ,SP2.DischargeDateTime
				) NumberOfWards
				---End of number of wards Sub Query---
				on NumberOfWards.SourceSpellNo = S.SourceSpellNo
				
left join
				------Gets the number of night time Ward Moves------
				(
				Select 
				SP3.SourceSpellNo
				,SP3.DischargeDateTime
				,COUNT(NightMoves.WardCode) as IndicatorNumberOfNightWardMoves
				 FROM [WHREPORTING].[APC].[Spell] SP3
				 
				left join---Sub Query within Sub Query----
				(
				Select 
				Distinct
				W.SourceSpellNo
				,W.WardCode
				,W.StartTime

				  FROM [WHREPORTING].[APC].[WardStay] W
				  left join
				  [WHREPORTING].[APC].[Spell] SW3
				  on SW3.SourceSpellNo = W.SourceSpellNo
				  
				  where
				  SW3.DischargeDate between (
select 
min(Cal.TheDate) as FirstDayOfRolling13Month
FROM LK.Calendar Cal
 
 where
Cal.TheMonth in 
(select distinct
SQ.TheMonth
  FROM LK.Calendar SQ
  where SQ.TheDate = dateadd(MM,-13,cast(GETDATE()as DATE))
)
)
and
(select
dbo.GetEndOfDay (DATEADD(day,-1,getdate())) 
)
				  and 
				  W.StartTime <> SW3.[AdmissionDateTime]
				  and
					(
					(DATEPART(hour,W.StartTime ) < 8)
					or 
					(DATEPART(hour,W.StartTime ) >= 22)
					)
				) NightMoves
				 on NightMoves.SourceSpellNo = SP3.SourceSpellNo
				  where
				  SP3.DischargeDate between (
select 
min(Cal.TheDate) as FirstDayOfRolling13Month
FROM LK.Calendar Cal
 
 where
Cal.TheMonth in 
(select distinct
SQ.TheMonth
  FROM LK.Calendar SQ
  where SQ.TheDate = dateadd(MM,-13,cast(GETDATE()as DATE))
)
)
and
(select
dbo.GetEndOfDay (DATEADD(day,-1,getdate())) 
)
				  and
				  SP3.DischargeWardCode in ('DL','A7','F11','F12','F14','F4','F10D','F15S',
						'F7','A9','POU','DYL','WIL','PEA')
				  
				  group by
				  SP3.SourceSpellNo
				  ,SP3.DischargeDateTime
				) NightWardMoves
				---End of number of wards Sub Query---
				on NightWardMoves.SourceSpellNo = S.SourceSpellNo
				
left join
				----Gets the number of emergency re-admissions ----
				(
				select 
				Readm.PreviousAdmissionRecno
				,AM.[AdmissionMethodType]
				,RS.AdmissionDateTime as ReadmissionDate
				,Readm.ReadmissionIntervalDays
		
				from
				[WHOLAP].[dbo].[FactReadmission] Readm
				left join [WHOLAP].[dbo].[OlapAdmissionMethod] AM
				on Readm.ReadmissionAdmissionMethodCode = AM.AdmissionMethodCode
				left join [WHREPORTING].[APC].[Spell] RS
				on Readm.ReadmissionRecno = RS.EncounterRecno
				where AM.[AdmissionMethodTypeCode] = 2 ----Emergency----
				) RR
				on RR.PreviousAdmissionRecno = S.EncounterRecno