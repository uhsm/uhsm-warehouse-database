﻿--USE [WHREPORTING]
--GO

--/****** Object:  View [SC].[APCSpecialtyFunctionCorrection]    Script Date: 12/12/2012 15:56:03 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

/*
--Author: N Scattergood
--Date created: 18/03/2013
--View created for Medical Outliers by Directorate for the Medical Model
*/

CREATE View  [MM].[vwMedicalModelWardsDirectorates]
as
select WD.*
,case
when Ward in ('A8','A10')
then 'Accident & Emergency'
when Ward in ('F4N','F10D','F15S','F7','A9')
then 'Complex Health & Social Care'
when Ward in ('A7','F11','F12','F14')
then 'Medical Specialities'
when Ward in ('POU','DYL','WIL','PEA')
then 'Respiratory'
Else 'Check SQL'
End as 'WardDirectorate'

 from
[WHREPORTING].[LK].[WardtoDivision] WD
where
Ward in (
'A8',
'A10',
'A7',
'F11',
'F12',
'F14',
'F4N',
'F10D',
'F15S',
'F7',
'A9',
'POU',
'DYL',
'WIL',
'PEA'
)

--Select 
--Distinct 
--SD.Division,
--SD.Direcorate
--from
--[WHREPORTING].[LK].[SpecialtyDivision] SD

--order by SD.Division,SD.Direcorate