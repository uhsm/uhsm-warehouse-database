﻿CREATE Procedure [OP].[BuildBookingFact] as 

/*
Select * from OP.SlotBookingFact
Select top 10 'Slot', * from WH.OP.Slot
Select top 10 'Session', * from WH.OP.Session
*/

truncate table OP.BookingFact

Insert into OP.BookingFact (
		 [SlotUniqueID]
		,[ServicePointUniqueID]
		,[SessionUniqueID]
		,[SessionTemplateUniqueID]
		,[CancelledSessionIndicator]
		,[SlotSpecialtyUniqueID]
		,[SlotProfessionalCarerUniqueID]
		,[SlotDate]
		,[SlotStartTime]
		,[SlotEndTime]
		,[SlotDuration]
		,[IsCancelledSlot]
		,[SlotCancelledDate]
		,[SlotStatusID]
		,[SlotChangeReasonID]
		,[SlotChangeRequestByID]
		,[AllowNewBooking]
		,[AllowFollowUpBooking]
		,[AllowOverBooking]
		,[BookingsAllowed]
		,[BookingsMade]
		,[BookingsFree]
		,[IsOverBooked]
		,[NewAttended]
		,[NewDNA]
		,[NewCancelledPatient]
		,[NewCancelledHospital]
		,[NewCancelledOther]
		,[NewRescheduled]
		,[FollowUpAttended]
		,[FollowUpDNA]
		,[FollowUpCancelledPatient]
		,[FollowUpCancelledHospital]
		,[FollowUpCancelledOther]
		,[FollowUpRescheduled]
		,[NotCashedup]
		
)	
Select
	 [SlotUniqueID] = OPSlot.SlotUniqueID
	,[ServicePointUniqueID] = OPSlot.ServicePointUniqueID
	,[SessionUniqueID] = OPSlot.SessionUniqueID
	,[SessionTemplateUniqueID] = IsNull(OPSession.SessionTemplateUnqiueID,OPSession.SessionUniqueID)
	,[CancelledSessionIndicator] = OPSession.SessionCancelledFlag
	,[SlotSpecialtyUniqueID] = OPSession.SessionSpecialtyCode
	,[SlotProfessionalCarerUniqueID] = Coalesce(OPSession.SessionProfessionalCarerCode, OPClinic.ClinicProfessionalCarerCode)
	,[SlotDate] = OPSlot.SlotDate
	,[SlotStartTime] = OPSlot.SlotStartTime
	,[SlotEndTime] = OPSlot.SlotEndTime
	,[SlotDuration] = OPSlot.SlotDuration
	,[IsCancelledSlot] = OPSlot.SlotCancelledFlag
	,[SlotCancelledDate] = OPSlot.SlotCancelledDate
	,[SlotStatusID] = OPSlot.SlotStatusCode
	,[SlotChangeReasonID] = OPSlot.SlotChangeReasonCode
	,[SlotChangeRequestByID] = OPSlot.SlotChangeRequestByCode
	,[AllowNewBooking] = 
/*
				CONVERT(Bit,
						Case When OPSession.NewVisits = 1 Then 
						1
						Else
							Case When SlotTypes.New = 0 Then
							0
							Else
							1
							End
						End
				)
*/
				CONVERT(Bit,
						Coalesce(
								SlotVisitType.New
								,SessionVisitType.New
								,ClinicVisitType.New
								,OPSession.NewVisits
								,0
						)

				)
	,[AllowFollowUpBooking] = 
/*	
				CONVERT(Bit,
						Case When SlotVisitType.Review = 1 Then
						1
						Else
						0
						End
				)
*/
				CONVERT(Bit,
						Coalesce(
								SlotVisitType.Review
								,SessionVisitType.Review
								,ClinicVisitType.Review
								,1
						)

				)
	,[AllowOverBooking] = 
				CONVERT(Bit,
						Case When 
						Coalesce(
								SlotOverbooking.OverBooking
								,SessionOverBooking.OverBooking
								,ClinicOverbooking.Overbooking
								,'N'
								) = 'Yes'
						Then 
						1 
						Else 
						0 
						End
						
				)
	,[BookingsAllowed] = Coalesce(SlotMaxAppts.Maxappts,SessionSlots.MaxAppts,OPSlot.SlotMaxBookings)
	,[BookingsMade] = OPSlot.SlotNumberofBookings
	,[BookingsFree] = 
				CAse When Coalesce(SlotMaxAppts.Maxappts,SessionSlots.MaxAppts,ClinicSlotMaxAppts.MaxAppts,OPSlot.SlotMaxBookings) - OPSlot.SlotNumberofBookings < 0
				Then 0
				Else Coalesce(SlotMaxAppts.Maxappts,SessionSlots.MaxAppts,ClinicSlotMaxAppts.MaxAppts,OPSlot.SlotMaxBookings) - OPSlot.SlotNumberofBookings 
				End
	,[IsOverbooked] = 
				Case When Coalesce(SlotMaxAppts.Maxappts,SessionSlots.MaxAppts,ClinicSlotMaxAppts.MaxAppts,OPSlot.SlotMaxBookings) - OPSlot.SlotNumberofBookings < 0
				Then 1
				Else 0
				End
	,[NewAttended] = IsNull(Activity.NewAttend,0)
	,[NewDNA] = IsNull(Activity.NewDNA,0)
	,[NewCancelledPatient] = IsNull(Activity.NewCancelledPatient,0)
	,[NewCancelledHospital] = IsNull(Activity.NewCancelledHospital,0)
	,[NewCancelledOther] = IsNull(Activity.NewCancelledOther,0)
	,[NewRescheduled] = 0
	,[FollowUpAttended] = IsNull(Activity.FollowUpAttend,0)
	,[FollowUpDNA] = IsNull(Activity.FollowUpDNA,0)
	,[FollowUpCancelledPatient] = IsNull(Activity.FollowUpCancelledPatient,0)
	,[FollowUpCancelledHospital] = IsNull(Activity.FollowUpCancelledHospital,0)
	,[FollowUpCancelledOther] = IsNull(Activity.FollowUpCancelledOther,0)
	,[FollowUpRescheduled] = 0
	,[NotCashedUp] = IsNull(Activity.NotCashedUp,0)
From WH.OP.Slot OPSlot
Inner join WH.OP.[Session] OPSession
	on OPSlot.SessionUniqueID = OPSession.SessionUniqueID
left outer join WH.OP.Clinic OPClinic
	on OPSlot.ServicePointUniqueID = OPClinic.ServicePointUniqueID
--Allowed Bookings
Left Outer Join WH.OP.SessionMaxApptsPerSlot SessionSlots
	on OPSession.SessionUniqueID = SessionSlots.SessionUniqueID
left outer join WH.OP.SlotMaxAppts SlotMaxAppts
	on OPSlot.SlotUniqueID = SlotMaxAppts.SlotUniqueID
left outer join WH.OP.ClinicMaxApptsPerSlot ClinicSlotMaxAppts
	on OPSlot.SlotUniqueID = SlotMaxAppts.SlotUniqueID
--Allowd Visit Types
left outer join WH.OP.SlotPriorityAndVisitType SlotVisitType
	on OPSlot.SlotUniqueID = SlotVisitType.SlotUniqueID
left outer join WH.OP.SessionPriorityAndVisitType SessionVisitType
	on OPSlot.SessionUniqueID = SessionVisitType.SessionUniqueID
left outer join WH.OP.ClinicPriorityAndVisitType ClinicVisitType
	on OPSlot.ServicePointUniqueID = ClinicVisitType.ServicePointUniqueID
--Allowed Overbooking
left outer join WH.OP.SlotOverbooking SlotOverBooking
	on OPSlot.SlotUniqueID = SlotOverBooking.SlotUniqueID
Left Outer Join WH.OP.SessionOverbooking SessionOverbooking
	on OPSession.SessionUniqueID = SessionOverbooking.SessionUniqueID
Left Outer Join WH.OP.ClinicOverbooking ClinicOverBooking
	on OPClinic.ServicePointUniqueID = ClinicOverBooking.ServicePointUniqueID
--Actual Activities
Left Outer Join WHREPORTING.dbo.OPSlotActivity Activity
	on OPSlot.SlotUniqueID = Activity.SlotUniqueID
Where
OPSlot.ArchiveFlag = 0
and OPSlot.IsTemplate = 'N'