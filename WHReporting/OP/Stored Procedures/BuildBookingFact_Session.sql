﻿CREATE Procedure [OP].[BuildBookingFact_Session] as 

/*
Select * from OP.SlotBookingFact
Select top 10 'Clinic', * from WH.OP.Clinic
Select top 10 'Slot', * from WH.OP.Slot where sessionUNiqueID = 11043835
Select top 10 'Session', * from WH.OP.Session
Select * from lorenzo.dbo.ServicePointSlot Where spssn_refno = 11043835
*/
/*----------------------------------------------------------------------------------------------------------
Created/Modified Date			Created/Modified By			Details
---------------------			-------------------			---------------
Modified 18/03/2014					KO					Added IsNull to [SessionSpecialtyUniqueID] because of null specialty code in WH.OP.[Session]



-------------------------------------------------------------------------------------------------------------*/

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

truncate table OP.BookingFact_Session

Insert into OP.BookingFact_Session (
		 [ServicePointUniqueID]
		,[SessionUniqueID]
		,[SessionTemplateUniqueID]
		,[CancelledSessionIndicator]
		,[SessionSpecialtyUniqueID]
		,[SessionProfessionalCarerUniqueID]
		,[SessionDate]
		,[SessionStartTime]
		,[SessionEndTime]
		,[SessionDuration]
		,[SlotDuration]
		,[SessionCancelledDate]
		,[SessionStatusID]
		,[SessionChangeReasonID]
		,[SessionChangeRequestByID]
		,[SessionInstructions]
		,[SessionComments]
		,[SessionStartStatusID]
		,[SessionStartStatusReasonID]
		,[SessionEndStatusID]
		,[SessionEndStatusReasonID]
		,[AllowNewBooking]
		,[AllowFollowUpBooking]
		,[AllowOverBooking]
		,[BookingsAllowed]
		,[BookingsMade]
		,[BookingsFree]
		,[IsOverbooked]
		,[NewAttended]
		,[NewDNA]
		,[NewCancelledPatient]
		,[NewCancelledHospital]
		,[NewCancelledOther]
		,[NewRescheduled]
		,[FollowUpAttended]
		,[FollowUpDNA]
		,[FollowUpCancelledPatient]
		,[FollowUpCancelledHospital]
		,[FollowUpCancelledOther]
		,[FollowUpRescheduled]
		,[NotCashedUp]	
		,[ExcludedClinic]
)	
Select Distinct 
		 [ServicePointUniqueID] = OPSession.ServicePointUniqueID
		,[SessionUniqueID] = OPSession.SessionUniqueID
		,[SessionTemplateUniqueID] = IsNull(OPSession.SessionTemplateUnqiueID,OPSession.SessionUniqueID)
		,[CancelledSessionIndicator] = OPSession.SessionCancelledFlag
		,[SessionSpecialtyUniqueID] = isnull(OPSession.SessionSpecialtyCode, 0)
		,[SessionProfessionalCarerUniqueID] = IsNull(OPSession.SessionProfessionalCarerCode,OPClinic.ClinicProfessionalCarerCode)
		,[SessionDate] = OPSession.SessionDate
		,[SessionStartTime] = OPSession.SessionStartTime
		,[SessionEndTime] = OPSession.SessionEndTime
		,[SessionDuration] = OPSession.SessionDuration
		,[SlotDuration] = OPSession.SlotDuration
		,[SessionCancelledDate] = OPSession.SessionCancelledDate
		,[SessionStatusID] = OPSession.SessionStatusCode
		,[SessionChangeReasonID] = OPSession.SessionStatusChangeReasonCode
		,[SessionChangeRequestByID] = OPSession.SessionStatusChangeRequestByCode
		,[SessionInstructions] = OPSession.SessionInstructions
		,[SessionComments] = OPSession.SessionComments
		,[SessionStartStatusID] = OPSession.SessionStartStatusCode
		,[SessionStartStatusReasonID] = OPSession.SessionStartStatusReasonCode
		,[SessionEndStatusID] = OPSession.SessionEndStatusCode
		,[SessionEndStatusReasonID] = OPSession.SessionEndStatusReasonCode
		,[AllowNewBooking] = 
					CONVERT(Bit,
							Coalesce(
									 SessionVisitType.New
									,ClinicVisitType.New
									,OPSession.NewVisits
									,0
							)

					)
		,[AllowFollowUpBooking] = 
					CONVERT(Bit,
							Coalesce(
									SessionVisitType.Review
									,ClinicVisitType.Review
									,1
							)

					)
		,[AllowOverBooking] = 
					CONVERT(Bit,
							Case When 
							Coalesce(
									SessionOverBooking.OverBooking
									,ClinicOverbooking.Overbooking
									,'N'
									) = 'Yes'
							Then 
							1 
							Else 
							0 
							End
							
					)
		,[BookingsAllowed] = 
							Coalesce(
									SlotBookings.SlotMaxBookings,
									SessionMaxAppts.Maxappts,
									ClinicSessionMaxAppts.MaxAppts,
--									(OPSession.SessionDuration/OPSession.SlotDuration)
									0
									)
		,[BookingsMade] = IsNull(SlotBookings.Booked,0)
		,[BookingsFree] = 
				Case When 

							Coalesce(
									SlotBookings.SlotMaxBookings,
									SessionMaxAppts.Maxappts,
									ClinicSessionMaxAppts.MaxAppts,
--									(OPSession.SessionDuration/OPSession.SlotDuration)) - 
									0) -
							ISNull(SlotBookings.Booked,0) < 0
				Then 0
				Else 
											Coalesce(
									SlotBookings.SlotMaxBookings,
									SessionMaxAppts.Maxappts,
									ClinicSessionMaxAppts.MaxAppts,
--									(OPSession.SessionDuration/OPSession.SlotDuration)) 
									0)
							- IsNull(SlotBookings.Booked,0)
				End
		,[IsOverbooked] = 
				Case When Coalesce(
						SlotBookings.SlotMaxBookings,
						SessionMaxAppts.Maxappts,
						ClinicSessionMaxAppts.MaxAppts,
--						(OPSession.SessionDuration/OPSession.SlotDuration)) 
						0)
						- IsNull(SlotBookings.Booked,0) < 0
				Then 1
				Else 0
				End
		,[NewAttended] = IsNull(Activity.NewAttend,0)
		,[NewDNA] = IsNull(Activity.NewDNA,0)
		,[NewCancelledPatient] = IsNull(Activity.NewCancelledPatient,0)
		,[NewCancelledHospital] = IsNull(Activity.NewCancelledHospital,0)
		,[NewCancelledOther] = IsNull(Activity.NewCancelledOther,0)
		,[NewRescheduled] = 0
		,[FollowUpAttended] = IsNull(Activity.FollowUpAttend,0)
		,[FollowUpDNA] = IsNull(Activity.FollowUpDNA,0)
		,[FollowUpCancelledPatient] = IsNull(Activity.FollowUpCancelledPatient,0)
		,[FollowUpCancelledHospital] = IsNull(Activity.FollowUpCancelledHospital,0)
		,[FollowUpCancelledOther] = IsNull(Activity.FollowUpCancelledOther,0)
		,[FollowUpRescheduled] = 0
		,[NotCashedUp] = IsNull(Activity.NotCashedUp,0)
		,ExcludedClinic = Case WHen ExcludedClinics.EntityCode is null then 0 Else 1 ENd
From WH.OP.[Session] OPSession
--Clinic Informaion
left outer join WH.OP.Clinic OPClinic
	on OPSession.ServicePointUniqueID = OPClinic.ServicePointUniqueID

--Allowed Bookings
Left Outer Join WH.OP.SessionMaxApptsPerSession SessionMaxAppts
	on OPSession.SessionUniqueID = SessionMaxAppts.SessionUniqueID

left outer join WH.OP.ClinicMaxApptsPerSession ClinicSessionMaxAppts
	on OPSession.ServicePointUniqueID = ClinicSessionMaxAppts.ServicePointUniqueID

--Allowd Visit Types
left outer join WH.OP.SessionPriorityAndVisitType SessionVisitType
	on OPSession.SessionUniqueID = SessionVisitType.SessionUniqueID
	
left outer join WH.OP.ClinicPriorityAndVisitType ClinicVisitType
	on OPSession.ServicePointUniqueID = ClinicVisitType.ServicePointUniqueID

--Allowed Overbooking
Left Outer Join WH.OP.SessionOverbooking SessionOverbooking
	on OPSession.SessionUniqueID = SessionOverbooking.SessionUniqueID
	
Left Outer Join WH.OP.ClinicOverbooking ClinicOverBooking
	on OPSession.ServicePointUniqueID = ClinicOverBooking.ServicePointUniqueID

--Number of Bookings
Left Outer Join 
	(
		Select 
		Slots.SessionUniqueID,
		Sum(Slots.SlotNumberofBookings) AS Booked, 
		SUM(
		Coalesce(
				SlotMaxAvailBookings.MaxAppts,			
				SessionSlotMaxAvailBookings.MaxAppts,
				ClinicSlotMaxAvailBookings.MaxAppts,
				Slots.SlotMaxBookings
				)
		) AS SlotMaxBookings
		from WH.OP.Slot Slots 
		Left outer join WH.OP.SlotMaxAppts SlotMaxAvailBookings
			on Slots.SlotUniqueID = SlotMaxAvailBookings.SlotUniqueID

		Left outer join WH.OP.SessionMaxApptsPerSlot SessionSlotMaxAvailBookings
			on Slots.SessionUniqueID = SessionSlotMaxAvailBookings.SessionUniqueID

		Left outer join WH.OP.ClinicMaxApptsPerSlot ClinicSlotMaxAvailBookings
			on Slots.ServicePointUniqueID = ClinicSlotMaxAvailBookings.ServicePointUniqueID
		Where 
		
	--	Slots.SlotCancelledFlag = 0 and Removed KD 2011-08-10 slot cancellation date is not reliable
		Slots.SlotStatusCode not in (1624,1625)
		Group By Slots.SessionUniqueID

	) SlotBookings
	on OPSession.SessionUniqueID = SlotBookings.SessionUniqueID

--Actual Activities
Left Outer Join WHREPORTING.dbo.OPSessionActivity Activity
	on OPSession.SessionUniqueID = Activity.SessionUniqueID

left outer join WH.dbo.EntityLookup ExcludedClinics
	on OPSession.ServicePointUniqueID = ExcludedClinics.EntityCode
	and (EntityTypeCode = 'EXCLUDEDCLINIC' 
	--Or EntityTypeCode = 'RTTPREOPCLINIC' Removed by IS 19/01/2012 Because PreOp Clinics are to be included in OPKPI
	Or EntityTypeCode = 'TESTCLINIC')

Where
OPSession.ArchiveFlag = 0
and OPSession.IsTemplate = 0


	select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

	select @Stats = 
		'Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

	exec WriteAuditLogEvent 'PAS - WHREPORTING OP.BuildBookingFact_Session', @Stats, @StartTime