﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Load WHREPORTING.OP.ClinicalCoding

Notes:			Stored in WHREPORTING.OP

Versions:
				1.0.0.1 - 20/01/2016 - MT
					This sproc could not be found in any other calling sproc.
					Maybe the table wasn't meant to be used when this sproc
					was originally created.
					Brought up to date with net change load method and scheduled
					as part of WHREPORTING.dbo.BuildReportingModel

				1.0.0.0 - ??/??/2011 - ??
					Sproc was created.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [OP].[BuildClinicalCoding]
As

Begin Try

Declare @SQL varchar(8000)
Declare @Sequence varchar(2)
Declare @Order varchar(2)
Declare @StartTime datetime
Declare @Elapsed int
Declare @RowsInserted Int
Declare @Stats varchar(255) = ''
Declare @LastRunDateTime datetime

Set @StartTime = getdate()

Set @LastRunDateTime = (
	Select	Max(src.EventTime)
	From	WH.dbo.AuditLog src With (NoLock)
	Where	src.ProcessCode = 'PAS - WHREPORTING OP BuildClinicalCoding'
	)

-- Load net change table

	Truncate Table OP.znClinicalCoding
	
	Insert Into OP.znClinicalCoding(SCHDL_REFNO,SourceType)
	Select	Distinct op.SCHDL_REFNO,'znNetChangeByPatient'
	From	WH.PAS.znNetChangeByPatient src With (NoLock)
	
			Inner Join Lorenzo.dbo.ScheduleEvent op With (NoLock)
				On src.PATNT_REFNO = op.PATNT_REFNO

	Where	src.LastUpdatedDateTime > @LastRunDateTime

-- Delete existing records identified by net change
Delete	src
From	OP.ClinicalCoding src

		Inner Join OP.znClinicalCoding zn With (NoLock)
			On src.SourceUniqueID = zn.SCHDL_REFNO

-- Load
Insert Into OP.ClinicalCoding(EncounterRecno,SourceUniqueID,LastLoadedDateTime)
Select	src.EncounterRecno,src.SourceUniqueID,GETDATE()
From	WHOLAP.dbo.OlapOP src With (NoLock)

		Inner Join OP.znClinicalCoding zn
			On src.SourceUniqueID = zn.SCHDL_REFNO

		Left Join WHOLAP.dbo.FactClinicalCoding d With (NoLock)
			On src.SourceUniqueID = d.SourceRecno
			And d.SourceCode = 'SCHDL'
			And d.ClinicalCodingTypeCode = 'DIAGN'
			And d.SequenceNo = 1
		
		Left Join WHOLAP.dbo.FactClinicalCoding p With (NoLock)
			On src.SourceUniqueID = p.SourceRecno
			And p.SourceCode = 'SCHDL'
			And p.ClinicalCodingTypeCode = 'PROCE'
			And p.SequenceNo = 1

-- Only load appts that have either diagnoses or procedures
Where	d.SourceRecno Is Not Null		
		Or p.SourceRecno IS Not Null

Select @RowsInserted = @@rowcount

-- Update Primary Diagnosis
Update	src
Set		PrimaryDiagnosisCode = cod.CodingLocalCode,
		PrimaryDiagnosis = cod.Coding
From	OP.ClinicalCoding src

		Inner Join OP.znClinicalCoding zn
			On src.SourceUniqueID = zn.SCHDL_REFNO

		Inner Join WHOLAP.dbo.FactClinicalCoding d
			On src.SourceUniqueID = d.SourceRecno
			And d.SourceCode = 'SCHDL'
			And d.ClinicalCodingTypeCode = 'DIAGN'
			And d.SequenceNo = 1

		Inner Join WHOLAP.dbo.OlapCoding cod
			On d.ClinicalCodingCode = cod.CodingCode

-- Update Subsidiary Diagnosis
Update	src
Set		SubsidiaryDiagnosisCode = cod.CodingLocalCode,
		SubsidiaryDiagnosis = cod.Coding
From	OP.ClinicalCoding src

		Inner Join OP.znClinicalCoding zn
			On src.SourceUniqueID = zn.SCHDL_REFNO

		Inner Join WHOLAP.dbo.FactClinicalCoding d
			On src.SourceUniqueID = d.SourceRecno
			And d.SourceCode = 'SCHDL'
			And d.ClinicalCodingTypeCode = 'DIAGN'
			And d.SequenceNo = 2

		Inner Join WHOLAP.dbo.OlapCoding cod
			On d.ClinicalCodingCode = cod.CodingCode
		
-- Update Secondary Diagnsosis 1 through 12 
Set @Sequence = 3
Set @Order = 1

While @Order <= 12
Begin
	Set @SQL = 
	'Update src' + 
	' Set SecondaryDiagnosisCode' + @Order + '  = cod.CodingLocalCode,' +
	' SecondaryDiagnosis' + @Order +'  = cod.Coding' +
	' From OP.ClinicalCoding src' +
	' Inner Join OP.znClinicalCoding zn' +
	' On src.SourceUniqueID = zn.SCHDL_REFNO' +
	' Inner Join WHOLAP.dbo.FactClinicalCoding d' +
	' On src.SourceUniqueID = d.SourceRecno' +
	' And d.SourceCode = ''SCHDL''' +
	' And d.ClinicalCodingTypeCode = ''DIAGN''' +
	' And d.SequenceNo = ' + @Sequence +
	' Inner Join WHOLAP.dbo.OlapCoding cod' +
	' On d.ClinicalCodingCode = cod.CodingCode'
	Exec(@SQL)

	Set @Order = @Order + 1
	Set @Sequence = @Sequence + 1
End

-- Update Primary Procedure
Update	src
Set		PriamryProcedureCode = cod.CodingLocalCode,
		PriamryProcedure = cod.Coding,
		PrimaryProcedureDate = p.ClinicalCodingDate
From	OP.ClinicalCoding src

		Inner Join OP.znClinicalCoding zn
			On src.SourceUniqueID = zn.SCHDL_REFNO

		Inner Join WHOLAP.dbo.FactClinicalCoding p
			On src.SourceUniqueID = p.SourceRecno
			And p.SourceCode = 'SCHDL'
			And p.ClinicalCodingTypeCode = 'PROCE'
			And p.SequenceNo = 1

		Inner Join WHOLAP.dbo.OlapCoding cod
			On p.ClinicalCodingCode = cod.CodingCode

-- Update Secondary Procedures 1 through 11
Set @Sequence = 2
Set @Order = 1

While @Order <= 11
Begin
	Set @SQL = 
	'Update src' +
	' Set SecondaryProcedureCode' + @Order + '  = cod.CodingLocalCode,' +
	' SecondaryProcedure' + @Order + '  = cod.Coding,' +
	' SecondaryProcedure' + @Order + 'Date = p.ClinicalCodingDate' +
	' From OP.ClinicalCoding src' +
	' Inner Join OP.znClinicalCoding zn' +
	' On src.SourceUniqueID = zn.SCHDL_REFNO' +
	' Inner Join WHOLAP.dbo.FactClinicalCoding p' +
	' On src.SourceUniqueID = p.SourceRecno' + 
	' And p.SourceCode = ''SCHDL''' +
	' And p.ClinicalCodingTypeCode = ''PROCE''' +
	' And p.SequenceNo = ' + @Sequence +
	' Inner Join WHOLAP.dbo.OlapCoding cod' +
	' On p.ClinicalCodingCode = cod.CodingCode'
	Exec(@SQL)

	Set @Order = @Order + 1
	Set @Sequence = @Sequence + 1
End

Select @Stats = @Stats + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted)

Select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

Select @Stats = @Stats + 
	'. Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

Exec WH.dbo.WriteAuditLogEvent 'PAS - WHREPORTING OP BuildClinicalCoding', @Stats, @StartTime

End Try

Begin Catch
	Exec msdb.dbo.sp_send_dbmail
	@profile_name = 'Business Intelligence',
	@recipients = 'Business.Intelligence@uhsm.nhs.uk',
	@subject = 'WHREPORTING OP Build ClinicalCoding failed',
	@body_format = 'HTML',
	@body = 'WHREPORTING.OP.BuildClinicalCoding failed.<br><br>';
End Catch