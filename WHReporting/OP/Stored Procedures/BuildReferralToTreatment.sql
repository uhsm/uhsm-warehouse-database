﻿CREATE Procedure [OP].[BuildReferralToTreatment] As

/*
--Changes
2014-04-04 KO Added SourceUniqueID and set as PK
*/

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

If Exists (Select * from INFORMATION_SCHEMA.TABLES Where Table_Schema = 'OP' and TABLE_NAME = 'ReferralToTreatment')
DROP TABLE OP.ReferralToTreatment;


CREATE TABLE OP.ReferralToTreatment
	(
	 EncounterRecno int not null
	,SourceUniqueID int not null
	,RTTPathwayID varchar(200)
	,RTTStartDate smalldatetime
	,RTTEndDate smallDatetime
	,RTTSpecialtyCode varchar(5)
	,RTTCurrentStatusCode varchar(20)
	,RTTCurrentStatus varchar(80)
	,RTTCurrentStatusDate Smalldatetime
	,RTTPeriodStatusCode varchar(20)
	,RTTPeriodStatus varchar(80)
	);

--Create Unique Clustered Index ixcReferralToTreatmemt on OP.ReferralToTreatment(EncounterRecno);
alter table OP.ReferralToTreatment add PRIMARY KEY CLUSTERED (SourceUniqueID ASC)
Create Unique Index ixcOPReferralToTreatmemt on OP.ReferralToTreatment(EncounterRecno);


Insert into OP.ReferralToTreatment
	(
	 EncounterRecno
	,SourceUniqueID
	,RTTPathwayID
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentStatusCode
	,RTTCurrentStatus
	,RTTCurrentStatusDate
	,RTTPeriodStatusCode
	,RTTPeriodStatus
	)
 
	(
	Select 
		 Encounter.EncounterRecno
		,Encounter.SourceUniqueID
		,Encounter.RTTPathwayID
		,Encounter.RTTStartDate
		,Encounter.RTTEndDate
		,Spec.RTTSpecialtyCode
		,CurrRTT.RTTStatusLocalCode
		,CurrRTT.RTTStatusDescription
		,Encounter.RTTCurrentStatusDate
		,PerRTT.RTTStatusLocalCode
		,PerRTT.RTTStatusDescription
	FROM WHOLAP.dbo.OlapOP Encounter
	left outer join dbo.vwPASRTTStatus CurrRTT
		on Encounter.RTTCurrentStatusCode = CurrRTT.RTTStatusCode
	left outer join dbo.vwPASRTTStatus PerRTT
		on Encounter.RTTPeriodStatusCode = PerRTT.RTTStatusCode
	left outer join WHOLAP.dbo.SpecialtyDivisionBase Spec
		on Encounter.SpecialtyCode = Spec.SpecialtyCode	
	);



	select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

	select @Stats = 
		'Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

	exec WriteAuditLogEvent 'PAS - WHREPORTING OP.BuildReferralToTreatment', @Stats, @StartTime