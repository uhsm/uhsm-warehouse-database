﻿CREATE Procedure [OP].[BuildSchedule] As

/******************************************************************************
 Description: Build OP.Schedule
  
 Modification History --------------------------------------------------------

 Date		Author			Change
13/04/2016	CM				Added additional index [<IX_Schedule_SpecCodeFunction>]
19/05/2014	KO				Updated source of reference values
							see S:\CO-HI-Information\New File Management - Jan 2011\Systems\iPM\IPM_LRD_140514.xlsx
							for analysis of changes

28/05/2014	KO	`			Bypassed OlapOP view to improve performance
29/05/2014	KO				removed addition of telephone communication to calculation of AppointmentTypeNHSCode
******************************************************************************/


declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

exec dbo.BuildPASReferenceValue

If Exists (Select * from INFORMATION_SCHEMA.TABLES Where Table_Schema = 'OP' and TABLE_NAME = 'Schedule')
DROP TABLE OP.Schedule;
--TRUNCATE TABLE OP.Schedule;




CREATE TABLE OP.Schedule 
(
	 EncounterRecno int not null
	,SourcePatientNo int not null
	,SourceUniqueID int not null
	,IsWardAttender bit not null
	,ReferralSourceUniqueID int null
	,WaitingListSourceUniqueID int null
	,SessionUniqueID int
	,FacilityID varchar(20) null
	,NHSNo varchar(20) null
	,AgeAtAppointment int null
	,ClinicCode varchar(20)
	,Clinic varchar(255)
	,ClinicLocation varchar(100) null
	,AppointmentRequestDate SmallDatetime
	,EBookingReferenceNumber int
	,AppointmentDate Date null
	,AppointmentTime Time(0) null
	,AppointmentDateTime SmallDatetime null
	,ArrivalTime time
	,SeenTime Time(0)
	,DepartedTime Time(0)
	,ClinicWait Time(0)
	,AppointmentTypeCode varchar(20) null
	,AppointmentType varchar(80) null
	,AppointmentTypeNHSCode varchar(5)
	,ProfessionalCarerType varchar(80)
	,ProfessionalCarerCode varchar(20) null
	,ProfessionalCarerName varchar(80) null
	,Division varchar(80) null
	,Directorate varchar(80) null
	,[SpecialtyCode] varchar(20) null
	,[Specialty] varchar(255) null
	,[SpecialtyCode(Function)] varchar(20) null
	,[Specialty(Function)] varchar(255) null
	,[SpecialtyCode(Main)] varchar(20) null
	,[Specialty(Main)] varchar(255) null
	,AttendStatusCode varchar(20) null
	,AttendStatus varchar(80) null
	,AttendStatusNHSCode varchar(5) null
	,[Status] varchar(20) --derived attend status
	,OutcomeCode varchar(20) null
	,Outcome varchar(80) null
	,OutcomeNHSCode varchar(5) null
	,CancelledByCode varchar(20) null
	,CancelledBy varchar(80)
	,CancelledDateTime Date
	,AdministrativeCategoryCode varchar(20)
	,AdministrativeCategory varchar(80)
	,[AdministrativeCategoryNHSCode] varchar(5)
	,PrimaryDiagnosisCode varchar(20) null
	,PrimaryProcedureCode varchar(20) null
	,PrimaryProcedureDateTime smalldatetime null
	,AppointmentGPCode varchar(20) null
	,AppointmentGP varchar(255) null
	,AppointmentPracticeCode varchar(20) null
	,AppointmentPractice varchar(255) null
	,AppointmentPCTCode varchar(10) null
	,AppointmentPCT varchar(255) null
	,AppointmentCCGCode varchar(10) null
	,AppointmentCCG varchar(255) null
	,AppointmentPCTCCGCode varchar(10) null
	,AppointmentPCTCCG varchar(255) null
	,PostCode varchar(20)
	,ResidencePCTCode varchar(10)
	,ResidencePCT varchar(255)
	,ResidenceLocalAuthorityCode varchar(10)
	,ResidenceLocalAuthority varchar(255)
	,PurchaserCode varchar(10) null
	,Purchaser varchar(255) null
	,ReferralDate smalldatetime NULL
	,ResponsibleCommissionerCode varchar(10) null
	,ResponsibleCommissioner varchar(255)
	,ResponsibleCommissionerCCGOnlyCode varchar(10) null
	,ResponsibleCommissionerCCGOnly varchar(255)
	,RTTStatusCode varchar(10) NULL
	,AppointmentCreated smalldatetime
	,AppointmentCreatedByWhom varchar(80)
	,AppointmentUpdated smalldatetime
	,AppointmentUpdatedByWhom varchar(80)
	,ExcludedClinic bit
	,IsWalkIn bit
	,OurActivityFlag varchar(1)
	,NotOurActProvCode varchar(10)
	
	);
	


Insert into OP.Schedule
	(
	 [EncounterRecno]
	,[SourcePatientNo]
	,[SourceUniqueID]
	,[IsWardAttender]
	,[ReferralSourceUniqueID]
	,[WaitingListSourceUniqueID]
	,[SessionUniqueID]
	,[FacilityID]
	,[NHSNo]
	,AgeAtAppointment
	,[ClinicCode]
	,[Clinic]
	,[AppointmentRequestDate]
	,[EBookingReferenceNumber]
	,[AppointmentDate]
	,[AppointmentTime]
	,[AppointmentDateTime]
	,[ArrivalTime]
	,[SeenTime]
	,[DepartedTime]
	,[ClinicWait]
	,[AppointmentTypeCode]
	,[AppointmentType]
	,[AppointmentTypeNHSCode]
	,[ProfessionalCarerType]
	,[ProfessionalCarerCode]
	,[ProfessionalCarerName]
	,[Division]
	,[Directorate]
	,[SpecialtyCode]
	,[Specialty]
	,[SpecialtyCode(Function)]
	,[Specialty(Function)]
	,[SpecialtyCode(Main)]
	,[Specialty(Main)]
	,[AttendStatusCode]
	,[AttendStatus]
	,[AttendStatusNHSCode]
	,[Status]
	,[OutcomeCode]
	,[Outcome]
	,[OutcomeNHSCode]
	,[CancelledByCode]
	,[CancelledBy]
	,[CancelledDateTime]
	,[AdministrativeCategoryCode]
	,[AdministrativeCategory]
	,[AdministrativeCategoryNHSCode]
	,[PrimaryDiagnosisCode]
	,[PrimaryProcedureCode]
	,[PrimaryProcedureDateTime]
	,[AppointmentGPCode]
	,[AppointmentGP]
	,[AppointmentPracticeCode]
	,[AppointmentPractice]
	,[AppointmentPCTCode]
	,[AppointmentPCT]
	,[AppointmentCCGCode] 
	,[AppointmentCCG] 
	,[AppointmentPCTCCGCode]
	,[AppointmentPCTCCG]
	,[PostCode]
	,[ResidencePCTCode]
	,[ResidencePCT]
	,[ResidenceLocalAuthorityCode]
	,[ResidenceLocalAuthority]
	,[PurchaserCode]
	,[Purchaser]
	,[ResponsibleCommissionerCode] 
	,[ResponsibleCommissioner] 
	,[ResponsibleCommissionerCCGOnlyCode]
	,[ResponsibleCommissionerCCGOnly]
	,[AppointmentCreated]
	,[AppointmentCreatedByWhom]
	,[AppointmentUpdated]
	,[AppointmentUpdatedByWhom]
	,[ExcludedClinic]
	,[IsWalkIn]
	,OurActivityFlag 
	,NotOurActProvCode
	 )
	(
	Select Distinct
	 Encounter.EncounterRecno
	,Encounter.SourcePatientNo
	,Encounter.SourceUniqueID
	,Encounter.IsWardAttender
	,Encounter.ReferralSourceUniqueID
	,Encounter.WaitingListSourceUniqueID
	,sched.SPSSN_REFNO 
	,Encounter.DistrictNo
	,Encounter.NHSNumber
	,AgeAtAppointment =
	CASE WHEN  Encounter.DateOfBirth IS NULL THEN 
		999 
	ELSE
		DATEDIFF (YY, Encounter.DateOfBirth, Encounter.AppointmentDate) -
		CASE WHEN (MONTH(Encounter.DateOfBirth)= MONTH(Encounter.AppointmentDate) AND DAY(Encounter.DateOfBirth) > DAY(Encounter.AppointmentDate)
		OR MONTH (Encounter.DateOfBirth) > MONTH (Encounter.AppointmentDate))
		THEN 1 
	ELSE 0 
		END 
	END 
	,Encounter.ClinicCode
	,Clinic.NAME
	,sched.REQST_DTTM 
	,Encounter.EBookingReferenceNo
	,AppointmentDate = CAST(Encounter.AppointmentDate as Date)
	,AppointmentTime = CAST(Encounter.AppointmentTime as Time(0))
	,AppointmentDateTime = CAST(Encounter.AppointmentTime as Datetime)
	,Cast(sched.ARRIVED_DTTM as Time(0))
	,Cast(sched.SEEN_DTTM as Time(0))
	,Cast(sched.DEPARTED_DTTM as Time(0))
	,CAST(null as time(0)) 
	--,ScType.AppointmentTypeLocalCode
	--,ScType.AppointmentTypeDescription
	----,ScType.AppointmentTypeNationalCode
	--,CASE ---All Appointment Type "Walk Ins" to be put under Follow Up apart from 340 Resp Medicine
	--	WHEN ScType.AppointmentTypeDescription = 'Walk-in' and Encounter.SpecialtyCode = '2000834'
	--	then '1'
	--	WHEN ScType.AppointmentTypeDescription = 'Walk-in' and Encounter.SpecialtyCode <> '2000834'
	--	then '2'
	--	ELSE ScType.AppointmentTypeNationalCode
	--END 
	,visit.MainCode
	,visit.ReferenceValue
	,AppointmentTypeNHSCode = --ScType2.MappedCode
		case ---All Appointment Type "Walk Ins" to be put under Follow Up apart from 340 Resp Medicine
			when visit.ReferenceValue = 'Walk-in' and Encounter.SpecialtyCode = '2000834'
			then '1'
			when visit.ReferenceValue = 'Walk-in' and Encounter.SpecialtyCode <> '2000834'
			then '2'
			else visit.MappedCode
		end  
		--KO 29/05/2014 removed addition of telephone communication 
		--+ case 
		--	when sched.CMDIA_REFNO in ( 
		--		2005362		--Telephone with Patient
		--		,2005363	--Telemedicine web camera with Patient
		--		,3307681	--Telephone with Proxy
		--		,3307682	--Telemedicine web camera with proxy
		--		,3307683	--Email
		--		,3307684	--Short Message Service (SMS) - Text Messaging
		--	)
		--	then 2
		--	else 0
		--end
	--,ProcaType.ProfessionalCarerTypeDescription
	,ProfessionalCarerType = prtyp.ReferenceValue
	,Cons.NationalConsultantCode
	,Cons.Consultant
	,SpecBase.Division
	,SpecBase.Direcorate
	,SpecBase.SubSpecialtyCode
	,SpecBase.SubSpecialty
	,SpecBase.SpecialtyFunctionCode
	,SpecBase.SpecialtyFunction
	,SpecBase.MainSpecialtyCode
	,SpecBase.MainSpecialty
	--,AttendStatusCode = Attnd.AttendStatusLocalCode
	--,AttendStatus = Attnd.AttendStatusDescription
	--,AttendStatusNHSCode = Attnd.AttendStatusNationalCode
	--,Encounter.AttendanceStatusCode
	,AttendStatusCode = Attnd.MainCode
	,AttendStatus = Attnd.ReferenceValue
	,AttendStatusNHSCode = Encounter.DNACode
	,[Status] = Encounter.AttendanceStatusCode2
	,OutcomeCode = scocm.AttendOutcomeLocalCode
	,Outcome = scocm.AttendOutcomeDescription
	,OutcomeNHSCode = scocm.AttendOutcomeNationalCode
	--,OutcomeCode = scocm.MainCode
	--,Outcome = scocm.ReferenceValue
	--,OutcomeNHSCode = scocm.MappedCode
	--,cancb.CancelledByLocalCode
	--,cancb.CancelledByDescription
	,CancelledByLocalCode = cancb.MainCode
	,CancelledByDescription = cancb.ReferenceValue
	,Encounter.AppointmentCancelDate
	--,adcat.AdministrativeCategoryLocalCode
	--,adcat.AdministrativeCategoryDescription
	--,adcat.AdministrativeCategoryNationalCode
	,AdministrativeCategoryLocalCode = adcat.MainCode
	,AdministrativeCategoryDescription = adcat.ReferenceValue
	,AdministrativeCategoryNationalCode = adcat.MappedCode
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.PrimaryOperationDate
	,GP.PROCA_REFNO_MAIN_IDENT
	,GPName = GP.SURNAME + ' ' + GP.FORENAME
	,AppointmentPracticeCode = 
					Case When (charindex('-',Practice.Practice) <1) Then
						cast(Practice.PracticeCode AS varchar)
					Else
						LTRIM(RTRIM(LEFT(Practice.Practice,CHARINDEX('-',Practice.Practice) -2)))
					End
	,AppointmentPractice = 
					Case When (charindex('-',Practice.Practice) <1) Then
						cast(Practice.Practice AS varchar)
					Else
						LTRIM(RTRIM(RIGHT(Practice.Practice,CHARINDEX('-',REVERSE(Practice.Practice)) -2)))
					End
	,Practice.PCTCode
	,Practice.PCT
	,Practice.CCGCode
	,Practice.CCG
	,AppointmentPCTCCGCode = 
		case 
			when Encounter.AppointmentDate > '2013-03-31 00:00:00'
			then Practice.CCGCode
			else Practice.PCTCode
		end
	,AppointmentPCTCCG = 
		case 
			when Encounter.AppointmentDate > '2013-03-31 00:00:00'
			then Practice.CCG
			else Practice.PCT
		end
	,Encounter.Postcode
	,PostC.PCTCode
	,PCT.Organisation
	,PostC.LocalAuthorityCode
	,LA.[Local Authority Name]
	,Purchaser.MAIN_IDENT
	,Purchaser.DESCRIPTION
	,Encounter.ResponsibleCommissioner
	,ResponsibleCommissioner.PCTCCG
	,Encounter.ResponsibleCommissionerCCGOnly
	,ResponsibleCommissionerCCGOnly.PCTCCG
	,CreateDate = Encounter.PASCreated
	,UserCreated = Case When UserCreate.CODE IS null Then	
						Encounter.PASCreatedByWhom
					Else
						UserCreate.[USER_NAME]
					End
	,ModifDate = Encounter.PASUpdated
	,Modifuser =Case When UserUpdate.CODE IS null Then	
						Encounter.PASUpdatedByWhom
					Else
						UserUpdate.[USER_NAME]
					End
	,ExcludedClinic
	,IsWalkIn
	,OurActivityFlag
	,NotOurActProvCode
	
	FROM WHOLAP.dbo.OlapOP Encounter -- KO 28/05/2014 bypassed OlapOP view to improve performance
	--(select 
	--		AdminCategoryCode
	--		,AppointmentCancelDate
	--		,AppointmentDate
	--		,AppointmentStatusCode
	--		,AppointmentTime
	--		,AttendanceStatusCode2 =
	--			case
	--				when DNACode in (
	--					5		--Attended On Time	5
	--					,6		--Patient Late / Seen	6
						
	--				)then 'ATTEND'
	--				when DNACode in (
	--					 3		--Did Not Attend	DNA
	--					,7		--Patient Late / Not Seen	7
	--				)then 'DNA'
	--				when DNACode = 2		--Cancelled By Patient	2
	--				then 'PATCAN'
	--				when DNACode = 4		--Cancelled by Hospital	4
	--				then 'HOSPCAN'
	--				else 'OTHER'
	--			end
	--		,CancelledByCode
	--		,ClinicCode
	--		,ClinicType
	--		,ConsultantCode = coalesce(ConsultantCode, -1)
	--		,DateOfBirth
	--		,DisposalCode
	--		,DistrictNo
	--		,DNACode
	--		,EBookingReferenceNo
	--		,EncounterRecno
	--		,EpisodicGpCode
	--		,EpisodicGpPracticeCode = coalesce(EpisodicGpPracticeCode, -1)
	--		,FirstAttendanceFlag
	--		,IsWardAttender
	--		,NHSNumber
	--		,PASCreated
	--		,PASCreatedByWhom
	--		,PASUpdated
	--		,PASUpdatedByWhom
	--		,Postcode
	--		,PrimaryDiagnosisCode =
	--			coalesce(left(PrimaryDiagnosisCode, 5), '##')
	--		,PrimaryOperationCode =
	--			coalesce(left(PrimaryOperationCode, 5), '##')
	--		,PrimaryOperationDate
	--		,PurchaserCode
	--		,ReferralSourceUniqueID
	--		,RegisteredGpPracticeCode =
	--			coalesce(RegisteredGpPracticeCode, -1)
	--		,ResponsibleCommissioner
	--		,ResponsibleCommissionerCCGOnly
	--		,SourcePatientNo
	--		,SourceUniqueID
	--		,SpecialtyCode = coalesce(
	--			case 
	--				when ConsultantCode = '10000340' --Prof Howell
	--				then 150000185 --Local code 370C for breast oncology)
	--				else SpecialtyCode
	--			end
	--		, -1)
	--		,WaitingListSourceUniqueID
	--		,ExcludedClinic
	--		,IsWalkIn
	--	from 	WHOLAP.dbo.BaseOP) 
	--Encounter
		
	left join Lorenzo.dbo.ScheduleEvent sched
		on Encounter.SourceUniqueID = sched.SCHDL_REFNO
	
	left join WH.PAS.ServicePointBase Clinic
		on sched.SPONT_REFNO = Clinic.SPONT_REFNO
	
	left join WHOLAP.dbo.OlapPASConsultant Cons
		on Encounter.ConsultantCode = Cons.ConsultantCode
	
	left join WHOLAP.dbo.SpecialtyDivisionBase SpecBase
		on Encounter.SpecialtyCode = SpecBase.SpecialtyCode
	
	left join WH.PAS.ProfessionalCarerBase GP 
		on Encounter.EpisodicGpCode = GP.PROCA_REFNO	

	left join WHOLAP.dbo.OlapPASPracticeCCG Practice
		on case 
			when Encounter.EpisodicGpPracticeCode = -1
			then Encounter.RegisteredGpPracticeCode
			else Encounter.EpisodicGpPracticeCode
		end = Practice.PracticeCode
		
	left join LK.PasPCTOdsCCG ResponsibleCommissioner
		on ResponsibleCommissioner.PCTCCGCode = Encounter.ResponsibleCommissioner
	
	left join LK.PasPCTOdsCCG ResponsibleCommissionerCCGOnly
		on ResponsibleCommissionerCCGOnly.PCTCCGCode = Encounter.ResponsibleCommissionerCCGOnly
		
	left outer join Lorenzo.dbo.Purchaser Purchaser
		on Encounter.PurchaserCode = Purchaser.PURCH_REFNO	
	
	left outer join Organisation.dbo.Postcode PostC
		on Encounter.Postcode = PostC.Postcode
	
	left outer join Organisation.dbo.[Local Authority] LA
		on PostC.LocalAuthorityCode = LA.[Local Authority Code]
	
	left outer join Organisation.dbo.PCT PCT
		on PostC.PCTCode = PCT.OrganisationCode
	
	--left outer join dbo.vwPASScheduleAttendStatus Attnd
	--	on Encounter.AppointmentStatusCode = Attnd.AttendStatusCode
	left join LK.PASReferenceValue Attnd
		on Encounter.AppointmentStatusCode = Attnd.ReferenceValueCode
	
	left outer join dbo.vwPASScheduleOutcome scocm
		on Encounter.DisposalCode = scocm.AttendOutcomeCode
	--left join WH.PAS.ReferenceValue scocm
	--	on Encounter.DisposalCode = scocm.ReferenceValueCode
	
	--left outer join dbo.vwPASScheduleCancelledBy cancb
	--	on Encounter.CancelledByCode = cancb.CancelledByCode
	left join LK.PASReferenceValue cancb
		on Encounter.CancelledByCode = cancb.ReferenceValueCode
	
	--left outer join dbo.vwPASScheduleType ScType
	--	on Encounter.FirstAttendanceFlag = ScType.AppointmentTypeCode
	--KO changed 07/04/2014 because ref value lookup view did not contain follow up code.
	left join LK.PASReferenceValue visit
		on replace(Encounter.FirstAttendanceFlag, 2003436, 8911) = visit.ReferenceValueCode 
		----replace depreciated value 2003436 (follow up) with 8911 (follow up)
	
	--left outer join dbo.vwPASAdministrativeCategory adcat
	--	on Encounter.AdminCategoryCode = adcat.AdministrativeCategoryCode
	left join LK.PASReferenceValue adcat
		on Encounter.AdminCategoryCode = adcat.ReferenceValueCode
	
	left outer join Lorenzo.dbo.[User] UserCreate
		on Encounter.PASCreatedByWhom = UserCreate.CODE and UserCreate.ARCHV_FLAG = 'N'
	
	left outer join Lorenzo.dbo.[user] UserUpdate
		on Encounter.PASUpdatedByWhom = UserUpdate.CODE and UserUpdate.ARCHV_FLAG = 'N'
	
	--left outer join dbo.vwPASScheduleProfCarerType ProcaType
	--	on Encounter.ClinicType = ProcaType.ProfessionalCarerTypeCode
	left join LK.PASReferenceValue prtyp
		on Encounter.ClinicType = prtyp.ReferenceValueCode
		
);

Create Unique Clustered Index ixcOPEncounter on OP.Schedule(EncounterRecNo);
Create Index ixAppointmentDate on OP.Schedule(AppointmentDate);
Create Index ixReferral on OP.Schedule(ReferralSourceUniqueID);
Create Index ixWardAttender on OP.Schedule(IsWardAttender);
Create Index ixWalkin on OP.Schedule(IsWalkIn);
Create Index ixOPSchedule on OP.Schedule ([SourceUniqueID])
	Include ([ClinicCode],[AppointmentDate],[ProfessionalCarerName]);
CREATE INDEX ixOPKPI ON [WHREPORTING].[OP].[Schedule] ([IsWardAttender], 
[Status],[AppointmentDate], [AppointmentType]) 
	INCLUDE ([ReferralSourceUniqueID], [ClinicCode], [Division], [Directorate], 
	[SpecialtyCode(Function)]);
--CM 13/04/2016 Added index below for performance of OP KPI by consultant SSRS report as per PAsemota
CREATE NONCLUSTERED INDEX [<IX_Schedule_SpecCodeFunction>]ON [OP].[Schedule] ([SpecialtyCode(Function)])INCLUDE ([Specialty(Function)]);

	
	


--Update reference data. KO added 20/05/2014 to impropve performance after repointing the reference value
--to WH.PAS.ReferenceValue instead of the vwPAS.... views in WHREPORTING
----------------------------------------------------------------------
--update WHREPORTING.op.Schedule set
--	AppointmentTypeCode = visit.MainCode
--	,AppointmentType = visit.ReferenceValue
--	,AppointmentTypeNHSCode = 
--		case ---All Appointment Type "Walk Ins" to be put under Follow Up apart from 340 Resp Medicine
--			when visit.ReferenceValue = 'Walk-in' and Encounter.SpecialtyCode = '340'
--			then '1'
--			when visit.ReferenceValue = 'Walk-in' and Encounter.SpecialtyCode <> '340'
--			then '2'
--			else visit.MappedCode
--		end  

--	,AttendStatusCode = Attnd.MainCode
--	,AttendStatus = Attnd.ReferenceValue
--	,OutcomeCode = scocm.AttendOutcomeLocalCode
--	,Outcome = scocm.AttendOutcomeDescription
--	,OutcomeNHSCode = scocm.AttendOutcomeNationalCode
--	,CancelledByCode = cancb.MainCode
--	,CancelledBy = cancb.ReferenceValue
--	,AdministrativeCategoryCode = adcat.MainCode
--	,AdministrativeCategory = adcat.ReferenceValue
--	,AdministrativeCategoryNHSCode = adcat.MappedCode

--FROM WHREPORTING.OP.Schedule Encounter
--left join Lorenzo.dbo.ScheduleEvent sched
--	on Encounter.SourceUniqueID = sched.SCHDL_REFNO
--left join LK.PASReferenceValue attnd
--	on sched.ATTND_REFNO = attnd.ReferenceValueCode
--left join dbo.vwPASScheduleOutcome scocm
--	on sched.SCOCM_REFNO = scocm.AttendOutcomeCode
--left join LK.PASReferenceValue cancb
--	on sched.CANCB_REFNO = cancb.ReferenceValueCode
--left join LK.PASReferenceValue visit
--	on replace(sched.VISIT_REFNO, 2003436, 8911) = visit.ReferenceValueCode 
--left join LK.PASReferenceValue adcat
--	on sched.ADCAT_REFNO = adcat.ReferenceValueCode


--Update clinic location KO added 19/05/2014
----------------------------------------------------------------------
update OP.Schedule
    set ClinicLocation = isnull(crm.ChronosArea, heorg.DESCRIPTION)
from OP.Schedule Encounter

left join Lorenzo.dbo.ScheduleEvent sched
	on Encounter.SourceUniqueID = sched.SCHDL_REFNO

left outer join WH.PAS.ServicePointBase Clinic
	on sched.SPONT_REFNO = Clinic.SPONT_REFNO
	
left join WH.CRM.ChronosLocations crm
	on crm.LocationCode collate database_default = Clinic.HEORG_REFNO_MAIN_IDENT collate database_default

left join WH.PAS.OrganisationBase heorg
	on heorg.MAIN_IDENT = Clinic.HEORG_REFNO_MAIN_IDENT
	and heorg.OWNER_HEORG_REFNO = 2002118


--Update data required for RTT build KO added 24/07/2014
----------------------------------------------------------------------
update OP.Schedule
    set 
		RTTStatusCode = rttst.MainCode,
		ReferralDate = ref.ReferralDate
from OP.Schedule Encounter
left join WH.RF.Encounter ref
	on ref.SourceUniqueID = Encounter.ReferralSourceUniqueID
left join Lorenzo.dbo.ScheduleEvent enc
	on enc.SCHDL_REFNO = Encounter.SourceUniqueID
left join LK.PASReferenceValue rttst
	on rttst.ReferenceValueCode = enc.RTTST_REFNO
	
--Create Unique Clustered Index ixcOPEncounter on OP.Schedule(EncounterRecNo) WITH DROP_EXISTING;
--Create Index ixAppointmentDate on OP.Schedule(AppointmentDate) WITH DROP_EXISTING;
--Create Index ixReferral on OP.Schedule(ReferralSourceUniqueID) WITH DROP_EXISTING;
--Create Index ixWardAttender on OP.Schedule(IsWardAttender) WITH DROP_EXISTING;
--Create Index ixWalkin on OP.Schedule(IsWalkIn) WITH DROP_EXISTING;
--Create Index ixOPSchedule on OP.Schedule ([SourceUniqueID])
--	Include ([ClinicCode],[AppointmentDate],[ProfessionalCarerName]) WITH DROP_EXISTING;
--CREATE INDEX ixOPKPI ON [WHREPORTING].[OP].[Schedule] ([IsWardAttender], 
--[Status],[AppointmentDate], [AppointmentType]) 
--	INCLUDE ([ReferralSourceUniqueID], [ClinicCode], [Division], [Directorate], 
--	[SpecialtyCode(Function)]) WITH DROP_EXISTING;
	
	
	select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

	select @Stats = 
		'Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

	exec WriteAuditLogEvent 'PAS - WHREPORTING OP.BuildSchedule', @Stats, @StartTime