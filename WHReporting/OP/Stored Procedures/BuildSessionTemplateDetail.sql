﻿CREATE Procedure [OP].[BuildSessionTemplateDetail] as 
/*
Select * from OP.SessionTemplateDetail
Select top 10 * from WH.OP.Session
*/

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

Truncate Table OP.SessionTemplateDetail


--Insert Actual SessionTemplates
Insert into OP.SessionTemplateDetail (
	 [SessionRecno]
	,[SessionUniqueID]
	,[IsAdhocTemplate]
	,[IsActive]
	,[ServicePointUniqueID]
	,[SessionCode]
	,[SessionName]
	,[SessionTemplateSpecialtyUniqueID]
	,[SessionTemplateProfessionalCarerUniqueID]
	,[SessionTemplateInstructions]
	,[SessionTemolateComments]
	,[SessionInceptionDate]
	,[SessionConclusionDate]
	,[SessionIntendedStartTime]
	,[SessionIntendedEndTime]
	,[SessionDuration]
	,[SlotDuration]
	,[AllowVariableSlots]
	,[AllowOverbookedSlots]
	,[PartialBookingFlag]
	,[PartialBookingLeadTime]
	,[EBSPublish]
	,[EBSPublishAllSlots]
	,[EBSServiceID]
	,[AllowNewVisits]
	,[SessionTiming]
	,[OnWeeks]
	,[OnDays]
	,[DaysInWeek1]
	,[DaysInWeek2]
	,[DaysInWeek3]
	,[DaysInWeek4]
	,[DaysInWeek5]
	,[DaysInWeek6]
	,[PASCreatedDate]
	,[PASUpdatedDate]
	,[PASCreatedByWhom]
	,[PASUpdatedByWhom]
)
Select
	 SessionRecno = Template.RuleRecno
	,SessionUniqueID = Template.SessionUniqueID
	,IsAdhocTemplate = 'N'
	,IsActive = Case When
					CAST(Template.SessionEndDateTime as Date) = '1900-01-01' 
					Or CAST(Template.SessionEndDateTime as Date) > CAST(GetDate() as date) Then
					'Y'
				Else
					'N'
				End
	,ServicePointUniqueID = Template.ServicePointUniqueID
	,SessionCode = Template.SessionCode
	,SessionName = Template.SessionName
	,SessionTemplateSpecialtyUniqueID = Template.SessionSpecialtyCode
	,[SessionTemplateProfessionalCarerUniqueID] = Template.SessionProfessionalCarerCode
	,[SessionTemplateInstructions] = Template.SessionInstructions
	,[SessionTemolateComments] = Template.SessionComments
	,[SessionInceptionDate] = Template.SessionDate
	,[SessionConclusionDate] = CAST(Template.SessionEndDateTime as Date)
	,[SessionIntendedStartTime] = Template.SessionStartTime
	,[SessionIntendedEndTime] = Template.SessionEndTime
	,[SessionDuration] = Template.SessionDuration
	,[SlotDuration] = Template.SlotDuration
	,[AllowVariableSlots] = Template.VariableSlotsFlag
	,[AllowOverbookedSlots] = Template.AllowOverbookedSlots
	,[PartialBookingFlag] = Template.PartialBookingFlag
	,[PartialBookingLeadTime] = IsNull(Template.PartialBookingLeadTime,'N/A')
	,[EBSPublish] = Case When Template.EBSPublishedFlag = 1 Then 'Y' Else 'N' End
	,[EBSPublishAllSlots] = Case When Template.EBSPublishAllSlots = 1 Then 'Y' Else 'N' End
	,[EBSServiceID] = Case When Len(Template.EBSServiceID) = 0 Then Null Else Template.EBSServiceID End
	,[AllowNewVisits] = Case When Template.NewVisits = 1 Then 'Y' Else 'N' End
	,[SessionTiming] = 
			Case When Template.FrequencyOffsetCode = 163 Then
				Ltrim(Rtrim(SessionFrequencyOffset.DESCRIPTION)) + ' ' +
				Ltrim(Rtrim(SessionFrequency.DESCRIPTION)) + ' - ' +
				Ltrim(rtrim(Frequency.DESCRIPTION))
			Else
				Ltrim(Rtrim(SessionFrequencyOffset.DESCRIPTION)) + ' ' +
				Ltrim(Rtrim(SessionFrequency.DESCRIPTION)) + ' ' +
				Ltrim(Rtrim(Frequency.DESCRIPTION)) + ' - ' +
				Ltrim(rtrim(Frequency.DESCRIPTION))	
			End		
	,[OnWeeks] = Template.SessionWeeks
	,[OnDays] = Template.SessionDays
	,[DaysInWeek1] = Template.DaysInWeek1
	,[DaysInWeek2] = Template.DaysInWeek2
	,[DaysInWeek3] = Template.DaysInWeek3
	,[DaysInWeek4] = Template.DaysInWeek4
	,[DaysInWeek5] = Template.DaysInWeek5
	,[DaysInWeek6] = Template.DaysInWeekLast
	,[PASCreatedDate] = Template.PASCreated
	,[PASUpdatedDate] = Template.PASUpdated
	,PASCreatedByWhom = Template.PASCreatedByWhom
	,PASUpdatedByWhom = Template.PASUpdatedByWhom
From WH.OP.[Session] Template
	Left Outer Join WH.PAS.ReferenceValueBase Frequency
		on Template.FrequencyCode = Frequency.RFVAL_REFNO
	Left Outer Join WH.PAS.ReferenceValueBase SessionFrequency
		on Template.SessionFrequencyCode = SessionFrequency.RFVAL_REFNO
	Left Outer Join WH.PAS.ReferenceValueBase FrequencyOffset
		on Template.FrequencyOffsetCode = FrequencyOffset.RFVAL_REFNO 
	Left Outer Join WH.PAS.ReferenceValueBase SessionFrequencyOffset
		On Template.SessionFrequencyOffsetCode = SessionFrequencyOffset.RFVAL_REFNO
Where 
	Template.IsTemplate = 1
	and Template.ArchiveFlag = 0


--Insert Adhoc SessionTemplates (Determined by not having a session template id
--SessionUniqueID will be used as SessionTemplateUniqueID in activity
Insert into OP.SessionTemplateDetail (
	 [SessionRecno]
	,[SessionUniqueID]
	,[IsAdhocTemplate]
	,[IsActive]
	,[ServicePointUniqueID]
	,[SessionCode]
	,[SessionName]
	,[SessionTemplateSpecialtyUniqueID]
	,[SessionTemplateProfessionalCarerUniqueID]
	,[SessionTemplateInstructions]
	,[SessionTemolateComments]
	,[SessionInceptionDate]
	,[SessionConclusionDate]
	,[SessionIntendedStartTime]
	,[SessionIntendedEndTime]
	,[SessionDuration]
	,[SlotDuration]
	,[AllowVariableSlots]
	,[AllowOverbookedSlots]
	,[PartialBookingFlag]
	,[PartialBookingLeadTime]
	,[EBSPublish]
	,[EBSPublishAllSlots]
	,[EBSServiceID]
	,[AllowNewVisits]
	,[SessionTiming]
	,[OnWeeks]
	,[OnDays]
	,[DaysInWeek1]
	,[DaysInWeek2]
	,[DaysInWeek3]
	,[DaysInWeek4]
	,[DaysInWeek5]
	,[DaysInWeek6]
	,[PASCreatedDate]
	,[PASUpdatedDate]
	,[PASCreatedByWhom]
	,[PASUpdatedByWhom]
)
Select
	 SessionRecno = Template.RuleRecno
	,SessionUniqueID = Template.SessionUniqueID
	,IsAdhocTemplate = 'Y'
	,IsActive = Case When
					CAST(Template.SessionEndDateTime as Date) = '1900-01-01' 
					Or CAST(Template.SessionEndDateTime as Date) > CAST(GetDate() as date) Then
					'Y'
				Else
					'N'
				End	,ServicePointUniqueID = Template.ServicePointUniqueID
	,SessionCode = Template.SessionCode
	,SessionName = Template.SessionName
	,SessionTemplateSpecialtyUniqueID = Template.SessionSpecialtyCode
	,[SessionTemplateProfessionalCarerUniqueID] = Template.SessionProfessionalCarerCode
	,[SessionTemplateInstructions] = Template.SessionInstructions
	,[SessionTemolateComments] = Template.SessionComments
	,[SessionInceptionDate] = Template.SessionDate
	,[SessionConclusionDate] = CAST(Template.SessionEndDateTime as Date)
	,[SessionIntendedStartTime] = Template.SessionStartTime
	,[SessionIntendedEndTime] = Template.SessionEndTime
	,[SessionDuration] = Template.SessionDuration
	,[SlotDuration] = Template.SlotDuration
	,[AllowVariableSlots] = Template.VariableSlotsFlag
	,[AllowOverbookedSlots] = Template.AllowOverbookedSlots
	,[PartialBookingFlag] = Template.PartialBookingFlag
	,[PartialBookingLeadTime] = IsNull(Template.PartialBookingLeadTime,'N/A')
	,[EBSPublish] = Case When Template.EBSPublishedFlag = 1 Then 'Y' Else 'N' End
	,[EBSPublishAllSlots] = Case When Template.EBSPublishAllSlots = 1 Then 'Y' Else 'N' End
	,[EBSServiceID] = Case When Len(Template.EBSServiceID) = 0 Then Null Else Template.EBSServiceID End
	,[AllowNewVisits] = Case When Template.NewVisits = 1 Then 'Y' Else 'N' End
	,[SessionTiming] = 
			Case When Template.FrequencyOffsetCode = 163 Then
				Ltrim(Rtrim(SessionFrequencyOffset.DESCRIPTION)) + ' ' +
				Ltrim(Rtrim(SessionFrequency.DESCRIPTION)) + ' - ' +
				Ltrim(rtrim(Frequency.DESCRIPTION))
			Else
				Ltrim(Rtrim(SessionFrequencyOffset.DESCRIPTION)) + ' ' +
				Ltrim(Rtrim(SessionFrequency.DESCRIPTION)) + ' ' +
				Ltrim(Rtrim(Frequency.DESCRIPTION)) + ' - ' +
				Ltrim(rtrim(Frequency.DESCRIPTION))	
			End		
	,[OnWeeks] = Template.SessionWeeks
	,[OnDays] = Template.SessionDays
	,[DaysInWeek1] = Template.DaysInWeek1
	,[DaysInWeek2] = Template.DaysInWeek2
	,[DaysInWeek3] = Template.DaysInWeek3
	,[DaysInWeek4] = Template.DaysInWeek4
	,[DaysInWeek5] = Template.DaysInWeek5
	,[DaysInWeek6] = Template.DaysInWeekLast
	,[PASCreatedDate] = Template.PASCreated
	,[PASUpdatedDate] = Template.PASUpdated
	,PASCreatedByWhom = Template.PASCreatedByWhom
	,PASUpdatedByWhom = Template.PASUpdatedByWhom
From WH.OP.[Session] Template
	Left Outer Join WH.PAS.ReferenceValueBase Frequency
		on Template.FrequencyCode = Frequency.RFVAL_REFNO
	Left Outer Join WH.PAS.ReferenceValueBase SessionFrequency
		on Template.SessionFrequencyCode = SessionFrequency.RFVAL_REFNO
	Left Outer Join WH.PAS.ReferenceValueBase FrequencyOffset
		on Template.FrequencyOffsetCode = FrequencyOffset.RFVAL_REFNO 
	Left Outer Join WH.PAS.ReferenceValueBase SessionFrequencyOffset
		On Template.SessionFrequencyOffsetCode = SessionFrequencyOffset.RFVAL_REFNO
Where 
	Template.IsTemplate = 0
	and Template.SessionTemplateUnqiueID is null
	and Template.ArchiveFlag = 0


	select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

	select @Stats = 
		'Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

	exec WriteAuditLogEvent 'PAS - WHREPORTING OP.BuildSessionTemplateDetail', @Stats, @StartTime