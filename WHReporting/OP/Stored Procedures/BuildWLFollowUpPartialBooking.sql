﻿CREATE procedure [OP].[BuildWLFollowUpPartialBooking] As

truncate table OP.WLFollowUpPartialBooking

insert
into
	OP.WLFollowUpPartialBooking
(
	 EncounterRecno
	,SourceUniqueID
	,SourcePatientNo
	,SpecialtyCode
	,Specialty
	,SpecialtyFunctionCode
	,SpecialtyFunction
	,MainSpecialtyCode
	,MainSpecialty
	,ConsultantCode
	,Consultant
	,FacilityID
	,NHSNumber
	,PatientSurname
	,ReferralDate
	,DateOnWaitingList
	,VisitType
	,WaitDays
	,InviteDate
	,LeadTimeWeeks
	,ProjectedAppointmentDate
	,AppointmentDate
	,WaitingListClinicCode
	,WaitingListClinic
	,AppointmentClinicCode
	,GeneralComment
)
select
	 BaseOPWaitingList.EncounterRecno
	,BaseOPWaitingList.SourceUniqueID
	,BaseOPWaitingList.SourcePatientNo

	,SpecialtyCode = Specialty.SubSpecialtyCode
	,Specialty = Specialty.SubSpecialty
	,SpecialtyFunctionCode = Specialty.SpecialtyFunctionCode
	,SpecialtyFunction = Specialty.SpecialtyFunction
	,MainSpecialtyCode = Specialty.MainSpecialtyCode
	,MainSpecialty = Specialty.MainSpecialty

	,ConsultantCode = Consultant.NationalConsultantCode
	,Consultant = Consultant.Consultant

	,FacilityID = BaseOPWaitingList.DistrictNo
	,BaseOPWaitingList.NHSNumber
	,BaseOPWaitingList.PatientSurname
	,BaseOPWaitingList.ReferralDate
	,BaseOPWaitingList.DateOnWaitingList
	,VisitType = VisitType.ReferenceValue
	,WaitDays = BaseOPWaitingList.KornerWait
	,InviteDate = BaseOPWaitingList.InviteDate

	,LeadTimeWeeks = 
		DATEDIFF(
			 ww
			,BaseOPWaitingList.QM08StartWaitDate
			,DATEADD(
				ww
				,6
				,BaseOPWaitingList.InviteDate
			)
		)

	,ProjectedAppointmentDate =
			DATEADD(
				ww
				,6
				,BaseOPWaitingList.InviteDate
			)

	,BaseOPWaitingList.AppointmentDate
	,WaitingListClinicCode = WaitingListClinic.ServicePointLocalCode
	,WaitingListClinic = WaitingListClinic.ServicePoint
	,AppointmentClinicCode = BaseOPWaitingList.ClinicCode
	,GeneralComment = BaseOPWaitingList.GeneralComment
from
	WHOLAP.dbo.FactOPWaitingList

inner join WHOLAP.dbo.BaseOPWaitingList
on	BaseOPWaitingList.EncounterRecno = FactOPWaitingList.EncounterRecno

left join WHOLAP.dbo.OlapPASConsultant Consultant
on Consultant.ConsultantCode = FactOPWaitingList.ConsultantCode

left join WHOLAP.dbo.SpecialtyDivisionBase Specialty
on Specialty.SpecialtyCode = FactOPWaitingList.SpecialtyCode

left join WH.PAS.ServicePoint WaitingListClinic
on	WaitingListClinic.ServicePointCode = BaseOPWaitingList.WaitingListClinicCode

left join WH.PAS.ReferenceValue VisitType
on	VisitType.ReferenceValueCode = BaseOPWaitingList.VisitCode

where
	BaseOPWaitingList.WaitingListRule = 150000536 --Follow Up Outpatient Waiting List