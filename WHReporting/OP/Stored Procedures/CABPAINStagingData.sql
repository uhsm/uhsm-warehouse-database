﻿CREATE Procedure [OP].[CABPAINStagingData] AS
TRUNCATE TABLE OP.CABPAINStaging

INSERT INTO OP.CABPAINStaging
(
	EncounterRecno 
	,GPTitle
	,GPSurname
	,GPAddressLine1
	,GPAddressLine2
	,GPAddressLine3
	,GPAddressLine4
	,GPPostCode
	,PatientFACILNumber
	,PatientNHSNumber
	,Referrer
)
(
SELECT
 OP.EncounterRecno
,'Dr'
,AppointmentGP
,[Address Line 1]
,[Address Line 2]
,[Address Line 3]
,[Address Line 4]
,Addr.Postcode
,OP.FacilityID
,OP.NHSNo
,Ref.ReferrerName
from OP.Schedule OP

LEFT JOIN Organisation.dbo.[General Medical Practice] Addr
ON OP.AppointmentPracticeCode = Addr.[Organisation Code]

LEFT JOIN RF.Referral Ref
ON OP.ReferralSourceUniqueID = Ref.ReferralSourceUniqueID 

WHERE  ClinicCode = 'CABPAIN'
AND [Status]= 'DNA'
);