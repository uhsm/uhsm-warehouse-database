﻿-- =============================================
-- Author:		JBegum
-- Create date: 02/11/2015
-- Description:	Stored Procedure for the creation of csv synertech files on PRISM server
-- =============================================



CREATE PROCEDURE [OP].[CABPAINSynertech]
AS
BEGIN


SELECT DISTINCT /*CAST(Phone.PhoneNumber AS VARCHAR),*/ CH.ChronosArea, /*SPB.HEORG_REFNO_MAIN_IDENT,*/ Schedule.NHSNo, Patient.PatientTitle, Patient.PatientForename, 
Patient.PatientSurname, Patient.PatientAddress1, Patient.PatientAddress2,
Patient.PatientAddress3, Patient.PatientAddress4, Patient.PostCode, Clinic, Organisation AS Location, 
AppointmentDate, LEFT(AppointmentTime,5) AS AppointmentTime, 
ProfessionalCarerCode, ProfessionalCarerName, [Specialty(Function)],

CASE WHEN TITLE_REFNO_DESCRIPTION = 'Not Known'
	THEN PRTYP_REFNO_DESCRIPTION
	ELSE TITLE_REFNO_DESCRIPTION
END AS Title,


CASE WHEN CHARINDEX(',',ProfessionalCarerName) > 0
	THEN substring(ProfessionalCarerName, 1, charindex(',', ProfessionalCarerName)-1)
	ELSE ProfessionalCarerName
END AS DrName
	

  FROM [WHREPORTING].[OP].[Schedule] Schedule
  
  LEFT OUTER JOIN Lorenzo.dbo.ProfessionalCarer A
  ON Schedule.ProfessionalCarerCode = A.PROCA_REFNO_MAIN_IDENT 
  
  
  LEFT OUTER JOIN [WH].[OP].[Clinic] PASPoint
  ON Schedule.ClinicCode = PASPoint.ClinicCode
  
  LEFT OUTER JOIN [WHOLAP].[dbo].[OlapPASServicePoint] OLAPPASPoint
  ON PASPoint.ServicePointUniqueID = OLAPPASPoint.ServicePointCode
  
  LEFT OUTER JOIN [WHOLAP].[dbo].[OlapPASOrganisation] OLAPPASOrg
  ON OLAPPASPoint.OrganisationCode = OLAPPASOrg.OrganisationCode
  
  LEFT OUTER JOIN WH.PAS.ServicePointBase SPB
  ON Schedule.ClinicCode = SPB.SPONT_REFNO_CODE
    
  LEFT OUTER JOIN [WH].[CRM].[ChronosLocations] CH
  ON SPB.HEORG_REFNO_MAIN_IDENT COLLATE Latin1_General_CI_AS = CH.LocationCode 
  
  --LEFT OUTER JOIN WH.dbo.TImportPasPatientPhone Phone
  --ON Schedule.SourcePatientNo = Phone.SourcePatientNo
    
  ----To get the location will have to link on the following tables--
  --LEFT OUTER JOIN WHREPORTING.OP.BookingFact_Session SessionID
  --ON Schedule.SessionUniqueID = SessionID.SessionUniqueID
  
  --LEFT OUTER JOIN PAS.ServicePointBase ServicePointB
  --ON ServicePointB.SPONT_REFNO = SessionID.ServicePointUniqueID
  
  --LEFT OUTER JOIN PAS.OrganisationBase OrgBase
  --ON ServicePointB.HEORG_REFNO = OrgBase.HEORG_REFNO
  ----End of linking to tables for location--
    
  LEFT OUTER JOIN [WHREPORTING].[OP].Patient Patient
  ON Schedule.EncounterRecno = Patient.EncounterRecno 
   
  
  where 
  --AppointmentCreatedByWhom LIKE 'EBS%' 
  --and
 -- AppointmentRequestDate >='2012-07-27' 
  --and AppointmentRequestDate < '2012-07-30' 
  
 AppointmentRequestDate >= CONVERT(DATE,GetDate()-1,103)
 and AppointmentRequestDate <  CONVERT(DATE,GetDate(),103)
  
  
  and 
  Schedule.ClinicCode IN ('CABPAIN')
  and SPB.ARCHV_FLAG = 'N'
  and CancelledDateTime IS NULL 
  and [Specialty(Function)] <> 'Midwife Episode' -- Added by IS   18/01/2012, Julia Henson asked for this to be excluded 
  and ArrivalTime is null
  and DepartedTime is null
  and AttendStatus = 'Not Specified'
  and AppointmentType = 'New'
  and A.MODIF_DTTM = 
  (
  SELECT MAX(MODIF_DTTM)
  FROM Lorenzo.dbo.ProfessionalCarer B
  WHERE A.PROCA_REFNO_MAIN_IDENT = B.PROCA_REFNO_MAIN_IDENT
  GROUP BY PROCA_REFNO_MAIN_IDENT
  ) 
	
  order by AppointmentDate




END