﻿-- =============================================
-- Author:		JBegum
-- Create date: 02/11/2015
-- Description:	Stored Procedure for the creation of csv synertech files on PRISM server
-- =============================================



CREATE PROCEDURE [OP].[CABSynertech]
AS
BEGIN


SELECT DISTINCT CH.ChronosArea, Schedule.NHSNo, Patient.PatientTitle, Patient.PatientForename, 
Patient.PatientSurname, Patient.PatientAddress1, Patient.PatientAddress2,
Patient.PatientAddress3, Patient.PatientAddress4, Patient.PostCode, Clinic, Organisation AS Location, 
AppointmentDate, LEFT(AppointmentTime,5) AS AppointmentTime, 
ProfessionalCarerCode, ProfessionalCarerName, [Specialty(Function)],

CASE WHEN TITLE_REFNO_DESCRIPTION = 'Not Known'
	THEN PRTYP_REFNO_DESCRIPTION
	ELSE TITLE_REFNO_DESCRIPTION
END AS Title,


CASE WHEN CHARINDEX(',',ProfessionalCarerName) > 0
	THEN substring(ProfessionalCarerName, 1, charindex(',', ProfessionalCarerName)-1)
	ELSE ProfessionalCarerName
END AS DrName
	

  FROM [WHREPORTING].[OP].[Schedule] Schedule
  
  LEFT OUTER JOIN Lorenzo.dbo.ProfessionalCarer A
  ON Schedule.ProfessionalCarerCode = A.PROCA_REFNO_MAIN_IDENT 
  
  
  LEFT OUTER JOIN [WH].[OP].[Clinic] PASPoint
  ON Schedule.ClinicCode = PASPoint.ClinicCode
  
  LEFT OUTER JOIN [WHOLAP].[dbo].[OlapPASServicePoint] OLAPPASPoint
  ON PASPoint.ServicePointUniqueID = OLAPPASPoint.ServicePointCode
  
  LEFT OUTER JOIN [WHOLAP].[dbo].[OlapPASOrganisation] OLAPPASOrg
  ON OLAPPASPoint.OrganisationCode = OLAPPASOrg.OrganisationCode
  
  LEFT OUTER JOIN WH.PAS.ServicePointBase SPB
  ON Schedule.ClinicCode = SPB.SPONT_REFNO_CODE
    
  LEFT OUTER JOIN [WH].[CRM].[ChronosLocations] CH
  ON SPB.HEORG_REFNO_MAIN_IDENT COLLATE Latin1_General_CI_AS = CH.LocationCode 

  LEFT OUTER JOIN [WHREPORTING].[OP].Patient Patient
  ON Schedule.EncounterRecno = Patient.EncounterRecno 
   
  
  where 
  AppointmentCreatedByWhom LIKE 'EBS%' 

  and AppointmentRequestDate >= CONVERT(DATE,GetDate()-1,103)
  and AppointmentRequestDate <  CONVERT(DATE,GetDate(),103)
  
  
  and Schedule.ClinicCode NOT IN ('ANCASTRA','CABUROL','CABDERM','DEXA-NG',
  'MSKWCASPT8A', 'MSKWCASPT8B', 'MSKWCASPT8C', 'MSKWCASPT8X', 'MSKWCASPT8Y', 'MSKWCASPTZ',
  'MSKWCASPD8A', 'MSKWCASPD7A', 'MSKWCASPD7V', 'MSKWCASJAC', 'MSKWCASHK', 'MSKWCASSEMA', 'MSKWCASSEMB',
  'MSKWYASPT8A', 'MSKWYASHK'
  )-- Added by IS 17/10/2014 to exclude some MSK clinics because wrong spec was going on the letters.  Phyliss Comer asked Ijaz to exclude
  and SPB.ARCHV_FLAG = 'N'
  and CancelledDateTime IS NULL 
  and Clinic NOT LIKE '%Tele%'
  and [Specialty(Function)] <> 'Midwife Episode' -- Added by IS 18/01/2012, Julia Henson asked for this to be excluded 
  and ArrivalTime is null
  and DepartedTime is null
  and AttendStatus = 'Not Specified'
  and AppointmentType = 'New'
  and A.MODIF_DTTM = 
  (
  SELECT MAX(MODIF_DTTM)
  FROM Lorenzo.dbo.ProfessionalCarer B
  WHERE A.PROCA_REFNO_MAIN_IDENT = B.PROCA_REFNO_MAIN_IDENT
  GROUP BY PROCA_REFNO_MAIN_IDENT
  )
  AND AppointmentDate > GETDATE()+3 -- Added by JB so only looking at appts. that are after 3 days because the otherwise anything less than that will not get sent
  order by AppointmentDate



END