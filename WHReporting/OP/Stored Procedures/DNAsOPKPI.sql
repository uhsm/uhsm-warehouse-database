﻿CREATE procedure [OP].[DNAsOPKPI]


@StartDate as datetime
,@EndDate as datetime
,@Division as varchar(80)
,@Directorate as varchar(80)
,@Specialty as varchar(20)
AS

--DECLARE @Spec as varchar(20)
--SET @Spec = @Division

--DECLARE @Dir as varchar(20)
--SET @Spec = @Directorate

--DECLARE @Div as varchar(20)
--SET @Spec = @Specialty

SELECT     
CASE WHEN AppointmentTypeNHSCode = 1 THEN 'New' 
	WHEN AppointmentTypeNHSCode = 2 THEN 'Follow Up' 
	WHEN AppointmentTypeNHSCode IS NULL THEN 'Follow Up' 
END AS ApptType, 

CASE WHEN AppointmentTypeNHSCode = 1 THEN 0.07 
ELSE 0.10 END AS DNATarget, 
                      
SUM(
CASE 
	WHEN AttendStatus IN ('Attended', 'Attended on Time', 'Patient Late / Seen') 
	AND (outcome IS NULL OR (Outcome NOT LIKE '%canc%' 
	AND Outcome NOT LIKE '%Did Not Attend%')) 
THEN 1 ELSE 0 END
) AS Attends, 
                      
SUM(
CASE 
	WHEN AttendStatus IN ('Refused to Wait', 'Patient Left / Not seen', 
	'Patient Late / Not Seen', 'Did Not Attend') 
	OR (AttendStatus IN ('Attended', 'Attended on Time', 'Patient Late / Seen') 
	AND Outcome = 'Did Not Attend') 
THEN 1 ELSE 0 END
) AS DNAs, 
                      
SUM(
CASE 
	WHEN AttendStatus IN ('Attended', 'Attended on Time', 'Patient Late / Seen') 
	AND (outcome IS NULL OR (Outcome NOT LIKE '%canc%' 
	AND Outcome NOT LIKE '%Did Not Attend%')) 
THEN 1 ELSE 0 END
) 
+ 
SUM(
CASE
	WHEN AttendStatus IN ('Refused to Wait','Patient Left / Not seen', 'Patient Late / Not Seen', 'Did Not Attend') 
	OR (AttendStatus IN ('Attended', 'Attended on Time', 'Patient Late / Seen') 
	AND Outcome = 'Did Not Attend') 
THEN 1 ELSE 0 END
) AS Total, 
                      
CAST(
SUM(
CASE 
	WHEN AttendStatus IN ('Refused to Wait', 'Patient Left / Not seen', 'Patient Late / Not Seen', 'Did Not Attend') 
	OR (AttendStatus IN ('Attended', 'Attended on Time', 'Patient Late / Seen') 
	AND Outcome = 'Did Not Attend') 
THEN 1 ELSE 0 END
) AS decimal
) 
/ 
CAST(
SUM(
CASE 
	WHEN AttendStatus IN ('Attended', 'Attended on Time', 'Patient Late / Seen') 
	AND (outcome IS NULL OR (Outcome NOT LIKE '%canc%' 
	AND Outcome NOT LIKE '%Did Not Attend%')) 
THEN 1 ELSE 0 END
) 
+ 
SUM(
CASE 
	WHEN AttendStatus IN ('Refused to Wait','Patient Left / Not seen', 'Patient Late / Not Seen', 'Did Not Attend') 
	OR (AttendStatus IN ('Attended', 'Attended on Time', 'Patient Late / Seen') 
	AND Outcome = 'Did Not Attend') 
THEN 1 ELSE 0 END
) AS decimal
) AS Percentage,
                       
CONVERT(CHAR(4), OP.AppointmentDate, 100) + CONVERT(CHAR(4), OP.AppointmentDate, 120) AS MonthYear

FROM OP.Schedule AS OP LEFT OUTER JOIN vwExcludeWAWardsandClinics AS Exc 
ON OP.ClinicCode = Exc.SPONT_REFNO_CODE
WHERE     
(OP.AppointmentDate >= @StartDate) 
AND (OP.AppointmentDate <= @EndDate) 
AND (OP.IsWardAttender = 0) 
AND (Exc.SPONT_REFNO_CODE IS NULL) 
AND (OP.AppointmentTypeNHSCode IN (1, 2) OR OP.AppointmentTypeNHSCode IS NULL) 
AND (OP.Status IN ('DNA', 'ATTEND')) 
AND (OP.AdministrativeCategory NOT LIKE '%Private%') 
AND (OP.Division IN (SELECT Param FROM dbo.fn_MVParam (@Division,',')))
AND (OP.Directorate IN (SELECT Param FROM dbo.fn_MVParam1 (@Directorate,',')))
--AND (OP.[SpecialtyCode(Function)] IN (@Specialty))
AND (OP.[SpecialtyCode(Function)] IN (SELECT Param FROM dbo.fn_MVParam2 (@Specialty,',')))
--WHERE ProductID IN (SELECT Item FROM dbo.Split (@Item, ','))
GROUP BY 
CASE 
	WHEN AppointmentTypeNHSCode = 1 THEN 'New' 
	WHEN AppointmentTypeNHSCode = 2 THEN 'Follow Up' 
	WHEN AppointmentTypeNHSCode IS NULL THEN 'Follow Up' 
END, 

CASE WHEN AppointmentTypeNHSCode = 1 THEN 0.07 
ELSE 0.10 END, 
CONVERT(CHAR(4), OP.AppointmentDate, 100) + CONVERT(CHAR(4), OP.AppointmentDate, 120)