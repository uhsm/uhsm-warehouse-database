﻿/*Clinics*/

CREATE Procedure [OP].[OPKPI_Clinics]
(
@StartDate datetime
,@EndDate   datetime
,@Division  nvarchar(max)  
,@Directorate nvarchar(max) 
,@Specialty nvarchar(max)
,@Consultant nvarchar(max)  
)
as 
set nocount on
/*
Declare	@StartDate datetime = '20150501'
Declare	@EndDate   datetime = '20150604'
Declare	@Division  nvarchar(255) = 'Scheduled Care'    
Declare @Directorate nvarchar(255) = 'Surgery'
Declare @Specialty nvarchar(255) = '100' 
Declare @Consultant nvarchar(255) =  'C3301993'
*/

/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/

SELECT DISTINCT 
sc.ClinicCode
,sc.ClinicDescription
FROM OP.SessionUtilisation  sc
	  LEFT OUTER JOIN
      vwExcludeWAWardsandClinics  Exc 
      ON sc.ClinicCode = Exc.SPONT_REFNO_CODE
WHERE     
Exc.SPONT_REFNO_CODE IS NULL
AND 
sc.Division IN (SELECT Item FROM WHREPORTING.dbo.Split (@Division, ','))
AND sc.Directorate IN (SELECT Item FROM WHREPORTING.dbo.Split (@Directorate, ','))
AND sc.TreatmentFunctionCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Specialty, ','))
AND sc.ExcludedClinic = 'N'
AND sc.SessionDate >= @StartDate 
AND sc.SessionDate <= @EndDate
AND sc.ProfessionalCarerCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Consultant, ','))
ORDER BY sc.ClinicCode