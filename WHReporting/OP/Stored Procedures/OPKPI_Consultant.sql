﻿/*Consultant*/

CREATE Procedure [OP].[OPKPI_Consultant]
(
@StartDate datetime
,@EndDate   datetime
,@Division  nvarchar(max)  
,@Directorate nvarchar(max) 
,@Specialty nvarchar(max)
)
as 
set nocount on
/*
Declare	@StartDate datetime = '20150501'
Declare	@EndDate   datetime = '20150604'
Declare	@Division  nvarchar(255) = 'Scheduled Care'    
Declare @Directorate nvarchar(255) = 'Surgery'
Declare @Specialty nvarchar(255) = '100' 
*/


/*xxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/


SELECT DISTINCT 
	sc.ProfessionalCarerCode
	,sc.ProfessionalCarerName
FROM OP.Schedule sc
		LEFT OUTER JOIN
		vwExcludeWAWardsandClinics  ex
		ON sc.ClinicCode = ex.SPONT_REFNO_CODE
WHERE     
	ex.SPONT_REFNO_CODE IS NULL
	AND 
	sc.ExcludedClinic = 0
	AND sc.Division IN (SELECT Item FROM WHREPORTING.dbo.Split (@Division, ','))
	AND sc.Directorate IN (SELECT Item FROM WHREPORTING.dbo.Split (@Directorate, ','))
	AND sc.[SpecialtyCode(Function)] IN (SELECT Item FROM WHREPORTING.dbo.Split (@Specialty, ','))
ORDER BY sc.ProfessionalCarerCode