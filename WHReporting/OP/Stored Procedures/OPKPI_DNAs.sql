﻿/*DNAs*/


CREATE Procedure [OP].[OPKPI_DNAs]
(
@StartDate datetime
,@EndDate   datetime
,@Division  nvarchar(max)  
,@Directorate nvarchar(max) 
,@Specialty nvarchar(max)
,@Clinics nvarchar(max)  
,@Consultant nvarchar(max)  
,@Reportlevel Int  
)
as 
set nocount on
/*
Declare	@StartDate datetime = '20150501'
Declare	@EndDate   datetime = '20150604'
Declare	@Division  nvarchar(255) = 'Scheduled Care'    
Declare @Directorate nvarchar(255) = 'Surgery'
Declare @Specialty nvarchar(255) = '100' 
Declare @Consultant nvarchar(255) =  'C3301993'
Declare @Clinics nvarchar(max)  = 'SWG1PR'
Declare @Reportlevel Int = 0 /*0 = Clinic and 1 = Trust*/
*/


SELECT     
		CASE 
		WHEN AppointmentTypeNHSCode = 1 THEN 'New' 
		WHEN AppointmentTypeNHSCode = 2 THEN 'Follow Up' 
		WHEN AppointmentTypeNHSCode IS NULL THEN 'Follow Up' END AS ApptType
		,0.075 AS DNATarget
		,SUM(CASE
			  WHEN AttendStatus IN ('Attended', 'Attended on Time', 'Patient Late / Seen') AND  (outcome IS NULL OR
					 (Outcome NOT LIKE '%canc%' AND Outcome NOT LIKE '%Did Not Attend%'))
			  THEN 1 ELSE 0 END) AS Attends
		,SUM(CASE 
			  WHEN AttendStatus IN ('Refused to Wait', 'Patient Left / Not seen', 'Patient Late / Not Seen', 'Did Not Attend') OR
				  (AttendStatus IN ('Attended', 'Attended on Time', 'Patient Late / Seen') AND Outcome = 'Did Not Attend') 
			  THEN 1 ELSE 0 END) AS DNAs
		,SUM(CASE 
			  WHEN AttendStatus IN ('Attended', 'Attended on Time', 'Patient Late / Seen') AND (outcome IS NULL OR
				   (Outcome NOT LIKE '%canc%' AND Outcome NOT LIKE '%Did Not Attend%')) 
			  THEN 1 ELSE 0 END) 
			  + 
		 SUM(CASE 
			  WHEN AttendStatus IN ('Refused to Wait', 'Patient Left / Not seen', 'Patient Late / Not Seen', 'Did Not Attend') OR
				   (AttendStatus IN ('Attended', 'Attended on Time', 'Patient Late / Seen') AND Outcome = 'Did Not Attend') 
			  THEN 1 ELSE 0 END) AS Total
		,CAST(SUM(CASE 
					WHEN AttendStatus IN ('Refused to Wait', 'Patient Left / Not seen', 'Patient Late / Not Seen', 'Did Not Attend') OR
						(AttendStatus IN ('Attended', 'Attended on Time', 'Patient Late / Seen') AND Outcome = 'Did Not Attend') 
					THEN 1 ELSE 0 END) AS decimal) 
			  / 
		 CAST(SUM(CASE 
					WHEN AttendStatus IN ('Attended', 'Attended on Time', 'Patient Late / Seen') AND (outcome IS NULL OR
						 (Outcome NOT LIKE '%canc%' AND Outcome NOT LIKE '%Did Not Attend%')) 
					THEN 1 ELSE 0 END)
			  + 
			  SUM(CASE 
					WHEN AttendStatus IN ('Refused to Wait', 'Patient Left / Not seen', 'Patient Late / Not Seen', 'Did Not Attend') OR
						(AttendStatus IN ('Attended', 'Attended on Time', 'Patient Late / Seen') AND Outcome = 'Did Not Attend') 
					THEN 1 ELSE 0 END) AS decimal) AS Percentage
		,CONVERT(CHAR(4), OP.AppointmentDate, 100) + CONVERT(CHAR(4), OP.AppointmentDate, 120) AS MonthYear
FROM         OP.Schedule OP 
             LEFT OUTER JOIN
             vwExcludeWAWardsandClinics Exc 
             ON OP.ClinicCode = Exc.SPONT_REFNO_CODE
WHERE     
		OP.AppointmentDate >= @StartDate 
		AND OP.AppointmentDate <= @EndDate 
		AND OP.IsWardAttender = 0
		AND Exc.SPONT_REFNO_CODE IS NULL 
        AND (
            OP.AppointmentTypeNHSCode IN (1, 2) 
			OR
            OP.AppointmentTypeNHSCode IS NULL
            ) 
        AND OP.Status IN ('DNA', 'ATTEND')
        AND OP.AdministrativeCategory NOT LIKE '%Private%'
        AND OP.Division IN (SELECT Item FROM WHREPORTING.dbo.Split (@Division, ','))
        AND OP.Directorate IN (SELECT Item FROM WHREPORTING.dbo.Split (@Directorate, ','))
        AND OP.[SpecialtyCode(Function)] IN (SELECT Item FROM WHREPORTING.dbo.Split (@Specialty, ','))
        AND
       (
       (
       @Reportlevel = 0 
       AND OP.ClinicCode  IN (SELECT Item FROM WHREPORTING.dbo.Split (@Clinics , ',')) 
       AND OP.ProfessionalCarerCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Consultant , ','))
       )
       or @Reportlevel = 1 
       )

        
        
GROUP BY  CASE WHEN AppointmentTypeNHSCode = 1 
      THEN 'New' WHEN AppointmentTypeNHSCode = 2 THEN 'Follow Up' WHEN AppointmentTypeNHSCode IS
                       NULL THEN 'Follow Up' END, CONVERT(CHAR(4), OP.AppointmentDate, 100) + CONVERT(CHAR(4), OP.AppointmentDate, 120)