﻿/*Directorate*/
CREATE Procedure [OP].[OPKPI_Directorate]
(
@Division  nvarchar(max)  
)
as 
set nocount on
/*
Declare	@Division  nvarchar(255) = 'Scheduled Care'    
*/

SELECT     Directorate
FROM         OP.SessionUtilisation
WHERE     Division  IN (SELECT Item FROM WHREPORTING.dbo.Split (@Division, ',')) 
GROUP BY Directorate