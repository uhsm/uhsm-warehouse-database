﻿/*ds_HospitalCancellations*/
CREATE Procedure [OP].[OPKPI_HospitalCancellations]
(
@StartDate datetime
,@EndDate   datetime
,@Division  nvarchar(max)  
,@Directorate nvarchar(max) 
,@Specialty nvarchar(max)
,@Clinics nvarchar(max)  
,@Consultant nvarchar(max)  
,@Reportlevel Int  

)
as 
Set nocount on

/*
Declare	@StartDate datetime = '20150101'
Declare	@EndDate   datetime = '20150604'
Declare	@Division  nvarchar(255) = 'Scheduled Care'    
Declare @Directorate nvarchar(255) = 'Surgery'
Declare @Specialty nvarchar(255) = '100, 110' 
Declare @Consultant nvarchar(255) =  'C3301993'
Declare @Clinics nvarchar(max)  = 'SWG1PR, SWGA5R'
Declare @Reportlevel Int = 0 /*0 = Clinic and 1 = Trust*/
*/

SELECT     
	CASE WHEN ReferralType IN ('Cancelled', 'Choose and Book') 
		THEN 'C&B' ELSE ReferralType 
		END                                                                     AS ReferralType, OP.Schedule.AppointmentType
	,SUM(CASE WHEN Status = 'HOSPCAN' THEN 1 ELSE 0 END)                        AS CountOfHospitalCanc
	,CONVERT(CHAR(4), OP.Schedule.AppointmentDate, 100)+CONVERT(CHAR(4), OP.Schedule.AppointmentDate, 120)                      AS MonthYear
FROM   OP.Schedule 
       INNER JOIN 
       RF.Referral RF ON OP.Schedule.ReferralSourceUniqueID = RF.ReferralSourceUniqueID 
       LEFT OUTER JOIN
       vwExcludeWAWardsandClinics  Exc ON OP.Schedule.ClinicCode = Exc.SPONT_REFNO_CODE
WHERE  
		OP.Schedule.AppointmentDate >= @StartDate
		AND OP.Schedule.AppointmentDate <= @EndDate
		AND OP.Schedule.Status = 'HOSPCAN' 
		AND OP.Schedule.IsWardAttender = 0 
		AND OP.Schedule.AppointmentType IN ('New', 'Follow Up')
		AND Exc.SPONT_REFNO_CODE IS NULL
		AND RF.ReferralType <> 'Duplicate' 
		AND OP.Schedule.Division IN (SELECT Item FROM WHREPORTING.dbo.Split (@Division, ','))
		AND OP.Schedule.Directorate IN (SELECT Item FROM WHREPORTING.dbo.Split (@Directorate, ','))
		AND OP.Schedule.[SpecialtyCode(Function)] IN (SELECT Item FROM WHREPORTING.dbo.Split (@Specialty, ',')) 
		AND
       (
       (
       @Reportlevel = 0 
       AND OP.Schedule.ClinicCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Clinics , ',')) 
       AND OP.Schedule.ProfessionalCarerCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Consultant , ','))
       )
       or @Reportlevel = 1 
       )

GROUP BY  CASE WHEN ReferralType IN ('Cancelled', 'Choose and Book') 
		THEN 'C&B' ELSE ReferralType END, OP.Schedule.AppointmentType
		,CONVERT(CHAR(4), OP.Schedule.AppointmentDate, 100) + CONVERT(CHAR(4), OP.Schedule.AppointmentDate, 120)