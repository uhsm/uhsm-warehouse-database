﻿/*LessThan6WksCancel*/
CREATE Procedure [OP].[OPKPI_LessThan6WksCancel]
(
@StartDate datetime
,@EndDate   datetime
,@Division  nvarchar(max)  
,@Directorate nvarchar(max) 
,@Specialty nvarchar(max)
,@Clinics nvarchar(max)  
,@Consultant nvarchar(max)  
,@Reportlevel int
)
as 

/*
Declare	@StartDate datetime = '20150501'
Declare	@EndDate   datetime = '20150604'
Declare	@Division  nvarchar(255) = 'Scheduled Care'    
Declare @Directorate nvarchar(255) = 'Surgery'
Declare @Specialty nvarchar(255) = '100' 
Declare @Consultant nvarchar(255) =  'C3301993'
Declare @Clinics nvarchar(max)  = 'SWG1PR'
Declare @Reportlevel Int = 0
*/
set nocount on

SELECT     
SUM(CASE WHEN IsCancelled = 'Y' AND DATEDIFF([d], SessionCancellationDate, SessionDate) >= 0 
     AND DATEDIFF([d], SessionCancellationDate, SessionDate) <= 42 THEN 1 ELSE 0 END)   AS CountofCancelled
,OPIn.Counting
,CONVERT(CHAR(4), OPOut.SessionDate, 100) + CONVERT(CHAR(4),OPOut.SessionDate, 120) AS MonthYear
FROM OP.SessionUtilisation OPOut 
        LEFT OUTER JOIN
          (
           SELECT     
               COUNT(1) AS Counting
               ,ClinicCode
               ,CONVERT(CHAR(4), SessionDate, 100) + CONVERT(CHAR(4), SessionDate, 120) AS MonthYear
            FROM  OP.SessionUtilisation
            WHERE 
				SessionDate >= @StartDate 
				AND SessionDate < @EndDate 
				AND Division IN (SELECT Item FROM WHREPORTING.dbo.Split (@Division, ',')) 
				AND Directorate IN (SELECT Item FROM WHREPORTING.dbo.Split (@Directorate, ',')) 
				AND TreatmentFunctionCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Specialty, ','))
				AND SessionIsActive = 'Y'
				AND ExcludedClinic = 'N'
           AND
       (
       (
       @Reportlevel = 0 
       AND ClinicCode  IN (SELECT Item FROM WHREPORTING.dbo.Split (@Clinics , ',')) 
       AND ProfessionalCarerCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Consultant, ','))
       )
       or @Reportlevel = 1 
       )

   
            GROUP BY 
               ClinicCode
               ,SessionDate
             ) OPIn
             ON  OPOut.ClinicCode = OPIn.ClinicCode 
             AND OPIn.MonthYear = CONVERT(CHAR(4), OPOut.SessionDate, 100) + CONVERT(CHAR(4), OPOut.SessionDate, 120)

WHERE     
	OPOut.SessionDate >= @StartDate 
	AND OPOut.SessionDate < @EndDate 
	AND OPOut.SessionIsActive = 'Y'
	AND OPOut.ExcludedClinic = 'N' 
	AND OPOut.ClinicCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Clinics, ',')) 
	AND OPOut.Division IN (SELECT Item FROM WHREPORTING.dbo.Split (@Division, ',')) 
	AND OPOut.Directorate IN (SELECT Item FROM WHREPORTING.dbo.Split (@Directorate, ',')) 
	AND OPOut.TreatmentFunctionCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Specialty, ','))
	AND OPOut.ProfessionalCarerCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Consultant, ','))
    AND
       (
       (
       @Reportlevel = 0 
       AND OPOut.ClinicCode  IN (SELECT Item FROM WHREPORTING.dbo.Split (@Clinics , ',')) 
       AND OPOut.ProfessionalCarerCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Consultant , ','))
       )
       or @Reportlevel = 1 
       )


GROUP BY 
 CONVERT(CHAR(4), OPOut.SessionDate, 100) + CONVERT(CHAR(4), OPOut.SessionDate, 120), OPIn.Counting