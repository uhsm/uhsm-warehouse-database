﻿/*NewtoReview1011*/
CREATE  Procedure [OP].[OPKPI_NewtoReview1011]
(
@StartDate datetime
,@EndDate   datetime
,@Division  nvarchar(max)  
,@Directorate nvarchar(max) 
,@Specialty nvarchar(max)
,@Clinics nvarchar(max)  
,@Consultant nvarchar(max) 
,@Reportlevel Int   
)
as 
set nocount on

/*
Declare	@StartDate datetime = '20150501'
Declare	@EndDate   datetime = '20150604'
Declare	@Division  nvarchar(255) = 'Scheduled Care'    
Declare @Directorate nvarchar(255) = 'Surgery'
Declare @Specialty nvarchar(255) = '100' 
Declare @Consultant nvarchar(255) =  'C3301993'
Declare @Clinics nvarchar(max)  = 'SWG1PR'
Declare @Reportlevel Int = 0 /*0 = Clinic and 1 = Trust*/
*/


SELECT     
SUM(CASE WHEN Status = 'Attend' AND AppointmentTypeNHSCode = 1 THEN 1 ELSE 0 END) AS CountOfNewAttend
,SUM(CASE WHEN Status = 'Attend' AND (AppointmentTypeNHSCode = 2 OR AppointmentTypeNHSCode IS NULL) THEN 1 ELSE 0 END) AS CountOfFollowUp
,CONVERT(CHAR(4), OP.AppointmentDate, 100) + CONVERT(CHAR(4),  OP.AppointmentDate, 120) AS MonthYear
FROM OP.Schedule OP 
	LEFT OUTER JOIN 
	vwExcludeWAWardsandClinics  Exc ON OP.ClinicCode = Exc.SPONT_REFNO_CODE
WHERE     
	OP.IsWardAttender = 0 
	AND Exc.SPONT_REFNO_CODE IS NULL
	AND OP.AttendStatus IN ('Attended', 'Attended on Time', 'Patient Late / Seen')
	AND (
		OP.AppointmentTypeNHSCode IN (1, 2)
		 OR
		OP.AppointmentTypeNHSCode IS NULL
		)
	AND OP.AppointmentDate >= DATEADD(year, - 1, @StartDate) 
	AND OP.AppointmentDate <= DATEADD(year, - 1, @EndDate)
	AND OP.Division IN (SELECT Item FROM WHREPORTING.dbo.Split (@Division, ',')) 
	AND OP.Directorate IN (SELECT Item FROM WHREPORTING.dbo.Split (@Directorate, ',')) 
	AND OP.[SpecialtyCode(Function)]IN (SELECT Item FROM WHREPORTING.dbo.Split (@Specialty, ',')) 
    AND
       (
       (
       @Reportlevel = 0 
       AND OP.ClinicCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Clinics , ',')) 
       AND OP.ProfessionalCarerCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Consultant , ','))
       )
       or @Reportlevel = 1 
       )
GROUP BY CONVERT(CHAR(4), OP.AppointmentDate, 100) + CONVERT(CHAR(4), OP.AppointmentDate, 120)