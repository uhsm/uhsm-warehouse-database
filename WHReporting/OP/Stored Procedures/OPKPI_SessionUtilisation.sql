﻿/*SessionUtilisation*/
CREATE Procedure [OP].[OPKPI_SessionUtilisation]
(
@StartDate datetime
,@EndDate   datetime
,@Division  nvarchar(max)  
,@Directorate nvarchar(max) 
,@Specialty nvarchar(max)
,@Clinics nvarchar(max)  
,@Consultant nvarchar(max)
,@Reportlevel Int  
  
)
as 
set nocount on

/*
Declare	@StartDate datetime = '20150501'
Declare	@EndDate   datetime = '20150604'
Declare	@Division  nvarchar(255) = 'Scheduled Care'    
Declare @Directorate nvarchar(255) = 'Surgery'
Declare @Specialty nvarchar(255) = '100' 
Declare @Consultant nvarchar(255) =  'C3301993'
Declare @Clinics nvarchar(max)  = 'SWG1PR'
Declare @Reportlevel Int = 0 /*0 = Clinic and 1 = Trust*/
*/

SELECT     
CONVERT(CHAR(4), SessionDate, 100) + CONVERT(CHAR(4), SessionDate, 120) AS MonthYear
,SUM(CASE WHEN SessionStatus IN ('Session Held', 'Session Initiated', 'Session Scheduled') THEN 1 ELSE 0 END) AS Utilised
,SUM(CASE WHEN SessionStatus 
		IN ('Session Cancelled', 'Session Held',  'Session Initiated', 'Session on Hold', 'Session Scheduled') 
		THEN 1 ELSE 0 END) AS TotalSessions
,0.8 AS SessionTarget
FROM OP.SessionUtilisation
WHERE     
		SessionDate >= @StartDate
		AND SessionDate <= @EndDate
		AND SessionIsActive = 'Y'
		AND ExcludedClinic = 'N' 
		AND ClinicDescription NOT LIKE '%TELE%' 
		AND Division IN (SELECT Item FROM WHREPORTING.dbo.Split (@Division, ',')) 
		AND Directorate IN (SELECT Item FROM WHREPORTING.dbo.Split (@Directorate, ',')) 
        AND TreatmentFunctionCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Specialty, ',')) 
        AND
       (
       (
       @Reportlevel = 0 
       AND ClinicCode NOT LIKE '%TELE%'
	   AND ClinicCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Clinics, ',')) 
       AND ProfessionalCarerCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Consultant , ','))
       )
       or @Reportlevel = 1 
       )

AND ProfessionalCarerCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Consultant, ',')) 
GROUP BY  CONVERT(CHAR(4), SessionDate, 100) + CONVERT(CHAR(4), SessionDate, 120)