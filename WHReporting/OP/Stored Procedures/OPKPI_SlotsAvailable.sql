﻿/*ds_SlotsAvailable*/

CREATE Procedure [OP].[OPKPI_SlotsAvailable]
(
@StartDate datetime
,@EndDate   datetime
,@Division  nvarchar(max)  
,@Directorate nvarchar(max) 
,@Specialty nvarchar(max)
,@Clinics nvarchar(max)  
,@Consultant nvarchar(max)
,@Reportlevel Int  
)
as 
set nocount on
/*
Declare	@StartDate datetime = '20150501'
Declare	@EndDate   datetime = '20150604'
Declare	@Division  nvarchar(255) = 'Scheduled Care'    
Declare @Directorate nvarchar(255) = 'Surgery'
Declare @Specialty nvarchar(255) = '100' 
Declare @Consultant nvarchar(255) =  'C3301993'
Declare @Clinics nvarchar(max)  = 'SWG1PR'
Declare @Reportlevel Int = 1 /*0 = Clinic and 1 = Trust*/
*/


SELECT     
	SUM(BookingsAllowed)                                                     AS BookingsAllowed
	,SUM(BookingsMade)														 AS BookingsMade
	,SUM(BookingsFree)                                                       AS BookingsFree
	,CONVERT(CHAR(4), SessionDate, 100) + CONVERT(CHAR(4), SessionDate, 120) AS MonthYear
FROM  OP.SessionUtilisation
WHERE (SessionDate >= @StartDate) 
       AND (SessionDate <= @EndDate) 
       AND (SessionIsActive = 'Y') 
       AND (IsCancelled = 'N') 
       AND (ExcludedClinic = 'N')
       AND (NOT (ClinicCode LIKE '%TELE%'))
       AND (NOT (ClinicDescription LIKE '%TELE%')) 
       AND Division IN (SELECT Item FROM WHREPORTING.dbo.Split(@Division, ',')) 
       AND SessionStatus IN ('Session Scheduled', 'Session Initiated', 'Session Held')
       AND Directorate IN (SELECT Item FROM WHREPORTING.dbo.Split (@Directorate, ','))
       AND TreatmentFunctionCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Specialty , ','))
       AND
       (
       (
       @Reportlevel = 0 
       AND ClinicCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Clinics , ',')) 
       AND ProfessionalCarerCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Consultant , ','))
       )
       or @Reportlevel = 1 
       )
       
       
GROUP BY CONVERT(CHAR(4), SessionDate, 100) + CONVERT(CHAR(4), SessionDate, 120)