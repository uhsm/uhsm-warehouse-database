﻿/*Specialty*/

CREATE Procedure [OP].[OPKPI_Specialty]
(
@Division  nvarchar(max)  
,@Directorate nvarchar(max) 
)
as 

set nocount on
/*
Declare	@Division  nvarchar(255) = 'Unscheduled Care'    
Declare @Directorate nvarchar(255) = 'Medical Specialities'
*/

/*	
Original code from SSRS design view translates in format below. It cannot, however cope with cascading o
parameters when move to a stored procedures, falling over on presence of 
 "AND  TreatmentFunction NOT IN ('Trauma & Orthopaedics', 'Oncology')"
 and 
"AND TreatmentFunctionCode = '301A'" in the where clause
as its required the results of the query to be able to apply these where clauses. 
Now runs from a two stage process that applies these in the where of the temp table.
(
	 Division in (SELECT Item FROM WHREPORTING.dbo.Split(@Division, ',')) 
     AND Directorate IN (SELECT Item FROM WHREPORTING.dbo.Split (@Directorate, ','))
     AND ISNUMERIC(SpecialtyCode) = 1
 AND  TreatmentFunction NOT IN ('Trauma & Orthopaedics', 'Oncology')#
     ) 
     OR
     (
	 Division in (SELECT Item FROM WHREPORTING.dbo.Split(@Division, ',')) 
     AND Directorate IN (SELECT Item FROM WHREPORTING.dbo.Split (@Directorate, ','))
   --  AND TreatmentFunctionCode = '301A'
     )
*/

SELECT DISTINCT 
	Division
	,Directorate
	,TreatmentFunctionCode
	,TreatmentFunction
	,ISNUMERIC(SpecialtyCode) as Specialtcode
into #results
FROM OP.SessionUtilisation
WHERE  
	 Division in (SELECT Item FROM WHREPORTING.dbo.Split(@Division, ',')) 
     AND Directorate IN (SELECT Item FROM WHREPORTING.dbo.Split (@Directorate, ','))

/*xxxxxxxxxxxxxxxxxx*/

Select
    TreatmentFunctionCode
	,TreatmentFunction
From #results
where 
(
  Specialtcode = 1
 AND  TreatmentFunction NOT IN ('Trauma & Orthopaedics', 'Oncology')
)
or 
(TreatmentFunctionCode = '301A') 
ORDER BY TreatmentFunction

/*xxxxxxxxxxxxxxxxxx*/

Drop table #results