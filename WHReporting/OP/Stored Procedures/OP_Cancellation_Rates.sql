﻿/*
Number of times each patient an OP appointment cancelled by Hospital by Division, Directorate, Clinician
NB should include all patient OP appointments that have been cancelled by Hospital within time period
NB should count at clinician level and aggregate up. ie patients who are under multiple doctors should be counted separately for each.
Each number within the body of the table is a count of individual patients. 
Report should include patient details so that the user may click on the numbers to reveal identifiers.
SSRS report should have filters: Date, Division, Directorate, Specialty, Clinician, Count of Hospital Cancellations
=============================================
 Author:		GFenton	
 Create date: 28/04/2015
 Description:	Built to Power SSRS report following requested from Alsion Olivant 
 =============================================*
 */

CREATE Procedure [OP].[OP_Cancellation_Rates]
(
	@StartDate datetime
	,@EndDate datetime 
	,@Division nvarchar(255) 
	,@Directorate nvarchar(255) 
	,@Specialty  nvarchar(255) 
	,@Clinician nvarchar(255) 
    ,@NoofCans int
    ,@NoofCansDrill nvarchar(255)
)

AS 
Set NoCount on
/*
Declare @StartDate datetime = '20150401'
Declare @EndDate datetime = '20150522'
Declare @Division nvarchar(255) = 'Scheduled Care'
Declare @Directorate nvarchar(255) ='Surgery'
Declare @Specialty  nvarchar(255) = 'General Surgery'
Declare @Clinician nvarchar(255) = 'C4118514'
Declare @NoofCans int = 0        /*0 =  all cancellations 1 =  selcted no of cancellations(used in drill through)*/
Declare @NoofCansDrill nvarchar(255) = 1 /*No of cancellations used in drilthrough*/
*/

/*xxxxxxxxxxxxxxxxxxxxxxxxx*/
/*No of Cancelations's by NHSno at Division, Directorate, Spec and Clinician*/
Select distinct
	OP.Division
	,OP.Directorate
	,OP.[Specialty(Function)]
	,OP.ProfessionalCarerName
	,OP.SourcePatientNo
	,COUNT(OP.SourcePatientNo)            as Cancelcount
into #NoofCancs
From   OP.Schedule AS OP
        Left Join  vwExcludeWAWardsandClinics AS Exc
                    ON OP.ClinicCode = Exc.SPONT_REFNO_CODE
Where 
       ( OP.AppointmentDate >= @StartDate )
       and ( OP.AppointmentDate <= @EndDate )
       and ( OP.IsWardAttender = 0 )
       and ( Exc.SPONT_REFNO_CODE IS NULL )
       and ( OP.AppointmentTypeNHSCode IN ( 1, 2 )
              OR OP.AppointmentTypeNHSCode IS NULL )
       and (op.AttendStatusNHSCode = 4 ) 
       /*
       2 - APPOINTMENT cancelled by, or on behalf of, the PATIENT 
       4 - APPOINTMENT cancelled or postponed by the Health Care Provider 
       */
       and ( OP.AdministrativeCategory NOT LIKE '%Private%' )
group by 
	OP.Division
	,OP.Directorate
	,OP.[Specialty(Function)]
    ,OP.ProfessionalCarerName
    ,OP.NHSNo
    ,OP.SourcePatientNo

/*xxxxxxxxxxxxxxxxxxxxxxxxx*/
/*Compiling Results*/
Select distinct
	OP.Division
	,OP.Directorate
	,OP.[SpecialtyCode(Function)]
	,OP.[Specialty(Function)]
    ,OP.SourcePatientNo
	,OP.NHSNo
	,OP.ClinicCode
	,OP.CancelledDateTime
	,OP.[Status]
	,OP.AppointmentCreated
    ,OP.AppointmentDate
    ,DATEDIFF(d,OP.AppointmentCreated,OP.AppointmentDate) as BookedtoAppt_days
	,OP.ProfessionalCarerCode
	,OP.ProfessionalCarerName
	,1                                                     as Count_Value
	,dna.Cancelcount                                        /*used in sort on SSRS*/
	,Case when dna.Cancelcount = 1 then '1'
	  when dna.Cancelcount  = 2 then '2'  
	  when dna.Cancelcount  = 3 then '3'
	  when dna.Cancelcount  = 4 then '4'
	  when dna.Cancelcount  = 5 then '5'
	  when dna.Cancelcount  >= 6 then '6+'   
	  else '0' end                                          as No_of_Cancellations 
into #Results
From   OP.Schedule OP
       Left Join vwExcludeWAWardsandClinics Exc
          on OP.ClinicCode = Exc.SPONT_REFNO_CODE
       Left join #NoofCancs dna 
          on OP.Division = dna.Division
	     and OP.Directorate = dna.Directorate
		 and OP.[Specialty(Function)] = dna.[Specialty(Function)]
         and OP.SourcePatientNo = dna.SourcePatientNo
Where 
       ( OP.AppointmentDate >= @StartDate )
       and ( OP.AppointmentDate <= @EndDate )
       and ( OP.IsWardAttender = 0 )
       and ( Exc.SPONT_REFNO_CODE IS NULL )
       and ( OP.AppointmentTypeNHSCode IN ( 1, 2 )
              OR OP.AppointmentTypeNHSCode IS NULL )
              and (op.AttendStatusNHSCode = 4) 
       /*
       2 - APPOINTMENT cancelled by, or on behalf of, the PATIENT 
       4 - APPOINTMENT cancelled or postponed by the Health Care Provider 
       */

       and ( OP.AdministrativeCategory NOT LIKE '%Private%' )
	    and OP.[Division]  IN (SELECT Item FROM WHREPORTING.dbo.Split (@Division , ','))
		and OP.Directorate IN (SELECT Item FROM WHREPORTING.dbo.Split (@Directorate , ','))
		and OP.[Specialty(Function)] IN (SELECT Item FROM WHREPORTING.dbo.Split (@Specialty , ','))
		and OP.ProfessionalCarerCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Clinician , ',')) 
Order by 
	OP.Division
	,OP.Directorate
	,OP.[Specialty(Function)]
	,OP.ProfessionalCarerName
	,OP.NHSNo
	,OP.AppointmentDate asc
/*xxxxxxxxxxxxxxxxxxxxxxxxx*/
Select * from #Results
where 
     @NoofCans = 0
     or 
     (@NoofCans = 1 and  No_of_Cancellations = @NoofCansDrill)

/*xxxxxxxxxxxxxxxxxxxxxxxxx*/
Drop table #NoofCancs, #Results