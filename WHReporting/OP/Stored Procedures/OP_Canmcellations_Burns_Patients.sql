﻿/*
SSRS for Burns Team.  This team want a list of patients who either DNA or cancel 
their appointment (the patient cancels as opposed to the hospital) in a particular 
period to check and see if they have been given a new appointment date.  
Can you develop a report that looks at this (just for the Burns specialty).  
Think the column headings should be: Patient NHS Number; Patient RM2 Number, 
Patient Name, Patient Surname; Patient Date of Birth; Date of Appointment; 
Date appointment was cancelled; Date appointment was DNAd; Date of patient’s next appointment 
in Burns (which would be blank if they don’t have another appointment sorted).
 =============================================
 Author:		GFenton
 Create date:  18/6/2015
 Description:	List of Cancellation patients for Burns team with subsequent attendances
 =============================================
*/
CREATE Procedure [OP].[OP_Canmcellations_Burns_Patients]
(
	@StartDate datetime
	,@EndDate datetime 
)

AS 

Set NoCount on
/*
Declare @StartDate datetime = '20150501'
Declare @EndDate datetime = '20150623'
*/
Select distinct
	o.FacilityID					   as Patient_RM2_number  
	,p.NHSNo           
	,p.PatientTitle
	,p.PatientForename
	,p.PatientSurname
	,p.DateOfBirth
	,o.AppointmentDateTime
    ,n.AppointmentDate			        as SubsequentApptDate
	,n.AppointmentDateTime 			    as SubsequentApptDateTime 
	,Case when cast(n.AppointmentDate as DATE) >= DATEADD(d,0,DATEDIFF(d,0,GETDATE()))
	 then 'Future Appointment'
	 else n.[Status] 
	 end                                as [Status]  
	,1                                  as Value
into #results
 From OP.Schedule o 
    Left Join vwExcludeWAWardsandClinics Exc
     on o.ClinicCode = Exc.SPONT_REFNO_CODE
    Left Join OP.Patient p 
     on o.FacilityID = p.FacilityID
left join 
/*sub query pulls through all remaining appts for Burns by patient*/
(	
	Select 
		 n.FacilityID
		,n.AppointmentDate
		,n.AppointmentDateTime
		,n.[Status]
	From OP.Schedule n
	Where [Specialty(Function)] = 'Burns Care' 
) n  on o.FacilityID = n.FacilityID and n.AppointmentDate > o.AppointmentDate 
Where 
/*Same rules as OP_Cancellation_Rates stores Procedure*/
       	 o.[Specialty(Function)] = 'Burns Care' 
       and  o.AppointmentDate >= @StartDate 
       and  o.AppointmentDate <= @EndDate  
       and  o.IsWardAttender = 0  
       and   Exc.SPONT_REFNO_CODE IS NULL  
       and   (
               o.AppointmentTypeNHSCode IN ( 1, 2 )
              OR o.AppointmentTypeNHSCode IS NULL 
              )
       and (o.AttendStatusNHSCode = 4) 
       /*
       2 - APPOINTMENT cancelled by, or on behalf of, the PATIENT 
       4 - APPOINTMENT cancelled or postponed by the Health Care Provider 
       */
       and o.AdministrativeCategory NOT LIKE '%Private%' 
    and p.DateOfDeath is null
group by 	
    o.FacilityID					 
	,p.NHSNo           
	,p.PatientTitle
	,p.PatientForename
	,p.PatientSurname
	,p.DateOfBirth
	,o.AppointmentDateTime
	,n.AppointmentDateTime 			
	,n.[Status]
	,n.AppointmentDate
/*xxxxxxxxxxxxxxxxxxxxxxxxx*/	
/*Results with row number to rank subsequent appt dates into order*/
Select 
	r.Patient_RM2_number	
	,r.NHSNo	
	,r.PatientTitle	
	,r.PatientForename	
	,r.PatientSurname	
	,r.DateOfBirth	
	,r.AppointmentDateTime	
	,ROW_NUMBER() over(Partition by r.Patient_RM2_number  Order by r.SubsequentApptDateTime) as Rankno
	,r.SubsequentApptDateTime 
	,r.[Status]	
	,r.Value	
From #results r 

/*xxxxxxxxxxxxxxxxxxxxxxxxx*/	

Drop table #results