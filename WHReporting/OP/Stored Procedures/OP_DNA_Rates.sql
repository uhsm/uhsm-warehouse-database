﻿/*
NB only includes DNAs where appointment booked date at least three weeks before appointment date.
NB should count at clinician level and aggregate up. ie patients who are under multiple doctors should be counted separately for each.
Each number within the body of the table is a count of individual patients. 
Report should include patient details so that the user may click on the numbers to reveal identifiers.
SSRS report should have filters: Date, Division, Directorate, Specialty, Clinician, Count of DNAs


 =============================================
 Author:		GFenton
 Create date: 28/04/2015
 Description:	Built to Power SSRS report following requested from Alsion Olivant 
 =============================================
*/
CREATE Procedure [OP].[OP_DNA_Rates]
(
	@StartDate datetime
	,@EndDate datetime 
	,@Division nvarchar(255) 
	,@Directorate nvarchar(255) 
	,@Specialty  nvarchar(255) 
	,@Clinician nvarchar(255)
  ,@NoofDNAs int
  ,@NoofDNAsDrill nvarchar(255)
 
)

AS 

Set NoCount on

/*
Declare @StartDate datetime = '20150401'
Declare @EndDate datetime = '20150511'
Declare @Division nvarchar(255) = 'Scheduled Care'
Declare @Directorate nvarchar(255) ='Surgery'
Declare @Specialty  nvarchar(255) = 'General Surgery,ORTH-FRACTURE CLINIC,Trauma & Orthopaedic,Urology,Vascular Surgery'
Declare @Clinician nvarchar(255) = 'C4118514'
Declare @NoofDNAs int = 1
Declare @NoofDNAsDrill nvarchar(255) = 1
*/


/*xxxxxxxxxxxxxxxxxxxxxxxxx*/
/*No of DNA's by NHSno at Division, Directorate, Spec and Clinician*/
Select distinct
	OP.Division
	,OP.Directorate
	,OP.[Specialty(Function)]
	,OP.ProfessionalCarerName
	,OP.SourcePatientNo
	,COUNT(OP.SourcePatientNo)            as DNAcount /*used to calculate the number cancels per person*/
into #NoofDNAs
From   OP.Schedule AS OP
        Left Join  vwExcludeWAWardsandClinics AS Exc
                    ON OP.ClinicCode = Exc.SPONT_REFNO_CODE
Where 
       ( OP.AppointmentDate >= @StartDate )
       and ( OP.AppointmentDate <= @EndDate )
       and ( OP.IsWardAttender = 0 )
       and ( Exc.SPONT_REFNO_CODE IS NULL )
       and ( OP.AppointmentTypeNHSCode IN ( 1, 2 )
              OR OP.AppointmentTypeNHSCode IS NULL )
       and ( OP.Status = 'DNA' /*IN ( 'DNA', 'ATTEND' )*/ )
       and ( OP.AdministrativeCategory NOT LIKE '%Private%' )
and DATEDIFF(d,OP.AppointmentCreated,OP.AppointmentDate) > 21 
group by 
	OP.Division
	,OP.Directorate
	,OP.[Specialty(Function)]
    ,OP.ProfessionalCarerName
    ,OP.NHSNo
    ,OP.SourcePatientNo

/*xxxxxxxxxxxxxxxxxxxxxxxxx*/
/*Compiling Results*/
Select distinct
	OP.Division
	,OP.Directorate
	,OP.[SpecialtyCode(Function)]
	,OP.[Specialty(Function)]
    ,OP.SourcePatientNo
	,OP.NHSNo
	,OP.ClinicCode
	,OP.CancelledDateTime
	,OP.[Status]
	,OP.Outcome
	,OP.AppointmentCreated
    ,OP.AppointmentDate
    ,DATEDIFF(d,OP.AppointmentCreated,OP.AppointmentDate) as BookedtoAppt_days
	,OP.ProfessionalCarerCode
	,OP.ProfessionalCarerName
	,1                                                     as Count_Value
	,dna.DNAcount                                    /*used in sort on SSRS*/
	,Case when dna.DNAcount = 1 then '1'
	  when dna.DNAcount = 2 then '2'  
	  when dna.DNAcount = 3 then '3'
	  when dna.DNAcount = 4 then '4'
	  when dna.DNAcount = 5 then '5'
	  when dna.DNAcount >= 6 then '6+'   
	  else '0' end                                          as No_of_DNAs 
into #Results
From   OP.Schedule OP
       Left Join vwExcludeWAWardsandClinics Exc
          on OP.ClinicCode = Exc.SPONT_REFNO_CODE
       Left join #NoofDNAs dna 
          on OP.Division = dna.Division
	     and OP.Directorate = dna.Directorate
		 and OP.[Specialty(Function)] = dna.[Specialty(Function)]
         and OP.SourcePatientNo = dna.SourcePatientNo
Where 
       ( OP.AppointmentDate >= @StartDate )
       and ( OP.AppointmentDate <= @EndDate )
       and ( OP.IsWardAttender = 0 )
       and ( Exc.SPONT_REFNO_CODE IS NULL )
       and ( OP.AppointmentTypeNHSCode IN ( 1, 2 )
              OR OP.AppointmentTypeNHSCode IS NULL )
       and ( OP.Status = 'DNA' /*IN ( 'DNA', 'ATTEND' )*/ )
       and ( OP.AdministrativeCategory NOT LIKE '%Private%' )
       and DATEDIFF(d,OP.AppointmentCreated,OP.AppointmentDate) > 21 
	    and OP.[Division]  IN (SELECT Item FROM WHREPORTING.dbo.Split (@Division , ','))
		and OP.Directorate IN (SELECT Item FROM WHREPORTING.dbo.Split (@Directorate , ','))
		and OP.[Specialty(Function)] IN (SELECT Item FROM WHREPORTING.dbo.Split (@Specialty , ','))
		and OP.ProfessionalCarerCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Clinician , ',')) 
Order by 
	OP.Division
	,OP.Directorate
	,OP.[Specialty(Function)]
	,OP.ProfessionalCarerName
	,OP.NHSNo
	,OP.AppointmentDate asc
/*xxxxxxxxxxxxxxxxxxxxxxxxx*/
Select * from #Results
where 
     @NoofDNAs = 0
     or 
     (@NoofDNAs = 1 and  No_of_DNAs  = @NoofDNAsDrill)
/*xxxxxxxxxxxxxxxxxxxxxxxxx*/

Drop table #NoofDNAs, #Results