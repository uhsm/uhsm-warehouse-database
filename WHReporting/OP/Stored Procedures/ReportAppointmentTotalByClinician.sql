﻿/******************************************************************************
 Description: Outpatients - AppointmentTotalByClinician
  
 Parameters:  @StartDate - Start date for the report
              @EndDate - End date for the report
              @Specialty - Specialties required on the report
              @Status - Referral status
              @Total - Total number of appointments attended 
 
 Modification History --------------------------------------------------------

 Date     Author		Change
 09/03/15 Heather Slim	Created


******************************************************************************/
CREATE PROCEDURE [OP].[ReportAppointmentTotalByClinician]

 @StartDate AS DATE 
	,@EndDate AS DATE 
	,@Specialty AS NVARCHAR(2000)
	,@ClinicCode AS NVARCHAR(2000) 
	,@Status AS NVARCHAR(20) 
	,@Total AS INT
as

SET nocount ON

IF Object_id('tempdb..#Specialty', 'U') IS NOT NULL
	DROP TABLE #Specialty

SELECT Item
INTO #Specialty
FROM WHREPORTING.dbo.Splitstrings_cte(@Specialty, N',')

IF Object_id('tempdb..#ClinicCode', 'U') IS NOT NULL
	DROP TABLE #ClinicCode

SELECT Item
INTO #ClinicCode
FROM WHREPORTING.dbo.Splitstrings_cte(@ClinicCode, N',')

SELECT s.[ProfessionalCarerName]
	,s.[FacilityID]
	,COUNT(*) AS CountOfAppointments
FROM [WHREPORTING].[OP].[Schedule] s
INNER JOIN #Specialty sp ON s.[Specialty] = sp.Item
INNER JOIN #ClinicCode cc ON s.[ClinicCode] = cc.Item
LEFT JOIN RF.Referral r ON s.ReferralSourceUniqueID = r.ReferralSourceUniqueID
WHERE s.AppointmentDate BETWEEN @StartDate
		AND @EndDate
	AND s.[Status] = 'Attend'
	AND (
		CASE 
			WHEN r.CompletionDate IS NULL
				THEN 'Open'
			ELSE 'Closed'
			END
		) IN (
		SELECT Item
		FROM WHREPORTING.dbo.Splitstrings_cte(@Status, N',')
		)
GROUP BY s.[ProfessionalCarerName]
	,s.[FacilityID]
HAVING COUNT(*) >= @Total