﻿/******************************************************************************
 Description: Outpatients - AppointmentTotalByClinician_DrillThrough
  
 Parameters:  @StartDate - Start date for the report
              @EndDate - End date for the report
              @Specialty - Specialties required on the report
              @Status - Referral status
              @Total - Total number of appointments attended 
 
 Modification History --------------------------------------------------------

 Date     Author		Change
 19/03/15 Heather Slim	Created


******************************************************************************/
CREATE PROCEDURE [OP].[ReportAppointmentTotalByClinician_DrillThrough]

 @StartDate AS DATE 
	,@EndDate AS DATE 
	,@Specialty AS NVARCHAR(2000)
	,@ProfessionalCarerName AS NVARCHAR(2000)
	,@ClinicCode AS NVARCHAR(2000) 
	,@Status AS NVARCHAR(20) 
	,@Total AS INT
as

SET nocount ON

--declare @StartDate AS DATE = '20150601'
--	declare @EndDate AS DATE = '20150630'
--	declare @Specialty AS NVARCHAR(2000)= 'Physiotherapy'
--	declare @ProfessionalCarerName AS NVARCHAR(2000)= 'Parke, T (TAFO)'
--	declare @ClinicCode AS NVARCHAR(2000) = 'PHYSMAT'
--	declare @Status AS NVARCHAR(20) = 'Open'
--	declare @Total AS INT = 2

--select 

-- @StartDate as startdate
--	,@EndDate as enddate
--	,@Specialty as specialty
--	,@ProfessionalCarerName as professionalcarername
--	,@ClinicCode as cliniccode
--	,@Status as [status]
--	,@Total as total
--into whreporting.op.[check]	
	

IF Object_id('tempdb..#Specialty', 'U') IS NOT NULL
	DROP TABLE #Specialty

SELECT Item
INTO #Specialty
FROM WHREPORTING.dbo.Splitstrings_cte(@Specialty, N',')

IF Object_id('tempdb..#ClinicCode', 'U') IS NOT NULL
	DROP TABLE #ClinicCode

SELECT Item
INTO #ClinicCode
FROM WHREPORTING.dbo.Splitstrings_cte(@ClinicCode, N',')

SELECT s.[ProfessionalCarerName]
	,s.[FacilityID]
	,max(s.NHSNo) as NHSNo
	,max(p.PatientSurname) as PatientSurname
	,max(p.PatientForename)as PatientForname
	,COUNT(*) AS CountOfAppointments
FROM [WHREPORTING].[OP].[Schedule] s
INNER JOIN #Specialty sp ON s.[Specialty] = sp.Item
INNER JOIN #ClinicCode cc ON s.[ClinicCode] = cc.Item
LEFT JOIN RF.Referral r ON s.ReferralSourceUniqueID = r.ReferralSourceUniqueID
left join OP.Patient p on s.EncounterRecno = p.EncounterRecno
WHERE s.AppointmentDate BETWEEN @StartDate
		AND @EndDate
	AND s.[Status] = 'Attend'
	AND (
		CASE 
			WHEN r.CompletionDate IS NULL
				THEN 'Open'
			ELSE 'Closed'
			END
		) IN (
		SELECT Item
		FROM WHREPORTING.dbo.Splitstrings_cte(@Status, N',')
		)
		and s.[ProfessionalCarerName] = @ProfessionalCarerName
GROUP BY 
s.[ProfessionalCarerName]
	,s.[FacilityID]
HAVING COUNT(*) = @Total