﻿-- =============================================
-- Author:		CMan 
-- Create date: 09/01/2014
-- Description:	Stored procedure for report to show a list of patients who have had an appointment in a specific list of Clinic Codes (clinic codes given by Mohammed Owaise)
--			but have not had an appointment in these clinic within the 3 month preceding the date of the report being run
--			Report reuqested by Mohammad Owaise via Paula Mcconnell on 17/12/2013
--			
--			RD451
-- =============================================
CREATE PROCEDURE [OP].[ReportOPClinicPatientsNotSeenLast3Months] @Clinic varchar(20), @seensince datetime

	-- Add the parameters for the stored procedure here
AS
BEGIN
SET nocount ON
 
--Note:  need to check with Owaise if we need to know if the patient has not had an appointment within the same clinic or just not had an appointment within this list of clinics?

--------------------------------------------------------------
--set parameter to get the a start date for identifying the last 3 months from date of running report
-----------------------------------------------------------
DECLARE @start AS DATETIME
SET @start = Dateadd(Month, -3,convert(datetime,'01/' + Datename(Month,GETDATE()) + '/' +   Datename(Year,GETDATE()))) --use start date of the month for getdate and then look back 3 months.  this way it will not matter at what point in the month the report gets run


;

SET nocount ON;
--------------------------------------------------------------
--Cohort of appointments for the specified list of clinic code where the appointment date is within the last 3 months.
--temp table to be used in final query to find all patients who are not within this cohort
--------------------------------------------------------------
With OP (AppointmentDateTime,Status,AttendStatus,ClinicCode,FacilityID,SourceUniqueID,SourcePatientNo)
as 
(
SELECT OP.AppointmentDateTime,
       OP.Status,
       OP.AttendStatus,
       OP.ClinicCode,
       OP.FacilityID,
       OP.SourceUniqueID,
       OP.SourcePatientNo
FROM   WHREPORTING.OP.Schedule OP
WHERE  ClinicCode = @clinic 
--IN ( '1RESPR', '2RESPR', 'RTWTRE', 'SRSCAL', 'RG1CAL', 'RG22TAM' )
       AND AppointmentDate >=  @start -- do not need an end date as we ned to know about patient who have appointments in the future too
		AND AppointmentDate < dateadd(day, datediff(day, 0, GETDATE()), 0)
)
,

FutureApptSameClinic (sourcePatientNo,ClinicCode,NextApptDateSameClinic)
as 
(
Select OP.SourcePatientNo,OP.ClinicCode,min(OP.AppointmentDateTime) as NextApptDateSameClinic
FROM   WHREPORTING.OP.Schedule OP
where ClinicCode in (@Clinic)
and AppointmentDate >= dateadd(day, datediff(day, 0, GETDATE()), 0)
Group by OP.SourcePatientNo,OP.ClinicCode
)
, 

FutureApptAllClinic (sourcePatientNo,NextApptDate,ClinicCode)
as 
(
Select MinOP.SourcePatientNo,min(MinOP.AppointmentDateTime) as NextApptDate, OPDetails.ClinicCode
FROM   WHREPORTING.OP.Schedule MinOP
Left outer join (Select SourcePatientNo, SourceUniqueID, ClinicCode, AppointmentDateTime from  WHREPORTING.OP.Schedule  where ClinicCode in ( '1RESPR', '2RESPR', 'RTWTRE', 'SRSCAL', 'RG1CAL', 'RG2TAM', 'RG2TPM ' ,'RTTPMR')) as OPDetails
on  MinOP.SourcePatientNo =  OPDetails.SourcePatientNo
where MinOP.ClinicCode in ( '1RESPR', '2RESPR', 'RTWTRE', 'SRSCAL', 'RG1CAL', 'RG2TAM', 'RG2TPM ' ,'RTTPMR')
and MinOP.AppointmentDate >= dateadd(day, datediff(day, 0, GETDATE()), 0)
Group by MinOP.SourcePatientNo, OPDetails.ClinicCode,OPDetails.AppointmentDateTime
Having min(MinOP.AppointmentDateTime) = OPDetails.AppointmentDateTime
)
,

-------------------------------------------------------------
--Cohort of patients who have had an appointment in the list of clinics specified by Owaise and have an open referral still at the point of the report being run
--temp table to be used in final query with above temp table to find list of patients who have previously attended these clinics but have not had an appointment in them witin the last 3 motnhs
--------------------------------------------------------------
OP_ALL (ReferralReceivedDate,CompletionDate,CompletionReason,CancelDate,AppointmentDateTime,Status,
                AttendStatus, ClinicCode,FacilityID,SourcePatientNo, PatientSurname,PatientForename, Outcome)
as 
(
SELECT DISTINCT RF.ReferralReceivedDate,
                RF.CompletionDate,
                RF.CompletionReason,
                RF.CancelDate,
                OP.AppointmentDateTime,
                OP.Status,
                OP.AttendStatus,
                OP.ClinicCode,
                OP.FacilityID,
                OP.SourcePatientNo,
                Pat.PatientSurname,
                Pat.PatientForename,
                OP.Outcome
FROM   WHREPORTING.OP.Schedule OP
       LEFT OUTER JOIN WHREPORTING.RF.Referral RF
                    ON OP.ReferralSourceUniqueID = RF.ReferralSourceUniqueID
       LEFT OUTER JOIN WHREPORTING.OP.Patient Pat
                    ON OP.SourcePatientNo = Pat.SourcePatientNo
WHERE  ClinicCode = @Clinic --IN ( '1RESPR', '2RESPR', 'RTWTRE', 'SRSCAL', 'RG1CAL', 'RG22TAM' )
       AND RF.CompletionDate IS NULL
      AND  OP.AppointmentDateTime >= @seensince

)

--------------------------------------------------------------
--Final Query to find the list of patients who have not had an appointment within last 3 months - includes details of the last appointment 
--------------------------------------------------------------
SELECT OP_ALL.FacilityID,
       OP_ALL.PatientForename,
       OP_ALL.PatientSurname,
       OP_ALL.SourcePatientNo,
       B2.appointmentdatetime LastAppointmentDate,
       B2.ClinicCode          AS ClinicCodeOfLastAppointment,
       B2.status              AS StatusOfLastAppointment,
       B2.Outcome as OutcomeofLastAppointment,
       B2.ReferralReceivedDate,
       B2.CompletionDate as ReferralCompletedDate,
       @start PeriodStart,
        FASame.ClinicCode,
       FASame.NextApptDateSameClinic,
       FApp_ALL.ClinicCode ClinicCodeofNextFutureAppointment,
       FApp_ALL.NextApptDate
FROM   OP_ALL --this is the temp table with the appointments within the list of clinics where the attached referral is still open
       LEFT OUTER JOIN OP_ALL AS B2 -- join back on itself to get the rest of the details for the latest appointment
                    ON OP_ALL.sourcepatientno = B2.sourcepatientno and  B2.appointmentdatetime < GETDATE()
     LEFT OUTER JOIN FutureApptSameClinic as FASame --to get details of any future appointment in the same clinic
					ON OP_ALL.SourcePatientNo = FASame.sourcePatientNo
       
       LEFT OUTER JOIN FutureApptAllClinic as FApp_ALL--to get any future appointment details within any of the listed clinics
					ON OP_ALL.SourcePatientNo = FApp_ALL.sourcePatientNo
                    
                    
WHERE  NOT EXISTS (SELECT *
                   FROM   OP -- this is the temp table with the list of appointments for the specified list of clinics within the last 3 months
                   WHERE  OP.SourcePatientNo = OP_ALL.SourcePatientNo)


GROUP  BY OP_ALL.FacilityID,
          OP_ALL.PatientForename,
          OP_ALL.PatientSurname,
          OP_ALL.SourcePatientNo,
          B2.appointmentdatetime,
          B2.ReferralReceivedDate,
          B2.ClinicCode,
          B2.status,
          B2.Outcome,
          B2.CompletionDate,
           FASame.ClinicCode,
       FASame.NextApptDateSameClinic,
       FApp_ALL.ClinicCode,
       FApp_ALL.NextApptDate
HAVING Max(OP_ALL.appointmentdatetime) = B2.appointmentdatetime
ORDER  BY 1 



END