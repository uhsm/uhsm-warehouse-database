﻿-- =============================================
-- Author:		CMan
-- Create date: 12/02/2014
-- Description:	Re-created script from the original script saved in SSRS report 'OPKPI by Clinic' 
-- =============================================
CREATE PROCEDURE [OP].[ReportOPKPIByClinic_DNAs] @StartDate DateTime, @EndDate Datetime, @Division varchar(200), @Directorate varchar(max), @Specialty varchar(max), @Clinics varchar(max), @Consultant varchar(max)




AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


If OBJECT_ID('tempdb..#OP') > 0
Drop table #OP
;
Create table #OP (SourceUniqueID int,
					AppointmentDate datetime, 
					ApptType varchar(50),
					Attends int,
					DNA int,
					Total int,
					Division varchar(100),
					Directorate varchar(200),
					SpecialtyCodeFunction varchar(10),
					ClinicCode  varchar(20),
					ProfessionalCarerCode    varchar(20) 
					)
;		
Insert into #OP

SELECT OP.SourceUniqueID,
		OP.AppointmentDate,
		CASE
         WHEN AppointmentTypeNHSCode = 1 THEN 'New'
         WHEN AppointmentTypeNHSCode = 2 THEN 'Follow Up'
         WHEN AppointmentTypeNHSCode IS NULL THEN 'Follow Up'
       END                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           AS ApptType,
       --0.075                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         AS DNATarget,
       CASE
             WHEN AttendStatus IN ( 'Attended', 'Attended on Time', 'Patient Late / Seen' )
                  AND ( outcome IS NULL
                         OR ( Outcome NOT LIKE '%canc%'
                              AND Outcome NOT LIKE '%Did Not Attend%' ) ) THEN 1
             ELSE 0
           END
        as Attends,
       CASE
             WHEN AttendStatus IN ( 'Refused to Wait', 'Patient Left / Not seen', 'Patient Late / Not Seen', 'Did Not Attend' )
                   OR ( AttendStatus IN ( 'Attended', 'Attended on Time', 'Patient Late / Seen' )
                        AND Outcome = 'Did Not Attend' ) THEN 1
             ELSE 0
           END
       as DNA,
       CASE WHEN AttendStatus IN ('Attended', 'Attended on Time', 'Patient Late / Seen') AND (outcome IS NULL OR (Outcome NOT LIKE '%canc%' AND Outcome NOT LIKE '%Did Not Attend%')) THEN 1
			WHEN AttendStatus IN ('Refused to Wait', 'Patient Left / Not seen', 'Patient Late / Not Seen', 'Did Not Attend') OR (AttendStatus IN ('Attended', 'Attended on Time', 'Patient Late / Seen') AND Outcome = 'Did Not Attend') THEN 1 
		ELSE 0 END                                             AS Total
        , OP.Division
        , OP.Directorate
        ,  OP.[SpecialtyCode(Function)]  
        ,	OP.ClinicCode 
        , OP.ProfessionalCarerCode                                                                                                                                                                                                                                                                                                                                                                                                                                                 AS MonthYear
FROM   OP.Schedule AS OP
       LEFT OUTER JOIN vwExcludeWAWardsandClinics AS Exc
                    ON OP.ClinicCode = Exc.SPONT_REFNO_CODE
WHERE  ( OP.AppointmentDate >= @StartDate )
       AND ( OP.AppointmentDate <= @EndDate )
       AND ( OP.IsWardAttender = 0 )
       AND ( Exc.SPONT_REFNO_CODE IS NULL )
       AND ( OP.AppointmentTypeNHSCode IN ( 1, 2 )
              OR OP.AppointmentTypeNHSCode IS NULL )
       AND ( OP.Status IN ( 'DNA', 'ATTEND' ) )
       AND ( OP.AdministrativeCategory NOT LIKE '%Private%' )
       AND ( OP.Division IN (SELECT Item
                                           FROM   dbo.Split(@Division, ',') AS Split_1) )
	 AND ( OP.Directorate IN (SELECT Item
                                          FROM   dbo.Split(@Directorate, ',') AS Split_1) )
     --AND (  OP.[SpecialtyCode(Function)]   IN (SELECT Item
     --                                    FROM   dbo.Split(@Specialty, ',') AS Split_1) )
  --AND 
  --  ( OP.ClinicCode IN (SELECT Item1
  --                           FROM   dbo.Split1(@Clinics, ',') AS Split_1) )
       --AND ( OP.ProfessionalCarerCode IN (SELECT Item
       --                                    FROM   dbo.Split( @Consultant,  ',') AS Split_1) ) 

;



Select 
		ApptType, 
		0.075    DNATarget,
		SUM(Attends) as Attends,
		SUM(DNA) as DNAs,
		SUM(Total) as Total,
		SUM(DNA)/cast(SUM(Total) as float) as Percentage,
		TheMonth as MonthYear
		
from #OP OP
left outer join WHREPORTING.LK.Calendar cal on OP.AppointmentDate = cal.TheDate
where -- ( OP.Division IN ( @Division ) )
    --   AND ( OP.Directorate IN ( @Directorate ) )
       --AND 
       ( OP.SpecialtyCodeFunction IN (SELECT Item
                                             FROM   dbo.Split(@Specialty, ',') AS Split_1) )
      AND 
    ( OP.ClinicCode IN (SELECT Item1
                            FROM   dbo.Split1(@Clinics, ',') AS Split_1) )
       AND ( OP.ProfessionalCarerCode IN  (SELECT Item
                                           FROM   dbo.Split( @Consultant,  ',') AS Split_1) ) 
Group by ApptType,TheMonth




END