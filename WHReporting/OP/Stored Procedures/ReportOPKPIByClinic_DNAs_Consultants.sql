﻿-- =============================================
-- Author:		CMan
-- Create date: 12/02/2014
-- Description:	Re-created script from the original script saved in SSRS report 'OPKPI by Clinic' 
-- =============================================
CREATE PROCEDURE [OP].[ReportOPKPIByClinic_DNAs_Consultants] 
@StartDate DateTime, 
@EndDate Datetime--, 
--@Division varchar(200), 
--@Directorate varchar(max), 
--@Specialty varchar(max)




AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON

--Declare @startdate  as datetime,
--@enddate as datetime

--set @StartDate = '20130401'
--set @EndDate = '20140201'

If OBJECT_ID('tempdb..#PC') > 0
Drop Table #PC

SELECT DISTINCT OP.Schedule.ProfessionalCarerCode,
                OP.Schedule.ProfessionalCarerName,	
                OP.Schedule.Division,
                OP.Schedule.Directorate,
                OP.Schedule.[SpecialtyCode(Function)] 
                

into #PC
FROM   OP.Schedule
   LEFT OUTER JOIN vwExcludeWAWardsandClinics
                    ON OP.Schedule.ClinicCode = vwExcludeWAWardsandClinics.SPONT_REFNO_CODE
    WHERE  ( vwExcludeWAWardsandClinics.SPONT_REFNO_CODE IS NULL )
          and
            ( OP.Schedule.AppointmentDate>= @StartDate)
       AND ( OP.Schedule.AppointmentDate <= @EndDate)
       AND ( OP.Schedule.ExcludedClinic = 0 )
       --AND ( OP.Schedule.Division IN (SELECT Item
       --                                    FROM   dbo.Split(@Division, ',') AS Split_1) )
       --AND ( OP.Schedule.Directorate IN (SELECT Item
       --                                   FROM   dbo.Split(@Directorate, ',') AS Split_1) )
       --AND ( OP.Schedule.[SpecialtyCode(Function)] IN (SELECT Item
       --                                   FROM   dbo.Split(@Specialty, ',') AS Split_1) )

ORDER  BY OP.Schedule.ProfessionalCarerCode 


Select distinct ProfessionalCarerCode
		,ProfessionalCarerName
from #PC
--where --(Division IN (SELECT Item
              --                             FROM   dbo.Split(@Division, ',') AS Split_1) )
       --AND ( Directorate IN (SELECT Item
          --                                FROM   dbo.Split(@Directorate, ',') AS Split_1) )
       --AND
       -- ( [SpecialtyCode(Function)] IN (SELECT Item
                    --                      FROM   dbo.Split(@Specialty, ',') AS Split_1) )

END