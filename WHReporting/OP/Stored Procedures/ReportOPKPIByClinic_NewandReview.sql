﻿-- =============================================
-- Author:		CMan
-- Create date: 12/02/2014
-- Description:	Re-created script from the original script saved in SSRS report 'OPKPI by Clinic' 
-- =============================================
CREATE PROCEDURE [OP].[ReportOPKPIByClinic_NewandReview] @StartDate DateTime, @EndDate Datetime, @Division varchar(200), @Directorate varchar(max), @Specialty varchar(max), @Clinics varchar(max), @Consultant varchar(max)




AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


If OBJECT_ID('tempdb..#NR') > 0
Drop table #NR
;
Create table #NR (CountOfNewAttend int,
					CountOfFollowUp int, 
					MonthYear varchar(20),
					SpecialtyCodeFunction varchar(10),
					ClinicCode  varchar(20),
					ProfessionalCarerCode    varchar(20) 
					)
;		
Insert into #NR

SELECT Sum(CASE
             WHEN Status = 'Attend'
                  AND AppointmentTypeNHSCode = 1 THEN 1
             ELSE 0
           END)                                    AS CountOfNewAttend,
       Sum(CASE
             WHEN Status = 'Attend'
                  AND ( AppointmentTypeNHSCode = 2
                         OR AppointmentTypeNHSCode IS NULL ) THEN 1
             ELSE 0
           END)                                    AS CountOfFollowUp,
       CONVERT(CHAR(4), OP.AppointmentDate, 100)
       + CONVERT(CHAR(4), OP.AppointmentDate, 120) AS MonthYear
		, OP.[SpecialtyCode(Function)]  
        ,	OP.ClinicCode 
        , OP.ProfessionalCarerCode       
FROM   OP.Schedule AS OP
       LEFT OUTER JOIN vwExcludeWAWardsandClinics AS Exc
                    ON OP.ClinicCode = Exc.SPONT_REFNO_CODE
WHERE  ( OP.AttendStatus IN ( 'Attended', 'Attended on Time', 'Patient Late / Seen' ) )
		AND ( OP.AppointmentDate >= @StartDate )
       AND ( OP.AppointmentDate <= @EndDate )
       AND ( OP.IsWardAttender = 0 )
       AND ( Exc.SPONT_REFNO_CODE IS NULL )
       AND ( OP.AppointmentTypeNHSCode IN ( 1, 2 )
              OR OP.AppointmentTypeNHSCode IS NULL )     
       AND ( OP.Division IN (SELECT Item
                                           FROM   dbo.Split(@Division, ',') AS Split_1) )
	 AND ( OP.Directorate IN (SELECT Item
                                          FROM   dbo.Split(@Directorate, ',') AS Split_1) )
       
       
       
       --AND ( OP.[SpecialtyCode(Function)] IN (SELECT Item
       --                                       FROM   dbo.Split(@Specialty, ',') AS Split_1) )
       --AND ( OP.ClinicCode IN (SELECT Item1
       --                        FROM   dbo.Split1(@Clinics, ',') AS Split_2) )
       --AND ( OP.ProfessionalCarerCode IN ( @Consultant ) )
GROUP  BY CONVERT(CHAR(4), OP.AppointmentDate, 100)
          + CONVERT(CHAR(4), OP.AppointmentDate, 120)
          , OP.[SpecialtyCode(Function)]  
        ,	OP.ClinicCode 
        , OP.ProfessionalCarerCode 


;



Select sum(CountOfNewAttend) CountOfNewAttend,
		sum(CountOfFollowUp) CountOfFollowUp,
		MonthYear
		
from #NR OP
where -- ( OP.Division IN ( @Division ) )
    --   AND ( OP.Directorate IN ( @Directorate ) )
       --AND 
       ( OP.SpecialtyCodeFunction IN (SELECT Item
                                             FROM   dbo.Split(@Specialty, ',') AS Split_1) )
      AND 
    ( OP.ClinicCode IN (SELECT Item1
                            FROM   dbo.Split1(@Clinics, ',') AS Split_1) )
       AND ( OP.ProfessionalCarerCode IN  (SELECT Item
                                           FROM   dbo.Split( @Consultant,  ',') AS Split_1) ) 
Group by MonthYear




END