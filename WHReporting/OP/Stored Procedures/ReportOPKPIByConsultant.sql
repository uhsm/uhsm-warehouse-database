﻿/*

---------------------------------------------------------------------------------------------------------------------------------
  --------------------------------
  
    Purpose :    OPKPI Consultant Level Report - data for SSRS Report on Outpatients KPI at consultant level
   
    Notes  :    Stored in WHREPORTING.OP as [OP].[ReportOPKPIByConsultant]
           
                This procedure has been created for use in the OPKPI Consultant Level Report in the location below
                S:\CO-HI-Information\New File Management - Jan 2011\Data Warehouse Project\SSRS Report Code\Outpatient Reports\SSRS\Outpatients
                This has been designed to populate KPI data for outpatients at Consultant level i.e. Slot Utilisation ,Session Utilisation, DNAs and 6 weeks Clinic cancellations for a selectted date range
            
  Versions :   	1.0.0.1 - 01/04/2016 - Peter Asemota   
                Taken out #temp tables and used common table expressions instead where pre-processing of the data is done.

                
     level :    1.0.0.0  - 18/03/2016  - Peter Asemota           
                     Initial Stored Procedure created
  
  ----------------------------
 --------------------------------------------------------------------------------------------------------------------------------- 
*/
CREATE PROCEDURE [OP].[ReportOPKPIByConsultant] (
	 @StartDate AS DATETIME
	,@EndDate AS DATETIME
	,@Division AS VARCHAR(MAX)
	,@Directorate AS VARCHAR(MAX)
	,@Specialty AS VARCHAR(MAX)
	--,@Clinics AS VARCHAR(MAX)
	,@Consultant AS VARCHAR(MAX)
	)
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET DATEFORMAT DMY
	



/********* This line of code is purely for testing purposes
DECLARE @StartDate AS DATETIME = '01/01/2016'
DECLARE @EndDate AS DATETIME = GETDATE()
DECLARE @Division AS VARCHAR(MAX) = 'Unscheduled Care'
DECLARE @Directorate AS VARCHAR(MAX) = 'Complex Health & Social Care,Medical Specialities,Respiratory Medicine'
DECLARE @Specialty As Varchar(MAX)= '329,300,301,301A,301B,302,303,307,315,316,317,324,330,340,340G,350,350A,361,370,400,410,430,800'
DECLARE @Consultant AS VARCHAR(MAX)  = 'C3483011,C4032234,C2613141,C1500185,C3310652,C5189371,C3617854,C3373451,C1212910,C2503426,C1271980,C5207345,NURONC,C7374848,C4540681,C5205471,ANWH
                                      ,C3340141,C2945428,C5166923,C4781143,C0616159,C6101982,C6129513,C3336229,C2855817,C2555272,C2839006,C4325150,C4466509,ASPIRE,C6106757,C7399661,C4312686,C2853293,MC,NLASP
                                      ,C4126911,C3565641,C3547041,C4422538,C6145529,C4343781,C6075440,NURNEU,C1405486,C4286679,C3645815,C6153704'
************/

	
 ------------------------------------------
	--	Script for Slot Utilisation
  --------------------------------------	
		;

	WITH SlotUtilisation_CTE
	AS (
		SELECT SessionUniqueID
			,SUM(BookingsAllowed) AS BookingsAllowed
			,SUM(BookingsMade) AS BookingsMade
			,SUM(BookingsFree) AS BookingsFree
			,AVG(Utilisation) AS SlotUtilisation
		FROM OP.SessionUtilisation SU
		INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Division, ',') div ON SU.Division = div.SplitValue
		INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Directorate, ',') dir ON SU.Directorate = dir.SplitValue
		INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Specialty, ',') sp ON SU.TreatmentFunctionCode = sp.SplitValue
		INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Consultant, ',') Con ON SU.ProfessionalCarerCode = Con.SplitValue
		WHERE (SessionDate >= @StartDate)
			AND (SessionDate <= @EndDate)
			AND (SessionIsActive = 'Y') --pulling through only only active sessions
			AND (IsCancelled = 'N') -- excluding cancelled slots
			AND (ExcludedClinic = 'N') -- excludeding clinics that are not needed
			AND (NOT (ClinicCode LIKE '%TELE%')) -- excluding Telephone advise and tele medcine clinics
			AND (NOT (ClinicDescription LIKE '%TELE%'))
			AND (
				SessionStatus IN (
					'Session Scheduled'
					,'Session Initiated'
					,'Session Held'
					)
				)
		GROUP BY SessionUniqueID
		)
		
 /**
 ------------------------------------------
		Script for Session utilisation
		--------------------------------------
**/
		,SessionUtilisation_CTE
	AS (
		SELECT SU.SessionUniqueID
			,SUM(CASE 
					WHEN SU.SessionStatus IN (
							'Session Held'
							,'Session Initiated'
							,'Session Scheduled'
							)
						THEN 1
					ELSE 0
					END) AS Utilised
			,SUM(CASE 
					WHEN SU.SessionStatus IN (
							'Session Cancelled'
							,'Session Held'
							,'Session Initiated'
							,'Session on Hold'
							,'Session Scheduled'
							)
						THEN 1
					ELSE 0
					END) AS TotalSessions
			,(
				CAST(SUM(CASE 
							WHEN SU.SessionStatus IN (
									'Session Held'
									,'Session Initiated'
									,'Session Scheduled'
									)
								THEN 1
							ELSE 0
							END) AS DECIMAL) / NULLIF(Cast(SUM(CASE 
								WHEN SU.SessionStatus IN (
										'Session Cancelled'
										,'Session Held'
										,'Session Initiated'
										,'Session on Hold'
										,'Session Scheduled'
										)
									THEN 1
								ELSE 0
								END) AS DECIMAL), 0)
				) * 100 AS SessionUtilisation
			,CASE 
				WHEN DATEDIFF(DAY, SU.SessionCancellationDate, SU.SessionDate) >= 42
					THEN 'More than 6 weeks'
				WHEN DATEDIFF(DAY, SU.SessionCancellationDate, SU.SessionDate) < 42
					THEN 'Less than 6 weeks'
				ELSE 'Not Cancelled'
				END AS [6 Weeks]
			,0.85 AS SessionTarget
		FROM OP.SessionUtilisation SU
		INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Division, ',') div ON SU.Division = div.SplitValue
		INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Directorate, ',') dir ON SU.Directorate = dir.SplitValue
		INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Specialty, ',') sp ON SU.TreatmentFunctionCode = sp.SplitValue
		INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Consultant, ',') Con ON SU.ProfessionalCarerCode = Con.SplitValue
		WHERE (SU.SessionDate >= @StartDate)
			AND (SU.SessionDate <= @EndDate)
			AND SU.Division IN (
				'Clinical Support'
				,'Scheduled Care'
				,'Unscheduled Care'
				)
			AND (SU.SessionIsActive = 'Y') --pulling through only only active sessions
			AND (SU.ExcludedClinic = 'N') -- excludeding clinics that are not needed
			AND (NOT (SU.ClinicCode LIKE '%TELE%')) -- excluding Telephone advise and tele medcine clinics
			AND (NOT (SU.ClinicDescription LIKE '%TELE%'))
		GROUP BY SU.SessionUniqueID
			,CASE 
				WHEN DATEDIFF(DAY, SU.SessionCancellationDate, SU.SessionDate) >= 42
					THEN 'More than 6 weeks'
				WHEN DATEDIFF(DAY, SU.SessionCancellationDate, SU.SessionDate) < 42
					THEN 'Less than 6 weeks'
				ELSE 'Not Cancelled' -- check this today
				END
		)
		
	/**
 ------------------------------------------
		Script for DNA Rate Calculation
		--------------------------------------
		**/
		
		,DNARate
	AS (
		SELECT OP.SessionUniqueID
			,0.065 AS DNATarget
			,SUM(CASE 
					WHEN AttendStatus IN (
							'Attended'
							,'Attended on Time'
							,'Patient Late / Seen'
							)
						AND (
							outcome IS NULL
							OR (
								Outcome NOT LIKE '%canc%'
								AND Outcome NOT LIKE '%Did Not Attend%'
								)
							)
						THEN 1
					ELSE 0
					END) AS Attends
			,SUM(CASE 
					WHEN AttendStatus IN (
							'Refused to Wait'
							,'Patient Left / Not seen'
							,'Patient Late / Not Seen'
							,'Did Not Attend'
							)
						OR (
							AttendStatus IN (
								'Attended'
								,'Attended on Time'
								,'Patient Late / Seen'
								)
							AND Outcome = 'Did Not Attend'
							)
						THEN 1
					ELSE 0
					END) AS DNAs
			,SUM(CASE 
					WHEN AttendStatus IN (
							'Attended'
							,'Attended on Time'
							,'Patient Late / Seen'
							)
						AND (
							outcome IS NULL
							OR (
								Outcome NOT LIKE '%canc%'
								AND Outcome NOT LIKE '%Did Not Attend%'
								)
							)
						THEN 1
					ELSE 0
					END) + SUM(CASE 
					WHEN AttendStatus IN (
							'Refused to Wait'
							,'Patient Left / Not seen'
							,'Patient Late / Not Seen'
							,'Did Not Attend'
							)
						OR (
							AttendStatus IN (
								'Attended'
								,'Attended on Time'
								,'Patient Late / Seen'
								)
							AND Outcome = 'Did Not Attend'
							)
						THEN 1
					ELSE 0
					END) AS Total
		FROM OP.Schedule AS OP
		LEFT OUTER JOIN vwExcludeWAWardsandClinics AS Exc ON OP.ClinicCode = Exc.SPONT_REFNO_CODE
		INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Division, ',') div ON OP.Division = div.SplitValue
		INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Directorate, ',') dir ON OP.Directorate = dir.SplitValue
		INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Specialty, ',') sp ON OP.[SpecialtyCode(Function)] = sp.SplitValue
		INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Consultant, ',') Con ON OP.ProfessionalCarerCode = Con.SplitValue
		WHERE (OP.AppointmentDate >= @StartDate)
			AND (OP.AppointmentDate <= @EndDate)
			AND (OP.IsWardAttender = 0)
			AND (Exc.SPONT_REFNO_CODE IS NULL)
			AND (
				OP.AppointmentTypeNHSCode IN (
					1
					,2
					)
				OR OP.AppointmentTypeNHSCode IS NULL
				)
			AND (
				OP.STATUS IN (
					'DNA'
					,'ATTEND'
					)
				)
			AND (OP.AdministrativeCategory NOT LIKE '%Private%')
		GROUP BY OP.SessionUniqueID
		)
	/*
  -------------------------------------------
  Script pulling together all metrics                  
  -------------------------------------------                     
   */
	SELECT
		-- FACT.[SessionUniqueID]
		FACT.Division
		,FACT.Directorate
		,FACT.TreatmentFunction
		,FACT.ProfessionalCarerCode
		,FACT.ProfessionalCarer
		-- ,FACT.ClinicCode
		-- ,FACT.[SessionDate]
		-- ,CAL.TheMonth
		--,CAL.FirstDateOfMonth 
		,FACT.[ExcludedClinic]
		,SUM(SUT.BookingsAllowed) AS BookingsAllowed
		,SUM(SUT.BookingsMade) AS BookingsMade
		,AVG(SUT.SlotUtilisation) AS SlotUtilisation
		,SUM(SUT.BookingsFree) AS Bookingsfree
		,SUM(SEUT.Utilised) AS Utilised
		,SUM(SEUT.TotalSessions) AS Totalsessions
		,AVG(SEUT.SessionUtilisation) AS SessionUtilisation
		--, SEUT.[6 Weeks]
		,SUM(CASE 
				WHEN SEUT.[6 Weeks] = 'Less than 6 weeks'
					THEN 1
				ELSE 0
				END) AS [Less Than 6 weeks Cancellations]
		,SUM(CASE 
				WHEN SEUT.[6 Weeks] = 'More than 6 weeks'
					THEN 1
				ELSE 0
				END) AS [More Than 6 weeks Cancellations]
		,SUM(CASE 
				WHEN SEUT.[6 Weeks] IN (
						'Less than 6 weeks'
						,'More than 6 weeks'
						)
					THEN 1
				ELSE 0
				END) AS [Total Clinic Cancellations < 6 weeks]
		,AVG(SEUT.SessionTarget) AS SessionTarget
		,AVG(DNA.DNATarget) AS DNATarget
		,SUM(DNA.Attends) AS Attends
		,sum(DNA.DNAs) AS DNAs
		,SUM(DNA.Total) AS [Total_Attends_DNA]
		--, AVG(DNA.Percentage) AS DNARate
		,Cast(sum(DNA.DNAs) AS DECIMAL) / NULLIF(Cast(SUM(DNA.Total) AS DECIMAL), 0) AS DNARate
	FROM [WHREPORTING].[OP].[SessionUtilisation] AS FACT
	LEFT OUTER JOIN SlotUtilisation_CTE AS SUT ON SUT.SessionUniqueID = FACT.SessionUniqueID
	LEFT OUTER JOIN SessionUtilisation_CTE AS SEUT ON SEUT.SessionUniqueID = FACT.SessionUniqueID
	LEFT OUTER JOIN DNARate AS DNA ON DNA.SessionUniqueID = FACT.SessionUniqueID
	--LEFT OUTER JOIN WHREPORTING.LK.Calendar AS CAL ON CAL.TheDate = FACT.SESSIONDATE
	INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Division, ',') div ON FACT.Division = div.SplitValue
	INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Directorate, ',') dir ON FACT.Directorate = dir.SplitValue
	INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Specialty, ',') sp ON FACT.TreatmentFunctionCode = sp.SplitValue
	INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Consultant, ',') Con ON FACT.ProfessionalCarerCode = Con.SplitValue
	WHERE (FACT.SessionDate >= @StartDate)
		AND (FACT.SessionDate <= @EndDate)
		AND FACT.[ExcludedClinic] = 'N'
	GROUP BY FACT.Division
		,FACT.Directorate
		,FACT.TreatmentFunction
		,FACT.ProfessionalCarerCode
		,FACT.ProfessionalCarer
		-- ,FACT.ClinicCode
		--,CAL.TheMonth
		--,CAL.FirstDateOfMonth 
		,FACT.[ExcludedClinic]
		--,SEUT.[6 Weeks]
END