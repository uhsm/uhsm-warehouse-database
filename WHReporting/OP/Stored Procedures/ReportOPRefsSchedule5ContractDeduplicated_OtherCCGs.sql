﻿/*===============================================================================
Author:		J Pitchforth
Date:		23 Oct 2012
Function:	Populates the new SSRS OP Refs Schedule 5 Contract report
			(note that the commented text with tables "T1" and "T2" shows the
			legacy fields the report replaces).  
			
			There are duplicates within the data, caused by different organisations 
			referring the same patient.  There are 2 tests to remove these, see notes 
			below for more details.
			
Version:	1.0
=================================================================================
Author:		J Pitchforth
Date:		04 Dec 2012
Changes:	Changed criteria for referrals to be identified as a dupe (different day) from
			DATEDIFF(dd,min(Referral.ReferralReceivedDate),MAX(PreviousReferral.ReferralReceivedDate)) <-10) 
			to
			DATEDIFF(dd,min(Referral.ReferralReceivedDate),MAX(PreviousReferral.ReferralReceivedDate)) <=-10) 
			(comparison is now less than or equal to -10, rather than just less than -10.
Version:	1.1
=================================================================================
Author:		J Pitchforth
Date:		07 Dec 2012
Changes:	Added join to WHOLAP.dbo.BaseRF to get self-referring babies
Version:	1.2
=================================================================================
=================================================================================
Author:		N Scattergood
Date:		01 Nov 2013
Changes:	Adapted from OP.ReportOPRefsSchedule5ContractDeduplicated to meet Schedule 6 requirements for 'Other'
Version:	1.2
=================================================================================
*/
--OP.ReportOPRefsSchedule5ContractDeduplicated '1 Sep 2012', '30 Jun 2013', '01N'

CREATE proc [OP].[ReportOPRefsSchedule5ContractDeduplicated_OtherCCGs]

	@StartDate date,
	@EndDate date
--Declare @StartDate as date  = '01 sep 2013'
--Declare @EndDate as date	= '30 sep 2013'

As 

---------------------------------------------------------------------------------
/*STEP 1: Identify referrals that are not duplicates (defined as from the same day, with the same
NHS number and the same specialism).  Referrals that do not appear in this list will be 
filtered out later.*/
create table #RemoveDupeReferralsSameDay 
(ReferralSourceUniqueID int)

insert into #RemoveDupeReferralsSameDay (ReferralSourceUniqueID)
	select	MIN(ReferralSourceUniqueID)
	from	RF.Referral
	where	(Referral.RequestedService = 'Outpatient Activity' or ReferralSource = 'General Practitioner Referral')
			and (CancelDate is null or Referral.CancelReasonCode = 'NSP') --not specified
			and Referral.ReferralReceivedDate between @StartDate and @EndDate --limit date range to improve performance
	Group by Referral.ReferralReceivedDate, Referral.NHSNo, Referral.[ReceivingSpecialtyCode(Function)]

--select * from #RemoveDupeReferralsSameDay 
---------------------------------------------------------------------------------
/*STEP 2: Identify referrals that are not duplicates (defined as within 10 days of each other, 
with the same NHS number and the same specialism).  Referrals that do not appear in this list will be 
filtered out later.*/
create table #RemoveDupeReferralsDifferentDay
(ReferralSourceUniqueID int not null,
CurrentReferralDate Date,
NHSNo varchar (20),
ReceivingSpecialtyCode varchar (25),
PreviousReferralDate Date,
TimeBetweenReferrals float,
Dupe Bit)

insert into #RemoveDupeReferralsDifferentDay
(ReferralSourceUniqueID,
CurrentReferralDate,
NHSNo,
ReceivingSpecialtyCode,
PreviousReferralDate,
TimeBetweenReferrals,
Dupe)

select 
Referral.ReferralSourceUniqueID,--Note: only ReferralSourceUniqueID is needed for the query, but the other fields will be useful for debugging
min(Referral.ReferralReceivedDate),
min(Referral.NHSNo),
Referral.[ReceivingSpecialtyCode(Function)],
MAX(PreviousReferral.ReferralReceivedDate) as Previous,
DATEDIFF(dd,min(Referral.ReferralReceivedDate),MAX(PreviousReferral.ReferralReceivedDate)),
case when (DATEDIFF(dd,min(Referral.ReferralReceivedDate),MAX(PreviousReferral.ReferralReceivedDate)) is null
or DATEDIFF(dd,min(Referral.ReferralReceivedDate),MAX(PreviousReferral.ReferralReceivedDate)) <=-10) 
then 0 else 1 end as Dupe



 from RF.Referral 

left join RF.Referral PreviousReferral
		on Referral.NHSNo = PreviousReferral.NHSNo
		and PreviousReferral.ReferralReceivedDate <Referral.ReferralReceivedDate
		and Referral.ReferralSourceUniqueID <> PreviousReferral.ReferralSourceUniqueID
		and Referral.[ReceivingSpecialtyCode(Function)] = PreviousReferral.[ReceivingSpecialtyCode(Function)]
		and (PreviousReferral.RequestedService = 'Outpatient Activity' or PreviousReferral.ReferralSource='General Practitioner Referral')
		and (PreviousReferral.CancelDate is null or PreviousReferral.CancelReasonCode = 'NSP') --not specified

		
where	(Referral.RequestedService = 'Outpatient Activity' or Referral.ReferralSource='General Practitioner Referral')
		and (Referral.CancelReasonCode = 'NSP' or Referral.CancelDate is null)  --not specified
	

group by 
		Referral.ReferralSourceUniqueID,
		Referral.NHSNo,
		Referral.[ReceivingSpecialtyCode(Function)],
		PreviousReferral.NHSNo

--having (DATEDIFF(dd,min(Referral.ReferralReceivedDate),MAX(PreviousReferral.ReferralReceivedDate)) is null
--		or DATEDIFF(dd,min(Referral.ReferralReceivedDate),MAX(PreviousReferral.ReferralReceivedDate)) >-10)

Order by MIN(referral.ReferralReceivedDate)
---------------------------------------------------------------------------------
--STEP 3: Final select statement (note inner joins to the 2 temp tables created earlier)


select 

	Referral.ReferralReceivedDate, 
	Null as LocalPatientID, --T1."Local Patient ID" has been removed, they all seemed to be blank
	'RM2' as 'ProviderOrganisationCode', 
	Referral.NHSNo,--T1."NHS Number" 
	Referral.PostCode,--T1."AD_POSTCODE"
	Patient.SexNHSCode,--T1."Gender", 
	Referral.[ReceivingSpecialtyCode(Main)] as MainSpecialtyCode,--T1."MainSpecialtyCode", 
	Referral.ReceivingSpecialtyCode as SpecialtyFunctionCode,--T1."SpecialtyFunctionCode", 
	Referral.EpisodeGPCode, --T1."Registered GP", 
	Referral.EpisodePracticeCode,--T1."General Medical Practice Code (Patient Registration)", 
	Referral.PriorityNHSCode,--T1."Priority Type", 
	Referral.ReferralSourceNHSCode, --T1."Source of Referral For Outpatients", 
	Referral.ReferrerCode, --T1."Referrer Code (GMC Or Consultant)", 
	Referral.ReferrerOrganisationCode, --T1."Referring Organisation Code (GP Practice or Provider Code)", 
	--Referral.EpisodeCCGCode  
	'OTH' as EpisodePCTCode,
	--EpisodePCTCode, --T1."PCTCode", 
	--Referral.EpisodeCCG 
	'Other' as EpisodePCT, --EpisodePCT, --T1."PCTName",
	Referral.ReferralSourceUniqueID,-- T1."REF_REF", FOR REFERENCE ONLY
	Referral.[ReceivingSpecialtyCode(Function)],--T2."Ref_to_spec", 
	Referral.ReceivingProfCarerCode, --T2."Ref_to_code", 
	Referral.ReceivingProfCarerTypeCode, --T2."Doctor_type", 
	Case when Referral.ReferralSourceCode = 'EBS'
		then 'Y'
		else 'N' end as ReferralSource, 
	--case  when T2."Source_of_referral" = 
	--'General Practitioner Referral' then 'Y' else 'N' end , 
	Patient.DateOfBirth
	--CAST(T1."Date of Birth" AS FLOAT)

from RF.Referral 
	inner join RF.Patient on Referral.EncounterRecno = Patient.EncounterRecno
	--LEFT JOIN "Information_Reporting"."dbo"."referral_dataset" T2 
	--on T1."REF_REF" = T2."Ref_ref"
	inner join #RemoveDupeReferralsSameDay as RemoveDupeReferralsSameDay
		on Referral.ReferralSourceUniqueID = RemoveDupeReferralsSameDay.ReferralSourceUniqueID
	inner join #RemoveDupeReferralsDifferentDay as RemoveDupeReferralsDifferentDay
		on Referral.ReferralSourceUniqueID = RemoveDupeReferralsDifferentDay.ReferralSourceUniqueID
		and RemoveDupeReferralsDifferentDay.Dupe = 0
	left join WHOLAP.dbo.BaseRF on Referral.ReferralSourceUniqueID = BaseRF.SourceUniqueID --added for v 1.2

Where 
Referral.ReferralReceivedDate between @StartDate and @EndDate	
	and 
	Referral.EpisodeCCGCode not in ('01M','01N','00W','02A')
	and 
	(Referral.RequestedService = 'Outpatient Activity' or ReferralSource='General Practitioner Referral')
	and 
	(Referral.CancelReasonCode = 'NSP' or CancelDate is null)  --not specified
	and 
	BaseRF.SelfReferringBaby = 0  --added for v 1.2

--where T1."Referral Request Received Date" between '20120901' and '20120930' and T1."PCTCode" in ('5F5')
and CancelDate is null
order by Referral.ReferralReceivedDate


---------------------------------------------------------------------------------
--STEP 4: Tidy up

drop table #RemoveDupeReferralsDifferentDay
drop table #RemoveDupeReferralsSameDay