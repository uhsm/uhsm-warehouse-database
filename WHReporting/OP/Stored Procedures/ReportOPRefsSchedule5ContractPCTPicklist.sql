﻿/*===============================================================================
Author:		J Pitchforth
Date:		24 Oct 2012
Function:	Populates a dropdown list of PCTs that can be included in the OP refs
			schedule 5 contract report (SSRS)
			I'm using the stored proc as a data source to maintain flexibility for the
			forthcoming PCT reorganisation
Version:	1.0
===============================================================================
Author:		J Pitchforth
Date:		24 Oct 2012
Changes:	Changed SP to list all CCGs
Version:	1.1
=================================================================================*/
--[OP].[ReportOPRefsSchedule5ContractPCTPicklist]

CREATE proc [OP].[ReportOPRefsSchedule5ContractPCTPicklist]


As 

--select 
--		PurchaserMainCode, 
--		Purchaser

--From	WH.PAS.Purchaser
--where	ArchiveFlag = 'N'

--	and PurchaserMainCode in --specified PCTs to include in report
--	(	'5NT',
--		'5NR',
--		'5F5',
--		'5F7'
--	)

--order by Purchaser

select 
Distinct CCG as PurchaserMainCode, 
[CCG Name] as Purchaser 
from Organisation..[CCG Practice PCT Lookup]