﻿/*
---------------------------------------------------------------------------------------------------------------------------------
  
    Purpose :    Outpatient Activity by Consultant - data for SSRS Report on Outpatients activity by consultant
   
    Notes  :    Stored in WHREPORTING.OP as [OP].[ReportOutpatientActivityByConsultant]
           
                This procedure has been created for use in the  Outpatient Activity by Consultant report in the location below
                S:\CO-HI-Information\New File Management - Jan 2011\Data Warehouse Project\SSRS Report Code\Outpatient Reports\SSRS\Outpatients\Outpatients
                This has been designed to populate DNA rate for outpatients by Consultant on by weekly and monthly basis 
                
  Versions :   
  
				1.0.0.1 - 14/04/2016 - Peter Asemota   
                Create a CTE to pre-process the data. Change DNA target to 0.065 as supposed to 0.075 contained in original script 

                
     level :    1.0.0.0  - 26/03/2012 - UHSM\isarwar          
                     Initial script embedded in report
  
 --------------------------------------------------------------------------------------------------------------------------------- 
*/

CREATE PROCEDURE [OP].[ReportOutpatientActivityByConsultant] (
	 @StartDate AS DATETIME
	,@EndDate AS DATETIME
	,@Division AS VARCHAR(MAX)
	,@Directorate AS VARCHAR(MAX)
	,@Specialty AS VARCHAR(MAX)
	,@Clinics AS VARCHAR(MAX)
	,@Consultant AS VARCHAR(MAX)
	,@AppType AS VARCHAR(50)
	)
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET DATEFORMAT DMY
	SET DATEFIRST 1
	

SELECT  OP.ProfessionalCarerCode  
		,OP.ProfessionalCarerName
         ,OP.[SpecialtyCode(Function)]
         ,OP.Division 
         ,OP.Directorate
         ,OP.ClinicCode
         ,OP.AppointmentDate
         ,OP.FacilityID As [RM2Number]
	,CASE 
		WHEN AppointmentTypeNHSCode = 1
			THEN 'New'
		WHEN AppointmentTypeNHSCode = 2
			THEN 'Follow Up'
		WHEN AppointmentTypeNHSCode IS NULL
			THEN 'Follow Up'
		END AS ApptType
	,0.065 As DNATarget --Changed by PA
	,SUM(CASE 
			WHEN AttendStatus IN (
					'Attended'
					,'Attended on Time'
					,'Patient Late / Seen'
					)
				AND (
					outcome IS NULL
					OR (
					Outcome NOT LIKE '%canc%'
				AND Outcome NOT LIKE '%Did Not Attend%'
						)
					)
				THEN 1
			ELSE 0
			END) AS Attends
	,SUM(CASE 
			WHEN AttendStatus IN (
					'Refused to Wait'
					,'Patient Left / Not seen'
					,'Patient Late / Not Seen'
					,'Did Not Attend'
					)
				OR (
					AttendStatus IN (
						'Attended'
						,'Attended on Time'
						,'Patient Late / Seen'
						)
					AND Outcome = 'Did Not Attend'
					)
				THEN 1
			ELSE 0
			END) AS DNAs
	,SUM(CASE 
			WHEN AttendStatus IN (
					'Attended'
					,'Attended on Time'
					,'Patient Late / Seen'
					)
				AND (
					outcome IS NULL
					OR (
						Outcome NOT LIKE '%canc%'
						AND Outcome NOT LIKE '%Did Not Attend%'
						)
					)
				THEN 1
			ELSE 0
			END) + SUM(CASE 
			WHEN AttendStatus IN (
					'Refused to Wait'
					,'Patient Left / Not seen'
					,'Patient Late / Not Seen'
					,'Did Not Attend'
					)
				OR (
					AttendStatus IN (
						'Attended'
						,'Attended on Time'
						,'Patient Late / Seen'
						)
					AND Outcome = 'Did Not Attend'
					)
				THEN 1
			ELSE 0
			END) AS Total
--18/08/2016 Commented out the section below as per Peter Asemota as a divide by zero is causing an error.
	--,CAST(SUM(CASE 
	--			WHEN AttendStatus IN (
	--					'Refused to Wait'
	--					,'Patient Left / Not seen'
	--					,'Patient Late / Not Seen'
	--					,'Did Not Attend'
	--					)
	--				OR (
	--					AttendStatus IN (
	--						'Attended'
	--						,'Attended on Time'
	--						,'Patient Late / Seen'
	--						)
	--					AND Outcome = 'Did Not Attend'
	--					)
	--				THEN 1
	--			ELSE 0
	--			END) AS DECIMAL) / CAST(SUM(CASE 
	--			WHEN AttendStatus IN (
	--					'Attended'
	--					,'Attended on Time'
	--					,'Patient Late / Seen'
	--					)
	--				AND (
	--					outcome IS NULL
	--					OR (
	--						Outcome NOT LIKE '%canc%'
	--						AND Outcome NOT LIKE '%Did Not Attend%'
	--						)
	--					)
	--				THEN 1
	--			ELSE 0
	--			END) + SUM(CASE 
	--			WHEN AttendStatus IN (
	--					'Refused to Wait'
	--					,'Patient Left / Not seen'
	--					,'Patient Late / Not Seen'
	--					,'Did Not Attend'
	--					)
	--				OR (
	--					AttendStatus IN (
	--						'Attended'
	--						,'Attended on Time'
	--						,'Patient Late / Seen'
	--						)
	--					AND Outcome = 'Did Not Attend'
	--					)
	--				THEN 1
	--			ELSE 0
	--			END) AS DECIMAL) AS Percentage
	,CONVERT(CHAR(4), OP.AppointmentDate, 100) + CONVERT(CHAR(4), OP.AppointmentDate, 120) AS MonthYear
	,'Week' + ' ' +  Convert(Varchar (10),DATEPART(ww, OP.AppointmentDate),103) As weekly
	,DATEADD(DAY, 7 - DATEPART(WEEKDAY, OP.AppointmentDate), CAST(OP.AppointmentDate AS DATE)) [WeekEnding]

	
FROM OP.Schedule AS OP
LEFT OUTER JOIN vwExcludeWAWardsandClinics AS Exc ON OP.ClinicCode = Exc.SPONT_REFNO_CODE
INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Division, ',') div ON OP.Division = div.SplitValue
INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Directorate, ',') dir ON OP.Directorate = dir.SplitValue
INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Specialty, ',') sp ON OP.[SpecialtyCode(Function)] = sp.SplitValue
INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Consultant, ',') Con ON OP.ProfessionalCarerCode = Con.SplitValue
INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Clinics, ',') Cli ON OP.ClinicCode = Cli.SplitValue
WHERE (OP.AppointmentDate >= @StartDate)
	AND (OP.AppointmentDate <= @EndDate)
	AND (OP.IsWardAttender = 0)
	AND (Exc.SPONT_REFNO_CODE IS NULL)
	AND (
		OP.AppointmentTypeNHSCode IN (
			 1 -- retieving new appointments and follow ups
			,2
			)
		OR OP.AppointmentTypeNHSCode IS NULL
		)
	AND (
		OP.STATUS IN (
			'DNA'
			,'ATTEND'
			)
		)
	AND (OP.AdministrativeCategory NOT LIKE '%Private%')
	AND CASE 
            WHEN AppointmentTypeNHSCode = 1
                  THEN 'New'
            WHEN AppointmentTypeNHSCode = 2
                  THEN 'Follow Up'
            WHEN AppointmentTypeNHSCode IS NULL
                  THEN 'Follow Up'
            END 
                 IN (
                  SELECT Item
                  FROM dbo.Split(@AppType, ',')
                  )

GROUP BY OP.[SpecialtyCode(Function)]
	,CASE 
		WHEN AppointmentTypeNHSCode = 1
			THEN 'New'
		WHEN AppointmentTypeNHSCode = 2
			THEN 'Follow Up'
		WHEN AppointmentTypeNHSCode IS NULL
			THEN 'Follow Up'
		END
	,CONVERT(CHAR(4), OP.AppointmentDate, 100) + CONVERT(CHAR(4), OP.AppointmentDate, 120)
	,OP.ProfessionalCarerCode 
	,OP.ProfessionalCarerName
	,OP.Division
    ,OP.Directorate
    ,OP.ClinicCode
    ,OP.AppointmentDate
    ,OP.FacilityID 
	
--ORDER BY OP.[SpecialtyCode(Function)]

END