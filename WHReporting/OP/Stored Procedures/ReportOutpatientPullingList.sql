﻿-- =============================================
-- Author:		CMan
-- Create date: 14/03/2014
-- Description:	Stored Procedure for SSRS Report Outpatient_Pulling_List.
--				Replaces the script which is embedded in the curent report which looks to InfoSQL to source the data
--				RD450
-- Dependencies: WHREPORTING.OP.Schedule
--				 WH.PAS.ServicePointBase
--				 WH.CRM.ChronosLocations  -- this table is apparently manually updated
--				 WHReporting.CN.CasenoteCurrentLocationDataset 
				 	
-- =============================================
CREATE PROCEDURE [OP].[ReportOutpatientPullingList] @start datetime, @end datetime, @Location varchar(100)

	-- Add the parameters for the stored procedure here
AS
BEGIN

/*For Testing 
Declare @start as datetime, @end as datetime

Set @start = '20140122'
set @end = '20140122'
*/


Select distinct  op.ClinicCode, 
		op.SourcePatientNo,
		op.SourceUniqueID,
		op.AppointmentDateTime,
		op.FacilityID,
		op.AppointmentType,
		op.Clinic,
		op.CancelledDateTime, 
		op.[SpecialtyCode(Function)],
		sp.DESCRIPTION,
		sp.HEORG_REFNO_MAIN_IDENT,
		cl.LocationDescription,
		cl.ChronosArea, 
		op.ProfessionalCarerName as Consultant,
		cds.CasenoteIdentifier,
		cds.LocationCode,
		cds.Location,
		cds.Notes,
		cds.VolumeIdentifier,
		cds.Volume_count,
		pat.PatientForename,
		pat.PatientSurname,
		Right(op.FacilityID, 2) as TerminalDigit
		
		

from WHREPORTING.OP.Schedule op
Left outer join (Select * from WH.PAS.ServicePointBase where ARCHV_FLAG = 'N' )sp on op.ClinicCode = sp.SPONT_REFNO_CODE
Left outer join WH.CRM.ChronosLocations cl on sp.HEORG_REFNO_MAIN_IDENT COLLATE Latin1_General_CI_AS = cl.LocationCode -- link to Chronos Location to identify the location from the manually updated table - logic copied from original report 
Left outer join WHReporting.CN.CasenoteCurrentLocationDataset cds on op.SourcePatientNo = cds.patnt_refno
Left outer join WHREPORTING.OP.Patient pat on op.SourcePatientNo = pat.SourcePatientNo

--the where criteria below is copied from the original code in the embedded report
where op.AppointmentDate >= @start
and op.AppointmentDate <= @end
and op.IsWardAttender <> '1'
and op.AppointmentType in ('New','Follow Up')
and op.CancelledDateTime is NULL
and sp.DESCRIPTION not like '%Do Not Pull%'
and (op.[SpecialtyCode(Function)] not in ('324', '824', '822', '650', '652')
or op.ClinicCode IN ('MSKWYPAED1','MSKWYPAED2', 'PAEDPHY')) --added PAEDPHY on 07/10/2015 as requested by karen Phethean
and cds.LocationCode in ('0OFS','PEND','5201','LARCH','LARC','TLIB','SLIB') -- these are the location which were available to be selected from in the old report 
--0OFS = OFF SITE 5201, PEND = PENDING IH 5200, 5201 = ARCHIVE IH 5201, LARCH = LIBRARY ARCHIVE 5201,LARC	= LIBRARY ARCHIVE 5201, TLIB = HEALTH RECORDS LIBRARY IH 5200, SLIB = SECONDARY LIBRARY 5201
and case when cds.LocationCode = 'LARCH' then 'LARC' Else cds.LocationCode End in (SELECT Item
                         FROM   dbo.Split (@Location, ',')) --reassign code LARCH to LARC  to avoid having two dropdown options with the same description in the report



Order by OP.ClinicCode

END