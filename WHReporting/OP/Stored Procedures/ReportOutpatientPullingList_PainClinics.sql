﻿-- =============================================
-- Author:		 Peter Asemota
-- Create date:  27/04/2014
-- Purpose:      [OP].[ReportOutpatientPullingList_PainClinics]
-- Description:	 Stored Procedure for SSRS Report Outpatient_Pulling_List for Pain Clinics.

-- Notes :       Stored in WHREPORTING.PTL
--               This Procedure has been created for use in the Outpatient_Pulling_List SSRS report for Pain Clinics and Area B Clinics can be found in the location below
--              S:\CO-HI-Information\New File Management - Jan 2011\Data Warehouse Project\SSRS Report Code\Outpatient Reports\SSRS\Outpatients
           
--              This has been designed to populate the data for the Outpatient_Pulling_List for Pain Clinics and Area B clinics.
--				
-- Dependencies: WHREPORTING.OP.Schedule
--				 WH.PAS.ServicePointBase
--				 WH.CRM.ChronosLocations  -- this table is apparently manually updated
--				 WHReporting.CN.CasenoteCurrentLocationDataset 
				 	
-- =============================================

CREATE PROCEDURE [OP].[ReportOutpatientPullingList_PainClinics] 
(
      @Start datetime
    , @End datetime
    , @Location varchar(Max)
    , @Consultant as Varchar (Max)
    , @ClinicType as Varchar (Max)
    
    )

As 

BEGIN

/* Test purposes only
Declare @start as datetime, @end as datetime ,@Location as Varchar(Max) ,@Consultant as Varchar (Max),@ClinicType as Varchar (Max)

Set @start = '20160401'
set @end = '20160411'
set @Location = '6374,6375,6382,6397'--'0OFS,PEND,5201,LARCH,LARC,TLIB,SLIB'
Set @Consultant = 'C4338404,C5205471'
Set @ClinicType = 'Area B Clinic'
*/

Select *  from (
Select distinct  op.ClinicCode, 
		op.SourcePatientNo,
		op.SourceUniqueID,
		op.AppointmentDateTime,
		op.FacilityID,
		op.AppointmentType,
		op.Clinic,
		op.CancelledDateTime, 
		op.[SpecialtyCode(Function)],
		sp.DESCRIPTION,
		sp.HEORG_REFNO_MAIN_IDENT,
		cl.LocationDescription,
		cl.ChronosArea, 
		op.ProfessionalCarerCode,
		op.ProfessionalCarerName as Consultant,
		cds.CasenoteIdentifier,
		cds.LocationCode,
		cds.Location,
		cds.Notes,
		cds.VolumeIdentifier,
		cds.Volume_count,
		pat.PatientForename,
		pat.PatientSurname,
		Right(op.FacilityID, 2) as TerminalDigit,
		Case When op.ClinicCode IN ('SRSHHP2','SRSHHFU','CNMU1R','KJT1P2','KJT1AR','WB01P2','WB01AM','PC01P2'
                             ,'PC01AR','NURDM1AM','NUL1REV','JHNEW1AM','JHREV1AM','BGIN1AM','BGIR1AM','GMEDP2'
                             ,'GMEREV','DIPP2','DIPREV','GUN1P2','GUN1AM','OPHYST','DJJO3A','TRC3AR','AQU4P2' 
                             ,'OPHYST2','AEC2AR','AEC2P2','GPN2AM','GPF2AM','OAN2AM','OAR2AM','PJWTEL'
                             ,'WB02P2','WBO2AM','PC02P2','PC02AR','HBC2P2','HBC2REV','TELDM2AM','JGY2AM'
                             ,'JGY2P2','KYC2P2','KYC2AM','NPCHAEM','JGSHP2','JGSH2R','CNMC2R','KHY2AM' 
                             ,'DJJ3P2','SAD3P2','SAD3AR','IMWEP2','IMW3PR','IMW3P2','HBC3P2','HBC3AR'
                             ,'EDUS3AM','NLDM-AH','JHNEW3AM','JHREV3AM','JHNEWTH3AM','JHREVTH3AM','KHY2P2'
                             ,'AGA3P2','AGA3REV','NYON3AM','NYOR3AM','SMN3AM','SMR3AM','RR3YPC','JCL2PT'
                             ,'RR3YP2','NYO3NK','JEFREV','JEFWYTH','JFIMMR','JFIMMN','SACH3P2','SACH3REV'
                             ,'AAHLHYST','OPHYST','KYC3AM','KYC3NEW','JCL3P2','JCL3AM','JCL4AM','JCL4P2'
                             ,'MESHPMB','AHLTHU','AHL4AR','4RKNEW','4RKREV','PJWNGC','NYFAST','NYO4P2'
                             ,'NYOA4R','SWA1P2','SWA1PR','SNN4AM','SNR4AM','SEH4P2','SEH4PR','NPCFR'
                             ,'RK5NEW','RK5REV','BGI5DIAB','BGI5P2','BGILREV','BGILP2','NYONP2','NYO5FT'
                             ,'JSWFRI','JSW5AN','PICKNEW','PICKREV','THG5P2','THG5AR','NPCFR' ,'NURONC'
                             ,'PTN5AR','PTN5P2','NLDM-AH','CYSTO','NLRHE5','JGHREV','JGHNEW','SBO5AM','SBO5P2'
-- PM
                             ,'HKN1PM','HKR1PM','JIQMON','JIQ1R','HPB-NL','SWA1P2','SWA1PR','SEH1P2','SEH1PR'
                             ,'SNN1PM','SNR1PM','SWG1P2','SWG1PR','NURDM1PM','NLDM-AM','NY1REN','BGIEDP2','BGIEDREV'
                             ,'SMN1PM','SMR1PM','PGHSC','PAS','JRCTUE','JRC2PR','RBJN2PM','RBJR2PM','SAD2P2','SAD2PR'
                             ,'KJT2P2','KJT2PR','DJJO3A','DJJ3P2','MPS2P2','MPS2PR','SAM2P2','SAM2PR','NURDM2PM'
                             ,'SBPMB','HBC2P2','HBC2REV','GUN2P2','GUN2PM','JGYP2','JGYSGR','JGY4CF','JGYTHU'
                             ,'JGYMET','NURSE','KC4NEW','KC4REV','FIRSWAY','CYSTO','MWE2P2','MWE2PM','UROGYNAE'
                             ,'JFMOP','OAN3PM','OAR3PM','SKVOICE','SAKFU','IMWEP2','UGI3P2','DELAMERE','PREOPUROL'
                             ,'STE3P2','STEREV','OAN4PM','OAR4PM','TSOTUE','TSO2PR','RARNEW','RAREV','TMHSC'
                             ,'TMHSCR','TMPMB','CHN4PM','CHR4PM','SMN4PM','SMR4PM','GUN4PM','KYCHMET','RHUNU4'
                             ,'DMARD4R','HOD4PM','GSO5PR','GSOFRI','TMNEW','TMREV','JRCFRI','JRCREV','OAN5PM'
                             ,'OAR5PM','GWFNEW','GWFREV','PREOPUROL'
                              ) THEN 'Area B Clinic'
      When op.ClinicCode in ( 'BVYANEW','PCNSPC','ACUNURMON','PELPHYS','CBREHAB','CBPMPASS','KGRPAI','CBARDEP1'
                             ,'KGRATEL','KGRWP2','WDMPAI','WDMPP2','AKAM2P2','AKAM2FU','PCNSPC','SGOLBS','CBARDEP1'
                             ,'CBREHAB','PAINPREOPTHU','WDMWED','PCNSPC','PELPHYS','ZMREHAB','ZMPMPASS','HDREBAB','ILIPAI'
                             ,'ILITP2','ILIWP2','ANGIPC','ANGIP2','WDMPELVIC/FU','BVYANEW/REV','PCNSPC','PAINTELREV'
                             ,'PAINPREOPTHU','BVYAREV','PELPHYS','PAINPREOPFRI'
--PM
                             ,'PAINPREOPMON','SGREHAB','SGPMPASS','SGOLBS','SGREHAB','ZMINDPSY','HDPMPASS','PAINPREOPWED'
                             ,'CBARBS','SGOLBS','ZMINDPSY','HDINDPSY'
                             ) Then 'Pain Clinic' Else 'other'
                         End As 	ClinicType	
		

from WHREPORTING.OP.Schedule op
Left outer join (Select * from WH.PAS.ServicePointBase where ARCHV_FLAG = 'N' )sp on op.ClinicCode = sp.SPONT_REFNO_CODE
Left outer join WH.CRM.ChronosLocations cl on sp.HEORG_REFNO_MAIN_IDENT COLLATE Latin1_General_CI_AS = cl.LocationCode -- link to Chronos Location to identify the location from the manually updated table - logic copied from original report 
Left outer join WHReporting.CN.CasenoteCurrentLocationDataset cds on op.SourcePatientNo = cds.patnt_refno
Left outer join WHREPORTING.OP.Patient pat on op.SourcePatientNo = pat.SourcePatientNo
INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Consultant, ',') Con ON op.ProfessionalCarerCode = Con.SplitValue

--the where criteria below is copied from the original code in the embedded report
where op.AppointmentDate >= @start
and op.AppointmentDate <= @end
and op.IsWardAttender <> '1'
and op.AppointmentType in ('New','Follow Up')
and op.CancelledDateTime is NULL
and sp.DESCRIPTION not like '%Do Not Pull%'
and (op.[SpecialtyCode(Function)] NOT IN ('324', '824', '822', '650', '652')
or op.ClinicCode IN ('MSKWYPAED1','MSKWYPAED2', 'PAEDPHY')) --added PAEDPHY on 07/10/2015 as requested by karen Phethean
--and cds.LocationCode in ('0OFS','PEND','5201','LARCH','LARC','TLIB','SLIB') -- these are the location which were available to be selected from in the old report  PA -- Now pulling through all location codes
--0OFS = OFF SITE 5201, PEND = PENDING IH 5200, 5201 = ARCHIVE IH 5201, LARCH = LIBRARY ARCHIVE 5201,LARC	= LIBRARY ARCHIVE 5201, TLIB = HEALTH RECORDS LIBRARY IH 5200, SLIB = SECONDARY LIBRARY 5201
and case when cds.LocationCode = 'LARCH' then 'LARC' Else cds.LocationCode End in (SELECT Item
                         FROM   dbo.Split (@Location, ',')) --reassign code LARCH to LARC  to avoid having two dropdown options with the same description in the report

) as Data 
INNER JOIN DW_REPORTING.LIB.fn_SplitString(@ClinicType, ',') Cli ON Data.ClinicType = Cli.SplitValue
where Data.LocationCode is not NULL



 
 END