﻿/*

---------------------------------------------------------------------------------------------------------------------------------
  --------------------------------
  
    Purpose :      52 Weeks Session Utilised Report - data for SSRS Report on Outpatients seesions Utilised at consultant level
   
    Notes  :    Stored in WHREPORTING.OP as [OP].[ReportSessionsUtilised52Weeks]
           
                This procedure has been created for use in the 52 weeks session utilised report  at Consultant Level in the location below
                S:\CO-HI-Information\New File Management - Jan 2011\Data Warehouse Project\SSRS Report Code\Outpatient Reports\SSRS\Outpatients
                This has been designed to counts of sessions undertaken for outpatients at Consultant level on a weekly basis
            
  Versions :    1.0.0.0 - 07/04/2016 - Peter Asemota   

  ----------------------------
 --------------------------------------------------------------------------------------------------------------------------------- 
*/
CREATE PROCEDURE [OP].[ReportSessionsUtilised52Weeks] (
	@StartDate AS DATETIME
	,@EndDate AS DATETIME
	,@Division AS VARCHAR(MAX)
	,@Directorate AS VARCHAR(MAX)
	,@Consultant AS VARCHAR(MAX)
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET DATEFORMAT DMY
	SET DATEFIRST 1
		/*88 This line of code is purely for testing purposes
Declare @StartDate as datetime  = DATEADD(week,-52,getdate ()) -- retrieving last 52 weeks worth of data
Declare @EndDate as datetime = getdate ()
Declare @Division As Varchar (Max)='Unscheduled Care,Clinical Support,Unscheduled Care'
Declare @Directorate As Varchar (Max)='Accident & Emergency,Anaesthetics Critical Care & Pain,Cardiothoracic'
**/
		;

	WITH DateOfWeek_cte
	AS (
		SELECT 'Monday' AS DOW
		
		UNION ALL
		
		SELECT 'Tuesday' AS DOW
		
		UNION ALL
		
		SELECT 'Wednesday' AS DOW
		
		UNION ALL
		
		SELECT 'Thursday' AS DOW
		
		UNION ALL
		
		SELECT 'Friday' AS DOW
		
		UNION ALL
		
		SELECT 'Saturday' AS DOW
		
		UNION ALL
		
		SELECT 'Sunday' AS DOW
		)
		
		
	SELECT SU.SessionUniqueID
		,SU.SessionDate
		,DATENAME(DW, SU.SessionDate) AS [Weekday]
		,DATEPART(DW, SU.SessionDate) AS [Weekday_int]
		,DATEADD(DAY, 1 - DATEPART(WEEKDAY, SU.SessionDate), CAST(SU.SessionDate AS DATE)) [WeekStart]
		,DATEADD(DAY, 7 - DATEPART(WEEKDAY, SU.SessionDate), CAST(SU.SessionDate AS DATE)) [WeekEnd]
		,DOW.DOW
		--,DOW.DayOfWeekKey
		--,DOW.FirstDayOfWeek
		--,DOW.LastDayOfWeek
		,SU.ProfessionalCarer
		,SU.ProfessionalCarerCode
		,SU.Division
		,SU.Directorate
		,SU.Specialty
		,SU.SpecialtyCode
		,SU.TreatmentFunction
		,SU.TreatmentFunctionCode
		,SUM(CASE 
				WHEN SU.SessionStatus IN (
						'Session Held'
						,'Session Initiated'
						,'Session Scheduled'
						)
					THEN 1
				ELSE 0
				END) AS Utilised
		,SUM(CASE 
				WHEN SU.SessionStatus IN (
						'Session Cancelled'
						,'Session Held'
						,'Session Initiated'
						,'Session on Hold'
						,'Session Scheduled'
						)
					THEN 1
				ELSE 0
				END) AS TotalSessions
		,1 AS Row
	FROM DateOfWeek_cte AS DOW
	LEFT OUTER JOIN OP.SessionUtilisation SU ON DOW.DOW = DATENAME(DW, SU.SessionDate)
	INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Division, ',') div ON SU.Division = div.SplitValue
	INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Directorate, ',') dir ON SU.Directorate = dir.SplitValue
	INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Consultant, ',') Con ON SU.ProfessionalCarerCode = Con.SplitValue
	WHERE (SU.SessionDate >= @StartDate)
		AND (SU.SessionDate <= @EndDate)
		AND SU.Division IN (
			'Clinical Support'
			,'Scheduled Care'
			,'Unscheduled Care'
			)
		AND (SU.SessionIsActive = 'Y') --pulling through only only active sessions
		AND (SU.ExcludedClinic = 'N') -- excludeding clinics that are not needed
		AND (NOT (SU.ClinicCode LIKE '%TELE%')) -- excluding Telephone advise and tele medcine clinics
		AND (NOT (SU.ClinicDescription LIKE '%TELE%'))
	GROUP BY SU.SessionUniqueID
		,SU.SessionDate
		,SU.ProfessionalCarer
		,SU.ProfessionalCarerCode
		,SU.Division
		,SU.Directorate
		,SU.Specialty
		,SU.SpecialtyCode
		,SU.TreatmentFunction
		,SU.TreatmentFunctionCode
		,DOW.DOW
		--,DOW.DayOfWeekKey
		--,DOW.FirstDayOfWeek
		--,DOW.LastDayOfWeek
	ORDER BY SU.SessionDate
END