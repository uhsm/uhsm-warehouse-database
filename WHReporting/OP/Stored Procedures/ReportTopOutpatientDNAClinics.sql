﻿/*
--Author: N Scattergood
--Date created: 23/01/2013
--Stored Procedure Built for SRSS Report on:
--Top Outpatient DNA Clinics RD076
*/


CREATE Procedure [OP].[ReportTopOutpatientDNAClinics]

@StartDate as Date
,@EndDate as Date
,@DIVPar as nvarchar(max)
,@SpecPar as nvarchar(max)
,@ConsPar as nvarchar(max)
,@ClinicPar as nvarchar(max)
,@ApptType as nvarchar(max)

as
select  

TOP 30

OP.[Specialty(Function)],
OP.[SpecialtyCode(Function)],
OP.[ProfessionalCarerCode],
OP.[ProfessionalCarerName],
OP.ClinicCode,
OP.Clinic,
OP.[AppointmentType],
OP.[AppointmentTypeNHSCode]

,COUNT(1) AS 'Number of DNAd Appts'


FROM [WHREPORTING].[OP].[Schedule] OP


WHERE

IsWardAttender = 0
AND
OP.ExcludedClinic = '0'
AND
(
AttendStatus IN 
	(
	'Refused to Wait',
	'Patient Left / Not seen',
	'Patient Late / Not Seen',
	'Did Not Attend'
	) 
	OR 
	(
	AttendStatus IN 
	(
	'Attended',
	'Attended on Time',
	'Patient Late / Seen'
	)
	AND Outcome = 'Did Not Attend'
	)
	)
	
	
AND 
(OP.AppointmentTypeNHSCode IN (1, 2) OR OP.AppointmentTypeNHSCode IS NULL)
AND 
AdministrativeCategory <> 'Private Patient'
AND
[SpecialtyCode(Function)]  NOT IN ('Unk','DQ')

	----Parameters----

----@StartDate as Date
----,@EndDate
----,@DIVPar as nvarchar(max)
----,@SpecPar as nvarchar(max)
----,@ConsPar as nvarchar(max)
----,@ClinicPar as nvarchar(max)
----,@ApptType as nvarchar(max)

	AND
	OP.AppointmentDate BETWEEN @StartDate and @EndDate
	
	and OP.Division 
	in (SELECT Val from dbo.fn_String_To_Table(@DIVPar,',',1))
	and OP.[SpecialtyCode(Function)] 
	in (SELECT Val from dbo.fn_String_To_Table(@SpecPar,',',1))
	and OP.ProfessionalCarerCode
	in (SELECT Val from dbo.fn_String_To_Table(@ConsPar,',',1))
	and OP.ClinicCode
	in (SELECT Val from dbo.fn_String_To_Table(@ClinicPar,',',1))
	and OP.[AppointmentTypeNHSCode]
	in (SELECT Val from dbo.fn_String_To_Table(@ApptType,',',1))
	
GROUP BY
OP.[Specialty(Function)],
OP.[SpecialtyCode(Function)],
OP.[ProfessionalCarerCode],
OP.[ProfessionalCarerName],
OP.ClinicCode,
OP.Clinic,
OP.[AppointmentType],
OP.[AppointmentTypeNHSCode]

ORDER BY
COUNT(1) DESC