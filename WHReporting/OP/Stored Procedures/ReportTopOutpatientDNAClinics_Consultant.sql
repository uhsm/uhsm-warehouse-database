﻿/*
--Author: N Scattergood
--Date created: 23/01/2013
--Stored Procedure Built for SRSS Report on:
--Top Outpatient DNA Clinics RD076
--This Procedure provides the Parameters
*/


CREATE Procedure [OP].[ReportTopOutpatientDNAClinics_Consultant]

@StartDate as Date
,@EndDate as Date
,@DIVPar as nvarchar(max)
,@SpecPar as nvarchar(max)
--,@ConsPar as nvarchar(max)
--,@ClinicPar as nvarchar(max)
--,@ApptType as nvarchar(max)

as
select  

Distinct

--OP.Division as DIVPar

--OP.[Specialty(Function)]	as SpecPar,
--OP.[SpecialtyCode(Function)]as SpecCodePar,

OP.[ProfessionalCarerCode]	as ConsCodePar,
OP.[ProfessionalCarerName]	as ConsPar

--OP.ClinicCode		as ClinicCodePar,
--OP.Clinic			as ClinicPar

--OP.[AppointmentType]		as ApptTypePar,
--OP.[AppointmentTypeNHSCode] as ApptTypeCodePar




FROM [WHREPORTING].[OP].[Schedule] OP


WHERE

IsWardAttender = 0
AND
OP.ExcludedClinic = '0'
AND
(
AttendStatus IN 
	(
	'Refused to Wait',
	'Patient Left / Not seen',
	'Patient Late / Not Seen',
	'Did Not Attend'
	) 
	OR 
	(
	AttendStatus IN 
	(
	'Attended',
	'Attended on Time',
	'Patient Late / Seen'
	)
	AND Outcome = 'Did Not Attend'
	)
	)
	
	
AND 
(OP.AppointmentTypeNHSCode IN (1, 2) OR OP.AppointmentTypeNHSCode IS NULL)
AND 
AdministrativeCategory <> 'Private Patient'

	----Parameters----

----@StartDate as Date
----,@EndDate
----,@DIVPar as nvarchar(max)
----,@SpecPar as nvarchar(max)
----,@ConsPar as nvarchar(max)
----,@ClinicPar as nvarchar(max)
----,@ApptType as nvarchar(max)

	AND
	OP.AppointmentDate BETWEEN @StartDate and @EndDate
	
	and OP.Division 
	in (SELECT Val from dbo.fn_String_To_Table(@DIVPar,',',1))
	and OP.[SpecialtyCode(Function)] 
	in (SELECT Val from dbo.fn_String_To_Table(@SpecPar,',',1))
	--and OP.ProfessionalCarerCode
	--in (SELECT Val from dbo.fn_String_To_Table(@ConsPar,',',1))
	--and OP.ClinicCode
	--in (SELECT Val from dbo.fn_String_To_Table(@ClinicPar,',',1))
	--and OP.[AppointmentTypeNHSCode]
	--in (SELECT Val from dbo.fn_String_To_Table(@ApptType,',',1))
	


ORDER BY
OP.[ProfessionalCarerName]