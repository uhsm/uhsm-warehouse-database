﻿/*
--Author: N Scattergood
--Date created: 23/01/2013
--Stored Procedure Built for SRSS Report on:
--Top Outpatient DNA Clinics RD076
--This Report is used to provide Patient Details on the Drillthrough
*/



CREATE Procedure [OP].[ReportTopOutpatientDNAClinics_DrillThrough]


@StartDate as Date
,@EndDate as Date
--,@DIVPar as nvarchar(max)
--,@SpecPar as nvarchar(max)
,@ConsPar as nvarchar(max)
,@ClinicPar as nvarchar(max)
--,@ApptType as nvarchar(max)

as

Select

OP.FacilityID as 'RM2 Number'
,PAT.PatientForename + ' ' + PAT.PatientSurname as 'PatientName'
      ,OP.[AppointmentDate]
      ,OP.[AppointmentTime]
      ,OP.[AppointmentDateTime]
      ,OP.[ClinicCode]
      ,OP.[Clinic] 
      ,OP.[Specialty(Function)]
      ,OP.ProfessionalCarerName  
      ,OP.[AppointmentRequestDate]   
      ,OP.[AppointmentCreated]
      ,OP.[AppointmentType]
	  ,OP.[AppointmentTypeNHSCode]
      ,SQ.NumberOfDNAs
	
 FROM [WHREPORTING].[OP].[Schedule] OP
 
		left join [WHREPORTING].[OP].[Patient] PAT
		on OP.EncounterRecno = PAT.EncounterRecno
		
			Left Join
-----Sub Query----
(
      Select 
      COUNT(*) as NumberOfDNAs,
      FacilityID, 
      ReferralSourceUniqueID,
      [SpecialtyCode(Function)]

      FROM [WHREPORTING].[OP].Schedule SCH
      WHERE
 
(
AttendStatus IN 
	(
	'Refused to Wait',
	'Patient Left / Not seen',
	'Patient Late / Not Seen',
	'Did Not Attend'
	) 
	OR 
	(
	AttendStatus IN 
	(
	'Attended',
	'Attended on Time',
	'Patient Late / Seen'
	)
	AND Outcome = 'Did Not Attend'
	)
	)
			----Parameters----

	AND
	SCH.AppointmentDate > DATEADD (year,-1,@StartDate)
 --	and SCH.Division 
	--in (SELECT Val from dbo.fn_String_To_Table(@DIVPar,',',1))
	--and SCH.[SpecialtyCode(Function)] 
	--in (SELECT Val from dbo.fn_String_To_Table(@SpecPar,',',1))
	--and SCH.ClinicCode
	--in (SELECT Val from dbo.fn_String_To_Table(@ClinicPar,',',1))
			
			AND 
(sch.AppointmentTypeNHSCode IN (1, 2) OR sch.AppointmentTypeNHSCode IS NULL)
		
		group by
		FacilityID, 
		ReferralSourceUniqueID,
		[SpecialtyCode(Function)]
		)   as SQ
----End of SQ ----

on SQ.ReferralSourceUniqueID = OP.ReferralSourceUniqueID



 
 WHERE

IsWardAttender = 0
AND
OP.ExcludedClinic = '0'
AND
(
AttendStatus IN 
	(
	'Refused to Wait',
	'Patient Left / Not seen',
	'Patient Late / Not Seen',
	'Did Not Attend'
	) 
	OR 
	(
	AttendStatus IN 
	(
	'Attended',
	'Attended on Time',
	'Patient Late / Seen'
	)
	AND Outcome = 'Did Not Attend'
	)
	)
	
	
AND 
(OP.AppointmentTypeNHSCode IN (1, 2) OR OP.AppointmentTypeNHSCode IS NULL)
AND 
AdministrativeCategory <> 'Private Patient'
AND
OP.[SpecialtyCode(Function)]  NOT IN ('Unk','DQ')



	----Parameters----

----@StartDate as Date
----,@EndDate
----,@DIVPar as nvarchar(max)
----,@SpecPar as nvarchar(max)
----,@ConsPar as nvarchar(max)
----,@ClinicPar as nvarchar(max)
----,@ApptType as nvarchar(max)

	AND
	OP.AppointmentDate BETWEEN @StartDate and @EndDate
	
	--and OP.Division 
	--in (SELECT Val from dbo.fn_String_To_Table(@DIVPar,',',1))
	--and OP.[SpecialtyCode(Function)] 
	--in (SELECT Val from dbo.fn_String_To_Table(@SpecPar,',',1))
	and OP.ProfessionalCarerCode
	in (SELECT Val from dbo.fn_String_To_Table(@ConsPar,',',1))
	and OP.ClinicCode
	in (SELECT Val from dbo.fn_String_To_Table(@ClinicPar,',',1))
	--and OP.[AppointmentTypeNHSCode]
	--in (SELECT Val from dbo.fn_String_To_Table(@ApptType,',',1))


order by SQ.NumberOfDNAs desc,OP.AppointmentDate desc