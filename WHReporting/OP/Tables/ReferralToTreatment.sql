﻿CREATE TABLE [OP].[ReferralToTreatment](
	[EncounterRecno] [int] NOT NULL,
	[SourceUniqueID] [int] NOT NULL,
	[RTTPathwayID] [varchar](200) NULL,
	[RTTStartDate] [smalldatetime] NULL,
	[RTTEndDate] [smalldatetime] NULL,
	[RTTSpecialtyCode] [varchar](5) NULL,
	[RTTCurrentStatusCode] [varchar](20) NULL,
	[RTTCurrentStatus] [varchar](80) NULL,
	[RTTCurrentStatusDate] [smalldatetime] NULL,
	[RTTPeriodStatusCode] [varchar](20) NULL,
	[RTTPeriodStatus] [varchar](80) NULL,
PRIMARY KEY CLUSTERED 
(
	[SourceUniqueID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]