﻿CREATE view [OP].[AppointmentStatusUpdate] as
/**
KO 16/05/2014
Temporary view to show changes in reference data for appointment attend status
and impact on recording of hospital cancellation vs patient cancellation
UpdatedAttendStatusNHSCode and UpdatedAttendanceStatusCode now taken from 
DNACode generated in WH.dbo.TLoadOPEncounter.
Logic identifying whether a hospital or patient cancelled appointment 
dependent on ScheduledCancelReasonCode value
**/
Select Distinct
	Encounter.DistrictNo
	,Encounter.SourceUniqueID
	,Encounter.AppointmentTime
	,Encounter.ClinicCode
	,Encounter.AppointmentStatusCode
	,CurrentAttendStatusCode = Attnd.AttendStatusLocalCode
	,CurrentAttendStatus = Attnd.AttendStatusDescription
	,CurrentAttendStatusNHSCode = Attnd.AttendStatusNationalCode
	,CurrentAttendanceStatusCode = Encounter.AttendanceStatusCode
	,UpdatedAttendStatusCode = attendWH.MAIN_CODE
	,UpdatedAttendStatus = attendWH.DESCRIPTION
	,UpdatedAttendStatusNHSCode = Encounter.DNACode
	,UpdatedAttendanceStatusCode = Encounter.AttendanceStatusCode2
	,CurrentCancelledByLocalCode = cancb.CancelledByLocalCode
	,CurrentCancelledByDescription = cancb.CancelledByDescription
	,UpdatedCancelledByLocalCode = cancbWH.MAIN_CODE
	,UpdatedCancelledByDescription = cancbWH.DESCRIPTION
	,CancelledBy = CancelledReasonLu.MappedCode
	,ScheduledCancelReason = CancelledReasonLu.ReferenceValue
	,Encounter.AppointmentCancelDate

FROM WHOLAP.dbo.OlapOP Encounter
left outer join dbo.vwPASScheduleAttendStatus Attnd
	on Encounter.AppointmentStatusCode = Attnd.AttendStatusCode
left outer join dbo.vwPASScheduleCancelledBy cancb
	on Encounter.CancelledByCode = cancb.CancelledByCode
left join WH.PAS.ReferenceValueBase attendWH
	on attendWH.RFVAL_REFNO = Encounter.AppointmentStatusCode
left join WH.PAS.ReferenceValueBase cancbWH
	on cancbWH.RFVAL_REFNO = Encounter.CancelledByCode
left join WH.PTL.OPLookups CancelledReasonLu 
	on Encounter.ScheduledCancelReasonCode = CancelledReasonLu.ReferenceValueCode
where SourceUniqueID in(
	--Select SourceUniqueID
	--from WHOLAP.dbo.OlapOP
	--where DNACode = 2 and AttendanceStatusCode = 'HOSPCAN'
	select SourceUniqueID
	from WHOLAP.dbo.OlapOP
	where AttendanceStatusCode2 <> AttendanceStatusCode
)
--order by Encounter.AppointmentTime desc