﻿CREATE View [OP].[SessionUtilisation] as 

Select
	 SessionUniqueID = fSession.SessionUniqueID
	,Division = Specialty.Division
	,Directorate = Specialty.Direcorate
	,MainSpecialtyCode = Specialty.MainSpecialtyCode
	,MainSpecialty = Specialty.MainSpecialty
	,TreatmentFunctionCode = Specialty.SpecialtyFunctionCode
	,TreatmentFunction = Specialty.SpecialtyFunction
	,SpecialtyCode = Specialty.SpecialtyCode
	,Specialty = Specialty.Specialty
	,ProfessionalCarerCode = Profcarer.MAIN_IDENT 
	,ProfessionalCarer = Profcarer.SURNAME + ', ' + Profcarer.FORENAME + ' (' + Profcarer.MAIN_IDENT + ')'
	,ClinicCode = CLinic.ClinicCode
	,ExcludedClinic = --Case When ExcludedClinic.ServicePointUniqueID is null then 'N' else 'Y' End
						Case When fSession.ExcludedClinic = 1 Then 'Y' Else 'N' End
	,ClinicDescription = CLinic.ClinicDescription
	,SessionInceptionDate = Template.SessionInceptionDate
	,SessionConclusionDate = Template.SessionConclusionDate
	,SessionCancellationDate = fsession.SessionCancelledDate
	,SessionIsActive = 
			Case When Template.SessionConclusionDate = '1900-01-01' OR fSession.SessionDate <= Template.SessionConclusionDate THEN
				'Y'
			Else

				'N'
			End
	,SessionCode = Template.SessionCode
	,SessionDescription = Template.SessionName
	,SessionDate = fSession.SessionDate
	,SessionStartTime = fSession.SessionStartTime
	,SessionEndTime = fSession.SessionEndTime
	,SessionDuration = fSession.SessionDuration
	,SlotDuration = fSession.SlotDuration
	,NumberOfSlotsInSession = fSession.NumberOfSlots
	,IsCancelled = CAse When fSession.CancelledSessionIndicator = 0 Then 'N' Else 'Y' End
	,SessionStatus = SessionStatus.SessionStatus
	,[AllowNewBooking] = Case When fSession.[AllowNewBooking] = 0 Then 'N' Else 'Y' End
	,[AllowFollowUpBooking] = Case When fSession.[AllowFollowUpBooking] = 0 Then 'N' Else 'Y' End
	,[AllowOverBooking] = Case When fSession.[AllowOverBooking] = 0 Then 'N' Else 'Y' End
	,[BookingsAllowed] = fSession.[BookingsAllowed]
	,[BookingsMade] = fSession.[BookingsMade]
	,[BookingsFree] = fSession.[BookingsFree]
	,[Utilisation] = fSession.Utilisation
	,[IsOverbooked] = Case When fSession.[IsOverbooked] = 0 Then 'N' Else 'Y' End
	,[NewAttended] = fSession.[NewAttended]
	,[NewDNA] = fSession.[NewDNA]
	,[NewCancelledPatient] = fSession.[NewCancelledPatient]
	,[NewCancelledHospital] = fSession.[NewCancelledHospital]
	,[NewCancelledOther] = fSession.[NewCancelledOther]
	,[NewRescheduled] = fSession.[NewRescheduled]
	,[FollowUpAttended] = fSession.[FollowUpAttended]
	,[FollowUpDNA] = fSession.[FollowUpDNA]
	,[FollowUpCancelledPatient] = fSession.[FollowUpCancelledPatient]
	,[FollowUpCancelledHospital] = fSession.[FollowUpCancelledHospital]
	,[FollowUpCancelledOther] = fSession.[FollowUpCancelledOther]
	,[FollowUpRescheduled] = fSession.[FollowUpRescheduled]
	,[NotCashedUp] = fSession.[NotCashedUp]
	,fSession.SessionComments

From OP.BookingFact_Session fSession
Left OUter JOin OP.SessionTemplateDetail Template
	on fSession.SessionTemplateUniqueID = Template.SessionUniqueID
Left Outer Join LK.SpecialtyDivision Specialty
	on fSession.SessionSpecialtyUniqueID = Specialty.SpecialtyRefno
left outer join WH.OP.Clinic CLinic
	on fSession.ServicePointUniqueID = CLinic.ServicePointUniqueID
left outer join WH.PAS.ProfessionalCarerBase Profcarer
	on fSession.SessionProfessionalCarerUniqueID = Profcarer.PROCA_REFNO
--left outer join LK.OPExcludedClinic ExcludedClinic
--	on fSession.ServicePointUniqueID = ExcludedClinic.ServicePointUniqueID
left outer join LK.OPSessionStatus SessionStatus
	on fSession.SessionStatusID = SessionStatus.SessionStatusID
Where fSession.ExcludedClinic = 0