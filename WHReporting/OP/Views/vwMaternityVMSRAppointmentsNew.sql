﻿CREATE view [OP].[vwMaternityVMSRAppointmentsNew] AS
/*
--Author: K Oakden
--Date created: 09/09/2013
--View to correct vwMaternityVMSRAppointmentsNew by joining to the latest referral on Evolution
to remove duplicates.

--To add date parameters
	select *
	from OP.vwMaternityVMSRAppointments
	where AppointmentDate between '2013-05-01' and '2013-05-31'

*/

select distinct
	Encounter.SourceUniqueID,
	--Encounter.ReferralSourceUniqueID,
	Encounter.FacilityID,
	SourcePatient.Patient_Pointer,
	Encounter.AppointmentDate,
	Encounter.ClinicCode,
	ClinicName = Encounter.Clinic,
	Encounter.AttendStatus,
	ReferralDate = Referral.[Date of Referral],
	Patient.PatientSurname,
	Patient.PatientForename,
	Patient.DateOfBirth,
	InitialEstimateOfDueDate = Referral.[Initial estimate of due date],
	FinalDueDate = Pregnancy.[Final due date],
	BestEDDFromLMPData = Pregnancy.[Best EDD from LMP data],
	Patient.Postcode,
	Encounter.AppointmentPCTCCGCode,
	Encounter.AppointmentPCTCCG,
	Encounter.AgeAtAppointment
from OP.Schedule Encounter

left join OP.Patient Patient
	on Patient.EncounterRecno = Encounter.EncounterRecno
	
left join MaternityReporting.dbo.view_Patients SourcePatient
	on SourcePatient.[Unit Number] COLLATE DATABASE_DEFAULT = Encounter.FacilityID COLLATE DATABASE_DEFAULT
	
left join MaternityReporting.dbo.view_Referrals Referral
	--select Patient_Pointer, Pregnancy_ID, [Pregnancy number], [Initial estimate of due date] , [Date of Referral]
	--from 
	on Referral.Patient_Pointer = SourcePatient.Patient_Pointer
	and Referral.[Date of Referral] <= Encounter.AppointmentDate
	and DATEDIFF(day, Referral.[Date of Referral], Encounter.AppointmentDate) < 260
	--where referral.Patient_Pointer = 74174
	and not exists (
		select 1 from MaternityReporting.dbo.view_Referrals LaterReferral
		where LaterReferral.Patient_Pointer = Referral.Patient_Pointer
		and LaterReferral.[Date of Referral] <= Encounter.AppointmentDate
		and DATEDIFF(day, Referral.[Date of Referral], Encounter.AppointmentDate) < 260
		and LaterReferral.[Date of Referral] > Referral.[Date of Referral]
		)
	--and not exists (
	--	select 1 from MaternityReporting.dbo.view_Referrals LaterPregnancy
	--	where LaterPregnancy.Patient_Pointer = Referral.Patient_Pointer
	--	and LaterPregnancy.Pregnancy_ID > Referral.Pregnancy_ID
	--	)
	--)
	--EvolutionReferral

left join MaternityReporting.dbo.view_Pregnancy Pregnancy
	on Pregnancy.Patient_Pointer = Referral.Patient_Pointer
	and Pregnancy.Pregnancy_ID = Referral.Pregnancy_ID
	and Pregnancy.[Pregnancy number] = Referral.[Pregnancy number]
	
where Encounter.ClinicCode IN 
	(
	'MLCWCH',
	'MLCWHP',
	'MLCWHP2',
	'MLCWITH',
	'MLCWYTH',
	'MLC2P2',
	'MLCAM2',
	'MLCBAGULEY',
	'MLCBENCHILL',
	'MLCBROADHEATH',
	'MLCBROOKLAND',
	'MLCBROOMWOOD',
	'MLCBURNAGE',
	'MLCDAVY',
	'MLCDUCK',
	'MLCKELLY',
	'MLCKELLY2',
	'MLCLIME',
	'MLCLIME2',
	'MLCMPM',
	'MLCONWAY',
	'MLCPART',
	'MLCPM1',
	'MLCROYLE',
	'MLCSALE',
	'MLCSAND',
	'MLCTRAFFORD',
	'MLC2P2',
	'MLCPM1',
	'MLC1P2',
	'MLCSAND',
	'MLCBAGULEY',
	'MLCBURNAGE',
	'MLCWCH',
	'MLCBENCHILL',
	'MLCWYTH',
	'MLCWHP',
	'MID1ABK',
	'MID2ABK',
	'MID3ABK',
	'MID5ABK',
	'MID1PBK',
	'MID2PBK',
	'MID3PBK',
	'MID4PBK',
	'MID5PBK',
	'MIDTABK',
	'MIDWABK',
	'MLC2ABK',
	'MIDTPBK',
	'MIDWPBK',
	'MLCROYLE',
	'MLCSALE',
	'MID4ABK',
	'MLCWHP2',
	'MLCWITH',
	'MLCDUCK',
	'MLCKELLY',
	'MLCKELLY2',
	'BROOKCC',
	'MLCBROOKLAND',
	'MIDBK',
	'MLCLIME',
	'MLCCOPPICE',
	'MLCTRAFFORD',
	'MIDBKTRAFFORD',
	'MLCBROADHEATH',
	'MLCBROOMWOOD',
	'MLCPART',
	'MLCONWAY',
	'MLCLIME2',
	'MLCDAVY',
	'MLCTIMPERLEY',
	'MLCFLIXTON',
	'MLCNEWALL',
	'MLCSTYAL',
	'MLCCONWAY'
	)

and Encounter.AppointmentDate >='20100701 00:00:00' 
and Encounter.AppointmentDate < cast(convert(varchar(8), getdate(),112) as datetime)
--and (Encounter.AttendStatusNHSCode not in ('2', '3', '4') OR Encounter.AttendStatusNHSCode is null)
--KO updated 29/05/2014 after reference value update
and Encounter.AttendStatusNHSCode in ('5', '6')--attended