﻿CREATE View [OP].[vwSessionSlotType] as 

select sessionuniqueId, sum(totalcount) as Total, sum([New]) as New, sum([Follow Up]) as [Follow-Up], sum([Walk-in]) as [Walk-in], sum([Walk-in Follow Up]) as [Walk-in Follow Up], 
sum([Not Specified]) as [Not Specified], sum([Panel]) as [Panel], sum([Walk-in New]) as [Walk-in New], sum([A&E attendance]) as [A&E attendance]

FROM [OP].[vwSlotType] as X

Pivot(sum(Slotmaxbookings)
For [Description] IN ([New], [Follow Up], [Walk-in], [Walk-in Follow Up], 
[Not Specified], [Panel], [Walk-in New], [A&E attendance])

) As pivottable

--where sessionuniqueid = ('10808797')
GROUP BY sessionuniqueid
--order by sessionuniqueid