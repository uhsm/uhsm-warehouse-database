﻿CREATE View [OP].[vwSlotType] as 

select Slot.SlotUniqueID, slot.SessionUniqueID, Slot.SlotMaxBookings
,x.[AppliedTo],  x.[RuleValueCode]
      ,x.[DESCRIPTION], 1 as TotalCount
from WH.OP.Slot Slot
left join 

(SELECT 
--r.[RuleRecno]
--      ,r.[RuleUniqueID]
distinct 
      r.[AppliedTo]
      ,r.[AppliedToUniqueID]
      --,r.[EnforcedFlag]
      --,r.[RuleAppliedUniqueID]
      ,r.[RuleValueCode]
      ,b.[DESCRIPTION]
      --,r.[ArchiveFlag]
      --,r.[PASCreated]
      --,r.[PASUpdated]
      --,r.[PASCreatedByWhom]
      --,r.[PASUpdatedByWhom]
      --,r.[Created]
      --,r.[updated]
      --,r.[ByWhom]
  FROM [WH].[OP].[Rule] r
  
  left join WH.PAS.ReferenceValueBase b
  on r.RuleValueCode = b.RFVAL_REFNO
  
  where RuleAppliedUniqueID = '14'
  and AppliedTo = 'SPSLT'
  
  and r.ArchiveFlag = '0'

) AS X 
ON Slot.SlotUniqueID = X.[AppliedToUniqueID]
where Slot.ArchiveFlag = '0'


--select sessionuniqueId, [New], [Follow Up], [Walk-in], [Walk-in Follow Up], 
--[Not Specified], [Panel], [Walk-in New], [A&E attendance]

--FROM #temp2 as X

--Pivot(sum(Slotmaxbookings)
--For [Description] IN ([New], [Follow Up], [Walk-in], [Walk-in Follow Up], 
--[Not Specified], [Panel], [Walk-in New], [A&E attendance])

--) As pivottable
--where sessionuniqueid = ('10808797')
----GROUP BY sessionuniqueid
--order by sessionuniqueid