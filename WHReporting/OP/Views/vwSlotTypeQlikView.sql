﻿CREATE View [OP].[vwSlotTypeQlikView] as 

Select 
  Slot.SlotUniqueID 
  ,Slot.SessionUniqueID
  ,Slot.SlotDate
  ,Slot.SlotMaxBookings
  ,Slot.SlotNumberofBookings
  ,ref.ReferenceValue
  ,DESCRIPTION
  ,SeU.Division
  ,SeU.Directorate
  ,SeU.SpecialtyCode
  ,SeU.Specialty
  ,SeU.TreatmentFunctionCode
  ,SeU.TreatmentFunction
  ,SeU.ClinicCode
  FROM [WH].[OP].[Slot]
  LEFT OUTER JOIN (Select * from WH.PAS.ReferenceValue where ReferenceDomainCode = 'TSTAT') ref on
  Slot.SlotStatusCode = ref.ReferenceValueCode

   
  LEFT OUTER JOIN

(SELECT 
distinct 
      r.[AppliedTo]
      ,r.[AppliedToUniqueID]

      ,r.[RuleValueCode]
      ,b.[DESCRIPTION]
  FROM [WH].[OP].[Rule] r
  
  left join WH.PAS.ReferenceValueBase b
  on r.RuleValueCode = b.RFVAL_REFNO
  
  where RuleAppliedUniqueID = '14'
  and AppliedTo = 'SPSLT'
  
  and 
  r.ArchiveFlag = '0'

) AS X
ON Slot.SlotUniqueID = X.[AppliedToUniqueID]
  left join  [WHREPORTING].[OP].[sessionutilisation] SeU
 on Slot.SessionuniqueID = SeU.SessionUniqueID 
  LEFT OUTER JOIN [WHREPORTING].[dbo].vwExcludeWAWardsandClinics AS Exc 
  ON SeU.ClinicCode = Exc.SPONT_REFNO_CODE
   
Where Slot.SlotDate >= '2015-09-01' 
--and Slot.SlotUniqueID = '192522494'
--and SessionUniqueID =  '153492645'
    AND SeU.sessionisactive = 'Y'
    AND SeU.iscancelled = 'N'
    AND SeU.excludedclinic = 'N'
    AND (SeU.ClinicCode NOT IN('JKB1AM-NEW','JKB1AM-FU','AKU1NEW','AKU1PM','CAP1AM-NEW','CAP1AM-FU','UMA2PM','HOLT2AM','LANG3AM','MEL3AM-NEW','MEL3AM-FU','AFO3PM-NEW','AFO3PM-FU','NJPI4AM','ANP4AM-NEW','ANP4AM-FU','LHA5AM-NEW','LHA5AM-FU','VFC'))
    AND Exc.SPONT_REFNO_CODE IS NULL
   -- AND SEU.TreatmentFunctionCode in ('110','101','420','191','430','107','100')