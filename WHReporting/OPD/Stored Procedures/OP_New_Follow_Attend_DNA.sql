﻿CREATE Procedure [OPD].[OP_New_Follow_Attend_DNA]

@StartDate date 
,@EndDate date
,@Division nvarchar(Max)
,@Directorate nvarchar(Max)
,@Specialty nvarchar(Max)
,@Consultant nvarchar(Max) 
,@Clinics nvarchar(Max)

as 



/*
Declare 
@StartDate date = '20150401'
,@EndDate date = '20160229'
,@Division nvarchar(Max) = 'Scheduled Care'
,@Directorate nvarchar(Max) =  'Cardiothoracic'
,@Specialty nvarchar(Max) = '170'
,@Consultant nvarchar(Max) = 'C3226274'
,@Clinics nvarchar(Max) = 'NYONTRANS'
*/


Select
	@StartDate 
	,@EndDate 
	,@Division
	,@Directorate
	,@Specialty 
	,@Consultant 
	,@Clinics 


 

--;With cteBasedata
--as (
--Select distinct 
--	--OP.[Division]  
--	--,OP.Directorate 
--	--,OP.[SpecialtyCode(Function)]
--	--,OP.ProfessionalCarerCode 
--	--,OP.ClinicCode 
-- --		,
-- 		Case when AppointmentTypeNHSCode = 1
--			then 'New'
--		when (AppointmentTypeNHSCode = 2 
--		   or AppointmentTypeNHSCode IS NULL)
--			then 'Follow Up'
--		end														   as ApptType
--   ,sum(case when OP.Status = 'DNA' 
--      or (OP.status = 'Attend'AND OP.Outcome = 'Did Not Attend')
--      then 1 else 0 end)                                           as DNAs
--   ,sum(case when OP.Status = 'Attend' and (OP.outcome IS NULL
--		OR  (OP.Outcome NOT LIKE '%canc%'AND OP.Outcome
--		NOT LIKE '%Did Not Attend%'))
--	then 1 else 0 end)                                             as Attends
--   ,sum(case when (OP.Status = 'Attend' and 
--		(OP.outcome IS NULL OR  
--		(OP.Outcome NOT LIKE '%canc%'AND OP.Outcome 
--		 NOT LIKE '%Did Not Attend%')))
--			or
--		(OP.Status = 'DNA'  or (OP.status = 'Attend' 
--		AND OP.Outcome = 'Did Not Attend'))
--	then 1
--	else 0 end)                                                    as Total
	
--  ,CONVERT(CHAR(4), OP.AppointmentDate, 100) + CONVERT(CHAR(4), OP.AppointmentDate, 120) AS MonthYear	

--From OP.Schedule AS OP
--Left JOIN vwExcludeWAWardsandClinics AS Exc ON OP.ClinicCode = Exc.SPONT_REFNO_CODE
--	AND Exc.SPONT_REFNO_CODE IS NULL
--Where 
--   (OP.AppointmentTypeNHSCode IN (1, 2) OR OP.AppointmentTypeNHSCode IS NULL)
--	AND OP.IsWardAttender = 0 
--	AND OP.STATUS in ('DNA','ATTEND') 
--	AND OP.AdministrativeCategory NOT LIKE '%Private%'
--	AND (OP.AppointmentDate >= @StartDate AND OP.AppointmentDate <= @EndDate) 
	
--	/*parameter filters*/
--	and OP.[Division]  IN (SELECT Item FROM WHREPORTING.dbo.Split (@Division , ','))
--	and OP.Directorate IN (SELECT Item FROM WHREPORTING.dbo.Split (@Directorate , ','))
--	and OP.[SpecialtyCode(Function)] IN (SELECT Item FROM WHREPORTING.dbo.Split (@Specialty , ','))
--	and OP.ProfessionalCarerCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Consultant , ',')) 
--	and OP.ClinicCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Clinics , ',')) 
--Group by OP.AppointmentTypeNHSCode, CONVERT(CHAR(4), OP.AppointmentDate, 100) + CONVERT(CHAR(4), OP.AppointmentDate, 120)

--)
--,
--ctesummeddate
--as
--(	
--Select 
--	 MonthYear
--	 ,sum(case when ApptType = 'New' then Attends else null end)					  as New_Attends
--	 ,sum(case when ApptType = 'New' then DNAs else null end)						  as New_DNAs 
--	 ,sum(case when ApptType = 'New' then Total else null end)						  as New_Total
--	 --,sum(cast(case when ApptType = 'New' then DNAs else null end as float)
--	 --/cast(case when ApptType = 'New' then Total else null end as float))		      as New_Percentage
--	 ,sum(case when ApptType = 'Follow Up' then Attends else null end)				  as FollowUp_Attends
--	 ,sum(case when ApptType = 'Follow Up' then DNAs else null end)					  as FollowUp_DNAs 
--	 ,sum(case when ApptType = 'Follow Up' then Total else null end)				  as FollowUp_Total
--	 ,sum(cast(case when ApptType = 'Follow Up' then DNAs else null end as float)
--	 /cast(case when ApptType = 'Follow Up' then Total else null end as float))       as FollowUp_Percentage
--	 --,sum(case when ApptType = 'Follow Up' then Attends else null end)
--	 --/
--	 --sum(case when ApptType = 'New' then Attends else null end)		
--from cteBasedata
--Group by 	
--    MonthYear
--)


--Select 
--	 MonthYear
--	 ,New_Attends
--	 ,New_DNAs 
--	 ,New_Total
--	 ,(cast(New_DNAs as float)
--	 /cast(New_Total as float))*100		      as New_Percentage
--	 ,FollowUp_Attends
--	 ,FollowUp_DNAs 
--	 ,FollowUp_Total
--	 ,cast(FollowUp_DNAs as float)
--	 /cast(FollowUp_Total as float)       as FollowUp_Percentage 
--	 ,cast(FollowUp_Attends as float) 
--	  /cast(New_Attends as float)         as New_FollowUp_Ratio  
--     ,0.065                               as DNATarget
--from ctesummeddate