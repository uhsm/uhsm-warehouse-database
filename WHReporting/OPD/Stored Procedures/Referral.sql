﻿Create Procedure [OPD].[Referral]

@StartDate date 
,@EndDate date
,@Division nvarchar(Max)
,@Directorate nvarchar(Max)
,@Specialty nvarchar(Max)

as


/*Declare 
@StartDate date = '20151201'
,@EndDate date = '20151231'
,@Division nvarchar(Max) = 'Scheduled Care'
,@Directorate nvarchar(Max) =  'Surgery'
,@Specialty nvarchar(Max) = '100'
*/

SELECT 
    LK.Calendar.TheMonth
    ,case when ReferralSourceNHSCode IN ('03','12')
	  then 'GP Referral' else ('Other')
	  end                                             as ReferralGroup
	,RF.Referral.Priority
	,COUNT(*)
FROM RF.Referral
LEFT JOIN LK.Calendar ON LK.Calendar.TheDate = RF.Referral.ReferralReceivedDate
Where 
	 RF.Referral.ReferralReceivedDate BETWEEN @StartDate	AND @EndDate
	AND Division IN (@Division)
	AND Directorate IN (@Directorate)
	AND RF.Referral.[ReceivingSpecialtyCode(Function)] IN (@Specialty)
	AND (
	    RF.Referral.ReferralTypeCode IN ('1','2','4','5')
		OR RF.Referral.ReferralTypeCode IS NULL
		) -- Gets rid of duplicates
	AND RequestedService IN (
		'Outpatient Activity'
		,'Not Specified'
		,'Other'
		,'Accident & Emergency'
		) -- Looks just at OP referrals and Not Specified
	AND (
		rf.Referral.ReferralType IN (
			'Choose and Book'
			,'Paper'
			)
		OR RF.Referral.ReferralType IS NULL
		) -- Just includes Paper/Choose and Book
GROUP BY CASE 
		WHEN ReferralSourceNHSCode IN (
				'03'
				,'12'
				)
			THEN 'GP Referral'
		ELSE ('Other')
		END
	,LK.Calendar.TheMonth
	,RF.Referral.Priority