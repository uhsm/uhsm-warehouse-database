﻿CREATE Procedure [OPD].[SlotSessionUtil]

@StartDate date 
,@EndDate date
,@Division nvarchar(Max)
,@Directorate nvarchar(Max)
,@Specialty nvarchar(Max)
,@Clinics nvarchar(Max)
,@Consultant nvarchar(Max) 

as

/*Declare 
	@StartDate date = '20151201'
	,@EndDate date = '20151231'
	,@Division nvarchar(Max) = 'Scheduled Care'
	,@Directorate nvarchar(Max) =  'Surgery'
	,@Specialty nvarchar(Max) = '100'
	,@Clinics nvarchar(Max) = 'SH3REVAM'
	,@Consultant nvarchar(Max) = 'C3522303'
*/

;with cteSession 
as
(
Select Distinct
    /*slot Utilisation*/
	sum(case when (SessionStatus IN (
		'Session Scheduled','Session Initiated','Session Held')
		AND IsCancelled = 'N')   
		then BookingsAllowed else 0 end )                           as BookingsAllowed
	,sum(case when (SessionStatus IN (
		'Session Scheduled','Session Initiated','Session Held')
		AND IsCancelled = 'N')   
		then BookingsMade else 0 end )                              as BookingsMade
	,sum(case when (SessionStatus IN (
		'Session Scheduled','Session Initiated','Session Held')
		AND IsCancelled = 'N')   
		then BookingsFree else 0 end )                              as BookingsFree
	/*Session Utilisation*/
	,sum(case when SessionStatus IN (
		'Session Held','Session Initiated','Session Scheduled')
		then  1 else 0 end )                                        as Utilised
	,sum(case when SessionStatus IN ('Session Cancelled',
		'Session Held','Session Initiated','Session on Hold',
		'Session Scheduled') then  1 else 0
		 end)                                                       as TotalSessions
    /*Less than 6 weeks*/
    ,sum(case when IsCancelled = 'Y' then 1
         else 0 end)                                                as CountofCancelled
    ,sum(case when 
		DATEDIFF([d], SessionCancellationDate, SessionDate) >= 0
		and 
		DATEDIFF([d], SessionCancellationDate, SessionDate) <= 42
		then 1 else 0 end )                                         as LessThan6WksSessCan
   ,CONVERT(CHAR(4), SessionDate, 100) 
    + CONVERT(CHAR(4), SessionDate, 120)                            as MonthYear
FROM OP.SessionUtilisation
Where 
	SessionDate >= @StartDate
	AND SessionDate <= @EndDate
	AND SessionIsActive = 'Y'
	AND ExcludedClinic = 'N'
	AND Division   IN (SELECT Item FROM WHREPORTING.dbo.Split (@Division , ','))
	AND Directorate IN (SELECT Item FROM WHREPORTING.dbo.Split (@Directorate , ','))
	AND TreatmentFunctionCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Specialty , ','))
	--AND ProfessionalCarerCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Consultant , ',')) 
	--AND (
	--	ClinicCode IN (SELECT Item FROM WHREPORTING.dbo.Split (@Clinics , ',')) 
	--	AND ClinicCode NOT LIKE '%TELE%'
	--	AND ClinicDescription NOT LIKE '%TELE%'
	--	)
		Group by CONVERT(CHAR(4), SessionDate, 100) 
    + CONVERT(CHAR(4), SessionDate, 120)
)

/*xxxxxx*/

Select 
    /*slot Utilisation*/
     BookingsAllowed
	,BookingsMade
	,BookingsFree
	,cast(BookingsMade as float)/cast(BookingsAllowed as float)   as Slot_Percentage
	,0.95                                                         as SlotTarget
	/*Session Utilisation*/
	,Utilised
	,TotalSessions
	,0.8                                                          as SessionTarget 
	,cast(Utilised as float)/cast(TotalSessions as Float)         as Session_Percentage
    /*Less than 6 weeks*/
    ,CountofCancelled
    ,LessThan6WksSessCan
    /*TimePeriod*/
    ,MonthYear
From cteSession