﻿CREATE function [PTL].[GetAdjustedBreachDate](@startdate datetime, @suspendedEndDate datetime, 
											@suspensionPeriod int, @limit int)
returns datetime as

begin 

	declare @breachDate datetime
	select @breachDate = 
		case 
			when @suspendedEndDate > 
				--case 
				--	when @suspensionPeriod = 0
				--	then dateadd(day, @limit, @startdate)
					--else 
					dateadd(day, (@limit + @suspensionPeriod), @startdate)
				--end
			then @suspendedEndDate
			else 		
				--case 
				--	when @suspensionPeriod = 0
				--	then dateadd(day, @limit, @startdate)
					--else 
					dateadd(day, (@limit + @suspensionPeriod), @startdate)
				--end
		end
		return @breachDate
end