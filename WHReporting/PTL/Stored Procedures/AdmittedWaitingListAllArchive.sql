﻿CREATE procedure [PTL].[AdmittedWaitingListAllArchive] 
(
	@archivedate datetime
)as

/**
Author: K Oakden
Date: 26/11/2014
Month end admitted waiting list snapshot. Based on cognos query IPDC WL All.imr 

TO use:
exec [PTL].[AdmittedWaitingListAllArchive]  '20141126'
**/
declare @MonthEndDate datetime

select @MonthEndDate = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+1,0))

select * 
from RTT_DW.PTL.AdmittedArchive ptl
where Archivedate = @archivedate