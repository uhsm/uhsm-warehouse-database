﻿CREATE Procedure [PTL].[PTL_18wk_Intergrated_tool]
(
@Division  nvarchar(max)  
,@Directorate nvarchar(max) 
,@Specialty nvarchar(max) 
,@Consultant nvarchar(max)  
,@conrep int                 /*This parameters determines whether the SP is servicing the overall of consulant level report 
							0 = Overall and 1 = Consultant*/
)
as 

/*
Declare	@Division  nvarchar(255) = 'Scheduled Care'    
Declare @Directorate nvarchar(255) = 'Surgery'
Declare @Specialty nvarchar(255) = 'General Surgery' --'General Surgery'
Declare @Consultant nvarchar(255) =  'all'
Declare @conrep int = '0'
*/


/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/
/*code below has been added so it can be joined further on to ensure every line will have all week waits, 
regardless of whether theyn have any data for a particular week (in which case SSRS will add a 0 for that week no) in SSRS*/ 

Create table #weeknos
(
[Weekno] nvarchar(100)
)
Insert into #weeknos ([Weekno]) 
values ('0')
       ,('1')
    ,('2')
    ,('3')
    ,('4')
    ,('5')
    ,('6')
    ,('7')
    ,('8')
    ,('9')
    ,('10')
    ,('11')
    ,('12')
    ,('13')
    ,('14')
    ,('15')
    ,('16')
    ,('17')
    ,('18+')
/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/
/*Specialty/division temp table to ensure correct Division and Directorate are pulled through*/
SELECT Distinct
      [SpecialtyCode]
      ,[Specialty]
      ,[Direcorate]
      ,[Division]
 into #specdiv
  FROM [WHREPORTING].[LK].[SpecialtyDivision]
  where 
  Direcorate <> 'Unknown'
/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/ 
/*Non Admitted - Extracts Non admitted patient 18wk wait data*/
Select
	CAST(NA.CensusDate as Date)                                            as CensusDate    
    ,'Non-Admitted'                                                        as PathwayType
	,NA.FacilityID +' '+NA.PatientID                                       as PatientID
    ,NA.RTTWeekWaitActual                                                  as Current18WWait
    ,case 
		when NA.RTTWeekWaitActual > 17 then '18+'
		when NA.RTTWeekWaitActual < 0 then '0'
		else cast(NA.RTTWeekWaitActual as varchar(3))
		end                                                                as Current18WWaitGrp
    ,isnull(Spec.Division,'Scheduled Care')		                           as Division
    ,isnull(Spec.Direcorate,'Womens & Childrens')	                       as Directorate
    ,NA.SpecialtyCode	
    ,NA.Specialty
    ,DoctorSurname                                                         as Consultant
    ,case 
		when NextApptDate IS NULL 
		then 'Amber'
		when (
		NA.LastAttendedApptDate IS NULL
		OR 
		LastAttendedApptStatus IN ('Did Not Attend','Cancelled')
		) and NA.WeekWaitToAppt > 9
		then 'Red'
		when NA.WeekWaitToAppt > 17
		then 'Red'
		when NA.WeekWaitToAppt < 18
		then 'Green'
		else 'Uncategorised'
		end                                                                as [Colour]
					
    ,case 
		--News
		when 
		NA.LastAttendedApptDate IS NULL 
		AND NextApptDate IS NULL 
		then '1. New - No Appt'
		when 
		NA.LastAttendedApptDate IS NULL
		AND NA.WeekWaitToAppt <= 9 
		then '2. New - Appt OK'
		when NA.LastAttendedApptDate IS NULL
		AND NA.WeekWaitToAppt > 9 
		then '3. New - Appt BREACH (10+)'
		--DNA / Cancel
		when
		LastAttendedApptStatus IN ('Did Not Attend','Cancelled')
		and NextApptDate IS NULL 
		then '4. DNA/Cancel - No Appt'
		when LastAttendedApptStatus IN ('Did Not Attend','Cancelled')
		and NA.WeekWaitToAppt <= 9 
		then '5. DNA/Cancel - Appt OK'
		when LastAttendedApptStatus IN ('Did Not Attend','Cancelled')
		and NA.WeekWaitToAppt > 9 
		then '6. DNA/Cancel - Appt BREACH (10+)'
		-- Follow UP
		when NA.LastAttendedApptDate IS NOT NULL 
		AND NextApptDate IS NULL 	
		then '7. Follow Up - No Appt'
		when NA.LastAttendedApptDate IS NOT NULL 
		AND NA.WeekWaitToAppt <= 17
		then '8. Follow Up - Appt OK' 
		when NA.LastAttendedApptDate IS NOT NULL 
		AND NA.WeekWaitToAppt > 17
		then '9. Follow UP - Appt BREACH (18+)' 
		else 'Uncategorised'
		end                                                            as [Status]
		,1                                                             as [Total]
Into #FinalNonadmitted
From [RTT_DW].[PTL].[NonAdmitted] NA left join
      #specdiv Spec on Spec.SpecialtyCode = NA.SpecialtyCode
  
  
/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/
/*Admitted- Extracts Admitted patient 18wk wait data*/


Select
       CAST(ADM.CensusDate as Date)                                       as [CensusDate]	 
    ,'Admitted'                                                           as [PathwayType] 
    ,ADM.PatientID                                                        as [PatientID]	
    ,ADM.RTTWaitNowWeeksAdj                                               as [Current18WWait]
    ,case 
		when ADM.RTTWaitNowWeeksUnAdj > 17 then '18+'
		when ADM.RTTWaitNowWeeksUnAdj < 0 then '0'
		else cast(ADM.RTTWaitNowWeeksUnAdj as varchar(3))
		end                                                               as [Current18WWaitGrp]
    ,isnull(Spec.Division,'Scheduled Care')		                           as Division
    ,isnull(Spec.Direcorate,'Womens & Childrens')	                       as Directorate
    ,ADM.SpecialtyCode
    ,ADM.SpecialtyDescription                                             as [Specialty]
    ,ADM.Consultant                                                       as [Consultant]
    ,CASE 
		 WHEN ADM.TCIDate IS Null 
		     or (ADM.TCIDate < cast(adm.CensusDate as date)
             and  ADM.RTTWaitNowWeeksUnAdj <= 17)
		 THEN 'Amber'
		 WHEN ADM.RTTWaitToTCIDaysAdj > 126
		 or (ADM.TCIDate < cast(adm.CensusDate as date)
             and  ADM.RTTWaitNowWeeksUnAdj > 17)
		 THEN 'Red'
		 WHEN ADM.DiagnosticProcedure = 'Y'
		 AND  ADM.TCIDate >= DiagnosticTarget
		 THEN 'Red'
		 ELSE 'Green'
		 END                                     as Colour 

    ,CASE
        WHEN ADM.TCIDate < cast(adm.CensusDate as date)
             and  ADM.RTTWaitNowWeeksUnAdj <= 17 
        THEN '16. Past TCI – Current Wait <18' 
        WHEN ADM.TCIDate < cast(adm.CensusDate as date)
             and  ADM.RTTWaitNowWeeksUnAdj > 17 
        THEN '17. Past TCI – Current Wait 18+'
		WHEN	ADM.DiagnosticProcedure = 'Y'
		AND		ADM.TCIDate IS Null
		THEN	'10. Diagnostic - No TCI'
		WHEN	ADM.DiagnosticProcedure = 'Y'
		AND		(
				ADM.RTTWaitToTCIDaysAdj <= 126
				AND
				ADM.TCIDate < DiagnosticTarget
				)
		THEN	'11. Diagnostic - OK'
		WHEN	ADM.DiagnosticProcedure = 'Y'
		AND		(
				ADM.RTTWaitToTCIDaysAdj > 126
				OR
				ADM.TCIDate >= DiagnosticTarget
				)
		THEN	'12. Diagnostic - BREACH'		

		--Therapeutic
		WHEN	ADM.DiagnosticProcedure = 'N'
		AND     ADM.TCIDate IS Null
		THEN    '13. Therapeutic - No TCI'
		WHEN	ADM.DiagnosticProcedure = 'N'
		AND     ADM.RTTWaitToTCIDaysAdj <= 126
		THEN    '14. Therapeutic - TCI OK'
		WHEN	ADM.DiagnosticProcedure = 'N'
		AND     ADM.RTTWaitToTCIDaysAdj > 126
		THEN	'15. Therapeutic - TCI BREACH'
		ELSE	'Uncategorised'
		END                                                                as [Status]	
		,1                                                                 as [Total]

Into #Finaladmitted
FROM [RTT_DW].[PTL].[Admitted]  ADM left join
      #specdiv Spec on Spec.SpecialtyCode =  ADM.SpecialtyCode

/*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
Select 
	*
into #results
From
(	
	
	Select 
	*
	From #FinalNonadmitted
	union all
	Select 
	*
	From #Finaladmitted
) x 
Where 
	x.Division        IN (SELECT Item FROM WHREPORTING.dbo.Split (@Division , ','))
	and x.Directorate IN (SELECT Item  FROM WHREPORTING.dbo.Split (@Directorate, ','))
	and x.[Specialty] IN (SELECT Item  FROM WHREPORTING.dbo.Split (@Specialty, ','))
	and (
	@conrep = 0 
	or (@conrep = 1  and x.[Consultant] IN (SELECT Item FROM WHREPORTING.dbo.Split (@Consultant, ',')))
	) 
order by 
	 PathwayType desc
	 ,[Status]
	 ,SpecialtyCode

/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/	 
insert into #results
Select	distinct 
	r.CensusDate
	,r.PathwayType
	,null                                as PatientID
	,r. [Current18WWait]
	,w.[Weekno]                          as [Current18WWaitGrp]
	,r.Division	
	,r.Directorate
	,r.SpecialtyCode
	,r.Specialty
	,r.Consultant
	,r.Colour
	,r.[Status]
	,0                                   as Total
From #weeknos w inner join #results r on 1 = 1 
/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/
Select * from #results 
/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/

drop table  #specdiv, #FinalNonadmitted,#Finaladmitted ,#weeknos, #results