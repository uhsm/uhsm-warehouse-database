﻿/*
--------------------------------------------------------------------------------------------------------------------------
Purpose:		PTL 18wk Integrated Tool - data for clinic parameter dropdown.
			
Notes:			Stored in WHREPORTING.PTL

				This Procedure has been created purely for use in the PTL SSRS report in the location below
				S:\CO-HI-Information\New File Management - Jan 2011\Data Warehouse Project\SSRS Report Code\PTLs_Waiting_List_RTT\PTLReports
				This has been designed to drive the drop down menus and the results of the following Procedure(s) 
				cascade their respectives results to this procedure
				[PTL].[PTL_18wk_Intergrated_tool_Division]
				[PTL].[PTL_18wk_Intergrated_tool_Directorate]
				[PTL].[PTL_18wk_Intergrated_tool_Specialty]
				[PTL].[PTL_18wk_Intergrated_tool_Consultant] 

Versions:
				1.0.0.1 - 30/03/2016 - MT
					Don't need select distinct records from LK.SpecialtyDivision in a CTE
					at SpecialtyCode level because SpecialtyCode is already the lowest level.

				1.0.0.0 - 30/03/2016 - Peter Asemota
					Created sproc.
--------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [PTL].[PTL_18wk_Intergrated_tool_Clinic]
	@Division varchar(max)
	,@Directorate varchar(max)
	,@Specialty varchar(max)
	,@Consultant varchar(max)
As

Select Distinct ClinicCode As Clinic

From	[RTT_DW].[PTL].[NonAdmitted] src

		Inner Join LK.SpecialtyDivision sd
			On src.SpecialtyCode = sd.SpecialtyCode

		Inner Join DW_REPORTING.LIB.fn_SplitString(@Division,',') div
			On sd.Division = div.SplitValue

		Inner Join DW_REPORTING.LIB.fn_SplitString(@Directorate,',') dir
			On sd.Direcorate = dir.SplitValue

		Inner Join DW_REPORTING.LIB.fn_SplitString(@Specialty,',') sp
			On src.Specialty = sp.SplitValue

		Inner Join DW_REPORTING.LIB.fn_SplitString(@Consultant,',') con
			On src.DoctorSurname = con.SplitValue

Union All

Select 'No Aptt Booked' As Clinic -- to cater for admitted PTL records which do not have a clinic (inpatient)

Order By Clinic