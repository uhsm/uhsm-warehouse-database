﻿CREATE Procedure [PTL].[PTL_18wk_Intergrated_tool_Consultant]
(
@Division  nvarchar(max) 
,@Directorate nvarchar(max) 
,@Specialty nvarchar(max) 
)
as 


/*
This Procedure has been created purely for use in the PTL SSRS report in the location below
S:\CO-HI-Information\New File Management - Jan 2011\Data Warehouse Project\SSRS Report Code\PTLs_Waiting_List_RTT\PTLReports
This has been designed to drive the drop down menus and the results of the following Procedure(s) 
cascade their respectives results to this procedure
[PTL].[PTL_18wk_Intergrated_tool_Division]
[PTL].[PTL_18wk_Intergrated_tool_Directorate]
[PTL].[PTL_18wk_Intergrated_tool_Specialty]
*/


/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/
/*Specialty/division temp table*/
SELECT Distinct
      [SpecialtyCode]
      ,[Specialty]
      ,[Direcorate]
      ,[Division]
 into #specdiv
  FROM [WHREPORTING].[LK].[SpecialtyDivision]
  where 
  Direcorate <> 'Unknown'
/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/
/*Non Admitted*/

Select distinct
	Consultant
From
(
Select
DoctorSurname                                                         as Consultant
From [RTT_DW].[PTL].[NonAdmitted] NA left join
      #specdiv Spec on Spec.SpecialtyCode = NA.SpecialtyCode
Where 
  Spec.[Division]  IN (SELECT Item FROM WHREPORTING.dbo.Split (@Division , ','))
  and Spec.Direcorate IN (SELECT Item FROM WHREPORTING.dbo.Split (@Directorate , ','))
  and NA.Specialty IN (SELECT Item FROM WHREPORTING.dbo.Split (@Specialty, ','))
 union all 
  
/*Admitted*/
Select  
    ADM.Consultant                                                       as [Consultant]
FROM [RTT_DW].[PTL].[Admitted]  ADM left join
      #specdiv Spec on Spec.SpecialtyCode =  ADM.SpecialtyCode
Where 
  Spec.[Division] IN (SELECT Item FROM WHREPORTING.dbo.Split (@Division , ','))
  and Spec.Direcorate IN (SELECT Item FROM WHREPORTING.dbo.Split (@Directorate , ','))
  and ADM.SpecialtyDescription IN (SELECT Item FROM WHREPORTING.dbo.Split (@Specialty, ','))

) x
order by Consultant asc
/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/

Drop table #specdiv