﻿CREATE Procedure [PTL].[PTL_18wk_Intergrated_tool_Directorate]
(
@Division  nvarchar(max)  
)
as 


/*
This Procedure has been created purely for use in the PTL SSRS report in the location below
S:\CO-HI-Information\New File Management - Jan 2011\Data Warehouse Project\SSRS Report Code\PTLs_Waiting_List_RTT\PTLReports
This has been designed to drive the drop down menus and the results the following Procedure(s) 
cascade the respectives results to this procedure
[PTL].[PTL_18wk_Intergrated_tool_Division]
*/

/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/
/*Specialty/division temp table*/
SELECT Distinct
      [SpecialtyCode]
      ,[Specialty]
      ,[Direcorate]
      ,[Division]
into #specdiv
  FROM [WHREPORTING].[LK].[SpecialtyDivision]
  where 
  Direcorate <> 'Unknown'
/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/
/*Non Admitted*/

Select distinct
	Directorate
From
(
Select
isnull(Spec.Direcorate,'Womens & Childrens')                  as Directorate
From [RTT_DW].[PTL].[NonAdmitted] NA left join
      #specdiv Spec on Spec.SpecialtyCode = NA.SpecialtyCode
Where 
  Spec.[Division]  IN (SELECT Item FROM WHREPORTING.dbo.Split (@Division , ','))
 union all 
  
/*Admitted*/
Select  
isnull(Spec.Direcorate,'Womens & Childrens')                   as Directorate
FROM [RTT_DW].[PTL].[Admitted]  ADM left join
      #specdiv Spec on Spec.SpecialtyCode =  ADM.SpecialtyCode
Where 
  Spec.[Division] IN (SELECT Item FROM WHREPORTING.dbo.Split (@Division , ','))
) x
order by Directorate asc

/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/

Drop table #specdiv