﻿CREATE Procedure [PTL].[PTL_18wk_Intergrated_tool_Division]

as

/*
This Procedure has been created purely for use in the PTL SSRS report in the location below
S:\CO-HI-Information\New File Management - Jan 2011\Data Warehouse Project\SSRS Report Code\PTLs_Waiting_List_RTT\PTLReports
*/

/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/
/*Specialty/division temp table*/
SELECT Distinct
      [SpecialtyCode]
      ,[Specialty]
      ,[Direcorate]
      ,[Division]
 into #specdiv
  FROM [WHREPORTING].[LK].[SpecialtyDivision]
  where 
  Direcorate <> 'Unknown'
/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/
/*Non Admitted*/

Select distinct
	Division
From
(
Select
    isnull(Spec.Division,'Scheduled Care')		                          as Division
From [RTT_DW].[PTL].[NonAdmitted] NA left join
      #specdiv Spec on Spec.SpecialtyCode = NA.SpecialtyCode
  
 union all 
  
/*Admitted*/
Select
    isnull(Spec.Division,'Scheduled Care')		                          as Division
FROM [RTT_DW].[PTL].[Admitted]  ADM left join
      #specdiv Spec on Spec.SpecialtyCode =  ADM.SpecialtyCode
) x
order by Division asc
/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/

Drop table #specdiv