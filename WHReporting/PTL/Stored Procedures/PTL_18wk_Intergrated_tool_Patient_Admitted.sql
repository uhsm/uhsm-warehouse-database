﻿CREATE Procedure [PTL].[PTL_18wk_Intergrated_tool_Patient_Admitted]
(
@Division  nvarchar(255)  
,@Directorate nvarchar(255)
,@Specialty nvarchar(255) 
,@Consultant nvarchar(255) 
,@conrep int
,@PTL18wk nvarchar(255) 
,@Status nvarchar(255) 

)
as 

/*\
Declare	@Division  nvarchar(255) = 'Scheduled Care'    
Declare @Directorate nvarchar(255) = 'Surgery'
Declare @Specialty nvarchar(255) = 'General Surgery' --'General Surgery'
Declare @Consultant nvarchar(255) =  'all'
Declare @conrep int = '0'
Declare @PTL18wk nvarchar(255) = 'all'
Declare @Status nvarchar(255) = '17. Past TCI – Current Wait 18+'
*/

/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/
/*Specialty/division temp table*/
SELECT Distinct
      [SpecialtyCode]
      ,[Specialty]
      ,[Direcorate]
      ,[Division]
 into #specdiv
  FROM [WHREPORTING].[LK].[SpecialtyDivision]
  where 
  Direcorate <> 'Unknown'
/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/

SELECT 
	CAST(ADM.CensusDate as Date)                  as [CensusDate]
	,'Admitted'                                   as [PathwayType] 
	,adm.PatientClass      
	,adm.[PatientID]       
	,ADM.PPID                                     as [RefUniqueID]
	,ADM.PatientName                              as [PatientSurname] 
	,adm.Age
	,adm.Sex
	,CAST(ADM.ClockStartDate as DATE)             as [ClockStart]   
	,adm.ClockStartType
	,CAST(ADM.RTTBreachDate  as DATE)             as [BreachDate]   
	,ADM.RTTWaitNowWeeksAdj                       as [Current18WWait] 
	,case                                         
        when ADM.RTTWaitNowWeeksUnAdj > 17 then '18+'
        when ADM.RTTWaitNowWeeksUnAdj < 0 then '0'
        else cast(ADM.RTTWaitNowWeeksUnAdj as varchar(3))
        end                                       as [Current18WWaitGrp]  
	,ADM.PTLComments                              as [CurrentStatus] 
	,adm.LatestRTTCode
	,CAST(ADM.TCIDate as date)                    as [TCI]       
	,CAST(ADM.DateOnList as date)                 as [LastApptDate]
	,cast(RTTWaitToTCIDaysUnAdj/7 as varchar(4))  as [Wait at TCI] 
	,adm.[DateTCIOffered]          
	,adm.[OperationDate]            
	,adm.[PreopDate]                     
             
	,case                                                  
        when RTTWaitToTCIDaysUnAdj/7 > 17 then '18+'
        else cast(RTTWaitToTCIDaysUnAdj/7 as varchar(4))
        end                                      as [Wait Group at TCI] 
	,cast(RTTWaitToTCIDaysAdj/7 as varchar(3))   as [Adjusted Wait at TCI]      
	,case 
		when RTTWaitToTCIDaysAdj/7 > 17 then '18+'
		else cast(RTTWaitToTCIDaysAdj/7 as varchar(4))
		end                                      as [Adjusted Wait Group at TCI] 
  	,adm.[AdmissionWard]         
	,adm.[SecondaryWard]           
	,adm.[PlannedProcedure1]   
	,adm.[ReferralPriority]        
	,adm.[Priority2]                          
	,ADM.RefCompComment                          as Comments
	,ADM.GeneralComment                          as [PathwayDetails]
	,adm.[EstTheatreTime]     
	,adm.[ConsultantCode]    
    ,isnull(Spec.Division,'Scheduled Care')		  as Division
    ,isnull(Spec.Direcorate,'Womens & Childrens') as Directorate
	,ADM.SpecialtyCode
	,ADM.SpecialtyDescription                    as [Specialty]           
	,ADM.Consultant                              as [Consultant]      
	,ReferringCCG                                as [CommissionerCode]   
    ,CASE 
		 WHEN ADM.TCIDate IS Null 
		     or (ADM.TCIDate < adm.CensusDate
             and  ADM.RTTWaitNowWeeksUnAdj <= 17)
		 THEN 'Amber'
		 WHEN ADM.RTTWaitToTCIDaysAdj > 126
		 or (ADM.TCIDate < adm.CensusDate
             and  ADM.RTTWaitNowWeeksUnAdj > 17)
		 THEN 'Red'
		 WHEN ADM.DiagnosticProcedure = 'Y'
		 AND  ADM.TCIDate >= DiagnosticTarget
		 THEN 'Red'
		 ELSE 'Green'
		 END                                     as Colour 

     ,CASE
        WHEN ADM.TCIDate < cast(adm.CensusDate as date)
             and  ADM.RTTWaitNowWeeksUnAdj <= 17 
        THEN '16. Past TCI – Current Wait <18' 
        WHEN ADM.TCIDate < cast(adm.CensusDate as date)
             and  ADM.RTTWaitNowWeeksUnAdj > 17 
        THEN '17. Past TCI – Current Wait 18+'
		WHEN	ADM.DiagnosticProcedure = 'Y'
		AND		ADM.TCIDate IS Null
		THEN	'10. Diagnostic - No TCI'
		WHEN	ADM.DiagnosticProcedure = 'Y'
		AND		(
				ADM.RTTWaitToTCIDaysAdj <= 126
				AND
				ADM.TCIDate < DiagnosticTarget
				)
		THEN	'11. Diagnostic - OK'
		WHEN	ADM.DiagnosticProcedure = 'Y'
		AND		(
				ADM.RTTWaitToTCIDaysAdj > 126
				OR
				ADM.TCIDate >= DiagnosticTarget
				)
		THEN	'12. Diagnostic - BREACH'		

		--Therapeutic
		WHEN	ADM.DiagnosticProcedure = 'N'
		AND     ADM.TCIDate IS Null
		THEN    '13. Therapeutic - No TCI'
		WHEN	ADM.DiagnosticProcedure = 'N'
		AND     ADM.RTTWaitToTCIDaysAdj <= 126
		THEN    '14. Therapeutic - TCI OK'
		WHEN	ADM.DiagnosticProcedure = 'N'
		AND     ADM.RTTWaitToTCIDaysAdj > 126
		THEN	'15. Therapeutic - TCI BREACH'
		ELSE	'Uncategorised'
		END                                                                as [Status]	
   into #results 
   
    FROM [RTT_DW].[PTL].[Admitted] adm Left Join
     #specdiv Spec on Spec.SpecialtyCode =  ADM.SpecialtyCode
     
      Where 
	(@Division = 'all' or Spec.Division IN (SELECT Item FROM WHREPORTING.dbo.Split (@Division , ',')))
	and 
	(@Directorate = 'all' or Spec.Direcorate IN (SELECT Item  FROM WHREPORTING.dbo.Split (@Directorate, ',')))
	and 
	(@Specialty = 'all' or adm.SpecialtyDescription IN (SELECT Item  FROM WHREPORTING.dbo.Split (@Specialty, ',')))
		and (
	@conrep = 0
	or (@conrep = 1  and @Consultant = 'all')
	or (@conrep = 1  and adm.Consultant IN (SELECT Item FROM WHREPORTING.dbo.Split (@Consultant, ',')))
	)
/*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
Select 
	*
From #results 
where 
  (
   @Status = 'all'
   or
   [Status] = @Status
	)
	and 
	(
	@PTL18wk = 'all'
	or
	[Current18WWaitGrp] = @PTL18wk 
	)
	 
/*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
drop table #specdiv, #results