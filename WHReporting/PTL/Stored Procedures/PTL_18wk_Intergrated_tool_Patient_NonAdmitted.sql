﻿CREATE Procedure [PTL].[PTL_18wk_Intergrated_tool_Patient_NonAdmitted]
(
@Division  nvarchar(255)  
,@Directorate nvarchar(255)
,@Specialty nvarchar(255) 
,@Consultant nvarchar(255) 
,@conrep int
,@PTL18wk nvarchar(255)  
,@Status nvarchar(255) 
)
as 

/*
Declare	@Division  nvarchar(255) = 'Scheduled Care'    
Declare @Directorate nvarchar(255) = 'Surgery'
Declare @Specialty nvarchar(255) = 'General Surgery' --'General Surgery'
Declare @Consultant nvarchar(255) =  'all'
Declare @conrep int = '0'
Declare @PTL18wk nvarchar(255) = 'all'
Declare @Status nvarchar(255) = '9. Follow UP - Appt BREACH (18+)' --'1. New - No Appt'
*/
/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/
/*Specialty/division temp table*/
SELECT Distinct
      [SpecialtyCode]
      ,[Specialty]
      ,[Direcorate]
      ,[Division]
 into #specdiv
  FROM [WHREPORTING].[LK].[SpecialtyDivision]
  where 
  Direcorate <> 'Unknown'
/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/

SELECT 
      adm.[Specialty]
      ,adm.[SpecialtyCode]
      ,adm.[DoctorSurname]
      ,adm.[FacilityID]+' '+adm.[PatientID] as PatientID
      --,adm.[FacilityID]
      ,adm.[PatientSurname]
      ,adm.[ReferralDate]
      ,adm.[RTTBreachDate]
      ,adm.[LastAttendedApptStatus]
      ,adm.[LastAttendedApptDate]
      ,adm.[NextApptDate]
      ,adm.[RTTWeekWaitActual]
      ,adm.[WeekWaitToAppt]
      ,adm.[FirstAdmissionOutcome]
      ,adm.[RefCompComment]
      ,adm.[PathwayDelays]
      ,adm.[RefUniqueID]
      ,adm.[CommissioningCCG]
      ,adm.[AdmissionWard]
      ,adm.[ClinicCode]
      ,adm.[CensusDate]
    ,isnull(Spec.Division,'Scheduled Care')		                           as Division
    ,isnull(Spec.Direcorate,'Womens & Childrens')	                       as Directorate
      ,case 
		--News
		when 
		adm.LastAttendedApptDate IS NULL 
		AND NextApptDate IS NULL 
		then '1. New - No Appt'
		when 
		adm.LastAttendedApptDate IS NULL
		AND adm.WeekWaitToAppt <= 9 
		then '2. New - Appt OK'
		when adm.LastAttendedApptDate IS NULL
		AND adm.WeekWaitToAppt > 9 
		then '3. New - Appt BREACH (10+)'
		--DNA / Cancel
		when
		LastAttendedApptStatus IN ('Did Not Attend','Cancelled')
		and NextApptDate IS NULL 
		then '4. DNA/Cancel - No Appt'
		when LastAttendedApptStatus IN ('Did Not Attend','Cancelled')
		and adm.WeekWaitToAppt <= 9 
		then '5. DNA/Cancel - Appt OK'
		when LastAttendedApptStatus IN ('Did Not Attend','Cancelled')
		and adm.WeekWaitToAppt > 9 
		then '6. DNA/Cancel - Appt BREACH (10+)'
		-- Follow UP
		when adm.LastAttendedApptDate IS NOT NULL 
		AND NextApptDate IS NULL 	
		then '7. Follow Up - No Appt'
		when adm.LastAttendedApptDate IS NOT NULL 
		AND adm.WeekWaitToAppt <= 17
		then '8. Follow Up - Appt OK' 
		when adm.LastAttendedApptDate IS NOT NULL 
		AND adm.WeekWaitToAppt > 17
		then '9. Follow UP - Appt BREACH (18+)' 
		else 'Uncategorised'
		end                                                            as [Status]
    ,case 
		when adm.RTTWeekWaitActual > 17 then '18+'
		when adm.RTTWeekWaitActual < 0 then '0'
		else cast(adm.RTTWeekWaitActual as varchar(3))
		end                                                                as Current18WWaitGrp
   into #results 
  FROM [RTT_DW].[PTL].[NonAdmitted] adm left join
      #specdiv Spec on Spec.SpecialtyCode = adm .SpecialtyCode
  Where 	(@Division = 'all' or Spec.Division IN (SELECT Item FROM WHREPORTING.dbo.Split (@Division , ',')))
	and 
	(@Directorate = 'all' or Spec.Direcorate IN (SELECT Item  FROM WHREPORTING.dbo.Split (@Directorate, ',')))
	and 
	(@Specialty = 'all' or adm.Specialty IN (SELECT Item  FROM WHREPORTING.dbo.Split (@Specialty, ',')))
		and (
	@conrep = 0 
	or (@conrep = 1  and @Consultant = 'all')
	or (@conrep = 1  and adm.[DoctorSurname] IN (SELECT Item FROM WHREPORTING.dbo.Split (@Consultant, ',')))
	)
	
/*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
Select 
	*
From #results 
where 
  (
  @Status = 'all'
   or
   [Status] = @Status
	)
	and 
	(
	@PTL18wk = 'all'
	or
	[Current18WWaitGrp] = @PTL18wk 
	)

/*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
drop table #specdiv,#results