﻿/*
---------------------------------------------------------------------------------------------------------------
   --------------------------------
Purpose  : PTL 18wk Integrated Tool - data for report

Notes:    stored in WHREPORTING.PTL
          This procedure has been created purely for the use in the PTL SSRS report in the location below.
          S:\CO-HI-Information\New File Management - Jan 2011\Data Warehouse Project\SSRS Report Code\PTLs_Waiting_List_RTT\PTLReports


Version  : 1.0.0.0 - 30/03/2016 - Peter Asemota
          Don't need distinct records from LK.specialtyDivision in a temp table at specialtyCode level
          Took out the #weekno as this is not needed.
         

*/
CREATE PROCEDURE [PTL].[PTL_18wk_Intergrated_tool_v2] (
	@Division NVARCHAR(max)
	,@Directorate NVARCHAR(max)
	,@Specialty NVARCHAR(max)
	,@Consultant NVARCHAR(max)
	,@Clinic NVARCHAR(max) -- added by PA on 30/03/2016 on Corinne Fisher's request
	,@conrep INT /*This parameters determines whether the SP is servicing the overall of consulant level report 
                                          0 = Overall and 1 = Consultant*/
	)
AS
--/*
--Declare @Division  nvarchar(255) = 'Scheduled Care'    
--Declare @Directorate nvarchar(255) = 'Surgery'
--Declare @Specialty nvarchar(255) = 'General Surgery' --'General Surgery'
--Declare @Consultant nvarchar(255) =  'Baraza'
--Declare @Clinic varchar(255) = 'WB02P2'
--Declare @conrep int = '0'
----*/
	;

WITH NonAdmitted
AS (
	SELECT CAST(NA.CensusDate AS DATE) AS CensusDate
		,'Non-Admitted' AS PathwayType
		,NA.FacilityID + ' ' + NA.PatientID AS PatientID
		,NA.RTTWeekWaitActual AS Current18WWait
		,CASE 
			WHEN NA.RTTWeekWaitActual > 17
				THEN '18+'
			WHEN NA.RTTWeekWaitActual < 0
				THEN '0'
			ELSE cast(NA.RTTWeekWaitActual AS VARCHAR(3))
			END AS Current18WWaitGrp
		,isnull(Spec.Division, 'Scheduled Care') AS Division
		,isnull(Spec.Direcorate, 'Womens & Childrens') AS Directorate
		,NA.SpecialtyCode
		,NA.Specialty
		,DoctorSurname AS Consultant
		,ISNULL(NA.ClinicCode, 'No Aptt Booked') AS Clinic -- added by PA 30/03/2016
		,CASE 
			WHEN NextApptDate IS NULL
				THEN 'Amber'
			WHEN (
					NA.LastAttendedApptDate IS NULL
					OR LastAttendedApptStatus IN (
						'Did Not Attend'
						,'Cancelled'
						)
					)
				AND NA.WeekWaitToAppt > 9
				THEN 'Red'
			WHEN NA.WeekWaitToAppt > 17
				THEN 'Red'
			WHEN NA.WeekWaitToAppt < 18
				THEN 'Green'
			ELSE 'Uncategorised'
			END AS [Colour]
		,CASE 
			--News
			WHEN NA.LastAttendedApptDate IS NULL
				AND NextApptDate IS NULL
				THEN '1. New - No Appt'
			WHEN NA.LastAttendedApptDate IS NULL
				AND NA.WeekWaitToAppt <= 9
				THEN '2. New - Appt OK'
			WHEN NA.LastAttendedApptDate IS NULL
				AND NA.WeekWaitToAppt > 9
				THEN '3. New - Appt BREACH (10+)'
					--DNA / Cancel
			WHEN LastAttendedApptStatus IN (
					'Did Not Attend'
					,'Cancelled'
					)
				AND NextApptDate IS NULL
				THEN '4. DNA/Cancel - No Appt'
			WHEN LastAttendedApptStatus IN (
					'Did Not Attend'
					,'Cancelled'
					)
				AND NA.WeekWaitToAppt <= 9
				THEN '5. DNA/Cancel - Appt OK'
			WHEN LastAttendedApptStatus IN (
					'Did Not Attend'
					,'Cancelled'
					)
				AND NA.WeekWaitToAppt > 9
				THEN '6. DNA/Cancel - Appt BREACH (10+)'
					-- Follow UP
			WHEN NA.LastAttendedApptDate IS NOT NULL
				AND NextApptDate IS NULL
				THEN '7. Follow Up - No Appt'
			WHEN NA.LastAttendedApptDate IS NOT NULL
				AND NA.WeekWaitToAppt <= 17
				THEN '8. Follow Up - Appt OK'
			WHEN NA.LastAttendedApptDate IS NOT NULL
				AND NA.WeekWaitToAppt > 17
				THEN '9. Follow UP - Appt BREACH (18+)'
			ELSE 'Uncategorised'
			END AS [Status]
		,1 AS [Total]
	--Into #FinalNonadmitted
	FROM [RTT_DW].[PTL].[NonAdmitted] NA
	INNER JOIN [WHREPORTING].[LK].[SpecialtyDivision] Spec ON Spec.SpecialtyCode = NA.SpecialtyCode
	INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Division, ',') DIV ON Spec.Division = DIV.SplitValue
	INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Directorate, ',') DIR ON Spec.Direcorate = DIR.SplitValue
	INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Specialty, ',') SP ON NA.Specialty = SP.SplitValue
	INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Consultant, ',') CON ON NA.DoctorSurname = CON.SplitValue
	)
	/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/
	/*Admitted- Extracts Admitted patient 18wk wait data*/
	,Admitted
AS (
	SELECT CAST(ADM.CensusDate AS DATE) AS [CensusDate]
		,'Admitted' AS [PathwayType]
		,ADM.PatientID AS [PatientID]
		,ADM.RTTWaitNowWeeksAdj AS [Current18WWait]
		,CASE 
			WHEN ADM.RTTWaitNowWeeksUnAdj > 17
				THEN '18+'
			WHEN ADM.RTTWaitNowWeeksUnAdj < 0
				THEN '0'
			ELSE cast(ADM.RTTWaitNowWeeksUnAdj AS VARCHAR(3))
			END AS [Current18WWaitGrp]
		,isnull(Spec.Division, 'Scheduled Care') AS Division
		,isnull(Spec.Direcorate, 'Womens & Childrens') AS Directorate
		,ADM.SpecialtyCode
		,ADM.SpecialtyDescription AS [Specialty]
		,ADM.Consultant AS [Consultant]
		,'No Aptt Booked' AS Clinic
		,CASE 
			WHEN ADM.TCIDate IS NULL
				OR (
					ADM.TCIDate < cast(adm.CensusDate AS DATE)
					AND ADM.RTTWaitNowWeeksUnAdj <= 17
					)
				THEN 'Amber'
			WHEN ADM.RTTWaitToTCIDaysAdj > 126
				OR (
					ADM.TCIDate < cast(adm.CensusDate AS DATE)
					AND ADM.RTTWaitNowWeeksUnAdj > 17
					)
				THEN 'Red'
			WHEN ADM.DiagnosticProcedure = 'Y'
				AND ADM.TCIDate >= DiagnosticTarget
				THEN 'Red'
			ELSE 'Green'
			END AS Colour
		,CASE 
			WHEN ADM.TCIDate < cast(adm.CensusDate AS DATE)
				AND ADM.RTTWaitNowWeeksUnAdj <= 17
				THEN '16. Past TCI – Current Wait <18'
			WHEN ADM.TCIDate < cast(adm.CensusDate AS DATE)
				AND ADM.RTTWaitNowWeeksUnAdj > 17
				THEN '17. Past TCI – Current Wait 18+'
			WHEN ADM.DiagnosticProcedure = 'Y'
				AND ADM.TCIDate IS NULL
				THEN '10. Diagnostic - No TCI'
			WHEN ADM.DiagnosticProcedure = 'Y'
				AND (
					ADM.RTTWaitToTCIDaysAdj <= 126
					AND ADM.TCIDate < DiagnosticTarget
					)
				THEN '11. Diagnostic - OK'
			WHEN ADM.DiagnosticProcedure = 'Y'
				AND (
					ADM.RTTWaitToTCIDaysAdj > 126
					OR ADM.TCIDate >= DiagnosticTarget
					)
				THEN '12. Diagnostic - BREACH'
					--Therapeutic
			WHEN ADM.DiagnosticProcedure = 'N'
				AND ADM.TCIDate IS NULL
				THEN '13. Therapeutic - No TCI'
			WHEN ADM.DiagnosticProcedure = 'N'
				AND ADM.RTTWaitToTCIDaysAdj <= 126
				THEN '14. Therapeutic - TCI OK'
			WHEN ADM.DiagnosticProcedure = 'N'
				AND ADM.RTTWaitToTCIDaysAdj > 126
				THEN '15. Therapeutic - TCI BREACH'
			ELSE 'Uncategorised'
			END AS [Status]
		,1 AS [Total]
	--Into #Finaladmitted
	FROM [RTT_DW].[PTL].[Admitted] ADM
	INNER JOIN [WHREPORTING].[LK].[SpecialtyDivision] Spec ON Spec.SpecialtyCode = ADM.SpecialtyCode
	INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Division, ',') DIV ON Spec.Division = DIV.SplitValue
	INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Directorate, ',') DIR ON Spec.Direcorate = DIR.SplitValue
	INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Specialty, ',') SP ON ADM.SpecialtyDescription = SP.SplitValue
	INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Consultant, ',') CON ON ADM.Consultant = CON.SplitValue
	)
/*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
--Retrieving both admitted and nonadmitted data
SELECT *
FROM NonAdmitted
WHERE Clinic IN (
		SELECT Item
		FROM dbo.Split(@Clinic, ',')
		)

UNION ALL

SELECT *
FROM Admitted
ORDER BY PathwayType DESC
	,[Status]
	,SpecialtyCode