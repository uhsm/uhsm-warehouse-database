﻿/******************************************************************************
 Description: AHP RTT SSRS Report
  
 Parameters:  @Specialty - Specialties required on the report
              
 Modification History --------------------------------------------------------

 Date     Author      Change
 23/04/14 Tim Dean	  Created

******************************************************************************/
CREATE PROCEDURE [PTL].[ReportAHPRTT]
 @Specialty AS NVARCHAR(2000)
as

SET NOCOUNT ON;

-- To improve query performance create tables from the parameters
-- SSRS passes the parameters a CSV strings

IF Object_id('tempdb..#Specialty', 'U') IS NOT NULL
  DROP TABLE #Specialty;

SELECT Item
INTO   #Specialty
FROM   WHREPORTING.dbo.Splitstrings_cte(@Specialty, N',');

--===================================================================

SELECT
  Referral.ReferralSourceUniqueID
 ,ReferralPatientNumber = Referral.FacilityID
 ,ReferralEncounter.PatientSurname
 ,ReferralEncounter.PatientForename 
 ,Referral.ReferralReceivedDate
 ,ReferralReceivingSpecialty = Referral.[ReceivingSpecialty(Function)]
 ,LastAppointmentDate = COALESCE(CONVERT(VARCHAR(10),(
                                                      SELECT TOP 1 PrevAppLkp.AppointmentDate
                                                      FROM WHREPORTING.OP.Schedule PrevAppLkp
                                                      WHERE PrevAppLkp.SpecialtyCode = Referral.ReceivingSpecialtyCode
                                                            AND PrevAppLkp.ReferralSourceUniqueID = Referral.ReferralSourceUniqueID
                                                            AND PrevAppLkp.AppointmentDateTime < GETDATE()
                                                      ORDER BY PrevAppLkp.AppointmentDateTime DESC
                                                      ),103),'')  
 ,LastAppointmentClinicCode = (
                               SELECT TOP 1 PrevClinicLkp.ClinicCode
                               FROM WHREPORTING.OP.Schedule PrevClinicLkp
                               WHERE PrevClinicLkp.SpecialtyCode = Referral.ReceivingSpecialtyCode
                                     AND PrevClinicLkp.ReferralSourceUniqueID = Referral.ReferralSourceUniqueID
                                     AND PrevClinicLkp.AppointmentDateTime < GETDATE()
                               ORDER BY PrevClinicLkp.AppointmentDateTime DESC
                              )                                                                        
 ,NextAppointmentDate = COALESCE(CONVERT(VARCHAR(10),(
                                                      SELECT TOP 1 NextAppLkp.AppointmentDate
                                                      FROM WHREPORTING.OP.Schedule NextAppLkp
                                                      WHERE NextAppLkp.SpecialtyCode = Referral.ReceivingSpecialtyCode
                                                            AND NextAppLkp.ReferralSourceUniqueID = Referral.ReferralSourceUniqueID
                                                            AND NextAppLkp.AppointmentDateTime >= GETDATE()
                                                      ORDER BY NextAppLkp.AppointmentDateTime ASC
                                                     ),103),'') 
 ,NextAppointmentClinicCode = (
                               SELECT TOP 1 NextClinicLkp.ClinicCode
                               FROM WHREPORTING.OP.Schedule NextClinicLkp
                               WHERE NextClinicLkp.SpecialtyCode = Referral.ReceivingSpecialtyCode
                                     AND NextClinicLkp.ReferralSourceUniqueID = Referral.ReferralSourceUniqueID
                                     AND NextClinicLkp.AppointmentDateTime >= GETDATE()
                               ORDER BY NextClinicLkp.AppointmentDateTime ASC
                              )   
 ,RTTBreachDate = CASE
                    WHEN ParentReferral.RTTStatusCode = '33' THEN DATEADD(WEEK,18,ParentReferral.StartDate)
                    WHEN ParentReferral.SourceUniqueID IS NOT NULL THEN DATEADD(WEEK,18,ParentReferral.ReferralDate)
                    WHEN LastRTTHistoryStatus.RTTStatusCode = '33' THEN DATEADD(WEEK,18,LastRTTHistory.StartDate)
                    ELSE DATEADD(WEEK,18,Referral.ReferralReceivedDate)
                  END     
 ,CurrentWaitWeeks = CASE                   
                       WHEN ParentReferral.RTTStatusCode = '33' THEN FLOOR(DATEDIFF(DAY,ParentReferral.StartDate,GETDATE()) / 7.00)
                       WHEN ParentReferral.SourceUniqueID IS NOT NULL THEN FLOOR(DATEDIFF(DAY,ParentReferral.ReferralDate,GETDATE()) / 7.00)
                       WHEN LastRTTHistoryStatus.RTTStatusCode = '33' THEN FLOOR(DATEDIFF(DAY,LastRTTHistory.StartDate,GETDATE()) / 7.00)
                       ELSE FLOOR(DATEDIFF(DAY,Referral.ReferralReceivedDate,GETDATE()) / 7.00)
                     END                                                                                                  
FROM (
      SELECT
        ReferralLkp.ReferralSourceUniqueID
      FROM WHREPORTING.RF.Referral ReferralLkp
      INNER JOIN #Specialty sp 
        ON ReferralLkp.ReceivingSpecialtyCode = sp.Item
      WHERE ReferralLkp.CompletionDate IS NULL  -- Referral has not been completed.
            AND ReferralLkp.CancelDate IS NULL -- Referral has not been cancelled.
            -- Only include referrals for the following treatment function codes
            --AND ReferralLkp.[ReceivingSpecialtyCode(Function)] IN ( '654' -- Dietetics.
            --                                                       ,'650' -- Physiotherapy.
            --                                                       ,'651' -- Occupational Therapy.
            --                                                       ,'653' -- Podiatry.
            --                                                       ,'652' -- Speech and Language Therapy  
            --                                                       ,'840') -- Audiology.
            -- Only include referrals that have not had specified RTT statuses before the end date.
            AND NOT EXISTS (
                            SELECT 1 
                            FROM WH.RTT.RTT ReferralRTTHistoryLookup 
                            LEFT JOIN WH.RTT.RTTStatus ReferralRTTHistoryLookupStatus 
                              ON (ReferralRTTHistoryLookup.RTTStatusCode = ReferralRTTHistoryLookupStatus.InternalCode)       
                            WHERE ReferralRTTHistoryLookup.ReferralSourceUniqueID =ReferralLkp.ReferralSourceUniqueID
                                  AND ReferralRTTHistoryLookup.StartDate < GETDATE()
                                  AND ReferralRTTHistoryLookupStatus.RTTStatusCode IN ('30','31','32','34','35','36','98')
                           ) 
            AND ReferralLkp.ReferralReceivedDate >= CONVERT(DATETIME,'01/04/2014',103)

     ) ReferralLookup
INNER JOIN WHREPORTING.RF.Referral
  ON (ReferralLookup.ReferralSourceUniqueID = Referral.ReferralSourceUniqueID)
    
-- Join to WH.RF.Encounter to access columns not available in WHREPORTING.
INNER JOIN WH.RF.Encounter AS ReferralEncounter 
  ON Referral.ReferralSourceUniqueID = ReferralEncounter.SourceUniqueID 

-- Join last RTT status before the end date.
LEFT JOIN WH.RTT.RTT LastRTTHistory
  ON (Referral.ReferralSourceUniqueID = LastRTTHistory.ReferralSourceUniqueID
      AND LastRTTHistory.EncounterRecno = (-- To work out the latest RTT status use the maximum EncounterRecno
                                           -- value for each referral.
                                           SELECT MAX(LastRTTHistoryLookup.EncounterRecno) MaxEncounterRecno
                                           FROM WH.RTT.RTT LastRTTHistoryLookup
                                           WHERE LastRTTHistoryLookup.StartDate < GETDATE()
                                                 AND LastRTTHistoryLookup.ReferralSourceUniqueID = Referral.ReferralSourceUniqueID
                                           GROUP BY LastRTTHistoryLookup.ReferralSourceUniqueID                                               
                                          )) 

LEFT JOIN WH.RTT.RTTStatus LastRTTHistoryStatus
  ON (LastRTTHistory.RTTStatusCode = LastRTTHistoryStatus.InternalCode)  
  
-- Join valid parent referrals used to calculate waiting time.
LEFT JOIN (SELECT 
             ParentReferralEncounter.SourceUniqueID
             ,ParentReferralEncounter.ReferralDate
             ,LastRTTHistoryParent.StartDate
             ,LastRTTHistoryParentStatus.RTTStatusCode
           FROM WH.RF.Encounter ParentReferralEncounter 
           
           LEFT JOIN WH.PAS.Specialty ParentReferralSpecialty
             ON (ParentReferralEncounter.SpecialtyCode = ParentReferralSpecialty.SpecialtyCode)
           
           LEFT JOIN WH.RTT.RTT LastRTTHistoryParent
             ON (ParentReferralEncounter.SourceUniqueID = LastRTTHistoryParent.ReferralSourceUniqueID
                 AND LastRTTHistoryParent.EncounterRecno = (-- To work out the latest RTT status use the maximum EncounterRecno
                                                            -- value for each parent referral.
                                                            SELECT MAX(LastRTTHistoryParentLookup.EncounterRecno) MaxEncounterRecno
                                                            FROM WH.RTT.RTT LastRTTHistoryParentLookup
                                                            WHERE LastRTTHistoryParentLookup.StartDate < GETDATE()
                                                                  AND LastRTTHistoryParentLookup.ReferralSourceUniqueID = ParentReferralEncounter.SourceUniqueID
                                                            GROUP BY LastRTTHistoryParentLookup.ReferralSourceUniqueID                                               
                                                           )) 

           LEFT JOIN WH.RTT.RTTStatus LastRTTHistoryParentStatus
             ON (LastRTTHistoryParent.RTTStatusCode = LastRTTHistoryParentStatus.InternalCode)
           
           WHERE -- Include parent referral if not had any excluded RTT status codes.
                 NOT EXISTS (SELECT 1
                             FROM WH.RTT.RTT ParentReferralRTTHistory
                             INNER JOIN WH.RTT.RTTStatus ParentReferralHistoryRTTStatus 
                              ON (ParentReferralRTTHistory.RTTStatusCode = ParentReferralHistoryRTTStatus.InternalCode)
                             WHERE ParentReferralRTTHistory.ReferralSourceUniqueID = ParentReferralEncounter.SourceUniqueID
                                   AND ParentReferralHistoryRTTStatus.RTTStatusCode IN ('30','31','32','34','35','36','98')
                                   AND ParentReferralRTTHistory.StartDate <= GETDATE() -- Only included statuses dated before end date.
                            )
                 -- Include parent referral if national specialty code not in exlusion list. 
                 AND ParentReferralSpecialty.NationalSpecialtyCode NOT IN ( '501' -- Obstetrics.
                                                                           ,'560') -- Midwife Episode.          
          ) ParentReferral 
       ON (ReferralEncounter.ParentSourceUniqueID = ParentReferral.SourceUniqueID)     
;

--SELECT ReferralDataset.ReferralSourceUniqueID
--      ,ReferralDataset.ReferralPatientNumber
--      ,ReferralDataset.PatientSurname
--      ,ReferralDataset.PatientForename
--      ,ReferralDataset.ReferralReceivedDate
--      ,ReferralDataset.ReferralReceivingSpecialty
--      ,LastAppointmentDate = COALESCE(CONVERT(VARCHAR(10), (SELECT TOP 1 PrevAppLkp.AppointmentDate
--                                                            FROM WHREPORTING.OP.Schedule PrevAppLkp
--                                                            WHERE PrevAppLkp.SpecialtyCode = ReferralDataset.ReferralReceivingSpecialtyCode
--                                                                  AND PrevAppLkp.ReferralSourceUniqueID = ReferralDataset.ReferralSourceUniqueID
--                                                                  AND PrevAppLkp.AppointmentDateTime < GETDATE()
--                                                            ORDER BY PrevAppLkp.AppointmentDateTime DESC
--                                                           ),103),'')   
--      ,LastAppointmentClinicCode = (SELECT TOP 1 PrevClinicLkp.ClinicCode
--                                    FROM WHREPORTING.OP.Schedule PrevClinicLkp
--                                    WHERE PrevClinicLkp.SpecialtyCode = ReferralDataset.ReferralReceivingSpecialtyCode
--                                          AND PrevClinicLkp.ReferralSourceUniqueID = ReferralDataset.ReferralSourceUniqueID
--                                          AND PrevClinicLkp.AppointmentDateTime < GETDATE()
--                                    ORDER BY PrevClinicLkp.AppointmentDateTime DESC
--                                   )                                                                                        
--      ,NextAppointmentDate = COALESCE(CONVERT(VARCHAR(10), (SELECT TOP 1 NextAppLkp.AppointmentDate
--                                                            FROM WHREPORTING.OP.Schedule NextAppLkp
--                                                            WHERE NextAppLkp.SpecialtyCode = ReferralDataset.ReferralReceivingSpecialtyCode
--                                                                  AND NextAppLkp.ReferralSourceUniqueID = ReferralDataset.ReferralSourceUniqueID
--                                                                  AND NextAppLkp.AppointmentDateTime >= GETDATE()
--                                                            ORDER BY NextAppLkp.AppointmentDateTime ASC
--                                                           ),103),'') 
--      ,NextAppointmentClinicCode = (SELECT TOP 1 NextClinicLkp.ClinicCode
--                                    FROM WHREPORTING.OP.Schedule NextClinicLkp
--                                    WHERE NextClinicLkp.SpecialtyCode = ReferralDataset.ReferralReceivingSpecialtyCode
--                                          AND NextClinicLkp.ReferralSourceUniqueID = ReferralDataset.ReferralSourceUniqueID
--                                          AND NextClinicLkp.AppointmentDateTime >= GETDATE()
--                                    ORDER BY NextClinicLkp.AppointmentDateTime ASC
--                                   )                                                           
--      ,RTTBreachDate = CASE
--                            WHEN ReferralDataset.ReferralHasParentReferralFlag = 'N' THEN DATEADD(WEEK,18,ReferralDataset.ReferralReceivedDate)
--                            WHEN ReferralDataset.ExcludeParentReferralSpecialty = 'Y'
--                                 OR ReferralDataset.ExcludeParentReferralRTTPathway = 'Y' THEN DATEADD(WEEK,18,ReferralDataset.ReferralReceivedDate)
--                            WHEN ReferralDataset.ReferralHasParentReferralFlag = 'Y' THEN DATEADD(WEEK,18,ReferralDataset.ParentReferralReceivedDate)
--                          END       
--      ,ReferralDataset.ReferralRTTPathwayID
--      ,ReferralRTTCurrentStatusDate =	COALESCE(CONVERT(VARCHAR(10),ReferralDataset.ReferralRTTCurrentStatusDate ,103),'')
--      ,ReferralCurrentRTTStatus = COALESCE(ReferralDataset.ReferralCurrentRTTStatus,'')
--      ,ReferralHasParentReferralFlag = COALESCE(ReferralDataset.ReferralHasParentReferralFlag,'')
--      ,ParentRTTPathwayID = COALESCE(ReferralDataset.ParentRTTPathwayID,'')
--      ,ParentRTTCurrentStatusDate = COALESCE(CONVERT(VARCHAR(10),ReferralDataset.ParentRTTCurrentStatusDate,103),'')
--      ,ParentCurrentRTTStatus = COALESCE(ReferralDataset.ParentCurrentRTTStatus,'')
--      ,ParentReferralReceivedDate = COALESCE(CONVERT(VARCHAR(10),ReferralDataset.ParentReferralReceivedDate,103),'')
--      ,ParentReferralSpecialtyCode = COALESCE(ReferralDataset.ParentReferralSpecialtyCode,'')
--      ,ExcludeParentReferralSpecialty = COALESCE(ReferralDataset.ExcludeParentReferralSpecialty,'')
--      ,ExcludeParentReferralRTTPathway = COALESCE(ReferralDataset.ExcludeParentReferralRTTPathway,'')
--      ,CurrentWaitWeeks = CASE
--                            WHEN ReferralDataset.ReferralHasParentReferralFlag = 'N' THEN (DATEDIFF(DAY,ReferralDataset.ReferralReceivedDate,GETDATE()) / 7.00) 
--                            WHEN ReferralDataset.ExcludeParentReferralSpecialty = 'Y'
--                                 OR ReferralDataset.ExcludeParentReferralRTTPathway = 'Y' THEN (DATEDIFF(DAY,ReferralDataset.ReferralReceivedDate,GETDATE()) / 7.00) 
--                            WHEN ReferralDataset.ReferralHasParentReferralFlag = 'Y' THEN (DATEDIFF(DAY,ReferralDataset.ParentReferralReceivedDate,GETDATE()) / 7.00)
--                          END
--      ,CurrentWaitWeeksStatus = CASE
--                                  WHEN ReferralDataset.ReferralHasParentReferralFlag = 'N' THEN 'Calculated from referral' 
--                                  WHEN ReferralDataset.ExcludeParentReferralSpecialty = 'Y'
--                                       OR ReferralDataset.ExcludeParentReferralRTTPathway = 'Y' THEN 'Calculated from referral, parent referral excluded' 
--                                  WHEN ReferralDataset.ReferralHasParentReferralFlag = 'Y' THEN 'Calculated from parent referral'
--                                END                                               
--FROM (
--      SELECT Referral.ReferralSourceUniqueID
--            ,ReferralPatientNumber = Referral.FacilityID
--            ,ReferralEncounter.PatientSurname
--            ,ReferralEncounter.PatientForename
--            ,Referral.ReferralReceivedDate
--            ,ReferralReceivingSpecialtyCode = Referral.ReceivingSpecialtyCode
--            ,ReferralReceivingSpecialty = Referral.[ReceivingSpecialty(Function)]
--            ,ReferralRTTPathwayID = ReferralEncounter.RTTPathwayID
--            ,ReferralRTTCurrentStatusDate = ReferralEncounter.RTTCurrentStatusDate 
--            ,ReferralCurrentRTTStatus = ReferralCurrentRTTStatus.RTTStatus    
--            ,ReferralHasParentReferralFlag = CASE
--                                               WHEN ParentReferralEncounter.SourceUniqueID IS NOT NULL THEN 'Y'
--                                               ELSE 'N'
--                                             END
--             -- Parent referral details.                                              
--            ,ParentRTTPathwayID = ParentReferralEncounter.RTTPathwayID
--            ,ParentRTTCurrentStatusDate = ParentReferralEncounter.RTTCurrentStatusDate
--            ,ParentCurrentRTTStatus = ParentReferralCurrentRTTStatus.RTTStatus
--            ,ParentReferralReceivedDate = ParentReferralEncounter.ReferralDate
--            ,ParentReferralSpecialtyCode = ParentReferralSpecialty.NationalSpecialtyCode
--            ,ExcludeParentReferralSpecialty = CASE
--                                                WHEN ParentReferralSpecialty.NationalSpecialtyCode IN ( '501' -- Obstetrics.
--                                                                                                       ,'560') -- Midwife Episode.
--                                                                                                           THEN 'Y'
--                                              END
--            ,ExcludeParentReferralRTTPathway = (
--                                                SELECT TOP 1 'Y'
--                                                FROM WH.RTT.RTT ParentReferralRTTHistory
              
--                                                     INNER JOIN WH.RTT.RTTStatus ParentReferralHistoryRTTStatus 
--                                                       ON ParentReferralRTTHistory.RTTStatusCode = ParentReferralHistoryRTTStatus.InternalCode   
         
--                                                WHERE ParentReferralRTTHistory.ReferralSourceUniqueID = ParentReferralEncounter.SourceUniqueID
--                                                      AND (LEFT(ParentReferralHistoryRTTStatus.RTTStatusCode,1) = '3'
--                                                           OR ParentReferralHistoryRTTStatus.RTTStatusCode = '98')
--                                               )
--      FROM WHREPORTING.RF.Referral

--           INNER JOIN WH.RF.Encounter AS ReferralEncounter 
--             ON Referral.ReferralSourceUniqueID = ReferralEncounter.SourceUniqueID
       
--           LEFT JOIN WH.RTT.RTTStatus ReferralCurrentRTTStatus
--             ON ReferralEncounter.RTTCurrentStatusCode = ReferralCurrentRTTStatus.InternalCode       

--           LEFT JOIN WH.RF.Encounter AS ParentReferralEncounter 
--             ON ReferralEncounter.ParentSourceUniqueID = ParentReferralEncounter.SourceUniqueID

--           LEFT JOIN WH.RTT.RTTStatus ParentReferralCurrentRTTStatus 
--             ON ParentReferralEncounter.RTTCurrentStatusCode = ParentReferralCurrentRTTStatus.InternalCode
       
--           LEFT JOIN WH.PAS.Specialty ParentReferralSpecialty
--             ON ParentReferralEncounter.SpecialtyCode = ParentReferralSpecialty.SpecialtyCode
      
--           INNER JOIN #Specialty sp 
--             ON Referral.ReceivingSpecialtyCode = sp.Item
      
--      WHERE Referral.CompletionDate IS NULL -- Referral has not been completed.
--            AND Referral.CancelDate IS NULL -- Referral has not been cancelled.                                                          
--            --Exclude any referrals that have a previously attended outpatient appointment. 
--            AND NOT EXISTS (SELECT 1
--                            FROM WHREPORTING.TPY.ContactsDataset LastContactLkp
--                            WHERE LastContactLkp.Ref_Ref = Referral.ReferralSourceUniqueID
--                                  AND LastContactLkp.Cont_Date < Cast(Getdate() AS DATE))   
                             
--            -- Exclude any referrals that have a previous contact.
--            AND NOT EXISTS (SELECT 1
--                            FROM WHREPORTING.OP.Schedule LastAttendedAppLkp
--                            WHERE LastAttendedAppLkp.ReferralSourceUniqueID = Referral.ReferralSourceUniqueID
--                                  AND LastAttendedAppLkp.Status = 'Attend'
--                                  AND LastAttendedAppLkp.AppointmentDate < Cast(Getdate() AS DATE))                                                                                        
--     ) ReferralDataset;

-- Tidy up temporary parameter tables
IF Object_id('tempdb..#Specialty', 'U') IS NOT NULL
  DROP TABLE #Specialty