﻿/*
--Author: N Scattergood
--Date created: 09/07/2012
--Stored Procedure Built for the SRSS Report on Cancer Additions To WL
--i.e. This is a listing of all new additions to the waiting list, 
--with procedures that could indicate skin cancer or lower GI cancer, 
--but where the “Cancer Suspected” flag has not been set on Lorenzo.
--1) Missing Offer Outcome
*/

CREATE Procedure [PTL].[ReportCancerAdditionsToWL]
@StartDate		as Date
, @EndDate		as Date  
,@CancType		nvarchar (100)
as
select
WL.CensusDate
,WL.[LocalCategoryCode]
      ,S.LocalSpecialtyCode
      ,S.Specialty
      ,C.[Consultant]

    
      ,WL.[DistrictNo] as 'RM2'
      ,WL.[PatientSurname]
          ,CASE 
      WHEN WL.[SexCode] = '9269' THEN 'Female'
      WHEN WL.[SexCode] = '9270' THEN 'Male'
      Else Null End as 'Gender'
          ,WL.[IntendedPrimaryOperationCode]
      ,WL.[Operation]
      ,CASE 
                WHEN MI.NationalManagementIntentionCode = '1' THEN 'IP' 
                WHEN MI.NationalManagementIntentionCode= '2' THEN 'DC' 
                WHEN MI.NationalManagementIntentionCode= '3' THEN 'IP' 
                WHEN MI.NationalManagementIntentionCode= '8' THEN 'Not Applicable' 
                WHEN MI.NationalManagementIntentionCode= '9' THEN 'Not Known' 
                ELSE MI.[ManagementIntention]
                End as 'IntendedManagement'
               
      ,MI.[ManagementIntention]
      ,CASE 
                WHEN AM.AdmissionMethodLocalCode in ('11','12') THEN 'Waiting'
                WHEN AM.AdmissionMethodLocalCode = '13' THEN 'Planned'
                ELSE AM.AdmissionMethod   
                END as 'WLType'         
      
      ,AM.[AdmissionMethod]
      ,P.[Priority]
      ,WL.GeneralComment
      ,WL.TCIDate
      ,WL.AdmissionOfferOutcomeCode
      ,WL.OriginalDateOnWaitingList
      ,WL.DateOnWaitingList
      ,case 
      when
      (
--Skin
S.LocalSpecialtyCode = 160
and
(
GeneralComment like '%melanoma%'
or GeneralComment like '%melanoma%'
or GeneralComment like '%scc%'
or GeneralComment like '%lesion%'
or GeneralComment like '% mm %'
or GeneralComment like '%carcinoma%'
)
	) then 'Skin'
	when
	(S.LocalSpecialtyCode = 100
and
	(
	GeneralComment like '%Fibroepithelial%'
or GeneralComment like '%Lymph%'
or GeneralComment like '%Node%'
or GeneralComment like '% Lump %'
or GeneralComment like '%Villous%'
or GeneralComment like '%Adenoma%'
or GeneralComment like '%Hemicolectomy%'
or GeneralComment like '%Colectomy%'
or GeneralComment like '%HARTMAN'
or GeneralComment like '%Ca %'
or GeneralComment like '%Tumour%'
or GeneralComment like '% GIST %'
or GeneralComment like '%Cholecystectomy%'
or GeneralComment like '%ERCP%'
or GeneralComment like '%STENT%'
or GeneralComment like '%MRCP%'
or GeneralComment like '%Hemicolectomy%'
)
	) then 'Lower GI'
	end as CancerType

	
      
  FROM [WH].[APC].[WaitingList] WL
  
    Left Join [WH].[PAS].[Consultant] C
			on WL.[ConsultantCode] = C.ConsultantCode
			
		Left Join [WH].[PAS].[Specialty] S
				on WL.SpecialtyCode = S.SpecialtyCode

			Left Join [WH].[PAS].[AdmissionMethod] AM
					on WL.AdmissionMethodCode = AM.AdmissionMethodCode
				
				Left Join [WH].[PAS].[ManagementIntention] MI
						on WL.ManagementIntentionCode = MI.ManagementIntentionCode
						
						Left Join [WH].[PAS].[Priority] P
								on WL.PriorityCode = P.PriorityCode
	  where 
  
  WL.CensusDate = (Select 
					MAX (WL.CensusDate)
						FROM [WH].[APC].[WL] WL) --Latest Census Date	
and
WL.[LocalCategoryCode] is null						
and 
cast(WL.PASCreated as DATE) between @StartDate and @EndDate
and
case 
      when
      (
--Skin
S.LocalSpecialtyCode = '160'
and
(
GeneralComment like '%melanoma%'
or GeneralComment like '%melanoma%'
or GeneralComment like '%scc%'
or GeneralComment like '%lesion%'
or GeneralComment like '% mm %'
or GeneralComment like '%carcinoma%'
)
	) then 'Skin'
	when
	(S.LocalSpecialtyCode = '100'
and
	(
	GeneralComment like '%Fibroepithelial%'
or GeneralComment like '%Lymph%'
or GeneralComment like '%Node%'
or GeneralComment like '% Lump %'
or GeneralComment like '%Villous%'
or GeneralComment like '%Adenoma%'
or GeneralComment like '%Hemicolectomy%'
or GeneralComment like '%Colectomy%'
or GeneralComment like '%HARTMAN'
or GeneralComment like '%Ca %'
or GeneralComment like '%Tumour%'
or GeneralComment like '% GIST %'
or GeneralComment like '%Cholecystectomy%'
or GeneralComment like '%ERCP%'
or GeneralComment like '%STENT%'
or GeneralComment like '%MRCP%'
or GeneralComment like '%Hemicolectomy%'
)
	) then 'Lower GI'
	end in (SELECT Val from dbo.fn_String_To_Table(@CancType,',',1)) 

order by 
WL.DateOnWaitingList asc
,TCIDate asc