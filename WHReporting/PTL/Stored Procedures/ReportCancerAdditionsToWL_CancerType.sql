﻿/*
--Author: N Scattergood
--Date created: 09/07/2012
--Stored Procedure Built for the SRSS Report on Cancer Additions To WL
--Provides Parameter dropdown for Cancer Type
*/

CREATE Procedure [PTL].[ReportCancerAdditionsToWL_CancerType]
as
select 
distinct
case 
      when
      (
--Skin
S.LocalSpecialtyCode = 160
and
(
GeneralComment like '%melanoma%'
or GeneralComment like '%melanoma%'
or GeneralComment like '%scc%'
or GeneralComment like '%lesion%'
or GeneralComment like '% mm %'
or GeneralComment like '%carcinoma%'
)
	) then 'Skin'
	when
	(S.LocalSpecialtyCode = 100
and
	(
	GeneralComment like '%Fibroepithelial%'
or GeneralComment like '%Lymph%'
or GeneralComment like '%Node%'
or GeneralComment like '% Lump %'
or GeneralComment like '%Villous%'
or GeneralComment like '%Adenoma%'
or GeneralComment like '%Hemicolectomy%'
or GeneralComment like '%Colectomy%'
or GeneralComment like '%HARTMAN'
or GeneralComment like '%Ca %'
or GeneralComment like '%Tumour%'
or GeneralComment like '% GIST %'
or GeneralComment like '%Cholecystectomy%'
or GeneralComment like '%ERCP%'
or GeneralComment like '%STENT%'
or GeneralComment like '%MRCP%'
or GeneralComment like '%Hemicolectomy%'
)
	) then 'Lower GI'
	end as CancerTypePar

	
      
  FROM [WH].[APC].[WaitingList] WL
  
  		Left Join [WH].[PAS].[Specialty] S
				on WL.SpecialtyCode = S.SpecialtyCode

	  where 
  
  WL.CensusDate = (Select 
					MAX (WL.CensusDate)
						FROM [WH].[APC].[WL] WL) --Latest Census Date
						
						and
(
  (
--Skin
S.LocalSpecialtyCode = 160
and
(
GeneralComment like '%melanoma%'
or GeneralComment like '%melanoma%'
or GeneralComment like '%scc%'
or GeneralComment like '%lesion%'
or GeneralComment like '% mm %'
or GeneralComment like '%carcinoma%'
)
	) OR
(S.LocalSpecialtyCode = 100
and
	(
	GeneralComment like '%Fibroepithelial%'
or GeneralComment like '%Lymph%'
or GeneralComment like '%Node%'
or GeneralComment like '% Lump %'
or GeneralComment like '%Villous%'
or GeneralComment like '%Adenoma%'
or GeneralComment like '%Hemicolectomy%'
or GeneralComment like '%Colectomy%'
or GeneralComment like '%HARTMAN'
or GeneralComment like '%Ca %'
or GeneralComment like '%Tumour%'
or GeneralComment like '% GIST %'
or GeneralComment like '%Cholecystectomy%'
or GeneralComment like '%ERCP%'
or GeneralComment like '%STENT%'
or GeneralComment like '%MRCP%'
or GeneralComment like '%Hemicolectomy%'
)
	)	
)