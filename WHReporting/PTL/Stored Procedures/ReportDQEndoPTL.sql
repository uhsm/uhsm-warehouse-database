﻿-- =============================================
-- Author:		J Potluri
-- Create date: 05/06/2014
-- Description:	Stored procedure for DQ Endo PTL SSRS report - RD216
-- =============================================
CREATE PROCEDURE [PTL].[ReportDQEndoPTL]
	-- Add the parameters for the stored procedure here
	 @Spec Varchar(500), @StartDate Datetime, @EndDate Datetime, @Status Varchar(100)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT *
	FROM
	(
	
	SELECT 
      'Removed' as Category
      ,[DistrictNo]
      --,[PatientForename]
      ,[PatientSurname]
      ,W.[SpecialtyCode]
      ,S.Specialty
      ,[ElectiveAdmissionMethod]
      ,LEFT(W.PlannedProcedure,4) as ProcedureCode
      ,PlannedProcedure
      ,TCIDate
      ,RemovalDate
      ,RemovalReason
      ,RTTCode
      ,RTTDescription
      ,D.Description as 'DiagDescription'
              
  FROM [WH].[PTL].[IPWL]W
  
  Left Join WH.WH.Specialty S
  ON W.SpecialtyCode = S.SpecialtyCode
  
  Left Join WH.LK.lkNewDiagExclusionCodes D
  ON LEFT(W.PlannedProcedure,4)= D.Code
  
  Where
  RemovalDate between @StartDate and  @EndDate
  AND
  ElectiveAdmissionMethod in ('11','12')
  AND
  D.Description is not null
  AND W.SpecialtyCode in (SELECT Item
                         FROM   dbo.Split (@Spec, ','))
  
  
union all

--Waiting 
SELECT 
      'Waiting' as Category
      ,[DistrictNo]
      --,[PatientForename]
      ,[PatientSurname]
      ,W.[SpecialtyCode]
      ,S.Specialty
      ,[ElectiveAdmissionMethod]
      ,LEFT(W.PlannedProcedure,4) as ProcedureCode
      ,PlannedProcedure
      ,TCIDate
      ,RemovalDate
      ,RemovalReason
      ,RTTCode
      ,RTTDescription
      ,D.Description as 'DiagDescription'
              
  FROM [WH].[PTL].[IPWL]W
  
  Left Join WH.WH.Specialty S
  ON W.SpecialtyCode = S.SpecialtyCode
  
  Left Join WH.LK.lkNewDiagExclusionCodes D
  ON LEFT(W.PlannedProcedure,4)= D.Code
  
  Where
  Convert(datetime,floor(convert(float,TCIDate))) between @StartDate and  @EndDate
  AND
  ElectiveAdmissionMethod in ('11','12')
  AND
  RemovalDate is null
  AND
  D.Description is not null
  AND W.SpecialtyCode in (SELECT Item
                         FROM   dbo.Split (@Spec, ','))
) allrecords
where Category in (SELECT Item
                         FROM   dbo.Split (@Status, ','))



END