﻿-- =============================================
-- Author:		JPotluri
-- Create date: 24/03/2014
-- Description: To create Priority dropdown list for SSRS report 'ReportDailyAdmitted_Specialty'
-- Associated procedure for the report is [PTL].[ReportDailyAdmitted_Specialty]
--KO updated 12/08/2014 to change data source to RTT_DW
-- =============================================
CREATE PROCEDURE [PTL].[ReportDailyAdmitted_BreachDate] 
@Spec varchar (1000)
,@Consultant varchar(MAX)	

AS
BEGIN
	
	SET NOCOUNT ON;
	
Select
Distinct
Cast(RTTBreachDate as DATE)as RTTBreachDate

FROM [RTT_DW].[PTL].[Admitted]

Where
  [SpecialtyDescription] in (SELECT Item			
                        FROM   dbo.Split (@Spec, ','))
  AND
  [Consultant] in (SELECT Item			
                        FROM   dbo.Split (@Consultant, ','))


Order by
[RTTBreachDate] Asc

    
	
END