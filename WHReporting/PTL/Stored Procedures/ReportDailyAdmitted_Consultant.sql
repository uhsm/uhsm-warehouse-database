﻿-- =============================================
-- Author:		JPotluri
-- Create date: 24/03/2014
-- Description: To create Consultant dropdown list for SSRS report 'PTL].[ReportDailyAdmitted]'
-- Associated procedure for the report is [PTL].[ReportDailyAdmitted_Specialty]
-- KO updated 12/08/2014 to change data source to RTT_DW
-- =============================================
CREATE PROCEDURE [PTL].[ReportDailyAdmitted_Consultant] 
@Spec varchar (1000)	
AS
BEGIN
	
	SET NOCOUNT ON;
	
Select
Distinct Case when Consultant = 'O''Ceallaigh' then 'OCeallaigh' else Consultant end  Consultant2,
Consultant

--FROM [v-uhsm-dw02].[RTT].[PTL].[Admitted]
FROM [RTT_DW].[PTL].[Admitted]


Where
[SpecialtyDescription] in (SELECT Item			
                        FROM   dbo.Split (@Spec, ','))


Order by
Consultant Asc

    
	
END