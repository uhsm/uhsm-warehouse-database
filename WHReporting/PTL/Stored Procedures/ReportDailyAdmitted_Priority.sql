﻿-- =============================================
-- Author:		JPotluri
-- Create date: 26/03/2014
-- Description: To create Priority dropdown list for SSRS report 'ReportDailyAdmitted_Specialty'
-- Associated procedure for the report is [PTL].[ReportDailyAdmitted_Specialty]
-- KO updated 12/08/2014 to change data source to RTT_DW
-- =============================================
CREATE PROCEDURE [PTL].[ReportDailyAdmitted_Priority] 
@Spec varchar (1000)
,@Consultant varchar(MAX)	

AS
BEGIN
	
	SET NOCOUNT ON;
	
Select
Distinct
isnull([ReferralPriority],'Unknown') as ReferralPriority

--FROM [v-uhsm-dw02].[RTT].[PTL].[Admitted]
FROM [RTT_DW].[PTL].[Admitted]
Where
  [SpecialtyDescription] in (SELECT Item			
                        FROM   dbo.Split (@Spec, ','))
  AND
  [Consultant] in (SELECT Item			
                        FROM   dbo.Split (@Consultant, ','))


Order by
[ReferralPriority] Desc

    
	
END