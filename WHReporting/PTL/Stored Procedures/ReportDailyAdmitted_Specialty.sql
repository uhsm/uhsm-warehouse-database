﻿CREATE procedure [PTL].[ReportDailyAdmitted_Specialty]
@Spec varchar (1000)
,@Consultant varchar(MAX)
,@Priority Varchar (250)
,@PTLComments Varchar (MAX)
,@RecordCreatedDateFrom Date
,@RecordCreatedDateTo Date
as
/*
--Author: J POTLURI
--Date created: 25/03/2014
--Description:	[PTL].[ReportDailyAdmitted_Specialty]								
--				Log Number : RD460	
--KO updated 11/08/2014 to change data source to RTT_DW
*/


IF Object_id('tempdb..#Consultant', 'U') IS NOT NULL
  DROP TABLE #Consultant

SELECT Item
INTO   #Consultant
FROM   WHREPORTING.dbo.Splitstrings_cte(@Consultant, N',')


SELECT [SpecialtyDescription] as Specialty
      ,[Consultant]
      ,[PatientID]
      ,[PatientName]
      ,[Sex]
      ,[Age]
      ,[Priority2]
      ,[CABGPTCA]
      ,[PlannedProcedure1]
      ,[GeneralComment]
      ,[AnaestheticType]
      ,[EstTheatreTime]
      ,[PatientClass]
      ,[ListType]
      ,[AdmissionWard]
      ,[SecondaryWard]
      ,[ClockStartDate]
      ,[DateOnList]
      ,[LatestRTTCode]
      ,[CurrentSuspensionType]
      ,[SuspendedEndDate]
      ,[FutureSuspensionDate]
      ,[FutureSuspensionDays]
      ,[RTTWaitNowDays]
      ,[RTTWaitNowWeeksAdj]
      ,[RTTWaitNowWeeksUnAdj]
      ,[RTTBreachDate]
      ,[TCIDate]
      ,[OperationDate]
      ,[PreopDate]
      --,CONVERT(VARCHAR(10),[DiagnosticTarget],105) AS 'DiagnosticTarget'
      ,[DiagnosticTarget]
      ,isnull([PTLComments],'Unknown')as PTLComments
      ,[RefCompComment]
      ,[ELAdmission]
      ,[GPCode]
      ,[GPPracticeCode]
      ,[PracticePCTCode]
      ,[ADPostCode]
      ,[ResidencePCTCode]
      ,[SuspToDate]
      ,[EthGroup]
      ,[PPID]
      ,[SpecialtyCode]
      ,[ConsultantCode]
      ,[ElectiveAdmissionMethod]
      ,[NHSSourceReferralCode]
      ,isnull([ReferralPriority],'Unknown') as ReferralPriority
      ,[ReferringGPCode]
      ,[ReferringPracticeCode]
      ,[ReferralReceivedDate]
      ,[ELAdmissionStatus]
      ,[WLUniqueID]
      ,[CensusDate]
      ,[RecordCreatedDate]
  --FROM [v-uhsm-dw02].[RTT].[PTL].[Admitted]
  FROM [RTT_DW].[PTL].[Admitted]
         INNER JOIN  #Consultant con
               ON   case when [Consultant] = 'O''Ceallaigh' then 'OCeallaigh' Else [Consultant] End = con.Item


  Where
  [SpecialtyDescription] in (SELECT Item			
                        FROM   dbo.Split (@Spec, ','))
  --AND
  --[Consultant] in (SELECT Item			
  --                      FROM   dbo.Split (@Consultant, ','))
  AND
  isnull([ReferralPriority],'Unknown') in (SELECT Item			
                        FROM   dbo.Split (@Priority, ','))
                     
   AND
   isnull([PTLComments],'Unknown') in (SELECT Item			
                        FROM   dbo.Split (@PTLComments, ','))
   AND RecordCreatedDate BETWEEN (@RecordCreatedDateFrom) AND (@RecordCreatedDateTo)
                     
                        
  
  
  Order by
  SpecialtyDescription Asc,
  Consultant Asc,
  [Priority2] Desc,
  --isnull([ReferralPriority],'Unknown') Desc,
  Cast(RTTBreachDate as DATE) Asc,
  isnull([PTLComments],'Unknown')