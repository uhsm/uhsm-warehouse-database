﻿-- =============================================
-- Author:		JPotluri
-- Create date: 24/03/2014
-- Description: To create Specialty dropdown list for SSRS report 'PTL].[ReportDailyAdmitted]'
-- Associated procedure for the report is [PTL].[ReportDailyAdmitted_Specialty]#
-- KO updated 12/08/2014 to change data source to RTT_DW
-- =============================================
CREATE PROCEDURE [PTL].[ReportDailyAdmitted_Specialty_DropdownList] 
	
AS
BEGIN
	
	SET NOCOUNT ON;
	
Select
Distinct
SpecialtyDescription as 'Specialty'
--FROM [v-uhsm-dw02].[RTT].[PTL].[Admitted]
FROM [RTT_DW].[PTL].[Admitted]
Order by
SpecialtyDescription Asc

    
	
END