﻿CREATE procedure [PTL].[ReportDailyAdmitted_Specialty_Original]
@Spec varchar (1000)
,@Consultant varchar(MAX)
,@Priority Varchar (250)
,@RTTBreachDate as Date 
,@PTLComments Varchar (MAX)
as
/*
--Author: J POTLURI
--Date created: 27/03/2014
--Description:	[ReportDailyAdmitted_Specialty_Original]								
--				Log Number : RD460	
--KO updated 11/08/2014 to change data source to RTT_DW

*/
SELECT [SpecialtyDescription] as Specialty
      ,[Consultant]
      ,[PatientID]
      ,[PatientName]
      ,[Sex]
      ,[Age]
      ,[Priority2]
      ,[CABGPTCA]
      ,[PlannedProcedure1]
      ,[GeneralComment]
      ,[AnaestheticType]
      ,[EstTheatreTime]
      ,[PatientClass]
      ,[ListType]
      ,[AdmissionWard]
      ,[SecondaryWard]
      ,[ClockStartDate]
      ,[DateOnList]
      ,[LatestRTTCode]
      ,[CurrentSuspensionType]
      ,[SuspendedEndDate]
      ,[FutureSuspensionDate]
      ,[FutureSuspensionDays]
      ,[RTTWaitNowDays]
      ,[RTTWaitNowWeeksAdj]
      ,[RTTWaitNowWeeksUnAdj]
      ,[RTTBreachDate]
      ,[TCIDate]
      ,[OperationDate]
      ,[PreopDate]
      ,[DiagnosticTarget]
      ,isnull([PTLComments],'Unknown')as PTLComments
      ,[RefCompComment]
      ,[ELAdmission]
      ,[GPCode]
      ,[GPPracticeCode]
      ,[PracticePCTCode]
      ,[ADPostCode]
      ,[ResidencePCTCode]
      ,[SuspToDate]
      ,[EthGroup]
      ,[PPID]
      ,[SpecialtyCode]
      ,[ConsultantCode]
      ,[ElectiveAdmissionMethod]
      ,[NHSSourceReferralCode]
      ,isnull([ReferralPriority],'Unknown') as ReferralPriority
      ,[ReferringGPCode]
      ,[ReferringPracticeCode]
      ,[ReferralReceivedDate]
      ,[ELAdmissionStatus]
      ,[WLUniqueID]
      ,[CensusDate]
  FROM [RTT_DW].[PTL].[Admitted]

  Where
  [SpecialtyDescription] in (SELECT Item			
                        FROM   dbo.Split (@Spec, ','))
  AND
  [Consultant] in (SELECT Item			
                        FROM   dbo.Split (@Consultant, ','))
  AND
  isnull([ReferralPriority],'Unknown') in (SELECT Item			
                        FROM   dbo.Split (@Priority, ','))
                     
   AND
   CAST(RTTBreachDate as DATE)  in (@RTTBreachDate) 
   AND
   isnull([PTLComments],'Unknown') in (SELECT Item			
                        FROM   dbo.Split (@PTLComments, ','))
                     
                        
  
  
  Order by
  SpecialtyDescription Asc,
  Consultant Asc,
  isnull([ReferralPriority],'Unknown') Desc,
  Cast(RTTBreachDate as DATE) Asc,
  isnull([PTLComments],'Unknown')