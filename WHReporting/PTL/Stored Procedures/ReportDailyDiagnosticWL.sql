﻿CREATE procedure [PTL].[ReportDailyDiagnosticWL] as

/**
Author: K Oakden
Date: 21/10/2014
Based on cognos query IPDC WL Diagnostics.imr for daily PTL

**/
declare @MonthEndDate datetime

select @MonthEndDate = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+1,0))

select 
	admitted.SpecialtyDescription
	,admitted.Consultant
	,admitted.DistrictNo
	,admitted.PatientSurname
	,admitted.Age
	,Diagnostic = admitted.DiagnosticGroup
	,ProcedureComment = admitted.GeneralComment
	,PatClass = 
		case 
			when IntendedManagement in ('1', 'I')
				then 'IP'
			when IntendedManagement in ('D', '2')
				then 'DC'
			else null
		end
	,admitted.IPWLWaitingStartDate
	,WeekWaitNow = DayWaitNow/7
	,WeekWaitEndIncSus = 
		(DayWaitNow + 
			case 
				when SuspendedEndDate < @MonthEndDate 
				then DATEDIFF(day,getdate(), SuspendedEndDate) - DATEDIFF(day,getdate(), @MonthEndDate)
				else DATEDIFF(day,getdate(), @MonthEndDate)
			end)/7
	,WeekWaitExpected = (DayWaitNow + DATEDIFF(day,getdate(), TCIDate))/7
	,SuspendedEndDate
	,DiagnosticTarget
	,TCIDate
	,FinalNewComments = 
		case 
			when TCIDate >= Breach26Date 
				then 
					case 
						when TCIDate > DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,Breach26Date)+1,0))
						then 'End'
						else 'In'
					end + ' 26wk Breach'
			when TCIDate >= Breach20Date 
				then 
					case 
						when TCIDate > DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,Breach20Date)+1,0))
						then 'End'
						else 'In'
					end + ' 20wk Breach'
					
			when TCIDate >= Breach18Date 
				then 
					case 
						when TCIDate > DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,Breach18Date)+1,0))
						then 'End'
						else 'In'
					end + ' 18wk Breach'
			when TCIDate >= Breach15Date 
				then 
					case 
						when TCIDate > DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,Breach15Date)+1,0))
						then 'End'
						else 'In'
					end + ' 15wk Breach'
				
			when TCIDate >= Breach13Date 
				then 
					case 
						when TCIDate > DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,Breach13Date)+1,0))
						then 'End'
						else 'In'
					end + ' 13wk Breach'
			when TCIDate >= Breach6Date 
				then 
					case 
						when TCIDate > DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,Breach6Date)+1,0))
						then 'End'
						else 'In'
					end + ' 6wk Breach'
			when TCIDate < dateadd(day, 0, datediff(day, 0, GETDATE()))
				then '? Outcome'
			when TCIDate is null	
				then 'No TCI Date'
			else 'OK'
		end
	,NextResetDate
	,BreachFilter = 
		case 
			when DiagnosticTarget < @MonthEndDate 
				then 'CurrentMonth'
			when DiagnosticTarget > @MonthEndDate and DiagnosticTarget< DATEADD(MONTH, 1, @MonthEndDate) 
				then 'NextMonth'
			else ''
		end
	--,WLUniqueID
from 
(
	select 
		ptl.SpecialtyDescription
		,ptl.Consultant
		,ptl.DistrictNo
		,ptl.PatientSurname
		,ptl.Age
		,DayWaitNow = DATEDIFF(day, ptl.IPWLWaitingStartDate, GETDATE()) + isnull(ipwl.SuspensionPeriod, 0)/7
		
		--KO 12/12/14 The Diagnostic case statement is now performed in the build of the Admitted PTL
		--,Diagnostic = 
		--	case 
		--		when left(ptl.PlannedProcedure1, 4) = 'K582'
		--		then 'Electro'
		--		when left(ptl.PlannedProcedure1, 4) = 'A847'
		--		then 'Sleep'
		--		when left(ptl.PlannedProcedure1, 3) = 'H25'
		--		then 'Sigmoid'
		--		when left(ptl.PlannedProcedure1, 3) in ('H22', 'H28')
		--		then 'Colon'
		--		when left(ptl.PlannedProcedure1, 3) in ('M30', 'M45', 'M77')
		--		then 'Cysto'
		--		when left(ptl.PlannedProcedure1, 3) in ('G80', 'G65', 'G55', 'G45')
		--		then 'Gastro'
		--		when left(ptl.PlannedProcedure1, 4) = 'M474'
		--		then 'Urodyn'
		--		when left(ptl.PlannedProcedure1, 4) = 'U202'
		--		then 'TOE'
		--		else null
		--	end
		,PTL.DiagnosticGroup
		,GeneralComment = dbo.CleanString(ptl.GeneralComment) --use Clean string as some text is being truncated ?? possible CR/LF
		,ProcedureCode = left(ptl.PlannedProcedure1, 4) 
		--,ptl.PatientClass
		,ipwl.IntendedManagement
		,ptl.IPWLWaitingStartDate
		,ptl.IPWLWaitNowWeeksAdjNoRTT
		,ptl.IPWLWaitNowWeeksUnAdjNoRTT
		,ptl.DateOnList
		,ptl.SuspToDate
		,ptl.DiagnosticTarget --6wk breach
		,ptl.WLUniqueID
		,ptl.TCIDate
		,CensusDate = CONVERT(VARCHAR(8), GETDATE(), 112)
		,ptl.SuspendedEndDate
		,ipwl.SuspensionPeriod
		,ipwl.NextResetDate
		,Breach26Date = PTL.GetAdjustedBreachDate (ptl.IPWLWaitingStartDate, ptl.SuspendedEndDate, isnull(ipwl.SuspensionPeriod, 0), 182)	
		,Breach20Date = PTL.GetAdjustedBreachDate (ptl.IPWLWaitingStartDate, ptl.SuspendedEndDate, isnull(ipwl.SuspensionPeriod, 0), 140)	
		,Breach18Date = PTL.GetAdjustedBreachDate (ptl.IPWLWaitingStartDate, ptl.SuspendedEndDate, isnull(ipwl.SuspensionPeriod, 0), 126)	
		,Breach15Date = PTL.GetAdjustedBreachDate (ptl.IPWLWaitingStartDate, ptl.SuspendedEndDate, isnull(ipwl.SuspensionPeriod, 0), 105)	
		,Breach13Date = PTL.GetAdjustedBreachDate (ptl.IPWLWaitingStartDate, ptl.SuspendedEndDate, isnull(ipwl.SuspensionPeriod, 0), 91)	
		,Breach6Date = PTL.GetAdjustedBreachDate (ptl.IPWLWaitingStartDate, ptl.SuspendedEndDate, isnull(ipwl.SuspensionPeriod, 0), 42)	
	from RTT_DW.PTL.Admitted ptl
	left join wh.ptl.IPWL ipwl
		on ipwl.SourceUniqueID = ptl.WLUniqueID
	where ptl.GeneralComment not like '%$T%'
	--where SuspendedEndDate is null
)admitted 

where admitted.DiagnosticGroup is not null
order by admitted.SpecialtyDescription, admitted.DayWaitNow desc, DiagnosticTarget