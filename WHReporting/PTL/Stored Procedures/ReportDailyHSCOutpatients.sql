﻿CREATE procedure [PTL].[ReportDailyHSCOutpatients] as
/**
Author: K Oakden
Date: 24/10/2014
Based on cognos query OP PTL HSC Booked and Non Booked.imr for daily PTL

**/
select * from 
(
	select
		SpecialtyCode = 
			case 
				when T1.ConsultantCode in (
						'C2436599'
						,'C2436702'
						,'C2630908'
						,'C3352889'
						,'C3490338'
						)
					then '103'
				when T1.ConsultantCode in (
						'C1548909'
						,'C3019085'
						,'C3486272'
						)
					then '107'
				else T1.WLSpecialtyCode
			end
		,Specialty = T1.WLSpecialty
		,T1.ConsultantSurname
		,T1.ClinicCode
		,T1.DistrictNo
		,T1.PatientSurname
		,ReferralDate = replace(convert(varchar(9), T1.ReferralDate, 6), ' ', '-') 
		,BookingStatus = 
			case 
				when T1.AppointmentDate is null
					then 'Non-Booked'
				else 'Booked'
			end

		,Day7Breach = replace(convert(varchar(9), dateadd(day, - 7, T1.CancerBreachDate), 6), ' ', '-') 
		,Day14Breach = replace(convert(varchar(9), T1.CancerBreachDate, 6), ' ', '-') 
		--,T1.WaitingStartDate
		,DayWait = datediff(day, T1.WaitingStartDate, getdate())
		,AppointmentDate = replace(convert(varchar(9), T1.AppointmentDate, 6), ' ', '-')
		,T1.NextReset
		,FinalComments = 
			case 
				when T1.AppointmentDate < dateadd(day, 0, datediff(day, 0, getdate()) )
					then '?Attendance' 
				else 		
					case 
						when T1.AppointmentDate > T1.CancerBreachDate
							then '14 day Breach'
						when T1.AppointmentDate > dateadd(day, - 4, T1.CancerBreachDate)
							then '10 day Breach'
						when T1.AppointmentDate > dateadd(day, - 7, T1.CancerBreachDate)
							then '7 day Breach'
						when T1.AppointmentDate > dateadd(day, - 9, T1.CancerBreachDate)
							then '5 day Breach'
						when T1.AppointmentDate is null and datediff(day, T1.WaitingStartDate, getdate()) > = 6
							then '10 Day Approaching'
						when T1.AppointmentDate is null and datediff(day, T1.WaitingStartDate, getdate()) > = 10
							then '14 Day Approaching'
							--Logic from cognos below - wrong
						--when T1.AppointmentDate > T1.CancerBreachDate
						--	then '14 day Breach'
						--when T1.AppointmentDate > dateadd(day, - 7, T1.CancerBreachDate)
						--	then '10 day Breach'
						--when T1.AppointmentDate > dateadd(day, - 9, T1.CancerBreachDate)
						--	then '5 day Breach'
						--when T1.AppointmentDate is null and datediff(day, T1.WaitingStartDate, getdate()) > = 6
						--	then '10 Day Approaching'
						--when T1.AppointmentDate is null and datediff(day, T1.WaitingStartDate, getdate()) > = 10
						--	then '14 Day Approaching'
						--when T1.AppointmentDate > dateadd(day, - 4, T1.CancerBreachDate)
						--	then '10 day Breach'
					end 
			end
		--,CensusDate
								
	from WH.PTL.OPWaitingList T1
	where T1.WLPriority = '2 Week Rule'
	and T1.VisitType <> 'Follow Up'
)hsc
order by DayWait desc