﻿CREATE procedure [PTL].[ReportDailyMonthlyAllBreaches_Specialty] 

@StartDate		as Date
, @EndDate		as Date 
,@Spec varchar (1000)
as
-- =============================================
-- Author:		JPotluri
-- Create date: 20/03/2014
-- Description:	PTL Report Daily Monthly All Breachesby specialty
--				Log Number : RD460

-- =============================================
/*
Created Modified Date		Modified By							Comments
---------------------		------------------------		------------------
12/08/2014					KO								change data source to RTT_DW
*/
SET NOCOUNT ON;

SELECT [DataSource]
      ,[Specialty]
      ,[Consultant]
      ,[FacilID]
      ,[PatientSurname]
      ,[PerfType]
      ,[ReferralDate]
      ,[WeekendFullWeeks]
      ,[BreachDate]
      ,[ClockStopDate]
      ,[ClockStopType]
      ,[RefCompComment]
      ,[BreachType]
      ,[ReferralID]
      ,[CensusDate]
--FROM [v-uhsm-dw02].[RTT].[PTL].[MonthlyAllBreaches]
FROM [RTT_DW].[PTL].[MonthlyAllBreaches]

where

cast(ClockStopDate as DATE) between @StartDate and @EndDate
AND
 [Specialty] in (SELECT Item
                        FROM   dbo.Split (@Spec, ','))

Order by
Specialty