﻿-- =============================================
-- Author:		JPotluri
-- Create date: 20/03/2014
-- Description: To create Specialty dropdown list for SSRS report 'PTL - RTT Monthly All Breaches'
-- Associated procedure for the report is [PTL].ReportDailyMonthlyAllBreaches_Specialty
-- =============================================
/*
Created Modified Date		Modified By							Comments
---------------------		------------------------		------------------
12/08/2014					KO								change data source to RTT_DW
*/
CREATE PROCEDURE [PTL].[ReportDailyMonthlyAllBreaches_Specialty_DropdownList] 
	
AS
BEGIN
	
	SET NOCOUNT ON;
	
Select
Distinct
Specialty
--FROM [v-uhsm-dw02].[RTT].[PTL].[MonthlyAllBreaches]
FROM [RTT_DW].[PTL].[MonthlyAllBreaches]

Order by
Specialty

    
	
END