﻿-- =============================================
-- Author:		JPotluri
-- Create date: 24/03/2014
-- Description: To create Specialty dropdown list for SSRS report 'PTL].[ReportDailyNonAdmitted]'
-- Associated procedure for the report is [PTL].[ReportDailyNonAdmitted_Specialty]
-- =============================================
/*
Created Modified Date		Modified By							Comments
---------------------		------------------------		------------------
12/08/2014					KO								change data source to RTT_DW
*/
CREATE PROCEDURE [PTL].[ReportDailyNonAdmitted_LastAttendedAptStatus] 
	
AS
BEGIN
	
	SET NOCOUNT ON;
	
Select
Distinct
isnull([LastAttendedApptStatus],'Unknown')as LastAttendedApptStatus
--FROM [v-uhsm-dw02].[RTT].[PTL].[NonAdmitted]
FROM [RTT_DW].[PTL].[NonAdmitted]

Order by
[LastAttendedApptStatus]

    
	
END