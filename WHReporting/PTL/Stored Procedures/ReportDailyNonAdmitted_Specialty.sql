﻿-- =============================================									
-- Author:		JPotluri							
-- Create date: 24/03/2014									
-- Description:	[PTL].[ReportDailyNonAdmitted_Specialty]								
--				Log Number : RD461					
									
-- =============================================									
/*
Created Modified Date		Modified By							Comments
---------------------		------------------------		------------------
12/08/2014					KO								change data source to RTT_DW
*/

CREATE PROCEDURE [PTL].[ReportDailyNonAdmitted_Specialty] 
@Spec varchar (1000)
,@AptStatus Varchar (MAX)
as	
SELECT [Specialty]
      ,[DoctorSurname]
      ,[PatientID]
      ,[FacilityID]
      ,[PatientSurname]
      ,[ReferralDate]
      ,[RTTBreachDate]
      ,isnull([LastAttendedApptStatus],'Unknown') as LastAttendedApptStatus
      ,[LastAttendedApptDate]
      ,[NextApptDate]
      ,[RTTWeekWaitActual]
      ,[WeekWaitToAppt]
      ,[FirstAdmissionOutcome]
      ,[RefCompComment]
      ,[PathwayDelays]
      ,[RefUniqueID]
      ,[CensusDate]
      ,[ClinicCode]
--FROM [v-uhsm-dw02].[RTT].[PTL].[NonAdmitted]
FROM [RTT_DW].[PTL].[NonAdmitted]

where
 [Specialty] in (SELECT Item			
                        FROM   dbo.Split (@Spec, ','))
 AND
 (isnull([LastAttendedApptStatus],'Unknown') in (SELECT Item			
                        FROM   dbo.Split (@AptStatus, ',')))