﻿-- =============================================
-- Author:		<Jubeda Begum>
-- Create date: <16/10/2014>
-- Description:	<Runs SSRS Report Daily Waiting List Addition PTL>
-- =============================================
CREATE PROCEDURE [PTL].[ReportDailyWaitingListAdditions] 
	-- Add the parameters for the stored procedure here
@DateCreated DATETIME
,@Specialty VARCHAR(MAX)
,@CancerCode VARCHAR(MAX)

AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT *
FROM [RTT_DW].[PTL].[DailyWaitingListAdditions]
WHERE DateRecordCreated  >= (@DateCreated)
AND Specialty IN --(@Specialty)
(SELECT Item
                         FROM   dbo.Split (@Specialty, ',')) 
AND CancerCode IN --(@CancerCode)
(SELECT Item
                         FROM   dbo.Split (@CancerCode, ','))
ORDER BY DistrictNo
	
END