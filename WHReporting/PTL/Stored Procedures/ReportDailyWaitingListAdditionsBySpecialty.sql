﻿/*-----------------------------------------------------------------------------------------------------------
     -------------------------------
Purpose :  [PTL].[ReportDailyWaitingListAdditionsBySpecialty]  -  data for daily Waiting List Additions by Specialty SSRS Report     

Notes :    Stored in WHREPORTING.PTL
           This Procedure has been created for use in the Daily List Additions by specialty SSRS report in the location below
           S:\CO-HI-Information\New File Management - Jan 2011\Data Warehouse Project\SSRS Report Code\PTLs_Waiting_List_RTT\PTLReports
           
           This has been designed to populate the data for the daily waiting List admissions.
           
Version: 1.0.0.0 25/04/2016 - Peter Asemota        

  --------------------------------------------------
-----------------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [PTL].[ReportDailyWaitingListAdditionsBySpecialty]  
(
	 @DateCreated DATETIME
	,@Specialty  VARCHAR(MAX)
	,@Consultant VARCHAR(MAX)
    ,@CancerCode VARCHAR(MAX)
    ,@Weekending Varchar(MAX)
	)
	
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET DATEFORMAT DMY
    SET DATEFIRST 1
    
 /*  -- this is purely for testing purposes
 -- Add the parameters for the stored procedure here
 Declare   @DateCreated DATETIME = '01 Mar 2016'
          ,@Weekending Varchar(Max) = '06 Mar 2016,13 Mar 2016'
        --,@Specialty VARCHAR(MAX)
        --,@CancerCode VARCHAR(MAX)
--*/




    -- select statements for procedure here
 Select *
 from (   
    
SELECT Distinct 
       [Specialty] 
      ,DWLA.[Consultant]
      ,DWLA.[DistrictNo] As FacilityID
    --,ROW_NUMBER () OVER (PARTITION BY DWLA.[DistrictNo] Order By [Clock Start Date]) as RowID
      ,RTT.PatientName As FullName
      ,DWLA.[PatientSurname]As LastName
      ,RTT.NHSNumber
      ,DWLA.DateOnList
      ,[CancerCode]
      ,[Latest_RTT_Code]
      ,[DateRecordCreated]
      ,LTRIM(RTRIM(RTT.PlannedProcedure1)) AS [Procedure]
      ,MAX([Clock Start Date]) AS [Latest_ClockStartDate]
      ,Max(RTT.RTTBreachDate) As RTTBreachDate
      ,RTT.ELAdmission
      ,DATENAME(DW,DWLA.[DateRecordCreated]) As [Weekday]
      ,DATEPART(DW,DWLA.[DateRecordCreated]) As [Weekday_int]
      ,DATEADD(DAY, 1 - DATEPART(WEEKDAY, DWLA.[DateRecordCreated]), CAST(DWLA.[DateRecordCreated] AS DATE)) [WeekStart]
      ,DATEADD(DAY, 7 - DATEPART(WEEKDAY, DWLA.[DateRecordCreated]), CAST(DWLA.[DateRecordCreated]AS DATE)) [WeekEnd]
      ,CONVERT(Varchar (20),DATEADD(DAY, 7 - DATEPART(WEEKDAY, DWLA.[DateRecordCreated]), CAST(DWLA.[DateRecordCreated]AS DATE)),106) As [Weekending]
      ,1 as data
     
            

FROM [RTT_DW].[PTL].[DailyWaitingListAdditions] As DWLA
LEFT OUTER JOIN [RTT_DW].[PTL].[AdmittedWaitingListAll] RTT 
on DWLA.DistrictNo = RTT.DistrictNo
and DWLA.Specialty = RTT.SpecialtyDescription
and DWLA.[Clock Start Date] =RTT.ClockStartDate 
INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Specialty, ',') dir ON DWLA.[Specialty] = dir.SplitValue
INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Consultant, ',') Con ON DWLA.[Consultant] = Con.SplitValue
INNER JOIN DW_REPORTING.LIB.fn_SplitString(@CancerCode, ',') Can ON DWLA.[CancerCode] = Can.SplitValue
WHERE DateRecordCreated  >= (@DateCreated) 
     --and DWLA.[DistrictNo] in ('RM21678604')

group By 

      [Specialty] 
      ,DWLA.[Consultant]
      ,DWLA.[DistrictNo]
      ,RTT.PatientName  
      ,DWLA.[PatientSurname]
      ,RTT.NHSNumber
      ,DWLA.DateOnList
      ,[CancerCode]
      ,[Latest_RTT_Code]
      ,[DateRecordCreated]    
      ,RTT.PlannedProcedure1 
      ,RTT.ELAdmission
     
) as data 
INNER JOIN DW_REPORTING.LIB.fn_SplitString(@Weekending, ',') Wkd ON data.[Weekending] = Wkd.SplitValue    

--where data.[Weekending] IN (SELECT Item
--                         FROM   dbo.Split (@Weekending, ',')) 

ORDER BY data.[DateRecordCreated],data.FacilityID


END