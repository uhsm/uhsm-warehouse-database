﻿CREATE procedure [PTL].[ReportHSCWeekly] as
/*

----------------------------------------------------------------------------------------------------------------
Action		Date		Author		Comments
----------------------------------------------------------------------------------------------------------------

Created		06/10/2014	KO			(RD227)  PTL - replacing procedure 24 in NewExtracts
----------------------------------------------------------------------------------------------------------------
*/
select 

	sched.SpecialtyCode
	,sched.[Specialty(Function)]
	,sched.ProfessionalCarerCode
	,sched.ProfessionalCarerName
	,sched.AppointmentDate
	,sched.AppointmentTime
	,sched.NHSNo
	,pat.PatientSurname
	,sched.ClinicCode
	,sched.AppointmentType
	--,enc.PRITY_REFNO
from 
	OP.Schedule sched

left join OP.Patient pat
	on pat.SourceUniqueID = sched.SourceUniqueID
--Need to get priority code from Lorenzo
inner join Lorenzo.dbo.ScheduleEvent enc
	on enc.SCHDL_REFNO = sched.SourceUniqueID
		and enc.PRITY_REFNO = 5478 --2 week rule
where sched.AppointmentDate between dateadd(day, 1, datediff(day, 0,getdate()))
and dateadd(day, 7, datediff(day, 0,getdate()))
and sched.CancelledDateTime is null
order by ProfessionalCarerCode desc