﻿-- =============================================
-- Author:		<Jpotluri>
-- Create date: <30/09/2014>
-- Description:	<SSRS report for IPDCPlannedList-PLAdmissionTypeNOTPL>
-- =============================================
CREATE PROCEDURE [PTL].[ReportIPDCPlannedList-PLAdmissionTypeNOTPL]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT 
       sp.Specialty,
       con.ConsultantName,
       [DistrictNo],
       [PatientForename],
       [PatientSurname],
       [DateOfBirth],
       Specialty
       [SpecialtyCode],
       [Consultant],
       ListType,
       SuspToDate,
       18wkbreachdate,
       TCIDate
       
        ,CASE
         WHEN [IntendedManagement] = '1' THEN 'IP'
         WHEN [IntendedManagement] = '2' THEN 'DC'
         ELSE NULL
       END AS 'PatClass',
       [ElectiveAdmissionMethod],
       [DateOnList],
       [AdmitByDate],
     
       [PlannedProcedure],
       [GeneralComment],
       WL.EntryStatus,
       Isnull([GeneralComment], [PlannedProcedure])AS ProDesc,
       CASE
         WHEN [ElectiveAdmissionMethod] = '11' THEN 'WL'
         WHEN [ElectiveAdmissionMethod] = '12' THEN 'BL'
         WHEN [ElectiveAdmissionMethod] = '13' THEN 'PL'
         ELSE [ElectiveAdmissionMethod]
       END                                                     AS ElAd,
       Datediff(DAY, [DateOnList], Getdate())
       + [SuspToDatePtReason]                                  AS WkWaitNow,
       Dateadd(DAY, 14, [AdmitByDate])                         AS DateAddedToRTT,
       CASE
         WHEN [AdmitByDate] < Getdate()
              AND Dateadd(DAY, 14, [AdmitByDate]) < Getdate() THEN 'RTT'
         WHEN [AdmitByDate] < Getdate() THEN 'Review'
         ELSE NULL
       END                                                     AS Category,
       Dateadd(DAY, 140, [AdmitByDate])                        AS [1_RTTBreachDate],
       Dateadd(DAY, 140 + [SuspToDatePtReason], [AdmitByDate]) AS [2_RTTBreachDate],
       CASE
         WHEN CASE
                WHEN [AdmitByDate] < Getdate()
                     AND Dateadd(DAY, 14, [AdmitByDate]) < Getdate() THEN 'RTT'
                WHEN [AdmitByDate] < Getdate() THEN 'Review'
                ELSE NULL
              END = 'RTT'
              AND Dateadd(DAY, 140 + [SuspToDatePtReason], [AdmitByDate]) IS NOT NULL THEN Dateadd(DAY, 140 + [SuspToDatePtReason], [AdmitByDate])
         ELSE Dateadd(DAY, 140, [AdmitByDate]) + '23:59'
       END                                                     AS [3_RTTBreachDate],
       CASE
         WHEN CASE
                WHEN [AdmitByDate] < Getdate()
                     AND Dateadd(DAY, 14, [AdmitByDate]) < Getdate() THEN 'RTT'
                WHEN [AdmitByDate] < Getdate() THEN 'Review'
                ELSE NULL
              END = 'Review' THEN
           CASE
             WHEN [AdmitByDate] < Dateadd(day,Datediff(day,0,Getdate()),0)
                  AND Dateadd(DAY, 14, [AdmitByDate]) < Dateadd(day,Datediff(day,0,Getdate()),0) THEN 'RTT'
             WHEN [AdmitByDate] < Dateadd(day,Datediff(day,0,Getdate()),0) THEN 'Review'
             ELSE NULL
           END
         WHEN [AdmitByDate] < Dateadd(day,Datediff(day,0,Getdate()),0) THEN 'Overdue'
         WHEN [AdmitByDate] IS NULL THEN 'No Date'
         ELSE 'OK'
       END                                                     AS Overdue,
       Datediff(DAY, WaitingStartDate, Getdate())
       + Isnull([SuspToDatePtReason], 0)                       AS DayWaitNow,
       ( Datediff(DAY, WaitingStartDate, Getdate())
         + Isnull([SuspToDatePtReason], 0) ) / 7               AS WeekWaitNow,
         Dateadd(DAY, 14, [AdmitByDate])as '14 days',
         AdmissionWard,
          CASE
         WHEN (/*[AdmitByDate] + */Dateadd(DAY, 14, [AdmitByDate])) < Dateadd(day,Datediff(day,0,Getdate()),0) THEN 'Overdue'
         WHEN [AdmitByDate] < Dateadd(day,Datediff(day,0,Getdate()),0) THEN 'Review'
         WHEN [AdmitByDate] IS NULL THEN 'No Admit By Date'
         ELSE 'OK'
       END                                                     AS 'Comments'

FROM   [WH].[PTL].[IPWL]WL

Left Join [WH].[PTL].[IPWLWaitingStatus]S
ON WL.SourceUniqueID = S.SourceUniqueID

LEFT OUTER JOIN WHreporting.LK.SpecialtyDivision sp
                    ON WL.[SpecialtyCode] = sp.[SpecialtyCode]
       LEFT OUTER JOIN WHREPORTING.LK.ConsultantSpecialty con
                    ON WL.[Consultant] = con.ConsultantCode

Where
S.EntryStatus = 'WAITING'
and
[ElectiveAdmissionMethod] <> '13'
and
ListType like '%PL%'

END