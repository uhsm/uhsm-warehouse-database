﻿CREATE procedure [PTL].[ReportLegacyStatus]
	@StartDate datetime
	,@DQActionFlag varchar(500)
as
/*
--Author: K Oakden
--Date created: 13/03/2015
--Check daily additions to RTT legacy rules
--SSRS report code at S:\CO-HI-Information\New File Management - Jan 2011\Data Warehouse Project\SSRS Report Code\PTLs_Waiting_List_RTT\PTLReports\PTLReports\CheckRTTLegacyStatus.rdl
--LegacyAuditTrail updated as part of daily RTT build RT_DW.Build.LoadLegacyStatusAuditTrail
---------------------------------------------------------------------------------------------------
LegacyStatus set in following parts of RTT build...

--Step12  ->  DQACTION = 'LGNotRTT', 'LegacyStatus = 'Legacy - Referral >12 Months No Act'
-------------------------------------------------------------------------------------------
--for refs with no activity
--  Join RTTWithoutClockStops WHERE FIRSTSCHEDULE IS NULL AND FIRSTWAITINGLIST IS NULL AND FIRSTSPELL IS NULL

--Step14  ->  DQACTION = 'LGNotRTT', LegacyStatus = 'Legacy - Ref over 12 Months No Appts'
-------------------------------------------------------------------------------------------
--for refs with no OPD history
--  Legacy All appts where there are referrals over 12 months old and nextApptDate is null and PrevApptDate is null

--Step15  ->  DQACTION = 'LGNotRTT'
---------------------------------------
-->  DQACTION = 'LgNotRTT', LegacyStatus = 'PreJan07Referral'
--  Referral date < 01/01/2007 and no clock stop date

-->  DQACTION = 'LgNotRTT', LegacyStatus = 'Referral No Activity'
--  where the wait is >18 weeks and all activity fields are null (FirstApptDate is null, WLRefNo is null, NextApptDate is null, PrevApptDate is null, ClockStopDate is null)

-->  DQACTION = 'LgNotRTT', LegacyStatus = 'Duplicate Referral'
--  where there is < 10 days since the last referral to the same specialty

-->  DQAction = 'LgNotRTT', LegacyStatus = 'Subsequent Clock Stop'
--  where there is a subsequent Clock Stop for the same Specialty

-->  DQAction = 'LgNotRTT', LegacyStatus = 'More than 3 months since last appt and no future activity'
--  where there is a prevappt >3 months old and no nextappt and no wlrefno

*/



-- To improve query performance create tables from the parameters
-- SSRS passes the parameters a CSV strings, sometimes there can
-- be 30+ entries.  
if Object_id('tempdb..#DQAction', 'U') is not null
	drop table #DQAction
	
select dq = rtrim(ltrim(item))
into   #DQAction
from   WHREPORTING.dbo.Splitstrings_cte(@DQActionFlag, N',')

select distinct
	DistrictNo = rtt.pt_Facil_ID
	,PatientName = rtt.Patient_forename + ' '+ rtt.Patient_surname
	,PPID = rtt.PPID
	,ClockStopDate = dateadd(day, 0, datediff(day, 0,rtt.ClockStopDate))
	,rtt.ClockStopType
	--,rtt.DQAction
	,LatestRTTCode = rtt.Latest_RTT_Code
	,AdjClockDaysWE = rtt.ClockDaysWeekend
	,CurrentDQAction = audit.DQAction
	,CurrentLegacyStatus = rtt.LegacyStatus
	,DQActionAddedDate = audit.StatusAddedDate
	,RefID = rtt.Ref_ref
from RTT_DW.RTT.rtt_dataset rtt
inner join RTT_DW.Build.LegacyStatusAuditTrail audit
	on audit.ref_ref = rtt.Ref_ref
	and audit.Archived = 'N'
	and audit.StatusAddedDate > '1948-01-01 00:00:00.000'

	
--select 
--	ptl.DistrictNo
--	,ptl.PatientName
--	,ptl.PPID
--	,ptl.ClockStartDate
--	,ptl.ClockStartType
--	,ptl.DateOnList
--	,ptl.LatestRTTCode
--	,ptl.PTLComments
--	,ptl.RTTWaitNowWeeksUnAdj
--	,CurrentDQAction = audit.DQAction
--	,CurrentLegacyStatus = audit.LegacyStatus
--	,DQActionAddedDate = audit.StatusAddedDate
--	,RefID = ptl.ReferralSourceUniqueID
--from RTT_DW.PTL.admitted ptl
--inner join RTT_DW.Build.LegacyStatusAuditTrail audit
--	on audit.ref_ref = ptl.ReferralSourceUniqueID
--	and audit.Archived = 'N'
--	and audit.StatusAddedDate > '1948-01-01 00:00:00.000'
--	--and audit.DQAction = 'lgNotrtt'

inner join #DQAction dq
	on audit.DQAction collate database_default = DQ.dq collate database_default
	
where audit.DQAction like '%NotRTT'
and audit.StatusAddedDate >= @StartDate 

order by audit.StatusAddedDate desc;

if Object_id('tempdb..#DQAction', 'U') is not null
	drop table #DQAction
/**
--LegacyStatus lookup used in report
select distinct DQAction from RTT_DW.Build.LegacyStatusAuditTrail
where Archived = 'N'
and StatusAddedDate > '1948-01-01 00:00:00.000'
and DQAction like '%NotRTT'
**/