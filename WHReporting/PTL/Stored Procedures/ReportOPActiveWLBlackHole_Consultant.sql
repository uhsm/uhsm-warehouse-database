﻿/*
--Author: C Bell-Hartley / N Scattergood	
--Date created: 21/06/2013
--Stored Procedure Built for SRSS Report on:
--Outpatient PTL – Validation
--Parameter for Consultants
*/

CREATE Procedure [PTL].[ReportOPActiveWLBlackHole_Consultant]

@Specialty nvarchar (max)
--,@Consultant nvarchar (max)
as
select 
distinct
      ----ActiveWL.[WaitingListRuleRef]
      --ActiveWL.[WaitingListRuleCode]
      --,ActiveWL.[WaitingListRuleName]
      ----,ActiveWL.[TotalActiveOnList]
      --,ActiveWL.[MinDateOnList]
      --,ActiveWL.[WLSvcType]
      --,ActiveWL.[WLVisitType]
      --,ActiveWL.[WLPatientSurname]
      --,ActiveWL.[WLDistrictNo]
      ActiveWL.[WLClinicianCode] as ConsCode
      ,ActiveWL.[WLClinician] as ConsDesc
      --ActiveWL.[WLSpecialty] as SpecPar
      --,ActiveWL.[WLDateOnList]
      ----,ActiveWL.[WLSourceUniqueID]
      ----,ActiveWL.[WLSourceReferralID]
      ----,ActiveWL.[WLSourcePatientID]
      --,ActiveAppointment.CurrentAppointmentDate
      --,ActiveAppointment.CurrentAppointmentType
      --,ActiveAppointment.CurrentClinic
 
from WH.PTL.ActiveWaitingList ActiveWL
	left join WH.PTL.OPWL opptl
      on opptl.ReferralSourceUniqueID = ActiveWL.WLSourceReferralID
	left join 
      (select
            Appt.SourceUniqueID
            ,Appt.ReferralSourceUniqueID
            ,CurrentClinic = Appt.ClinicCode
            ,CurrentAppointmentDate = Appt.AppointmentDate
            ,CurrentAppointmentType = Visit.ReferenceValue
     
      from WH.OP.Encounter Appt
      left join WH.PAS.ReferenceValue Visit 
            on Visit.ReferenceValueCode = Appt.FirstAttendanceFlag
      where 
      Appt.AppointmentCancelDate is null
      and	
      Appt.AppointmentStatusCode = 45 --ATTND Not Specified
      )	ActiveAppointment
      on ActiveAppointment.ReferralSourceUniqueID = ActiveWL.WLSourceReferralID

where 
opptl.ReferralSourceUniqueID is null
and 
ActiveWL.WLVisitType <> 'Follow Up'
and 
ActiveWL.WLSvcType = 'Outpatient service'

----Following comes from the parameter---
and 
ActiveWL.[WLSpecialtyCode] in (SELECT Val from dbo.fn_String_To_Table(@Specialty,',',1)) 
--and
--ActiveWL.[WLClinician] in (SELECT Val from dbo.fn_String_To_Table(@Consultant,',',1))
      
order by 
ActiveWL.[WLClinician]
--ActiveWL.[WLSpecialty]