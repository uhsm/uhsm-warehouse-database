﻿-- =============================================
-- Author:		CMan
-- Create date: 27/01/2014
-- Description:	Stored Procedures created to support report PBFU Full Waiting List Summary by Book By Status
--				RD455
-- =============================================
CREATE PROCEDURE [PTL].[ReportOPPartialBookingFUBookByBreachDate_SummaryOLD] @BookingStatus varchar(20), @BookByStatus varchar(20)
	-- Add the parameters for the stored procedure here


	

/*
 Modified Date				Modified By			Modification
-----------------			------------		------------
09/06/2014					CMan				amended the logic so that the BookingStatus (red, amber, green) also takes into account the adjusted book by breach date rather than the original book by breach date - as per CBH request



*/




AS
BEGIN

Declare @date as datetime
Set @date = GETDATE()


/*************************************
--this set of temp tables are to help with identifying wl entries which have had a appointment which has been cancelled by patient so that the waiting list start and tolerance dates can be reset
--The logic has been taken from stored procedure WH.[PTL].[BuildOPWaitingListPartialBooking]
**************************************/

-- Temp table to identify all waiting list entries which have had a rescheduled appointment
If OBJECT_ID ('tempdb..#WL_RESCHED') > 0
DROP TABLE #WL_RESCHED

SELECT
WLIST_REFNO,
MAX(CAST(old_start_dttm AS DATETIME)) AS RESET_DATE
INTO #WL_RESCHED
FROM Lorenzo.dbo.ScheduleHistory
WHERE 
MOVRN_REFNO IN (
	'383',
	'3248',
	'4520',
	'2003539',
	'2003540',
	'2003541',
	'2003544',
	'2004148')--these are the same codes as used in the stored procedure above

--MOVRN_REFNO_MAIN_CODE = 'PT'
AND ARCHV_FLAG = 'N' AND (NEW_START_DTTM > OLD_START_DTTM) AND NOT WLIST_REFNO IS NULL
--AND CAST(OLD_START_DTTM AS DATETIME) < GETDATE()
GROUP BY WLIST_REFNO 

--this temp table identifies all waiting lists which have an appointment which has been cancelled by patient or DNA - this is different from those which have been rescheduled

If OBJECT_ID ('tempdb..#WL_RESETS') > 0
DROP TABLE #WL_RESETS

select wlist_refno, max(CAST(start_dttm AS DATETIME)) as 'Reset_date'
into #WL_RESETS
from Lorenzo.dbo.ScheduleEvent
where attnd_refno 
in ('2868', '358', '2870', '2004301', '2000724', '358', '2003494', '2003495') 
and archv_flag = 'n' 
AND NOT WLIST_REFNO IS NULL
--AND CAST(START_DTTM AS DATETIME) <GETDATE()
group by wlist_refno


--Insert the rescheuled temp table into the cncelled appointments temp  table to join the two together into one dataset
INSERT INTO  #WL_RESETS
SELECT *
FROM #WL_RESCHED



--Create final temp table with a ToleranceResetDate, BookByBreachDate-ToleranceStartReset and an adjusted BookByBreachDate field
-- ToleranceResetDate is the date of the leatest cancelled/DNA appointment on the waiting list
-- BookByBreachDate-ToleranceStartReset is the ToleranceResetDate plus the appropriate number of weeks of tolerance allowed based on the lead time 
-- AdjustedBookByBreachDate --this will be the book by breach date is there have been no cancelled/DNAs, or if the BookByBreachDate-ToleranceStartReset date is before the original BookByBreachDate.  Otherwise the 'BookByBreachDate-ToleranceStartReset' is used.
--Final Temp table to be used in subsequent Select Query below for summing up easier to read

If OBJECT_ID ('tempdb..#Final') > 0
DROP TABLE #Final
SELECT [WLSpecialty],
       [Consultant],
       [PatientID],
       [PatientSurname],
       [ReferralDate],
       [WaitingStartDate],
        Reset.RESET_DATE as ToleranceResetDate,--this is the date of any subsequent latest appointment cancelled/DNA's by patient.  Book by Breach date is to use this date in calcultions for any patients who have cancelled or DNA'd their appointment
       [WLClinicCode],
       [ApptClinicCode],
       [VisitType],
       [DaysWait],
       [WeeksWait],
       [InviteDate],
       [LeadTimeWeeks],
       [ProjectedApptDate],--this is based on the original DateOnList plus lead time
       [AppointmentDate],
       --[AppointmentDate],
       [BookByBreachDate],--Original Booked By Breach Date
          Case when Reset.RESET_DATE IS NOT NULL then 
					case when LeadTimeWeeks >= 52 then convert(varchar(11),DateAdd(week, 12, Reset.RESET_DATE),106)
			when LeadTimeWeeks >= 26 and LeadTimeWeeks < 52 then convert(varchar(11),DATEADD(week, 4, Reset.RESET_DATE),106)
			when LeadTimeWeeks >= 12 and LeadTimeWeeks < 26  then convert(varchar(11),DATEADD(week, 3, Reset.RESET_DATE),106)
			Else [BookByBreachDate] End 
	Else [BookByBreachDate]
	 End as [BookByBreachDate-ToleranceStartReset],
	         Case when
       Case when Reset.RESET_DATE IS NOT NULL then 
					case when LeadTimeWeeks >= 52 then convert(varchar(11),DateAdd(week, 12, Reset.RESET_DATE),106)
			when LeadTimeWeeks >= 26 and LeadTimeWeeks < 52 then convert(varchar(11),DATEADD(week, 4, Reset.RESET_DATE),106)
			when LeadTimeWeeks >= 12 and LeadTimeWeeks < 26  then convert(varchar(11),DATEADD(week, 3, Reset.RESET_DATE),106)
			Else [BookByBreachDate]  End 
	Else [BookByBreachDate]
	 End <  [BookByBreachDate] then  [BookByBreachDate] Else 
	 Case when Reset.RESET_DATE IS NOT NULL then 
					case when LeadTimeWeeks >= 52 then convert(varchar(11),DateAdd(week, 12, Reset.RESET_DATE),106)
			when LeadTimeWeeks >= 26 and LeadTimeWeeks < 52 then convert(varchar(11),DATEADD(week, 4, Reset.RESET_DATE),106)
			when LeadTimeWeeks >= 12 and LeadTimeWeeks < 26  then convert(varchar(11),DATEADD(week, 3, Reset.RESET_DATE),106)
			Else [BookByBreachDate]  End 
	Else [BookByBreachDate]
	 End 
	 End 
	  as AdjustedBookByBreachDate,
       [WLGeneralComment],
       [WLName],
       [ReferralStatus],
       [CreateDate],
       [CreatedByUser],
       [BookingStatus],
       [InviteStatus],
       [OverdueWks],
       [DateOnList],
       [NumberOfFUPatientCancellations],
       [NumberOfFUHospitalCancellations],
       [NumberOfDNAs],
       [HSC],
       --CASE
       --  WHEN COALESCE(AppointmentDate, @date) >= DateOnList
       --       AND COALESCE(AppointmentDate, @date) < ProjectedApptDate THEN 'Green'
       --  WHEN COALESCE(AppointmentDate, @date) >= ProjectedApptDate
       --       AND COALESCE(AppointmentDate, @date) < BookByBreachDate THEN 'Amber'
       --  WHEN COALESCE(AppointmentDate, @date) >= BookByBreachDate THEN 'Red'
       --  ELSE NULL
       --END            AS BookByStatus,
       1              AS Value,
       DataLastUpdated = (SELECT Max(eventtime)
                          FROM   WH.dbo.AuditLog
                          WHERE  ProcessCode = 'PAS - WH BuildOPWaitingListPartialBooking')--09/06/2014 CM : added as per CBH request to give indication of when the data extracts were last updated 
       
into #Final
FROM   WH.[PTL].[ReportFUPB]
left outer join (SELECT WLIST_REFNO,MAX(RESET_DATE) AS RESET_DATE FROM #WL_RESETS
							GROUP BY WLIST_REFNO) Reset --grab the latest cancelled/rescheduled/DNA's appointment from the temp table above - this latest cancelled/resr appointment will be the WL reset date
		on [ReportFUPB].SourceWaitingListID = Reset.WLIST_REFNO

--Final Query
SELECT [WLSpecialty],
       CASE
         WHEN COALESCE(AppointmentDate, @date) >= DateOnList
              AND COALESCE(AppointmentDate, @date) < ProjectedApptDate THEN 'Green'
         WHEN COALESCE(AppointmentDate, @date) >= ProjectedApptDate
              AND COALESCE(AppointmentDate, @date) < AdjustedBookByBreachDate THEN 'Amber'
         WHEN COALESCE(AppointmentDate, @date) >= AdjustedBookByBreachDate THEN 'Red'
         ELSE NULL
       END     AS BookByStatus,
      DataLastUpdated ,
       Count(*)AS Value
FROM   #final
WHERE  BookingStatus IN (SELECT Item
                         FROM   dbo.Split (@BookingStatus, ','))
       AND CASE
             WHEN COALESCE(AppointmentDate, @date) >= DateOnList
                  AND COALESCE(AppointmentDate, @date) < ProjectedApptDate THEN 'Green'
             WHEN COALESCE(AppointmentDate, @date) >= ProjectedApptDate
                  AND COALESCE(AppointmentDate, @date) < AdjustedBookByBreachDate THEN 'Amber'
             WHEN COALESCE(AppointmentDate, @date) >= AdjustedBookByBreachDate THEN 'Red'
             ELSE NULL
           END IN (SELECT Item
                   FROM   dbo.Split (@BookByStatus, ','))
GROUP  BY [WLSpecialty],
          CASE
            WHEN COALESCE(AppointmentDate, @date) >= DateOnList
                 AND COALESCE(AppointmentDate, @date) < ProjectedApptDate THEN 'Green'
            WHEN COALESCE(AppointmentDate, @date) >= ProjectedApptDate
                 AND COALESCE(AppointmentDate, @date) < AdjustedBookByBreachDate THEN 'Amber'
            WHEN COALESCE(AppointmentDate, @date) >= AdjustedBookByBreachDate THEN 'Red'
            ELSE NULL
          END,
          DataLastUpdated 



END