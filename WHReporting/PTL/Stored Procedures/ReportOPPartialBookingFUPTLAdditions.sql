﻿/*
--Author: C Bell-Hartley
--Date created: 10/04/2013
--Stored Procedure Built for SRSS Report on:
--Outpatient Partial Booking Follow-up Waiting List - ADDITIONS >=104 WEEKS
--This provides data for Partial Booking PTL
*/

CREATE Procedure [PTL].[ReportOPPartialBookingFUPTLAdditions]

as

SELECT [WLSpecialty]
      ,[Consultant]
      ,[PatientID]
      ,[PatientSurname]
      ,[ReferralDate]
      ,[WaitingStartDate] 
      ,[WLClinicCode]
      ,[ApptClinicCode]  
      ,[VisitType]
      ,[DaysWait]
      ,[InviteDate]
      ,[LeadTimeWeeks]
      ,[ProjectedApptDate]
      ,[AppointmentDate]
      ,[WLGeneralComment]
      ,[WLName]
      ,[ReferralStatus]
      ,CreateDate
      ,CreatedByUser
      
   ,case when appointmentdate <> '1 apr 1998' then 'Booked' else 'UnBooked'
       end as [BookingStatus]
   ,case when InviteDate < getdate() then 'Overdue' else 'OK' end as [InviteStatus]
    
   ,case when (DATEDIFF(WW,[ProjectedApptDate], GETDATE()) ) <0 then 0 else (DATEDIFF(WW,[ProjectedApptDate], GETDATE())) 
   end  as [OverdueWks]
      
     FROM [WH].[PTL].[WaitingListPartialBooking]
     
     WHERE CreateDate >= (GETDATE()-8)
     and
     LeadTimeWeeks >=104
     
     ORDER BY CreateDate, CreatedByUser