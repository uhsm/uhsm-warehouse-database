﻿-- =============================================
-- Author:		CMan
-- Create date: 29/05/2014
-- Description:	Stored Procedure for SSRS report which identifies all those records on the OP Partial Booking FU Waiting List which have an appointment booked with a different specilaity
--				RD472
-- =============================================
CREATE PROCEDURE [PTL].[ReportOPPartialBookingFUPTLDifferentApptSpecialty] @WLSpecialty varchar(max)


 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   
 Select WLSpecialty,
		ApptSpecialty,
		SpecCheck = Case when WLSpecialty <> ApptSpecialty then 'DiffSpec' Else 'OK' End, 
		Consultant,
		PatientID,
		PatientSurname,
		ReferralDate,
		WaitingStartDate,
		WLClinicCode,
		ApptClinicCode,
		AppointmentDate,
		VisitType,
		DaysWait,
		InviteDate,
		LeadTimeWeeks,
		ProjectedApptDate,
		WLGeneralComment,
		WLName,
		ReferralStatus,
		CreateDate,
		CreatedByUser,
		CASE
		 WHEN appointmentdate <> '1 apr 1998' THEN 'Booked'
		 ELSE 'UnBooked'
		END AS BookingStatus,
		CASE
		 WHEN InviteDate < Getdate() THEN 'Overdue'
		 ELSE 'OK'
		END AS InviteStatus,
		CASE
		 WHEN ( Datediff(WW, [ProjectedApptDate], Getdate()) ) < 0 THEN 0
		 ELSE ( Datediff(WW, [ProjectedApptDate], Getdate()) )
		END AS OverdueWks

 from  [WH].[PTL].[WaitingListPartialBooking]
 where WLSpecialty <> ApptSpecialty
 and WLSpecialty in (SELECT Item
                         FROM   dbo.Split (@WLSpecialty, ',')) 
    
    
END