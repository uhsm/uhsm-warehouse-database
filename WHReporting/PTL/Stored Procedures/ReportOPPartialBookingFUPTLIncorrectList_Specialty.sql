﻿/*
--Author: N Scattergood
--Date created: 24/07/2013
--Stored Procedure Built for SRSS Report on:
--Outpatient Partial Booking FU PTL – validation (incorrect list name)
--This provides values for the Parameters
*/

Create Procedure [PTL].[ReportOPPartialBookingFUPTLIncorrectList_Specialty]
--@SpecPar		as nvarchar(4000)
--,@User			as nvarchar(4000)
as

SELECT 
Distinct
[WLSpecialty] as SpecPar
      --,[Consultant]
      --,[PatientID]
      --,[PatientSurname]
      --,[ReferralDate]
      --,[WaitingStartDate] 
      --,[WLClinicCode]
      --,[ApptClinicCode]  
      --,[VisitType]
      --,[DaysWait]
      --,[Wk Wait]=  (DATEDIFF(DAY,WL.[WaitingStartDate] ,GETDATE())/ 7)
      --,[InviteDate]
      --,[LeadTimeWeeks]
      --,[ProjectedApptDate]
      --,[AppointmentDate]
      --,[WLGeneralComment]
      --,[WLName]
      --,[ReferralStatus]
      --,CreateDate
      --,CreatedByUser
      
	--,case 
	--when appointmentdate <> '1 apr 1998' 
	--then 'Booked' 
	--else 'UnBooked'
 --      end as [BookingStatus]
 --  ,case 
 --  when InviteDate < getdate() 
 --  then 'Overdue' 
 --  else 'OK' 
 --  end as [InviteStatus]
    
 --  ,case when (DATEDIFF(WW,[ProjectedApptDate], GETDATE()) ) <0 
 --  then 0 
 --  else (DATEDIFF(WW,[ProjectedApptDate], GETDATE())) 
 --  end  as [OverdueWks]
      
     FROM [WH].[PTL].[WaitingListPartialBooking] WL
     
     where 
     WLName <> 'Follow Up Outpatient Waiting List'
   
     --Following come from Parameters
    -- and 
    -- [WLSpecialty] in	(SELECT Val from dbo.fn_String_To_Table(@SpecPar,',',1))
    -- and
    --[CreatedByUser] in  (SELECT Val from dbo.fn_String_To_Table(@User,',',1))
     
     
     
     Order by
     [WLSpecialty]