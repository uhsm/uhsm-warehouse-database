﻿/*
--Author: C Bell-Hartley
--Date created: 10/04/2013
--Stored Procedure Built for SRSS Report on:
--Outpatient Partial Booking Follow-up Waiting List Summary count
--This provides data for Partial Booking PTL
*/

CREATE Procedure [PTL].[ReportOPPartialBookingFUPTLSummary]

as

SELECT [WLSpecialty]
      
   ,case 
   when (DATEDIFF(WW,[ProjectedApptDate], GETDATE()) ) <0 
   then '0'
   
   when (DATEDIFF(WW,[ProjectedApptDate], GETDATE()) ) >17 
    then '18+'
    
   else cast(
   (DATEDIFF(WW,[ProjectedApptDate], GETDATE())) as varchar(2))
   end  as [OverdueWks]
   
   ,case when (DATEDIFF(WW,[ProjectedApptDate], GETDATE()) ) <0 then 0 else (DATEDIFF(WW,[ProjectedApptDate], GETDATE())) 
   end  as [OverdueWksSort]
   
     ,COUNT(1) as datacount
      
     FROM [WH].[PTL].[WaitingListPartialBooking]
     where case when (DATEDIFF(WW,[ProjectedApptDate], GETDATE()) ) <0 then 0 else (DATEDIFF(WW,[ProjectedApptDate], GETDATE())) 
   end <>0
   
   GROUP BY [WLSpecialty]
      
   ,case 
   when (DATEDIFF(WW,[ProjectedApptDate], GETDATE()) ) <0 
   then '0'
   
   when (DATEDIFF(WW,[ProjectedApptDate], GETDATE()) ) >17 
    then '18+'
    
   else cast(
   (DATEDIFF(WW,[ProjectedApptDate], GETDATE())) as varchar(2))
   end  
   
   ,case when (DATEDIFF(WW,[ProjectedApptDate], GETDATE()) ) <0 then 0 else (DATEDIFF(WW,[ProjectedApptDate], GETDATE())) 
   end  
   
   order by OverdueWks