﻿/*
--Author: N Scattergood
--Date created: 27/06/2013
--Stored Procedure Built for SRSS Report on:
--Outpatient Partial Booking Follow-up Waiting List Summary count
--This provides drill though data for the PBFU_Summary report
*/

CREATE Procedure [PTL].[ReportOPPartialBookingFUPTLSummary_Drillthrough]

	@SpecPar		as nvarchar(255) = null
,	@WksPar			as nvarchar(3) = null
as
SELECT     
WLSpecialty
, Consultant
, PatientID
, PatientSurname
, ReferralDate
, WaitingStartDate
, WLClinicCode
, ApptClinicCode
, VisitType
, DaysWait
, InviteDate
, LeadTimeWeeks
, ProjectedApptDate
, AppointmentDate
, WLGeneralComment
, WLName
, ReferralStatus
, CreateDate
, CreatedByUser
,CancerDiagnosis
,CancerSite
, CASE 
	WHEN appointmentdate <> '1 apr 1998' 
	THEN 'Booked' 
	ELSE 'UnBooked' 
	END AS BookingStatus
	, CASE 
	WHEN InviteDate < getdate() 
	THEN 'Overdue' 
	ELSE 'OK' 
	END AS InviteStatus
	, CASE 
	WHEN (DATEDIFF(WW, [ProjectedApptDate], GETDATE())) < 0 
	THEN 0 
	ELSE (DATEDIFF(WW, [ProjectedApptDate], GETDATE())) 
	END AS OverdueWks
	
	, case 
   when (DATEDIFF(WW,[ProjectedApptDate], GETDATE()) ) <0 
   then '0'
   
   when (DATEDIFF(WW,[ProjectedApptDate], GETDATE()) ) >17 
    then '18+'
    
   else cast(
   (DATEDIFF(WW,[ProjectedApptDate], GETDATE())) as varchar(2))
   end  as WkWait

	FROM         [WH].[PTL].[WaitingListPartialBooking]
		
		WHERE  
		case 
		when (DATEDIFF(WW,[ProjectedApptDate], GETDATE()) ) <0 
		then 0 
		else (DATEDIFF(WW,[ProjectedApptDate], GETDATE())) 
		end <> 0
		and
		-------Following comes from Paramters-------
		   
	(
	(WLSpecialty IN (@SpecPar))
		or
	(@SpecPar is Null)
	)
	and
	
	(
	(case 
   when (DATEDIFF(WW,[ProjectedApptDate], GETDATE()) ) <0 
   then '0'
   
   when (DATEDIFF(WW,[ProjectedApptDate], GETDATE()) ) >17 
    then '18+'
    
   else cast(
   (DATEDIFF(WW,[ProjectedApptDate], GETDATE())) as varchar(2))
   end = @WksPar)
		or
	(@WksPar is null)
	)


Order by PatientSurname