﻿/*
--Author: N Scattergood	
--Date created: 08/07/2013
--Modified J POTLURI on 22/05/2014, Added new Clinic code 'ASSOO'
--Stored Procedure Built for SRSS Report on:
--Outpatient Pulse Oximetry Diagnostic PTL 
*/

CREATE Procedure [PTL].[ReportOPPulseOxDiagnosticPTL]
as
select DISTINCT
	CensusDate
	,WL.ReferralSourceUniqueID
	--,WL.ReferredToSpecialty
	,WL.ReferredToConsultant
	,WL.CurrentClinic
	,WL.PatientId
	,WL.PatientSurname
	,case 
	when WL.CurrentAppointmentDate is null
	then 'NonBooked'
	else 'Booked'
	end as BookingStatus
	,WL.ReferralStatus
	,WL.ReferralSource
	
	--,WL.ReferralPriority
	--,WL.RTTReferralDate
	--,WL.CABAdjustedReferralDate
	--,RTTWeekWaitNow = ROUND(DATEDIFF(DAY,RTTReferralDate,GETDATE())/ cast(7 as float),0)
	--,RTTWaitToAppt = DATEDIFF(WEEK,WL.RTTReferralDate,WL. CurrentAppointmentDate)
	,WL.WaitingListStartDate
	,[Wk Wait]=  (DATEDIFF(DAY,WL.WaitingListStartDate,GETDATE())/ 7)
	,[BookBy6WkBreach] = WL.WaitingListStartDate+ 21
	,[6WkBreach] = WL.WaitingListStartDate + 42
	,WL.CurrentAppointmentDate
	,case 
	when WL.CurrentAppointmentDate is null
	then 'No Slot Date'
	when WL.CurrentAppointmentDate > (WL.RTTReferralDate + 42)
	then '6wk Breach'
	else 'OK'
	end as FinalComments

	--,WL.ActivityType
	--,WL.NumberOfHospitalCancellations
	--,WL.NumberOfPatientCancellations
	--,WL.NumberOfPatientReschedules
	--,WL.NumberOfDNAs
	--,WL.ReferringPCT
	--,WL.FirstAppointmentStatus

from WH.PTL.OPWL WL

	left join 
      (select
            Appt.SourceUniqueID
            ,Appt.ReferralSourceUniqueID
            ,CurrentClinic = Appt.ClinicCode
            ,CurrentAppointmentDate = Appt.AppointmentDate
            ,CurrentAppointmentType = Visit.ReferenceValue
     
      from WH.OP.Encounter Appt
      left join WH.PAS.ReferenceValue Visit 
            on Visit.ReferenceValueCode = Appt.FirstAttendanceFlag
      where 
      Appt.AppointmentCancelDate is null
      and	
      Appt.AppointmentStatusCode = 45 --ATTND Not Specified
      )	ActiveAppointment
      on ActiveAppointment.ReferralSourceUniqueID = WL.ReferralSourceUniqueID


--where FirstAppointmentStatus = COALESCE(@FirstAppt, FirstAppointmentStatus)
where 
WL.CurrentClinic in ('ASSTP1','ASSTP5','ASSOC','ASSOO' )
and 
ActiveAppointment.CurrentAppointmentType <> 'Follow Up'


order by
WL.WaitingListStartDate asc