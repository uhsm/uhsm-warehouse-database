﻿/*
--Author: K Oakden
--Date created: 04/11/2012
SP to pull OP Waiting list Report data from master OP PTL table
Set @FirstAppt parameter to 'Undated' for undated report (where no future booked appointment), 
to 'Booked' for appointments due and leave as null for all

ie
exec WHREPORTING.PTL.ReportOPWaitingList @FirstAppt = NULL
exec WHREPORTING.PTL.ReportOPWaitingList @FirstAppt = 'Undated'
exec WHREPORTING.PTL.ReportOPWaitingList @FirstAppt = 'Booked'
*/
CREATE procedure [PTL].[ReportOPWaitingList]
	--@FirstAppt varchar(10)
	@ReferredToSpecialty varchar(255)
	,@ActivityType varchar(80)
	,@ReferredToConsultant varchar(62)
	,@CurrentClinic varchar(25)
	,@FirstAppointmentStatus varchar(7)
as

select DISTINCT
	CensusDate
	,ReferralSourceUniqueID
	,ReferredToSpecialty
	,ReferredToConsultant
	,PatientId
	,PatientSurname
	,ReferralPriority
	,RTTReferralDate
	,CABAdjustedReferralDate
	,RTTWeekWaitNow = ROUND(DATEDIFF(DAY, RTTReferralDate, GETDATE())/ cast(7 as float),0)
	,RTTWaitToAppt = DATEDIFF(WEEK, RTTReferralDate, CurrentAppointmentDate)
	,CurrentClinic
	,CurrentAppointmentDate
	,[RTTBreachDate] = RTTReferralDate + 126
	,ReferralStatus
	,ActivityType
	,ReferralSource
	,NumberOfHospitalCancellations
	,NumberOfPatientCancellations
	,NumberOfPatientReschedules
	,NumberOfDNAs
	,ReferringPCT
	,FirstAppointmentStatus

from WH.PTL.OPWL
--where FirstAppointmentStatus = COALESCE(@FirstAppt, FirstAppointmentStatus)
where 
--FirstAppointmentStatus in ('Undated', 'Booked')
ReferredToSpecialty in (@ReferredToSpecialty)
and ActivityType in (@ActivityType)
and ReferredToConsultant in (@ReferredToConsultant)
and CurrentClinic in (@CurrentClinic)
and FirstAppointmentStatus in (@FirstAppointmentStatus)

order by ReferralSourceUniqueID desc