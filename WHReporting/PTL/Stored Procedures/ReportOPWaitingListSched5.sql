﻿/*
--Author: K Oakden
--Date created: 04/11/2012
SP to pull OP Waiting list Report - Schedule 5 Contract data from master OP PTL table
--Is FirstAppointmentStatus filter required?
*/
CREATE procedure [PTL].[ReportOPWaitingListSched5]
as
select 
	CensusDate
	,[ReferralSourceUniqueID]
	,[ReferredToSpecialty]
	,[ReferredToConsultant]
	,[PatientId]
	,[PatientSurname]
	,[ReferralPriority]
	,[RTTReferralDate]
	,[CABAdjustedReferralDate]
	,RTTWeekWaitNow = ROUND(DATEDIFF(DAY, [RTTReferralDate], GETDATE())/ cast(7 as float),0)
	,RTTWaitToAppt = DATEDIFF(WEEK, [RTTReferralDate], [CurrentAppointmentDate])
	,[CurrentClinic]
	,[CurrentAppointmentDate]
	,[RTTBreachDate]
	,[ReferralStatus]
	,[ActivityType]
	,[ReferralSource]
	,[NumberOfHospitalCancellations]
	,[NumberOfPatientCancellations]
	,[NumberOfPatientReschedules]
	,[NumberOfDNAs]
	,[ReferringPCT]
	,[Sched5_DateOfBirth]
	,[Sched5_Age]
	,[Sched5_Sex]
	,[Sched5_ReferringGpCode]
	,[Sched5_ReferringPracticeCode]
	,[Sched5_SourceOfReferralCode]
	,[Sched5_Site]
	,[Sched5_PPID]
	,[Sched5_CCG]
	,[Sched5_Ethnicity]
	,[Sched5_UBRN]
	,[Sched5_RTTCurrentStatusCode]
	,[Sched5_ConsultantCode]
	,[Sched5_SpecialtyCode]
	,[Sched5_Provider]
	,[Sched5_RegisteredGpCode]
	,[Sched5_RegisteredGpPracticeCode]
	,[Sched5_RegisteredPCTCode]
from WH.PTL.OPWL
order by ReferralSourceUniqueID desc