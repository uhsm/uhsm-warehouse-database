﻿/*
--Author: K Oakden
--Date created: 04/11/2012
SP to pull OP Waiting List Summary data from master OP PTL table
Set @FirstAppt parameter to 'Undated' for undated report (where no future booked appointment), 
to 'Booked' for appointments due and leave as null for all

ie
exec WHREPORTING.PTL.ReportOPWaitingListSummary @FirstAppt = NULL
exec WHREPORTING.PTL.ReportOPWaitingListSummary @FirstAppt = 'Undated'
exec WHREPORTING.PTL.ReportOPWaitingListSummary @FirstAppt = 'Booked'

*/
CREATE procedure [PTL].[ReportOPWaitingListSummary]
	@FirstAppt varchar(10)
as

select 
	ReferredToSpecialty
	,WkWait = case when RTTWeekWaitNow >= 18 then '>=18'
	else cast(RTTWeekWaitNow as varchar)
	end
	,PatientCount = count(ReferralSourceUniqueID)
from 
	(select distinct
		ReferralSourceUniqueID
		,ReferredToSpecialty
		,ReferredToConsultant
		,PatientId
		,PatientSurname
		,ReferralPriority
		,RTTReferralDate
		,CABAdjustedReferralDate
		,RTTWeekWaitNow = ROUND(DATEDIFF(DAY, RTTReferralDate, GETDATE())/ cast(7 as float),0)
		--,RTTWaitToAppt = DATEDIFF(WEEK, RTTReferralDate, CurrentAppointmentDate)
		,CurrentClinic
		,CurrentAppointmentDate
		,[RTTBreachDate] = RTTReferralDate + 126
		,ReferralStatus
		,ActivityType
		,ReferralSource
		,NumberOfHospitalCancellations
		,NumberOfPatientCancellations
		,NumberOfPatientReschedules
		,NumberOfDNAs
		,ReferringPCT
		,FirstAppointmentStatus
		--ReferralSourceUniqueID
		--,ReferredToSpecialty
		--,RTTWeekWaitNow = ROUND(DATEDIFF(DAY, RTTReferralDate, GETDATE())/ cast(7 as float),0)
	from WH.PTL.OPWL
	where FirstAppointmentStatus = COALESCE(@FirstAppt, FirstAppointmentStatus)
	) opwldata
group by       
	ReferredToSpecialty
	,case when RTTWeekWaitNow >= 18 then '>=18'
	else cast(RTTWeekWaitNow as varchar)
	end
--ORDER BY ReferredToSpecialty, WkWait