﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Open Referrals By Category (SSRS)
				Part of the RTT Validation Project.

Notes:			Stored in WHREPORTING.PTL

Versions:		
				1.0.0.0 - 03/12/2015 - MT - Job 198
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [PTL].[ReportOpenReferralsByCategory]
	@CategoryID int
As

;With MySpecialties As (
	Select	Distinct
			src.MainSpecialtyCode,
			src.MainSpecialty
	From	WHOLAP.dbo.SpecialtyDivisionBase src
	)
Select
	src.[ReferralID]
	,src.[PatientIdentifier]
	,src.[ReceivedDate]
	,src.[SpecialtyCode]
	,SpecialtyName = sp.MainSpecialty
	,src.[CategoryID]
	,src.[Details]
	,Hyperlink = 'javascript:void(window.open(''http://uhsm-is1/reportserver?%2fWaiting+Lists+and+PTLs%2fRTT+Status+History+By+Patient&rs:Command=Render&DistrictNo=' + src.PatientIdentifier + '&PathwayID=' + ref.PATNT_PATHWAY_ID + '''))'

From	OpenReferral src

		Left Join MySpecialties sp
			On src.SpecialtyCode = sp.MainSpecialtyCode

		-- Limiting to the sample that needs to be ratified
		-- 10 % or 25 whichever is the greater
		Inner Join OpenReferralSample sm
			On src.ReferralID = sm.ReferralID
			And sm.SampleFlag = 'Y'

		-- Get the PathwayID for the hyperlink
		Inner Join Lorenzo.dbo.Referral ref
			On src.ReferralID = ref.REFRL_REFNO

Where	src.CategoryID = @CategoryID
		
Order By
	src.[PatientIdentifier]
	,src.[ReceivedDate]
	,src.SeqNo