﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Open Referrals (SSRS) - Months Dropdown

Notes:			Stored in WHREPORTING.PTL

Versions:		
				1.0.0.0 - 20/11/2015 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [PTL].[ReportOpenReferrals_MonthsList]
As

Select	
		cal.FinancialMonthKey,
		cal.TheMonth,
		COUNT(*) As Referrals
			
From	RF.Referral src With (NoLock)

		Inner Join RF.Patient pat With (NoLock)
			On src.SourceEncounterNo = pat.ReferralSourceUniqueID
			And pat.DateOfDeath Is Null

		Inner Join LK.Calendar cal With (NoLOck)
			On src.ReferralReceivedDate = cal.TheDate

Where	src.CompletionDate Is Null		-- Open referrals
		And src.ReferralReceivedDate >= '2005-03-01'

Group By 
		cal.FinancialMonthKey,
		cal.TheMonth

Union All

Select	
		0 As FinancialMonthKey,
		'<Earlier>' As TheMonth,
		COUNT(*) As Referrals
			
From	RF.Referral src With (NoLock)

		Inner Join RF.Patient pat With (NoLock)
			On src.SourceEncounterNo = pat.ReferralSourceUniqueID
			And pat.DateOfDeath Is Null

		Inner Join LK.Calendar cal With (NoLOck)
			On src.ReferralReceivedDate = cal.TheDate

Where	src.CompletionDate Is Null		-- Open referrals
		And src.ReferralReceivedDate < '2005-03-01'

Order By FinancialMonthKey