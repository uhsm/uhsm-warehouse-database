﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Open Referrals - Summary (SSRS)
				Part of the RTT Validation Project.

Notes:			Stored in WHREPORTING.PTL

Versions:		
				1.0.0.0 - 24/11/2015 - MT
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [PTL].[ReportOpenReferrals_Summary]
As

;With MyOpenReferrals As (
	Select	src.SourceEncounterNo
			,pat.FacilityID
			,[PatientName] = pat.PatientSurname + ',' + pat.PatientForename
			
	From	RF.Referral src With (NoLock)

			Inner Join RF.Patient pat With (NoLock)
				On src.SourceEncounterNo = pat.ReferralSourceUniqueID
				And pat.DateOfDeath Is Null

	Where	src.CompletionDate Is Null		-- Open referrals
	),

	MyLastOPActivity As (
	Select	src.ReferralSourceUniqueID,
			src.AppointmentDate,
			src.Outcome,
			ROW_NUMBER() Over (Partition By src.ReferralSourceUniqueID Order By src.AppointmentDateTime Desc) As RowNo
	From	WHREPORTING.OP.Schedule src With (NoLock)

			Inner Join MyOpenReferrals ref With (NoLock)
				On src.ReferralSourceUniqueID = ref.SourceEncounterNo

	Where	src.AttendStatusCode In ('1','5','6') -- Attended, Attended On Time, Patient Late / Seen
	),

	MyLastIPActivity As (
	Select	src.ReferralSourceUniqueID,
			src.AdmissionDate,
			ROW_NUMBER() Over (Partition By src.ReferralSourceUniqueID Order By src.AdmissionDateTime Desc) As RowNo
	From	WHREPORTING.APC.Spell src With (NoLock)
	
			Inner Join MyOpenReferrals ref With (NoLock)
				On src.ReferralSourceUniqueID = ref.SourceEncounterNo
	),
	
	MyNextOPActivity As (
	Select	src.ReferralSourceUniqueID,
			src.AppointmentDate,
			ROW_NUMBER() Over (Partition By src.ReferralSourceUniqueID Order By src.AppointmentDateTime Asc) As RowNo
	From	WHREPORTING.OP.Schedule src With (NoLock)

			Inner Join MyOpenReferrals ref With (NoLock)
				On src.ReferralSourceUniqueID = ref.SourceEncounterNo

	Where	src.AttendStatusCode Is Null  -- Not cancelled
			And src.AppointmentDate > GETDATE()
	),
	
	MyCurrentOPWL As (
	Select	src.ReferralSourceUniqueID,
			src.WaitingListStartDate,
			ROW_NUMBER() Over (Partition By src.ReferralSourceUniqueID Order By src.WaitingListStartDate,src.WLSourceUniqueID Asc) As RowNo
	From	WH.PTL.OPWL src With (NoLock)
	
			Inner Join MyOpenReferrals ref With (NoLock)
				On src.ReferralSourceUniqueID = ref.SourceEncounterNo
	),
	
	MyCurrentIPWL As (	
	Select	src.ReferralSourceUniqueID,
			src.WaitingStartDate,
			ROW_NUMBER() Over (Partition By src.ReferralSourceUniqueID Order By src.WaitingStartDate,src.SourceUniqueID Asc) As RowNo
	From	WH.PTL.IPWL src With (NoLock)
	
			Inner Join MyOpenReferrals ref With (NoLock)
				On src.ReferralSourceUniqueID = ref.SourceEncounterNo
				And src.RemovalDate Is Null
	)

Select
	COUNT(*) As OpenReferrals,
	SUM(Case 
		When Coalesce(mnop.AppointmentDate,mcop.WaitingListStartDate,mcip.WaitingStartDate) Is Null
		And mlop.Outcome In ('Discharged') Then 1 Else 0 End) As DischargedWithoutFutureActivity

From	MyOpenReferrals src With (NoLock)

		Inner Join RF.Referral ref With (NoLock)
			On src.SourceEncounterNo = ref.SourceEncounterNo

		Left Join MyLastOPActivity mlop With (NoLock)
			On src.SourceEncounterNo = mlop.ReferralSourceUniqueID
			And mlop.RowNo = 1

		Left Join MyLastIPActivity mlip With (NoLock)
			On src.SourceEncounterNo = mlip.ReferralSourceUniqueID
			And mlip.RowNo = 1
			
		Left Join MyNextOPActivity mnop With (NoLock)
			On src.SourceEncounterNo = mnop.ReferralSourceUniqueID
			And mnop.RowNo = 1

		Left Join MyCurrentOPWL mcop With (NoLock)
			On src.SourceEncounterNo = mcop.ReferralSourceUniqueID
			And mcop.RowNo = 1
			
		Left Join MyCurrentIPWL mcip With (NoLock)
			On src.SourceEncounterNo = mcip.ReferralSourceUniqueID
			And mcip.RowNo = 1