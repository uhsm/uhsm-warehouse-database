﻿/*
--Author: C Bell-Hartley / N Scattergood
--Date created: 14/06/2013
--Stored Procedure Built for SRSS Report on:
--Outpatient New HSC Waiting List PTL
*/


CREATE Procedure [PTL].[ReportOutpatientNewHSCWaitingListPTL]

@SpecPar		as nvarchar(max)
--,@ConsPar		as nvarchar(max)
,@BookingPar	as nvarchar(max)

as
SELECT [ReferredToSpecialty]
      ,[ReferredToConsultant]
      ,coalesce([PatientId],s.NHSNo) as PatientID
      ,w.[PatientSurname]
      ,[ReferralPriority]
      ,[DateOfReferral]
      ,[CABAdjustedReferralDate]
      ,s.AppointmentType
      ,[FirstAppointmentStatus]
      ,[CurrentClinic]
      ,[CurrentAppointmentDate]
      ,case
      when [CurrentAppointmentDate] is null
      then 'Booked'
      else 'Unbooked'
      end as [BookingStatus] 
      ,[ActivityType]
      ,[ReferralSource]
      ,[ReferralStatus]
      ,[ReferringPCT]
      ,[ReferringPracticeCode]
      ,isnull([NumberOfPatientCancellations],0) as NumberOfPatientCancellations1
      ,isnull([NumberOfHospitalCancellations],0) as NumberOfHospitalCancellations1
      ,[NumberOfPatientReschedules]
      ,[NumberOfDNAs]
      ,DATEDIFF(DAY,CABAdjustedReferralDate,GETDATE()) as Daywait
      ,DATEDIFF(DAY,CABAdjustedReferralDate,AppointmentDate) as DaywaitToAppt
      ,DATEADD(day,7,CABAdjustedReferralDate) as [7DayBreachDate]
      ,DATEADD(day,14,CABAdjustedReferralDate) as [14DayBreachDate]
      
      ,CASE when CurrentAppointmentDate <GETDATE() 
      then '?Attendance' 
      when CurrentAppointmentDate > (DATEADD(day,14,CABAdjustedReferralDate))
      then 'BookedBreach'
      when (DATEDIFF(DAY,CABAdjustedReferralDate,GETDATE())) >14
      then 'Undated Breach'
      when FirstAppointmentStatus = 'Undated' and (DATEDIFF(DAY,CABAdjustedReferralDate,GETDATE())) >=14
      then 'Undated Breach'
      else 'OK'
      end as [Comments]

  FROM [WH].[PTL].[OPWL] w
  
    left join [WHREPORTING].[OP].Schedule s
		on  w.AppointmentSourceUniqueID = s.SourceUniqueID
		  
  WHERE 
  ReferralPriority = '2 Week Rule'
  and 
  (s.AppointmentType <> 'Follow Up' or s.AppointmentType IS NULL)
  and
  [ReferredToSpecialty] in (SELECT Val from dbo.fn_String_To_Table(@SpecPar,',',1)) 
  --and
  --[ReferredToConsultant] in (@ConsPar)
  and
  case
      when [CurrentAppointmentDate] is null
      then 'Booked'
      else 'Unbooked'
      end 
      in  (SELECT Val from dbo.fn_String_To_Table(@BookingPar,',',1))
      
      order by 
      
      [ReferredToSpecialty]
      ,[ReferredToConsultant]
      ,[CABAdjustedReferralDate]