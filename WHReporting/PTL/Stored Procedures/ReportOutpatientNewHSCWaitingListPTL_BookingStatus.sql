﻿/*
--Author: N Scattergood
--Date created: 18/06/2013
--Stored Procedure Built for SRSS Report on:
--Outpatient New HSC Waiting List PTL
*/


Create Procedure [PTL].[ReportOutpatientNewHSCWaitingListPTL_BookingStatus]

@SpecPar		as nvarchar(max)
----,@ConsPar		as nvarchar(max)
--,@BookingPar	as nvarchar(max)

as
SELECT 
Distinct
--[ReferredToSpecialty] as SpecPar
      --,[ReferredToConsultant]
      
      case
      when [CurrentAppointmentDate] is null
      then 'Booked'
      else 'Unbooked'
      end as [BookingStatusPar] 
      

  FROM [WH].[PTL].[OPWL] w
  
    left join [WHREPORTING].[OP].Schedule s
		on  w.AppointmentSourceUniqueID = s.SourceUniqueID
		  
  WHERE 
  ReferralPriority = '2 Week Rule'
  and 
  (s.AppointmentType <> 'Follow Up' or s.AppointmentType IS NULL)
  and
  [ReferredToSpecialty] in (SELECT Val from dbo.fn_String_To_Table(@SpecPar,',',1)) 
  ----and
  ----[ReferredToConsultant] in (@ConsPar)
  --and
  --case
  --    when [CurrentAppointmentDate] is null
  --    then 'Booked'
  --    else 'Unbooked'
  --    end 
  --    in  (SELECT Val from dbo.fn_String_To_Table(@BookingPar,',',1))