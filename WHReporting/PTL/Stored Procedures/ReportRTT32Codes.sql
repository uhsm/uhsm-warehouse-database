﻿CREATE procedure [PTL].[ReportRTT32Codes] as
/*

----------------------------------------------------------------------------------------------------------------
Action		Date		Author		Comments
----------------------------------------------------------------------------------------------------------------

Created		04/09/2014	KO			RD225 PTL - RTT Codes for validation - 32 rebuild
									To do - build SSRS report 
----------------------------------------------------------------------------------------------------------------
*/


select 
	ipwl.SpecialtyCode,
	spec.Specialty,
	ipwl.DistrictNo,
	ipwl.PatientSurname,
	rttstatus.StatusDate,
	rttstatus.RTTStatusCode,
	rttstatus.RTTStatusDesc,
	susp.START_DTTM,
	susp.end_DTTM,
	susp.SUSRS_REFNO_DESCRIPTION,
	ipwl.SourceUniqueID,
	ipwl.DateRecordModified,
	ipwl.ModifiedByUser,
	ipwl.SourcePatientNo
from WH.PTL.IPWLSuspensions susp 
inner join WH.PTL.IPWL ipwl
	on ipwl.SourceUniqueID = susp.WLIST_REFNO
inner join RTT_DW.RTT.IPMRTTOutcomes rttstatus
	on susp.START_DTTM = rttstatus.StatusDate
	and ipwl.ReferralSourceUniqueID = rttstatus.ref_ref
left join WHreporting.LK.SpecialtyDivision spec
	on ipwl.SpecialtyCode = spec.[SpecialtyCode]

where rttstatus.RTTStatusCode in ('31','32')