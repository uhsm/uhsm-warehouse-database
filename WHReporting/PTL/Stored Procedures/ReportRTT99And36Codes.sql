﻿CREATE procedure [PTL].[ReportRTT99And36Codes] 
/*

----------------------------------------------------------------------------------------------------------------
Action		Date		Author		Comments
----------------------------------------------------------------------------------------------------------------

Created		04/09/2014	KO			RD224 PTL - Outpatient Appointment Codes for validation rebuild
									To do - build SSRS report and remove hard coded date parameters
----------------------------------------------------------------------------------------------------------------
*/

--declare 
@startdate AS datetime,
--declare 
@enddate AS datetime,
--select @startdate = '2014-09-01'
--select @enddate = '2014-09-08'
@Specialty AS varchar(max),
@RTTOutcomeCode AS varchar(max)

AS
BEGIN


SET NOCOUNT ON;

select
	sched.Division,
	sched.Specialty,
	ConsultantSurname = cons.Surname,
	sched.NHSNo,
	sched.FacilityID,
	pat.PatientSurname,
	pat.DateOfDeath,
	sched.ClinicCode,
	sched.Clinic,
	RTTStatusCode = rttst.MainCode,
	RTTStatusDescription = rttst.ReferenceValue,
	sched.AppointmentDateTime,
	sched.AttendStatus,
	sched.Outcome,
	sched.AppointmentUpdatedByWhom

from OP.Schedule sched
left join OP.Patient pat
	on pat.SourceUniqueID = sched.SourceUniqueID
left join Lorenzo.dbo.ScheduleEvent enc
	on enc.SCHDL_REFNO = sched.SourceUniqueID
left join WH.PAS.ReferenceValue rttst
	on rttst.ReferenceValueCode = enc.RTTST_REFNO
left join WH.PAS.Consultant cons
	on cons.ConsultantCode = enc.PROCA_REFNO
where sched.IsWardAttender = 0
and sched.AppointmentDate between @startdate and @enddate
and rttst.MainCode in ('99', '36')
and sched.Specialty 

IN (SELECT Item
                         FROM   dbo.Split (@Specialty, ','))
and rttst.MainCode in (SELECT ITEM FROM dbo.Split (@RTTOutcomeCode, ','))

ORDER BY
sched.Division,
sched.Specialty,
sched.AppointmentDateTime


END