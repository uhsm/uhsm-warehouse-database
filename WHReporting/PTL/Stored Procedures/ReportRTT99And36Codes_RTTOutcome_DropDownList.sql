﻿-- =============================================
-- Author:		JBegum
-- Create date: 09/04/2014
-- Description: To create RTTOutcome dropdown list for SSRS report [PTL].[ReportRTT99And36Codes_Specialty_DropDownList] 
-- =============================================
CREATE PROCEDURE [PTL].[ReportRTT99And36Codes_RTTOutcome_DropDownList] 
	
@startdate AS datetime,
@enddate AS datetime,
@Specialty AS varchar(max) 
	
AS
BEGIN
	
	SET NOCOUNT ON;
	
Select distinct rttst.MainCode
from OP.Schedule sched
left join OP.Patient pat
	on pat.SourceUniqueID = sched.SourceUniqueID
left join Lorenzo.dbo.ScheduleEvent enc
	on enc.SCHDL_REFNO = sched.SourceUniqueID
left join WH.PAS.ReferenceValue rttst
	on rttst.ReferenceValueCode = enc.RTTST_REFNO
left join WH.PAS.Consultant cons
	on cons.ConsultantCode = enc.PROCA_REFNO
where sched.IsWardAttender = 0
and sched.AppointmentDate between @startdate and @enddate
and sched.Specialty IN (@Specialty)
and rttst.MainCode in ('99', '36')

    
	
END