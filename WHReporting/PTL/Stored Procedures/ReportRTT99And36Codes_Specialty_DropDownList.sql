﻿-- =============================================
-- Author:		JBegum
-- Create date: 09/04/2014
-- Description: To create Specialty dropdown list for SSRS report [PTL].[ReportRTT99And36Codes_Specialty_DropDownList] 
-- =============================================
CREATE PROCEDURE [PTL].[ReportRTT99And36Codes_Specialty_DropDownList] 
	
@startdate AS datetime,
@enddate AS datetime
	
AS
BEGIN
	
	SET NOCOUNT ON;
	
Select distinct Specialty
from OP.Schedule sched
left join OP.Patient pat
	on pat.SourceUniqueID = sched.SourceUniqueID
left join Lorenzo.dbo.ScheduleEvent enc
	on enc.SCHDL_REFNO = sched.SourceUniqueID
left join WH.PAS.ReferenceValue rttst
	on rttst.ReferenceValueCode = enc.RTTST_REFNO
left join WH.PAS.Consultant cons
	on cons.ConsultantCode = enc.PROCA_REFNO
where sched.IsWardAttender = 0
and sched.AppointmentDate between @startdate and @enddate
and rttst.MainCode in ('99', '36')
ORDER BY Specialty

    
	
END