﻿CREATE procedure [PTL].[ReportRTTOutpatient90Codes] 
	@StartDate datetime
	,@EndDate datetime
	,@AppointmentType varchar(500)
as
/*

----------------------------------------------------------------------------------------------------------------
Action		Date		Author		Comments
----------------------------------------------------------------------------------------------------------------

Created		23/01/2015	KO			Report for outpatients with 90 code without preceding 30
									--exec Lorenzo.dbo.BuildRTTFirst90Code run as part of WH.dbo.LoadPAS
----------------------------------------------------------------------------------------------------------------
*/
if Object_id('tempdb..#AppointmentType', 'U') is not null
	drop table #AppointmentType
	
select apptType = rtrim(ltrim(item))
into   #AppointmentType
from   WHREPORTING.dbo.Splitstrings_cte(@AppointmentType, N',')


--exec Lorenzo.dbo.BuildRTTFirst90Code run as part of WH.dbo.LoadPAS

select 
	--Schedule.[Specialty(Function)]
	Schedule.Specialty
	,ISNULL(Schedule.AttendStatus, 'NULL') as AttendStatus
	,Schedule.Outcome
	,Schedule.AppointmentType
	--,Schedule.[SpecialtyCode(Function)]
	,Schedule.SpecialtyCode
	,1 as Counter
from OP.Schedule Schedule
left join OP.ReferralToTreatment
	on OP.ReferralToTreatment.EncounterRecno = Schedule.EncounterRecno
	
left join LK.RTTSpecialtyExclusions specExcl
	on specExcl.LocalSpecialtyCode = Schedule.SpecialtyCode

inner join Lorenzo.dbo.RTTFirst90Code first90Code
	on first90Code.ref_ref = Schedule.ReferralSourceUniqueID
	
inner join #AppointmentType apptType
	on apptType.apptType collate database_default = Schedule.AppointmentType collate database_default

where dateadd(day, 0, datediff(day, 0, Schedule.AppointmentDate)) >= @StartDate 
and dateadd(day, 0, datediff(day, 0, Schedule.AppointmentDate)) <= @EndDate  
and RTTPeriodStatusCode in ('90','91','92')
and specExcl.SpecialtyCode is null

if Object_id('tempdb..#AppointmentType', 'U') is 
not 
null

	drop table #AppointmentType