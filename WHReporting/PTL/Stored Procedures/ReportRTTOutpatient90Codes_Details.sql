﻿CREATE procedure [PTL].[ReportRTTOutpatient90Codes_Details] 
	@StartDate datetime
	,@EndDate datetime
	,@AppointmentType varchar(500)
	,@Specialty varchar(100)
as
/*

----------------------------------------------------------------------------------------------------------------
Action		Date		Author		Comments
----------------------------------------------------------------------------------------------------------------

Created		26/01/2015	KO			Report for outpatients with 90 code without preceding 30 - Detail
----------------------------------------------------------------------------------------------------------------
*/
if Object_id('tempdb..#AppointmentType', 'U') is not null
	drop table #AppointmentType
	
select apptType = rtrim(ltrim(item))
into   #AppointmentType
from   WHREPORTING.dbo.Splitstrings_cte(@AppointmentType, N',')


select 
	Schedule.[Specialty(Function)]
	,Schedule.Specialty
	,Schedule.ProfessionalCarerName
	,Schedule.FacilityID
	,PatientSurname
	,Schedule.ClinicCode
	,Schedule.AppointmentDate
	,ISNULL(Schedule.AttendStatus, 'NULL') as AttendStatus
	,Schedule.Outcome
	,Schedule.AppointmentType
	,ReferralToTreatment.RTTPeriodStatusCode
	,ReferralToTreatment.RTTPeriodStatus
	,Schedule.[SpecialtyCode(Function)]
	,Schedule.SpecialtyCode
	,First90CodeDate = cast(first90Code.First90Code as date)
	,1 as Counter
	,Schedule.SourceUniqueID
	,Schedule.ReferralSourceUniqueID
	,Referral.ReferralReceivedDate
	,RTTWeeksWait = datediff(week, Referral.ReferralReceivedDate, GETDATE())

from OP.Schedule Schedule
left join OP.ReferralToTreatment ReferralToTreatment
	on ReferralToTreatment.EncounterRecno = Schedule.EncounterRecno
left join RF.Referral Referral
	on Referral.ReferralSourceUniqueID = Schedule.ReferralSourceUniqueID
inner join OP.Patient
	on Schedule.EncounterRecno = OP.Patient.EncounterRecno
inner join Lorenzo.dbo.RTTFirst90Code first90Code
	on first90Code.ref_ref = Schedule.ReferralSourceUniqueID
--left join [v-uhsm-dw02].RTT_DW.RTT.RTT_Dataset rtt 
--	on RTT.ref_ref = Schedule.ReferralSourceUniqueID
inner join #AppointmentType apptType
	on apptType.apptType collate database_default = Schedule.AppointmentType collate database_default

where dateadd(day, 0, datediff(day, 0, Schedule.AppointmentDate)) >= @StartDate 
and dateadd(day, 0, datediff(day, 0, Schedule.AppointmentDate)) <= @EndDate  
and RTTPeriodStatusCode in ('90','91','92')
and Schedule.SpecialtyCode = @Specialty

order by Schedule.FacilityID

if Object_id('tempdb..#AppointmentType', 'U') is not null
	drop table #AppointmentType