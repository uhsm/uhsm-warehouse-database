﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Outpatient attendances with RTT 90 Code - Review

Notes:			Stored in WHREPORTING.PTL

Versions:
				1.0.0.0 - 11/11/2015 - MT
					Previously just a script inside the SSRS report:
					OP_Appts_New_90_Codes.rdl
					Replaced with this sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [PTL].[ReportRTTOutpatientNew90Codes]
	@StartDate date,
	@EndDate date
As

Select
		src.[Specialty(Function)] As SpecialtyFunction
		,src.ProfessionalCarerName
		,src.FacilityID
		,pat.PatientSurname
		,src.ClinicCode
		,src.AppointmentDate
		,src.AttendStatus
		,ISNULL(src.Outcome,'NULL') As Outcome
		,src.AppointmentType
		,rtt.RTTPeriodStatusCode
		,rtt.RTTPeriodStatus
		,src.[SpecialtyCode(Function)] As SpecialtyCodeFunction
		,1 As Counter

From	OP.Schedule src

		Left Join OP.ReferralToTreatment rtt
			On src.EncounterRecno = rtt.EncounterRecno

		Inner Join OP.Patient pat
			On src.EncounterRecno = pat.EncounterRecno

Where	src.AppointmentDate >= @StartDate
		And src.AppointmentDate <= @EndDate
		And src.AppointmentType = 'New'
		And rtt.RTTPeriodStatusCode In ('90','91','92')
		And src.SpecialtyCode In ('103','320','170','120','301','300','100','430',
		'503','502','140','171','420','190','191','160','340','173','110','101','107')