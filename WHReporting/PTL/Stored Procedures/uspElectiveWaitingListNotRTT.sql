﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Elective Waiting List Not RTT

Notes:			Stored in WHREPORTING.PTL

				Based on PTL.ReportDailyAdmitted_Specialty but this version
				only has those elective waiters that are not RTT because those that
				are RTT will be covered by the new Incomplete RTT Open Pathways PTL report.

Versions:		
				1.0.0.1 - 08/03/2016 - CM
					Amended referencs from DW_Staging to DW_REporting 

				1.0.0.0 - 26/02/2016 - MT - Job 343
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [PTL].[uspElectiveWaitingListNotRTT]
	@Specialty varchar(Max)
	,@Consultant varchar(Max)
	,@Priority varchar (Max)
	,@PTLComment varchar(Max)
	,@RecordCreatedDateFrom Date
	,@RecordCreatedDateTo Date
As

-- CTE used as a consultant name lookup as there are multiples in this table (all specialties)
;With ConsultantCTE As (
	Select	Distinct
			ConsultantCode,
			Surname
	From	LK.ConsultantSpecialty src
	),

	BreachDateCTE As (
	Select	src.SourceUniqueID,
			Case
				When src.IPWLDiagnosticProcedure = 'Y'
					And Coalesce(src.PlannedProcedure,'xx') + Coalesce(src.GeneralComment,'xx') Not Like '%$T%' Then
					DATEADD(d,43,src.WaitingStartDate)  -- 6 weeks
				Else DATEADD(d,127,src.ReferralDate) -- 18 weeks
			End As BreachDate
	From	WH.PTL.IPWL src
	Where	src.RemovalDate Is Null
			And src.ElectiveAdmissionMethod <> '13'
	)

Select
	Specialty = sp.SubSpecialty
	,Consultant = con.Surname
	,RM2Number = src.DistrictNo
	,PatientName = src.PatientSurname + ', ' + Coalesce(src.PatientForename,'')
	,Sex = Case When src.Sex = 'Male' Then 'M' When src.Sex = 'Female' Then 'F' Else 'U' End
	,Age = DW_REPORTING.LIB.fn_AgeAsInteger(lpat.DATE_OF_BIRTH,GETDATE())
	,src.Priority
	,src.GeneralComment
	,src.AnaestheticType
	,src.EstimatedTheatreTime
	,IntendedMgmt = DW_REPORTING.LIB.fn_IntendedMgmtGroup(src.IntendedManagement)
	,src.ListType
	,src.AdmissionWard
	,src.SecondaryWard
	,ClockStartDate = src.ReferralDate
	,src.DateOnList
	,WeeksWait = (DATEDIFF(d,src.ReferralDate,GETDATE()) - 1) / 7
	,Diagnostic = 
		Case
			When src.IPWLDiagnosticProcedure = 'Y'
				And Coalesce(src.PlannedProcedure,'xx') + Coalesce(src.GeneralComment,'xx') Not Like '%$T%' Then 'Y'
		End
	,br.BreachDate
	,src.TCIDate
	,OperationDate = op.OrmisOperationDate
	,src.PreOpDate
	,PTLComment = DW_REPORTING.LIB.fn_PTLComment(src.TCIDate,DATEADD(d,127,src.DateOnList),GETDATE())
	,ref.ReferralCompletionComments
	,Hyperlink = 'javascript:void(window.open(''http://uhsm-is1/reportserver?%2fWaiting+Lists+and+PTLs%2fRTT+Status+History+By+Patient&rs:Command=Render&DistrictNo=' + src.DistrictNo + '&PathwayID=' + lref.PATNT_PATHWAY_ID + '''))'

From	WH.PTL.IPWL src

		Left Join WHOLAP.dbo.SpecialtyDivisionBase sp
			On src.SpecialtyCode = sp.SubSpecialtyCode

		Left Join ConsultantCTE con
			On src.Consultant = con.ConsultantCode

		Left Join WH.PAS.Patient pat
			On src.DistrictNo = pat.LocalPatientIdentifier
			
		Left Join Lorenzo.dbo.Patient lpat
			On pat.MasterPATNT_REFNO = lpat.PATNT_REFNO
			
		Left Join WH.PTL.ORMISWaitingListBooking op
			On src.SourceUniqueID = op.WLIST_REFNO

		Left Join RF.Referral ref
			On src.ReferralSourceUniqueID = ref.ReferralSourceUniqueID
			
		-- Get the pathway ID
		Left Join Lorenzo.dbo.Referral lref
			On src.ReferralSourceUniqueID = lref.REFRL_REFNO
			
		-- Open pathway
		Left Join DW_Reporting.RTT.Period prd
			On src.ReferralSourceUniqueID = prd.ReferralID
			And prd.EndDate Is Null
			
		Left Join BreachDateCTE br
			On src.SourceUniqueID = br.SourceUniqueID 
			
		Inner Join DW_REPORTING.LIB.fn_SplitString(@Specialty,',') sp2
			On src.SpecialtyCode = sp2.SplitValue

		Inner Join DW_REPORTING.LIB.fn_SplitString(@Consultant,',') con2
			On src.Consultant = con2.SplitValue

		Inner Join DW_REPORTING.LIB.fn_SplitString(@Priority,',') pr
			On src.Priority = pr.SplitValue

		Inner Join DW_REPORTING.LIB.fn_SplitString(@PTLComment,',') com
			On DW_REPORTING.LIB.fn_PTLComment(src.TCIDate,DATEADD(d,127,src.DateOnList),GETDATE()) = com.SplitValue

Where	src.RemovalDate Is Null					-- Waiting
		And src.ElectiveAdmissionMethod <> '13'	-- Exclude planned
		And prd.PathwayID Is Null				-- Not on an open pathway
		And src.DateRecordCreated Between @RecordCreatedDateFrom And @RecordCreatedDateTo

Order By
	sp.SubSpecialty
	,con.Surname
	,src.Priority
	,br.BreachDate
	-- PTL Comment
	,Coalesce(DW_REPORTING.LIB.fn_PTLComment(src.TCIDate,DATEADD(d,127,src.DateOnList),GETDATE()),'Unknown')