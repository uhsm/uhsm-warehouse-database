﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Elective Waiting List Not RTT - Consultant Dropdown List

Notes:			Stored in WHREPORTING.PTL

Versions:		
				1.0.0.1 - 08/03/2016 - CM 
					Amended RTT references from DW_STAGING to DW_REPORTING

				1.0.0.0 - 26/02/2016 - MT - Job 343
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [PTL].[uspElectiveWaitingListNotRTT_ConsultantList]
	@Specialty varchar(Max)
As

-- CTE used as a consultant name lookup as there are multiples in this table (all specialties)
;With ConsultantCTE As (
	Select	Distinct
			ConsultantCode,
			Surname
	From	LK.ConsultantSpecialty src
	)

Select	Distinct
		ConsultantCode = src.Consultant
		,ConsultantName = Coalesce(con.Surname,'Unknown')

From	WH.PTL.IPWL src

		Inner Join WHOLAP.dbo.SpecialtyDivisionBase sp
			On src.SpecialtyCode = sp.SubSpecialtyCode
			
		Inner Join DW_REPORTING.LIB.fn_SplitString(@Specialty,',') ss
			On src.SpecialtyCode = ss.SplitValue
			
		Left Join ConsultantCTE con
			On src.Consultant = con.ConsultantCode

		-- Open pathway
		Left Join DW_REPORTING.RTT.Period prd
			On src.ReferralSourceUniqueID = prd.ReferralID
			And prd.EndDate Is Null
			
Where	src.RemovalDate Is Null					-- Waiting
		And src.ElectiveAdmissionMethod <> '13'	-- Exclude planned
		And prd.PathwayID Is Null				-- Not on an open pathway

Order By
	ConsultantName