﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Elective Waiting List Not RTT - PTLComment Dropdown List

Notes:			Stored in WHREPORTING.PTL

Versions:
				1.0.0.1 - 08/03/2016 - CM 
					Amended RTT references from DW_STAGINg to DW_REporting	

				1.0.0.0 - 26/02/2016 - MT - Job 343
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [PTL].[uspElectiveWaitingListNotRTT_PTLCommentList]
As

Select	Distinct
		PTLComment = DW_REPORTING.LIB.fn_PTLComment(src.TCIDate,DATEADD(d,127,src.DateOnList),GETDATE())

From	WH.PTL.IPWL src

		-- Open pathway
		Left Join DW_REPORTING.RTT.Period prd
			On src.ReferralSourceUniqueID = prd.ReferralID
			And prd.EndDate Is Null
			
Where	src.RemovalDate Is Null					-- Waiting
		And src.ElectiveAdmissionMethod <> '13'	-- Exclude planned
		And prd.PathwayID Is Null				-- Not on an open pathway

Order By
	PTLComment