﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Elective Waiting List Not RTT - Priority Dropdown List

Notes:			Stored in WHREPORTING.PTL

Versions:	
				1.0.0.1 - 08/03/2016 - CM 
					Amended RTT references from DW_STAGINg to DW_REporting	

				1.0.0.0 - 26/02/2016 - MT - Job 343
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [PTL].[uspElectiveWaitingListNotRTT_PriorityList]
As

Select	Distinct
		PriorityCode = src.Priority
		,PriorityName = rv.[DESCRIPTION]

From	WH.PTL.IPWL src

		Inner Join Lorenzo.dbo.ReferenceValue rv
			On rv.RFVDM_CODE = 'PRITY'
			And src.Priority = rv.MAIN_CODE

		-- Open pathway
		Left Join DW_REPORTING.RTT.Period prd
			On src.ReferralSourceUniqueID = prd.ReferralID
			And prd.EndDate Is Null
			
Where	src.RemovalDate Is Null					-- Waiting
		And src.ElectiveAdmissionMethod <> '13'	-- Exclude planned
		And prd.PathwayID Is Null				-- Not on an open pathway

Order By
	PriorityName