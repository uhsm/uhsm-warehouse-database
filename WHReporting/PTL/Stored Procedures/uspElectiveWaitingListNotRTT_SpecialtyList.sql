﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Elective Waiting List Not RTT - Specialty Dropdown List

Notes:			Stored in WHREPORTING.PTL

Versions:
				1.0.0.1 - 08/03/2016 - CM 
					Amended RTT references from DW_STAGINg to DW_REporting	

				1.0.0.0 - 26/02/2016 - MT - Job 343
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [PTL].[uspElectiveWaitingListNotRTT_SpecialtyList]
As

Select	Distinct
		SpecialtyCode = src.SpecialtyCode
		,SpecialtyName = sp.SubSpecialty

From	WH.PTL.IPWL src

		Left Join WHOLAP.dbo.SpecialtyDivisionBase sp
			On src.SpecialtyCode = sp.SubSpecialtyCode
			
		-- Open pathway
		Left Join DW_REPORTING.RTT.Period prd
			On src.ReferralSourceUniqueID = prd.ReferralID
			And prd.EndDate Is Null
			
Where	src.RemovalDate Is Null					-- Waiting
		And src.ElectiveAdmissionMethod <> '13'	-- Exclude planned
		And prd.PathwayID Is Null				-- Not on an open pathway

Order By
	SpecialtyName