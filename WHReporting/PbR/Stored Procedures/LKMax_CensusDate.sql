﻿/*
--Author: C Beckett
--Date created: 23/09/2015
--Stored Procedure Built used as lookup for the Max Census Date on PbRCut
-- Currently used in SSRS Report - CT Activity via SLAM
--Modified on:
*/


CREATE Procedure [PbR].[LKMax_CensusDate]

as


SELECT 
     
  MAX(CensusDate)
  
  FROM [PbR2015].[dbo].[PbRCut]