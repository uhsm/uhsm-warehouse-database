﻿/*
--Author: G Ryder
--Date created: 22/01/2016
--Stored Procedure Built used for "Allergy Data For Finance" SSRS Report
--Currently set up as a union to pick up 2014/15 and 2015/16 data, depending
--on what month Finance need to look at (they generally only want to look
--at one month at a time). Will need changing to include a union to 2016/17
--data once this is available.
--Amended for new year (2016/17 replaced 2014/15) 3rd May 2016 (NS)
*/

CREATE Procedure [PbR].[ReportAllergyDataForFinance]
  
@StartDate as Datetime
,@EndDate as Datetime



as

SELECT p.EncounterTypeCode,
       p.InterfaceCode,
       p.DatasetCode,
       p.CCGCode,
       p.ProviderSpellNo,
       p.DistrictNo,
       p.Age,
       p.AdmissionDate,
       p.DischargeDate,
       p.PODCode,
       p.TreatmentFunctionCode,
       sp.[AdmissionSpecialty(Function)],
       sp.AdmissionConsultantName,
       sp.AdmissionSpecialty,
       p.SpellLOS,
       p.TrimPoint,
       p.ExcessBedDays,
       p.HRGCode,
       p.DominantProcedureCode,
       e.PrimaryDiagnosisCode,
       CASE
         WHEN ICD.DiagnosisSequence = 1 THEN ICD.[DiagnosisCode] + '-' + ICD.[Diagnosis]
         ELSE NULL
       END                       AS PrimaryDiagnosis,
       e.SubsidiaryDiagnosisCode,
       CASE
         WHEN ICD2.DiagnosisSequence = 2 THEN ICD2.[DiagnosisCode] + '-' + ICD2.[Diagnosis]
         ELSE NULL
       END                       AS SubsidiaryDiagnosis,
       e.SecondaryDiagnosisCode1 AS secondaryDiagnosis2,
       CASE
         WHEN ICD3.DiagnosisSequence = 3 THEN ICD3.[DiagnosisCode] + '-' + ICD3.[Diagnosis]
         ELSE NULL
       END                       AS SecondaryDiagnosis2details,
       e.SecondaryDiagnosisCode2 AS secondarydiagnosis3,
       CASE
         WHEN ICD4.DiagnosisSequence = 4 THEN ICD4.[DiagnosisCode] + '-' + ICD4.[Diagnosis]
         ELSE NULL
       END                       AS SecondaryDiagnosis3details,
       e.SecondaryDiagnosisCode3 AS secondarydiagnosis4,
       CASE
         WHEN ICD5.DiagnosisSequence = 5 THEN ICD5.[DiagnosisCode] + '-' + ICD5.[Diagnosis]
         ELSE NULL
       END                       AS SecondaryDiagnosis4details,
       e.PrimaryOperationCode,
       CASE
         WHEN OPCS.ProcedureSequence = 1 THEN OPCS.[ProcedureCode] + '-' + OPCS.[Procedure]
         ELSE NULL
       END                       AS PrimaryOperationdetails,
       e.SecondaryOperationCode1,
       CASE
         WHEN OPCS1.ProcedureSequence = 2 THEN OPCS1.[ProcedureCode] + '-'
                                               + OPCS1.[Procedure]
         ELSE NULL
       END                       AS SecondaryOperation1details,
       e.SecondaryOperationCode2,
       CASE
         WHEN OPCS2.ProcedureSequence = 3 THEN OPCS2.[ProcedureCode] + '-'
                                               + OPCS2.[Procedure]
         ELSE NULL
       END                       AS SecondaryOperation2details,
       e.SecondaryOperationCode3,
       CASE
         WHEN OPCS3.ProcedureSequence = 4 THEN OPCS3.[ProcedureCode] + '-'
                                               + OPCS3.[Procedure]
         ELSE NULL
       END                       AS SecondaryOperation3details,
       e.SecondaryOperationCode4,
       CASE
         WHEN OPCS4.ProcedureSequence = 5 THEN OPCS4.[ProcedureCode] + '-'
                                               + OPCS4.[Procedure]
         ELSE NULL
       END                       AS SecondaryOperation4details,
       e.SecondaryOperationCode5,
       CASE
         WHEN OPCS5.ProcedureSequence = 6 THEN OPCS5.[ProcedureCode] + '-'
                                               + OPCS5.[Procedure]
         ELSE NULL
       END                       AS SecondaryOperation5details,
       e.SourceUniqueID
--OPCS.[Procedure] as PrimaryProcedure
FROM   PbR2016.dbo.PbR p
       LEFT OUTER JOIN WHREPORTING.APC.Spell sp
                    ON p.ProviderSpellNo = sp.SourceSpellNo
       LEFT OUTER JOIN WH.APC.Encounter e
                    ON p.ProviderSpellNo = e.SourceSpellNo
       LEFT OUTER JOIN WHREPORTING.APC.AllProcedures OPCS
                    ON e.SourceUniqueID = OPCS.EpisodeSourceUniqueID
                       AND e.PrimaryOperationCode = OPCS.ProcedureCode
       LEFT OUTER JOIN WHREPORTING.APC.AllDiagnosis ICD
                    ON e.SourceUniqueID = ICD.EpisodeSourceUniqueID
                       AND e.PrimaryDiagnosisCode = ICD.DiagnosisCode
       LEFT OUTER JOIN WHREPORTING.APC.AllDiagnosis ICD2
                    ON e.SourceUniqueID = ICD2.EpisodeSourceUniqueID
                       AND e.SubsidiaryDiagnosisCode = ICD2.DiagnosisCode
       LEFT OUTER JOIN WHREPORTING.APC.AllDiagnosis ICD3
                    ON e.SourceUniqueID = ICD3.EpisodeSourceUniqueID
                       AND e.SecondaryDiagnosisCode1 = ICD3.DiagnosisCode
       LEFT OUTER JOIN WHREPORTING.APC.AllDiagnosis ICD4
                    ON e.SourceUniqueID = ICD4.EpisodeSourceUniqueID
                       AND e.SecondaryDiagnosisCode2 = ICD4.DiagnosisCode
       LEFT OUTER JOIN WHREPORTING.APC.AllDiagnosis ICD5
                    ON e.SourceUniqueID = ICD5.EpisodeSourceUniqueID
                       AND e.SecondaryDiagnosisCode3 = ICD5.DiagnosisCode
       LEFT OUTER JOIN WHREPORTING.APC.AllProcedures OPCS1
                    ON e.SourceUniqueID = OPCS1.EpisodeSourceUniqueID
                       AND e.SecondaryOperationCode1 = OPCS1.ProcedureCode
       LEFT OUTER JOIN WHREPORTING.APC.AllProcedures OPCS2
                    ON e.SourceUniqueID = OPCS2.EpisodeSourceUniqueID
                       AND e.SecondaryOperationCode2 = OPCS2.ProcedureCode
       LEFT OUTER JOIN WHREPORTING.APC.AllProcedures OPCS3
                    ON e.SourceUniqueID = OPCS3.EpisodeSourceUniqueID
                       AND e.SecondaryOperationCode3 = OPCS3.ProcedureCode
       LEFT OUTER JOIN WHREPORTING.APC.AllProcedures OPCS4
                    ON e.SourceUniqueID = OPCS4.EpisodeSourceUniqueID
                       AND e.SecondaryOperationCode4 = OPCS4.ProcedureCode
       LEFT OUTER JOIN WHREPORTING.APC.AllProcedures OPCS5
                    ON e.SourceUniqueID = OPCS5.EpisodeSourceUniqueID
                       AND e.SecondaryOperationCode6 = OPCS5.ProcedureCode
WHERE  TreatmentFunctionCode = '317'
       AND EncounterTypeCode = 'IP'
       AND p.DischargeDate >= @StartDate
       AND p.DischargeDate <= @EndDate
       
       
union

SELECT p.EncounterTypeCode,
       p.InterfaceCode,
       p.DatasetCode,
       p.CCGCode,
       p.ProviderSpellNo,
       p.DistrictNo,
       p.Age,
       p.AdmissionDate,
       p.DischargeDate,
       p.PODCode,
       p.TreatmentFunctionCode,
       sp.[AdmissionSpecialty(Function)],
       sp.AdmissionConsultantName,
       sp.AdmissionSpecialty,
       p.SpellLOS,
       p.TrimPoint,
       p.ExcessBedDays,
       p.HRGCode,
       p.DominantProcedureCode,
       e.PrimaryDiagnosisCode,
       CASE
         WHEN ICD.DiagnosisSequence = 1 THEN ICD.[DiagnosisCode] + '-' + ICD.[Diagnosis]
         ELSE NULL
       END                       AS PrimaryDiagnosis,
       e.SubsidiaryDiagnosisCode,
       CASE
         WHEN ICD2.DiagnosisSequence = 2 THEN ICD2.[DiagnosisCode] + '-' + ICD2.[Diagnosis]
         ELSE NULL
       END                       AS SubsidiaryDiagnosis,
       e.SecondaryDiagnosisCode1 AS secondaryDiagnosis2,
       CASE
         WHEN ICD3.DiagnosisSequence = 3 THEN ICD3.[DiagnosisCode] + '-' + ICD3.[Diagnosis]
         ELSE NULL
       END                       AS SecondaryDiagnosis2details,
       e.SecondaryDiagnosisCode2 AS secondarydiagnosis3,
       CASE
         WHEN ICD4.DiagnosisSequence = 4 THEN ICD4.[DiagnosisCode] + '-' + ICD4.[Diagnosis]
         ELSE NULL
       END                       AS SecondaryDiagnosis3details,
       e.SecondaryDiagnosisCode3 AS secondarydiagnosis4,
       CASE
         WHEN ICD5.DiagnosisSequence = 5 THEN ICD5.[DiagnosisCode] + '-' + ICD5.[Diagnosis]
         ELSE NULL
       END                       AS SecondaryDiagnosis4details,
       e.PrimaryOperationCode,
       CASE
         WHEN OPCS.ProcedureSequence = 1 THEN OPCS.[ProcedureCode] + '-' + OPCS.[Procedure]
         ELSE NULL
       END                       AS PrimaryOperationdetails,
       e.SecondaryOperationCode1,
       CASE
         WHEN OPCS1.ProcedureSequence = 2 THEN OPCS1.[ProcedureCode] + '-'
                                               + OPCS1.[Procedure]
         ELSE NULL
       END                       AS SecondaryOperation1details,
       e.SecondaryOperationCode2,
       CASE
         WHEN OPCS2.ProcedureSequence = 3 THEN OPCS2.[ProcedureCode] + '-'
                                               + OPCS2.[Procedure]
         ELSE NULL
       END                       AS SecondaryOperation2details,
       e.SecondaryOperationCode3,
       CASE
         WHEN OPCS3.ProcedureSequence = 4 THEN OPCS3.[ProcedureCode] + '-'
                                               + OPCS3.[Procedure]
         ELSE NULL
       END                       AS SecondaryOperation3details,
       e.SecondaryOperationCode4,
       CASE
         WHEN OPCS4.ProcedureSequence = 5 THEN OPCS4.[ProcedureCode] + '-'
                                               + OPCS4.[Procedure]
         ELSE NULL
       END                       AS SecondaryOperation4details,
       e.SecondaryOperationCode5,
       CASE
         WHEN OPCS5.ProcedureSequence = 6 THEN OPCS5.[ProcedureCode] + '-'
                                               + OPCS5.[Procedure]
         ELSE NULL
       END                       AS SecondaryOperation5details,
       e.SourceUniqueID
--OPCS.[Procedure] as PrimaryProcedure
FROM   PbR2015.dbo.PbR p
       LEFT OUTER JOIN WHREPORTING.APC.Spell sp
                    ON p.ProviderSpellNo = sp.SourceSpellNo
       LEFT OUTER JOIN WH.APC.Encounter e
                    ON p.ProviderSpellNo = e.SourceSpellNo
       LEFT OUTER JOIN WHREPORTING.APC.AllProcedures OPCS
                    ON e.SourceUniqueID = OPCS.EpisodeSourceUniqueID
                       AND e.PrimaryOperationCode = OPCS.ProcedureCode
       LEFT OUTER JOIN WHREPORTING.APC.AllDiagnosis ICD
                    ON e.SourceUniqueID = ICD.EpisodeSourceUniqueID
                       AND e.PrimaryDiagnosisCode = ICD.DiagnosisCode
       LEFT OUTER JOIN WHREPORTING.APC.AllDiagnosis ICD2
                    ON e.SourceUniqueID = ICD2.EpisodeSourceUniqueID
                       AND e.SubsidiaryDiagnosisCode = ICD2.DiagnosisCode
       LEFT OUTER JOIN WHREPORTING.APC.AllDiagnosis ICD3
                    ON e.SourceUniqueID = ICD3.EpisodeSourceUniqueID
                       AND e.SecondaryDiagnosisCode1 = ICD3.DiagnosisCode
       LEFT OUTER JOIN WHREPORTING.APC.AllDiagnosis ICD4
                    ON e.SourceUniqueID = ICD4.EpisodeSourceUniqueID
                       AND e.SecondaryDiagnosisCode2 = ICD4.DiagnosisCode
       LEFT OUTER JOIN WHREPORTING.APC.AllDiagnosis ICD5
                    ON e.SourceUniqueID = ICD5.EpisodeSourceUniqueID
                       AND e.SecondaryDiagnosisCode3 = ICD5.DiagnosisCode
       LEFT OUTER JOIN WHREPORTING.APC.AllProcedures OPCS1
                    ON e.SourceUniqueID = OPCS1.EpisodeSourceUniqueID
                       AND e.SecondaryOperationCode1 = OPCS1.ProcedureCode
       LEFT OUTER JOIN WHREPORTING.APC.AllProcedures OPCS2
                    ON e.SourceUniqueID = OPCS2.EpisodeSourceUniqueID
                       AND e.SecondaryOperationCode2 = OPCS2.ProcedureCode
       LEFT OUTER JOIN WHREPORTING.APC.AllProcedures OPCS3
                    ON e.SourceUniqueID = OPCS3.EpisodeSourceUniqueID
                       AND e.SecondaryOperationCode3 = OPCS3.ProcedureCode
       LEFT OUTER JOIN WHREPORTING.APC.AllProcedures OPCS4
                    ON e.SourceUniqueID = OPCS4.EpisodeSourceUniqueID
                       AND e.SecondaryOperationCode4 = OPCS4.ProcedureCode
       LEFT OUTER JOIN WHREPORTING.APC.AllProcedures OPCS5
                    ON e.SourceUniqueID = OPCS5.EpisodeSourceUniqueID
                       AND e.SecondaryOperationCode6 = OPCS5.ProcedureCode
WHERE  TreatmentFunctionCode = '317'
       AND EncounterTypeCode = 'IP'
       AND p.DischargeDate >= @StartDate
       AND p.DischargeDate <= @EndDate