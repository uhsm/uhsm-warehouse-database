﻿/*
--Author: C Beckett
--Date created: 22/09/2015
--Stored Procedure Built used for SSRS Report on CT Activity via SLAM
--Modified on:
*/


CREATE Procedure [PbR].[ReportCTActivity_SLAM]

as
Select * From	
(	
SELECT  [MonthNo]	
      ,[CCG_Code]	
      , Left(Spec_Code,3) as Spec_Code	
      ,[Spec_Code] as Spec_Desc	
      ,[HRG_Code]	
      ,HRGDescription	
      ,[POD_Code]	
      ,[Activity]	
      --,[SpecService_ID]	
      --,[SpecService_ID_2]	
      --,[SpecService_ID_3]	
      --,[SpecService_ID_4]	
      --,[SpecService_ID_5]	
      --,[SpecService_ID_6]	
      --,[SpecService_ID_7]	
      --,[SpecService_ID_8]	
      --,[SpecService_ID_9]	
      --,[SpecService_ID_10]	
      --,[SpecService_ID_11]	
      --,[SpecService_ID_12]	
      --,[SpecService_ID_13]	
      --,[SpecService_ID_14]	
      --,[SpecService_ID_15]	
      --,[SpecService_ID_16]	
      --,[SpecService_ID_17]	
      --,[SpecService_ID_18]	
      --,[SpecService_ID_19]	
      --,[SpecService_ID_20]	
      --,[SpecService_ID_21]	
      --,[Gpp_Code]	
      ,[Pat_Code]	
      --,[DOB]	
      ,[Cons_Code]	
      ,[GP_Code]	
      ,[SpellAdmissionDate]	
      ,[SpellDischargeDate]	
      ,[LOS]	
      ,[ReportMasterID]	
      ,[UD1Act_Code]	
      ,[UD2Act_Code]	
      ,[UD3Act_Code]	
      ,[UD4Act_Code]	
      ,[UD5Act_Code]	
    	
      --,[PatientClass_Code]	
      ,[AdmissionMethod_Code]	
      --,[Weight]	
      --,[IsReadmission]	
      ,[ServiceLine_Code]	
      ,[OriginatingCCG_Code]	
      ,[NHSNumber]	
      --,[PatPostCd]	
      --,[Clinic_Code]	
      ,[Ward_Code]	
      ,[ICD10_Code]	
      ,[OPCS_Code]	
      --,[UD1ActualAct_Code]	
      --,[UD2ActualAct_Code]	
      --,[UD3ActualAct_Code]	
      --,[UD4ActualAct_Code]	
      --,[UD5ActualAct_Code]	
      --,[PseudoReportMasterID]	
      ,[ReferralSource_Code]	
      ,[PathwayID]	
      ,[MainSpec_Code]	
      ,[SUSActivityID]	
      --,[ServiceClass_Code]	
  FROM [PbR2015].[dbo].[SLAMCombinedAllPOD] SLAM	
  	
  	left Join PbR2015.LK.HRGFull HRG
	on SLAM.HRG_Code = HRG.HRG
  	
  Where	
  Left(Spec_Code,3) in ('170','172','173','174','320')	
  and	
  LEN(Spec_Code) > 3	
  	
  	
  UNION ALL	
  	
  SELECT  [MonthNo]	
      ,[CCG_Code]	
      , Left(Spec_Code,3) as Spec_Code	
      ,[Spec_Code] as Spec_Desc	
      ,[HRG_Code]	
      ,HRGDescription	
      ,[POD_Code]	
      ,[Activity]	
      --,[SpecService_ID]	
      --,[SpecService_ID_2]	
      --,[SpecService_ID_3]	
      --,[SpecService_ID_4]	
      --,[SpecService_ID_5]	
      --,[SpecService_ID_6]	
      --,[SpecService_ID_7]	
      --,[SpecService_ID_8]	
      --,[SpecService_ID_9]	
      --,[SpecService_ID_10]	
      --,[SpecService_ID_11]	
      --,[SpecService_ID_12]	
      --,[SpecService_ID_13]	
      --,[SpecService_ID_14]	
      --,[SpecService_ID_15]	
      --,[SpecService_ID_16]	
      --,[SpecService_ID_17]	
      --,[SpecService_ID_18]	
      --,[SpecService_ID_19]	
      --,[SpecService_ID_20]	
      --,[SpecService_ID_21]	
      --,[Gpp_Code]	
      ,[Pat_Code]	
      --,[DOB]	
      ,[Cons_Code]	
      ,[GP_Code]	
      ,[SpellAdmissionDate]	
      ,[SpellDischargeDate]	
      ,[LOS]	
      ,[ReportMasterID]	
      ,[UD1Act_Code]	
      ,[UD2Act_Code]	
      ,[UD3Act_Code]	
      ,[UD4Act_Code]	
      ,[UD5Act_Code]	
    	
      --,[PatientClass_Code]	
      ,[AdmissionMethod_Code]	
      --,[Weight]	
      --,[IsReadmission]	
      ,[ServiceLine_Code]	
      ,[OriginatingCCG_Code]	
      ,[NHSNumber]	
      --,[PatPostCd]	
      --,[Clinic_Code]	
      ,[Ward_Code]	
      ,[ICD10_Code]	
      ,[OPCS_Code]	
      --,[UD1ActualAct_Code]	
      --,[UD2ActualAct_Code]	
      --,[UD3ActualAct_Code]	
      --,[UD4ActualAct_Code]	
      --,[UD5ActualAct_Code]	
      --,[PseudoReportMasterID]	
      ,[ReferralSource_Code]	
      ,[PathwayID]	
      ,[MainSpec_Code]	
      ,[SUSActivityID]	
      --,[ServiceClass_Code]	
  FROM [PbR2015].[dbo].[SLAMCombinedAllPODExcluded] SLAM	
  	
    	left Join PbR2015.LK.HRGFull HRG
	on SLAM.HRG_Code = HRG.HRG
  	
  Where	
  Left(Spec_Code,3) in ('170','172','173','174','320')	  
  and	
  LEN(Spec_Code) > 3
  )x