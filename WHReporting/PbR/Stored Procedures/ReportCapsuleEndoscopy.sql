﻿/*
--Author: N Scattergood
--Date created: 08/05/2014
--Stored Procedure Built used for SSRS Report on Capsule Endoscopy 
--This is based on a query written by John Cleworth (no RD or Report Spec
--Switched to Stored Proc at the beginning of 1415
--
*/

Create Procedure [PbR].[ReportCapsuleEndoscopy]
@DateFrom as Date
,@DateTo  as Date

as

SELECT 
	   CreationDate,
       PerformanceDate,
       DateOfBirth,
       Replace(PatientNo1, ';1', '')          AS PatientNumber,
       PatientAddress1,
       Postcode,
       SocialSecurityNo,
       RegisteredGpPracticeCode,
       Replace(AttendingPhysician1, ';1', '') AS AttendingPhysician,
       ExamCode1,
       ExamDescription,
       OrderingLocationCode,
       OrderingPhysicianCode,
       PatientTypeCode
       
FROM   wholap.DBO.baseorder

WHERE  
	   PerformanceDate BETWEEN @DateFrom AND @DateTo
       AND 
       EXAMCODE1 IN ( 'YSBCE' )
       AND 
       performancelocation IS NOT NULL
       
ORDER  BY 
	      PerformanceDate,
          Replace(PatientNo1, ';1', ''),
          CaseTypeCode