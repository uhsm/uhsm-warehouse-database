﻿/*
--Author: N Scattergood
--Date created: 26/02/2014
--Stored Procedure Built used for SSRS Report on Maternity Pathways Report (RD423)
*/


Create Procedure [PbR].[ReportCountryofBirth]
  
 @StartDate	as Date,
 @EndDate	as Date 
 
AS
SELECT 

	  Cal.TheMonth as DischargeMonth 
	  ,right(Cal.FinancialMonthKey,2) as FinMonth
	  ,country.DESCRIPTION AS CountryOfBirth

      ,S.[SourceSpellNo] as SpellNumber
      ,[FaciltyID] as DistrictNumber
      ,S.[NHSNo] as NHSNumber
      ,S.[AdmissionDate]
      ,S.[DischargeDate]
        ,[LengthOfSpell]
      ,P.PatientForename
      ,P.PatientSurname
      ,P.PatientAddress1
      ,P.PatientAddress2
      ,P.PatientAddress3
      ,P.PatientAddress4
      ,P.PostCode
      ,S.GPCode
      ,S.GP
      ,S.PracticeCode
      ,S.Practice
      ,GP.[Contact Telephone Number]
      ,S.ResponsibleCommissionerCode
      ,S.ResponsibleCommissioner
      ,[Age]
      ,P.DateOfBirth
      ,P.DateOfDeath
      ,[AdmissionSpecialtyCode(Function)]
      ,[AdmissionSpecialty(Function)]
      ,[AdmissionConsultantName]
      ,S.PatientClass
      --,[AdmissiontWardCode]
      ,[AdmissionWard]
      --,[DischargeWardCode]
     ,Isnull(W.WardCode ,'') as CurrentWard
     ,Isnull(E.ConsultantName,'') as CurrentConsultant 
     ,isnull(E.[Specialty(Function)],'') as CurrentSpecialty
      ,[DischargeWard]

      
FROM WHREPORTING.APC.Spell S

LEFT JOIN (
      SELECT *
      FROM [Lorenzo].[dbo].[Patient]
      WHERE archv_flag = 'N'--this is the raw extract so changed records will come through with an archive flag of ‘Y’ which is why we need this logic added
      ) pat ON S.SourcePatientNo = pat.Patnt_refno
      
LEFT JOIN [Lorenzo].[dbo].[ReferenceValue] country ON country.rfval_refno = pat.cntry_refno

LEFT JOIN WHREPORTING.lk.Calendar Cal
on S.DischargeDate = Cal.TheDate

left join WHREPORTING.APC.Patient P
on S.EncounterRecno=P.EncounterRecno

left join WHREPORTING.APC.WardStay W	-- identifies current ward
on W.SourceSpellNo=S.SourceSpellNo
and S.DischargeDate is null
and W.EndTime is null

left join WHREPORTING.APC.Episode E
on E.SourceSpellNo=S.SourceSpellNo
and E.EpisodeEndDate is null
and S.DischargeDate is null

left join OrganisationCCG.dbo.[General Medical Practice] GP
on S.PracticeCode=GP.[Organisation Code]


where
S.AdmissionDate between @StartDate and @EndDate
and
S.InpatientStayCode <> 'B'
and 
country.DESCRIPTION not in (' ','England','Great Britain','Isle of Man','Northern Ireland','Scotland','United Kingdom','UK','Wales')

--Or S.DischargeDate is null
order by
 isnull(S.DischargeDate,getdate()) desc ,
 --S.DischargeDate desc, 
 S.admissionDate desc,Country.DESCRIPTION