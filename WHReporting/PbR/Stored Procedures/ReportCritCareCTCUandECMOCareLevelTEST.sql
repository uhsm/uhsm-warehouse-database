﻿CREATE PROCEDURE [PbR].[ReportCritCareCTCUandECMOCareLevelTEST]
--For testing to extract 2014/15 Level 2 and 3 days Adhoc from Julian Barker via CBH for Carol B 
AS 
DECLARE @FY as varchar(20),
@start AS DATETIME,
@PrevFY varchar(20)


SET @FY = (SELECT FinancialYear
           FROM   WHReporting.Lk.calendar
           WHERE  TheDate = CONVERT(DATE, Getdate())) 

set @PrevFY = (select FinancialYear from WHReporting.Lk.calendar
where TheDate  =  convert(date,dateadd(year,-1,GetDate())))

SET @start =  (select MIN(TheDate) from WHReporting.Lk.calendar
where FinancialYear = @PrevFY)

;
/*This next section no longer required as the columns PrevFY and CurrentFY have been added to the actual ECMO table. going forwards, Nick to populate these columns when populating the tables*/
--/* Temporarily Create ECMO table to identify those ECMO days which relate to Prev Year and those ECMO days which relate to this financial year.
--going Forwards - see if Nick and put this into the ECMO table as additional columns.
--*/
--IF Object_id('tempdb..#ECMO') > 1
--  DROP TABLE #ECMO

--SELECT SourceSpellNo,
--       CriticalCareDays,
--       CASE
--         WHEN SourceSpellNo = '150421422' THEN '45'
--         WHEN SourceSpellNo = '150430042' THEN '14'
--         WHEN SourceSpellNo = '150379173' THEN '16'
--         ELSE 0
--       END AS PrevFY,
--       CASE
--         WHEN SourceSpellNo = '150421422' THEN '7'
--         WHEN SourceSpellNo = '150430042' THEN '9'
--         WHEN SourceSpellNo = '150379173' THEN '0'
--         ELSE CriticalCareDays
--       END AS CurrentFY
--INTO   #ECMO
--FROM   PbR2013.dbo.ECMO;

/*This is part of the Access Database Data Accrediation procedure however it does not look like it gets used further down the line so no need for this temp table to be created
--Get Critical Care part of SLAM import file
DROP TABLE #CC

SELECT *
INTO   #CC
FROM   PbR2013.dbo.SLAMInpatientAndCustomPODBase
WHERE  POD_Code = 'Crit'
*/
--Get IP Base Data - script as per instructions in the Data Accreditation file
--In the Access database this simply extracts all records from PbrCut where the EncounterType is IP rather than just extracting the Critical Care records as the cardiac category recorded for the spell
--used  further down in this script
WITH IP (period, FinancialMonthKey,CommissionerCodeCCG, ModelTypeCode, EncounterTypeCode, AdmissionDate, DischargeDate, DistrictNo, PbRExcluded, ExclusionReason, ExclusionType, TreatmentFunctionCode, PODCode, NHSNumber, ServiceCode, HRGCode, CriticalCareDays, BestPracticeCode, ProviderSpellNo, CardiacCategory, ExcessBedDays, TrimPoint, SpellLOS, Cases, AdmissionMethodCode, DischargeMethodCode, PatientForename, PatientSurname, DateOfBirth, Postcode, ConsultantCode)
 AS (
 
  --1415 data 
          SELECT  period,
				cal.FinancialMonthKey,
                CommissionerCodeCCG,
                ModelTypeCode,
                EncounterTypeCode,
                AdmissionDate,
                DischargeDate,
                DistrictNo,
                PbRExcluded,
                ExclusionReason,
                ExclusionType,
                TreatmentFunctionCode,
                PODCode,
                NHSNumber,
                case when ServiceCode in ('RESP_ECMO','TRANSP_ECMO') then 'ECMO' Else ServiceCode End as Servicecode,
                HRGCode,
                CriticalCareDays,
                BestPracticeCode,
                ProviderSpellNo,
                CardiacCategory,
                ExcessBedDays,
                TrimPoint,
                sum(SpellLOS),
                Cases,
                AdmissionMethodCode,
                DischargeMethodCode,
                PatientForename,
                PatientSurname,
                DateOfBirth,
                Postcode,
                ConsultantCode
		FROM   Pbr2014.dbo.PbRcut
		Left outer join WHReporting.LK.Calendar cal on dateadd(day, datediff(day, 0,coalesce(
		 PbRcut.DischargeDate
		,PbRcut.EncounterEndDate
		,PbRcut.EncounterStartDate) ), 0)= cal.TheDate
         WHERE  EncounterTypeCode = 'IP'
         
        Group by  period,
				cal.FinancialMonthKey,
                CommissionerCodeCCG,
                ModelTypeCode,
                EncounterTypeCode,
                AdmissionDate,
                DischargeDate,
                DistrictNo,
                PbRExcluded,
                ExclusionReason,
                ExclusionType,
                TreatmentFunctionCode,
                PODCode,
                NHSNumber,
                case when ServiceCode in ('RESP_ECMO','TRANSP_ECMO') then 'ECMO' Else ServiceCode End,
                HRGCode,
                CriticalCareDays,
                BestPracticeCode,
                ProviderSpellNo,
                CardiacCategory,
                ExcessBedDays,
                TrimPoint,
                Cases,
                AdmissionMethodCode,
                DischargeMethodCode,
                PatientForename,
                PatientSurname,
                DateOfBirth,
                Postcode,
                ConsultantCode
  --union all
  -- --1516 data  
  --      SELECT  period,
		--		cal.FinancialMonthKey,
  --              CommissionerCodeCCG,
  --              ModelTypeCode,
  --              EncounterTypeCode,
  --              AdmissionDate,
  --              DischargeDate,
  --              DistrictNo,
  --              PbRExcluded,
  --              ExclusionReason,
  --              ExclusionType,
  --              TreatmentFunctionCode,
  --              PODCode,
  --              NHSNumber,
  --              case when ServiceCode in ('RESP_ECMO','TRANSP_ECMO') then 'ECMO' Else ServiceCode End as Servicecode,
  --              HRGCode,
  --              CriticalCareDays,
  --              BestPracticeCode,
  --              ProviderSpellNo,
  --              CardiacCategory,
  --              ExcessBedDays,
  --              TrimPoint,
  --              sum(SpellLOS),
  --              Cases,
  --              AdmissionMethodCode,
  --              DischargeMethodCode,
  --              PatientForename,
  --              PatientSurname,
  --              DateOfBirth,
  --              Postcode,
  --              ConsultantCode
		--FROM   Pbr2015.dbo.PbRcut
		--Left outer join WHReporting.LK.Calendar cal on dateadd(day, datediff(day, 0,coalesce(
		-- PbRcut.DischargeDate
		--,PbRcut.EncounterEndDate
		--,PbRcut.EncounterStartDate) ), 0)= cal.TheDate
  --       WHERE  EncounterTypeCode = 'IP'
         
  --      Group by  period,
		--		cal.FinancialMonthKey,
  --              CommissionerCodeCCG,
  --              ModelTypeCode,
  --              EncounterTypeCode,
  --              AdmissionDate,
  --              DischargeDate,
  --              DistrictNo,
  --              PbRExcluded,
  --              ExclusionReason,
  --              ExclusionType,
  --              TreatmentFunctionCode,
  --              PODCode,
  --              NHSNumber,
  --              case when ServiceCode in ('RESP_ECMO','TRANSP_ECMO') then 'ECMO' Else ServiceCode End,
  --              HRGCode,
  --              CriticalCareDays,
  --              BestPracticeCode,
  --              ProviderSpellNo,
  --              CardiacCategory,
  --              ExcessBedDays,
  --              TrimPoint,
  --              Cases,
  --              AdmissionMethodCode,
  --              DischargeMethodCode,
  --              PatientForename,
  --              PatientSurname,
  --              DateOfBirth,
  --              Postcode,
  --              ConsultantCode
         
         ),
     --Replicate the query Qry Crit Care Level by SpellNo in Access database.
     --Query is to get the Care levels per spell from the Critical Care Dataset on Lorenzo.
     --The Access query uses the old tables on infosql. 
     --This script now uses the tables from Lorenzo and WH Reporting. Checks were done to ensure the two match
     --Query creates and initial temp table #careLevel to get the bed days into the right categories i.e. level 3, level 2, etc. 
     --and also gives other columns to count the breakdown of these levels between prev and current year
     --then creates a final temp table to sum the beddays for each level by spell
     CareLevel (SourceSpellNo, Care_Level, CCPPeriod, BedDays)
     AS (SELECT CritP.SourceSpellNo,
                Stay.Care_Level,
                CASE
                  WHEN Stay.StayDate < @start THEN 'PrevYear'
                  WHEN Stay.StayDate >= @start THEN 'CurrentYear'
                  ELSE NULL
                END          CCPPeriod,
                --Stay.StayDate,
                Sum(CCPStay) AS BedDays
         FROM   Lorenzo.dbo.ExtractCriticalCarePeriodStay Stay
                LEFT OUTER JOIN WHREporting.APC.CriticalCarePeriod CritP
                             ON CritP.SourceCCPNo = Stay.SourceCCPNo
                LEFT OUTER JOIN WHReporting.APC.SPELL SPL --need to 
                             ON CritP.SourceSpellNo = SPL.SourceSpellNo
         WHERE  SPONT_REFNO_CODE IN ( 'CTCU', 'CT Transplant' )
                AND SPL.DischargeDate >= @start
         GROUP  BY CritP.SourceSpellNo,
                   Stay.Care_Level,
                   CASE
                     WHEN Stay.StayDate < @start THEN 'PrevYear'
                     WHEN Stay.StayDate >= @start THEN 'CurrentYear'
                     ELSE NULL
                   END),
     Care (SourceSpellNo, Level3, Level2, OtherLevel, PrevLevel3, PrevLevel2, PrevOthLevel, CurrentLevel3, CurrentLevel2, CurrentOthLevel)
     AS
     --temp table to put sum bed days for each level by Spell
     (SELECT SourceSpellNo,
             Sum(CASE
                   WHEN Care_Level = 3 THEN BedDays
                   ELSE NULL
                 END) AS Level3,
             Sum(CASE
                   WHEN Care_Level = 2 THEN BedDays
                   ELSE NULL
                 END) AS Level2,
             Sum(CASE
                   WHEN Care_Level IS NULL THEN BedDays
                   ELSE NULL
                 END) AS OtherLevel,
             Sum(CASE
                   WHEN Care_Level = 3
                        AND CCPPeriod = 'PrevYear' THEN BedDays
                   ELSE NULL
                 END) AS PrevLevel3,
             Sum(CASE
                   WHEN Care_Level = 2
                        AND CCPPeriod = 'PrevYear' THEN BedDays
                   ELSE NULL
                 END) AS PrevLevel2,
             Sum(CASE
                   WHEN Care_Level IS NULL
                        AND CCPPeriod = 'PrevYear' THEN BedDays
                   ELSE NULL
                 END) AS PrevOthLevel,
             Sum(CASE
                   WHEN Care_Level = 3
                        AND CCPPeriod = 'CurrentYear' THEN BedDays
                   ELSE NULL
                 END) AS CurrentLevel3,
             Sum(CASE
                   WHEN Care_Level = 2
                        AND CCPPeriod = 'CurrentYear' THEN BedDays
                   ELSE NULL
                 END) AS CurrentLevel2,
             Sum(CASE
                   WHEN Care_Level IS NULL
                        AND CCPPeriod = 'CurrentYear' THEN BedDays
                   ELSE NULL
                 END) AS CurrentOthLevel
      FROM   CareLevel
      GROUP  BY SourceSpellNo),
     SPCL(Period, FinancialMonthKey,CommissionerCodeCCG, ProviderSpellNo, TreatmentFunctionCode, AdmissionDate, DischargeDate, DistrictNo, PodCode, NHSNumber, CriticalCareDays, TrimPoint, ExcessBedDays, BestPracticeCode, ModelTypeCode, CardiacCategory, ExclusionReason, SourceSpellNo, Level3, Level2, OtherLevel, PrevLevel3, PrevLevel2, PrevOthLevel, CurrentLevel3, CurrentLevel2, CurrentOthLevel)
     AS
     --Replicates Access query Cardiac Crit qry1 NEW
     --This query joins together the Critical Care records taken from #IP table above (where the pod is CRIT) which is based on wardstay data and the Critical Care Care Level data from the Critical Care Dataset
     -- i.e. the two temp tables created above
     (SELECT IP.Period,
			IP.FinancialMonthKey,
             IP.CommissionerCodeCCG,
             IP.ProviderSPellNo,
             IP.TreatmentFunctionCode,
             IP.AdmissionDate,
             IP.DischargeDate,
             IP.DistrictNo,
             IP.PodCode,
             IP.NHSNumber,
             IP.CriticalCareDays,
             IP.TrimPoint,
             IP.ExcessBedDays,
             IP.BestPracticeCode,
             IP.ModelTypeCode,
             IP.CardiacCategory,
             IP.ExclusionReason,
             care.SourceSpellNo,
             care.Level3,
             care.Level2,
             care.OtherLevel,
             care.PrevLevel3,
             care.PrevLevel2,
             care.PrevOthLevel,
             care.CurrentLevel3,
             care.CurrentLevel2,
             care.CurrentOthLevel
      FROM   IP
             LEFT OUTER JOIN care
                          ON IP.ProviderSpellNO = care.SourceSpellNo
             
      WHERE  PodCode = 'CRIT'),
     --Replicates the Access Query - Cardiac Category New
     --this temp table Identifies all the Spells which have a Cardiac Category recorded
     Card ( ProviderSpellNo, CardiacCategory)
     AS (SELECT ProviderSpellNo,
                CardiacCategory
         FROM   IP
         WHERE  cardiaccategory IS NOT NULL
         GROUP  BY ProviderSpellNo,
                   CardiacCategory),
     --Replicates Access Query: Excluded Reason New
     --This temp table identifies all the Critical Care records which has an exclusion reason which is used later to determine whether the activity is to be included or excluded
     EXC (ModelTypeCode, Podcode, ProviderSpellNo, ExclusionReason)
     AS (SELECT DISTINCT ModelTypeCode,
                         Podcode,
                         ProviderSpellNo,
                         ExclusionReason
         FROM   IP
         WHERE  ExclusionReason IN ( 'Transplant', 'VAD', 'TAVI', 'Cystic Fibrosis', 'Overseas Patient' )
                AND PodCode = 'CRIT'),
     /*This table is not part of the previous Access database process.
     It has been created to facilitate the identification of the Prev and Current year bed days for the CICU Critical care activity 
     as finance want to be able to distinguish between the two.
     As the base data (i.e. #IP temp table ) does not have the ward stay start and end details and already has all the wardstay days summed together by spell - 
      - it is neccesary to join back onto the WH.APC.Wardstay table to get this information.
     Bear in mind that the SpellLOS will already be a sum of all the wardstays which will come through when joining back to the APC.Wardstays table.
     Then the previous year and current year bed days are calculated accordingly i.e where ward start < 01/04 then taken 01/04 for the current year bed day calculation,
     where the start and end date is  within this financial year calculate as normal - Datediff(dd, starttime, endtime)
     In the #IP table - because it is taken from the PbrCut table which already has all the wardstay days summed together by spell - i
     */
     PrevCurDays (Period, ServiceCode, ProviderSpellNo, SourceUniqueID, CICU, StartTime, EndTime, PrevYearStart, PrevYearEnd, PrevYearDays, CurStart, CurEnd, CurDays)
     AS (SELECT Period,
                CASE
                  WHEN ProviderSpellNo = '150346895'
                       AND serviceCode = 'EXTR' THEN 'CICU'
                  ELSE Servicecode
                END      AS ServiceCode,--this logic has been added to get patient 150346895 into the CICU bed days count as it should be being included 
                ProviderSpellNo,
                WS.SourceUniqueID,
                SpellLOS CICU,
                StartTime,
                EndTime,
                CASE
                  WHEN StartTime < @start THEN StartTime
                  WHEN StartTime < @start
                       AND EndTime < @start THEN StartTime
                  ELSE NULL
                END      AS PrevYearStart,
                CASE
                  WHEN StartTime < @start
                       AND EndTime >= @start THEN Dateadd(minute, -1, @start)
                  WHEN StartTime < @start
                       AND EndTime < @start THEN EndTime
                  ELSE NULL
                END      AS PrevYearEnd,
                CASE
                  WHEN StartTime < @start
                       AND EndTime < @start THEN Datediff(DD, Starttime, EndTime) + 1
                  WHEN StartTime < @start
                       AND EndTime >= @start THEN Datediff(DD, StartTime, @start)
                  ELSE 0
                END      AS PrevYearDays,
                CASE
                  WHEN StartTime >= @start
                       AND EndTime >= @start THEN StartTime
                  WHEN StartTime < @start
                       AND EndTime >= @start THEN @start
                  ELSE NULL
                END      AS CurStart,
                CASE
                  WHEN StartTime >= @start
                       AND EndTime >= @start THEN EndTime
                  WHEN StartTime < @start
                       AND EndTime >= @start THEN EndTime
                  ELSE NULL
                END      AS CurEnd,
                CASE
                  WHEN StartTime >= @start
                       AND EndTime >= @start THEN Datediff(dd, StartTime, EndTime) + 1
                  WHEN StartTime < @start
                       AND EndTime >= @start THEN Datediff(dd, @start, EndTime) + 1
                  ELSE 0
                END      AS CurDays
         FROM   IP
                LEFT OUTER JOIN (SELECT *
                                 FROM   WH.APC.WardStay
                                        INNER JOIN WH.PAS.ServicePoint
                                                ON ServicePoint.ServicePointCode = WardStay.WardCode
                                        INNER JOIN WH.dbo.EntityXref WardServiceMap
                                                ON WardServiceMap.EntityTypeCode = 'WARD'
                                                   AND WardServiceMap.XrefEntityTypeCode = 'CCSERVICE'
                                                   AND WardServiceMap.EntityCode = ServicePoint.ServicePointLocalCode
                                 WHERE  WardStay.EndTime IS NOT NULL) WS
                             ON IP.ProviderSpellNo = WS.SourceSpellNo
                                AND IP.ServiceCode = WS.XrefEntityCode
         WHERE  Podcode = 'CRIT'
                AND ServiceCode IN ( 'CICU', 'EXTR' ) -- have included EXTR as there appears to be one spell which has a servicecode of EXTR that needs to be included to get it to match the Access database process.
        --the EXTR for this record is applied as part of the manualexclusioninclusion table
        ),
     --Replicates Access query - Crit beddays by Spell no & crit ward NEW
     --This query gets all the ECMO and CICU days per spell, taking into account the breakdown for previous and current years
     --using the temp table created above #prevCurDays to get the beddays for the previous and current year
     CCbyWard (Period, ProviderSpellNo, ECMO, CICU, ECMOPrevYear, ECMOCurrentYear, CICUPrevYear, CICUCurrentYear )
     AS (SELECT IP.Period,
                IP.ProviderSpellNo,
                ECMO,
                CICU,
                Isnull(ECMOPrevYear, 0)    AS ECMOPrevYear,
                Isnull(ECMOCurrentYear, 0) AS ECMOCurrentYear,
                Isnull(CASE
                         WHEN ECMOPrevYear > PrevYearCICU THEN 0
                         ELSE PrevYearCICU - COALESCE (ECMOPrevYear, 0)
                       END, 0)             AS CICUPrevYear,
                Isnull(CASE
                         WHEN ECMOCurrentYear > CurCICU THEN 0
                         ELSE CurCICU - COALESCE (ECMOCurrentYear, 0)
                       END, 0)             AS CICUCurrentYear
         FROM   IP
                LEFT OUTER JOIN (SELECT Period,
                                        ProviderSpellNo,
                                        Sum(SpellLOS)                         ECMO,
                                        Isnull(Sum(PrevFY), 0)                AS ECMOPrevYear,
                                        Isnull(Sum(CurrentFY), Sum(SpellLOS)) AS ECMOCurrentYear
                                 FROM   IP
                                        LEFT OUTER JOIN (Select SourceSpellNo, CriticalCareDays,Resp_PrevFY + Transplant_PrevFY as PrevFY,   Resp_CurFY + Transplant_CurFY as CurrentFY from PbR2014.dbo.ECMO
														union all
														Select SourceSpellNo, CriticalCareDays,Resp_PrevFY + Transplant_PrevFY as PrevFY,   Resp_CurFY + Transplant_CurFY as CurrentFY from PbR2015.dbo.ECMO
														) ECMO
                                        --pbr2013.dbo.ECMO
                                                     ON IP.ProviderspellNo = ECMO.SourceSpellNo
                                 WHERE  Podcode = 'CRIT'
                                        AND ServiceCode IN ( 'ECMO' , 'RESP_ECMO','TRANSP_ECMO')
                                        and ProviderSpellNo <> '150346895'--not including the ECMO days for this ProviderSpell as the CICU days and ECMO days as to be excluded and included respictively as per Judith notes in the month 5 file processed from Access
                                 GROUP  BY Period,
                                           ProviderSpellNo) ECMO
                             ON IP.ProviderSpellNo = ECMO.ProviderSpellNo
                LEFT OUTER JOIN (SELECT Period,
                                        ProviderSpellNo,
                                        CICU,
                                        Sum(PrevYearDays) AS PrevYearCICU,
                                        Sum(CurDays)      AS CurCICU
                                 FROM   PrevCurDays
                                 GROUP  BY Period,
                                           ProviderSpellNo,
                                           CICU) CICU
                             ON IP.ProviderSpellNo = CICU.ProviderSpellNo
         WHERE  PodCode = 'CRIT'
                AND ServiceCode IN ( 'CICU', 'ECMO', 'EXTR',  'RESP_ECMO','TRANSP_ECMO' )
         GROUP  BY IP.Period,
                   IP.ProviderSpellNo,
                   ECMO,
                   CICU,
                   ECMOPrevYear,
                   ECMOCurrentYear,
                   Isnull(CASE
                            WHEN ECMOPrevYear > PrevYearCICU THEN 0
                            ELSE PrevYearCICU - COALESCE (ECMOPrevYear, 0)
                          END, 0),
                   Isnull(CASE
                            WHEN ECMOCurrentYear > CurCICU THEN 0
                            ELSE CurCICU - COALESCE (ECMOCurrentYear, 0)
                          END, 0)),
 --these are records which have been investigated by Judith and need to be excluded/amended somehow.
ManualExc (ProviderSpellNo,ColumnToBeAmended, Value)
as 
(Select '150430702' as ProviderSpellNo, 'CritExclusionFlag' ColumnToBeAmended,	'1' Value
union all
Select '150435649','CritExclusionFlag','1'
union all
Select '150451395',	'CritExclusionFlag',	'1'
union all
Select '150346895',	'CritExclusionFlag',	'1'
union all
Select '150441027',	'Category'	,'Gen Card')
                       
                           
                      
--Replicates Access query -Cardiac Crit qry2 NEW
--Takes the records in the temp table #SPCL which details all the Crit Care records (from #IP) and links it to the temp table that has the care level details (from temp table #care)
--Left joins to the cardiology category table to get the cardiology category for that Crit Care stay
--Left joins to the exclusions table to get the exclusion reason for the crit care record - exclusion reason is used to determine whether the Crit care days are PBR included or excluded etc - as per log in the Data Accreditation for the Access Database process
--There are a series of Case statements which will work out the UnknownCareLevel, PrevUnknownCareLevel and CurrentUknownCareLevel. These are determined by where the sum of the ECMo and CICU days is greater than the sum of the 3 dare level bed days, then the difference between the two is entered into the UnknownCareLevel field to make the 2 sum totals match.
--There is also a series of case statements which will work out the re-calculated bed days under each care level.  This applies where the sum of the CICU and ECMO bed days is less than the sum of the Care Level bed days.
--In these instances, the difference between the two is subtracted from the care levels. If there are bed days recorded under the OtherLevel category then the difference is subtracted from here, if there are no bed days recorded in the OtherLevel category then the difference will be subtracted from the Level2 category bed days,
--if there are no beds days recorded under the OtherLevel or Level2 categories then the difference is subtracted from the Level3 bed days.

SELECT DISTINCT SPCL.Period,
                SPCL.DistrictNo,
                SPCL.ProviderSpellNo,
                Case when MEx.ProviderSpellNo is not null and MEx.ColumnToBeAmended = 'Category' then MEx.Value Else
                ISNULL(card.CardiacCategory, 'Core Activity') End as Category,
                exc.ExclusionReason,
                CICU,
                ECMO,
                Isnull(CICU, 0) + Isnull(ECMO, 0) AS TotalBD,
                Isnull(Level2, 0) + Isnull(Level3, 0)
                + Isnull(OtherLevel, 0)           AS TotalCL,
                CASE
                  WHEN Isnull(CICU, 0) + Isnull(ECMO, 0) > Isnull(Level2, 0) + Isnull(Level3, 0)
                                                           + Isnull(OtherLevel, 0) THEN ( Isnull(CICU, 0) + Isnull(ECMO, 0) ) - ( Isnull(Level2, 0) + Isnull(Level3, 0)
                                                                                                                                  + Isnull(OtherLevel, 0) )
                  ELSE NULL
                END                               AS UnknownLevel,
                CASE
                  WHEN Isnull(CICU, 0) + Isnull(ECMO, 0) < Isnull(Level2, 0) + Isnull(Level3, 0)
                                                           + Isnull(OtherLevel, 0)
                       AND OtherLevel IS NOT NULL THEN OtherLevel - ( ( Isnull(Level2, 0) + Isnull(Level3, 0)
                                                                        + Isnull(OtherLevel, 0) ) - ( Isnull(CICU, 0) + Isnull(ECMO, 0) ) )
                  ELSE OtherLevel
                END                               AS OtherLevel,
                CASE
                  WHEN Isnull(CICU, 0) + Isnull(ECMO, 0) < Isnull(Level2, 0) + Isnull(Level3, 0)
                                                           + Isnull(OtherLevel, 0)
                       AND OtherLevel IS NULL
                       AND Level2 IS NOT NULL THEN Level2 - ( ( Isnull(Level2, 0) + Isnull(Level3, 0)
                                                                + Isnull(OtherLevel, 0) ) - ( Isnull(CICU, 0) + Isnull(ECMO, 0) ) )
                  ELSE Level2
                END                               AS Level2,
                -- Level2 as OriginalLevel2,
                CASE
                  WHEN Isnull(CICU, 0) + Isnull(ECMO, 0) < Isnull(Level2, 0) + Isnull(Level3, 0)
                                                           + Isnull(OtherLevel, 0)
                       AND OtherLevel IS NULL
                       AND Level2 IS NULL
                       AND Level3 IS NOT NULL THEN Level3 - ( ( Isnull(Level2, 0) + Isnull(Level3, 0)
                                                                + Isnull(OtherLevel, 0) ) - ( Isnull(CICU, 0) + Isnull(ECMO, 0) ) )
                  ELSE Level3
                END                               AS Level3,
                -- Level3 as OriginalLevel3,
                ECMOPrevYear,
                CICUPrevYear,
                Isnull(CICUPrevYear, 0)
                + Isnull(ECMOPrevYear, 0)         AS PrevTotalBD,
                Isnull(PrevLevel2, 0) + Isnull(PrevLevel3, 0)
                + Isnull(PrevOthLevel, 0)         AS PrevTotalCL,
                CASE
                  WHEN Isnull(CICUPrevYear, 0)
                       + Isnull(ECMOPrevYear, 0) > Isnull(PrevLevel2, 0) + Isnull(PrevLevel3, 0)
                                                   + Isnull(PrevOthLevel, 0) THEN ( Isnull(CICUPrevYear, 0)
                                                                                    + Isnull(ECMOPrevYear, 0) ) - ( Isnull(PrevLevel2, 0) + Isnull(PrevLevel3, 0)
                                                                                                                    + Isnull(PrevOthLevel, 0) )
                  ELSE NULL
                END                               AS PrevUnknownLevel,
                CASE
                  WHEN Isnull(CICUPrevYear, 0)
                       + Isnull(ECMOPrevYear, 0) < Isnull(PrevLevel2, 0) + Isnull(PrevLevel3, 0)
                                                   + Isnull(PrevOthLevel, 0)
                       AND PrevOthLevel IS NOT NULL THEN PrevOthLevel - ( ( Isnull(PrevLevel2, 0) + Isnull(PrevLevel3, 0)
                                                                            + Isnull(PrevOthLevel, 0) ) - ( Isnull(CICUPrevYear, 0)
                                                                                                            + Isnull(ECMOPrevYear, 0) ) )
                  ELSE PrevOthLevel
                END                               AS PrevOthLevel,
                CASE
                  WHEN Isnull(CICUPrevYear, 0)
                       + Isnull(ECMOPrevYear, 0) < Isnull(PrevLevel2, 0) + Isnull(PrevLevel3, 0)
                                                   + Isnull(PrevOthLevel, 0)
                       AND PrevOthLevel IS NULL
                       AND PrevLevel2 IS NOT NULL THEN PrevLevel2 - ( ( Isnull(PrevLevel2, 0) + Isnull(PrevLevel3, 0)
                                                                        + Isnull(PrevOthLevel, 0) ) - ( Isnull(CICUPrevYear, 0)
                                                                                                        + Isnull(ECMOPrevYear, 0) ) )
                  ELSE PrevLevel2
                END                               AS PrevLevel2,
                CASE
                  WHEN Isnull(CICUPrevYear, 0)
                       + Isnull(ECMOPrevYear, 0) < Isnull(PrevLevel2, 0) + Isnull(PrevLevel3, 0)
                                                   + Isnull(PrevOthLevel, 0)
                       AND PrevOthLevel IS NULL
                       AND PrevLevel2 IS NULL
                       AND PrevLevel3 IS NOT NULL THEN PrevLevel3 - ( ( Isnull(PrevLevel2, 0) + Isnull(PrevLevel3, 0)
                                                                        + Isnull(PrevOthLevel, 0) ) - ( Isnull(CICUPrevYear, 0)
                                                                                                        + Isnull(ECMOPrevYear, 0) ) )
                  ELSE PrevLevel3
                END                               AS PrevLevel3,
                --PrevOthLevel as OriginalPrevOthLevel,
                --PrevLevel2 as OriginalPrevLevel2,
                --PrevLevel3 as OriginalPrevLevel3,
                ECMOCurrentYear,
                CICUCurrentYear,
                Isnull(CICUCurrentYear, 0)
                + Isnull(ECMOCurrentYear, 0)      AS CurrentTotalBD,
                Isnull(CurrentLevel2, 0)
                + Isnull(CurrentLevel3, 0)
                + Isnull(CurrentOthLevel, 0)      AS CurrentTotalCL,
                CASE
                  WHEN Isnull(CICUCurrentYear, 0)
                       + Isnull(ECMOCurrentYear, 0) > Isnull(CurrentLevel2, 0)
                                                      + Isnull(CurrentLevel3, 0)
                                                      + Isnull(CurrentOthLevel, 0) THEN ( Isnull(CICUCurrentYear, 0)
                                                                                          + Isnull(ECMOCurrentYear, 0) ) - ( Isnull(CurrentLevel2, 0)
                                                                                                                             + Isnull(CurrentLevel3, 0)
                                                                                                                             + Isnull(CurrentOthLevel, 0) )
                  ELSE NULL
                END                               AS CurrentUnknownLevel,
                CASE
                  WHEN Isnull(CICUCurrentYear, 0)
                       + Isnull(ECMOCurrentYear, 0) < Isnull(CurrentLevel2, 0)
                                                      + Isnull(CurrentLevel3, 0)
                                                      + Isnull(CurrentOthLevel, 0)
                       AND CurrentOthLevel IS NOT NULL THEN CurrentOthLevel - ( ( Isnull(CurrentLevel2, 0)
                                                                                  + Isnull(CurrentLevel3, 0)
                                                                                  + Isnull(CurrentOthLevel, 0) ) - ( Isnull(CICUCurrentYear, 0)
                                                                                                                     + Isnull(ECMOCurrentYear, 0) ) )
                  ELSE CurrentOthLevel
                END                               AS CurrentOthLevel,
                CASE
                  WHEN Isnull(CICUCurrentYear, 0)
                       + Isnull(ECMOCurrentYear, 0) < Isnull(CurrentLevel2, 0)
                                                      + Isnull(CurrentLevel3, 0)
                                                      + Isnull(CurrentOthLevel, 0)
                       AND CurrentOthLevel IS NULL
                       AND CurrentLevel2 IS NOT NULL THEN CurrentLevel2 - ( ( Isnull(CurrentLevel2, 0)
                                                                              + Isnull(CurrentLevel3, 0)
                                                                              + Isnull(CurrentOthLevel, 0) ) - ( Isnull(CICUCurrentYear, 0)
                                                                                                                 + Isnull(ECMOCurrentYear, 0) ) )
                  ELSE CurrentLevel2
                END                               AS CurrentLevel2,
                CASE
                  WHEN Isnull(CICUCurrentYear, 0)
                       + Isnull(ECMOCurrentYear, 0) < Isnull(CurrentLevel2, 0)
                                                      + Isnull(CurrentLevel3, 0)
                                                      + Isnull(CurrentOthLevel, 0)
                       AND CurrentOthLevel IS NULL
                       AND CurrentLevel2 IS NULL
                       AND CurrentLevel3 IS NOT NULL THEN CurrentLevel3 - ( ( Isnull(CurrentLevel2, 0)
                                                                              + Isnull(CurrentLevel3, 0)
                                                                              + Isnull(CurrentOthLevel, 0) ) - ( Isnull(CICUCurrentYear, 0)
                                                                                                                 + Isnull(ECMOCurrentYear, 0) ) )
                  ELSE CurrentLevel3
                END                               AS CurrentLevel3,
                --CurrentOthLevel as OriginalCurrentOthLevel,
                --CurrentLevel2 as OriginalCurrentLevel2,
                --CurrentLevel3 as OriginalCurrentLevel3
                CASE
					WHEN MEx.ProviderSpellNo IS NOT NULL and MEx.ColumnToBeAmended = 'CritExclusionFlag' then MEx.Value
					WHEN exc.exclusionreason IN ( 'TAVI', 'Cystic Fibrosis', 'Overseas Patient', 'Transplant' ) THEN 1
                  ELSE 0
                END                               AS CritExclusionFlag
                
FROM   SPCL
       LEFT OUTER JOIN card
                    ON SPCL.ProviderSPellNo = Card.ProviderSpellNo
       LEFT OUTER JOIN exc
                    ON SPCL.ProviderSPellNo = exc.ProviderSpellNo --and #SPCL.modeltypecode = #exc.modeltypecode
       LEFT OUTER JOIN CCbyWard
                    ON SPCL.Providerspellno = CCbyWard.ProviderSpellNo
       Left outer join ManualExc MEx on SPCL.ProviderSpellNo = MEx.ProviderSpellNo
WHERE  SPCL.podcode = 'CRIT'
       AND ( Isnull(ccbyward.ECMO, 0) > 0
              OR Isnull(ccbyward.CICU, 0) > 0 ) 
      --   and SPCL.FinancialMonthKey in  (SELECT Item FROM dbo.Split (@period, ','))
--           and SPCL.Period in (SELECT Item FROM dbo.Split (@period, ','))

union all
---Have to add this part manually as ECMO days are to be included in the Pbr count whereas the CICU days are to be excluded as per Judith notes
SELECT DISTINCT Period,
                DistrictNo,
                SPCL.ProviderSpellNo,
                Card.CardiacCategory,
                EXC.ExclusionReason,
                NULL AS CICU,
                11   AS ECMO,
                11   AS TotalBD,
                NULL AS TotalCL,
                11   AS UnknownLevel,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                11   AS ECMOCurrentYear,
                NULL,
                11   CurrentTotalBD,
                NULL AS CurrentTotalCL,
                11   AS CurrentUnknownLevel,
                NULL,
                NULL,
                NULL,
                0
FROM   SPCL
       LEFT OUTER JOIN card
                    ON SPCL.ProviderSPellNo = Card.ProviderSpellNo
       LEFT OUTER JOIN exc
                    ON SPCL.ProviderSPellNo = exc.ProviderSpellNo --and #SPCL.modeltypecode = #exc.modeltypecode
WHERE  SPCL.ProviderSpellNo = '150346895' 
--and SPCL.FinancialMonthKey in  (SELECT Item FROM dbo.Split (@period, ','))
-- and SPCL.Period in (SELECT Item FROM dbo.Split (@period, ','))