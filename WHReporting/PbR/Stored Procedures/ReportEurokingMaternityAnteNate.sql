﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Created to replicate the stored procedure WHReporting.PbR.ReportMaternityAnteNate

Notes:			Stored in WHReporting
				Stored Proc reads from the dbo.EurokingMatAnteEncounter

				If the metrics have changed then make changes to the table DW_REPORTING.HROST.Metrics
				and modify the load usp_LoadMetrics accordingly.

Versions:		
				1.0.0.0 - 13/06/2016 - CM 
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/


CREATE Procedure [PbR].[ReportEurokingMaternityAnteNate]
  
  as

SELECT RIGHT(Cal.[FinancialMonthKey], 2) AS FinMonth
	,[SourceUniqueID]
	,[NHSNumber]
	,[DistrictNumber]
	,BookingDate
	,[Pathway]
	,[Postcode]
	,[PracticeCode]
	,[CCG]
	,CCG.PCTCCG AS CCGDescription
	,[DateOfBirth]
	,[TransferIn]
	,[Parity]
	,[Gravida]
	,[Gestation At Booking]
	,[EDD]
	,[Accommodation]
	,[Age at booking]
	,[Agencies Involved]
	,[Alcohol at Booking]
	,[Antibodies]
	,[Autoimmune Disease]
	,[BMI at Booking]
	,[Broader Family Issues]
	,[Cardiac Problems]
	,[Endocrine Problems]
	,[Gastrointestinal Problems]
	,[Genetic Disorder Specialist Care]
	,[Genetic Inherited Disorder]
	,[Gynaecological  Problems or Surgery]
	,[Haematological Problems]
	,[Haemoglobinopathies]
	,[Hepatic Problems]
	,[Hepatitis B]
	,[HIV]
	,[Hypertension]
	,[Infections]
	,[Language & Literacy]
	,[Malignancy]
	,[Mental Health Problems]
	,[Mental Health Referrals or Admissions]
	,[Mental Health Secondary Care]
	,[Musculoskeletal Problems]
	,[Neurological Problems]
	,[Number of Babies on Scan]
	,[Physical Disabilities]
	,[Renal Problems]
	,[Respiratory Problems]
	,[Screening for Hepatitis B]
	,[Screening for HIV]
	,[Substance Use at Booking]
	,[Thromboembolic Disorder]
	,[POH Consecutive Miscarriages]
FROM [PbR2016].[dbo].[EurokingMatAnteEncounter]
INNER JOIN [WHREPORTING].[LK].[Calendar] Cal ON Cal.TheDate = BookingDate
	AND Cal.FinancialYear = '2016/2017'
LEFT JOIN [WHREPORTING].[LK].[PasPCTOdsCCG] CCG ON [CCG] = CCG.PCTCCGCode
ORDER BY BookingDate DESC