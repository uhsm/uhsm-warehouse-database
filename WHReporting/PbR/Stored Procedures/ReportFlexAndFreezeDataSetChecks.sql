﻿/*
--Author: N Scattergood
--Date created: 07/08/2013
--Stored Procedure Built used for checking Datasets in PbRFlexAndFreeze
*/


CREATE Procedure [PbR].[ReportFlexAndFreezeDataSetChecks]
  
  as
  Select 
  RIGHT(Cal.FinancialMonthKey,2) as [FinMonth]
  ,Cal.FinancialMonthKey
   ,EncounterTypeCode
   ,DatasetCode
   ,PBR.[PODCode] 
   ,COUNT(*) as [RowCount]
   
   FROM [WHREPORTING].[PbR].[PBRFlex] PBR
	
	left join WHREPORTING.LK.Calendar Cal
	on Cal.TheDate = PBR.[EncounterEndDate]
   
   Group by
   Cal.FinancialMonthKey
   ,EncounterTypeCode
   ,DatasetCode
   ,PBR.[PODCode] 
   
   ORDER BY
   Cal.FinancialMonthKey
    ,EncounterTypeCode
   ,DatasetCode
   ,PBR.[PODCode]