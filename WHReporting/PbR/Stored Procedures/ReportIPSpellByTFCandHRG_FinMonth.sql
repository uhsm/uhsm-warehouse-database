﻿/*
--Author: N Scattergood
--Date created: 25/09/2013
--Stored Procedure Built for SRSS Report on:
--PbR IP Spell by TFC and HRG with Drill-through Report (RD429)
--This Provides Default for Fin Months on drillthrough
*/

CREATE Procedure [PbR].[ReportIPSpellByTFCandHRG_FinMonth]
as
select 

distinct
RIGHT(Cal.[FinancialMonthKey],2) as FinMonth
FROM PbR2015.dbo.PbR  PBR

			left join [WHREPORTING].[LK].[Calendar] Cal
			on Cal.TheDate = PbR.DischargeDate

  Where 
  EncounterTypeCode = 'IP'
  and
  DatasetCode = 'ENCOUNTER'
  and 
  PbRExcluded = 0