﻿/*
--Author: N Scattergood
--Date created: 25/09/2013
--Stored Procedure Built for SRSS Report on:
--PbR IP Spell by TFC and HRG with Drill-through Report (RD429)
--This Provides Parameters
*/

CREATE Procedure [PbR].[ReportIPSpellByTFCandHRG_POD]

@TFCPar	as nvarchar(1000) 
,@ConsPAR as nvarchar(4000)
--,@PODPar as nvarchar(1000)
--,@HRGPar as nvarchar(4000)

as
SELECT 
Distinct
--S.AdmissionConsultantCode as ConsParCode
--,S.AdmissionConsultantName as ConsParDesc

	--RIGHT(Cal.[FinancialMonthKey],2) as FinMonth
-- ,PBR.[EncounterTypeCode]
--      ,PBR.[InterfaceCode]
--      ,PBR.[DatasetCode]
--,PBR.ProviderSpellNo
--,PBR.DistrictNo
--,PBR.[Age]
--,PBR.[CommissionerCodeCCG]
--,CCG.PCTCCG as CCGDescription
--,PBR.AdmissionDate
--,PBR.DischargeDate 
  PBR.[PODCode]
--   ,PBR.[TreatmentFunctionCode]
--    ,PBR.[TreatmentFunctionCodeFinal]  
--,PBR.SpellLOS
--      ,PBR.[ExcessBedDays]
--      ,PBR.[ExcessBedDaysFinal]
--      ,PBR.[TrimPoint]
      --,PBR.HRGCode
      --,HRG.[HRGDescription]
      --Spec.TFCCode
      --,Spec.TFCDesc
--,PBR.DominantProcedureCode
--,PBR.PrimaryDiagnosisCode
--,PBR.PrimaryProcedureCode
--,PBR.PrimaryProcedureDate

            
--      --,PBR.[PatientForename]
--      --,PBR.[PatientSurname]
--      --,PBR.[PatientTitle]
--       --,PBR.[SexCode]
  
--      ,PBR.[TreatmentFunctionCode]
--      --,PBR.[BestPracticeCode]
--      --,PBR.[BestPracticeSectionCode]
    
--      --,PBR.[PbRExcluded]
--      --,PBR.[CriticalCareDays]
--      --,PBR.[CardiacCategory]
--      ,PBR.[TreatmentFunctionCodeFinal]
--                         ,cc.[PrimaryDiagnosis]
--      ,cc.[SubsidiaryDiagnosis]
--      ,cc.[SecondaryDiagnosis1]
--      ,cc.[SecondaryDiagnosis2]
--      ,cc.[SecondaryDiagnosis3]
--      ,cc.[SecondaryDiagnosis4]
--      	  ,cc.[PriamryProcedure]
--      ,cc.[SecondaryProcedure1]
--      ,cc.[SecondaryProcedure2]
--      ,cc.[SecondaryProcedure3]
--      ,cc.[SecondaryProcedure4]
--      ,cc.[SecondaryProcedure5]
--,COUNT(1) as Activity

      
  FROM PbR2015.dbo.PbR   PBR
  
	--left join WHREPORTING.APC.Spell S
	--on PBR.ProviderSpellNo = S.SourceSpellNo
	
	--	left join WHREPORTING.APC.ClinicalCoding cc
	--	on S.EncounterRecno = cc.EncounterRecno
		
	--		left join [WHREPORTING].[LK].[Calendar] Cal
	--		on Cal.TheDate = PbR.DischargeDate
						
	--			left join
	--			[WHREPORTING].[LK].[PasPCTOdsCCG] CCG
	--			on PBR.[CommissionerCodeCCG] = CCG.PCTCCGCode
				
					--left join
					--[PbR2013].[dbo].[HRGLookup] HRG
					--on PBR.HRGCode = HRG.HRGCode
					
						--inner join
						--(
						--SELECT distinct
						--[SpecialtyFunctionCode] as TFCCode
						--,[SpecialtyFunction] as TFCDesc

						--  FROM [WHREPORTING].[LK].[SpecialtyDivision]
						--  where 
						--  ISNUMERIC(SpecialtyCode) = 1
						--    and
						--  ISNUMERIC(SpecialtyFunctionCode) = 1
						--) Spec
						--on Spec.TFCCode = PbR.TreatmentFunctionCodeFinal
						
  
  Where 
  EncounterTypeCode = 'IP'
  and
  DatasetCode = 'ENCOUNTER'
  and 
  PbRExcluded = 0
  ----Following Comes from Parameters---------
  and
 PbR.TreatmentFunctionCode in (SELECT Val from dbo.fn_String_To_Table(@TFCPar,',',1))
 and
 PbR.ConsultantCode in (SELECT Val from dbo.fn_String_To_Table(@ConsPAR,',',1))
 --and
 --PbR.PODCode in (SELECT Val from dbo.fn_String_To_Table(@PODPar,',',1))
 --and
 --PbR.HRGCode in  (SELECT Val from dbo.fn_String_To_Table(@HRGPAR,',',1))
 
  
  --group by
  --	RIGHT(Cal.[FinancialMonthKey],2)
  --    ,PBR.HRGCode
  --    ,HRG.[HRGDescription]
  --          ,PbR.TreatmentFunctionCodeFinal
  --    ,Spec.TFCDesc
      
      order by 
    PODCode