﻿/*
--Author: N Scattergood
--Date created: 18/03/2016
--Stored Procedure Built for Spells without CC which previously had a CC
--
*/

CREATE Procedure [PbR].[ReportIPSpellsCodedWithoutCCWithPreviousCC]

@StartDate	as Date 
,@EndDate	as Date	

as
Declare @CRLF		as varchar(2) 
SET @CRLF			=  CHAR(13)+CHAR(10)
SELECT 
PbR.DistrictNo
,PbR.[PatientSurname]
,PbR.ProviderSpellNo
,PbR.AdmissionDate
,PbR.DischargeDate
,PbR.SpellLOS
,PbR.PODCode
,PbR.TreatmentFunctionCode
,E.SpecialtyCode as EpisodeSpecialtyCode
,E.[Specialty] as EpisodeSpecialtyDesc
,E.ConsultantName as EpisodeConsultant
--,PbR.CardiacCategoryTFC
--,PbR.[CardiacCategory]
,HRG.[HRG Subchapter]
,HRG.[HRG Subchapter Description]
,PbR.HRGCode
,HRG.HRGDescription
--,HRG.CCFlag
,EstimatedCurrentTariff = 1.059353* case
when PbR.PODCode = 'NELST' 
then coalesce(TAR.[ReducedShortStayEmergencyTariff],TAR.[NonElectiveSpellTariff])
when PbR.PODCode like '%NEL%' 
then TAR.[NonElectiveSpellTariff]
when PbR.PODCode like '%DC%' 
then coalesce(TAR.[DaycaseTariff],TAR.[CombinedDaycaseElectiveTariff])
when PbR.PODCode like '%EL%' 
then coalesce(TAR.[ElectiveSpellTariff],TAR.[CombinedDaycaseElectiveTariff]) 
end
,PotentialTariff = 1.059353* case
when PbR.PODCode = 'NELST' 
then coalesce(TARCC.RSSTariffCC,TARCC.NELTariffCC)
when PbR.PODCode like '%NEL%' 
then TARCC.NELTariffCC
when PbR.PODCode like '%DC%' 
then coalesce(TARCC.DCTariffCC,TARCC.CombELTariffCC)
when PbR.PODCode like '%EL%' 
then coalesce(TARCC.ELTariffCC,TARCC.CombELTariffCC) 
end
,PotentialIncomeGain = --PotentialTariff - EstimatedCurrentTariff
(1.059353* case
when PbR.PODCode = 'NELST' 
then coalesce(TARCC.RSSTariffCC,TARCC.NELTariffCC)
when PbR.PODCode like '%NEL%' 
then TARCC.NELTariffCC
when PbR.PODCode like '%DC%' 
then coalesce(TARCC.DCTariffCC,TARCC.CombELTariffCC)
when PbR.PODCode like '%EL%' 
then coalesce(TARCC.ELTariffCC,TARCC.CombELTariffCC) 
end
)
- 
(1.059353* case
when PbR.PODCode = 'NELST' 
then coalesce(TAR.[ReducedShortStayEmergencyTariff],TAR.[NonElectiveSpellTariff])
when PbR.PODCode like '%NEL%' 
then TAR.[NonElectiveSpellTariff]
when PbR.PODCode like '%DC%' 
then coalesce(TAR.[DaycaseTariff],TAR.[CombinedDaycaseElectiveTariff])
when PbR.PODCode like '%EL%' 
then coalesce(TAR.[ElectiveSpellTariff],TAR.[CombinedDaycaseElectiveTariff]) 
end
)


      --,PbR.[ExclusionType]
      ,SameHRGSubChapter
      = case 
      when HRG.[HRG Subchapter] = SQ.[HRG Subchapter]
      then 'YES'
      else 'NO'
      end
      ,SQ.PreviousAdmissionDate
      ,SQ.PreviousDischargeDate
      ,SQ.PreviousSpecialtyDesc
      ,SQ.PreviousConsultant
      ,SQ.PreviousPOD
      ,SQ.PreviousHRG
      ,SQ.PreviousDiagnosisCoded
      ,CC.CodingComplete
      ,D.UserCreate
      ,D.UserModified
      ,D.CreateDateTime
      ,D.ModifiedDateTime
      , CurrentDiagnosisCoded
       = isnull(cc.[PrimaryDiagnosis],'-')+@CRLF
             +isnull(cc.[SubsidiaryDiagnosis],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis1],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis2],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis3],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis4],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis5],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis6],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis7],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis8],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis9],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis10],'-')
 
     

  FROM [PbR2016].[dbo].[PbR] PbR
  
  left join PbR2016.lk.HRGFull HRG
  on HRG.HRG = PbR.HRGCode
  
  left join PbR2016.Tariff.APCOPProcedure TAR
  on TAR.HRGCode = PbR.HRGCode
  
  left join [PbR2016].[HRG].[HRG49APCEncounter] ENC
  on PbR.ProviderSpellNo = ENC.ProviderSpellNo
  and ENC.SpellReportFlag = 1
  
  left join WHREPORTING.APC.Episode E
  on E.EpisodeUniqueID = Enc.SourceUniqueID
  
  left join WHREPORTING.APC.ClinicalCoding CC
  on CC.SourceUniqueID = ENC.SourceUniqueID
  
  left join WHREPORTING.APC.AllDiagnosis D
  on D.EpisodeSourceUniqueID = CC.SourceUniqueID
  and D.DiagnosisSequence = 1
  
  inner join--------------Sub Query for Tariff  with CC--------------------
  (
  select 
HCC.[HRG Root]
	  ,max([CombinedDaycaseElectiveTariff])	as CombELTariffCC
      ,max([DaycaseTariff])						as DCTariffCC
      ,max([ElectiveSpellTariff])				as ELTariffCC
      ,max([NonElectiveSpellTariff])			as NELTariffCC
      ,max([ReducedShortStayEmergencyTariff])	as RSSTariffCC

FROM
[PbR2016].[Tariff].[APCOPProcedure] APCT

  inner join PbR2016.lk.HRGFull HCC
  on APCT.[HRGCode] = HCC.[HRG]
  and HCC.CCFlag = 'with cc'
  
  Group By
HCC.[HRG Root]
  ) TARCC--------------End of Sub Query--------------------
  on TARCC.[HRG Root] = HRG.[HRG Root]
  
  ---------------SUB QUERY WHERE PREVIOUS ADMISSION HAS BEEN CODED WITH A CC---------------------
 inner JOIN
 (Select
--ENC.SourceUniqueID
DistrictNo
,PbR.AdmissionDate as PreviousAdmissionDate
,PbR.DischargeDate as PreviousDischargeDate
,E.SpecialtyCode as PreviousSpecialtyCode
,E.[Specialty] as PreviousSpecialtyDesc
,E.ConsultantName as PreviousConsultant
,PbR.PODCode		as PreviousPOD
,HRG.[HRG Subchapter]
,PbR.HRGCode+' '+HRG.HRGDescription PreviousHRG
--,HRG.HRGDescription as PreviousHRGDesc
,HRG.CCFlag

             , PreviousDiagnosisCoded = isnull(cc.[PrimaryDiagnosis],'-')+@CRLF
             +isnull(cc.[SubsidiaryDiagnosis],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis1],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis2],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis3],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis4],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis5],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis6],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis7],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis8],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis9],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis10],'-')+@CRLF
   

  FROM [PbR2016].[dbo].[PbR] PbR
  
  left join PbR2016.lk.HRGFull HRG
  on HRG.HRG = PbR.HRGCode
  
  left join [PbR2016].[HRG].[HRG49APCEncounter] ENC
  on PbR.ProviderSpellNo = ENC.ProviderSpellNo
  and ENC.SpellReportFlag = 1
  
  left join WHREPORTING.APC.Episode E
  on E.EpisodeUniqueID = Enc.SourceUniqueID
  
  left join WHREPORTING.APC.ClinicalCoding CC
  on CC.SourceUniqueID = ENC.SourceUniqueID
  

  
  --left join [PbR2016].[HRG].[HRG48APCInputFileExtractDump] INP
  --on ENC.[SourceUniqueID] = INP.[SourceUniqueID]
  
  WHERE 
  DatasetCode = 'Encounter'
  and
  EncounterTypeCode = 'IP'
  and 
  CCFlag = 'With CC'
  
  UNION ALL
  
 Select 
      
DistrictNo = S.FaciltyID
,S.AdmissionDate as PreviousAdmissionDate
,S.DischargeDate as PreviousDischargeDate
,E.SpecialtyCode as PreviousSpecialtyCode
,E.[Specialty] as PreviousSpecialtyDesc
,E.ConsultantName as PreviousConsultant
,PreviousPOD = case
				when S.InpatientStayCode = 'D'
				then 'DC'
				when S.AdmissionMethodNHSCode IN ('11','12','13')
				then 'EL'
				when TAR.[BestPractice] in ('1','16' )
				then
					case
					when S.LengthOfSpell < 1
					then 'NELSD'

					when
					Tar.ReducedShortStayEmergencyTariffFlag = 'Yes'
					and	S.LengthOfSpell < 2
					and	S.AdmissionMethodNHSCode between '21' and '28' 
					then 'NELST'

					when S.AdmissionMethodNHSCode not between '21' and '28' 
					then 'NELNE'
					else 'NEL'
					END
				when S.AdmissionMethodNHSCode between '21' and '28' 
				then 'NEL' +
					case
					when
					Tar.ReducedShortStayEmergencyTariffFlag = 'Yes'
					and	S.LengthOfSpell < 2
					and S.Age > 17
					then 'ST'

					else ''
					end

				when S.AdmissionMethodNHSCode not between '21' and '28' 
				then 'NELNE'	
				when S.AdmissionMethodNHSCode not in ('11','12','13') 
				then 'NEL' 	
				END
				

,HRG.[HRG Subchapter]
,ENC.HRGCode+' '+HRG.HRGDescription PreviousHRG
--,HRG.HRGDescription as PreviousHRGDesc
,HRG.CCFlag

             , PreviousDiagnosisCoded = isnull(cc.[PrimaryDiagnosis],'-')+@CRLF
             +isnull(cc.[SubsidiaryDiagnosis],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis1],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis2],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis3],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis4],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis5],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis6],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis7],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis8],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis9],'-')+@CRLF
             +isnull(cc.[SecondaryDiagnosis10],'-')+@CRLF

  FROM [PbR2016].[ActivityFiveYears].[HRG49APCEncounter_042011_032016] ENC
  
  Left Join WHREPORTING.APC.Spell S
  on S.SourceSpellNo = ENC.ProviderSpellNo
  
  left join WHREPORTING.APC.Episode E
  on E.EpisodeUniqueID = Enc.SourceUniqueID
  
  left join WHREPORTING.APC.ClinicalCoding CC
  on CC.SourceUniqueID = ENC.SourceUniqueID
  
  left join PbR2016.lk.HRGFull HRG
  on HRG.HRG = ENC.HRGCode
  
  left join PbR2016.Tariff.APCOPProcedure TAR
  on ENC.HRGCode = TAR.HRGCode
  
  
  Where 
  ENC.SpellReportFlag = 1
  and
  S.DischargeDate between '01 apr 2015' and '31 Mar 2016'
  and 
  CCFlag = 'With CC'
  ) SQ 
  on SQ.DistrictNo = PbR.DistrictNo
  and SQ.PreviousDischargeDate < PbR.DischargeDate
 
  
  WHERE 
  PbR.DatasetCode = 'Encounter'
  and
  PbR.EncounterTypeCode = 'IP'
  --and 
  --PbR.DischargeDate between @StartDate and @EndDate
  and 
  HRG.CCFlag = 'Without CC'
  and
  [ExclusionType] = 'INCLUDED'
  and
  --pbR.HRGCode not in ('UZ01Z','WA14Z')
  HRG.HRGDescription like '%without CC%'
  
  
  
Order by
PotentialIncomeGain desc,
PbR.DischargeDate,
SameHRGSubChapter,
PreviousAdmissionDate