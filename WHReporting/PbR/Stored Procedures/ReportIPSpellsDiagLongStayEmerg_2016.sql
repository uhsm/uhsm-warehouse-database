﻿/*
--Author: N Scattergood
--Date created: 08/06/2016
--Stored Procedure based Patients with a LoS >2 where they qualify for the Short Stay Emergency Tariff

--
*/

CREATE Procedure [PbR].[ReportIPSpellsDiagLongStayEmerg_2016]

@StartDate	as Date 
,@EndDate	as Date	

as
Declare @CRLF		as varchar(2) 
SET @CRLF			=  CHAR(13)+CHAR(10)

SELECT 
S.SourceSpellNo
,PbRD.DistrictNo
,PbRD.PatientSurname
                      ,S.PatientClass
                      ,S.AdmissionType
                      --,S.[AdmissionSpecialty(Function)]
                      --,S.AdmissionConsultantName
                      ,S.AdmissionDate 
                      , S.DischargeDate
                      ,S.LengthOfSpell
                      ,PbRD.EpisodeCount
                      ,TARC.NonElectiveLongStayTrimpoint
                      ,PbRC.ExcessBedDays
                                            	
                      ,PbRC.BestPracticeSectionCode
                      ,DailyPbR_HRGCode			= PbRD.HRGCode
                      ,DailyPbR_HRGDescription	= HRGD.HRGDescription
                      ,DailyPbR_CCFlag			= HRGD.CCFlag
                      ,DailyPbR_EstimatedTariff =  1.059353* case
													when PbRD.PODCode = 'NELST' 
													then coalesce(TAR.[ReducedShortStayEmergencyTariff],TAR.[NonElectiveSpellTariff])
													when PbRD.PODCode like '%NEL%' 
													then TAR.[NonElectiveSpellTariff]
													when PbRD.PODCode like '%DC%' 
													then coalesce(TAR.[DaycaseTariff],TAR.[CombinedDaycaseElectiveTariff])
													when PbRD.PODCode like '%EL%' 
													then coalesce(TAR.[ElectiveSpellTariff],TAR.[CombinedDaycaseElectiveTariff]) 
													end
                      ,DailyPbR_DominantProcCode = PbRD.DominantProcedureCode
                      --,DailyPbR_DominantProc	= OPD.[OPCS Description]

                      ,PbRCut_CensusDate		= PbRC.CensusDate
                      ,PbRCut_CutType			= PbRC.CutTypeCode
                      ,PbRCut_POD				= PbRC.PODCode
                      ,PbRCut_HRGCodeCut		= PbRC.HRGCode
					  ,PbRCut_HRGDescription	= HRGC.HRGDescription
                      ,PbRCut_CCFlag			= HRGC.CCFlag
                      ,PbRCut_EstimatedTariff	=  1.059353* case
														when PbRC.PODCode = 'NELST' 
														then coalesce(TARC.[ReducedShortStayEmergencyTariff],TARC.[NonElectiveSpellTariff])
														when PbRC.PODCode like '%NEL%' 
														then TARC.[NonElectiveSpellTariff]
														when PbRC.PODCode like '%DC%' 
														then coalesce(TARC.[DaycaseTariff],TARC.[CombinedDaycaseElectiveTariff])
														when PbRC.PODCode like '%EL%' 
														then coalesce(TARC.[ElectiveSpellTariff],TARC.[CombinedDaycaseElectiveTariff]) 
														end
					, TariffPerBedDayCut = (1.059353* case
														when PbRC.PODCode = 'NELST' 
														then coalesce(TARC.[ReducedShortStayEmergencyTariff],TARC.[NonElectiveSpellTariff])
														when PbRC.PODCode like '%NEL%' 
														then TARC.[NonElectiveSpellTariff]
														when PbRC.PODCode like '%DC%' 
														then coalesce(TARC.[DaycaseTariff],TARC.[CombinedDaycaseElectiveTariff])
														when PbRC.PODCode like '%EL%' 
														then coalesce(TARC.[ElectiveSpellTariff],TARC.[CombinedDaycaseElectiveTariff]) 
														end
										+ 1.059353*TARC.PerDayLongStayPayment*isnull(PbRC.ExcessBedDays,0)
										) / (S.LengthOfSpell +1)							
                      ,PbRCut_DominantProcCode	= PbRC.DominantProcedureCode
                      --,PbRCut_DominantProc		= OPC.[OPCS Description]
                      
                      ,HRGChange = case 
                      when PbRD.HRGCode <> PbRC.HRGCode
                      then 'Y'
                      else 'N'
                      end
                      
                      ,TariffChange =  1.059353* case
													when PbRD.PODCode = 'NELST' 
													then coalesce(TAR.[ReducedShortStayEmergencyTariff],TAR.[NonElectiveSpellTariff])
													when PbRD.PODCode like '%NEL%' 
													then TAR.[NonElectiveSpellTariff]
													when PbRD.PODCode like '%DC%' 
													then coalesce(TAR.[DaycaseTariff],TAR.[CombinedDaycaseElectiveTariff])
													when PbRD.PODCode like '%EL%' 
													then coalesce(TAR.[ElectiveSpellTariff],TAR.[CombinedDaycaseElectiveTariff]) 
													end 
											-		 1.059353* case
														when PbRC.PODCode = 'NELST' 
														then coalesce(TARC.[ReducedShortStayEmergencyTariff],TARC.[NonElectiveSpellTariff])
														when PbRC.PODCode like '%NEL%' 
														then TARC.[NonElectiveSpellTariff]
														when PbRC.PODCode like '%DC%' 
														then coalesce(TARC.[DaycaseTariff],TARC.[CombinedDaycaseElectiveTariff])
														when PbRC.PODCode like '%EL%' 
														then coalesce(TARC.[ElectiveSpellTariff],TARC.[CombinedDaycaseElectiveTariff]) 
														end
						,E.Specialty
						,E.SpecialtyCode
						,E.ConsultantName	
,D.UserModified as CodedBy
,D.ModifedDate as CodeModified
,CN.Location as NotesLocations
,CN.Notes	as NotesDetail						
                      , CurrentDiagnosisCoded
								   = isnull(cc.[PrimaryDiagnosis],'-')+@CRLF
										 +isnull(cc.[SubsidiaryDiagnosis],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis1],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis2],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis3],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis4],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis5],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis6],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis7],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis8],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis9],'-')
										 --+isnull(cc.[SecondaryDiagnosis10],'-')
					 , CurrentProceduresCoded
       = isnull(cc.PriamryProcedure,'-')+@CRLF
             +isnull(cc.[SecondaryProcedure1],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure2],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure3],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure4],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure5],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure6],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure7],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure8],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure9],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure10],'-')     
  ,ClinicalScenario = 
  Case
when 	PbRC.BestPracticeCode	 = 	'BP35'	 then 	'Epileptic seizure'
when 	PbRC.HRGCode	 = 	'AA31A'	 then 	'Acute headache'
when 	PbRC.HRGCode	 = 	'AA31B'	 then 	'Acute headache'
when 	PbRC.HRGCode	 = 	'DZ15E'	 then 	'Asthma'
when 	PbRC.HRGCode	 = 	'DZ15F'	 then 	'Asthma'
when 	PbRC.HRGCode	 = 	'DZ22C'	 then 	'Lower respiratory tract infections without COPD'
when 	PbRC.HRGCode	 = 	'DZ09B'	 then 	'Pulmonary embolism'
when 	PbRC.HRGCode	 = 	'DZ09C'	 then 	'Pulmonary embolism'
when 	PbRC.BestPracticeCode	 = 	'BP40'	 then 	'Chest pain'
when 	PbRC.BestPracticeCode	 = 	'BP42'	 then 	'Appendicular fractures not requiring immediate internal fixation'
when 	PbRC.BestPracticeCode	 = 	'BP39'	 then 	'Cellulitis'
when 	PbRC.HRGCode	 = 	'LB40A'	 then 	'Renal/ureteric stones'
when 	PbRC.HRGCode	 = 	'LB40B'	 then 	'Renal/ureteric stones'
when 	PbRC.HRGCode	 = 	'QZ20Z'	 then 	'DVT'
when 	PbRC.HRGCode	 = 	'WA11X'	 then 	'Self harm'
when 	PbRC.HRGCode	 = 	'WA11Y'	 then 	'Self harm'
when 	PbRC.HRGCode	 = 	'EB08I'	 then 	'Falls including syncope or collapse'
when 	PbRC.HRGCode	 = 	'DZ11C'	 then 	'Community acquired pneumonia '
when 	PbRC.HRGCode	 = 	'EB07H'	 then 	'Supraventricular tachcardias including atrial fibrillation'
when 	PbRC.HRGCode	 = 	'EB07I'	 then 	'Supraventricular tachcardias including atrial fibrillation'
when 	PbRC.HRGCode	 = 	'HA83B'	 then 	'Minor head injury'
when 	PbRC.HRGCode	 = 	'HA83C'	 then 	'Minor head injury'
when 	PbRC.BestPracticeCode	 = 	'BP53'	 then 	'Low risk pubic rami'
when 	PbRC.HRGCode	 = 	'LB16B'	 then 	'Bladder outflow obstruction'
when 	PbRC.HRGCode	 = 	'LB16C'	 then 	'Bladder outflow obstruction'
when 	PbRC.HRGCode	 = 	'SA03D'	 then 	'Anaemia'
when 	PbRC.HRGCode	 = 	'SA03F'	 then 	'Anaemia'
when 	PbRC.HRGCode	 = 	'SA04D'	 then 	'Anaemia'
when 	PbRC.HRGCode	 = 	'SA04F'	 then 	'Anaemia'
when 	PbRC.HRGCode	 = 	'SA05D'	 then 	'Anaemia'
when 	PbRC.HRGCode	 = 	'SA05F'	 then 	'Anaemia'
when 	PbRC.HRGCode	 = 	'FZ47B'	 then 	'Abdominal pain'
when 	PbRC.HRGCode	 = 	'FZ47C'	 then 	'Abdominal pain'
end



     
FROM PbR2016.dbo.PbRCut PbRC 

left Join WHREPORTING.APC.Spell S
on S.SourceSpellNo = PbRC.ProviderSpellNo

  left join PbR2016.Tariff.APCOPProcedure TARC
  on TARC.HRGCode = PbRC.HRGCode



left join PbR2016.lk.HRGFull HRGC
on HRGC.HRG = PbRC.HRGCode


LEFT Join PbR2016.dbo.PbR PbRD 
on PbRD.ProviderSpellNo = PbRC.ProviderSpellNo
and PbRD.DatasetCode = 'Encounter'
and PbRD.EncounterTypeCode = 'IP'

left join PbR2016.lk.HRGFull HRGD
on HRGD.HRG = PbRD.HRGCode

  left join PbR2016.Tariff.APCOPProcedure TAR
  on TAR.HRGCode = PbRD.HRGCode
  
    left join [PbR2016].[HRG].[HRG49APCEncounter] ENC
  on PbRD.ProviderSpellNo = ENC.ProviderSpellNo
  and ENC.SpellReportFlag = 1
  
  left join WHREPORTING.APC.Episode E
  on E.EpisodeUniqueID = Enc.SourceUniqueID
  
  left join WHREPORTING.APC.ClinicalCoding CC
  on CC.SourceUniqueID = ENC.SourceUniqueID
  
  left join WHREPORTING.APC.AllDiagnosis D
on D.EpisodeSourceUniqueID = E.EpisodeUniqueID
and D.DiagnosisSequence = 1


	Left Join WHREPORTING.CN.CasenoteCurrentLocationDataset cn With (NoLock)
	On S.SourcePatientNo = cn.Patnt_refno
      

  
  Where 
S.DischargeDate BETWEEN @StartDate AND @EndDate
and
PbRC.DatasetCode = 'Encounter'
and 
PbRC.EncounterTypeCode = 'IP'
  and
  S.AdmissionMethodNHSCode between '21' and '28' --i.e. Emergency
  and
  S.LengthOfSpell > 2
  and
  (
  PbRC.BestPracticeSectionCode ='16'
    or
  PbRC.HRGCode in
  ('AA31A',
'AA31B',
'DZ15E',
'DZ15F',
'DZ22C',
'DZ09B',
'DZ09C',
'LB40A',
'LB40B',
'QZ20Z',
'WA11X',
'WA11Y',
'EB08I',
'DZ11C',
'EB07H',
'EB07I',
'HA83B',
'HA83C',
'LB16B',
'LB16C',
'SA03D',
'SA03F',
'SA04D',
'SA04F',
'SA05D',
'SA05F',
'FZ47B',
'FZ47C'
)
)


 



 

  
  order by
  TariffPerBedDayCut,
            PbRC.BestPracticeCode
            ,PbRC.BestPracticeSectionCode
            ,PbRD.HRGCode