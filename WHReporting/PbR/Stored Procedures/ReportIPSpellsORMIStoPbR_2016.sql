﻿/*
--Author: N Scattergood
--Date created: 21/03/2016
--Stored Procedure based on Fuzzy Match between ORMIS and Lorenzo to compare Procedure
 as described on ORMIS/Waiting List to what was coded
 Modified for 1617 on 3rd May 2016
--
*/

CREATE Procedure [PbR].[ReportIPSpellsORMIStoPbR_2016]

@StartDate	as Date 
,@EndDate	as Date	

as
Declare @CRLF		as varchar(2) 
SET @CRLF			=  CHAR(13)+CHAR(10)


SELECT     
					o.DistrictNo 
					,o.Surname as PatientSurname
					--,S.SourceSpellNo
					,OS.OperatingSuite
                     , t.Theatre
                      , s1.Surname AS anaes1name
                      , s2.Surname AS anaes2name
                      , s3.Surname AS anaes3name
                     ,OperationConsultant = Cons.Surname +' '+ Cons.Forename
                      , s4.Surname AS surgeon1name
                      , s5.Surname AS surgeon2name
                      , s6.Surname AS surgeon3name
                      --, o.InSuiteTime
                      --, o.InAnaestheticTime
                      --, o.AnaestheticInductionTime
                      --, o.AnaestheticReadyTime
                      --, o.OperationStartDate
                      --,o.OperationEndDate
                      --, p.ProcedureStartTime
                      --, p.ProcedureEndTime
                      --,cast(o.AnaestheticInductionTime as time) as AnaesInductTime
                      ,o.OperationDate
                      ,left(cast(o.AnaestheticInductionTime as time),2) as AnaesInductTime
                      --,cast(o.AnaestheticreadyTime as time) as AnaesReadyTime
                      --,cast(o.OperationStartDate as time) as OpStartTime
                      --,cast(p.ProcedureEndTime as time) as OpEndTime
                      
                        ,DATEDIFF(N,o.AnaestheticInductionTime,o.AnaestheticReadyTime) 
                     + DATEDIFF(N,o.OperationStartDate,o.OperationEndDate) as TotTime
                        ,DATEDIFF(N,o.AnaestheticInductionTime,o.AnaestheticReadyTime) as AnaesTime
                        ,DATEDIFF(N,o.OperationStartDate,p.ProcedureEndTime) as OpTime
                        ,DATEDIFF(N,o.AnaestheticInductionTime,InRecoveryTime) as ProductiveTime
                                             
                        --,InRecoveryTime
                        --,cast(InRecoveryTime as date) as InRecoveryDate
                        --,cast(InRecoveryTime as time) as InRecoveryTimeTime
                        --,ReadyToDepartTime
                        --,DischargeTime
                        --,cast(DischargeTime as date) as DischargeDate
                        --,case when (cast(DischargeTime as date) ) >(cast(InRecoveryTime as date) ) 
                        --then 'Overnight' end as 'RecoveryStayClass'
                        -- ,DATEDIFF(HOUR,InRecoveryTime,DischargeTime) as RecoveryTimeHr
                        -- ,DATEDIFF(N,InRecoveryTime,DischargeTime) as RecoveryTimeMin
                        
                      , o.Operation
                      --,OP.OperationCode1 as 'OrmisOPCS'
                      --, p.ProcedureDescription, 
                      --CASE WHEN datepart(hour, s.starttime) < 12 
                      --THEN 'AM' ELSE 'PM' 
                      --END AS SessionType
                      --, c.DayOfWeek
                      , o.OperationTypeCode
                      ,sp.Specialty as ORMISOperationSpec

              
                                          ,left(cast(InRecoveryTime as time),2) as RecoveryHr
                                        ,left(cast(o.AnaestheticInductionTime as time),2) as AnaesHr
                                        ,ASA.ASAScore
                      
                      ,S.SourceSpellNo
                      ,S.PatientClass
                      ,S.[AdmissionSpecialty(Function)]
                      ,S.AdmissionConsultantName
                      ,S.AdmissionDate 
                      , S.DischargeDate
                      ,S.LengthOfSpell
                      ,DailyPbR_HRGCode			= PbRD.HRGCode
                      ,DailyPbR_HRGDescription	= HRGD.HRGDescription
                      ,DailyPbR_CCFlag			= HRGD.CCFlag
                      ,DailyPbR_EstimatedTariff =  1.059353* case
													when PbRD.PODCode = 'NELST' 
													then coalesce(TAR.[ReducedShortStayEmergencyTariff],TAR.[NonElectiveSpellTariff])
													when PbRD.PODCode like '%NEL%' 
													then TAR.[NonElectiveSpellTariff]
													when PbRD.PODCode like '%DC%' 
													then coalesce(TAR.[DaycaseTariff],TAR.[CombinedDaycaseElectiveTariff])
													when PbRD.PODCode like '%EL%' 
													then coalesce(TAR.[ElectiveSpellTariff],TAR.[CombinedDaycaseElectiveTariff]) 
													end
                      ,DailyPbR_DominantProcCode = PbRD.DominantProcedureCode
                      ,DailyPbR_DominantProc	= OPD.[OPCS Description]

                      ,PbRCut_CensusDate		= PbRC.CensusDate
                      ,PbRCut_CutType			= PbRC.CutTypeCode
                      ,PbRCut_POD				= PbRC.PODCode
                      ,PbRCut_HRGCodeCut		= PbRC.HRGCode
					  ,PbRCut_HRGDescription	= HRGC.HRGDescription
                      ,PbRCut_CCFlag			= HRGC.CCFlag
                      ,PbRCut_EstimatedTariff	=  1.059353* case
														when PbRC.PODCode = 'NELST' 
														then coalesce(TARC.[ReducedShortStayEmergencyTariff],TARC.[NonElectiveSpellTariff])
														when PbRC.PODCode like '%NEL%' 
														then TARC.[NonElectiveSpellTariff]
														when PbRC.PODCode like '%DC%' 
														then coalesce(TARC.[DaycaseTariff],TARC.[CombinedDaycaseElectiveTariff])
														when PbRC.PODCode like '%EL%' 
														then coalesce(TARC.[ElectiveSpellTariff],TARC.[CombinedDaycaseElectiveTariff]) 
														end
                      ,PbRCut_DominantProcCode	= PbRC.DominantProcedureCode
                      ,PbRCut_DominantProc		= OPC.[OPCS Description]
                      
                      ,HRGChange = case 
                      when PbRD.HRGCode <> PbRC.HRGCode
                      then 'Y'
                      else 'N'
                      end
                      
                      ,TariffChange =  1.059353* case
													when PbRD.PODCode = 'NELST' 
													then coalesce(TAR.[ReducedShortStayEmergencyTariff],TAR.[NonElectiveSpellTariff])
													when PbRD.PODCode like '%NEL%' 
													then TAR.[NonElectiveSpellTariff]
													when PbRD.PODCode like '%DC%' 
													then coalesce(TAR.[DaycaseTariff],TAR.[CombinedDaycaseElectiveTariff])
													when PbRD.PODCode like '%EL%' 
													then coalesce(TAR.[ElectiveSpellTariff],TAR.[CombinedDaycaseElectiveTariff]) 
													end 
											-		 1.059353* case
														when PbRC.PODCode = 'NELST' 
														then coalesce(TARC.[ReducedShortStayEmergencyTariff],TARC.[NonElectiveSpellTariff])
														when PbRC.PODCode like '%NEL%' 
														then TARC.[NonElectiveSpellTariff]
														when PbRC.PODCode like '%DC%' 
														then coalesce(TARC.[DaycaseTariff],TARC.[CombinedDaycaseElectiveTariff])
														when PbRC.PODCode like '%EL%' 
														then coalesce(TARC.[ElectiveSpellTariff],TARC.[CombinedDaycaseElectiveTariff]) 
														end
						,E.Specialty
						,E.SpecialtyCode
						,E.ConsultantName								
                      , CurrentDiagnosisCoded
								   = isnull(cc.[PrimaryDiagnosis],'-')+@CRLF
										 +isnull(cc.[SubsidiaryDiagnosis],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis1],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis2],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis3],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis4],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis5],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis6],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis7],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis8],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis9],'-')
										 --+isnull(cc.[SecondaryDiagnosis10],'-')
					 , CurrentProceduresCoded
       = isnull(cc.PriamryProcedure,'-')+@CRLF
             +isnull(cc.[SecondaryProcedure1],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure2],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure3],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure4],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure5],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure6],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure7],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure8],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure9],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure10],'-')                                                       
                      
FROM         [WH].Theatre.OperationDetail AS o 
LEFT OUTER JOIN       [WH].Theatre.ProcedureDetail AS p 
				ON p.OperationDetailSourceUniqueID = o.SourceUniqueID     
				and    p.PrimaryProcedureFlag = 1
INNER JOIN            [WH].Theatre.Theatre AS t ON t.TheatreCode = o.TheatreCode 
LEFT JOIN			  [WH].[Theatre].[OperatingSuite] OS on OS.OperatingSuiteCode = T.OperatingSuiteCode
LEFT OUTER JOIN       [WH].Theatre.Staff AS s1 ON s1.StaffCode = p.Anaesthetist1Code 
LEFT OUTER JOIN       [WH].Theatre.Staff AS s2 ON s2.StaffCode = p.Anaesthetist2Code 
LEFT OUTER JOIN       [WH].Theatre.Staff AS s3 ON s3.StaffCode = p.Anaesthetist3Code 
LEFT OUTER JOIN       [WH].Theatre.Staff AS s4 ON s4.StaffCode = p.Surgeon1Code 
LEFT OUTER JOIN       [WH].Theatre.Staff AS s5 ON s5.StaffCode = p.Surgeon2Code 
LEFT OUTER JOIN       [WH].Theatre.Staff AS s6 ON s6.StaffCode = p.Surgeon3Code 
--LEFT OUTER JOIN       [WH].Theatre.Session AS s ON s.SourceUniqueID = o.SessionSourceUniqueID 
--LEFT OUTER JOIN       [WH].WH.Calendar AS c ON c.TheDate = o.OperationDate 
LEFT OUTER JOIN       [WH].Theatre.Specialty AS sp ON sp.SpecialtyCode = o.SpecialtyCode
--left outer join        WHREPORTING.LK.Calendar as cal on cal.TheDate = o.OperationDate
--left outer join         WH.Theatre.AdmissionType as AT on AT.AdmissionTypeCode = o.AdmissionTypeCode
            
LEFT join  WH.Theatre.Operation as OP on OP.OperationCode = P.ProcedureCode
left join [WH].[Theatre].[Staff] Cons on Cons.StaffCode = O.ConsultantCode 
left join WH.Theatre.ASAScore as ASA on ASA.ASAScoreCode = o.ASAScoreCode

----------------------------PAS and PBR Tables--------------------------
Inner Join WHREPORTING.APC.Spell S on o.DistrictNo = S.FaciltyID
and o.OperationDate between S.AdmissionDate and S.DischargeDate

left join PbR2016.dbo.PbR PbRD on PbRD.ProviderSpellNo = S.SourceSpellNo
and PbRD.DatasetCode = 'Encounter'
and PbRD.EncounterTypeCode = 'IP'

  left join PbR2016.Tariff.APCOPProcedure TAR
  on TAR.HRGCode = PbRD.HRGCode
  
    left join [PbR2016].[HRG].[HRG49APCEncounter] ENC
  on PbRD.ProviderSpellNo = ENC.ProviderSpellNo
  and ENC.SpellReportFlag = 1
  
  left join WHREPORTING.APC.Episode E
  on E.EpisodeUniqueID = Enc.SourceUniqueID
  
  left join WHREPORTING.APC.ClinicalCoding CC
  on CC.SourceUniqueID = ENC.SourceUniqueID
  

left join PbR2016.dbo.PbRCut PbRC on PbRC.ProviderSpellNo = S.SourceSpellNo
and PbRC.DatasetCode = 'Encounter'
and PbRC.EncounterTypeCode = 'IP'

  left join PbR2016.Tariff.APCOPProcedure TARC
  on TARC.HRGCode = PbRC.HRGCode

left join PbR2016.lk.HRGFull HRGD
on HRGD.HRG = PbRD.HRGCode

left join PbR2016.lk.HRGFull HRGC
on HRGC.HRG = PbRC.HRGCode

left join PbR2016.lk.OPCS4 OPD
on OPD.[OPCS Code ] = PbRD.DominantProcedureCode

left join PbR2016.lk.OPCS4 OPC
on OPC.[OPCS Code ] = PbRC.DominantProcedureCode

            
WHERE 
    S.DischargeDate BETWEEN @StartDate AND @EndDate
    and
    (o.OperationCancelledFlag = 0)
	and
	PbRD.HRGCode <> 'UZ01Z'

 
Order by
OS.OperatingSuite
,E.Specialty
,S.DischargeDate asc
,S.FaciltyID