﻿/*
--Author: N Scattergood
--Date created: 01/07/2016
--Stored Procedure based on R Codes (Sign and Symptoms)

--
*/

CREATE Procedure [PbR].[ReportIPSpellsRCodes_2016]

@StartDate	as Date 
,@EndDate	as Date	

as
Declare @CRLF		as varchar(2) 
SET @CRLF			=  CHAR(13)+CHAR(10)

SELECT 
S.SourceSpellNo
,PbRD.DistrictNo
,PbRD.PatientSurname
                      ,S.PatientClass
                      ,S.AdmissionType
                      --,S.[AdmissionSpecialty(Function)]
                      --,S.AdmissionConsultantName
                      ,S.AdmissionWard
                      ,S.AdmissionDate 
                      , S.DischargeDate
                      ,S.DischargeWard
                      ,S.LengthOfSpell
                      ,PbRD.EpisodeCount
                      ,TARC.NonElectiveLongStayTrimpoint
                      ,PbRC.ExcessBedDays
                                            	
                      --,PbRC.BestPracticeSectionCode
             --         ,DailyPbR_HRGCode			= PbRD.HRGCode
             --         ,DailyPbR_HRGDescription	= HRGD.HRGDescription
             --         ,DailyPbR_CCFlag			= HRGD.CCFlag
             --         ,DailyPbR_EstimatedTariff =  1.059353* case
													--when PbRD.PODCode = 'NELST' 
													--then coalesce(TAR.[ReducedShortStayEmergencyTariff],TAR.[NonElectiveSpellTariff])
													--when PbRD.PODCode like '%NEL%' 
													--then TAR.[NonElectiveSpellTariff]
													--when PbRD.PODCode like '%DC%' 
													--then coalesce(TAR.[DaycaseTariff],TAR.[CombinedDaycaseElectiveTariff])
													--when PbRD.PODCode like '%EL%' 
													--then coalesce(TAR.[ElectiveSpellTariff],TAR.[CombinedDaycaseElectiveTariff]) 
													--end
             --         ,DailyPbR_DominantProcCode = PbRD.DominantProcedureCode
                      --,DailyPbR_DominantProc	= OPD.[OPCS Description]

                      ,PbRCut_CensusDate		= PbRC.CensusDate
                      ,PbRCut_CutType			= PbRC.CutTypeCode
                      ,PbRCut_POD				= PbRC.PODCode
                      ,PbRCut_HRGCodeCut		= PbRC.HRGCode
					  ,PbRCut_HRGDescription	= HRGC.HRGDescription
                      ,PbRCut_CCFlag			= HRGC.CCFlag
                      ,PbRCut_GroupingMethod	= ENC.GroupingMethodFlag
                      ,PbRCut_TariffInSLAM		= SL.[PriceActual]
              --        ,PbRCut_EstimatedTariff	=  1.059353* case
														--when PbRC.PODCode = 'NELST' 
														--then coalesce(TARC.[ReducedShortStayEmergencyTariff],TARC.[NonElectiveSpellTariff])
														--when PbRC.PODCode like '%NEL%' 
														--then TARC.[NonElectiveSpellTariff]
														--when PbRC.PODCode like '%DC%' 
														--then coalesce(TARC.[DaycaseTariff],TARC.[CombinedDaycaseElectiveTariff])
														--when PbRC.PODCode like '%EL%' 
														--then coalesce(TARC.[ElectiveSpellTariff],TARC.[CombinedDaycaseElectiveTariff]) 
														--end
					, TariffPerBedDayCut = (SL.[PriceActual]
					
					--1.059353* case
					--									when PbRC.PODCode = 'NELST' 
					--									then coalesce(TARC.[ReducedShortStayEmergencyTariff],TARC.[NonElectiveSpellTariff])
					--									when PbRC.PODCode like '%NEL%' 
					--									then TARC.[NonElectiveSpellTariff]
					--									when PbRC.PODCode like '%DC%' 
					--									then coalesce(TARC.[DaycaseTariff],TARC.[CombinedDaycaseElectiveTariff])
					--									when PbRC.PODCode like '%EL%' 
					--									then coalesce(TARC.[ElectiveSpellTariff],TARC.[CombinedDaycaseElectiveTariff]) 
					--									end
										+ isnull(1.059353*TARC.PerDayLongStayPayment,0)*isnull(PbRC.ExcessBedDays,0)
										) / (S.LengthOfSpell +1)							
                      --,PbRCut_DominantProcCode	= PbRC.DominantProcedureCode
                      --,PbRCut_DominantProc		= OPC.[OPCS Description]
                      
                      --,HRGChange = case 
                      --when PbRD.HRGCode <> PbRC.HRGCode
                      --then 'Y'
                      --else 'N'
                      --end
                      
           --           ,TariffChange =  1.059353* case
											--		when PbRD.PODCode = 'NELST' 
											--		then coalesce(TAR.[ReducedShortStayEmergencyTariff],TAR.[NonElectiveSpellTariff])
											--		when PbRD.PODCode like '%NEL%' 
											--		then TAR.[NonElectiveSpellTariff]
											--		when PbRD.PODCode like '%DC%' 
											--		then coalesce(TAR.[DaycaseTariff],TAR.[CombinedDaycaseElectiveTariff])
											--		when PbRD.PODCode like '%EL%' 
											--		then coalesce(TAR.[ElectiveSpellTariff],TAR.[CombinedDaycaseElectiveTariff]) 
											--		end 
											---		 1.059353* case
											--			when PbRC.PODCode = 'NELST' 
											--			then coalesce(TARC.[ReducedShortStayEmergencyTariff],TARC.[NonElectiveSpellTariff])
											--			when PbRC.PODCode like '%NEL%' 
											--			then TARC.[NonElectiveSpellTariff]
											--			when PbRC.PODCode like '%DC%' 
											--			then coalesce(TARC.[DaycaseTariff],TARC.[CombinedDaycaseElectiveTariff])
											--			when PbRC.PODCode like '%EL%' 
											--			then coalesce(TARC.[ElectiveSpellTariff],TARC.[CombinedDaycaseElectiveTariff]) 
											--			end
						,E.Division
						,E.Directorate								
						,E.Specialty
						,E.SpecialtyCode
						,E.ConsultantName	
,D.UserModified as CodedBy
,D.ModifedDate as CodeModified
,CN.Location as NotesLocations
,CN.Notes	as NotesDetail						
                      , CurrentDiagnosisCoded
								   = isnull(cc.[PrimaryDiagnosis],'-')+@CRLF
										 +isnull(cc.[SubsidiaryDiagnosis],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis1],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis2],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis3],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis4],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis5],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis6],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis7],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis8],'-')+@CRLF
										 +isnull(cc.[SecondaryDiagnosis9],'-')
										 --+isnull(cc.[SecondaryDiagnosis10],'-')
					 , CurrentProceduresCoded
       = isnull(cc.PriamryProcedure,'-')+@CRLF
             +isnull(cc.[SecondaryProcedure1],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure2],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure3],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure4],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure5],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure6],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure7],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure8],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure9],'-')+@CRLF
             +isnull(cc.[SecondaryProcedure10],'-')     
  

     
FROM PbR2016.dbo.PbRCut PbRC 

left Join WHREPORTING.APC.Spell S
on S.SourceSpellNo = PbRC.ProviderSpellNo

left Join [PbR2016].[Slam].[APCImport] SL
on SL.[ReportMasterID] = PbRC.ProviderSpellNo

left join PbR2016.Tariff.APCOPProcedure TARC
on TARC.HRGCode = PbRC.HRGCode

left join PbR2016.lk.HRGFull HRGC
on HRGC.HRG = PbRC.HRGCode


LEFT Join PbR2016.dbo.PbR PbRD 
on PbRD.ProviderSpellNo = PbRC.ProviderSpellNo
and PbRD.DatasetCode = 'Encounter'
and PbRD.EncounterTypeCode = 'IP'

left join PbR2016.lk.HRGFull HRGD
on HRGD.HRG = PbRD.HRGCode

  left join PbR2016.Tariff.APCOPProcedure TAR
  on TAR.HRGCode = PbRD.HRGCode
  
    left join [PbR2016].[HRG].[HRG49APCEncounter] ENC
  on PbRD.ProviderSpellNo = ENC.ProviderSpellNo
  and ENC.SpellReportFlag = 1
  
  left join WHREPORTING.APC.Episode E
  on E.EpisodeUniqueID = Enc.SourceUniqueID
  
  left join WHREPORTING.APC.ClinicalCoding CC
  on CC.SourceUniqueID = ENC.SourceUniqueID
  
  left join WHREPORTING.APC.AllDiagnosis D
on D.EpisodeSourceUniqueID = E.EpisodeUniqueID
and D.DiagnosisSequence = 1


	Left Join WHREPORTING.CN.CasenoteCurrentLocationDataset cn With (NoLock)
	On S.SourcePatientNo = cn.Patnt_refno
      

  
  Where 
S.DischargeDate BETWEEN @StartDate AND @EndDate
and
PbRC.DatasetCode = 'Encounter'
and 
PbRC.EncounterTypeCode = 'IP'
and 
left(D.DiagnosisCode,1) = 'R'
  


 



 

  
  order by
  TariffPerBedDayCut