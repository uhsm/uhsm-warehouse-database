﻿/* =============================================
-- Author:		CMan
-- Create date: 27/11/2013
-- Description:	For summary report which looks at the all the spells in Pbr where the exlusion reason 
				is transplant.  Requested by NS - used by Karen Holdship(Finance) and Craig McaAllister (Data Quality)
-- Dependencies: Tables: Pbr2014.dbo.Pbrcut
						Pbr2014.dbo.Transplant
						WHREPORTING.LK.Calendar
				SSRS Report: PbR Transplant Spells by Visit Type

Modifications:
Modified by			Date				Modification
CMan				10/12/2013			Remove exclusion reason from where clause to bring through those
										records in Transplant table where there is no exclusion reason recorded in the PbR table
NScat				14/05/2014			Switched to PbR 2014 for 14/15 Reporting	
CMan				06/05/2014			Switched to PbR 2015 for 15/16 Reporting									
				
=============================================*/
CREATE PROCEDURE [PbR].[ReportIPTransplantExclSpellsbyVisitType]



AS
BEGIN

SELECT DISTINCT Cal.FinancialYear                 AS DischargeFinYear,
                RIGHT(Cal.[FinancialMonthKey], 2) AS DischargeFinMonth,
                pbr.ExclusionReason,
                pbr.ProviderSpellNo,
                pbr.AdmissionDate,
                pbr.EncounterEndDate,
                pbr.DistrictNo,
                pbr.NHSNumber,
                pbr.HRGCode,
                t.VisitTypeCode,
                case when t.VisitTypeCode = 'ASSESSMENT'  then '1'
						when  t.VisitTypeCode = 'TRANSPLANT'  then '2'
						when  t.VisitTypeCode = 'FOLLOW-UP' then '3' Else '4' End as VisitTypeOrder,
                pbr.CommissionerCodeCCG,
                pbr.FinalSLACode,
                CC.DatasetCode                    AS CriticalCareDatasetCode,
                CC.SpellLOS                       AS BedDaysonCriticalCare,
                1                                 AS SpellCount --add a value of spell count here to facilitate the SSRS matric summing
FROM   pbr2015.dbo.pbrcut pbr --takes all the inpatient spells from PbrCut
       INNER JOIN PbR2015.dbo.Transplant t --inner join to only return those spells which are listed in the the Transplant table to get the visit type for that spell
                    ON pbr.ProviderSpellNo = t.SourceSpellNo
       LEFT JOIN [WHREPORTING].[LK].[Calendar] Cal
              ON PbR.EncounterEndDate = Cal.TheDate
       LEFT OUTER JOIN (SELECT *
                        FROM   pbr2015.dbo.pbrcut
                        WHERE  podcode = 'CRIT'
                               AND DatasetCode = 'CC') AS CC--link back to PbrCut table but just the Crit Care spell and only those for the CC dataset to get the Crit Care LOS as NS stated that Transplant patients are most likely on CTCU ward and not AICU etc.
                    ON pbr.ProviderSpellNo = CC.ProviderSpellNo
WHERE pbr.EncounterTypeCode = 'ip'
       AND pbr.Datasetcode = 'Encounter' 



END