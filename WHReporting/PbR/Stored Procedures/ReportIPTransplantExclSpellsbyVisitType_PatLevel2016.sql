﻿/* =============================================
-- Author:		CMan
-- Create date: 27/11/2013
-- Description:	For Patient Level Drillthrough report which looks at the all the spells in Pbr where the exlusion reason 
				is transplant.  Requested by NS - used by Karen Holdship(Finance) and Craig McaAllister (Data Quality)
-- Dependencies: Tables: Pbr2014.dbo.Pbrcut
						Pbr2014.dbo.Transplant
						WHREPORTING.LK.Calendar
				SSRS Report: PbR Transplant Spells by Visit Type - Patient Level
							PbR Transplant Spells by Visit Type

Modifications:
Modified by			Date				Modification
CMan				10/12/2013			Following discussion with Nicholas - added join back to PbR table top pick
										up any changes to Commissioner CCG and add as column to report. REmove exclusion reason from where clause to bring through those
										records in Transplant table where there is no exclusion reason recorded in the PbR table
NScat				14/05/2014			Switched to PbR 2014 for 14/15 Reporting	
CMan				06/05/2014			Switched to PbR 2015 for 15/16 Reporting
NScat				03/05/2016			Switched to PbR 2016 for 16/17 Reporting	
							
=============================================*/
CREATE PROCEDURE [PbR].[ReportIPTransplantExclSpellsbyVisitType_PatLevel2016]   
@FinYear varchar(10)
, @finMonth varchar(5)= null
--, @excl varchar(20)=null
, @vType varchar(20)= null



AS
BEGIN

SELECT DISTINCT Cal.FinancialYear                 AS DischargeFinYear,
                RIGHT(Cal.[FinancialMonthKey], 2) AS DischargeFinMonth,
                pbr.ExclusionReason,
                pbr.ProviderSpellNo,
                pbr.AdmissionDate,
                pbr.EncounterEndDate,
                pbr.DistrictNo,
                pbr.NHSNumber,
                pbr.HRGCode,
                t.VisitTypeCode,
                pbr.CommissionerCodeCCG as CommissionerCodeCCG_frozen_data,
                pbr.FinalSLACode as FinalSLACode_frozen_data,
                pbr_updated.CommissionerCodeCCG as CommissionerCodeCCG_updated_data,
                CC.DatasetCode                    AS CriticalCareDatasetCode,
                CC.SpellLOS                       AS BedDaysonCriticalCare,
                1                                 AS SpellCount --add a value of spell count here to facilitate the SSRS matric summing
FROM   pbr2015.dbo.pbrcut pbr --takes all the inpatient spells from PbrCut
       INNER JOIN PbR2015.dbo.Transplant t --inner join link back to the Transplat table to get the visit type for all spells which are detailed in the Transplant table 
                    ON pbr.ProviderSpellNo = t.SourceSpellNo
       LEFT JOIN [WHREPORTING].[LK].[Calendar] Cal
              ON PbR.EncounterEndDate = Cal.TheDate
       LEFT OUTER JOIN (SELECT *
                        FROM   pbr2015.dbo.pbrcut
                        WHERE  podcode = 'CRIT'
                               AND DatasetCode = 'CC') AS CC--link back to PbrCut table to get Crit Care LOS - just the Crit Care spell and only those for the CC dataset as NS stated that Transplant patients are most likely on CTCU ward and not AICU etc
                    ON pbr.ProviderSpellNo = CC.ProviderSpellNo
       LEFT OUTER JOIN  pbr2015.dbo.pbr pbr_updated on pbr.ProviderSpellNo = pbr_updated.ProviderSpellNo
WHERE  pbr.EncounterTypeCode = 'ip'
       AND pbr.Datasetcode = 'Encounter'
       AND ( Cal.FinancialYear = @FinYear )
       AND ( RIGHT(Cal.[FinancialMonthKey], 2) = @finMonth
              OR @finMonth IS NULL )
       --AND ( pbr.ExclusionReason = @excl  OR @excl IS NULL) --removed 10/12/2013 as there are records in the Transplant table which do not have an exclusion reason recorded which need to be included in this report
       AND ( t.VisitTypeCode = @vType
              OR @vType IS NULL ) 




END