﻿/*
--Author: N Scattergood
--Date created: 23/09/2013
--Stored Procedure Built used for checking Inpatient Spells with HRG 'U codes' (data invalid for grouping
--Amended for 2014/15 on the 2nd May 2014
--Amended 27th May to work off Flex Data
*/


CREATE Procedure [PbR].[ReportInpatientHRGUCodes]
  
	@StartDate		as Date
,	@EndDate		as Date 
,	@QualityType	as nvarchar(1000)
 
as
SELECT 

PBR.ProviderSpellNo
,PBR.DistrictNo
,PBR.[PatientSurname]
,PBR.[Age]
--,PBR.[CommissionerCodeCCG]
,PBR.AdmissionDate
,PBR.DischargeDate
,PBR.SpellLOS
,PBR.EpisodeCount
,S.AdmissionMethod
,S.DischargeMethod
,S.DischargeDestination

,S.[AdmissionSpecialty(Function)]
,S.AdmissionConsultantName
      ,TreatmentFunctionCodeFinal = 
		Case 
			when CardiacCategory in (
				'CABG'
				,'Cardiac Surgery Other'
				,'TAVI'
				,'Valve'
				)
				then 170
			when CardiacCategory in (
				'Non CT Other'
				,'Thoracic Surgery'
				)
				then 173
			when CardiacCategory = 'Transplant'
				then 174
			when CardiacCategory in (
				'Ablat'
				,'Angio'
				,'Defib'
				,'EPS'
				,'Gen Card'
				,'Loop'
				,'PACE DUAL'
				,'PACE SING'
				,'PTCA'
				,'Trans ECG'
				)
				then 320
			else TreatmentFunctionCode
		end
      ,PBR.[EncounterTypeCode]
      ,PBR.[InterfaceCode]
      ,PBR.[DatasetCode]
,PBR.HRGCode
,PBR.PrimaryDiagnosisCode
,PBR.PrimaryProcedureCode
,PBR.PrimaryProcedureDate
,PBR.DominantProcedureCode

      --,PBR.[ExcessBedDays]
      --,PBR.[ExcessBedDaysFinal]
      --,PBR.[TrimPoint]
            
      --,PBR.[PatientForename]
      
      --,PBR.[PatientTitle]
       --,PBR.[SexCode]

      --,PBR.[TreatmentFunctionCode]
      --,PBR.[BestPracticeCode]
      --,PBR.[BestPracticeSectionCode]
      ,PBR.[PODCode]
      ,PBR.[PbRExcluded]
   ,case 
when DQ.QualityTypeCode is null 
then 'UNKNOWN'
else DQ.QualityTypeCode 
end as QualityType
,DQ.QualityMessage
      --,PBR.[CriticalCareDays]
      --,PBR.[CardiacCategory]

      --,PBR.AdmissionMethodCode
      --,PBR.AdminCategoryCode
  FROM PbR2014.dbo.[PbR] PBR
  
	left join	WHREPORTING.APC.Spell S
	on PBR.ProviderSpellNo = S.SourceSpellNo
	
		left outer join [WH].[HRG].[HRG47APCQuality] DQ 
		on PBR.SourceEncounterRecno = DQ.EncounterRecno
	
  Where 
  EncounterTypeCode = 'IP'
  and
  DatasetCode = 'ENCOUNTER'
  and 
  LEFT (HRGCode,1) = 'U' 
  --and 
  --PbRExcluded = 0
  and
  PBR.DischargeDate between @StartDate and @EndDate
  and
case 
when DQ.QualityTypeCode is null 
then 'UNKNOWN'
else DQ.QualityTypeCode 
end in  (SELECT Val from dbo.fn_String_To_Table(@QualityType,',',1))
  
  order by
DQ.QualityTypeCode
,DQ.QualityMessage