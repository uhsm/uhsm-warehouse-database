﻿/*
--Author: N Scattergood
--Date created: 23/09/2013
--Stored Procedure Built used for checking Inpatient Spells with HRG 'U codes' (data invalid for grouping
--This Procedure provides the Paramenter for the Type of Error
--Amended for 2014/15 on the 2nd May 2014
--Amended 27th May 2014 to work off Flex Data
--Amended 6th May 2015 for new year
--Amended 3rd May 2016 for new year
*/


CREATE Procedure [PbR].[ReportInpatientHRGUCodes_201617_QualityMessage]
  
	@StartDate		as Date
,	@EndDate		as Date 

 
as
SELECT 
Distinct
case 
when DQ.QualityTypeCode is null 
then 'UNKNOWN'
else DQ.QualityTypeCode 
end as QualityType


  FROM PbR2016.dbo.[PbR] PBR
  
	left join	WHREPORTING.APC.Spell S
	on PBR.ProviderSpellNo = S.SourceSpellNo
	
		left outer join PbR2016.HRG.[HRG49APCQuality] DQ 
		on PBR.SourceEncounterRecno = DQ.EncounterRecno
	
  Where 
  EncounterTypeCode = 'IP'
  and
  DatasetCode = 'ENCOUNTER'
  and 
  LEFT (HRGCode,1) = 'U' 
  --and 
  --PbRExcluded = 0
  and
  PBR.DischargeDate between @StartDate and @EndDate
 -- and
 --DQ.QualityTypeCode in  (SELECT Val from dbo.fn_String_To_Table(@QualityType,',',1))
  
  order by
case 
when DQ.QualityTypeCode is null 
then 'UNKNOWN'
else DQ.QualityTypeCode 
end