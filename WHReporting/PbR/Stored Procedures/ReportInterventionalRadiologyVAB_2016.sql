﻿-- =============================================
-- Author:		Carol Beckett
-- Create date: 11/06/2015
-- Description:	Stored procedure to extract data for SSRS report Interventional Radiology VAB for Finance
-- =============================================





Create PROCEDURE [PbR].[ReportInterventionalRadiologyVAB_2016]
	

/*
 Modified Date				Modified By			Modification
-----------------			------------		------------
03 May 2016					NScatter			Report From 16/17
*/


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT 
NHSNumber
,DistrictNo
,PatientSurname
,EncounterTypeCode
,PODCode
,PbR.AdmissionDate
,PbR.DischargeDate
,SpellLOS
,PbR.[NotOurActProvCode]
,S.AdmissionWard
,S.AdmissionConsultantName
,HRGCode
--DatasetCode


,PrimaryProcedureCode
,DominantProcedureCode

,BestPracticeCode
,BestPracticeSectionCode


  FROM [PbR2016].dbo.PbR PbR
  
		left join WHREPORTING.APC.Spell S
		on S.SourceSpellNo = PbR.ProviderSpellNo 
  
  Where
  BestPracticeCode
  in
  ('BP27')
  and
  DatasetCode = 'ENCOUNTER'
--NHSNumber in 
--(
-- '4689418535', 
-- '6330140553' 
-- )
--and
--BestPracticeCode is not null
order by
DischargeDate desc
,PODCode



END