﻿/*
--Author: N Scattergood
--Amended for 2015/16 on 8th May 2015
--Amended for 2016/17 on 3rd May 2016
*/


Create Procedure [PbR].[ReportMajorTraumaActivity2016]
  
as
SELECT  [MonthNo] as FinMonth
,[ReportMasterID] as ProviderSpellNo
,[Pat_Code] as DistrictNo
,FF.Age
,[CCG_Code] as CommissionerCodeCCG
  ,CCG.PCTCCG as CCGDescription
      ,[SpellAdmissionDate] as AdmissionDate
      ,S.AdmissionDateTime
      ,DATEPART(HOUR,S.AdmissionDateTime) as AdmissionHour
      ,[SpellDischargeDate] as DischargeDate
      ,ISS.ISS
      --,[Spec_Code]
      ,[HRG_Code] as HRGCode
      ,[UD4Act_Code] as PrimaryDiagnosisCode
      ,[UD5Act_Code] as PrimaryProcedureCode
     ,FF.PrimaryProcedureDate
,FF.DominantProcedureCode
      ,[POD_Code] as PODCode
      ,[Spec_Code]
      ,[Activity]
      --,[SpecService_ID]
      --,[SpecService_ID_2]
      --,[SpecService_ID_3]
      --,[Gpp_Code]
      --,[Pat_Code]
      --,[DOB]
      --,[Cons_Code]
      --,[LOS]
      --,[ReportMasterID]
      --,[UD1Act_Code]
      --,[UD2Act_Code]
      --,[UD3Act_Code]
      --,[UD4Act_Code]
      --,[UD5Act_Code]
      --,[SpellAdmissionDate]
      --,[SpellDischargeDate]
      --,[PatClass]
      --,[AdmMethod]
      
       ,CAST(PBR.[ReportMasterID] as varchar(9))+PBR.[POD_Code] as JoinKey
       
  FROM [PbR2016].[dbo].[SLAMCombinedAllPOD] PbR
  
inner join	PbR2016.dbo.TARN T
on PBR.[ReportMasterID] = T.AdmissionNo

left join PbR2016.dbo.TARN_ISS ISS -- added 4th September to give ISS Score
on T.AdmissionNo = ISS.AdmissionNo

left join [WHREPORTING].[LK].[Calendar] Cal
on Cal.TheDate = PbR.SpellDischargeDate

left join
[WHREPORTING].[LK].[PasPCTOdsCCG] CCG
on PBR.[CCG_Code]= CCG.PCTCCGCode

left join PbR2016.dbo.PbRCut FF
on FF.ProviderSpellNo = PBR.[ReportMasterID]
and EncounterTypeCode = 'IP'
and DatasetCode = 'ENCOUNTER'

	left join WHREPORTING.APC.Spell S
	on S.SourceSpellNo = PBR.[ReportMasterID]

order by 
[MonthNo]desc,
[POD_Code]