﻿/*
--Author: N Scattergood
--Date created: 18/10/2013
--Stored Procedure Built used for SSRS Report on Major Trauma Activity Report (RD427)
--This is a supporting report to show any patients we are unable to match because we cannot identify the Spell Number
--Duplicate Report created so we can see new 2013 and 2014 until M2 on 02/05/14
--Amended for 2015/16 on the 8th May 2015
--
*/

Create Procedure [PbR].[ReportMajorTraumaActivity_MissingSpellNumbers2015]
  
as
SELECT 
 [Patient ID]
      ,[NHS Number]
      ,[Forename]
      ,[Surname]
      ,[Admission Date]
      ,[Discharge Date]
      ,[AdmissionNo]
      --,[ISS]
  FROM [PbR2015].[dbo].[TARN]
  
  Where
  [AdmissionNo] is null
  
  order by
  [Discharge Date] desc