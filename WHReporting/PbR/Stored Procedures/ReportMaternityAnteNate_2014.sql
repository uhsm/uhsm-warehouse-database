﻿/*
--Author: N Scattergood
--Date created: 29/08/2013
--Stored Procedure Built used for SSRS Report on Maternity Pathways Report (RD423)
--Amended for 2014/15 on the 1st May 2014
*/


Create Procedure [PbR].[ReportMaternityAnteNate_2014]
  as

SELECT RIGHT(Cal.[FinancialMonthKey],2) as FinMonth
		,[SourceUniqueID]
      ,[NHSNumber]
      ,[DistrictNumber]
      ,[AppointmentDate]
      ,[IntermediateFactors]
      ,[IntermediatePathway]
      ,[IntensiveFactors]
      ,[IntensivePathway]
      ,[Postcode]
      ,[PracticeCode]
      ,[CCG]
      ,CCG.PCTCCG as CCGDescription
      ,[DTTM_OF_BIRTH]
  FROM [PbR2014].[dbo].[MatAnteEncounter]
  
  inner join [WHREPORTING].[LK].[Calendar] Cal
on Cal.TheDate = [AppointmentDate]
and Cal.FinancialYear = '2014/2015'

left join
[WHREPORTING].[LK].[PasPCTOdsCCG] CCG
on [CCG] = CCG.PCTCCGCode
  
  order by AppointmentDate Desc