﻿/*
--Author: N Scattergood
--Date created: 29/08/2013
--Stored Procedure Built used for SSRS Report on Maternity Pathways Report (RD423)
--Amended for 2014/15 on the 1st May 2014
--Amended for 2015/16 on the 6th May 2015
--Amended for 2016/17 on the 3rd May 2016 
*/


CREATE Procedure [PbR].[ReportMaternityAnteNate_2016]
  
  as

SELECT RIGHT(Cal.[FinancialMonthKey],2) as FinMonth
		,[SourceUniqueID]
      ,[NHSNumber]
      ,[DistrictNumber]
      ,[AppointmentDate]
      ,[IntermediateFactors]
      ,[IntermediatePathway]
      ,[IntensiveFactors]
      ,[IntensivePathway]
      ,[Postcode]
      ,[PracticeCode]
      ,[CCG]
      ,CCG.PCTCCG as CCGDescription
      ,[DTTM_OF_BIRTH]
  FROM [PbR2016].[dbo].[MatAnteEncounter]
  
  inner join [WHREPORTING].[LK].[Calendar] Cal
on Cal.TheDate = [AppointmentDate]
and Cal.FinancialYear = '2016/2017'

left join
[WHREPORTING].[LK].[PasPCTOdsCCG] CCG
on [CCG] = CCG.PCTCCGCode
  
  order by AppointmentDate Desc