﻿--USE [WHREPORTING]
--GO
--/****** Object:  StoredProcedure [PbR].[ReportMaternityDatingScan]    Script Date: 04/30/2014 11:28:09 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

--/*
----Author: N Scattergood
----Date created: 30/08/2013
----Stored Procedure Built used for SSRS Report on Dating Scan Activity Report (RD425)
----Most of the work is done in the view [WHREPORTING].[PbR].[MaternityDatingScan]
--Amended for 2014/15 on the 1st May 2014
--*/


CREATE Procedure [PbR].[ReportMaternityDatingScan]
  
  as
SELECT [Fin Month]
      ,[FinalSLACode]
      ,CCG.PCTCCG as CCGDescription
      ,[EncounterTypeCode]
      ,[DatasetCode]
      ,[NHSNumber]
      ,[DistrictNo]
      ,ReferralPathwayID
      ,[TreatmentFunctionCode]
      ,[HRGCode]
      ,[PODCode]
      ,[EncounterStartDate] as AppointmentDate
      ,[SourceUniqueID]
      ,[Cases]
      ,[ClinicCode]
      ,[SessionCode]
      ,[ConsultantCode]
      ,[PbRExcluded]
      ,[ExclusionReason]
      ,[PrimaryProcedureCode]
      ,[DominantProcedureCode]
      ,[ServiceCode]
  FROM [WHREPORTING].[PbR].[MaternityDatingScan]
  
  left join
[WHREPORTING].[LK].[PasPCTOdsCCG] CCG
on [FinalSLACode] = CCG.PCTCCGCode

order by
[Fin Month] desc,
[EncounterStartDate],
CCG.PCTCCG