﻿/*
--Author: N Scattergood
--Date created: 20/11/2013
--Stored Procedure Built for SRSS Report on:
--PbR OP Attendances In PbR Flex but not Freeze 
--Amended 30/03/15 so it works off PbR2014 rather than table in WHREPORTING
*/

CREATE Procedure [PbR].[ReportOPAttendancesInFlexButNotFreeze]

as

SELECT 
RIGHT(Cal.[FinancialMonthKey],2)  as FinMonth
--,Cal.TheMonth
--    ,PBR.[InterfaceCode]
--      ,PBR.[EncounterTypeCode]
--      ,PBR.[DatasetCode]
      ,Case
      when PBR.FirstAttendanceCode = 1 
      then 'New'
      else 'FollowUp'
      end as FirstFollowUp
        ,PBR.FirstAttendanceCode
--          ,PBR.[ClinicCode]
--          ,OP.Clinic
--,OP.[SpecialtyCode(Function)]
--,OP.[Specialty(Function)]
       ,PBR.[TreatmentFunctionCode]
      --,PBR.[TreatmentFunctionCodeFinal] 
      --,PBR.[ConsultantCode]
      ,COUNT(1) as Attendances
        
  FROM [PbR2015].dbo.PbR PBR
  
  left join [WHREPORTING].[LK].[Calendar] Cal
      on Cal.TheDate = PbR.EncounterEndDate
      
      left join WHREPORTING.OP.Schedule OP
      on OP.SourceUniqueID = PBR.SourceUniqueID
      
      left join [PbR2015].dbo.PbRCut PBRFF
      on PBR.SourceUniqueID = PBRFF.SourceUniqueID
      and
      PBRFF.EncounterTypeCode = 'OP'
      and
      PBRFF.DatasetCode = 'ENCOUNTER'

    
    Where 
  PBR.EncounterTypeCode = 'OP'
  and
  PBR.DatasetCode = 'ENCOUNTER'
and
--PBR.[AppointmentDate] <=
--(select 
-- max(Cal.TheDate)
--FROM WHREPORTING.LK.Calendar Cal
--  where
--Cal.TheMonth = 
--(select distinct
--SQ.TheMonth
--  FROM WHREPORTING.LK.Calendar SQ
--  where SQ.TheDate = 
-- (
--dateadd(MONTH,-1,cast(GETDATE()as DATE))
--)
--))
--and
PBRFF.SourceUniqueID is null
and
PBR.ExclusionReason is null
and 
cast(RIGHT(Cal.[FinancialMonthKey],2)  as int) in (Select Distinct Period from PbR2015.dbo.PbRCut  where CutTypeCode = 'FREEZE')

Group By
RIGHT(Cal.[FinancialMonthKey],2) 
--,Cal.TheMonth
--    ,PBR.[InterfaceCode]
--      ,PBR.[EncounterTypeCode]
--      ,PBR.[DatasetCode]
      ,Case
      when PBR.FirstAttendanceCode = 1 
      then 'New'
      else 'FollowUp'
      end 
        ,PBR.FirstAttendanceCode
--          ,PBR.[ClinicCode]
--          ,OP.Clinic
--,OP.[SpecialtyCode(Function)]
--,OP.[Specialty(Function)]
       ,PBR.[TreatmentFunctionCode]
      --,PBR.[TreatmentFunctionCodeFinal] 
      --,PBR.[ConsultantCode]

  
  Order by 
  RIGHT(Cal.[FinancialMonthKey],2) desc,
  Case
      when PBR.FirstAttendanceCode = 1 
      then 'New'
      else 'FollowUp'
      end desc,
  PBR.[TreatmentFunctionCode]