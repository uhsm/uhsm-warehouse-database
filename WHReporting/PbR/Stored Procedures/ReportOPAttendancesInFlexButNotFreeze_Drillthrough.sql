﻿/*
--Author: N Scattergood
--Date created: 20/11/2013
--Stored Procedure Built for SRSS Report on:
--PbR OP Attendances In PbR Flex but not Freeze 
--This procedure provides the Drillthrough Patient Detail
--Amended 30/03/15 so it works off PbR2014 rather than table in WHREPORTING
*/

CREATE Procedure [PbR].[ReportOPAttendancesInFlexButNotFreeze_Drillthrough]

@FirstFollowUp	as nvarchar(2) = null 
,@FinMonth		as nvarchar(2) = null 
,@TFC			as nvarchar(5) = null 
as

SELECT 
OP.AppointmentDate
,Cal.TheMonth
,OP.FacilityID
,PbR.PatientSurname
	--,PBR.[InterfaceCode]
 --     ,PBR.[EncounterTypeCode]
 --     ,PBR.[DatasetCode]
      ,Case
      when PBR.FirstAttendanceCode = 1 
      then 'New'
      else 'FollowUp'
      end as FirstFollowUp
        --,PBR.FirstAttendanceCode
		,PBR.[ClinicCode]
		,OP.Clinic
		,OP.ProfessionalCarerName
       ,PBR.[TreatmentFunctionCode]
      --,PBR.[TreatmentFunctionCodeFinal] 
      ,PBR.CommissionerCodeCCG
      ,CCG.PCTCCG as Commissioner
      --,PBR.[ConsultantCode]
        
 FROM [PbR2015].dbo.PbR PBR
  
  left join [WHREPORTING].[LK].[Calendar] Cal
	on Cal.TheDate = PbR.EncounterEndDate
	
	left join WHREPORTING.OP.Schedule OP
	on OP.EncounterRecno = PBR.SourceEncounterRecno
	
	left join [PbR2015].dbo.PbRCut PBRFF
	on PBR.SourceUniqueID = PBRFF.SourceUniqueID
	and
	PBRFF.EncounterTypeCode = 'OP'
	and
	PBRFF.DatasetCode = 'ENCOUNTER'
	
							left join WHREPORTING.LK.PasPCTOdsCCG CCG
						on PbR.CommissionerCodeCCG = CCG.PCTCCGCode

    
    Where 
  PBR.EncounterTypeCode = 'OP'
  and
  PBR.DatasetCode = 'ENCOUNTER'

and
PBRFF.SourceUniqueID is null
and
PBR.ExclusionReason is null
and 
cast(RIGHT(Cal.[FinancialMonthKey],2)  as int) in (Select Distinct Period from PbR2015.dbo.PbRCut  where CutTypeCode = 'FREEZE')

---------------Following Come from Parameters-------
 --and
 --PBR.FirstAttendanceCode in  (SELECT Val from dbo.fn_String_To_Table(@FirstFollowUp,',',1))
 --and 
 --RIGHT(Cal.[FinancialMonthKey],2) in  (SELECT Val from dbo.fn_String_To_Table(@FinMonth,',',1))
 --and
 --PBR.[TreatmentFunctionCode] in (SELECT Val from dbo.fn_String_To_Table(@TFC,',',1))
and
(
(PBR.FirstAttendanceCode = @FirstFollowUp)
or 
(@FirstFollowUp is null)
)
and
(
( RIGHT(Cal.[FinancialMonthKey],2) = @FinMonth)
or 
(@FinMonth is null)
)
and
(
(PBR.[TreatmentFunctionCode] = @TFC)
or 
(@TFC is null)
) 
 
  
  Order by 
  OP.AppointmentDate asc,
  PBR.[TreatmentFunctionCode],
  PbR.CommissionerCodeCCG