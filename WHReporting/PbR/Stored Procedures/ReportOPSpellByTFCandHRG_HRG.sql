﻿/*
--Author: N Scattergood
--Date created: 25/09/2013
--Stored Procedure Built for SRSS Report on:
--PbR OP Attendances by TFC and HRG with Drill-through Report (RD430)
--This procedure provides a Parameter
*/

Create Procedure [PbR].[ReportOPSpellByTFCandHRG_HRG]
@TFCPar	as nvarchar(1000) 
,@ConsPAR as nvarchar(4000)
,@PODPar as nvarchar(1000)
--,@HRGPar as nvarchar(4000)
as

SELECT 

Distinct
	--RIGHT(Cal.[FinancialMonthKey],2) as FinMonth,
      --case 
      --when [TreatmentFunctionCode] = '904'
      --then 650
      --else [TreatmentFunctionCode]
      --end						as TFCParCode
      --,TFCDesc					as TFCParDesc
     --PbR.ConsultantCode as ConsParCode
     -- ,Con.ConsName as ConsParDesc 
		--PODCode
		PbR.[HRGCode]
     

      
  FROM PbR2013.dbo.PbR PbR
  
  left join [WHREPORTING].[OP].[Schedule] OP
  on OP.EncounterRecno = PbR.SourceEncounterRecno

	 --  left join  WH.PAS.CodingBase C
	 -- on C.CODE = PbR.[DominantProcedureCode]
	 -- and C.ARCHV_FLAG = 'N'
	 -- and CCSXT_CODE like 'OPCS%' 
	 -- and END_DTTM is null
		
			--left join [WHREPORTING].[LK].[Calendar] Cal
			--on Cal.TheDate = PbR.EncounterEndDate	
						
				--left join
				--[WHREPORTING].[LK].[PasPCTOdsCCG] CCG
				--on PBR.[CommissionerCodeCCG] = CCG.PCTCCGCode
				
					--left join
					--[PbR2013].[dbo].[HRGLookup] HRG
					--on PBR.HRGCode = HRG.HRGCode
					
						--left join
						--(
						--SELECT Distinct
						--[MAIN_IDENT] as TFCCode
						--,[DESCRIPTION] as TFCDesc
						--  FROM [WH].[PAS].[SpecialtyBase]
						--	where
						--  ARCHV_FLAG = 'N'
						--  and
						--  isnumeric([MAIN_IDENT]) = 1
						--  and 
						--  LEN ([MAIN_IDENT]) = 3
						--) Spec
						--on Spec.TFCCode =       case 
						--						  when [TreatmentFunctionCode] = '904'
						--						  then 650
						--						  else [TreatmentFunctionCode]
						--						  end	
						
						--left join
						--(
						--select 
						--distinct
						--NationalConsultantCode as ConsCode
						--,Consultant as ConsName
						--from WH.PAS.LocalConsultantSpecialty
						--) Con
						--on Con.ConsCode = PbR.ConsultantCode
  
  Where 
  EncounterTypeCode = 'OP'
  and
  DatasetCode = 'ENCOUNTER'
  and 
  PbRExcluded = 0
  and 
  TreatmentFunctionCode is not null
  
    ----Following Comes from Parameters---------
 and
case 
when [TreatmentFunctionCode] = '904'
then 650
else [TreatmentFunctionCode]
end in (SELECT Val from dbo.fn_String_To_Table(@TFCPar,',',1))
 and
 PbR.ConsultantCode in (SELECT Val from dbo.fn_String_To_Table(@ConsPAR,',',1))
 and
 PbR.PODCode in (SELECT Val from dbo.fn_String_To_Table(@PODPar,',',1))
 --and
 --PbR.HRGCode in  (SELECT Val from dbo.fn_String_To_Table(@HRGPAR,',',1))
  
order by
PbR.HRGCode