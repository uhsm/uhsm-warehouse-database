﻿/*
--Author: N Scattergood
--Date created: 24/09/2013
--Stored Procedure Built used for SSRS Report on Overseas Inpatients (RD375)
--Amended for 2014/15 on the 1st May 2014
--Amended for 2015/16 on the 7th May 2015 (Plus New Cardiac Categories Added)
*/


CREATE Procedure [PbR].[ReportOverseaInpatients_2015]

as
SELECT 
RIGHT(Cal.[FinancialMonthKey],2) as FinMonth
	,PBR.[InterfaceCode]
      ,PBR.[EncounterTypeCode]
      ,PBR.[DatasetCode]
      ,PBR.[CommissionerCodeCCG]
      ,PBR.[SourceUniqueID] as SpellNumber
      ,PBR.[DistrictNo]
    ,PBR.[NHSNumber]
      ,PBR.[PatientSurname]+', '+PBR.[PatientForename]+', '+PBR.[PatientTitle] as PatientName
      ,PBR.[PatientAddress1]+','+PBR.[PatientAddress2]+','+PBR.[PatientAddress3]+','+PBR.[PatientAddress4] as PatientAddress
      ,PBR.[Postcode]
       ,PBR.[Age]      
       ,PBR.[DateOfBirth]
      ,PBR.[DateOfDeath]
      ,PBR.[AdmissionMethodCode] 
        ,PBR.[PODCode]     
       --,PBR.[TreatmentFunctionCode]
      ,TreatmentFunctionCodeFinal = 
		--Case 
		--	when CardiacCategory in (
		--		'CABG'
		--		,'Cardiac Surgery Other'
		--		,'TAVI'
		--		,'Valve'
		--		)
		--		then 170
		--	when CardiacCategory in (
		--		'Non CT Other'
		--		,'Thoracic Surgery'
		--		)
		--		then 173
		--	when CardiacCategory = 'Transplant'
		--		then 174
		--	when CardiacCategory in (
		--		'Ablat'
		--		,'Angio'
		--		,'Defib'
		--		,'EPS'
		--		,'Gen Card'
		--		,'Loop'
		--		,'PACE DUAL'
		--		,'PACE SING'
		--		,'PTCA'
		--		,'Trans ECG'
		--		)
		--		then 320
		--	else
			 TreatmentFunctionCode
			 ,CardiacCategoryTFC
			 ,CardiacCategory
		
      ,PBR.[ConsultantCode]
   ,PBR.[AdmissionDate]
   ,PBR.[DischargeDate]
       ,PBR.[SpellLOS]
       ,PBR.[EpisodeCount]
      ,PBR.[ExcessBedDays]
      ,ExcessBedDaysFinal = 
		case 
			when ExcessBedDays - CriticalCareDays < 0
			then 0
			else ExcessBedDays - CriticalCareDays
		end
      ,PBR.[TrimPoint]
      ,PBR.[CriticalCareDays]
      --,PBR.[CardiacCategory]


      --,PBR.[AppointmentDate]
      --,PBR.[AttendanceID]
      --,PBR.[AttendanceOutcomeCode]
      --,PBR.[ClinicCode]


      ,PBR.[HRGCode]
      ,PBR.[PrimaryDiagnosisCode]
      ,PBR.[PrimaryProcedureCode]
      ,PBR.[PrimaryProcedureDate]
      ,PBR.[DominantProcedureCode]
      --,PBR.[RegisteredGpCode]
      --,PBR.[RegisteredGpPracticeCode]
      --,PBR.[SourceOfAdmissionCode]
      ,PBR.[ServiceCode]
      ,PBR.[BestPracticeCode]
      ,PBR.[BestPracticeSectionCode]
    
      ,PBR.[PbRExcluded]
      ,CDSSLACode  = 
		case
			when PbRExcluded = 1 
			then ExclusionType
			else FinalSLACode
		end
      ,PBR.[ModelTypeCode]

  FROM PbR2015.dbo.[PbR] PBR
  
  left join [WHREPORTING].[LK].[Calendar] Cal
	on Cal.TheDate = PbR.EncounterEndDate
	--left join WHREPORTING.APC.Patient P
	--on P.EncounterRecno = PBR.SourceEncounterRecno
  
  
    Where 
  EncounterTypeCode = 'IP'
  and
  DatasetCode = 'ENCOUNTER'
  and
  CommissionerCodeCCG in ('TDH','VPP')

  
  
  Order by 
  FinMonth desc,
  CommissionerCodeCCG,
  EncounterEndDate