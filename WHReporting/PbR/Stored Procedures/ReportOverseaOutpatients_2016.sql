﻿/*
--Author: N Scattergood
--Date created: 24/09/2013
--Stored Procedure Built used for SSRS Report on Overseas Outpatients (RD375)
--Amended for 2014/15 on the 1st May 2014
--Amended for 2015/16 on the 7th May 2015
--Amended for 2016/17 on the 3rd May 2016 
*/


CREATE Procedure [PbR].[ReportOverseaOutpatients_2016]

as
SELECT 
RIGHT(Cal.[FinancialMonthKey],2) as FinMonth
	,PBR.[InterfaceCode]
      ,PBR.[EncounterTypeCode]
      ,PBR.[DatasetCode]
      ,PBR.[CommissionerCodeCCG]

      ,PBR.[DistrictNo]
    ,PBR.[NHSNumber]
      ,PBR.[PatientSurname]+', '+PBR.[PatientForename]+', '+PBR.[PatientTitle] as PatientName
      ,PBR.[PatientAddress1]+','+PBR.[PatientAddress2]+','+PBR.[PatientAddress3]+','+PBR.[PatientAddress4] as PatientAddress
      ,PBR.[Postcode]
       ,PBR.[Age]      
       ,PBR.[DateOfBirth]
      ,PBR.[DateOfDeath]      
      ,PBR.AdminCategoryCode
      , Case
      when PBR.AdminCategoryCode = '01'
      then 'NHS Patient'
      when PBR.AdminCategoryCode = '02'
      then 'Private Patient'
      when PBR.AdminCategoryCode = '03'
      then 'Amenity Patient'
      when PBR.AdminCategoryCode = '04'
      then 'Category II Patient'
      when PBR.AdminCategoryCode = '98'
      then 'Not Applicable'
      else 'Not Known'
      end as AdminCategory
      ,PBR.[SourceUniqueID] as ApptRef
          ,PBR.[AppointmentDate]
      ,PBR.[AttendanceID]
      --,PBR.[AttendanceOutcomeCode]
      ,PBR.[ClinicCode]

        ,PBR.[PODCode]     
       --,PBR.[TreatmentFunctionCode]
      ,TreatmentFunctionCodeFinal = TreatmentFunctionCode --it's the same for Outpatients 
      ,PBR.[ConsultantCode]
      ,PBR.[HRGCode]
      ,PBR.[DominantProcedureCode]
      --,PBR.[RegisteredGpCode]
      --,PBR.[RegisteredGpPracticeCode]
      --,PBR.[SourceOfAdmissionCode]

      ,PBR.[PbRExcluded]
      ,CDSSLACode  = 
		case
			when PbRExcluded = 1 
			then ExclusionType
			else FinalSLACode
		end
      ,PBR.[ModelTypeCode]
FROM PbR2016.dbo.[PbRCut] PBR 
  
  left join [WHREPORTING].[LK].[Calendar] Cal
	on Cal.TheDate = PbR.EncounterEndDate

    
    Where 
  EncounterTypeCode = 'OP'
  and
  DatasetCode = 'ENCOUNTER'
  and
  CommissionerCodeCCG in ('TDH','VPP')
 
  
  Order by 
  FinMonth,
  CommissionerCodeCCG,
  EncounterEndDate