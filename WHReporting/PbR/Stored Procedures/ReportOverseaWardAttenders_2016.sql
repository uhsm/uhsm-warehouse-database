﻿/*
--Author: N Scattergood
--Date created: 24/09/2013
--Stored Procedure Built used for SSRS Report on Overseas WardAttenders (RD375)
--Amended for 2014/15 on the 1st May 2014
--Amended for 2015/16 on the 7th May 2015
--Amended for 2016/17 on the 3rd May 2016 
*/


CREATE Procedure [PbR].[ReportOverseaWardAttenders_2016]

as
SELECT 
RIGHT(Cal.[FinancialMonthKey],2) as FinMonth
	,PBR.[InterfaceCode]
      ,PBR.[EncounterTypeCode]
      ,PBR.[DatasetCode]
      ,PBR.[CommissionerCodeCCG]

      ,PBR.[DistrictNo]
    ,PBR.[NHSNumber]
      ,P.[PatientSurname]+', '+P.[PatientForename]+', '+P.[PatientTitle] as PatientName
      ,P.[PatientAddress1]+','+P.[PatientAddress2]+','+P.[PatientAddress3]+','+P.[PatientAddress4] as PatientAddress
      ,P.[Postcode]
      ,P.EthnicGroup
      ,P.Sex as Gender
         ,PBR.[DateOfBirth]
      ,PBR.[DateOfDeath]      
      ,PBR.[SourceUniqueID] 
          ,PBR.[AppointmentDate]
      --,PBR.[AttendanceOutcomeCode]
      ,PBR.[ClinicCode]

        ,PBR.[PODCode]     
       --,PBR.[TreatmentFunctionCode]
      ,TreatmentFunctionCodeFinal = TreatmentFunctionCode --it's the same for Outpatients 
      ,PBR.[HRGCode]
      --,PBR.[DominantProcedureCode]
      --,PBR.[RegisteredGpCode]
      --,PBR.[RegisteredGpPracticeCode]
      --,PBR.[SourceOfAdmissionCode]

      ,PBR.[PbRExcluded]
      ,CDSSLACode  = 
		case
			when PbRExcluded = 1 
			then ExclusionType
			else FinalSLACode
		end
      ,PBR.[ModelTypeCode]



 FROM PbR2016.dbo.[PbRCut] PBR
  
  left join [WHREPORTING].[LK].[Calendar] Cal
	on Cal.TheDate = PbR.EncounterEndDate
	
			left join WHREPORTING.OP.Patient P
		on P.EncounterRecno = PBR.SourceEncounterRecno

    
    Where 
  EncounterTypeCode = 'WA'
  and
  DatasetCode = 'WardAtt'
  and
  CommissionerCodeCCG in ('TDH','VPP')
 
  
  Order by 
  FinMonth,
  CommissionerCodeCCG,
  EncounterEndDate