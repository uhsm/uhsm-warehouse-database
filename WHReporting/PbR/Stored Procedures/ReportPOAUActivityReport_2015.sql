﻿/*
--Author: N Scattergood
--Date created: 30/08/2013
--Stored Procedure Built used for SSRS Report on POAU Activity Report (RD426)
--Amended for 2014/15 on the 1st May 2014
--Amended for 2015/16 on the 7th May 2015
*/

Create Procedure [PbR].[ReportPOAUActivityReport_2015]
  
as
SELECT 

        LOS=
        case              
        when P.SpellLOS=0
        then 'LOS = 0'
        else 'LOS > 0'
        end
        ,T.[SLACode]
        ,CCG.PCTCCG as CCGDescription
        ,T.[PeriodNo]
       ,T.[TreatmentFunctionCode]
       ,T.[HRGCOde]
      ,T.[PODCode]
      ,T.[SourceUniqueID]
      ,T.[EncounterStartDate]
      ,T.[EncounterEndDate]
      ,S.AdmissionDateTime
      ----,S.AdmissionTime
      ,Case
      when datepart(hour,S.AdmissionTime) between 0 and 9
      then '0000 – 0959'
      when datepart(hour,S.AdmissionTime) between 10 and 20
      then '1000 – 2059'
      when datepart(hour,S.AdmissionTime) between 21 and 23
      then '2100 - 2359'
      End as TimeCategory
       --,Activity=Sum(TPbRExtract.[Cases])
       
  FROM [PbR2015].[dbo].[TPbRExtract] T
  
  inner join
  PBR2015.dbo.PbRCut P
  on  T.SourceUniqueID= P.ProviderSpellNo
    
  left join WH.PAS.ServicePoint Ward
on	cast(Ward.ServicePointCode as varchar) = P.StartWardTypeCode
and	P.EncounterTypeCode = 'IP'

left join WHREPORTING.APC.Spell S
on S.SourceSpellNo = T.SourceUniqueID

left join
[WHREPORTING].[LK].[PasPCTOdsCCG] CCG
on T.[SLACode] = CCG.PCTCCGCode
  
  
  where
  --T.PODCode='NEL'
  --and
  --T.TreatmentFunctionCode ='420'
  --and
  DatasetCode='encounter'
  and 
  Ward.ServicePointLocalCode='POAU'
  
  --Group by
  --case              
  --      when
  --      pbrcut.SpellLOS=0
  --      then
  --      'LOS = 0'
  --      else
  --      'LOS > 0'
  --      end
        
  --      ,TPbRExtract.[SLACode]
  --     ,TPbRExtract.[TreatmentFunctionCode]
  --     ,TPbRExtract.[HRGCOde]
  --    ,TPbRExtract.[PODCode]
  --    ,TPbRExtract.[PeriodNo]
  --    ,TPbRExtract.[SourceUniqueID]      
  --    ,TPbRExtract.[EncounterStartDate]
  --    ,TPbRExtract.[EncounterEndDate]
      
      order by 
    T.[PeriodNo] desc,
    [EncounterEndDate],S.AdmissionTime