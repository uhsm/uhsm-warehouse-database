﻿/*
--Author: N Scattergood
--Date created: 30/09/2013
--Stored Procedure Built for SRSS Report on:
--PbR Exclusions Report (2014)
--Duplicate Report created so we can see new 2013 and 2014 until M2 on 02/05/14
--Amended (new version) for 2015 10th June 2015
--Provides Drillthrough Fields
*/

CREATE Procedure [PbR].[ReportPbRExclusions2015_Drillthrough]

@TFCPar	as nvarchar(500) 
,@CCG as nvarchar(1000) 
,@FinMonth as nvarchar(2) = null 
,@DataSetPar as nvarchar(10) = null
,@EncounterTypePar as nvarchar(5) = null 
,@ExclusionTypePar as nvarchar(20) = null 
,@ExclusionReasonPar as  nvarchar(50) = null 
 
as
SELECT 
[CutTypeCode]
      ,[Period]
      --,[IsActive]
      --,[EncounterRecno]
      ,[DatasetCode]
      ,[SourceUniqueID]
      --,[SourceEncounterRecno]
      --,[InterfaceCode]
      ,[EncounterTypeCode]
      ,[EncounterStartDate]
      ,[EncounterEndDate]
      --,[Cases]
      --,[Age]
      --,[Excluded]
      ,[ExcessBedDays]
      
      --,[NationalTariff]
      --,[SpecialistServiceSupplement]
      --,[ChildrensServicesSupplement]
      --,[ShortStaySupplement]
      --,[LongStaySupplement]
      --,[TrimPoint]
      --,[LongStayTariff]
      --,[ShortStayFlag]
      --,[ChildrenFlag]
      --,[TotalTariff]
      --,[LocalTariff]
      --,[ActualTariff]
      --,[Mandatory]
      --,[AdminCategoryCode]
      --,[AdmissionDate]
      ,[AdmissionMethodCode]
      --,[AppointmentDate]
      ,[AttendanceID]
      ,[AttendanceOutcomeCode]
      ,[CardiacCategory]
      --,[CarerSupportIndicator]
      --,[CasenoteNo]
      ,[CategoryCode]
      ,[CCGCode]
      ,[ClinicCode]
      --,[ClinicFunctionCode]
      --,[CommissionerCode]
      --,[CommissionerCodeCCG]
      --,[CommissionerReferenceNo]
      --,[CommissioningSerialNo]
      ,[ConsultantCode]
      ,[DNACode]
      --,[DateOfBirth]
      --,[DateOfDeath]
      --,[DecidedToAdmitDate]
      --,[DischargeDate]
      --,[DischargeDestinationCode]
      --,[DischargeMethodCode]
      --,[DischargeReadyDate]
      --,[DistrictNoOrganisationCode]
      ,[DistrictNo]
      --,[DurationOfElectiveWait]
      --,[EarliestReasonableOfferDate]
      --,[EndLocationClassCode]
      --,[EndLocationTypeCode]
      --,[EndSiteCode]
      --,[EndWardTypeCode]
      --,[EpisodeCount]
      ,[EpisodeEndDate]
      --,[EpisodeNo]
      ,[EpisodeStartDate]
      --,[EpisodicAdminCategoryCode]
      --,[EpisodicLegalStatusClassificationCode]
      --,[EthnicCategoryCode]
      ,[FirstAttendanceCode]
      --,[FirstRegularDayNightAdmissionFlag]
      ,[HRGCode]
      --,[HRGVersionCode]
      --,[ICDDiagnosisSchemeCode]
      --,[LastDNAOrCancelledDate]
      --,[LastEpisodeInSpellIndicator]
      --,[LegalStatusClassificationCode]
      --,[LocationClassCode]
      --,[LocationTypeCode]
      --,[ManagementIntentionCode]
      --,[MaritalStatusCode]
      --,[MedicalStaffTypeCode]
      --,[NHSNumberStatusId]
      ,[NHSNumber]
      --,[NHSServiceAgreementLineNo]
      --,[NeonatalLevelOfCareCode]
      --,[OPCSProcedureSchemeCode]
      --,[OperationStatusCode]
      --,[PASDGVPCode]
      --,[PASHRGCode]
      --,[PCTCode]
      --,[PathwayIdIssuerCode]
      --,[PathwayId]
      --,[PatientAddress1]
      --,[PatientAddress2]
      --,[PatientAddress3]
      --,[PatientAddress4]
      ,[PatientClassificationCode]
      ,[PatientForename]
      ,[PatientSurname]
      --,[PatientTitle]
      --,[Postcode]
      --,[PrimaryDiagnosisCode]
      --,[PrimaryProcedureCode]
      --,[PrimaryProcedureDate]
      --,[PriorityTypeCode]
      --,[ProviderCode]
      --,[ProviderReferenceNo]
      ,[ProviderSpellNo]
      --,[PsychiatricPatientStatusCode]
      --,[RTTEndDate]
      --,[RTTStartDate]
      --,[RTTStatusCode]
      --,[ReferralRequestReceivedDate]
      --,[ReferrerCode]
      --,[ReferrerOrganisationCode]
      --,[RegisteredGpCode]
      ,[RegisteredGpPracticeCode]
      --,[ServiceTypeRequestedCode]
      --,[SexCode]
      --,[SiteCode]
      --,[SourceOfAdmissionCode]
      --,[SourceOfReferralCode]
      --,[SpecialistServiceCode]
      ,[SpecialtyCode]
      ,[SpellLOS]
      --,[StartLocationClassCode]
      --,[StartLocationTypeCode]
      --,[StartSiteCode]
      --,[StartWardTypeCode]
      ,[StartWard]
      ,[TreatmentFunctionCode]
      --,[TrustNo]
      --,[UniqueBookingReferenceNo]
      --,[VersionCode]
      --,[AEAttendanceDisposal]
      --,[ArrivalDate]
      --,[InvestigationCodeList]
      --,[SLACategoryCode]
      --,[ServiceLineCode]
      --,[ServiceLineBaseCode]
      --,[ServiceCode]
      ,[SLACode]
      --,[DominantProcedureCode]
      --,[DepartureCode]
      --,[LocalConsultantCode]
      --,[EarlySupportedDischarge]
      --,[BestPracticeTariff]
      ,[BestPracticeCode]
      --,[BestPractice]
      --,[PbRMandated]
      --,[SpellProgramBudgetCode]
      --,[AllocatedTreatmentFunctionCode]
      --,[ContractKey]
      --,[BestPracticeSectionCode]
      ,[PODCode]
      --,[SourcePatientNo]
      --,[PbRExcluded]
      ,[ExclusionType]
      ,[ExclusionReason]
      ,[ModelTypeCode]
      ,[CriticalCareDays]
      --,[CensusDate]
      ,[FinalSLACode]
      ,CCG.PCTCCG as CCGDescription
  FROM 
  --[PbR2013].[Archive].[PbR_1stCut_Month4_20130813]
  [PbR2015].dbo.[PbRCut] PBR
  
  	--		left join [WHREPORTING].[LK].[Calendar] Cal
			--on Cal.TheDate = PbR.DischargeDate
						
				left join
				[WHREPORTING].[LK].[PasPCTOdsCCG] CCG
				on PBR.FinalSLACode = CCG.PCTCCGCode
  
  where
  PbRExcluded = 1
  --and
  --DatasetCode in ('AICU','CC','ECMO','ENCOUNTER','PICU','WARDATT')
  
   ----Following Comes from Parameters---------
 and
 case
 when TreatmentFunctionCode is null 
 then '(blank)'
 else
 TreatmentFunctionCode end in (SELECT Val from dbo.fn_String_To_Table(@TFCPar,',',1))
 and
 [FinalSLACode] in (SELECT Val from dbo.fn_String_To_Table(@CCG,',',1))

and
(
([Period] = @FinMonth)
or 
(@FinMonth is null)
)
and
(
([DatasetCode] = @DataSetPar)
or 
(@DataSetPar is null)
)
and
(
([EncounterTypeCode] = @EncounterTypePar)
or 
(@EncounterTypePar is null)
)    
and
(
([ExclusionType] = @ExclusionTypePar)
or 
(@ExclusionTypePar is null)
)
and
(
([ExclusionReason] = @ExclusionReasonPar)
or 
(@ExclusionReasonPar is null)
)


order by [EncounterEndDate] desc