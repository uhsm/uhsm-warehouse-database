﻿/*
--Author: N Scattergood
--Date created: 05/06/2014
--Stored Procedure Built for PbR Report on Patients who should be coming through as RA60Z
--This currently provides patient level data 
--Will be converted to an SRSS Report when Finance signs it off
--Amended to 15/16 11th May 2015
--Amended to 16/17 3rd May 2016
*/

CREATE Procedure [PbR].[ReportPrismRA60Z_2016]
as
SELECT DISTINCT 
[CutTypeCode]
      ,[Period]
      ,[CensusDate]
      ,FinalSLACode
      ,CCGCode
      ,[PbRExcluded]
,ExclusionReason
,ExclusionType
      ,[SourceUniqueID] as ReportMasterID
      ,[DistrictNo] 
      ,[NHSNumber]
      --,[UniqueBookingReferenceNo]
      --,PathwayId
       ,[AppointmentDate]
       ,CategoryCode
         ,[PODCode]
       ,[TreatmentFunctionCode]
      ,[ClinicCode]
      ,[ConsultantCode]
      ,Con.ConsultantName
      ,[HRGCode] as HRGCodeInPBR
      ,HRGLKP.XrefEntityCode as PRISMHRG
      ,[PrimaryProcedureCode]
      ,[SpecialistServiceCode]
   

FROM   PbR2016.dbo.PbRCut PbR

       LEFT OUTER JOIN (SELECT *
                        FROM   PbR2016.dbo.EntityXref
                        WHERE  EntityTypeCode = 'OPCSCodesForPRISM')
                        HRGLKP--this table holds the procedures which need to be re-mapped to a different HRG code for the PRISM data
						ON PbR.PrimaryProcedureCode = HRGLKP.EntityCode
						
												
						left join
						(
						select 
						distinct
						NationalConsultantCode as ConsCode
						,Consultant as ConsultantName
						from WH.PAS.LocalConsultantSpecialty
						) Con
						on Con.ConsCode = PbR.ConsultantCode
		
		WHERE  
		EncounterTypeCode = 'OP'
        AND
        DatasetCode = 'ENCOUNTER'
        AND 
        left(PbR.ClinicCode,3) = 'CDC'
        --IN ( 'CDCDSE1', 'CDCDSE2', 'CDCDSE3', 'CDCDSE4',
        --                         'CDCDSE5', 'CDCECHO1', 'CDCECHO2', 'CDCECHO3',
        --                         'CDCECHO4', 'CDCECHO5', 'CDCEXER1', 'CDCEXER2',
        --                         'CDCEXER3', 'CDCEXER4', 'CDCEXER5', 'CDCTAPE1',
        --                         'CDCTAPE2', 'CDCTAPE3', 'CDCTAPE4', 'CDCTAPE5',
        --                         'CDCVIR2', 'CDCVIR1AH', 'CDCVIR3AH', 'CDCVIR4AH', 'CDCVIR5AH' ) --This is the list of PRISM clinics
        AND HRGLKP.XrefEntityCode = 'RA60Z'  

		
		ORDER BY
		[AppointmentDate] desc