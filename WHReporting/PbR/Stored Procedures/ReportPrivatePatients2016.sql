﻿/* =============================================
Modifications:
Modified by			Date				Modification
NScat				03/05/2016			Switched to PbR 2016 for 16/17 Reporting	
							
=============================================*/

CREATE Procedure [PbR].[ReportPrivatePatients2016]
as
SELECT
PbR.DataSetCode
,SPL.AdmissionDivision,
Cal.FinancialQuarter AS 'Quarter',
RIGHT(Cal.FinancialMonthKey,2) AS 'Month',
PbR.CCGCode AS 'CCG',
SLA.ContractTypeCode AS 'Cont',
SPL.[AdmissionSpecialtyCode(Function)] AS 'SpecCode',
SPL.[AdmissionSpecialty(Function)] AS 'Spec',
PbR.DistrictNo AS 'RM2',
PbR.NHSNumber AS 'NHS No.',
PbR.ProviderSpellNo AS 'Spell No.',
PbR.PatientForename,
PbR.PatientSurname,
PrivatePat.Hospital AS 'Hospital',
PbR.AdmissionDate,
PbR.DischargeDate,
PbR.ExcessBedDays,
PbR.SpellLOS,
PbR.CriticalCareDays,
PbR.PODCode AS 'POD',
PbR.HRGCode,
WS.WardCode,
PbR.ActualTariff AS 'National Tariff',

COALESCE(CriticalCareDays,0) * 1100 AS 'Local Crit Tariff'
 
FROM PbR2016.dbo.PbR PbR

LEFT JOIN WH.PAS.ServicePointBase SPB
ON PbR.StartWardTypeCode = SPB.SPONT_REFNO

inner JOIN [PbR2016].lk.PrivateSector PrivatePat
ON SPB.SPONT_REFNO_CODE = PrivatePat.[Private Sector Ward Code]

LEFT JOIN [PbR2016].dbo.SLA SLA
ON PbR.CCGCode = SLA.SLACode

LEFT JOIN WHREPORTING.APC.Spell SPL
ON PbR.ProviderSpellNo = SPL.SourceSpellNo

LEFT JOIN WHREPORTING.LK.Calendar Cal
ON PbR.DischargeDate = Cal.TheDate

LEFT JOIN WHREPORTING.APC.WardStay WS
ON PbR.ProviderSpellNo = WS.SourceSpellNo
AND WS.WardCode = 'ALEXICU'



WHERE 
--PbR.DischargeDate >= '01 Apr 2014'
--AND 
SPB.ARCHV_FLAG = 'N'
AND 
PbR.DatasetCode <> 'CC'

ORDER BY PbR.DischargeDate desc