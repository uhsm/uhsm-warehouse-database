﻿/*
--Author: H Slim
--Date created: 05/06/2014
--Stored Procedure Built used for SSRS Report on Speech and Language Therapy Contacts which are chargable under PbR
--Report Spec ID RD485
--
*/
CREATE Procedure [PbR].[ReportSaLTContacts]
@DateFrom as Date
,@DateTo  as Date

as


SELECT 
  --A.[Cont_Ref]
    A.[FACIL_ID]
    ,A.[NHS_number]
   ,A.[Cont_Date]
   ,A.[Contact_Type]
   ,A.[Prof_Carer_Name]
   ,r.EpisodePracticeCode
   ,r.EpisodeCCGCode
   
  FROM [WHREPORTING].[TPY].[ContactsDataset]AS A
  
  LEFT JOIN LK.Calendar AS C
  ON A.Cont_Date=C.TheDate
  
  left join RF.Referral r
 on A.Ref_Ref = r.ReferralSourceUniqueID
  
 where A.[Spec_Code] in ('652','652S SPEECH','652SVS','652SW','652V VOICE','9014')


 and A.Cont_Date BETWEEN @DateFrom AND @DateTo
 
 and Contact_Type not like 'Other%'
 
 and A.Location_Type in ('Nursing Home (NHS)','Nursing Home (non-NHS)','Patients Home','Patient''s Visiting Address')
 
 and A.[Cancellation_Date] is null
 
 and A.Attend_Status = 'attended'
 
 order by A.Cont_Date desc