﻿/*
--Author: N Scattergood
--Date created: 07/10/2013
--Stored Procedure Built used for SSRS Report on Stroke Activity Report (RD432)
--Duplicate Report created so we can see new 2013 and 2014 until M2 on 02/05/14
--Or added to capture HRGs of AA22B and AA23B on request of Liesl 09/05/14
--Amended for 2015/16 on the 7th May 2015 (Duplicated Procedure)
*/

CREATE Procedure [PbR].[ReportStrokeActivity2015]

as

SELECT  [MonthNo] as FinMonth
,[ReportMasterID] as ProviderSpellNo
,[UD1Act_Code] as DistrictNo
,FF.Age
,[PCT_Code] as CommissionerCode
  ,CCG.PCTCCG as CCGDescription
      ,[SpellAdmissionDate] as AdmissionDate
      ,[SpellDischargeDate] as DischargeDate

      --,[Spec_Code]
      ,[HRG_Code] as HRGCode
      ,HRG.[HRG] as HRGDescription
      ,SpecService_ID
      ,case 
      when SpecService_ID is null 
      then HRG_Code
      else [HRG_Code]+'/'+SpecService_ID 
      end as HRG
      --,[UD4Act_Code] as PrimaryDiagnosisCode
      --,[UD5Act_Code] as PrimaryProcedureCode
--     ,FF.PrimaryProcedureDate
--,FF.DominantProcedureCode
      ,[POD_Code] as PODCode
      ,[LOS]
      --,[Spec_Code]
      ,[Activity]
      --,[SpecService_ID]
      --,[SpecService_ID_2]
      --,[SpecService_ID_3]
      --,[Gpp_Code]
      --,[Pat_Code]
      --,[DOB]
      --,[Cons_Code]
      --,[LOS]
      --,[ReportMasterID]
      --,[UD1Act_Code]
      --,[UD2Act_Code]
      --,[UD3Act_Code]
      --,[UD4Act_Code]
      --,[UD5Act_Code]
      --,[SpellAdmissionDate]
      --,[SpellDischargeDate]
      --,[PatClass]
      --,[AdmMethod]
      
       --,CAST(PBR.[ReportMasterID] as varchar(9))+PBR.[POD_Code] as JoinKey
       
  FROM [PbR2015].[dbo].[SLAMInpatientAndCustomPODBase] PbR
  
left join [WHREPORTING].[LK].[Calendar] Cal
on Cal.TheDate = PbR.SpellDischargeDate

left join
[WHREPORTING].[LK].[PasPCTOdsCCG] CCG
on left(PBR.PCT_Code,3) = CCG.PCTCCGCode

inner join PbR2015.dbo.PbRCut FF
on FF.ProviderSpellNo = PBR.[ReportMasterID]
and EncounterTypeCode = 'IP'
and DatasetCode = 'ENCOUNTER'
and 
(
CardiacStrokeFlag = 'STR'
or
FF.HRGCode in ('AA22B','AA23B')
) 

left join
[PbR2014].[Tariff].[APCOPProcedure] HRG
on PbR.HRG_Code = HRG.HRGCode


  --where 
  --[PCT_Code] like '%STR'


order by 
HRG_Code,
[MonthNo]desc,
[POD_Code]