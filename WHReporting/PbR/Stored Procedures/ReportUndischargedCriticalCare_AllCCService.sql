﻿/*
--Author: N Scattergood	
--Date created: 29/05/2015
--Stored Procedure Built for SRSS Report on:
--Patients Undischarged Critical Care
--Returns spells where with a stay on Critical Care
--which were undischarged at the end of a given month
--(even where they have subsequently been discharged)
--requested by Simon Jenkins
*/


CREATE Procedure [PbR].[ReportUndischargedCriticalCare_AllCCService]
@ReportDate as Date 
--=  dateadd(day,-(datepart(day,cast(GETDATE() as date))-1),cast(GETDATE() as date))
as
select
Distinct
	-- DatasetCode = 'CC'
	--,WardStay.SourceUniqueID
	WardStay.CC_Service
	,WardStay.WardCode
	,WardStay.SourceSpellNo
	,S.FaciltyID as DistrictNumber
	,SpellCCGCode
	,'SpecCommFlag' = null
	,case when
	   CC_Service = 'BIC'
	   and
	   SpellCCGCode  in ('01W','01Y','02A','01M','02H','00W','01G','00T','00V','01D','01N','00Y')
      then 'Y'
      else 'N'
      end as 'BIC - GM Block'
      ,WardStay.CritCareStart
      ,WardStay.CritCareEnd
	,S.AdmissionDate
	,S.DischargeDate
	,S.DischargeMethod
	,LOSAtMonthEnd = datediff(day,S.AdmissionDate,DATEADD(day,-1,@ReportDate))
	,CurrentLOS = S.LengthOfSpell	
	,CritCareBedDay = WardStay.BedDays 
	     -- ,AdjustedCritCareBedDays = 
	     -- case 
	     -- when isnull(ECMO.CriticalCareDays,0) > WardStay.LOS then 0 
      --when WardStay.ServiceCode = 'CICU' then
      --      WardStay.LOS
      ---
      --      coalesce(
      --            (
      --            select
      --                  coalesce(
      --                        ECMO.CriticalCareDays
      --                        ,0
      --                  )
      --            from
      --                  dbo.ECMO
      --            where
      --                  ECMO.SourceSpellNo = PbR.ProviderSpellNo
      --            )
      --            ,0
      --      )

      --else WardStay.LOS
      ----end
      --,CritCareLevel.Level2SupportDays as 'Level2(ToDate)'
      --,CritCareLevel.Level3SupportDays as 'Level3(ToDate)'

      

from
	(
	
--Declare @ReportDate as Date = dateadd(day,-(datepart(day,cast(GETDATE() as date))-1),cast(GETDATE() as date)) 

	select
		 WS.SourceSpellNo
		 --,WS.WardCode
		 ,EntityCode as WardCode
		,XrefEntityCode as [CC_Service]
	
,WS.StartTime as CritCareStart
,WS.EndTime		as CritCareEnd
		,BedDays =

			SUM(
			DATEDIFF(day,cast(WS.StartTime as date),
			(CASE
			when WS.EndTime is null 
			then cast((dateadd(day,-1*((DATEPART(day,@ReportDate))),@ReportDate)) as DATE)
			when cast(WS.EndTime as DATE) <= cast((dateadd(day,-1*((DATEPART(day,@ReportDate))),@ReportDate)) as DATE)
			then WS.EndTime
			else cast((dateadd(day,-1*((DATEPART(day,@ReportDate))),@ReportDate)) as DATE)
			end)
			)
			)+1

			
	from
		WH.APC.WardStay WS

	inner join WH.PAS.ServicePoint
	on	ServicePoint.ServicePointCode = WS.WardCode
	and ServicePointType = 'Ward'
	and ArchiveFlag = 'N'

	inner join WH.dbo.EntityXref WardServiceMap
	on	 ServicePoint.ServicePointLocalCode = WardServiceMap.EntityCode
	and WardServiceMap.EntityTypeCode = 'WARD'
	
	left join WHREPORTING.APC.Spell S
	on S.SourceSpellNo = WS.SourceSpellNo


	where
	WS.StartTime < @ReportDate --i.e. were on CriticalCare sometime before month end
	and
	(
	S.DischargeDateTime > @ReportDate --i.e. were discharged from hospital after month end
	or 
	S.DischargeDateTime is null -- or still in hospital now
	)
	group by
		 WS.SourceSpellNo
		, XrefEntityCode  
		,WS.StartTime
,WS.EndTime
,EntityCode
		


	) WardStay



left join 
(
SELECT
      [SourceSpellNo]
      --,cast(CCP.CriticalCareStartDatetime as Date) CCP_StartDate
      --,cast(CCP.CriticalCareEndDatetime as Date) CCP_EndDate
      ,sum([HDU Days]) as Level2SupportDays
      ,sum([ICU Days]) as Level3SupportDays
  FROM [WHREPORTING].[APC].[CriticalCarePeriod] CCP
  
  Group by
   [SourceSpellNo]
      --   ,cast(CCP.CriticalCareStartDatetime as Date) 
      --,cast(CCP.CriticalCareEndDatetime as Date) 
   ) as CritCareLevel
   on CritCareLevel.SourceSpellNo = WardStay.SourceSpellNo
 --   and cast(WardStay.CritCareStart as Date) = CritCareLevel.CCP_StartDate
	--and cast(WardStay.CritCareEnd as Date) = CritCareLevel.CCP_EndDate
   
   left join WHREPORTING.APC.Spell S
   on S.SourceSpellNo = WardStay.SourceSpellNo 

WHERE
Case
when WardStay.[CC_Service] in ('MICU','PICU') 
and
(
CritCareLevel.Level2SupportDays > 0
or 
CritCareLevel.Level3SupportDays > 0
)
then 1 
when WardStay.[CC_Service] not in ('MICU','PICU') 
then 1
else 0 
end
= 1

--GROUP BY
--WardStay.CC_Service
--	,WardStay.SourceSpellNo
--	,S.FaciltyID
--	,case when
--	   CC_Service = 'BIC'
--	   and
--	   SpellCCGCode  in ('01W','01Y','02A','01M','02H','00W','01G','00T','00V','01D','01N','00Y')
--      then 'Y'
--      else 'N'
--      end 
--      ,S.AdmissionDate
--	,S.DischargeDate
--	,S.DischargeMethod
--	,datediff(day,S.AdmissionDate,DATEADD(day,-1,@ReportDate))
--	,S.LengthOfSpell	
--	, WardStay.BedDays 
--      ,CritCareLevel.Level2SupportDays 
--      ,CritCareLevel.Level3SupportDays 


order by
WardStay.[CC_Service],
WardStay.BedDays