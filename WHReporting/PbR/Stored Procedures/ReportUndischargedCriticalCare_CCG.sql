﻿/*
--Author: N Scattergood	
--Date created: 18/10/2013
--Stored Procedure Built for SRSS Report on:
--Patients Undischarged Critical Care
--this provides the CCG Paramete
*/

CREATE Procedure [PbR].[ReportUndischargedCriticalCare_CCG]

as
    ( select Distinct U.* from
(
(
select
distinct
coalesce(
case when spell.SpellCCGCode='N/A' 
then '01N' 
else spell.SpellCCGCode 
end,'01N') as SpellCCGCode,
coalesce(case when spell.SpellCCGCode='N/A' 
then '01N - NHS SOUTH MANCHESTER CCG'
else spell.SpellCCG
end,'Unknown') AS SpellCCG

from

APC.Spell spell 

inner join APC.CriticalCarePeriod ccperiod 
on ccperiod.SourceSpellNo = spell.SourceSpellNo

inner join Lorenzo.dbo.ExtractCriticalCarePeriodStay stay 
on stay.SourceCCPNo=ccperiod.SourceCCPNo

LEFT JOIN WHREPORTING.LK.Calendar cdis 
on cdis.TheDate = DischargeDate

								--left join [PbR2013].[dbo].[Transplant] T
								--on Spell.SourceSpellNo = T.SourceSpellNo 
								--and T.VisitTypeCode = 'TRANSPLANT'
								
								--left join [PbR2013].[dbo].[VAD] V
								--on Spell.SourceSpellNo = V.SourceSpellNo 
								
								--left join [PbR2013].dbo.ECMO E
								--on E.SourceSpellNo = spell.SourceSpellNo

where 
coalesce(DischargeDate,getdate()) > cast((dateadd(day,-1*(DATEPART(day,getdate())),getdate())) as DATE)
and 
admissionDatetime < cast((dateadd(day,-1*((DATEPART(day,getdate()))-1),getdate())) as DATE)
and 
SPONT_REFNO_CODE in ('ICA','CTCU','CT Transplant')

)


union all

(
select
distinct
coalesce(
case when spell.SpellCCGCode='N/A' 
then '01N' else spell.SpellCCGCode 
end,'01N') as SpellCCGCode,
coalesce(case when spell.SpellCCGCode='N/A' 
then '01N - NHS SOUTH MANCHESTER CCG'
else spell.SpellCCG
end,'Unknown') AS SpellCCG
from

APC.Spell spell 
inner join APC.WardStay ccperiod 
on ccperiod.SourceSpellNo = spell.SourceSpellNo

LEFT JOIN WHREPORTING.LK.Calendar cdis 
on cdis.TheDate=DischargeDate

								--left join [PbR2013].[dbo].[Transplant] T
								--on Spell.SourceSpellNo = T.SourceSpellNo 
								--and T.VisitTypeCode = 'TRANSPLANT'
								
								--left join [PbR2013].[dbo].[VAD] V
								--on Spell.SourceSpellNo = V.SourceSpellNo 
								
								--left join [PbR2013].dbo.ECMO E
								--on E.SourceSpellNo = spell.SourceSpellNo

where 
coalesce(DischargeDate,getdate())>cast((dateadd(day,-1*(DATEPART(day,getdate())),getdate())) as DATE)
and 
WardCode in('BIC','SCB')
--and SPONT_REFNO_CODE<>'ICA'
and 
admissionDatetime<cast((dateadd(day,-1*((DATEPART(day,getdate()))-1),getdate())) as DATE)
)) as U
)