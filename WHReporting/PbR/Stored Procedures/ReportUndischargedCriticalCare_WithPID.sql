﻿/*
--Author: N Scattergood	
--Date created: 05/11/2013
--Stored Procedure Built for SRSS Report on:
--Patients Undischarged Critical Care
--Returns spells where with a stay on Critical Care
--which were undischarged at the end of a given month
--(even where they have subsequently been discharged)
--adapted from a report John C originally wrote
--adaptation from ReportUndischargedCriticalCare for AICU CTCU and CT Transplant
--requested by Karen Holdship
*/

CREATE Procedure [PbR].[ReportUndischargedCriticalCare_WithPID]
@CritPar as nvarchar(50)
as
select
distinct
coalesce(
case when spell.SpellCCGCode='N/A' 
then '01N' 
else spell.SpellCCGCode 
end,'01N') as SpellCCGCode,
coalesce(spell.SpellCCG,'Unknown') AS SpellCCG,
--CriticalCareType
--=
--case
--when T.VisitTypeCode = 'TRANSPLANT'
--then 'Transplant'
--when V.SourceSpellNo is not null
--then 'VAD'
--when E.SourceSpellNo is not null
--then 'Respiratory ECMO'
--else 'Critical Care'
--end,
Spell.SourceSpellNo,
FaciltyID,
NHSNo,
spell.PracticeCode as GPPractice,
AdmissionMethod,
AdmissionDate,
DischargeDate,
LOSAsAtToday=DATEDIFF(day,admissiondatetime,coalesce(dischargedatetime,getdate())),
--LOSAsAtToday=sum(ccpstay),
stay.SPONT_REFNO_CODE,
CCBedDaysasatEndofMonth=
DATEDIFF(day,ccperiod.CriticalCareStartDatetime,
coalesce(ccperiod.CriticalCareEndDatetime,cast((dateadd(day,-1*(DATEPART(day,getdate())),getdate())) as DATE)
))
,CCBedDaysasatEndofMonthFinance=
DATEDIFF(day,cast(ccperiod.CriticalCareStartDatetime as date),
coalesce(cast(ccperiod.CriticalCareEndDatetime as date),cast((dateadd(day,-1*(DATEPART(day,getdate())),getdate())) as DATE)
))+1
,ccperiod.CriticalCareStartDatetime as WardStart,
ccperiod.CriticalCareEndDatetime as WardEnd,
CCBedDaysasatToday=
DATEDIFF(day,ccperiod.CriticalCareStartDatetime,
coalesce(ccperiod.CriticalCareEndDatetime,getdate())),
CCBedDaysasatTodayFinance=

DATEDIFF(day,cast(ccperiod.CriticalCareStartDatetime as date),
coalesce(cast(ccperiod.CriticalCareEndDatetime as date),cast(getdate()as date)))+1,

[Status]=case when ccperiod.CriticalCareEndDatetime is null then 'Still in Critical Care'
when DischargeDate is null then 'Inpatient'
else 'Discharged M'+right(cdis.FinancialMonthKey,2) end

,ReportDate = cast((dateadd(day,-1*(DATEPART(day,getdate())),getdate())) as DATE)

from

APC.Spell spell 

inner join APC.CriticalCarePeriod ccperiod 
on ccperiod.SourceSpellNo = spell.SourceSpellNo

inner join Lorenzo.dbo.ExtractCriticalCarePeriodStay stay 
on stay.SourceCCPNo=ccperiod.SourceCCPNo

LEFT JOIN WHREPORTING.LK.Calendar cdis 
on cdis.TheDate = DischargeDate

								--left join [PbR2013].[dbo].[Transplant] T
								--on Spell.SourceSpellNo = T.SourceSpellNo 
								--and T.VisitTypeCode = 'TRANSPLANT'
								
								--left join [PbR2013].[dbo].[VAD] V
								--on Spell.SourceSpellNo = V.SourceSpellNo 
								
								--left join [PbR2013].dbo.ECMO E
								--on E.SourceSpellNo = spell.SourceSpellNo

where 
coalesce(DischargeDate,getdate()) > cast((dateadd(day,-1*(DATEPART(day,getdate())),getdate())) as DATE)
and 
admissionDatetime < cast((dateadd(day,-1*((DATEPART(day,getdate()))-1),getdate())) as DATE)
and 
SPONT_REFNO_CODE in (SELECT Val from dbo.fn_String_To_Table(@CritPar,',',1))