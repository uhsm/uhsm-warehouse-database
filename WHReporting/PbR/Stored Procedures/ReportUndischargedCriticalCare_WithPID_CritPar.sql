﻿/*
--Author: N Scattergood	
--Date created: 05/11/2013
--Stored Procedure Built for SRSS Report on:
--Undischarged Critical Care With PID
--This Provides the Crit Care Wards
*/


CREATE Procedure [PbR].[ReportUndischargedCriticalCare_WithPID_CritPar]

  as
			  select distinct
				SPB.SPONT_REFNO_CODE as CritCareCode
				,SPB.SPONT_REFNO_NAME as CritCareDesc
		
				from WH.PAS.ServicePointBase SPB
				where
				SPB.[SPTYP_REFNO_MAIN_CODE] = 'WARD'
				and
				SPB.SPONT_REFNO_CODE in ('ICA','CTCU','CT Transplant')
				and
				ARCHV_FLAG = 'N'
				
				order by
				SPB.SPONT_REFNO_NAME