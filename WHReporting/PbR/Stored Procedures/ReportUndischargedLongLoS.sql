﻿/*
--Author: N Scattergood	
--Date created: 07/09/2013
--Stored Procedure Built for SRSS Report on:
--Patients Undischarged Long LOS
--Returns spells which were undischarged at the end of a given month
--(even where they have subsequently been discharged)
--adapted from a report Jennifer P originally wrote
*/

CREATE Procedure [PbR].[ReportUndischargedLongLoS]

@EndDate Date
,@CCGPar as nvarchar(1000) 
as 
select 
	Spell.SpellCCGCode as PurchaserCode,
	Spell.SpellCCG as Purchaser,
	AdmissionType = 
		Case	when Spell.InpatientStayCode = 'D' then 'Day Case' 
				when Spell.AdmissionType in ('Emergency','Non-Elective','Maternity') then 'Non Elective'
				else 'Elective' end,
	Spell.AdmissionMethodCode,
	Spell.NHSNo,
	Spell.SourceSpellNo,
	Spell.FaciltyID,
	Patient.DateOfBirth,
	Spell.AdmissiontWardCode,
	Spell.GPCode,
	Spell.PracticeCode,
	Spell.AdmissionDate,
	Spell.DischargeDate,
	Spell.[AdmissionSpecialtyCode(Function)],
	Spell.LengthOfSpell,
	DATEDIFF(day,Spell.AdmissionDate,@EndDate) as 'LoSAtMonthEnd',
	case 
	when CysticFibrosisRegister.DistrictNo is null 
	then null 
	else 'CF Patient' 
	end as CFPatient
	
from WHReporting.APC.Spell 

	left join WHREPORTING.APC.Patient 
	on Spell.EncounterRecno = Patient.EncounterRecno
	
	left join PbR2014.dbo.CysticFibrosisRegister 
	on Spell.FaciltyID = 'RM2'+cast(CysticFibrosisRegister.DistrictNo as nvarchar(20)) 
	--left join PbR2013.dbo.CysticFibrosisRegister on RIGHT(len(Spell.FacilityID)-3,Spell.FaciltyID)=CysticFibrosisRegister.DistrictNo
	--inner join WHReporting.APC.Episode on Spell.SourceSpellNo = Episode.SourceSpellNo
	--inner join WH.APC.Encounter on Episode.EncounterRecno = Encounter.EncounterRecno

where 
Spell.AdmissionDate < @EndDate
and
(Spell.DischargeDate is null or spell.DischargeDate > @EndDate)
and 
DATEDIFF(day,Spell.AdmissionDate,@EndDate) > 30
and 
PurchaserCode not in ('TDH00','YDD82')
and
Spell.SpellCCGCode in (SELECT Val from dbo.fn_String_To_Table(@CCGPar,',',1))

order by 

SpellCCGCode, 
DATEDIFF(day,Spell.AdmissionDate,@EndDate)