﻿/*
--Author: N Scattergood	
--Date created: 07/09/2013
--Stored Procedure Built for SRSS Report on:
--Patients Undischarged Long LOS
--This Provides the CCG Parameter
*/

Create Procedure [PbR].[ReportUndischargedLongLoS_CCG]

@EndDate Date
as 
select 
Distinct
	Spell.SpellCCGCode as CCGPARCode,
	Spell.SpellCCG as CCGPARDesc
	
	
from WHReporting.APC.Spell 

	
where 
Spell.AdmissionDate < @EndDate
and
(Spell.DischargeDate is null or spell.DischargeDate > @EndDate)
and 
DATEDIFF(day,Spell.AdmissionDate,@EndDate) > 30
and 
PurchaserCode not in ('TDH00','YDD82')

order by 

SpellCCGCode