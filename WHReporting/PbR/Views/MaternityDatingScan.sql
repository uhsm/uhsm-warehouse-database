﻿--select top 100 * from dbo.PbR where EncounterTypeCode = 'OP'
CREATE view [PbR].[MaternityDatingScan] as 
/*
--Author: K Oakden
--Date created: 15/07/2013
--Required by finance to identify MATANT POD codes
--Switched to 1415 on 01/5/14 by NScatter
--Switched to 1516 on 06/5/15 by NScatter
--Switched to 1617 on 03/5/16 by NScatter
*/
select 
	right(Cal.FinancialMonthKey,2) as 'Fin Month'
	,PbR.FinalSLACode
	,PbR.EncounterTypeCode
	,PbR.DatasetCode
	,PbR.NHSNumber
	,PbR.DistrictNo
	,Src.ReferralSourceUniqueID  as ReferralPathwayID
	,PbR.TreatmentFunctionCode
	,PbR.HRGCode
	,PbR.PODCode
	,PbR.EncounterStartDate
	,PbR.SourceUniqueID
	,PbR.Cases
	,PbR.ClinicCode
	,Sess.SessionCode
	,PbR.ConsultantCode
	,PbR.PbRExcluded
	,PbR.ExclusionReason
	,PbR.PrimaryProcedureCode
	,PbR.DominantProcedureCode
	,PbR.ServiceCode
	


from PbR2016.dbo.PbR PbR
left join WH.OP.Encounter Src
	on Src.SourceUniqueID = PbR.SourceUniqueID
left join WH.OP.[Session] Sess
	on Sess.SessionUniqueID = src.SessionUniqueID
left join WHREPORTING.LK.Calendar Cal
	on PbR.EncounterStartDate = Cal.TheDate
	
where EncounterTypeCode = 'OP'

and 
	(
	PbR.ClinicCode in (
		'MID1DSCAM'
		,'MID1DSCPM'
		,'MID2DSCAM'
		,'MID2DSCPM'
		,'MID3DSCAM'
		,'MID3DSCPM-AH'
		,'MID4DSCAM'
		,'MID4DSCPM'
		,'MID5DSCAM'
		,'MIDDSCAN'
		--,'MLC4ACOM'
		--,'MLC4COM'
		,'MW4DSCAM'
		,'MW5DSCAM'
		,'MW4DSCANTRA'	-- Added 13/08/13 -- Amended 15/08/13 (JS)
		,'MW5DSCANTRA'	-- Added 13/08/13 -- Amended 15/08/13 (JS)
		,'MW4DSCAN'		-- Added 13/08/13
		,'MW5DSCAN'		-- Added 13/08/13
		)
	or
		(
		PbR.ClinicCode in ('MLC4ACOM','MLC4COM')
		and 
		Sess.SessionCode in('MLC4COM-SCAN', 'MLC4ACOM-SCAN')
		)
	)