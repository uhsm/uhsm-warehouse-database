﻿CREATE VIEW [PbR].[PBRFlexAndFreeze]

as


SELECT 
	EncounterRecno  -- unique from PbR2013 (identity field)
	,DatasetCode  
	,SourceUniqueID
	,SourceEncounterRecno --from source data eg WH
	,InterfaceCode
	,EncounterTypeCode
	,EncounterStartDate
	,EncounterEndDate
	,Cases
	,Age
	--,Excluded
	--,ExclusionType
	,ExcessBedDays
	,ExcessBedDaysFinal = 
		case 
			when ExcessBedDays - CriticalCareDays < 0
			then 0
			else ExcessBedDays - CriticalCareDays
		end
	--,NationalTariff
	--,SpecialistServiceSupplement
	--,ChildrensServicesSupplement
	--,ShortStaySupplement
	--,LongStaySupplement
	,TrimPoint
	--,LongStayTariff
	--,ShortStayFlag
	--,ChildrenFlag
	--,TotalTariff
	--,LocalTariff
	--,ActualTariff
	--,Mandatory
	,AdminCategoryCode
	,AdmissionDate
	,AdmissionMethodCode
	,AppointmentDate
	,AttendanceID
	,AttendanceOutcomeCode
	--,CarerSupportIndicator
	--,CasenoteNo
	--,CategoryCode
	,ClinicCode
	-- ,ClinicFunctionCode
	,CommissionerCode
	,CommissionerCodeCCG
	--,CommissionerReferenceNo
	--,CommissioningSerialNo
	,ConsultantCode
	--,DNACode
	,DateOfBirth
	,DateOfDeath
	-- ,DecidedToAdmitDate
	,DischargeDate
	,DischargeDestinationCode
	,DischargeMethodCode
	-- ,DischargeReadyDate
	--,DistrictNoOrganisationCode
	,DistrictNo
	--,DurationOfElectiveWait
	--,EarliestReasonableOfferDate
	--,EndLocationClassCode
	--,EndLocationTypeCode
	-- ,EndSiteCode
	--,EndWardTypeCode
	,EpisodeCount
	,EpisodeEndDate
	,EpisodeNo
	,EpisodeStartDate
	-- ,EpisodicAdminCategoryCode
	--,EpisodicLegalStatusClassificationCode
	--,EpisodicGPPracticeCode
	--,EthnicCategoryCode
	,FirstAttendanceCode
	--,FirstRegularDayNightAdmissionFlag
	,HRGCode
	--,HRGVersionCode
	-- ,ICDDiagnosisSchemeCode
	--,LastDNAOrCancelledDate
	,LastEpisodeInSpellIndicator
	-- ,LegalStatusClassificationCode
	-- ,LocationClassCode
	--,LocationTypeCode
	,ManagementIntentionCode
	--,MaritalStatusCode
	,MedicalStaffTypeCode
	--,NHSNumberStatusId
	,NHSNumber
	--,NHSServiceAgreementLineNo
	--,NeonatalLevelOfCareCode
	--,OPCSProcedureSchemeCode
	-- ,OperationStatusCode
	--,PASDGVPCode
	-- ,PASHRGCode
	-- ,PCTCode
	-- ,CCGCode
	--,PathwayIdIssuerCode
	-- ,PathwayId
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientClassificationCode
	,PatientForename
	,PatientSurname
	,PatientTitle
	,Postcode
	,PrimaryDiagnosisCode
	,PrimaryProcedureCode
	,PrimaryProcedureDate
	,DominantProcedureCode
	-- ,PriorityTypeCode
	,ProviderCode
	-- ,ProviderReferenceNo
	,ProviderSpellNo
	--,PsychiatricPatientStatusCode
	-- ,RTTEndDate
	--,RTTStartDate
	--,RTTStatusCode
	,ReferralRequestReceivedDate
	,ReferrerCode
	,ReferrerOrganisationCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	--,ServiceTypeRequestedCode
	,SexCode
	-- ,SiteCode
	,SourceOfAdmissionCode
	,SourceOfReferralCode
	--,SpecialistServiceCode
	,SpecialtyCode
	,SpellLOS
	--,StartLocationClassCode
	-- ,StartLocationTypeCode
	--,StartSiteCode
	-- ,StartWardTypeCode
	,TreatmentFunctionCode
	--,TrustNo
	--,UniqueBookingReferenceNo
	-- ,VersionCode
	,AEAttendanceDisposal
	,ArrivalDate
	-- ,InvestigationCodeList
	--,SLACategoryCode
	-- ,ServiceLineCode
	-- ,ServiceLineBaseCode
	,ServiceCode
	-- ,SLACode
	,FinalSLACode

	-- ,DepartureCode
	-- ,LocalConsultantCode
	-- ,EarlySupportedDischarge
	-- ,BestPracticeTariff
	,BestPracticeCode
	--,BestPractice
	-- ,PbRMandated
	--,SpellProgramBudgetCode
	--,AllocatedTreatmentFunctionCode
	-- ,ContractKey
	,BestPracticeSectionCode
	,PODCode
	--,SourcePatientNo
	,PbRExcluded
	,CDSSLACode = 
		case
			when PbRExcluded = 1 
			then ExclusionType
			else FinalSLACode
		end
	,ModelTypeCode
	,CriticalCareDays
	,CardiacCategory
	--For Cardiac activity. payment allocated to cardiac department 
	--rather than admitting department
	,TreatmentFunctionCodeFinal = 
		Case 
			when CardiacCategory in (
				'CABG'
				,'Cardiac Surgery Other'
				,'TAVI'
				,'Valve'
				)
				then 170
			when CardiacCategory in (
				'Non CT Other'
				,'Thoracic Surgery'
				)
				then 173
			when CardiacCategory = 'Transplant'
				then 174
			when CardiacCategory in (
				'Ablat'
				,'Angio'
				,'Defib'
				,'EPS'
				,'Gen Card'
				,'Loop'
				,'PACE DUAL'
				,'PACE SING'
				,'PTCA'
				,'Trans ECG'
				)
				then 320
			else TreatmentFunctionCode
		end
FROM PbR2013.dbo.PbRCut