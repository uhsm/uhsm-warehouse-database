﻿CREATE view [PbR].[TraffordNonCommissionedProcedures] as
/*
--Author: K Oakden
--Date created: 17/07/2013
--Required by finance for audit of Non-Commissioned Procedures 
*/
select 
	Traff.Reference
	,PbR.EncounterTypeCode
	,PbR.PbRExcluded
	,PbR.CommissionerCodeCCG
	,PbR.FinalSLACode
	,PbR.DistrictNo
	,PbR.DominantProcedureCode
	,PbR.PrimaryDiagnosisCode
	,PbR.PrimaryProcedureCode
from PbR2013.dbo.PbRCut PbR

left join PbR2013.dbo.TraffordNonCommissionedProcedures Traff
	on Traff.OPCSCode = PbR.DominantProcedureCode
	
where PbR.DominantProcedureCode is not null
and PbR.CommissionerCodeCCG = '02A'
and Traff.Reference is not null
and (
	(left(isnull(PbR.PrimaryDiagnosisCode, 'xx'), 1) <> 'C')
	or
	(isnull(PbR.PrimaryDiagnosisCode, 'xx') like 'D0[0123456789][0123456789]'))

and not(PbR.DominantProcedureCode = 'S069' and PbR.PrimaryDiagnosisCode = 'N63X')
and not(PbR.DominantProcedureCode = 'D151' and PbR.PrimaryDiagnosisCode = 'H901')
and not(PbR.DominantProcedureCode = 'Q088' and PbR.PrimaryDiagnosisCode = 'N809')
and not(PbR.DominantProcedureCode = 'Q088' and PbR.PrimaryDiagnosisCode = 'N811')
and not(PbR.DominantProcedureCode = 'Q089' and PbR.PrimaryDiagnosisCode = 'N319')
and not(PbR.DominantProcedureCode = 'Q303' and PbR.PrimaryDiagnosisCode = 'N47X')

--order by PrimaryDiagnosisCode