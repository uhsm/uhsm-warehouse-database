﻿/******************************************************************************
 Description: Extracts AE data for use by Finance in 
              the SLR/PLCS project.
  
 Parameters:  @ActivityStartDate - Start date for data extract
              @ActivityEndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author			Change
 12/11/14 Jubeda Begum		Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportAE]
 @ActivityStartDate DATETIME
,@ActivityEndDate DATETIME
AS

SET NOCOUNT ON;

   
SELECT     AE.EncounterRecno, AE.SourceUniqueID, AE.DistrictNo, AE.NHSNumber, AE.LocalPatientID, AE.PatientTitle, AE.PatientForename, AE.PatientSurname, 
                      AE.PatientAddress1, AE.PatientAddress2, AE.PatientAddress3, AE.PatientAddress4, AE.Postcode, AE.DateOfBirth, AE.DateOfDeath, AE.SexCode, 
                      AE.RegisteredGpCode, AE.RegisteredGpName, AE.RegisteredGpPracticeCode, AE.RegisteredGpPracticeName, AE.RegisteredGpPracticePostcode, 
                      AE.Consultant, AE.AttendanceNumber,AE.ArrivalModeCode ,AM.ArrivalMode, AE.AttendanceCategoryCode, AC.AttendanceCategory, AE.AttendanceDisposalCode, 
                      AD.AttendanceDisposal, AE.IncidentLocationTypeCode,AE.SourceOfReferralCode,SR.label,AE.ArrivalDate,AE.ArrivalTime, AE.AgeOnArrival, AE.InitialAssessmentTime, 
                      AE.SeenForTreatmentTime, AE.AttendanceConclusionTime, AE.DepartureTime, 
                      AE.CommissionerCodeCCG, AE.SeenByCode, AE.SeenBy, AE.InvestigationCodeFirst, AE.InvestigationCodeSecond, AE.DiagnosisCodeFirst, 
                      AE.DiagnosisCodeSecond, AE.CascadeDiagnosisCode, AE.TreatmentCodeFirst, AE.TreatmentCodeSecond, AE.Created, AE.InterfaceCode, AE.PCTCode, 
                      AE.CCGCode, AE.ResidencePCTCode, AE.ResidenceCCGCode, AE.SourceOfCCGCode, AE.TriageCategoryCode, CASE WHEN TriageCategoryCode IN ('O',
                       'R', 'Y') THEN 'Major' ELSE 'Minor' END AS Triage, AE.PresentingProblem, AE.ToXrayTime, AE.FromXrayTime, AE.ToSpecialtyTime, 
                      AE.SeenBySpecialtyTime, AE.EthnicCategoryCode, AE.ReferredToSpecialtyCode, AE.DecisionToAdmitTime, AE.DischargeDestinationCode, 
                      AE.RegisteredTime, AE.Attends, AE.WhereSeen, AE.EncounterDurationMinutes, AE.BreachReason, AE.AdmitToWard, AE.AdviceTime, AE.StandbyTime, 
                      AE.TrolleyDurationMinutes, AE.ManchesterTriageCode, AE.FFTextStatus, AE.ReferGP
FROM         WH.AE.Encounter AS AE LEFT OUTER JOIN
                      WH.AE.ArrivalMode AS AM ON REPLACE(AE.ArrivalModeCode, ' ', '') = AM.ArrivalModeCode LEFT OUTER JOIN
                      WH.AE.AttendanceCategory AS AC ON AE.AttendanceCategoryCode = AC.AttendanceCategoryCode LEFT OUTER JOIN
                      WH.AE.AttendanceDisposal AS AD ON AE.AttendanceDisposalCode = AD.AttendanceDisposalCode LEFT OUTER JOIN
                      WH.[CASCADE].AESource AS SR ON AE.SourceOfReferralCode = SR.code
WHERE     (AE.AttendanceCategoryCode IN ('1', '2'))

AND AE.ArrivalDate BETWEEN @ActivityStartDate AND @ActivityEndDate

;