﻿/******************************************************************************
 Description: Extracts COMAQPodiatry data for use by Finance in 
              the SLR/PLCS project.
  
 Parameters:  @ActivityStartDate - Start date for data extract
              @ActivityEndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author			Change
 12/11/14 Jubeda Begum		Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportCOMAQPPodiatry]
 @ActivityStartDate DATETIME
,@ActivityEndDate DATETIME
AS

SET NOCOUNT ON;

select distinct
Schedules.PatientID
,cast(Schedules.ScheduleID as varchar)+cast(dp.code as varchar) as SourceUniqueID
,Schedules.ScheduleID
,Schedules.NHSnumber AS NHSNumber
,spat.EpisodicGPCode AS EpisodicGpCode, 
 spat.EpisodicGPPracticeCode AS EpisodicGpPracticeCode
 ,spat.EpisodicCCGCode
 ,spat.EpisodicCCGName
,Referrals.ReferralRequestReceivedDateTime as ReferralDate
,Schedules.AppointmentDateTime AS AppointmentDate
,case when NewAppointments.MinSchedule is null then 'Follow-Up' else 'New' end as ApptType
,Referrals.SourceOfReferral
,Referrals.SourceOfReferralDescription
,spat.PatientDateOfBirth as DateOfBirth
,COALESCE(Schedules.ProfessionalCarer,SeenByProfessionalCarer) AS ProfessionalCarer
,case when dp.CODE='255611003' then 'A'
when dp.CODE='6736007' then 'B1'
when dp.CODE='CC00469' then 'B2'
when dp.CODE='255604002' then 'C'
when dp.CODE='CC00507' then 'D'
when dp.CODE='371923003' then 'D1'
when dp.CODE='371924009' then 'D2' 
when dp.CODE='225399009' then 'E'
when dp.CODE in ('308451001','225363004') then 'Non-AQP' 
end as PathwayType
,case when dp.CODE='255611003' then 'Wound Care Pathway'
when dp.CODE='6736007' then 'Nail Surgery 1 Toe'
when dp.CODE='CC00469' then 'Nail Surgery 2 Toe'
when dp.CODE='255604002' then 'Routine Consultation and Treatment'
when dp.CODE='CC00507' then 'Biomechanical (all adults and children without concurrent co-morbidities)'
when dp.CODE='371923003' then 'Simple off the shelf standard insoles'
when dp.CODE='371924009' then 'Higher cost standard insoles' 
when dp.CODE='225399009' then 'Pain Assessment'
when dp.CODE in ('308451001','225363004') then 'Non-AQP Appointment' 
end as PathwayDescription
--,Spec.[Description] as SpecialtyDesc
--,isnull(Div.[Description],'Unknown') as DivDesc
,ModifiedDate=Schedules.ModifiedDate
--Into WHReporting.Community.AQPPodiatry

from WHReporting.com.Schedules Schedules
INNER JOIN WHREPORTING.COM.SchedulesPatient spat
on spat.ScheduleID=Schedules.ScheduleID
inner join WHReporting.com.Patients Patients
on Schedules.PatientID=Patients.PatientID
inner join WHReporting.com.Referrals Referrals
on Referrals.ReferralID=Schedules.ReferralID
left join [uhsm-is1].community.pas.DIAGNOSIS_PROCEDURES_VE01 dp
on Schedules.ScheduleID= dp.sorce_refno
left join whreporting.COM.lkSourceOfReferral SLookup on SLookup.National_code=Referrals.SourceOfReferral   

left join
				(select cast(OPappointments2.referralid as varchar)+cast(min(OPAppointments2.AppointmentDateTime) as varchar) as MinSchedule 
				from COM.SCHEDULES OPAppointments2
				inner join COM.REFERRALS Referrals2
				on Referrals2.ReferralID=OPAppointments2.ReferralID
				left join [uhsm-is1].community.pas.DIAGNOSIS_PROCEDURES_VE01 dp2
				on OPAppointments2.scheduleid = dp2.sorce_refno
				where dp2.CODE in ('255611003','6736007','CC00469','255604002','371923003','371924009','308451001','CC00507','225399009' ,'225363004')			
				and CCSXT_CODE = 'CCRFR'
				group by OPappointments2.referralid,dp2.CODE) as NewAppointments
				on NewAppointments.MinSchedule=cast(schedules.ReferralID as varchar)+cast(AppointmentDateTime as varchar)



            
where 
dp.CODE in ('255611003','6736007','CC00469','255604002','371923003','371924009','308451001','CC00507','225399009','225363004')
and Schedules.AttendedOrDidNotAttendCode =5
and CCSXT_CODE = 'CCRFR'
and Schedules.AppointmentDate BETWEEN @ActivityStartDate AND @ActivityEndDate
--and Schedules.MODIF_DTTM>(SELECT MAX(ModifiedDate) FROM Community.AQPPodiatry)
and Schedules.ActivityTypeMain='Outpatient Schedule'
and DP.ARCHV_FLAG = 'N'

--and case when NewAppointments.MinSchedule is null then 'Follow-Up' else 'New' end='New'
      
;