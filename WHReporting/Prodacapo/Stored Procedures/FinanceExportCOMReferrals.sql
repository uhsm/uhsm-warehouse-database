﻿/******************************************************************************
 Description: Extracts COMReferrals data for use by Finance in 
              the SLR/PLCS project.
  
 
 Modification History --------------------------------------------------------

 Date     Author			Change
 12/11/14 Jubeda Begum		Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportCOMReferrals]
AS

SET NOCOUNT ON;

SELECT    
[PatientID]
      ,[NHSNumber]
      ,[FacilityID]
      ,[ReferralID]
      ,[ReferralRequestReceivedDateTime]
      ,[ReferralRequestReceivedDate]
      ,[ReferralRequestReceivedTime]
      ,[CodeOfCommissioner]
      ,[CIDServiceTypeReferredTo]
      ,[ReferringOrganisationCode]
      ,[ReferringCareProfessionalStaffGroup]
      ,[Priority]
      ,[ReferralClosureDate]
      ,[ReferralClosureReason]
      ,[DischargeDate]
      ,[ReceivingProfCarer]
      ,[ReferralCreatedDate]
      ,[ReferredToDivision]
      ,[ReferredToDirectorate]
      ,[ReferringOrganisationName]
      ,[ReferrerCode]
      ,[ReferrerName]
      ,[ReferralCreatedBy]
      ,[Authorised]
      ,[ReferralStatus]
      ,[EpisodicGPCode]
      ,[EpisodicGPName]
      ,[EpisodicGPPracticeCode]
      ,[EpisodicGPPracticeName]
      ,[ReferredToSpecialtyCode]
      ,[ReferredToSpecialty]
      ,[SourceOfReferral]
      ,[SourceOfReferralDescription]
      ,[EpisodicCCGCode]
      ,[EpisodicCCGName]
      ,[ModifiedDate]
      ,[PatientPostCode]
      ,[PatientForename]
      ,[PatientSurname]
      ,[PatientAge]
      ,[Sex]
      ,[Title]
      ,[DateOfBirth]
      ,[DateOfDeath]
      ,[AddressLine1]
      ,[AddressLine2]
      ,[AddressLine3]
      ,[AddressLine4]
      ,[ActivityInLastYear]
      ,[AgeOfReferralCode]
FROM WHReporting.COM.Referrals

      
;