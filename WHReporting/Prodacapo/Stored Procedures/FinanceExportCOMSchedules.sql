﻿/******************************************************************************
 Description: Extracts COMSchedules data for use by Finance in 
              the SLR/PLCS project.
  
 Parameters:  @ActivityStartDate - Start date for data extract
              @ActivityEndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author			Change
 12/11/14 Jubeda Begum		Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportCOMSchedules]
 @ActivityStartDate DATETIME
,@ActivityEndDate DATETIME
AS

SET NOCOUNT ON;

SELECT    
[NHSnumber]
      ,[PatientID]
      ,[FacilityID]
      ,[ReferralID]
      ,[ScheduleID]
      ,[Age]
      ,[AppointmentDate]
      ,[AppointmentTime]
      ,[AppointmentDateTime]
      ,[AppointmentDuration]
      ,[AppointmentType]
      ,[CareContactSubjectCode]
      ,[ConsultantMediumUsedNationalCode]
      ,[ActivityLocationTypeCode]
      ,[SiteCodeOfTreatment]
      ,[AttendedOrDidNotAttendCode]
      ,[AttendNationalCode]
      ,[Outcome]
      ,[CareProfessionalStaffGroupCommunityCareNationalCode]
      ,[EarliestReasonableOfferDate]
      ,[CancellationDate]
      ,[CancellationReason]
      ,[EarliestClinicallyAppropriateDate]
      ,[ReplacementAppointmentBookedDateCommunityCare]
      ,[ReplacementAppointmentDateOfferedCommunityCare]
      ,[ActivityType]
      ,[GroupTherapyIndicatorCommunityCare]
      ,[ProfessionalCarer]
      ,[SeenByProfessionalCarer]
      ,[OtherCarerPresent]
      ,[AppointmentSpecialty]
      ,[AppointmentSpecialtyDivision]
      ,[AppointmentSpecialtyDirectorate]
      ,[AppointmentArrivalDateTime]
      ,[AppointmentSeenDateTime]
      ,[AppointmentDepartedDateTime]
      ,[AppointmentCreatedBy]
      ,[AppointmentCreatedDate]
      ,[ClinicAttendanceStatus]
      ,[AppointmentOutcome]
      ,[ClinicDescription]
      ,[ClinicCode]
      ,[ActivityTypeMain]
      ,[ModifiedDate]
FROM WHReporting.COM.Schedules
WHERE  AppointmentDate BETWEEN @ActivityStartDate AND @ActivityEndDate
      
;