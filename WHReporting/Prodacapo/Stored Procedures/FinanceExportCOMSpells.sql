﻿/******************************************************************************
 Description: Extracts COMSpells data for use by Finance in 
              the SLR/PLCS project.

 
 Modification History --------------------------------------------------------

 Date     Author			Change
 12/11/14 Jubeda Begum		Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportCOMSpells]

AS

SET NOCOUNT ON;

SELECT    
[NHSNumber]
      ,[FacilityID]
      ,[PatientID]
      ,[AgeOnAdmission]
      ,[ProviderEncounterID]
      ,[ReferralID]
      ,[Specialty]
      ,[AdmissionMethodCode]
      ,[WardName]
      ,[AdmissionsSource]
      ,[DischargeDestination]
      ,[DischargeMethod]
      ,[AdmissionDate]
      ,[DischargeDate]
      ,[SpellLengthOfStay]
      ,[SpellCreateDate]
      ,[SpellCreatedBy]
      ,[EpisodicGPCode]
      ,[EpisodicGPName]
      ,[EpisodicGPPracticeCode]
      ,[EpisodicGPPracticeName]
      ,[EpisodicCCGCode]
      ,[EpisodicCCGName]
      ,[Postcode]
FROM WHReporting.COM.Spells

      
;