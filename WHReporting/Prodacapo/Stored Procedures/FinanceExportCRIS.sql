﻿/******************************************************************************
 Description: Extracts radiology data from CRIS for use by Finance in 
              the SLR/PLCS project.
  
 Parameters:  @ActivityStartDate - Start date for data extract
              @ActivityEndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date        Author			Change
 19/02/2016  Gordon Fenton	Created
 02/06/2016  Tim Dean		Re-coded correctly
******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportCRIS]
 @ActivityStartDate DATETIME
,@ActivityEndDate DATETIME
AS

SET NOCOUNT ON;

SELECT Patient.PatientID
	,HospitalID = COALESCE(Patient.HospitalID, 'N/A')
	,Patient.Surname
	,Patient.Forenames
	,Patient.NhsNumber
	,Patient.DateOfBirth
	,Patient.Sex
	,[Events].EventKey
	,[Events].RequestDate
	,[Events].DateBooked
	,[Events].EventDate
	,[Events].PatientGroupCode
	,[Events].PatientGroup
	,[Events].BookingModeCode
	,[Events].BookingMode
	,[Events].EventUrgencyCode
	,[Events].EventUrgency
	,[Events].SpecialtyCode
	,[Events].SpecialtyName
	,Exams.ExamsKey
	,Exams.ExamDate
	,Exams.ExamStartTime
	,ExamDateTime = CAST(Exams.ExamDate AS DATETIME) + CAST(Exams.ExamStartTime AS TIME)
	,Exams.ExamCode
	,Exams.ExamName
	,Exams.NoOfProcedures
	,Exams.ModalityCode
	,Exams.Modality
	,Exams.StatusTypeCode
	,Exams.StatusTypeGroup
	,Reports.ReportsKey
	,Reports.ReportedDate
	,Reports.ReportedTime
	,ReportedDateTime = CAST(Reports.ReportedDate AS DATETIME) + CAST(Reports.ReportedTime AS TIME)
	,Orders.OrderID
	,Orders.EpisodeNumber
	,OrderCreationDate = Orders.CreationDate
	,Orders.ReferrerCode
	,OrderingLocation = Wards.WardCode
	,Rooms.RoomCode
FROM DW_REPORTING.CRIS.Exams AS Exams
LEFT JOIN DW_REPORTING.CRIS.[Events] AS [Events] ON (Exams.EventKey = [Events].EventKey)
LEFT JOIN DW_REPORTING.CRIS.Reports AS Reports ON (
		Exams.ExamsKey = Reports.ExamsKey
		AND Reports.ReportTypeCode = 'S'
		AND Reports.FoetusNumber < 2 -- Used to remove duplicate reports for same examination
		)
LEFT JOIN DW_REPORTING.CRIS.Patient AS Patient ON ([Events].PatientKey = Patient.PatientKey)
LEFT JOIN DW_REPORTING.CRIS.Rooms AS Rooms ON (Exams.RoomsKey = Rooms.RoomsKey)
LEFT JOIN DW_REPORTING.CRIS.Orders AS Orders ON (Exams.ExamsKey = Orders.ExamsKey)
LEFT JOIN DW_REPORTING.CRIS.Wards ON (Orders.WardKey = Wards.WardKey)
WHERE Exams.ExamDate BETWEEN @ActivityStartDate
		AND @ActivityEndDate;

/*
Select distinct 
	/*Patients*/
	 Patient.PatientID                    
	,COALESCE(Patient.HospitalID,'N/A') as HospitalID
	,Patient.Surname
	,Patient.Forenames
	,Patient.NhsNumber
	,Patient.DateOfBirth
	,Patient.Sex
	
	/*Events*/
	,[Events].EventKey
	,[Events].RequestDate
	,[Events].DateBooked
	,[Events].EventDate
	,[Events].PatientGroupCode
	,[Events].PatientGroup
	,[Events].BookingModeCode
	,[Events].BookingMode
	,[Events].EventUrgencyCode
	,[Events].EventUrgency
	,[Events].SpecialtyCode
	,[Events].SpecialtyName

	/*Exams*/
	,Exams.ExamsKey
	,Exams.ExamDate
	,Exams.ExamStartTime														
    ,CAST(Exams.ExamDate as DATETIME) + CAST(Exams.ExamStartTime  as TIME)        as ExamDateTime
	,Exams.ExamCode
	,Exams.ExamName
	,Exams.NoOfProcedures
	,Exams.ModalityCode
	,Exams.Modality
	,Exams.StatusTypeCode
	,Exams.StatusTypeGroup
	
	/*Reports*/
    ,Reports.ReportsKey
	,Reports.ReportedDate
	,Reports.ReportedTime
	,CAST(Reports.ReportedDate as DATETIME) + CAST(Reports.ReportedTime  as TIME)        as ReportedDateTime

	/*Orders*/
	,Orders.OrderID
	,Orders.EpisodeNumber
	,Orders.CreationDate                                                                 as OrderCreationDate
	,Orders.ReferrerCode	

	/*Activity*/
	,Activity.OrderingLocation

    /*Rooms*/
    ,Rooms.RoomCode
FROM 
    /*Patients*/
    DW_REPORTING.CRIS.Patient AS Patient
    /*Events*/     
    LEFT JOIN DW_REPORTING.CRIS.[Events]  as [Events] ON Patient.PatientKey = [Events].PatientKey
    
    /*Exams*/
    LEFT JOIN DW_REPORTING.CRIS.Exams  AS Exams ON [Events].EventKey = Exams.EventKey

	/*Reports*/
	LEFT JOIN DW_REPORTING.CRIS.Reports as Reports on Exams.ExamsKey = Reports.ExamsKey

	/*Orders*/
	LEFT JOIN DW_REPORTING.CRIS.Orders AS Orders ON Exams.ExamsKey = Orders.ExamsKey

	/*Activity*/
	LEFT JOIN DW_REPORTING.CRIS.Activity AS Activity ON Exams.ExamsKey = Activity.ExamsKey
	
	/*Rooms*/
	LEFT JOIN [DW_REPORTING].[CRIS].[Rooms] AS Rooms on Rooms.RoomsKey = Exams.RoomsKey

Where Exams.ExamDate BETWEEN @ActivityStartDate AND @ActivityEndDate;
*/