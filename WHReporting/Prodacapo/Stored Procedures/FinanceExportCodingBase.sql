﻿/******************************************************************************
 Description: Extracts CodingBase data for use by Finance in 
              the SLR/PLCS project.
 
 Modification History --------------------------------------------------------

 Date     Author			Change
 12/11/14 Jubeda Begum		Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportCodingBase]

AS

SET NOCOUNT ON;

SELECT [ODPCD_REFNO]
      ,[CODE]
      ,[CCSXT_CODE]
      ,[DESCRIPTION]
      ,[SUPL_CODE]
      ,[SUPL_CCSXT_CODE]
      ,[PARNT_REFNO]
      ,[PRICE]
      ,[CREATE_DTTM]
      ,[MODIF_DTTM]
      ,[USER_CREATE]
      ,[USER_MODIF]
      ,[SUPL_DESCRIPTION]
      ,[ARCHV_FLAG]
      ,[STRAN_REFNO]
      ,[PRIOR_POINTER]
      ,[EXTERNAL_KEY]
      ,[TRAVERSE_ONLY_FLAG]
      ,[DURATION]
      ,[CITEM_REFNO]
      ,[BLOCK_NUMBER]
      ,[CDTYP_REFNO]
      ,[START_DTTM]
      ,[END_DTTM]
      ,[SCLVL_REFNO]
      ,[SYN_CODE]
      ,[OWNER_HEORG_REFNO]
      ,[Created]
FROM [WH].[PAS].[CodingBase]
WHERE CCSXT_CODE in ('I10', 'OPCS4')  
  
;