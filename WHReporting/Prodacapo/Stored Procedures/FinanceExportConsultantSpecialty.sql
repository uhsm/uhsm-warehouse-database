﻿/******************************************************************************
 Description: Extracts consultant specialty for use by Finance in 
              the SLR/PLCS project.
  
 Parameters:  @ActivityStartDate - Start date for data extract
              @ActivityEndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author			Change
 13/11/14 Jubeda Begum		Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportConsultantSpecialty]

AS

SET NOCOUNT ON;

SELECT     
[ConsultantCode]
      ,[ConsultantName]
      ,[SpecialtyFunctionCode]
      ,[SpecialtyCode]
      ,[SpecialtyCodeMain]
      ,[LatestActivity]
      ,[EarliestActivity]
FROM WHREPORTING.LK.ConsultantSpecialty
;