﻿/******************************************************************************
 Description: Extracts critical care period for use by Finance in 
              the SLR/PLCS project.
  
 Parameters:  @ActivityStartDate - Start date for data extract
              @ActivityEndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author			Change
 13/11/14 Jubeda Begum		Created
 21/06/16 CMan				Modified to include a join to spell so that all crit care periods relating to a spell discharge in the financial year period is included 

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportCriticalCarePeriod]
 @ActivityStartDate DATETIME
,@ActivityEndDate DATETIME
AS

SET NOCOUNT ON;

SELECT     
		CCP.[EncounterRecno]
      ,CCP.[SourceCCPStayNo]
      ,CCP.[SourceCCPNo]
      ,CCP.[CCPSequenceNo]
      ,CCP.[SourceEncounterNo]
      ,CCP.[SourceWardStayNo]
      ,CCP.[SourceSpellNo]
      ,CCP.[CCPIndicator]
      ,CCP.[CCPStartDate]
      ,CCP.[CCPEndDate]
      ,CCP.[CCPReadyDate]
      ,CCP.[CCPNoOfOrgansSupported]
      ,CCP.[CCPICUDays]
      ,CCP.[CCPHDUDays]
      ,CCP.[CCPAdmissionSource]
      ,CCP.[CCPAdmissionType]
      ,CCP.[CCPAdmissionSourceLocation]
      ,CCP.[CCPUnitFunction]
      ,CCP.[CCPUnitBedFunction]
      ,CCP.[CCPDischargeStatus]
      ,CCP.[CCPDischargeDestination]
      ,CCP.[CCPDischargeLocation]
      ,CCP.[CCPStayOrganSupported]
      ,CCP.[CCPStayCareLevel]
      ,CCP.[CCPStayNoOfOrgansSupported]
      ,CCP.[CCPStayWard]
      ,CCP.[CCPStayDate]
      ,CCP.[CCPStay]
      ,CCP.[CCPStayType]
      ,CCP.[CCPBedStay]
FROM WH.APC.CriticalCarePeriod CCP
LEFT OUTER JOIN WHREPORTING.APC.Spell on CCP.SourceSpellNo = Spell.SourceSpellNo-- added this as AB has requested that Critical Care Periods needs to include all periods which relate to a spell discharge in the financial year that we are looking at.  Therefore joined back to spell to use the spell discharge date 
WHERE Spell.DischargeDate BETWEEN @ActivityStartDate AND @ActivityEndDate;