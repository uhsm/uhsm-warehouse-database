﻿/******************************************************************************
 Description: Extracts finished consultant episodes for use by Finance in 
              the SLR/PLCS project.
  
 Parameters:  @ActivityStartDate - Start date for data extract
              @ActivityEndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author       Change
 12/11/14 Tim Dean	   Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportEpisodes]
 @ActivityStartDate DATETIME
,@ActivityEndDate DATETIME
AS

SET NOCOUNT ON;

SELECT     
  e.EncounterRecno
 ,e.SourcePatientNo
 ,e.SourceSpellNo
 ,e.EpisodeUniqueID
 ,e.ReferralSourceUniqueID
 ,e.WaitingListSourceUniqueID
 ,e.FacilityID
 ,e.NHSNo
 ,e.AdmissionDate
 ,e.AdmissionTime
 ,e.AdmissionDateTime
 ,e.DischargeDate
 ,e.DischargeTime
 ,e.DischargeDateTime
 ,e.EpisodeStartDate
 ,e.EpisodeStartTime
 ,e.EpisodeStartDateTime
 ,e.EpisodeEndDate
 ,e.EpisodeEndTime
 ,e.EpisodeEndDateTime
 ,e.StartWardCode
 ,e.StartWard
 ,e.EndWardCode
 ,e.EndWard
 ,e.ConsultantCode
 ,e.ConsultantName
 ,e.Division
 ,e.Directorate
 ,e.SpecialtyCode
 ,e.Specialty
 ,e.[SpecialtyCode(Function)]
 ,e.[Specialty(Function)]
 ,e.[SpecialtyCode(Main)]
 ,e.[Specialty(Main)]
 ,e.LengthOfEpisode
 ,e.PrimaryDiagnosisCode
 ,e.PrimaryProcedureCode
 ,e.PrimaryProcedureDateTime
 ,e.EpisodeGPCode
 ,e.EpisodeGP
 ,e.EpisodicPracticeCode
 ,e.EpisodePractice
 ,e.EpisodePCTCode
 ,e.EpisodePCT
 ,e.EpisodeCCGCode
 ,e.EpisodeCCG
 ,e.EpisodePCTCCGCode
 ,e.EpisodePCTCCG
 ,e.PostCode
 ,e.ResidencePCTCode
 ,e.ResidencePCT
 ,e.ResidenceLocalAuthorityCode
 ,e.ResidenceLocalAuthority
 ,e.PurchaserCode
 ,e.Purchaser
 ,e.ResponsibleCommissionerCode
 ,e.ResponsibleCommissioner
 ,e.ResponsibleCommissionerCCGOnlyCode
 ,e.ResponsibleCommissionerCCGOnly
 ,e.TheatrePatientBookingKey
 ,e.TheatreCode
 ,e.Theatre
 ,e.AgeCode
 ,e.AdmissionType
 ,e.NeoNatalLevelOfCare
 ,e.FirstEpisodeInSpell
 ,e.LastEpisodeInSpell
 ,p.SexCode
FROM WHREPORTING.APC.Episode AS e 
LEFT OUTER JOIN WHREPORTING.APC.Patient AS p ON e.EncounterRecno = p.EncounterRecno
WHERE e.EpisodeEndDate BETWEEN @ActivityStartDate AND @ActivityEndDate;