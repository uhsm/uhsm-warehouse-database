﻿/******************************************************************************
 Description: Extracts all consultant episodes for use by Finance in 
              the SLR/PLCS project.
  
 Parameters:  @ActivityStartDate - Start date for data extract
              @ActivityEndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author       Change
 08/10/15 Tim Dean	   Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportEpisodesWIP]
 @ActivityStartDate DATETIME
,@ActivityEndDate DATETIME
AS

SET NOCOUNT ON;

SELECT Episode.EncounterRecno
	,Episode.SourcePatientNo
	,Episode.SourceSpellNo
	,Episode.EpisodeUniqueID
	,Episode.ReferralSourceUniqueID
	,Episode.WaitingListSourceUniqueID
	,Episode.FacilityID
	,Episode.NHSNo
	,Episode.AdmissionDate
	,Episode.AdmissionTime
	,Episode.AdmissionDateTime
	,Episode.DischargeDate
	,Episode.DischargeTime
	,Episode.DischargeDateTime
	,Episode.EpisodeStartDate
	,Episode.EpisodeStartTime
	,Episode.EpisodeStartDateTime
	,Episode.EpisodeEndDate
	,Episode.EpisodeEndTime
	,Episode.EpisodeEndDateTime
	,Episode.StartWardCode
	,Episode.StartWard
	,Episode.EndWardCode
	,Episode.EndWard
	,Episode.ConsultantCode
	,Episode.ConsultantName
	,Episode.Division
	,Episode.Directorate
	,Episode.SpecialtyCode
	,Episode.Specialty
	,Episode.[SpecialtyCode(Function)]
	,Episode.[Specialty(Function)]
	,Episode.[SpecialtyCode(Main)]
	,Episode.[Specialty(Main)]
	,Episode.LengthOfEpisode
	,Episode.PrimaryDiagnosisCode
	,Episode.PrimaryProcedureCode
	,Episode.PrimaryProcedureDateTime
	,Episode.EpisodeGPCode
	,Episode.EpisodeGP
	,Episode.EpisodicPracticeCode
	,Episode.EpisodePractice
	,Episode.EpisodePCTCode
	,Episode.EpisodePCT
	,Episode.EpisodeCCGCode
	,Episode.EpisodeCCG
	,Episode.EpisodePCTCCGCode
	,Episode.EpisodePCTCCG
	,Episode.PostCode
	,Episode.ResidencePCTCode
	,Episode.ResidencePCT
	,Episode.ResidenceLocalAuthorityCode
	,Episode.ResidenceLocalAuthority
	,Episode.PurchaserCode
	,Episode.Purchaser
	,Episode.ResponsibleCommissionerCode
	,Episode.ResponsibleCommissioner
	,Episode.ResponsibleCommissionerCCGOnlyCode
	,Episode.ResponsibleCommissionerCCGOnly
	,Episode.TheatrePatientBookingKey
	,Episode.TheatreCode
	,Episode.Theatre
	,Episode.AgeCode
	,Episode.AdmissionType
	,Episode.NeoNatalLevelOfCare
	,Episode.FirstEpisodeInSpell
	,Episode.LastEpisodeInSpell
	,Patient.SexCode
	,PeriodEndStatus = CASE 
		WHEN Episode.EpisodeEndDate IS NULL
			THEN 'Open'
		ELSE 'Closed'
		END
FROM WHREPORTING.APC.Episode
LEFT JOIN WHREPORTING.APC.Patient ON Episode.EncounterRecno = Patient.EncounterRecno
WHERE Episode.EpisodeStartDate <= @ActivityEndDate
	AND (
		Episode.EpisodeEndDate >= @ActivityStartDate
		OR Episode.EpisodeEndDate IS NULL
		)