﻿/******************************************************************************
 Description: Extracts HRGAPCEncounter data for use by Finance in 
              the SLR/PLCS project.
 
 Modification History --------------------------------------------------------

 Date     Author			Change
 12/11/14 Jubeda Begum		Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportHRGAPCEncounter]

AS

SET NOCOUNT ON;

SELECT   
[HRGCode]
      ,[GroupingMethodFlag]
      ,[DominantOperationCode]
      ,[PBCCode]
      ,[CalculatedEpisodeDuration]
      ,[ReportingEpisodeDuration]
      ,[Trimpoint]
      ,[ExcessBeddays]
      ,[SpellReportFlag]
      ,[ProviderSpellNo]
      ,[SourceUniqueID]
      ,[RehabilitationDaysInput]
      ,[EncounterRecno]
      ,[Created]
FROM WH.HRG.HRG46APCEncounter

UNION ALL

SELECT   
[HRGCode]
      ,[GroupingMethodFlag]
      ,[DominantOperationCode]
      ,[PBCCode]
      ,[CalculatedEpisodeDuration]
      ,[ReportingEpisodeDuration]
      ,[Trimpoint]
      ,[ExcessBeddays]
      ,[SpellReportFlag]
      ,[ProviderSpellNo]
      ,[SourceUniqueID]
      ,[RehabilitationDaysInput]
      ,[EncounterRecno]
      ,[Created]
FROM WH.HRG.HRG47APCEncounter

UNION ALL

SELECT  
     [HRGCode]
      ,[GroupingMethodFlag]
      ,[DominantOperationCode]
      ,[PBCCode]
      ,[CalculatedEpisodeDuration]
      ,[ReportingEpisodeDuration]
      ,[Trimpoint]
      ,[ExcessBeddays]
      ,[SpellReportFlag]
      ,[ProviderSpellNo]
      ,[SourceUniqueID]
      ,[RehabilitationDaysInput]
      ,[EncounterRecno]
      ,[Created]
FROM PbR2015.HRG.HRG48APCEncounter