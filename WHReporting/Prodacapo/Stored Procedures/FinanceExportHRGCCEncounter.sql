﻿/******************************************************************************
 Description: Extracts HRGCCEncounter data for use by Finance in 
              the SLR/PLCS project.
 
 Modification History --------------------------------------------------------

 Date     Author			Change
 13/11/14 Jubeda Begum		Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportHRGCCEncounter]

AS

SET NOCOUNT ON;

SELECT   
[EncounterRecno]
      ,[HRGCode]
      ,[CriticalCareDays]
      ,[WarningCode]
      ,[Created]
      ,[SourceSpellNo]
FROM WH.HRG.HRG46CCEncounter
union all 
SELECT   
[EncounterRecno]
      ,[HRGCode]
      ,[CriticalCareDays]
      ,[WarningCode]
      ,[Created]
      ,[SourceSpellNo]
FROM WH.HRG.HRG47CCEncounter
union all 
SELECT   
[EncounterRecno]
      ,[HRGCode]
      ,[CriticalCareDays]
      ,[WarningCode]
      ,[Created]
      ,[SourceSpellNo]
FROM PbR2015.HRG.HRG48CCEncounter