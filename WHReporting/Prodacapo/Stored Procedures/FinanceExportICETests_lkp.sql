﻿/******************************************************************************
 Description: Extracts Therapy Contacts data for use by Finance in 
              the SLR/PLCS project.
  
 Parameters:  @ActivityStartDate - Start date for data extract
              @ActivityEndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author			Change
 12/11/14 Jubeda Begum	    Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportICETests_lkp]
AS

SET NOCOUNT ON;

    
SELECT distinct 
      [Test_Code]
      ,[Test_Name]
FROM [ICE_DB].[dbo].[OrderTests]
Order by [Test_Code]
;