﻿/******************************************************************************
 Description: Extracts OP Attendances for use by Finance in 
              the SLR/PLCS project.
  
 Parameters:  @ActivityStartDate - Start date for data extract
              @ActivityEndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author			Change
 12/11/14 Jubeda Begum		Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportOPAttendances]
 @ActivityStartDate DATETIME
,@ActivityEndDate DATETIME
AS

SET NOCOUNT ON;

   
SELECT     OP.EncounterRecno, OP.SourcePatientNo, OP.SourceUniqueID, OP.IsWardAttender, OP.ReferralSourceUniqueID, OP.WaitingListSourceUniqueID, 
                      OP.SessionUniqueID, OP.FacilityID, OP.NHSNo, OP.AgeAtAppointment, OP.ClinicCode, OP.Clinic, OP.AppointmentRequestDate, 
                      OP.EBookingReferenceNumber, OP.AppointmentDate, OP.AppointmentTime, OP.AppointmentDateTime, OP.ArrivalTime, OP.SeenTime, 
                      OP.DepartedTime, OP.ClinicWait, OP.AppointmentTypeCode, OP.AppointmentType, OP.AppointmentTypeNHSCode, OP.ProfessionalCarerType, 
                      OP.ProfessionalCarerCode, OP.ProfessionalCarerName, OP.Division, OP.Directorate, OP.SpecialtyCode, OP.Specialty, OP.[SpecialtyCode(Function)], 
                      OP.[Specialty(Function)], OP.[SpecialtyCode(Main)], OP.[Specialty(Main)], OP.AttendStatusCode, OP.AttendStatus, OP.AttendStatusNHSCode, OP.Status, 
                      OP.OutcomeCode, OP.Outcome, OP.OutcomeNHSCode, OP.CancelledByCode, OP.CancelledBy, OP.CancelledDateTime, 
                      OP.AdministrativeCategoryCode, OP.AdministrativeCategory, OP.AdministrativeCategoryNHSCode, OP.PrimaryDiagnosisCode, 
                      OP.PrimaryProcedureCode, OP.PrimaryProcedureDateTime, OP.AppointmentGPCode, OP.AppointmentGP, OP.AppointmentPracticeCode, 
                      OP.AppointmentPractice, OP.AppointmentPCTCode, OP.AppointmentPCT, OP.AppointmentCCGCode, OP.AppointmentCCG, OP.AppointmentPCTCCGCode, 
                      OP.AppointmentPCTCCG, OP.PostCode, OP.ResidencePCTCode, OP.ResidencePCT, OP.ResidenceLocalAuthorityCode, OP.ResidenceLocalAuthority, 
                      OP.PurchaserCode, OP.Purchaser, OP.ResponsibleCommissionerCode, OP.ResponsibleCommissioner, OP.ResponsibleCommissionerCCGOnlyCode, 
                      OP.ResponsibleCommissionerCCGOnly, OP.AppointmentCreated, OP.AppointmentCreatedByWhom, OP.AppointmentUpdated, 
                      OP.AppointmentUpdatedByWhom, OP.ExcludedClinic, OP.IsWalkIn, Pat.SexCode
FROM         OP.Schedule AS OP LEFT OUTER JOIN
                      OP.Patient AS Pat ON OP.EncounterRecno = Pat.EncounterRecno
WHERE OP.AppointmentDate BETWEEN @ActivityStartDate AND @ActivityEndDate;