﻿/******************************************************************************
 Description: Extracts OPSessionSlots for use by Finance in 
              the SLR/PLCS project.
  
 Parameters:  @ActivityStartDate - Start date for data extract
              @ActivityEndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author       Change
 12/11/14 Tim Dean	   Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportOPSessionSlots]
 @ActivityStartDate DATETIME
,@ActivityEndDate DATETIME
AS

SET NOCOUNT ON;

SELECT *
FROM OP.SessionUtilisation
WHERE SessionDate BETWEEN @ActivityStartDate AND @ActivityEndDate;