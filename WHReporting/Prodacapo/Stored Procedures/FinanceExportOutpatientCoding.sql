﻿/******************************************************************************
 Description: Extracts Outpatient Coding for use by Finance in 
              the SLR/PLCS project.
  
 Modification History --------------------------------------------------------

 Date     Author       Change
 12/11/14 Tim Dean	   Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportOutpatientCoding]

AS

SET NOCOUNT ON;

SELECT     
[EncounterRecno]
      ,[PrimaryDiagnosisCode]
      ,[PrimaryDiagnosis]
      ,[SubsidiaryDiagnosisCode]
      ,[SubsidiaryDiagnosis]
      ,[SecondaryDiagnosisCode1]
      ,[SecondaryDiagnosis1]
      ,[SecondaryDiagnosisCode2]
      ,[SecondaryDiagnosis2]
      ,[SecondaryDiagnosisCode3]
      ,[SecondaryDiagnosis3]
      ,[SecondaryDiagnosisCode4]
      ,[SecondaryDiagnosis4]
      ,[SecondaryDiagnosisCode5]
      ,[SecondaryDiagnosis5]
      ,[SecondaryDiagnosisCode6]
      ,[SecondaryDiagnosis6]
      ,[SecondaryDiagnosisCode7]
      ,[SecondaryDiagnosis7]
      ,[SecondaryDiagnosisCode8]
      ,[SecondaryDiagnosis8]
      ,[SecondaryDiagnosisCode9]
      ,[SecondaryDiagnosis9]
      ,[SecondaryDiagnosisCode10]
      ,[SecondaryDiagnosis10]
      ,[SecondaryDiagnosisCode11]
      ,[SecondaryDiagnosis11]
      ,[SecondaryDiagnosisCode12]
      ,[SecondaryDiagnosis12]
      ,[PriamryProcedureCode]
      ,[PriamryProcedure]
      ,[PrimaryProcedureDate]
      ,[SecondaryProcedureCode1]
      ,[SecondaryProcedure1]
      ,[SecondaryProcedure1Date]
      ,[SecondaryProcedureCode2]
      ,[SecondaryProcedure2]
      ,[SecondaryProcedure2Date]
      ,[SecondaryProcedureCode3]
      ,[SecondaryProcedure3]
      ,[SecondaryProcedure3Date]
      ,[SecondaryProcedureCode4]
      ,[SecondaryProcedure4]
      ,[SecondaryProcedure4Date]
      ,[SecondaryProcedureCode5]
      ,[SecondaryProcedure5]
      ,[SecondaryProcedure5Date]
      ,[SecondaryProcedureCode6]
      ,[SecondaryProcedure6]
      ,[SecondaryProcedure6Date]
      ,[SecondaryProcedureCode7]
      ,[SecondaryProcedure7]
      ,[SecondaryProcedure7Date]
      ,[SecondaryProcedureCode8]
      ,[SecondaryProcedure8]
      ,[SecondaryProcedure8Date]
      ,[SecondaryProcedureCode9]
      ,[SecondaryProcedure9]
      ,[SecondaryProcedure9Date]
      ,[SecondaryProcedureCode10]
      ,[SecondaryProcedure10]
      ,[SecondaryProcedure10Date]
      ,[SecondaryProcedureCode11]
      ,[SecondaryProcedure11]
      ,[SecondaryProcedure11Date]
FROM WHREPORTING.OP.ClinicalCoding
;