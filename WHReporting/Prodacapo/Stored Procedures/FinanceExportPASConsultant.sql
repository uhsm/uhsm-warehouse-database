﻿/******************************************************************************
 Description: Extracts PAS Consultants for use by Finance in 
              the SLR/PLCS project.
  
 Modification History --------------------------------------------------------

 Date     Author       Change
 12/11/14 Tim Dean	   Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportPASConsultant]

AS

SET NOCOUNT ON;

SELECT     
[PROCA_REFNO]
      ,[PROCA_REFNO_MAIN_IDENT]
      ,[PRTYP_REFNO]
      ,[PRTYP_REFNO_MAIN_CODE]
      ,[PRTYP_REFNO_DESCRIPTION]
      ,[FORENAME]
      ,[TITLE_REFNO]
      ,[TITLE_REFNO_MAIN_CODE]
      ,[TITLE_REFNO_DESCRIPTION]
      ,[GRADE_REFNO]
      ,[GRADE_REFNO_MAIN_CODE]
      ,[GRADE_REFNO_DESCRIPTION]
      ,[SURNAME]
      ,[SEXXX_REFNO]
      ,[SEXXX_REFNO_MAIN_CODE]
      ,[SEXXX_REFNO_DESCRIPTION]
      ,[DATE_OF_BIRTH]
      ,[CONTR_HOURS]
      ,[LEAVE_ENTITLEMENT]
      ,[PLACE_OF_BIRTH]
      ,[QUALIFICATIONS]
      ,[GLOVE_INITM_REFNO]
      ,[GLOVE_INITM_REFNO_MAIN_CODE]
      ,[GLOVE_INITM_REFNO_DESCRIPTION]
      ,[SKNPR_INITM_REFNO]
      ,[SKNPR_INITM_REFNO_MAIN_CODE]
      ,[SKNPR_INITM_REFNO_DESCRIPTION]
      ,[DEF_PATH_SPONT_REFNO]
      ,[DEF_PATH_SPONT_REFNO_CODE]
      ,[DEF_PATH_SPONT_REFNO_NAME]
      ,[START_DTTM]
      ,[END_DTTM]
      ,[DESCRIPTION]
      ,[CREATE_DTTM]
      ,[MODIF_DTTM]
      ,[USER_CREATE]
      ,[USER_MODIF]
      ,[LOCAL_FLAG]
      ,[MAIN_IDENT]
      ,[ARCHV_FLAG]
      ,[PRIMARY_STEAM_REFNO]
      ,[PRIMARY_STEAM_REFNO_CODE]
      ,[PRIMARY_STEAM_REFNO_NAME]
      ,[OWNER_HEORG_REFNO]
      ,[OWNER_HEORG_REFNO_MAIN_IDENT]
      ,[Created]
FROM WH.PAS.ProfessionalCarerBase;