﻿/******************************************************************************
 Description: Extracts Pathology ICE Requests data for use by Finance in 
              the SLR/PLCS project.
  
 Parameters:  @ActivityStartDate - Start date for data extract
              @ActivityEndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author			Change
 12/11/14 Jubeda Begum		Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportPathologyICERequests]
 @ActivityStartDate DATETIME
,@ActivityEndDate DATETIME
AS

SET NOCOUNT ON;

    
SELECT cal.TheMonth AS MonthRequestAdded,
       cal.FinancialMonthKey AS MonthSeq,
       PathTest.RequestDate,
       Con.ClinicianKey,
       PathTest.ClinicianSurname,
       PathTest.ClinicianSpecialtyCode,
       PathTest.ClinicianSpecialty,
       PathTest.ProviderName,
       PathTest.ProviderType,
       Loc.LocationName,
       Loc.LocationType,
       PathTest.InvestigationRequested,
       PathTest.SetRequested,
       PathTest.RequestIndex,
       1 AS CountSet,
       FR.MasterPatientCode,
       Pat.FacilID,
       Pat.NHSNumber,
       Pat.forename,
       Pat.Surname
FROM   ICE.CSDataset AS PathTest
       INNER JOIN ICE.FactInvestigation AS inv
               ON inv.InvestigationIndex = PathTest.InvestigationIndex
       LEFT OUTER JOIN LK.Calendar AS cal
                    ON cal.TheDate = PathTest.RequestDate
       LEFT JOIN ICE.FactRequest FR
       ON PathTest.RequestIndex = FR.RequestIndex
       
       LEFT JOIN ICE.MatchedPatients Pat
       ON FR.MasterPatientCode = Pat.PatientKey
       
       LEFT JOIN ICE.DimLocation Loc
       ON FR.MasterLocationCode = Loc.LocationKey
       
       LEFT JOIN ICE.DimClinician Con
       ON PathTest.ClinicianKey = Con.ClinicianKey
       
--WHERE  ( PathTest.providerkey IN ( @Provider ) )
       --AND ( PathTest.ClinicianSpecialtyCode IN ( @Specialty ) )
       WHERE ( PathTest.ProviderTypeKey = 1 )
       AND ( PathTest.ClinicianSpecialtyCode NOT IN ( '999', '9999' ) )
       AND ( PathTest.RequestDate BETWEEN @ActivityStartDate AND @ActivityEndDate  )
       AND ( PathTest.RequestIndex IN (SELECT DISTINCT test.RequestIndex
                                       FROM   ICE_DB.dbo.TestList AS test
                                              INNER JOIN ICE.CSDataset AS cs
                                                      ON cs.RequestIndex = test.RequestIndex
                                       WHERE  ( cs.RequestDate BETWEEN @ActivityStartDate AND @ActivityEndDate  )
                                              AND ( cs.ProviderTypeKey = 1 )
                                              --AND ( cs.providerkey IN ( @Provider ) )
                                              --AND ( cs.ClinicianSpecialtyCode IN ( @Specialty ) )
                                              AND ( cs.ClinicianSpecialtyCode NOT IN ( '999', '9999' ) )
                                              --AND ( cs.ClinicianKey IN ( @Clinician ) )
                                              --AND ( Rtrim(Ltrim(test.Test)) IN ( @SetContains ) )
                                               --OR ( test.Test IN ( 'Unknown' ) )
                                               ) 
                                               )
       --AND ( inv.InvestigationDescriptionKey IN ( @Investigation ) )
       AND ( PathTest.RequestDate BETWEEN @ActivityStartDate AND @ActivityEndDate )

GROUP  BY cal.TheMonth,
          cal.FinancialMonthKey,
          PathTest.RequestDate,
          Con.ClinicianKey,
          PathTest.ClinicianSurname,
          PathTest.ClinicianSpecialtyCode,
          PathTest.ClinicianSpecialty,
          PathTest.ProviderName,
          PathTest.ProviderType,
          Loc.LocationName,
		  Loc.LocationType,
          PathTest.InvestigationRequested,
          PathTest.SetRequested,
          PathTest.RequestIndex,
          FR.MasterPatientCode,
          Pat.FacilID,
          Pat.NHSNumber,
          Pat.forename,
          Pat.Surname;