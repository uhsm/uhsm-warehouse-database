﻿/******************************************************************************
 Description: Extracts RadiologyRoom for use by Finance in 
              the SLR/PLCS project.
  
 Modification History --------------------------------------------------------

 Date     Author			Change
 12/11/14 Jubeda Begum	    Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportRadiologyRoom]
AS

SET NOCOUNT ON;

SELECT
      [dict_radiology_locations]
      ,[rad_location_code]
      ,[rad_loc_desc]
FROM [WH].[MISYS].[DictRadiologyLocation];