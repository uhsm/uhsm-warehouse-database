﻿/******************************************************************************
 Description: Extracts Spells for use by Finance in 
              the SLR/PLCS project.
  
 Parameters:  @ActivityStartDate - Start date for data extract
              @ActivityEndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author			Change
 12/11/14 Jubeda Begum		Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportSpells]
 @ActivityStartDate DATETIME
,@ActivityEndDate DATETIME
AS

SET NOCOUNT ON;

SELECT [EncounterRecno]
      ,[SourcePatientNo]
      ,[SourceSpellNo]
      ,[ReferralSourceUniqueID]
      ,[WaitingListSourceUniqueID]
      ,[AdmissionOfferSourceUniqueID]
      ,[FaciltyID]
      ,[NHSNo]
      ,[AdmissionDate]
      ,[AdmissionTime]
      ,[AdmissionDateTime]
      ,[DischargeDate]
      ,[DischargeTime]
      ,[DischargeDateTime]
      ,[AdmissionConsultantCode]
      ,[AdmissionConsultantName]
      ,[AdmissionDivision]
      ,[AdmissionDirectorate]
      ,[AdmissionSpecialtyCode]
      ,[AdmissionSpecialty]
      ,[AdmissionSpecialtyCode(Function)]
      ,[AdmissionSpecialty(Function)]
      ,[ADmissionSpecialtyCode(Main)]
      ,[AdmissionSpecialty(Main)]
      ,[AdmissionSpecialtyCode(RTT)]
      ,[AdmissiontWardCode]
      ,[AdmissionWard]
      ,[AdmissionMethodCode]
      ,[AdmissionMethod]
      ,[AdmissionMethodNHSCode]
      ,[AdmissionSourceCode]
      ,[AdmissionSource]
      ,[AdmissionSourceNHSCode]
      ,[AdmissionCategoryCode]
      ,[AdmissionCategory]
      ,[AdmissionCategoryNHSCode]
      ,[IntendedManagementCode]
      ,[IntendedManagement]
      ,[IntendedManagementNHSCode]
      ,[FirstRegularDayOrNightAdmission]
      ,[InpatientStayCode]
      ,[DischargeWardCode]
      ,[DischargeWard]
      ,[DischargeMethodCode]
      ,[DischargeMethod]
      ,[DischargeMethodNHSCode]
      ,[DischargeDestinationCode]
      ,[DischargeDestination]
      ,[DischargeDestinationNHSCode]
      ,[LengthOfSpell]
      ,[GPCode]
      ,[GP]
      ,[PracticeCode]
      ,[Practice]
      ,[PCTCode]
      ,[PCT]
      ,[ProvisionalEndIndicator]
      ,[SpellCCGCode]
      ,[SpellCCG]
      ,[SpellPCTCCGCode]
      ,[SpellPCTCCG]
      ,[PurchaserCode]
      ,[Purchaser]
      ,[ResponsibleCommissionerCode]
      ,[ResponsibleCommissioner]
      ,[ResponsibleCommissionerCCGOnlyCode]
      ,[ResponsibleCommissionerCCGOnly]
      ,[TheatrePatientBookingKey]
      ,[TheatreCode]
      ,[Theatre]
      ,[AgeCode]
      ,[Age]
      ,[AdmissionType]
      ,[AdmissionCreated]
      ,[AdmissionCreatedByWhom]
      ,[AdmissionUpdated]
      ,[AdmissionUpdatedByWhom]
      ,[SmartboardEDD]
      ,[SmartboardEDDSetOnDate]
      ,[SmartboardEDDSetOnWard]
      ,[PatientClass]  
FROM WHREPORTING.APC.Spell  
WHERE AdmissionDate BETWEEN @ActivityStartDate AND @ActivityEndDate;