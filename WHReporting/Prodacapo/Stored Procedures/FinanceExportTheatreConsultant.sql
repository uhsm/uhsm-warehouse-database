﻿/******************************************************************************
 Description: Extracts Theatre Consultants data for use by Finance in 
              the SLR/PLCS project.
 
 Modification History --------------------------------------------------------

 Date     Author			Change
 12/11/14 Jubeda Begum		Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportTheatreConsultant]

AS

SET NOCOUNT ON;

SELECT   
[StaffCode]
      ,[Surname]
      ,[Forename]
      ,[Initial]
      ,[StaffCode1]
      ,[StaffCategoryCode]
      ,[SpecialtyCode]
FROM WH.Theatre.Staff
;