﻿/******************************************************************************
 Description: Extracts Theatre Session data for use by Finance in 
              the SLR/PLCS project.
  
 Parameters:  @ActivityStartDate - Start date for data extract
              @ActivityEndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author			Change
 12/11/14 Jubeda Begum		Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportTheatreSession]
 @ActivityStartDate DATETIME
,@ActivityEndDate DATETIME
AS

SET NOCOUNT ON;

    
SELECT TS.*, S.Specialty, S.SpecialtyCode1, S.InactiveFlag FROM
WH.Theatre.Session TS
LEFT JOIN WH.Theatre.Specialty S
ON TS.SpecialtyCode = S.SpecialtyCode
WHERE StartTime BETWEEN @ActivityStartDate AND @ActivityEndDate;