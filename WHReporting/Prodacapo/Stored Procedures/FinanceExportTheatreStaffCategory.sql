﻿/******************************************************************************
 Description: Extracts Theatre Staff Category data for use by Finance in 
              the SLR/PLCS project.
 
 Modification History --------------------------------------------------------

 Date     Author			Change
 12/11/14 Jubeda Begum		Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportTheatreStaffCategory]

AS

SET NOCOUNT ON;

SELECT   
[StaffCategoryCode]
      ,[StaffCategoryCode1]
      ,[StaffCategory]
      ,[SurgeonFlag]
      ,[InactiveFlag]
      ,[ConsultantFlag]
      ,[SupervisionFlag]
      ,[AnaesthetistFlag]
      ,[OtherFlag]
      ,[NurseFlag]
      ,[PorterFlag]
      ,[Technician1Flag]
      ,[Technician2Flag]
      ,[ClerkFlag]
FROM WH.Theatre.StaffCategory
;