﻿/******************************************************************************
 Description: Extracts Therapy Contacts data for use by Finance in 
              the SLR/PLCS project.
  
 Parameters:  @ActivityStartDate - Start date for data extract
              @ActivityEndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author       Change
 12/11/14 Tim Dean	   Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportTherapyContacts]
 @ActivityStartDate DATETIME
,@ActivityEndDate DATETIME
AS

SET NOCOUNT ON;

SELECT     
[Cont_Ref]
      ,[Ref_Ref]
      ,[WList_Ref]
      ,[Pat_Ref]
      ,[FACIL_ID]
      ,[NHS_number]
      ,[NHSNoStat]
      ,[Forename]
      ,[Surname]
      ,[DOB]
      ,[Gender]
      ,[AdministrativeCategory]
      ,[Cont_Date]
      ,[Cont_Time]
      ,[Spec_Code]
      ,[Spec_Name]
      ,[Prof_Carer]
      ,[Prof_Carer_Name]
      ,[Purchaser_Code]
      ,[Location]
      ,[Location_Type]
      ,[Seen_By_Carer_Code]
      ,[Seen_By_Carer_Surname]
      ,[Resource_Unit_Type]
      ,[Purpose]
      ,[Visit_Type]
      ,[Urgency]
      ,[Priority]
      ,[Contact_Duration]
      ,[Contact_Type]
      ,[Booking_Type]
      ,[Cancellation_Reason]
      ,[Cancelled_By]
      ,[Cancellation_Date]
      ,[Arrival_Time]
      ,[Seen_Time]
      ,[Depart_Time]
      ,[Attend_Status]
      ,[DNA_Reason]
      ,[Planned_Number]
      ,[Actual_Number]
      ,[Service_Point_Ref]
      ,[Service_Point_Code]
      ,[CREATOR]
      ,[Create_Date]
      ,[MODIFIER]
      ,[Modified_Date]
      ,[Outcome]
FROM WHREPORTING.TPY.ContactsDataset
WHERE [Cont_Date] BETWEEN @ActivityStartDate AND @ActivityEndDate;