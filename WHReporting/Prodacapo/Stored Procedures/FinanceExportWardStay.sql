﻿/******************************************************************************
 Description: Extracts ward stay for use by Finance in 
              the SLR/PLCS project.
  
 Parameters:  @ActivityStartDate - Start date for data extract
              @ActivityEndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author       Change
 12/11/14 Tim Dean	   Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportWardStay]
 @ActivityStartDate DATETIME
,@ActivityEndDate DATETIME
AS

SET NOCOUNT ON;

SELECT     
[SourceUniqueID]
      ,[SourceSpellNo]
      ,[SourcePatientNo]
      ,[StartTime]
      ,[EndTime]
      ,[WardCode]
      ,[ProvisionalFlag]
      ,[PASCreated]
      ,[PASUpdated]
      ,[PASCreatedByWhom]
      ,[PASUpdatedByWhom]
FROM WHREPORTING.APC.WardStay 
WHERE StartTime BETWEEN @ActivityStartDate AND @ActivityEndDate;