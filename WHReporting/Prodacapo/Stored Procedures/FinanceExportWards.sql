﻿/******************************************************************************
 Description: Extracts Wards data for use by Finance in 
              the SLR/PLCS project.
  
 Parameters:  @ActivityStartDate - Start date for data extract
              @ActivityEndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author			Change
 12/11/14 Jubeda Begum		Created

******************************************************************************/

CREATE PROCEDURE [Prodacapo].[FinanceExportWards]
AS

SET NOCOUNT ON;

    
SELECT 
[SPONT_REFNO]
,[SPONT_REFNO_CODE]
,[SPONT_REFNO_NAME]
,[SPECT_REFNO_MAIN_IDENT]
,[CODE]
,[DESCRIPTION]
,[NAME]
FROM [WH].[PAS].[ServicePointBase]
where SPTYP_REFNO = '1520'
and END_DTTM is null and ARCHV_FLAG = 'N'
;