﻿/******************************************************************************
 Description: Extracts and updates the A&E 4 Hour numerator and
              denominator values for the scorecard measures.
  
 Parameters:  @StartDate - Start date for data extract
              @EndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author       Change
 04/08/14 Tim Dean	   Created

******************************************************************************/

CREATE Procedure [Prodacapo].[Update4HOURS]
 @StartDate as Date
,@EndDate as Date
as

UPDATE WHREPORTING.Prodacapo.IMPME30_PDC
  SET ACTUAL = NewData.ACTUAL
     ,PLANNED = NewData.PLANNED
FROM WHREPORTING.Prodacapo.IMPME30_PDC ExistingData
INNER JOIN ( 
            SELECT 
              MESTRID = '5-101'
             ,PERIOD = DATENAME(YEAR,Calendar.FirstDateOfMonth) + ' ' + DATENAME(MONTH,Calendar.FirstDateOfMonth)
             ,ACTUAL = ROUND(((COUNT(*) - SUM(CASE
                                                WHEN Attendance.EncounterDurationMinutes > 240 THEN 1.00
                                                ELSE 0.00
                                              END)) / COUNT(*)) * 100,2)	
             ,PLANNED = 95.00
            FROM WH.AE.Encounter Attendance
              LEFT JOIN WHREPORTING.LK.Calendar Calendar
                ON (Calendar.TheDate = Attendance.ArrivalDate)       
            WHERE Attendance.ArrivalDate BETWEEN @StartDate AND @EndDate
                  AND Attendance.AttendanceCategoryCode IN ( '1', '2' )
            GROUP BY Calendar.FirstDateOfMonth             
           ) NewData ON (ExistingData.MESTRID = NewData.MESTRID AND ExistingData.PERIOD = NewData.PERIOD);