﻿/******************************************************************************
 Description: Extracts and updates the A&E trolley wait > 12 hours measure.
  
 Parameters:  @StartDate - Start date for data extract
              @EndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author       Change
 07/07/14 Tim Dean	   Created

******************************************************************************/

CREATE Procedure [Prodacapo].[UpdateAETROLLEY]
 @StartDate as Date
,@EndDate as Date
as

UPDATE WHREPORTING.Prodacapo.IMPME30_PDC
  SET ACTUAL = NewData.ACTUAL
FROM WHREPORTING.Prodacapo.IMPME30_PDC ExistingData
INNER JOIN ( 
            SELECT 
              MESTRID = 'UHSM-AETROLLEY'
             ,PERIOD = DATENAME(YEAR,Calendar.FirstDateOfMonth) + ' ' + DATENAME(MONTH,Calendar.FirstDateOfMonth)
             ,ACTUAL = SUM(CASE
                             WHEN DATEDIFF(MINUTE,Attendance.DecisionToAdmitTime,Attendance.AttendanceConclusionTime)  > 720 THEN 1
                             ELSE 0
                           END
                           )	
            FROM WH.AE.Encounter Attendance
              LEFT JOIN WHREPORTING.LK.Calendar Calendar
                ON (Calendar.TheDate = Attendance.ArrivalDate)       
            WHERE Attendance.ArrivalDate BETWEEN @StartDate AND @EndDate
                  AND Attendance.AttendanceCategoryCode IN ( '1', '2' )         
            GROUP BY Calendar.FirstDateOfMonth           
           ) NewData ON (ExistingData.MESTRID = NewData.MESTRID AND ExistingData.PERIOD = NewData.PERIOD);