﻿/******************************************************************************
 Description: Extracts and updates the divisional OP DNA rate numerator and
              denominator values for the scorecard measures.
  
 Parameters:  @StartDate - Start date for data extract
              @EndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author       Change
 05/08/14 Tim Dean	   Created

******************************************************************************/

CREATE Procedure [Prodacapo].[UpdateOPDNA]
 @StartDate as Date
,@EndDate as Date
as

-- Numerator values
UPDATE WHREPORTING.Prodacapo.IMPME30_PDC
  SET ACTUAL = NewData.ACTUAL
     ,PLANNED = NewData.PLANNED
FROM WHREPORTING.Prodacapo.IMPME30_PDC ExistingData
INNER JOIN ( 
            SELECT 
              MESTRID = CAST(Scorecard.SpecialtyScorecardID AS VARCHAR(2)) + '-103N' 
             ,PERIOD = DATENAME(YEAR,Calendar.FirstDateOfMonth) + ' ' + DATENAME(MONTH,Calendar.FirstDateOfMonth)
             ,ACTUAL = SUM(CASE
                             WHEN Appointment.[Status] = 'DNA' THEN 1
                             ELSE 0	
                           END)
             ,PLANNED = CAST(COUNT(*) * 0.075 AS INT)
            FROM WHREPORTING.OP.Schedule Appointment
              LEFT JOIN WHREPORTING.dbo.vwExcludeWAWardsandClinics Exclude
                ON (Appointment.ClinicCode = Exclude.SPONT_REFNO_CODE)
              LEFT JOIN WHREPORTING.LK.Calendar Calendar
                ON (Calendar.TheDate = Appointment.AppointmentDate)   
              INNER JOIN WHREPORTING.Prodacapo.SpecialtyToScorecard Scorecard 
                    ON (Appointment.[SpecialtyCode(Function)] = Scorecard.TreatmentFunctionCode)                        
            WHERE Appointment.AppointmentDate BETWEEN @StartDate AND @EndDate
                  AND Exclude.SPONT_REFNO_CODE IS NULL
                  AND Appointment.IsWardAttender = 0
                  AND Appointment.[Status]IN ('DNA','ATTEND')
                  AND Appointment.Division IN ( 'Unscheduled Care'
                                               ,'Scheduled Care'
                                               ,'Clinical Support' )                                   
                  AND (Appointment.AppointmentTypeNHSCode IN (1, 2) 
                       OR Appointment.AppointmentTypeNHSCode IS NULL)
                  AND Appointment.AdministrativeCategory NOT LIKE '%Private%'
            GROUP BY Scorecard.SpecialtyScorecardID
                     ,Calendar.FirstDateOfMonth
           ) NewData ON (ExistingData.MESTRID = NewData.MESTRID AND ExistingData.PERIOD = NewData.PERIOD);


-- Denominator values 
UPDATE WHREPORTING.Prodacapo.IMPME30_PDC
  SET ACTUAL = NewData.ACTUAL
FROM WHREPORTING.Prodacapo.IMPME30_PDC ExistingData
INNER JOIN ( 
            SELECT 
              MESTRID = CAST(Scorecard.SpecialtyScorecardID AS VARCHAR(2)) + '-103D' 
             ,PERIOD = DATENAME(YEAR,Calendar.FirstDateOfMonth) + ' ' + DATENAME(MONTH,Calendar.FirstDateOfMonth)
             ,ACTUAL = COUNT(*)
            FROM WHREPORTING.OP.Schedule Appointment
              LEFT JOIN WHREPORTING.dbo.vwExcludeWAWardsandClinics Exclude
                ON (Appointment.ClinicCode = Exclude.SPONT_REFNO_CODE)
              LEFT JOIN WHREPORTING.LK.Calendar Calendar
                ON (Calendar.TheDate = Appointment.AppointmentDate)   
              INNER JOIN WHREPORTING.Prodacapo.SpecialtyToScorecard Scorecard 
                    ON (Appointment.[SpecialtyCode(Function)] = Scorecard.TreatmentFunctionCode)                        
            WHERE Appointment.AppointmentDate BETWEEN @StartDate AND @EndDate
                  AND Exclude.SPONT_REFNO_CODE IS NULL
                  AND Appointment.IsWardAttender = 0
                  AND Appointment.[Status]IN ('DNA','ATTEND')
                  AND Appointment.Division IN ( 'Unscheduled Care'
                                               ,'Scheduled Care'
                                               ,'Clinical Support' )                                   
                  AND (Appointment.AppointmentTypeNHSCode IN (1, 2) 
                       OR Appointment.AppointmentTypeNHSCode IS NULL)
                  AND Appointment.AdministrativeCategory NOT LIKE '%Private%'
            GROUP BY Scorecard.SpecialtyScorecardID
                     ,Calendar.FirstDateOfMonth
           ) NewData ON (ExistingData.MESTRID = NewData.MESTRID AND ExistingData.PERIOD = NewData.PERIOD);