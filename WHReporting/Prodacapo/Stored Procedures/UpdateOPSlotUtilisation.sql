﻿/******************************************************************************
 Description: Extracts and updates the OP slot utilisation rate numerator and
              denominator values for the scorecard measures.
  
 Parameters:  @StartDate - Start date for data extract
              @EndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author       Change
 28/08/14 Tim Dean	   Created

******************************************************************************/

CREATE Procedure [Prodacapo].[UpdateOPSlotUtilisation]
 @StartDate as Date
,@EndDate as Date
as

-- Numerator values
UPDATE WHREPORTING.Prodacapo.IMPME30_PDC
  SET ACTUAL = NewData.ACTUAL
     ,PLANNED = NewData.PLANNED
FROM WHREPORTING.Prodacapo.IMPME30_PDC ExistingData
INNER JOIN ( 
            SELECT 
              MESTRID = CAST(Scorecard.SpecialtyScorecardID AS VARCHAR(2)) + '-105N' 
             ,PERIOD = DATENAME(YEAR,Calendar.FirstDateOfMonth) + ' ' + DATENAME(MONTH,Calendar.FirstDateOfMonth)
             ,ACTUAL = SUM(Util.BookingsMade)
             ,PLANNED = CAST(SUM(Util.BookingsAllowed) * 0.90 AS INT)
            FROM WHREPORTING.OP.SessionUtilisation Util
              INNER JOIN WHREPORTING.Prodacapo.SpecialtyToScorecard Scorecard 
                    ON (LEFT(Util.TreatmentFunctionCode,3) = Scorecard.TreatmentFunctionCode) 
              LEFT JOIN WHREPORTING.LK.Calendar Calendar
                    ON (Calendar.TheDate = Util.SessionDate)           
            WHERE Util.SessionDate BETWEEN @StartDate AND @EndDate
                  AND Util.SessionIsActive = 'Y'
                  AND Util.IsCancelled = 'N'
                  AND Util.ExcludedClinic = 'N'
                  AND NOT Util.ClinicCode LIKE '%TELE'
                  AND NOT Util.ClinicDescription LIKE '%TELE'
                  AND Util.SessionStatus IN ('Session Scheduled', 'Session Initiated', 'Session Held')
            GROUP BY Scorecard.SpecialtyScorecardID
                    ,Calendar.FirstDateOfMonth
            ) NewData ON (ExistingData.MESTRID = NewData.MESTRID AND ExistingData.PERIOD = NewData.PERIOD);
            
-- Denominator values             
UPDATE WHREPORTING.Prodacapo.IMPME30_PDC
  SET ACTUAL = NewData.ACTUAL
FROM WHREPORTING.Prodacapo.IMPME30_PDC ExistingData
INNER JOIN ( 
            SELECT 
              MESTRID = CAST(Scorecard.SpecialtyScorecardID AS VARCHAR(2)) + '-105D' 
             ,PERIOD = DATENAME(YEAR,Calendar.FirstDateOfMonth) + ' ' + DATENAME(MONTH,Calendar.FirstDateOfMonth)
             ,ACTUAL = SUM(Util.BookingsAllowed)
            FROM WHREPORTING.OP.SessionUtilisation Util
              INNER JOIN WHREPORTING.Prodacapo.SpecialtyToScorecard Scorecard 
                    ON (LEFT(Util.TreatmentFunctionCode,3) = Scorecard.TreatmentFunctionCode) 
              LEFT JOIN WHREPORTING.LK.Calendar Calendar
                    ON (Calendar.TheDate = Util.SessionDate)           
            WHERE Util.SessionDate BETWEEN @StartDate AND @EndDate
                  AND Util.SessionIsActive = 'Y'
                  AND Util.IsCancelled = 'N'
                  AND Util.ExcludedClinic = 'N'
                  AND NOT Util.ClinicCode LIKE '%TELE'
                  AND NOT Util.ClinicDescription LIKE '%TELE'
                  AND Util.SessionStatus IN ('Session Scheduled', 'Session Initiated', 'Session Held')
            GROUP BY Scorecard.SpecialtyScorecardID
                    ,Calendar.FirstDateOfMonth
            ) NewData ON (ExistingData.MESTRID = NewData.MESTRID AND ExistingData.PERIOD = NewData.PERIOD);