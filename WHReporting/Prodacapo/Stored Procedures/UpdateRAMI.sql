﻿/******************************************************************************
 Description: Extracts and updates the specialty RAMI numerator and
              denominator values for the scorecard measures.
  
 Parameters:  @StartDate - Start date for data extract
              @EndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author       Change
 04/08/14 Tim Dean	   Created

******************************************************************************/

CREATE Procedure [Prodacapo].[UpdateRAMI]
 @StartDate as Date
,@EndDate as Date
as

-- Update the numerator
UPDATE WHREPORTING.Prodacapo.IMPME30_PDC
SET --ACTUAL = NewData.PLANNED
	ACTUAL = NewData.ACTUAL
	,PLANNED = NewData.PLANNED
FROM WHREPORTING.Prodacapo.IMPME30_PDC ExistingData
INNER JOIN (
	SELECT DISTINCT MESTRID = CAST(Scorecard.SpecialtyScorecardID AS VARCHAR(2)) + '-100N'
		,PERIOD = DATENAME(YEAR, RollingPeriod.PeriodEndDate) + ' ' + DATENAME(MONTH, RollingPeriod.PeriodEndDate)
		,ACTUAL = SUM(CASE 
				WHEN Data.Death = 'Yes'
					AND Data.[RAMI 2013 inc] = 'Yes'
					THEN 1
				ELSE 0
				END)
		,PLANNED = ROUND(SUM(Data.[RAMI 2013]), 2)
	FROM (
		SELECT DISTINCT PeriodEndDate = Period.[Month]
			,PeriodStartDate = DATEADD(MONTH, - 11, Period.[Month])
		FROM WHREPORTING.CHKS.PatientLevelExtract Period
		WHERE Period.[Month] BETWEEN @StartDate
				AND @EndDate
		) RollingPeriod
	LEFT JOIN WHREPORTING.CHKS.PatientLevelExtract Data ON (
			RollingPeriod.PeriodStartDate <= Data.[Month]
			AND RollingPeriod.PeriodEndDate >= Data.[Month]
			)
	INNER JOIN WHREPORTING.Prodacapo.SpecialtyToScorecard Scorecard ON (Data.[PbR (HRG35) Treatment Func] = Scorecard.TreatmentFunctionCode)
	GROUP BY Scorecard.SpecialtyScorecardID
		,RollingPeriod.PeriodEndDate
	) NewData ON (
		ExistingData.MESTRID = NewData.MESTRID
		AND ExistingData.PERIOD = NewData.PERIOD
		);
		
-- Update the denominator
UPDATE WHREPORTING.Prodacapo.IMPME30_PDC
SET ACTUAL = NewData.PLANNED
FROM WHREPORTING.Prodacapo.IMPME30_PDC ExistingData
INNER JOIN (
	SELECT DISTINCT MESTRID = CAST(Scorecard.SpecialtyScorecardID AS VARCHAR(2)) + '-100D'
		,PERIOD = DATENAME(YEAR, RollingPeriod.PeriodEndDate) + ' ' + DATENAME(MONTH, RollingPeriod.PeriodEndDate)
		,ACTUAL = SUM(CASE 
				WHEN Data.Death = 'Yes'
					AND Data.[RAMI 2013 inc] = 'Yes'
					THEN 1
				ELSE 0
				END)
		,PLANNED = ROUND(SUM(Data.[RAMI 2013]), 2)
	FROM (
		SELECT DISTINCT PeriodEndDate = Period.[Month]
			,PeriodStartDate = DATEADD(MONTH, - 11, Period.[Month])
		FROM WHREPORTING.CHKS.PatientLevelExtract Period
		WHERE Period.[Month] BETWEEN @StartDate
				AND @EndDate
		) RollingPeriod
	LEFT JOIN WHREPORTING.CHKS.PatientLevelExtract Data ON (
			RollingPeriod.PeriodStartDate <= Data.[Month]
			AND RollingPeriod.PeriodEndDate >= Data.[Month]
			)
	INNER JOIN WHREPORTING.Prodacapo.SpecialtyToScorecard Scorecard ON (Data.[PbR (HRG35) Treatment Func] = Scorecard.TreatmentFunctionCode)
	GROUP BY Scorecard.SpecialtyScorecardID
		,RollingPeriod.PeriodEndDate
	) NewData ON (
		ExistingData.MESTRID = NewData.MESTRID
		AND ExistingData.PERIOD = NewData.PERIOD
		);