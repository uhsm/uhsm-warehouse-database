﻿/******************************************************************************
 Description: Extracts and updates the theatre utilisation rate numerator and
              denominator values for the scorecard measures.
  
 Parameters:  @StartDate - Start date for data extract
              @EndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author       Change
 15/09/14 Tim Dean	   Created

******************************************************************************/

CREATE Procedure [Prodacapo].[UpdateTheatreUtilisation]
 @StartDate as Date
,@EndDate as Date
as

-- Numerator values
UPDATE WHREPORTING.Prodacapo.IMPME30_PDC
  SET ACTUAL = NewData.ACTUAL
     ,PLANNED = NewData.PLANNED
FROM WHREPORTING.Prodacapo.IMPME30_PDC ExistingData
INNER JOIN ( 
			SELECT 
			  MESTRID = CAST(Scorecard.SpecialtyScorecardID AS VARCHAR(2)) + '-124N' 
			 ,PERIOD = DATENAME(YEAR,c.FirstDateOfMonth) + ' ' + DATENAME(MONTH,c.FirstDateOfMonth)
			 ,ACTUAL = SUM(CASE 
							 WHEN t.KPI='Capped Session Utilisation' THEN (t.actual) 
							 ELSE 0 
						   END)
			 ,PLANNED = CAST(SUM(CASE 
							  WHEN t.KPI='Planned Minutes Of Actual Session' THEN (t.actual) 
							  ELSE 0 
							 END) * 0.90 AS INT)
			FROM WHOLAP.dbo.FactTheatreKPI t
			INNER JOIN WHOLAP.dbo.OlapTheatreSpecialty s ON (t.SpecialtyCode = s.SpecialtyCode)
			INNER JOIN WHOLAP.dbo.OlapTheatre th ON (th.TheatreCode=t.TheatreCode)
			INNER JOIN WHREPORTING.Prodacapo.SpecialtyToScorecard Scorecard ON (s.NationalSpecialtyCode = Scorecard.TreatmentFunctionCode)
			LEFT JOIN WHREPORTING.LK.Calendar c ON (c.TheDate = t.SessionDate)  
			WHERE t.SessionDate BETWEEN @StartDate AND @EndDate
				  AND (t.KPI = 'Planned Minutes Of Actual Session'
					   OR t.KPI='Capped Session Utilisation')
				  AND th.TheatreCode IN ( 1 -- ACUTE BLOCK THEATRE 1
										 ,2 -- ACUTE BLOCK THEATRE 2
										 ,3 -- ACUTE BLOCK THEATRE 3
										 ,6 -- ACUTE BLOCK THEATRE 6
										 ,78 -- ACUTE BLOCK THEATRE 6 A
										 ,37 -- CARDIO THORACIC THEATRE 1
										 ,38 -- CARDIO THORACIC THEATRE 2
										 ,39 -- CARDIO THORACIC THEATRE 3
										 ,40 -- CARDIO THORACIC THEATRE 4
										 ,14 -- ENT THEATRE 11
										 ,16 -- ENT THEATRE 12
										 ,21 -- F BLOCK THEATRE 10
										 ,13 -- F BLOCK THEATRE 7
										 ,15 -- F BLOCK THEATRE 8
										 ,19 -- F BLOCK THEATRE 9
										 ,20 -- TDC THEATRE 1
										 ,22 -- TDC THEATRE 2
										 ,23 -- TDC THEATRE 3
										 )
			GROUP BY Scorecard.SpecialtyScorecardID
					,c.FirstDateOfMonth
		   ) NewData ON (ExistingData.MESTRID = NewData.MESTRID AND ExistingData.PERIOD = NewData.PERIOD);
      
-- Denominator values  
UPDATE WHREPORTING.Prodacapo.IMPME30_PDC
  SET ACTUAL = NewData.ACTUAL
FROM WHREPORTING.Prodacapo.IMPME30_PDC ExistingData
INNER JOIN ( 
			SELECT 
			  MESTRID = CAST(Scorecard.SpecialtyScorecardID AS VARCHAR(2)) + '-124D' 
			 ,PERIOD = DATENAME(YEAR,c.FirstDateOfMonth) + ' ' + DATENAME(MONTH,c.FirstDateOfMonth)
			 ,ACTUAL = SUM(t.Actual)
			FROM WHOLAP.dbo.FactTheatreKPI t
			INNER JOIN WHOLAP.dbo.OlapTheatreSpecialty s ON (t.SpecialtyCode = s.SpecialtyCode)
			INNER JOIN WHOLAP.dbo.OlapTheatre th ON (th.TheatreCode=t.TheatreCode)
			INNER JOIN WHREPORTING.Prodacapo.SpecialtyToScorecard Scorecard ON (s.NationalSpecialtyCode = Scorecard.TreatmentFunctionCode)
			LEFT JOIN WHREPORTING.LK.Calendar c ON (c.TheDate = t.SessionDate)  
			WHERE t.SessionDate BETWEEN @StartDate AND @EndDate
				  AND t.KPI = 'Planned Minutes Of Actual Session'
				  AND th.TheatreCode IN ( 1 -- ACUTE BLOCK THEATRE 1
										 ,2 -- ACUTE BLOCK THEATRE 2
										 ,3 -- ACUTE BLOCK THEATRE 3
										 ,6 -- ACUTE BLOCK THEATRE 6
										 ,78 -- ACUTE BLOCK THEATRE 6 A
										 ,37 -- CARDIO THORACIC THEATRE 1
										 ,38 -- CARDIO THORACIC THEATRE 2
										 ,39 -- CARDIO THORACIC THEATRE 3
										 ,40 -- CARDIO THORACIC THEATRE 4
										 ,14 -- ENT THEATRE 11
										 ,16 -- ENT THEATRE 12
										 ,21 -- F BLOCK THEATRE 10
										 ,13 -- F BLOCK THEATRE 7
										 ,15 -- F BLOCK THEATRE 8
										 ,19 -- F BLOCK THEATRE 9
										 ,20 -- TDC THEATRE 1
										 ,22 -- TDC THEATRE 2
										 ,23 -- TDC THEATRE 3
										 )
			GROUP BY Scorecard.SpecialtyScorecardID
					,c.FirstDateOfMonth
            ) NewData ON (ExistingData.MESTRID = NewData.MESTRID AND ExistingData.PERIOD = NewData.PERIOD);