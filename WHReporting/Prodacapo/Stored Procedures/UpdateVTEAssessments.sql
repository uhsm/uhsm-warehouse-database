﻿/******************************************************************************
 Description: Extracts and updates the VTE Assessment numerator and
              denominator values for the scorecard measures.
  
 Parameters:  @StartDate - Start date for data extract
              @EndDate - End date for data extract
 
 Modification History --------------------------------------------------------

 Date     Author       Change
 28/08/14 Tim Dean	   Created

******************************************************************************/

CREATE Procedure [Prodacapo].[UpdateVTEAssessments]
 @StartDate as Date
,@EndDate as Date
as

-- Numerator values - ACTUAL
UPDATE WHREPORTING.Prodacapo.IMPME30_PDC
  SET ACTUAL = NewData.ACTUAL
FROM WHREPORTING.Prodacapo.IMPME30_PDC ExistingData
INNER JOIN ( 
            SELECT 
              MESTRID = CAST(Scorecard.SpecialtyScorecardID AS VARCHAR(2)) + '-106N' 
             ,PERIOD = DATENAME(YEAR,Calendar.FirstDateOfMonth) + ' ' + DATENAME(MONTH,Calendar.FirstDateOfMonth)
             ,ACTUAL = COUNT(*)
            FROM CorporateReports.VTE.SpellsWithAdmissionType Spell
              INNER JOIN WHREPORTING.Prodacapo.SpecialtyToScorecard Scorecard 
                    ON (LEFT(Spell.[AdmissionSpecialtyCode(Function)],3) = Scorecard.TreatmentFunctionCode) 
              LEFT JOIN WHREPORTING.LK.Calendar Calendar
                    ON (Calendar.TheDate = Spell.AdmissionDate)         
            WHERE Spell.AdmissionDate BETWEEN @StartDate AND @EndDate
                  AND (Spell.VTE = 1
                       OR Spell.DVT = 1
                       OR Spell.HD = 1
                       OR Spell.Chemo = 1
                       OR Spell.Endo = 1
                       OR Spell.DC = 1
                       OR Spell.EL_NoProcAnd0LOS = 1
                       OR Spell.Ward = 1
                       OR Spell.NEL_NoProcOrMinorProcAnd0LOS = 1
                       OR Spell.ProcUnder90min = 1)
            GROUP BY Scorecard.SpecialtyScorecardID
                    ,Calendar.FirstDateOfMonth
           ) NewData ON (ExistingData.MESTRID = NewData.MESTRID AND ExistingData.PERIOD = NewData.PERIOD);

-- Numerator values - PLANNED
UPDATE WHREPORTING.Prodacapo.IMPME30_PDC
  SET PLANNED = NewData.PLANNED
FROM WHREPORTING.Prodacapo.IMPME30_PDC ExistingData
INNER JOIN ( 
            SELECT 
              MESTRID = CAST(Scorecard.SpecialtyScorecardID AS VARCHAR(2)) + '-106N' 
             ,PERIOD = DATENAME(YEAR,Calendar.FirstDateOfMonth) + ' ' + DATENAME(MONTH,Calendar.FirstDateOfMonth)
             ,PLANNED = CAST(COUNT(*) * 0.95 AS INT)
            FROM CorporateReports.VTE.SpellsWithAdmissionType Spell
              INNER JOIN WHREPORTING.Prodacapo.SpecialtyToScorecard Scorecard 
                    ON (LEFT(Spell.[AdmissionSpecialtyCode(Function)],3) = Scorecard.TreatmentFunctionCode) 
              LEFT JOIN WHREPORTING.LK.Calendar Calendar
                    ON (Calendar.TheDate = Spell.AdmissionDate)         
            WHERE Spell.AdmissionDate BETWEEN @StartDate AND @EndDate
            GROUP BY Scorecard.SpecialtyScorecardID
                    ,Calendar.FirstDateOfMonth
           ) NewData ON (ExistingData.MESTRID = NewData.MESTRID AND ExistingData.PERIOD = NewData.PERIOD);

-- Denominator values
UPDATE WHREPORTING.Prodacapo.IMPME30_PDC
  SET ACTUAL = NewData.ACTUAL
FROM WHREPORTING.Prodacapo.IMPME30_PDC ExistingData
INNER JOIN ( 
            SELECT 
              MESTRID = CAST(Scorecard.SpecialtyScorecardID AS VARCHAR(2)) + '-106D' 
             ,PERIOD = DATENAME(YEAR,Calendar.FirstDateOfMonth) + ' ' + DATENAME(MONTH,Calendar.FirstDateOfMonth)
             ,ACTUAL = COUNT(*)
            FROM CorporateReports.VTE.SpellsWithAdmissionType Spell
              INNER JOIN WHREPORTING.Prodacapo.SpecialtyToScorecard Scorecard 
                    ON (LEFT(Spell.[AdmissionSpecialtyCode(Function)],3) = Scorecard.TreatmentFunctionCode) 
              LEFT JOIN WHREPORTING.LK.Calendar Calendar
                    ON (Calendar.TheDate = Spell.AdmissionDate)         
            WHERE Spell.AdmissionDate BETWEEN @StartDate AND @EndDate
            GROUP BY Scorecard.SpecialtyScorecardID
                    ,Calendar.FirstDateOfMonth
           ) NewData ON (ExistingData.MESTRID = NewData.MESTRID AND ExistingData.PERIOD = NewData.PERIOD);