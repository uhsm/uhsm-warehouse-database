﻿CREATE TABLE [Prodacapo].[CDIFF](
	[Ward] [nvarchar](255) NULL,
	[Directorate] [nvarchar](255) NULL,
	[Division] [nvarchar](255) NULL,
	[SpecimenDate] [datetime] NULL,
	[Date of Admission] [datetime] NULL,
	[Exclude_Report] [nvarchar](255) NULL
) ON [PRIMARY]