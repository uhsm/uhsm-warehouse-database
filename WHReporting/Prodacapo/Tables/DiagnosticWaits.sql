﻿CREATE TABLE [Prodacapo].[DiagnosticWaits](
	[Test] [nvarchar](255) NULL,
	[Month] [datetime] NULL,
	[Breach] [float] NULL,
	[Total Waiters] [float] NULL
) ON [PRIMARY]