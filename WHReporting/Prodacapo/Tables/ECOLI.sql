﻿CREATE TABLE [Prodacapo].[ECOLI](
	[SpecimenDate] [datetime] NULL,
	[Division] [nvarchar](255) NULL,
	[Pathway] [nvarchar](255) NULL,
	[CommAcquiredConfirmed] [nvarchar](255) NULL,
	[SpecimenTakenWard] [nvarchar](255) NULL
) ON [PRIMARY]