﻿CREATE TABLE [Prodacapo].[IMPME30_PDCCopy](
	[MODELID] [numeric](10, 0) NULL,
	[MODEL] [varchar](30) NULL,
	[MEASUREID] [numeric](10, 0) NULL,
	[MESTRID] [varchar](50) NULL,
	[DATESTAMP] [datetime] NULL,
	[PERIOD] [varchar](30) NULL,
	[ACTUAL] [float] NULL,
	[PLANNED] [float] NULL,
	[FORECAST] [float] NULL,
	[DATACOMMENT] [varchar](2000) NULL
) ON [PRIMARY]