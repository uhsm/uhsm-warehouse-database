﻿CREATE TABLE [Prodacapo].[TrustInduction](
	[Main Directorate] [nvarchar](255) NULL,
	[Sub Directorate] [nvarchar](255) NULL,
	[Specialty] [nvarchar](255) NULL,
	[Ward/Department] [nvarchar](255) NULL,
	[Position Title] [nvarchar](255) NULL,
	[Title] [nvarchar](255) NULL,
	[First Name] [nvarchar](255) NULL,
	[Last Name] [nvarchar](255) NULL,
	[Competence Match] [nvarchar](255) NULL,
	[Person Effective Start Date] [datetime] NULL,
	[Competence Name] [nvarchar](255) NULL,
	[Expiry Date] [nvarchar](255) NULL,
	[Role] [nvarchar](255) NULL,
	[Staff Group] [nvarchar](255) NULL,
	[Assignment Status] [nvarchar](255) NULL,
	[Assignment Category] [nvarchar](255) NULL,
	[Assignment] [float] NULL
) ON [PRIMARY]