﻿CREATE View [Prodacapo].[vwDNARate] AS 
SELECT

OP.Division,
OP.Directorate,
OP.[Specialty(Function)],


CASE WHEN AppointmentTypeNHSCode = 1 Then 'New'
WHEN AppointmentTypeNHSCode = 2 THEN 'Follow Up'
WHEN AppointmentTypeNHSCode IS NULL THEN 'Follow Up'
END AS ApptType,
SUM(CASE WHEN AttendStatus IN ('Refused to Wait','Patient Left / Not seen',
	'Patient Late / Not Seen','Did Not Attend') 
	OR (AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen') 
	AND Outcome = 'Did Not Attend') 
THEN 1 ELSE 0 END) AS DNAs, 

SUM(CASE WHEN AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen')
	AND (outcome IS NULL 
	OR (Outcome NOT LIKE '%canc%' AND Outcome NOT LIKE '%Did Not Attend%')) 
THEN 1 ELSE 0 END) AS Attends,

Total = SUM(CASE WHEN AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen')
	AND (outcome IS NULL 
	OR (Outcome NOT LIKE '%canc%' AND Outcome NOT LIKE '%Did Not Attend%')) 
THEN 1 ELSE 0 END)
+ 
SUM(CASE WHEN AttendStatus IN ('Refused to Wait','Patient Left / Not seen',
	'Patient Late / Not Seen','Did Not Attend') 
	OR (AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen') 
	AND Outcome = 'Did Not Attend') 
THEN 1 ELSE 0 END), 

CAST(

SUM(CASE WHEN AttendStatus IN ('Refused to Wait','Patient Left / Not seen',
	'Patient Late / Not Seen','Did Not Attend') 
	OR (AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen') 
	AND Outcome = 'Did Not Attend') 
THEN 1 ELSE 0 END)
AS decimal) 

/
 
CAST(

SUM(CASE WHEN AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen')
	AND (outcome IS NULL 
	OR (Outcome NOT LIKE '%canc%' AND Outcome NOT LIKE '%Did Not Attend%')) 
THEN 1 ELSE 0 END)
+ 
SUM(CASE WHEN AttendStatus IN ('Refused to Wait','Patient Left / Not seen',
	'Patient Late / Not Seen','Did Not Attend') 
	OR (AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen') 
	AND Outcome = 'Did Not Attend') 
THEN 1 ELSE 0 END) 
AS decimal) AS Percentage, 

--REPLACE(RIGHT(CONVERT(VARCHAR(9), AppointmentDate, 6), 6), ' ', '-') AS MonthYear
--DATENAME(MM,AppointmentDate) + ' ' + CAST(Year(AppointmentDate) AS VARCHAR(4)) AS MonthYear
--CONVERT(CHAR(4), AppointmentDate, 100) + CONVERT(CHAR(4), AppointmentDate, 120) AS MonthYear
Cal.TheMonth as TheMonth


FROM OP.Schedule OP
LEFT OUTER JOIN dbo.vwExcludeWAWardsandClinics Exc
ON OP.ClinicCode = Exc.SPONT_REFNO_CODE

LEFT JOIN LK.Calendar Cal
ON Cal.TheDate = OP.AppointmentDate

WHERE 
IsWardAttender = 0
AND AppointmentDate >= '2013-04-01'
--AND AppointmentDate <= @EndDate
AND Exc.SPONT_REFNO_CODE IS NULL
AND Status IN ('DNA','ATTEND')
AND OP.Division IN ('Unscheduled Care','Scheduled Care','Clinical Support' )
AND (OP.AppointmentTypeNHSCode IN (1, 2) OR OP.AppointmentTypeNHSCode IS NULL)
AND AdministrativeCategory NOT LIKE '%Private%'

GROUP BY 

CASE WHEN AppointmentTypeNHSCode = 1 Then 'New'
WHEN AppointmentTypeNHSCode = 2 THEN 'Follow Up'
WHEN AppointmentTypeNHSCode IS NULL THEN 'Follow Up'
END,

--REPLACE(RIGHT(CONVERT(VARCHAR(9), AppointmentDate, 6), 6), ' ', '-')
--DATENAME(MM,AppointmentDate) + ' ' + CAST(Year(AppointmentDate) AS VARCHAR(4))
--CONVERT(CHAR(4), AppointmentDate, 100) + CONVERT(CHAR(4), AppointmentDate, 120)
--ORDER BY [SpecialtyCode(Function)]

Cal.TheMonth,
OP.[Specialty(Function)],
OP.Division,
OP.Directorate