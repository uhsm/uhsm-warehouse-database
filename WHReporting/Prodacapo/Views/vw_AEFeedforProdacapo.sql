﻿CREATE VIEW [Prodacapo].[vw_AEFeedforProdacapo]
AS
/*************************************************************************************
 Modification History ----------------------------------------------------------------

 Date		Author			Change
29/05/2014	KO				Changed lookups for ArrivalMode and SourceOfReferral
*************************************************************************************/
SELECT     AE.EncounterRecno, AE.SourceUniqueID, AE.DistrictNo, AE.NHSNumber, AE.LocalPatientID, AE.PatientTitle, AE.PatientForename, AE.PatientSurname, 
                      AE.PatientAddress1, AE.PatientAddress2, AE.PatientAddress3, AE.PatientAddress4, AE.Postcode, AE.DateOfBirth, AE.DateOfDeath, AE.SexCode, 
                      AE.RegisteredGpCode, AE.RegisteredGpName, AE.RegisteredGpPracticeCode, AE.RegisteredGpPracticeName, AE.RegisteredGpPracticePostcode, 
                      AE.Consultant, AE.AttendanceNumber
                      ,AE.ArrivalModeCode
                      --,AM.ArrivalMode
                      ,ArrivalMode = 
						case 
							when AE.ArrivalModeCode = '1'
							then 'Ambulance'
							when AE.ArrivalModeCode = '2'
							then 'Other'
						end
                      , AE.AttendanceCategoryCode, AC.AttendanceCategory, 
                      AE.AttendanceDisposalCode, AD.AttendanceDisposal, AE.IncidentLocationTypeCode
                      ,AE.SourceOfReferralCode
                      --,SR.label
                      ,sorrf.SourceOfReferral
                      ,AE.ArrivalDate
                      ,AE.ArrivalTime, AE.AgeOnArrival, AE.InitialAssessmentTime, AE.SeenForTreatmentTime, AE.AttendanceConclusionTime, AE.DepartureTime, 
                      AE.CommissionerCodeCCG, AE.SeenByCode, AE.SeenBy, AE.InvestigationCodeFirst, AE.InvestigationCodeSecond, AE.DiagnosisCodeFirst, 
                      AE.DiagnosisCodeSecond, AE.CascadeDiagnosisCode, AE.TreatmentCodeFirst, AE.TreatmentCodeSecond, AE.Created, AE.InterfaceCode, AE.PCTCode, 
                      AE.CCGCode, AE.ResidencePCTCode, AE.ResidenceCCGCode, AE.SourceOfCCGCode, AE.TriageCategoryCode, CASE WHEN TriageCategoryCode IN ('O',
                       'R', 'Y') THEN 'Major' ELSE 'Minor' END AS Triage, AE.PresentingProblem, AE.ToXrayTime, AE.FromXrayTime, AE.ToSpecialtyTime, 
                      AE.SeenBySpecialtyTime, AE.EthnicCategoryCode, AE.ReferredToSpecialtyCode, AE.DecisionToAdmitTime, AE.DischargeDestinationCode, 
                      AE.RegisteredTime, AE.Attends, AE.WhereSeen, AE.EncounterDurationMinutes, AE.BreachReason, AE.AdmitToWard, AE.AdviceTime, AE.StandbyTime, 
                      AE.TrolleyDurationMinutes, AE.ManchesterTriageCode, AE.FFTextStatus, AE.ReferGP
FROM         WH.AE.Encounter AS AE LEFT OUTER JOIN
                      --WH.AE.ArrivalMode AS AM ON REPLACE(AE.ArrivalModeCode, ' ', '') = AM.ArrivalModeCode LEFT OUTER JOIN
                      WH.AE.AttendanceCategory AS AC ON AE.AttendanceCategoryCode = AC.AttendanceCategoryCode LEFT OUTER JOIN
                      WH.AE.AttendanceDisposal AS AD ON AE.AttendanceDisposalCode = AD.AttendanceDisposalCode LEFT OUTER JOIN
                      --WH.[CASCADE].AESource AS SR ON AE.SourceOfReferralCode = SR.code
                      WH.AE.SourceOfReferralLookup sorrf
						on sorrf.SourceOfReferralCode = AE.SourceOfReferralCode
WHERE     (AE.AttendanceCategoryCode IN ('1', '2'))