﻿CREATE VIEW [Prodacapo].[vw_CDIFF]
AS

SELECT TOP 100 PERCENT
Division,
Directorate,
Ward,
Cal.TheMonth,
Cal.FinancialMonthKey,

SUM(
CASE WHEN
CAST(SpecimenDate - [Date of Admission] AS INT) > 2
AND Exclude_Report = 'No' THEN 1 ELSE 0
END) AS [No. of CDiff]

FROM Prodacapo.CDIFF C
LEFT JOIN LK.Calendar Cal
ON C.SpecimenDate = Cal.TheDate

WHERE
CASE WHEN
CAST(SpecimenDate - [Date of Admission] AS INT) > 2
AND Exclude_Report = 'No' THEN 1 ELSE 0
END > 0
AND SpecimenDate >= '20130401'


GROUP BY
Division,
Directorate,
Ward,
Cal.TheMonth,
Cal.FinancialMonthKey
order by Cal.FinancialMonthKey