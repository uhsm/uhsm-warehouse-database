﻿CREATE VIEW [Prodacapo].[vw_DiagnosticWaits]
AS

SELECT TOP 100 PERCENT
LKDiag.Division, Cal.TheMonth, FinancialMonthKey, SUM(Breach) AS Breaches, SUM([Total Waiters]) AS TotalWaiters
FROM
Prodacapo.DiagnosticWaits Diag

LEFT JOIN Prodacapo.LKDiagnostics LKDiag
ON Diag.Test = LKDiag.Test

LEFT JOIN LK.Calendar Cal
ON Cal.TheDate = Diag.Month

GROUP BY
LKDiag.Division, Cal.TheMonth, FinancialMonthKey

order by Cal.FinancialMonthKey