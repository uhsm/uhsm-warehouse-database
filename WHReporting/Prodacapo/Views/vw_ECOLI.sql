﻿CREATE VIEW [Prodacapo].[vw_ECOLI]
AS

SELECT TOP 100 PERCENT

Pathway AS Division, 
Division AS Directorate, 
SpecimenTakenWard AS Ward, 
Cal.CalendarYear + ' ' + LEFT(TheMonth,3) AS [YearMonth], 
COUNT(1) [Count of ECOLI]
FROM
Prodacapo.ECOLI ECOLI
LEFT JOIN WHREPORTING.LK.Calendar Cal
ON ECOLI.SpecimenDate = Cal.TheDate
WHERE CommAcquiredConfirmed = 'Hospital'
GROUP BY Division, Pathway, SpecimenTakenWard, Cal.CalendarYear + ' ' + LEFT(TheMonth,3)