﻿CREATE VIEW [Prodacapo].[vw_MSSA]
AS

SELECT TOP 100 PERCENT

Pathway AS Division, 
Division AS Directorate, 
SpecimenTakenWard AS Ward, 
Cal.CalendarYear + ' ' + LEFT(TheMonth,3) AS [YearMonth], 
COUNT(1) [Count of MSSA]
FROM
Prodacapo.MSSA MSSA
LEFT JOIN WHREPORTING.LK.Calendar Cal
ON MSSA.SpecimenDate = Cal.TheDate
WHERE CommAcquiredConfirmed = 'Hospital'
GROUP BY Division, Pathway, SpecimenTakenWard, Cal.CalendarYear + ' ' + LEFT(TheMonth,3)