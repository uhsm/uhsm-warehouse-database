﻿CREATE VIEW [Prodacapo].[vw_NHSThermometer]
AS
SELECT     TOP (100) PERCENT CAL.TheMonth, CAL.FinancialMonthKey, Ward.Division, NHSThermometer.Ward, 95 AS TargetNHS, 
                      SUM(CASE WHEN [New PUs] <> '1' THEN 1 ELSE 0 END) + SUM(CASE WHEN [Falls] NOT IN ('1', '2') THEN '1' ELSE 0 END) 
                      + SUM(CASE WHEN [Catheter & UTI] = '3' THEN 1 ELSE 0 END) + SUM(CASE WHEN [VTE Type] IN ('5', '6', '7') THEN 1 ELSE 0 END) AS [New Harms], 
                      SUM(CASE WHEN [Number of Harms] <> '1' THEN 1 ELSE 0 END) AS [Total Harms], COUNT(*) AS [Total Patients Surveyed]
FROM         Thermometer.DataImport AS NHSThermometer LEFT OUTER JOIN
                      Thermometer.WardsToDivision AS Ward ON NHSThermometer.Ward = Ward.Ward LEFT OUTER JOIN
                      LK.Calendar AS CAL ON NHSThermometer.Date = CAL.TheDate
WHERE     (NHSThermometer.Date >= '20130401')
GROUP BY CAL.TheMonth, CAL.FinancialMonthKey, Ward.Division, NHSThermometer.Ward
ORDER BY CAL.FinancialMonthKey