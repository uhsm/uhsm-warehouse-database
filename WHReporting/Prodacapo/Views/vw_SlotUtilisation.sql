﻿CREATE View [Prodacapo].[vw_SlotUtilisation] AS

SELECT TOP 100 Percent

	OP.Division as Division,
	OP.Directorate as Directorate
	,OP.SpecialtyCode
	,OP.Specialty as Speciality
	
	,Sum(BookingsAllowed)                 AS BookingsAllowed,
       Sum(BookingsMade)                    AS BookingsMade,
       Sum(BookingsFree)                    AS BookingsFree,
       --CONVERT(CHAR(4), SessionDate, 100)
       --+ CONVERT(CHAR(4), SessionDate, 120) AS MonthYear
       Cal.CalendarYear + ' ' + LEFT(TheMonth,3) AS [YearMonth], 
       --Cal.TheMonth, 
       Cal.FinancialMonthKey
       
FROM   OP.SessionUtilisation OP


left join LK.Calendar Cal
ON Cal.TheDate = OP.SessionDate

WHERE  ( SessionDate >=  '01 Apr 2013' )
       AND ( SessionDate <= CONVERT(varchar,dateadd(d,-(day(getdate())),getdate()),106))
       AND ( SessionIsActive = 'Y' )
       AND ( IsCancelled = 'N' )
       AND ( ExcludedClinic = 'N' )
       AND ( NOT ( ClinicCode LIKE '%TELE%' ) )
       AND ( NOT ( ClinicDescription LIKE '%TELE%' ) )
       AND ( Division IN ( 'Clinical Support', 'Scheduled Care', 'Unscheduled Care' ) )
       AND ( SessionStatus IN ( 'Session Scheduled', 'Session Initiated', 'Session Held' ) )
       --AND ( Directorate IN ( @Directorate ) )
       --AND ( TreatmentFunctionCode IN ( @Specialty ) )
      
      GROUP  BY 
--CONVERT(CHAR(4), SessionDate, 100)
--          + CONVERT(CHAR(4), SessionDate, 120), 
          --Cal.TheMonth,
          Cal.CalendarYear + ' ' + LEFT(TheMonth,3),
          Cal.FinancialMonthKey, 
          Division, 
          Directorate,
          OP.SpecialtyCode, 
          Specialty
ORDER BY Specialty