﻿CREATE VIEW [Prodacapo].[vw_TrustInduction] AS
  
	  SELECT TOP 100 PERCENT
	  [Main Directorate]
	  ,[Competence Match]
	  ,[Competence Name]
	  ,Cal.TheMonth
	  ,Cal.FinancialYear
	  ,[Person Effective Start Date]
	  ,Cal.FinancialMonthKey
	  FROM [WHREPORTING].[Prodacapo].[TrustInduction] TI
	  LEFT JOIN LK.Calendar Cal
	  ON Cal.TheDate = TI.[Person Effective Start Date]
	  WHERE [Competence Name] = 'Induction'
	  ORDER BY Cal.FinancialMonthKey