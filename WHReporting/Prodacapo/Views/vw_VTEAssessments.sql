﻿CREATE VIEW [Prodacapo].[vw_VTEAssessments]

AS

select  top 100 percent COUNT(distinct SourceSpellNo) as Total,
Cal.CalendarYear + ' ' + LEFT(TheMonth,3) AS [YearMonth],
CAL.FinancialMonthKey,
AdmissionDivision,
VTESpecDesc,
[AdmissionSpecialtyCode(Function)],
AdmissionType,
Coded,
AdmissiontWardCode,
AdmissionWardName,

Case 
	when VTE = 1 then 'VTE'
	when DVT = 1 then 'DVT'
	when HD = 1 then 'HD'
	When Chemo = 1 then 'Chemo'
	when Endo = 1 then 'Endo'
	When DC = 1 then 'DC'
	When EL_NoProcAnd0LOS = 1 then 'EL No proc'
	when Ward = 1 then 'Ward'
	when NEL_NoProcOrMinorProcAnd0LOS = 1 then 'NEL No proc or minor proc'
	when ProcUnder90min = 1 then 'Under 90 mins'
else 'No Cohort' end as Cohort

from CorporateReports.VTE.SpellsWithAdmissionType S
LEFT JOIN WHREPORTING.LK.Calendar CAL
ON S.AdmissionDate = CAL.TheDate
where AdmissionDate >= '20130401' 
/*CASE statement below defaults to current financial year*/
--case
--        when month(getdate()) > 3 
--            then convert(datetime, cast(year(getdate()) as varchar) + '-4-1')
--        else 
--            convert(datetime, cast(year(getdate()) - 1 as varchar) + '-4-1')
--    end 
and (AdmissionDate is null or AdmissionDate < 
    case 
        when month(getdate()) > 3 
            then convert(datetime, cast(year(getdate()) + 1 as varchar) + '-4-1')
        else 
            convert(datetime, cast(year(getdate()) as varchar) + '-4-1')
    end)

group by 
Cal.CalendarYear + ' ' + LEFT(TheMonth,3),
CAL.FinancialMonthKey,
AdmissionDivision,
VTESpecDesc,
[AdmissionSpecialtyCode(Function)],
AdmissionType,
Coded,
AdmissiontWardCode,
AdmissionWardName,

Case 
	when VTE = 1 then 'VTE'
	when DVT = 1 then 'DVT'
	when HD = 1 then 'HD'
	When Chemo = 1 then 'Chemo'
	when Endo = 1 then 'Endo'
	When DC = 1 then 'DC'
	When EL_NoProcAnd0LOS = 1 then 'EL No proc'
	when Ward = 1 then 'Ward'
	when NEL_NoProcOrMinorProcAnd0LOS = 1 then 'NEL No proc or minor proc'
	when ProcUnder90min = 1 then 'Under 90 mins'
else 'No Cohort' end

ORDER BY CAL.FinancialMonthKey