﻿-- =============================================
-- Author:		CMan
-- Create date: 04/12/2014
-- Description:	Stored Procedure to create table to be pushed over the Development Team server for a replication of the Live Dashboard to be built on the Intranet by SFarrell
-- =============================================
CREATE PROCEDURE [QLIK].[BuildAEMeasuresTable]


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
--Create A&E temp table with the logic for determining most of the measures - i.e. no of current day arrivals, no currently in A&E , no of breaches etc., based on logic in the Qlikview Live dashboard.
--Temp table to then be used in the series of insert statements to populate a table for the developement team to use.	
If OBJECT_ID ('tempdb..#AE') > 0
DROP TABLE #AE

SELECT SourceUniqueID
	,CASE 
		WHEN ([CD].[ArrivalDate] = Cast(GETDATE() AS DATE))
			THEN 1
		ELSE 0
		END AS NoOfArrivals--This is the number of current day arrivals
	,CASE 
		WHEN CD.AttendanceConclusionTime IS NULL
			THEN 1
		ELSE 0
		END AS CurrentlyInAE-- this is the number of patients currently in A&E.  In the early hours of the morning, this may include patients who arrived previous day but are still in A&E
	,CASE 
		WHEN EncounterDurationMinutes > '240'
			THEN 1
		WHEN EncounterDurationMinutes IS NULL
			AND DateAdd(MINUTE, 240 + WHReporting.dbo.BSTAdjustment(ArrivalTime, AttendanceConclusionTime), ArrivalTime) < GETDATE()-- have taken this logic from some calculation in WHOLAP as this accounts for the March/October time changes
			THEN 1
		ELSE 0
		END AS NoOfBreaches--this is the number of those who have waited longer than 240 minutes and are still in A&E or the number who have left A&E and are a definitely a breach
	,CASE 
		WHEN CD.AttendanceConclusionTime IS NULL
			AND (CASE 
				WHEN EncounterDurationMinutes > '240'
					THEN 1
				WHEN EncounterDurationMinutes IS NULL
					AND DateAdd(MINUTE, 240 + WHReporting.dbo.BSTAdjustment(ArrivalTime, AttendanceConclusionTime), ArrivalTime) < GETDATE()
					THEN 1
				ELSE 0
				END) = 0--this section replicates the part in qlikview which determines whether or not the patient is a breach 
			AND DateDiff(minute, [CD].[ArrivalTime], GetDate()) > 225
			AND DateDiff(minute, [CD].[ArrivalTime], GetDate()) <240
			THEN 1
		ELSE 0
		END AS NoOfNearBreachWaiters--basically the logic for this is, where the attendance conclusion time is null, and where the patient is not a breach (as determined in the breach measure above), and the patient has been waiting between 225 and 240 minutes then these are the near breach waiters
	,CASE 
		WHEN CD.AttendanceConclusionTime IS NULL
			AND CD.InitialAssessmentTime IS NULL
			THEN 1
		ELSE 0
		END AS AwaitingTriage
	,CASE 
		WHEN CD.AttendanceConclusionTime IS NULL
			AND CD.InitialAssessmentTime IS NOT NULL
			AND CD.SeenForTreatmentTime IS NULL
			THEN 1
		ELSE 0
		END AS AwaitingSeen
	,CASE 
		WHEN CD.AttendanceConclusionTime IS NULL
			AND CD.ToSpecialtyTime IS NOT NULL
			AND CD.SeenBySpecialtyTime IS NULL
			THEN 1
		ELSE 0
		END AS AwaitingSpecialty
	,CASE 
		WHEN CD.AttendanceConclusionTime IS NULL
			AND CD.DecisionToAdmitTime IS NOT NULL
			THEN 1
		ELSE 0
		END AS AwaitingAdmission
	,DATEDIFF(MINUTE, CD.ArrivalTime, GETDATE()) AS CurrentTriageWait--current wait times, calculated so that it can be used to determine the maximum wait for those who are in the department still between arrival time and getdate- will be used in conjuction with the Awaiting Triage measure 
	,DATEDIFF(MINUTE, CD.InitialAssessmentTime, GETDATE()) AS CurrentSeenWait--current wait times, calculated so that it can be used to determine the maximum wait for those who are in the department still between Initial assessment time and getdate- will be used in conjuction with the Awaiting Seen measure
	,DATEDIFF(MINUTE, CD.ToSpecialtyTime, GETDATE()) AS CurrentSpecialtyWait--current wait times, calculated so that it can be used to determine the maximum wait for those who are in the department still between referred to specialty time and getdate- will be used in conjuction with the Awaiting Specialty measure
	,DATEDIFF(MINUTE, CD.DecisionToAdmitTime, GETDATE()) AS CurrentAdmissionWait--current wait times, calculated so that it can be used to determine the maximum wait for those who are in the department still between decision to admit time and getdate- will be used in conjuction with the Awaiting Admission measure
	,CD.ArrivalTime
	,CD.InitialAssessmentTime
	,CD.SeenForTreatmentTime
	,CD.ToSpecialtyTime
	,CD.SeenBySpecialtyTime
	,CD.DecisionToAdmitTime
	,CD.AttendanceConclusionTime
INTO #AE
FROM WH.AE.CurrentData CD
WHERE CD.[AttendanceCategoryCode] IN (
		'1'
		,'2'
		)
	AND ([CD].[ArrivalDate] = Cast(GETDATE() AS DATE) or [CD].[AttendanceConclusionTime] is null)
	;




TRUNCATE TABLE QLIK.AEMeasuresTable

--Number of Arrivals - current day
INSERT INTO QLIK.AEMeasuresTable
SELECT 'NoOfArrivals' AS Measure
	,cast(SUM(NoOfArrivals) as varchar)AS Value
	,NULL
	, 4 as [Order]
FROM #AE


--Number currently in A&E - in the early hours this may include counts of patients who arrived yesterday
INSERT INTO QLIK.AEMeasuresTable
SELECT 'CurrentlyInAE' AS Measure
	,cast(SUM(CurrentlyInAE) as varchar) AS Value
	,CASE 
		WHEN SUM(CurrentlyInAE) < 30
			THEN 'Green'
		WHEN SUM(CurrentlyInAE) >= 30
			AND SUM(CurrentlyInAE) < 60
			THEN 'Amber'
		WHEN SUM(CurrentlyInAE) >= 60
			THEN 'Red'
		ELSE NULL
		END AS RAG
		, 5 as [Order]
FROM #AE

--No of Breaches - includes those who have left the department and are a definite breach and those who are still in the department but the duration between arrival time and get date is > 240
INSERT INTO QLIK.AEMeasuresTable
SELECT 'NoOfBreaches' AS Measure
	,cast(SUM(NoOfBreaches) as varchar) AS Value
	,NULL
	, 2 as [Order]
FROM #AE
where DateAdd(day, DateDiff(day, 0,[ArrivalTime]) ,0) = Cast(GETDATE() AS DATE) 


--No of near breach waiters - number still in department, who are not a breach as determined above, and have been waiting for between 225 and 240 minutes
INSERT INTO QLIK.AEMeasuresTable
SELECT 'NoOfNearBreachWaiters' AS Measure
	,cast(SUM(NoOfNearBreachWaiters) as varchar) AS Value
	,NULL
	, 6 as [Order]
FROM #AE

--No currently waiting for Triage -  in the early hours this may include counts of patients who arrived yesterday
INSERT INTO QLIK.AEMeasuresTable
SELECT 'AwaitingTriage' AS Measure
	,cast(SUM(AwaitingTriage) as varchar) AS Value
	,NULL
	, 7 as [Order]
FROM #AE


--No currently waiting to be seen - in the early hours this may include counts of patients who arrived yesterday
INSERT INTO QLIK.AEMeasuresTable
SELECT 'AwaitingSeen' AS Measure
	,cast(SUM(AwaitingSeen) as varchar) AS Value
	,CASE 
		WHEN SUM(AwaitingSeen) > 20
			THEN 'Red'
		WHEN SUM(AwaitingSeen) > 5
			AND SUM(AwaitingSeen) < 21
			THEN 'Amber'
		WHEN SUM(AwaitingSeen) <= 5
			THEN 'Green'
		ELSE NULL
		END AS RAG
	, 8 as [Order]
FROM #AE

--No of patients who have been referred to specialty and awaiting to be seen by specialty  - in the early hours this may include counts of patients who arrived yesterday
INSERT INTO QLIK.AEMeasuresTable
SELECT 'AwaitingSpecialty' AS Measure
	,cast(SUM(AwaitingSpecialty) as varchar) AS Value
	,NULL
	,9 as [Order]
FROM #AE


--No of patients with decision to admit and still in department   - in the early hours this may include counts of patients who arrived yesterday

INSERT INTO QLIK.AEMeasuresTable
SELECT 'AwaitingAdmission' AS Measure
	,cast(SUM(AwaitingAdmission)  as varchar)AS Value
	,NULL
	, 10 as [Order]
FROM #AE


--Maximum Triage wait for those patients currently waiting for initial assessment
INSERT INTO QLIK.AEMeasuresTable
SELECT 'MaxTriageWaitTime' AS Measure
	,case when MAX(CurrentTriageWait) IS NULL then '-' Else  cast(MAX(CurrentTriageWait) as varchar) End AS Value
	,NULL
	,11 as [Order]
FROM #AE
WHERE AwaitingTriage = 1-- only for patients who are deemed to be waiting Triage


--Maximum Doctor wait for those patients currently waiting to be seen
INSERT INTO QLIK.AEMeasuresTable
SELECT 'MaxSeenWaitTime' AS Measure
	,case when MAX(CurrentSeenWait) IS NULL then '-' Else cast(MAX(CurrentSeenWait) as varchar) End AS Value
	,NULL
	,12 as [Order]
FROM #AE
WHERE AwaitingSeen = 1-- only for patients who are deemed to be waiting to be seen



--Maximum Specialty wait for those patients currently waiting to be seen by specialty
INSERT INTO QLIK.AEMeasuresTable
SELECT 'MaxSpecialtyWaitTime' AS Measure
	,case when MAX(CurrentSpecialtyWait) is NULL then '-' Else cast(MAX(CurrentSpecialtyWait) as varchar) End  AS Value
	,NULL
	,13 as [Order]
FROM #AE
WHERE AwaitingSpecialty = 1--only for patients who have been referred to specialty and still waiting


--Maximum Admission wait for those patients currently waiting to be admitted
INSERT INTO QLIK.AEMeasuresTable
SELECT 'MaxAdmissionWaitTime' AS Measure
	,case when MAX(CurrentAdmissionWait) IS NULL then '-' Else  cast(MAX(CurrentAdmissionWait) as varchar) End AS Value
	,NULL
	,14 as [Order]
FROM #AE
WHERE AwaitingAdmission = 1--only for patients who have a decision to admit and still in department


--4 hour performance for current day arrivals only
INSERT INTO QLIK.AEMeasuresTable
SELECT '4HourPerformance' AS Measure
	, cast(Round((SUM(NoOfArrivals) - SUM(NoOfBreaches)) / cast(SUM(NoOfArrivals) AS FLOAT) * 100 ,1) as varchar) AS Value
	,CASE 
		WHEN (SUM(NoOfArrivals) - SUM(NoOfBreaches)) / cast(SUM(NoOfArrivals) AS FLOAT) * 100 >= 95
			THEN 'Green'
		WHEN (SUM(NoOfArrivals) - SUM(NoOfBreaches)) / cast(SUM(NoOfArrivals) AS FLOAT) * 100 >= 93
			AND (SUM(NoOfArrivals) - SUM(NoOfBreaches)) / cast(SUM(NoOfArrivals) AS FLOAT) * 100 < 95
			THEN 'Amber'
		WHEN (SUM(NoOfArrivals) - SUM(NoOfBreaches)) / cast(SUM(NoOfArrivals) AS FLOAT) * 100 < 93
			THEN 'Red'
		ELSE NULL
		END AS RAG
	,1 as [Order]
FROM #AE
WHERE NoOfArrivals = 1--criteria for picking out the currentl day arrivals only

--A&E forecast number of attendances --taken from table which replicates the logic in excel spreadhseet which AAllcut highlighted.
INSERT INTO QLIK.AEMeasuresTable
SELECT 'ForecastAttendances' AS Measure
	,cast(Round(AllAttendForecast,0)  as varchar) AS Value
	,NULL
	,3 as [Order]
FROM WHREPORTING.QLIK.AEForecast
WHERE AllAttendForecast IS NOT NULL
	AND [Date] = DateAdd(Day, DATEDIFF(day, 0, GetDate()), 0);


--Elective Admissions
INSERT INTO QLIK.AEMeasuresTable
SELECT 'ElectiveAdmissions' AS Measure
	,cast(COUNT(DISTINCT SourceSpellID) as varchar)  Value
	,NULL
	,19 as [Order]
FROM UHSMApplications.LPI.ADTData adt
WHERE DateAdd(Day, DateDiff(Day, 0, adt.AdmissionDateTime), 0) = DateAdd(Day, DateDiff(Day, 0, GETDATE()), 0)
	AND adt.EventType = 'Admission'
	AND adt.AdmissionType = 'EL';

--NonElective Admissions
INSERT INTO QLIK.AEMeasuresTable
SELECT 'NonElectiveAdmissions' AS Measure
	,cast(COUNT(DISTINCT SourceSpellID) as varchar) Value
	,NULL
	,20 as [Order]
--Select *  
FROM UHSMApplications.LPI.ADTData adt
WHERE DateAdd(Day, DateDiff(Day, 0, adt.AdmissionDateTime), 0) = DateAdd(Day, DateDiff(Day, 0, GETDATE()), 0)
	AND adt.EventType = 'Admission'
	AND adt.AdmissionType IN (
		'EM'
		,'NE'
		);

--Elective Discharges
INSERT INTO QLIK.AEMeasuresTable
SELECT 'ElectiveDischarges' AS Measure
	,cast(COUNT(DISTINCT adt.SourceSpellID) as varchar) Value
	,NULL
	,21 as [Order]
FROM UHSMApplications.LPI.ADTData adt -- base data with discharges from last 3 days
LEFT JOIN (
	SELECT DISTINCT ADTData.SourceSpellID
		,ADTData.Location
	FROM UHSMApplications.LPI.ADTData -- details of the previous ward has to come from the original table 
	INNER JOIN (
		--grab the max AlternativeID of the event prior to the discharge lounge event from the LPI - after joining on RowNo-1 from first subquery query to RowNo of the second subquery
		SELECT dl.SourceSpellID
			,MAX(prev_ward.AlternativeWardID) AS previous_ward_id
		FROM (
			--assign row number to the distinct rows of events in the LPI data 
			SELECT ROW_NUMBER() OVER (
					PARTITION BY SourceSpellID ORDER BY AlternativeWardID
					) AS RowNo
				,*
			FROM (
				--get a distinct row of events from the LPI data  
				SELECT DISTINCT SourceSpellID
					,AdmissionDateTime
					,DischargeDateTime
					,AssignedPatientLocation
					,AlternativeWardID
					,Location
					,Specialty
				FROM UHSMApplications.LPI.ADTData
				WHERE AssignedPatientLocation = 'DL'
					AND EventType <> 'Discharge'
				) a
			) AS dl --Subquery to get a distinct of sourcespellid and the different ward transfers with row count
		LEFT JOIN (
			SELECT ROW_NUMBER() OVER (
					PARTITION BY SourceSpellID ORDER BY AlternativeWardID
					) AS RowNo
				,*
			FROM (
				SELECT DISTINCT SourceSpellID
					,AdmissionDateTime
					,DischargeDateTime
					,AssignedPatientLocation
					,AlternativeWardID
					,Location
					,Specialty
				FROM UHSMApplications.LPI.ADTData
				WHERE AssignedPatientLocation = 'DL'
					AND EventType <> 'Discharge'
				) a
			) AS prev_ward --repeat of the above subquery with a join as per the fowllowing to get the row number of the discharge lounge event from the LPI joined with the last row no (row no -1) of the transfer event prior to the discharge lounge event 
			ON dl.SourceSpellID = prev_ward.SourceSpellID
			AND dl.RowNo - 1 = prev_ward.RowNo
		GROUP BY dl.SourceSpellID
		) AS prev_ward_detail ON ADTData.SourceSpellID = prev_ward_detail.SourceSpellID
		AND ADTData.AlternativeWardID = prev_ward_detail.previous_ward_id
	WHERE AssignedPatientLocation = 'DL'
	) pw ON adt.SourceSpellID = pw.SourceSpellID
WHERE DateAdd(Day, DateDiff(Day, 0, adt.DischargeDateTime), 0) = DateAdd(Day, DateDiff(Day, 0, GETDATE()), 0)
	AND adt.EventType = 'Discharge'
	AND adt.AdmissionType = 'EL'

--NonElective Discharges
INSERT INTO QLIK.AEMeasuresTable
SELECT 'NonElectiveDischarges' AS Measure
	,cast(COUNT(DISTINCT adt.SourceSpellID)  as varchar) Value
	,NULL
	,22 as [Order]
FROM UHSMApplications.LPI.ADTData adt -- base data with discharges from last 3 days
LEFT JOIN (
	SELECT DISTINCT ADTData.SourceSpellID
		,ADTData.Location
	FROM UHSMApplications.LPI.ADTData -- details of the previous ward has to come from the original table 
	INNER JOIN (
		--grab the max AlternativeID of the event prior to the discharge lounge event from the LPI - after joining on RowNo-1 from first subquery query to RowNo of the second subquery
		SELECT dl.SourceSpellID
			,MAX(prev_ward.AlternativeWardID) AS previous_ward_id
		FROM (
			--assign row number to the distinct rows of events in the LPI data 
			SELECT ROW_NUMBER() OVER (
					PARTITION BY SourceSpellID ORDER BY AlternativeWardID
					) AS RowNo
				,*
			FROM (
				--get a distinct row of events from the LPI data  
				SELECT DISTINCT SourceSpellID
					,AdmissionDateTime
					,DischargeDateTime
					,AssignedPatientLocation
					,AlternativeWardID
					,Location
					,Specialty
				FROM UHSMApplications.LPI.ADTData
				WHERE AssignedPatientLocation = 'DL'
					AND EventType <> 'Discharge'
				) a
			) AS dl --Subquery to get a distinct of sourcespellid and the different ward transfers with row count
		LEFT JOIN (
			SELECT ROW_NUMBER() OVER (
					PARTITION BY SourceSpellID ORDER BY AlternativeWardID
					) AS RowNo
				,*
			FROM (
				SELECT DISTINCT SourceSpellID
					,AdmissionDateTime
					,DischargeDateTime
					,AssignedPatientLocation
					,AlternativeWardID
					,Location
					,Specialty
				FROM UHSMApplications.LPI.ADTData
				WHERE AssignedPatientLocation = 'DL'
					AND EventType <> 'Discharge'
				) a
			) AS prev_ward --repeat of the above subquery with a join as per the fowllowing to get the row number of the discharge lounge event from the LPI joined with the last row no (row no -1) of the transfer event prior to the discharge lounge event 
			ON dl.SourceSpellID = prev_ward.SourceSpellID
			AND dl.RowNo - 1 = prev_ward.RowNo
		GROUP BY dl.SourceSpellID
		) AS prev_ward_detail ON ADTData.SourceSpellID = prev_ward_detail.SourceSpellID
		AND ADTData.AlternativeWardID = prev_ward_detail.previous_ward_id
	WHERE AssignedPatientLocation = 'DL'
	) pw ON adt.SourceSpellID = pw.SourceSpellID
WHERE DateAdd(Day, DateDiff(Day, 0, adt.DischargeDateTime), 0) = DateAdd(Day, DateDiff(Day, 0, GETDATE()), 0)
	AND adt.EventType = 'Discharge'
	AND adt.AdmissionType IN (
		'NE'
		,'EM'
		)

--Median Triage Wait- Patients who have been triaged already - what has been the median wait for them
INSERT INTO QLIK.AEMeasuresTable
SELECT 'MedianTriageWait' AS Measure
	,case when AVG(1.0 * TriageMinutes) is null then '-' Else cast(cast(AVG(1.0 * TriageMinutes)  as decimal(16,1)) as varchar)  End AS Value
	,CASE 
		WHEN AVG(1.0 * TriageMinutes) > 15
			THEN 'Red'
		WHEN AVG(1.0 * TriageMinutes) > 8
			AND AVG(1.0 * TriageMinutes) <= 15
			THEN 'Amber'
		WHEN AVG(1.0 * TriageMinutes) <= 8
			THEN 'Green'
		WHEN AVG(1.0 * TriageMinutes) is null
		    THEN 'Green'
		ELSE NULL
		END AS RAG
	,15 as [Order]
FROM (
	SELECT DATEDIFF(MINUTE, ArrivalTime, InitialAssessmentTime) TriageMinutes
		,row_number() OVER (
			ORDER BY DATEDIFF(MINUTE, ArrivalTime, InitialAssessmentTime)
			) AS rownum
		,count(*) OVER () AS numrows
	FROM #AE
	WHERE NoOfArrivals = 1
		AND InitialAssessmentTime IS NOT NULL
	) AS x
WHERE rownum IN (
		(numrows + 1) / 2
		,(numrows + 2) / 2
		)

--Median Doctor Wait- Patients who have been seen already - what has been the median seen wait for them
INSERT INTO QLIK.AEMeasuresTable
SELECT 'MedianDoctorWait' AS Measure
	,case when  AVG(1.0 * DoctorMinutes)  is null then '-' Else cast(cast(AVG(1.0 * DoctorMinutes) as decimal(16,1)) as varchar) End AS Value
	,CASE 
		WHEN AVG(1.0 * DoctorMinutes) > 60
			THEN 'Red'
		WHEN AVG(1.0 * DoctorMinutes) > 45
			AND AVG(1.0 * DoctorMinutes) <= 60
			THEN 'Amber'
		WHEN AVG(1.0 * DoctorMinutes) <= 45
			THEN 'Green'
		WHEN AVG(1.0 * DoctorMinutes) is null
		    THEN 'Green'
		ELSE NULL
		END AS RAG
	,16 as [Order]
FROM (
	SELECT DATEDIFF(MINUTE, ArrivalTime, SeenForTreatmentTime) DoctorMinutes
		,row_number() OVER (
			ORDER BY DATEDIFF(MINUTE, ArrivalTime, SeenForTreatmentTime)
			) AS rownum
		,count(*) OVER () AS numrows
	FROM #AE
	WHERE NoOfArrivals = 1
		AND SeenForTreatmentTime IS NOT NULL
	) AS x
WHERE rownum IN (
		(numrows + 1) / 2
		,(numrows + 2) / 2
		)

--Median Specialty Wait - Patients who have been referred to  and seen by specialty already - what has been the median wait for them
INSERT INTO QLIK.AEMeasuresTable
SELECT 'MedianSpecialtyWait' AS Measure
	,case when AVG(1.0 * SpecialtyMinutes) is NULL then '-' Else cast(cast(AVG(1.0 * SpecialtyMinutes) as decimal(16,1)) as varchar) End AS Value
	,CASE 
		WHEN AVG(1.0 * SpecialtyMinutes) >= 45
			THEN 'Red'
		WHEN AVG(1.0 * SpecialtyMinutes) > 30
			AND AVG(1.0 * SpecialtyMinutes) < 45
			THEN 'Amber'
		WHEN AVG(1.0 * SpecialtyMinutes) <= 30
			THEN 'Green'
		WHEN AVG(1.0 * SpecialtyMinutes) is null
		    THEN 'Green'
		ELSE NULL
		END AS RAG
	,17 as [Order]
FROM (
	SELECT DATEDIFF(MINUTE, ToSpecialtyTime, SeenBySpecialtyTime) SpecialtyMinutes
		,row_number() OVER (
			ORDER BY DATEDIFF(MINUTE, ToSpecialtyTime, SeenBySpecialtyTime)
			) AS rownum
		,count(*) OVER () AS numrows
	FROM #AE
	WHERE NoOfArrivals = 1
		AND ToSpecialtyTime IS NOT NULL
		AND SeenBySpecialtyTime IS NOT NULL
	) AS x
WHERE rownum IN (
		(numrows + 1) / 2
		,(numrows + 2) / 2
		)

--Median Admission Wait - Patients who have a decision to admit time and an attendance conclusion time - what has been the median wait for them
INSERT INTO QLIK.AEMeasuresTable
SELECT 'MedianAdmissionWait' AS Measure
	,case when AVG(1.0 * AdmissionWaitMinutes) is null then '-' Else  cast(Cast(AVG(1.0 * AdmissionWaitMinutes) as decimal(16,1)) as varchar) End AS Value
	,CASE 
		WHEN AVG(1.0 * AdmissionWaitMinutes) > 60
			THEN 'Red'
		WHEN AVG(1.0 * AdmissionWaitMinutes) > 30
			AND AVG(1.0 * AdmissionWaitMinutes) < 61
			THEN 'Amber'
		WHEN AVG(1.0 * AdmissionWaitMinutes) <= 30
			THEN 'Green'
		WHEN AVG(1.0 * AdmissionWaitMinutes) is null
		    THEN 'Green'
		ELSE NULL
		END AS RAG
		,18 as [Order]
FROM (
	SELECT DATEDIFF(MINUTE, DecisionToAdmitTime, AttendanceConclusionTime) AdmissionWaitMinutes
		,row_number() OVER (
			ORDER BY DATEDIFF(MINUTE, DecisionToAdmitTime, AttendanceConclusionTime)
			) AS rownum
		,count(*) OVER () AS numrows
	FROM #AE
	WHERE NoOfArrivals = 1
		AND DecisionToAdmitTime IS NOT NULL
		AND AttendanceConclusionTime IS NOT NULL
	) AS x
WHERE rownum IN (
		(numrows + 1) / 2
		,(numrows + 2) / 2
		)


--Set RAG rating here for NonElective discharges as it needs to be where the Nonelective discharge is lower than non elective admission then 'Red'
UPDATE QLIK.AEMeasuresTable
SET RAG = CASE 
		WHEN Value < (
				SELECT Value
				FROM QLIK.AEMeasuresTable
				WHERE Measure = 'NonElectiveAdmissions'
				)
			THEN 'Red'
		ELSE NULL
		END
FROM QLIK.AEMeasuresTable
WHERE Measure = 'NonElectiveDischarges'



--Set RAG rating here for Elective discharges as it needs to be where the elective discharge is lower than elective admission then 'Red'
UPDATE QLIK.AEMeasuresTable
SET RAG = CASE 
		WHEN Value < (
				SELECT Value
				FROM QLIK.AEMeasuresTable
				WHERE Measure = 'ElectiveAdmissions'
				)
			THEN 'Red'
		ELSE NULL
		END
FROM QLIK.AEMeasuresTable
WHERE Measure = 'ElectiveDischarges'

END