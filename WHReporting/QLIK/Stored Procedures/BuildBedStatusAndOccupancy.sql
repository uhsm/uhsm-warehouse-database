﻿-- =============================================
-- Author:		CMan
-- Create date: 10/04/2014
-- Description:	Stored Procedure to create tables detailing Bed status at midnight, Bed Occupancy at midnight and Episode wardstay matching to support Qlikview reporting of LOS and Bed Occupancy etc.
-- =============================================
CREATE PROCEDURE [QLIK].[BuildBedStatusAndOccupancy]
	-- Add the parameters for the stored procedure here
AS
BEGIN

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

/************************************************************
 Create a BED status table taking the min census date - which should be the midnight extraction of the smartboards information
Joins A10, A8 and AMU bed counts together, and also C2 and CC2 Bed counts together
**************************************************************/
Truncate table  WHReporting.QLIK.BedStatus
Insert into WHReporting.QLIK.BedStatus
Select Dateadd(day, 0, Datediff(day, 0, bs.CensusDate)) [Date],
		bs.CensusDate,
		bs.TimeFlag,
		 CASE
         WHEN bs.WardCode IN ( 'A10', 'A8', 'AMU' ) THEN 'AMU'
         WHEN bs.WardCode IN ( 'CC2', 'C2' ) THEN 'C2'
         ELSE bs.WardCode
       END                 AS WardCode,
        CASE
         WHEN bs.Wardcode IN ( 'A10', 'A8', 'AMU' ) THEN 'AMU'
         WHEN bs.WardCode IN ( 'CC2', 'C2' ) THEN 'C2'
         ELSE bs.WardName
         END as WardName,
        Sum(bs.SBOpenBeds)  AS SBOpenBeds,
       Sum(bs.SBTotalBeds) AS SBTotalBeds,
       SUM(bs.OccupiedOnLPI) as OccupiedOnLPI
FROM   UHSMApplications.SB.BedStatus bs
WHERE  bs.CensusDate >= '20140101'
Group by Dateadd(day, 0, Datediff(day, 0, bs.CensusDate)),
		bs.Censusdate,
		bs.TimeFlag,
          CASE
            WHEN bs.WardCode IN ( 'A10', 'A8', 'AMU' ) THEN 'AMU'
            WHEN bs.WardCode IN ( 'CC2', 'C2' ) THEN 'C2'
            ELSE bs.WardCode
          END,
          CASE
            WHEN bs.Wardcode IN ( 'A10', 'A8', 'AMU' ) THEN 'AMU'
            WHEN bs.WardCode IN ( 'CC2', 'C2' ) THEN 'C2'
            ELSE bs.WardName
          END

ORDER  BY Dateadd(day, 0, Datediff(day, 0, bs.CensusDate))





/**************************************************
 Create a day by day wardstay table to allow for a count of patients in a bed at midnight and 11
 **************************************************/

--Initial table to join wards stays to the calendar table to get a day by day row for each wardstay
If OBJECT_ID('tempdb..#WS')>0
DROP TABLE #WS

SELECT DISTINCT Calendar.TheDate,
                WardStay.SourceUniqueID,
		  WardStay.SourceSpellNo,
		  WardStay.SourcePatientNo,
		  WardStay.StartTime,
		  ISNULL(WardStay.Endtime,GETDATE()) Endtime ,
		  WardStay.WardCode,
		  WardStay.PASCreated,
		  WardStay.PasUpdated 
INTO #WS
FROM    WHReporting.APC.WardStay
       LEFT OUTER JOIN  WHReporting.LK.Calendar
                    ON Calendar.TheDate >= Dateadd(day, 0, Datediff(day, 0, WardStay.StartTime))
                       AND Calendar.TheDate < ISNULL(WardStay.EndTime, GETDATE())
WHERE  ISNULL(EndTime, GETDATE()) >= '20120401'--starting from 01/01/2014 as a cut off for all smartboards being in use on all neccesary wards.









/* CM  01/05/2014 no longer need this next sectiopn as PN and Newton have suggested couting at different times therefore re-built a bed occupancy table with flags to indicate in bed at midnight, in bed at 11 am etc

--final table to disccount any patients who may have been admitted on a particular day but weren't actually in the bed as at midnight

Truncate table  WHReporting.QLIK.BedOccupancyAtMidnight
Insert into  WHReporting.QLIK.BedOccupancyAtMidnight

SELECT #WS.TheDate as [Date] ,
		#WS.SourceUniqueID as WardStayUniqueID,
		#WS.SourceSpellNo as SourceSpellNo,
		#WS.SourcePatientNo as SourcePatientNo,
		#WS.StartTime as WardstayStart,
		#WS.Endtime as WardstayEnd,
		#WS.WardCode,
		#WS.PasUpdated 

FROM   #WS
WHERE  CASE
         WHEN #WS.TheDate = Dateadd(day, 0, Datediff(day, 0, #WS.StartTime))
              AND #WS.StartTime <> #WS.TheDate THEN NULL
         ELSE #WS.TheDate
       END IS NOT NULL -- add this bit of logic in to discount any patients which were not actually in a bed at midnight
--and sourceuniqueid in ('150739939','10026838','10026727')

*/

-------------
-- Bed Occupancy table without link to the Episode Specialty, Directorate and Division at time of wardstay
-------------
TRUNCATE TABLE WHReporting.QLIK.BedOccupancy

INSERT INTO WHReporting.QLIK.BedOccupancy

SELECT #WS.TheDate                    AS [Date],
       #WS.SourceUniqueID             AS WardStayUniqueID,
       #WS.SourceSpellNo              AS SourceSpellNo,
       #WS.SourcePatientNo            AS SourcePatientNo,
       #WS.StartTime                  AS WardstayStart,
       #WS.EndTime                    AS WardstayEnd,
       #WS.WardCode,
       Dateadd(hour, 11, #WS.TheDate) AS Date11,
       CASE
         WHEN #WS.StartTime <= #WS.TheDate
              AND #WS.EndTime > #WS.TheDate THEN 1
         ELSE 0
       END                            AS MidnightFlag,
       CASE
         WHEN #WS.Starttime <= Dateadd(hour, 11, #WS.TheDate)
              AND #WS.EndTime > Dateadd(hour, 11, #WS.TheDate) THEN 1
         ELSE 0
       END                            AS [11amFlag],
       #WS.PasUpdated

FROM   #WS 


---------------------------
--Build Second Bed Occupancy table to link ward stay to an episode to get the specialty, directorate and divison at the time of the stay
----------------------------

--Create temp table from the #WS temp table above to get the beds occupied view as a midnight
If OBJECT_ID('tempdb..#mid')>0
Drop table #mid
SELECT #WS.TheDate                    AS [Date],
       #WS.SourceUniqueID             AS WardStayUniqueID,
       #WS.SourceSpellNo              AS SourceSpellNo,
       #WS.SourcePatientNo            AS SourcePatientNo,
       #WS.StartTime                  AS WardstayStart,
       #WS.EndTime                    AS WardstayEnd,
       #WS.WardCode,
       Dateadd(hour, 11, #WS.TheDate) AS Date11,
		1 AS MidnightFlag,
		0 AS [11amFlag],
       --CASE
       --  WHEN #WS.Starttime <= Dateadd(hour, 11, #WS.TheDate)
       --       AND #WS.EndTime > Dateadd(hour, 11, #WS.TheDate) THEN 1
       --  ELSE 0
       --END                            AS [11amFlag],
       #WS.PasUpdated
into  #mid
FROM   #WS 
where #WS.StartTime <= #WS.TheDate
              AND #WS.EndTime > #WS.TheDate
              
--Create temp table from the #WS temp table above to get the beds occupied view as a 11 am
If OBJECT_ID('tempdb..#ele')>0
Drop table #ele
SELECT #WS.TheDate                    AS [Date],
       #WS.SourceUniqueID             AS WardStayUniqueID,
       #WS.SourceSpellNo              AS SourceSpellNo,
       #WS.SourcePatientNo            AS SourcePatientNo,
       #WS.StartTime                  AS WardstayStart,
       #WS.EndTime                    AS WardstayEnd,
       #WS.WardCode,
       Dateadd(hour, 11, #WS.TheDate) AS Date11,
		0 AS MidnightFlag,
		1 AS [11amFlag],
       --CASE
       --  WHEN #WS.Starttime <= Dateadd(hour, 11, #WS.TheDate)
       --       AND #WS.EndTime > Dateadd(hour, 11, #WS.TheDate) THEN 1
       --  ELSE 0
       --END                            AS [11amFlag],
       #WS.PasUpdated
into  #ele
FROM   #WS 
where #WS.Starttime <= Dateadd(hour, 11, #WS.TheDate)
        AND #WS.EndTime > Dateadd(hour, 11, #WS.TheDate)



--Final part of the build which links the midnight view temp table to episodes to get episode spec and division at midnight
--and the 11 am view temp table to episodes to get the spec and division at 11 am
--Join birth together to get overall table BedOccupancy2

Truncate table WHReporting.QLIK.BedOccupancy2
Insert into WHReporting.QLIK.BedOccupancy2
Select *
from (
Select distinct  mid.*, ep.EpisodeStartDateTime, ep.EpisodeEndDateTime , ep.Directorate, ep.Division,ep.Specialty, ep.ConsultantName, ep.[Specialty(Main)], ep.AdmissionType
from #mid mid
left outer join WHREPORTING.APC.Episode ep on mid.sourcespellno = ep.SourceSpellNo and ep.EpisodeStartDateTime<= mid.[Date] and isnull(ep.EpisodeEndDateTime, GETDATE()) > mid.[Date]
where mid.Date >= '20120401'
union all 
Select distinct ele.*, ep.EpisodeStartDateTime, ep.EpisodeEndDateTime , ep.Directorate, ep.Division,ep.Specialty, ep.ConsultantName, ep.[Specialty(Main)], ep.AdmissionType
from #ele ele
left outer join WHREPORTING.APC.Episode ep on ele.sourcespellno = ep.SourceSpellNo and ep.EpisodeStartDateTime<= ele.Date11 and isnull(ep.EpisodeEndDateTime, GETDATE()) > ele.Date11
where ele.Date >= '20120401'
) total




--/**************************************************
-- Create a day by day wardstay table to allow for a count of patients in a bed at 11 am  - as per PN Request
-- **************************************************/

----Initial table to join wards stays to the calendar table to get a day by day row for each wardstay
--If OBJECT_ID('tempdb..#WS11')>0
--DROP TABLE #WS11

--SELECT DISTINCT Calendar.TheDate,
----DateAdd(hour,11,Calendar.TheDate) as At11,
--                WardStay.SourceUniqueID,
--		  WardStay.SourceSpellNo,
--		  WardStay.SourcePatientNo,
--		  WardStay.StartTime,
--		  ISNULL(WardStay.Endtime,GETDATE()) Endtime ,
--		  WardStay.WardCode,
--		  WardStay.PASCreated,
--		  WardStay.PasUpdated 
--INTO #WS11
--FROM    WHReporting.APC.WardStay
--       LEFT OUTER JOIN  WHReporting.LK.Calendar
--                    ON DateAdd(hour,11,Calendar.TheDate) >= WardStay.StartTime--Dateadd(day, 0, Datediff(day, 0, WardStay.StartTime))
--                       AND DateAdd(hour,11,Calendar.TheDate) < ISNULL(WardStay.EndTime, GETDATE())
--WHERE  ISNULL(EndTime, GETDATE()) >= '20140101'--starting from 01/01/2014 as a cut off for all smartboards being in use on all neccesary wards.


----final table to disccount any patients who may have been admitted on a particular day but weren't actually in the bed as at midnight


--Truncate table  WHReporting.QLIK.BedOccupancyAt11
--Insert into  WHReporting.QLIK.BedOccupancyAt11


--SELECT distinct #WS11.TheDate as [Date] ,
--		#WS11.SourceUniqueID as WardStayUniqueID,
--		#WS11.SourceSpellNo as SourceSpellNo,
--		#WS11.SourcePatientNo as SourcePatientNo,
--		#WS11.StartTime as WardstayStart,
--		#WS11.Endtime as WardstayEnd,
--		#WS11.WardCode,
--		#WS11.PasUpdated 
--FROM   #WS11
--WHERE TheDate is NOT NULL






/********************************************
Create EpisodeWardStartEndtable
This table joins episodes to wardstays to create a table which details the correct ward for each part of the episode based on whether the ward start date was first or the ward end date was first
**********************************************/
Truncate Table    WHReporting.QLIK.EpisodeWardMatch  
Insert into  WHReporting.QLIK.EpisodeWardMatch  

--SELECT DISTINCT EP.SourceSpellNo,
--                EP.EpisodeUniqueID,
--                WS.SourceUniqueID as WardStayUniqueID,
--                EP.EpisodeStartDateTime,
--                EP.EpisodeEndDateTime,
--                WS.WardCode,
--                WS.StartTime             AS WardStartTime,
--                WS.EndTime               AS WardEndTime,
--                COALESCE(CASE
--                           WHEN WS.StartTime >= EP.EpisodeStartDateTime
--                                AND WS.EndTime <= EP.EpisodeEndDateTime THEN WS.StartTime
--                           ELSE NULL
--                         END
--                , CASE
--                    WHEN WS.StartTime >= EP.EpisodeStartDateTime
--                         AND WS.StartTime < EP.EpisodeEndDateTime
--                         AND WS.EndTime > EP.EpisodeEndDateTime THEN WS.StartTime
--                    ELSE NULL
--                  END
--                , CASE
--                    WHEN WS.StartTime < EP.EpisodeStartDateTime
--                         AND WS.EndTime > EP.EpisodeStartDateTime
--                         AND WS.EndTime < EP.EpisodeEndDateTime THEN EP.EpisodeStartDateTime
--                  END)                   AS StartDateTime,
--                COALESCE (CASE
--                            WHEN ws.StartTime >= EP.EpisodeStartDateTime
--                                 AND WS.EndTime < EP.EpisodeEndDateTime THEN WS.EndTime
--                          END, CASE
--                                 WHEN Ws.EndTime > EP.EpisodeStartDateTime
--                                      AND ws.EndTime < EP.EpisodeEndDateTime THEN WS.EndTime
--                               END, CASE
--                                      WHEN WS.StartTime >= EP.EpisodeStartDateTime
--                                           AND WS.StartTime < EP.EpisodeEndDateTime
--                                           AND WS.EndTime >= EP.EpisodeEndDateTime THEN EP.EpisodeEndDateTime
--                                    END) AS EndDateTime
--FROM   WHREPORTING.APC.Episode EP
--       LEFT OUTER JOIN WHReporting.APC.WardStay WS
--                    ON EP.SourceSpellNo = WS.SourceSpellNo
--WHERE  --EP.SourceSpellNo = '150508994'
--  --and 
--  COALESCE(CASE
--             WHEN WS.StartTime >= EP.EpisodeStartDateTime
--                  AND WS.EndTime <= EP.EpisodeEndDateTime THEN WS.StartTime
--             ELSE NULL
--           END
--  , CASE
--      WHEN WS.StartTime >= EP.EpisodeStartDateTime
--           AND WS.StartTime < EP.EpisodeEndDateTime
--           AND WS.EndTime > EP.EpisodeEndDateTime THEN WS.StartTime
--      ELSE NULL
--    END
--  , CASE
--      WHEN WS.StartTime < EP.EpisodeStartDateTime
--           AND WS.EndTime > EP.EpisodeStartDateTime
--           AND WS.EndTime < EP.EpisodeEndDateTime THEN EP.EpisodeStartDateTime
--    END) IS NOT NULL

--03/05/2014 CM Amended the above script to the one below to take into account of episodes that hav not ended.
SELECT DISTINCT EP.SourceSpellNo,
                EP.EpisodeUniqueID,
                WS.SourceUniqueID as WardStayUniqueID,
                EP.EpisodeStartDateTime,
                EP.EpisodeEndDateTime,
                WS.WardCode,
                WS.StartTime             AS WardStartTime,
                WS.EndTime               AS WardEndTime,
                COALESCE(CASE
                           WHEN WS.StartTime >= EP.EpisodeStartDateTime
                                AND ISNULL(WS.EndTime,getdate()) <= isnull(EP.EpisodeEndDateTime,getdate()) THEN WS.StartTime
                           ELSE NULL
                         END
                , CASE
                    WHEN WS.StartTime >= EP.EpisodeStartDateTime
                         AND WS.StartTime <  isnull(EP.EpisodeEndDateTime,getdate()) 
                         AND ISNULL(WS.EndTime,getdate()) > isnull(EP.EpisodeEndDateTime,getdate()) THEN WS.StartTime
                    ELSE NULL
                  END
                , CASE
                    WHEN WS.StartTime < EP.EpisodeStartDateTime
                         AND ISNULL(WS.EndTime,getdate()) > EP.EpisodeStartDateTime
                         AND ISNULL(WS.EndTime,getdate()) <=  isnull(EP.EpisodeEndDateTime,getdate())  THEN EP.EpisodeStartDateTime
                  END)                   AS StartDateTime,
                COALESCE (CASE
                            WHEN ws.StartTime >= EP.EpisodeStartDateTime
                                 AND ISNULL(WS.EndTime,getdate()) <  isnull(EP.EpisodeEndDateTime,getdate()) THEN WS.EndTime
                          END, CASE
                                 WHEN ISNULL(WS.EndTime,getdate()) > EP.EpisodeStartDateTime
                                      AND ISNULL(WS.EndTime,getdate()) <  isnull(EP.EpisodeEndDateTime,getdate())  THEN WS.EndTime
                               END, CASE
                                      WHEN WS.StartTime >= EP.EpisodeStartDateTime
                                           AND WS.StartTime <  isnull(EP.EpisodeEndDateTime,getdate()) 
                                           AND ISNULL(WS.EndTime,getdate()) >=  isnull(EP.EpisodeEndDateTime,getdate())  THEN  isnull(EP.EpisodeEndDateTime,getdate()) 
                                    END) AS EndDateTime
FROM   WHREPORTING.APC.Episode EP
       LEFT OUTER JOIN WHReporting.APC.WardStay WS
                    ON EP.SourceSpellNo = WS.SourceSpellNo
WHERE  --EP.SourceSpellNo = '150508994'
  --and 
  COALESCE(CASE
             WHEN WS.StartTime >= EP.EpisodeStartDateTime
                  AND ISNULL(WS.EndTime,getdate()) <=  isnull(EP.EpisodeEndDateTime,getdate())  THEN WS.StartTime
             ELSE NULL
           END
  , CASE
      WHEN WS.StartTime >= EP.EpisodeStartDateTime
           AND WS.StartTime <  isnull(EP.EpisodeEndDateTime,getdate()) 
           AND ISNULL(WS.EndTime,getdate()) >  isnull(EP.EpisodeEndDateTime,getdate())  THEN WS.StartTime
      ELSE NULL
    END
  , CASE
      WHEN WS.StartTime < EP.EpisodeStartDateTime
           AND ISNULL(WS.EndTime,getdate()) > EP.EpisodeStartDateTime
           AND ISNULL(WS.EndTime,getdate()) <=  isnull(EP.EpisodeEndDateTime,getdate())  THEN EP.EpisodeStartDateTime
    END) IS NOT NULL


select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 'Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'Qlik - WHREPORTING BuildBedStatusAndOccupancy', @Stats, @StartTime

END