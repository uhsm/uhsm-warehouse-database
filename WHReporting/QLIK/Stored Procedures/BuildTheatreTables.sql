﻿-- =============================================
-- Author:		CMan
-- Create date: 05/08/2014
-- Description:	Stored procedure to build logic and table required for the Theatres Daschboard
-- =============================================
CREATE PROCEDURE [QLIK].[BuildTheatreTables]
	-- Add the parameters for the stored procedure here



AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()



--Create session table to work from
If OBJECT_ID('WHReporting.QLIK.TheatreSessions') >0 
Drop table WHReporting.QLIK.TheatreSessions
Create Table WHReporting.QLIK.TheatreSessions
			(SessionSourceUniqueID int,
		ActualSessionMinutes int,
		PlannedSessionMinutes int,
		SessionStartTime datetime,
		SessionEndTime datetime,
	--	InSessionStart datetime,
	--InSessionEnd datetime,
		AdjustedSessionStart datetime,
		AdjustedSessionend datetime,
		AdjustedPlannedSessionMinutes int,
		SessionNumber int,
		SessionTheatreCode int,
		SessionTheatre varchar(100),
		SessionOperatingSuite varchar(100),
		SessionConsultantCode int,
		SessionConsultant varchar(100),
		SessionSpecialtyCode int,
		SessionSpecialty varchar(100),
		SessionDivision varchar(100),
		SessionDirectorate varchar(100),
		SessionAnaesthetistCode1 int,
		SessionAnaesthetist varchar(100),
		SessionTimetableDetailCode int,
		SessionType varchar(100),
		SessionTheatreType varchar(100),
		SessionDayOfWeek varchar(20),
		SessionCancelledFlag bit,	
		SessionOverrunReason varchar(255),
		SessionListCount float,
		ActualStartTime datetime, -- as per what is recorded in OperationDetails table based on what gets decided as the start and end time - this is now to be Anaesthetic Induction to Operation End Time + Recovery : Recovery to be the time between OperationEndTime to InRecoveryTime except where this is more than 15 minutes in duration in which case it is capped at 15 minutes 
		ActualEndTime datetime	,	-- as per what is recorded in OperationDetails table based on what gets decided as the start and end time - this is now to be Anaesthetic Induction to Operation End Time + Recovery : Recovery to be the time between OperationEndTime to InRecoveryTime except where this is more than 15 minutes in duration in which case it is capped at 15 minutes
		EarlyStartFlag int,--based on 5 min tolerance
		LateStartFlag int,--based on 5 min tolerance
		EarlyFinishFlag int,--based on 5 min tolerance
		LateFinishFlag int,--based on 5 min tolerance
		EarlyStartMinutes int,
		LateStartMinutes int,
		EarlyFinishMinutes int,
		LateFinishMinutes int,
		StandardisedSessionListCount float,
		EarlyStart15Flag int,--23/07/2015 CM added this field to assist Helen Ransome with reports for BUnwin.  Current Early/Late start and Early/Late Finish Flags will only get set based on a 5 min tolerance.  Bob would like a report showing the counts based on 15 min tolerance
		LateStart15Flag int,--23/07/2015 CM added this field to assist Helen Ransome with reports for BUnwin.  Current Early/Late start and Early/Late Finish Flags will only get set based on a 5 min tolerance.  Bob would like a report showing the counts based on 15 min tolerance
		EarlyFinish15Flag int, --23/07/2015 CM added this field to assist Helen Ransome with reports for BUnwin.  Current Early/Late start and Early/Late Finish Flags will only get set based on a 5 min tolerance.  Bob would like a report showing the counts based on 15 min tolerance
		LateFinish15Flag int,--23/07/2015 CM added this field to assist Helen Ransome with reports for BUnwin.  Current Early/Late start and Early/Late Finish Flags will only get set based on a 5 min tolerance.  Bob would like a report showing the counts based on 15 min tolerance
		ORMISSessionTheatreType varchar(20)

		)
		
		
--Insert data into theatre sessions table
Insert into WHREPORTING.QLIK.TheatreSessions
(SessionSourceUniqueID,
		ActualSessionMinutes,-- actual minutes as recorded in session table
		PlannedSessionMinutes,
		SessionStartTime,
		SessionEndTime,
		SessionNumber,
		SessionTheatreCode,
		SessionTheatre,
		SessionOperatingSuite,
		SessionConsultantCode,
		SessionConsultant,
		SessionSpecialtyCode,
		SessionSpecialty,
		SessionDivision,
		SessionDirectorate,
		SessionAnaesthetistCode1,
		SessionAnaesthetist,
		SessionTimetableDetailCode,
		SessionType,
		SessionTheatreType,
		SessionDayOfWeek,
		SessionCancelledFlag,	
		SessionOverrunReason,
		StandardisedSessionListCount,
		ORMISSessionTheatreType)
Select distinct sess.SourceUniqueID as SessionSourceUniqueID,
		sess.ActualSessionMinutes,-- actual minutes as recorded in session table
		sess.PlannedSessionMinutes,
		sess.StartTime,
		sess.EndTime,
		sess.SessionNumber,
		sess.TheatreCode,
		th.Theatre,
		opsuite.OperatingSuite,
		sess.ConsultantCode,
		cons.Forename + ' '+ cons.Surname as Consultant,
		case when sess.TheatreCode in ('27','29')  then '501' 
			when sess.SpecialtyCode  in ('501','502') and sess.TheatreCode not in ('27','29') then '502'
			when sess.SpecialtyCode  in ('190') then '191'
		Else sess.SpecialtyCode End as SpecialtyCode ,--24/02/2015 Added logic to reassign Obstetrics and Gynaecology sessions correctly based on discussions with CBH and SM in meeting on 11/02/2015: When session is in Delivery Suite 1 or 2 then specialty is 'Obstetric', when specialty of session consultant is Obstetrics or Gynaecology and the session theatre is not Delivery suites then the specialty is 'Gynaecology'
														--24/02/2015 CM: Also add logic to reassign Anaesthetics to Pain Management 
		case when sess.TheatreCode in ('27','29')  then 'Obstetrics' 
			when ISNull(spec.Specialty,'Unknown') in ('Obstetrics','Gynaecology') and sess.TheatreCode not in ('27','29') then 'Gynaecology'
			when ISNull(spec.Specialty,'Unknown') in ('Anaesthetics') then 'Pain Management'
			Else ISNull(spec.Specialty,'Unknown') End as Specialty,--24/02/2015 CM: Added logic to reassign Obstetrics and Gynaecology sessions correctly based on discussions with CBH and SM in meeting on 11/02/2015: When session is in Delivery Suite 1 or 2 then specialty is 'Obstetric', when specialty of session consultant is Obstetrics or Gynaecology and the session theatre is not Delivery suites then the specialty is 'Gynaecology'
																		--24/02/2015 CM: Also add logic to reassign Anaesthetics to Pain Management 
		ISNull(sd.Division, 'Unknown') Division,
		ISNull(sd.Direcorate,'Unknown') Directorate,
		sess.AnaesthetistCode1,
		anaes.Forename + ' '+ anaes.Surname as Anaesthetist, 
		sess.TimetableDetailCode,
		case	
		--when sess.PlannedSessionMinutes >= 48 then 'Triple'
		when sess.PlannedSessionMinutes >= 480  then 'AllDay'
		when datepart(hour,sess.StartTime) < 6
		then 'Night'
		when datepart(hour,sess.StartTime) >= 16
		then 'Evening'
		when datepart(hour,sess.StartTime) >= 12
		then 'Afternoon'
		else 'Morning'
		end as SessionType,--logic here is taken from stored procedure WHReporting.TH.ReportTheatreSessionSummaryReportByConsultant - logic as previously aggreed to determine session type.  Re-check with Andrea Myerscough
		Case when sess.TheatreCode in ('1','2','3','78','37','38','39','40','13','15','19','21','14','16','23','92',--'25', CM 12/01/2016 : Remove Virtual Theatre FBLock from Elective to stop it from appearing on Booking and Scheduling Tool as per temephone conversation with WNickerson
										'57', '75'--Baguley Pain Clinic and Baguley Pain Clinic 1--added these in as per SManns email from 06/10/2015
										,'81', '82'--Radiology Interventional 1 and 2 - CM 12/10/2015 added these as all lost are now booking into these theatres as opposed to Radiology - as per conversation and email from JWaldron 
										,'108', '109')--Added hybrid theatres TDC4 and TDC5 as per WNickerson email 09/05/2016
										 
										then 'Elective'
			when sess.TheatreCode in ('79') and sess.StartTime < '20150901' then 'Elective'--RADIOLOGY - CM 12/10/2015 - this theatre is no longer used - as per email from SManns
			/* Theatre Code 20 - TDC1 logic added here as all Elective work from TDC 1 was moved to TDC2 from 21/9/2015-31/10/2015, and TDC1 only carried out Trauma work - as per WNickerson 
			- 03/11/2015 CM Amended TDC1 changes to end from 01/11/2015 to 01/12/2015 as per WNickerson email */							
			when sess.TheatreCode in ('20') and sess.StartTime <'20150921' then 'Elective'
			when sess.TheatreCode in ('20') and sess.StartTime >= '20150921' and sess.StartTime < '20160201' then 'Emergency'
			when sess.TheatreCode in ('20') and sess.StartTime >= '20160201' then 'Elective'--27/01/2016 CM Amended as per WNickerson email 26/01/2016  
			--when sess.TheatreCode in ('20') and sess.StartTime >= '20151101' then 'Elective'--16/12/2015 CM commented this out as TDC1 is to continur being Trauma theatre until  we hear otherwise - as per conversation with WNickerson
			
			when sess.TheatreCode  = '6' and cal.DayOfWeek not in ('Saturday','Sunday')  then 'Elective'
			when sess.TheatreCode  = '6' and cal.DayOfWeek  in ('Saturday','Sunday')  then 'Emergency'
			
		
			when sess.TheatreCode = '22' and sess.StartTime < '20150406' and sess.StartTime < '20150921' and cal.DayOfWeek = 'Monday' and 	datepart(hour,sess.StartTime) < 12 and  datepart(hour,sess.StartTime) >= 6 then 'Elective' --TDC2 Monday morning sessions only to be included as Elective . 12/05/2015 CM amended this to only be session from before 6th APril 2015.  After this date, all TDC2 elective sessions have moved to F Block - as per SManns email
			when sess.TheatreCode = '22' and sess.StartTime < '20150406' and sess.StartTime < '20150921'  and cal.DayOfWeek = 'Monday' and 	datepart(hour,sess.StartTime) >= 12 then 'Emergency'
			when sess.TheatreCode = '22' and sess.StartTime < '20150406' and sess.StartTime < '20150921'  and cal.DayOfWeek = 'Monday' and   datepart(hour,sess.StartTime) < 6 then 'Emergency'
			when sess.TheatreCode = '22' and sess.StartTime >= '20150921' and sess.StartTime < '20160201' then 'Elective' --CM 12/10/2015 Amended so the booking and scheduling tool shows TDC2 sessions between these dates as Elective, as TDC1 work was moved to TDC2 during this period - as per email from WNickerson.- 03/11/2015 CM Amended this from 01/11/2015 to 01/12/2015 as per WNickerson email  - 16/12/2015 CM Amended end date to  01/02/2016 as per WNickerson email
			when sess.TheatreCode = '22' and sess.StartTime >= '20160229' then 'Elective' --07/03/2016 CM Added this change in as per WNickerson email re:further changes to TDC
			when cal.DayOfWeek in ('Saturday','Sunday') and sess.SessionType = 'WLI' then 'Elective'--03/02/2015 added this criteria to include weekend sessions which are to be counted as an Elective -  logic based on SManns email
			when sess.TheatreCode in ('4','5','31','22') then 'Emergency'
			when sess.TheatreCode in ('47','76', '77') then 'DayCase'
			when sess.TheatreCode in ('27','29') then 'Maternity'
		Else 'Other'
		End as SessionTheatreType ,
		cal.DayOfWeek ,
		sess.CancelledFlag,	
		sess.OverrunReason,
		sess.PlannedSessionMinutes/cast(240 as float) as StandardisedSessionListCount,
		Sess.SessionType--07/03/2016 CM Added this field to help with the identification of Trauma and Emergency session on the Booking and scheduling tool, as per WNickerson phone call
		
from WH.Theatre.Session sess
Left outer join WH.Theatre.Staff cons on sess.ConsultantCode = cons.StaffCode
Left outer join WH.Theatre.Staff anaes on sess.AnaesthetistCode1 = anaes.StaffCode
Left outer join WH.Theatre.Specialty spec on sess.SpecialtyCode = spec.specialtycode
Left outer join WH.Theatre.Theatre th on sess.TheatreCode = th.TheatreCode
Left outer join WH.Theatre.OperatingSuite opsuite on th.OperatingSuiteCode = opsuite.OperatingSuiteCode
Left outer join WHREPORTING.LK.SpecialtyDivision sd on spec.SpecialtyCode1 = sd.SpecialtyCode
Left outer join WHREPORTING.LK.Calendar cal on dateadd(day, datediff(day, 0, sess.StartTime), 0) = cal.TheDate



Select @RowsInserted = @@ROWCOUNT
select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'Qlik - WHREPORTING TheatreSessions', @Stats, @StartTime


--Update WHReporting.QLIK.TheatreSessions with a count of lists based on the logic that if it is a morning,afternoon, evening or night session then count as 1 list each, if it is an all day session then it could be 2, 2.5 or 2.75 lists depending on whether planned session minutes is 480 mins, > 480 mins and <= 600 mins or >600 mins
--Logic was discussed and agreed with AM, CBH and SM on 11/07/2014
--Logic is different to that used by NS in hhis report
--Newton logic just looks at 1 or 2 lists 
Update  WHREPORTING.QLIK.TheatreSessions
Set SessionListCount  = --case when SessionType in ('Triple') then 3
					--	when SessionType in ('Double') then 2
			--Else 1 End
	case when SessionType = 'AllDay' and PlannedSessionMinutes = 480 then 2
		when SessionType = 'AllDay' and PlannedSessionMinutes > 480 and PlannedSessionMinutes <=600 then 2.5 
		when SessionType = 'AllDay' and PlannedSessionMinutes > 600 then 2.75
		Else 1
		End





--Get Temp table of all the operations with the datetime stamps required to be used to link back to the table above
--Note:  there seems to be some issue where some operations have a recorded specilty which is different to the session specialty.
--For purposes of the Qlikview report, we will link the operations to the session using session unique id field and use the specialty, consultant, other details etc of the session, as we want to measure session utilisation/productivity
--this may result in some operations being missed as there are some recorded with a sessionsourceuniqueid of 0 
If OBJECT_ID('tempdb..#OPDetails') >0
Drop table #OPDetails
Select distinct  opdet.DistrictNo,
		opdet.SourceUniqueID,
		opdet.OperationDate,
		opdet.OperationStartDate,
		opdet.OperationEndDate,
		opdet.TheatreCode,
		th.Theatre,
		opsuite.OperatingSuite,
		opdet.OperationTypeCode,
		opdet.AdmissionTypeCode,
		at.AdmissionType,
		opdet.SpecialtyCode,
		spec.Specialty,
		opdet.ConsultantCode,
		cons.Forename + ' '+ cons.Surname as Consultant,
		opdet.Anaesthetist1Code,
		anaes.Forename + ' '+ anaes.Surname as Anaesthetist,
		opdet.OperationOrder as SystemOriginalOperationOrder,
		ROW_NUMBER ( ) OVER (PARTITION BY SessionSourceUniqueID, operationdate ,opdet.TheatreCode Order by OperationStartDate) as OperationOrder,
		opdet.PatientBookingSourceUniqueID,
		opdet.OperationCancelledFlag,
		opdet.SessionSourceUniqueID,
		opdet.InSuiteTime, 
		opdet.InAnaestheticTime,
		opdet.AnaestheticInductionTime,
		opdet.AnaestheticReadyTime,
		--pdet.ProcedureStartTime, 
		--pdet.ProcedureEndTime,
		min(pdet.ProcedureStartTime) as ProcedureStartTime,--need to use min and max for procedure end time as there are instances where the opdetails matches to more than one entry in the procedure details table both of which have a primary procedure flag of 1 e.g. operationdetailsourceuniqueid : 20623
		max(pdet.ProcedureEndTime) as ProcedureEndTime, 
		--pdet.ProcedureCode,
		opdet.InRecoveryTime, 
		DateDiff(Minute,ISNULL(opdet.OperationEndDate, opdet.InRecoveryTime), ISNULL(opdet.InRecoveryTime,opdet.OperationEndDate)) as ActualOpEndToRecovery,--18/09/2014 CM Added an ISNULL around the Inrecovery time as there are some records which do not have a recovery time recorded
																																							--18/09/2014 CM Also Added ISNULL around OperationEndDate to assign the Procedure End where the OP end is null
		Case when DateDiff(Minute,ISNULL(opdet.OperationEndDate,opdet.InRecoveryTime), ISNULL(opdet.InRecoveryTime,opdet.OperationEndDate))> 15 then '15'
			when  DateDiff(Minute,ISNULL(opdet.OperationEndDate, opdet.InRecoveryTime), ISNULL(opdet.InRecoveryTime,opdet.OperationEndDate))< 0 then '0'
			Else DateDiff(Minute,ISNULL(opdet.OperationEndDate, opdet.InRecoveryTime), ISNULL(opdet.InRecoveryTime,opdet.OperationEndDate)) End 
		as AdjustedOpEndToRecovery,--this calculation is to help determine an adjusted actual session end where it include a period of recovery after the OperationEndTime - this recovery has ben agrreed by AM,PN and CBH to be the time between OperationEndTime to InRecoveryTime except where this is more than 15 minutes in duration in which case it is capped at 15 minutes
		DateAdd(minute, Case when DateDiff(Minute,ISNULL(opdet.OperationEndDate, opdet.InRecoveryTime), ISNULL(opdet.InRecoveryTime,opdet.OperationEndDate))> 15 then '15'
			when  DateDiff(Minute,ISNULL(opdet.OperationEndDate, opdet.InRecoveryTime), ISNULL(opdet.InRecoveryTime,opdet.OperationEndDate))< 0 then '0'
			Else DateDiff(Minute,ISNULL(opdet.OperationEndDate, opdet.InRecoveryTime), ISNULL(opdet.InRecoveryTime,opdet.OperationEndDate)) End ,ISNULL(opdet.OperationEndDate, opdet.InRecoveryTime)) as AdjustedOperationEndDate,
		DATEDIFF(MINUTE, opdet.AnaestheticInductionTime, DateAdd(minute, Case when DateDiff(Minute,ISNULL(opdet.OperationEndDate, opdet.InRecoveryTime), ISNULL(opdet.InRecoveryTime,opdet.OperationEndDate))> 15 then '15'
			when  DateDiff(Minute,ISNULL(opdet.OperationEndDate, opdet.InRecoveryTime), ISNULL(opdet.InRecoveryTime,opdet.OperationEndDate))< 0 then '0'
			Else DateDiff(Minute,ISNULL(opdet.OperationEndDate, opdet.InRecoveryTime), ISNULL(opdet.InRecoveryTime,opdet.OperationEndDate)) End ,ISNULL(opdet.OperationEndDate, opdet.InRecoveryTime))) as UtilisedMinutes

into #OpDetails
from WH.Theatre.OperationDetail opdet
Left outer join (Select * from WH.Theatre.ProcedureDetail where PrimaryProcedureFlag = 1)pdet on opdet.SourceUniqueID = pdet.OperationDetailSourceUniqueID
Left outer join WH.Theatre.Staff cons on opdet.ConsultantCode = cons.StaffCode
Left outer join WH.Theatre.Staff anaes on opdet.Anaesthetist1Code = anaes.StaffCode 
Left outer join WH.Theatre.Specialty spec on opdet.SpecialtyCode = spec.SpecialtyCode
Left outer join WH.Theatre.Theatre th on opdet.TheatreCode = th.TheatreCode
Left outer join WH.Theatre.OperatingSuite opsuite on th.OperatingSuiteCode = opsuite.OperatingSuiteCode
LEFT outer join WH.Theatre.AdmissionType at on opdet.AdmissionTypeCode = at.AdmissionTypeCode

Group by  opdet.DistrictNo,
		opdet.SourceUniqueID,
		opdet.OperationDate,
		opdet.OperationStartDate,
		opdet.OperationEndDate,
		opdet.TheatreCode,
		th.Theatre,
		opsuite.OperatingSuite,
		opdet.OperationTypeCode,
		opdet.AdmissionTypeCode,
		at.AdmissionType,
		opdet.SpecialtyCode,
		spec.Specialty,
		opdet.ConsultantCode,
		cons.Forename + ' '+ cons.Surname,
		opdet.Anaesthetist1Code,
		anaes.Forename + ' '+ anaes.Surname,
		opdet.OperationOrder,
		opdet.PatientBookingSourceUniqueID,
		opdet.OperationCancelledFlag,
		opdet.SessionSourceUniqueID,
		opdet.InSuiteTime, 
		opdet.InAnaestheticTime,
		opdet.AnaestheticInductionTime,
		opdet.AnaestheticReadyTime,
		opdet.InRecoveryTime, 
		DateDiff(Minute,ISNULL(opdet.OperationEndDate, opdet.InRecoveryTime),ISNULL(opdet.InRecoveryTime,opdet.OperationEndDate)),
		Case when DateDiff(Minute,ISNULL(opdet.OperationEndDate, opdet.InRecoveryTime), ISNULL(opdet.InRecoveryTime,opdet.OperationEndDate))> 15 then '15'
			when  DateDiff(Minute,ISNULL(opdet.OperationEndDate, opdet.InRecoveryTime), ISNULL(opdet.InRecoveryTime,opdet.OperationEndDate))< 0 then '0'
			Else DateDiff(Minute,ISNULL(opdet.OperationEndDate, opdet.InRecoveryTime), ISNULL(opdet.InRecoveryTime,opdet.OperationEndDate)) End ,--this calculation is to help determine an adjusted actual session end where it include a period of recovery after the OperationEndTime - this recovery has ben agrreed by AM,PN and CBH to be the time between OperationEndTime to InRecoveryTime except where this is more than 15 minutes in duration in which case it is capped at 15 minutes
		DateAdd(minute, Case when DateDiff(Minute,ISNULL(opdet.OperationEndDate, opdet.InRecoveryTime), ISNULL(opdet.InRecoveryTime,opdet.OperationEndDate))> 15 then '15'
			when  DateDiff(Minute,ISNULL(opdet.OperationEndDate,opdet.InRecoveryTime),ISNULL(opdet.InRecoveryTime,opdet.OperationEndDate))< 0 then '0'
			Else DateDiff(Minute,ISNULL(opdet.OperationEndDate,opdet.InRecoveryTime),ISNULL(opdet.InRecoveryTime,opdet.OperationEndDate)) End ,ISNULL(opdet.OperationEndDate, pdet.ProcedureEndTime)),
		DATEDIFF(MINUTE, opdet.AnaestheticInductionTime, ISNULL(opdet.OperationEndDate, opdet.InRecoveryTime))

--Select * from #OpDetails
--where sourceuniqueid = '170347'


--Create temp table linking #OpDetails back to the session table to get the actual start and end times of a session
--AnaestheticInduction of 1st op in session and AdjustedOperationEndDate (this is adjusted to include capped recovery) of last op in session is to be used for for actual session start and en - as per discussion with PN , AM and CBH
If OBJECT_ID('tempdb..#actuals') > 0
Drop table #actuals
--Select ts.SessionSourceUniqueID, MIN(od.AnaestheticInductionTime) as ActualStart, MAX(od.OperationEndDate) as ActualEnd--Max(od.AdjustedOperationEndDate) As ActualEnd --CM 10/12/2014 Amended this so that the late finish flag is flagged as anything where the max operation end time (rather than max adjusted operation end )is after the planned session end and late finish minutes get calculated as number of minutes the max opereration end time is after the planned session end.
--into #actuals
--from WHREPORTING.QLIK.TheatreSessions ts
--left outer join #OpDetails  od on ts.SessionSourceUniqueID = od.sessionsourceuniqueid
----where ts.SessionSourceUniqueID = '177162'
--Group by ts.SessionSourceUniqueID

Select ts.SessionSourceUniqueID, MIN(od.AnaestheticInductionTime) as ActualStart, MaxOp.OperationEndDate as ActualEnd--Max(od.AdjustedOperationEndDate) As ActualEnd --CM 10/12/2014 Amended this so that the late finish flag is flagged as anything where the max operation end time (rather than max adjusted operation end )is after the planned session end and late finish minutes get calculated as number of minutes the max opereration end time is after the planned session end.
into #actuals
from WHREPORTING.QLIK.TheatreSessions ts
left outer join #OpDetails  od on ts.SessionSourceUniqueID = od.sessionsourceuniqueid
Left outer join (SELECT #OPDetails.SessionSourceUniqueID
					,#OPDetails.OperationEndDate
				FROM #OPDetails
				INNER JOIN (
					SELECT SessionSourceUniqueID
						,Max(OperationOrder) AS MaxOpOrder
					FROM #OpDetails
					GROUP BY SessionSourceUniqueId
					) a ON #OpDetails.SessionSourceUniqueID = a.SEssionSourceuniqueID
					AND #OpDetails.OperationOrder = a.MaxOpOrder
)as MaxOp--to get the operation end date of the last op as the max(operationEndTime) is not neccesarily alway the operation end time of the last op due to data quality error in inputting
On ts.SessionSourceUniqueID = MaxOp.SessionSourceUniqueID
Group by ts.SessionSourceUniqueID, MaxOp.OperationEndDate




--Update the WHREPORTING.QLIK.TheatreSessions table with actual start and end times from the #actuals temp table above
Update WHREPORTING.QLIK.TheatreSessions
set ActualStartTime = ActualStart,
ActualEndTime = ActualEnd
from WHREPORTING.QLIK.TheatreSessions ts
left outer join #actuals   on ts.SessionSourceUniqueID = #actuals.sessionsourceuniqueid



--Update  WHREPORTING.QLIK.TheatreSessions with EarlyStart, LateStart, EarlyFinish, LateFinish flags- these are based on 5 min tolerance
----23/07/2015 CM added additional EarlyStart15Flag, LateStart15Flag, EarlyFinish15Flag and LateFinish15Flag to be set based on 15 minute tolerance tohelp HRansome with report which Bob Unwin has requested
Update WHREPORTING.QLIK.TheatreSessions
Set EarlyStartFlag = case when ActualStartTime < DATEADD(MINUTE, -5,SessionStartTime) then '1' Else '0' End,
	LateStartFlag = case when ActualStartTime > DATEADD(MINUTE, 5,SessionStartTime) then '1' Else '0' End ,
   EarlyFinishFlag = case when ActualEndTime < DATEADD(MINUTE, -5,SessionEndTime)then '1' Else '0' End ,
LateFinishFlag = case when ActualEndTime >  DATEADD(MINUTE, 5,SessionEndTime)then '1' Else '0' End,
EarlyStart15Flag = case when ActualStartTime < DATEADD(MINUTE, -15,SessionStartTime) then '1' Else '0' End,
	LateStart15Flag = case when ActualStartTime > DATEADD(MINUTE, 15,SessionStartTime) then '1' Else '0' End ,
   EarlyFinish15Flag = case when ActualEndTime < DATEADD(MINUTE, -15,SessionEndTime)then '1' Else '0' End ,
LateFinish15Flag = case when ActualEndTime >  DATEADD(MINUTE, 15,SessionEndTime)then '1' Else '0' End
from WHREPORTING.QLIK.TheatreSessions


--Update  WHREPORTING.QLIK.TheatreSessions with Early Start Minutes 
Update WHREPORTING.QLIK.TheatreSessions
Set EarlyStartMinutes = DATEDIFF(MINUTE,ActualStartTime , SessionStartTime)
from WHREPORTING.QLIK.TheatreSessions
where EarlyStartFlag = 1

--Update  WHREPORTING.QLIK.TheatreSessions with Late Start Minutes 
Update WHREPORTING.QLIK.TheatreSessions
Set LateStartMinutes = DATEDIFF(MINUTE, SessionStartTime, ActualStartTime)
from WHREPORTING.QLIK.TheatreSessions
where LateStartFlag = 1	


--Update  WHREPORTING.QLIK.TheatreSessions with Early Finish Minutes 
Update WHREPORTING.QLIK.TheatreSessions
Set EarlyFinishMinutes = DATEDIFF(MINUTE,ActualEndTime , SessionEndTime)
from WHREPORTING.QLIK.TheatreSessions
where EarlyFinishFlag = 1

--Update  WHREPORTING.QLIK.TheatreSessions with Late finish Minutes 
Update WHREPORTING.QLIK.TheatreSessions
Set LateFinishMinutes = DATEDIFF(MINUTE, SessionEndTime, ActualEndTime)
from WHREPORTING.QLIK.TheatreSessions
where LatefinishFlag = 1	

	
		
		
/*Update WHReporting.QLIK.TheatreSessions table with InSessionStart and InSessionEnd, and AdjustedSessionStart, AdjustedSessionEnd  and AdjustedSessionPlannedMinutes to help with the calculation of the different productivity measures
--InSessionStart is the adjusted actual start time based on the InAnaestheticTime of the first op on the session- as calculated above. 
--InSessionEnd is the adjusted actual end time based on the InRecoveryTime of the last op in the session - as calculated above
The above measures have been adjusted so that if the actual start of the session (InAnaestheticTime of the first op) is before the planned start then the InSessionStart is taken to be the SessionStartTime(Planned start of the session),
similarly if the actual end of the session (InRecoveryTime of the last op) is after the SessionEndTime (planned end of the session) then the InSessionEnd is taken to be the SessionEndTime
this will help with the calculation of utilised minutes in the OpDetails table below to help with the derivation of the InSessionProductivity.  The idea is that only productive time within the  planned session is measured.
--AdjustedSessionStart - this detailed the adjusted planned start time to  take into account early starts and late finishes.  If the actual start of the session  (InAnaestheticTime of the first op) is before the planned start of the session then the AdjustedStartTime is the ActualStart 
--AdjustedSessionEnd  - If the actual end of the session (InRecoveryTime of the last op) is after the planned end of the session then the AdjustedEndTime is the ActualEnd.
the idea of the AdjustedSessionStart and the AdjustedSessionEnd is so that in the overall Productivity measure, any early start and late finishes is included when calculating the denominator - which will be the field AdjustedSessionPlannedMinutes below .  This is as per Newton logic.
--AdjustedSessionPlannedMinutes - calculated as the nummber of minutes between the two adjusted fields above.
*/
Update WHREPORTING.QLIK.TheatreSessions
Set 	--InSessionStart = CASE 
		--WHEN ActualStartTime < SessionStartTime
		--	THEN SessionStartTime
		--ELSE ActualStartTime
		--END 
		--,InSessionEnd = CASE 
		--WHEN ActualEndTime > SessionEndTime
		--	THEN SessionEndTime
		--ELSE ActualEndTime
		--END
	AdjustedSessionStart = Case when ActualStartTime < SessionStartTime then ActualStartTime Else SessionStartTime end
	,AdjustedSessionEnd = Case when ActualEndTime > SessionEndTime then ActualEndTime Else SessionEndTime end
	, AdjustedPlannedSessionMinutes = DATEDIFF(MINUTE,Case when ActualStartTime < SessionStartTime then ActualStartTime Else SessionStartTime end
	,Case when ActualEndTime > SessionEndTime then ActualEndTime Else SessionEndTime end )

FROM WHREPORTING.QLIK.TheatreSessions






---Determine turnaround time for each operation within the session
If OBJECT_ID('tempdb..#Turn') >0
Drop table #Turn
SELECT a.*, 
case when a.SessionSourceUniqueId = 0  then NULL Else b.AnaestheticInductionTime End as NextAnaestheticInductionTime,
       --b.AnaestheticReadyTime                                       AS NestOpAnaestheticReadyTime,
       case when a.SessionSourceUniqueId = 0 then 0--all ops which are not linked to a session ID have had turaround set to 0
			when Datediff(MINUTE, a.AdjustedOperationEndDate, b.AnaestheticInductionTime)  < 0 then 0--this sets all those ops where the next anaesthetic induction started before adjusted op end to 0
			Else  Datediff(MINUTE, a.AdjustedOperationEndDate, b.AnaestheticInductionTime) End AS TurnaroundTime
into #Turn
FROM   #OPDetails a
       LEFT OUTER JOIN #OPDetails b
                    ON a.OperationDate = b.OperationDate
                       AND a.SessionSourceUniqueID = b.SessionSourceUniqueID
                       AND a.OperationOrder+1 = b.OperationOrder 
                       and a.TheatreCode = b.TheatreCode
                       

                       



--Create an actual Operation Details table with procedures details, turnaround times, untilised minutes etc to work from and insert the data from the #Turn temp table.
If OBJECT_ID('WHReporting.QLIK.TheatreOpDetails') >0 
Drop table WHReporting.QLIK.TheatreOpDetails

Create Table WHReporting.QLIK.TheatreOpDetails
(SourceUniqueID int,
DistrictNo varchar(20),
OperationDate datetime,
OperationStartDate datetime,
OperationEndDate datetime,
OperationTheatreCode int,
OperationTheatre varchar(100),
OperationOperatingSuite varchar(100),
OperationTypeCode varchar(100),
OperationAdmissionTypeCode varchar(5),
OperationAdmissionType varchar (50),
OperationSpecialtyCode int,
OperationSpecialty varchar(100),
OperationConsultantCode int,
OperationConsultant  varchar(100),
OperationAnaesthetist1Code int,
OperationAnaesthetist  varchar(100),
SystemOriginalOperationOrder int,
OperationOrder int,
PatientBookingSourceUniqueID  int,
OperationCancelledFlag bit,
SessionSourceUniqueID int,
InSuiteTime datetime,
InAnaestheticTime datetime,
AnaestheticInductionTime datetime,
AnaestheticReadyTime datetime,
ProcedureStartTime datetime,
ProcedureEndTime datetime,
InRecoveryTime datetime,
ActualOpEndToRecovery int,
AdjustedOpEndToRecovery int,
AdjustedOperationEndDate datetime,
UtilisedMinutes int,
AnaestheticInductionTimeWithSessionStartAdj datetime,
AdjustedOperationEndTimeWithSessionEndAdj datetime,
AdjustedUtilisedMinutes int,
NextAnaestheticInductionTime datetime,
TurnaroundTime int
)
Insert into  WHReporting.QLIK.TheatreOpDetails
(SourceUniqueID,
DistrictNo,
OperationDate,
OperationStartDate,
OperationEndDate,
OperationTheatreCode,
OperationTheatre,
OperationOperatingSuite,
OperationTypeCode,
OperationAdmissionTypeCode,
OperationAdmissionType,
OperationSpecialtyCode,
OperationSpecialty,
OperationConsultantCode,
OperationConsultant,
OperationAnaesthetist1Code,
OperationAnaesthetist,
SystemOriginalOperationOrder,
OperationOrder,
PatientBookingSourceUniqueID,
OperationCancelledFlag,
SessionSourceUniqueID,
InSuiteTime,
InAnaestheticTime,
AnaestheticInductionTime,
AnaestheticReadyTime,
ProcedureStartTime,
ProcedureEndTime,
InRecoveryTime,
ActualOpEndToRecovery,
AdjustedOpEndToRecovery,
AdjustedOperationEndDate,
UtilisedMinutes,--this is the utilised minutes calculated from the original AnaestheticInduction to AdjustedOperationEndDate
AnaestheticInductionTimeWithSessionStartAdj,
AdjustedOperationEndTimeWithSessionEndAdj,
AdjustedUtilisedMinutes,
NextAnaestheticInductionTime,
TurnaroundTime
)


Select SourceUniqueID,
	DistrictNo,
	OperationDate,
	OperationStartDate,
	OperationEndDate,
	TheatreCode,
	Theatre,
	OperatingSuite,
	OperationTypeCode,
	AdmissionTypeCode,
	AdmissionType,
	SpecialtyCode,
	Specialty,
	ConsultantCode,
	Consultant,
	Anaesthetist1Code,
	Anaesthetist,
	SystemOriginalOperationOrder,
	OperationOrder,
	PatientBookingSourceUniqueID,
	OperationCancelledFlag,
	op.SessionSourceUniqueID,
	InSuiteTime,
	InAnaestheticTime,
	AnaestheticInductionTime,
	AnaestheticReadyTime,
	ProcedureStartTime,
	ProcedureEndTime,
	InRecoveryTime,
	ActualOpEndToRecovery,
	AdjustedOpEndToRecovery,
	AdjustedOperationEndDate,
	UtilisedMinutes,--this is the utilised minutes calculated from the original InAnaestheticTime to InRecoveryTime
	Case when op.AnaestheticInductionTime < ss.SessionStartTime then ss.SessionStartTime Else AnaestheticInductionTime End as AnaestheticInductionTimeWithSessionStartAdj,--If the Anaesthetic Induction is before the SessionStart then make this the InSessionStart as we do not want to be including minutes as a result of early starts into the InSEssionProductivity Measure
	Case when AdjustedOperationEndDate > ss.SessionEndTime then ss.SessionEndTime Else AdjustedOperationEndDate End as AdjustedOperationEndTimeWithSessionEndAdj,--If the AdjustedOperationEndDate is after SessionEnd then make this the SessionEnd as we do not want to be including minutes as a result of late finishes into the InSEssionProductivity Measure
	Case when DateDiff(Minute,Case when op.AnaestheticInductionTime < ss.SessionStartTime then ss.SessionStartTime Else AnaestheticInductionTime End ,Case when AdjustedOperationEndDate > ss.SessionEndTime then ss.SessionEndTime Else AdjustedOperationEndDate End ) < 0 then 0
	Else  DateDiff(Minute,Case when op.AnaestheticInductionTime < ss.SessionStartTime then ss.SessionStartTime Else AnaestheticInductionTime End ,Case when AdjustedOperationEndDate > ss.SessionEndTime then ss.SessionEndTime Else AdjustedOperationEndDate End ) 
	END  as AdjustedUtilisedMinutes,--this is a calculation of the above which has capped the utilised minutes which were within the session only.  In some instances data quality errors will result negative minutes - reset these to 0 so as not to skew the data
	NextAnaestheticInductionTime,
	TurnaroundTime
from #Turn op
left outer join WHREPORTING.QLIK.TheatreSessions ss on op.SessionSourceUniqueID = ss.sessionsourceuniqueid--join back to  WHREPORTING.QLIK.TheatreSessions here to get the InSessionstart and InSessionEnd times so the InAnaestheticTime and InRecoveryTime can be adjusted to only reflect the correct times within the session - this will help with the calculation og AdjustedUtilisedMinutes as for the InSessionProductivity measure, only the minutes utilised within then session are to be included 





Select @RowsInserted = @@ROWCOUNT
select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'Qlik - WHREPORTING TheatreOpDetails', @Stats, @StartTime



/***************
Create Cancellations table
***************/
If OBJECT_ID('[WHReporting].[QLIK].[TheatreCancellations]') >0 
Drop table [WHReporting].[QLIK].[TheatreCancellations]

CREATE TABLE [WHReporting].[QLIK].[TheatreCancellations]
  (
     [CancellationSourceUniqueID] INT,
     [CancellationDate]           [DATETIME],
     [ProposedOperationDate]      [DATETIME],
    -- [IntendedProcedureDate]      [DATETIME],
     [CancelReasonCode]           INT,
     [CancelReason]               [VARCHAR](255),
     [CancelReasonGroup]          [VARCHAR](255),
     [SurgeonCode]                INT,
     [Surgeon]                    [VARCHAR](150),
     [SpecialtyCode]              INT,
     [Specialty]                  [VARCHAR](255),
     [TheatreCode]                INT,
     [Theatre]                    [VARCHAR](255),
     [OperatingSuite]             [VARCHAR](255),
     [Division]                   [VARCHAR](80),
     [Direcorate]                 [VARCHAR](80),
     [IntendedOperationDuration]  INT,
     [DaysBetweenCancAndOp]       [INT],
     [IncludeAsCancelledOnDay]    [INT]
  ) 


Insert into WHReporting.QLIK.TheatreCancellations
Select canc.SourceUniqueID as CancellationSourceUniqueID,
			canc.CancellationDate,
			canc.ProposedOperationDate,
			--pb.IntendedProcedureDate,-- this should be the same as above but not always the case
			canc.CancelReasonCode,
			cr.CancelReason,
			crg.CancelReasonGroup,
			canc.SurgeonCode,
			surgeon.Forename + ' '+ Surgeon.Surname as Surgeon,
			case when canc.TheatreCode in ('27','29')  then '46' 
			when canc.SpecialtyCode in ('46','47') and canc.TheatreCode not in ('27','29') then '47'
			when canc.SpecialtyCode in ('15')  then '16'
			Else canc.SpecialtyCode  End as SpecialtyCode,--25/02/2015 CM: Added logic to reassign Obstetrics and Gynaecology sessions correctly based on discussions with CBH and SM in meeting on 11/02/2015: When session is in Delivery Suite 1 or 2 then specialty is 'Obstetric', when specialty of session consultant is Obstetrics or Gynaecology and the session theatre is not Delivery suites then the specialty is 'Gynaecology'
								
			case when canc.TheatreCode in ('27','29')  then 'Obstetrics' 
			when ISNull(spec.Specialty,'Unknown') in ('Obstetrics','Gynaecology') and canc.TheatreCode not in ('27','29') then 'Gynaecology'
			when ISNull(spec.Specialty,'Unknown') in ('Anaesthetics') then 'Pain Management'
			Else ISNull(spec.Specialty,'Unknown') End as Specialty,--25/02/2015 CM: Added logic to reassign Obstetrics and Gynaecology sessions correctly based on discussions with CBH and SM in meeting on 11/02/2015: When session is in Delivery Suite 1 or 2 then specialty is 'Obstetric', when specialty of session consultant is Obstetrics or Gynaecology and the session theatre is not Delivery suites then the specialty is 'Gynaecology'
																		--25/02/2015 CM: Also add logic to reassign Anaesthetics to Pain Management 
			canc.TheatreCode,
			th.Theatre,
			os.OperatingSuite,
			sd.Division,
			sd.Direcorate,
			OperationDuration as IntendedOperationDuration,
			DATEDIFF(DAY,Dateadd(day,datediff(day,0,canc.CancellationDate), 0), canc.ProposedOperationDate) as DaysBetweenCancAndOp,
			case when DATEDIFF(DAY,Dateadd(day,datediff(day,0,canc.CancellationDate), 0), canc.ProposedOperationDate)  =0  then 1 else 0 end as IncludeAsCancelledOnDay
			--case when DATEDIFF(DAY,Dateadd(day,datediff(day,0,canc.CancellationDate), 0), canc.ProposedOperationDate)  >= '-1' and DATEDIFF(DAY,Dateadd(day,datediff(day,0,canc.CancellationDate), 0), canc.ProposedOperationDate)  <= 3 then 1 else 0 end as IncludeAsCancelledOnDay--Same Day Cancellations are to be included for some measure.  What is deemed to be same day cancellation are those where it would be a struggle to re-fill the spot.
																																																																					--Based on meeting between AM, CBH and SM, this means that if taking into account any pre-op assessments, anything is cancelled within 3 days of the op or 1 day after the op would be a struggle to re-fill.
																																																																					--23/07/2014 change this back to only cancellations on same day after meeting with Clare and Peter on 16/07

	from WH.Theatre.Cancellation canc 
	left outer join WH.Theatre.PatientBooking pb on canc.PatientSourceUniqueID = pb.SourceUniqueID 
	left outer join WH.Theatre.Staff Surgeon on canc.SurgeonCode = surgeon.StaffCode
	left outer join WH.Theatre.Specialty spec on canc.SpecialtyCode = spec.SpecialtyCode
	Left outer join WHREPORTING.LK.SpecialtyDivision sd on spec.SpecialtyCode1 = sd.SpecialtyCode
	Left outer join WH.Theatre.Theatre th on canc.TheatreCode = th.TheatreCode
	Left outer join WH.Theatre.OperatingSuite os on th.OperatingSuiteCode = os.OperatingSuiteCode
	Left outer join WH.Theatre.CancelReason cr on canc.CancelReasonCode = cr.CancelReasonCode
	Left outer join WH.Theatre.CancelReasonGroup crg on cr.CancelReasonGroupCode = crg.CancelReasonGroupCode
	Order by ProposedOperationDate






Select @RowsInserted = @@ROWCOUNT
select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'Qlik - WHREPORTING TheatreCancellations', @Stats, @StartTime


/***************
Theatre Operation Delays
****************/
IF Object_id('WHReporting.QLIK.TheatreOperationDelays') > 0
  DROP TABLE WHReporting.QLIK.TheatreOperationDelays

CREATE TABLE WHReporting.QLIK.TheatreOperationDelays
  (
     [DelaySourceUniqueID]           INT,
     [OperationDetailSourceUniqueID] INT,
     [DelayStage]                    [VARCHAR](100),
     [DelayMinutes]                  INT,
     [DelayComments]                 [VARCHAR](400),
     [DelayReasonCode]               INT,
     [DelayReasonDescription]        [VARCHAR](60)
  )

INSERT INTO WHReporting.QLIK.TheatreOperationDelays
            ([DelaySourceUniqueID],
             [OperationDetailSourceUniqueID],
             [DelayStage],
             [DelayMinutes],
             [DelayComments],
             [DelayReasonCode],
             [DelayReasonDescription])
SELECT d.DelaySourceUniqueID,
       d.OperationDetailSourceUniqueID,
       d.DelayStage,
       d.DelayMinutes,
       d.DelayComments,
       d.DelayReasonCode,
       dr.DelayReasonDescription

FROM   WH.Theatre.Delays d
       LEFT OUTER JOIN WH.Theatre.DelayReason dr
                    ON d.DelayReasonCode = dr.DelayReasonCode 


Select @RowsInserted = @@ROWCOUNT
select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'Qlik - WHREPORTING BuildTheatreTables', @Stats, @StartTime






END