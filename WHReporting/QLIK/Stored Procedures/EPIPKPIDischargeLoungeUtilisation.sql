﻿/******************************************************************************
 Description: EPIP Qlikview dashboard KPI: Discharge Lounge utilisation 
              percentage.
  
 Parameters:  @ThisWeekMinus1StartDate - The start date for last week
              @ThisWeekMinus1EndDate -   The end date for last week
              @ThisWeekStartDate - The start date of the current week
              @CurrentMonitorQuarter - The current Monitor quarter name
              @CurrentMonth - The start date of the current month
              @ThisWeekMinus2StartDate - The start date of the previous week to last week
              @ThisWeekMinus2EndDate - The end date of the previous week to last week
              @ReportingPeriod - the required reporting period
                                 1 = Last week
                                 2 = Previous week to last week
                                 3 = Current week to date
                                 4 = Current month to date
                                 5 = Current Monitor quarter to date
              @ReportingDivision - the required reporting Division
                                   SC = Scheduled Care
                                   UC = Unscheduled Care
 
 Modification History --------------------------------------------------------

 Date     Author       Change
 25/07/14 Tim Dean	   Created

******************************************************************************/

CREATE Procedure [QLIK].[EPIPKPIDischargeLoungeUtilisation]
 @ThisWeekMinus1StartDate DATETIME
,@ThisWeekMinus1EndDate DATETIME
,@ThisWeekStartDate DATETIME
,@CurrentMonitorQuarter VARCHAR(30)
,@CurrentMonth DATETIME
,@ThisWeekMinus2StartDate DATETIME
,@ThisWeekMinus2EndDate DATETIME
,@ReportingPeriod INT
,@ReportingDivision CHAR(2)
,@UtilisationRate DECIMAL(10,5) OUTPUT
as

SET NOCOUNT ON;

SELECT @UtilisationRate = SUM(d.DischargeLoungeFlag) / COUNT(*)  -- Calculate the Discharge Lounge utilisation percentage.
FROM (
	SELECT DischargeLoungeFlag = CASE LastWard.WardCode
			WHEN 'DL'
				THEN 1.00
			ELSE 0.00
			END
	    -- Determine the Division from the discharging ward.  If the spell was discharged from the
	    -- Discharge Lounge we need to look at the previous ward stay to determine the discharging ward.
		,Division = CASE 
			WHEN (
					CASE LastWard.WardCode
						WHEN 'DL'
							THEN NextLastWard.WardCode
						ELSE LastWard.WardCode
						END
					) IN (
					'A1'
					,'A2'
					,'A3'
					,'A4'
					,'A5'
					,'A6'
					,'BUR'
					,'C2'
					,'C3'
					,'CCA'
					,'F1'
					,'F16'
					,'F2N'
					,'F3'
					,'F5'
					,'F6'
					,'F9'
					,'JIM'
					)
				THEN 'SC'
			WHEN (
					CASE LastWard.WardCode
						WHEN 'DL'
							THEN NextLastWard.WardCode
						ELSE LastWard.WardCode
						END
					) IN (
					'A7'
					,'A9'
					,'DYL'
					,'EAU'
					,'F10'
					,'F11'
					,'F12'
					,'F14'
					,'F15S'
					,'F4N'
					,'F7'
					,'PEA'
					,'POU'
					,'WIL'
					)
				THEN 'UC'
			ELSE 'Other'
			END
	FROM WHREPORTING.LK.Calendar c
	-- Join spells to the calendar, so that we only have spells for the required reporting period.
	LEFT JOIN WHREPORTING.APC.Spell ON (c.TheDate = Spell.DischargeDate)
	-- Join to the last ward stay in the spell.
	LEFT JOIN WHREPORTING.APC.WardStay LastWard ON (
			Spell.SourceSpellNo = LastWard.SourceSpellNo
			AND Spell.SourcePatientNo = LastWard.SourcePatientNo
			AND Spell.DischargeDateTime = LastWard.EndTime
			)
	-- Join to the ward stay before the last ward stay in the spell.
	LEFT JOIN WHREPORTING.APC.WardStay NextLastWard ON (
			LastWard.SourceSpellNo = NextLastWard.SourceSpellNo
			AND LastWard.SourcePatientNo = NextLastWard.SourcePatientNo
			AND LastWard.StartTime = NextLastWard.EndTime
			)
	WHERE -- Filter the calendar for the selected reporting period.
		(
			(
				@ReportingPeriod = 1 -- Last week.
				AND c.FirstDayOfWeek = @ThisWeekMinus1StartDate
				AND c.LastDayOfWeek = @ThisWeekMinus1EndDate
				)
			OR (
				@ReportingPeriod = 2 -- Previous week to last week.
				AND c.FirstDayOfWeek = @ThisWeekMinus2StartDate
				AND c.LastDayOfWeek = @ThisWeekMinus2EndDate
				)
			OR (
				@ReportingPeriod = 3 -- Current week to date.
				AND c.FirstDayOfWeek = @ThisWeekStartDate
				AND c.TheDate < CAST(GETDATE() AS DATE)
				)
			OR (
				@ReportingPeriod = 4 -- Current month to date.
				AND c.FirstDateOfMonth = @CurrentMonth
				AND c.TheDate < CAST(GETDATE() AS DATE)
				)
			OR (
				@ReportingPeriod = 5 -- Current Monitor quarter to date.
				AND c.MonitorQuarter = @CurrentMonitorQuarter
				AND c.TheDate < CAST(GETDATE() AS DATE)
				)
			)
		AND Spell.PatientClass = 'NON ELECTIVE' -- Only need non-elective spells.
		-- The discharging ward is in the cohort of wards we need to monitor.
		-- If the spell was discharged from the Discharge Lounge we need to look at the 
		-- the previos ward stay to determin the discharging ward.
		AND (
			CASE LastWard.WardCode
				WHEN 'DL'
					THEN NextLastWard.WardCode
				ELSE LastWard.WardCode
				END
			) IN (
			'F11'
			,'F12'
			,'F14'
			,'F4N'
			,'F10'
			,'F15S'
			,'F7'
			,'POU'
			,'DYL'
			,'WIL'
			,'PEA'
			,'A1'
			,'A2'
			,'A3'
			,'A4'
			,'A5'
			,'A6'
			,'A7'
			,'A9'
			,'BUR'
			,'C2'
			,'C3'
			,'CCA'
			,'DYL'
			,'EAU'
			,'F1'
			,'F16'
			,'F2N'
			,'F3'
			,'F5'
			,'F6'
			,'F9'
			,'JIM'
			)
	) d
WHERE d.Division = @ReportingDivision -- Only report on the required Division.
;

RETURN