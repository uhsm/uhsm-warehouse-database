﻿CREATE procedure [QLIK].[ExportAEMeasuresTable] as

--comments / audit log to do
exec QLIK.BuildAEMeasuresTable

EXEC [V-UHSM-EPR].TState.sys.sp_executesql N'TRUNCATE TABLE dbo.AEMeasuresTable'
insert into [V-UHSM-EPR].TState.dbo.AEMeasuresTable (
	Measure
	,Value
	,RAG
	,[Order]
	,CensusDate
	)
select 
	Measure
	,Value
	,RAG
	,[Order]
	,CensusDate = GETDATE() 
from QLIK.AEMeasuresTable