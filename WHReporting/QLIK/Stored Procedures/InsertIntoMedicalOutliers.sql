﻿-- =============================================
-- Author:		CMan
-- Create date: 20/07/2015
-- Description:	
--Stored Procedure to be run to insert the data from the SSRS Historical Lodgers Report into the WHReporting.QLIK.MedicalOutliers table to be used in the Qlikview Patient Flow Dashboard.
--Script has been taken from SQL behiind the SSRS report
--As part of the daily sitrep process, GR explained that the medical outliers for unscheduled care is taken from the SSRS Historical Lodgers report and sent to be verified.  
--The number of outliers from this report is then entered onto the sitrep.  If feedback is received from the team to indicate that the number of outliers is actually different following validation, then the nnumber in the dailt sit rep will be amended.
--This stored procedure, will insert the figure for the previous day as reporting in the SSRS report into the WHReporting.QLIK.MedicalOutliers.  If this number needs to be amended following validation, the Info Team will edit the table to reflect the agreed number to be reported

--05/04/2016 CM - change/amend
--Further to email from GR, it tranpires that late ADT causes the number to increase over time.  Therefore by freezing the data, these number of medical outliers is actualy inaccurate when compared againt that which has been recorded on PAS over time.
--Amend insert so that all the data in the WHReporting.QLIK.MedicalOutliers table is overwritten over time.


-- =============================================
CREATE PROCEDURE [QLIK].[InsertIntoMedicalOutliers]
AS
BEGIN
	 SET NOCOUNT ON --added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Declare @Date datetime =   DateAdd( day, DateDiff(day, 0, getdate()-1),0)

--05/04/2016 CM - remove this and replace with Truncate as all the records in the table will be gettimng overwritten
--Delete From  WHReporting.QLIK.MedicalOutliers
--where SitRepDate = @Date

Truncate table WHReporting.QLIK.MedicalOutliers

Insert into  WHReporting.QLIK.MedicalOutliers
Select SnapshotDate = SnapshotDate
		, SitRepDate = TheDate
		, PatientFlowDate = TheDate +1--added this so as for the Patient Flow Dashboard, the medical outliers is reports with a lag.  Adding this column into the table may help us to remember in the future that this is the way we report
		, TotalOutliers = SUM(Counter) 
		, LastUpdated = GETDATE()
From 
(
select 
cast(cal.TheDate as datetime)+'23:59:59' as SnapshotDate,
cal.TheDate,
EPI.EncounterRecno
,EPI.SourceSpellNo
,EPI.FacilityID
,EPI.Specialty
,EPI.SpecialtyCode
,EPI.AdmissionDate
,EPI.DischargeDate
,EPI.EpisodeStartDate
,EPI.EpisodeStartTime
,EPI.EpisodeEndDate
,EPI.EpisodeEndTime
,EPI.StartWardCode
,EPI.Division
,EPI.ConsultantCode
,EPI.ConsultantName
,EPI.LengthOfEpisode
,EPI.EpisodePCTCode
,EPI.EpisodePCT
,WS.WardCode
,WS.StartTime
,WS.EndTime
,LK.Division as LookUpDivision

,DATEDIFF([d],AdmissionDate, GetDate()) as LOS

,CONVERT(VARCHAR(30),WS.EndTime,103) as EndTime2

,case when EPI.Division <> LK.Division 
then 'Outlier' else 'Non Outlier'
end as Outliers

,sum(case when EPI.Division = 'Unscheduled Care'  AND LK.Division = 'Scheduled Care'  
AND SpecialtyCode <> '340' 
then 1 else 0
end) 
+
sum(case when EPI.Division = 'Unscheduled Care'  AND LK.Division = 'Scheduled Care'  
AND SpecialtyCode = '340' AND WardCode IN ('F1N', 'F5', 'F6','F2', 'F1')
then 1 else 0
end)
+
sum(case when EPI.Division = 'Scheduled Care' AND LK.Division = 'Unscheduled Care'
then 1 else 0
end)
+
sum(case when EPI.Division = 'Clinical Support' AND LK.Division IN ('Unscheduled Care', 'Scheduled Care')
then 1 else 0
end) as Counter

from APC.WardStay WS
left outer join APC.Episode EPI
on WS.SourceSpellNo = EPI.SourceSpellNo
left outer join LK.WardtoDivision LK
on LK.Ward = WS.WardCode
right join lk.Calendar cal on cast(cal.TheDate as datetime)+'23:59:59' between EpisodeStartDateTime and coalesce(EpisodeEndDateTime,getdate())
and cast(cal.TheDate as datetime)+'23:59:59' between ws.starttime and coalesce(ws.endtime,getdate())

where  WS.WardCode in ('A8','A10','AMU','A7','A9', 'F4N', 'F4', 'F15S', 'F15','F7','F10','F11','F12','F14','CDU', 'EAU','DYL','PEA','POU','WIL','A2','A1','A4','A5','A6','F1S','F3','F3PLAS','F9','BUR','SAU','F16','JIM','F1','F2','F5','F6','A3', 'F1N')
--and cast(cal.TheDate as int) in (@Date)--05/04/2016 CM - remove this so that all records in the table are updated - not just yesterdays data 
and (EPI.Division IN ('Unscheduled Care'))-- only the unscheduled care outliers are sent to be checked and verified for the daily sitrep as discussed with GR
and case when EPI.Division <> LK.Division
then 'Outlier' else 'Non Outlier'
end in ('Outlier')

group by
cast(cal.TheDate as datetime)+'23:59:59',
cal.TheDate,
EPI.EncounterRecno
,EPI.SourceSpellNo
,EPI.FacilityID
,EPI.Specialty
,EPI.SpecialtyCode
,EPI.AdmissionDate
,EPI.DischargeDate
,EPI.EpisodeStartDate
,EPI.EpisodeStartTime
,EPI.EpisodeEndDate
,EPI.EpisodeEndTime
,EPI.StartWardCode
,EPI.Division
,EPI.ConsultantCode
,EPI.ConsultantName
,EPI.LengthOfEpisode
,EPI.EpisodePCTCode
,EPI.EpisodePCT
,WS.WardCode
,WS.StartTime
,WS.EndTime
,LK.Division
,case when EPI.Division <> LK.Division
then 'Outlier' else 'Non Outlier'
end
) as  A
Group by A.TheDate, SnapshotDate
order by SnapshotDate desc


END