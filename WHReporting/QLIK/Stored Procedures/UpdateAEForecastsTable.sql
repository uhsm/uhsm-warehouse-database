﻿-- =============================================
-- Author:		CMan
-- Create date: 08/09/2014
-- Description:	Stored procedure to calculate the A&E forecasts based on historic attendances
-- =============================================
CREATE PROCEDURE [QLIK].[UpdateAEForecastsTable]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;





--Create temp tables with the lastest A&E actual attend counts
If OBJECT_ID ('tempdb..#att') > 0
Drop table #att
Select  ArrivalDate, count(SourceUniqueID) as NumberofAttend 
into #att
From WH.AE.Encounter
where-- ArrivalDate not in (Select [DATE] from WHREPORTING.QLIK.AEForecast  where AllAttend is not null)
 ArrivalDate >= '20140401'
and AttendanceCategoryCode IN ('1','2')
Group by ArrivalDate


If OBJECT_ID ('tempdb..#maj') > 0
Drop table #maj
Select  ArrivalDate, count(SourceUniqueID) as Majors 
into #maj
From WH.AE.Encounter
where --ArrivalDate not in (Select [DATE] from WHREPORTING.QLIK.AEForecast )
ArrivalDate >= '20140401'
and AttendanceCategoryCode IN ('1','2')
and ( [TriageCategoryCode] IN  ('R','O','Y') or [TriageCategoryCode] IS NULL AND [WhereSeen] NOT IN ('2','3','4')) 
Group by ArrivalDate


If OBJECT_ID ('tempdb..#min') > 0
Drop table #min
Select  ArrivalDate, count(SourceUniqueID) as Minors 
into #min
From WH.AE.Encounter
where --ArrivalDate not in (Select [DATE] from WHREPORTING.QLIK.AEForecast )
ArrivalDate >= '20140401'
and AttendanceCategoryCode IN ('1','2')
and ( [TriageCategoryCode] Not IN  ('R','O','Y') or [TriageCategoryCode] IS NULL AND [WhereSeen]  IN ('2','3','4')) 
Group by ArrivalDate




--Update WHREPORTING.QLIK.AEForecast table with the latest actual counts from the temp tables above
--tempdb table to join above 3 temp tables together
If OBJECT_ID ('tempdb..#AE') > 0
Drop table #AE
Select #att.arrivaldate, #att.NumberofAttend, #maj.majors, #min.minors
into #AE
From #Att
left outer join #maj on #att.arrivaldate = #maj.arrivaldate
Left outer join #min on #att.arrivaldate = #min.arrivaldate




Update WHREPORTING.QLIK.AEForecast
Set AllAttend = ae.NumberofAttend,
MajorAttend = ae.majors,
MinorAttend = ae.minors
from WHREPORTING.QLIK.AEForecast fore
inner join #AE ae on fore.[Date] = ae.arrivaldate





--Update forecast columns as far as possible depending on what has been inserted into the actual counts in the step above.

--Update all attendances forecast
Update WHREPORTING.QLIK.AEForecast 
Set AllAttendForecast = ((1-0.36) * b.AllAttend) + (0.36 * b.AllAttendForecast)
From WHREPORTING.QLIK.AEForecast a
Left outer join  (Select * from WHREPORTING.QLIK.AEForecast) as b on b.[Date] = DATEADD(DAY,-14,a.[Date]) 
where a.Date >= Dateadd(dd, -14, GETDATE())--just to add in a point to update forecasts from in case forecasts from previous days need refreshing.


--Update all major attends forecast
Update WHREPORTING.QLIK.AEForecast 
Set  MajorAttendForecast = ((1-0.36) * b.MajorAttend) + (0.36 * b.MajorAttendForecast)
From WHREPORTING.QLIK.AEForecast a
Left outer join  (Select * from WHREPORTING.QLIK.AEForecast) as b on b.[Date] = DATEADD(DAY,-14,a.[Date]) 
where a.Date >= Dateadd(dd, -14, GETDATE())--just to add in a point to update forecasts from in case forecasts from previous days need refreshing.


--Update all minor attends forecast
Update WHREPORTING.QLIK.AEForecast 
Set  MinorAttendForecast = ((1-0.36) * b.MinorAttend) + (0.36 * b.MinorAttendForecast)
From WHREPORTING.QLIK.AEForecast a
Left outer join  (Select * from WHREPORTING.QLIK.AEForecast) as b on b.[Date] = DATEADD(DAY,-14,a.[Date]) 
where a.Date >= Dateadd(dd, -14, GETDATE())--just to add in a point to update forecasts from in case forecasts from previous days need refreshing.






END