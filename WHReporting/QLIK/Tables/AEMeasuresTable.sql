﻿CREATE TABLE [QLIK].[AEMeasuresTable](
	[Measure] [varchar](150) NULL,
	[Value] [varchar](10) NULL,
	[RAG] [varchar](20) NULL,
	[Order] [int] NULL
) ON [PRIMARY]