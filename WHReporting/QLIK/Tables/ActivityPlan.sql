﻿CREATE TABLE [QLIK].[ActivityPlan](
	[ActivityDate] [datetime] NULL,
	[LocalSpecialtyCode] [varchar](50) NULL,
	[ActivityArea] [nvarchar](255) NULL,
	[TargetValue] [float] NULL
) ON [PRIMARY]