﻿CREATE TABLE [QLIK].[ActivityPlanOLD](
	[ActivityDate] [datetime] NOT NULL,
	[LocalSpecialtyCode] [varchar](20) NOT NULL,
	[ActivityArea] [varchar](5) NOT NULL,
	[TargetValue] [decimal](10, 6) NULL,
 CONSTRAINT [PK_ActivityPlan] PRIMARY KEY CLUSTERED 
(
	[ActivityDate] ASC,
	[LocalSpecialtyCode] ASC,
	[ActivityArea] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]