﻿CREATE TABLE [QLIK].[AmbulanceDataQuarterly](
	[TheDate] [datetime] NULL,
	[Indicator<15Min] [float] NULL,
	[Indicator<30Min] [float] NULL,
	[Indicator<60Min] [float] NULL
) ON [PRIMARY]