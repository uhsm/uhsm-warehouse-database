﻿CREATE TABLE [QLIK].[AvLOS1213and1314byDiag](
	[NumberofDischarges_1213_1314] [int] NULL,
	[LOS_1213_1314] [int] NULL,
	[AverageLOS] [float] NULL,
	[PrimaryDiagnosisCode] [varchar](20) NULL
) ON [PRIMARY]