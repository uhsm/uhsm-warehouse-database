﻿CREATE TABLE [QLIK].[AvLOS1213byDiag](
	[NumberofDischarges_1213] [int] NULL,
	[LOS_1213] [int] NULL,
	[AverageLOS] [float] NULL,
	[PrimaryDiagnosisCode] [varchar](20) NULL
) ON [PRIMARY]