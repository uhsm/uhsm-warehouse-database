﻿CREATE TABLE [QLIK].[AvLOS1314byDiag](
	[NumberofDischarges_1314] [int] NULL,
	[LOS_1314] [int] NULL,
	[AverageLOS] [float] NULL,
	[PrimaryDiagnosisCode] [varchar](20) NULL
) ON [PRIMARY]