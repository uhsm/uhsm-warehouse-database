﻿CREATE TABLE [QLIK].[BedOccupancy](
	[Date] [datetime] NULL,
	[WardStayUniqueID] [int] NOT NULL,
	[SourceSpellNo] [int] NOT NULL,
	[SourcePatientNo] [int] NOT NULL,
	[WardstayStart] [smalldatetime] NULL,
	[WardstayEnd] [smalldatetime] NULL,
	[WardCode] [varchar](25) NULL,
	[Date11] [datetime] NULL,
	[MidnightFlag] [int] NOT NULL,
	[11amFlag] [int] NOT NULL,
	[PasUpdated] [datetime] NULL
) ON [PRIMARY]