﻿CREATE TABLE [QLIK].[BedOccupancyAt11](
	[Date] [datetime] NULL,
	[WardStayUniqueID] [int] NOT NULL,
	[SourceSpellNo] [int] NOT NULL,
	[SourcePatientNo] [int] NOT NULL,
	[WardstayStart] [smalldatetime] NULL,
	[WardstayEnd] [smalldatetime] NULL,
	[WardCode] [varchar](25) NULL,
	[PasUpdated] [datetime] NULL
) ON [PRIMARY]