﻿CREATE TABLE [QLIK].[DPR_Appraisal](
	[Month] [datetime] NULL,
	[Appraisal] [nvarchar](255) NULL,
	[Division (Org L3)] [nvarchar](255) NULL,
	[Directorate (Org L4)] [nvarchar](255) NULL,
	[Speciality (Org L5)] [nvarchar](255) NULL,
	[Organisation (Org L6)] [nvarchar](255) NULL,
	[IN DATE] [float] NULL,
	[EXPIRED] [float] NULL
) ON [PRIMARY]