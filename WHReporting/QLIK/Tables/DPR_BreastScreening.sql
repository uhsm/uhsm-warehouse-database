﻿CREATE TABLE [QLIK].[DPR_BreastScreening](
	[Month] [datetime] NULL,
	[Screening] [nvarchar](255) NULL,
	[Within Target] [float] NULL,
	[Total] [float] NULL
) ON [PRIMARY]