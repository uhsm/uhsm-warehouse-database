﻿CREATE TABLE [QLIK].[DPR_CancelledOps](
	[ID] [float] NULL,
	[Specialty] [nvarchar](255) NULL,
	[Patient UR Number] [nvarchar](255) NULL,
	[Patient Name] [nvarchar](255) NULL,
	[Date of Op Cancelled] [datetime] NULL,
	[MonthNumber] [float] NULL,
	[Month] [nvarchar](255) NULL,
	[Reason for Cancellation] [nvarchar](255) NULL,
	[2nd Reason] [nvarchar](255) NULL,
	[Comments] [nvarchar](255) NULL,
	[Week-end Date] [datetime] NULL,
	[Financial Year] [nvarchar](255) NULL,
	[NHS_CODE] [nvarchar](255) NULL,
	[SpecialtyFunction] [nvarchar](255) NULL,
	[Data Added to Database] [datetime] NULL
) ON [PRIMARY]