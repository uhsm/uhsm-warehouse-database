﻿CREATE TABLE [QLIK].[DPR_ComplaintsResponses](
	[Case Number] [varchar](50) NULL,
	[Response Due Date] [datetime] NULL,
	[Response Date] [datetime] NULL,
	[Directorate] [varchar](50) NULL,
	[Department] [varchar](50) NULL,
	[Specialty] [varchar](50) NULL,
	[Days Over Agreed Date] [varchar](50) NULL,
	[Category] [varchar](255) NULL,
	[Category Type] [varchar](255) NULL,
	[Date Received] [datetime] NULL
) ON [PRIMARY]