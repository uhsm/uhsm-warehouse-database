﻿CREATE TABLE [QLIK].[DPR_DM01](
	[TestName] [nvarchar](255) NULL,
	[Type] [nvarchar](255) NULL,
	[Month] [datetime] NULL,
	[Result] [float] NULL
) ON [PRIMARY]