﻿CREATE TABLE [QLIK].[DPR_FriendsFamilyTestsInpatients](
	[Month] [datetime] NULL,
	[Ward] [nvarchar](255) NULL,
	[Extremely Likely] [float] NULL,
	[Likely] [float] NULL,
	[Neither likely nor unlikely] [float] NULL,
	[Unlikely] [float] NULL,
	[Extremely unlikely] [float] NULL,
	[Don't Know] [float] NULL,
	[Total Number of people eligible to respond] [float] NULL
) ON [PRIMARY]