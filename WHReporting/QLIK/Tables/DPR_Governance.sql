﻿CREATE TABLE [QLIK].[DPR_Governance](
	[Reported Month] [nvarchar](255) NULL,
	[FinancialYear] [nvarchar](255) NULL,
	[WardCode] [nvarchar](255) NULL,
	[Incident Month] [nvarchar](255) NULL,
	[Managed] [nvarchar](255) NULL,
	[Check] [float] NULL,
	[SnapShot] [datetime] NULL,
	[Reported Date] [datetime] NULL,
	[Incident Number] [float] NULL,
	[Incident Type] [nvarchar](255) NULL,
	[Alert Type] [nvarchar](255) NULL,
	[Cause Group] [nvarchar](255) NULL,
	[Cause 1] [nvarchar](255) NULL,
	[Cause 2] [nvarchar](255) NULL,
	[Actual Impact] [nvarchar](255) NULL,
	[Ward] [nvarchar](255) NULL,
	[Specialty] [nvarchar](255) NULL,
	[Directorate] [nvarchar](255) NULL,
	[Division] [nvarchar](255) NULL,
	[Site] [nvarchar](255) NULL,
	[Incident Date] [datetime] NULL,
	[Incident Time] [datetime] NULL,
	[Safeguarding Adults] [nvarchar](255) NULL,
	[Safeguarding Children] [nvarchar](255) NULL,
	[Status of Incident] [nvarchar](255) NULL,
	[Patient Safety Incident (NRLS)] [nvarchar](255) NULL
) ON [PRIMARY]