﻿CREATE TABLE [QLIK].[DPR_HRCorporateAreaLookup](
	[DirectorateCode] [nvarchar](255) NULL,
	[Directorate] [nvarchar](255) NULL,
	[SpecialtyCode] [nvarchar](255) NULL,
	[Specialty] [nvarchar](4000) NULL
) ON [PRIMARY]