﻿CREATE TABLE [QLIK].[DPR_HRHeadcount](
	[MonthStart] [datetime] NULL,
	[Org L3] [nvarchar](255) NULL,
	[Org L4] [nvarchar](255) NULL,
	[Org L5] [nvarchar](255) NULL,
	[Org L6] [nvarchar](255) NULL,
	[Staff Group] [nvarchar](255) NULL,
	[FTE] [float] NULL,
	[Headcount] [float] NULL
) ON [PRIMARY]