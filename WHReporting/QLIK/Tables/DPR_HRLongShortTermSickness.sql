﻿CREATE TABLE [QLIK].[DPR_HRLongShortTermSickness](
	[MonthStart] [datetime] NULL,
	[Org L3] [nvarchar](255) NULL,
	[Org L4] [nvarchar](255) NULL,
	[Org L5] [nvarchar](255) NULL,
	[Org L6] [nvarchar](255) NULL,
	[ST-No Of Episodes] [float] NULL,
	[ST-Prorated FTE Days Lost] [float] NULL,
	[ST-Prorated Calendar Days Lost] [float] NULL,
	[ST-Percent SUM(Prorated FTE Days Lost Long Tem Short Term)] [float] NULL,
	[LT-No Of Episodes] [float] NULL,
	[LT-Prorated FTE Days Lost] [float] NULL,
	[LT-Prorated Calendar Days Lost] [float] NULL,
	[LT-Percent SUM(Prorated FTE Days Lost Long Tem Short Term)] [float] NULL,
	[Total FTE Lost] [float] NULL
) ON [PRIMARY]