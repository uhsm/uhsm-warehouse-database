﻿CREATE TABLE [QLIK].[DPR_HSMR_56](
	[Specialty (of discharge)] [nvarchar](255) NULL,
	[Spells] [int] NULL,
	[Super Spells] [float] NULL,
	[Spells (%)] [float] NULL,
	[Observed] [float] NULL,
	[Expected] [float] NULL,
	[Obs# - Exp#] [float] NULL,
	[Rate (%)] [float] NULL,
	[Exp# (%)] [float] NULL,
	[Relative Risk] [float] NULL,
	[Low] [float] NULL,
	[High] [float] NULL,
	[Period] [datetime] NULL,
	[Rag Rating] [bit] NULL
) ON [PRIMARY]