﻿CREATE TABLE [QLIK].[DPR_InfectionWard](
	[Month] [datetime] NULL,
	[Ward] [nvarchar](255) NULL,
	[MRSA] [float] NULL,
	[CDIFF] [nvarchar](255) NULL,
	[CDIFFAvoidable] [float] NULL
) ON [PRIMARY]