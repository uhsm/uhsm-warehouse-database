﻿CREATE TABLE [QLIK].[DPR_LabourTurnover](
	[MonthStart] [datetime] NULL,
	[Org L3] [nvarchar](255) NULL,
	[Org L4] [nvarchar](255) NULL,
	[Org L5] [nvarchar](255) NULL,
	[Org L6] [nvarchar](255) NULL,
	[Headcount] [float] NULL,
	[FTE] [float] NULL,
	[Starters Headcount] [float] NULL,
	[Starters FTE] [float] NULL,
	[Leavers Headcount] [float] NULL,
	[Leavers FTE] [float] NULL,
	[LTR Headcount %] [float] NULL,
	[LTR FTE %] [float] NULL
) ON [PRIMARY]