﻿CREATE TABLE [QLIK].[DPR_LabourTurnover_StaffGroup](
	[MonthStart] [datetime] NULL,
	[Division (Org L3)] [nvarchar](255) NULL,
	[Directorate (Org L4)] [nvarchar](255) NULL,
	[Speciality (Org L5)] [nvarchar](255) NULL,
	[Organisation (Org L6)] [nvarchar](255) NULL,
	[Staff Group] [nvarchar](255) NULL,
	[FTE] [float] NULL,
	[Leavers FTE] [float] NULL,
	[LTR FTE %] [float] NULL
) ON [PRIMARY]