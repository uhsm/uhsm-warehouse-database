﻿CREATE TABLE [QLIK].[DPR_MandatoryTraining](
	[Month] [datetime] NULL,
	[Directorate] [nvarchar](255) NULL,
	[Sub Directorate] [nvarchar](255) NULL,
	[Specialty] [nvarchar](255) NULL,
	[Ward/Department] [nvarchar](255) NULL,
	[Positions that require competence] [float] NULL,
	[Positions that meet requirement] [float] NULL,
	[Positions that do not meet requirement] [float] NULL,
	[% Compliance] [float] NULL
) ON [PRIMARY]