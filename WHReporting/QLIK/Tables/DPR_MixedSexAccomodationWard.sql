﻿CREATE TABLE [QLIK].[DPR_MixedSexAccomodationWard](
	[Month] [datetime] NULL,
	[Division] [nvarchar](255) NULL,
	[Code] [nvarchar](255) NULL,
	[Breaches] [float] NULL
) ON [PRIMARY]