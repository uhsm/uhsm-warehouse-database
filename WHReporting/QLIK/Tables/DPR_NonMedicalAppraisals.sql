﻿CREATE TABLE [QLIK].[DPR_NonMedicalAppraisals](
	[Month] [datetime] NULL,
	[Org L3] [nvarchar](255) NULL,
	[Org L4] [nvarchar](255) NULL,
	[Org L5] [nvarchar](255) NULL,
	[Org L6] [nvarchar](255) NULL,
	[In Post] [float] NULL,
	[In Date] [float] NULL,
	[%] [float] NULL
) ON [PRIMARY]