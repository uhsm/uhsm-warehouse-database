﻿CREATE TABLE [QLIK].[DPR_PFDs_Candour_NEs](
	[Month] [datetime] NULL,
	[PreventionOfFutureDeaths] [int] NULL,
	[DutyOfCandour] [int] NULL,
	[NeverEvents] [int] NULL
) ON [PRIMARY]