﻿CREATE TABLE [QLIK].[DPR_Pathology](
	[Month] [datetime] NULL,
	[U&E] [float] NULL,
	[Troponin T] [float] NULL,
	[LFTs] [float] NULL,
	[FBC] [float] NULL,
	[D-Dimer] [float] NULL,
	[HSC205] [float] NULL
) ON [PRIMARY]