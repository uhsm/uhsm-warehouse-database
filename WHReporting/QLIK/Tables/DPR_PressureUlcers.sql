﻿CREATE TABLE [QLIK].[DPR_PressureUlcers](
	[Reported Date] [datetime] NULL,
	[Incident Number] [float] NULL,
	[Cause Group] [nvarchar](255) NULL,
	[Cause 1] [nvarchar](255) NULL,
	[Actual Impact] [nvarchar](255) NULL,
	[Department] [nvarchar](255) NULL,
	[Specialty] [nvarchar](255) NULL,
	[Directorate] [nvarchar](255) NULL,
	[Division] [nvarchar](255) NULL
) ON [PRIMARY]