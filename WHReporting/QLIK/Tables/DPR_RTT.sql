﻿CREATE TABLE [QLIK].[DPR_RTT](
	[MonthYear] [datetime] NULL,
	[Spec Code] [varchar](20) NULL,
	[Area] [nvarchar](255) NULL,
	[Treatment Function Code] [nvarchar](255) NULL,
	[Treatment Function] [nvarchar](255) NULL,
	[MEDIAN] [float] NULL,
	[95th Percentile] [float] NULL,
	[Clock Stops] [float] NULL,
	[within 18 Weeks] [float] NULL,
	[18+] [float] NULL,
	[% within 18 Weeks] [float] NULL
) ON [PRIMARY]