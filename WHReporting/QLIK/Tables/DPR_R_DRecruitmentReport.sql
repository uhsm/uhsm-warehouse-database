﻿CREATE TABLE [QLIK].[DPR_R_DRecruitmentReport](
	[Month] [datetime] NULL,
	[HUB] [nvarchar](255) NULL,
	[PatientsRecruited] [float] NULL
) ON [PRIMARY]