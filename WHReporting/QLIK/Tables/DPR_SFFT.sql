﻿CREATE TABLE [QLIK].[DPR_SFFT](
	[Month] [datetime] NULL,
	[Division] [nvarchar](255) NULL,
	[Q] [float] NULL,
	[Value] [float] NULL
) ON [PRIMARY]