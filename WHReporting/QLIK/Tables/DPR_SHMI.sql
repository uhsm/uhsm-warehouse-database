﻿CREATE TABLE [QLIK].[DPR_SHMI](
	[ReportedMonth] [datetime] NULL,
	[SHMI] [float] NULL,
	[ODBanding] [int] NULL
) ON [PRIMARY]