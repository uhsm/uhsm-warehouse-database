﻿CREATE TABLE [QLIK].[DPR_Safeguarding Adults](
	[Reported Date] [datetime] NULL,
	[Incident Number] [float] NULL,
	[Cause Group] [nvarchar](255) NULL,
	[Cause 1] [nvarchar](255) NULL,
	[Alert Type] [nvarchar](255) NULL,
	[Department] [nvarchar](255) NULL,
	[Specialty] [nvarchar](255) NULL,
	[Directorate] [nvarchar](255) NULL,
	[Division] [nvarchar](255) NULL
) ON [PRIMARY]