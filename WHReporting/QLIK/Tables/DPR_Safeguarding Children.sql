﻿CREATE TABLE [QLIK].[DPR_Safeguarding Children](
	[Reported Date] [varchar](50) NULL,
	[Incident Number] [varchar](50) NULL,
	[Cause Group] [varchar](50) NULL,
	[Cause 1] [varchar](50) NULL,
	[Alert Type] [varchar](50) NULL,
	[Department] [varchar](50) NULL,
	[Specialty] [varchar](50) NULL,
	[Directorate] [varchar](50) NULL,
	[Division] [varchar](50) NULL
) ON [PRIMARY]