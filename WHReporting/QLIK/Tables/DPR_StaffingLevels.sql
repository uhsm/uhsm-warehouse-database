﻿CREATE TABLE [QLIK].[DPR_StaffingLevels](
	[Date] [datetime] NULL,
	[Ward] [nvarchar](255) NULL,
	[Specialty 1] [nvarchar](255) NULL,
	[Specialty 2] [nvarchar](255) NULL,
	[Day - Registered midwives / nurses planned staff hours] [float] NULL,
	[Day - Registered midwives / nurses actual staff hours] [float] NULL,
	[Day - Care Staff planned staff hours] [float] NULL,
	[Day - Care Staff actual staff hours] [float] NULL,
	[Night - Registered midwives / nurses planned staff hours] [float] NULL,
	[Night - Registered midwives / nurses actual staff hours] [float] NULL,
	[Night - Care Staff planned staff hours] [float] NULL,
	[Night - Care Staff actual staff hours] [float] NULL
) ON [PRIMARY]