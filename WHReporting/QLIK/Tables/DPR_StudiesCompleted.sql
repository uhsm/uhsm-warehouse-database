﻿CREATE TABLE [QLIK].[DPR_StudiesCompleted](
	[Month] [datetime] NULL,
	[TotalApproved] [float] NULL,
	[TotalMeetingTarget] [float] NULL
) ON [PRIMARY]