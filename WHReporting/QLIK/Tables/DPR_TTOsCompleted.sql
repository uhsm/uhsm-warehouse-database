﻿CREATE TABLE [QLIK].[DPR_TTOsCompleted](
	[Date] [datetime] NULL,
	[Ward ] [nvarchar](255) NULL,
	[TTOs Completed] [float] NULL
) ON [PRIMARY]