﻿CREATE TABLE [QLIK].[DPR_TherapyResponse](
	[Month] [datetime] NULL,
	[Therapy] [nvarchar](255) NULL,
	[Number of High priority patients requiring input] [float] NULL,
	[Number of High priority patients seen] [float] NULL,
	[Number of medium priority patients requiring input] [float] NULL,
	[Number of medium priority patients seen] [float] NULL,
	[Number of Low priority patients requiring input] [float] NULL,
	[Number of Low priority patients seen] [float] NULL
) ON [PRIMARY]