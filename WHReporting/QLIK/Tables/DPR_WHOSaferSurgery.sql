﻿CREATE TABLE [QLIK].[DPR_WHOSaferSurgery](
	[Month] [datetime] NULL,
	[Areas] [nvarchar](255) NULL,
	[Surveys] [float] NULL,
	[Complete] [float] NULL
) ON [PRIMARY]