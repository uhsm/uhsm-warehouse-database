﻿CREATE TABLE [QLIK].[DPR_WardAccreditation](
	[Ward] [nvarchar](255) NULL,
	[Assessment Date] [datetime] NULL,
	[Accreditation Level] [nvarchar](255) NULL,
	[%] [float] NULL
) ON [PRIMARY]