﻿CREATE TABLE [QLIK].[DPR_WardLookUp](
	[CODE] [nvarchar](255) NULL,
	[DESCRIPTION] [nvarchar](255) NULL,
	[Specialty] [nvarchar](255) NULL,
	[Directorate] [nvarchar](255) NULL,
	[Division] [nvarchar](255) NULL,
	[Exclude] [nvarchar](255) NULL
) ON [PRIMARY]