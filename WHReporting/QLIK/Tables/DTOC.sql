﻿CREATE TABLE [QLIK].[DTOC](
	[Date] [datetime] NULL,
	[Authority] [nvarchar](255) NULL,
	[Count of DTOCs] [float] NULL,
	[Sum of Days Delayed] [float] NULL
) ON [PRIMARY]