﻿CREATE TABLE [QLIK].[DTOCByCategory](
	[Date] [datetime] NULL,
	[AuthorityName] [nvarchar](255) NULL,
	[NHS] [float] NULL,
	[Social] [float] NULL,
	[Both] [float] NULL
) ON [PRIMARY]