﻿CREATE TABLE [QLIK].[EDD](
	[Week Ending] [datetime] NULL,
	[No EDD Set] [float] NULL,
	[EDD in the Past] [float] NULL
) ON [PRIMARY]