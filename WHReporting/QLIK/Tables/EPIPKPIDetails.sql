﻿CREATE TABLE [QLIK].[EPIPKPIDetails](
	[KPIID] [int] NOT NULL,
	[KPIName] [varchar](100) NOT NULL,
	[ThresholdUpperValue] [decimal](12, 6) NOT NULL,
	[ThresholdLowerValue] [decimal](12, 6) NOT NULL,
	[ThresholdUpperColour] [varchar](20) NULL,
	[ThresholdLowerColour] [varchar](20) NULL,
	[NormalColour] [varchar](20) NULL,
	[DisplayAsPercent] [varchar](3) NOT NULL,
 CONSTRAINT [PK_EPIPKPIDetails] PRIMARY KEY CLUSTERED 
(
	[KPIID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]