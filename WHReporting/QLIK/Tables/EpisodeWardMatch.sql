﻿CREATE TABLE [QLIK].[EpisodeWardMatch](
	[SourceSpellNo] [int] NOT NULL,
	[EpisodeUniqueID] [int] NOT NULL,
	[WardStayUniqueID] [int] NULL,
	[EpisodeStartDateTime] [smalldatetime] NULL,
	[EpisodeEndDateTime] [smalldatetime] NULL,
	[WardCode] [varchar](25) NULL,
	[WardStartTime] [smalldatetime] NULL,
	[WardEndTime] [smalldatetime] NULL,
	[StartDateTime] [smalldatetime] NULL,
	[EndDateTime] [smalldatetime] NULL
) ON [PRIMARY]