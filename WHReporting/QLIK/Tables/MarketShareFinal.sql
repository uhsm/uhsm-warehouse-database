﻿CREATE TABLE [QLIK].[MarketShareFinal](
	[MonthStart] [datetime] NULL,
	[SpecialtyCode] [nvarchar](3) NULL,
	[SpecialtyName] [nvarchar](255) NULL,
	[Specialty] [nvarchar](255) NULL,
	[Organisations] [nvarchar](128) NULL,
	[Activity] [float] NULL
) ON [PRIMARY]