﻿CREATE TABLE [QLIK].[MedicalOutliers](
	[SnapshotDate] [datetime] NULL,
	[SitRepDate] [datetime] NULL,
	[PatientFlowDate] [datetime] NULL,
	[NoOfOutliers] [int] NULL,
	[LastUpdated] [datetime] NULL
) ON [PRIMARY]