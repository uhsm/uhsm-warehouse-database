﻿CREATE TABLE [QLIK].[RAID](
	[Week Ending] [datetime] NULL,
	[A&E Referrals] [float] NULL,
	[Urgent Wards Referrals] [float] NULL,
	[Other Referrals] [float] NULL,
	[Responses within 1 Hour] [float] NULL,
	[Responses within 6 Hour] [float] NULL,
	[Responses within 24 Hour] [float] NULL,
	[Breaches] [nvarchar](255) NULL
) ON [PRIMARY]