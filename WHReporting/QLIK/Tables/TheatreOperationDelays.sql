﻿CREATE TABLE [QLIK].[TheatreOperationDelays](
	[DelaySourceUniqueID] [int] NULL,
	[OperationDetailSourceUniqueID] [int] NULL,
	[DelayStage] [varchar](100) NULL,
	[DelayMinutes] [int] NULL,
	[DelayComments] [varchar](400) NULL,
	[DelayReasonCode] [int] NULL,
	[DelayReasonDescription] [varchar](60) NULL
) ON [PRIMARY]