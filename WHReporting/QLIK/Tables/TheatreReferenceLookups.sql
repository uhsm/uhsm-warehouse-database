﻿CREATE TABLE [QLIK].[TheatreReferenceLookups](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ORMISCode] [int] NULL,
	[Description] [varchar](250) NULL,
	[ORMISCode1] [varchar](100) NULL,
	[ORMISReasonGroup] [int] NULL,
	[NewGroupingOrMapping] [varchar](250) NULL,
	[ReferenceType] [varchar](50) NULL
) ON [PRIMARY]