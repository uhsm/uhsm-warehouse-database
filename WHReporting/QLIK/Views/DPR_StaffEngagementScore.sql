﻿CREATE View [QLIK].[DPR_StaffEngagementScore]
as

--Treat
--Metric ID 231: FFT positive response - treatment
	SELECT
		'Treat' as Question 
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division 
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN [How likely are you to recommend UHSM as a place to receive treat] IN (
						'Likely'
						,'Extremely Likely'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [How likely are you to recommend UHSM as a place to receive treat] IS NOT NULL
	GROUP BY FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END 
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]

UNION ALL
--Work
--Metric ID 232: FFT positive response - place to work
	SELECT
		'Work' as Question 
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN [How likely are you to recommend UHSM as a place to work] IN (
						'Likely'
						,'Extremely Likely'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [How likely are you to recommend UHSM as a place to work] IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
	
UNION ALL	
--Q24
-- Metric ID 234: Appraisal quality

	SELECT 
		'Q24' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN [My appraisal did help improve how I do my job] IN ('Yes')
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [My appraisal did help improve how I do my job] IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]

UNION ALL
--Q18
--Metric ID 237: Support from line manager

	SELECT 		'Q18' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN [My manager gives me and the members of the team appreciation and support for our contributions] IN (
						'Agree'
						,'Strongly Agree'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [My manager gives me and the members of the team appreciation and support for our contributions] IS NOT NULL 
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]

UNION ALL
--Q25
--Metric ID 238: SMS communicating with staff

	SELECT 		'Q25' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN [Communication from the Senior Team is both open and honest] IN (
						'Agree'
						,'Strongly Agree'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [Communication from the Senior Team is both open and honest]  IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]


UNION ALL
--Q3

	SELECT 		'Q3' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN [I am aware of the values and mission of UHSM] IN (
						'Agree'
						,'Strongly Agree'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [I am aware of the values and mission of UHSM]  IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]


UNION ALL
--Q4

	SELECT 		'Q4' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN [I am able to work in line with UHSM's mission and values] IN (
						'Agree'
						,'Strongly Agree'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [I am able to work in line with UHSM's mission and values]  IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]

UNION ALL 
--Q5

	SELECT 		'Q5' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN [UHSM places delivery of quality patient care at the heart of all] IN (
						'Agree'
						,'Strongly Agree'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [UHSM places delivery of quality patient care at the heart of all]  IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]

UNION ALL 
--Q6

	SELECT 		'Q6' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN [UHSM balances patient care against target delivery] IN (
						'Agree'
						,'Strongly Agree'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [UHSM balances patient care against target delivery]  IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]


UNION ALL 
--Q7

	SELECT 		'Q7' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN [Management decisions support professional standards for patient] IN (
						'Agree'
						,'Strongly Agree'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [Management decisions support professional standards for patient]  IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]



UNION ALL 
--Q8

	SELECT 		'Q8' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN [We often discuss ways of improving the care and services we offe] IN (
						'Agree'
						,'Strongly Agree'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [We often discuss ways of improving the care and services we offe]  IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]



UNION ALL 
--Q9

	SELECT 		'Q9' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN [Teamwork is encouraged and practised in UHSM] IN (
						'Agree'
						,'Strongly Agree'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [Teamwork is encouraged and practised in UHSM]  IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]

UNION ALL 
--Q10

	SELECT 		'Q10' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN [I am regularly briefed on what is happening in my department]   IN (
						'Agree'
						,'Strongly Agree'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [I am regularly briefed on what is happening in my department]  IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]


UNION ALL 
--Q11

	SELECT 		'Q11' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN [I am regularly briefed on what is happening in UHSM]  IN (
						'Agree'
						,'Strongly Agree'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [I am regularly briefed on what is happening in UHSM]  IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]

UNION ALL 
--Q12

	SELECT 		'Q12' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN [I am encouraged to reflect on my own practice and learn when thi]  IN (
						'Agree'
						,'Strongly Agree'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [I am encouraged to reflect on my own practice and learn when thi]  IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]

UNION ALL 
--Q13

	SELECT 		'Q13' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN [I am aware of the leadership behaviours expected within UHSM]  IN (
						'Agree'
						,'Strongly Agree'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [I am aware of the leadership behaviours expected within UHSM]  IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]


UNION ALL 
--Q14

	SELECT 		'Q14' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN  [My manager sets a positive personal example of what they expect ]   IN (
						'Agree'
						,'Strongly Agree'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [My manager sets a positive personal example of what they expect ]  IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
UNION ALL 
--Q15

	SELECT 		'Q15' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN  [My manager creates an inspiring image of what the future could b]   IN (
						'Agree'
						,'Strongly Agree'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [My manager creates an inspiring image of what the future could b]  IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]

UNION ALL 
--Q16

	SELECT 		'Q16' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN  [My manager encourages us to look for ways to improve our service]   IN (
						'Agree'
						,'Strongly Agree'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [My manager encourages us to look for ways to improve our service]  IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]



UNION ALL 
--Q17

	SELECT 		'Q17' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN  [My manager ensures that people grow in their job by learning new]   IN (
						'Agree'
						,'Strongly Agree'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [My manager ensures that people grow in their job by learning new]  IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]


UNION ALL 
--Q19

	SELECT 		'Q19' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN  [My manager provides me with feedback on how I am performing]   IN (
						'Agree'
						,'Strongly Agree'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [My manager provides me with feedback on how I am performing]  IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		

UNION ALL 
--Q20

	SELECT 		'Q20' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN  [I feel my opinion is heard and valued by my manager]   IN (
						'Agree'
						,'Strongly Agree'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [I feel my opinion is heard and valued by my manager]  IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]

UNION ALL 
--Q21

	SELECT 		'Q21' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN  [I am empowered to deliver my job to a standard that I am happy w]   IN (
						'Agree'
						,'Strongly Agree'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [I am empowered to deliver my job to a standard that I am happy w]  IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		
		
UNION ALL 
--Q22

	SELECT 		'Q22' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN  [UHSM supports and invests in my health and wellbeing]   IN (
						'Agree'
						,'Strongly Agree'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [UHSM supports and invests in my health and wellbeing]  IS NOT NULL
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		
		
UNION ALL 
--Q23

	SELECT 		'Q23' as Question  
		,FinancialQuarter
		,CASE 
		WHEN [What Division are you based in?] = 'Clinical Support Services'
			THEN 'Clinical Support'
		WHEN [What Division are you based in?] IN (
				'Corporate'
				,'Education and Training'
				,'Charitable Funds'
				,'Research and Development'
				)
			THEN 'Corporate'
		ELSE [What Division are you based in?]
		END AS Division
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]
		,COUNT(RespondentID) AS Denominator 
		,Sum(CASE 
				WHEN  [In the last 12 months I have received an appraisal]   IN (
						'Yes'
						)
					THEN 1
				ELSE 0
				END) AS Numerator 
	FROM WHREPORTING.QLIK.DPR_StaffSurvey
	LEFT OUTER JOIN WHREPORTING.LK.Calendar ON DateAdd(day, Datediff(day, 0, DPR_StaffSurvey.StartDate), 0) = Calendar.TheDate
	WHERE [In the last 12 months I have received an appraisal] in ('Yes', 'No')
	GROUP BY FinancialQuarter
		,[What Division are you based in?]
		, [Clinical Support Services Directorate]
		, [Scheduled Care]
		, [Unscheduled Care Directorate]