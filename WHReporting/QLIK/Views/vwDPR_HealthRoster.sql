﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		View - QLIK.DPR_HealthRoster

Notes:			Stored in WHReporting.QLIK

				View to pull together all the Health Roster metrics required for the Divisional Dashboards

Versions:		
				1.0.0.0 - 21/04/2016 - CM - Job 
					Created view.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/


CREATE View [QLIK].[vwDPR_HealthRoster]
AS 


Select Fact.Division,
		Fact.Directorate,
		Fact.MetricID,
		Fact.Definition,
		Fact.QlikViewFlag,
		Fact.MonthStartDate,
		Fact.Numerator,
		Fact.Denominator, 
		Amber.Numerator as AmberNumerator,
		Amber.Denominator as AmberDenominator,
		Red.Numerator as RedNumerator,
		Red.Denominator as RedDenominator
		
From 
(
SELECT Directorate.Division
	,Department.Directorate
	,Metric.MetricID
	,Metric.Definition
	,Metric.QlikViewFlag
	,Calendar.MonthStartDate
	,Sum(Fact.Numerator) as Numerator
	,SUM(Fact.Denominator) as Denominator
FROM DW_REPORTING.HROST.Fact 
LEFT OUTER JOIN DW_REPORTING.HROST.Calendar ON Fact.FactDate = Calendar.WeekCommDate
LEFT OUTER JOIN DW_REPORTING.HROST.Metric ON Fact.MetricID = Metric.MetricID
LEFT OUTER JOIN DW_REPORTING.HROST.Department ON Fact.DepartmentID = Department.DepartmentID
LEFT OUTER JOIN DW_REPORTING.HROST.Directorate ON Department.Directorate = Directorate.Directorate
WHERE QlikViewFlag = 'Y'
--and Fact.MetricID = 1
Group by Directorate.Division
	,Department.Directorate
	, Metric.MetricID
	,Metric.QlikViewFlag
	,Metric.Definition
	,Calendar.MonthStartDate
) as Fact
Left outer join 
(SELECT Directorate.Division
	,Department.Directorate
	, Metric.MetricID
	,Metric.Definition
	,Calendar.MonthStartDate
	,Sum(Target.AmberNumerator) as Numerator
	,SUM(Target.Denominator) as Denominator
FROM DW_REPORTING.HROST.Target --this is the targets
LEFT OUTER JOIN DW_REPORTING.HROST.Calendar ON Target.WeekCommDate = Calendar.WeekCommDate
LEFT OUTER JOIN DW_REPORTING.HROST.Metric ON Target.MetricID = Metric.MetricID
LEFT OUTER JOIN DW_REPORTING.HROST.Department ON Target.DepartmentID = Department.DepartmentID
LEFT OUTER JOIN DW_REPORTING.HROST.Directorate ON Department.Directorate = Directorate.Directorate
WHERE QlikViewFlag = 'Y'
--and Target.MetricID = 1
Group by Directorate.Division
	,Department.Directorate
	, Metric.MetricID
	,Metric.Definition
	,Calendar.MonthStartDate) as Amber
on Fact.MetricID = Amber.MetricID and Fact.MonthStartDate = Amber.MonthStartDate and Fact.Directorate = Amber.Directorate
Left outer join 
(SELECT Directorate.Division
	,Department.Directorate
	, Metric.MetricID
	,Metric.Definition
	,Calendar.MonthStartDate
	,Sum(Target.RedNumerator) as Numerator
	,SUM(Target.Denominator) as Denominator
FROM DW_REPORTING.HROST.Target --this is the targets
LEFT OUTER JOIN DW_REPORTING.HROST.Calendar ON Target.WeekCommDate = Calendar.WeekCommDate
LEFT OUTER JOIN DW_REPORTING.HROST.Metric ON Target.MetricID = Metric.MetricID
LEFT OUTER JOIN DW_REPORTING.HROST.Department ON Target.DepartmentID = Department.DepartmentID
LEFT OUTER JOIN DW_REPORTING.HROST.Directorate ON Department.Directorate = Directorate.Directorate
WHERE QlikViewFlag = 'Y'
--and Target.MetricID = 1
Group by Directorate.Division
	,Department.Directorate
	, Metric.MetricID
	,Metric.Definition
	,Calendar.MonthStartDate
) as Red
on Fact.MetricID = Red.MetricID and Fact.MonthStartDate = Red.MonthStartDate and Fact.Directorate = Red.Directorate