﻿CREATE VIEW [QLIK].[vwDelayedDischargesICU]
AS

/*****************************************************************************
* View Name:			Events
* View Desc:			View to support reporting of Delayed Discharges for the Qlikview Patient Flow Dashboard. Logic taken from the script behind the excel version of the Patient Flow Dashboard
*						Where there is a wardstay on ICU with no end date and the stay is linkied to a critical care period which has been marked as 1 in the [Enhanced Nursing Care] field - these are the delays
* Dependancies		Lorenzo.dbo.APCStay
*					Lorenzo.dbo.AugmentedCarePeriod
*					Lorenzo.dbo.ProviderSpell
*					Lorenzo.dbo.ServicePointStay
*					Lorenzo.dbo.vwCriticalCareOrganSupport	
*
* Modification History --------------------------------------------------------
*
* Date			Author	Change
* 16/07/2015	CM		Created	

******************************************************************************/


SELECT DATEADD(day, DATEDIFF(day, 0, GetDate()-1), 0) AS SnapshotDate-- have entered snapshot date as getdate () -1 as this will be a snapshot as of close yesterday as Lorenzo extracts will only be as up to date as that
	,Sum(CASE 
			WHEN [Enhanced Nursing Care] = 1
				AND CCP.END_DTTM IS NULL
				THEN 1
			ELSE 0
			END) AS NoOfDelayedDischarges
FROM Lorenzo.dbo.APCStay
LEFT OUTER JOIN Lorenzo.dbo.AugmentedCarePeriod AS CCP ON Lorenzo.dbo.APCStay.APCDT_REFNO = CCP.AUGCP_REFNO
LEFT OUTER JOIN Lorenzo.dbo.ProviderSpell AS Spell ON CCP.PRVSP_REFNO = Spell.PRVSP_REFNO
LEFT OUTER JOIN Lorenzo.dbo.ServicePointStay AS SStay ON Lorenzo.dbo.APCStay.SSTAY_REFNO = SStay.SSTAY_REFNO
LEFT OUTER JOIN Lorenzo.dbo.vwCriticalCareOrganSupport AS OS ON Lorenzo.dbo.APCStay.APCST_REFNO = OS.APCST_REFNO
WHERE SStay.SPONT_REFNO_NAME = 'ACUTE - ICU'