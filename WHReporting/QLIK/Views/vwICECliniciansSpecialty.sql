﻿CREATE VIEW [QLIK].[vwICECliniciansSpecialty]
AS

--@startdate datetime = '20140401', @enddate datetime = '20141231'
--,  @Directorate varchar(400)
--, @Location varchar(max) 
--, @Clinician varchar(max)
--, @Provider varchar(50)
--, @ProviderDiscipline varchar(250) 
--, @abnormal varchar(5)
--, @cxr varchar(5)


SELECT distinct clin.ClinicianKey,
       ISNULL(clin.ClinicianForename,'') + ' '  + ISNULL(clin.ClinicianSurname,'') as ClinicianSurname,
       clin.ClinicianSpecialtyCode,
       Isnull(( CASE
				WHEN clin.ClinicianKey in ('293', '605','13099','507') then 'Womens & Childrens' --as per A Allcut request
				WHEN clin.ClinicianKey  = '593' then 'Disablement Services Centre' -- as per G Ryder request
                  WHEN sd.Direcorate = 'Cardio & Thoracic' THEN 'Cardiothoracic'
                  WHEN clin.clinicianspecialtycode IN ( '242', '500' ) THEN 'Womens & Childrens'
                  WHEN clin.clinicianspecialtycode IN ( '172' ) THEN 'Cardiothoracic'
                  WHEN clin.clinicianspecialtycode IN ( '653' ) THEN 'Therapies'
                  ELSE sd.Direcorate
                END ), 'Unknown')          AS Directorate,
       CASE	WHEN clin.ClinicianKey in ('293', '605','13099','507') then 'Breast Radiology' Else Isnull(sd.MainSpecialty, 'Unknown') end  AS Specialty,--Breast Radiology as per A Allcut request
        Case when clin.clinicianspecialtycode IN ( '242', '500' )  then 'Scheduled Care' 
        when clin.ClinicianKey  = '593' then  'Clinical Support'   
        when clin.ClinicianKey in ('293', '605','13099','507') then 'Scheduled Care' --as per A Allcut request
        else  sd.division end as Division
--into #RequestDoc
FROM   WHReporting.ICE.DimClinician clin
       LEFT OUTER JOIN lk.specialtydivision sd
                    ON sd.SpecialtyCode = Cast(( CASE
                                                   WHEN clin.ClinicianKey = 3460 THEN 370
                                                   ELSE clin.clinicianspecialtycode
                                                 END ) AS VARCHAR)