﻿CREATE VIEW [QLIK].[vwICEReports]
AS

--@startdate datetime = '20140401', @enddate datetime = '20141231'
--,  @Directorate varchar(400)
--, @Location varchar(max) 
--, @Clinician varchar(max)
--, @Provider varchar(50)
--, @ProviderDiscipline varchar(250) 
--, @abnormal varchar(5)
--, @cxr varchar(5)


SELECT distinct ReportIndex,
       RequestIndex,
       RequestDateTime,
       MasterClinicianCode,
       MasterPatientCode,
       MasterLocationCode,
       FileStatus.FilingStatus,
       CASE
                              WHEN FileStatus.FilingStatusKey = 0 THEN 1
                              ELSE 0
                            END as NotViewed,
        CASE
                             WHEN FileStatus.FilingStatusKey = 1 THEN 1
                             ELSE 0
                           END  as NotFiled,
     CASE
                          WHEN FileStatus.FilingStatusKey = 2 THEN 1
                          ELSE 0
                        END as Filed ,
       Total = 1,
       UserFiledKey,
       ReportDate,
       ReportTime,
       InpatientEpisodeUniqueID,
       OutpatientAppointmentUniqueID,
       Specialty,
       Latest,
       ServiceProviderCOde
       --, case when dbo.Ufntransposeinvestigations(FactReport.ReportIndex)  like '%XCHES%' then 1 else 0 end  as IsChestXray


FROM   WHREPORTING.ICE.FactReport
  LEFT OUTER JOIN  ICE.DimFilingStatus FileStatus
                    ON FactReport.FilingStatusKey = FileStatus.FilingStatusKey
WHERE  ( ReportDate >= '20140401')
       --AND Latest <> 0 --28/07/2014 CM: Commented this out as this is now incorrect logic after investigations with DAhearn.  Correct logic for identifying valid latest reports is via the use of the Category field in the line below
       AND Category is null -- Category 1 (Added), 2 (Updated), 3(Deleted) -- any reports which have an entry in this field should be excluded from the reporting as they will no longer be visible on front end and may have been replaced with a new report index int he back end.