﻿CREATE Procedure [RF].[BuildPatient] As

/*
--Changes
2014-03-04 KO Added ReferralSourceUniqueID and set as PK
*/

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

If Exists (Select * from INFORMATION_SCHEMA.TABLES Where Table_Schema = 'RF' and TABLE_NAME = 'Patient')
DROP TABLE RF.Patient;


CREATE TABLE RF.Patient
	(
	ReferralSourceUniqueID int not null
	,EncounterRecno int not null
	,SourcePatientNo int
	,FacilityID varchar(20)
	,NHSNo varchar(20)
	,NHSNoStatusCode varchar(20)
	,NHSNoStatus varchar(80)
	,NHSNoStatusNHSCode varchar(20)
	,PatientTitle varchar(20)
	,PatientForename varchar(30)
	,PatientSurname varchar(30)
	,DateOfBirth smalldatetime
	,DateOfDeath smallDatetime
	,PatientAddress1 varchar(50)
	,PatientAddress2 varchar(50)
	,PatientAddress3 varchar(50)
	,PatientAddress4 varchar(50)
	,PostCode varchar(25)
	,SexCode varchar(20) 
	,Sex varchar(80)
	,SexNHSCode varchar(5)
	,EthnicGroupCode varchar(20)
	,EthnicGroup varchar(80)
	,EthnicGroupNHSCode varchar(5)
	,ReligionCode varchar(20)
	,Religion varchar(80)
	,ReligionNHSCode varchar(5)
	);
alter table RF.Patient add PRIMARY KEY CLUSTERED (ReferralSourceUniqueID ASC)
Create Unique Index ixcPatient on RF.Patient(EncounterRecno);



Insert into RF.Patient
	(
	ReferralSourceUniqueID
	,[EncounterRecno]
	,[SourcePatientNo]
	,[FacilityID]
	,[NHSNo]
	--,[NHSNoStatusCode]
	--,[NHSNoStatus]
	--,[NHSNoStatusNHSCode]
	,[PatientTitle]
	,[PatientForename]
	,[PatientSurname]
	,[DateOfBirth]
	,[DateOfDeath]
	,[PatientAddress1]
	,[PatientAddress2]
	,[PatientAddress3]
	,[PatientAddress4]
	,[PostCode]
	,[SexCode]
	,[Sex]
	,[SexNHSCode]
	,[EthnicGroupCode]
	,[EthnicGroup]
	,[EthnicGroupNHSCode]
	,[ReligionCode]
	,[Religion]
	,[ReligionNHSCode]
	)
 
	(
	Select 
		Encounter.SourceUniqueID
		,Encounter.EncounterRecno
		,Encounter.SourcePatientNo
		,Encounter.DistrictNo
		,Encounter.NHSNumber
	--	,NHSN.NHSNoStatusLocalCode
	--	,NHSN.NHSNoStatusDescription
	--	,NHSN.NHSNoStatusNationalCode
		,Encounter.PatientTitle
		,Encounter.PatientForename
		,Encounter.PatientSurname
		,Encounter.DateOfBirth
		,Encounter.DateOfDeath
		,Encounter.PatientAddress1
		,Encounter.PatientAddress2
		,Encounter.PatientAddress3
		,Encounter.PatientAddress4
		,Encounter.Postcode
		,Sex.SexLocalCode
		,Sex.SexDescription
		,Sex.SexNationalCode
		,Eth.EthnicOriginLocalCode
		,Eth.EthnicOriginDescription
		,Eth.EthnicOriginNationalCode
		,Relgn.ReligionLocalCode
		,Relgn.ReligionDescription
		,Relgn.ReligionNationalCode
		
	
	FROM WHOLAP.dbo.BaseRF Encounter
	--left outer join dbo.vwPASNHSNoStatus NHSN
	--	on Encounter.NHSNumberStatusCode = NHSN.NHSNoStatusLocalCode
	left outer join dbo.vwPASEthnicOrigin Eth
		on Encounter.EthnicOriginCode = Eth.EthnicOriginCode
	left outer join dbo.vwPASReligion Relgn
		on Encounter.ReligionCode = Relgn.ReligionCode
	left outer join vwPASSex Sex
		on Encounter.SexCode = Sex.SexCode
	);

	select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

	select @Stats = 
		'Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

	exec WriteAuditLogEvent 'PAS - WHREPORTING RF.BuildPatient', @Stats, @StartTime