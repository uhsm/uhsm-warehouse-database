﻿CREATE Procedure [RF].[BuildReferral] As

/*
--Changes
2014-08-18 KO Added ReferralCompletionComments and ReferralStatusComments
*/
declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

If Exists (Select * from INFORMATION_SCHEMA.TABLES Where Table_Schema = 'RF' and TABLE_NAME = 'Referral')
DROP TABLE RF.Referral;

CREATE TABLE RF.Referral
(
	 EncounterRecno int not null
	,ReferralSourceUniqueID int null
	,ParentReferralSourceUniqueID int null
	,SourcePatientNo int null
	,SourceEncounterNo int NUll
	,FacilityID varchar(20) null
	,NHSNo varchar(20) null
	,RequestedService varchar(80) null
	,DecisionToReferDate Date null
	,DateOnReferrersWaitingList Date null
	,ReferralMedium varchar(80) null
	,ReferralReceivedDate date null
	,ReferralSourceCode varchar(25) null
	,ReferralSource varchar(80) null
	,ReferralSourceNHSCode varchar(50) null
	,PriorityCode varchar(25) null
	,Priority varchar(80) null
	,PriorityNHSCode varchar(50) null
	,ReferralReasonCode varchar(25)
	,ReferralReason varchar(80)
	,Division varchar(80) null
	,Directorate varchar(80) null
	,ReceivingSpecialtyCode varchar(25) null
	,ReceivingSpecialty varchar(80) null
	,[ReceivingSpecialtyCode(Function)] varchar(25) null
	,[ReceivingSpecialty(Function)] varchar(80) null
	,[ReceivingSpecialtyCode(Main)] varchar(25) null
	,[ReceivingSpecialty(Main)] varchar(80) null
	,[ReceivingSpecialtyCode(RTT)] varchar(50) null
	,ReceivingProfCarerTypeCode varchar(25) null
	,ReceivingProfCarerType varchar(80) null
	,ReceivingProfCarerCode varchar(80) null
	,ReceivingProfCarerName varchar(255) null
	,ReferrerOrganisationCode varchar(20) null
	,ReferrerOrganisationName varchar(255) null
	,ReferrerSpecialtyCode varchar(20) null
	,ReferrerSpecialty varchar(80) null
	,ReferrerCode varchar(25) null
	,ReferrerName varchar(255) null
	,CancelDate date null
	,CancelReasonCode varchar(25) null
	,CancelReason varchar(80) Null
	,CompletionDate date null
	,CompletionTime time(0) null
	,CompletionDateTime smalldatetime null
	,CompletionReasonCode varchar(25)
	,CompletionReason varchar(80)
	,EpisodeGPCode varchar(25)
	,EpisodeGP varchar(255) null
	,EpisodePracticeCode varchar(278) null
	,EpisodePractice varchar(278) null
	,EpisodePCTCode varchar(50) null
	,EpisodePCT varchar(278) null
	,EpisodeCCGCode varchar(50) null
	,EpisodeCCG varchar(278) null
	,EpisodePCTCCGCode varchar(10) null
	,EpisodePCTCCG varchar(255) null
	,PostCode varchar(25) null
	,ResidencePCTCode varchar(20) null
	,ResidencePCT varchar(255) null
	,ResidenceLocalAuthorityCode varchar(50)
	,ResidenceLocalAuthority varchar(100)
	,AgeCode varchar(50)
	,ReferralCreated smalldatetime null
	,ReferralCreatedByWhom varchar(80) null
	,ReferralUpdated smalldatetime null
	,ReferralUpdatedByWhom varchar(80) null
	,ReferralTypeCode int null
	,ReferralType varchar(100) null
	,ResponsibleCommissionerCCGOnly varchar(10) null
	,ReferralCompletionComments varchar(2000) null
	,ReferralStatusComments varchar(2000) null
	,ReferralComment varchar(2048)
	,OurActivityFlag varchar(1)
	,NotOurActProvCode varchar(10)
);


INSERT INTO RF.Referral
(
	 EncounterRecno
	,ReferralSourceUniqueID
	,ParentReferralSourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,FacilityID
	,NHSNo
	,RequestedService
	,DecisionToReferDate
	,DateOnReferrersWaitingList
	,ReferralMedium
	,ReferralReceivedDate
	,ReferralSourceCode
	,ReferralSource
	,ReferralSourceNHSCode
	,PriorityCode
	,Priority
	,PriorityNHSCode
	,ReferralReasonCode
	,ReferralReason
	,Division
	,Directorate
	,ReceivingSpecialtyCode
	,ReceivingSpecialty
	,[ReceivingSpecialtyCode(Function)]
	,[ReceivingSpecialty(Function)]
	,[ReceivingSpecialtyCode(Main)]
	,[ReceivingSpecialty(Main)]
	,[ReceivingSpecialtyCode(RTT)]
	,ReceivingProfCarerTypeCode
	,ReceivingProfCarerType
	,ReceivingProfCarerCode
	,ReceivingProfCarerName
	,ReferrerOrganisationCode
	,ReferrerOrganisationName
	,ReferrerSpecialtyCode
	,ReferrerSpecialty
	,ReferrerCode
	,ReferrerName
	,CancelDate
	,CancelReasonCode
	,CancelReason
	,CompletionDate
	,CompletionTime
	,CompletionDateTime
	,CompletionReasonCode
	,CompletionReason
	,EpisodeGPCode
	,EpisodeGP
	,EpisodePracticeCode
	,EpisodePractice
	,EpisodePCTCode
	,EpisodePCT
	,EpisodeCCGCode
	,EpisodeCCG
	,EpisodePCTCCGCode
	,EpisodePCTCCG
	,PostCode
	,ResidencePCTCode
	,ResidencePCT
	,ResidenceLocalAuthorityCode
	,ResidenceLocalAuthority
	,AgeCode
	,ReferralCreated
	,ReferralCreatedByWhom
	,ReferralUpdated
	,ReferralUpdatedByWhom
	,ReferralTypeCode
	,ReferralType
	,ResponsibleCommissionerCCGOnly
	,ReferralComment
	,OurActivityFlag 
	,NotOurActProvCode
)
(
	Select
		 Encounter.EncounterRecno
		,Encounter.SourceUniqueID
		--KO updated 23/02/2014
		--,Ref.PARNT_REFRL_REFNO
		,Ref.PARNT_REFNO
		,Encounter.SourcePatientNo
		,Encounter.SourceEncounterNo
		,Encounter.DistrictNo
		,Encounter.NHSNumber
		,actyp.ReferralRequestedServiceDescription
		,DecToRefDate = CAST(Ref.SENT_DTTM as Date)
		,DateOnReferrersWL = CAST(Ref.ORDTA_DTTM AS Date)
		,rfmed.ReferralMediumDescription
		,RefDate = Cast(Encounter.ReferralDate as date)
		--,Sorrf.ReferralSourceLocalCode
		--,Sorrf.ReferralSourceDescription
		--,Sorrf.ReferralSourceNationalCode
		,Sorrf.MainCode
		,Sorrf.ReferenceValue
		,Sorrf.MappedCode
		,prity.ReferralPriorityLocalCode
		,Prity.ReferralPriorityDescription
		,Prity.ReferralPriorityNationalCode
		--,Reasn.ReferralReasonLocalCode
		--,Reasn.ReferralReasonDescription
		,reasn.MainCode
		,reasn.ReferenceValue
		,RefToSpec.Division
		,RefToSpec.Direcorate
		,ReferredToSpecCode = RefToSpec.SubSpecialtyCode
		,ReferredToSpec = RefToSpec.SubSpecialty
		,RefToSpec.SpecialtyFunctionCode
		,RefToSpec.SpecialtyFunction
		,RefToSpec.MainSpecialtyCode
		,RefToSpec.MainSpecialty
		,RefToSpec.RTTSpecialtyCode
		,RefToProca.PRTYP_REFNO_MAIN_CODE
		,RefToProca.PRTYP_REFNO_DESCRIPTION
		,RefToCons.NationalConsultantCode
		,RefToCons.Consultant
		,heorg.MAIN_IDENT
		,heorg.[DESCRIPTION]
		,RefbySpec.SubSpecialtyCode
		,RefbySpec.SubSpecialty
		,ReferrerCode = Referrer.PROCA_REFNO_MAIN_IDENT
		,ReferrefName = Referrer.Surname  + ' ' + Referrer.FORENAME
		,CancelDate = Cast(Ref.CANCD_DTTM AS Date)
		--,canrs.ReferralCancelReasonLocalCode
		--,canrs.ReferralCancelReasonDescription
		,canrs.MainCode
		,canrs.ReferenceValue
		,Encounter.DischargeDate
		,DischargeTime = CAST(Encounter.DischargeTime as Time(0))
		,DischargeDateTime = Encounter.DischargeTime
		--,clors.ReferralCompleteReasonLocalCode
		--,clors.ReferralCompleteReasonDescription
		,clors.MainCode
		,clors.ReferenceValue
		,GP.PROCA_REFNO_MAIN_IDENT
		,GPName = GP.SURNAME + ' ' + GP.FORENAME 
		--,EpisodicPracticeCode = 
		--				Case When (charindex('-',Pract.Practice) <1) Then
		--					cast(Pract.PracticeCode AS varchar)
		--				Else
		--					LTRIM(RTRIM(LEFT(Pract.Practice,CHARINDEX('-',Pract.Practice) -2)))
		--				End
		--,EpisodicPractice = 
		--				Case When (charindex('-',Pract.Practice) <1) Then
		--					cast(Pract.Practice AS varchar)
		--				Else
		--					LTRIM(RTRIM(RIGHT(Pract.Practice,CHARINDEX('-',REVERSE(Pract.Practice)) -2)))
		--				End
		,EpisodicPracticeCode = 
						Case When (charindex('-',Practice.Practice) <1) Then
							cast(Practice.PracticeCode AS varchar)
						Else
							LTRIM(RTRIM(LEFT(Practice.Practice,CHARINDEX('-',Practice.Practice) -2)))
						End
		,EpisodicPractice = 
						Case When (charindex('-',Practice.Practice) <1) Then
							cast(Practice.Practice AS varchar)
						Else
							LTRIM(RTRIM(RIGHT(Practice.Practice,CHARINDEX('-',REVERSE(Practice.Practice)) -2)))
						End
		,Practice.PCTCode
		,Practice.PCT
		,Practice.CCGCode
		,Practice.CCGName
		,EpisodePCTCCGCode = 
			case 
				when Encounter.ReferralDate > '2013-03-31 00:00:00'
				then Practice.CCGCode
				else Practice.PCTCode
			end
		,EpisodePCTCCG = 
			case 
				when Encounter.ReferralDate > '2013-03-31 00:00:00'
				then Practice.CCGName
				else Practice.PCTName
			end
		,Encounter.Postcode
		,ResPCTCode = PostC.PCTCode
		,PCT.Organisation
		,PostC.LocalAuthorityCode
		,LA.[Local Authority Name]
		,Encounter.AgeCode
		,ReferralCreated = Encounter.PASCreated
		,ReferralCreatedByWhom = 
								Case When UserCreate.Code is null Then
									Encounter.PASCreatedByWhom
								Else
									UserCreate.[USER_NAME]
								End
		,ReferralUpdated = Encounter.PASUpdated
		,ReferralUpdatedByWhom = 
								Case When UserModif.Code is null Then
									Encounter.PASUpdatedByWhom
								Else
									UserModif.[USER_NAME]
								End
		,RefType.ReferralTypeCode
		,RefType.ReferralType
		,Encounter.ResponsibleCommissionerCCGOnly
		,Encounter.ReferralComment
		,Encounter.OurActivityFlag
		,Encounter.NotOurActProvCode
	from WHOLAP.dbo.BaseRF Encounter
	Left Outer Join WHOLAP.dbo.FactRF Encounterfact
		on Encounter.EncounterRecno = Encounterfact.EncounterRecno
	left Outer Join WHOLAP.dbo.olapPASReferralCategory RefType
		on Encounterfact.ReferralCategoryCode = RefType.ReferralCategoryCode
	Left outer Join Lorenzo.dbo.Referral Ref
		on Encounter.SourceUniqueID = Ref.REFRL_REFNO
		
	--KO amended 18/02/2014 - reference views in WHREPORTING have missing data
	--Left outer join vwPASSourceOfReferral Sorrf
	--	on Encounter.SourceOfReferralCode = Sorrf.ReferralSourceCode
	Left join WH.pas.ReferenceValue Sorrf
		on Encounter.SourceOfReferralCode = Sorrf.ReferenceValueCode
		
	Left Outer Join vwPASReferralPriority Prity
		on Encounter.PriorityCode = PRITY.ReferralPriorityCode
		
	--KO amended 20/02/2014 - reference views in WHREPORTING have missing data
	--left outer join vwPASReferralReason Reasn
	--	on Ref.REASN_REFNO = Reasn.ReferralReasonCode
	left join WH.pas.ReferenceValue reasn
		on Ref.REASN_REFNO  = reasn.ReferenceValueCode
		
	left outer join Organisation.dbo.Postcode PostC
		on REPLACE(Encounter.PostCode,' ','') = PostC.SlimPostcode
	left outer join Organisation.dbo.[Local Authority] LA
		on PostC.LocalAuthorityCode = LA.[Local Authority Code]
	left outer join Organisation.dbo.PCT PCT
		on PostC.PCTCode = PCT.OrganisationCode
	left outer join vwPASReferralRequestedService actyp
		on Ref.ACTYP_REFNO = actyp.ReferralRequestedServiceCode
	left outer join vwPASReferralMedium	rfmed
		on Ref.RFMED_REFNO = rfmed.ReferralMediumCode
	left outer join WHOLAP.dbo.SpecialtyDivisionBase RefToSpec
		on Encounter.SpecialtyCode = RefToSpec.SpecialtyCode
	left outer join Lorenzo.dbo.ProfessionalCarer RefToProca
		on Ref.REFTO_PROCA_REFNO = RefToProca.PROCA_REFNO
	left outer join WHOLAP.dbo.OlapPASConsultant RefToCons
		on Encounter.ConsultantCode = RefToCons.ConsultantCode
	left outer join Lorenzo.dbo.Organisation heorg
		on Ref.REFBY_HEORG_REFNO = heorg.HEORG_REFNO
	left outer join WHOLAP.dbo.SpecialtyDivisionBase RefbySpec
		On Ref.REFBY_SPECT_REFNO = RefbySpec.SpecialtyCode
	left outer join WH.PAS.ProfessionalCarerBase Referrer
		ON Ref.REFBY_PROCA_REFNO = Referrer.PROCA_REFNO 
		
	--KO amended 20/02/2014 - reference views in WHREPORTING have missing data
	--left outer join [vwPASReferralCancelReason] canrs
	--	on Ref.CANRS_REFNO = canrs.ReferralCancelReasonCode
	left join WH.pas.ReferenceValue canrs
		on Ref.CANRS_REFNO  = canrs.ReferenceValueCode
		
	--KO amended 20/02/2014 - reference views in WHREPORTING have missing data
	--left outer join vwPASReferralCompleteReason clors
	--	on Ref.CLORS_REFNO = clors.ReferralCompleteReasonCode
	left join WH.pas.ReferenceValue clors
		on Ref.CLORS_REFNO  = clors.ReferenceValueCode
		
	left outer join WH.PAS.ProfessionalCarerBase GP 
		on Encounter.EpisodicGpCode = GP.PROCA_REFNO	
	--left outer join WHOLAP.dbo.OlapPASPractice Pract
	--	on Encounter.RegisteredGpPracticeCode = Pract.PracticeCode
		
	left outer join WHOLAP.dbo.OlapPASPracticeCCG Practice
		--on Encounter.RegisteredGpPracticeCode = Pract.PracticeCode
		on case 
			when Encounter.EpisodicGpPracticeCode = -1
			then Encounter.RegisteredGpPracticeCode
			else Encounter.EpisodicGpPracticeCode
		end = Practice.PracticeCode
		
	left outer join Lorenzo.dbo.[User] UserCreate
			on Encounter.PASCreatedByWhom = UserCreate.CODE 
			and UserCreate.ARCHV_FLAG = 'N'
	left outer join Lorenzo.dbo.[User] UserModif
			on Encounter.PASUpdatedByWhom = UserModif.CODE 
			and UserModif.ARCHV_FLAG = 'N'
			

);



Create unique Clustered Index ixcRFReferral On RF.Referral(EncounterRecno);
Create Index ixRefDate on RF.Referral(ReferralReceivedDate);
Create Index ixRefSource on RF.Referral(ReferralSourceCode);
Create Index ixDivision on RF.Referral(Division);
Create Index ixDirectorate on RF.Referral(Directorate);
Create Index ixReceivingProfcarer on RF.Referral(ReceivingProfcarerCode);
Create Index ixNHSNo on RF.Referral(NHSNo);
Create Index ixFaciltyID on RF.Referral(FacilityID);
Create Index ixEpisodePractice on RF.Referral(EpisodePracticeCode);
Create Index ixEpisodePCT on RF.Referral(EpisodePCTCode);
Create Index ixReferrer on RF.Referral(ReferrerCode);
Create Index ixProfCarerType on RF.Referral(ReceivingProfCarerType);
Create Index ixCompletionDate on RF.Referral(CompletionDate);
Create Index ixPriorityCode on RF.Referral(PriorityCode);
Create Index ixRefTypeCode on RF.Referral(ReferralTypeCode);
CREATE NONCLUSTERED INDEX ixRefType ON [RF].[Referral] ([ReferralSourceUniqueID])
INCLUDE ([ReferralType]);

---------------------------------------------------------------------------------------
--Add comments
---------------------------------------------------------------------------------------
if Object_id('tempdb..#ReferralNotes', 'U') is not null
	drop table #ReferralNotes
	
select 
	notes_refno_note,
	sorce_refno,
	sorce_code, 
	notrl_refno
into #ReferralNotes
from Lorenzo.dbo.NoteRole  note
where note.SORCE_CODE in ('RFARC', 'RDCMT')
and ARCHV_FLAG = 'N'

update RF.Referral set
	ReferralCompletionComments = commComp.NOTES_REFNO_NOTE
from RF.Referral ref
left join #ReferralNotes commComp 
	on ref.ReferralSourceUniqueID = commComp.SORCE_REFNO and commComp.SORCE_CODE = 'RDCMT' 
	and not exists (
		select 1 from #ReferralNotes laterCommComp 
		where ref.ReferralSourceUniqueID = laterCommComp.SORCE_REFNO 
		and laterCommComp.SORCE_CODE = 'RDCMT' 
		and laterCommComp.notrl_refno > CommComp.notrl_refno
		)


update RF.Referral set
	ReferralStatusComments = commStat.NOTES_REFNO_NOTE
from RF.Referral ref
left join #ReferralNotes commStat 
	on ref.ReferralSourceUniqueID = commStat.SORCE_REFNO and commStat.SORCE_CODE = 'RFARC' 
	and not exists (
		select 1 from #ReferralNotes laterCommStat 
		where ref.ReferralSourceUniqueID = laterCommStat.SORCE_REFNO 
		and laterCommStat.SORCE_CODE = 'RFARC' 
		and laterCommStat.notrl_refno > commStat.notrl_refno
		)

drop table #ReferralNotes
------------------------------------------------------------------------
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'PAS - WHREPORTING RF.BuildReferral', @Stats, @StartTime