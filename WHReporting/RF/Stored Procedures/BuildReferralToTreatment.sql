﻿CREATE Procedure [RF].[BuildReferralToTreatment] As

/*
--Changes
2014-04-04 KO Added ReferralSourceUniqueID and set as PK
*/

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

If Exists (Select * from INFORMATION_SCHEMA.TABLES Where Table_Schema = 'RF' and TABLE_NAME = 'ReferralToTreatment')
DROP TABLE RF.ReferralToTreatment;




CREATE TABLE RF.ReferralToTreatment
	(
	ReferralSourceUniqueID int not null
	,EncounterRecno int not null
	,RTTPathwayID varchar(200)
	,RTTStartDate datetime
	,RTTEndDate Datetime
	,RTTSpecialtyCode varchar(5)
	,RTTCurrentStatusCode varchar(20)
	,RTTCurrentStatus varchar(80)
	,RTTCurrentStatusDate smalldatetime
	,OurActivityFlag varchar(1)
	,NotOurActProvCode varchar(10)
	);
alter table RF.ReferralToTreatment add PRIMARY KEY CLUSTERED (ReferralSourceUniqueID ASC)
Create Unique Index ixcReferralToTreatmemt on RF.ReferralToTreatment(EncounterRecno);



Insert into RF.ReferralToTreatment
	(
	ReferralSourceUniqueID
	,EncounterRecno
	,RTTPathwayID
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentStatusCode
	,RTTCurrentStatus
	,RTTCurrentStatusDate
	,OurActivityFlag 
	,NotOurActProvCode 
	)
 
	(
	Select 
		Encounter.SourceUniqueID
		,Encounter.EncounterRecno
		,Encounter.RTTPathwayID
		,Encounter.RTTStartDate
		,Encounter.RTTEndDate
		,Spec.RTTSpecialtyCode
		,CurrRTT.RTTStatusLocalCode
		,CurrRTT.RTTStatusDescription
		,Encounter.RTTCurrentStatusDate
		,Encounter.OurActivityFlag
		,Encounter.NotOurActProvCode
	FROM WHOLAP.dbo.BaseRF Encounter
	left outer join dbo.vwPASRTTStatus CurrRTT
		on Encounter.RTTCurrentStatusCode = CurrRTT.RTTStatusCode
	left outer join WHOLAP.dbo.SpecialtyDivisionBase Spec
		on Encounter.SpecialtyCode = Spec.SpecialtyCode	
	);


	select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

	select @Stats = 
		'Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

	exec WriteAuditLogEvent 'PAS - WHREPORTING RF.BuildReferralToTreatment', @Stats, @StartTime