﻿/****** Script for SelectTopNRows command from SSMS  ******/


/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		For SSRS report to show all Breast Surgery referrals which have been flagged as RM3 to help booking team double check all the referrals they have updated
				Also includes referals which have not been flagged with RM3 but have a responsible commissioner as 01G - Salford CCG so booking team can check through what potentially needs to be updated with RM3
				

Notes:			Stored in WHReporting 
Versions:		
				1.0.0.0 - 12/04/2016 - CM
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/


CREATE Procedure [RF].[ReportRM3FlaggedBreastSurgeryReferrals] @startdate datetime
AS 
--Declare @startdate datetime = '20160401'

Select ReferralFlagged =  Case when Referral.NotOurActProvCode = 'RM3' then Referral.NotOurActProvCode Else NULL END
		, Referral.NHSNo
		, Referral.FacilityID
		, rf.RTTPathwayID
		, PatientName =  ISNULL(Patient.PatientForename,'') + ' ' + ISNULL(Patient.PatientSurname,'')
		, Referral.DecisionToReferDate
		, Referral.ReferralReceivedDate
		, Referral.ReferralMedium
		, Referral.ReferralType
		, Referral.Priority
		, Referral.ReferralReason
		, Referral.ReceivingSpecialty
		, Referral.ReceivingProfCarerName
		, Referral.ReferrerName
		, Referral.ReferrerOrganisationCode
		, ReferringCCG = ISNULL(Referral.EpisodeCCGCode,'') + '-' + ISNULL(Referral.EpisodeCCG,'')
		, Referral.EpisodeCCGCode
		, Referral.ResponsibleCommissionerCCGOnly
		, Referral.ReferralComment
		, Referral.ReferralCompletionComments
		


from WHREPORTING.RF.Referral
Left outer join WHREPORTING.RF.Patient on Referral.ReferralSourceUniqueID = Patient.ReferralSourceUniqueID
Left outer join WH.RF.Encounter rf --join to this to get patient pathway id
on Referral.ReferralSourceUniqueID = rf.SourceUniqueID
where --(EpisodeCCGCode = '01G' or 
Referral.NotOurActProvCode = 'RM3'--this section of the where criteria may need changing once Booking Team have a process which they are following
and ReferralReceivedDate >= @startdate
and ReceivingSpecialtyCode = '103'--only want to flagg Breast Surgery referrals
Order by Referral.ReferralReceivedDate