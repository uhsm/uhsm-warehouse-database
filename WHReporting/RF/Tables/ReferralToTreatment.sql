﻿CREATE TABLE [RF].[ReferralToTreatment](
	[ReferralSourceUniqueID] [int] NOT NULL,
	[EncounterRecno] [int] NOT NULL,
	[RTTPathwayID] [varchar](200) NULL,
	[RTTStartDate] [datetime] NULL,
	[RTTEndDate] [datetime] NULL,
	[RTTSpecialtyCode] [varchar](5) NULL,
	[RTTCurrentStatusCode] [varchar](20) NULL,
	[RTTCurrentStatus] [varchar](80) NULL,
	[RTTCurrentStatusDate] [smalldatetime] NULL,
	[OurActivityFlag] [varchar](1) NULL,
	[NotOurActProvCode] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[ReferralSourceUniqueID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]