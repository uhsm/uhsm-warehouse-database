﻿/***********************************************************************************
 Description: Called by SSIS Package to extract the flex and freeze AHP RTT
              Incomplete data for the breach report.
 
 Modification History --------------------------------------------------------

 Date     Author      Change
 16/05/14 Tim Dean	  Created.

************************************************************************************/

CREATE Procedure [RTT].[AHPExtractIncomplete]
 @StartDate DATETIME,
 @EndDate DATETIME,
 @RefreshType VARCHAR(6)
as

SET NOCOUNT ON;

INSERT INTO WHREPORTING.RTT.AHPBreachReport
            (
             ActivityDate
	        ,RefreshType
            ,ReportType
            ,ReferralSourceUniqueID
            ,ReferralReceivedDate
            ,ReceivingSpecialtyCode
            ,ParentReferralSourceUniqueID
            ,ParentReferralReceivedDate
            ,RTTStatusCode
            ,RTTStatusDate
            ,ParentRTTStatusCode
            ,ParentRTTStatusDate
            ,ClockStopDate
            ,ClockRestartDate
            ,DaysWait
            ,ExtractDate
            )
SELECT
  ActivityDate = @StartDate
 ,RefreshType = @RefreshType
 ,ReportType = 'Incomplete'
 ,Referral.ReferralSourceUniqueID
 ,Referral.ReferralReceivedDate
 ,Referral.ReceivingSpecialtyCode
 ,ParentReferralSourceUniqueID = ParentReferral.SourceUniqueID
 ,ParentReferralReceivedDate = ParentReferral.ReferralDate
 ,RTTStatusCode = LastRTTHistoryStatus.RTTStatusCode
 ,RTTStatusDate = LastRTTHistory.StartDate
 ,ParentRTTStatusCode = ParentReferral.RTTStatusCode
 ,ParentRTTStatusDate = ParentReferral.StartDate
 ,ClockStopDate = NULL
 ,ClockRestartDate = CASE
                       WHEN ParentReferral.RTTStatusCode = '33' THEN ParentReferral.StartDate
                       WHEN ParentReferral.SourceUniqueID IS NOT NULL THEN ParentReferral.ReferralDate
                       WHEN LastRTTHistoryStatus.RTTStatusCode = '33' THEN LastRTTHistory.StartDate
                       ELSE Referral.ReferralReceivedDate
                     END
 ,DaysWait = CASE
               WHEN ParentReferral.RTTStatusCode = '33' THEN DATEDIFF(DAY,ParentReferral.StartDate,@EndDate)
               WHEN ParentReferral.SourceUniqueID IS NOT NULL THEN DATEDIFF(DAY,ParentReferral.ReferralDate,@EndDate)
               WHEN LastRTTHistoryStatus.RTTStatusCode = '33' THEN DATEDIFF(DAY,LastRTTHistory.StartDate,@EndDate)
               ELSE DATEDIFF(DAY,Referral.ReferralReceivedDate,@EndDate)
             END
 ,ExtractDate = GETDATE()
FROM WHREPORTING.RF.Referral

-- Join to WH.RF.Encounter to access columns not available in WHREPORTING.
INNER JOIN WH.RF.Encounter AS ReferralEncounter 
  ON Referral.ReferralSourceUniqueID = ReferralEncounter.SourceUniqueID

-- Join last RTT status before the end date.
LEFT JOIN WH.RTT.RTT LastRTTHistory
  ON (Referral.ReferralSourceUniqueID = LastRTTHistory.ReferralSourceUniqueID
      AND LastRTTHistory.EncounterRecno = (-- To work out the latest RTT status use the maximum EncounterRecno
                                           -- value for each referral.
                                           SELECT MAX(LastRTTHistoryLookup.EncounterRecno) MaxEncounterRecno
                                           FROM WH.RTT.RTT LastRTTHistoryLookup
                                           WHERE LastRTTHistoryLookup.StartDate < @EndDate
                                                 AND LastRTTHistoryLookup.ReferralSourceUniqueID = Referral.ReferralSourceUniqueID
                                           GROUP BY LastRTTHistoryLookup.ReferralSourceUniqueID                                               
                                          )) 

LEFT JOIN WH.RTT.RTTStatus LastRTTHistoryStatus
  ON (LastRTTHistory.RTTStatusCode = LastRTTHistoryStatus.InternalCode)       

-- Join valid parent referrals used to calculate waiting time.
LEFT JOIN (SELECT 
             ParentReferralEncounter.SourceUniqueID
             ,ParentReferralEncounter.ReferralDate
             ,LastRTTHistoryParent.StartDate
             ,LastRTTHistoryParentStatus.RTTStatusCode
           FROM WH.RF.Encounter ParentReferralEncounter 
           
           LEFT JOIN WH.PAS.Specialty ParentReferralSpecialty
             ON (ParentReferralEncounter.SpecialtyCode = ParentReferralSpecialty.SpecialtyCode)
           
           LEFT JOIN WH.RTT.RTT LastRTTHistoryParent
             ON (ParentReferralEncounter.SourceUniqueID = LastRTTHistoryParent.ReferralSourceUniqueID
                 AND LastRTTHistoryParent.EncounterRecno = (-- To work out the latest RTT status use the maximum EncounterRecno
                                                            -- value for each parent referral.
                                                            SELECT MAX(LastRTTHistoryParentLookup.EncounterRecno) MaxEncounterRecno
                                                            FROM WH.RTT.RTT LastRTTHistoryParentLookup
                                                            WHERE LastRTTHistoryParentLookup.StartDate < @EndDate
                                                                  AND LastRTTHistoryParentLookup.ReferralSourceUniqueID = ParentReferralEncounter.SourceUniqueID
                                                            GROUP BY LastRTTHistoryParentLookup.ReferralSourceUniqueID                                               
                                                           )) 

           LEFT JOIN WH.RTT.RTTStatus LastRTTHistoryParentStatus
             ON (LastRTTHistoryParent.RTTStatusCode = LastRTTHistoryParentStatus.InternalCode)
           
           WHERE -- Include parent referral if not had any excluded RTT status codes.
                 NOT EXISTS (SELECT 1
                             FROM WH.RTT.RTT ParentReferralRTTHistory
                             INNER JOIN WH.RTT.RTTStatus ParentReferralHistoryRTTStatus 
                              ON (ParentReferralRTTHistory.RTTStatusCode = ParentReferralHistoryRTTStatus.InternalCode)
                             WHERE ParentReferralRTTHistory.ReferralSourceUniqueID = ParentReferralEncounter.SourceUniqueID
                                   AND ParentReferralHistoryRTTStatus.RTTStatusCode IN ('30','31','32','34','35','36','98')
                                   AND ParentReferralRTTHistory.StartDate <= @EndDate -- Only included statuses dated before end date.
                            )
                 -- Include parent referral if national specialty code not in exlusion list. 
                 AND ParentReferralSpecialty.NationalSpecialtyCode NOT IN ( '501' -- Obstetrics.
                                                                           ,'560') -- Midwife Episode.          
          ) ParentReferral 
       ON (ReferralEncounter.ParentSourceUniqueID = ParentReferral.SourceUniqueID)   

WHERE -- Only include referrals for the following treatment function codes
      Referral.[ReceivingSpecialtyCode(Function)] IN ( '654' -- Dietetics.
                                                      ,'650' -- Physiotherapy.
                                                      ,'651' -- Occupational Therapy.
                                                      ,'653' -- Podiatry.
                                                      ,'652' -- Speech and Language Therapy  
                                                      ,'840') -- Audiology.
      AND(Referral.CompletionDate IS NULL  -- Referral has not been completed or completed after the end date.
           OR Referral.CompletionDate > @EndDate)
      AND (Referral.CancelDate IS NULL -- Referral has not been cancelled or cancelled after the end date. 
            OR Referral.CancelDate > @EndDate) 
      AND Referral.ReferralReceivedDate < @EndDate -- Referral has been received before the end date.
      -- Only include referrals that have not had specified RTT statuses before the end date.
      AND NOT EXISTS (SELECT 1 
                      FROM WH.RTT.RTT ReferralRTTHistoryLookup 
                      LEFT JOIN WH.RTT.RTTStatus ReferralRTTHistoryLookupStatus 
                        ON (ReferralRTTHistoryLookup.RTTStatusCode = ReferralRTTHistoryLookupStatus.InternalCode)       
                      WHERE ReferralRTTHistoryLookup.ReferralSourceUniqueID =Referral.ReferralSourceUniqueID
                            AND ReferralRTTHistoryLookup.StartDate < @EndDate
                            AND ReferralRTTHistoryLookupStatus.RTTStatusCode IN ('30','31','32','34','35','36','98')
                     ) 
      AND Referral.ReferralReceivedDate >= @StartDate                                                                         
;