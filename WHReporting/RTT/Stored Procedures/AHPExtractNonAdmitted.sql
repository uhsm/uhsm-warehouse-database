﻿/***********************************************************************************
 Description: Called by SSIS Package to extract the flex and freeze AHP RTT
              Non-Admitted data for the breach report.
 
 Modification History --------------------------------------------------------

 Date     Author      Change
 16/05/14 Tim Dean	  Created.

************************************************************************************/

CREATE Procedure [RTT].[AHPExtractNonAdmitted]
 @StartDate DATETIME,
 @EndDate DATETIME,
 @RefreshType VARCHAR(6)
as

SET NOCOUNT ON;

INSERT INTO WHREPORTING.RTT.AHPBreachReport
            (
             ActivityDate
	        ,RefreshType
            ,ReportType
            ,ReferralSourceUniqueID
            ,ReferralReceivedDate
            ,ReceivingSpecialtyCode
            ,ParentReferralSourceUniqueID
            ,ParentReferralReceivedDate
            ,RTTStatusCode
            ,RTTStatusDate
            ,ParentRTTStatusCode
            ,ParentRTTStatusDate
            ,ClockStopDate
            ,ClockRestartDate
            ,DaysWait
            ,ExtractDate
            )
SELECT
  ActivityDate = @StartDate
 ,RefreshType = @RefreshType
 ,ReportType = 'Non-Admitted'
 ,Referral.ReferralSourceUniqueID
 ,Referral.ReferralReceivedDate
 ,Referral.ReceivingSpecialtyCode
 ,ParentReferralSourceUniqueID = ParentReferral.SourceUniqueID
 ,ParentReferralReceivedDate = ParentReferral.ReferralDate
 ,RTTStatusCode = NULL
 ,RTTStatusDate = NULL
 ,ParentRTTStatusCode = NULL
 ,ParentRTTStatusDate = NULL
 ,ClockStopDate = ClockStop.StartDate
 ,ClockRestartDate = NULL
 ,DaysWait = DATEDIFF(DAY,COALESCE(ParentReferral.ReferralDate,Referral.ReferralReceivedDate),ClockStop.StartDate)
 ,ExtractDate = GETDATE()
FROM WHREPORTING.RF.Referral

  INNER JOIN WH.RF.Encounter AS ReferralEncounter 
        ON Referral.ReferralSourceUniqueID = ReferralEncounter.SourceUniqueID
 
  -- Only include referrals that have had a clock stop in the specified period. 
  INNER JOIN (
              SELECT 
                ReferralRTTHistory.ReferralSourceUniqueID
               ,MIN(ReferralRTTHistory.StartDate) StartDate
              FROM WH.RTT.RTT ReferralRTTHistory   
                LEFT JOIN WH.RTT.RTTStatus ReferralRTTHistoryStatus 
                         ON (ReferralRTTHistory.RTTStatusCode = ReferralRTTHistoryStatus.InternalCode)       
              WHERE ReferralRTTHistory.StartDate BETWEEN @StartDate AND @EndDate
                    AND ReferralRTTHistoryStatus.RTTStatusCode IN ('30','31','32','34','35','36')
              GROUP BY ReferralRTTHistory.ReferralSourceUniqueID
             ) ClockStop ON (ReferralEncounter.SourceUniqueID = ClockStop.ReferralSourceUniqueID)
 
  -- Join valid parent referrals used to calculate waiting time.
  LEFT JOIN (SELECT 
               ParentReferralEncounter.SourceUniqueID
              ,ParentReferralEncounter.ReferralDate
             FROM WH.RF.Encounter ParentReferralEncounter 
               LEFT JOIN WH.PAS.Specialty ParentReferralSpecialty
                    ON (ParentReferralEncounter.SpecialtyCode = ParentReferralSpecialty.SpecialtyCode)
             WHERE -- Include parent referral if not had any excluded RTT status codes.
                   NOT EXISTS (SELECT 1
                               FROM WH.RTT.RTT ParentReferralRTTHistory
                                 INNER JOIN WH.RTT.RTTStatus ParentReferralHistoryRTTStatus 
                                       ON (ParentReferralRTTHistory.RTTStatusCode = ParentReferralHistoryRTTStatus.InternalCode)
                               WHERE ParentReferralRTTHistory.ReferralSourceUniqueID = ParentReferralEncounter.SourceUniqueID
                                     AND (LEFT(ParentReferralHistoryRTTStatus.RTTStatusCode,1) = '3'
                                          OR ParentReferralHistoryRTTStatus.RTTStatusCode = '98')
                               )
                   -- Include parent referral if national specialty code not in exlusion list. 
                   AND ParentReferralSpecialty.NationalSpecialtyCode NOT IN ( '501' -- Obstetrics.
                                                                             ,'560') -- Midwife Episode.          
            ) ParentReferral 
       ON (ReferralEncounter.ParentSourceUniqueID = ParentReferral.SourceUniqueID)      

WHERE Referral.[ReceivingSpecialtyCode(Function)] IN ( '654' -- Dietetics.
                                                      ,'650' -- Physiotherapy.
                                                      ,'651' -- Occupational Therapy.
                                                      ,'653' -- Podiatry.
                                                      ,'652' -- Speech and Language Therapy  
                                                      ,'840') -- Audiology.
      AND Referral.ReferralReceivedDate >= CONVERT(DATETIME,'01/04/2014',103) -- Only include referrals after this date.
;