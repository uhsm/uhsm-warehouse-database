﻿CREATE procedure [RTT].[BuildAdmDischDataset_] as
--KO 0204/2014
--Columns required from DW WHREPORTING.APC.Spell formatted to InfoSQL adm_disch_dataset format

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

--drop table RTT.adm_disch_dataset
--if dropping set SPELL_REFNO as primary key
--alter table RTT.adm_disch_dataset alter column SPELL_REFNO varchar(30) NOT NULL
--alter table RTT.adm_disch_dataset add PRIMARY KEY CLUSTERED (SPELL_REFNO ASC)

truncate table RTT.adm_disch_dataset
insert into RTT.adm_disch_dataset

select 
	PAT_REF = cast(SourcePatientNo as varchar), 
	REF_REF = cast(ReferralSourceUniqueID as varchar), 
	SPELL_REFNO = cast(SourceSpellNo as varchar), 
	ADMISSION_DATE = AdmissionDateTime,
	RTT_CODE = RTTCode,
	SPEC_CODE = AdmissionSpecialtyCode,
	CONS_CODE = AdmissionConsultantCode,
	ADMISSION_METHOD_CODE = AdmissionMethodCode,
	DISCHAGE_DATE = DischargeDateTime,
	WLIST_REFNO = cast(WaitingListSourceUniqueID as varchar)
--into RTT.adm_disch_dataset
from APC.Spell


select @RowsInserted = @@Rowcount

select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'RTT - WHREPORTING BuildAdmDischDataset', @Stats, @StartTime