﻿CREATE procedure [RTT].[BuildFCEDataset_] as
--KO 03/04/2014
--Columns required from DW WHREPORTING.APC.Episode formatted to InfoSQL fce_dataset format
--additional data from INFORMATION_VER2.[dbo].[uspCreateFCEs] 

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

--drop table RTT.fce_dataset
--if dropping set EPI_REF as primary key
--alter table RTT.fce_dataset alter column EPI_REF varchar(30) NOT NULL
--alter table RTT.fce_dataset add PRIMARY KEY CLUSTERED (EPI_REF ASC)

truncate table RTT.fce_dataset
insert into RTT.fce_dataset (
	[EPI_REF]
	,[SPELL_REFNO]
	,[EPI_ORDER]
	,[DIAG_CODE1]
	,[PROC_CODE1]
	,[EPI_OUTCOME])
select 
	EPI_REF = cast(EpisodeUniqueID as varchar),
	SPELL_REFNO = cast(SourceSpellNo as varchar),
	EPI_ORDER = null,
	DIAG_CODE1 = PrimaryDiagnosisCode,
	PROC_CODE1 = PrimaryProcedureCode,
	EPI_OUTCOME = EpisodeOutcome
--into RTT.fce_dataset
from APC.Episode

select @RowsInserted = @@Rowcount

/*CREATE EPISODE ORDER IN IPM_FCES*/
declare 
@numeps as int,
@epnum as int
set @numeps = (
	select max(numeps)
	from (
		select spell_refno, count(spell_refno) as numeps
		from RTT.fce_dataset
		group by spell_refno
		) as geteps
	)
set @epnum = 1
while @epnum <= @numeps
begin
	update RTT.fce_dataset --set epi_order = null
	set epi_order = @epnum
	where cast(epi_ref as int) in(
		select ep_id
		from (
			select spell_refno, min(cast(epi_ref as int)) as ep_id
			from RTT.fce_dataset 
			where epi_order is null
			group by spell_refno
			) 
		as tmp_fces
		)

	set @epnum = @epnum+1
end


select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'RTT - WHREPORTING BuildFCEDataset', @Stats, @StartTime

--select * from APC.Episode where EpisodeUniqueID = 150435742