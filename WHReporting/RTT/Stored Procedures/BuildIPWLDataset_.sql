﻿CREATE procedure [RTT].[BuildIPWLDataset_] as
--KO 17/04/2014
--Columns required from DW WH.PTL.IPWL formatted to InfoSQL ipwl_dataset format


declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

--drop table RTT.IPWL_dataset
------if dropping set wlist_refno as primary key
--alter table RTT.IPWL_dataset alter column wlist_refno varchar(30) NOT NULL
--alter table RTT.IPWL_dataset add PRIMARY KEY CLUSTERED (wlist_refno ASC)
truncate table RTT.IPWL_dataset
insert into RTT.IPWL_dataset

select 
	wlist_refno = cast(SourceUniqueID as varchar), 
	ref_ref = cast(ReferralSourceUniqueID as varchar), 
	elective_admission_method = ElectiveAdmissionMethod, 
	planned_procedure = PlannedProcedure, 
	tci_date = TCIDate,
	removal_date = RemovalDate,
	entry_status = EntryStatus,
	general_comment = GeneralComment,
	admission_date = AdmissionDate,
	susp_to_date_pt_reason = SuspToDatePtReason,
	susp_pat_end_date = SuspPatEndDate,
	date_on_list = DateOnList,
	Suspension_Period_Pt_Reason = SuspensionPeriodPtReason
--into RTT.IPWL_dataset
from WH.PTL.IPWL

select @RowsInserted = @@Rowcount

select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'RTT - WHREPORTING BuildIPWLDataset', @Stats, @StartTime