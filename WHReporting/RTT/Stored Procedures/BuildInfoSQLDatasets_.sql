﻿CREATE procedure [RTT].[BuildInfoSQLDatasets_] as
/**
--KO 05/04/2014
the following sps are an interim build to re-create the columns required by 
procedures that call key InfoSQL datasets.

Once they are complete and tested, any required data will be added to the various
WHREPORTING tables and objects using them will be amended to use the DW column names.
**/


--select * from WH.dbo.Parameter where Parameter = 'BUILDINFOSQLCOPIES'
--select * from WH.dbo.Parameter where Parameter = 'BUILDWHREPORTINGDATE'

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

if 
	--Check not already run today
	(select DateValue from WH.dbo.Parameter where Parameter = 'BUILDINFOSQLCOPIES')
		< (select DateValue from WH.dbo.Parameter where Parameter = 'BUILDWHREPORTINGDATE')
	
	begin
	
		exec RTT.BuildNotesTable
		exec RTT.GetLatestRTTStatus
		exec RTT.BuildReferralsDataset
		exec RTT.BuildFCEDataset
		exec RTT.BuildAdmDischDataset
		exec RTT.BuildOutpatientDataset
		exec RTT.BuildIPWLDataset
		exec RTT.BuildWaitingListV1Dataset

		update WH.dbo.Parameter set DateValue = GETDATE() where Parameter = 'BUILDINFOSQLCOPIES'

		select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
		select @Stats = 
			'Total Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

		exec WriteAuditLogEvent 'RTT - WHREPORTING BuildInfoSQLDatasets', @Stats, @StartTime

	end