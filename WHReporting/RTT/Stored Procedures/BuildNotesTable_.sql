﻿CREATE procedure [RTT].[BuildNotesTable_] as
--KO 23/03/2014
----Pull only relevent comments from Lorenzo notes table

--drop table RTT.Notes
--if dropping set notrl_refno as primary key
--alter table RTT.Notes add PRIMARY KEY CLUSTERED (notrl_refno ASC)

truncate table RTT.Notes
insert into RTT.Notes
select 
	notes_refno_note,
	sorce_refno,
	sorce_code, 
	notrl_refno
--into RTT.Notes
from Lorenzo.dbo.NoteRole  note
where note.SORCE_CODE in ('RFARC', 'RDCMT')
and ARCHV_FLAG = 'N'
--PK = notrl_refno
--INdex = sorce_refno

--CREATE NONCLUSTERED INDEX idx_RTTNotes
--ON [RTT].[Notes] ([sorce_refno],[sorce_code],[notrl_refno])