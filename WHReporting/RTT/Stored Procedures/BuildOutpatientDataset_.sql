﻿CREATE procedure [RTT].[BuildOutpatientDataset_] as
--KO 04/04/2014
--Columns required from DW WHREPORTING.OP.Schedule formatted to InfoSQL outpatient_dataset format

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

--drop table RTT.outpatient_dataset
--if dropping set APPT_REF as primary key
--alter table RTT.outpatient_dataset alter column APPT_REF varchar(30) NOT NULL
--alter table RTT.outpatient_dataset add PRIMARY KEY CLUSTERED (APPT_REF ASC)

truncate table RTT.outpatient_dataset
insert into RTT.outpatient_dataset

select
	REF_REF = cast(sched.ReferralSourceUniqueID as varchar),
	PAT_REF = cast(sched.SourcePatientNo as varchar),
	APPT_REF = cast(sched.SourceUniqueID as varchar),
	APPT_DATE = cast(sched.AppointmentDate as datetime),
	APPT_TIME = sched.AppointmentDateTime,
	APPT_TYPE = sched.AppointmentType,
	CLINIC_CODE = sched.ClinicCode,
	--DATA_SOURCE,
	NHS_NUMBER = sched.NHSNo,
	PATIENT_FORENAME = pat.PatientForename,
	PATIENT_SURNAME = pat.PatientSurname,
	CONS_CODE = sched.ProfessionalCarerCode,
	SPEC_CODE = sched.SpecialtyCode,
	APPT_STATUS = sched.Outcome,
	APPT_OUTCOME = sched.AttendStatus,
	SOURCE_OF_REFERRAL = ref.ReferralSource, 
	REF_DATE = dateadd(day, 0, datediff(day, 0, ref.ReferralReceivedDate)),
	RTT_CODE = RTTPeriodStatusCode,
	MODIF_DATE = AppointmentUpdated,
	PT_FACIL_ID = pat.FacilityID
--into RTT.outpatient_dataset
from OP.Schedule sched
left join OP.Patient pat
	on pat.SourceUniqueID = sched.SourceUniqueID
left join OP.ReferralToTreatment rtt
	on rtt.SourceUniqueID = sched.SourceUniqueID
left join RF.Referral ref
	on ref.ReferralSourceUniqueID = sched.ReferralSourceUniqueID
where sched.IsWardAttender = 0
select @RowsInserted = @@Rowcount

--select 
--	sched.APPT_REF
--	,WHRepApptStatus = sched.APPT_OUTCOME 
--	,WHApptStatus = attnd.ReferenceValue
update RTT.outpatient_dataset set 
	APPT_OUTCOME = attnd.ReferenceValue
from RTT.outpatient_dataset sched
left join WH.OP.Encounter enc
	on enc.SourceUniqueID = sched.APPT_REF
left join WH.PAS.ReferenceValue attnd
	on attnd.ReferenceValueCode = enc.AppointmentStatusCode
--where isnull(sched.APPT_OUTCOME, 'xx') <> isnull(attnd.ReferenceValue, 'xx')


--select 
--	sched.APPT_REF
--	,WHRepApptOutcome = sched.APPT_STATUS 
--	,WHApptOutcome = scocm.ReferenceValue
update RTT.outpatient_dataset set 
	APPT_STATUS = scocm.ReferenceValue
from RTT.outpatient_dataset sched
left join WH.OP.Encounter enc
	on enc.SourceUniqueID = sched.APPT_REF
left join WH.PAS.ReferenceValue scocm
	on scocm.ReferenceValueCode = enc.DisposalCode
--where isnull(sched.APPT_STATUS, 'xx') <> isnull(scocm.ReferenceValue, 'xx')

--RTT Status taken from original source schedule table rather than source RTT table
--select 
--	sched.APPT_REF
--	,RTT_CODE_OLD = sched.RTT_CODE
--	,RTT_CODE = rttst.MainCode
--	,RTT_DESC = rttst.ReferenceValue
update RTT.outpatient_dataset set 
	RTT_CODE = rttst.MainCode
from RTT.outpatient_dataset sched
left join Lorenzo.dbo.ScheduleEvent enc
	on enc.SCHDL_REFNO = sched.APPT_REF
left join WH.PAS.ReferenceValue rttst
	on rttst.ReferenceValueCode = enc.RTTST_REFNO
--where isnull(sched.RTT_CODE, 'xx') <> isnull(rttst.MainCode, 'xx')


	
	
select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'RTT - WHREPORTING BuildOutpatientDataset', @Stats, @StartTime