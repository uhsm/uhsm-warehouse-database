﻿CREATE procedure [RTT].[BuildReferralsDataset_] as
--KO 18/02/2014
----Columns required from DW WHREPORTING.RF.REferral formatted to InfoSQL referrals_dataset format

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

--drop table RTT.referral_dataset
--if dropping set ref_ref as primary key
--alter table RTT.referral_dataset alter column ref_ref varchar(30) NOT NULL
--alter table RTT.referral_dataset add PRIMARY KEY CLUSTERED (ref_ref ASC)

truncate table RTT.referral_dataset
insert into RTT.referral_dataset

SELECT     
	PPID = rtt.RTTPathwayID, 
	DATA_SOURCE = 'IPM', 
	latestRTT.Latest_RTT_Code,
	latestRTT.RTT_Status_Date,
	latestRTT.Latest_RTT_Comments,
	latestRTT.Latest_RTT_Source,
	Date_On_Referrers_WL = cast(ref.DateOnReferrersWaitingList as datetime), 
	Ref_ref = cast(ref.ReferralSourceUniqueID as varchar), 
	Ref_by_spec = ref.ReferrerSpecialtyCode, 
	Ref_to_spec = ref.ReceivingSpecialtyCode, 
	Pat_ref = cast(ref.SourcePatientNo as varchar), 
	Creator = ref.ReferralCreatedByWhom, 
	Modifier = ref.ReferralUpdatedByWhom, 
	NHS_Number = ref.NHSNo, 
	FACIL_ID = pat.FacilityID,
	Source_of_referral = ref.ReferralSource, 
	NHS_Source_of_referral_Code = ref.ReferralSourceCode, 
	ref_date = cast(ref.ReferralReceivedDate as datetime),
	Ref_reason = ref.ReferralReason, 
	Ref_cancellation_date = cast(ref.CancelDate as datetime), 
	Ref_cancel_reason = ref.CancelReason, 
	ref_completion_date = cast(ref.CompletionDateTime as datetime),
	Ref_Completion_Reason = ref.CompletionReason, 
	Ref_priority = ref.Priority, 
	Ref_by_code = ref.ReferrerCode, 
	Ref_to_code = ref.ReceivingProfCarerCode, 
	National_code = 
		case 
			when ref.ReceivingProfCarerCode ='C32599886' 
			then 'C3259986' 
			else ref.ReceivingProfCarerCode 
		end,
	Doctor_type = ref.ReceivingProfCarerType, 
	Doctor_surname = 
			case
			when charindex(',', ref.ReceivingProfCarerName, 0) > 0
			then left(ref.ReceivingProfCarerName, charindex(',', ref.ReceivingProfCarerName, 0)-1)
			else ref.ReceivingProfCarerName
		end,

	Patient_forename = pat.PatientForename, 
	Patient_surname = pat.PatientSurname, 
	Date_of_Birth = pat.DateOfBirth, 
	ref.RequestedService, 
	Ref_GP = ref.ReferrerCode,
	Ref_GP_Name = ref.ReferrerName,
	Ref_Practice = ref.ReferrerOrganisationCode,
	PARNT_REF_REF = cast(ref.ParentReferralSourceUniqueID as varchar),

	REF_STATUS_COMMENTS = cast(null as varchar(1024)),
	REF_COMP_COMMENTS = cast(null as varchar(1024)),
	pat.PostCode,
	pat.DateOfDeath,
	ref.EpisodeGPCode,
	ref.EpisodeGP,
	ref.EpisodePracticeCode,
	ref.ResponsibleCommissionerCCGOnly
--into RTT.referral_dataset
from RF.Referral ref
join RF.Patient pat 
	on pat.ReferralSourceUniqueID = ref.ReferralSourceUniqueID
left join RF.ReferralToTreatment rtt 
	on rtt.ReferralSourceUniqueID = ref.ReferralSourceUniqueID
left join RTT.LatestRTTStatus latestRTT
	on latestRTT.REFRL_REFNO = ref.ReferralSourceUniqueID

select @RowsInserted = @@Rowcount
--add comments (most recent where duplicates)

update RTT.referral_dataset set
	REF_COMP_COMMENTS = commComp.NOTES_REFNO_NOTE
from RTT.referral_dataset ref
left join RTT.Notes commComp 
	on ref.Ref_ref = commComp.SORCE_REFNO and commComp.SORCE_CODE = 'RDCMT' 
and not exists (
	select 1 from RTT.Notes laterCommComp 
	where ref.Ref_ref = laterCommComp.SORCE_REFNO 
	and laterCommComp.SORCE_CODE = 'RDCMT' 
	and laterCommComp.notrl_refno > CommComp.notrl_refno
	)

update RTT.referral_dataset set
	REF_STATUS_COMMENTS = commStat.NOTES_REFNO_NOTE
from RTT.referral_dataset ref
left join RTT.Notes commStat 
	on ref.Ref_ref = commStat.SORCE_REFNO and commStat.SORCE_CODE = 'RFARC' 
and not exists (
	select 1 from RTT.Notes laterCommStat 
	where ref.Ref_ref = laterCommStat.SORCE_REFNO 
	and laterCommStat.SORCE_CODE = 'RFARC' 
	and laterCommStat.notrl_refno > commStat.notrl_refno
	)



select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'RTT - WHREPORTING BuildReferralsDataset', @Stats, @StartTime