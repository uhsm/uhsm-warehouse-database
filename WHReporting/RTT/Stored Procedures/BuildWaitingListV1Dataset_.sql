﻿CREATE procedure [RTT].[BuildWaitingListV1Dataset_] as
--KO 13/06/2014
--Columns required formatted to InfoSQL waiting_listv1 format


declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

--drop table RTT.waiting_listv1
----if dropping set wlist_refno as primary key
--alter table RTT.waiting_listv1 alter column WLRefNo int NOT NULL
--alter table RTT.waiting_listv1 add PRIMARY KEY CLUSTERED (WLRefNo ASC)
truncate table RTT.waiting_listv1
insert into RTT.waiting_listv1

select 
	WLRefNo = wl.wlist_refno, 
	ref_ref = wl.refrl_refno,
	pat_ref = wl.patnt_refno,
	Date_on_list = wl.wlist_dttm,
	Waiting_start_date = wl.waiting_start_dttm,
	wl.WLRUL_REFNO
	--r.CODE,
	--r.name
--into RTT.waiting_listv1
from Lorenzo.dbo.WaitingList wl
--left join Lorenzo.dbo.WaitingListRule r
--on r.WLRUL_REFNO = wl.WLRUL_REFNO
where wl.archv_flag = 'N' 
and wl.svtyp_refno = '4223'
and wl.remvl_dttm is null 
and isnull(wl.WLRUL_REFNO, 150000536) <> 150000536 --Follow up OPWL

select @RowsInserted = @@Rowcount

select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'RTT - WHREPORTING BuildWaitingListV1Dataset', @Stats, @StartTime