﻿CREATE PROCEDURE [RTT].[GetLatestRTTStatus_] AS
--Copied from INfoSQL.Information_Ver2.uspGetLatestRTTStatus
--KO 05/04/2014. Part of RTT build on v-uhsm-dw02

--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[RTT].[LatestRTTStatus]') AND type in (N'U'))
--DROP TABLE [RTT].[LatestRTTStatus]

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

truncate table RTT.LatestRTTStatus
insert into RTT.LatestRTTStatus

select 
	a.refrl_refno,
	c.main_code as Latest_RTT_Code,
	c.[description] as Latest_RTT_DESC,
	cast(a.rttst_date as datetime) as RTT_Status_Date,
	a.Comments as Latest_RTT_Comments,
	a.Sorce as Latest_RTT_Source
--into RTT.LatestRTTStatus
from Lorenzo.dbo.RTT A with (nolock) 
inner join
	(
	SELECT
	REFRL_REFNO,
	MAX(RTTPR_REFNO) AS LastStat
	FROM
	(
		SELECT 
		A.REFRL_REFNO,
		CAST(A.RTTST_DATE AS DATETIME) AS StatusDate,
		A.RTTPR_REFNO,
		A.RTTST_REFNO,
		A.SORCE
		FROM Lorenzo.dbo.RTT A WITH (NOLOCK) 
		INNER JOIN
			(
			select
			refrl_refno,
			max(CAST(RTTST_DATE AS DATETIME)) as LastStatDate
			from Lorenzo.dbo.RTT WITH (NOLOCK)
			where archv_flag = 'N'
			Group By Refrl_refno
			) B ON A.REFRL_REFNO = B.REFRL_REFNO AND CAST(A.RTTST_DATE AS DATETIME) = B.LastStatDate
		--ORDER BY A.REFRL_REFNO, A.RTTPR_REFNO
		) AS SKSK
	GROUP BY
	REFRL_REFNO
	) B on a.RTTPR_REFNO = B.LAstStat
inner join WH.PAS.ReferenceValueBase c on a.rttst_Refno = c.rfval_refno

select @RowsInserted = @@Rowcount
select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'RTT - WHREPORTING GetLatestRTTStatus', @Stats, @StartTime