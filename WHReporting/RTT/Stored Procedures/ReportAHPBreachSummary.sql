﻿/***********************************************************************************
 Description: Called by SSRS Report to extract summary data for AHP RTT Breach Report
 
 Parameters:  @ActivityDate - Start of the reporting month.
              @ReportType - Non-Admitted or Incomplete.
 
 Modification History --------------------------------------------------------

 Date     Author      Change
 19/05/14 Tim Dean	  Created.

************************************************************************************/

CREATE Procedure [RTT].[ReportAHPBreachSummary]
 @ActivityDate DATETIME,
 @ReportType VARCHAR(20)
as

SET NOCOUNT ON;

SELECT 
  SpecialtyDivision.SpecialtyFunctionCode
 ,SpecialtyFunction = MAX(SpecialtyDivision.SpecialtyFunction)
 ,FreezeNotBreached = SUM(CASE
                            WHEN data.RefreshType = 'Freeze' AND data.Breach = 'N' THEN 1
                            ELSE 0
                          END)
 ,FreezeBreached = SUM(CASE
                         WHEN data.RefreshType = 'Freeze' AND data.Breach = 'Y' THEN 1
                         ELSE 0
                       END)     
 ,FreezeDate = MAX(CASE
                     WHEN data.RefreshType = 'Freeze' THEN data.ExtractDate
                     ELSE 0
                   END) 
 ,FlexNotBreached = SUM(CASE
                          WHEN data.RefreshType = 'Flex' AND data.Breach = 'N' THEN 1
                          ELSE 0
                        END)     
 ,FlexBreached = SUM(CASE
                       WHEN data.RefreshType = 'Flex' AND data.Breach = 'Y' THEN 1
                       ELSE 0
                     END)
 ,FlexDate = MAX(CASE
                   WHEN data.RefreshType = 'Flex' THEN data.ExtractDate
                   ELSE 0
                 END)                                       
FROM (
      SELECT 
        ReceivingSpecialtyCode
       ,RefreshType
       ,ExtractDate
       ,Breach = CASE
                   WHEN DaysWait <= 126 THEN 'N'
                   ELSE 'Y'
                 END 
       ,Total = 1
      FROM WHREPORTING.RTT.AHPBreachReport
      WHERE ReportType = @ReportType
            AND ActivityDate = @ActivityDate
     ) data
  LEFT JOIN WHREPORTING.LK.SpecialtyDivision 
    ON (data.ReceivingSpecialtyCode = SpecialtyDivision.SpecialtyCode)
GROUP BY SpecialtyDivision.SpecialtyFunctionCode;