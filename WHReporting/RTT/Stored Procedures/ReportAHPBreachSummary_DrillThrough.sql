﻿/***********************************************************************************
 Description: Called by SSRS Report to extract drill through data 
              for AHP RTT Breach Report
 
 Parameters:  @ActivityDate - Start of the reporting month.
              @ReportType - Non-Admitted or Incomplete.
              @RefreshType - Freeze or Flex.
              @SpecialtyCode - Code for required specialty.
              @Breach - Y or N for 18 Week breach.
 
 Modification History --------------------------------------------------------

 Date     Author      Change
 20/05/14 Tim Dean	  Created.

************************************************************************************/

CREATE Procedure [RTT].[ReportAHPBreachSummary_DrillThrough]
 @ActivityDate DATETIME,
 @ReportType VARCHAR(20),
 @RefreshType VARCHAR(6),
 @SpecialtyCode VARCHAR(20),
 @Breach CHAR(1)
as

SET NOCOUNT ON;

SELECT 
  Referral.DistrictNo
 ,Referral.PatientSurname
 ,Referral.PatientForename
 ,data.ReferralReceivedDate
 ,RTTStatus.RTTStatus
 ,data.RTTStatusDate
 ,data.ClockStopDate
 ,data.ClockRestartDate
 ,data.DaysWait           
FROM (
      SELECT 
        AHPBreachReport.ActivityDate
       ,AHPBreachReport.RefreshType
       ,AHPBreachReport.ReportType
       ,AHPBreachReport.ReferralSourceUniqueID
       ,AHPBreachReport.ReferralReceivedDate
       ,AHPBreachReport.ReceivingSpecialtyCode
       ,AHPBreachReport.ParentReferralSourceUniqueID
       ,AHPBreachReport.ParentReferralReceivedDate
       ,AHPBreachReport.RTTStatusCode
       ,AHPBreachReport.RTTStatusDate
       ,AHPBreachReport.ParentRTTStatusCode
       ,AHPBreachReport.ParentRTTStatusDate
       ,AHPBreachReport.ClockStopDate
       ,AHPBreachReport.ClockRestartDate
       ,AHPBreachReport.DaysWait
       ,AHPBreachReport.ExtractDate
       ,Breach = CASE
                   WHEN AHPBreachReport.DaysWait <= 126 THEN 'N'
                   ELSE 'Y'
                 END 
      FROM WHREPORTING.RTT.AHPBreachReport
      WHERE ReportType = @ReportType
            AND RefreshType = @RefreshType
            AND ActivityDate = @ActivityDate
     ) data
  LEFT JOIN WHREPORTING.LK.SpecialtyDivision 
    ON (data.ReceivingSpecialtyCode = SpecialtyDivision.SpecialtyCode)
  LEFT JOIN WH.RF.Encounter Referral
    ON (data.ReferralSourceUniqueID = Referral.SourceUniqueID)
  LEFT JOIN WH.RTT.RTTStatus
    ON (data.RTTStatusCode = RTTStatus.RTTStatusCode)
WHERE SpecialtyDivision.SpecialtyFunctionCode = @SpecialtyCode
      AND data.Breach = @Breach;