﻿/*
--Author: N Scattergood
--Date created: 20/05/2014
--Stored Procedure Built for Excel Based Report on RTT Status Outcomes in Outpatients
-- Defaults to last 12 months
*/

CREATE Procedure [RTT].[ReportOutpatientRTTOutcomes]
as
SELECT 
      Cal.TheMonth as ApptMonth
      ,[ClinicCode]
      ,[Clinic]
      ,[ClinicLocation]
      ,[AppointmentType]
      ,[AppointmentTypeNHSCode]
      ,[ProfessionalCarerName]
      ,[Division]
      ,[Directorate]
      ,[SpecialtyCode]
      ,[Specialty]
      ,[SpecialtyCode(Function)]
      ,[Specialty(Function)]
      ,[AttendStatusCode]
      ,[AttendStatus]
      ,[AttendStatusNHSCode]
      ,[Status]
      ,[RTTStatusCode]
      ,Case 
      WHEN [RTTStatusCode] between '10' and '12'
      THEN 'The first ACTIVITY in a REFERRAL TO TREATMENT PERIOD'
      WHEN [RTTStatusCode] between '20' and '21'
      THEN 'Subsequent ACTIVITY during a REFERRAL TO TREATMENT PERIOD'
      WHEN [RTTStatusCode] between '30' and '36'
      THEN 'ACTIVITY that ends the REFERRAL TO TREATMENT PERIOD'
      WHEN [RTTStatusCode] between '90' and '98'
      THEN 'ACTIVITY that is not part of a REFERRAL TO TREATMENT PERIOD'
      WHEN [RTTStatusCode] = '99' 
      THEN 'ACTIVITY where the REFERRAL TO TREATMENT PERIOD STATUS is not yet known'
      ELSE 'Other (Null/Not Specified)' 
      END as 'RTT_Status_Cat'
      
      ,COUNT(*) as Activity
      
      
     

      
 --     ,  CASE 
 --     WHEN AttendStatus IN ('Refused to Wait','Patient Left / Not seen',
	--'Patient Late / Not Seen','Did Not Attend') 
	--OR (AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen') 
	--AND Outcome = 'Did Not Attend')
	--	THEN 'DNA'
		
	--	WHEN AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen')
	--AND (outcome IS NULL 
	--OR (Outcome NOT LIKE '%canc%' AND Outcome NOT LIKE '%Did Not Attend%')) 
	--THEN 'Attended'
  FROM [WHREPORTING].[OP].[Schedule] OP
  
	Left Join WHREPORTING.LK.Calendar Cal
	on Cal.TheDate = OP.AppointmentDate
  
  WHERE
  [AttendStatusNHSCode] between 2 and 7
  and
AppointmentType in ('New','Follow Up')

  AND
  AppointmentDate between
  (select
cast(
'1 '
+
DATENAME(MM,
(
DATEADD(MM,-12,getdate())
)
)
+ ' ' +
DATENAME(YY,
(
DATEADD(MM,-12,getdate())
)
) as Datetime)
) 
AND

dateadd(day,-1,CAST(getdate() as DATE))

Group By
Cal.TheMonth
 ,[ClinicCode]
      ,[Clinic]
      ,[ClinicLocation]
      ,[AppointmentType]
      ,[AppointmentTypeNHSCode]
      ,[ProfessionalCarerName]
      ,[Division]
      ,[Directorate]
      ,[SpecialtyCode]
      ,[Specialty]
      ,[SpecialtyCode(Function)]
      ,[Specialty(Function)]
      ,[AttendStatusCode]
      ,[AttendStatus]
      ,[AttendStatusNHSCode]
      ,[Status]
      ,[RTTStatusCode]
      ,Case 
      WHEN [RTTStatusCode] between '10' and '12'
      THEN 'The first ACTIVITY in a REFERRAL TO TREATMENT PERIOD'
      WHEN [RTTStatusCode] between '20' and '21'
      THEN 'Subsequent ACTIVITY during a REFERRAL TO TREATMENT PERIOD'
      WHEN [RTTStatusCode] between '30' and '36'
      THEN 'ACTIVITY that ends the REFERRAL TO TREATMENT PERIOD'
      WHEN [RTTStatusCode] between '90' and '98'
      THEN 'ACTIVITY that is not part of a REFERRAL TO TREATMENT PERIOD'
      WHEN [RTTStatusCode] = '99' 
      THEN 'ACTIVITY where the REFERRAL TO TREATMENT PERIOD STATUS is not yet known'
      ELSE 'Other (Null/Not Specified)' 
      END