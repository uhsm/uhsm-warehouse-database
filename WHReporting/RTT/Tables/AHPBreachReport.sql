﻿CREATE TABLE [RTT].[AHPBreachReport](
	[ActivityDate] [datetime] NOT NULL,
	[RefreshType] [varchar](6) NOT NULL,
	[ReportType] [varchar](20) NOT NULL,
	[ReferralSourceUniqueID] [int] NOT NULL,
	[ReferralReceivedDate] [datetime] NOT NULL,
	[ReceivingSpecialtyCode] [varchar](20) NOT NULL,
	[ParentReferralSourceUniqueID] [int] NULL,
	[ParentReferralReceivedDate] [datetime] NULL,
	[RTTStatusCode] [varchar](10) NULL,
	[RTTStatusDate] [datetime] NULL,
	[ParentRTTStatusCode] [varchar](10) NULL,
	[ParentRTTStatusDate] [datetime] NULL,
	[ClockStopDate] [datetime] NULL,
	[ClockRestartDate] [datetime] NULL,
	[DaysWait] [int] NOT NULL,
	[ExtractDate] [datetime] NULL,
 CONSTRAINT [PK_AHPBreachReport] PRIMARY KEY CLUSTERED 
(
	[ActivityDate] ASC,
	[RefreshType] ASC,
	[ReportType] ASC,
	[ReferralSourceUniqueID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]