﻿CREATE TABLE [RTT].[IPWL_dataset](
	[wlist_refno] [varchar](30) NOT NULL,
	[ref_ref] [varchar](30) NULL,
	[elective_admission_method] [varchar](25) NULL,
	[planned_procedure] [varchar](255) NULL,
	[tci_date] [datetime] NULL,
	[removal_date] [datetime] NULL,
	[entry_status] [varchar](20) NULL,
	[general_comment] [varchar](2000) NULL,
	[admission_date] [datetime] NULL,
	[susp_to_date_pt_reason] [int] NULL,
	[susp_pat_end_date] [datetime] NULL,
	[date_on_list] [datetime] NULL,
	[Suspension_Period_Pt_Reason] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[wlist_refno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]