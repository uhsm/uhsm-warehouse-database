﻿CREATE TABLE [RTT].[LatestRTTStatus](
	[refrl_refno] [numeric](18, 0) NULL,
	[Latest_RTT_Code] [varchar](25) NULL,
	[Latest_RTT_DESC] [varchar](80) NULL,
	[RTT_Status_Date] [datetime] NULL,
	[Latest_RTT_Comments] [varchar](255) NULL,
	[Latest_RTT_Source] [varchar](10) NULL
) ON [PRIMARY]