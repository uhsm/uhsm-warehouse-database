﻿CREATE TABLE [RTT].[Notes](
	[notes_refno_note] [varchar](1200) NULL,
	[sorce_refno] [numeric](18, 0) NULL,
	[sorce_code] [varchar](5) NULL,
	[notrl_refno] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK__Notes__E362B9F04F757405] PRIMARY KEY CLUSTERED 
(
	[notrl_refno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]