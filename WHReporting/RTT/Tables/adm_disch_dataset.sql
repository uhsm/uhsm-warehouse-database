﻿CREATE TABLE [RTT].[adm_disch_dataset](
	[PAT_REF] [varchar](30) NULL,
	[REF_REF] [varchar](30) NULL,
	[SPELL_REFNO] [varchar](30) NOT NULL,
	[ADMISSION_DATE] [datetime] NULL,
	[RTT_CODE] [varchar](25) NULL,
	[SPEC_CODE] [varchar](20) NULL,
	[CONS_CODE] [varchar](20) NULL,
	[ADMISSION_METHOD_CODE] [varchar](25) NULL,
	[DISCHAGE_DATE] [datetime] NULL,
	[WLIST_REFNO] [varchar](30) NULL
) ON [PRIMARY]