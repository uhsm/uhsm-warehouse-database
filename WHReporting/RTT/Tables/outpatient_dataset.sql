﻿CREATE TABLE [RTT].[outpatient_dataset](
	[REF_REF] [varchar](30) NULL,
	[PAT_REF] [varchar](30) NULL,
	[APPT_REF] [varchar](30) NOT NULL,
	[APPT_DATE] [datetime] NULL,
	[APPT_TIME] [smalldatetime] NULL,
	[APPT_TYPE] [varchar](80) NULL,
	[CLINIC_CODE] [varchar](20) NULL,
	[NHS_NUMBER] [varchar](20) NULL,
	[PATIENT_FORENAME] [varchar](30) NULL,
	[PATIENT_SURNAME] [varchar](30) NULL,
	[CONS_CODE] [varchar](20) NULL,
	[SPEC_CODE] [varchar](20) NULL,
	[APPT_STATUS] [varchar](80) NULL,
	[APPT_OUTCOME] [varchar](80) NULL,
	[SOURCE_OF_REFERRAL] [varchar](80) NULL,
	[REF_DATE] [datetime] NULL,
	[RTT_CODE] [varchar](20) NULL,
	[MODIF_DATE] [smalldatetime] NULL,
	[PT_FACIL_ID] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[APPT_REF] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]