﻿CREATE TABLE [RTT].[waiting_listv1](
	[WLRefNo] [int] NOT NULL,
	[ref_ref] [int] NULL,
	[pat_ref] [int] NULL,
	[Date_on_list] [smalldatetime] NULL,
	[Waiting_start_date] [smalldatetime] NULL,
	[WLRUL_REFNO] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[WLRefNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]