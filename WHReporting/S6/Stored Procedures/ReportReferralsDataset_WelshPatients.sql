﻿/*===============================================================================
Author:		J Pitchforth
Date:		23 Oct 2012
Function:	Populates the new SSRS OP Refs Schedule 5 Contract report
			(note that the commented text with tables "T1" and "T2" shows the
			legacy fields the report replaces).  
			
			There are duplicates within the data, caused by different organisations 
			referring the same patient.  There are 2 tests to remove these, see notes 
			below for more details.
			
Version:	1.0
=================================================================================
Author:		J Pitchforth
Date:		04 Dec 2012
Changes:	Changed criteria for referrals to be identified as a dupe (different day) from
			DATEDIFF(dd,min(Referral.ReferralReceivedDate),MAX(PreviousReferral.ReferralReceivedDate)) <-10) 
			to
			DATEDIFF(dd,min(Referral.ReferralReceivedDate),MAX(PreviousReferral.ReferralReceivedDate)) <=-10) 
			(comparison is now less than or equal to -10, rather than just less than -10.
Version:	1.1
=================================================================================
Author:		J Pitchforth
Date:		07 Dec 2012
Changes:	Added join to WHOLAP.dbo.BaseRF to get self-referring babies
Version:	1.2
=================================================================================
=================================================================================
Author:		N Scattergood
Date:		01 Nov 2013
Changes:	Adapted from OP.ReportOPRefsSchedule5ContractDeduplicated to meet Schedule 6 requirements for 'Other'
Version:	1.3
=================================================================================
Author:		N Scattergood
Date:		08 May 2014
Changes:	This Procedure is an adaptation of [OP].[ReportOPRefsSchedule5ContractDeduplicated]
			Amended to Meet Schedule 6 Requirements for 2014/15
Version:	1.4
=================================================================================
Author:		N Scattergood
Date:		03 Sept May 2014
Changes:	This Procedure is an adaptation of S6.ReportReferralsDataset
			Amended to Meet Welsh Requirements for 2014/15
Version:	1.5
=================================================================================*/


CREATE Proc [S6].[ReportReferralsDataset_WelshPatients]
	@StartDate date ,
	@EndDate date  

As 

---------------------------------------------------------------------------------
/*STEP 1: Identify referrals that are not duplicates (defined as from the same day, with the same
NHS number and the same specialism).  Referrals that do not appear in this list will be 
filtered out later.*/
create table #RemoveDupeReferralsSameDay 
(ReferralSourceUniqueID int)

insert into #RemoveDupeReferralsSameDay (ReferralSourceUniqueID)
	select	MIN(ReferralSourceUniqueID)
	from	RF.Referral
	where	(Referral.RequestedService = 'Outpatient Activity' or ReferralSource = 'General Practitioner Referral')
			and (CancelDate is null or Referral.CancelReasonCode = 'NSP') --not specified
			and Referral.ReferralReceivedDate between @StartDate and @EndDate --limit date range to improve performance
	Group by Referral.ReferralReceivedDate, Referral.NHSNo, Referral.[ReceivingSpecialtyCode(Function)]

--select * from #RemoveDupeReferralsSameDay 
---------------------------------------------------------------------------------
/*STEP 2: Identify referrals that are not duplicates (defined as within 10 days of each other, 
with the same NHS number and the same specialism).  Referrals that do not appear in this list will be 
filtered out later.*/
create table #RemoveDupeReferralsDifferentDay
(ReferralSourceUniqueID int not null,
CurrentReferralDate Date,
NHSNo varchar (20),
ReceivingSpecialtyCode varchar (25),
PreviousReferralDate Date,
TimeBetweenReferrals float,
Dupe Bit)

insert into #RemoveDupeReferralsDifferentDay
(ReferralSourceUniqueID,
CurrentReferralDate,
NHSNo,
ReceivingSpecialtyCode,
PreviousReferralDate,
TimeBetweenReferrals,
Dupe)

select 
Referral.ReferralSourceUniqueID,--Note: only ReferralSourceUniqueID is needed for the query, but the other fields will be useful for debugging
min(Referral.ReferralReceivedDate),
min(Referral.NHSNo),
Referral.[ReceivingSpecialtyCode(Function)],
MAX(PreviousReferral.ReferralReceivedDate) as Previous,
DATEDIFF(dd,min(Referral.ReferralReceivedDate),MAX(PreviousReferral.ReferralReceivedDate)),
case when (DATEDIFF(dd,min(Referral.ReferralReceivedDate),MAX(PreviousReferral.ReferralReceivedDate)) is null
or DATEDIFF(dd,min(Referral.ReferralReceivedDate),MAX(PreviousReferral.ReferralReceivedDate)) <=-10) 
then 0 else 1 end as Dupe



 from RF.Referral 

left join RF.Referral PreviousReferral
		on Referral.NHSNo = PreviousReferral.NHSNo
		and PreviousReferral.ReferralReceivedDate <Referral.ReferralReceivedDate
		and Referral.ReferralSourceUniqueID <> PreviousReferral.ReferralSourceUniqueID
		and Referral.[ReceivingSpecialtyCode(Function)] = PreviousReferral.[ReceivingSpecialtyCode(Function)]
		and (PreviousReferral.RequestedService = 'Outpatient Activity' or PreviousReferral.ReferralSource='General Practitioner Referral')
		and (PreviousReferral.CancelDate is null or PreviousReferral.CancelReasonCode = 'NSP') --not specified

		
where	(Referral.RequestedService = 'Outpatient Activity' or Referral.ReferralSource='General Practitioner Referral')
		and (Referral.CancelReasonCode = 'NSP' or Referral.CancelDate is null)  --not specified
	

group by 
		Referral.ReferralSourceUniqueID,
		Referral.NHSNo,
		Referral.[ReceivingSpecialtyCode(Function)],
		PreviousReferral.NHSNo

--having (DATEDIFF(dd,min(Referral.ReferralReceivedDate),MAX(PreviousReferral.ReferralReceivedDate)) is null
--		or DATEDIFF(dd,min(Referral.ReferralReceivedDate),MAX(PreviousReferral.ReferralReceivedDate)) >-10)

Order by MIN(referral.ReferralReceivedDate)
---------------------------------------------------------------------------------
--STEP 3: Final select statement (note inner joins to the 2 temp tables created earlier)


select 
Case 
when left(rtt.RTTPathwayID,1) <> 'R'
and DATALENGTH(rtt.RTTPathwayID) < 13
then rtt.RTTPathwayID	
else Null
end											as 'Unique Booking Reference Number (Converted)'
,rtt.RTTPathwayID							as 'Patient Pathway Identifier'
,convert(varchar,rtt.RTTStartDate,103)		as 'Referral to Treatment Period Start Date'
,Referral.NHSNo								as 'NHS Number'
,'N/A'										as 'Local Patient Identifier'
,Patient.DateOfBirth						as 'Person Birth Date'
,Patient.SexNHSCode							as 'Person Gender Code'
,isnull(Patient.EthnicGroupNHSCode,'99')	as 'Ethnic Category Code'
,Patient.PostCode
,case 
when left(Referral.ReceivingProfCarerCode,1) = 'C' 
and isnumeric(SUBSTRING(Referral.ReceivingProfCarerCode,2,1)) = 1
then Referral.ReceivingProfCarerCode	
else 'NULL' end								as 'Consultant Code'
,Referral.ReceivingProfCarerCode			as 'Local Consultant Code'
,Referral.[ReceivingSpecialtyCode(Main)]	as 'Main Specialty Code'
,case
when Referral.ReceivingSpecialtyCode = '103A'
then '103'
when Referral.ReceivingSpecialtyCode = '370C'
then '370'
when Referral.ReceivingSpecialtyCode = '110E'
then '110'
when Referral.ReceivingSpecialtyCode = '120N'
then '120'
when Referral.ReceivingSpecialtyCode = '9025'
then '320'
when Referral.ReceivingSpecialtyCode = '340G'
then '340'
when Referral.ReceivingSpecialtyCode = '350A'
then '350'
when Referral.ReceivingSpecialtyCode = '501B'
then '501'
when Referral.ReceivingSpecialtyCode = '9010'
then '650'
when Referral.ReceivingSpecialtyCode like '950%'
then ''
when Referral.ReceivingSpecialtyCode = '9046'
then '650'
else Referral.ReceivingSpecialtyCode
end											as 'Treatment Function Code'
,OPAtt.FirstAppointment						as 'Appointment Date'	
,'RM2'										as 'Provider Code'
 ,'RM202'									as 'Provider Site Code'
 ,Referral.EpisodeCCGCode					as 'Commissioner Code'
,Referral.EpisodeGPCode						as 'GP Code'
,Referral.EpisodePracticeCode				as 'GP Practice Code'
,Referral.PriorityNHSCode					as 'Priority Type Code'
,Referral.ReferralSourceNHSCode				as 'Source of Referral For Outpatients'
,Referral.ReferralSourceCode				as 'Local Source of Referral Code'
,Referral.ReferralReceivedDate				as 'Referral Request Received Date' 
,OPDNAC.[LatestDNAorCancellation]			as 'Last DNA or Patient Cancelled Date'
,Referral.ReferrerCode						as 'Referrer Code'
,Referral.ReferrerOrganisationCode			as 'Referring Organisation Code'
,'N/A'										as 'Referral Takeover Flag'
,Case 
when Referral.ReferralSourceCode = 'EBS'
then 1
else 0 
end											as 'Choose and Book'
,'N/A'										as 'Referral Gateway'
,'N/A'										as 'Count of Patients'
	
	



	 

	--Referral.ReferralSourceUniqueID,-- T1."REF_REF", FOR REFERENCE ONLY
	--Referral.[ReceivingSpecialtyCode(Function)],--T2."Ref_to_spec", 
	--Referral.ReceivingProfCarerCode, --T2."Ref_to_code", 
	--Referral.ReceivingProfCarerTypeCode, --T2."Doctor_type", 




from RF.Referral 
	inner join RF.Patient 
	on Referral.EncounterRecno = Patient.EncounterRecno
	--LEFT JOIN "Information_Reporting"."dbo"."referral_dataset" T2 
	--on T1."REF_REF" = T2."Ref_ref"
	
	inner join #RemoveDupeReferralsSameDay as RemoveDupeReferralsSameDay
		on Referral.ReferralSourceUniqueID = RemoveDupeReferralsSameDay.ReferralSourceUniqueID
	
	inner join #RemoveDupeReferralsDifferentDay as RemoveDupeReferralsDifferentDay
		on Referral.ReferralSourceUniqueID = RemoveDupeReferralsDifferentDay.ReferralSourceUniqueID
		and RemoveDupeReferralsDifferentDay.Dupe = 0
	
	left join WHOLAP.dbo.BaseRF 
	on Referral.ReferralSourceUniqueID = BaseRF.SourceUniqueID --added for v 1.2
		
		left join RF.ReferralToTreatment rtt 
			on rtt.ReferralSourceUniqueID = Referral.ReferralSourceUniqueID
		
		left join	(---Sub Query for First Appointment
				select 
					ReferralSourceUniqueID
					,MAX(AppointmentDate) as [FirstAppointment]
					from OP.Schedule
					where
					AppointmentTypeNHSCode = '1'
					and
					(
					AttendStatusNHSCode in ('5','6')
					or 
					AttendStatusNHSCode is null
					)
					and 
					AppointmentDate >= @StartDate
					Group by 
					ReferralSourceUniqueID
					)OPAtt
	on OPAtt.ReferralSourceUniqueID = Referral.ReferralSourceUniqueID

			left join (--Sub Query for	Latest DNA or Cancellation
					select 
						ReferralSourceUniqueID
						,MAX(AppointmentDate) as [LatestDNAorCancellation]
						from OP.Schedule
						where
						AppointmentTypeNHSCode = '1'
						and
						(
						AttendStatusNHSCode in ('2','3','7')
						)
						and 
						AppointmentDate >= @StartDate 
						--'01 apr 2014'
						Group by 
						ReferralSourceUniqueID
						)OPDNAC
			on Referral.ReferralSourceUniqueID = OPDNAC.ReferralSourceUniqueID
				

	
Where 
Referral.ReferralReceivedDate between @StartDate and @EndDate
	and 
	(Referral.RequestedService = 'Outpatient Activity' or ReferralSource='General Practitioner Referral')
	and 
	(Referral.CancelReasonCode = 'NSP' or CancelDate is null)  --not specified
	and 
	BaseRF.SelfReferringBaby = 0  --added for v 1.2
	and 
	CancelDate is null
	and
	left(Referral.EpisodeCCGCode,1) = '7'
	
order by 

'Treatment Function Code'


---------------------------------------------------------------------------------
--STEP 4: Tidy up

drop table #RemoveDupeReferralsDifferentDay
drop table #RemoveDupeReferralsSameDay