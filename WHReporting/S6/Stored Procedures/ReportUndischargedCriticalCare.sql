﻿--USE [WHREPORTING]
--GO
--/****** Object:  StoredProcedure [S6].[ReportUndischargedCriticalCare]    Script Date: 02/16/2015 08:27:29 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

--/*
----Author: N Scattergood	
----Date created: 09/05/2014
----Stored Procedure Built for Report on:
----Patients Undischarged Critical Care
----Returns spells where with a stay on Critical Care
----which were undischarged at the end of the previous month
----(even where they have subsequently been discharged)
----adapted from a report John C originally wrote
----For Schedule 6 it now only includes those who have been on Crit Care for longer than 10 days

--Amendments
--1. 22nd December - BIC and SCB taken out of the query at the request of Liesl Hacker as this will be included as Spec Comm
--*/

CREATE Procedure [S6].[ReportUndischargedCriticalCare] @date datetime 
as

--Declare @date datetime = '20150217' 
select
NHSNo as 'NHS Number',
spell.PracticeCode as 'GP Practice Code',
coalesce(spell.PurchaserCode,spell.SpellCCGCode) as 'Commissioner Code', 
--Spell.SourceSpellNo,
--FaciltyID,
--AdmissionMethod,
--AdmissionDate,
--DischargeDate,
'Current Length of Stay'--LoS at MonthEnd
=DATEDIFF(day,admissiondatetime,cast((dateadd(day,-1*((DATEPART(day,@date))),@date)) as DATE)),
--LOSAsAtToday=sum(ccpstay),
'Number of Undischarged CC* Days' =-- as at Month End
SUM(
DATEDIFF(day,cast(ccperiod.CriticalCareStartDatetime as date),
(CASE
when CriticalCareEndDatetime is null 
then cast((dateadd(day,-1*((DATEPART(day,@date))),@date)) as DATE)
when cast(CriticalCareEndDatetime as DATE) <= cast((dateadd(day,-1*((DATEPART(day,@date))),@date)) as DATE)
then CriticalCareEndDatetime
else cast((dateadd(day,-1*((DATEPART(day,@date))),@date)) as DATE)
end)
)
)+1

--,cast(ccperiod.CriticalCareStartDatetime as date)
--,max(cast(ccperiod.CriticalCareEndDatetime as date))
----,stay.SourceCCPNo

from

APC.Spell spell 

inner join APC.CriticalCarePeriod ccperiod 
on ccperiod.SourceSpellNo = spell.SourceSpellNo

inner join (select Distinct SourceCCPNo,SPONT_REFNO_CODE from Lorenzo.dbo.ExtractCriticalCarePeriodStay) stay 
on stay.SourceCCPNo=ccperiod.SourceCCPNo

LEFT JOIN WHREPORTING.LK.Calendar cdis 
on cdis.TheDate = DischargeDate



where 
coalesce(DischargeDate,@date) > cast((dateadd(day,-1*(DATEPART(day,@date)),@date)) as DATE)
and 
admissionDatetime < cast((dateadd(day,-1*((DATEPART(day,@date))-1),@date)) as DATE)
and
ccperiod.CriticalCareStartDatetime < cast((dateadd(day,-1*((DATEPART(day,@date))-1),@date)) as DATE)
and 
SPONT_REFNO_CODE in ('ICA','CTCU','CT Transplant')
--and 
--spell.SourceSpellNo  in 
--('150603208',
--'150609784',
--'150609875',
--'150610713',
--'150610922',
--'150612071',
--'150612447'
--)

group by

NHSNo ,
spell.PracticeCode,
coalesce(spell.PurchaserCode,spell.SpellCCGCode),
DATEDIFF(day,admissiondatetime,cast((dateadd(day,-1*((DATEPART(day,@date))),@date)) as DATE))
--,ccperiod.CriticalCareStartDatetime
--,ccperiod.CriticalCareEndDatetime
--,Spell.SourceSpellNo
--FaciltyID,
--AdmissionMethod,
--AdmissionDate,
--,DischargeDate

Having
SUM(
DATEDIFF(day,cast(ccperiod.CriticalCareStartDatetime as date),
(CASE
when CriticalCareEndDatetime is null 
then cast((dateadd(day,-1*((DATEPART(day,@date))),@date)) as DATE)
when cast(CriticalCareEndDatetime as DATE) <= cast((dateadd(day,-1*((DATEPART(day,@date))),@date)) as DATE)
then CriticalCareEndDatetime
else cast((dateadd(day,-1*((DATEPART(day,@date))),@date)) as DATE)
end)
)
)+1
 > 9
--Order by spell.SourceSpellNo

--union


--select
--NHSNo,
--spell.PracticeCode,
--coalesce(spell.PurchaserCode,spell.SpellCCGCode),

----Spell.SourceSpellNo,
----FaciltyID,


----AdmissionMethod,
----AdmissionDate,
----DischargeDate,
----ccperiod.StartTime,
----ccperiod.EndTime,
------LOSAsAtToday=sum(ccpstay),
----WardCode,
--'Current Length of Stay'--LoS at MonthEnd
--=DATEDIFF(day,admissiondatetime,cast((dateadd(day,-1*((DATEPART(day,getdate()))),getdate())) as DATE)),
----LOSAsAtToday=sum(ccpstay),
--'Number of Undischarged CC* Days' =-- as at Month End
--SUM(
--DATEDIFF(day,cast(ccperiod.StartTime as date),
--(CASE
--when ccperiod.EndTime is null 
--then cast((dateadd(day,-1*((DATEPART(day,getdate()))),getdate())) as DATE)
--when cast(ccperiod.EndTime as DATE) <= cast((dateadd(day,-1*((DATEPART(day,getdate()))),getdate())) as DATE)
--then ccperiod.EndTime
--else cast((dateadd(day,-1*((DATEPART(day,getdate()))),getdate())) as DATE)
--end)
--)
--)+1

--from

--APC.Spell spell 
--inner join APC.WardStay ccperiod 
--on ccperiod.SourceSpellNo = spell.SourceSpellNo

--LEFT JOIN WHREPORTING.LK.Calendar cdis 
--on cdis.TheDate=DischargeDate

--								--left join [PbR2013].[dbo].[Transplant] T
--								--on Spell.SourceSpellNo = T.SourceSpellNo 
--								--and T.VisitTypeCode = 'TRANSPLANT'
								
--								--left join [PbR2013].[dbo].[VAD] V
--								--on Spell.SourceSpellNo = V.SourceSpellNo 
								
--								--left join [PbR2013].dbo.ECMO E
--								--on E.SourceSpellNo = spell.SourceSpellNo

--where 
--coalesce(DischargeDate,getdate()) > cast((dateadd(day,-1*(DATEPART(day,getdate())),getdate())) as DATE)
--and 
--admissionDatetime < cast((dateadd(day,-1*((DATEPART(day,getdate()))-1),getdate())) as DATE)
--and
--ccperiod.StartTime < cast((dateadd(day,-1*((DATEPART(day,getdate()))-1),getdate())) as DATE)
--and
--WardCode in('BIC','SCB')
----and SPONT_REFNO_CODE<>'ICA'

--Group by
--NHSNo,
--spell.PracticeCode,
--coalesce(spell.PurchaserCode,spell.SpellCCGCode),
--DATEDIFF(day,admissiondatetime,cast((dateadd(day,-1*((DATEPART(day,getdate()))),getdate())) as DATE))

--Having
--SUM(
--DATEDIFF(day,cast(ccperiod.StartTime as date),
--(CASE
--when ccperiod.EndTime is null 
--then cast((dateadd(day,-1*((DATEPART(day,getdate()))),getdate())) as DATE)
--when cast(ccperiod.EndTime as DATE) <= cast((dateadd(day,-1*((DATEPART(day,getdate()))),getdate())) as DATE)
--then ccperiod.EndTime
--else cast((dateadd(day,-1*((DATEPART(day,getdate()))),getdate())) as DATE)
--end)
--)
--)+1 > 9

--order by
--NHSNo