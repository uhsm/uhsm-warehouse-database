﻿/*
--Author: N Scattergood	
--Date created: 09/05/2014
--Stored Procedure Built for Report on:
--Patients Undischarged Long LOS
--Returns spells which were undischarged at the end of a given month
--(even where they have subsequently been discharged)

Amendments
1. 22nd December - Specialty Codes Paed Card 321,Neurology 400 and Burns 161 taken out of the query at the request of Liesl Hacker as this will be included as Spec Comm
*/

CREATE Procedure [S6].[ReportUndischargedLongLOS]
@EndDate as Date	
as
 
select 

Spell.NHSNo											as 'NHS Number',
spell.PracticeCode									as 'GP Practice Code',
coalesce(spell.PurchaserCode,spell.SpellCCGCode)	as 'Commissioner Code', 
Spell.AdmissiontWardCode							as 'Admission Ward Code',
DATEDIFF(day,Spell.AdmissionDate,@EndDate)			as 'Current Length of Stay',
Spell.[AdmissionSpecialtyCode(Function)]			as 'Admission Speciality Code',
Spell.AdmissionMethodNHSCode						as 'Admission Type'				
--,Spell.DischargeDate

	--AdmissionType = 
	--	Case	when Spell.InpatientStayCode = 'D' then 'Day Case' 
	--			when Spell.AdmissionType in ('Emergency','Non-Elective','Maternity') then 'Non Elective'
	--			else 'Elective' end,
	--,
	--Spell.NHSNo,
	--Spell.SourceSpellNo,
	--Spell.FaciltyID,
	--Patient.DateOfBirth,
	--Spell.AdmissiontWardCode,
	--Spell.GPCode,
	--Spell.PracticeCode,
	--Spell.AdmissionDate,
	--Spell.DischargeDate,
	--Spell.[AdmissionSpecialtyCode(Function)],
	--Spell.LengthOfSpell,
	--DATEDIFF(day,Spell.AdmissionDate,@EndDate) as 'LoSAtMonthEnd',
	--case 
	--when CysticFibrosisRegister.DistrictNo is null 
	--then null 
	--else 'CF Patient' 
	--end as CFPatient
	
from WHReporting.APC.Spell 

	--left join WHREPORTING.APC.Patient 
	--on Spell.EncounterRecno = Patient.EncounterRecno
	
	--left join PbR2013.dbo.CysticFibrosisRegister 
	--on Spell.FaciltyID = 'RM2'+cast(CysticFibrosisRegister.DistrictNo as nvarchar(20)) 
	----left join PbR2013.dbo.CysticFibrosisRegister on RIGHT(len(Spell.FacilityID)-3,Spell.FaciltyID)=CysticFibrosisRegister.DistrictNo
	----inner join WHReporting.APC.Episode on Spell.SourceSpellNo = Episode.SourceSpellNo
	----inner join WH.APC.Encounter on Episode.EncounterRecno = Encounter.EncounterRecno

where 
Spell.AdmissionDate < @EndDate
and
(Spell.DischargeDate is null or spell.DischargeDateTime > @EndDate)
and 
DATEDIFF(day,Spell.AdmissionDate,@EndDate) > 27
and 
PurchaserCode not in ('TDH00','YDD82')
and
Spell.[AdmissionSpecialtyCode(Function)] not in ('321','400','161')


order by 
DATEDIFF(day,Spell.AdmissionDate,@EndDate) asc,
SpellCCGCode