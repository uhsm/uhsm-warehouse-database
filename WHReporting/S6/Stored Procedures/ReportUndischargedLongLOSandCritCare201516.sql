﻿/*
--Author: N Scattergood	
--Date created: 29/05/2014
--Stored Procedure Built for Report on:
--Patients Undischarged Long LOS and Critical Care Patients
--Returns spells which were undischarged at the end of a given month
--(even where they have subsequently been discharged)

From Document
19.	Where Trusts are not already capturing this information through SLAM on a monthly basis, 
a separate data set should be submitted to allow Commissioners the opportunity to accrue. 
Critical Care can be very costly and is chargeable on a daily basis so if Providers are not 
charging until patient discharge it can have a significant impact on Commissioners financial 
position / forecast. It is appreciated that until patients are discharged full clinical coding
has not taken place, however the opportunity for Commissioners to be aware of approximate 
charges in advance would be beneficial.

20.	This data set should be completed for current inpatients (i.e. as at last day of month) in
critical care that have a length of stay in ICU of 10 days or greater, and for current inpatients 
that have a length of stay greater than 28 days. 


Amendments
1. 22nd December 2014 - Specialty Codes Paed Card 321,Neurology 400 and Burns 161 taken out of the query at the request of Liesl Hacker as this will be included as Spec Comm
*/

CREATE Procedure [S6].[ReportUndischargedLongLOSandCritCare201516]
 @ReportDate as Date 	
as
 
Select * From
(
 
select 

Spell.NHSNo											as 'NHS Number',
spell.PracticeCode									as 'GP Practice Code',
coalesce(spell.PurchaserCode,spell.SpellCCGCode)	as 'Commissioner Code', 
Spell.AdmissiontWardCode							as 'Residing Ward Code',
DATEDIFF(day,Spell.AdmissionDate,@ReportDate)-1		as 'Current Length of Stay',
Spell.[AdmissionSpecialtyCode(Function)]			as 'Admission Specialty Code',
Spell.AdmissionMethodNHSCode						as 'Admission Type',				
isnull(SQ.BedDays,0)								as 'Number of Undischarged CC* Days'
--,Spell.DischargeDate

	--AdmissionType = 
	--	Case	when Spell.InpatientStayCode = 'D' then 'Day Case' 
	--			when Spell.AdmissionType in ('Emergency','Non-Elective','Maternity') then 'Non Elective'
	--			else 'Elective' end,
	--,
	--Spell.NHSNo,
	--Spell.SourceSpellNo,
	--Spell.FaciltyID,
	--Patient.DateOfBirth,
	--Spell.AdmissiontWardCode,
	--Spell.GPCode,
	--Spell.PracticeCode,
	--Spell.AdmissionDate,
	--Spell.DischargeDate,
	--Spell.[AdmissionSpecialtyCode(Function)],
	--Spell.LengthOfSpell,
	--DATEDIFF(day,Spell.AdmissionDate,@ReportDate) as 'LoSAtMonthEnd',
	--case 
	--when CysticFibrosisRegister.DistrictNo is null 
	--then null 
	--else 'CF Patient' 
	--end as CFPatient
	
from WHReporting.APC.Spell 

	left join ---To remove patients who have been on Crit Care for 10 days or more from LonG LoS element)
	( 
	--Declare @ReportDate as Date = '01 may 2015'	
		Select 
					WS.SourceSpellNo
					,BedDays =
					SUM(
				DATEDIFF(day,WS.StartTime ,
				(CASE
				when WS.EndTime is null 
				then cast((dateadd(day,-1*((DATEPART(day,@ReportDate))),@ReportDate)) as DATE)
				when cast(WS.EndTime as DATE) <= cast((dateadd(day,-1*((DATEPART(day,@ReportDate))),@ReportDate)) as DATE)
				then WS.EndTime
				else cast((dateadd(day,-1*((DATEPART(day,@ReportDate))),@ReportDate)) as DATE)
				end)
				)
				)+1
					FROM WHREPORTING.APC.WardStay WS
					
					left join WHReporting.APC.Spell S
					on S.SourceSpellNo = WS.SourceSpellNo
					
					where 
				coalesce(DischargeDate,@ReportDate) > cast((dateadd(day,-1*(DATEPART(day,@ReportDate)),@ReportDate)) as DATE)
				and 
				admissionDatetime < cast((dateadd(day,-1*((DATEPART(day,@ReportDate))-1),@ReportDate)) as DATE)
				and
				WS.StartTime < cast((dateadd(day,-1*((DATEPART(day,@ReportDate))-1),@ReportDate)) as DATE)
				and 
				WS.WardCode in ('ICA','CTCU','CT Transplant')
				--and
				--S.NHSNo = '6201622675'
				GROUP BY
				WS.SourceSpellNo
	) SQ on SQ.SourceSpellNo = Spell.SourceSpellNo
	
	left join --get's current Ward--
			( 
	--Declare @ReportDate as Date = '01 may 2015'	
		Select 
					WS.SourceSpellNo
					,WS.WardCode
					FROM WHREPORTING.APC.WardStay WS
					
					--left join WHReporting.APC.Spell S
					--on S.SourceSpellNo = WS.SourceSpellNo
					
					where 
				coalesce(WS.EndTime,@ReportDate) > @ReportDate
				and
				WS.StartTime < @ReportDate
	
				GROUP BY
					WS.SourceSpellNo
					,WS.WardCode
				) CW
			 on CW.SourceSpellNo = Spell.SourceSpellNo
	

	--left join WHREPORTING.APC.Patient 
	--on Spell.EncounterRecno = Patient.EncounterRecno
	
	--left join PbR2013.dbo.CysticFibrosisRegister 
	--on Spell.FaciltyID = 'RM2'+cast(CysticFibrosisRegister.DistrictNo as nvarchar(20)) 
	----left join PbR2013.dbo.CysticFibrosisRegister on RIGHT(len(Spell.FacilityID)-3,Spell.FaciltyID)=CysticFibrosisRegister.DistrictNo
	----inner join WHReporting.APC.Episode on Spell.SourceSpellNo = Episode.SourceSpellNo
	----inner join WH.APC.Encounter on Episode.EncounterRecno = Encounter.EncounterRecno

where 
Spell.AdmissionDate < @ReportDate
and
(Spell.DischargeDate is null or spell.DischargeDateTime > @ReportDate)
and 
DATEDIFF(day,Spell.AdmissionDate,@ReportDate) > 27
and 
PurchaserCode not in ('TDH00','YDD82')
and
Spell.[AdmissionSpecialtyCode(Function)] not in ('321','400','161')
and
(
SQ.BedDays < 10
or
SQ.BedDays is null
)

--order by 
--DATEDIFF(day,Spell.AdmissionDate,@ReportDate) asc,
--SpellCCGCode


UNION ALL
----------------------------CRITICAL CARE---------------------------------




select
NHSNo												as 'NHS Number',
spell.PracticeCode									as 'GP Practice Code',
coalesce(spell.PurchaserCode,spell.SpellCCGCode)	as 'Commissioner Code', 
CW.WardCode											as 'Residing Ward Code',
--Spell.SourceSpellNo,
--FaciltyID,
--AdmissionMethod,
--AdmissionDate,
--DischargeDate,
DATEDIFF(day,Spell.AdmissionDate,@ReportDate)-1		as 'Current Length of Stay' ,--LoS at MonthEnd
--=DATEDIFF(day,admissiondatetime,cast((dateadd(day,-1*((DATEPART(day,@ReportDate))),@ReportDate)) as DATE)),
--LOSAsAtToday=sum(ccpstay),
													
Spell.[AdmissionSpecialtyCode(Function)]			as 'Admission Specialty Code',
Spell.AdmissionMethodNHSCode						as 'Admission Type'	,
--,cast(ccperiod.CriticalCareStartDatetime as date)
--,max(cast(ccperiod.CriticalCareEndDatetime as date))
----,stay.SourceCCPNo
'Number of Undischarged CC* Days' =-- as at Month End
SUM(
DATEDIFF(day,cast(ccperiod.CriticalCareStartDatetime as date),
(CASE
when CriticalCareEndDatetime is null 
then cast((dateadd(day,-1*((DATEPART(day,@ReportDate))),@ReportDate)) as DATE)
when cast(CriticalCareEndDatetime as DATE) <= cast((dateadd(day,-1*((DATEPART(day,@ReportDate))),@ReportDate)) as DATE)
then CriticalCareEndDatetime
else cast((dateadd(day,-1*((DATEPART(day,@ReportDate))),@ReportDate)) as DATE)
end)
)
)+1

from

APC.Spell spell 

inner join APC.CriticalCarePeriod ccperiod 
on ccperiod.SourceSpellNo = spell.SourceSpellNo

inner join (select Distinct SourceCCPNo,SPONT_REFNO_CODE from Lorenzo.dbo.ExtractCriticalCarePeriodStay) stay 
on stay.SourceCCPNo=ccperiod.SourceCCPNo

--LEFT JOIN WHREPORTING.LK.Calendar cdis 
--on cdis.TheDate = DischargeDate

left join --get's current Ward--
			( 
	--Declare @ReportDate as Date = '01 may 2015'	
		Select 
					WS.SourceSpellNo
					,WS.WardCode
					FROM WHREPORTING.APC.WardStay WS
					
					--left join WHReporting.APC.Spell S
					--on S.SourceSpellNo = WS.SourceSpellNo
					
					where 
				coalesce(WS.EndTime,@ReportDate) > @ReportDate
				and
				WS.StartTime < @ReportDate
	
				GROUP BY
					WS.SourceSpellNo
					,WS.WardCode
				) CW
			 on CW.SourceSpellNo = Spell.SourceSpellNo



where 
coalesce(DischargeDate,@ReportDate) > cast((dateadd(day,-1*(DATEPART(day,@ReportDate)),@ReportDate)) as DATE)
and 
admissionDatetime < cast((dateadd(day,-1*((DATEPART(day,@ReportDate))-1),@ReportDate)) as DATE)
and
ccperiod.CriticalCareStartDatetime < cast((dateadd(day,-1*((DATEPART(day,@ReportDate))-1),@ReportDate)) as DATE)
and 
SPONT_REFNO_CODE in ('ICA','CTCU','CT Transplant')
--and 
--spell.SourceSpellNo  in 
--('150603208',
--'150609784',
--'150609875',
--'150610713',
--'150610922',
--'150612071',
--'150612447'
--)

group by

NHSNo ,
spell.PracticeCode,
coalesce(spell.PurchaserCode,spell.SpellCCGCode),
CW.WardCode,
DATEDIFF(day,Spell.AdmissionDate,@ReportDate)-1	
--DATEDIFF(day,admissiondatetime,cast((dateadd(day,-1*((DATEPART(day,@ReportDate))),@ReportDate)) as DATE))
--,ccperiod.CriticalCareStartDatetime
--,ccperiod.CriticalCareEndDatetime
--,Spell.SourceSpellNo
--FaciltyID,
--AdmissionMethod,
--AdmissionDate,
--,DischargeDate
,
Spell.[AdmissionSpecialtyCode(Function)],			
Spell.AdmissionMethodNHSCode						

Having
SUM(
DATEDIFF(day,cast(ccperiod.CriticalCareStartDatetime as date),
(CASE
when CriticalCareEndDatetime is null 
then cast((dateadd(day,-1*((DATEPART(day,@ReportDate))),@ReportDate)) as DATE)
when cast(CriticalCareEndDatetime as DATE) <= cast((dateadd(day,-1*((DATEPART(day,@ReportDate))),@ReportDate)) as DATE)
then CriticalCareEndDatetime
else cast((dateadd(day,-1*((DATEPART(day,@ReportDate))),@ReportDate)) as DATE)
end)
)
)+1
 > 9
 
 ) U
 
 Order by 
 
 U.[NHS Number]