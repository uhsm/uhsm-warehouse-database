﻿CREATE procedure [SC].[AdmissionMonthBySpecialty] 
	@spec varchar(5),
	@month int,
	@year int
as
/*

----------------------------------------------------------------------------------------------------------------
Action		Date		Author		Comments
----------------------------------------------------------------------------------------------------------------

Created		08/09/2014	KO			To replace Qry_QACancOps_CT in New Extracts access db

----------------------------------------------------------------------------------------------------------------
To use: To return count of admissions in september 2012 under specialty 17[x]
exec WHREPORTING.SC.AdmissionMonthBySpecialty '17', 9, 2012
*/

--declare @spec varchar(5)
--declare @month int
--declare @year int

--select @spec = '17'
--select @month = 9
--select @year = 2012

--select * from (
	select 
		AdmissionYear = datepart(year, AdmissionDate)
		,AdmissionMonth = datepart(month, AdmissionDate)
		,AdmissionSpecialtyCode
		,Cnt = COUNT(1)
	from APC.Spell
	where PatientClass In ('DAY CASE','ELECTIVE INPATIENT')
	and AdmissionDate >= '2011-04-01'
	and AdmissionSpecialtyCode like isnull(@spec, AdmissionSpecialtyCode) + '%'
	and datepart(month, AdmissionDate) = @month
	and datepart(year, AdmissionDate) = @year
	group by datepart(year, AdmissionDate), datepart(month, AdmissionDate), AdmissionSpecialtyCode
		
--)grp
--PIVOT (SUM(cnt) FOR AdmissionYear IN ([2011],[2012],[2013],[2014])) AS pvt