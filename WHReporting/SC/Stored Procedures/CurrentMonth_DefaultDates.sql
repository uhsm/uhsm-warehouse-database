﻿/*
--Author: N Scattergood
--Date created: 16/05/2013
--Stored Procedure Built for pulling out details of last month
--Currently used for:
>
> 
*/

CREATE Procedure [SC].[CurrentMonth_DefaultDates]
as

 
 select 
 distinct
  
 min(Cal.TheDate) as FirstDayOfCurrentMonth
 ,max(Cal.TheDate) LastDayofCurrentMonth
 ,dbo.GetEndOfDay (max(Cal.TheDate)) LastDateTimeofCurrentMonth
 ,Cal.TheMonth as CurrentMonth
 ,Cal.FinancialYear as FYCurrentMonth

 FROM LK.Calendar Cal
 
 where
Cal.TheMonth = 
(select distinct
SQ.TheMonth
  FROM LK.Calendar SQ
  where SQ.TheDate = 
 cast(GETDATE()as DATE)
 )


 group by
Cal.TheMonth
,Cal.FinancialYear