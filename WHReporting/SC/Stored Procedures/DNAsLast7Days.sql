﻿/*
--Author: N Scattergood
--Date created: 21/11/2012
--Stored Procedure Built for SRSS Report on Patients who have had DNA'd in the last seven days
*/

--[SC].[DNAsLast7Days] ('Proc')

CREATE Procedure [SC].[DNAsLast7Days]

@DIVPar as nvarchar(max)
,@DirPar as nvarchar(max)
,@SpecPar as nvarchar(max)
,@ClinicPar as nvarchar(max)

as

Select

[FacilityID]
      ,[NHSNo]
      ,[AgeAtAppointment]
      ,[ClinicCode]
      ,[Clinic]
      ,[AppointmentDate]
      ,[AppointmentTime]
      ,[AppointmentDateTime]
      --,[AppointmentTypeCode]
      ,[AppointmentType]
      ,[AppointmentTypeNHSCode]
      ,[ProfessionalCarerType]
      ,[ProfessionalCarerCode]
      ,[ProfessionalCarerName]
      ,[Division]
      ,[Directorate]
      --,[SpecialtyCode]
      --,[Specialty]
      ,[SpecialtyCode(Function)]
      ,[Specialty(Function)]
      --,[SpecialtyCode(Main)]
      --,[Specialty(Main)]
      ,[AttendStatusCode]
      ,[AttendStatus]
      ,[AttendStatusNHSCode]
      ,[Status]
      ,[OutcomeCode]
      ,[Outcome]
      ,[OutcomeNHSCode]
      --,[AppointmentGPCode]
      ,[AppointmentGP]
      --,[AppointmentPracticeCode]
      ,[AppointmentPractice]
      ,[AppointmentPCTCode]
      ,[AppointmentPCT]
      --,[PostCode]
      --,[ResidencePCTCode]
      --,[ResidencePCT]
      --,[PurchaserCode]
      --,[Purchaser]

      ,[ExcludedClinic]
      ,[IsWalkIn]
      , 1 as [Count]

FROM [WHREPORTING].[OP].[Schedule] OP

		LEFT OUTER JOIN dbo.vwExcludeWAWardsandClinics Exc
		ON OP.ClinicCode = Exc.SPONT_REFNO_CODE

WHERE


AppointmentDateTime BETWEEN 
--'01 APR 2012' AND '31 OCT 2012'
DATEADD(DD,-7,(CAST(GETDATE() AS DATE))) AND CAST(GETDATE() AS DATE)
AND
IsWardAttender = 0
AND
OP.ExcludedClinic = '0'
AND  
Exc.SPONT_REFNO_CODE IS NULL
AND
(
AttendStatus IN 
	(
	'Refused to Wait',
	'Patient Left / Not seen',
	'Patient Late / Not Seen',
	'Did Not Attend'
	) 
	OR 
	(
	AttendStatus IN 
	(
	'Attended',
	'Attended on Time',
	'Patient Late / Seen'
	)
	AND Outcome = 'Did Not Attend'
	)
	)
	
	
AND 
(OP.AppointmentTypeNHSCode IN (1, 2) OR OP.AppointmentTypeNHSCode IS NULL)
AND 
AdministrativeCategory <> 'Private Patient'
AND
[SpecialtyCode(Function)]  NOT IN ('Unk','DQ')

and
OP.Division in (SELECT Val from dbo.fn_String_To_Table(@DIVPar,',',1))
and
OP.Directorate in (SELECT Val from dbo.fn_String_To_Table(@DirPar,',',1))
and
OP.[SpecialtyCode(Function)] in (SELECT Val from dbo.fn_String_To_Table(@SpecPar,',',1))
and
OP.ClinicCode in (SELECT Val from dbo.fn_String_To_Table(@ClinicPar,',',1))



	
	ORDER BY AppointmentDateTime DESC