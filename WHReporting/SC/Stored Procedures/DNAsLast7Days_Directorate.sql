﻿/*
--Author: N Scattergood
--Date created: 21/11/2012
--Stored Procedure Built for SRSS Report on Patients who have had DNA'd in the last seven days
--This Provides Parameters based on a view identical to the Stored Procedure
*/

CREATE Procedure [SC].[DNAsLast7Days_Directorate]

@DIVPar as nvarchar(max)
as

Select

Distinct


      --[ClinicCode]
      --[Clinic]
      --[ProfessionalCarerType]
      --[ProfessionalCarerCode]
      --[ProfessionalCarerName]
      --[Division] as DivisionParameter
      [Directorate] as DirectorateParameter
      --[SpecialtyCode(Function)]
      --[Specialty(Function)]
     
  FROM [WHREPORTING].[SC].[vwDNAsLast7Days] OP



WHERE

OP.Division in (SELECT Val from dbo.fn_String_To_Table(@DIVPar,',',1))

	
	--ORDER BY AppointmentDateTime DESC