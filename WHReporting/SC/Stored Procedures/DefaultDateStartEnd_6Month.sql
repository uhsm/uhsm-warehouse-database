﻿/*
--Author: N Scattergood
--Date created: 13th November 2012
--Stored Procedure Built for the SRSS Reports
--
--This is used to pullout the Default Start and End Dates for the last 6 months
*/

CREATE Procedure [SC].[DefaultDateStartEnd_6Month]
as


select
cast(
'1 '
+
DATENAME(MM,
(
DATEADD(MM,-6,getdate())
)
)
+ ' ' +
DATENAME(YY,
(
DATEADD(MM,-6,getdate())
)
) as Datetime) as DefaultStart
,
(DATEADD(DD,-1,
(cast(
'1 '
+
DATENAME(MM,
getdate()
)
+ ' ' +
DATENAME(YY,
getdate()
)
as Datetime)))) as DefaultEnd
,dbo.GetEndOfDay
(DATEADD(DD,-1,
(cast(
'1 '
+
DATENAME(MM,
getdate()
)
+ ' ' +
DATENAME(YY,
getdate()
)
as Datetime)))) as DefaultEndDateTime