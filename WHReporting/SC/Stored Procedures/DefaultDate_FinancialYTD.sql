﻿/*
--Author: N Scattergood
--Date created: 05/06/2014
--Stored Procedure Built for pulling out details of last month
*/

Create Procedure [SC].[DefaultDate_FinancialYTD]
as

 
 select 
 distinct
  
 min(Cal.TheDate) as FirstDayOfFinYear
 ,max(Cal.TheDate) LastDayofFinYear
 ,dbo.GetEndOfDay (max(Cal.TheDate)) LastDateTimeofFinYear
 ,Cal.FinancialYear

 FROM LK.Calendar Cal
 
 where
Cal.FinancialYear = 
(select distinct
SQ.FinancialYear
  FROM LK.Calendar SQ
  where SQ.TheDate = 
 (
dateadd(Month,-1,cast(GETDATE()as DATE))
)
)
And 
Cal.TheDate <=
(Select
max(Cal.TheDate)
FROM LK.Calendar Cal
 
 where
Cal.TheMonth = 
(select distinct
SQ.TheMonth
  FROM LK.Calendar SQ
  where SQ.TheDate = 
 (
dateadd(MM,-1,cast(GETDATE()as DATE))
)
))



 group by
Cal.FinancialYear