﻿/*
--Author: N Scattergood
--Date created: 9/07/2013
--Stored Procedure Built for pulling out details of next 7 days
--Currently used for:
> Cancer Additions to WL

*/

Create Procedure [SC].[Last7Days_DefaultDates]
as

 
 select 
dateadd(day,-7,cast(Getdate() as Date)) as FirstDayOfLast7Days
,dateadd(day,-1,cast(Getdate() as Date)) as LastDayOfLast7Days
,dbo.GetEndOfDay(dateadd(day,-1,cast(Getdate() as Date))) as LastDateTimeLast7Days