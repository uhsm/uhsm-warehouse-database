﻿/*
--Author: N Scattergood
--Date created: 27/11/2012
--Stored Procedure Built for pulling out details of last month
--Currently used for:
> SRSS Report on Uncoded Deaths 
> SRSS Report on Operations on ORMIS without a Unique Session ID 
*/

CREATE Procedure [SC].[LastMonth_DefaultDates]
as

 
 select 
 distinct
  
 min(Cal.TheDate) as FirstDayOfLastMonth
 ,max(Cal.TheDate) LastDayofLastMonth
 ,dbo.GetEndOfDay (max(Cal.TheDate)) LastDateTimeofLastMonth
 ,Cal.TheMonth as LastMonth
 ,Cal.FinancialYear as FYLastMonth

 FROM LK.Calendar Cal
 
 where
Cal.TheMonth = 
(select distinct
SQ.TheMonth
  FROM LK.Calendar SQ
  where SQ.TheDate = 
 (
dateadd(MM,-1,cast(GETDATE()as DATE))
)
)

 group by
Cal.TheMonth
,Cal.FinancialYear