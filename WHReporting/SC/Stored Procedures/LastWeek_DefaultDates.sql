﻿/*
--Author: N Scattergood
--Date created: 27/11/2012
--Stored Procedure Built for pulling out details of last week
--Currently used for:
> SRSS Report on Top Outpatient Clinic DNAs
> SRSS Report on ORMIS Cancellation Report
*/

CREATE Procedure [SC].[LastWeek_DefaultDates]
as

 
 select 
 distinct
  
 min(Cal.TheDate) as FirstDayOfLastWeek
 ,max(Cal.TheDate) LastDayofLastWeek
 ,dbo.GetEndOfDay (max(Cal.TheDate)) LastDateTimeofLastWeek
 ,Cal.FirstDayOfWeek as WkCommencing
 ,Cal.LastDayOfWeek as WkEnding

 FROM LK.Calendar Cal
 
 where
Cal.FirstDayOfWeek = 
(select distinct
SQ.FirstDayOfWeek
  FROM LK.Calendar SQ
  where SQ.TheDate = 
 (
dateadd(day,-7,cast(GETDATE()as DATE))
)
)

 group by
Cal.FirstDayOfWeek 
 ,Cal.LastDayOfWeek