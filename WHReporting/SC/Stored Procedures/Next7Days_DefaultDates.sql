﻿/*
--Author: N Scattergood
--Date created: 19/06/2013
--Stored Procedure Built for pulling out details of next 7 days
--Currently used for:
> Cancer - Surgery TCIDate

*/

Create Procedure [SC].[Next7Days_DefaultDates]
as

 
 select 
cast(getdate()as DATE) as StartDateToday
,dateadd(day,6,cast(Getdate() as Date)) as LastDateof7Days
,dbo.GetEndOfDay(dateadd(day,6,cast(Getdate() as Date))) as LastDateTimeOf7Days