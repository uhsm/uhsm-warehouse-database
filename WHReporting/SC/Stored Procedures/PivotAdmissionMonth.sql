﻿CREATE procedure [SC].[PivotAdmissionMonth] as
/*

----------------------------------------------------------------------------------------------------------------
Action		Date		Author		Comments
----------------------------------------------------------------------------------------------------------------

Created		08/09/2014	KO			To replace Qry_QACancOps in New Extracts access db
----------------------------------------------------------------------------------------------------------------
*/


--http://stackoverflow.com/questions/7822004/pivots-with-dynamic-columns-in-sql-server

declare @cols varchar(100)   --dynamically define column headers
declare @sql NVARCHAR(4000)   

select @cols = isnull(@cols + ',', '') + '[' + AdmissionYear + ']'
from 
	(
	select distinct AdmissionYear = cast(datepart(year, AdmissionDate) as varchar) 
	from APC.Spell
	where AdmissionDate >= '2011-04-01'
	)yr
order by AdmissionYear

--print @cols

 

SET @sql = 
	'select * from (
		select 
			AdmissionYear = datepart(year, AdmissionDate)
			,AdmissionMonth = datepart(month, AdmissionDate)
			,Cnt = COUNT(1)
		from APC.Spell
		where PatientClass In (''DAY CASE'',''ELECTIVE INPATIENT'')
		and AdmissionDate >= ''2011-04-01''
		group by datepart(year, AdmissionDate), datepart(month, AdmissionDate)
	)grp
	PIVOT (SUM(cnt) FOR AdmissionYear IN ('
	+ @cols +
	')) AS pvt'

-----------------------------------------------------------------------------
--end up with 
--select * from (
--		select 
--			AdmissionYear = datepart(year, AdmissionDate)
--			,AdmissionMonth = datepart(month, AdmissionDate)
--			,Cnt = COUNT(1)
--		from APC.Spell
--		where PatientClass In ('DAY CASE','ELECTIVE INPATIENT')
--		and AdmissionDate >= '2011-04-01'
--		group by datepart(year, AdmissionDate), datepart(month, AdmissionDate)
--	)grp
--	PIVOT (SUM(cnt) FOR AdmissionYear IN ([2011],[2012],[2013],[2014])) AS pvt
------------------------------------------------------------------------------

--print @sql
execute(@sql)