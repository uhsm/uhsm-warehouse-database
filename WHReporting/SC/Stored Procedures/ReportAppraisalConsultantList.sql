﻿/*
--Author: N Scattergood
--Date created: 31/01/2013
--Stored Procedure Built for SRSS Report on:
--Consultant Appraisals 
--This provides the ConsultantParameter
--Modified: J Potluri on 21/02/2015
--removed [v-uhsm-dw01]
*/


CREATE Procedure [SC].[ReportAppraisalConsultantList]

@StartDate as Date
,@EndDate as Date

as

Select Distinct * from
(
Select
Distinct
OP.ProfessionalCarerCode as ConsCodePar,
OP.ProfessionalCarerName as ConsPar

FROM [WHREPORTING].[OP].[Schedule] OP
where
AppointmentDate between @StartDate and @EndDate
and
LEFT(OP.ProfessionalCarerCode,1) = 'c'
and
ISNUMERIC (RIGHT(OP.ProfessionalCarerCode,7)) = 1	


union all

Select
Distinct
S.AdmissionConsultantCode
,S.AdmissionConsultantName

FROM [WHREPORTING].[APC].[Spell] S
where
S.DischargeDate between @StartDate and @EndDate
and
LEFT(S.AdmissionConsultantCode,1) = 'c'
and
ISNUMERIC (RIGHT(S.AdmissionConsultantCode,7)) = 1	

) C

order by
C.ConsPar