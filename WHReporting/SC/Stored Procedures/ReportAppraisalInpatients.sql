﻿/*
--Author: N Scattergood
--Date created: 31/01/2013
--Stored Procedure Built for SRSS Report on:
--Consultant Appraisals 
--This provides data for Inpatient Data
--Modified: J Potluri on 21/02/2015
--removed [v-uhsm-dw01]
*/


CREATE Procedure [SC].[ReportAppraisalInpatients]

@StartDate as Date
,@EndDate as Date
,@ConsPar as nvarchar(100)
,@AdmMethodPar as nvarchar(100)

as
Select

datename(mm,S.[DischargeDateTime] ) + '-' + datename(yy,S.[DischargeDateTime] ) as DischargeMonth
,S.[AdmissionSpecialtyCode]
,S.[AdmissionSpecialty]

,S.AdmissionConsultantName
,case when S.[AdmissionType] in ('Emergency','Non-Elective') then 'Non-Elective'
		else S.[AdmissionType] end as 'AdmissionMethod' 
		
		     , case when S.[InpatientStayCode] = 'D'	then 'DayCase'
			when  S.[InpatientStayCode] = 'I'	then 'Inpatient'
			when  S.[InpatientStayCode] = 'B'	then 'Baby'
			else [InpatientStayCode] end as InpatientStay       

,COUNT(*) as 'NumberOfSpells'

 FROM [WHREPORTING].[APC].[Spell] S
 
   where

	S.[DischargeDateTime]  between  @StartDate and @EndDate 
	and
	S.AdmissionConsultantCode = @ConsPar
	and
	case when S.[AdmissionType] in ('Emergency','Non-Elective') then 'Non-Elective'
		else S.[AdmissionType] end in (SELECT Val from dbo.fn_String_To_Table(@AdmMethodPar,',',1))


	group by
	
	datename(mm,S.[DischargeDateTime] ) + '-' + datename(yy,S.[DischargeDateTime] ) 

,S.AdmissionConsultantName
,S.[AdmissionSpecialty]
,S.[AdmissionSpecialtyCode]
		,case when S.[AdmissionType] in ('Emergency','Non-Elective') then 'Non-Elective'
		else S.[AdmissionType] end 
		
		      ,case when S.[InpatientStayCode] = 'D'	then 'DayCase'
			when  S.[InpatientStayCode] = 'I'	then 'Inpatient'
			when  S.[InpatientStayCode] = 'B'	then 'Baby'
			else [InpatientStayCode] end