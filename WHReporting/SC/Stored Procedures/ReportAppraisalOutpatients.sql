﻿/*
--Author: N Scattergood
--Date created: 31/01/2013
--Stored Procedure Built for SRSS Report on:
--Consultant Appraisals 
--This provides data for Inpatient Data
*/


CREATE Procedure [SC].[ReportAppraisalOutpatients]

@StartDate as Date
,@EndDate as Date
,@ConsPar as nvarchar(100)
as

SELECT
--OP.ClinicCode,  
--OP.Clinic,
OP.ProfessionalCarerCode,
OP.ProfessionalCarerName,
--DATEPART(YY,OP.AppointmentDate) as [YearCode],
--DATEPART(mm,OP.AppointmentDate) as [MonthCode],
datename(mm,OP.AppointmentDate ) + ' ' + datename(yy,OP.AppointmentDate ) as [MonthYearDesc]

,CASE WHEN AppointmentTypeNHSCode = 1 Then 'New'
WHEN AppointmentTypeNHSCode = 2 THEN 'Follow Up'
WHEN AppointmentTypeNHSCode IS NULL THEN 'Follow Up'
END AS ApptType,


SUM(CASE WHEN AttendStatus IN ('Attended','Attended on Time','Patient Late / Seen')
	AND (outcome IS NULL 
	OR (Outcome NOT LIKE '%canc%' AND Outcome NOT LIKE '%Did Not Attend%')) 
THEN 1 ELSE 0 END) AS Attends


FROM [WHREPORTING].[OP].[Schedule] OP
LEFT OUTER JOIN [WHREPORTING].[dbo].[vwExcludeWAWardsandClinics] Exc
ON OP.ClinicCode = Exc.SPONT_REFNO_CODE

WHERE 

 Status IN ('ATTEND') 

AND IsWardAttender = 0
AND AppointmentDate between @StartDate and @EndDate

AND Exc.SPONT_REFNO_CODE IS NULL

AND (OP.AppointmentTypeNHSCode IN (1, 2) OR OP.AppointmentTypeNHSCode IS NULL)
AND AdministrativeCategory NOT LIKE '%Private%'
--AND OP.ProfessionalCarerCode = (SELECT Val from dbo.fn_String_To_Table(@ConsPar,',',1))
AND OP.ProfessionalCarerCode = @ConsPar

GROUP BY 
OP.ProfessionalCarerCode,
OP.ProfessionalCarerName,
--DATEPART(YY,OP.AppointmentDate),
--DATEPART(mm,OP.AppointmentDate),
datename(mm,OP.AppointmentDate ) + ' ' + datename(yy,OP.AppointmentDate )

,CASE WHEN AppointmentTypeNHSCode = 1 Then 'New'
WHEN AppointmentTypeNHSCode = 2 THEN 'Follow Up'
WHEN AppointmentTypeNHSCode IS NULL THEN 'Follow Up'
END 

--order by 
--DATEPART(YY,OP.AppointmentDate),
--DATEPART(mm,OP.AppointmentDate)