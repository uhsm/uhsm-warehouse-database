﻿/*
--Author: N Scattergood
--Date created: 18/07/2013
--Stored Procedure Built for SRSS Report on:
--Bed Modelling by Date and Year
*/


CREATE Procedure [SC].[ReportBedModellingByDateAndYear]
@StartDate		as Date
, @EndDate		as Date 
,@CompStart		as Date
, @CompEnd		as Date   
--,@SpecPar		as nvarchar(4000)
,@PatClassPar	as nvarchar(5)
,@HourOfDay		as nvarchar(50)

as
Select 

BMO.DateDDMMM
    ,BMO.CalendarYear

,COUNT(1) as TotalOccupancy

FROM
[WHREPORTING].[SC].[vwBedModellingOccupancy] BMO

	
	WHERE
	BMO.AdmissionLocation = 'WYTH'
	and
	(
	BMO.TheDate between @StartDate and @EndDate
	or
	BMO.TheDate between @CompStart and @CompEnd
	--DATEADD(year,-1,@StartDate) and DATEADD(year,-1,@EndDate)
	)
	and
	BMO.PatClassCode in (SELECT Val from dbo.fn_String_To_Table(@PatClassPar,',',1))
	and
	BMO.HOURINT  in (SELECT Val from dbo.fn_String_To_Table(@HourOfDay,',',1)) 

	
	
	GROUP by
BMO.DateDDMMM
    ,BMO.CalendarYear
	
	ORDER by
BMO.DateDDMMM
    ,BMO.CalendarYear