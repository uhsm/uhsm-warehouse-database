﻿/*
--Author: N Scattergood
--Date created: 05/07/2013
--Stored Procedure Built for SRSS Report on:
--Bed Modelling by Day of Week
*/


CREATE Procedure [SC].[ReportBedModellingByDayofWeek]
@StartDate		as Date
, @EndDate		as Date 
,@CompStart		as Date
, @CompEnd		as Date   
--,@SpecPar		as nvarchar(max)
,@PatClassPar	as nvarchar(5)
,@HourOfDay		as nvarchar(50)

as
Select 

Case 
      when BMO.TheDate between @StartDate and @EndDate
      then 'Main Period'
      when BMO.TheDate between @CompStart and @CompEnd
      then 'Comparison Period'
      end as [Period],
BMO.[DayofWeek],
BMO.[DayOfWeekKey],
D.Denominator,
COUNT(1) as TotalOccupancy
,((cast(COUNT(1)as decimal)) / Cast(D.Denominator as decimal)) as AverageOccupany

FROM
[WHREPORTING].[SC].[vwBedModellingOccupancy] BMO

	left join 
	(SELECT 
      [DayOfWeek]
      ,[DayOfWeekKey]
      ,Case 
      when TheDate between @StartDate and @EndDate
      then 'Main Period'
      when TheDate between @CompStart and @CompEnd
      then 'Comparison Period'
      end as [Period]
      ,COUNT(1) as [Denominator]
      FROM [WHREPORTING].[LK].[vwDateTime]
      where
     (
	TheDate between @StartDate and @EndDate
	or
	TheDate between @CompStart and @CompEnd
	--DATEADD(year,-1,@StartDate) and DATEADD(year,-1,@EndDate)
	)
      and 
      datepart(hour,HOURTIME) in (SELECT Val from dbo.fn_String_To_Table(@HourOfDay,',',1)) 
  Group by
  [DayOfWeek]
  ,[DayOfWeekKey]
  ,Case 
      when TheDate between @StartDate and @EndDate
      then 'Main Period'
      when TheDate between @CompStart and @CompEnd
      then 'Comparison Period'
      end 
 	) D
 	on Case 
      when TheDate between @StartDate and @EndDate
      then 'Main Period'
      when TheDate between @CompStart and @CompEnd
      then 'Comparison Period'
      end  = D.Period
	and BMO.[DayOfWeekKey] = D.[DayOfWeekKey]

	
	WHERE
	BMO.AdmissionLocation = 'WYTH'
	and
	(
	BMO.TheDate between @StartDate and @EndDate
	or
	BMO.TheDate between @CompStart and @CompEnd
	--DATEADD(year,-1,@StartDate) and DATEADD(year,-1,@EndDate)
	)
	and
	BMO.PatClassCode in (SELECT Val from dbo.fn_String_To_Table(@PatClassPar,',',1))
	and
	BMO.HOURINT  in (SELECT Val from dbo.fn_String_To_Table(@HourOfDay,',',1)) 

	
	
	GROUP by
	Case 
      when BMO.TheDate  between @StartDate and @EndDate
      then 'Main Period'
      when BMO.TheDate  between @CompStart and @CompEnd
      then 'Comparison Period'
      end,
	BMO.[DayofWeek],
	BMO.[DayOfWeekKey],
	D.Denominator
	
	ORDER by
	BMO.[DayOfWeekKey]