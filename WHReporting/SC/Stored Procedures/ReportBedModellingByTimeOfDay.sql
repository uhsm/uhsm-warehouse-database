﻿/*
--Author: N Scattergood
--Date created: 29/04/2013
--Stored Procedure Built for SRSS Report on:
--Over Night Bed Usage By Ward And Specialty
--Used to analyse Bed Outliers from differing perspectives
*/


CREATE Procedure [SC].[ReportBedModellingByTimeOfDay]

@ReportDate			as Datetime
,@AdmPar			as nvarchar(max)
,@LocationPar		as nvarchar(max)

as
SELECT
S.AdmissiontWardCode as AdmissionWardCode
,SPB.AdmissionLocation
      ,case when S.[InpatientStayCode] = 'D'	then 'DayCase'
			when  S.[InpatientStayCode] = 'I'	then 'Inpatient'
			when S.[InpatientStayCode] = 'B'	then 'Baby'
			else [InpatientStayCode] 
			end as InpatientStay
	,case 
	when S.[AdmissionType] in ('Emergency','Non-Elective') 
	then 'Non-Elective'
	else S.[AdmissionType] 
	end as 'AdmissionType' 
      ,E.[SourceSpellNo]
      ,E.[EpisodeUniqueID]
      ,E.[FacilityID]
      ,E.[NHSNo]
      ,E.[AdmissionDateTime]
      ,E.[DischargeDateTime]
      ,E.[EpisodeStartDateTime]
      ,E.[EpisodeEndDateTime]
      --,E.[ConsultantCode]
      --,E.[ConsultantName],
      --E.[Division]
      --,E.[Directorate]
      --,E.[SpecialtyCode(Function)]
      --,E.[Specialty(Function)]
      ,case
      when 
      E.EpisodeStartDateTime <= @ReportDate
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	@ReportDate
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '00:00'
		    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,1,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,1,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '01:00'
				    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,2,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,2,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '02:00'
					    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,3,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,3,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '03:00'
					    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,4,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,4,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '04:00'
					    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,5,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,5,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '05:00'
					    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,6,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,6,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '06:00'
					    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,7,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,7,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '07:00'
					    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,8,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,8,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '08:00'
					    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,9,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,9,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '09:00'
					    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,10,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,10,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '10:00'
					    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,11,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,11,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '11:00'
					    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,12,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,12,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '12:00'
					    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,13,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,13,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '13:00'
					    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,14,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,14,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '14:00'
					    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,15,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,15,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '15:00'
					    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,16,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,16,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '16:00'
					    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,17,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,17,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '17:00'
					    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,18,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,18,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '18:00'
					    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,19,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,19,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '19:00'
					    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,20,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,20,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '20:00'
					    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,21,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,21,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '21:00'
					    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,22,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,22,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '22:00'
					    ,case
      when 
      E.EpisodeStartDateTime <= dateadd(hour,23,@ReportDate)
      and (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	dateadd(hour,23,@ReportDate)
		  or 
		  (E.EpisodeEndDate	is null and E.DischargeDate is null)
		  ) 
		then 1
		else 0
		end as '23:00'
					 
      
      
      
        FROM [WHREPORTING].[APC].[Episode] E
        
				left join  [WHREPORTING].[APC].[Spell] S
				on S.SourceSpellNo = E.SourceSpellNo
        
			
						  left join	
				  (	
				  select distinct
				SPB.SPONT_REFNO_CODE
				,[SPECT_REFNO_MAIN_IDENT] as AdmissionLocation
				from WH.PAS.ServicePointBase SPB
				where
				SPB.[SPTYP_REFNO_MAIN_CODE] = 'WARD'
				  )	 SPB
				  on S.AdmissiontWardCode = SPB.SPONT_REFNO_CODE
        
		where 
		  E.EpisodeStartDateTime	
		  <= [WHREPORTING].dbo.GetEndOfDay(@ReportDate)
		  and
		  (
		  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime) >	@ReportDate
		  or 
		  (
		  E.EpisodeEndDate	is null
		  and
		  E.DischargeDate is null
		  )
		  )
		  
		  and case 
				when S.[AdmissionType] in ('Emergency','Non-Elective') 
				then 'Non-Elective'
				else S.[AdmissionType] 
				end in (SELECT Val from dbo.fn_String_To_Table(@AdmPar,',',1))
		  
		  and SPB.AdmissionLocation		in (SELECT Val from dbo.fn_String_To_Table(@LocationPar,',',1))

		
		  
		  order by 

		  E.EpisodeEndDate
		  ,E.DischargeDate
		  ,E.EpisodeStartDateTime