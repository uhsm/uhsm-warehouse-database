﻿/*
--Author: N Scattergood
--Date created: 05/07/2013
--Stored Procedure Built for SRSS Report on:
--Bed Modelling by Day of Week
--Used for Hour of Day Parameter
*/


Create Procedure [SC].[ReportBedModelling_PARHourofDay]
as
Select 
distinct
left(HOURTIME,5) as [HourOfDayDesc]
,datepart(hour,HOURTIME) as [HourOfDayPar]
 FROM [WHREPORTING].[LK].[vwDateTime]
 order by 
 datepart(hour,HOURTIME)