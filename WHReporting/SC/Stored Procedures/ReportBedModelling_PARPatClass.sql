﻿/*
--Author: N Scattergood
--Date created: 17/07/2013
--Stored Procedure Built for SRSS Reports on Bed Modelling :
--This Provides the dropdown parameter for PatClass
*/


CREATE Procedure [SC].[ReportBedModelling_PARPatClass]

as
Select 
Distinct

case
	when S.[AdmissionType] in ('Emergency','Non-Elective') 
	then 'Non-Elective'
	when S.[InpatientStayCode] = 'D'	
	then 'Elective - DayCase'
	when  S.[InpatientStayCode] = 'I'	
	then 'Elective - Inpatient'
	else 'Check SQL'
	end as 'PatClassDesc',
	case
	when S.[AdmissionType] in ('Emergency','Non-Elective') 
	then 1
	when S.[InpatientStayCode] = 'D'	
	then 2
	when  S.[InpatientStayCode] = 'I'	
	then 3
	else 9
	end as 'PatClassCode'
	
	FROM [WHREPORTING].[APC].[Spell] S
	
	WHERE
	S.[InpatientStayCode] <> 'B' 
	
	Order By
	PatClassDesc desc