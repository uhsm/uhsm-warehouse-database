﻿/*
--Author: N Scattergood
--Date created: 18/07/2013
--Stored Procedure Built for SRSS Reports on Bed Modelling :
--This Provides the dropdown parameter for Specilaty
*/


Create Procedure [SC].[ReportBedModelling_PARSpec]

as
Select 
Distinct

E.[SpecialtyCode(Function)] as SpecCode
,E.[Specialty(Function)] as SpecDesc


	
	FROM [WHREPORTING].[APC].[Episode] E
	
	left join [WHREPORTING].[APC].[Spell] S
	on S.SourceSpellNo = E.SourcePatientNo
	
	WHERE
	S.[InpatientStayCode] <> 'B' 
	and 
	E.EpisodeStartDate > dateadd(year,-2,GETDATE())
	and
	ISNUMERIC (E.[SpecialtyCode]) = 1	
	
	Order By
	E.[SpecialtyCode(Function)] asc