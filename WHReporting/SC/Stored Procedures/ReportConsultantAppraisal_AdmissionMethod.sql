﻿/*
--Author: N Scattergood
--Date created: 31/01/2013
--Stored Procedure Built for SRSS Report on:
--Consultant Appraisals 
--This provides the Admission Method Dropdown
--Modified: J Potluri on 21/02/2015
--removed [v-uhsm-dw01]
*/


CREATE Procedure [SC].[ReportConsultantAppraisal_AdmissionMethod]

@StartDate as Date
,@EndDate as Date
,@ConsPar as nvarchar(max)


as

Select
Distinct
		case when S.[AdmissionType] in ('Emergency','Non-Elective') then 'Non-Elective'
		else S.[AdmissionType] end 
		as 'AdmissionMethodPar'
		
		
		 FROM [WHREPORTING].[APC].[Spell] S
 
   where

	S.[DischargeDateTime]  between  @StartDate and @EndDate 
	and
	S.AdmissionConsultantCode = @ConsPar