﻿/*
--Author: N Scattergood
--Date created: 30/11/2012
--Stored Procedure Built for SRSS Report on:
--Patients who have had DNA'd a Pre-Op and Remained on a WL
*/


CREATE Procedure [SC].[ReportDNAdPreOPStillOnWL]

@SpecPar as nvarchar(max)
,@ConsPar as nvarchar(max)
,@UrgencyPar as nvarchar(max)
,@AdmMethod as nvarchar(max)

as

SELECT

1 as Data
      ,WL.[CensusDate]
      --,WL.[SourceUniqueID]
      --,WL.[SourcePatientNo]
      --,WL.[SourceEncounterNo]
      --,WL.[ReferralSourceUniqueID]
      --,WL.[NHSNumber]
      ,WL.[DistrictNo] as 'RM2'
      --,WL.[PatientTitle]
      ,WL.[PatientForename]
      ,WL.[PatientSurname]
      ,WL.DateOfBirth
      ,      DATEDIFF(yy, WL.DateOfBirth,GETDATE()) -
    Case when
    DATEADD(yy,Datediff(yy, WL.DateOfBirth,GETDATE()),WL.DateOfBirth)
      > GETDATE()
      then 1 else 0
      End as [AgeToday]
      --,WL.[DateOfBirth]
      --,WL.[DateOfDeath]
      ,CASE 
      WHEN WL.[SexCode] = '9269' THEN 'Female'
      WHEN WL.[SexCode] = '9270' THEN 'Male'
      Else Null End as 'Gender'
,C.ConsultantCode
,C.[Consultant]
      --,WL.SpecialtyCode as SpecialtyRef
      ,S.LocalSpecialtyCode
      ,S.Specialty
      --,WL.[PASSpecialtyCode]
      
      --,        CASE 
      --          WHEN MI.NationalManagementIntentionCode= '3' THEN 1 
      --          ELSE MI.NationalManagementIntentionCode
      --          End as 'IntendedManagementKey'
      ,CASE 
                WHEN MI.NationalManagementIntentionCode = '1' THEN 'IP' 
                WHEN MI.NationalManagementIntentionCode= '2' THEN 'DC' 
                WHEN MI.NationalManagementIntentionCode= '3' THEN 'IP' 
                WHEN MI.NationalManagementIntentionCode= '8' THEN 'Not Applicable' 
                WHEN MI.NationalManagementIntentionCode= '9' THEN 'Not Known' 
                ELSE MI.[ManagementIntention]
                End as 'IntendedManagement'
               
      --,MI.[ManagementIntention]
      ,		CASE 
                WHEN AM.AdmissionMethodLocalCode in ('11','12') THEN 11
                WHEN AM.AdmissionMethodLocalCode = '13' THEN 13
                ELSE AM.AdmissionMethodLocalCode
                END as 'WLTypeKey'  
      ,CASE 
                WHEN AM.AdmissionMethodLocalCode in ('11','12') THEN 'Elective'
                WHEN AM.AdmissionMethodLocalCode = '13' THEN 'Planned'
                ELSE AM.AdmissionMethod   
                END as 'WLType'         
      
      --,AM.[AdmissionMethod]
      ,P.PriorityCode
      ,P.[Priority]
      --,WL.[WaitingListCode]
      --,WL.[CommentClinical]
      --,WL.[CommentNonClinical]
      ,Coalesce(WL.[Operation],WL.[IntendedPrimaryOperationCode]) as IntendedProcedure
      ,WL.[IntendedPrimaryOperationCode]
      ,WL.[Operation]

      ,SP.[SPONT_REFNO_NAME] as AdmissionWard
      ,WL.[SiteCode]
      --,WL.[WardCode]
      ,WL.[WLStatus]
      ,WL.[DateOnWaitingList]
      ,WL.[TCIDate]


,OP.LatestOutpApptDate,
	OP.OutpClinicCode,  
	OP.OutpClinic,
	OP.OutpAttendStatus,
	OP.OutpStatus,
	OP.OutpSpec,
	OP.NumberOfDNAs

      

	----,OP.FacilityID,
	--  ,OP.AppointmentDate	as OutpApptDate,
	--OP.ClinicCode			as OutpClinicCode,  
	--OP.Clinic				as OutpClinic,

	----CASE WHEN AppointmentTypeNHSCode = 1 Then 'New'
	----WHEN AppointmentTypeNHSCode = 2 THEN 'Follow Up'
	----ELSE NULL
	----END						as OutpApptType,

	--OP.AttendStatus				as OutpAttendStatus,
	--OP.[Status]					as OutpStatus,
	--OP.[Specialty(Function)]	as OutpSpec
	--		--,WL.SpecialtyCode as SpecialtyRef
	--		--,S.LocalSpecialtyCode
	--		--,OP.[SpecialtyCode(Main)]
	--		--,OP.[SpecialtyCode(Function)]


  FROM [WH].[APC].[WL] WL
  
  Left Join [WH].[PAS].[Consultant] C
			on WL.[ConsultantCode] = C.ConsultantCode
			
		Left Join [WH].[PAS].[Specialty] S
				on WL.SpecialtyCode = S.SpecialtyCode
				
			Left Join [WH].[PAS].[AdmissionMethod] AM
					on WL.AdmissionMethodCode = AM.AdmissionMethodCode
				
				Left Join [WH].[PAS].[ManagementIntention] MI
						on WL.ManagementIntentionCode = MI.ManagementIntentionCode
						
						Left Join [WH].[PAS].[Priority] P
								on WL.PriorityCode = P.PriorityCode
								
								  left join [WH].[PAS].[ServicePointBase] SP
										on WL.SiteCode = SP.[SPONT_REFNO]
										and [SPTYP_REFNO_DESCRIPTION] = 'Ward'
	
	----START SUB QUERY OUTPATIENT JOIN----
	right join
(
select
SQ.FacilityID,
SQ.[SpecialtyCode(Function)],

	  max(SQ.AppointmentDate)	as LatestOutpApptDate,
	SQ.ClinicCode				as OutpClinicCode,  
	SQ.Clinic					as OutpClinic,
		SQ.AttendStatus			as OutpAttendStatus,
	SQ.[Status]					as OutpStatus,
	SQ.[Specialty(Function)]	as OutpSpec,
	COUNT(*) AS NumberOfDNAs


FROM
	[WHREPORTING].[OP].Schedule SQ
	where
					SQ.AppointmentDate >  dateadd(year,-1,getdate())
					and SQ.Clinic like '%pre%' 
					and	SQ.Clinic not like '%preg%' 
					and	SQ.Clinic not like '%prevent%'
					and	SQ.AttendStatus in
						(
				'Refused to Wait',
				'Patient Left / Not seen',
				'Patient Late / Not Seen',
				'Did Not Attend'
						) 
						
						group by
SQ.FacilityID,
SQ.[SpecialtyCode(Function)],
	SQ.ClinicCode			,  
	SQ.Clinic				,
		SQ.AttendStatus		,
	SQ.[Status]				,
	SQ.[Specialty(Function)]
)	OP
			
	----END OF SUB QUERY----		

		ON OP.FacilityID = WL.[DistrictNo]
	and LEFT(OP.[SpecialtyCode(Function)],3) = left(S.LocalSpecialtyCode,3)
	----END OF JOIN----				
					
  where 
  
  WL.CensusDate = (Select 
					MAX (WL.CensusDate)
						FROM [WH].[APC].[WL] WL) --Latest Census Date
									
	
	and WL.CancelledBy is null					--excludes Cancelled Patients
	and LEFT(WL.NHSNumber,3) <> '999'			--excludes Test Patient						
	
	
	----Parameters----
	
	and S.LocalSpecialtyCode in (SELECT Val from dbo.fn_String_To_Table(@SpecPar,',',1))
	and C.ConsultantCode	 in (SELECT Val from dbo.fn_String_To_Table(@ConsPar,',',1))
	and P.PriorityCode	     in (SELECT Val from dbo.fn_String_To_Table(@UrgencyPar,',',1))	
	and
	CASE 
                WHEN AM.AdmissionMethodLocalCode in ('11','12') THEN 11
                WHEN AM.AdmissionMethodLocalCode = '13' THEN 13
                ELSE AM.AdmissionMethodLocalCode
                END 
                in   (SELECT Val from dbo.fn_String_To_Table(@AdmMethod,',',1))	
                
	
	ORDER BY
S.Specialty	
	
,C.[Consultant]
  ,OP.LatestOutpApptDate asc