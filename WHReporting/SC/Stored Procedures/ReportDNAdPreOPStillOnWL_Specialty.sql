﻿/*
--Author: N Scattergood
--Date created: 30/11/2012
--Stored Procedure Built for SRSS Report on:
--Patients who have had DNA'd a Pre-Op and Remained on a WL
--This Provides Cascading Parameter Fields
*/


CREATE Procedure [SC].[ReportDNAdPreOPStillOnWL_Specialty]

as

SELECT

Distinct
S.LocalSpecialtyCode	as SpecCodePar,
S.Specialty				as SpecPar

--C.ConsultantCode,
--C.[Consultant]
      
--      CASE 
--                WHEN AM.AdmissionMethodLocalCode in ('11','12') THEN 11
--                WHEN AM.AdmissionMethodLocalCode = '13' THEN 13
--                ELSE AM.AdmissionMethodLocalCode
--                END as 'WLTypeKey'  ,
--      CASE 
--                WHEN AM.AdmissionMethodLocalCode in ('11','12') THEN 'Elective'
--                WHEN AM.AdmissionMethodLocalCode = '13' THEN 'Planned'
--                ELSE AM.AdmissionMethod   
--                END as 'WLType'         
      

--      P.PriorityCode,
--      P.[Priority]





  FROM [WH].[APC].[WL] WL
  
  Left Join [WH].[PAS].[Consultant] C
			on WL.[ConsultantCode] = C.ConsultantCode
			
		Left Join [WH].[PAS].[Specialty] S
				on WL.SpecialtyCode = S.SpecialtyCode
				
			Left Join [WH].[PAS].[AdmissionMethod] AM
					on WL.AdmissionMethodCode = AM.AdmissionMethodCode
				

						Left Join [WH].[PAS].[Priority] P
								on WL.PriorityCode = P.PriorityCode
								
						
	----START SUB QUERY OUTPATIENT JOIN----
	right join
(
select
SQ.FacilityID,
SQ.[SpecialtyCode(Function)],

	  max(SQ.AppointmentDate)	as LatestOutpApptDate,
	SQ.ClinicCode				as OutpClinicCode,  
	SQ.Clinic					as OutpClinic,
		SQ.AttendStatus			as OutpAttendStatus,
	SQ.[Status]					as OutpStatus,
	SQ.[Specialty(Function)]	as OutpSpec,
	COUNT(*) AS NumberOfDNAs


FROM
	[WHREPORTING].[OP].Schedule SQ
	where
					SQ.AppointmentDate >  dateadd(year,-1,getdate())
					and SQ.Clinic like '%pre%' 
					and	SQ.Clinic not like '%preg%' 
					and	SQ.AttendStatus in
						(
				'Refused to Wait',
				'Patient Left / Not seen',
				'Patient Late / Not Seen',
				'Did Not Attend'
						) 
						
						group by
SQ.FacilityID,
SQ.[SpecialtyCode(Function)],
	SQ.ClinicCode			,  
	SQ.Clinic				,
		SQ.AttendStatus		,
	SQ.[Status]				,
	SQ.[Specialty(Function)]
)	OP
			
	----END OF SUB QUERY----		

		ON OP.FacilityID = WL.[DistrictNo]
	and LEFT(OP.[SpecialtyCode(Function)],3) = left(S.LocalSpecialtyCode,3)
	----END OF JOIN----				
					
  where 
  
  WL.CensusDate = (Select 
					MAX (WL.CensusDate)
						FROM [WH].[APC].[WL] WL) --Latest Census Date
									
	
	and WL.CancelledBy is null					--excludes Cancelled Patients
	and LEFT(WL.NHSNumber,3) <> '999'			--excludes Test Patient						
	
	
	ORDER BY
	S.Specialty