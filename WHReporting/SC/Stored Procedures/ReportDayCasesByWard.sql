﻿/******************************************************************************
 Description: Provides a day case procedures carried out in a month by ward
 Parameters:  @StartDate and @EndDate
 
 Modification History --------------------------------------------------------

 Date     Author		Change
 19/08/14 Jubeda Begum  Created

******************************************************************************/
CREATE PROCEDURE [SC].[ReportDayCasesByWard] 
@STARTDATE AS DATE
,@ENDDATE AS DATE

AS
BEGIN


SET NOCOUNT ON;

SELECT 
[PatientClass]
,[AdmissionDate]
,[DischargeDate]
,[AdmissiontWardCode]
,[AdmissionWard]
,[SourcePatientNo]
,[AdmissionSpecialtyCode(Function)]
,[AdmissionSpecialty(Function)]
,[AdmissionDirectorate]
,[IntendedManagementCode]
,[IntendedManagement]
,1 AS DayCasesCOUNT

FROM [WHREPORTING].[APC].[Spell]
WHERE [PatientClass] = 'DAY CASE'
AND [AdmissionDate] BETWEEN @STARTDATE AND @ENDDATE

ORDER BY 
[AdmissionSpecialty(Function)]

END