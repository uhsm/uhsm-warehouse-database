﻿/*
--Author: N Scattergood
--Date created: 23/01/2013
--Stored Procedure Built for SRSS Report on:
--IP Elective Demand
--Contains both the IP and WL Section
*/


CREATE Procedure [SC].[ReportDemandElectiveIP]

@MonthEndPar as Date
,@CompYearPar as Integer

,@InpatientPar as nvarchar(100)

as

--Declare @StartDate as Datetime
--Declare @EndDate as Datetime

--Set		@StartDate	=	'01 apr 2011'			--Amend if Necessary to the Start of the Reporting Period
--Set		@EndDate	=	'31 Jan 2013 23:59:59'	--Amend if Necessary to the End of the Reporting Period

Select * from
(
(
SELECT  
'Activity' as Area
,case
	when S.DischargeDate <= DATEADD(year,@CompYearPar,@MonthEndPar)  
	then 'ACT1'
	when S.DischargeDate <=  @MonthEndPar
	then 'ACT2'
	else 'CHECK SQL'
	end as [Period]

,Cal.FinancialYear as 'FinYear'
 ,Cal.TheMonth as 'MonthYear'		
  --,datename(mm,S.[DischargeDateTime]) + '-' + datename(yy,s.[DischargeDateTime]) as DischargeMonth
,Null as [CensusDate]

      ,D.Division 
      ,D.Direcorate as Directorate
  
      

   --   ,case when S.admiss = '161' then '160' ---Burns into Plastics
			--when [AdmissionSpecialtyCode] = '211' then '171' ---Paed Uro into Paed Surg
			--else  cast(left(S.[AdmissionSpecialtyCode],3) AS Integer) 
			--END as AdmissionSpecialtyCode 
			--Left 3 rolls 103A into 103, 110E into 110, 120N into 120, 340G into 340, 350A into 350

      --,S.[AdmissionSpecialty]
      --,S.AdmissionConsultantName
      ,case
      when S.AdmissionConsultantCode in ('C2648831','C3246395')
      then 107
      when S.Purchaser = 'NSCAG'
      then 174
	when S.[AdmissionSpecialtyCode(Function)] in ('144','145')
	then 140
	when S.[AdmissionSpecialtyCode(Function)]  in ('102')
	then 174
	when S.[AdmissionSpecialtyCode(Function)]  in ('106')
	then 100
	else cast(left(S.[AdmissionSpecialtyCode(Function)],3) AS Integer) 
	end as SpecialtyCode
      --this partly corrects for Cardiothoracic Patients that are reported under Transplant for PbR 
      --it also addresses the 2 Vascular Surgeons who's activity is recorded under Radiology

		,D.SpecialtyFunction as SpecialtyDesc
		,S.[InpatientStayCode] as InpatientCode
 
      
      ,count(*) as Data
      
--Because of the agreement with the PCT around charging Day Case Cystoscopies as Outpatients the following has been deducted from the Urology line					
--SpellHRG	TRETSPEF	PatClass	Oct	Nov	Dec
--LB14E		101			DAY CASE	182	196	14
,case 
when S.[InpatientStayCode] =  'D'
and  S.[AdmissionSpecialtyCode(Function)] = '101'
and	 S.DischargeDate between '01 oct 2012' and '31 oct 2012'
then 182
when S.[InpatientStayCode] =  'D'
and  S.[AdmissionSpecialtyCode(Function)] = '101'
and	 S.DischargeDate between '01 Nov 2012' and '30 Nov 2012'
then 196
when S.[InpatientStayCode] =  'D'
and  S.[AdmissionSpecialtyCode(Function)] = '101'
and	 S.DischargeDate between '01 Dec 2012' and '31 Dec 2012'
then 14
else 0 end as ManualAmendments

   
      
      
  FROM [WHREPORTING].[APC].[Spell] S
  
  --  left join [WHREPORTING].[APC].[Episode]  E
		--on E.[SourceSpellNo] = S.[SourceSpellNo]
		--and E.LastEpisodeInSpell = '1'
		--and E.FirstEpisodeInSpell = '1'
		
		--left join [WHREPORTING].[APC].[ClinicalCoding] C
		--on  C.EncounterRecno = S.EncounterRecno
  
  		--		left join [WHREPORTING].[SC].[vwAPCCardiacConsultantSpec] CARDIAC
				--on S.AdmissionConsultantCode = CARDIAC.CardiacConsCode

					left join WH.WH.Calendar Cal 
					on S.DischargeDate = Cal.TheDate
					
					left join 
					(select 
						distinct
						SD.SpecialtyFunctionCode
						,case 
						when SD.SpecialtyFunctionCode = '430'
						then 'Care of the Elderly'
						--Geriatric Medicine be called ‘Care of the Elderly’ at request of Peter Nuttall 15/03/2013
						else SD.SpecialtyFunction
						end as SpecialtyFunction
						,SD.Division
						,SD.Direcorate 
						from
						[WHREPORTING].[LK].[SpecialtyDivision] SD

						where 
						isnumeric(SD.SpecialtyCode) = 1
						and 
						isnumeric(SD.SpecialtyFunctionCode) = 1
						and
						SD.SpecialtyFunction not in ('Trauma & Orthopaedic','Oncology')
						and
						SD.Direcorate not in ('Unknown','Not UHSM')
					) D					
					on case 
					when S.AdmissionConsultantCode in ('C2648831','C3246395')
					then 107
					when S.Purchaser = 'NSCAG'
					then 174
					when S.[AdmissionSpecialtyCode(Function)] in ('144','145')
					then 140
					when S.[AdmissionSpecialtyCode(Function)]  in ('102')
					then 174
					when S.[AdmissionSpecialtyCode(Function)]  in ('106')
					then 100
					else cast(left(S.[AdmissionSpecialtyCode(Function)],3) AS Integer) 
					End	 = D.SpecialtyFunctionCode

  
  where
  	S.[InpatientStayCode] <> 'B'
	and
	S.PurchaserCode <> 'VPP00'----------currently used because No Private Patient Field
	and
	S.AdmissionMethodCode in ('11','12','13')
	and
	S.[AdmissionSpecialtyCode(Function)] <> '400'
	and
	(
	S.DischargeDate between  
	-------Start Date------
	(
Select
Min(Cal.TheDate)

 FROM [WHREPORTING].[LK].[Calendar] Cal
  right join
 ( select
FY.FinancialYear
 FROM [WHREPORTING].[LK].[Calendar] FY
 where FY.TheDate = @MonthEndPar
 )SQ
 on SQ.FinancialYear = Cal.FinancialYear
)-------Start Date------
and
--------Last Day--------
@MonthEndPar
--------Last Day--------
	OR
	S.DischargeDate between  
-------Start Date for Comparison------
	(
Select
DATEADD(year,@CompYearPar,(Min(Cal.TheDate))) 

 FROM [WHREPORTING].[LK].[Calendar] Cal
  right join
 ( select
FY.FinancialYear
 FROM [WHREPORTING].[LK].[Calendar] FY
 where FY.TheDate = @MonthEndPar
 )SQ
 on SQ.FinancialYear = Cal.FinancialYear
)
-------Start Date for Comparison------
	and 
-------End Date for Comparison------

DATEADD(year,@CompYearPar,@MonthEndPar)  


-------End Date for Comparison------
	)
	
		----Parameters----

----@InpatientPar as Date

	and S.InpatientStayCode 
	in (SELECT Val from dbo.fn_String_To_Table(@InpatientPar,',',1))
	
	
		
	
group by 
case
	when S.DischargeDate <= DATEADD(year,@CompYearPar,@MonthEndPar)  
	then 'ACT1'
	when S.DischargeDate <=  @MonthEndPar
	then 'ACT2'
	else 'CHECK SQL'
	end,
Cal.FinancialYear 
 ,Cal.TheMonth 		
--,datename(mm,S.[DischargeDateTime]) + '-' + datename(yy,s.[DischargeDateTime])
--+ '-' + datename(yy,S.DischargeDateTime ) 
,D.Division
,D.Direcorate


      ,case
      when S.AdmissionConsultantCode in ('C2648831','C3246395')
      then 107
      when S.Purchaser = 'NSCAG'
      then 174
	when S.[AdmissionSpecialtyCode(Function)] in ('144','145')
	then 140
	when S.[AdmissionSpecialtyCode(Function)]  in ('102')
	then 174
	when S.[AdmissionSpecialtyCode(Function)]  in ('106')
	then 100
     else cast(left(S.[AdmissionSpecialtyCode(Function)],3) AS Integer) 
	 end
      ,D.SpecialtyFunction
		,S.[InpatientStayCode]
           ,S.[InpatientStayCode]

,case 
when S.[InpatientStayCode] =  'D'
and  S.[AdmissionSpecialtyCode(Function)] = '101'
and	 S.DischargeDate between '01 oct 2012' and '31 oct 2012'
then 182
when S.[InpatientStayCode] =  'D'
and  S.[AdmissionSpecialtyCode(Function)] = '101'
and	 S.DischargeDate between '01 Nov 2012' and '30 Nov 2012'
then 196
when S.[InpatientStayCode] =  'D'
and  S.[AdmissionSpecialtyCode(Function)] = '101'
and	 S.DischargeDate between '01 Dec 2012' and '31 Dec 2012'
then 14
else 0 end
  )      
  
  ---------End of IP Activity---------	
  	UNION ALL
  ---------Start of WL Selection Year---------	
  
	(
	select
 'WaitList' as 'Area'
 ,case
 when WL.CensusDate =
(
Select
    Max(WL.CensusDate)

  from  [WH].[APC].[WL] WL
 right join
  [WHREPORTING].[LK].[Calendar] Cal
  on Cal.TheDate = WL.CensusDate
  and WL.CensusDate <= @MonthEndPar
  right join 
 ( select
FY.FinancialYear
 FROM [WHREPORTING].[LK].[Calendar] FY
 where FY.TheDate = @MonthEndPar
 )SQ
 on SQ.FinancialYear = Cal.FinancialYear
)
then 'WL1'
 when WL.CensusDate =
	(
Select
    Max(WL.CensusDate)

  from  [WH].[APC].[WL] WL
 right join
  [WHREPORTING].[LK].[Calendar] Cal
  on Cal.TheDate = WL.CensusDate
  right join 
 ( select
FY.FinancialYear
 FROM [WHREPORTING].[LK].[Calendar] FY
 where FY.TheDate = DATEADD(year,@CompYearPar,@MonthEndPar) 
 )SQ
 on SQ.FinancialYear = Cal.FinancialYear
)
then 'WL2'
 	else 'CHECK SQL'
	end as [Period]
	
-----------------------
 
 
  ,FY.FinancialYear as 'FinYear'
 ,Cal.TheMonth as 'MonthYear'
,WL.CensusDate
,D.Division
,D.Direcorate as Directorate
,D.SpecialtyFunctionCode
, D.SpecialtyFunction

      ,case 
      when
      IM.NationalManagementIntentionCode in ('2','4')
      then 'D'
      else 'I'
      end as IntendedManagement

 
 
 ,Count(*) as TotalOnWL
 ,0 as ManualAmendments
 
  FROM [WH].[APC].[WL] WL
 
   		Left Join [WH].[PAS].[Specialty] S
		on WL.SpecialtyCode = S.SpecialtyCode
				
			Left Join [WH].[PAS].[ManagementIntention] IM
			on WL.ManagementIntentionCode = IM.ManagementIntentionCode
			
				Left Join [WHREPORTING].[LK].[Calendar] Cal
				on Cal.TheDate = WL.CensusDate
				
				Left Join [WHREPORTING].[LK].[Calendar] FY
				on FY.TheDate = @MonthEndPar
   
left join 
					(select 
						distinct
						SD.SpecialtyFunctionCode
						,case 
						when SD.SpecialtyFunctionCode = '430'
						then 'Care of the Elderly'
						--Geriatric Medicine be called ‘Care of the Elderly’ at request of Peter Nuttall 15/03/2013
						else SD.SpecialtyFunction
						end as SpecialtyFunction
						,SD.Division
						,SD.Direcorate 
						from
						[WHREPORTING].[LK].[SpecialtyDivision] SD

						where 
						isnumeric(SD.SpecialtyCode) = 1
						and 
						isnumeric(SD.SpecialtyFunctionCode) = 1
						and
						SD.SpecialtyFunction not in ('Trauma & Orthopaedic','Oncology')
						and
						SD.Direcorate not in ('Unknown','Not UHSM')
					) D	
					on case 
       when		S.LocalSpecialtyCode in ('144','145')
       then		140
       when		S.LocalSpecialtyCode in ('172')
       then		171
       when		S.LocalSpecialtyCode in ('219')
       then		171
       when		S.LocalSpecialtyCode in ('106')
       then		100
       else		cast(left(S.LocalSpecialtyCode,3) AS Integer) 
       End	 = D.SpecialtyFunctionCode
					
   where
   WL.CensusDate in
   (
   (
Select
    Max(WL.CensusDate)

  from  [WH].[APC].[WL] WL
 right join
  [WHREPORTING].[LK].[Calendar] Cal
  on Cal.TheDate = WL.CensusDate
  and WL.CensusDate <= @MonthEndPar
  right join 
 ( select
FY.FinancialYear
 FROM [WHREPORTING].[LK].[Calendar] FY
 where FY.TheDate = @MonthEndPar
 )SQ
 on SQ.FinancialYear = Cal.FinancialYear
)
UNION ALL

	(
Select
    Max(WL.CensusDate)

  from  [WH].[APC].[WL] WL
 right join
  [WHREPORTING].[LK].[Calendar] Cal
  on Cal.TheDate = WL.CensusDate
  right join 
 ( select
FY.FinancialYear
 FROM [WHREPORTING].[LK].[Calendar] FY
 where FY.TheDate = DATEADD(year,@CompYearPar,@MonthEndPar) 
 )SQ
 on SQ.FinancialYear = Cal.FinancialYear
)
)
and   WL.SpecialtyCode not in ('2000842','2000888')			--exclude Neurology, Physio
   --	 S.LocalSpecialtyCode   	 --not in ('650','400') --exclude Neurology, Physio
and case 
      when
      IM.NationalManagementIntentionCode in ('2','4')
      then 'D'
      else 'I'
      end in (SELECT Val from dbo.fn_String_To_Table(@InpatientPar,',',1))

   
   group by
 FY.FinancialYear 
 ,Cal.TheMonth 
,WL.CensusDate
,D.Division
,D.Direcorate 
,D.SpecialtyFunctionCode
,D.SpecialtyFunction

      ,case 
      when
      IM.NationalManagementIntentionCode in ('2','4')
      then 'D'
      else 'I'
      end 
      )
      ---------End of WL Selection Year---------
      UNION ALL
  ---------Start of Comparison Year---------	
  
	(
	select
 'WaitList' as 'Area'
 ,case
  when WL.CensusDate =
	(
Select
    Max(WL.CensusDate)

  from  [WH].[APC].[WL] WL
 right join
  [WHREPORTING].[LK].[Calendar] Cal
  on Cal.TheDate = WL.CensusDate
  and WL.CensusDate <=  DATEADD(year,@CompYearPar,@MonthEndPar) 
  right join 
 ( select
FY.FinancialYear
 FROM [WHREPORTING].[LK].[Calendar] FY
 where FY.TheDate =  DATEADD(year,@CompYearPar,@MonthEndPar) 
 )SQ
 on SQ.FinancialYear = Cal.FinancialYear
)
then 'WL3'
 when WL.CensusDate =
	(
Select
    Max(WL.CensusDate)

  from  [WH].[APC].[WL] WL
 right join
  [WHREPORTING].[LK].[Calendar] Cal
  on Cal.TheDate = WL.CensusDate
  right join 
 ( select
FY.FinancialYear
 FROM [WHREPORTING].[LK].[Calendar] FY
 where FY.TheDate = DATEADD(year,(@CompYearPar-1),@MonthEndPar) 
 )SQ
 on SQ.FinancialYear = Cal.FinancialYear
)
then 'WL4'
	else 'CHECK SQL'
	end as [Period]
	
-----------------------
 
 
  ,FY.FinancialYear as 'FinYear'
 ,Cal.TheMonth as 'MonthYear'
,WL.CensusDate
,D.Division
,D.Direcorate as Directorate
,D.SpecialtyFunctionCode
,D.SpecialtyFunction

      ,case 
      when
      IM.NationalManagementIntentionCode in ('2','4')
      then 'D'
      else 'I'
      end as IntendedManagement

 
 
 ,Count(*) as TotalOnWL
 ,0 as ManualAmendments
 
  FROM [WH].[APC].[WL] WL
 
   		Left Join [WH].[PAS].[Specialty] S
		on WL.SpecialtyCode = S.SpecialtyCode
				
			Left Join [WH].[PAS].[ManagementIntention] IM
			on WL.ManagementIntentionCode = IM.ManagementIntentionCode
			
				Left Join [WHREPORTING].[LK].[Calendar] Cal
				on Cal.TheDate = WL.CensusDate
				
				Left Join [WHREPORTING].[LK].[Calendar] FY
				on FY.TheDate =  DATEADD(year,@CompYearPar,@MonthEndPar) 
   
left join 
					(select 
						distinct
						SD.SpecialtyFunctionCode
						,case 
						when SD.SpecialtyFunctionCode = '430'
						then 'Care of the Elderly'
						--Geriatric Medicine be called ‘Care of the Elderly’ at request of Peter Nuttall 15/03/2013
						else SD.SpecialtyFunction
						end as SpecialtyFunction
						,SD.Division
						,SD.Direcorate 
						from
						[WHREPORTING].[LK].[SpecialtyDivision] SD

						where 
						isnumeric(SD.SpecialtyCode) = 1
						and 
						isnumeric(SD.SpecialtyFunctionCode) = 1
						and
						SD.SpecialtyFunction not in ('Trauma & Orthopaedic','Oncology')
						and
						SD.Direcorate not in ('Unknown','Not UHSM')
					) D	
					on case 
       when		S.LocalSpecialtyCode in ('144','145')
       then		140
       when		S.LocalSpecialtyCode in ('172')
       then		171
       when		S.LocalSpecialtyCode in ('219')
       then		171
       when		S.LocalSpecialtyCode in ('106')
       then		100
       else		cast(left(S.LocalSpecialtyCode,3) AS Integer) 
       End	 = D.SpecialtyFunctionCode
					
					
   where
   WL.CensusDate in
   (
	(
Select
    Max(WL.CensusDate)

  from  [WH].[APC].[WL] WL
 right join
  [WHREPORTING].[LK].[Calendar] Cal
  on Cal.TheDate = WL.CensusDate
  and WL.CensusDate <=  DATEADD(year,@CompYearPar,@MonthEndPar) 
  right join 
 ( select
FY.FinancialYear
 FROM [WHREPORTING].[LK].[Calendar] FY
 where FY.TheDate =  DATEADD(year,@CompYearPar,@MonthEndPar) 
 )SQ
 on SQ.FinancialYear = Cal.FinancialYear
)
UNION ALL
	(
Select
    Max(WL.CensusDate)

  from  [WH].[APC].[WL] WL
 right join
  [WHREPORTING].[LK].[Calendar] Cal
  on Cal.TheDate = WL.CensusDate
  right join 
 ( select
FY.FinancialYear
 FROM [WHREPORTING].[LK].[Calendar] FY
 where FY.TheDate = DATEADD(year,(@CompYearPar-1),@MonthEndPar) 
 )SQ
 on SQ.FinancialYear = Cal.FinancialYear
)
)
and   WL.SpecialtyCode not in ('2000842','2000888')			--exclude Neurology, Physio
   --	 S.LocalSpecialtyCode   	 --not in ('650','400') --exclude Neurology, Physio
and  case 
      when
      IM.NationalManagementIntentionCode in ('2','4')
      then 'D'
      else 'I'
      end in (SELECT Val from dbo.fn_String_To_Table(@InpatientPar,',',1))
 
   
   group by
 FY.FinancialYear 
 ,Cal.TheMonth 
,WL.CensusDate
,D.Division
,D.Direcorate 
,D.SpecialtyFunctionCode
, D.SpecialtyFunction

      ,case 
      when
      IM.NationalManagementIntentionCode in ('2','4')
      then 'D'
      else 'I'
      end 
      )
      ---------End of WL2---------	
      )
      U
      order by U.Area,U.Period,U.SpecialtyCode