﻿/*
--Author: N Scattergood
--Date created: 27/02/2013
--Stored Procedure Built for SRSS Report on:
--IP Elective Demand
--This Procedure provides the Parameters for the Comparison Year Perios
*/


Create Procedure [SC].[ReportDemandElectiveIP_CompYear]
as

Select distinct
Cal.FinancialYear
,cast(left(Cal.FinancialYear,4) as INT) - cast(left(CY.FinancialYear,4) as INT) as CompYearPar 

 FROM [WHREPORTING].[LK].[Calendar] Cal
 
 left join  [WHREPORTING].[LK].[Calendar] CY
 on CY.TheDate = DATEADD(MONTH,-1,CAST(getdate() AS date)) 
 
 where
Cal.FinancialYear =
   ( select
FY.FinancialYear
 FROM [WHREPORTING].[LK].[Calendar] FY
 where FY.TheDate = 
 (DATEADD(MONTH,-25,CAST(getdate() AS date))) 
 )
 or
 Cal.FinancialYear =
   ( select
FY.FinancialYear
 FROM [WHREPORTING].[LK].[Calendar] FY
 where FY.TheDate = 
 (DATEADD(MONTH,-13,CAST(getdate() AS date))) 
 )
 
 order by
 Cal.FinancialYear desc