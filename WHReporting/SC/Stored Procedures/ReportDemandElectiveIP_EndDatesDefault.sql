﻿/*
--Author: N Scattergood
--Date created: 26/02/2013
--Stored Procedure Built for SRSS Report on:
--IP Elective Demand
--This Procedure provides the Parameters for End Date 
-- Provides Default (last month)

*/


Create Procedure [SC].[ReportDemandElectiveIP_EndDatesDefault]

as

select 

 Cal.TheMonth as MonthDropdown
 --,Cal.FinancialYear
 ,max(Cal.TheDate) as  ActivityEndPar
 
  FROM [WHREPORTING].[LK].[Calendar] Cal
  
  where

 Cal.TheMonth =
   ( select
   distinct
CM.TheMonth
 FROM [WHREPORTING].[LK].[Calendar] CM
where CM.TheDate = 
 (DATEADD(MONTH,-1,CAST(getdate() AS date))) 
)
 group by
Cal.TheMonth

order by
max(Cal.TheDate) desc