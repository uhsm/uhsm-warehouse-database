﻿/*
--Author: N Scattergood
--Date created: 23/01/2013
--Stored Procedure Built for SRSS Report on:
--IP Elective Demand
--Provides the Parameter for Inpatient/DayCase
*/

CREATE Procedure [SC].[ReportDemandElectiveIP_InpatientType]
as

select
Distinct
case 
when S.InpatientStayCode = 'I'
then 'Inpatient'
else 'Daycase'
end as IPTypePar
,S.InpatientStayCode as IPTypeCodePar


  FROM [WHREPORTING].[APC].[Spell] S
    
    where
  	S.[InpatientStayCode] in ('I','D')
and
	S.DischargeDate >  dateadd(month,-1,getdate())
	
	order by 
	S.InpatientStayCode desc