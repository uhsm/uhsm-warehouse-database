﻿/*
--Author: N Scattergood
--Date created: 24th September 2012
--Stored Procedure Built for the SRSS Report on LoSConsultantTracker
--
--This is used to pullout the Default Weekending Parameter
--At the current time it only pulls out the last 12 weeks
*/

CREATE Procedure [SC].[ReportLoSConsultantTracker_WkEnding]
as

Select
 [LastDayOfWeek]

  FROM [WHREPORTING].[LK].[Calendar]
  
  where
  TheDate between
  DATEADD(dd,-42,CAST(getdate() as DATE))
  and
  DATEADD(dd,-7,CAST(getdate() as DATE))
  
  
  group by
 [LastDayOfWeek]
  
  order by
 [LastDayOfWeek]
   desc