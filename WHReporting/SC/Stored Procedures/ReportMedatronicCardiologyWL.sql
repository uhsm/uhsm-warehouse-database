﻿/*
--Author: N Scattergood
--Date created: 23/01/2013
--Stored Procedure Built for SRSS Report on:
--IP Elective Demand
--Contains both the IP and WL Section
*/


CREATE Procedure [SC].[ReportMedatronicCardiologyWL]
 
 as
 select
   S.LocalSpecialtyCode
      ,S.Specialty
         ,case 
      when
      IM.NationalManagementIntentionCode in ('2','4')
      then 'Daycase'
      else 'Inpatient'
      end as IntendedStay
      ,Con.Consultant
    ,WL.IntendedPrimaryOperationCode
	,WL.Operation
 ,WL.CensusDate
 ,SQ.TheMonth
 ,Count(*) as TotalOnWL
 
  FROM [WH].[APC].[WL] WL
  
  right join
 
 (
 select 
 distinct
  
 max(WL.CensusDate) as LastCensus
 ,Cal.TheMonth
 ,Cal.FinancialYear 
 
 FROM [WH].[APC].[WL] WL
 
 left join WH.WH.Calendar Cal
 on	WL.CensusDate = Cal.TheDate
 --and Cal.FinancialYear = '2012/2013'
 
 where
	datename(mm,WL.CensusDate)+' '+datename(yy,WL.CensusDate) <>  datename(mm,GETDATE())+ ' '+datename(yy,GETDATE())
 and 
	WL.CensusDate > '01 apr 2011'
 group by
Cal.TheMonth
,Cal.FinancialYear 
--order by  max(WL.CensusDate)
   )  SQ
   
   on WL.CensusDate = SQ.LastCensus
   
   		Left Join [WH].[PAS].[Specialty] S
				on WL.SpecialtyCode = S.SpecialtyCode
				
				   		Left Join [WH].[PAS].[ManagementIntention] IM
								on WL.ManagementIntentionCode = IM.ManagementIntentionCode
								
								Left Join [WH].[PAS].[Consultant] Con
								on WL.ConsultantCode = Con.ConsultantCode
								
   where
    
	S.LocalSpecialtyCode = '320'
	
   group by
   S.LocalSpecialtyCode
      ,S.Specialty
         ,case 
      when
      IM.NationalManagementIntentionCode in ('2','4')
      then 'Daycase'
      else 'Inpatient'
      end 
       ,Con.Consultant
    ,WL.IntendedPrimaryOperationCode
	,WL.Operation
 ,WL.CensusDate
 ,SQ.TheMonth
    
   order by
     --IM.ManagementIntention,

    WL.CensusDate desc ,  S.Specialty