﻿/*
--Author: N Scattergood
--Date created: 05/10/2012
--Stored Procedure Built for the SRSS Report to replace the Daily Mortality Coding Report
--i.e. Those patients who have been dishcharged as Dead and have been coded
-- only holds data for last 7 days
*/

CREATE Procedure [SC].[ReportMortalityDailyCodingCompleted]

@CodedDate as varchar (max)

as

SELECT 
--ModifedDate, 

D.ModifedDate as ReportDateRAW,
CONVERT(varchar, D.ModifedDate,105) as ReportDateDATE,
floor(cast((D.[ModifiedDateTime]) as float)) as ReportDateKEY,


S.[FaciltyID]
--,S.[DischargeMethod] 
,P.PatientForename
,P.PatientSurname
,S.AdmissionDate
,S.DischargeDate

      ,E.EpisodeStartDateTime
      ,E.ConsultantName
      
      ,cc.[PrimaryDiagnosis]
      ,cc.[SubsidiaryDiagnosis]
      ,cc.[SecondaryDiagnosis1]
      ,cc.[SecondaryDiagnosis2]
      ,cc.[SecondaryDiagnosis3]
      ,cc.[SecondaryDiagnosis4]
      ,cc.[SecondaryDiagnosis5]    
      ,cc.[SecondaryDiagnosis6]     
      ,cc.[SecondaryDiagnosis7]  
      ,cc.[SecondaryDiagnosis8]    
      ,cc.[SecondaryDiagnosis9]      
      ,cc.[SecondaryDiagnosis10]
      ,cc.[SecondaryDiagnosis11]
      ,cc.[SecondaryDiagnosis12]
      ,cc.[SecondaryDiagnosis13]
      ,cc.[SecondaryDiagnosis14]
      --,cc.[SecondaryDiagnosis15]
      --,cc.[SecondaryDiagnosis16]
      --,cc.[SecondaryDiagnosis17]
      --,cc.[SecondaryDiagnosis18]
      --,cc.[SecondaryDiagnosis19]
      --,cc.[SecondaryDiagnosis20]   
      
      ,cc.[PriamryProcedure]
      ,cc.[SecondaryProcedure1]
      ,cc.[SecondaryProcedure2]
      ,cc.[SecondaryProcedure3]
      ,cc.[SecondaryProcedure4]
      ,cc.[SecondaryProcedure5]
      ,cc.[SecondaryProcedure6]
      ,cc.[SecondaryProcedure7]
      ,cc.[SecondaryProcedure8]
      ,cc.[SecondaryProcedure9]
      ,cc.[SecondaryProcedure10]
      ,cc.[SecondaryProcedure11]
           
 FROM [WHREPORTING].[APC].[Spell] S
  
    left join [WHREPORTING].[APC].[Episode]  E
            on E.[SourceSpellNo] = S.[SourceSpellNo]
            --and E.LastEpisodeInSpell = '1'
            
        left join [WHREPORTING].[APC].[Patient] P
        on P.EncounterRecno = S.EncounterRecno
        
				left join [WHREPORTING].[APC].[ClinicalCoding] Cc
				on Cc.EncounterRecno = E.EncounterRecno
            
					left join [WHREPORTING].[APC].[AllDiagnosis] D
					on D.EncounterRecno = E.EncounterRecno
					and D.DiagnosisSequence = '1'
  
    where
  

  D.ModifedDate between
	DATEADD(dd,-8,CAST(getdate() as DATE))
  and
	CAST(getdate() as DATE)
  and
    S.[DischargeMethodCode] = '4'
    
    	and

floor(cast((D.[ModifiedDateTime]) as float)) in (select * from SC.list_to_tbl (@CodedDate))


      
      order by 
      D.ModifedDate desc,
      S.[FaciltyID],
      E.EpisodeStartDateTime