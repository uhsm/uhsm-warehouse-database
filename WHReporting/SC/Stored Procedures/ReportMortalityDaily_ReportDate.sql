﻿/*
--Author: N Scattergood
--Date created: 05/10/2012
--Stored Procedure Built for the SRSS Report Daily Mortality Coding Report
--i.e. Used for Parameter on Coding Completed
-- Defaults to last 7 days (no coding completed at weekends)
*/

Create Procedure [SC].[ReportMortalityDaily_ReportDate]
as


SELECT 
Distinct
CONVERT(varchar, D.ModifedDate,105) as ReportDateDATE,
floor(cast((D.[ModifiedDateTime]) as float)) as ReportDateKEY


           
 FROM [WHREPORTING].[APC].[AllDiagnosis] D
				
  
    where
  

  D.ModifedDate between
	DATEADD(dd,-8,CAST(getdate() as DATE))
  and
	CAST(getdate() as DATE)
  and
   	D.DiagnosisSequence = '1'

      
      order by 
floor(cast((D.[ModifiedDateTime]) as float)) desc