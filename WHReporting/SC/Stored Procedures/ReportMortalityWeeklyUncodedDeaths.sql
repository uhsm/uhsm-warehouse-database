﻿/*
--Author: N Scattergood
--Date created: 27/11/2012
--Stored Procedure Built for the SRSS Report to replace the Weekly Mortality Coding Report
--i.e. Those patients who have been dishcharged as Dead and have not been coded (No Primary Diagnosis given)
*/

CREATE Procedure [SC].[ReportMortalityWeeklyUncodedDeaths]

@ReportDate as Date
,@SpecPar as nvarchar(max)
,@Coded  as nvarchar(max)

as


SELECT  

case when D.ModifedDate is null then 'Not Coded'
		else 'Coded'
			end as [CodingStatus],
			
--D.UserModified,
S.AdmissionDateTime,
S.DischargeDateTime,

--,datename(mm,S.DischargeDateTime  ) + '-' + datename(yy,S.DischargeDateTime  )as Discharge_Month,
--S.AdmissionDate,
--convert(varchar(8),S.AdmissionDateTime ,14) as Admission_Time,
--S.DischargeDate,
--D.ModifedDate as DateCoded,
S.NHSNo,
S.SourceSpellNo,
E.EpisodeStartDateTime,
E.EpisodeEndDateTime,
S.[FaciltyID],
P.PatientForename,
P.PatientSurname,
S.AgeCode,

S.AdmissionMethod,
S.InpatientStayCode,




--E.StartWardCode,
E.[Specialty(Function)] as DischargeSpecialty,
E.ConsultantName as DischargeConsultant,
D.Diagnosis as PrimaryDischargeDiagnosis,
sp.DESCRIPTION as CaseNoteLocation,
cas.LocationCode CurrentCasenoteLocationCode,
cas.Location CurrentCasenoteLocation,
cas.Notes CasenoteLocationComments
 
      --,cc.[PrimaryDiagnosis]
      --,cc.[SubsidiaryDiagnosis]
      --,cc.[SecondaryDiagnosis1]
      --,cc.[SecondaryDiagnosis2]
      --,cc.[SecondaryDiagnosis3]
      --,cc.[SecondaryDiagnosis4]
      --,cc.[SecondaryDiagnosis5]    
      --,cc.[SecondaryDiagnosis6]     
      --,cc.[SecondaryDiagnosis7]  
      --,cc.[SecondaryDiagnosis8]    
      --,cc.[SecondaryDiagnosis9]      
      --,cc.[SecondaryDiagnosis10]
      --,cc.[SecondaryDiagnosis11]
      --,cc.[SecondaryDiagnosis12]
      --,cc.[SecondaryDiagnosis13]
      --,cc.[SecondaryDiagnosis14]
      ----,cc.[SecondaryDiagnosis15]
      ----,cc.[SecondaryDiagnosis16]
      ----,cc.[SecondaryDiagnosis17]
      ----,cc.[SecondaryDiagnosis18]
      ----,cc.[SecondaryDiagnosis19]
      ----,cc.[SecondaryDiagnosis20]   
      
      --,cc.[PriamryProcedure]
      --,cc.[SecondaryProcedure1]
      --,cc.[SecondaryProcedure2]
      --,cc.[SecondaryProcedure3]
      --,cc.[SecondaryProcedure4]
      --,cc.[SecondaryProcedure5]
      --,cc.[SecondaryProcedure6]
      --,cc.[SecondaryProcedure7]
      --,cc.[SecondaryProcedure8]
      --,cc.[SecondaryProcedure9]
      --,cc.[SecondaryProcedure10]
      --,cc.[SecondaryProcedure11]
           

      
  FROM [WHREPORTING].[APC].[Spell] S
  
    left join [WHREPORTING].[APC].[Episode]  E
		on E.[SourceSpellNo] = S.[SourceSpellNo]
		and E.LastEpisodeInSpell = '1'
		
	  left join [WHREPORTING].[APC].[Patient] P
	  on P.EncounterRecno = S.EncounterRecno
	  
	 -- 	  left join [WHREPORTING].[APC].[ClinicalCoding] Cc
		--on Cc.EncounterRecno = E.EncounterRecno
		
		left join [WHREPORTING].[APC].[AllDiagnosis] D
		 on D.EpisodeSourceUniqueID = E.EpisodeUniqueID
		 and D.DiagnosisSequence = '1'
		 
			left join [Lorenzo].[dbo].[CasenoteVolume] csv
			on S.FaciltyID = csv.CASVL_PASID
		 
			left join  [Lorenzo].dbo.ServicePoint sp
			on csv.CURNT_SPONT_REFNO = sp.SPONT_REFNo
			
		Left outer join WHREPORTING.CN.CasenoteCurrentLocationDataset cas
		on s.SourcePatientNo = cas.Patnt_refno --25/11/2014 CM:  Added this in to get current location, current location code and commments as per Lynn Ashton request 
	
  
--left outer join Lorenzo.dbo.vwCurrentCasenoteLocationLastMoved C
--on S.SourcePatientNo = C.SourcePatientNo
  

  
  where
  

S.DischargeDateTime  >=  @ReportDate
	and
S.[DischargeMethodCode] = '4'
and
E.[SpecialtyCode(Function)] in (SELECT Val from dbo.fn_String_To_Table(@SpecPar,',',1))
and
case when D.ModifedDate is null then 'Not Coded'
		else 'Coded' end in (SELECT Val from dbo.fn_String_To_Table(@Coded,',',1))
--and

--and
--D.ModifedDate is  null

	


	
	order by 
	
	case when D.ModifedDate is null then 'Not Coded'
		else 'Coded'
			 end desc
			,S.DischargeDateTime asc , S.[FaciltyID],E.EpisodeStartDateTime