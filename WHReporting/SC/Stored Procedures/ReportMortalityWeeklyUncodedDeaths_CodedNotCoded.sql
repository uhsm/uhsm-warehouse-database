﻿/*
--Author: N Scattergood
--Date created: 28/11/2012
--Stored Procedure Built for the SRSS Report to replace the Weekly Mortality Coding Report
--i.e. Those patients who have been dishcharged as Dead and have not been coded (No Primary Diagnosis given)
--This Procedure provides a Parameter for patients coded not coded which cascades from the selected ReportDate and Specialty
*/

CREATE Procedure [SC].[ReportMortalityWeeklyUncodedDeaths_CodedNotCoded]

@ReportDate as Date
,@SpecPar as nvarchar(max)

as


SELECT  
distinct
case when D.ModifedDate is null then 'Not Coded'
		else 'Coded'
			end as [CodingStatus]


  FROM [WHREPORTING].[APC].[Spell] S
  
    left join [WHREPORTING].[APC].[Episode]  E
		on E.[SourceSpellNo] = S.[SourceSpellNo]
		and E.LastEpisodeInSpell = '1'
		
			left join [WHREPORTING].[APC].[AllDiagnosis] D
		 on D.EpisodeSourceUniqueID = E.EpisodeUniqueID
		 and D.DiagnosisSequence = '1'
		
		
		where
		
		S.DischargeDateTime  >=  @ReportDate
	and
		S.[DischargeMethodCode] = '4'
--	and
--		(
--ISNUMERIC (E.[SpecialtyCode] ) = 1	
----or 
----[SpecialtyCode(Function)] = '652'  
--) 

and
E.[SpecialtyCode(Function)] in (SELECT Val from dbo.fn_String_To_Table(@SpecPar,',',1))

order by
case when D.ModifedDate is null then 'Not Coded'
		else 'Coded'
			end