﻿/*
--Author: N Scattergood
--Date created: 05/10/2012
--Stored Procedure Built for the SRSS Report to replace the Weekly Mortality Coding Report
--i.e. Those patients who have been dishcharged as Dead and have not been coded (No Primary Diagnosis given)
*/

CREATE Procedure [SC].[ReportMortalityWeeklyUncodedDeaths_Spec]

@ReportDate as Date

as


SELECT  
Distinct
E.[SpecialtyCode(Function)] as SpecCodeParameter
,E.[Specialty(Function)] as SpecParameter


  FROM [WHREPORTING].[APC].[Spell] S
  
    left join [WHREPORTING].[APC].[Episode]  E
		on E.[SourceSpellNo] = S.[SourceSpellNo]
		and E.LastEpisodeInSpell = '1'
		
		
		where
		
		S.DischargeDateTime  >=  @ReportDate
	and
		S.[DischargeMethodCode] = '4'
	and
		(
ISNUMERIC (E.[SpecialtyCode] ) = 1	
--or 
--[SpecialtyCode(Function)] = '652'  
) 

order by E.[Specialty(Function)]