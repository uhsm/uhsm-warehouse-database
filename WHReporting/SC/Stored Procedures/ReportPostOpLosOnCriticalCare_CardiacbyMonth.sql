﻿/*
--Author: N Scattergood
--Date created: 25/02/2012
--Stored Procedure Built based on the same query used for SRSS Report on:
--Post Op LoS split between Critical Care and Other Wards
--This Query is used to analyse Cardiothoracic on it's own by Month
*/



CREATE  Procedure [SC].[ReportPostOpLosOnCriticalCare_CardiacbyMonth]
as
Select
--S.SourceSpellNo,
--,S.FaciltyID
--,S.AdmissionDateTime
--,convert(char,C.PrimaryProcedureDate,103) as [FirstEpisodeProcDate]
--,S.DischargeDateTime
--  ,W.[StartTime]
--   ,   W.[EndTime]

      datename(mm,S.DischargeDateTime ) + ' ' + datename(yy,S.DischargeDateTime ) as 'DischargeMonth'   
      ,E.[Directorate] as 'DischargeDirectorate'
      ,  coalesce(CARDIAC.CardiacSpecCode, E.[SpecialtyCode(Function)])  'DischargeSpecialtyCode'
      ,	 coalesce(CARDIAC.CardiacSpecDesc, E.[Specialty(Function)]) 'DischargeSpecialty'
      ,  E.ConsultantCode as 'DischargeConsultant'	
      ,	 E.ConsultantName as 'DischargeConsultantName'
      , case	when S.[AdmissionType] in ('Emergency','Non-Elective') then 'Non-Elective'
		else S.[AdmissionType] end as 'AdmissionType' 
   
   ,sum
   (case	when	(
					cast(W.[StartTime] as Date) <= cast(C.PrimaryProcedureDate as Date)
					and 
					cast(W.[EndTime] as Date) > cast(C.PrimaryProcedureDate as Date)
					)
			then	1
			when	cast(W.[StartTime] as Date) > cast(C.PrimaryProcedureDate as Date)
			then	0
			else	0
			end)		as 'Count'
			

, sum
(case		when	cast(W.[StartTime] as Date) <= cast(C.PrimaryProcedureDate as Date)
			then	DATEDIFF (dd,C.PrimaryProcedureDate,W.EndTime)
			when	cast(W.[StartTime] as Date) > cast(C.PrimaryProcedureDate as Date)
			then	DATEDIFF (dd,W.[StartTime],W.EndTime)
			else	'Check SQL'
			end	)	as PostOpWardLOS
			
    
    --,  W.[WardCode]
, sum	
(case	when	W.[WardCode] in ('CTCU','CT TRANSPLANT','ICA')
			then	1
			else	0
			End) as CriticalCareStays    
,	case	when	W.[WardCode] in ('CTCU','CT TRANSPLANT','ICA')
			then 'CriticalCare'
			else 'OtherWard'
			End as WardType
			
   --   ,S.[LengthOfSpell]  
   --   ,DATEDIFF(dd, S.AdmissionDateTime		, C.PrimaryProcedureDate  ) as [PreOpLOS]
	  --,DATEDIFF(dd, C.PrimaryProcedureDate	, S.DischargeDate  ) as [PostOpLOS] 
      --,E.[LengthOfEpisode]
      --,E.[PrimaryDiagnosisCode]
      --,E.[PrimaryProcedureCode]

      
  FROM [WHREPORTING].[APC].[Spell] S
  
    left join [WHREPORTING].[APC].[Episode]  E
		on E.[SourceSpellNo] = S.[SourceSpellNo]
		and E.LastEpisodeInSpell = '1'
			
		left join [WHREPORTING].[APC].[ClinicalCoding] C
		on  C.EncounterRecno = S.EncounterRecno
		
				left join [WHREPORTING].[APC].[WardStay] W
				on S.SourceSpellNo = W.SourceSpellNo
				
				left join [WHREPORTING].[SC].[vwAPCCardiacConsultantSpec] CARDIAC
				on E.ConsultantCode = CARDIAC.CardiacConsCode
				--and E.Directorate =  'Cardio & Thoracic'
  

  where
  
  	S.DischargeDateTime between
  	  ----Sub Query for Rolling 13 Months---- 
	( select  distinct min(Cal.TheDate)
		FROM LK.Calendar Cal
			where
			Cal.TheMonth = 
			(select distinct
			SQ.TheMonth
			  FROM LK.Calendar SQ
			  where SQ.TheDate = 
			 (
			dateadd(MM,-13,cast(GETDATE()as DATE))
			)))
	 and 
	( select distinct
	dbo.GetEndOfDay (max(Cal.TheDate)) 
	FROM LK.Calendar Cal
		where
			Cal.TheMonth = 
			(select distinct
			SQ.TheMonth
			  FROM LK.Calendar SQ
			  where SQ.TheDate = 
			(
			dateadd(MM,-1,cast(GETDATE()as DATE))
			)))	 
	  ----End of Query for Rolling 13 Months---- 
	  
	and
	S.[InpatientStayCode] <> 'D'
	and
	S.[LengthOfSpell] > '0'
	and
	DATEDIFF(dd, S.AdmissionDateTime, C.PrimaryProcedureDate  ) >= '0'
	and
	cast(W.[EndTime] as Date) >= cast(C.PrimaryProcedureDate as Date)
	and
	E.Division <> 'Unknown'
	and
	E.Directorate = 'Cardio & Thoracic'
	
		----Parameters----
		
		--@DIVPar as nvarchar(max)
--,@dirPar as nvarchar(max)
--,@SpecPar as nvarchar(max)
--,@ConsPar as nvarchar(max)
--,@AdmMethod as nvarchar(max)
	
	--and E.Division in (SELECT Val from dbo.fn_String_To_Table(@DIVPar,',',1))
	--and E.Directorate in (SELECT Val from dbo.fn_String_To_Table(@dirPar,',',1))
	--and coalesce(CARDIAC.CardiacSpecCode, E.[SpecialtyCode(Function)])
	--in (SELECT Val from dbo.fn_String_To_Table(@SpecPar,',',1))
	--and E.ConsultantCode in (SELECT Val from dbo.fn_String_To_Table(@ConsPar,',',1))
	--and case	when S.[AdmissionType] in ('Emergency','Non-Elective') then 'Non-Elective'
	--	else S.[AdmissionType] end in (SELECT Val from dbo.fn_String_To_Table(@AdmMethod,',',1))
	


group by
      datename(mm,S.DischargeDateTime ) + ' ' + datename(yy,S.DischargeDateTime )
      ,E.[Directorate] 
      ,  coalesce(CARDIAC.CardiacSpecCode, E.[SpecialtyCode(Function)])  
      ,	 coalesce(CARDIAC.CardiacSpecDesc, E.[Specialty(Function)])
      ,  E.ConsultantCode 
      ,	 E.ConsultantName 
      , case	when S.[AdmissionType] in ('Emergency','Non-Elective') then 'Non-Elective'
		else S.[AdmissionType] end 
		
		,	case	when	W.[WardCode] in ('CTCU','CT TRANSPLANT','ICA')
			then 'CriticalCare'
			else 'OtherWard'
			End
	
	order by
	--E.Division
	    E.[Directorate] 
        ,  coalesce(CARDIAC.CardiacSpecCode, E.[SpecialtyCode(Function)]) 
      ,  E.ConsultantCode