﻿/*
--Author: N Scattergood
--Date created: 14/12/2012
--Stored Procedure Built for SRSS Report on:
--Post Op LoS split between Critical Care and Other Wards
--This Procedure Provides the Parameters
*/


CREATE Procedure [SC].[ReportPostOpLosOnCriticalCare_Division]

--@DIVPar as nvarchar(max)
--,@dirPar as nvarchar(max)
--,@SpecPar as nvarchar(max)
--,@ConsPar as nvarchar(max)
--,@AdmMethod as nvarchar(max)

As
Declare
@START AS datetime = DATEADD(dd,-(DAY(DATEADD(mm,1,cast(getdate()as date)))-1),DATEADD(mm,-13,cast(getdate()as date)))

,@END AS datetime =  WHREPORTING.dbo.GetEndOfDay(DATEADD(dd, -DAY(DATEADD(m,1,getdate())), DATEADD(m,-0,getdate())))
---Rolling 13 Months


SELECT  

Distinct



      E.Division			as 'DIVPar' 
  --    E.[Directorate]		as 'dirPar'
  --    coalesce(CARDIAC.CardiacSpecCode, E.[SpecialtyCode(Function)])  'SpecCodePar'
  --    ,	 coalesce(CARDIAC.CardiacSpecDesc, E.[Specialty(Function)]) 'SpecPar'
  --    ,  E.ConsultantCode	as 'ConsCodePar'	
  --    ,	 E.ConsultantName	as 'ConsPar'
  --     case	when S.[AdmissionType] in ('Emergency','Non-Elective') then 'Non-Elective'
		--else S.[AdmissionType] end as 'AdmissionMethodPar' 
   

      
  FROM [WHREPORTING].[APC].[Spell] S
  
    left join [WHREPORTING].[APC].[Episode]  E
		on E.[SourceSpellNo] = S.[SourceSpellNo]
		and E.LastEpisodeInSpell = '1'
			
		left join [WHREPORTING].[APC].[ClinicalCoding] C
		on  C.EncounterRecno = S.EncounterRecno
		
				--left join [WHREPORTING].[APC].[WardStay] W
				--on S.SourceSpellNo = W.SourceSpellNo
				
				left join [WHREPORTING].[SC].[vwAPCCardiacConsultantSpec] CARDIAC
				on E.ConsultantCode = CARDIAC.CardiacConsCode
				--and E.Directorate =  'Cardio & Thoracic'
  

  where
  
  	S.DischargeDateTime between @START and @END
  	--13 Months	  
	and
	S.[InpatientStayCode] <> 'D'
	and
	S.[LengthOfSpell] > '0'
	and
	E.Division <> 'Unknown'
	--and
	--DATEDIFF(dd, S.AdmissionDateTime, C.PrimaryProcedureDate  ) >= '0'
	and
	DATEDIFF(dd, C.PrimaryProcedureDate,  S.DischargeDateTime ) > '0'
	
		----Parameters----
		
		--@DIVPar as nvarchar(max)
--,@dirPar as nvarchar(max)
--,@SpecPar as nvarchar(max)
--,@ConsPar as nvarchar(max)
--,@AdmMethod as nvarchar(max)
	
	--and S.LocalSpecialtyCode in (SELECT Val from dbo.fn_String_To_Table(@SpecPar,',',1))
	--and C.ConsultantCode	 in (SELECT Val from dbo.fn_String_To_Table(@ConsPar,',',1))
	--and P.PriorityCode	     in (SELECT Val from dbo.fn_String_To_Table(@UrgencyPar,',',1))	


	
	order by
	E.Division
	  --,  E.[Directorate] 
   --     ,  coalesce(CARDIAC.CardiacSpecCode, E.[SpecialtyCode(Function)]) 
   --   ,  E.ConsultantCode