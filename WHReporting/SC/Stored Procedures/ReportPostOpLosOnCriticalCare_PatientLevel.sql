﻿/*
--Author: N Scattergood
--Date created: 12/12/2012
--Stored Procedure Built for SRSS Report on:
--Post Op LoS split between Critical Care and Other Wards
*/


CREATE Procedure [SC].[ReportPostOpLosOnCriticalCare_PatientLevel]


as
Declare
@START AS datetime = DATEADD(dd,-(DAY(DATEADD(mm,1,cast(getdate()as date)))-1),DATEADD(mm,-13,cast(getdate()as date)))
,@END AS datetime =  WHREPORTING.dbo.GetEndOfDay(DATEADD(dd, -DAY(DATEADD(m,1,getdate())), DATEADD(m,-0,getdate())))
---Rolling 13 Months

SELECT  

S.SourceSpellNo
,S.FaciltyID
,S.AdmissionDateTime
,convert(char,C.PrimaryProcedureDate,103) as [FirstEpisodeProcDate]
,S.DischargeDateTime
  ,W.[StartTime]
   ,   W.[EndTime],

      E.Division as   'DischargeDivision'   
      ,E.[Directorate] as 'DischargeDirectorate'
      ,  coalesce(CARDIAC.CardiacSpecCode, E.[SpecialtyCode(Function)])  'DischargeSpecialtyCode'
      ,	 coalesce(CARDIAC.CardiacSpecDesc, E.[Specialty(Function)]) 'DischargeSpecialty'
      ,  E.ConsultantCode as 'DischargeConsultant'	
      ,	 E.ConsultantName as 'DischargeConsultantName'
      , case	when S.[AdmissionType] in ('Emergency','Non-Elective') then 'Non-Elective'
		else S.[AdmissionType] end as 'AdmissionType' 
   
   ,sum
   (case	when	(
					cast(W.[StartTime] as Date) <= cast(C.PrimaryProcedureDate as Date)
					and 
					cast(W.[EndTime] as Date) > cast(C.PrimaryProcedureDate as Date)
					)
			then	1
			when	cast(W.[StartTime] as Date) > cast(C.PrimaryProcedureDate as Date)
			then	0
			else	0
			end)		as 'Count'
			

, sum
(case		when	cast(W.[StartTime] as Date) <= cast(C.PrimaryProcedureDate as Date)
			then	DATEDIFF (dd,C.PrimaryProcedureDate,W.EndTime)
			when	cast(W.[StartTime] as Date) > cast(C.PrimaryProcedureDate as Date)
			then	DATEDIFF (dd,W.[StartTime],W.EndTime)
			else	'Check SQL'
			end	)	as PostOpWardLOS
    
    --,  W.[WardCode]
, sum	
(case	when	W.[WardCode] in ('CTCU','CT TRANSPLANT','ICA')
			then	1
			else	0
			End) as CriticalCareStays    
,	case	when	W.[WardCode] in ('CTCU','CT TRANSPLANT','ICA')
			then 'CriticalCare'
			else 'OtherWard'
			End as WardType
			
   --   ,S.[LengthOfSpell]  
   --   ,DATEDIFF(dd, S.AdmissionDateTime		, C.PrimaryProcedureDate  ) as [PreOpLOS]
	  --,DATEDIFF(dd, C.PrimaryProcedureDate	, S.DischargeDate  ) as [PostOpLOS] 
      --,E.[LengthOfEpisode]
      --,E.[PrimaryDiagnosisCode]
      --,E.[PrimaryProcedureCode]

      
  FROM [WHREPORTING].[APC].[Spell] S
  
    left join [WHREPORTING].[APC].[Episode]  E
		on E.[SourceSpellNo] = S.[SourceSpellNo]
		and E.LastEpisodeInSpell = '1'
			
		left join [WHREPORTING].[APC].[ClinicalCoding] C
		on  C.EncounterRecno = S.EncounterRecno
		
				left join [WHREPORTING].[APC].[WardStay] W
				on S.SourceSpellNo = W.SourceSpellNo
				
				left join [WHREPORTING].[SC].[vwAPCCardiacConsultantSpec] CARDIAC
				on E.ConsultantCode = CARDIAC.CardiacConsCode
				--and E.Directorate =  'Cardio & Thoracic'
  

  where
  
  	S.DischargeDateTime between @START and @END
 	and
	S.[InpatientStayCode] <> 'D'
	and
	S.[LengthOfSpell] > '0'
	and
	DATEDIFF(dd, S.AdmissionDateTime, C.PrimaryProcedureDate  ) >= '0'
	and
	cast(W.[EndTime] as Date) >= cast(C.PrimaryProcedureDate as Date)
	and
	E.Division <> 'Unknown'
	



group by
S.SourceSpellNo
,S.FaciltyID
,S.AdmissionDateTime
,convert(char,C.PrimaryProcedureDate,103) 
,S.DischargeDateTime
  ,W.[StartTime]
   ,   W.[EndTime],
      E.Division  
      ,E.[Directorate] 
      ,  coalesce(CARDIAC.CardiacSpecCode, E.[SpecialtyCode(Function)])  
      ,	 coalesce(CARDIAC.CardiacSpecDesc, E.[Specialty(Function)])
      ,  E.ConsultantCode 
      ,	 E.ConsultantName 
      , case	when S.[AdmissionType] in ('Emergency','Non-Elective') then 'Non-Elective'
		else S.[AdmissionType] end 
		
		,	case	when	W.[WardCode] in ('CTCU','CT TRANSPLANT','ICA')
			then 'CriticalCare'
			else 'OtherWard'
			End
	
	order by
	E.Division
	  ,  E.[Directorate] 
        ,  coalesce(CARDIAC.CardiacSpecCode, E.[SpecialtyCode(Function)]) 
      ,  E.ConsultantCode