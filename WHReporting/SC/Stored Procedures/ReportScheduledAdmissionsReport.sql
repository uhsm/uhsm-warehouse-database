﻿/*
--Author: N Scattergood
--Date created: 06/09/2012
--Stored Procedure Built for the SRSS Report on Scheuled Admission
--i.e. Those patients on the IP WL Scheduled to have an Admission in the next 2 weeks
--Current Issues 
--1) Missing Cancer flag
--2) There are Nulls in the WL.[Operation] field where the WL.[IntendedPrimaryOperationCode] is populated
*/

CREATE Procedure [SC].[ReportScheduledAdmissionsReport]

@Specialty nvarchar (max)
,@Ward nvarchar (max)
,@IntendedManagement as nvarchar (max)
,@WLType  nvarchar (max)
,@Canc		nvarchar (max)
,@Gender  nvarchar (max)
,@TCI  nvarchar (max)
,@PROM  nvarchar (max)
,@PROC nvarchar (max)
as

SELECT 
		--WL.[EncounterRecno]
		1 as Data
      ,WL.[CensusDate]
      --,WL.[SourceUniqueID]
      --,WL.[SourcePatientNo]
      --,WL.[SourceEncounterNo]
      --,WL.[ReferralSourceUniqueID]
      --,WL.[NHSNumber]
      ,WL.[DistrictNo] as 'RM2'
      --,WL.[PatientTitle]
      ,WL.[PatientForename]
      ,WL.[PatientSurname]
      --,WL.[DateOfBirth]
      --,WL.[DateOfDeath]
      ,CASE 
      WHEN WL.[SexCode] = '9269' THEN 'Female'
      WHEN WL.[SexCode] = '9270' THEN 'Male'
      Else Null End as 'Gender'
      ,WL.[SexCode] as 'GenderKey'

      --,WL.[Postcode]
      --,WL.[PatientAddress1]
      --,WL.[PatientAddress2]
      --,WL.[PatientAddress3]
      --,WL.[PatientAddress4]
      --,WL.[DHACode]
      --,WL.[HomePhone]
      --,WL.[WorkPhone]
      --,WL.[EthnicOriginCode]
      --,WL.[MaritalStatusCode]
      --,WL.[ReligionCode]
      ,C.[Consultant]
      ,WL.SpecialtyCode as SpecialtyRef
      ,S.LocalSpecialtyCode
      ,S.Specialty
      --,WL.[PASSpecialtyCode]
      ,        CASE 
                WHEN MI.NationalManagementIntentionCode= '3' THEN 1 
                ELSE MI.NationalManagementIntentionCode
                End as 'IntendedManagementKey'
      ,CASE 
                WHEN MI.NationalManagementIntentionCode = '1' THEN 'IP' 
                WHEN MI.NationalManagementIntentionCode= '2' THEN 'DC' 
                WHEN MI.NationalManagementIntentionCode= '3' THEN 'IP' 
                WHEN MI.NationalManagementIntentionCode= '8' THEN 'Not Applicable' 
                WHEN MI.NationalManagementIntentionCode= '9' THEN 'Not Known' 
                ELSE MI.[ManagementIntention]
                End as 'IntendedManagement'
               
      --,MI.[ManagementIntention]
      ,		CASE 
                WHEN AM.AdmissionMethodLocalCode in ('11','12') THEN 11
                WHEN AM.AdmissionMethodLocalCode = '13' THEN 13
                ELSE AM.AdmissionMethodLocalCode
                END as 'WLTypeKey'  
      ,CASE 
                WHEN AM.AdmissionMethodLocalCode in ('11','12') THEN 'Waiting'
                WHEN AM.AdmissionMethodLocalCode = '13' THEN 'Planned'
                ELSE AM.AdmissionMethod   
                END as 'WLType'         
      
      --,AM.[AdmissionMethod]
      ,P.[Priority]
      --,WL.[WaitingListCode]
      --,WL.[CommentClinical]
      --,WL.[CommentNonClinical]
      ,WL.[IntendedPrimaryOperationCode]
      ,WL.[Operation]
      
      ,CASE 
      WHEN [IntendedPrimaryOperationCode] IN
      (
       'W371', 'W378','W379', 'W381','W388', 'W389','W391', 'W398','W399'
      ,'W461', 'W468','W469', 'W471','W478', 'W479','W481', 'W488','W489'
      ,'W931', 'W938','W939', 'W941','W948', 'W949','W951', 'W958','W959'
      ,'W521', 'W528','W529', 'W531','W538', 'W539','W541', 'W548','W549'
      ,'W370', 'W372','W373', 'W374','W380', 'W382','W383', 'W384','W390'
      ,'W392', 'W393', 'W394','W395', 'W396','W460', 'W462','W463', 'W470'
      ,'W472', 'W473','W480', 'W482','W483', 'W484','W485','W930', 'W932'
      ,'W933', 'W940','W942', 'W943','W950', 'W952','W953', 'W954','W520'
      , 'W522','W523', 'W530','W532', 'W533','W540', 'W542','W543', 'W544'
      --PROMS Unilateral HIP Replacement
	,'W401'	,'W409'	,'W418'	,'W421'	,'W429'	,'W521'	,'W529'	,'W538'	,'W541'	
	,'W549'	,'O181'	,'O189'	,'W400'	,'W403'	,'W410'	,'W413'	,'W420'	,'W423'	
	,'W425'	,'W520'	,'W523'	,'W532'	,'W540'	,'W543'	,'O180'	,'O183'
	,'W408'	,'W411'	,'W419'	,'W428'	,'W528'	,'W531'	,'W539'	,'W548'	,'O188'
	,'W402'	,'W404'	,'W412'	,'W414'	,'W422'	,'W424'	,'W426'	,'W522'	,'W530'	
	,'W533'	,'W542'	,'W544'	,'O182'	,'O184'
	--PROMS Unilateral KNEE Replacement
		,'L852'	,'L859'	,'L871'	,'L873'	,'L875'	,'L878'	,'L883'	,'L889'	
		,'L882'	,'L881'	,'L877'		,'L861'	,'L862'		,'L841'	,'L842'	
		,'L843'	,'L844'	,'L845'	,'L846'	,'L847'	,'L848'	,'L849'
		,'L851'	,'L853'	,'L869'	,'L872'	,'L874'	,'L876'	,'L879'	,'L888'	
		,'L858'			,'L868'			,'L931'	,'L932'	,'L933'	,'L934'	
		,'L935'	,'L936'	,'L937'	,'L938'	,'L939'
		--PROMS Varicose Veins Surgery
		,'T191'	,'T201'	,'T211'	,'T221'	,'T231'	,'T251'	,'T261'
		,'T192'	,'T202'	,'T212'	,'T222'	,'T232'	,'T252'	,'T262'
		,'T193'	,'T203'	,'T213'	,'T223'	,'T233'	,'T253'	,'T263'
		,'T194'	,'T204'	,'T214'	,'T224'	,'T234'	,'T254'	,'T264'
		,'T195'	,'T205'	,'T215'	,'T225'	,'T235'	,'T255'	,'T265'
		,'T196'	,'T206'	,'T216'	,'T226'	,'T236'	,'T256'	,'T266'
		,'T197'	,'T207'	,'T217'	,'T227'	,'T237'	,'T257'	,'T267'
		,'T198'	,'T208'	,'T218'	,'T228'	,'T238'	,'T258'	,'T268'
		,'T199'	,'T209'	,'T219'	,'T229'	,'T239'	,'T259'	,'T269'
		--PROMS Groin Hernia Surgery
		)
		Then 'Yes'
		else 'No'
		end as 'PROMs'
      
      
      ,SP.[SPONT_REFNO_NAME] as AdmissionWard
      ,WL.[SiteCode]
      --,SPB.SPONT_REFNO_NAME as SecondaryWard
      --,WL.[WardCode]
      ,WL.[WLStatus]
      , isnull(WL.[LocalCategoryCode],'None') as CancerCode
      --,WL.[PurchaserCode]
      --,WL.[ProviderCode]
      --,WL.[ContractSerialNo]
      --,WL.[AdminCategoryCode]
      --,WL.[CancelledBy]
      --,WL.[BookingTypeCode]
      --,WL.[CasenoteNumber]
      --,WL.[OriginalDateOnWaitingList]
      ,WL.[DateOnWaitingList]
      ,WL.[TCIDate]
      ,CAST(WL.[TCIDate] as Date) as TCIDate_Date
      ,floor(cast((WL.[TCIDate]) as float)) as TCIKey
      ,WL.GeneralComment
      ,WL.PatientPreparation
      ,SPB.SPONT_REFNO_NAME as SecondaryWard
      --,WL.[KornerWait]
      --,WL.[CountOfDaysSuspended]
      --,WL.[SuspensionStartDate]
      --,WL.[SuspensionEndDate]
      --,WL.[SuspensionReasonCode]
      --,WL.[SuspensionReason]
      --,WL.[InterfaceCode]
      --,WL.[RegisteredGpCode]
      --,WL.[RegisteredGpPracticeCode]
      --,WL.[EpisodicGpCode]
      --,WL.[EpisodicGpPracticeCode]
      --,WL.[SourceTreatmentFunctionCode]
      --,WL.[TreatmentFunctionCode]
      --,WL.[NationalSpecialtyCode]
      --,WL.[PCTCode]
      --,WL.[BreachDate]
      --,WL.[ExpectedAdmissionDate]
      --,WL.[ReferralDate]
      --,WL.[FuturePatientCancelDate]
      --,WL.[FutureCancelDate]
      --,WL.[TheatrePatientBookingKey]
      --,WL.[ProcedureTime]
      --,WL.[TheatreCode]
      --,WL.[AdmissionReason]
      --,WL.[EpisodeNo]
      --,WL.[BreachDays]
      --,WL.[MRSAFlag]
      --,WL.[RTTPathwayID]
      --,WL.[RTTPathwayCondition]
      --,WL.[RTTStartDate]
      --,WL.[RTTEndDate]
      --,WL.[RTTSpecialtyCode]
      --,WL.[RTTCurrentProviderCode]
      --,WL.[RTTCurrentStatusCode]
      --,WL.[RTTCurrentStatusDate]
      --,WL.[RTTCurrentPrivatePatientFlag]
      --,WL.[RTTOverseasStatusFlag]
      --,WL.[NationalBreachDate]
      --,WL.[NationalBreachDays]
      --,WL.[DerivedBreachDays]
      --,WL.[DerivedClockStartDate]
      --,WL.[DerivedBreachDate]
      --,WL.[RTTBreachDate]
      --,WL.[RTTDiagnosticBreachDate]
      --,WL.[NationalDiagnosticBreachDate]
      --,WL.[SocialSuspensionDays]
      --,WL.[BreachTypeCode]
      --,WL.[PASCreated]
      --,WL.[PASUpdated]
      --,WL.[PASCreatedByWhom]
      --,WL.[PASUpdatedByWhom]
      --,WL.[Created]
      --,WL.[Updated]
      --,WL.[ByWhom]
      --,WL.[ExpectedDischargeDate]
      --,WL.[AnaestheticTypeCode]
      --,WL.[CancelReasonCode]
      
  FROM [WH].[APC].[WaitingList] WL
  
  Left Join [WH].[PAS].[Consultant] C
			on WL.[ConsultantCode] = C.ConsultantCode
			
		Left Join [WH].[PAS].[Specialty] S
				on WL.SpecialtyCode = S.SpecialtyCode
				
			Left Join [WH].[PAS].[AdmissionMethod] AM
					on WL.AdmissionMethodCode = AM.AdmissionMethodCode
				
				Left Join [WH].[PAS].[ManagementIntention] MI
						on WL.ManagementIntentionCode = MI.ManagementIntentionCode
						
						Left Join [WH].[PAS].[Priority] P
								on WL.PriorityCode = P.PriorityCode
								
								  left join [WH].[PAS].[ServicePointBase] SP
										on WL.SiteCode = SP.[SPONT_REFNO]
										and SP.[SPTYP_REFNO_DESCRIPTION] = 'Ward'
										
									  left join [WH].[PAS].[ServicePointBase] SPB
										on WL.WardCode = SPB.[SPONT_REFNO]
										and SPB.[SPTYP_REFNO_DESCRIPTION] = 'Ward'
										
  where 
  
  WL.CensusDate = (Select 
					MAX (WL.CensusDate)
						FROM [WH].[APC].[WL] WL) --Latest Census Date
  and 
  WL.TCIDate between
  CAST (getdate() as DATE)
	and
	DATEADD(dd,15,
	(CAST (getdate() as DATE))
	)											--TCI Date today or next 14 days
	
	and WL.CancelledBy is null					--excludes Cancelled Patients
	and LEFT(WL.NHSNumber,3) <> '999'			--excludes Test Patient
AND
	--following comes from Parameters

	(
	WL.SpecialtyCode in (SELECT Val from dbo.fn_String_To_Table(@Specialty,',',1)) 
	and
	WL.SiteCode in (SELECT Val from dbo.fn_String_To_Table(@Ward,',',1)) 
	and
	CASE 
                WHEN MI.NationalManagementIntentionCode= '3' THEN 1 
                ELSE MI.NationalManagementIntentionCode
                End 
                in (SELECT Val from dbo.fn_String_To_Table(@IntendedManagement,',',1)) 
    and
	CASE 
                WHEN AM.AdmissionMethodLocalCode in ('11','12') THEN 11
                WHEN AM.AdmissionMethodLocalCode = '13' THEN 13
                ELSE AM.AdmissionMethodLocalCode
                END 
                in (SELECT Val from dbo.fn_String_To_Table(@WLType,',',1))
    and isnull(WL.[LocalCategoryCode],'None') in (SELECT Val from dbo.fn_String_To_Table(@Canc,',',1))          
      	and
	WL.[SexCode] in (SELECT Val from dbo.fn_String_To_Table(@Gender,',',1))          
      and           
	floor(cast((WL.[TCIDate]) as float)) in (SELECT Val from dbo.fn_String_To_Table(@TCI,',',1)) 
	and
	CASE 
      WHEN [IntendedPrimaryOperationCode] IN
      (
       'W371', 'W378','W379', 'W381','W388', 'W389','W391', 'W398','W399'
      ,'W461', 'W468','W469', 'W471','W478', 'W479','W481', 'W488','W489'
      ,'W931', 'W938','W939', 'W941','W948', 'W949','W951', 'W958','W959'
      ,'W521', 'W528','W529', 'W531','W538', 'W539','W541', 'W548','W549'
      ,'W370', 'W372','W373', 'W374','W380', 'W382','W383', 'W384','W390'
      ,'W392', 'W393', 'W394','W395', 'W396','W460', 'W462','W463', 'W470'
      ,'W472', 'W473','W480', 'W482','W483', 'W484','W485','W930', 'W932'
      ,'W933', 'W940','W942', 'W943','W950', 'W952','W953', 'W954','W520'
      , 'W522','W523', 'W530','W532', 'W533','W540', 'W542','W543', 'W544'
      --PROMS Unilateral HIP Replacement
	,'W401'	,'W409'	,'W418'	,'W421'	,'W429'	,'W521'	,'W529'	,'W538'	,'W541'	
	,'W549'	,'O181'	,'O189'	,'W400'	,'W403'	,'W410'	,'W413'	,'W420'	,'W423'	
	,'W425'	,'W520'	,'W523'	,'W532'	,'W540'	,'W543'	,'O180'	,'O183'
	,'W408'	,'W411'	,'W419'	,'W428'	,'W528'	,'W531'	,'W539'	,'W548'	,'O188'
	,'W402'	,'W404'	,'W412'	,'W414'	,'W422'	,'W424'	,'W426'	,'W522'	,'W530'	
	,'W533'	,'W542'	,'W544'	,'O182'	,'O184'
	--PROMS Unilateral KNEE Replacement
		,'L852'	,'L859'	,'L871'	,'L873'	,'L875'	,'L878'	,'L883'	,'L889'	
		,'L882'	,'L881'	,'L877'		,'L861'	,'L862'		,'L841'	,'L842'	
		,'L843'	,'L844'	,'L845'	,'L846'	,'L847'	,'L848'	,'L849'
		,'L851'	,'L853'	,'L869'	,'L872'	,'L874'	,'L876'	,'L879'	,'L888'	
		,'L858'			,'L868'			,'L931'	,'L932'	,'L933'	,'L934'	
		,'L935'	,'L936'	,'L937'	,'L938'	,'L939'
		--PROMS Varicose Veins Surgery
		,'T191'	,'T201'	,'T211'	,'T221'	,'T231'	,'T251'	,'T261'
		,'T192'	,'T202'	,'T212'	,'T222'	,'T232'	,'T252'	,'T262'
		,'T193'	,'T203'	,'T213'	,'T223'	,'T233'	,'T253'	,'T263'
		,'T194'	,'T204'	,'T214'	,'T224'	,'T234'	,'T254'	,'T264'
		,'T195'	,'T205'	,'T215'	,'T225'	,'T235'	,'T255'	,'T265'
		,'T196'	,'T206'	,'T216'	,'T226'	,'T236'	,'T256'	,'T266'
		,'T197'	,'T207'	,'T217'	,'T227'	,'T237'	,'T257'	,'T267'
		,'T198'	,'T208'	,'T218'	,'T228'	,'T238'	,'T258'	,'T268'
		,'T199'	,'T209'	,'T219'	,'T229'	,'T239'	,'T259'	,'T269'
		--PROMS Groin Hernia Surgery
		)
		Then 'Yes'
		else 'No'
		end in  (SELECT Val from dbo.fn_String_To_Table(@PROM,',',1))
		
		and
		isnull(WL.[IntendedPrimaryOperationCode],'Uncoded') in  (SELECT Val from dbo.fn_String_To_Table(@PROC,',',1))

              	)