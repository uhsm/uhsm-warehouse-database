﻿/*
--Author: N Scattergood
--Date created: 12/09/2012
--Stored Procedure Built for the SRSS Report on Scheduled Admission
--This is used to pullout the Parameters and Default Values for Gender
*/

CREATE Procedure [SC].[ReportScheduledAdmissionsReport_Gender]
@Specialty	nvarchar (max)
,@Ward		nvarchar (max)
,@IntendedManagement nvarchar (max)
,@WLType	nvarchar (max)
,@Canc		nvarchar (max)

as

SELECT 
Distinct
      CASE 
      WHEN WL.[SexCode] = '9269' THEN 'Female'
      WHEN WL.[SexCode] = '9270' THEN 'Male'
      Else Null end as GenderDesc
      ,WL.[SexCode] as GenderPar

 FROM  [WH].[APC].[WaitingList] WL
 
 		Left Join [WH].[PAS].[ManagementIntention] MI
						on WL.ManagementIntentionCode = MI.ManagementIntentionCode
 
 			Left Join [WH].[PAS].[AdmissionMethod] AM
					on WL.AdmissionMethodCode = AM.AdmissionMethodCode

where 
  
  WL.CensusDate = (Select 
					MAX (WL.CensusDate)
						FROM [WH].[APC].[WL] WL) --Latest Census Date
  and 
  WL.TCIDate between
  CAST (getdate() as DATE)
	and
	DATEADD(dd,15,
	(CAST (getdate() as DATE))
	)											--TCI Date today or next 14 days
	
	and WL.CancelledBy is null					--excludes Cancelled Patients
	and LEFT(WL.NHSNumber,3) <> '999'
	
	--following comes from Parameters
and
	
	WL.SpecialtyCode in (SELECT Val from dbo.fn_String_To_Table(@Specialty,',',1)) 
	and
	WL.SiteCode in (SELECT Val from dbo.fn_String_To_Table(@Ward,',',1)) 
	and
	CASE 
                WHEN MI.NationalManagementIntentionCode= '3' THEN 1 
                ELSE MI.NationalManagementIntentionCode
                End 
                in (SELECT Val from dbo.fn_String_To_Table(@IntendedManagement,',',1)) 
      and
	CASE 
                WHEN AM.AdmissionMethodLocalCode in ('11','12') THEN 11
                WHEN AM.AdmissionMethodLocalCode = '13' THEN 13
                ELSE AM.AdmissionMethodLocalCode
                END 
                in (SELECT Val from dbo.fn_String_To_Table(@WLType,',',1)) 
               
      and isnull(WL.[LocalCategoryCode],'None') in (SELECT Val from dbo.fn_String_To_Table(@Canc,',',1))          


	order by
 CASE 
      WHEN WL.[SexCode] = '9269' THEN 'Female'
      WHEN WL.[SexCode] = '9270' THEN 'Male'
      Else Null end