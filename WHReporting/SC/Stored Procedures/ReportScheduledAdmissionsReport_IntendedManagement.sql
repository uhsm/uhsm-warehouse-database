﻿/*
--Author: N Scattergood
--Date created: 12/09/2012
--Stored Procedure Built for the SRSS Report on Scheduled Admission
--This is used to pullout the Default IntendedManagement
*/

CREATE Procedure [SC].[ReportScheduledAdmissionsReport_IntendedManagement]
@Specialty	nvarchar (max)
,@Ward		nvarchar (max)
as

SELECT 
Distinct
        CASE 
                WHEN MI.NationalManagementIntentionCode= '3' THEN 1 
                ELSE MI.NationalManagementIntentionCode
                End as IntendedManagementPar
      ,CASE 
                WHEN MI.NationalManagementIntentionCode = '1' THEN 'IP' 
                WHEN MI.NationalManagementIntentionCode= '2' THEN 'DC' 
                WHEN MI.NationalManagementIntentionCode= '3' THEN 'IP' 
                WHEN MI.NationalManagementIntentionCode= '8' THEN 'Not Applicable' 
                WHEN MI.NationalManagementIntentionCode= '9' THEN 'Not Known' 
                ELSE MI.[ManagementIntention]
                End as IntendedManagementDesc



  FROM  [WH].[APC].[WaitingList] WL
 
 		Left Join [WH].[PAS].[ManagementIntention] MI
						on WL.ManagementIntentionCode = MI.ManagementIntentionCode

where 
  
  WL.CensusDate = (Select 
					MAX (WL.CensusDate)
						FROM [WH].[APC].[WL] WL) --Latest Census Date
  and 
  WL.TCIDate between
  CAST (getdate() as DATE)
	and
	DATEADD(dd,15,
	(CAST (getdate() as DATE))
	)											--TCI Date today or next 14 days
	
	and WL.CancelledBy is null					--excludes Cancelled Patients
	and LEFT(WL.NHSNumber,3) <> '999'
	
	--following comes from Parameters
	and
	WL.SpecialtyCode in (SELECT Val from dbo.fn_String_To_Table(@Specialty,',',1)) 
	and
	WL.SiteCode in (SELECT Val from dbo.fn_String_To_Table(@Ward,',',1)) 
	
	order by
	CASE 
                WHEN MI.NationalManagementIntentionCode = '1' THEN 'IP' 
                WHEN MI.NationalManagementIntentionCode= '2' THEN 'DC' 
                WHEN MI.NationalManagementIntentionCode= '3' THEN 'IP' 
                WHEN MI.NationalManagementIntentionCode= '8' THEN 'Not Applicable' 
                WHEN MI.NationalManagementIntentionCode= '9' THEN 'Not Known' 
                ELSE MI.[ManagementIntention]
                End asc