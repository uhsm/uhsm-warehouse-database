﻿/*
--Author: N Scattergood
--Date created: 06/09/2012
--Stored Procedure Built for the SRSS Report on Scheuled Admission
--This is used to pullout the Default Specialties
*/

CREATE Procedure [SC].[ReportScheduledAdmissionsReport_Specialty]

as

SELECT 
Distinct
      WL.SpecialtyCode		as SpecPar
      ,S.LocalSpecialtyCode	as SpecCode
      ,S.Specialty			as SpecDesc

 FROM [WH].[APC].[WaitingList] WL
 
 		Left Join [WH].[PAS].[Specialty] S
				on WL.SpecialtyCode = S.SpecialtyCode

where 
  
  WL.CensusDate = (Select 
					MAX (WL.CensusDate)
						FROM [WH].[APC].[WL] WL) --Latest Census Date
  and 
  WL.TCIDate between
  CAST (getdate() as DATE)
	and
	DATEADD(dd,15,
	(CAST (getdate() as DATE))
	)											--TCI Date today or next 14 days
	
	and WL.CancelledBy is null					--excludes Cancelled Patients
	and LEFT(WL.NHSNumber,3) <> '999'
	


	
	order by
	S.Specialty asc