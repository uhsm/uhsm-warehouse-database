﻿/*
--Author: N Scattergood
--Date created: 11/09/2012
--Stored Procedure Built for the SRSS Report on Scheduled Admission
--This is used to pullout the Parameter and Default Values for Ward Names
*/

CREATE Procedure [SC].[ReportScheduledAdmissionsReport_Wards]
@Specialty nvarchar (max)

as
SELECT 
Distinct
      SP.[SPONT_REFNO_NAME] as WardDesc
      ,WL.[SiteCode]		as WardPar

 FROM  [WH].[APC].[WaitingList] WL
 
 	  left join [WH].[PAS].[ServicePointBase] SP
										on WL.SiteCode = SP.[SPONT_REFNO]
										and [SPTYP_REFNO_DESCRIPTION] = 'Ward'
where 
  
  WL.CensusDate = (Select 
					MAX (WL.CensusDate)
						FROM [WH].[APC].[WL] WL) --Latest Census Date
  and 
  WL.TCIDate between
  CAST (getdate() as DATE)
	and
	DATEADD(dd,15,
	(CAST (getdate() as DATE))
	)											--TCI Date today or next 14 days
	
	and WL.CancelledBy is null					--excludes Cancelled Patients
	and LEFT(WL.NHSNumber,3) <> '999'
	
	--following comes from Parameters
	and
	WL.SpecialtyCode in (SELECT Val from dbo.fn_String_To_Table(@Specialty,',',1)) 
	
	order by
  SP.[SPONT_REFNO_NAME] asc