﻿/*
--Author: N Scattergood
--Date created: 01/07/2016
--Stored Procedure Built for the SRSS Report on Scheduled Admission
--This is used to pullout the Parameter and Default Values for Ward Names
*/

CREATE Procedure [SC].[ReportScheduledAdmissionsReport_Wards_Secondary]
@Specialty nvarchar (max)

as


BEGIN
/*
Declare @Specialty NVARCHAR(max) = '2000777,2000778,2000780,2000784,2000785,2000786,2000788,2000799,2000800,2000803,2000804,2000808,2000811,2000812,2000817,2000827,2000830,2000834,2000844,2000845,2000853,2000865,2000961,10000247'

--*/
SELECT 
Distinct
      ISNULL(SPB.SPONT_REFNO_NAME, 'OTHER') as WardDesc
      ,ISNULL(WL.WardCode, '999')	as WardPar

 FROM  [WH].[APC].[WaitingList] WL
 
   LEFT JOIN [WH].[PAS].[ServicePointBase] SPB ON WL.WardCode = SPB.[SPONT_REFNO]
			AND SPB.[SPTYP_REFNO_DESCRIPTION] = 'Ward'
 
where 
   --WL.WardCode IS NOT NULL
    WL.CensusDate = (Select 
					MAX (WL.CensusDate)
						FROM [WH].[APC].[WL] WL) --Latest Census Date
  and 
  WL.TCIDate between
  CAST (getdate() as DATE)
	and
	DATEADD(dd,15,
	(CAST (getdate() as DATE))
	)											--TCI Date today or next 14 days
	
	and WL.CancelledBy is null					--excludes Cancelled Patients
	and LEFT(WL.NHSNumber,3) <> '999'
	
	--following comes from Parameters
	and
	WL.SpecialtyCode in (SELECT Val from dbo.fn_String_To_Table(@Specialty,',',1)) 
	
	order by ISNULL(SPB.SPONT_REFNO_NAME, 'OTHER') asc
  
  END