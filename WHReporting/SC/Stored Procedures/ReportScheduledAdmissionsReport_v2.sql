﻿/*
-----------------------------------------------------------------------------------------------------------------------
--   -----------------------------------
--   Purpose :    Scheduled Admissions Summary Report - data for SSRS Report on scheduled admissions
   
--    Notes  :    Stored in WHREPORTING.SC
           
--                This procedure has been created for use in the Scheduled admissions report in the location below
--                S:\CO-HI-Information\New File Management - Jan 2011\Scheduled Care\DataWarehouseReports\ScheduledAdmissions
                
--                This has been designed to populate data for scheduled admissions i.e. those patients on the IP WL scheduled to have an admission in the next 2 weeks
            
--  Versions :    1.0.0.1 - 01/04/2016 - Peter Asemota   
--                Taken out the nested  select statement in the Where clause as this can be achieved using a row number syntax and then partitioning by district number and TCIdata       
--                completely taken out fields that were excluded from the original script. By using the row number syntax we are getting more records back
                
--     level :    
--                  1.0.0.0  - 06/09/2012  - N Scattergood           
--                        Created Stored Procedure Built for the SRSS Report on Scheuled Admission
--                     i.e. Those patients on the IP WL Scheduled to have an Admission in the next 2 weeks
--                     Previous Issues addressed by  N Scattergood 
--                    1) Missing Cancer flag
 --                   2) There are Nulls in the WL.[Operation] field where the WL.[IntendedPrimaryOperationCode] is populated  
 -------------------------------------------------------------------------------------------------------------------------------- 
    ----------------------------------- 
   */
   
CREATE PROCEDURE [SC].[ReportScheduledAdmissionsReport_v2] 
(
 @Specialty NVARCHAR(max)
,@Ward NVARCHAR(max)
,@IntendedManagement AS NVARCHAR(max)
,@WLType NVARCHAR(max)
,@Canc NVARCHAR(max)
,@Gender NVARCHAR(max)
,@TCI NVARCHAR(max)
,@PROM NVARCHAR(max)
,@PROC NVARCHAR(max)

)

AS


BEGIN

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED



	/*  this is solely for testing pursoses
    Declare @Specialty NVARCHAR(max) = '3100997,2000827,2000780,2000803,2000830,2000812,2000786,2000811,2000777,2000849,2000854'                             
	Declare @Ward NVARCHAR(max)= '10009051,150003745,10012358,150008476,150003186,10009080,10009082,10009083,10009045,10009091,10012601,150012741,10009092,10009085,10009048,10009104'
	Declare @IntendedManagement AS NVARCHAR(max)= '1,2,3,8,9'
	Declare @WLType NVARCHAR(max)= '11,12,13'
	Declare @Canc NVARCHAR(max)= 'None,CD,CS'
	Declare @Gender NVARCHAR(max)='9269,9270'
	Declare @TCI NVARCHAR(max)= '42458,42459,42460,42461,42462,42463,42464,42465,42466,42467,42468,42469,42470,42471,42471'
	Declare @PROM NVARCHAR(max)= 'No,Yes'
	Declare @PROC NVARCHAR(max)= 'W371,W378,W379,W381,W388,W389,W391,W398,W399,A528,A484,B088,E361'
	*/


; WITH Censusdate_CTE As 
                           ( select    DistrictNo
                                   ,MAX(Censusdate) As Latest_Censusdate
		                              FROM [WH].[APC].[WaitingList] WL
		                              Group By DistrictNo
		                              )
	
	
		SELECT
			1 AS Data
			,WL.[CensusDate]
			,CDL.Latest_Censusdate as currentDate
			,WL.[DistrictNo] AS [RM2]
			,WL.[PatientForename]
			,WL.[PatientSurname]
			,CASE 
				WHEN WL.[SexCode] = '9269'
					THEN 'Female'
				WHEN WL.[SexCode] = '9270'
					THEN 'Male'
				ELSE NULL
				END AS [Gender]
			,WL.[SexCode] AS 'GenderKey'		
			,C.[Consultant]
			,WL.SpecialtyCode AS SpecialtyRef
			,S.LocalSpecialtyCode
			,S.Specialty
			,CASE 
				WHEN MI.NationalManagementIntentionCode = '3'
					THEN 1
				ELSE MI.NationalManagementIntentionCode
				END AS [IntendedManagementKey]
			,CASE 
				WHEN MI.NationalManagementIntentionCode = '1'
					THEN 'IP'
				WHEN MI.NationalManagementIntentionCode = '2'
					THEN 'DC'
				WHEN MI.NationalManagementIntentionCode = '3'
					THEN 'IP'
				WHEN MI.NationalManagementIntentionCode = '8'
					THEN 'Not Applicable'
				WHEN MI.NationalManagementIntentionCode = '9'
					THEN 'Not Known'
				ELSE MI.[ManagementIntention]
				END AS [IntendedManagement]
			,CASE 
				WHEN AM.AdmissionMethodLocalCode IN (
						'11'
						,'12'
						)
					THEN 11
				WHEN AM.AdmissionMethodLocalCode = '13'
					THEN 13
				ELSE AM.AdmissionMethodLocalCode
				END AS [WLTypeKey]
			,CASE 
				WHEN AM.AdmissionMethodLocalCode IN (
						'11'
						,'12'
						)
					THEN 'Waiting'
				WHEN AM.AdmissionMethodLocalCode = '13'
					THEN 'Planned'
				ELSE AM.AdmissionMethod
				END AS [WLType]
			,P.[Priority]
			,WL.[IntendedPrimaryOperationCode]
			,WL.[Operation]
			,CASE 
				WHEN [IntendedPrimaryOperationCode] IN (
						'W371'
						,'W378'
						,'W379'
						,'W381'
						,'W388'
						,'W389'
						,'W391'
						,'W398'
						,'W399'
						,'W461'
						,'W468'
						,'W469'
						,'W471'
						,'W478'
						,'W479'
						,'W481'
						,'W488'
						,'W489'
						,'W931'
						,'W938'
						,'W939'
						,'W941'
						,'W948'
						,'W949'
						,'W951'
						,'W958'
						,'W959'
						,'W521'
						,'W528'
						,'W529'
						,'W531'
						,'W538'
						,'W539'
						,'W541'
						,'W548'
						,'W549'
						,'W370'
						,'W372'
						,'W373'
						,'W374'
						,'W380'
						,'W382'
						,'W383'
						,'W384'
						,'W390'
						,'W392'
						,'W393'
						,'W394'
						,'W395'
						,'W396'
						,'W460'
						,'W462'
						,'W463'
						,'W470'
						,'W472'
						,'W473'
						,'W480'
						,'W482'
						,'W483'
						,'W484'
						,'W485'
						,'W930'
						,'W932'
						,'W933'
						,'W940'
						,'W942'
						,'W943'
						,'W950'
						,'W952'
						,'W953'
						,'W954'
						,'W520'
						,'W522'
						,'W523'
						,'W530'
						,'W532'
						,'W533'
						,'W540'
						,'W542'
						,'W543'
						,'W544'
						--PROMS Unilateral HIP Replacement
						,'W401'
						,'W409'
						,'W418'
						,'W421'
						,'W429'
						,'W521'
						,'W529'
						,'W538'
						,'W541'
						,'W549'
						,'O181'
						,'O189'
						,'W400'
						,'W403'
						,'W410'
						,'W413'
						,'W420'
						,'W423'
						,'W425'
						,'W520'
						,'W523'
						,'W532'
						,'W540'
						,'W543'
						,'O180'
						,'O183'
						,'W408'
						,'W411'
						,'W419'
						,'W428'
						,'W528'
						,'W531'
						,'W539'
						,'W548'
						,'O188'
						,'W402'
						,'W404'
						,'W412'
						,'W414'
						,'W422'
						,'W424'
						,'W426'
						,'W522'
						,'W530'
						,'W533'
						,'W542'
						,'W544'
						,'O182'
						,'O184'
						--PROMS Unilateral KNEE Replacement
						,'L852'
						,'L859'
						,'L871'
						,'L873'
						,'L875'
						,'L878'
						,'L883'
						,'L889'
						,'L882'
						,'L881'
						,'L877'
						,'L861'
						,'L862'
						,'L841'
						,'L842'
						,'L843'
						,'L844'
						,'L845'
						,'L846'
						,'L847'
						,'L848'
						,'L849'
						,'L851'
						,'L853'
						,'L869'
						,'L872'
						,'L874'
						,'L876'
						,'L879'
						,'L888'
						,'L858'
						,'L868'
						,'L931'
						,'L932'
						,'L933'
						,'L934'
						,'L935'
						,'L936'
						,'L937'
						,'L938'
						,'L939'
						--PROMS Varicose Veins Surgery
						,'T191'
						,'T201'
						,'T211'
						,'T221'
						,'T231'
						,'T251'
						,'T261'
						,'T192'
						,'T202'
						,'T212'
						,'T222'
						,'T232'
						,'T252'
						,'T262'
						,'T193'
						,'T203'
						,'T213'
						,'T223'
						,'T233'
						,'T253'
						,'T263'
						,'T194'
						,'T204'
						,'T214'
						,'T224'
						,'T234'
						,'T254'
						,'T264'
						,'T195'
						,'T205'
						,'T215'
						,'T225'
						,'T235'
						,'T255'
						,'T265'
						,'T196'
						,'T206'
						,'T216'
						,'T226'
						,'T236'
						,'T256'
						,'T266'
						,'T197'
						,'T207'
						,'T217'
						,'T227'
						,'T237'
						,'T257'
						,'T267'
						,'T198'
						,'T208'
						,'T218'
						,'T228'
						,'T238'
						,'T258'
						,'T268'
						,'T199'
						,'T209'
						,'T219'
						,'T229'
						,'T239'
						,'T259'
						,'T269'
						--PROMS Groin Hernia Surgery
						)
					THEN 'Yes'
				ELSE 'No'
				END AS [PROMs]
			,SP.[SPONT_REFNO_NAME] AS AdmissionWard
			,WL.[SiteCode]
			,ISNULL(WL.WardCode, '999') As [WardCode]
			,WL.[WLStatus]
			,isnull(WL.[LocalCategoryCode], 'None') AS CancerCode
			,WL.[DateOnWaitingList]
			,WL.[TCIDate]
			,CAST(WL.[TCIDate] AS DATE) AS TCIDate_Date
			,floor(cast((WL.[TCIDate]) AS FLOAT)) AS TCIKey
			,WL.GeneralComment
			,WL.PatientPreparation
			,ISNULL(SPB.SPONT_REFNO_NAME, 'OTHER') AS SecondaryWard
			--,Coalesce(SPB.SPONT_REFNO_NAME, SP.[SPONT_REFNO_NAME]) AS Pri_Secondary_Ward 
		   ,Case When SP.[SPONT_REFNO_NAME]= 'ADMISSIONS LOUNGE'  Then Coalesce(SPB.SPONT_REFNO_NAME, SP.[SPONT_REFNO_NAME])
                  ELSE SP.[SPONT_REFNO_NAME]
                  -- When (SP.[SPONT_REFNO_NAME]<> 'ADMISSIONS LOUNGE') Then SP.[SPONT_REFNO_NAME]
                        END AS Primary_Secondary_Ward

			,ROW_NUMBER() OVER (
				PARTITION BY WL.[DistrictNo]
				,WL.TCIDate ORDER BY WL.CensusDate DESC
				) AS Row
			,CASE 
				WHEN SPB.SPONT_REFNO_NAME IS NOT NULL
					THEN 1
				ELSE 0
				END AS [SecondaryWard_counts] --added by PA to include secondary ward counts
			,CASE 
				WHEN SP.[SPONT_REFNO_NAME] IS NOT NULL
					THEN 1
				ELSE 0
				END AS [AdmissionWard_counts]  --added by PA to include Admission ward counts

		FROM [WH].[APC].[WaitingList] WL
		LEFT JOIN [WH].[PAS].[Consultant] C ON WL.[ConsultantCode] = C.ConsultantCode
		LEFT JOIN [WH].[PAS].[Specialty] S ON WL.SpecialtyCode = S.SpecialtyCode
		LEFT JOIN [WH].[PAS].[AdmissionMethod] AM ON WL.AdmissionMethodCode = AM.AdmissionMethodCode
		LEFT JOIN [WH].[PAS].[ManagementIntention] MI ON WL.ManagementIntentionCode = MI.ManagementIntentionCode
		LEFT JOIN [WH].[PAS].[Priority] P ON WL.PriorityCode = P.PriorityCode
		LEFT JOIN [WH].[PAS].[ServicePointBase] SP ON WL.SiteCode = SP.[SPONT_REFNO]
			AND SP.[SPTYP_REFNO_DESCRIPTION] = 'Ward'
		LEFT JOIN [WH].[PAS].[ServicePointBase] SPB ON WL.WardCode = SPB.[SPONT_REFNO]
			AND SPB.[SPTYP_REFNO_DESCRIPTION] = 'Ward'
		Inner join Censusdate_CTE  CDL ON WL.DistrictNo = CDL.DistrictNo 
		         and WL.CensusDate = CDL.Latest_Censusdate
		WHERE
			  WL.TCIDate BETWEEN CAST(getdate() AS DATE) --TCI Date today or next 14 days
				  AND DATEADD(dd, 15, (CAST(getdate() AS DATE)))
				  AND WL.CancelledBy IS NULL --excludes Cancelled Patients
			      AND LEFT(WL.NHSNumber, 3) <> '999' --excludes Test Patient
			

		AND
	
		  (
		 WL.SpecialtyCode IN (
			SELECT Val
                         FROM  dbo.fn_String_To_Table (@Specialty, ',',1)
			)
			
			
			
		AND ISNULL(WL.WardCode, '999') IN (
			SELECT Val
			            FROM dbo.fn_String_To_Table(@Ward, ',',1)
	
			)
		AND CASE 
			WHEN MI.NationalManagementIntentionCode = '3'
				THEN 1
			ELSE MI.NationalManagementIntentionCode
			END IN (
			       SELECT Val
			            FROM dbo.fn_String_To_Table(@IntendedManagement, ',', 1)
			        )
			       -- )
		AND CASE 
			WHEN AM.AdmissionMethodLocalCode IN (
					'11'
					,'12'
					)
				THEN 11
			WHEN AM.AdmissionMethodLocalCode = '13'
				THEN 13
			ELSE AM.AdmissionMethodLocalCode
			END IN (
			SELECT Val
			FROM dbo.fn_String_To_Table(@WLType, ',', 1)
			)
		
		AND isnull(WL.[LocalCategoryCode], 'None') IN (
			SELECT Val
			FROM dbo.fn_String_To_Table(@Canc, ',', 1)
			)
			
		AND WL.[SexCode] IN (
			SELECT Val
			FROM dbo.fn_String_To_Table(@Gender, ',', 1)
			)
		AND floor(cast((WL.[TCIDate]) AS FLOAT)) IN (
			SELECT Val
			FROM dbo.fn_String_To_Table(@TCI, ',', 1)
			)
			
		AND CASE 
			WHEN [IntendedPrimaryOperationCode] IN (
					'W371'
					,'W378'
					,'W379'
					,'W381'
					,'W388'
					,'W389'
					,'W391'
					,'W398'
					,'W399'
					,'W461'
					,'W468'
					,'W469'
					,'W471'
					,'W478'
					,'W479'
					,'W481'
					,'W488'
					,'W489'
					,'W931'
					,'W938'
					,'W939'
					,'W941'
					,'W948'
					,'W949'
					,'W951'
					,'W958'
					,'W959'
					,'W521'
					,'W528'
					,'W529'
					,'W531'
					,'W538'
					,'W539'
					,'W541'
					,'W548'
					,'W549'
					,'W370'
					,'W372'
					,'W373'
					,'W374'
					,'W380'
					,'W382'
					,'W383'
					,'W384'
					,'W390'
					,'W392'
					,'W393'
					,'W394'
					,'W395'
					,'W396'
					,'W460'
					,'W462'
					,'W463'
					,'W470'
					,'W472'
					,'W473'
					,'W480'
					,'W482'
					,'W483'
					,'W484'
					,'W485'
					,'W930'
					,'W932'
					,'W933'
					,'W940'
					,'W942'
					,'W943'
					,'W950'
					,'W952'
					,'W953'
					,'W954'
					,'W520'
					,'W522'
					,'W523'
					,'W530'
					,'W532'
					,'W533'
					,'W540'
					,'W542'
					,'W543'
					,'W544'
					--PROMS Unilateral HIP Replacement
					,'W401'
					,'W409'
					,'W418'
					,'W421'
					,'W429'
					,'W521'
					,'W529'
					,'W538'
					,'W541'
					,'W549'
					,'O181'
					,'O189'
					,'W400'
					,'W403'
					,'W410'
					,'W413'
					,'W420'
					,'W423'
					,'W425'
					,'W520'
					,'W523'
					,'W532'
					,'W540'
					,'W543'
					,'O180'
					,'O183'
					,'W408'
					,'W411'
					,'W419'
					,'W428'
					,'W528'
					,'W531'
					,'W539'
					,'W548'
					,'O188'
					,'W402'
					,'W404'
					,'W412'
					,'W414'
					,'W422'
					,'W424'
					,'W426'
					,'W522'
					,'W530'
					,'W533'
					,'W542'
					,'W544'
					,'O182'
					,'O184'
					--PROMS Unilateral KNEE Replacement
					,'L852'
					,'L859'
					,'L871'
					,'L873'
					,'L875'
					,'L878'
					,'L883'
					,'L889'
					,'L882'
					,'L881'
					,'L877'
					,'L861'
					,'L862'
					,'L841'
					,'L842'
					,'L843'
					,'L844'
					,'L845'
					,'L846'
					,'L847'
					,'L848'
					,'L849'
					,'L851'
					,'L853'
					,'L869'
					,'L872'
					,'L874'
					,'L876'
					,'L879'
					,'L888'
					,'L858'
					,'L868'
					,'L931'
					,'L932'
					,'L933'
					,'L934'
					,'L935'
					,'L936'
					,'L937'
					,'L938'
					,'L939'
					--PROMS Varicose Veins Surgery
					,'T191'
					,'T201'
					,'T211'
					,'T221'
					,'T231'
					,'T251'
					,'T261'
					,'T192'
					,'T202'
					,'T212'
					,'T222'
					,'T232'
					,'T252'
					,'T262'
					,'T193'
					,'T203'
					,'T213'
					,'T223'
					,'T233'
					,'T253'
					,'T263'
					,'T194'
					,'T204'
					,'T214'
					,'T224'
					,'T234'
					,'T254'
					,'T264'
					,'T195'
					,'T205'
					,'T215'
					,'T225'
					,'T235'
					,'T255'
					,'T265'
					,'T196'
					,'T206'
					,'T216'
					,'T226'
					,'T236'
					,'T256'
					,'T266'
					,'T197'
					,'T207'
					,'T217'
					,'T227'
					,'T237'
					,'T257'
					,'T267'
					,'T198'
					,'T208'
					,'T218'
					,'T228'
					,'T238'
					,'T258'
					,'T268'
					,'T199'
					,'T209'
					,'T219'
					,'T229'
					,'T239'
					,'T259'
					,'T269'
					--PROMS Groin Hernia Surgery
					)
				THEN 'Yes'
			ELSE 'No'
			END IN (
			SELECT Val
			FROM dbo.fn_String_To_Table(@PROM, ',', 1)
			)
			
		AND isnull(WL.[IntendedPrimaryOperationCode], 'Uncoded') IN (
			SELECT Val
			FROM dbo.fn_String_To_Table(@PROC, ',', 1)
			)
		)
	
		order by WL.[DistrictNo]
END