﻿/*
--Author: N Scattergood
--Date created: 12/12/2012
--Stored Procedure Built for pulling out details of rolling 13 months
--Currently used for:
> SRSS Report on PostOpLosOnCriticalCare
*/

CREATE Procedure [SC].[Rolling13Months_DefaultDates]
as

 
 select 
 distinct
  
 min(Cal.TheDate) as FirstDayOfRolling13Month
 ,max(Cal.TheDate) LastDayofRolling13Month
 ,dbo.GetEndOfDay (max(Cal.TheDate)) LastDateTimeofRolling13Month
 --,Cal.TheMonth as Rolling13Month
 --,Cal.FinancialYear as FYLastMonth

 FROM LK.Calendar Cal
 
 where
Cal.TheMonth in 
(select distinct
SQ.TheMonth
  FROM LK.Calendar SQ
  where SQ.TheDate between 
 (
dateadd(MM,-13,cast(GETDATE()as DATE))
)
and
(
dateadd(MM,-1,cast(GETDATE()as DATE))
)

)