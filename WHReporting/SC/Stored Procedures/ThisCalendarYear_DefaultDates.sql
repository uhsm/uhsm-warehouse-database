﻿/*
--Author: N Scattergood
--Date created: 25/03/2013
--Stored Procedure Built for pulling out details of Current Year
--Includes Same Dates but for previous year for comparison reports
--Currently used for:
--Bed Modelling
*/

CREATE Procedure [SC].[ThisCalendarYear_DefaultDates]
as
 
 select 
 distinct
  
 min(Cal.TheDate) as FirstDayOfThisYear
 ,max(Cal.TheDate) LastDayofThisYear
 ,dbo.GetEndOfDay (max(Cal.TheDate)) LastDateTimeofThisYear
 ,DATEADD(year,-1,(min(Cal.TheDate))) as FirstDayOfLASTYear
  ,DATEADD(year,-1,(max(Cal.TheDate))) as LastDayOfLASTYear
  ,DATEADD(year,-1,(dbo.GetEndOfDay (max(Cal.TheDate)))) as LastDateTimeOfLASTYear

 FROM LK.Calendar Cal
 
 where
Cal.CalendarYear = 
(select distinct
SQ.CalendarYear
  FROM LK.Calendar SQ
  where SQ.TheDate = 
 (
dateadd(day,-1,cast(GETDATE()as DATE))
)
) and Cal.TheDate <=dateadd(day,-1,cast(GETDATE()as DATE))

-- group by
--Cal.CalendarYear