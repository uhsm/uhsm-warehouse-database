﻿/*
--Author: N Scattergood
--Date created: 25/03/2013
--Stored Procedure Built for pulling out details of last month
--Currently used for:
> TheatreSessionSummaryReportByConsultant

*/

Create Procedure [SC].[ThisMonth_DefaultDates]
as

 
 select 
 distinct
  
 min(Cal.TheDate) as FirstDayOfThisMonth
 ,max(Cal.TheDate) LastDayofThisMonth
 ,dbo.GetEndOfDay (max(Cal.TheDate)) LastDateTimeofThisMonth
 ,Cal.TheMonth as ThisMonth
 ,Cal.FinancialYear as FYThisMonth

 FROM LK.Calendar Cal
 
 where
Cal.TheMonth = 
(select distinct
SQ.TheMonth
  FROM LK.Calendar SQ
  where SQ.TheDate = 
 (
dateadd(day,-1,cast(GETDATE()as DATE))
)
) and Cal.TheDate <=dateadd(day,-1,cast(GETDATE()as DATE))

 group by
Cal.TheMonth
,Cal.FinancialYear