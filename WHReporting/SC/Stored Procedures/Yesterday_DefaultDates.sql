﻿/*
--Author: N Scattergood
--Date created: 26/06/2013
--Stored Procedure Built for pulling out details of yesterday
--Currently used for:
> Bed Modelling By Time Of Day
*/

Create Procedure [SC].[Yesterday_DefaultDates]
as

 
 select 
 
 WHREPORTING.dbo.GetStartOfDay(CAST((dateadd(day,-1,GETDATE())) as DATE)) as StartOfDay 
 ,WHREPORTING.dbo.GetEndOfDay(CAST((dateadd(day,-1,GETDATE())) as DATE)) as EndOfDay