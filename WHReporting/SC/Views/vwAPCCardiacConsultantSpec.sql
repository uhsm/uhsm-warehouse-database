﻿--USE [WHREPORTING]
--GO

--/****** Object:  View [SC].[APCSpecialtyFunctionCorrection]    Script Date: 12/12/2012 15:56:03 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

/*
--Author: N Scattergood
--Date created: 12/12/2012
--View Created to correct issue arounding inconsistent recording of Cardiac Consultants
Join to this View using Consultant Code for Cardiac Queries
*/

CREATE View  [SC].[vwAPCCardiacConsultantSpec]

as

Select

Distinct
      
      --AdmissionSpecialtyCode,
      S.AdmissionConsultantCode				as CardiacConsCode
      ,S.AdmissionConsultantName			as CardiacConsName
      ,case 
when  S.AdmissionConsultantCode	 IN  
('C4325150','C4690287','C4730271')
then 102
when  S.AdmissionConsultantCode	 IN
('C4445638')
then 170
when   S.AdmissionConsultantCode	 IN  
('C3286898','C3166114','C3631845','C3226274','C2503756','C3451180'
  ,'C4686551','C4683551','C4486266','C4511416','C4509060')
then 172
when   S.AdmissionConsultantCode	 IN  
('C2386780','C4346052','C4002776','C1738489','C4780001','C4109680')
then 173
when   S.AdmissionConsultantCode	 IN
('C4188434','C4634423')
then 320

ELSE [AdmissionSpecialtyCode(Function)]
end 										as CardiacSpecCode		
      
      -----FOLLOWING CASE MUST BE IDENTICAL FOR CONSULTANTS AS ABOVE---
      ,case 
when  S.AdmissionConsultantCode	 IN  
('C4325150','C4690287','C4730271')
then 'Transplantation Surgery'
when  S.AdmissionConsultantCode	 IN
('C4445638')
then 'Cardiothoracic Surgery'
when   S.AdmissionConsultantCode	 IN  
('C3286898','C3166114','C3631845','C3226274','C2503756','C3451180'
  ,'C4686551','C4683551','C4486266','C4511416','C4509060')
then 'Cardiac Surgery'
when   S.AdmissionConsultantCode	 IN  
('C2386780','C4346052','C4002776','C1738489','C4780001','C4445638','C4109680')
then 'Thoracic Surgery'
when   S.AdmissionConsultantCode	 IN
('C4188434','C4634423')
then 'Cardiology'

ELSE [AdmissionSpecialty(Function)]
end 										as CardiacSpecDesc

     
  FROM [WHREPORTING].[APC].[Spell] S
  


WHERE
	S.DischargeDateTime between
  	  ----Sub Query for Rolling 13 Months---- 
	( select  distinct min(Cal.TheDate)
		FROM LK.Calendar Cal
			where
			Cal.TheMonth = 
			(select distinct
			SQ.TheMonth
			  FROM LK.Calendar SQ
			  where SQ.TheDate = 
			 (
			dateadd(MM,-13,cast(GETDATE()as DATE))
			)))
	 and 
	( select distinct
	dbo.GetEndOfDay (max(Cal.TheDate)) 
	FROM LK.Calendar Cal
		where
			Cal.TheMonth = 
			(select distinct
			SQ.TheMonth
			  FROM LK.Calendar SQ
			  where SQ.TheDate = 
			(
			dateadd(MM,-1,cast(GETDATE()as DATE))
			)))	 
	  ----End of Query for Rolling 13 Months---- 
and
S.AdmissionDirectorate
= 'Cardiothoracic'