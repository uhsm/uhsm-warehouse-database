﻿/*
--Author: N Scattergood
--Date created: 26/11/2012
--View Created to correct for issue arounding Specialty Function Codes having multiple Descriptions
--This includes:
103A	Thyroid
110E	ORTH-FRACTURE CLINIC
120N	ENT-OPEN-ACCESS
320S	Cardiology
320H	Cardiology
340G	CYSTIC FIBROSIS
350A	Aspergillosis
501B	OBSTETRICS

Join to this View using [AdmissionSpecialtyCode(Function)] 
*/

CREATE View  [SC].[vwAPCSpecialtyFunctionCorrection]

as

Select

Distinct
      
      --AdmissionSpecialtyCode,
      [AdmissionSpecialtyCode(Function)] as SpecCode
      ,[AdmissionSpecialty(Function)] as SpecDescription
      	
      
     
  FROM [WHREPORTING].[APC].[Spell] 



WHERE
ISNUMERIC ([AdmissionSpecialtyCode]) = 1	

	--ORDER BY AppointmentDateTime DESC