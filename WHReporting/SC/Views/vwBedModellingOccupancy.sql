﻿--USE [WHREPORTING]
--GO

--/****** Object:  View [SC].[APCSpecialtyFunctionCorrection]    Script Date: 12/12/2012 15:56:03 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

/*
--Author: N Scattergood
--Date created: 01/07/2013
--Used for Bed Modelling Report
*/

CREATE VIEW [SC].[vwBedModellingOccupancy]

as

SELECT 

DT.[TheDate]
      ,DT.[DateAndHour]
      ,DT.[HOURTIME]
      ,DT.[HOURINT]
      ,DT.[DayOfWeek]
      ,DT.DayOfWeekKey
      ,LEFT(DT.LongDate,6) as DateDDMMM
    ,DT.CalendarYear
    ,DT.TheMonth,
S.AdmissiontWardCode as AdmissionWardCode,
SPB.AdmissionLocation,
case
	when S.[AdmissionType] in ('Emergency','Non-Elective','Maternity') 
	then 'Non-Elective'
	when S.[InpatientStayCode] = 'D'	
	then 'Elective - DayCase'
	when  S.[InpatientStayCode] = 'I'	
	then 'Elective - Inpatient'
	else 'Check SQL'
	end as 'PatClassDesc',
	case
	when S.[AdmissionType] in ('Emergency','Non-Elective','Maternity') 
	then 1
	when S.[InpatientStayCode] = 'D'	
	then 2
	when  S.[InpatientStayCode] = 'I'	
	then 3
	else 9
	end as 'PatClassCode'
--,   S.[InpatientStayCode]
--,   S.[AdmissionType]
 --     ,case when S.[InpatientStayCode] = 'D'	then 'DayCase'
	--		when  S.[InpatientStayCode] = 'I'	then 'Inpatient'
	--		when S.[InpatientStayCode] = 'B'	then 'Baby'
	--		else [InpatientStayCode] 
	--		end as InpatientStay
	--,case 
	--when S.[AdmissionType] in ('Emergency','Non-Elective') 
	--then 'Non-Elective'
	--else S.[AdmissionType] 
	--end as 'AdmissionType' 
      ,E.[SourceSpellNo]
      ,E.[EpisodeUniqueID]
      ,E.[FacilityID]
      ,E.[NHSNo]
      ,E.[AdmissionDateTime]
      ,E.[DischargeDateTime]
      ,E.[SpecialtyCode(Function)]
      ,E.[Specialty(Function)]
      ,E.[EpisodeStartDateTime]
      ,E.[EpisodeEndDateTime]
      
          FROM [WHREPORTING].[APC].[Episode] E
        
        inner join [WHREPORTING].[LK].[vwDateTime] DT
        on E.EpisodeStartDateTime <= DT.DateAndHour	
        and coalesce(E.EpisodeEndDateTime,E.DischargeDateTime,
        (Select 
WHREPORTING.dbo.GetEndOfDay(CAST((dateadd(day,-1,GETDATE())) as DATE))))
			> DT.DateAndHour	
        
				left join  [WHREPORTING].[APC].[Spell] S
				on S.SourceSpellNo = E.SourceSpellNo
				
				
				
					left join	
				  (	
				  select distinct
				SPB.SPONT_REFNO_CODE
				,[SPECT_REFNO_MAIN_IDENT] as AdmissionLocation
				from WH.PAS.ServicePointBase SPB
				where
				SPB.[SPTYP_REFNO_MAIN_CODE] = 'WARD'
				  )	 SPB
				  on S.AdmissiontWardCode = SPB.SPONT_REFNO_CODE
				  
	Where
	S.[InpatientStayCode] <> 'B' ---excludes Baby----
	and 
	DT.[TheDate] between
	--E.EpisodeStartDate between 
	(
Select
Min(Cal.TheDate)

 FROM [WHREPORTING].[LK].[Calendar] Cal
  right join
 ( select
CY.CalendarYear
 FROM [WHREPORTING].[LK].[Calendar] CY
 where CY.TheDate = DATEADD(year,-1,cast(getdate() as date))
 )SQ
 on SQ.CalendarYear = Cal.CalendarYear
)
and
(
Select 
WHREPORTING.dbo.GetEndOfDay(CAST((dateadd(day,-1,GETDATE())) as DATE))
)