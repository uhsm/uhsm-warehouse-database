﻿/*
--Author: N Scattergood
--Date created: 20/11/2012
--View Built for the SRSS Report on DNAs in Last Seven Days
*/

CREATE View [SC].[vwDNAsLast7Days] as 

Select

[FacilityID]
      ,[NHSNo]
      ,[AgeAtAppointment]
      ,[ClinicCode]
      ,[Clinic]
      ,[AppointmentDate]
      ,[AppointmentTime]
      ,[AppointmentDateTime]
      --,[AppointmentTypeCode]
      ,[AppointmentType]
      ,[AppointmentTypeNHSCode]
      ,[ProfessionalCarerType]
      ,[ProfessionalCarerCode]
      ,[ProfessionalCarerName]
      ,[Division]
      ,[Directorate]
      ,[SpecialtyCode]
      ,[Specialty]
      ,[SpecialtyCode(Function)]
      ,[Specialty(Function)]
      --,[SpecialtyCode(Main)]
      --,[Specialty(Main)]
      ,[AttendStatusCode]
      ,[AttendStatus]
      ,[AttendStatusNHSCode]
      ,[Status]
      ,[OutcomeCode]
      ,[Outcome]
      ,[OutcomeNHSCode]
      --,[AppointmentGPCode]
      ,[AppointmentGP]
      --,[AppointmentPracticeCode]
      ,[AppointmentPractice]
      ,[AppointmentPCTCode]
      ,[AppointmentPCT]
      --,[PostCode]
      --,[ResidencePCTCode]
      --,[ResidencePCT]
      --,[PurchaserCode]
      --,[Purchaser]

      ,[ExcludedClinic]
      ,[IsWalkIn]

FROM [WHREPORTING].[OP].[Schedule] OP

		LEFT OUTER JOIN dbo.vwExcludeWAWardsandClinics Exc
		ON OP.ClinicCode = Exc.SPONT_REFNO_CODE

WHERE


AppointmentDateTime BETWEEN 
--'01 APR 2012' AND '31 OCT 2012'
DATEADD(DD,-7,(CAST(GETDATE() AS DATE))) AND CAST(GETDATE() AS DATE)
AND
IsWardAttender = 0
AND
OP.ExcludedClinic = '0'
AND  
Exc.SPONT_REFNO_CODE IS NULL
AND
(
AttendStatus IN 
	(
	'Refused to Wait',
	'Patient Left / Not seen',
	'Patient Late / Not Seen',
	'Did Not Attend'
	) 
	OR 
	(
	AttendStatus IN 
	(
	'Attended',
	'Attended on Time',
	'Patient Late / Seen'
	)
	AND Outcome = 'Did Not Attend'
	)
	)
	
	
AND 
(OP.AppointmentTypeNHSCode IN (1, 2) OR OP.AppointmentTypeNHSCode IS NULL)
AND 
AdministrativeCategory <> 'Private Patient'
AND
[SpecialtyCode(Function)]  NOT IN ('Unk','DQ')


	
	--ORDER BY AppointmentDateTime DESC