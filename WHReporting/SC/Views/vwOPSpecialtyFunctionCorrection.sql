﻿/*
--Author: N Scattergood
--Date created: 26/11/2012
--View Created to correct for issue arounding Specialty Function Codes having multiple Descriptions
--This includes:
110E	ORTH-FRACTURE CLINIC
120N	ENT-OPEN-ACCESS
340G	CYSTIC FIBROSIS
350A	Aspergillosis
501B	OBSTETRICS
Join to this View using SpecialtyFunctionCode
*/

CREATE View  [SC].[vwOPSpecialtyFunctionCorrection]

as

Select

Distinct
      [SpecialtyCode(Function)] as SpecCode
      ,[Specialty(Function)]		as SpecDescription
      
     
  FROM [WHREPORTING].[OP].[Schedule] OP



WHERE
ISNUMERIC (OP.SpecialtyCode) = 1	

	--ORDER BY AppointmentDateTime DESC