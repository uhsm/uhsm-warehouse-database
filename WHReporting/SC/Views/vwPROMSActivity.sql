﻿/*
--Author: N Scattergood
--Date created: 12/11/2012
--View Built for the SRSS Report on PROMS Activity
*/

CREATE View [SC].[vwPROMSActivity] as 

SELECT 

[EncounterRecno]
,P.ProcedureDate
,P.EpisodeSourceUniqueID
,P.ProcedureCode 

,case 
when P.ProcedureCode in
(
       'W371', 'W378','W379', 'W381','W388', 'W389','W391', 'W398','W399'
      ,'W461', 'W468','W469', 'W471','W478', 'W479','W481', 'W488','W489'
      ,'W931', 'W938','W939', 'W941','W948', 'W949','W951', 'W958','W959'
      ,'W521', 'W528','W529', 'W531','W538', 'W539','W541', 'W548','W549'
      ,'W370', 'W372','W373', 'W374','W380', 'W382','W383', 'W384','W390'
      ,'W392', 'W393', 'W394','W395', 'W396','W460', 'W462','W463', 'W470'
      ,'W472', 'W473','W480', 'W482','W483', 'W484','W485','W930', 'W932'
      ,'W933', 'W940','W942', 'W943','W950', 'W952','W953', 'W954','W520'
      , 'W522','W523', 'W530','W532', 'W533','W540', 'W542','W543', 'W544'
 )
 then 'Unilateral HIP Replacement'
 when P.ProcedureCode in
 (
	'W401'	,'W409'	,'W418'	,'W421'	,'W429'	,'W521'	,'W529'	,'W538'	,'W541'	
	,'W549'	,'O181'	,'O189'	,'W400'	,'W403'	,'W410'	,'W413'	,'W420'	,'W423'	
	,'W425'	,'W520'	,'W523'	,'W532'	,'W540'	,'W543'	,'O180'	,'O183'
	,'W408'	,'W411'	,'W419'	,'W428'	,'W528'	,'W531'	,'W539'	,'W548'	,'O188'
	,'W402'	,'W404'	,'W412'	,'W414'	,'W422'	,'W424'	,'W426'	,'W522'	,'W530'	
	,'W533'	,'W542'	,'W544'	,'O182'	,'O184'
)
then 'Unilateral KNEE Replacement'
when  P.ProcedureCode in
(
		'L852'	,'L859'	,'L871'	,'L873'	,'L875'	,'L878'	,'L883'	,'L889'	
		,'L882'	,'L881'	,'L877'		,'L861'	,'L862'		,'L841'	,'L842'	
		,'L843'	,'L844'	,'L845'	,'L846'	,'L847'	,'L848'	,'L849'
		,'L851'	,'L853'	,'L869'	,'L872'	,'L874'	,'L876'	,'L879'	,'L888'	
		,'L858'			,'L868'			,'L931'	,'L932'	,'L933'	,'L934'	
		,'L935'	,'L936'	,'L937'	,'L938'	,'L939'
)
then 'Varicose Veins Surgery'
when  P.ProcedureCode in
(
		'T191'	,'T201'	,'T211'	,'T221'	,'T231'	,'T251'	,'T261'
		,'T192'	,'T202'	,'T212'	,'T222'	,'T232'	,'T252'	,'T262'
		,'T193'	,'T203'	,'T213'	,'T223'	,'T233'	,'T253'	,'T263'
		,'T194'	,'T204'	,'T214'	,'T224'	,'T234'	,'T254'	,'T264'
		,'T195'	,'T205'	,'T215'	,'T225'	,'T235'	,'T255'	,'T265'
		,'T196'	,'T206'	,'T216'	,'T226'	,'T236'	,'T256'	,'T266'
		,'T197'	,'T207'	,'T217'	,'T227'	,'T237'	,'T257'	,'T267'
		,'T198'	,'T208'	,'T218'	,'T228'	,'T238'	,'T258'	,'T268'
		,'T199'	,'T209'	,'T219'	,'T229'	,'T239'	,'T259'	,'T269'
)
then 'Groin Hernia Surgery'
else 'checkSQL'
end as PROMSProc
,case 
when P.ProcedureCode in
(
       'W371', 'W378','W379', 'W381','W388', 'W389','W391', 'W398','W399'
      ,'W461', 'W468','W469', 'W471','W478', 'W479','W481', 'W488','W489'
      ,'W931', 'W938','W939', 'W941','W948', 'W949','W951', 'W958','W959'
      ,'W521', 'W528','W529', 'W531','W538', 'W539','W541', 'W548','W549'
      ,'W370', 'W372','W373', 'W374','W380', 'W382','W383', 'W384','W390'
      ,'W392', 'W393', 'W394','W395', 'W396','W460', 'W462','W463', 'W470'
      ,'W472', 'W473','W480', 'W482','W483', 'W484','W485','W930', 'W932'
      ,'W933', 'W940','W942', 'W943','W950', 'W952','W953', 'W954','W520'
      , 'W522','W523', 'W530','W532', 'W533','W540', 'W542','W543', 'W544'
 )
 then 1
 when P.ProcedureCode in
 (
	'W401'	,'W409'	,'W418'	,'W421'	,'W429'	,'W521'	,'W529'	,'W538'	,'W541'	
	,'W549'	,'O181'	,'O189'	,'W400'	,'W403'	,'W410'	,'W413'	,'W420'	,'W423'	
	,'W425'	,'W520'	,'W523'	,'W532'	,'W540'	,'W543'	,'O180'	,'O183'
	,'W408'	,'W411'	,'W419'	,'W428'	,'W528'	,'W531'	,'W539'	,'W548'	,'O188'
	,'W402'	,'W404'	,'W412'	,'W414'	,'W422'	,'W424'	,'W426'	,'W522'	,'W530'	
	,'W533'	,'W542'	,'W544'	,'O182'	,'O184'
)
then 2
when  P.ProcedureCode in
(
		'L852'	,'L859'	,'L871'	,'L873'	,'L875'	,'L878'	,'L883'	,'L889'	
		,'L882'	,'L881'	,'L877'		,'L861'	,'L862'		,'L841'	,'L842'	
		,'L843'	,'L844'	,'L845'	,'L846'	,'L847'	,'L848'	,'L849'
		,'L851'	,'L853'	,'L869'	,'L872'	,'L874'	,'L876'	,'L879'	,'L888'	
		,'L858'			,'L868'			,'L931'	,'L932'	,'L933'	,'L934'	
		,'L935'	,'L936'	,'L937'	,'L938'	,'L939'
)
then 3
when  P.ProcedureCode in
(
		'T191'	,'T201'	,'T211'	,'T221'	,'T231'	,'T251'	,'T261'
		,'T192'	,'T202'	,'T212'	,'T222'	,'T232'	,'T252'	,'T262'
		,'T193'	,'T203'	,'T213'	,'T223'	,'T233'	,'T253'	,'T263'
		,'T194'	,'T204'	,'T214'	,'T224'	,'T234'	,'T254'	,'T264'
		,'T195'	,'T205'	,'T215'	,'T225'	,'T235'	,'T255'	,'T265'
		,'T196'	,'T206'	,'T216'	,'T226'	,'T236'	,'T256'	,'T266'
		,'T197'	,'T207'	,'T217'	,'T227'	,'T237'	,'T257'	,'T267'
		,'T198'	,'T208'	,'T218'	,'T228'	,'T238'	,'T258'	,'T268'
		,'T199'	,'T209'	,'T219'	,'T229'	,'T239'	,'T259'	,'T269'
)
then 4
else 'checkSQL'
end as PROMSProcCode



  FROM [WHREPORTING].[APC].[AllProcedures] P
  
  where
  P.ProcedureDate > '01 apr 2011'
  and
  P.ProcedureCode
  IN
      (
       'W371', 'W378','W379', 'W381','W388', 'W389','W391', 'W398','W399'
      ,'W461', 'W468','W469', 'W471','W478', 'W479','W481', 'W488','W489'
      ,'W931', 'W938','W939', 'W941','W948', 'W949','W951', 'W958','W959'
      ,'W521', 'W528','W529', 'W531','W538', 'W539','W541', 'W548','W549'
      ,'W370', 'W372','W373', 'W374','W380', 'W382','W383', 'W384','W390'
      ,'W392', 'W393', 'W394','W395', 'W396','W460', 'W462','W463', 'W470'
      ,'W472', 'W473','W480', 'W482','W483', 'W484','W485','W930', 'W932'
      ,'W933', 'W940','W942', 'W943','W950', 'W952','W953', 'W954','W520'
      , 'W522','W523', 'W530','W532', 'W533','W540', 'W542','W543', 'W544'
      --PROMS Unilateral HIP Replacement
	,'W401'	,'W409'	,'W418'	,'W421'	,'W429'	,'W521'	,'W529'	,'W538'	,'W541'	
	,'W549'	,'O181'	,'O189'	,'W400'	,'W403'	,'W410'	,'W413'	,'W420'	,'W423'	
	,'W425'	,'W520'	,'W523'	,'W532'	,'W540'	,'W543'	,'O180'	,'O183'
	,'W408'	,'W411'	,'W419'	,'W428'	,'W528'	,'W531'	,'W539'	,'W548'	,'O188'
	,'W402'	,'W404'	,'W412'	,'W414'	,'W422'	,'W424'	,'W426'	,'W522'	,'W530'	
	,'W533'	,'W542'	,'W544'	,'O182'	,'O184'
	--PROMS Unilateral KNEE Replacement
		,'L852'	,'L859'	,'L871'	,'L873'	,'L875'	,'L878'	,'L883'	,'L889'	
		,'L882'	,'L881'	,'L877'		,'L861'	,'L862'		,'L841'	,'L842'	
		,'L843'	,'L844'	,'L845'	,'L846'	,'L847'	,'L848'	,'L849'
		,'L851'	,'L853'	,'L869'	,'L872'	,'L874'	,'L876'	,'L879'	,'L888'	
		,'L858'			,'L868'			,'L931'	,'L932'	,'L933'	,'L934'	
		,'L935'	,'L936'	,'L937'	,'L938'	,'L939'
		--PROMS Varicose Veins Surgery
		,'T191'	,'T201'	,'T211'	,'T221'	,'T231'	,'T251'	,'T261'
		,'T192'	,'T202'	,'T212'	,'T222'	,'T232'	,'T252'	,'T262'
		,'T193'	,'T203'	,'T213'	,'T223'	,'T233'	,'T253'	,'T263'
		,'T194'	,'T204'	,'T214'	,'T224'	,'T234'	,'T254'	,'T264'
		,'T195'	,'T205'	,'T215'	,'T225'	,'T235'	,'T255'	,'T265'
		,'T196'	,'T206'	,'T216'	,'T226'	,'T236'	,'T256'	,'T266'
		,'T197'	,'T207'	,'T217'	,'T227'	,'T237'	,'T257'	,'T267'
		,'T198'	,'T208'	,'T218'	,'T228'	,'T238'	,'T258'	,'T268'
		,'T199'	,'T209'	,'T219'	,'T229'	,'T239'	,'T259'	,'T269'
		--PROMS Groin Hernia Surgery
		)