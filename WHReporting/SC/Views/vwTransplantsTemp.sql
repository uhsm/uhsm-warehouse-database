﻿--USE [WHREPORTING]
--GO

--/****** Object:  View [SC].[APCSpecialtyFunctionCorrection]    Script Date: 12/12/2012 15:56:03 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

/*
--Author: N Scattergood
--Date created: 08/03/2013
--View Created to report on Transplants sincecorrect issue arounding inconsistent recording of Cardiac Consultants
--last updated 08/03/2013
*/

Create View  [SC].[vwTransplantsTemp]

as

SELECT 
S.SourceSpellNo
	,S.FaciltyID
	,S.NHSNo
	,P.PatientTitle
	,P.PatientForename
	,P.PatientSurname

      --,S.EncounterRecno
      ,S.LengthOfSpell
      ,S.AdmissionDate
      ,cc.PrimaryProcedureDate
     ,s.[DischargeDate]
      ,S.[AdmissionConsultantName]
      --,S.[AdmissionSpecialty]

      --,S.[FaciltyID] as HospitalID
 
           
       --,S.[AdmissionMethodCode]
      --,S.[AdmissionMethod]
      --,S.DischargeMethod

       ,cc.[PrimaryDiagnosis]
      ,cc.[SubsidiaryDiagnosis]
      ,cc.[SecondaryDiagnosis1]
      ,cc.[SecondaryDiagnosis2]
      ,cc.[SecondaryDiagnosis3]
      ,cc.[SecondaryDiagnosis4]
      ,cc.[SecondaryDiagnosis5]    
      ,cc.[SecondaryDiagnosis6]     
      ,cc.[SecondaryDiagnosis7]  
      ,cc.[SecondaryDiagnosis8]    
      ,cc.[SecondaryDiagnosis9]      
      ,cc.[SecondaryDiagnosis10]
      ,cc.[SecondaryDiagnosis11]
      ,cc.[SecondaryDiagnosis12]
      ,cc.[SecondaryDiagnosis13]
      ,cc.[SecondaryDiagnosis14]
      ,cc.[SecondaryDiagnosis15]
      ,cc.[SecondaryDiagnosis16]
      ,cc.[SecondaryDiagnosis17]
      ,cc.[SecondaryDiagnosis18]
      ,cc.[SecondaryDiagnosis19]
      ,cc.[SecondaryDiagnosis20]  
      
             ,cc.[PriamryProcedure]
      ,cc.[SecondaryProcedure1]
      ,cc.[SecondaryProcedure2]
      ,cc.[SecondaryProcedure3]
      ,cc.[SecondaryProcedure4]
      ,cc.[SecondaryProcedure5]
      ,cc.[SecondaryProcedure6]
      ,cc.[SecondaryProcedure7]
      ,cc.[SecondaryProcedure8]
      ,cc.[SecondaryProcedure9]
      ,cc.[SecondaryProcedure10]
      ,cc.[SecondaryProcedure11]
           
            
     
  FROM [WHREPORTING].[APC].[Spell] S
  
  left join [WHREPORTING].[APC].[ClinicalCoding] cc
  on cc.[EncounterRecno] = S.EncounterRecno
  
  left join [WHREPORTING].[APC].[Patient] P
  on  S.EncounterRecno = P.EncounterRecno
  
  where


(
   
 (S.FaciltyID = 'RM24031879'  and S.AdmissionDate < ='03 Sep 2008' and s.[DischargeDate] >='03 Sep 2008')
 or (S.FaciltyID = 'RM21875032'  and S.AdmissionDate < ='16 Sep 2008' and s.[DischargeDate] >='16 Sep 2008')
 or (S.FaciltyID = 'RM21910828'  and S.AdmissionDate < ='18 Sep 2008' and s.[DischargeDate] >='18 Sep 2008')
 or (S.FaciltyID = 'RM21884186'  and S.AdmissionDate < ='03 Oct 2008' and s.[DischargeDate] >='03 Oct 2008')
 or (S.FaciltyID = 'RM24008432'  and S.AdmissionDate < ='02 Nov 2008' and s.[DischargeDate] >='02 Nov 2008')
 or (S.FaciltyID = 'RM21237317'  and S.AdmissionDate < ='11 Nov 2008' and s.[DischargeDate] >='11 Nov 2008')
 or (S.FaciltyID = 'RM21753220'  and S.AdmissionDate < ='12 Nov 2008' and s.[DischargeDate] >='12 Nov 2008')
 or (S.FaciltyID = 'RM2414523'  and S.AdmissionDate < ='27 Nov 2008' and s.[DischargeDate] >='27 Nov 2008')
 or (S.FaciltyID = 'RM21884188'  and S.AdmissionDate < ='29 Nov 2008' and s.[DischargeDate] >='29 Nov 2008')
 or (S.FaciltyID = 'RM21931968'  and S.AdmissionDate < ='26 Dec 2008' and s.[DischargeDate] >='26 Dec 2008')
 or (S.FaciltyID = 'RM21666780'  and S.AdmissionDate < ='28 Dec 2008' and s.[DischargeDate] >='28 Dec 2008')
 or (S.FaciltyID = 'RM21409209'  and S.AdmissionDate < ='22 Jan 2009' and s.[DischargeDate] >='22 Jan 2009')
 or (S.FaciltyID = 'RM21722603'  and S.AdmissionDate < ='06 Feb 2009' and s.[DischargeDate] >='06 Feb 2009')
 or (S.FaciltyID = 'RM21626234'  and S.AdmissionDate < ='11 Feb 2009' and s.[DischargeDate] >='11 Feb 2009')
 or (S.FaciltyID = 'RM24031886'  and S.AdmissionDate < ='23 Feb 2009' and s.[DischargeDate] >='23 Feb 2009')
 or (S.FaciltyID = 'RM24011014'  and S.AdmissionDate < ='09 Mar 2009' and s.[DischargeDate] >='09 Mar 2009')
 or (S.FaciltyID = 'RM21787722'  and S.AdmissionDate < ='27 Apr 2009' and s.[DischargeDate] >='27 Apr 2009')
 or (S.FaciltyID = 'RM21590044'  and S.AdmissionDate < ='04 May 2009' and s.[DischargeDate] >='04 May 2009')
 or (S.FaciltyID = 'RM21458772'  and S.AdmissionDate < ='25 May 2009' and s.[DischargeDate] >='25 May 2009')
 or (S.FaciltyID = 'RM21915524'  and S.AdmissionDate < ='27 May 2009' and s.[DischargeDate] >='27 May 2009')
 or (S.FaciltyID = 'RM24065645'  and S.AdmissionDate < ='12 Jun 2009' and s.[DischargeDate] >='12 Jun 2009')
 or (S.FaciltyID = 'RM21917297'  and S.AdmissionDate < ='24 Jun 2009' and s.[DischargeDate] >='24 Jun 2009')
 or (S.FaciltyID = 'RM21872192'  and S.AdmissionDate < ='06 Jul 2009' and s.[DischargeDate] >='06 Jul 2009')
 or (S.FaciltyID = 'RM24076365'  and S.AdmissionDate < ='29 Jul 2009' and s.[DischargeDate] >='29 Jul 2009')
 or (S.FaciltyID = 'RM24029309'  and S.AdmissionDate < ='29 Aug 2009' and s.[DischargeDate] >='29 Aug 2009')
 or (S.FaciltyID = 'RM2377245'  and S.AdmissionDate < ='06 Sep 2009' and s.[DischargeDate] >='06 Sep 2009')
 or (S.FaciltyID = 'RM24094992'  and S.AdmissionDate < ='13 Sep 2009' and s.[DischargeDate] >='13 Sep 2009')
 or (S.FaciltyID = 'RM24008585'  and S.AdmissionDate < ='18 Sep 2009' and s.[DischargeDate] >='18 Sep 2009')
 or (S.FaciltyID = 'RM24029310'  and S.AdmissionDate < ='08 Dec 2009' and s.[DischargeDate] >='08 Dec 2009')
 or (S.FaciltyID = 'RM21787654'  and S.AdmissionDate < ='14 Dec 2009' and s.[DischargeDate] >='14 Dec 2009')
 or (S.FaciltyID = 'RM21096471'  and S.AdmissionDate < ='16 Dec 2009' and s.[DischargeDate] >='16 Dec 2009')
 or (S.FaciltyID = 'RM24033698'  and S.AdmissionDate < ='31 Dec 2009' and s.[DischargeDate] >='31 Dec 2009')
 or (S.FaciltyID = 'RM24047601'  and S.AdmissionDate < ='27 Jan 2010' and s.[DischargeDate] >='27 Jan 2010')
 or (S.FaciltyID = 'RM24018253'  and S.AdmissionDate < ='10 Feb 2010' and s.[DischargeDate] >='10 Feb 2010')
 or (S.FaciltyID = 'RM24095320'  and S.AdmissionDate < ='02 Mar 2010' and s.[DischargeDate] >='02 Mar 2010')
 or (S.FaciltyID = 'RM24019695'  and S.AdmissionDate < ='05 Mar 2010' and s.[DischargeDate] >='05 Mar 2010')
 or (S.FaciltyID = 'RM21879940'  and S.AdmissionDate < ='11 Mar 2010' and s.[DischargeDate] >='11 Mar 2010')
 or (S.FaciltyID = 'RM21871310'  and S.AdmissionDate < ='16 Mar 2010' and s.[DischargeDate] >='16 Mar 2010')
 or (S.FaciltyID = 'RM21582653'  and S.AdmissionDate < ='24 Mar 2010' and s.[DischargeDate] >='24 Mar 2010')
 or (S.FaciltyID = 'RM24065662'  and S.AdmissionDate < ='29 Mar 2010' and s.[DischargeDate] >='29 Mar 2010')
 or (S.FaciltyID = 'RM21918418'  and S.AdmissionDate < ='09 Apr 2010' and s.[DischargeDate] >='09 Apr 2010')
 or (S.FaciltyID = 'RM2432346'  and S.AdmissionDate < ='13 Apr 2010' and s.[DischargeDate] >='13 Apr 2010')
 or (S.FaciltyID = 'RM21876564'  and S.AdmissionDate < ='21 Apr 2010' and s.[DischargeDate] >='21 Apr 2010')
 or (S.FaciltyID = 'RM24107191'  and S.AdmissionDate < ='23 May 2010' and s.[DischargeDate] >='23 May 2010')
 or (S.FaciltyID = 'RM24024437'  and S.AdmissionDate < ='22 Jun 2010' and s.[DischargeDate] >='22 Jun 2010')
 or (S.FaciltyID = 'RM24090808'  and S.AdmissionDate < ='29 Sep 2010' and s.[DischargeDate] >='29 Sep 2010')
 or (S.FaciltyID = 'RM24142982'  and S.AdmissionDate < ='14 Oct 2010' and s.[DischargeDate] >='14 Oct 2010')
 or (S.FaciltyID = 'RM24032356'  and S.AdmissionDate < ='15 Oct 2010' and s.[DischargeDate] >='15 Oct 2010')
 or (S.FaciltyID = 'RM24099947'  and S.AdmissionDate < ='04 Nov 2010' and s.[DischargeDate] >='04 Nov 2010')
 or (S.FaciltyID = 'RM21541234'  and S.AdmissionDate < ='06 Nov 2010' and s.[DischargeDate] >='06 Nov 2010')
 or (S.FaciltyID = 'RM24066398'  and S.AdmissionDate < ='12 Dec 2010' and s.[DischargeDate] >='12 Dec 2010')
 or (S.FaciltyID = 'RM24092072'  and S.AdmissionDate < ='11 Jan 2011' and s.[DischargeDate] >='11 Jan 2011')
 or (S.FaciltyID = 'RM24140063'  and S.AdmissionDate < ='12 Jan 2011' and s.[DischargeDate] >='12 Jan 2011')
 or (S.FaciltyID = 'RM24145803'  and S.AdmissionDate < ='30 Jan 2011' and s.[DischargeDate] >='30 Jan 2011')
 or (S.FaciltyID = 'RM24156277'  and S.AdmissionDate < ='16 Feb 2011' and s.[DischargeDate] >='16 Feb 2011')
 or (S.FaciltyID = 'RM21651740'  and S.AdmissionDate < ='26 Feb 2011' and s.[DischargeDate] >='26 Feb 2011')
 or (S.FaciltyID = 'RM21850519'  and S.AdmissionDate < ='27 Feb 2011' and s.[DischargeDate] >='27 Feb 2011')
 or (S.FaciltyID = 'RM24126478'  and S.AdmissionDate < ='06 Mar 2011' and s.[DischargeDate] >='06 Mar 2011')
 or (S.FaciltyID = 'RM24131550'  and S.AdmissionDate < ='20 Mar 2011' and s.[DischargeDate] >='20 Mar 2011')
 or (S.FaciltyID = 'RM21754711'  and S.AdmissionDate < ='20 Mar 2011' and s.[DischargeDate] >='20 Mar 2011')
 or (S.FaciltyID = 'RM21799261'  and S.AdmissionDate < ='23 Mar 2011' and s.[DischargeDate] >='23 Mar 2011')
 or (S.FaciltyID = 'RM24126465'  and S.AdmissionDate < ='29 Apr 2011' and s.[DischargeDate] >='29 Apr 2011')
 or (S.FaciltyID = 'RM24115059'  and S.AdmissionDate < ='30 Apr 2011' and s.[DischargeDate] >='30 Apr 2011')
 or (S.FaciltyID = 'RM24101398'  and S.AdmissionDate < ='08 May 2011' and s.[DischargeDate] >='08 May 2011')
 or (S.FaciltyID = 'RM24054225'  and S.AdmissionDate < ='10 May 2011' and s.[DischargeDate] >='10 May 2011')
 or (S.FaciltyID = 'RM24036339'  and S.AdmissionDate < ='12 May 2011' and s.[DischargeDate] >='12 May 2011')
 or (S.FaciltyID = 'RM24130024'  and S.AdmissionDate < ='20 May 2011' and s.[DischargeDate] >='20 May 2011')
 or (S.FaciltyID = 'RM24145040'  and S.AdmissionDate < ='27 May 2011' and s.[DischargeDate] >='27 May 2011')
 or (S.FaciltyID = 'RM24110061'  and S.AdmissionDate < ='30 May 2011' and s.[DischargeDate] >='30 May 2011')
 or (S.FaciltyID = 'RM24191228'  and S.AdmissionDate < ='09 Jun 2011' and s.[DischargeDate] >='09 Jun 2011')
 or (S.FaciltyID = 'RM24132208'  and S.AdmissionDate < ='05 Jul 2011' and s.[DischargeDate] >='05 Jul 2011')
 or (S.FaciltyID = 'RM24187366'  and S.AdmissionDate < ='19 Jul 2011' and s.[DischargeDate] >='19 Jul 2011')
 or (S.FaciltyID = 'RM24110382'  and S.AdmissionDate < ='27 Jul 2011' and s.[DischargeDate] >='27 Jul 2011')
 or (S.FaciltyID = 'RM24150160'  and S.AdmissionDate < ='23 Aug 2011' and s.[DischargeDate] >='23 Aug 2011')
 or (S.FaciltyID = 'RM24207692'  and S.AdmissionDate < ='25 Aug 2011' and s.[DischargeDate] >='25 Aug 2011')
 or (S.FaciltyID = 'RM24124976'  and S.AdmissionDate < ='01 Sep 2011' and s.[DischargeDate] >='01 Sep 2011')
 or (S.FaciltyID = 'RM24043661'  and S.AdmissionDate < ='03 Sep 2011' and s.[DischargeDate] >='03 Sep 2011')
 or (S.FaciltyID = 'RM21759325'  and S.AdmissionDate < ='23 Sep 2011' and s.[DischargeDate] >='23 Sep 2011')
 or (S.FaciltyID = 'RM24169238'  and S.AdmissionDate < ='17 Oct 2011' and s.[DischargeDate] >='17 Oct 2011')
 or (S.FaciltyID = 'RM24149428'  and S.AdmissionDate < ='22 Dec 2011' and s.[DischargeDate] >='22 Dec 2011')
 or (S.FaciltyID = 'RM21738346'  and S.AdmissionDate < ='12 Jan 2012' and s.[DischargeDate] >='12 Jan 2012')
 or (S.FaciltyID = 'RM21522730'  and S.AdmissionDate < ='20 Jan 2012' and s.[DischargeDate] >='20 Jan 2012')
 or (S.FaciltyID = 'RM24033822'  and S.AdmissionDate < ='25 Jan 2012' and s.[DischargeDate] >='25 Jan 2012')
 or (S.FaciltyID = 'RM21845938'  and S.AdmissionDate < ='15 Feb 2012' and s.[DischargeDate] >='15 Feb 2012')
 or (S.FaciltyID = 'RM24103397'  and S.AdmissionDate < ='16 Feb 2012' and s.[DischargeDate] >='16 Feb 2012')
 or (S.FaciltyID = 'RM24232811'  and S.AdmissionDate < ='18 Feb 2012' and s.[DischargeDate] >='18 Feb 2012')
 or (S.FaciltyID = 'RM24208107'  and S.AdmissionDate < ='22 Feb 2012' and s.[DischargeDate] >='22 Feb 2012')
 or (S.FaciltyID = 'RM24210392'  and S.AdmissionDate < ='05 Apr 2012' and s.[DischargeDate] >='05 Apr 2012')
 or (S.FaciltyID = 'RM24223655'  and S.AdmissionDate < ='30 Apr 2012' and s.[DischargeDate] >='30 Apr 2012')
 or (S.FaciltyID = 'RM24069282'  and S.AdmissionDate < ='11 May 2012' and s.[DischargeDate] >='11 May 2012')
 or (S.FaciltyID = 'RM24159881'  and S.AdmissionDate < ='13 Jun 2012' and s.[DischargeDate] >='13 Jun 2012')

)