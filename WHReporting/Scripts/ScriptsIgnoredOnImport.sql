﻿
/****** Object:  Table [QLIK].[DTOCByCategory]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DTOC]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[DS_CYP901]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[DS_CYP202]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[DS_CYP201]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[DS_CYP104]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[DS_CYP102]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[DS_CYP101]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[DS_CYP002]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[DS_CYP001]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[DPR_WHOSaferSurgery]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_WardLookUp]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_WardAccreditation]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_UrgentOpsCancelledTwice]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_TTOsCompleted]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_TTOs]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_Transfusions]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[DPR_TherapyResponse]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_SUI]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_StudiesCompleted]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_StrokeMetricID]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_HRHeadcount]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_HRCorporateAreaLookup]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_Governance]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_FriendsFamilyTestsOutpatients]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_FriendsFamilyTestsInpatients]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_FriendsFamilyTestsA&E]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_FriendsFamilyTestCOMMAT]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_DocumentationAudit]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[DPR_DM01]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_DelayedTransfersOfCare]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[DPR_CQUINData]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[DPR_ComplaintsResponses]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[DPR_Cancer]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_CancelledOps]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_BreastScreening]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_Appraisal]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[DNMPostCodeGroups]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[DnmHpPc]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[DNM-FOI16-141-DataAD1]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[DNM163-Dr Yoon Toong Chin-coding information for sepsis list-RequestData]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[CoderSpecMapping]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [RTT].[adm_disch_dataset]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[AdHocSueB]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[AdHocPasswords]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[ActivityPlanOLD]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[ActivityPlan]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ACM_Referrals]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [OP].[BookingFact_Slot]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[OPRapidAccessClinics]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[NewFURatioTarget]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[New Patients NNN order]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [BADGER].[NCCEpisodes_cp]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [BADGER].[NCCEpisodes]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [BADGER].[NCCDailyActivity_cp]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [BADGER].[NCCDailyActivity]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [BADGER].[NCCBabies_cp]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [BADGER].[NCCBabies]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[NursingCareHomes]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [RTT].[Notes]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[QARDATAQ120132014bu]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[QARDATAQ120132014]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[NHSChoicesBaskets]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[OPSessionStatusChangeRequestBy]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [LK].[OPSessionStatusChangeReason]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [LK].[OPSessionStatus]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [LK].[OPSessionEndStatusReason]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[PTListTemp]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[OPSSessionStartStatus]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [LK].[OPSSessionSessionStartStatusReason]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [LK].[OPSSessionEndStatus]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ICE].[FactRequest_cp20150302]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ICE].[FactRequest]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[lkIndirectPatientActivityType]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[lkGroupTherapyIndicator]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[lkGroupSessionType]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[lkDischargeMethod]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[lkDischargeDestination]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Prodacapo].[LKDiagnostics]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[lkContactType]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[lkConsultantMedium]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[lkCID]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[lkCareProfessionalStaffGroup]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[lkCancellationReason]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[lkAttendStatus]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[lkAttendGroup]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[lkAttendanceType]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[lkAliedHealthProfessional]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[lkAgeOfReferral]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[lkAgeBands]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  UserDefinedFunction [dbo].[fn_IntCSVSplit]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[FinancePharmacyIssues]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Prodacapo].[MSSA]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[MRSAExclusions]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].['Monthly (Abs Rate) Data Warehou$']    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [APC].[MonitorSensitiveProcedure]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [APC].[MonitorSensitiveDiagnosis]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [APC].[MonitorPatientList]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[lkReferralAuthorisation]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[lkProfessionalCarer]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[lkPlaceOfDeath]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[lkOutcome]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[lkNewSpecialties]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[RadiologyInterventionCodes]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[RadiologyExamTypes]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [AE].[ReportAECodingValidation_DrillThrough]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [AE].[ReportAECodingValidation]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[APCMancPostcodes]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SLS].[APCActivity]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [AE].[AmbulancePostcodeSector]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [AE].[AmbulanceLateTurnaroundReason]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [AE].[AmbulanceHospital]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[AmbulanceDataWeekly]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[AmbulanceDataQuarterly]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[AmbulanceDataMonthly]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[SecondaryCarer]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[SchedulesPatient]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [RTT].[waiting_listv1]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [APC].[Patient]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [PTH].[PathologyKPI]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Prodacapo].[PathologyFeedforProdacapo]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  View [dbo].[ObjectRelationships]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[NursingHomes]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Prodacapo].[TrustInduction]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[TreatmentFunction]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[TransplantVADPatients]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TRANSPLANT].[TransplantedPatients]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [TRANSPLANT].[TransplantAssessments]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[TransfusionUnfatedUnitsWithRefresh]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[TransfusionUnfatedUnits_old]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[TransfusionUnfatedUnits_cp20150421]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[RTT]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Version]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vAudit]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ICE].[Results]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ICE].[Request]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[StagingTransfusionUnfatedUnits]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[StagingTransfusionFatedUnits]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DQ].[ReportUnrecordedAttendanceAndOutcome_DrillThrough]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[LookupConnectionID]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[lkWaitingTimeMeasurementType]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TARN].[lkTARNCodes]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[lkSpecialtyDivision]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[lkSourceOfReferral]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[lkServices]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[lkSameSpecialties]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[lkReferralStatus]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[lkReferralClosure]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[SpecDivDependencies]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  View [dbo].[SourceTables]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[SoccerStudy]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[DHClinicLocation]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[DermPatientsonPTL]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[Dementia]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[DemandCapacityConsSpecMatrix]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Community].[AQPPodiatry]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [APC].[ElectiveAdmissionMRSAScreens]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [DM01].[ElectiveActivity]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [LK].[WardSpecialtyLookup]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [BCC].[WardsDirectorate]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[WardDischargeTargets]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[Cancer62DayFilesCWTBaseline]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [IDS].[EDTPracticeStart_xxx]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [IDS].[EDTGPPractices_New]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [IDS].[EDTGPPractices]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[EDD]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Prodacapo].[ECOLI]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [APC].[ECMODays]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[ECMO Patients]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [CN].[CasenoteCurrentLocationDataset]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[OPDClinicCodesHS180116]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[OPCS_List_v2]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[OPClinicCodesResp]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [SLS].[OPActivity]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OPD].[OP_New_Follow_Attend_DNA]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[OP ATTS WITH CONT FROZEN LATEST FINAL M12 1213 1ST CUT]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[PriorApprovalProcedures]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[Practices_To_CCGs]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[PotluriAdHocPatientIds]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TPY].[PhysioClinicMap]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[TheatreReferenceLookups]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [TH].[TheatreProcedureTimesByProcedureOnly]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [TH].[TheatreProcedureTimes_old]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [TH].[TheatreProcedureTimes]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[TheatreOperationDelays]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[TheatreOpDetails]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[TheatreMetrics]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[dbo_ICP]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Prodacapo].[SpecialtyToScorecard]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [LK].[SpecialtyDivisionTemp]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[EPIPKPIValues]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[XXX_DPR_MixedSexAccomodation]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [BLOODTRACK].[ReportTransfusionsFatedUnfatedCombined]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[DPR_SicknessAbsence]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_SHMI]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_SFFT]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_Safeguarding Children]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[DPR_Safeguarding Adults]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_RTT]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[DPR_R_DRecruitmentReport]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_PressureUlcers]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_PFDs_Candour_NEs]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_Pathology]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_NonMedicalAppraisals]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_MixedSexAccomodationWard]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_MandatoryTraining]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_LabourTurnover_StaffGroup]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_LabourTurnover_Managers]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_LabourTurnover]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_InfectionWard]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_InfectionDirectorate]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_HSMR_All]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[DPR_HSMR_56]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_HRLongShortTermSickness]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  UserDefinedFunction [dbo].[CalcAge]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [RF].[Patient]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [LK].[RTTSpecialtyExclusions]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  UserDefinedFunction [dbo].[CleanString]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [CHKS].[CHKSPatientLevelExtract2_xxxx]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [CHKS].[CHKSPatientLevelExtract]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [CHKS].[CHKSBalancedScorecardRAMI]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [CHKS].[CHKSBalancedScorecardAliasLkp]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [OP].[check]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[CharlsonIndex]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Prodacapo].[CDIFF]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportOPRefsSchedule5ContractPCTPicklist]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_StaffSurvey]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[DPR_StaffingLevels]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  UserDefinedFunction [dbo].[SplitStrings_CTE]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  UserDefinedFunction [dbo].[Split1]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [OP].[BookingFact_Session]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[BedStatus]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[BedOccupancyAtMidnight]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[BedOccupancyAt11]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[BedOccupancy2]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[BedOccupancy]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[AvLOS1314byDiag]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[AvLOS1213byDiag]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[AvLOS1213and1314byDiag]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [OP].[CABPAINStaging]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [OP].[CABPAINMain]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[CABG and Valve Patients 2009 to 2012]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[C]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[EPIPKPIDetails]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ztOpenReferral1]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[znOpenReferral]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [OP].[znClinicalCoding]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  UserDefinedFunction [PTL].[GetAdjustedBreachDate]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  UserDefinedFunction [dbo].[GEN_FN_StripCharacters]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  UserDefinedFunction [dbo].[GetAgeOnSpecifiedDate]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  UserDefinedFunction [dbo].[GetAgeFromBirthDate]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[PADSPatientDetail]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[PadsKPI]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  View [dbo].[Packages]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [RTT].[outpatient_dataset]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[OtherClientLinkedActivity]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [VTE].[Exclusions]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[EpisodeWardMatch]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [APC].[AllDiagnosis]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  UserDefinedFunction [dbo].[fnWeekDiff]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  UserDefinedFunction [dbo].[fnMyTimeFormat]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  UserDefinedFunction [dbo].[fnc_FiscalYear]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [PbR].[SpellHRG20122013]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[CalendarCopy]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [HarmFreeCare].[HarmFreeCareWardtoDivision]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [HarmFreeCare].[HarmFreeCareData]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[GroupSessions]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [RTT].[AHPBreachReport]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[AEMeasuresTable]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[AEForecast]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [IDS].[AEDischargeSummaries]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[InvalidPostcodes]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [CAN].[InterfaceAudit]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [TPY].[ContactsDataset]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  View [PbR].[PBRFlex]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[Patients]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [CHKS].[PatientLevelExtract]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[TransfusionUnfatedUnits]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[TransfusionPASBloodTrackLoc_mapping]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[TransfusionFatedUnitsWithRefresh]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[TransfusionFatedUnits_old]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[TransfusionFatedUnits_cp20150421]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[TransfusionFatedUnits]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[TransfusionAdhocPatientList]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [LK].[TimeOfDay]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [TH].[TheatreTargetStartEndTimes]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[TheatreSessions]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ICE].[Patient]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[Schedules]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[TheatreCancellations]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [TH].[TheatreAnaestheticTimesLA]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [TH].[TheatreAnaestheticTimesByProcedureOnlyLA]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [TH].[TheatreAnaestheticTimesByProcedureOnly]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [TH].[TheatreAnaestheticTimes_old]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [TH].[TheatreAnaestheticTimes]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[Test]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[tempcancersurgery]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[temp_ws_pompe_in]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [OP].[WLFollowUpPartialBooking]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[IndirectPatientActivity]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Prodacapo].[IMPME30_PDCCopy]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Prodacapo].[IMPME30_PDC]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ICE].[ICEReportsforRRO]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [LK].[ICD-10CCSCategory]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[IBASupportSpecialtyLookup_TLA_NHSCode]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [OP].[ClinicalCoding]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [APC].[MonitorDecileGroup]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[MissingSLSPatients]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [LK].[MissingEDOPALPatients]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[OPExcludedClinic]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [APC].[ReferralToTreatment]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[Referrals]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [RTT].[referral_dataset]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ICE].[FactReport]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [PbR].[TraffordNonCommissionedProcedures]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [PbR].[PBRFlexAndFreeze]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  UserDefinedFunction [dbo].[Split]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ICE].[MatchedPatients2]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ICE].[MatchedPatients]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[MarketShareImport]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[MarketShareFinal]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [APC].[CriticalCarePeriod]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [QLIK].[MedicalOutliers]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [BCC].[MBCQT_Data]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[ReportingAudit]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Prodacapo].[PathologyFeedBforProdacapo]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [LK].[PASReferenceValue]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [LK].[GPCurrentDetailsTest]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [PTH].[GPAccessData]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  UserDefinedFunction [dbo].[GetStartOfDay]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ICE].[FilingReportWho]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Prodacapo].[FFT]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [RTT].[fce_dataset]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ICE].[FactResults]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ICE].[FactInvestigation]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [APC].[ClinicalCoding]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[lkAdmissionSource]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[lkAdmissionMethod]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[lkActivityType]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[lkActivityLocationType]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  UserDefinedFunction [SC].[list_to_tbl]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[LineageMap]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Community].[LearningDisability]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [RTT].[LatestRTTStatus]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [RTT].[IPWL_dataset]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[MainTemplateClinicCodesforAO]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [OP].[TelephoneClinics]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[TAVIPatients]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [AE].[TARNExclusionDiagnoses]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [AE].[TARNExclusionCodes]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[TargetTables]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Thermometer].[DataImport]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[DataFlows]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[DataCompliancesPatients]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[DataCompliancesIndicatorThresholds]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[DataCompliances]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[DataAccreditation]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ICE].[CSDataset]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [LK].[ConsultantSpecialty]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[StrokeTIALookUp]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [APC].[StrokeComments]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [APC].[DaycaseIndicators]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [LK].[DateTime]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  UserDefinedFunction [dbo].[RemoveNonDisplayChars]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [RF].[ReferralToTreatment]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [OP].[ReferralToTreatment]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Audit]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[CommunityNursingPatchLookUpList]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[WardtoDivision]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Thermometer].[WardsToDivision]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [COM].[WardStay]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [APC].[WardStay]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  UserDefinedFunction [dbo].[fn_String_To_Table]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [APC].[Spell]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [LK].[PasPCTOdsCCG]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [OP].[SessionTemplateDetail]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[SESSION_BASE_TABLE]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  UserDefinedFunction [dbo].[GetEndOfDay]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[ASIs]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [QLIK].[RAID]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[RadiologyReporterTypeLookup]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportRadiologyValidation]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [WsB].[Winscribe_Turnaround_Data]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[Ward_Department$]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [COM].[UpdateDataCompliances]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].['July cancellations$']    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  UserDefinedFunction [dbo].[ufnFormatPathResult]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_CountChar]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[TXPatients]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Prodacapo].[SLAMM09Cut1PL]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[SLAM_Extract]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[SLAM TABLE FROZEN LATEST M12 1213 2ND CUT]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [OP].[Schedule]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[SaferBundleManualLoad]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [RF].[Referral]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [IDS].[DischargeSummaries]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ICE].[DimServiceUser]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ICE].[DimRequestStatus]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ICE].[DimProvider]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[CC_Procedures]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[CC_ProcedureGroups]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [CN].[CasenoteLocationDataset2]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [CN].[CasenoteLocationDataset]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[CasenoteDatasetInfoSQL]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [CN].[CasenoteCurrentLocationDataset_cp_20151126]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [AE].[AmbulanceDailyMDS]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [AE].[AmbulanceCallNature]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [AE].[AmbulanceCallCategory]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[AllSpellsforDemand&Capacity]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [APC].[AllProcedures]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [OP].[Patient]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [APC].[Episode]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[OPERATION_BASE_TABLE]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[PatientCurrentDetails]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ICE].[DimLocation]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ICE].[DimInvestigationRequested]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ICE].[DimFilingStatus]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [LK].[SpecialtyDivisionBackup220715]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [LK].[SpecialtyDivision]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [COM].[Spells]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ICE].[DimClinician]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Prodacapo].[DiagnosticWaits]    Script Date: 07/04/2016 10:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [LK].[Calendar]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  View [INFO].[vwDayCaseRate]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [LK].[vwDateTime]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OPD].[Referral]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [DM01].[SleepActivity]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ICE].[ExtractICEReportsforRRO]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Community].[UpdateAQPPodiatry]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[UpdateAETROLLEY]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [QLIK].[UpdateAEForecastsTable]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[Update4HOURS]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[Update StrokeExclusions]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DQ].[UndischargedPatients]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  UserDefinedFunction [dbo].[ufnTransposeInvestigations]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[JBTEST]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[AdmissionMonthBySpecialty]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[WalkSources]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [WsB].[Winscribe_Turnaround]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DQ].[Wellbabies]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [WsB].[Winscribe_Turnaround_type]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [WsB].[Winscribe_Turnaround_dates]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[Reasonableness_Report]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [USS].[ReadmissionsRolling6Months_DefaultDates]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [APC].[Readmissions]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [OP].[SessionUtilisation]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [AE].[ReportAEAttendancesByTriage__DrillThroughTrafford]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [AE].[ReportAEAttendancesByTriage__DrillThrough]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[ReportAdmmissions16and17yearOlds]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[ReportAdmissionsByAdmissionSpecialty]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CAN].[Report2wwreferrals]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[Report_AuditCommissionBasketDayCases2_patientlevel]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[Report_AuditCommissionBasketDayCases2]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[Report_AuditCommissionBasketDayCases_PatientLevel]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[Report_AuditCommissionBasketDayCases]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[Report Duration of Theatre Ops by Discharge Date]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DM01].[AudIPMWL_WL_Breaches]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DQ].[DateCoded]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[DailyDOSAReport]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[CurrentMonth_DefaultDates]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[Connections]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DM01].[Audiology_NonIPMWL_WL_Breaches]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [RTT].[GetLatestRTTStatus_]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vw_EpisodeFeedsforProdacapo]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Prodacapo].[vw_ECOLI]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Prodacapo].[vw_DiagnosticWaits]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Prodacapo].[vw_CDIFF]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vw_allfces_for_TransplantDB]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Prodacapo].[vw_AEFeedforProdacapo]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DQ].[StyalOP]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DQ].[StyalIP]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vw_OPFeedforProdacapo]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Prodacapo].[vw_NHSThermometer]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Prodacapo].[vw_MSSA]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vw_ICUWardEpisodes_for_TransplantDB]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [LRD].[CurrentWard]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vw_RadiologyRoomLookUpFeedforProdacapo]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vw_PathologyFeedforProdacapo]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vw_WardsforProdacapo]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Prodacapo].[vw_VTEAssessments]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Prodacapo].[vw_TrustInduction]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vw_TheatresFeedforProdacapo]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vw_TheatreSessionFeedforProdacapo]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[LastWeek_DefaultDates]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[LastMonth_DefaultDates]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[Last7Days_DefaultDates]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [APC].[vwCriticalCareOrgans]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [APC].[vwCriticalCareLevels]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [APC].[vwCriticalCareFactJCReport]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [APC].[vwCriticalCareFact]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [APC].[vwCriticalCareAdmissions]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [APC].[vwCriticalCareActualDaysJCReport]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [APC].[vwCriticalCareActualDays]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportInpatientHRGUCodes_201516]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportInpatientHRGUCodes]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [ICE].[vwICEDirectorateReportDetailsbySpecialty]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [ICE].[vwICEDirectorateReportDetailsbyLocationCriticalCare]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [ICE].[vwICEDirectorateReportDetailsbyLocationAE]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [ICE].[vwICEDirectorateReportDetailsbyLocation]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwICEDirectorateReportDetails]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [QLIK].[vwICECliniciansSpecialty]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [COM].[vwFriendsFamily]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [QLIK].[InsertIntoMedicalOutliers]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  UserDefinedFunction [dbo].[GetPseudonomisedValue]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ICE].[ReportICEReportsforRRO]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ICE].[ReportICEFilingSummary]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[ReportICEFilingDetail]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ICE].[ReportICEFilingByDirectorateSpecialtyConsultant]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ICE].[ReportICEDemand]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[ReportICEDemand]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportHSCWeekly]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ETM].[ReportHospitalSmartBoardOccupancy]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ETM].[ReportHospitalOccupancyMidnightLoS]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [PbR].[MaternityDatingScan]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ETM].[ReportHospitalMedicalOutliers]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [RTT].[BuildReferralsDataset_]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportFlexAndFreezeDataSetChecks]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ICE].[ReportFilingByPatient]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[CreateTimeOfDay]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[CorrectCalendarYearEnd]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[Manchester_Over60s_discharges]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[ProcessTransplants_GetSpellEpisodeDetails]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportHRGAPCEncounter]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportEpisodesWIP]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportEpisodes]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportCriticalCarePeriod]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportCRIS]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportConsultantSpecialty]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportCOMSpells]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportCOMSchedules]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportCOMReferrals]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportCOMAQPPodiatry]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportCodingBase]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportBIC]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportAE]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[PTL_18wk_Intergrated_tool_Consultant]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[PTL_18wk_Intergrated_tool_Clinic]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[PTL_18wk_Intergrated_tool]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ThisMonth_DefaultDates]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ThisCalendarYear_DefaultDates]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Community].[ExportUrgentCareDashboardData]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [AE].[AEAttendancesNewHealthDealTrafford]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TPY].[AHPRTT]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [RTT].[AHPExtractNonAdmitted]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [RTT].[AHPExtractIncomplete]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[ORMISTheatre]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [INFO].[GetAdmissionByMethod]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DM01].[Forecast_CombinedWL]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[AdmittedWaitingListAllArchive]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[Yesterday_DefaultDates]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[BuildWLFollowUpPartialBooking]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CAN].[AuditQueries]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [QLIK].[DPR_StaffEngagementScore]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[CABSynertech]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[CABPAINSynertech]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[CABPAINStagingData]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportTopOutpatientDNAClinics_Specialty]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportTopOutpatientDNAClinics_DrillThrough]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportTopOutpatientDNAClinics_Division]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportTopOutpatientDNAClinics_Consultant]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportTopOutpatientDNAClinics_Clinic]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[WriteAuditLogEvent]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[PivotAdmissionsByPatientClass]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[PivotAdmissionMonth]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [COM].[UpdateIndirectPatientActivityNew]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [COM].[UpdateIndirectPatientActivity]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [COM].[UpdateGroupSessionsNew]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [COM].[UpdateGroupSessions]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [QLIK].[EPIPKPIDischargeLoungeUtilisation]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportUndischargedCriticalCare_WithPID_CritPar]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportUndischargedCriticalCare_WithPID]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportUndischargedCriticalCare_CCG]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportUndischargedCriticalCare_AllCCService]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [S6].[ReportUndischargedCriticalCare]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportUndischargedCriticalCare]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [AE].[ReportUnder17sAttendingAandEDrunkOverdose_MainReport]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [AE].[ReportUnder17sAttendingAandEDrunkOverdose_DrillThrough]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CAN].[ReportTYA]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ESR].[ReportTrustMandatoryTrainingCompliance]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[ReportTraumaTransfers]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[BuildEpisode]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[BuildElectiveAdmissionMRSAScreens]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DM01].[BuildElectiveActivity]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [COM].[BuildDataComplianceOld]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[BuildCriticalCarePeriod]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TPY].[BuildContactsDataset]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[BuildClinicalCoding]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[BuildClinicalCoding]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CN].[BuildCasenoteCurrentLocationDataset]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[BuildBookingFact_Slot]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [LRD].[Specialty]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [DM01].[SleepWaitingList]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportOPAttendances]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportInpatientCoding]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportICETests_lkp]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportHRGCCEncounter]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [QLIK].[vwDelayedDischargesICU]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [TPY].[vwPhysioContacts]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportWards]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportTherapyContacts]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportTheatreStaffCategory]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportTheatreSession]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportTheatres]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportTheatreConsultant]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportSpells]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportRadiologyRoom]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportRadiology]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[PHETRACEStudyXMLExtract]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[PHETRACEStudyParameters]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportPathologyICERequests]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportPASConsultant]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportOutpatientCoding]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [AE].[OpenOrClosedFracturesDischargedFromAE]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [COM].[UpdateRTT]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [COM].[UpdateReferralsNew]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [COM].[UpdateReferrals]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[UpdateRAMI]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [COM].[UpdateProfessionalCarer]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [COM].[UpdatePatients]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [COM].[UpdateOtherClientLinkedActivityNew]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [COM].[UpdateOtherClientLinkedActivity]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[UrgentCareAdmissionsNewHealthDealTrafford]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [COM].[UpdateWardStay]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[UpdateVTEAssessments]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[UpdateTheatreUtilisation]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [COM].[UpdateSpells]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [COM].[UpdateSecondaryCarer]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [COM].[UpdateSchedulesPatient]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[Casenote_Location_report_Specialty]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[Casenote_Location_report_RM2_search]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[Casenote_Location_report]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[Casenote_location_CodingDept]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [DM01].[EchoTOEWaitingList]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DM01].[EchoToe_WL_Breaches]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DM01].[Echo_WL_Breaches]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASTemporaryPatientAddress]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASSourceOfReferral]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASSex]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASScheduleType]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASScheduleProfCarerType]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASScheduleOutcome]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASScheduleCancelledBy]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASScheduleAttendStatus]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASRTTStatus]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASReligion]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASReferralRequestedService]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASReferralReason]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASReferralPriority]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASReferralMedium]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASReferralCompleteReason]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASReferralCancelReason]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASOverseasStatus]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASNHSNoStatus]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASMaritalStatus]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASIntendedManagement]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASEthnicOrigin]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [COM].[vwReferrals]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [DM01].[vwRadiologyWLValidation]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [SC].[vwPROMSActivity]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASDischargeMethod]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASDischargeDestination]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASAdmissionSource]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASAdmissionMethod]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPASAdministrativeCategory]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwPaediatricNetworkOutpatientDataRequest]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [TPY].[vwOTContacts]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [SC].[vwOPSpecialtyFunctionCorrection]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [TPY].[vwTherapiesContacts]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [APC].[vwTarnExclusionsbySpell]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [APC].[vwStrokeData]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [COM].[vwSpells]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[UserAccess]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [OP].[vwSlotType]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [TPY].[vwTherapiesOP]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwTherapiesFirstApt]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CSS].[DiabetesProjectPatientLevel]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DQ].[BuildInvalidPostcodes]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [RTT].[BuildInfoSQLDatasets_]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [IDS].[BuildIDSReportingTable]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [RTT].[BuildFCEDataset_]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DM01].[Elective_WL_Breaches]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildLKPasPCTOdsCCG]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildLKConsultantSpecialty]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [RTT].[BuildIPWLDataset_]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[BuildMedianTheatreTimesTables]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildPASReferenceValue]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [RTT].[BuildOutpatientDataset_]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [RTT].[BuildNotesTable_]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [SC].[vwTransplantsTemp]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[DefaultDateStartEnd_6Month]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[DefaultDate_FinancialYTD]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwExcludeWAWardsandClinics]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [APC].[vwECMODays]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [QLIK].[vwDPR_HealthRoster]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[ReportStrokes]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportStrokeActivity2016]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportStrokeActivity2015]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CAN].[ReportStraightToTest]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[ReportSSRSReportHistory]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[ReportSmartboardSSABreach]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportSessionTheatrePAR]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreDelaySummaryDrillThrough]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreDelaySummary]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreBookingandScheduling_TheatrePatientLevel]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreBookingandScheduling_SpecialtyPatientLevel]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreBookingandScheduling]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[ReportTarn]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CAN].[ReportSurgeryTCIDate]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [BLOODTRACK].[ReportSummaryFated_UnfatedTransfusions]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DQ].[ReportUnrecordedAttendanceAndOutcome]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportUnpopulatedTheatreSessions]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [BLOODTRACK].[ReportUnFatedTransfusionsByLocation_PatientLevel]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [BLOODTRACK].[ReportUnFatedTransfusionsByLocation]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [S6].[ReportUndischargedLongLOSandCritCare201516]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportUndischargedLongLoS_CCG]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [S6].[ReportUndischargedLongLOS]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportUndischargedLongLoS]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportTopOutpatientDNAClinics_ApptType]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportTopOutpatientDNAClinics]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TPY].[ReportTherapiesReferrals]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TPY].[ReportTherapiesPhysioContacts]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TPY].[ReportTherapiesInpatientReferrals]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TPY].[ReportTherapiesContacts]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TPY].[ReportTherapiesAttendances]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreTimesAuditSummary]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreTimesAudit]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreSpecialtyPAR]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreKPIConsultantByMonth_EndDate]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreKPIConsultantByMonth_StartDate]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreOpDurationPatientLevel]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreKPIDashboardCancellations]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreKPIDashboard_Theatres]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [MM].[ReportWeekendWardRoundPilot]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[uspExtractCodingDataForEasyAuditTestCB]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[uspExtractCodingDataForEasyAuditNoCasenote2016]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[uspExtractCodingDataForEasyAuditNoCasenote]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[uspExtractCodingDataForEasyAudit2016]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[uspExtractCodingDataForEasyAudit]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[uspElectiveWaitingListNotRTT_SpecialtyList]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[uspElectiveWaitingListNotRTT_PTLCommentList]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[uspElectiveWaitingListNotRTT_PriorityList]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[uspElectiveWaitingListNotRTT_ConsultantList]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[uspElectiveWaitingListNotRTT]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[uspCriticalCareWardReadmissionRates]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[uspCheckIceFilingReport]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[uspAdhoc_MichaelaSchofied_20160212]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[Rolling6Months_DefaultDates]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[Rolling13Months_DefaultDates]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [INFO].[Stroke]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [COM].[vwAQPPodiatry]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [SC].[vwAPCSpecialtyFunctionCorrection]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [INFO].[vwAPCElectiveAdmissions2013]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [INFO].[vwAPCElectiveAdmissions201213]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [SC].[vwAPCCardiacConsultantSpec]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [AE].[vwAmbulanceDailyMDS]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [COM].[vwActivityInLast12Months]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [COM].[vwActivity]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vw_AEFeedforProdacapo]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vw_AEAttendType1]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [CHKS].[vwBalancedScorecardRAMI_N]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [CHKS].[vwBalancedScorecardRAMI_D]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwLongStayMPI]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwInvalidPostcodes]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [QLIK].[vwICEReports]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwICEPathRequests]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [MM].[vwMedicalModelWardsDirectorates]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [MM].[vwMedicalModelDashboardMedicalWards]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [DM01].[ElectiveWaitingList]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportAppraisalInpatients]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportAppraisalConsultantList]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportAppointmentTotalByClinician_DrillThrough]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportAppointmentTotalByClinician]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportAllergyDataForFinance]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CAN].[ReportAll2wwBreaches]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[ReportAlertsReport]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [AE].[ReportAlcoholScreenRate]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [AE].[ReportAlcoholMonitoring]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [AE].[ReportAlcoholConsultants]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [AE].[ReportAlcohol]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportAHPRTT]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [RTT].[ReportAHPBreachSummary_DrillThrough]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [RTT].[ReportAHPBreachSummary]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [IDS].[ReportAEDischargeSummariesByPractice]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [AE].[ReportAEAttendancesByTriage_MainReportTrafford]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [AE].[ReportAEAttendancesByTriage_MainReport]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DM01].[Radiology_WL_Breaches]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [DM01].[QuarterlyEndoscopyWaitingList]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [DM01].[QuarterlyCardiologyWaitingList]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportCritCareCTCUandECMOCareLevelTEST]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportCritCareCTCUandECMOCareLevel]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportCRISCapsuleEndoscopy]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportCountryofBirth]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportConsultantAppraisal_AdmissionMethod]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[ReportColposcopyReadmissions]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DQ].[ReportCodingStatus]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Coding].[ReportCodingQualityImprovementsDashboard]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Coding].[ReportCodingProductivityReport_DrillThroughEpisodes]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Coding].[ReportCodingProductivityReport]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[ReportCodedActivityByConsultant]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[ReportClinicalCodingDrillThrough]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DQ].[ReportCasenoteTrackingForSpecficUser]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TPY].[ReportCardiacPhysio]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportCapsuleEndoscopy]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CAN].[ReportCancerTypePAR]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CAN].[ReportCancerSurgery_CancerSite]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CAN].[ReportCancerSitePAR_DiagnosisDate]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CAN].[ReportCancerSitePAR]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CAN].[ReportCancerProcedurePAR]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CAN].[ReportCancerProcedure]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CAN].[ReportCancerOrganisationPAR]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CAN].[ReportCancerFirstSeenOrganisationcodePAR]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportCancerAdditionsToWL_CancerType]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportCancerAdditionsToWL]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportMaternityAnteNate_2016]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportMaternityAnteNate_2014]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportMaternityAnteNate]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DQ].[ReportMarriedMinors]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportMajorTraumaActivity2016]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportMajorTraumaActivity2015]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportMajorTraumaActivity_MissingSpellNumbers2016]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportMajorTraumaActivity_MissingSpellNumbers2015]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[ReportLOSEscalationProcessReviewerList]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[ReportLOSEscalationProcess]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportLoSConsultantTracker_WkEnding]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportBedModellingByTimeOfDay]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportBedModelling_PARSpec]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportBedModelling_PARPatClass]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TPY].[ReportAverageContactDuration]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [AE].[ReportAttendance_Breaches]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DQ].[ReportApptsWithNoPurchaser]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportDailyStartsandFinishesProductivity]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportDailyNonAdmitted_Specialty_DropdownList]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportDailyNonAdmitted_Specialty]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportDailyNonAdmitted_LastAttendedAptStatus]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportDailyMonthlyAllBreaches_Specialty_DropdownList]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportDailyMonthlyAllBreaches_Specialty]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportDailyHSCOutpatients]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportDailyDiagnosticWL]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportDailyAdmitted_Specialty_Original]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportDailyAdmitted_Specialty_DropdownList]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportDailyAdmitted_Specialty]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportDailyAdmitted_PTLComments]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportDailyAdmitted_Priority]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportDailyAdmitted_PainManagement]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportDailyAdmitted_Consultant]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportDailyAdmitted_BreachDate]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [LPI].[ReportCurrentLodgers_PatientLevel]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [LPI].[ReportCurrentLodgers]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[ReportCurrentIpatientLOS14Days]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportCTActivity_SLAM_2016]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportCTActivity_SLAM]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportLegacyStatus]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CORP].[ReportKH03_MidnightBedsAndOccupancy]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportIPTransplantExclSpellsbyVisitType_PatLevel2016]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportIPTransplantExclSpellsbyVisitType_PatLevel]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportIPTransplantExclSpellsbyVisitType_2016]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportIPTransplantExclSpellsbyVisitType]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DQ].[ReportIPsWithNoPurchaser]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportIPSpellsRCodes_2016]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportIPSpellsORMIStoPbR_2016]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportIPSpellsORMIStoPbR]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportIPSpellsDiagLongStayEmerg_2016]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportIPSpellsCodedWithoutCCWithPreviousCC]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportIPSpellByTFCandHRG_TFC]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportIPSpellByTFCandHRG_POD]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportIPSpellByTFCandHRG_HRG]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportIPSpellByTFCandHRG_FinMonth]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportIPSpellByTFCandHRG_Drillthrough]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportIPSpellByTFCandHRG_Cons]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportIPSpellByTFCandHRG]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportIPDCPlannedPainManagement]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportIPDCPlannedList-PLAdmissionTypeNOTPL]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportIPDCPlanned]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DQ].[ReportInvalidPostcode]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportInterventionalRadiologyVAB_2016]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportInterventionalRadiologyVAB]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CAN].[ReportInterTrustTreated]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CAN].[ReportInterTrustTransfer]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportInpatientHRGUCodes_QualityMessage]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportInpatientHRGUCodes_201617_QualityMessage]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportInpatientHRGUCodes_201617]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportInpatientHRGUCodes_201516_QualityMessage]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [BLOODTRACK].[ReportFatedTransfusionsByLocation_PatientLevel]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [BLOODTRACK].[ReportFatedTransfusionsByLocation]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportEurokingMaternityAnteNate]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ETM].[ReportETMActivity_DailyOutpatient_MissingAttend]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ETM].[ReportETMActivity_DailyOutpatient]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ETM].[ReportETMActivity_DailyInpatient]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [USS].[ReportEnhancedRecoveryDashboard_Excel]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [AE].[ReportEmergencyAdmissions]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ETM].[ReportEmergencyAdmissionDischargesNetChange]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[ReportElectiveAdmissionMRSAScreensSummary]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[ReportElectiveAdmissionMRSAScreensPatientLevel]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[ReportEDDUndischarged]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ETM].[ReportEDBreakdownByCCG]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ETM].[ReportEDBreakdownByArrivalTime]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ETM].[ReportEDBreakdownBreachReason]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportDurationofTheatreOpsbyOperationDate]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportDurationofTheatreOpsbyDischargeDate]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportDQEndoPTL]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportDNAdPreOPStillOnWL_Specialty]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportDNAdPreOPStillOnWL_Priority]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportDNAdPreOPStillOnWL_Consultant]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportDNAdPreOPStillOnWL_AdmMeth]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportDNAdPreOPStillOnWL]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [IDS].[ReportDischargeSummariesBySentCategory_PatientLevel]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [IDS].[ReportDischargeSummariesBySentCategory]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [IDS].[ReportDischargeSummariesByPractice]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportDirectorateReportProductivityUtilisation]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportDemandElectiveIP_InpatientType]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportDemandElectiveIP_EndDatesDefault]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportDemandElectiveIP_EndDates]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportDemandElectiveIP_CompYearDefault]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportDemandElectiveIP_CompYear]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportDemandElectiveIP]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportDefaultTheatres]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportDefaultOperationSuite]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CAN].[ReportDeathsWithin30DaysOfChemo]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportDayCasesByWard]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ESR].[ReportDateStamp]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [COM].[ReportDataCompliancesPatients]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [COM].[ReportDataCompliances]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportDailyWaitingListAdditionsBySpecialty]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportDailyWaitingListAdditions]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportDailyTheatrePerformanceSummary]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOpenReferralsByCategory]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOpenReferrals_Summary]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOpenReferrals_MonthsList]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOpenReferrals_DischargedWithoutFutureActivity]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOpenReferrals]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportOPClinicPatientsNotSeenLast3Months]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportOPAttendancesInFlexButNotFreeze_Drillthrough]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportOPAttendancesInFlexButNotFreeze]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOPActiveWLBlackHole_Specialty]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOPActiveWLBlackHole_Consultant]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOPActiveWLBlackHole]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[ReportNHSChoicesDaycaseIndicators]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportMortalityWeeklyUncodedDeaths_Spec]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportMortalityWeeklyUncodedDeaths_CodedNotCoded]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportMortalityWeeklyUncodedDeaths]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportMortalityDailyCodingCompleted]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportMortalityDaily_ReportDate]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CAN].[ReportMorphology_morphology]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CAN].[ReportMorphology]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [MM].[ReportMonthDropdowns]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [MM].[ReportMedicalWardsDashboard_DrillThroughReadm72Hrs]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [MM].[ReportMedicalWardsDashboard_DrillThroughReadm30Days]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [MM].[ReportMedicalWardsDashboard_DrillThroughNightTimeWardMoves]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [MM].[ReportMedicalWardsDashboard_DrillThroughMoreThan3Wards]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [MM].[ReportMedicalWardsDashboard]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [MM].[ReportMedicalWards_Ward]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [MM].[ReportMedicalModelAMUDashboard_DrillThrough72Hrs]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [MM].[ReportMedicalModelAMUDashboard_DrillThrough30Days]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [MM].[ReportMedicalModelAMUDashboard]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [MM].[ReportMedicalModelAMU_Ward]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportMedatronicCardiologyWL]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportMaternityPostNate_2016]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportMaternityPostNate_2014]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportMaternityPostNate]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportPOAUActivityReport_2016]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportPOAUActivityReport_2015]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportPOAUActivityReport]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Coding].[ReportPneumoniaTracker]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [IDS].[ReportPercentageofJACDischargeSummariesCreated_PatientLevel]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [IDS].[ReportPercentageofJACDischargeSummariesCreated]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportPbRExclusions2015_TFC]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportPbRExclusions2015_Drillthrough]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportPbRExclusions2015_CCG]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportPbRExclusions2015]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportPbRExclusions2014_TFC]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportPbRExclusions2014_Drillthrough]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportPbRExclusions2014_CCG]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportPbRExclusions2014]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportPbRExclusions_TFC]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportPbRExclusions_Drillthrough]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportPbRExclusions_CCG]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportPbRExclusions]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DQ].[ReportPasBirthValidationError]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportOverseaWardAttenders_2016]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportOverseaWardAttenders_2015]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportOverseaOutpatients_2016]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportOverseaOutpatients_2015]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportOverseaInpatients_2016]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportOverseaInpatients_2015]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [USS].[ReportOverNightBedUsageByWardAndSpecialty_WARD]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [USS].[ReportOverNightBedUsageByWardAndSpecialty_Spec]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [USS].[ReportOverNightBedUsageByWardAndSpecialty_DrillThrough]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [USS].[ReportOverNightBedUsageByWardAndSpecialty_DIV]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [USS].[ReportOverNightBedUsageByWardAndSpecialty_dir]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [USS].[ReportOverNightBedUsageByWardAndSpecialty]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [RTT].[ReportOutpatientRTTOutcomes]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportOutpatientPullingList_PainClinics]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportOutpatientPullingList]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOutpatientNewHSCWaitingListPTL_Specialty]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOutpatientNewHSCWaitingListPTL_BookingStatus]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOutpatientNewHSCWaitingListPTL]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportSessionBreakdownDrillthoughChart]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportSessionBreakdownDrillthough]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportScheduledAdmissionsReport_WLType_v2]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportScheduledAdmissionsReport_WLType]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportScheduledAdmissionsReport_Wards_Secondary]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportScheduledAdmissionsReport_Wards_AdmittingFacility]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportScheduledAdmissionsReport_Wards]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportScheduledAdmissionsReport_v2]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportScheduledAdmissionsReport_TCIDATE_v2]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportScheduledAdmissionsReport_TCIDATE]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportScheduledAdmissionsReport_Specialty]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportScheduledAdmissionsReport_PROMS_v2]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportScheduledAdmissionsReport_PROMS]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportScheduledAdmissionsReport_Procedures_v2]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportScheduledAdmissionsReport_Procedures]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportScheduledAdmissionsReport_IntendedManagement_v2]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportScheduledAdmissionsReport_IntendedManagement]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportScheduledAdmissionsReport_Gender_v2]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportScheduledAdmissionsReport_Gender]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportScheduledAdmissionsReport_Cancer_V2]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportScheduledAdmissionsReport_Cancer]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportScheduledAdmissionsReport]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportSaLTContacts]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportRTTOutpatientNew90Codes]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportRTTOutpatient90Codes_Details]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportRTTOutpatient90Codes]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportRTT99And36Codes_Specialty_DropDownList]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportRTT99And36Codes_RTTOutcome_DropDownList]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportRTT99And36Codes]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportRTT32Codes]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [CAN].[ReportRM3FlaggedSCRReferrals]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [RF].[ReportRM3FlaggedBreastSurgeryReferrals]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [S6].[ReportReferralsDataset_WelshPatients]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [S6].[ReportReferralsDataset]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [USS].[ReportReadmissionbyDischargeWard_Specialty]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [USS].[ReportReadmissionbyDischargeWard_PatClass]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [USS].[ReportReadmissionbyDischargeWard_DrillThrough]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [USS].[ReportReadmissionbyDischargeWard_Division]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [USS].[ReportReadmissionbyDischargeWard_Directorate]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [USS].[ReportReadmissionbyDischargeWard]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [MISYS].[ReportRadiologyWaitingTimeSpans_PatientLevel]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [MISYS].[ReportRadiologyWaitingTimeSpans]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportOPRefsSchedule5ContractDeduplicatedWithoutPCT]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportOPRefsSchedule5ContractDeduplicated_OtherCCGs]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportOPRefsSchedule5ContractDeduplicated]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOPPulseOxDiagnosticPTL]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOPPartialBookingFUPTLSummary_Drillthrough]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOPPartialBookingFUPTLSummary]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOPPartialBookingFUPTLIncorrectList_User]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOPPartialBookingFUPTLIncorrectList_Specialty]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOPPartialBookingFUPTLIncorrectList]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOPPartialBookingFUPTLDifferentApptSpecialty]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOPPartialBookingFUPTLAdditions]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOPPartialBookingFUBookByBreachDate_SummaryOLD]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOPPartialBookingFUBookByBreachDate_Summary]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOPPartialBookingFUBookByBreachDate_PatLevelDrillthroughOLD]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOPPartialBookingFUBookByBreachDate_PatLevelDrillthrough]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportORMISCancellationReport]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOPWaitingListSummary]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOPWaitingListSched5]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[ReportOPWaitingList]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportOPSpellByTFCandHRG_TFC]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportOPSpellByTFCandHRG_POD]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportOPSpellByTFCandHRG_HRG]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportOPSpellByTFCandHRG_Drillthrough]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportOPSpellByTFCandHRG_Cons]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportOPSpellByTFCandHRG]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportPrivatePatients2016]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportPrivatePatients2015]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportPrismRA60Z_2016]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportPrismRA60Z_2015]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[LKMax_CensusDate]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportWardStay]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[Duration of Theatre Ops by Discharge Date]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DQ].[DuplicateAdmissions]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[OPSlotActivity]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[PTL_18wk_Intergrated_tool_v2]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[PTL_18wk_Intergrated_tool_Specialty]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[PTL_18wk_Intergrated_tool_Patient_NonAdmitted]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[PTL_18wk_Intergrated_tool_Patient_Admitted]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[PTL_18wk_Intergrated_tool_Division]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PTL].[PTL_18wk_Intergrated_tool_Directorate]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[OPSessionActivity]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[Next7Days_DefaultDates]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [INFO].[NHSNo1]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[MyTestTableIjaz]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[MyTestTableDID]    Script Date: 07/04/2016 10:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DM01].[Neuro_WL_Breaches]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [INFO].[AdmissionByMethod]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [QLIK].[BuildBedStatusAndOccupancy]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Community].[BuildAQPPodiatry]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildAPCAllProcedures]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildAPCAllDiagnosis]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildSpecialtyDivision]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[BuildSessionTemplateDetail]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[BuildWardStay]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [RTT].[BuildWaitingListV1Dataset_]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [IDS].[BuildAEIDSReportingTable]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [RTT].[BuildAdmDischDataset_]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  UserDefinedFunction [dbo].[BSTAdjustment]    Script Date: 07/04/2016 10:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[BuildWaitingList]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TRANSPLANT].[BuildTransplantTables]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [QLIK].[BuildTheatreTables]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [BLOODTRACK].[BuildSummaryFatedUnfatedTransfusionsTable]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[BuildSpell]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[BuildBookingFact_Session]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[BuildBookingFact]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [QLIK].[BuildAEMeasuresTable]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[OPKPI_Specialty]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[OPKPI_SlotsAvailable]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[OPKPI_SessionUtilisation]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportPostOpLosOnCriticalCare_Specialty]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportPostOpLosOnCriticalCare_PatientLevel]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportPostOpLosOnCriticalCare_Division]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportPostOpLosOnCriticalCare_Directorate]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportPostOpLosOnCriticalCare_Consultant]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportPostOpLosOnCriticalCare_CardiacbyMonth]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportPostOpLosOnCriticalCare_AdmissionMethod]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportPostOpLosOnCriticalCare]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportOPKPIByConsultant]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportOPKPIByClinic_NewandReviewPrevYear]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportOPKPIByClinic_NewandReview]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportOPKPIByClinic_DNAs_Consultants]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportOPKPIByClinic_DNAs]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportOperationsWithoutSessionUniqueID_Theatre]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportOperationsWithoutSessionUniqueID_Specialty]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportOperationsWithoutSessionUniqueID_OperationType]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportOperationsWithoutSessionUniqueID_OperatingSuite]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportOperationsWithoutSessionUniqueID]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportOutpatientActivityByConsultant]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [PbR].[ReportMaternityDatingScan]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportDailyTheatrePerformance]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[ReportCriticalCareBedDaysSummaryV2]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[ReportCriticalCareBedDaysSummary]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[ReportCriticalCareBedDays]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportAppraisalOutpatients]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportBedModelling_PARHourofDay]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [INFO].[vwTrustCancellationsByPriority]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [INFO].[vwTrustCancellations]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [OP].[vwMaternityVMSRAppointmentsNew]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

/****** Object:  View [OP].[vwMaternityVMSRAppointments]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

/****** Object:  View [dbo].[vwICEDirectorateReportsFiledwithSpec]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [ICE].[vwICEDirectorateReportsFiledbySpecialtyAndLocation]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [ICE].[vwICEDirectorateReportsFiledbySpecialty]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [CHKS].[vwBalancedScorecardRAMI]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [DQ].[ReportVTEAssessmentsNotComplete]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreKPIDashboard]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreKPIConsultantByMonthActivity]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreKPIConsultantByMonth_Theatre]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreKPIConsultantByMonth_Specialty]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreKPIConsultantByMonth_OpSuite]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreKPIConsultantByMonth]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreSessionSummaryReportByConsultant_PatientLevel]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreSessionSummaryReportByConsultant_Consultant]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [TH].[ReportTheatreSessionSummaryReportByConsultant]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[ReportStrokesSummary]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[ReportSessionsUtilised52Weeks]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [SC].[vwDNAsLast7Days]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [INFO].[vwDNARateForETMReport]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Prodacapo].[vwDNARate]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [INFO].[vwDNARate]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OPD].[SlotSessionUtil]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [TPY].[vwTherapiesOPDirectorateReport]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildLKPatientCurrentDetails]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [OP].[AppointmentStatusUpdate]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [TPY].[vwTherapiesContactsDirectorateReport]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [OP].[vwSessionSlotType]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [INFO].[vwSlotUtilisationForOPBoard]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [INFO].[vwSlotUtilisation]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [OP].[vwSlotTypeQlikView]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [INFO].[vwNewReviewRatio]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [COM].[UpdateSchedules]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[UpdateOPSlotUtilisation]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[UpdateOPDNA]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[OP_DNA_Rates]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[OP_DNA_Burns_Patients]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[OP_Canmcellations_Burns_Patients]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[OP_Cancellation_Rates]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportOPSessionSlots]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Prodacapo].[FinanceExportPathologyICERequestsB]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[BuildScheduleTest]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[BuildSchedule]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [RF].[BuildReferralToTreatment]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[BuildReferralToTreatment]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[BuildReferralToTreatment]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [RF].[BuildReferral]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [RF].[BuildPatient]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[BuildPatient]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[BuildPatient]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[MedtronicAudit]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[OPKPI_NewtoReview1011]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[OPKPI_NewandReview]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[OPKPI_LessThan6WksCancel]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[OPKPI_HospitalCancellations]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[OPKPI_DNAs]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[OPKPI_Directorate]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[OPKPI_Consultant]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[OPKPI_Clinics]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [ICE].[vwICEDirectorateReportsFiledbyLocationCriticalCare]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [ICE].[vwICEDirectorateReportsFiledbyLocationAE]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [ICE].[vwICEDirectorateReportsFiledbyLocation]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwICEDirectorateReportsFiled]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ETM].[ReportHospitalOccupancy]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ICE].[ReportICEFilingByDirectorateSpecialtyConsultant2]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ICE].[ReportICEFilingByDirectorateSpecialtyConsultant_PatientLevel2]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [SC].[vwBedModellingOccupancy]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Prodacapo].[vw_SlotUtilisation]    Script Date: 07/04/2016 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vw_OPSessionandSlotFeedforProdacapo]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vw_ICEPathRequestsForProdacapo]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[TableLineageMap]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[ConnectionsMapping]    Script Date: 07/04/2016 10:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [OP].[DNAsOPKPI]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[DNAsLast7Days]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [COM].[UpdateCommunityDataWHReporting]    Script Date: 07/04/2016 10:14:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[DNAsLast7Days_Specialty]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[DNAsLast7Days_Division]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[DNAsLast7Days_Directorate]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[DNAsLast7Days_Clinics]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ICE].[ReportICEFilingByDirectorateSpecialtyConsultant_PatientLevel]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ICE].[MDTReferralsForLungCancer]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [QLIK].[ExportAEMeasuresTable]    Script Date: 07/04/2016 10:14:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildWHReporting_OP]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildWHReporting_APC]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildReportingModel]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ICE].[HIVScreenICERequestsFromACU]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ICE].[ReportListOfICEReportsWithAbnormalFlag2]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ICE].[ReportListOfICEReportsWithAbnormalFlag]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportBedModellingByDayofWeek]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [SC].[ReportBedModellingByDateAndYear]    Script Date: 07/04/2016 10:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ICE].[ReportBronchoscopyRequests]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ICE].[ReportBreastPreoperativeProceduresRequested]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[QARCopyOfMAR]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildWHReporting]    Script Date: 07/04/2016 10:14:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
