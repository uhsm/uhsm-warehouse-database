﻿CREATE TABLE [TARN].[lkTARNCodes](
	[Code] [nvarchar](255) NULL,
	[ICD10 Codes + Descriptions] [nvarchar](255) NULL,
	[Comments] [nvarchar](255) NULL
) ON [PRIMARY]