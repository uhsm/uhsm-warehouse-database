﻿-- =============================================
-- Author:		CMan
-- Create date: 06/01/2015
-- Description:	Stored Procedure to be run at the end of the Theatres build to create tables to be used in the Theatre Booking and Scheduling Tool Report.
--				Tables created at the end of the buld are:
--					-Th.TheatreAnaestheticTimes - this is a table showing the median anaesthetic times by Anaesthetist and Procedure for the last rolling 12 months
--					-Th.TheatreAnaestheticTimesByProcedureOnly - this is a table showing the median anaesthetic times by Procedure for the last rolling 12 months to be used in the report query later as a catch all for ops which have been booked with an Anaesthetist and Procedure that does not have a median time in the last 12 months
--					-Th.MedianProcedureTimes - this is a table showing the median procedure times by session consultant and Procedure for the last rolling 12 months 
-- =============================================
CREATE PROCEDURE [TH].[BuildMedianTheatreTimesTables]

	-- Add the parameters for the stored procedure here



AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()




-- Declare Start and End Times for the last rolling twelve full month period build of the median Theatre Procedure and Anaesthetics times table build
Declare @start datetime = DateAdd(month, -12,Cast('01/' + Datename(month, getdate()) + '/'+ Datename(year,getdate()) AS DATETIME)),
@end datetime =  Cast('01/' + Datename(month, getdate()) + '/' + Datename(year,getdate()) AS DATETIME)

/******************************************
--Base Episodes table - those episodes where the end date is within the time period as specified above.
******************************************/
If OBJECT_ID('tempdb..#EP') > 0
DROP TABLE #EP
SELECT distinct Episode.SourcePatientNo
	,Episode.SourceSpellNo
	,Episode.EpisodeUniqueID
	,Episode.FacilityID
	,Episode.AdmissionDateTime
	,Episode.DischargeDateTime
	,Episode.EpisodeStartDateTime
	,Episode.EpisodeEndDateTime
	,Left(Episode.PrimaryProcedureCode,3) as PrimaryProcedureCodeGrouped --extract the left 3 of the primary procedure code as matching back to the get median times for the eventual report will be based on the 3 digit code to get more matches
 	,Episode.PrimaryProcedureCode
	,cc.PriamryProcedure as PrimaryProcedure
	,Episode.PrimaryProcedureDateTime
	,Episode.ConsultantCode
	,Episode.ConsultantName
	,Episode.AdmissionType
	,spell.AdmissionMethodNHSCode
	,Spell.AdmissionMethod
	,Spell.AdmissionWard
into #EP
FROM WHREPORTING.APC.Episode
LEFT OUTER JOIN WHREPORTING.APC.Spell on Episode.SourceSpellNo = Spell.SourceSpellNo
LEFT OUTER JOIN [WHREPORTING].[APC].[ClinicalCoding] CC
on	Episode.EncounterRecno = CC.EncounterRecno
WHERE EpisodeEndDate >= @start
	AND EpisodeEndDate <= @end
ORDER BY SourceSpellNo


/******************************************
--Min Operation Details table to get the first op which falls within the spell admission and discharge date as for Elective ops, this should be the one which the patient was booked in for
--Also min on Episode number as there may be more than one in a spell but the one under which the op relates to should be the first one as this is what the patient came in for
********************************************/
If OBJECT_ID('tempdb..#MinEPOP') > 0
Drop table #MinEPOP
Select #EP.#EP.sourcePatientno,
#EP.sourceSpellNo,
min(#EP.EpisodeUniqueID) MinEPID,--to get the first episode in the spell - this should be the one which relates to the op for Elective admissions
#EP.FacilityId,
#EP.AdmissionDateTime,
#EP.DischargeDateTime,
#EP.AdmissionWard,
#EP.AdmissionType,
#EP.AdmissionMethodNHSCode,
#EP.AdmissionMethod ,
min(op.SourceUniqueID) as OpDetailSourceUniqueID--to get the first op in the spell - this should be the Elective one.

into #MinEPOP
from #EP
Left outer join (Select *
from WH.Theatre.OperationDetail
where OperationCancelledFlag <> 1) op 
ON #EP.FacilityID = OP.DistrictNo and OP.OperationStartDate >= #EP.AdmissionDateTime and OP.OperationStartDate < #EP.DischargeDateTime -- and #EP.PrimaryProcedureDateTime = OP.OperationDate
where AdmissionMethodNHSCode in ('11','12','13')

Group by #EP.sourcePatientno,
#EP.sourceSpellNo,
#EP.FacilityId,
#EP.AdmissionDateTime,
#EP.DischargeDateTime,
#EP.AdmissionWard,
#EP.AdmissionType,
#EP.AdmissionMethodNHSCode,
#EP.AdmissionMethod
Order by #EP.SourceSpellNo


/************************************************
Create Final Table with calculated Operation  and anaesthetic durations by Primary Procedure Grouped
************************************************/
If OBJECT_ID('tempdb..#Final') > 0
Drop table #Final
Select distinct  #MinEPOP.*,
#EP.EpisodeStartDateTime,
#EP.EpisodeEndDateTime,
#EP.PrimaryProcedureCode PASPrimaryProcedureCode,
#EP.PrimaryProcedureCodeGrouped PrimaryProcedureCodeGrouped,
#EP.PrimaryProcedure PASPrimaryProcedureDescription,
#EP.PrimaryProcedureDateTime,
#EP.ConsultantCode EpisodeConsultantCode,
#EP.ConsultantName EpisodeConsultant,
op.InAnaestheticTime,
op.AnaestheticInductionTime,
op.AnaestheticReadyTime,
DATEDIFF(minute, op.OperationStartDate, op.OperationEndDate) as OpDuration ,
DATEDIFF(minute, op.AnaestheticInductionTime, op.AnaestheticReadyTime) as AnaestheticDuration,
op.OperationStartDate,
op.OperationEndDate,
op.InRecoveryTime,
pd.ProcedureDescription as ORMISProcedureDescription,
ORMISOpDesc.CD_CODE as ORMISProcedureCode, 
cons.Forename + ' ' + cons.Surname as OperationDetailConsultant,
cons.StaffCode1 as OperationDetailConsultantCode,
anaes.Forename + ' ' + anaes.Surname as Anaesthetist,
anaes.StaffCode1 as AnaesthetistCode,
AnaestheticTypeCodeSet, 
AnaestheticTypeSet
--op.Anaesthetist1Code opAnaes,pd.Anaesthetist1Code procAnaes

into #Final		
from #MinEPOP
Left outer join #EP on #MinEPOP.MinEPID = #EP.EpisodeUniqueID
Left outer join (Select *
from WH.Theatre.OperationDetail
where OperationCancelledFlag <> 1) op on #MinEPOP.OpDetailSourceUniqueID = op.SourceUniqueID
Left outer join (Select * from WH.Theatre.ProcedureDetail where PrimaryProcedureFlag = 1) pd--add criteria to only select those where the primary procedure is 1 as there appear to be cases where there is more than one procedure detail record per op.  Primary Procedure Flag seem to remove the duplicate issue.  Need to join to Procedure Detail to get the Anaesthetist1Code to replace any Anaesthetist1Code in Op Details which are 0
 on op.SourceUniqueID = pd.OperationDetailSourceUniqueID
Left outer join WH.Theatre.Staff cons on op.ConsultantCode = cons.StaffCode
Left outer join WH.Theatre.Staff anaes on case when op.Anaesthetist1Code = 0 then pd.Anaesthetist1Code Else op.Anaesthetist1Code End = anaes.StaffCode -- case when on op.Anaesthetist1Code the pd.Anaesthetist1Code is used because in some ops this is shoing as 0 for the record when there is an anaesthestist showing on the front end. Anaesthetist1Code from the Procedure details is more complete - not sure why
Left outer join  [ORMIS].[dbo].[FCDOPER]  ORMISOpDesc on pd.ProcedureCode = ORMISOpDesc.CD_SEQU




--Median op procedure duration temp table 
--SQL statement to get the median per Operation Consultant and Episode Primary Procedure 
If OBJECT_ID ('tempdb..#MedianProc')> 0
Drop table #MedianProc
SELECT OperationDetailConsultantCode
	,OperationDetailConsultant
	,PrimaryProcedureCodeGrouped
	,AVG(1.0 * OpDuration) as MedianOperationMinutes
	,numrows as NumberOfCases
into #MedianProc
FROM (
	SELECT OperationDetailConsultantCode
		,
		OperationDetailConsultant
		,PrimaryProcedureCodeGrouped
		,OpDuration
		,row_number() OVER (
			PARTITION BY OperationDetailConsultant
		,PrimaryProcedureCodeGrouped ORDER BY OpDuration
			) AS rownum
		,count(*) OVER (
			PARTITION BY  OperationDetailConsultant,
		PrimaryProcedureCodeGrouped 
			) AS numrows
	FROM #Final
	where OpdetailSourceUniqueId is not null
	) AS x
WHERE rownum IN (
		(numrows + 1) / 2
		,(numrows + 2) / 2
		)
Group By OperationDetailConsultantCode
		,OperationDetailConsultant
		,PrimaryProcedureCodeGrouped
		,numrows
	
	
--Median Anaesthetic Time Temp table 
--SQL statement to get the median per anaesthetic Consultant and Episode Primary Procedure  for non LA procedures
If OBJECT_ID ('tempdb..#AnaesMedian')> 0
Drop table #AnaesMedian
SELECT AnaesthetistCode
	,Anaesthetist
	,PrimaryProcedureCodeGrouped
	,AVG(1.0 * AnaestheticDuration) as MedianAnaestheticMinutes
	,numrows as NumberOfCases
into #AnaesMedian
FROM (
	SELECT AnaesthetistCode
		,Anaesthetist
		,PrimaryProcedureCodeGrouped
		,AnaestheticDuration
		,AnaestheticTypeCodeSet
		,row_number() OVER (
			PARTITION BY Anaesthetist
		,PrimaryProcedureCodeGrouped ORDER BY AnaestheticDuration
			) AS rownum
		,count(*) OVER (
			PARTITION BY  Anaesthetist,
		PrimaryProcedureCodeGrouped 
			) AS numrows
	FROM #Final
	where OpdetailSourceUniqueId is not null
	and (AnaestheticTypeCodeSet not like '%LA%' or AnaestheticTypeCodeSet IS NULL)
	) AS x
WHERE rownum IN (
		(numrows + 1) / 2
		,(numrows + 2) / 2
		)
Group By AnaesthetistCode
		,Anaesthetist
		,PrimaryProcedureCodeGrouped
	,numrows
	

--SQL statement to get the median per anaesthetic Consultant and Episode Primary Procedure  for  LA procedures only
If OBJECT_ID ('tempdb..#AnaesMedianLA')> 0
Drop table #AnaesMedianLA
SELECT AnaesthetistCode
	,Anaesthetist
	,PrimaryProcedureCodeGrouped
	,AVG(1.0 * AnaestheticDuration) as MedianAnaestheticMinutes
	,numrows as NumberOfCases
into #AnaesMedianLA
FROM (
	SELECT AnaesthetistCode
		,Anaesthetist
		,PrimaryProcedureCodeGrouped
		,AnaestheticDuration
		,AnaestheticTypeCodeSet
		,row_number() OVER (
			PARTITION BY Anaesthetist
		,PrimaryProcedureCodeGrouped ORDER BY AnaestheticDuration
			) AS rownum
		,count(*) OVER (
			PARTITION BY  Anaesthetist,
		PrimaryProcedureCodeGrouped 
			) AS numrows
	FROM #Final
	where OpdetailSourceUniqueId is not null
	and AnaestheticTypeCodeSet  like '%LA%'
	) AS x
WHERE rownum IN (
		(numrows + 1) / 2
		,(numrows + 2) / 2
		)
Group By AnaesthetistCode
		,Anaesthetist
		,PrimaryProcedureCodeGrouped
	,numrows
	

	
--Median Anaesthetic Time Temp table 2 
--SQL statement to get the median anaesthetic time per Episode Primary Procedure only for GA and Others excl LA - to be used as a catch all for any ops which cannot be matched back to the first anaesthetic median times table above by anaesthetist and procedire
If OBJECT_ID ('tempdb..#AnaesMedian2')> 0
Drop table #AnaesMedian2
SELECT PrimaryProcedureCodeGrouped
	,AVG(1.0 * AnaestheticDuration) as MedianAnaestheticMinutes
	,numrows as NumberOfCases
into #AnaesMedian2
FROM (
	SELECT PrimaryProcedureCodeGrouped
		,AnaestheticDuration
		,row_number() OVER (
			PARTITION BY PrimaryProcedureCodeGrouped ORDER BY AnaestheticDuration
			) AS rownum
		,count(*) OVER (
			PARTITION BY  PrimaryProcedureCodeGrouped 
			) AS numrows
	FROM #Final
	where OpdetailSourceUniqueId is not null
	and (AnaestheticTypeCodeSet not like '%LA%' or AnaestheticTypeCodeSet IS NULL)
	) AS x
WHERE rownum IN (
		(numrows + 1) / 2
		,(numrows + 2) / 2
		)
Group By PrimaryProcedureCodeGrouped
	,numrows
	



--SQL statement to get the median anaesthetic time per Episode Primary Procedure only for LA's only- to be used as a catch all for any ops which cannot be matched back to the first anaesthetic median times table above by anaesthetist and procedire
If OBJECT_ID ('tempdb..#AnaesMedian2LA')> 0
Drop table #AnaesMedian2LA
SELECT PrimaryProcedureCodeGrouped
	,AVG(1.0 * AnaestheticDuration) as MedianAnaestheticMinutes
	,numrows as NumberOfCases
into #AnaesMedian2LA
FROM (
	SELECT PrimaryProcedureCodeGrouped
		,AnaestheticDuration
		,row_number() OVER (
			PARTITION BY PrimaryProcedureCodeGrouped ORDER BY AnaestheticDuration
			) AS rownum
		,count(*) OVER (
			PARTITION BY  PrimaryProcedureCodeGrouped 
			) AS numrows
	FROM #Final
	where OpdetailSourceUniqueId is not null
	and AnaestheticTypeCodeSet  like '%LA%'
	) AS x
WHERE rownum IN (
		(numrows + 1) / 2
		,(numrows + 2) / 2
		)
Group By PrimaryProcedureCodeGrouped
	,numrows
	


--Median op procedure duration temp table 2
--SQL statement to get the median per Operation By Episode Primary Procedure only - to be used for catch all for any ops which cannot be matched back to a consultant and prim proc op
If OBJECT_ID ('tempdb..#MedianProc2')> 0
Drop table #MedianProc2
SELECT PrimaryProcedureCodeGrouped
	,AVG(1.0 * OpDuration) as MedianOperationMinutes
	,numrows as NumberOfCases
into #MedianProc2
FROM (
	SELECT PrimaryProcedureCodeGrouped
		,OpDuration
		,row_number() OVER (
			PARTITION BY PrimaryProcedureCodeGrouped ORDER BY OpDuration
			) AS rownum
		,count(*) OVER (
			PARTITION BY  PrimaryProcedureCodeGrouped 
			) AS numrows
	FROM #Final
	where OpdetailSourceUniqueId is not null
	) AS x
WHERE rownum IN (
		(numrows + 1) / 2
		,(numrows + 2) / 2
		)
Group By PrimaryProcedureCodeGrouped
		,numrows



--Insert data into the Median Procedure Times table
TRUNCATE TABLE WHReporting.TH.TheatreProcedureTimes

INSERT INTO WHReporting.TH.TheatreProcedureTimes
SELECT DISTINCT #medianProc.OperationDetailConsultantCode
	,#medianProc.OperationDetailConsultant
	,#medianProc.PrimaryProcedureCodeGrouped
	,cb.Description AS PrimaryProcedureDescription
	,#medianProc.MedianOperationMinutes
	,#medianProc.NumberOfCases
FROM #medianProc
LEFT OUTER JOIN (
	SELECT *
	FROM [WH].[PAS].[CodingBase]
	WHERE CCSXT_CODE = 'OPCS4'
		AND END_DTTM IS NULL
	) cb ON #medianProc.PrimaryProcedureCodeGrouped = cb.CODE





--INSERT data INTO the Median Anaesthetic Times TABLE - GAs and other only

TRUNCATE TABLE WHREPORTING.TH.TheatreAnaestheticTimes
INSERT INTO WHREPORTING.TH.TheatreAnaestheticTimes
SELECT DISTINCT #AnaesMedian.AnaesthetistCode
	,#AnaesMedian.Anaesthetist
	,#AnaesMedian.PrimaryProcedureCodeGrouped
	,cb.Description AS PrimaryProcedureDescription
	,#AnaesMedian.MedianAnaestheticMinutes
	,#AnaesMedian.NumberOfCases
FROM #AnaesMedian
LEFT OUTER JOIN (
	SELECT *
	FROM [WH].[PAS].[CodingBase]
	WHERE CCSXT_CODE = 'OPCS4'
		AND END_DTTM IS NULL
	) cb ON #AnaesMedian.PrimaryProcedureCodeGrouped = cb.CODE



--INSERT data INTO the Median Anaesthetic Times TABLE - LA's only

TRUNCATE TABLE WHREPORTING.TH.TheatreAnaestheticTimesLA
INSERT INTO WHREPORTING.TH.TheatreAnaestheticTimesLA
SELECT DISTINCT #AnaesMedianLA.AnaesthetistCode
	,#AnaesMedianLA.Anaesthetist
	,#AnaesMedianLA.PrimaryProcedureCodeGrouped
	,cb.Description AS PrimaryProcedureDescription
	,#AnaesMedianLA.MedianAnaestheticMinutes
	,#AnaesMedianLA.NumberOfCases
FROM #AnaesMedianLA
LEFT OUTER JOIN (
	SELECT *
	FROM [WH].[PAS].[CodingBase]
	WHERE CCSXT_CODE = 'OPCS4'
		AND END_DTTM IS NULL
	) cb ON #AnaesMedianLA.PrimaryProcedureCodeGrouped = cb.CODE



--Select * from #Final 
--where AnaestheticTypeCodeSet  like '%LA%'
----(AnaestheticTypeCodeSet not like '%LA%' or  AnaestheticTypeCOdeSet IS  NULL)
--and OpdetailSourceUniqueId  is not null
--and PrimaryProcedureCodeGrouped = 'L29'
--and AnaesthetistCode = 'C1617759'
--order by anaestheticduration

--Select  * from #AnaesMedian

--Insert data into the Median Anaesthetic Times By Procedure Only table - GAs and other anaesthetic types

TRUNCATE TABLE WHReporting.TH.TheatreAnaestheticTimesByProcedureOnly
INSERT INTO WHReporting.TH.TheatreAnaestheticTimesByProcedureOnly
SELECT DISTINCT #AnaesMedian2.PrimaryProcedureCodeGrouped
	,#AnaesMedian2.MedianAnaestheticMinutes
	,#AnaesMedian2.NumberOfCases
FROM #AnaesMedian2
LEFT OUTER JOIN (
	SELECT *
	FROM [WH].[PAS].[CodingBase]
	WHERE CCSXT_CODE = 'OPCS4'
		AND END_DTTM IS NULL
	) cb ON #AnaesMedian2.PrimaryProcedureCodeGrouped = cb.CODE



--Insert data into the Median Anaesthetic Times By Procedure Only table - LA's only
TRUNCATE TABLE WHReporting.TH.TheatreAnaestheticTimesByProcedureOnlyLA
INSERT INTO WHReporting.TH.TheatreAnaestheticTimesByProcedureOnlyLA
SELECT DISTINCT #AnaesMedian2LA.PrimaryProcedureCodeGrouped
	,#AnaesMedian2LA.MedianAnaestheticMinutes
	,#AnaesMedian2LA.NumberOfCases
FROM #AnaesMedian2LA
LEFT OUTER JOIN (
	SELECT *
	FROM [WH].[PAS].[CodingBase]
	WHERE CCSXT_CODE = 'OPCS4'
		AND END_DTTM IS NULL
	) cb ON #AnaesMedian2LA.PrimaryProcedureCodeGrouped = cb.CODE




--Insert data into the Median Procedure Times By Procedure only table
TRUNCATE TABLE WHReporting.TH.TheatreProcedureTimesByProcedureOnly
INSERT INTO WHReporting.TH.TheatreProcedureTimesByProcedureOnly
SELECT DISTINCT #medianProc2.PrimaryProcedureCodeGrouped
	,cb.Description AS PrimaryProcedureDescription
	,#medianProc2.MedianOperationMinutes
	,#medianProc2.NumberOfCases
FROM #medianProc2
LEFT OUTER JOIN (
	SELECT *
	FROM [WH].[PAS].[CodingBase]
	WHERE CCSXT_CODE = 'OPCS4'
		AND END_DTTM IS NULL
	) cb ON #medianProc2.PrimaryProcedureCodeGrouped = cb.CODE




Select @RowsInserted = @@ROWCOUNT
select @Elapsed = DATEDIFF(minute, @StartTime,getdate())
select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'TH - WHREPORTING BuildMedianTheatreTimesTables', @Stats, @StartTime






END