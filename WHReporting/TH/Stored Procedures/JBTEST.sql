﻿/*
--Author: Helen Ransome
--Date created: 16/05/2013
--Stored Procedure Built for SSRS Report on:
--Generic store procedure used for Cancer Type Parameter
*/
CREATE Procedure [TH].[JBTEST]


 @StartDate as datetime
 ,@EndDate as datetime
 ,@Specialty as varchar (max)
 ,@Consultant as varchar (max)
 --,@DelayReasonDescriotion as varchar (max)
 
 
as

SELECT 
X.OperationSpecialty
--,X.OperationSpecialtyCode
,X.OperationConsultant
,X.OperationDate
--,X.OperationDate 
,X.DelayReason
,X.SessionType  
,X.SessionDayOfWeek 
--,X.LateStartMinutes 
,COUNT (DISTINCT(x.SourceUniqueID)) as TotalList
,SUM(x.LateStarts) as LateStarts
,1 AS total
FROM

(Select 
OP.SourceUniqueID
,OP.OperationSpecialty 
,OP.OperationSpecialtyCode
,Sess.SessionDirectorate 
,Sess.SessionDivision 
,Sess.SessionTheatre 
,OP.OperationDate 
,OP.OperationConsultant 
,Sess.ActualStartTime 
,Sess.ActualEndTime 
,Sess.SessionStartTime 
,Sess.SessionEndTime  
,Case when OD.DelayReasonDescription in ('Scrub team not available','Anaesthetic support team not available','Radiographer not available','Staff Teaching/Training','All Staff Already Deployed') 
then 'Theatre Staff Issues'
when OD.DelayReasonDescription in ('Anaesthetist At Meeting','Anaesthetist Delayed','Anaesthetist delayed seeing patients','Anaesthetist delayed, awaiting replacement')
then 'Anaesthetic Issues'
when OD.DelayReasonDescription in ('Surgeon At Meeting','Surgeon delayed','Surgeon delayed seeing patient')
then 'Surgical Staff Issues'
when OD.DelayReasonDescription in ('Agreed late start','Previous list overran','Session Management','Additions/Changes to list','Recovery full')
then  'Session Management Issues'
when OD.DelayReasonDescription in ('Awaiting Blood Results','Results not available','Patient having investigations','No Notes','Awaiting operating schedule','Case notes missing','Images not available') 
then 'Information Not Available'
when OD.DelayReasonDescription in ('Surgical equipment not available','Awaiting sterile instrumentation','Equipment','Surgical equipment failure','Anaesthetic equipment failure','Anaesthetic equipment not available','Other equipment failure, eg list/transport')
then 'Equipment Issues'
when OD.DelayReasonDescription in ('Bed Management','Ward bed not available','No ITU bed available','HDU bed not available')
then 'Bed Issues'
when OD.DelayReasonDescription in ('Escort Nurse not available','External delay','Patient Delayed in Transit','Logistical Problems','Porter not available','Fire alarm')
then 'Transit Issues'
when OD.DelayReasonDescription in ('Unidentified patient allergy/infection','Site not marked','Consent not completed','Patient Awaiting Medical Staff Clearance','Patient not prepared','Patient fed','Patient on incorrect ward','Pre med not given','Patient Management')
then 'Preparation Issues'
when OD.DelayReasonDescription in ('Clinical event','Emergency case priority','Previous Patient Returned to Theatre','Return to theatre','Surgical complications','Unplanned events','Prolonged Anaesthesia','anaesthetic complications','Consultant required','Patient unfit for surgery')
then 'Unanticipated Clinical Issues'
 else 'Other Reasons' End AS DelayReason
,OD.DelayReasonDescription
,Sess.SessionType 
,Sess.SessionDayOfWeek 
,Sess.EarlyStartMinutes 
,Sess.LateStartMinutes 
,Sess.EarlyFinishMinutes 
,Sess.LateFinishMinutes 
,OD.DelayMinutes 
,case when DATEDIFF(MINUTE,Sess.SessionStartTime,Sess.ActualStartTime) > 15 then 1 else 0 end as LateStarts
--,1 as total
,OD.DelayComments 

from qlik.TheatreOpDetails OP

left join QLIK.TheatreOperationDelays OD
on OP.SourceUniqueID = OD.OperationDetailSourceUniqueID

left join QLIK.TheatreSessions Sess
on OP.SessionSourceUniqueID = Sess.SessionSourceUniqueID

where 

 OP.OperationDate between @StartDate and @EndDate
 
 and 
 
 OperationTheatreCode in
('1',
'2',
'3',
'6',
'78',
'37',
'38',
'39',
'40',
'14',
'16',
'21',
'13',
'15',
'19',
'20',
'22',
'23'
)

and OperationTypeCode <> 'EMERGENCY'
and DelayReasonDescription IS NOT NULL
 


and OP.OperationSpecialty IN (SELECT Item FROM WHREPORTING.dbo.Split (@Specialty, ','))
and OP.OperationConsultant IN (SELECT Item FROM WHREPORTING.dbo.Split (@Consultant,','))
) AS X
GROUP BY
X.OperationSpecialty
--,X.OperationSpecialtyCode
,X.OperationConsultant
,X.OperationDate
--,X.OperationDate 
,X.DelayReason
,X.SessionType  
,X.SessionDayOfWeek 
--,X.LateStartMinutes