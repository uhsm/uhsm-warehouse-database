﻿CREATE procedure [TH].[ReportDailyAdmitted_PainManagement]

as
/*
Author: Helen Ransome
Date created: 22/10/2015
Description:	[TH].[ReportDailyAdmitted_PainManagement]								
*/

/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/
/*gets last calender week*/
 select distinct
	 min(Cal.TheDate) as FirstDayOfLastWeek
	 ,max(Cal.TheDate) LastDayofLastWeek
	 ,dbo.GetEndOfDay (max(Cal.TheDate)) LastDateTimeofLastWeek
	 ,Cal.FirstDayOfWeek as WkCommencing
	 ,Cal.LastDayOfWeek as WkEnding

into #daterange

FROM LK.Calendar Cal
 where  Cal.FirstDayOfWeek = 
     (select distinct
		SQ.FirstDayOfWeek
		  FROM LK.Calendar SQ
		  where SQ.TheDate = 
		 (
		dateadd(day,-7,cast(GETDATE()as DATE))
		)
		)
 group by
	Cal.FirstDayOfWeek 
	 ,Cal.LastDayOfWeek
	 
/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/

SELECT a.[SpecialtyDescription] as Specialty
      ,a.[Consultant]
      ,a.ConsultantCode
      ,a.[PatientID]
      ,a.[PatientName]
      ,a.[Sex]
      ,a.[Age]
      ,a.[Priority2]
      ,a.[PlannedProcedure1]
      ,a.[GeneralComment]
      ,a.[PatientClass]
      ,a.[ListType]
      ,a.[AdmissionWard]
      ,a.[SecondaryWard]
      ,a.[ClockStartDate]
      ,a.[DateOnList]
      ,a.[LatestRTTCode]
      ,a.[CurrentSuspensionType]
      ,a.[SuspendedEndDate]
      ,a.[FutureSuspensionDate]
      ,a.[FutureSuspensionDays]
      ,a.[RTTWaitNowDays]
      ,a.[RTTWaitNowWeeksAdj]
      ,a.[RTTWaitNowWeeksUnAdj]
      ,a.[RTTBreachDate]
      ,a.[TCIDate]
      ,a.[OperationDate]
      ,isnull(a.[PTLComments],'Unknown')as PTLComments
      ,a.[ELAdmission]    
      ,a.[SpecialtyCode]
      ,a.[ElectiveAdmissionMethod]  
      ,isnull(a.[ReferralPriority],'Unknown') as ReferralPriority   
  FROM [RTT_DW].[PTL].[Admitted] a
  Left join #daterange dr on 1 = 1
  Where
		[SpecialtyDescription] = 'Pain Management' 
		and a.RecordCreatedDate between dr.WkCommencing and dr.WkEnding
  Order by
		SpecialtyDescription Asc,
		Consultant Asc,
		[Priority2] Desc,
	  Cast(RTTBreachDate as DATE) Asc,
	  isnull([PTLComments],'Unknown')
 /*xxxxxxxxxxxxxxxxxxx*/
Drop table #daterange