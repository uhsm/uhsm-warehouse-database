﻿/*
Author: Helen Ransome
Date created: 23/10/2015
Stored Procedure Built for pulling out Productivity and Utilisation
*/

CREATE Procedure [TH].[ReportDailyStartsandFinishesProductivity]
/*
@StartDate		as Date
,@EndDate		as Date
*/
as
/*
Declare @StartDate Date = '20151019'
Declare @EndDate Date = '20151025'
*/

/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/
/*gets last calender week*/
 select distinct
	 min(Cal.TheDate) as FirstDayOfLastWeek
	 ,max(Cal.TheDate) LastDayofLastWeek
	 ,dbo.GetEndOfDay (max(Cal.TheDate)) LastDateTimeofLastWeek
	 ,Cal.FirstDayOfWeek as WkCommencing
	 ,Cal.LastDayOfWeek as WkEnding

into #daterange

FROM LK.Calendar Cal
 where  Cal.FirstDayOfWeek = 
     (select distinct
		SQ.FirstDayOfWeek
		  FROM LK.Calendar SQ
		  where SQ.TheDate = 
		 (
		dateadd(day,-7,cast(GETDATE()as DATE))
		)
		)
 group by
	Cal.FirstDayOfWeek 
	 ,Cal.LastDayOfWeek


/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/


SELECT 
     Dateadd(DAY, 0, DATEDIFF(DAY, 0, ss.SessionStartTime)) AS SessionDate
	,cal.DayOfWeek
	,ss.SessionSourceUniqueID
	,ss.SessionStartTime
	,ss.SessionEndTime
	,ss.AdjustedSessionStart
	,ss.AdjustedSessionend
	,ss.AdjustedPlannedSessionMinutes
	,ss.ActualStartTime AS StartTime
	,ss.ActualEndTime
	,ss.SessionTheatre
	,ss.SessionConsultant
	,ss.SessionConsultantCode
	,ss.SessionSpecialty
	,ss.SessionCancelledFlag
	,Max(op.OperationEndDate) AS EndTime
	,COUNT(DISTINCT op.SourceUniqueID) AS Patients
	,SUM(op.UtilisedMinutes) AS AdjustedUtilisedMinutes
	,SUM(op.UtilisedMinutes)/ cast(ss.AdjustedPlannedSessionMinutes AS FLOAT ) AS Productivity
	,ss.PlannedSessionMinutes --to be used for predicted productivity calculation
FROM WHREPORTING.QLIK.TheatreSessions ss
LEFT OUTER JOIN WHReporting.QLIK.TheatreOpDetails op ON ss.SessionSourceUniqueID = op.SessionSourceUniqueID
LEFT OUTER JOIN WHREPORTING.LK.Calendar cal ON Dateadd(DAY, 0, DATEDIFF(DAY, 0, ss.SessionStartTime)) = cal.TheDate
Left join #daterange dr on 1 = 1
WHERE  ss.SessionStartTime between dr.WkCommencing and dr.WkEnding
	AND ss.SessionTheatreType IN (
		'Elective'
		,'DayCase'
		)
	AND ss.SessionTheatreCode NOT IN ('57')
	AND SessionCancelledFlag <> 1
	and ss.SessionSpecialty = 'Pain Management'
GROUP BY 

    ss.SessionStartTime
	,cal.DayOfWeek
	,ss.SessionSourceUniqueID
	,ss.SessionStartTime
	,ss.SessionEndTime
	,ss.AdjustedSessionStart
	,ss.AdjustedSessionend
	,ss.AdjustedPlannedSessionMinutes
	,ss.ActualStartTime
	,ss.ActualEndTime
	,ss.SessionTheatre
	,ss.SessionConsultant
	,ss.SessionConsultantCode
	,ss.SessionSpecialty
	,ss.SessionCancelledFlag
	,ss.PlannedSessionMinutes
	
ORDER BY ss.SessionTheatre
 /*xxxxxxxxxxxxxxxxxxx*/
Drop table #daterange