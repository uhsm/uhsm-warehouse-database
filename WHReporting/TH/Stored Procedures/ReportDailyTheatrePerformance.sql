﻿-- =============================================
-- Author:		CMan
-- Create date: 13/11/2014
-- Description:	Stored procedure for generating the Daily Theatre Performance Report  - to replace one that is manually put together by CBH for the weekly Theatre huddle meetings
--				
--				
-- =============================================
CREATE PROCEDURE [TH].[ReportDailyTheatrePerformance] @date datetime
AS

/*
 Modified Date				Modified By			Modification
---------------			------------		------------
13/11/2014					CMan				Created
15/01/2015					CMan				Added temp table to calculate the rolling 12 month average cases per session to get the column to be added to the report 
09/03/2015					CMan				Added changes to help build in the ward from which the first op came from.
31/03/2015					CMan				Added is null to the ward to bring in the ORMIS op type where the ward is null for Emergency and Urgent op types.
09/04/2015					CMan				Added new temp table to bring in column of predicted productivity - i.e % booked as reported in the Booking and Scheduling Tool
*/





BEGIN





--Declare @date  datetime = '20150401'
DECLARE @start DATETIME = DateAdd(month, - 12, Cast('01/' + Datename(month, getdate()) + '/' + Datename(year, getdate()) AS DATETIME))
	,@end DATETIME = Cast('01/' + Datename(month, getdate()) + '/' + Datename(year, getdate()) AS DATETIME)

--Build Temp table to get the Average patients per session for each consultant over a rollowing 12 month period.
--Data to be used in the final query below.
IF OBJECT_ID('tempdb..#AvCase') > 0
	DROP TABLE #AvCase

SELECT count(DISTINCT ss.SessionSourceUniqueID) AS NumberofSessions
	,sum(ss.PlannedSessionMinutes / CAST(240 AS FLOAT)) AS NumberOfStandardisedLists
	,SessionConsultant
	,sum(op.NumberOfPatients) AS NumberOfPatients
INTO #AvCase
FROM WHREPORTING.QLIK.TheatreSessions ss
LEFT OUTER JOIN (
	SELECT SessionSourceUniqueID
		,COUNT(DISTINCT SourceUniqueID) AS NumberOfPatients
	FROM WHREPORTING.QLIK.TheatreOpDetails
	WHERE OperationCancelledFlag <> 1
	GROUP BY SessionSourceUniqueID
	) op ON ss.SessionSourceUniqueID = op.SessionSourceUniqueID
WHERE Dateadd(DAY, 0, DATEDIFF(DAY, 0, SessionStartTime)) >= @start --DateAdd(month, -12,Dateadd( DAY,0 , DATEDIFF(DAY, 0, GETDATE()))
	AND Dateadd(DAY, 0, DATEDIFF(DAY, 0, SessionStartTime)) < @end
	AND SessionCancelledFlag <> 1
	AND (
	 ss.SessionTheatreType IN (
		'Elective'
		,'DayCase'
		)
		OR ss.SessionTheatreCode in ('20','22','23')
		)-- added this as per SManns email. All TDC theatres irrespective of Elective or Emergency type are to be included for the huddle report
	AND ss.SessionTheatreCode NOT IN ('57')
GROUP BY SessionConsultant
ORDER BY SessionConsultant




/*********************************Booking data*************************
--create temp table to get a list of bookings where the intended procedure date on the date selected
--Required to add additional column of prediticted productivity into Huddle report
**************************************************************************/

--SM advises that the booking team tend to record the anaesthetist against first procedure on the list
--Therefore - create temp table below which will effectively give us the anaesthetist for each session, have had to use a min as there are some instances where two bookings recorded against the same session id have been entered with two different anaesthetists. e.g. session id 188584
If OBJECT_ID ('tempdb..#LkpAnaes') > 0
Drop table #LkpAnaes
SELECT SessionSourceUniqueID
	,min(StaffCode1) AS AnaesthetistCode
into #LkpAnaes
FROM WH.Theatre.PatientBooking
LEFT OUTER JOIN WH.Theatre.Staff ON AnaesthetistCode = staff.StaffCode
WHERE IntendedProcedureDate = @Date
GROUP BY SessionSourceUniqueID

IF OBJECT_ID('tempdb..#Booking') > 0
DROP TABLE #Booking
Create table #Booking (SessionSourceUniqueID varchar(20), BookedDuration float)
Insert into  #Booking
SELECT SessionSourceUniqueID
		,SUM(CASE WHEN booking.BookingConsultantCode <> booking.Consultant then isnull(booking.MedianOperationMinutesFinal,0) + isnull(booking.MedianAnaestheticMinutesFinal,0)--09/03/2015 CM rule added to use median time if the Consultant on the waiting list is different to that on the booking.
		WHEN booking.EstimatedTheatreTime = 0 then isnull(booking.MedianOperationMinutesFinal,0) + isnull(booking.MedianAnaestheticMinutesFinal,0)--if estimated op duration is 0 has been added to the WL then use the median op time by consultant and prim proc, or just median time by prim proc if there is not one for the consultant
		WHEN booking.EstimatedTheatreTime IS NULL then isnull(booking.MedianOperationMinutesFinal,0) + isnull(booking.MedianAnaestheticMinutesFinal,0)--if estimated op duration is null has been added to the WL then use the median op time by consultant and prim proc, or just median time by prim proc if there is not one for the consultant
		ELSE booking.EstimatedTheatreTime+ Isnull(booking.MedianAnaestheticMinutesFinal,0)
		END) as BookedDuration--Booked time as per booking and scheduling tool

FROM (
	SELECT DISTINCT pb.SourceUniqueID AS BookingUniqueID
		,pb.DistrictNo
		,pb.IntendedProcedureDate
		,pb.TheatreCode
		,pb.ConsultantCode
		,pb.AnaesthetistCode
		,pb.SessionSourceUniqueID
		,cons.StaffCode1 AS BookingConsultantCode
		,isnull(anaes.StaffCode1, #lkpAnaes.AnaesthetistCode) AS BookingAnaesthetistCode
		,wl.SourceUniqueID
		,wl.Consultant
		,wl.EstimatedTheatreTime
		,cc.CODE
		,cc.DESCRIPTION
		,antyp.ReferenceValue AS WLAnaestheticType
		,pt.MedianOperationMinutes--median procedure time for last 12 months by consultant and procedure
		,ptp.MedianOperationMinutes AS MedianOperationMinutesByProc--median procedure time for last 12 months broken down by procedure only
		,at.MedianAnaestheticMinutes --this is the median anaesthetic time for the last 12 months broken down by consultant and procedure
		,atp.MedianAnaestheticMinutes AS MedianAnaestheticMinutesByProc --this is the median anaesthetic time broken by procedure only  
		,CASE 
			WHEN (
					wl.EstimatedTheatreTime = 0
					OR wl.EstimatedTheatreTime IS NULL
					)
				AND isnull(pt.MedianOperationMinutes, ptp.MedianOperationMinutes) IS NULL
				THEN 1
			ELSE 0
			END AS NoOpTimeFlag
		,Case when Coalesce(at.MedianAnaestheticMinutes, atp.MedianAnaestheticMinutes, '10') < 10 
				Then 10 Else Coalesce(at.MedianAnaestheticMinutes, atp.MedianAnaestheticMinutes, '10') End  AS MedianAnaestheticMinutesFinal--this is the median anaesthetic time to be added into the final report - i.e. mapping to the breakdown by consultant and procedure where there is one and then to the breakdown by procedure only if there isn't one.  Also as agreed with MOsborne, if anaesthetic mins < 10 then minimum cap at 10 
		,ISNULL(pt.MedianOperationMinutes, ptp.MedianOperationMinutes) as MedianOperationMinutesFinal
		,CASE 
			WHEN ISNULL(at.MedianAnaestheticMinutes, atp.MedianAnaestheticMinutes) IS NULL
				THEN 1
			ELSE 0
			END AS NoAnaestheticTimeFlag--field to help flag up ops which do not have a median anaesthetic time included in the duration calculation

	FROM WH.Theatre.PatientBooking pb
	LEFT OUTER JOIN WH.PTL.IPWL wl ON pb.PatientEpisodeNumber = wl.SourceUniqueID
	LEFT OUTER JOIN (
		SELECT *
		FROM WH.PAS.ClinicalCodingBase
		WHERE SORCE_CODE = 'WLIST'
			AND SORT_ORDER = 1
			AND ARCHV_FLAG = 'N'
			AND DPTYP_CODE = 'PROCE'
		) cc ON cast(pb.PatientEpisodeNumber AS INT) = cc.SORCE_REFNO
	LEFT OUTER JOIN #LkpAnaes ON pb.SessionSourceUniqueID = #LkpAnaes.SessionSourceUniqueID -- bring this temp table in so that the AnaesthetistCode field can be more complete - if the actual patient booking itself does not ahve an anesthetist recorded against it then bring in the anesthetist from the temp table above as an isnull - see BookingAnaesthetistCode field above
	LEFT OUTER JOIN WH.Theatre.Staff cons ON pb.ConsultantCode = cons.StaffCode
	LEFT OUTER JOIN WH.Theatre.Staff anaes ON pb.AnaesthetistCode = anaes.StaffCode
	LEFT OUTER JOIN WHReporting.TH.TheatreProcedureTimes pt ON cons.StaffCode1 = pt.OperationDetailConsultantCode
		AND left(cc.CODE, 3) = pt.PrimaryProcedureCodeGrouped
	LEFT OUTER JOIN WHReporting.TH.TheatreAnaestheticTimes at ON isnull(anaes.StaffCode1, #lkpAnaes.AnaesthetistCode) = at.AnaesthetistCode
		AND left(cc.CODE, 3) = at.PrimaryProcedureCodeGrouped
	LEFT OUTER JOIN (
		SELECT *
		FROM WH.PAS.ReferenceValue
		WHERE ReferenceDomainCode = 'ANTYP'
		) antyp ON wl.AnaestheticType = antyp.MainCode
	LEFT OUTER JOIN WHREPORTING.TH.TheatreAnaestheticTimesByProcedureOnly atp ON left(cc.CODE, 3) = atp.PrimaryProcedureCodeGrouped
	LEFT OUTER JOIN WHREPORTING.TH.TheatreProcedureTimesByProcedureOnly ptp ON left(cc.CODE, 3) = ptp.PrimaryProcedureCodeGrouped
	WHERE IntendedProcedureDate = @Date
		AND pb.PatientEpisodeNumber <> '' --this will select the cohort of patients where the ORMIS booking is attached to a PAS waiting list
		AND pb.SourceUniqueID NOT IN (
			SELECT PatientSourceUniqueID
			FROM WH.Theatre.Cancellation
			WHERE Dateadd(day,DATEDIFF(DAY,0,CancellationDate),0)<> ProposedOperationDate
			)
	
	UNION ALL
	
	SELECT DISTINCT pb.SourceUniqueID AS BookingUniqueID
		,pb.DistrictNo
		,pb.IntendedProcedureDate
		,pb.TheatreCode
		,pb.ConsultantCode
		,pb.AnaesthetistCode
		,pb.SessionSourceUniqueID
		,cons.StaffCode1 AS BookingConsultantCode
		,isnull(anaes.StaffCode1, #lkpAnaes.AnaesthetistCode) AS BookingAnaesthetistCode
		,wl.SourceUniqueID
		,wl.Consultant
		,wl.EstimatedTheatreTime
		,cc.CODE
		,cc.DESCRIPTION
		,antyp.ReferenceValue AS WLAnaestheticType
		,pt.MedianOperationMinutes--median procedure time for last 12 months by consultant and procedure
		,ptp.MedianOperationMinutes AS MedianOperationMinutesByProc --median procedure time for last 12 months broken down by procedure only
		,at.MedianAnaestheticMinutes --this is the median anaesthetic time for the last 12 months broken down by consultant and procedure
		,atp.MedianAnaestheticMinutes AS MedianAnaestheticMinutesByProc --this is the median anaesthetic time broken by procedure only  
		,CASE 
			WHEN (
					wl.EstimatedTheatreTime = 0
					OR wl.EstimatedTheatreTime IS NULL
					)
				AND isnull(pt.MedianOperationMinutes, ptp.MedianOperationMinutes) IS NULL
				THEN 1
			ELSE 0
			END AS NoOpTimeFlag
		,Case when Coalesce(at.MedianAnaestheticMinutes, atp.MedianAnaestheticMinutes, '10') < 10 
				Then 10 Else Coalesce(at.MedianAnaestheticMinutes, atp.MedianAnaestheticMinutes, '10') End  AS MedianAnaestheticMinutesFinal--this is the median anaesthetic time to be added into the final report - i.e. mapping to the breakdown by consultant and procedure where there is one and then to the breakdown by procedure only if there isn't one.  Also as agreed with MOsborne, if anaesthetic mins < 10 then minimum cap at 10 , if there is no match at all assign 10
		,ISNULL(pt.MedianOperationMinutes, ptp.MedianOperationMinutes) as MedianOperationMinutesFinal
		,CASE 
			WHEN ISNULL(at.MedianAnaestheticMinutes, atp.MedianAnaestheticMinutes) IS NULL
				THEN 1
			ELSE 0
			END AS NoAnaestheticTimeFlag--field to help flag up ops which do not have a median anaesthetic time included in the duration calculation
	FROM WH.Theatre.PatientBooking pb
	LEFT OUTER JOIN WH.PTL.IPWL wl ON pb.DistrictNo = wl.DistrictNo
		AND (
			pb.IntendedProcedureDate = DateAdd(day, Datediff(day, 0, wl.TCIDate), 0)
			OR pb.IntendedProcedureDate = DateAdd(day, Datediff(day, 0, wl.TCIDate) + 1, 0)
			)
	LEFT OUTER JOIN (
		SELECT *
		FROM WH.PAS.ClinicalCodingBase
		WHERE SORCE_CODE = 'WLIST'
			AND SORT_ORDER = 1
			AND ARCHV_FLAG = 'N'
			AND DPTYP_CODE = 'PROCE'
		) cc ON cast(wl.SourceUniqueID AS INT) = cc.SORCE_REFNO
	LEFT OUTER JOIN #LkpAnaes ON pb.SessionSourceUniqueID = #LkpAnaes.SessionSourceUniqueID -- bring this temp table in so that the AnaesthetistCode field can be more complete - if the actual patient booking itself does not ahve an anesthetist recorded against it then bring in the anesthetist from the temp table above as an isnull - see BookingAnaesthetistCode field above
	LEFT OUTER JOIN WH.Theatre.Staff cons ON pb.ConsultantCode = cons.StaffCode
	LEFT OUTER JOIN WH.Theatre.Staff anaes ON pb.AnaesthetistCode = anaes.StaffCode
	LEFT OUTER JOIN WHReporting.TH.TheatreProcedureTimes pt ON cons.StaffCode1 = pt.OperationDetailConsultantCode
		AND left(cc.CODE, 3) = pt.PrimaryProcedureCodeGrouped
	LEFT OUTER JOIN WHReporting.TH.TheatreAnaestheticTimes at ON isnull(anaes.StaffCode1, #lkpAnaes.AnaesthetistCode) = at.AnaesthetistCode
		AND left(cc.CODE, 3) = at.PrimaryProcedureCodeGrouped
	LEFT OUTER JOIN (
		SELECT *
		FROM WH.PAS.ReferenceValue
		WHERE ReferenceDomainCode = 'ANTYP'
		) antyp ON wl.AnaestheticType = antyp.MainCode
	LEFT OUTER JOIN WHREPORTING.TH.TheatreAnaestheticTimesByProcedureOnly atp ON left(cc.CODE, 3) = atp.PrimaryProcedureCodeGrouped
	LEFT OUTER JOIN WHREPORTING.TH.TheatreProcedureTimesByProcedureOnly ptp ON left(cc.CODE, 3) = ptp.PrimaryProcedureCodeGrouped
	WHERE IntendedProcedureDate = @Date
		AND PatientEpisodeNumber = '' --this will be the cohort of patients who have an ORMIS booking without it being linked to a waiting list.  Different matching criteria is used for these ones.--see left join above
		AND pb.SourceUniqueID NOT IN (
			SELECT PatientSourceUniqueID
			FROM WH.Theatre.Cancellation
			WHERE Dateadd(day,DATEDIFF(DAY,0,CancellationDate),0)<> ProposedOperationDate
			)
	) booking

Group by SessionSourceUniqueID




--Build Temp table to get Turnaround time per session
IF OBJECT_ID('tempdb..#Turn') > 0
	DROP TABLE #Turn

SELECT SessionSourceUniqueID
	,SUM(TurnaroundTime) AS TurnaroundTime
	,COUNT(DISTINCT SourceUniqueID) AS NoOfPatients
INTO #Turn
FROM (
	SELECT CASE 
			WHEN a.SessionSourceUniqueId = 0
				THEN NULL
			ELSE b.AnaestheticInductionTime
			END AS NextAnaestheticInductionTime
		,
		--b.AnaestheticReadyTime                                       AS NestOpAnaestheticReadyTime,
		CASE 
			WHEN a.SessionSourceUniqueId = 0
				THEN 0 --all ops which are not linked to a session ID have had turaround set to 0
			WHEN Datediff(MINUTE, a.OperationEndDate, b.AnaestheticInductionTime) < 0
				THEN 0 --this sets all those ops where the next anaesthetic induction started before adjusted op end to 0
			ELSE Datediff(MINUTE, a.OperationEndDate, b.AnaestheticInductionTime)
			END AS TurnaroundTime
		,a.SessionSourceUniqueID
		,a.SourceUniqueID
	FROM WHReporting.QLIK.TheatreOpDetails a
	LEFT OUTER JOIN WHReporting.QLIK.TheatreOpDetails b ON a.OperationDate = b.OperationDate
		AND a.SessionSourceUniqueID = b.SessionSourceUniqueID
		AND a.OperationOrder + 1 = b.OperationOrder
		AND a.OperationTheatreCode = b.OperationTheatreCode
	WHERE a.OperationDate = @date
		AND CASE 
			WHEN a.SessionSourceUniqueId = 0
				THEN 0 --all ops which are not linked to a session ID have had turaround set to 0
			WHEN Datediff(MINUTE, a.OperationEndDate, b.AnaestheticInductionTime) < 0
				THEN 0 --this sets all those ops where the next anaesthetic induction started before adjusted op end to 0
			ELSE Datediff(MINUTE, a.OperationEndDate, b.AnaestheticInductionTime)
			END IS NOT NULL --add this criteria as we do not want to count the last patient of the session which does not have a turnaround time 
	) Turn
GROUP BY SessionSourceUniqueID





--Final Query
SELECT Dateadd(DAY, 0, DATEDIFF(DAY, 0, ss.SessionStartTime)) AS SessionDate
	,cal.DayOfWeek
	,ss.SessionSourceUniqueID
	,ss.SessionStartTime
	,ss.SessionEndTime
	,ss.AdjustedSessionStart
	,ss.AdjustedSessionend
	,ss.AdjustedPlannedSessionMinutes
	,ss.ActualStartTime AS StartTime
	,ss.ActualEndTime
	,ss.SessionTheatre
	,ss.SessionConsultant
	,ss.SessionConsultantCode
	,ss.SessionSpecialty
	,ss.SessionCancelledFlag
	,Max(op.OperationEndDate) AS EndTime
	,--using the out of OR time of the last op in the session as the actual end time of the session
	COUNT(DISTINCT op.SourceUniqueID) AS Patients
	,canc.cancellations AS Cancellations
	,CASE 
		WHEN ss.ActualStartTime > ss.SessionStartTime
			THEN DATEDIFF(MINUTE, ss.SessionStartTime, ss.ActualStartTime)
		ELSE 0
		END AS LateStartMinutes
	,CASE 
		WHEN ss.SessionStartTime > ss.ActualStartTime
			THEN DATEDIFF(MINUTE, ss.ActualStartTime, ss.SessionStartTime)
		ELSE 0
		END AS EarlyStartMinutes
	,CASE 
		WHEN Max(op.OperationEndDate) < ss.SessionEndTime
			THEN DATEDIFF(Minute, Max(op.OperationEndDate), ss.SessionEndTime)
		ELSE 0
		END AS EarlyFinishMinutes
	,CASE 
		WHEN ss.SessionEndTime < Max(op.OperationEndDate)
			THEN DATEDIFF(Minute, ss.SessionEndTime, Max(op.OperationEndDate))
		ELSE 0
		END AS OverrunMinutes
	,SUM(op.UtilisedMinutes) AS AdjustedUtilisedMinutes
	,SUM(op.UtilisedMinutes) / cast(ss.AdjustedPlannedSessionMinutes AS FLOAT) AS Productivity
	,#Turn.TurnaroundTime AS TotalTurnaroundTime
	,#Turn.TurnaroundTime / cast(#Turn.NoOfPatients AS FLOAT) AS AverageTurnaroundTime -- Use the count of Patients from the #Turn temp table as this excludes the final patient of the session. Average Turnaround time calculation here is sum(Minutes between OpEndDate to next anaesthetic induction)/no of patients, but the last patient in the session will not have a turnaround time, therefore not to be included in the denominator count 
	,#AvCase.NumberOfPatients / cast(#AvCase.NumberofStandardisedLists AS FLOAT) AS AverageCasesPerStandardisedSession
	,Anaes.OperationAnaesthetist
	,Anaes.WardCode
	,Anaes.WardName
	,bk.BookedDuration
	,ss.PlannedSessionMinutes --to be used for predicted productivity calculation
FROM WHREPORTING.QLIK.TheatreSessions ss
LEFT OUTER JOIN WHReporting.QLIK.TheatreOpDetails op ON ss.SessionSourceUniqueID = op.SessionSourceUniqueID
LEFT OUTER JOIN (
	SELECT DISTINCT TheatreCode
		,SurgeonCode
		,COUNT(DISTINCT CancellationSourceUniqueID) AS Cancellations
	FROM WHREPORTING.QLIK.TheatreCancellations
	WHERE ProposedOperationDate = @date
		AND IncludeAsCancelledOnDay = 1
	GROUP BY TheatreCode
		,SurgeonCode
	) canc ON ss.SessionTheatreCode = canc.TheatreCode
	AND ss.SessionConsultantCode = canc.SurgeonCode
LEFT OUTER JOIN WHREPORTING.LK.Calendar cal ON Dateadd(DAY, 0, DATEDIFF(DAY, 0, ss.SessionStartTime)) = cal.TheDate
LEFT OUTER JOIN #AVCase ON ss.SessionConsultant = #AvCase.SessionConsultant
LEFT OUTER JOIN #Turn ON ss.SessionSourceUniqueID = #Turn.SessionSourceUniqueID --Link back to the temptable which details the total turnaround for each session
LEFT OUTER JOIN (SELECT SessionSourceUniqueID
					,OperationAnaesthetist
					,ISNULL(WardCode, case when op.OperationTypeCode in ('EMERGENCY','URGENT') Then op.OperationTypeCode Else NULL End ) as WardCode
					,ISNULL(WardName, case when op.OperationTypeCode in ('EMERGENCY','URGENT') Then op.OperationTypeCode Else NULL End ) as WardName--31/03/2015 CM:  Added this and above row to help indicate whether or not any sessions where the first op is missing a ward are as a result of an emergency as when an emergency it is possible patient goes straight to the Theatre before the ward so there is no ward at the time of In Anaesthetic in theatre.
				FROM WHREPORTING.QLIK.TheatreOpDetails op
				LEFT OUTER JOIN (
					SELECT ws.StartTime
						,ws.EndTime
						,ws.WardCode
						,wd.WardName						
						,pat.FacilityID
					FROM WHREPORTING.APC.WardStay ws
					LEFT OUTER JOIN WHREPORTING.LK.PatientCurrentDetails pat ON ws.SourcePatientNo = pat.SourcePatientNo
					LEFT OUTER JOIN WHREPORTING.LRD.CurrentWard wd ON ws.WardCode = wd.WardCode
						) stay ON op.DistrictNo = stay.FacilityID AND Coalesce(op.AnaestheticInductionTime, op.OperationStartDate, op.InSuiteTime) >= stay.StartTime  AND (Coalesce(op.AnaestheticInductionTime, op.OperationStartDate, op.InSuiteTime) < stay.EndTime or stay.EndTime is null)--use Coalesce here as sometimes anaesthetic Induction and Op Start fields have not been completed.
WHERE OperationOrder = 1) Anaes-- join back to the first operation in a session to get the Anaesthetic Details of the first op 
								--also included in thhis subquery is a join to wardstay to get the details of the ward which the first op came from - as per request from CBH in meeting on 25/02/2015
ON ss.SessionSourceUniqueID = Anaes.SessionSourceUniqueID
Left outer join #Booking  bk on ss.SessionSourceUniqueID = bk.SessionSourceUniqueID
WHERE Dateadd(DAY, 0, DATEDIFF(DAY, 0, ss.SessionStartTime)) = @date
	AND (
		ss.SessionTheatreType IN (
		'Elective'
		,'DayCase'
		)
		OR ss.SessionTheatreCode in ('20','22','23')
		)-- added this as per SManns email. All TDC theatres irrespective of Elective or Emergency type are to be included for the huddle report
	AND ss.SessionTheatreCode NOT IN ('57')
	AND SessionCancelledFlag <> 1
GROUP BY Dateadd(DAY, 0, DATEDIFF(DAY, 0, ss.SessionStartTime))
	,cal.DayOfWeek
	,ss.SessionSourceUniqueID
	,ss.SessionStartTime
	,ss.SessionEndTime
	,ss.AdjustedSessionStart
	,ss.AdjustedSessionend
	,ss.AdjustedPlannedSessionMinutes
	,ss.ActualStartTime
	,ss.ActualEndTime
	,ss.SessionTheatre
	,ss.SessionConsultant
	,ss.SessionConsultantCode
	,ss.SessionSpecialty
	,ss.SessionCancelledFlag
	,CASE 
		WHEN ss.ActualStartTime > ss.SessionStartTime
			THEN DATEDIFF(MINUTE, ss.SessionStartTime, ss.ActualStartTime)
		ELSE 0
		END
	,CASE 
		WHEN ss.SessionStartTime > ss.ActualStartTime
			THEN DATEDIFF(MINUTE, ss.ActualStartTime, ss.SessionStartTime)
		ELSE 0
		END
	,canc.cancellations
	,#AvCase.NumberOfPatients / cast(#AvCase.NumberOfStandardisedLists AS FLOAT)
	,#Turn.TurnaroundTime
	,#Turn.TurnaroundTime / cast(#Turn.NoOfPatients AS FLOAT)
	,Anaes.OperationAnaesthetist
	,Anaes.WardCode
	,Anaes.WardName
	,bk.BookedDuration
	,ss.PlannedSessionMinutes
ORDER BY ss.SessionTheatre



--Select OperationEndDate, AnaestheticInductionTime, * from WHREPORTING.QLIK.TheatreOpDetails
--where SessionSourceUniqueID  = '189510'           
--order by OperationOrder
--SELECT *
--FROM #Turn
--WHERE sessionsourceuniqueid = '189114'






END