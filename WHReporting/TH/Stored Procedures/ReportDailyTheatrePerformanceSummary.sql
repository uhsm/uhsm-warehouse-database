﻿-- =============================================
-- Author:		CMan
-- Create date: 02/03/2015
-- Description:	Stored procedure for generating a summary of the Daily Theatre Performance Report to be used on the Theatre screens as per SManns and CBH email from 02/03/2015
--				
-- =============================================
CREATE  PROCEDURE [TH].[ReportDailyTheatrePerformanceSummary] @date datetime
AS

/*
 Modified Date				Modified By			Modification
-----------------			------------		------------
13/11/2014					CMan				Created
15/01/2015					CMan				Added temp table to calculate the rolling 12 month average cases per session to get the column to be added to the report 


*/





BEGIN




--Declare @date  datetime = '20150223'
DECLARE @start DATETIME = DateAdd(month, - 12, Cast('01/' + Datename(month, getdate()) + '/' + Datename(year, getdate()) AS DATETIME))
	,@end DATETIME = Cast('01/' + Datename(month, getdate()) + '/' + Datename(year, getdate()) AS DATETIME)
	,@reportingstart datetime =  @date
	,@reportingend datetime = DateAdd(day, 7, @date)

--Build Temp table to get the Average patients per session for each consultant over a rollowing 12 month period.
--Data to be used in the final query below.
IF OBJECT_ID('tempdb..#AvCase') > 0
	DROP TABLE #AvCase

SELECT count(DISTINCT ss.SessionSourceUniqueID) AS NumberofSessions
	,sum(ss.PlannedSessionMinutes / CAST(240 AS FLOAT)) AS NumberOfStandardisedLists
	,SessionConsultant
	,sum(op.NumberOfPatients) AS NumberOfPatients
INTO #AvCase
FROM WHREPORTING.QLIK.TheatreSessions ss
LEFT OUTER JOIN (
	SELECT SessionSourceUniqueID
		,COUNT(DISTINCT SourceUniqueID) AS NumberOfPatients
	FROM WHREPORTING.QLIK.TheatreOpDetails
	WHERE OperationCancelledFlag <> 1
	GROUP BY SessionSourceUniqueID
	) op ON ss.SessionSourceUniqueID = op.SessionSourceUniqueID
WHERE Dateadd(DAY, 0, DATEDIFF(DAY, 0, SessionStartTime)) >= @start --DateAdd(month, -12,Dateadd( DAY,0 , DATEDIFF(DAY, 0, GETDATE()))
	AND Dateadd(DAY, 0, DATEDIFF(DAY, 0, SessionStartTime)) < @end
	AND SessionCancelledFlag <> 1
	AND (
	 ss.SessionTheatreType IN (
		'Elective'
		,'DayCase'
		)
		OR ss.SessionTheatreCode in ('20','22','23')
		)-- added this as per SManns email. All TDC theatres irrespective of Elective or Emergency type are to be included for the huddle report
	AND ss.SessionTheatreCode NOT IN ('57')
GROUP BY SessionConsultant
ORDER BY SessionConsultant





--Build Temp table to get Turnaround time per session
IF OBJECT_ID('tempdb..#Turn') > 0
	DROP TABLE #Turn

SELECT SessionSourceUniqueID
	,SUM(TurnaroundTime) AS TurnaroundTime
	,COUNT(DISTINCT SourceUniqueID) AS NoOfPatients
INTO #Turn
FROM (
	SELECT CASE 
			WHEN a.SessionSourceUniqueId = 0
				THEN NULL
			ELSE b.AnaestheticInductionTime
			END AS NextAnaestheticInductionTime
		,
		--b.AnaestheticReadyTime                                       AS NestOpAnaestheticReadyTime,
		CASE 
			WHEN a.SessionSourceUniqueId = 0
				THEN 0 --all ops which are not linked to a session ID have had turaround set to 0
			WHEN Datediff(MINUTE, a.OperationEndDate, b.AnaestheticInductionTime) < 0
				THEN 0 --this sets all those ops where the next anaesthetic induction started before adjusted op end to 0
			ELSE Datediff(MINUTE, a.OperationEndDate, b.AnaestheticInductionTime)
			END AS TurnaroundTime
		,a.SessionSourceUniqueID
		,a.SourceUniqueID
	FROM WHReporting.QLIK.TheatreOpDetails a
	LEFT OUTER JOIN WHReporting.QLIK.TheatreOpDetails b ON a.OperationDate = b.OperationDate
		AND a.SessionSourceUniqueID = b.SessionSourceUniqueID
		AND a.OperationOrder + 1 = b.OperationOrder
		AND a.OperationTheatreCode = b.OperationTheatreCode
	WHERE a.OperationDate >= @reportingstart
		AND a.OperationDate <= @reportingend
		AND CASE 
			WHEN a.SessionSourceUniqueId = 0
				THEN 0 --all ops which are not linked to a session ID have had turaround set to 0
			WHEN Datediff(MINUTE, a.OperationEndDate, b.AnaestheticInductionTime) < 0
				THEN 0 --this sets all those ops where the next anaesthetic induction started before adjusted op end to 0
			ELSE Datediff(MINUTE, a.OperationEndDate, b.AnaestheticInductionTime)
			END IS NOT NULL --add this criteria as we do not want to count the last patient of the session which does not have a turnaround time 
	) Turn
GROUP BY SessionSourceUniqueID





--Final Query
SELECT Dateadd(DAY, 0, DATEDIFF(DAY, 0, ss.SessionStartTime)) AS SessionDate
	,cal.DayOfWeek
	,ss.SessionSourceUniqueID
	,ss.SessionStartTime
	,ss.SessionEndTime
	,ss.AdjustedSessionStart
	,ss.AdjustedSessionend
	,ss.AdjustedPlannedSessionMinutes
	,ss.ActualStartTime AS StartTime
	,ss.ActualEndTime
	,ss.SessionTheatre
	,ss.SessionConsultant
	,ss.SessionConsultantCode
	,ss.SessionSpecialty
	,ss.SessionCancelledFlag
	,Max(op.OperationEndDate) AS EndTime
	--,--using the out of OR time of the last op in the session as the actual end time of the session
	--COUNT(DISTINCT op.SourceUniqueID) AS Patients
	--,canc.cancellations AS Cancellations
	,CASE 
		WHEN ss.ActualStartTime > ss.SessionStartTime
			THEN DATEDIFF(MINUTE, ss.SessionStartTime, ss.ActualStartTime)
		ELSE 0
		END AS LateStartMinutes
	,CASE 
		WHEN ss.SessionStartTime > ss.ActualStartTime
			THEN DATEDIFF(MINUTE, ss.ActualStartTime, ss.SessionStartTime)
		ELSE 0
		END AS EarlyStartMinutes
	,CASE 
		WHEN Max(op.OperationEndDate) < ss.SessionEndTime
			THEN DATEDIFF(Minute, Max(op.OperationEndDate), ss.SessionEndTime)
		ELSE 0
		END AS EarlyFinishMinutes
	,CASE 
		WHEN ss.SessionEndTime < Max(op.OperationEndDate)
			THEN DATEDIFF(Minute, ss.SessionEndTime, Max(op.OperationEndDate))
		ELSE 0
		END AS OverrunMinutes
	--,SUM(op.UtilisedMinutes) AS AdjustedUtilisedMinutes
	,SUM(op.UtilisedMinutes) / cast(ss.AdjustedPlannedSessionMinutes AS FLOAT) AS Productivity
	--,#Turn.TurnaroundTime AS TotalTurnaroundTime
	--,#Turn.TurnaroundTime / cast(#Turn.NoOfPatients AS FLOAT) AS AverageTurnaroundTime -- Use the count of Patients from the #Turn temp table as this excludes the final patient of the session. Average Turnaround time calculation here is sum(Minutes between OpEndDate to next anaesthetic induction)/no of patients, but the last patient in the session will not have a turnaround time, therefore not to be included in the denominator count 
	--,#AvCase.NumberOfPatients / cast(#AvCase.NumberofStandardisedLists AS FLOAT) AS AverageCasesPerStandardisedSession
	--,Anaes.OperationAnaesthetist
FROM WHREPORTING.QLIK.TheatreSessions ss
LEFT OUTER JOIN WHReporting.QLIK.TheatreOpDetails op ON ss.SessionSourceUniqueID = op.SessionSourceUniqueID
LEFT OUTER JOIN (
	SELECT DISTINCT TheatreCode
		,SurgeonCode
		,COUNT(DISTINCT CancellationSourceUniqueID) AS Cancellations
	FROM WHREPORTING.QLIK.TheatreCancellations
	WHERE ProposedOperationDate = @date
		AND IncludeAsCancelledOnDay = 1
	GROUP BY TheatreCode
		,SurgeonCode
	) canc ON ss.SessionTheatreCode = canc.TheatreCode
	AND ss.SessionConsultantCode = canc.SurgeonCode
LEFT OUTER JOIN WHREPORTING.LK.Calendar cal ON Dateadd(DAY, 0, DATEDIFF(DAY, 0, ss.SessionStartTime)) = cal.TheDate
LEFT OUTER JOIN #AVCase ON ss.SessionConsultant = #AvCase.SessionConsultant
LEFT OUTER JOIN #Turn ON ss.SessionSourceUniqueID = #Turn.SessionSourceUniqueID --Link back to the temptable which details the total turnaround for each session
LEFT OUTER JOIN (Select SessionSourceUniqueID,OperationAnaesthetist  from WHREPORTING.QLIK.TheatreOpDetails where OperationOrder = 1) Anaes-- join back to the first operation in a session to get the Anaesthetic Details of the first op 
ON ss.SessionSourceUniqueID = Anaes.SessionSourceUniqueID
WHERE Dateadd(DAY, 0, DATEDIFF(DAY, 0, ss.SessionStartTime)) >=  @reportingstart
	AND Dateadd(DAY, 0, DATEDIFF(DAY, 0, ss.SessionStartTime)) <  @reportingend
	AND (
	 ss.SessionTheatreType IN (
		'Elective'
		,'DayCase'
		)
		OR ss.SessionTheatreCode in ('20','22','23')
		)-- added this as per SManns email. All TDC theatres irrespective of Elective or Emergency type are to be included for the huddle report
	AND ss.SessionTheatreCode NOT IN ('57')
	AND SessionCancelledFlag <> 1
GROUP BY Dateadd(DAY, 0, DATEDIFF(DAY, 0, ss.SessionStartTime))
	,cal.DayOfWeek
	,ss.SessionSourceUniqueID
	,ss.SessionStartTime
	,ss.SessionEndTime
	,ss.AdjustedSessionStart
	,ss.AdjustedSessionend
	,ss.AdjustedPlannedSessionMinutes
	,ss.ActualStartTime
	,ss.ActualEndTime
	,ss.SessionTheatre
	,ss.SessionConsultant
	,ss.SessionConsultantCode
	,ss.SessionSpecialty
	,ss.SessionCancelledFlag
	,CASE 
		WHEN ss.ActualStartTime > ss.SessionStartTime
			THEN DATEDIFF(MINUTE, ss.SessionStartTime, ss.ActualStartTime)
		ELSE 0
		END
	,CASE 
		WHEN ss.SessionStartTime > ss.ActualStartTime
			THEN DATEDIFF(MINUTE, ss.ActualStartTime, ss.SessionStartTime)
		ELSE 0
		END
	--,canc.cancellations
	--,#AvCase.NumberOfPatients / cast(#AvCase.NumberOfStandardisedLists AS FLOAT)
	--,#Turn.TurnaroundTime
	--,#Turn.TurnaroundTime / cast(#Turn.NoOfPatients AS FLOAT)
	--,Anaes.OperationAnaesthetist
ORDER BY ss.SessionTheatre



--Select OperationEndDate, AnaestheticInductionTime, * from WHREPORTING.QLIK.TheatreOpDetails
--where SessionSourceUniqueID  = '189510'           
--order by OperationOrder
--SELECT *
--FROM #Turn
--WHERE sessionsourceuniqueid = '189114'






END