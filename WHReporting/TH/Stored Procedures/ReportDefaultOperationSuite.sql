﻿/*
--Author: N Scattergood
--Date created: 14/02/2013
--Stored Procedure Built for SRSS Reports on Theatre
--Many of the Theatre Reports are requested to default to specific Theatres
--This Procedure provides default Operating Suites relevant to these default Theatres. Only used for Reports created or modified by Nick Scat
--Used in Following Reports:
--KPIConsultantByMonth
*/


Create Procedure [TH].[ReportDefaultOperationSuite]
as
Select 
Distinct

	TH.OperatingSuiteCode	as 'OpSuiteCodePar'
	,TH.OperatingSuite	as 'OpSuitePar'

from
[WHOLAP].[dbo].[OlapTheatre] TH

where
TH.TheatreCode in
('1',
'2',
'3',
'6',
'78',
'37',
'38',
'39',
'40',
'14',
'16',
'21',
'13',
'15',
'19',
'20',
'22',
'23'
)
	
	
	order by TH.OperatingSuite