﻿/*
--Author: N Scattergood
--Date created: 25/03/2013
--Stored Procedure Built for pulling out Productivity and Utilisation
--Used for Directorate Repores
*/

CREATE Procedure [TH].[ReportDirectorateReportProductivityUtilisation]
as

select
u.Specialty,
u.NationalSpecialtyCode,
u.Theatre,
u.MonthNumber,
u.MonthYear,
u.[Year],
Case 
when  u.Planned is null 
then null 
else u.Planned end as Planned,
u.Productive,
u.Utilisation,
u.CappedUtilisation
--,(case when u.Planned=0 then null else (sum(u.Productive)/sum(u.Planned)) end) as KPIProductivity,
--(case when u.Planned=0 then null else (sum(u.Utilisation)/sum(u.Planned)) end) as KPIUtilisation

from

(select
s.NationalSpecialtyCode,
s.Specialty,
th.Theatre,
MONTH(t.sessiondate) as MonthNumber,
c.TheMonth as MonthYear,
c.FinancialYear as [Year],

Planned=sum(Case when t.KPI='Planned Minutes Of Actual Session' then (t.actual) else 0 end),
Productive=sum(Case when t.KPI='Productive Minutes' then (t.actual) else 0 end),
Utilisation=sum(Case when t.KPI='Session Utilisation Minutes' then (t.actual) else 0 end),
CappedUtilisation=sum(Case when t.KPI='Capped Session Utilisation' then (t.actual) else 0 end)
from [WHOLAP].dbo.FactTheatreKPI t

		inner join [WHOLAP].dbo.OlapTheatreSpecialty s 
		on s.SpecialtyCode=t.SpecialtyCode
		
			inner join [WHOLAP].dbo.OlapTheatre th 
			on th.TheatreCode=t.TheatreCode
			
					inner join [WHOLAP].dbo.OlapCalendar c 
					on c.TheDate=t.SessionDate

where 
t.SessionDate between '01 apr 2012' and 
 (select 
 distinct
 max(Cal.TheDate) LastDayofRolling13Month
 FROM [WHREPORTING].[LK].[Calendar] Cal
 
 where
Cal.TheMonth in 
(select distinct
SQ.TheMonth
  FROM LK.Calendar SQ
  where SQ.TheDate = 
(
dateadd(MONTH,-1,cast(GETDATE()as DATE))
)
))
	
group by
s.NationalSpecialtyCode,
s.Specialty,
th.Theatre,
MONTH(t.sessiondate),
c.TheMonth,
c.FinancialYear) as u

where u.Planned>0
group by
u.Specialty,
u.NationalSpecialtyCode,
u.Theatre,
u.MonthNumber,
u.MonthYear,
u.[Year],
u.Planned,
u.Productive,
u.Utilisation,
u.CappedUtilisation