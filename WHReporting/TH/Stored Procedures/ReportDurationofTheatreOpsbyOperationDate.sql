﻿/*
--Author: Helen Ransome
--Date created: 15/07/2015
--Stored Procedure Built for SSRS Report on:
--Theatre Duration of Theatre Ops by Discharge Date
*/

CREATE Procedure [TH].[ReportDurationofTheatreOpsbyOperationDate]

 @StartDate as date
 ,@EndDate as date
 
  as 


SELECT     
o.DistrictNo AS 'Hospital Number'
, o.Surname AS 'Patient Surname'
, o.Forename AS 'Patient Forename'
,o.DateOfBirth
--, o.SourceUniqueID AS id
, CAST(o.OperationDate AS DATE) AS OperationDate
,DATEDIFF(MINUTE,Operationstartdate,Operationenddate) as 'Operation Time(Minutes)'
                     -- , t.Theatre
                     -- , s1.Surname AS anaes1name
                     -- , s2.Surname AS anaes2name
                     -- , s3.Surname AS anaes3name
                     -- , s4.Surname AS surgeon1name
                     -- --,s4.Forename AS surgeon1Forename
                     ----,s4.StaffCategoryCode
                     -- , s5.Surname AS surgeon2name
                      --,s5.Forename AS surgeon2Forename
                      --,s5.StaffCategoryCode
                      --, s6.Surname AS surgeon3name
                      --, s6.Forename AS surgeon3Forename
                      --,s6.StaffCategoryCode
                      --, o.InSuiteTime
                      --, o.InAnaestheticTime
                      --, o.AnaestheticInductionTime
                      --, o.AnaestheticReadyTime
                      --, o.OperationStartDate
                      , p.ProcedureStartTime
                            --cast(p.ProcedureStartTime as date) as Procedure,
                      ,p.ProcedureEndTime
                      --,DischargeTime
                      --  ,cast(DischargeTime as date) as DischargeDate
                      --,Spell.AdmissionDate
                      --,Spell.DischargeDate
                      , o.Operation
                      , p.ProcedureDescription,
                      o.OperationStartDate,  
                      o.OperationEndDate,
                      o.DischargeTime,
                        cast(o.DischargeTime as date) as DischargeDate,
                      CASE WHEN datepart(hour, s.starttime) < 12 
                      THEN 'AM' ELSE 'PM' 
                      END AS SessionType
                      --, c.DayOfWeek
                      , o.OperationTypeCode as 'Operation Type'
                      --, o.SpecialtyCode
                      ,sp.Specialty
                      --,o.ASAScoreCode
                      --,A.ASAScore
                                           
                      
FROM         [WH].Theatre.OperationDetail AS o 
LEFT OUTER JOIN       [WH].Theatre.ProcedureDetail AS p 
ON p.OperationDetailSourceUniqueID = o.SourceUniqueID     
and    p.PrimaryProcedureFlag = 1


LEFT JOIN WH.Theatre.ASAScore A
ON o.ASAScoreCode = A.ASAScoreCode

--LEFT Join [WHREPORTING].[APC].[Spell] Spell
--on o.DistrictNo = Spell.FaciltyID
--and o.[OperationStartDate] between Spell.admissiondate and Spell.DischargeDate
                 


INNER JOIN            [WH].Theatre.Theatre AS t ON t.TheatreCode = o.TheatreCode 
LEFT OUTER JOIN       [WH].Theatre.Staff AS s1 ON s1.StaffCode = p.Anaesthetist1Code 
LEFT OUTER JOIN       [WH].Theatre.Staff AS s2 ON s2.StaffCode = p.Anaesthetist2Code 
LEFT OUTER JOIN           [WH].Theatre.Staff AS s3 ON s3.StaffCode = p.Anaesthetist3Code 
LEFT OUTER JOIN       [WH].Theatre.Staff AS s4 ON s4.StaffCode = p.Surgeon1Code 
LEFT OUTER JOIN       [WH].Theatre.Staff AS s5 ON s5.StaffCode = p.Surgeon2Code 
LEFT OUTER JOIN           [WH].Theatre.Staff AS s6 ON s6.StaffCode = p.Surgeon3Code 
LEFT OUTER JOIN       [WH].Theatre.Session AS s ON s.SourceUniqueID = o.SessionSourceUniqueID 
LEFT OUTER JOIN       [WH].WH.Calendar AS c ON c.TheDate = o.OperationDate 
LEFT OUTER JOIN           [WH].Theatre.Specialty AS sp ON sp.SpecialtyCode = o.SpecialtyCode
left outer join               WHREPORTING.LK.Calendar as cal on cal.TheDate = o.OperationDate


WHERE 
    (o.OperationDate BETWEEN @StartDate AND @EndDate) 
    --and
    --(o.OperationCancelledFlag = 0)