﻿/* =============================================
 Author:		<Helen Ransome>
 Create date: <23/10/2015>

 Description:	<SSRS report for IPDC Planned PTL Pain Management>
 <Revised report to add comments field>
 =============================================*/
CREATE PROCEDURE [TH].[ReportIPDCPlannedPainManagement]

	
AS

	SET NOCOUNT ON;

/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/


 select distinct
	 min(Cal.TheDate) as FirstDayOfLastWeek
	 ,max(Cal.TheDate) LastDayofLastWeek
	 ,dbo.GetEndOfDay (max(Cal.TheDate)) LastDateTimeofLastWeek
	 ,Cal.FirstDayOfWeek as WkCommencing
	 ,Cal.LastDayOfWeek as WkEnding

into #daterange

FROM LK.Calendar Cal
 where  Cal.FirstDayOfWeek = 
     (select distinct
		SQ.FirstDayOfWeek
		  FROM LK.Calendar SQ
		  where SQ.TheDate = 
		 (
		dateadd(day,-7,cast(GETDATE()as DATE))
		)
		)
 group by
	Cal.FirstDayOfWeek 
	 ,Cal.LastDayOfWeek

/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/

SELECT SourceUniqueID,
       [DistrictNo],
       [PatientForename],
       [PatientSurname],
       [DateOfBirth],
       [SpecialtyCode],
       [Consultant],
       CASE
         WHEN [IntendedManagement] = '1' THEN 'IP'
         WHEN [IntendedManagement] = '2' THEN 'DC'
         ELSE NULL
       END AS 'PatClass',
       [ElectiveAdmissionMethod],
       [DateOnList],
       [AdmitByDate],
       [TCIDate],
       [PlannedProcedure],
       [GeneralComment],
       EntryStatus,
       Isnull([GeneralComment], [PlannedProcedure])AS ProDesc,
       CASE
         WHEN [ElectiveAdmissionMethod] = '11' THEN 'WL'
         WHEN [ElectiveAdmissionMethod] = '12' THEN 'BL'
         WHEN [ElectiveAdmissionMethod] = '13' THEN 'PL'
         ELSE [ElectiveAdmissionMethod]
       END                                                     AS ElAd,
       (Datediff(DAY, [DateOnList], Getdate())
       + [SuspToDatePtReason] )/7                                 AS WkWaitNow,
       Dateadd(DAY, 14, [AdmitByDate])                         AS DateAddedToRTT,
       CASE
         WHEN [AdmitByDate] < Getdate()
              AND Dateadd(DAY, 14, [AdmitByDate]) < Getdate() THEN 'RTT'
         WHEN [AdmitByDate] < Getdate() THEN 'Review'
         ELSE NULL
       END                                                     AS Category,
       Dateadd(DAY, 140, [AdmitByDate])                        AS [1_RTTBreachDate],
       Dateadd(DAY, 140 + [SuspToDatePtReason], [AdmitByDate]) AS [2_RTTBreachDate],
       CASE
         WHEN   CASE
                WHEN [AdmitByDate] < Getdate()
                     AND Dateadd(DAY, 14, [AdmitByDate]) < Getdate() THEN 'RTT'
                WHEN [AdmitByDate] < Getdate() THEN 'Review'
                ELSE NULL
              END = 'RTT'
              AND Dateadd(DAY, 140 + [SuspToDatePtReason], [AdmitByDate]) IS NOT NULL THEN Dateadd(DAY, 140 + [SuspToDatePtReason], [AdmitByDate])
         ELSE Dateadd(DAY, 140, [AdmitByDate]) + '23:59'
       END                                                     AS [3_RTTBreachDate],
       CASE
         WHEN CASE
                WHEN [AdmitByDate] < Getdate()
                     AND Dateadd(DAY, 14, [AdmitByDate]) < Getdate() THEN 'RTT'
                WHEN [AdmitByDate] < Getdate() THEN 'Review'
                ELSE NULL
              END = 'Review' THEN
           CASE
             WHEN [AdmitByDate] < Dateadd(day,Datediff(day,0,Getdate()),0)
                  AND Dateadd(DAY, 14, [AdmitByDate]) < Dateadd(day,Datediff(day,0,Getdate()),0) THEN 'RTT'
             WHEN [AdmitByDate] < Dateadd(day,Datediff(day,0,Getdate()),0) THEN 'Review'
             ELSE NULL
           END
         WHEN [AdmitByDate] < Dateadd(day,Datediff(day,0,Getdate()),0) THEN 'Overdue'
         WHEN [AdmitByDate] IS NULL THEN 'No Date'
         ELSE 'OK'
       END                                                     AS Overdue,
       Datediff(DAY, WaitingStartDate, Getdate())
       + Isnull([SuspToDatePtReason], 0)                       AS DayWaitNow,
       ( Datediff(DAY, WaitingStartDate, Getdate())
         + Isnull([SuspToDatePtReason], 0) ) / 7               AS WeekWaitNow,
         Dateadd(DAY, 14, [AdmitByDate])as '14 days',
         AdmissionWard,
          CASE
         WHEN (/*[AdmitByDate] + */Dateadd(DAY, 14, [AdmitByDate])) < Dateadd(day,Datediff(day,0,Getdate()),0) THEN 'Overdue'
         WHEN [AdmitByDate] < Dateadd(day,Datediff(day,0,Getdate()),0) THEN 'Review'
         WHEN [AdmitByDate] IS NULL THEN 'No Admit By Date'
         ELSE 'OK'
       END                                                     AS 'Comments'
INTO   #WL
FROM   [WH].[PTL].[IPWL] 
     Left join #daterange dr on 1 = 1
where  DateOnList between dr.WkCommencing and dr.WkEnding
ORDER  BY DateOnList DESC

/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/

SELECT DISTINCT sp.Specialty,
                con.ConsultantName,
                #WL.SourceUniqueID,
                #WL.[DistrictNo],
                #WL.[PatientForename],
                #WL.[PatientSurname],
                #WL.[DateOfBirth],
                #WL.[SpecialtyCode],
                #WL.[Consultant],
                #WL.PatClass,
                #WL.[ElectiveAdmissionMethod],
                #WL.[DateOnList],
                #WL.[AdmitByDate],
                #WL.[TCIDate],
                #WL.[PlannedProcedure],
                #WL.[GeneralComment],
                #WL.EntryStatus,
                #WL.ProDesc,
                #WL.ElAd,
                #WL.WkWaitNow,
                #WL.DateAddedToRTT,
                #WL.Category,
                #WL.[1_RTTBreachDate],
                #WL.[2_RTTBreachDate],
                #WL.[3_RTTBreachDate],
                #WL.Overdue,
                #WL.WeekWaitNow,
                #WL.[14 days],
                #WL.Comments,
                CASE
                  WHEN #WL.Overdue = 'Overdue' THEN Datediff(DAY, #WL.DateAddedToRTT, Getdate())
                  ELSE NULL
                END RTTWaitDays,
                CASE
                  WHEN #WL.Overdue = 'Overdue' THEN Datediff(DAY, #WL.DateAddedToRTT, Getdate()) / 7
                  ELSE NULL
                END RTTWaitWeeks,
                #WL.AdmissionWard
FROM   #WL
       LEFT OUTER JOIN WHreporting.LK.SpecialtyDivision sp
                    ON #WL.[SpecialtyCode] = sp.[SpecialtyCode]
       LEFT OUTER JOIN WHREPORTING.LK.ConsultantSpecialty con
                    ON #WL.[Consultant] = con.ConsultantCode
  

WHERE  #WL.EntryStatus = 'Waiting'
       AND #WL.ElAd = 'PL'
       and sp.MainSpecialtyCode = '190'
       Order by
       [AdmitByDate] Asc
       
/*xxxxxxxxxxxxxxxxxxxxxxxxxx*/
	

Drop Table #WL, #daterange