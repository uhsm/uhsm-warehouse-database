﻿/*
--Author: N Scattergood
--Date created: 14/03/2013
--Stored Procedure Built for pulling out details of last week
--Currently used for:
> SRSS Report on Top Outpatient Clinic DNAs
*/

CREATE Procedure [TH].[ReportORMISCancellationReport]

@StartDate as Date
,@EndDate as Date

as

select 

 
  --cal.TheMonth as MonthYear,
  1 as DataCount,
        FCC.[CC_MRN] as RM2Number,
 FCC.[CC_DATE] as OperationDate
		
       ,FCC.[CC_DATE_TIME_CANCELLED] as DateCancelled
       --,FCC.CC_CANCEL_SU_SEQU
       --,FCC.[CC_SU_SEQU]
      

      ,FCC.[CC_FNAME] +' '+ FCC.[CC_LNAME] as PatientName
      -- ,[CLP_CODE] As Clinical_Priority_Code
      --,[CLP_DESCRIPTION] as Clinical_Priority_Desc
		,CP.CLP_CODE as AdmissionMethod
		      ,toa.TA_DESCRIPTION as IntendedManagement
      --,FCC.[CC_WARD]
      ,CG.CG_DESC as CancellationReasonGroup
      --,Can.CN_CODE as CancellationReasonCode
      ,Can.CN_DESC as CancellationReasonDesc
      
 --  I have just spoken with Fiona about this and asked her to contact you to condense the list and merge the following:-
--•	Bed shortage & ward beds unavailable
--•	ICU/HDU bed unavailable & non critical care bed
--•	Equipment failure/unavailable & equipment not available
--•	Lack of theatre time & list over run

		,case 
		when Can.CN_DESC = 'Administrative Error'
		then 'Administrative Error'
		when Can.CN_DESC = 'Hospital  reason'
		then 'Hospital Reason'
		when Can.CN_DESC = 'Lack of theatre time'
		then 'Lack of Theatre Time'	
		when Can.CN_DESC = 'Emergencies/Trauma'
		then 'Emergencies/Trauma'
		when Can.CN_DESC = 'List Overrun'
		then 'List Over Ran'
		--Following are combined
		when Can.CN_DESC in ('Bed Shortage','Ward beds unavailable')
		then 'Shortage of Beds'
		when Can.CN_DESC in ('ICU/HDU beds unavailable','No critical care beds')
		then 'Shortage of Critical Care Beds'
		when Can.CN_DESC in ('Equipment failure/unavailable','Equipment not available ')
		then 'Equipment Failure/Unavailable'
		when Can.CN_CODE in ('DNA','M-DNA')
		then 'Patient Cancellation DNA'
		--Anything else is combined under Other
		else 'Other'
		end as CancDescCombined
      ,FCC.[CC_DETAILS] as CancellationDetails
      ,PA_OPCOMM as OperationDetails

      
      ,OSUR.[SU_FNAME]+ ' ' + OSUR.[SU_LNAME] as OperatingSurgeon  
      --,SP.S1_CODE as ConsSpecCode
      ,SP.S1_DESC  as ConsSpecDescription
      ,Div.Direcorate as Directorate
      
      --,TH.[TH_SEQU] as TheatreCode
      ,TH.[TH_DESC] as TheatreDesc
      ,CSUR.[SU_FNAME]+ ' ' + CSUR.[SU_LNAME] as CancellingStaff
--,case when DATEDIFF(dd,FCC.[CC_DATE],[CC_DATE_TIME_CANCELLED]) = '0' then 'Cancelled Same Day'
--else 'None 28 Day' end as 'Cancelled Same Day?'
      
      ---- Unused fields
      --,FCC.[CC_PREMED]
      --,FCC.[CC_FASTED]
      --,FCC.[CC_FILLER_01]
      --,FCC.[CC_SURG_SPECL]
      --,FCC.[CC_LOG_DATE]
      --,FCC.[CC_LOG_DETAILS_FILLER]
      ----,FCC.[CC_CN_SEQU]

      --,FCC.[CC_WA_SEQU]
      --,FCC.[CC_CL_SEQU]
      --,FCC.[CC_PA_SEQU]
      --,FCC.[CC_SEQU]
      ----,FCC.[CC_S1_SEQU]

      --,FCC.[CC_CA_SEQU]
      --,FCC.[CC_OP_SEQU]
      --,FCC.[CC_TH_CODE]
      --,FCC.[CC_TH_SEQU]
      ----,FCC.[CC_CANCEL_SU_SEQU]
      --,FCC.[CC_GLI_SEQU]
      --,FCC.[CC_CLP_SEQU]
      --,FCC.[CC_ADMIT_DATE]
      --,FCC.[CC_MNAME]
      --,FCC.[CC_REBOOKED_PA_SEQU]
      
--,FCC.CC_OP_SEQU
--, OP_SEQU
--,OP_TA_SEQU
--,TA.TA_DESCRIPTION     
      
      
  FROM [ORMIS].[dbo].[FCCITEMS] FCC
  
	Left Join [ORMIS].[dbo].[FCANCEL] Can		
	--Cancellation Reasons
			on   FCC.CC_CN_SEQU = Can.CN_SEQU
			
	Left Join  [ORMIS].[dbo].[F_Cancel_Group] CG 
	--Cancellation Reason Group (e.g. Hospital Cancellation- Non-Clinical)
			on	Can.[CN_CG_SEQU] = CG.CG_SEQU	
			
	Left Join [ORMIS].[dbo].FSURGN OSUR			
	--Operating Surgeon
		on	 FCC.CC_SU_SEQU = oSUR.SU_SEQU	
			
	Left Join [ORMIS].[dbo].[FS1SPEC] SP		
	--Specialty of SURGEON
			on	OSUR.SU_S1_SEQU = SP.S1_SEQU	
			
	LEFT OUTER JOIN [WHOLAP].[dbo].[OlapSpecialityDivision] Div
			on SP.S1_CODE = Div.[SubSpecialtyCode]
				
	Left Join [ORMIS].[dbo].FSURGN CSUR			
	--Cancelling Member of Staff
			on	 FCC.CC_CANCEL_SU_SEQU = cSUR.SU_SEQU

	Left Join [ORMIS].[dbo].[FTHEAT] TH			
	--Theatre
			on	 FCC.CC_TH_SEQU = TH.TH_SEQU	
				
	Left Join [ORMIS].[dbo].F_Clinical_Priority CP		
	--Clinical Priority
			on FCC.CC_CLP_SEQU = CP.CLP_SEQU
			
  left join [ORMIS].[dbo].[FPATS] FPATS
  	--Operation Details (e.g. Type of Admission, Procedure)
  on FCC.CC_PA_SEQU = FPATS.PA_SEQU 
  
  left join [ORMIS].[dbo].[F_Type_of_Admission] TOA
  --Description of Type of Admission
  on FPATS.PA_TA_SEQU = TOA.TA_SEQU
			
	--Left Join [ORMIS].[dbo].[FOPERAT] FO				
	--Operation Details (e.g. Type of Admission) doesn't link many patients though
	--on FCC.CC_OP_SEQU = OP_SEQU
	--Following email from CSC we know longer use this table
	--If the CC_OP_SEQU is zero, then the cancellation was done before the Operation record (FOPERAT) was created 
	--ie Booking state The Booking table (FPATS) pointer can be used to link to the FCCITEMS table. FPATS.PA_SEQU = FCCITEMS.CC_PA_SEQU.
			
						LEFT  JOIN WHOLAP.dbo.OlapCalendar	 AS cal
						ON [CC_DATE] = cal.TheDate

			
  where 


	FCC.[CC_DATE] between  @StartDate and @EndDate 
	and
	(
	(
	DATEDIFF(dd,FCC.[CC_DATE],[CC_DATE_TIME_CANCELLED]) = '0' -- i.e. Cancelled Same Day
	and
	Can.[CN_CG_SEQU] = '1' -- i.e. Hospital Cancellation- Non-Clinical
	)
	)
	and 
	(FPATS.PA_TA_SEQU not in ('16','19') -- i.e. Not Day Case
	or
	FPATS.PA_TA_SEQU is null)		
	--and
	--Can.CN_SEQU = '32'		
	 --Does not find 100% of Int Management Details so need Nulls
	 
	 order by
	 Can.CN_DESC
	 ,Div.Division
	 ,Div.Direcorate
	 ,Div.SpecialtyFunction