﻿/*
--Author: N Scattergood
--Date created: 23/01/2013
--Stored Procedure Built for SRSS Report on:
--Theatre Data Quality Report where Operations are recorded without a Session
Unique ID which means they are not linked to a Session for Session Level Analysis
*/

/*
 Modified Date				Modified By			Modification
-----------------			------------		------------
28/08/2014					CMan				Amended stored procedure to include operation start date and operation end date so that theste fields can be included into the SSRS report
												as per CBH request - following conversation with SMann with regards to linking ops within the working day to a session



*/







CREATE Procedure [TH].[ReportOperationsWithoutSessionUniqueID]

@StartDate as Date
,@EndDate as Date
,@SuitePar  as nvarchar(max)
,@TheatrePar as nvarchar(max)
,@SpecPar as nvarchar(max)
,@OpTypePar as nvarchar(max)

as

select  
--B.StartTime as SessionStartTime
--,B.CancelledFlag as SessionCancelledFlag
--,B.SourceUniqueID,
--th.SessionSourceUniqueID
--,th.OperationCancelledFlag
--,th.Operation
[DistrictNo]
      ,[Forename] + ' ' + [Surname] PatientName
      --,th.SourceUniqueID
      --,[OperationStartDate]
  ,[SexCode]
      --,[OperationEndDate]
      ,[DateOfBirth]
      ,[OperationTypeCode]

           ,[OperationDate]
           ,[Operation]
           ,[OperationStartDate]
           ,[OperationEndDate]
      --,[PatientDiedFlag]
     
      
,S.Specialty as ConsultantSpecialty
,cons.StaffName as Consultant
--,SCat.StaffCategory As [Consultant Category]
--,Surg1.StaffName as Surgeon1
--,Surg2.StaffName as Surgeon2
      ,T.[Theatre]
      ,T.[OperatingSuite]

--,cast(th.OperationStartDate as time) as OperationStartTime
--,cast(th.AnaestheticInductionTime as time) AnaestheticInductionTime
--,cast(th.OperationEndDate as time) OperationEndDate
--,B.CancelledFlag

FROM wh.Theatre.OperationDetail th

	--inner join WHOLAP.dbo.OlapCalendar cal 
	--on cal.TheDate=th.OperationDate

		Left JOIN WHOLAP.dbo.OlapTheatreStaff Cons 
		on cons.StaffCode=th.ConsultantCode
		
		--left join wh.Theatre.StaffCategory SCat
		--on Cons.StaffCategoryCode = SCat.StaffCategoryCode
		
				--inner join WHOLAP.dbo.OlapTheatreStaff Surg1 
				--	on Surg1.StaffCode=th.Surgeon1Code

				--inner join WHOLAP.dbo.OlapTheatreStaff Surg2
				--	on Surg2.StaffCode=th.Surgeon2Code		

			Left JOIN [WHOLAP].[dbo].[OlapTheatreSpecialty] S
			on Cons.SpecialtyCode = S.SpecialtyCode
				
						Left Join [WHOLAP].[dbo].[OlapTheatre] T
						on th.TheatreCode = T.TheatreCode
						

where 

TH.OperationCancelledFlag = 0
and
th.SessionSourceUniqueID = 0
and 

-------Parameters----------
TH.OperationDate between @StartDate and @EndDate

	
	and T.OperatingSuiteCode in (SELECT Val from dbo.fn_String_To_Table(@SuitePar,',',1))
	and T.TheatreCode in (SELECT Val from dbo.fn_String_To_Table(@TheatrePar,',',1))
	and S.SpecialtyCode in (SELECT Val from dbo.fn_String_To_Table(@SpecPar,',',1))
	and th.OperationTypeCode in (SELECT Val from dbo.fn_String_To_Table(@OpTypePar,',',1))
	
order by 
th.OperationDate
,th.SessionSourceUniqueID
    ,T.[OperatingSuite]     
    ,T.[Theatre]