﻿/*
--Author: N Scattergood
--Date created: 23/01/2013
--Stored Procedure Built for SRSS Report on:
--Theatre Data Quality Report where Operations are recorded without a Session
Unique ID which means they are not linked to a Session for Session Level Analysis
--This Query Provides fields for the Parameters
*/


Create Procedure [TH].[ReportOperationsWithoutSessionUniqueID_Theatre]

@StartDate as Date
,@EndDate as Date
,@SuitePar  as nvarchar(max)
--,@TheatrePar as nvarchar(max)
--,@SpecPar as nvarchar(max)

as

select  
Distinct
     
     --T.OperatingSuiteCode	as 'SuiteCodePar'
     --   ,T.[OperatingSuite] as 'SuitePar'   

	T.[TheatreCode]	as 'TheatreCodePar'
	,T.[Theatre]		as 'TheatrePar'
	--S.Specialty		as 'SpecPar'
	--,S.SpecialtyCode	as 'SpecCodePar'


FROM wh.Theatre.OperationDetail th

	--inner join WHOLAP.dbo.OlapCalendar cal 
	--on cal.TheDate=th.OperationDate

		Left JOIN WHOLAP.dbo.OlapTheatreStaff Cons 
		on cons.StaffCode=th.ConsultantCode
		
		--left join wh.Theatre.StaffCategory SCat
		--on Cons.StaffCategoryCode = SCat.StaffCategoryCode
		
				--inner join WHOLAP.dbo.OlapTheatreStaff Surg1 
				--	on Surg1.StaffCode=th.Surgeon1Code

				--inner join WHOLAP.dbo.OlapTheatreStaff Surg2
				--	on Surg2.StaffCode=th.Surgeon2Code		

			Left JOIN [WHOLAP].[dbo].[OlapTheatreSpecialty] S
			on Cons.SpecialtyCode = S.SpecialtyCode
				
						Left Join [WHOLAP].[dbo].[OlapTheatre] T
						on th.TheatreCode = T.TheatreCode
						

where 

TH.OperationCancelledFlag = 0
and
th.SessionSourceUniqueID = 0
and 

-------Parameters----------
TH.OperationDate between @StartDate and @EndDate

	
	and T.OperatingSuiteCode in (SELECT Val from dbo.fn_String_To_Table(@SuitePar,',',1))
	--and T.TheatreCode in (SELECT Val from dbo.fn_String_To_Table(@TheatrePar,',',1))
	--and S.SpecialtyCode in (SELECT Val from dbo.fn_String_To_Table(@SpecPar,',',1))

order by 
T.Theatre