﻿/*
--Author: N Scattergood
--Date created: 28/06/2013
--Stored Procedure Built for ReportSessionBreakdownDrillthough
--
*/


CREATE Procedure [TH].[ReportSessionBreakdownDrillthough]

@SessionID as int

as
select 
--distinct
op.sourceuniqueid,
sess.SourceUniqueID as sessionid
,op.OperationOrder,
sess.ConsultantCode,
op.SourceUniqueID as opid,
fp.PR_CODE,
isnull(fp.PR_DESC,'n/a') as delaydescription,
isnull(fi.DIT_SOURCE,'n/a') as delaystage,
isnull(fi.DIT_FREE_TEXT,'n/a') as delaytext,
staff.Surname+' '+staff.Forename as Consultant,
spec.Specialty,
op.DistrictNo,
op.Operation,
op.InSuiteTime,
op.InAnaestheticTime,
op.AnaestheticReadyTime,
op.AnaestheticInductionTime,
op.OperationStartDate,
pd.ProcedureStartTime,
pd.ProcedureEndTime,
op.OperationEndDate,
op.InRecoveryTime,
op.DischargeTime,
case when operationcancelledflag=1 then 'Cancelled' else null end as CancellationStatus
,fi.dit_minutes as DelayMinutes
,case DIT_SOURCE when 'SESSION' THEN 1
WHEN 'PORT_SENT' THEN 2
WHEN 'IN_SUITE' THEN 3
WHEN 'IN_ANAES' THEN 4
WHEN 'INDUCTION' THEN 5
WHEN 'IN_OR' THEN 6
ELSE 7 END AS DelayOrder


from 
WH.Theatre.[Session] sess 

left outer join	WH.Theatre.OperationDetail op 
on sess.SourceUniqueID=op.SessionSourceUniqueID 


 LEFT OUTER JOIN [WH].Theatre.ProcedureDetail AS pd 
ON pd.OperationDetailSourceUniqueID = op.SourceUniqueID 
and pd.PrimaryProcedureFlag = 1

LEFT OUTER JOIN ORMIS.dbo.F_Delay_Items AS fi WITH (nolock) 
ON op.SourceUniqueID = fi.DIT_OP_SEQU 

LEFT OUTER JOIN ORMIS.dbo.FPROCDY AS fp WITH (nolock) 
ON fp.PR_SEQU = fi.DIT_PR_SEQU

left join WH.Theatre.Staff staff 
on staff.StaffCode = sess.ConsultantCode
                        
left join WH.Theatre.Specialty spec 
on spec.SpecialtyCode=sess.SpecialtyCode
                  
                  where  
                  sess.SourceUniqueID IN(@SessionID) 
                  --= '153370'
                  
                  
                  
                  order by sess.SourceUniqueID,OperationOrder