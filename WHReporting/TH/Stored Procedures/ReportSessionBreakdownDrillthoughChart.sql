﻿/*
--Author: N Scattergood
--Date created: 28/06/2013
--Stored Procedure Built for ReportSessionBreakdownDrillthough
--Provides figures for ProcedureChart
*/


CREATE Procedure [TH].[ReportSessionBreakdownDrillthoughChart]

@SessionID as int
as
select 
op.DistrictNo
,op.OperationOrder
,DATEDIFF(minute,op.InSuiteTime,op.InAnaestheticTime) as 'In Suite'
,DATEDIFF(minute,op.InAnaestheticTime,op.AnaestheticInductionTime) as 'In Anaesthetic'
,DATEDIFF(minute,op.AnaestheticInductionTime,op.OperationStartDate) as 'Anaesthetic'
,DATEDIFF(minute,op.OperationStartDate,pd.ProcedureStart) as 'In OR'
,DATEDIFF(minute,pd.ProcedureStart,pd.ProcedureEnd) as 'Procedure Time'
,DATEDIFF(minute,pd.ProcedureEnd,op.InRecoveryTime) as 'Out of OR'


from 
WH.Theatre.[Session] sess 

left outer join	WH.Theatre.OperationDetail op 
on sess.SourceUniqueID=op.SessionSourceUniqueID 

 LEFT OUTER JOIN 
 (select p.OperationDetailSourceUniqueID
 ,MIN(P.ProcedureStartTime) as ProcedureStart
 ,MAX(P.ProcedureEndTime) as ProcedureEnd
 from
 [WH].Theatre.ProcedureDetail P
 group by
 p.OperationDetailSourceUniqueID
 ) 
  AS pd 
ON pd.OperationDetailSourceUniqueID = op.SourceUniqueID 



 where  
 sess.SourceUniqueID IN(@SessionID) 
                  
                  
 order by 
 sess.SourceUniqueID,OperationOrder desc