﻿/*
--Author: Helen Ransome
--Date created: 16/05/2013
--Stored Procedure Built for SSRS Report on:
--Generic store procedure used for Cancer Type Parameter
*/

CREATE Procedure [TH].[ReportSessionTheatrePAR]

as
SELECT

SessionTheatre 
      
  FROM QLIK.TheatreSessions Sess
  Order by SessionTheatre