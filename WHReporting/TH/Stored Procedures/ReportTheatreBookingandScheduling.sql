﻿-- =============================================
-- Author:		CMan
-- Create date: 11/12/2014
-- Description:	Stored Procedure for the Booking and Scheduling SSRS report 
-- =============================================
CREATE PROCEDURE [TH].[ReportTheatreBookingandScheduling] @Date datetime





AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


/*
Questions to ask
-In the session table all future sessions appear to be marked as cancelled, eventhough in the Patient Booking table, the session unique id is attached to bookings within here - how is the booking done in the ORMIs front end.
Does the session remain cancelled until on the day?-ask SM.  Answer from SM - there is not such concept as a cancelled session on ORMIs fron end.  What happens if a session is physically cancelled, then SM will go into the ORMIS template and remove all the details for that session. 
With respct to the ORMIS back end tables, once all details pertaining to a session has been deleted from the front end no longer appears as a session on the front end and therefore does not come through into the sesioon table.
--How do the booking clerks link a booking to a WL as not all bookings are appearing in the Theatre patient booking table with a waiting list source unique id , despite the fact that it should be.
--Is the consultant on the waiting list always going to tbe the one which they are listed under
--What do we want to put in if they haven't entered the estimated time in the WL and there is now median time for that procedure and consultant?

*/
--DECLARE @Date DATETIME = '20150613'


--21/01/2015 CM:  SM advises that the booking team tend to record the anaesthetist against first procedure on the list
--Therefore - create temp table below which will effectively give us the anaesthetist for each session, have had to use a min as there are some instances where two bookings recorded against the same session id have been entered with two different anaesthetists. e.g. session id 188584
If OBJECT_ID ('tempdb..#LkpAnaes') > 0
Drop table #LkpAnaes
SELECT SessionSourceUniqueID
	,min(StaffCode1) AS AnaesthetistCode
into #LkpAnaes
FROM WH.Theatre.PatientBooking
LEFT OUTER JOIN WH.Theatre.Staff ON AnaesthetistCode = staff.StaffCode
WHERE IntendedProcedureDate >= @Date
	AND IntendedProcedureDate < DATEADD(day, 7, @Date)
GROUP BY SessionSourceUniqueID



--Temp table to get a list of bookings where the intended procedure date is within the dates selected
IF OBJECT_ID('tempdb..#Booking') > 0
	DROP TABLE #Booking


SELECT *
INTO #Booking
FROM (
	SELECT DISTINCT pb.SourceUniqueID AS BookingUniqueID
		,pb.DistrictNo
		,pb.IntendedProcedureDate
		,pb.TheatreCode
		,pb.ConsultantCode
		,pb.AnaesthetistCode
		,pb.SessionSourceUniqueID
		,cons.StaffCode1 AS BookingConsultantCode
		,isnull(anaes.StaffCode1, #lkpAnaes.AnaesthetistCode) AS BookingAnaesthetistCode
		,wl.SourceUniqueID
		,wl.Consultant
		,wl.EstimatedTheatreTime
		,cc.CODE
		,cc.DESCRIPTION
		,antyp.ReferenceValue AS WLAnaestheticType
		,pt.MedianOperationMinutes--median procedure time for last 12 months by consultant and procedure
		,ptp.MedianOperationMinutes AS MedianOperationMinutesByProc--median procedure time for last 12 months broken down by procedure only
		,CASE WHEN at.NumberOfCases = 1 and at.MedianAnaestheticMinutes > 100 Then NULL Else at.MedianAnaestheticMinutes END as MedianAnaestheticMinutes  --this is the median anaesthetic time for the last 12 months broken down by consultant and procedure
																																						--22/07/2015 CM Added case statement to say that if the median anaesthetic minutes is > 100 and there is only one historic case to look at then leave numm as this will then automatically send it to pick up the mediananaestheticminutesbyproc in the calculation of the median anaesthetic minutes final 
		,atp.MedianAnaestheticMinutes AS MedianAnaestheticMinutesByProc --this is the median anaesthetic time broken by procedure only 
		,CASE WHEN  atla.NumberOfCases = 1 and atla.MedianAnaestheticMinutes > 100 then NULL Else atla.MedianAnaestheticMinutes  END as MedianAnaestheticMinutesLA--this is the median anaesthetic time for the last 12 months broken down by consultant and procedure- LA procedures only
																																								--22/07/2015 CM Added case statement to say that if the median anaesthetic minutes is > 100 and there is only one historic case to look at then leave numm as this will then automatically send it to pick up the mediananaestheticminutesbyproclain the calculation of the median anaesthetic minutes final  
		,atpla.MedianAnaestheticMinutes AS MedianAnaestheticMinutesByProcLA --this is the median anaesthetic time broken by procedure only   - LA procedures only
		,CASE 
			WHEN (
					wl.EstimatedTheatreTime = 0
					OR wl.EstimatedTheatreTime IS NULL
					)
				AND isnull(pt.MedianOperationMinutes, ptp.MedianOperationMinutes) IS NULL
				THEN 1
			ELSE 0
			END AS NoOpTimeFlag
		,NULL AS MedianAnaestheticMinutesFinal--this is the median anaesthetic time to be added into the final report - i.e. mapping to the breakdown by consultant and procedure where there is one and then to the breakdown by procedure only if there isn't one.  Also as agreed with MOsborne, if anaesthetic mins < 10 then minimum cap at 10 
		,ISNULL(pt.MedianOperationMinutes, ptp.MedianOperationMinutes) as MedianOperationMinutesFinal
		,NULL AS NoAnaestheticTimeFlag--field to help flag up ops which do not have a median anaesthetic time included in the duration calculation
		, pb.AnaestheticTypeCode
	FROM WH.Theatre.PatientBooking pb
	LEFT OUTER JOIN WH.PTL.IPWL wl ON pb.PatientEpisodeNumber = wl.SourceUniqueID
	LEFT OUTER JOIN (
		SELECT *
		FROM WH.PAS.ClinicalCodingBase
		WHERE SORCE_CODE = 'WLIST'
			AND SORT_ORDER = 1
			AND ARCHV_FLAG = 'N'
			AND DPTYP_CODE = 'PROCE'
		) cc ON cast(pb.PatientEpisodeNumber AS INT) = cc.SORCE_REFNO
	LEFT OUTER JOIN #LkpAnaes ON pb.SessionSourceUniqueID = #LkpAnaes.SessionSourceUniqueID -- bring this temp table in so that the AnaesthetistCode field can be more complete - if the actual patient booking itself does not ahve an anesthetist recorded against it then bring in the anesthetist from the temp table above as an isnull - see BookingAnaesthetistCode field above
	LEFT OUTER JOIN WH.Theatre.Staff cons ON pb.ConsultantCode = cons.StaffCode
	LEFT OUTER JOIN WH.Theatre.Staff anaes ON pb.AnaesthetistCode = anaes.StaffCode
	LEFT OUTER JOIN WHReporting.TH.TheatreProcedureTimes pt ON cons.StaffCode1 = pt.OperationDetailConsultantCode
		AND left(cc.CODE, 3) = pt.PrimaryProcedureCodeGrouped
	LEFT OUTER JOIN WHReporting.TH.TheatreAnaestheticTimes at ON isnull(anaes.StaffCode1, #lkpAnaes.AnaesthetistCode) = at.AnaesthetistCode and (pb.AnaestheticTypeCode not like '%LA%' or pb.AnaestheticTypeCode is null)
		AND left(cc.CODE, 3) = at.PrimaryProcedureCodeGrouped
	LEFT OUTER JOIN (
		SELECT *
		FROM WH.PAS.ReferenceValue
		WHERE ReferenceDomainCode = 'ANTYP'
		) antyp ON wl.AnaestheticType = antyp.MainCode
	LEFT OUTER JOIN WHREPORTING.TH.TheatreAnaestheticTimesByProcedureOnly atp ON left(cc.CODE, 3) = atp.PrimaryProcedureCodeGrouped  and (pb.AnaestheticTypeCode not like '%LA%' or pb.AnaestheticTypeCode is null)
	LEFT OUTER JOIN WHREPORTING.TH.TheatreProcedureTimesByProcedureOnly ptp ON left(cc.CODE, 3) = ptp.PrimaryProcedureCodeGrouped
	LEFT OUTER JOIN WHReporting.TH.TheatreAnaestheticTimesLA atla on isnull(anaes.StaffCode1, #lkpAnaes.AnaesthetistCode) = atla.AnaesthetistCode  and pb.AnaestheticTypeCode like '%LA%' and left(cc.CODE, 3) = atla.PrimaryProcedureCodeGrouped  
	LEFT OUTER JOIN WHREPORTING.TH.TheatreAnaestheticTimesByProcedureOnly atpla ON left(cc.CODE, 3) = atpla.PrimaryProcedureCodeGrouped  and pb.AnaestheticTypeCode like '%LA%'
	WHERE IntendedProcedureDate >= @Date
		AND IntendedProcedureDate < DATEADD(day, 7, @Date)
		AND pb.PatientEpisodeNumber <> '' --this will select the cohort of patients where the ORMIS booking is attached to a PAS waiting list
		AND pb.SourceUniqueID NOT IN (
			SELECT PatientSourceUniqueID
			FROM WH.Theatre.Cancellation
			WHERE Dateadd(day,DATEDIFF(DAY,0,CancellationDate),0)<> ProposedOperationDate
			)
	
	UNION ALL
	
	SELECT DISTINCT pb.SourceUniqueID AS BookingUniqueID
		,pb.DistrictNo
		,pb.IntendedProcedureDate
		,pb.TheatreCode
		,pb.ConsultantCode
		,pb.AnaesthetistCode
		,pb.SessionSourceUniqueID
		,cons.StaffCode1 AS BookingConsultantCode
		,isnull(anaes.StaffCode1, #lkpAnaes.AnaesthetistCode) AS BookingAnaesthetistCode
		,wl.SourceUniqueID
		,wl.Consultant
		,wl.EstimatedTheatreTime
		,cc.CODE
		,cc.DESCRIPTION
		,antyp.ReferenceValue AS WLAnaestheticType
		,pt.MedianOperationMinutes--median procedure time for last 12 months by consultant and procedure
		,ptp.MedianOperationMinutes AS MedianOperationMinutesByProc --median procedure time for last 12 months broken down by procedure only
		,CASE WHEN at.NumberOfCases = 1 and at.MedianAnaestheticMinutes > 100 Then NULL Else at.MedianAnaestheticMinutes END as MedianAnaestheticMinutes  --this is the median anaesthetic time for the last 12 months broken down by consultant and procedure
																																							--22/07/2015 CM Added case statement to say that if the median anaesthetic minutes is > 100 and there is only one historic case to look at then leave numm as this will then automatically send it to pick up the mediananaestheticminutesbyproc in the calculation of the median anaesthetic minutes final  
		,atp.MedianAnaestheticMinutes AS MedianAnaestheticMinutesByProc --this is the median anaesthetic time broken by procedure only 
		,CASE WHEN  atla.NumberOfCases = 1 and atla.MedianAnaestheticMinutes > 100 then NULL Else atla.MedianAnaestheticMinutes  END as MedianAnaestheticMinutesLA--this is the median anaesthetic time for the last 12 months broken down by consultant and procedure- LA procedures only
																																									--22/07/2015 CM Added case statement to say that if the median anaesthetic minutes is > 100 and there is only one historic case to look at then leave numm as this will then automatically send it to pick up the mediananaestheticminutesbyprocla in the calculation of the median anaesthetic minutes final  
		,atpla.MedianAnaestheticMinutes AS MedianAnaestheticMinutesByProcLA --this is the median anaesthetic time broken by procedure only   - LA procedures only

		,CASE 
			WHEN (
					wl.EstimatedTheatreTime = 0
					OR wl.EstimatedTheatreTime IS NULL
					)
				AND isnull(pt.MedianOperationMinutes, ptp.MedianOperationMinutes) IS NULL
				THEN 1
			ELSE 0
			END AS NoOpTimeFlag
		,NULL AS MedianAnaestheticMinutesFinal--this is the median anaesthetic time to be added into the final report - i.e. mapping to the breakdown by consultant and procedure where there is one and then to the breakdown by procedure only if there isn't one.  Also as agreed with MOsborne, if anaesthetic mins < 10 then minimum cap at 10 , if there is no match at all assign 10
		,ISNULL(pt.MedianOperationMinutes, ptp.MedianOperationMinutes) as MedianOperationMinutesFinal
		,NULL as NoAnaestheticTimeFlag--field to help flag up ops which do not have a median anaesthetic time included in the duration calculation
		, pb.AnaestheticTypeCode
		FROM WH.Theatre.PatientBooking pb
	LEFT OUTER JOIN WH.PTL.IPWL wl ON pb.DistrictNo = wl.DistrictNo
		AND (
			pb.IntendedProcedureDate = DateAdd(day, Datediff(day, 0, wl.TCIDate), 0)
			OR pb.IntendedProcedureDate = DateAdd(day, Datediff(day, 0, wl.TCIDate) + 1, 0)
			)
	LEFT OUTER JOIN (
		SELECT *
		FROM WH.PAS.ClinicalCodingBase
		WHERE SORCE_CODE = 'WLIST'
			AND SORT_ORDER = 1
			AND ARCHV_FLAG = 'N'
			AND DPTYP_CODE = 'PROCE'
		) cc ON cast(wl.SourceUniqueID AS INT) = cc.SORCE_REFNO
	LEFT OUTER JOIN #LkpAnaes ON pb.SessionSourceUniqueID = #LkpAnaes.SessionSourceUniqueID -- bring this temp table in so that the AnaesthetistCode field can be more complete - if the actual patient booking itself does not ahve an anesthetist recorded against it then bring in the anesthetist from the temp table above as an isnull - see BookingAnaesthetistCode field above
	LEFT OUTER JOIN WH.Theatre.Staff cons ON pb.ConsultantCode = cons.StaffCode
	LEFT OUTER JOIN WH.Theatre.Staff anaes ON pb.AnaesthetistCode = anaes.StaffCode
	LEFT OUTER JOIN WHReporting.TH.TheatreProcedureTimes pt ON cons.StaffCode1 = pt.OperationDetailConsultantCode
		AND left(cc.CODE, 3) = pt.PrimaryProcedureCodeGrouped
	LEFT OUTER JOIN WHReporting.TH.TheatreAnaestheticTimes at ON isnull(anaes.StaffCode1, #lkpAnaes.AnaesthetistCode) = at.AnaesthetistCode and (pb.AnaestheticTypeCode not like '%LA%' or pb.AnaestheticTypeCode is null)
		AND left(cc.CODE, 3) = at.PrimaryProcedureCodeGrouped
	LEFT OUTER JOIN (
		SELECT *
		FROM WH.PAS.ReferenceValue
		WHERE ReferenceDomainCode = 'ANTYP'
		) antyp ON wl.AnaestheticType = antyp.MainCode
	LEFT OUTER JOIN WHREPORTING.TH.TheatreAnaestheticTimesByProcedureOnly atp ON left(cc.CODE, 3) = atp.PrimaryProcedureCodeGrouped and (pb.AnaestheticTypeCode not like '%LA%' or pb.AnaestheticTypeCode is null)
	LEFT OUTER JOIN WHREPORTING.TH.TheatreProcedureTimesByProcedureOnly ptp ON left(cc.CODE, 3) = ptp.PrimaryProcedureCodeGrouped
	LEFT OUTER JOIN WHReporting.TH.TheatreAnaestheticTimesLA atla on isnull(anaes.StaffCode1, #lkpAnaes.AnaesthetistCode) = atla.AnaesthetistCode  and pb.AnaestheticTypeCode like '%LA%' 	AND left(cc.CODE, 3) = atla.PrimaryProcedureCodeGrouped
	LEFT OUTER JOIN WHREPORTING.TH.TheatreAnaestheticTimesByProcedureOnly atpla ON left(cc.CODE, 3) = atpla.PrimaryProcedureCodeGrouped  and pb.AnaestheticTypeCode like '%LA%'	
WHERE IntendedProcedureDate >= @Date
		AND IntendedProcedureDate < DATEADD(day, 7, @Date)
		AND PatientEpisodeNumber = '' --this will be the cohort of patients who have an ORMIS booking without it being linked to a waiting list.  Different matching criteria is used for these ones.--see left join above
		AND pb.SourceUniqueID NOT IN (
			SELECT PatientSourceUniqueID
			FROM WH.Theatre.Cancellation
			WHERE Dateadd(day,DATEDIFF(DAY,0,CancellationDate),0)<> ProposedOperationDate
			)
	) booking




--Update the Median Anaesthetic minutes final field 
---this is the median anaesthetic time to be added into the final report - i.e. mapping to the breakdown by consultant and procedure where there is one and then to the breakdown by procedure only if there isn't one.  Also as agreed with MOsborne, if anaesthetic mins < 10 then minimum cap at 10 , if there is no match at all assign 10
--further logic as discussed with Sarah Manns is to take into account the anaesthetic type - if GA ot other type then set it to 10 mins if no anaesthetic time , if LA set this to 5 minutes

Update  #booking
SET NoAnaestheticTimeFlag = CASE when anaesthetictypecode not like '%LA%' and  ISNULL(MedianAnaestheticMinutes, MedianAnaestheticMinutesByProc)IS NULL
				THEN 1
			when anaesthetictypecode IS NULL  and  ISNULL(MedianAnaestheticMinutes, MedianAnaestheticMinutesByProc)IS NULL
				THEN 1
			when anaesthetictypecode like 'LA'  and  ISNULL(MedianAnaestheticMinutesLA, MedianAnaestheticMinutesByProcLA)IS NULL
				THEN 1
			ELSE 0
			END,
MedianAnaestheticMinutesFinal =  CASE 
		WHEN anaesthetictypecode NOT LIKE '%LA%'
			THEN CASE 
					WHEN Coalesce(MedianAnaestheticMinutes, MedianAnaestheticMinutesByProc, '10') < 10
						THEN 10
					ELSE Coalesce(MedianAnaestheticMinutes, MedianAnaestheticMinutesByProc, '10')
					END
		WHEN anaesthetictypecode IS NULL
			THEN CASE 
					WHEN Coalesce(MedianAnaestheticMinutes, MedianAnaestheticMinutesByProc, '10') < 10
						THEN 10
					ELSE Coalesce(MedianAnaestheticMinutes, MedianAnaestheticMinutesByProc, '10')
					END
		WHEN anaesthetictypecode LIKE 'LA'
			THEN Coalesce(MedianAnaestheticMinutesLA, MedianAnaestheticMinutesByProcLA, '5')
		ELSE NULL
		END
From #booking 





------
--select * -- distinct code, description 
--from #Booking
--where noanaesthetictimeflag = 1


/*Run these queries to get the patient level data behind the summary
--Joining Sessions to booking to determine the duration for each op scheduled in the session to get booking %
--Using WH.Theatre.Session
Select distinct  ss.SourceUniqueID, ss.TheatreCode, th.Theatre, ss.ConsultantCode, ss.StartTime, ss.EndTime, ss.PlannedSessionMinutes , ss.CancelledFlag, bk.DistrictNo, bk.IntendedProcedureDate, bk.EstimatedTheatreTime, bk.Code as PrimaryProcCode, bk.Description as PrimaryProcDescription,Cast(bk.MedianOperationMinutes as Decimal(16,0)) as MedianOpTime
from WH.Theatre.[Session] ss
Left outer join WH.Theatre.Theatre th on ss.TheatreCode = th.TheatreCode
left outer join #Booking bk on ss.SourceUniqueID = bk.SessionSourceUniqueID

where DateAdd(day,DATEDIFF(day,0,ss.StartTime),0) >= @Date 
and  DateAdd(day,DATEDIFF(day,0,ss.StartTime), 0)  < DATEADD(day,7,@Date)

Order by 1


--Using WHReporting.QLIK.TheatreSessions
Select distinct  ss.SessionSourceUniqueID, ss.SessionTheatreType,ss.SessionTheatreCode, ss.SessionTheatre,ss.SessionConsultantCode, ss.SessionConsultant, ss.SessionStartTime, ss.SessionEndTime, ss.PlannedSessionMinutes , ss.SessionCancelledFlag, bk.DistrictNo, bk.IntendedProcedureDate, bk.EstimatedTheatreTime,bk.Code as PrimaryProcCode, bk.Description as PrimaryProcDescription,Cast(bk.MedianOperationMinutes as Decimal(16,0))  as MedianOpTime
from WHREPORTING.QLIK.TheatreSessions ss--WH.Theatre.[Session] ss
--Left outer join WH.Theatre.Theatre th on ss.TheatreCode = th.TheatreCode
left outer join #Booking bk on ss.SessionSourceUniqueID = bk.SessionSourceUniqueID

where DateAdd(day,DATEDIFF(day,0,ss.SessionStartTime),0) >= @Date 
and  DateAdd(day,DATEDIFF(day,0,ss.SessionStartTime), 0)  < DATEADD(day,7,@Date)

Order by 1
*/



--Query to get summary
--Using WHReporting.QLIK.TheatreSessions
SELECT cal.DayOfWeek
	,DateAdd(day, DATEDIFF(day, 0, ss.SessionStartTime), 0) as SessionDate, 
	ss.SessionSourceUniqueID
	,ss.SessionDirectorate
	,ss.SessionSpecialty
	,ss.SessionOperatingSuite
	,ss.SessionTheatreType
	,ss.SessionTheatreCode
	,ss.SessionTheatre
	,ss.SessionConsultantCode
	,ISNULL(ss.SessionConsultant, 'Unknown') as SessionConsultant
	,ss.SessionStartTime
	,ss.SessionEndTime
	,ss.PlannedSessionMinutes
	,ss.SessionCancelledFlag
	,CASE WHEN ss.ORMISSessionTheatreType  = 'ELE' then 'Elective'
		WHEN ss.ORMISSessionTheatreType  = 'EMER' then 'Emergency' 
		WHEN ss.ORMISSessionTheatreType  = 'TRAU' then 'Trauma' 
		WHEN ss.ORMISSessionTheatreType  = 'WLI' then 'WLI' 
		ELse 'Unknown'
		END as ORMISSessionTheatreType	--07/03/2016 CM - Added this field to help identify ORMIS Theatre Session Type of Trauma or Emergency as per W Nickerson phone call
	,SUM(CASE	WHEN bk.BookingConsultantCode <> bk.Consultant then isnull(bk.MedianOperationMinutesFinal,0) + isnull(bk.MedianAnaestheticMinutesFinal,0)--09/03/2015 CM rule added to use median time if the Consultant on the waiting list is different to that on the booking.
				WHEN bk.EstimatedTheatreTime = 0 then isnull(bk.MedianOperationMinutesFinal,0) + isnull(bk.MedianAnaestheticMinutesFinal,0)--if estimated op duration is 0 has been added to the WL then use the median op time by consultant and prim proc, or just median time by prim proc if there is not one for the consultant
				WHEN bk.EstimatedTheatreTime IS NULL then isnull(bk.MedianOperationMinutesFinal,0) + isnull(bk.MedianAnaestheticMinutesFinal,0)--if estimated op duration is null has been added to the WL then use the median op time by consultant and prim proc, or just median time by prim proc if there is not one for the consultant
				ELSE bk.EstimatedTheatreTime+ Isnull(bk.MedianAnaestheticMinutesFinal,0)--09/03/2015 as per CBH email from 06/03/2015 - median anesthetic time is to be added to the estimated procedure time.
				END) as BookedDuration-- commented this out and use calculation below which does not include Anaesthetic times for the moment until we have had meeting with MOsborne 
	--,SUM(CASE WHEN bk.EstimatedTheatreTime = 0 then isnull(bk.MedianOperationMinutes,0)
	--			WHEN bk.EstimatedTheatreTime IS NULL then isnull(bk.MedianOperationMinutes,0)
	--			ELSE bk.EstimatedTheatreTime
	--			END) as BookedDuration
	, COUNT (distinct BookingUniqueId) as NumberOfBookings
	, SUM(NoOpTimeFlag) as NumberWithNoOpTime
	, SUM(NoAnaestheticTimeFlag) as NumberWithNoAnaestheticTime
	, SUM(CASE WHEN bk.BookingConsultantCode <> bk.Consultant then 1 Else 0 End) as NumberWhereBookingConsAndWLConsDifferent 
	--,SUM(CASE 
	--		WHEN Coalesce(bk.EstimatedTheatreTime, Cast(bk.MedianOperationMinutes AS DECIMAL(16, 0))) IS NULL
	--			THEN 0
	--		ELSE Coalesce(bk.EstimatedTheatreTime, Cast(bk.MedianOperationMinutes AS DECIMAL(16, 0)))
	--		END) AS BookedDuration --, bk.DistrictNo, bk.IntendedProcedureDate, bk.EstimatedTheatreTime,bk.Code as PrimaryProcCode, bk.Description as PrimaryProcDescription, Round(bk.MedianOperationMinutes,0) as MedianOpTime
FROM WHREPORTING.QLIK.TheatreSessions ss --WH.Theatre.[Session] ss
LEFT JOIN #Booking bk ON ss.SessionSourceUniqueID = bk.SessionSourceUniqueID
LEFT OUTER JOIN  WHREPORTING.LK.Calendar cal on DateAdd(day, DATEDIFF(day, 0, ss.SessionStartTime), 0)= cal.TheDate

WHERE DateAdd(day, DATEDIFF(day, 0, ss.SessionStartTime), 0) >= @Date
	AND DateAdd(day, DATEDIFF(day, 0, ss.SessionStartTime), 0) < DATEADD(day, 7, @Date)
	AND	 (SessionTheatreType in ('Elective','DayCase')
	--	OR SessionTheatreCode = '22'--TDC 2 to be on Booking and Scheduling Tool as per WNickerson email 23/02/2016
		)
	--AND SessionTheatreCode <> '22'--Exclude TDC 2 as users do not want it to appear on the Booking and Scheduling tool, but management want to view the productivoity for it therefore it needs to be categorised as an elective theatre for it to appear on Qlikview dashboard.
GROUP BY cal.DayOfWeek,
	DateAdd(day, DATEDIFF(day, 0, ss.SessionStartTime), 0) , 
	ss.SessionSourceUniqueID
	,ss.SessionDirectorate
	,ss.SessionSpecialty
	,ss.SessionOperatingSuite
	,ss.SessionTheatreType
	,ss.SessionTheatreCode
	,ss.SessionTheatre
	,ss.SessionConsultantCode
	,ISNULL(ss.SessionConsultant, 'Unknown')
	,ss.SessionStartTime
	,ss.SessionEndTime
	,ss.PlannedSessionMinutes
	,ss.SessionCancelledFlag
	,CASE WHEN ss.ORMISSessionTheatreType  = 'ELE' then 'Elective'
		WHEN ss.ORMISSessionTheatreType  = 'EMER' then 'Emergency' 
		WHEN ss.ORMISSessionTheatreType  = 'TRAU' then 'Trauma' 
		WHEN ss.ORMISSessionTheatreType  = 'WLI' then 'WLI' 
		ELse 'Unknown'
		END
ORDER BY 1


--Select * from #Booking where SessionSourceUniqueid =  '186031'


--Select * 
--from #Booking bk 
--left outer join  WHREPORTING.TH.TheatreAnaestheticTimes  at on bk.BookingAnaesthetistCode = at.anaesthetistcode and 
--where bk.BookingAnaesthetistCode  is NULL

--Select * from WHREPORTING.QLIK.TheatreSessions where SessionConsultant like '%onon%'
-- and SessionStartTime >= '20150105' and SessionStartTime < '20150106'
--Select * from #Booking
--where sessionsourceuniqueid = '188273'
--Select * from  WHReporting.TH.TheatreProcedureTimes 
--where OperationDetailConsultantCode = 'C3451180'
--Order by PrimaryProcedureCodeGrouped

----example of booking being linked to wrong waiting list
--Select * from WH.PTL.IPWL
--where SourceUniqueID = '150842108'

--Select * from  WH.PTL.IPWL
--where DistrictNo = 'RM2425932'

--Select * from WH.Theatre.PatientBooking 
--where DistrictNo = 'RM2425932'


----Anaesthetic Types - 
--Select ReferenceValue from WH.PAS.ReferenceValue
--where ReferenceDomainCode = 'ANTYP'




END