﻿-- =============================================
-- Author:		CMan
-- Create date: 12/01/2015
-- Description:	Stored Procedure to support as drill through SSRS report to give the record level data behind the Theatre Booking and Scheduling - by Theatre report 
-- =============================================
CREATE PROCEDURE [TH].[ReportTheatreBookingandScheduling_TheatrePatientLevel] @Date datetime,@Theatre varchar(100)
	-- Add the parameters for the stored procedure here

	


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	
--Declare @Date datetime = '20150722'

--21/01/2015 CM:  SM advises that the booking team tend to record the anaesthetist against first procedure on the list
--Therefore - create temp table below which will effectively give us the anaesthetist for each session, have had to use a min as there are some instances where two bookings recorded against the same session id have been entered with two different anaesthetists. e.g. session id 188584
If OBJECT_ID ('tempdb..#LkpAnaes') > 0
Drop table #LkpAnaes
SELECT SessionSourceUniqueID
	,min(StaffCode1) AS AnaesthetistCode
into #LkpAnaes
FROM WH.Theatre.PatientBooking
LEFT OUTER JOIN WH.Theatre.Staff ON AnaesthetistCode = staff.StaffCode
WHERE IntendedProcedureDate >= @Date
	AND IntendedProcedureDate < DATEADD(day, 7, @Date)
GROUP BY SessionSourceUniqueID




--Temp table to get a list of bookings where the intended procedure date is within the dates selected
IF OBJECT_ID('tempdb..#Booking') > 0
	DROP TABLE #Booking


SELECT *
INTO #Booking
FROM (
	SELECT DISTINCT pb.SourceUniqueID AS BookingUniqueID
		,pb.DistrictNo
		,pb.Forename + ' ' + pb.Surname as PatientName
		,pb.DateOfBirth
		,pb.IntendedProcedureDate
		,pb.TheatreCode
		,pb.ConsultantCode
		,pb.AnaesthetistCode
		,pb.SessionSourceUniqueID
		,cons.StaffCode1 AS BookingConsultantCode
		,cons.Forename + ' ' + cons.Surname as BookingConsultant
		,isnull(anaes.StaffCode1, #lkpAnaes.AnaesthetistCode) AS BookingAnaesthetistCode
		,ISNULL(anaes.Forename + ' ' + anaes.Surname, anaes2.Forename + ' ' + anaes2.Surname) as BookingAnaesthetist
		,wl.SourceUniqueID
		,wl.Consultant
		,WLCons.Forename + ' '  + WLCons.Surname as ConsultantName-- Waiting List Consultant 
		,wl.EstimatedTheatreTime
		,cc.CODE
		,cc.DESCRIPTION
		,antyp.ReferenceValue AS WLAnaestheticType
		,pt.MedianOperationMinutes--median procedure time for last 12 months by consultant and procedure
		,ptp.MedianOperationMinutes AS MedianOperationMinutesByProc--median procedure time for last 12 months broken down by procedure only
		,CASE WHEN at.NumberOfCases = 1 and at.MedianAnaestheticMinutes > 100 Then NULL Else at.MedianAnaestheticMinutes END as MedianAnaestheticMinutes  --this is the median anaesthetic time for the last 12 months broken down by consultant and procedure
																																						--22/07/2015 CM Added case statement to say that if the median anaesthetic minutes is > 100 and there is only one historic case to look at then leave numm as this will then automatically send it to pick up the mediananaestheticminutesbyproc in the calculation of the median anaesthetic minutes final 
		,atp.MedianAnaestheticMinutes AS MedianAnaestheticMinutesByProc --this is the median anaesthetic time broken by procedure only 
		,CASE WHEN  atla.NumberOfCases = 1 and atla.MedianAnaestheticMinutes > 100 then NULL Else atla.MedianAnaestheticMinutes  END as MedianAnaestheticMinutesLA--this is the median anaesthetic time for the last 12 months broken down by consultant and procedure- LA procedures only
																																								--22/07/2015 CM Added case statement to say that if the median anaesthetic minutes is > 100 and there is only one historic case to look at then leave numm as this will then automatically send it to pick up the mediananaestheticminutesbyproclain the calculation of the median anaesthetic minutes final  
		,atpla.MedianAnaestheticMinutes AS MedianAnaestheticMinutesByProcLA --this is the median anaesthetic time broken by procedure only   - LA procedures only
		,CASE 
			WHEN (
					wl.EstimatedTheatreTime = 0
					OR wl.EstimatedTheatreTime IS NULL
					)
				AND isnull(pt.MedianOperationMinutes, ptp.MedianOperationMinutes) IS NULL
				THEN 1
			ELSE 0
			END AS NoOpTimeFlag
		,NULL AS MedianAnaestheticMinutesFinal--this is the median anaesthetic time to be added into the final report - i.e. mapping to the breakdown by consultant and procedure where there is one and then to the breakdown by procedure only if there isn't one.  Also as agreed with MOsborne, if anaesthetic mins < 10 then minimum cap at 10 
		,ISNULL(pt.MedianOperationMinutes, ptp.MedianOperationMinutes) as MedianOperationMinutesFinal
		,NULL AS NoAnaestheticTimeFlag--field to help flag up ops which do not have a median anaesthetic time included in the duration calculation
		, pb.AnaestheticTypeCode
	FROM WH.Theatre.PatientBooking pb
	LEFT OUTER JOIN WH.PTL.IPWL wl ON pb.PatientEpisodeNumber = wl.SourceUniqueID
	LEFT OUTER JOIN (
		SELECT *
		FROM WH.PAS.ClinicalCodingBase
		WHERE SORCE_CODE = 'WLIST'
			AND SORT_ORDER = 1
			AND ARCHV_FLAG = 'N'
			AND DPTYP_CODE = 'PROCE'
		) cc ON cast(pb.PatientEpisodeNumber AS INT) = cc.SORCE_REFNO
	LEFT OUTER JOIN #LkpAnaes ON pb.SessionSourceUniqueID = #LkpAnaes.SessionSourceUniqueID -- bring this temp table in so that the AnaesthetistCode field can be more complete - if the actual patient booking itself does not ahve an anesthetist recorded against it then bring in the anesthetist from the temp table above as an isnull - see BookingAnaesthetistCode field above
	LEFT OUTER JOIN WH.Theatre.Staff cons ON pb.ConsultantCode = cons.StaffCode
	LEFT OUTER JOIN WH.Theatre.Staff anaes ON pb.AnaesthetistCode = anaes.StaffCode
	LEFT OUTER JOIN WH.Theatre.Staff anaes2 on #LkpAnaes.AnaesthetistCode = anaes2.StaffCode1
	LEFT OUTER JOIN WHReporting.TH.TheatreProcedureTimes pt ON cons.StaffCode1 = pt.OperationDetailConsultantCode
		AND left(cc.CODE, 3) = pt.PrimaryProcedureCodeGrouped
	LEFT OUTER JOIN WHReporting.TH.TheatreAnaestheticTimes at ON isnull(anaes.StaffCode1, #lkpAnaes.AnaesthetistCode) = at.AnaesthetistCode and (pb.AnaestheticTypeCode not like '%LA%' or pb.AnaestheticTypeCode is null)
		AND left(cc.CODE, 3) = at.PrimaryProcedureCodeGrouped
	LEFT OUTER JOIN (
		SELECT *
		FROM WH.PAS.ReferenceValue
		WHERE ReferenceDomainCode = 'ANTYP'
		) antyp ON wl.AnaestheticType = antyp.MainCode
	LEFT OUTER JOIN WHREPORTING.TH.TheatreAnaestheticTimesByProcedureOnly atp ON left(cc.CODE, 3) = atp.PrimaryProcedureCodeGrouped  and (pb.AnaestheticTypeCode not like '%LA%' or pb.AnaestheticTypeCode is null)
	LEFT OUTER JOIN WHREPORTING.TH.TheatreProcedureTimesByProcedureOnly ptp ON left(cc.CODE, 3) = ptp.PrimaryProcedureCodeGrouped
	LEFT OUTER JOIN WHReporting.TH.TheatreAnaestheticTimesLA atla on isnull(anaes.StaffCode1, #lkpAnaes.AnaesthetistCode) = atla.AnaesthetistCode  and pb.AnaestheticTypeCode like '%LA%' and left(cc.CODE, 3) = atla.PrimaryProcedureCodeGrouped  
	LEFT OUTER JOIN WHREPORTING.TH.TheatreAnaestheticTimesByProcedureOnly atpla ON left(cc.CODE, 3) = atpla.PrimaryProcedureCodeGrouped  and pb.AnaestheticTypeCode like '%LA%'
	LEFT OUTER JOIN WHREPORTING.LK.ConsultantSpecialty WLCons ON  wl.Consultant = WLCons.ConsultantCode --join to this table to get WL consultant name to bring into the report for info
	WHERE IntendedProcedureDate >= @Date
		AND IntendedProcedureDate < DATEADD(day, 7, @Date)
		AND pb.PatientEpisodeNumber <> '' --this will select the cohort of patients where the ORMIS booking is attached to a PAS waiting list
		AND pb.SourceUniqueID NOT IN (
			SELECT PatientSourceUniqueID
			FROM WH.Theatre.Cancellation
			WHERE Dateadd(day,DATEDIFF(DAY,0,CancellationDate),0)<> ProposedOperationDate
			)
	
	UNION ALL
	
	SELECT DISTINCT pb.SourceUniqueID AS BookingUniqueID
		,pb.DistrictNo
		,pb.Forename + ' ' + pb.Surname as PatientName
		,pb.DateOfBirth
		,pb.IntendedProcedureDate
		,pb.TheatreCode
		,pb.ConsultantCode
		,pb.AnaesthetistCode
		,pb.SessionSourceUniqueID
		,cons.StaffCode1 AS BookingConsultantCode
		,cons.Forename + ' ' + cons.Surname  as BookingConsultant
		,isnull(anaes.StaffCode1, #lkpAnaes.AnaesthetistCode) AS BookingAnaesthetistCode
		,ISNULL(anaes.Forename + ' ' + anaes.Surname,anaes2.Forename + ' ' + anaes2.Surname ) as BookingAnaesthetist
		,wl.SourceUniqueID
		,wl.Consultant
		,WLCons.Forename + ' '  + WLCons.Surname as ConsultantName-- Waiting List Consultant 
		,wl.EstimatedTheatreTime
		,cc.CODE
		,cc.DESCRIPTION
		,antyp.ReferenceValue AS WLAnaestheticType
		,pt.MedianOperationMinutes--median procedure time for last 12 months by consultant and procedure
		,ptp.MedianOperationMinutes AS MedianOperationMinutesByProc --median procedure time for last 12 months broken down by procedure only
		,CASE WHEN at.NumberOfCases = 1 and at.MedianAnaestheticMinutes > 100 Then NULL Else at.MedianAnaestheticMinutes END as MedianAnaestheticMinutes  --this is the median anaesthetic time for the last 12 months broken down by consultant and procedure
																																							--22/07/2015 CM Added case statement to say that if the median anaesthetic minutes is > 100 and there is only one historic case to look at then leave numm as this will then automatically send it to pick up the mediananaestheticminutesbyproc in the calculation of the median anaesthetic minutes final  
		,atp.MedianAnaestheticMinutes AS MedianAnaestheticMinutesByProc --this is the median anaesthetic time broken by procedure only 
		,CASE WHEN  atla.NumberOfCases = 1 and atla.MedianAnaestheticMinutes > 100 then NULL Else atla.MedianAnaestheticMinutes  END as MedianAnaestheticMinutesLA--this is the median anaesthetic time for the last 12 months broken down by consultant and procedure- LA procedures only
																																									--22/07/2015 CM Added case statement to say that if the median anaesthetic minutes is > 100 and there is only one historic case to look at then leave numm as this will then automatically send it to pick up the mediananaestheticminutesbyprocla in the calculation of the median anaesthetic minutes final  
		,atpla.MedianAnaestheticMinutes AS MedianAnaestheticMinutesByProcLA --this is the median anaesthetic time broken by procedure only   - LA procedures only

		,CASE 
			WHEN (
					wl.EstimatedTheatreTime = 0
					OR wl.EstimatedTheatreTime IS NULL
					)
				AND isnull(pt.MedianOperationMinutes, ptp.MedianOperationMinutes) IS NULL
				THEN 1
			ELSE 0
			END AS NoOpTimeFlag
		,NULL AS MedianAnaestheticMinutesFinal--this is the median anaesthetic time to be added into the final report - i.e. mapping to the breakdown by consultant and procedure where there is one and then to the breakdown by procedure only if there isn't one.  Also as agreed with MOsborne, if anaesthetic mins < 10 then minimum cap at 10 , if there is no match at all assign 10
		,ISNULL(pt.MedianOperationMinutes, ptp.MedianOperationMinutes) as MedianOperationMinutesFinal
		,NULL as NoAnaestheticTimeFlag--field to help flag up ops which do not have a median anaesthetic time included in the duration calculation
		, pb.AnaestheticTypeCode
		FROM WH.Theatre.PatientBooking pb
	LEFT OUTER JOIN WH.PTL.IPWL wl ON pb.DistrictNo = wl.DistrictNo
		AND (
			pb.IntendedProcedureDate = DateAdd(day, Datediff(day, 0, wl.TCIDate), 0)
			OR pb.IntendedProcedureDate = DateAdd(day, Datediff(day, 0, wl.TCIDate) + 1, 0)
			)
	LEFT OUTER JOIN (
		SELECT *
		FROM WH.PAS.ClinicalCodingBase
		WHERE SORCE_CODE = 'WLIST'
			AND SORT_ORDER = 1
			AND ARCHV_FLAG = 'N'
			AND DPTYP_CODE = 'PROCE'
		) cc ON cast(wl.SourceUniqueID AS INT) = cc.SORCE_REFNO
	LEFT OUTER JOIN #LkpAnaes ON pb.SessionSourceUniqueID = #LkpAnaes.SessionSourceUniqueID -- bring this temp table in so that the AnaesthetistCode field can be more complete - if the actual patient booking itself does not ahve an anesthetist recorded against it then bring in the anesthetist from the temp table above as an isnull - see BookingAnaesthetistCode field above
	LEFT OUTER JOIN WH.Theatre.Staff cons ON pb.ConsultantCode = cons.StaffCode
	LEFT OUTER JOIN WH.Theatre.Staff anaes ON pb.AnaesthetistCode = anaes.StaffCode
	LEFT OUTER JOIN WH.Theatre.Staff anaes2 on #LkpAnaes.AnaesthetistCode = anaes2.StaffCode1
	LEFT OUTER JOIN WHReporting.TH.TheatreProcedureTimes pt ON cons.StaffCode1 = pt.OperationDetailConsultantCode
		AND left(cc.CODE, 3) = pt.PrimaryProcedureCodeGrouped
	LEFT OUTER JOIN WHReporting.TH.TheatreAnaestheticTimes at ON isnull(anaes.StaffCode1, #lkpAnaes.AnaesthetistCode) = at.AnaesthetistCode and (pb.AnaestheticTypeCode not like '%LA%' or pb.AnaestheticTypeCode is null)
		AND left(cc.CODE, 3) = at.PrimaryProcedureCodeGrouped
	LEFT OUTER JOIN (
		SELECT *
		FROM WH.PAS.ReferenceValue
		WHERE ReferenceDomainCode = 'ANTYP'
		) antyp ON wl.AnaestheticType = antyp.MainCode
	LEFT OUTER JOIN WHREPORTING.TH.TheatreAnaestheticTimesByProcedureOnly atp ON left(cc.CODE, 3) = atp.PrimaryProcedureCodeGrouped and (pb.AnaestheticTypeCode not like '%LA%' or pb.AnaestheticTypeCode is null)
	LEFT OUTER JOIN WHREPORTING.TH.TheatreProcedureTimesByProcedureOnly ptp ON left(cc.CODE, 3) = ptp.PrimaryProcedureCodeGrouped
	LEFT OUTER JOIN WHReporting.TH.TheatreAnaestheticTimesLA atla on isnull(anaes.StaffCode1, #lkpAnaes.AnaesthetistCode) = atla.AnaesthetistCode  and pb.AnaestheticTypeCode like '%LA%' 	AND left(cc.CODE, 3) = atla.PrimaryProcedureCodeGrouped
	LEFT OUTER JOIN WHREPORTING.TH.TheatreAnaestheticTimesByProcedureOnly atpla ON left(cc.CODE, 3) = atpla.PrimaryProcedureCodeGrouped  and pb.AnaestheticTypeCode like '%LA%'	
	LEFT OUTER JOIN WHREPORTING.LK.ConsultantSpecialty WLCons ON  wl.Consultant = WLCons.ConsultantCode --join to this table to get WL consultant name to bring into the report for info
	WHERE IntendedProcedureDate >= @Date
		AND IntendedProcedureDate < DATEADD(day, 7, @Date)
		AND PatientEpisodeNumber = '' --this will be the cohort of patients who have an ORMIS booking without it being linked to a waiting list.  Different matching criteria is used for these ones.--see left join above
		AND pb.SourceUniqueID NOT IN (
			SELECT PatientSourceUniqueID
			FROM WH.Theatre.Cancellation
			WHERE Dateadd(day,DATEDIFF(DAY,0,CancellationDate),0)<> ProposedOperationDate
			)
	) booking




--Update the Median Anaesthetic minutes final field 
---this is the median anaesthetic time to be added into the final report - i.e. mapping to the breakdown by consultant and procedure where there is one and then to the breakdown by procedure only if there isn't one.  Also as agreed with MOsborne, if anaesthetic mins < 10 then minimum cap at 10 , if there is no match at all assign 10
--further logic as discussed with Sarah Manns is to take into account the anaesthetic type - if GA ot other type then set it to 10 mins if no anaesthetic time , if LA set this to 5 minutes

Update  #booking
SET NoAnaestheticTimeFlag = CASE when anaesthetictypecode not like '%LA%' and  ISNULL(MedianAnaestheticMinutes, MedianAnaestheticMinutesByProc)IS NULL
				THEN 1
			when anaesthetictypecode IS NULL  and  ISNULL(MedianAnaestheticMinutes, MedianAnaestheticMinutesByProc)IS NULL
				THEN 1
			when anaesthetictypecode like 'LA'  and  ISNULL(MedianAnaestheticMinutesLA, MedianAnaestheticMinutesByProcLA)IS NULL
				THEN 1
			ELSE 0
			END,
MedianAnaestheticMinutesFinal =  CASE 
		WHEN anaesthetictypecode NOT LIKE '%LA%'
			THEN CASE 
					WHEN Coalesce(MedianAnaestheticMinutes, MedianAnaestheticMinutesByProc, '10') < 10
						THEN 10
					ELSE Coalesce(MedianAnaestheticMinutes, MedianAnaestheticMinutesByProc, '10')
					END
		WHEN anaesthetictypecode IS NULL
			THEN CASE 
					WHEN Coalesce(MedianAnaestheticMinutes, MedianAnaestheticMinutesByProc, '10') < 10
						THEN 10
					ELSE Coalesce(MedianAnaestheticMinutes, MedianAnaestheticMinutesByProc, '10')
					END
		WHEN anaesthetictypecode LIKE 'LA'
			THEN Coalesce(MedianAnaestheticMinutesLA, MedianAnaestheticMinutesByProcLA, '5')
		ELSE NULL
		END
From #booking 







--Final Query to extract the relevant records
SELECT DISTINCT ss.SessionSourceUniqueID
	,ss.SessionTheatreType
	,ss.SessionTheatreCode
	,ss.SessionTheatre
	,ss.SessionConsultantCode
	,ss.SessionConsultant
	,bk.BookingAnaesthetistCode
	,bk.BookingAnaesthetist
	,ss.SessionStartTime
	,ss.SessionEndTime
	,ss.PlannedSessionMinutes
	,ss.SessionCancelledFlag
	,bk.BookingConsultantCode
	,bk.BookingConsultant
	,bk.Consultant as WLConsultant 
	,bk.ConsultantName as WLConsultantName
	,bk.PatientName
	,bk.DateOfBirth
	,bk.DistrictNo
	,bk.IntendedProcedureDate
	,bk.EstimatedTheatreTime
	,bk.Code AS PrimaryProcCode
	,bk.Description AS PrimaryProcDescription
	,Cast(bk.MedianOperationMinutesFinal AS DECIMAL(16, 0)) AS MedianOpTime
	,cast(bk.MedianAnaestheticMinutesFinal as Decimal (16,0)) as MedianAnaestheticMinutes
	,Case when bk.BookingConsultantCode <> bk.Consultant Then 'Yes' 
			when bk.BookingConsultantCode = bk.Consultant Then 'No' 
	Else ''
	End as BookingConsAndWLConsDifferent
	
	
FROM WHREPORTING.QLIK.TheatreSessions ss 

LEFT OUTER JOIN #Booking bk ON ss.SessionSourceUniqueID = bk.SessionSourceUniqueID
WHERE DateAdd(day, DATEDIFF(day, 0, ss.SessionStartTime), 0) = @Date
	AND ss.SessionTheatre = @Theatre
ORDER BY 1



END