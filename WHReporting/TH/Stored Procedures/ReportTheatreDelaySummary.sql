﻿/*
--Author: Helen Ransome
--Date created: 16/05/2013
--Stored Procedure Built for SSRS Report on:
--Generic store procedure used for Cancer Type Parameter
*/

CREATE Procedure [TH].[ReportTheatreDelaySummary]


 @StartDate as datetime
 ,@EndDate as datetime
 ,@Specialty as varchar (max)
 ,@Consultant as varchar (max)

 
 
as
SELECT 
X.SessionSpecialty
,X.SessionConsultant
--,X.SessionDayOfWeek
--,X.SessionType
,SUM(X.Total) AS Total
,SUM(X.NumberOfDelays) AS NumberOfDelays
,SUM(X.IN_ANAESDelayStage) AS AnaesDelay
FROM
(

SELECT DISTINCT
Sess.SessionSourceUniqueID
,SessionStartTime
,SessionEndTime
,ActualStartTime
,ActualEndTime
,LateStartMinutes
,LateStart15Flag
,Sess.SessionDivision
,Sess.SessionDirectorate
,Sess.SessionSpecialty
,Sess.SessionType
,Sess.SessionDayOfWeek
,Sess.SessionConsultant
,OP.AnaestheticInductionTime AS OperationDate
,OperationOrder
,X.Comments AS DelayComments
,CASE WHEN X.Comments Like '%IN_ANAES%' 
AND LateStart15Flag = 1
THEN 1 ELSE 0 END AS IN_ANAESDelayStage
,1 AS Total
,LateStart15Flag AS NumberOfDelays
FROM QLIK.TheatreSessions Sess
LEFT JOIN qlik.TheatreOpDetails OP
ON Sess.SessionSourceUniqueID = OP.SessionSourceUniqueID
LEFT JOIN QLIK.TheatreOperationDelays OD
ON OP.SourceUniqueID = OD.OperationDetailSourceUniqueID

LEFT JOIN ( 
SELECT final.[OperationDetailSourceUniqueID],
        LEFT(final.Res, Len(final.Res) - 1) AS Comments

FROM   (SELECT DISTINCT ST2.[OperationDetailSourceUniqueID],
 
                        (SELECT Case when ST1.DelayReasonDescription in ('Scrub team not available','Anaesthetic support team not available','Radiographer not available','Staff Teaching/Training','All Staff Already Deployed') 
then 'Theatre Staff Issues' + ': ' 
when ST1.DelayReasonDescription in ('Anaesthetist At Meeting','Anaesthetist Delayed','Anaesthetist delayed seeing patients','Anaesthetist delayed, awaiting replacement')
then 'Anaesthetic Issues' + ': ' 
when ST1.DelayReasonDescription in ('Surgeon At Meeting','Surgeon delayed','Surgeon delayed seeing patient')
then 'Surgical Staff Issues' + ': ' 
when ST1.DelayReasonDescription in ('Agreed late start','Previous list overran','Session Management','Additions/Changes to list','Recovery full')
then  'Session Management Issues' + ': ' 
when ST1.DelayReasonDescription in ('Awaiting Blood Results','Results not available','Patient having investigations','No Notes','Awaiting operating schedule','Case notes missing','Images not available') 
then 'Information Not Available' + ': ' 
when ST1.DelayReasonDescription in ('Surgical equipment not available','Awaiting sterile instrumentation','Equipment','Surgical equipment failure','Anaesthetic equipment failure','Anaesthetic equipment not available','Other equipment failure, eg list/transport')
then 'Equipment Issues' + ': ' 
when ST1.DelayReasonDescription in ('Bed Management','Ward bed not available','No ITU bed available','HDU bed not available')
then 'Bed Issues' + ': ' 
when ST1.DelayReasonDescription in ('Escort Nurse not available','External delay','Patient Delayed in Transit','Logistical Problems','Porter not available','Fire alarm')
then 'Transit Issues' + ': ' 
when ST1.DelayReasonDescription in ('Unidentified patient allergy/infection','Site not marked','Consent not completed','Patient Awaiting Medical Staff Clearance','Patient not prepared','Patient fed','Patient on incorrect ward','Pre med not given','Patient Management')
then 'Preparation Issues' + ': ' 
when ST1.DelayReasonDescription in ('Clinical event','Emergency case priority','Previous Patient Returned to Theatre','Return to theatre','Surgical complications','Unplanned events','Prolonged Anaesthesia','anaesthetic complications','Consultant required','Patient unfit for surgery')
then 'Unanticipated Clinical Issues' + ': ' 
else 'Other' + ': '  End  AS [text()]
                          FROM   [WHREPORTING].[QLIK].[TheatreOperationDelays] ST1
                          WHERE  ST1.[OperationDetailSourceUniqueID] = ST2.[OperationDetailSourceUniqueID]
                                
                          ORDER  BY ST1.[DelaySourceUniqueID]
                          FOR XML PATH ('')) Res
         FROM   [WHREPORTING].[QLIK].[TheatreOperationDelays]  ST2
      )final) AS X
ON OD.OperationDetailSourceUniqueID = X.OperationDetailSourceUniqueID

WHERE Sess.SessionCancelledFlag = 0
--AND LateStart15Flag = 1
AND OperationCancelledFlag = 0
AND OperationOrder = 1
AND  SessionTheatreType IN ('Elective', 'DayCase')
AND SessionStartTime BETWEEN @StartDate and @EndDate
AND OP.OperationTheatreCode in
('1',
'2',
'3',
'6',
'78',
'37',
'38',
'39',
'40',
'14',
'16',
'21',
'13',
'15',
'19',
'20',
'22',
'23'
)
 
and Sess.SessionSpecialty IN (SELECT Item FROM WHREPORTING.dbo.Split (@Specialty, ','))
and Sess.SessionConsultant IN (SELECT Item FROM WHREPORTING.dbo.Split (@Consultant,','))
) AS X

GROUP BY
X.SessionSpecialty
,X.SessionConsultant
--,X.SessionDayOfWeek
--,X.SessionType
--,X.IN_ANAESDelayStage

ORDER BY 
SUM(X.IN_ANAESDelayStage)

Desc