﻿/*
--Author: N Scattergood
--Date created: 10/06/2013
--Stored Procedure Built for Theatre KPI by Consultant Patients per List and by Month
--
*/


Create Procedure [TH].[ReportTheatreKPIConsultantByMonthActivity]

@StartDate		as Date
,@EndDate		as Date
,@OpSuitePar	as nvarchar (max)
,@TheatrePar	as nvarchar (max)
,@SpecPar		as nvarchar (max)

as
SELECT 

    cal.TheMonth
    ,TH.OperatingSuite
  ,TH.Theatre
  ,Div.Direcorate as Directorate
 ,Cons.StaffName as SessionConsultant
  ,spec.Specialty  as ConsultantSpecialty
  ,T.KPI
  ,SUM(T.Actual) AS Indicator

  
FROM         WHOLAP.dbo.FactTheatreKPI AS t 


INNER JOIN     WHOLAP.dbo.FactTheatreSession AS Session 
				ON Session.SourceUniqueID = t.SourceUniqueID 
						
	LEFT OUTER JOIN   WHOLAP.dbo.OlapTheatreStaff AS Cons
						 ON Cons.StaffCode = Session.ConsultantCode 
								 
		LEFT OUTER JOIN  WHOLAP.dbo.OlapTheatreSpecialty AS spec 
							ON Cons.SpecialtyCode = spec.SpecialtyCode
										
			LEFT  JOIN WHOLAP.dbo.OlapCalendar	 AS cal
						ON T.SessionDate = cal.TheDate
																													
				Left Join [WHOLAP].[dbo].[OlapTheatre] TH
							ON th.TheatreCode = T.TheatreCode
											
					LEFT OUTER JOIN [WHOLAP].[dbo].[OlapSpecialityDivision] Div
										on spec.NationalSpecialtyCode = Div.[SubSpecialtyCode]
                                	
WHERE
Session.CancelledSession = '0'
AND
T.KPI IN 
(
'Actual Session'
,'Number of Patients Operated On'
)
AND
spec.SpecialtyCode <> '0' ---excludes N/A

--following comes from Parameters
AND
T.SessionDate BETWEEN @StartDate and @EndDate
AND
TH.OperatingSuiteCode in  (SELECT Val from dbo.fn_String_To_Table(@OpSuitePar,',',1))
AND
T.TheatreCode in  (SELECT Val from dbo.fn_String_To_Table(@TheatrePar,',',1)) 
AND
spec.SpecialtyCode in (SELECT Val from dbo.fn_String_To_Table(@SpecPar,',',1)) 


GROUP BY

   cal.TheMonth
  ,TH.OperatingSuite
  ,TH.Theatre
  ,Div.Direcorate
   ,Cons.StaffName
  ,spec.Specialty
  ,T.KPI
  ,FinancialMonthKey
  									
ORDER BY 

T.KPI
  
,FinancialMonthKey
,TH.OperatingSuite
,spec.Specialty