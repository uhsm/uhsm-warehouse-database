﻿/*
--Author: N Scattergood
--Date created: 24/04/2013
--Stored Procedure Built for Theatre KPI by Consultant and by Month
--This Provides Dropdown for StartDate
--
*/

CREATE Procedure [TH].[ReportTheatreKPIConsultantByMonth_StartDate]

as

select 

  convert(varchar,Min(Cal.TheDate),103) as MonthDropdown
 ,Min(Cal.TheDate) as  StartDatePar
 
  FROM [WHREPORTING].[LK].[Calendar] Cal
  
  where
  Cal.TheDate < GETDATE()
  and
  Cal.TheDate >=
   (
   select 
   Min(ST.TheDate)
   FROM [WHREPORTING].[LK].[Calendar] ST
   where ST.FinancialYear = 
   ( select 
   CFY.FinancialYear
 FROM [WHREPORTING].[LK].[Calendar] CFY
 where CFY.TheDate = 
 (DATEADD(MONTH,-12,CAST(getdate() AS date))) 
	)
	)------gets the first day of the previous Financial Year
and
 Cal.TheMonth <>
   ( select
   distinct
CM.TheMonth
 FROM [WHREPORTING].[LK].[Calendar] CM
where CM.TheDate = 
 CAST(getdate() AS date) 
)-----Excludes the Current Month
 group by
Cal.TheMonth

order by
Min(Cal.TheDate) asc