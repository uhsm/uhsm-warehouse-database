﻿/*
--Author: N Scattergood
--Date created: 21/02/2013
--Stored Procedure Built Theatre KPI Dashboard (not SRSS)
--
*/


CREATE Procedure [TH].[ReportTheatreKPIDashboard]

as
SELECT 

    cal.TheMonth
    ,cal.FinancialYear
    ,TH.OperatingSuite
  ,TH.Theatre
  ,Div.Direcorate as Directorate
 ,Cons.StaffName as OPConsultant
  ,case 
  when spec.SpecialtyCode in (132,46) 
  --rolls Gynaecological Oncology and Obstetrics into Gynaecology
  then 'Gynaecology'
  else spec.Specialty end as Specialty
  ,T.KPI
  ,SUM(T.Actual) AS Indicator

  
FROM         WHOLAP.dbo.FactTheatreKPI AS t 


INNER JOIN     WHOLAP.dbo.FactTheatreSession AS Session 
				ON Session.SourceUniqueID = t.SourceUniqueID 
						
	LEFT OUTER JOIN   WHOLAP.dbo.OlapTheatreStaff AS Cons
						 ON Cons.StaffCode = Session.ConsultantCode 
								 
		LEFT OUTER JOIN  WHOLAP.dbo.OlapTheatreSpecialty AS spec 
							ON Cons.SpecialtyCode = spec.SpecialtyCode
										
			LEFT  JOIN WHOLAP.dbo.OlapCalendar	 AS cal
						ON T.SessionDate = cal.TheDate
																													
				Left Join [WHOLAP].[dbo].[OlapTheatre] TH
							ON th.TheatreCode = T.TheatreCode
											
					LEFT OUTER JOIN [WHOLAP].[dbo].[OlapSpecialityDivision] Div
										on spec.NationalSpecialtyCode = Div.[SubSpecialtyCode]
                                	
WHERE
t.SessionDate BETWEEN 
 -----------------Sub Querys for rolling 12 Months-----------
 (select 
 distinct
 min(Cal.TheDate) 
 FROM LK.Calendar Cal
 
 where
Cal.TheMonth in 
(select distinct
SQ.TheMonth
  FROM LK.Calendar SQ
  where SQ.TheDate = 
 (
dateadd(Month,-12,cast(GETDATE()as DATE))
)
)
)
 AND 
		(select  distinct cast(max(Cal.TheDate)as DATE)
		FROM LK.Calendar Cal
			where
			Cal.TheMonth = 
			(
			select distinct
			SQ.TheMonth
			  FROM LK.Calendar SQ
			  where SQ.TheDate = 
			 (
			dateadd(MONTH,-1,cast(GETDATE()as DATE))
			)))
-----------------End of Sub Querys for rolling 12 Months-----------
AND
Session.CancelledSession = '0'
AND
T.KPI IN 
(
'Late Finish'
,'Late Start'
,'Early Finish'
,'Capped Session Utilisation'
,'Productive Minutes',
'Planned Minutes',
'Actual Session',
'Number of Patients Operated On'
)
AND
TH.TheatreCode in
('1',
'2',
'3',
'6',
'13',
'14',
'15',
'16',
'19',
'20',
'21',
'23',
'37',
'38',
'39',
'40',
'78'
)
AND
spec.SpecialtyCode <> '0' 
---excludes N/A


GROUP BY

   cal.TheMonth
    ,cal.FinancialYear
  ,TH.OperatingSuite
  ,TH.Theatre
  ,Div.Direcorate
   ,Cons.StaffName
  ,case 
  when spec.SpecialtyCode in (132,46) 
  --rolls Gynaecological Oncology and Obstetrics into Gynaecology
  then 'Gynaecology'
  else spec.Specialty end 
  ,spec.Specialty
  ,T.KPI
  ,FinancialMonthKey
  									
ORDER BY 

T.KPI
  
,FinancialMonthKey
,TH.OperatingSuite
,spec.Specialty