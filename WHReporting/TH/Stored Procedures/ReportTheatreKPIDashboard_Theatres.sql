﻿/*
--Author: N Scattergood
--Date created: 14/02/2013
--Stored Procedure Built for SRSS Reports on Theatre
--This Procedure provides the parameters for the Theatres
--Used in: 
--TheatreKPIDashboard
--(The defualts are provided by TH.ReportDefaultTheatres)
--
*/


Create Procedure [TH].[ReportTheatreKPIDashboard_Theatres]
as
Select 

	TH.[TheatreCode]	as 'TheatreCodePar'
	,TH.[Theatre]		as 'TheatrePar'
	--,TH.OperatingSuite
	--,TH.OperatingSuiteCode

from
[WHOLAP].[dbo].[OlapTheatre] TH

where
TH.OperatingSuiteCode in (1,2,3,4,5,6,7)
and 
TH.TheatreCode not in (32,33,34,35,36,45,50,51)
	
	
	order by TH.OperatingSuite, TH.Theatre