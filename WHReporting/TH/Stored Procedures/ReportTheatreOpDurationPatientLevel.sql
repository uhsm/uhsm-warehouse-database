﻿-- =============================================
-- Author:		CMan
-- Create date: 06/12/2013
-- Description:	Feeds report to look at theatre duration of Elective Operations for Ordinary Admissions at Patient Level.
--				Requested by Craig McAllister
-- Dependencies: [WH].Theatre.OperationDetail
--					[WH].Theatre.ProcedureDetail
--					[WH].Theatre.Theatre
--					[WH].Theatre.Session 
--					[WH].WH.Calendar
--					[WH].Theatre.Specialty
--					[WH].Theatre.PatientBooking
--				SSRS report: Theatres/Duration of Theatre Ops - Patient Level			
-- =============================================
CREATE PROCEDURE [TH].[ReportTheatreOpDurationPatientLevel]
	-- Add the parameters for the stored procedure here
@DateFrom datetime,
@DateTo datetime

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

SELECT o.DistrictNo                                          AS RM2,
       o.Surname                                             AS ptsurname,
       o.SourceUniqueID                                      AS ID,
       Cast(o.OperationDate AS DATE)                         AS OperationDate,
       t.Theatre,
       o.Operation,
       o.OperationTypeCode,
       sp.Specialty,
       p.ProcedureStartTime,
       p.ProcedureEndTime,
       Datediff(N, p.ProcedurestartTime, p.ProcedureEndTime) AS OpTime
FROM   [WH].Theatre.OperationDetail AS o
       LEFT OUTER JOIN [WH].Theatre.ProcedureDetail AS p
                    ON p.OperationDetailSourceUniqueID = o.SourceUniqueID
                       AND p.PrimaryProcedureFlag = 1
       INNER JOIN [WH].Theatre.Theatre AS t
               ON t.TheatreCode = o.TheatreCode
       LEFT OUTER JOIN [WH].Theatre.Session AS s
                    ON s.SourceUniqueID = o.SessionSourceUniqueID
       LEFT OUTER JOIN [WH].WH.Calendar AS c
                    ON c.TheDate = o.OperationDate
       LEFT OUTER JOIN [WH].Theatre.Specialty AS sp
                    ON sp.SpecialtyCode = o.SpecialtyCode
       LEFT OUTER JOIN WH.Theatre.PatientBooking AS pb
                    ON pb.SourceUniqueID = o.PatientBookingSourceUniqueID
WHERE  ( o.OperationDate BETWEEN @DateFrom AND @DateTo )
       AND ( o.OperationCancelledFlag = 0 )
       AND OperationTypeCode = 'ELECTIVE' --Elective 
       AND pb.AdmissionTypeCode <> '18' --Ordinary Admission
ORDER  BY id 



END