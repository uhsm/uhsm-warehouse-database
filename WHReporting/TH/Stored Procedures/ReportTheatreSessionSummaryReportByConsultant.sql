﻿/*
--Author: N Scattergood
--Date created: 25/03/2013
--Stored Procedure Built for SRSS Report on:
--Theatre Session Summary Report By Consultant
*/

CREATE Procedure [TH].[ReportTheatreSessionSummaryReportByConsultant]


@StartDate	as Date 
,@EndDate	as Date 
,@ConsPar	as int 

as
SELECT     
ts.SourceUniqueID
--, ts.SessionNumber
,case	
	when ts.PlannedSessionMinutes >= 600 then 'Triple Session'
	when ts.PlannedSessionMinutes > 360  then 'Double Session'
	when datepart(hour,ts.StartTime) < 6
	then 'Single Session Night'
	when datepart(hour,ts.StartTime) >= 16
	then 'Single Session Evening'
	when datepart(hour,ts.StartTime) >= 12
	then 'Single Session Afternoon'
	else 'Single Session Morning'
	end as SessionType,
	 th.OperatingSuiteCode
 ,th.OperatingSuite
 , th.Theatre
 , CAST(ts.ActualSessionStartTime AS DATE) AS SessionDate
 , sp.Specialty as SessionSpecialty
 , ts.ActualSessionStartTime
 , ts.ActualSessionEndTime
 , ts.StartTime, ts.EndTime
 , DATEDIFF(minute, ts.ActualSessionStartTime, ts.ActualSessionEndTime) AS Utilised
 , DATEDIFF(minute, ts.StartTime, ts.EndTime) AS Planned
 ,KPI.PlannedKPI
 ,KPI.UtilisationKPI
 ,KPI.CappedUtilisationKPI
 ,KPI.ProductiveKPI
 ,KPI.NoPatientsOperatedOnKPI
 , c.DayOfWeek
 , st.StaffName as Consultant
 , an.StaffName as Anaesthetist
 ,cs.Specialty as ConsultantSpecialty

 ,CASE 
 WHEN datediff(minute,actualsessionendtime,ts.endtime) > 15
 THEN 1
 ELSE 0 
 END AS Underruns
 ,1 AS CountSessions
 ,
 CASE 
 WHEN datediff(minute,ts.endtime,actualsessionendtime) > 15
 THEN 1
 ELSE 0 
 END AS Overruns
 ,
 CASE 
 WHEN datediff(minute,ts.endtime,actualsessionendtime)  > 15
 THEN 'Blue'
 WHEN datediff(minute,actualsessionendtime,ts.endtime) > 15
 THEN 'Red'
 ELSE 'Black'
 END AS EndColour
 ,
 CASE 
 WHEN datediff(minute,actualsessionstarttime,ts.starttime) > 15 
 THEN 1
 ELSE 0
 END AS EarlyStarts
 ,
 CASE 
 WHEN datediff(minute,ts.starttime,actualsessionstarttime) > 15 
 THEN 1
 ELSE 0
 END AS LateStarts
 ,
 CASE 
 WHEN datediff(minute,actualsessionstarttime,ts.starttime) > 15 
 THEN 'Blue'
 WHEN datediff(minute,ts.starttime,actualsessionstarttime) > 15 
 THEN 'Red'
 ELSE 'Black'
 END AS StartColour
 --,
 --CASE 
 --WHEN datediff(minute,ts.starttime,actualsessionstarttime) > 15 
 --AND  datediff(minute,ts.endtime,actualsessionendtime) > 15
 --THEN 1 
 --ELSE 0 
 --END AS LateStartLateFinish 
 --,CASE 
 --WHEN datediff(minute,ts.starttime,actualsessionstarttime) > 15 
 --AND datediff(minute,actualsessionendtime,ts.endtime) > 15
 --THEN 1
 --ELSE 0 
 --END AS LateStartEarlyFinish
 --, CASE 
 --WHEN DATEDIFF(minute, ts .starttime, actualsessionstarttime) < 1 
 --THEN 'OnTime/Early' 
 --WHEN DATEDIFF(minute, ts .starttime, actualsessionstarttime) BETWEEN 1 AND 15 
 --THEN '1-15' 
 --WHEN DATEDIFF(minute, ts .starttime, actualsessionstarttime) BETWEEN 16 AND 30 
 --THEN '16-30' 
 --WHEN DATEDIFF(minute, ts .starttime, actualsessionstarttime) BETWEEN 31 AND 45 
 --THEN '31-45' 
 --WHEN DATEDIFF(minute, ts .starttime, actualsessionstarttime) BETWEEN 46 AND 60 
 --THEN '46-60' 
 --ELSE '>60' 
 --END AS starttimerange
 --, DATEDIFF(minute, ts.StartTime, ts.ActualSessionStartTime) AS delay


FROM WHOLAP.dbo.BaseTheatreSession AS ts 

LEFT JOIN WHOLAP.dbo.OlapTheatre AS th 
ON th.TheatreCode = ts.TheatreCode 
LEFT JOIN WHOLAP.dbo.OlapCalendar AS c 
ON c.TheDate = CAST(ts.StartTime AS DATE) 
LEFT JOIN WHOLAP.dbo.OlapTheatreStaff AS st 
ON st.StaffCode = ts.ConsultantCode 
LEFT JOIN WHOLAP.dbo.OlapTheatreStaff AS an 
ON an.StaffCode = ts.AnaesthetistCode1
LEFT JOIN WHOLAP.dbo.OlapTheatreSpecialty AS sp 
ON sp.SpecialtyCode = ts.SpecialtyCode 
LEFT JOIN WHOLAP.dbo.OlapTheatreSpecialty AS cs 
ON cs.SpecialtyCode = st.SpecialtyCode

LEFT JOIN
	(
	select
T.SourceUniqueID
,PlannedKPI=sum(Case when t.KPI='Planned Minutes Of Actual Session' then (t.actual) else 0 end),
ProductiveKPI=sum(Case when t.KPI='Productive Minutes' then (t.actual) else 0 end),
UtilisationKPI=sum(Case when t.KPI='Session Utilisation Minutes' then (t.actual) else 0 end),
CappedUtilisationKPI=sum(Case when t.KPI='Capped Session Utilisation' then (t.actual) else 0 end),
NoPatientsOperatedOnKPI=sum(Case when t.KPI='Number of Patients Operated On' then (t.actual) else 0 end)

from [WHOLAP].dbo.FactTheatreKPI T
where T.SessionDate BETWEEN @StartDate AND @EndDate 
group by
T.SourceUniqueID
	) KPI
	on KPI.SourceUniqueID = ts.SourceUniqueID
--INNER JOIN
--        (
--        SELECT     SessionSourceUniqueID, 
--        SUM(OperationProductiveTime) AS OperationProductiveTime
--        FROM          
--        WHOLAP.dbo.FactTheatreOperation AS TheatreOperation
--        GROUP BY SessionSourceUniqueID
--        ) AS TheatreOperation_1 
--        ON TheatreOperation_1.SessionSourceUniqueID = ts.SourceUniqueID 
--LEFT JOIN WH.Theatre.Session AS WHSession 
--ON WHSession.SourceUniqueID = ts.SourceUniqueID 
--LEFT JOIN ORMIS.dbo.F_Delay_Items AS fi WITH (nolock) 
--ON WHSession.SourceUniqueID = fi.DIT_OP_SEQU 
--LEFT JOIN ORMIS.dbo.FPROCDY AS fp WITH (nolock) 
--ON fp.PR_SEQU = fi.DIT_PR_SEQU
WHERE     
(CAST(ts.ActualSessionStartTime AS DATE) BETWEEN @StartDate AND @EndDate) 
	AND (ts.CancelledFlag <> 1) 
	AND (ts.ConsultantCode IN (@ConsPar))
	
--GROUP BY ts.SourceUniqueID, ts.SessionNumber,
--case	
--	when ts.PlannedSessionMinutes >= 600 then 'Triple Session'
--	when ts.PlannedSessionMinutes > 360  then 'Double Session'
--	when datepart(hour,ts.StartTime) < 6
--	then 'Single Session Night'
--	when datepart(hour,ts.StartTime) >= 16
--	then 'Single Session Evening'
--	when datepart(hour,ts.StartTime) >= 12
--	then 'Single Session Afternoon'
--	else 'Single Session Morning'
--	end
--, th.OperatingSuite
--, th.Theatre
--, CAST(ts.ActualSessionStartTime AS DATE)
--, ts.ActualSessionStartTime
--, ts.ActualSessionEndTime
--, ts.StartTime
--, ts.EndTime
--, sp.Specialty
--, DATEDIFF(minute, ts.ActualSessionStartTime, ts.ActualSessionEndTime)
--, DATEDIFF(minute, ts.StartTime, ts.EndTime)
--, c.DayOfWeek
--, st.StaffName
--, cs.Specialty 
--, ts.SourceUniqueID
--, th.OperatingSuiteCode