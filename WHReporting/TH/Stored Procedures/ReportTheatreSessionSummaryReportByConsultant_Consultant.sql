﻿/*
--Author: N Scattergood
--Date created: 25/03/2013
--Stored Procedure Built for SRSS Report on:
--Theatre Session Summary Report By Consultant
--This Procedure provides the Consultant Parameter
*/

Create Procedure [TH].[ReportTheatreSessionSummaryReportByConsultant_Consultant]


@StartDate	as Date 
,@EndDate	as Date 


as
SELECT     
Distinct
ts.ConsultantCode	as ConsCodePar
,st.StaffName		as ConsNamePar

FROM WHOLAP.dbo.BaseTheatreSession AS ts 



LEFT JOIN WHOLAP.dbo.OlapTheatreStaff AS st 
ON st.StaffCode = ts.ConsultantCode 



WHERE     
(CAST(ts.ActualSessionStartTime AS DATE) BETWEEN @StartDate AND @EndDate) 
	AND (ts.CancelledFlag <> 1) 
	
	order by
st.StaffName