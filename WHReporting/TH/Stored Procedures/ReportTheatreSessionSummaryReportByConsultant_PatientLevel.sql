﻿/*
--Author: N Scattergood
--Date created: 25/03/2013
--Stored Procedure Built for SRSS Report on:
--Theatre Session Summary Report By Consultant
--This Provides the Patient Level Operations
*/

CREATE Procedure [TH].[ReportTheatreSessionSummaryReportByConsultant_PatientLevel]


@StartDate	as Date 
,@EndDate	as Date 
,@ConsPar	as int 

as

select  
		[DistrictNo]
		,[Forename] + ' ' + [Surname] PatientName
		,[SexCode]
		--,[DateOfBirth]
		,[OperationTypeCode]
           ,[OperationDate]
           ,[Operation]
,S.Specialty as ConsultantSpecialty
,cons.StaffName as Consultant

		,T.[Theatre]
		,T.[OperatingSuite]
		,TH.[InSuiteTime]
          ,TH.[InAnaestheticTime]  
          ,TH.[AnaestheticInductionTime]
          ,TH.[AnaestheticReadyTime]    
		  ,TH.[OperationStartDate]
		    ,P.ProcedureStartTime
		    ,P.ProcedureEndTime
		    ,TH.[OperationEndDate]
		  ,TH.[InRecoveryTime]
		  ,TH.[DischargeTime]
		
		  
	      
	      ,InSuite		  = DATEDIFF(minute,TH.[InSuiteTime],TH.[InAnaestheticTime])
	      ,InAnaesthetic  = DATEDIFF(minute,TH.[InAnaestheticTime],TH.[AnaestheticInductionTime])
		  ,Anaesthetic	  = DATEDIFF(minute,TH.[AnaestheticInductionTime],TH.[AnaestheticReadyTime] )
		  ,InOR			  = DATEDIFF(minute,TH.[OperationStartDate],P.ProcedureStartTime)
		  ,ProcedureTime  = DATEDIFF(minute,P.ProcedureStartTime,P.ProcedureEndTime)
		  ,OutOfOR		  = DATEDIFF(minute,P.ProcedureEndTime,TH.OperationEndDate)

FROM wh.Theatre.OperationDetail th

	--inner join WHOLAP.dbo.OlapCalendar cal 
	--on cal.TheDate=th.OperationDate
	
	inner join WHOLAP.dbo.BaseTheatreSession AS ts 
	on ts.SourceUniqueID = th.SessionSourceUniqueID
	and ts.SourceUniqueID is not null
	and CAST(ts.ActualSessionStartTime AS DATE) BETWEEN @StartDate AND @EndDate 
	AND ts.CancelledFlag <> 1
	AND ts.ConsultantCode IN (@ConsPar)
	AND th.OperationCancelledFlag <> 1

			LEFT JOIN [WH].[Theatre].ProcedureDetail AS p 
			ON p.OperationDetailSourceUniqueID = TH.SourceUniqueID	
			and p.PrimaryProcedureFlag = 1

		Left JOIN WHOLAP.dbo.OlapTheatreStaff Cons 
		on cons.StaffCode=th.ConsultantCode
		
			Left JOIN [WHOLAP].[dbo].[OlapTheatreSpecialty] S
			on Cons.SpecialtyCode = S.SpecialtyCode
				
						Left Join [WHOLAP].[dbo].[OlapTheatre] T
						on th.TheatreCode = T.TheatreCode
						
order by 
th.OperationDate
,th.SessionSourceUniqueID
    ,T.[OperatingSuite]     
    ,T.[Theatre]