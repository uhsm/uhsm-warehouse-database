﻿/*
--Author: Helen Ransome
--Date created: 16/05/2013
--Stored Procedure Built for SSRS Report on:
--Generic store procedure used for Cancer Type Parameter
*/

CREATE Procedure [TH].[ReportTheatreSpecialtyPAR]

as
SELECT

 OperationSpecialty 
      
from qlik.TheatreOpDetails OP
  --Order by SessionTheatre