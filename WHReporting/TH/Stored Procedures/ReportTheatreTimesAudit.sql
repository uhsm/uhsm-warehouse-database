﻿-- =============================================
-- Author:		CMan
-- Create date: 15/11/2013
-- Description:	Stored procedure for generating the Theatre Times Audit report 
--				RD443
--				Report replaces an existing excel one which CBH currently generates
-- =============================================
CREATE PROCEDURE [TH].[ReportTheatreTimesAudit] @StartDate datetime, @EndDate datetime, @Theatre varchar (4000)
AS

/*
 Modified Date				Modified By			Modification
-----------------			------------		------------
27/02/2014					CMan				Amend to take into account the different start and end time targets for the different days in particular with regards to the CT theatres-											
												Also amended to look at a manually created tabel to start and end target time  WHReporting.TH.TheatreTargetStartEndTimes - this is the table to be amended if the target start and ends are to be amended
27/03/2014					CMan				Amended the Anaesthetic time to reflect request from CBH.  Changing the Anaesthetic start time to read from the AnaestheticInductionTime field and the Anaesthetic End Time to read from the AnaestheticReadyTime field.



*/





BEGIN




------Parameters declared for testing stored procedure
--DECLARE @theatre   VARCHAR(50),
--     @startdate DATETIME,
--        @enddate   DATETIME

--SET @theatre = 'CARDIO THORACIC THEATRE 4'--'F BLOCK THEATRE 7'
--SET @startdate = '20140207'
--SET @enddate = '20140207';



--Build of CTE to pull together the activity records to be used
With OPDetails (DistrictNo, SourceUniqueID, OperationStartDate, OperationDate, DayOfWeek, DayOfWeekKey, TheatreCode, OperationEndDate, OperationTypeCode, InSuiteTime, InRecoveryTime, PatientSourceUniqueNo, AnaestheticInductionTime, AnaestheticReadyTime, InAnaestheticTime, ProcedureStartTime, ProcedureEndTime, OperationCancelledFlag, SurgeonCode1, SurgeonCode2, SurgeonCode3, Anaesthetist1Code, Anaesthetist2Code, Anaesthetist3Code, sessionsourceuniqueID, specialtycode)
     AS (SELECT o.DistrictNo,
                o.SourceUniqueID,
                o.OperationStartDate,
                Cast(o.OperationDate AS DATE) OperationDate,
                cal.DayOfWeek,
                cal.DayOfWeekKey,
                o.TheatreCode,
                o.OperationEndDate,
                o.OperationTypeCode,
                o.InSuiteTime,
                o.InRecoveryTime,
                o.PatientSourceUniqueNo,
                o.AnaestheticInductionTime,
                o.AnaestheticReadyTime,
                o.InAnaestheticTime,
                p.ProcedureStartTime,
                p.ProcedureEndTime,
                o.OperationCancelledFlag,
                p.Surgeon1Code,
                p.Surgeon2Code,
                p.Surgeon3Code,
                p.Anaesthetist1Code,
                p.Anaesthetist2Code,
                p.Anaesthetist3Code,
                o.sessionsourceuniqueID,
                o.SpecialtyCode
         FROM   [WH].Theatre.OperationDetail o
                LEFT OUTER JOIN WH.Theatre.ProcedureDetail p
                             ON o.SourceUniqueID = p.OperationDetailSourceUniqueID
                                AND p.PrimaryProcedureFlag = 1
                 LEFT OUTER JOIN WHREPORTING.LK.Calendar cal on o.OperationDate = cal.TheDate -- join back to the calendar table to get the day of the week top be used in the final queries further down.
        --WHERE  SessionSourceUniqueID = '160905' 
        ),


--CTE to get the min/ max times for each theatre on each day.  Uses a union as for CT and TDC we want to look at all activity but for Acute Block, F Block and ENT we only want to look at the min/max for Elective activity.
     OpTimes (TheatreCode, Theatre, OperationDate, DayOfWeek, DayOfWeekKey, ProcedureStartTime, ProcedureEndTime, InAnaestheticTime, OperationStartDate)
     AS (SELECT OPDetails.TheatreCode,
                OlapTheatre.Theatre,
                OPDetails.OperationDate,
                OPDetails.DayOfWeek,
                OPDetails.DayOfWeekKey,
                Min(ProcedureStartTime) AS ProcedureStartTime,
                Max(ProcedureEndtime)   AS ProcedureEndTime,
                Min(AnaestheticInductionTime)  AS InAnaestheticTime,
                Min(AnaestheticReadyTime) AS OperationStartDate
         FROM   OPDetails
                INNER JOIN WHOLAP.dbo.OlapTheatre
                        ON OPDetails.TheatreCode = OlapTheatre.TheatreCode
         WHERE  OlapTheatre.OperatingSuite IN ( 'F BLOCK AND ENT THEATRES', 'ACUTE BLOCK THEATRES' )
                AND OlapTheatre.TheatreCode NOT IN ( 24, 25, 45, 86,
                                                     4, 32, 33, 72 )--only certain theatres are to be included. Virtual Theatres are to be excluded so this has been written into the where clause here
                AND OperationTypeCode IN ( 'Elective' )
         GROUP  BY OPDetails.TheatreCode,
                   OlapTheatre.Theatre,
                  -- OperationTypeCode,
                   OPDetails.OperationDate,
                   OPDetails.DayOfWeek,
					OPDetails.DayOfWeekKey
         UNION ALL
         SELECT OPDetails.TheatreCode,
                OlapTheatre.Theatre,
                OPDetails.OperationDate,
                OPDetails.DayOfWeek,
				OPDetails.DayOfWeekKey,
                Min(ProcedureStartTime) AS ProcedureStartTime,
                Max(ProcedureEndtime)   AS ProcedureEndTime,
                Min(AnaestheticInductionTime)  AS InAnaestheticTime,
                Min(AnaestheticReadyTime) AS OperationStartDate
         FROM   OPDetails
                INNER JOIN WHOLAP.dbo.OlapTheatre
                        ON OPDetails.TheatreCode = OlapTheatre.TheatreCode
         WHERE  OlapTheatre.OperatingSuite IN ( 'CARDIO THORACIC THEATRES', 'TDC' )
                AND OlapTheatre.TheatreCode NOT IN ( 36, 71, 46, 51, 80 )--only certain theatres are to be included. Virtual Theatres are to be excluded so this has been written into the where clause here
         GROUP  BY OPDetails.TheatreCode,
                   OlapTheatre.Theatre,
                  -- OPDetails.OperationTypeCode,
                   OPDetails.OperationDate,
                   OPDetails.DayOfWeek,
					OPDetails.DayOfWeekKey)
                   




--The query uses the OPTimes created above and joins back to the OPDetails above to get the details for each of the min/max activity.
--It is a union of 4 queries each of which look at the Surg Start Time, Surg End Time, Anaesthetic Start Time and Anaesthetic End Time 
SELECT o.OperationDate,
       o.Theatre,
       'Surg Start'                                              AS Process,
       TT.TimeTarget                                             AS Target,
       cast(cast(o.OperationDate as varchar) + ' ' + TT.TimeTarget as datetime) as TargetDateTime,--calculate a target date time to be used in the SSRS for conditional formatting.  Date needs to be included as some operations end up finishing in the early hours of the following day which will show as green if only the time was used.
       LEFT(Cast(Dateadd(MINUTE, 15, TT.TimeTarget) AS TIME), 8) AS TargetAmber,--this is the target time for amber -if the  15mins after the target start
         cast(o.OperationDate as varchar) + ' ' + Dateadd(MINUTE, 15, TT.TimeTarget) as TargetAmberDateTime,
         p.ProcedureStartTime                                      AS ProcedureStartDateTime,
       s4.Forename + ' ' + s4.Surname                            AS Surgeon,
       LEFT(Cast(p.ProcedureStartTime AS TIME), 8)               AS Time,
       o.DayOfWeek,
       3                                                         AS ProcessOrder
FROM   OpTimes o
       LEFT OUTER JOIN OPDetails p --join back to the same table to get the details of the earliest surgery start time i.e the surgeon etc
                    ON o.OperationDate = p.OperationDate
                       AND o.TheatreCode = p.TheatreCode
                       AND o.ProcedureStartTime = p.ProcedureStartTime
       --INNER JOIN [WH].Theatre.Theatre AS t
       --        ON t.TheatreCode = o.TheatreCode
       LEFT OUTER JOIN [WH].Theatre.Staff AS s1
                    ON s1.StaffCode = p.Anaesthetist1Code
       LEFT OUTER JOIN [WH].Theatre.Staff AS s4
                    ON s4.StaffCode = p.SurgeonCode1
       LEFT OUTER JOIN [WH].WH.Calendar AS c -- join to calendar to get day of the week
                    ON c.TheDate = o.OperationDate
       LEFT OUTER JOIN [WH].Theatre.Specialty AS sp
                    ON sp.SpecialtyCode = p.SpecialtyCode
       --LEFT OUTER JOIN [WHOLAP].[dbo].[OlapTheatre] Th -- join to [WHOLAP].[dbo].[OlapTheatre] to get the block details so only certain blocks can be specified in where clause
       --             ON t.Theatre = Th.Theatre
       LEFT OUTER JOIN TH.TheatreTargetStartEndTimes TT
                    ON O.Theatre = TT.Theatre and o.DayOfWeek = TT.DayOfWeek
                       AND TT.Process = 'Knife-to-skin'
WHERE  ( o.OperationDate BETWEEN @StartDate AND @EndDate )
       AND ( p.OperationCancelledFlag = 0 )
       AND O.Theatre IN (SELECT Item
                         FROM   dbo.Split (@Theatre, ','))
UNION ALL
--Query to get the Surg End Time -  broken down into two queries - one to look at Cardio Thoracic and TDC block - this is to include all patients and one to look at F Block and ENT - this is to include only Elective patients
SELECT o.OperationDate,
       O.Theatre,
       'Surg End'                                                AS Process,
       TT.TimeTarget                                             AS Target,
       cast(cast(o.OperationDate as varchar) + ' ' + TT.TimeTarget as datetime) as TargetDateTime,
       LEFT(Cast(Dateadd(MINUTE, 15, TT.TimeTarget) AS TIME), 8) AS TargetAmber,
         cast(o.OperationDate as varchar) + ' ' + Dateadd(MINUTE, 15, TT.TimeTarget) as TargetAmberDateTime,
       p.ProcedureEndTime                                        AS ProcedureEndDateTime,
       s4.Forename + ' ' + s4.Surname                            AS Surgeon,
       LEFT(Cast(p.ProcedureEndTime AS TIME), 8)                 AS Time,
       o.DayOfWeek,
       4                                                         AS ProcessOrder
FROM   OPTimes o
       LEFT OUTER JOIN OPDetails p --join back to the same table to get the details of the earliest surgery start time i.e the surgeon etc
                    ON o.OperationDate = p.OperationDate
                       AND o.TheatreCode = p.TheatreCode
                       AND o.ProcedureEndTime = p.ProcedureEndTime
       --INNER JOIN [WH].Theatre.Theatre AS t
       --        ON t.TheatreCode = o.TheatreCode
       LEFT OUTER JOIN [WH].Theatre.Staff AS s1
                    ON s1.StaffCode = p.Anaesthetist1Code
       LEFT OUTER JOIN [WH].Theatre.Staff AS s4
                    ON s4.StaffCode = p.SurgeonCode1
       LEFT OUTER JOIN [WH].WH.Calendar AS c
                    ON c.TheDate = o.OperationDate
       LEFT OUTER JOIN [WH].Theatre.Specialty AS sp
                    ON sp.SpecialtyCode = p.SpecialtyCode
       --LEFT OUTER JOIN [WHOLAP].[dbo].[OlapTheatre] Th
       --             ON t.Theatre = Th.Theatre
         LEFT OUTER JOIN TH.TheatreTargetStartEndTimes TT
                    ON O.Theatre = TT.Theatre and o.DayOfWeek = TT.DayOfWeek
                       AND TT.Process = 'List finish time'
WHERE  ( o.OperationDate BETWEEN @StartDate AND @EndDate )
       AND ( p.OperationCancelledFlag = 0 )
       AND O.Theatre IN (SELECT Item
                         FROM   dbo.Split (@Theatre, ','))
UNION ALL
--query to get Anaesthetic start time - broken down into two queries - one to look at Cardio Thoracic and TDC block - this is to include all patients and one to look at F Block and ENT - this is to include only Elective patients
SELECT o.OperationDate,
       o.Theatre,
       'Anaes Start',
       TT.TimeTarget                                             AS Target,
       cast(cast(o.OperationDate as varchar) + ' ' + TT.TimeTarget as datetime) as TargetDateTime,
       LEFT(Cast(Dateadd(MINUTE, 15, TT.TimeTarget) AS TIME), 8) AS TargetAmber,
         cast(o.OperationDate as varchar) + ' ' + Dateadd(MINUTE, 15, TT.TimeTarget) as TargetAmberDateTime,
       p.AnaestheticInductionTime                                       AS InAnaestheticDateTime,
       s1.Forename + ' ' + s1.Surname                            AS Surgeon,
       LEFT(Cast(p.AnaestheticInductionTime AS TIME), 8)                AS Time,
       o.DayOfWeek,
       1                                                       AS ProcessOrder
FROM   OPTimes o
       LEFT OUTER JOIN OPDetails p --join back to the same table to get the details of the earliest surgery start time i.e the surgeon etc
                    ON o.OperationDate = p.OperationDate
                       AND o.TheatreCode = p.TheatreCode
                       AND o.InAnaestheticTime = p.AnaestheticInductionTime
       --INNER JOIN [WH].Theatre.Theatre AS t
       --        ON t.TheatreCode = o.TheatreCode
       LEFT OUTER JOIN [WH].Theatre.Staff AS s1
                    ON s1.StaffCode = p.Anaesthetist1Code
       LEFT OUTER JOIN [WH].Theatre.Staff AS s4
                    ON s4.StaffCode = p.SurgeonCode1
       LEFT OUTER JOIN [WH].WH.Calendar AS c
                    ON c.TheDate = o.OperationDate
       LEFT OUTER JOIN [WH].Theatre.Specialty AS sp
                    ON sp.SpecialtyCode = p.SpecialtyCode
       --LEFT OUTER JOIN [WHOLAP].[dbo].[OlapTheatre] Th
       --             ON t.Theatre = Th.Theatre
       LEFT OUTER JOIN TH.TheatreTargetStartEndTimes TT
                    ON O.Theatre = TT.Theatre and o.DayOfWeek = TT.DayOfWeek
                       AND TT.Process = 'Needle-to-skin'
WHERE  ( o.OperationDate BETWEEN @StartDate AND @EndDate )
       AND ( p.OperationCancelledFlag = 0 )
       AND o.Theatre IN (SELECT Item
                         FROM   dbo.Split (@Theatre, ','))
UNION ALL
--query to get Anaesthetic End Time broken down into two queries - one to look at Cardio Thoracic and TDC block - this is to include all patients and one to look at F Block and ENT - this is to include only Elective patients
SELECT o.OperationDate,
       o.Theatre,
       'Anaes End',
       TT.TimeTarget                                             AS Target,
       cast(cast(o.OperationDate as varchar) + ' ' + TT.TimeTarget as datetime) as TargetDateTime,
       LEFT(Cast(Dateadd(MINUTE, 15, TT.TimeTarget) AS TIME), 8) AS TargetAmber,
         cast(o.OperationDate as varchar) + ' ' + Dateadd(MINUTE, 15, TT.TimeTarget) as TargetAmberDateTime,
       p.AnaestheticReadyTime                                      AS OperationStartDateDate,
       s1.Forename + ' ' + s1.Surname                            AS Surgeon,
       LEFT(Cast(p.AnaestheticReadyTime AS TIME), 8)               AS Time,
       o.DayOfWeek,
       2                                                         AS ProcessOrder
FROM   OPTimes o
       LEFT OUTER JOIN OPDetails p --join back to the same table to get the details of the earliest surgery start time i.e the surgeon etc
                    ON o.OperationDate = p.OperationDate
                       AND o.TheatreCode = p.TheatreCode
                       AND o.OperationStartDate = p.AnaestheticReadyTime
       --INNER JOIN [WH].Theatre.Theatre AS t
       --        ON t.TheatreCode = o.TheatreCode
       LEFT OUTER JOIN [WH].Theatre.Staff AS s1
                    ON s1.StaffCode = p.Anaesthetist1Code
       LEFT OUTER JOIN [WH].Theatre.Staff AS s4
                    ON s4.StaffCode = p.SurgeonCode1
       LEFT OUTER JOIN [WH].WH.Calendar AS c
                    ON c.TheDate = o.OperationDate
       LEFT OUTER JOIN [WH].Theatre.Specialty AS sp
                    ON sp.SpecialtyCode = p.SpecialtyCode
       --LEFT OUTER JOIN [WHOLAP].[dbo].[OlapTheatre] Th
       --             ON t.Theatre = Th.Theatre
         LEFT OUTER JOIN TH.TheatreTargetStartEndTimes TT
                    ON O.Theatre = TT.Theatre and o.DayOfWeek = TT.DayOfWeek
                       AND TT.Process = 'Into theatre'
WHERE  ( o.OperationDate BETWEEN @StartDate AND @EndDate )
       AND ( p.OperationCancelledFlag = 0 )
       AND o.Theatre IN (SELECT Item
                         FROM   dbo.Split (@Theatre, ',')) 




END