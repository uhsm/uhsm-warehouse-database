﻿-- =============================================
-- Author:		CMan
-- Create date: 18/11/2013
-- Description:	Stored Procedure to give summary of all theatres by block against certain measures e.g percentage knife to skin before target
-- =============================================
CREATE PROCEDURE [TH].[ReportTheatreTimesAuditSummary] @startdate datetime, @enddate datetime
	-- Add the parameters for the stored procedure here
	
	

/*
 Modified Date				Modified By			Modification
-----------------			------------		------------
27/03/2014					CMan				Amended the Anaesthetic time to reflect request from CBH.  Changing the Anaesthetic start time to read from the AnaestheticInductionTime field and the Anaesthetic End Time to read from the AnaestheticReadyTime field.



*/

	
	
AS
BEGIN
----Parameters declared for testing stored procedure
--DECLARE
--@startdate DATETIME,
--@enddate   DATETIME

--SET @startdate = '20131028'
--SET @enddate = '20131101';

--CTE to create the combination of different target times - these are different depending on which theatre it is.
WITH TimeTarget1 (Process, TimeTarget)
     AS (SELECT 'Needle-to-skin' AS Process,
                '08:30'          AS TimeTarget
         UNION ALL
         SELECT 'Into theatre',
                '09:00'
         UNION ALL
         SELECT 'Knife-to-skin',
                '09:15'
         UNION ALL
         SELECT 'List finish time',
                '18:00'),
     TimeTarget2 (Process, TimeTarget)
     AS (SELECT 'Needle-to-skin' AS Process,
                '09:00'          AS TimeTarget
         UNION ALL
         SELECT 'Into theatre',
                '09:20'
         UNION ALL
         SELECT 'Knife-to-skin',
                '09:30'
         UNION ALL
         SELECT 'List finish time',
                '17:00'),
     TimeTarget3 (Process, TimeTarget)
     AS (SELECT 'Needle-to-skin' AS Process,
                '08:30'          AS TimeTarget
         UNION ALL
         SELECT 'Into theatre',
                '09:30'
         UNION ALL
         SELECT 'Knife-to-skin',
                '09:45'
         UNION ALL
         SELECT 'List finish time',
                '18:00'),
     --CTE to asign then appropriate target times generated in the CTEs above to the different theatres by cross joining the target time to the correspongding theatre
     TheatreTarget (Theatre, Process, TimeTarget)
     AS (SELECT *
         FROM   (SELECT Theatre,
                        Process,
                        TimeTarget
                 FROM   TimeTarget1
                        CROSS JOIN [WHOLAP].[dbo].[OlapTheatre]
                 WHERE  OperatingSuite = 'CARDIO THORACIC THEATRES'
                        AND TheatreCode IN ( 38, 40 )
                 UNION ALL
                 SELECT Theatre,
                        Process,
                        TimeTarget
                 FROM   TimeTarget3
                        CROSS JOIN [WHOLAP].[dbo].[OlapTheatre]
                 WHERE  OperatingSuite = 'CARDIO THORACIC THEATRES'
                        AND TheatreCode IN ( 37, 39 )
                 UNION ALL
                 SELECT Theatre,
                        Process,
                        TimeTarget
                 FROM   TimeTarget2
                        CROSS JOIN [WHOLAP].[dbo].[OlapTheatre]
                 WHERE  OperatingSuite IN ( 'F BLOCK AND ENT THEATRES', 'TDC', 'ACUTE BLOCK THEATRES' )
                        AND TheatreCode NOT IN ( 24, 25, 36, 45,
                                                 71, 86, 4, 32,
                                                 33, 72 ))TT),
     Final (OperationDate, TheatreBlock, Theatre, Process, Target, TargetAmber, DateTime, Time, DayOfWeek, [Count])
     AS (
        --Query to get the Surg Start Time
        SELECT OperationDate,
               OperatingSuite                                   AS TheatreBlock,
               OP.Theatre,
               'Surg Start'                                     AS Process,
               TT.TimeTarget                                    AS Target,
               Cast(Dateadd(MINUTE, 15, TT.TimeTarget) AS TIME) AS TargetAmber,
               ProcedureStartTime                               AS ProcedureStartDateTime,
               LEFT(Cast(ProcedureStartTime AS TIME), 8)        AS Time,
               DayOfWeek,
               1                                                AS [Count]
        FROM   (SELECT Cast(o.OperationDate AS DATE) AS OperationDate,
                       t.OperatingSuite,
                       t.Theatre,
                       Min(p.ProcedureStartTime)     AS ProcedureStartTime,
                       c.DayOfWeek
                FROM   [WH].Theatre.OperationDetail AS o
                       LEFT OUTER JOIN [WH].Theatre.ProcedureDetail AS p
                                    ON p.OperationDetailSourceUniqueID = o.SourceUniqueID
                                       AND p.PrimaryProcedureFlag = 1
                       INNER JOIN [WHOLAP].[dbo].[OlapTheatre]AS t
                               ON t.TheatreCode = o.TheatreCode
                       LEFT OUTER JOIN [WH].WH.Calendar AS c
                                    ON c.TheDate = o.OperationDate
                       LEFT OUTER JOIN [WH].Theatre.Specialty AS sp
                                    ON sp.SpecialtyCode = o.SpecialtyCode
                WHERE  ( o.OperationDate BETWEEN @StartDate AND @EndDate )
                       AND ( o.OperationCancelledFlag = 0 )
                       AND T.OperatingSuite IN ( 'CARDIO THORACIC THEATRES', 'TDC' )
                       AND T.TheatreCode NOT IN ( 36, 71, 46, 51, 80 ) --only certain theatres within the blocks need to be included. Others such as virtual theatres is not to ve included for this report
                GROUP  BY Cast(o.OperationDate AS DATE),
                          t.OperatingSuite,
                          t.Theatre,
                          c.DayOfWeek
                UNION ALL
                SELECT Cast(o.OperationDate AS DATE) AS OperationDate,
                       t.operatingsuite,
                       t.Theatre,
                       Min(p.ProcedureStartTime)     AS ProcedureStartTime,
                       c.DayOfWeek
                FROM   [WH].Theatre.OperationDetail AS o
                       LEFT OUTER JOIN [WH].Theatre.ProcedureDetail AS p
                                    ON p.OperationDetailSourceUniqueID = o.SourceUniqueID
                                       AND p.PrimaryProcedureFlag = 1
                       INNER JOIN [WHOLAP].dbo.OlapTheatre AS t
                               ON t.TheatreCode = o.TheatreCode
                       LEFT OUTER JOIN [WH].WH.Calendar AS c
                                    ON c.TheDate = o.OperationDate
                       LEFT OUTER JOIN [WH].Theatre.Specialty AS sp
                                    ON sp.SpecialtyCode = o.SpecialtyCode
                WHERE  ( o.OperationDate BETWEEN @StartDate AND @EndDate )
                       AND ( o.OperationCancelledFlag = 0 )
                       AND T.OperatingSuite IN( 'F BLOCK AND ENT THEATRES', 'ACUTE BLOCK THEATRES' )
                       AND T.TheatreCode NOT IN ( 24, 25, 45, 86,
                                                  4, 32, 33, 72 )
                       AND ( o.OperationTypeCode IN ( 'Elective' ) )
                GROUP  BY Cast(o.OperationDate AS DATE),
                          t.OperatingSuite,
                          t.Theatre,
                          c.DayOfWeek) OP
               LEFT OUTER JOIN TheatreTarget TT
                            ON OP.Theatre = TT.Theatre
                               AND TT.Process = 'Knife-to-skin'
        UNION ALL
        --Query to get the Surg End Time
        SELECT OperationDate,
               OperatingSuite                                   AS TheatreBlock,
               OP.Theatre,
               'Surg End'                                       AS Process,
               TT.TimeTarget                                    AS Target,
               Cast(Dateadd(MINUTE, 15, TT.TimeTarget) AS TIME) AS TargetAmber,
               ProcedureEndTime                                 AS ProcedureEndDateTime,
               LEFT(Cast(ProcedureEndTime AS TIME), 8)          AS Time,
               DayOfWeek,
               1                                                AS [Count]
        FROM   (SELECT Cast(o.OperationDate AS DATE) AS OperationDate,
                       t.OperatingSuite,
                       t.Theatre,
                       Max(p.ProcedureEndTime)       AS ProcedureEndTime,
                       c.DayOfWeek
                FROM   [WH].Theatre.OperationDetail AS o
                       LEFT OUTER JOIN [WH].Theatre.ProcedureDetail AS p
                                    ON p.OperationDetailSourceUniqueID = o.SourceUniqueID
                                       AND p.PrimaryProcedureFlag = 1
                       INNER JOIN [WHOLAP].dbo.OlapTheatre AS t
                               ON t.TheatreCode = o.TheatreCode
                       LEFT OUTER JOIN [WH].WH.Calendar AS c
                                    ON c.TheDate = o.OperationDate
                       LEFT OUTER JOIN [WH].Theatre.Specialty AS sp
                                    ON sp.SpecialtyCode = o.SpecialtyCode
                WHERE  ( o.OperationDate BETWEEN @StartDate AND @EndDate )
                       AND ( o.OperationCancelledFlag = 0 )
                       AND T.OperatingSuite IN ( 'CARDIO THORACIC THEATRES', 'TDC' )
                       AND T.TheatreCode NOT IN ( 36, 71, 46, 51, 80 ) --only certain theatres within the blocks need to be included. Others such as virtual theatres is not to ve included for this report
                GROUP  BY Cast(o.OperationDate AS DATE),
                          t.OperatingSuite,
                          t.Theatre,
                          c.DayOfWeek
                UNION ALL
                SELECT Cast(o.OperationDate AS DATE) AS OperationDate,
                       t.OperatingSuite,
                       t.Theatre,
                       Max(p.ProcedureEndTime)       AS ProcedureEndTime,
                       c.DayOfWeek
                FROM   [WH].Theatre.OperationDetail AS o
                       LEFT OUTER JOIN [WH].Theatre.ProcedureDetail AS p
                                    ON p.OperationDetailSourceUniqueID = o.SourceUniqueID
                                       AND p.PrimaryProcedureFlag = 1
                       INNER JOIN [WHOLAP].dbo.OLAPTheatre AS t
                               ON t.TheatreCode = o.TheatreCode
                       LEFT OUTER JOIN [WH].WH.Calendar AS c
                                    ON c.TheDate = o.OperationDate
                       LEFT OUTER JOIN [WH].Theatre.Specialty AS sp
                                    ON sp.SpecialtyCode = o.SpecialtyCode
                WHERE  ( o.OperationDate BETWEEN @StartDate AND @EndDate )
                       AND ( o.OperationCancelledFlag = 0 )
                       AND T.OperatingSuite IN( 'F BLOCK AND ENT THEATRES', 'ACUTE BLOCK THEATRES' )
                       AND T.TheatreCode NOT IN ( 24, 25, 45, 86,
                                                  4, 32, 33, 72 )
                       AND ( o.OperationTypeCode IN ( 'Elective' ) )
                GROUP  BY Cast(o.OperationDate AS DATE),
                          t.OperatingSuite,
                          t.Theatre,
                          c.DayOfWeek) OP
               LEFT OUTER JOIN TheatreTarget TT
                            ON OP.Theatre = TT.Theatre
                               AND TT.Process = 'List finish time'
        UNION ALL
        --query to get Anaesthetic start time
        SELECT OperationDate,
               OperatingSuite                                   AS TheatreBlock,
               OP.Theatre,
               'Anaes Start',
               TT.TimeTarget                                    AS Target,
               Cast(Dateadd(MINUTE, 15, TT.TimeTarget) AS TIME) AS TargetAmber,
               InAnaestheticTime                                AS InAnaestheticDateTime,
               LEFT(Cast(InAnaestheticTime AS TIME), 8)         AS Time,
               DayOfWeek,
               1                                                AS [Count]
        FROM   (SELECT Cast(o.OperationDate AS DATE) AS OperationDate,
                       t.OperatingSuite,
                       t.Theatre,
                       Min(o.AnaestheticInductionTime)      AS InAnaestheticTime,
                       c.DayOfWeek
                FROM   [WH].Theatre.OperationDetail AS o
                       LEFT OUTER JOIN [WH].Theatre.ProcedureDetail AS p
                                    ON p.OperationDetailSourceUniqueID = o.SourceUniqueID
                                       AND p.PrimaryProcedureFlag = 1
                       INNER JOIN [WHOLAP].dbo.OLAPTheatre AS t
                               ON t.TheatreCode = o.TheatreCode
                       LEFT OUTER JOIN [WH].WH.Calendar AS c
                                    ON c.TheDate = o.OperationDate
                       LEFT OUTER JOIN [WH].Theatre.Specialty AS sp
                                    ON sp.SpecialtyCode = o.SpecialtyCode
                WHERE  ( o.OperationDate BETWEEN @StartDate AND @EndDate )
                       AND ( o.OperationCancelledFlag = 0 )
                       AND T.OperatingSuite IN ( 'CARDIO THORACIC THEATRES', 'TDC' )
                       AND T.TheatreCode NOT IN ( 36, 71, 46, 51, 80 ) --only certain theatres within the blocks need to be included. Others such as virtual theatres is not to ve included for this report
                GROUP  BY Cast(o.OperationDate AS DATE),
                          t.OperatingSuite,
                          t.Theatre,
                          c.DayOfWeek
                UNION ALL
                SELECT Cast(o.OperationDate AS DATE) AS OperationDate,
                       t.OperatingSuite,
                       t.Theatre,
                       Min(o.AnaestheticInductionTime)      AS InAnaestheticTime,
                       c.DayOfWeek
                FROM   [WH].Theatre.OperationDetail AS o
                       LEFT OUTER JOIN [WH].Theatre.ProcedureDetail AS p
                                    ON p.OperationDetailSourceUniqueID = o.SourceUniqueID
                                       AND p.PrimaryProcedureFlag = 1
                       INNER JOIN [WHOLAP].dbo.OLAPTheatre AS t
                               ON t.TheatreCode = o.TheatreCode
                       LEFT OUTER JOIN [WH].WH.Calendar AS c
                                    ON c.TheDate = o.OperationDate
                       LEFT OUTER JOIN [WH].Theatre.Specialty AS sp
                                    ON sp.SpecialtyCode = o.SpecialtyCode
                WHERE  ( o.OperationDate BETWEEN @StartDate AND @EndDate )
                       AND ( o.OperationCancelledFlag = 0 )
                       AND T.OperatingSuite IN( 'F BLOCK AND ENT THEATRES', 'ACUTE BLOCK THEATRES' )
                       AND T.TheatreCode NOT IN ( 24, 25, 45, 86,
                                                  4, 32, 33, 72 )
                       AND ( o.OperationTypeCode IN ( 'Elective' ) )
                GROUP  BY Cast(o.OperationDate AS DATE),
                          t.OperatingSuite,
                          t.Theatre,
                          c.DayOfWeek) OP
               LEFT OUTER JOIN TheatreTarget TT
                            ON OP.Theatre = TT.Theatre
                               AND TT.Process = 'Needle-to-skin'
         UNION ALL
         --query to get Anaesthetic End Time
         SELECT OperationDate,
                OperatingSuite                                   AS TheatreBlock,
                OP.Theatre,
                'Anaes End',
                TT.TimeTarget                                    AS Target,
                Cast(Dateadd(MINUTE, 15, TT.TimeTarget) AS TIME) AS TargetAmber,
                OperationStartDate                               AS OperationStartDateDate,
                LEFT(Cast(OperationStartDate AS TIME), 8)        AS Time,
                DayOfWeek,
                1                                                AS [Count]
         FROM   (SELECT Cast(o.OperationDate AS DATE) AS OperationDate,
                        t.OperatingSuite,
                        t.Theatre,
                        Min(o.AnaestheticReadyTime)     AS OperationStartDate,
                        c.DayOfWeek
                 FROM   [WH].Theatre.OperationDetail AS o
                        LEFT OUTER JOIN [WH].Theatre.ProcedureDetail AS p
                                     ON p.OperationDetailSourceUniqueID = o.SourceUniqueID
                                        AND p.PrimaryProcedureFlag = 1
                        INNER JOIN [WHOLAP].dbo.OlapTheatre AS t
                                ON t.TheatreCode = o.TheatreCode
                        LEFT OUTER JOIN [WH].WH.Calendar AS c
                                     ON c.TheDate = o.OperationDate
                        LEFT OUTER JOIN [WH].Theatre.Specialty AS sp
                                     ON sp.SpecialtyCode = o.SpecialtyCode
                 WHERE  ( o.OperationDate BETWEEN @StartDate AND @EndDate )
                        AND ( o.OperationCancelledFlag = 0 )
                        AND T.OperatingSuite IN ( 'CARDIO THORACIC THEATRES', 'TDC' )
                        AND T.TheatreCode NOT IN ( 36, 71, 46, 51, 80 ) --only certain theatres within the blocks need to be included. Others such as virtual theatres is not to ve included for this report
                 GROUP  BY Cast(o.OperationDate AS DATE),
                           t.OperatingSuite,
                           t.Theatre,
                           c.DayOfWeek
                 UNION ALL
                 SELECT Cast(o.OperationDate AS DATE) AS OperationDate,
                        t.OperatingSuite,
                        t.Theatre,
                        Min(o.AnaestheticReadyTime)     AS OperationStartDate,
                        c.DayOfWeek
                 FROM   [WH].Theatre.OperationDetail AS o
                        LEFT OUTER JOIN [WH].Theatre.ProcedureDetail AS p
                                     ON p.OperationDetailSourceUniqueID = o.SourceUniqueID
                                        AND p.PrimaryProcedureFlag = 1
                        INNER JOIN [WHOLAP].dbo.OlapTheatre AS t
                                ON t.TheatreCode = o.TheatreCode
                        LEFT OUTER JOIN [WH].WH.Calendar AS c
                                     ON c.TheDate = o.OperationDate
                        LEFT OUTER JOIN [WH].Theatre.Specialty AS sp
                                     ON sp.SpecialtyCode = o.SpecialtyCode
                 WHERE  ( o.OperationDate BETWEEN @StartDate AND @EndDate )
                        AND ( o.OperationCancelledFlag = 0 )
                        AND T.OperatingSuite IN( 'F BLOCK AND ENT THEATRES', 'ACUTE BLOCK THEATRES' )
                        AND T.TheatreCode NOT IN ( 24, 25, 45, 86,
                                                   4, 32, 33, 72 )
                        AND ( o.OperationTypeCode IN ( 'Elective' ) )
                 GROUP  BY Cast(o.OperationDate AS DATE),
                           t.OperatingSuite,
                           t.Theatre,
                           c.DayOfWeek) OP
                LEFT OUTER JOIN TheatreTarget TT
                             ON OP.Theatre = TT.Theatre
                                AND TT.Process = 'Into theatre') SELECT TheatreBlock,
       'Anaes Needle-to-Skin Before Target' AS Measure,
       Sum(count)                           AS Total,
       Sum(CASE
             WHEN LEFT(Time, 5) <= Target THEN 1
             ELSE 0
           END)                             AS StartBeforeTarget
FROM   Final
WHERE  Process = 'Anaes Start'
       AND TheatreBlock IS NOT NULL
GROUP  BY TheatreBlock
UNION ALL
SELECT TheatreBlock,
       'Knife-to-Skin Before Target' AS Measure,
       Sum(count),
       Sum(CASE
             WHEN LEFT(Time, 5) <= Target THEN 1
             ELSE 0
           END)                      AS StartBeforeTarget
FROM   Final
WHERE  Process = 'Surg Start'
       AND TheatreBlock IS NOT NULL
GROUP  BY TheatreBlock
UNION ALL
SELECT TheatreBlock,
       'Surgery Finish Before Target' AS Measure,
       Sum(count),
       Sum(CASE
             WHEN LEFT(Time, 5) <= Target THEN 1
             ELSE 0
           END)                       AS StartBeforeTarget
FROM   Final
WHERE  Process = 'Surg End'
       AND TheatreBlock IS NOT NULL
GROUP  BY TheatreBlock
ORDER  BY 1,
          2 
END