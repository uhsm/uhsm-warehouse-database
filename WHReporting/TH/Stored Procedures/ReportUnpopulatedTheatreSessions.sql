﻿-- =============================================
-- Author:		CMan
-- Create date: 22/07/2015
-- Description:	Stored procedure for generating the Unpopulated Theatre Sessions SSRS report
--				

-- =============================================
CREATE  PROCEDURE [TH].[ReportUnpopulatedTheatreSessions] @start datetime, @end datetime
AS

/*
 Modified Date				Modified By			Modification
-----------------			------------		------------
22/07/2015					CMan				Created


*/





BEGIN




--Declare @start datetime = '20150729'
--,@end datetime =  '20150730'


SELECT cal.TheDate
		,cal.DayOfWeek 
		,cal.LastDayOfWeek
		,TheatreSessions.SessionSpecialty
		,TheatreSessions.SessionTheatre
		,TheatreSessions.SessionTheatreCode
		,TheatreSessions.SessionSourceUniqueID
		,TheatreSessions.SessionStartTime
		,TheatreSessions.SessionConsultant
		,TheatreSessions.SessionCancelledFlag
		,TheatreSessions.PlannedSessionMinutes
		--,Booking.BookingUniqueID
		,ISNULL(Sum(Booking.NotCancelledFlag),0)  as NumberOfPatientsBooked
		,Case when Sum(Booking.NotCancelledFlag)  IS NULL then 1 Else 0 End as UnpopulatedSession
	
	FROM WHREPORTING.QLIK.TheatreSessions
	LEFT OUTER JOIN (
		SELECT PatientBooking.SourceUniqueID AS BookingUniqueID
			,PatientBooking.SessionSourceUniqueID
			,CASE 
				WHEN cancellations.PatientSourceUniqueID IS NULL
					THEN 1
				ELSE 0
				END AS NotCancelledFlag
		FROM WH.Theatre.PatientBooking
		LEFT OUTER JOIN (
			SELECT *
			FROM WH.Theatre.Cancellation
			) cancellations ON PatientBooking.SourceUniqueID = cancellations.PatientSourceUniqueID
		) Booking ON TheatreSessions.SessionSourceUniqueID = Booking.SessionSourceUniqueID
	LEFT OUTER JOIN WHREPORTING.LK.Calendar cal on DateAdd(day, DATEDIFF(day, 0, TheatreSessions.SessionStartTime),0) = cal.TheDate
	WHERE  SessionStartTime >= @start
		AND SessionStartTime <= @end+ '23:59:00'
		AND TheatreSessions.SessionTheatreCode in ('1','2','3','6','78','13','15','19','21','14','16','20','23')

	Group By  cal.TheDate
		,cal.DayOfWeek
		,cal.LastDayOfWeek
		,TheatreSessions.SessionSpecialty 
		,TheatreSessions.SessionTheatre
		,TheatreSessions.SessionTheatreCode
		,TheatreSessions.SessionSourceUniqueID
		,TheatreSessions.SessionStartTime
		,TheatreSessions.SessionConsultant
		,TheatreSessions.SessionCancelledFlag
		,TheatreSessions.PlannedSessionMinutes
	ORDER BY TheatreSessions.SessionSourceUniqueID



END