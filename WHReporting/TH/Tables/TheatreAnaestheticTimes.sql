﻿CREATE TABLE [TH].[TheatreAnaestheticTimes](
	[AnaesthetistCode] [varchar](10) NULL,
	[Anaesthetist] [varchar](121) NULL,
	[PrimaryProcedureCodeGrouped] [varchar](3) NULL,
	[PrimaryProcedureDescription] [varchar](255) NULL,
	[MedianAnaestheticMinutes] [numeric](38, 6) NULL,
	[NumberOfCases] [int] NULL
) ON [PRIMARY]