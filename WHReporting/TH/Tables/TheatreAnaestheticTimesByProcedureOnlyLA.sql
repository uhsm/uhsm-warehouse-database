﻿CREATE TABLE [TH].[TheatreAnaestheticTimesByProcedureOnlyLA](
	[PrimaryProcedureCodeGrouped] [varchar](3) NULL,
	[MedianAnaestheticMinutes] [numeric](38, 6) NULL,
	[NumberOfCases] [int] NULL
) ON [PRIMARY]