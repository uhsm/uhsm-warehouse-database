﻿CREATE TABLE [TH].[TheatreProcedureTimesByProcedureOnly](
	[PrimaryProcedureCodeGrouped] [varchar](3) NULL,
	[PrimaryProcedureDescription] [varchar](255) NULL,
	[MedianOperationMinutes] [numeric](38, 6) NULL,
	[NumberOfCases] [int] NULL
) ON [PRIMARY]