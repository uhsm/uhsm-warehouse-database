﻿CREATE TABLE [TH].[TheatreProcedureTimes_old](
	[OperationDetailConsultantCode] [varchar](10) NULL,
	[OperationDetailConsultant] [varchar](121) NULL,
	[PrimaryProcedureCodeGrouped] [varchar](3) NULL,
	[PrimaryProcedureDescription] [varchar](255) NULL,
	[MedianOperationMinutes] [numeric](38, 6) NULL,
	[NumberOfCases] [int] NULL
) ON [PRIMARY]