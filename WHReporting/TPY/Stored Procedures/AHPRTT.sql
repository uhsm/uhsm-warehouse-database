﻿/*
--Author: J Cleworth
--Date created: 21/06/2013
--Stored Procedure Built for SRSS Report on:

*/

CREATE Procedure [TPY].[AHPRTT]


@Specialty varchar (20),
@RequestedService varchar (25)


AS

select 
      ref.ReferralSourceUniqueID
,     ref.FacilityID
,     ref.NHSNo
,     AgeCode
,     ReferralReceivedDate
,	  ref.RequestedService
,     case when spell.FaciltyID is null then 'Outpatient' else 'Inpatient' end 
			as RequestedServiceDerived -- derive by linking back to the spells dataset (Inpatient or Outpatient)
,     ref.ReferralMedium as ReferralType
,     ReferralSource
,     Priority
,     ReferralReason
,     ReceivingSpecialty
,     [ReceivingSpecialty(Function)]
,     ReceivingProfCarerType
,     ReceivingProfCarerName
,     ReferrerOrganisationName
,     ReferrerSpecialty
,     ReferrerName
,     EpisodeGP
,     EpisodePractice
,     EpisodeCCG
,     ReferralCreated
,     ReferralCreatedByWhom
,     rfenc.RTTPathwayID as RTTPathwayID
,     rfenc.RTTStartDate
,     rfenc.RTTCurrentStatusCode
,     rfenc.RTTCurrentStatusDate
,     rfenc.GeneralComment
,FutureAP.AppointmentDateTime AS BookedAppointmentDate
,FutureContact.Cont_Date as BookedContactDate
--,   '' as '1st DNA'
--,   '' as '
from RF.Referral ref

left join (

select FaciltyID, AdmissionDateTime,DischargeDateTime from APC.spell
where DischargeDate is null) spell

on spell.FaciltyID=ref.FacilityID

and ref.ReferralCreated between spell.AdmissionDateTime and GETDATE()

inner join WH.RF.Encounter rfenc on rfenc.SourceUniqueID=ref.ReferralSourceUniqueID 

left join 

(select referralsourceuniqueid,1 as OPAttended from WHREPORTING.OP.Schedule
where AppointmentDate<cast(getdate() as DATE)
and [Status]='Attend'
) ops

on ops.ReferralSourceUniqueID=ref.ReferralSourceUniqueID

left join (select referralsourceuniqueid,appointmentdatetime from whreporting.op.schedule
where AppointmentDate>=CAST(getdate() as date)) as FutureAP

ON FutureAP.ReferralSourceUniqueID=ref.ReferralSourceUniqueID

left join (select Ref_Ref, 1 as PtSeen from TPY.ContactsDataset

where Cont_Date<cast(GETDATE() as date)

) Cont on Cont.Ref_Ref=ref.ReferralSourceUniqueID


left join (select Ref_Ref, Cont_Date from TPY.ContactsDataset

where Cont_Date>=cast(GETDATE() as date) ) FutureContact on FutureContact.Ref_Ref=ref.ReferralSourceUniqueID


where [ReceivingSpecialtyCode(Function)] in ('654','650','651','653','652')

and OPAttended is null
and Cont.PtSeen is null

and ref.CancelDate is null
and ref.ReferralCreated>'2012-12-31'
and ReceivingSpecialtyCode in (@Specialty)
and case when spell.FaciltyID is null then 'Outpatient' else 'Inpatient' end  in (@RequestedService)