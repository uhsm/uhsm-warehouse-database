﻿CREATE Procedure [TPY].[BuildContactsDataset] 
as

/******************************************************************************
 Modification History --------------------------------------------------------

 Date		Author			Change
02/06/2014	KO				Added if statement to only run if not already run today
******************************************************************************/

declare @MaxModDate as datetime
declare @MaxCreateDate as datetime
--Only run if not already completed
if 
	(select DateValue from WH.dbo.Parameter where Parameter = 'BUILDCONTACTS')
	 < DATEADD(day, 0, datediff(day, 0,(getdate())))
	begin

		set @MaxModDate=(select MAX(Modified_Date) from  WHReporting.TPY.CONTACTSDATASET)
		set @MaxCreateDate=(select MAX(Create_Date) from  WHReporting.TPY.CONTACTSDATASET)



		MERGE WHReporting.TPY.CONTACTSDATASET as encounter
		USING
		(SELECT 	
			A.SCHDL_REFNO AS 'Cont_Ref',
			cast(A.REFRL_REFNO as varchar(50)) AS 'Ref_Ref',
			A.WLIST_REFNO AS 'WList_Ref',	
			CAST(A.PATNT_REFNO AS VARCHAR(20)) AS 'Pat_Ref',
			vw.IDENTIFIER as FACIL_ID,
			E.PATNT_REFNO_NHS_IDENTIFIER AS 'NHS_number',
			E.NNNTS_CODE AS 'NHSNoStat',
			E.FORENAME AS 'Forename',
			E.SURNAME AS 'Surname',
			CAST(E.DTTM_OF_BIRTH AS DATEtime) AS 'DOB',
			E.sexxx_refno_description as 'Gender',
			M.MAIN_CODE AS 'AdministrativeCategory',
			CAST(A.START_DTTM AS DATE) AS 'Cont_Date',
			RIGHT(A.START_DTTM,8) AS 'Cont_Time',
			B.MAIN_IDENT AS 'Spec_Code',
			B.[DESCRIPTION] AS 'Spec_Name',
			J.PROCA_REFNO_MAIN_IDENT AS 'Prof_Carer',
			J.FORENAME + ' ' + J.SURNAME AS 'Prof_Carer_Name',
			D.MAIN_IDENT AS 'Purchaser_Code', 		
			A.LOCATION AS 'Location',
			V.DESCRIPTION as 'Location_Type',
			AY.PROCA_REFNO_MAIN_IDENT AS 'Seen_By_Carer_Code',
			AY.SURNAME AS 'Seen_By_Carer_Surname',
			Z.DESCRIPTION as 'Resource_Unit_Type', 
			W.DESCRIPTION as 'Purpose',
			F.[DESCRIPTION] AS 'Visit_Type',	
			G.[DESCRIPTION] AS 'Urgency',
			H.[DESCRIPTION] AS 'Priority',
			A.ACTUAL_DURATION AS 'Contact_Duration',
			P.[DESCRIPTION] AS 'Contact_Type',
			R.[DESCRIPTION] AS 'Booking_Type',
			I.[DESCRIPTION] AS 'Cancellation_Reason',
			N.[DESCRIPTION] AS 'Cancelled_By',
			CAST(A.CANCR_DTTM AS DATETIME) AS 'Cancellation_Date',
			RIGHT(A.ARRIVED_DTTM,8) AS 'Arrival_Time',
			RIGHT(A.SEEN_DTTM,8) AS 'Seen_Time',	
			RIGHT(A.DEPARTED_DTTM,8) AS 'Depart_Time',
			K.[DESCRIPTION] AS 'Attend_Status',
			AZ.DESCRIPTION AS 'DNA_Reason',
			A.PLANNED_NUMBER AS 'Planned_Number',
			A.ACTUAL_NUMBER AS 'Actual_Number',
			L.[DESCRIPTION] AS 'Outcome',
			A.SPONT_REFNO AS 'Service_Point_Ref',
			Y.CODE AS 'Service_Point_Code',
			A.USER_CREATE AS CREATOR,
			CAST(A.CREATE_DTTM AS DATETIME) AS 'Create_Date',
			A.USER_MODIF AS MODIFIER,
			CAST(A.MODIF_DTTM AS DATETIME) AS 'Modified_Date'
		FROM 	
			lorenzo.dbo.ScheduleEvent A LEFT OUTER JOIN lorenzo.dbo.Specialty B ON A.SPECT_REFNO = B.SPECT_REFNO 
			LEFT OUTER JOIN lorenzo.dbo.[Contract] C ON A.CONTR_REFNO = C.CONTR_REFNO
			LEFT OUTER JOIN lorenzo.dbo.Purchaser D ON A.PURCH_REFNO = D.PURCH_REFNO
			LEFT OUTER JOIN lorenzo.dbo.Patient E ON A.PATNT_REFNO = E.PATNT_REFNO 
			LEFT OUTER JOIN lorenzo.dbo.ReferenceValue F ON A.VISIT_REFNO = F.RFVAL_REFNO 
			LEFT OUTER JOIN lorenzo.dbo.ReferenceValue G ON A.URGNC_REFNO = G.RFVAL_REFNO 
			LEFT OUTER JOIN lorenzo.dbo.ReferenceValue H ON A.PRITY_REFNO = H.RFVAL_REFNO 
			LEFT OUTER JOIN lorenzo.dbo.ReferenceValue I ON A.CANCR_REFNO = I.RFVAL_REFNO 
			LEFT OUTER JOIN lorenzo.dbo.Professionalcarer J ON A.PROCA_REFNO = J.PROCA_REFNO 
			LEFT OUTER JOIN lorenzo.dbo.ReferenceValue K ON A.ATTND_REFNO = K.RFVAL_REFNO 
			LEFT OUTER JOIN lorenzo.dbo.ReferenceValue L ON A.SCOCM_REFNO = L.RFVAL_REFNO 
			LEFT OUTER JOIN lorenzo.dbo.ReferenceValue M ON A.ADCAT_REFNO = M.RFVAL_REFNO 
			LEFT OUTER JOIN lorenzo.dbo.ReferenceValue N ON A.CANCB_REFNO = N.RFVAL_REFNO 
			LEFT OUTER JOIN lorenzo.dbo.ReferenceValue P ON A.CONTY_REFNO = P.RFVAL_REFNO 
			LEFT OUTER JOIN lorenzo.dbo.ReferenceValue R ON A.BKTYP_REFNO = R.RFVAL_REFNO 
			LEFT OUTER JOIN lorenzo.dbo.ReferenceValue S ON A.SCTYP_REFNO = S.RFVAL_REFNO 
			LEFT OUTER JOIN lorenzo.dbo.Referral T ON A.REFRL_REFNO = T.REFRL_REFNO 
			LEFT OUTER JOIN lorenzo.dbo.Specialty U ON T.REFTO_SPECT_REFNO = U.SPECT_REFNO
			LEFT OUTER JOIN lorenzo.dbo.ReferenceValue V ON A.LOTYP_REFNO = V.RFVAL_REFNO 
			LEFT OUTER JOIN lorenzo.dbo.ReferenceValue W ON A.CONTP_REFNO = W.RFVAL_REFNO 
			LEFT OUTER JOIN lorenzo.dbo.ReferenceValue X ON A.CSTAT_REFNO = X.RFVAL_REFNO 
			LEFT OUTER JOIN lorenzo.dbo.ServicePoint Y ON A.SPONT_REFNO = Y.SPONT_REFNO 
			LEFT OUTER JOIN lorenzo.dbo.ReferenceValue Z ON A.RESUT_REFNO = Z.RFVAL_REFNO 
			LEFT OUTER JOIN lorenzo.dbo.ReferenceValue AZ ON A.DNARS_REFNO = AZ.RFVAL_REFNO 
			LEFT OUTER JOIN lorenzo.dbo.Professionalcarer AY ON A.SEENBY_PROCA_REFNO = AY.PROCA_REFNO 
			LEFT OUTER JOIN 	
				(SELECT
					A.IDENTIFIER,A.PATNT_REFNO
					FROM lorenzo.dbo.PatientIdentifier A INNER JOIN 
						(
							SELECT
							A.PATNT_REFNO,
							max(a.patid_refno) as latest_patid
							FROM lorenzo.dbo.patientidentifier A INNER JOIN Lorenzo.dbo.ReferenceValue B ON A.PITYP_REFNO = B.RFVAL_REFNO
							WHERE A.ARCHV_FLAG = 'N' AND B.MAIN_CODE = 'FACIL' AND B.ARCHV_FLAG = 'N' AND A.HEORG_REFNO = '2002118'
							AND LEFT(A.IDENTIFIER,3) = 'RM2'
							group by a.patnt_refno) as t
							on t.latest_patid=A.PATID_REFNO) as vw on vw.PATNT_REFNO=A.PATNT_REFNO
			

		WHERE 	
			A.ARCHV_FLAG = 'N' AND A.SCTYP_REFNO = 1468
			and 
			(A.CREATE_DTTM>=@MaxCreateDate
			OR
			A.MODIF_DTTM >=@MaxModDate
			)

			) AS Import

			on Encounter.Cont_Ref=Import.Cont_Ref

			WHEN Matched then Update

			set
			Cont_Ref=Import.Cont_Ref,
			Ref_Ref=Import.Ref_Ref,
			WList_Ref=Import.WList_Ref,	
			Pat_Ref=Import.Pat_Ref,
			FACIL_ID=Import.FACIL_ID,
			NHS_number=Import.NHS_number,
			NHSNoStat=Import.NHSNoStat,
			Forename=Import.Forename,
			Surname=Import.Surname,
			DOB=Import.DOB,
			Gender=Import.Gender,
			AdministrativeCategory=Import.AdministrativeCategory,
			Cont_Date=Import.Cont_Date,
			Cont_Time=Import.Cont_Time,
			Spec_Code=Import.Spec_Code,
			Spec_Name=Import.Spec_Name,
			Prof_Carer=Import.Prof_Carer,
			Prof_Carer_Name=Import.Prof_Carer_Name,
			Purchaser_Code=Import.Purchaser_Code, 		
			Location=Import.Location,
			Location_Type=Import.Location_Type,
			Seen_By_Carer_Code=Import.Seen_By_Carer_Code,
			Seen_By_Carer_Surname=Import.Seen_By_Carer_Surname,
			Resource_Unit_Type=Import.Resource_Unit_Type, 
			Purpose=Import.Purpose,
			Visit_Type=Import.Visit_Type,	
			Urgency=Import.Urgency,
			Priority=Import.Priority,
			Contact_Duration=Import.Contact_Duration,
			Contact_Type=Import.Contact_Type,
			Booking_Type=Import.Booking_Type,
			Cancellation_Reason=Import.Cancellation_Reason,
			Cancelled_By=Import.Cancelled_By,
			Cancellation_Date=Import.Cancellation_Date,
			Arrival_Time=Import.Arrival_Time,
			Seen_Time=Import.Seen_Time,	
			Depart_Time=Import.Depart_Time,
			Attend_Status=Import.Attend_Status,
			DNA_Reason=Import.DNA_Reason,
			Planned_Number=Import.Planned_Number,
			Actual_Number=Import.Actual_Number,
			Outcome=Import.Outcome,
			Service_Point_Ref=Import.Service_Point_Ref,
			Service_Point_Code=Import.Service_Point_Code,
			CREATOR=Import.Creator,
			Create_Date=Import.Create_Date,
			MODIFIER=Import.Modifier,
			Modified_Date=Import.Modified_Date


			when not matched then Insert

			(Cont_Ref,
			Ref_Ref,
			WList_Ref,	
			Pat_Ref,
			FACIL_ID,
			NHS_number,
			NHSNoStat,
			Forename,
			Surname,
			DOB,
			Gender,
			AdministrativeCategory,
			Cont_Date,
			Cont_Time,
			Spec_Code,
			Spec_Name,
			Prof_Carer,
			Prof_Carer_Name,
			Purchaser_Code, 		
			Location,
			Location_Type,
			Seen_By_Carer_Code,
			Seen_By_Carer_Surname,
			Resource_Unit_Type, 
			Purpose,
			Visit_Type,	
			Urgency,
			Priority,
			Contact_Duration,
			Contact_Type,
			Booking_Type,
			Cancellation_Reason,
			Cancelled_By,
			Cancellation_Date,
			Arrival_Time,
			Seen_Time,	
			Depart_Time,
			Attend_Status,
			DNA_Reason,
			Planned_Number,
			Actual_Number,
			Outcome,
			Service_Point_Ref,
			Service_Point_Code,
			Creator,
			Create_Date,
			Modifier,
			Modified_Date)


		values
			(Import.Cont_Ref,
			Import.Ref_Ref,
			Import.WList_Ref,	
			Import.Pat_Ref,
			Import.FACIL_ID,
			Import.NHS_number,
			Import.NHSNoStat,
			Import.Forename,
			Import.Surname,
			Import.DOB,
			Import.Gender,
			Import.AdministrativeCategory,
			Import.Cont_Date,
			Import.Cont_Time,
			Import.Spec_Code,
			Import.Spec_Name,
			Import.Prof_Carer,
			Import.Prof_Carer_Name,
			Import.Purchaser_Code, 		
			Import.Location,
			Import.Location_Type,
			Import.Seen_By_Carer_Code,
			Import.Seen_By_Carer_Surname,
			Import.Resource_Unit_Type, 
			Import.Purpose,
			Import.Visit_Type,	
			Import.Urgency,
			Import.Priority,
			Import.Contact_Duration,
			Import.Contact_Type,
			Import.Booking_Type,
			Import.Cancellation_Reason,
			Import.Cancelled_By,
			Import.Cancellation_Date,
			Import.Arrival_Time,
			Import.Seen_Time,	
			Import.Depart_Time,
			Import.Attend_Status,
			Import.DNA_Reason,
			Import.Planned_Number,
			Import.Actual_Number,
			Import.Outcome,
			Import.Service_Point_Ref,
			Import.Service_Point_Code,
			Import.Creator,
			Import.Create_Date,
			Import.Modifier,
			Import.Modified_Date);

			
			
		UPDATE	TPY.CONTACTSDATASET
		SET Attend_Status = 'Attended'
		FROM TPY.CONTACTSDATASET A Inner Join Lorenzo.dbo.ScheduleEvent b
		On a.Cont_ref = b.schdl_refno
		WHERE b.START_DTTM is not null
		AND B.CANCR_DTTM is null
		AND B.ARRIVED_DTTM is not null
		AND B.SCOCM_REFNO <> 1457
		AND B.CONTY_REFNO in (2004176,2001538,2004177,2004179,9065,9064,2004178,89)


		UPDATE TPY.CONTACTSDATASET
		SET Attend_Status = 'DNA'
		FROM TPY.CONTACTSDATASET A Inner Join Lorenzo.dbo.ScheduleEvent b
		On a.Cont_ref = b.schdl_refno
		WHERE START_DTTM is not null
		AND CANCR_DTTM is null
		AND (SCOCM_REFNO = 1457
		OR ATTND_REFNO = 358)


		UPDATE TPY.CONTACTSDATASET
		SET Attend_Status = 'Cancelled'
		FROM TPY.CONTACTSDATASET A Inner Join Lorenzo.dbo.ScheduleEvent b
		On a.Cont_ref = b.schdl_refno
		WHERE  START_DTTM is not null
		AND (CANCR_DTTM is not null
		OR ATTND_REFNO in (2870,2871,2003532,2004300,2004301))


		UPDATE TPY.CONTACTSDATASET
		SET Attend_Status = 'Scheduled'
		FROM TPY.CONTACTSDATASET A Inner Join Lorenzo.dbo.ScheduleEvent b
		On a.Cont_ref = b.schdl_refno
		WHERE START_DTTM is not null
		AND CANCR_DTTM is null
		AND ARRIVED_DTTM is null
		AND SCOCM_REFNO <> 1457
		
		--Update parameter table
		update WH.dbo.Parameter set DateValue = GETDATE() where Parameter = 'BUILDCONTACTS'

	end