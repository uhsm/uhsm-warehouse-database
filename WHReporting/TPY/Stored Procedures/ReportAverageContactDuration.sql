﻿/******************************************************************************
 Description: Provides an average of contact time (minutes) by specialty and prof carer
 Parameters:  @Month
 
 Modification History --------------------------------------------------------

 Date     Author		Change
 19/08/14 Jubeda Begum  Created

******************************************************************************/
CREATE PROCEDURE [TPY].[ReportAverageContactDuration] 
@STARTDATE AS DATE
,@ENDDATE AS DATE

AS
BEGIN


SET NOCOUNT ON;

SELECT 
CASE WHEN Spec_Code IN ('652S SPEECH', '652', '652SVS', '652SW', '652V VOICE')
THEN 'SALT' ELSE Spec_Name
END AS Specialty
,[Prof_Carer_Name]
,TheMonth
,AVG(DATEDIFF(MINUTE,[Arrival_Time],[Depart_Time])) AS AVG_Contact_Time
FROM [WHREPORTING].[TPY].[ContactsDataset] CD
LEFT OUTER JOIN LK.Calendar CL
ON CAST(CD.Cont_Date AS DATE) = CL.TheDate
WHERE Spec_Code IN ('650', '652S SPEECH', '652', '652SVS', '652SW', '652V VOICE', '654', '651', '653')
AND CAST([Cont_Date] AS DATE) BETWEEN @StartDate AND @EndDate
GROUP BY
Spec_Code
,Spec_Name
,[Prof_Carer_Name]
,TheMonth
ORDER BY
TheMonth

END