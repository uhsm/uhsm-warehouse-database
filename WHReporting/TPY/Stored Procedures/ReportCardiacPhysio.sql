﻿CREATE procedure [TPY].[ReportCardiacPhysio]

    @Month as int
        
as


select op.Clinic,
op.ClinicCode,
cal.Themonth,
cal.FinancialMonthKey,
--,ArrivalTime,
--departedtime,
DATEDIFF(MINUTE,ArrivalTime,DepartedTime) as totaltimeminutes ,
case when DATEDIFF(MINUTE,ArrivalTime,DepartedTime) =75 then 'All Classes'
when DATEDIFF(MINUTE,ArrivalTime,DepartedTime) =45 then 'Talk'
when DATEDIFF(MINUTE,ArrivalTime,DepartedTime) =60 then 'Wednesday Class/Talk'
when DATEDIFF(MINUTE,ArrivalTime,DepartedTime) =120 then 'Class + Talk'
when DATEDIFF(MINUTE,ArrivalTime,DepartedTime) =105 then 'D/C + Class'
when DATEDIFF(MINUTE,ArrivalTime,DepartedTime) =150 then 'D/C + Class + Talk'
when DATEDIFF(MINUTE,ArrivalTime,DepartedTime) =95 then 'D/C + class (Wed)'
when DATEDIFF(MINUTE,ArrivalTime,DepartedTime) =125 then 'D/C + Class + talk (Wed)'
when DATEDIFF(MINUTE,ArrivalTime,DepartedTime) =65 then '1:1'
when DATEDIFF(MINUTE,ArrivalTime,DepartedTime) =90 then 'Assessment'
else 'Other' end as AttendanceType

,count(op.encounterrecno) as Attendances

from OP.Schedule op
inner join LK.Calendar cal
on cal.TheDate=cast(op.AppointmentDate as date)

where Cliniccode in ('TELECR','PHYFGCR','PHYFGCRNEW','PHYCRAH')

AND cal.FinancialMonthKey in (@Month)
AND Status ='Attend'


group by

op.Clinic,
op.ClinicCode,
cal.Themonth,
cal.FinancialMonthKey,
--,ArrivalTime,
--departedtime,
DATEDIFF(MINUTE,ArrivalTime,DepartedTime),
case when DATEDIFF(MINUTE,ArrivalTime,DepartedTime) =75 then 'All Classes'
when DATEDIFF(MINUTE,ArrivalTime,DepartedTime) =60 then 'Wednesday Class'
when DATEDIFF(MINUTE,ArrivalTime,DepartedTime) =120 then 'Class + Talk'
when DATEDIFF(MINUTE,ArrivalTime,DepartedTime) =105 then 'D/C + Class'
when DATEDIFF(MINUTE,ArrivalTime,DepartedTime) =150 then 'D/C + Class + Talk'
when DATEDIFF(MINUTE,ArrivalTime,DepartedTime) =95 then 'D/C + class (Wed)'
when DATEDIFF(MINUTE,ArrivalTime,DepartedTime) =125 then 'D/C + Class + talk (Wed)'
when DATEDIFF(MINUTE,ArrivalTime,DepartedTime) =65 then '1:1'
when DATEDIFF(MINUTE,ArrivalTime,DepartedTime) =90 then 'Assessment'
else 'Other' end

order by 
op.Clinic