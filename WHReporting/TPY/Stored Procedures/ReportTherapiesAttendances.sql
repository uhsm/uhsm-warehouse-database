﻿create procedure [TPY].[ReportTherapiesAttendances]

    @DateFrom datetime
     ,@DateTo datetime
     

     
   
as

select
c.ServicePoint, 
m.Metric,
Cal.TheMonth,
Cal.FinancialMonthKey,
spec.specialty,
sum(b.Cases) as cases




from 
wholap.dbo.FactOP b inner join 
wholap.dbo.OlapMetricOP m on b.MetricCode=m.MetricCode
inner join  

wholap.dbo.OlapPASServicePoint c on b.ClinicCode=c.ServicePointlocalCode
inner join wholap.dbo.OlapCalendar cal on cal.TheDate=b.EncounterDate
inner join wholap.dbo.OlapPASSpecialty spec on spec.SpecialtyCode=b.SpecialtyCode
where EncounterDate between @DateFrom and @DateTo
and b.SpecialtyCode in

(10000217,
10000218,
10000227,
15000055,
150000104,
2000887,
2000888,
2000893,
2000924,
2000964,
3101002,
2000886)







group by 
c.ServicePoint, 
m.Metric,
Cal.TheMonth,
Cal.FinancialMonthKey,
spec.specialty