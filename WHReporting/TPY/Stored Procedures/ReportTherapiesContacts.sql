﻿CREATE procedure [TPY].[ReportTherapiesContacts]

    @DateFrom datetime
     ,@DateTo datetime
     ,@Specialty varchar(100)

     
   
as

select 
--Spec_Code,
cal.FinancialMonthKey,
cal.TheMonth,
CASE WHEN Spec_Code IN ('652S','652','652SVS','652SW','652V VOICE','652S SPEECH') THEN 'SVS' Else Spec_Name end as Specialty,
Prof_Carer,
Prof_Carer_Name,
Location_Type,
location,
COUNT(cont_ref) as Contacts
from TPY.ContactsDataset cont
inner join LK.Calendar cal on cal.TheDate=cont.Cont_Date

where 

Cont_Date between @DateFrom and @DateTo

and spec_code in ('652S','652','652SVS','652SW','652V VOICE','652S SPEECH','650','651','653','654')
and CASE WHEN Spec_Code IN ('652S','652','652SVS','652SW','652V VOICE','652S SPEECH') THEN 'SVS' Else Spec_Name end in (@Specialty)
group by
--Spec_Code,
cal.FinancialMonthKey,
cal.TheMonth,
CASE WHEN Spec_Code IN ('652S','652','652SVS','652SW','652V VOICE','652S SPEECH') THEN 'SVS' Else Spec_Name end,
Prof_Carer,
Prof_Carer_Name,
location,
Location_Type

/*union all

select 
--Spec_Code,
'',
left(cal.TheMonth,4)+cast((cast(right(cal.TheMonth,4) as int)+1) as varchar),
CASE WHEN Spec_Code IN ('652S','652','652SVS','652SW','652V VOICE','652S SPEECH') THEN 'SVS' Else Spec_Name end as Specialty,
'Previous Year',
'',
'',
'',
COUNT(cont_ref) as Contacts
from TPY.ContactsDataset cont
inner join LK.Calendar cal on cal.TheDate=cont.Cont_Date
where 

Cont_Date between dateadd(year,-1,@DateFrom) and dateadd(year,-1,@DateTo)
and spec_code in ('652S','652','652SVS','652SW','652V VOICE','652S SPEECH','650','651','653','654')
and CASE WHEN Spec_Code IN ('652S','652','652SVS','652SW','652V VOICE','652S SPEECH') THEN 'SVS' Else Spec_Name end in (@Specialty)
group by
--Spec_Code,
--cal.FinancialMonthKey,
cal.TheMonth,
CASE WHEN Spec_Code IN ('652S','652','652SVS','652SW','652V VOICE','652S SPEECH') THEN 'SVS' Else Spec_Name end
--Prof_Carer,
--Prof_Carer_Name

--ORDER BY CASE WHEN Spec_Code IN ('652S','652','652SVS','652SW','652V VOICE','652S SPEECH') THEN 'SVS' Else Spec_Name end,Prof_Carer_Name
*/