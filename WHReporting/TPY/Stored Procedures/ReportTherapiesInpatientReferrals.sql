﻿CREATE procedure [TPY].[ReportTherapiesInpatientReferrals]

  
@DateFrom as datetime,
@DateTo as Datetime,
@TherapiesSpecialty as VARCHAR(100),
@ProfCarer as varchar (25)

as

select 
cont.Cont_Ref
,cont.Prof_Carer_Name
,cont.Spec_Name
,cont.Spec_Code
,isnull(admission.[Specialty(Function)],'Unknown') as SpecialtyFunction
,isnull(admission.ConsultantName,'Unknown') as ConsultantName


from TPY.CONTACTS_DATASET cont

left outer join 


(select ref.ReferralSourceUniqueID,
APC.endward,
apc.ConsultantCode,
apc.ConsultantName,
apc.Division,
apc.Directorate,
apc.[SpecialtyCode(Function)],
apc.[Specialty(Function)],
apc.SpecialtyCode,
apc.Specialty,
apc.AdmissionType,
apc.AdmissionDate,
apc.DischargeDate
 
from RF.Referral ref
left outer join APC.Episode apc on apc.FacilityID=ref.FacilityID
and ref.Referralcreated between apc.EpisodeStartDateTime and EpisodeEndDateTime

where cast(ReferralCreated as DATE) between @DateFrom and @DateTo)

as admission on admission.ReferralSourceUniqueID=cont.Ref_Ref

where 

cont.Cont_Ref in 

(select MIN(cont_ref)
from TPY.CONTACTS_DATASET
where Attend_Status='Attended'

and Cont_Date>@DateFrom
and Spec_Code in (@TherapiesSpecialty)
and Prof_Carer in (@ProfCarer)
group by Ref_Ref)