﻿CREATE procedure [TPY].[ReportTherapiesPhysioContacts]

    @DateFrom datetime
     ,@DateTo datetime
     
    
as
select 
cal.TheMonth as MonthCont,
cal.FinancialMonthKey,
spec.Division,
spec.Direcorate,
ref.ReferrerSpecialty,
isnull(ref.ReferrerSpecialtyCode,'zzz') as ReferrerSpecialtyCode,
COUNT(distinct cont_ref) as contacts
from TPY.CONTACTSDATASET cont

left join RF.Referral ref on cont.Ref_Ref=ref.ReferralSourceUniqueID
left join lk.SpecialtyDivision spec on spec.SpecialtyCode=ref.ReferrerSpecialtyCode
left join LK.Calendar cal on cal.TheDate=cont.Cont_Date

where Spec_Code='650' and Cont_Date between @DateFrom and @DateTo

group by
cal.TheMonth,
cal.FinancialMonthKey,
spec.Division,spec.Direcorate,ref.ReferrerSpecialty,isnull(ref.ReferrerSpecialtyCode,'zzz')

order by spec.Division,spec.Direcorate,ref.ReferrerSpecialty,isnull(ref.ReferrerSpecialtyCode,'zzz')