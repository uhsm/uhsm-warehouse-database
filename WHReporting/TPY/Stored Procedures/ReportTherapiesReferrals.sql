﻿CREATE procedure [TPY].[ReportTherapiesReferrals]
  @DateFrom datetime
 ,@DateTo datetime
 ,@ProfCarer varchar (80)
   
as  



select

cal.themonth,
ReceivingProfCarerCode,
ReceivingProfCarerName,
ReceivingProfCarerType,
ReceivingSpecialty,
[ReceivingSpecialty(Function)],
FinancialMonthKey,
ReferralSource,
ReferralMedium,
ReferralType,
ReferrerName,
ReferrerOrganisationName,
ReferrerSpecialty,
count(ReferralSourceUniqueID) as CountReferrals



from RF.Referral ref

inner join LK.Calendar cal on cal.TheDate=ref.ReferralReceivedDate

where [ReceivingSpecialty(Function)] = 'Physiotherapy'
and ReferralReceivedDate between @DateFrom and @DateTo

and ReceivingProfCarerCode in (@ProfCarer)

group by

cal.themonth,
ReceivingProfCarerCode,
ReceivingProfCarerName,
ReceivingProfCarerType,
ReceivingSpecialty,
[ReceivingSpecialty(Function)]
,FinancialMonthKey,
ReferralSource,
ReferralMedium,
ReferralType,
ReferrerName,
ReferrerOrganisationName,
ReferrerSpecialty