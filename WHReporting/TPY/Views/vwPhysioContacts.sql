﻿Create view [TPY].[vwPhysioContacts] as

select 
contact_type, 
cont_date, 
cont_ref, 
con.facil_id, 
con.location_type,
con.location, 
con.attend_status, 
ref.ReferralReceivedDate,
ref.ReferralSourceUniqueID

from tpy.CONTACTSDATASET con
left join RF.Referral ref
	on con.ref_ref = ref.ReferralSourceUniqueID

where	con.cont_date BETWEEN CONVERT(DATETIME,'01/04/2012 00:00:00.000',103) 
					AND CONVERT(DATETIME,'31/03/2013 00:00:00.000',103)
and 
		spec_name = 'Physiotherapy'
and 
		con.cancellation_date is null