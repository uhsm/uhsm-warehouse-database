﻿CREATE View [TPY].[vwTherapiesContacts] as 
select
cont.Spec_Name,
cont.Spec_Code,
cont.Contact_Type,
coalesce(case when rtrim(ltrim(cont.Location)) like 'CLINICAL SUPPORT%' Then 'Clinical Support'
when rtrim(ltrim(cont.Location)) like 'F16%' Then 'F16'
when rtrim(ltrim(cont.Location)) like 'A1%' Then 'A1'
when rtrim(ltrim(cont.Location)) like '%CT OBST P/NATAL%' Then 'C2 OBST P/NATAL'
when rtrim(ltrim(cont.Location)) like '%CT COTS%' Then 'C2 COTS'
else rtrim(ltrim(cont.Location)) end,'Unknown')






as Location,
cont.Location_Type,
cal.FinancialMonthKey
,case when left(cal.TheMonth,3) ='Apr' then 1
when left(cal.TheMonth,3) ='May' then 2
when left(cal.TheMonth,3) ='Jun' then 3
when left(cal.TheMonth,3) ='Jul' then 4
when left(cal.TheMonth,3) ='Aug' then 5
when left(cal.TheMonth,3) ='Sep' then 6
when left(cal.TheMonth,3) ='Oct' then 7
when left(cal.TheMonth,3) ='Nov' then 8
when left(cal.TheMonth,3) ='Dec' then 9
when left(cal.TheMonth,3) ='Jan' then 10
when left(cal.TheMonth,3) ='Feb' then 11
when left(cal.TheMonth,3) ='Mar' then 12 end
as TheMonthOrder,

cal.financialyear,
left(cal.TheMonth,3) as MonthCont,
coalesce(spec.Division,'Unknown') as Division,
coalesce(spec.Direcorate,'Unknown') as Directorate,
coalesce(ref.ReferrerSpecialty,'Unknown') as ReferrerSpecialty,
coalesce(ref.ReferrerSpecialtyCode,'Unk') as ReferrerSpecialtyCode,
COUNT(distinct cont_ref) as contacts
,Prof_Carer_Name
from TPY.CONTACTSDATASET cont

inner join RF.Referral ref on cont.Ref_Ref=ref.ReferralSourceUniqueID
left join lk.SpecialtyDivision spec on spec.SpecialtyCode=ref.ReferrerSpecialtyCode
left join LK.Calendar cal on cal.TheDate=cast(Cont_Date as date)

where Spec_Code IN (
'9014',
'652S SPEECH',
'652SW',
'652V VOICE',
'651',
'652',
'652SVS',
'654',
'650')







 and Cont_Date >'2011-03-31'

group by
cont.Spec_Name,
cont.Spec_Code,
cont.Contact_Type,
cont.Location,
cont.Location_Type,
cal.FinancialMonthKey,
case when left(cal.TheMonth,3) ='Apr' then 1
when left(cal.TheMonth,3) ='May' then 2
when left(cal.TheMonth,3) ='Jun' then 3
when left(cal.TheMonth,3) ='Jul' then 4
when left(cal.TheMonth,3) ='Aug' then 5
when left(cal.TheMonth,3) ='Sep' then 6
when left(cal.TheMonth,3) ='Oct' then 7
when left(cal.TheMonth,3) ='Nov' then 8
when left(cal.TheMonth,3) ='Dec' then 9
when left(cal.TheMonth,3) ='Jan' then 10
when left(cal.TheMonth,3) ='Feb' then 11
when left(cal.TheMonth,3) ='Mar' then 12 end,
cal.financialyear,
left(cal.TheMonth,3),
coalesce(spec.Division,'Unknown'),
coalesce(spec.Direcorate,'Unknown'),
coalesce(ref.ReferrerSpecialty,'Unknown'),
coalesce(ref.ReferrerSpecialtyCode,'Unk')
,Prof_Carer_Name