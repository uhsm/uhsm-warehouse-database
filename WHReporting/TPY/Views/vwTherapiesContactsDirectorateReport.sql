﻿CREATE View [TPY].[vwTherapiesContactsDirectorateReport] as 
SELECT 
'CurrentYear' as Indicator,
*

FROM [TPY].[vwTherapiesContacts]

WHERE financialyear in (select financialyear from LK.Calendar c where TheDate=cast(getdate() as date))
and FinancialMonthKey<(select FinancialMonthKey from LK.Calendar where TheDate=cast(GETDATE() as date))
union

SELECT 
'PreviousYear' as Indicator,
*

FROM [TPY].[vwTherapiesContacts]

WHERE 
/*and cast(((cast(left(cast(financialmonthkey as varchar),4) as numeric)+1)) as varchar)+

substring(cast(financialmonthkey as varchar),5,2)*/


cast(cast(left(financialyear,4) as numeric)+1 as varchar)
+'/'+

cast(cast(substring(financialyear,6,4) as numeric)+1 as varchar)



 IN (select financialyear from LK.Calendar c where TheDate=cast(getdate() as date))