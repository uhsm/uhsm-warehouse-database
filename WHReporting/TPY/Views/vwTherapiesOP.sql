﻿CREATE View [TPY].[vwTherapiesOP] as 
Select cliniccode,
financialyear,
[Status],
TheMonth,
financialmonthkey,
ReferringSource,
AppointmentType,
SpecialtyDesc,
TheMonthOrder,
ProfessionalCarerName,
count(CountRecord) as CountRecord

from 


(SELECT DISTINCT
cliniccode,
--ref.ReferrerCode,
--ref.ReferrerSpecialty,
--ref.ReferralSourceUniqueID,
c.financialyear,
[Status]
,left(c.TheMonth,3) as TheMonth

,case when left(c.TheMonth,3) ='Apr' then 1
when left(c.TheMonth,3) ='May' then 2
when left(c.TheMonth,3) ='Jun' then 3
when left(c.TheMonth,3) ='Jul' then 4
when left(c.TheMonth,3) ='Aug' then 5
when left(c.TheMonth,3) ='Sep' then 6
when left(c.TheMonth,3) ='Oct' then 7
when left(c.TheMonth,3) ='Nov' then 8
when left(c.TheMonth,3) ='Dec' then 9
when left(c.TheMonth,3) ='Jan' then 10
when left(c.TheMonth,3) ='Feb' then 11
when left(c.TheMonth,3) ='Mar' then 12 end
as TheMonthOrder




,c.financialmonthkey,
--ref.ReferralSource,
coalesce(case when referrercode like ('G%') THEN 'GP' ELSE referrerspecialty END,sdiv.SpecialtyFunction,ReferralSource,'Unknown') as ReferringSource,
AppointmentType,
case when LEFT(coalesce(map.newcliniccode,op.cliniccode),3)='MSK' and ClinicCode like '%AS%' Then 'MSK - Assessment'
when LEFT(coalesce(map.newcliniccode,op.cliniccode),3)='MSK' then 'MSK - Treatment'
when [SpecialtyCode(Function)] ='653' THEN 'Podiatry'
when ClinicCode like 'Hand%' then 'Hand Therapy'
when [SpecialtyCode(Function)]='651' then 'Occupational Therapy'
when ClinicCode in ('TELECR','PHYFGCR','PHYFGCRNEW','PHYCRAH') Then 'Physiotherapy - Cardiac Rehab'
else [Specialty(Function)] end as SpecialtyDesc

,
ProfessionalCarerName,


op.EncounterRecno as CountRecord



 FROM OP.Schedule op
 inner join RF.Referral ref on ref.ReferralSourceUniqueID=op.ReferralSourceUniqueID
inner join LK.Calendar c on c.TheDate=op.AppointmentDate


left join 


(select distinct consultantcode,SpecialtyFunctionCode
from
LK.ConsultantSpecialty 

where ConsultantCode+cast(EarliestActivity as varchar)in(select ConsultantCode+cast(MAX(EarliestActivity) as varchar) 
from LK.ConsultantSpecialty
group by consultantcode)
) as cons

on cons.ConsultantCode=ref.ReferrerCode


left join TPY.PhysioClinicMap map on op.ClinicCode=map.OldClinicCode

left join LK.SpecialtyDivision sdiv on sdiv.SpecialtyCode=


coalesce(

ref.referrerspecialtycode,

case when cons.consultantcode='C2699600' then '410' 
when cons.consultantcode='C2500447' then '410'
when cons.consultantcode='C2699600' then '410'
when cons.consultantcode='C3197398' then '140'
when cons.consultantcode='C4092425' then '101'
when cons.consultantcode='C4654207' then '340'
when cons.consultantcode='C3549146' then '100'
when cons.consultantcode='C3367960' then '361'
when cons.consultantcode='C1233207' then '190'
when cons.consultantcode='C2678650' then '314'
when cons.consultantcode='C1500484' then '330'
when cons.consultantcode='C4054302' then '810'
when cons.consultantcode='C3176692' then '314'
when cons.consultantcode='C3497287' then '190'


else 

cons.SpecialtyFunctionCode end)


WHERE  [SpecialtyCode(Function)] in ('650','653','651','652','654','9046')

AND 
[Status] IN ('ATTEND','DNA')

AND AppointmentDate > '2011-03-31' 
group by 
cliniccode,
--ref.ReferralSource,
--ref.ReferrerSpecialty,
--ref.ReferrerCode,
--ref.ReferralSourceUniqueID,
ProfessionalCarerName,
coalesce(case when referrercode like ('G%') THEN 'GP' ELSE referrerspecialty END,sdiv.SpecialtyFunction,ReferralSource,'Unknown'),
c.financialyear,
[Status],
left(c.TheMonth,3),
c.financialmonthkey
,sdiv.SpecialtyFunction,
case when LEFT(coalesce(map.newcliniccode,op.cliniccode),3)='MSK' and ClinicCode like '%AS%' Then 'MSK - Assessment'
when LEFT(coalesce(map.newcliniccode,op.cliniccode),3)='MSK' then 'MSK - Treatment'
when [SpecialtyCode(Function)] ='653' THEN 'Podiatry'
when ClinicCode like 'Hand%' then 'Hand Therapy'
when [SpecialtyCode(Function)]='651' then 'Occupational Therapy'
when ClinicCode in ('TELECR','PHYFGCR','PHYFGCRNEW','PHYCRAH') Then 'Physiotherapy - Cardiac Rehab'
else [Specialty(Function)] end,
AppointmentType
,case when left(c.TheMonth,3) ='Apr' then 1
when left(c.TheMonth,3) ='May' then 2
when left(c.TheMonth,3) ='Jun' then 3
when left(c.TheMonth,3) ='Jul' then 4
when left(c.TheMonth,3) ='Aug' then 5
when left(c.TheMonth,3) ='Sep' then 6
when left(c.TheMonth,3) ='Oct' then 7
when left(c.TheMonth,3) ='Nov' then 8
when left(c.TheMonth,3) ='Dec' then 9
when left(c.TheMonth,3) ='Jan' then 10
when left(c.TheMonth,3) ='Feb' then 11
when left(c.TheMonth,3) ='Mar' then 12 end

,op.EncounterRecno) AS T


group by
cliniccode,
financialyear,
[Status],
TheMonth,
financialmonthkey,
ReferringSource,
AppointmentType,
SpecialtyDesc,
ProfessionalCarerName,
TheMonthOrder