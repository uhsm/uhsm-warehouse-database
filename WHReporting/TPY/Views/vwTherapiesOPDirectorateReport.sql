﻿CREATE View [TPY].[vwTherapiesOPDirectorateReport] as 
SELECT 
'CurrentYear' as Indicator,
case when referringsource='GP' then 'GP' else 'Other' end as ReferralType,
*

FROM [TPY].[vwTherapiesOP]

WHERE  financialyear = (select financialyear from LK.Calendar where TheDate=cast(GETDATE() as date))
and financialmonthkey<(select financialmonthkey from LK.Calendar where TheDate=cast(GETDATE() as date))






union

SELECT 
'PreviousYear' as Indicator,
case when referringsource='GP' then 'GP' else 'Other' end as ReferralType,
*

FROM [TPY].[vwTherapiesOP]

WHERE 
/*and cast(((cast(left(cast(financialmonthkey as varchar),4) as numeric)+1)) as varchar)+

substring(cast(financialmonthkey as varchar),5,2)*/


FinancialYear = 


(select financialyear from LK.Calendar where TheDate=dateadd(year,-1,cast(GETDATE() as date))
)