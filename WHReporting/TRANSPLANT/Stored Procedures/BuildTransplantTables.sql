﻿-- =============================================
-- Author:		CMan
-- Create date: 28/01/2015
-- Description: Stored Procedure to create the Transplant.Assessments and the Transplant.Transplanted_Patients tables from the Heart and Lung Transplant Extract from Sharepoint
--				Tables to be used to generate the Transplants for consideration for PbR and the Transplants return				
-- =============================================
CREATE PROCEDURE [TRANSPLANT].[BuildTransplantTables]
	-- Add the parameters for the stored procedure here




AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;


TRUNCATE TABLE WHReporting.TRANSPLANT.TransplantAssessments
INSERT INTO WHReporting.TRANSPLANT.TransplantAssessments
SELECT DISTINCT HLT.Category
	,HLT.RM2
	,HLT.Forename
	,HLT.Surname
	,EP.SourceSpellNo
	,EP.AdmissionDateTime
	,EP.DischargeDateTime
	,EP.ResponsibleCommissionerCCGOnlyCode
	,EP.EpisodicPracticeCode
	,EP.EpisodeGPCode
	--,EP.PrimaryDiagnosisCode
	--,EP.PrimaryProcedureCode--Query with CB how significant it is to not have the Prim Diagnosis or Primary Procedure Code included in the final assessments table 
	,SP.AdmissionSpecialtyCode
	,SP.AdmissionMethod
	,SP.PatientClass
	,DATEDIFF(DAY, EP.AdmissionDateTime, EP.DischargeDateTime) AS LOS
	,HLT.[Assessment Capped Bed Days]
	,HLT.[Assessment Critical Care Bed Days]
	,HLT.[Transplant Type]
	,HLT.Created
FROM UHSMApplications.SP.HeartandLungTransplantExtract HLT
LEFT OUTER JOIN WHREPORTING.APC.Episode EP ON HLT.RM2 = EP.FacilityID
	AND HLT.[Date of Assessment/Transplant] >= EP.EpisodeStartDate
	AND (
		HLT.[Date of Assessment/Transplant] <= EP.EpisodeEndDate
		OR EP.EpisodeEndDate IS NULL
		)
LEFT OUTER JOIN WHREPORTING.APC.Spell SP ON EP.SourceSpellNo = SP.SourceSpellNo
WHERE Category = 'Assessment'



--Create temp table to get all Transplant records with the corresponding ITU ward stays.
--Only Wards stays in ITU which crosses over the Transplant date and post Tranplant are included in the ITU days count
IF OBJECT_ID('tempdb..#Transplant') > 0
	DROP TABLE #Transplant

SELECT HLT.RM2
	,HLT.Surname
	,HLT.Forename
	,HLT.[Date of Birth]
	,HLT.[Transplant Consultant]
	,HLT.[Transplant Type]
	,HLT.[Date of Assessment/Transplant]
	,SP.SourceSpellNo
	,SP.DischargeDateTime
	,SP.ResponsibleCommissionerCCGOnlyCode
	,pat.DateOfDeath
	--, ws.WardCode
	,CASE 
		WHEN HLT.[Date of Assessment/Transplant] >= dateadd(day, datediff(day, 0, ws.StartTime), 0)
			AND HLT.[Date of Assessment/Transplant] < ws.EndTime
			THEN HLT.[Date of Assessment/Transplant]
		ELSE ws.StartTime
		END AS AdjustedStart--create and adjusted start date to be used when calculating the ITU days in the final query below. If the Ward Stay start date is before the Transplant Date change the adjusted start to the Transaplant date 
	,ws.StartTime
	,ws.EndTime
	,CCG.HACode
	,HA.[Organisation Name] HAName
INTO #Transplant
FROM UHSMApplications.SP.HeartandLungTransplantExtract HLT
LEFT OUTER JOIN WHREPORTING.APC.Spell SP ON HLT.RM2 = SP.FaciltyID
	AND HLT.[Date of Assessment/Transplant] >= SP.AdmissionDate
	AND (
		HLT.[Date of Assessment/Transplant] <= SP.DischargeDateTime
		OR SP.DischargeDateTime IS NULL
		)
LEFT OUTER JOIN WHReporting.LK.PatientCurrentDetails pat ON HLT.RM2 = Pat.FacilityID
LEFT OUTER JOIN (
	SELECT *
	FROM WHREPORTING.APC.WardStay
	WHERE WardCode IN (
			'ICA'
			,'ICU'
			,'HDU'
			,'CTCU'
			,'CT Transplant'
			,'CTTRAN'--01/06/2016 CM Added as per new ward changes for JAC
			)
	) ws ON SP.SourceSpellNo = ws.SourceSpellNo
	AND (
		(
			HLT.[Date of Assessment/Transplant] >= dateadd(day, datediff(day, 0, ws.StartTime), 0)
			AND HLT.[Date of Assessment/Transplant] <= ws.EndTime
			)
		OR (
			HLT.[Date of Assessment/Transplant] >= dateadd(day, datediff(day, 0, ws.StartTime), 0)
			AND HLT.[Date of Assessment/Transplant] <= ws.EndTime
			)
		)--only want to include wardstays which cross over the Transplant date and after the transplant date in the count of ITU Bed days as per NS email from 27/01/2015
LEFT OUTER JOIN OrganisationCCG.dbo.CCG ON SP.ResponsibleCommissionerCCGOnlyCode = CCG.OrganisationCode
LEFT OUTER JOIN OrganisationCCG.dbo.[Health Authority] HA ON CCG.HACode = HA.[Organisation Code]
--LEFT JOIN select * from [OrganisationCCG].[dbo].[General Medical Practice] GMP
--ON GMP.[Organisation Code] = Sp.PracticeCode
WHERE Category = 'Transplant'






--Insert into Final Transplant table 
--Only calculating the ITU days from the transplant date. Also adding the additonal one night to the sum of the total ITU ward stay days if there are multiple ITU stays in the spell
TRUNCATE TABLE WHReporting.TRANSPLANT.TransplantedPatients

INSERT INTO WHReporting.TRANSPLANT.TransplantedPatients
SELECT #Transplant.RM2
	,#Transplant.Surname
	,#Transplant.Forename
	,#Transplant.[Date of Birth]
	,#Transplant.[Transplant Consultant]
	,#Transplant.[Transplant Type]
	,#Transplant.[Date of Assessment/Transplant]
	,#Transplant.SourceSpellNo
	,#Transplant.DischargeDateTime
	,#Transplant.ResponsibleCommissionerCCGOnlyCode
	,#Transplant.DateOfDeath
	,sum(datediff(day, #Transplant.AdjustedStart, #Transplant.EndTime)) + 1 AS ITUDays --calculating the sum of ITU days using the adjusted start field created in the temp table above as we only want to count start the ITU Bed Days from the Day the day they were transplanted  as per NS email 27/01/2015.
	--also only adding the 1 additional bed day to the overall sum of ITU bed days rather than an additional one to each ITU bed day
	,MAX(EndTime) AS FinalITUEnd -- this is the end date of the last ITU stay
	,#Transplant.HACode
	,#Transplant.HAName
FROM #Transplant
GROUP BY #Transplant.RM2
	,#Transplant.Surname
	,#Transplant.Forename
	,#Transplant.[Date of Birth]
	,#Transplant.[Transplant Consultant]
	,#Transplant.[Transplant Type]
	,#Transplant.[Date of Assessment/Transplant]
	,#Transplant.SourceSpellNo
	,#Transplant.DischargeDateTime
	,#Transplant.ResponsibleCommissionerCCGOnlyCode
	,#Transplant.DateOfDeath
	,#Transplant.HACode
	,#Transplant.HAName





END