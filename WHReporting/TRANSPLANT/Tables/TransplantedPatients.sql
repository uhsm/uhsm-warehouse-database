﻿CREATE TABLE [TRANSPLANT].[TransplantedPatients](
	[RM2] [nvarchar](255) NULL,
	[Surname] [nvarchar](255) NULL,
	[Forename] [nvarchar](255) NULL,
	[Date of Birth] [datetime] NULL,
	[Transplant Consultant] [nvarchar](255) NULL,
	[Transplant Type] [nvarchar](255) NULL,
	[Date of Assessment/Transplant] [datetime] NULL,
	[SourceSpellNo] [int] NULL,
	[DischargeDateTime] [datetime] NULL,
	[ResponsibleCommissionerCCGOnlyCode] [varchar](5) NULL,
	[DateOfDeath] [datetime] NULL,
	[ITUDays] [int] NULL,
	[FinalITUEnd] [smalldatetime] NULL,
	[HACode] [nvarchar](3) NULL,
	[HAName] [nvarchar](100) NULL
) ON [PRIMARY]