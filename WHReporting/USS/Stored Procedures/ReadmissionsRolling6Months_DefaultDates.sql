﻿/*
--Author: N Scattergood
--Date created: 3/7/2012
--Stored Procedure Built for pulling out details of rolling 6 months with 30 Day Lag 
--Especially for use with Readmission Reports
--Currently used for:
> SRSS Report on 30 Day Emergency Readmissions By Discharge Ward
*/

Create Procedure [USS].[ReadmissionsRolling6Months_DefaultDates]
as

 
 select 
 distinct
  
 min(Cal.TheDate) as FirstDayOfRolling6Month
 ,max(Cal.TheDate) LastDayofRolling6Month
 ,dbo.GetEndOfDay (max(Cal.TheDate)) LastDateTimeofRolling6Month
 --,Cal.TheMonth as Rolling13Month
 --,Cal.FinancialYear as FYLastMonth

 FROM LK.Calendar Cal
 
 where
Cal.TheMonth in 
(select distinct
SQ.TheMonth
  FROM LK.Calendar SQ
  where SQ.TheDate between 
 (
dateadd(Month,-6,(dateadd(day,-30,(cast(GETDATE()as DATE)))))
)
and
(
dateadd(Month,-1,(dateadd(day,-30,(cast(GETDATE()as DATE)))))
)

)