﻿/*
--Author: N Scattergood
--Date created: 02/05/2013
--Stored Procedure Built for the Excel Report on Enhanced Recovery
--#NOF
*/

CREATE Procedure [USS].[ReportEnhancedRecoveryDashboard_Excel]

as
select 
Distinct
Dis.TheMonth as DischargeMonthYear
,datename(mm,S.[DischargeDateTime] )  as DischargeMonth
,Dis.TheMonth
,Dis.FinancialYear as DischargeYear
,Dis.[DayOfWeek] as DayOfDischarge
,Adm.FinancialYear as AdmissionYear
,S.FaciltyID
,S.SourceSpellNo
--,P.PatientForename + ' ' + P.PatientSurname as PatientName
,       case when S.AgeCode = 'Age Unknown'
		then 'Age Unknown'
		when RIGHT	(S.AgeCode,1) = '+'
		then '80+'
		when RIGHT	(S.AgeCode,5) <> 'Years'
		then '< 40'
		when LEFT	(S.AgeCode,2) < '40'
		then '< 40'
		when LEFT	(S.AgeCode,2) < '50'
		then '40-49'
		when LEFT	(S.AgeCode,2) < '60'
		then '50-59'
		when LEFT	(S.AgeCode,2) < '70'
		then '60-69'
		when LEFT	(S.AgeCode,2) < '80'
		then '70-79'
		when LEFT	(S.AgeCode,2) >= '80'
		then '80+'
		else 'Check SQL'
		end as AgeGroup		
,S.AdmissionConsultantName
,(CONVERT(varchar(10),S.AdmissionDate,112)+'-'+CONVERT(varchar(10),S.DischargeDate ,112)+'-'+cast(S.SourceSpellNo as Varchar(9))) as LosString 
,S.AdmissionDateTime
,Cc.PrimaryProcedureDate
,A5.FirstStayOnA5
,A5.LastStayOnA5
,Rehab.FirstRehabProc
,S.DischargeDateTime
      ,S.[LengthOfSpell] 

,DATEDIFF(dd, S.AdmissionDateTime, cc.PrimaryProcedureDate  ) as [PreOpLOS]
,DATEDIFF(dd, cc.PrimaryProcedureDate , S.DischargeDateTime  ) as [PostOpLOS]
,DATEDIFF(dd, S.AdmissionDateTime, A5.LastStayOnA5 ) as [AdmitToA5Discharge]
,CASE WHEN cc.PrimaryProcedureDate IS NULL 
			THEN 'EXCLUDE'
			WHEN DATEDIFF(dd, S.AdmissionDateTime, cc.PrimaryProcedureDate  ) < '0'
			THEN 'EXCLUDE'
			WHEN DATEDIFF(dd, cc.PrimaryProcedureDate , S.DischargeDateTime  ) < '0'
			THEN 'EXCLUDE'
			ELSE 'INCLUDE' end AS [PreOpExclusion]
			
 ,CASE		when ORMIS.ORMISOperationStartDate is null 
			then 'Not Applicable'
			WHEN CONVERT(datetime,S.AdmissionDate,103)  > ORMIS.ORMISOperationStartDate
			then 'Not Applicable'
			WHEN DATEDIFF(dd, ORMIS.ORMISOperationStartDate , S.DischargeDateTime  ) < '0'
			THEN 'Not Applicable'
				WHEN datediff(MINUTE,S.AdmissionDateTime,ORMIS.ORMISOperationStartDate) between 0 and 1440
				THEN 'Operated within 24 Hours'
				WHEN datediff(MINUTE,S.AdmissionDateTime,ORMIS.ORMISOperationStartDate) between 1441 and 2160
				THEN 'Operated within 36 Hours'
				WHEN datediff(MINUTE,S.AdmissionDateTime,ORMIS.ORMISOperationStartDate)> 2160
				THEN 'Operated after 36 Hours'  
				else 'Check SQL' end as 'TimeToOperation'
		

		,ORMIS.ORMISInSuiteTime
      ,ORMIS.ORMISInAnaestheticTime
      ,ORMIS.ORMISAnaestheticInductionTime
      ,ORMIS.ORMISOperationStartDate
      ,ORMIS.ORMISOperationEndDate
      ,ORMIS.ORMISInRecoveryTime
		
		,case when S.[AdmissionType] in ('Emergency','Non-Elective') then 'Non-Elective'
		else S.[AdmissionType] end as 'AdmissionMethod' 
		
		 --     ,case when S.[InpatientStayCode] = 'D'	then 'DayCase'
			--when  S.[InpatientStayCode] = 'I'	then 'Inpatient'
			--when  S.[InpatientStayCode] = 'B'	then 'Baby'
			--else [InpatientStayCode] end as InpatientStay 				

,S.[AdmissionSpecialty(Function)]
,S.[AdmissionSpecialtyCode(Function)]
,S.AdmissionMethod
,E.[Specialty(Function)]	AS 'DischargeSpecialty(Function)'
,E.[SpecialtyCode(Function)] AS 'DischargeSpecialtyCode(Function)'
,S.DischargeMethod	
,S.DischargeDestination		

     ,RR.ReadmissionDate
     --,RR.ReadmissionIntervalDays
      --,RR.ReadmissionDate - S.[DischargeDateTime]
      ,case 
      when 
      datediff(minute,S.[DischargeDateTime],RR.ReadmissionDate) < 4320 
      then 'within 72 Hours'
      when RR.ReadmissionIntervalDays <= '30'
      then 'within 30 Days'
	  else 'NULL'
	  end as Readmission
		
		
			  ,cc.[PriamryProcedure]
      ,cc.[SecondaryProcedure1]
      ,cc.[SecondaryProcedure2]
      --,cc.[SecondaryProcedure3]
      --,cc.[SecondaryProcedure4]
         ,cc.[PrimaryDiagnosis]
      ,cc.[SubsidiaryDiagnosis]
      ,cc.[SecondaryDiagnosis1]
      ,cc.[SecondaryDiagnosis2]
      ,cc.[SecondaryDiagnosis3]
      ,cc.[SecondaryDiagnosis4]
      ,cc.[SecondaryDiagnosis5]    
      ,cc.[SecondaryDiagnosis6]     
      ,cc.[SecondaryDiagnosis7]  
      ,cc.[SecondaryDiagnosis8]    
      ,cc.[SecondaryDiagnosis9]      
      ,cc.[SecondaryDiagnosis10]
      ,cc.[SecondaryDiagnosis11]
      ,cc.[SecondaryDiagnosis12]
      ,cc.[SecondaryDiagnosis13]
      ,cc.[SecondaryDiagnosis14]
      ,cc.[SecondaryDiagnosis15]
      ,cc.[SecondaryDiagnosis16]
      ,cc.[SecondaryDiagnosis17]
      ,cc.[SecondaryDiagnosis18]
      ,cc.[SecondaryDiagnosis19]
      ,cc.[SecondaryDiagnosis20]


      

FROM [APC].[Spell] S
  
	left join [APC].[ClinicalCoding] cc
		on  s.EncounterRecno = cc.EncounterRecno
		
		left join [APC].[Episode] E
		on E.SourceSpellNo = S.SourceSpellNo
		and E.LastEpisodeInSpell = 1
	
				--left join [APC].[Patient] P
				--on P.[EncounterRecno] = S.EncounterRecno
					
  					left join [WH].[Theatre].[OperationDetail] TH
					  on TH.DistrictNo = E.FacilityID
						and
						TH.OperationDate between S.AdmissionDate and S.DischargeDate
						and
						th.OperationCancelledFlag <> 1
						
left join ---Sub Query For ORMISOperationTimes
							(
							Select 
							E.EpisodeUniqueID
							,E.SourceSpellNo
							,E.FacilityID

									,Min(TH.[InSuiteTime])				as ORMISInSuiteTime
								  ,Min(TH.[InAnaestheticTime])			as ORMISInAnaestheticTime
								  ,Min(TH.[AnaestheticInductionTime])	as ORMISAnaestheticInductionTime
								  ,Min(TH.[OperationStartDate])			as ORMISOperationStartDate
								  ,Min(TH.[OperationEndDate])			as ORMISOperationEndDate
								  ,Min(TH.[InRecoveryTime])				as ORMISInRecoveryTime
							from  [APC].[Episode] E
								
								RIGHT join [APC].[AllDiagnosis] Diag
								on E.EpisodeUniqueID = Diag.EpisodeSourceUniqueID
								and left(Diag.DiagnosisCode,4) = 'S720'
								
	  												left join [WH].[Theatre].[OperationDetail] TH
												  on TH.DistrictNo = E.FacilityID
													and
													TH.OperationDate between E.AdmissionDate and E.DischargeDate
													and
													th.OperationCancelledFlag <> 1
													
													left join [WH].[Theatre].[Staff] Cons
													on Cons.StaffCode = TH.ConsultantCode 	
														
											
														Left Join [WHOLAP].[dbo].[OlapTheatre] T
														on th.TheatreCode = T.TheatreCode
													
							where 
							E.FirstEpisodeInSpell ='1'

							group by
							E.EpisodeUniqueID
							,E.SourceSpellNo
							,E.FacilityID

							) ORMIS
							on ORMIS.SourceSpellNo = S.SourceSpellNo
							
					
				left join
				----Gets the number of re-admissions---
				(
				select 
				Readm.PreviousAdmissionRecno
				,AM.[AdmissionMethodType]
				,RS.AdmissionDateTime as ReadmissionDate
				,Readm.ReadmissionIntervalDays
		
				from
				[WHOLAP].[dbo].[FactReadmission] Readm
				left join [WHOLAP].[dbo].[OlapAdmissionMethod] AM
				on Readm.ReadmissionAdmissionMethodCode = AM.AdmissionMethodCode
				left join [APC].[Spell] RS
				on Readm.ReadmissionRecno = RS.EncounterRecno
				where AM.[AdmissionMethodTypeCode] = 2 ----Emergency----
				) RR
				on RR.PreviousAdmissionRecno = S.EncounterRecno
				
				left join
				[LK].[Calendar] Dis
				on Dis.TheDate = S.DischargeDate
				
				left join
				[LK].[Calendar] Adm
				on Adm.TheDate = S.AdmissionDate
				
				left join
				----Gets the first stay on A5----
				(
				SELECT 
						 WS.[SourceSpellNo]
						,MIN([StartTime]) as FirstStayOnA5
						,MAX(WS.EndTime)as LastStayOnA5 

							FROM [APC].[WardStay] WS
						  
							left join [APC].[Spell] S
							on WS.SourceSpellNo = S.SourceSpellNo
						  
						  where 
							(S.DischargeDateTime between  
							(select 
						 min(Cal.TheDate)
						 FROM LK.Calendar Cal
						 where
						Cal.FinancialYear in 
						(select distinct
						SQ.FinancialYear
						  FROM LK.Calendar SQ
						  where SQ.TheDate = 
						 (
						dateadd(MONTH,-24,cast(GETDATE()as DATE))
						)))
						and
						(select 
						dbo.GetEndOfDay (max(Cal.TheDate)) LastDateTimeofRolling13Month
						 FROM LK.Calendar Cal
						 
						 where
						Cal.FinancialYear in 
						(select distinct
						SQ.FinancialYear
						  FROM LK.Calendar SQ
						  where SQ.TheDate = 
						 (
						cast(GETDATE()as DATE)
						))))

						and WS.WardCode = 'A5'
						group by  WS.[SourceSpellNo]
						) A5
						on A5.SourceSpellNo = S.SourceSpellNo
				-----End of A5 Sub Query----
				
				Left join
				----Gets the first day of Rehab----
				  (Select 
						E.SourceSpellNo
						,Min(P.ProcedureDate) FirstRehabProc
				  
				  FROM [APC].[Episode] E

						right join
						(
						Select distinct
						P.EpisodeSourceUniqueID
				,		P.ProcedureDate
						From [APC].[AllProcedures] P
						where
						left(P.ProcedureCode,3) in ('U50','U53')---Insert Procedures here
						) 
						P
						on E.EpisodeUniqueID = P.EpisodeSourceUniqueID
						Group By
						 E.SourceSpellNo
						 ) Rehab
						 on Rehab.SourceSpellNo = S.SourceSpellNo
				------End of RehabQuery
		
	
  where
(
	(S.DischargeDateTime between  
	(select 
 min(Cal.TheDate)
 FROM LK.Calendar Cal
 where
Cal.FinancialYear in 
(select distinct
SQ.FinancialYear
  FROM LK.Calendar SQ
  where SQ.TheDate = 
 (
dateadd(MONTH,-24,cast(GETDATE()as DATE))
)))
and
(select 
dbo.GetEndOfDay (max(Cal.TheDate)) LastDateTimeofRolling13Month
 FROM LK.Calendar Cal
 
 where
Cal.TheMonth = 
(select distinct
SQ.TheMonth
  FROM LK.Calendar SQ
  where SQ.TheDate = 
 (
dateadd(MM,-1,cast(GETDATE()as DATE))
))
))
or
	(S.AdmissionDateTime >=  
	(select 
 min(Cal.TheDate)
 FROM LK.Calendar Cal
 where
Cal.FinancialYear in 
(select distinct
SQ.FinancialYear
  FROM LK.Calendar SQ
  where SQ.TheDate = 
 (
dateadd(MONTH,-24,cast(GETDATE()as DATE))
)))
and S.DischargeDate <= (select 
dbo.GetEndOfDay (max(Cal.TheDate)) LastDateTimeofRolling13Month
 FROM LK.Calendar Cal
 
 where
Cal.TheMonth = 
(select distinct
SQ.TheMonth
  FROM LK.Calendar SQ
  where SQ.TheDate = 
 (
dateadd(MM,-1,cast(GETDATE()as DATE))
))
)
)
)
	
	and
	S.LengthOfSpell between 1 and 49
	and
	(
	  left(cc.[PrimaryDiagnosis],4) = 'S720'
      or left(cc.[SubsidiaryDiagnosis],4) = 'S720'
      or left(cc.[SecondaryDiagnosis1],4) = 'S720'
      or left(cc.[SecondaryDiagnosis2],4) = 'S720'
      or left(cc.[SecondaryDiagnosis3],4) = 'S720'
      or left(cc.[SecondaryDiagnosis4],4) = 'S720'
      or left(cc.[SecondaryDiagnosis5],4) = 'S720'    
      or left(cc.[SecondaryDiagnosis6],4) = 'S720'     
      or left(cc.[SecondaryDiagnosis7],4) = 'S720'  
      or left(cc.[SecondaryDiagnosis8],4) = 'S720'    
      or left(cc.[SecondaryDiagnosis9],4) = 'S720'      
      or left(cc.[SecondaryDiagnosis10],4) = 'S720'
      or left(cc.[SecondaryDiagnosis11],4) = 'S720'
      or left(cc.[SecondaryDiagnosis12],4) = 'S720'
      or left(cc.[SecondaryDiagnosis13],4) = 'S720'
      or left(cc.[SecondaryDiagnosis14],4) = 'S720'
      or left(cc.[SecondaryDiagnosis15],4) = 'S720'
      or left(cc.[SecondaryDiagnosis16],4) = 'S720'
      or left(cc.[SecondaryDiagnosis17],4) = 'S720'
      or left(cc.[SecondaryDiagnosis18],4) = 'S720'
      or left(cc.[SecondaryDiagnosis19],4) = 'S720'
      or left(cc.[SecondaryDiagnosis20],4) = 'S720'
      )
      


          
     order by 
  
     S.AdmissionDateTime