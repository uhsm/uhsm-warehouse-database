﻿/*
--Author: N Scattergood
--Date created: 29/04/2013
--Stored Procedure Built for SRSS Report on:
--Over Night Bed Usage By Ward And Specialty
--Used to analyse Bed Outliers from differing perspectives
*/


CREATE Procedure [USS].[ReportOverNightBedUsageByWardAndSpecialty]

@StartDate		as Date
, @EndDate		as Date  
,@WardPar		as nvarchar(max)
,@DIVPar		as nvarchar(max)
,@dirPar		as nvarchar(max)
,@SpecPar		as nvarchar(max)
as
SELECT

--E.[EncounterRecno]
--      ,E.[SourceSpellNo]
--      ,E.[EpisodeUniqueID]
--      ,E.[FacilityID]
--      ,E.[NHSNo]
--      ,E.[AdmissionDateTime]
--      ,E.[DischargeDateTime]
--      ,E.[EpisodeStartDateTime]
--      ,E.[EpisodeEndDateTime]
--      ,E.[ConsultantCode]
--      ,E.[ConsultantName],
      E.[Division]
      ,E.[Directorate]
      ,E.[SpecialtyCode(Function)]
      ,E.[Specialty(Function)]
      --,E.[LengthOfEpisode]
      --,E.[LastEpisodeInSpell]
      --,E.[EpisodeStartDateTime]
      --,E.[EpisodeEndDateTime]
    
      ,WS.[WardCode]
  --    ,WS.[StartTime]	as WardStart
  --,WS.[EndTime]			as WardEnd
  
  --,case 
  --when WS.[StartTime] >= E.[EpisodeStartDateTime]
  --and WS.[StartTime] >= @StartDate
  --then WS.StartTime
  --when E.[EpisodeStartDateTime] >= @StartDate
  --then E.[EpisodeStartDateTime]
  --else @StartDate
  --end
  --as [StartTime]
  --,case 
  --when (WS.[EndTime] <= (coalesce(E.EpisodeEndDateTime,E.DischargeDateTime))
  --or (coalesce(E.EpisodeEndDateTime,E.DischargeDateTime)) is null)
  --and WS.[EndTime] <= [WHREPORTING].dbo.GetEndOfDay (@EndDate) 
  --then [EndTime]
  --when (coalesce(E.EpisodeEndDateTime,E.DischargeDateTime)) <= [WHREPORTING].dbo.GetEndOfDay (@EndDate) 
  --then (coalesce(E.EpisodeEndDateTime,E.DischargeDateTime))
  --else [WHREPORTING].dbo.GetEndOfDay (@EndDate) 
  --end
  --as [EndTime]	
  ,SUM(
  DATEDIFF
  (day,
  case 
  when WS.[StartTime] >= E.[EpisodeStartDateTime]
  and WS.[StartTime] >= @StartDate
  then WS.StartTime
  when E.[EpisodeStartDateTime] >= @StartDate
  then E.[EpisodeStartDateTime]
  else @StartDate
  end
  ,case 
  when (WS.[EndTime] <= (coalesce(E.EpisodeEndDateTime,E.DischargeDateTime))
  or (coalesce(E.EpisodeEndDateTime,E.DischargeDateTime)) is null)
  and WS.[EndTime] <= [WHREPORTING].dbo.GetEndOfDay (@EndDate) 
  then [EndTime]
  when (coalesce(E.EpisodeEndDateTime,E.DischargeDateTime)) <= [WHREPORTING].dbo.GetEndOfDay (@EndDate) 
  then (coalesce(E.EpisodeEndDateTime,E.DischargeDateTime))
  else [WHREPORTING].dbo.GetEndOfDay (@EndDate) 
  end
)) AS OverNightBedsUsed

,cast((datediff(day,@StartDate,@EndDate))as varchar) as 'LengthOfPeriod'
  

  FROM [WHREPORTING].[APC].[Episode] E
  
		left join [WHREPORTING].[APC].[WardStay] WS
		on E.SourceSpellNo = WS.SourceSpellNo
		and
		WS.StartTime < [WHREPORTING].dbo.GetEndOfDay (@EndDate) 
		and
		(WS.StartTime < (coalesce(E.EpisodeEndDateTime,E.DischargeDateTime))
		or 
		  (
		  E.EpisodeEndDate	is null
		  and
		  E.DischargeDate is null
		  )
		)
		and 
		(
		WS.EndTime is null
		or 
		(
		WS.EndTime > E.EpisodeStartDateTime
		and 
		WS.EndTime >@StartDate
		)
		)						
			  left join	
				  (	
				  select distinct
				SPB.SPONT_REFNO_CODE
				,[SPECT_REFNO_MAIN_IDENT]
				from WH.PAS.ServicePointBase SPB
				where
				SPB.[SPTYP_REFNO_MAIN_CODE] = 'WARD'
				  )	 SPB
				  on WS.WardCode = SPB.SPONT_REFNO_CODE

  where 
  [SPECT_REFNO_MAIN_IDENT] = 'Wyth' --- Only Wythenshawe Overnight based Wards
  and
  --Following Comes From Parameters
  E.EpisodeStartDateTime	
  < @EndDate
  --< =[WHREPORTING].dbo.GetEndOfDay (@EndDate) 
  and
  (
  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime)		>	@StartDate
  or 
  (
  E.EpisodeEndDate	is null
  and
  E.DischargeDate is null
  )
  )
	and WS.WardCode		in (SELECT Val from dbo.fn_String_To_Table(@WardPar,',',1))
	and E.Division	in (SELECT Val from dbo.fn_String_To_Table(@DIVPar,',',1))
	and E.Directorate	in (SELECT Val from dbo.fn_String_To_Table(@dirPar,',',1))
	and E.[SpecialtyCode(Function)] in	(SELECT Val from dbo.fn_String_To_Table(@SpecPar,',',1))

  
  Group By
  
       E.[Division]
      ,E.[Directorate]
      ,E.[SpecialtyCode(Function)]
      ,E.[Specialty(Function)]
       ,WS.[WardCode]
       --,(datediff(day,@StartDate,@EndDate))

  
--order by
--OverNightBedsUsed
--,
--E.EpisodeEndDateTime
----,
----E.EpisodeStartDateTime
--,WS.EndTime