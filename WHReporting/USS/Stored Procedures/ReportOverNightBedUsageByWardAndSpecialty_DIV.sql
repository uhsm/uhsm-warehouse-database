﻿/*
--Author: N Scattergood
--Date created: 29/04/2013
--Stored Procedure Built for SRSS Report on:
--Over Night Bed Usage By Ward And Specialty
--Used to analyse Bed Outliers from differing perspectives
--This Provides the Episodic Division Parameter
*/


CREATE Procedure [USS].[ReportOverNightBedUsageByWardAndSpecialty_DIV]

@StartDate		as Date
, @EndDate		as Date  
,@WardPar		as nvarchar(max)
--,@dirPar		as nvarchar(max)
--,@SpecPar		as nvarchar(max)
as
SELECT
Distinct
      
      E.[Division]					as DIVPar
      --E.[Directorate]					as dirPar
      --,E.[SpecialtyCode(Function)]	as SpecParCode
      --E.[Specialty(Function)]			as SpecParDesc
      --WS.[WardCode]					as WardPar

  

  FROM [WHREPORTING].[APC].[Episode] E
  
		left join [WHREPORTING].[APC].[WardStay] WS
		on E.SourceSpellNo = WS.SourceSpellNo
		and
		WS.StartTime < [WHREPORTING].dbo.GetEndOfDay (@EndDate) 
		and
		(WS.StartTime < (coalesce(E.EpisodeEndDateTime,E.DischargeDateTime))
		or 
		  (
		  E.EpisodeEndDate	is null
		  and
		  E.DischargeDate is null
		  )
		)
		and 
		(
		WS.EndTime is null
		or 
		(
		WS.EndTime > E.EpisodeStartDateTime
		and 
		WS.EndTime >@StartDate
		)
		)						
			  left join	
				  (	
				  select distinct
				SPB.SPONT_REFNO_CODE
				,[SPECT_REFNO_MAIN_IDENT]
				from WH.PAS.ServicePointBase SPB
				where
				SPB.[SPTYP_REFNO_MAIN_CODE] = 'WARD'
				  )	 SPB
				  on WS.WardCode = SPB.SPONT_REFNO_CODE

  where 
  [SPECT_REFNO_MAIN_IDENT] = 'Wyth' --- Only Wythenshawe based Wards
  and
  --Following Comes From Parameters
  E.EpisodeStartDateTime	
  < @EndDate
  --< =[WHREPORTING].dbo.GetEndOfDay (@EndDate) 
  and
  (
  coalesce(E.EpisodeEndDateTime,E.DischargeDateTime)		>	@StartDate
  or 
  (
  E.EpisodeEndDate	is null
  and
  E.DischargeDate is null
  )
  )
  and E.Division <> 'Data Quality'
	and WS.WardCode		in (SELECT Val from dbo.fn_String_To_Table(@WardPar,',',1))
	--and E.Directorate	in (SELECT Val from dbo.fn_String_To_Table(@dirPar,',',1))
	--and E.[SpecialtyCode(Function)] in	(SELECT Val from dbo.fn_String_To_Table(@SpecPar,',',1))
        
order by
    E.[Division]					
      --E.[Directorate]					
      --,E.[SpecialtyCode(Function)]	
      --E.[Specialty(Function)]			
      --WS.[WardCode]