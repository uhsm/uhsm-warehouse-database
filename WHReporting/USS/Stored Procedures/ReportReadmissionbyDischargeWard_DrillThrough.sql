﻿/*
--Author: N Scattergood
--Date created: 02/07/2013
--Stored Procedure Built for SRSS Report on:
--30 Day Emergency Readmissions by Discharge Ward
--Used for Drillthrough Details
*/


CREATE Procedure [USS].[ReportReadmissionbyDischargeWard_DrillThrough]
@StartDate		as Date
, @EndDate		as Date  
--,@WardPar		as nvarchar(max)
--,@DIVPar		as nvarchar(max)
--,@dirPar		as nvarchar(max)
--,@SpecPar		as nvarchar(max)

as
SELECT 

Distinct

      S.[SourceSpellNo]
      ,S.FaciltyID
      ,S.NHSNo
      ,S.[AdmissiontWardCode] as AdmissionWardCode
    ,case 
    when S.[AdmissionType] in ('Emergency','Non-Elective') 
    then 'Non-Elective'
	else S.[AdmissionType] 
	end as 'AdmissionMethod' 
    ,case when S.[InpatientStayCode] = 'D'	
    then 'DayCase'
	when  S.[InpatientStayCode] = 'I'	
	then 'Inpatient'
	when  S.[InpatientStayCode] = 'B'	
	then 'Baby'
	else S.[InpatientStayCode] 
	end as InpatientStay 
	,S.AdmissionDateTime
	,S.DischargeDateTime
	,Cal.TheMonth as DischargeMonth
	,E.Division
	,E.Directorate
	,E.[SpecialtyCode(Function)]
	,E.[Specialty(Function)]
	,AC.PriamryProcedure as PreviousPrimaryProcedure
	,AC.PrimaryDiagnosis as PreviousDiagnosis
	,AC.SubsidiaryDiagnosis as PreviousSubsidiaryDiagnosis
	,AC.SecondaryDiagnosis1 as PreviousSecondaryDiagnosis1
	,S.DischargeWardCode
	,coalesce(PW.WardCode,S.DischargeWardCode) as Ward
	,RR.ReadmissionAdmissionMethod
	,RR.ReadmissionDate
	,RR.ReadmissionIntervalDays
	,RR.ReadmissionPrimaryProcedure
	,RR.ReadmissionPrimaryDiagnosis
	,RR.ReadmissionSubsidiaryDiagnosis
	,RR.ReadmissionSecondaryDiagnosis1
	,RR.SameSpec
	,RR.ReadmissionSpecialty
	
	FROM [WHREPORTING].[APC].[Spell] S
	
	left join	[WHREPORTING].[APC].[WardStay]  DL
						on DL.WardCode = 'DL'
						and S.DischargeDateTime = DL.EndTime
						and S.SourceSpellNo = DL.SourceSpellNo
						
		left join [WHREPORTING].[APC].[WardStay]  PW
					on DL.SourceSpellNo = PW.SourceSpellNo
					and DL.StartTime = PW.EndTime
					
					left join .[APC].[Episode] E
					on E.SourceSpellNo = S.SourceSpellNo
					and E.LastEpisodeInSpell = 1
					
					left join [WHREPORTING].[APC].[ClinicalCoding] AC
				on AC.EncounterRecno = E.EncounterRecno
				
				left join------Gets the Date Information for the Discharge Date------
				[WHREPORTING].[LK].[Calendar] Cal 
				on Cal.TheDate = S.DischargeDate
					
				inner join 
				----Gets the emergency re-admissions( inner join restricts it to Readmissions Only) ----
				(
				select 
				Readm.PreviousAdmissionRecno
				,AM.[AdmissionMethodType] as ReadmissionAdmissionMethod
				,RS.AdmissionDateTime as ReadmissionDate
				,Readm.ReadmissionIntervalDays
				,Readm.MetricCode as SameSpec
				,RS.DischargeDateTime as ReadmissionDischargeDate
				,RC.PriamryProcedure as ReadmissionPrimaryProcedure
				,RC.PrimaryDiagnosis as ReadmissionPrimaryDiagnosis
				,RC.SubsidiaryDiagnosis as ReadmissionSubsidiaryDiagnosis
				,RC.SecondaryDiagnosis1 as ReadmissionSecondaryDiagnosis1
				,RS.[AdmissionSpecialty(Function)] as ReadmissionSpecialty
		
				from
				[WHOLAP].[dbo].[FactReadmission] Readm
				left join [WHOLAP].[dbo].[OlapAdmissionMethod] AM
				on Readm.ReadmissionAdmissionMethodCode = AM.AdmissionMethodCode
				left join [WHREPORTING].[APC].[Spell] RS
				on Readm.ReadmissionRecno = RS.EncounterRecno
				left join [WHREPORTING].[APC].[ClinicalCoding] RC
				on RS.EncounterRecno = RC.EncounterRecno
				where 
				AM.[AdmissionMethodTypeCode] = 2 ----Emergency----
				and
				Readm.ReadmissionIntervalDays between 0 and 30 ----30 Day Readmissions
				--and 
				--RS.DischargeDate between @StartDate and @EndDate
				) RR
				on RR.PreviousAdmissionRecno = S.EncounterRecno		
				
					
					
				where
				S.DischargeDate between @StartDate and @EndDate
				--E.Division = 'Scheduled Care'
				
				
				
				order by 
				S.DischargeWardCode