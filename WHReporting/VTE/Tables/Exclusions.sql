﻿CREATE TABLE [VTE].[Exclusions](
	[OPCS] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
	[Exc] [varchar](25) NULL
) ON [PRIMARY]