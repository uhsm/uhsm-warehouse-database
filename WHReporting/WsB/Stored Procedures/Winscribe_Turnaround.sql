﻿CREATE Procedure [WsB].[Winscribe_Turnaround]
	 @JobTypeID int
	 ,@Startdate Datetime
     ,@Enddate Datetime
as

/*
Declare @JobTypeID int = 23
Declare @Startdate Datetime = '2015-01-01 00:00:00.000'
Declare @Enddate Datetime = '2016-01-01 00:00:00.000'
*/

/*xxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
SELECT 
      [JL_JOBNO]
      ,[JobTypeID]
      ,[JobTypeDescription]
      ,[EndJobDate]
      ,DATEADD(month, DATEDIFF(month, 0, [EndJobDate]), 0)               as FirstdayofMonth
      ,MONTH([EndJobDate])												 as [Monthno]
      ,Datename(MONTH,[EndJobDate])										 as [Monthname]
      ,YEAR([EndJobDate])												 as [Year]
      ,convert(Char(3),Datename(MONTH,[EndJobDate]),0)	
        +' - '+cast(YEAR([EndJobDate])as varchar(10))     	             as [Year-Month]
      ,sum([LengthofJobasplayedtotheTypist])							 as TotalDictationLength
      ,sum([Timetakentotypejob])                                         as Timetakentotypejob
      ,sum([ActualTurnaround])                                           as ActualTurnaround
  into #Result
  FROM [WHREPORTING].[WsB].[Winscribe_Turnaround_Data]
where [JobTypeID] = @JobTypeID and DATEADD(month, DATEDIFF(month, 0, [EndJobDate]), 0) between @Startdate and @Enddate
  Group by 
       [JobTypeID]
      ,[JobTypeDescription]
      ,[EndJobDate]
      ,[JL_JOBNO]
 Order by [EndJobDate], [JobTypeDescription]
 
 /*xxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
 Select 
      [JobTypeID]
      ,[JobTypeDescription]
      ,[Monthno]
      ,[Monthname]
      ,[Year]
      ,[Year-Month]
      ,FirstdayofMonth
      ,count([JL_JOBNO])												 as TotalJobs
      ,sum(TotalDictationLength)							             as TotalDictationLength
      ,sum([TotalDictationLength])/count([JL_JOBNO])			         as AvgDictationLength 
      ,sum([Timetakentotypejob])/count([JL_JOBNO])						 as AvgTimetoTypeJob
      ,sum([ActualTurnaround])/count([JL_JOBNO])						 as AvgTurnaroundTime                                               
      ,Round(((cast(sum(cast([ActualTurnaround] as bigint))/count([JL_JOBNO])as float)) 
         /86400.00),2)													 as AvgTurnaroundDays
From #Result 
Group by 
        [JobTypeID]
      ,[JobTypeDescription]      
      ,[Monthno]
      ,[Monthname]
      ,[Year]
      ,[Year-Month]
     ,FirstdayofMonth
Order by FirstdayofMonth
/*xxxxxxxxxxxxxxxxxxxxxxxxxxxx*/

Drop table #Result