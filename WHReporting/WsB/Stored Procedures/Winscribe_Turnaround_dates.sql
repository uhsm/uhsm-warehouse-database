﻿CREATE Procedure [WsB].[Winscribe_Turnaround_dates]
as

SELECT distinct
     DATEADD(month, DATEDIFF(month, 0, [EndJobDate]), 0)               as StartMonth
     ,DATEADD(month, DATEDIFF(month, 0, [EndJobDate]), 0)               as EndMonth
      ,convert(Char(3),Datename(MONTH,[EndJobDate]),0)	
        +' - '+cast(YEAR([EndJobDate])as varchar(10))     	             as [Year-Month]
  FROM [WHREPORTING].[WsB].[Winscribe_Turnaround_Data]
 order by DATEADD(month, DATEDIFF(month, 0, [EndJobDate]), 0)