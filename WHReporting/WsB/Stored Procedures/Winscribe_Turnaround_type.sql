﻿Create Procedure [WsB].[Winscribe_Turnaround_type]
as

SELECT Distinct
      [JobTypeID]
      ,[JobTypeDescription]
FROM [WHREPORTING].[WsB].[Winscribe_Turnaround_Data]
order by [JobTypeDescription]