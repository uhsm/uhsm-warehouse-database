﻿CREATE FUNCTION [dbo].[BSTAdjustment] 
(
	@from smalldatetime
	,@to smalldatetime
)
RETURNS int

AS
BEGIN
	DECLARE @adjustment int
	
	--For testing
	--DECLARE @from smalldatetime
	--DECLARE @to smalldatetime
	
	--set @from = '2013-03-31 00:59:00'
	--set @to = '2013-03-31 03:01:00'
	
	select
		@adjustment =
			case
			when @from <= 
				dateadd(
					hour
					,1
					,(
					select
						max(Calendar.TheDate)
					from
						WHOLAP.dbo.CalendarBase Calendar
					where
						Calendar.TheDate <= '31 March ' + datename(year, @from)
					and	datename(dw, Calendar.TheDate) = 'Sunday'
					)
				)

			and	@to >=
				dateadd(
					hour
					,1
					,(
					select
						max(Calendar.TheDate)
					from
						WHOLAP.dbo.CalendarBase Calendar
					where
						Calendar.TheDate <= '31 March ' + datename(year, @to)
					and	datename(dw, Calendar.TheDate) = 'Sunday'
					)
				)
			then -60
			-- If @from < 01:00 on morning of clock change
			-- and  @to > 01:00 on morning of clock change
			-- then -60
			when @from <= 
				dateadd(
					hour
					,1
					,(
					select
						max(Calendar.TheDate)
					from
						WHOLAP.dbo.CalendarBase Calendar
					where
						Calendar.TheDate <= '31 October ' + datename(year, @from)
					and	datename(dw, Calendar.TheDate) = 'Sunday'
					)
				)

			and	@to >=
				dateadd(
					hour
					,1
					,(
					select
						max(Calendar.TheDate)
					from
						WHOLAP.dbo.CalendarBase Calendar
					where
						Calendar.TheDate <= '31 October ' + datename(year, @to)
					and	datename(dw, Calendar.TheDate) = 'Sunday'
					)
				)
			then 60
			else 0
			end


	RETURN @adjustment
	--select  @adjustment

END