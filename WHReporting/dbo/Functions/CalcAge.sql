﻿CREATE FUNCTION [dbo].[CalcAge] (@StartDate datetime, @EndDate datetime)

RETURNS decimal(18,9)

WITH EXECUTE AS CALLER

AS

BEGIN

DECLARE @Age decimal(18,9), @RecentAnniversary smalldatetime, @UpcomingAnniversary smalldatetime, 

@NumDaysPerYear int

--SELECT @StartDate='10/18/1910', @EndDate='8/11/2000' 

-- DETERMINE THE MOST RECENT ANNIVERSARY DATE AND NEXT ANNIVERSARY DATE. 

-- THIS WILL BE USED FOR DETERMINING THE DECIMAL PART OF AGE.

select @RecentAnniversary=cast(cast(month(@StartDate) as varchar(2)) + '/' + cast(day(@StartDate) as varchar(2)) + '/' + cast(year(@EndDate) as varchar(4)) as smalldatetime)

if @RecentAnniversary>@EndDate

BEGIN

select @UpcomingAnniversary=@RecentAnniversary 

select @RecentAnniversary=cast(cast(month(@StartDate) as varchar(2)) + '/' + cast(day(@StartDate) as varchar(2)) + '/' + cast(year(@EndDate)-1 as varchar(4)) as smalldatetime)

END

else

select @UpcomingAnniversary=cast(cast(month(@StartDate) as varchar(2)) + '/' + cast(day(@StartDate) as varchar(2)) + '/' + cast(year(@EndDate)+1 as varchar(4)) as smalldatetime)

-- NOW DETERMINE THE NUMBER OF DAYS BETWEEN THE ANNIVERSARY DATES. T

-- THIS WILL TAKE INTO ACCOUNT LEAP YEARS

select @NumDaysPerYear = DateDiff(day, @RecentAnniversary, @UpcomingAnniversary)

-- GET THE BASE AGE OF OF THE DATES

SELECT @Age=DATEDIFF (YYYY, @StartDate, @EndDate) - 

CASE 

WHEN (MONTH(@StartDate)=MONTH(@EndDate) AND DAY(@StartDate) > DAY(@EndDate) 

OR MONTH (@StartDate) > MONTH (@EndDate)) 

THEN 1 

ELSE 0 

END 

-- NOW GET THE DECIMAL PART

--select @Age, DateDiff(day, @RecentAnniversary, @EndDate), @NumDaysPerYear

Select @Age=@Age + cast(DateDiff(day, @RecentAnniversary, @EndDate) as decimal(12,9)) / cast(@NumDaysPerYear as decimal(12,9))

-- TEST BREAKING OUT COMPONENTS. THERE ARE SOME PRECISION ISSUES, BUT THE ROUNDING 

-- BELOW SEEMS TO ADDRESS IT

--select @Age as Age, floor(@Age) AS AgeYear, cast(ROUND(((@Age-floor(@Age)) * @NumDaysPerYear),0) as smallint) As DaysSinceLastAnniversary

RETURN (@Age)

END;