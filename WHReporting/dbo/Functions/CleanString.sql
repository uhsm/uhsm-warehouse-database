﻿create function [dbo].[CleanString](@str varchar(8000))
returns varchar(8000) as
--removes multiple spaces, 
--replaces carriage returns (13) and line feeds (10) with single space
begin 
    --WHILE CHARINDEX('  ', @str) > 0 
    --    SET @str = REPLACE(@str, '  ', ' ')
	set @str = 
	replace(replace(
		replace(replace(replace(replace(@str,'  ',' '),'  ',' '),'  ',' '),'  ',' ')
		, char(13), ' '), char(10), ' ')
		return @str
end