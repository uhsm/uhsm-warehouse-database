﻿/*===============================================================================
Author:		J Pitchforth
Date:		26 Oct 2012
Function:	Fetch age from birthdate.  Returns exact age without rounding.
Version:	1.0
=================================================================================*/
--Example: select dbo.GetAgeFromBirthDate( '1926-10-07' )

create function [dbo].[GetAgeFromBirthDate] ( @BirthDate datetime )
 
returns int
 
as
 
begin
 
declare @Today datetime
declare @Age int
 
set @Today = getdate()
 
set @Age =  datediff( year, @BirthDate, @Today ) - 
                  case when 100 * month( @Today ) + 
                  day( @Today ) < 100 * month( @BirthDate ) + 
                  day( @BirthDate ) then 1 else 0 end
 
return @Age
 
end