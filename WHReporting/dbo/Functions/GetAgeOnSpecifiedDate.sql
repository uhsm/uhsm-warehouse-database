﻿/*===============================================================================
Author:		J Pitchforth
Date:		2 Nov 2012
Function:	Fetch age on a given date.  Returns exact age without rounding.
Version:	1.0
=================================================================================*/
--Example: select dbo.GetAgeOnSpecifiedDate( '1926-10-07', '2000-01-01 )

CREATE function [dbo].[GetAgeOnSpecifiedDate] ( @BirthDate datetime , @SpecifiedDate datetime)
 
returns int
 
as
 
begin
 

declare @Age int
 
 
set @Age =  datediff( year, @BirthDate, @SpecifiedDate ) - 
                  case when 100 * month( @SpecifiedDate ) + 
                  day( @SpecifiedDate) < 100 * month( @BirthDate ) + 
                  day( @BirthDate ) then 1 else 0 end
 
return @Age
 
end