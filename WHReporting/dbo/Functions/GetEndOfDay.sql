﻿/*===============================================================================
Author:		J Pitchforth
Date:		26 Oct 2012
Function:	Fetch the time at the end of the day from the given date. That is, 
			set the time element to 23:59 hours.
Version:	1.0
=================================================================================*/
--Example: InDate = 2010-01-18 15:56:00  OutDate = 2010-01-18 23:59:00
 
CREATE function [dbo].[GetEndOfDay] ( @InDate smalldatetime )
 
returns smalldatetime
 
as
 
begin 
 
      declare @OutDate smalldatetime
      set @OutDate = convert( smalldatetime, convert( char(10), max( @InDate ), 120 ) + ' 23:59', 120 )
      
      return @OutDate
      
end