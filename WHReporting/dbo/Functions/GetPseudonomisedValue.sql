﻿CREATE function [dbo].[GetPseudonomisedValue]
(
	 @SourcePatientNo varchar(50)
	,@AttributeCode varchar(10)
	,@SourceSystemCode varchar(10)
)

RETURNS varchar(20)

as

begin

	declare @ReturnValue varchar(20)

	select
		@ReturnValue =
			'P' + PseudoMap.PseudonomisedValue
	from
		Pseudonomisation.dbo.PatientAttributeMap PseudoMap

	inner join Pseudonomisation.dbo.Attribute
	on	Attribute.AttributeCode = @AttributeCode

	inner join Pseudonomisation.dbo.SourceSystem
	on	SourceSystem.SourceSystemCode = @SourceSystemCode

	where
		PseudoMap.SourcePatientNo = @SourcePatientNo
	and	PseudoMap.AttributeRecno = Attribute.AttributeRecno
	and	PseudoMap.SourceSystemRecno = SourceSystem.SourceSystemRecno

return @ReturnValue

end