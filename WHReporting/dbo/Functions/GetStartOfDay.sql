﻿/*===============================================================================
Author:		J Pitchforth
Date:		26 Oct 2012
Function:	Fetch the time at the start of the day from the given date. That is, 
			set the time element to 00:00 hours.
Version:	1.0
=================================================================================*/
--Example: InDate = 2010-01-18 15:56:00  OutDate = 2010-01-18 00:00:00

 
create function [dbo].[GetStartOfDay] ( @InDate smalldatetime )
 
returns smalldatetime
 
as
 
begin 
 
      declare @OutDate smalldatetime
      set @OutDate = convert( smalldatetime, convert( char(10), max( @InDate ), 120 ), 120 )
      
      return @OutDate
      
end