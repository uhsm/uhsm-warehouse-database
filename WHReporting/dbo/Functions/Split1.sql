﻿CREATE FUNCTION [dbo].[Split1]
/* This function is used to split up multi-value parameters */
(
@ItemList NVARCHAR(max),
@delimiter CHAR(1)
)
RETURNS @IDTable TABLE (Item1 NVARCHAR(100) collate database_default )
AS
BEGIN
DECLARE @tempItemList NVARCHAR(max)
SET @tempItemList = @ItemList

DECLARE @i INT
DECLARE @Item1 NVARCHAR(max)

SET @tempItemList = REPLACE (@tempItemList, @delimiter + ' ', @delimiter)
SET @i = CHARINDEX(@delimiter, @tempItemList)

WHILE (LEN(@tempItemList) > 0)
BEGIN
IF @i = 0
SET @Item1 = @tempItemList
ELSE
SET @Item1 = LEFT(@tempItemList, @i - 1)

INSERT INTO @IDTable(Item1) VALUES(@Item1)

IF @i = 0
SET @tempItemList = ''
ELSE
SET @tempItemList = RIGHT(@tempItemList, LEN(@tempItemList) - @i)

SET @i = CHARINDEX(@delimiter, @tempItemList)
END
RETURN
END
--use the following line in your stored procedure
--WHERE ProductID IN (SELECT Item FROM dbo.Split (@Item, ','))