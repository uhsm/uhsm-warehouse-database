﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fnMyTimeFormat]
(
    @FromDate DATETIME,
    @ToDate DATETIME
)
RETURNS VARCHAR(80)
AS
BEGIN
    RETURN  (
                SELECT  CASE DATEPART(DAYOFYEAR, Duration)
                            WHEN 2 THEN '1 Day, '
                            ELSE CAST(DATEDIFF(DAY, 0, Duration) AS VARCHAR(11)) + ' Days, '
                        END +
                        CASE DATEPART(HOUR, Duration)
                            WHEN 1 THEN '1 Hour, '
                            ELSE DATENAME(HOUR, Duration) + ' Hours, '
                        END + 
                        CASE DATEPART(MINUTE, Duration)
                            WHEN 1 THEN '1 Minute, '
                            ELSE DATENAME(MINUTE, Duration) + ' Minutes, '
                        END + 
                        CASE DATEPART(SECOND, Duration)
                            WHEN 1 THEN '1 Second, '
                            ELSE DATENAME(SECOND, Duration) + ' Seconds'
                        END
                FROM    (
                            SELECT  DATEADD(SECOND, ABS(DATEDIFF(SECOND, @FromDate, @ToDate)), 0) AS Duration
                        ) AS d
            )
END