﻿CREATE FUNCTION [dbo].[fnc_FiscalYear](          
@AsOf DATETIME  )  RETURNS INT  
AS  BEGIN            
DECLARE @Answer INT            
-- You define what you want here (September being your changeover month)         
 IF ( MONTH(@AsOf) < 4 ) SET @Answer = YEAR(@AsOf) - 1          
 ELSE SET @Answer = YEAR(@AsOf)              
 RETURN @Answer    
 END