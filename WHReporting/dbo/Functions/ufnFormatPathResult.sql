﻿CREATE FUNCTION [dbo].[ufnFormatPathResult](@result VARCHAR(30))
RETURNS decimal
AS
  BEGIN
-- Strip @number of extra chracters
      DECLARE @lenResult INT ,@ResultStr NVARCHAR(30)
      
      WHILE PATINDEX('%[^0-9.]%', @result) > 0 
        SET @result = REPLACE(@result,SUBSTRING(@result,PATINDEX('%[^0-9.]%', @result),1),'') 

    RETURN @result
END