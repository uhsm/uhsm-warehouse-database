﻿CREATE Function [dbo].[ufnTransposeInvestigations] 
	(
	@ReportIndex int
	)
Returns nvarchar(1000)
as

Begin
	Declare @Investigations varchar(max)

	Select @Investigations = Coalesce(@Investigations + ', ','') + dInvestigation.InvestigationCode +  ' - ' + dInvestigation.InvestigationRequested
	From ICE.FactInvestigation fInvestigation
	inner join ICE.DimInvestigationRequested dInvestigation
		on fInvestigation.InvestigationDescriptionKey = dInvestigation.InvestigationKey
	Where fInvestigation.ReportIndex = @ReportIndex

	Return @Investigations
End