﻿CREATE Procedure [dbo].[BuildAPCAllDiagnosis] as 

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

/*
Create Table APC.AllDiagnosis (
EncounterRecno int not null
,EpisodeSourceUniqueID int not null
,ClinicalCodingKey int not null
,DiagnosisCode varchar(20) null
,Diagnosis varchar(255) null
,DiagnosisSequence int not null
,DiagnosisDate date
,UserCreate varchar(70)
,CreateDate Date
,CreateTime time(0)
,CreateDateTime datetime
,UserModified varchar(70)
,ModifedDate Date
,ModifiedTime Time(0)
,ModifiedDateTime datetime
,CONSTRAINT [pk_APCAllDiagnosis] PRIMARY KEY CLUSTERED

	(
	 EncounterRecno
	,DiagnosisSequence
	,ClinicalCodingKey
	)
)

Create Index ixDiagnosisDate on APC.AllDiagnosis(DiagnosisDate)
Create Index ixDiagnosisCreateDate on APC.AllDiagnosis(CreateDate)
Create Index ixDiagnosisCreateTime on APC.AllDiagnosis(CreateTime)
Create Index ixDiagnosisUserCreate on APC.AllDiagnosis(UserCreate)





*/
Truncate Table APC.AllDiagnosis

Insert into APC.AllDiagnosis  (
		 EncounterRecno
		,EpisodeSourceUniqueID
		,ClinicalCodingKey
		,DiagnosisCode
		,Diagnosis
		,DiagnosisSequence
		,DiagnosisDate
		,UserCreate
		,CreateDate
		,CreateTime
		,CreateDateTime
		,UserModified
		,ModifedDate
		,ModifiedTime
		,ModifiedDateTime
		,[Counter]
)

Select  
		Episode.EncounterRecno
		,Episode.SourceUniqueID
		,ClinicalCodingKey = ClinicalCoding.ClinicalCodingCode
		,DiagnosisCode= Coding.Code
		,Diagnosis = Coding.DESCRIPTION
		,DiagnosisSequence = ClinicalCoding.SortOrder
		,DiagnosisDate = Cast(ClinicalCoding.ClinicalCodingTime as Date)
		,UserCreate = Creator.UserName
		,CreateDate = Cast(ClinicalCoding.PASCreatedDate As Date)
		,CreatedTime = CAST(ClinicalCoding.PASCreatedDate as time(0))
		,CreatedDateTime = ClinicalCoding.PASCreatedDate
		,UserModifed = Modifier.UserName
		,Modifiedate = Cast(ClinicalCoding.PASModifiedDate As Date)
		,ModifiedTime = CAST(ClinicalCoding.PASModifiedDate as time(0))
		,ModifiedDateTime = ClinicalCoding.PASModifiedDate
		,[Counter] = 1

from WHOLAP.dbo.BaseClinicalCoding ClinicalCoding
inner join WHOLAP.dbo.BaseAPC Episode
	on ClinicalCoding.SourceRecno = Episode.SourceUniqueID
Left outer join WH.PAS.CodingBase Coding
	on ClinicalCoding.ClinicalCodingCode = Coding.ODPCD_REFNO
left outer join WHOLAP.dbo.olapLorenzoUser Creator
	on ClinicalCoding.PASCreatedByWhom = Creator.UserUniqueID
left outer join WHOLAP.dbo.olapLorenzoUser Modifier
	on ClinicalCoding.PASUpdatedByWhom= Modifier.UserUniqueID
Where 
	ClinicalCoding.SourceCode = 'PRCAE' 
	and ClinicalCoding.ClinicalCodingTypeCode = 'DIAGN'	


	select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

	select @Stats = 
		'Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

	exec WriteAuditLogEvent 'PAS - WHREPORTING BuildAPCAllDiagnosis', @Stats, @StartTime