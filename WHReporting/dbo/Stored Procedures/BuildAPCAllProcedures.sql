﻿CREATE Procedure [dbo].[BuildAPCAllProcedures] as 

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

/*
Create Table APC.AllProcedures (
EncounterRecno int not null
,EpisodeSourceUniqueID int not null
,ClinicalCodingKey int not null
,ProcedureCode varchar(20) null
,[Procedure] varchar(255) null
,ProcedureSequence int not null
,ProcedureDate date
,UserCreate varchar(70)
,CreateDate Date
,CreateTime time(0)
,CreateDateTime datetime
,UserModified varchar(70)
,ModifedDate Date
,ModifiedTime Time(0)
,ModifiedDateTime datetime
,CONSTRAINT [pk_APCAllProcedures] PRIMARY KEY CLUSTERED

	(
	 EncounterRecno
	,ProcedureSequence
	,ClinicalCodingKey
	)
)

Create Index ixProcedureDate on APC.AllProcedures(ProcedureDate)
Create Index ixProcedureCreateDate on APC.AllProcedures(CreateDate)
Create Index ixProcedureCreateTime on APC.AllProcedures(CreateTime)
Create Index ixProcedureUserCreate on APC.AllProcedures(UserCreate)
Create Index ixClinicalCodingKey on APC.AllProcedures(ClinicalCodingKey)
Create Index ixProcedureCode on APC.AllProcedures(ProcedureCode)
Create Index ixProcedureSequence on APC.AllProcedures(ProcedureSequence)
*/

Truncate Table APC.AllProcedures

Insert into APC.AllProcedures  (
		 EncounterRecno
		,EpisodeSourceUniqueID
		,ClinicalCodingKey
		,ProcedureCode
		,[Procedure]
		,ProcedureSequence
		,ProcedureDate
		,UserCreate
		,CreateDate
		,CreateTime
		,CreateDateTime
		,UserModified
		,ModifedDate
		,ModifiedTime
		,ModifiedDateTime
		,[Counter]
)

Select  
Episode.EncounterRecno
,Episode.SourceUniqueID
,ClinicalCodingKey = ClinicalCoding.ClinicalCodingCode
,ProcedureCode= Coding.Code
,[Procedure] = Coding.DESCRIPTION
,ProcedureSequence = ClinicalCoding.SortOrder
,ProcedureDate = Cast(ClinicalCoding.ClinicalCodingTime as Date)
,UserCreate = Creator.UserName
,CreateDate = Cast(ClinicalCoding.PASCreatedDate As Date)
,CreatedTime = CAST(ClinicalCoding.PASCreatedDate as time(0))
,CreatedDateTime = ClinicalCoding.PASCreatedDate
,UserModifed = Modifier.UserName
,Modifiedate = Cast(ClinicalCoding.PASModifiedDate As Date)
,ModifiedTime = CAST(ClinicalCoding.PASModifiedDate as time(0))
,ModifiedDateTime = ClinicalCoding.PASModifiedDate
,[Counter] = 1
from WHOLAP.dbo.BaseClinicalCoding ClinicalCoding
inner join WHOLAP.dbo.BaseAPC Episode
	on ClinicalCoding.SourceRecno = Episode.SourceUniqueID
Left outer join WH.PAS.CodingBase Coding
	on ClinicalCoding.ClinicalCodingCode = Coding.ODPCD_REFNO
left outer join WHOLAP.dbo.olapLorenzoUser Creator
	on ClinicalCoding.PASCreatedByWhom = Creator.UserUniqueID
left outer join WHOLAP.dbo.olapLorenzoUser Modifier
	on ClinicalCoding.PASUpdatedByWhom= Modifier.UserUniqueID
Where 
	ClinicalCoding.SourceCode = 'PRCAE' 
	and ClinicalCoding.ClinicalCodingTypeCode = 'PROCE'	


	select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

	select @Stats = 
		'Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

	exec WriteAuditLogEvent 'PAS - WHREPORTING BuildAPCAllProcedures', @Stats, @StartTime