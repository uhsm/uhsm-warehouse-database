﻿CREATE procedure [dbo].[BuildLKConsultantSpecialty] as


/*----------------------------------------------------------------------------------------------------------
Created/Modified Date		Created/Modified By		Details
---------------------		-------------------		---------------
Modified 11/09/2014			K Oakden				Added update to include seperate forename / surname
-------------------------------------------------------------------------------------------------------------*/


truncate table LK.ConsultantSpecialty

insert into LK.ConsultantSpecialty

(ConsultantCode
,ConsultantName
,SpecialtyFunctionCode
,SpecialtyCode
,SpecialtyCodeMain
,LatestActivity
,EarliestActivity)

select
i.consultantcode,
i.ConsultantName
,i.[Specialtycode(Function)]
,i.SpecialtyCode
,i.[SpecialtyCode(Main)]
,case when MAX(episodeenddate)>GETDATE() then GETDATE() else MAX(EpisodeEndDate) end as LatestActivity
,MIN(i.EpisodeStartDate)
  FROM [WHREPORTING].[APC].[Episode] i
  
  group by
  i.consultantcode,
i.ConsultantName
,i.[Specialtycode(Function)]
,i.SpecialtyCode
,i.[SpecialtyCode(Main)]
   
  union all
  
  select
o.ProfessionalCarercode,
o.ProfessionalCarerName
,o.[SpecialtyCode(Function)]
,SpecialtyCode
,[SpecialtyCode(Main)]
,case when MAX(AppointmentDate)>GETDATE() then GETDATE() else MAX(appointmentdate) end
,case when MIN(appointmentdate)>GETDATE() then GETDATE() else MIN(appointmentdate) end
  FROM [WHREPORTING].[op].[schedule] o
  
  group by
o.ProfessionalCarercode,
o.ProfessionalCarerName
,o.[SpecialtyCode(Function)]
,SpecialtyCode
,[SpecialtyCode(Main)]

--Add forename / Surname
update LK.ConsultantSpecialty 
set Forename = cons.Forename, 
	Surname = cons.Surname
from LK.ConsultantSpecialty lk
left join [WH].[PAS].[Consultant] cons
	on lk.ConsultantCode = cons.NationalConsultantCode
	and cons.LocalFlag = 'Y'
	and ParentHealthOrganisation = 'RM2'