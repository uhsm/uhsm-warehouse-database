﻿CREATE procedure [dbo].[BuildLKPasPCTOdsCCG] as

/*
--Author: K Oakden
--Date created: 21/05/2013

SP used to populate PCT / CCG lookup used in several build dataset sp's
Temporary workaround in absence of complete CCG list available in PAS
This table is a combination of CCG data from ODS lookup and other (non-english)
Organisation data from PAS.
*/

--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[LK].[PasPCTOdsCCG]') AND type in (N'U'))
--DROP TABLE [LK].[PasPCTOdsCCG]
truncate table WHREPORTING.LK.PasPCTOdsCCG

insert into WHREPORTING.LK.PasPCTOdsCCG
select PCTCCGCode, max(PCTCCG) as PCTCCG
--into WHREPORTING.LK.PasPCTOdsCCG
from

(
select distinct CCGName as PCTCCG, CCGCode as PCTCCGCode
from WHOLAP.dbo.OlapPASPracticeCCG where CCGCode <> 'N/A'
union
select distinct PCTName, PCTCode
from WHOLAP.dbo.OlapPASPracticeCCG where PCTCode <> 'N/A'
) pctccg
group by  PCTCCGCode