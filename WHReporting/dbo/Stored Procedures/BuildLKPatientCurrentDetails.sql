﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		Build PatientCurrentDetails.

Notes:			Stored in WHREPORTING.
				Build table PatientCurrentDetails to replace tbl_Mpi from infosql

Versions:
				1.0.0.3 - 26/10/2015 - MT
					Use the revised table PatientAddressHistory.

				1.0.0.2 - 21/10/2015 - MT
					Use the revised table PatientGPHistory.

				1.0.0.1 - 30/01/2015 - KO
					Join to ReferenceValue tables to the demographic descriptions
					eg Title, Marital status etc. Because latest IPM patient extract does not contain 
					the descriptive values. Note that when the old patient extract is no longer in use
					the missing patient insert should no longer be necessary.

				1.0.0.0 - 17/06/2014 - CM
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [dbo].[BuildLKPatientCurrentDetails]
AS

Set NoCount On

Declare @StartTime datetime
Declare @Elapsed int
Declare @Stats varchar(255)

Select @StartTime = getdate()

-- Load
Truncate table WHReporting.LK.PatientCurrentDetails

;With MyDistrictNo As (
	Select	PatientNo = src.PATNT_REFNO
			,DistricyNo = src.IDENTIFIER
			,ROW_NUMBER() Over (Partition By src.PATNT_REFNO Order By src.START_DTTM Desc,src.PATID_REFNO Desc) As RowNum
	From	Lorenzo.dbo.PatientIdentifier src With (NoLock)
	Where	src.ARCHV_FLAG = 'N'
			And src.IDENTIFIER Like 'RM2%'
			And src.PITYP_REFNO = 2001232 -- Patient ID (Facility)
	)

Insert Into WHReporting.LK.PatientCurrentDetails

Select Distinct 
	pat.PATNT_REFNO as SourcePatientNo
	, DistrictNo.DistricyNo as FacilityID
	, pat.PATNT_REFNO_NHS_IDENTIFIER as NHSNo
	, sexxx.[DESCRIPTION] as Sex
	, title.[DESCRIPTION] as PatientTitle
	, pat.FORENAME as PatientForename
	, pat.SURNAME as PatientSurname
	, ethgr.[DESCRIPTION] as EthnicGroup
	, pat.DTTM_OF_BIRTH as DateOfBirth
	, pat.DECSD_FLAG as DeceasedFlag
	, pat.DTTM_OF_DEATH as DateOfDeath
	, relig.[DESCRIPTION] as Religion
	, marry.[DESCRIPTION] as MaritalStatus
	, natnl.[DESCRIPTION] as Nationality
	, spokl.[DESCRIPTION]as SpokenLanguage
	, ad.Address1
	, ad.Address2
	, ad.Address3
	, ad.Address4
	, ad.Postcode
	, ad.CountryCode
	, ad.CountryDescription
	, phone.HomePhone
	, phone.MobilePhone
	, gp.GPCode as CurrentRegisteredGPCode
	, GPDetails.[Organisation Name] as CurrentRegisteredGPName
	, gp.practicerefno CurrentRegisteredGPRefno
	, gp.practicecode as CurrentRegisteredPracticeCode
	, GPPractice.[Organisation Name] as CurrentRegisteredGPPractice
	, GPDetails.[Address Line 1] as GPPracticeAddress1
	, GPDetails.[Address Line 2] as GPPracticeAddress2
	, GPDetails.[Address Line 3] as GPPracticeAddress3
	, GPDetails.[Address Line 4] as GPPracticeAddress4
	, GPDetails.[Address Line 5] as GPPracticeAddress5
	, GPDetails.[Postcode] as GPPracticePostcode
	, GPPractice.[Parent Organisation Code] as CCGCodeOfGPPractice
	, CCG.[Organisation Name] as CCGNameofPractice
	, pat.NNNTS_CODE as NHSNumberStatusCode

From	Lorenzo.dbo.Patient pat

	Left Join  WH.PAS.PatientAddressHistory ad
		on pat.PATNT_REFNO = ad.PatientNo
		And ad.CurrentFlag = 'Y'

	Left Join WH.PAS.PatientGPHistory GP 
		on pat.PATNT_REFNO = gp.patientno
		And GP.CurrentFlag = 'Y'

	Left Join OrganisationCCG.dbo.[General Medical Practitioner] GPDetails 
		on GP.GPCode = GPDetails.[Organisation Code]

	Left Join OrganisationCCG.dbo.[General Medical Practice] GPPractice 
		on GP.practicecode = GPPractice.[Organisation Code]

	Left Join OrganisationCCG.dbo.[Primary Care Organisation] CCG 
		on GPPractice.[Parent Organisation Code] = CCG.[Organisation Code]

	Left Join  MyDistrictNo DistrictNo
		on pat.PATNT_REFNO = DistrictNo.PatientNo
		And DistrictNo.RowNum = 1

	Left Join WH.PAS.PatientPhoneNumber phone 
		on pat.PATNT_REFNO = phone.SourcePatientNo 

	Left Join Lorenzo.dbo.ReferenceValue title
		on title.RFVAL_REFNO = pat.TITLE_REFNO

	Left Join Lorenzo.dbo.ReferenceValue ethgr
		on ethgr.RFVAL_REFNO = pat.ETHGR_REFNO

	Left Join Lorenzo.dbo.ReferenceValue marry
		on marry.RFVAL_REFNO = pat.MARRY_REFNO

	Left Join Lorenzo.dbo.ReferenceValue sexxx
		on sexxx.RFVAL_REFNO = pat.SEXXX_REFNO

	Left Join Lorenzo.dbo.ReferenceValue relig
		on relig.RFVAL_REFNO = pat.RELIG_REFNO

	Left Join Lorenzo.dbo.ReferenceValue natnl
		on natnl.RFVAL_REFNO = pat.NATNL_REFNO

	Left Join Lorenzo.dbo.ReferenceValue spokl
		on spokl.RFVAL_REFNO = pat.SPOKL_REFNO

Where	pat.ARCHV_FLAG = 'N' 
		And pat.MERGE_MINOR_FLAG = 'N'

--Tidy up MPI by removing all records which do not have a RM2 number match
Delete	src
From	WHReporting.LK.PatientCurrentDetails src
Where	src.FacilityId is null

--Insert records from WH.PAS.MissingPatient into the MPI table
--These are patient records for patients who have had activity (OP, OP,Referrals) but for some reason do not appear in the Lorenzo.dbo.Patient table with any demographic details.
--The missing patients table is generated by looking in each of the activity tables for any patients which do no appear in the Lorenzo.dbo.Patient table and bilds a list of these, then goes to look in the LPI (which are messges from PAS to the interface) to see if it can get the demographic details and populated the missing patients table with this information.
--the reason the details may be in the LPI but not in Lorenzo.dbo.Patient is due to a glitch in the extracts which does not affect the LPI interface transactions
--Need to insert these records into the MPI table to complete the set of patients
Insert into WHReporting.LK.PatientCurrentDetails(
	SourcePatientNo
	,FacilityID
	,NHSNo
	,Sex
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,CurrentRegisteredGPCode
	,CurrentRegisteredGPName
	,CurrentRegisteredPracticeCode
	,CurrentRegisteredGPPractice
	,GPPracticeAddress1
	,GPPracticeAddress2
	,GPPracticeAddress3
	,GPPracticeAddress4
	,GPPracticeAddress5
	,GPPracticePostcode
	,CCGCodeOfGPPractice
	,CCGNameofPractice
	,NHSNumberStatusCode
	)
Select
	src.SourcePatientNo
	,FacilityID = src.LocalPatientID
	,NHSNo = src.NhsNumber
	,src.Sex
	,src.PatientForename
	,src.PatientSurname
	,DOB = cast(src.DOB as datetime)
	,src.GPCode
	,GPDetails.[Organisation Name]
	,src.PracticeCode
	,GPPractice.[Organisation Name] AS CurrentRegisteredGPPractice
	,GPDetails.[Address Line 1]
	,GPDetails.[Address Line 2]
	,GPDetails.[Address Line 3]
	,GPDetails.[Address Line 4]
	,GPDetails.[Address Line 5]
	,GPDetails.[Postcode]
	,GPPractice.[Parent Organisation Code]
	,CCG.[Organisation Name]
	,src.NHSNumberStatusCode
From	WH.PAS.MissingPatient src

		-- Exclude existing records
		Left Join WHReporting.LK.PatientCurrentDetails ex
			On src.SourcePatientNo = ex.SourcePatientNo

		Left Join OrganisationCCG.dbo.[General Medical Practitioner] GPDetails
			On src.GPCode = GPDetails.[Organisation Code]

		Left Join OrganisationCCG.dbo.[General Medical Practice] GPPractice
			On src.practicecode = GPPractice.[Organisation Code]

		Left Join OrganisationCCG.dbo.[Primary Care Organisation] CCG
			On GPPractice.[Parent Organisation Code] = CCG.[Organisation Code] 

Where  Archived = 'N'
		And ex.SourcePatientNo Is Null

Select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

Select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

Exec WriteAuditLogEvent 'PAS - WHREPORTING BuildLKPatientCurrentDetails', @Stats, @StartTime