﻿CREATE procedure [dbo].[BuildPASReferenceValue] as

/**
KO 23/05/2014
Dynamic table to create reduced ref value table to improve performance of
OP.BuildSchedule
Will replace the vwPAS... lookup views
**/
if Object_id('tempdb..#ReferenceValueCheck', 'U') is not null
	drop table #ReferenceValueCheck

CREATE TABLE #ReferenceValueCheck(
	[rfval] [int] NULL
	--[domain] [varchar](10) NOT NULL,
	--[src] [varchar](20) NOT NULL
) 

	--insert into #ReferenceValueCheck 
	--select distinct SourceOfReferralCode as rfval, 'SORRF' as domain, 'WH.RF.Encounter' as src
	--from WH.RF.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct SourceOfReferralCode, 'SORRF', 'WH.OP.Encounter'
	--from WH.OP.Encounter

	insert into #ReferenceValueCheck 
	select distinct AdminCategoryCode--, 'ADCAT'--, 'WH.OP.Encounter'
	from WH.OP.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct AdmissionMethodCode, 'ADMET', 'WH.APC.Encounter'
	--from WH.APC.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct AdmissionSourceCode, 'ADSOR', 'WH.APC.Encounter'
	--from WH.APC.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct DischargeDestinationCode, 'DISDE', 'WH.APC.Encounter'
	--from WH.APC.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct DischargeMethodCode, 'DISMT', 'WH.APC.Encounter'
	--from WH.APC.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct EthnicOriginCode, 'ETHGR', 'WH.APC.Encounter'
	--from WH.APC.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct EthnicOriginCode, 'ETHGR', 'WH.OP.Encounter'
	--from WH.OP.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct EthnicOriginCode, 'ETHGR', 'WH.RF.Encounter'
	--from WH.RF.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct ManagementIntentionCode, 'INMGT', 'WH.APC.Encounter'
	--from WH.APC.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct MaritalStatusCode, 'MARRY', 'WH.APC.Encounter'
	--from WH.APC.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct MaritalStatusCode, 'MARRY', 'WH.OP.Encounter'
	--from WH.OP.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct MaritalStatusCode, 'MARRY', 'WH.RF.Encounter'
	--from WH.RF.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct refval.ReferenceValueCode, 'NNNTS', 'WH.APC.Encounter'
	----,enc.NHSNumberStatusCode
	--from WH.APC.Encounter enc
	--inner join WH.PAS.ReferenceValue refval 
	--on refval.MainCode = enc.NHSNumberStatusCode
	--and refval.ReferenceDomainCode = 'NNNTS'

	--insert into #ReferenceValueCheck 
	--select distinct refval.ReferenceValueCode, 'NNNTS', 'WH.OP.Encounter'
	----,enc.NHSNumberStatusCode
	--from WH.OP.Encounter enc
	--inner join WH.PAS.ReferenceValue refval 
	--on refval.MainCode = enc.NHSNumberStatusCode
	--and refval.ReferenceDomainCode = 'NNNTS'

	--insert into #ReferenceValueCheck 
	--select distinct OverseasStatusFlag, 'OVSVS', 'WH.APC.Encounter'
	--from WH.APC.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct OverseasStatusFlag, 'OVSVS', 'WH.OP.Encounter'
	--from WH.OP.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct CANRS_REFNO, 'CANRS', 'Lorenzo.dbo.Referral'
	--from Lorenzo.dbo.Referral

	--insert into #ReferenceValueCheck 
	--select distinct CLORS_REFNO, 'CLORS', 'Lorenzo.dbo.Referral'
	--from Lorenzo.dbo.Referral

	--insert into #ReferenceValueCheck 
	--select distinct RFMED_REFNO, 'RFMED', 'Lorenzo.dbo.Referral'
	--from Lorenzo.dbo.Referral

	--insert into #ReferenceValueCheck 
	--select distinct PriorityCode, 'PRITY', 'WH.RF.Encounter'
	--from WH.RF.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct REASN_REFNO, 'REASN', 'Lorenzo.dbo.Referral'
	--from Lorenzo.dbo.Referral

	--insert into #ReferenceValueCheck 
	--select distinct ACTYP_REFNO, 'ACTYP', 'Lorenzo.dbo.Referral'
	--from Lorenzo.dbo.Referral


	--insert into #ReferenceValueCheck 
	--select distinct ReligionCode, 'RELIG', 'WH.APC.Encounter'
	--from WH.APC.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct ReligionCode, 'RELIG', 'WH.OP.Encounter'
	--from WH.OP.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct ReligionCode, 'RELIG', 'WH.RF.Encounter'
	--from WH.RF.Encounter

	----delete from #ReferenceValueCheck where domain = 'RTTST'
	--insert into #ReferenceValueCheck 
	--select distinct RTTCurrentStatusCode, 'RTTST', 'WH.APC.Encounter'
	--from WH.APC.Encounter
	--union select distinct RTTPeriodStatusCode, 'RTTST', 'WH.APC.Encounter'
	--from WH.APC.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct RTTCurrentStatusCode, 'RTTST', 'WH.OP.Encounter'
	--from WH.OP.Encounter
	--union select distinct RTTPeriodStatusCode, 'RTTST', 'WH.OP.Encounter'
	--from WH.OP.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct RTTCurrentStatusCode, 'RTTST', 'WH.RF.Encounter'
	--from WH.RF.Encounter

	insert into #ReferenceValueCheck 
	select distinct AppointmentStatusCode--, 'ATTND', 'WH.OP.Encounter'
	from WH.OP.Encounter

	insert into #ReferenceValueCheck 
	select distinct CancelledByCode--, 'CANCB', 'WH.OP.Encounter'
	from WH.OP.Encounter

	insert into #ReferenceValueCheck 
	select distinct DisposalCode--, 'SCOCM', 'WH.OP.Encounter'
	from WH.OP.Encounter

	insert into #ReferenceValueCheck 
	select distinct ClinicType--, 'PRTYP', 'WH.OP.Encounter'
	from WH.OP.Encounter

	insert into #ReferenceValueCheck 
	select distinct FirstAttendanceFlag--, 'VISIT', 'WH.OP.Encounter'
	from WH.OP.Encounter
	
	insert into #ReferenceValueCheck 
	select distinct RTTCurrentStatusCode--, 'VISIT', 'WH.OP.Encounter'
	from WH.OP.Encounter
	
	insert into #ReferenceValueCheck 
	select distinct RTTPeriodStatusCode--, 'VISIT', 'WH.OP.Encounter'
	from WH.OP.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct SexCode, 'SEXXX', 'WH.APC.Encounter'
	--from WH.APC.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct SexCode, 'SEXXX', 'WH.OP.Encounter'
	--from WH.OP.Encounter

	--insert into #ReferenceValueCheck 
	--select distinct SexCode, 'SEXXX', 'WH.RF.Encounter'
	--from WH.RF.Encounter

--select rfval, count(1) as cnt from (
--select distinct rfval, domain from #ReferenceValueCheck
--)a
--group by rfval
--having COUNT(1) > 1

--select * from #ReferenceValueCheck where rfval in (45, 5477)
--select * from WH.PAS.ReferenceValue where ReferenceValueCode in (45, 5477)

truncate table LK.PASReferenceValue
insert into LK.PASReferenceValue
select distinct 
	ref.ReferenceValueCode
	,ref.ReferenceDomainCode
	,ref.ReferenceValue
	,ref.MainCode
	,ref.ArchiveFlag
	,ref.MappedCode 
--into LK.PASReferenceValue
from WH.PAS.ReferenceValue ref
inner join #ReferenceValueCheck incl
	on incl.rfval = ref.ReferenceValueCode