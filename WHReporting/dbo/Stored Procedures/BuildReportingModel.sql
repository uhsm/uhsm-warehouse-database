﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:	Build Reporting Model

Notes:		Stored in WHREPORTING

Versions:		
			1.0.1.0 - 20/01/2016 - MT
				Added call to OP.BuildClinicalCoding

			1.0.0.9 - 07/12/2015 - MT
				Added call to PTL.BuildReportFUPB.

			1.0.0.8 - 03/12/2015 - MT - Job 198
				Added call to usp_LoadOpenReferral.

			1.0.0.7 - 20/07/2015 - CM
				Added stored procedure QLIK.InsertIntoMedicalOutliers to insert 
				medical outliers into table for Qlikview Patient Flow dashboard
				to pull from.

			1.0.0.6 = 05/05/2015 - CM
				Added stored procedure BLOODTRACK.BuildSummaryFatedUnfatedTransfusionsTable 
				to build a Transfusion table for Qlikview Divisional Dashboards to pull from.
	
			1.0.0.5 - 24/09/2014 - JB
				Added stored procedure WHReporting.DQ.InvalidPostcodes to run daily.

			1.0.0.4 - 10/09/2014 - KO
				Added stored procedure DM01.BuildElectiveActivity to run daily.
				Archive stored proc not required.

			1.0.0.3 - 21/08/2014 - KO
				Added stored procedure DM01.ArchiveElectiveActivity to run on 1st of every month.

			1.0.0.2 - 10/04/2014 - CM
				Added stored procedure QLIK.BuildBedStatusAndOccupancy to 
				create tables required for Qlikview LOS report

			1.0.0.1 - 17/03/2014 - CM
				Added stored procedure CN.BuildCasenoteCurrentLocationDataset to 
				create CasenoteCurrentLocationDataset

			1.0.0.0 - ??/??/???? - ??
				Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [dbo].[BuildReportingModel]
As

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

if --only execute if the build model has run and this process has not previously run
	(
	select
		Process =
			CONVERT(
				bit
				,case
				when
					(
					select
						max(DateValue)
					from
						WH.dbo.Parameter
					where
						Parameter in
							(
							 'BUILDMODELDATE'
							)
					) > 
					(
					select
						DateValue
					from
						WH.dbo.Parameter
					where
						Parameter = 'BUILDWHREPORTINGDATE'
					)
				then 1
				else 0
				end
			)
	) = 1

begin
	exec dbo.BuildLKPasPCTOdsCCG
	exec APC.BuildEpisode
	exec APC.BuildSpell
	exec APC.BuildWardStay
	exec APC.BuildPatient
	exec APC.BuildClinicalCoding
	exec APC.BuildCriticalCarePeriod
	exec APC.BuildReferralToTreatment
	exec dbo.BuildAPCAllDiagnosis
	exec dbo.BuildAPCAllProcedures
	exec OP.BuildSchedule
	exec OP.BuildPatient
	exec OP.BuildReferralToTreatment
	exec OP.BuildSessionTemplateDetail
	exec OP.BuildBookingFact_Session
	exec OP.BuildClinicalCoding
	exec RF.BuildReferral
	exec RF.BuildPatient
	exec RF.BuildReferralToTreatment--
	exec dbo.BuildSpecialtyDivision
	exec CN.BuildCasenoteCurrentLocationDataset-- stored procedure to create CasenoteCurrentLocationDataset to replicate the CaasenoteLocationDataset on InfoSQL
	--exec QLIK.BuildBedStatusAndOccupancy --stored procedure added to create tables required for Qlikview LOS reporting
	exec WH.LK.BuildPatientCurrentDetails --17/06/2014  CM - added this to create the PatientcurrentDetails table
	exec DQ.BuildInvalidPostcodes -- 24/09/2014 JB - added this to create the InvalidPostcodes table
	--comment IDS stored procedure below from main build for now CM 27/02/2015
	--exec IDS.BuildIDSReportingTable --17/06/2014 CM 
	
	--if(datepart(DAY, GETDATE()) = 1)--21/08/2014 KO - Archive diagnostic elective activity on first of month for DM01 return
	--	begin
	--		exec DM01.ArchiveElectiveActivity
	--	end
	exec DM01.BuildElectiveActivity --10/09/2014 KO 
	
	exec BLOODTRACK.BuildSummaryFatedUnfatedTransfusionsTable--05/05/2015 CM - added this stored procedure to build a Transfusion table for Qlikview Divisional Dashboards to pull from 
	
	exec QLIK.InsertIntoMedicalOutliers --20/07/2015 CM added this stored procedure to insert medical outliers into table for Qlikview Patient Flow dashboard to pull from - addition comments in actual stored procedure for info
  
	Exec WH.PTL.BuildReportFUPB	
	
	Exec APC.[BuildElectiveAdmissionMRSAScreens]

	--store build time
	update WH.dbo.Parameter
	set
		DateValue = GETDATE()
	where
		Parameter = 'BUILDWHREPORTINGDATE'

	if @@rowcount = 0

	insert into WH.dbo.Parameter
		(
		 Parameter
		,DateValue
		)
	select
		 Parameter = 'BUILDWHREPORTINGDATE'
		,DateValue = GETDATE()

	select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

	select @Stats = 
		'Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

	exec WriteAuditLogEvent 'PAS - WHREPORTING BuildReportingModel Total', @Stats, @StartTime

end