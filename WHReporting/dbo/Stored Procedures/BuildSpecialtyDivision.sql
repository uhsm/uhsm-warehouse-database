﻿CREATE Procedure [dbo].[BuildSpecialtyDivision] as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

Truncate table lk.SpecialtyDivision

Insert into LK.SpecialtyDivision 
	(
	 SpecialtyRefno
	,SpecialtyCode
	,Specialty
	,SpecialtyFunctionCode
	,SpecialtyFunction
	,MainSpecialtyCode
	,MainSpecialty
	,RTTSpecialtyCode
	,RTTExcludeFlag
	,Direcorate
	,Division
	)

Select 
	 SpecialtyCode
	,SubSpecialtyCode
	,SubSpecialty
	,SpecialtyFunctionCode
	,SpecialtyFunction
	,MainSpecialtyCode
	,MainSpecialty
	,RTTSpecialtyCode
	,RTTExcludeFlag
	,Direcorate
	,Division
From WHOLAP.dbo.SpecialtyDivisionBase


	select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

	select @Stats = 
		'Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

	exec WriteAuditLogEvent 'PAS - WHREPORTING BuildSpecialtyDivision', @Stats, @StartTime