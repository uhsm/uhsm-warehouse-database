﻿CREATE Procedure [dbo].[BuildWHReporting] as



IF
	(
	select DateValue from WH.dbo.Parameter
	where Parameter = 'BUILDMODELDATE'
	) > 
	(
	select DateValue from WH.dbo.Parameter
	where Parameter = 'UpdateWHReporting'

	)
begin

exec APC.BuildEpisode
exec APC.BuildSpell
exec APC.BuildWardStay
exec APC.BuildPatient
exec APC.BuildClinicalCoding
exec APC.BuildCriticalCarePeriod
exec APC.BuildReferralToTreatment
exec dbo.BuildAPCAllDiagnosis
exec dbo.BuildAPCAllProcedures

exec OP.BuildSchedule
exec OP.BuildPatient
exec OP.BuildReferralToTreatment
exec OP.BuildSessionTemplateDetail
exec OP.BuildBookingFact_Session

exec RF.BuildReferral
exec RF.BuildPatient
exec RF.BuildReferralToTreatment

exec dbo.BuildSpecialtyDivision



UPdate WH.dbo.Parameter
Set DateValue = GETDATE()	
where Parameter = 'UpdateWHReporting'




End