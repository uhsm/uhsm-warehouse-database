﻿Create  Procedure [dbo].[BuildWHReporting_APC] as

exec APC.BuildEpisode;
exec APC.BuildSpell;
exec APC.BuildPatient;
exec APC.BuildClinicalCoding;
exec APC.BuildCriticalCarePeriod;
exec APC.BuildReferralToTreatment;
exec dbo.BuildAPCAllDiagnosis;
exec dbo.BuildAPCAllProcedures;

--exec OP.BuildSchedule;
--exec OP.BuildPatient;
--exec OP.BuildReferralToTreatment;
--exec OP.BuildSessionTemplateDetail;
--exec OP.BuildBookingFact_Session;

--exec RF.BuildReferral;
--exec RF.BuildPatient;
--exec RF.BuildReferralToTreatment;

exec dbo.BuildSpecialtyDivision;