﻿CREATE Procedure [dbo].[Casenote_Location_report]
	(
	@StartDate  datetime
	,@EndDate datetime
	,@Specialty varchar(255)
	)
	as

/*
Declare @StartDate datetime = '20141101'
,@EndDate datetime = '20151107'
,@Specialty varchar(255) = 'General Surgery'
*/
SELECT 
      p.PatientForename
      ,p.PatientSurname
      ,ops.[SourceUniqueID]
      ,ops.[FacilityID]
      ,ops.[NHSNo]
      ,ops.[AppointmentDateTime]
      ,ops.ProfessionalCarerCode
      ,ops.ProfessionalCarerName
      ,ops.[Division]
      ,ops.[Directorate]
      ,ops.[SpecialtyCode(Function)]  as SpecialtyFunctionCode
      /*Preop custom Specialty added as requested by AO*/
      ,case when ops.ClinicCode in (
	    'GSUPREOP'
		,'PLASTICPREOP'
		,'LAPREOP'
		,'MAXFAXPREOP'
		,'UROPREOP'
		,'KNEEPREOP'
		,'HIPPREOP'
		,'ORTHOPREOP'
		,'VASPREOP'
		,'ENTPREOPWCH'
		,'ORTHOPREOPWCH'
		,'RADPREOP'
		,'ANAESPREOP'
		,'THYROIDPREOP'
		,'ENDOPREOP      '
		,'ENTPREOP'
		,'EVLTPREOP'
		,'PACPMO'
		,'TDCPLAPREOP'
		,'TELEPREOP'
		,'WCHPO'
		,'PREOPUROL'
		,'PREOPWCH'
        )               then 'Pre-op'
       else ops.[Specialty(Function)] 
       end  as SpecialtyFunction
      ,ops.ClinicCode
      ,ccl.Location
      ,ccl.LocationCode
      ,Case when ccl.Location in (
			'ARCHIVE 5200'
			,'LIBRARY ARCHIVE 5200'
			,'PENDING 5200'
			,'SECONDARY LIBRARY 5200'
			,'HEALTH RECORD LIBRARY 5200'
			,'OPD PREP 5152'
			,'CHEST CLINIC 2852'
			,'WCH-PREP IH 5194'
			,'MAX FAX PREP 5167'
			,'PLASTICS 5154'
			,'PAIN 5156' 
			)
		   then 'Locations Excluded' 
		   else  'Locations Requested'
		   end                       as Excludedlocation 
From [WHREPORTING].[OP].[Schedule] ops 
	  Left Join WHReporting.CN.CasenoteCurrentLocationDataset ccl 
	  on ops.SourcePatientNo = ccl.Patnt_refno
	  Left Join [WHREPORTING].[OP].[Patient] p
	  on ops.SourcePatientNo = p.SourcePatientNo and ops.SourceUniqueID = p.SourceUniqueID
Where ops.[AppointmentDate] between @StartDate and @EndDate
   and ops.AttendStatus Not like '%Cancelled%' and ops.AttendStatus not like '%Unable%'
   and case when ops.ClinicCode in (
	    'GSUPREOP'
		,'PLASTICPREOP'
		,'LAPREOP'
		,'MAXFAXPREOP'
		,'UROPREOP'
		,'KNEEPREOP'
		,'HIPPREOP'
		,'ORTHOPREOP'
		,'VASPREOP'
		,'ENTPREOPWCH'
		,'ORTHOPREOPWCH'
		,'RADPREOP'
		,'ANAESPREOP'
		,'THYROIDPREOP'
		,'ENDOPREOP      '
		,'ENTPREOP'
		,'EVLTPREOP'
		,'PACPMO'
		,'TDCPLAPREOP'
		,'TELEPREOP'
		,'WCHPO'
		,'PREOPUROL'
		,'PREOPWCH'
        )               then 'Pre-op'
       else ops.[Specialty(Function)] 
       end  = @Specialty