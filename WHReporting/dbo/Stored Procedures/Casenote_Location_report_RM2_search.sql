﻿CREATE Procedure [dbo].[Casenote_Location_report_RM2_search]
	(
	@Districtno varchar(max)
	)
	as

/*
Declare 
@DistrictNo varchar(max) = 'RM24287416,RM24447468,RM24447461,RM24447923,RM24423462,RM24441721,RM2438946,RM2325264,RM2445727,RM264074,RM24118421,RM24447925,RM24351827,RM2730720,RM21790666,RM24168382,RM21310587,RM24446888,RM2260464,RM24410376'
*/

SELECT Item as DistrictNo 
Into #paradist
FROM WHREPORTING.dbo.Split (@Districtno , ',')


SELECT distinct
      pd.DistrictNo
      ,RIGHT(p.FacilityID,2)   AS LOCCODE
      ,p.PatientSurname
      ,ccl.Patnt_refno
      ,ccl.CasenoteIdentifier
      ,ccl.Location
      ,ccl.Notes
      ,ccl.VolumeIdentifier
      ,Case when ccl.CasenoteIdentifier is null
       then 0 else 1  
       end                              as Returnedcount
FROM #paradist pd

--left join WH.PAS.PatientCurrentAddress p
--      on pd.DistrictNo = p.DistrictNo 

left join [WHREPORTING].[LK].[PatientCurrentDetails] p
      on pd.DistrictNo = p.FacilityID

Left join WHREPORTING.CN.CasenoteCurrentLocationDataset ccl
  on p.SourcePatientNo = ccl.Patnt_refno



--inner join       
--      (SELECT Item as DistrictNo FROM WHREPORTING.dbo.Split (@Districtno , ',')) a 
--      on a.DistrictNo = p.DistrictNo
--Where 
   --and ops.AttendStatus Not like '%Cancelled%' and ops.AttendStatus not like '%Unable%'
   --p.DistrictNo in (SELECT Item FROM WHREPORTING.dbo.Split (@Districtno , ','))

--Order by  p.DistrictNo
         
Drop table #paradist