﻿Create Procedure [dbo].[Casenote_Location_report_Specialty]
	(
	@StartDate  datetime
	,@EndDate datetime
	)
	as

/*
Declare @StartDate datetime = '20141101'
,@EndDate datetime = '20151107'
*/
/*Proc built for use in SSRS as filter for other proce dbo.Casenote_Location_report*/
/*xxxxxxxxxxxxxxxxxxxxxxxxxxx*/
Select Distinct
	ops.[Specialty(Function)]  as Specialty
into #spec
From [WHREPORTING].[OP].[Schedule] ops
Where ops.[AppointmentDate] between @StartDate and @EndDate

/*xxxxxxxxxxxxxxxxxxxxxxxxx*/
/*Insert in custom spec (based certain clinic codes */
insert into #spec
values ('Pre-op')

/*xxxxxxxxxxxxxxxxxxxxxxxxx*/
/*Results*/
Select * from #spec

/*xxxxxxxxxxxxxxxxxxxxxxxx*/
Drop table #spec