﻿CREATE Procedure [dbo].[Casenote_location_CodingDept]
as 

SELECT
      ccl.[Patnt_refno]
      ,ccl.[CasenoteIdentifier]
      ,pcd.FacilityID
      ,pcd.PatientSurname
      ,ccl.[Volume_count]
      ,ccl.[Location]
      ,ccl.[Notes]
      ,ccl.[Received]
      ,ccl.[VolumeIdentifier]
      ,ccl.[LocationCode]
      ,ccl.[RequestStatusDate]    
     ,DATEDIFF(dd,convert(Date,RequestStatusDate),convert(date,Getdate())) as Time_in_Dept
  FROM [WHREPORTING].[CN].[CasenoteCurrentLocationDataset] ccl
		Left Join [WHREPORTING].[LK].[PatientCurrentDetails] pcd on ccl.Patnt_refno = pcd.SourcePatientNo
  where [LocationCode] = '2461' and [Received] = 'Y'