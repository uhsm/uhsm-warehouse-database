﻿CREATE procedure [dbo].[CorrectCalendarYearEnd] as

/**
K Oakden
08/05/2014
Required to fix discrepancies in week related values in Calendar lookup
Similar fix appended to WHOLAP.dbo.BuildOlapCalendar to correct dbo.OlapCalendar
**/

declare 
	@firstday datetime,
	@wknum varchar(6),
	@NewWeekNoKey varchar(6),
	@NewWeekNo varchar(20),
	@NewFirstDayOfWeek datetime,
	@NewLastDayOfWeek datetime

--get all WeekNoKeys that fall at the end of the year --eg 201453
declare lastweek cursor for
	select
		WeekNoKey =  cast(DATEPART(YEAR, TheDate)as varchar) +  max(RIGHT(WeekNoKey, 2))
	from WHREPORTING.lk.Calendar
	group by DATEPART(YEAR, TheDate)
	order by DATEPART(YEAR, TheDate)

open lastweek
	fetch next from lastweek into @wknum
	while @@fetch_status = 0   
	begin  
		--Get first date for a given week 
		--plus the value of the WeekNo, FirstDayOfWeekand and LastDayOfWeek
		--parameters to apply to the whole week (correct at start of week)
		select 
			@firstday = TheDate,
			@NewWeekNo = WeekNo,
			@NewFirstDayOfWeek = FirstDayOfWeek,
			@NewLastDayOfWeek = LastDayOfWeek
		from WHREPORTING.lk.Calendar 
		where WeekNoKey = @wknum and DayOfWeekKey = 1
		
		--Get the value of the WeekNoKey parameter to apply to the whole week
		--(correct at end of week)
		select 
			@NewWeekNoKey = WeekNoKey
		from WHREPORTING.lk.Calendar 
		where TheDate = DATEADD (day, 6, @firstday)
		
		--update full week
		update WHREPORTING.lk.Calendar set
		--select
			WeekNo = @NewWeekNo,
			FirstDayOfWeek = @NewFirstDayOfWeek,
			LastDayOfWeek =@NewLastDayOfWeek,
			WeekNoKey = @NewWeekNoKey
		from WHREPORTING.lk.Calendar 
		where TheDate between @firstday and DATEADD (day, 6, @firstday)
		
	fetch next from lastweek into @wknum
	end   
close lastweek   
deallocate lastweek


--select top 100 * from WHREPORTING.lk.Calendar 
--where TheDate between '2014-12-20' and '2015-01-10'
--order by TheDate

--select * into LK.CalendarCopy from LK.Calendar