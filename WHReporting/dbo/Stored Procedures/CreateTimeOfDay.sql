﻿CREATE Procedure [dbo].[CreateTimeOfDay] as
/*
Create Table LK.TimeOfDay (
	TimeKey int Identity(1,1)
	,TimeValue Time(0)
	,TimeTextValue varchar(8)
	,HourValue char(2)
	,MinuteValue char(2)
	,HourBand varchar(13)
	,HalfHourBand varchar(5)
	,QuarterHourBand varchar(5)
	,PartOfDay varchar(50)

,Constraint [pk_TimeOfDay] PRIMARY KEY CLUSTERED 
	(TimeKey)
)

Create Index ixTheTime on LK.TimeOFDay(TimeValue)
Create Index ixHour on LK.TimeOFDay(HourValue)
Create Index ixMinute on LK.TimeOFDay(MinuteValue)
Create Index ixHourBand on LK.TimeOFDay(HourBand)

*/





Declare @Time datetime

Set @Time = '00:00:00'


While @Time <= '11:59:59 PM'

BEGIN

	Insert into LK.TimeOfDay 
	(
		 TimeValue
		,TimeTextValue
		,HourValue
		,MinuteValue
		,HourBand
		,HalfHourBand
		,QuarterHourBand
		,PartOfDay
	)
Select 
	
		 TimeValue = (Cast(@Time as time(0)))
		,TimeTextValue = Cast((Cast(@Time as time(0))) as varchar(8))
		,HourValue = Right('0' + Cast(DATEPART(Hour,@Time) as varchar(2)),2)
		,MinuteValue = Right('0' + Cast(DATEPART(Minute,@Time) as varchar(2)),2)
		,HourBand = 
					Case When DATEPART(hour,@Time) = 23 Then
						'23:00 - 00:00'
					Else
						Right('0' + Cast(DATEPART(Hour,@Time) as varchar(2)),2) + ':00 - ' +
						Right('0' + Cast(DATEPART(Hour,@Time) +1  as varchar(2)),2) + ':00'
					End
									
		,HalfHourBand = 
					Case When DATEPART(Minute,@Time) > 29 Then '30-59' Else '00-29' End
		,QuarterHourBand = 
					Case 
					When (DATEPART(Minute,@Time) >=0 and DATEPART(Minute,@Time) <15)  Then '00-14' 
					When (DATEPART(Minute,@Time) >=15 and DATEPART(Minute,@Time) <30)  Then '15-29' 
					When (DATEPART(Minute,@Time) >=30 and DATEPART(Minute,@Time) <45)  Then '30-44' 
					Else '45-59'
					End
		,PartOfDay = 
					Case 
					When (DATEPART(Hour,@Time) >=0 and DATEPART(Hour,@Time) < 6) Then 'Night Time'
					When (DATEPART(Hour,@Time) >=6 and DATEPART(Hour,@Time) < 8) Then 'Early Morning'
					When (DATEPART(Hour,@Time) >=8 and DATEPART(Hour,@Time) < 12) Then 'Morning'
					When (DATEPART(Hour,@Time) >=12 and DATEPART(Hour,@Time) < 14) Then 'Lunch Time'
					When (DATEPART(Hour,@Time) >=14 and DATEPART(Hour,@Time) < 18) Then 'Afternoon'
					When (DATEPART(Hour,@Time) >=18 and DATEPART(Hour,@Time) < 22) Then 'Evening'
					Else 'Late Evening'
					End

Select @Time = DATEADD(Second,1,@Time)

End