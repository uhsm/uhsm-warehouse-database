﻿/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:	PHE TRACE Study - parameters.

			Used in the SSIS package: SFTP PHE TRACE Study.

Notes:		Stored in WHREPORTING

Versions:
			1.0.0.0 - 26/11/2015 - MT
				Created sproc.
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [dbo].[PHETRACEStudyParameters]
As

Declare @FileName varchar(8)

Set @FileName =
	LEFT(
	DW_REPORTING.LIB.fn_DateConvertSQLToDBS(
	DW_REPORTING.LIB.fn_PreviousMonthStartOfDate(GETDATE()))
	,6)
	
Select @FileName As [FileName]