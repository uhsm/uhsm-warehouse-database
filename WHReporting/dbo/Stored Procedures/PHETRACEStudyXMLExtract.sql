﻿/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:	PHE TRACE Study XML Extract

Notes:		Stored in WHREPORTING

			Article on producing XML from SQL:
			https://www.simple-talk.com/sql/learn-sql-server/using-the-for-xml-clause-to-return-query-results-as-xml/

			Article on using bulk copy protocol (BCP) with a bcp format file:
			https://www.simple-talk.com/sql/database-administration/working-with-the-bcp-command-line-utility/
			
			You can generate the actual XML file using SSIS. See:
			S:\CO-HI-Information\Martin\SSIS\PHE TRACE Study\PHE TRACE Study.sln
			However this uses the DT_NTEXT datatype for the output which I think is the largest datatype available but
			this has a maximum size of 1.07GB.  This particular task will generate a file in the region of 4.2GB.  So I'm using
			BCP rather than SSIS to generate the file.  Having said this the XML file can be greatly reduced in size by zipping
			it to around 185MB.
			
			Date range: all admissions from 01/01/2009 but limited to discharged patients who have been coded, so
			at this particular time where DischargeDate <= 31/10/2015.

Versions:
			1.0.0.3 - 26/11/2015 - MT
				Auto-calculating start and end date parameters.

			1.0.0.2 - 26/10/2015 - MT
				PHE have sent a revised XML schema file - microexportv2.xsd - which has a new element:
				episodeType.
				Modify this sproc to create the XML for the new element.

			1.0.0.1 - 07/10/2015 - MT
				Following discussion with TD agreed changes:
				1. Get Colin Owen to modify the data sharing agreement for this task to include DOB.
				   If agreement cannot be modified then do not send DOB.
				   DOB commented out for now.
				2. Do not send versioningInfo nodes.

			1.0.0.0 - 02/10/2015 - MT
				Created sproc.
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [dbo].[PHETRACEStudyXMLExtract]
As

Declare @StartDate date
Declare @EndDate date
Declare @LastSentAdmissionDate date
Declare @CreateDate datetime

Set @StartDate = DW_REPORTING.LIB.fn_PreviousMonthStartOfDate(GETDATE())
Set @EndDate = DW_REPORTING.LIB.fn_PreviousMonthEndOfDate(GETDATE())
Set @LastSentAdmissionDate = '2013-12-31'
Set @CreateDate = GETDATE()

-- Get the patient data in one table
Declare @Patient Table (
	localpatientid int Not Null Primary Key,
	nhsnumber varchar(10),
	birthdate date,
	deathdate date,
	sex varchar(10)
	)
Insert Into @Patient(
	localpatientid,
	nhsnumber,
	birthdate,
	deathdate,
	sex
	)
Select	Distinct
	src.SourcePatientNo,
	src.NHSNo,
	pat.DateOfBirth,
	pat.DateOfDeath,
	pat.SexNHSCode
From	APC.Spell src With (NoLock)

	Inner Join APC.Patient pat With (NoLock)
		On src.EncounterRecno = pat.EncounterRecno

Where	src.AdmissionDate > @LastSentAdmissionDate -- Already transmitted all spells with AdmDate pre 2014.
		And src.DischargeDate Between @StartDate And @EndDate

-- Get the diagnosis data into one table
Declare @Diagnosis Table (
	provider_spellid int,
	episodeid int,
	diagnosisid varchar(30),
	episodestart datetime,
	episodeend datetime,
	diagnosisstart datetime,
	diagnosistype varchar(10),
	diagnosiscode varchar(10),
	versionDate datetime,
	SeqNo int,
	Unique (provider_spellid,diagnosisid)
	)
Insert Into @Diagnosis(
	provider_spellid,
	episodeid,
	diagnosisid,
	episodestart,
	episodeend,
	diagnosisstart,
	diagnosistype,
	diagnosiscode,
	versionDate,
	SeqNo
	)
Select	
	src.SourceSpellNo,
	src.EpisodeUniqueID,
	CONVERT(varchar(8),d.EncounterRecNo) + '_' +
	CONVERT(varchar(10),d.ClinicalCodingKey) + '_' +
	CONVERT(varchar(3),d.DiagnosisSequence),
	src.EpisodeStartDateTime,
	src.EpisodeEndDateTime,
	d.DiagnosisDate,
	'ICD10',
	d.DiagnosisCode,
	d.ModifiedDateTime,
	d.DiagnosisSequence
From	APC.Episode src With (NoLock)

		Inner Join APC.AllDiagnosis d With (NoLock)
		On src.EpisodeUniqueID = d.EpisodeSourceUniqueID

Where	src.AdmissionDate > @LastSentAdmissionDate
		And src.DischargeDate Between @StartDate And @EndDate

-- Get the procedure data into one table
Declare @Procedure Table (
	provider_spellid int,
	episodeid int,
	procedureid varchar(30),
	episodestart datetime,
	episodeend datetime,
	procedurestart datetime,
	proceduretype varchar(10),
	procedurecode varchar(10),
	versionDate datetime,
	SeqNo int,
	Unique (provider_spellid,procedureid)
	)
Insert Into @Procedure(
	provider_spellid,
	episodeid,
	procedureid,
	episodestart,
	episodeend,
	procedurestart,
	proceduretype,
	procedurecode,
	versionDate,
	SeqNo
	)
Select	
	src.SourceSpellNo,
	src.EpisodeUniqueID,
	CONVERT(varchar(8),p.EncounterRecNo) + '_' +
	CONVERT(varchar(10),p.ClinicalCodingKey) + '_' +
	CONVERT(varchar(3),p.ProcedureSequence),
	src.EpisodeStartDateTime,
	src.EpisodeEndDateTime,
	p.ProcedureDate,
	'OPCS',
	p.ProcedureCode,
	p.ModifiedDateTime,
	p.ProcedureSequence

From	APC.Episode src With (NoLock)

		Inner Join APC.AllProcedures p With (NoLock)
		On src.EpisodeUniqueID = p.EpisodeSourceUniqueID

Where	src.AdmissionDate > @LastSentAdmissionDate
		And src.DischargeDate Between @StartDate And @EndDate

-- Generate XML
Select	'UHSM' As 'clinical/@hospital',
	@CreateDate As 'clinical/@createdate',
	'micro' as 'clinical/@datatype',
	'martin.thatcher@uhsm.nhs.uk' as 'clinical/@emailcontact',
(Select	
	-- patient
	patient.sex,
	patient.birthdate,
	patient.deathdate,
	patient.nhsnumber,
	patient.localpatientid,

	-- patientProviderSpell
	patientProviderSpell.SourcePatientNo As [patientid],

	-- ProviderSpell
	ProviderSpell.SourceSpellNo As [provider_spellid],
	ProviderSpell.AdmissionDateTime As [admissiondate],
	ProviderSpell.AdmissionSpecialtyCode As [admissionspecialty],
	ProviderSpell.AdmissionMethodNHSCode As [admissionmethod],
	ProviderSpell.DischargeDateTime As [dischargedate],
	ProviderSpell.DischargeMethodNHSCode As [dischargemethod],

	-- Ward Stays
	-- The spec asks for Critical Care Periods but the XML spec does not cater
	-- for anything other than ward stays.
	-- ICA is the critical care ward so will be included in ward stays anyway.
	(Select	wardStay.SourceUniqueID As [ward_stayid],
		wardStay.SourceSpellNo As [provider_spellid],
		wardStay.WardCode As [ward_shortname],
		wardStay.StartTime As [wardstay_start],
		wardStay.EndTime as [wardstay_end]
	From	APC.WardStay As wardStay With (NoLock)
	Where	ProviderSpell.SourceSpellNo = wardStay.SourceSpellNo
	Order By wardStay.StartTime
	FOR XML AUTO, TYPE, ELEMENTS),

	-- Episodes
	(Select	episode.EpisodeUniqueID As [episodeid],
		episode.ConsultantCode As [consultantcode],
		episode.[SpecialtyCode(Main)] As [specialtycode],
		episode.EpisodeStartDateTime As [episodestart],
		episode.EpisodeEndDateTime As [episodeend],

		-- Diagnoses
		(Select	diagnosisid,
			provider_spellid,
			episodestart,
			episodeend,
			diagnosisstart,
			diagnosistype,
			diagnosiscode
		From	@Diagnosis diagnosis
		Where	episode.EpisodeUniqueID = diagnosis.episodeid
		Order By diagnosis.SeqNo
		FOR XML AUTO, TYPE, ELEMENTS),

		-- Procedures
		(Select	procedureid,
			provider_spellid,
			episodestart,
			episodeend,
			procedurestart,
			proceduretype,
			procedurecode
		From	@Procedure [procedure]
		Where	episode.EpisodeUniqueID = [procedure].episodeid
		Order By [procedure].SeqNo
		FOR XML AUTO, TYPE, ELEMENTS)

	From	APC.Episode As episode With (NoLock)
	Where	ProviderSpell.SourceSpellNo = episode.SourceSpellNo
	Order By episode.EpisodeStartDateTime
	FOR XML AUTO, TYPE, ELEMENTS)

From	@Patient patient
	
	-- patientProviderSpell
	Inner Join APC.Spell patientProviderSpell With (NoLock)
		On patient.localpatientid = patientProviderSpell.SourcePatientNo
		And patientProviderSpell.AdmissionDate > @LastSentAdmissionDate
		And patientProviderSpell.DischargeDate Between @StartDate And @EndDate

	-- ProviderSpell
	Inner Join APC.Spell ProviderSpell With (NoLock)
		On patientProviderSpell.SourceSpellNo = ProviderSpell.SourceSpellNo

Order By patient.localpatientid,patientProviderSpell.SourceSpellNo

FOR XML AUTO, TYPE, ELEMENTS) As 'clinical'
FOR XML PATH ('')