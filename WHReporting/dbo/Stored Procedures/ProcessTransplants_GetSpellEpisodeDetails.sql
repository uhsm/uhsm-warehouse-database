﻿-- =============================================
-- Author:		CMan
-- Create date: 04/02/2014
-- Description:	Stored Procedure to replace Cognos query 'Lookup txplant assessments.imr' in the processing of the transplant data
-- =============================================
CREATE PROCEDURE [dbo].[ProcessTransplants_GetSpellEpisodeDetails]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

/*
This replaces the cognos query Lookup txplant assessments.imr 
the output of this script is to be saved as an excel file 'Look transplant assessment yyymm.xls, which is then amended with additional data manually before getting  copied into 
another table on in the access database 'Assessments'
Require the RM2 numbers which are sent through to be manually entered into this query
Also need to set the start and end period which you require the admissions details bringing back for

*/

DECLARE @start DATETIME,
        @end   DATETIME
        
Set @start = '20131101'--set this date to the date of the start of the period you require
Set @end = '20140101'--set this date to the date of the end of the period you require - as the query below looks for less than this date if you want January data then set the end date to 1st Feb 

/*
This could be used to automate the setting of start and end date
--This grabs the start date of the month we are running the data for
SET @start =  CONVERT(DATETIME, '01/'
                               + RIGHT(('000' + CONVERT(VARCHAR, (Datepart(month, Dateadd(month, -1, Getdate()))))), 2)
                               + '/' + Datename(year, Getdate()), 103)
                               
--This sets the end date for the data relatingn to the month we are running the report for.
SET @end = CONVERT(DATETIME, '01/'
                             + RIGHT(('000' + CONVERT(VARCHAR, (Datepart(month, Getdate())))), 2)
                             + '/' + Datename(year, Getdate()), 103)
*/


--Query to give output of the spell/episode details for each of the RM2 numbers entered 
SELECT DISTINCT'ASSESSMENT'                   AS Type,
               ep.FacilityID,
               pat.PatientForename            AS PtForename,
               pat.PatientSurname             AS PtSurname,
               ep.SourceSpellNo               AS SpellRefno,
               Enc.episodeno,
               ep.AdmissionDate,
               ep.DischargeDate,
               ep.EpisodeCCGCode,
               ep.EpisodicPracticeCode,
               ep.EpisodeGPCode,
               NULL,
               ep.PrimaryProcedureCode,
               ep.PrimaryDiagnosisCode,
               ep.[SpecialtyCode(Main)],
               spell.AdmissionMethod,
               spell.PatientClass,
               spell.LengthOfSpell            AS LOS,
               NULL                           AS CappedLOS,
               NULL                           AS CriticalCareBedDays,
               ep.ResponsibleCommissionerCode AS purchasercode,
               NULL                           AS TxType
FROM   WHREPORTING.APC.Episode ep
       LEFT OUTER JOIN WHREPORTING.APC.Patient pat
                    ON ep.SourcePatientNo = pat.SourcePatientNo
       LEFT OUTER JOIN WH.APC.encounter Enc
                    ON ep.EpisodeUniqueID = Enc.SourceUniqueID
       LEFT OUTER JOIN WHREPORTING.APC.Spell spell
                    ON ep.SourceSpellNo = spell.SourceSpellNo
WHERE  ep.FacilityID IN ( 'RM2755778', 'RM24334676', 'RM21187239', 'RM21463917'---need to enter into here the RM2 numbers which need looking up for spell and episode details for 
-- these RM2 numbers will to be amended each month with the RM2 numbers which have been sent for the previous months Transplants.
                          )
       AND CONVERT(DATETIME, ep.AdmissionDate) >= @start
       AND CONVERT(DATETIME, ep.AdmissionDate) < @end
       AND Enc.EpisodeNo = 1 







END