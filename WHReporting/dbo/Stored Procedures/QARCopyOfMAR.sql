﻿/*===============================================================================
Author:		J Pitchforth
Date:		12 Mar 2013
Function:	Combines records from the DeduplicatedOPReferrals, OPAppointemtns and IPEpisodes
			tables to create the MAR report
Version:	1.0
===============================================================================
Author:		J Pitchforth
Date:		09 Apr 2013
Changes:	Extra fields added for Unify template (see comments below)
Version:	1.1
=================================================================================*/
--[MARQAR].[MAR] '1 June 2013','30 June 2013'
create proc [dbo].[QARCopyOfMAR]

@StartDate Date,
@EndDate Date

as

exec MARQAR.PopulateOPReferrals @StartDate,@EndDate
exec MARQAR.PopulateIPEpisodes @StartDate,@EndDate
exec MARQAR.PopulateOPAppointments @StartDate, @EndDate

select 
'1' as ProvCommFlag, --Field for Unify template
'RM2' as [Prov Org Code], --Field for Unify template
Coalesce(IPDetails.PurchaserCode, GPReferrals.EpisodePCTCode, GPReferralsSeen.PurchaserCode) as [Com Org Code],
'Line1' as [YG CONCAT], --Field for Unify template
'XG1' as XG1, --Field for Unify template
IPDetails.ElectiveOrdinary as [Elective Ordinary],
IPDetails.ElectiveDayCase as [Elective Daycase] ,
IPDetails.ElectiveTotal as [Elective Total],
IPDetails.ElectiveOrdinaryPlanned as [Elective Ordinary Planned],
IPDetails.ElectiveDayCasePlanned as [Elective Daycase Planned],
IPDetails.ElectiveTotalPlanned as [Elective Total Planned],
IPDetails.TreatmentCentres as [NHS Treatment Centres (TC)],
IPDetails.NonElective as [Total Non-Elective],
GPReferrals.GPWrittenReferralsMade_AllSpecs as [GP written referrals made],
GPReferralsSeen.GPWrittenReferralsSeen_AllSpecs as [GP written referrals seen],
GPReferrals.GPWrittenReferralsMade_GA as [GP written referrals made (G&A)],
GPReferralsSeen.GPWrittenReferralsSeen_GA as [GP written referrals seen (G&A)],
GPReferrals.OtherReferralsMade_GA as [Other referrals made (G&A)],
GPReferralsSeen.AllFirstOPAttends as [All first outpatient attendances (G&A)],
Null as PERIODID --Field for Unify template (period ID will be populated by Excel)

into whreporting.dbo.QARLookup
from MARQAR.IPDetails
full outer join MARQAR.GPReferrals
on IPDetails.PurchaserCode = GPReferrals.EpisodePCTCode
full outer join MARQAR.GPReferralsSeen
on ISNULL(IPDetails.PurchaserCode,GPReferrals.EpisodePCTCode) 
	= GPReferralsSeen.PurchaserCode
	
order by Coalesce(IPDetails.PurchaserCode, GPReferrals.EpisodePCTCode, GPReferralsSeen.PurchaserCode)