﻿CREATE Procedure [dbo].[Reasonableness_Report]
 @Offeredtime varchar(255) /*'3 weeks and longer' or 'Less than 3 weeks'*/
as

/*
Declare @Offeredtime varchar(255) = '3 weeks and longer'
*/

select 
	ip.PatientID
	,ip.PatientSurname
	,s.Division
	,s.Direcorate
	,s.SpecialtyFunctionCode
	,s.SpecialtyFunction
	,ip.ReferralReceivedDate
	,ip.DateOnList
	,ip.DateTCIOffered
	,ip.TCIDate
	,DATEDIFF(Day, ip.DateTCIOffered, ip.TCIDate) as Offered_to_TCI_Days
	,Case when DATEDIFF(Day, ip.DateTCIOffered, ip.TCIDate)>= 21
	 then '3 weeks and longer'
	 else 'Less than 3 weeks'
	 end                                          as Offered_to_TCI_Flag
	,ip.IPWLWaitNowWeeksAdjNoRTT                  as WeeksWait
	,ip.PTLComments
	,1                                            as Total
into #results
FROM [RTT_DW].[PTL].[Admitted] ip 
	Left Join WHREPORTING.LK.SpecialtyDivision s 
	on ip.SpecialtyCode = s.SpecialtyCode   
where 
	ip.IPWLWaitNowWeeksAdjNoRTT >14 
	and 
	ip.DateTCIOffered is not null
	
/*xxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/

Select 
	*
From #results r
where r.Offered_to_TCI_Flag = @Offeredtime

/*xxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
drop table #results