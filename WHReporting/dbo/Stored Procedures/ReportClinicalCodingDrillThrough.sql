﻿CREATE procedure [dbo].[ReportClinicalCodingDrillThrough]
	@User varchar(70)
	,@DateModified date
	,@UserRole char(1)
as

declare @AllowClear bit =
	case
	when
		Pseudonomisation.dbo.AllowClear() = 1
	and	@UserRole = 'C' --clear
	then 1
	else 0
	end

SELECT

     Diag.DiagnosisCode, Diag.Diagnosis, Diag.DiagnosisSequence, Diag.CreateDate, Diag.UserCreate, Diag.Counter, LK.Calendar.DayOfWeek 


	,FacilityID =
		case
		when @AllowClear = 1
		then APC.Episode.FacilityID
		else Pseudonomisation.dbo.GetPseudonomisedValue(APC.Episode.SourcePatientNo, 'FACILITYID', 'IPM')
		end

, APC.Episode.AdmissionDate, APC.Episode.DischargeDate, APC.Episode.ConsultantCode, APC.Episode.ConsultantName, 
                      APC.Episode.Division, APC.Episode.Directorate, APC.Episode.[Specialty(Function)], APC.Episode.[Specialty(Main)], APC.Episode.AdmissionType, 
                      APC.ClinicalCoding.PrimaryDiagnosisCode, APC.ClinicalCoding.SubsidiaryDiagnosisCode, APC.ClinicalCoding.SecondaryDiagnosisCode1, 
                      APC.ClinicalCoding.SecondaryDiagnosisCode2, APC.ClinicalCoding.SecondaryDiagnosisCode3, APC.ClinicalCoding.SecondaryDiagnosisCode4, 
                      APC.ClinicalCoding.SecondaryDiagnosisCode5, APC.ClinicalCoding.SecondaryDiagnosisCode6, APC.ClinicalCoding.SecondaryDiagnosisCode7, 
                      APC.ClinicalCoding.SecondaryDiagnosisCode8, APC.ClinicalCoding.SecondaryDiagnosisCode9, APC.ClinicalCoding.SecondaryDiagnosisCode10, 
                      APC.ClinicalCoding.SecondaryDiagnosisCode11, APC.ClinicalCoding.SecondaryDiagnosisCode12, APC.ClinicalCoding.PriamryProcedureCode, 
                      APC.ClinicalCoding.PrimaryProcedureDate, APC.ClinicalCoding.SecondaryProcedureCode1, APC.ClinicalCoding.SecondaryProcedureCode2, 
                      APC.ClinicalCoding.SecondaryProcedureCode3, APC.ClinicalCoding.SecondaryProcedureCode4, APC.ClinicalCoding.SecondaryProcedureCode5, 
                      APC.ClinicalCoding.SecondaryProcedureCode6, APC.ClinicalCoding.SecondaryProcedureCode7, APC.ClinicalCoding.SecondaryProcedureCode8, 
                      APC.ClinicalCoding.SecondaryProcedureCode9, APC.ClinicalCoding.SecondaryProcedureCode10, APC.ClinicalCoding.SecondaryProcedureCode11, 
                      Diag.UserModified, Diag.ModifedDate, Diag.ModifiedTime, APC.ClinicalCoding.PrimaryDiagnosis, APC.ClinicalCoding.SubsidiaryDiagnosis, 
                      APC.ClinicalCoding.SecondaryDiagnosis1, APC.ClinicalCoding.SecondaryDiagnosis2, APC.ClinicalCoding.SecondaryDiagnosis3, 
                      APC.ClinicalCoding.SecondaryDiagnosis4, APC.ClinicalCoding.SecondaryDiagnosis5, APC.ClinicalCoding.SecondaryDiagnosis6, 
                      APC.ClinicalCoding.SecondaryDiagnosis7, APC.ClinicalCoding.SecondaryDiagnosis8, APC.ClinicalCoding.SecondaryDiagnosis9, 
                      APC.ClinicalCoding.SecondaryDiagnosis10, APC.ClinicalCoding.SecondaryDiagnosis11, APC.ClinicalCoding.SecondaryDiagnosis12, 
                      APC.ClinicalCoding.PriamryProcedure, APC.ClinicalCoding.SecondaryProcedure1, APC.ClinicalCoding.SecondaryProcedure2, 
                      APC.ClinicalCoding.SecondaryProcedure3, APC.ClinicalCoding.SecondaryProcedure4, APC.ClinicalCoding.SecondaryProcedure5, 
                      APC.ClinicalCoding.SecondaryProcedure6, APC.ClinicalCoding.SecondaryProcedure7, APC.ClinicalCoding.SecondaryProcedure8, 
                      APC.ClinicalCoding.SecondaryProcedure9, APC.ClinicalCoding.SecondaryProcedure11, APC.ClinicalCoding.SecondaryProcedure10, 
                      CASE WHEN APC.Spell.InpatientStayCode = 'D' THEN 'Day Case' WHEN APC.Spell.InpatientStayCode = 'RD' THEN 'Day Case' WHEN APC.Spell.InpatientStayCode
                       = 'I' THEN 'Inpatient' WHEN APC.Spell.InpatientStayCode = 'RN' THEN 'Inpatient' END AS PatClass, Diag.CreateTime, Diag.CreateDateTime, 
                      APC.ClinicalCoding.CodingComplete
FROM         APC.AllDiagnosis AS Diag INNER JOIN
                      LK.Calendar ON Diag.CreateDate = LK.Calendar.TheDate INNER JOIN
                      APC.Episode ON Diag.EncounterRecno = APC.Episode.EncounterRecno INNER JOIN
                      APC.ClinicalCoding ON Diag.EncounterRecno = APC.ClinicalCoding.EncounterRecno INNER JOIN
                      APC.Spell ON APC.Episode.SourceSpellNo = APC.Spell.SourceSpellNo


WHERE     (Diag.ModifedDate = @DateModified) AND (Diag.UserModified = @User)
ORDER BY APC.Episode.FacilityID