﻿CREATE procedure [dbo].[ReportICEDemand]

    @DateFrom datetime
     ,@DateTo datetime
    ,@Provider varchar (100)
    ,@Specialty varchar (100)
  
as

SELECT     
Result_Code
,Result_Rubric
,MonthAdded
,COUNT(Sample_Index) AS results
,ClinicianSurname
,ClinicianSpecialtyCode
,ClinicianSpecialty
,ProviderName, ProviderType
FROM ICE.CSDataset
WHERE
(providerkey IN (@Provider)) 
AND (date_added BETWEEN @DateFrom AND @DateTo) 
AND (cast(ClinicianSpecialtyCode as varchar) IN (@Specialty)) 
AND (ProviderTypeKey = 1)
GROUP BY Result_Code
,Result_Rubric
,ClinicianSurname
,ClinicianSpecialtyCode
,ClinicianSpecialty
,ProviderName
,ProviderType
,MonthAdded