﻿CREATE Procedure [dbo].[ReportICEFilingDetail] 


--Declare

@StartDate				Date,
@EndDate				Date,
@ProviderType			int,
@RequestingClinician	int,
@Location				int

as

/*
Set @StartDate = '2011-08-10'
Set @EndDate = '2011-08-17'
Set @ProviderType = 2
Set @RequestingClinician = 0
Set @Location = 58
*/

Select 
	 Report.ReportIndex
	,Report.RequestIndex
	,Report.RequestDateTime
	,RequestingLocation = Location.LocationName
	,RequestingClinician = RequestDoc.ClinicianSurname
	,UniquePatientNo = Report.MasterPatientCode
	,Report.FilingStatusKey
	,FilingStatus.FilingStatus
	,Report.UserFiledKey
	,FilingUserType = ServiceUser.UserDescription
	,FilingUserName = ServiceUser.UserName	
	,Report.ReportDate
	,Report.ReportTime
	,Provider.ProviderType
	,Provider.ProviderTypeKey
	,Provider = Provider.ProviderShortCode + ' - ' + Provider.ProviderName
	,iPMKeyEvent = 
				Case 
					When Report.InpatientEpisodeUniqueID > 0 Then 'Inpatient' 
					When Report.OutpatientAppointmentUniqueID > 0 Then 'Outpatient'
				Else 'N/A'
				End
	,iPMServicePoint = Coalesce(IP.EndWardCode,OP.ClinicCode,'N/A')
	,iPMKeyDate = Coalesce(IP.EpisodeEndDate,OP.AppointmentDate)--,'1900-01-01')
	,iPMIPAdmissionDate = IP.AdmissionDate
	,iPMProfessionalCarer = Coalesce(IP.ConsultantName,OP.ProfessionalCarerName,'N/A')
	,HospitalNumber = Coalesce(Pat.FacilID,Pat.ICEHospitalNumber)
	,FacilID = Pat.FacilID
	,PatientName = Pat.Surname + ', ' + Pat.Forename
	,Investigation.InvestigationIndex
	,InvestigationDate = Investigation.DateTimeInvestigationAdded
	,InvestigationDescription = /*'('+InvestigationRequested.InvestigationCode + '): ' + */InvestigationRequested.InvestigationRequested
From ICE.FactReport Report 
Left outer join ICE.DimClinician RequestDoc with (nolock)
	on Report.MasterClinicianCode = RequestDoc.ClinicianKey
Left Outer Join ICE.DimServiceUser ServiceUser with (nolock)
	on Report.UserFiledKey = ServiceUser.UserKey
Left Outer Join ICE.DimLocation Location with (nolock)
	on Report.MasterLocationCode = Location.LocationKey
Left Outer Join APC.Episode IP with (nolock) 
	on Report.InpatientEpisodeUniqueID = IP.EpisodeUniqueID
Left Outer Join OP.Schedule OP with (nolock)
	on Report.OutpatientAppointmentUniqueID = OP.SourceUniqueID
Left Outer Join ICE.Patient Pat with (nolock)
	on Report.MasterPatientCode = Pat.PatientKey
Left Outer Join ICE.DimProvider Provider with (nolock)
	on Report.ServiceProviderCode = Provider.ProviderKey
Left Outer Join ICE.FactInvestigation Investigation with (nolock)
	on Report.ReportIndex = Investigation.ReportIndex
Left Outer Join ICE.DimInvestigationRequested InvestigationRequested with (nolock)
	on Investigation.InvestigationDescriptionKey = InvestigationRequested.InvestigationKey
Left Outer Join ICE.DimFilingStatus FilingStatus with (nolock)
	on Report.FilingStatusKey = FilingStatus.FilingStatusKey
Where 
Report.ReportDate >= @StartDate
and Report.ReportDate <=@EndDate
/*
and 
	(
	Provider.ProviderTypeKey IN(@ProviderType)
--	or
--	@ProviderType = cast('0' as int)
	)
and 
	(
	Report.MasterClinicianCode  IN(@RequestingClinician)
--	or
--	@RequestingClinician = 0
	)

and 

	(
	Report.MasterLocationCode in (@Location)
--	or
--	@Location = 0
	)
*/