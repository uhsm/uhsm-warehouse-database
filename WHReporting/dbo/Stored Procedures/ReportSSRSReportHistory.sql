﻿CREATE procedure [dbo].[ReportSSRSReportHistory] 
	@RootFolder varchar(500)
	,@ViewedTimeBand varchar(500)
as

/**
K Oakden 12/05/2014
Historic SSRS access data
Based on view on Report Server UHSM-IS1
See WH.dbo.UpdateReportLog on UHSM-IS1 for population of dbo.LogHistory
**/

if Object_id('tempdb..#RootFolder', 'U') is not null
	drop table #RootFolder
	
select folder = rtrim(ltrim(item))
into   #RootFolder
from   WHREPORTING.dbo.Splitstrings_cte(@RootFolder, N',')

if Object_id('tempdb..#TimeBand', 'U') is not null
	drop table #TimeBand
	
select timeband = rtrim(ltrim(item))
into   #TimeBand
from   WHREPORTING.dbo.Splitstrings_cte(@ViewedTimeBand, N',')

select distinct
	log1.RootFolder
	,log1.ReportPath
	,log1.ReportName
	,LastAccessed = convert(varchar(11), log1.LastAccessed, 113)
	,log1.CountAccessed
	,log1.ViewedTimeBand
	,log1.ReportType
	,log1.LinkedReportPath
	,CreationDate = convert(varchar(11), log1.CreationDate, 113)
	,log1.CreatedBy
	,ModifiedDate = convert(varchar(11), log1.ModifiedDate, 113)
	,log1.ModifiedBy
	,log1.ReportURL
	,RecordCount = 1
	,dateadd(day, 0, datediff(day , 0, log1.LastAccessed))
from [UHSM-IS1].ReportAdmin.dbo.HistoricReportLog log1
inner join #RootFolder folder
	on folder.folder collate database_default = log1.RootFolder collate database_default
inner join #TimeBand timeband
	on timeband.timeband collate database_default = log1.ViewedTimeBand collate database_default
order by dateadd(day, 0, datediff(day , 0, log1.LastAccessed)) desc, log1.RootFolder
	
/** Parameters
exec dbo.ReportSSRSReportHistory 'Cancer', '1-7 days ago'

select RootFolder 
from [UHSM-IS1].WH.dbo.HistoricReportLog
group by RootFolder
order by RootFolder

select ViewedTimeBand, lastViewed = MAX(LastAccessed)
from [UHSM-IS1].WH.dbo.HistoricReportLog
group by ViewedTimeBand
order by lastViewed desc

select MAX(CensusDate) from  [UHSM-IS1].WH.dbo.LogHistory
select * from   [UHSM-IS1].WH.dbo.HistoricReportLog
**/