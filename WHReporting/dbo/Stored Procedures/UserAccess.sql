﻿create procedure [dbo].[UserAccess] as

select
	 UserAccessCode = 'C'
	,UserAccess = 'Clear'
where
	Pseudonomisation.dbo.[AllowClear]() = 1

union all

select
	 UserAccessCode = 'U'
	,UserAccess = 'Unclear'