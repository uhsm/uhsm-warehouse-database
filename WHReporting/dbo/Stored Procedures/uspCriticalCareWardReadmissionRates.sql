﻿/*
------------------------------------------------------------------------------------------------------------------------------------------------------
Purpose:		CQC data request - readmission rates in critical care wards.

				Patients who have had a stay on ICA/CTCU/CT Transplant/BIC, then transferred
				to a general ward (excluding diagnostic wards RAD/GIU) and then been transferred
				back to ICA/CTCU/CT Transplant/BIC all within the same spell.
				
				Combine data from CT TRANSPLANT and CTCU into one ward - CTCCU.

				Note: Original bried quoted AICU ward which is coded as ICA in WHREPORTING.APC.WardStays.

Notes:			Stored in WHREPORTING

				Ward code list:
				Select	Distinct src.WardCode
				From	WHREPORTING.APC.WardStay src
				Where	src.StartTime >= DATEADD(d,-366,GETDATE())
				Order By src.WardCode

				Sample of ward stay records:
				Select Top 1000 * From WHREPORTING.APC.WardStay

Versions:		
				1.0.0.0 - 04/02/2016 - MT - Job 336
					Created sproc.
------------------------------------------------------------------------------------------------------------------------------------------------------
*/
CREATE Procedure [dbo].[uspCriticalCareWardReadmissionRates]
As

-- Stage 1: cte of all ward stays in datetime order categorised into Critical/General
;With Stage1 As (
	Select	src.SourceSpellNo,
			src.WardCode,
			Case
				When src.WardCode In ('ICA','BIC','CT TRANSPLANT','CTCU') Then 'Critical'
				Else 'General'
			End As WardType,
			ROW_NUMBER() Over (Partition By src.SourceSpellNo Order By src.StartTime) As RowNo
	From	WHREPORTING.APC.WardStay src
	Where	src.StartTime >= DATEADD(d,-366,GETDATE())
			And src.WardCode Not In ('GIU','RAD') -- Exclude diagnostic wards
	),

	-- Stage 2: cte to remove consecutive ward stays of the same type
	Stage2 As (
	Select	src.SourceSpellNo,
			src.WardType,
			ROW_NUMBER() Over (Partition By src.SourceSpellNo Order By src.RowNo) As RowNo
	From	Stage1 src
	
			-- Join to the previous ward stay
			Left Join Stage1 s1
				On src.SourceSpellNo = s1.SourceSpellNo
				And src.RowNo - 1 = s1.RowNo
				And src.WardType = s1.WardType
	
	Where	s1.SourceSpellNo Is Null -- Exclude where previous ward stay is the same type
	),

	-- Stage 3: get the spells where the readmission criteria is met
	Stage3 As (
	Select	src.SourceSpellNo
	From	Stage2 src

			-- Limit to where previous ward type was General
			Inner Join Stage2 s1
				On src.SourceSpellNo = s1.SourceSpellNo
				And src.RowNo - 1 = s1.RowNo
				And s1.WardType = 'General'
				
			-- Limit to where previous ward type to that was Critical
			Inner Join Stage2 s2
				On s1.SourceSpellNo = s2.SourceSpellNo
				And s1.RowNo - 1 = s2.RowNo
				And s2.WardType = 'Critical'

	Where	src.WardType = 'Critical'
	--122
	),
	-- Stage 4: Get the first critical care ward for each of the above so that
	-- can work out rates separately for each of the 4 critical care wards
	Stage4 As (
	Select	src.SourceSpellNo,
			Case
				When ws.WardCode In ('CT TRANSPLANT','CTCU') Then 'CTCCU'
				Else ws.WardCode
			End As WardCode,
			ROW_NUMBER() Over (Partition By src.SourceSpellNo Order By ws.StartTime) As RowNo
	From	Stage3 src

			Inner Join WHREPORTING.APC.WardStay ws
				On src.SourceSpellNo = ws.SourceSpellNo
				And ws.WardCode In ('ICA','BIC','CT TRANSPLANT','CTCU')
	),
	
	-- Stage 5: Get the numerator figures
	Stage5 As (
	Select	src.WardCode,COUNT(*) As Spells
	From	Stage4 src
	Where	src.RowNo = 1
	Group By src.WardCode
	),

	-- Stage 6: Get the denominator figures part 1
	Stage6 As (
	Select	Distinct 
			Case
				When src.WardCode In ('CT TRANSPLANT','CTCU') Then 'CTCCU'
				Else src.WardCode
			End As WardCode,
			src.SourceSpellNo
	From	WHREPORTING.APC.WardStay src
	Where	src.StartTime >= DATEADD(d,-366,GETDATE())
			And src.WardCode In ('ICA','BIC','CT TRANSPLANT','CTCU')
	),
	
	-- Stage 7: Get the denominator figures part 2
	Stage7 As (
	Select	src.WardCode,COUNT(*) As Spells
	From	Stage6 src
	Group By src.WardCode
	)
-- List the readmission rates
Select	Case 
			When src.WardCode = 'ICA' Then 'AICU'
			Else src.WardCode
		End As [FirstCriticalCareWard],
		src.Spells As [TotalSpells],
		s5.Spells As [ReadmissionsToAnyCriticalCareWard],
		Coalesce(CONVERT(float,s5.Spells),0) / src.Spells As [ReadmissionRate]
From	Stage7 src

		Left Join Stage5 s5
			On src.WardCode = s5.WardCode

Where	src.WardCode <> 'BIC' -- Not required but included in the processes above because it is a critical care ward

Order By src.WardCode