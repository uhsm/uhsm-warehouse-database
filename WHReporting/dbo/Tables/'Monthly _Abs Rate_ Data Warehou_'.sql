﻿CREATE TABLE [dbo].['Monthly (Abs Rate) Data Warehou$'](
	[Directorate] [nvarchar](255) NULL,
	[Directorate1] [nvarchar](255) NULL,
	[Specialty] [nvarchar](255) NULL,
	[Department] [nvarchar](255) NULL,
	[Month] [datetime] NULL,
	[Abs (FTE)] [float] NULL,
	[Avail (FTE)] [float] NULL,
	[% Abs Rate (FTE)] [float] NULL,
	[Salary Based Absence Cost (In Month)] [float] NULL
) ON [PRIMARY]