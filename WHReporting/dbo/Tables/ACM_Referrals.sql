﻿CREATE TABLE [dbo].[ACM_Referrals](
	[refrl_refno] [float] NULL,
	[patnt_refno] [float] NULL,
	[nhs_identifier] [float] NULL,
	[recvd_dttm] [datetime] NULL
) ON [PRIMARY]