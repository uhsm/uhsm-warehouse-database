﻿CREATE TABLE [dbo].[AdHocPasswords](
	[Analyst Name] [varchar](50) NULL,
	[File Name] [varchar](200) NULL,
	[Location of File] [varchar](800) NULL,
	[Password] [varchar](50) NULL
) ON [PRIMARY]