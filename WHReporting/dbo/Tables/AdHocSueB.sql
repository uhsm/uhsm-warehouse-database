﻿CREATE TABLE [dbo].[AdHocSueB](
	[NHSNumber (no spaces)] [varchar](50) NULL,
	[NHSNumber] [nvarchar](255) NULL,
	[Meeting Date] [date] NULL,
	[PracNum] [float] NULL,
	[Pracname] [nvarchar](255) NULL,
	[Prac] [nvarchar](255) NULL,
	[PracCode] [nvarchar](255) NULL
) ON [PRIMARY]