﻿CREATE TABLE [dbo].[Audit](
	[PackageGUID] [varchar](50) NOT NULL,
	[DataFlowTaskID] [int] NOT NULL,
	[SourceReadRows] [int] NULL,
	[SourceReadErrorRows] [int] NULL,
	[CleansedRows] [int] NULL,
	[TargetWriteRows] [int] NULL,
	[TargetWriteErrorRows] [int] NULL,
	[Comment] [nvarchar](255) NULL
) ON [PRIMARY]