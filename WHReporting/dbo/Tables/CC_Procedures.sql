﻿CREATE TABLE [dbo].[CC_Procedures](
	[OPCS4Code] [nvarchar](255) NULL,
	[OPCS4] [nvarchar](255) NULL,
	[OPCS4_Description] [nvarchar](255) NULL,
	[validation] [nvarchar](255) NULL
) ON [PRIMARY]