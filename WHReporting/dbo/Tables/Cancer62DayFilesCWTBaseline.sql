﻿CREATE TABLE [dbo].[Cancer62DayFilesCWTBaseline](
	[Tumour Type] [nvarchar](255) NULL,
	[Treatment Group] [nvarchar](255) NULL,
	[Included In Report] [nvarchar](255) NULL,
	[Breach Indicator] [nvarchar](255) NULL,
	[NHS Number] [float] NULL,
	[Patient CWT Ref] [float] NULL,
	[Primary Care Trust] [nvarchar](255) NULL,
	[Strategic Health Authority] [nvarchar](255) NULL,
	[Children's Cancer Marker] [nvarchar](255) NULL,
	[Patient Pathway Identifier] [float] NULL,
	[Organisation Code (Patient Pathway Identifier Issuer)] [nvarchar](255) NULL,
	[Source Of Referral For Out Patients] [float] NULL,
	[Decision to Refer Date (Cancer and Breast Symptoms)] [datetime] NULL,
	[Cancer Referral to Treatment Period Start Date] [datetime] NULL,
	[Priority Type] [float] NULL,
	[Urgent Cancer or Symptomatic Breast Referral Type] [float] NULL,
	[Consultant Upgrade Date] [nvarchar](255) NULL,
	[Organisation Code (Provider Consultant Upgrade)] [nvarchar](255) NULL,
	[Date First Seen] [datetime] NULL,
	[Organisation Code (Provider First Seen)] [nvarchar](255) NULL,
	[Waiting Time Adjustment (First Seen)] [nvarchar](255) NULL,
	[Waiting Time Adjustment Reason (First Seen)] [nvarchar](255) NULL,
	[Waiting Time Calculation (Referral To First Seen)] [nvarchar](255) NULL,
	[Delay Reason Comment (First Seen)] [nvarchar](255) NULL,
	[Delay Reason Referral To First Seen (Cancer and Breast Symptoms)] [nvarchar](255) NULL,
	[MDT Discussion Indicator] [nvarchar](255) NULL,
	[Multidisciplinary Team Discussion Date] [nvarchar](255) NULL,
	[Cancer or Symptomatic Breast Referral Patient Status] [float] NULL,
	[Primary Diagnosis (ICD)] [nvarchar](255) NULL,
	[Metastatic Site] [nvarchar](255) NULL,
	[Tumour Laterality] [nvarchar](255) NULL,
	[Cancer Treatment Event Type] [float] NULL,
	[Cancer Treatment Period Start Date] [datetime] NULL,
	[Organisation Code (Provider Decision To Treat)] [nvarchar](255) NULL,
	[Treatment Start Date (Cancer)] [datetime] NULL,
	[Organisation Code (Provider Treatment Start)] [nvarchar](255) NULL,
	[Cancer Treatment Modality] [float] NULL,
	[Clinical Trial Indicator] [float] NULL,
	[Cancer Care Setting (Treatment)] [float] NULL,
	[Radiotherapy Treatment Intent] [nvarchar](255) NULL,
	[Radiotherapy Priority] [nvarchar](255) NULL,
	[Waiting Time Adjustment (Treatment)] [nvarchar](255) NULL,
	[Waiting Time Adjustment Reason (Treatment)] [nvarchar](255) NULL,
	[Waiting Time Calculation (Decision To Treatment)] [nvarchar](255) NULL,
	[Waiting Time Calculation (Referral To Treatment)] [float] NULL,
	[Delay Reason Comment (Decision To Treatment)] [nvarchar](255) NULL,
	[Delay Reason Comment (Referral To Treatment)] [nvarchar](255) NULL,
	[Delay Reason Comment (Consultant Upgrade)] [nvarchar](255) NULL,
	[Delay Reason Decision To Treatment (Cancer)] [nvarchar](255) NULL,
	[Delay Reason Referral To Treatment (Cancer)] [nvarchar](255) NULL,
	[Delay Reason (Consultant Upgrade)] [nvarchar](255) NULL,
	[Possible Orphan Record] [nvarchar](255) NULL
) ON [PRIMARY]