﻿CREATE TABLE [dbo].[CommunityNursingPatchLookUpList](
	[Name] [nvarchar](255) NULL,
	[ProcaRefno] [float] NULL,
	[ProfessionalCarer] [nvarchar](255) NULL,
	[Patch] [nvarchar](255) NULL
) ON [PRIMARY]