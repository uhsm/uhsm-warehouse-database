﻿CREATE TABLE [dbo].[DNM-FOI16-141-DataAD1](
	[Specialty] [nvarchar](255) NULL,
	[Patient UR Number] [nvarchar](255) NULL,
	[Patient Name] [nvarchar](255) NULL,
	[Procedure] [nvarchar](255) NULL,
	[Date of Op Cancelled] [datetime] NULL,
	[Reason for Cancellation] [nvarchar](255) NULL,
	[2nd Reason] [nvarchar](255) NULL,
	[Comments] [nvarchar](255) NULL,
	[Week-end Date] [datetime] NULL,
	[Financial Year] [nvarchar](255) NULL,
	[NHS_CODE] [float] NULL,
	[SpecialtyFunction] [nvarchar](255) NULL,
	[Data Added to Database] [datetime] NULL
) ON [PRIMARY]