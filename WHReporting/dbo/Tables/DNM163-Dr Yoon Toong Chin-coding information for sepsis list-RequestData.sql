﻿CREATE TABLE [dbo].[DNM163-Dr Yoon Toong Chin-coding information for sepsis list-RequestData](
	[Admission date to include] [nvarchar](255) NULL,
	[First name] [nvarchar](255) NULL,
	[Surname] [nvarchar](255) NULL,
	[DoB] [nvarchar](255) NULL,
	[RM2] [float] NULL,
	[Req RM2 CONCAT] [nvarchar](255) NULL,
	[Req count rm2] [float] NULL,
	[Req Admission date to include AS DATE] [datetime] NULL,
	[Req DOB AS DATE] [datetime] NULL,
	[PCD Surname] [nvarchar](255) NULL,
	[PCD DOB] [datetime] NULL
) ON [PRIMARY]