﻿CREATE TABLE [dbo].[DataAccreditation](
	[Report] [nvarchar](255) NULL,
	[Report Date] [datetime] NULL,
	[Day] [nvarchar](255) NULL,
	[Frequency] [nvarchar](255) NULL,
	[Operational Group Area] [nvarchar](255) NULL,
	[Person Responsible] [nvarchar](255) NULL,
	[Data Accreditation File Name] [nvarchar](255) NULL,
	[Data Accreditation Link] [nvarchar](255) NULL
) ON [PRIMARY]