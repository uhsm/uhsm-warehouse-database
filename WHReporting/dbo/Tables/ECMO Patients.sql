﻿CREATE TABLE [dbo].[ECMO Patients](
	[SPELL_REFNO] [int] NULL,
	[DIAG_CODE1] [nvarchar](255) NULL,
	[DIAG_CODE2] [nvarchar](255) NULL,
	[DIAG_CODE3] [nvarchar](255) NULL,
	[DIAG_CODE4] [nvarchar](255) NULL,
	[DIAG_CODE5] [nvarchar](255) NULL,
	[PROC_CODE1] [nvarchar](255) NULL,
	[PROC_CODE2] [nvarchar](255) NULL,
	[PROC_CODE3] [nvarchar](255) NULL,
	[PROC_CODE4] [nvarchar](255) NULL,
	[PROC_CODE5] [nvarchar](255) NULL,
	[PROC_CODE6] [nvarchar](255) NULL,
	[ADMISSION_DATE] [datetime] NULL,
	[DISCHARGE_DATE] [datetime] NULL,
	[DATA_SOURCE] [nvarchar](255) NULL,
	[SPEC_CODE] [float] NULL,
	[CONS_CODE] [nvarchar](255) NULL
) ON [PRIMARY]