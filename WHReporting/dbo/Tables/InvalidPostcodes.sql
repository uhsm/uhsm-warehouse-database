﻿CREATE TABLE [dbo].[InvalidPostcodes](
	[SourcePatientNo] [int] NOT NULL,
	[FacilityID] [varchar](20) NULL,
	[PatientForename] [varchar](30) NULL,
	[PatientSurname] [varchar](30) NULL,
	[PatientAddress1] [varchar](50) NULL,
	[PatientAddress2] [varchar](50) NULL,
	[PatientAddress3] [varchar](50) NULL,
	[PatientAddress4] [varchar](50) NULL,
	[Postcode] [varchar](25) NULL
) ON [PRIMARY]