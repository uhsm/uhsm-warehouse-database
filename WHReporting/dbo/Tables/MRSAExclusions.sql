﻿CREATE TABLE [dbo].[MRSAExclusions](
	[OPCSCode] [varchar](5) NULL,
	[Description] [varchar](255) NULL,
	[IncludeForMRSA] [int] NULL,
	[EndoscopyFlag] [int] NULL
) ON [PRIMARY]