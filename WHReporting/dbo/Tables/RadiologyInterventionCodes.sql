﻿CREATE TABLE [dbo].[RadiologyInterventionCodes](
	[OrderExamCode] [nvarchar](255) NULL,
	[Diagnostic procedure] [nvarchar](255) NULL,
	[Interventional Procedure] [nvarchar](255) NULL
) ON [PRIMARY]