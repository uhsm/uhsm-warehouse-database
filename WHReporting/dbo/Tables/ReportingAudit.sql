﻿CREATE TABLE [dbo].[ReportingAudit](
	[Process] [varchar](50) NULL,
	[StartTime] [time](0) NULL,
	[EndTime] [time](0) NULL
) ON [PRIMARY]