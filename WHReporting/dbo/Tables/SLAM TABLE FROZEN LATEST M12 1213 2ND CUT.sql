﻿CREATE TABLE [dbo].[SLAM TABLE FROZEN LATEST M12 1213 2ND CUT](
	[QTR] [float] NULL,
	[MTH] [float] NULL,
	[PURCHASER] [nvarchar](255) NULL,
	[CONT] [nvarchar](255) NULL,
	[PROVSPNO] [int] NULL,
	[EPIORDER] [int] NULL,
	[STARTAGE] [int] NULL,
	[SEX] [int] NULL,
	[CLASSPAT] [int] NULL,
	[ADMISORC] [int] NULL,
	[ADMIMETH] [int] NULL,
	[DISDEST] [int] NULL,
	[DISMETH] [int] NULL,
	[EPIDUR] [int] NULL,
	[MAINSPEF] [int] NULL,
	[NEOCARE] [int] NULL,
	[TRETSPEF] [int] NULL,
	[DIAG_01] [nvarchar](255) NULL,
	[DIAG_02] [nvarchar](255) NULL,
	[DIAG_03] [nvarchar](255) NULL,
	[DIAG_04] [nvarchar](255) NULL,
	[DIAG_05] [nvarchar](255) NULL,
	[DIAG_06] [nvarchar](255) NULL,
	[DIAG_07] [nvarchar](255) NULL,
	[DIAG_08] [nvarchar](255) NULL,
	[DIAG_09] [nvarchar](255) NULL,
	[DIAG_10] [nvarchar](255) NULL,
	[DIAG_11] [nvarchar](255) NULL,
	[DIAG_12] [nvarchar](255) NULL,
	[DIAG_13] [nvarchar](255) NULL,
	[DIAG_14] [nvarchar](255) NULL,
	[OPER_01] [nvarchar](255) NULL,
	[OPER_02] [nvarchar](255) NULL,
	[OPER_03] [nvarchar](255) NULL,
	[OPER_04] [nvarchar](255) NULL,
	[OPER_05] [nvarchar](255) NULL,
	[OPER_06] [nvarchar](255) NULL,
	[OPER_07] [nvarchar](255) NULL,
	[OPER_08] [nvarchar](255) NULL,
	[OPER_09] [nvarchar](255) NULL,
	[OPER_10] [nvarchar](255) NULL,
	[OPER_11] [nvarchar](255) NULL,
	[OPER_12] [nvarchar](255) NULL,
	[REHABILITATIONDAYS] [int] NULL,
	[SPCDAYS] [int] NULL,
	[CRITICALCAREDAYS] [int] NULL,
	[EPISTART] [datetime] NULL,
	[EPIEND] [datetime] NULL,
	[ADMITDT] [datetime] NULL,
	[DISCHDT] [datetime] NULL,
	[RM No] [nvarchar](255) NULL,
	[POD] [nvarchar](255) NULL,
	[PRIVATE] [nvarchar](255) NULL,
	[GPPRACT] [nvarchar](255) NULL,
	[Ward] [nvarchar](255) NULL,
	[NHS_No] [nvarchar](255) NULL,
	[Postcode] [nvarchar](255) NULL,
	[DOB] [datetime] NULL,
	[BABY] [nvarchar](255) NULL,
	[F8Mdays] [float] NULL,
	[EPI_REF] [int] NULL,
	[PT_EthGroup] [nvarchar](255) NULL,
	[RowNo] [int] NULL,
	[FCE_HRG] [nvarchar](255) NULL,
	[GroupingMethodFlag] [nvarchar](255) NULL,
	[DominantProcedure] [nvarchar](255) NULL,
	[FCE_PBC] [nvarchar](255) NULL,
	[CalcEpidur] [int] NULL,
	[ReportingEPIDUR] [nvarchar](255) NULL,
	[FCETrimpoint] [nvarchar](255) NULL,
	[FCEExcessBeddays] [nvarchar](255) NULL,
	[SpellReportFlag] [nvarchar](255) NULL,
	[SpellHRG] [nvarchar](255) NULL,
	[SpellGroupingMethodFlag] [nvarchar](255) NULL,
	[SpellDominantProcedure] [nvarchar](255) NULL,
	[SpellPDiag] [nvarchar](255) NULL,
	[SpellSDiag] [nvarchar](255) NULL,
	[SpellEpisodeCount] [int] NULL,
	[SpellLOS] [int] NULL,
	[ReportingSpellLOS] [nvarchar](255) NULL,
	[SpellTrimpoint] [nvarchar](255) NULL,
	[SpellExcessBeddays] [nvarchar](255) NULL,
	[SpellCCDays] [int] NULL,
	[SpellPBC] [nvarchar](255) NULL,
	[SSC] [nvarchar](255) NULL,
	[SSC1] [nvarchar](255) NULL,
	[SSC2] [nvarchar](255) NULL,
	[WPU] [float] NULL,
	[CICU] [float] NULL,
	[ECMO] [int] NULL,
	[PIC] [int] NULL,
	[AICU] [float] NULL,
	[BIC] [float] NULL,
	[SCBU] [float] NULL,
	[TCU] [float] NULL,
	[CRIT] [float] NULL,
	[TRIM] [float] NULL,
	[SHORT] [nvarchar](255) NULL,
	[SSC YESNO] [nvarchar](255) NULL,
	[CALC XBD] [float] NULL,
	[ADJ XBD] [float] NULL,
	[XBD SPEC] [int] NULL,
	[REHAB UNB] [int] NULL,
	[ADJ XBD2] [float] NULL,
	[PBR] [nvarchar](255) NULL,
	[CF_IND] [nvarchar](255) NULL,
	[EX_CF] [nvarchar](255) NULL,
	[TR PAT] [nvarchar](255) NULL,
	[EX TR] [nvarchar](255) NULL,
	[EX TAVI] [nvarchar](255) NULL,
	[EX VAD] [nvarchar](255) NULL,
	[EX BIC] [nvarchar](255) NULL,
	[HRG2] [nvarchar](255) NULL,
	[BPT] [nvarchar](255) NULL,
	[BPT1] [nvarchar](255) NULL,
	[BPT2] [nvarchar](255) NULL,
	[POD2] [nvarchar](255) NULL,
	[POD2B] [nvarchar](255) NULL,
	[POD3] [nvarchar](255) NULL,
	[Cons] [nvarchar](255) NULL,
	[EffGP] [nvarchar](255) NULL,
	[UNB EX] [nvarchar](255) NULL,
	[STSS] [nvarchar](255) NULL,
	[ASPGLS] [nvarchar](255) NULL,
	[EX ASP] [nvarchar](255) NULL,
	[ALTEPLASE] [nvarchar](255) NULL,
	[MODEL] [nvarchar](255) NULL,
	[SPELL EXCLUSION] [nvarchar](255) NULL
) ON [PRIMARY]