﻿CREATE TABLE [dbo].[StrokeTIALookUp](
	[FacilityID] [nvarchar](255) NULL,
	[ClinicCode] [nvarchar](255) NULL,
	[Clinic] [nvarchar](255) NULL,
	[Appt Month] [datetime] NULL,
	[Appt Date] [datetime] NULL,
	[Appt Time] [time](7) NULL,
	[Referral Date] [datetime] NULL,
	[ReferralType] [nvarchar](255) NULL,
	[AppointmentType] [nvarchar](255) NULL,
	[Status] [nvarchar](255) NULL,
	[Appt Ref] [float] NULL,
	[Purchaser] [nvarchar](255) NULL,
	[CCG] [nvarchar](255) NULL,
	[ReferralReason] [nvarchar](255) NULL,
	[CancelDate] [nvarchar](255) NULL,
	[CancelReason] [nvarchar](255) NULL,
	[CompletionDateTime] [datetime] NULL,
	[CompletionReason] [nvarchar](255) NULL,
	[DayOfWeek] [nvarchar](255) NULL,
	[Referral to Appt] [float] NULL,
	[Treated within 24hrs?] [nvarchar](255) NULL,
	[Referred as HighRisk (H) or Low Risk (L)?] [nvarchar](255) NULL,
	[Comments 1] [nvarchar](255) NULL,
	[Comments 2] [nvarchar](255) NULL
) ON [PRIMARY]