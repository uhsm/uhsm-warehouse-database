﻿CREATE TABLE [dbo].[TransfusionAdhocPatientList](
	[Location] [nvarchar](255) NULL,
	[Unit] [nvarchar](255) NULL,
	[Product] [nvarchar](255) NULL,
	[Product Code] [int] NULL,
	[Unit Fate] [nvarchar](255) NULL,
	[Date] [datetime] NULL,
	[Name] [nvarchar](255) NULL,
	[RM2 Number] [varchar](255) NULL,
	[Transfusion] [nvarchar](255) NULL
) ON [PRIMARY]