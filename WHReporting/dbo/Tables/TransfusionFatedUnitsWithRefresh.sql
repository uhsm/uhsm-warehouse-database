﻿CREATE TABLE [dbo].[TransfusionFatedUnitsWithRefresh](
	[ReportingMonth] [datetime] NULL,
	[Location] [nvarchar](255) NULL,
	[Blood Unit] [nvarchar](255) NULL,
	[Product] [nvarchar](255) NULL,
	[Unit Fate] [nvarchar](255) NULL,
	[Date] [datetime] NULL,
	[Patient] [nvarchar](255) NULL,
	[DateImported] [datetime] NULL,
	[SequenceNumber] [int] NULL,
	[FirstImport] [int] NULL,
	[LastImport] [int] NULL
) ON [PRIMARY]