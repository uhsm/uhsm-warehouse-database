﻿CREATE TABLE [dbo].[TransfusionFatedUnits_cp20150421](
	[Location] [nvarchar](255) NULL,
	[Blood Unit] [nvarchar](255) NULL,
	[Product] [nvarchar](255) NULL,
	[Unit Fate] [nvarchar](255) NULL,
	[Date] [datetime] NULL,
	[Patient] [nvarchar](255) NULL
) ON [PRIMARY]