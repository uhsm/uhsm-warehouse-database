﻿CREATE TABLE [dbo].[TransfusionPASBloodTrackLoc_mapping](
	[WardCode] [nvarchar](255) NULL,
	[WardName] [nvarchar](255) NULL,
	[PASWardCode] [nvarchar](255) NULL,
	[PASWardName] [nvarchar](255) NULL,
	[MappedAutoTrackWard] [nvarchar](255) NULL,
	[Division] [nvarchar](255) NULL
) ON [PRIMARY]