﻿CREATE TABLE [dbo].[TransfusionUnfatedUnitsWithRefresh](
	[ReportingMonth] [datetime] NULL,
	[Blood Unit] [nvarchar](255) NULL,
	[Product] [nvarchar](255) NULL,
	[Last Location] [nvarchar](255) NULL,
	[Last Activity] [datetime] NULL,
	[Last Transaction] [nvarchar](255) NULL,
	[Patient] [nvarchar](255) NULL,
	[Patient Location] [nvarchar](255) NULL,
	[DateImported] [datetime] NULL,
	[SequenceNumber] [int] NULL,
	[FirstImport] [int] NULL,
	[LastImport] [int] NULL
) ON [PRIMARY]