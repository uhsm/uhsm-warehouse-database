﻿CREATE TABLE [dbo].[TransfusionUnfatedUnits_old](
	[Blood Unit] [nvarchar](255) NULL,
	[Product] [nvarchar](255) NULL,
	[Last Location] [nvarchar](255) NULL,
	[Last Activity] [datetime] NULL,
	[Last Transaction] [nvarchar](255) NULL,
	[Patient] [nvarchar](255) NULL,
	[Patient Location] [nvarchar](255) NULL
) ON [PRIMARY]