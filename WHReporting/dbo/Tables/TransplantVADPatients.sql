﻿CREATE TABLE [dbo].[TransplantVADPatients](
	[SourceSpellNo] [float] NULL,
	[ProcedureCategory] [nvarchar](255) NULL,
	[LastUpdated] [datetime] NULL
) ON [PRIMARY]