﻿CREATE TABLE [dbo].[Ward_Department$](
	[Month] [datetime] NULL,
	[Directorate] [nvarchar](255) NULL,
	[Sub Directorate] [nvarchar](255) NULL,
	[Specialty] [nvarchar](255) NULL,
	[Ward/Department] [nvarchar](255) NULL,
	[Total - Positions that require competence] [float] NULL,
	[Total - Positions that meet requirement] [float] NULL,
	[Total - Positions that do not meet requirement] [float] NULL,
	[Total - % Compliance] [float] NULL
) ON [PRIMARY]