﻿CREATE TABLE [dbo].[tempcancersurgery](
	[Tumour Type] [nvarchar](255) NULL,
	[Treatment Group] [nvarchar](255) NULL,
	[NHS Number] [nvarchar](255) NULL,
	[ADMISSION_DATE] [datetime] NULL,
	[PROCEDURE DESCRIPTION] [nvarchar](255) NULL,
	[AGE_AT_ADM] [float] NULL
) ON [PRIMARY]