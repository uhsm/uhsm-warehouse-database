﻿CREATE TABLE [dbo].[znOpenReferral](
	[PatientIdentifier] [varchar](20) NOT NULL,
	[SourceType] [varchar](50) NOT NULL,
 CONSTRAINT [PK_znOpenReferral] PRIMARY KEY CLUSTERED 
(
	[PatientIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]