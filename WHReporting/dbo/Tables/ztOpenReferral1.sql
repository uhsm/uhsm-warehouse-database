﻿CREATE TABLE [dbo].[ztOpenReferral1](
	[ActivityType] [varchar](2) NOT NULL,
	[SourceUniqueID] [int] NOT NULL,
	[PatientID] [varchar](20) NOT NULL,
	[SpecialtyCode] [varchar](10) NOT NULL,
	[EventDateTime] [datetime] NOT NULL,
	[AppointmentTypeCode] [varchar](20) NULL,
	[SeqNo] [int] NULL,
	[ReverseSeqNo] [int] NULL,
 CONSTRAINT [PK_ztOpenReferral1] PRIMARY KEY CLUSTERED 
(
	[ActivityType] ASC,
	[SourceUniqueID] ASC,
	[SpecialtyCode] ASC,
	[EventDateTime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]