﻿CREATE VIEW [dbo].[Connections]
AS
SELECT 
	[Objects].[RunKey],
	[Objects].ObjectKey AS ConnectionID,
	[Objects].ObjectName AS ConnectionName,
	[Objects].ObjectDesc AS ConnectionDesc,
	ConnectionString,
	ConnectionProperties.[Server],
	ConnectionProperties.[Database]
FROM [dbo].[Objects] 
INNER JOIN
	(SELECT
		RunKey,
		ConnectionProperties.ObjectKey,
		ConnectionString,
		[Server],
		[Database]
		FROM [dbo].[ObjectAttributes] 
		PIVOT 
			(
				MIN(ObjectAttrValue) FOR ObjectAttrName 
					IN (ConnectionString, [Server], [Database])
			) AS ConnectionProperties
	) AS ConnectionProperties
	ON [Objects].ObjectKey = ConnectionProperties.ObjectKey
	AND [Objects].RunKey = ConnectionProperties.RunKey
INNER JOIN dbo.LookupConnectionID
	ON ConnectionGUID = [Objects].ObjectTypeString