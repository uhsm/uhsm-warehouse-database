﻿CREATE VIEW [dbo].[ConnectionsMapping]
AS
SELECT DISTINCT 
    srel.ParentObjectKey AS SourceConnectionID,
    trel.ParentObjectKey AS TargetConnectionID
FROM dbo.WalkSources
INNER JOIN dbo.SourceTables
        ON dbo.WalkSources.osrc = dbo.SourceTables.ObjectKey
        AND dbo.WalkSources.RunKey = dbo.SourceTables.RunKey
INNER JOIN dbo.TargetTables
        ON dbo.WalkSources.tgt = dbo.TargetTables.ObjectKey
        AND dbo.WalkSources.RunKey = dbo.TargetTables.RunKey
INNER JOIN dbo.ObjectRelationships AS srel
        ON dbo.SourceTables.ObjectKey = srel.ChildObjectKey
        AND srel.RunKey = dbo.SourceTables.RunKey
INNER JOIN dbo.ObjectRelationships AS trel
        ON dbo.TargetTables.ObjectKey = trel.ChildObjectKey
        AND dbo.TargetTables.RunKey = trel.RunKey