﻿CREATE VIEW [dbo].[DataFlows]
AS
SELECT
    dbo.Objects.RunKey,
    dbo.Objects.ObjectKey,
    dbo.Objects.ObjectName,
    dbo.Objects.ObjectDesc,
    dbo.ObjectDependencies.SrcObjectKey AS PackageID
FROM dbo.Objects
INNER JOIN dbo.ObjectDependencies
        ON dbo.Objects.ObjectKey = dbo.ObjectDependencies.TgtObjectKey
        AND dbo.Objects.RunKey = dbo.ObjectDependencies.RunKey
WHERE dbo.Objects.ObjectTypeString IN (
	N'{C3BF9DC1-4715-4694-936F-D3CFDA9E42C5}', 
	N'{E3CFBEA8-1F48-40D8-91E1-2DEDC1EDDD56}'
)
  AND dbo.ObjectDependencies.DependencyType = N'Containment'