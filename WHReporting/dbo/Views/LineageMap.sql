﻿CREATE VIEW [dbo].[LineageMap]
AS
SELECT
	RunKey,
	SrcObjectKey,
	TgtObjectKey
FROM dbo.ObjectDependencies
WHERE DependencyType = N'Map'