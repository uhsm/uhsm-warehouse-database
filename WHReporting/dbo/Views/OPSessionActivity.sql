﻿CREATE View [dbo].[OPSessionActivity] as
Select
	 Encounter.SessionUniqueID
	,[NewAttend] = SUM(
				Case When 
				(Encounter.FirstAttendanceFlag = 9268			-- New
				Or Encounter.FirstAttendanceFlag = 5808			-- Walk In New
				Or Encounter.FirstAttendanceFlag = 2002180		-- A&E Attendance
				Or Encounter.FirstAttendanceFlag = 2003439		-- Walk In
				Or Encounter.FirstAttendanceFlag = 2003440)		-- Walk In A&E
				AND 
				(Encounter.AppointmentStatusCode = 2004151		--Attended
				OR Encounter.AppointmentStatusCode = 2868
				OR Encounter.AppointmentStatusCode = 357)
				Then 1 
				Else 0
				End
				)
	,[NewDNA] = SUM(
				Case When 
				(Encounter.FirstAttendanceFlag = 9268			-- New
				Or Encounter.FirstAttendanceFlag = 5808			-- Walk In New
				Or Encounter.FirstAttendanceFlag = 2002180		-- A&E Attendance
				Or Encounter.FirstAttendanceFlag = 2003439		-- Walk In
				Or Encounter.FirstAttendanceFlag = 2003440)		-- Walk In A&E
				AND 
				(Encounter.AppointmentStatusCode = 358			--Did Not Attend
				OR Encounter.AppointmentStatusCode = 2003535	--Did Not Attend
				OR Encounter.AppointmentStatusCode = 2000724	--Patient Late / Not Seen
				OR Encounter.AppointmentStatusCode = 2003494	--Patient Left / Not seen
				OR Encounter.AppointmentStatusCode = 2003495	--Refused To Wait
				)

				Then 1 
				Else 0
				End
				)
	,[NewCancelledPatient] = SUM(
				Case When 
				(Encounter.FirstAttendanceFlag = 9268			-- New
				Or Encounter.FirstAttendanceFlag = 5808			-- Walk In New
				Or Encounter.FirstAttendanceFlag = 2002180		-- A&E Attendance
				Or Encounter.FirstAttendanceFlag = 2003439		-- Walk In
				Or Encounter.FirstAttendanceFlag = 2003440)		-- Walk In A&E
				AND 
				(
				Encounter.AppointmentStatusCode = 2870			--Cancelled By Patient
				OR Encounter.AppointmentStatusCode = 2004301	--Cancelled by Patient
				OR Encounter.AppointmentStatusCode = 2003534	--Unable to Attend
				)

				Then 1 
				Else 0
				End
				)
	,[NewCancelledHospital] = SUM(
				Case When 
				(Encounter.FirstAttendanceFlag = 9268			-- New
				Or Encounter.FirstAttendanceFlag = 5808			-- Walk In New
				Or Encounter.FirstAttendanceFlag = 2002180		-- A&E Attendance
				Or Encounter.FirstAttendanceFlag = 2003439		-- Walk In
				Or Encounter.FirstAttendanceFlag = 2003440)		-- Walk In A&E
				AND 
				(
				Encounter.AppointmentStatusCode = 2004300 --Cancelled by Hospital
				OR Encounter.AppointmentStatusCode = 2004528 --Cancelled by EBS
				OR Encounter.AppointmentStatusCode = 2871 --Cancelled by Hosp.
				)
				Then 1 
				Else 0
				End
				)
	,[NewCancelledOther] = SUM(
				Case When 
				(Encounter.FirstAttendanceFlag = 9268			-- New
				Or Encounter.FirstAttendanceFlag = 5808			-- Walk In New
				Or Encounter.FirstAttendanceFlag = 2002180		-- A&E Attendance
				Or Encounter.FirstAttendanceFlag = 2003439		-- Walk In
				Or Encounter.FirstAttendanceFlag = 2003440)		-- Walk In A&E
				AND 
				(
				Encounter.AppointmentStatusCode = 2870 --Cancelled By Patient
				OR Encounter.AppointmentStatusCode = 2004301 --Cancelled by Patient
				OR Encounter.AppointmentStatusCode = 2003534 --Unable to Attend
				)
				Then 1 
				Else 0
				End
				)

	,FollowUpAttend = SUM(
				Case When 
				(Encounter.FirstAttendanceFlag =  8911			-- Follow Up
				Or Encounter.FirstAttendanceFlag = 2003648		-- Panel 
				Or Encounter.FirstAttendanceFlag = 5809)		-- Walk In Follow Up
				AND 
				(Encounter.AppointmentStatusCode = 2004151		--Attended
				OR Encounter.AppointmentStatusCode = 2868
				OR Encounter.AppointmentStatusCode = 357)
				Then 1 
				Else 0
				End
				)
	,FollowUpDNA = SUM(
				Case When 
				(Encounter.FirstAttendanceFlag =  8911			-- Follow Up
				Or Encounter.FirstAttendanceFlag = 2003648		-- Panel 
				Or Encounter.FirstAttendanceFlag = 5809)		-- Walk In Follow Up
				AND 
				(Encounter.AppointmentStatusCode = 358			--Did Not Attend
				OR Encounter.AppointmentStatusCode = 2003535	--Did Not Attend
				OR Encounter.AppointmentStatusCode = 2000724	--Patient Late / Not Seen
				OR Encounter.AppointmentStatusCode = 2003494	--Patient Left / Not seen
				OR Encounter.AppointmentStatusCode = 2003495	--Refused To Wait
				)
				Then 1 
				Else 0
				End
				)
	,FollowUpCancelledPatient = SUM(
				Case When 
				(Encounter.FirstAttendanceFlag =  8911			-- Follow Up
				Or Encounter.FirstAttendanceFlag = 2003648		-- Panel 
				Or Encounter.FirstAttendanceFlag = 5809)		-- Walk In Follow Up
				AND 
				(
				Encounter.AppointmentStatusCode = 2870 --Cancelled By Patient
				OR Encounter.AppointmentStatusCode = 2004301 --Cancelled by Patient
				OR Encounter.AppointmentStatusCode = 2003534 --Unable to Attend
				)
				Then 1 
				Else 0
				End
				)
	,FollowUpCancelledHospital = SUM(
				Case When 
				(Encounter.FirstAttendanceFlag =  8911			-- Follow Up
				Or Encounter.FirstAttendanceFlag = 2003648		-- Panel 
				Or Encounter.FirstAttendanceFlag = 5809)		-- Walk In Follow Up
				AND 
				(
				Encounter.AppointmentStatusCode = 2004300 --Cancelled by Hospital
				OR Encounter.AppointmentStatusCode = 2004528 --Cancelled by EBS
				OR Encounter.AppointmentStatusCode = 2871 --Cancelled by Hosp.
				)
				Then 1 
				Else 0
				End
				)
	,FollowUpCancelledOther = SUM(
				Case When 
				(Encounter.FirstAttendanceFlag =  8911			-- Follow Up
				Or Encounter.FirstAttendanceFlag = 2003648		-- Panel 
				Or Encounter.FirstAttendanceFlag = 5809)		-- Walk In Follow Up
				AND 
				(
				Encounter.AppointmentStatusCode = 2870 --Cancelled By Patient
				OR Encounter.AppointmentStatusCode = 2004301 --Cancelled by Patient
				OR Encounter.AppointmentStatusCode = 2003534 --Unable to Attend
				)
				Then 1 
				Else 0
				End
				)		
	,[NotCashedUp] = SUM(
				Case When 
				Encounter.AppointmentStatusCode =  45			-- Not Specified
				Then 1 
				Else 0
				End
				)

From WH.OP.Encounter Encounter
Left Outer Join WH.OP.Session OPSession
	On Encounter.SessionUniqueID = OPSession.SessionUniqueID
Where 
Encounter.IsWardAttender = 0
Group By
Encounter.SessionUniqueID