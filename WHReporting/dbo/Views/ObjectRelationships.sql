﻿CREATE VIEW [dbo].[ObjectRelationships]
AS
SELECT
    RunKey,
    SrcObjectKey AS ParentObjectKey,
    TgtObjectKey AS ChildObjectKey
FROM dbo.ObjectDependencies
WHERE DependencyType = N'Containment'