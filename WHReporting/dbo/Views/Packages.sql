﻿CREATE VIEW [dbo].[Packages]
AS
SELECT 
	Objects.RunKey,
	Objects.ObjectKey AS PackageID, 
	Objects.ObjectName AS PackageName,
	Objects.ObjectDesc AS PackageDesc,
	PackageProperties.PackageLocation,
	PackageProperties.PackageGUID
FROM [dbo].[Objects],
	(SELECT 
		RunKey,
		PackageProperties.ObjectKey,
		[PackageLocation],
		[PackageGUID]
	FROM dbo.ObjectAttributes 
	PIVOT (
		MIN (ObjectAttrValue) 
		FOR ObjectAttrName 
		IN ([PackageLocation], [PackageGUID])
		) AS PackageProperties
	) AS PackageProperties
WHERE [Objects].ObjectKey = PackageProperties.ObjectKey 
AND [Objects].RunKey = PackageProperties.RunKey
AND [Objects].ObjectTypeString = N'SSIS Package'