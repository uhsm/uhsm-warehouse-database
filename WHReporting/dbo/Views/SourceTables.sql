﻿CREATE VIEW [dbo].[SourceTables]
AS
SELECT
    dbo.Objects.RunKey,
    dbo.Objects.ObjectKey,
    dbo.Objects.ObjectName,
    dbo.Objects.ObjectTypeString,
    dbo.Objects.ObjectDesc,
    dbo.ObjectDependencies.TgtObjectKey AS SrcComponentKey,
    SourceObjects.ObjectName AS SourceObjectsName,
    SourceObjects.ObjectDesc AS SourceObjectsDesc,
    OD_DataFlow.SrcObjectKey AS DataFlowID,
    OD_DestConnection.SrcObjectKey AS SourceConnectionID
FROM dbo.Objects
INNER JOIN dbo.ObjectDependencies
        ON dbo.Objects.ObjectKey = dbo.ObjectDependencies.SrcObjectKey
        AND dbo.Objects.RunKey = dbo.ObjectDependencies.RunKey
INNER JOIN dbo.ObjectDependencies AS OD_DataFlow
        ON dbo.ObjectDependencies.TgtObjectKey = OD_DataFlow.TgtObjectKey
        AND dbo.ObjectDependencies.RunKey = OD_DataFlow.RunKey
INNER JOIN dbo.Objects AS SourceObjects
        ON dbo.ObjectDependencies.TgtObjectKey = SourceObjects.ObjectKey
        AND dbo.ObjectDependencies.RunKey = SourceObjects.RunKey
INNER JOIN dbo.ObjectDependencies AS OD_DestConnection
        ON dbo.Objects.ObjectKey = OD_DestConnection.TgtObjectKey
        AND dbo.Objects.RunKey = OD_DestConnection.RunKey
WHERE dbo.ObjectDependencies.DependencyType = N'Map'
  AND dbo.Objects.ObjectTypeString = N'Table'
  AND OD_DataFlow.DependencyType = N'Containment'
  AND OD_DataFlow.DependencyType = OD_DestConnection.DependencyType