﻿CREATE VIEW [dbo].[TableLineageMap]
AS
SELECT
    dbo.WalkSources.RunKey,
    dbo.SourceTables.ObjectKey AS SourceTableObjectKey,
    dbo.SourceTables.ObjectName AS SourceTable,
    srel.ParentObjectKey AS SourceConnectionKey,
    sconn.ConnectionName AS SourceConnectionName,
    sconn.ConnectionString AS SourceConnectionString,
    sconn.[Server] AS SourceServer,
    sconn.[Database] AS SourceDatabase,
    dbo.SourceTables.SrcComponentKey AS SourceComponentKey,
    dbo.TargetTables.ObjectName AS TargetTable,
    dbo.TargetTables.TgtComponentKey AS TargetComponentKey,
    trel.ParentObjectKey AS TargetConnectionKey,
    tconn.ConnectionName AS TargetConnectionName,
    tconn.ConnectionString AS TargetConnectionString,
    tconn.[Server] AS TargetServer,
    tconn.[Database] AS TargetDatabase,
    dfrel.ParentObjectKey AS DataFlowKey,
    dbo.Packages.PackageName,
    dbo.Packages.PackageDesc,
    dbo.Packages.PackageLocation,
    dbo.Packages.PackageGUID
FROM dbo.WalkSources
INNER JOIN dbo.SourceTables
        ON dbo.WalkSources.osrc = dbo.SourceTables.ObjectKey
        AND dbo.WalkSources.RunKey = dbo.SourceTables.RunKey
INNER JOIN dbo.TargetTables
        ON dbo.WalkSources.tgt = dbo.TargetTables.ObjectKey
        AND dbo.WalkSources.RunKey = dbo.TargetTables.RunKey
INNER JOIN dbo.ObjectRelationships AS srel
        ON dbo.SourceTables.ObjectKey = srel.ChildObjectKey
        AND dbo.SourceTables.RunKey = srel.RunKey
INNER JOIN dbo.ObjectRelationships AS trel

       ON dbo.TargetTables.ObjectKey = trel.ChildObjectKey
        AND dbo.TargetTables.RunKey = trel.RunKey
INNER JOIN dbo.ObjectRelationships AS dfrel
        ON dbo.TargetTables.TgtComponentKey = dfrel.ChildObjectKey
        AND dbo.TargetTables.RunKey = dfrel.RunKey
INNER JOIN dbo.ObjectRelationships AS pkgrel
        ON dfrel.ParentObjectKey = pkgrel.ChildObjectKey
        AND dfrel.RunKey = pkgrel.RunKey
INNER JOIN dbo.Packages
        ON pkgrel.ParentObjectKey = dbo.Packages.PackageID
        AND pkgrel.RunKey = dbo.Packages.RunKey
INNER JOIN dbo.Connections AS sconn
        ON srel.ParentObjectKey = sconn.ConnectionID
        AND srel.RunKey = sconn.RunKey
INNER JOIN dbo.Connections AS tconn
        ON trel.ParentObjectKey = tconn.ConnectionID
        AND trel.RunKey = tconn.RunKey