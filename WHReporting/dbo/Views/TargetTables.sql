﻿CREATE VIEW [dbo].[TargetTables]
AS
SELECT
    Objects.RunKey,
    ObjectDependencies.DependencyType,
    Objects.ObjectKey,
    Objects.ObjectName,
    Objects.ObjectDesc,
    ObjectDependencies.SrcObjectKey AS TgtComponentKey,
    TargetObjects.ObjectName AS TargetComponentName,
    TargetObjects.ObjectDesc AS TargetComponentDesc,
    OD_DataFlow.SrcObjectKey AS DataFlowID,
    OD_DestConnection.SrcObjectKey AS DestinationConnectionID
FROM dbo.Objects
INNER JOIN dbo.ObjectDependencies AS ObjectDependencies
        ON Objects.ObjectKey = ObjectDependencies.TgtObjectKey
       AND Objects.RunKey = ObjectDependencies.RunKey
INNER JOIN dbo.Objects AS TargetObjects
        ON ObjectDependencies.SrcObjectKey = TargetObjects.ObjectKey
       AND Objects.RunKey = TargetObjects.RunKey
       AND ObjectDependencies.RunKey = TargetObjects.RunKey
INNER JOIN dbo.ObjectDependencies AS OD_DataFlow
        ON ObjectDependencies.SrcObjectKey = OD_DataFlow.TgtObjectKey
       AND ObjectDependencies.RunKey = OD_DataFlow.RunKey
INNER JOIN dbo.ObjectDependencies AS OD_DestConnection
        ON Objects.ObjectKey = OD_DestConnection.TgtObjectKey
       AND Objects.RunKey = OD_DestConnection.RunKey
WHERE ObjectDependencies.DependencyType = N'Map'
  AND Objects.ObjectTypeString = N'Table'
  AND OD_DataFlow.DependencyType = N'Containment'
  AND OD_DestConnection.DependencyType = N'Containment'