﻿CREATE VIEW [dbo].[WalkSources]
AS
WITH WalkSourceCTE(RunKey, osrc, tgt, lvl, objecttype, ParentString) 
AS 
(
	SELECT  Objects.RunKey
		, dbo.SourceTables.ObjectKey
		, dbo.SourceTables.SrcComponentKey
		, 0 AS Expr1
		, dbo.Objects.ObjectTypeString
		, CAST(',' + CAST(dbo.SourceTables.ObjectKey as varchar(14)) + ',' AS VARCHAR(2000)) AS ParentString
	FROM dbo.SourceTables 
	INNER JOIN dbo.Objects 
		ON dbo.SourceTables.ObjectKey = dbo.Objects.ObjectKey
		AND SourceTables.RunKey = Objects.RunKey
	UNION ALL
	SELECT  Objects.RunKey
		, WalkSourceCTE.osrc
		, dbo.LineageMap.TgtObjectKey
		, WalkSourceCTE.lvl + 1 AS Expr1
		, Objects.ObjectTypeString
		, CAST(WalkSourceCTE.ParentString +  CAST(WalkSourceCTE.tgt as varchar(14)) + ',' AS VARCHAR(2000)) AS ParentString
	FROM         WalkSourceCTE
	INNER JOIN dbo.LineageMap 
		ON WalkSourceCTE.tgt = dbo.LineageMap.SrcObjectKey 
		AND WalkSourceCTE.RunKey = dbo.LineageMap.RunKey
	INNER JOIN dbo.Objects
		ON dbo.LineageMap.TgtObjectKey = Objects.ObjectKey
		AND LineageMap.RunKey = Objects.RunKey
	WHERE NOT ((WalkSourceCTE.osrc = WalkSourceCTE.tgt)
	OR CHARINDEX(',' + CAST(WalkSourceCTE.tgt AS VARCHAR(40)) + ',', WalkSourceCTE.ParentString) > 0)
)
    
SELECT   RunKey,   osrc, tgt, lvl, objecttype
FROM         WalkSourceCTE