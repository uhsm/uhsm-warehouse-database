﻿CREATE VIEW [dbo].[vwICEDirectorateReportDetails]
AS
SELECT DISTINCT 
case when 
sd.Direcorate ='Cardio & Thoracic' then 'Cardiothoracic'
when clinician.clinicianspecialtycode in ('242')

then 'Womens & Childrens' 
when clinician.clinicianspecialtycode in ('172','500') then 'Cardiothoracic'
when clinician.clinicianspecialtycode in ('653') then 'Therapies'
else sd.Direcorate end as OPDirectorate,
	MONTH(cal.TheDate) AS MonthNumber, 
    left(cal.TheMonth,3)+'-'+RIGHT(cal.TheMonth,2) as TheMonth, 
    cal.FinancialMonthKey,
    Provider.ProviderTypeKey, 
    CASE WHEN ProviderTypeKey = 2 THEN 'Radiology' WHEN ProviderTypeKey = 1 AND 
                    ProviderDisciplineKey = 2 THEN 'Histopathology' WHEN ProviderTypeKey = 1 AND 
                    ProviderDisciplineKey <> 2 THEN 'Pathology' ELSE 'Other' END AS Dept, 
    case when IP.Directorate='Cardio & Thoracic' then 'Cardiothoracic' else IP.Directorate end AS IPDirectorate, 
    case when IP.Directorate is not null
    then 'Inpatient' 
    when IP.Directorate is null and LocationTypeKey IN (3, 6)
    then 'Outpatient' else 'Other' end as PatientType ,
    
    
    
    
    
    /*case when LocationTypeKey IN (3, 6) then 'Outpatient' 
					WHEN LocationTypeKey IN (1, 2) then 'Inpatient' 
					else 'Other' end as PatientType,*/
    CASE WHEN LocationTypeKey IN (3, 6) 
                    THEN 
                    (case when sd.SpecialtyFunctionCode='191' then '190' 
                    else sd.SpecialtyFunctionCode end)
         ELSE NULL END AS OPSpecialtyFunction, 
    CASE WHEN LocationTypeKey IN (1,2) 
                    THEN 
                    (case when IP.[SpecialtyCode(Function)]='191' then '190' else IP.[SpecialtyCode(Function)]end)
                    ELSE NULL END AS IPSpecialtyFunction, 
    Location.LocationName, 
    Report.MasterLocationCode AS LocationCode, 
    Report.ServiceProviderCode AS ProviderTypeCode, 
    Provider.ProviderType, 
    Clinician.ClinicianSurname, 
    Report.ReportIndex, 
    Report.MasterClinicianCode, 
    FileStatus.FilingStatus, 
    CASE WHEN FileStatus.FilingStatusKey = 0 THEN 1 ELSE 0 END AS NotViewed, 
    CASE WHEN FileStatus.FilingStatusKey = 1  THEN 1 ELSE 0 END AS NotFiled, 
    CASE WHEN FileStatus.FilingStatusKey = 2 THEN 1 ELSE 0 END AS Filed,
    Provider.ProviderDiscipline, Provider.ProviderDisciplineKey, 
    1 AS Total, 
    COALESCE (IP.ConsultantName, 'Consultant Not Known') AS ResponsibleConsultantIP, 
    COALESCE (IP.ConsultantCode, 'N/A') AS ResponsibleConsultantCodeIP
FROM ICE.FactReport AS Report LEFT OUTER JOIN
    ICE.DimFilingStatus AS FileStatus ON Report.FilingStatusKey = FileStatus.FilingStatusKey LEFT OUTER JOIN
    ICE.DimLocation AS Location ON Report.MasterLocationCode = Location.LocationKey LEFT OUTER JOIN
    ICE.DimClinician AS Clinician ON Report.MasterClinicianCode = Clinician.ClinicianKey LEFT OUTER JOIN
		(SELECT DISTINCT ProviderSpecialtyCode, 
						 ProviderTypeKey, 
						 ProviderDisciplineKey, 
						 ProviderType, 
						 ProviderDiscipline
                 FROM ICE.DimProvider) AS Provider 
								  ON Report.Specialty = Provider.ProviderSpecialtyCode LEFT OUTER JOIN
     APC.Episode AS IP WITH (nolock) ON Report.InpatientEpisodeUniqueID = IP.EpisodeUniqueID INNER JOIN
     LK.Calendar AS cal ON cal.TheDate = Report.ReportDate left outer join lk.specialtydivision sd on sd.SpecialtyFunctionCode=cast(
(case when cliniciankey=3460 then 370
 else
clinician.clinicianspecialtycode end)

 as varchar) 
WHERE cal.FinancialMonthKey IN
               (select distinct top 12 financialmonthkey
      from lk.calendar
      
      where

       FinancialMonthKey<
      
      (select FinancialMonthKey from LK.Calendar
      where TheDate=CAST(GETDATE() as DATE)
      
      
      )
      
     order by FinancialMonthKey desc)
        AND (Provider.ProviderTypeKey IN (1, 2)) 
        AND (Report.Latest = 1) 
        --AND (Location.LocationTypeKey IN (1,2,3,6))
        and 
        
        
        isnull((case when coalesce((case when LocationTypeKey IN (3, 6) then 
        
        
        
        (case when 
sd.Direcorate ='Cardio & Thoracic' then 'Cardiothoracic'
when clinician.clinicianspecialtycode in ('242')

then 'Womens & Childrens' 
when clinician.clinicianspecialtycode in ('172','500') then 'Cardiothoracic'
when clinician.clinicianspecialtycode in ('653') then 'Therapies'
else sd.Direcorate end)
        
        
        
        
         else null end),
        ip.directorate
        ) ='Cardio & Thoracic' then 'Cardiothoracic' else coalesce((case when LocationTypeKey IN (3, 6) then sd.Direcorate else null end),
        ip.directorate
        ) end),'Unknown')
        not in 
        ('-1','Trust',
         'Not UHSM',
         'Audiology',
         'Data quality',
         'Unknown'
        ,'Therapies')
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Report"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 275
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "FileStatus"
            Begin Extent = 
               Top = 6
               Left = 313
               Bottom = 84
               Right = 466
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Location"
            Begin Extent = 
               Top = 6
               Left = 504
               Bottom = 114
               Right = 666
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Clinician"
            Begin Extent = 
               Top = 6
               Left = 704
               Bottom = 114
               Right = 916
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Provider"
            Begin Extent = 
               Top = 84
               Left = 313
               Bottom = 192
               Right = 501
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "IP"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 261
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cal"
            Begin Extent = 
               Top = 114
               Left = 815
               Bottom = 222
               Right = 992
            End
            DisplayFlags = 280
       ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwICEDirectorateReportDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'     TopColumn = 0
         End
         Begin Table = "sd"
            Begin Extent = 
               Top = 114
               Left = 539
               Bottom = 222
               Right = 728
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 12
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwICEDirectorateReportDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwICEDirectorateReportDetails'