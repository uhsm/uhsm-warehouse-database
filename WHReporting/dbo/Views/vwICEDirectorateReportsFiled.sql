﻿CREATE VIEW [dbo].[vwICEDirectorateReportsFiled]
AS
SELECT     ipDirectorate+PatientType+Dept+'Reports' as DirKey,FinancialMonthKey,TheMonth, 
Actual=sum(case when themonth in ('Jan-12', 'Feb-12', 'Mar-12') then 0 else Total end)
FROM         dbo.vwICEDirectorateReportDetails
WHERE     patienttype = 'Inpatient' 
GROUP BY  ipDirectorate+PatientType+Dept+'Reports',FinancialMonthKey,TheMonth
UNION
SELECT      ipDirectorate+PatientType+Dept+'Filed' as DirKey,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12', 'Feb-12', 'Mar-12') then 0 else Filed end)
FROM         dbo.vwICEDirectorateReportDetails
WHERE     patienttype = 'Inpatient'
GROUP BY  ipDirectorate+PatientType+Dept+'Filed' ,FinancialMonthKey,TheMonth
UNION
SELECT      ipDirectorate+PatientType+Dept+'NotFiled' as DirKey,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12', 'Feb-12', 'Mar-12') then 0 else NotFiled end)
FROM         dbo.vwICEDirectorateReportDetails
WHERE     patienttype = 'Inpatient' 
GROUP BY  ipDirectorate+PatientType+Dept+'NotFiled' ,FinancialMonthKey,TheMonth
UNION
SELECT     ipDirectorate+PatientType+Dept+'NotViewed' as DirKey,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12', 'Feb-12', 'Mar-12') then 0 else NotViewed end)
FROM         dbo.vwICEDirectorateReportDetails
WHERE     patienttype = 'Inpatient' 
GROUP BY  ipDirectorate+PatientType+Dept+'NotViewed' ,FinancialMonthKey,TheMonth
UNION
SELECT     opDirectorate+PatientType+Dept+'Reports' as DirKey,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12','Feb-12','Mar-12','Apr-12', 'May-12', 'Jun-12', 'Jul-12', 'Aug-12', 'Sep-12', 'Oct-12') then 0 else Total end)
FROM         dbo.vwICEDirectorateReportDetails
WHERE     patienttype = 'Outpatient' 
GROUP BY   opDirectorate+PatientType+Dept+'Reports' ,FinancialMonthKey,TheMonth
UNION
SELECT       opDirectorate+PatientType+Dept+'Filed' as DirKey,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12','Feb-12','Mar-12','Apr-12', 'May-12', 'Jun-12', 'Jul-12', 'Aug-12', 'Sep-12', 'Oct-12') then 0 else Filed end)
FROM         dbo.vwICEDirectorateReportDetails
WHERE     patienttype = 'Outpatient' 
GROUP BY   opDirectorate+PatientType+Dept+'Filed' ,FinancialMonthKey,TheMonth
UNION
SELECT       opDirectorate+PatientType+Dept+'NotFiled' as DirKey,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12','Feb-12','Mar-12','Apr-12', 'May-12', 'Jun-12', 'Jul-12', 'Aug-12', 'Sep-12', 'Oct-12') then 0 else NotFiled end)
FROM         dbo.vwICEDirectorateReportDetails
WHERE     patienttype = 'Outpatient' 
GROUP BY   opDirectorate+PatientType+Dept+'NotFiled' ,FinancialMonthKey,TheMonth
UNION
SELECT       opDirectorate+PatientType+Dept+'NotViewed' as DirKey,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12','Feb-12','Mar-12','Apr-12', 'May-12', 'Jun-12', 'Jul-12', 'Aug-12', 'Sep-12', 'Oct-12') then 0 else NotViewed end)
FROM         dbo.vwICEDirectorateReportDetails
WHERE     patienttype = 'Outpatient'
GROUP BY   opDirectorate+PatientType+Dept+'NotViewed' ,FinancialMonthKey,TheMonth
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[8] 4[15] 2[58] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwICEDirectorateReportsFiled'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwICEDirectorateReportsFiled'