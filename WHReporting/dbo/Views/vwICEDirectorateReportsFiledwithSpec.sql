﻿CREATE VIEW [dbo].[vwICEDirectorateReportsFiledwithSpec]
AS
SELECT     ipDirectorate+PatientType+Dept+(Case when IPSpecialtyFunction is null then 'Unknown Spec' else IPSpecialtyFunction end)+'Reports' as DirKey,
IPSpecialtyFunction,FinancialMonthKey,TheMonth, 
Actual=sum(case when themonth in ('Jan-12', 'Feb-12', 'Mar-12') then 0 else Total end)
FROM         dbo.vwICEDirectorateReportDetails
WHERE     patienttype = 'Inpatient' 
GROUP BY  ipDirectorate+PatientType+Dept+(Case when IPSpecialtyFunction is null then 'Unknown Spec' else IPSpecialtyFunction end)+'Reports',IPSpecialtyFunction,FinancialMonthKey,TheMonth

UNION
SELECT      ipDirectorate+PatientType+Dept+(Case when IPSpecialtyFunction is null then 'Unknown Spec' else IPSpecialtyFunction end)+'Filed' as DirKey,IPSpecialtyFunction,FinancialMonthKey,TheMonth, 
Actual=sum(case when themonth in ('Jan-12', 'Feb-12', 'Mar-12') then 0 else Filed end)
FROM         dbo.vwICEDirectorateReportDetails
WHERE     patienttype = 'Inpatient'
GROUP BY  ipDirectorate+PatientType+Dept+(Case when IPSpecialtyFunction is null then 'Unknown Spec' else IPSpecialtyFunction end)+'Filed' ,IPSpecialtyFunction,FinancialMonthKey,TheMonth

UNION
SELECT      ipDirectorate+PatientType+Dept+(Case when IPSpecialtyFunction is null then 'Unknown Spec' else IPSpecialtyFunction end)+'NotFiled' as DirKey,IPSpecialtyFunction,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12', 'Feb-12', 'Mar-12') then 0 else NotFiled end)
FROM         dbo.vwICEDirectorateReportDetails
WHERE     patienttype = 'Inpatient' 
GROUP BY  ipDirectorate+PatientType+Dept+(Case when IPSpecialtyFunction is null then 'Unknown Spec' else IPSpecialtyFunction end)+'NotFiled' ,IPSpecialtyFunction,FinancialMonthKey,TheMonth

UNION
SELECT     ipDirectorate+PatientType+Dept+(Case when IPSpecialtyFunction is null then 'Unknown Spec' else IPSpecialtyFunction end)+'NotViewed' as DirKey,IPSpecialtyFunction,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12', 'Feb-12', 'Mar-12') then 0 else NotViewed end)
FROM         dbo.vwICEDirectorateReportDetails
WHERE     patienttype = 'Inpatient' 
GROUP BY  ipDirectorate+PatientType+Dept+(Case when IPSpecialtyFunction is null then 'Unknown Spec' else IPSpecialtyFunction end)+'NotViewed' ,IPSpecialtyFunction,FinancialMonthKey,TheMonth

UNION
SELECT     opDirectorate+PatientType+Dept+(Case when OPSpecialtyFunction is null then 'Unknown Spec' else OPSpecialtyFunction end)+'Reports' as DirKey,OPSpecialtyFunction,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12','Feb-12','Mar-12','Apr-12', 'May-12', 'Jun-12', 'Jul-12', 'Aug-12', 'Sep-12', 'Oct-12') then 0 else Total end)
FROM         dbo.vwICEDirectorateReportDetails
WHERE     patienttype = 'Outpatient' 
GROUP BY   opDirectorate+PatientType+Dept+(Case when OPSpecialtyFunction is null then 'Unknown Spec' else OPSpecialtyFunction end)+'Reports' ,OPSpecialtyFunction,FinancialMonthKey,TheMonth

UNION
SELECT       opDirectorate+PatientType+Dept+(Case when OPSpecialtyFunction is null then 'Unknown Spec' else OPSpecialtyFunction end)+'Filed' as DirKey,OPSpecialtyFunction,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12','Feb-12','Mar-12','Apr-12', 'May-12', 'Jun-12', 'Jul-12', 'Aug-12', 'Sep-12', 'Oct-12') then 0 else Filed end)
FROM         dbo.vwICEDirectorateReportDetails
LEFT JOIN WH.SCR.Specialty ON OPSpecialtyFunction = SpecialtyCode
WHERE     patienttype = 'Outpatient' 
GROUP BY   opDirectorate+PatientType+Dept+(Case when OPSpecialtyFunction is null then 'Unknown Spec' else OPSpecialtyFunction end)+'Filed' ,OPSpecialtyFunction,FinancialMonthKey,TheMonth

UNION
SELECT       opDirectorate+PatientType+Dept+(Case when OPSpecialtyFunction is null then 'Unknown Spec' else OPSpecialtyFunction end)+'NotFiled' as DirKey,OPSpecialtyFunction,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12','Feb-12','Mar-12','Apr-12', 'May-12', 'Jun-12', 'Jul-12', 'Aug-12', 'Sep-12', 'Oct-12') then 0 else NotFiled end)
FROM         dbo.vwICEDirectorateReportDetails
WHERE     patienttype = 'Outpatient' 
GROUP BY   opDirectorate+PatientType+Dept+(Case when OPSpecialtyFunction is null then 'Unknown Spec' else OPSpecialtyFunction end)+'NotFiled' ,OPSpecialtyFunction,FinancialMonthKey,TheMonth

UNION
SELECT       opDirectorate+PatientType+Dept+(Case when OPSpecialtyFunction is null then 'Unknown Spec' else OPSpecialtyFunction end)+'NotViewed' as DirKey,OPSpecialtyFunction,FinancialMonthKey,TheMonth, Actual=sum(case when themonth in ('Jan-12','Feb-12','Mar-12','Apr-12', 'May-12', 'Jun-12', 'Jul-12', 'Aug-12', 'Sep-12', 'Oct-12') then 0 else NotViewed end)
FROM         dbo.vwICEDirectorateReportDetails
WHERE     patienttype = 'Outpatient'
GROUP BY   opDirectorate+PatientType+Dept+(Case when OPSpecialtyFunction is null then 'Unknown Spec' else OPSpecialtyFunction end)+'NotViewed' ,OPSpecialtyFunction,FinancialMonthKey,TheMonth