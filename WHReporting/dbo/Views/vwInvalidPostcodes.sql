﻿CREATE View [dbo].[vwInvalidPostcodes] as 
SELECT 
SourcePatientNo
,FacilityID
,PatientForename
,PatientSurname
,PatientAddress1
,PatientAddress2
,PatientAddress3
,PatientAddress4
,A.Postcode AS APostcode


FROM [WHREPORTING].[LK].[PatientCurrentDetails] A 


LEFT OUTER JOIN [OrganisationCCG].[dbo].[Postcode] B
ON REPLACE(A.POSTCODE, ' ','') = B.SlimPostcode

WHERE B.SlimPostcode IS NULL