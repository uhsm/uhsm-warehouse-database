﻿CREATE View [dbo].[vwLongStayMPI] as 
SELECT DISTINCT FacilityID, NHSNo, SourcePatientNo, 
PatientForename, PatientSurname
FROM         
APC.Patient