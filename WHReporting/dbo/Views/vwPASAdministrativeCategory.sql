﻿--KO updated 06/11/12
CREATE View [dbo].[vwPASAdministrativeCategory] as 
SELECT
	 AdministrativeCategoryCode = RefVal.RFVAL_REFNO
	,AdministrativeCategoryLocalCode = RefVal.MAIN_CODE
	,AdministrativeCategoryDescription = RefVal.DESCRIPTION
	,AdministrativeCategoryNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'adcat'
and RefVal.ARCHV_FLAG = 'N'
and refvalid.RITYP_CODE = 'NHS'
and refvalid.ARCHV_FLAG = 'N'