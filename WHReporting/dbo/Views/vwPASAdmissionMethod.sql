﻿--KO updated 06/11/12
CREATE View [dbo].[vwPASAdmissionMethod] as 
SELECT
	 AdmissionMethodCode = RefVal.RFVAL_REFNO
	,AdmissionMethodLocalCode = RefVal.MAIN_CODE
	,AdmissionMethodDescription = RefVal.DESCRIPTION
	,AdmissionMethodNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'admet'
and RefVal.ARCHV_FLAG = 'N'
and refvalid.RITYP_CODE = 'NHS'
and refvalid.ARCHV_FLAG = 'N'