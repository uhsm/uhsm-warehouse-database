﻿CREATE View [dbo].[vwPASAdmissionSource] as 
SELECT
	 AdmissionSourceCode = RefVal.RFVAL_REFNO
	,AdmissionSourceLocalCode = RefVal.MAIN_CODE
	,AdmissionSourceDescription = RefVal.DESCRIPTION
	,AdmissionSourceNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'ADSOR'
and RefVal.ARCHV_FLAG = 'N'
and refvalid.RITYP_CODE = 'NHS'
and refvalid.ARCHV_FLAG = 'N'