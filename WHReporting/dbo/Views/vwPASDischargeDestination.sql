﻿Create View [dbo].[vwPASDischargeDestination] as 
SELECT
	 DischargeDestinationCode = RefVal.RFVAL_REFNO
	,DischargeDestinationLocalCode = RefVal.MAIN_CODE
	,DischargeDestinationDescription = RefVal.DESCRIPTION
	,DischargeDestinationNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'DISDE'
and RefVal.ARCHV_FLAG = 'N'
and refvalid.RITYP_CODE = 'NHS'
and refvalid.ARCHV_FLAG = 'N'