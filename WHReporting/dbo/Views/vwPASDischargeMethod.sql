﻿create View [dbo].[vwPASDischargeMethod] as 
SELECT
	 DischargeMethodCode = RefVal.RFVAL_REFNO
	,DischargeMethodLocalCode = RefVal.MAIN_CODE
	,DishcargeMethodDescription = RefVal.DESCRIPTION
	,DischargeMethodNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'DISMT'
and RefVal.ARCHV_FLAG = 'N'
and refvalid.RITYP_CODE = 'NHS'
and refvalid.ARCHV_FLAG = 'N'