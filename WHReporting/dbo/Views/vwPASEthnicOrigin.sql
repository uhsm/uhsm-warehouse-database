﻿CREATE View [dbo].[vwPASEthnicOrigin] as 
SELECT
	 EthnicOriginCode = RefVal.RFVAL_REFNO
	,EthnicOriginLocalCode = RefVal.MAIN_CODE
	,EthnicOriginDescription = RefVal.DESCRIPTION
	,EthnicOriginNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'ETHGR'
and RefVal.ARCHV_FLAG = 'N'
and refvalid.RITYP_CODE = 'NHS'
and refvalid.ARCHV_FLAG = 'N'