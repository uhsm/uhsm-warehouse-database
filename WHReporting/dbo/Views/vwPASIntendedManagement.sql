﻿Create View [dbo].[vwPASIntendedManagement] as 
SELECT
	 IntendedMangementCode = RefVal.RFVAL_REFNO
	,IntendedMangementLocalCode = RefVal.MAIN_CODE
	,IntendedMangementDescription = RefVal.DESCRIPTION
	,IntendedMangementNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'inmgt'
and RefVal.ARCHV_FLAG = 'N'
and refvalid.RITYP_CODE = 'NHS'
and refvalid.ARCHV_FLAG = 'N'