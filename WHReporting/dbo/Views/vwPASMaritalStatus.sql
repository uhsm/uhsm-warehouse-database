﻿CREATE View [dbo].[vwPASMaritalStatus] as 
SELECT
	 MaritalStatusCode = RefVal.RFVAL_REFNO
	,MaritalStatusLocalCode = RefVal.MAIN_CODE
	,MaritalStatusDescription = RefVal.DESCRIPTION
	,MaritalStatusNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'MARRY'
and RefVal.ARCHV_FLAG = 'N'
and refvalid.RITYP_CODE = 'NHS'
and refvalid.ARCHV_FLAG = 'N'