﻿CREATE View [dbo].[vwPASNHSNoStatus] as 
SELECT
	 NHSNoStatusCode = RefVal.RFVAL_REFNO
	,NHSNoStatusLocalCode = RefVal.MAIN_CODE
	,NHSNoStatusDescription = RefVal.DESCRIPTION
	,NHSNoStatusNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'NNNTS'
and RefVal.ARCHV_FLAG = 'N'
and refvalid.RITYP_CODE = 'NHS'
and refvalid.ARCHV_FLAG = 'N'