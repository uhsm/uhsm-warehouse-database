﻿CREATE View [dbo].[vwPASOverseasStatus] as 
SELECT
	 OverSeasStatusCode = RefVal.RFVAL_REFNO
	,OverSeasStatusLocalCode = RefVal.MAIN_CODE
	,OverSeasStatusDescription = RefVal.DESCRIPTION
	,OverSeasStatusNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'OVSVS'
and RefVal.ARCHV_FLAG = 'N'
and refvalid.RITYP_CODE = 'NHS'
and refvalid.ARCHV_FLAG = 'N'