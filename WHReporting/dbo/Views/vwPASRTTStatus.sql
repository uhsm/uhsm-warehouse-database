﻿Create View [dbo].[vwPASRTTStatus] as 
SELECT
	 RTTStatusCode = RefVal.RFVAL_REFNO
	,RTTStatusLocalCode = RefVal.MAIN_CODE
	,RTTStatusDescription = RefVal.DESCRIPTION
	,RTTStatusNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'rttst'
and RefVal.ARCHV_FLAG = 'N'
and refvalid.RITYP_CODE = 'NHS'
and refvalid.ARCHV_FLAG = 'N'