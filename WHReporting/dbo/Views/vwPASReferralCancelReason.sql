﻿Create View [dbo].[vwPASReferralCancelReason] as 
SELECT
	 ReferralCancelReasonCode = RefVal.RFVAL_REFNO
	,ReferralCancelReasonLocalCode = RefVal.MAIN_CODE
	,ReferralCancelReasonDescription = RefVal.DESCRIPTION
	--,ReferralReasonNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
--left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
--	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'CANRS'
and RefVal.ARCHV_FLAG = 'N'
--and refvalid.RITYP_CODE = 'NHS'
--and refvalid.ARCHV_FLAG = 'N'