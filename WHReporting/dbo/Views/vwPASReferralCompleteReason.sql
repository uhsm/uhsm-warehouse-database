﻿Create View [dbo].[vwPASReferralCompleteReason] as 
SELECT
	 ReferralCompleteReasonCode = RefVal.RFVAL_REFNO
	,ReferralCompleteReasonLocalCode = RefVal.MAIN_CODE
	,ReferralCompleteReasonDescription = RefVal.DESCRIPTION
	--,ReferralReasonNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
--left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
--	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'CLORS'
and RefVal.ARCHV_FLAG = 'N'
--and refvalid.RITYP_CODE = 'NHS'
--and refvalid.ARCHV_FLAG = 'N'