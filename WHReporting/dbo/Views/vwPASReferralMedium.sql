﻿Create View [dbo].[vwPASReferralMedium] as 
SELECT
	 ReferralMediumCode = RefVal.RFVAL_REFNO
	,ReferralMediumLocalCode = RefVal.MAIN_CODE
	,ReferralMediumDescription = RefVal.DESCRIPTION
	--,ReferralReasonNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
--left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
--	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'RFMED'
and RefVal.ARCHV_FLAG = 'N'
--and refvalid.RITYP_CODE = 'NHS'
--and refvalid.ARCHV_FLAG = 'N'