﻿CREATE View [dbo].[vwPASReferralPriority] as 
SELECT
	 ReferralPriorityCode = RefVal.RFVAL_REFNO
	,ReferralPriorityLocalCode = RefVal.MAIN_CODE
	,ReferralPriorityDescription = RefVal.DESCRIPTION
	,ReferralPriorityNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'PRITY'
and RefVal.ARCHV_FLAG = 'N'
and refvalid.RITYP_CODE = 'NHS'
and refvalid.ARCHV_FLAG = 'N'