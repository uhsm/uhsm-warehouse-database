﻿CREATE View [dbo].[vwPASReferralReason] as 
SELECT
	 ReferralReasonCode = RefVal.RFVAL_REFNO
	,ReferralReasonLocalCode = RefVal.MAIN_CODE
	,ReferralReasonDescription = RefVal.DESCRIPTION
	,ReferralReasonNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'REASN'
and RefVal.ARCHV_FLAG = 'N'
and refvalid.RITYP_CODE = 'NHS'
and refvalid.ARCHV_FLAG = 'N'