﻿CREATE View [dbo].[vwPASReferralRequestedService] as 
SELECT
	 ReferralRequestedServiceCode = RefVal.RFVAL_REFNO
	,ReferralRequestedServiceLocalCode = RefVal.MAIN_CODE
	,ReferralRequestedServiceDescription = RefVal.DESCRIPTION
	--,ReferralReasonNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
--left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
--	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'ACTYP'
and RefVal.ARCHV_FLAG = 'N'
--and refvalid.RITYP_CODE = 'NHS'
--and refvalid.ARCHV_FLAG = 'N'