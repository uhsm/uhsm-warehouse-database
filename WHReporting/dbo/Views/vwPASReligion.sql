﻿CREATE View [dbo].[vwPASReligion] as 
SELECT
	 ReligionCode = RefVal.RFVAL_REFNO
	,ReligionLocalCode = RefVal.MAIN_CODE
	,ReligionDescription = RefVal.DESCRIPTION
	,ReligionNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'RELIG'
and RefVal.ARCHV_FLAG = 'N'
--KO update 14/05/2014 - religion codes use ID type 'NAT' instead of 'NHS'
--and refvalid.RITYP_CODE = 'NHS'
and refvalid.RITYP_CODE = 'NAT'
and refvalid.ARCHV_FLAG = 'N'