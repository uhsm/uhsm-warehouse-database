﻿CREATE View [dbo].[vwPASScheduleAttendStatus] as 
SELECT
	 AttendStatusCode = RefVal.RFVAL_REFNO
	,AttendStatusLocalCode = RefVal.MAIN_CODE
	,AttendStatusDescription = RefVal.DESCRIPTION
	,AttendStatusNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'ATTND'
and RefVal.ARCHV_FLAG = 'N'
and refvalid.RITYP_CODE = 'NHS'
and refvalid.ARCHV_FLAG = 'N'