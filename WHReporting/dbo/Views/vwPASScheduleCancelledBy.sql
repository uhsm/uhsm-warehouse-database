﻿CREATE View [dbo].[vwPASScheduleCancelledBy] as 
SELECT
	 CancelledByCode = RefVal.RFVAL_REFNO
	,CancelledByLocalCode = RefVal.MAIN_CODE
	,CancelledByDescription = RefVal.DESCRIPTION
	,CancelledByNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'CANCB'
and RefVal.ARCHV_FLAG = 'N'
and refvalid.RITYP_CODE = 'NHS'
and refvalid.ARCHV_FLAG = 'N'