﻿CREATE View [dbo].[vwPASScheduleOutcome] as 
SELECT
	 AttendOutcomeCode = RefVal.RFVAL_REFNO
	,AttendOutcomeLocalCode = RefVal.MAIN_CODE
	,AttendOutcomeDescription = RefVal.DESCRIPTION
	,AttendOutcomeNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'SCOCM'
and RefVal.ARCHV_FLAG = 'N'
and (refvalid.RITYP_CODE = 'NHS' 
-- KO 12/10/2012 Need to include PIMS code for failed home visit
or (refvalid.RITYP_CODE = 'PIMS' and RefVal.RFVAL_REFNO = 3307388))
and refvalid.ARCHV_FLAG = 'N'