﻿CREATE View [dbo].[vwPASScheduleProfCarerType] as 
SELECT
	 ProfessionalCarerTypeCode = RefVal.RFVAL_REFNO
	,ProfessionalCarerTypeLocalCode = RefVal.MAIN_CODE
	,ProfessionalCarerTypeDescription = RefVal.DESCRIPTION
	,ProfessionalCarerTypeNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'PRTYP'
and RefVal.ARCHV_FLAG = 'N'
and refvalid.RITYP_CODE = 'NHS'
and refvalid.ARCHV_FLAG = 'N'