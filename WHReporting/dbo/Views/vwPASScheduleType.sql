﻿Create View [dbo].[vwPASScheduleType] as 
SELECT
	 AppointmentTypeCode = RefVal.RFVAL_REFNO
	,AppointmentTypeLocalCode = RefVal.MAIN_CODE
	,AppointmentTypeDescription = RefVal.DESCRIPTION
	,AppointmentTypeNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'VISIT'
and RefVal.ARCHV_FLAG = 'N'
and refvalid.RITYP_CODE = 'NHS'
and refvalid.ARCHV_FLAG = 'N'