﻿CREATE View [dbo].[vwPASSex] as 
SELECT
	 SexCode = RefVal.RFVAL_REFNO
	,SexLocalCode = RefVal.MAIN_CODE
	,SexDescription = RefVal.DESCRIPTION
	,SexNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'SEXXX'
and RefVal.ARCHV_FLAG = 'N'
and refvalid.RITYP_CODE = 'NHS'
and refvalid.ARCHV_FLAG = 'N'