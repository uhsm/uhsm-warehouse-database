﻿CREATE View [dbo].[vwPASSourceOfReferral] as 
SELECT
	 ReferralSourceCode = RefVal.RFVAL_REFNO
	,ReferralSourceLocalCode = RefVal.MAIN_CODE
	,ReferralSourceDescription = RefVal.DESCRIPTION
	,ReferralSourceNationalCode = refvalid.IDENTIFIER

FROM wh.PAS.ReferenceValueBase RefVal
left outer join wh.PAS.ReferenceValueIdentifierBase refvalid 
	on RefVal.RFVAL_REFNO = refvalid.RFVAL_REFNO
WHERE 
refval.rfvdm_code = 'SORRF'
and RefVal.ARCHV_FLAG = 'N'
and refvalid.RITYP_CODE = 'NHS'
and refvalid.ARCHV_FLAG = 'N'