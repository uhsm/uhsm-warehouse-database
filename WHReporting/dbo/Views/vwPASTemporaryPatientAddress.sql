﻿CREATE view [dbo].[vwPASTemporaryPatientAddress] as
select
	SourcePatientNo = Patient.PATNT_REFNO
	,PatientTitle = Patient.TITLE_REFNO_DESCRIPTION
	,PatientForename = Patient.Forename
	,PatientSurname = Patient.Surname
	,DistrictNo = DistrictNo.IDENTIFIER
	,TemporaryPostcode = PatientAddress.PCODE
	,TemporaryAddress1 = PatientAddress.LINE1
	,TemporaryAddress2 = PatientAddress.LINE2
	,TemporaryAddress3 = PatientAddress.LINE3
	,TemporaryAddress4 = PatientAddress.LINE4
	,TemporaryCountry= Country.[DESCRIPTION]
	,StartDate = PatientAddress.START_DTTM
	,EndDate = PatientAddress.END_DTTM
	,AddressType = 
		case 
			when PatientAddressRole.ROTYP_CODE = 'TMP'
			then 'Temporary'
			when PatientAddressRole.ROTYP_CODE = 'CORR'
			then 'Correspondance'
		end
from Lorenzo.dbo.Patient

left join Lorenzo.dbo.PatientIdentifier DistrictNo
on	DistrictNo.PATNT_REFNO = Patient.PATNT_REFNO
and	DistrictNo.PITYP_REFNO = 2001232 --facility
and	DistrictNo.IDENTIFIER like 'RM2%'
and	DistrictNo.ARCHV_FLAG = 'N'
and	not exists
	(
	select
		1
	from
		Lorenzo.dbo.PatientIdentifier Previous
	where
		Previous.PATNT_REFNO = DistrictNo.PATNT_REFNO
	and	Previous.PITYP_REFNO = DistrictNo.PITYP_REFNO
	and	Previous.IDENTIFIER like 'RM2%'
	and	Previous.ARCHV_FLAG = 'N'
	and	(
			Previous.START_DTTM > DistrictNo.START_DTTM
		or
			(
				Previous.START_DTTM = DistrictNo.START_DTTM
			and	Previous.PATID_REFNO > DistrictNo.PATID_REFNO
			)
		)
	)

inner join Lorenzo.dbo.PatientAddressRole
on	PatientAddressRole.PATNT_REFNO = Patient.PATNT_REFNO
and	PatientAddressRole.ARCHV_FLAG = 'N'
and PatientAddressRole.ROTYP_CODE in ('TMP', 'CORR')
and PatientAddressRole.END_DTTM is null
and	exists
	(
	select
		1
	from
		Lorenzo.dbo.PatientAddress
	where
		PatientAddress.ADDSS_REFNO = PatientAddressRole.ADDSS_REFNO
	and	PatientAddress.ADTYP_CODE = 'POSTL'
	and	PatientAddress.ARCHV_FLAG = 'N'
	)

left join Lorenzo.dbo.PatientAddress
on	PatientAddress.ADDSS_REFNO = PatientAddressRole.ADDSS_REFNO
and	PatientAddress.ARCHV_FLAG = 'N'

left join Lorenzo.dbo.ReferenceValue Country
on Country.RFVAL_REFNO = PatientAddress.CNTRY_REFNO

where PatientAddress.START_DTTM is not null
and Patient.ARCHV_FLAG = 'N'

--order by Patient.PATNT_REFNO