﻿CREATE View [dbo].[vwPaediatricNetworkOutpatientDataRequest] as 
Select 
	 Appointment.SourceUniqueID
	,ProviderCode = 'RM200'
	,SiteCode = 'RM202'
	,[Code of Commissioner] = Appointment.PurchaserCode
	,[Commissioning Serial Number] = contr.IDENTIFIER
	,[PostCode of Usual Address] = Appointment.PostCode
	,[PCT Of Residence] = Appointment.ResidencePCTCode
	,[Sex] = Patient.SexNHSCode
	,[Date of Birth] = CONVERT(varchar(10), Patient.DateOfBirth,103)
	,[Registered GP Code] = Appointment.AppointmentGPCode
	,[Registered GP Practice Code] = Appointment.AppointmentPracticeCode
	,[Local Patient ID] = Patient.FacilityID
	,[Ethnic Group Code] = Patient.EthnicGroupNHSCode
	,[Administrative Category Code] = Appointment.AdministrativeCategoryNHSCode
	,[Source of Referral] = referral.ReferralSourceNHSCode
	,[Priority Type] = referral.PriorityNHSCode
	,[Service Type Requested] = referral.ReferralReasonCode
	,[Attendance Date] = CONVERT(varchar(10), Appointment.AppointmentDate,103)
	,[Attended or Did Not Attend Code] = Appointment.AttendStatusNHSCode
	,[First Attendance Code] = Appointment.AppointmentTypeNHSCode
	,[Outcome Of Attendance] = Appointment.OutcomeNHSCode
	,[Main Specialty] = Appointment.[SpecialtyCode(Main)]
	,[Treatment Function] = Appointment.[SpecialtyCode(Function)]
	,[Medical Staff Type Seeing Patient] = NUll
	,[Clinic Code] = Appointment.ClinicCode
	,[Clinian Code] = Appointment.ProfessionalCarerCode
	,[Primary Diagnosis Code] = Appointment.PrimaryDiagnosisCode
	,[Subsidiary Diagnosis Code] = null
	,[Operation Status] = 
						Case 
						When Coding.PriamryProcedureCode Is null 
						Then 8 
						Else 1 
						End
	,[Primary Procedure 1 (OPCS4) Code] = Coding.PriamryProcedureCode
	,[Procedure 2 Code] = Coding.SecondaryProcedure1
	,[Procedure 3 Code] = Coding.SecondaryProcedure2
	,[Procedure 4 Code] = Coding.SecondaryProcedure3
	,[Procedure 5 Code] = Coding.SecondaryProcedure4
	,[Procedure 6 Code] = Coding.SecondaryProcedure5
	,[Age] = Appointment.AgeAtAppointment
from WHREPORTING.OP.Schedule Appointment
left outer join Lorenzo.dbo.ScheduleEvent Schdl
on Appointment.SourceUniqueID = Schdl.SCHDL_REFNO
left outer join Lorenzo.dbo.[Contract] contr
on Schdl.CONTR_REFNO = contr.CONTR_REFNO
left outer join WHReporting.OP.Patient Patient
on Appointment.EncounterRecno = Patient.EncounterRecno
left outer join WHREPORTING.RF.Referral referral
on Appointment.ReferralSourceUniqueID = Referral.ReferralSourceUniqueID
left outer join WHREPORTING.OP.ClinicalCoding Coding
on Appointment.EncounterRecno = Coding.EncounterRecno
Where 
AppointmentDate >='20100401 00:00:00' and AppointmentDate <'20110401 00:00:00'
And
(
Appointment.AgeAtAppointment < 19
or 
Appointment.[SpecialtyCode(Function)] 
	in
		('501', '502', '503', '510', '520', '560', '610')
Or
Appointment.[SpecialtyCode(Main)]
	in
		('501', '502', '503', '510', '520', '560', '610')
)
and IsWardAttender = 0