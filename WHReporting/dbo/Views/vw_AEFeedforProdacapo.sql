﻿CREATE VIEW [dbo].[vw_AEFeedforProdacapo]
AS

SELECT     AE.EncounterRecno, AE.SourceUniqueID, AE.DistrictNo, AE.NHSNumber, AE.LocalPatientID, AE.PatientTitle, AE.PatientForename, AE.PatientSurname, 
                      AE.PatientAddress1, AE.PatientAddress2, AE.PatientAddress3, AE.PatientAddress4, AE.Postcode, AE.DateOfBirth, AE.DateOfDeath, AE.SexCode, 
                      AE.RegisteredGpCode, AE.RegisteredGpName, AE.RegisteredGpPracticeCode, AE.RegisteredGpPracticeName, AE.RegisteredGpPracticePostcode, 
                      AE.Consultant, AE.AttendanceNumber,AE.ArrivalModeCode ,AM.ArrivalMode, AE.AttendanceCategoryCode, AC.AttendanceCategory, AE.AttendanceDisposalCode, 
                      AD.AttendanceDisposal, AE.IncidentLocationTypeCode,AE.SourceOfReferralCode,SR.label,AE.ArrivalDate,AE.ArrivalTime, AE.AgeOnArrival, AE.InitialAssessmentTime, 
                      AE.SeenForTreatmentTime, AE.AttendanceConclusionTime, AE.DepartureTime, 
                      AE.CommissionerCodeCCG, AE.SeenByCode, AE.SeenBy, AE.InvestigationCodeFirst, AE.InvestigationCodeSecond, AE.DiagnosisCodeFirst, 
                      AE.DiagnosisCodeSecond, AE.CascadeDiagnosisCode, AE.TreatmentCodeFirst, AE.TreatmentCodeSecond, AE.Created, AE.InterfaceCode, AE.PCTCode, 
                      AE.CCGCode, AE.ResidencePCTCode, AE.ResidenceCCGCode, AE.SourceOfCCGCode, AE.TriageCategoryCode, CASE WHEN TriageCategoryCode IN ('O',
                       'R', 'Y') THEN 'Major' ELSE 'Minor' END AS Triage, AE.PresentingProblem, AE.ToXrayTime, AE.FromXrayTime, AE.ToSpecialtyTime, 
                      AE.SeenBySpecialtyTime, AE.EthnicCategoryCode, AE.ReferredToSpecialtyCode, AE.DecisionToAdmitTime, AE.DischargeDestinationCode, 
                      AE.RegisteredTime, AE.Attends, AE.WhereSeen, AE.EncounterDurationMinutes, AE.BreachReason, AE.AdmitToWard, AE.AdviceTime, AE.StandbyTime, 
                      AE.TrolleyDurationMinutes, AE.ManchesterTriageCode, AE.FFTextStatus, AE.ReferGP
FROM         WH.AE.Encounter AS AE LEFT OUTER JOIN
                      WH.AE.ArrivalMode AS AM ON REPLACE(AE.ArrivalModeCode, ' ', '') = AM.ArrivalModeCode LEFT OUTER JOIN
                      WH.AE.AttendanceCategory AS AC ON AE.AttendanceCategoryCode = AC.AttendanceCategoryCode LEFT OUTER JOIN
                      WH.AE.AttendanceDisposal AS AD ON AE.AttendanceDisposalCode = AD.AttendanceDisposalCode LEFT OUTER JOIN
                      WH.[CASCADE].AESource AS SR ON AE.SourceOfReferralCode = SR.code
WHERE     (AE.AttendanceCategoryCode IN ('1', '2'))
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AE"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 265
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AM"
            Begin Extent = 
               Top = 6
               Left = 303
               Bottom = 84
               Right = 465
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AC"
            Begin Extent = 
               Top = 6
               Left = 503
               Bottom = 99
               Right = 713
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AD"
            Begin Extent = 
               Top = 84
               Left = 303
               Bottom = 162
               Right = 503
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SR"
            Begin Extent = 
               Top = 102
               Left = 541
               Bottom = 195
               Right = 692
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_AEFeedforProdacapo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_AEFeedforProdacapo'