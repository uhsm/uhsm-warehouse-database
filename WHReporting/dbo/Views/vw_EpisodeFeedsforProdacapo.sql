﻿CREATE VIEW [dbo].[vw_EpisodeFeedsforProdacapo]
AS
SELECT     e.EncounterRecno, e.SourcePatientNo, e.SourceSpellNo, e.EpisodeUniqueID, e.ReferralSourceUniqueID, e.WaitingListSourceUniqueID, e.FacilityID, 
                      e.NHSNo, e.AdmissionDate, e.AdmissionTime, e.AdmissionDateTime, e.DischargeDate, e.DischargeTime, e.DischargeDateTime, e.EpisodeStartDate, 
                      e.EpisodeStartTime, e.EpisodeStartDateTime, e.EpisodeEndDate, e.EpisodeEndTime, e.EpisodeEndDateTime, e.StartWardCode, e.StartWard, 
                      e.EndWardCode, e.EndWard, e.ConsultantCode, e.ConsultantName, e.Division, e.Directorate, e.SpecialtyCode, e.Specialty, e.[SpecialtyCode(Function)], 
                      e.[Specialty(Function)], e.[SpecialtyCode(Main)], e.[Specialty(Main)], e.LengthOfEpisode, e.PrimaryDiagnosisCode, e.PrimaryProcedureCode, 
                      e.PrimaryProcedureDateTime, e.EpisodeGPCode, e.EpisodeGP, e.EpisodicPracticeCode, e.EpisodePractice, e.EpisodePCTCode, e.EpisodePCT, 
                      e.EpisodeCCGCode, e.EpisodeCCG, e.EpisodePCTCCGCode, e.EpisodePCTCCG, e.PostCode, e.ResidencePCTCode, e.ResidencePCT, 
                      e.ResidenceLocalAuthorityCode, e.ResidenceLocalAuthority, e.PurchaserCode, e.Purchaser, e.ResponsibleCommissionerCode, 
                      e.ResponsibleCommissioner, e.ResponsibleCommissionerCCGOnlyCode, e.ResponsibleCommissionerCCGOnly, e.TheatrePatientBookingKey, 
                      e.TheatreCode, e.Theatre, e.AgeCode, e.AdmissionType, e.NeoNatalLevelOfCare, e.FirstEpisodeInSpell, e.LastEpisodeInSpell, p.SexCode
FROM         APC.Episode AS e LEFT OUTER JOIN
                      APC.Patient AS p ON e.EncounterRecno = p.EncounterRecno
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "e"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 308
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "p"
            Begin Extent = 
               Top = 6
               Left = 346
               Bottom = 114
               Right = 535
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_EpisodeFeedsforProdacapo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_EpisodeFeedsforProdacapo'