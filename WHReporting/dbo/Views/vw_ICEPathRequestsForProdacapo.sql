﻿CREATE VIEW [dbo].[vw_ICEPathRequestsForProdacapo]
AS

SELECT DISTINCT Prov.ProviderDiscipline,
                Prov.ProviderDisciplineKey,
                Prov.ProviderType,
                Prov.ProviderKey,
                Prov.ProviderName,
                Pat.FacilID,
                Pat.NHSNumber,
                Clin.Clinician_Surname,
                Clin.Clinician_Forename,
                Clin.GP_Indicator,
                Clin.Clinician_Active,
                Clin.Clinician_National_Code,
                Clin.Clinician_Speciality_Code,
                Loc.LocationName,
                Loc.LocationType,
                Loc.IsGPPractice,
                SRequest.*,
                OT.Service_Request_Test_Index,
                OT.Test_Code,
                OT.Test_Name--,
               -- SRD.Tes
                
FROM   ICE_DB.dbo.ServiceRequest SRequest
       LEFT OUTER JOIN ICE_DB.dbo.OrderTests OT
                    ON SRequest.Service_Request_Index = OT.Service_Request_Index
     --  Left outer join ICE_DB.dbo.ServiceRequestDetail SRD on SRequest.Service_Request_Index = SRD.Request_Index
       LEFT OUTER JOIN WHREPORTING.ICE.DimProvider Prov
                    ON SRequest.Service_Provider_ID = Prov.ProviderKey -- link to this to identify Pathology requests
       LEFT OUTER JOIN WHREPORTING.ICE.Patient Pat
                    ON SRequest.Patient_id_key = Pat.PatientKey --link to this table to get RM2 and NHS Number where there is one.  Use the SRequest.Hospital_number as the ICE HospitalNumber
       LEFT OUTER JOIN ICE_DB.dbo.Clinician Clin
                    ON SRequest.Clinician_Index = Clin.Clinician_Index -- Requesting Clinician Details
       LEFT OUTER JOIN WHReporting.ICE.DimLocation Loc
                    ON SRequest.Location_Index = Loc.LocationKey --Requesting Location
WHERE  DateTime_Of_Request >= '20130401'
       AND DateTime_Of_Request < '20140201'
       AND SRequest.Status <> 'DEL'--add this criteria to remove deleted requests
       AND NOT EXISTS (SELECT 1
                       FROM   ICE_DB.dbo.ServiceRequestInformation RequestInfo
                       WHERE  SRequest.Service_Request_Index = RequestInfo.Service_Request_Id
                              AND RequestInfo.Information_Type = 'D' --  add this criteria as it looks like these are the cancelled requests
                      )
       AND Prov.ProviderType = 'Pathology'