﻿CREATE VIEW [dbo].[vw_ICUWardEpisodes_for_TransplantDB]
AS
----This view is to replace the cognos query Transplant_newcat.imr - the output of which is saved as a dbf file called allfces whic is brought through to the Access
----database as a table.
----We could possible put this script into a view in WHreporting and then bring the view into the access database as a table


--DECLARE @FYStart DATETIME,
--        @FYEnd  DATETIME

----This grabs the finance year start date that we are running the data for
--SET @FYStart = CONVERT(DATETIME, '01/04/' + convert(varchar,WHReporting.[dbo].[fnc_FiscalYear](getdate()))
--                             , 103)
                               
----This sets the latest end date for the month we are running the report for. i.e if runnning report in December it would be April - November data with the end date set at 01/12/2013
--SET @FYEnd = CONVERT(DATETIME, '01/'
--                             + RIGHT(('000' + CONVERT(VARCHAR, (Datepart(month, Getdate())))), 2)
--                             + '/' + Datename(year, Getdate()), 103)


--script to extract data
select ws.WardCode as [Ward Code], 
ws.SourcePatientNo as [Patnt Refno], 
ws.SourceSpellNo as [Spell Refno],
Spell.AdmissionDate as AdmitDate,
Spell.DischargeDate as DischDate,
ws.SourceUniqueID as [WdStay Number],
DATEDIFF(day, ws.StartTime, ws.EndTime) as WardLOS,
Spell.LengthOfSpell as SpellLOS,
Spell.DischargeDestinationCode as [Discharge Destination Code],
Spell.DischargeMethodCode as [Discharge Method Code],
cast(ws.StartTime as DATE) as WardStart,
cast(ws.EndTime as DATE) as WardEnd


from  WHReporting.APC.WardStay ws
LEFT JOIN   WHReporting.APC.Spell Spell
on ws.SourceSpellNo = Spell.SourceSpellNo
where WardCode in ('ICA','ICU','HDU','CTCU','CT Transplant', 'CTTRAN')
and Spell.DischargeDateTime >='20130401'--CONVERT(DATETIME, '01/04/' + convert(varchar,WHReporting.[dbo].[fnc_FiscalYear](getdate())) , 103)
and Spell.DischargeDateTime <  '20170401'--'CONVERT(DATETIME, '01/' + RIGHT(('000' + CONVERT(VARCHAR, (Datepart(month, Getdate())))), 2)+ '/' + Datename(year, Getdate()), 103)