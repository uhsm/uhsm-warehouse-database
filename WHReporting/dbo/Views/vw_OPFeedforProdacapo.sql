﻿CREATE VIEW [dbo].[vw_OPFeedforProdacapo]
AS
SELECT     OP.EncounterRecno, OP.SourcePatientNo, OP.SourceUniqueID, OP.IsWardAttender, OP.ReferralSourceUniqueID, OP.WaitingListSourceUniqueID, 
                      OP.SessionUniqueID, OP.FacilityID, OP.NHSNo, OP.AgeAtAppointment, OP.ClinicCode, OP.Clinic, OP.AppointmentRequestDate, 
                      OP.EBookingReferenceNumber, OP.AppointmentDate, OP.AppointmentTime, OP.AppointmentDateTime, OP.ArrivalTime, OP.SeenTime, 
                      OP.DepartedTime, OP.ClinicWait, OP.AppointmentTypeCode, OP.AppointmentType, OP.AppointmentTypeNHSCode, OP.ProfessionalCarerType, 
                      OP.ProfessionalCarerCode, OP.ProfessionalCarerName, OP.Division, OP.Directorate, OP.SpecialtyCode, OP.Specialty, OP.[SpecialtyCode(Function)], 
                      OP.[Specialty(Function)], OP.[SpecialtyCode(Main)], OP.[Specialty(Main)], OP.AttendStatusCode, OP.AttendStatus, OP.AttendStatusNHSCode, OP.Status, 
                      OP.OutcomeCode, OP.Outcome, OP.OutcomeNHSCode, OP.CancelledByCode, OP.CancelledBy, OP.CancelledDateTime, 
                      OP.AdministrativeCategoryCode, OP.AdministrativeCategory, OP.AdministrativeCategoryNHSCode, OP.PrimaryDiagnosisCode, 
                      OP.PrimaryProcedureCode, OP.PrimaryProcedureDateTime, OP.AppointmentGPCode, OP.AppointmentGP, OP.AppointmentPracticeCode, 
                      OP.AppointmentPractice, OP.AppointmentPCTCode, OP.AppointmentPCT, OP.AppointmentCCGCode, OP.AppointmentCCG, OP.AppointmentPCTCCGCode, 
                      OP.AppointmentPCTCCG, OP.PostCode, OP.ResidencePCTCode, OP.ResidencePCT, OP.ResidenceLocalAuthorityCode, OP.ResidenceLocalAuthority, 
                      OP.PurchaserCode, OP.Purchaser, OP.ResponsibleCommissionerCode, OP.ResponsibleCommissioner, OP.ResponsibleCommissionerCCGOnlyCode, 
                      OP.ResponsibleCommissionerCCGOnly, OP.AppointmentCreated, OP.AppointmentCreatedByWhom, OP.AppointmentUpdated, 
                      OP.AppointmentUpdatedByWhom, OP.ExcludedClinic, OP.IsWalkIn, Pat.SexCode
FROM         OP.Schedule AS OP LEFT OUTER JOIN
                      OP.Patient AS Pat ON OP.EncounterRecno = Pat.EncounterRecno
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "OP"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 308
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Pat"
            Begin Extent = 
               Top = 6
               Left = 346
               Bottom = 114
               Right = 535
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_OPFeedforProdacapo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_OPFeedforProdacapo'