﻿CREATE VIEW [dbo].[vw_RadiologyRoomLookUpFeedforProdacapo]
AS
SELECT
      [dict_radiology_locations]
      ,[rad_location_code]
      ,[rad_loc_desc]
FROM [WH].[MISYS].[DictRadiologyLocation]