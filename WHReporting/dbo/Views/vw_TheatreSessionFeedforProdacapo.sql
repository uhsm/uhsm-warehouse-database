﻿CREATE VIEW [dbo].[vw_TheatreSessionFeedforProdacapo]
AS
SELECT TS.*, S.Specialty, S.SpecialtyCode1, S.InactiveFlag FROM
WH.Theatre.Session TS
LEFT JOIN WH.Theatre.Specialty S
ON TS.SpecialtyCode = S.SpecialtyCode