﻿CREATE VIEW [dbo].[vw_allfces_for_TransplantDB]
AS
----This view is to replace the cognos query Transplant_newcat.imr - the output of which is saved as a dbf file called allfces whic is brought through to the Access
----database as a table.
----We could possible put this script into a view in WHreporting and then bring the view into the access database as a table


--DECLARE @FYStart DATETIME,
--        @FYEnd  DATETIME

----This grabs the finance year start date that we are running the data for
--SET @FYStart = CONVERT(DATETIME, '01/04/' + convert(varchar,WHReporting.[dbo].[fnc_FiscalYear](getdate()))
--                             , 103)
                               
----This sets the latest end date for the month we are running the report for. i.e if runnning report in December it would be April - November data with the end date set at 01/12/2013
--SET @FYEnd = CONVERT(DATETIME, '01/'
--                             + RIGHT(('000' + CONVERT(VARCHAR, (Datepart(month, Getdate())))), 2)
--                             + '/' + Datename(year, Getdate()), 103)


--script to extract data
select Distinct  ep.FacilityID as PT_FACIL_I,
		pat.PatientForename as PT_FORENAM ,
		pat.PatientSurname as  PT_SURNAME,
		ep.SourceSpellNo as SPELL_REFN,
		Enc.EpisodeNo as EPI_ORDER,
		ep.EpisodeStartDate as EPI_START_,
		ep.EpisodeEndDate as EPI_END_DA,
		ep.EpisodeCCGCode as EFFECTIVE_,--query which CCg code we should be using -  ep.responsibleCCG,ep.EpisodeCCGCode, ep.EpisodePCTCCGCode etc
		ep.EpisodicPracticeCode as EFFECTIVE2,
		ep.EpisodeGPCode as EFFECTIVE3,
		Cast('' as varchar) as ContractID ,
		ep.PrimaryProcedureCode as PROC_CODE1,
		ep.[SpecialtyCode(Main)] as Spec_Code,
		spell.AdmissionMethod as ADMET_DESC,
		spell.PatientClass as PATCLASS,
		ep.LengthOfEpisode as DAYS ,
		ep.EpisodeCCGCode as EFFECTIVE4,
		ep.PrimaryDiagnosisCode as DIAG_CODE1,
		ep.EpisodeStartDateTime as EPI_START_TIME,
		ep.EpisodeEndDateTime as EPI_END_TIME

from WHReporting.apc.episode ep
       LEFT OUTER JOIN WHREPORTING.APC.Patient pat
                    ON ep.SourcePatientNo = pat.SourcePatientNo
       LEFT OUTER JOIN WHREPORTING.APC.Spell spell
                    ON ep.SourceSpellNo = spell.SourceSpellNo
      LEFT OUTER JOIN WH.APC.encounter Enc
                    ON ep.EpisodeUniqueID = Enc.SourceUniqueID

where ep.EpisodeEndDate >= '20130401'--CONVERT(DATETIME, '01/04/' + convert(varchar,WHReporting.[dbo].[fnc_FiscalYear](getdate())), 103)
and  ep.EpisodeEndDate < '20170401'--CONVERT(DATETIME, '01/'+ RIGHT(('000' + CONVERT(VARCHAR, (Datepart(month, Getdate())))), 2) + '/' + Datename(year, dateadd(year, 1,Getdate())), 103)
and ep.SpecialtyCode in ('320','340','170','172','174','173','190','341','350','317','192','102','340G')