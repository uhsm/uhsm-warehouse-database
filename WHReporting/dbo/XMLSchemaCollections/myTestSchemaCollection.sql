﻿/****** Object:  XmlSchemaCollection [dbo].[myTestSchemaCollection]    Script Date: 07/04/2016 10:14:47 ******/
CREATE XML SCHEMA COLLECTION [dbo].[myTestSchemaCollection] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:t="http://schemas.adventure-works.com/Additional/ContactInfo" targetNamespace="http://schemas.adventure-works.com/Additional/ContactInfo"><xsd:element name="AdditionalContactInfo"><xsd:complexType mixed="true"><xsd:complexContent mixed="true"><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:any namespace="http://schemas.adventure-works.com/Contact/Record http://schemas.adventure-works.com/AdditionalContactTypes" minOccurs="0" maxOccurs="unbounded" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:element><xsd:element name="telephone" type="xsd:decimal" /></xsd:schema>'