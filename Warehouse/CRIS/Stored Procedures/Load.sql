﻿CREATE procedure [CRIS].[Load]
as

exec CRIS.LoadDepartment
exec CRIS.LoadEvent
exec CRIS.LoadEventExam
exec CRIS.LoadExam
exec CRIS.LoadExamOrder
exec CRIS.LoadExamStatus
exec CRIS.LoadPatientMap
exec CRIS.LoadPatient
exec CRIS.LoadRadiographer
exec CRIS.LoadRadiologist
exec CRIS.LoadReference
exec CRIS.LoadReferenceMap
exec CRIS.LoadReferrer
exec CRIS.LoadReferrerOrganisation
exec CRIS.LoadReport
exec CRIS.LoadRoom
exec CRIS.LoadSecretary
exec CRIS.LoadSite
exec CRIS.LoadSiteExam
exec CRIS.LoadSpecialty
exec CRIS.LoadWard