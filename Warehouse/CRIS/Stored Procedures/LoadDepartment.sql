﻿create procedure CRIS.LoadDepartment as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@deleted int
	,@inserted int
	,@updated int

declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	CRIS.Department target
using
	(
	select
		 DepartmentCode = upper(cast([code] as varchar(16)))
		,Department = upper(cast(dept_name as varchar(80)))
		,SiteCode = upper(cast(sitecode as varchar(5)))
	from
		CRIS.dbo.dept
	) source
	on	source.DepartmentCode = target.DepartmentCode

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 DepartmentCode
			,Department
			,SiteCode
			,Created
			,ByWhom
			)
		values
			(
			 source.DepartmentCode
			,source.Department
			,source.SiteCode
			,getdate()
			,suser_name()
			)

	when matched
	and
		checksum(
			 source.Department
			,source.SiteCode
		)
		<>
		checksum(
			 target.Department
			,target.SiteCode
		)
	then
		update
		set
			 target.DepartmentCode = source.DepartmentCode
			,target.Department = source.Department
			,target.SiteCode = source.SiteCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 $action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime
