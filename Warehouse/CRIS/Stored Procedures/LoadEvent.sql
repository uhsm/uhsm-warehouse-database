﻿CREATE PROCEDURE [CRIS].[LoadEvent] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			PatientID
			,ReferrerCode
			,ReferralSourceCode
			,ReferrerSpecialtyID
			,HospitalCode
			,WardCode
			,RequestCategoryCode
			,RequestDate
			,BookingTypeCode
			,BookedDate
			,IntendedRadiologistCode
			,UrgencyCode
			,PatientTypeCode
			,EventTime
			,RequiredClinician
		)
into
	#TLoadEvent
from
	(
	select
		 EventID = cast([event_key] as int)
		,PatientID = cast([computer_number] as int)
		,ReferrerCode = cast(replace([referrer], ' ', '*') as varchar(10)) --spaces are replaced with * to ensure unique link to Referrer table
		,ReferralSourceCode = cast([referal_source] as varchar(18))
		,ReferrerSpecialtyID = cast([speciality] as int)
		,HospitalCode = cast([site] as varchar(20))
		,WardCode = cast([ward] as varchar(30))
		,RequestCategoryCode = cast([request_cat] as varchar(1))
		,RequestDate = cast([request_date] as date)
		,BookingTypeCode = cast([booking_type] as varchar(1))
		,BookedDate = cast([date_booked] as date)
		,IntendedRadiologistCode = cast(replace([intended_radiol], ' ', '*') as varchar(20)) --spaces are replaced with * to ensure unique link to Radiologist table
		,UrgencyCode = cast([urgent] as tinyint)
		,PatientTypeCode = cast([patient_type] as varchar(1))
		,RequiredClinician = cast(required_clinician as varchar(255))

		,EventTime = 
			cast(
				case when events_date between '1753-01-01 00:00' and '9999-12-31 23:59'
				then cast(events_date as datetime) + 
					case
					when left(isnull([events_time],'9999'),2) > 23 
					then '00:00' 
					else left(events_time,2) + ':' + substring(events_time,3,2)
					end

				else '1753-01-01 00:00'
				end
				
				as datetime
			)
			
	from
		CRIS.dbo.[events]
	where
		upper(ltrim(rtrim(deleted))) <> 'Y'

	) Encounter


create unique nonclustered index #IX_TLoadEvent on #TLoadEvent
	(
	EventID ASC
	)
include
	(
	EncounterChecksum
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	CRIS.Event target
using
	(
	select
		 EventID
		,PatientID
		,ReferrerCode
		,ReferralSourceCode
		,ReferrerSpecialtyID
		,HospitalCode
		,WardCode
		,RequestCategoryCode
		,RequestDate
		,BookingTypeCode
		,BookedDate
		,IntendedRadiologistCode
		,UrgencyCode
		,PatientTypeCode
		,EventTime
		,RequiredClinician
		,EncounterChecksum
	from
		#TLoadEvent
	
	) source
	on	source.EventID = target.EventID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(			 
			EventID
			,PatientID
			,ReferrerCode
			,ReferralSourceCode
			,ReferrerSpecialtyID
			,HospitalCode
			,WardCode
			,RequestCategoryCode
			,RequestDate
			,BookingTypeCode
			,BookedDate
			,IntendedRadiologistCode
			,UrgencyID
			,PatientTypeCode
			,EventTime
			,RequiredClinician
			,Created
			,ByWhom
			)
		values
			(
			source.EventID
			,source.PatientID
			,source.ReferrerCode
			,source.ReferralSourceCode
			,source.ReferrerSpecialtyID
			,source.HospitalCode
			,source.WardCode
			,source.RequestCategoryCode
			,source.RequestDate
			,source.BookingTypeCode
			,source.BookedDate
			,source.IntendedRadiologistCode
			,source.UrgencyCode
			,source.PatientTypeCode
			,source.EventTime
			,source.RequiredClinician
			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> 
		CHECKSUM(
				target.PatientID
				,target.ReferrerCode
				,target.ReferralSourceCode
				,target.ReferrerSpecialtyID
				,target.HospitalCode
				,target.WardCode
				,target.RequestCategoryCode
				,target.RequestDate
				,target.BookingTypeCode
				,target.BookedDate
				,target.IntendedRadiologistCode
				,target.UrgencyID
				,target.PatientTypeCode
				,target.EventTime
				,target.RequiredClinician
				)
	then
		update
		set
			target.PatientID = source.PatientID
			,target.ReferrerCode = source.ReferrerCode
			,target.ReferralSourceCode = source.ReferralSourceCode
			,target.ReferrerSpecialtyID = source.ReferrerSpecialtyID
			,target.HospitalCode = source.HospitalCode
			,target.WardCode = source.WardCode
			,target.RequestCategoryCode = source.RequestCategoryCode
			,target.RequestDate = source.RequestDate
			,target.BookingTypeCode = source.BookingTypeCode
			,target.BookedDate = source.BookedDate
			,target.IntendedRadiologistCode = source.IntendedRadiologistCode
			,target.UrgencyID = source.UrgencyCode
			,target.PatientTypeCode = source.PatientTypeCode
			,target.EventTime = source.EventTime
			,target.RequiredClinician = source.RequiredClinician
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime