﻿CREATE PROCEDURE [CRIS].[LoadEventExam] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			EventID
			,ExamTime
			,ExamCode
			,RoomCode
			,RadiologistCode
			,RadiographerCode1
			,RadiographerCode2
			,RadiographerCode3
			,BookedTime
			,AccessionCode
		)
into
	#TLoadEventExam
from
	(
	select
		 EventExamID = cast([exam_key] as int)
		,EventID = cast([event_key] as int)
		,ExamCode = cast([examination] as varchar(16))
		,RoomCode = cast([room] as varchar(12))
		,RadiologistCode = cast([radiologist] as varchar(20))

		,ExamTime =
			cast(
				dateadd(
					minute

					,case
					when isnumeric(replace(replace(start_time, ':', ''), '.', '')) = 0 then 0
					when cast(replace(replace(start_time, ':', ''), '.', '') as int) not between 0 and 2359 then 0

					else
						cast(left(right('0000' + replace(replace(start_time, ':', ''), '.', ''), 4), 2) as int) * 60 +
						cast(right('0000' + replace(replace(start_time, ':', ''), '.', ''), 2) as int)	
					end

					,case 
					when [exams_date] is null then [exams_date]
					when [exams_date] not between '1753-01-01 00:00' and '9999-12-31 23:59' then '1753-01-01 00:00'
					else [exams_date]
					end
				)
						
				as datetime)

		,RadiographerCode1 = cast([radiographer_1] as varchar(20))
		,RadiographerCode2 = cast([radiographer_2] as varchar(20))
		,RadiographerCode3 = cast([radiographer_3] as varchar(20))

		,BookedTime = 
					 cast(dateadd(
								minute

								,case
								when isnumeric(replace(replace(booked_time, ':', ''), '.', '')) = 0 then 0
								when cast(replace(replace(booked_time, ':', ''), '.', '') as int) not between 0 and 2359 then 0

								else
									cast(left(right('0000' + replace(replace(booked_time, ':', ''), '.', ''), 4), 2) as int) * 60 +
									cast(right('0000' + replace(replace(booked_time, ':', ''), '.', ''), 2) as int)	
								end

								,case 
								when [booked_date] is null then [booked_date]
								when [booked_date] not between '1753-01-01 00:00' and '9999-12-31 23:59' then '1753-01-01 00:00'
								else [booked_date]
								end
							) as datetime)

		,AccessionCode = cast([accession] as varchar(40))

	from
		CRIS.dbo.exams
	where
		exam_key is not NULL
	and	upper(ltrim(rtrim(deleted))) <> 'Y'
	
	) Encounter


create unique nonclustered index #IX_TLoadEventExam on #TLoadEventExam
	(
	EventExamID ASC
	)
include
	(
	EncounterChecksum
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	CRIS.EventExam target
using
	(
	select
		 EventExamID
		,EventID
		,ExamTime
		,ExamCode
		,RoomCode
		,RadiologistCode
		,RadiographerCode1
		,RadiographerCode2
		,RadiographerCode3
		,BookedTime
		,AccessionCode
		,EncounterChecksum
	from
		#TLoadEventExam	
	) source
	on	source.EventExamID = target.EventExamID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			EventExamID
			,EventID
			,ExamTime
			,ExamCode
			,RoomCode
			,RadiologistCode
			,RadiographerCode1
			,RadiographerCode2
			,RadiographerCode3
			,BookedTime
			,AccessionCode

			,Created
			,ByWhom
			)
		values
			(
			source.EventExamID
			,source.EventID
			,source.ExamTime
			,source.ExamCode
			,source.RoomCode
			,source.RadiologistCode
			,source.RadiographerCode1
			,source.RadiographerCode2
			,source.RadiographerCode3
			,source.BookedTime
			,source.AccessionCode

			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> 
		CHECKSUM(
			target.EventID
			,target.ExamTime
			,target.ExamCode
			,target.RoomCode
			,target.RadiologistCode
			,target.RadiographerCode1
			,target.RadiographerCode2
			,target.RadiographerCode3
			,target.BookedTime
			,target.AccessionCode
		)
	then
		update
		set
			target.EventID = source.EventID
			,target.ExamTime = source.ExamTime
			,target.ExamCode = source.ExamCode
			,target.RoomCode = source.RoomCode
			,target.RadiologistCode = source.RadiologistCode
			,target.RadiographerCode1 = source.RadiographerCode1
			,target.RadiographerCode2 = source.RadiographerCode2
			,target.RadiographerCode3 = source.RadiographerCode3
			,target.BookedTime = source.BookedTime
			,target.AccessionCode = source.AccessionCode

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime