﻿create procedure CRIS.LoadExam as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@deleted int
	,@inserted int
	,@updated int

declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	CRIS.Exam target
using
	(
	select
		 ExamCode = cast([code] as varchar(16))
		,Exam = cast(examcd_name as varchar(80))
		,ModalityCode = cast(modality as varchar(2))
		,InterventionalCode = cast(interventional as varchar(2))
		,NumberOfExams = cast(no_exams as tinyint)
		,InactiveDate = cast(end_date as date)
		,GroupOneCode = cast(group_1 as varchar(4))
	from
		CRIS.dbo.examcd
	) source
	on	source.ExamCode = target.ExamCode

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 ExamCode
			,Exam
			,ModalityCode
			,InterventionalCode
			,NumberOfExams
			,InactiveDate
			,GroupOneCode
			,Created
			,ByWhom
			)
		values
			(
			 source.ExamCode
			,source.Exam
			,source.ModalityCode
			,source.InterventionalCode
			,source.NumberOfExams
			,source.InactiveDate
			,source.GroupOneCode
			,getdate()
			,suser_name()
			)

	when matched
	and
		checksum(
			 source.Exam
			,source.ModalityCode
			,source.InterventionalCode
			,source.NumberOfExams
			,source.InactiveDate
			,source.GroupOneCode
		)
		<>
		checksum(
			 target.Exam
			,target.ModalityCode
			,target.InterventionalCode
			,target.NumberOfExams
			,target.InactiveDate
			,target.GroupOneCode
		)
	then
		update
		set
			 target.Exam = source.Exam
			,target.ModalityCode = source.ModalityCode
			,target.InterventionalCode = source.InterventionalCode
			,target.NumberOfExams = source.NumberOfExams
			,target.InactiveDate = source.InactiveDate
			,target.GroupOneCode = source.GroupOneCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
output
	 $action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime
