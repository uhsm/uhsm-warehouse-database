﻿CREATE PROCEDURE [CRIS].[LoadExamOrder] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			PASLinkID
			,LeadClinicianCode
			,PathwayID
			,SequenceNumber
			,OrderID
			,ScheduleDateTime
			,ScheduledBy
			,EpisodeNumber
			,CasenoteNumber
			,ExamCode
			,UrgencyCode
			,WardCode
			,ReferralSourceCode
			,PatientTypeCode
			,RequestCatCode
			,ReferrerCode
			,BillReferrerCode
			,MobilityCode
			,LMP
			,Pregnant
			,ExamOrderStatusCode
			,EventExamID
			,Miscellaneous1
			,Miscellaneous2
			,CancelReason
			,Notes
			,SiteCode
			,AENumber
			,PatientHeight
			,PatientWeight
			,OrderSourceCode
			,OrderTypeCode
			,RequiredDateTime
			,ROAcknowledgeDate
			,ROAcknowledgeStateCode
			,ROAcknowledgeReason
			,RCAcknowledgeDate
			,RCAcknowledgeStateCode
			,RCAcknowledgeReason
			,RXAcknowledgeDate
			,RXAcknowledgeStateCode
			,RXAcknowledgeReason
			,SpecialtyCode
			,ClinicalHistory
			,ContactNumber
			,Reason
			,CreationDate
			,Questions
		)
into
	#TLoadExamOrder
from
	(
	select
		ExamOrderID
		,PASLinkID = cast(PASLinkID as int)
		,LeadClinicianCode = cast(nullif(LeadClinicianCode, '') as nvarchar(96))
		,PathwayID = cast(nullif(PathwayID, '') as nvarchar(128))
		,SequenceNumber = cast(nullif(SequenceNumber, '') as nvarchar(40))
		,OrderID = cast(nullif(OrderID, '') as nvarchar(120))
		,ScheduleDateTime = cast(ScheduleDateTime as datetime)
		,ScheduledBy = cast(nullif(ScheduledBy, '') as nvarchar(128))
		,EpisodeNumber = cast(nullif(EpisodeNumber, '') as nvarchar(120))
		,CasenoteNumber = cast(nullif(CasenoteNumber, '') as nvarchar(40))
		,ExamCode = cast(nullif(ExamCode, '') as nvarchar(32))
		,UrgencyCode = cast(nullif(UrgencyCode, '') as nvarchar(40))
		,WardCode = cast(nullif(WardCode, '') as nvarchar(60))
		,ReferralSourceCode = cast(nullif(ReferralSourceCode, '') as nvarchar(96))
		,PatientTypeCode = cast(nullif(PatientTypeCode, '') as nvarchar(4))
		,RequestCatCode = cast(nullif(RequestCatCode, '') as nvarchar(4))
		,ReferrerCode = cast(nullif(ReferrerCode, '') as nvarchar(60))
		,BillReferrerCode = cast(nullif(BillReferrerCode, '') as nvarchar(60))
		,MobilityCode = cast(nullif(MobilityCode, '') as nvarchar(4))
		,LMP = cast(LMP as datetime2)
		,Pregnant = cast(nullif(Pregnant, '') as nvarchar(4))
		,ExamOrderStatusCode = cast(ExamOrderStatusCode as int)
		,EventExamID = cast(EventExamID as int)
		,Miscellaneous1 = cast(nullif(Miscellaneous1, '') as nvarchar(160))
		,Miscellaneous2 = cast(nullif(Miscellaneous2, '') as nvarchar(160))
		,CancelReason = cast(nullif(CancelReason, '') as nvarchar(max))
		,Notes = cast(nullif(Notes, '') as nvarchar(max))
		,SiteCode = cast(nullif(SiteCode, '') as nvarchar(48))
		,AENumber = cast(nullif(AENumber, '') as nvarchar(80))
		,PatientHeight = cast(PatientHeight as numeric)
		,PatientWeight = cast(PatientWeight as numeric)
		,OrderSourceCode = cast(nullif(OrderSourceCode, '') as nvarchar(4))
		,OrderTypeCode = cast(nullif(OrderTypeCode, '') as nvarchar(4))
		,RequiredDateTime = cast(RequiredDateTime as datetime)
		,ROAcknowledgeDate = cast(ROAcknowledgeDate as datetime2)
		,ROAcknowledgeStateCode = cast(nullif(ROAcknowledgeStateCode, '') as nvarchar(4))
		,ROAcknowledgeReason = cast(nullif(ROAcknowledgeReason, '') as nvarchar(max))
		,RCAcknowledgeDate = cast(RCAcknowledgeDate as datetime2)
		,RCAcknowledgeStateCode = cast(nullif(RCAcknowledgeStateCode, '') as nvarchar(4))
		,RCAcknowledgeReason = cast(nullif(RCAcknowledgeReason, '') as nvarchar(max))
		,RXAcknowledgeDate = cast(RXAcknowledgeDate as datetime2)
		,RXAcknowledgeStateCode = cast(nullif(RXAcknowledgeStateCode, '') as nvarchar(4))
		,RXAcknowledgeReason = cast(nullif(RXAcknowledgeReason, '') as nvarchar(max))
		,SpecialtyCode = cast(nullif(SpecialtyCode, '') as nvarchar(96))
		,ClinicalHistory = cast(nullif(ClinicalHistory, '') as nvarchar(max))
		,ContactNumber = cast(nullif(ContactNumber, '') as nvarchar(96))
		,Reason = cast(nullif(Reason, '') as nvarchar(max))
		,CreationDate = cast(CreationDate as datetime2)
		,Questions = cast(nullif(Questions, '') as nvarchar(max))
	from
		(
		select
			ExamOrderID = order_key
			,PASLinkID = paslink_key
			,LeadClinicianCode = lead_clinician
			,PathwayID = pathway_id
			,SequenceNumber = orders_sequence
			,OrderID = order_id

			,ScheduleDateTime =
					cast(
						dateadd(
							minute

							,case
							when isnumeric(replace(replace(schedule_time, ':', ''), '.', '')) = 0 then 0
							when cast(replace(replace(schedule_time, ':', ''), '.', '') as int) not between 0 and 2359 then 0

							else
								cast(left(right('0000' + replace(replace(schedule_time, ':', ''), '.', ''), 4), 2) as int) * 60 +
								cast(right('0000' + replace(replace(schedule_time, ':', ''), '.', ''), 2) as int)	
							end

							,case 
							when schedule_date is null then schedule_date
							when schedule_date not between '1753-01-01 00:00' and '9999-12-31 23:59' then '1753-01-01 00:00'
							else schedule_date
							end
						)
			
						as datetime
					)

			,ScheduledBy = scheduled_by
			,EpisodeNumber = episode_number
			,CasenoteNumber = casenote_number
			,ExamCode = examination
			,UrgencyCode = urgency
			,WardCode = ward
			,ReferralSourceCode = referal_source
			,PatientTypeCode = patient_type
			,RequestCatCode = request_cat
			,ReferrerCode = referrer
			,BillReferrerCode = bill_referrer
			,MobilityCode = mobility
			,LMP = lmp
			,Pregnant = pregnant
			,ExamOrderStatusCode = status
			,EventExamID = exam_key
			,Miscellaneous1 = miscelaneous_1
			,Miscellaneous2 = miscelaneous_2
			,CancelReason = cancel_reason
			,Notes = notes
			,SiteCode = site
			,AENumber = a_e_number
			,PatientHeight = patient_height
			,PatientWeight = patient_weight
			,OrderSourceCode = order_source
			,OrderTypeCode = order_type

			,RequiredDateTime =
					cast(
						dateadd(
							minute

							,case
							when isnumeric(replace(replace(required_time, ':', ''), '.', '')) = 0 then 0
							when cast(replace(replace(required_time, ':', ''), '.', '') as int) not between 0 and 2359 then 0

							else
								cast(left(right('0000' + replace(replace(required_time, ':', ''), '.', ''), 4), 2) as int) * 60 +
								cast(right('0000' + replace(replace(required_time, ':', ''), '.', ''), 2) as int)	
							end

							,case 
							when required_date is null then required_date
							when required_date not between '1753-01-01 00:00' and '9999-12-31 23:59' then '1753-01-01 00:00'
							else required_date
							end
						)
			
						as datetime
					)

			,ROAcknowledgeDate = ro_ack_date
			,ROAcknowledgeStateCode = ro_ack_state
			,ROAcknowledgeReason = ro_ack_reason
			,RCAcknowledgeDate = rc_ack_date
			,RCAcknowledgeStateCode = rc_ack_state
			,RCAcknowledgeReason = rc_ack_reason
			,RXAcknowledgeDate = rx_ack_date
			,RXAcknowledgeStateCode = rx_ack_state
			,RXAcknowledgeReason = rx_ack_reason
			,SpecialtyCode = speciality
			,ClinicalHistory = clinical_history
			,ContactNumber = contact_number
			,Reason = reason
			,CreationDate = creation_date
			,Questions = questions
		from
			CRIS.dbo.orders
	
		) Encounter
	) Encounter


create unique nonclustered index #IX_TLoadExamOrder on #TLoadExamOrder
	(
	ExamOrderID ASC
	)
include
	(
	EncounterChecksum
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	CRIS.ExamOrder target
using
	(
	select
		ExamOrderID
		,PASLinkID
		,LeadClinicianCode
		,PathwayID
		,SequenceNumber
		,OrderID
		,ScheduleDateTime
		,ScheduledBy
		,EpisodeNumber
		,CasenoteNumber
		,ExamCode
		,UrgencyCode
		,WardCode
		,ReferralSourceCode
		,PatientTypeCode
		,RequestCatCode
		,ReferrerCode
		,BillReferrerCode
		,MobilityCode
		,LMP
		,Pregnant
		,ExamOrderStatusCode
		,EventExamID
		,Miscellaneous1
		,Miscellaneous2
		,CancelReason
		,Notes
		,SiteCode
		,AENumber
		,PatientHeight
		,PatientWeight
		,OrderSourceCode
		,OrderTypeCode
		,RequiredDateTime
		,ROAcknowledgeDate
		,ROAcknowledgeStateCode
		,ROAcknowledgeReason
		,RCAcknowledgeDate
		,RCAcknowledgeStateCode
		,RCAcknowledgeReason
		,RXAcknowledgeDate
		,RXAcknowledgeStateCode
		,RXAcknowledgeReason
		,SpecialtyCode
		,ClinicalHistory
		,ContactNumber
		,Reason
		,CreationDate
		,Questions
		,EncounterChecksum
	from
		#TLoadExamOrder	
	) source
	on	source.ExamOrderID = target.ExamOrderID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			ExamOrderID
			,PASLinkID
			,LeadClinicianCode
			,PathwayID
			,SequenceNumber
			,OrderID
			,ScheduleDateTime
			,ScheduledBy
			,EpisodeNumber
			,CasenoteNumber
			,ExamCode
			,UrgencyCode
			,WardCode
			,ReferralSourceCode
			,PatientTypeCode
			,RequestCatCode
			,ReferrerCode
			,BillReferrerCode
			,MobilityCode
			,LMP
			,Pregnant
			,ExamOrderStatusCode
			,EventExamID
			,Miscellaneous1
			,Miscellaneous2
			,CancelReason
			,Notes
			,SiteCode
			,AENumber
			,PatientHeight
			,PatientWeight
			,OrderSourceCode
			,OrderTypeCode
			,RequiredDateTime
			,ROAcknowledgeDate
			,ROAcknowledgeStateCode
			,ROAcknowledgeReason
			,RCAcknowledgeDate
			,RCAcknowledgeStateCode
			,RCAcknowledgeReason
			,RXAcknowledgeDate
			,RXAcknowledgeStateCode
			,RXAcknowledgeReason
			,SpecialtyCode
			,ClinicalHistory
			,ContactNumber
			,Reason
			,CreationDate
			,Questions

			,Created
			,ByWhom
			)
		values
			(
			source.ExamOrderID
			,source.PASLinkID
			,source.LeadClinicianCode
			,source.PathwayID
			,source.SequenceNumber
			,source.OrderID
			,source.ScheduleDateTime
			,source.ScheduledBy
			,source.EpisodeNumber
			,source.CasenoteNumber
			,source.ExamCode
			,source.UrgencyCode
			,source.WardCode
			,source.ReferralSourceCode
			,source.PatientTypeCode
			,source.RequestCatCode
			,source.ReferrerCode
			,source.BillReferrerCode
			,source.MobilityCode
			,source.LMP
			,source.Pregnant
			,source.ExamOrderStatusCode
			,source.EventExamID
			,source.Miscellaneous1
			,source.Miscellaneous2
			,source.CancelReason
			,source.Notes
			,source.SiteCode
			,source.AENumber
			,source.PatientHeight
			,source.PatientWeight
			,source.OrderSourceCode
			,source.OrderTypeCode
			,source.RequiredDateTime
			,source.ROAcknowledgeDate
			,source.ROAcknowledgeStateCode
			,source.ROAcknowledgeReason
			,source.RCAcknowledgeDate
			,source.RCAcknowledgeStateCode
			,source.RCAcknowledgeReason
			,source.RXAcknowledgeDate
			,source.RXAcknowledgeStateCode
			,source.RXAcknowledgeReason
			,source.SpecialtyCode
			,source.ClinicalHistory
			,source.ContactNumber
			,source.Reason
			,source.CreationDate
			,source.Questions

			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> 
		CHECKSUM(
			target.PASLinkID
			,target.LeadClinicianCode
			,target.PathwayID
			,target.SequenceNumber
			,target.OrderID
			,target.ScheduleDateTime
			,target.ScheduledBy
			,target.EpisodeNumber
			,target.CasenoteNumber
			,target.ExamCode
			,target.UrgencyCode
			,target.WardCode
			,target.ReferralSourceCode
			,target.PatientTypeCode
			,target.RequestCatCode
			,target.ReferrerCode
			,target.BillReferrerCode
			,target.MobilityCode
			,target.LMP
			,target.Pregnant
			,target.ExamOrderStatusCode
			,target.EventExamID
			,target.Miscellaneous1
			,target.Miscellaneous2
			,target.CancelReason
			,target.Notes
			,target.SiteCode
			,target.AENumber
			,target.PatientHeight
			,target.PatientWeight
			,target.OrderSourceCode
			,target.OrderTypeCode
			,target.RequiredDateTime
			,target.ROAcknowledgeDate
			,target.ROAcknowledgeStateCode
			,target.ROAcknowledgeReason
			,target.RCAcknowledgeDate
			,target.RCAcknowledgeStateCode
			,target.RCAcknowledgeReason
			,target.RXAcknowledgeDate
			,target.RXAcknowledgeStateCode
			,target.RXAcknowledgeReason
			,target.SpecialtyCode
			,target.ClinicalHistory
			,target.ContactNumber
			,target.Reason
			,target.CreationDate
			,target.Questions
		)
	then
		update
		set
			target.ExamOrderID = source.ExamOrderID
			,target.PASLinkID = source.PASLinkID
			,target.LeadClinicianCode = source.LeadClinicianCode
			,target.PathwayID = source.PathwayID
			,target.SequenceNumber = source.SequenceNumber
			,target.OrderID = source.OrderID
			,target.ScheduleDateTime = source.ScheduleDateTime
			,target.ScheduledBy = source.ScheduledBy
			,target.EpisodeNumber = source.EpisodeNumber
			,target.CasenoteNumber = source.CasenoteNumber
			,target.ExamCode = source.ExamCode
			,target.UrgencyCode = source.UrgencyCode
			,target.WardCode = source.WardCode
			,target.ReferralSourceCode = source.ReferralSourceCode
			,target.PatientTypeCode = source.PatientTypeCode
			,target.RequestCatCode = source.RequestCatCode
			,target.ReferrerCode = source.ReferrerCode
			,target.BillReferrerCode = source.BillReferrerCode
			,target.MobilityCode = source.MobilityCode
			,target.LMP = source.LMP
			,target.Pregnant = source.Pregnant
			,target.ExamOrderStatusCode = source.ExamOrderStatusCode
			,target.EventExamID = source.EventExamID
			,target.Miscellaneous1 = source.Miscellaneous1
			,target.Miscellaneous2 = source.Miscellaneous2
			,target.CancelReason = source.CancelReason
			,target.Notes = source.Notes
			,target.SiteCode = source.SiteCode
			,target.AENumber = source.AENumber
			,target.PatientHeight = source.PatientHeight
			,target.PatientWeight = source.PatientWeight
			,target.OrderSourceCode = source.OrderSourceCode
			,target.OrderTypeCode = source.OrderTypeCode
			,target.RequiredDateTime = source.RequiredDateTime
			,target.ROAcknowledgeDate = source.ROAcknowledgeDate
			,target.ROAcknowledgeStateCode = source.ROAcknowledgeStateCode
			,target.ROAcknowledgeReason = source.ROAcknowledgeReason
			,target.RCAcknowledgeDate = source.RCAcknowledgeDate
			,target.RCAcknowledgeStateCode = source.RCAcknowledgeStateCode
			,target.RCAcknowledgeReason = source.RCAcknowledgeReason
			,target.RXAcknowledgeDate = source.RXAcknowledgeDate
			,target.RXAcknowledgeStateCode = source.RXAcknowledgeStateCode
			,target.RXAcknowledgeReason = source.RXAcknowledgeReason
			,target.SpecialtyCode = source.SpecialtyCode
			,target.ClinicalHistory = source.ClinicalHistory
			,target.ContactNumber = source.ContactNumber
			,target.Reason = source.Reason
			,target.CreationDate = source.CreationDate
			,target.Questions = source.Questions

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime