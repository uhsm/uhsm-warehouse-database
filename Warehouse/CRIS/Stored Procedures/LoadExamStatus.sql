﻿CREATE PROCEDURE [CRIS].[LoadExamStatus] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			PatientID
			,EventID
			,EventExamID
			,StatusCategoryCode
			,StatusCurrentCode
			,StatusCode
			,StatusTypeCode
			,StatusTime
			,UserID
			,StartTime
			,EndTime
			,ExamCode
			,SiteCode
			,Remark
			,Comment
		)
into
	#TLoadExamStatus
from
	(
	select
		 ExamStatusID = cast([status_key] as int)
		,PatientID = cast([computer_number] as int)
		,EventID = cast([event_key] as int)
		,EventExamID = cast([exam_key] as int)
		,StatusCategoryCode = cast([status_category] as varchar(4))
		,StatusCurrentCode = cast([status_current] as varchar(2))
		,StatusCode = cast([status_code] as varchar(6))
		,StatusTypeCode = cast(status_type as varchar(6))

		,StatusTime =
			cast(
				dateadd(
					minute

					,case
					when isnumeric(replace(replace(status_time, ':', ''), '.', '')) = 0 then 0
					when cast(replace(replace(status_time, ':', ''), '.', '') as int) not between 0 and 2359 then 0

					else
						cast(left(right('0000' + replace(replace(status_time, ':', ''), '.', ''), 4), 2) as int) * 60 +
						cast(right('0000' + replace(replace(status_time, ':', ''), '.', ''), 2) as int)	
					end

					,case 
					when [status_date] is null then [status_date]
					when [status_date] not between '1753-01-01 00:00' and '9999-12-31 23:59' then '1753-01-01 00:00'
					else [status_date]
					end
				)
			
				as datetime
			)

		,UserID = cast([user_id] as varchar(20))

		,StartTime =
			cast(
				dateadd(
					minute

					,case
					when isnumeric(replace(replace(start_time, ':', ''), '.', '')) = 0 then 0
					when cast(replace(replace(start_time, ':', ''), '.', '') as int) not between 0 and 2359 then 0

					else
						cast(left(right('0000' + replace(replace(start_time, ':', ''), '.', ''), 4), 2) as int) * 60 +
						cast(right('0000' + replace(replace(start_time, ':', ''), '.', ''), 2) as int)	
					end

					,case 
					when [start_date] is null then [start_date]
					when [start_date] not between '1753-01-01 00:00' and '9999-12-31 23:59' then '1753-01-01 00:00'
					else [start_date]
					end
				)
				as datetime
			)

		,EndTime =
			cast(
				dateadd(
					minute

					,case
					when isnumeric(replace(replace(end_time, ':', ''), '.', '')) = 0 then 0
					when cast(replace(replace(end_time, ':', ''), '.', '') as int) not between 0 and 2359 then 0

					else
						cast(left(right('0000' + replace(replace(end_time, ':', ''), '.', ''), 4), 2) as int) * 60 +
						cast(right('0000' + replace(replace(end_time, ':', ''), '.', ''), 2) as int)	
					end

					,case 
					when [end_date] is null then [end_date]
					when [end_date] not between '1753-01-01 00:00' and '9999-12-31 23:59' then '1753-01-01 00:00'
					else end_date
					end
				)
				as datetime
			)

		,ExamCode = cast(examination as varchar(16))
		,SiteCode = cast([site] as varchar(5))
		,Remark = cast([other_code] as varchar(24))
		,Comment = cast([description] as varchar(max))
	from 
		CRIS.dbo.status
	where
		upper(ltrim(rtrim(deleted))) <> 'Y'

	) Encounter


create unique nonclustered index #IX_TLoadEvent on #TLoadExamStatus
	(
	ExamStatusID ASC
	)
include
	(
	EncounterChecksum
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	CRIS.ExamStatus target
using
	(
	select
		 ExamStatusID
		,PatientID
		,EventID
		,EventExamID
		,StatusCategoryCode
		,StatusCurrentCode
		,StatusCode
		,StatusTypeCode
		,StatusTime
		,UserID
		,StartTime
		,EndTime
		,ExamCode
		,SiteCode
		,Remark
		,Comment
		,EncounterChecksum
	from
		#TLoadExamStatus
	) source
	on	source.ExamStatusID = target.ExamStatusID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 ExamStatusID
			,PatientID
			,EventID
			,EventExamID
			,StatusCategoryCode
			,StatusCurrentCode
			,StatusCode
			,StatusTypeCode
			,StatusTime
			,UserID
			,StartTime
			,EndTime
			,ExamCode
			,SiteCode
			,Remark
			,Comment
			,Created
			,ByWhom
			)
		values
			(
			 source.ExamStatusID
			,source.PatientID
			,source.EventID
			,source.EventExamID
			,source.StatusCategoryCode
			,source.StatusCurrentCode
			,source.StatusCode
			,source.StatusTypeCode
			,source.StatusTime
			,source.UserID
			,source.StartTime
			,source.EndTime
			,source.ExamCode
			,source.SiteCode
			,source.Remark
			,source.Comment
			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> 
		CHECKSUM(
				 target.PatientID
				,target.EventID
				,target.EventExamID
				,target.StatusCategoryCode
				,target.StatusCurrentCode
				,target.StatusCode
				,target.StatusTypeCode
				,target.StatusTime
				,target.UserID
				,target.StartTime
				,target.EndTime
				,target.ExamCode
				,target.SiteCode
				,target.Remark
				,target.Comment
				)
	then
		update
		set
			 target.PatientID = source.PatientID
			,target.EventID = source.EventID
			,target.EventExamID = source.EventExamID
			,target.StatusCategoryCode = source.StatusCategoryCode
			,target.StatusCurrentCode = source.StatusCurrentCode
			,target.StatusCode = source.StatusCode
			,target.StatusTypeCode = source.StatusTypeCode
			,target.StatusTime = source.StatusTime
			,target.UserID = source.UserID
			,target.StartTime = source.StartTime
			,target.EndTime = source.EndTime
			,target.ExamCode = source.ExamCode
			,target.SiteCode = source.SiteCode
			,target.Remark = source.Remark
			,target.Comment = source.Comment
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime