﻿CREATE PROCEDURE [CRIS].[LoadPatient] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			PatientSurname							
			,PatientForename						
			,NHSNumber						
			,PatientTitle							
			,SexCode								
			,PatientAddressLine1					
			,PatientAddressLine2						 
			,PatientAddressLine3	
			,PatientAddressLine4					
			,PatientPostcode						
			,RegisteredGpCode							
			,EthnicCategoryCode	
			,DateOfDeath			
			,DateOfBirth						
			,HomeTelephone					
			,WorkTelephone					 
			,MobileTelephone				 
			,IsOKToCallMobile	
		)
into
	#TLoadPatient
from
	(
	select
		 PatientID = cast(computer_number as int)
		,PatientSurname = cast(surname as varchar(50))
		,PatientForename = cast(forenames as varchar(50))
		,NHSNumber = cast(replace(nhs_number,' ','') as varchar(10))
		,PatientTitle = cast(title as varchar(12))
		,SexCode = cast(sex as varchar(1))
		,PatientAddressLine1 = cast(address_1 as varchar(80))
		,PatientAddressLine2 = cast(address_2 as varchar(80))
		,PatientAddressLine3 = cast(address_3 as varchar(50))
		,PatientAddressLine4 = cast(address_4 as varchar(50))
		,PatientPostcode = cast(postcode_1 + ' ' + postcode_2 as varchar(8))
		,RegisteredGpCode = cast(gp as varchar(10))
		,EthnicCategoryCode = cast(ethnic_origin as varchar(6))
		,DateOfDeath = cast(date_of_death as date)
		,DateOfBirth = cast(dob as date)
		,HomeTelephone = cast(telephone as varchar(30))
		,WorkTelephone = cast(telephone2 as varchar(30))
		,MobileTelephone = cast(tel_mobile as varchar(30))

		,IsOKToCallMobile = 
			cast(
				case ltrim(rtrim(tel_mobile_consent))
				when 'Y'
				then 1
				else 0
				end
				as bit
			)

	from
		CRIS.dbo.patient Patient
	where
		upper(ltrim(rtrim(deleted))) <> 'Y'

	) Encounter

create unique nonclustered index #IX_TLoadPatient on #TLoadPatient
	(
	PatientID ASC
	)

include
	(
	EncounterChecksum
	)

declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	CRIS.Patient target
using
	(
	select
		 PatientID				
		,PatientSurname							
		,PatientForename						
		,NHSNumber						
		,PatientTitle							
		,SexCode								
		,PatientAddressLine1					
		,PatientAddressLine2						 
		,PatientAddressLine3	
		,PatientAddressLine4					
		,PatientPostcode						
		,RegisteredGpCode							
		,EthnicCategoryCode	
		,DateOfDeath			
		,DateOfBirth						
		,HomeTelephone					
		,WorkTelephone					 
		,MobileTelephone				 
		,IsOKToCallMobile	
		,EncounterChecksum
	from
		#TLoadPatient
	
	) source
	on	source.PatientID = target.PatientID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 PatientID				
			,PatientSurname							
			,PatientForename						
			,NHSNumber						
			,PatientTitle							
			,SexCode								
			,PatientAddressLine1					
			,PatientAddressLine2						 
			,PatientAddressLine3	
			,PatientAddressLine4					
			,PatientPostcode						
			,RegisteredGpCode							
			,EthnicCategoryCode	
			,DateOfDeath			
			,DateOfBirth						
			,HomeTelephone					
			,WorkTelephone					 
			,MobileTelephone				 
			,IsOKToCallMobile	
			,Created
			,ByWhom
			)
		values
			(
			 source.PatientID	
			,source.PatientSurname							
			,source.PatientForename						
			,source.NHSNumber						
			,source.PatientTitle							
			,source.SexCode								
			,source.PatientAddressLine1					
			,source.PatientAddressLine2						 
			,source.PatientAddressLine3	
			,source.PatientAddressLine4					
			,source.PatientPostcode						
			,source.RegisteredGpCode							
			,source.EthnicCategoryCode	
			,source.DateOfDeath			
			,source.DateOfBirth						
			,source.HomeTelephone					
			,source.WorkTelephone					 
			,source.MobileTelephone				 
			,source.IsOKToCallMobile
			,getdate()
			,suser_name()	
			)

	when matched
	and source.EncounterChecksum <> 
		CHECKSUM(
			target.PatientSurname							
			,target.PatientForename						
			,target.NHSNumber						
			,target.PatientTitle							
			,target.SexCode								
			,target.PatientAddressLine1					
			,target.PatientAddressLine2						 
			,target.PatientAddressLine3	
			,target.PatientAddressLine4					
			,target.PatientPostcode						
			,target.RegisteredGpCode							
			,target.EthnicCategoryCode	
			,target.DateOfDeath			
			,target.DateOfBirth						
			,target.HomeTelephone					
			,target.WorkTelephone					 
			,target.MobileTelephone				 
			,target.IsOKToCallMobile
		)
	then
		update
		set			
			target.PatientSurname = source.PatientSurname
			,target.PatientForename	= source.PatientForename
			,target.NHSNumber = source.NHSNumber
			,target.PatientTitle = source.PatientTitle
			,target.SexCode	= source.SexCode
			,target.PatientAddressLine1 = source.PatientAddressLine1
			,target.PatientAddressLine2 = source.PatientAddressLine2
			,target.PatientAddressLine3 = source.PatientAddressLine3
			,target.PatientAddressLine4 = source.PatientAddressLine4
			,target.PatientPostcode = source.PatientPostcode
			,target.RegisteredGpCode = source.RegisteredGpCode
			,target.EthnicCategoryCode = source.EthnicCategoryCode
			,target.DateOfDeath	= source.DateOfDeath
			,target.DateOfBirth	= source.DateOfBirth
			,target.HomeTelephone = source.HomeTelephone
			,target.WorkTelephone = source.WorkTelephone
			,target.MobileTelephone	= source.MobileTelephone
			,target.IsOKToCallMobile = source.IsOKToCallMobile
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime