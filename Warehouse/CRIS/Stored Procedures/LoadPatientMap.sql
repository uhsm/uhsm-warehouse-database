﻿create procedure CRIS.LoadPatientMap as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@deleted int
	,@inserted int
	,@updated int

declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	CRIS.PatientMap target
using
	(
	select
		 DistrictNumber = cast(his_id as varchar(30))
		,PatientID = cast(computer_number as int)
		,NHSNumber = cast(replace(nhs_number,' ','') as varchar(10))
	from
		CRIS.dbo.his
	where
		his_id is not null
	) source
	on	source.DistrictNumber = target.DistrictNumber

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 DistrictNumber
			,PatientID
			,NHSNumber
			,Created
			,ByWhom
			)
		values
			(
			 source.DistrictNumber
			,source.PatientID
			,source.NHSNumber
			,getdate()
			,suser_name()
			)

	when matched
	and
		checksum(
			 source.PatientID
			,source.NHSNumber
		)
		<>
		checksum(
			 target.PatientID
			,target.NHSNumber
		)
	then
		update
		set
			 target.PatientID = source.PatientID
			,target.NHSNumber = source.NHSNumber
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
output
	 $action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime

