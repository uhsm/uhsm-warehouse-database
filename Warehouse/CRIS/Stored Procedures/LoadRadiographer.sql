﻿create procedure CRIS.LoadRadiographer as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@deleted int
	,@inserted int
	,@updated int

declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	CRIS.Radiographer target
using
	(
	select
		 RadiographerCode = cast(code as varchar(20))
		,Radiographer =  cast(radiog_name as varchar(80))
		,EndDate = cast(end_date as date)
	from
		CRIS.dbo.radiog
	where
		code is not null
	) source
	on	source.RadiographerCode = target.RadiographerCode

	when not matched by source
	then delete

	when not matched
	then
		insert
		(
			 RadiographerCode
			,Radiographer
			,EndDate
			,Created
			,ByWhom
		)
		values
		(
			 source.RadiographerCode
			,source.Radiographer
			,source.EndDate
			,getdate()
			,suser_name()
		)

	when matched
	and
		checksum(
			 source.Radiographer
			,source.EndDate
		)
		<>
		checksum(
			 target.Radiographer
			,target.EndDate
		)
	then
		update
		set
			 target.Radiographer = source.Radiographer 
			,target.EndDate = source.EndDate
			,target.Updated = getdate()
			,target.ByWhom = suser_name()		

output
	 $action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime
