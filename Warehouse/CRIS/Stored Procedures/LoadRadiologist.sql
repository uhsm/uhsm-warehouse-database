﻿create procedure CRIS.LoadRadiologist as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@deleted int
	,@inserted int
	,@updated int

declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	CRIS.Radiologist target
using
	(
	select
		 RadiologistCode = cast(replace(code, ' ', '*') as varchar(20)) --spaces are replaced with * to ensure uniqueness
		,Radiologist = cast(radiol_name as varchar(20))
		,RadiologistType = cast(radiol_type as varchar(12))
		,Unverified = cast(unverified as int)
		,Suspended = cast(suspended as int)
		,Unprinted = cast(unprinted as int)
		,EndDate = cast(case when end_date between '1753-01-01 00:00' and '9999-12-31 23:59' then end_date else '1753-01-01 00:00' end as date)
	from
		CRIS.dbo.radiol
	where
		radiol.code is not null

	) source
	on	source.RadiologistCode = target.RadiologistCode

	when not matched by source
	then delete

	when not matched
	then
		insert
		(
			 RadiologistCode
			,Radiologist
			,RadiologistType
			,Unverified
			,Suspended
			,Unprinted
			,EndDate
			,Created
			,ByWhom
		)
		values
		(
			 source.RadiologistCode
			,source.Radiologist
			,source.RadiologistType
			,source.Unverified
			,source.Suspended
			,source.Unprinted
			,source.EndDate
			,getdate()
			,suser_name()
		)

	when matched
	and
		checksum(
			 source.Radiologist
			,source.RadiologistType
			,source.Unverified
			,source.Suspended
			,source.Unprinted
			,source.EndDate
		)
		<>
		checksum(
			 target.Radiologist
			,target.RadiologistType
			,target.Unverified
			,target.Suspended
			,target.Unprinted
			,target.EndDate
		)
	then
		update
		set
			 target.Radiologist = source.Radiologist
			,target.RadiologistType = source.RadiologistType
			,target.Unverified = source.Unverified
			,target.Suspended = source.Suspended
			,target.Unprinted = source.Unprinted
			,target.EndDate = source.EndDate
			,target.Updated = getdate()
			,target.ByWhom = suser_name()		

output
	 $action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime
