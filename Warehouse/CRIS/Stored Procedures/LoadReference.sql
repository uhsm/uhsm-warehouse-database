﻿create procedure CRIS.LoadReference as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@deleted int
	,@inserted int
	,@updated int

declare @ProcessList table
	(
	Action nvarchar(10)
	)

;
with unpivotCode as 
	(
	select
		DomainCode
		,Domain
		,IsDeleted
		,CreatedDateTime
		,UpdatedDateTime

		,ColumnID =
			cast(
				replace(
					SourceCodeColumn
					,'valchar_'
					,''
				)

				as int
			)

		,SourceCode

	from
		(
		select
			DomainCode = tablecode
			,Domain = description

			,IsDeleted =
					cast(
					case
					when deleted = 'Y'
					then 1
					else 0
					end
					
					as bit
					)

			,CreatedDateTime =
				cast(
					cast(
						dateadd(
							second
							,creation_time
							,'1 jan 2000' --according to postgresql documentation: values are stored as seconds before or after midnight 2000-01-01
						)
						as time
					)
					as datetime
				) + creation_date


			,UpdatedDateTime =
				cast(
					cast(
						dateadd(
							second
							,mutation_time
							,'1 jan 2000' --according to postgresql documentation: values are stored as seconds before or after midnight 2000-01-01
						)
						as time
					)
					as datetime
				) + mutation_date

			,valchar_1 = isnull(valchar_1, '-1')
			,valchar_2 = isnull(valchar_2, '-1')
			,valchar_3 = isnull(valchar_3, '-1')
			,valchar_4 = isnull(valchar_4, '-1')
			,valchar_5 = isnull(valchar_5, '-1')
			,valchar_6 = isnull(valchar_6, '-1')
			,valchar_7 = isnull(valchar_7, '-1')
			,valchar_8 = isnull(valchar_8, '-1')
			,valchar_9 = isnull(valchar_9, '-1')
		from
			CRIS.dbo.hdslmt sourcetable
		) sourcetable

	unpivot (
		SourceCode for SourceCodeColumn in 
		(
		valchar_1
		,valchar_2
		,valchar_3
		,valchar_4
		,valchar_5
		,valchar_6
		,valchar_7
		,valchar_8
		,valchar_9
		)
	) Codes
)

,unpivotValue as 
	(
	select
		DomainCode

		,ColumnID =
			cast(
				replace(
					SourceValueColumn
					,'valtxt_'
					,''
				)

				as int
			)

		,SourceValue

	from
		(
		select
			DomainCode = tablecode
			,valtxt_1 = isnull(valtxt_1, '-1')
			,valtxt_2 = isnull(valtxt_2, '-1')
			,valtxt_3 = isnull(valtxt_3, '-1')
			,valtxt_4 = isnull(valtxt_4, '-1')
			,valtxt_5 = isnull(valtxt_5, '-1')
			,valtxt_6 = isnull(valtxt_6, '-1')
			,valtxt_7 = isnull(valtxt_7, '-1')
			,valtxt_8 = isnull(valtxt_8, '-1')
			,valtxt_9 = isnull(valtxt_9, '-1')

		from
			CRIS.dbo.hdslmt sourcetable
		) sourcetable

	unpivot (
		SourceValue for SourceValueColumn in 
		(
		valtxt_1
		,valtxt_2
		,valtxt_3
		,valtxt_4
		,valtxt_5
		,valtxt_6
		,valtxt_7
		,valtxt_8
		,valtxt_9
		)
	) Value
)

merge
	CRIS.Reference target
using
	(

	select
		DomainCode = cast(nullif(unpivotCode.DomainCode, '') as varchar(16))
		,SourceCode = cast(nullif(unpivotCode.SourceCode, '') as varchar(2))
		,SourceValue = cast(nullif(unpivotValue.SourceValue, '') as varchar(30))
		,Domain = cast(nullif(unpivotCode.Domain, '') as varchar(80))
		,CreatedDateTime = cast(unpivotCode.CreatedDateTime as datetime)
		,UpdatedDateTime = cast(unpivotCode.UpdatedDateTime as datetime)
	from
		unpivotCode

	inner join unpivotValue
	on	unpivotValue.DomainCode = unpivotCode.DomainCode
	and	unpivotValue.ColumnID = unpivotCode.ColumnID

	where
		not
		(
			unpivotCode.SourceCode = '-1'
		and	unpivotValue.SourceValue = '-1'
		)

	and	unpivotCode.IsDeleted = 0

	) source
	on	source.DomainCode = target.DomainCode
	and	source.SourceCode = target.SourceCode

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			DomainCode
			,SourceCode
			,SourceValue
			,Domain
			,CreatedDateTime
			,UpdatedDateTime
			,Created
			,ByWhom
			)
		values
			(
			source.DomainCode
			,source.SourceCode
			,source.SourceValue
			,source.Domain
			,source.CreatedDateTime
			,source.UpdatedDateTime
			,getdate()
			,suser_name()
			)

	when matched
	and
		checksum(
			source.SourceValue
			,source.Domain
			,source.CreatedDateTime
			,source.UpdatedDateTime
		)
		<>
		checksum(
			target.SourceValue
			,target.Domain
			,target.CreatedDateTime
			,target.UpdatedDateTime
		)
	then
		update
		set
			target.SourceValue = source.SourceValue
			,target.Domain = source.Domain
			,target.CreatedDateTime = source.CreatedDateTime
			,target.UpdatedDateTime = source.UpdatedDateTime
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 $action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime
