﻿create procedure CRIS.LoadReferenceMap as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@deleted int
	,@inserted int
	,@updated int

declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	CRIS.ReferenceMap target
using
	(
	select
		DomainCode
		,ReferenceMapID
		,SourceCode = cast(nullif(SourceCode, '') as varchar(48))
		,MapCode1 = cast(nullif(MapCode1, '') as varchar(48))
		,MapCode2 = cast(nullif(MapCode2, '') as varchar(48))
		,MapCode3 = cast(nullif(MapCode3, '') as varchar(48))
		,MapCode4 = cast(nullif(MapCode4, '') as varchar(48))
		,MapCode5 = cast(nullif(MapCode5, '') as varchar(48))
		,MapCode6 = cast(nullif(MapCode6, '') as varchar(48))
		,MapCode7 = cast(nullif(MapCode7, '') as varchar(48))
		,MapCode8 = cast(nullif(MapCode8, '') as varchar(48))
		,MapCode9 = cast(nullif(MapCode9, '') as varchar(48))
		,MapCode10 = cast(nullif(MapCode10, '') as varchar(48))
		,SourceValue = cast(nullif(SourceValue, '') as varchar(255))
		,CreatedDateTime = cast(CreatedDateTime as datetime2)
		,UpdatedDateTime = cast(UpdatedDateTime as datetime2)
	from
		(
		select
			DomainCode = table_number
			,ReferenceMapID = entry_number
			,SourceCode = return_value
			,MapCode1 = value_1
			,MapCode2 = value_2
			,MapCode3 = value_3
			,MapCode4 = value_4
			,MapCode5 = value_5
			,MapCode6 = value_6
			,MapCode7 = value_7
			,MapCode8 = value_8
			,MapCode9 = value_9
			,MapCode10 = value_10
			,SourceValue = description

			,CreatedDateTime =
				cast(
					left(convert(varchar, creation_date, 113), 11)
					+ ' ' +
					left(
						right(
							convert(
								varchar
					
								,dateadd(
									second
									,creation_time
									,'1 jan 2000' --according to postgresql documentation: values are stored as seconds before or after midnight 2000-01-01
								)

								,113
							)
							,12
						)
						,8
					)
					as datetime2
				)

			,UpdatedDateTime =
				cast(
					left(convert(varchar, mutation_date, 113), 11)
					+ ' ' +
					left(
						right(
							convert(
								varchar
					
								,dateadd(
									second
									,mutation_time
									,'1 jan 2000' --according to postgresql documentation: values are stored as seconds before or after midnight 2000-01-01
								)

								,113
							)
							,12
						)
						,8
					)
					as datetime2
				)
		from
			CRIS.dbo.hdstbc
		where
			deleted <> 'Y'
		) source
	) source
	on	source.DomainCode = target.DomainCode
	and	source.ReferenceMapID = target.ReferenceMapID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			DomainCode
			,ReferenceMapID
			,SourceCode
			,MapCode1
			,MapCode2
			,MapCode3
			,MapCode4
			,MapCode5
			,MapCode6
			,MapCode7
			,MapCode8
			,MapCode9
			,MapCode10
			,SourceValue
			,CreatedDateTime
			,UpdatedDateTime
			,Created
			,ByWhom
			)
		values
			(
			source.DomainCode
			,source.ReferenceMapID
			,source.SourceCode
			,source.MapCode1
			,source.MapCode2
			,source.MapCode3
			,source.MapCode4
			,source.MapCode5
			,source.MapCode6
			,source.MapCode7
			,source.MapCode8
			,source.MapCode9
			,source.MapCode10
			,source.SourceValue
			,source.CreatedDateTime
			,source.UpdatedDateTime
			,getdate()
			,suser_name()
			)

	when matched
	and
		checksum(
			source.SourceCode
			,source.MapCode1
			,source.MapCode2
			,source.MapCode3
			,source.MapCode4
			,source.MapCode5
			,source.MapCode6
			,source.MapCode7
			,source.MapCode8
			,source.MapCode9
			,source.MapCode10
			,source.SourceValue
			,source.CreatedDateTime
			,source.UpdatedDateTime
		)
		<>
		checksum(
			target.SourceCode
			,target.MapCode1
			,target.MapCode2
			,target.MapCode3
			,target.MapCode4
			,target.MapCode5
			,target.MapCode6
			,target.MapCode7
			,target.MapCode8
			,target.MapCode9
			,target.MapCode10
			,target.SourceValue
			,target.CreatedDateTime
			,target.UpdatedDateTime
		)
	then
		update
		set
			target.SourceCode = source.SourceCode
			,target.MapCode1 = source.MapCode1
			,target.MapCode2 = source.MapCode2
			,target.MapCode3 = source.MapCode3
			,target.MapCode4 = source.MapCode4
			,target.MapCode5 = source.MapCode5
			,target.MapCode6 = source.MapCode6
			,target.MapCode7 = source.MapCode7
			,target.MapCode8 = source.MapCode8
			,target.MapCode9 = source.MapCode9
			,target.MapCode10 = source.MapCode10
			,target.SourceValue = source.SourceValue
			,target.CreatedDateTime = source.CreatedDateTime
			,target.UpdatedDateTime = source.UpdatedDateTime
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 $action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime
