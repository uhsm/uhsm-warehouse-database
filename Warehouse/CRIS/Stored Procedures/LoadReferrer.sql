﻿create procedure CRIS.LoadReferrer as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@deleted int
	,@inserted int
	,@updated int

declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	CRIS.Referrer target
using
	(
	select
		 ReferrerCode = cast(replace(code, ' ', '*') as varchar(10)) --spaces are replaced with * to ensure uniqueness
		,StartDate = cast(start_date as date)
		,Referrer = cast(referer_name as varchar(158))
		,ReferrerTypeCode = cast(referer_type as varchar(1))
		,EndDate = cast(end_date as date)
		,ReferrerSpecialityCode = cast(speciality as varchar(60))
		,ReferrerGroupCode =cast(referer_group as varchar(3))
		,ReferrerNationalCode=cast(national_code as varchar(30))
		,SendElectronicReport =cast(send_edi as varchar(1))
	from
		CRIS.dbo.referer 
	where
		code is not null
	) source
	on	source.ReferrerCode = target.ReferrerCode

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 ReferrerCode 
			,StartDate 
			,Referrer 
			,ReferrerTypeCode 
			,EndDate 
			,ReferrerSpecialityCode 
			,ReferrerGroupCode 
			,ReferrerNationalCode 
			,SendElectronicReport 
			,Created
			,ByWhom
			)
		values
			(
			 source.ReferrerCode 
			,source.StartDate 
			,source.Referrer 
			,source.ReferrerTypeCode 
			,source.EndDate 
			,source.ReferrerSpecialityCode 
			,source.ReferrerGroupCode 
			,source.ReferrerNationalCode 
			,source.SendElectronicReport 								
			,getdate()
			,suser_name()
			)

	when matched
	and
		checksum(
			 source.StartDate
			,source.Referrer
			,source.ReferrerTypeCode
			,source.EndDate
			,source.ReferrerSpecialityCode
			,source.ReferrerGroupCode
			,source.ReferrerNationalCode
			,source.SendElectronicReport
		)
		<>
		checksum(
			 target.StartDate
			,target.Referrer
			,target.ReferrerTypeCode
			,target.EndDate
			,target.ReferrerSpecialityCode
			,target.ReferrerGroupCode
			,target.ReferrerNationalCode
			,target.SendElectronicReport
		)
	then
		update
		set
			 target.StartDate = source.StartDate 
			,target.Referrer = source.Referrer
			,target.ReferrerTypeCode = source.ReferrerTypeCode 
			,target.EndDate = source.EndDate 
			,target.ReferrerSpecialityCode = source.ReferrerSpecialityCode 
			,target.ReferrerGroupCode = source.ReferrerGroupCode
			,target.ReferrerNationalCode = source.ReferrerNationalCode 
			,target.SendElectronicReport = source.SendElectronicReport 
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 $action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime

