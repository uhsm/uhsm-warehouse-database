﻿create procedure CRIS.LoadReferrerOrganisation as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@deleted int
	,@inserted int
	,@updated int

declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	CRIS.ReferrerOrganisation target
using
	(
	select
		 ReferrerOrganisationCode = cast([code] as varchar(10))
		,Referrer = cast(refsrc_name as varchar(158))
		,GroupCode = cast(refsrc_group as varchar(3))
		,NationalCode = cast(national_code as varchar(30))
		,TypeCode = cast(refsrc_type as varchar(2))
		,AddressLine1 = cast(address_1 as varchar(80))
		,AddressLine2 = cast(address_2 as varchar(80))
		,AddressLine3 = cast(address_3 as varchar(50))
		,AddressLine4 = cast(address_4 as varchar(50))
		,AddressLine5 = cast(address_5 as varchar(50))

		,Postcode = cast(postcode_1 + ' ' + postcode_2 as varchar(8))

		,SendElectronicReport = cast(send_edi as varchar(2))

		,InactiveDate = 
			cast(
				case 
				when end_date not between '1753-01-01 00:00' and '9999-12-31 23:59' then '1753-01-01 00:00'
				else cast(end_date as datetime)
				end

				as date
			)

		,AreaCode = cast(pct as varchar(5))
	from
		CRIS.dbo.refsrc
	where
		[code] is not null

	) source
	on	source.ReferrerOrganisationCode = target.ReferrerOrganisationCode

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 ReferrerOrganisationCode
			,ReferrerOrganisation
			,GroupCode
			,NationalCode
			,TypeCode
			,AddressLine1
			,AddressLine2
			,AddressLine3
			,AddressLine4
			,AddressLine5
			,Postcode
			,SendElectronicReport
			,InactiveDate
			,AreaCode
			,Created
			,ByWhom
			)
		values
			(
			 source.ReferrerOrganisationCode
			,source.Referrer
			,source.GroupCode
			,source.NationalCode
			,source.TypeCode
			,source.AddressLine1
			,source.AddressLine2
			,source.AddressLine3
			,source.AddressLine4
			,source.AddressLine5
			,source.Postcode
			,source.SendElectronicReport
			,source.InactiveDate
			,source.AreaCode
			,getdate()
			,suser_name()
			)

	when matched
	and
		checksum(
			 source.Referrer
			,source.GroupCode
			,source.NationalCode
			,source.TypeCode
			,source.AddressLine1
			,source.AddressLine2
			,source.AddressLine3
			,source.AddressLine4
			,source.AddressLine5
			,source.Postcode
			,source.SendElectronicReport
			,source.InactiveDate
			,source.AreaCode
		)
		<>
		checksum(
			 target.ReferrerOrganisation
			,target.GroupCode
			,target.NationalCode
			,target.TypeCode
			,target.AddressLine1
			,target.AddressLine2
			,target.AddressLine3
			,target.AddressLine4
			,target.AddressLine5
			,target.Postcode
			,target.SendElectronicReport
			,target.InactiveDate
			,target.AreaCode
		)
	then
		update
		set
			 target.ReferrerOrganisation = source.Referrer
			,target.GroupCode = source.GroupCode
			,target.NationalCode = source.NationalCode
			,target.TypeCode = source.TypeCode
			,target.AddressLine1 = source.AddressLine1
			,target.AddressLine2 = source.AddressLine2
			,target.AddressLine3 = source.AddressLine3
			,target.AddressLine4 = source.AddressLine4
			,target.AddressLine5 = source.AddressLine5
			,target.Postcode = source.Postcode
			,target.SendElectronicReport = source.SendElectronicReport
			,target.InactiveDate = source.InactiveDate
			,target.AreaCode = source.AreaCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 $action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime

