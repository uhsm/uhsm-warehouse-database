﻿CREATE PROCEDURE [CRIS].[LoadReport]

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = object_schema_name(@@procid) + '.' + object_name(@@procid)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		checksum(
			EventExamID
			,ReportCode
			,IsVerified
			,Report
			,ReportedTime
			,ReportedByCode
			,TypedTime
			,TypedByCode
			,ChangedDate
			,ChangedByCode
			,IsPrinted
			,Sent
			,ReportsRestricted
			,LastAddedBy
			,LastAddendumTime
			,AddendumByCode
			,AddendumTime
			,NumberOfFoetus
			,ReportedByCode2
			,VerifiedByCode
			,VerifiedTime
			,VerifiedCheckedCode
			,VerifiedCheckTime
			,LastVerifiedCode
			,LastVerifiedTime
			,PrintedDate
			,Priority
			,SentTime
			,AcknowledgedTime
			,StatusCode
			,RemotePrintedTime
			,CreationDate
			,MutationDate
			,Confirmed
			,ConfirmedByCode
			,RejectReason
			,CommStatusCode
			,LastQADate
			,LastQABy
		)
into
	#TLoadReports
from
	(
	select
		 ReportID = cast([report_key] as int)
		,EventExamID = cast([exam_key] as int)
		,ReportCode = cast([report_type] as varchar(2))

		,IsVerified =
			cast(
				case ltrim(rtrim(verified))
				when 'Y'
				then 1
				else 0
				end
				as bit
			)

		,Report = cast([report] as varchar(max))

		,ReportedTime = cast(
							[date_reported] + 
							case 
								when	isnumeric(left(time_reported, 2)) = 1 and isnumeric(right(time_reported, 2)) = 1 then 
										convert(datetime, isnull(left(time_reported, 2) + ':' + right(time_reported, 2), '00:00'), 114)
								else	'00:00'
							end
							as datetime)

		,ReportedByCode = cast([reported_by] as varchar(20))

		,TypedTime = cast(	
							[date_typed] + 
							case 
								when	isnumeric(left(time_typed, 2)) = 1 and isnumeric(right(time_typed, 2)) = 1 then 
										convert(datetime, isnull(left(time_typed, 2) + ':' + right(time_typed, 2), '00:00'), 114)
								else	'00:00'
							end
						as datetime)

		,TypedByCode = cast([typed_by] as varchar(20))
		,ChangedDate = cast([date_changed] as date)
		,ChangedByCode = cast([changed_by] as varchar(20))

		,IsPrinted =
			cast(
				case ltrim(rtrim(printed))
				when 'Y'
				then 1
				else 0
				end
				as bit
			)

		,Sent = cast([sent] as varchar(2))
		,ReportsRestricted = cast([reports_restricted] as varchar(2))
		,LastAddedBy = cast([last_add_by] as varchar(20))

		,LastAddendumTime = cast(	
							[last_addendum] + 
							case 
								when	isnumeric(left([last_add_time], 2)) = 1 and isnumeric(right([last_add_time], 2)) = 1 then 
										convert(datetime, isnull(left([last_add_time], 2) + ':' + right([last_add_time], 2), '00:00'), 114)
								else	'00:00'
							end
							as datetime)

		,AddendumByCode = cast([addendum_by] as varchar(20))

		,AddendumTime = cast(
							[addendum_date] + 
							case 
								when	isnumeric(left([addendum_time], 2)) = 1 and isnumeric(right([addendum_time], 2)) = 1 then 
										convert(datetime, isnull(left([addendum_time], 2) + ':' + right([addendum_time], 2), '00:00'), 114)
								else	'00:00'
							end
						as datetime)
		,NumberOfFoetus = cast([foetus_number] as int)
		,ReportedByCode2 = cast([reported_by2] as varchar(20))
		,VerifiedByCode = cast([verified_by] as varchar(20))

		,VerifiedTime = cast(
							[date_verified] + 
							case 
								when	isnumeric(left([time_verified], 2)) = 1 and isnumeric(right([time_verified], 2)) = 1 then 
										convert(datetime, isnull(left([time_verified], 2) + ':' + right([time_verified], 2), '00:00'), 114)
								else	'00:00'
							end
						as datetime)

		,VerifiedCheckedCode = cast([verify_check_by] as varchar(20))

		,VerifiedCheckTime = cast(
							[date_verify_chk] + 
							case 
								when	isnumeric(left([time_verify_chk], 2)) = 1 and isnumeric(right([time_verify_chk], 2)) = 1 then 
										convert(datetime, isnull(left([time_verify_chk], 2) + ':' + right([time_verify_chk], 2), '00:00'), 114)
								else	'00:00'
							end
							as datetime)

		,LastVerifiedCode = cast([last_verify_by] as varchar(20))

		,LastVerifiedTime = cast(
							[date_last_verif] + 
							case 
								when	isnumeric(left([time_last_verif], 2)) = 1 and isnumeric(right([time_last_verif], 2)) = 1 then 
										convert(datetime, isnull(left([time_last_verif], 2) + ':' + right([time_last_verif], 2), '00:00'), 114)
								else	'00:00'
							end
							as datetime)

		,PrintedDate = cast([date_printed] as datetime)
		,Priority = cast([priority] as int)
		,SentTime = cast([date_sent] as datetime)
		,AcknowledgedTime = cast([date_ack] as datetime)
		,StatusCode = cast([status] as varchar(6))
		,RemotePrintedTime = cast([remote_print] as datetime)
		,CreationDate = cast([creation_date] as date)
		,MutationDate = cast([mutation_date] as date)
		,Confirmed = cast([confirmed] as varchar(2))
		,ConfirmedByCode = cast([confirmed_by] as varchar(20))
		,RejectReason = cast([reject_reason] as varchar(max))
		,CommStatusCode = cast([comm_status] as varchar(10))
		,LastQADate = cast([time_last_qa] as varchar(8))
		,LastQABy = cast([last_qa_by] as varchar(20))
	FROM
		CRIS.dbo.reports
	where
		upper(ltrim(rtrim(deleted))) <> 'Y'

	) Encounter


create unique nonclustered index #IX_TLoadReports on #TLoadReports
	(
	ReportID ASC
	)
include
	(
	EncounterChecksum
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	CRIS.Report target
using
	(
	select
		 ReportID
		,EventExamID
		,ReportCode
		,IsVerified
		,Report
		,ReportedTime
		,ReportedByCode
		,TypedTime
		,TypedByCode
		,ChangedDate
		,ChangedByCode
		,IsPrinted
		,Sent
		,ReportsRestricted
		,LastAddedBy
		,LastAddendumTime
		,AddendumByCode
		,AddendumTime
		,NumberOfFoetus
		,ReportedByCode2
		,VerifiedByCode
		,VerifiedTime
		,VerifiedCheckedCode
		,VerifiedCheckTime
		,LastVerifiedCode
		,LastVerifiedTime
		,PrintedDate
		,Priority
		,SentTime
		,AcknowledgedTime
		,StatusCode
		,RemotePrintedTime
		,CreationDate
		,MutationDate
		,Confirmed
		,ConfirmedByCode
		,RejectReason
		,CommStatusCode
		,LastQADate
		,LastQABy
		,EncounterChecksum
	from
		#TLoadReports
	
	) source
	on	source.ReportID = target.ReportID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			ReportID
			,EventExamID
			,ReportCode
			,IsVerified
			,Report
			,ReportedTime
			,ReportedByCode
			,TypedTime
			,TypedByCode
			,ChangedDate
			,ChangedByCode
			,IsPrinted
			,Sent
			,ReportsRestricted
			,LastAddedBy
			,LastAddendumTime
			,AddendumByCode
			,AddendumTime
			,NumberOfFoetus
			,ReportedByCode2
			,VerifiedByCode
			,VerifiedTime
			,VerifiedCheckedCode
			,VerifiedCheckTime
			,LastVerifiedCode
			,LastVerifiedTime
			,PrintedDate
			,Priority
			,SentTime
			,AcknowledgedTime
			,StatusCode
			,RemotePrintedTime
			,CreationDate
			,MutationDate
			,Confirmed
			,ConfirmedByCode
			,RejectReason
			,CommStatusCode
			,LastQADate
			,LastQABy
			
			,Created
			,ByWhom
			)
		values
			(
			source.ReportID
			,source.EventExamID
			,source.ReportCode
			,source.IsVerified
			,source.Report
			,source.ReportedTime
			,source.ReportedByCode
			,source.TypedTime
			,source.TypedByCode
			,source.ChangedDate
			,source.ChangedByCode
			,source.IsPrinted
			,source.Sent
			,source.ReportsRestricted
			,source.LastAddedBy
			,source.LastAddendumTime
			,source.AddendumByCode
			,source.AddendumTime
			,source.NumberOfFoetus
			,source.ReportedByCode2
			,source.VerifiedByCode
			,source.VerifiedTime
			,source.VerifiedCheckedCode
			,source.VerifiedCheckTime
			,source.LastVerifiedCode
			,source.LastVerifiedTime
			,source.PrintedDate
			,source.Priority
			,source.SentTime
			,source.AcknowledgedTime
			,source.StatusCode
			,source.RemotePrintedTime
			,source.CreationDate
			,source.MutationDate
			,source.Confirmed
			,source.ConfirmedByCode
			,source.RejectReason
			,source.CommStatusCode
			,source.LastQADate
			,source.LastQABy

			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <>
		checksum(
			target.EventExamID
			,target.ReportCode
			,target.IsVerified
			,target.Report
			,target.ReportedTime
			,target.ReportedByCode
			,target.TypedTime
			,target.TypedByCode
			,target.ChangedDate
			,target.ChangedByCode
			,target.IsPrinted
			,target.Sent
			,target.ReportsRestricted
			,target.LastAddedBy
			,target.LastAddendumTime
			,target.AddendumByCode
			,target.AddendumTime
			,target.NumberOfFoetus
			,target.ReportedByCode2
			,target.VerifiedByCode
			,target.VerifiedTime
			,target.VerifiedCheckedCode
			,target.VerifiedCheckTime
			,target.LastVerifiedCode
			,target.LastVerifiedTime
			,target.PrintedDate
			,target.Priority
			,target.SentTime
			,target.AcknowledgedTime
			,target.StatusCode
			,target.RemotePrintedTime
			,target.CreationDate
			,target.MutationDate
			,target.Confirmed
			,target.ConfirmedByCode
			,target.RejectReason
			,target.CommStatusCode
			,target.LastQADate
			,target.LastQABy
		)

	then
		update
		set
			target.EventExamID = source.EventExamID
			,target.ReportCode = source.ReportCode
			,target.IsVerified = source.IsVerified
			,target.Report = source.Report
			,target.ReportedTime = source.ReportedTime
			,target.ReportedByCode = source.ReportedByCode
			,target.TypedTime = source.TypedTime
			,target.TypedByCode = source.TypedByCode
			,target.ChangedDate = source.ChangedDate
			,target.ChangedByCode = source.ChangedByCode
			,target.IsPrinted = source.IsPrinted
			,target.Sent = source.Sent
			,target.ReportsRestricted = source.ReportsRestricted
			,target.LastAddedBy = source.LastAddedBy
			,target.LastAddendumTime = source.LastAddendumTime
			,target.AddendumByCode = source.AddendumByCode
			,target.AddendumTime = source.AddendumTime
			,target.NumberOfFoetus = source.NumberOfFoetus
			,target.ReportedByCode2 = source.ReportedByCode2
			,target.VerifiedByCode = source.VerifiedByCode
			,target.VerifiedTime = source.VerifiedTime
			,target.VerifiedCheckedCode = source.VerifiedCheckedCode
			,target.VerifiedCheckTime = source.VerifiedCheckTime
			,target.LastVerifiedCode = source.LastVerifiedCode
			,target.LastVerifiedTime = source.LastVerifiedTime
			,target.PrintedDate = source.PrintedDate
			,target.Priority = source.Priority
			,target.SentTime = source.SentTime
			,target.AcknowledgedTime = source.AcknowledgedTime
			,target.StatusCode = source.StatusCode
			,target.RemotePrintedTime = source.RemotePrintedTime
			,target.CreationDate = source.CreationDate
			,target.MutationDate = source.MutationDate
			,target.Confirmed = source.Confirmed
			,target.ConfirmedByCode = source.ConfirmedByCode
			,target.RejectReason = source.RejectReason
			,target.CommStatusCode = source.CommStatusCode
			,target.LastQADate = source.LastQADate
			,target.LastQABy = source.LastQABy
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary

select
	@Elapsed = datediff(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + cast(@Elapsed as varchar) + ' minutes'

exec Audit.WriteLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime