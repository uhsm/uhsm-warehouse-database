﻿create procedure CRIS.LoadRoom as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@deleted int
	,@inserted int
	,@updated int

declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	CRIS.Room target
using
	(
	select
		 RoomCode = cast([code] as varchar(12))
		,Room = cast([rooms_name] as varchar(100))
		,SiteCode = cast([hospital] as varchar(5))
		,InactiveDate = cast(end_date as date)
		,DepartmentCode = cast(dept as varchar(16))
	from
		CRIS.dbo.rooms
	) source
	on	source.RoomCode = target.RoomCode

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 RoomCode
			,Room
			,SiteCode
			,InactiveDate
			,DepartmentCode
			,Created
			,ByWhom
			)
		values
			(
			 source.RoomCode
			,source.Room
			,source.SiteCode
			,source.InactiveDate
			,source.DepartmentCode
			,getdate()
			,suser_name()
			)

	when matched
	and
		checksum(
			 source.Room
			,source.SiteCode
			,source.InactiveDate
			,source.DepartmentCode
		)
		<>
		checksum(
			 target.Room
			,target.SiteCode
			,target.InactiveDate
			,target.DepartmentCode
		)
	then
		update
		set
			 target.Room = source.Room
			,target.SiteCode = source.SiteCode
			,target.InactiveDate = source.InactiveDate
			,target.DepartmentCode = source.DepartmentCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 $action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime
