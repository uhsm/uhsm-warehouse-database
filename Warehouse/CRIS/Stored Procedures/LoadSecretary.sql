﻿create procedure CRIS.LoadSecretary as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@deleted int
	,@inserted int
	,@updated int

declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	CRIS.Secretary target
using
	(
	select
		SecretaryCode = code
		,Secretary = cast(nullif(secrets_name, '') as nvarchar(160))
		,EndDateTime = cast(end_date as datetime2)
		,UserID = cast(nullif(user_id, '') as nvarchar(40))
	from
		CRIS.dbo.secrets
	where
		code is not null
	) source
	on	source.SecretaryCode = target.SecretaryCode

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			SecretaryCode
			,Secretary
			,EndDateTime
			,UserID
			,Created
			,ByWhom
			)
		values
			(
			source.SecretaryCode
			,source.Secretary
			,source.EndDateTime
			,source.UserID
			,getdate()
			,suser_name()
			)

	when matched
	and
		checksum(
			source.Secretary
			,source.EndDateTime
			,source.UserID
		)
		<>
		checksum(
			target.Secretary
			,target.EndDateTime
			,target.UserID
		)
	then
		update
		set
			target.Secretary = source.Secretary
			,target.EndDateTime = source.EndDateTime
			,target.UserID = source.UserID
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
output
	 $action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime
