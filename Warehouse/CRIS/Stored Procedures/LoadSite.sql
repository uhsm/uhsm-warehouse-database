﻿create procedure CRIS.LoadSite as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@deleted int
	,@inserted int
	,@updated int

declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	CRIS.Site target
using
	(
	select
		  SiteCode = cast(code as varchar(5))
		 ,Site = cast(sites_name as varchar(80))	
	from
		CRIS.dbo.sites 
	where
		code is not null
	) source
	on	source.SiteCode = target.SiteCode

	when not matched
	then
		insert
			(
			 SiteCode
			,Site
			,Created
			,ByWhom
			)
		values
			(
			 source.SiteCode
			,source.Site
			,getdate()
			,suser_name()
			)

	when matched
	and
		checksum(
			source.Site
		)
		<>
		checksum(
			target.Site
		)
	then
		update
		set
			 target.Site = source.Site
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
		
output
	 $action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime
