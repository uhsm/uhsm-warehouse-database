﻿create procedure CRIS.LoadSiteExam as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@deleted int
	,@inserted int
	,@updated int

declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	CRIS.SiteExam target
using
	(
	select
		SiteCode
		,ExamCode
		,RequiresVetting = cast(RequiresVetting as int)
		,RequiresJustify = cast(RequiresJustify as int)
		,SendDicom = cast(SendDicom as int)
		,LocalModalityGroupCode = cast(nullif(LocalModalityGroupCode, '') as nvarchar(8))
		,MaximumWait = cast(MaximumWait as int)
		,LetterCode = cast(nullif(LetterCode, '') as nvarchar(24))
		,DaysOfWeek = cast(nullif(DaysOfWeek, '') as nvarchar(28))
		,OVRoom1 = cast(nullif(OVRoom1, '') as nvarchar(24))
		,OVRoom2 = cast(nullif(OVRoom2, '') as nvarchar(24))
		,OVRoom3 = cast(nullif(OVRoom3, '') as nvarchar(24))
		,OVRoom4 = cast(nullif(OVRoom4, '') as nvarchar(24))
		,OVRoom5 = cast(nullif(OVRoom5, '') as nvarchar(24))
		,OVRoom6 = cast(nullif(OVRoom6, '') as nvarchar(24))
		,ScanReasonCode = cast(nullif(ScanReasonCode, '') as nvarchar(32))
		,LMP = cast(nullif(LMP, '') as nvarchar(32))
		,ArsacLimit = cast(ArsacLimit as numeric)
		,ResGroup1 = cast(nullif(ResGroup1, '') as nvarchar(64))
		,ResGroup2 = cast(nullif(ResGroup2, '') as nvarchar(64))
		,ResGroup3 = cast(nullif(ResGroup3, '') as nvarchar(64))
		,ResGroup4 = cast(nullif(ResGroup4, '') as nvarchar(64))
		,ResGroup5 = cast(nullif(ResGroup5, '') as nvarchar(64))
		,ResGroup6 = cast(nullif(ResGroup6, '') as nvarchar(64))
		,LMPCode = cast(nullif(LMPCode, '') as nvarchar(8))
		,QaPercent = cast(QaPercent as int)
		,DBLReportPercent = cast(DBLReportPercent as int)
		,OvDaysOfWeek = cast(nullif(OvDaysOfWeek, '') as nvarchar(28))
		,ProcedureTime = cast(nullif(ProcedureTime, '') as nvarchar(16))
		,NoVisits = cast(NoVisits as int)
		,VisitFrequency = cast(nullif(VisitFrequency, '') as nvarchar(16))
		,VisitMargin = cast(nullif(VisitMargin, '') as nvarchar(16))
		,PreparationTime = cast(nullif(PreparationTime, '') as nvarchar(16))
		,Room1 = cast(nullif(Room1, '') as nvarchar(24))
		,Room2 = cast(nullif(Room2, '') as nvarchar(24))
		,Room3 = cast(nullif(Room3, '') as nvarchar(24))
		,Room4 = cast(nullif(Room4, '') as nvarchar(24))
		,Room5 = cast(nullif(Room5, '') as nvarchar(24))
		,Room6 = cast(nullif(Room6, '') as nvarchar(24))
		,UniqueID = cast(nullif(UniqueID, '') as nvarchar(80))
	from
		(
		select
			SiteCode = hospital
			,ExamCode = examination
			,RequiresVetting = case when requires_vetting = 'Y' then 1 else 0 end
			,RequiresJustify = case when requires_justify = 'Y' then 1 else 0 end
			,SendDicom = case when send_dicom = 'Y' then 1 else 0 end
			,LocalModalityGroupCode = examcdh_group
			,MaximumWait = max_wait
			,LetterCode = letter
			,DaysOfWeek = days_of_week
			,OVRoom1 = ov_room_1
			,OVRoom2 = ov_room_2
			,OVRoom3 = ov_room_3
			,OVRoom4 = ov_room_4
			,OVRoom5 = ov_room_5
			,OVRoom6 = ov_room_6
			,ScanReasonCode = scan_reason
			,LMP = lmp_text
			,ArsacLimit = arsac_limit
			,ResGroup1 = res_group_1
			,ResGroup2 = res_group_2
			,ResGroup3 = res_group_3
			,ResGroup4 = res_group_4
			,ResGroup5 = res_group_5
			,ResGroup6 = res_group_6
			,LMPCode = lmp
			,QaPercent = qa_percent
			,DBLReportPercent = dblreport_percent
			,OvDaysOfWeek = ov_days_of_week
			,ProcedureTime = procedure_time
			,NoVisits = no_visits
			,VisitFrequency = visit_frequency
			,VisitMargin = visit_margin
			,PreparationTime = prep_time
			,Room1 = room_1
			,Room2 = room_2
			,Room3 = room_3
			,Room4 = room_4
			,Room5 = room_5
			,Room6 = room_6
			,UniqueID = uniqueid
		from
			CRIS.dbo.examcdh
		) source
	) source
	on	source.SiteCode = target.SiteCode
	and	source.ExamCode = target.ExamCode

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			SiteCode
			,ExamCode
			,RequiresVetting
			,RequiresJustify
			,SendDicom
			,LocalModalityGroupCode
			,MaximumWait
			,LetterCode
			,DaysOfWeek
			,OVRoom1
			,OVRoom2
			,OVRoom3
			,OVRoom4
			,OVRoom5
			,OVRoom6
			,ScanReasonCode
			,LMP
			,ArsacLimit
			,ResGroup1
			,ResGroup2
			,ResGroup3
			,ResGroup4
			,ResGroup5
			,ResGroup6
			,LMPCode
			,QaPercent
			,DBLReportPercent
			,OvDaysOfWeek
			,ProcedureTime
			,NoVisits
			,VisitFrequency
			,VisitMargin
			,PreparationTime
			,Room1
			,Room2
			,Room3
			,Room4
			,Room5
			,Room6
			,UniqueID
			,Created
			,ByWhom
			)
		values
			(
			source.SiteCode
			,source.ExamCode
			,source.RequiresVetting
			,source.RequiresJustify
			,source.SendDicom
			,source.LocalModalityGroupCode
			,source.MaximumWait
			,source.LetterCode
			,source.DaysOfWeek
			,source.OVRoom1
			,source.OVRoom2
			,source.OVRoom3
			,source.OVRoom4
			,source.OVRoom5
			,source.OVRoom6
			,source.ScanReasonCode
			,source.LMP
			,source.ArsacLimit
			,source.ResGroup1
			,source.ResGroup2
			,source.ResGroup3
			,source.ResGroup4
			,source.ResGroup5
			,source.ResGroup6
			,source.LMPCode
			,source.QaPercent
			,source.DBLReportPercent
			,source.OvDaysOfWeek
			,source.ProcedureTime
			,source.NoVisits
			,source.VisitFrequency
			,source.VisitMargin
			,source.PreparationTime
			,source.Room1
			,source.Room2
			,source.Room3
			,source.Room4
			,source.Room5
			,source.Room6
			,source.UniqueID
			,getdate()
			,suser_name()
			)

	when matched
	and
		checksum(
			source.RequiresVetting
			,source.RequiresJustify
			,source.SendDicom
			,source.LocalModalityGroupCode
			,source.MaximumWait
			,source.LetterCode
			,source.DaysOfWeek
			,source.OVRoom1
			,source.OVRoom2
			,source.OVRoom3
			,source.OVRoom4
			,source.OVRoom5
			,source.OVRoom6
			,source.ScanReasonCode
			,source.LMP
			,source.ArsacLimit
			,source.ResGroup1
			,source.ResGroup2
			,source.ResGroup3
			,source.ResGroup4
			,source.ResGroup5
			,source.ResGroup6
			,source.LMPCode
			,source.QaPercent
			,source.DBLReportPercent
			,source.OvDaysOfWeek
			,source.ProcedureTime
			,source.NoVisits
			,source.VisitFrequency
			,source.VisitMargin
			,source.PreparationTime
			,source.Room1
			,source.Room2
			,source.Room3
			,source.Room4
			,source.Room5
			,source.Room6
			,source.UniqueID
		)
		<>
		checksum(
			target.RequiresVetting
			,target.RequiresJustify
			,target.SendDicom
			,target.LocalModalityGroupCode
			,target.MaximumWait
			,target.LetterCode
			,target.DaysOfWeek
			,target.OVRoom1
			,target.OVRoom2
			,target.OVRoom3
			,target.OVRoom4
			,target.OVRoom5
			,target.OVRoom6
			,target.ScanReasonCode
			,target.LMP
			,target.ArsacLimit
			,target.ResGroup1
			,target.ResGroup2
			,target.ResGroup3
			,target.ResGroup4
			,target.ResGroup5
			,target.ResGroup6
			,target.LMPCode
			,target.QaPercent
			,target.DBLReportPercent
			,target.OvDaysOfWeek
			,target.ProcedureTime
			,target.NoVisits
			,target.VisitFrequency
			,target.VisitMargin
			,target.PreparationTime
			,target.Room1
			,target.Room2
			,target.Room3
			,target.Room4
			,target.Room5
			,target.Room6
			,target.UniqueID
		)
	then
		update
		set
			target.RequiresVetting = source.RequiresVetting
			,target.RequiresJustify = source.RequiresJustify
			,target.SendDicom = source.SendDicom
			,target.LocalModalityGroupCode = source.LocalModalityGroupCode
			,target.MaximumWait = source.MaximumWait
			,target.LetterCode = source.LetterCode
			,target.DaysOfWeek = source.DaysOfWeek
			,target.OVRoom1 = source.OVRoom1
			,target.OVRoom2 = source.OVRoom2
			,target.OVRoom3 = source.OVRoom3
			,target.OVRoom4 = source.OVRoom4
			,target.OVRoom5 = source.OVRoom5
			,target.OVRoom6 = source.OVRoom6
			,target.ScanReasonCode = source.ScanReasonCode
			,target.LMP = source.LMP
			,target.ArsacLimit = source.ArsacLimit
			,target.ResGroup1 = source.ResGroup1
			,target.ResGroup2 = source.ResGroup2
			,target.ResGroup3 = source.ResGroup3
			,target.ResGroup4 = source.ResGroup4
			,target.ResGroup5 = source.ResGroup5
			,target.ResGroup6 = source.ResGroup6
			,target.LMPCode = source.LMPCode
			,target.QaPercent = source.QaPercent
			,target.DBLReportPercent = source.DBLReportPercent
			,target.OvDaysOfWeek = source.OvDaysOfWeek
			,target.ProcedureTime = source.ProcedureTime
			,target.NoVisits = source.NoVisits
			,target.VisitFrequency = source.VisitFrequency
			,target.VisitMargin = source.VisitMargin
			,target.PreparationTime = source.PreparationTime
			,target.Room1 = source.Room1
			,target.Room2 = source.Room2
			,target.Room3 = source.Room3
			,target.Room4 = source.Room4
			,target.Room5 = source.Room5
			,target.Room6 = source.Room6
			,target.UniqueID = source.UniqueID
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
output
	 $action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime
