﻿create procedure CRIS.LoadSpecialty as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@deleted int
	,@inserted int
	,@updated int

declare @ProcessList table
	(
	Action nvarchar(10)
	)
merge
	CRIS.Specialty target
using
	(
	select
		SpecialtyCode = cast(code as varchar(16)) ,
		Specialty = cast(special_name as varchar(90)) ,
		SpecialtyGroupCode = cast(special_group as varchar(6)) ,
		InactiveDate = cast(end_date as date)
	from
		CRIS.dbo.special
	where
		code is not null
	) source
	on	source.SpecialtyCode = target.SpecialtyCode

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 SpecialtyCode 
			,Specialty 
			,SpecialtyGroupCode
			,InactiveDate
			,Created
			,ByWhom
			)
		values
			(
			 source.SpecialtyCode 
			,source.Specialty
			,source.SpecialtyGroupCode 
			,source.InactiveDate 
			,getdate()
			,suser_name()
			)

	when matched
	and
		checksum(
			 source.Specialty
			,source.SpecialtyGroupCode 
			,source.InactiveDate 
		)
		<>
		checksum(
			 target.Specialty
			,target.SpecialtyGroupCode 
			,target.InactiveDate 
		)
	then
		update
		set
			 target.Specialty = source.Specialty 
			,target.SpecialtyGroupCode = source.SpecialtyGroupCode
			,target.InactiveDate =source.InactiveDate 
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 $action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime
