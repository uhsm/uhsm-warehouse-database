﻿create procedure CRIS.LoadWard as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@deleted int
	,@inserted int
	,@updated int

declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	CRIS.Ward target
using
	(
	select
		 WardCode = cast([code] as varchar(30))
		,Ward = cast([wards_name] as varchar(80))
		,SiteCode = cast([hospital] as varchar(5))
		,InactiveDate = cast([end_date] as date)
	from
		[CRIS].[dbo].[wards]
	where
		[code] is not null
	) source
	on	source.WardCode = target.WardCode

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 WardCode
			,Ward
			,SiteCode
			,InactiveDate
			,Created
			,ByWhom
			)
		values
			(
			 source.WardCode
			,source.Ward
			,source.SiteCode
			,source.InactiveDate
			,getdate()
			,suser_name()
			)

	when matched
	and
		checksum(
			 source.Ward
			,source.SiteCode
			,source.InactiveDate
		)
		<>
		checksum(
			 target.Ward
			,target.SiteCode
			,target.InactiveDate
		)
	then
		update
		set
			 target.Ward = source.Ward
			,target.SiteCode = source.SiteCode
			,target.InactiveDate = source.InactiveDate
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 $action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime
