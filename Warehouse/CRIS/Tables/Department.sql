﻿CREATE TABLE [CRIS].[Department](
	[DepartmentCode] varchar(16) NOT NULL,
	[Department] varchar(80) NOT NULL,
	SiteCode varchar(5) null,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,

 CONSTRAINT [PK_Department] PRIMARY KEY CLUSTERED 
(
	DepartmentCode ASC
)
)
