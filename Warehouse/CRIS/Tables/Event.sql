﻿CREATE TABLE [CRIS].[Event] (
    [EventID]                 INT           NOT NULL,
    [PatientID]               INT           NULL,
    [ReferrerCode]            VARCHAR (10)  NULL,
    [ReferralSourceCode]      VARCHAR (18)  NULL,
    [ReferrerSpecialtyID]     INT           NULL,
    [HospitalCode]            VARCHAR (20)  NULL,
    [WardCode]                VARCHAR (30)  NULL,
    [RequestCategoryCode]     VARCHAR (1)   NULL,
    [RequestDate]             DATE          NULL,
    [BookingTypeCode]         VARCHAR (1)   NULL,
    [BookedDate]              DATE          NULL,
    [IntendedRadiologistCode] VARCHAR (20)  NULL,
    [UrgencyID]             TINYINT       NULL,
    [PatientTypeCode]         VARCHAR (1)   NULL,
    [EventTime]               DATETIME      NULL,
    [RequiredClinician]       VARCHAR (255) NULL,
    [Created]                 DATETIME      NOT NULL,
    [Updated]                 DATETIME      NULL,
    [ByWhom]                  VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_CRIS_Event] PRIMARY KEY CLUSTERED ([EventID] ASC)
);

GO

