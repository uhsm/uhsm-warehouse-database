﻿CREATE TABLE [CRIS].[EventExam](
	
	[EventExamID] [int] NOT NULL,
	[EventID] [int] NOT NULL,
	[ExamTime] datetime,
	ExamCode varchar(16) NULL,
	RoomCode varchar(12) NULL,
	RadiologistCode varchar(20) NULL,
	RadiographerCode1 varchar(20) NULL,
	RadiographerCode2 varchar(20) NULL,
	RadiographerCode3 varchar(20) NULL,
	BookedTime datetime,
	AccessionCode varchar(40) NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,
 CONSTRAINT [PK_CRIS_EventExam] PRIMARY KEY CLUSTERED 
(
	[EventExamID] ASC
)
)
GO
