﻿CREATE TABLE CRIS.Exam(
	
	ExamCode varchar(16) NOT NULL,
	Exam varchar(80) NULL,
	ModalityCode varchar(2) null,
	InterventionalCode varchar(2) null,
	NumberOfExams tinyint null,
	InactiveDate date,
	[GroupOneCode] varchar(4) null,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,

 CONSTRAINT [PK_CRIS_Exam] PRIMARY KEY CLUSTERED 
(
	ExamCode ASC
)
)
