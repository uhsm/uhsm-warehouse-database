﻿CREATE TABLE CRIS.ExamMap (
	
	ExamCode varchar(16) NOT NULL,
	NewExamCode varchar(16) NOT NULL,

	[Created] [datetime] NOT NULL default getdate(),
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL default suser_name(),

 CONSTRAINT [PK_CRIS_ExamMap] PRIMARY KEY CLUSTERED 
(
	ExamCode ASC
)
)
