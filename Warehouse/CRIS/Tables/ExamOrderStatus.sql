﻿CREATE TABLE [CRIS].[ExamOrderStatus](
	ExamOrderStatusCode int NOT NULL,
	ExamOrderStatus varchar(80) NOT NULL,

	[Created] [datetime] NOT NULL default getdate(),
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL default suser_name(),

 CONSTRAINT [PK_CRIS_ExamOrderStatus] PRIMARY KEY CLUSTERED 
(
	ExamOrderStatusCode ASC
)
)
