﻿CREATE TABLE [CRIS].ExamStatus(
	ExamStatusID int NOT NULL,
	PatientID int NULL,
	EventID  int NULL,
	[EventExamID] int  NULL,
	[StatusCategoryCode] varchar(4) NOT NULL,
	[StatusCurrentCode] varchar (2) NULL,
	StatusCode varchar(6)  NULL,
	[StatusTypeCode] varchar(6) null,
	[StatusTime] datetime null,
	UserID varchar(20) NULL,
	StartTime  datetime  NULL,
	EndTime    datetime NULL,
	[ExamCode] varchar(16) NUll,
	[SiteCode] varchar(5) NULL,
	[Remark] varchar(24) NULL,
	[Comment]  varchar(max) NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,

 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	ExamStatusID ASC
)
)
GO

