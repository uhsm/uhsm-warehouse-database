﻿CREATE TABLE [CRIS].[LocalModalityGroup](
	LocalModalityGroupID nvarchar(4) NOT NULL,
	LocalModalityGroup varchar(80) NOT NULL,

	[Created] [datetime] NOT NULL default getdate(),
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL default suser_name(),

 CONSTRAINT [PK_CRIS_LocalModalityGroup] PRIMARY KEY CLUSTERED 
(
	LocalModalityGroupID ASC
)
)
