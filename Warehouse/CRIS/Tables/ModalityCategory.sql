﻿CREATE TABLE [CRIS].[ModalityCategory](
	ModalityCategoryID int NOT NULL,
	ModalityCategory varchar(80) NOT NULL,

	[Created] [datetime] NOT NULL default getdate(),
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL default suser_name(),

 CONSTRAINT [PK_CRIS_ModalityCategory] PRIMARY KEY CLUSTERED 
(
	ModalityCategoryID ASC
)
)
