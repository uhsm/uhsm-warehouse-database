﻿CREATE TABLE [CRIS].[ModalityModalityCategoryMap](
	ModalityCode varchar(2) NOT NULL,
	ModalityCategoryID int NOT NULL,

	[Created] [datetime] NOT NULL default getdate(),
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL default suser_name(),

 CONSTRAINT [PK_CRIS_ModalityModalityCategoryMap] PRIMARY KEY CLUSTERED 
(
	ModalityCode
	,ModalityCategoryID
), 
    CONSTRAINT [FK_ModalityModalityCategoryMap_ModalityCategory] FOREIGN KEY ([ModalityCategoryID]) REFERENCES [CRIS].[ModalityCategory] ([ModalityCategoryID])
)
