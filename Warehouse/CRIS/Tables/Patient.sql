﻿CREATE TABLE [CRIS].[Patient](
	PatientID  int  NOT NULL
	,PatientSurname  varchar(50)   NULL 
	,PatientForename  varchar(50) NULL
	,NHSNumber   varchar(10) NULL
	,PatientTitle  varchar(12) NULL
	,SexCode  varchar (1) NULL
	,PatientAddressLine1   varchar(80) NULL
	,PatientAddressLine2  varchar(80) NULL
	,PatientAddressLine3  varchar(50) NULL
	,PatientAddressLine4  varchar(50) NULL
	,PatientPostcode  varchar(8) NULL
	,RegisteredGpCode varchar(10) NULL
	,EthnicCategoryCode varchar(6) NULL
	,DateOfDeath date NULL
	,DateOfBirth date NULL
	,HomeTelephone  varchar(30) NULL
	,WorkTelephone  varchar(30) NULL
	,MobileTelephone  varchar(30) NULL
	,IsOKToCallMobile  bit
	,Created datetime NOT NULL
	,Updated datetime NULL
	,ByWhom varchar(50) NOT NULL
	
 CONSTRAINT [PK_Patient] PRIMARY KEY CLUSTERED 
(
	PatientID ASC
)
)