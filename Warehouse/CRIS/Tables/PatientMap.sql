﻿CREATE TABLE [CRIS].PatientMap(	
	DistrictNumber  varchar(30)  NOT NULL,
	PatientID  int   NULL,
	NHSNumber varchar(10) NULL,
	Created datetime NOT NULL,
	Updated datetime NULL,
	ByWhom varchar(50) NOT NULL,
	CONSTRAINT [PK_CRIS_PatientMap] PRIMARY KEY CLUSTERED ([DistrictNumber])
)
