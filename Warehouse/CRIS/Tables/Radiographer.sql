﻿CREATE TABLE [CRIS].[Radiographer]
(
	RadiographerCode varchar(20) NOT NULL,
	[Radiographer] varchar(80) NULL,
	EndDate date NULL,
	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,
	 CONSTRAINT [PK_Radiographer] PRIMARY KEY CLUSTERED ([RadiographerCode])
)
