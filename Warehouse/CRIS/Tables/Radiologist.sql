﻿CREATE TABLE [CRIS].[Radiologist]
(
	RadiologistCode varchar(20) NOT NULL,
	Radiologist varchar(80) NULL,
	RadiologistType varchar(12) NULL,
	Unverified int  NULL,
	Suspended  int  NULL,
	Unprinted  int  NULL,
	EndDate DATE NULL,
	Created datetime NOT NULL,
	Updated datetime NULL,
	ByWhom varchar(50) NOT NULL,
	CONSTRAINT [PK_Radiologist] PRIMARY KEY CLUSTERED (RadiologistCode ASC)

)
