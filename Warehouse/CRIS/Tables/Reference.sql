﻿CREATE TABLE [CRIS].[Reference](
	[DomainCode] [varchar](16) NOT NULL,
	[SourceCode] [varchar](2) NOT NULL,
	[SourceValue] [varchar](30) NULL,
	[Domain] [varchar](80) NULL,
	[CreatedDateTime] [datetime] NULL,
	[UpdatedDateTime] [datetime] NULL, 

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,

    CONSTRAINT [PK_Reference] PRIMARY KEY ([DomainCode], [SourceCode])
)

GO


