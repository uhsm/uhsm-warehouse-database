﻿CREATE TABLE [CRIS].[ReferenceMap](
	[DomainCode] [varchar](16) NOT NULL,
	[ReferenceMapID] int NOT NULL,
	[SourceCode] [varchar](48) NULL,
	[MapCode1] [varchar](48) NULL,
	[MapCode2] [varchar](48) NULL,
	[MapCode3] [varchar](48) NULL,
	[MapCode4] [varchar](48) NULL,
	[MapCode5] [varchar](48) NULL,
	[MapCode6] [varchar](48) NULL,
	[MapCode7] [varchar](48) NULL,
	[MapCode8] [varchar](48) NULL,
	[MapCode9] [varchar](48) NULL,
	[MapCode10] [varchar](48) NULL,
	[SourceValue] [varchar](255) NULL,
	[CreatedDateTime] DATETIME2 NULL,
	[UpdatedDateTime] DATETIME2 NULL, 

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,

    CONSTRAINT [PK_ReferenceMap] PRIMARY KEY ([DomainCode], [ReferenceMapID])
)

GO


