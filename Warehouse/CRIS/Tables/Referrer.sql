﻿CREATE TABLE CRIS.Referrer(
	[ReferrerCode] varchar(10) NOT NULL,
	StartDate  DATE NULL,
	[Referrer] varchar(158) NULL,
	[ReferrerTypeCode]  varchar(1) NULL,
	EndDate DATE NULL,
	[ReferrerSpecialityCode] VARCHAR(60) NULL,
	[ReferrerGroupCode]  varchar(3) NULL,
	[ReferrerNationalCode]  varchar(30) NULL,
	SendElectronicReport varchar(1) NULL,

	--start of system columns
	
	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,
	--end of system columns
	
 CONSTRAINT [PK_Referer] PRIMARY KEY CLUSTERED 
(
	[ReferrerCode] ASC
)
)
