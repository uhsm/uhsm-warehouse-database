﻿CREATE TABLE [CRIS].ReferrerOrganisation(
	ReferrerOrganisationCode varchar(10) NOT NULL,
	ReferrerOrganisation varchar(158) NULL,
	GroupCode varchar(3) NULL,
	NationalCode varchar(30) NULL,
	TypeCode VARCHAR(2) NULL,
	AddressLine1 varchar(80) NULL,
	AddressLine2 varchar(80) NULL,
	AddressLine3 varchar(50) NULL,
	AddressLine4 varchar(50) NULL,
	AddressLine5 varchar(50) NULL,
	Postcode varchar(8) NULL,
	SendElectronicReport  VARCHAR(2) NULL,
	InactiveDate date NULL,
	AreaCode varchar(5) NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,

 CONSTRAINT [PK_ReferrerSource] PRIMARY KEY CLUSTERED 
(
	ReferrerOrganisationCode ASC
)
)
