﻿CREATE TABLE [CRIS].Room(
	RoomCode [varchar](12) NOT NULL,
	Room [varchar](100) NOT NULL,
	SiteCode [varchar](5) NULL,
	InactiveDate date,
	DepartmentCode varchar(16) NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,

 CONSTRAINT [PK_Room] PRIMARY KEY CLUSTERED 
(
	RoomCode ASC
)
)




