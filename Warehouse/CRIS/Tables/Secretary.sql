﻿CREATE TABLE CRIS.Secretary(
	
	[SecretaryCode] [nvarchar](20) NOT NULL,
	[Secretary] [nvarchar](80) NULL,
	[EndDateTime] [datetime2](7) NULL,
	[UserID] [nvarchar](20) NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,

 CONSTRAINT [PK_CRIS_Secretary] PRIMARY KEY CLUSTERED 
(
	SecretaryCode ASC
)
)
