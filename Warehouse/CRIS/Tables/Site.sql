﻿CREATE TABLE CRIS.Site
	(
	SiteCode varchar(5) NOT NULL,
	Site varchar(80) NOT NULL,
	Created datetime NOT NULL,
	Updated datetime NULL,
	ByWhom varchar(50) NOT NULL,
	
 CONSTRAINT [PK_CRIS_SitesCode] PRIMARY KEY CLUSTERED 
(
	SiteCode ASC
)
)
