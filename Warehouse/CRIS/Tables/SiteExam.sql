﻿CREATE TABLE CRIS.SiteExam(
	
	[SiteCode] [nvarchar](24) NOT NULL,
	[ExamCode] [nvarchar](16) NOT NULL,
	[RequiresVetting] [int] NOT NULL,
	[RequiresJustify] [int] NOT NULL,
	[SendDicom] [int] NOT NULL,
	[LocalModalityGroupCode] [nvarchar](4) NULL,
	[MaximumWait] [int] NULL,
	[LetterCode] [nvarchar](12) NULL,
	[DaysOfWeek] [nvarchar](14) NULL,
	[OVRoom1] [nvarchar](12) NULL,
	[OVRoom2] [nvarchar](12) NULL,
	[OVRoom3] [nvarchar](12) NULL,
	[OVRoom4] [nvarchar](12) NULL,
	[OVRoom5] [nvarchar](12) NULL,
	[OVRoom6] [nvarchar](12) NULL,
	[ScanReasonCode] [nvarchar](16) NULL,
	[LMP] [nvarchar](16) NULL,
	[ArsacLimit] [numeric](7, 2) NULL,
	[ResGroup1] [nvarchar](32) NULL,
	[ResGroup2] [nvarchar](32) NULL,
	[ResGroup3] [nvarchar](32) NULL,
	[ResGroup4] [nvarchar](32) NULL,
	[ResGroup5] [nvarchar](32) NULL,
	[ResGroup6] [nvarchar](32) NULL,
	[LMPCode] [nvarchar](4) NULL,
	[QaPercent] [int] NULL,
	[DBLReportPercent] [int] NULL,
	[OvDaysOfWeek] [nvarchar](14) NULL,
	[ProcedureTime] [nvarchar](8) NULL,
	[NoVisits] [int] NULL,
	[VisitFrequency] [nvarchar](8) NULL,
	[VisitMargin] [nvarchar](8) NULL,
	[PreparationTime] [nvarchar](8) NULL,
	[Room1] [nvarchar](12) NULL,
	[Room2] [nvarchar](12) NULL,
	[Room3] [nvarchar](12) NULL,
	[Room4] [nvarchar](12) NULL,
	[Room5] [nvarchar](12) NULL,
	[Room6] [nvarchar](12) NULL,
	[UniqueID] [nvarchar](40) NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,

 CONSTRAINT [PK_CRIS_SiteExam] PRIMARY KEY CLUSTERED 
(
	SiteCode
	,ExamCode ASC
)
)
