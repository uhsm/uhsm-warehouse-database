﻿CREATE TABLE [CRIS].Specialty(
	SpecialtyCode varchar(16) NOT NULL,
	[Specialty] varchar(90)  NULL, 
	[SpecialtyGroupCode]   varchar(6) NULL,
	[InactiveDate] DATE NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,

 CONSTRAINT [PK_Specialty] PRIMARY KEY CLUSTERED 
(
	SpecialtyCode ASC
)
)




