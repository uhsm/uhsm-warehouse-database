﻿CREATE TABLE [CRIS].[TestPatient]
(
	[PatientID] INT NOT NULL,
	[Created] [datetime] NOT NULL Default GetDate(),
	CONSTRAINT [PK_CRIS_TestPatient] PRIMARY KEY ([PatientID])
)
