﻿CREATE TABLE [CRIS].Ward(
	[WardCode] [varchar](30) NOT NULL,
	[Ward] [varchar](80) NULL,
	SiteCode [varchar](5) NULL,
	InactiveDate date NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,

 CONSTRAINT [PK_Ward] PRIMARY KEY CLUSTERED 
(
	[WardCode] ASC
)
)




