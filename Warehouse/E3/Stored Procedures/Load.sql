﻿CREATE PROCEDURE [E3].[Load] as

exec E3.LoadAddress
exec E3.LoadBaby
exec E3.LoadBirth
exec E3.LoadCurrentPregnancy
exec E3.LoadDeliveryProcedure
exec E3.LoadEthnicOrigin
exec E3.LoadGender
exec E3.LoadGPPractice
exec E3.LoadLabourAndDelivery
exec E3.LoadNeonatalExamination
exec E3.LoadPatient
exec E3.LoadPatientAddress
exec E3.LoadPerson
exec E3.LoadPostnatalTransferMother
exec E3.LoadPracticeAddress
exec E3.LoadPreDelivery
exec E3.LoadPregnancy
exec E3.LoadThirdStage
