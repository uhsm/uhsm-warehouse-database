﻿CREATE procedure [E3].[LoadAddress] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			Line1
			,Line2
			,Line3
			,Line4
			,Postcode
			,HomeTelephone
			,MobileTelephone
			,WorkTelephone
			,EMail
			,Fax
			,AreaOfResidence
			,EKRecord
			,GUID
			,UserID
			,LastUpdated
			,OtherTelephone
			)
into
	#TLoadE3Address
from
	(
	select
		AddressID
		,Line1 = cast(nullif(Line1, '') as nvarchar(100))
		,Line2 = cast(nullif(Line2, '') as nvarchar(100))
		,Line3 = cast(nullif(Line3, '') as nvarchar(100))
		,Line4 = cast(nullif(Line4, '') as nvarchar(100))
		,Postcode = cast(nullif(Postcode, '') as nchar(20))
		,HomeTelephone = cast(nullif(HomeTelephone, '') as nvarchar(100))
		,MobileTelephone = cast(nullif(MobileTelephone, '') as nvarchar(100))
		,WorkTelephone = cast(nullif(WorkTelephone, '') as nvarchar(100))
		,EMail = cast(nullif(EMail, '') as nvarchar(200))
		,Fax = cast(nullif(Fax, '') as nvarchar(100))
		,AreaOfResidence = cast(nullif(AreaOfResidence, '') as varchar(50))
		,EKRecord = cast(EKRecord as bit)
		,GUID = cast(GUID as uniqueidentifier)
		,UserID = cast(UserID as int)
		,LastUpdated = cast(LastUpdated as datetime)
		,OtherTelephone = cast(nullif(OtherTelephone, '') as nvarchar(100))
	from
		(
		select
			AddressID = Address.AddressID
			,Line1 = Address.Line1
			,Line2 = Address.Line2
			,Line3 = Address.Line3
			,Line4 = Address.Line4
			,Postcode = Address.PostCode
			,HomeTelephone = Address.HomeTelephone
			,MobileTelephone = Address.MobileTelephone
			,WorkTelephone = Address.WorkTelephone
			,EMail = Address.EMail
			,Fax = Address.Fax
			,AreaOfResidence = Address.AreaOfResidence
			,EKRecord = Address.EK_Record
			,GUID = Address.GUID
			,UserID = Address.UserID
			,LastUpdated = Address.LastUpdated
			,OtherTelephone = Address.OtherTelephone
		from
			E3.dbo.Address Address
		where
			Address.Deleted <> 1

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3Address on #TLoadE3Address
	(
	AddressID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	E3.Address target
using
	(
	select
		*
	from
		#TLoadE3Address
	
	) source
	on	source.AddressID = target.AddressID

when not matched by source
then
	delete

when not matched
then
	insert
		(
		AddressID
		,Line1
		,Line2
		,Line3
		,Line4
		,Postcode
		,HomeTelephone
		,MobileTelephone
		,WorkTelephone
		,EMail
		,Fax
		,AreaOfResidence
		,EKRecord
		,GUID
		,UserID
		,LastUpdated
		,OtherTelephone

		,Created
		,ByWhom
		)
	values
		(
		source.AddressID
		,source.Line1
		,source.Line2
		,source.Line3
		,source.Line4
		,source.Postcode
		,source.HomeTelephone
		,source.MobileTelephone
		,source.WorkTelephone
		,source.EMail
		,source.Fax
		,source.AreaOfResidence
		,source.EKRecord
		,source.GUID
		,source.UserID
		,source.LastUpdated
		,source.OtherTelephone

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.Line1
		,target.Line2
		,target.Line3
		,target.Line4
		,target.Postcode
		,target.HomeTelephone
		,target.MobileTelephone
		,target.WorkTelephone
		,target.EMail
		,target.Fax
		,target.AreaOfResidence
		,target.EKRecord
		,target.GUID
		,target.UserID
		,target.LastUpdated
		,target.OtherTelephone
		)

then
	update
	set
		target.Line1 = source.Line1
		,target.Line2 = source.Line2
		,target.Line3 = source.Line3
		,target.Line4 = source.Line4
		,target.Postcode = source.Postcode
		,target.HomeTelephone = source.HomeTelephone
		,target.MobileTelephone = source.MobileTelephone
		,target.WorkTelephone = source.WorkTelephone
		,target.EMail = source.EMail
		,target.Fax = source.Fax
		,target.AreaOfResidence = source.AreaOfResidence
		,target.EKRecord = source.EKRecord
		,target.GUID = source.GUID
		,target.UserID = source.UserID
		,target.LastUpdated = source.LastUpdated
		,target.OtherTelephone = source.OtherTelephone

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime