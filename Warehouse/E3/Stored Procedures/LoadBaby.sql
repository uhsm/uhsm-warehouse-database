﻿CREATE procedure [E3].[LoadBaby] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			PatientID
			,PregnancyID
			,CarePlanID
			,EKRecord
			,GUID
			,UserID
			,IsRequestedNN4B
			,IsRegisteredInPAS
			,BabyNotesMemoID
			,DateOfBirth
			,GestationBirth
			)
into
	#TLoadE3Baby
from
	(
	select
		BabyID
		,PatientID = cast(PatientID as bigint)
		,PregnancyID = cast(PregnancyID as bigint)
		,CarePlanID = cast(CarePlanID as int)
		,EKRecord = cast(EKRecord as bit)
		,GUID = cast(GUID as uniqueidentifier)
		,UserID = cast(UserID as int)
		,IsRequestedNN4B = cast(IsRequestedNN4B as bit)
		,IsRegisteredInPAS = cast(IsRegisteredInPAS as bit)
		,BabyNotesMemoID = cast(BabyNotesMemoID as bigint)
		,DateOfBirth = cast(DateOfBirth as datetime)
		,GestationBirth = cast(nullif(GestationBirth, '') as nvarchar(60))
	from
		(
		select
			BabyID = Baby.BabyID
			,PatientID = Baby.PatientID
			,PregnancyID = Baby.PregnancyID
			,CarePlanID = Baby.CarePlan
			,EKRecord = Baby.EK_Record
			,GUID = Baby.GUID
			,UserID = Baby.UserID
			,IsRequestedNN4B = Baby.RequestedNN4B
			,IsRegisteredInPAS = Baby.RegisteredInPAS
			,BabyNotesMemoID = Baby.BabyNotesMemoID
			,DateOfBirth = Baby.Birthday
			,GestationBirth = Baby.GestationBirth
		from
			E3.dbo.Baby Baby
		where
			Baby.Deleted <> 1

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3Baby on #TLoadE3Baby
	(
	BabyID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	E3.Baby target
using
	(
	select
		*
	from
		#TLoadE3Baby
	
	) source
	on	source.BabyID = target.BabyID

when not matched by source
then
	delete

when not matched
then
	insert
		(
		BabyID
		,PatientID
		,PregnancyID
		,CarePlanID
		,EKRecord
		,GUID
		,UserID
		,IsRequestedNN4B
		,IsRegisteredInPAS
		,BabyNotesMemoID
		,DateOfBirth
		,GestationBirth

		,Created
		,ByWhom
		)
	values
		(
		source.BabyID
		,source.PatientID
		,source.PregnancyID
		,source.CarePlanID
		,source.EKRecord
		,source.GUID
		,source.UserID
		,source.IsRequestedNN4B
		,source.IsRegisteredInPAS
		,source.BabyNotesMemoID
		,source.DateOfBirth
		,source.GestationBirth

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.PatientID
		,target.PregnancyID
		,target.CarePlanID
		,target.EKRecord
		,target.GUID
		,target.UserID
		,target.IsRequestedNN4B
		,target.IsRegisteredInPAS
		,target.BabyNotesMemoID
		,target.DateOfBirth
		,target.GestationBirth
		)

then
	update
	set
		target.PatientID = source.PatientID
		,target.PregnancyID = source.PregnancyID
		,target.CarePlanID = source.CarePlanID
		,target.EKRecord = source.EKRecord
		,target.GUID = source.GUID
		,target.UserID = source.UserID
		,target.IsRequestedNN4B = source.IsRequestedNN4B
		,target.IsRegisteredInPAS = source.IsRegisteredInPAS
		,target.BabyNotesMemoID = source.BabyNotesMemoID
		,target.DateOfBirth = source.DateOfBirth
		,target.GestationBirth = source.GestationBirth

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime