﻿CREATE procedure [E3].[LoadBirth] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			UserID
			,PregnancyID
			,AbnormalitiesCode
			,AnaesthetistRequestedCode
			,AnalgesiaDeliveryCode
			,ANProblemsCode
			,AnteriorShoulderAtDeliveryCode
			,Apgar10MinutesCode
			,Apgar1MinuteNN4BCode
			,Apgar5MinutesCode
			,BabyExamSupervisedByCode
			,BaseExcessArterialCode
			,BaseExcessVenousCode
			,BirthInjurySuspectedCode
			,BirthNN4BDTCode
			,BirthOrderCode
			,BirthOrderNRCode
			,ClassificationCode
			,CoreMidwifeRequestedCode
			,DeliveredByCode
			,DeliveryPositionCode
			,DeliveryTypeCode
			,DrugsOtherProceduresCode
			,ENBNN4BCode
			,ENBPerformedByCode
			,FBSLowestCode
			,FBSNumberCode
			,FeedingMethodDeliveryCode
			,FetalElectrodeReasonCode
			,FetocidePerformedCode
			,HeadDeliveredModeCode
			,HepatitisImmunoglobulinCode
			,InstrumentalReasonPrimaryCode
			,InstrumentalReasonSecondaryCode
			,LiquorStateDeliveryCode
			,LiquorStateRoMCode
			,ManoeuvresPerformedCode
			,MisoprostolDosesCode
			,MonitoringPerformedCode
			,NeonatalAssessmentCode
			,NeonatalAssessmentByCode
			,ObstetricianRequestedCode
			,OutcomeCode
			,PaedConsultantCode
			,Paediatrician2ArriveCode
			,PaediatricianRequestedCode
			,PlaceOfBirthCode
			,PlaceOfDeliveryCode
			,PresentAtDeliveryStaffCode
			,PresentingPartCode
			,ProblemsIntrapartumCode
			,ProceduresCode
			,ProceduresNonRegCode
			,ReasonKnownForLossCode
			,ReasonNotBornAsPlannedCode
			,ReasonNotFedWithin1HourCode
			,RegisterableBirthNN4BCode
			,ResponsibleForBNFCode
			,ResuscitatedByCode
			,ResuscitationInitiatedByCode
			,ResuscitationTypeCode
			,RoMDTCode
			,ShoulderDystociaCode
			,SkinToSkinContactCode
			,StillbirthClassificationCode
			,SupervisedByCode
			,TermMethodCode
			,TimeBirthToRespsCode
			,TimesTemperatureTakenCode
			,TransferToNN4BCode
			,VitaminKCode
			,Abnormalities
			,AgeAtIntubation
			,AnaesthetistArrivedDT
			,AnaesthetistCalledDT
			,AnaesthetistRequested
			,AnalgesiaDelivery
			,ANProblems
			,AnteriorShoulderAtDelivery
			,Apgar10Minutes
			,Apgar1MinuteNN4B
			,Apgar5Minutes
			,BabyExamSupervisedBy
			,BaseExcessArterial
			,BaseExcessVenous
			,BirthInjurySuspected
			,BirthLength
			,BirthNN4BDT
			,BirthOrder
			,BirthOrderNR
			,BirthWeightNN4B
			,BreechDiagnosedWhen
			,ChildProtectionIssues
			,Classification
			,ContinuousMonitoring
			,CordAbnormalities
			,CordBloodTaken
			,CordPhArterial
			,CordPhVenous
			,CordRoundNeck
			,CordVessels
			,CoreMidwifeArrivedDT
			,CoreMidwifeCalledDT
			,CoreMidwifeRequested
			,CTGAbnormalities
			,CTGLSCS
			,CTGNormal
			,DeliveredBy
			,DeliveryPosition
			,DeliveryType
			,DilatationARM
			,DQMidwifePresent
			,DrugsOtherProcedures
			,DurationBirthToIntubation
			,DurationO2Intubation
			,ENBMeconium
			,ENBNN4B
			,ENBPerformedBy
			,ENBUrine
			,FBS1DT
			,FBS2DT
			,FBS3DT
			,FBSBaseExcess
			,FBSLowest
			,FBSNumber
			,Fed1Hour
			,FeedingMethodDelivery
			,FetalElectrodeReason
			,FetocidePerformed
			,FHMonitoringLabour
			,ForcepsType
			,Gender
			,GestationDiag
			,HeadCircumference
			,HeadDeliveredMode
			,HeadDeliveredSDDT
			,HepatitisImmunoglobulin
			,InstrumentalReasonPrimary
			,InstrumentalReasonSecondary
			,IntermitPosPresVentil
			,IntermittentMonitoring
			,LiePriorToDelivery
			,LiquorStateDelivery
			,LiquorStateRoM
			,ManoeuvresPerformed
			,MisoprostolDoses
			,MonitoringPerformed
			,NeonatalAssessment
			,NeonatalAssessmentBy
			,ObstetricianArrivedDT
			,ObstetricianCalledDT
			,ObstetricianRequested
			,Outcome
			,PaedConsultant
			,Paediatrician2Arrive
			,PaediatricianArrivedDT
			,PaediatricianCalledDT
			,PaediatricianRequested
			,PlaceOfBirth
			,PlaceOfDelivery
			,PresentAtDeliveryStaff
			,PresentingPart
			,ProblemsIntrapartum
			,Procedures
			,ProceduresNonReg
			,ReasonKnownForLoss
			,ReasonNotBornAsPlanned
			,ReasonNotFedWithin1Hour
			,RegisterableBirthNN4B
			,ResponsibleForBNF
			,ResuscitatedBy
			,Resuscitation
			,ResuscitationInitiatedBy
			,ResuscitationType
			,RoMDT
			,RoMHow
			,ShoulderDystocia
			,ShoulderDystociaHelp
			,SkinToSkinContact
			,StillbirthClassification
			,SupervisedBy
			,SwabOrAspirateTaken
			,Temperature
			,TermMethod
			,TermReason
			,TimeBirthToGasp
			,TimeBirthToResps
			,TimesTemperatureTaken
			,TransferToNN4B
			,VitaminK
			,Waterbirth
			,McRoberts
			,McRobertsCode
			,McRobertsDT
			,McRobertsOrder
			,McRobertsOrderCode
			,McRobertsBy
			,SuprapubicPressure
			,SuprapubicPressureDT
			,SuprapubicPressureOrder
			,SuprapubicPressureBy
			,McRobertsByCode
			,SuprapubicPressureCode
			,SuprapubicPressureByCode
			,SuprapubicPressureOrderCode
			,EpisiotomyPerformed
			,EpisiotomyDT
			,EpisiotomyOrder
			,EpisiotomyBy
			,EpisiotomyEvaluation
			,EpisiotomyByCode
			,EpisiotomyEvaluationCode
			,EpisiotomyOrderCode
			,EpisiotomyPerformedCode
			,PosteriorArm
			,PosteriorArmDT
			,PosteriorArmOrder
			,PosteriorArmBy
			,PosteriorArmCode
			,PosteriorArmByCode
			,PosteriorArmOrderCode
			,WoodScrewManoeuvre
			,WoodScrewDT
			,WoodScrewOrder
			,WoodScrewBy
			,WoodScrewByCode
			,WoodScrewManoeuvreCode
			,WoodScrewOrderCode
			,AllFoursPosition
			,AllFoursDT
			,AllFoursOrder
			,AllFoursBy
			,AllFoursByCode
			,AllFoursOrderCode
			,AllFoursPositionCode
			,OtherManoeuvres
			,OtherManoeuvresCode
			,AmbulanceDT
			,AmbulanceArrivedDT
			,ForcepsTypeCode
			,GenderCode
			,HeadCircumferenceCode
			,InstrumentalDT
			,InstrumentalProcedures
			,InstrumentalProceduresCode
			,DurationBirthToIntubationCode
			,ConsultantInvolved
			,ConsultantInvolvedCode
			,FirstFeedDT
			,RoMHowCode
			,ANFetalProbsNR
			,ANFetalProbsNRCode
			,CordVesselsCode
			,FHMonitoringLabourCode
			,BreechPosition
			,CephalicPosition
			,PositionFace
			,BreechPositionCode
			,CephalicPositionCode
			,CordRoundNeckCode
			,PositionFaceCode
			,FetusDeliveredBy
			,FetusDeliveredByCode
			,Fed1HourCode
			,BreechDiagnosedWhenCode
			,ApexBeat
			,ApexBeatCode
			,TermReasonCode
			,WeightNR
			,BreastFeedingSupport
			,BreastFeedingSupportCode
			,SkinToSkinDuration
			,SkinToSkinDurationCode
			,MonitoringChangedInLabour
			,MonitoringChangedInLabourCode
			,GestationDiagCode
			,TypeOfBreastMilk
			,TypeOfBreastMilkCode
			,TransferToNNUDT
			,BreechDiagnosisDT
			,TypeOfMLUBabyDelivered
			,TypeOfMLUBabyDeliveredCode
			,DeliveryTechnique
			,DeliveryTechniqueCode
			,VitaminKPaediatricReferral
			,VitaminKPaediatricReferralCode
			,BabyLabelsLocation
			,ShoulderDystociaEmergencyCallDT
			,BabyLabelsLocationCode
			)
into
	#TLoadE3Birth
from
	(
	select
		UserID = cast(UserID as int)
		,PregnancyID = cast(PregnancyID as bigint)
		,BabyID = cast(BabyID as bigint)
		,AbnormalitiesCode = cast(AbnormalitiesCode as bigint)
		,AnaesthetistRequestedCode = cast(AnaesthetistRequestedCode as bigint)
		,AnalgesiaDeliveryCode = cast(AnalgesiaDeliveryCode as bigint)
		,ANProblemsCode = cast(ANProblemsCode as bigint)
		,AnteriorShoulderAtDeliveryCode = cast(AnteriorShoulderAtDeliveryCode as bigint)
		,Apgar10MinutesCode = cast(Apgar10MinutesCode as bigint)
		,Apgar1MinuteNN4BCode = cast(Apgar1MinuteNN4BCode as bigint)
		,Apgar5MinutesCode = cast(Apgar5MinutesCode as bigint)
		,BabyExamSupervisedByCode = cast(BabyExamSupervisedByCode as bigint)
		,BaseExcessArterialCode = cast(BaseExcessArterialCode as bigint)
		,BaseExcessVenousCode = cast(BaseExcessVenousCode as bigint)
		,BirthInjurySuspectedCode = cast(BirthInjurySuspectedCode as bigint)
		,BirthNN4BDTCode = cast(BirthNN4BDTCode as bigint)
		,BirthOrderCode = cast(BirthOrderCode as bigint)
		,BirthOrderNRCode = cast(BirthOrderNRCode as bigint)
		,ClassificationCode = cast(ClassificationCode as bigint)
		,CoreMidwifeRequestedCode = cast(CoreMidwifeRequestedCode as bigint)
		,DeliveredByCode = cast(DeliveredByCode as bigint)
		,DeliveryPositionCode = cast(DeliveryPositionCode as bigint)
		,DeliveryTypeCode = cast(DeliveryTypeCode as bigint)
		,DrugsOtherProceduresCode = cast(DrugsOtherProceduresCode as bigint)
		,ENBNN4BCode = cast(ENBNN4BCode as bigint)
		,ENBPerformedByCode = cast(ENBPerformedByCode as bigint)
		,FBSLowestCode = cast(FBSLowestCode as bigint)
		,FBSNumberCode = cast(FBSNumberCode as bigint)
		,FeedingMethodDeliveryCode = cast(FeedingMethodDeliveryCode as bigint)
		,FetalElectrodeReasonCode = cast(FetalElectrodeReasonCode as bigint)
		,FetocidePerformedCode = cast(FetocidePerformedCode as bigint)
		,HeadDeliveredModeCode = cast(HeadDeliveredModeCode as bigint)
		,HepatitisImmunoglobulinCode = cast(HepatitisImmunoglobulinCode as bigint)
		,InstrumentalReasonPrimaryCode = cast(InstrumentalReasonPrimaryCode as bigint)
		,InstrumentalReasonSecondaryCode = cast(InstrumentalReasonSecondaryCode as bigint)
		,LiquorStateDeliveryCode = cast(LiquorStateDeliveryCode as bigint)
		,LiquorStateRoMCode = cast(LiquorStateRoMCode as bigint)
		,ManoeuvresPerformedCode = cast(ManoeuvresPerformedCode as bigint)
		,MisoprostolDosesCode = cast(MisoprostolDosesCode as bigint)
		,MonitoringPerformedCode = cast(MonitoringPerformedCode as bigint)
		,NeonatalAssessmentCode = cast(NeonatalAssessmentCode as bigint)
		,NeonatalAssessmentByCode = cast(NeonatalAssessmentByCode as bigint)
		,ObstetricianRequestedCode = cast(ObstetricianRequestedCode as bigint)
		,OutcomeCode = cast(OutcomeCode as bigint)
		,PaedConsultantCode = cast(PaedConsultantCode as bigint)
		,Paediatrician2ArriveCode = cast(Paediatrician2ArriveCode as bigint)
		,PaediatricianRequestedCode = cast(PaediatricianRequestedCode as bigint)
		,PlaceOfBirthCode = cast(PlaceOfBirthCode as bigint)
		,PlaceOfDeliveryCode = cast(PlaceOfDeliveryCode as bigint)
		,PresentAtDeliveryStaffCode = cast(PresentAtDeliveryStaffCode as bigint)
		,PresentingPartCode = cast(PresentingPartCode as bigint)
		,ProblemsIntrapartumCode = cast(ProblemsIntrapartumCode as bigint)
		,ProceduresCode = cast(ProceduresCode as bigint)
		,ProceduresNonRegCode = cast(ProceduresNonRegCode as bigint)
		,ReasonKnownForLossCode = cast(ReasonKnownForLossCode as bigint)
		,ReasonNotBornAsPlannedCode = cast(ReasonNotBornAsPlannedCode as bigint)
		,ReasonNotFedWithin1HourCode = cast(ReasonNotFedWithin1HourCode as bigint)
		,RegisterableBirthNN4BCode = cast(RegisterableBirthNN4BCode as bigint)
		,ResponsibleForBNFCode = cast(ResponsibleForBNFCode as bigint)
		,ResuscitatedByCode = cast(ResuscitatedByCode as bigint)
		,ResuscitationInitiatedByCode = cast(ResuscitationInitiatedByCode as bigint)
		,ResuscitationTypeCode = cast(ResuscitationTypeCode as bigint)
		,RoMDTCode = cast(RoMDTCode as bigint)
		,ShoulderDystociaCode = cast(ShoulderDystociaCode as bigint)
		,SkinToSkinContactCode = cast(SkinToSkinContactCode as bigint)
		,StillbirthClassificationCode = cast(StillbirthClassificationCode as bigint)
		,SupervisedByCode = cast(SupervisedByCode as bigint)
		,TermMethodCode = cast(TermMethodCode as bigint)
		,TimeBirthToRespsCode = cast(TimeBirthToRespsCode as bigint)
		,TimesTemperatureTakenCode = cast(TimesTemperatureTakenCode as bigint)
		,TransferToNN4BCode = cast(TransferToNN4BCode as bigint)
		,VitaminKCode = cast(VitaminKCode as bigint)
		,Abnormalities = cast(nullif(Abnormalities, '') as nvarchar(2000))
		,AgeAtIntubation = cast(nullif(AgeAtIntubation, '') as nvarchar(2000))
		,AnaesthetistArrivedDT = cast(nullif(AnaesthetistArrivedDT, '') as nvarchar(2000))
		,AnaesthetistCalledDT = cast(nullif(AnaesthetistCalledDT, '') as nvarchar(2000))
		,AnaesthetistRequested = cast(nullif(AnaesthetistRequested, '') as nvarchar(2000))
		,AnalgesiaDelivery = cast(nullif(AnalgesiaDelivery, '') as nvarchar(2000))
		,ANProblems = cast(nullif(ANProblems, '') as nvarchar(2000))
		,AnteriorShoulderAtDelivery = cast(nullif(AnteriorShoulderAtDelivery, '') as nvarchar(2000))
		,Apgar10Minutes = cast(nullif(Apgar10Minutes, '') as nvarchar(2000))
		,Apgar1MinuteNN4B = cast(nullif(Apgar1MinuteNN4B, '') as nvarchar(2000))
		,Apgar5Minutes = cast(nullif(Apgar5Minutes, '') as nvarchar(2000))
		,BabyExamSupervisedBy = cast(nullif(BabyExamSupervisedBy, '') as nvarchar(2000))
		,BaseExcessArterial = cast(nullif(BaseExcessArterial, '') as nvarchar(2000))
		,BaseExcessVenous = cast(nullif(BaseExcessVenous, '') as nvarchar(2000))
		,BirthInjurySuspected = cast(nullif(BirthInjurySuspected, '') as nvarchar(2000))
		,BirthLength = cast(nullif(BirthLength, '') as nvarchar(2000))
		,BirthNN4BDT = cast(nullif(BirthNN4BDT, '') as nvarchar(2000))
		,BirthOrder = cast(nullif(BirthOrder, '') as nvarchar(2000))
		,BirthOrderNR = cast(nullif(BirthOrderNR, '') as nvarchar(2000))
		,BirthWeightNN4B = cast(nullif(BirthWeightNN4B, '') as nvarchar(2000))
		,BreechDiagnosedWhen = cast(nullif(BreechDiagnosedWhen, '') as nvarchar(2000))
		,ChildProtectionIssues = cast(nullif(ChildProtectionIssues, '') as nvarchar(2000))
		,Classification = cast(nullif(Classification, '') as nvarchar(2000))
		,ContinuousMonitoring = cast(nullif(ContinuousMonitoring, '') as nvarchar(2000))
		,CordAbnormalities = cast(nullif(CordAbnormalities, '') as nvarchar(2000))
		,CordBloodTaken = cast(nullif(CordBloodTaken, '') as nvarchar(2000))
		,CordPhArterial = cast(nullif(CordPhArterial, '') as nvarchar(2000))
		,CordPhVenous = cast(nullif(CordPhVenous, '') as nvarchar(2000))
		,CordRoundNeck = cast(nullif(CordRoundNeck, '') as nvarchar(2000))
		,CordVessels = cast(nullif(CordVessels, '') as nvarchar(2000))
		,CoreMidwifeArrivedDT = cast(nullif(CoreMidwifeArrivedDT, '') as nvarchar(2000))
		,CoreMidwifeCalledDT = cast(nullif(CoreMidwifeCalledDT, '') as nvarchar(2000))
		,CoreMidwifeRequested = cast(nullif(CoreMidwifeRequested, '') as nvarchar(2000))
		,CTGAbnormalities = cast(nullif(CTGAbnormalities, '') as nvarchar(2000))
		,CTGLSCS = cast(nullif(CTGLSCS, '') as nvarchar(2000))
		,CTGNormal = cast(nullif(CTGNormal, '') as nvarchar(2000))
		,DeliveredBy = cast(nullif(DeliveredBy, '') as nvarchar(2000))
		,DeliveryPosition = cast(nullif(DeliveryPosition, '') as nvarchar(2000))
		,DeliveryType = cast(nullif(DeliveryType, '') as nvarchar(2000))
		,DilatationARM = cast(nullif(DilatationARM, '') as nvarchar(2000))
		,DQMidwifePresent = cast(nullif(DQMidwifePresent, '') as nvarchar(2000))
		,DrugsOtherProcedures = cast(nullif(DrugsOtherProcedures, '') as nvarchar(2000))
		,DurationBirthToIntubation = cast(nullif(DurationBirthToIntubation, '') as nvarchar(2000))
		,DurationO2Intubation = cast(nullif(DurationO2Intubation, '') as nvarchar(2000))
		,ENBMeconium = cast(nullif(ENBMeconium, '') as nvarchar(2000))
		,ENBNN4B = cast(nullif(ENBNN4B, '') as nvarchar(2000))
		,ENBPerformedBy = cast(nullif(ENBPerformedBy, '') as nvarchar(2000))
		,ENBUrine = cast(nullif(ENBUrine, '') as nvarchar(2000))
		,FBS1DT = cast(nullif(FBS1DT, '') as nvarchar(2000))
		,FBS2DT = cast(nullif(FBS2DT, '') as nvarchar(2000))
		,FBS3DT = cast(nullif(FBS3DT, '') as nvarchar(2000))
		,FBSBaseExcess = cast(nullif(FBSBaseExcess, '') as nvarchar(2000))
		,FBSLowest = cast(nullif(FBSLowest, '') as nvarchar(2000))
		,FBSNumber = cast(nullif(FBSNumber, '') as nvarchar(2000))
		,Fed1Hour = cast(nullif(Fed1Hour, '') as nvarchar(2000))
		,FeedingMethodDelivery = cast(nullif(FeedingMethodDelivery, '') as nvarchar(2000))
		,FetalElectrodeReason = cast(nullif(FetalElectrodeReason, '') as nvarchar(2000))
		,FetocidePerformed = cast(nullif(FetocidePerformed, '') as nvarchar(2000))
		,FHMonitoringLabour = cast(nullif(FHMonitoringLabour, '') as nvarchar(2000))
		,ForcepsType = cast(nullif(ForcepsType, '') as nvarchar(2000))
		,Gender = cast(nullif(Gender, '') as nvarchar(2000))
		,GestationDiag = cast(nullif(GestationDiag, '') as nvarchar(2000))
		,HeadCircumference = cast(nullif(HeadCircumference, '') as nvarchar(2000))
		,HeadDeliveredMode = cast(nullif(HeadDeliveredMode, '') as nvarchar(2000))
		,HeadDeliveredSDDT = cast(nullif(HeadDeliveredSDDT, '') as nvarchar(2000))
		,HepatitisImmunoglobulin = cast(nullif(HepatitisImmunoglobulin, '') as nvarchar(2000))
		,InstrumentalReasonPrimary = cast(nullif(InstrumentalReasonPrimary, '') as nvarchar(2000))
		,InstrumentalReasonSecondary = cast(nullif(InstrumentalReasonSecondary, '') as nvarchar(2000))
		,IntermitPosPresVentil = cast(nullif(IntermitPosPresVentil, '') as nvarchar(2000))
		,IntermittentMonitoring = cast(nullif(IntermittentMonitoring, '') as nvarchar(2000))
		,LiePriorToDelivery = cast(nullif(LiePriorToDelivery, '') as nvarchar(2000))
		,LiquorStateDelivery = cast(nullif(LiquorStateDelivery, '') as nvarchar(2000))
		,LiquorStateRoM = cast(nullif(LiquorStateRoM, '') as nvarchar(2000))
		,ManoeuvresPerformed = cast(nullif(ManoeuvresPerformed, '') as nvarchar(2000))
		,MisoprostolDoses = cast(nullif(MisoprostolDoses, '') as nvarchar(2000))
		,MonitoringPerformed = cast(nullif(MonitoringPerformed, '') as nvarchar(2000))
		,NeonatalAssessment = cast(nullif(NeonatalAssessment, '') as nvarchar(2000))
		,NeonatalAssessmentBy = cast(nullif(NeonatalAssessmentBy, '') as nvarchar(2000))
		,ObstetricianArrivedDT = cast(nullif(ObstetricianArrivedDT, '') as nvarchar(2000))
		,ObstetricianCalledDT = cast(nullif(ObstetricianCalledDT, '') as nvarchar(2000))
		,ObstetricianRequested = cast(nullif(ObstetricianRequested, '') as nvarchar(2000))
		,Outcome = cast(nullif(Outcome, '') as nvarchar(2000))
		,PaedConsultant = cast(nullif(PaedConsultant, '') as nvarchar(2000))
		,Paediatrician2Arrive = cast(nullif(Paediatrician2Arrive, '') as nvarchar(2000))
		,PaediatricianArrivedDT = cast(nullif(PaediatricianArrivedDT, '') as nvarchar(2000))
		,PaediatricianCalledDT = cast(nullif(PaediatricianCalledDT, '') as nvarchar(2000))
		,PaediatricianRequested = cast(nullif(PaediatricianRequested, '') as nvarchar(2000))
		,PlaceOfBirth = cast(nullif(PlaceOfBirth, '') as nvarchar(2000))
		,PlaceOfDelivery = cast(nullif(PlaceOfDelivery, '') as nvarchar(2000))
		,PresentAtDeliveryStaff = cast(nullif(PresentAtDeliveryStaff, '') as nvarchar(2000))
		,PresentingPart = cast(nullif(PresentingPart, '') as nvarchar(2000))
		,ProblemsIntrapartum = cast(nullif(ProblemsIntrapartum, '') as nvarchar(2000))
		,Procedures = cast(nullif(Procedures, '') as nvarchar(2000))
		,ProceduresNonReg = cast(nullif(ProceduresNonReg, '') as nvarchar(2000))
		,ReasonKnownForLoss = cast(nullif(ReasonKnownForLoss, '') as nvarchar(2000))
		,ReasonNotBornAsPlanned = cast(nullif(ReasonNotBornAsPlanned, '') as nvarchar(2000))
		,ReasonNotFedWithin1Hour = cast(nullif(ReasonNotFedWithin1Hour, '') as nvarchar(2000))
		,RegisterableBirthNN4B = cast(nullif(RegisterableBirthNN4B, '') as nvarchar(2000))
		,ResponsibleForBNF = cast(nullif(ResponsibleForBNF, '') as nvarchar(2000))
		,ResuscitatedBy = cast(nullif(ResuscitatedBy, '') as nvarchar(2000))
		,Resuscitation = cast(nullif(Resuscitation, '') as nvarchar(2000))
		,ResuscitationInitiatedBy = cast(nullif(ResuscitationInitiatedBy, '') as nvarchar(2000))
		,ResuscitationType = cast(nullif(ResuscitationType, '') as nvarchar(2000))
		,RoMDT = cast(nullif(RoMDT, '') as nvarchar(2000))
		,RoMHow = cast(nullif(RoMHow, '') as nvarchar(2000))
		,ShoulderDystocia = cast(nullif(ShoulderDystocia, '') as nvarchar(2000))
		,ShoulderDystociaHelp = cast(nullif(ShoulderDystociaHelp, '') as nvarchar(2000))
		,SkinToSkinContact = cast(nullif(SkinToSkinContact, '') as nvarchar(2000))
		,StillbirthClassification = cast(nullif(StillbirthClassification, '') as nvarchar(2000))
		,SupervisedBy = cast(nullif(SupervisedBy, '') as nvarchar(2000))
		,SwabOrAspirateTaken = cast(nullif(SwabOrAspirateTaken, '') as nvarchar(2000))
		,Temperature = cast(nullif(Temperature, '') as nvarchar(2000))
		,TermMethod = cast(nullif(TermMethod, '') as nvarchar(2000))
		,TermReason = cast(nullif(TermReason, '') as nvarchar(2000))
		,TimeBirthToGasp = cast(nullif(TimeBirthToGasp, '') as nvarchar(2000))
		,TimeBirthToResps = cast(nullif(TimeBirthToResps, '') as nvarchar(2000))
		,TimesTemperatureTaken = cast(nullif(TimesTemperatureTaken, '') as nvarchar(2000))
		,TransferToNN4B = cast(nullif(TransferToNN4B, '') as nvarchar(2000))
		,VitaminK = cast(nullif(VitaminK, '') as nvarchar(2000))
		,Waterbirth = cast(nullif(Waterbirth, '') as nvarchar(2000))
		,McRoberts = cast(nullif(McRoberts, '') as nvarchar(2000))
		,McRobertsCode = cast(McRobertsCode as bigint)
		,McRobertsDT = cast(nullif(McRobertsDT, '') as nvarchar(2000))
		,McRobertsOrder = cast(nullif(McRobertsOrder, '') as nvarchar(2000))
		,McRobertsOrderCode = cast(McRobertsOrderCode as bigint)
		,McRobertsBy = cast(nullif(McRobertsBy, '') as nvarchar(2000))
		,SuprapubicPressure = cast(nullif(SuprapubicPressure, '') as nvarchar(2000))
		,SuprapubicPressureDT = cast(nullif(SuprapubicPressureDT, '') as nvarchar(2000))
		,SuprapubicPressureOrder = cast(nullif(SuprapubicPressureOrder, '') as nvarchar(2000))
		,SuprapubicPressureBy = cast(nullif(SuprapubicPressureBy, '') as nvarchar(2000))
		,McRobertsByCode = cast(McRobertsByCode as bigint)
		,SuprapubicPressureCode = cast(SuprapubicPressureCode as bigint)
		,SuprapubicPressureByCode = cast(SuprapubicPressureByCode as bigint)
		,SuprapubicPressureOrderCode = cast(SuprapubicPressureOrderCode as bigint)
		,EpisiotomyPerformed = cast(nullif(EpisiotomyPerformed, '') as nvarchar(2000))
		,EpisiotomyDT = cast(nullif(EpisiotomyDT, '') as nvarchar(2000))
		,EpisiotomyOrder = cast(nullif(EpisiotomyOrder, '') as nvarchar(2000))
		,EpisiotomyBy = cast(nullif(EpisiotomyBy, '') as nvarchar(2000))
		,EpisiotomyEvaluation = cast(nullif(EpisiotomyEvaluation, '') as nvarchar(2000))
		,EpisiotomyByCode = cast(EpisiotomyByCode as bigint)
		,EpisiotomyEvaluationCode = cast(EpisiotomyEvaluationCode as bigint)
		,EpisiotomyOrderCode = cast(EpisiotomyOrderCode as bigint)
		,EpisiotomyPerformedCode = cast(EpisiotomyPerformedCode as bigint)
		,PosteriorArm = cast(nullif(PosteriorArm, '') as nvarchar(2000))
		,PosteriorArmDT = cast(nullif(PosteriorArmDT, '') as nvarchar(2000))
		,PosteriorArmOrder = cast(nullif(PosteriorArmOrder, '') as nvarchar(2000))
		,PosteriorArmBy = cast(nullif(PosteriorArmBy, '') as nvarchar(2000))
		,PosteriorArmCode = cast(PosteriorArmCode as bigint)
		,PosteriorArmByCode = cast(PosteriorArmByCode as bigint)
		,PosteriorArmOrderCode = cast(PosteriorArmOrderCode as bigint)
		,WoodScrewManoeuvre = cast(nullif(WoodScrewManoeuvre, '') as nvarchar(2000))
		,WoodScrewDT = cast(nullif(WoodScrewDT, '') as nvarchar(2000))
		,WoodScrewOrder = cast(nullif(WoodScrewOrder, '') as nvarchar(2000))
		,WoodScrewBy = cast(nullif(WoodScrewBy, '') as nvarchar(2000))
		,WoodScrewByCode = cast(WoodScrewByCode as bigint)
		,WoodScrewManoeuvreCode = cast(WoodScrewManoeuvreCode as bigint)
		,WoodScrewOrderCode = cast(WoodScrewOrderCode as bigint)
		,AllFoursPosition = cast(nullif(AllFoursPosition, '') as nvarchar(2000))
		,AllFoursDT = cast(nullif(AllFoursDT, '') as nvarchar(2000))
		,AllFoursOrder = cast(nullif(AllFoursOrder, '') as nvarchar(2000))
		,AllFoursBy = cast(nullif(AllFoursBy, '') as nvarchar(2000))
		,AllFoursByCode = cast(AllFoursByCode as bigint)
		,AllFoursOrderCode = cast(AllFoursOrderCode as bigint)
		,AllFoursPositionCode = cast(AllFoursPositionCode as bigint)
		,OtherManoeuvres = cast(nullif(OtherManoeuvres, '') as nvarchar(2000))
		,OtherManoeuvresCode = cast(OtherManoeuvresCode as bigint)
		,AmbulanceDT = cast(nullif(AmbulanceDT, '') as nvarchar(2000))
		,AmbulanceArrivedDT = cast(nullif(AmbulanceArrivedDT, '') as nvarchar(2000))
		,ForcepsTypeCode = cast(ForcepsTypeCode as bigint)
		,GenderCode = cast(GenderCode as bigint)
		,HeadCircumferenceCode = cast(HeadCircumferenceCode as bigint)
		,InstrumentalDT = cast(nullif(InstrumentalDT, '') as nvarchar(2000))
		,InstrumentalProcedures = cast(nullif(InstrumentalProcedures, '') as nvarchar(2000))
		,InstrumentalProceduresCode = cast(InstrumentalProceduresCode as bigint)
		,DurationBirthToIntubationCode = cast(DurationBirthToIntubationCode as bigint)
		,ConsultantInvolved = cast(nullif(ConsultantInvolved, '') as nvarchar(2000))
		,ConsultantInvolvedCode = cast(ConsultantInvolvedCode as bigint)
		,FirstFeedDT = cast(nullif(FirstFeedDT, '') as nvarchar(2000))
		,RoMHowCode = cast(RoMHowCode as bigint)
		,ANFetalProbsNR = cast(nullif(ANFetalProbsNR, '') as nvarchar(2000))
		,ANFetalProbsNRCode = cast(ANFetalProbsNRCode as bigint)
		,CordVesselsCode = cast(CordVesselsCode as bigint)
		,FHMonitoringLabourCode = cast(FHMonitoringLabourCode as bigint)
		,BreechPosition = cast(nullif(BreechPosition, '') as nvarchar(2000))
		,CephalicPosition = cast(nullif(CephalicPosition, '') as nvarchar(2000))
		,PositionFace = cast(nullif(PositionFace, '') as nvarchar(2000))
		,BreechPositionCode = cast(BreechPositionCode as bigint)
		,CephalicPositionCode = cast(CephalicPositionCode as bigint)
		,CordRoundNeckCode = cast(CordRoundNeckCode as bigint)
		,PositionFaceCode = cast(PositionFaceCode as bigint)
		,FetusDeliveredBy = cast(nullif(FetusDeliveredBy, '') as nvarchar(2000))
		,FetusDeliveredByCode = cast(FetusDeliveredByCode as bigint)
		,Fed1HourCode = cast(Fed1HourCode as bigint)
		,BreechDiagnosedWhenCode = cast(BreechDiagnosedWhenCode as bigint)
		,ApexBeat = cast(nullif(ApexBeat, '') as nvarchar(2000))
		,ApexBeatCode = cast(ApexBeatCode as bigint)
		,TermReasonCode = cast(TermReasonCode as bigint)
		,WeightNR = cast(nullif(WeightNR, '') as nvarchar(2000))
		,BreastFeedingSupport = cast(nullif(BreastFeedingSupport, '') as nvarchar(2000))
		,BreastFeedingSupportCode = cast(BreastFeedingSupportCode as bigint)
		,SkinToSkinDuration = cast(nullif(SkinToSkinDuration, '') as nvarchar(2000))
		,SkinToSkinDurationCode = cast(SkinToSkinDurationCode as bigint)
		,MonitoringChangedInLabour = cast(nullif(MonitoringChangedInLabour, '') as nvarchar(2000))
		,MonitoringChangedInLabourCode = cast(MonitoringChangedInLabourCode as bigint)
		,GestationDiagCode = cast(GestationDiagCode as bigint)
		,TypeOfBreastMilk = cast(nullif(TypeOfBreastMilk, '') as nvarchar(2000))
		,TypeOfBreastMilkCode = cast(TypeOfBreastMilkCode as bigint)
		,TransferToNNUDT = cast(nullif(TransferToNNUDT, '') as nvarchar(2000))
		,BreechDiagnosisDT = cast(nullif(BreechDiagnosisDT, '') as nvarchar(2000))
		,TypeOfMLUBabyDelivered = cast(nullif(TypeOfMLUBabyDelivered, '') as nvarchar(2000))
		,TypeOfMLUBabyDeliveredCode = cast(TypeOfMLUBabyDeliveredCode as bigint)
		,DeliveryTechnique = cast(nullif(DeliveryTechnique, '') as nvarchar(2000))
		,DeliveryTechniqueCode = cast(DeliveryTechniqueCode as bigint)
		,VitaminKPaediatricReferral = cast(nullif(VitaminKPaediatricReferral, '') as nvarchar(2000))
		,VitaminKPaediatricReferralCode = cast(VitaminKPaediatricReferralCode as bigint)
		,BabyLabelsLocation = cast(nullif(BabyLabelsLocation, '') as nvarchar(2000))
		,ShoulderDystociaEmergencyCallDT = cast(nullif(ShoulderDystociaEmergencyCallDT, '') as nvarchar(2000))
		,BabyLabelsLocationCode = cast(BabyLabelsLocationCode as bigint)

	from
		(
		select
			UserID = Birth.UserID 
			,PregnancyID = Birth.PregnancyID
			,BabyID = Birth.BabyID
			,AbnormalitiesCode = Birth.Abnormalities_Value
			,AnaesthetistRequestedCode = Birth.AnaesthetistRequested_Value
			,AnalgesiaDeliveryCode = Birth.AnalgesiaDelivery_Value
			,ANProblemsCode = Birth.ANProblems_Value
			,AnteriorShoulderAtDeliveryCode = Birth.AnteriorShoulderAtDelivery_Value
			,Apgar10MinutesCode = Birth.Apgar10Minutes_Value
			,Apgar1MinuteNN4BCode = Birth.Apgar1MinuteNN4B_Value
			,Apgar5MinutesCode = Birth.Apgar5Minutes_Value
			,BabyExamSupervisedByCode = Birth.BabyExamSupervisedBy_Value
			,BaseExcessArterialCode = Birth.BaseExcessArterial_Value
			,BaseExcessVenousCode = Birth.BaseExcessVenous_Value
			,BirthInjurySuspectedCode = Birth.BirthInjurySuspected_Value
			,BirthNN4BDTCode = Birth.BirthNN4BDT_Value
			,BirthOrderCode = Birth.BirthOrder_Value
			,BirthOrderNRCode = Birth.BirthOrderNR_Value
			,ClassificationCode = Birth.Classification_Value
			,CoreMidwifeRequestedCode = Birth.CoreMidwifeRequested_Value
			,DeliveredByCode = Birth.DeliveredBy_Value
			,DeliveryPositionCode = Birth.DeliveryPosition_Value
			,DeliveryTypeCode = Birth.DeliveryType_Value
			,DrugsOtherProceduresCode = Birth.DrugsOtherProcedures_Value
			,ENBNN4BCode = Birth.ENBNN4B_Value
			,ENBPerformedByCode = Birth.ENBPerformedBy_Value
			,FBSLowestCode = Birth.FBSLowest_Value
			,FBSNumberCode = Birth.FBSNumber_Value
			,FeedingMethodDeliveryCode = Birth.FeedingMethodDelivery_Value
			,FetalElectrodeReasonCode = Birth.FetalElectrodeReason_Value
			,FetocidePerformedCode = Birth.FetocidePerformed_Value
			,HeadDeliveredModeCode = Birth.HeadDeliveredMode_Value
			,HepatitisImmunoglobulinCode = Birth.HepatitisImmunoglobulin_Value
			,InstrumentalReasonPrimaryCode = Birth.InstrumentalReasonPrimary_Value
			,InstrumentalReasonSecondaryCode = Birth.InstrumentalReasonSecondary_Value
			,LiquorStateDeliveryCode = Birth.LiquorStateDelivery_Value
			,LiquorStateRoMCode = Birth.LiquorStateRoM_Value
			,ManoeuvresPerformedCode = Birth.ManoeuvresPerformed_Value
			,MisoprostolDosesCode = Birth.MisoprostolDoses_Value
			,MonitoringPerformedCode = Birth.MonitoringPerformed_Value
			,NeonatalAssessmentCode = Birth.NeonatalAssessment_Value
			,NeonatalAssessmentByCode = Birth.NeonatalAssessmentBy_Value
			,ObstetricianRequestedCode = Birth.ObstetricianRequested_Value
			,OutcomeCode = Birth.Outcome_Value
			,PaedConsultantCode = Birth.PaedConsultant_Value
			,Paediatrician2ArriveCode = Birth.Paediatrician2Arrive_Value
			,PaediatricianRequestedCode = Birth.PaediatricianRequested_Value
			,PlaceOfBirthCode = Birth.PlaceOfBirth_Value
			,PlaceOfDeliveryCode = Birth.PlaceOfDelivery_Value
			,PresentAtDeliveryStaffCode = Birth.PresentAtDeliveryStaff_Value
			,PresentingPartCode = Birth.PresentingPart_Value
			,ProblemsIntrapartumCode = Birth.ProblemsIntrapartum_Value
			,ProceduresCode = Birth.Procedures_Value
			,ProceduresNonRegCode = Birth.ProceduresNonReg_Value
			,ReasonKnownForLossCode = Birth.ReasonKnownForLoss_Value
			,ReasonNotBornAsPlannedCode = Birth.ReasonNotBornAsPlanned_Value
			,ReasonNotFedWithin1HourCode = Birth.ReasonNotFedWithin1Hour_Value
			,RegisterableBirthNN4BCode = Birth.RegisterableBirthNN4B_Value
			,ResponsibleForBNFCode = Birth.ResponsibleForBNF_Value
			,ResuscitatedByCode = Birth.ResuscitatedBy_Value
			,ResuscitationInitiatedByCode = Birth.ResuscitationInitiatedBy_Value
			,ResuscitationTypeCode = Birth.ResuscitationType_Value
			,RoMDTCode = Birth.RoMDT_Value
			,ShoulderDystociaCode = Birth.ShoulderDystocia_Value
			,SkinToSkinContactCode = Birth.SkinToSkinContact_Value
			,StillbirthClassificationCode = Birth.StillbirthClassification_Value
			,SupervisedByCode = Birth.SupervisedBy_Value
			,TermMethodCode = Birth.TermMethod_Value
			,TimeBirthToRespsCode = Birth.TimeBirthToResps_Value
			,TimesTemperatureTakenCode = Birth.TimesTemperatureTaken_Value
			,TransferToNN4BCode = Birth.TransferToNN4B_Value
			,VitaminKCode = Birth.VitaminK_Value
			,Abnormalities = Birth.Abnormalities
			,AgeAtIntubation = Birth.AgeAtIntubation
			,AnaesthetistArrivedDT = Birth.AnaesthetistArrivedDT
			,AnaesthetistCalledDT = Birth.AnaesthetistCalledDT
			,AnaesthetistRequested = Birth.AnaesthetistRequested
			,AnalgesiaDelivery = Birth.AnalgesiaDelivery
			,ANProblems = Birth.ANProblems
			,AnteriorShoulderAtDelivery = Birth.AnteriorShoulderAtDelivery
			,Apgar10Minutes = Birth.Apgar10Minutes
			,Apgar1MinuteNN4B = Birth.Apgar1MinuteNN4B
			,Apgar5Minutes = Birth.Apgar5Minutes
			,BabyExamSupervisedBy = Birth.BabyExamSupervisedBy
			,BaseExcessArterial = Birth.BaseExcessArterial
			,BaseExcessVenous = Birth.BaseExcessVenous
			,BirthInjurySuspected = Birth.BirthInjurySuspected
			,BirthLength = Birth.BirthLength
			,BirthNN4BDT = Birth.BirthNN4BDT
			,BirthOrder = Birth.BirthOrder
			,BirthOrderNR = Birth.BirthOrderNR
			,BirthWeightNN4B = Birth.BirthWeightNN4B
			,BreechDiagnosedWhen = Birth.BreechDiagnosedWhen
			,ChildProtectionIssues = Birth.ChildProtectionIssues
			,Classification = Birth.Classification
			,ContinuousMonitoring = Birth.ContinuousMonitoring
			,CordAbnormalities = Birth.CordAbnormalities
			,CordBloodTaken = Birth.CordBloodTaken
			,CordPhArterial = Birth.CordPhArterial
			,CordPhVenous = Birth.CordPhVenous
			,CordRoundNeck = Birth.CordRoundNeck
			,CordVessels = Birth.CordVessels
			,CoreMidwifeArrivedDT = Birth.CoreMidwifeArrivedDT
			,CoreMidwifeCalledDT = Birth.CoreMidwifeCalledDT
			,CoreMidwifeRequested = Birth.CoreMidwifeRequested
			,CTGAbnormalities = Birth.CTGAbnormalities
			,CTGLSCS = Birth.CTGLSCS
			,CTGNormal = Birth.CTGNormal
			,DeliveredBy = Birth.DeliveredBy
			,DeliveryPosition = Birth.DeliveryPosition
			,DeliveryType = Birth.DeliveryType
			,DilatationARM = Birth.DilatationARM
			,DQMidwifePresent = Birth.DQMidwifePresent
			,DrugsOtherProcedures = Birth.DrugsOtherProcedures
			,DurationBirthToIntubation = Birth.DurationBirthToIntubation
			,DurationO2Intubation = Birth.DurationO2Intubation
			,ENBMeconium = Birth.ENBMeconium
			,ENBNN4B = Birth.ENBNN4B
			,ENBPerformedBy = Birth.ENBPerformedBy
			,ENBUrine = Birth.ENBUrine
			,FBS1DT = Birth.FBS1DT
			,FBS2DT = Birth.FBS2DT
			,FBS3DT = Birth.FBS3DT
			,FBSBaseExcess = Birth.FBSBaseExcess
			,FBSLowest = Birth.FBSLowest
			,FBSNumber = Birth.FBSNumber
			,Fed1Hour = Birth.Fed1Hour
			,FeedingMethodDelivery = Birth.FeedingMethodDelivery
			,FetalElectrodeReason = Birth.FetalElectrodeReason
			,FetocidePerformed = Birth.FetocidePerformed
			,FHMonitoringLabour = Birth.FHMonitoringLabour
			,ForcepsType = Birth.ForcepsType
			,Gender = Birth.Gender
			,GestationDiag = Birth.GestationDiag
			,HeadCircumference = Birth.HeadCircumference
			,HeadDeliveredMode = Birth.HeadDeliveredMode
			,HeadDeliveredSDDT = Birth.HeadDeliveredSDDT
			,HepatitisImmunoglobulin = Birth.HepatitisImmunoglobulin
			,InstrumentalReasonPrimary = Birth.InstrumentalReasonPrimary
			,InstrumentalReasonSecondary = Birth.InstrumentalReasonSecondary
			,IntermitPosPresVentil = Birth.IntermitPosPresVentil
			,IntermittentMonitoring = Birth.IntermittentMonitoring
			,LiePriorToDelivery = Birth.LiePriorToDelivery
			,LiquorStateDelivery = Birth.LiquorStateDelivery
			,LiquorStateRoM = Birth.LiquorStateRoM
			,ManoeuvresPerformed = Birth.ManoeuvresPerformed
			,MisoprostolDoses = Birth.MisoprostolDoses
			,MonitoringPerformed = Birth.MonitoringPerformed
			,NeonatalAssessment = Birth.NeonatalAssessment
			,NeonatalAssessmentBy = Birth.NeonatalAssessmentBy
			,ObstetricianArrivedDT = Birth.ObstetricianArrivedDT
			,ObstetricianCalledDT = Birth.ObstetricianCalledDT
			,ObstetricianRequested = Birth.ObstetricianRequested
			,Outcome = Birth.Outcome
			,PaedConsultant = Birth.PaedConsultant
			,Paediatrician2Arrive = Birth.Paediatrician2Arrive
			,PaediatricianArrivedDT = Birth.PaediatricianArrivedDT
			,PaediatricianCalledDT = Birth.PaediatricianCalledDT
			,PaediatricianRequested = Birth.PaediatricianRequested
			,PlaceOfBirth = Birth.PlaceOfBirth
			,PlaceOfDelivery = Birth.PlaceOfDelivery
			,PresentAtDeliveryStaff = Birth.PresentAtDeliveryStaff
			,PresentingPart = Birth.PresentingPart
			,ProblemsIntrapartum = Birth.ProblemsIntrapartum
			,Procedures = Birth.Procedures
			,ProceduresNonReg = Birth.ProceduresNonReg
			,ReasonKnownForLoss = Birth.ReasonKnownForLoss
			,ReasonNotBornAsPlanned = Birth.ReasonNotBornAsPlanned
			,ReasonNotFedWithin1Hour = Birth.ReasonNotFedWithin1Hour
			,RegisterableBirthNN4B = Birth.RegisterableBirthNN4B
			,ResponsibleForBNF = Birth.ResponsibleForBNF
			,ResuscitatedBy = Birth.ResuscitatedBy
			,Resuscitation = Birth.Resuscitation
			,ResuscitationInitiatedBy = Birth.ResuscitationInitiatedBy
			,ResuscitationType = Birth.ResuscitationType
			,RoMDT = Birth.RoMDT
			,RoMHow = Birth.RoMHow
			,ShoulderDystocia = Birth.ShoulderDystocia
			,ShoulderDystociaHelp = Birth.ShoulderDystociaHelp
			,SkinToSkinContact = Birth.SkinToSkinContact
			,StillbirthClassification = Birth.StillbirthClassification
			,SupervisedBy = Birth.SupervisedBy
			,SwabOrAspirateTaken = Birth.SwabOrAspirateTaken
			,Temperature = Birth.Temperature
			,TermMethod = Birth.TermMethod
			,TermReason = Birth.TermReason
			,TimeBirthToGasp = Birth.TimeBirthToGasp
			,TimeBirthToResps = Birth.TimeBirthToResps
			,TimesTemperatureTaken = Birth.TimesTemperatureTaken
			,TransferToNN4B = Birth.TransferToNN4B
			,VitaminK = Birth.VitaminK
			,Waterbirth = Birth.Waterbirth
			,McRoberts = Birth.McRoberts
			,McRobertsCode = Birth.McRoberts_Value
			,McRobertsDT = Birth.McRobertsDT
			,McRobertsOrder = Birth.McRobertsOrder
			,McRobertsOrderCode = Birth.McRobertsOrder_Value
			,McRobertsBy = Birth.McRobertsBy
			,SuprapubicPressure = Birth.SuprapubicPressure
			,SuprapubicPressureDT = Birth.SuprapubicPressureDT
			,SuprapubicPressureOrder = Birth.SuprapubicPressureOrder
			,SuprapubicPressureBy = Birth.SuprapubicPressureBy
			,McRobertsByCode = Birth.McRobertsBy_Value
			,SuprapubicPressureCode = Birth.SuprapubicPressure_Value
			,SuprapubicPressureByCode = Birth.SuprapubicPressureBy_Value
			,SuprapubicPressureOrderCode = Birth.SuprapubicPressureOrder_Value
			,EpisiotomyPerformed = Birth.EpisiotomyPerformed
			,EpisiotomyDT = Birth.EpisiotomyDT
			,EpisiotomyOrder = Birth.EpisiotomyOrder
			,EpisiotomyBy = Birth.EpisiotomyBy
			,EpisiotomyEvaluation = Birth.EpisiotomyEvaluation
			,EpisiotomyByCode = Birth.EpisiotomyBy_Value
			,EpisiotomyEvaluationCode = Birth.EpisiotomyEvaluation_Value
			,EpisiotomyOrderCode = Birth.EpisiotomyOrder_Value
			,EpisiotomyPerformedCode = Birth.EpisiotomyPerformed_Value
			,PosteriorArm = Birth.PosteriorArm
			,PosteriorArmDT = Birth.PosteriorArmDT
			,PosteriorArmOrder = Birth.PosteriorArmOrder
			,PosteriorArmBy = Birth.PosteriorArmBy
			,PosteriorArmCode = Birth.PosteriorArm_Value
			,PosteriorArmByCode = Birth.PosteriorArmBy_Value
			,PosteriorArmOrderCode = Birth.PosteriorArmOrder_Value
			,WoodScrewManoeuvre = Birth.WoodScrewManoeuvre
			,WoodScrewDT = Birth.WoodScrewDT
			,WoodScrewOrder = Birth.WoodScrewOrder
			,WoodScrewBy = Birth.WoodScrewBy
			,WoodScrewByCode = Birth.WoodScrewBy_Value
			,WoodScrewManoeuvreCode = Birth.WoodScrewManoeuvre_Value
			,WoodScrewOrderCode = Birth.WoodScrewOrder_Value
			,AllFoursPosition = Birth.AllFoursPosition
			,AllFoursDT = Birth.AllFoursDT
			,AllFoursOrder = Birth.AllFoursOrder
			,AllFoursBy = Birth.AllFoursBy
			,AllFoursByCode = Birth.AllFoursBy_Value
			,AllFoursOrderCode = Birth.AllFoursOrder_Value
			,AllFoursPositionCode = Birth.AllFoursPosition_Value
			,OtherManoeuvres = Birth.OtherManoeuvres
			,OtherManoeuvresCode = Birth.OtherManoeuvres_Value
			,AmbulanceDT = Birth.AmbulanceDT
			,AmbulanceArrivedDT = Birth.AmbulanceArrivedDT
			,ForcepsTypeCode = Birth.ForcepsType_Value
			,GenderCode = Birth.Gender_Value
			,HeadCircumferenceCode = Birth.HeadCircumference_Value
			,InstrumentalDT = Birth.InstrumentalDT
			,InstrumentalProcedures = Birth.InstrumentalProcedures
			,InstrumentalProceduresCode = Birth.InstrumentalProcedures_Value
			,DurationBirthToIntubationCode = Birth.DurationBirthToIntubation_Value
			,ConsultantInvolved = Birth.ConsultantInvolved
			,ConsultantInvolvedCode = Birth.ConsultantInvolved_Value
			,FirstFeedDT = Birth.FirstFeedDT
			,RoMHowCode = Birth.RoMHow_Value
			,ANFetalProbsNR = Birth.ANFetalProbsNR
			,ANFetalProbsNRCode = Birth.ANFetalProbsNR_Value
			,CordVesselsCode = Birth.CordVessels_Value
			,FHMonitoringLabourCode = Birth.FHMonitoringLabour_Value
			,BreechPosition = Birth.BreechPosition
			,CephalicPosition = Birth.CephalicPosition
			,PositionFace = Birth.PositionFace
			,BreechPositionCode = Birth.BreechPosition_Value
			,CephalicPositionCode = Birth.CephalicPosition_Value
			,CordRoundNeckCode = Birth.CordRoundNeck_Value
			,PositionFaceCode = Birth.PositionFace_Value
			,FetusDeliveredBy = Birth.FetusDeliveredBy
			,FetusDeliveredByCode = Birth.FetusDeliveredBy_Value
			,Fed1HourCode = Birth.Fed1Hour_Value
			,BreechDiagnosedWhenCode = Birth.BreechDiagnosedWhen_Value
			,ApexBeat = Birth.ApexBeat
			,ApexBeatCode = Birth.ApexBeat_Value
			,TermReasonCode = Birth.TermReason_Value
			,WeightNR = Birth.WeightNR
			,BreastFeedingSupport = Birth.BreastFeedingSupport
			,BreastFeedingSupportCode = Birth.BreastFeedingSupport_Value
			,SkinToSkinDuration = Birth.SkinToSkinDuration
			,SkinToSkinDurationCode = Birth.SkinToSkinDuration_Value
			,MonitoringChangedInLabour = Birth.MonitoringChangedInLabour
			,MonitoringChangedInLabourCode = Birth.MonitoringChangedInLabour_Value
			,GestationDiagCode = Birth.GestationDiag_Value
			,TypeOfBreastMilk = Birth.TypeOfBreastMilk
			,TypeOfBreastMilkCode = Birth.TypeOfBreastMilk_Value
			,TransferToNNUDT = Birth.TransferToNNUDT
			,BreechDiagnosisDT = Birth.BreechDiagnosisDT
			,TypeOfMLUBabyDelivered = Birth.TypeOfMLUBabyDelivered
			,TypeOfMLUBabyDeliveredCode = Birth.TypeOfMLUBabyDelivered_Value
			,DeliveryTechnique = Birth.DeliveryTechnique
			,DeliveryTechniqueCode = Birth.DeliveryTechnique_Value
			,VitaminKPaediatricReferral = Birth.VitaminKPaediatricReferral
			,VitaminKPaediatricReferralCode = Birth.VitaminKPaediatricReferral_Value
			,BabyLabelsLocation = Birth.BabyLabelsLocation
			,ShoulderDystociaEmergencyCallDT = Birth.ShoulderDystociaEmergencyCallDT
			,BabyLabelsLocationCode = Birth.BabyLabelsLocation_Value
		from
			E3.dbo.tblReport_Birth_2_108 Birth

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3Birth on #TLoadE3Birth
	(
	BabyID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	E3.Birth target
using
	(
	select
		*
	from
		#TLoadE3Birth
	
	) source
	on	source.BabyID = target.BabyID

when not matched by source
then
	delete

when not matched
then
	insert
		(
		UserID
		,PregnancyID
		,BabyID
		,AbnormalitiesCode
		,AnaesthetistRequestedCode
		,AnalgesiaDeliveryCode
		,ANProblemsCode
		,AnteriorShoulderAtDeliveryCode
		,Apgar10MinutesCode
		,Apgar1MinuteNN4BCode
		,Apgar5MinutesCode
		,BabyExamSupervisedByCode
		,BaseExcessArterialCode
		,BaseExcessVenousCode
		,BirthInjurySuspectedCode
		,BirthNN4BDTCode
		,BirthOrderCode
		,BirthOrderNRCode
		,ClassificationCode
		,CoreMidwifeRequestedCode
		,DeliveredByCode
		,DeliveryPositionCode
		,DeliveryTypeCode
		,DrugsOtherProceduresCode
		,ENBNN4BCode
		,ENBPerformedByCode
		,FBSLowestCode
		,FBSNumberCode
		,FeedingMethodDeliveryCode
		,FetalElectrodeReasonCode
		,FetocidePerformedCode
		,HeadDeliveredModeCode
		,HepatitisImmunoglobulinCode
		,InstrumentalReasonPrimaryCode
		,InstrumentalReasonSecondaryCode
		,LiquorStateDeliveryCode
		,LiquorStateRoMCode
		,ManoeuvresPerformedCode
		,MisoprostolDosesCode
		,MonitoringPerformedCode
		,NeonatalAssessmentCode
		,NeonatalAssessmentByCode
		,ObstetricianRequestedCode
		,OutcomeCode
		,PaedConsultantCode
		,Paediatrician2ArriveCode
		,PaediatricianRequestedCode
		,PlaceOfBirthCode
		,PlaceOfDeliveryCode
		,PresentAtDeliveryStaffCode
		,PresentingPartCode
		,ProblemsIntrapartumCode
		,ProceduresCode
		,ProceduresNonRegCode
		,ReasonKnownForLossCode
		,ReasonNotBornAsPlannedCode
		,ReasonNotFedWithin1HourCode
		,RegisterableBirthNN4BCode
		,ResponsibleForBNFCode
		,ResuscitatedByCode
		,ResuscitationInitiatedByCode
		,ResuscitationTypeCode
		,RoMDTCode
		,ShoulderDystociaCode
		,SkinToSkinContactCode
		,StillbirthClassificationCode
		,SupervisedByCode
		,TermMethodCode
		,TimeBirthToRespsCode
		,TimesTemperatureTakenCode
		,TransferToNN4BCode
		,VitaminKCode
		,Abnormalities
		,AgeAtIntubation
		,AnaesthetistArrivedDT
		,AnaesthetistCalledDT
		,AnaesthetistRequested
		,AnalgesiaDelivery
		,ANProblems
		,AnteriorShoulderAtDelivery
		,Apgar10Minutes
		,Apgar1MinuteNN4B
		,Apgar5Minutes
		,BabyExamSupervisedBy
		,BaseExcessArterial
		,BaseExcessVenous
		,BirthInjurySuspected
		,BirthLength
		,BirthNN4BDT
		,BirthOrder
		,BirthOrderNR
		,BirthWeightNN4B
		,BreechDiagnosedWhen
		,ChildProtectionIssues
		,Classification
		,ContinuousMonitoring
		,CordAbnormalities
		,CordBloodTaken
		,CordPhArterial
		,CordPhVenous
		,CordRoundNeck
		,CordVessels
		,CoreMidwifeArrivedDT
		,CoreMidwifeCalledDT
		,CoreMidwifeRequested
		,CTGAbnormalities
		,CTGLSCS
		,CTGNormal
		,DeliveredBy
		,DeliveryPosition
		,DeliveryType
		,DilatationARM
		,DQMidwifePresent
		,DrugsOtherProcedures
		,DurationBirthToIntubation
		,DurationO2Intubation
		,ENBMeconium
		,ENBNN4B
		,ENBPerformedBy
		,ENBUrine
		,FBS1DT
		,FBS2DT
		,FBS3DT
		,FBSBaseExcess
		,FBSLowest
		,FBSNumber
		,Fed1Hour
		,FeedingMethodDelivery
		,FetalElectrodeReason
		,FetocidePerformed
		,FHMonitoringLabour
		,ForcepsType
		,Gender
		,GestationDiag
		,HeadCircumference
		,HeadDeliveredMode
		,HeadDeliveredSDDT
		,HepatitisImmunoglobulin
		,InstrumentalReasonPrimary
		,InstrumentalReasonSecondary
		,IntermitPosPresVentil
		,IntermittentMonitoring
		,LiePriorToDelivery
		,LiquorStateDelivery
		,LiquorStateRoM
		,ManoeuvresPerformed
		,MisoprostolDoses
		,MonitoringPerformed
		,NeonatalAssessment
		,NeonatalAssessmentBy
		,ObstetricianArrivedDT
		,ObstetricianCalledDT
		,ObstetricianRequested
		,Outcome
		,PaedConsultant
		,Paediatrician2Arrive
		,PaediatricianArrivedDT
		,PaediatricianCalledDT
		,PaediatricianRequested
		,PlaceOfBirth
		,PlaceOfDelivery
		,PresentAtDeliveryStaff
		,PresentingPart
		,ProblemsIntrapartum
		,Procedures
		,ProceduresNonReg
		,ReasonKnownForLoss
		,ReasonNotBornAsPlanned
		,ReasonNotFedWithin1Hour
		,RegisterableBirthNN4B
		,ResponsibleForBNF
		,ResuscitatedBy
		,Resuscitation
		,ResuscitationInitiatedBy
		,ResuscitationType
		,RoMDT
		,RoMHow
		,ShoulderDystocia
		,ShoulderDystociaHelp
		,SkinToSkinContact
		,StillbirthClassification
		,SupervisedBy
		,SwabOrAspirateTaken
		,Temperature
		,TermMethod
		,TermReason
		,TimeBirthToGasp
		,TimeBirthToResps
		,TimesTemperatureTaken
		,TransferToNN4B
		,VitaminK
		,Waterbirth
		,McRoberts
		,McRobertsCode
		,McRobertsDT
		,McRobertsOrder
		,McRobertsOrderCode
		,McRobertsBy
		,SuprapubicPressure
		,SuprapubicPressureDT
		,SuprapubicPressureOrder
		,SuprapubicPressureBy
		,McRobertsByCode
		,SuprapubicPressureCode
		,SuprapubicPressureByCode
		,SuprapubicPressureOrderCode
		,EpisiotomyPerformed
		,EpisiotomyDT
		,EpisiotomyOrder
		,EpisiotomyBy
		,EpisiotomyEvaluation
		,EpisiotomyByCode
		,EpisiotomyEvaluationCode
		,EpisiotomyOrderCode
		,EpisiotomyPerformedCode
		,PosteriorArm
		,PosteriorArmDT
		,PosteriorArmOrder
		,PosteriorArmBy
		,PosteriorArmCode
		,PosteriorArmByCode
		,PosteriorArmOrderCode
		,WoodScrewManoeuvre
		,WoodScrewDT
		,WoodScrewOrder
		,WoodScrewBy
		,WoodScrewByCode
		,WoodScrewManoeuvreCode
		,WoodScrewOrderCode
		,AllFoursPosition
		,AllFoursDT
		,AllFoursOrder
		,AllFoursBy
		,AllFoursByCode
		,AllFoursOrderCode
		,AllFoursPositionCode
		,OtherManoeuvres
		,OtherManoeuvresCode
		,AmbulanceDT
		,AmbulanceArrivedDT
		,ForcepsTypeCode
		,GenderCode
		,HeadCircumferenceCode
		,InstrumentalDT
		,InstrumentalProcedures
		,InstrumentalProceduresCode
		,DurationBirthToIntubationCode
		,ConsultantInvolved
		,ConsultantInvolvedCode
		,FirstFeedDT
		,RoMHowCode
		,ANFetalProbsNR
		,ANFetalProbsNRCode
		,CordVesselsCode
		,FHMonitoringLabourCode
		,BreechPosition
		,CephalicPosition
		,PositionFace
		,BreechPositionCode
		,CephalicPositionCode
		,CordRoundNeckCode
		,PositionFaceCode
		,FetusDeliveredBy
		,FetusDeliveredByCode
		,Fed1HourCode
		,BreechDiagnosedWhenCode
		,ApexBeat
		,ApexBeatCode
		,TermReasonCode
		,WeightNR
		,BreastFeedingSupport
		,BreastFeedingSupportCode
		,SkinToSkinDuration
		,SkinToSkinDurationCode
		,MonitoringChangedInLabour
		,MonitoringChangedInLabourCode
		,GestationDiagCode
		,TypeOfBreastMilk
		,TypeOfBreastMilkCode
		,TransferToNNUDT
		,BreechDiagnosisDT
		,TypeOfMLUBabyDelivered
		,TypeOfMLUBabyDeliveredCode
		,DeliveryTechnique
		,DeliveryTechniqueCode
		,VitaminKPaediatricReferral
		,VitaminKPaediatricReferralCode
		,BabyLabelsLocation
		,ShoulderDystociaEmergencyCallDT
		,BabyLabelsLocationCode

		,Created
		,ByWhom
		)
	values
		(
		source.UserID
		,source.PregnancyID
		,source.BabyID
		,source.AbnormalitiesCode
		,source.AnaesthetistRequestedCode
		,source.AnalgesiaDeliveryCode
		,source.ANProblemsCode
		,source.AnteriorShoulderAtDeliveryCode
		,source.Apgar10MinutesCode
		,source.Apgar1MinuteNN4BCode
		,source.Apgar5MinutesCode
		,source.BabyExamSupervisedByCode
		,source.BaseExcessArterialCode
		,source.BaseExcessVenousCode
		,source.BirthInjurySuspectedCode
		,source.BirthNN4BDTCode
		,source.BirthOrderCode
		,source.BirthOrderNRCode
		,source.ClassificationCode
		,source.CoreMidwifeRequestedCode
		,source.DeliveredByCode
		,source.DeliveryPositionCode
		,source.DeliveryTypeCode
		,source.DrugsOtherProceduresCode
		,source.ENBNN4BCode
		,source.ENBPerformedByCode
		,source.FBSLowestCode
		,source.FBSNumberCode
		,source.FeedingMethodDeliveryCode
		,source.FetalElectrodeReasonCode
		,source.FetocidePerformedCode
		,source.HeadDeliveredModeCode
		,source.HepatitisImmunoglobulinCode
		,source.InstrumentalReasonPrimaryCode
		,source.InstrumentalReasonSecondaryCode
		,source.LiquorStateDeliveryCode
		,source.LiquorStateRoMCode
		,source.ManoeuvresPerformedCode
		,source.MisoprostolDosesCode
		,source.MonitoringPerformedCode
		,source.NeonatalAssessmentCode
		,source.NeonatalAssessmentByCode
		,source.ObstetricianRequestedCode
		,source.OutcomeCode
		,source.PaedConsultantCode
		,source.Paediatrician2ArriveCode
		,source.PaediatricianRequestedCode
		,source.PlaceOfBirthCode
		,source.PlaceOfDeliveryCode
		,source.PresentAtDeliveryStaffCode
		,source.PresentingPartCode
		,source.ProblemsIntrapartumCode
		,source.ProceduresCode
		,source.ProceduresNonRegCode
		,source.ReasonKnownForLossCode
		,source.ReasonNotBornAsPlannedCode
		,source.ReasonNotFedWithin1HourCode
		,source.RegisterableBirthNN4BCode
		,source.ResponsibleForBNFCode
		,source.ResuscitatedByCode
		,source.ResuscitationInitiatedByCode
		,source.ResuscitationTypeCode
		,source.RoMDTCode
		,source.ShoulderDystociaCode
		,source.SkinToSkinContactCode
		,source.StillbirthClassificationCode
		,source.SupervisedByCode
		,source.TermMethodCode
		,source.TimeBirthToRespsCode
		,source.TimesTemperatureTakenCode
		,source.TransferToNN4BCode
		,source.VitaminKCode
		,source.Abnormalities
		,source.AgeAtIntubation
		,source.AnaesthetistArrivedDT
		,source.AnaesthetistCalledDT
		,source.AnaesthetistRequested
		,source.AnalgesiaDelivery
		,source.ANProblems
		,source.AnteriorShoulderAtDelivery
		,source.Apgar10Minutes
		,source.Apgar1MinuteNN4B
		,source.Apgar5Minutes
		,source.BabyExamSupervisedBy
		,source.BaseExcessArterial
		,source.BaseExcessVenous
		,source.BirthInjurySuspected
		,source.BirthLength
		,source.BirthNN4BDT
		,source.BirthOrder
		,source.BirthOrderNR
		,source.BirthWeightNN4B
		,source.BreechDiagnosedWhen
		,source.ChildProtectionIssues
		,source.Classification
		,source.ContinuousMonitoring
		,source.CordAbnormalities
		,source.CordBloodTaken
		,source.CordPhArterial
		,source.CordPhVenous
		,source.CordRoundNeck
		,source.CordVessels
		,source.CoreMidwifeArrivedDT
		,source.CoreMidwifeCalledDT
		,source.CoreMidwifeRequested
		,source.CTGAbnormalities
		,source.CTGLSCS
		,source.CTGNormal
		,source.DeliveredBy
		,source.DeliveryPosition
		,source.DeliveryType
		,source.DilatationARM
		,source.DQMidwifePresent
		,source.DrugsOtherProcedures
		,source.DurationBirthToIntubation
		,source.DurationO2Intubation
		,source.ENBMeconium
		,source.ENBNN4B
		,source.ENBPerformedBy
		,source.ENBUrine
		,source.FBS1DT
		,source.FBS2DT
		,source.FBS3DT
		,source.FBSBaseExcess
		,source.FBSLowest
		,source.FBSNumber
		,source.Fed1Hour
		,source.FeedingMethodDelivery
		,source.FetalElectrodeReason
		,source.FetocidePerformed
		,source.FHMonitoringLabour
		,source.ForcepsType
		,source.Gender
		,source.GestationDiag
		,source.HeadCircumference
		,source.HeadDeliveredMode
		,source.HeadDeliveredSDDT
		,source.HepatitisImmunoglobulin
		,source.InstrumentalReasonPrimary
		,source.InstrumentalReasonSecondary
		,source.IntermitPosPresVentil
		,source.IntermittentMonitoring
		,source.LiePriorToDelivery
		,source.LiquorStateDelivery
		,source.LiquorStateRoM
		,source.ManoeuvresPerformed
		,source.MisoprostolDoses
		,source.MonitoringPerformed
		,source.NeonatalAssessment
		,source.NeonatalAssessmentBy
		,source.ObstetricianArrivedDT
		,source.ObstetricianCalledDT
		,source.ObstetricianRequested
		,source.Outcome
		,source.PaedConsultant
		,source.Paediatrician2Arrive
		,source.PaediatricianArrivedDT
		,source.PaediatricianCalledDT
		,source.PaediatricianRequested
		,source.PlaceOfBirth
		,source.PlaceOfDelivery
		,source.PresentAtDeliveryStaff
		,source.PresentingPart
		,source.ProblemsIntrapartum
		,source.Procedures
		,source.ProceduresNonReg
		,source.ReasonKnownForLoss
		,source.ReasonNotBornAsPlanned
		,source.ReasonNotFedWithin1Hour
		,source.RegisterableBirthNN4B
		,source.ResponsibleForBNF
		,source.ResuscitatedBy
		,source.Resuscitation
		,source.ResuscitationInitiatedBy
		,source.ResuscitationType
		,source.RoMDT
		,source.RoMHow
		,source.ShoulderDystocia
		,source.ShoulderDystociaHelp
		,source.SkinToSkinContact
		,source.StillbirthClassification
		,source.SupervisedBy
		,source.SwabOrAspirateTaken
		,source.Temperature
		,source.TermMethod
		,source.TermReason
		,source.TimeBirthToGasp
		,source.TimeBirthToResps
		,source.TimesTemperatureTaken
		,source.TransferToNN4B
		,source.VitaminK
		,source.Waterbirth
		,source.McRoberts
		,source.McRobertsCode
		,source.McRobertsDT
		,source.McRobertsOrder
		,source.McRobertsOrderCode
		,source.McRobertsBy
		,source.SuprapubicPressure
		,source.SuprapubicPressureDT
		,source.SuprapubicPressureOrder
		,source.SuprapubicPressureBy
		,source.McRobertsByCode
		,source.SuprapubicPressureCode
		,source.SuprapubicPressureByCode
		,source.SuprapubicPressureOrderCode
		,source.EpisiotomyPerformed
		,source.EpisiotomyDT
		,source.EpisiotomyOrder
		,source.EpisiotomyBy
		,source.EpisiotomyEvaluation
		,source.EpisiotomyByCode
		,source.EpisiotomyEvaluationCode
		,source.EpisiotomyOrderCode
		,source.EpisiotomyPerformedCode
		,source.PosteriorArm
		,source.PosteriorArmDT
		,source.PosteriorArmOrder
		,source.PosteriorArmBy
		,source.PosteriorArmCode
		,source.PosteriorArmByCode
		,source.PosteriorArmOrderCode
		,source.WoodScrewManoeuvre
		,source.WoodScrewDT
		,source.WoodScrewOrder
		,source.WoodScrewBy
		,source.WoodScrewByCode
		,source.WoodScrewManoeuvreCode
		,source.WoodScrewOrderCode
		,source.AllFoursPosition
		,source.AllFoursDT
		,source.AllFoursOrder
		,source.AllFoursBy
		,source.AllFoursByCode
		,source.AllFoursOrderCode
		,source.AllFoursPositionCode
		,source.OtherManoeuvres
		,source.OtherManoeuvresCode
		,source.AmbulanceDT
		,source.AmbulanceArrivedDT
		,source.ForcepsTypeCode
		,source.GenderCode
		,source.HeadCircumferenceCode
		,source.InstrumentalDT
		,source.InstrumentalProcedures
		,source.InstrumentalProceduresCode
		,source.DurationBirthToIntubationCode
		,source.ConsultantInvolved
		,source.ConsultantInvolvedCode
		,source.FirstFeedDT
		,source.RoMHowCode
		,source.ANFetalProbsNR
		,source.ANFetalProbsNRCode
		,source.CordVesselsCode
		,source.FHMonitoringLabourCode
		,source.BreechPosition
		,source.CephalicPosition
		,source.PositionFace
		,source.BreechPositionCode
		,source.CephalicPositionCode
		,source.CordRoundNeckCode
		,source.PositionFaceCode
		,source.FetusDeliveredBy
		,source.FetusDeliveredByCode
		,source.Fed1HourCode
		,source.BreechDiagnosedWhenCode
		,source.ApexBeat
		,source.ApexBeatCode
		,source.TermReasonCode
		,source.WeightNR
		,source.BreastFeedingSupport
		,source.BreastFeedingSupportCode
		,source.SkinToSkinDuration
		,source.SkinToSkinDurationCode
		,source.MonitoringChangedInLabour
		,source.MonitoringChangedInLabourCode
		,source.GestationDiagCode
		,source.TypeOfBreastMilk
		,source.TypeOfBreastMilkCode
		,source.TransferToNNUDT
		,source.BreechDiagnosisDT
		,source.TypeOfMLUBabyDelivered
		,source.TypeOfMLUBabyDeliveredCode
		,source.DeliveryTechnique
		,source.DeliveryTechniqueCode
		,source.VitaminKPaediatricReferral
		,source.VitaminKPaediatricReferralCode
		,source.BabyLabelsLocation
		,source.ShoulderDystociaEmergencyCallDT
		,source.BabyLabelsLocationCode

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.UserID
		,target.PregnancyID
		,target.AbnormalitiesCode
		,target.AnaesthetistRequestedCode
		,target.AnalgesiaDeliveryCode
		,target.ANProblemsCode
		,target.AnteriorShoulderAtDeliveryCode
		,target.Apgar10MinutesCode
		,target.Apgar1MinuteNN4BCode
		,target.Apgar5MinutesCode
		,target.BabyExamSupervisedByCode
		,target.BaseExcessArterialCode
		,target.BaseExcessVenousCode
		,target.BirthInjurySuspectedCode
		,target.BirthNN4BDTCode
		,target.BirthOrderCode
		,target.BirthOrderNRCode
		,target.ClassificationCode
		,target.CoreMidwifeRequestedCode
		,target.DeliveredByCode
		,target.DeliveryPositionCode
		,target.DeliveryTypeCode
		,target.DrugsOtherProceduresCode
		,target.ENBNN4BCode
		,target.ENBPerformedByCode
		,target.FBSLowestCode
		,target.FBSNumberCode
		,target.FeedingMethodDeliveryCode
		,target.FetalElectrodeReasonCode
		,target.FetocidePerformedCode
		,target.HeadDeliveredModeCode
		,target.HepatitisImmunoglobulinCode
		,target.InstrumentalReasonPrimaryCode
		,target.InstrumentalReasonSecondaryCode
		,target.LiquorStateDeliveryCode
		,target.LiquorStateRoMCode
		,target.ManoeuvresPerformedCode
		,target.MisoprostolDosesCode
		,target.MonitoringPerformedCode
		,target.NeonatalAssessmentCode
		,target.NeonatalAssessmentByCode
		,target.ObstetricianRequestedCode
		,target.OutcomeCode
		,target.PaedConsultantCode
		,target.Paediatrician2ArriveCode
		,target.PaediatricianRequestedCode
		,target.PlaceOfBirthCode
		,target.PlaceOfDeliveryCode
		,target.PresentAtDeliveryStaffCode
		,target.PresentingPartCode
		,target.ProblemsIntrapartumCode
		,target.ProceduresCode
		,target.ProceduresNonRegCode
		,target.ReasonKnownForLossCode
		,target.ReasonNotBornAsPlannedCode
		,target.ReasonNotFedWithin1HourCode
		,target.RegisterableBirthNN4BCode
		,target.ResponsibleForBNFCode
		,target.ResuscitatedByCode
		,target.ResuscitationInitiatedByCode
		,target.ResuscitationTypeCode
		,target.RoMDTCode
		,target.ShoulderDystociaCode
		,target.SkinToSkinContactCode
		,target.StillbirthClassificationCode
		,target.SupervisedByCode
		,target.TermMethodCode
		,target.TimeBirthToRespsCode
		,target.TimesTemperatureTakenCode
		,target.TransferToNN4BCode
		,target.VitaminKCode
		,target.Abnormalities
		,target.AgeAtIntubation
		,target.AnaesthetistArrivedDT
		,target.AnaesthetistCalledDT
		,target.AnaesthetistRequested
		,target.AnalgesiaDelivery
		,target.ANProblems
		,target.AnteriorShoulderAtDelivery
		,target.Apgar10Minutes
		,target.Apgar1MinuteNN4B
		,target.Apgar5Minutes
		,target.BabyExamSupervisedBy
		,target.BaseExcessArterial
		,target.BaseExcessVenous
		,target.BirthInjurySuspected
		,target.BirthLength
		,target.BirthNN4BDT
		,target.BirthOrder
		,target.BirthOrderNR
		,target.BirthWeightNN4B
		,target.BreechDiagnosedWhen
		,target.ChildProtectionIssues
		,target.Classification
		,target.ContinuousMonitoring
		,target.CordAbnormalities
		,target.CordBloodTaken
		,target.CordPhArterial
		,target.CordPhVenous
		,target.CordRoundNeck
		,target.CordVessels
		,target.CoreMidwifeArrivedDT
		,target.CoreMidwifeCalledDT
		,target.CoreMidwifeRequested
		,target.CTGAbnormalities
		,target.CTGLSCS
		,target.CTGNormal
		,target.DeliveredBy
		,target.DeliveryPosition
		,target.DeliveryType
		,target.DilatationARM
		,target.DQMidwifePresent
		,target.DrugsOtherProcedures
		,target.DurationBirthToIntubation
		,target.DurationO2Intubation
		,target.ENBMeconium
		,target.ENBNN4B
		,target.ENBPerformedBy
		,target.ENBUrine
		,target.FBS1DT
		,target.FBS2DT
		,target.FBS3DT
		,target.FBSBaseExcess
		,target.FBSLowest
		,target.FBSNumber
		,target.Fed1Hour
		,target.FeedingMethodDelivery
		,target.FetalElectrodeReason
		,target.FetocidePerformed
		,target.FHMonitoringLabour
		,target.ForcepsType
		,target.Gender
		,target.GestationDiag
		,target.HeadCircumference
		,target.HeadDeliveredMode
		,target.HeadDeliveredSDDT
		,target.HepatitisImmunoglobulin
		,target.InstrumentalReasonPrimary
		,target.InstrumentalReasonSecondary
		,target.IntermitPosPresVentil
		,target.IntermittentMonitoring
		,target.LiePriorToDelivery
		,target.LiquorStateDelivery
		,target.LiquorStateRoM
		,target.ManoeuvresPerformed
		,target.MisoprostolDoses
		,target.MonitoringPerformed
		,target.NeonatalAssessment
		,target.NeonatalAssessmentBy
		,target.ObstetricianArrivedDT
		,target.ObstetricianCalledDT
		,target.ObstetricianRequested
		,target.Outcome
		,target.PaedConsultant
		,target.Paediatrician2Arrive
		,target.PaediatricianArrivedDT
		,target.PaediatricianCalledDT
		,target.PaediatricianRequested
		,target.PlaceOfBirth
		,target.PlaceOfDelivery
		,target.PresentAtDeliveryStaff
		,target.PresentingPart
		,target.ProblemsIntrapartum
		,target.Procedures
		,target.ProceduresNonReg
		,target.ReasonKnownForLoss
		,target.ReasonNotBornAsPlanned
		,target.ReasonNotFedWithin1Hour
		,target.RegisterableBirthNN4B
		,target.ResponsibleForBNF
		,target.ResuscitatedBy
		,target.Resuscitation
		,target.ResuscitationInitiatedBy
		,target.ResuscitationType
		,target.RoMDT
		,target.RoMHow
		,target.ShoulderDystocia
		,target.ShoulderDystociaHelp
		,target.SkinToSkinContact
		,target.StillbirthClassification
		,target.SupervisedBy
		,target.SwabOrAspirateTaken
		,target.Temperature
		,target.TermMethod
		,target.TermReason
		,target.TimeBirthToGasp
		,target.TimeBirthToResps
		,target.TimesTemperatureTaken
		,target.TransferToNN4B
		,target.VitaminK
		,target.Waterbirth
		,target.McRoberts
		,target.McRobertsCode
		,target.McRobertsDT
		,target.McRobertsOrder
		,target.McRobertsOrderCode
		,target.McRobertsBy
		,target.SuprapubicPressure
		,target.SuprapubicPressureDT
		,target.SuprapubicPressureOrder
		,target.SuprapubicPressureBy
		,target.McRobertsByCode
		,target.SuprapubicPressureCode
		,target.SuprapubicPressureByCode
		,target.SuprapubicPressureOrderCode
		,target.EpisiotomyPerformed
		,target.EpisiotomyDT
		,target.EpisiotomyOrder
		,target.EpisiotomyBy
		,target.EpisiotomyEvaluation
		,target.EpisiotomyByCode
		,target.EpisiotomyEvaluationCode
		,target.EpisiotomyOrderCode
		,target.EpisiotomyPerformedCode
		,target.PosteriorArm
		,target.PosteriorArmDT
		,target.PosteriorArmOrder
		,target.PosteriorArmBy
		,target.PosteriorArmCode
		,target.PosteriorArmByCode
		,target.PosteriorArmOrderCode
		,target.WoodScrewManoeuvre
		,target.WoodScrewDT
		,target.WoodScrewOrder
		,target.WoodScrewBy
		,target.WoodScrewByCode
		,target.WoodScrewManoeuvreCode
		,target.WoodScrewOrderCode
		,target.AllFoursPosition
		,target.AllFoursDT
		,target.AllFoursOrder
		,target.AllFoursBy
		,target.AllFoursByCode
		,target.AllFoursOrderCode
		,target.AllFoursPositionCode
		,target.OtherManoeuvres
		,target.OtherManoeuvresCode
		,target.AmbulanceDT
		,target.AmbulanceArrivedDT
		,target.ForcepsTypeCode
		,target.GenderCode
		,target.HeadCircumferenceCode
		,target.InstrumentalDT
		,target.InstrumentalProcedures
		,target.InstrumentalProceduresCode
		,target.DurationBirthToIntubationCode
		,target.ConsultantInvolved
		,target.ConsultantInvolvedCode
		,target.FirstFeedDT
		,target.RoMHowCode
		,target.ANFetalProbsNR
		,target.ANFetalProbsNRCode
		,target.CordVesselsCode
		,target.FHMonitoringLabourCode
		,target.BreechPosition
		,target.CephalicPosition
		,target.PositionFace
		,target.BreechPositionCode
		,target.CephalicPositionCode
		,target.CordRoundNeckCode
		,target.PositionFaceCode
		,target.FetusDeliveredBy
		,target.FetusDeliveredByCode
		,target.Fed1HourCode
		,target.BreechDiagnosedWhenCode
		,target.ApexBeat
		,target.ApexBeatCode
		,target.TermReasonCode
		,target.WeightNR
		,target.BreastFeedingSupport
		,target.BreastFeedingSupportCode
		,target.SkinToSkinDuration
		,target.SkinToSkinDurationCode
		,target.MonitoringChangedInLabour
		,target.MonitoringChangedInLabourCode
		,target.GestationDiagCode
		,target.TypeOfBreastMilk
		,target.TypeOfBreastMilkCode
		,target.TransferToNNUDT
		,target.BreechDiagnosisDT
		,target.TypeOfMLUBabyDelivered
		,target.TypeOfMLUBabyDeliveredCode
		,target.DeliveryTechnique
		,target.DeliveryTechniqueCode
		,target.VitaminKPaediatricReferral
		,target.VitaminKPaediatricReferralCode
		,target.BabyLabelsLocation
		,target.ShoulderDystociaEmergencyCallDT
		,target.BabyLabelsLocationCode
		)

then
	update
	set
		target.UserID = source.UserID
		,target.PregnancyID = source.PregnancyID
		,target.AbnormalitiesCode = source.AbnormalitiesCode
		,target.AnaesthetistRequestedCode = source.AnaesthetistRequestedCode
		,target.AnalgesiaDeliveryCode = source.AnalgesiaDeliveryCode
		,target.ANProblemsCode = source.ANProblemsCode
		,target.AnteriorShoulderAtDeliveryCode = source.AnteriorShoulderAtDeliveryCode
		,target.Apgar10MinutesCode = source.Apgar10MinutesCode
		,target.Apgar1MinuteNN4BCode = source.Apgar1MinuteNN4BCode
		,target.Apgar5MinutesCode = source.Apgar5MinutesCode
		,target.BabyExamSupervisedByCode = source.BabyExamSupervisedByCode
		,target.BaseExcessArterialCode = source.BaseExcessArterialCode
		,target.BaseExcessVenousCode = source.BaseExcessVenousCode
		,target.BirthInjurySuspectedCode = source.BirthInjurySuspectedCode
		,target.BirthNN4BDTCode = source.BirthNN4BDTCode
		,target.BirthOrderCode = source.BirthOrderCode
		,target.BirthOrderNRCode = source.BirthOrderNRCode
		,target.ClassificationCode = source.ClassificationCode
		,target.CoreMidwifeRequestedCode = source.CoreMidwifeRequestedCode
		,target.DeliveredByCode = source.DeliveredByCode
		,target.DeliveryPositionCode = source.DeliveryPositionCode
		,target.DeliveryTypeCode = source.DeliveryTypeCode
		,target.DrugsOtherProceduresCode = source.DrugsOtherProceduresCode
		,target.ENBNN4BCode = source.ENBNN4BCode
		,target.ENBPerformedByCode = source.ENBPerformedByCode
		,target.FBSLowestCode = source.FBSLowestCode
		,target.FBSNumberCode = source.FBSNumberCode
		,target.FeedingMethodDeliveryCode = source.FeedingMethodDeliveryCode
		,target.FetalElectrodeReasonCode = source.FetalElectrodeReasonCode
		,target.FetocidePerformedCode = source.FetocidePerformedCode
		,target.HeadDeliveredModeCode = source.HeadDeliveredModeCode
		,target.HepatitisImmunoglobulinCode = source.HepatitisImmunoglobulinCode
		,target.InstrumentalReasonPrimaryCode = source.InstrumentalReasonPrimaryCode
		,target.InstrumentalReasonSecondaryCode = source.InstrumentalReasonSecondaryCode
		,target.LiquorStateDeliveryCode = source.LiquorStateDeliveryCode
		,target.LiquorStateRoMCode = source.LiquorStateRoMCode
		,target.ManoeuvresPerformedCode = source.ManoeuvresPerformedCode
		,target.MisoprostolDosesCode = source.MisoprostolDosesCode
		,target.MonitoringPerformedCode = source.MonitoringPerformedCode
		,target.NeonatalAssessmentCode = source.NeonatalAssessmentCode
		,target.NeonatalAssessmentByCode = source.NeonatalAssessmentByCode
		,target.ObstetricianRequestedCode = source.ObstetricianRequestedCode
		,target.OutcomeCode = source.OutcomeCode
		,target.PaedConsultantCode = source.PaedConsultantCode
		,target.Paediatrician2ArriveCode = source.Paediatrician2ArriveCode
		,target.PaediatricianRequestedCode = source.PaediatricianRequestedCode
		,target.PlaceOfBirthCode = source.PlaceOfBirthCode
		,target.PlaceOfDeliveryCode = source.PlaceOfDeliveryCode
		,target.PresentAtDeliveryStaffCode = source.PresentAtDeliveryStaffCode
		,target.PresentingPartCode = source.PresentingPartCode
		,target.ProblemsIntrapartumCode = source.ProblemsIntrapartumCode
		,target.ProceduresCode = source.ProceduresCode
		,target.ProceduresNonRegCode = source.ProceduresNonRegCode
		,target.ReasonKnownForLossCode = source.ReasonKnownForLossCode
		,target.ReasonNotBornAsPlannedCode = source.ReasonNotBornAsPlannedCode
		,target.ReasonNotFedWithin1HourCode = source.ReasonNotFedWithin1HourCode
		,target.RegisterableBirthNN4BCode = source.RegisterableBirthNN4BCode
		,target.ResponsibleForBNFCode = source.ResponsibleForBNFCode
		,target.ResuscitatedByCode = source.ResuscitatedByCode
		,target.ResuscitationInitiatedByCode = source.ResuscitationInitiatedByCode
		,target.ResuscitationTypeCode = source.ResuscitationTypeCode
		,target.RoMDTCode = source.RoMDTCode
		,target.ShoulderDystociaCode = source.ShoulderDystociaCode
		,target.SkinToSkinContactCode = source.SkinToSkinContactCode
		,target.StillbirthClassificationCode = source.StillbirthClassificationCode
		,target.SupervisedByCode = source.SupervisedByCode
		,target.TermMethodCode = source.TermMethodCode
		,target.TimeBirthToRespsCode = source.TimeBirthToRespsCode
		,target.TimesTemperatureTakenCode = source.TimesTemperatureTakenCode
		,target.TransferToNN4BCode = source.TransferToNN4BCode
		,target.VitaminKCode = source.VitaminKCode
		,target.Abnormalities = source.Abnormalities
		,target.AgeAtIntubation = source.AgeAtIntubation
		,target.AnaesthetistArrivedDT = source.AnaesthetistArrivedDT
		,target.AnaesthetistCalledDT = source.AnaesthetistCalledDT
		,target.AnaesthetistRequested = source.AnaesthetistRequested
		,target.AnalgesiaDelivery = source.AnalgesiaDelivery
		,target.ANProblems = source.ANProblems
		,target.AnteriorShoulderAtDelivery = source.AnteriorShoulderAtDelivery
		,target.Apgar10Minutes = source.Apgar10Minutes
		,target.Apgar1MinuteNN4B = source.Apgar1MinuteNN4B
		,target.Apgar5Minutes = source.Apgar5Minutes
		,target.BabyExamSupervisedBy = source.BabyExamSupervisedBy
		,target.BaseExcessArterial = source.BaseExcessArterial
		,target.BaseExcessVenous = source.BaseExcessVenous
		,target.BirthInjurySuspected = source.BirthInjurySuspected
		,target.BirthLength = source.BirthLength
		,target.BirthNN4BDT = source.BirthNN4BDT
		,target.BirthOrder = source.BirthOrder
		,target.BirthOrderNR = source.BirthOrderNR
		,target.BirthWeightNN4B = source.BirthWeightNN4B
		,target.BreechDiagnosedWhen = source.BreechDiagnosedWhen
		,target.ChildProtectionIssues = source.ChildProtectionIssues
		,target.Classification = source.Classification
		,target.ContinuousMonitoring = source.ContinuousMonitoring
		,target.CordAbnormalities = source.CordAbnormalities
		,target.CordBloodTaken = source.CordBloodTaken
		,target.CordPhArterial = source.CordPhArterial
		,target.CordPhVenous = source.CordPhVenous
		,target.CordRoundNeck = source.CordRoundNeck
		,target.CordVessels = source.CordVessels
		,target.CoreMidwifeArrivedDT = source.CoreMidwifeArrivedDT
		,target.CoreMidwifeCalledDT = source.CoreMidwifeCalledDT
		,target.CoreMidwifeRequested = source.CoreMidwifeRequested
		,target.CTGAbnormalities = source.CTGAbnormalities
		,target.CTGLSCS = source.CTGLSCS
		,target.CTGNormal = source.CTGNormal
		,target.DeliveredBy = source.DeliveredBy
		,target.DeliveryPosition = source.DeliveryPosition
		,target.DeliveryType = source.DeliveryType
		,target.DilatationARM = source.DilatationARM
		,target.DQMidwifePresent = source.DQMidwifePresent
		,target.DrugsOtherProcedures = source.DrugsOtherProcedures
		,target.DurationBirthToIntubation = source.DurationBirthToIntubation
		,target.DurationO2Intubation = source.DurationO2Intubation
		,target.ENBMeconium = source.ENBMeconium
		,target.ENBNN4B = source.ENBNN4B
		,target.ENBPerformedBy = source.ENBPerformedBy
		,target.ENBUrine = source.ENBUrine
		,target.FBS1DT = source.FBS1DT
		,target.FBS2DT = source.FBS2DT
		,target.FBS3DT = source.FBS3DT
		,target.FBSBaseExcess = source.FBSBaseExcess
		,target.FBSLowest = source.FBSLowest
		,target.FBSNumber = source.FBSNumber
		,target.Fed1Hour = source.Fed1Hour
		,target.FeedingMethodDelivery = source.FeedingMethodDelivery
		,target.FetalElectrodeReason = source.FetalElectrodeReason
		,target.FetocidePerformed = source.FetocidePerformed
		,target.FHMonitoringLabour = source.FHMonitoringLabour
		,target.ForcepsType = source.ForcepsType
		,target.Gender = source.Gender
		,target.GestationDiag = source.GestationDiag
		,target.HeadCircumference = source.HeadCircumference
		,target.HeadDeliveredMode = source.HeadDeliveredMode
		,target.HeadDeliveredSDDT = source.HeadDeliveredSDDT
		,target.HepatitisImmunoglobulin = source.HepatitisImmunoglobulin
		,target.InstrumentalReasonPrimary = source.InstrumentalReasonPrimary
		,target.InstrumentalReasonSecondary = source.InstrumentalReasonSecondary
		,target.IntermitPosPresVentil = source.IntermitPosPresVentil
		,target.IntermittentMonitoring = source.IntermittentMonitoring
		,target.LiePriorToDelivery = source.LiePriorToDelivery
		,target.LiquorStateDelivery = source.LiquorStateDelivery
		,target.LiquorStateRoM = source.LiquorStateRoM
		,target.ManoeuvresPerformed = source.ManoeuvresPerformed
		,target.MisoprostolDoses = source.MisoprostolDoses
		,target.MonitoringPerformed = source.MonitoringPerformed
		,target.NeonatalAssessment = source.NeonatalAssessment
		,target.NeonatalAssessmentBy = source.NeonatalAssessmentBy
		,target.ObstetricianArrivedDT = source.ObstetricianArrivedDT
		,target.ObstetricianCalledDT = source.ObstetricianCalledDT
		,target.ObstetricianRequested = source.ObstetricianRequested
		,target.Outcome = source.Outcome
		,target.PaedConsultant = source.PaedConsultant
		,target.Paediatrician2Arrive = source.Paediatrician2Arrive
		,target.PaediatricianArrivedDT = source.PaediatricianArrivedDT
		,target.PaediatricianCalledDT = source.PaediatricianCalledDT
		,target.PaediatricianRequested = source.PaediatricianRequested
		,target.PlaceOfBirth = source.PlaceOfBirth
		,target.PlaceOfDelivery = source.PlaceOfDelivery
		,target.PresentAtDeliveryStaff = source.PresentAtDeliveryStaff
		,target.PresentingPart = source.PresentingPart
		,target.ProblemsIntrapartum = source.ProblemsIntrapartum
		,target.Procedures = source.Procedures
		,target.ProceduresNonReg = source.ProceduresNonReg
		,target.ReasonKnownForLoss = source.ReasonKnownForLoss
		,target.ReasonNotBornAsPlanned = source.ReasonNotBornAsPlanned
		,target.ReasonNotFedWithin1Hour = source.ReasonNotFedWithin1Hour
		,target.RegisterableBirthNN4B = source.RegisterableBirthNN4B
		,target.ResponsibleForBNF = source.ResponsibleForBNF
		,target.ResuscitatedBy = source.ResuscitatedBy
		,target.Resuscitation = source.Resuscitation
		,target.ResuscitationInitiatedBy = source.ResuscitationInitiatedBy
		,target.ResuscitationType = source.ResuscitationType
		,target.RoMDT = source.RoMDT
		,target.RoMHow = source.RoMHow
		,target.ShoulderDystocia = source.ShoulderDystocia
		,target.ShoulderDystociaHelp = source.ShoulderDystociaHelp
		,target.SkinToSkinContact = source.SkinToSkinContact
		,target.StillbirthClassification = source.StillbirthClassification
		,target.SupervisedBy = source.SupervisedBy
		,target.SwabOrAspirateTaken = source.SwabOrAspirateTaken
		,target.Temperature = source.Temperature
		,target.TermMethod = source.TermMethod
		,target.TermReason = source.TermReason
		,target.TimeBirthToGasp = source.TimeBirthToGasp
		,target.TimeBirthToResps = source.TimeBirthToResps
		,target.TimesTemperatureTaken = source.TimesTemperatureTaken
		,target.TransferToNN4B = source.TransferToNN4B
		,target.VitaminK = source.VitaminK
		,target.Waterbirth = source.Waterbirth
		,target.McRoberts = source.McRoberts
		,target.McRobertsCode = source.McRobertsCode
		,target.McRobertsDT = source.McRobertsDT
		,target.McRobertsOrder = source.McRobertsOrder
		,target.McRobertsOrderCode = source.McRobertsOrderCode
		,target.McRobertsBy = source.McRobertsBy
		,target.SuprapubicPressure = source.SuprapubicPressure
		,target.SuprapubicPressureDT = source.SuprapubicPressureDT
		,target.SuprapubicPressureOrder = source.SuprapubicPressureOrder
		,target.SuprapubicPressureBy = source.SuprapubicPressureBy
		,target.McRobertsByCode = source.McRobertsByCode
		,target.SuprapubicPressureCode = source.SuprapubicPressureCode
		,target.SuprapubicPressureByCode = source.SuprapubicPressureByCode
		,target.SuprapubicPressureOrderCode = source.SuprapubicPressureOrderCode
		,target.EpisiotomyPerformed = source.EpisiotomyPerformed
		,target.EpisiotomyDT = source.EpisiotomyDT
		,target.EpisiotomyOrder = source.EpisiotomyOrder
		,target.EpisiotomyBy = source.EpisiotomyBy
		,target.EpisiotomyEvaluation = source.EpisiotomyEvaluation
		,target.EpisiotomyByCode = source.EpisiotomyByCode
		,target.EpisiotomyEvaluationCode = source.EpisiotomyEvaluationCode
		,target.EpisiotomyOrderCode = source.EpisiotomyOrderCode
		,target.EpisiotomyPerformedCode = source.EpisiotomyPerformedCode
		,target.PosteriorArm = source.PosteriorArm
		,target.PosteriorArmDT = source.PosteriorArmDT
		,target.PosteriorArmOrder = source.PosteriorArmOrder
		,target.PosteriorArmBy = source.PosteriorArmBy
		,target.PosteriorArmCode = source.PosteriorArmCode
		,target.PosteriorArmByCode = source.PosteriorArmByCode
		,target.PosteriorArmOrderCode = source.PosteriorArmOrderCode
		,target.WoodScrewManoeuvre = source.WoodScrewManoeuvre
		,target.WoodScrewDT = source.WoodScrewDT
		,target.WoodScrewOrder = source.WoodScrewOrder
		,target.WoodScrewBy = source.WoodScrewBy
		,target.WoodScrewByCode = source.WoodScrewByCode
		,target.WoodScrewManoeuvreCode = source.WoodScrewManoeuvreCode
		,target.WoodScrewOrderCode = source.WoodScrewOrderCode
		,target.AllFoursPosition = source.AllFoursPosition
		,target.AllFoursDT = source.AllFoursDT
		,target.AllFoursOrder = source.AllFoursOrder
		,target.AllFoursBy = source.AllFoursBy
		,target.AllFoursByCode = source.AllFoursByCode
		,target.AllFoursOrderCode = source.AllFoursOrderCode
		,target.AllFoursPositionCode = source.AllFoursPositionCode
		,target.OtherManoeuvres = source.OtherManoeuvres
		,target.OtherManoeuvresCode = source.OtherManoeuvresCode
		,target.AmbulanceDT = source.AmbulanceDT
		,target.AmbulanceArrivedDT = source.AmbulanceArrivedDT
		,target.ForcepsTypeCode = source.ForcepsTypeCode
		,target.GenderCode = source.GenderCode
		,target.HeadCircumferenceCode = source.HeadCircumferenceCode
		,target.InstrumentalDT = source.InstrumentalDT
		,target.InstrumentalProcedures = source.InstrumentalProcedures
		,target.InstrumentalProceduresCode = source.InstrumentalProceduresCode
		,target.DurationBirthToIntubationCode = source.DurationBirthToIntubationCode
		,target.ConsultantInvolved = source.ConsultantInvolved
		,target.ConsultantInvolvedCode = source.ConsultantInvolvedCode
		,target.FirstFeedDT = source.FirstFeedDT
		,target.RoMHowCode = source.RoMHowCode
		,target.ANFetalProbsNR = source.ANFetalProbsNR
		,target.ANFetalProbsNRCode = source.ANFetalProbsNRCode
		,target.CordVesselsCode = source.CordVesselsCode
		,target.FHMonitoringLabourCode = source.FHMonitoringLabourCode
		,target.BreechPosition = source.BreechPosition
		,target.CephalicPosition = source.CephalicPosition
		,target.PositionFace = source.PositionFace
		,target.BreechPositionCode = source.BreechPositionCode
		,target.CephalicPositionCode = source.CephalicPositionCode
		,target.CordRoundNeckCode = source.CordRoundNeckCode
		,target.PositionFaceCode = source.PositionFaceCode
		,target.FetusDeliveredBy = source.FetusDeliveredBy
		,target.FetusDeliveredByCode = source.FetusDeliveredByCode
		,target.Fed1HourCode = source.Fed1HourCode
		,target.BreechDiagnosedWhenCode = source.BreechDiagnosedWhenCode
		,target.ApexBeat = source.ApexBeat
		,target.ApexBeatCode = source.ApexBeatCode
		,target.TermReasonCode = source.TermReasonCode
		,target.WeightNR = source.WeightNR
		,target.BreastFeedingSupport = source.BreastFeedingSupport
		,target.BreastFeedingSupportCode = source.BreastFeedingSupportCode
		,target.SkinToSkinDuration = source.SkinToSkinDuration
		,target.SkinToSkinDurationCode = source.SkinToSkinDurationCode
		,target.MonitoringChangedInLabour = source.MonitoringChangedInLabour
		,target.MonitoringChangedInLabourCode = source.MonitoringChangedInLabourCode
		,target.GestationDiagCode = source.GestationDiagCode
		,target.TypeOfBreastMilk = source.TypeOfBreastMilk
		,target.TypeOfBreastMilkCode = source.TypeOfBreastMilkCode
		,target.TransferToNNUDT = source.TransferToNNUDT
		,target.BreechDiagnosisDT = source.BreechDiagnosisDT
		,target.TypeOfMLUBabyDelivered = source.TypeOfMLUBabyDelivered
		,target.TypeOfMLUBabyDeliveredCode = source.TypeOfMLUBabyDeliveredCode
		,target.DeliveryTechnique = source.DeliveryTechnique
		,target.DeliveryTechniqueCode = source.DeliveryTechniqueCode
		,target.VitaminKPaediatricReferral = source.VitaminKPaediatricReferral
		,target.VitaminKPaediatricReferralCode = source.VitaminKPaediatricReferralCode
		,target.BabyLabelsLocation = source.BabyLabelsLocation
		,target.ShoulderDystociaEmergencyCallDT = source.ShoulderDystociaEmergencyCallDT
		,target.BabyLabelsLocationCode = source.BabyLabelsLocationCode

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime