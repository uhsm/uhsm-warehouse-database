﻿CREATE procedure [E3].[LoadCurrentPregnancy] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			UserID
			,PatientID
			,HeightCode
			,BloodGroup
			,Height
			,AccomodationCode
			,AgenciesInvolvedCode
			,AlcoholAtBookingCode
			,AlcoholBeforePregCode
			,AppointmentsCode
			,BabiesOnScanCode
			,BabysFatherEthnicityCode
			,BenefitsInformationCode
			,BirthPlaceIntendedCode
			,BloodTakenBookingCode
			,BookingAfter12wksCode
			,BPBookingCode
			,BPCuffSizeCode
			,BroaderFamilyIssuesCode
			,CAFFormCompletedCode
			,CareTypeCode
			,ChlamydiaScreeningCode
			,ConsentBloodProductsCode
			,ContraceptionFailCode
			,EDDScanCode
			,FeelingDownDepressedCode
			,FeelingUniterestedCode
			,FirstContactCode
			,FirstContactDateCode
			,FolicAcidCode
			,GravidaCode
			,HaemoglobinopathyScreeningCode
			,HepatitisBScreeningCode
			,HIVScreeningCode
			,HospitalAttachedCode
			,InformationLeafletsCode
			,InterviewCode
			,InterviewerCode
			,InterviewSupervisedByCode
			,LargeBPCuffCode
			,LeadProfessionalCode
			,LivesWithCode
			,MedicationThisPregnancyCode
			,MinorDisordersCode
			,NamedMidwifeCode
			,ParityCode
			,PCTCode
			,PrevCSCode
			,QuittingSmokingCode
			,RecreationalDrugsCode
			,RecreationalDrugsBookingCode
			,ReferralsCode
			,RubellaScreeingCode
			,ScreeningTestASBCode
			,SmokedEverCode
			,SmokerBeforePregnancyCode
			,SmokerInHouseholdCode
			,SmokerInHouseholdReferralCode
			,SmokesAtBookingCode
			,SubstanceAlcoholActionCode
			,SupportRequestedCode
			,SyphilisScreeningCode
			,TeamCode
			,TenseOrStressedCode
			,ThromboembolicRiskCode
			,ThromboprophylaxisPrescribedCode
			,TreatmentCode
			,VBACCode
			,WeightAtBookingCode
			,WhereBookedCode
			,Accomodation
			,AgenciesInvolved
			,AlcoholAtBooking
			,AlcoholBeforePreg
			,Appointments
			,BabiesOnScan
			,BabysFatherEthnicity
			,BenefitsInformation
			,BirthPlaceIntended
			,BleedingEpisodes
			,BloodTakenBooking
			,BookingAfter12wks
			,BookingDate
			,BPBooking
			,BPCuffSize
			,BroaderFamilyIssues
			,CAFFormCompleted
			,CareType
			,CategoryRickSocial
			,CategoryRisk
			,CertainLMP
			,ChlamydiaScreening
			,ConsentBloodProducts
			,ContraceptionFail
			,CycleLength
			,DateCAFFormCompleted
			,Diet
			,EDDScan
			,FeelingDownDepressed
			,FeelingUniterested
			,FirstContact
			,FirstContactDate
			,FolicAcid
			,Gravida
			,HaemoglobinopathyScreening
			,HepatitisBScreening
			,HIVCouncelling
			,HIVScreening
			,HospitalAttached
			,InformationLeaflets
			,Interview
			,Interviewer
			,InterviewSupervisedBy
			,LargeBPCuff
			,LeadProfessional
			,LivesWith
			,LMP
			,MedicationThisPregnancy
			,MinorDisorders
			,NamedMidwife
			,Parity
			,PartnerBloodRel
			,PCT
			,PlannedPregnancy
			,PrevCS
			,QuittingSmoking
			,RecreationalDrugs
			,RecreationalDrugsBooking
			,Referrals
			,RubellaScreeing
			,ScreeningTestASB
			,SmokedEver
			,SmokerBeforePregnancy
			,SmokerInHousehold
			,SmokerInHouseholdReferral
			,SmokesAtBooking
			,SubstanceAlcoholAction
			,SupportRequested
			,SyphilisScreening
			,Team
			,TenseOrStressed
			,ThromboembolicRisk
			,ThromboprophylaxisPrescribed
			,Treatment
			,Urinalysis
			,VBAC
			,WeightAtBooking
			,WhereBooked
			,DietCode
			,SmokesNumber
			,DownsScreening
			,DownsScreeningCode
			,SmokesNumberCode
			,KetoneTestingStrips
			,KetoneTestingStripsCode
			,MonitorBloodGlucose
			,MonitorBloodGlucoseCode
			,SmokingCessation
			,SmokingCessationCode
			,SmokingCessationComments
			,SmokingCessationCommentsCode
			,COReading
			,CategoryRiskCode
			,COReadingCode
			,SeasonalFluVaccine
			,SeasonalFluVaccineCode
			,Comments
			,CommentsCode
			,PartnersEthnicity
			,PartnersEthnicityCode
			,LanguageLiteracy
			,LanguageLiteracyCode
			,LMPCode
			,EmploymentStatusMotherBooking
			,EmploymentStatusMotherBookingCode
			,EmploymentStatusPartnerBooking
			,BleedingEpisodesCode
			,EmploymentStatusPartnerBookingCode
			,BookedAtOtherHospital
			,BookedAtOtherHospitalCode
			,OtherCloseConcerns
			,OtherCloseConcernsCode
			,BookedAtOtherHospitalReason
			,BookedAtOtherHospitalReasonCode
			,DiscussedAtBooking
			,DiscussedAtBookingCode
			,AnomalyScanOfferedAtBooking
			,FirstLanguage
			,AnomalyScanOfferedAtBookingCode
			,FirstLanguageCode
			,DatingScanDiscussed
			,DatingScanDiscussedCode
			,DatingScanPerformedDate
			,DatingScanAnomalyDiagnosed
			,DatingScanAnomalyDiagnosedCode
			,InterpreterRequired
			,InterpreterRequiredCode
			,AnomalyScanResult
			,AnomalyScanResultCode
			,AnomalyScanResultFetus2
			,AnomalyScanDT
			,AnomalyScanResultFetus3
			,AnomalyScanResultFetus4
			,AnomalyScanResultFetus5
			,AnomalyScanResultFetus6
			,AnomalyScanResultFetus2Code
			,AnomalyScanResultFetus3Code
			,AnomalyScanResultFetus4Code
			,AnomalyScanResultFetus5Code
			,AnomalyScanResultFetus6Code
			,MDSConsent
			,DepressionIndicator3
			,DepressionIndicator3Code
			,DepressionIndicator4
			,DepressionIndicator4Code
			,PreCAFForm
			,PreCAFFormCode
			,VTEConsultantReferral
			,MDSConsentCode
			,VTEConsultantReferralCode
			,MentalHealthReferral
			,MentalHealthReferralCode
			,DownsTestType
			,DownsTestTypeCode
			,DownsTestDate
			,DownsScreenResult
			,DownsScreenResultCode
			,DownsScreeningRiskRatioResult
			,DownsScreeningRiskRatioResultCode
			,JointScreeningTestTrisomy13and18
			,JointScreeningTestTrisomy13and18Code
			,JointScreeningTestResult
			,JointScreeningTestResultCode
			,JointScreeningTestRiskRatio
			,JointScreeningTestRiskRatioCode
			,ECigaretteUse
			)
into
	#TLoadE3CurrentPregnancy
from
	(
	select
		UserID = cast(UserID as int)
		,PatientID = cast(PatientID as bigint)
		,PregnancyID = cast(PregnancyID as bigint)
		,HeightCode = cast(HeightCode as bigint)
		,BloodGroup = cast(nullif(BloodGroup, '') as nvarchar(2000))
		,Height = cast(nullif(Height, '') as nvarchar(2000))
		,AccomodationCode = cast(AccomodationCode as bigint)
		,AgenciesInvolvedCode = cast(AgenciesInvolvedCode as bigint)
		,AlcoholAtBookingCode = cast(AlcoholAtBookingCode as bigint)
		,AlcoholBeforePregCode = cast(AlcoholBeforePregCode as bigint)
		,AppointmentsCode = cast(AppointmentsCode as bigint)
		,BabiesOnScanCode = cast(BabiesOnScanCode as bigint)
		,BabysFatherEthnicityCode = cast(BabysFatherEthnicityCode as bigint)
		,BenefitsInformationCode = cast(BenefitsInformationCode as bigint)
		,BirthPlaceIntendedCode = cast(BirthPlaceIntendedCode as bigint)
		,BloodTakenBookingCode = cast(BloodTakenBookingCode as bigint)
		,BookingAfter12wksCode = cast(BookingAfter12wksCode as bigint)
		,BPBookingCode = cast(BPBookingCode as bigint)
		,BPCuffSizeCode = cast(BPCuffSizeCode as bigint)
		,BroaderFamilyIssuesCode = cast(BroaderFamilyIssuesCode as bigint)
		,CAFFormCompletedCode = cast(CAFFormCompletedCode as bigint)
		,CareTypeCode = cast(CareTypeCode as bigint)
		,ChlamydiaScreeningCode = cast(ChlamydiaScreeningCode as bigint)
		,ConsentBloodProductsCode = cast(ConsentBloodProductsCode as bigint)
		,ContraceptionFailCode = cast(ContraceptionFailCode as bigint)
		,EDDScanCode = cast(EDDScanCode as bigint)
		,FeelingDownDepressedCode = cast(FeelingDownDepressedCode as bigint)
		,FeelingUniterestedCode = cast(FeelingUniterestedCode as bigint)
		,FirstContactCode = cast(FirstContactCode as bigint)
		,FirstContactDateCode = cast(FirstContactDateCode as bigint)
		,FolicAcidCode = cast(FolicAcidCode as bigint)
		,GravidaCode = cast(GravidaCode as bigint)
		,HaemoglobinopathyScreeningCode = cast(HaemoglobinopathyScreeningCode as bigint)
		,HepatitisBScreeningCode = cast(HepatitisBScreeningCode as bigint)
		,HIVScreeningCode = cast(HIVScreeningCode as bigint)
		,HospitalAttachedCode = cast(HospitalAttachedCode as bigint)
		,InformationLeafletsCode = cast(InformationLeafletsCode as bigint)
		,InterviewCode = cast(InterviewCode as bigint)
		,InterviewerCode = cast(InterviewerCode as bigint)
		,InterviewSupervisedByCode = cast(InterviewSupervisedByCode as bigint)
		,LargeBPCuffCode = cast(LargeBPCuffCode as bigint)
		,LeadProfessionalCode = cast(LeadProfessionalCode as bigint)
		,LivesWithCode = cast(LivesWithCode as bigint)
		,MedicationThisPregnancyCode = cast(MedicationThisPregnancyCode as bigint)
		,MinorDisordersCode = cast(MinorDisordersCode as bigint)
		,NamedMidwifeCode = cast(NamedMidwifeCode as bigint)
		,ParityCode = cast(ParityCode as bigint)
		,PCTCode = cast(PCTCode as bigint)
		,PrevCSCode = cast(PrevCSCode as bigint)
		,QuittingSmokingCode = cast(QuittingSmokingCode as bigint)
		,RecreationalDrugsCode = cast(RecreationalDrugsCode as bigint)
		,RecreationalDrugsBookingCode = cast(RecreationalDrugsBookingCode as bigint)
		,ReferralsCode = cast(ReferralsCode as bigint)
		,RubellaScreeingCode = cast(RubellaScreeingCode as bigint)
		,ScreeningTestASBCode = cast(ScreeningTestASBCode as bigint)
		,SmokedEverCode = cast(SmokedEverCode as bigint)
		,SmokerBeforePregnancyCode = cast(SmokerBeforePregnancyCode as bigint)
		,SmokerInHouseholdCode = cast(SmokerInHouseholdCode as bigint)
		,SmokerInHouseholdReferralCode = cast(SmokerInHouseholdReferralCode as bigint)
		,SmokesAtBookingCode = cast(SmokesAtBookingCode as bigint)
		,SubstanceAlcoholActionCode = cast(SubstanceAlcoholActionCode as bigint)
		,SupportRequestedCode = cast(SupportRequestedCode as bigint)
		,SyphilisScreeningCode = cast(SyphilisScreeningCode as bigint)
		,TeamCode = cast(TeamCode as bigint)
		,TenseOrStressedCode = cast(TenseOrStressedCode as bigint)
		,ThromboembolicRiskCode = cast(ThromboembolicRiskCode as bigint)
		,ThromboprophylaxisPrescribedCode = cast(ThromboprophylaxisPrescribedCode as bigint)
		,TreatmentCode = cast(TreatmentCode as bigint)
		,VBACCode = cast(VBACCode as bigint)
		,WeightAtBookingCode = cast(WeightAtBookingCode as bigint)
		,WhereBookedCode = cast(WhereBookedCode as bigint)
		,Accomodation = cast(nullif(Accomodation, '') as nvarchar(2000))
		,AgenciesInvolved = cast(nullif(AgenciesInvolved, '') as nvarchar(2000))
		,AlcoholAtBooking = cast(nullif(AlcoholAtBooking, '') as nvarchar(2000))
		,AlcoholBeforePreg = cast(nullif(AlcoholBeforePreg, '') as nvarchar(2000))
		,Appointments = cast(nullif(Appointments, '') as nvarchar(2000))
		,BabiesOnScan = cast(nullif(BabiesOnScan, '') as nvarchar(2000))
		,BabysFatherEthnicity = cast(nullif(BabysFatherEthnicity, '') as nvarchar(2000))
		,BenefitsInformation = cast(nullif(BenefitsInformation, '') as nvarchar(2000))
		,BirthPlaceIntended = cast(nullif(BirthPlaceIntended, '') as nvarchar(2000))
		,BleedingEpisodes = cast(nullif(BleedingEpisodes, '') as nvarchar(2000))
		,BloodTakenBooking = cast(nullif(BloodTakenBooking, '') as nvarchar(2000))
		,BookingAfter12wks = cast(nullif(BookingAfter12wks, '') as nvarchar(2000))
		,BookingDate = cast(nullif(BookingDate, '') as nvarchar(2000))
		,BPBooking = cast(nullif(BPBooking, '') as nvarchar(2000))
		,BPCuffSize = cast(nullif(BPCuffSize, '') as nvarchar(2000))
		,BroaderFamilyIssues = cast(nullif(BroaderFamilyIssues, '') as nvarchar(2000))
		,CAFFormCompleted = cast(nullif(CAFFormCompleted, '') as nvarchar(2000))
		,CareType = cast(nullif(CareType, '') as nvarchar(2000))
		,CategoryRickSocial = cast(nullif(CategoryRickSocial, '') as nvarchar(2000))
		,CategoryRisk = cast(nullif(CategoryRisk, '') as nvarchar(2000))
		,CertainLMP = cast(nullif(CertainLMP, '') as nvarchar(2000))
		,ChlamydiaScreening = cast(nullif(ChlamydiaScreening, '') as nvarchar(2000))
		,ConsentBloodProducts = cast(nullif(ConsentBloodProducts, '') as nvarchar(2000))
		,ContraceptionFail = cast(nullif(ContraceptionFail, '') as nvarchar(2000))
		,CycleLength = cast(nullif(CycleLength, '') as nvarchar(2000))
		,DateCAFFormCompleted = cast(nullif(DateCAFFormCompleted, '') as nvarchar(2000))
		,Diet = cast(nullif(Diet, '') as nvarchar(2000))
		,EDDScan = cast(nullif(EDDScan, '') as nvarchar(2000))
		,FeelingDownDepressed = cast(nullif(FeelingDownDepressed, '') as nvarchar(2000))
		,FeelingUniterested = cast(nullif(FeelingUniterested, '') as nvarchar(2000))
		,FirstContact = cast(nullif(FirstContact, '') as nvarchar(2000))
		,FirstContactDate = cast(nullif(FirstContactDate, '') as nvarchar(2000))
		,FolicAcid = cast(nullif(FolicAcid, '') as nvarchar(2000))
		,Gravida = cast(nullif(Gravida, '') as nvarchar(2000))
		,HaemoglobinopathyScreening = cast(nullif(HaemoglobinopathyScreening, '') as nvarchar(2000))
		,HepatitisBScreening = cast(nullif(HepatitisBScreening, '') as nvarchar(2000))
		,HIVCouncelling = cast(nullif(HIVCouncelling, '') as nvarchar(2000))
		,HIVScreening = cast(nullif(HIVScreening, '') as nvarchar(2000))
		,HospitalAttached = cast(nullif(HospitalAttached, '') as nvarchar(2000))
		,InformationLeaflets = cast(nullif(InformationLeaflets, '') as nvarchar(2000))
		,Interview = cast(nullif(Interview, '') as nvarchar(2000))
		,Interviewer = cast(nullif(Interviewer, '') as nvarchar(2000))
		,InterviewSupervisedBy = cast(nullif(InterviewSupervisedBy, '') as nvarchar(2000))
		,LargeBPCuff = cast(nullif(LargeBPCuff, '') as nvarchar(2000))
		,LeadProfessional = cast(nullif(LeadProfessional, '') as nvarchar(2000))
		,LivesWith = cast(nullif(LivesWith, '') as nvarchar(2000))
		,LMP = cast(nullif(LMP, '') as nvarchar(2000))
		,MedicationThisPregnancy = cast(nullif(MedicationThisPregnancy, '') as nvarchar(2000))
		,MinorDisorders = cast(nullif(MinorDisorders, '') as nvarchar(2000))
		,NamedMidwife = cast(nullif(NamedMidwife, '') as nvarchar(2000))
		,Parity = cast(nullif(Parity, '') as nvarchar(2000))
		,PartnerBloodRel = cast(nullif(PartnerBloodRel, '') as nvarchar(2000))
		,PCT = cast(nullif(PCT, '') as nvarchar(2000))
		,PlannedPregnancy = cast(nullif(PlannedPregnancy, '') as nvarchar(2000))
		,PrevCS = cast(nullif(PrevCS, '') as nvarchar(2000))
		,QuittingSmoking = cast(nullif(QuittingSmoking, '') as nvarchar(2000))
		,RecreationalDrugs = cast(nullif(RecreationalDrugs, '') as nvarchar(2000))
		,RecreationalDrugsBooking = cast(nullif(RecreationalDrugsBooking, '') as nvarchar(2000))
		,Referrals = cast(nullif(Referrals, '') as nvarchar(2000))
		,RubellaScreeing = cast(nullif(RubellaScreeing, '') as nvarchar(2000))
		,ScreeningTestASB = cast(nullif(ScreeningTestASB, '') as nvarchar(2000))
		,SmokedEver = cast(nullif(SmokedEver, '') as nvarchar(2000))
		,SmokerBeforePregnancy = cast(nullif(SmokerBeforePregnancy, '') as nvarchar(2000))
		,SmokerInHousehold = cast(nullif(SmokerInHousehold, '') as nvarchar(2000))
		,SmokerInHouseholdReferral = cast(nullif(SmokerInHouseholdReferral, '') as nvarchar(2000))
		,SmokesAtBooking = cast(nullif(SmokesAtBooking, '') as nvarchar(2000))
		,SubstanceAlcoholAction = cast(nullif(SubstanceAlcoholAction, '') as nvarchar(2000))
		,SupportRequested = cast(nullif(SupportRequested, '') as nvarchar(2000))
		,SyphilisScreening = cast(nullif(SyphilisScreening, '') as nvarchar(2000))
		,Team = cast(nullif(Team, '') as nvarchar(2000))
		,TenseOrStressed = cast(nullif(TenseOrStressed, '') as nvarchar(2000))
		,ThromboembolicRisk = cast(nullif(ThromboembolicRisk, '') as nvarchar(2000))
		,ThromboprophylaxisPrescribed = cast(nullif(ThromboprophylaxisPrescribed, '') as nvarchar(2000))
		,Treatment = cast(nullif(Treatment, '') as nvarchar(2000))
		,Urinalysis = cast(nullif(Urinalysis, '') as nvarchar(2000))
		,VBAC = cast(nullif(VBAC, '') as nvarchar(2000))
		,WeightAtBooking = cast(nullif(WeightAtBooking, '') as nvarchar(2000))
		,WhereBooked = cast(nullif(WhereBooked, '') as nvarchar(2000))
		,DietCode = cast(DietCode as bigint)
		,SmokesNumber = cast(nullif(SmokesNumber, '') as nvarchar(2000))
		,DownsScreening = cast(nullif(DownsScreening, '') as nvarchar(2000))
		,DownsScreeningCode = cast(DownsScreeningCode as bigint)
		,SmokesNumberCode = cast(SmokesNumberCode as bigint)
		,KetoneTestingStrips = cast(nullif(KetoneTestingStrips, '') as nvarchar(2000))
		,KetoneTestingStripsCode = cast(KetoneTestingStripsCode as bigint)
		,MonitorBloodGlucose = cast(nullif(MonitorBloodGlucose, '') as nvarchar(2000))
		,MonitorBloodGlucoseCode = cast(MonitorBloodGlucoseCode as bigint)
		,SmokingCessation = cast(nullif(SmokingCessation, '') as nvarchar(2000))
		,SmokingCessationCode = cast(SmokingCessationCode as bigint)
		,SmokingCessationComments = cast(nullif(SmokingCessationComments, '') as nvarchar(2000))
		,SmokingCessationCommentsCode = cast(SmokingCessationCommentsCode as bigint)
		,COReading = cast(nullif(COReading, '') as nvarchar(2000))
		,CategoryRiskCode = cast(CategoryRiskCode as bigint)
		,COReadingCode = cast(COReadingCode as bigint)
		,SeasonalFluVaccine = cast(nullif(SeasonalFluVaccine, '') as nvarchar(2000))
		,SeasonalFluVaccineCode = cast(SeasonalFluVaccineCode as bigint)
		,Comments = cast(nullif(Comments, '') as nvarchar(2000))
		,CommentsCode = cast(CommentsCode as bigint)
		,PartnersEthnicity = cast(nullif(PartnersEthnicity, '') as nvarchar(2000))
		,PartnersEthnicityCode = cast(PartnersEthnicityCode as bigint)
		,LanguageLiteracy = cast(nullif(LanguageLiteracy, '') as nvarchar(2000))
		,LanguageLiteracyCode = cast(LanguageLiteracyCode as bigint)
		,LMPCode = cast(LMPCode as bigint)
		,EmploymentStatusMotherBooking = cast(nullif(EmploymentStatusMotherBooking, '') as nvarchar(2000))
		,EmploymentStatusMotherBookingCode = cast(EmploymentStatusMotherBookingCode as bigint)
		,EmploymentStatusPartnerBooking = cast(nullif(EmploymentStatusPartnerBooking, '') as nvarchar(2000))
		,BleedingEpisodesCode = cast(BleedingEpisodesCode as bigint)
		,EmploymentStatusPartnerBookingCode = cast(EmploymentStatusPartnerBookingCode as bigint)
		,BookedAtOtherHospital = cast(nullif(BookedAtOtherHospital, '') as nvarchar(2000))
		,BookedAtOtherHospitalCode = cast(BookedAtOtherHospitalCode as bigint)
		,OtherCloseConcerns = cast(nullif(OtherCloseConcerns, '') as nvarchar(2000))
		,OtherCloseConcernsCode = cast(OtherCloseConcernsCode as bigint)
		,BookedAtOtherHospitalReason = cast(nullif(BookedAtOtherHospitalReason, '') as nvarchar(2000))
		,BookedAtOtherHospitalReasonCode = cast(BookedAtOtherHospitalReasonCode as bigint)
		,DiscussedAtBooking = cast(nullif(DiscussedAtBooking, '') as nvarchar(2000))
		,DiscussedAtBookingCode = cast(DiscussedAtBookingCode as bigint)
		,AnomalyScanOfferedAtBooking = cast(nullif(AnomalyScanOfferedAtBooking, '') as nvarchar(2000))
		,FirstLanguage = cast(nullif(FirstLanguage, '') as nvarchar(2000))
		,AnomalyScanOfferedAtBookingCode = cast(AnomalyScanOfferedAtBookingCode as bigint)
		,FirstLanguageCode = cast(FirstLanguageCode as bigint)
		,DatingScanDiscussed = cast(nullif(DatingScanDiscussed, '') as nvarchar(2000))
		,DatingScanDiscussedCode = cast(DatingScanDiscussedCode as bigint)
		,DatingScanPerformedDate = cast(nullif(DatingScanPerformedDate, '') as nvarchar(2000))
		,DatingScanAnomalyDiagnosed = cast(nullif(DatingScanAnomalyDiagnosed, '') as nvarchar(2000))
		,DatingScanAnomalyDiagnosedCode = cast(DatingScanAnomalyDiagnosedCode as bigint)
		,InterpreterRequired = cast(nullif(InterpreterRequired, '') as nvarchar(2000))
		,InterpreterRequiredCode = cast(InterpreterRequiredCode as bigint)
		,AnomalyScanResult = cast(nullif(AnomalyScanResult, '') as nvarchar(2000))
		,AnomalyScanResultCode = cast(AnomalyScanResultCode as bigint)
		,AnomalyScanResultFetus2 = cast(nullif(AnomalyScanResultFetus2, '') as nvarchar(2000))
		,AnomalyScanDT = cast(nullif(AnomalyScanDT, '') as nvarchar(2000))
		,AnomalyScanResultFetus3 = cast(nullif(AnomalyScanResultFetus3, '') as nvarchar(2000))
		,AnomalyScanResultFetus4 = cast(nullif(AnomalyScanResultFetus4, '') as nvarchar(2000))
		,AnomalyScanResultFetus5 = cast(nullif(AnomalyScanResultFetus5, '') as nvarchar(2000))
		,AnomalyScanResultFetus6 = cast(nullif(AnomalyScanResultFetus6, '') as nvarchar(2000))
		,AnomalyScanResultFetus2Code = cast(AnomalyScanResultFetus2Code as bigint)
		,AnomalyScanResultFetus3Code = cast(AnomalyScanResultFetus3Code as bigint)
		,AnomalyScanResultFetus4Code = cast(AnomalyScanResultFetus4Code as bigint)
		,AnomalyScanResultFetus5Code = cast(AnomalyScanResultFetus5Code as bigint)
		,AnomalyScanResultFetus6Code = cast(AnomalyScanResultFetus6Code as bigint)
		,MDSConsent = cast(nullif(MDSConsent, '') as nvarchar(2000))
		,DepressionIndicator3 = cast(nullif(DepressionIndicator3, '') as nvarchar(2000))
		,DepressionIndicator3Code = cast(DepressionIndicator3Code as bigint)
		,DepressionIndicator4 = cast(nullif(DepressionIndicator4, '') as nvarchar(2000))
		,DepressionIndicator4Code = cast(DepressionIndicator4Code as bigint)
		,PreCAFForm = cast(nullif(PreCAFForm, '') as nvarchar(2000))
		,PreCAFFormCode = cast(PreCAFFormCode as bigint)
		,VTEConsultantReferral = cast(nullif(VTEConsultantReferral, '') as nvarchar(2000))
		,MDSConsentCode = cast(MDSConsentCode as bigint)
		,VTEConsultantReferralCode = cast(VTEConsultantReferralCode as bigint)
		,MentalHealthReferral = cast(nullif(MentalHealthReferral, '') as nvarchar(2000))
		,MentalHealthReferralCode = cast(MentalHealthReferralCode as bigint)
		,DownsTestType = cast(nullif(DownsTestType, '') as nvarchar(2000))
		,DownsTestTypeCode = cast(DownsTestTypeCode as bigint)
		,DownsTestDate = cast(nullif(DownsTestDate, '') as nvarchar(2000))
		,DownsScreenResult = cast(nullif(DownsScreenResult, '') as nvarchar(2000))
		,DownsScreenResultCode = cast(DownsScreenResultCode as bigint)
		,DownsScreeningRiskRatioResult = cast(nullif(DownsScreeningRiskRatioResult, '') as nvarchar(2000))
		,DownsScreeningRiskRatioResultCode = cast(DownsScreeningRiskRatioResultCode as bigint)
		,JointScreeningTestTrisomy13and18 = cast(nullif(JointScreeningTestTrisomy13and18, '') as nvarchar(2000))
		,JointScreeningTestTrisomy13and18Code = cast(JointScreeningTestTrisomy13and18Code as bigint)
		,JointScreeningTestResult = cast(nullif(JointScreeningTestResult, '') as nvarchar(2000))
		,JointScreeningTestResultCode = cast(JointScreeningTestResultCode as bigint)
		,JointScreeningTestRiskRatio = cast(nullif(JointScreeningTestRiskRatio, '') as nvarchar(2000))
		,JointScreeningTestRiskRatioCode = cast(JointScreeningTestRiskRatioCode as bigint)
		,ECigaretteUse = cast(nullif(ECigaretteUse, '') as nvarchar(2000))
	from
		(
		select
			UserID = CurrentPregnancy.UserID
			,PatientID = CurrentPregnancy.PatientID
			,PregnancyID = CurrentPregnancy.PregnancyID
			,HeightCode = CurrentPregnancy.Height_Value
			,BloodGroup = CurrentPregnancy.BloodGroup
			,Height = CurrentPregnancy.Height
			,AccomodationCode = CurrentPregnancy.Accomodation_Value
			,AgenciesInvolvedCode = CurrentPregnancy.AgenciesInvolved_Value
			,AlcoholAtBookingCode = CurrentPregnancy.AlcoholAtBooking_Value
			,AlcoholBeforePregCode = CurrentPregnancy.AlcoholBeforePreg_Value
			,AppointmentsCode = CurrentPregnancy.Appointments_Value
			,BabiesOnScanCode = CurrentPregnancy.BabiesOnScan_Value
			,BabysFatherEthnicityCode = CurrentPregnancy.BabysFatherEthnicity_Value
			,BenefitsInformationCode = CurrentPregnancy.BenefitsInformation_Value
			,BirthPlaceIntendedCode = CurrentPregnancy.BirthPlaceIntended_Value
			,BloodTakenBookingCode = CurrentPregnancy.BloodTakenBooking_Value
			,BookingAfter12wksCode = CurrentPregnancy.BookingAfter12wks_Value
			,BPBookingCode = CurrentPregnancy.BPBooking_Value
			,BPCuffSizeCode = CurrentPregnancy.BPCuffSize_Value
			,BroaderFamilyIssuesCode = CurrentPregnancy.BroaderFamilyIssues_Value
			,CAFFormCompletedCode = CurrentPregnancy.CAFFormCompleted_Value
			,CareTypeCode = CurrentPregnancy.CareType_Value
			,ChlamydiaScreeningCode = CurrentPregnancy.ChlamydiaScreening_Value
			,ConsentBloodProductsCode = CurrentPregnancy.ConsentBloodProducts_Value
			,ContraceptionFailCode = CurrentPregnancy.ContraceptionFail_Value
			,EDDScanCode = CurrentPregnancy.EDDScan_Value
			,FeelingDownDepressedCode = CurrentPregnancy.FeelingDownDepressed_Value
			,FeelingUniterestedCode = CurrentPregnancy.FeelingUniterested_Value
			,FirstContactCode = CurrentPregnancy.FirstContact_Value
			,FirstContactDateCode = CurrentPregnancy.FirstContactDate_Value
			,FolicAcidCode = CurrentPregnancy.FolicAcid_Value
			,GravidaCode = CurrentPregnancy.Gravida_Value
			,HaemoglobinopathyScreeningCode = CurrentPregnancy.HaemoglobinopathyScreening_Value
			,HepatitisBScreeningCode = CurrentPregnancy.HepatitisBScreening_Value
			,HIVScreeningCode = CurrentPregnancy.HIVScreening_Value
			,HospitalAttachedCode = CurrentPregnancy.HospitalAttached_Value
			,InformationLeafletsCode = CurrentPregnancy.InformationLeaflets_Value
			,InterviewCode = CurrentPregnancy.Interview_Value
			,InterviewerCode = CurrentPregnancy.Interviewer_Value
			,InterviewSupervisedByCode = CurrentPregnancy.InterviewSupervisedBy_Value
			,LargeBPCuffCode = CurrentPregnancy.LargeBPCuff_Value
			,LeadProfessionalCode = CurrentPregnancy.LeadProfessional_Value
			,LivesWithCode = CurrentPregnancy.LivesWith_Value
			,MedicationThisPregnancyCode = CurrentPregnancy.MedicationThisPregnancy_Value
			,MinorDisordersCode = CurrentPregnancy.MinorDisorders_Value
			,NamedMidwifeCode = CurrentPregnancy.NamedMidwife_Value
			,ParityCode = CurrentPregnancy.Parity_Value
			,PCTCode = CurrentPregnancy.PCT_Value
			,PrevCSCode = CurrentPregnancy.PrevCS_Value
			,QuittingSmokingCode = CurrentPregnancy.QuittingSmoking_Value
			,RecreationalDrugsCode = CurrentPregnancy.RecreationalDrugs_Value
			,RecreationalDrugsBookingCode = CurrentPregnancy.RecreationalDrugsBooking_Value
			,ReferralsCode = CurrentPregnancy.Referrals_Value
			,RubellaScreeingCode = CurrentPregnancy.RubellaScreeing_Value
			,ScreeningTestASBCode = CurrentPregnancy.ScreeningTestASB_Value
			,SmokedEverCode = CurrentPregnancy.SmokedEver_Value
			,SmokerBeforePregnancyCode = CurrentPregnancy.SmokerBeforePregnancy_Value
			,SmokerInHouseholdCode = CurrentPregnancy.SmokerInHousehold_Value
			,SmokerInHouseholdReferralCode = CurrentPregnancy.SmokerInHouseholdReferral_Value
			,SmokesAtBookingCode = CurrentPregnancy.SmokesAtBooking_Value
			,SubstanceAlcoholActionCode = CurrentPregnancy.SubstanceAlcoholAction_Value
			,SupportRequestedCode = CurrentPregnancy.SupportRequested_Value
			,SyphilisScreeningCode = CurrentPregnancy.SyphilisScreening_Value
			,TeamCode = CurrentPregnancy.Team_Value
			,TenseOrStressedCode = CurrentPregnancy.TenseOrStressed_Value
			,ThromboembolicRiskCode = CurrentPregnancy.ThromboembolicRisk_Value
			,ThromboprophylaxisPrescribedCode = CurrentPregnancy.ThromboprophylaxisPrescribed_Value
			,TreatmentCode = CurrentPregnancy.Treatment_Value
			,VBACCode = CurrentPregnancy.VBAC_Value
			,WeightAtBookingCode = CurrentPregnancy.WeightAtBooking_Value
			,WhereBookedCode = CurrentPregnancy.WhereBooked_Value
			,Accomodation = CurrentPregnancy.Accomodation
			,AgenciesInvolved = CurrentPregnancy.AgenciesInvolved
			,AlcoholAtBooking = CurrentPregnancy.AlcoholAtBooking
			,AlcoholBeforePreg = CurrentPregnancy.AlcoholBeforePreg
			,Appointments = CurrentPregnancy.Appointments
			,BabiesOnScan = CurrentPregnancy.BabiesOnScan
			,BabysFatherEthnicity = CurrentPregnancy.BabysFatherEthnicity
			,BenefitsInformation = CurrentPregnancy.BenefitsInformation
			,BirthPlaceIntended = CurrentPregnancy.BirthPlaceIntended
			,BleedingEpisodes = CurrentPregnancy.BleedingEpisodes
			,BloodTakenBooking = CurrentPregnancy.BloodTakenBooking
			,BookingAfter12wks = CurrentPregnancy.BookingAfter12wks
			,BookingDate = CurrentPregnancy.BookingDate
			,BPBooking = CurrentPregnancy.BPBooking
			,BPCuffSize = CurrentPregnancy.BPCuffSize
			,BroaderFamilyIssues = CurrentPregnancy.BroaderFamilyIssues
			,CAFFormCompleted = CurrentPregnancy.CAFFormCompleted
			,CareType = CurrentPregnancy.CareType
			,CategoryRickSocial = CurrentPregnancy.CategoryRickSocial
			,CategoryRisk = CurrentPregnancy.CategoryRisk
			,CertainLMP = CurrentPregnancy.CertainLMP
			,ChlamydiaScreening = CurrentPregnancy.ChlamydiaScreening
			,ConsentBloodProducts = CurrentPregnancy.ConsentBloodProducts
			,ContraceptionFail = CurrentPregnancy.ContraceptionFail
			,CycleLength = CurrentPregnancy.CycleLength
			,DateCAFFormCompleted = CurrentPregnancy.DateCAFFormCompleted
			,Diet = CurrentPregnancy.Diet
			,EDDScan = CurrentPregnancy.EDDScan
			,FeelingDownDepressed = CurrentPregnancy.FeelingDownDepressed
			,FeelingUniterested = CurrentPregnancy.FeelingUniterested
			,FirstContact = CurrentPregnancy.FirstContact
			,FirstContactDate = CurrentPregnancy.FirstContactDate
			,FolicAcid = CurrentPregnancy.FolicAcid
			,Gravida = CurrentPregnancy.Gravida
			,HaemoglobinopathyScreening = CurrentPregnancy.HaemoglobinopathyScreening
			,HepatitisBScreening = CurrentPregnancy.HepatitisBScreening
			,HIVCouncelling = CurrentPregnancy.HIVCouncelling
			,HIVScreening = CurrentPregnancy.HIVScreening
			,HospitalAttached = CurrentPregnancy.HospitalAttached
			,InformationLeaflets = CurrentPregnancy.InformationLeaflets
			,Interview = CurrentPregnancy.Interview
			,Interviewer = CurrentPregnancy.Interviewer
			,InterviewSupervisedBy = CurrentPregnancy.InterviewSupervisedBy
			,LargeBPCuff = CurrentPregnancy.LargeBPCuff
			,LeadProfessional = CurrentPregnancy.LeadProfessional
			,LivesWith = CurrentPregnancy.LivesWith
			,LMP = CurrentPregnancy.LMP
			,MedicationThisPregnancy = CurrentPregnancy.MedicationThisPregnancy
			,MinorDisorders = CurrentPregnancy.MinorDisorders
			,NamedMidwife = CurrentPregnancy.NamedMidwife
			,Parity = CurrentPregnancy.Parity
			,PartnerBloodRel = CurrentPregnancy.PartnerBloodRel
			,PCT = CurrentPregnancy.PCT
			,PlannedPregnancy = CurrentPregnancy.PlannedPregnancy
			,PrevCS = CurrentPregnancy.PrevCS
			,QuittingSmoking = CurrentPregnancy.QuittingSmoking
			,RecreationalDrugs = CurrentPregnancy.RecreationalDrugs
			,RecreationalDrugsBooking = CurrentPregnancy.RecreationalDrugsBooking
			,Referrals = CurrentPregnancy.Referrals
			,RubellaScreeing = CurrentPregnancy.RubellaScreeing
			,ScreeningTestASB = CurrentPregnancy.ScreeningTestASB
			,SmokedEver = CurrentPregnancy.SmokedEver
			,SmokerBeforePregnancy = CurrentPregnancy.SmokerBeforePregnancy
			,SmokerInHousehold = CurrentPregnancy.SmokerInHousehold
			,SmokerInHouseholdReferral = CurrentPregnancy.SmokerInHouseholdReferral
			,SmokesAtBooking = CurrentPregnancy.SmokesAtBooking
			,SubstanceAlcoholAction = CurrentPregnancy.SubstanceAlcoholAction
			,SupportRequested = CurrentPregnancy.SupportRequested
			,SyphilisScreening = CurrentPregnancy.SyphilisScreening
			,Team = CurrentPregnancy.Team
			,TenseOrStressed = CurrentPregnancy.TenseOrStressed
			,ThromboembolicRisk = CurrentPregnancy.ThromboembolicRisk
			,ThromboprophylaxisPrescribed = CurrentPregnancy.ThromboprophylaxisPrescribed
			,Treatment = CurrentPregnancy.Treatment
			,Urinalysis = CurrentPregnancy.Urinalysis
			,VBAC = CurrentPregnancy.VBAC
			,WeightAtBooking = CurrentPregnancy.WeightAtBooking
			,WhereBooked = CurrentPregnancy.WhereBooked
			,DietCode = CurrentPregnancy.Diet_Value
			,SmokesNumber = CurrentPregnancy.SmokesNumber
			,DownsScreening = CurrentPregnancy.DownsScreening
			,DownsScreeningCode = CurrentPregnancy.DownsScreening_Value
			,SmokesNumberCode = CurrentPregnancy.SmokesNumber_Value
			,KetoneTestingStrips = CurrentPregnancy.KetoneTestingStrips
			,KetoneTestingStripsCode = CurrentPregnancy.KetoneTestingStrips_Value
			,MonitorBloodGlucose = CurrentPregnancy.MonitorBloodGlucose
			,MonitorBloodGlucoseCode = CurrentPregnancy.MonitorBloodGlucose_Value
			,SmokingCessation = CurrentPregnancy.SmokingCessation
			,SmokingCessationCode = CurrentPregnancy.SmokingCessation_Value
			,SmokingCessationComments = CurrentPregnancy.SmokingCessationComments
			,SmokingCessationCommentsCode = CurrentPregnancy.SmokingCessationComments_Value
			,COReading = CurrentPregnancy.COReading
			,CategoryRiskCode = CurrentPregnancy.CategoryRisk_Value
			,COReadingCode = CurrentPregnancy.COReading_Value
			,SeasonalFluVaccine = CurrentPregnancy.SeasonalFluVaccine
			,SeasonalFluVaccineCode = CurrentPregnancy.SeasonalFluVaccine_Value
			,Comments = CurrentPregnancy.Comments
			,CommentsCode = CurrentPregnancy.Comments_Value
			,PartnersEthnicity = CurrentPregnancy.PartnersEthnicity
			,PartnersEthnicityCode = CurrentPregnancy.PartnersEthnicity_Value
			,LanguageLiteracy = CurrentPregnancy.LanguageLiteracy
			,LanguageLiteracyCode = CurrentPregnancy.LanguageLiteracy_Value
			,LMPCode = CurrentPregnancy.LMP_Value
			,EmploymentStatusMotherBooking = CurrentPregnancy.EmploymentStatusMotherBooking
			,EmploymentStatusMotherBookingCode = CurrentPregnancy.EmploymentStatusMotherBooking_Value
			,EmploymentStatusPartnerBooking = CurrentPregnancy.EmploymentStatusPartnerBooking
			,BleedingEpisodesCode = CurrentPregnancy.BleedingEpisodes_Value
			,EmploymentStatusPartnerBookingCode = CurrentPregnancy.EmploymentStatusPartnerBooking_Value
			,BookedAtOtherHospital = CurrentPregnancy.BookedAtOtherHospital
			,BookedAtOtherHospitalCode = CurrentPregnancy.BookedAtOtherHospital_Value
			,OtherCloseConcerns = CurrentPregnancy.OtherCloseConcerns
			,OtherCloseConcernsCode = CurrentPregnancy.OtherCloseConcerns_Value
			,BookedAtOtherHospitalReason = CurrentPregnancy.BookedAtOtherHospitalReason
			,BookedAtOtherHospitalReasonCode = CurrentPregnancy.BookedAtOtherHospitalReason_Value
			,DiscussedAtBooking = CurrentPregnancy.DiscussedAtBooking
			,DiscussedAtBookingCode = CurrentPregnancy.DiscussedAtBooking_Value
			,AnomalyScanOfferedAtBooking = CurrentPregnancy.AnomalyScanOfferedAtBooking
			,FirstLanguage = CurrentPregnancy.FirstLanguage
			,AnomalyScanOfferedAtBookingCode = CurrentPregnancy.AnomalyScanOfferedAtBooking_Value
			,FirstLanguageCode = CurrentPregnancy.FirstLanguage_Value
			,DatingScanDiscussed = CurrentPregnancy.DatingScanDiscussed
			,DatingScanDiscussedCode = CurrentPregnancy.DatingScanDiscussed_Value
			,DatingScanPerformedDate = CurrentPregnancy.DatingScanPerformedDate
			,DatingScanAnomalyDiagnosed = CurrentPregnancy.DatingScanAnomalyDiagnosed
			,DatingScanAnomalyDiagnosedCode = CurrentPregnancy.DatingScanAnomalyDiagnosed_Value
			,InterpreterRequired = CurrentPregnancy.InterpreterRequired
			,InterpreterRequiredCode = CurrentPregnancy.InterpreterRequired_Value
			,AnomalyScanResult = CurrentPregnancy.AnomalyScanResult
			,AnomalyScanResultCode = CurrentPregnancy.AnomalyScanResult_Value
			,AnomalyScanResultFetus2 = CurrentPregnancy.AnomalyScanResultFetus2
			,AnomalyScanDT = CurrentPregnancy.AnomalyScanDT
			,AnomalyScanResultFetus3 = CurrentPregnancy.AnomalyScanResultFetus3
			,AnomalyScanResultFetus4 = CurrentPregnancy.AnomalyScanResultFetus4
			,AnomalyScanResultFetus5 = CurrentPregnancy.AnomalyScanResultFetus5
			,AnomalyScanResultFetus6 = CurrentPregnancy.AnomalyScanResultFetus6
			,AnomalyScanResultFetus2Code = CurrentPregnancy.AnomalyScanResultFetus2_Value
			,AnomalyScanResultFetus3Code = CurrentPregnancy.AnomalyScanResultFetus3_Value
			,AnomalyScanResultFetus4Code = CurrentPregnancy.AnomalyScanResultFetus4_Value
			,AnomalyScanResultFetus5Code = CurrentPregnancy.AnomalyScanResultFetus5_Value
			,AnomalyScanResultFetus6Code = CurrentPregnancy.AnomalyScanResultFetus6_Value
			,MDSConsent = CurrentPregnancy.MDSConsent
			,DepressionIndicator3 = CurrentPregnancy.DepressionIndicator3
			,DepressionIndicator3Code = CurrentPregnancy.DepressionIndicator3_Value
			,DepressionIndicator4 = CurrentPregnancy.DepressionIndicator4
			,DepressionIndicator4Code = CurrentPregnancy.DepressionIndicator4_Value
			,PreCAFForm = CurrentPregnancy.PreCAFForm
			,PreCAFFormCode = CurrentPregnancy.PreCAFForm_Value
			,VTEConsultantReferral = CurrentPregnancy.VTEConsultantReferral
			,MDSConsentCode = CurrentPregnancy.MDSConsent_Value
			,VTEConsultantReferralCode = CurrentPregnancy.VTEConsultantReferral_Value
			,MentalHealthReferral = CurrentPregnancy.MentalHealthReferral
			,MentalHealthReferralCode = CurrentPregnancy.MentalHealthReferral_Value
			,DownsTestType = CurrentPregnancy.DownsTestType
			,DownsTestTypeCode = CurrentPregnancy.DownsTestType_Value
			,DownsTestDate = CurrentPregnancy.DownsTestDate
			,DownsScreenResult = CurrentPregnancy.DownsScreenResult
			,DownsScreenResultCode = CurrentPregnancy.DownsScreenResult_Value
			,DownsScreeningRiskRatioResult = CurrentPregnancy.DownsScreeningRiskRatioResult
			,DownsScreeningRiskRatioResultCode = CurrentPregnancy.DownsScreeningRiskRatioResult_Value
			,JointScreeningTestTrisomy13and18 = CurrentPregnancy.JointScreeningTestTrisomy13and18
			,JointScreeningTestTrisomy13and18Code = CurrentPregnancy.JointScreeningTestTrisomy13and18_Value
			,JointScreeningTestResult = CurrentPregnancy.JointScreeningTestResult
			,JointScreeningTestResultCode = CurrentPregnancy.JointScreeningTestResult_Value
			,JointScreeningTestRiskRatio = CurrentPregnancy.JointScreeningTestRiskRatio
			,JointScreeningTestRiskRatioCode = CurrentPregnancy.JointScreeningTestRiskRatio_Value
			,ECigaretteUse = CurrentPregnancy.ECigaretteUse
		from
			E3.dbo.tblReport_Current_Pregnancy_10 CurrentPregnancy

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3CurrentPregnancy on #TLoadE3CurrentPregnancy
	(
	PregnancyID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	E3.CurrentPregnancy target
using
	(
	select
		*
	from
		#TLoadE3CurrentPregnancy
	
	) source
	on	source.PregnancyID = target.PregnancyID

when not matched by source
then
	delete

when not matched
then
	insert
		(
		UserID
		,PatientID
		,PregnancyID
		,HeightCode
		,BloodGroup
		,Height
		,AccomodationCode
		,AgenciesInvolvedCode
		,AlcoholAtBookingCode
		,AlcoholBeforePregCode
		,AppointmentsCode
		,BabiesOnScanCode
		,BabysFatherEthnicityCode
		,BenefitsInformationCode
		,BirthPlaceIntendedCode
		,BloodTakenBookingCode
		,BookingAfter12wksCode
		,BPBookingCode
		,BPCuffSizeCode
		,BroaderFamilyIssuesCode
		,CAFFormCompletedCode
		,CareTypeCode
		,ChlamydiaScreeningCode
		,ConsentBloodProductsCode
		,ContraceptionFailCode
		,EDDScanCode
		,FeelingDownDepressedCode
		,FeelingUniterestedCode
		,FirstContactCode
		,FirstContactDateCode
		,FolicAcidCode
		,GravidaCode
		,HaemoglobinopathyScreeningCode
		,HepatitisBScreeningCode
		,HIVScreeningCode
		,HospitalAttachedCode
		,InformationLeafletsCode
		,InterviewCode
		,InterviewerCode
		,InterviewSupervisedByCode
		,LargeBPCuffCode
		,LeadProfessionalCode
		,LivesWithCode
		,MedicationThisPregnancyCode
		,MinorDisordersCode
		,NamedMidwifeCode
		,ParityCode
		,PCTCode
		,PrevCSCode
		,QuittingSmokingCode
		,RecreationalDrugsCode
		,RecreationalDrugsBookingCode
		,ReferralsCode
		,RubellaScreeingCode
		,ScreeningTestASBCode
		,SmokedEverCode
		,SmokerBeforePregnancyCode
		,SmokerInHouseholdCode
		,SmokerInHouseholdReferralCode
		,SmokesAtBookingCode
		,SubstanceAlcoholActionCode
		,SupportRequestedCode
		,SyphilisScreeningCode
		,TeamCode
		,TenseOrStressedCode
		,ThromboembolicRiskCode
		,ThromboprophylaxisPrescribedCode
		,TreatmentCode
		,VBACCode
		,WeightAtBookingCode
		,WhereBookedCode
		,Accomodation
		,AgenciesInvolved
		,AlcoholAtBooking
		,AlcoholBeforePreg
		,Appointments
		,BabiesOnScan
		,BabysFatherEthnicity
		,BenefitsInformation
		,BirthPlaceIntended
		,BleedingEpisodes
		,BloodTakenBooking
		,BookingAfter12wks
		,BookingDate
		,BPBooking
		,BPCuffSize
		,BroaderFamilyIssues
		,CAFFormCompleted
		,CareType
		,CategoryRickSocial
		,CategoryRisk
		,CertainLMP
		,ChlamydiaScreening
		,ConsentBloodProducts
		,ContraceptionFail
		,CycleLength
		,DateCAFFormCompleted
		,Diet
		,EDDScan
		,FeelingDownDepressed
		,FeelingUniterested
		,FirstContact
		,FirstContactDate
		,FolicAcid
		,Gravida
		,HaemoglobinopathyScreening
		,HepatitisBScreening
		,HIVCouncelling
		,HIVScreening
		,HospitalAttached
		,InformationLeaflets
		,Interview
		,Interviewer
		,InterviewSupervisedBy
		,LargeBPCuff
		,LeadProfessional
		,LivesWith
		,LMP
		,MedicationThisPregnancy
		,MinorDisorders
		,NamedMidwife
		,Parity
		,PartnerBloodRel
		,PCT
		,PlannedPregnancy
		,PrevCS
		,QuittingSmoking
		,RecreationalDrugs
		,RecreationalDrugsBooking
		,Referrals
		,RubellaScreeing
		,ScreeningTestASB
		,SmokedEver
		,SmokerBeforePregnancy
		,SmokerInHousehold
		,SmokerInHouseholdReferral
		,SmokesAtBooking
		,SubstanceAlcoholAction
		,SupportRequested
		,SyphilisScreening
		,Team
		,TenseOrStressed
		,ThromboembolicRisk
		,ThromboprophylaxisPrescribed
		,Treatment
		,Urinalysis
		,VBAC
		,WeightAtBooking
		,WhereBooked
		,DietCode
		,SmokesNumber
		,DownsScreening
		,DownsScreeningCode
		,SmokesNumberCode
		,KetoneTestingStrips
		,KetoneTestingStripsCode
		,MonitorBloodGlucose
		,MonitorBloodGlucoseCode
		,SmokingCessation
		,SmokingCessationCode
		,SmokingCessationComments
		,SmokingCessationCommentsCode
		,COReading
		,CategoryRiskCode
		,COReadingCode
		,SeasonalFluVaccine
		,SeasonalFluVaccineCode
		,Comments
		,CommentsCode
		,PartnersEthnicity
		,PartnersEthnicityCode
		,LanguageLiteracy
		,LanguageLiteracyCode
		,LMPCode
		,EmploymentStatusMotherBooking
		,EmploymentStatusMotherBookingCode
		,EmploymentStatusPartnerBooking
		,BleedingEpisodesCode
		,EmploymentStatusPartnerBookingCode
		,BookedAtOtherHospital
		,BookedAtOtherHospitalCode
		,OtherCloseConcerns
		,OtherCloseConcernsCode
		,BookedAtOtherHospitalReason
		,BookedAtOtherHospitalReasonCode
		,DiscussedAtBooking
		,DiscussedAtBookingCode
		,AnomalyScanOfferedAtBooking
		,FirstLanguage
		,AnomalyScanOfferedAtBookingCode
		,FirstLanguageCode
		,DatingScanDiscussed
		,DatingScanDiscussedCode
		,DatingScanPerformedDate
		,DatingScanAnomalyDiagnosed
		,DatingScanAnomalyDiagnosedCode
		,InterpreterRequired
		,InterpreterRequiredCode
		,AnomalyScanResult
		,AnomalyScanResultCode
		,AnomalyScanResultFetus2
		,AnomalyScanDT
		,AnomalyScanResultFetus3
		,AnomalyScanResultFetus4
		,AnomalyScanResultFetus5
		,AnomalyScanResultFetus6
		,AnomalyScanResultFetus2Code
		,AnomalyScanResultFetus3Code
		,AnomalyScanResultFetus4Code
		,AnomalyScanResultFetus5Code
		,AnomalyScanResultFetus6Code
		,MDSConsent
		,DepressionIndicator3
		,DepressionIndicator3Code
		,DepressionIndicator4
		,DepressionIndicator4Code
		,PreCAFForm
		,PreCAFFormCode
		,VTEConsultantReferral
		,MDSConsentCode
		,VTEConsultantReferralCode
		,MentalHealthReferral
		,MentalHealthReferralCode
		,DownsTestType
		,DownsTestTypeCode
		,DownsTestDate
		,DownsScreenResult
		,DownsScreenResultCode
		,DownsScreeningRiskRatioResult
		,DownsScreeningRiskRatioResultCode
		,JointScreeningTestTrisomy13and18
		,JointScreeningTestTrisomy13and18Code
		,JointScreeningTestResult
		,JointScreeningTestResultCode
		,JointScreeningTestRiskRatio
		,JointScreeningTestRiskRatioCode
		,ECigaretteUse

		,Created
		,ByWhom
		)
	values
		(
		source.UserID
		,source.PatientID
		,source.PregnancyID
		,source.HeightCode
		,source.BloodGroup
		,source.Height
		,source.AccomodationCode
		,source.AgenciesInvolvedCode
		,source.AlcoholAtBookingCode
		,source.AlcoholBeforePregCode
		,source.AppointmentsCode
		,source.BabiesOnScanCode
		,source.BabysFatherEthnicityCode
		,source.BenefitsInformationCode
		,source.BirthPlaceIntendedCode
		,source.BloodTakenBookingCode
		,source.BookingAfter12wksCode
		,source.BPBookingCode
		,source.BPCuffSizeCode
		,source.BroaderFamilyIssuesCode
		,source.CAFFormCompletedCode
		,source.CareTypeCode
		,source.ChlamydiaScreeningCode
		,source.ConsentBloodProductsCode
		,source.ContraceptionFailCode
		,source.EDDScanCode
		,source.FeelingDownDepressedCode
		,source.FeelingUniterestedCode
		,source.FirstContactCode
		,source.FirstContactDateCode
		,source.FolicAcidCode
		,source.GravidaCode
		,source.HaemoglobinopathyScreeningCode
		,source.HepatitisBScreeningCode
		,source.HIVScreeningCode
		,source.HospitalAttachedCode
		,source.InformationLeafletsCode
		,source.InterviewCode
		,source.InterviewerCode
		,source.InterviewSupervisedByCode
		,source.LargeBPCuffCode
		,source.LeadProfessionalCode
		,source.LivesWithCode
		,source.MedicationThisPregnancyCode
		,source.MinorDisordersCode
		,source.NamedMidwifeCode
		,source.ParityCode
		,source.PCTCode
		,source.PrevCSCode
		,source.QuittingSmokingCode
		,source.RecreationalDrugsCode
		,source.RecreationalDrugsBookingCode
		,source.ReferralsCode
		,source.RubellaScreeingCode
		,source.ScreeningTestASBCode
		,source.SmokedEverCode
		,source.SmokerBeforePregnancyCode
		,source.SmokerInHouseholdCode
		,source.SmokerInHouseholdReferralCode
		,source.SmokesAtBookingCode
		,source.SubstanceAlcoholActionCode
		,source.SupportRequestedCode
		,source.SyphilisScreeningCode
		,source.TeamCode
		,source.TenseOrStressedCode
		,source.ThromboembolicRiskCode
		,source.ThromboprophylaxisPrescribedCode
		,source.TreatmentCode
		,source.VBACCode
		,source.WeightAtBookingCode
		,source.WhereBookedCode
		,source.Accomodation
		,source.AgenciesInvolved
		,source.AlcoholAtBooking
		,source.AlcoholBeforePreg
		,source.Appointments
		,source.BabiesOnScan
		,source.BabysFatherEthnicity
		,source.BenefitsInformation
		,source.BirthPlaceIntended
		,source.BleedingEpisodes
		,source.BloodTakenBooking
		,source.BookingAfter12wks
		,source.BookingDate
		,source.BPBooking
		,source.BPCuffSize
		,source.BroaderFamilyIssues
		,source.CAFFormCompleted
		,source.CareType
		,source.CategoryRickSocial
		,source.CategoryRisk
		,source.CertainLMP
		,source.ChlamydiaScreening
		,source.ConsentBloodProducts
		,source.ContraceptionFail
		,source.CycleLength
		,source.DateCAFFormCompleted
		,source.Diet
		,source.EDDScan
		,source.FeelingDownDepressed
		,source.FeelingUniterested
		,source.FirstContact
		,source.FirstContactDate
		,source.FolicAcid
		,source.Gravida
		,source.HaemoglobinopathyScreening
		,source.HepatitisBScreening
		,source.HIVCouncelling
		,source.HIVScreening
		,source.HospitalAttached
		,source.InformationLeaflets
		,source.Interview
		,source.Interviewer
		,source.InterviewSupervisedBy
		,source.LargeBPCuff
		,source.LeadProfessional
		,source.LivesWith
		,source.LMP
		,source.MedicationThisPregnancy
		,source.MinorDisorders
		,source.NamedMidwife
		,source.Parity
		,source.PartnerBloodRel
		,source.PCT
		,source.PlannedPregnancy
		,source.PrevCS
		,source.QuittingSmoking
		,source.RecreationalDrugs
		,source.RecreationalDrugsBooking
		,source.Referrals
		,source.RubellaScreeing
		,source.ScreeningTestASB
		,source.SmokedEver
		,source.SmokerBeforePregnancy
		,source.SmokerInHousehold
		,source.SmokerInHouseholdReferral
		,source.SmokesAtBooking
		,source.SubstanceAlcoholAction
		,source.SupportRequested
		,source.SyphilisScreening
		,source.Team
		,source.TenseOrStressed
		,source.ThromboembolicRisk
		,source.ThromboprophylaxisPrescribed
		,source.Treatment
		,source.Urinalysis
		,source.VBAC
		,source.WeightAtBooking
		,source.WhereBooked
		,source.DietCode
		,source.SmokesNumber
		,source.DownsScreening
		,source.DownsScreeningCode
		,source.SmokesNumberCode
		,source.KetoneTestingStrips
		,source.KetoneTestingStripsCode
		,source.MonitorBloodGlucose
		,source.MonitorBloodGlucoseCode
		,source.SmokingCessation
		,source.SmokingCessationCode
		,source.SmokingCessationComments
		,source.SmokingCessationCommentsCode
		,source.COReading
		,source.CategoryRiskCode
		,source.COReadingCode
		,source.SeasonalFluVaccine
		,source.SeasonalFluVaccineCode
		,source.Comments
		,source.CommentsCode
		,source.PartnersEthnicity
		,source.PartnersEthnicityCode
		,source.LanguageLiteracy
		,source.LanguageLiteracyCode
		,source.LMPCode
		,source.EmploymentStatusMotherBooking
		,source.EmploymentStatusMotherBookingCode
		,source.EmploymentStatusPartnerBooking
		,source.BleedingEpisodesCode
		,source.EmploymentStatusPartnerBookingCode
		,source.BookedAtOtherHospital
		,source.BookedAtOtherHospitalCode
		,source.OtherCloseConcerns
		,source.OtherCloseConcernsCode
		,source.BookedAtOtherHospitalReason
		,source.BookedAtOtherHospitalReasonCode
		,source.DiscussedAtBooking
		,source.DiscussedAtBookingCode
		,source.AnomalyScanOfferedAtBooking
		,source.FirstLanguage
		,source.AnomalyScanOfferedAtBookingCode
		,source.FirstLanguageCode
		,source.DatingScanDiscussed
		,source.DatingScanDiscussedCode
		,source.DatingScanPerformedDate
		,source.DatingScanAnomalyDiagnosed
		,source.DatingScanAnomalyDiagnosedCode
		,source.InterpreterRequired
		,source.InterpreterRequiredCode
		,source.AnomalyScanResult
		,source.AnomalyScanResultCode
		,source.AnomalyScanResultFetus2
		,source.AnomalyScanDT
		,source.AnomalyScanResultFetus3
		,source.AnomalyScanResultFetus4
		,source.AnomalyScanResultFetus5
		,source.AnomalyScanResultFetus6
		,source.AnomalyScanResultFetus2Code
		,source.AnomalyScanResultFetus3Code
		,source.AnomalyScanResultFetus4Code
		,source.AnomalyScanResultFetus5Code
		,source.AnomalyScanResultFetus6Code
		,source.MDSConsent
		,source.DepressionIndicator3
		,source.DepressionIndicator3Code
		,source.DepressionIndicator4
		,source.DepressionIndicator4Code
		,source.PreCAFForm
		,source.PreCAFFormCode
		,source.VTEConsultantReferral
		,source.MDSConsentCode
		,source.VTEConsultantReferralCode
		,source.MentalHealthReferral
		,source.MentalHealthReferralCode
		,source.DownsTestType
		,source.DownsTestTypeCode
		,source.DownsTestDate
		,source.DownsScreenResult
		,source.DownsScreenResultCode
		,source.DownsScreeningRiskRatioResult
		,source.DownsScreeningRiskRatioResultCode
		,source.JointScreeningTestTrisomy13and18
		,source.JointScreeningTestTrisomy13and18Code
		,source.JointScreeningTestResult
		,source.JointScreeningTestResultCode
		,source.JointScreeningTestRiskRatio
		,source.JointScreeningTestRiskRatioCode
		,source.ECigaretteUse

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.UserID
		,target.PatientID
		,target.HeightCode
		,target.BloodGroup
		,target.Height
		,target.AccomodationCode
		,target.AgenciesInvolvedCode
		,target.AlcoholAtBookingCode
		,target.AlcoholBeforePregCode
		,target.AppointmentsCode
		,target.BabiesOnScanCode
		,target.BabysFatherEthnicityCode
		,target.BenefitsInformationCode
		,target.BirthPlaceIntendedCode
		,target.BloodTakenBookingCode
		,target.BookingAfter12wksCode
		,target.BPBookingCode
		,target.BPCuffSizeCode
		,target.BroaderFamilyIssuesCode
		,target.CAFFormCompletedCode
		,target.CareTypeCode
		,target.ChlamydiaScreeningCode
		,target.ConsentBloodProductsCode
		,target.ContraceptionFailCode
		,target.EDDScanCode
		,target.FeelingDownDepressedCode
		,target.FeelingUniterestedCode
		,target.FirstContactCode
		,target.FirstContactDateCode
		,target.FolicAcidCode
		,target.GravidaCode
		,target.HaemoglobinopathyScreeningCode
		,target.HepatitisBScreeningCode
		,target.HIVScreeningCode
		,target.HospitalAttachedCode
		,target.InformationLeafletsCode
		,target.InterviewCode
		,target.InterviewerCode
		,target.InterviewSupervisedByCode
		,target.LargeBPCuffCode
		,target.LeadProfessionalCode
		,target.LivesWithCode
		,target.MedicationThisPregnancyCode
		,target.MinorDisordersCode
		,target.NamedMidwifeCode
		,target.ParityCode
		,target.PCTCode
		,target.PrevCSCode
		,target.QuittingSmokingCode
		,target.RecreationalDrugsCode
		,target.RecreationalDrugsBookingCode
		,target.ReferralsCode
		,target.RubellaScreeingCode
		,target.ScreeningTestASBCode
		,target.SmokedEverCode
		,target.SmokerBeforePregnancyCode
		,target.SmokerInHouseholdCode
		,target.SmokerInHouseholdReferralCode
		,target.SmokesAtBookingCode
		,target.SubstanceAlcoholActionCode
		,target.SupportRequestedCode
		,target.SyphilisScreeningCode
		,target.TeamCode
		,target.TenseOrStressedCode
		,target.ThromboembolicRiskCode
		,target.ThromboprophylaxisPrescribedCode
		,target.TreatmentCode
		,target.VBACCode
		,target.WeightAtBookingCode
		,target.WhereBookedCode
		,target.Accomodation
		,target.AgenciesInvolved
		,target.AlcoholAtBooking
		,target.AlcoholBeforePreg
		,target.Appointments
		,target.BabiesOnScan
		,target.BabysFatherEthnicity
		,target.BenefitsInformation
		,target.BirthPlaceIntended
		,target.BleedingEpisodes
		,target.BloodTakenBooking
		,target.BookingAfter12wks
		,target.BookingDate
		,target.BPBooking
		,target.BPCuffSize
		,target.BroaderFamilyIssues
		,target.CAFFormCompleted
		,target.CareType
		,target.CategoryRickSocial
		,target.CategoryRisk
		,target.CertainLMP
		,target.ChlamydiaScreening
		,target.ConsentBloodProducts
		,target.ContraceptionFail
		,target.CycleLength
		,target.DateCAFFormCompleted
		,target.Diet
		,target.EDDScan
		,target.FeelingDownDepressed
		,target.FeelingUniterested
		,target.FirstContact
		,target.FirstContactDate
		,target.FolicAcid
		,target.Gravida
		,target.HaemoglobinopathyScreening
		,target.HepatitisBScreening
		,target.HIVCouncelling
		,target.HIVScreening
		,target.HospitalAttached
		,target.InformationLeaflets
		,target.Interview
		,target.Interviewer
		,target.InterviewSupervisedBy
		,target.LargeBPCuff
		,target.LeadProfessional
		,target.LivesWith
		,target.LMP
		,target.MedicationThisPregnancy
		,target.MinorDisorders
		,target.NamedMidwife
		,target.Parity
		,target.PartnerBloodRel
		,target.PCT
		,target.PlannedPregnancy
		,target.PrevCS
		,target.QuittingSmoking
		,target.RecreationalDrugs
		,target.RecreationalDrugsBooking
		,target.Referrals
		,target.RubellaScreeing
		,target.ScreeningTestASB
		,target.SmokedEver
		,target.SmokerBeforePregnancy
		,target.SmokerInHousehold
		,target.SmokerInHouseholdReferral
		,target.SmokesAtBooking
		,target.SubstanceAlcoholAction
		,target.SupportRequested
		,target.SyphilisScreening
		,target.Team
		,target.TenseOrStressed
		,target.ThromboembolicRisk
		,target.ThromboprophylaxisPrescribed
		,target.Treatment
		,target.Urinalysis
		,target.VBAC
		,target.WeightAtBooking
		,target.WhereBooked
		,target.DietCode
		,target.SmokesNumber
		,target.DownsScreening
		,target.DownsScreeningCode
		,target.SmokesNumberCode
		,target.KetoneTestingStrips
		,target.KetoneTestingStripsCode
		,target.MonitorBloodGlucose
		,target.MonitorBloodGlucoseCode
		,target.SmokingCessation
		,target.SmokingCessationCode
		,target.SmokingCessationComments
		,target.SmokingCessationCommentsCode
		,target.COReading
		,target.CategoryRiskCode
		,target.COReadingCode
		,target.SeasonalFluVaccine
		,target.SeasonalFluVaccineCode
		,target.Comments
		,target.CommentsCode
		,target.PartnersEthnicity
		,target.PartnersEthnicityCode
		,target.LanguageLiteracy
		,target.LanguageLiteracyCode
		,target.LMPCode
		,target.EmploymentStatusMotherBooking
		,target.EmploymentStatusMotherBookingCode
		,target.EmploymentStatusPartnerBooking
		,target.BleedingEpisodesCode
		,target.EmploymentStatusPartnerBookingCode
		,target.BookedAtOtherHospital
		,target.BookedAtOtherHospitalCode
		,target.OtherCloseConcerns
		,target.OtherCloseConcernsCode
		,target.BookedAtOtherHospitalReason
		,target.BookedAtOtherHospitalReasonCode
		,target.DiscussedAtBooking
		,target.DiscussedAtBookingCode
		,target.AnomalyScanOfferedAtBooking
		,target.FirstLanguage
		,target.AnomalyScanOfferedAtBookingCode
		,target.FirstLanguageCode
		,target.DatingScanDiscussed
		,target.DatingScanDiscussedCode
		,target.DatingScanPerformedDate
		,target.DatingScanAnomalyDiagnosed
		,target.DatingScanAnomalyDiagnosedCode
		,target.InterpreterRequired
		,target.InterpreterRequiredCode
		,target.AnomalyScanResult
		,target.AnomalyScanResultCode
		,target.AnomalyScanResultFetus2
		,target.AnomalyScanDT
		,target.AnomalyScanResultFetus3
		,target.AnomalyScanResultFetus4
		,target.AnomalyScanResultFetus5
		,target.AnomalyScanResultFetus6
		,target.AnomalyScanResultFetus2Code
		,target.AnomalyScanResultFetus3Code
		,target.AnomalyScanResultFetus4Code
		,target.AnomalyScanResultFetus5Code
		,target.AnomalyScanResultFetus6Code
		,target.MDSConsent
		,target.DepressionIndicator3
		,target.DepressionIndicator3Code
		,target.DepressionIndicator4
		,target.DepressionIndicator4Code
		,target.PreCAFForm
		,target.PreCAFFormCode
		,target.VTEConsultantReferral
		,target.MDSConsentCode
		,target.VTEConsultantReferralCode
		,target.MentalHealthReferral
		,target.MentalHealthReferralCode
		,target.DownsTestType
		,target.DownsTestTypeCode
		,target.DownsTestDate
		,target.DownsScreenResult
		,target.DownsScreenResultCode
		,target.DownsScreeningRiskRatioResult
		,target.DownsScreeningRiskRatioResultCode
		,target.JointScreeningTestTrisomy13and18
		,target.JointScreeningTestTrisomy13and18Code
		,target.JointScreeningTestResult
		,target.JointScreeningTestResultCode
		,target.JointScreeningTestRiskRatio
		,target.JointScreeningTestRiskRatioCode
		,target.ECigaretteUse
		)

then
	update
	set
		target.UserID = source.UserID
		,target.PatientID = source.PatientID
		,target.HeightCode = source.HeightCode
		,target.BloodGroup = source.BloodGroup
		,target.Height = source.Height
		,target.AccomodationCode = source.AccomodationCode
		,target.AgenciesInvolvedCode = source.AgenciesInvolvedCode
		,target.AlcoholAtBookingCode = source.AlcoholAtBookingCode
		,target.AlcoholBeforePregCode = source.AlcoholBeforePregCode
		,target.AppointmentsCode = source.AppointmentsCode
		,target.BabiesOnScanCode = source.BabiesOnScanCode
		,target.BabysFatherEthnicityCode = source.BabysFatherEthnicityCode
		,target.BenefitsInformationCode = source.BenefitsInformationCode
		,target.BirthPlaceIntendedCode = source.BirthPlaceIntendedCode
		,target.BloodTakenBookingCode = source.BloodTakenBookingCode
		,target.BookingAfter12wksCode = source.BookingAfter12wksCode
		,target.BPBookingCode = source.BPBookingCode
		,target.BPCuffSizeCode = source.BPCuffSizeCode
		,target.BroaderFamilyIssuesCode = source.BroaderFamilyIssuesCode
		,target.CAFFormCompletedCode = source.CAFFormCompletedCode
		,target.CareTypeCode = source.CareTypeCode
		,target.ChlamydiaScreeningCode = source.ChlamydiaScreeningCode
		,target.ConsentBloodProductsCode = source.ConsentBloodProductsCode
		,target.ContraceptionFailCode = source.ContraceptionFailCode
		,target.EDDScanCode = source.EDDScanCode
		,target.FeelingDownDepressedCode = source.FeelingDownDepressedCode
		,target.FeelingUniterestedCode = source.FeelingUniterestedCode
		,target.FirstContactCode = source.FirstContactCode
		,target.FirstContactDateCode = source.FirstContactDateCode
		,target.FolicAcidCode = source.FolicAcidCode
		,target.GravidaCode = source.GravidaCode
		,target.HaemoglobinopathyScreeningCode = source.HaemoglobinopathyScreeningCode
		,target.HepatitisBScreeningCode = source.HepatitisBScreeningCode
		,target.HIVScreeningCode = source.HIVScreeningCode
		,target.HospitalAttachedCode = source.HospitalAttachedCode
		,target.InformationLeafletsCode = source.InformationLeafletsCode
		,target.InterviewCode = source.InterviewCode
		,target.InterviewerCode = source.InterviewerCode
		,target.InterviewSupervisedByCode = source.InterviewSupervisedByCode
		,target.LargeBPCuffCode = source.LargeBPCuffCode
		,target.LeadProfessionalCode = source.LeadProfessionalCode
		,target.LivesWithCode = source.LivesWithCode
		,target.MedicationThisPregnancyCode = source.MedicationThisPregnancyCode
		,target.MinorDisordersCode = source.MinorDisordersCode
		,target.NamedMidwifeCode = source.NamedMidwifeCode
		,target.ParityCode = source.ParityCode
		,target.PCTCode = source.PCTCode
		,target.PrevCSCode = source.PrevCSCode
		,target.QuittingSmokingCode = source.QuittingSmokingCode
		,target.RecreationalDrugsCode = source.RecreationalDrugsCode
		,target.RecreationalDrugsBookingCode = source.RecreationalDrugsBookingCode
		,target.ReferralsCode = source.ReferralsCode
		,target.RubellaScreeingCode = source.RubellaScreeingCode
		,target.ScreeningTestASBCode = source.ScreeningTestASBCode
		,target.SmokedEverCode = source.SmokedEverCode
		,target.SmokerBeforePregnancyCode = source.SmokerBeforePregnancyCode
		,target.SmokerInHouseholdCode = source.SmokerInHouseholdCode
		,target.SmokerInHouseholdReferralCode = source.SmokerInHouseholdReferralCode
		,target.SmokesAtBookingCode = source.SmokesAtBookingCode
		,target.SubstanceAlcoholActionCode = source.SubstanceAlcoholActionCode
		,target.SupportRequestedCode = source.SupportRequestedCode
		,target.SyphilisScreeningCode = source.SyphilisScreeningCode
		,target.TeamCode = source.TeamCode
		,target.TenseOrStressedCode = source.TenseOrStressedCode
		,target.ThromboembolicRiskCode = source.ThromboembolicRiskCode
		,target.ThromboprophylaxisPrescribedCode = source.ThromboprophylaxisPrescribedCode
		,target.TreatmentCode = source.TreatmentCode
		,target.VBACCode = source.VBACCode
		,target.WeightAtBookingCode = source.WeightAtBookingCode
		,target.WhereBookedCode = source.WhereBookedCode
		,target.Accomodation = source.Accomodation
		,target.AgenciesInvolved = source.AgenciesInvolved
		,target.AlcoholAtBooking = source.AlcoholAtBooking
		,target.AlcoholBeforePreg = source.AlcoholBeforePreg
		,target.Appointments = source.Appointments
		,target.BabiesOnScan = source.BabiesOnScan
		,target.BabysFatherEthnicity = source.BabysFatherEthnicity
		,target.BenefitsInformation = source.BenefitsInformation
		,target.BirthPlaceIntended = source.BirthPlaceIntended
		,target.BleedingEpisodes = source.BleedingEpisodes
		,target.BloodTakenBooking = source.BloodTakenBooking
		,target.BookingAfter12wks = source.BookingAfter12wks
		,target.BookingDate = source.BookingDate
		,target.BPBooking = source.BPBooking
		,target.BPCuffSize = source.BPCuffSize
		,target.BroaderFamilyIssues = source.BroaderFamilyIssues
		,target.CAFFormCompleted = source.CAFFormCompleted
		,target.CareType = source.CareType
		,target.CategoryRickSocial = source.CategoryRickSocial
		,target.CategoryRisk = source.CategoryRisk
		,target.CertainLMP = source.CertainLMP
		,target.ChlamydiaScreening = source.ChlamydiaScreening
		,target.ConsentBloodProducts = source.ConsentBloodProducts
		,target.ContraceptionFail = source.ContraceptionFail
		,target.CycleLength = source.CycleLength
		,target.DateCAFFormCompleted = source.DateCAFFormCompleted
		,target.Diet = source.Diet
		,target.EDDScan = source.EDDScan
		,target.FeelingDownDepressed = source.FeelingDownDepressed
		,target.FeelingUniterested = source.FeelingUniterested
		,target.FirstContact = source.FirstContact
		,target.FirstContactDate = source.FirstContactDate
		,target.FolicAcid = source.FolicAcid
		,target.Gravida = source.Gravida
		,target.HaemoglobinopathyScreening = source.HaemoglobinopathyScreening
		,target.HepatitisBScreening = source.HepatitisBScreening
		,target.HIVCouncelling = source.HIVCouncelling
		,target.HIVScreening = source.HIVScreening
		,target.HospitalAttached = source.HospitalAttached
		,target.InformationLeaflets = source.InformationLeaflets
		,target.Interview = source.Interview
		,target.Interviewer = source.Interviewer
		,target.InterviewSupervisedBy = source.InterviewSupervisedBy
		,target.LargeBPCuff = source.LargeBPCuff
		,target.LeadProfessional = source.LeadProfessional
		,target.LivesWith = source.LivesWith
		,target.LMP = source.LMP
		,target.MedicationThisPregnancy = source.MedicationThisPregnancy
		,target.MinorDisorders = source.MinorDisorders
		,target.NamedMidwife = source.NamedMidwife
		,target.Parity = source.Parity
		,target.PartnerBloodRel = source.PartnerBloodRel
		,target.PCT = source.PCT
		,target.PlannedPregnancy = source.PlannedPregnancy
		,target.PrevCS = source.PrevCS
		,target.QuittingSmoking = source.QuittingSmoking
		,target.RecreationalDrugs = source.RecreationalDrugs
		,target.RecreationalDrugsBooking = source.RecreationalDrugsBooking
		,target.Referrals = source.Referrals
		,target.RubellaScreeing = source.RubellaScreeing
		,target.ScreeningTestASB = source.ScreeningTestASB
		,target.SmokedEver = source.SmokedEver
		,target.SmokerBeforePregnancy = source.SmokerBeforePregnancy
		,target.SmokerInHousehold = source.SmokerInHousehold
		,target.SmokerInHouseholdReferral = source.SmokerInHouseholdReferral
		,target.SmokesAtBooking = source.SmokesAtBooking
		,target.SubstanceAlcoholAction = source.SubstanceAlcoholAction
		,target.SupportRequested = source.SupportRequested
		,target.SyphilisScreening = source.SyphilisScreening
		,target.Team = source.Team
		,target.TenseOrStressed = source.TenseOrStressed
		,target.ThromboembolicRisk = source.ThromboembolicRisk
		,target.ThromboprophylaxisPrescribed = source.ThromboprophylaxisPrescribed
		,target.Treatment = source.Treatment
		,target.Urinalysis = source.Urinalysis
		,target.VBAC = source.VBAC
		,target.WeightAtBooking = source.WeightAtBooking
		,target.WhereBooked = source.WhereBooked
		,target.DietCode = source.DietCode
		,target.SmokesNumber = source.SmokesNumber
		,target.DownsScreening = source.DownsScreening
		,target.DownsScreeningCode = source.DownsScreeningCode
		,target.SmokesNumberCode = source.SmokesNumberCode
		,target.KetoneTestingStrips = source.KetoneTestingStrips
		,target.KetoneTestingStripsCode = source.KetoneTestingStripsCode
		,target.MonitorBloodGlucose = source.MonitorBloodGlucose
		,target.MonitorBloodGlucoseCode = source.MonitorBloodGlucoseCode
		,target.SmokingCessation = source.SmokingCessation
		,target.SmokingCessationCode = source.SmokingCessationCode
		,target.SmokingCessationComments = source.SmokingCessationComments
		,target.SmokingCessationCommentsCode = source.SmokingCessationCommentsCode
		,target.COReading = source.COReading
		,target.CategoryRiskCode = source.CategoryRiskCode
		,target.COReadingCode = source.COReadingCode
		,target.SeasonalFluVaccine = source.SeasonalFluVaccine
		,target.SeasonalFluVaccineCode = source.SeasonalFluVaccineCode
		,target.Comments = source.Comments
		,target.CommentsCode = source.CommentsCode
		,target.PartnersEthnicity = source.PartnersEthnicity
		,target.PartnersEthnicityCode = source.PartnersEthnicityCode
		,target.LanguageLiteracy = source.LanguageLiteracy
		,target.LanguageLiteracyCode = source.LanguageLiteracyCode
		,target.LMPCode = source.LMPCode
		,target.EmploymentStatusMotherBooking = source.EmploymentStatusMotherBooking
		,target.EmploymentStatusMotherBookingCode = source.EmploymentStatusMotherBookingCode
		,target.EmploymentStatusPartnerBooking = source.EmploymentStatusPartnerBooking
		,target.BleedingEpisodesCode = source.BleedingEpisodesCode
		,target.EmploymentStatusPartnerBookingCode = source.EmploymentStatusPartnerBookingCode
		,target.BookedAtOtherHospital = source.BookedAtOtherHospital
		,target.BookedAtOtherHospitalCode = source.BookedAtOtherHospitalCode
		,target.OtherCloseConcerns = source.OtherCloseConcerns
		,target.OtherCloseConcernsCode = source.OtherCloseConcernsCode
		,target.BookedAtOtherHospitalReason = source.BookedAtOtherHospitalReason
		,target.BookedAtOtherHospitalReasonCode = source.BookedAtOtherHospitalReasonCode
		,target.DiscussedAtBooking = source.DiscussedAtBooking
		,target.DiscussedAtBookingCode = source.DiscussedAtBookingCode
		,target.AnomalyScanOfferedAtBooking = source.AnomalyScanOfferedAtBooking
		,target.FirstLanguage = source.FirstLanguage
		,target.AnomalyScanOfferedAtBookingCode = source.AnomalyScanOfferedAtBookingCode
		,target.FirstLanguageCode = source.FirstLanguageCode
		,target.DatingScanDiscussed = source.DatingScanDiscussed
		,target.DatingScanDiscussedCode = source.DatingScanDiscussedCode
		,target.DatingScanPerformedDate = source.DatingScanPerformedDate
		,target.DatingScanAnomalyDiagnosed = source.DatingScanAnomalyDiagnosed
		,target.DatingScanAnomalyDiagnosedCode = source.DatingScanAnomalyDiagnosedCode
		,target.InterpreterRequired = source.InterpreterRequired
		,target.InterpreterRequiredCode = source.InterpreterRequiredCode
		,target.AnomalyScanResult = source.AnomalyScanResult
		,target.AnomalyScanResultCode = source.AnomalyScanResultCode
		,target.AnomalyScanResultFetus2 = source.AnomalyScanResultFetus2
		,target.AnomalyScanDT = source.AnomalyScanDT
		,target.AnomalyScanResultFetus3 = source.AnomalyScanResultFetus3
		,target.AnomalyScanResultFetus4 = source.AnomalyScanResultFetus4
		,target.AnomalyScanResultFetus5 = source.AnomalyScanResultFetus5
		,target.AnomalyScanResultFetus6 = source.AnomalyScanResultFetus6
		,target.AnomalyScanResultFetus2Code = source.AnomalyScanResultFetus2Code
		,target.AnomalyScanResultFetus3Code = source.AnomalyScanResultFetus3Code
		,target.AnomalyScanResultFetus4Code = source.AnomalyScanResultFetus4Code
		,target.AnomalyScanResultFetus5Code = source.AnomalyScanResultFetus5Code
		,target.AnomalyScanResultFetus6Code = source.AnomalyScanResultFetus6Code
		,target.MDSConsent = source.MDSConsent
		,target.DepressionIndicator3 = source.DepressionIndicator3
		,target.DepressionIndicator3Code = source.DepressionIndicator3Code
		,target.DepressionIndicator4 = source.DepressionIndicator4
		,target.DepressionIndicator4Code = source.DepressionIndicator4Code
		,target.PreCAFForm = source.PreCAFForm
		,target.PreCAFFormCode = source.PreCAFFormCode
		,target.VTEConsultantReferral = source.VTEConsultantReferral
		,target.MDSConsentCode = source.MDSConsentCode
		,target.VTEConsultantReferralCode = source.VTEConsultantReferralCode
		,target.MentalHealthReferral = source.MentalHealthReferral
		,target.MentalHealthReferralCode = source.MentalHealthReferralCode
		,target.DownsTestType = source.DownsTestType
		,target.DownsTestTypeCode = source.DownsTestTypeCode
		,target.DownsTestDate = source.DownsTestDate
		,target.DownsScreenResult = source.DownsScreenResult
		,target.DownsScreenResultCode = source.DownsScreenResultCode
		,target.DownsScreeningRiskRatioResult = source.DownsScreeningRiskRatioResult
		,target.DownsScreeningRiskRatioResultCode = source.DownsScreeningRiskRatioResultCode
		,target.JointScreeningTestTrisomy13and18 = source.JointScreeningTestTrisomy13and18
		,target.JointScreeningTestTrisomy13and18Code = source.JointScreeningTestTrisomy13and18Code
		,target.JointScreeningTestResult = source.JointScreeningTestResult
		,target.JointScreeningTestResultCode = source.JointScreeningTestResultCode
		,target.JointScreeningTestRiskRatio = source.JointScreeningTestRiskRatio
		,target.JointScreeningTestRiskRatioCode = source.JointScreeningTestRiskRatioCode
		,target.ECigaretteUse = source.ECigaretteUse

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime