﻿CREATE procedure [E3].[LoadDeliveryProcedure] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			UserID
			,PatientID
			,BabiesNumberCode
			,BladderEmptiedCode
			,CommentsPostOpCode
			,ComplicationsCode
			,DiscussionWithConsCode
			,DrainageCode
			,FatStitchCode
			,FirstProcedureCode
			,ParietalPeritoneumCode
			,ProcedureConsentCode
			,SkinClosureCode
			,SkinIncisionCode
			,SterilizedMethodCode
			,ThromboembolicRiskCode
			,ThromboprophylaxisPrescribedCode
			,UterineIncisionCode
			,UterusClosedCode
			,UterusTubesOvariesCode
			,VisceralPeritoneumCode
			,ActionTaken
			,AnalgesiaPerineum
			,Antibiotics
			,AssistantSurgeon
			,BabiesNumber
			,BladderEmptied
			,BloodLossAtDelivery
			,CatheterInstructions
			,CommentsPostOp
			,Complications
			,DilatationBeforeLSCS
			,DiscussedDT
			,DiscussionWithCons
			,Drainage
			,DrainInstructions
			,Dressing
			,DrugsPostDelivery
			,ExamAfterRepair
			,FatStitch
			,FirstProcedure
			,LabiaSutured
			,LowerSegmentFormed
			,LowerSegmentThin
			,NeedlesAccountedFor
			,ParietalPeritoneum
			,PerinealMuscle
			,PerineumVaginalTears
			,PostVaginalWall
			,ProcedureConsent
			,SheathRepaired
			,SkinClosure
			,SkinIncision
			,SkinIncisionDT
			,SkinRepaired
			,SterilizationHistology
			,SterilizedAtLSCS
			,SterilizedMethod
			,SurgeonAtOperation
			,ThromboembolicRisk
			,ThromboprophylaxisPrescribed
			,UterineCavityChecked
			,UterineIncision
			,UterusClosed
			,UterusTubesOvaries
			,VisceralPeritoneum
			,InstrumentalDT
			,ConsultantInvolvedOVD
			,ConsultantInvolvedOVDCode
			,OVDPlacePerformed
			,OVDPlacePerformedCode
			)
into
	#TLoadE3DeliveryProcedure
from
	(
	select
		UserID = cast(UserID as int)
		,PatientID = cast(PatientID as bigint)
		,PregnancyID = cast(PregnancyID as bigint)
		,BabiesNumberCode = cast(BabiesNumberCode as bigint)
		,BladderEmptiedCode = cast(BladderEmptiedCode as bigint)
		,CommentsPostOpCode = cast(CommentsPostOpCode as bigint)
		,ComplicationsCode = cast(ComplicationsCode as bigint)
		,DiscussionWithConsCode = cast(DiscussionWithConsCode as bigint)
		,DrainageCode = cast(DrainageCode as bigint)
		,FatStitchCode = cast(FatStitchCode as bigint)
		,FirstProcedureCode = cast(FirstProcedureCode as bigint)
		,ParietalPeritoneumCode = cast(ParietalPeritoneumCode as bigint)
		,ProcedureConsentCode = cast(ProcedureConsentCode as bigint)
		,SkinClosureCode = cast(SkinClosureCode as bigint)
		,SkinIncisionCode = cast(SkinIncisionCode as bigint)
		,SterilizedMethodCode = cast(SterilizedMethodCode as bigint)
		,ThromboembolicRiskCode = cast(ThromboembolicRiskCode as bigint)
		,ThromboprophylaxisPrescribedCode = cast(ThromboprophylaxisPrescribedCode as bigint)
		,UterineIncisionCode = cast(UterineIncisionCode as bigint)
		,UterusClosedCode = cast(UterusClosedCode as bigint)
		,UterusTubesOvariesCode = cast(UterusTubesOvariesCode as bigint)
		,VisceralPeritoneumCode = cast(VisceralPeritoneumCode as bigint)
		,ActionTaken = cast(nullif(ActionTaken, '') as nvarchar(2000))
		,AnalgesiaPerineum = cast(nullif(AnalgesiaPerineum, '') as nvarchar(2000))
		,Antibiotics = cast(nullif(Antibiotics, '') as nvarchar(2000))
		,AssistantSurgeon = cast(nullif(AssistantSurgeon, '') as nvarchar(2000))
		,BabiesNumber = cast(nullif(BabiesNumber, '') as nvarchar(2000))
		,BladderEmptied = cast(nullif(BladderEmptied, '') as nvarchar(2000))
		,BloodLossAtDelivery = cast(nullif(BloodLossAtDelivery, '') as nvarchar(2000))
		,CatheterInstructions = cast(nullif(CatheterInstructions, '') as nvarchar(2000))
		,CommentsPostOp = cast(nullif(CommentsPostOp, '') as nvarchar(2000))
		,Complications = cast(nullif(Complications, '') as nvarchar(2000))
		,DilatationBeforeLSCS = cast(nullif(DilatationBeforeLSCS, '') as nvarchar(2000))
		,DiscussedDT = cast(nullif(DiscussedDT, '') as nvarchar(2000))
		,DiscussionWithCons = cast(nullif(DiscussionWithCons, '') as nvarchar(2000))
		,Drainage = cast(nullif(Drainage, '') as nvarchar(2000))
		,DrainInstructions = cast(nullif(DrainInstructions, '') as nvarchar(2000))
		,Dressing = cast(nullif(Dressing, '') as nvarchar(2000))
		,DrugsPostDelivery = cast(nullif(DrugsPostDelivery, '') as nvarchar(2000))
		,ExamAfterRepair = cast(nullif(ExamAfterRepair, '') as nvarchar(2000))
		,FatStitch = cast(nullif(FatStitch, '') as nvarchar(2000))
		,FirstProcedure = cast(nullif(FirstProcedure, '') as nvarchar(2000))
		,LabiaSutured = cast(nullif(LabiaSutured, '') as nvarchar(2000))
		,LowerSegmentFormed = cast(nullif(LowerSegmentFormed, '') as nvarchar(2000))
		,LowerSegmentThin = cast(nullif(LowerSegmentThin, '') as nvarchar(2000))
		,NeedlesAccountedFor = cast(nullif(NeedlesAccountedFor, '') as nvarchar(2000))
		,ParietalPeritoneum = cast(nullif(ParietalPeritoneum, '') as nvarchar(2000))
		,PerinealMuscle = cast(nullif(PerinealMuscle, '') as nvarchar(2000))
		,PerineumVaginalTears = cast(nullif(PerineumVaginalTears, '') as nvarchar(2000))
		,PostVaginalWall = cast(nullif(PostVaginalWall, '') as nvarchar(2000))
		,ProcedureConsent = cast(nullif(ProcedureConsent, '') as nvarchar(2000))
		,SheathRepaired = cast(nullif(SheathRepaired, '') as nvarchar(2000))
		,SkinClosure = cast(nullif(SkinClosure, '') as nvarchar(2000))
		,SkinIncision = cast(nullif(SkinIncision, '') as nvarchar(2000))
		,SkinIncisionDT = cast(nullif(SkinIncisionDT, '') as nvarchar(2000))
		,SkinRepaired = cast(nullif(SkinRepaired, '') as nvarchar(2000))
		,SterilizationHistology = cast(nullif(SterilizationHistology, '') as nvarchar(2000))
		,SterilizedAtLSCS = cast(nullif(SterilizedAtLSCS, '') as nvarchar(2000))
		,SterilizedMethod = cast(nullif(SterilizedMethod, '') as nvarchar(2000))
		,SurgeonAtOperation = cast(nullif(SurgeonAtOperation, '') as nvarchar(2000))
		,ThromboembolicRisk = cast(nullif(ThromboembolicRisk, '') as nvarchar(2000))
		,ThromboprophylaxisPrescribed = cast(nullif(ThromboprophylaxisPrescribed, '') as nvarchar(2000))
		,UterineCavityChecked = cast(nullif(UterineCavityChecked, '') as nvarchar(2000))
		,UterineIncision = cast(nullif(UterineIncision, '') as nvarchar(2000))
		,UterusClosed = cast(nullif(UterusClosed, '') as nvarchar(2000))
		,UterusTubesOvaries = cast(nullif(UterusTubesOvaries, '') as nvarchar(2000))
		,VisceralPeritoneum = cast(nullif(VisceralPeritoneum, '') as nvarchar(2000))
		,InstrumentalDT = cast(nullif(InstrumentalDT, '') as nvarchar(2000))
		,ConsultantInvolvedOVD = cast(nullif(ConsultantInvolvedOVD, '') as nvarchar(2000))
		,ConsultantInvolvedOVDCode = cast(ConsultantInvolvedOVDCode as bigint)
		,OVDPlacePerformed = cast(nullif(OVDPlacePerformed, '') as nvarchar(2000))
		,OVDPlacePerformedCode = cast(OVDPlacePerformedCode as bigint)
	from
		(
		select
			UserID= DeliveryProcedure.UserID
			,PatientID= DeliveryProcedure.PatientID
			,PregnancyID= DeliveryProcedure.PregnancyID
			,BabiesNumberCode= DeliveryProcedure.BabiesNumber_Value
			,BladderEmptiedCode= DeliveryProcedure.BladderEmptied_Value
			,CommentsPostOpCode= DeliveryProcedure.CommentsPostOp_Value
			,ComplicationsCode= DeliveryProcedure.Complications_Value
			,DiscussionWithConsCode= DeliveryProcedure.DiscussionWithCons_Value
			,DrainageCode= DeliveryProcedure.Drainage_Value
			,FatStitchCode= DeliveryProcedure.FatStitch_Value
			,FirstProcedureCode= DeliveryProcedure.FirstProcedure_Value
			,ParietalPeritoneumCode= DeliveryProcedure.ParietalPeritoneum_Value
			,ProcedureConsentCode= DeliveryProcedure.ProcedureConsent_Value
			,SkinClosureCode= DeliveryProcedure.SkinClosure_Value
			,SkinIncisionCode= DeliveryProcedure.SkinIncision_Value
			,SterilizedMethodCode= DeliveryProcedure.SterilizedMethod_Value
			,ThromboembolicRiskCode= DeliveryProcedure.ThromboembolicRisk_Value
			,ThromboprophylaxisPrescribedCode= DeliveryProcedure.ThromboprophylaxisPrescribed_Value
			,UterineIncisionCode= DeliveryProcedure.UterineIncision_Value
			,UterusClosedCode= DeliveryProcedure.UterusClosed_Value
			,UterusTubesOvariesCode= DeliveryProcedure.UterusTubesOvaries_Value
			,VisceralPeritoneumCode= DeliveryProcedure.VisceralPeritoneum_Value
			,ActionTaken= DeliveryProcedure.ActionTaken
			,AnalgesiaPerineum= DeliveryProcedure.AnalgesiaPerineum
			,Antibiotics= DeliveryProcedure.Antibiotics
			,AssistantSurgeon= DeliveryProcedure.AssistantSurgeon
			,BabiesNumber= DeliveryProcedure.BabiesNumber
			,BladderEmptied= DeliveryProcedure.BladderEmptied
			,BloodLossAtDelivery= DeliveryProcedure.BloodLossAtDelivery
			,CatheterInstructions= DeliveryProcedure.CatheterInstructions
			,CommentsPostOp= DeliveryProcedure.CommentsPostOp
			,Complications= DeliveryProcedure.Complications
			,DilatationBeforeLSCS= DeliveryProcedure.DilatationBeforeLSCS
			,DiscussedDT= DeliveryProcedure.DiscussedDT
			,DiscussionWithCons= DeliveryProcedure.DiscussionWithCons
			,Drainage= DeliveryProcedure.Drainage
			,DrainInstructions= DeliveryProcedure.DrainInstructions
			,Dressing= DeliveryProcedure.Dressing
			,DrugsPostDelivery= DeliveryProcedure.DrugsPostDelivery
			,ExamAfterRepair= DeliveryProcedure.ExamAfterRepair
			,FatStitch= DeliveryProcedure.FatStitch
			,FirstProcedure= DeliveryProcedure.FirstProcedure
			,LabiaSutured= DeliveryProcedure.LabiaSutured
			,LowerSegmentFormed= DeliveryProcedure.LowerSegmentFormed
			,LowerSegmentThin= DeliveryProcedure.LowerSegmentThin
			,NeedlesAccountedFor= DeliveryProcedure.NeedlesAccountedFor
			,ParietalPeritoneum= DeliveryProcedure.ParietalPeritoneum
			,PerinealMuscle= DeliveryProcedure.PerinealMuscle
			,PerineumVaginalTears= DeliveryProcedure.PerineumVaginalTears
			,PostVaginalWall= DeliveryProcedure.PostVaginalWall
			,ProcedureConsent= DeliveryProcedure.ProcedureConsent
			,SheathRepaired= DeliveryProcedure.SheathRepaired
			,SkinClosure= DeliveryProcedure.SkinClosure
			,SkinIncision= DeliveryProcedure.SkinIncision
			,SkinIncisionDT= DeliveryProcedure.SkinIncisionDT
			,SkinRepaired= DeliveryProcedure.SkinRepaired
			,SterilizationHistology= DeliveryProcedure.SterilizationHistology
			,SterilizedAtLSCS= DeliveryProcedure.SterilizedAtLSCS
			,SterilizedMethod= DeliveryProcedure.SterilizedMethod
			,SurgeonAtOperation= DeliveryProcedure.SurgeonAtOperation
			,ThromboembolicRisk= DeliveryProcedure.ThromboembolicRisk
			,ThromboprophylaxisPrescribed= DeliveryProcedure.ThromboprophylaxisPrescribed
			,UterineCavityChecked= DeliveryProcedure.UterineCavityChecked
			,UterineIncision= DeliveryProcedure.UterineIncision
			,UterusClosed= DeliveryProcedure.UterusClosed
			,UterusTubesOvaries= DeliveryProcedure.UterusTubesOvaries
			,VisceralPeritoneum= DeliveryProcedure.VisceralPeritoneum
			,InstrumentalDT= DeliveryProcedure.InstrumentalDT
			,ConsultantInvolvedOVD= DeliveryProcedure.ConsultantInvolvedOVD
			,ConsultantInvolvedOVDCode= DeliveryProcedure.ConsultantInvolvedOVD_Value
			,OVDPlacePerformed= DeliveryProcedure.OVDPlacePerformed
			,OVDPlacePerformedCode= DeliveryProcedure.OVDPlacePerformed_Value
		from
			E3.dbo.tblReport_Delivery_Procedures_5 DeliveryProcedure

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3DeliveryProcedure on #TLoadE3DeliveryProcedure
	(
	PregnancyID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	E3.DeliveryProcedure target
using
	(
	select
		*
	from
		#TLoadE3DeliveryProcedure
	
	) source
	on	source.PregnancyID = target.PregnancyID

when not matched by source
then
	delete

when not matched
then
	insert
		(
		UserID
		,PatientID
		,PregnancyID
		,BabiesNumberCode
		,BladderEmptiedCode
		,CommentsPostOpCode
		,ComplicationsCode
		,DiscussionWithConsCode
		,DrainageCode
		,FatStitchCode
		,FirstProcedureCode
		,ParietalPeritoneumCode
		,ProcedureConsentCode
		,SkinClosureCode
		,SkinIncisionCode
		,SterilizedMethodCode
		,ThromboembolicRiskCode
		,ThromboprophylaxisPrescribedCode
		,UterineIncisionCode
		,UterusClosedCode
		,UterusTubesOvariesCode
		,VisceralPeritoneumCode
		,ActionTaken
		,AnalgesiaPerineum
		,Antibiotics
		,AssistantSurgeon
		,BabiesNumber
		,BladderEmptied
		,BloodLossAtDelivery
		,CatheterInstructions
		,CommentsPostOp
		,Complications
		,DilatationBeforeLSCS
		,DiscussedDT
		,DiscussionWithCons
		,Drainage
		,DrainInstructions
		,Dressing
		,DrugsPostDelivery
		,ExamAfterRepair
		,FatStitch
		,FirstProcedure
		,LabiaSutured
		,LowerSegmentFormed
		,LowerSegmentThin
		,NeedlesAccountedFor
		,ParietalPeritoneum
		,PerinealMuscle
		,PerineumVaginalTears
		,PostVaginalWall
		,ProcedureConsent
		,SheathRepaired
		,SkinClosure
		,SkinIncision
		,SkinIncisionDT
		,SkinRepaired
		,SterilizationHistology
		,SterilizedAtLSCS
		,SterilizedMethod
		,SurgeonAtOperation
		,ThromboembolicRisk
		,ThromboprophylaxisPrescribed
		,UterineCavityChecked
		,UterineIncision
		,UterusClosed
		,UterusTubesOvaries
		,VisceralPeritoneum
		,InstrumentalDT
		,ConsultantInvolvedOVD
		,ConsultantInvolvedOVDCode
		,OVDPlacePerformed
		,OVDPlacePerformedCode

		,Created
		,ByWhom
		)
	values
		(
		source.UserID
		,source.PatientID
		,source.PregnancyID
		,source.BabiesNumberCode
		,source.BladderEmptiedCode
		,source.CommentsPostOpCode
		,source.ComplicationsCode
		,source.DiscussionWithConsCode
		,source.DrainageCode
		,source.FatStitchCode
		,source.FirstProcedureCode
		,source.ParietalPeritoneumCode
		,source.ProcedureConsentCode
		,source.SkinClosureCode
		,source.SkinIncisionCode
		,source.SterilizedMethodCode
		,source.ThromboembolicRiskCode
		,source.ThromboprophylaxisPrescribedCode
		,source.UterineIncisionCode
		,source.UterusClosedCode
		,source.UterusTubesOvariesCode
		,source.VisceralPeritoneumCode
		,source.ActionTaken
		,source.AnalgesiaPerineum
		,source.Antibiotics
		,source.AssistantSurgeon
		,source.BabiesNumber
		,source.BladderEmptied
		,source.BloodLossAtDelivery
		,source.CatheterInstructions
		,source.CommentsPostOp
		,source.Complications
		,source.DilatationBeforeLSCS
		,source.DiscussedDT
		,source.DiscussionWithCons
		,source.Drainage
		,source.DrainInstructions
		,source.Dressing
		,source.DrugsPostDelivery
		,source.ExamAfterRepair
		,source.FatStitch
		,source.FirstProcedure
		,source.LabiaSutured
		,source.LowerSegmentFormed
		,source.LowerSegmentThin
		,source.NeedlesAccountedFor
		,source.ParietalPeritoneum
		,source.PerinealMuscle
		,source.PerineumVaginalTears
		,source.PostVaginalWall
		,source.ProcedureConsent
		,source.SheathRepaired
		,source.SkinClosure
		,source.SkinIncision
		,source.SkinIncisionDT
		,source.SkinRepaired
		,source.SterilizationHistology
		,source.SterilizedAtLSCS
		,source.SterilizedMethod
		,source.SurgeonAtOperation
		,source.ThromboembolicRisk
		,source.ThromboprophylaxisPrescribed
		,source.UterineCavityChecked
		,source.UterineIncision
		,source.UterusClosed
		,source.UterusTubesOvaries
		,source.VisceralPeritoneum
		,source.InstrumentalDT
		,source.ConsultantInvolvedOVD
		,source.ConsultantInvolvedOVDCode
		,source.OVDPlacePerformed
		,source.OVDPlacePerformedCode

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.UserID
		,target.PatientID
		,target.BabiesNumberCode
		,target.BladderEmptiedCode
		,target.CommentsPostOpCode
		,target.ComplicationsCode
		,target.DiscussionWithConsCode
		,target.DrainageCode
		,target.FatStitchCode
		,target.FirstProcedureCode
		,target.ParietalPeritoneumCode
		,target.ProcedureConsentCode
		,target.SkinClosureCode
		,target.SkinIncisionCode
		,target.SterilizedMethodCode
		,target.ThromboembolicRiskCode
		,target.ThromboprophylaxisPrescribedCode
		,target.UterineIncisionCode
		,target.UterusClosedCode
		,target.UterusTubesOvariesCode
		,target.VisceralPeritoneumCode
		,target.ActionTaken
		,target.AnalgesiaPerineum
		,target.Antibiotics
		,target.AssistantSurgeon
		,target.BabiesNumber
		,target.BladderEmptied
		,target.BloodLossAtDelivery
		,target.CatheterInstructions
		,target.CommentsPostOp
		,target.Complications
		,target.DilatationBeforeLSCS
		,target.DiscussedDT
		,target.DiscussionWithCons
		,target.Drainage
		,target.DrainInstructions
		,target.Dressing
		,target.DrugsPostDelivery
		,target.ExamAfterRepair
		,target.FatStitch
		,target.FirstProcedure
		,target.LabiaSutured
		,target.LowerSegmentFormed
		,target.LowerSegmentThin
		,target.NeedlesAccountedFor
		,target.ParietalPeritoneum
		,target.PerinealMuscle
		,target.PerineumVaginalTears
		,target.PostVaginalWall
		,target.ProcedureConsent
		,target.SheathRepaired
		,target.SkinClosure
		,target.SkinIncision
		,target.SkinIncisionDT
		,target.SkinRepaired
		,target.SterilizationHistology
		,target.SterilizedAtLSCS
		,target.SterilizedMethod
		,target.SurgeonAtOperation
		,target.ThromboembolicRisk
		,target.ThromboprophylaxisPrescribed
		,target.UterineCavityChecked
		,target.UterineIncision
		,target.UterusClosed
		,target.UterusTubesOvaries
		,target.VisceralPeritoneum
		,target.InstrumentalDT
		,target.ConsultantInvolvedOVD
		,target.ConsultantInvolvedOVDCode
		,target.OVDPlacePerformed
		,target.OVDPlacePerformedCode
		)

then
	update
	set
		target.UserID = source.UserID
		,target.PatientID = source.PatientID
		,target.BabiesNumberCode = source.BabiesNumberCode
		,target.BladderEmptiedCode = source.BladderEmptiedCode
		,target.CommentsPostOpCode = source.CommentsPostOpCode
		,target.ComplicationsCode = source.ComplicationsCode
		,target.DiscussionWithConsCode = source.DiscussionWithConsCode
		,target.DrainageCode = source.DrainageCode
		,target.FatStitchCode = source.FatStitchCode
		,target.FirstProcedureCode = source.FirstProcedureCode
		,target.ParietalPeritoneumCode = source.ParietalPeritoneumCode
		,target.ProcedureConsentCode = source.ProcedureConsentCode
		,target.SkinClosureCode = source.SkinClosureCode
		,target.SkinIncisionCode = source.SkinIncisionCode
		,target.SterilizedMethodCode = source.SterilizedMethodCode
		,target.ThromboembolicRiskCode = source.ThromboembolicRiskCode
		,target.ThromboprophylaxisPrescribedCode = source.ThromboprophylaxisPrescribedCode
		,target.UterineIncisionCode = source.UterineIncisionCode
		,target.UterusClosedCode = source.UterusClosedCode
		,target.UterusTubesOvariesCode = source.UterusTubesOvariesCode
		,target.VisceralPeritoneumCode = source.VisceralPeritoneumCode
		,target.ActionTaken = source.ActionTaken
		,target.AnalgesiaPerineum = source.AnalgesiaPerineum
		,target.Antibiotics = source.Antibiotics
		,target.AssistantSurgeon = source.AssistantSurgeon
		,target.BabiesNumber = source.BabiesNumber
		,target.BladderEmptied = source.BladderEmptied
		,target.BloodLossAtDelivery = source.BloodLossAtDelivery
		,target.CatheterInstructions = source.CatheterInstructions
		,target.CommentsPostOp = source.CommentsPostOp
		,target.Complications = source.Complications
		,target.DilatationBeforeLSCS = source.DilatationBeforeLSCS
		,target.DiscussedDT = source.DiscussedDT
		,target.DiscussionWithCons = source.DiscussionWithCons
		,target.Drainage = source.Drainage
		,target.DrainInstructions = source.DrainInstructions
		,target.Dressing = source.Dressing
		,target.DrugsPostDelivery = source.DrugsPostDelivery
		,target.ExamAfterRepair = source.ExamAfterRepair
		,target.FatStitch = source.FatStitch
		,target.FirstProcedure = source.FirstProcedure
		,target.LabiaSutured = source.LabiaSutured
		,target.LowerSegmentFormed = source.LowerSegmentFormed
		,target.LowerSegmentThin = source.LowerSegmentThin
		,target.NeedlesAccountedFor = source.NeedlesAccountedFor
		,target.ParietalPeritoneum = source.ParietalPeritoneum
		,target.PerinealMuscle = source.PerinealMuscle
		,target.PerineumVaginalTears = source.PerineumVaginalTears
		,target.PostVaginalWall = source.PostVaginalWall
		,target.ProcedureConsent = source.ProcedureConsent
		,target.SheathRepaired = source.SheathRepaired
		,target.SkinClosure = source.SkinClosure
		,target.SkinIncision = source.SkinIncision
		,target.SkinIncisionDT = source.SkinIncisionDT
		,target.SkinRepaired = source.SkinRepaired
		,target.SterilizationHistology = source.SterilizationHistology
		,target.SterilizedAtLSCS = source.SterilizedAtLSCS
		,target.SterilizedMethod = source.SterilizedMethod
		,target.SurgeonAtOperation = source.SurgeonAtOperation
		,target.ThromboembolicRisk = source.ThromboembolicRisk
		,target.ThromboprophylaxisPrescribed = source.ThromboprophylaxisPrescribed
		,target.UterineCavityChecked = source.UterineCavityChecked
		,target.UterineIncision = source.UterineIncision
		,target.UterusClosed = source.UterusClosed
		,target.UterusTubesOvaries = source.UterusTubesOvaries
		,target.VisceralPeritoneum = source.VisceralPeritoneum
		,target.InstrumentalDT = source.InstrumentalDT
		,target.ConsultantInvolvedOVD = source.ConsultantInvolvedOVD
		,target.ConsultantInvolvedOVDCode = source.ConsultantInvolvedOVDCode
		,target.OVDPlacePerformed = source.OVDPlacePerformed
		,target.OVDPlacePerformedCode = source.OVDPlacePerformedCode

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime