﻿CREATE procedure [E3].[LoadEthnicOrigin] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			EthnicOrigin
			,PASCode
			,EKRecord
			,GUID
			,UserID
			,LastUpdatedDateTime
			)
into
	#TLoadE3EthnicOrigin
from
	(
	select
		EthnicOriginID
		,EthnicOrigin = cast(nullif(EthnicOrigin, '') as varchar(50))
		,PASCode = cast(nullif(PASCode, '') as varchar(30))
		,EKRecord = cast(EKRecord as bit)
		,GUID = cast(GUID as uniqueidentifier)
		,UserID = cast(UserID as int)
		,LastUpdatedDateTime = cast(LastUpdatedDateTime as smalldatetime)
	from
		(
		select
			EthnicOriginID= EthnicOrigin.EthnicOriginID
			,EthnicOrigin= EthnicOrigin.EthnicOrigin
			,PASCode= EthnicOrigin.PASCode
			,IsDeleted = EthnicOrigin.Deleted
			,EKRecord= EthnicOrigin.EK_Record
			,GUID= EthnicOrigin.GUID
			,UserID= EthnicOrigin.UserID
			,LastUpdatedDateTime = EthnicOrigin.LastUpdated
		from
			E3.dbo.EthnicOrigin EthnicOrigin
		where
			EthnicOrigin.Deleted <> 1

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3EthnicOrigin on #TLoadE3EthnicOrigin
	(
	EthnicOriginID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	E3.EthnicOrigin target
using
	(
	select
		*
	from
		#TLoadE3EthnicOrigin
	
	) source
	on	source.EthnicOriginID = target.EthnicOriginID

when not matched by source
then
	delete

when not matched
then
	insert
		(
		EthnicOriginID
		,EthnicOrigin
		,PASCode
		,EKRecord
		,GUID
		,UserID
		,LastUpdatedDateTime

		,Created
		,ByWhom
		)
	values
		(
		source.EthnicOriginID
		,source.EthnicOrigin
		,source.PASCode
		,source.EKRecord
		,source.GUID
		,source.UserID
		,source.LastUpdatedDateTime

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.EthnicOrigin
		,target.PASCode
		,target.EKRecord
		,target.GUID
		,target.UserID
		,target.LastUpdatedDateTime
		)

then
	update
	set
		target.EthnicOrigin = source.EthnicOrigin
		,target.PASCode = source.PASCode
		,target.EKRecord = source.EKRecord
		,target.GUID = source.GUID
		,target.UserID = source.UserID
		,target.LastUpdatedDateTime = source.LastUpdatedDateTime

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime