﻿CREATE procedure [E3].[LoadGPPractice] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			PracticeCode
			,EKRecord
			,GUID
			,UserID
			,LastUpdatedDateTime
			,PCTID
			)
into
	#TLoadE3GPPractice
from
	(
	select
		GPPracticeID
		,PracticeCode = cast(nullif(PracticeCode, '') as nvarchar(60))
		,EKRecord = cast(EKRecord as bit)
		,GUID = cast(GUID as uniqueidentifier)
		,UserID = cast(UserID as int)
		,LastUpdatedDateTime = cast(LastUpdatedDateTime as smalldatetime)
		,PCTID = cast(PCTID as int)
	from
		(
		select
			GPPracticeID = GPPractice.GPPracticeID
			,PracticeCode = GPPractice.PracticeCode
			,EKRecord = GPPractice.EK_Record
			,GUID = GPPractice.GUID
			,UserID= GPPractice.UserID
			,LastUpdatedDateTime = GPPractice.LastUpdated
			,PCTID= GPPractice.PCTID
		from
			E3.dbo.GPPractice GPPractice
		where
			GPPractice.Deleted <> 1

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3GPPractice on #TLoadE3GPPractice
	(
	GPPracticeID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	E3.GPPractice target
using
	(
	select
		*
	from
		#TLoadE3GPPractice
	
	) source
	on	source.GPPracticeID = target.GPPracticeID

when not matched by source
then
	delete

when not matched
then
	insert
		(
		GPPracticeID
		,PracticeCode
		,EKRecord
		,GUID
		,UserID
		,LastUpdatedDateTime
		,PCTID

		,Created
		,ByWhom
		)
	values
		(
		source.GPPracticeID
		,source.PracticeCode
		,source.EKRecord
		,source.GUID
		,source.UserID
		,source.LastUpdatedDateTime
		,source.PCTID

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.PracticeCode
		,target.EKRecord
		,target.GUID
		,target.UserID
		,target.LastUpdatedDateTime
		,target.PCTID
		)

then
	update
	set
		target.PracticeCode = source.PracticeCode
		,target.EKRecord = source.EKRecord
		,target.GUID = source.GUID
		,target.UserID = source.UserID
		,target.LastUpdatedDateTime = source.LastUpdatedDateTime
		,target.PCTID = source.PCTID

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime