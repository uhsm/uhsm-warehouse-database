﻿CREATE procedure [E3].[LoadGender] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			Gender
			,PASCode
			,EKRecord
			,GUID
			,UserID
			,LastUpdatedDateTime
			)
into
	#TLoadE3Gender
from
	(
	select
		GenderID
		,Gender = cast(nullif(Gender, '') as varchar(50))
		,PASCode = cast(nullif(PASCode, '') as varchar(30))
		,EKRecord = cast(EKRecord as bit)
		,GUID = cast(GUID as uniqueidentifier)
		,UserID = cast(UserID as int)
		,LastUpdatedDateTime = cast(LastUpdatedDateTime as smalldatetime)
	from
		(
		select
			GenderID= Gender.GenderID
			,Gender= Gender.Gender
			,PASCode= Gender.PASCode
			,EKRecord= Gender.EK_Record
			,GUID= Gender.GUID
			,UserID= Gender.UserID
			,LastUpdatedDateTime = Gender.LastUpdated
		from
			E3.dbo.Gender Gender
		where
			Gender.Deleted <> 1

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3Gender on #TLoadE3Gender
	(
	GenderID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	E3.Gender target
using
	(
	select
		*
	from
		#TLoadE3Gender
	
	) source
	on	source.GenderID = target.GenderID

when not matched by source
then
	delete

when not matched
then
	insert
		(
		GenderID
		,Gender
		,PASCode
		,EKRecord
		,GUID
		,UserID
		,LastUpdatedDateTime

		,Created
		,ByWhom
		)
	values
		(
		source.GenderID
		,source.Gender
		,source.PASCode
		,source.EKRecord
		,source.GUID
		,source.UserID
		,source.LastUpdatedDateTime

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.Gender
		,target.PASCode
		,target.EKRecord
		,target.GUID
		,target.UserID
		,target.LastUpdatedDateTime
		)

then
	update
	set
		target.Gender = source.Gender
		,target.PASCode = source.PASCode
		,target.EKRecord = source.EKRecord
		,target.GUID = source.GUID
		,target.UserID = source.UserID
		,target.LastUpdatedDateTime = source.LastUpdatedDateTime

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime