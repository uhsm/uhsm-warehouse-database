﻿CREATE procedure [E3].[LoadLabourAndDelivery] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 UserID
			,PatientID
			,AnaesthsiaAtCaesareanCode
			,AnalgesiaCode
			,AnalgesiaDeliveryCode
			,AntibioticCoverCSCode
			,CervixDilatedDTCode
			,ComplicationsCode
			,CSDelayInProcedureCode
			,CSGradeCode
			,DilatationEpiduralCode
			,DilatationSyntocinonCode
			,DurationSupportCode
			,ExamVaginalCode
			,LabourAugmentedCode
			,LabourDrugsCode
			,LabourSupportCode
			,Midwife1LeftDTCode
			,Midwife1stDTCode
			,Midwife2ArrivalDTCode
			,Midwife2LeftDTCode
			,NumberHomeVisitsCode
			,ProblemsMaternalCode
			,ReasonCSOtherCode
			,ReasonCSPrimaryCode
			,ReasonCSSecondaryCode
			,SyntocinonStartDTCode
			,ThromboembolicRiskCode
			,ThromboprophylaxisPrescribedCode
			,TransferHospitalCode
			,TransferReasonCode
			,AdmissionCTG
			,AlternativePainRelief
			,AnaesthsiaAtCaesarean
			,Analgesia
			,AnalgesiaDelivery
			,AntibioticCoverCS
			,CervixDilatedDT
			,Complications
			,CSDelayInProcedure
			,CSGrade
			,CSSecondStage
			,DilatationEpidural
			,DilatationSyntocinon
			,DurationSupport
			,ExamVaginal
			,LabourAugmented
			,LabourDrugs
			,LabourSupport
			,LSCSDecisionDT
			,Midwife1LeftDT
			,Midwife1stDT
			,Midwife2ArrivalDT
			,Midwife2LeftDT
			,NumberHomeVisits
			,ProblemsMaternal
			,PushingCommenceDT
			,ReasonCSOther
			,ReasonCSPrimary
			,ReasonCSSecondary
			,SyntocinonStartDT
			,ThromboembolicRisk
			,ThromboprophylaxisPrescribed
			,TransferHospital
			,TransferReason
			,AmbulanceCalledDT
			,AmbulanceArrivedDT
			,ReasonAnalgesiaDelivery
			,ReasonAnalgesiaDeliveryCode
			,AnaestheticProblem
			,AnaestheticProblemCode
			,SkinIncisionDT
			,CSDecisionMadeBy
			,CSDecisionMadeByCode
			,DiscussionWithCons
			,DiscussionWithConsCode
			,ArriveAtHospitalDT
			,AlternativePainReliefCode
			,MOHPersonnelInformed
			,MOHCause
			,MOHManagement
			,CSSwabCount
			,CSSwabCountCode
			,MLUAdmission
			,MLUAdmissionCode
			,MLUAdmissionDT
			,AbaestheticCriticalIncidentDT
			,AnaestheticCIDTCode
			,AnaestheticCIDT
			,AnaestheticCriticalIncident
			,AnaestheticCriticalIncidentCode
			)
into
	#TLoadE3LabourAndDelivery
from
	(
	select
		UserID = cast(UserID as int)
		,PatientID = cast(PatientID as bigint)
		,PregnancyID = cast(PregnancyID as bigint)
		,AnaesthsiaAtCaesareanCode = cast(AnaesthsiaAtCaesareanCode as bigint)
		,AnalgesiaCode = cast(AnalgesiaCode as bigint)
		,AnalgesiaDeliveryCode = cast(AnalgesiaDeliveryCode as bigint)
		,AntibioticCoverCSCode = cast(AntibioticCoverCSCode as bigint)
		,CervixDilatedDTCode = cast(CervixDilatedDTCode as bigint)
		,ComplicationsCode = cast(ComplicationsCode as bigint)
		,CSDelayInProcedureCode = cast(CSDelayInProcedureCode as bigint)
		,CSGradeCode = cast(CSGradeCode as bigint)
		,DilatationEpiduralCode = cast(DilatationEpiduralCode as bigint)
		,DilatationSyntocinonCode = cast(DilatationSyntocinonCode as bigint)
		,DurationSupportCode = cast(DurationSupportCode as bigint)
		,ExamVaginalCode = cast(ExamVaginalCode as bigint)
		,LabourAugmentedCode = cast(LabourAugmentedCode as bigint)
		,LabourDrugsCode = cast(LabourDrugsCode as bigint)
		,LabourSupportCode = cast(LabourSupportCode as bigint)
		,Midwife1LeftDTCode = cast(Midwife1LeftDTCode as bigint)
		,Midwife1stDTCode = cast(Midwife1stDTCode as bigint)
		,Midwife2ArrivalDTCode = cast(Midwife2ArrivalDTCode as bigint)
		,Midwife2LeftDTCode = cast(Midwife2LeftDTCode as bigint)
		,NumberHomeVisitsCode = cast(NumberHomeVisitsCode as bigint)
		,ProblemsMaternalCode = cast(ProblemsMaternalCode as bigint)
		,ReasonCSOtherCode = cast(ReasonCSOtherCode as bigint)
		,ReasonCSPrimaryCode = cast(ReasonCSPrimaryCode as bigint)
		,ReasonCSSecondaryCode = cast(ReasonCSSecondaryCode as bigint)
		,SyntocinonStartDTCode = cast(SyntocinonStartDTCode as bigint)
		,ThromboembolicRiskCode = cast(ThromboembolicRiskCode as bigint)
		,ThromboprophylaxisPrescribedCode = cast(ThromboprophylaxisPrescribedCode as bigint)
		,TransferHospitalCode = cast(TransferHospitalCode as bigint)
		,TransferReasonCode = cast(TransferReasonCode as bigint)
		,AdmissionCTG = cast(nullif(AdmissionCTG, '') as nvarchar(2000))
		,AlternativePainRelief = cast(nullif(AlternativePainRelief, '') as nvarchar(2000))
		,AnaesthsiaAtCaesarean = cast(nullif(AnaesthsiaAtCaesarean, '') as nvarchar(2000))
		,Analgesia = cast(nullif(Analgesia, '') as nvarchar(2000))
		,AnalgesiaDelivery = cast(nullif(AnalgesiaDelivery, '') as nvarchar(2000))
		,AntibioticCoverCS = cast(nullif(AntibioticCoverCS, '') as nvarchar(2000))
		,CervixDilatedDT = cast(nullif(CervixDilatedDT, '') as nvarchar(2000))
		,Complications = cast(nullif(Complications, '') as nvarchar(2000))
		,CSDelayInProcedure = cast(nullif(CSDelayInProcedure, '') as nvarchar(2000))
		,CSGrade = cast(nullif(CSGrade, '') as nvarchar(2000))
		,CSSecondStage = cast(nullif(CSSecondStage, '') as nvarchar(2000))
		,DilatationEpidural = cast(nullif(DilatationEpidural, '') as nvarchar(2000))
		,DilatationSyntocinon = cast(nullif(DilatationSyntocinon, '') as nvarchar(2000))
		,DurationSupport = cast(nullif(DurationSupport, '') as nvarchar(2000))
		,ExamVaginal = cast(nullif(ExamVaginal, '') as nvarchar(2000))
		,LabourAugmented = cast(nullif(LabourAugmented, '') as nvarchar(2000))
		,LabourDrugs = cast(nullif(LabourDrugs, '') as nvarchar(2000))
		,LabourSupport = cast(nullif(LabourSupport, '') as nvarchar(2000))
		,LSCSDecisionDT = cast(nullif(LSCSDecisionDT, '') as nvarchar(2000))
		,Midwife1LeftDT = cast(nullif(Midwife1LeftDT, '') as nvarchar(2000))
		,Midwife1stDT = cast(nullif(Midwife1stDT, '') as nvarchar(2000))
		,Midwife2ArrivalDT = cast(nullif(Midwife2ArrivalDT, '') as nvarchar(2000))
		,Midwife2LeftDT = cast(nullif(Midwife2LeftDT, '') as nvarchar(2000))
		,NumberHomeVisits = cast(nullif(NumberHomeVisits, '') as nvarchar(2000))
		,ProblemsMaternal = cast(nullif(ProblemsMaternal, '') as nvarchar(2000))
		,PushingCommenceDT = cast(nullif(PushingCommenceDT, '') as nvarchar(2000))
		,ReasonCSOther = cast(nullif(ReasonCSOther, '') as nvarchar(2000))
		,ReasonCSPrimary = cast(nullif(ReasonCSPrimary, '') as nvarchar(2000))
		,ReasonCSSecondary = cast(nullif(ReasonCSSecondary, '') as nvarchar(2000))
		,SyntocinonStartDT = cast(nullif(SyntocinonStartDT, '') as nvarchar(2000))
		,ThromboembolicRisk = cast(nullif(ThromboembolicRisk, '') as nvarchar(2000))
		,ThromboprophylaxisPrescribed = cast(nullif(ThromboprophylaxisPrescribed, '') as nvarchar(2000))
		,TransferHospital = cast(nullif(TransferHospital, '') as nvarchar(2000))
		,TransferReason = cast(nullif(TransferReason, '') as nvarchar(2000))
		,AmbulanceCalledDT = cast(nullif(AmbulanceCalledDT, '') as nvarchar(2000))
		,AmbulanceArrivedDT = cast(nullif(AmbulanceArrivedDT, '') as nvarchar(2000))
		,ReasonAnalgesiaDelivery = cast(nullif(ReasonAnalgesiaDelivery, '') as nvarchar(2000))
		,ReasonAnalgesiaDeliveryCode = cast(ReasonAnalgesiaDeliveryCode as bigint)
		,AnaestheticProblem = cast(nullif(AnaestheticProblem, '') as nvarchar(2000))
		,AnaestheticProblemCode = cast(AnaestheticProblemCode as bigint)
		,SkinIncisionDT = cast(nullif(SkinIncisionDT, '') as nvarchar(2000))
		,CSDecisionMadeBy = cast(nullif(CSDecisionMadeBy, '') as nvarchar(2000))
		,CSDecisionMadeByCode = cast(CSDecisionMadeByCode as bigint)
		,DiscussionWithCons = cast(nullif(DiscussionWithCons, '') as nvarchar(2000))
		,DiscussionWithConsCode = cast(DiscussionWithConsCode as bigint)
		,ArriveAtHospitalDT = cast(nullif(ArriveAtHospitalDT, '') as nvarchar(2000))
		,AlternativePainReliefCode = cast(AlternativePainReliefCode as bigint)
		,MOHPersonnelInformed = cast(nullif(MOHPersonnelInformed, '') as nvarchar(2000))
		,MOHCause = cast(nullif(MOHCause, '') as nvarchar(2000))
		,MOHManagement = cast(nullif(MOHManagement, '') as nvarchar(2000))
		,CSSwabCount = cast(nullif(CSSwabCount, '') as nvarchar(2000))
		,CSSwabCountCode = cast(CSSwabCountCode as bigint)
		,MLUAdmission = cast(nullif(MLUAdmission, '') as nvarchar(2000))
		,MLUAdmissionCode = cast(MLUAdmissionCode as bigint)
		,MLUAdmissionDT = cast(nullif(MLUAdmissionDT, '') as nvarchar(2000))
		,AbaestheticCriticalIncidentDT = cast(nullif(AbaestheticCriticalIncidentDT, '') as nvarchar(2000))
		,AnaestheticCIDTCode = cast(AnaestheticCIDTCode as bigint)
		,AnaestheticCIDT = cast(nullif(AnaestheticCIDT, '') as nvarchar(2000))
		,AnaestheticCriticalIncident = cast(nullif(AnaestheticCriticalIncident, '') as nvarchar(2000))
		,AnaestheticCriticalIncidentCode = cast(AnaestheticCriticalIncidentCode as bigint)
	from
		(
		select
			UserID = LabourAndDelivery.UserID
			,PatientID = LabourAndDelivery.PatientID
			,PregnancyID = LabourAndDelivery.PregnancyID
			,AnaesthsiaAtCaesareanCode = LabourAndDelivery.AnaesthsiaAtCaesarean_Value
			,AnalgesiaCode = LabourAndDelivery.Analgesia_Value
			,AnalgesiaDeliveryCode = LabourAndDelivery.AnalgesiaDelivery_Value
			,AntibioticCoverCSCode = LabourAndDelivery.AntibioticCoverCS_Value
			,CervixDilatedDTCode = LabourAndDelivery.CervixDilatedDT_Value
			,ComplicationsCode = LabourAndDelivery.Complications_Value
			,CSDelayInProcedureCode = LabourAndDelivery.CSDelayInProcedure_Value
			,CSGradeCode = LabourAndDelivery.CSGrade_Value
			,DilatationEpiduralCode = LabourAndDelivery.DilatationEpidural_Value
			,DilatationSyntocinonCode = LabourAndDelivery.DilatationSyntocinon_Value
			,DurationSupportCode = LabourAndDelivery.DurationSupport_Value
			,ExamVaginalCode = LabourAndDelivery.ExamVaginal_Value
			,LabourAugmentedCode = LabourAndDelivery.LabourAugmented_Value
			,LabourDrugsCode = LabourAndDelivery.LabourDrugs_Value
			,LabourSupportCode = LabourAndDelivery.LabourSupport_Value
			,Midwife1LeftDTCode = LabourAndDelivery.Midwife1LeftDT_Value
			,Midwife1stDTCode = LabourAndDelivery.Midwife1stDT_Value
			,Midwife2ArrivalDTCode = LabourAndDelivery.Midwife2ArrivalDT_Value
			,Midwife2LeftDTCode = LabourAndDelivery.Midwife2LeftDT_Value
			,NumberHomeVisitsCode = LabourAndDelivery.NumberHomeVisits_Value
			,ProblemsMaternalCode = LabourAndDelivery.ProblemsMaternal_Value
			,ReasonCSOtherCode = LabourAndDelivery.ReasonCSOther_Value
			,ReasonCSPrimaryCode = LabourAndDelivery.ReasonCSPrimary_Value
			,ReasonCSSecondaryCode = LabourAndDelivery.ReasonCSSecondary_Value
			,SyntocinonStartDTCode = LabourAndDelivery.SyntocinonStartDT_Value
			,ThromboembolicRiskCode = LabourAndDelivery.ThromboembolicRisk_Value
			,ThromboprophylaxisPrescribedCode = LabourAndDelivery.ThromboprophylaxisPrescribed_Value
			,TransferHospitalCode = LabourAndDelivery.TransferHospital_Value
			,TransferReasonCode = LabourAndDelivery.TransferReason_Value
			,AdmissionCTG = LabourAndDelivery.AdmissionCTG
			,AlternativePainRelief = LabourAndDelivery.AlternativePainRelief
			,AnaesthsiaAtCaesarean = LabourAndDelivery.AnaesthsiaAtCaesarean
			,Analgesia = LabourAndDelivery.Analgesia
			,AnalgesiaDelivery = LabourAndDelivery.AnalgesiaDelivery
			,AntibioticCoverCS = LabourAndDelivery.AntibioticCoverCS
			,CervixDilatedDT = LabourAndDelivery.CervixDilatedDT
			,Complications = LabourAndDelivery.Complications
			,CSDelayInProcedure = LabourAndDelivery.CSDelayInProcedure
			,CSGrade = LabourAndDelivery.CSGrade
			,CSSecondStage = LabourAndDelivery.CSSecondStage
			,DilatationEpidural = LabourAndDelivery.DilatationEpidural
			,DilatationSyntocinon = LabourAndDelivery.DilatationSyntocinon
			,DurationSupport = LabourAndDelivery.DurationSupport
			,ExamVaginal = LabourAndDelivery.ExamVaginal
			,LabourAugmented = LabourAndDelivery.LabourAugmented
			,LabourDrugs = LabourAndDelivery.LabourDrugs
			,LabourSupport = LabourAndDelivery.LabourSupport
			,LSCSDecisionDT = LabourAndDelivery.LSCSDecisionDT
			,Midwife1LeftDT = LabourAndDelivery.Midwife1LeftDT
			,Midwife1stDT = LabourAndDelivery.Midwife1stDT
			,Midwife2ArrivalDT = LabourAndDelivery.Midwife2ArrivalDT
			,Midwife2LeftDT = LabourAndDelivery.Midwife2LeftDT
			,NumberHomeVisits = LabourAndDelivery.NumberHomeVisits
			,ProblemsMaternal = LabourAndDelivery.ProblemsMaternal
			,PushingCommenceDT = LabourAndDelivery.PushingCommenceDT
			,ReasonCSOther = LabourAndDelivery.ReasonCSOther
			,ReasonCSPrimary = LabourAndDelivery.ReasonCSPrimary
			,ReasonCSSecondary = LabourAndDelivery.ReasonCSSecondary
			,SyntocinonStartDT = LabourAndDelivery.SyntocinonStartDT
			,ThromboembolicRisk = LabourAndDelivery.ThromboembolicRisk
			,ThromboprophylaxisPrescribed = LabourAndDelivery.ThromboprophylaxisPrescribed
			,TransferHospital = LabourAndDelivery.TransferHospital
			,TransferReason = LabourAndDelivery.TransferReason
			,AmbulanceCalledDT = LabourAndDelivery.AmbulanceCalledDT
			,AmbulanceArrivedDT = LabourAndDelivery.AmbulanceArrivedDT
			,ReasonAnalgesiaDelivery = LabourAndDelivery.ReasonAnalgesiaDelivery
			,ReasonAnalgesiaDeliveryCode = LabourAndDelivery.ReasonAnalgesiaDelivery_Value
			,AnaestheticProblem = LabourAndDelivery.AnaestheticProblem
			,AnaestheticProblemCode = LabourAndDelivery.AnaestheticProblem_Value
			,SkinIncisionDT = LabourAndDelivery.SkinIncisionDT
			,CSDecisionMadeBy = LabourAndDelivery.CSDecisionMadeBy
			,CSDecisionMadeByCode = LabourAndDelivery.CSDecisionMadeBy_Value
			,DiscussionWithCons = LabourAndDelivery.DiscussionWithCons
			,DiscussionWithConsCode = LabourAndDelivery.DiscussionWithCons_Value
			,ArriveAtHospitalDT = LabourAndDelivery.ArriveAtHospitalDT
			,AlternativePainReliefCode = LabourAndDelivery.AlternativePainRelief_Value
			,MOHPersonnelInformed = LabourAndDelivery.MOHPersonnelInformed
			,MOHCause = LabourAndDelivery.MOHCause
			,MOHManagement = LabourAndDelivery.MOHManagement
			,CSSwabCount = LabourAndDelivery.CSSwabCount
			,CSSwabCountCode = LabourAndDelivery.CSSwabCount_Value
			,MLUAdmission = LabourAndDelivery.MLUAdmission
			,MLUAdmissionCode = LabourAndDelivery.MLUAdmission_Value
			,MLUAdmissionDT = LabourAndDelivery.MLUAdmissionDT
			,AbaestheticCriticalIncidentDT = LabourAndDelivery.AbaestheticCriticalIncidentDT
			,AnaestheticCIDTCode = LabourAndDelivery.AnaestheticCIDT_Value
			,AnaestheticCIDT = LabourAndDelivery.AnaestheticCIDT
			,AnaestheticCriticalIncident = LabourAndDelivery.AnaestheticCriticalIncident
			,AnaestheticCriticalIncidentCode = LabourAndDelivery.AnaestheticCriticalIncident_Value
		from
			E3.dbo.tblReport_Labour_and_Delivery_33 LabourAndDelivery

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3LabourAndDelivery on #TLoadE3LabourAndDelivery
	(
	PregnancyID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	E3.LabourAndDelivery target
using
	(
	select
		*
	from
		#TLoadE3LabourAndDelivery
	
	) source
	on	source.PregnancyID = target.PregnancyID

when not matched by source
then
	delete

when not matched
then
	insert
		(
		UserID
		,PatientID
		,PregnancyID
		,AnaesthsiaAtCaesareanCode
		,AnalgesiaCode
		,AnalgesiaDeliveryCode
		,AntibioticCoverCSCode
		,CervixDilatedDTCode
		,ComplicationsCode
		,CSDelayInProcedureCode
		,CSGradeCode
		,DilatationEpiduralCode
		,DilatationSyntocinonCode
		,DurationSupportCode
		,ExamVaginalCode
		,LabourAugmentedCode
		,LabourDrugsCode
		,LabourSupportCode
		,Midwife1LeftDTCode
		,Midwife1stDTCode
		,Midwife2ArrivalDTCode
		,Midwife2LeftDTCode
		,NumberHomeVisitsCode
		,ProblemsMaternalCode
		,ReasonCSOtherCode
		,ReasonCSPrimaryCode
		,ReasonCSSecondaryCode
		,SyntocinonStartDTCode
		,ThromboembolicRiskCode
		,ThromboprophylaxisPrescribedCode
		,TransferHospitalCode
		,TransferReasonCode
		,AdmissionCTG
		,AlternativePainRelief
		,AnaesthsiaAtCaesarean
		,Analgesia
		,AnalgesiaDelivery
		,AntibioticCoverCS
		,CervixDilatedDT
		,Complications
		,CSDelayInProcedure
		,CSGrade
		,CSSecondStage
		,DilatationEpidural
		,DilatationSyntocinon
		,DurationSupport
		,ExamVaginal
		,LabourAugmented
		,LabourDrugs
		,LabourSupport
		,LSCSDecisionDT
		,Midwife1LeftDT
		,Midwife1stDT
		,Midwife2ArrivalDT
		,Midwife2LeftDT
		,NumberHomeVisits
		,ProblemsMaternal
		,PushingCommenceDT
		,ReasonCSOther
		,ReasonCSPrimary
		,ReasonCSSecondary
		,SyntocinonStartDT
		,ThromboembolicRisk
		,ThromboprophylaxisPrescribed
		,TransferHospital
		,TransferReason
		,AmbulanceCalledDT
		,AmbulanceArrivedDT
		,ReasonAnalgesiaDelivery
		,ReasonAnalgesiaDeliveryCode
		,AnaestheticProblem
		,AnaestheticProblemCode
		,SkinIncisionDT
		,CSDecisionMadeBy
		,CSDecisionMadeByCode
		,DiscussionWithCons
		,DiscussionWithConsCode
		,ArriveAtHospitalDT
		,AlternativePainReliefCode
		,MOHPersonnelInformed
		,MOHCause
		,MOHManagement
		,CSSwabCount
		,CSSwabCountCode
		,MLUAdmission
		,MLUAdmissionCode
		,MLUAdmissionDT
		,AbaestheticCriticalIncidentDT
		,AnaestheticCIDTCode
		,AnaestheticCIDT
		,AnaestheticCriticalIncident
		,AnaestheticCriticalIncidentCode

		,Created
		,ByWhom
		)
	values
		(
		source.UserID
		,source.PatientID
		,source.PregnancyID
		,source.AnaesthsiaAtCaesareanCode
		,source.AnalgesiaCode
		,source.AnalgesiaDeliveryCode
		,source.AntibioticCoverCSCode
		,source.CervixDilatedDTCode
		,source.ComplicationsCode
		,source.CSDelayInProcedureCode
		,source.CSGradeCode
		,source.DilatationEpiduralCode
		,source.DilatationSyntocinonCode
		,source.DurationSupportCode
		,source.ExamVaginalCode
		,source.LabourAugmentedCode
		,source.LabourDrugsCode
		,source.LabourSupportCode
		,source.Midwife1LeftDTCode
		,source.Midwife1stDTCode
		,source.Midwife2ArrivalDTCode
		,source.Midwife2LeftDTCode
		,source.NumberHomeVisitsCode
		,source.ProblemsMaternalCode
		,source.ReasonCSOtherCode
		,source.ReasonCSPrimaryCode
		,source.ReasonCSSecondaryCode
		,source.SyntocinonStartDTCode
		,source.ThromboembolicRiskCode
		,source.ThromboprophylaxisPrescribedCode
		,source.TransferHospitalCode
		,source.TransferReasonCode
		,source.AdmissionCTG
		,source.AlternativePainRelief
		,source.AnaesthsiaAtCaesarean
		,source.Analgesia
		,source.AnalgesiaDelivery
		,source.AntibioticCoverCS
		,source.CervixDilatedDT
		,source.Complications
		,source.CSDelayInProcedure
		,source.CSGrade
		,source.CSSecondStage
		,source.DilatationEpidural
		,source.DilatationSyntocinon
		,source.DurationSupport
		,source.ExamVaginal
		,source.LabourAugmented
		,source.LabourDrugs
		,source.LabourSupport
		,source.LSCSDecisionDT
		,source.Midwife1LeftDT
		,source.Midwife1stDT
		,source.Midwife2ArrivalDT
		,source.Midwife2LeftDT
		,source.NumberHomeVisits
		,source.ProblemsMaternal
		,source.PushingCommenceDT
		,source.ReasonCSOther
		,source.ReasonCSPrimary
		,source.ReasonCSSecondary
		,source.SyntocinonStartDT
		,source.ThromboembolicRisk
		,source.ThromboprophylaxisPrescribed
		,source.TransferHospital
		,source.TransferReason
		,source.AmbulanceCalledDT
		,source.AmbulanceArrivedDT
		,source.ReasonAnalgesiaDelivery
		,source.ReasonAnalgesiaDeliveryCode
		,source.AnaestheticProblem
		,source.AnaestheticProblemCode
		,source.SkinIncisionDT
		,source.CSDecisionMadeBy
		,source.CSDecisionMadeByCode
		,source.DiscussionWithCons
		,source.DiscussionWithConsCode
		,source.ArriveAtHospitalDT
		,source.AlternativePainReliefCode
		,source.MOHPersonnelInformed
		,source.MOHCause
		,source.MOHManagement
		,source.CSSwabCount
		,source.CSSwabCountCode
		,source.MLUAdmission
		,source.MLUAdmissionCode
		,source.MLUAdmissionDT
		,source.AbaestheticCriticalIncidentDT
		,source.AnaestheticCIDTCode
		,source.AnaestheticCIDT
		,source.AnaestheticCriticalIncident
		,source.AnaestheticCriticalIncidentCode

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.UserID
		,target.PatientID
		,target.AnaesthsiaAtCaesareanCode
		,target.AnalgesiaCode
		,target.AnalgesiaDeliveryCode
		,target.AntibioticCoverCSCode
		,target.CervixDilatedDTCode
		,target.ComplicationsCode
		,target.CSDelayInProcedureCode
		,target.CSGradeCode
		,target.DilatationEpiduralCode
		,target.DilatationSyntocinonCode
		,target.DurationSupportCode
		,target.ExamVaginalCode
		,target.LabourAugmentedCode
		,target.LabourDrugsCode
		,target.LabourSupportCode
		,target.Midwife1LeftDTCode
		,target.Midwife1stDTCode
		,target.Midwife2ArrivalDTCode
		,target.Midwife2LeftDTCode
		,target.NumberHomeVisitsCode
		,target.ProblemsMaternalCode
		,target.ReasonCSOtherCode
		,target.ReasonCSPrimaryCode
		,target.ReasonCSSecondaryCode
		,target.SyntocinonStartDTCode
		,target.ThromboembolicRiskCode
		,target.ThromboprophylaxisPrescribedCode
		,target.TransferHospitalCode
		,target.TransferReasonCode
		,target.AdmissionCTG
		,target.AlternativePainRelief
		,target.AnaesthsiaAtCaesarean
		,target.Analgesia
		,target.AnalgesiaDelivery
		,target.AntibioticCoverCS
		,target.CervixDilatedDT
		,target.Complications
		,target.CSDelayInProcedure
		,target.CSGrade
		,target.CSSecondStage
		,target.DilatationEpidural
		,target.DilatationSyntocinon
		,target.DurationSupport
		,target.ExamVaginal
		,target.LabourAugmented
		,target.LabourDrugs
		,target.LabourSupport
		,target.LSCSDecisionDT
		,target.Midwife1LeftDT
		,target.Midwife1stDT
		,target.Midwife2ArrivalDT
		,target.Midwife2LeftDT
		,target.NumberHomeVisits
		,target.ProblemsMaternal
		,target.PushingCommenceDT
		,target.ReasonCSOther
		,target.ReasonCSPrimary
		,target.ReasonCSSecondary
		,target.SyntocinonStartDT
		,target.ThromboembolicRisk
		,target.ThromboprophylaxisPrescribed
		,target.TransferHospital
		,target.TransferReason
		,target.AmbulanceCalledDT
		,target.AmbulanceArrivedDT
		,target.ReasonAnalgesiaDelivery
		,target.ReasonAnalgesiaDeliveryCode
		,target.AnaestheticProblem
		,target.AnaestheticProblemCode
		,target.SkinIncisionDT
		,target.CSDecisionMadeBy
		,target.CSDecisionMadeByCode
		,target.DiscussionWithCons
		,target.DiscussionWithConsCode
		,target.ArriveAtHospitalDT
		,target.AlternativePainReliefCode
		,target.MOHPersonnelInformed
		,target.MOHCause
		,target.MOHManagement
		,target.CSSwabCount
		,target.CSSwabCountCode
		,target.MLUAdmission
		,target.MLUAdmissionCode
		,target.MLUAdmissionDT
		,target.AbaestheticCriticalIncidentDT
		,target.AnaestheticCIDTCode
		,target.AnaestheticCIDT
		,target.AnaestheticCriticalIncident
		,target.AnaestheticCriticalIncidentCode
		)

then
	update
	set
		target.UserID = source.UserID
		,target.PatientID = source.PatientID
		,target.AnaesthsiaAtCaesareanCode = source.AnaesthsiaAtCaesareanCode
		,target.AnalgesiaCode = source.AnalgesiaCode
		,target.AnalgesiaDeliveryCode = source.AnalgesiaDeliveryCode
		,target.AntibioticCoverCSCode = source.AntibioticCoverCSCode
		,target.CervixDilatedDTCode = source.CervixDilatedDTCode
		,target.ComplicationsCode = source.ComplicationsCode
		,target.CSDelayInProcedureCode = source.CSDelayInProcedureCode
		,target.CSGradeCode = source.CSGradeCode
		,target.DilatationEpiduralCode = source.DilatationEpiduralCode
		,target.DilatationSyntocinonCode = source.DilatationSyntocinonCode
		,target.DurationSupportCode = source.DurationSupportCode
		,target.ExamVaginalCode = source.ExamVaginalCode
		,target.LabourAugmentedCode = source.LabourAugmentedCode
		,target.LabourDrugsCode = source.LabourDrugsCode
		,target.LabourSupportCode = source.LabourSupportCode
		,target.Midwife1LeftDTCode = source.Midwife1LeftDTCode
		,target.Midwife1stDTCode = source.Midwife1stDTCode
		,target.Midwife2ArrivalDTCode = source.Midwife2ArrivalDTCode
		,target.Midwife2LeftDTCode = source.Midwife2LeftDTCode
		,target.NumberHomeVisitsCode = source.NumberHomeVisitsCode
		,target.ProblemsMaternalCode = source.ProblemsMaternalCode
		,target.ReasonCSOtherCode = source.ReasonCSOtherCode
		,target.ReasonCSPrimaryCode = source.ReasonCSPrimaryCode
		,target.ReasonCSSecondaryCode = source.ReasonCSSecondaryCode
		,target.SyntocinonStartDTCode = source.SyntocinonStartDTCode
		,target.ThromboembolicRiskCode = source.ThromboembolicRiskCode
		,target.ThromboprophylaxisPrescribedCode = source.ThromboprophylaxisPrescribedCode
		,target.TransferHospitalCode = source.TransferHospitalCode
		,target.TransferReasonCode = source.TransferReasonCode
		,target.AdmissionCTG = source.AdmissionCTG
		,target.AlternativePainRelief = source.AlternativePainRelief
		,target.AnaesthsiaAtCaesarean = source.AnaesthsiaAtCaesarean
		,target.Analgesia = source.Analgesia
		,target.AnalgesiaDelivery = source.AnalgesiaDelivery
		,target.AntibioticCoverCS = source.AntibioticCoverCS
		,target.CervixDilatedDT = source.CervixDilatedDT
		,target.Complications = source.Complications
		,target.CSDelayInProcedure = source.CSDelayInProcedure
		,target.CSGrade = source.CSGrade
		,target.CSSecondStage = source.CSSecondStage
		,target.DilatationEpidural = source.DilatationEpidural
		,target.DilatationSyntocinon = source.DilatationSyntocinon
		,target.DurationSupport = source.DurationSupport
		,target.ExamVaginal = source.ExamVaginal
		,target.LabourAugmented = source.LabourAugmented
		,target.LabourDrugs = source.LabourDrugs
		,target.LabourSupport = source.LabourSupport
		,target.LSCSDecisionDT = source.LSCSDecisionDT
		,target.Midwife1LeftDT = source.Midwife1LeftDT
		,target.Midwife1stDT = source.Midwife1stDT
		,target.Midwife2ArrivalDT = source.Midwife2ArrivalDT
		,target.Midwife2LeftDT = source.Midwife2LeftDT
		,target.NumberHomeVisits = source.NumberHomeVisits
		,target.ProblemsMaternal = source.ProblemsMaternal
		,target.PushingCommenceDT = source.PushingCommenceDT
		,target.ReasonCSOther = source.ReasonCSOther
		,target.ReasonCSPrimary = source.ReasonCSPrimary
		,target.ReasonCSSecondary = source.ReasonCSSecondary
		,target.SyntocinonStartDT = source.SyntocinonStartDT
		,target.ThromboembolicRisk = source.ThromboembolicRisk
		,target.ThromboprophylaxisPrescribed = source.ThromboprophylaxisPrescribed
		,target.TransferHospital = source.TransferHospital
		,target.TransferReason = source.TransferReason
		,target.AmbulanceCalledDT = source.AmbulanceCalledDT
		,target.AmbulanceArrivedDT = source.AmbulanceArrivedDT
		,target.ReasonAnalgesiaDelivery = source.ReasonAnalgesiaDelivery
		,target.ReasonAnalgesiaDeliveryCode = source.ReasonAnalgesiaDeliveryCode
		,target.AnaestheticProblem = source.AnaestheticProblem
		,target.AnaestheticProblemCode = source.AnaestheticProblemCode
		,target.SkinIncisionDT = source.SkinIncisionDT
		,target.CSDecisionMadeBy = source.CSDecisionMadeBy
		,target.CSDecisionMadeByCode = source.CSDecisionMadeByCode
		,target.DiscussionWithCons = source.DiscussionWithCons
		,target.DiscussionWithConsCode = source.DiscussionWithConsCode
		,target.ArriveAtHospitalDT = source.ArriveAtHospitalDT
		,target.AlternativePainReliefCode = source.AlternativePainReliefCode
		,target.MOHPersonnelInformed = source.MOHPersonnelInformed
		,target.MOHCause = source.MOHCause
		,target.MOHManagement = source.MOHManagement
		,target.CSSwabCount = source.CSSwabCount
		,target.CSSwabCountCode = source.CSSwabCountCode
		,target.MLUAdmission = source.MLUAdmission
		,target.MLUAdmissionCode = source.MLUAdmissionCode
		,target.MLUAdmissionDT = source.MLUAdmissionDT
		,target.AbaestheticCriticalIncidentDT = source.AbaestheticCriticalIncidentDT
		,target.AnaestheticCIDTCode = source.AnaestheticCIDTCode
		,target.AnaestheticCIDT = source.AnaestheticCIDT
		,target.AnaestheticCriticalIncident = source.AnaestheticCriticalIncident
		,target.AnaestheticCriticalIncidentCode = source.AnaestheticCriticalIncidentCode

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime