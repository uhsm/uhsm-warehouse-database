﻿CREATE procedure [E3].[LoadNeonatalExamination] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			UserID
			,PregnancyID
			,AbnormalFaceMouthPalateCode
			,AppearanceOfBabyCode
			,BabyExaminedByCode
			,BabysStoolCode
			,CardiovascularLungsCode
			,ENBMotherPresentCode
			,ExaminationConveyedToParentsCode
			,ExaminationOfNewBornCode
			,FaceCode
			,GenitoUrinaryCode
			,HistoryOfHeartDiseaseCode
			,InvestigationsCode
			,LengthOfBabyCode
			,NeckCode
			,NeurologicalCode
			,NoseCode
			,PaediatricAlertCode
			,PassingUrineCode
			,PresentAtExaminationCode
			,ReferralsAfterExaminationCode
			,RiskFactorsSepsisCode
			,SyndromesCode
			,TemperatureInstabilityCode
			,UmbilicusCode
			,AbdomenGenitalia
			,AbnormalEars
			,AbnormalEyes
			,AbnormalFaceMouthPalate
			,AbnormalHead
			,AbnormalHeart
			,AbnormalHips
			,AbnormalScalpSkull
			,AbnormalSkin
			,AdmitToSCBU
			,AppearanceOfBaby
			,BabyExaminedBy
			,BabysStool
			,CardiovascularLungs
			,ENBAdditionalComments
			,ENBMeconium
			,ENBMotherPresent
			,ENBUrine
			,ExaminationConveyedToParents
			,ExaminationDT
			,ExaminationOfNewBorn
			,Face
			,FemoralPulse
			,GenitoUrinary
			,Gestation
			,HeadCirc1stExam
			,HeadCircumferenceExamination
			,HistoryOfHeartDisease
			,Investigations
			,LengthOfBaby
			,Musculoskeletal
			,Neck
			,Neurological
			,NNUAdmittedReason
			,Nose
			,PaediatricAlert
			,PassingUrine
			,PresentAtExamination
			,RedReflex
			,ReferralsAfterExamination
			,RiskFactorsSepsis
			,Syndromes
			,TemperatureInstability
			,Umbilicus
			,AbnormalHipsCode
			,ENBAdditionalCommentsCode
			,RelevantHistory
			,SpinalExamination
			,HearingScreeningResult
			,RelevantPaediatricAlerts
			,AbdomenGenitaliaCode
			,AbnormalEarsCode
			,AbnormalEyesCode
			,AbnormalHeadCode
			,AbnormalHeartCode
			,AbnormalSkinCode
			,FemoralPulseCode
			,HearingScreeningResultCode
			,MusculoskeletalCode
			,RedReflexCode
			,RelevantHistoryCode
			,RelevantPaediatricAlertsCode
			,SpinalExaminationCode
			,ReasonExaminationDelayed
			,ReasonExaminationDelayedCode
			,BloodGroup
			,BloodGroupCode
			,Meconium
			,MeconiumCode
			,CongenitalAnomalies
			,CongenitalAnomaliesCode
			,HearingScreenDate
			,ExaminationHeartRate
			,ExaminationHeartRateCode
			,ExaminationRespiratoryRate
			,OxygenSaturationLevelDelivery
			,ExaminationRespiratoryRateCode
			,OxygenSaturationLevelDeliveryCode
			,O2SaturationLevelDeliveryPostDuctal
			)
into
	#TLoadE3NeonatalExamination
from
	(
	select
		UserID = cast(UserID as int)
		,PregnancyID = cast(PregnancyID as bigint)
		,BabyID = cast(BabyID as bigint)
		,AbnormalFaceMouthPalateCode = cast(AbnormalFaceMouthPalateCode as bigint)
		,AppearanceOfBabyCode = cast(AppearanceOfBabyCode as bigint)
		,BabyExaminedByCode = cast(BabyExaminedByCode as bigint)
		,BabysStoolCode = cast(BabysStoolCode as bigint)
		,CardiovascularLungsCode = cast(CardiovascularLungsCode as bigint)
		,ENBMotherPresentCode = cast(ENBMotherPresentCode as bigint)
		,ExaminationConveyedToParentsCode = cast(ExaminationConveyedToParentsCode as bigint)
		,ExaminationOfNewBornCode = cast(ExaminationOfNewBornCode as bigint)
		,FaceCode = cast(FaceCode as bigint)
		,GenitoUrinaryCode = cast(GenitoUrinaryCode as bigint)
		,HistoryOfHeartDiseaseCode = cast(HistoryOfHeartDiseaseCode as bigint)
		,InvestigationsCode = cast(InvestigationsCode as bigint)
		,LengthOfBabyCode = cast(LengthOfBabyCode as bigint)
		,NeckCode = cast(NeckCode as bigint)
		,NeurologicalCode = cast(NeurologicalCode as bigint)
		,NoseCode = cast(NoseCode as bigint)
		,PaediatricAlertCode = cast(PaediatricAlertCode as bigint)
		,PassingUrineCode = cast(PassingUrineCode as bigint)
		,PresentAtExaminationCode = cast(PresentAtExaminationCode as bigint)
		,ReferralsAfterExaminationCode = cast(ReferralsAfterExaminationCode as bigint)
		,RiskFactorsSepsisCode = cast(RiskFactorsSepsisCode as bigint)
		,SyndromesCode = cast(SyndromesCode as bigint)
		,TemperatureInstabilityCode = cast(TemperatureInstabilityCode as bigint)
		,UmbilicusCode = cast(UmbilicusCode as bigint)
		,AbdomenGenitalia = cast(nullif(AbdomenGenitalia, '') as nvarchar(2000))
		,AbnormalEars = cast(nullif(AbnormalEars, '') as nvarchar(2000))
		,AbnormalEyes = cast(nullif(AbnormalEyes, '') as nvarchar(2000))
		,AbnormalFaceMouthPalate = cast(nullif(AbnormalFaceMouthPalate, '') as nvarchar(2000))
		,AbnormalHead = cast(nullif(AbnormalHead, '') as nvarchar(2000))
		,AbnormalHeart = cast(nullif(AbnormalHeart, '') as nvarchar(2000))
		,AbnormalHips = cast(nullif(AbnormalHips, '') as nvarchar(2000))
		,AbnormalScalpSkull = cast(nullif(AbnormalScalpSkull, '') as nvarchar(2000))
		,AbnormalSkin = cast(nullif(AbnormalSkin, '') as nvarchar(2000))
		,AdmitToSCBU = cast(nullif(AdmitToSCBU, '') as nvarchar(2000))
		,AppearanceOfBaby = cast(nullif(AppearanceOfBaby, '') as nvarchar(2000))
		,BabyExaminedBy = cast(nullif(BabyExaminedBy, '') as nvarchar(2000))
		,BabysStool = cast(nullif(BabysStool, '') as nvarchar(2000))
		,CardiovascularLungs = cast(nullif(CardiovascularLungs, '') as nvarchar(2000))
		,ENBAdditionalComments = cast(nullif(ENBAdditionalComments, '') as nvarchar(2000))
		,ENBMeconium = cast(nullif(ENBMeconium, '') as nvarchar(2000))
		,ENBMotherPresent = cast(nullif(ENBMotherPresent, '') as nvarchar(2000))
		,ENBUrine = cast(nullif(ENBUrine, '') as nvarchar(2000))
		,ExaminationConveyedToParents = cast(nullif(ExaminationConveyedToParents, '') as nvarchar(2000))
		,ExaminationDT = cast(nullif(ExaminationDT, '') as nvarchar(2000))
		,ExaminationOfNewBorn = cast(nullif(ExaminationOfNewBorn, '') as nvarchar(2000))
		,Face = cast(nullif(Face, '') as nvarchar(2000))
		,FemoralPulse = cast(nullif(FemoralPulse, '') as nvarchar(2000))
		,GenitoUrinary = cast(nullif(GenitoUrinary, '') as nvarchar(2000))
		,Gestation = cast(nullif(Gestation, '') as nvarchar(2000))
		,HeadCirc1stExam = cast(nullif(HeadCirc1stExam, '') as nvarchar(2000))
		,HeadCircumferenceExamination = cast(nullif(HeadCircumferenceExamination, '') as nvarchar(2000))
		,HistoryOfHeartDisease = cast(nullif(HistoryOfHeartDisease, '') as nvarchar(2000))
		,Investigations = cast(nullif(Investigations, '') as nvarchar(2000))
		,LengthOfBaby = cast(nullif(LengthOfBaby, '') as nvarchar(2000))
		,Musculoskeletal = cast(nullif(Musculoskeletal, '') as nvarchar(2000))
		,Neck = cast(nullif(Neck, '') as nvarchar(2000))
		,Neurological = cast(nullif(Neurological, '') as nvarchar(2000))
		,NNUAdmittedReason = cast(nullif(NNUAdmittedReason, '') as nvarchar(2000))
		,Nose = cast(nullif(Nose, '') as nvarchar(2000))
		,PaediatricAlert = cast(nullif(PaediatricAlert, '') as nvarchar(2000))
		,PassingUrine = cast(nullif(PassingUrine, '') as nvarchar(2000))
		,PresentAtExamination = cast(nullif(PresentAtExamination, '') as nvarchar(2000))
		,RedReflex = cast(nullif(RedReflex, '') as nvarchar(2000))
		,ReferralsAfterExamination = cast(nullif(ReferralsAfterExamination, '') as nvarchar(2000))
		,RiskFactorsSepsis = cast(nullif(RiskFactorsSepsis, '') as nvarchar(2000))
		,Syndromes = cast(nullif(Syndromes, '') as nvarchar(2000))
		,TemperatureInstability = cast(nullif(TemperatureInstability, '') as nvarchar(2000))
		,Umbilicus = cast(nullif(Umbilicus, '') as nvarchar(2000))
		,AbnormalHipsCode = cast(AbnormalHipsCode as bigint)
		,ENBAdditionalCommentsCode = cast(ENBAdditionalCommentsCode as bigint)
		,RelevantHistory = cast(nullif(RelevantHistory, '') as nvarchar(2000))
		,SpinalExamination = cast(nullif(SpinalExamination, '') as nvarchar(2000))
		,HearingScreeningResult = cast(nullif(HearingScreeningResult, '') as nvarchar(2000))
		,RelevantPaediatricAlerts = cast(nullif(RelevantPaediatricAlerts, '') as nvarchar(2000))
		,AbdomenGenitaliaCode = cast(AbdomenGenitaliaCode as bigint)
		,AbnormalEarsCode = cast(AbnormalEarsCode as bigint)
		,AbnormalEyesCode = cast(AbnormalEyesCode as bigint)
		,AbnormalHeadCode = cast(AbnormalHeadCode as bigint)
		,AbnormalHeartCode = cast(AbnormalHeartCode as bigint)
		,AbnormalSkinCode = cast(AbnormalSkinCode as bigint)
		,FemoralPulseCode = cast(FemoralPulseCode as bigint)
		,HearingScreeningResultCode = cast(HearingScreeningResultCode as bigint)
		,MusculoskeletalCode = cast(MusculoskeletalCode as bigint)
		,RedReflexCode = cast(RedReflexCode as bigint)
		,RelevantHistoryCode = cast(RelevantHistoryCode as bigint)
		,RelevantPaediatricAlertsCode = cast(RelevantPaediatricAlertsCode as bigint)
		,SpinalExaminationCode = cast(SpinalExaminationCode as bigint)
		,ReasonExaminationDelayed = cast(nullif(ReasonExaminationDelayed, '') as nvarchar(2000))
		,ReasonExaminationDelayedCode = cast(ReasonExaminationDelayedCode as bigint)
		,BloodGroup = cast(nullif(BloodGroup, '') as nvarchar(2000))
		,BloodGroupCode = cast(BloodGroupCode as bigint)
		,Meconium = cast(nullif(Meconium, '') as nvarchar(2000))
		,MeconiumCode = cast(MeconiumCode as bigint)
		,CongenitalAnomalies = cast(nullif(CongenitalAnomalies, '') as nvarchar(2000))
		,CongenitalAnomaliesCode = cast(CongenitalAnomaliesCode as bigint)
		,HearingScreenDate = cast(nullif(HearingScreenDate, '') as nvarchar(2000))
		,ExaminationHeartRate = cast(nullif(ExaminationHeartRate, '') as nvarchar(2000))
		,ExaminationHeartRateCode = cast(ExaminationHeartRateCode as bigint)
		,ExaminationRespiratoryRate = cast(nullif(ExaminationRespiratoryRate, '') as nvarchar(2000))
		,OxygenSaturationLevelDelivery = cast(nullif(OxygenSaturationLevelDelivery, '') as nvarchar(2000))
		,ExaminationRespiratoryRateCode = cast(ExaminationRespiratoryRateCode as bigint)
		,OxygenSaturationLevelDeliveryCode = cast(OxygenSaturationLevelDeliveryCode as bigint)
		,O2SaturationLevelDeliveryPostDuctal = cast(nullif(O2SaturationLevelDeliveryPostDuctal, '') as nvarchar(2000))
	from
		(
		select
			UserID = NeonatalExamination.UserID
			,PregnancyID = NeonatalExamination.PregnancyID
			,BabyID = NeonatalExamination.BabyID
			,AbnormalFaceMouthPalateCode = NeonatalExamination.AbnormalFaceMouthPalate_Value
			,AppearanceOfBabyCode = NeonatalExamination.AppearanceOfBaby_Value
			,BabyExaminedByCode = NeonatalExamination.BabyExaminedBy_Value
			,BabysStoolCode = NeonatalExamination.BabysStool_Value
			,CardiovascularLungsCode = NeonatalExamination.CardiovascularLungs_Value
			,ENBMotherPresentCode = NeonatalExamination.ENBMotherPresent_Value
			,ExaminationConveyedToParentsCode = NeonatalExamination.ExaminationConveyedToParents_Value
			,ExaminationOfNewBornCode = NeonatalExamination.ExaminationOfNewBorn_Value
			,FaceCode = NeonatalExamination.Face_Value
			,GenitoUrinaryCode = NeonatalExamination.GenitoUrinary_Value
			,HistoryOfHeartDiseaseCode = NeonatalExamination.HistoryOfHeartDisease_Value
			,InvestigationsCode = NeonatalExamination.Investigations_Value
			,LengthOfBabyCode = NeonatalExamination.LengthOfBaby_Value
			,NeckCode = NeonatalExamination.Neck_Value
			,NeurologicalCode = NeonatalExamination.Neurological_Value
			,NoseCode = NeonatalExamination.Nose_Value
			,PaediatricAlertCode = NeonatalExamination.PaediatricAlert_Value
			,PassingUrineCode = NeonatalExamination.PassingUrine_Value
			,PresentAtExaminationCode = NeonatalExamination.PresentAtExamination_Value
			,ReferralsAfterExaminationCode = NeonatalExamination.ReferralsAfterExamination_Value
			,RiskFactorsSepsisCode = NeonatalExamination.RiskFactorsSepsis_Value
			,SyndromesCode = NeonatalExamination.Syndromes_Value
			,TemperatureInstabilityCode = NeonatalExamination.TemperatureInstability_Value
			,UmbilicusCode = NeonatalExamination.Umbilicus_Value
			,AbdomenGenitalia = NeonatalExamination.AbdomenGenitalia
			,AbnormalEars = NeonatalExamination.AbnormalEars
			,AbnormalEyes = NeonatalExamination.AbnormalEyes
			,AbnormalFaceMouthPalate = NeonatalExamination.AbnormalFaceMouthPalate
			,AbnormalHead = NeonatalExamination.AbnormalHead
			,AbnormalHeart = NeonatalExamination.AbnormalHeart
			,AbnormalHips = NeonatalExamination.AbnormalHips
			,AbnormalScalpSkull = NeonatalExamination.AbnormalScalpSkull
			,AbnormalSkin = NeonatalExamination.AbnormalSkin
			,AdmitToSCBU = NeonatalExamination.AdmitToSCBU
			,AppearanceOfBaby = NeonatalExamination.AppearanceOfBaby
			,BabyExaminedBy = NeonatalExamination.BabyExaminedBy
			,BabysStool = NeonatalExamination.BabysStool
			,CardiovascularLungs = NeonatalExamination.CardiovascularLungs
			,ENBAdditionalComments = NeonatalExamination.ENBAdditionalComments
			,ENBMeconium = NeonatalExamination.ENBMeconium
			,ENBMotherPresent = NeonatalExamination.ENBMotherPresent
			,ENBUrine = NeonatalExamination.ENBUrine
			,ExaminationConveyedToParents = NeonatalExamination.ExaminationConveyedToParents
			,ExaminationDT = NeonatalExamination.ExaminationDT
			,ExaminationOfNewBorn = NeonatalExamination.ExaminationOfNewBorn
			,Face = NeonatalExamination.Face
			,FemoralPulse = NeonatalExamination.FemoralPulse
			,GenitoUrinary = NeonatalExamination.GenitoUrinary
			,Gestation = NeonatalExamination.Gestation
			,HeadCirc1stExam = NeonatalExamination.HeadCirc1stExam
			,HeadCircumferenceExamination = NeonatalExamination.HeadCircumferenceExamination
			,HistoryOfHeartDisease = NeonatalExamination.HistoryOfHeartDisease
			,Investigations = NeonatalExamination.Investigations
			,LengthOfBaby = NeonatalExamination.LengthOfBaby
			,Musculoskeletal = NeonatalExamination.Musculoskeletal
			,Neck = NeonatalExamination.Neck
			,Neurological = NeonatalExamination.Neurological
			,NNUAdmittedReason = NeonatalExamination.NNUAdmittedReason
			,Nose = NeonatalExamination.Nose
			,PaediatricAlert = NeonatalExamination.PaediatricAlert
			,PassingUrine = NeonatalExamination.PassingUrine
			,PresentAtExamination = NeonatalExamination.PresentAtExamination
			,RedReflex = NeonatalExamination.RedReflex
			,ReferralsAfterExamination = NeonatalExamination.ReferralsAfterExamination
			,RiskFactorsSepsis = NeonatalExamination.RiskFactorsSepsis
			,Syndromes = NeonatalExamination.Syndromes
			,TemperatureInstability = NeonatalExamination.TemperatureInstability
			,Umbilicus = NeonatalExamination.Umbilicus
			,AbnormalHipsCode = NeonatalExamination.AbnormalHips_Value
			,ENBAdditionalCommentsCode = NeonatalExamination.ENBAdditionalComments_Value
			,RelevantHistory = NeonatalExamination.RelevantHistory
			,SpinalExamination = NeonatalExamination.SpinalExamination
			,HearingScreeningResult = NeonatalExamination.HearingScreeningResult
			,RelevantPaediatricAlerts = NeonatalExamination.RelevantPaediatricAlerts
			,AbdomenGenitaliaCode = NeonatalExamination.AbdomenGenitalia_Value
			,AbnormalEarsCode = NeonatalExamination.AbnormalEars_Value
			,AbnormalEyesCode = NeonatalExamination.AbnormalEyes_Value
			,AbnormalHeadCode = NeonatalExamination.AbnormalHead_Value
			,AbnormalHeartCode = NeonatalExamination.AbnormalHeart_Value
			,AbnormalSkinCode = NeonatalExamination.AbnormalSkin_Value
			,FemoralPulseCode = NeonatalExamination.FemoralPulse_Value
			,HearingScreeningResultCode = NeonatalExamination.HearingScreeningResult_Value
			,MusculoskeletalCode = NeonatalExamination.Musculoskeletal_Value
			,RedReflexCode = NeonatalExamination.RedReflex_Value
			,RelevantHistoryCode = NeonatalExamination.RelevantHistory_Value
			,RelevantPaediatricAlertsCode = NeonatalExamination.RelevantPaediatricAlerts_Value
			,SpinalExaminationCode = NeonatalExamination.SpinalExamination_Value
			,ReasonExaminationDelayed = NeonatalExamination.ReasonExaminationDelayed
			,ReasonExaminationDelayedCode = NeonatalExamination.ReasonExaminationDelayed_Value
			,BloodGroup = NeonatalExamination.BloodGroup
			,BloodGroupCode = NeonatalExamination.BloodGroup_Value
			,Meconium = NeonatalExamination.Meconium
			,MeconiumCode = NeonatalExamination.Meconium_Value
			,CongenitalAnomalies = NeonatalExamination.CongenitalAnomalies
			,CongenitalAnomaliesCode = NeonatalExamination.CongenitalAnomalies_Value
			,HearingScreenDate = NeonatalExamination.HearingScreenDate
			,ExaminationHeartRate = NeonatalExamination.ExaminationHeartRate
			,ExaminationHeartRateCode = NeonatalExamination.ExaminationHeartRate_Value
			,ExaminationRespiratoryRate = NeonatalExamination.ExaminationRespiratoryRate
			,OxygenSaturationLevelDelivery = NeonatalExamination.OxygenSaturationLevelDelivery
			,ExaminationRespiratoryRateCode = NeonatalExamination.ExaminationRespiratoryRate_Value
			,OxygenSaturationLevelDeliveryCode = NeonatalExamination.OxygenSaturationLevelDelivery_Value
			,O2SaturationLevelDeliveryPostDuctal = NeonatalExamination.O2SaturationLevelDeliveryPostDuctal
		from
			E3.dbo.tblReport_Neonatal_Examination_13 NeonatalExamination

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3NeonatalExamination on #TLoadE3NeonatalExamination
	(
	BabyID ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	E3.NeonatalExamination target
using
	(
	select
		*
	from
		#TLoadE3NeonatalExamination
	
	) source
	on	source.BabyID = target.BabyID

when not matched by source
then
	delete

when not matched
then
	insert
		(
		UserID
		,PregnancyID
		,BabyID
		,AbnormalFaceMouthPalateCode
		,AppearanceOfBabyCode
		,BabyExaminedByCode
		,BabysStoolCode
		,CardiovascularLungsCode
		,ENBMotherPresentCode
		,ExaminationConveyedToParentsCode
		,ExaminationOfNewBornCode
		,FaceCode
		,GenitoUrinaryCode
		,HistoryOfHeartDiseaseCode
		,InvestigationsCode
		,LengthOfBabyCode
		,NeckCode
		,NeurologicalCode
		,NoseCode
		,PaediatricAlertCode
		,PassingUrineCode
		,PresentAtExaminationCode
		,ReferralsAfterExaminationCode
		,RiskFactorsSepsisCode
		,SyndromesCode
		,TemperatureInstabilityCode
		,UmbilicusCode
		,AbdomenGenitalia
		,AbnormalEars
		,AbnormalEyes
		,AbnormalFaceMouthPalate
		,AbnormalHead
		,AbnormalHeart
		,AbnormalHips
		,AbnormalScalpSkull
		,AbnormalSkin
		,AdmitToSCBU
		,AppearanceOfBaby
		,BabyExaminedBy
		,BabysStool
		,CardiovascularLungs
		,ENBAdditionalComments
		,ENBMeconium
		,ENBMotherPresent
		,ENBUrine
		,ExaminationConveyedToParents
		,ExaminationDT
		,ExaminationOfNewBorn
		,Face
		,FemoralPulse
		,GenitoUrinary
		,Gestation
		,HeadCirc1stExam
		,HeadCircumferenceExamination
		,HistoryOfHeartDisease
		,Investigations
		,LengthOfBaby
		,Musculoskeletal
		,Neck
		,Neurological
		,NNUAdmittedReason
		,Nose
		,PaediatricAlert
		,PassingUrine
		,PresentAtExamination
		,RedReflex
		,ReferralsAfterExamination
		,RiskFactorsSepsis
		,Syndromes
		,TemperatureInstability
		,Umbilicus
		,AbnormalHipsCode
		,ENBAdditionalCommentsCode
		,RelevantHistory
		,SpinalExamination
		,HearingScreeningResult
		,RelevantPaediatricAlerts
		,AbdomenGenitaliaCode
		,AbnormalEarsCode
		,AbnormalEyesCode
		,AbnormalHeadCode
		,AbnormalHeartCode
		,AbnormalSkinCode
		,FemoralPulseCode
		,HearingScreeningResultCode
		,MusculoskeletalCode
		,RedReflexCode
		,RelevantHistoryCode
		,RelevantPaediatricAlertsCode
		,SpinalExaminationCode
		,ReasonExaminationDelayed
		,ReasonExaminationDelayedCode
		,BloodGroup
		,BloodGroupCode
		,Meconium
		,MeconiumCode
		,CongenitalAnomalies
		,CongenitalAnomaliesCode
		,HearingScreenDate
		,ExaminationHeartRate
		,ExaminationHeartRateCode
		,ExaminationRespiratoryRate
		,OxygenSaturationLevelDelivery
		,ExaminationRespiratoryRateCode
		,OxygenSaturationLevelDeliveryCode
		,O2SaturationLevelDeliveryPostDuctal

		,Created
		,ByWhom
		)
	values
		(
		source.UserID
		,source.PregnancyID
		,source.BabyID
		,source.AbnormalFaceMouthPalateCode
		,source.AppearanceOfBabyCode
		,source.BabyExaminedByCode
		,source.BabysStoolCode
		,source.CardiovascularLungsCode
		,source.ENBMotherPresentCode
		,source.ExaminationConveyedToParentsCode
		,source.ExaminationOfNewBornCode
		,source.FaceCode
		,source.GenitoUrinaryCode
		,source.HistoryOfHeartDiseaseCode
		,source.InvestigationsCode
		,source.LengthOfBabyCode
		,source.NeckCode
		,source.NeurologicalCode
		,source.NoseCode
		,source.PaediatricAlertCode
		,source.PassingUrineCode
		,source.PresentAtExaminationCode
		,source.ReferralsAfterExaminationCode
		,source.RiskFactorsSepsisCode
		,source.SyndromesCode
		,source.TemperatureInstabilityCode
		,source.UmbilicusCode
		,source.AbdomenGenitalia
		,source.AbnormalEars
		,source.AbnormalEyes
		,source.AbnormalFaceMouthPalate
		,source.AbnormalHead
		,source.AbnormalHeart
		,source.AbnormalHips
		,source.AbnormalScalpSkull
		,source.AbnormalSkin
		,source.AdmitToSCBU
		,source.AppearanceOfBaby
		,source.BabyExaminedBy
		,source.BabysStool
		,source.CardiovascularLungs
		,source.ENBAdditionalComments
		,source.ENBMeconium
		,source.ENBMotherPresent
		,source.ENBUrine
		,source.ExaminationConveyedToParents
		,source.ExaminationDT
		,source.ExaminationOfNewBorn
		,source.Face
		,source.FemoralPulse
		,source.GenitoUrinary
		,source.Gestation
		,source.HeadCirc1stExam
		,source.HeadCircumferenceExamination
		,source.HistoryOfHeartDisease
		,source.Investigations
		,source.LengthOfBaby
		,source.Musculoskeletal
		,source.Neck
		,source.Neurological
		,source.NNUAdmittedReason
		,source.Nose
		,source.PaediatricAlert
		,source.PassingUrine
		,source.PresentAtExamination
		,source.RedReflex
		,source.ReferralsAfterExamination
		,source.RiskFactorsSepsis
		,source.Syndromes
		,source.TemperatureInstability
		,source.Umbilicus
		,source.AbnormalHipsCode
		,source.ENBAdditionalCommentsCode
		,source.RelevantHistory
		,source.SpinalExamination
		,source.HearingScreeningResult
		,source.RelevantPaediatricAlerts
		,source.AbdomenGenitaliaCode
		,source.AbnormalEarsCode
		,source.AbnormalEyesCode
		,source.AbnormalHeadCode
		,source.AbnormalHeartCode
		,source.AbnormalSkinCode
		,source.FemoralPulseCode
		,source.HearingScreeningResultCode
		,source.MusculoskeletalCode
		,source.RedReflexCode
		,source.RelevantHistoryCode
		,source.RelevantPaediatricAlertsCode
		,source.SpinalExaminationCode
		,source.ReasonExaminationDelayed
		,source.ReasonExaminationDelayedCode
		,source.BloodGroup
		,source.BloodGroupCode
		,source.Meconium
		,source.MeconiumCode
		,source.CongenitalAnomalies
		,source.CongenitalAnomaliesCode
		,source.HearingScreenDate
		,source.ExaminationHeartRate
		,source.ExaminationHeartRateCode
		,source.ExaminationRespiratoryRate
		,source.OxygenSaturationLevelDelivery
		,source.ExaminationRespiratoryRateCode
		,source.OxygenSaturationLevelDeliveryCode
		,source.O2SaturationLevelDeliveryPostDuctal

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.UserID
		,target.PregnancyID
		,target.AbnormalFaceMouthPalateCode
		,target.AppearanceOfBabyCode
		,target.BabyExaminedByCode
		,target.BabysStoolCode
		,target.CardiovascularLungsCode
		,target.ENBMotherPresentCode
		,target.ExaminationConveyedToParentsCode
		,target.ExaminationOfNewBornCode
		,target.FaceCode
		,target.GenitoUrinaryCode
		,target.HistoryOfHeartDiseaseCode
		,target.InvestigationsCode
		,target.LengthOfBabyCode
		,target.NeckCode
		,target.NeurologicalCode
		,target.NoseCode
		,target.PaediatricAlertCode
		,target.PassingUrineCode
		,target.PresentAtExaminationCode
		,target.ReferralsAfterExaminationCode
		,target.RiskFactorsSepsisCode
		,target.SyndromesCode
		,target.TemperatureInstabilityCode
		,target.UmbilicusCode
		,target.AbdomenGenitalia
		,target.AbnormalEars
		,target.AbnormalEyes
		,target.AbnormalFaceMouthPalate
		,target.AbnormalHead
		,target.AbnormalHeart
		,target.AbnormalHips
		,target.AbnormalScalpSkull
		,target.AbnormalSkin
		,target.AdmitToSCBU
		,target.AppearanceOfBaby
		,target.BabyExaminedBy
		,target.BabysStool
		,target.CardiovascularLungs
		,target.ENBAdditionalComments
		,target.ENBMeconium
		,target.ENBMotherPresent
		,target.ENBUrine
		,target.ExaminationConveyedToParents
		,target.ExaminationDT
		,target.ExaminationOfNewBorn
		,target.Face
		,target.FemoralPulse
		,target.GenitoUrinary
		,target.Gestation
		,target.HeadCirc1stExam
		,target.HeadCircumferenceExamination
		,target.HistoryOfHeartDisease
		,target.Investigations
		,target.LengthOfBaby
		,target.Musculoskeletal
		,target.Neck
		,target.Neurological
		,target.NNUAdmittedReason
		,target.Nose
		,target.PaediatricAlert
		,target.PassingUrine
		,target.PresentAtExamination
		,target.RedReflex
		,target.ReferralsAfterExamination
		,target.RiskFactorsSepsis
		,target.Syndromes
		,target.TemperatureInstability
		,target.Umbilicus
		,target.AbnormalHipsCode
		,target.ENBAdditionalCommentsCode
		,target.RelevantHistory
		,target.SpinalExamination
		,target.HearingScreeningResult
		,target.RelevantPaediatricAlerts
		,target.AbdomenGenitaliaCode
		,target.AbnormalEarsCode
		,target.AbnormalEyesCode
		,target.AbnormalHeadCode
		,target.AbnormalHeartCode
		,target.AbnormalSkinCode
		,target.FemoralPulseCode
		,target.HearingScreeningResultCode
		,target.MusculoskeletalCode
		,target.RedReflexCode
		,target.RelevantHistoryCode
		,target.RelevantPaediatricAlertsCode
		,target.SpinalExaminationCode
		,target.ReasonExaminationDelayed
		,target.ReasonExaminationDelayedCode
		,target.BloodGroup
		,target.BloodGroupCode
		,target.Meconium
		,target.MeconiumCode
		,target.CongenitalAnomalies
		,target.CongenitalAnomaliesCode
		,target.HearingScreenDate
		,target.ExaminationHeartRate
		,target.ExaminationHeartRateCode
		,target.ExaminationRespiratoryRate
		,target.OxygenSaturationLevelDelivery
		,target.ExaminationRespiratoryRateCode
		,target.OxygenSaturationLevelDeliveryCode
		,target.O2SaturationLevelDeliveryPostDuctal
		)

then
	update
	set
		target.UserID = source.UserID
		,target.PregnancyID = source.PregnancyID
		,target.AbnormalFaceMouthPalateCode = source.AbnormalFaceMouthPalateCode
		,target.AppearanceOfBabyCode = source.AppearanceOfBabyCode
		,target.BabyExaminedByCode = source.BabyExaminedByCode
		,target.BabysStoolCode = source.BabysStoolCode
		,target.CardiovascularLungsCode = source.CardiovascularLungsCode
		,target.ENBMotherPresentCode = source.ENBMotherPresentCode
		,target.ExaminationConveyedToParentsCode = source.ExaminationConveyedToParentsCode
		,target.ExaminationOfNewBornCode = source.ExaminationOfNewBornCode
		,target.FaceCode = source.FaceCode
		,target.GenitoUrinaryCode = source.GenitoUrinaryCode
		,target.HistoryOfHeartDiseaseCode = source.HistoryOfHeartDiseaseCode
		,target.InvestigationsCode = source.InvestigationsCode
		,target.LengthOfBabyCode = source.LengthOfBabyCode
		,target.NeckCode = source.NeckCode
		,target.NeurologicalCode = source.NeurologicalCode
		,target.NoseCode = source.NoseCode
		,target.PaediatricAlertCode = source.PaediatricAlertCode
		,target.PassingUrineCode = source.PassingUrineCode
		,target.PresentAtExaminationCode = source.PresentAtExaminationCode
		,target.ReferralsAfterExaminationCode = source.ReferralsAfterExaminationCode
		,target.RiskFactorsSepsisCode = source.RiskFactorsSepsisCode
		,target.SyndromesCode = source.SyndromesCode
		,target.TemperatureInstabilityCode = source.TemperatureInstabilityCode
		,target.UmbilicusCode = source.UmbilicusCode
		,target.AbdomenGenitalia = source.AbdomenGenitalia
		,target.AbnormalEars = source.AbnormalEars
		,target.AbnormalEyes = source.AbnormalEyes
		,target.AbnormalFaceMouthPalate = source.AbnormalFaceMouthPalate
		,target.AbnormalHead = source.AbnormalHead
		,target.AbnormalHeart = source.AbnormalHeart
		,target.AbnormalHips = source.AbnormalHips
		,target.AbnormalScalpSkull = source.AbnormalScalpSkull
		,target.AbnormalSkin = source.AbnormalSkin
		,target.AdmitToSCBU = source.AdmitToSCBU
		,target.AppearanceOfBaby = source.AppearanceOfBaby
		,target.BabyExaminedBy = source.BabyExaminedBy
		,target.BabysStool = source.BabysStool
		,target.CardiovascularLungs = source.CardiovascularLungs
		,target.ENBAdditionalComments = source.ENBAdditionalComments
		,target.ENBMeconium = source.ENBMeconium
		,target.ENBMotherPresent = source.ENBMotherPresent
		,target.ENBUrine = source.ENBUrine
		,target.ExaminationConveyedToParents = source.ExaminationConveyedToParents
		,target.ExaminationDT = source.ExaminationDT
		,target.ExaminationOfNewBorn = source.ExaminationOfNewBorn
		,target.Face = source.Face
		,target.FemoralPulse = source.FemoralPulse
		,target.GenitoUrinary = source.GenitoUrinary
		,target.Gestation = source.Gestation
		,target.HeadCirc1stExam = source.HeadCirc1stExam
		,target.HeadCircumferenceExamination = source.HeadCircumferenceExamination
		,target.HistoryOfHeartDisease = source.HistoryOfHeartDisease
		,target.Investigations = source.Investigations
		,target.LengthOfBaby = source.LengthOfBaby
		,target.Musculoskeletal = source.Musculoskeletal
		,target.Neck = source.Neck
		,target.Neurological = source.Neurological
		,target.NNUAdmittedReason = source.NNUAdmittedReason
		,target.Nose = source.Nose
		,target.PaediatricAlert = source.PaediatricAlert
		,target.PassingUrine = source.PassingUrine
		,target.PresentAtExamination = source.PresentAtExamination
		,target.RedReflex = source.RedReflex
		,target.ReferralsAfterExamination = source.ReferralsAfterExamination
		,target.RiskFactorsSepsis = source.RiskFactorsSepsis
		,target.Syndromes = source.Syndromes
		,target.TemperatureInstability = source.TemperatureInstability
		,target.Umbilicus = source.Umbilicus
		,target.AbnormalHipsCode = source.AbnormalHipsCode
		,target.ENBAdditionalCommentsCode = source.ENBAdditionalCommentsCode
		,target.RelevantHistory = source.RelevantHistory
		,target.SpinalExamination = source.SpinalExamination
		,target.HearingScreeningResult = source.HearingScreeningResult
		,target.RelevantPaediatricAlerts = source.RelevantPaediatricAlerts
		,target.AbdomenGenitaliaCode = source.AbdomenGenitaliaCode
		,target.AbnormalEarsCode = source.AbnormalEarsCode
		,target.AbnormalEyesCode = source.AbnormalEyesCode
		,target.AbnormalHeadCode = source.AbnormalHeadCode
		,target.AbnormalHeartCode = source.AbnormalHeartCode
		,target.AbnormalSkinCode = source.AbnormalSkinCode
		,target.FemoralPulseCode = source.FemoralPulseCode
		,target.HearingScreeningResultCode = source.HearingScreeningResultCode
		,target.MusculoskeletalCode = source.MusculoskeletalCode
		,target.RedReflexCode = source.RedReflexCode
		,target.RelevantHistoryCode = source.RelevantHistoryCode
		,target.RelevantPaediatricAlertsCode = source.RelevantPaediatricAlertsCode
		,target.SpinalExaminationCode = source.SpinalExaminationCode
		,target.ReasonExaminationDelayed = source.ReasonExaminationDelayed
		,target.ReasonExaminationDelayedCode = source.ReasonExaminationDelayedCode
		,target.BloodGroup = source.BloodGroup
		,target.BloodGroupCode = source.BloodGroupCode
		,target.Meconium = source.Meconium
		,target.MeconiumCode = source.MeconiumCode
		,target.CongenitalAnomalies = source.CongenitalAnomalies
		,target.CongenitalAnomaliesCode = source.CongenitalAnomaliesCode
		,target.HearingScreenDate = source.HearingScreenDate
		,target.ExaminationHeartRate = source.ExaminationHeartRate
		,target.ExaminationHeartRateCode = source.ExaminationHeartRateCode
		,target.ExaminationRespiratoryRate = source.ExaminationRespiratoryRate
		,target.OxygenSaturationLevelDelivery = source.OxygenSaturationLevelDelivery
		,target.ExaminationRespiratoryRateCode = source.ExaminationRespiratoryRateCode
		,target.OxygenSaturationLevelDeliveryCode = source.OxygenSaturationLevelDeliveryCode
		,target.O2SaturationLevelDeliveryPostDuctal = source.O2SaturationLevelDeliveryPostDuctal

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime