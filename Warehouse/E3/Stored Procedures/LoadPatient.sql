﻿CREATE procedure [E3].[LoadPatient] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			TitleID
			,FullName
			,Forename
			,Surname
			,IsAdopted
			,ArchiveID
			,IsBabyBornOutsideTrust
			,CountryBirthID
			,DateOfBirth
			,EthnicOriginID
			,GPUsualCareID
			,GenderID
			,HasArchiveRecord
			,HospitalNumber
			,IsInfant
			,LocalAuthorityID
			,LanguageID
			,MaritalStatusID
			,NHSNumber
			,NHSNumberRequestedBy
			,NN4BChildHealthOrg
			,NN4BMessage
			,NN4BOrganisationCode
			,NN4BRequestedBy
			,NN4BTime
			,Occupation
			,OverseasVisitorID
			,PASUnitReferenceNumber
			,PPS
			,PartnerOccupation
			,PCTID
			,ReligionID
			,BabyID
			,EKRecord
			,GUID
			,UserID
			,LastUpdated
			,GPUsualPersonID
			,SystemNumber
			,BirthSurname
			,PatientNotesMemoID
			,GPUsualGPAddressID
			,GPUsualPracticeAddressID
			,PASInfoID
			,ChildrenCentreID
			,DeathCauseMain
			,DeathCauseOther
			,DeathContact
			,DeathCoroner
			,DeathCoronerPM
			,DateOfDeath
			,DeathDiseaseMaternal
			,DeathOtherMaternal
			,DeathOtherRelevant
			,DeathPlace
			,DeathPM
			,DeathPMConsent
			,DeathPMDT
			,DeathPMPlace
			,PostMortem
			,BoardNumber
			,NHSVerificationID
			,DeathLocation
			,IsDeathPMConsentRequired
			,BloodGroup
			,PatientNotesConfidential
			)
into
	#TLoadE3Patient
from
	(
	select
		PatientID
		,TitleID = cast(TitleID as int)
		,FullName = cast(nullif(FullName, '') as nvarchar(100))
		,Forename = cast(nullif(Forename, '') as nvarchar(100))
		,Surname = cast(nullif(Surname, '') as nvarchar(100))
		,IsAdopted = cast(IsAdopted as bit)
		,ArchiveID = cast(ArchiveID as int)
		,IsBabyBornOutsideTrust = cast(IsBabyBornOutsideTrust as bit)
		,CountryBirthID = cast(CountryBirthID as int)
		,DateOfBirth = cast(DateOfBirth as datetime)
		,EthnicOriginID = cast(EthnicOriginID as int)
		,GPUsualCareID = cast(GPUsualCareID as int)
		,GenderID = cast(GenderID as int)
		,HasArchiveRecord = cast(HasArchiveRecord as bit)
		,HospitalNumber = cast(nullif(HospitalNumber, '') as nvarchar(40))
		,IsInfant = cast(IsInfant as bit)
		,LocalAuthorityID = cast(LocalAuthorityID as int)
		,LanguageID = cast(LanguageID as int)
		,MaritalStatusID = cast(MaritalStatusID as int)
		,NHSNumber = cast(nullif(NHSNumber, '') as nvarchar(30))
		,NHSNumberRequestedBy = cast(NHSNumberRequestedBy as int)
		,NN4BChildHealthOrg = cast(nullif(NN4BChildHealthOrg, '') as nvarchar(100))
		,NN4BMessage = cast(nullif(NN4BMessage, '') as nvarchar(510))
		,NN4BOrganisationCode = cast(nullif(NN4BOrganisationCode, '') as nvarchar(100))
		,NN4BRequestedBy = cast(nullif(NN4BRequestedBy, '') as nvarchar(100))
		,NN4BTime = cast(nullif(NN4BTime, '') as nvarchar(100))
		,Occupation = cast(nullif(Occupation, '') as nvarchar(100))
		,OverseasVisitorID = cast(OverseasVisitorID as int)
		,PASUnitReferenceNumber = cast(nullif(PASUnitReferenceNumber, '') as nvarchar(100))
		,PPS = cast(nullif(PPS, '') as nvarchar(20))
		,PartnerOccupation = cast(nullif(PartnerOccupation, '') as nvarchar(100))
		,PCTID = cast(PCTID as int)
		,ReligionID = cast(ReligionID as int)
		,BabyID = cast(BabyID as bigint)
		,EKRecord = cast(EKRecord as bit)
		,GUID = cast(GUID as uniqueidentifier)
		,UserID = cast(UserID as int)
		,LastUpdated = cast(LastUpdated as smalldatetime)
		,GPUsualPersonID = cast(GPUsualPersonID as int)
		,SystemNumber = cast(nullif(SystemNumber, '') as nvarchar(40))
		,BirthSurname = cast(nullif(BirthSurname, '') as nvarchar(100))
		,PatientNotesMemoID = cast(PatientNotesMemoID as bigint)
		,GPUsualGPAddressID = cast(GPUsualGPAddressID as int)
		,GPUsualPracticeAddressID = cast(GPUsualPracticeAddressID as int)
		,PASInfoID = cast(PASInfoID as int)
		,ChildrenCentreID = cast(ChildrenCentreID as int)
		,DeathCauseMain = cast(nullif(DeathCauseMain, '') as nvarchar(510))
		,DeathCauseOther = cast(nullif(DeathCauseOther, '') as nvarchar(510))
		,DeathContact = cast(nullif(DeathContact, '') as nvarchar(510))
		,DeathCoroner = cast(nullif(DeathCoroner, '') as nvarchar(510))
		,DeathCoronerPM = cast(nullif(DeathCoronerPM, '') as nvarchar(510))
		,DateOfDeath = cast(DateOfDeath as datetime)
		,DeathDiseaseMaternal = cast(nullif(DeathDiseaseMaternal, '') as nvarchar(510))
		,DeathOtherMaternal = cast(nullif(DeathOtherMaternal, '') as nvarchar(510))
		,DeathOtherRelevant = cast(nullif(DeathOtherRelevant, '') as nvarchar(510))
		,DeathPlace = cast(nullif(DeathPlace, '') as nvarchar(510))
		,DeathPM = cast(nullif(DeathPM, '') as nvarchar(510))
		,DeathPMConsent = cast(nullif(DeathPMConsent, '') as nvarchar(510))
		,DeathPMDT = cast(DeathPMDT as datetime)
		,DeathPMPlace = cast(nullif(DeathPMPlace, '') as nvarchar(510))
		,PostMortem = cast(nullif(PostMortem, '') as nvarchar(510))
		,BoardNumber = cast(nullif(BoardNumber, '') as nvarchar(20))
		,NHSVerificationID = cast(NHSVerificationID as int)
		,DeathLocation = cast(nullif(DeathLocation, '') as nvarchar(510))
		,IsDeathPMConsentRequired = cast(IsDeathPMConsentRequired as bit)
		,BloodGroup = cast(nullif(BloodGroup, '') as nvarchar(50))
		,PatientNotesConfidential = cast(PatientNotesConfidential as bit)
	from
		(
		select
			PatientID = Patient.PatientID
			,TitleID = Patient.TitleID
			,FullName = Patient.FullName
			,Forename = Patient.Forenames
			,Surname = Patient.Surname
			,IsAdopted = Patient.Adopted
			,ArchiveID = Patient.ArchiveID
			,IsBabyBornOutsideTrust = Patient.BabyBornOutsideTrust
			,CountryBirthID = Patient.CountryBirthID
			,DateOfBirth = Patient.DoB
			,EthnicOriginID = Patient.EthnicOriginID
			,GPUsualCareID = Patient.GPUsualCare
			,GenderID = Patient.GenderID
			,HasArchiveRecord = Patient.HasArchiveRecord
			,HospitalNumber = Patient.HospitalNumber
			,IsInfant = Patient.Infant
			,LocalAuthorityID = Patient.LocalAuthorityID
			,LanguageID = Patient.LanguageID
			,MaritalStatusID = Patient.MaritalStatusID
			,NHSNumber = Patient.NHSNumber
			,NHSNumberRequestedBy = Patient.NHSNumberRequestedBy
			,NN4BChildHealthOrg = Patient.NN4BChildHealthOrg
			,NN4BMessage = Patient.NN4BMessage
			,NN4BOrganisationCode = Patient.NN4BOrganisationCode
			,NN4BRequestedBy = Patient.NN4BRequestedBy
			,NN4BTime = Patient.NN4BTime
			,Occupation = Patient.Occupation
			,OverseasVisitorID = Patient.OverseasVisitorID
			,PASUnitReferenceNumber = Patient.PASUnitReferenceNumber
			,PPS = Patient.PPS
			,PartnerOccupation = Patient.PartnerOccupation
			,PCTID = Patient.PCTID
			,ReligionID = Patient.ReligionID
			,BabyID = Patient.BabyID
			,EKRecord = Patient.EK_Record
			,GUID = Patient.GUID
			,UserID = Patient.UserID
			,LastUpdated = Patient.LastUpdated
			,GPUsualPersonID = Patient.GPUsualPersonID
			,SystemNumber = Patient.SystemNumber
			,BirthSurname = Patient.BirthSurname
			,PatientNotesMemoID = Patient.PatientNotesMemoID
			,GPUsualGPAddressID = Patient.GPUsualGPAddressID
			,GPUsualPracticeAddressID = Patient.GPUsualPracticeAddressID
			,PASInfoID = Patient.PASInfoID
			,ChildrenCentreID = Patient.ChildrenCentreID
			,DeathCauseMain = Patient.DeathCauseMain
			,DeathCauseOther = Patient.DeathCauseOther
			,DeathContact = Patient.DeathContact
			,DeathCoroner = Patient.DeathCoroner
			,DeathCoronerPM = Patient.DeathCoronerPM
			,DateOfDeath = Patient.DeathDate
			,DeathDiseaseMaternal = Patient.DeathDiseaseMaternal
			,DeathOtherMaternal = Patient.DeathOtherMaternal
			,DeathOtherRelevant = Patient.DeathOtherRelevant
			,DeathPlace = Patient.DeathPlace
			,DeathPM = Patient.DeathPM
			,DeathPMConsent = Patient.DeathPMConsent
			,DeathPMDT = Patient.DeathPMDT
			,DeathPMPlace = Patient.DeathPMPlace
			,PostMortem = Patient.PostMortem
			,BoardNumber = Patient.BoardNumber
			,NHSVerificationID = Patient.NHSVerificationID
			,DeathLocation = Patient.DeathLocationDescription
			,IsDeathPMConsentRequired = Patient.DeathPMConsentRequired
			,BloodGroup = Patient.BloodGroup
			,PatientNotesConfidential = Patient.PatientNotesConfidential
		from
			E3.dbo.Patient Patient
		where
			Patient.Deleted <> 1

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3Patient on #TLoadE3Patient
	(
	PatientID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	E3.Patient target
using
	(
	select
		*
	from
		#TLoadE3Patient
	
	) source
	on	source.PatientID = target.PatientID

when not matched by source
then
	delete

when not matched
then
	insert
		(
		PatientID
		,TitleID
		,FullName
		,Forename
		,Surname
		,IsAdopted
		,ArchiveID
		,IsBabyBornOutsideTrust
		,CountryBirthID
		,DateOfBirth
		,EthnicOriginID
		,GPUsualCareID
		,GenderID
		,HasArchiveRecord
		,HospitalNumber
		,IsInfant
		,LocalAuthorityID
		,LanguageID
		,MaritalStatusID
		,NHSNumber
		,NHSNumberRequestedBy
		,NN4BChildHealthOrg
		,NN4BMessage
		,NN4BOrganisationCode
		,NN4BRequestedBy
		,NN4BTime
		,Occupation
		,OverseasVisitorID
		,PASUnitReferenceNumber
		,PPS
		,PartnerOccupation
		,PCTID
		,ReligionID
		,BabyID
		,EKRecord
		,GUID
		,UserID
		,LastUpdated
		,GPUsualPersonID
		,SystemNumber
		,BirthSurname
		,PatientNotesMemoID
		,GPUsualGPAddressID
		,GPUsualPracticeAddressID
		,PASInfoID
		,ChildrenCentreID
		,DeathCauseMain
		,DeathCauseOther
		,DeathContact
		,DeathCoroner
		,DeathCoronerPM
		,DateOfDeath
		,DeathDiseaseMaternal
		,DeathOtherMaternal
		,DeathOtherRelevant
		,DeathPlace
		,DeathPM
		,DeathPMConsent
		,DeathPMDT
		,DeathPMPlace
		,PostMortem
		,BoardNumber
		,NHSVerificationID
		,DeathLocation
		,IsDeathPMConsentRequired
		,BloodGroup
		,PatientNotesConfidential

		,Created
		,ByWhom
		)
	values
		(
		source.PatientID
		,source.TitleID
		,source.FullName
		,source.Forename
		,source.Surname
		,source.IsAdopted
		,source.ArchiveID
		,source.IsBabyBornOutsideTrust
		,source.CountryBirthID
		,source.DateOfBirth
		,source.EthnicOriginID
		,source.GPUsualCareID
		,source.GenderID
		,source.HasArchiveRecord
		,source.HospitalNumber
		,source.IsInfant
		,source.LocalAuthorityID
		,source.LanguageID
		,source.MaritalStatusID
		,source.NHSNumber
		,source.NHSNumberRequestedBy
		,source.NN4BChildHealthOrg
		,source.NN4BMessage
		,source.NN4BOrganisationCode
		,source.NN4BRequestedBy
		,source.NN4BTime
		,source.Occupation
		,source.OverseasVisitorID
		,source.PASUnitReferenceNumber
		,source.PPS
		,source.PartnerOccupation
		,source.PCTID
		,source.ReligionID
		,source.BabyID
		,source.EKRecord
		,source.GUID
		,source.UserID
		,source.LastUpdated
		,source.GPUsualPersonID
		,source.SystemNumber
		,source.BirthSurname
		,source.PatientNotesMemoID
		,source.GPUsualGPAddressID
		,source.GPUsualPracticeAddressID
		,source.PASInfoID
		,source.ChildrenCentreID
		,source.DeathCauseMain
		,source.DeathCauseOther
		,source.DeathContact
		,source.DeathCoroner
		,source.DeathCoronerPM
		,source.DateOfDeath
		,source.DeathDiseaseMaternal
		,source.DeathOtherMaternal
		,source.DeathOtherRelevant
		,source.DeathPlace
		,source.DeathPM
		,source.DeathPMConsent
		,source.DeathPMDT
		,source.DeathPMPlace
		,source.PostMortem
		,source.BoardNumber
		,source.NHSVerificationID
		,source.DeathLocation
		,source.IsDeathPMConsentRequired
		,source.BloodGroup
		,source.PatientNotesConfidential

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.TitleID
		,target.FullName
		,target.Forename
		,target.Surname
		,target.IsAdopted
		,target.ArchiveID
		,target.IsBabyBornOutsideTrust
		,target.CountryBirthID
		,target.DateOfBirth
		,target.EthnicOriginID
		,target.GPUsualCareID
		,target.GenderID
		,target.HasArchiveRecord
		,target.HospitalNumber
		,target.IsInfant
		,target.LocalAuthorityID
		,target.LanguageID
		,target.MaritalStatusID
		,target.NHSNumber
		,target.NHSNumberRequestedBy
		,target.NN4BChildHealthOrg
		,target.NN4BMessage
		,target.NN4BOrganisationCode
		,target.NN4BRequestedBy
		,target.NN4BTime
		,target.Occupation
		,target.OverseasVisitorID
		,target.PASUnitReferenceNumber
		,target.PPS
		,target.PartnerOccupation
		,target.PCTID
		,target.ReligionID
		,target.BabyID
		,target.EKRecord
		,target.GUID
		,target.UserID
		,target.LastUpdated
		,target.GPUsualPersonID
		,target.SystemNumber
		,target.BirthSurname
		,target.PatientNotesMemoID
		,target.GPUsualGPAddressID
		,target.GPUsualPracticeAddressID
		,target.PASInfoID
		,target.ChildrenCentreID
		,target.DeathCauseMain
		,target.DeathCauseOther
		,target.DeathContact
		,target.DeathCoroner
		,target.DeathCoronerPM
		,target.DateOfDeath
		,target.DeathDiseaseMaternal
		,target.DeathOtherMaternal
		,target.DeathOtherRelevant
		,target.DeathPlace
		,target.DeathPM
		,target.DeathPMConsent
		,target.DeathPMDT
		,target.DeathPMPlace
		,target.PostMortem
		,target.BoardNumber
		,target.NHSVerificationID
		,target.DeathLocation
		,target.IsDeathPMConsentRequired
		,target.BloodGroup
		,target.PatientNotesConfidential
		)

then
	update
	set
		target.TitleID = source.TitleID
		,target.FullName = source.FullName
		,target.Forename = source.Forename
		,target.Surname = source.Surname
		,target.IsAdopted = source.IsAdopted
		,target.ArchiveID = source.ArchiveID
		,target.IsBabyBornOutsideTrust = source.IsBabyBornOutsideTrust
		,target.CountryBirthID = source.CountryBirthID
		,target.DateOfBirth = source.DateOfBirth
		,target.EthnicOriginID = source.EthnicOriginID
		,target.GPUsualCareID = source.GPUsualCareID
		,target.GenderID = source.GenderID
		,target.HasArchiveRecord = source.HasArchiveRecord
		,target.HospitalNumber = source.HospitalNumber
		,target.IsInfant = source.IsInfant
		,target.LocalAuthorityID = source.LocalAuthorityID
		,target.LanguageID = source.LanguageID
		,target.MaritalStatusID = source.MaritalStatusID
		,target.NHSNumber = source.NHSNumber
		,target.NHSNumberRequestedBy = source.NHSNumberRequestedBy
		,target.NN4BChildHealthOrg = source.NN4BChildHealthOrg
		,target.NN4BMessage = source.NN4BMessage
		,target.NN4BOrganisationCode = source.NN4BOrganisationCode
		,target.NN4BRequestedBy = source.NN4BRequestedBy
		,target.NN4BTime = source.NN4BTime
		,target.Occupation = source.Occupation
		,target.OverseasVisitorID = source.OverseasVisitorID
		,target.PASUnitReferenceNumber = source.PASUnitReferenceNumber
		,target.PPS = source.PPS
		,target.PartnerOccupation = source.PartnerOccupation
		,target.PCTID = source.PCTID
		,target.ReligionID = source.ReligionID
		,target.BabyID = source.BabyID
		,target.EKRecord = source.EKRecord
		,target.GUID = source.GUID
		,target.UserID = source.UserID
		,target.LastUpdated = source.LastUpdated
		,target.GPUsualPersonID = source.GPUsualPersonID
		,target.SystemNumber = source.SystemNumber
		,target.BirthSurname = source.BirthSurname
		,target.PatientNotesMemoID = source.PatientNotesMemoID
		,target.GPUsualGPAddressID = source.GPUsualGPAddressID
		,target.GPUsualPracticeAddressID = source.GPUsualPracticeAddressID
		,target.PASInfoID = source.PASInfoID
		,target.ChildrenCentreID = source.ChildrenCentreID
		,target.DeathCauseMain = source.DeathCauseMain
		,target.DeathCauseOther = source.DeathCauseOther
		,target.DeathContact = source.DeathContact
		,target.DeathCoroner = source.DeathCoroner
		,target.DeathCoronerPM = source.DeathCoronerPM
		,target.DateOfDeath = source.DateOfDeath
		,target.DeathDiseaseMaternal = source.DeathDiseaseMaternal
		,target.DeathOtherMaternal = source.DeathOtherMaternal
		,target.DeathOtherRelevant = source.DeathOtherRelevant
		,target.DeathPlace = source.DeathPlace
		,target.DeathPM = source.DeathPM
		,target.DeathPMConsent = source.DeathPMConsent
		,target.DeathPMDT = source.DeathPMDT
		,target.DeathPMPlace = source.DeathPMPlace
		,target.PostMortem = source.PostMortem
		,target.BoardNumber = source.BoardNumber
		,target.NHSVerificationID = source.NHSVerificationID
		,target.DeathLocation = source.DeathLocation
		,target.IsDeathPMConsentRequired = source.IsDeathPMConsentRequired
		,target.BloodGroup = source.BloodGroup
		,target.PatientNotesConfidential = source.PatientNotesConfidential

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime