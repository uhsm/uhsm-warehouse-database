﻿CREATE procedure [E3].[LoadPatientAddress] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			AddressTypeID
			,PatientID
			,BabyID
			,AddressID
			,EKRecord
			,GUID
			,UserID
			,LastUpdatedDateTime
			)
into
	#TLoadE3PatientAddress
from
	(
	select
		PatientAddressID
		,AddressTypeID = cast(AddressTypeID as int)
		,PatientID = cast(PatientID as bigint)
		,BabyID = cast(BabyID as bigint)
		,AddressID = cast(AddressID as bigint)
		,EKRecord = cast(EKRecord as bit)
		,GUID = cast(GUID as uniqueidentifier)
		,UserID = cast(UserID as int)
		,LastUpdatedDateTime = cast(LastUpdatedDateTime as smalldatetime)
	from
		(
		select
			PatientAddressID = PatientAddress.PatientAddressID
			,AddressTypeID = PatientAddress.AddressTypeID
			,PatientID = PatientAddress.PatientID
			,BabyID = PatientAddress.BabyID
			,AddressID = PatientAddress.AddressID
			,EKRecord = PatientAddress.EK_Record
			,GUID = PatientAddress.GUID
			,UserID = PatientAddress.UserID
			,LastUpdatedDateTime = PatientAddress.LastUpdated
		from
			E3.dbo.PatientAddress PatientAddress
		where
			PatientAddress.Deleted <> 1

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3PatientAddress on #TLoadE3PatientAddress
	(
	PatientAddressID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	E3.PatientAddress target
using
	(
	select
		*
	from
		#TLoadE3PatientAddress
	
	) source
	on	source.PatientAddressID = target.PatientAddressID

when not matched by source
then
	delete

when not matched
then
	insert
		(
		PatientAddressID
		,AddressTypeID
		,PatientID
		,BabyID
		,AddressID
		,EKRecord
		,GUID
		,UserID
		,LastUpdatedDateTime

		,Created
		,ByWhom
		)
	values
		(
		source.PatientAddressID
		,source.AddressTypeID
		,source.PatientID
		,source.BabyID
		,source.AddressID
		,source.EKRecord
		,source.GUID
		,source.UserID
		,source.LastUpdatedDateTime

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.AddressTypeID
		,target.PatientID
		,target.BabyID
		,target.AddressID
		,target.EKRecord
		,target.GUID
		,target.UserID
		,target.LastUpdatedDateTime
		)

then
	update
	set
		target.AddressTypeID = source.AddressTypeID
		,target.PatientID = source.PatientID
		,target.BabyID = source.BabyID
		,target.AddressID = source.AddressID
		,target.EKRecord = source.EKRecord
		,target.GUID = source.GUID
		,target.UserID = source.UserID
		,target.LastUpdatedDateTime = source.LastUpdatedDateTime

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime