﻿CREATE procedure [E3].[LoadPerson] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			Forename
			,Surname
			,TitleID
			,PersonTypeID
			,StaffGradeID
			,Qualifications
			,Prescribe
			,GMCCode
			,PASCode
			,Person
			,StaffID
			,EKRecord
			,GUID
			,UserID
			,LastUpdated
			,GPPracticeID
			,IsTriumUser
			)
into
	#TLoadE3Person
from
	(
	select
		PersonID
		,Forename = cast(nullif(Forename, '') as nvarchar(100))
		,Surname = cast(nullif(Surname, '') as nvarchar(100))
		,TitleID = cast(TitleID as int)
		,PersonTypeID = cast(PersonTypeID as int)
		,StaffGradeID = cast(StaffGradeID as int)
		,Qualifications = cast(nullif(Qualifications, '') as nvarchar(100))
		,Prescribe = cast(Prescribe as bit)
		,GMCCode = cast(nullif(GMCCode, '') as nvarchar(40))
		,PASCode = cast(nullif(PASCode, '') as nvarchar(100))
		,Person = cast(nullif(Person, '') as nvarchar(100))
		,StaffID = cast(StaffID as int)
		,EKRecord = cast(EKRecord as bit)
		,GUID = cast(GUID as uniqueidentifier)
		,UserID = cast(UserID as int)
		,LastUpdated = cast(LastUpdated as smalldatetime)
		,GPPracticeID = cast(GPPracticeID as int)
		,IsTriumUser = cast(IsTriumUser as bit)
	from
		(
		select
			PersonID = Person.PersonID
			,Forename = Person.Forenames
			,Surname = Person.Surname
			,TitleID = Person.TitleID
			,PersonTypeID = Person.PersonTypeID
			,StaffGradeID = Person.StaffGradeID
			,Qualifications = Person.Qualifications
			,Prescribe = Person.Prescribe
			,GMCCode = Person.GMCCode
			,PASCode = Person.PASCode
			,Person = Person.Description
			,StaffID = Person.StaffID
			,EKRecord = Person.EK_Record
			,GUID = Person.GUID
			,UserID = Person.UserID
			,LastUpdated = Person.LastUpdated
			,GPPracticeID = Person.GPPracticeID
			,IsTriumUser = Person.TriumUser
		from
			E3.dbo.Person Person
		where
			Person.Deleted <> 1

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3Person on #TLoadE3Person
	(
	PersonID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	E3.Person target
using
	(
	select
		*
	from
		#TLoadE3Person
	
	) source
	on	source.PersonID = target.PersonID

when not matched by source
then
	delete

when not matched
then
	insert
		(
		PersonID
		,Forename
		,Surname
		,TitleID
		,PersonTypeID
		,StaffGradeID
		,Qualifications
		,Prescribe
		,GMCCode
		,PASCode
		,Person
		,StaffID
		,EKRecord
		,GUID
		,UserID
		,LastUpdated
		,GPPracticeID
		,IsTriumUser

		,Created
		,ByWhom
		)
	values
		(
		source.PersonID
		,source.Forename
		,source.Surname
		,source.TitleID
		,source.PersonTypeID
		,source.StaffGradeID
		,source.Qualifications
		,source.Prescribe
		,source.GMCCode
		,source.PASCode
		,source.Person
		,source.StaffID
		,source.EKRecord
		,source.GUID
		,source.UserID
		,source.LastUpdated
		,source.GPPracticeID
		,source.IsTriumUser

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.Forename
		,target.Surname
		,target.TitleID
		,target.PersonTypeID
		,target.StaffGradeID
		,target.Qualifications
		,target.Prescribe
		,target.GMCCode
		,target.PASCode
		,target.Person
		,target.StaffID
		,target.EKRecord
		,target.GUID
		,target.UserID
		,target.LastUpdated
		,target.GPPracticeID
		,target.IsTriumUser
		)

then
	update
	set
		target.Forename = source.Forename
		,target.Surname = source.Surname
		,target.TitleID = source.TitleID
		,target.PersonTypeID = source.PersonTypeID
		,target.StaffGradeID = source.StaffGradeID
		,target.Qualifications = source.Qualifications
		,target.Prescribe = source.Prescribe
		,target.GMCCode = source.GMCCode
		,target.PASCode = source.PASCode
		,target.Person = source.Person
		,target.StaffID = source.StaffID
		,target.EKRecord = source.EKRecord
		,target.GUID = source.GUID
		,target.UserID = source.UserID
		,target.LastUpdated = source.LastUpdated
		,target.GPPracticeID = source.GPPracticeID
		,target.IsTriumUser = source.IsTriumUser

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime