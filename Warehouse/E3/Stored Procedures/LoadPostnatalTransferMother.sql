﻿CREATE procedure [E3].[LoadPostnatalTransferMother] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 UserID
			,PregnancyID
			,TransfusionProblemsCode
			,TransfusionProblems
			,ActionFollowingCounsellingCode
			,AgenciesCode
			,AntiDCode
			,AntiDGivenByCode
			,AppointmentsCode
			,BloodTransfusionRQDCode
			,BowelProblemsCode
			,BPCode
			,BreastExamCode
			,ContactTelephoneCode
			,CounselledByCode
			,CounsellingOrDebriefingCode
			,DestinationCode
			,DischargedByCode
			,EmotionalStateCode
			,FeedingMethodCode
			,FinalDischargeCode
			,HbCode
			,InformationGivenCode
			,LegExamCode
			,LochiaCode
			,ModeCode
			,OtherPNProblemsCode
			,PerineumCode
			,ProceduresDoneCode
			,PulseCode
			,ReasonCode
			,ReferralsMadeCode
			,RoutinePNCareCode
			,RubellaCode
			,RubellaSerologyTestCode
			,SmokerDischargeCode
			,SupervisedByCode
			,TemperatureCode
			,TransferredFromCode
			,UterusCode
			,ActionFollowingCounselling
			,AdditionalComments
			,AdmissionDT
			,Agencies
			,AntiD
			,AntiDBatchNo
			,AntiDDate
			,AntiDGivenBy
			,Appointments
			,BloodsTaken
			,BloodTransfusionRQD
			,BowelProblems
			,BP
			,BPProblems
			,BreastExam
			,BreastFeedSupport
			,CMRingWard
			,ContactTelephone
			,CounselledBy
			,CounsellingOrDebriefing
			,Debriefing
			,Destination
			,DischargedBy
			,DischargedDT
			,EmotionalState
			,FeedingMethod
			,FinalDischarge
			,Hb
			,InformationGiven
			,IntravenousFluids
			,LegExam
			,Lochia
			,Medication
			,MicturitionProblems
			,Mode
			,ObsReviewReq
			,ObstetricDecision
			,OtherPNProblems
			,OtherTests
			,ParentEducation
			,Perineum
			,PerineumManagement
			,ProceduresDone
			,Pulse
			,PulseRateProblems
			,Reason
			,ReferralsMade
			,RoutinePNCare
			,Rubella
			,RubellaSerologyTest
			,SmearRequired
			,SmokerDischarge
			,SupervisedBy
			,SupportGroups
			,Temperature
			,TemperatureProblems
			,TransferredFrom
			,Urinalysis
			,Uterus
			,Wound
			,AdditionalCommentsCode
			,PerinealFollowUP
			,PerinealFollowUPCode
			,UrinePostDelivery
			,UrineDT
			,UrineVolume
			,HbResult
			,HbResultCode
			,AntiDDosePostNatal
			,AntiDDosePostNatalCode
			,DischargeMethodCode
			,DischargeMethod
			,PNInvestigations
			,PNInvestigationsCode
			,PNBloodTests
			,PNBloodTestsCode
			,RespirationRate
			,RespirationRateCode
			,MicturitionProblemsCode
			,WoundCode
			,MedicationCode
			,ThromboembolicRiskPN
			,TeamPN
			,ThromboembolicRiskPNCode
			,TeamPNCode
			,TransferredWithBaby
			,TransferredWithBabyCode
			,DiscussionsDuringPNAdmission
			,DiscussionsDuringPNAdmissionCode
			,FGMInformationImplications
			,FGMInformationImplicationsCode
			,FGMInformationIllegality
			)
into
	#TLoadE3PostnatalTransferMother
from
	(
	select
		UserID = cast(UserID as int)
		,PregnancyID = cast(PregnancyID as bigint)
		,PregnancyAdmissionID = cast(PregnancyAdmissionID as bigint)
		,TransfusionProblemsCode = cast(TransfusionProblemsCode as bigint)
		,TransfusionProblems = cast(nullif(TransfusionProblems, '') as nvarchar(2000))
		,ActionFollowingCounsellingCode = cast(ActionFollowingCounsellingCode as bigint)
		,AgenciesCode = cast(AgenciesCode as bigint)
		,AntiDCode = cast(AntiDCode as bigint)
		,AntiDGivenByCode = cast(AntiDGivenByCode as bigint)
		,AppointmentsCode = cast(AppointmentsCode as bigint)
		,BloodTransfusionRQDCode = cast(BloodTransfusionRQDCode as bigint)
		,BowelProblemsCode = cast(BowelProblemsCode as bigint)
		,BPCode = cast(BPCode as bigint)
		,BreastExamCode = cast(BreastExamCode as bigint)
		,ContactTelephoneCode = cast(ContactTelephoneCode as bigint)
		,CounselledByCode = cast(CounselledByCode as bigint)
		,CounsellingOrDebriefingCode = cast(CounsellingOrDebriefingCode as bigint)
		,DestinationCode = cast(DestinationCode as bigint)
		,DischargedByCode = cast(DischargedByCode as bigint)
		,EmotionalStateCode = cast(EmotionalStateCode as bigint)
		,FeedingMethodCode = cast(FeedingMethodCode as bigint)
		,FinalDischargeCode = cast(FinalDischargeCode as bigint)
		,HbCode = cast(HbCode as bigint)
		,InformationGivenCode = cast(InformationGivenCode as bigint)
		,LegExamCode = cast(LegExamCode as bigint)
		,LochiaCode = cast(LochiaCode as bigint)
		,ModeCode = cast(ModeCode as bigint)
		,OtherPNProblemsCode = cast(OtherPNProblemsCode as bigint)
		,PerineumCode = cast(PerineumCode as bigint)
		,ProceduresDoneCode = cast(ProceduresDoneCode as bigint)
		,PulseCode = cast(PulseCode as bigint)
		,ReasonCode = cast(ReasonCode as bigint)
		,ReferralsMadeCode = cast(ReferralsMadeCode as bigint)
		,RoutinePNCareCode = cast(RoutinePNCareCode as bigint)
		,RubellaCode = cast(RubellaCode as bigint)
		,RubellaSerologyTestCode = cast(RubellaSerologyTestCode as bigint)
		,SmokerDischargeCode = cast(SmokerDischargeCode as bigint)
		,SupervisedByCode = cast(SupervisedByCode as bigint)
		,TemperatureCode = cast(TemperatureCode as bigint)
		,TransferredFromCode = cast(TransferredFromCode as bigint)
		,UterusCode = cast(UterusCode as bigint)
		,ActionFollowingCounselling = cast(nullif(ActionFollowingCounselling, '') as nvarchar(2000))
		,AdditionalComments = cast(nullif(AdditionalComments, '') as nvarchar(2000))
		,AdmissionDT = cast(nullif(AdmissionDT, '') as nvarchar(2000))
		,Agencies = cast(nullif(Agencies, '') as nvarchar(2000))
		,AntiD = cast(nullif(AntiD, '') as nvarchar(2000))
		,AntiDBatchNo = cast(nullif(AntiDBatchNo, '') as nvarchar(2000))
		,AntiDDate = cast(nullif(AntiDDate, '') as nvarchar(2000))
		,AntiDGivenBy = cast(nullif(AntiDGivenBy, '') as nvarchar(2000))
		,Appointments = cast(nullif(Appointments, '') as nvarchar(2000))
		,BloodsTaken = cast(nullif(BloodsTaken, '') as nvarchar(2000))
		,BloodTransfusionRQD = cast(nullif(BloodTransfusionRQD, '') as nvarchar(2000))
		,BowelProblems = cast(nullif(BowelProblems, '') as nvarchar(2000))
		,BP = cast(nullif(BP, '') as nvarchar(2000))
		,BPProblems = cast(nullif(BPProblems, '') as nvarchar(2000))
		,BreastExam = cast(nullif(BreastExam, '') as nvarchar(2000))
		,BreastFeedSupport = cast(nullif(BreastFeedSupport, '') as nvarchar(2000))
		,CMRingWard = cast(nullif(CMRingWard, '') as nvarchar(2000))
		,ContactTelephone = cast(nullif(ContactTelephone, '') as nvarchar(2000))
		,CounselledBy = cast(nullif(CounselledBy, '') as nvarchar(2000))
		,CounsellingOrDebriefing = cast(nullif(CounsellingOrDebriefing, '') as nvarchar(2000))
		,Debriefing = cast(nullif(Debriefing, '') as nvarchar(2000))
		,Destination = cast(nullif(Destination, '') as nvarchar(2000))
		,DischargedBy = cast(nullif(DischargedBy, '') as nvarchar(2000))
		,DischargedDT = cast(nullif(DischargedDT, '') as nvarchar(2000))
		,EmotionalState = cast(nullif(EmotionalState, '') as nvarchar(2000))
		,FeedingMethod = cast(nullif(FeedingMethod, '') as nvarchar(2000))
		,FinalDischarge = cast(nullif(FinalDischarge, '') as nvarchar(2000))
		,Hb = cast(nullif(Hb, '') as nvarchar(2000))
		,InformationGiven = cast(nullif(InformationGiven, '') as nvarchar(2000))
		,IntravenousFluids = cast(nullif(IntravenousFluids, '') as nvarchar(2000))
		,LegExam = cast(nullif(LegExam, '') as nvarchar(2000))
		,Lochia = cast(nullif(Lochia, '') as nvarchar(2000))
		,Medication = cast(nullif(Medication, '') as nvarchar(2000))
		,MicturitionProblems = cast(nullif(MicturitionProblems, '') as nvarchar(2000))
		,Mode = cast(nullif(Mode, '') as nvarchar(2000))
		,ObsReviewReq = cast(nullif(ObsReviewReq, '') as nvarchar(2000))
		,ObstetricDecision = cast(nullif(ObstetricDecision, '') as nvarchar(2000))
		,OtherPNProblems = cast(nullif(OtherPNProblems, '') as nvarchar(2000))
		,OtherTests = cast(nullif(OtherTests, '') as nvarchar(2000))
		,ParentEducation = cast(nullif(ParentEducation, '') as nvarchar(2000))
		,Perineum = cast(nullif(Perineum, '') as nvarchar(2000))
		,PerineumManagement = cast(nullif(PerineumManagement, '') as nvarchar(2000))
		,ProceduresDone = cast(nullif(ProceduresDone, '') as nvarchar(2000))
		,Pulse = cast(nullif(Pulse, '') as nvarchar(2000))
		,PulseRateProblems = cast(nullif(PulseRateProblems, '') as nvarchar(2000))
		,Reason = cast(nullif(Reason, '') as nvarchar(2000))
		,ReferralsMade = cast(nullif(ReferralsMade, '') as nvarchar(2000))
		,RoutinePNCare = cast(nullif(RoutinePNCare, '') as nvarchar(2000))
		,Rubella = cast(nullif(Rubella, '') as nvarchar(2000))
		,RubellaSerologyTest = cast(nullif(RubellaSerologyTest, '') as nvarchar(2000))
		,SmearRequired = cast(nullif(SmearRequired, '') as nvarchar(2000))
		,SmokerDischarge = cast(nullif(SmokerDischarge, '') as nvarchar(2000))
		,SupervisedBy = cast(nullif(SupervisedBy, '') as nvarchar(2000))
		,SupportGroups = cast(nullif(SupportGroups, '') as nvarchar(2000))
		,Temperature = cast(nullif(Temperature, '') as nvarchar(2000))
		,TemperatureProblems = cast(nullif(TemperatureProblems, '') as nvarchar(2000))
		,TransferredFrom = cast(nullif(TransferredFrom, '') as nvarchar(2000))
		,Urinalysis = cast(nullif(Urinalysis, '') as nvarchar(2000))
		,Uterus = cast(nullif(Uterus, '') as nvarchar(2000))
		,Wound = cast(nullif(Wound, '') as nvarchar(2000))
		,AdditionalCommentsCode = cast(AdditionalCommentsCode as bigint)
		,PerinealFollowUP = cast(nullif(PerinealFollowUP, '') as nvarchar(2000))
		,PerinealFollowUPCode = cast(PerinealFollowUPCode as bigint)
		,UrinePostDelivery = cast(nullif(UrinePostDelivery, '') as nvarchar(2000))
		,UrineDT = cast(nullif(UrineDT, '') as nvarchar(2000))
		,UrineVolume = cast(nullif(UrineVolume, '') as nvarchar(2000))
		,HbResult = cast(nullif(HbResult, '') as nvarchar(2000))
		,HbResultCode = cast(HbResultCode as bigint)
		,AntiDDosePostNatal = cast(nullif(AntiDDosePostNatal, '') as nvarchar(2000))
		,AntiDDosePostNatalCode = cast(AntiDDosePostNatalCode as bigint)
		,DischargeMethodCode = cast(DischargeMethodCode as bigint)
		,DischargeMethod = cast(nullif(DischargeMethod, '') as nvarchar(2000))
		,PNInvestigations = cast(nullif(PNInvestigations, '') as nvarchar(2000))
		,PNInvestigationsCode = cast(PNInvestigationsCode as bigint)
		,PNBloodTests = cast(nullif(PNBloodTests, '') as nvarchar(2000))
		,PNBloodTestsCode = cast(PNBloodTestsCode as bigint)
		,RespirationRate = cast(nullif(RespirationRate, '') as nvarchar(2000))
		,RespirationRateCode = cast(RespirationRateCode as bigint)
		,MicturitionProblemsCode = cast(MicturitionProblemsCode as bigint)
		,WoundCode = cast(WoundCode as bigint)
		,MedicationCode = cast(MedicationCode as bigint)
		,ThromboembolicRiskPN = cast(nullif(ThromboembolicRiskPN, '') as nvarchar(2000))
		,TeamPN = cast(nullif(TeamPN, '') as nvarchar(2000))
		,ThromboembolicRiskPNCode = cast(ThromboembolicRiskPNCode as bigint)
		,TeamPNCode = cast(TeamPNCode as bigint)
		,TransferredWithBaby = cast(nullif(TransferredWithBaby, '') as nvarchar(2000))
		,TransferredWithBabyCode = cast(TransferredWithBabyCode as bigint)
		,DiscussionsDuringPNAdmission = cast(nullif(DiscussionsDuringPNAdmission, '') as nvarchar(2000))
		,DiscussionsDuringPNAdmissionCode = cast(DiscussionsDuringPNAdmissionCode as bigint)
		,FGMInformationImplications = cast(nullif(FGMInformationImplications, '') as nvarchar(2000))
		,FGMInformationImplicationsCode = cast(FGMInformationImplicationsCode as bigint)
		,FGMInformationIllegality = cast(nullif(FGMInformationIllegality, '') as nvarchar(2000))
	from
		(
		select
			UserID = PostnatalTransferMother.UserID
			,PregnancyID = PostnatalTransferMother.PregnancyID
			,PregnancyAdmissionID = PostnatalTransferMother.PregnancyadmissionID
			,TransfusionProblemsCode = PostnatalTransferMother.TransfusionProblems_Value
			,TransfusionProblems = PostnatalTransferMother.TransfusionProblems
			,ActionFollowingCounsellingCode = PostnatalTransferMother.ActionFollowingCounselling_Value
			,AgenciesCode = PostnatalTransferMother.Agencies_Value
			,AntiDCode = PostnatalTransferMother.AntiD_Value
			,AntiDGivenByCode = PostnatalTransferMother.AntiDGivenBy_Value
			,AppointmentsCode = PostnatalTransferMother.Appointments_Value
			,BloodTransfusionRQDCode = PostnatalTransferMother.BloodTransfusionRQD_Value
			,BowelProblemsCode = PostnatalTransferMother.BowelProblems_Value
			,BPCode = PostnatalTransferMother.BP_Value
			,BreastExamCode = PostnatalTransferMother.BreastExam_Value
			,ContactTelephoneCode = PostnatalTransferMother.ContactTelephone_Value
			,CounselledByCode = PostnatalTransferMother.CounselledBy_Value
			,CounsellingOrDebriefingCode = PostnatalTransferMother.CounsellingOrDebriefing_Value
			,DestinationCode = PostnatalTransferMother.Destination_Value
			,DischargedByCode = PostnatalTransferMother.DischargedBy_Value
			,EmotionalStateCode = PostnatalTransferMother.EmotionalState_Value
			,FeedingMethodCode = PostnatalTransferMother.FeedingMethod_Value
			,FinalDischargeCode = PostnatalTransferMother.FinalDischarge_Value
			,HbCode = PostnatalTransferMother.Hb_Value
			,InformationGivenCode = PostnatalTransferMother.InformationGiven_Value
			,LegExamCode = PostnatalTransferMother.LegExam_Value
			,LochiaCode = PostnatalTransferMother.Lochia_Value
			,ModeCode = PostnatalTransferMother.Mode_Value
			,OtherPNProblemsCode = PostnatalTransferMother.OtherPNProblems_Value
			,PerineumCode = PostnatalTransferMother.Perineum_Value
			,ProceduresDoneCode = PostnatalTransferMother.ProceduresDone_Value
			,PulseCode = PostnatalTransferMother.Pulse_Value
			,ReasonCode = PostnatalTransferMother.Reason_Value
			,ReferralsMadeCode = PostnatalTransferMother.ReferralsMade_Value
			,RoutinePNCareCode = PostnatalTransferMother.RoutinePNCare_Value
			,RubellaCode = PostnatalTransferMother.Rubella_Value
			,RubellaSerologyTestCode = PostnatalTransferMother.RubellaSerologyTest_Value
			,SmokerDischargeCode = PostnatalTransferMother.SmokerDischarge_Value
			,SupervisedByCode = PostnatalTransferMother.SupervisedBy_Value
			,TemperatureCode = PostnatalTransferMother.Temperature_Value
			,TransferredFromCode = PostnatalTransferMother.TransferredFrom_Value
			,UterusCode = PostnatalTransferMother.Uterus_Value
			,ActionFollowingCounselling = PostnatalTransferMother.ActionFollowingCounselling
			,AdditionalComments = PostnatalTransferMother.AdditionalComments
			,AdmissionDT = PostnatalTransferMother.AdmissionDT
			,Agencies = PostnatalTransferMother.Agencies
			,AntiD = PostnatalTransferMother.AntiD
			,AntiDBatchNo = PostnatalTransferMother.AntiDBatchNo
			,AntiDDate = PostnatalTransferMother.AntiDDate
			,AntiDGivenBy = PostnatalTransferMother.AntiDGivenBy
			,Appointments = PostnatalTransferMother.Appointments
			,BloodsTaken = PostnatalTransferMother.BloodsTaken
			,BloodTransfusionRQD = PostnatalTransferMother.BloodTransfusionRQD
			,BowelProblems = PostnatalTransferMother.BowelProblems
			,BP = PostnatalTransferMother.BP
			,BPProblems = PostnatalTransferMother.BPProblems
			,BreastExam = PostnatalTransferMother.BreastExam
			,BreastFeedSupport = PostnatalTransferMother.BreastFeedSupport
			,CMRingWard = PostnatalTransferMother.CMRingWard
			,ContactTelephone = PostnatalTransferMother.ContactTelephone
			,CounselledBy = PostnatalTransferMother.CounselledBy
			,CounsellingOrDebriefing = PostnatalTransferMother.CounsellingOrDebriefing
			,Debriefing = PostnatalTransferMother.Debriefing
			,Destination = PostnatalTransferMother.Destination
			,DischargedBy = PostnatalTransferMother.DischargedBy
			,DischargedDT = PostnatalTransferMother.DischargedDT
			,EmotionalState = PostnatalTransferMother.EmotionalState
			,FeedingMethod = PostnatalTransferMother.FeedingMethod
			,FinalDischarge = PostnatalTransferMother.FinalDischarge
			,Hb = PostnatalTransferMother.Hb
			,InformationGiven = PostnatalTransferMother.InformationGiven
			,IntravenousFluids = PostnatalTransferMother.IntravenousFluids
			,LegExam = PostnatalTransferMother.LegExam
			,Lochia = PostnatalTransferMother.Lochia
			,Medication = PostnatalTransferMother.Medication
			,MicturitionProblems = PostnatalTransferMother.MicturitionProblems
			,Mode = PostnatalTransferMother.Mode
			,ObsReviewReq = PostnatalTransferMother.ObsReviewReq
			,ObstetricDecision = PostnatalTransferMother.ObstetricDecision
			,OtherPNProblems = PostnatalTransferMother.OtherPNProblems
			,OtherTests = PostnatalTransferMother.OtherTests
			,ParentEducation = PostnatalTransferMother.ParentEducation
			,Perineum = PostnatalTransferMother.Perineum
			,PerineumManagement = PostnatalTransferMother.PerineumManagement
			,ProceduresDone = PostnatalTransferMother.ProceduresDone
			,Pulse = PostnatalTransferMother.Pulse
			,PulseRateProblems = PostnatalTransferMother.PulseRateProblems
			,Reason = PostnatalTransferMother.Reason
			,ReferralsMade = PostnatalTransferMother.ReferralsMade
			,RoutinePNCare = PostnatalTransferMother.RoutinePNCare
			,Rubella = PostnatalTransferMother.Rubella
			,RubellaSerologyTest = PostnatalTransferMother.RubellaSerologyTest
			,SmearRequired = PostnatalTransferMother.SmearRequired
			,SmokerDischarge = PostnatalTransferMother.SmokerDischarge
			,SupervisedBy = PostnatalTransferMother.SupervisedBy
			,SupportGroups = PostnatalTransferMother.SupportGroups
			,Temperature = PostnatalTransferMother.Temperature
			,TemperatureProblems = PostnatalTransferMother.TemperatureProblems
			,TransferredFrom = PostnatalTransferMother.TransferredFrom
			,Urinalysis = PostnatalTransferMother.Urinalysis
			,Uterus = PostnatalTransferMother.Uterus
			,Wound = PostnatalTransferMother.Wound
			,AdditionalCommentsCode = PostnatalTransferMother.AdditionalComments_Value
			,PerinealFollowUP = PostnatalTransferMother.PerinealFollowUP
			,PerinealFollowUPCode = PostnatalTransferMother.PerinealFollowUP_Value
			,UrinePostDelivery = PostnatalTransferMother.UrinePostDelivery
			,UrineDT = PostnatalTransferMother.UrineDT
			,UrineVolume = PostnatalTransferMother.UrineVolume
			,HbResult = PostnatalTransferMother.HbResult
			,HbResultCode = PostnatalTransferMother.HbResult_Value
			,AntiDDosePostNatal = PostnatalTransferMother.AntiDDosePostNatal
			,AntiDDosePostNatalCode = PostnatalTransferMother.AntiDDosePostNatal_Value
			,DischargeMethodCode = PostnatalTransferMother.DischargeMethod_Value
			,DischargeMethod = PostnatalTransferMother.DischargeMethod
			,PNInvestigations = PostnatalTransferMother.PNInvestigations
			,PNInvestigationsCode = PostnatalTransferMother.PNInvestigations_Value
			,PNBloodTests = PostnatalTransferMother.PNBloodTests
			,PNBloodTestsCode = PostnatalTransferMother.PNBloodTests_Value
			,RespirationRate = PostnatalTransferMother.RespirationRate
			,RespirationRateCode = PostnatalTransferMother.RespirationRate_Value
			,MicturitionProblemsCode = PostnatalTransferMother.MicturitionProblems_Value
			,WoundCode = PostnatalTransferMother.Wound_Value
			,MedicationCode = PostnatalTransferMother.Medication_Value
			,ThromboembolicRiskPN = PostnatalTransferMother.ThromboembolicRiskPN
			,TeamPN = PostnatalTransferMother.TeamPN
			,ThromboembolicRiskPNCode = PostnatalTransferMother.ThromboembolicRiskPN_Value
			,TeamPNCode = PostnatalTransferMother.TeamPN_Value
			,TransferredWithBaby = PostnatalTransferMother.TransferredWithBaby
			,TransferredWithBabyCode = PostnatalTransferMother.TransferredWithBaby_Value
			,DiscussionsDuringPNAdmission = PostnatalTransferMother.DiscussionsDuringPNAdmission
			,DiscussionsDuringPNAdmissionCode = PostnatalTransferMother.DiscussionsDuringPNAdmission_Value
			,FGMInformationImplications = PostnatalTransferMother.FGMInformationImplications
			,FGMInformationImplicationsCode = PostnatalTransferMother.FGMInformationImplications_Value
			,FGMInformationIllegality = PostnatalTransferMother.FGMInformationIllegality
		from
			E3.dbo.tblReport_Postnatal_Transfer_Mother_26 PostnatalTransferMother

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3PostnatalTransferMother on #TLoadE3PostnatalTransferMother
	(
	PregnancyAdmissionID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	E3.PostnatalTransferMother target
using
	(
	select
		*
	from
		#TLoadE3PostnatalTransferMother
	
	) source
	on	source.PregnancyAdmissionID = target.PregnancyAdmissionID

when not matched by source
then
	delete

when not matched
then
	insert
		(
		UserID
		,PregnancyID
		,PregnancyAdmissionID
		,TransfusionProblemsCode
		,TransfusionProblems
		,ActionFollowingCounsellingCode
		,AgenciesCode
		,AntiDCode
		,AntiDGivenByCode
		,AppointmentsCode
		,BloodTransfusionRQDCode
		,BowelProblemsCode
		,BPCode
		,BreastExamCode
		,ContactTelephoneCode
		,CounselledByCode
		,CounsellingOrDebriefingCode
		,DestinationCode
		,DischargedByCode
		,EmotionalStateCode
		,FeedingMethodCode
		,FinalDischargeCode
		,HbCode
		,InformationGivenCode
		,LegExamCode
		,LochiaCode
		,ModeCode
		,OtherPNProblemsCode
		,PerineumCode
		,ProceduresDoneCode
		,PulseCode
		,ReasonCode
		,ReferralsMadeCode
		,RoutinePNCareCode
		,RubellaCode
		,RubellaSerologyTestCode
		,SmokerDischargeCode
		,SupervisedByCode
		,TemperatureCode
		,TransferredFromCode
		,UterusCode
		,ActionFollowingCounselling
		,AdditionalComments
		,AdmissionDT
		,Agencies
		,AntiD
		,AntiDBatchNo
		,AntiDDate
		,AntiDGivenBy
		,Appointments
		,BloodsTaken
		,BloodTransfusionRQD
		,BowelProblems
		,BP
		,BPProblems
		,BreastExam
		,BreastFeedSupport
		,CMRingWard
		,ContactTelephone
		,CounselledBy
		,CounsellingOrDebriefing
		,Debriefing
		,Destination
		,DischargedBy
		,DischargedDT
		,EmotionalState
		,FeedingMethod
		,FinalDischarge
		,Hb
		,InformationGiven
		,IntravenousFluids
		,LegExam
		,Lochia
		,Medication
		,MicturitionProblems
		,Mode
		,ObsReviewReq
		,ObstetricDecision
		,OtherPNProblems
		,OtherTests
		,ParentEducation
		,Perineum
		,PerineumManagement
		,ProceduresDone
		,Pulse
		,PulseRateProblems
		,Reason
		,ReferralsMade
		,RoutinePNCare
		,Rubella
		,RubellaSerologyTest
		,SmearRequired
		,SmokerDischarge
		,SupervisedBy
		,SupportGroups
		,Temperature
		,TemperatureProblems
		,TransferredFrom
		,Urinalysis
		,Uterus
		,Wound
		,AdditionalCommentsCode
		,PerinealFollowUP
		,PerinealFollowUPCode
		,UrinePostDelivery
		,UrineDT
		,UrineVolume
		,HbResult
		,HbResultCode
		,AntiDDosePostNatal
		,AntiDDosePostNatalCode
		,DischargeMethodCode
		,DischargeMethod
		,PNInvestigations
		,PNInvestigationsCode
		,PNBloodTests
		,PNBloodTestsCode
		,RespirationRate
		,RespirationRateCode
		,MicturitionProblemsCode
		,WoundCode
		,MedicationCode
		,ThromboembolicRiskPN
		,TeamPN
		,ThromboembolicRiskPNCode
		,TeamPNCode
		,TransferredWithBaby
		,TransferredWithBabyCode
		,DiscussionsDuringPNAdmission
		,DiscussionsDuringPNAdmissionCode
		,FGMInformationImplications
		,FGMInformationImplicationsCode
		,FGMInformationIllegality

		,Created
		,ByWhom
		)
	values
		(
		source.UserID
		,source.PregnancyID
		,source.PregnancyAdmissionID
		,source.TransfusionProblemsCode
		,source.TransfusionProblems
		,source.ActionFollowingCounsellingCode
		,source.AgenciesCode
		,source.AntiDCode
		,source.AntiDGivenByCode
		,source.AppointmentsCode
		,source.BloodTransfusionRQDCode
		,source.BowelProblemsCode
		,source.BPCode
		,source.BreastExamCode
		,source.ContactTelephoneCode
		,source.CounselledByCode
		,source.CounsellingOrDebriefingCode
		,source.DestinationCode
		,source.DischargedByCode
		,source.EmotionalStateCode
		,source.FeedingMethodCode
		,source.FinalDischargeCode
		,source.HbCode
		,source.InformationGivenCode
		,source.LegExamCode
		,source.LochiaCode
		,source.ModeCode
		,source.OtherPNProblemsCode
		,source.PerineumCode
		,source.ProceduresDoneCode
		,source.PulseCode
		,source.ReasonCode
		,source.ReferralsMadeCode
		,source.RoutinePNCareCode
		,source.RubellaCode
		,source.RubellaSerologyTestCode
		,source.SmokerDischargeCode
		,source.SupervisedByCode
		,source.TemperatureCode
		,source.TransferredFromCode
		,source.UterusCode
		,source.ActionFollowingCounselling
		,source.AdditionalComments
		,source.AdmissionDT
		,source.Agencies
		,source.AntiD
		,source.AntiDBatchNo
		,source.AntiDDate
		,source.AntiDGivenBy
		,source.Appointments
		,source.BloodsTaken
		,source.BloodTransfusionRQD
		,source.BowelProblems
		,source.BP
		,source.BPProblems
		,source.BreastExam
		,source.BreastFeedSupport
		,source.CMRingWard
		,source.ContactTelephone
		,source.CounselledBy
		,source.CounsellingOrDebriefing
		,source.Debriefing
		,source.Destination
		,source.DischargedBy
		,source.DischargedDT
		,source.EmotionalState
		,source.FeedingMethod
		,source.FinalDischarge
		,source.Hb
		,source.InformationGiven
		,source.IntravenousFluids
		,source.LegExam
		,source.Lochia
		,source.Medication
		,source.MicturitionProblems
		,source.Mode
		,source.ObsReviewReq
		,source.ObstetricDecision
		,source.OtherPNProblems
		,source.OtherTests
		,source.ParentEducation
		,source.Perineum
		,source.PerineumManagement
		,source.ProceduresDone
		,source.Pulse
		,source.PulseRateProblems
		,source.Reason
		,source.ReferralsMade
		,source.RoutinePNCare
		,source.Rubella
		,source.RubellaSerologyTest
		,source.SmearRequired
		,source.SmokerDischarge
		,source.SupervisedBy
		,source.SupportGroups
		,source.Temperature
		,source.TemperatureProblems
		,source.TransferredFrom
		,source.Urinalysis
		,source.Uterus
		,source.Wound
		,source.AdditionalCommentsCode
		,source.PerinealFollowUP
		,source.PerinealFollowUPCode
		,source.UrinePostDelivery
		,source.UrineDT
		,source.UrineVolume
		,source.HbResult
		,source.HbResultCode
		,source.AntiDDosePostNatal
		,source.AntiDDosePostNatalCode
		,source.DischargeMethodCode
		,source.DischargeMethod
		,source.PNInvestigations
		,source.PNInvestigationsCode
		,source.PNBloodTests
		,source.PNBloodTestsCode
		,source.RespirationRate
		,source.RespirationRateCode
		,source.MicturitionProblemsCode
		,source.WoundCode
		,source.MedicationCode
		,source.ThromboembolicRiskPN
		,source.TeamPN
		,source.ThromboembolicRiskPNCode
		,source.TeamPNCode
		,source.TransferredWithBaby
		,source.TransferredWithBabyCode
		,source.DiscussionsDuringPNAdmission
		,source.DiscussionsDuringPNAdmissionCode
		,source.FGMInformationImplications
		,source.FGMInformationImplicationsCode
		,source.FGMInformationIllegality

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.UserID
		,target.PregnancyID
		,target.TransfusionProblemsCode
		,target.TransfusionProblems
		,target.ActionFollowingCounsellingCode
		,target.AgenciesCode
		,target.AntiDCode
		,target.AntiDGivenByCode
		,target.AppointmentsCode
		,target.BloodTransfusionRQDCode
		,target.BowelProblemsCode
		,target.BPCode
		,target.BreastExamCode
		,target.ContactTelephoneCode
		,target.CounselledByCode
		,target.CounsellingOrDebriefingCode
		,target.DestinationCode
		,target.DischargedByCode
		,target.EmotionalStateCode
		,target.FeedingMethodCode
		,target.FinalDischargeCode
		,target.HbCode
		,target.InformationGivenCode
		,target.LegExamCode
		,target.LochiaCode
		,target.ModeCode
		,target.OtherPNProblemsCode
		,target.PerineumCode
		,target.ProceduresDoneCode
		,target.PulseCode
		,target.ReasonCode
		,target.ReferralsMadeCode
		,target.RoutinePNCareCode
		,target.RubellaCode
		,target.RubellaSerologyTestCode
		,target.SmokerDischargeCode
		,target.SupervisedByCode
		,target.TemperatureCode
		,target.TransferredFromCode
		,target.UterusCode
		,target.ActionFollowingCounselling
		,target.AdditionalComments
		,target.AdmissionDT
		,target.Agencies
		,target.AntiD
		,target.AntiDBatchNo
		,target.AntiDDate
		,target.AntiDGivenBy
		,target.Appointments
		,target.BloodsTaken
		,target.BloodTransfusionRQD
		,target.BowelProblems
		,target.BP
		,target.BPProblems
		,target.BreastExam
		,target.BreastFeedSupport
		,target.CMRingWard
		,target.ContactTelephone
		,target.CounselledBy
		,target.CounsellingOrDebriefing
		,target.Debriefing
		,target.Destination
		,target.DischargedBy
		,target.DischargedDT
		,target.EmotionalState
		,target.FeedingMethod
		,target.FinalDischarge
		,target.Hb
		,target.InformationGiven
		,target.IntravenousFluids
		,target.LegExam
		,target.Lochia
		,target.Medication
		,target.MicturitionProblems
		,target.Mode
		,target.ObsReviewReq
		,target.ObstetricDecision
		,target.OtherPNProblems
		,target.OtherTests
		,target.ParentEducation
		,target.Perineum
		,target.PerineumManagement
		,target.ProceduresDone
		,target.Pulse
		,target.PulseRateProblems
		,target.Reason
		,target.ReferralsMade
		,target.RoutinePNCare
		,target.Rubella
		,target.RubellaSerologyTest
		,target.SmearRequired
		,target.SmokerDischarge
		,target.SupervisedBy
		,target.SupportGroups
		,target.Temperature
		,target.TemperatureProblems
		,target.TransferredFrom
		,target.Urinalysis
		,target.Uterus
		,target.Wound
		,target.AdditionalCommentsCode
		,target.PerinealFollowUP
		,target.PerinealFollowUPCode
		,target.UrinePostDelivery
		,target.UrineDT
		,target.UrineVolume
		,target.HbResult
		,target.HbResultCode
		,target.AntiDDosePostNatal
		,target.AntiDDosePostNatalCode
		,target.DischargeMethodCode
		,target.DischargeMethod
		,target.PNInvestigations
		,target.PNInvestigationsCode
		,target.PNBloodTests
		,target.PNBloodTestsCode
		,target.RespirationRate
		,target.RespirationRateCode
		,target.MicturitionProblemsCode
		,target.WoundCode
		,target.MedicationCode
		,target.ThromboembolicRiskPN
		,target.TeamPN
		,target.ThromboembolicRiskPNCode
		,target.TeamPNCode
		,target.TransferredWithBaby
		,target.TransferredWithBabyCode
		,target.DiscussionsDuringPNAdmission
		,target.DiscussionsDuringPNAdmissionCode
		,target.FGMInformationImplications
		,target.FGMInformationImplicationsCode
		,target.FGMInformationIllegality
		)

then
	update
	set
		target.UserID = source.UserID
		,target.PregnancyID = source.PregnancyID
		,target.TransfusionProblemsCode = source.TransfusionProblemsCode
		,target.TransfusionProblems = source.TransfusionProblems
		,target.ActionFollowingCounsellingCode = source.ActionFollowingCounsellingCode
		,target.AgenciesCode = source.AgenciesCode
		,target.AntiDCode = source.AntiDCode
		,target.AntiDGivenByCode = source.AntiDGivenByCode
		,target.AppointmentsCode = source.AppointmentsCode
		,target.BloodTransfusionRQDCode = source.BloodTransfusionRQDCode
		,target.BowelProblemsCode = source.BowelProblemsCode
		,target.BPCode = source.BPCode
		,target.BreastExamCode = source.BreastExamCode
		,target.ContactTelephoneCode = source.ContactTelephoneCode
		,target.CounselledByCode = source.CounselledByCode
		,target.CounsellingOrDebriefingCode = source.CounsellingOrDebriefingCode
		,target.DestinationCode = source.DestinationCode
		,target.DischargedByCode = source.DischargedByCode
		,target.EmotionalStateCode = source.EmotionalStateCode
		,target.FeedingMethodCode = source.FeedingMethodCode
		,target.FinalDischargeCode = source.FinalDischargeCode
		,target.HbCode = source.HbCode
		,target.InformationGivenCode = source.InformationGivenCode
		,target.LegExamCode = source.LegExamCode
		,target.LochiaCode = source.LochiaCode
		,target.ModeCode = source.ModeCode
		,target.OtherPNProblemsCode = source.OtherPNProblemsCode
		,target.PerineumCode = source.PerineumCode
		,target.ProceduresDoneCode = source.ProceduresDoneCode
		,target.PulseCode = source.PulseCode
		,target.ReasonCode = source.ReasonCode
		,target.ReferralsMadeCode = source.ReferralsMadeCode
		,target.RoutinePNCareCode = source.RoutinePNCareCode
		,target.RubellaCode = source.RubellaCode
		,target.RubellaSerologyTestCode = source.RubellaSerologyTestCode
		,target.SmokerDischargeCode = source.SmokerDischargeCode
		,target.SupervisedByCode = source.SupervisedByCode
		,target.TemperatureCode = source.TemperatureCode
		,target.TransferredFromCode = source.TransferredFromCode
		,target.UterusCode = source.UterusCode
		,target.ActionFollowingCounselling = source.ActionFollowingCounselling
		,target.AdditionalComments = source.AdditionalComments
		,target.AdmissionDT = source.AdmissionDT
		,target.Agencies = source.Agencies
		,target.AntiD = source.AntiD
		,target.AntiDBatchNo = source.AntiDBatchNo
		,target.AntiDDate = source.AntiDDate
		,target.AntiDGivenBy = source.AntiDGivenBy
		,target.Appointments = source.Appointments
		,target.BloodsTaken = source.BloodsTaken
		,target.BloodTransfusionRQD = source.BloodTransfusionRQD
		,target.BowelProblems = source.BowelProblems
		,target.BP = source.BP
		,target.BPProblems = source.BPProblems
		,target.BreastExam = source.BreastExam
		,target.BreastFeedSupport = source.BreastFeedSupport
		,target.CMRingWard = source.CMRingWard
		,target.ContactTelephone = source.ContactTelephone
		,target.CounselledBy = source.CounselledBy
		,target.CounsellingOrDebriefing = source.CounsellingOrDebriefing
		,target.Debriefing = source.Debriefing
		,target.Destination = source.Destination
		,target.DischargedBy = source.DischargedBy
		,target.DischargedDT = source.DischargedDT
		,target.EmotionalState = source.EmotionalState
		,target.FeedingMethod = source.FeedingMethod
		,target.FinalDischarge = source.FinalDischarge
		,target.Hb = source.Hb
		,target.InformationGiven = source.InformationGiven
		,target.IntravenousFluids = source.IntravenousFluids
		,target.LegExam = source.LegExam
		,target.Lochia = source.Lochia
		,target.Medication = source.Medication
		,target.MicturitionProblems = source.MicturitionProblems
		,target.Mode = source.Mode
		,target.ObsReviewReq = source.ObsReviewReq
		,target.ObstetricDecision = source.ObstetricDecision
		,target.OtherPNProblems = source.OtherPNProblems
		,target.OtherTests = source.OtherTests
		,target.ParentEducation = source.ParentEducation
		,target.Perineum = source.Perineum
		,target.PerineumManagement = source.PerineumManagement
		,target.ProceduresDone = source.ProceduresDone
		,target.Pulse = source.Pulse
		,target.PulseRateProblems = source.PulseRateProblems
		,target.Reason = source.Reason
		,target.ReferralsMade = source.ReferralsMade
		,target.RoutinePNCare = source.RoutinePNCare
		,target.Rubella = source.Rubella
		,target.RubellaSerologyTest = source.RubellaSerologyTest
		,target.SmearRequired = source.SmearRequired
		,target.SmokerDischarge = source.SmokerDischarge
		,target.SupervisedBy = source.SupervisedBy
		,target.SupportGroups = source.SupportGroups
		,target.Temperature = source.Temperature
		,target.TemperatureProblems = source.TemperatureProblems
		,target.TransferredFrom = source.TransferredFrom
		,target.Urinalysis = source.Urinalysis
		,target.Uterus = source.Uterus
		,target.Wound = source.Wound
		,target.AdditionalCommentsCode = source.AdditionalCommentsCode
		,target.PerinealFollowUP = source.PerinealFollowUP
		,target.PerinealFollowUPCode = source.PerinealFollowUPCode
		,target.UrinePostDelivery = source.UrinePostDelivery
		,target.UrineDT = source.UrineDT
		,target.UrineVolume = source.UrineVolume
		,target.HbResult = source.HbResult
		,target.HbResultCode = source.HbResultCode
		,target.AntiDDosePostNatal = source.AntiDDosePostNatal
		,target.AntiDDosePostNatalCode = source.AntiDDosePostNatalCode
		,target.DischargeMethodCode = source.DischargeMethodCode
		,target.DischargeMethod = source.DischargeMethod
		,target.PNInvestigations = source.PNInvestigations
		,target.PNInvestigationsCode = source.PNInvestigationsCode
		,target.PNBloodTests = source.PNBloodTests
		,target.PNBloodTestsCode = source.PNBloodTestsCode
		,target.RespirationRate = source.RespirationRate
		,target.RespirationRateCode = source.RespirationRateCode
		,target.MicturitionProblemsCode = source.MicturitionProblemsCode
		,target.WoundCode = source.WoundCode
		,target.MedicationCode = source.MedicationCode
		,target.ThromboembolicRiskPN = source.ThromboembolicRiskPN
		,target.TeamPN = source.TeamPN
		,target.ThromboembolicRiskPNCode = source.ThromboembolicRiskPNCode
		,target.TeamPNCode = source.TeamPNCode
		,target.TransferredWithBaby = source.TransferredWithBaby
		,target.TransferredWithBabyCode = source.TransferredWithBabyCode
		,target.DiscussionsDuringPNAdmission = source.DiscussionsDuringPNAdmission
		,target.DiscussionsDuringPNAdmissionCode = source.DiscussionsDuringPNAdmissionCode
		,target.FGMInformationImplications = source.FGMInformationImplications
		,target.FGMInformationImplicationsCode = source.FGMInformationImplicationsCode
		,target.FGMInformationIllegality = source.FGMInformationIllegality

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime