﻿CREATE procedure [E3].[LoadPracticeAddress] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			PCTID
			,Practice
			,Sequence
			,AreaID
			,Email
			,Fax
			,HomeTelephone
			,Line1
			,Line2
			,Line3
			,Line4
			,MobileTelephone
			,Postcode
			,WorkTelephone
			,EKRecord
			,GUID
			,UserID
			,LastUpdatedDateTime
			,GPPracticeID
			)
into
	#TLoadE3PracticeAddress
from
	(
	select
		PracticeAddressID = cast(PracticeAddressID as int)
		,PCTID = cast(PCTID as int)
		,Practice = cast(Practice as int)
		,Sequence = cast(nullif(Sequence, '') as nvarchar(50))
		,AreaID = cast(AreaID as tinyint)
		,Email = cast(nullif(Email, '') as nvarchar(60))
		,Fax = cast(nullif(Fax, '') as nvarchar(20))
		,HomeTelephone = cast(nullif(HomeTelephone, '') as nvarchar(20))
		,Line1 = cast(nullif(Line1, '') as nvarchar(40))
		,Line2 = cast(nullif(Line2, '') as nvarchar(40))
		,Line3 = cast(nullif(Line3, '') as nvarchar(40))
		,Line4 = cast(nullif(Line4, '') as nvarchar(40))
		,MobileTelephone = cast(nullif(MobileTelephone, '') as nvarchar(20))
		,Postcode = cast(nullif(Postcode, '') as nvarchar(10))
		,WorkTelephone = cast(nullif(WorkTelephone, '') as nvarchar(20))
		,EKRecord = cast(EKRecord as bit)
		,GUID = cast(GUID as uniqueidentifier)
		,UserID = cast(UserID as int)
		,LastUpdatedDateTime = cast(LastUpdatedDateTime as smalldatetime)
		,GPPracticeID = cast(GPPracticeID as int)
	from
		(
		select
			PracticeAddressID = PracticeAddress.PracticeAddressID
			,PCTID = PracticeAddress.PCTID
			,Practice = PracticeAddress.Practice
			,Sequence = PracticeAddress.Sequence
			,AreaID = PracticeAddress.AreaID
			,Email = PracticeAddress.Email
			,Fax = PracticeAddress.Fax
			,HomeTelephone = PracticeAddress.HomeTelephone
			,Line1 = PracticeAddress.Line1
			,Line2 = PracticeAddress.Line2
			,Line3 = PracticeAddress.Line3
			,Line4 = PracticeAddress.Line4
			,MobileTelephone = PracticeAddress.MobileTelephone
			,Postcode = PracticeAddress.Postcode
			,WorkTelephone = PracticeAddress.WorkTelephone
			,EKRecord = PracticeAddress.EK_Record
			,GUID = PracticeAddress.GUID
			,UserID = PracticeAddress.UserID
			,LastUpdatedDateTime = PracticeAddress.LastUpdated
			,GPPracticeID = PracticeAddress.GPPracticeID
		from
			E3.dbo.PracticeAddress PracticeAddress
		where
			PracticeAddress.Deleted <> 1

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3PracticeAddress on #TLoadE3PracticeAddress
	(
	PracticeAddressID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	E3.PracticeAddress target
using
	(
	select
		*
	from
		#TLoadE3PracticeAddress
	
	) source
	on	source.PracticeAddressID = target.PracticeAddressID

when not matched by source
then
	delete

when not matched
then
	insert
		(
		PracticeAddressID
		,PCTID
		,Practice
		,Sequence
		,AreaID
		,Email
		,Fax
		,HomeTelephone
		,Line1
		,Line2
		,Line3
		,Line4
		,MobileTelephone
		,Postcode
		,WorkTelephone
		,EKRecord
		,GUID
		,UserID
		,LastUpdatedDateTime
		,GPPracticeID

		,Created
		,ByWhom
		)
	values
		(
		source.PracticeAddressID
		,source.PCTID
		,source.Practice
		,source.Sequence
		,source.AreaID
		,source.Email
		,source.Fax
		,source.HomeTelephone
		,source.Line1
		,source.Line2
		,source.Line3
		,source.Line4
		,source.MobileTelephone
		,source.Postcode
		,source.WorkTelephone
		,source.EKRecord
		,source.GUID
		,source.UserID
		,source.LastUpdatedDateTime
		,source.GPPracticeID

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.PCTID
		,target.Practice
		,target.Sequence
		,target.AreaID
		,target.Email
		,target.Fax
		,target.HomeTelephone
		,target.Line1
		,target.Line2
		,target.Line3
		,target.Line4
		,target.MobileTelephone
		,target.Postcode
		,target.WorkTelephone
		,target.EKRecord
		,target.GUID
		,target.UserID
		,target.LastUpdatedDateTime
		,target.GPPracticeID
		)

then
	update
	set
		target.PCTID = source.PCTID
		,target.Practice = source.Practice
		,target.Sequence = source.Sequence
		,target.AreaID = source.AreaID
		,target.Email = source.Email
		,target.Fax = source.Fax
		,target.HomeTelephone = source.HomeTelephone
		,target.Line1 = source.Line1
		,target.Line2 = source.Line2
		,target.Line3 = source.Line3
		,target.Line4 = source.Line4
		,target.MobileTelephone = source.MobileTelephone
		,target.Postcode = source.Postcode
		,target.WorkTelephone = source.WorkTelephone
		,target.EKRecord = source.EKRecord
		,target.GUID = source.GUID
		,target.UserID = source.UserID
		,target.LastUpdatedDateTime = source.LastUpdatedDateTime
		,target.GPPracticeID = source.GPPracticeID

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime