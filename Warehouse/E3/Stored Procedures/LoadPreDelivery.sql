﻿CREATE procedure [E3].[LoadPreDelivery] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			UserID
			,PatientID
			,BloodGroupCode
			,BloodGroup
			,AlcoholInPregnancyCode
			,ANComplicationsCode
			,AntenatalCareCode
			,AntenatalCareMedCode
			,AntenatalSteroidsCode
			,BabiesNumberCode
			,BishopScoreCode
			,CategoryRiskSocialCode
			,CategoryRiskCode
			,CervicalSweepCode
			,ConsentBloodProductsCode
			,DeliveryLocationCode
			,ECVPerformedCode
			,EDDScanCode
			,GravidaCode
			,HepatitisBstatusCode
			,HospitalDeliveringCode
			,InductionLabourCode
			,InductionMethodCode
			,IntendsToDeliverCode
			,LabourOnsetCode
			,LabourStartedDTCode
			,MLUTypeCode
			,MRSAScreenCode
			,NumberAntenantalContactsCode
			,ParityCode
			,PrevCSCode
			,PreviousEctopicsCode
			,PreviousLivebirthsCode
			,PreviousMiscarriagesCode
			,PreviousMolarPregnanciesCode
			,PreviousStillbirthsCode
			,PreviousTOPCode
			,ReasondelPlaceChangeCode
			,ReasonForChangeANCode
			,ReasonIndOtherCode
			,ReasonIndPrimaryCode
			,ReasonIndSecondaryCode
			,RubellaStatusCode
			,SmokingAtDeliveryTimeCode
			,SubstanceUseInPregnancyCode
			,ThromboembolicRiskCode
			,ThromboprophylaxisPrescribedCode
			,VBACCode
			,AlcoholInPregnancy
			,ANComplications
			,AntenatalCare
			,AntenatalCareMed
			,AntenatalSteroids
			,BabiesNumber
			,BishopScore
			,CategoryRiskSocial
			,CategoryRisk
			,CervicalSweep
			,ConsentBloodProducts
			,DeliveryLocation
			,ECVPerformed
			,EDDScan
			,Gravida
			,HepatitisBstatus
			,HospitalDelivering
			,InductionLabour
			,InductionMethod
			,IntendsToDeliver
			,LabourOnset
			,LabourStartedDT
			,LMP
			,MLUType
			,MRSAScreen
			,NumberAntenantalContacts
			,NumberProstinDoses
			,OxytocinDT
			,Parity
			,PrevCS
			,PreviousEctopics
			,PreviousLivebirths
			,PreviousMiscarriages
			,PreviousMolarPregnancies
			,PreviousStillbirths
			,PreviousTOP
			,ReasondelPlaceChange
			,ReasonForChangeAN
			,ReasonIndOther
			,ReasonIndPrimary
			,ReasonIndSecondary
			,RubellaStatus
			,SmokingAtDeliveryTime
			,SubstanceUseInPregnancy
			,ThromboembolicRisk
			,ThromboprophylaxisPrescribed
			,VBAC
			,PreviousENND
			,PreviousLNND
			,PreviousENNDCode
			,PreviousLNNDCode
			,BookingDate
			,BookingDateCode
			,DeliveryCareAtOnset
			,DeliveryCareAtOnsetCode
			,Chronicity
			,ChorionicityCode
			,Chorionicity
			,IntraPartumCarePlan
			,InfectionInPregnancy
			,InfectionInPregnancyCode
			,CervicalSweepNumber
			,ANProcedures
			,ANProceduresCode
			,CategoryMedicalRiskLabourCode
			,CategoryMedicalRiskLabour
			,CategorySocialRiskLabourCode
			,CategorySocialRiskLabour
			,AntenatalVaccinations
			,AntenatalVaccinationsCode
			,ComplexSocialFactors
			,ComplexSocialFactorsCode
			,PreExistingMedicalConditions
			,PreExistingMedicalConditionsCode
			,PreviousNeonatalDeaths
			,PreviousNeonatalDeathsCode
			,ThromboembolicRiskLabour
			,ThromboembolicRiskLabourCode
			,MRSAResult
			,MRSAResultCode
			,AdmittedInLabourDT
			,AdmittedToDeliverySuiteDT
			,AdmittedToDeliverySuiteDTCode
			,FGMIdentifiedLabour
			,FGMIdentifiedLabourCode
			,MDSConsent
			,MDSConsentCode
			,FGM
			,FGMCode
			,FGMClassification
			,FGMClassificationCode
			,FGMClassificationType4
			,FGMClassificationType4Code
			,FGMIdentification
			,FGMIdentificationCode
			,FGMAgePerformed
			,FGMAgePerformedCode
			,FGMInformationImplications
			,FGMInformationImplicationsCode
			,FGMInformationIllegality
			,FGMInformationIllegalityCode
			,FGMFamilyHistory
			,FGMFamilyHistoryCode
			,FGMDaughtersUnder18
			)
into
	#TLoadE3PreDelivery
from
	(
	select
		UserID = cast(UserID as int)
		,PatientID = cast(PatientID as bigint)
		,PregnancyID = cast(PregnancyID as bigint)
		,BloodGroupCode = cast(BloodGroupCode as bigint)
		,BloodGroup = cast(nullif(BloodGroup, '') as nvarchar(2000))
		,AlcoholInPregnancyCode = cast(AlcoholInPregnancyCode as bigint)
		,ANComplicationsCode = cast(ANComplicationsCode as bigint)
		,AntenatalCareCode = cast(AntenatalCareCode as bigint)
		,AntenatalCareMedCode = cast(AntenatalCareMedCode as bigint)
		,AntenatalSteroidsCode = cast(AntenatalSteroidsCode as bigint)
		,BabiesNumberCode = cast(BabiesNumberCode as bigint)
		,BishopScoreCode = cast(BishopScoreCode as bigint)
		,CategoryRiskSocialCode = cast(CategoryRiskSocialCode as bigint)
		,CategoryRiskCode = cast(CategoryRiskCode as bigint)
		,CervicalSweepCode = cast(CervicalSweepCode as bigint)
		,ConsentBloodProductsCode = cast(ConsentBloodProductsCode as bigint)
		,DeliveryLocationCode = cast(DeliveryLocationCode as bigint)
		,ECVPerformedCode = cast(ECVPerformedCode as bigint)
		,EDDScanCode = cast(EDDScanCode as bigint)
		,GravidaCode = cast(GravidaCode as bigint)
		,HepatitisBstatusCode = cast(HepatitisBstatusCode as bigint)
		,HospitalDeliveringCode = cast(HospitalDeliveringCode as bigint)
		,InductionLabourCode = cast(InductionLabourCode as bigint)
		,InductionMethodCode = cast(InductionMethodCode as bigint)
		,IntendsToDeliverCode = cast(IntendsToDeliverCode as bigint)
		,LabourOnsetCode = cast(LabourOnsetCode as bigint)
		,LabourStartedDTCode = cast(LabourStartedDTCode as bigint)
		,MLUTypeCode = cast(MLUTypeCode as bigint)
		,MRSAScreenCode = cast(MRSAScreenCode as bigint)
		,NumberAntenantalContactsCode = cast(NumberAntenantalContactsCode as bigint)
		,ParityCode = cast(ParityCode as bigint)
		,PrevCSCode = cast(PrevCSCode as bigint)
		,PreviousEctopicsCode = cast(PreviousEctopicsCode as bigint)
		,PreviousLivebirthsCode = cast(PreviousLivebirthsCode as bigint)
		,PreviousMiscarriagesCode = cast(PreviousMiscarriagesCode as bigint)
		,PreviousMolarPregnanciesCode = cast(PreviousMolarPregnanciesCode as bigint)
		,PreviousStillbirthsCode = cast(PreviousStillbirthsCode as bigint)
		,PreviousTOPCode = cast(PreviousTOPCode as bigint)
		,ReasondelPlaceChangeCode = cast(ReasondelPlaceChangeCode as bigint)
		,ReasonForChangeANCode = cast(ReasonForChangeANCode as bigint)
		,ReasonIndOtherCode = cast(ReasonIndOtherCode as bigint)
		,ReasonIndPrimaryCode = cast(ReasonIndPrimaryCode as bigint)
		,ReasonIndSecondaryCode = cast(ReasonIndSecondaryCode as bigint)
		,RubellaStatusCode = cast(RubellaStatusCode as bigint)
		,SmokingAtDeliveryTimeCode = cast(SmokingAtDeliveryTimeCode as bigint)
		,SubstanceUseInPregnancyCode = cast(SubstanceUseInPregnancyCode as bigint)
		,ThromboembolicRiskCode = cast(ThromboembolicRiskCode as bigint)
		,ThromboprophylaxisPrescribedCode = cast(ThromboprophylaxisPrescribedCode as bigint)
		,VBACCode = cast(VBACCode as bigint)
		,AlcoholInPregnancy = cast(nullif(AlcoholInPregnancy, '') as nvarchar(2000))
		,ANComplications = cast(nullif(ANComplications, '') as nvarchar(2000))
		,AntenatalCare = cast(nullif(AntenatalCare, '') as nvarchar(2000))
		,AntenatalCareMed = cast(nullif(AntenatalCareMed, '') as nvarchar(2000))
		,AntenatalSteroids = cast(nullif(AntenatalSteroids, '') as nvarchar(2000))
		,BabiesNumber = cast(nullif(BabiesNumber, '') as nvarchar(2000))
		,BishopScore = cast(nullif(BishopScore, '') as nvarchar(2000))
		,CategoryRiskSocial = cast(nullif(CategoryRiskSocial, '') as nvarchar(2000))
		,CategoryRisk = cast(nullif(CategoryRisk, '') as nvarchar(2000))
		,CervicalSweep = cast(nullif(CervicalSweep, '') as nvarchar(2000))
		,ConsentBloodProducts = cast(nullif(ConsentBloodProducts, '') as nvarchar(2000))
		,DeliveryLocation = cast(nullif(DeliveryLocation, '') as nvarchar(2000))
		,ECVPerformed = cast(nullif(ECVPerformed, '') as nvarchar(2000))
		,EDDScan = cast(nullif(EDDScan, '') as nvarchar(2000))
		,Gravida = cast(nullif(Gravida, '') as nvarchar(2000))
		,HepatitisBstatus = cast(nullif(HepatitisBstatus, '') as nvarchar(2000))
		,HospitalDelivering = cast(nullif(HospitalDelivering, '') as nvarchar(2000))
		,InductionLabour = cast(nullif(InductionLabour, '') as nvarchar(2000))
		,InductionMethod = cast(nullif(InductionMethod, '') as nvarchar(2000))
		,IntendsToDeliver = cast(nullif(IntendsToDeliver, '') as nvarchar(2000))
		,LabourOnset = cast(nullif(LabourOnset, '') as nvarchar(2000))
		,LabourStartedDT = cast(nullif(LabourStartedDT, '') as nvarchar(2000))
		,LMP = cast(nullif(LMP, '') as nvarchar(2000))
		,MLUType = cast(nullif(MLUType, '') as nvarchar(2000))
		,MRSAScreen = cast(nullif(MRSAScreen, '') as nvarchar(2000))
		,NumberAntenantalContacts = cast(nullif(NumberAntenantalContacts, '') as nvarchar(2000))
		,NumberProstinDoses = cast(nullif(NumberProstinDoses, '') as nvarchar(2000))
		,OxytocinDT = cast(nullif(OxytocinDT, '') as nvarchar(2000))
		,Parity = cast(nullif(Parity, '') as nvarchar(2000))
		,PrevCS = cast(nullif(PrevCS, '') as nvarchar(2000))
		,PreviousEctopics = cast(nullif(PreviousEctopics, '') as nvarchar(2000))
		,PreviousLivebirths = cast(nullif(PreviousLivebirths, '') as nvarchar(2000))
		,PreviousMiscarriages = cast(nullif(PreviousMiscarriages, '') as nvarchar(2000))
		,PreviousMolarPregnancies = cast(nullif(PreviousMolarPregnancies, '') as nvarchar(2000))
		,PreviousStillbirths = cast(nullif(PreviousStillbirths, '') as nvarchar(2000))
		,PreviousTOP = cast(nullif(PreviousTOP, '') as nvarchar(2000))
		,ReasondelPlaceChange = cast(nullif(ReasondelPlaceChange, '') as nvarchar(2000))
		,ReasonForChangeAN = cast(nullif(ReasonForChangeAN, '') as nvarchar(2000))
		,ReasonIndOther = cast(nullif(ReasonIndOther, '') as nvarchar(2000))
		,ReasonIndPrimary = cast(nullif(ReasonIndPrimary, '') as nvarchar(2000))
		,ReasonIndSecondary = cast(nullif(ReasonIndSecondary, '') as nvarchar(2000))
		,RubellaStatus = cast(nullif(RubellaStatus, '') as nvarchar(2000))
		,SmokingAtDeliveryTime = cast(nullif(SmokingAtDeliveryTime, '') as nvarchar(2000))
		,SubstanceUseInPregnancy = cast(nullif(SubstanceUseInPregnancy, '') as nvarchar(2000))
		,ThromboembolicRisk = cast(nullif(ThromboembolicRisk, '') as nvarchar(2000))
		,ThromboprophylaxisPrescribed = cast(nullif(ThromboprophylaxisPrescribed, '') as nvarchar(2000))
		,VBAC = cast(nullif(VBAC, '') as nvarchar(2000))
		,PreviousENND = cast(nullif(PreviousENND, '') as nvarchar(2000))
		,PreviousLNND = cast(nullif(PreviousLNND, '') as nvarchar(2000))
		,PreviousENNDCode = cast(PreviousENNDCode as bigint)
		,PreviousLNNDCode = cast(PreviousLNNDCode as bigint)
		,BookingDate = cast(nullif(BookingDate, '') as nvarchar(2000))
		,BookingDateCode = cast(BookingDateCode as bigint)
		,DeliveryCareAtOnset = cast(nullif(DeliveryCareAtOnset, '') as nvarchar(2000))
		,DeliveryCareAtOnsetCode = cast(DeliveryCareAtOnsetCode as bigint)
		,Chronicity = cast(nullif(Chronicity, '') as nvarchar(2000))
		,ChorionicityCode = cast(ChorionicityCode as bigint)
		,Chorionicity = cast(nullif(Chorionicity, '') as nvarchar(2000))
		,IntraPartumCarePlan = cast(nullif(IntraPartumCarePlan, '') as nvarchar(2000))
		,InfectionInPregnancy = cast(nullif(InfectionInPregnancy, '') as nvarchar(2000))
		,InfectionInPregnancyCode = cast(InfectionInPregnancyCode as bigint)
		,CervicalSweepNumber = cast(nullif(CervicalSweepNumber, '') as nvarchar(2000))
		,ANProcedures = cast(nullif(ANProcedures, '') as nvarchar(2000))
		,ANProceduresCode = cast(ANProceduresCode as bigint)
		,CategoryMedicalRiskLabourCode = cast(CategoryMedicalRiskLabourCode as bigint)
		,CategoryMedicalRiskLabour = cast(nullif(CategoryMedicalRiskLabour, '') as nvarchar(2000))
		,CategorySocialRiskLabourCode = cast(CategorySocialRiskLabourCode as bigint)
		,CategorySocialRiskLabour = cast(nullif(CategorySocialRiskLabour, '') as nvarchar(2000))
		,AntenatalVaccinations = cast(nullif(AntenatalVaccinations, '') as nvarchar(2000))
		,AntenatalVaccinationsCode = cast(AntenatalVaccinationsCode as bigint)
		,ComplexSocialFactors = cast(nullif(ComplexSocialFactors, '') as nvarchar(2000))
		,ComplexSocialFactorsCode = cast(ComplexSocialFactorsCode as bigint)
		,PreExistingMedicalConditions = cast(nullif(PreExistingMedicalConditions, '') as nvarchar(2000))
		,PreExistingMedicalConditionsCode = cast(PreExistingMedicalConditionsCode as bigint)
		,PreviousNeonatalDeaths = cast(nullif(PreviousNeonatalDeaths, '') as nvarchar(2000))
		,PreviousNeonatalDeathsCode = cast(PreviousNeonatalDeathsCode as bigint)
		,ThromboembolicRiskLabour = cast(nullif(ThromboembolicRiskLabour, '') as nvarchar(2000))
		,ThromboembolicRiskLabourCode = cast(ThromboembolicRiskLabourCode as bigint)
		,MRSAResult = cast(nullif(MRSAResult, '') as nvarchar(2000))
		,MRSAResultCode = cast(MRSAResultCode as bigint)
		,AdmittedInLabourDT = cast(nullif(AdmittedInLabourDT, '') as nvarchar(2000))
		,AdmittedToDeliverySuiteDT = cast(nullif(AdmittedToDeliverySuiteDT, '') as nvarchar(2000))
		,AdmittedToDeliverySuiteDTCode = cast(AdmittedToDeliverySuiteDTCode as bigint)
		,FGMIdentifiedLabour = cast(nullif(FGMIdentifiedLabour, '') as nvarchar(2000))
		,FGMIdentifiedLabourCode = cast(FGMIdentifiedLabourCode as bigint)
		,MDSConsent = cast(nullif(MDSConsent, '') as nvarchar(2000))
		,MDSConsentCode = cast(MDSConsentCode as bigint)
		,FGM = cast(nullif(FGM, '') as nvarchar(2000))
		,FGMCode = cast(FGMCode as bigint)
		,FGMClassification = cast(nullif(FGMClassification, '') as nvarchar(2000))
		,FGMClassificationCode = cast(FGMClassificationCode as bigint)
		,FGMClassificationType4 = cast(nullif(FGMClassificationType4, '') as nvarchar(2000))
		,FGMClassificationType4Code = cast(FGMClassificationType4Code as bigint)
		,FGMIdentification = cast(nullif(FGMIdentification, '') as nvarchar(2000))
		,FGMIdentificationCode = cast(FGMIdentificationCode as bigint)
		,FGMAgePerformed = cast(nullif(FGMAgePerformed, '') as nvarchar(2000))
		,FGMAgePerformedCode = cast(FGMAgePerformedCode as bigint)
		,FGMInformationImplications = cast(nullif(FGMInformationImplications, '') as nvarchar(2000))
		,FGMInformationImplicationsCode = cast(FGMInformationImplicationsCode as bigint)
		,FGMInformationIllegality = cast(nullif(FGMInformationIllegality, '') as nvarchar(2000))
		,FGMInformationIllegalityCode = cast(FGMInformationIllegalityCode as bigint)
		,FGMFamilyHistory = cast(nullif(FGMFamilyHistory, '') as nvarchar(2000))
		,FGMFamilyHistoryCode = cast(FGMFamilyHistoryCode as bigint)
		,FGMDaughtersUnder18 = cast(nullif(FGMDaughtersUnder18, '') as nvarchar(2000))
	from
		(
	select
		UserID = PreDelivery.UserID
		,PatientID = PreDelivery.PatientID
		,PregnancyID = PreDelivery.PregnancyID
		,BloodGroupCode = PreDelivery.BloodGroup_Value
		,BloodGroup = PreDelivery.BloodGroup
		,AlcoholInPregnancyCode = PreDelivery.AlcoholInPregnancy_Value
		,ANComplicationsCode = PreDelivery.ANComplications_Value
		,AntenatalCareCode = PreDelivery.AntenatalCare_Value
		,AntenatalCareMedCode = PreDelivery.AntenatalCareMed_Value
		,AntenatalSteroidsCode = PreDelivery.AntenatalSteroids_Value
		,BabiesNumberCode = PreDelivery.BabiesNumber_Value
		,BishopScoreCode = PreDelivery.BishopScore_Value
		,CategoryRiskSocialCode = PreDelivery.CategoryRickSocial_Value
		,CategoryRiskCode = PreDelivery.CategoryRisk_Value
		,CervicalSweepCode = PreDelivery.CervicalSweep_Value
		,ConsentBloodProductsCode = PreDelivery.ConsentBloodProducts_Value
		,DeliveryLocationCode = PreDelivery.DeliveryLocation_Value
		,ECVPerformedCode = PreDelivery.ECVPerformed_Value
		,EDDScanCode = PreDelivery.EDDScan_Value
		,GravidaCode = PreDelivery.Gravida_Value
		,HepatitisBstatusCode = PreDelivery.HepatitisBstatus_Value
		,HospitalDeliveringCode = PreDelivery.HospitalDelivering_Value
		,InductionLabourCode = PreDelivery.InductionLabour_Value
		,InductionMethodCode = PreDelivery.InductionMethod_Value
		,IntendsToDeliverCode = PreDelivery.IntendsToDeliver_Value
		,LabourOnsetCode = PreDelivery.LabourOnset_Value
		,LabourStartedDTCode = PreDelivery.LabourStartedDT_Value
		,MLUTypeCode = PreDelivery.MLUType_Value
		,MRSAScreenCode = PreDelivery.MRSAScreen_Value
		,NumberAntenantalContactsCode = PreDelivery.NumberAntenantalContacts_Value
		,ParityCode = PreDelivery.Parity_Value
		,PrevCSCode = PreDelivery.PrevCS_Value
		,PreviousEctopicsCode = PreDelivery.PreviousEctopics_Value
		,PreviousLivebirthsCode = PreDelivery.PreviousLivebirths_Value
		,PreviousMiscarriagesCode = PreDelivery.PreviousMiscarriages_Value
		,PreviousMolarPregnanciesCode = PreDelivery.PreviousMolarPregnancies_Value
		,PreviousStillbirthsCode = PreDelivery.PreviousStillbirths_Value
		,PreviousTOPCode = PreDelivery.PreviousTOP_Value
		,ReasondelPlaceChangeCode = PreDelivery.ReasondelPlaceChange_Value
		,ReasonForChangeANCode = PreDelivery.ReasonForChangeAN_Value
		,ReasonIndOtherCode = PreDelivery.ReasonIndOther_Value
		,ReasonIndPrimaryCode = PreDelivery.ReasonIndPrimary_Value
		,ReasonIndSecondaryCode = PreDelivery.ReasonIndSecondary_Value
		,RubellaStatusCode = PreDelivery.RubellaStatus_Value
		,SmokingAtDeliveryTimeCode = PreDelivery.SmokingAtDeliveryTime_Value
		,SubstanceUseInPregnancyCode = PreDelivery.SubstanceUseInPregnancy_Value
		,ThromboembolicRiskCode = PreDelivery.ThromboembolicRisk_Value
		,ThromboprophylaxisPrescribedCode = PreDelivery.ThromboprophylaxisPrescribed_Value
		,VBACCode = PreDelivery.VBAC_Value
		,AlcoholInPregnancy = PreDelivery.AlcoholInPregnancy
		,ANComplications = PreDelivery.ANComplications
		,AntenatalCare = PreDelivery.AntenatalCare
		,AntenatalCareMed = PreDelivery.AntenatalCareMed
		,AntenatalSteroids = PreDelivery.AntenatalSteroids
		,BabiesNumber = PreDelivery.BabiesNumber
		,BishopScore = PreDelivery.BishopScore
		,CategoryRiskSocial = PreDelivery.CategoryRickSocial
		,CategoryRisk = PreDelivery.CategoryRisk
		,CervicalSweep = PreDelivery.CervicalSweep
		,ConsentBloodProducts = PreDelivery.ConsentBloodProducts
		,DeliveryLocation = PreDelivery.DeliveryLocation
		,ECVPerformed = PreDelivery.ECVPerformed
		,EDDScan = PreDelivery.EDDScan
		,Gravida = PreDelivery.Gravida
		,HepatitisBstatus = PreDelivery.HepatitisBstatus
		,HospitalDelivering = PreDelivery.HospitalDelivering
		,InductionLabour = PreDelivery.InductionLabour
		,InductionMethod = PreDelivery.InductionMethod
		,IntendsToDeliver = PreDelivery.IntendsToDeliver
		,LabourOnset = PreDelivery.LabourOnset
		,LabourStartedDT = PreDelivery.LabourStartedDT
		,LMP = PreDelivery.LMP
		,MLUType = PreDelivery.MLUType
		,MRSAScreen = PreDelivery.MRSAScreen
		,NumberAntenantalContacts = PreDelivery.NumberAntenantalContacts
		,NumberProstinDoses = PreDelivery.NumberProstinDoses
		,OxytocinDT = PreDelivery.OxytocinDT
		,Parity = PreDelivery.Parity
		,PrevCS = PreDelivery.PrevCS
		,PreviousEctopics = PreDelivery.PreviousEctopics
		,PreviousLivebirths = PreDelivery.PreviousLivebirths
		,PreviousMiscarriages = PreDelivery.PreviousMiscarriages
		,PreviousMolarPregnancies = PreDelivery.PreviousMolarPregnancies
		,PreviousStillbirths = PreDelivery.PreviousStillbirths
		,PreviousTOP = PreDelivery.PreviousTOP
		,ReasondelPlaceChange = PreDelivery.ReasondelPlaceChange
		,ReasonForChangeAN = PreDelivery.ReasonForChangeAN
		,ReasonIndOther = PreDelivery.ReasonIndOther
		,ReasonIndPrimary = PreDelivery.ReasonIndPrimary
		,ReasonIndSecondary = PreDelivery.ReasonIndSecondary
		,RubellaStatus = PreDelivery.RubellaStatus
		,SmokingAtDeliveryTime = PreDelivery.SmokingAtDeliveryTime
		,SubstanceUseInPregnancy = PreDelivery.SubstanceUseInPregnancy
		,ThromboembolicRisk = PreDelivery.ThromboembolicRisk
		,ThromboprophylaxisPrescribed = PreDelivery.ThromboprophylaxisPrescribed
		,VBAC = PreDelivery.VBAC
		,PreviousENND = PreDelivery.PreviousENND
		,PreviousLNND = PreDelivery.PreviousLNND
		,PreviousENNDCode = PreDelivery.PreviousENND_Value
		,PreviousLNNDCode = PreDelivery.PreviousLNND_Value
		,BookingDate = PreDelivery.BookingDate
		,BookingDateCode = PreDelivery.BookingDate_Value
		,DeliveryCareAtOnset = PreDelivery.DeliveryCareAtOnset
		,DeliveryCareAtOnsetCode = PreDelivery.DeliveryCareAtOnset_Value
		,Chronicity = PreDelivery.Chronicity
		,ChorionicityCode = PreDelivery.Chorionicity_Value
		,Chorionicity = PreDelivery.Chorionicity
		,IntraPartumCarePlan = PreDelivery.IntraPartumCarePlan
		,InfectionInPregnancy = PreDelivery.InfectionInPregnancy
		,InfectionInPregnancyCode = PreDelivery.InfectionInPregnancy_Value
		,CervicalSweepNumber = PreDelivery.CervicalSweepNumber
		,ANProcedures = PreDelivery.ANProcedures
		,ANProceduresCode = PreDelivery.ANProcedures_Value
		,CategoryMedicalRiskLabourCode = PreDelivery.CategoryMedicalRiskLabour_Value
		,CategoryMedicalRiskLabour = PreDelivery.CategoryMedicalRiskLabour
		,CategorySocialRiskLabourCode = PreDelivery.CategorySocialRiskLabour_Value
		,CategorySocialRiskLabour = PreDelivery.CategorySocialRiskLabour
		,AntenatalVaccinations = PreDelivery.AntenatalVaccinations
		,AntenatalVaccinationsCode = PreDelivery.AntenatalVaccinations_Value
		,ComplexSocialFactors = PreDelivery.ComplexSocialFactors
		,ComplexSocialFactorsCode = PreDelivery.ComplexSocialFactors_Value
		,PreExistingMedicalConditions = PreDelivery.PreExistingMedicalConditions
		,PreExistingMedicalConditionsCode = PreDelivery.PreExistingMedicalConditions_Value
		,PreviousNeonatalDeaths = PreDelivery.PreviousNeonatalDeaths
		,PreviousNeonatalDeathsCode = PreDelivery.PreviousNeonatalDeaths_Value
		,ThromboembolicRiskLabour = PreDelivery.ThromboembolicRiskLabour
		,ThromboembolicRiskLabourCode = PreDelivery.ThromboembolicRiskLabour_Value
		,MRSAResult = PreDelivery.MRSAResult
		,MRSAResultCode = PreDelivery.MRSAResult_Value
		,AdmittedInLabourDT = PreDelivery.AdmittedInLabourDT
		,AdmittedToDeliverySuiteDT = PreDelivery.AdmittedToDeliverySuiteDT
		,AdmittedToDeliverySuiteDTCode = PreDelivery.AdmittedToDeliverySuiteDT_Value
		,FGMIdentifiedLabour = PreDelivery.FGMIdentifiedLabour
		,FGMIdentifiedLabourCode = PreDelivery.FGMIdentifiedLabour_Value
		,MDSConsent = PreDelivery.MDSConsent
		,MDSConsentCode = PreDelivery.MDSConsent_Value
		,FGM = PreDelivery.FGM
		,FGMCode = PreDelivery.FGM_Value
		,FGMClassification = PreDelivery.FGMClassification
		,FGMClassificationCode = PreDelivery.FGMClassification_Value
		,FGMClassificationType4 = PreDelivery.FGMClassificationType4
		,FGMClassificationType4Code = PreDelivery.FGMClassificationType4_Value
		,FGMIdentification = PreDelivery.FGMIdentification
		,FGMIdentificationCode = PreDelivery.FGMIdentification_Value
		,FGMAgePerformed = PreDelivery.FGMAgePerformed
		,FGMAgePerformedCode = PreDelivery.FGMAgePerformed_Value
		,FGMInformationImplications = PreDelivery.FGMInformationImplications
		,FGMInformationImplicationsCode = PreDelivery.FGMInformationImplications_Value
		,FGMInformationIllegality = PreDelivery.FGMInformationIllegality
		,FGMInformationIllegalityCode = PreDelivery.FGMInformationIllegality_Value
		,FGMFamilyHistory = PreDelivery.FGMFamilyHistory
		,FGMFamilyHistoryCode = PreDelivery.FGMFamilyHistory_Value
		,FGMDaughtersUnder18 = PreDelivery.FGMDaughtersUnder18
	from
		E3.dbo.tblReport_Pre_Delivery_32 PreDelivery

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3PreDelivery on #TLoadE3PreDelivery
	(
	PregnancyID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	E3.PreDelivery target
using
	(
	select
		*
	from
		#TLoadE3PreDelivery
	
	) source
	on	source.PregnancyID = target.PregnancyID

when not matched by source
then
	delete

when not matched
then
	insert
		(
		UserID
		,PatientID
		,PregnancyID
		,BloodGroupCode
		,BloodGroup
		,AlcoholInPregnancyCode
		,ANComplicationsCode
		,AntenatalCareCode
		,AntenatalCareMedCode
		,AntenatalSteroidsCode
		,BabiesNumberCode
		,BishopScoreCode
		,CategoryRiskSocialCode
		,CategoryRiskCode
		,CervicalSweepCode
		,ConsentBloodProductsCode
		,DeliveryLocationCode
		,ECVPerformedCode
		,EDDScanCode
		,GravidaCode
		,HepatitisBstatusCode
		,HospitalDeliveringCode
		,InductionLabourCode
		,InductionMethodCode
		,IntendsToDeliverCode
		,LabourOnsetCode
		,LabourStartedDTCode
		,MLUTypeCode
		,MRSAScreenCode
		,NumberAntenantalContactsCode
		,ParityCode
		,PrevCSCode
		,PreviousEctopicsCode
		,PreviousLivebirthsCode
		,PreviousMiscarriagesCode
		,PreviousMolarPregnanciesCode
		,PreviousStillbirthsCode
		,PreviousTOPCode
		,ReasondelPlaceChangeCode
		,ReasonForChangeANCode
		,ReasonIndOtherCode
		,ReasonIndPrimaryCode
		,ReasonIndSecondaryCode
		,RubellaStatusCode
		,SmokingAtDeliveryTimeCode
		,SubstanceUseInPregnancyCode
		,ThromboembolicRiskCode
		,ThromboprophylaxisPrescribedCode
		,VBACCode
		,AlcoholInPregnancy
		,ANComplications
		,AntenatalCare
		,AntenatalCareMed
		,AntenatalSteroids
		,BabiesNumber
		,BishopScore
		,CategoryRiskSocial
		,CategoryRisk
		,CervicalSweep
		,ConsentBloodProducts
		,DeliveryLocation
		,ECVPerformed
		,EDDScan
		,Gravida
		,HepatitisBstatus
		,HospitalDelivering
		,InductionLabour
		,InductionMethod
		,IntendsToDeliver
		,LabourOnset
		,LabourStartedDT
		,LMP
		,MLUType
		,MRSAScreen
		,NumberAntenantalContacts
		,NumberProstinDoses
		,OxytocinDT
		,Parity
		,PrevCS
		,PreviousEctopics
		,PreviousLivebirths
		,PreviousMiscarriages
		,PreviousMolarPregnancies
		,PreviousStillbirths
		,PreviousTOP
		,ReasondelPlaceChange
		,ReasonForChangeAN
		,ReasonIndOther
		,ReasonIndPrimary
		,ReasonIndSecondary
		,RubellaStatus
		,SmokingAtDeliveryTime
		,SubstanceUseInPregnancy
		,ThromboembolicRisk
		,ThromboprophylaxisPrescribed
		,VBAC
		,PreviousENND
		,PreviousLNND
		,PreviousENNDCode
		,PreviousLNNDCode
		,BookingDate
		,BookingDateCode
		,DeliveryCareAtOnset
		,DeliveryCareAtOnsetCode
		,Chronicity
		,ChorionicityCode
		,Chorionicity
		,IntraPartumCarePlan
		,InfectionInPregnancy
		,InfectionInPregnancyCode
		,CervicalSweepNumber
		,ANProcedures
		,ANProceduresCode
		,CategoryMedicalRiskLabourCode
		,CategoryMedicalRiskLabour
		,CategorySocialRiskLabourCode
		,CategorySocialRiskLabour
		,AntenatalVaccinations
		,AntenatalVaccinationsCode
		,ComplexSocialFactors
		,ComplexSocialFactorsCode
		,PreExistingMedicalConditions
		,PreExistingMedicalConditionsCode
		,PreviousNeonatalDeaths
		,PreviousNeonatalDeathsCode
		,ThromboembolicRiskLabour
		,ThromboembolicRiskLabourCode
		,MRSAResult
		,MRSAResultCode
		,AdmittedInLabourDT
		,AdmittedToDeliverySuiteDT
		,AdmittedToDeliverySuiteDTCode
		,FGMIdentifiedLabour
		,FGMIdentifiedLabourCode
		,MDSConsent
		,MDSConsentCode
		,FGM
		,FGMCode
		,FGMClassification
		,FGMClassificationCode
		,FGMClassificationType4
		,FGMClassificationType4Code
		,FGMIdentification
		,FGMIdentificationCode
		,FGMAgePerformed
		,FGMAgePerformedCode
		,FGMInformationImplications
		,FGMInformationImplicationsCode
		,FGMInformationIllegality
		,FGMInformationIllegalityCode
		,FGMFamilyHistory
		,FGMFamilyHistoryCode
		,FGMDaughtersUnder18

		,Created
		,ByWhom
		)
	values
		(
		source.UserID
		,source.PatientID
		,source.PregnancyID
		,source.BloodGroupCode
		,source.BloodGroup
		,source.AlcoholInPregnancyCode
		,source.ANComplicationsCode
		,source.AntenatalCareCode
		,source.AntenatalCareMedCode
		,source.AntenatalSteroidsCode
		,source.BabiesNumberCode
		,source.BishopScoreCode
		,source.CategoryRiskSocialCode
		,source.CategoryRiskCode
		,source.CervicalSweepCode
		,source.ConsentBloodProductsCode
		,source.DeliveryLocationCode
		,source.ECVPerformedCode
		,source.EDDScanCode
		,source.GravidaCode
		,source.HepatitisBstatusCode
		,source.HospitalDeliveringCode
		,source.InductionLabourCode
		,source.InductionMethodCode
		,source.IntendsToDeliverCode
		,source.LabourOnsetCode
		,source.LabourStartedDTCode
		,source.MLUTypeCode
		,source.MRSAScreenCode
		,source.NumberAntenantalContactsCode
		,source.ParityCode
		,source.PrevCSCode
		,source.PreviousEctopicsCode
		,source.PreviousLivebirthsCode
		,source.PreviousMiscarriagesCode
		,source.PreviousMolarPregnanciesCode
		,source.PreviousStillbirthsCode
		,source.PreviousTOPCode
		,source.ReasondelPlaceChangeCode
		,source.ReasonForChangeANCode
		,source.ReasonIndOtherCode
		,source.ReasonIndPrimaryCode
		,source.ReasonIndSecondaryCode
		,source.RubellaStatusCode
		,source.SmokingAtDeliveryTimeCode
		,source.SubstanceUseInPregnancyCode
		,source.ThromboembolicRiskCode
		,source.ThromboprophylaxisPrescribedCode
		,source.VBACCode
		,source.AlcoholInPregnancy
		,source.ANComplications
		,source.AntenatalCare
		,source.AntenatalCareMed
		,source.AntenatalSteroids
		,source.BabiesNumber
		,source.BishopScore
		,source.CategoryRiskSocial
		,source.CategoryRisk
		,source.CervicalSweep
		,source.ConsentBloodProducts
		,source.DeliveryLocation
		,source.ECVPerformed
		,source.EDDScan
		,source.Gravida
		,source.HepatitisBstatus
		,source.HospitalDelivering
		,source.InductionLabour
		,source.InductionMethod
		,source.IntendsToDeliver
		,source.LabourOnset
		,source.LabourStartedDT
		,source.LMP
		,source.MLUType
		,source.MRSAScreen
		,source.NumberAntenantalContacts
		,source.NumberProstinDoses
		,source.OxytocinDT
		,source.Parity
		,source.PrevCS
		,source.PreviousEctopics
		,source.PreviousLivebirths
		,source.PreviousMiscarriages
		,source.PreviousMolarPregnancies
		,source.PreviousStillbirths
		,source.PreviousTOP
		,source.ReasondelPlaceChange
		,source.ReasonForChangeAN
		,source.ReasonIndOther
		,source.ReasonIndPrimary
		,source.ReasonIndSecondary
		,source.RubellaStatus
		,source.SmokingAtDeliveryTime
		,source.SubstanceUseInPregnancy
		,source.ThromboembolicRisk
		,source.ThromboprophylaxisPrescribed
		,source.VBAC
		,source.PreviousENND
		,source.PreviousLNND
		,source.PreviousENNDCode
		,source.PreviousLNNDCode
		,source.BookingDate
		,source.BookingDateCode
		,source.DeliveryCareAtOnset
		,source.DeliveryCareAtOnsetCode
		,source.Chronicity
		,source.ChorionicityCode
		,source.Chorionicity
		,source.IntraPartumCarePlan
		,source.InfectionInPregnancy
		,source.InfectionInPregnancyCode
		,source.CervicalSweepNumber
		,source.ANProcedures
		,source.ANProceduresCode
		,source.CategoryMedicalRiskLabourCode
		,source.CategoryMedicalRiskLabour
		,source.CategorySocialRiskLabourCode
		,source.CategorySocialRiskLabour
		,source.AntenatalVaccinations
		,source.AntenatalVaccinationsCode
		,source.ComplexSocialFactors
		,source.ComplexSocialFactorsCode
		,source.PreExistingMedicalConditions
		,source.PreExistingMedicalConditionsCode
		,source.PreviousNeonatalDeaths
		,source.PreviousNeonatalDeathsCode
		,source.ThromboembolicRiskLabour
		,source.ThromboembolicRiskLabourCode
		,source.MRSAResult
		,source.MRSAResultCode
		,source.AdmittedInLabourDT
		,source.AdmittedToDeliverySuiteDT
		,source.AdmittedToDeliverySuiteDTCode
		,source.FGMIdentifiedLabour
		,source.FGMIdentifiedLabourCode
		,source.MDSConsent
		,source.MDSConsentCode
		,source.FGM
		,source.FGMCode
		,source.FGMClassification
		,source.FGMClassificationCode
		,source.FGMClassificationType4
		,source.FGMClassificationType4Code
		,source.FGMIdentification
		,source.FGMIdentificationCode
		,source.FGMAgePerformed
		,source.FGMAgePerformedCode
		,source.FGMInformationImplications
		,source.FGMInformationImplicationsCode
		,source.FGMInformationIllegality
		,source.FGMInformationIllegalityCode
		,source.FGMFamilyHistory
		,source.FGMFamilyHistoryCode
		,source.FGMDaughtersUnder18

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.UserID
		,target.PatientID
		,target.BloodGroupCode
		,target.BloodGroup
		,target.AlcoholInPregnancyCode
		,target.ANComplicationsCode
		,target.AntenatalCareCode
		,target.AntenatalCareMedCode
		,target.AntenatalSteroidsCode
		,target.BabiesNumberCode
		,target.BishopScoreCode
		,target.CategoryRiskSocialCode
		,target.CategoryRiskCode
		,target.CervicalSweepCode
		,target.ConsentBloodProductsCode
		,target.DeliveryLocationCode
		,target.ECVPerformedCode
		,target.EDDScanCode
		,target.GravidaCode
		,target.HepatitisBstatusCode
		,target.HospitalDeliveringCode
		,target.InductionLabourCode
		,target.InductionMethodCode
		,target.IntendsToDeliverCode
		,target.LabourOnsetCode
		,target.LabourStartedDTCode
		,target.MLUTypeCode
		,target.MRSAScreenCode
		,target.NumberAntenantalContactsCode
		,target.ParityCode
		,target.PrevCSCode
		,target.PreviousEctopicsCode
		,target.PreviousLivebirthsCode
		,target.PreviousMiscarriagesCode
		,target.PreviousMolarPregnanciesCode
		,target.PreviousStillbirthsCode
		,target.PreviousTOPCode
		,target.ReasondelPlaceChangeCode
		,target.ReasonForChangeANCode
		,target.ReasonIndOtherCode
		,target.ReasonIndPrimaryCode
		,target.ReasonIndSecondaryCode
		,target.RubellaStatusCode
		,target.SmokingAtDeliveryTimeCode
		,target.SubstanceUseInPregnancyCode
		,target.ThromboembolicRiskCode
		,target.ThromboprophylaxisPrescribedCode
		,target.VBACCode
		,target.AlcoholInPregnancy
		,target.ANComplications
		,target.AntenatalCare
		,target.AntenatalCareMed
		,target.AntenatalSteroids
		,target.BabiesNumber
		,target.BishopScore
		,target.CategoryRiskSocial
		,target.CategoryRisk
		,target.CervicalSweep
		,target.ConsentBloodProducts
		,target.DeliveryLocation
		,target.ECVPerformed
		,target.EDDScan
		,target.Gravida
		,target.HepatitisBstatus
		,target.HospitalDelivering
		,target.InductionLabour
		,target.InductionMethod
		,target.IntendsToDeliver
		,target.LabourOnset
		,target.LabourStartedDT
		,target.LMP
		,target.MLUType
		,target.MRSAScreen
		,target.NumberAntenantalContacts
		,target.NumberProstinDoses
		,target.OxytocinDT
		,target.Parity
		,target.PrevCS
		,target.PreviousEctopics
		,target.PreviousLivebirths
		,target.PreviousMiscarriages
		,target.PreviousMolarPregnancies
		,target.PreviousStillbirths
		,target.PreviousTOP
		,target.ReasondelPlaceChange
		,target.ReasonForChangeAN
		,target.ReasonIndOther
		,target.ReasonIndPrimary
		,target.ReasonIndSecondary
		,target.RubellaStatus
		,target.SmokingAtDeliveryTime
		,target.SubstanceUseInPregnancy
		,target.ThromboembolicRisk
		,target.ThromboprophylaxisPrescribed
		,target.VBAC
		,target.PreviousENND
		,target.PreviousLNND
		,target.PreviousENNDCode
		,target.PreviousLNNDCode
		,target.BookingDate
		,target.BookingDateCode
		,target.DeliveryCareAtOnset
		,target.DeliveryCareAtOnsetCode
		,target.Chronicity
		,target.ChorionicityCode
		,target.Chorionicity
		,target.IntraPartumCarePlan
		,target.InfectionInPregnancy
		,target.InfectionInPregnancyCode
		,target.CervicalSweepNumber
		,target.ANProcedures
		,target.ANProceduresCode
		,target.CategoryMedicalRiskLabourCode
		,target.CategoryMedicalRiskLabour
		,target.CategorySocialRiskLabourCode
		,target.CategorySocialRiskLabour
		,target.AntenatalVaccinations
		,target.AntenatalVaccinationsCode
		,target.ComplexSocialFactors
		,target.ComplexSocialFactorsCode
		,target.PreExistingMedicalConditions
		,target.PreExistingMedicalConditionsCode
		,target.PreviousNeonatalDeaths
		,target.PreviousNeonatalDeathsCode
		,target.ThromboembolicRiskLabour
		,target.ThromboembolicRiskLabourCode
		,target.MRSAResult
		,target.MRSAResultCode
		,target.AdmittedInLabourDT
		,target.AdmittedToDeliverySuiteDT
		,target.AdmittedToDeliverySuiteDTCode
		,target.FGMIdentifiedLabour
		,target.FGMIdentifiedLabourCode
		,target.MDSConsent
		,target.MDSConsentCode
		,target.FGM
		,target.FGMCode
		,target.FGMClassification
		,target.FGMClassificationCode
		,target.FGMClassificationType4
		,target.FGMClassificationType4Code
		,target.FGMIdentification
		,target.FGMIdentificationCode
		,target.FGMAgePerformed
		,target.FGMAgePerformedCode
		,target.FGMInformationImplications
		,target.FGMInformationImplicationsCode
		,target.FGMInformationIllegality
		,target.FGMInformationIllegalityCode
		,target.FGMFamilyHistory
		,target.FGMFamilyHistoryCode
		,target.FGMDaughtersUnder18
		)

then
	update
	set
		target.UserID = source.UserID
		,target.PatientID = source.PatientID
		,target.BloodGroupCode = source.BloodGroupCode
		,target.BloodGroup = source.BloodGroup
		,target.AlcoholInPregnancyCode = source.AlcoholInPregnancyCode
		,target.ANComplicationsCode = source.ANComplicationsCode
		,target.AntenatalCareCode = source.AntenatalCareCode
		,target.AntenatalCareMedCode = source.AntenatalCareMedCode
		,target.AntenatalSteroidsCode = source.AntenatalSteroidsCode
		,target.BabiesNumberCode = source.BabiesNumberCode
		,target.BishopScoreCode = source.BishopScoreCode
		,target.CategoryRiskSocialCode = source.CategoryRiskSocialCode
		,target.CategoryRiskCode = source.CategoryRiskCode
		,target.CervicalSweepCode = source.CervicalSweepCode
		,target.ConsentBloodProductsCode = source.ConsentBloodProductsCode
		,target.DeliveryLocationCode = source.DeliveryLocationCode
		,target.ECVPerformedCode = source.ECVPerformedCode
		,target.EDDScanCode = source.EDDScanCode
		,target.GravidaCode = source.GravidaCode
		,target.HepatitisBstatusCode = source.HepatitisBstatusCode
		,target.HospitalDeliveringCode = source.HospitalDeliveringCode
		,target.InductionLabourCode = source.InductionLabourCode
		,target.InductionMethodCode = source.InductionMethodCode
		,target.IntendsToDeliverCode = source.IntendsToDeliverCode
		,target.LabourOnsetCode = source.LabourOnsetCode
		,target.LabourStartedDTCode = source.LabourStartedDTCode
		,target.MLUTypeCode = source.MLUTypeCode
		,target.MRSAScreenCode = source.MRSAScreenCode
		,target.NumberAntenantalContactsCode = source.NumberAntenantalContactsCode
		,target.ParityCode = source.ParityCode
		,target.PrevCSCode = source.PrevCSCode
		,target.PreviousEctopicsCode = source.PreviousEctopicsCode
		,target.PreviousLivebirthsCode = source.PreviousLivebirthsCode
		,target.PreviousMiscarriagesCode = source.PreviousMiscarriagesCode
		,target.PreviousMolarPregnanciesCode = source.PreviousMolarPregnanciesCode
		,target.PreviousStillbirthsCode = source.PreviousStillbirthsCode
		,target.PreviousTOPCode = source.PreviousTOPCode
		,target.ReasondelPlaceChangeCode = source.ReasondelPlaceChangeCode
		,target.ReasonForChangeANCode = source.ReasonForChangeANCode
		,target.ReasonIndOtherCode = source.ReasonIndOtherCode
		,target.ReasonIndPrimaryCode = source.ReasonIndPrimaryCode
		,target.ReasonIndSecondaryCode = source.ReasonIndSecondaryCode
		,target.RubellaStatusCode = source.RubellaStatusCode
		,target.SmokingAtDeliveryTimeCode = source.SmokingAtDeliveryTimeCode
		,target.SubstanceUseInPregnancyCode = source.SubstanceUseInPregnancyCode
		,target.ThromboembolicRiskCode = source.ThromboembolicRiskCode
		,target.ThromboprophylaxisPrescribedCode = source.ThromboprophylaxisPrescribedCode
		,target.VBACCode = source.VBACCode
		,target.AlcoholInPregnancy = source.AlcoholInPregnancy
		,target.ANComplications = source.ANComplications
		,target.AntenatalCare = source.AntenatalCare
		,target.AntenatalCareMed = source.AntenatalCareMed
		,target.AntenatalSteroids = source.AntenatalSteroids
		,target.BabiesNumber = source.BabiesNumber
		,target.BishopScore = source.BishopScore
		,target.CategoryRiskSocial = source.CategoryRiskSocial
		,target.CategoryRisk = source.CategoryRisk
		,target.CervicalSweep = source.CervicalSweep
		,target.ConsentBloodProducts = source.ConsentBloodProducts
		,target.DeliveryLocation = source.DeliveryLocation
		,target.ECVPerformed = source.ECVPerformed
		,target.EDDScan = source.EDDScan
		,target.Gravida = source.Gravida
		,target.HepatitisBstatus = source.HepatitisBstatus
		,target.HospitalDelivering = source.HospitalDelivering
		,target.InductionLabour = source.InductionLabour
		,target.InductionMethod = source.InductionMethod
		,target.IntendsToDeliver = source.IntendsToDeliver
		,target.LabourOnset = source.LabourOnset
		,target.LabourStartedDT = source.LabourStartedDT
		,target.LMP = source.LMP
		,target.MLUType = source.MLUType
		,target.MRSAScreen = source.MRSAScreen
		,target.NumberAntenantalContacts = source.NumberAntenantalContacts
		,target.NumberProstinDoses = source.NumberProstinDoses
		,target.OxytocinDT = source.OxytocinDT
		,target.Parity = source.Parity
		,target.PrevCS = source.PrevCS
		,target.PreviousEctopics = source.PreviousEctopics
		,target.PreviousLivebirths = source.PreviousLivebirths
		,target.PreviousMiscarriages = source.PreviousMiscarriages
		,target.PreviousMolarPregnancies = source.PreviousMolarPregnancies
		,target.PreviousStillbirths = source.PreviousStillbirths
		,target.PreviousTOP = source.PreviousTOP
		,target.ReasondelPlaceChange = source.ReasondelPlaceChange
		,target.ReasonForChangeAN = source.ReasonForChangeAN
		,target.ReasonIndOther = source.ReasonIndOther
		,target.ReasonIndPrimary = source.ReasonIndPrimary
		,target.ReasonIndSecondary = source.ReasonIndSecondary
		,target.RubellaStatus = source.RubellaStatus
		,target.SmokingAtDeliveryTime = source.SmokingAtDeliveryTime
		,target.SubstanceUseInPregnancy = source.SubstanceUseInPregnancy
		,target.ThromboembolicRisk = source.ThromboembolicRisk
		,target.ThromboprophylaxisPrescribed = source.ThromboprophylaxisPrescribed
		,target.VBAC = source.VBAC
		,target.PreviousENND = source.PreviousENND
		,target.PreviousLNND = source.PreviousLNND
		,target.PreviousENNDCode = source.PreviousENNDCode
		,target.PreviousLNNDCode = source.PreviousLNNDCode
		,target.BookingDate = source.BookingDate
		,target.BookingDateCode = source.BookingDateCode
		,target.DeliveryCareAtOnset = source.DeliveryCareAtOnset
		,target.DeliveryCareAtOnsetCode = source.DeliveryCareAtOnsetCode
		,target.Chronicity = source.Chronicity
		,target.ChorionicityCode = source.ChorionicityCode
		,target.Chorionicity = source.Chorionicity
		,target.IntraPartumCarePlan = source.IntraPartumCarePlan
		,target.InfectionInPregnancy = source.InfectionInPregnancy
		,target.InfectionInPregnancyCode = source.InfectionInPregnancyCode
		,target.CervicalSweepNumber = source.CervicalSweepNumber
		,target.ANProcedures = source.ANProcedures
		,target.ANProceduresCode = source.ANProceduresCode
		,target.CategoryMedicalRiskLabourCode = source.CategoryMedicalRiskLabourCode
		,target.CategoryMedicalRiskLabour = source.CategoryMedicalRiskLabour
		,target.CategorySocialRiskLabourCode = source.CategorySocialRiskLabourCode
		,target.CategorySocialRiskLabour = source.CategorySocialRiskLabour
		,target.AntenatalVaccinations = source.AntenatalVaccinations
		,target.AntenatalVaccinationsCode = source.AntenatalVaccinationsCode
		,target.ComplexSocialFactors = source.ComplexSocialFactors
		,target.ComplexSocialFactorsCode = source.ComplexSocialFactorsCode
		,target.PreExistingMedicalConditions = source.PreExistingMedicalConditions
		,target.PreExistingMedicalConditionsCode = source.PreExistingMedicalConditionsCode
		,target.PreviousNeonatalDeaths = source.PreviousNeonatalDeaths
		,target.PreviousNeonatalDeathsCode = source.PreviousNeonatalDeathsCode
		,target.ThromboembolicRiskLabour = source.ThromboembolicRiskLabour
		,target.ThromboembolicRiskLabourCode = source.ThromboembolicRiskLabourCode
		,target.MRSAResult = source.MRSAResult
		,target.MRSAResultCode = source.MRSAResultCode
		,target.AdmittedInLabourDT = source.AdmittedInLabourDT
		,target.AdmittedToDeliverySuiteDT = source.AdmittedToDeliverySuiteDT
		,target.AdmittedToDeliverySuiteDTCode = source.AdmittedToDeliverySuiteDTCode
		,target.FGMIdentifiedLabour = source.FGMIdentifiedLabour
		,target.FGMIdentifiedLabourCode = source.FGMIdentifiedLabourCode
		,target.MDSConsent = source.MDSConsent
		,target.MDSConsentCode = source.MDSConsentCode
		,target.FGM = source.FGM
		,target.FGMCode = source.FGMCode
		,target.FGMClassification = source.FGMClassification
		,target.FGMClassificationCode = source.FGMClassificationCode
		,target.FGMClassificationType4 = source.FGMClassificationType4
		,target.FGMClassificationType4Code = source.FGMClassificationType4Code
		,target.FGMIdentification = source.FGMIdentification
		,target.FGMIdentificationCode = source.FGMIdentificationCode
		,target.FGMAgePerformed = source.FGMAgePerformed
		,target.FGMAgePerformedCode = source.FGMAgePerformedCode
		,target.FGMInformationImplications = source.FGMInformationImplications
		,target.FGMInformationImplicationsCode = source.FGMInformationImplicationsCode
		,target.FGMInformationIllegality = source.FGMInformationIllegality
		,target.FGMInformationIllegalityCode = source.FGMInformationIllegalityCode
		,target.FGMFamilyHistory = source.FGMFamilyHistory
		,target.FGMFamilyHistoryCode = source.FGMFamilyHistoryCode
		,target.FGMDaughtersUnder18 = source.FGMDaughtersUnder18

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime