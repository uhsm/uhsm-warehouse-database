﻿CREATE procedure [E3].[LoadPregnancy] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			PatientID
			,IsClosed
			,EKRecord
			,GUID
			,UserID
			,LastUpdated
			,PregnancyNotesMemoID
			,ClosedDateTime
			,ClosedReason
			,ViewpointEpisodeID
			,DeliveryDateTime
			,LabourStartedDateTime
			,CervixDilatedDateTime
			,PushingCommencedDateTime
			,PlacentaDeliveredDateTime
			,RuptureOfMembranesDateTime
			,ROMToBirthInterval
			,FirstStage
			,SecondStage
			,ActiveSecondStage
			,ThirdStage
			,TotalDuration
			,DeliveryDate
			,PASInfoID
			,IsConfidential
			,KornerSent
			)
into
	#TLoadE3Pregnancy
from
	(
	select
		PregnancyID
		,PatientID = cast(PatientID as bigint)
		,IsClosed = cast(IsClosed as bit)
		,EKRecord = cast(EKRecord as bit)
		,GUID = cast(GUID as uniqueidentifier)
		,UserID = cast(UserID as int)
		,LastUpdated = cast(LastUpdated as smalldatetime)
		,PregnancyNotesMemoID = cast(PregnancyNotesMemoID as bigint)
		,ClosedDateTime = cast(ClosedDateTime as smalldatetime)
		,ClosedReason = cast(nullif(ClosedReason, '') as nvarchar(510))
		,ViewpointEpisodeID = cast(ViewpointEpisodeID as int)
		,DeliveryDateTime = cast(DeliveryDateTime as datetime)
		,LabourStartedDateTime = cast(LabourStartedDateTime as datetime)
		,CervixDilatedDateTime = cast(CervixDilatedDateTime as datetime)
		,PushingCommencedDateTime = cast(PushingCommencedDateTime as datetime)
		,PlacentaDeliveredDateTime = cast(PlacentaDeliveredDateTime as datetime)
		,RuptureOfMembranesDateTime = cast(RuptureOfMembranesDateTime as datetime)
		,ROMToBirthInterval = cast(nullif(ROMToBirthInterval, '') as nvarchar(40))
		,FirstStage = cast(nullif(FirstStage, '') as nvarchar(40))
		,SecondStage = cast(nullif(SecondStage, '') as nvarchar(40))
		,ActiveSecondStage = cast(nullif(ActiveSecondStage, '') as nvarchar(40))
		,ThirdStage = cast(nullif(ThirdStage, '') as nvarchar(40))
		,TotalDuration = cast(nullif(TotalDuration, '') as nvarchar(40))
		,DeliveryDate = cast(nullif(DeliveryDate, '') as nvarchar(40))
		,PASInfoID = cast(PASInfoID as int)
		,IsConfidential = cast(IsConfidential as bit)
		,KornerSent = cast(KornerSent as bit)
	from
		(
		select
			PregnancyID = Pregnancy.PregnancyID
			,PatientID = Pregnancy.PatientID
			,IsClosed = Pregnancy.Closed
			,EKRecord = Pregnancy.EK_Record
			,GUID = Pregnancy.GUID
			,UserID = Pregnancy.UserID
			,LastUpdated = Pregnancy.LastUpdated
			,PregnancyNotesMemoID = Pregnancy.PregnancyNotesMemoID
			,ClosedDateTime = Pregnancy.ClosedDate
			,ClosedReason = Pregnancy.ClosedReason
			,ViewpointEpisodeID = Pregnancy.ViewpointEpisodeID
			,DeliveryDateTime = Pregnancy.DeliveryDateTime
			,LabourStartedDateTime = Pregnancy.LabourStarted
			,CervixDilatedDateTime = Pregnancy.CervixDilated
			,PushingCommencedDateTime = Pregnancy.PushingCommenced
			,PlacentaDeliveredDateTime = Pregnancy.PlacentaDelivered
			,RuptureOfMembranesDateTime = Pregnancy.RuptureOfMembranes
			,ROMToBirthInterval = Pregnancy.ROMToBirthInterval
			,FirstStage = Pregnancy.FirstStage
			,SecondStage = Pregnancy.SecondStage
			,ActiveSecondStage = Pregnancy.ActiveSecondStage
			,ThirdStage = Pregnancy.ThirdStage
			,TotalDuration = Pregnancy.TotalDuration
			,DeliveryDate = Pregnancy.DeliveryDate
			,PASInfoID = Pregnancy.PASInfoID
			,IsConfidential = Pregnancy.Confidential
			,KornerSent = Pregnancy.KornerSent
		from
			E3.dbo.Pregnancy Pregnancy
		where
			Pregnancy.Deleted <> 1

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3Pregnancy on #TLoadE3Pregnancy
	(
	PregnancyID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	E3.Pregnancy target
using
	(
	select
		*
	from
		#TLoadE3Pregnancy
	
	) source
	on	source.PregnancyID = target.PregnancyID

when not matched by source
then
	delete

when not matched
then
	insert
		(
		PregnancyID
		,PatientID
		,IsClosed
		,EKRecord
		,GUID
		,UserID
		,LastUpdated
		,PregnancyNotesMemoID
		,ClosedDateTime
		,ClosedReason
		,ViewpointEpisodeID
		,DeliveryDateTime
		,LabourStartedDateTime
		,CervixDilatedDateTime
		,PushingCommencedDateTime
		,PlacentaDeliveredDateTime
		,RuptureOfMembranesDateTime
		,ROMToBirthInterval
		,FirstStage
		,SecondStage
		,ActiveSecondStage
		,ThirdStage
		,TotalDuration
		,DeliveryDate
		,PASInfoID
		,IsConfidential
		,KornerSent

		,Created
		,ByWhom
		)
	values
		(
		source.PregnancyID
		,source.PatientID
		,source.IsClosed
		,source.EKRecord
		,source.GUID
		,source.UserID
		,source.LastUpdated
		,source.PregnancyNotesMemoID
		,source.ClosedDateTime
		,source.ClosedReason
		,source.ViewpointEpisodeID
		,source.DeliveryDateTime
		,source.LabourStartedDateTime
		,source.CervixDilatedDateTime
		,source.PushingCommencedDateTime
		,source.PlacentaDeliveredDateTime
		,source.RuptureOfMembranesDateTime
		,source.ROMToBirthInterval
		,source.FirstStage
		,source.SecondStage
		,source.ActiveSecondStage
		,source.ThirdStage
		,source.TotalDuration
		,source.DeliveryDate
		,source.PASInfoID
		,source.IsConfidential
		,source.KornerSent

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.PatientID
		,target.IsClosed
		,target.EKRecord
		,target.GUID
		,target.UserID
		,target.LastUpdated
		,target.PregnancyNotesMemoID
		,target.ClosedDateTime
		,target.ClosedReason
		,target.ViewpointEpisodeID
		,target.DeliveryDateTime
		,target.LabourStartedDateTime
		,target.CervixDilatedDateTime
		,target.PushingCommencedDateTime
		,target.PlacentaDeliveredDateTime
		,target.RuptureOfMembranesDateTime
		,target.ROMToBirthInterval
		,target.FirstStage
		,target.SecondStage
		,target.ActiveSecondStage
		,target.ThirdStage
		,target.TotalDuration
		,target.DeliveryDate
		,target.PASInfoID
		,target.IsConfidential
		,target.KornerSent
		)

then
	update
	set
		target.PatientID = source.PatientID
		,target.IsClosed = source.IsClosed
		,target.EKRecord = source.EKRecord
		,target.GUID = source.GUID
		,target.UserID = source.UserID
		,target.LastUpdated = source.LastUpdated
		,target.PregnancyNotesMemoID = source.PregnancyNotesMemoID
		,target.ClosedDateTime = source.ClosedDateTime
		,target.ClosedReason = source.ClosedReason
		,target.ViewpointEpisodeID = source.ViewpointEpisodeID
		,target.DeliveryDateTime = source.DeliveryDateTime
		,target.LabourStartedDateTime = source.LabourStartedDateTime
		,target.CervixDilatedDateTime = source.CervixDilatedDateTime
		,target.PushingCommencedDateTime = source.PushingCommencedDateTime
		,target.PlacentaDeliveredDateTime = source.PlacentaDeliveredDateTime
		,target.RuptureOfMembranesDateTime = source.RuptureOfMembranesDateTime
		,target.ROMToBirthInterval = source.ROMToBirthInterval
		,target.FirstStage = source.FirstStage
		,target.SecondStage = source.SecondStage
		,target.ActiveSecondStage = source.ActiveSecondStage
		,target.ThirdStage = source.ThirdStage
		,target.TotalDuration = source.TotalDuration
		,target.DeliveryDate = source.DeliveryDate
		,target.PASInfoID = source.PASInfoID
		,target.IsConfidential = source.IsConfidential
		,target.KornerSent = source.KornerSent

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime