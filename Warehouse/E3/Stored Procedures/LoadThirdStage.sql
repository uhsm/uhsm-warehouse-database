﻿CREATE procedure [E3].[LoadThirdStage] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			UserID
			,PatientID
			,BloodLossAfterDeliveryCode
			,BloodsTakenCode
			,BloodTransfusionCode
			,DrugsPostDeliveryCode
			,EpiduralCannulaRemovedCode
			,EpisiotomyReasonCode
			,ExamAfterRepairCode
			,HistopathologyCode
			,LabourCareByCode
			,LochiaPostDeliveryCode
			,MembranesCompleteCode
			,MEOWSScoreCode
			,NeedlesAccountedForCode
			,NeedlesAccountedForByCode
			,OxytocicDrug3rdStageCode
			,PerineumVaginalTearsCode
			,PlacentaCompleteCode
			,PlacentaDeliveredHowCode
			,PostnatalProblemsCode
			,ProblemsPostDeliveryCode
			,SuturedWithin1HourCode
			,SutureMaterialCode
			,SuturingByCode
			,SuturingSupervisedByCode
			,SwabsEtcCheckedWithCode
			,TemperatureCode
			,TransferredCode
			,TransfusionProblemsCode
			,UrinePostDeliveryCode
			,UterusPostDeliveryCode
			,AnalgesiaPerineum
			,BloodLossAfterDelivery
			,BloodLossAtDelivery
			,BloodsTaken
			,BloodTransfusion
			,BP
			,DrugsPostDelivery
			,DurationOfLabour
			,EpiduralCannulaRemoved
			,EpisiotomyReason
			,EpisiotomyType
			,ExamAfterRepair
			,Histopathology
			,Investigations
			,IVTherapyPostDelivery
			,LabourCareBy
			,LochiaPostDelivery
			,MembranesComplete
			,MembranesIncomplete
			,MEOWSScore
			,NeedlesAccountedFor
			,NeedlesAccountedForBy
			,OxytocicDrug3rdStage
			,PerineumVaginalTears
			,PlacentaAppearance
			,PlacentaComplete
			,PlacentaDeliveredDT
			,PlacentaDeliveredHow
			,PlacentaIncTreatment
			,PostnatalProblems
			,ProblemsPostDelivery
			,ProductsOfConception
			,Pulse
			,ReasonForDelay
			,SuturedWithin1Hour
			,SutureMaterial
			,SuturingBy
			,SuturingSupervisedBy
			,SwabsEtcCheckedWith
			,Temperature
			,Transferred
			,TransfusionProblems
			,UrinePostDelivery
			,UterusPostDelivery
			,Analgesia3rdStage
			,Analgesia3rdStageCode
			,ReasonAnalgesia
			,ReasonAnalgesiaCode
			,PerinealRepair
			,PerinealRepairCode
			,InstrumentsCheckedPrior
			,InstrumentsCheckedPriorCode
			,PerinealRepairDT
			,UrineVolume
			,SwabsPerinealInspection
			,PerinealInspectionPrior
			,PerinealInspectionPost
			,PerinealInspectionPostCode
			,PerinealInspectionPriorCode
			,SwabsPerinealInspectionCode
			,Respirations
			,AdvicePerinealRepair
			,AdvicePerinealRepairCode
			,PlacentaAppearanceCode
			,UrineDT
			,RespirationsCode
			,ExistingAnaesthesiaOrAnalgesia
			,IVTherapyPostDeliveryCode
			,UrineVolumeCode
			,MOHPersonnelInformed
			,MOHCause
			,MOHManagement
			,MOHOperativeIntervention
			,MOHCauseCode
			,MOHManagementCode
			,MOHOperativeInterventionCode
			,MOHPersonnelInformedCode
			,MOHBloodProductsInfused
			,MOHBloodProductsInfusedCode
			,ConsentSuturing
			,ConsentSuturingCode
			,EpisiotomyTypeCode
			,OneToOneMidwiferyCare
			,OneToOneMidwiferyCareCode
			,UKOSSDisease
			,UKOSSDiseaseCode
			,MLUTransferredOut
			,MLUTransferReason
			,MLUTransferReasonCode
			,MLUTransferredOutCode
			,MLUTransferDT
			,EpisiotomyPerformedBy
			,EpisiotomyPerformedByCode
			,PPHDT
			,TransferToTheatreDT
			,AdmittedITUDT
			,DiagnosisEmbolismDT
			,HysterectomyOrLaparotomyDT
			)
into
	#TLoadE3ThirdStage
from
	(
	select
		UserID = cast(UserID as int)
		,PatientID = cast(PatientID as bigint)
		,PregnancyID = cast(PregnancyID as bigint)
		,BloodLossAfterDeliveryCode = cast(BloodLossAfterDeliveryCode as bigint)
		,BloodsTakenCode = cast(BloodsTakenCode as bigint)
		,BloodTransfusionCode = cast(BloodTransfusionCode as bigint)
		,DrugsPostDeliveryCode = cast(DrugsPostDeliveryCode as bigint)
		,EpiduralCannulaRemovedCode = cast(EpiduralCannulaRemovedCode as bigint)
		,EpisiotomyReasonCode = cast(EpisiotomyReasonCode as bigint)
		,ExamAfterRepairCode = cast(ExamAfterRepairCode as bigint)
		,HistopathologyCode = cast(HistopathologyCode as bigint)
		,LabourCareByCode = cast(LabourCareByCode as bigint)
		,LochiaPostDeliveryCode = cast(LochiaPostDeliveryCode as bigint)
		,MembranesCompleteCode = cast(MembranesCompleteCode as bigint)
		,MEOWSScoreCode = cast(MEOWSScoreCode as bigint)
		,NeedlesAccountedForCode = cast(NeedlesAccountedForCode as bigint)
		,NeedlesAccountedForByCode = cast(NeedlesAccountedForByCode as bigint)
		,OxytocicDrug3rdStageCode = cast(OxytocicDrug3rdStageCode as bigint)
		,PerineumVaginalTearsCode = cast(PerineumVaginalTearsCode as bigint)
		,PlacentaCompleteCode = cast(PlacentaCompleteCode as bigint)
		,PlacentaDeliveredHowCode = cast(PlacentaDeliveredHowCode as bigint)
		,PostnatalProblemsCode = cast(PostnatalProblemsCode as bigint)
		,ProblemsPostDeliveryCode = cast(ProblemsPostDeliveryCode as bigint)
		,SuturedWithin1HourCode = cast(SuturedWithin1HourCode as bigint)
		,SutureMaterialCode = cast(SutureMaterialCode as bigint)
		,SuturingByCode = cast(SuturingByCode as bigint)
		,SuturingSupervisedByCode = cast(SuturingSupervisedByCode as bigint)
		,SwabsEtcCheckedWithCode = cast(SwabsEtcCheckedWithCode as bigint)
		,TemperatureCode = cast(TemperatureCode as bigint)
		,TransferredCode = cast(TransferredCode as bigint)
		,TransfusionProblemsCode = cast(TransfusionProblemsCode as bigint)
		,UrinePostDeliveryCode = cast(UrinePostDeliveryCode as bigint)
		,UterusPostDeliveryCode = cast(UterusPostDeliveryCode as bigint)
		,AnalgesiaPerineum = cast(nullif(AnalgesiaPerineum, '') as nvarchar(2000))
		,BloodLossAfterDelivery = cast(nullif(BloodLossAfterDelivery, '') as nvarchar(2000))
		,BloodLossAtDelivery = cast(nullif(BloodLossAtDelivery, '') as nvarchar(2000))
		,BloodsTaken = cast(nullif(BloodsTaken, '') as nvarchar(2000))
		,BloodTransfusion = cast(nullif(BloodTransfusion, '') as nvarchar(2000))
		,BP = cast(nullif(BP, '') as nvarchar(2000))
		,DrugsPostDelivery = cast(nullif(DrugsPostDelivery, '') as nvarchar(2000))
		,DurationOfLabour = cast(nullif(DurationOfLabour, '') as nvarchar(2000))
		,EpiduralCannulaRemoved = cast(nullif(EpiduralCannulaRemoved, '') as nvarchar(2000))
		,EpisiotomyReason = cast(nullif(EpisiotomyReason, '') as nvarchar(2000))
		,EpisiotomyType = cast(nullif(EpisiotomyType, '') as nvarchar(2000))
		,ExamAfterRepair = cast(nullif(ExamAfterRepair, '') as nvarchar(2000))
		,Histopathology = cast(nullif(Histopathology, '') as nvarchar(2000))
		,Investigations = cast(nullif(Investigations, '') as nvarchar(2000))
		,IVTherapyPostDelivery = cast(nullif(IVTherapyPostDelivery, '') as nvarchar(2000))
		,LabourCareBy = cast(nullif(LabourCareBy, '') as nvarchar(2000))
		,LochiaPostDelivery = cast(nullif(LochiaPostDelivery, '') as nvarchar(2000))
		,MembranesComplete = cast(nullif(MembranesComplete, '') as nvarchar(2000))
		,MembranesIncomplete = cast(nullif(MembranesIncomplete, '') as nvarchar(2000))
		,MEOWSScore = cast(nullif(MEOWSScore, '') as nvarchar(2000))
		,NeedlesAccountedFor = cast(nullif(NeedlesAccountedFor, '') as nvarchar(2000))
		,NeedlesAccountedForBy = cast(nullif(NeedlesAccountedForBy, '') as nvarchar(2000))
		,OxytocicDrug3rdStage = cast(nullif(OxytocicDrug3rdStage, '') as nvarchar(2000))
		,PerineumVaginalTears = cast(nullif(PerineumVaginalTears, '') as nvarchar(2000))
		,PlacentaAppearance = cast(nullif(PlacentaAppearance, '') as nvarchar(2000))
		,PlacentaComplete = cast(nullif(PlacentaComplete, '') as nvarchar(2000))
		,PlacentaDeliveredDT = cast(nullif(PlacentaDeliveredDT, '') as nvarchar(2000))
		,PlacentaDeliveredHow = cast(nullif(PlacentaDeliveredHow, '') as nvarchar(2000))
		,PlacentaIncTreatment = cast(nullif(PlacentaIncTreatment, '') as nvarchar(2000))
		,PostnatalProblems = cast(nullif(PostnatalProblems, '') as nvarchar(2000))
		,ProblemsPostDelivery = cast(nullif(ProblemsPostDelivery, '') as nvarchar(2000))
		,ProductsOfConception = cast(nullif(ProductsOfConception, '') as nvarchar(2000))
		,Pulse = cast(nullif(Pulse, '') as nvarchar(2000))
		,ReasonForDelay = cast(nullif(ReasonForDelay, '') as nvarchar(2000))
		,SuturedWithin1Hour = cast(nullif(SuturedWithin1Hour, '') as nvarchar(2000))
		,SutureMaterial = cast(nullif(SutureMaterial, '') as nvarchar(2000))
		,SuturingBy = cast(nullif(SuturingBy, '') as nvarchar(2000))
		,SuturingSupervisedBy = cast(nullif(SuturingSupervisedBy, '') as nvarchar(2000))
		,SwabsEtcCheckedWith = cast(nullif(SwabsEtcCheckedWith, '') as nvarchar(2000))
		,Temperature = cast(nullif(Temperature, '') as nvarchar(2000))
		,Transferred = cast(nullif(Transferred, '') as nvarchar(2000))
		,TransfusionProblems = cast(nullif(TransfusionProblems, '') as nvarchar(2000))
		,UrinePostDelivery = cast(nullif(UrinePostDelivery, '') as nvarchar(2000))
		,UterusPostDelivery = cast(nullif(UterusPostDelivery, '') as nvarchar(2000))
		,Analgesia3rdStage = cast(nullif(Analgesia3rdStage, '') as nvarchar(2000))
		,Analgesia3rdStageCode = cast(Analgesia3rdStageCode as bigint)
		,ReasonAnalgesia = cast(nullif(ReasonAnalgesia, '') as nvarchar(2000))
		,ReasonAnalgesiaCode = cast(ReasonAnalgesiaCode as bigint)
		,PerinealRepair = cast(nullif(PerinealRepair, '') as nvarchar(2000))
		,PerinealRepairCode = cast(PerinealRepairCode as bigint)
		,InstrumentsCheckedPrior = cast(nullif(InstrumentsCheckedPrior, '') as nvarchar(2000))
		,InstrumentsCheckedPriorCode = cast(InstrumentsCheckedPriorCode as bigint)
		,PerinealRepairDT = cast(nullif(PerinealRepairDT, '') as nvarchar(2000))
		,UrineVolume = cast(nullif(UrineVolume, '') as nvarchar(2000))
		,SwabsPerinealInspection = cast(nullif(SwabsPerinealInspection, '') as nvarchar(2000))
		,PerinealInspectionPrior = cast(nullif(PerinealInspectionPrior, '') as nvarchar(2000))
		,PerinealInspectionPost = cast(nullif(PerinealInspectionPost, '') as nvarchar(2000))
		,PerinealInspectionPostCode = cast(PerinealInspectionPostCode as bigint)
		,PerinealInspectionPriorCode = cast(PerinealInspectionPriorCode as bigint)
		,SwabsPerinealInspectionCode = cast(SwabsPerinealInspectionCode as bigint)
		,Respirations = cast(nullif(Respirations, '') as nvarchar(2000))
		,AdvicePerinealRepair = cast(nullif(AdvicePerinealRepair, '') as nvarchar(2000))
		,AdvicePerinealRepairCode = cast(AdvicePerinealRepairCode as bigint)
		,PlacentaAppearanceCode = cast(PlacentaAppearanceCode as bigint)
		,UrineDT = cast(nullif(UrineDT, '') as nvarchar(2000))
		,RespirationsCode = cast(RespirationsCode as bigint)
		,ExistingAnaesthesiaOrAnalgesia = cast(nullif(ExistingAnaesthesiaOrAnalgesia, '') as nvarchar(2000))
		,IVTherapyPostDeliveryCode = cast(IVTherapyPostDeliveryCode as bigint)
		,UrineVolumeCode = cast(UrineVolumeCode as bigint)
		,MOHPersonnelInformed = cast(nullif(MOHPersonnelInformed, '') as nvarchar(2000))
		,MOHCause = cast(nullif(MOHCause, '') as nvarchar(2000))
		,MOHManagement = cast(nullif(MOHManagement, '') as nvarchar(2000))
		,MOHOperativeIntervention = cast(nullif(MOHOperativeIntervention, '') as nvarchar(2000))
		,MOHCauseCode = cast(MOHCauseCode as bigint)
		,MOHManagementCode = cast(MOHManagementCode as bigint)
		,MOHOperativeInterventionCode = cast(MOHOperativeInterventionCode as bigint)
		,MOHPersonnelInformedCode = cast(MOHPersonnelInformedCode as bigint)
		,MOHBloodProductsInfused = cast(nullif(MOHBloodProductsInfused, '') as nvarchar(2000))
		,MOHBloodProductsInfusedCode = cast(MOHBloodProductsInfusedCode as bigint)
		,ConsentSuturing = cast(nullif(ConsentSuturing, '') as nvarchar(2000))
		,ConsentSuturingCode = cast(ConsentSuturingCode as bigint)
		,EpisiotomyTypeCode = cast(EpisiotomyTypeCode as bigint)
		,OneToOneMidwiferyCare = cast(nullif(OneToOneMidwiferyCare, '') as nvarchar(2000))
		,OneToOneMidwiferyCareCode = cast(OneToOneMidwiferyCareCode as bigint)
		,UKOSSDisease = cast(nullif(UKOSSDisease, '') as nvarchar(2000))
		,UKOSSDiseaseCode = cast(UKOSSDiseaseCode as bigint)
		,MLUTransferredOut = cast(nullif(MLUTransferredOut, '') as nvarchar(2000))
		,MLUTransferReason = cast(nullif(MLUTransferReason, '') as nvarchar(2000))
		,MLUTransferReasonCode = cast(MLUTransferReasonCode as bigint)
		,MLUTransferredOutCode = cast(MLUTransferredOutCode as bigint)
		,MLUTransferDT = cast(nullif(MLUTransferDT, '') as nvarchar(2000))
		,EpisiotomyPerformedBy = cast(nullif(EpisiotomyPerformedBy, '') as nvarchar(2000))
		,EpisiotomyPerformedByCode = cast(EpisiotomyPerformedByCode as bigint)
		,PPHDT = cast(nullif(PPHDT, '') as nvarchar(2000))
		,TransferToTheatreDT = cast(nullif(TransferToTheatreDT, '') as nvarchar(2000))
		,AdmittedITUDT = cast(nullif(AdmittedITUDT, '') as nvarchar(2000))
		,DiagnosisEmbolismDT = cast(nullif(DiagnosisEmbolismDT, '') as nvarchar(2000))
		,HysterectomyOrLaparotomyDT = cast(nullif(HysterectomyOrLaparotomyDT, '') as nvarchar(2000))
	from
		(
		select
			UserID = ThirdStage.UserID
			,PatientID = ThirdStage.PatientID
			,PregnancyID = ThirdStage.PregnancyID
			,BloodLossAfterDeliveryCode = ThirdStage.BloodLossAfterDelivery_Value
			,BloodsTakenCode = ThirdStage.BloodsTaken_Value
			,BloodTransfusionCode = ThirdStage.BloodTransfusion_Value
			,DrugsPostDeliveryCode = ThirdStage.DrugsPostDelivery_Value
			,EpiduralCannulaRemovedCode = ThirdStage.EpiduralCannulaRemoved_Value
			,EpisiotomyReasonCode = ThirdStage.EpisiotomyReason_Value
			,ExamAfterRepairCode = ThirdStage.ExamAfterRepair_Value
			,HistopathologyCode = ThirdStage.Histopathology_Value
			,LabourCareByCode = ThirdStage.LabourCareBy_Value
			,LochiaPostDeliveryCode = ThirdStage.LochiaPostDelivery_Value
			,MembranesCompleteCode = ThirdStage.MembranesComplete_Value
			,MEOWSScoreCode = ThirdStage.MEOWSScore_Value
			,NeedlesAccountedForCode = ThirdStage.NeedlesAccountedFor_Value
			,NeedlesAccountedForByCode = ThirdStage.NeedlesAccountedForBy_Value
			,OxytocicDrug3rdStageCode = ThirdStage.OxytocicDrug3rdStage_Value
			,PerineumVaginalTearsCode = ThirdStage.PerineumVaginalTears_Value
			,PlacentaCompleteCode = ThirdStage.PlacentaComplete_Value
			,PlacentaDeliveredHowCode = ThirdStage.PlacentaDeliveredHow_Value
			,PostnatalProblemsCode = ThirdStage.PostnatalProblems_Value
			,ProblemsPostDeliveryCode = ThirdStage.ProblemsPostDelivery_Value
			,SuturedWithin1HourCode = ThirdStage.SuturedWithin1Hour_Value
			,SutureMaterialCode = ThirdStage.SutureMaterial_Value
			,SuturingByCode = ThirdStage.SuturingBy_Value
			,SuturingSupervisedByCode = ThirdStage.SuturingSupervisedBy_Value
			,SwabsEtcCheckedWithCode = ThirdStage.SwabsEtcCheckedWith_Value
			,TemperatureCode = ThirdStage.Temperature_Value
			,TransferredCode = ThirdStage.Transferred_Value
			,TransfusionProblemsCode = ThirdStage.TransfusionProblems_Value
			,UrinePostDeliveryCode = ThirdStage.UrinePostDelivery_Value
			,UterusPostDeliveryCode = ThirdStage.UterusPostDelivery_Value
			,AnalgesiaPerineum = ThirdStage.AnalgesiaPerineum
			,BloodLossAfterDelivery = ThirdStage.BloodLossAfterDelivery
			,BloodLossAtDelivery = ThirdStage.BloodLossAtDelivery
			,BloodsTaken = ThirdStage.BloodsTaken
			,BloodTransfusion = ThirdStage.BloodTransfusion
			,BP = ThirdStage.BP
			,DrugsPostDelivery = ThirdStage.DrugsPostDelivery
			,DurationOfLabour = ThirdStage.DurationOfLabour
			,EpiduralCannulaRemoved = ThirdStage.EpiduralCannulaRemoved
			,EpisiotomyReason = ThirdStage.EpisiotomyReason
			,EpisiotomyType = ThirdStage.EpisiotomyType
			,ExamAfterRepair = ThirdStage.ExamAfterRepair
			,Histopathology = ThirdStage.Histopathology
			,Investigations = ThirdStage.Investigations
			,IVTherapyPostDelivery = ThirdStage.IVTherapyPostDelivery
			,LabourCareBy = ThirdStage.LabourCareBy
			,LochiaPostDelivery = ThirdStage.LochiaPostDelivery
			,MembranesComplete = ThirdStage.MembranesComplete
			,MembranesIncomplete = ThirdStage.MembranesIncomplete
			,MEOWSScore = ThirdStage.MEOWSScore
			,NeedlesAccountedFor = ThirdStage.NeedlesAccountedFor
			,NeedlesAccountedForBy = ThirdStage.NeedlesAccountedForBy
			,OxytocicDrug3rdStage = ThirdStage.OxytocicDrug3rdStage
			,PerineumVaginalTears = ThirdStage.PerineumVaginalTears
			,PlacentaAppearance = ThirdStage.PlacentaAppearance
			,PlacentaComplete = ThirdStage.PlacentaComplete
			,PlacentaDeliveredDT = ThirdStage.PlacentaDeliveredDT
			,PlacentaDeliveredHow = ThirdStage.PlacentaDeliveredHow
			,PlacentaIncTreatment = ThirdStage.PlacentaIncTreatment
			,PostnatalProblems = ThirdStage.PostnatalProblems
			,ProblemsPostDelivery = ThirdStage.ProblemsPostDelivery
			,ProductsOfConception = ThirdStage.ProductsOfConception
			,Pulse = ThirdStage.Pulse
			,ReasonForDelay = ThirdStage.ReasonForDelay
			,SuturedWithin1Hour = ThirdStage.SuturedWithin1Hour
			,SutureMaterial = ThirdStage.SutureMaterial
			,SuturingBy = ThirdStage.SuturingBy
			,SuturingSupervisedBy = ThirdStage.SuturingSupervisedBy
			,SwabsEtcCheckedWith = ThirdStage.SwabsEtcCheckedWith
			,Temperature = ThirdStage.Temperature
			,Transferred = ThirdStage.Transferred
			,TransfusionProblems = ThirdStage.TransfusionProblems
			,UrinePostDelivery = ThirdStage.UrinePostDelivery
			,UterusPostDelivery = ThirdStage.UterusPostDelivery
			,Analgesia3rdStage = ThirdStage.Analgesia3rdStage
			,Analgesia3rdStageCode = ThirdStage.Analgesia3rdStage_Value
			,ReasonAnalgesia = ThirdStage.ReasonAnalgesia
			,ReasonAnalgesiaCode = ThirdStage.ReasonAnalgesia_Value
			,PerinealRepair = ThirdStage.PerinealRepair
			,PerinealRepairCode = ThirdStage.PerinealRepair_Value
			,InstrumentsCheckedPrior = ThirdStage.InstrumentsCheckedPrior
			,InstrumentsCheckedPriorCode = ThirdStage.InstrumentsCheckedPrior_Value
			,PerinealRepairDT = ThirdStage.PerinealRepairDT
			,UrineVolume = ThirdStage.UrineVolume
			,SwabsPerinealInspection = ThirdStage.SwabsPerinealInspection
			,PerinealInspectionPrior = ThirdStage.PerinealInspectionPrior
			,PerinealInspectionPost = ThirdStage.PerinealInspectionPost
			,PerinealInspectionPostCode = ThirdStage.PerinealInspectionPost_Value
			,PerinealInspectionPriorCode = ThirdStage.PerinealInspectionPrior_Value
			,SwabsPerinealInspectionCode = ThirdStage.SwabsPerinealInspection_Value
			,Respirations = ThirdStage.Respirations
			,AdvicePerinealRepair = ThirdStage.AdvicePerinealRepair
			,AdvicePerinealRepairCode = ThirdStage.AdvicePerinealRepair_Value
			,PlacentaAppearanceCode = ThirdStage.PlacentaAppearance_Value
			,UrineDT = ThirdStage.UrineDT
			,RespirationsCode = ThirdStage.Respirations_Value
			,ExistingAnaesthesiaOrAnalgesia = ThirdStage.ExistingAnaesthesiaOrAnalgesia
			,IVTherapyPostDeliveryCode = ThirdStage.IVTherapyPostDelivery_Value
			,UrineVolumeCode = ThirdStage.UrineVolume_Value
			,MOHPersonnelInformed = ThirdStage.MOHPersonnelInformed
			,MOHCause = ThirdStage.MOHCause
			,MOHManagement = ThirdStage.MOHManagement
			,MOHOperativeIntervention = ThirdStage.MOHOperativeIntervention
			,MOHCauseCode = ThirdStage.MOHCause_Value
			,MOHManagementCode = ThirdStage.MOHManagement_Value
			,MOHOperativeInterventionCode = ThirdStage.MOHOperativeIntervention_Value
			,MOHPersonnelInformedCode = ThirdStage.MOHPersonnelInformed_Value
			,MOHBloodProductsInfused = ThirdStage.MOHBloodProductsInfused
			,MOHBloodProductsInfusedCode = ThirdStage.MOHBloodProductsInfused_Value
			,ConsentSuturing = ThirdStage.ConsentSuturing
			,ConsentSuturingCode = ThirdStage.ConsentSuturing_Value
			,EpisiotomyTypeCode = ThirdStage.EpisiotomyType_Value
			,OneToOneMidwiferyCare = ThirdStage.OneToOneMidwiferyCare
			,OneToOneMidwiferyCareCode = ThirdStage.OneToOneMidwiferyCare_Value
			,UKOSSDisease = ThirdStage.UKOSSDisease
			,UKOSSDiseaseCode = ThirdStage.UKOSSDisease_Value
			,MLUTransferredOut = ThirdStage.MLUTransferredOut
			,MLUTransferReason = ThirdStage.MLUTransferReason
			,MLUTransferReasonCode = ThirdStage.MLUTransferReason_Value
			,MLUTransferredOutCode = ThirdStage.MLUTransferredOut_Value
			,MLUTransferDT = ThirdStage.MLUTransferDT
			,EpisiotomyPerformedBy = ThirdStage.EpisiotomyPerformedBy
			,EpisiotomyPerformedByCode = ThirdStage.EpisiotomyPerformedBy_Value
			,PPHDT = ThirdStage.PPHDT
			,TransferToTheatreDT = ThirdStage.TransferToTheatreDT
			,AdmittedITUDT = ThirdStage.AdmittedITUDT
			,DiagnosisEmbolismDT = ThirdStage.DiagnosisEmbolismDT
			,HysterectomyOrLaparotomyDT = ThirdStage.HysterectomyOrLaparotomyDT
		from
			E3.dbo.tblReport_Third_Stage_4 ThirdStage

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3ThirdStage on #TLoadE3ThirdStage
	(
	PregnancyID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	E3.ThirdStage target
using
	(
	select
		*
	from
		#TLoadE3ThirdStage
	
	) source
	on	source.PregnancyID = target.PregnancyID

when not matched by source
then
	delete

when not matched
then
	insert
		(
		UserID
		,PatientID
		,PregnancyID
		,BloodLossAfterDeliveryCode
		,BloodsTakenCode
		,BloodTransfusionCode
		,DrugsPostDeliveryCode
		,EpiduralCannulaRemovedCode
		,EpisiotomyReasonCode
		,ExamAfterRepairCode
		,HistopathologyCode
		,LabourCareByCode
		,LochiaPostDeliveryCode
		,MembranesCompleteCode
		,MEOWSScoreCode
		,NeedlesAccountedForCode
		,NeedlesAccountedForByCode
		,OxytocicDrug3rdStageCode
		,PerineumVaginalTearsCode
		,PlacentaCompleteCode
		,PlacentaDeliveredHowCode
		,PostnatalProblemsCode
		,ProblemsPostDeliveryCode
		,SuturedWithin1HourCode
		,SutureMaterialCode
		,SuturingByCode
		,SuturingSupervisedByCode
		,SwabsEtcCheckedWithCode
		,TemperatureCode
		,TransferredCode
		,TransfusionProblemsCode
		,UrinePostDeliveryCode
		,UterusPostDeliveryCode
		,AnalgesiaPerineum
		,BloodLossAfterDelivery
		,BloodLossAtDelivery
		,BloodsTaken
		,BloodTransfusion
		,BP
		,DrugsPostDelivery
		,DurationOfLabour
		,EpiduralCannulaRemoved
		,EpisiotomyReason
		,EpisiotomyType
		,ExamAfterRepair
		,Histopathology
		,Investigations
		,IVTherapyPostDelivery
		,LabourCareBy
		,LochiaPostDelivery
		,MembranesComplete
		,MembranesIncomplete
		,MEOWSScore
		,NeedlesAccountedFor
		,NeedlesAccountedForBy
		,OxytocicDrug3rdStage
		,PerineumVaginalTears
		,PlacentaAppearance
		,PlacentaComplete
		,PlacentaDeliveredDT
		,PlacentaDeliveredHow
		,PlacentaIncTreatment
		,PostnatalProblems
		,ProblemsPostDelivery
		,ProductsOfConception
		,Pulse
		,ReasonForDelay
		,SuturedWithin1Hour
		,SutureMaterial
		,SuturingBy
		,SuturingSupervisedBy
		,SwabsEtcCheckedWith
		,Temperature
		,Transferred
		,TransfusionProblems
		,UrinePostDelivery
		,UterusPostDelivery
		,Analgesia3rdStage
		,Analgesia3rdStageCode
		,ReasonAnalgesia
		,ReasonAnalgesiaCode
		,PerinealRepair
		,PerinealRepairCode
		,InstrumentsCheckedPrior
		,InstrumentsCheckedPriorCode
		,PerinealRepairDT
		,UrineVolume
		,SwabsPerinealInspection
		,PerinealInspectionPrior
		,PerinealInspectionPost
		,PerinealInspectionPostCode
		,PerinealInspectionPriorCode
		,SwabsPerinealInspectionCode
		,Respirations
		,AdvicePerinealRepair
		,AdvicePerinealRepairCode
		,PlacentaAppearanceCode
		,UrineDT
		,RespirationsCode
		,ExistingAnaesthesiaOrAnalgesia
		,IVTherapyPostDeliveryCode
		,UrineVolumeCode
		,MOHPersonnelInformed
		,MOHCause
		,MOHManagement
		,MOHOperativeIntervention
		,MOHCauseCode
		,MOHManagementCode
		,MOHOperativeInterventionCode
		,MOHPersonnelInformedCode
		,MOHBloodProductsInfused
		,MOHBloodProductsInfusedCode
		,ConsentSuturing
		,ConsentSuturingCode
		,EpisiotomyTypeCode
		,OneToOneMidwiferyCare
		,OneToOneMidwiferyCareCode
		,UKOSSDisease
		,UKOSSDiseaseCode
		,MLUTransferredOut
		,MLUTransferReason
		,MLUTransferReasonCode
		,MLUTransferredOutCode
		,MLUTransferDT
		,EpisiotomyPerformedBy
		,EpisiotomyPerformedByCode
		,PPHDT
		,TransferToTheatreDT
		,AdmittedITUDT
		,DiagnosisEmbolismDT
		,HysterectomyOrLaparotomyDT

		,Created
		,ByWhom
		)
	values
		(
		source.UserID
		,source.PatientID
		,source.PregnancyID
		,source.BloodLossAfterDeliveryCode
		,source.BloodsTakenCode
		,source.BloodTransfusionCode
		,source.DrugsPostDeliveryCode
		,source.EpiduralCannulaRemovedCode
		,source.EpisiotomyReasonCode
		,source.ExamAfterRepairCode
		,source.HistopathologyCode
		,source.LabourCareByCode
		,source.LochiaPostDeliveryCode
		,source.MembranesCompleteCode
		,source.MEOWSScoreCode
		,source.NeedlesAccountedForCode
		,source.NeedlesAccountedForByCode
		,source.OxytocicDrug3rdStageCode
		,source.PerineumVaginalTearsCode
		,source.PlacentaCompleteCode
		,source.PlacentaDeliveredHowCode
		,source.PostnatalProblemsCode
		,source.ProblemsPostDeliveryCode
		,source.SuturedWithin1HourCode
		,source.SutureMaterialCode
		,source.SuturingByCode
		,source.SuturingSupervisedByCode
		,source.SwabsEtcCheckedWithCode
		,source.TemperatureCode
		,source.TransferredCode
		,source.TransfusionProblemsCode
		,source.UrinePostDeliveryCode
		,source.UterusPostDeliveryCode
		,source.AnalgesiaPerineum
		,source.BloodLossAfterDelivery
		,source.BloodLossAtDelivery
		,source.BloodsTaken
		,source.BloodTransfusion
		,source.BP
		,source.DrugsPostDelivery
		,source.DurationOfLabour
		,source.EpiduralCannulaRemoved
		,source.EpisiotomyReason
		,source.EpisiotomyType
		,source.ExamAfterRepair
		,source.Histopathology
		,source.Investigations
		,source.IVTherapyPostDelivery
		,source.LabourCareBy
		,source.LochiaPostDelivery
		,source.MembranesComplete
		,source.MembranesIncomplete
		,source.MEOWSScore
		,source.NeedlesAccountedFor
		,source.NeedlesAccountedForBy
		,source.OxytocicDrug3rdStage
		,source.PerineumVaginalTears
		,source.PlacentaAppearance
		,source.PlacentaComplete
		,source.PlacentaDeliveredDT
		,source.PlacentaDeliveredHow
		,source.PlacentaIncTreatment
		,source.PostnatalProblems
		,source.ProblemsPostDelivery
		,source.ProductsOfConception
		,source.Pulse
		,source.ReasonForDelay
		,source.SuturedWithin1Hour
		,source.SutureMaterial
		,source.SuturingBy
		,source.SuturingSupervisedBy
		,source.SwabsEtcCheckedWith
		,source.Temperature
		,source.Transferred
		,source.TransfusionProblems
		,source.UrinePostDelivery
		,source.UterusPostDelivery
		,source.Analgesia3rdStage
		,source.Analgesia3rdStageCode
		,source.ReasonAnalgesia
		,source.ReasonAnalgesiaCode
		,source.PerinealRepair
		,source.PerinealRepairCode
		,source.InstrumentsCheckedPrior
		,source.InstrumentsCheckedPriorCode
		,source.PerinealRepairDT
		,source.UrineVolume
		,source.SwabsPerinealInspection
		,source.PerinealInspectionPrior
		,source.PerinealInspectionPost
		,source.PerinealInspectionPostCode
		,source.PerinealInspectionPriorCode
		,source.SwabsPerinealInspectionCode
		,source.Respirations
		,source.AdvicePerinealRepair
		,source.AdvicePerinealRepairCode
		,source.PlacentaAppearanceCode
		,source.UrineDT
		,source.RespirationsCode
		,source.ExistingAnaesthesiaOrAnalgesia
		,source.IVTherapyPostDeliveryCode
		,source.UrineVolumeCode
		,source.MOHPersonnelInformed
		,source.MOHCause
		,source.MOHManagement
		,source.MOHOperativeIntervention
		,source.MOHCauseCode
		,source.MOHManagementCode
		,source.MOHOperativeInterventionCode
		,source.MOHPersonnelInformedCode
		,source.MOHBloodProductsInfused
		,source.MOHBloodProductsInfusedCode
		,source.ConsentSuturing
		,source.ConsentSuturingCode
		,source.EpisiotomyTypeCode
		,source.OneToOneMidwiferyCare
		,source.OneToOneMidwiferyCareCode
		,source.UKOSSDisease
		,source.UKOSSDiseaseCode
		,source.MLUTransferredOut
		,source.MLUTransferReason
		,source.MLUTransferReasonCode
		,source.MLUTransferredOutCode
		,source.MLUTransferDT
		,source.EpisiotomyPerformedBy
		,source.EpisiotomyPerformedByCode
		,source.PPHDT
		,source.TransferToTheatreDT
		,source.AdmittedITUDT
		,source.DiagnosisEmbolismDT
		,source.HysterectomyOrLaparotomyDT

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.UserID
		,target.PatientID
		,target.BloodLossAfterDeliveryCode
		,target.BloodsTakenCode
		,target.BloodTransfusionCode
		,target.DrugsPostDeliveryCode
		,target.EpiduralCannulaRemovedCode
		,target.EpisiotomyReasonCode
		,target.ExamAfterRepairCode
		,target.HistopathologyCode
		,target.LabourCareByCode
		,target.LochiaPostDeliveryCode
		,target.MembranesCompleteCode
		,target.MEOWSScoreCode
		,target.NeedlesAccountedForCode
		,target.NeedlesAccountedForByCode
		,target.OxytocicDrug3rdStageCode
		,target.PerineumVaginalTearsCode
		,target.PlacentaCompleteCode
		,target.PlacentaDeliveredHowCode
		,target.PostnatalProblemsCode
		,target.ProblemsPostDeliveryCode
		,target.SuturedWithin1HourCode
		,target.SutureMaterialCode
		,target.SuturingByCode
		,target.SuturingSupervisedByCode
		,target.SwabsEtcCheckedWithCode
		,target.TemperatureCode
		,target.TransferredCode
		,target.TransfusionProblemsCode
		,target.UrinePostDeliveryCode
		,target.UterusPostDeliveryCode
		,target.AnalgesiaPerineum
		,target.BloodLossAfterDelivery
		,target.BloodLossAtDelivery
		,target.BloodsTaken
		,target.BloodTransfusion
		,target.BP
		,target.DrugsPostDelivery
		,target.DurationOfLabour
		,target.EpiduralCannulaRemoved
		,target.EpisiotomyReason
		,target.EpisiotomyType
		,target.ExamAfterRepair
		,target.Histopathology
		,target.Investigations
		,target.IVTherapyPostDelivery
		,target.LabourCareBy
		,target.LochiaPostDelivery
		,target.MembranesComplete
		,target.MembranesIncomplete
		,target.MEOWSScore
		,target.NeedlesAccountedFor
		,target.NeedlesAccountedForBy
		,target.OxytocicDrug3rdStage
		,target.PerineumVaginalTears
		,target.PlacentaAppearance
		,target.PlacentaComplete
		,target.PlacentaDeliveredDT
		,target.PlacentaDeliveredHow
		,target.PlacentaIncTreatment
		,target.PostnatalProblems
		,target.ProblemsPostDelivery
		,target.ProductsOfConception
		,target.Pulse
		,target.ReasonForDelay
		,target.SuturedWithin1Hour
		,target.SutureMaterial
		,target.SuturingBy
		,target.SuturingSupervisedBy
		,target.SwabsEtcCheckedWith
		,target.Temperature
		,target.Transferred
		,target.TransfusionProblems
		,target.UrinePostDelivery
		,target.UterusPostDelivery
		,target.Analgesia3rdStage
		,target.Analgesia3rdStageCode
		,target.ReasonAnalgesia
		,target.ReasonAnalgesiaCode
		,target.PerinealRepair
		,target.PerinealRepairCode
		,target.InstrumentsCheckedPrior
		,target.InstrumentsCheckedPriorCode
		,target.PerinealRepairDT
		,target.UrineVolume
		,target.SwabsPerinealInspection
		,target.PerinealInspectionPrior
		,target.PerinealInspectionPost
		,target.PerinealInspectionPostCode
		,target.PerinealInspectionPriorCode
		,target.SwabsPerinealInspectionCode
		,target.Respirations
		,target.AdvicePerinealRepair
		,target.AdvicePerinealRepairCode
		,target.PlacentaAppearanceCode
		,target.UrineDT
		,target.RespirationsCode
		,target.ExistingAnaesthesiaOrAnalgesia
		,target.IVTherapyPostDeliveryCode
		,target.UrineVolumeCode
		,target.MOHPersonnelInformed
		,target.MOHCause
		,target.MOHManagement
		,target.MOHOperativeIntervention
		,target.MOHCauseCode
		,target.MOHManagementCode
		,target.MOHOperativeInterventionCode
		,target.MOHPersonnelInformedCode
		,target.MOHBloodProductsInfused
		,target.MOHBloodProductsInfusedCode
		,target.ConsentSuturing
		,target.ConsentSuturingCode
		,target.EpisiotomyTypeCode
		,target.OneToOneMidwiferyCare
		,target.OneToOneMidwiferyCareCode
		,target.UKOSSDisease
		,target.UKOSSDiseaseCode
		,target.MLUTransferredOut
		,target.MLUTransferReason
		,target.MLUTransferReasonCode
		,target.MLUTransferredOutCode
		,target.MLUTransferDT
		,target.EpisiotomyPerformedBy
		,target.EpisiotomyPerformedByCode
		,target.PPHDT
		,target.TransferToTheatreDT
		,target.AdmittedITUDT
		,target.DiagnosisEmbolismDT
		,target.HysterectomyOrLaparotomyDT
		)

then
	update
	set
		target.UserID = source.UserID
		,target.PatientID = source.PatientID
		,target.BloodLossAfterDeliveryCode = source.BloodLossAfterDeliveryCode
		,target.BloodsTakenCode = source.BloodsTakenCode
		,target.BloodTransfusionCode = source.BloodTransfusionCode
		,target.DrugsPostDeliveryCode = source.DrugsPostDeliveryCode
		,target.EpiduralCannulaRemovedCode = source.EpiduralCannulaRemovedCode
		,target.EpisiotomyReasonCode = source.EpisiotomyReasonCode
		,target.ExamAfterRepairCode = source.ExamAfterRepairCode
		,target.HistopathologyCode = source.HistopathologyCode
		,target.LabourCareByCode = source.LabourCareByCode
		,target.LochiaPostDeliveryCode = source.LochiaPostDeliveryCode
		,target.MembranesCompleteCode = source.MembranesCompleteCode
		,target.MEOWSScoreCode = source.MEOWSScoreCode
		,target.NeedlesAccountedForCode = source.NeedlesAccountedForCode
		,target.NeedlesAccountedForByCode = source.NeedlesAccountedForByCode
		,target.OxytocicDrug3rdStageCode = source.OxytocicDrug3rdStageCode
		,target.PerineumVaginalTearsCode = source.PerineumVaginalTearsCode
		,target.PlacentaCompleteCode = source.PlacentaCompleteCode
		,target.PlacentaDeliveredHowCode = source.PlacentaDeliveredHowCode
		,target.PostnatalProblemsCode = source.PostnatalProblemsCode
		,target.ProblemsPostDeliveryCode = source.ProblemsPostDeliveryCode
		,target.SuturedWithin1HourCode = source.SuturedWithin1HourCode
		,target.SutureMaterialCode = source.SutureMaterialCode
		,target.SuturingByCode = source.SuturingByCode
		,target.SuturingSupervisedByCode = source.SuturingSupervisedByCode
		,target.SwabsEtcCheckedWithCode = source.SwabsEtcCheckedWithCode
		,target.TemperatureCode = source.TemperatureCode
		,target.TransferredCode = source.TransferredCode
		,target.TransfusionProblemsCode = source.TransfusionProblemsCode
		,target.UrinePostDeliveryCode = source.UrinePostDeliveryCode
		,target.UterusPostDeliveryCode = source.UterusPostDeliveryCode
		,target.AnalgesiaPerineum = source.AnalgesiaPerineum
		,target.BloodLossAfterDelivery = source.BloodLossAfterDelivery
		,target.BloodLossAtDelivery = source.BloodLossAtDelivery
		,target.BloodsTaken = source.BloodsTaken
		,target.BloodTransfusion = source.BloodTransfusion
		,target.BP = source.BP
		,target.DrugsPostDelivery = source.DrugsPostDelivery
		,target.DurationOfLabour = source.DurationOfLabour
		,target.EpiduralCannulaRemoved = source.EpiduralCannulaRemoved
		,target.EpisiotomyReason = source.EpisiotomyReason
		,target.EpisiotomyType = source.EpisiotomyType
		,target.ExamAfterRepair = source.ExamAfterRepair
		,target.Histopathology = source.Histopathology
		,target.Investigations = source.Investigations
		,target.IVTherapyPostDelivery = source.IVTherapyPostDelivery
		,target.LabourCareBy = source.LabourCareBy
		,target.LochiaPostDelivery = source.LochiaPostDelivery
		,target.MembranesComplete = source.MembranesComplete
		,target.MembranesIncomplete = source.MembranesIncomplete
		,target.MEOWSScore = source.MEOWSScore
		,target.NeedlesAccountedFor = source.NeedlesAccountedFor
		,target.NeedlesAccountedForBy = source.NeedlesAccountedForBy
		,target.OxytocicDrug3rdStage = source.OxytocicDrug3rdStage
		,target.PerineumVaginalTears = source.PerineumVaginalTears
		,target.PlacentaAppearance = source.PlacentaAppearance
		,target.PlacentaComplete = source.PlacentaComplete
		,target.PlacentaDeliveredDT = source.PlacentaDeliveredDT
		,target.PlacentaDeliveredHow = source.PlacentaDeliveredHow
		,target.PlacentaIncTreatment = source.PlacentaIncTreatment
		,target.PostnatalProblems = source.PostnatalProblems
		,target.ProblemsPostDelivery = source.ProblemsPostDelivery
		,target.ProductsOfConception = source.ProductsOfConception
		,target.Pulse = source.Pulse
		,target.ReasonForDelay = source.ReasonForDelay
		,target.SuturedWithin1Hour = source.SuturedWithin1Hour
		,target.SutureMaterial = source.SutureMaterial
		,target.SuturingBy = source.SuturingBy
		,target.SuturingSupervisedBy = source.SuturingSupervisedBy
		,target.SwabsEtcCheckedWith = source.SwabsEtcCheckedWith
		,target.Temperature = source.Temperature
		,target.Transferred = source.Transferred
		,target.TransfusionProblems = source.TransfusionProblems
		,target.UrinePostDelivery = source.UrinePostDelivery
		,target.UterusPostDelivery = source.UterusPostDelivery
		,target.Analgesia3rdStage = source.Analgesia3rdStage
		,target.Analgesia3rdStageCode = source.Analgesia3rdStageCode
		,target.ReasonAnalgesia = source.ReasonAnalgesia
		,target.ReasonAnalgesiaCode = source.ReasonAnalgesiaCode
		,target.PerinealRepair = source.PerinealRepair
		,target.PerinealRepairCode = source.PerinealRepairCode
		,target.InstrumentsCheckedPrior = source.InstrumentsCheckedPrior
		,target.InstrumentsCheckedPriorCode = source.InstrumentsCheckedPriorCode
		,target.PerinealRepairDT = source.PerinealRepairDT
		,target.UrineVolume = source.UrineVolume
		,target.SwabsPerinealInspection = source.SwabsPerinealInspection
		,target.PerinealInspectionPrior = source.PerinealInspectionPrior
		,target.PerinealInspectionPost = source.PerinealInspectionPost
		,target.PerinealInspectionPostCode = source.PerinealInspectionPostCode
		,target.PerinealInspectionPriorCode = source.PerinealInspectionPriorCode
		,target.SwabsPerinealInspectionCode = source.SwabsPerinealInspectionCode
		,target.Respirations = source.Respirations
		,target.AdvicePerinealRepair = source.AdvicePerinealRepair
		,target.AdvicePerinealRepairCode = source.AdvicePerinealRepairCode
		,target.PlacentaAppearanceCode = source.PlacentaAppearanceCode
		,target.UrineDT = source.UrineDT
		,target.RespirationsCode = source.RespirationsCode
		,target.ExistingAnaesthesiaOrAnalgesia = source.ExistingAnaesthesiaOrAnalgesia
		,target.IVTherapyPostDeliveryCode = source.IVTherapyPostDeliveryCode
		,target.UrineVolumeCode = source.UrineVolumeCode
		,target.MOHPersonnelInformed = source.MOHPersonnelInformed
		,target.MOHCause = source.MOHCause
		,target.MOHManagement = source.MOHManagement
		,target.MOHOperativeIntervention = source.MOHOperativeIntervention
		,target.MOHCauseCode = source.MOHCauseCode
		,target.MOHManagementCode = source.MOHManagementCode
		,target.MOHOperativeInterventionCode = source.MOHOperativeInterventionCode
		,target.MOHPersonnelInformedCode = source.MOHPersonnelInformedCode
		,target.MOHBloodProductsInfused = source.MOHBloodProductsInfused
		,target.MOHBloodProductsInfusedCode = source.MOHBloodProductsInfusedCode
		,target.ConsentSuturing = source.ConsentSuturing
		,target.ConsentSuturingCode = source.ConsentSuturingCode
		,target.EpisiotomyTypeCode = source.EpisiotomyTypeCode
		,target.OneToOneMidwiferyCare = source.OneToOneMidwiferyCare
		,target.OneToOneMidwiferyCareCode = source.OneToOneMidwiferyCareCode
		,target.UKOSSDisease = source.UKOSSDisease
		,target.UKOSSDiseaseCode = source.UKOSSDiseaseCode
		,target.MLUTransferredOut = source.MLUTransferredOut
		,target.MLUTransferReason = source.MLUTransferReason
		,target.MLUTransferReasonCode = source.MLUTransferReasonCode
		,target.MLUTransferredOutCode = source.MLUTransferredOutCode
		,target.MLUTransferDT = source.MLUTransferDT
		,target.EpisiotomyPerformedBy = source.EpisiotomyPerformedBy
		,target.EpisiotomyPerformedByCode = source.EpisiotomyPerformedByCode
		,target.PPHDT = source.PPHDT
		,target.TransferToTheatreDT = source.TransferToTheatreDT
		,target.AdmittedITUDT = source.AdmittedITUDT
		,target.DiagnosisEmbolismDT = source.DiagnosisEmbolismDT
		,target.HysterectomyOrLaparotomyDT = source.HysterectomyOrLaparotomyDT

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime