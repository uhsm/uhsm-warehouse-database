﻿CREATE TABLE [E3].[Address](
	[AddressID] [bigint] NOT NULL,
	[Line1] [nvarchar](50) NULL,
	[Line2] [nvarchar](50) NULL,
	[Line3] [nvarchar](50) NULL,
	[Line4] [nvarchar](50) NULL,
	[Postcode] [nchar](10) NULL,
	[HomeTelephone] [nvarchar](50) NULL,
	[MobileTelephone] [nvarchar](50) NULL,
	[WorkTelephone] [nvarchar](50) NULL,
	[EMail] [nvarchar](100) NULL,
	[Fax] [nvarchar](50) NULL,
	[AreaOfResidence] [varchar](50) NULL,
	[EKRecord] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[OtherTelephone] [nvarchar](50) NULL,
    [Created] DATETIME NOT NULL,
    [Updated] DATETIME NULL,
    [ByWhom] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Address] PRIMARY KEY ([AddressID])
)

GO

