﻿CREATE TABLE [E3].[Baby](
	[BabyID] [bigint] NOT NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NULL,
	[CarePlanID] [int] NULL,
	EKRecord [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[IsRequestedNN4B] [bit] NULL,
	[IsRegisteredInPAS] [bit] NULL,
	[BabyNotesMemoID] [bigint] NULL,
	[DateOfBirth] [datetime] NULL,
	[GestationBirth] nvarchar(30) null, 
    [Created] DATETIME NOT NULL,
    [Updated] DATETIME NULL,
    [ByWhom] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Baby] PRIMARY KEY ([BabyID])
) ON [PRIMARY]