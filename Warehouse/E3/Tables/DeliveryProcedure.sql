﻿CREATE TABLE [E3].[DeliveryProcedure](
	[UserID] [int] NULL,
	[PatientID] [bigint] NULL,
	[PregnancyID] [bigint] NOT NULL,
	[BabiesNumberCode] [bigint] NULL,
	[BladderEmptiedCode] [bigint] NULL,
	[CommentsPostOpCode] [bigint] NULL,
	[ComplicationsCode] [bigint] NULL,
	[DiscussionWithConsCode] [bigint] NULL,
	[DrainageCode] [bigint] NULL,
	[FatStitchCode] [bigint] NULL,
	[FirstProcedureCode] [bigint] NULL,
	[ParietalPeritoneumCode] [bigint] NULL,
	[ProcedureConsentCode] [bigint] NULL,
	[SkinClosureCode] [bigint] NULL,
	[SkinIncisionCode] [bigint] NULL,
	[SterilizedMethodCode] [bigint] NULL,
	[ThromboembolicRiskCode] [bigint] NULL,
	[ThromboprophylaxisPrescribedCode] [bigint] NULL,
	[UterineIncisionCode] [bigint] NULL,
	[UterusClosedCode] [bigint] NULL,
	[UterusTubesOvariesCode] [bigint] NULL,
	[VisceralPeritoneumCode] [bigint] NULL,
	[ActionTaken] [nvarchar](1000) NULL,
	[AnalgesiaPerineum] [nvarchar](1000) NULL,
	[Antibiotics] [nvarchar](1000) NULL,
	[AssistantSurgeon] [nvarchar](1000) NULL,
	[BabiesNumber] [nvarchar](1000) NULL,
	[BladderEmptied] [nvarchar](1000) NULL,
	[BloodLossAtDelivery] [nvarchar](1000) NULL,
	[CatheterInstructions] [nvarchar](1000) NULL,
	[CommentsPostOp] [nvarchar](1000) NULL,
	[Complications] [nvarchar](1000) NULL,
	[DilatationBeforeLSCS] [nvarchar](1000) NULL,
	[DiscussedDT] [nvarchar](1000) NULL,
	[DiscussionWithCons] [nvarchar](1000) NULL,
	[Drainage] [nvarchar](1000) NULL,
	[DrainInstructions] [nvarchar](1000) NULL,
	[Dressing] [nvarchar](1000) NULL,
	[DrugsPostDelivery] [nvarchar](1000) NULL,
	[ExamAfterRepair] [nvarchar](1000) NULL,
	[FatStitch] [nvarchar](1000) NULL,
	[FirstProcedure] [nvarchar](1000) NULL,
	[LabiaSutured] [nvarchar](1000) NULL,
	[LowerSegmentFormed] [nvarchar](1000) NULL,
	[LowerSegmentThin] [nvarchar](1000) NULL,
	[NeedlesAccountedFor] [nvarchar](1000) NULL,
	[ParietalPeritoneum] [nvarchar](1000) NULL,
	[PerinealMuscle] [nvarchar](1000) NULL,
	[PerineumVaginalTears] [nvarchar](1000) NULL,
	[PostVaginalWall] [nvarchar](1000) NULL,
	[ProcedureConsent] [nvarchar](1000) NULL,
	[SheathRepaired] [nvarchar](1000) NULL,
	[SkinClosure] [nvarchar](1000) NULL,
	[SkinIncision] [nvarchar](1000) NULL,
	[SkinIncisionDT] [nvarchar](1000) NULL,
	[SkinRepaired] [nvarchar](1000) NULL,
	[SterilizationHistology] [nvarchar](1000) NULL,
	[SterilizedAtLSCS] [nvarchar](1000) NULL,
	[SterilizedMethod] [nvarchar](1000) NULL,
	[SurgeonAtOperation] [nvarchar](1000) NULL,
	[ThromboembolicRisk] [nvarchar](1000) NULL,
	[ThromboprophylaxisPrescribed] [nvarchar](1000) NULL,
	[UterineCavityChecked] [nvarchar](1000) NULL,
	[UterineIncision] [nvarchar](1000) NULL,
	[UterusClosed] [nvarchar](1000) NULL,
	[UterusTubesOvaries] [nvarchar](1000) NULL,
	[VisceralPeritoneum] [nvarchar](1000) NULL,
	[InstrumentalDT] [nvarchar](1000) NULL,
	[ConsultantInvolvedOVD] [nvarchar](1000) NULL,
	[ConsultantInvolvedOVDCode] [bigint] NULL,
	[OVDPlacePerformed] [nvarchar](1000) NULL,
	[OVDPlacePerformedCode] [bigint] NULL,
    [Created] DATETIME NOT NULL,
    [Updated] DATETIME NULL,
    [ByWhom] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_DeliveryProcedure] PRIMARY KEY ([PregnancyID])
)

GO

