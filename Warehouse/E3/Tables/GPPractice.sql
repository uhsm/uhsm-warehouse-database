﻿CREATE TABLE [E3].[GPPractice](
	[GPPracticeID] [int] NOT NULL,
	[PracticeCode] [nvarchar](30) NULL,
	[EKRecord] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdatedDateTime] [smalldatetime] NULL,
	[PCTID] [int] NULL, 
    [Created] DATETIME NOT NULL,
    [Updated] DATETIME NULL,
    [ByWhom] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_GPPractice] PRIMARY KEY ([GPPracticeID])
)

GO

