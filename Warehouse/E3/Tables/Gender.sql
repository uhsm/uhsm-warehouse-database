﻿CREATE TABLE [E3].[Gender](
	[GenderID] [int] NOT NULL,
	[Gender] [varchar](50) NULL,
	[PASCode] [varchar](30) NULL,
	[EKRecord] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdatedDateTime] [smalldatetime] NULL, 
    [Created] DATETIME NOT NULL,
    [Updated] DATETIME NULL,
    [ByWhom] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Gender] PRIMARY KEY ([GenderID])
)

GO


