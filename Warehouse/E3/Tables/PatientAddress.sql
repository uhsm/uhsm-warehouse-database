﻿CREATE TABLE [E3].[PatientAddress](
	[PatientAddressID] [bigint] NOT NULL,
	[AddressTypeID] [int] NULL,
	[PatientID] [bigint] NULL,
	[BabyID] [bigint] NULL,
	[AddressID] [bigint] NULL,
	[EKRecord] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	LastUpdatedDateTime [smalldatetime] NULL,
    [Created] DATETIME NOT NULL,
    [Updated] DATETIME NULL,
    [ByWhom] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_PatientAddress] PRIMARY KEY ([PatientAddressID])
)

GO

