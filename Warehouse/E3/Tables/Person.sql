﻿CREATE TABLE [E3].[Person](
	[PersonID] [int] NOT NULL,
	[Forename] [nvarchar](50) NULL,
	[Surname] [nvarchar](50) NULL,
	[TitleID] [int] NULL,
	[PersonTypeID] [int] NULL,
	[StaffGradeID] [int] NULL,
	[Qualifications] [nvarchar](50) NULL,
	[Prescribe] [bit] NULL,
	[GMCCode] [nvarchar](20) NULL,
	[PASCode] [nvarchar](50) NULL,
	[Person] [nvarchar](50) NULL,
	[StaffID] [int] NULL,
	[EKRecord] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
	[GPPracticeID] [int] NULL,
	[IsTriumUser] [bit] NULL,
    [Created] DATETIME NOT NULL,
    [Updated] DATETIME NULL,
    [ByWhom] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Person] PRIMARY KEY ([PersonID])
)

GO

