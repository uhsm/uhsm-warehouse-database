﻿CREATE TABLE [E3].[PracticeAddress](
	[PracticeAddressID] [int] NOT NULL,
	[PCTID] [int] NULL,
	[Practice] [int] NULL,
	[Sequence] [nvarchar](50) NULL,
	[AreaID] [tinyint] NULL,
	[Email] [nvarchar](60) NULL,
	[Fax] [nvarchar](20) NULL,
	[HomeTelephone] [nvarchar](20) NULL,
	[Line1] [nvarchar](40) NULL,
	[Line2] [nvarchar](40) NULL,
	[Line3] [nvarchar](40) NULL,
	[Line4] [nvarchar](40) NULL,
	[MobileTelephone] [nvarchar](20) NULL,
	[Postcode] [nvarchar](10) NULL,
	[WorkTelephone] [nvarchar](20) NULL,
	[EKRecord] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[UserID] [int] NULL,
	[LastUpdatedDateTime] [smalldatetime] NULL,
	[GPPracticeID] [int] NULL,
    [Created] DATETIME NOT NULL,
    [Updated] DATETIME NULL,
    [ByWhom] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_PracticeAddress] PRIMARY KEY ([PracticeAddressID])
)

GO

