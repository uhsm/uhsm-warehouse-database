﻿CREATE TABLE [E3].[TestPatient]
(
	[PatientID] BIGINT NOT NULL,
	[Created] [datetime] NOT NULL Default GetDate(),
	CONSTRAINT [PK_Lorenzo_TestPatient] PRIMARY KEY ([PatientID])
)
