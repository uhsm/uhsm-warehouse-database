﻿CREATE PROCEDURE [ESR].[Load] as

exec ESR.LoadAssignment
exec ESR.LoadAssignmentCategory
exec ESR.LoadAssignmentCompetency
exec ESR.LoadAssignmentStatus
exec ESR.LoadBankPostsHeld
exec ESR.LoadCompetency
exec ESR.LoadDirectorate
exec ESR.LoadDivision
exec ESR.LoadEmployeeCategory
exec ESR.LoadLocation
exec ESR.LoadPayroll
exec ESR.LoadPosition
exec ESR.LoadRole
exec ESR.LoadService
exec ESR.LoadStaffGroup
