﻿CREATE procedure [ESR].[LoadAssignmentCompetency] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


--clear out unwanted snapshots
select distinct
	CensusDate
into
	#KeepSnapshot
from
	ESR.AssignmentCompetency
where
	AssignmentCompetency.CensusDate >= dateadd(day, -20, cast(getdate() as date)) --last 20 days
or	datepart(day, dateadd(day, 1, AssignmentCompetency.CensusDate)) = 1 --last day of month
or	datepart(dw, AssignmentCompetency.CensusDate) = 1 --Sunday


delete
from
	ESR.AssignmentCompetency
where
	not exists
	(
	select
		1
	from
		#KeepSnapshot
	where
		#KeepSnapshot.CensusDate = AssignmentCompetency.CensusDate
	)


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,CensusDate = cast(getdate() as date)
	,EncounterChecksum =
		CHECKSUM(
			MeetsRequirement
			,ExpiryDate
			)
into
	#TLoadE3AssignmentCompetency
from
	(
	select
		AssignmentID = cast(AssignmentID as varchar(30))
		,CompetencyCode = cast(CompetencyCode as varchar(255))
		,MeetsRequirement = cast(MeetsRequirement as bit)
		,ExpiryDate = cast(ExpiryDate as date)
	from
		(
		select
			AssignmentID = cast(Assignment as varchar(30))
			,CompetencyCode = cast([Competence Name] as varchar(255))

			,MeetsRequirement =
				cast(
					case
					when [Competence Match] = 'Meets Requirement' then 1
					else 0
					end

					as bit
				)

			,ExpiryDate = cast(nullif([Expiry Date], '') as date)

		from
			ESR.dbo.AssignmentCompetency

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3AssignmentCompetency on #TLoadE3AssignmentCompetency
	(
	CensusDate
	,AssignmentID
	,CompetencyCode
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	ESR.AssignmentCompetency target
using
	(
	select
		*
	from
		#TLoadE3AssignmentCompetency
	
	) source
	on	source.CensusDate = target.CensusDate
	and	source.AssignmentID = target.AssignmentID
	and	source.CompetencyCode = target.CompetencyCode

when not matched by source
and	target.CensusDate = cast(getdate() as date)
then
	delete

when not matched
then
	insert
		(
		AssignmentID
		,CensusDate
		,CompetencyCode
		,MeetsRequirement
		,ExpiryDate

		,Created
		,ByWhom
		)
	values
		(
		source.AssignmentID
		,source.CensusDate
		,source.CompetencyCode
		,source.MeetsRequirement
		,source.ExpiryDate

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.MeetsRequirement
		,target.ExpiryDate
		)

then
	update
	set
		target.MeetsRequirement = source.MeetsRequirement
		,target.ExpiryDate = source.ExpiryDate

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime