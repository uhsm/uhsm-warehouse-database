﻿CREATE procedure [ESR].[LoadAssignmentStatus] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			AssignmentStatus
			)
into
	#TLoadE3AssignmentStatus
from
	(
	select distinct
		AssignmentStatusCode = cast(AssignmentStatusCode as varchar(255))
		,AssignmentStatus = cast(AssignmentStatus as varchar(255))
	from
		(
		select
			AssignmentStatusCode = [Assignment Status]
			,AssignmentStatus = [Assignment Status]
		from
			ESR.dbo.Assignment
		where
			[Assignment Status] is not null

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3AssignmentStatus on #TLoadE3AssignmentStatus
	(
	AssignmentStatusCode
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	ESR.AssignmentStatus target
using
	(
	select
		*
	from
		#TLoadE3AssignmentStatus
	
	) source
	on	source.AssignmentStatusCode = target.AssignmentStatusCode

when not matched
then
	insert
		(
		AssignmentStatusCode
		,AssignmentStatus

		,Created
		,ByWhom
		)
	values
		(
		source.AssignmentStatusCode
		,source.AssignmentStatus

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.AssignmentStatus
		)

then
	update
	set
		target.AssignmentStatus = source.AssignmentStatus

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime