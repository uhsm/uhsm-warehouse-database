﻿CREATE procedure [ESR].[LoadBankPostsHeld] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			BankPostsHeld
			)
into
	#TLoadE3BankPostsHeld
from
	(
	select distinct
		BankPostsHeldCode = cast(BankPostsHeldCode as varchar(255))
		,BankPostsHeld = cast(BankPostsHeld as varchar(255))
	from
		(
		select
			BankPostsHeldCode = [Bank Posts Held]
			,BankPostsHeld =
				cast(
					right(
						[Bank Posts Held]

						,len([Bank Posts Held]) -
						charindex(
							' '
							,[Bank Posts Held]
						)
					)

					as varchar(255)
				)

		from
			ESR.dbo.Assignment
		where
			[Bank Posts Held] is not null

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3BankPostsHeld on #TLoadE3BankPostsHeld
	(
	BankPostsHeldCode
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	ESR.BankPostsHeld target
using
	(
	select
		*
	from
		#TLoadE3BankPostsHeld
	
	) source
	on	source.BankPostsHeldCode = target.BankPostsHeldCode

when not matched
then
	insert
		(
		BankPostsHeldCode
		,BankPostsHeld

		,Created
		,ByWhom
		)
	values
		(
		source.BankPostsHeldCode
		,source.BankPostsHeld

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.BankPostsHeld
		)

then
	update
	set
		target.BankPostsHeld = source.BankPostsHeld

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime