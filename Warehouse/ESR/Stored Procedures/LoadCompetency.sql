﻿CREATE procedure [ESR].[LoadCompetency] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			Competency
			)
into
	#TLoadE3Competency
from
	(
	select distinct
		CompetencyCode = cast(CompetencyCode as varchar(255))
		,Competency = cast(Competency as varchar(255))
	from
		(
		select
			CompetencyCode = cast([Competence Name] as varchar(255))

			,Competency =
				replace(
					right(
						[Competence Name]

						,len([Competence Name]) -
						charindex(
							'|'
							,[Competence Name]

							,charindex(
								'|'
								,[Competence Name]
							) + 1
						)
					)
					,'|'
					,''
				)

		from
			ESR.dbo.AssignmentCompetency

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3Competency on #TLoadE3Competency
	(
	CompetencyCode
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	ESR.Competency target
using
	(
	select
		*
	from
		#TLoadE3Competency
	
	) source
	on	source.CompetencyCode = target.CompetencyCode

when not matched
then
	insert
		(
		CompetencyCode
		,Competency

		,Created
		,ByWhom
		)
	values
		(
		source.CompetencyCode
		,source.Competency

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.Competency
		)

then
	update
	set
		target.Competency = source.Competency

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime