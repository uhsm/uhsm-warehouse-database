﻿CREATE procedure [ESR].[LoadDirectorate] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			Directorate
			)
into
	#TLoadE3Directorate
from
	(
	select distinct
		DirectorateCode = cast(DirectorateCode as varchar(255))
		,Directorate = cast(Directorate as varchar(255))
	from
		(
		select
			DirectorateCode = cast([Sub Directorate] as varchar(255))

			,Directorate =
				cast(
					right(
						[Sub Directorate]

						,len([Sub Directorate]) -
						charindex(
							' '
							,[Sub Directorate]
						)
					)

					as varchar(255)
				)

		from
			ESR.dbo.Assignment
		where
			[Sub Directorate] is not null

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3Directorate on #TLoadE3Directorate
	(
	DirectorateCode
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	ESR.Directorate target
using
	(
	select
		*
	from
		#TLoadE3Directorate
	
	) source
	on	source.DirectorateCode = target.DirectorateCode

when not matched
then
	insert
		(
		DirectorateCode
		,Directorate

		,Created
		,ByWhom
		)
	values
		(
		source.DirectorateCode
		,source.Directorate

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.Directorate
		)

then
	update
	set
		target.Directorate = source.Directorate

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime