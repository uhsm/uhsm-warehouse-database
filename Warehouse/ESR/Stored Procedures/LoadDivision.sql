﻿CREATE procedure [ESR].[LoadDivision] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			Division
			)
into
	#TLoadE3Division
from
	(
	select distinct
		DivisionCode = cast(DivisionCode as varchar(255))
		,Division = cast(Division as varchar(255))
	from
		(
		select
			DivisionCode = cast([Directorate] as varchar(255))

			,Division =
				cast(
					right(
						[Directorate]

						,len([Directorate]) -
						charindex(
							' '
							,[Directorate]
						)
					)

					as varchar(255)
				)

		from
			ESR.dbo.Assignment
		where
			[Directorate] is not null

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3Division on #TLoadE3Division
	(
	DivisionCode
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	ESR.Division target
using
	(
	select
		*
	from
		#TLoadE3Division
	
	) source
	on	source.DivisionCode = target.DivisionCode

when not matched
then
	insert
		(
		DivisionCode
		,Division

		,Created
		,ByWhom
		)
	values
		(
		source.DivisionCode
		,source.Division

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.Division
		)

then
	update
	set
		target.Division = source.Division

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime