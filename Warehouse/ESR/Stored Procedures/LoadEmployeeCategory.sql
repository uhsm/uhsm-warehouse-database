﻿CREATE procedure [ESR].[LoadEmployeeCategory] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			EmployeeCategory
			)
into
	#TLoadE3EmployeeCategory
from
	(
	select distinct
		EmployeeCategoryCode = cast(EmployeeCategoryCode as varchar(255))
		,EmployeeCategory = cast(EmployeeCategory as varchar(255))
	from
		(
		select
			EmployeeCategoryCode = [Employee Category]
			,EmployeeCategory = [Employee Category]
		from
			ESR.dbo.Assignment
		where
			[Employee Category] is not null

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3EmployeeCategory on #TLoadE3EmployeeCategory
	(
	EmployeeCategoryCode
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	ESR.EmployeeCategory target
using
	(
	select
		*
	from
		#TLoadE3EmployeeCategory
	
	) source
	on	source.EmployeeCategoryCode = target.EmployeeCategoryCode

when not matched
then
	insert
		(
		EmployeeCategoryCode
		,EmployeeCategory

		,Created
		,ByWhom
		)
	values
		(
		source.EmployeeCategoryCode
		,source.EmployeeCategory

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.EmployeeCategory
		)

then
	update
	set
		target.EmployeeCategory = source.EmployeeCategory

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime