﻿CREATE procedure [ESR].[LoadLocation] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			Location
			)
into
	#TLoadE3Location
from
	(
	select distinct
		LocationCode = cast(LocationCode as varchar(255))
		,Location = cast(Location as varchar(255))
	from
		(
		select
			LocationCode = cast([Ward Department] as varchar(255))

			,Location =
				cast(
					right(
						[Ward Department]

						,len([Ward Department]) -
						charindex(
							' '
							,[Ward Department]
						)
					)

					as varchar(255)
				)

		from
			ESR.dbo.Assignment
		where
			[Ward Department] is not null

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3Location on #TLoadE3Location
	(
	LocationCode
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	ESR.Location target
using
	(
	select
		*
	from
		#TLoadE3Location
	
	) source
	on	source.LocationCode = target.LocationCode

when not matched
then
	insert
		(
		LocationCode
		,Location

		,Created
		,ByWhom
		)
	values
		(
		source.LocationCode
		,source.Location

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.Location
		)

then
	update
	set
		target.Location = source.Location

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime