﻿CREATE procedure [ESR].[LoadPayroll] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			Payroll
			)
into
	#TLoadE3Payroll
from
	(
	select distinct
		PayrollCode = cast(PayrollCode as varchar(255))
		,Payroll = cast(Payroll as varchar(255))
	from
		(
		select
			PayrollCode = [Payroll]
			,Payroll =
				cast(
					right(
						[Payroll]

						,len([Payroll]) -
						charindex(
							' '
							,[Payroll]
						)
					)

					as varchar(255)
				)

		from
			ESR.dbo.Assignment
		where
			[Payroll] is not null

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3Payroll on #TLoadE3Payroll
	(
	PayrollCode
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	ESR.Payroll target
using
	(
	select
		*
	from
		#TLoadE3Payroll
	
	) source
	on	source.PayrollCode = target.PayrollCode

when not matched
then
	insert
		(
		PayrollCode
		,Payroll

		,Created
		,ByWhom
		)
	values
		(
		source.PayrollCode
		,source.Payroll

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.Payroll
		)

then
	update
	set
		target.Payroll = source.Payroll

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime