﻿CREATE procedure [ESR].[LoadPosition] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			Position
			)
into
	#TLoadE3Position
from
	(
	select distinct
		PositionCode = cast(PositionCode as varchar(255))
		,Position = cast(Position as varchar(255))
	from
		(
		select
			PositionCode = [Position Title]
			,Position = [Position Title]
		from
			ESR.dbo.Assignment
		where
			[Position] is not null

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3Position on #TLoadE3Position
	(
	PositionCode
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	ESR.Position target
using
	(
	select
		*
	from
		#TLoadE3Position
	
	) source
	on	source.PositionCode = target.PositionCode

when not matched
then
	insert
		(
		PositionCode
		,Position

		,Created
		,ByWhom
		)
	values
		(
		source.PositionCode
		,source.Position

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.Position
		)

then
	update
	set
		target.Position = source.Position

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime