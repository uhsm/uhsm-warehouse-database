﻿CREATE procedure [ESR].[LoadRole] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			Role
			)
into
	#TLoadE3Role
from
	(
	select distinct
		RoleCode = cast(RoleCode as varchar(255))
		,Role = cast(Role as varchar(255))
	from
		(
		select
			RoleCode = [Role]
			,Role = [Role]
		from
			ESR.dbo.Assignment
		where
			[Role] is not null

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3Role on #TLoadE3Role
	(
	RoleCode
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	ESR.Role target
using
	(
	select
		*
	from
		#TLoadE3Role
	
	) source
	on	source.RoleCode = target.RoleCode

when not matched
then
	insert
		(
		RoleCode
		,Role

		,Created
		,ByWhom
		)
	values
		(
		source.RoleCode
		,source.Role

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.Role
		)

then
	update
	set
		target.Role = source.Role

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime