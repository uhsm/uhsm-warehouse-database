﻿CREATE procedure [ESR].[LoadService] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			Service
			)
into
	#TLoadE3Service
from
	(
	select distinct
		ServiceCode = cast(ServiceCode as varchar(255))
		,Service = cast(Service as varchar(255))
	from
		(
		select
			ServiceCode = cast([Specialty] as varchar(255))

			,Service =
				cast(
					right(
						[Specialty]

						,len([Specialty]) -
						charindex(
							' '
							,[Specialty]
						)
					)

					as varchar(255)
				)

		from
			ESR.dbo.Assignment
		where
			[Specialty] is not null

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3Service on #TLoadE3Service
	(
	ServiceCode
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	ESR.Service target
using
	(
	select
		*
	from
		#TLoadE3Service
	
	) source
	on	source.ServiceCode = target.ServiceCode

when not matched
then
	insert
		(
		ServiceCode
		,Service

		,Created
		,ByWhom
		)
	values
		(
		source.ServiceCode
		,source.Service

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.Service
		)

then
	update
	set
		target.Service = source.Service

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime