﻿CREATE procedure [ESR].[LoadStaffGroup] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			StaffGroup
			)
into
	#TLoadE3StaffGroup
from
	(
	select distinct
		StaffGroupCode = cast(StaffGroupCode as varchar(255))
		,StaffGroup = cast(StaffGroup as varchar(255))
	from
		(
		select
			StaffGroupCode = [Staff Group]
			,StaffGroup = [Staff Group]
		from
			ESR.dbo.Assignment
		where
			[Staff Group] is not null

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadE3StaffGroup on #TLoadE3StaffGroup
	(
	StaffGroupCode
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	ESR.StaffGroup target
using
	(
	select
		*
	from
		#TLoadE3StaffGroup
	
	) source
	on	source.StaffGroupCode = target.StaffGroupCode

when not matched
then
	insert
		(
		StaffGroupCode
		,StaffGroup

		,Created
		,ByWhom
		)
	values
		(
		source.StaffGroupCode
		,source.StaffGroup

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.StaffGroup
		)

then
	update
	set
		target.StaffGroup = source.StaffGroup

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime