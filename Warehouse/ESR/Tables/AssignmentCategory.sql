﻿CREATE TABLE [ESR].[BankPostsHeld]
(
	[BankPostsHeldCode] [varchar](255) NOT NULL,
	[BankPostsHeld] [varchar](255) NOT NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,
	
    CONSTRAINT [PK_ESR_BankPostsHeld] PRIMARY KEY ([BankPostsHeldCode]),
)
