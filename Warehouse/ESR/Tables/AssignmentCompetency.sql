﻿CREATE TABLE [ESR].[AssignmentCompetency]
(
	[EncounterRecno] int identity,
	[AssignmentID] [varchar](30) NOT NULL,
	[CensusDate] date not null,
	[CompetencyCode] [varchar](255) NOT NULL,
	[MeetsRequirement] [bit] NULL,
	[ExpiryDate] [date] NULL, 

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,
	
   CONSTRAINT [PK_ESR_AssignmentCompetency] PRIMARY KEY ([CensusDate], [AssignmentID], [CompetencyCode])
)
