﻿CREATE TABLE [ESR].[AssignmentStatus]
(
	[AssignmentStatusCode] [varchar](255) NOT NULL,
	[AssignmentStatus] [varchar](255) NOT NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,
	
    CONSTRAINT [PK_ESR_AssignmentStatus] PRIMARY KEY ([AssignmentStatusCode]),
)
