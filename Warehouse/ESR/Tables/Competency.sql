﻿CREATE TABLE [ESR].[Competency]
(
	[CompetencyCode] [varchar](255) NOT NULL,
	[Competency] [varchar](255) NOT NULL,
	[IsNew] [bit] NULL,
	[GroupLevelID] [int] NULL,
	[GroupTypeID] [int] NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,
	
    CONSTRAINT [PK_ESR_Competency] PRIMARY KEY ([CompetencyCode]), 
    CONSTRAINT [FK_Competency_GroupLevel] FOREIGN KEY ([GroupLevelID]) REFERENCES [ESR].[GroupLevel]([GroupLevelID]), 
    CONSTRAINT [FK_Competency_GroupType] FOREIGN KEY ([GroupTypeID]) REFERENCES [ESR].[GroupType]([GroupTypeID])
)
