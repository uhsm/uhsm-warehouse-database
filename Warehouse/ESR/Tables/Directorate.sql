﻿CREATE TABLE [ESR].[Directorate]
(
	[DirectorateCode] [varchar](255) NOT NULL,
	[Directorate] [varchar](255) NOT NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,
	
    CONSTRAINT [PK_ESR_Directorate] PRIMARY KEY ([DirectorateCode]),
)
