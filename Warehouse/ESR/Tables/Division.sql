﻿CREATE TABLE [ESR].[Division]
(
	[DivisionCode] [varchar](255) NOT NULL,
	[Division] [varchar](255) NOT NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,
	
    CONSTRAINT [PK_ESR_Division] PRIMARY KEY ([DivisionCode]),
)
