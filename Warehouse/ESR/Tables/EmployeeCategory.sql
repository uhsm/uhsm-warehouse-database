﻿CREATE TABLE [ESR].[EmployeeCategory]
(
	[EmployeeCategoryCode] [varchar](255) NOT NULL,
	[EmployeeCategory] [varchar](255) NOT NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,
	
    CONSTRAINT [PK_ESR_EmployeeCategory] PRIMARY KEY ([EmployeeCategoryCode]),
)
