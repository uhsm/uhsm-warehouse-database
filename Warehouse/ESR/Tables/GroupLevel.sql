﻿CREATE TABLE [ESR].[GroupLevel]
(
	[GroupLevelID] [int] NOT NULL,
	[GroupLevel] [varchar] (255) NULL,

	[Created] [datetime] NOT NULL default getdate(),
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL default suser_name(),
	
   CONSTRAINT [PK_ESR_GroupLevel] PRIMARY KEY ([GroupLevelID])
)
