﻿CREATE TABLE [ESR].[GroupType]
(
	[GroupTypeID] [int] NOT NULL,
	[GroupType] [varchar] (255) NULL,

	[Created] [datetime] NOT NULL default getdate(),
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL default suser_name(),
	
   CONSTRAINT [PK_ESR_GroupType] PRIMARY KEY ([GroupTypeID])
)
