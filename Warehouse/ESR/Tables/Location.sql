﻿CREATE TABLE [ESR].[Location]
(
	[LocationCode] [varchar](255) NOT NULL,
	[Location] [varchar](255) NOT NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,
	
    CONSTRAINT [PK_ESR_Location] PRIMARY KEY ([LocationCode]),
)
