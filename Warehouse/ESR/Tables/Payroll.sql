﻿CREATE TABLE [ESR].[Payroll]
(
	[PayrollCode] [varchar](255) NOT NULL,
	[Payroll] [varchar](255) NOT NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,
	
    CONSTRAINT [PK_ESR_Payroll] PRIMARY KEY ([PayrollCode]),
)
