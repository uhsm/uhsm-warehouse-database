﻿CREATE TABLE [ESR].[Position]
(
	[PositionCode] [varchar](255) NOT NULL,
	[Position] [varchar](255) NOT NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,
	
    CONSTRAINT [PK_ESR_Position] PRIMARY KEY ([PositionCode]),
)
