﻿CREATE TABLE [ESR].[Role]
(
	[RoleCode] [varchar](255) NOT NULL,
	[Role] [varchar](255) NOT NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,
	
    CONSTRAINT [PK_ESR_Role] PRIMARY KEY ([RoleCode]),
)
