﻿CREATE TABLE [ESR].[Service]
(
	[ServiceCode] [varchar](255) NOT NULL,
	[Service] [varchar](255) NOT NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,
	
    CONSTRAINT [PK_ESR_Service] PRIMARY KEY ([ServiceCode]),
)
