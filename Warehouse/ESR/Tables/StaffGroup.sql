﻿CREATE TABLE [ESR].[StaffGroup]
(
	[StaffGroupCode] [varchar](255) NOT NULL,
	[StaffGroup] [varchar](255) NOT NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,
	
    CONSTRAINT [PK_ESR_StaffGroup] PRIMARY KEY ([StaffGroupCode]),
)
