﻿CREATE PROCEDURE Lorenzo.AssignClinicalCodingSequenceNumber as

--The ClinicalCodingOrder column in Lorenzo is not reliable - for example, it contains many 1's for diagnosis, for a single inpatient episode making it difficult to chose the primary diagnosis.
--This procedure orders and sequences consistently.

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare @updated int


update Lorenzo.ClinicalCoding
set
	SequenceNumber = ClinicalCodingSequenced.SequenceNumber
from
	(
	select
		ClinicalCoding.ClinicalCodingID
		,SequenceNumber =
			row_number() over
				(
				partition by
					SourceCode
					,ClinicalCodingSubtypeCode
					,SourceID

				order by
					ClinicalCodingOrder
					,CreatedDateTime desc
					,ClinicalCodingID desc
				)
	from
		Lorenzo.ClinicalCoding
	) ClinicalCodingSequenced

where
	ClinicalCodingSequenced.ClinicalCodingID = ClinicalCoding.ClinicalCodingID
and	(
		ClinicalCodingSequenced.SequenceNumber <> ClinicalCoding.SequenceNumber
	or	ClinicalCoding.SequenceNumber is null
	)


select
	@updated = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime
