﻿CREATE procedure [Lorenzo].[Load] as

exec Lorenzo.LoadAdmissionDecision
exec Lorenzo.LoadAdmissionOffer
exec Lorenzo.LoadBirth
exec Lorenzo.LoadClinicalCoding
exec Lorenzo.LoadContract
exec Lorenzo.LoadMaternitySpell
exec Lorenzo.LoadOverseasVisitorStatus
exec Lorenzo.LoadPatient
exec Lorenzo.LoadPatientAddress
exec Lorenzo.LoadPatientAddressRole
exec Lorenzo.LoadPatientIdentifier
exec Lorenzo.LoadPatientProfessionalCarer
exec Lorenzo.LoadProfessionalCareEpisode
exec Lorenzo.LoadProviderSpell
exec Lorenzo.LoadReferral
exec Lorenzo.LoadRTT
exec Lorenzo.LoadServicePoint
exec Lorenzo.LoadServicePointStay
exec Lorenzo.LoadWaitingList
exec Lorenzo.LoadUser
exec Lorenzo.LoadProfessionalCarer
exec Lorenzo.LoadNoteRole
exec Lorenzo.LoadScheduleEvent
exec Lorenzo.LoadAugmentedCarePeriod
exec Lorenzo.LoadDependantRule
exec Lorenzo.LoadServicePointSlot
exec Lorenzo.LoadServicePointSession
exec Lorenzo.LoadWaitingListRule
exec Lorenzo.LoadWaitingListSuspension

