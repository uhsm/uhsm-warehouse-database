﻿CREATE procedure [Lorenzo].[LoadAdmissionDecision] as 


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(AdmissionDecision.LorenzoCreated)
		from
			Lorenzo.AdmissionDecision
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			ClinicalCostCentreID
			,SpecialtyID
			,PriorityID
			,ReferralID
			,ProfessionalCarerID
			,PatientID
			,ScheduleID
			,DecisionToAdmitDateTime
			,CreatedDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,SessionTransactionID
			,PriorPointerID
			,ExternalKey
			,NoAdmissionAfterDateTime
			,StaffTeamID
			,HealthOrganisationOwnerID
			,LorenzoCreated
			)
into
	#TLoadPASAdmissionDecision
from
	(
	select
		AdmissionDecisionID
		,ClinicalCostCentreID = cast(ClinicalCostCentreID as int)
		,SpecialtyID = cast(SpecialtyID as int)
		,PriorityID = cast(PriorityID as int)
		,ReferralID = cast(ReferralID as int)
		,ProfessionalCarerID = cast(ProfessionalCarerID as int)
		,PatientID = cast(PatientID as int)
		,ScheduleID = cast(ScheduleID as int)
		,DecisionToAdmitDateTime = cast(DecisionToAdmitDateTime as datetime)
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(50))
		,UserModify = cast(nullif(UserModify, '') as varchar(50))
		,SessionTransactionID = cast(SessionTransactionID as int)
		,PriorPointerID = cast(PriorPointerID as int)
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(50))
		,NoAdmissionAfterDateTime = cast(NoAdmissionAfterDateTime as datetime)
		,StaffTeamID = cast(StaffTeamID as int)
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as int)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			AdmissionDecisionID = ADMDC_REFNO
			,ClinicalCostCentreID = CCCCC_REFNO
			,SpecialtyID = SPECT_REFNO
			,PriorityID = PRITY_REFNO
			,ReferralID = REFRL_REFNO
			,ProfessionalCarerID = PROCA_REFNO
			,PatientID = PATNT_REFNO
			,ScheduleID = SCHDL_REFNO
			,DecisionToAdmitDateTime = DTA_DTTM
			,CreatedDateTime = CREATE_DTTM
			,ModifiedDateTime = MODIF_DTTM
			,UserCreate = USER_CREATE
			,UserModify = USER_MODIF
			,SessionTransactionID = STRAN_REFNO
			,PriorPointerID = PRIOR_POINTER
			,ExternalKey = EXTERNAL_KEY
			,NoAdmissionAfterDateTime = ADMIT_NOTAF_DTTM
			,StaffTeamID = STEAM_REFNO
			,HealthOrganisationOwnerID = OWNER_HEORG_REFNO
			,LorenzoCreated = Created


			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.AdmissionDecision
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASAdmissionDecision on #TLoadPASAdmissionDecision
	(
	AdmissionDecisionID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.AdmissionDecision target
using
	(
	select
		*
	from
		#TLoadPASAdmissionDecision
	
	) source
	on	source.AdmissionDecisionID = target.AdmissionDecisionID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		 AdmissionDecisionID
		,ClinicalCostCentreID
		,SpecialtyID
		,PriorityID
		,ReferralID
		,ProfessionalCarerID
		,PatientID
		,ScheduleID
		,DecisionToAdmitDateTime
		,CreatedDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,SessionTransactionID
		,PriorPointerID
		,ExternalKey
		,NoAdmissionAfterDateTime
		,StaffTeamID
		,HealthOrganisationOwnerID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		 source.AdmissionDecisionID
		,source.ClinicalCostCentreID
		,source.SpecialtyID
		,source.PriorityID
		,source.ReferralID
		,source.ProfessionalCarerID
		,source.PatientID
		,source.ScheduleID
		,source.DecisionToAdmitDateTime
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.UserCreate
		,source.UserModify
		,source.SessionTransactionID
		,source.PriorPointerID
		,source.ExternalKey
		,source.NoAdmissionAfterDateTime
		,source.StaffTeamID
		,source.HealthOrganisationOwnerID
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.ClinicalCostCentreID
		,target.SpecialtyID
		,target.PriorityID
		,target.ReferralID
		,target.ProfessionalCarerID
		,target.PatientID
		,target.ScheduleID
		,target.DecisionToAdmitDateTime
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.SessionTransactionID
		,target.PriorPointerID
		,target.ExternalKey
		,target.NoAdmissionAfterDateTime
		,target.StaffTeamID
		,target.HealthOrganisationOwnerID
		,target.LorenzoCreated
		)

then
	update
	set
		target.ClinicalCostCentreID = source.ClinicalCostCentreID
		,target.SpecialtyID = source.SpecialtyID
		,target.PriorityID = source.PriorityID
		,target.ReferralID = source.ReferralID
		,target.ProfessionalCarerID = source.ProfessionalCarerID
		,target.PatientID = source.PatientID
		,target.ScheduleID = source.ScheduleID
		,target.DecisionToAdmitDateTime = source.DecisionToAdmitDateTime
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.SessionTransactionID = source.SessionTransactionID
		,target.PriorPointerID = source.PriorPointerID
		,target.ExternalKey = source.ExternalKey
		,target.NoAdmissionAfterDateTime = source.NoAdmissionAfterDateTime
		,target.StaffTeamID = source.StaffTeamID
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime