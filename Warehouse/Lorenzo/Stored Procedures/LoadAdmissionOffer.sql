﻿CREATE procedure [Lorenzo].[LoadAdmissionOffer] as 


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(AdmissionOffer.LorenzoCreated)
		from
			Lorenzo.AdmissionOffer
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			WaitingListID
			,DeferralReasonID
			,OfferOutcomeID
			,ServicePointBedID
			,AdmissionOfferDateTime
			,AdmissionOfferOutcomeDateTime
			,TCIDateTime
			,OfferCancellationDateTime
			,ConfirmationDateTime
			,Comments
			,IsDeferred
			,DeferralDateTime
			,ServicePointID
			,BedCategoryID
			,TransferBedCategoryID
			,ExpectedDischargeDateTime
			,TransferServicePointID
			,TransferDateTime
			,LatestPossibleTCIDateTime
			,ConfirmationStatusID
			,OperationDateTime
			,NilByMouthDateTime
			,IsAccompanied
			,IsLockedBed
			,ScheduleCancellationReasonID
			,CreatedDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,SessionTransactionID
			,PriorPointerID
			,ExternalKey
			,ServicePointBedCopyID
			,AdmissionOfferTypeID
			,BookingTypeID
			,InpatientOtherDateTime
			,ShortNoticeFlag
			,RTTStatusID
			,EarliestReasonableOfferDateTime
			,VerbalFlag
			,IsPatientUnavailable
			,ConfirmationDetailID
			,OutcomeDetailID
			,HealthOrganisationOwnerID
			,LorenzoCreated
			)
into
	#TLoadPASAdmissionOffer
from
	(
	select
		AdmissionOfferID
		,WaitingListID = cast(WaitingListID as int)
		,DeferralReasonID = cast(DeferralReasonID as int)
		,OfferOutcomeID = cast(OfferOutcomeID as int)
		,ServicePointBedID = cast(ServicePointBedID as int)
		,AdmissionOfferDateTime = cast(AdmissionOfferDateTime as datetime)
		,AdmissionOfferOutcomeDateTime = cast(AdmissionOfferOutcomeDateTime as datetime)
		,TCIDateTime = cast(TCIDateTime as datetime)
		,OfferCancellationDateTime = cast(OfferCancellationDateTime as datetime)
		,ConfirmationDateTime = cast(ConfirmationDateTime as datetime)
		,Comments = cast(nullif(Comments, '') as varchar(255))
		,IsDeferred = cast(nullif(IsDeferred, '') as char(1))
		,DeferralDateTime = cast(DeferralDateTime as datetime)
		,ServicePointID = cast(ServicePointID as int)
		,BedCategoryID = cast(BedCategoryID as int)
		,TransferBedCategoryID = cast(TransferBedCategoryID as int)
		,ExpectedDischargeDateTime = cast(ExpectedDischargeDateTime as datetime)
		,TransferServicePointID = cast(TransferServicePointID as int)
		,TransferDateTime = cast(TransferDateTime as datetime)
		,LatestPossibleTCIDateTime = cast(LatestPossibleTCIDateTime as datetime)
		,ConfirmationStatusID = cast(ConfirmationStatusID as int)
		,OperationDateTime = cast(OperationDateTime as datetime)
		,NilByMouthDateTime = cast(NilByMouthDateTime as datetime)
		,IsAccompanied = cast(nullif(IsAccompanied, '') as char(1))
		,IsLockedBed = cast(nullif(IsLockedBed, '') as char(1))
		,ScheduleCancellationReasonID = cast(ScheduleCancellationReasonID as int)
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(50))
		,UserModify = cast(nullif(UserModify, '') as varchar(50))
		,SessionTransactionID = cast(SessionTransactionID as int)
		,PriorPointerID = cast(PriorPointerID as int)
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(50))
		,ServicePointBedCopyID = cast(ServicePointBedCopyID as int)
		,AdmissionOfferTypeID = cast(AdmissionOfferTypeID as int)
		,BookingTypeID = cast(BookingTypeID as int)
		,InpatientOtherDateTime = cast(InpatientOtherDateTime as datetime)
		,ShortNoticeFlag = cast(nullif(ShortNoticeFlag, '') as char(1))
		,RTTStatusID = cast(nullif(RTTStatusID, '') as varchar(50))
		,EarliestReasonableOfferDateTime = cast(EarliestReasonableOfferDateTime as datetime)
		,VerbalFlag = cast(nullif(VerbalFlag, '') as varchar(50))
		,IsPatientUnavailable = cast(nullif(IsPatientUnavailable, '') as char(1))
		,ConfirmationDetailID = cast(ConfirmationDetailID as int)
		,OutcomeDetailID = cast(OutcomeDetailID as int)
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as int)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			AdmissionOfferID = ADMOF_REFNO
			,WaitingListID = WLIST_REFNO
			,DeferralReasonID = DEFER_REFNO
			,OfferOutcomeID = OFOCM_REFNO
			,ServicePointBedID = SPBED_REFNO
			,AdmissionOfferDateTime = ADMOF_DTTM
			,AdmissionOfferOutcomeDateTime = OFOCM_DTTM
			,TCIDateTime = TCI_DTTM
			,OfferCancellationDateTime = CANCEL_DTTM
			,ConfirmationDateTime = CONFM_DTTM
			,Comments = COMMENTS
			,IsDeferred = DEFER_FLAG
			,DeferralDateTime = DEFER_DTTM
			,ServicePointID = SPONT_REFNO
			,BedCategoryID = BDCAT_REFNO
			,TransferBedCategoryID = XFER_BDCAT_REFNO
			,ExpectedDischargeDateTime = EXPDS_DTTM
			,TransferServicePointID = XFER_SPONT_REFNO
			,TransferDateTime = XFER_DTTM
			,LatestPossibleTCIDateTime = TCI_TO_DTTM
			,ConfirmationStatusID = CONFM_REFNO
			,OperationDateTime = OPERATION_DTTM
			,NilByMouthDateTime = NIL_BY_MOUTH_DTTM
			,IsAccompanied = ACCOMPANIED_FLAG
			,IsLockedBed = LOCKED_BED_FLAG
			,ScheduleCancellationReasonID = CANCR_REFNO
			,CreatedDateTime = CREATE_DTTM
			,ModifiedDateTime = MODIF_DTTM
			,UserCreate = USER_CREATE
			,UserModify = USER_MODIF
			,SessionTransactionID = STRAN_REFNO
			,PriorPointerID = PRIOR_POINTER
			,ExternalKey = EXTERNAL_KEY
			,ServicePointBedCopyID = SPBED_ORI_REFNO
			,AdmissionOfferTypeID = ADOFT_REFNO
			,BookingTypeID = IPBKT_REFNO
			,InpatientOtherDateTime = OTHER_DTTM
			,ShortNoticeFlag = SHORT_NOTICE_FLAG
			,RTTStatusID = RTTST_REFNO
			,EarliestReasonableOfferDateTime = ERO_DTTM
			,VerbalFlag = VERBAL_FLAG
			,IsPatientUnavailable = PATNT_UNAVAILABLE
			,ConfirmationDetailID = CONDT_REFNO
			,OutcomeDetailID = OCMDT_REFNO
			,HealthOrganisationOwnerID = OWNER_HEORG_REFNO
			,LorenzoCreated = Created

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.AdmissionOffer
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASAdmissionOffer on #TLoadPASAdmissionOffer
	(
	AdmissionOfferID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.AdmissionOffer target
using
	(
	select
		*
	from
		#TLoadPASAdmissionOffer
	
	) source
	on	source.AdmissionOfferID = target.AdmissionOfferID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		AdmissionOfferID
		,WaitingListID
		,DeferralReasonID
		,OfferOutcomeID
		,ServicePointBedID
		,AdmissionOfferDateTime
		,AdmissionOfferOutcomeDateTime
		,TCIDateTime
		,OfferCancellationDateTime
		,ConfirmationDateTime
		,Comments
		,IsDeferred
		,DeferralDateTime
		,ServicePointID
		,BedCategoryID
		,TransferBedCategoryID
		,ExpectedDischargeDateTime
		,TransferServicePointID
		,TransferDateTime
		,LatestPossibleTCIDateTime
		,ConfirmationStatusID
		,OperationDateTime
		,NilByMouthDateTime
		,IsAccompanied
		,IsLockedBed
		,ScheduleCancellationReasonID
		,CreatedDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,SessionTransactionID
		,PriorPointerID
		,ExternalKey
		,ServicePointBedCopyID
		,AdmissionOfferTypeID
		,BookingTypeID
		,InpatientOtherDateTime
		,ShortNoticeFlag
		,RTTStatusID
		,EarliestReasonableOfferDateTime
		,VerbalFlag
		,IsPatientUnavailable
		,ConfirmationDetailID
		,OutcomeDetailID
		,HealthOrganisationOwnerID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		source.AdmissionOfferID
		,source.WaitingListID
		,source.DeferralReasonID
		,source.OfferOutcomeID
		,source.ServicePointBedID
		,source.AdmissionOfferDateTime
		,source.AdmissionOfferOutcomeDateTime
		,source.TCIDateTime
		,source.OfferCancellationDateTime
		,source.ConfirmationDateTime
		,source.Comments
		,source.IsDeferred
		,source.DeferralDateTime
		,source.ServicePointID
		,source.BedCategoryID
		,source.TransferBedCategoryID
		,source.ExpectedDischargeDateTime
		,source.TransferServicePointID
		,source.TransferDateTime
		,source.LatestPossibleTCIDateTime
		,source.ConfirmationStatusID
		,source.OperationDateTime
		,source.NilByMouthDateTime
		,source.IsAccompanied
		,source.IsLockedBed
		,source.ScheduleCancellationReasonID
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.UserCreate
		,source.UserModify
		,source.SessionTransactionID
		,source.PriorPointerID
		,source.ExternalKey
		,source.ServicePointBedCopyID
		,source.AdmissionOfferTypeID
		,source.BookingTypeID
		,source.InpatientOtherDateTime
		,source.ShortNoticeFlag
		,source.RTTStatusID
		,source.EarliestReasonableOfferDateTime
		,source.VerbalFlag
		,source.IsPatientUnavailable
		,source.ConfirmationDetailID
		,source.OutcomeDetailID
		,source.HealthOrganisationOwnerID
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.WaitingListID
		,target.DeferralReasonID
		,target.OfferOutcomeID
		,target.ServicePointBedID
		,target.AdmissionOfferDateTime
		,target.AdmissionOfferOutcomeDateTime
		,target.TCIDateTime
		,target.OfferCancellationDateTime
		,target.ConfirmationDateTime
		,target.Comments
		,target.IsDeferred
		,target.DeferralDateTime
		,target.ServicePointID
		,target.BedCategoryID
		,target.TransferBedCategoryID
		,target.ExpectedDischargeDateTime
		,target.TransferServicePointID
		,target.TransferDateTime
		,target.LatestPossibleTCIDateTime
		,target.ConfirmationStatusID
		,target.OperationDateTime
		,target.NilByMouthDateTime
		,target.IsAccompanied
		,target.IsLockedBed
		,target.ScheduleCancellationReasonID
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.SessionTransactionID
		,target.PriorPointerID
		,target.ExternalKey
		,target.ServicePointBedCopyID
		,target.AdmissionOfferTypeID
		,target.BookingTypeID
		,target.InpatientOtherDateTime
		,target.ShortNoticeFlag
		,target.RTTStatusID
		,target.EarliestReasonableOfferDateTime
		,target.VerbalFlag
		,target.IsPatientUnavailable
		,target.ConfirmationDetailID
		,target.OutcomeDetailID
		,target.HealthOrganisationOwnerID
		,target.LorenzoCreated
		)

then
	update
	set
		target.WaitingListID = source.WaitingListID
		,target.DeferralReasonID = source.DeferralReasonID
		,target.OfferOutcomeID = source.OfferOutcomeID
		,target.ServicePointBedID = source.ServicePointBedID
		,target.AdmissionOfferDateTime = source.AdmissionOfferDateTime
		,target.AdmissionOfferOutcomeDateTime = source.AdmissionOfferOutcomeDateTime
		,target.TCIDateTime = source.TCIDateTime
		,target.OfferCancellationDateTime = source.OfferCancellationDateTime
		,target.ConfirmationDateTime = source.ConfirmationDateTime
		,target.Comments = source.Comments
		,target.IsDeferred = source.IsDeferred
		,target.DeferralDateTime = source.DeferralDateTime
		,target.ServicePointID = source.ServicePointID
		,target.BedCategoryID = source.BedCategoryID
		,target.TransferBedCategoryID = source.TransferBedCategoryID
		,target.ExpectedDischargeDateTime = source.ExpectedDischargeDateTime
		,target.TransferServicePointID = source.TransferServicePointID
		,target.TransferDateTime = source.TransferDateTime
		,target.LatestPossibleTCIDateTime = source.LatestPossibleTCIDateTime
		,target.ConfirmationStatusID = source.ConfirmationStatusID
		,target.OperationDateTime = source.OperationDateTime
		,target.NilByMouthDateTime = source.NilByMouthDateTime
		,target.IsAccompanied = source.IsAccompanied
		,target.IsLockedBed = source.IsLockedBed
		,target.ScheduleCancellationReasonID = source.ScheduleCancellationReasonID
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.SessionTransactionID = source.SessionTransactionID
		,target.PriorPointerID = source.PriorPointerID
		,target.ExternalKey = source.ExternalKey
		,target.ServicePointBedCopyID = source.ServicePointBedCopyID
		,target.AdmissionOfferTypeID = source.AdmissionOfferTypeID
		,target.BookingTypeID = source.BookingTypeID
		,target.InpatientOtherDateTime = source.InpatientOtherDateTime
		,target.ShortNoticeFlag = source.ShortNoticeFlag
		,target.RTTStatusID = source.RTTStatusID
		,target.EarliestReasonableOfferDateTime = source.EarliestReasonableOfferDateTime
		,target.VerbalFlag = source.VerbalFlag
		,target.IsPatientUnavailable = source.IsPatientUnavailable
		,target.ConfirmationDetailID = source.ConfirmationDetailID
		,target.OutcomeDetailID = source.OutcomeDetailID
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime