﻿CREATE procedure [Lorenzo].[LoadAugmentedCarePeriod] as 


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(AugmentedCarePeriod.LorenzoCreated)
		from
			Lorenzo.AugmentedCarePeriod
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			ProviderSpellID
			,ProfessionalCareEpisodeID
			,SequenceNo
			,StartDateTime
			,EndDateTime
			,APCSourceID
			,ICUDays
			,HDUDays
			,APCLocationID
			,OrgansSupported
			,ProfessionalCarerID
			,SpecialtyID
			,IsPlanned
			,APCOutcomeID
			,APCDisposalID
			,UserCreate
			,UserModify
			,CreatedDateTime
			,ModifiedDateTime
			,SessionTransactionID
			,PriorPointerID
			,ExternalKey
			,AdmissionSourceID
			,AdmissionTypeID
			,DischargeDestinationID
			,DischargeLocationID
			,DischargeStatusID
			,AdmissionSourceLocationID
			,UnitBedFunctionID
			,UnitFunctionID
			,IsCriticalCarePeriod
			,ReadyDateTime
			,HealthOrganisationOwnerID
			,LorenzoCreated
			)
into
	#TLoadPASAugmentedCarePeriod
from
	(
	select
		AugmentedCarePeriodID
		,ProviderSpellID = cast(ProviderSpellID as int)
		,ProfessionalCareEpisodeID = cast(ProfessionalCareEpisodeID as int)
		,SequenceNo = cast(SequenceNo as int)
		,StartDateTime = cast(StartDateTime as smalldatetime)
		,EndDateTime = cast(EndDateTime as smalldatetime)
		,APCSourceID = cast(APCSourceID as int)
		,ICUDays = cast(ICUDays as int)
		,HDUDays = cast(HDUDays as int)
		,APCLocationID = cast(APCLocationID as int)
		,OrgansSupported = cast(nullif(OrgansSupported, '') as char(1))
		,ProfessionalCarerID = cast(ProfessionalCarerID as int)
		,SpecialtyID = cast(SpecialtyID as int)
		,IsPlanned = cast(nullif(IsPlanned, '') as char(1))
		,APCOutcomeID = cast(APCOutcomeID as int)
		,APCDisposalID = cast(APCDisposalID as int)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(30))
		,UserModify = cast(nullif(UserModify, '') as varchar(30))
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,SessionTransactionID = cast(SessionTransactionID as int)
		,PriorPointerID = cast(PriorPointerID as int)
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(20))
		,AdmissionSourceID = cast(AdmissionSourceID as int)
		,AdmissionTypeID = cast(AdmissionTypeID as int)
		,DischargeDestinationID = cast(DischargeDestinationID as int)
		,DischargeLocationID = cast(DischargeLocationID as int)
		,DischargeStatusID = cast(DischargeStatusID as int)
		,AdmissionSourceLocationID = cast(AdmissionSourceLocationID as int)
		,UnitBedFunctionID = cast(UnitBedFunctionID as int)
		,UnitFunctionID = cast(UnitFunctionID as int)
		,IsCriticalCarePeriod = cast(nullif(IsCriticalCarePeriod, '') as char(1))
		,ReadyDateTime = cast(ReadyDateTime as smalldatetime)
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as int)
		,LorenzoCreated = cast(LorenzoCreated as datetime)
		,IsDeleted
	from
		(
		select
			 AugmentedCarePeriodID = AUGCP_REFNO
			,ProviderSpellID = PRVSP_REFNO
			,ProfessionalCareEpisodeID = PRCAE_REFNO
			,SequenceNo = ACP_NUMBER
			,StartDateTime = START_DTTM
			,EndDateTime = END_DTTM
			,APCSourceID = APCSO_REFNO
			,ICUDays = ICU_DAYS
			,HDUDays = HDU_DAYS
			,APCLocationID = APCLO_REFNO
			,OrgansSupported = ORGANS_SUPPORTED
			,ProfessionalCarerID = PROCA_REFNO
			,SpecialtyID = SPECT_REFNO
			,IsPlanned = PLANNED_FLAG
			,APCOutcomeID = APCOC_REFNO
			,APCDisposalID = APCDI_REFNO
			,UserCreate = USER_CREATE
			,UserModify = USER_MODIF
			,CreatedDateTime = CREATE_DTTM
			,ModifiedDateTime = MODIF_DTTM
			,SessionTransactionID = STRAN_REFNO
			,PriorPointerID = PRIOR_POINTER
			,ExternalKey = EXTERNAL_KEY
			,AdmissionSourceID = CCASO_REFNO
			,AdmissionTypeID = CCATY_REFNO
			,DischargeDestinationID = CCDDE_REFNO
			,DischargeLocationID = CCDLO_REFNO
			,DischargeStatusID = CCDST_REFNO
			,AdmissionSourceLocationID = CCSLO_REFNO
			,UnitBedFunctionID = CCUBC_REFNO
			,UnitFunctionID = CCUFU_REFNO
			,IsCriticalCarePeriod = CCP_FLAG
			,ReadyDateTime = READY_DTTM
			,HealthOrganisationOwnerID = OWNER_HEORG_REFNO
			,LorenzoCreated = Created

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.AugmentedCarePeriod
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASAugmentedCarePeriod on #TLoadPASAugmentedCarePeriod
	(
	AugmentedCarePeriodID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.AugmentedCarePeriod target
using
	(
	select
		*
	from
		#TLoadPASAugmentedCarePeriod
	
	) source
	on	source.AugmentedCarePeriodID = target.AugmentedCarePeriodID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		AugmentedCarePeriodID
		,ProviderSpellID
		,ProfessionalCareEpisodeID
		,SequenceNo
		,StartDateTime
		,EndDateTime
		,APCSourceID
		,ICUDays
		,HDUDays
		,APCLocationID
		,OrgansSupported
		,ProfessionalCarerID
		,SpecialtyID
		,IsPlanned
		,APCOutcomeID
		,APCDisposalID
		,UserCreate
		,UserModify
		,CreatedDateTime
		,ModifiedDateTime
		,SessionTransactionID
		,PriorPointerID
		,ExternalKey
		,AdmissionSourceID
		,AdmissionTypeID
		,DischargeDestinationID
		,DischargeLocationID
		,DischargeStatusID
		,AdmissionSourceLocationID
		,UnitBedFunctionID
		,UnitFunctionID
		,IsCriticalCarePeriod
		,ReadyDateTime
		,HealthOrganisationOwnerID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		source.AugmentedCarePeriodID
		,source.ProviderSpellID
		,source.ProfessionalCareEpisodeID
		,source.SequenceNo
		,source.StartDateTime
		,source.EndDateTime
		,source.APCSourceID
		,source.ICUDays
		,source.HDUDays
		,source.APCLocationID
		,source.OrgansSupported
		,source.ProfessionalCarerID
		,source.SpecialtyID
		,source.IsPlanned
		,source.APCOutcomeID
		,source.APCDisposalID
		,source.UserCreate
		,source.UserModify
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.SessionTransactionID
		,source.PriorPointerID
		,source.ExternalKey
		,source.AdmissionSourceID
		,source.AdmissionTypeID
		,source.DischargeDestinationID
		,source.DischargeLocationID
		,source.DischargeStatusID
		,source.AdmissionSourceLocationID
		,source.UnitBedFunctionID
		,source.UnitFunctionID
		,source.IsCriticalCarePeriod
		,source.ReadyDateTime
		,source.HealthOrganisationOwnerID
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.ProviderSpellID
		,target.ProfessionalCareEpisodeID
		,target.SequenceNo
		,target.StartDateTime
		,target.EndDateTime
		,target.APCSourceID
		,target.ICUDays
		,target.HDUDays
		,target.APCLocationID
		,target.OrgansSupported
		,target.ProfessionalCarerID
		,target.SpecialtyID
		,target.IsPlanned
		,target.APCOutcomeID
		,target.APCDisposalID
		,target.UserCreate
		,target.UserModify
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.SessionTransactionID
		,target.PriorPointerID
		,target.ExternalKey
		,target.AdmissionSourceID
		,target.AdmissionTypeID
		,target.DischargeDestinationID
		,target.DischargeLocationID
		,target.DischargeStatusID
		,target.AdmissionSourceLocationID
		,target.UnitBedFunctionID
		,target.UnitFunctionID
		,target.IsCriticalCarePeriod
		,target.ReadyDateTime
		,target.HealthOrganisationOwnerID
		,target.LorenzoCreated
		)

then
	update
	set
		target.ProviderSpellID = source.ProviderSpellID
		,target.ProfessionalCareEpisodeID = source.ProfessionalCareEpisodeID
		,target.SequenceNo = source.SequenceNo
		,target.StartDateTime = source.StartDateTime
		,target.EndDateTime = source.EndDateTime
		,target.APCSourceID = source.APCSourceID
		,target.ICUDays = source.ICUDays
		,target.HDUDays = source.HDUDays
		,target.APCLocationID = source.APCLocationID
		,target.OrgansSupported = source.OrgansSupported
		,target.ProfessionalCarerID = source.ProfessionalCarerID
		,target.SpecialtyID = source.SpecialtyID
		,target.IsPlanned = source.IsPlanned
		,target.APCOutcomeID = source.APCOutcomeID
		,target.APCDisposalID = source.APCDisposalID
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.SessionTransactionID = source.SessionTransactionID
		,target.PriorPointerID = source.PriorPointerID
		,target.ExternalKey = source.ExternalKey
		,target.AdmissionSourceID = source.AdmissionSourceID
		,target.AdmissionTypeID = source.AdmissionTypeID
		,target.DischargeDestinationID = source.DischargeDestinationID
		,target.DischargeLocationID = source.DischargeLocationID
		,target.DischargeStatusID = source.DischargeStatusID
		,target.AdmissionSourceLocationID = source.AdmissionSourceLocationID
		,target.UnitBedFunctionID = source.UnitBedFunctionID
		,target.UnitFunctionID = source.UnitFunctionID
		,target.IsCriticalCarePeriod = source.IsCriticalCarePeriod
		,target.ReadyDateTime = source.ReadyDateTime
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime