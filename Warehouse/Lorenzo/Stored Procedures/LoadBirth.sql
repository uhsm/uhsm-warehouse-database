﻿CREATE procedure [Lorenzo].[LoadBirth] as 


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(Birth.LorenzoCreated)
		from
			Lorenzo.Birth
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			MaternitySpellID
			,PatientID
			,MotherID
			,ProfessionalCarerID
			,BirthOrder
			,DateOfBirth
			,SexID
			,DeliveryPersonStatusID
			,BirthStatusID
			,DeliveryPlaceID
			,ResuscitationMethodByPressureID
			,ResuscitationMethodByDrugID
			,BirthWeight
			,ApgarScoreAtOneMinute
			,ApgarScoreAtFiveMinutes
			,BCGAdministeredID
			,BabyHeadCircumference
			,BabyLength
			,ExaminationOfHipsID
			,FollowUpCareID
			,FoetalPresentationID
			,MetabolicScreeningID
			,JaundiceID
			,FeedingTypeID
			,DeliveryMethodID
			,MaternityDrugsID
			,GestationLengthID
			,ApgarScoreAtTenMinutes
			,SessionTransactionID
			,ReferralID
			,PriorPointerID
			,UserCreate
			,UserModify
			,CreatedDateTime
			,ModifiedDateTime
			,ExternalKey
			,NeonatalLevelOfCareID
			,HealthOrganisationOwnerID
			,LorenzoCreated
			)
into
	#TLoadPASBirth
from
	(
	select
		BirthID
		,MaternitySpellID = cast(MaternitySpellID as int)
		,PatientID = cast(PatientID as int)
		,MotherID = cast(MotherID as int)
		,ProfessionalCarerID = cast(ProfessionalCarerID as int)
		,BirthOrder = cast(BirthOrder as int)
		,DateOfBirth = cast(DateOfBirth as smalldatetime)
		,SexID = cast(SexID as int)
		,DeliveryPersonStatusID = cast(DeliveryPersonStatusID as int)
		,BirthStatusID = cast(BirthStatusID as int)
		,DeliveryPlaceID = cast(DeliveryPlaceID as int)
		,ResuscitationMethodByPressureID = cast(ResuscitationMethodByPressureID as int)
		,ResuscitationMethodByDrugID = cast(ResuscitationMethodByDrugID as int)
		,BirthWeight = cast(BirthWeight as int)
		,ApgarScoreAtOneMinute = cast(ApgarScoreAtOneMinute as int)
		,ApgarScoreAtFiveMinutes = cast(ApgarScoreAtFiveMinutes as int)
		,BCGAdministeredID = cast(BCGAdministeredID as int)
		,BabyHeadCircumference = cast(BabyHeadCircumference as int)
		,BabyLength = cast(BabyLength as int)
		,ExaminationOfHipsID = cast(ExaminationOfHipsID as int)
		,FollowUpCareID = cast(FollowUpCareID as int)
		,FoetalPresentationID = cast(FoetalPresentationID as int)
		,MetabolicScreeningID = cast(MetabolicScreeningID as int)
		,JaundiceID = cast(JaundiceID as int)
		,FeedingTypeID = cast(FeedingTypeID as int)
		,DeliveryMethodID = cast(DeliveryMethodID as int)
		,MaternityDrugsID = cast(MaternityDrugsID as int)
		,GestationLengthID = cast(GestationLengthID as int)
		,ApgarScoreAtTenMinutes = cast(ApgarScoreAtTenMinutes as int)
		,SessionTransactionID = cast(SessionTransactionID as int)
		,ReferralID = cast(ReferralID as int)
		,PriorPointerID = cast(PriorPointerID as int)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(30))
		,UserModify = cast(nullif(UserModify, '') as varchar(30))
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(20))
		,NeonatalLevelOfCareID = cast(NeonatalLevelOfCareID as int)
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as int)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			BirthID = RGBIR_REFNO
			,MaternitySpellID = MATSP_REFNO
			,PatientID = PATNT_REFNO
			,MotherID = MTHER_REFNO
			,ProfessionalCarerID = PROCA_REFNO
			,BirthOrder = BIRTH_ORDER
			,DateOfBirth = DTTM_OF_BIRTH
			,SexID = SEXXX_REFNO
			,DeliveryPersonStatusID = DPSTS_REFNO
			,BirthStatusID = BIRST_REFNO
			,DeliveryPlaceID = DEPLA_REFNO
			,ResuscitationMethodByPressureID = RESME_REFNO
			,ResuscitationMethodByDrugID = RESMD_REFNO
			,BirthWeight = BIRTH_WEIGHT
			,ApgarScoreAtOneMinute = APGAR_ONEMIN
			,ApgarScoreAtFiveMinutes = APGAR_FIVEMIN
			,BCGAdministeredID = BCGAD_REFNO
			,BabyHeadCircumference = CIRCM_HEAD
			,BabyLength = BIRTH_LENGTH
			,ExaminationOfHipsID = EXHIP_REFNO
			,FollowUpCareID = FOLUP_REFNO
			,FoetalPresentationID = FPRES_REFNO
			,MetabolicScreeningID = METSC_REFNO
			,JaundiceID = JAUND_REFNO
			,FeedingTypeID = FDTYP_REFNO
			,DeliveryMethodID = DELME_REFNO
			,MaternityDrugsID = MTDRG_REFNO
			,GestationLengthID = GESTN_LENGTH
			,ApgarScoreAtTenMinutes = APGAR_TENMIN
			,SessionTransactionID = STRAN_REFNO
			,ReferralID = REFRL_REFNO
			,PriorPointerID = PRIOR_POINTER
			,UserCreate = USER_CREATE
			,UserModify = USER_MODIF
			,CreatedDateTime = CREATE_DTTM
			,ModifiedDateTime = MODIF_DTTM
			,ExternalKey = EXTERNAL_KEY
			,NeonatalLevelOfCareID = NNLVL_REFNO
			,HealthOrganisationOwnerID = OWNER_HEORG_REFNO
			,LorenzoCreated = Created

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.Birth
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASBirth on #TLoadPASBirth
	(
	BirthID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.Birth target
using
	(
	select
		*
	from
		#TLoadPASBirth
	
	) source
	on	source.BirthID = target.BirthID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		BirthID
		,MaternitySpellID
		,PatientID
		,MotherID
		,ProfessionalCarerID
		,BirthOrder
		,DateOfBirth
		,SexID
		,DeliveryPersonStatusID
		,BirthStatusID
		,DeliveryPlaceID
		,ResuscitationMethodByPressureID
		,ResuscitationMethodByDrugID
		,BirthWeight
		,ApgarScoreAtOneMinute
		,ApgarScoreAtFiveMinutes
		,BCGAdministeredID
		,BabyHeadCircumference
		,BabyLength
		,ExaminationOfHipsID
		,FollowUpCareID
		,FoetalPresentationID
		,MetabolicScreeningID
		,JaundiceID
		,FeedingTypeID
		,DeliveryMethodID
		,MaternityDrugsID
		,GestationLengthID
		,ApgarScoreAtTenMinutes
		,SessionTransactionID
		,ReferralID
		,PriorPointerID
		,UserCreate
		,UserModify
		,CreatedDateTime
		,ModifiedDateTime
		,ExternalKey
		,NeonatalLevelOfCareID
		,HealthOrganisationOwnerID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		source.BirthID
		,source.MaternitySpellID
		,source.PatientID
		,source.MotherID
		,source.ProfessionalCarerID
		,source.BirthOrder
		,source.DateOfBirth
		,source.SexID
		,source.DeliveryPersonStatusID
		,source.BirthStatusID
		,source.DeliveryPlaceID
		,source.ResuscitationMethodByPressureID
		,source.ResuscitationMethodByDrugID
		,source.BirthWeight
		,source.ApgarScoreAtOneMinute
		,source.ApgarScoreAtFiveMinutes
		,source.BCGAdministeredID
		,source.BabyHeadCircumference
		,source.BabyLength
		,source.ExaminationOfHipsID
		,source.FollowUpCareID
		,source.FoetalPresentationID
		,source.MetabolicScreeningID
		,source.JaundiceID
		,source.FeedingTypeID
		,source.DeliveryMethodID
		,source.MaternityDrugsID
		,source.GestationLengthID
		,source.ApgarScoreAtTenMinutes
		,source.SessionTransactionID
		,source.ReferralID
		,source.PriorPointerID
		,source.UserCreate
		,source.UserModify
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.ExternalKey
		,source.NeonatalLevelOfCareID
		,source.HealthOrganisationOwnerID
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.MaternitySpellID
		,target.PatientID
		,target.MotherID
		,target.ProfessionalCarerID
		,target.BirthOrder
		,target.DateOfBirth
		,target.SexID
		,target.DeliveryPersonStatusID
		,target.BirthStatusID
		,target.DeliveryPlaceID
		,target.ResuscitationMethodByPressureID
		,target.ResuscitationMethodByDrugID
		,target.BirthWeight
		,target.ApgarScoreAtOneMinute
		,target.ApgarScoreAtFiveMinutes
		,target.BCGAdministeredID
		,target.BabyHeadCircumference
		,target.BabyLength
		,target.ExaminationOfHipsID
		,target.FollowUpCareID
		,target.FoetalPresentationID
		,target.MetabolicScreeningID
		,target.JaundiceID
		,target.FeedingTypeID
		,target.DeliveryMethodID
		,target.MaternityDrugsID
		,target.GestationLengthID
		,target.ApgarScoreAtTenMinutes
		,target.SessionTransactionID
		,target.ReferralID
		,target.PriorPointerID
		,target.UserCreate
		,target.UserModify
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.ExternalKey
		,target.NeonatalLevelOfCareID
		,target.HealthOrganisationOwnerID
		,target.LorenzoCreated
		)

then
	update
	set
		target.MaternitySpellID = source.MaternitySpellID
		,target.PatientID = source.PatientID
		,target.MotherID = source.MotherID
		,target.ProfessionalCarerID = source.ProfessionalCarerID
		,target.BirthOrder = source.BirthOrder
		,target.DateOfBirth = source.DateOfBirth
		,target.SexID = source.SexID
		,target.DeliveryPersonStatusID = source.DeliveryPersonStatusID
		,target.BirthStatusID = source.BirthStatusID
		,target.DeliveryPlaceID = source.DeliveryPlaceID
		,target.ResuscitationMethodByPressureID = source.ResuscitationMethodByPressureID
		,target.ResuscitationMethodByDrugID = source.ResuscitationMethodByDrugID
		,target.BirthWeight = source.BirthWeight
		,target.ApgarScoreAtOneMinute = source.ApgarScoreAtOneMinute
		,target.ApgarScoreAtFiveMinutes = source.ApgarScoreAtFiveMinutes
		,target.BCGAdministeredID = source.BCGAdministeredID
		,target.BabyHeadCircumference = source.BabyHeadCircumference
		,target.BabyLength = source.BabyLength
		,target.ExaminationOfHipsID = source.ExaminationOfHipsID
		,target.FollowUpCareID = source.FollowUpCareID
		,target.FoetalPresentationID = source.FoetalPresentationID
		,target.MetabolicScreeningID = source.MetabolicScreeningID
		,target.JaundiceID = source.JaundiceID
		,target.FeedingTypeID = source.FeedingTypeID
		,target.DeliveryMethodID = source.DeliveryMethodID
		,target.MaternityDrugsID = source.MaternityDrugsID
		,target.GestationLengthID = source.GestationLengthID
		,target.ApgarScoreAtTenMinutes = source.ApgarScoreAtTenMinutes
		,target.SessionTransactionID = source.SessionTransactionID
		,target.ReferralID = source.ReferralID
		,target.PriorPointerID = source.PriorPointerID
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.ExternalKey = source.ExternalKey
		,target.NeonatalLevelOfCareID = source.NeonatalLevelOfCareID
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime