﻿CREATE procedure [Lorenzo].[LoadClinicalCoding] as 


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(ClinicalCoding.LorenzoCreated)
		from
			Lorenzo.ClinicalCoding
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			ClinicalCodingClassificationID
			,PatientID
			,MedicalProblemLevelID
			,CarriedOutOrProposedID
			,OtherClinicalCodeID
			,LinkClinicalCodingID
			,Comments
			,ClinicalCoding
			,SourceID
			,SourceCode
			,ClinicalCodingDateTime
			,ClinicalCodingSubtypeCode
			,CreatedDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,ClinicalCodingCode
			,ClinicalCodingInternalCode
			,SupplementaryCode
			,SupplementaryInternalCode
			,SessionTransactionID
			,PriorPointerID
			,ExternalKey
			,StartDateTime
			,EndDateTime
			,UpdatedDateTime
			,StaffTeamID
			,ProfessionalCarerID
			,DurationMinutes
			,IsConfidential
			,Dosage
			,DosageUnitID
			,LocationOnBodyID
			,Frequency
			,FrequencyUnitID
			,CancelDateTime
			,ParentClinicalCodingID
			,RulesID
			,Value
			,IsProblem
			,LateralityID
			,ProfessionalCareEpisodeID
			,PeriodAdministeredID
			,ReasonAdministeredID
			,AnaestheticAnalgesicCategoryID
			,IsCauseOfDeath
			,ClinicalCodingOrder
			,CodingAuthorisationFlag
			,LatchedAuthorisationFlag
			,ActionToTake
			,AlertSeverityID
			,IsContractedProcedure
			,SpecialtyID
			,IsHospitalService
			,MinimumPrice
			,MaximumPrice
			,ClinicalCodingEndDateTime
			,BillODPCDID
			,ProcedureLocationID
			,TheatreBodyRegionID
			,IndividualCodingStatusID
			,SynchronisationCode
			,AnaestheticTypeID
			,CodingOnsetTypeID
			,VisitPurposeID
			,HealthOrganisationOwnerID
			,LorenzoCreated
			)
into
	#TLoadPASClinicalCoding
from
	(
	select
		ClinicalCodingID
		,ClinicalCodingClassificationID = cast(ClinicalCodingClassificationID as int)
		,PatientID = cast(PatientID as int)
		,MedicalProblemLevelID = cast(MedicalProblemLevelID as int)
		,CarriedOutOrProposedID = cast(CarriedOutOrProposedID as int)
		,OtherClinicalCodeID = cast(OtherClinicalCodeID as int)
		,LinkClinicalCodingID = cast(LinkClinicalCodingID as int)
		,Comments = cast(nullif(Comments, '') as varchar(255))
		,ClinicalCoding = cast(nullif(ClinicalCoding, '') as varchar(255))
		,SourceID = cast(SourceID as int)
		,SourceCode = cast(nullif(SourceCode, '') as varchar(5))
		,ClinicalCodingDateTime = cast(ClinicalCodingDateTime as smalldatetime)
		,ClinicalCodingSubtypeCode = cast(nullif(ClinicalCodingSubtypeCode, '') as varchar(5))
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(30))
		,UserModify = cast(nullif(UserModify, '') as varchar(30))
		,ClinicalCodingCode = cast(nullif(ClinicalCodingCode, '') as varchar(20))
		,ClinicalCodingInternalCode = cast(nullif(ClinicalCodingInternalCode, '') as varchar(25))
		,SupplementaryCode = cast(nullif(SupplementaryCode, '') as varchar(20))
		,SupplementaryInternalCode = cast(nullif(SupplementaryInternalCode, '') as varchar(5))
		,SessionTransactionID = cast(SessionTransactionID as int)
		,PriorPointerID = cast(PriorPointerID as int)
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(20))
		,StartDateTime = cast(StartDateTime as smalldatetime)
		,EndDateTime = cast(EndDateTime as smalldatetime)
		,UpdatedDateTime = cast(UpdatedDateTime as smalldatetime)
		,StaffTeamID = cast(StaffTeamID as int)
		,ProfessionalCarerID = cast(ProfessionalCarerID as int)
		,DurationMinutes = cast(DurationMinutes as int)
		,IsConfidential = cast(nullif(IsConfidential, '') as char(1))
		,Dosage = cast(Dosage as int)
		,DosageUnitID = cast(DosageUnitID as int)
		,LocationOnBodyID = cast(LocationOnBodyID as int)
		,Frequency = cast(Frequency as int)
		,FrequencyUnitID = cast(FrequencyUnitID as int)
		,CancelDateTime = cast(CancelDateTime as smalldatetime)
		,ParentClinicalCodingID = cast(ParentClinicalCodingID as int)
		,RulesID = cast(RulesID as int)
		,Value = cast(nullif(Value, '') as varchar(255))
		,IsProblem = cast(nullif(IsProblem, '') as char(1))
		,LateralityID = cast(LateralityID as int)
		,ProfessionalCareEpisodeID = cast(ProfessionalCareEpisodeID as int)
		,PeriodAdministeredID = cast(PeriodAdministeredID as int)
		,ReasonAdministeredID = cast(ReasonAdministeredID as int)
		,AnaestheticAnalgesicCategoryID = cast(AnaestheticAnalgesicCategoryID as int)
		,IsCauseOfDeath = cast(nullif(IsCauseOfDeath, '') as char(1))
		,ClinicalCodingOrder = cast(ClinicalCodingOrder as int)
		,CodingAuthorisationFlag = cast(nullif(CodingAuthorisationFlag, '') as char(1))
		,LatchedAuthorisationFlag = cast(nullif(LatchedAuthorisationFlag, '') as varchar(1))
		,ActionToTake = cast(nullif(ActionToTake, '') as varchar(255))
		,AlertSeverityID = cast(AlertSeverityID as int)
		,IsContractedProcedure = cast(nullif(IsContractedProcedure, '') as varchar(1))
		,SpecialtyID = cast(SpecialtyID as int)
		,IsHospitalService = cast(nullif(IsHospitalService, '') as varchar(1))
		,MinimumPrice = cast(MinimumPrice as int)
		,MaximumPrice = cast(MaximumPrice as int)
		,ClinicalCodingEndDateTime = cast(ClinicalCodingEndDateTime as smalldatetime)
		,BillODPCDID = cast(BillODPCDID as int)
		,ProcedureLocationID = cast(ProcedureLocationID as int)
		,TheatreBodyRegionID = cast(TheatreBodyRegionID as int)
		,IndividualCodingStatusID = cast(IndividualCodingStatusID as int)
		,SynchronisationCode = cast(nullif(SynchronisationCode, '') as varchar(20))
		,AnaestheticTypeID = cast(AnaestheticTypeID as int)
		,CodingOnsetTypeID = cast(CodingOnsetTypeID as int)
		,VisitPurposeID = cast(VisitPurposeID as int)
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as int)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			ClinicalCodingID = DGPRO_REFNO
			,ClinicalCodingClassificationID = DPCLA_REFNO
			,PatientID = PATNT_REFNO
			,MedicalProblemLevelID = MPLEV_REFNO
			,CarriedOutOrProposedID = CPTYP_REFNO
			,OtherClinicalCodeID = ODPCD_REFNO
			,LinkClinicalCodingID = LINK_DGPRO_REFNO
			,Comments = COMMENTS
			,ClinicalCoding = DESCRIPTION
			,SourceID = SORCE_REFNO
			,SourceCode = SORCE_CODE
			,ClinicalCodingDateTime = DGPRO_DTTM
			,ClinicalCodingSubtypeCode = DPTYP_CODE
			,CreatedDateTime = CREATE_DTTM
			,ModifiedDateTime = MODIF_DTTM
			,UserCreate = USER_CREATE
			,UserModify = USER_MODIF
			,ClinicalCodingCode = CODE
			,ClinicalCodingInternalCode = CCSXT_CODE
			,SupplementaryCode = SUPL_CODE
			,SupplementaryInternalCode = SUPL_CCSXT_CODE
			,SessionTransactionID = STRAN_REFNO
			,PriorPointerID = PRIOR_POINTER
			,ExternalKey = EXTERNAL_KEY
			,StartDateTime = START_DTTM
			,EndDateTime = END_DTTM
			,UpdatedDateTime = UPDATE_DTTM
			,StaffTeamID = STEAM_REFNO
			,ProfessionalCarerID = PROCA_REFNO
			,DurationMinutes = DURATION
			,IsConfidential = CONFIDENTIAL
			,Dosage = DOSAGE
			,DosageUnitID = DOUNT_REFNO
			,LocationOnBodyID = DPLOC_REFNO
			,Frequency = FREQUENCY
			,FrequencyUnitID = FRUNT_REFNO
			,CancelDateTime = CANCEL_DTTM
			,ParentClinicalCodingID = PARNT_REFNO
			,RulesID = RULES_REFNO
			,Value = VALUE
			,IsProblem = PROBLEM
			,LateralityID = LATRL_REFNO
			,ProfessionalCareEpisodeID = PRCAE_REFNO
			,PeriodAdministeredID = PERAD_REFNO
			,ReasonAdministeredID = RSADM_REFNO
			,AnaestheticAnalgesicCategoryID = ANALC_REFNO
			,IsCauseOfDeath = CAUSE_OF_DEATH
			,ClinicalCodingOrder = SORT_ORDER
			,CodingAuthorisationFlag = COD_AUTH_FLAG
			,LatchedAuthorisationFlag = LATCH_AUTH_FLAG
			,ActionToTake = ACTION
			,AlertSeverityID = ALSEV_REFNO
			,IsContractedProcedure = CONTRACTED_PROCEDURE
			,SpecialtyID = SPECT_REFNO
			,IsHospitalService = HOSP_SERV_FLAG
			,MinimumPrice = MIN_PRICE
			,MaximumPrice = MAX_PRICE
			,ClinicalCodingEndDateTime = DGPRO_END_DTTM
			,BillODPCDID = BILL_ODPCD_REFNO
			,ProcedureLocationID = PLCTN_REFNO
			,TheatreBodyRegionID = THEBR_REFNO
			,IndividualCodingStatusID = ICDST_REFNO
			,SynchronisationCode = SYN_CODE
			,AnaestheticTypeID = ANTYP_REFNO
			,CodingOnsetTypeID = CODON_REFNO
			,VisitPurposeID = VISTP_REFNO
			,HealthOrganisationOwnerID = OWNER_HEORG_REFNO
			,LorenzoCreated = Created

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.ClinicalCoding
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASClinicalCoding on #TLoadPASClinicalCoding
	(
	ClinicalCodingID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.ClinicalCoding target
using
	(
	select
		*
	from
		#TLoadPASClinicalCoding
	
	) source
	on	source.ClinicalCodingID = target.ClinicalCodingID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		ClinicalCodingID
		,ClinicalCodingClassificationID
		,PatientID
		,MedicalProblemLevelID
		,CarriedOutOrProposedID
		,OtherClinicalCodeID
		,LinkClinicalCodingID
		,Comments
		,ClinicalCoding
		,SourceID
		,SourceCode
		,ClinicalCodingDateTime
		,ClinicalCodingSubtypeCode
		,CreatedDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,ClinicalCodingCode
		,ClinicalCodingInternalCode
		,SupplementaryCode
		,SupplementaryInternalCode
		,SessionTransactionID
		,PriorPointerID
		,ExternalKey
		,StartDateTime
		,EndDateTime
		,UpdatedDateTime
		,StaffTeamID
		,ProfessionalCarerID
		,DurationMinutes
		,IsConfidential
		,Dosage
		,DosageUnitID
		,LocationOnBodyID
		,Frequency
		,FrequencyUnitID
		,CancelDateTime
		,ParentClinicalCodingID
		,RulesID
		,Value
		,IsProblem
		,LateralityID
		,ProfessionalCareEpisodeID
		,PeriodAdministeredID
		,ReasonAdministeredID
		,AnaestheticAnalgesicCategoryID
		,IsCauseOfDeath
		,ClinicalCodingOrder
		,CodingAuthorisationFlag
		,LatchedAuthorisationFlag
		,ActionToTake
		,AlertSeverityID
		,IsContractedProcedure
		,SpecialtyID
		,IsHospitalService
		,MinimumPrice
		,MaximumPrice
		,ClinicalCodingEndDateTime
		,BillODPCDID
		,ProcedureLocationID
		,TheatreBodyRegionID
		,IndividualCodingStatusID
		,SynchronisationCode
		,AnaestheticTypeID
		,CodingOnsetTypeID
		,VisitPurposeID
		,HealthOrganisationOwnerID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
	source.ClinicalCodingID
	,source.ClinicalCodingClassificationID
	,source.PatientID
	,source.MedicalProblemLevelID
	,source.CarriedOutOrProposedID
	,source.OtherClinicalCodeID
	,source.LinkClinicalCodingID
	,source.Comments
	,source.ClinicalCoding
	,source.SourceID
	,source.SourceCode
	,source.ClinicalCodingDateTime
	,source.ClinicalCodingSubtypeCode
	,source.CreatedDateTime
	,source.ModifiedDateTime
	,source.UserCreate
	,source.UserModify
	,source.ClinicalCodingCode
	,source.ClinicalCodingInternalCode
	,source.SupplementaryCode
	,source.SupplementaryInternalCode
	,source.SessionTransactionID
	,source.PriorPointerID
	,source.ExternalKey
	,source.StartDateTime
	,source.EndDateTime
	,source.UpdatedDateTime
	,source.StaffTeamID
	,source.ProfessionalCarerID
	,source.DurationMinutes
	,source.IsConfidential
	,source.Dosage
	,source.DosageUnitID
	,source.LocationOnBodyID
	,source.Frequency
	,source.FrequencyUnitID
	,source.CancelDateTime
	,source.ParentClinicalCodingID
	,source.RulesID
	,source.Value
	,source.IsProblem
	,source.LateralityID
	,source.ProfessionalCareEpisodeID
	,source.PeriodAdministeredID
	,source.ReasonAdministeredID
	,source.AnaestheticAnalgesicCategoryID
	,source.IsCauseOfDeath
	,source.ClinicalCodingOrder
	,source.CodingAuthorisationFlag
	,source.LatchedAuthorisationFlag
	,source.ActionToTake
	,source.AlertSeverityID
	,source.IsContractedProcedure
	,source.SpecialtyID
	,source.IsHospitalService
	,source.MinimumPrice
	,source.MaximumPrice
	,source.ClinicalCodingEndDateTime
	,source.BillODPCDID
	,source.ProcedureLocationID
	,source.TheatreBodyRegionID
	,source.IndividualCodingStatusID
	,source.SynchronisationCode
	,source.AnaestheticTypeID
	,source.CodingOnsetTypeID
	,source.VisitPurposeID
	,source.HealthOrganisationOwnerID
	,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.ClinicalCodingClassificationID
		,target.PatientID
		,target.MedicalProblemLevelID
		,target.CarriedOutOrProposedID
		,target.OtherClinicalCodeID
		,target.LinkClinicalCodingID
		,target.Comments
		,target.ClinicalCoding
		,target.SourceID
		,target.SourceCode
		,target.ClinicalCodingDateTime
		,target.ClinicalCodingSubtypeCode
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.ClinicalCodingCode
		,target.ClinicalCodingInternalCode
		,target.SupplementaryCode
		,target.SupplementaryInternalCode
		,target.SessionTransactionID
		,target.PriorPointerID
		,target.ExternalKey
		,target.StartDateTime
		,target.EndDateTime
		,target.UpdatedDateTime
		,target.StaffTeamID
		,target.ProfessionalCarerID
		,target.DurationMinutes
		,target.IsConfidential
		,target.Dosage
		,target.DosageUnitID
		,target.LocationOnBodyID
		,target.Frequency
		,target.FrequencyUnitID
		,target.CancelDateTime
		,target.ParentClinicalCodingID
		,target.RulesID
		,target.Value
		,target.IsProblem
		,target.LateralityID
		,target.ProfessionalCareEpisodeID
		,target.PeriodAdministeredID
		,target.ReasonAdministeredID
		,target.AnaestheticAnalgesicCategoryID
		,target.IsCauseOfDeath
		,target.ClinicalCodingOrder
		,target.CodingAuthorisationFlag
		,target.LatchedAuthorisationFlag
		,target.ActionToTake
		,target.AlertSeverityID
		,target.IsContractedProcedure
		,target.SpecialtyID
		,target.IsHospitalService
		,target.MinimumPrice
		,target.MaximumPrice
		,target.ClinicalCodingEndDateTime
		,target.BillODPCDID
		,target.ProcedureLocationID
		,target.TheatreBodyRegionID
		,target.IndividualCodingStatusID
		,target.SynchronisationCode
		,target.AnaestheticTypeID
		,target.CodingOnsetTypeID
		,target.VisitPurposeID
		,target.HealthOrganisationOwnerID
		,target.LorenzoCreated
		)

then
	update
	set
		target.ClinicalCodingClassificationID = source.ClinicalCodingClassificationID
		,target.PatientID = source.PatientID
		,target.MedicalProblemLevelID = source.MedicalProblemLevelID
		,target.CarriedOutOrProposedID = source.CarriedOutOrProposedID
		,target.OtherClinicalCodeID = source.OtherClinicalCodeID
		,target.LinkClinicalCodingID = source.LinkClinicalCodingID
		,target.Comments = source.Comments
		,target.ClinicalCoding = source.ClinicalCoding
		,target.SourceID = source.SourceID
		,target.SourceCode = source.SourceCode
		,target.ClinicalCodingDateTime = source.ClinicalCodingDateTime
		,target.ClinicalCodingSubtypeCode = source.ClinicalCodingSubtypeCode
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.ClinicalCodingCode = source.ClinicalCodingCode
		,target.ClinicalCodingInternalCode = source.ClinicalCodingInternalCode
		,target.SupplementaryCode = source.SupplementaryCode
		,target.SupplementaryInternalCode = source.SupplementaryInternalCode
		,target.SessionTransactionID = source.SessionTransactionID
		,target.PriorPointerID = source.PriorPointerID
		,target.ExternalKey = source.ExternalKey
		,target.StartDateTime = source.StartDateTime
		,target.EndDateTime = source.EndDateTime
		,target.UpdatedDateTime = source.UpdatedDateTime
		,target.StaffTeamID = source.StaffTeamID
		,target.ProfessionalCarerID = source.ProfessionalCarerID
		,target.DurationMinutes = source.DurationMinutes
		,target.IsConfidential = source.IsConfidential
		,target.Dosage = source.Dosage
		,target.DosageUnitID = source.DosageUnitID
		,target.LocationOnBodyID = source.LocationOnBodyID
		,target.Frequency = source.Frequency
		,target.FrequencyUnitID = source.FrequencyUnitID
		,target.CancelDateTime = source.CancelDateTime
		,target.ParentClinicalCodingID = source.ParentClinicalCodingID
		,target.RulesID = source.RulesID
		,target.Value = source.Value
		,target.IsProblem = source.IsProblem
		,target.LateralityID = source.LateralityID
		,target.ProfessionalCareEpisodeID = source.ProfessionalCareEpisodeID
		,target.PeriodAdministeredID = source.PeriodAdministeredID
		,target.ReasonAdministeredID = source.ReasonAdministeredID
		,target.AnaestheticAnalgesicCategoryID = source.AnaestheticAnalgesicCategoryID
		,target.IsCauseOfDeath = source.IsCauseOfDeath
		,target.ClinicalCodingOrder = source.ClinicalCodingOrder
		,target.CodingAuthorisationFlag = source.CodingAuthorisationFlag
		,target.LatchedAuthorisationFlag = source.LatchedAuthorisationFlag
		,target.ActionToTake = source.ActionToTake
		,target.AlertSeverityID = source.AlertSeverityID
		,target.IsContractedProcedure = source.IsContractedProcedure
		,target.SpecialtyID = source.SpecialtyID
		,target.IsHospitalService = source.IsHospitalService
		,target.MinimumPrice = source.MinimumPrice
		,target.MaximumPrice = source.MaximumPrice
		,target.ClinicalCodingEndDateTime = source.ClinicalCodingEndDateTime
		,target.BillODPCDID = source.BillODPCDID
		,target.ProcedureLocationID = source.ProcedureLocationID
		,target.TheatreBodyRegionID = source.TheatreBodyRegionID
		,target.IndividualCodingStatusID = source.IndividualCodingStatusID
		,target.SynchronisationCode = source.SynchronisationCode
		,target.AnaestheticTypeID = source.AnaestheticTypeID
		,target.CodingOnsetTypeID = source.CodingOnsetTypeID
		,target.VisitPurposeID = source.VisitPurposeID
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime