﻿CREATE procedure [Lorenzo].[LoadContract] as 


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(Contract.LorenzoCreated)
		from
			Lorenzo.Contract
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			PurchaserID
			,SerialNumber
			,Identifier
			,Contract
			,StartDateTime
			,EndDateTime
			,ContractTypeID
			,ContractTypeMainCode
			,ContractType
			,ContractStatusID
			,ContractStatusMainCode
			,ContractStatus
			,UserCreate
			,UserModify
			,CreatedDateTime
			,ModifiedDateTime
			,LorenzoCreated
			)
into
	#TLoadPASContract
from
	(
	select
		ContractID
		,PurchaserID = cast(PurchaserID as int)
		,SerialNumber = cast(nullif(SerialNumber, '') as varchar(20))
		,Identifier = cast(nullif(Identifier, '') as varchar(60))
		,Contract = cast(nullif(Contract, '') as varchar(80))
		,StartDateTime = cast(StartDateTime as smalldatetime)
		,EndDateTime = cast(EndDateTime as smalldatetime)
		,ContractTypeID = cast(ContractTypeID as int)
		,ContractTypeMainCode = cast(nullif(ContractTypeMainCode, '') as varchar(25))
		,ContractType = cast(nullif(ContractType, '') as varchar(80))
		,ContractStatusID = cast(ContractStatusID as int)
		,ContractStatusMainCode = cast(nullif(ContractStatusMainCode, '') as varchar(25))
		,ContractStatus = cast(nullif(ContractStatus, '') as varchar(80))
		,UserCreate = cast(nullif(UserCreate, '') as varchar(30))
		,UserModify = cast(nullif(UserModify, '') as varchar(30))
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			ContractID = CONTR_REFNO
			,PurchaserID = PURCH_REFNO
			,SerialNumber = SERIAL_NUMBER
			,Identifier = IDENTIFIER
			,Contract = DESCRIPTION
			,StartDateTime = START_DTTM
			,EndDateTime = END_DTTM
			,ContractTypeID = CTTYP_REFNO
			,ContractTypeMainCode = CTTYP_REFNO_MAIN_CODE
			,ContractType = CTTYP_REFNO_DESCRIPTION
			,ContractStatusID = CTSTS_REFNO
			,ContractStatusMainCode = CTSTS_REFNO_MAIN_CODE
			,ContractStatus = CTSTS_REFNO_DESCRIPTION
			,UserCreate = USER_CREATE
			,UserModify = USER_MODIF
			,CreatedDateTime = CREATE_DTTM
			,ModifiedDateTime = MODIF_DTTM
			,LorenzoCreated = Created

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.Contract
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASContract on #TLoadPASContract
	(
	ContractID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.Contract target
using
	(
	select
		*
	from
		#TLoadPASContract
	
	) source
	on	source.ContractID = target.ContractID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		ContractID
		,PurchaserID
		,SerialNumber
		,Identifier
		,Contract
		,StartDateTime
		,EndDateTime
		,ContractTypeID
		,ContractTypeMainCode
		,ContractType
		,ContractStatusID
		,ContractStatusMainCode
		,ContractStatus
		,UserCreate
		,UserModify
		,CreatedDateTime
		,ModifiedDateTime
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		source.ContractID
		,source.PurchaserID
		,source.SerialNumber
		,source.Identifier
		,source.Contract
		,source.StartDateTime
		,source.EndDateTime
		,source.ContractTypeID
		,source.ContractTypeMainCode
		,source.ContractType
		,source.ContractStatusID
		,source.ContractStatusMainCode
		,source.ContractStatus
		,source.UserCreate
		,source.UserModify
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.PurchaserID
		,target.SerialNumber
		,target.Identifier
		,target.Contract
		,target.StartDateTime
		,target.EndDateTime
		,target.ContractTypeID
		,target.ContractTypeMainCode
		,target.ContractType
		,target.ContractStatusID
		,target.ContractStatusMainCode
		,target.ContractStatus
		,target.UserCreate
		,target.UserModify
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.LorenzoCreated
		)

then
	update
	set
		target.PurchaserID = source.PurchaserID
		,target.SerialNumber = source.SerialNumber
		,target.Identifier = source.Identifier
		,target.Contract = source.Contract
		,target.StartDateTime = source.StartDateTime
		,target.EndDateTime = source.EndDateTime
		,target.ContractTypeID = source.ContractTypeID
		,target.ContractTypeMainCode = source.ContractTypeMainCode
		,target.ContractType = source.ContractType
		,target.ContractStatusID = source.ContractStatusID
		,target.ContractStatusMainCode = source.ContractStatusMainCode
		,target.ContractStatus = source.ContractStatus
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime