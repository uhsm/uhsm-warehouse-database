﻿CREATE PROCEDURE [Lorenzo].[LoadDependantRule]

AS


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(DependantRule.LorenzoCreated)
		from
			Lorenzo.DependantRule
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 DependantRuleID
			,RuleID
			,SourceID
			,SourceCode
			,IsEnforced
			,RuleOperator
			,RuleValue
			,CreatedDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,SessionTransactionID
			,PriorPointerID
			,ExternalKey
			,Serial
			,Parenthesis
			,LogicalOperator
			,AssociatedValue
			,HealthOrganisationOwnerID
			,LorenzoCreated
			)
into
	#TLoadPASDependantRule
from
	(
	select
		DependantRuleID
		,RuleID = cast(RuleID as int)
		,SourceID = cast(SourceID as numeric)
		,SourceCode = cast(nullif(SourceCode, '') as varchar(5))
		,IsEnforced = cast(nullif(IsEnforced, '') as char(1))
		,RuleOperator = cast(nullif(RuleOperator, '') as varchar(10))
		,RuleValue = cast(nullif(RuleValue, '') as varchar(20))
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(30))
		,UserModify = cast(nullif(UserModify, '') as varchar(30))
		,SessionTransactionID = cast(SessionTransactionID as numeric)
		,PriorPointerID = cast(PriorPointerID as int)
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(20))
		,Serial = cast(Serial as numeric)
		,Parenthesis = cast(nullif(Parenthesis, '') as varchar(30))
		,LogicalOperator = cast(nullif(LogicalOperator, '') as char(1))
		,AssociatedValue = cast(nullif(AssociatedValue, '') as varchar(20))
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as int)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			DependantRuleID = [DPRUL_REFNO]
			  ,RuleID = [RULES_REFNO]
			  ,SourceID = [SORCE_REFNO]
			  ,SourceCode = [SORCE_CODE]
			  ,IsEnforced = [ENFORCE_FLAG]
			  ,RuleOperator = [OPERATOR]
			  ,RuleValue = [VALUE]
			  ,CreatedDateTime = [CREATE_DTTM]
			  ,ModifiedDateTime = [MODIF_DTTM]
			  ,UserCreate = [USER_CREATE]
			  ,UserModify = [USER_MODIF]
			  ,SessionTransactionID = [STRAN_REFNO]
			  ,PriorPointerID = [PRIOR_POINTER]
			  ,ExternalKey = [EXTERNAL_KEY]
			  ,Serial = [SERIAL]
			  ,Parenthesis = [OPEN_CLOSE_PARENTHESIS]
			  ,LogicalOperator = [LOGICAL_OPERATOR]
			  ,AssociatedValue = [ASSOC_VALUE]
			  ,HealthOrganisationOwnerID = [OWNER_HEORG_REFNO]
			  ,LorenzoCreated = [Created]

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.ContractRule
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASDependantRule on #TLoadPASDependantRule
	(
	DependantRuleID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.DependantRule target
using
	(
	select
		*
	from
		#TLoadPASDependantRule
	
	) source
	on	source.DependantRuleID = target.DependantRuleID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		  DependantRuleID
		,RuleID
		,SourceID
		,SourceCode
		,IsEnforced
		,RuleOperator
		,RuleValue
		,CreatedDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,SessionTransactionID
		,PriorPointerID
		,ExternalKey
		,Serial
		,Parenthesis
		,LogicalOperator
		,AssociatedValue
		,HealthOrganisationOwnerID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		  source.DependantRuleID
		,source.RuleID
		,source.SourceID
		,source.SourceCode
		,source.IsEnforced
		,source.RuleOperator
		,source.RuleValue
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.UserCreate
		,source.UserModify
		,source.SessionTransactionID
		,source.PriorPointerID
		,source.ExternalKey
		,source.Serial
		,source.Parenthesis
		,source.LogicalOperator
		,source.AssociatedValue
		,source.HealthOrganisationOwnerID
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		 target.DependantRuleID
		,target.RuleID
		,target.SourceID
		,target.SourceCode
		,target.IsEnforced
		,target.RuleOperator
		,target.RuleValue
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.SessionTransactionID
		,target.PriorPointerID
		,target.ExternalKey
		,target.Serial
		,target.Parenthesis
		,target.LogicalOperator
		,target.AssociatedValue
		,target.HealthOrganisationOwnerID
		,target.LorenzoCreated
		)

then
	update
	set
		 target.DependantRuleID = source.DependantRuleID
		,target.RuleID = source.RuleID
		,target.SourceID = source.SourceID
		,target.SourceCode = source.SourceCode
		,target.IsEnforced = source.IsEnforced
		,target.RuleOperator = source.RuleOperator
		,target.RuleValue = source.RuleValue
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.SessionTransactionID = source.SessionTransactionID
		,target.PriorPointerID = source.PriorPointerID
		,target.ExternalKey = source.ExternalKey
		,target.Serial = source.Serial
		,target.Parenthesis = source.Parenthesis
		,target.LogicalOperator = source.LogicalOperator
		,target.AssociatedValue = source.AssociatedValue
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime