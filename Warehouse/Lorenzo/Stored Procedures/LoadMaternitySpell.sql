﻿CREATE procedure [Lorenzo].LoadMaternitySpell as 


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(MaternitySpell.LorenzoCreated)
		from
			Lorenzo.MaternitySpell
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
		PatientID
		,ProviderSpellID
		,ProfessionalCareEpisodeID
		,ProfessionalCarerID
		,ServicePointStayID
		,ServicePointID
		,ReferralID
		,DeliveryPlaceID
		,DeliveryPlaceChangeReasonID
		,PregnancyNumber
		,Parity
		,FirstAntenatalDateTime
		,MaternalHeight
		,NumberOfPreviousCaesareans
		,NumberOfPreviousInducedAbortions
		,TotalNumberOfLiveBirths
		,TotalNumberOfStillBirths
		,TotalNumberOfNeonatalDeaths
		,TotalNumberOfPreviousPregnancies
		,NumberOfNonInducedAbortions
		,StartDateTime
		,EndDateTime
		,NumberOfPreviousBloodTransfusions
		,RubellaImmunityFlag
		,RubellaTestedFlag
		,RubellaPositiveFlag
		,RubellaImmunised
		,DeliveryDateTime
		,LabourDeliveryOnsetID
		,DeliveryPersonStatusID
		,GestationLength
		,FirstStageWeeks
		,SecondStageWeeks
		,NoOfBabies
		,AnalgesicAdministeredInLabourID
		,AnalgesicAdministeredAfterLabourID
		,SessionTransactionID
		,PriorPointerID
		,UserCreate
		,UserModify
		,ModifiedDateTime
		,CreatedDateTime
		,ExternalKey
		,FirstStageDateTime
		,SecondStageDateTime
		,InitialPlaceOfDeliveryID
		,DelivererID
		,AdditionalDelivererID
		,PriorProfessionalCarerID
		,DuringAnaestheticReasonID
		,AfterAnaestheticReasonID
		,MaternityPeriodID
		,AnaestheticReasonID
		,SelfAdministeredInhalationID
		,NarcoticsID
		,EpiduralCaudalID
		,SpinalID
		,LocationInfiltrationID
		,GeneralID
		,OtherID
		,PreviousBloodTransfusionComments
		,HealthOrganisationOwnerID
		,LorenzoCreated
			)
into
	#TLoadPASMaternitySpell
from
	(
	select
		MaternitySpellID
		,PatientID = cast(PatientID as int)
		,ProviderSpellID = cast(ProviderSpellID as int)
		,ProfessionalCareEpisodeID = cast(ProfessionalCareEpisodeID as int)
		,ProfessionalCarerID = cast(ProfessionalCarerID as int)
		,ServicePointStayID = cast(ServicePointStayID as int)
		,ServicePointID = cast(ServicePointID as int)
		,ReferralID = cast(ReferralID as int)
		,DeliveryPlaceID = cast(DeliveryPlaceID as int)
		,DeliveryPlaceChangeReasonID = cast(DeliveryPlaceChangeReasonID as int)
		,PregnancyNumber = cast(PregnancyNumber as int)
		,Parity = cast(nullif(Parity, '') as varchar(30))
		,FirstAntenatalDateTime = cast(FirstAntenatalDateTime as smalldatetime)
		,MaternalHeight = cast(MaternalHeight as int)
		,NumberOfPreviousCaesareans = cast(NumberOfPreviousCaesareans as int)
		,NumberOfPreviousInducedAbortions = cast(NumberOfPreviousInducedAbortions as int)
		,TotalNumberOfLiveBirths = cast(TotalNumberOfLiveBirths as int)
		,TotalNumberOfStillBirths = cast(TotalNumberOfStillBirths as int)
		,TotalNumberOfNeonatalDeaths = cast(TotalNumberOfNeonatalDeaths as int)
		,TotalNumberOfPreviousPregnancies = cast(TotalNumberOfPreviousPregnancies as int)
		,NumberOfNonInducedAbortions = cast(NumberOfNonInducedAbortions as int)
		,StartDateTime = cast(StartDateTime as smalldatetime)
		,EndDateTime = cast(EndDateTime as smalldatetime)
		,NumberOfPreviousBloodTransfusions = cast(nullif(NumberOfPreviousBloodTransfusions, '') as varchar(1))
		,RubellaImmunityFlag = cast(nullif(RubellaImmunityFlag, '') as varchar(1))
		,RubellaTestedFlag = cast(nullif(RubellaTestedFlag, '') as varchar(1))
		,RubellaPositiveFlag = cast(nullif(RubellaPositiveFlag, '') as varchar(1))
		,RubellaImmunised = cast(nullif(RubellaImmunised, '') as varchar(1))
		,DeliveryDateTime = cast(DeliveryDateTime as smalldatetime)
		,LabourDeliveryOnsetID = cast(LabourDeliveryOnsetID as int)
		,DeliveryPersonStatusID = cast(DeliveryPersonStatusID as int)
		,GestationLength = cast(GestationLength as int)
		,FirstStageWeeks = cast(FirstStageWeeks as int)
		,SecondStageWeeks = cast(SecondStageWeeks as int)
		,NoOfBabies = cast(NoOfBabies as int)
		,AnalgesicAdministeredInLabourID = cast(AnalgesicAdministeredInLabourID as int)
		,AnalgesicAdministeredAfterLabourID = cast(AnalgesicAdministeredAfterLabourID as int)
		,SessionTransactionID = cast(SessionTransactionID as int)
		,PriorPointerID = cast(PriorPointerID as int)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(30))
		,UserModify = cast(nullif(UserModify, '') as varchar(30))
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(20))
		,FirstStageDateTime = cast(FirstStageDateTime as smalldatetime)
		,SecondStageDateTime = cast(SecondStageDateTime as smalldatetime)
		,InitialPlaceOfDeliveryID = cast(InitialPlaceOfDeliveryID as int)
		,DelivererID = cast(DelivererID as int)
		,AdditionalDelivererID = cast(AdditionalDelivererID as int)
		,PriorProfessionalCarerID = cast(PriorProfessionalCarerID as int)
		,DuringAnaestheticReasonID = cast(DuringAnaestheticReasonID as int)
		,AfterAnaestheticReasonID = cast(AfterAnaestheticReasonID as int)
		,MaternityPeriodID = cast(MaternityPeriodID as int)
		,AnaestheticReasonID = cast(AnaestheticReasonID as int)
		,SelfAdministeredInhalationID = cast(SelfAdministeredInhalationID as int)
		,NarcoticsID = cast(NarcoticsID as int)
		,EpiduralCaudalID = cast(EpiduralCaudalID as int)
		,SpinalID = cast(SpinalID as int)
		,LocationInfiltrationID = cast(LocationInfiltrationID as int)
		,GeneralID = cast(GeneralID as int)
		,OtherID = cast(OtherID as int)
		,PreviousBloodTransfusionComments = cast(nullif(PreviousBloodTransfusionComments, '') as varchar(255))
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as int)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			MaternitySpellID = MATSP_REFNO
			,PatientID = PATNT_REFNO
			,ProviderSpellID = PRVSP_REFNO
			,ProfessionalCareEpisodeID = PRCAE_REFNO
			,ProfessionalCarerID = PROCA_REFNO
			,ServicePointStayID = SSTAY_REFNO
			,ServicePointID = SPONT_REFNO
			,ReferralID = REFRL_REFNO
			,DeliveryPlaceID = DEPLA_REFNO
			,DeliveryPlaceChangeReasonID = DEPCR_REFNO
			,PregnancyNumber = PREGNANCY_NUMBER
			,Parity = PARITY
			,FirstAntenatalDateTime = FIRST_ANTENATAL
			,MaternalHeight = MATERNAL_HEIGHT
			,NumberOfPreviousCaesareans = NOOF_PRE_CAESAREANS
			,NumberOfPreviousInducedAbortions = NOOF_IND_ABORTIONS
			,TotalNumberOfLiveBirths = TOTAL_LIVE_BIRTHS
			,TotalNumberOfStillBirths = TOTAL_STILL_BIRTHS
			,TotalNumberOfNeonatalDeaths = TOTAL_NEONATAL_DEATHS
			,TotalNumberOfPreviousPregnancies = TOTAL_PREVIOUS_PREGS
			,NumberOfNonInducedAbortions = NOOF_NIN_ABORTIONS
			,StartDateTime = START_DTTM
			,EndDateTime = END_DTTM
			,NumberOfPreviousBloodTransfusions = PREVIOUS_BLOOD_TRANS
			,RubellaImmunityFlag = RUBLA_IMMUNE
			,RubellaTestedFlag = RUBLA_TESTED
			,RubellaPositiveFlag = RUBLA_POSITIVE
			,RubellaImmunised = RUBLA_IMMUNISED
			,DeliveryDateTime = DELIV_DTTM
			,LabourDeliveryOnsetID = ONSET_REFNO
			,DeliveryPersonStatusID = DPSTS_REFNO
			,GestationLength = GESTN_LENGTH
			,FirstStageWeeks = FIRST_STAGE
			,SecondStageWeeks = SECOND_STAGE
			,NoOfBabies = NOOF_BABIES
			,AnalgesicAdministeredInLabourID = DURING_ANALC_REFNO
			,AnalgesicAdministeredAfterLabourID = AFTER_ANALC_REFNO
			,SessionTransactionID = STRAN_REFNO
			,PriorPointerID = PRIOR_POINTER
			,UserCreate = USER_CREATE
			,UserModify = USER_MODIF
			,ModifiedDateTime = MODIF_DTTM
			,CreatedDateTime = CREATE_DTTM
			,ExternalKey = EXTERNAL_KEY
			,FirstStageDateTime = STAGE1_DTTM
			,SecondStageDateTime = STAGE2_DTTM
			,InitialPlaceOfDeliveryID = INITIAL_DEPLA_REFNO
			,DelivererID = DELIV_PROCA_REFNO
			,AdditionalDelivererID = ADDITIONAL_PROCA_REFNO
			,PriorProfessionalCarerID = PRIOR_PROCA_REFNO
			,DuringAnaestheticReasonID = DUR_ANRSN_REFNO
			,AfterAnaestheticReasonID = AFT_ANRSN_REFNO
			,MaternityPeriodID = MTPER_REFNO
			,AnaestheticReasonID = ANRSN_REFNO
			,SelfAdministeredInhalationID = SELF_YNUNK_REFNO
			,NarcoticsID = NARC_YNUNK_REFNO
			,EpiduralCaudalID = EPID_YNUNK_REFNO
			,SpinalID = SPIN_YNUNK_REFNO
			,LocationInfiltrationID = LOCN_YNUNK_REFNO
			,GeneralID = GENR_YNUNK_REFNO
			,OtherID = OTHR_YNUNK_REFNO
			,PreviousBloodTransfusionComments = PREV_BLOOD_TRANS_COMMENTS
			,HealthOrganisationOwnerID = OWNER_HEORG_REFNO
			,LorenzoCreated = Created


			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.MaternitySpell
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASMaternitySpell on #TLoadPASMaternitySpell
	(
	MaternitySpellID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.MaternitySpell target
using
	(
	select
		*
	from
		#TLoadPASMaternitySpell
	
	) source
	on	source.MaternitySpellID = target.MaternitySpellID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		 MaternitySpellID
		,PatientID
		,ProviderSpellID
		,ProfessionalCareEpisodeID
		,ProfessionalCarerID
		,ServicePointStayID
		,ServicePointID
		,ReferralID
		,DeliveryPlaceID
		,DeliveryPlaceChangeReasonID
		,PregnancyNumber
		,Parity
		,FirstAntenatalDateTime
		,MaternalHeight
		,NumberOfPreviousCaesareans
		,NumberOfPreviousInducedAbortions
		,TotalNumberOfLiveBirths
		,TotalNumberOfStillBirths
		,TotalNumberOfNeonatalDeaths
		,TotalNumberOfPreviousPregnancies
		,NumberOfNonInducedAbortions
		,StartDateTime
		,EndDateTime
		,NumberOfPreviousBloodTransfusions
		,RubellaImmunityFlag
		,RubellaTestedFlag
		,RubellaPositiveFlag
		,RubellaImmunised
		,DeliveryDateTime
		,LabourDeliveryOnsetID
		,DeliveryPersonStatusID
		,GestationLength
		,FirstStageWeeks
		,SecondStageWeeks
		,NoOfBabies
		,AnalgesicAdministeredInLabourID
		,AnalgesicAdministeredAfterLabourID
		,SessionTransactionID
		,PriorPointerID
		,UserCreate
		,UserModify
		,ModifiedDateTime
		,CreatedDateTime
		,ExternalKey
		,FirstStageDateTime
		,SecondStageDateTime
		,InitialPlaceOfDeliveryID
		,DelivererID
		,AdditionalDelivererID
		,PriorProfessionalCarerID
		,DuringAnaestheticReasonID
		,AfterAnaestheticReasonID
		,MaternityPeriodID
		,AnaestheticReasonID
		,SelfAdministeredInhalationID
		,NarcoticsID
		,EpiduralCaudalID
		,SpinalID
		,LocationInfiltrationID
		,GeneralID
		,OtherID
		,PreviousBloodTransfusionComments
		,HealthOrganisationOwnerID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		 source.MaternitySpellID
		,source.PatientID
		,source.ProviderSpellID
		,source.ProfessionalCareEpisodeID
		,source.ProfessionalCarerID
		,source.ServicePointStayID
		,source.ServicePointID
		,source.ReferralID
		,source.DeliveryPlaceID
		,source.DeliveryPlaceChangeReasonID
		,source.PregnancyNumber
		,source.Parity
		,source.FirstAntenatalDateTime
		,source.MaternalHeight
		,source.NumberOfPreviousCaesareans
		,source.NumberOfPreviousInducedAbortions
		,source.TotalNumberOfLiveBirths
		,source.TotalNumberOfStillBirths
		,source.TotalNumberOfNeonatalDeaths
		,source.TotalNumberOfPreviousPregnancies
		,source.NumberOfNonInducedAbortions
		,source.StartDateTime
		,source.EndDateTime
		,source.NumberOfPreviousBloodTransfusions
		,source.RubellaImmunityFlag
		,source.RubellaTestedFlag
		,source.RubellaPositiveFlag
		,source.RubellaImmunised
		,source.DeliveryDateTime
		,source.LabourDeliveryOnsetID
		,source.DeliveryPersonStatusID
		,source.GestationLength
		,source.FirstStageWeeks
		,source.SecondStageWeeks
		,source.NoOfBabies
		,source.AnalgesicAdministeredInLabourID
		,source.AnalgesicAdministeredAfterLabourID
		,source.SessionTransactionID
		,source.PriorPointerID
		,source.UserCreate
		,source.UserModify
		,source.ModifiedDateTime
		,source.CreatedDateTime
		,source.ExternalKey
		,source.FirstStageDateTime
		,source.SecondStageDateTime
		,source.InitialPlaceOfDeliveryID
		,source.DelivererID
		,source.AdditionalDelivererID
		,source.PriorProfessionalCarerID
		,source.DuringAnaestheticReasonID
		,source.AfterAnaestheticReasonID
		,source.MaternityPeriodID
		,source.AnaestheticReasonID
		,source.SelfAdministeredInhalationID
		,source.NarcoticsID
		,source.EpiduralCaudalID
		,source.SpinalID
		,source.LocationInfiltrationID
		,source.GeneralID
		,source.OtherID
		,source.PreviousBloodTransfusionComments
		,source.HealthOrganisationOwnerID
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.PatientID
		,target.ProviderSpellID
		,target.ProfessionalCareEpisodeID
		,target.ProfessionalCarerID
		,target.ServicePointStayID
		,target.ServicePointID
		,target.ReferralID
		,target.DeliveryPlaceID
		,target.DeliveryPlaceChangeReasonID
		,target.PregnancyNumber
		,target.Parity
		,target.FirstAntenatalDateTime
		,target.MaternalHeight
		,target.NumberOfPreviousCaesareans
		,target.NumberOfPreviousInducedAbortions
		,target.TotalNumberOfLiveBirths
		,target.TotalNumberOfStillBirths
		,target.TotalNumberOfNeonatalDeaths
		,target.TotalNumberOfPreviousPregnancies
		,target.NumberOfNonInducedAbortions
		,target.StartDateTime
		,target.EndDateTime
		,target.NumberOfPreviousBloodTransfusions
		,target.RubellaImmunityFlag
		,target.RubellaTestedFlag
		,target.RubellaPositiveFlag
		,target.RubellaImmunised
		,target.DeliveryDateTime
		,target.LabourDeliveryOnsetID
		,target.DeliveryPersonStatusID
		,target.GestationLength
		,target.FirstStageWeeks
		,target.SecondStageWeeks
		,target.NoOfBabies
		,target.AnalgesicAdministeredInLabourID
		,target.AnalgesicAdministeredAfterLabourID
		,target.SessionTransactionID
		,target.PriorPointerID
		,target.UserCreate
		,target.UserModify
		,target.ModifiedDateTime
		,target.CreatedDateTime
		,target.ExternalKey
		,target.FirstStageDateTime
		,target.SecondStageDateTime
		,target.InitialPlaceOfDeliveryID
		,target.DelivererID
		,target.AdditionalDelivererID
		,target.PriorProfessionalCarerID
		,target.DuringAnaestheticReasonID
		,target.AfterAnaestheticReasonID
		,target.MaternityPeriodID
		,target.AnaestheticReasonID
		,target.SelfAdministeredInhalationID
		,target.NarcoticsID
		,target.EpiduralCaudalID
		,target.SpinalID
		,target.LocationInfiltrationID
		,target.GeneralID
		,target.OtherID
		,target.PreviousBloodTransfusionComments
		,target.HealthOrganisationOwnerID
		,target.LorenzoCreated
		)

then
	update
	set
		target.PatientID = source.PatientID
		,target.ProviderSpellID = source.ProviderSpellID
		,target.ProfessionalCareEpisodeID = source.ProfessionalCareEpisodeID
		,target.ProfessionalCarerID = source.ProfessionalCarerID
		,target.ServicePointStayID = source.ServicePointStayID
		,target.ServicePointID = source.ServicePointID
		,target.ReferralID = source.ReferralID
		,target.DeliveryPlaceID = source.DeliveryPlaceID
		,target.DeliveryPlaceChangeReasonID = source.DeliveryPlaceChangeReasonID
		,target.PregnancyNumber = source.PregnancyNumber
		,target.Parity = source.Parity
		,target.FirstAntenatalDateTime = source.FirstAntenatalDateTime
		,target.MaternalHeight = source.MaternalHeight
		,target.NumberOfPreviousCaesareans = source.NumberOfPreviousCaesareans
		,target.NumberOfPreviousInducedAbortions = source.NumberOfPreviousInducedAbortions
		,target.TotalNumberOfLiveBirths = source.TotalNumberOfLiveBirths
		,target.TotalNumberOfStillBirths = source.TotalNumberOfStillBirths
		,target.TotalNumberOfNeonatalDeaths = source.TotalNumberOfNeonatalDeaths
		,target.TotalNumberOfPreviousPregnancies = source.TotalNumberOfPreviousPregnancies
		,target.NumberOfNonInducedAbortions = source.NumberOfNonInducedAbortions
		,target.StartDateTime = source.StartDateTime
		,target.EndDateTime = source.EndDateTime
		,target.NumberOfPreviousBloodTransfusions = source.NumberOfPreviousBloodTransfusions
		,target.RubellaImmunityFlag = source.RubellaImmunityFlag
		,target.RubellaTestedFlag = source.RubellaTestedFlag
		,target.RubellaPositiveFlag = source.RubellaPositiveFlag
		,target.RubellaImmunised = source.RubellaImmunised
		,target.DeliveryDateTime = source.DeliveryDateTime
		,target.LabourDeliveryOnsetID = source.LabourDeliveryOnsetID
		,target.DeliveryPersonStatusID = source.DeliveryPersonStatusID
		,target.GestationLength = source.GestationLength
		,target.FirstStageWeeks = source.FirstStageWeeks
		,target.SecondStageWeeks = source.SecondStageWeeks
		,target.NoOfBabies = source.NoOfBabies
		,target.AnalgesicAdministeredInLabourID = source.AnalgesicAdministeredInLabourID
		,target.AnalgesicAdministeredAfterLabourID = source.AnalgesicAdministeredAfterLabourID
		,target.SessionTransactionID = source.SessionTransactionID
		,target.PriorPointerID = source.PriorPointerID
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ExternalKey = source.ExternalKey
		,target.FirstStageDateTime = source.FirstStageDateTime
		,target.SecondStageDateTime = source.SecondStageDateTime
		,target.InitialPlaceOfDeliveryID = source.InitialPlaceOfDeliveryID
		,target.DelivererID = source.DelivererID
		,target.AdditionalDelivererID = source.AdditionalDelivererID
		,target.PriorProfessionalCarerID = source.PriorProfessionalCarerID
		,target.DuringAnaestheticReasonID = source.DuringAnaestheticReasonID
		,target.AfterAnaestheticReasonID = source.AfterAnaestheticReasonID
		,target.MaternityPeriodID = source.MaternityPeriodID
		,target.AnaestheticReasonID = source.AnaestheticReasonID
		,target.SelfAdministeredInhalationID = source.SelfAdministeredInhalationID
		,target.NarcoticsID = source.NarcoticsID
		,target.EpiduralCaudalID = source.EpiduralCaudalID
		,target.SpinalID = source.SpinalID
		,target.LocationInfiltrationID = source.LocationInfiltrationID
		,target.GeneralID = source.GeneralID
		,target.OtherID = source.OtherID
		,target.PreviousBloodTransfusionComments = source.PreviousBloodTransfusionComments
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime