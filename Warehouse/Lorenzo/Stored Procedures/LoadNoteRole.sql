﻿CREATE PROCEDURE [Lorenzo].[LoadNoteRole]


AS

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(NoteRole.LorenzoCreated)
		from
			Lorenzo.NoteRole
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 NoteRoleID
			,NotesID
			,NotesIDKeyWords
			,NotesIDNote
			,ActivityID
			,ActivityCode
			,StartDateTime
			,EndDateTime
			,UserCreate
			,UserModify
			,CreatedDateTime
			,ModifiedDateTime
			,NoteSerial
			,NoteRoleTypeID
			,NoteRoleTypeMainCode
			,NoteRoleTypeDescription
			,HealthOrganisationOwnerID
			,HealthOrgnanisationOwnerMainCode
			,LorenzoCreated
			)
into
	#TLoadPASNoteRole
from
	(
	select
		NoteRoleID
		,NotesID = cast(NotesID as numeric)
		,NotesIDKeyWords = cast(nullif(NotesIDKeyWords, '') as varchar(100))
		,NotesIDNote = cast(nullif(NotesIDNote, '') as varchar(2000))
		,ActivityID = cast(ActivityID as numeric)
		,ActivityCode = cast(nullif(ActivityCode, '') as varchar(5))
		,StartDateTime = cast(StartDateTime as datetime)
		,EndDateTime = cast(EndDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(30))
		,UserModify = cast(nullif(UserModify, '') as varchar(30))
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,NoteSerial = cast(NoteSerial as numeric)
		,NoteRoleTypeID = cast(NoteRoleTypeID as numeric)
		,NoteRoleTypeMainCode = cast(nullif(NoteRoleTypeMainCode, '') as varchar(25))
		,NoteRoleTypeDescription = cast(nullif(NoteRoleTypeDescription, '') as varchar(80))
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as numeric)
		,HealthOrgnanisationOwnerMainCode = cast(nullif(HealthOrgnanisationOwnerMainCode, '') as varchar(25))
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			NoteRoleID = [NOTRL_REFNO]
		  ,NotesID = [NOTES_REFNO]
		  ,NotesIDKeyWords = [NOTES_REFNO_KEY_WORDS]
		  ,NotesIDNote = [NOTES_REFNO_NOTE]
		  ,ActivityID = [SORCE_REFNO]
		  ,ActivityCode = [SORCE_CODE]
		  ,StartDateTime = [START_DTTM]
		  ,EndDateTime = [END_DTTM] 
		  ,UserCreate = [USER_CREATE]
		  ,UserModify = [USER_MODIF]
		  ,CreatedDateTime = [CREATE_DTTM]
		  ,ModifiedDateTime = [MODIF_DTTM]
		  ,NoteSerial = [SERIAL] 
		  ,NoteRoleTypeID = [NRTYP_REFNO]
		  ,NoteRoleTypeMainCode = [NRTYP_REFNO_MAIN_CODE]
		  ,NoteRoleTypeDescription = [NRTYP_REFNO_DESCRIPTION]
		  ,HealthOrganisationOwnerID = [OWNER_HEORG_REFNO]
		  ,HealthOrgnanisationOwnerMainCode = [OWNER_HEORG_REFNO_MAIN_IDENT]  
		  ,LorenzoCreated = [Created]

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.NoteRole
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASNoteRole on #TLoadPASNoteRole
	(
	NoteRoleID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.NoteRole target
using
	(
	select
		*
	from
		#TLoadPASNoteRole
	
	) source
	on	source.NoteRoleID = target.NoteRoleID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		 NoteRoleID
		,NotesID
		,NotesIDKeyWords
		,NotesIDNote
		,ActivityID
		,ActivityCode
		,StartDateTime
		,EndDateTime
		,UserCreate
		,UserModify
		,CreatedDateTime
		,ModifiedDateTime
		,NoteSerial
		,NoteRoleTypeID
		,NoteRoleTypeMainCode
		,NoteRoleTypeDescription
		,HealthOrganisationOwnerID
		,HealthOrgnanisationOwnerMainCode
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		  source.NoteRoleID
		,source.NotesID
		,source.NotesIDKeyWords
		,source.NotesIDNote
		,source.ActivityID
		,source.ActivityCode
		,source.StartDateTime
		,source.EndDateTime
		,source.UserCreate
		,source.UserModify
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.NoteSerial
		,source.NoteRoleTypeID
		,source.NoteRoleTypeMainCode
		,source.NoteRoleTypeDescription
		,source.HealthOrganisationOwnerID
		,source.HealthOrgnanisationOwnerMainCode
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		 target.NoteRoleID
		,target.NotesID
		,target.NotesIDKeyWords
		,target.NotesIDNote
		,target.ActivityID
		,target.ActivityCode
		,target.StartDateTime
		,target.EndDateTime
		,target.UserCreate
		,target.UserModify
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.NoteSerial
		,target.NoteRoleTypeID
		,target.NoteRoleTypeMainCode
		,target.NoteRoleTypeDescription
		,target.HealthOrganisationOwnerID
		,target.HealthOrgnanisationOwnerMainCode
		,target.LorenzoCreated
		)

then
	update
	set
		  target.NoteRoleID = source.NoteRoleID
		,target.NotesID = source.NotesID
		,target.NotesIDKeyWords = source.NotesIDKeyWords
		,target.NotesIDNote = source.NotesIDNote
		,target.ActivityID = source.ActivityID
		,target.ActivityCode = source.ActivityCode
		,target.StartDateTime = source.StartDateTime
		,target.EndDateTime = source.EndDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.NoteSerial = source.NoteSerial
		,target.NoteRoleTypeID = source.NoteRoleTypeID
		,target.NoteRoleTypeMainCode = source.NoteRoleTypeMainCode
		,target.NoteRoleTypeDescription = source.NoteRoleTypeDescription
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.HealthOrgnanisationOwnerMainCode = source.HealthOrgnanisationOwnerMainCode
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime
