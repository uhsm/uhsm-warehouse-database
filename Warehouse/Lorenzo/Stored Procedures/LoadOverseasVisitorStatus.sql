﻿CREATE procedure [Lorenzo].[LoadOverseasVisitorStatus] as 


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(OverseasVisitorStatus.LorenzoCreated)
		from
			Lorenzo.OverseasVisitorStatus
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			OverseasStatusID
			,OverseasStatusMainCode
			,OverseasStatus
			,PatientID
			,PatientSystemIdentifier
			,NHSNumber
			,StartDateTime
			,EndDateTime
			,EnteredHostCountryDateTime
			,CreatedDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,LorenzoCreated
			)
into
	#TLoadPASOverseasVisitorStatus
from
	(
	select
		OverseasVisitorStatusID
		,OverseasStatusID = cast(OverseasStatusID as int)
		,OverseasStatusMainCode = cast(nullif(OverseasStatusMainCode, '') as varchar(25))
		,OverseasStatus = cast(nullif(OverseasStatus, '') as varchar(80))
		,PatientID = cast(PatientID as int)
		,PatientSystemIdentifier = cast(nullif(PatientSystemIdentifier, '') as varchar(20))
		,NHSNumber = cast(nullif(NHSNumber, '') as varchar(20))
		,StartDateTime = cast(StartDateTime as datetime)
		,EndDateTime = cast(EndDateTime as datetime)
		,EnteredHostCountryDateTime = cast(EnteredHostCountryDateTime as datetime)
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(30))
		,UserModify = cast(nullif(UserModify, '') as varchar(30))
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			OverseasVisitorStatusID = OVSEA_REFNO
			,OverseasStatusID = OVSVS_REFNO
			,OverseasStatusMainCode = OVSVS_REFNO_MAIN_CODE
			,OverseasStatus = OVSVS_REFNO_DESCRIPTION
			,PatientID = PATNT_REFNO
			,PatientSystemIdentifier = PATNT_REFNO_PASID
			,NHSNumber = PATNT_REFNO_NHS_IDENTIFIER
			,StartDateTime = START_DTTM
			,EndDateTime = END_DTTM
			,EnteredHostCountryDateTime = ENTRY_DTTM
			,CreatedDateTime = CREATE_DTTM
			,ModifiedDateTime = MODIF_DTTM
			,UserCreate = USER_CREATE
			,UserModify = USER_MODIF
			,LorenzoCreated = Created

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.OverseasVisitorStatus
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASOverseasVisitorStatus on #TLoadPASOverseasVisitorStatus
	(
	OverseasVisitorStatusID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.OverseasVisitorStatus target
using
	(
	select
		*
	from
		#TLoadPASOverseasVisitorStatus
	
	) source
	on	source.OverseasVisitorStatusID = target.OverseasVisitorStatusID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		OverseasVisitorStatusID
		,OverseasStatusID
		,OverseasStatusMainCode
		,OverseasStatus
		,PatientID
		,PatientSystemIdentifier
		,NHSNumber
		,StartDateTime
		,EndDateTime
		,EnteredHostCountryDateTime
		,CreatedDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		source.OverseasVisitorStatusID
		,source.OverseasStatusID
		,source.OverseasStatusMainCode
		,source.OverseasStatus
		,source.PatientID
		,source.PatientSystemIdentifier
		,source.NHSNumber
		,source.StartDateTime
		,source.EndDateTime
		,source.EnteredHostCountryDateTime
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.UserCreate
		,source.UserModify
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.OverseasStatusID
		,target.OverseasStatusMainCode
		,target.OverseasStatus
		,target.PatientID
		,target.PatientSystemIdentifier
		,target.NHSNumber
		,target.StartDateTime
		,target.EndDateTime
		,target.EnteredHostCountryDateTime
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.LorenzoCreated
		)

then
	update
	set
		target.OverseasStatusID = source.OverseasStatusID
		,target.OverseasStatusMainCode = source.OverseasStatusMainCode
		,target.OverseasStatus = source.OverseasStatus
		,target.PatientID = source.PatientID
		,target.PatientSystemIdentifier = source.PatientSystemIdentifier
		,target.NHSNumber = source.NHSNumber
		,target.StartDateTime = source.StartDateTime
		,target.EndDateTime = source.EndDateTime
		,target.EnteredHostCountryDateTime = source.EnteredHostCountryDateTime
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime