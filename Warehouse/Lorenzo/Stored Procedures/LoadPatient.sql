﻿CREATE procedure [Lorenzo].[LoadPatient] as 


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(Patient.LorenzoCreated)
		from
			Lorenzo.Patient
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			SexID
			,Forename
			,Title
			,Surname
			,SearchSurname
			,EthnicCategoryID
			,MaritalStatusID
			,DateTimeOfBirth
			,OccupationID
			,SpokenLanguageID
			,AccessLevelID
			,SoundexForename
			,SoundexSurname
			,UpperForename
			,UpperSurname
			,ChronicallySickFlag
			,DisabledFlag
			,DeceasedFlag
			,ConfidentialFlag
			,DateTimeOfDeath
			,PlaceOfBirth
			,PlaceOfDeath
			,ReligionID
			,CountryID
			,IncomeSourceID
			,Occupation
			,FastTrackCode
			,CreateDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,PASID
			,DateOfBirth
			,DateOfDeath
			,DeathNotifiedDateTime
			,Comments
			,SessionTransactionID
			,PriorPointerID
			,ExternalKey
			,TempPASID
			,BirthDateTimeEstimateFlag
			,InterpreterRequiredFlag
			,BloodID
			,LocalFlag
			,PCTIdealBodyWeight
			,ReasonForEstimationOfBirthDateTime
			,RegistrationDateTime
			,MultipleBirthIndicator
			,PrivateIdentifier
			,NHSIdentifier
			,LastSpellIdentifier
			,CasenoteStatusID
			,ApproximateDateOfDeathReasonID
			,CasenoteLocationTypeID
			,PreferredForename
			,UpperPreferredForename
			,SoundexPreferredForename
			,SecondForename
			,UpperSecondForename
			,SoundexSecondForename
			,ThirdForename
			,UpperThirdForename
			,SoundexThirdForename
			,SecondEthnicCategoryID
			,ThirdEthnicCategoryID
			,PatientIdentifierSuffixID
			,DeathDateTimeEstimateFlag
			,NHSNumberTracingServiceTypeID
			,NationalityID
			,LastUpdateFromCFUDateTime
			,RestrictedAccessFlag
			,RestrictedVIPFlag
			,RestrictionStartDateTime
			,RestrictionEndDateTime
			,RestrictionAccessTypeID
			,DietaryRequirementsID
			,RestrictionRequestBy
			,RelationshipTypeID
			,RestrictionLevelID
			,MultilingualName
			,ResidentYesNoUnknownID
			,SundryDebtorFlag
			,PlaceOfDeathID
			,HowCauseOfDeathEstablishedID
			,CauseOfCancerRelatedDeathID
			,ImmediateCauseOfDeathID
			,ConditionLeadingToDeathID
			,UnderlyingConditionLeadingToDeathID
			,SignificantConditionNotLeadingToDeathID
			,CauseOfDeathDiscrepancyID
			,MergeMinorFlag
			,StaffFlag
			,PayeeOnlyFlag
			,SCNNumber
			,PDSUpdateNeeded
			,PDSReason
			,PDSStopNotedFlag
			,DeathNotificationStatusID
			,InterestedOrganisations
			,OwnerHealthOrganisationSubFlag
			,PreviousNHSContactDetailsID
			,PDSIdentifier
			,PDSDateOfBirth
			,ConsentStatusID
			,DementiaID
			,FrailtyID
			,FirstCareDateTime
			,DirectDebitTransactionCount
			,Suffix
			,SchoolHealthOrganisationID
			,EmploymentStatusID
			,OtherLanguageID
			,NumberOfDependents
			,PDSDateOfDeath
			,PatientConsentID
			,DeceasedRemoveFlag
			,PDSSupersededFlag
			,SupersedingNHSNumber
			,SexualOrientationID
			,SuitabilityIndicator
			,ReferralDateTime
			,StatutorySickPayIndicatorID
			,PreferredPlaceOfDeathID
			,OwnerHealthOrganisationID
			,LorenzoCreated
			)
into
	#TLoadPASPatient
from
	(
	select
		PatientID
		,SexID = cast(SexID as int)
		,Forename = cast(nullif(Forename, '') as varchar(30))
		,Title = cast(Title as int)
		,Surname = cast(nullif(Surname, '') as varchar(30))
		,SearchSurname = cast(nullif(SearchSurname, '') as varchar(30))
		,EthnicCategoryID = cast(EthnicCategoryID as int)
		,MaritalStatusID = cast(MaritalStatusID as int)
		,DateTimeOfBirth = cast(DateTimeOfBirth as datetime)
		,OccupationID = cast(OccupationID as int)
		,SpokenLanguageID = cast(SpokenLanguageID as int)
		,AccessLevelID = cast(AccessLevelID as int)
		,SoundexForename = cast(nullif(SoundexForename, '') as varchar(30))
		,SoundexSurname = cast(nullif(SoundexSurname, '') as varchar(30))
		,UpperForename = cast(nullif(UpperForename, '') as varchar(30))
		,UpperSurname = cast(nullif(UpperSurname, '') as varchar(30))
		,ChronicallySickFlag = cast(nullif(ChronicallySickFlag, '') as varchar(1))
		,DisabledFlag = cast(nullif(DisabledFlag, '') as varchar(1))
		,DeceasedFlag = cast(nullif(DeceasedFlag, '') as varchar(1))
		,ConfidentialFlag = cast(nullif(ConfidentialFlag, '') as varchar(1))
		,DateTimeOfDeath = cast(DateTimeOfDeath as datetime)
		,PlaceOfBirth = cast(nullif(PlaceOfBirth, '') as varchar(80))
		,PlaceOfDeath = cast(nullif(PlaceOfDeath, '') as varchar(80))
		,ReligionID = cast(ReligionID as int)
		,CountryID = cast(CountryID as int)
		,IncomeSourceID = cast(IncomeSourceID as int)
		,Occupation = cast(nullif(Occupation, '') as varchar(255))
		,FastTrackCode = cast(nullif(FastTrackCode, '') as varchar(1))
		,CreateDateTime = cast(CreateDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(30))
		,UserModify = cast(nullif(UserModify, '') as varchar(30))
		,PASID = cast(nullif(PASID, '') as varchar(20))
		,DateOfBirth = cast(DateOfBirth as date)
		,DateOfDeath = cast(DateOfDeath as date)
		,DeathNotifiedDateTime = cast(DeathNotifiedDateTime as datetime)
		,Comments = cast(nullif(Comments, '') as varchar(255))
		,SessionTransactionID = cast(SessionTransactionID as int)
		,PriorPointerID = cast(PriorPointerID as int)
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(20))
		,TempPASID = cast(nullif(TempPASID, '') as varchar(1))
		,BirthDateTimeEstimateFlag = cast(nullif(BirthDateTimeEstimateFlag, '') as varchar(1))
		,InterpreterRequiredFlag = cast(nullif(InterpreterRequiredFlag, '') as varchar(1))
		,BloodID = cast(BloodID as int)
		,LocalFlag = cast(nullif(LocalFlag, '') as varchar(1))
		,PCTIdealBodyWeight = cast(PCTIdealBodyWeight as int)
		,ReasonForEstimationOfBirthDateTime = cast(ReasonForEstimationOfBirthDateTime as int)
		,RegistrationDateTime = cast(RegistrationDateTime as datetime)
		,MultipleBirthIndicator = cast(MultipleBirthIndicator as int)
		,PrivateIdentifier = cast(nullif(PrivateIdentifier, '') as varchar(20))
		,NHSIdentifier = cast(nullif(NHSIdentifier, '') as varchar(20))
		,LastSpellIdentifier = cast(nullif(LastSpellIdentifier, '') as varchar(20))
		,CasenoteStatusID = cast(CasenoteStatusID as int)
		,ApproximateDateOfDeathReasonID = cast(ApproximateDateOfDeathReasonID as int)
		,CasenoteLocationTypeID = cast(CasenoteLocationTypeID as int)
		,PreferredForename = cast(nullif(PreferredForename, '') as varchar(30))
		,UpperPreferredForename = cast(nullif(UpperPreferredForename, '') as varchar(30))
		,SoundexPreferredForename = cast(nullif(SoundexPreferredForename, '') as varchar(30))
		,SecondForename = cast(nullif(SecondForename, '') as varchar(30))
		,UpperSecondForename = cast(nullif(UpperSecondForename, '') as varchar(30))
		,SoundexSecondForename = cast(nullif(SoundexSecondForename, '') as varchar(30))
		,ThirdForename = cast(nullif(ThirdForename, '') as varchar(30))
		,UpperThirdForename = cast(nullif(UpperThirdForename, '') as varchar(30))
		,SoundexThirdForename = cast(nullif(SoundexThirdForename, '') as varchar(30))
		,SecondEthnicCategoryID = cast(SecondEthnicCategoryID as int)
		,ThirdEthnicCategoryID = cast(ThirdEthnicCategoryID as int)
		,PatientIdentifierSuffixID = cast(PatientIdentifierSuffixID as int)
		,DeathDateTimeEstimateFlag = cast(nullif(DeathDateTimeEstimateFlag, '') as varchar(1))
		,NHSNumberTracingServiceTypeID = cast(nullif(NHSNumberTracingServiceTypeID, '') as varchar(5))
		,NationalityID = cast(NationalityID as int)
		,LastUpdateFromCFUDateTime = cast(LastUpdateFromCFUDateTime as datetime)
		,RestrictedAccessFlag = cast(nullif(RestrictedAccessFlag, '') as varchar(1))
		,RestrictedVIPFlag = cast(nullif(RestrictedVIPFlag, '') as varchar(1))
		,RestrictionStartDateTime = cast(RestrictionStartDateTime as datetime)
		,RestrictionEndDateTime = cast(RestrictionEndDateTime as datetime)
		,RestrictionAccessTypeID = cast(RestrictionAccessTypeID as int)
		,DietaryRequirementsID = cast(DietaryRequirementsID as int)
		,RestrictionRequestBy = cast(nullif(RestrictionRequestBy, '') as varchar(20))
		,RelationshipTypeID = cast(RelationshipTypeID as int)
		,RestrictionLevelID = cast(RestrictionLevelID as int)
		,MultilingualName = cast(nullif(MultilingualName, '') as varchar(100))
		,ResidentYesNoUnknownID = cast(ResidentYesNoUnknownID as int)
		,SundryDebtorFlag = cast(nullif(SundryDebtorFlag, '') as varchar(1))
		,PlaceOfDeathID = cast(PlaceOfDeathID as int)
		,HowCauseOfDeathEstablishedID = cast(HowCauseOfDeathEstablishedID as int)
		,CauseOfCancerRelatedDeathID = cast(CauseOfCancerRelatedDeathID as int)
		,ImmediateCauseOfDeathID = cast(ImmediateCauseOfDeathID as int)
		,ConditionLeadingToDeathID = cast(ConditionLeadingToDeathID as int)
		,UnderlyingConditionLeadingToDeathID = cast(UnderlyingConditionLeadingToDeathID as int)
		,SignificantConditionNotLeadingToDeathID = cast(SignificantConditionNotLeadingToDeathID as int)
		,CauseOfDeathDiscrepancyID = cast(nullif(CauseOfDeathDiscrepancyID, '') as varchar(1))
		,MergeMinorFlag = cast(nullif(MergeMinorFlag, '') as varchar(1))
		,StaffFlag = cast(nullif(StaffFlag, '') as varchar(1))
		,PayeeOnlyFlag = cast(nullif(PayeeOnlyFlag, '') as varchar(1))
		,SCNNumber = cast(SCNNumber as int)
		,PDSUpdateNeeded = cast(nullif(PDSUpdateNeeded, '') as varchar(500))
		,PDSReason = cast(nullif(PDSReason, '') as varchar(500))
		,PDSStopNotedFlag = cast(nullif(PDSStopNotedFlag, '') as varchar(1))
		,DeathNotificationStatusID = cast(DeathNotificationStatusID as int)
		,InterestedOrganisations = cast(nullif(InterestedOrganisations, '') as varchar(255))
		,OwnerHealthOrganisationSubFlag = cast(nullif(OwnerHealthOrganisationSubFlag, '') as varchar(1))
		,PreviousNHSContactDetailsID = cast(PreviousNHSContactDetailsID as int)
		,PDSIdentifier = cast(nullif(PDSIdentifier, '') as varchar(25))
		,PDSDateOfBirth = cast(nullif(PDSDateOfBirth, '') as varchar(20))
		,ConsentStatusID = cast(ConsentStatusID as int)
		,DementiaID = cast(DementiaID as int)
		,FrailtyID = cast(FrailtyID as int)
		,FirstCareDateTime = cast(FirstCareDateTime as datetime)
		,DirectDebitTransactionCount = cast(DirectDebitTransactionCount as int)
		,Suffix = cast(nullif(Suffix, '') as varchar(30))
		,SchoolHealthOrganisationID = cast(SchoolHealthOrganisationID as int)
		,EmploymentStatusID = cast(EmploymentStatusID as int)
		,OtherLanguageID = cast(OtherLanguageID as int)
		,NumberOfDependents = cast(NumberOfDependents as int)
		,PDSDateOfDeath = cast(nullif(PDSDateOfDeath, '') as varchar(20))
		,PatientConsentID = cast(PatientConsentID as int)
		,DeceasedRemoveFlag = cast(nullif(DeceasedRemoveFlag, '') as varchar(1))
		,PDSSupersededFlag = cast(nullif(PDSSupersededFlag, '') as varchar(1))
		,SupersedingNHSNumber = cast(nullif(SupersedingNHSNumber, '') as varchar(20))
		,SexualOrientationID = cast(SexualOrientationID as int)
		,SuitabilityIndicator = cast(nullif(SuitabilityIndicator, '') as varchar(2))
		,ReferralDateTime = cast(ReferralDateTime as datetime)
		,StatutorySickPayIndicatorID = cast(StatutorySickPayIndicatorID as int)
		,PreferredPlaceOfDeathID = cast(PreferredPlaceOfDeathID as int)
		,OwnerHealthOrganisationID = cast(OwnerHealthOrganisationID as int)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			PatientID = Patient.PATNT_REFNO
			,SexID = Patient.SEXXX_REFNO
			,Forename = Patient.FORENAME
			,Title = Patient.TITLE_REFNO
			,Surname = Patient.SURNAME
			,SearchSurname = Patient.SEARCH_SURNAME
			,EthnicCategoryID = Patient.ETHGR_REFNO
			,MaritalStatusID = Patient.MARRY_REFNO
			,DateTimeOfBirth = Patient.DTTM_OF_BIRTH
			,OccupationID = Patient.OCCUP_REFNO
			,SpokenLanguageID = Patient.SPOKL_REFNO
			,AccessLevelID = Patient.ACLEV_REFNO
			,SoundexForename = Patient.SNDEX_FORENAME
			,SoundexSurname = Patient.SNDEX_SURNAME
			,UpperForename = Patient.UPPER_FORENAME
			,UpperSurname = Patient.UPPER_SURNAME
			,ChronicallySickFlag = Patient.CHRON_SICK
			,DisabledFlag = Patient.[DISABLED]
			,DeceasedFlag = Patient.DECSD_FLAG
			,ConfidentialFlag = Patient.CONFIDENTIAL
			,DateTimeOfDeath = Patient.DTTM_OF_DEATH
			,PlaceOfBirth = Patient.PLACE_OF_BIRTH
			,PlaceOfDeath = Patient.PLACE_OF_DEATH
			,ReligionID = Patient.RELIG_REFNO
			,CountryID = Patient.CNTRY_REFNO
			,IncomeSourceID = Patient.INSRC_REFNO
			,Occupation = Patient.OCCUP_DESCRIPTION
			,FastTrackCode = Patient.FAST_TRACK
			,CreateDateTime = Patient.CREATE_DTTM
			,ModifiedDateTime = Patient.MODIF_DTTM
			,UserCreate = Patient.USER_CREATE
			,UserModify = Patient.USER_MODIF
			,PASID = Patient.PASID
			,DateOfBirth = Patient.DATE_OF_BIRTH
			,DateOfDeath = Patient.DATE_OF_DEATH
			,DeathNotifiedDateTime = Patient.DEATH_NOTIFIED_DTTM
			,Comments = Patient.COMMENTS
			,SessionTransactionID = Patient.STRAN_REFNO
			,PriorPointerID = Patient.PRIOR_POINTER
			,ExternalKey = Patient.EXTERNAL_KEY
			,TempPASID = Patient.TEMP_PAS_ID
			,BirthDateTimeEstimateFlag = Patient.BIRTH_DTTM_ESTIMATE_FLAG
			,InterpreterRequiredFlag = Patient.INTRP_REQD_FLAG
			,BloodID = Patient.BLOOD_REFNO
			,LocalFlag = Patient.LOCAL_FLAG
			,PCTIdealBodyWeight = Patient.PCT_IDEAL_BODY_WEGHT
			,ReasonForEstimationOfBirthDateTime = Patient.REBES_REFNO
			,RegistrationDateTime = Patient.REGIS_DTTM
			,MultipleBirthIndicator = Patient.MULTB_REFNO
			,PrivateIdentifier = Patient.PRIVATE_IDENTIFIER
			,NHSIdentifier = Patient.NHS_IDENTIFIER
			,LastSpellIdentifier = Patient.LAST_SPELL_IDENTIFIER
			,CasenoteStatusID = Patient.CNTST_REFNO
			,ApproximateDateOfDeathReasonID = Patient.APDTH_REFNO
			,CasenoteLocationTypeID = Patient.CASLT_REFNO
			,PreferredForename = Patient.PREFRD_FORENAME
			,UpperPreferredForename = Patient.UPPER_PREFRD_FORENAME
			,SoundexPreferredForename = Patient.SNDEX_PREFRD_FORENAME
			,SecondForename = Patient.SECOND_FORENAME
			,UpperSecondForename = Patient.UPPER_SECOND_FORENAME
			,SoundexSecondForename = Patient.SNDEX_SECOND_FORENAME
			,ThirdForename = Patient.THIRD_FORENAME
			,UpperThirdForename = Patient.UPPER_THIRD_FORENAME
			,SoundexThirdForename = Patient.SNDEX_THIRD_FORENAME
			,SecondEthnicCategoryID = Patient.SECOND_ETHGR_REFNO
			,ThirdEthnicCategoryID = Patient.THIRD_ETHGR_REFNO
			,PatientIdentifierSuffixID = Patient.IDSUF_REFNO
			,DeathDateTimeEstimateFlag = Patient.DEATH_DTTM_ESTIMATE_FLAG
			,NHSNumberTracingServiceTypeID = Patient.NNNTS_CODE
			,NationalityID = Patient.NATNL_REFNO
			,LastUpdateFromCFUDateTime = Patient.CFU_UPDATE_DTTM
			,RestrictedAccessFlag = Patient.RESTRICTED_ACCESS
			,RestrictedVIPFlag = Patient.RESTRICTED_VIP
			,RestrictionStartDateTime = Patient.RESTRICTION_START_DTTM
			,RestrictionEndDateTime = Patient.RESTRICTION_END_DTTM
			,RestrictionAccessTypeID = Patient.RSTTP_REFNO
			,DietaryRequirementsID = Patient.DIETY_REFNO
			,RestrictionRequestBy = Patient.RESTRICTION_REQUESTBY
			,RelationshipTypeID = Patient.RELTN_REFNO
			,RestrictionLevelID = Patient.RSTLV_REFNO
			,MultilingualName = Patient.MULTILINGUAL_NAME
			,ResidentYesNoUnknownID = Patient.RESIDENT_YNUNK_REFNO
			,SundryDebtorFlag = Patient.SUNDRY_DEBTOR
			,PlaceOfDeathID = Patient.PLDTH_REFNO
			,HowCauseOfDeathEstablishedID = Patient.DCEST_REFNO
			,CauseOfCancerRelatedDeathID = Patient.CCDTH_REFNO
			,ImmediateCauseOfDeathID = Patient.IMMED_CODTH_ODPCD_REFNO
			,ConditionLeadingToDeathID = Patient.CONDT_CODTH_ODPCD_REFNO
			,UnderlyingConditionLeadingToDeathID = Patient.UNCON_CODTH_ODPCD_REFNO
			,SignificantConditionNotLeadingToDeathID = Patient.SGNIF_CODTH_ODPCD_REFNO
			,CauseOfDeathDiscrepancyID = Patient.CODTH_DISCREPANCY
			,MergeMinorFlag = Patient.MERGE_MINOR_FLAG
			,StaffFlag = Patient.STAFF_FLAG
			,PayeeOnlyFlag = Patient.PAYEE_ONLY
			,SCNNumber = Patient.SCN_NUMBER
			,PDSUpdateNeeded = Patient.PDS_UPDATE_NEEDED
			,PDSReason = Patient.PDS_REASON
			,PDSStopNotedFlag = Patient.PDS_STOP_NOTED
			,DeathNotificationStatusID = Patient.DENOT_REFNO
			,InterestedOrganisations = Patient.INTERESTED_ORGS
			,OwnerHealthOrganisationSubFlag = Patient.OWNER_HEORG_SUB_FLAG
			,PreviousNHSContactDetailsID = Patient.PRNHS_REFNO
			,PDSIdentifier = Patient.PDS_NAME_IDENTIFIER
			,PDSDateOfBirth = Patient.PDS_DOB
			,ConsentStatusID = Patient.CSTST_REFNO
			,DementiaID = Patient.DEMEN_REFNO
			,FrailtyID = Patient.FRAIL_REFNO
			,FirstCareDateTime = Patient.FIRSTCARE_DTTM
			,DirectDebitTransactionCount = Patient.DD_COUNT
			,Suffix = Patient.SUFFIX
			,SchoolHealthOrganisationID = Patient.SCHOOL_HEORG_REFNO
			,EmploymentStatusID = Patient.EMPST_REFNO
			,OtherLanguageID = Patient.OTHER_LANG_REFNO
			,NumberOfDependents = Patient.NO_OF_DPNDS
			,PDSDateOfDeath = Patient.PDS_DOD
			,PatientConsentID = Patient.PTCNT_REFNO
			,DeceasedRemoveFlag = Patient.DECEASE_REMOVED_FLAG
			,PDSSupersededFlag = Patient.SUPERSEDED_ON_PDS
			,SupersedingNHSNumber = Patient.SUPERSEDING_NHS
			,SexualOrientationID = Patient.SEXOR_REFNO
			,SuitabilityIndicator = Patient.SUITABLITY_INDICATOR
			,ReferralDateTime = Patient.REFRL_DTTM
			,StatutorySickPayIndicatorID = Patient.SSPIN_REFNO
			,PreferredPlaceOfDeathID = Patient.PREF_PLDTH_REFNO
			,OwnerHealthOrganisationID = Patient.OWNER_HEORG_REFNO
			,LorenzoCreated = Patient.Created
			,IsDeleted = cast(case when Patient.ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.Patient
		where
			Patient.Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASPatient on #TLoadPASPatient
	(
	 PatientID ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.Patient target
using
	(
	select
		*
	from
		#TLoadPASPatient
	
	) source
	on	source.PatientID = target.PatientID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		PatientID
		,SexID
		,Forename
		,Title
		,Surname
		,SearchSurname
		,EthnicCategoryID
		,MaritalStatusID
		,DateTimeOfBirth
		,OccupationID
		,SpokenLanguageID
		,AccessLevelID
		,SoundexForename
		,SoundexSurname
		,UpperForename
		,UpperSurname
		,ChronicallySickFlag
		,DisabledFlag
		,DeceasedFlag
		,ConfidentialFlag
		,DateTimeOfDeath
		,PlaceOfBirth
		,PlaceOfDeath
		,ReligionID
		,CountryID
		,IncomeSourceID
		,Occupation
		,FastTrackCode
		,CreateDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,PASID
		,DateOfBirth
		,DateOfDeath
		,DeathNotifiedDateTime
		,Comments
		,SessionTransactionID
		,PriorPointerID
		,ExternalKey
		,TempPASID
		,BirthDateTimeEstimateFlag
		,InterpreterRequiredFlag
		,BloodID
		,LocalFlag
		,PCTIdealBodyWeight
		,ReasonForEstimationOfBirthDateTime
		,RegistrationDateTime
		,MultipleBirthIndicator
		,PrivateIdentifier
		,NHSIdentifier
		,LastSpellIdentifier
		,CasenoteStatusID
		,ApproximateDateOfDeathReasonID
		,CasenoteLocationTypeID
		,PreferredForename
		,UpperPreferredForename
		,SoundexPreferredForename
		,SecondForename
		,UpperSecondForename
		,SoundexSecondForename
		,ThirdForename
		,UpperThirdForename
		,SoundexThirdForename
		,SecondEthnicCategoryID
		,ThirdEthnicCategoryID
		,PatientIdentifierSuffixID
		,DeathDateTimeEstimateFlag
		,NHSNumberTracingServiceTypeID
		,NationalityID
		,LastUpdateFromCFUDateTime
		,RestrictedAccessFlag
		,RestrictedVIPFlag
		,RestrictionStartDateTime
		,RestrictionEndDateTime
		,RestrictionAccessTypeID
		,DietaryRequirementsID
		,RestrictionRequestBy
		,RelationshipTypeID
		,RestrictionLevelID
		,MultilingualName
		,ResidentYesNoUnknownID
		,SundryDebtorFlag
		,PlaceOfDeathID
		,HowCauseOfDeathEstablishedID
		,CauseOfCancerRelatedDeathID
		,ImmediateCauseOfDeathID
		,ConditionLeadingToDeathID
		,UnderlyingConditionLeadingToDeathID
		,SignificantConditionNotLeadingToDeathID
		,CauseOfDeathDiscrepancyID
		,MergeMinorFlag
		,StaffFlag
		,PayeeOnlyFlag
		,SCNNumber
		,PDSUpdateNeeded
		,PDSReason
		,PDSStopNotedFlag
		,DeathNotificationStatusID
		,InterestedOrganisations
		,OwnerHealthOrganisationSubFlag
		,PreviousNHSContactDetailsID
		,PDSIdentifier
		,PDSDateOfBirth
		,ConsentStatusID
		,DementiaID
		,FrailtyID
		,FirstCareDateTime
		,DirectDebitTransactionCount
		,Suffix
		,SchoolHealthOrganisationID
		,EmploymentStatusID
		,OtherLanguageID
		,NumberOfDependents
		,PDSDateOfDeath
		,PatientConsentID
		,DeceasedRemoveFlag
		,PDSSupersededFlag
		,SupersedingNHSNumber
		,SexualOrientationID
		,SuitabilityIndicator
		,ReferralDateTime
		,StatutorySickPayIndicatorID
		,PreferredPlaceOfDeathID
		,OwnerHealthOrganisationID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		source.PatientID
		,source.SexID
		,source.Forename
		,source.Title
		,source.Surname
		,source.SearchSurname
		,source.EthnicCategoryID
		,source.MaritalStatusID
		,source.DateTimeOfBirth
		,source.OccupationID
		,source.SpokenLanguageID
		,source.AccessLevelID
		,source.SoundexForename
		,source.SoundexSurname
		,source.UpperForename
		,source.UpperSurname
		,source.ChronicallySickFlag
		,source.DisabledFlag
		,source.DeceasedFlag
		,source.ConfidentialFlag
		,source.DateTimeOfDeath
		,source.PlaceOfBirth
		,source.PlaceOfDeath
		,source.ReligionID
		,source.CountryID
		,source.IncomeSourceID
		,source.Occupation
		,source.FastTrackCode
		,source.CreateDateTime
		,source.ModifiedDateTime
		,source.UserCreate
		,source.UserModify
		,source.PASID
		,source.DateOfBirth
		,source.DateOfDeath
		,source.DeathNotifiedDateTime
		,source.Comments
		,source.SessionTransactionID
		,source.PriorPointerID
		,source.ExternalKey
		,source.TempPASID
		,source.BirthDateTimeEstimateFlag
		,source.InterpreterRequiredFlag
		,source.BloodID
		,source.LocalFlag
		,source.PCTIdealBodyWeight
		,source.ReasonForEstimationOfBirthDateTime
		,source.RegistrationDateTime
		,source.MultipleBirthIndicator
		,source.PrivateIdentifier
		,source.NHSIdentifier
		,source.LastSpellIdentifier
		,source.CasenoteStatusID
		,source.ApproximateDateOfDeathReasonID
		,source.CasenoteLocationTypeID
		,source.PreferredForename
		,source.UpperPreferredForename
		,source.SoundexPreferredForename
		,source.SecondForename
		,source.UpperSecondForename
		,source.SoundexSecondForename
		,source.ThirdForename
		,source.UpperThirdForename
		,source.SoundexThirdForename
		,source.SecondEthnicCategoryID
		,source.ThirdEthnicCategoryID
		,source.PatientIdentifierSuffixID
		,source.DeathDateTimeEstimateFlag
		,source.NHSNumberTracingServiceTypeID
		,source.NationalityID
		,source.LastUpdateFromCFUDateTime
		,source.RestrictedAccessFlag
		,source.RestrictedVIPFlag
		,source.RestrictionStartDateTime
		,source.RestrictionEndDateTime
		,source.RestrictionAccessTypeID
		,source.DietaryRequirementsID
		,source.RestrictionRequestBy
		,source.RelationshipTypeID
		,source.RestrictionLevelID
		,source.MultilingualName
		,source.ResidentYesNoUnknownID
		,source.SundryDebtorFlag
		,source.PlaceOfDeathID
		,source.HowCauseOfDeathEstablishedID
		,source.CauseOfCancerRelatedDeathID
		,source.ImmediateCauseOfDeathID
		,source.ConditionLeadingToDeathID
		,source.UnderlyingConditionLeadingToDeathID
		,source.SignificantConditionNotLeadingToDeathID
		,source.CauseOfDeathDiscrepancyID
		,source.MergeMinorFlag
		,source.StaffFlag
		,source.PayeeOnlyFlag
		,source.SCNNumber
		,source.PDSUpdateNeeded
		,source.PDSReason
		,source.PDSStopNotedFlag
		,source.DeathNotificationStatusID
		,source.InterestedOrganisations
		,source.OwnerHealthOrganisationSubFlag
		,source.PreviousNHSContactDetailsID
		,source.PDSIdentifier
		,source.PDSDateOfBirth
		,source.ConsentStatusID
		,source.DementiaID
		,source.FrailtyID
		,source.FirstCareDateTime
		,source.DirectDebitTransactionCount
		,source.Suffix
		,source.SchoolHealthOrganisationID
		,source.EmploymentStatusID
		,source.OtherLanguageID
		,source.NumberOfDependents
		,source.PDSDateOfDeath
		,source.PatientConsentID
		,source.DeceasedRemoveFlag
		,source.PDSSupersededFlag
		,source.SupersedingNHSNumber
		,source.SexualOrientationID
		,source.SuitabilityIndicator
		,source.ReferralDateTime
		,source.StatutorySickPayIndicatorID
		,source.PreferredPlaceOfDeathID
		,source.OwnerHealthOrganisationID
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.SexID
		,target.Forename
		,target.Title
		,target.Surname
		,target.SearchSurname
		,target.EthnicCategoryID
		,target.MaritalStatusID
		,target.DateTimeOfBirth
		,target.OccupationID
		,target.SpokenLanguageID
		,target.AccessLevelID
		,target.SoundexForename
		,target.SoundexSurname
		,target.UpperForename
		,target.UpperSurname
		,target.ChronicallySickFlag
		,target.DisabledFlag
		,target.DeceasedFlag
		,target.ConfidentialFlag
		,target.DateTimeOfDeath
		,target.PlaceOfBirth
		,target.PlaceOfDeath
		,target.ReligionID
		,target.CountryID
		,target.IncomeSourceID
		,target.Occupation
		,target.FastTrackCode
		,target.CreateDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.PASID
		,target.DateOfBirth
		,target.DateOfDeath
		,target.DeathNotifiedDateTime
		,target.Comments
		,target.SessionTransactionID
		,target.PriorPointerID
		,target.ExternalKey
		,target.TempPASID
		,target.BirthDateTimeEstimateFlag
		,target.InterpreterRequiredFlag
		,target.BloodID
		,target.LocalFlag
		,target.PCTIdealBodyWeight
		,target.ReasonForEstimationOfBirthDateTime
		,target.RegistrationDateTime
		,target.MultipleBirthIndicator
		,target.PrivateIdentifier
		,target.NHSIdentifier
		,target.LastSpellIdentifier
		,target.CasenoteStatusID
		,target.ApproximateDateOfDeathReasonID
		,target.CasenoteLocationTypeID
		,target.PreferredForename
		,target.UpperPreferredForename
		,target.SoundexPreferredForename
		,target.SecondForename
		,target.UpperSecondForename
		,target.SoundexSecondForename
		,target.ThirdForename
		,target.UpperThirdForename
		,target.SoundexThirdForename
		,target.SecondEthnicCategoryID
		,target.ThirdEthnicCategoryID
		,target.PatientIdentifierSuffixID
		,target.DeathDateTimeEstimateFlag
		,target.NHSNumberTracingServiceTypeID
		,target.NationalityID
		,target.LastUpdateFromCFUDateTime
		,target.RestrictedAccessFlag
		,target.RestrictedVIPFlag
		,target.RestrictionStartDateTime
		,target.RestrictionEndDateTime
		,target.RestrictionAccessTypeID
		,target.DietaryRequirementsID
		,target.RestrictionRequestBy
		,target.RelationshipTypeID
		,target.RestrictionLevelID
		,target.MultilingualName
		,target.ResidentYesNoUnknownID
		,target.SundryDebtorFlag
		,target.PlaceOfDeathID
		,target.HowCauseOfDeathEstablishedID
		,target.CauseOfCancerRelatedDeathID
		,target.ImmediateCauseOfDeathID
		,target.ConditionLeadingToDeathID
		,target.UnderlyingConditionLeadingToDeathID
		,target.SignificantConditionNotLeadingToDeathID
		,target.CauseOfDeathDiscrepancyID
		,target.MergeMinorFlag
		,target.StaffFlag
		,target.PayeeOnlyFlag
		,target.SCNNumber
		,target.PDSUpdateNeeded
		,target.PDSReason
		,target.PDSStopNotedFlag
		,target.DeathNotificationStatusID
		,target.InterestedOrganisations
		,target.OwnerHealthOrganisationSubFlag
		,target.PreviousNHSContactDetailsID
		,target.PDSIdentifier
		,target.PDSDateOfBirth
		,target.ConsentStatusID
		,target.DementiaID
		,target.FrailtyID
		,target.FirstCareDateTime
		,target.DirectDebitTransactionCount
		,target.Suffix
		,target.SchoolHealthOrganisationID
		,target.EmploymentStatusID
		,target.OtherLanguageID
		,target.NumberOfDependents
		,target.PDSDateOfDeath
		,target.PatientConsentID
		,target.DeceasedRemoveFlag
		,target.PDSSupersededFlag
		,target.SupersedingNHSNumber
		,target.SexualOrientationID
		,target.SuitabilityIndicator
		,target.ReferralDateTime
		,target.StatutorySickPayIndicatorID
		,target.PreferredPlaceOfDeathID
		,target.OwnerHealthOrganisationID
		,target.LorenzoCreated
		)

then
	update
	set
		target.SexID = source.SexID
		,target.Forename = source.Forename
		,target.Title = source.Title
		,target.Surname = source.Surname
		,target.SearchSurname = source.SearchSurname
		,target.EthnicCategoryID = source.EthnicCategoryID
		,target.MaritalStatusID = source.MaritalStatusID
		,target.DateTimeOfBirth = source.DateTimeOfBirth
		,target.OccupationID = source.OccupationID
		,target.SpokenLanguageID = source.SpokenLanguageID
		,target.AccessLevelID = source.AccessLevelID
		,target.SoundexForename = source.SoundexForename
		,target.SoundexSurname = source.SoundexSurname
		,target.UpperForename = source.UpperForename
		,target.UpperSurname = source.UpperSurname
		,target.ChronicallySickFlag = source.ChronicallySickFlag
		,target.DisabledFlag = source.DisabledFlag
		,target.DeceasedFlag = source.DeceasedFlag
		,target.ConfidentialFlag = source.ConfidentialFlag
		,target.DateTimeOfDeath = source.DateTimeOfDeath
		,target.PlaceOfBirth = source.PlaceOfBirth
		,target.PlaceOfDeath = source.PlaceOfDeath
		,target.ReligionID = source.ReligionID
		,target.CountryID = source.CountryID
		,target.IncomeSourceID = source.IncomeSourceID
		,target.Occupation = source.Occupation
		,target.FastTrackCode = source.FastTrackCode
		,target.CreateDateTime = source.CreateDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.PASID = source.PASID
		,target.DateOfBirth = source.DateOfBirth
		,target.DateOfDeath = source.DateOfDeath
		,target.DeathNotifiedDateTime = source.DeathNotifiedDateTime
		,target.Comments = source.Comments
		,target.SessionTransactionID = source.SessionTransactionID
		,target.PriorPointerID = source.PriorPointerID
		,target.ExternalKey = source.ExternalKey
		,target.TempPASID = source.TempPASID
		,target.BirthDateTimeEstimateFlag = source.BirthDateTimeEstimateFlag
		,target.InterpreterRequiredFlag = source.InterpreterRequiredFlag
		,target.BloodID = source.BloodID
		,target.LocalFlag = source.LocalFlag
		,target.PCTIdealBodyWeight = source.PCTIdealBodyWeight
		,target.ReasonForEstimationOfBirthDateTime = source.ReasonForEstimationOfBirthDateTime
		,target.RegistrationDateTime = source.RegistrationDateTime
		,target.MultipleBirthIndicator = source.MultipleBirthIndicator
		,target.PrivateIdentifier = source.PrivateIdentifier
		,target.NHSIdentifier = source.NHSIdentifier
		,target.LastSpellIdentifier = source.LastSpellIdentifier
		,target.CasenoteStatusID = source.CasenoteStatusID
		,target.ApproximateDateOfDeathReasonID = source.ApproximateDateOfDeathReasonID
		,target.CasenoteLocationTypeID = source.CasenoteLocationTypeID
		,target.PreferredForename = source.PreferredForename
		,target.UpperPreferredForename = source.UpperPreferredForename
		,target.SoundexPreferredForename = source.SoundexPreferredForename
		,target.SecondForename = source.SecondForename
		,target.UpperSecondForename = source.UpperSecondForename
		,target.SoundexSecondForename = source.SoundexSecondForename
		,target.ThirdForename = source.ThirdForename
		,target.UpperThirdForename = source.UpperThirdForename
		,target.SoundexThirdForename = source.SoundexThirdForename
		,target.SecondEthnicCategoryID = source.SecondEthnicCategoryID
		,target.ThirdEthnicCategoryID = source.ThirdEthnicCategoryID
		,target.PatientIdentifierSuffixID = source.PatientIdentifierSuffixID
		,target.DeathDateTimeEstimateFlag = source.DeathDateTimeEstimateFlag
		,target.NHSNumberTracingServiceTypeID = source.NHSNumberTracingServiceTypeID
		,target.NationalityID = source.NationalityID
		,target.LastUpdateFromCFUDateTime = source.LastUpdateFromCFUDateTime
		,target.RestrictedAccessFlag = source.RestrictedAccessFlag
		,target.RestrictedVIPFlag = source.RestrictedVIPFlag
		,target.RestrictionStartDateTime = source.RestrictionStartDateTime
		,target.RestrictionEndDateTime = source.RestrictionEndDateTime
		,target.RestrictionAccessTypeID = source.RestrictionAccessTypeID
		,target.DietaryRequirementsID = source.DietaryRequirementsID
		,target.RestrictionRequestBy = source.RestrictionRequestBy
		,target.RelationshipTypeID = source.RelationshipTypeID
		,target.RestrictionLevelID = source.RestrictionLevelID
		,target.MultilingualName = source.MultilingualName
		,target.ResidentYesNoUnknownID = source.ResidentYesNoUnknownID
		,target.SundryDebtorFlag = source.SundryDebtorFlag
		,target.PlaceOfDeathID = source.PlaceOfDeathID
		,target.HowCauseOfDeathEstablishedID = source.HowCauseOfDeathEstablishedID
		,target.CauseOfCancerRelatedDeathID = source.CauseOfCancerRelatedDeathID
		,target.ImmediateCauseOfDeathID = source.ImmediateCauseOfDeathID
		,target.ConditionLeadingToDeathID = source.ConditionLeadingToDeathID
		,target.UnderlyingConditionLeadingToDeathID = source.UnderlyingConditionLeadingToDeathID
		,target.SignificantConditionNotLeadingToDeathID = source.SignificantConditionNotLeadingToDeathID
		,target.CauseOfDeathDiscrepancyID = source.CauseOfDeathDiscrepancyID
		,target.MergeMinorFlag = source.MergeMinorFlag
		,target.StaffFlag = source.StaffFlag
		,target.PayeeOnlyFlag = source.PayeeOnlyFlag
		,target.SCNNumber = source.SCNNumber
		,target.PDSUpdateNeeded = source.PDSUpdateNeeded
		,target.PDSReason = source.PDSReason
		,target.PDSStopNotedFlag = source.PDSStopNotedFlag
		,target.DeathNotificationStatusID = source.DeathNotificationStatusID
		,target.InterestedOrganisations = source.InterestedOrganisations
		,target.OwnerHealthOrganisationSubFlag = source.OwnerHealthOrganisationSubFlag
		,target.PreviousNHSContactDetailsID = source.PreviousNHSContactDetailsID
		,target.PDSIdentifier = source.PDSIdentifier
		,target.PDSDateOfBirth = source.PDSDateOfBirth
		,target.ConsentStatusID = source.ConsentStatusID
		,target.DementiaID = source.DementiaID
		,target.FrailtyID = source.FrailtyID
		,target.FirstCareDateTime = source.FirstCareDateTime
		,target.DirectDebitTransactionCount = source.DirectDebitTransactionCount
		,target.Suffix = source.Suffix
		,target.SchoolHealthOrganisationID = source.SchoolHealthOrganisationID
		,target.EmploymentStatusID = source.EmploymentStatusID
		,target.OtherLanguageID = source.OtherLanguageID
		,target.NumberOfDependents = source.NumberOfDependents
		,target.PDSDateOfDeath = source.PDSDateOfDeath
		,target.PatientConsentID = source.PatientConsentID
		,target.DeceasedRemoveFlag = source.DeceasedRemoveFlag
		,target.PDSSupersededFlag = source.PDSSupersededFlag
		,target.SupersedingNHSNumber = source.SupersedingNHSNumber
		,target.SexualOrientationID = source.SexualOrientationID
		,target.SuitabilityIndicator = source.SuitabilityIndicator
		,target.ReferralDateTime = source.ReferralDateTime
		,target.StatutorySickPayIndicatorID = source.StatutorySickPayIndicatorID
		,target.PreferredPlaceOfDeathID = source.PreferredPlaceOfDeathID
		,target.OwnerHealthOrganisationID = source.OwnerHealthOrganisationID
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime