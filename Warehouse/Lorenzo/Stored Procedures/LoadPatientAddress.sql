﻿CREATE procedure [Lorenzo].[LoadPatientAddress] as 


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(PatientAddress.LorenzoCreated)
		from
			Lorenzo.PatientAddress
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			AddressTypeID
			,Line1
			,Line2
			,Line3
			,Line4
			,Postcode
			,LocalAuthorityCode
			,HealthDistrictCode
			,ElectoralWardCode
			,County
			,CountryID
			,StartDateTime
			,EndDateTime
			,CreatedDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,SessionTransactionID
			,PriorPointerID
			,ExternalKey
			,CountyID
			,CommunityID
			,ParishID
			,PurchaserAreaID
			,PrimaryCareAreaID
			,ResRegisteredDateTime
			,Suburb
			,StateCode
			,PatientAccomodationID
			,PCGCode
			,TeamAreaID
			,CommunityCareAreaID
			,CommunitySectorID
			,TownlandsID
			,SynchronisationCode
			,DeliveryPointID
			,QASBarcode
			,PAFKey
			,QASVerified
			,PAFAddKey
			,PAFInvalidDate
			,HealthOrganisationOwnerID
			,LorenzoCreated
			)
into
	#TLoadPASPatientAddress
from
	(
	select
		PatientAddressID
		,AddressTypeID = cast(nullif(AddressTypeID, '') as varchar(5))
		,Line1 = cast(nullif(Line1, '') as varchar(50))
		,Line2 = cast(nullif(Line2, '') as varchar(50))
		,Line3 = cast(nullif(Line3, '') as varchar(50))
		,Line4 = cast(nullif(Line4, '') as varchar(50))
		,Postcode = cast(nullif(Postcode, '') as varchar(25))
		,LocalAuthorityCode = cast(nullif(LocalAuthorityCode, '') as varchar(10))
		,HealthDistrictCode = cast(nullif(HealthDistrictCode, '') as varchar(10))
		,ElectoralWardCode = cast(nullif(ElectoralWardCode, '') as varchar(10))
		,County = cast(nullif(County, '') as varchar(30))
		,CountryID = cast(CountryID as int)
		,StartDateTime = cast(StartDateTime as datetime)
		,EndDateTime = cast(EndDateTime as smalldatetime)
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(30))
		,UserModify = cast(nullif(UserModify, '') as varchar(30))
		,SessionTransactionID = cast(SessionTransactionID as int)
		,PriorPointerID = cast(PriorPointerID as int)
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(20))
		,CountyID = cast(CountyID as int)
		,CommunityID = cast(CommunityID as int)
		,ParishID = cast(ParishID as int)
		,PurchaserAreaID = cast(PurchaserAreaID as int)
		,PrimaryCareAreaID = cast(PrimaryCareAreaID as int)
		,ResRegisteredDateTime = cast(ResRegisteredDateTime as smalldatetime)
		,Suburb = cast(nullif(Suburb, '') as varchar(40))
		,StateCode = cast(nullif(StateCode, '') as varchar(5))
		,PatientAccomodationID = cast(PatientAccomodationID as int)
		,PCGCode = cast(nullif(PCGCode, '') as varchar(10))
		,TeamAreaID = cast(TeamAreaID as int)
		,CommunityCareAreaID = cast(CommunityCareAreaID as int)
		,CommunitySectorID = cast(CommunitySectorID as int)
		,TownlandsID = cast(TownlandsID as int)
		,SynchronisationCode = cast(nullif(SynchronisationCode, '') as varchar(20))
		,DeliveryPointID = cast(DeliveryPointID as int)
		,QASBarcode = cast(nullif(QASBarcode, '') as varchar(255))
		,PAFKey = cast(PAFKey as int)
		,QASVerified = cast(nullif(QASVerified, '') as char(1))
		,PAFAddKey = cast(nullif(PAFAddKey, '') as varchar(25))
		,PAFInvalidDate = cast(PAFInvalidDate as datetime)
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as int)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			PatientAddressID = ADDSS_REFNO
			,AddressTypeID = ADTYP_CODE
			,Line1 = LINE1
			,Line2 = LINE2
			,Line3 = LINE3
			,Line4 = LINE4
			,Postcode = PCODE
			,LocalAuthorityCode = LOCAT_CODE
			,HealthDistrictCode = HDIST_CODE
			,ElectoralWardCode = ELECT_CODE
			,County = COUNTY
			,CountryID = CNTRY_REFNO
			,StartDateTime = START_DTTM
			,EndDateTime = END_DTTM
			,CreatedDateTime = CREATE_DTTM
			,ModifiedDateTime = MODIF_DTTM
			,UserCreate = USER_CREATE
			,UserModify = USER_MODIF
			,SessionTransactionID = STRAN_REFNO
			,PriorPointerID = PRIOR_POINTER
			,ExternalKey = EXTERNAL_KEY
			,CountyID = COUNT_OTCOD_REFNO
			,CommunityID = COMMU_OTCOD_REFNO
			,ParishID = PARSH_OTCOD_REFNO
			,PurchaserAreaID = PUARE_OTCOD_REFNO
			,PrimaryCareAreaID = PCARE_OTCOD_REFNO
			,ResRegisteredDateTime = RES_REG_DTTM
			,Suburb = SUBURB
			,StateCode = STATE_CODE
			,PatientAccomodationID = MPIAS_REFNO
			,PCGCode = PCG_CODE
			,TeamAreaID = TEAMA_REFNO
			,CommunityCareAreaID = CMMCA_REFNO
			,CommunitySectorID = CMMSC_REFNO
			,TownlandsID = TLAND_REFNO
			,SynchronisationCode = SYN_CODE
			,DeliveryPointID = DEL_POINT_ID
			,QASBarcode = QAS_BARCODE
			,PAFKey = PAFKey
			,QASVerified = QAS_VERIFIED
			,PAFAddKey = PAF_ADD_KEY
			,PAFInvalidDate = PAF_INVALID_DATE
			,HealthOrganisationOwnerID = OWNER_HEORG_REFNO
			,LorenzoCreated = Created

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.PatientAddress
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASPatientAddress on #TLoadPASPatientAddress
	(
	PatientAddressID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.PatientAddress target
using
	(
	select
		*
	from
		#TLoadPASPatientAddress
	
	) source
	on	source.PatientAddressID = target.PatientAddressID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		PatientAddressID
		,AddressTypeID
		,Line1
		,Line2
		,Line3
		,Line4
		,Postcode
		,LocalAuthorityCode
		,HealthDistrictCode
		,ElectoralWardCode
		,County
		,CountryID
		,StartDateTime
		,EndDateTime
		,CreatedDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,SessionTransactionID
		,PriorPointerID
		,ExternalKey
		,CountyID
		,CommunityID
		,ParishID
		,PurchaserAreaID
		,PrimaryCareAreaID
		,ResRegisteredDateTime
		,Suburb
		,StateCode
		,PatientAccomodationID
		,PCGCode
		,TeamAreaID
		,CommunityCareAreaID
		,CommunitySectorID
		,TownlandsID
		,SynchronisationCode
		,DeliveryPointID
		,QASBarcode
		,PAFKey
		,QASVerified
		,PAFAddKey
		,PAFInvalidDate
		,HealthOrganisationOwnerID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		source.PatientAddressID
		,source.AddressTypeID
		,source.Line1
		,source.Line2
		,source.Line3
		,source.Line4
		,source.Postcode
		,source.LocalAuthorityCode
		,source.HealthDistrictCode
		,source.ElectoralWardCode
		,source.County
		,source.CountryID
		,source.StartDateTime
		,source.EndDateTime
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.UserCreate
		,source.UserModify
		,source.SessionTransactionID
		,source.PriorPointerID
		,source.ExternalKey
		,source.CountyID
		,source.CommunityID
		,source.ParishID
		,source.PurchaserAreaID
		,source.PrimaryCareAreaID
		,source.ResRegisteredDateTime
		,source.Suburb
		,source.StateCode
		,source.PatientAccomodationID
		,source.PCGCode
		,source.TeamAreaID
		,source.CommunityCareAreaID
		,source.CommunitySectorID
		,source.TownlandsID
		,source.SynchronisationCode
		,source.DeliveryPointID
		,source.QASBarcode
		,source.PAFKey
		,source.QASVerified
		,source.PAFAddKey
		,source.PAFInvalidDate
		,source.HealthOrganisationOwnerID
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.AddressTypeID
		,target.Line1
		,target.Line2
		,target.Line3
		,target.Line4
		,target.Postcode
		,target.LocalAuthorityCode
		,target.HealthDistrictCode
		,target.ElectoralWardCode
		,target.County
		,target.CountryID
		,target.StartDateTime
		,target.EndDateTime
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.SessionTransactionID
		,target.PriorPointerID
		,target.ExternalKey
		,target.CountyID
		,target.CommunityID
		,target.ParishID
		,target.PurchaserAreaID
		,target.PrimaryCareAreaID
		,target.ResRegisteredDateTime
		,target.Suburb
		,target.StateCode
		,target.PatientAccomodationID
		,target.PCGCode
		,target.TeamAreaID
		,target.CommunityCareAreaID
		,target.CommunitySectorID
		,target.TownlandsID
		,target.SynchronisationCode
		,target.DeliveryPointID
		,target.QASBarcode
		,target.PAFKey
		,target.QASVerified
		,target.PAFAddKey
		,target.PAFInvalidDate
		,target.HealthOrganisationOwnerID
		,target.LorenzoCreated
		)

then
	update
	set
		target.AddressTypeID = source.AddressTypeID
		,target.Line1 = source.Line1
		,target.Line2 = source.Line2
		,target.Line3 = source.Line3
		,target.Line4 = source.Line4
		,target.Postcode = source.Postcode
		,target.LocalAuthorityCode = source.LocalAuthorityCode
		,target.HealthDistrictCode = source.HealthDistrictCode
		,target.ElectoralWardCode = source.ElectoralWardCode
		,target.County = source.County
		,target.CountryID = source.CountryID
		,target.StartDateTime = source.StartDateTime
		,target.EndDateTime = source.EndDateTime
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.SessionTransactionID = source.SessionTransactionID
		,target.PriorPointerID = source.PriorPointerID
		,target.ExternalKey = source.ExternalKey
		,target.CountyID = source.CountyID
		,target.CommunityID = source.CommunityID
		,target.ParishID = source.ParishID
		,target.PurchaserAreaID = source.PurchaserAreaID
		,target.PrimaryCareAreaID = source.PrimaryCareAreaID
		,target.ResRegisteredDateTime = source.ResRegisteredDateTime
		,target.Suburb = source.Suburb
		,target.StateCode = source.StateCode
		,target.PatientAccomodationID = source.PatientAccomodationID
		,target.PCGCode = source.PCGCode
		,target.TeamAreaID = source.TeamAreaID
		,target.CommunityCareAreaID = source.CommunityCareAreaID
		,target.CommunitySectorID = source.CommunitySectorID
		,target.TownlandsID = source.TownlandsID
		,target.SynchronisationCode = source.SynchronisationCode
		,target.DeliveryPointID = source.DeliveryPointID
		,target.QASBarcode = source.QASBarcode
		,target.PAFKey = source.PAFKey
		,target.QASVerified = source.QASVerified
		,target.PAFAddKey = source.PAFAddKey
		,target.PAFInvalidDate = source.PAFInvalidDate
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime