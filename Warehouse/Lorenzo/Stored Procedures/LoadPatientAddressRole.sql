﻿CREATE procedure [Lorenzo].[LoadPatientAddressRole] as 


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(PatientAddressRole.LorenzoCreated)
		from
			Lorenzo.PatientAddressRole
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			ProfessionalCarerID
			,PatientID
			,PersonalCarerID
			,HealthOrganisationID
			,AddressID
			,RollTypeCode
			,IsForCorrespondence
			,StartDateTime
			,EndDateTime
			,PatientTransportationAddressID
			,IsCurrent
			,HomeLeaveID
			,PurchaserID
			,ProviderID
			,SecureFlag
			,PatientPersonalCarerID
			,PersonID
			,OrderID
			,CreatedDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,SessionTransactionID
			,PriorPointerID
			,ExternalKey
			,BookingFormID
			,SynchronisationCode
			,PDSIdentifier
			,ReferralID
			,DecoupleFlag
			,HealthOrganisationOwnerID
			,LorenzoCreated
			)
into
	#TLoadPASPatientAddressRole
from
	(
	select
		PatientAddressRoleID
		,ProfessionalCarerID = cast(ProfessionalCarerID as int)
		,PatientID = cast(PatientID as int)
		,PersonalCarerID = cast(PersonalCarerID as int)
		,HealthOrganisationID = cast(HealthOrganisationID as int)
		,AddressID = cast(AddressID as int)
		,RollTypeCode = cast(nullif(RollTypeCode, '') as varchar(25))
		,IsForCorrespondence = cast(nullif(IsForCorrespondence, '') as char(1))
		,StartDateTime = cast(StartDateTime as datetime)
		,EndDateTime = cast(EndDateTime as datetime)
		,PatientTransportationAddressID = cast(PatientTransportationAddressID as int)
		,IsCurrent = cast(nullif(IsCurrent, '') as varchar(1))
		,HomeLeaveID = cast(HomeLeaveID as int)
		,PurchaserID = cast(PurchaserID as int)
		,ProviderID = cast(ProviderID as int)
		,SecureFlag = cast(nullif(SecureFlag, '') as char(1))
		,PatientPersonalCarerID = cast(PatientPersonalCarerID as int)
		,PersonID = cast(PersonID as int)
		,OrderID = cast(OrderID as int)
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(50))
		,UserModify = cast(nullif(UserModify, '') as varchar(50))
		,SessionTransactionID = cast(SessionTransactionID as int)
		,PriorPointerID = cast(PriorPointerID as int)
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(50))
		,BookingFormID = cast(BookingFormID as int)
		,SynchronisationCode = cast(nullif(SynchronisationCode, '') as varchar(50))
		,PDSIdentifier = cast(nullif(PDSIdentifier, '') as varchar(50))
		,ReferralID = cast(ReferralID as int)
		,DecoupleFlag = cast(nullif(DecoupleFlag, '') as char(1))
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as int)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			PatientAddressRoleID = ROLES_REFNO
			,ProfessionalCarerID = PROCA_REFNO
			,PatientID = PATNT_REFNO
			,PersonalCarerID = PERCA_REFNO
			,HealthOrganisationID = HEORG_REFNO
			,AddressID = ADDSS_REFNO
			,RollTypeCode = ROTYP_CODE
			,IsForCorrespondence = CORRESPONDENCE
			,StartDateTime = START_DTTM
			,EndDateTime = END_DTTM
			,PatientTransportationAddressID = DEPRT_PATRN_REFNO
			,IsCurrent = CURNT_FLAG
			,HomeLeaveID = HOMEL_REFNO
			,PurchaserID = PURCH_REFNO
			,ProviderID = PROVD_REFNO
			,SecureFlag = SECURE_FLAG
			,PatientPersonalCarerID = PATPC_REFNO
			,PersonID = PERSS_REFNO
			,OrderID = ORDRR_REFNO
			,CreatedDateTime = CREATE_DTTM
			,ModifiedDateTime = MODIF_DTTM
			,UserCreate = USER_CREATE
			,UserModify = USER_MODIF
			,SessionTransactionID = STRAN_REFNO
			,PriorPointerID = PRIOR_POINTER
			,ExternalKey = EXTERNAL_KEY
			,BookingFormID = BFORM_REFNO
			,SynchronisationCode = SYN_CODE
			,PDSIdentifier = PDS_IDENTIFIER
			,ReferralID = REFRL_REFNO
			,DecoupleFlag = DECOUPLE_FLAG
			,HealthOrganisationOwnerID = OWNER_HEORG_REFNO
			,LorenzoCreated = Created

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.PatientAddressRole
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASPatientAddressRole on #TLoadPASPatientAddressRole
	(
	PatientAddressRoleID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.PatientAddressRole target
using
	(
	select
		*
	from
		#TLoadPASPatientAddressRole
	
	) source
	on	source.PatientAddressRoleID = target.PatientAddressRoleID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		PatientAddressRoleID
		,ProfessionalCarerID
		,PatientID
		,PersonalCarerID
		,HealthOrganisationID
		,AddressID
		,RollTypeCode
		,IsForCorrespondence
		,StartDateTime
		,EndDateTime
		,PatientTransportationAddressID
		,IsCurrent
		,HomeLeaveID
		,PurchaserID
		,ProviderID
		,SecureFlag
		,PatientPersonalCarerID
		,PersonID
		,OrderID
		,CreatedDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,SessionTransactionID
		,PriorPointerID
		,ExternalKey
		,BookingFormID
		,SynchronisationCode
		,PDSIdentifier
		,ReferralID
		,DecoupleFlag
		,HealthOrganisationOwnerID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		source.PatientAddressRoleID
		,source.ProfessionalCarerID
		,source.PatientID
		,source.PersonalCarerID
		,source.HealthOrganisationID
		,source.AddressID
		,source.RollTypeCode
		,source.IsForCorrespondence
		,source.StartDateTime
		,source.EndDateTime
		,source.PatientTransportationAddressID
		,source.IsCurrent
		,source.HomeLeaveID
		,source.PurchaserID
		,source.ProviderID
		,source.SecureFlag
		,source.PatientPersonalCarerID
		,source.PersonID
		,source.OrderID
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.UserCreate
		,source.UserModify
		,source.SessionTransactionID
		,source.PriorPointerID
		,source.ExternalKey
		,source.BookingFormID
		,source.SynchronisationCode
		,source.PDSIdentifier
		,source.ReferralID
		,source.DecoupleFlag
		,source.HealthOrganisationOwnerID
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.ProfessionalCarerID
		,target.PatientID
		,target.PersonalCarerID
		,target.HealthOrganisationID
		,target.AddressID
		,target.RollTypeCode
		,target.IsForCorrespondence
		,target.StartDateTime
		,target.EndDateTime
		,target.PatientTransportationAddressID
		,target.IsCurrent
		,target.HomeLeaveID
		,target.PurchaserID
		,target.ProviderID
		,target.SecureFlag
		,target.PatientPersonalCarerID
		,target.PersonID
		,target.OrderID
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.SessionTransactionID
		,target.PriorPointerID
		,target.ExternalKey
		,target.BookingFormID
		,target.SynchronisationCode
		,target.PDSIdentifier
		,target.ReferralID
		,target.DecoupleFlag
		,target.HealthOrganisationOwnerID
		,target.LorenzoCreated
		)

then
	update
	set
		target.PatientAddressRoleID = source.PatientAddressRoleID
		,target.ProfessionalCarerID = source.ProfessionalCarerID
		,target.PatientID = source.PatientID
		,target.PersonalCarerID = source.PersonalCarerID
		,target.HealthOrganisationID = source.HealthOrganisationID
		,target.AddressID = source.AddressID
		,target.RollTypeCode = source.RollTypeCode
		,target.IsForCorrespondence = source.IsForCorrespondence
		,target.StartDateTime = source.StartDateTime
		,target.EndDateTime = source.EndDateTime
		,target.PatientTransportationAddressID = source.PatientTransportationAddressID
		,target.IsCurrent = source.IsCurrent
		,target.HomeLeaveID = source.HomeLeaveID
		,target.PurchaserID = source.PurchaserID
		,target.ProviderID = source.ProviderID
		,target.SecureFlag = source.SecureFlag
		,target.PatientPersonalCarerID = source.PatientPersonalCarerID
		,target.PersonID = source.PersonID
		,target.OrderID = source.OrderID
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.SessionTransactionID = source.SessionTransactionID
		,target.PriorPointerID = source.PriorPointerID
		,target.ExternalKey = source.ExternalKey
		,target.BookingFormID = source.BookingFormID
		,target.SynchronisationCode = source.SynchronisationCode
		,target.PDSIdentifier = source.PDSIdentifier
		,target.ReferralID = source.ReferralID
		,target.DecoupleFlag = source.DecoupleFlag
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime