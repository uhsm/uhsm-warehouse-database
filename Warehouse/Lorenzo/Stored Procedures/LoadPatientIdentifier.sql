﻿CREATE procedure [Lorenzo].[LoadPatientIdentifier] as 


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(PatientIdentifier.LorenzoCreated)
		from
			Lorenzo.PatientIdentifier
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			PatientIdentifierTypeID
			,PatientID
			,PatientIdentifier
			,RDProjectID
			,EndDateTime
			,StartDateTime
			,IsCurrent
			,PatientIdentifierStatusID
			,PatientServiceRankID
			,CreatedDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,SessionTransactionID
			,PriorPointerID
			,ExternalKey
			,PatientIdentifierSuffixID
			,HealthOrganisationID
			,MedicalRecordsHealthOrganisationID
			,HealthOrganisationOwnerID
			,LorenzoCreated
			)
into
	#TLoadPASPatientIdentifier
from
	(
	select
		PatientIdentifierID
		,PatientIdentifierTypeID = cast(PatientIdentifierTypeID as int)
		,PatientID = cast(PatientID as int)
		,PatientIdentifier = cast(nullif(PatientIdentifier, '') as varchar(20))
		,RDProjectID = cast(RDProjectID as int)
		,EndDateTime = cast(EndDateTime as datetime)
		,StartDateTime = cast(StartDateTime as datetime)
		,IsCurrent = cast(nullif(IsCurrent, '') as char(1))
		,PatientIdentifierStatusID = cast(PatientIdentifierStatusID as int)
		,PatientServiceRankID = cast(PatientServiceRankID as int)
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(30))
		,UserModify = cast(nullif(UserModify, '') as varchar(30))
		,SessionTransactionID = cast(SessionTransactionID as int)
		,PriorPointerID = cast(PriorPointerID as int)
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(20))
		,PatientIdentifierSuffixID = cast(PatientIdentifierSuffixID as int)
		,HealthOrganisationID = cast(HealthOrganisationID as int)
		,MedicalRecordsHealthOrganisationID = cast(MedicalRecordsHealthOrganisationID as int)
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as int)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			PatientIdentifierID = PATID_REFNO
			,PatientIdentifierTypeID = PITYP_REFNO
			,PatientID = PATNT_REFNO
			,PatientIdentifier = IDENTIFIER
			,RDProjectID = RDPRJ_REFNO
			,EndDateTime = END_DTTM
			,StartDateTime = START_DTTM
			,IsCurrent = CURNT_FLAG
			,PatientIdentifierStatusID = PISTS_REFNO
			,PatientServiceRankID = SRANK_REFNO
			,CreatedDateTime = CREATE_DTTM
			,ModifiedDateTime = MODIF_DTTM
			,UserCreate = USER_CREATE
			,UserModify = USER_MODIF
			,SessionTransactionID = STRAN_REFNO
			,PriorPointerID = PRIOR_POINTER
			,ExternalKey = EXTERNAL_KEY
			,PatientIdentifierSuffixID = IDSUF_REFNO
			,HealthOrganisationID = HEORG_REFNO
			,MedicalRecordsHealthOrganisationID = MRD_HEORG_REFNO
			,HealthOrganisationOwnerID = OWNER_HEORG_REFNO
			,LorenzoCreated = Created

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.PatientIdentifier
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASPatientIdentifier on #TLoadPASPatientIdentifier
	(
	PatientIdentifierID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.PatientIdentifier target
using
	(
	select
		*
	from
		#TLoadPASPatientIdentifier
	
	) source
	on	source.PatientIdentifierID = target.PatientIdentifierID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		PatientIdentifierID
		,PatientIdentifierTypeID
		,PatientID
		,PatientIdentifier
		,RDProjectID
		,EndDateTime
		,StartDateTime
		,IsCurrent
		,PatientIdentifierStatusID
		,PatientServiceRankID
		,CreatedDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,SessionTransactionID
		,PriorPointerID
		,ExternalKey
		,PatientIdentifierSuffixID
		,HealthOrganisationID
		,MedicalRecordsHealthOrganisationID
		,HealthOrganisationOwnerID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		source.PatientIdentifierID
		,source.PatientIdentifierTypeID
		,source.PatientID
		,source.PatientIdentifier
		,source.RDProjectID
		,source.EndDateTime
		,source.StartDateTime
		,source.IsCurrent
		,source.PatientIdentifierStatusID
		,source.PatientServiceRankID
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.UserCreate
		,source.UserModify
		,source.SessionTransactionID
		,source.PriorPointerID
		,source.ExternalKey
		,source.PatientIdentifierSuffixID
		,source.HealthOrganisationID
		,source.MedicalRecordsHealthOrganisationID
		,source.HealthOrganisationOwnerID
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.PatientIdentifierTypeID
		,target.PatientID
		,target.PatientIdentifier
		,target.RDProjectID
		,target.EndDateTime
		,target.StartDateTime
		,target.IsCurrent
		,target.PatientIdentifierStatusID
		,target.PatientServiceRankID
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.SessionTransactionID
		,target.PriorPointerID
		,target.ExternalKey
		,target.PatientIdentifierSuffixID
		,target.HealthOrganisationID
		,target.MedicalRecordsHealthOrganisationID
		,target.HealthOrganisationOwnerID
		,target.LorenzoCreated
		)

then
	update
	set
		target.PatientIdentifierTypeID = source.PatientIdentifierTypeID
		,target.PatientID = source.PatientID
		,target.PatientIdentifier = source.PatientIdentifier
		,target.RDProjectID = source.RDProjectID
		,target.EndDateTime = source.EndDateTime
		,target.StartDateTime = source.StartDateTime
		,target.IsCurrent = source.IsCurrent
		,target.PatientIdentifierStatusID = source.PatientIdentifierStatusID
		,target.PatientServiceRankID = source.PatientServiceRankID
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.SessionTransactionID = source.SessionTransactionID
		,target.PriorPointerID = source.PriorPointerID
		,target.ExternalKey = source.ExternalKey
		,target.PatientIdentifierSuffixID = source.PatientIdentifierSuffixID
		,target.HealthOrganisationID = source.HealthOrganisationID
		,target.MedicalRecordsHealthOrganisationID = source.MedicalRecordsHealthOrganisationID
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime