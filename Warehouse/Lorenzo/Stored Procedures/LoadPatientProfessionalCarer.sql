﻿CREATE procedure [Lorenzo].[LoadPatientProfessionalCarer] as 


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(PatientProfessionalCarer.LorenzoCreated)
		from
			Lorenzo.PatientProfessionalCarer
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			ProfessionalCarerTypeID
			,ProfessionalCarerTypeMainCode
			,ProfessionalCarerType
			,ProfessionalCarerID
			,ProfessionalCarerMainIdentifier
			,PatientID
			,SystemPatientIdentifier
			,NHSNumber
			,HealthOrganisationID
			,HealthOrganisationMainIdentifier
			,StartDateTime
			,EndDateTime
			,CreatedDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,LorenzoCreated
			)
into
	#TLoadPASPatientProfessionalCarer
from
	(
	select
		PatientProfessionalCarerID
		,ProfessionalCarerTypeID = cast(ProfessionalCarerTypeID as int)
		,ProfessionalCarerTypeMainCode = cast(nullif(ProfessionalCarerTypeMainCode, '') as varchar(25))
		,ProfessionalCarerType = cast(nullif(ProfessionalCarerType, '') as varchar(80))
		,ProfessionalCarerID = cast(ProfessionalCarerID as int)
		,ProfessionalCarerMainIdentifier = cast(nullif(ProfessionalCarerMainIdentifier, '') as varchar(25))
		,PatientID = cast(PatientID as int)
		,SystemPatientIdentifier = cast(nullif(SystemPatientIdentifier, '') as varchar(20))
		,NHSNumber = cast(nullif(NHSNumber, '') as varchar(20))
		,HealthOrganisationID = cast(HealthOrganisationID as int)
		,HealthOrganisationMainIdentifier = cast(nullif(HealthOrganisationMainIdentifier, '') as varchar(25))
		,StartDateTime = cast(StartDateTime as datetime)
		,EndDateTime = cast(EndDateTime as datetime)
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(30))
		,UserModify = cast(nullif(UserModify, '') as varchar(30))
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			PatientProfessionalCarerID = PATPC_REFNO
			,ProfessionalCarerTypeID = PRTYP_REFNO
			,ProfessionalCarerTypeMainCode = PRTYP_MAIN_CODE
			,ProfessionalCarerType = PRTYP_DESCRIPTION
			,ProfessionalCarerID = PROCA_REFNO
			,ProfessionalCarerMainIdentifier = PROCA_MAIN_IDENT
			,PatientID = PATNT_REFNO
			,SystemPatientIdentifier = PATNT_PASID
			,NHSNumber = PATNT_NHS_IDENTIFIER
			,HealthOrganisationID = HEORG_REFNO
			,HealthOrganisationMainIdentifier = HEORG_MAIN_IDENT
			,StartDateTime = START_DTTM
			,EndDateTime = END_DTTM
			,CreatedDateTime = CREATE_DTTM
			,ModifiedDateTime = MODIF_DTTM
			,UserCreate = USER_CREATE
			,UserModify = USER_MODIF
			,LorenzoCreated = Created

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.PatientProfessionalCarer
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASPatientProfessionalCarer on #TLoadPASPatientProfessionalCarer
	(
	PatientProfessionalCarerID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.PatientProfessionalCarer target
using
	(
	select
		*
	from
		#TLoadPASPatientProfessionalCarer
	
	) source
	on	source.PatientProfessionalCarerID = target.PatientProfessionalCarerID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		PatientProfessionalCarerID
		,ProfessionalCarerTypeID
		,ProfessionalCarerTypeMainCode
		,ProfessionalCarerType
		,ProfessionalCarerID
		,ProfessionalCarerMainIdentifier
		,PatientID
		,SystemPatientIdentifier
		,NHSNumber
		,HealthOrganisationID
		,HealthOrganisationMainIdentifier
		,StartDateTime
		,EndDateTime
		,CreatedDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		source.PatientProfessionalCarerID
		,source.ProfessionalCarerTypeID
		,source.ProfessionalCarerTypeMainCode
		,source.ProfessionalCarerType
		,source.ProfessionalCarerID
		,source.ProfessionalCarerMainIdentifier
		,source.PatientID
		,source.SystemPatientIdentifier
		,source.NHSNumber
		,source.HealthOrganisationID
		,source.HealthOrganisationMainIdentifier
		,source.StartDateTime
		,source.EndDateTime
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.UserCreate
		,source.UserModify
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.ProfessionalCarerTypeID
		,target.ProfessionalCarerTypeMainCode
		,target.ProfessionalCarerType
		,target.ProfessionalCarerID
		,target.ProfessionalCarerMainIdentifier
		,target.PatientID
		,target.SystemPatientIdentifier
		,target.NHSNumber
		,target.HealthOrganisationID
		,target.HealthOrganisationMainIdentifier
		,target.StartDateTime
		,target.EndDateTime
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.LorenzoCreated
		)

then
	update
	set
		target.ProfessionalCarerTypeID = source.ProfessionalCarerTypeID
		,target.ProfessionalCarerTypeMainCode = source.ProfessionalCarerTypeMainCode
		,target.ProfessionalCarerType = source.ProfessionalCarerType
		,target.ProfessionalCarerID = source.ProfessionalCarerID
		,target.ProfessionalCarerMainIdentifier = source.ProfessionalCarerMainIdentifier
		,target.PatientID = source.PatientID
		,target.SystemPatientIdentifier = source.SystemPatientIdentifier
		,target.NHSNumber = source.NHSNumber
		,target.HealthOrganisationID = source.HealthOrganisationID
		,target.HealthOrganisationMainIdentifier = source.HealthOrganisationMainIdentifier
		,target.StartDateTime = source.StartDateTime
		,target.EndDateTime = source.EndDateTime
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime