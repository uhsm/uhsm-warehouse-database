﻿CREATE procedure [Lorenzo].[LoadProfessionalCareEpisode] as 


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(ProfessionalCareEpisode.LorenzoCreated)
		from
			Lorenzo.ProfessionalCareEpisode
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			WaitingListID
			,ServicePointStayID
			,ProviderSpellID
			,ServicePointStayServicePointID
			,ProviderID
			,SpecialtyID
			,PurchaserID
			,SourceOfReferralID
			,EpisodeID
			,ManagementIntentionID
			,AdministrativeCategoryID
			,EpisodeTypeID
			,MentalCategoryOnTransferID
			,ContractID
			,AttendedIndicatorID
			,PatientClassificationID
			,EpisodeOutcomeID
			,IncidentLocationID
			,PatientGroupID
			,ReferralID
			,LegalStatusCategoryID
			,ProfessionalCarerID
			,PatientID
			,PaymentRulesID
			,VisitTypeID
			,StartDateTime
			,EndDateTime
			,DNAReasonID
			,AdmissionMethodID
			,PriorProfessionalCareEpisodeID
			,SelfPaymentExemptionReasonID
			,Comments
			,CreatedDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,EpisodeIdentifier
			,SessionTransactionID
			,PriorPointerID
			,ExternalKey
			,CodingCompleteFlag
			,AgeBandID
			,MentalHealthCareEpisodeID
			,HRGCode
			,ContractServiceID
			,ServicePointID
			,ReallocateFlag
			,DischargeSummaryCompleteFlag
			,ProvisionalOnlyIndicatorFlag
			,ClinicalCostCentreID
			,AdmissionOfferID
			,ClinicalTransferReasonID
			,CodingStatusID
			,CodingAuthorisationFlag
			,CodingAuthorisationUser
			,CodingAuthorisationDateTime
			,LatchedAuthorisationFlag
			,InvoiceTag
			,ServiceCategoryID
			,PatientClassificationForBillingID
			,ResetDaysFlag
			,PriorDays
			,ICUHours
			,CCUHours
			,MechanicalVentilationHours
			,PalliativeCareStatusID
			,ClinicalCodingID
			,ActivityAlias
			,DDGVPCode
			,WIESCasemixValue
			,ActivityCode
			,ActivityID
			,HRGVersionCode
			,RTTStatusID
			,HealthOrganisationOwnerID
			,LorenzoCreated
			)
into
	#TLoadPASProfessionalCareEpisode
from
	(
	select
		ProfessionalCareEpisodeID
		,WaitingListID = cast(WaitingListID as int)
		,ServicePointStayID = cast(ServicePointStayID as int)
		,ProviderSpellID = cast(ProviderSpellID as int)
		,ServicePointStayServicePointID = cast(ServicePointStayServicePointID as int)
		,ProviderID = cast(ProviderID as int)
		,SpecialtyID = cast(SpecialtyID as int)
		,PurchaserID = cast(PurchaserID as int)
		,SourceOfReferralID = cast(SourceOfReferralID as int)
		,EpisodeID = cast(EpisodeID as int)
		,ManagementIntentionID = cast(ManagementIntentionID as int)
		,AdministrativeCategoryID = cast(AdministrativeCategoryID as int)
		,EpisodeTypeID = cast(EpisodeTypeID as int)
		,MentalCategoryOnTransferID = cast(MentalCategoryOnTransferID as int)
		,ContractID = cast(ContractID as int)
		,AttendedIndicatorID = cast(AttendedIndicatorID as int)
		,PatientClassificationID = cast(PatientClassificationID as int)
		,EpisodeOutcomeID = cast(EpisodeOutcomeID as int)
		,IncidentLocationID = cast(IncidentLocationID as int)
		,PatientGroupID = cast(PatientGroupID as int)
		,ReferralID = cast(ReferralID as int)
		,LegalStatusCategoryID = cast(LegalStatusCategoryID as int)
		,ProfessionalCarerID = cast(ProfessionalCarerID as int)
		,PatientID = cast(PatientID as int)
		,PaymentRulesID = cast(PaymentRulesID as int)
		,VisitTypeID = cast(VisitTypeID as int)
		,StartDateTime = cast(StartDateTime as datetime)
		,EndDateTime = cast(EndDateTime as datetime)
		,DNAReasonID = cast(DNAReasonID as int)
		,AdmissionMethodID = cast(AdmissionMethodID as int)
		,PriorProfessionalCareEpisodeID = cast(PriorProfessionalCareEpisodeID as int)
		,SelfPaymentExemptionReasonID = cast(SelfPaymentExemptionReasonID as int)
		,Comments = cast(nullif(Comments, '') as varchar(255))
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(50))
		,UserModify = cast(nullif(UserModify, '') as varchar(50))
		,EpisodeIdentifier = cast(nullif(EpisodeIdentifier, '') as varchar(50))
		,SessionTransactionID = cast(SessionTransactionID as int)
		,PriorPointerID = cast(PriorPointerID as int)
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(50))
		,CodingCompleteFlag = cast(nullif(CodingCompleteFlag, '') as char(1))
		,AgeBandID = cast(AgeBandID as int)
		,MentalHealthCareEpisodeID = cast(MentalHealthCareEpisodeID as int)
		,HRGCode = cast(nullif(HRGCode, '') as varchar(50))
		,ContractServiceID = cast(ContractServiceID as int)
		,ServicePointID = cast(ServicePointID as int)
		,ReallocateFlag = cast(nullif(ReallocateFlag, '') as char(1))
		,DischargeSummaryCompleteFlag = cast(nullif(DischargeSummaryCompleteFlag, '') as char(1))
		,ProvisionalOnlyIndicatorFlag = cast(nullif(ProvisionalOnlyIndicatorFlag, '') as char(1))
		,ClinicalCostCentreID = cast(ClinicalCostCentreID as int)
		,AdmissionOfferID = cast(AdmissionOfferID as int)
		,ClinicalTransferReasonID = cast(ClinicalTransferReasonID as int)
		,CodingStatusID = cast(CodingStatusID as int)
		,CodingAuthorisationFlag = cast(nullif(CodingAuthorisationFlag, '') as char(1))
		,CodingAuthorisationUser = cast(nullif(CodingAuthorisationUser, '') as varchar(50))
		,CodingAuthorisationDateTime = cast(CodingAuthorisationDateTime as datetime)
		,LatchedAuthorisationFlag = cast(nullif(LatchedAuthorisationFlag, '') as char(1))
		,InvoiceTag = cast(nullif(InvoiceTag, '') as varchar(50))
		,ServiceCategoryID = cast(ServiceCategoryID as int)
		,PatientClassificationForBillingID = cast(PatientClassificationForBillingID as int)
		,ResetDaysFlag = cast(nullif(ResetDaysFlag, '') as char(1))
		,PriorDays = cast(PriorDays as int)
		,ICUHours = cast(ICUHours as int)
		,CCUHours = cast(CCUHours as int)
		,MechanicalVentilationHours = cast(MechanicalVentilationHours as int)
		,PalliativeCareStatusID = cast(PalliativeCareStatusID as int)
		,ClinicalCodingID = cast(ClinicalCodingID as int)
		,ActivityAlias = cast(nullif(ActivityAlias, '') as char(1))
		,DDGVPCode = cast(nullif(DDGVPCode, '') as varchar(50))
		,WIESCasemixValue = cast(WIESCasemixValue as int)
		,ActivityCode = cast(nullif(ActivityCode, '') as varchar(50))
		,ActivityID = cast(ActivityID as int)
		,HRGVersionCode = cast(nullif(HRGVersionCode, '') as varchar(50))
		,RTTStatusID = cast(RTTStatusID as int)
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as int)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			ProfessionalCareEpisodeID = PRCAE_REFNO
			,WaitingListID = WLIST_REFNO
			,ServicePointStayID = SSTAY_REFNO
			,ProviderSpellID = PRVSP_REFNO
			,ServicePointStayServicePointID = SSTAY_SPONT_REFNO
			,ProviderID = PROVD_REFNO
			,SpecialtyID = SPECT_REFNO
			,PurchaserID = PURCH_REFNO
			,SourceOfReferralID = SORRF_REFNO
			,EpisodeID = EPISO_REFNO
			,ManagementIntentionID = INMGT_REFNO
			,AdministrativeCategoryID = ADCAT_REFNO
			,EpisodeTypeID = EPTYP_REFNO
			,MentalCategoryOnTransferID = MENCT_REFNO
			,ContractID = CONTR_REFNO
			,AttendedIndicatorID = ATTND_REFNO
			,PatientClassificationID = PATCL_REFNO
			,EpisodeOutcomeID = CEOCM_REFNO
			,IncidentLocationID = INLOC_REFNO
			,PatientGroupID = PAGRP_REFNO
			,ReferralID = REFRL_REFNO
			,LegalStatusCategoryID = LEGSC_REFNO
			,ProfessionalCarerID = PROCA_REFNO
			,PatientID = PATNT_REFNO
			,PaymentRulesID = PAYMT_REFNO
			,VisitTypeID = VISIT_REFNO
			,StartDateTime = START_DTTM
			,EndDateTime = END_DTTM
			,DNAReasonID = DNARS_REFNO
			,AdmissionMethodID = ADMET_REFNO
			,PriorProfessionalCareEpisodeID = PRIOR_REFNO
			,SelfPaymentExemptionReasonID = SPXRL_REFNO
			,Comments = COMMENTS
			,CreatedDateTime = CREATE_DTTM
			,ModifiedDateTime = MODIF_DTTM
			,UserCreate = USER_CREATE
			,UserModify = USER_MODIF
			,EpisodeIdentifier = IDENTIFIER
			,SessionTransactionID = STRAN_REFNO
			,PriorPointerID = PRIOR_POINTER
			,ExternalKey = EXTERNAL_KEY
			,CodingCompleteFlag = CODING_COMPLETE_FLAG
			,AgeBandID = AGEBD_REFNO
			,MentalHealthCareEpisodeID = MHCEP_REFNO
			,HRGCode = HRG_CODE
			,ContractServiceID = CTRSV_REFNO
			,ServicePointID = SPONT_REFNO
			,ReallocateFlag = ALLCN_OVERRIDE_FLAG
			,DischargeSummaryCompleteFlag = SUMMARY_COMPLETE_FLAG
			,ProvisionalOnlyIndicatorFlag = PRVSN_FLAG
			,ClinicalCostCentreID = CCCCC_REFNO
			,AdmissionOfferID = ADMOF_REFNO
			,ClinicalTransferReasonID = RECTR_REFNO
			,CodingStatusID = CSTAT_REFNO
			,CodingAuthorisationFlag = COD_AUTH_FLAG
			,CodingAuthorisationUser = COD_AUTH_USER
			,CodingAuthorisationDateTime = COD_AUTH_DTTM
			,LatchedAuthorisationFlag = LATCH_AUTH_FLAG
			,InvoiceTag = INVOICE_TAG
			,ServiceCategoryID = SVCAT_REFNO
			,PatientClassificationForBillingID = PTCLS_REFNO
			,ResetDaysFlag = RESET_PTCLS_DAYS
			,PriorDays = PRIOR_PTCLS_DAYS
			,ICUHours = ICU_HOURS
			,CCUHours = CCU_HOURS
			,MechanicalVentilationHours = MECH_VENT_HRS
			,PalliativeCareStatusID = PALST_REFNO
			,ClinicalCodingID = DGPRO_REFNO
			,ActivityAlias = PTCLS_MANUAL_FLAG
			,DDGVPCode = DDGVP_CODE
			,WIESCasemixValue = WIES_CMX_VALUE
			,ActivityCode = SORCE_CODE
			,ActivityID = SORCE_REFNO
			,HRGVersionCode = HRG_VERSION
			,RTTStatusID = RTTST_REFNO
			,HealthOrganisationOwnerID = OWNER_HEORG_REFNO
			,LorenzoCreated = Created

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.ProfessionalCareEpisode
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASProfessionalCareEpisode on #TLoadPASProfessionalCareEpisode
	(
	ProfessionalCareEpisodeID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.ProfessionalCareEpisode target
using
	(
	select
		*
	from
		#TLoadPASProfessionalCareEpisode
	
	) source
	on	source.ProfessionalCareEpisodeID = target.ProfessionalCareEpisodeID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		ProfessionalCareEpisodeID
		,WaitingListID
		,ServicePointStayID
		,ProviderSpellID
		,ServicePointStayServicePointID
		,ProviderID
		,SpecialtyID
		,PurchaserID
		,SourceOfReferralID
		,EpisodeID
		,ManagementIntentionID
		,AdministrativeCategoryID
		,EpisodeTypeID
		,MentalCategoryOnTransferID
		,ContractID
		,AttendedIndicatorID
		,PatientClassificationID
		,EpisodeOutcomeID
		,IncidentLocationID
		,PatientGroupID
		,ReferralID
		,LegalStatusCategoryID
		,ProfessionalCarerID
		,PatientID
		,PaymentRulesID
		,VisitTypeID
		,[StartDateTime]
		,[EndDateTime]
		,DNAReasonID
		,AdmissionMethodID
		,PriorProfessionalCareEpisodeID
		,SelfPaymentExemptionReasonID
		,Comments
		,CreatedDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,EpisodeIdentifier
		,SessionTransactionID
		,PriorPointerID
		,ExternalKey
		,CodingCompleteFlag
		,AgeBandID
		,MentalHealthCareEpisodeID
		,HRGCode
		,ContractServiceID
		,ServicePointID
		,ReallocateFlag
		,DischargeSummaryCompleteFlag
		,ProvisionalOnlyIndicatorFlag
		,ClinicalCostCentreID
		,AdmissionOfferID
		,ClinicalTransferReasonID
		,CodingStatusID
		,CodingAuthorisationFlag
		,CodingAuthorisationUser
		,CodingAuthorisationDateTime
		,LatchedAuthorisationFlag
		,InvoiceTag
		,ServiceCategoryID
		,PatientClassificationForBillingID
		,ResetDaysFlag
		,PriorDays
		,ICUHours
		,CCUHours
		,MechanicalVentilationHours
		,PalliativeCareStatusID
		,ClinicalCodingID
		,ActivityAlias
		,DDGVPCode
		,WIESCasemixValue
		,ActivityCode
		,ActivityID
		,HRGVersionCode
		,RTTStatusID
		,HealthOrganisationOwnerID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		source.ProfessionalCareEpisodeID
		,source.WaitingListID
		,source.ServicePointStayID
		,source.ProviderSpellID
		,source.ServicePointStayServicePointID
		,source.ProviderID
		,source.SpecialtyID
		,source.PurchaserID
		,source.SourceOfReferralID
		,source.EpisodeID
		,source.ManagementIntentionID
		,source.AdministrativeCategoryID
		,source.EpisodeTypeID
		,source.MentalCategoryOnTransferID
		,source.ContractID
		,source.AttendedIndicatorID
		,source.PatientClassificationID
		,source.EpisodeOutcomeID
		,source.IncidentLocationID
		,source.PatientGroupID
		,source.ReferralID
		,source.LegalStatusCategoryID
		,source.ProfessionalCarerID
		,source.PatientID
		,source.PaymentRulesID
		,source.VisitTypeID
		,source.StartDateTime
		,source.EndDateTime
		,source.DNAReasonID
		,source.AdmissionMethodID
		,source.PriorProfessionalCareEpisodeID
		,source.SelfPaymentExemptionReasonID
		,source.Comments
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.UserCreate
		,source.UserModify
		,source.EpisodeIdentifier
		,source.SessionTransactionID
		,source.PriorPointerID
		,source.ExternalKey
		,source.CodingCompleteFlag
		,source.AgeBandID
		,source.MentalHealthCareEpisodeID
		,source.HRGCode
		,source.ContractServiceID
		,source.ServicePointID
		,source.ReallocateFlag
		,source.DischargeSummaryCompleteFlag
		,source.ProvisionalOnlyIndicatorFlag
		,source.ClinicalCostCentreID
		,source.AdmissionOfferID
		,source.ClinicalTransferReasonID
		,source.CodingStatusID
		,source.CodingAuthorisationFlag
		,source.CodingAuthorisationUser
		,source.CodingAuthorisationDateTime
		,source.LatchedAuthorisationFlag
		,source.InvoiceTag
		,source.ServiceCategoryID
		,source.PatientClassificationForBillingID
		,source.ResetDaysFlag
		,source.PriorDays
		,source.ICUHours
		,source.CCUHours
		,source.MechanicalVentilationHours
		,source.PalliativeCareStatusID
		,source.ClinicalCodingID
		,source.ActivityAlias
		,source.DDGVPCode
		,source.WIESCasemixValue
		,source.ActivityCode
		,source.ActivityID
		,source.HRGVersionCode
		,source.RTTStatusID
		,source.HealthOrganisationOwnerID
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.WaitingListID
		,target.ServicePointStayID
		,target.ProviderSpellID
		,target.ServicePointStayServicePointID
		,target.ProviderID
		,target.SpecialtyID
		,target.PurchaserID
		,target.SourceOfReferralID
		,target.EpisodeID
		,target.ManagementIntentionID
		,target.AdministrativeCategoryID
		,target.EpisodeTypeID
		,target.MentalCategoryOnTransferID
		,target.ContractID
		,target.AttendedIndicatorID
		,target.PatientClassificationID
		,target.EpisodeOutcomeID
		,target.IncidentLocationID
		,target.PatientGroupID
		,target.ReferralID
		,target.LegalStatusCategoryID
		,target.ProfessionalCarerID
		,target.PatientID
		,target.PaymentRulesID
		,target.VisitTypeID
		,target.[StartDateTime]
		,target.[EndDateTime]
		,target.DNAReasonID
		,target.AdmissionMethodID
		,target.PriorProfessionalCareEpisodeID
		,target.SelfPaymentExemptionReasonID
		,target.Comments
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.EpisodeIdentifier
		,target.SessionTransactionID
		,target.PriorPointerID
		,target.ExternalKey
		,target.CodingCompleteFlag
		,target.AgeBandID
		,target.MentalHealthCareEpisodeID
		,target.HRGCode
		,target.ContractServiceID
		,target.ServicePointID
		,target.ReallocateFlag
		,target.DischargeSummaryCompleteFlag
		,target.ProvisionalOnlyIndicatorFlag
		,target.ClinicalCostCentreID
		,target.AdmissionOfferID
		,target.ClinicalTransferReasonID
		,target.CodingStatusID
		,target.CodingAuthorisationFlag
		,target.CodingAuthorisationUser
		,target.CodingAuthorisationDateTime
		,target.LatchedAuthorisationFlag
		,target.InvoiceTag
		,target.ServiceCategoryID
		,target.PatientClassificationForBillingID
		,target.ResetDaysFlag
		,target.PriorDays
		,target.ICUHours
		,target.CCUHours
		,target.MechanicalVentilationHours
		,target.PalliativeCareStatusID
		,target.ClinicalCodingID
		,target.ActivityAlias
		,target.DDGVPCode
		,target.WIESCasemixValue
		,target.ActivityCode
		,target.ActivityID
		,target.HRGVersionCode
		,target.RTTStatusID
		,target.HealthOrganisationOwnerID
		,target.LorenzoCreated
		)

then
	update
	set
		target.ProfessionalCareEpisodeID = source.ProfessionalCareEpisodeID
		,target.WaitingListID = source.WaitingListID
		,target.ServicePointStayID = source.ServicePointStayID
		,target.ProviderSpellID = source.ProviderSpellID
		,target.ServicePointStayServicePointID = source.ServicePointStayServicePointID
		,target.ProviderID = source.ProviderID
		,target.SpecialtyID = source.SpecialtyID
		,target.PurchaserID = source.PurchaserID
		,target.SourceOfReferralID = source.SourceOfReferralID
		,target.EpisodeID = source.EpisodeID
		,target.ManagementIntentionID = source.ManagementIntentionID
		,target.AdministrativeCategoryID = source.AdministrativeCategoryID
		,target.EpisodeTypeID = source.EpisodeTypeID
		,target.MentalCategoryOnTransferID = source.MentalCategoryOnTransferID
		,target.ContractID = source.ContractID
		,target.AttendedIndicatorID = source.AttendedIndicatorID
		,target.PatientClassificationID = source.PatientClassificationID
		,target.EpisodeOutcomeID = source.EpisodeOutcomeID
		,target.IncidentLocationID = source.IncidentLocationID
		,target.PatientGroupID = source.PatientGroupID
		,target.ReferralID = source.ReferralID
		,target.LegalStatusCategoryID = source.LegalStatusCategoryID
		,target.ProfessionalCarerID = source.ProfessionalCarerID
		,target.PatientID = source.PatientID
		,target.PaymentRulesID = source.PaymentRulesID
		,target.VisitTypeID = source.VisitTypeID
		,target.[StartDateTime] = source.StartDateTime
		,target.[EndDateTime] = source.EndDateTime
		,target.DNAReasonID = source.DNAReasonID
		,target.AdmissionMethodID = source.AdmissionMethodID
		,target.PriorProfessionalCareEpisodeID = source.PriorProfessionalCareEpisodeID
		,target.SelfPaymentExemptionReasonID = source.SelfPaymentExemptionReasonID
		,target.Comments = source.Comments
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.EpisodeIdentifier = source.EpisodeIdentifier
		,target.SessionTransactionID = source.SessionTransactionID
		,target.PriorPointerID = source.PriorPointerID
		,target.ExternalKey = source.ExternalKey
		,target.CodingCompleteFlag = source.CodingCompleteFlag
		,target.AgeBandID = source.AgeBandID
		,target.MentalHealthCareEpisodeID = source.MentalHealthCareEpisodeID
		,target.HRGCode = source.HRGCode
		,target.ContractServiceID = source.ContractServiceID
		,target.ServicePointID = source.ServicePointID
		,target.ReallocateFlag = source.ReallocateFlag
		,target.DischargeSummaryCompleteFlag = source.DischargeSummaryCompleteFlag
		,target.ProvisionalOnlyIndicatorFlag = source.ProvisionalOnlyIndicatorFlag
		,target.ClinicalCostCentreID = source.ClinicalCostCentreID
		,target.AdmissionOfferID = source.AdmissionOfferID
		,target.ClinicalTransferReasonID = source.ClinicalTransferReasonID
		,target.CodingStatusID = source.CodingStatusID
		,target.CodingAuthorisationFlag = source.CodingAuthorisationFlag
		,target.CodingAuthorisationUser = source.CodingAuthorisationUser
		,target.CodingAuthorisationDateTime = source.CodingAuthorisationDateTime
		,target.LatchedAuthorisationFlag = source.LatchedAuthorisationFlag
		,target.InvoiceTag = source.InvoiceTag
		,target.ServiceCategoryID = source.ServiceCategoryID
		,target.PatientClassificationForBillingID = source.PatientClassificationForBillingID
		,target.ResetDaysFlag = source.ResetDaysFlag
		,target.PriorDays = source.PriorDays
		,target.ICUHours = source.ICUHours
		,target.CCUHours = source.CCUHours
		,target.MechanicalVentilationHours = source.MechanicalVentilationHours
		,target.PalliativeCareStatusID = source.PalliativeCareStatusID
		,target.ClinicalCodingID = source.ClinicalCodingID
		,target.ActivityAlias = source.ActivityAlias
		,target.DDGVPCode = source.DDGVPCode
		,target.WIESCasemixValue = source.WIESCasemixValue
		,target.ActivityCode = source.ActivityCode
		,target.ActivityID = source.ActivityID
		,target.HRGVersionCode = source.HRGVersionCode
		,target.RTTStatusID = source.RTTStatusID
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime