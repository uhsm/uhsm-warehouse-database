﻿CREATE PROCEDURE [Lorenzo].[LoadProfessionalCarer]

AS


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(ProfessionalCarer.LorenzoCreated)
		from
			Lorenzo.ProfessionalCarer
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 ProfessionalCarerID
			,ProfessionalCarerMainIdentifier
			,ProfessionalCarerTypeID
			,ProfessionalCarerTypeMainCode
			,ProfessionalCarerType
			,Forename
			,TitleID
			,TitleMainCode
			,Title
			,GradeID
			,GradeMainCode
			,Grade
			,Surname
			,SexID
			,SexMainCode
			,Sex
			,DateOfBirth
			,ContractedHours
			,LeaveEntitlement
			,PlaceOfBirth
			,Qualifications
			,GloveInventoryID
			,GloveInventoryMainCode
			,GloveInventory
			,SkinPreparationInventoryID
			,SkinPreparationInventoryMainCode
			,SkinPreparationInventory
			,DefaultPathologyLabID
			,DefaultPathologyLabCode
			,DefaultPathologyLab
			,StartDateTime
			,EndDateTime
			,Description
			,CreatedDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,IsLocal
			,MainIdenitifier
			,PrimaryStaffTeamID
			,PrimaryStaffTeamCode
			,PrimaryStaffTeam
			,OwnerHealthOrganisationID
			,OwnerHealthOrganisationMainIdentifier
			,LorenzoCreated
			)
into
	#TLoadPASProfessionalCarer
from
	(
	select
		ProfessionalCarerID
		,ProfessionalCarerMainIdentifier = cast(nullif(ProfessionalCarerMainIdentifier, '') as varchar(25))
		,ProfessionalCarerTypeID = cast(ProfessionalCarerTypeID as numeric)
		,ProfessionalCarerTypeMainCode = cast(nullif(ProfessionalCarerTypeMainCode, '') as varchar(25))
		,ProfessionalCarerType = cast(nullif(ProfessionalCarerType, '') as varchar(80))
		,Forename = cast(nullif(Forename, '') as varchar(30))
		,TitleID = cast(TitleID as numeric)
		,TitleMainCode = cast(nullif(TitleMainCode, '') as varchar(25))
		,Title = cast(nullif(Title, '') as varchar(80))
		,GradeID = cast(GradeID as numeric)
		,GradeMainCode = cast(nullif(GradeMainCode, '') as varchar(25))
		,Grade = cast(nullif(Grade, '') as varchar(80))
		,Surname = cast(nullif(Surname, '') as varchar(30))
		,SexID = cast(SexID as numeric)
		,SexMainCode = cast(nullif(SexMainCode, '') as varchar(25))
		,Sex = cast(nullif(Sex, '') as varchar(80))
		,DateOfBirth = cast(DateOfBirth as datetime)
		,ContractedHours = cast(ContractedHours as numeric)
		,LeaveEntitlement = cast(LeaveEntitlement as numeric)
		,PlaceOfBirth = cast(nullif(PlaceOfBirth, '') as varchar(80))
		,Qualifications = cast(nullif(Qualifications, '') as varchar(80))
		,GloveInventoryID = cast(GloveInventoryID as numeric)
		,GloveInventoryMainCode = cast(nullif(GloveInventoryMainCode, '') as varchar(25))
		,GloveInventory = cast(nullif(GloveInventory, '') as varchar(80))
		,SkinPreparationInventoryID = cast(SkinPreparationInventoryID as numeric)
		,SkinPreparationInventoryMainCode = cast(nullif(SkinPreparationInventoryMainCode, '') as varchar(25))
		,SkinPreparationInventory = cast(nullif(SkinPreparationInventory, '') as varchar(80))
		,DefaultPathologyLabID = cast(DefaultPathologyLabID as numeric)
		,DefaultPathologyLabCode = cast(nullif(DefaultPathologyLabCode, '') as varchar(25))
		,DefaultPathologyLab = cast(nullif(DefaultPathologyLab, '') as varchar(80))
		,StartDateTime = cast(StartDateTime as datetime)
		,EndDateTime = cast(EndDateTime as datetime)
		,Description = cast(nullif(Description, '') as varchar(80))
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(30))
		,UserModify = cast(nullif(UserModify, '') as varchar(30))
		,IsLocal = cast(nullif(IsLocal, '') as char(1))
		,MainIdenitifier = cast(nullif(MainIdenitifier, '') as varchar(20))
		,PrimaryStaffTeamID = cast(PrimaryStaffTeamID as numeric)
		,PrimaryStaffTeamCode = cast(nullif(PrimaryStaffTeamCode, '') as varchar(5))
		,PrimaryStaffTeam = cast(nullif(PrimaryStaffTeam, '') as varchar(20))
		,OwnerHealthOrganisationID = cast(OwnerHealthOrganisationID as numeric)
		,OwnerHealthOrganisationMainIdentifier = cast(nullif(OwnerHealthOrganisationMainIdentifier, '') as varchar(25))
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			ProfessionalCarerID =  [PROCA_REFNO]
		  ,ProfessionalCarerMainIdentifier = [PROCA_REFNO_MAIN_IDENT]
		  ,ProfessionalCarerTypeID = [PRTYP_REFNO]
		  ,ProfessionalCarerTypeMainCode = [PRTYP_REFNO_MAIN_CODE]
		  ,ProfessionalCarerType = [PRTYP_REFNO_DESCRIPTION]
		  ,Forename = [FORENAME]
		  ,TitleID = [TITLE_REFNO]
		  ,TitleMainCode = [TITLE_REFNO_MAIN_CODE]
		  ,Title = [TITLE_REFNO_DESCRIPTION]
		  ,GradeID = [GRADE_REFNO]
		  ,GradeMainCode = [GRADE_REFNO_MAIN_CODE]
		  ,Grade = [GRADE_REFNO_DESCRIPTION]
		  ,Surname = [SURNAME]
		  ,SexID = [SEXXX_REFNO]
		  ,SexMainCode = [SEXXX_REFNO_MAIN_CODE]
		  ,Sex = [SEXXX_REFNO_DESCRIPTION]
		  ,DateOfBirth = [DATE_OF_BIRTH]
		  ,ContractedHours = [CONTR_HOURS]
		  ,LeaveEntitlement = [LEAVE_ENTITLEMENT]
		  ,PlaceOfBirth = [PLACE_OF_BIRTH]
		  ,Qualifications = [QUALIFICATIONS]
		  ,GloveInventoryID = [GLOVE_INITM_REFNO]
		  ,GloveInventoryMainCode = [GLOVE_INITM_REFNO_MAIN_CODE]
		  ,GloveInventory = [GLOVE_INITM_REFNO_DESCRIPTION]
		  ,SkinPreparationInventoryID = [SKNPR_INITM_REFNO]
		  ,SkinPreparationInventoryMainCode = [SKNPR_INITM_REFNO_MAIN_CODE]
		  ,SkinPreparationInventory = [SKNPR_INITM_REFNO_DESCRIPTION]
		  ,DefaultPathologyLabID = [DEF_PATH_SPONT_REFNO] 
		  ,DefaultPathologyLabCode = [DEF_PATH_SPONT_REFNO_CODE]
		  ,DefaultPathologyLab = [DEF_PATH_SPONT_REFNO_NAME]
		  ,StartDateTime = [START_DTTM]
		  ,EndDateTime = [END_DTTM]
		  ,Description = [DESCRIPTION]
		  ,CreatedDateTime = [CREATE_DTTM]
		  ,ModifiedDateTime = [MODIF_DTTM]
		  ,UserCreate = [USER_CREATE]
		  ,UserModify = [USER_MODIF]
		  ,IsLocal = [LOCAL_FLAG]
		  ,MainIdenitifier = [MAIN_IDENT]
		  ,PrimaryStaffTeamID = [PRIMARY_STEAM_REFNO]
		  ,PrimaryStaffTeamCode = [PRIMARY_STEAM_REFNO_CODE]
		  ,PrimaryStaffTeam = [PRIMARY_STEAM_REFNO_NAME]
		  ,OwnerHealthOrganisationID = [OWNER_HEORG_REFNO]
		  ,OwnerHealthOrganisationMainIdentifier = [OWNER_HEORG_REFNO_MAIN_IDENT]
		  ,LorenzoCreated = [Created]

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.ProfessionalCarer
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASProfessionalCarer on #TLoadPASProfessionalCarer
	(
	ProfessionalCarerID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.ProfessionalCarer target
using
	(
	select
		*
	from
		#TLoadPASProfessionalCarer
	
	) source
	on	source.ProfessionalCarerID = target.ProfessionalCarerID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		 ProfessionalCarerID
		,ProfessionalCarerMainIdentifier
		,ProfessionalCarerTypeID
		,ProfessionalCarerTypeMainCode
		,ProfessionalCarerType
		,Forename
		,TitleID
		,TitleMainCode
		,Title
		,GradeID
		,GradeMainCode
		,Grade
		,Surname
		,SexID
		,SexMainCode
		,Sex
		,DateOfBirth
		,ContractedHours
		,LeaveEntitlement
		,PlaceOfBirth
		,Qualifications
		,GloveInventoryID
		,GloveInventoryMainCode
		,GloveInventory
		,SkinPreparationInventoryID
		,SkinPreparationInventoryMainCode
		,SkinPreparationInventory
		,DefaultPathologyLabID
		,DefaultPathologyLabCode
		,DefaultPathologyLab
		,StartDateTime
		,EndDateTime
		,Description
		,CreatedDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,IsLocal
		,MainIdenitifier
		,PrimaryStaffTeamID
		,PrimaryStaffTeamCode
		,PrimaryStaffTeam
		,OwnerHealthOrganisationID
		,OwnerHealthOrganisationMainIdentifier
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		 source.ProfessionalCarerID
		,source.ProfessionalCarerMainIdentifier
		,source.ProfessionalCarerTypeID
		,source.ProfessionalCarerTypeMainCode
		,source.ProfessionalCarerType
		,source.Forename
		,source.TitleID
		,source.TitleMainCode
		,source.Title
		,source.GradeID
		,source.GradeMainCode
		,source.Grade
		,source.Surname
		,source.SexID
		,source.SexMainCode
		,source.Sex
		,source.DateOfBirth
		,source.ContractedHours
		,source.LeaveEntitlement
		,source.PlaceOfBirth
		,source.Qualifications
		,source.GloveInventoryID
		,source.GloveInventoryMainCode
		,source.GloveInventory
		,source.SkinPreparationInventoryID
		,source.SkinPreparationInventoryMainCode
		,source.SkinPreparationInventory
		,source.DefaultPathologyLabID
		,source.DefaultPathologyLabCode
		,source.DefaultPathologyLab
		,source.StartDateTime
		,source.EndDateTime
		,source.Description
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.UserCreate
		,source.UserModify
		,source.IsLocal
		,source.MainIdenitifier
		,source.PrimaryStaffTeamID
		,source.PrimaryStaffTeamCode
		,source.PrimaryStaffTeam
		,source.OwnerHealthOrganisationID
		,source.OwnerHealthOrganisationMainIdentifier
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		 target.ProfessionalCarerID
		,target.ProfessionalCarerMainIdentifier
		,target.ProfessionalCarerTypeID
		,target.ProfessionalCarerTypeMainCode
		,target.ProfessionalCarerType
		,target.Forename
		,target.TitleID
		,target.TitleMainCode
		,target.Title
		,target.GradeID
		,target.GradeMainCode
		,target.Grade
		,target.Surname
		,target.SexID
		,target.SexMainCode
		,target.Sex
		,target.DateOfBirth
		,target.ContractedHours
		,target.LeaveEntitlement
		,target.PlaceOfBirth
		,target.Qualifications
		,target.GloveInventoryID
		,target.GloveInventoryMainCode
		,target.GloveInventory
		,target.SkinPreparationInventoryID
		,target.SkinPreparationInventoryMainCode
		,target.SkinPreparationInventory
		,target.DefaultPathologyLabID
		,target.DefaultPathologyLabCode
		,target.DefaultPathologyLab
		,target.StartDateTime
		,target.EndDateTime
		,target.Description
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.IsLocal
		,target.MainIdenitifier
		,target.PrimaryStaffTeamID
		,target.PrimaryStaffTeamCode
		,target.PrimaryStaffTeam
		,target.OwnerHealthOrganisationID
		,target.OwnerHealthOrganisationMainIdentifier
		,target.LorenzoCreated
		)

then
	update
	set
		 target.ProfessionalCarerID = source.ProfessionalCarerID
		,target.ProfessionalCarerMainIdentifier = source.ProfessionalCarerMainIdentifier
		,target.ProfessionalCarerTypeID = source.ProfessionalCarerTypeID
		,target.ProfessionalCarerTypeMainCode = source.ProfessionalCarerTypeMainCode
		,target.ProfessionalCarerType = source.ProfessionalCarerType
		,target.Forename = source.Forename
		,target.TitleID = source.TitleID
		,target.TitleMainCode = source.TitleMainCode
		,target.Title = source.Title
		,target.GradeID = source.GradeID
		,target.GradeMainCode = source.GradeMainCode
		,target.Grade = source.Grade
		,target.Surname = source.Surname
		,target.SexID = source.SexID
		,target.SexMainCode = source.SexMainCode
		,target.Sex = source.Sex
		,target.DateOfBirth = source.DateOfBirth
		,target.ContractedHours = source.ContractedHours
		,target.LeaveEntitlement = source.LeaveEntitlement
		,target.PlaceOfBirth = source.PlaceOfBirth
		,target.Qualifications = source.Qualifications
		,target.GloveInventoryID = source.GloveInventoryID
		,target.GloveInventoryMainCode = source.GloveInventoryMainCode
		,target.GloveInventory = source.GloveInventory
		,target.SkinPreparationInventoryID = source.SkinPreparationInventoryID
		,target.SkinPreparationInventoryMainCode = source.SkinPreparationInventoryMainCode
		,target.SkinPreparationInventory = source.SkinPreparationInventory
		,target.DefaultPathologyLabID = source.DefaultPathologyLabID
		,target.DefaultPathologyLabCode = source.DefaultPathologyLabCode
		,target.DefaultPathologyLab = source.DefaultPathologyLab
		,target.StartDateTime = source.StartDateTime
		,target.EndDateTime = source.EndDateTime
		,target.Description = source.Description
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.IsLocal = source.IsLocal
		,target.MainIdenitifier = source.MainIdenitifier
		,target.PrimaryStaffTeamID = source.PrimaryStaffTeamID
		,target.PrimaryStaffTeamCode = source.PrimaryStaffTeamCode
		,target.PrimaryStaffTeam = source.PrimaryStaffTeam
		,target.OwnerHealthOrganisationID = source.OwnerHealthOrganisationID
		,target.OwnerHealthOrganisationMainIdentifier = source.OwnerHealthOrganisationMainIdentifier
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime
