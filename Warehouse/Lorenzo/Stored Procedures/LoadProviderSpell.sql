﻿CREATE procedure [Lorenzo].[LoadProviderSpell] as 


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(ProviderSpell.LorenzoCreated)
		from
			Lorenzo.ProviderSpell
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			SpecialtyID
			,ServicePointID
			,AdmissionMethodID
			,AdmissionSourceID
			,PatientClassificationID
			,ReadmissionReasonID
			,ConfidentialityLevelID
			,ProfessionalCarerID
			,PostMortemID
			,DischargeDestinationID
			,AdministrativeCategoryID
			,DischargeMethodID
			,PatientID
			,SpellTypeID
			,ReferralParagraphID
			,FollowUpPlanID
			,DischargeParagraphID
			,FirstPALLID
			,PreviousPALLID
			,AdmissionDateTime
			,DischargeDateTime
			,ProviderID
			,PurchaserID
			,ResponsibleHealthOrganisationID
			,ReferralID
			,AEAttendanceID
			,NotifiableAdmissionTypeID
			,IsFirstRegularAdmission
			,ManagementIntentionID
			,ExpectedDischargeDateTime
			,IsDisabled
			,IsConfidential
			,IsSilentReferral
			,PriorProfessionalCareEpisodeID
			,CreatedDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,EpisodeIdentifier
			,SessionTransactionID
			,PriorPointerID
			,AdmissionDecisionID
			,AdmissionOfferID
			,DecisionToAdmitDateTime
			,AddedToWaitingListDateTime
			,IsFastTrack
			,ContractID
			,ExternalKey
			,AgeBandID
			,ArrivalCarrierID
			,DischargeCarrierID
			,ArrivalTransportID
			,DepartTransportID
			,PACACID
			,MaternitySpellID
			,MentalCategoryOnAdmissionID
			,LegalStatusOnAdmissionID
			,MentalCategoryOnDischargeID
			,LegalStatusOnDischargeID
			,MentalHealthCareEpisodeID
			,LastEpisodeInSpellIdentifier
			,NoFundsFlag
			,ClinicalCostCentreID
			,IsProvisionalStart
			,IsProvisionalEnd
			,ContactProfessionalCarerID
			,MedicalDischargeDateTime
			,BillingExemptionCategoryID
			,BillingExemptionReasonID
			,BillingPaymentMethodID
			,ReasonForEconomicCodeID
			,DiscloseInformationID
			,DiscloseInformation
			,IdentityPapersReferenceID
			,DischargeDelayReasonID
			,DepositAmount
			,NilByMouthDateTime
			,TheatreVisitStatusID
			,UsualAccomodationTypeID
			,FinancialSubProgramID
			,EmploymentStatusID
			,PensionStatusID
			,FirstAdmissionToPsychUnitDateTime
			,PreviousAdmissionToPsychUnitDateTime
			,ConsentFlag
			,GPAdvisedFlag
			,FormReferenceNumber
			,AdmissionWeight
			,ICUHours
			,CCUHours
			,IntensionToReadmitID
			,AdmissionPaymentStatusID
			,DischargePaymentStatusID
			,AdmissionServiceCategoryID
			,DischargeServiceCategoryID
			,MechanicalVentilationHours
			,AmbulanceNumber
			,PriorDays
			,RelatedAdmission
			,AdmissionPatientClassificationID
			,CFWDDays
			,AccExpirtDateTime
			,ContractRoleID
			,ContractTypeID
			,CompensibleDetailID
			,ApplyVATToAll
			,ServiceUnitTypeID
			,HealthInsuranceStatusID
			,ClaimStatusID
			,DataLinkageConsentID
			,CarerAvailabilityID
			,ReferralForFurtherCareID
			,AccessLevelsID
			,BarthelScoreOnAdmissionID
			,BarthelScoreOnSeparationID
			,ClinicalSubProgramID
			,OnsetDate
			,ReadmissionToRehabFlag
			,SourceOfReferralToPalliativeCareID
			,RugadlScoreOnAdmissionID
			,RugadlScoreOnSeparationID
			,ICUTime
			,CCUTime
			,CHDID
			,PresentingProblemOnAdmissionID
			,PresentingProblemOnAdmission
			,NonInvasiveVentilationHours
			,AgedCareAssessmentID
			,CrisiResolutionServicesOfferedFlag
			,LegalStatusID
			,MentalCategoryOnTransferID
			,HARPFlag
			,TreatInitFlag
			,RTTStatusID
			,EarliestReasonableOfferDateTime
			,DelayedDischargeAttributedToID
			,HealthOrganisationOwnerID
			,LorenzoCreated
			)
into
	#TLoadPASProviderSpell
from
	(
	select
		ProviderSpellID
		,SpecialtyID = cast(SpecialtyID as int)
		,ServicePointID = cast(ServicePointID as int)
		,AdmissionMethodID = cast(AdmissionMethodID as int)
		,AdmissionSourceID = cast(AdmissionSourceID as int)
		,PatientClassificationID = cast(PatientClassificationID as int)
		,ReadmissionReasonID = cast(ReadmissionReasonID as int)
		,ConfidentialityLevelID = cast(ConfidentialityLevelID as int)
		,ProfessionalCarerID = cast(ProfessionalCarerID as int)
		,PostMortemID = cast(PostMortemID as int)
		,DischargeDestinationID = cast(DischargeDestinationID as int)
		,AdministrativeCategoryID = cast(AdministrativeCategoryID as int)
		,DischargeMethodID = cast(DischargeMethodID as int)
		,PatientID = cast(PatientID as int)
		,SpellTypeID = cast(SpellTypeID as int)
		,ReferralParagraphID = cast(ReferralParagraphID as int)
		,FollowUpPlanID = cast(FollowUpPlanID as int)
		,DischargeParagraphID = cast(DischargeParagraphID as int)
		,FirstPALLID = cast(FirstPALLID as int)
		,PreviousPALLID = cast(PreviousPALLID as int)
		,AdmissionDateTime = cast(AdmissionDateTime as datetime)
		,DischargeDateTime = cast(DischargeDateTime as datetime)
		,ProviderID = cast(ProviderID as int)
		,PurchaserID = cast(PurchaserID as int)
		,ResponsibleHealthOrganisationID = cast(ResponsibleHealthOrganisationID as int)
		,ReferralID = cast(ReferralID as int)
		,AEAttendanceID = cast(AEAttendanceID as int)
		,NotifiableAdmissionTypeID = cast(NotifiableAdmissionTypeID as int)
		,IsFirstRegularAdmission = cast(nullif(IsFirstRegularAdmission, '') as varchar(50))
		,ManagementIntentionID = cast(ManagementIntentionID as int)
		,ExpectedDischargeDateTime = cast(ExpectedDischargeDateTime as datetime)
		,IsDisabled = cast(nullif(IsDisabled, '') as varchar(50))
		,IsConfidential = cast(nullif(IsConfidential, '') as varchar(50))
		,IsSilentReferral = cast(nullif(IsSilentReferral, '') as varchar(50))
		,PriorProfessionalCareEpisodeID = cast(PriorProfessionalCareEpisodeID as int)
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(50))
		,UserModify = cast(nullif(UserModify, '') as varchar(50))
		,EpisodeIdentifier = cast(nullif(EpisodeIdentifier, '') as varchar(50))
		,SessionTransactionID = cast(SessionTransactionID as int)
		,PriorPointerID = cast(PriorPointerID as int)
		,AdmissionDecisionID = cast(AdmissionDecisionID as int)
		,AdmissionOfferID = cast(AdmissionOfferID as int)
		,DecisionToAdmitDateTime = cast(DecisionToAdmitDateTime as datetime)
		,AddedToWaitingListDateTime = cast(AddedToWaitingListDateTime as datetime)
		,IsFastTrack = cast(nullif(IsFastTrack, '') as varchar(50))
		,ContractID = cast(ContractID as int)
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(50))
		,AgeBandID = cast(AgeBandID as int)
		,ArrivalCarrierID = cast(ArrivalCarrierID as int)
		,DischargeCarrierID = cast(DischargeCarrierID as int)
		,ArrivalTransportID = cast(ArrivalTransportID as int)
		,DepartTransportID = cast(DepartTransportID as int)
		,PACACID = cast(PACACID as int)
		,MaternitySpellID = cast(MaternitySpellID as int)
		,MentalCategoryOnAdmissionID = cast(MentalCategoryOnAdmissionID as int)
		,LegalStatusOnAdmissionID = cast(LegalStatusOnAdmissionID as int)
		,MentalCategoryOnDischargeID = cast(MentalCategoryOnDischargeID as int)
		,LegalStatusOnDischargeID = cast(LegalStatusOnDischargeID as int)
		,MentalHealthCareEpisodeID = cast(MentalHealthCareEpisodeID as int)
		,LastEpisodeInSpellIdentifier = cast(nullif(LastEpisodeInSpellIdentifier, '') as varchar(50))
		,NoFundsFlag = cast(nullif(NoFundsFlag, '') as char(1))
		,ClinicalCostCentreID = cast(ClinicalCostCentreID as int)
		,IsProvisionalStart = cast(nullif(IsProvisionalStart, '') as char(1))
		,IsProvisionalEnd = cast(nullif(IsProvisionalEnd, '') as varchar(50))
		,ContactProfessionalCarerID = cast(ContactProfessionalCarerID as int)
		,MedicalDischargeDateTime = cast(MedicalDischargeDateTime as datetime)
		,BillingExemptionCategoryID = cast(BillingExemptionCategoryID as int)
		,BillingExemptionReasonID = cast(BillingExemptionReasonID as int)
		,BillingPaymentMethodID = cast(BillingPaymentMethodID as int)
		,ReasonForEconomicCodeID = cast(ReasonForEconomicCodeID as int)
		,DiscloseInformationID = cast(DiscloseInformationID as int)
		,DiscloseInformation = cast(nullif(DiscloseInformation, '') as varchar(255))
		,IdentityPapersReferenceID = cast(IdentityPapersReferenceID as int)
		,DischargeDelayReasonID = cast(DischargeDelayReasonID as int)
		,DepositAmount = cast(DepositAmount as int)
		,NilByMouthDateTime = cast(NilByMouthDateTime as datetime)
		,TheatreVisitStatusID = cast(TheatreVisitStatusID as int)
		,UsualAccomodationTypeID = cast(UsualAccomodationTypeID as int)
		,FinancialSubProgramID = cast(FinancialSubProgramID as int)
		,EmploymentStatusID = cast(EmploymentStatusID as int)
		,PensionStatusID = cast(PensionStatusID as int)
		,FirstAdmissionToPsychUnitDateTime = cast(FirstAdmissionToPsychUnitDateTime as datetime)
		,PreviousAdmissionToPsychUnitDateTime = cast(PreviousAdmissionToPsychUnitDateTime as datetime)
		,ConsentFlag = cast(nullif(ConsentFlag, '') as char(1))
		,GPAdvisedFlag = cast(nullif(GPAdvisedFlag, '') as char(1))
		,FormReferenceNumber = cast(nullif(FormReferenceNumber, '') as varchar(50))
		,AdmissionWeight = cast(AdmissionWeight as int)
		,ICUHours = cast(ICUHours as int)
		,CCUHours = cast(CCUHours as int)
		,IntensionToReadmitID = cast(IntensionToReadmitID as int)
		,AdmissionPaymentStatusID = cast(AdmissionPaymentStatusID as int)
		,DischargePaymentStatusID = cast(DischargePaymentStatusID as int)
		,AdmissionServiceCategoryID = cast(AdmissionServiceCategoryID as int)
		,DischargeServiceCategoryID = cast(DischargeServiceCategoryID as int)
		,MechanicalVentilationHours = cast(MechanicalVentilationHours as int)
		,AmbulanceNumber = cast(nullif(AmbulanceNumber, '') as varchar(50))
		,PriorDays = cast(PriorDays as int)
		,RelatedAdmission = cast(nullif(RelatedAdmission, '') as varchar(50))
		,AdmissionPatientClassificationID = cast(AdmissionPatientClassificationID as int)
		,CFWDDays = cast(nullif(CFWDDays, '') as varchar(50))
		,AccExpirtDateTime = cast(AccExpirtDateTime as datetime)
		,ContractRoleID = cast(ContractRoleID as int)
		,ContractTypeID = cast(ContractTypeID as int)
		,CompensibleDetailID = cast(CompensibleDetailID as int)
		,ApplyVATToAll = cast(nullif(ApplyVATToAll, '') as varchar(50))
		,ServiceUnitTypeID = cast(ServiceUnitTypeID as int)
		,HealthInsuranceStatusID = cast(HealthInsuranceStatusID as int)
		,ClaimStatusID = cast(ClaimStatusID as int)
		,DataLinkageConsentID = cast(DataLinkageConsentID as int)
		,CarerAvailabilityID = cast(CarerAvailabilityID as int)
		,ReferralForFurtherCareID = cast(ReferralForFurtherCareID as int)
		,AccessLevelsID = cast(AccessLevelsID as int)
		,BarthelScoreOnAdmissionID = cast(BarthelScoreOnAdmissionID as int)
		,BarthelScoreOnSeparationID = cast(BarthelScoreOnSeparationID as int)
		,ClinicalSubProgramID = cast(ClinicalSubProgramID as int)
		,OnsetDate = cast(OnsetDate as datetime)
		,ReadmissionToRehabFlag = cast(nullif(ReadmissionToRehabFlag, '') as varchar(50))
		,SourceOfReferralToPalliativeCareID = cast(SourceOfReferralToPalliativeCareID as int)
		,RugadlScoreOnAdmissionID = cast(RugadlScoreOnAdmissionID as int)
		,RugadlScoreOnSeparationID = cast(RugadlScoreOnSeparationID as int)
		,ICUTime = cast(ICUTime as int)
		,CCUTime = cast(CCUTime as int)
		,CHDID = cast(CHDID as int)
		,PresentingProblemOnAdmissionID = cast(PresentingProblemOnAdmissionID as int)
		,PresentingProblemOnAdmission = cast(nullif(PresentingProblemOnAdmission, '') as varchar(255))
		,NonInvasiveVentilationHours = cast(NonInvasiveVentilationHours as int)
		,AgedCareAssessmentID = cast(AgedCareAssessmentID as int)
		,CrisiResolutionServicesOfferedFlag = cast(nullif(CrisiResolutionServicesOfferedFlag, '') as char(1))
		,LegalStatusID = cast(LegalStatusID as int)
		,MentalCategoryOnTransferID = cast(MentalCategoryOnTransferID as int)
		,HARPFlag = cast(nullif(HARPFlag, '') as varchar(50))
		,TreatInitFlag = cast(nullif(TreatInitFlag, '') as char(1))
		,RTTStatusID = cast(RTTStatusID as int)
		,EarliestReasonableOfferDateTime = cast(EarliestReasonableOfferDateTime as datetime)
		,DelayedDischargeAttributedToID = cast(DelayedDischargeAttributedToID as int)
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as int)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			ProviderSpellID = PRVSP_REFNO
			,SpecialtyID = SPECT_REFNO
			,ServicePointID = SPONT_REFNO
			,AdmissionMethodID = ADMET_REFNO
			,AdmissionSourceID = ADSOR_REFNO
			,PatientClassificationID = PATCL_REFNO
			,ReadmissionReasonID = READM_REFNO
			,ConfidentialityLevelID = CLEVL_REFNO
			,ProfessionalCarerID = PROCA_REFNO
			,PostMortemID = PMORT_REFNO
			,DischargeDestinationID = DISDE_REFNO
			,AdministrativeCategoryID = ADCAT_REFNO
			,DischargeMethodID = DISMT_REFNO
			,PatientID = PATNT_REFNO
			,SpellTypeID = SPELL_REFNO
			,ReferralParagraphID = REFPA_REFNO
			,FollowUpPlanID = FOLUP_REFNO
			,DischargeParagraphID = DISCH_REFNO
			,FirstPALLID = FIRST_PALL_YNUNK_REFNO
			,PreviousPALLID = PREV_PALL_YNUNK_REFNO
			,AdmissionDateTime = ADMIT_DTTM
			,DischargeDateTime = DISCH_DTTM
			,ProviderID = PROVD_REFNO
			,PurchaserID = PURCH_REFNO
			,ResponsibleHealthOrganisationID = RESP_HEORG_REFNO
			,ReferralID = REFRL_REFNO
			,AEAttendanceID = AEATT_REFNO
			,NotifiableAdmissionTypeID = NOTAD_REFNO
			,IsFirstRegularAdmission = FIRST_REGULAR
			,ManagementIntentionID = INMGT_REFNO
			,ExpectedDischargeDateTime = EXPDS_DTTM
			,IsDisabled = DISABLED
			,IsConfidential = CONFIDENTIAL
			,IsSilentReferral = SILNT_REFRL
			,PriorProfessionalCareEpisodeID = PRIOR_REFNO
			,CreatedDateTime = CREATE_DTTM
			,ModifiedDateTime = MODIF_DTTM
			,UserCreate = USER_CREATE
			,UserModify = USER_MODIF
			,EpisodeIdentifier = IDENTIFIER
			,SessionTransactionID = STRAN_REFNO
			,PriorPointerID = PRIOR_POINTER
			,AdmissionDecisionID = ADMDC_REFNO
			,AdmissionOfferID = ADMOF_REFNO
			,DecisionToAdmitDateTime = DTA_DTTM
			,AddedToWaitingListDateTime = WLIST_DTTM
			,IsFastTrack = FAST_TRACK
			,ContractID = CONTR_REFNO
			,ExternalKey = EXTERNAL_KEY
			,AgeBandID = AGEBD_REFNO
			,ArrivalCarrierID = ARCAR_HEORG_REFNO
			,DischargeCarrierID = DICAR_HEORG_REFNO
			,ArrivalTransportID = ARRIV_TRANS_REFNO
			,DepartTransportID = DEPRT_TRANS_REFNO
			,PACACID = PACAC_REFNO
			,MaternitySpellID = MATSP_REFNO
			,MentalCategoryOnAdmissionID = ADMIT_MENCT_REFNO
			,LegalStatusOnAdmissionID = ADMIT_LEGSC_REFNO
			,MentalCategoryOnDischargeID = DISCH_MENCT_REFNO
			,LegalStatusOnDischargeID = DISCH_LEGSC_REFNO
			,MentalHealthCareEpisodeID = MHCEP_REFNO
			,LastEpisodeInSpellIdentifier = LAST_PRCAE_IDENTIFIER
			,NoFundsFlag = NO_FUNDS_FLAG
			,ClinicalCostCentreID = CCCCC_REFNO
			,IsProvisionalStart = PRVSN_START_FLAG
			,IsProvisionalEnd = PRVSN_END_FLAG
			,ContactProfessionalCarerID = CNTCT_PROCA_REFNO
			,MedicalDischargeDateTime = MED_DISCH_DTTM
			,BillingExemptionCategoryID = EXMCT_REFNO
			,BillingExemptionReasonID = EXMRE_REFNO
			,BillingPaymentMethodID = PMETD_REFNO
			,ReasonForEconomicCodeID = RFECO_REFNO
			,DiscloseInformationID = DSINF_REFNO
			,DiscloseInformation = DSINF_TEXT
			,IdentityPapersReferenceID = IDPAP_REFNO
			,DischargeDelayReasonID = READD_REFNO
			,DepositAmount = DEPOSIT_AMOUNT
			,NilByMouthDateTime = NILBYMOUTH_DTTM
			,TheatreVisitStatusID = THVISIT_YNUNK_REFNO
			,UsualAccomodationTypeID = ADTAC_REFNO
			,FinancialSubProgramID = FINSP_REFNO
			,EmploymentStatusID = EMPST_REFNO
			,PensionStatusID = PENST_REFNO
			,FirstAdmissionToPsychUnitDateTime = FIRST_PSYCH_DTTM
			,PreviousAdmissionToPsychUnitDateTime = PREV_PSYCH_DTTM
			,ConsentFlag = CONSENT_FLAG
			,GPAdvisedFlag = GP_ADVISE_FLAG
			,FormReferenceNumber = FORM_REF_NO
			,AdmissionWeight = ADMISSION_WEIGHT
			,ICUHours = ICU_HOURS
			,CCUHours = CCU_HOURS
			,IntensionToReadmitID = IRADM_REFNO
			,AdmissionPaymentStatusID = ADMIT_ADTPS_REFNO
			,DischargePaymentStatusID = DISCH_ADTPS_REFNO
			,AdmissionServiceCategoryID = ADMIT_SVCAT_REFNO
			,DischargeServiceCategoryID = DISCH_SVCAT_REFNO
			,MechanicalVentilationHours = MECH_VENT_HRS
			,AmbulanceNumber = AMBULANCE_NUMBER
			,PriorDays = PRIOR_DAYS
			,RelatedAdmission = RELATED_ADMISSION
			,AdmissionPatientClassificationID = ADMIT_PTCLS_REFNO
			,CFWDDays = CFWD_DAYS
			,AccExpirtDateTime = ACC_EXPIRY_DTTM
			,ContractRoleID = CNTRL_REFNO
			,ContractTypeID = CNTTP_REFNO
			,CompensibleDetailID = COMPS_REFNO
			,ApplyVATToAll = APPLY_VAT_TO_ALL
			,ServiceUnitTypeID = ASFSR_REFNO
			,HealthInsuranceStatusID = HLINS_REFNO
			,ClaimStatusID = ICLST_REFNO
			,DataLinkageConsentID = DLNKC_REFNO
			,CarerAvailabilityID = CARAV_REFNO
			,ReferralForFurtherCareID = RFRFC_REFNO
			,AccessLevelsID = ACLEV_REFNO
			,BarthelScoreOnAdmissionID = ADMIT_BARTHEL_SCORE
			,BarthelScoreOnSeparationID = SEPAR_BARTHEL_SCORE
			,ClinicalSubProgramID = CLNSP_REFNO
			,OnsetDate = ONSET_DATE
			,ReadmissionToRehabFlag = READM_REHAB_FLAG
			,SourceOfReferralToPalliativeCareID = SRTPC_REFNO
			,RugadlScoreOnAdmissionID = ADMIT_RUGADL_SCORE
			,RugadlScoreOnSeparationID = SEPAR_RUGADL_SCORE
			,ICUTime = ICU_TIME
			,CCUTime = CCU_TIME
			,CHDID = CROEP_REFNO
			,PresentingProblemOnAdmissionID = ADTPP_REFNO
			,PresentingProblemOnAdmission = ADTPP_COMMENT
			,NonInvasiveVentilationHours = NONINV_VENT_HRS
			,AgedCareAssessmentID = ACAST_REFNO
			,CrisiResolutionServicesOfferedFlag = CRISIS_RES_SVC_FLAG
			,LegalStatusID = LEGST_REFNO
			,MentalCategoryOnTransferID = MENCT_REFNO
			,HARPFlag = HARP_FLAG
			,TreatInitFlag = TREAT_INIT_FLAG
			,RTTStatusID = RTTST_REFNO
			,EarliestReasonableOfferDateTime = ERO_DTTM
			,DelayedDischargeAttributedToID = DDATT_REFNO
			,HealthOrganisationOwnerID = OWNER_HEORG_REFNO
			,LorenzoCreated = Created


			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.ProviderSpell
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASProviderSpell on #TLoadPASProviderSpell
	(
	ProviderSpellID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.ProviderSpell target
using
	(
	select
		*
	from
		#TLoadPASProviderSpell
	
	) source
	on	source.ProviderSpellID = target.ProviderSpellID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		ProviderSpellID
		,SpecialtyID
		,ServicePointID
		,AdmissionMethodID
		,AdmissionSourceID
		,PatientClassificationID
		,ReadmissionReasonID
		,ConfidentialityLevelID
		,ProfessionalCarerID
		,PostMortemID
		,DischargeDestinationID
		,AdministrativeCategoryID
		,DischargeMethodID
		,PatientID
		,SpellTypeID
		,ReferralParagraphID
		,FollowUpPlanID
		,DischargeParagraphID
		,FirstPALLID
		,PreviousPALLID
		,AdmissionDateTime
		,DischargeDateTime
		,ProviderID
		,PurchaserID
		,ResponsibleHealthOrganisationID
		,ReferralID
		,AEAttendanceID
		,NotifiableAdmissionTypeID
		,IsFirstRegularAdmission
		,ManagementIntentionID
		,ExpectedDischargeDateTime
		,IsDisabled
		,IsConfidential
		,IsSilentReferral
		,PriorProfessionalCareEpisodeID
		,CreatedDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,EpisodeIdentifier
		,SessionTransactionID
		,PriorPointerID
		,AdmissionDecisionID
		,AdmissionOfferID
		,DecisionToAdmitDateTime
		,AddedToWaitingListDateTime
		,IsFastTrack
		,ContractID
		,ExternalKey
		,AgeBandID
		,ArrivalCarrierID
		,DischargeCarrierID
		,ArrivalTransportID
		,DepartTransportID
		,PACACID
		,MaternitySpellID
		,MentalCategoryOnAdmissionID
		,LegalStatusOnAdmissionID
		,MentalCategoryOnDischargeID
		,LegalStatusOnDischargeID
		,MentalHealthCareEpisodeID
		,LastEpisodeInSpellIdentifier
		,NoFundsFlag
		,ClinicalCostCentreID
		,IsProvisionalStart
		,IsProvisionalEnd
		,ContactProfessionalCarerID
		,MedicalDischargeDateTime
		,BillingExemptionCategoryID
		,BillingExemptionReasonID
		,BillingPaymentMethodID
		,ReasonForEconomicCodeID
		,DiscloseInformationID
		,DiscloseInformation
		,IdentityPapersReferenceID
		,DischargeDelayReasonID
		,DepositAmount
		,NilByMouthDateTime
		,TheatreVisitStatusID
		,UsualAccomodationTypeID
		,FinancialSubProgramID
		,EmploymentStatusID
		,PensionStatusID
		,FirstAdmissionToPsychUnitDateTime
		,PreviousAdmissionToPsychUnitDateTime
		,ConsentFlag
		,GPAdvisedFlag
		,FormReferenceNumber
		,AdmissionWeight
		,ICUHours
		,CCUHours
		,IntensionToReadmitID
		,AdmissionPaymentStatusID
		,DischargePaymentStatusID
		,AdmissionServiceCategoryID
		,DischargeServiceCategoryID
		,MechanicalVentilationHours
		,AmbulanceNumber
		,PriorDays
		,RelatedAdmission
		,AdmissionPatientClassificationID
		,CFWDDays
		,AccExpirtDateTime
		,ContractRoleID
		,ContractTypeID
		,CompensibleDetailID
		,ApplyVATToAll
		,ServiceUnitTypeID
		,HealthInsuranceStatusID
		,ClaimStatusID
		,DataLinkageConsentID
		,CarerAvailabilityID
		,ReferralForFurtherCareID
		,AccessLevelsID
		,BarthelScoreOnAdmissionID
		,BarthelScoreOnSeparationID
		,ClinicalSubProgramID
		,OnsetDate
		,ReadmissionToRehabFlag
		,SourceOfReferralToPalliativeCareID
		,RugadlScoreOnAdmissionID
		,RugadlScoreOnSeparationID
		,ICUTime
		,CCUTime
		,CHDID
		,PresentingProblemOnAdmissionID
		,PresentingProblemOnAdmission
		,NonInvasiveVentilationHours
		,AgedCareAssessmentID
		,CrisiResolutionServicesOfferedFlag
		,LegalStatusID
		,MentalCategoryOnTransferID
		,HARPFlag
		,TreatInitFlag
		,RTTStatusID
		,EarliestReasonableOfferDateTime
		,DelayedDischargeAttributedToID
		,HealthOrganisationOwnerID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		source.ProviderSpellID
		,source.SpecialtyID
		,source.ServicePointID
		,source.AdmissionMethodID
		,source.AdmissionSourceID
		,source.PatientClassificationID
		,source.ReadmissionReasonID
		,source.ConfidentialityLevelID
		,source.ProfessionalCarerID
		,source.PostMortemID
		,source.DischargeDestinationID
		,source.AdministrativeCategoryID
		,source.DischargeMethodID
		,source.PatientID
		,source.SpellTypeID
		,source.ReferralParagraphID
		,source.FollowUpPlanID
		,source.DischargeParagraphID
		,source.FirstPALLID
		,source.PreviousPALLID
		,source.AdmissionDateTime
		,source.DischargeDateTime
		,source.ProviderID
		,source.PurchaserID
		,source.ResponsibleHealthOrganisationID
		,source.ReferralID
		,source.AEAttendanceID
		,source.NotifiableAdmissionTypeID
		,source.IsFirstRegularAdmission
		,source.ManagementIntentionID
		,source.ExpectedDischargeDateTime
		,source.IsDisabled
		,source.IsConfidential
		,source.IsSilentReferral
		,source.PriorProfessionalCareEpisodeID
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.UserCreate
		,source.UserModify
		,source.EpisodeIdentifier
		,source.SessionTransactionID
		,source.PriorPointerID
		,source.AdmissionDecisionID
		,source.AdmissionOfferID
		,source.DecisionToAdmitDateTime
		,source.AddedToWaitingListDateTime
		,source.IsFastTrack
		,source.ContractID
		,source.ExternalKey
		,source.AgeBandID
		,source.ArrivalCarrierID
		,source.DischargeCarrierID
		,source.ArrivalTransportID
		,source.DepartTransportID
		,source.PACACID
		,source.MaternitySpellID
		,source.MentalCategoryOnAdmissionID
		,source.LegalStatusOnAdmissionID
		,source.MentalCategoryOnDischargeID
		,source.LegalStatusOnDischargeID
		,source.MentalHealthCareEpisodeID
		,source.LastEpisodeInSpellIdentifier
		,source.NoFundsFlag
		,source.ClinicalCostCentreID
		,source.IsProvisionalStart
		,source.IsProvisionalEnd
		,source.ContactProfessionalCarerID
		,source.MedicalDischargeDateTime
		,source.BillingExemptionCategoryID
		,source.BillingExemptionReasonID
		,source.BillingPaymentMethodID
		,source.ReasonForEconomicCodeID
		,source.DiscloseInformationID
		,source.DiscloseInformation
		,source.IdentityPapersReferenceID
		,source.DischargeDelayReasonID
		,source.DepositAmount
		,source.NilByMouthDateTime
		,source.TheatreVisitStatusID
		,source.UsualAccomodationTypeID
		,source.FinancialSubProgramID
		,source.EmploymentStatusID
		,source.PensionStatusID
		,source.FirstAdmissionToPsychUnitDateTime
		,source.PreviousAdmissionToPsychUnitDateTime
		,source.ConsentFlag
		,source.GPAdvisedFlag
		,source.FormReferenceNumber
		,source.AdmissionWeight
		,source.ICUHours
		,source.CCUHours
		,source.IntensionToReadmitID
		,source.AdmissionPaymentStatusID
		,source.DischargePaymentStatusID
		,source.AdmissionServiceCategoryID
		,source.DischargeServiceCategoryID
		,source.MechanicalVentilationHours
		,source.AmbulanceNumber
		,source.PriorDays
		,source.RelatedAdmission
		,source.AdmissionPatientClassificationID
		,source.CFWDDays
		,source.AccExpirtDateTime
		,source.ContractRoleID
		,source.ContractTypeID
		,source.CompensibleDetailID
		,source.ApplyVATToAll
		,source.ServiceUnitTypeID
		,source.HealthInsuranceStatusID
		,source.ClaimStatusID
		,source.DataLinkageConsentID
		,source.CarerAvailabilityID
		,source.ReferralForFurtherCareID
		,source.AccessLevelsID
		,source.BarthelScoreOnAdmissionID
		,source.BarthelScoreOnSeparationID
		,source.ClinicalSubProgramID
		,source.OnsetDate
		,source.ReadmissionToRehabFlag
		,source.SourceOfReferralToPalliativeCareID
		,source.RugadlScoreOnAdmissionID
		,source.RugadlScoreOnSeparationID
		,source.ICUTime
		,source.CCUTime
		,source.CHDID
		,source.PresentingProblemOnAdmissionID
		,source.PresentingProblemOnAdmission
		,source.NonInvasiveVentilationHours
		,source.AgedCareAssessmentID
		,source.CrisiResolutionServicesOfferedFlag
		,source.LegalStatusID
		,source.MentalCategoryOnTransferID
		,source.HARPFlag
		,source.TreatInitFlag
		,source.RTTStatusID
		,source.EarliestReasonableOfferDateTime
		,source.DelayedDischargeAttributedToID
		,source.HealthOrganisationOwnerID
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		 target.SpecialtyID
		,target.ServicePointID
		,target.AdmissionMethodID
		,target.AdmissionSourceID
		,target.PatientClassificationID
		,target.ReadmissionReasonID
		,target.ConfidentialityLevelID
		,target.ProfessionalCarerID
		,target.PostMortemID
		,target.DischargeDestinationID
		,target.AdministrativeCategoryID
		,target.DischargeMethodID
		,target.PatientID
		,target.SpellTypeID
		,target.ReferralParagraphID
		,target.FollowUpPlanID
		,target.DischargeParagraphID
		,target.FirstPALLID
		,target.PreviousPALLID
		,target.AdmissionDateTime
		,target.DischargeDateTime
		,target.ProviderID
		,target.PurchaserID
		,target.ResponsibleHealthOrganisationID
		,target.ReferralID
		,target.AEAttendanceID
		,target.NotifiableAdmissionTypeID
		,target.IsFirstRegularAdmission
		,target.ManagementIntentionID
		,target.ExpectedDischargeDateTime
		,target.IsDisabled
		,target.IsConfidential
		,target.IsSilentReferral
		,target.PriorProfessionalCareEpisodeID
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.EpisodeIdentifier
		,target.SessionTransactionID
		,target.PriorPointerID
		,target.AdmissionDecisionID
		,target.AdmissionOfferID
		,target.DecisionToAdmitDateTime
		,target.AddedToWaitingListDateTime
		,target.IsFastTrack
		,target.ContractID
		,target.ExternalKey
		,target.AgeBandID
		,target.ArrivalCarrierID
		,target.DischargeCarrierID
		,target.ArrivalTransportID
		,target.DepartTransportID
		,target.PACACID
		,target.MaternitySpellID
		,target.MentalCategoryOnAdmissionID
		,target.LegalStatusOnAdmissionID
		,target.MentalCategoryOnDischargeID
		,target.LegalStatusOnDischargeID
		,target.MentalHealthCareEpisodeID
		,target.LastEpisodeInSpellIdentifier
		,target.NoFundsFlag
		,target.ClinicalCostCentreID
		,target.IsProvisionalStart
		,target.IsProvisionalEnd
		,target.ContactProfessionalCarerID
		,target.MedicalDischargeDateTime
		,target.BillingExemptionCategoryID
		,target.BillingExemptionReasonID
		,target.BillingPaymentMethodID
		,target.ReasonForEconomicCodeID
		,target.DiscloseInformationID
		,target.DiscloseInformation
		,target.IdentityPapersReferenceID
		,target.DischargeDelayReasonID
		,target.DepositAmount
		,target.NilByMouthDateTime
		,target.TheatreVisitStatusID
		,target.UsualAccomodationTypeID
		,target.FinancialSubProgramID
		,target.EmploymentStatusID
		,target.PensionStatusID
		,target.FirstAdmissionToPsychUnitDateTime
		,target.PreviousAdmissionToPsychUnitDateTime
		,target.ConsentFlag
		,target.GPAdvisedFlag
		,target.FormReferenceNumber
		,target.AdmissionWeight
		,target.ICUHours
		,target.CCUHours
		,target.IntensionToReadmitID
		,target.AdmissionPaymentStatusID
		,target.DischargePaymentStatusID
		,target.AdmissionServiceCategoryID
		,target.DischargeServiceCategoryID
		,target.MechanicalVentilationHours
		,target.AmbulanceNumber
		,target.PriorDays
		,target.RelatedAdmission
		,target.AdmissionPatientClassificationID
		,target.CFWDDays
		,target.AccExpirtDateTime
		,target.ContractRoleID
		,target.ContractTypeID
		,target.CompensibleDetailID
		,target.ApplyVATToAll
		,target.ServiceUnitTypeID
		,target.HealthInsuranceStatusID
		,target.ClaimStatusID
		,target.DataLinkageConsentID
		,target.CarerAvailabilityID
		,target.ReferralForFurtherCareID
		,target.AccessLevelsID
		,target.BarthelScoreOnAdmissionID
		,target.BarthelScoreOnSeparationID
		,target.ClinicalSubProgramID
		,target.OnsetDate
		,target.ReadmissionToRehabFlag
		,target.SourceOfReferralToPalliativeCareID
		,target.RugadlScoreOnAdmissionID
		,target.RugadlScoreOnSeparationID
		,target.ICUTime
		,target.CCUTime
		,target.CHDID
		,target.PresentingProblemOnAdmissionID
		,target.PresentingProblemOnAdmission
		,target.NonInvasiveVentilationHours
		,target.AgedCareAssessmentID
		,target.CrisiResolutionServicesOfferedFlag
		,target.LegalStatusID
		,target.MentalCategoryOnTransferID
		,target.HARPFlag
		,target.TreatInitFlag
		,target.RTTStatusID
		,target.EarliestReasonableOfferDateTime
		,target.DelayedDischargeAttributedToID
		,target.HealthOrganisationOwnerID
		,target.LorenzoCreated
		)

then
	update
	set
		target.SpecialtyID = source.SpecialtyID
		,target.ServicePointID = source.ServicePointID
		,target.AdmissionMethodID = source.AdmissionMethodID
		,target.AdmissionSourceID = source.AdmissionSourceID
		,target.PatientClassificationID = source.PatientClassificationID
		,target.ReadmissionReasonID = source.ReadmissionReasonID
		,target.ConfidentialityLevelID = source.ConfidentialityLevelID
		,target.ProfessionalCarerID = source.ProfessionalCarerID
		,target.PostMortemID = source.PostMortemID
		,target.DischargeDestinationID = source.DischargeDestinationID
		,target.AdministrativeCategoryID = source.AdministrativeCategoryID
		,target.DischargeMethodID = source.DischargeMethodID
		,target.PatientID = source.PatientID
		,target.SpellTypeID = source.SpellTypeID
		,target.ReferralParagraphID = source.ReferralParagraphID
		,target.FollowUpPlanID = source.FollowUpPlanID
		,target.DischargeParagraphID = source.DischargeParagraphID
		,target.FirstPALLID = source.FirstPALLID
		,target.PreviousPALLID = source.PreviousPALLID
		,target.AdmissionDateTime = source.AdmissionDateTime
		,target.DischargeDateTime = source.DischargeDateTime
		,target.ProviderID = source.ProviderID
		,target.PurchaserID = source.PurchaserID
		,target.ResponsibleHealthOrganisationID = source.ResponsibleHealthOrganisationID
		,target.ReferralID = source.ReferralID
		,target.AEAttendanceID = source.AEAttendanceID
		,target.NotifiableAdmissionTypeID = source.NotifiableAdmissionTypeID
		,target.IsFirstRegularAdmission = source.IsFirstRegularAdmission
		,target.ManagementIntentionID = source.ManagementIntentionID
		,target.ExpectedDischargeDateTime = source.ExpectedDischargeDateTime
		,target.IsDisabled = source.IsDisabled
		,target.IsConfidential = source.IsConfidential
		,target.IsSilentReferral = source.IsSilentReferral
		,target.PriorProfessionalCareEpisodeID = source.PriorProfessionalCareEpisodeID
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.EpisodeIdentifier = source.EpisodeIdentifier
		,target.SessionTransactionID = source.SessionTransactionID
		,target.PriorPointerID = source.PriorPointerID
		,target.AdmissionDecisionID = source.AdmissionDecisionID
		,target.AdmissionOfferID = source.AdmissionOfferID
		,target.DecisionToAdmitDateTime = source.DecisionToAdmitDateTime
		,target.AddedToWaitingListDateTime = source.AddedToWaitingListDateTime
		,target.IsFastTrack = source.IsFastTrack
		,target.ContractID = source.ContractID
		,target.ExternalKey = source.ExternalKey
		,target.AgeBandID = source.AgeBandID
		,target.ArrivalCarrierID = source.ArrivalCarrierID
		,target.DischargeCarrierID = source.DischargeCarrierID
		,target.ArrivalTransportID = source.ArrivalTransportID
		,target.DepartTransportID = source.DepartTransportID
		,target.PACACID = source.PACACID
		,target.MaternitySpellID = source.MaternitySpellID
		,target.MentalCategoryOnAdmissionID = source.MentalCategoryOnAdmissionID
		,target.LegalStatusOnAdmissionID = source.LegalStatusOnAdmissionID
		,target.MentalCategoryOnDischargeID = source.MentalCategoryOnDischargeID
		,target.LegalStatusOnDischargeID = source.LegalStatusOnDischargeID
		,target.MentalHealthCareEpisodeID = source.MentalHealthCareEpisodeID
		,target.LastEpisodeInSpellIdentifier = source.LastEpisodeInSpellIdentifier
		,target.NoFundsFlag = source.NoFundsFlag
		,target.ClinicalCostCentreID = source.ClinicalCostCentreID
		,target.IsProvisionalStart = source.IsProvisionalStart
		,target.IsProvisionalEnd = source.IsProvisionalEnd
		,target.ContactProfessionalCarerID = source.ContactProfessionalCarerID
		,target.MedicalDischargeDateTime = source.MedicalDischargeDateTime
		,target.BillingExemptionCategoryID = source.BillingExemptionCategoryID
		,target.BillingExemptionReasonID = source.BillingExemptionReasonID
		,target.BillingPaymentMethodID = source.BillingPaymentMethodID
		,target.ReasonForEconomicCodeID = source.ReasonForEconomicCodeID
		,target.DiscloseInformationID = source.DiscloseInformationID
		,target.DiscloseInformation = source.DiscloseInformation
		,target.IdentityPapersReferenceID = source.IdentityPapersReferenceID
		,target.DischargeDelayReasonID = source.DischargeDelayReasonID
		,target.DepositAmount = source.DepositAmount
		,target.NilByMouthDateTime = source.NilByMouthDateTime
		,target.TheatreVisitStatusID = source.TheatreVisitStatusID
		,target.UsualAccomodationTypeID = source.UsualAccomodationTypeID
		,target.FinancialSubProgramID = source.FinancialSubProgramID
		,target.EmploymentStatusID = source.EmploymentStatusID
		,target.PensionStatusID = source.PensionStatusID
		,target.FirstAdmissionToPsychUnitDateTime = source.FirstAdmissionToPsychUnitDateTime
		,target.PreviousAdmissionToPsychUnitDateTime = source.PreviousAdmissionToPsychUnitDateTime
		,target.ConsentFlag = source.ConsentFlag
		,target.GPAdvisedFlag = source.GPAdvisedFlag
		,target.FormReferenceNumber = source.FormReferenceNumber
		,target.AdmissionWeight = source.AdmissionWeight
		,target.ICUHours = source.ICUHours
		,target.CCUHours = source.CCUHours
		,target.IntensionToReadmitID = source.IntensionToReadmitID
		,target.AdmissionPaymentStatusID = source.AdmissionPaymentStatusID
		,target.DischargePaymentStatusID = source.DischargePaymentStatusID
		,target.AdmissionServiceCategoryID = source.AdmissionServiceCategoryID
		,target.DischargeServiceCategoryID = source.DischargeServiceCategoryID
		,target.MechanicalVentilationHours = source.MechanicalVentilationHours
		,target.AmbulanceNumber = source.AmbulanceNumber
		,target.PriorDays = source.PriorDays
		,target.RelatedAdmission = source.RelatedAdmission
		,target.AdmissionPatientClassificationID = source.AdmissionPatientClassificationID
		,target.CFWDDays = source.CFWDDays
		,target.AccExpirtDateTime = source.AccExpirtDateTime
		,target.ContractRoleID = source.ContractRoleID
		,target.ContractTypeID = source.ContractTypeID
		,target.CompensibleDetailID = source.CompensibleDetailID
		,target.ApplyVATToAll = source.ApplyVATToAll
		,target.ServiceUnitTypeID = source.ServiceUnitTypeID
		,target.HealthInsuranceStatusID = source.HealthInsuranceStatusID
		,target.ClaimStatusID = source.ClaimStatusID
		,target.DataLinkageConsentID = source.DataLinkageConsentID
		,target.CarerAvailabilityID = source.CarerAvailabilityID
		,target.ReferralForFurtherCareID = source.ReferralForFurtherCareID
		,target.AccessLevelsID = source.AccessLevelsID
		,target.BarthelScoreOnAdmissionID = source.BarthelScoreOnAdmissionID
		,target.BarthelScoreOnSeparationID = source.BarthelScoreOnSeparationID
		,target.ClinicalSubProgramID = source.ClinicalSubProgramID
		,target.OnsetDate = source.OnsetDate
		,target.ReadmissionToRehabFlag = source.ReadmissionToRehabFlag
		,target.SourceOfReferralToPalliativeCareID = source.SourceOfReferralToPalliativeCareID
		,target.RugadlScoreOnAdmissionID = source.RugadlScoreOnAdmissionID
		,target.RugadlScoreOnSeparationID = source.RugadlScoreOnSeparationID
		,target.ICUTime = source.ICUTime
		,target.CCUTime = source.CCUTime
		,target.CHDID = source.CHDID
		,target.PresentingProblemOnAdmissionID = source.PresentingProblemOnAdmissionID
		,target.PresentingProblemOnAdmission = source.PresentingProblemOnAdmission
		,target.NonInvasiveVentilationHours = source.NonInvasiveVentilationHours
		,target.AgedCareAssessmentID = source.AgedCareAssessmentID
		,target.CrisiResolutionServicesOfferedFlag = source.CrisiResolutionServicesOfferedFlag
		,target.LegalStatusID = source.LegalStatusID
		,target.MentalCategoryOnTransferID = source.MentalCategoryOnTransferID
		,target.HARPFlag = source.HARPFlag
		,target.TreatInitFlag = source.TreatInitFlag
		,target.RTTStatusID = source.RTTStatusID
		,target.EarliestReasonableOfferDateTime = source.EarliestReasonableOfferDateTime
		,target.DelayedDischargeAttributedToID = source.DelayedDischargeAttributedToID
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime