﻿CREATE procedure [Lorenzo].[LoadRTT] as 


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(RTT.LorenzoCreated)
		from
			Lorenzo.RTT
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 ReferralID
			,PatientID
			,SourceCode
			,SourceID
			,StatusDateTime
			,StatusID
			,Comments
			,CreatedDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,SessionTransactionID
			,ExternalKey
			,HealthOrganisationOwnerID
			,LorenzoCreated
			)
into
	#TLoadPASRTT
from
	(
	select
		RTTID
		,ReferralID = cast(ReferralID as int)
		,PatientID = cast(PatientID as int)
		,SourceCode = cast(nullif(SourceCode, '') as varchar(10))
		,SourceID = cast(SourceID as int)
		,StatusDateTime = cast(StatusDateTime as datetime)
		,StatusID = cast(StatusID as int)
		,Comments = cast(nullif(Comments, '') as varchar(255))
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(30))
		,UserModify = cast(nullif(UserModify, '') as varchar(30))
		,SessionTransactionID = cast(SessionTransactionID as int)
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(20))
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as int)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			RTTID = RTTPR_REFNO
			,ReferralID = REFRL_REFNO
			,PatientID = PATNT_REFNO
			,SourceCode = SORCE
			,SourceID = SORCE_REFNO
			,StatusDateTime = RTTST_DATE
			,StatusID = RTTST_REFNO
			,Comments = COMMENTS
			,CreatedDateTime = CREATE_DTTM
			,ModifiedDateTime = MODIF_DTTM
			,UserCreate = USER_CREATE
			,UserModify = USER_MODIF
			,SessionTransactionID = STRAN_REFNO
			,ExternalKey = EXTERNAL_KEY
			,HealthOrganisationOwnerID = OWNER_HEORG_REFNO
			,LorenzoCreated = Created

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.RTT
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASRTT on #TLoadPASRTT
	(
	RTTID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.RTT target
using
	(
	select
		*
	from
		#TLoadPASRTT
	
	) source
	on	source.RTTID = target.RTTID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		RTTID
		,ReferralID
		,PatientID
		,SourceCode
		,SourceID
		,StatusDateTime
		,StatusID
		,Comments
		,CreatedDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,SessionTransactionID
		,ExternalKey
		,HealthOrganisationOwnerID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		source.RTTID
		,source.ReferralID
		,source.PatientID
		,source.SourceCode
		,source.SourceID
		,source.StatusDateTime
		,source.StatusID
		,source.Comments
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.UserCreate
		,source.UserModify
		,source.SessionTransactionID
		,source.ExternalKey
		,source.HealthOrganisationOwnerID
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.ReferralID
		,target.PatientID
		,target.SourceCode
		,target.SourceID
		,target.StatusDateTime
		,target.StatusID
		,target.Comments
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.SessionTransactionID
		,target.ExternalKey
		,target.HealthOrganisationOwnerID
		,target.LorenzoCreated
		)

then
	update
	set
		target.ReferralID = source.ReferralID
		,target.PatientID = source.PatientID
		,target.SourceCode = source.SourceCode
		,target.SourceID = source.SourceID
		,target.StatusDateTime = source.StatusDateTime
		,target.StatusID = source.StatusID
		,target.Comments = source.Comments
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.SessionTransactionID = source.SessionTransactionID
		,target.ExternalKey = source.ExternalKey
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime