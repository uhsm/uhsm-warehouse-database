﻿CREATE procedure [Lorenzo].[LoadReferral] as 


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(Referral.LorenzoCreated)
		from
			Lorenzo.Referral
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			ReferredBySpecialtyID
			,ReferredToSpecialtyID
			,ProviderID
			,ContractID
			,PurchaserID
			,PatientID
			,PriorProfessionalCareEpisodeID
			,SourceOfReferralID
			,ReceivedDateTime
			,ReasonForReferral
			,CancelledDateTime
			,CancelReasonID
			,Comments
			,SentDateTime
			,ReferredByReference
			,UrgencyID
			,PriorityID
			,AuthorisationID
			,AuthorisationDateTime
			,LetterSentFlag
			,OriginalHospitalReceivedDateTime
			,ReferralEndDateTime
			,AuthorisationFlag
			,CreatedDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,ReferralClosureDateTime
			,OriginalDescisionToAdmitDateTime
			,ReferredByProfessionalCarerID
			,ReferredByHealthOrganisationID
			,ReferredToProfessionalCarerID
			,ReferredToHealthOrganisationID
			,ReferredToStaffTeamID
			,ReferredByStaffTeamID
			,ReferredToReference
			,ReferralOutcomeID
			,CampusID
			,SessionTransactionID
			,PriorPointerID
			,ExternalKey
			,ReferralStatusID
			,ReferralRejectStatusID
			,AuthorisationProfessionalCarerID
			,AuthorisationHealthOrganisationID
			,AuthorisationStaffTeamID
			,AuthorisationSpecialtyID
			,ResponsibleProfessionalCarerID
			,ResponsibleHealthOrganisationID
			,ResponsibleStaffTeamID
			,ResponsibleSpecialtyID
			,MentalHealthCareEpisodeID
			,ContractServiceID
			,PeriodOfCareID
			,ReferralTypeID
			,ParentID
			,LastContactDateTime
			,NextContactDateTime
			,SuspensionStartDateTime
			,SuspensionEndDateTime
			,SuspensionReasonID
			,RequestUrgencyForAuthorisationID
			,ECRRequestStatusID
			,ECRRequestStatusDateTime
			,LastGPConsultationDateTime
			,CodingStatusID
			,AdministrativeCategoryID
			,ActivityTypeID
			,ClinicalPriority
			,AccidentRelatedStatusID
			,AccidentTypeID
			,AccidentNumber
			,AccidentApprovalStatusID
			,AccidentClaimNumber
			,ExpirtDateTime
			,ReferredToWardID
			,ReferredToClinicID
			,Referral
			,ClinicalManagementStatusID
			,CloseReasonID
			,AccessLevelID
			,LastNonAppServerModifiedDateTime
			,UserID
			,ReferralMediumID
			,ParentReferralID
			,FirstSeenDelayReasonID
			,ReferralToTreatmentDelayReasonID
			,DescisionToTreatmentDelayReasonID
			,SuspectedCancerTypeID
			,PrimaryClientTypeID
			,PrimaryClientSubTypeID
			,IsServiceInitiated
			,ServiceInitiatedDateTime
			,SeenDateTime
			,AssessmentDateTime
			,CareAllocationDateTime
			,InactiveDateTime
			,IsEnhancedAudit
			,StatusDateTime
			,PatientPathwayID
			,RTTStartDateTime
			,RTTStarted
			,RTTStatusID
			,PatientPathwayIdentifierOriginalOrganisationCode
			,IsPatientPathwayIdentifierAvailable
			,SpecifiedGPReferenceNumber
			,IsDirectAccess
			,SuperClusterID
			,HasLongTermCondition
			,BritishArmedForcesID
			,PreviousSymptomID
			,Disability
			,PreviousSymptom
			,IsCommunityActivity
			,ServiceTypeID
			,HealthOrganisationOwnerID
			,LorenzoCreated
			)
into
	#TLoadPASReferral
from
	(
	select
		ReferralID
		,ReferredBySpecialtyID = cast(ReferredBySpecialtyID as int)
		,ReferredToSpecialtyID = cast(ReferredToSpecialtyID as int)
		,ProviderID = cast(ProviderID as int)
		,ContractID = cast(ContractID as int)
		,PurchaserID = cast(PurchaserID as int)
		,PatientID = cast(PatientID as int)
		,PriorProfessionalCareEpisodeID = cast(PriorProfessionalCareEpisodeID as int)
		,SourceOfReferralID = cast(SourceOfReferralID as int)
		,ReceivedDateTime = cast(ReceivedDateTime as datetime)
		,ReasonForReferral = cast(ReasonForReferral as int)
		,CancelledDateTime = cast(CancelledDateTime as datetime)
		,CancelReasonID = cast(CancelReasonID as int)
		,Comments = cast(nullif(Comments, '') as varchar(255))
		,SentDateTime = cast(SentDateTime as datetime)
		,ReferredByReference = cast(nullif(ReferredByReference, '') as varchar(50))
		,UrgencyID = cast(UrgencyID as int)
		,PriorityID = cast(PriorityID as int)
		,AuthorisationID = cast(AuthorisationID as int)
		,AuthorisationDateTime = cast(AuthorisationDateTime as datetime)
		,LetterSentFlag = cast(nullif(LetterSentFlag, '') as char(1))
		,OriginalHospitalReceivedDateTime = cast(OriginalHospitalReceivedDateTime as datetime)
		,ReferralEndDateTime = cast(ReferralEndDateTime as datetime)
		,AuthorisationFlag = cast(nullif(AuthorisationFlag, '') as char(1))
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(50))
		,UserModify = cast(nullif(UserModify, '') as varchar(50))
		,ReferralClosureDateTime = cast(ReferralClosureDateTime as datetime)
		,OriginalDescisionToAdmitDateTime = cast(OriginalDescisionToAdmitDateTime as datetime)
		,ReferredByProfessionalCarerID = cast(ReferredByProfessionalCarerID as int)
		,ReferredByHealthOrganisationID = cast(ReferredByHealthOrganisationID as int)
		,ReferredToProfessionalCarerID = cast(ReferredToProfessionalCarerID as int)
		,ReferredToHealthOrganisationID = cast(ReferredToHealthOrganisationID as int)
		,ReferredToStaffTeamID = cast(ReferredToStaffTeamID as int)
		,ReferredByStaffTeamID = cast(ReferredByStaffTeamID as int)
		,ReferredToReference = cast(nullif(ReferredToReference, '') as varchar(50))
		,ReferralOutcomeID = cast(ReferralOutcomeID as int)
		,CampusID = cast(CampusID as int)
		,SessionTransactionID = cast(SessionTransactionID as int)
		,PriorPointerID = cast(PriorPointerID as int)
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(50))
		,ReferralStatusID = cast(ReferralStatusID as int)
		,ReferralRejectStatusID = cast(ReferralRejectStatusID as int)
		,AuthorisationProfessionalCarerID = cast(AuthorisationProfessionalCarerID as int)
		,AuthorisationHealthOrganisationID = cast(AuthorisationHealthOrganisationID as int)
		,AuthorisationStaffTeamID = cast(AuthorisationStaffTeamID as int)
		,AuthorisationSpecialtyID = cast(AuthorisationSpecialtyID as int)
		,ResponsibleProfessionalCarerID = cast(ResponsibleProfessionalCarerID as int)
		,ResponsibleHealthOrganisationID = cast(ResponsibleHealthOrganisationID as int)
		,ResponsibleStaffTeamID = cast(ResponsibleStaffTeamID as int)
		,ResponsibleSpecialtyID = cast(ResponsibleSpecialtyID as int)
		,MentalHealthCareEpisodeID = cast(MentalHealthCareEpisodeID as int)
		,ContractServiceID = cast(ContractServiceID as int)
		,PeriodOfCareID = cast(PeriodOfCareID as int)
		,ReferralTypeID = cast(ReferralTypeID as int)
		,ParentID = cast(ParentID as int)
		,LastContactDateTime = cast(LastContactDateTime as datetime)
		,NextContactDateTime = cast(NextContactDateTime as datetime)
		,SuspensionStartDateTime = cast(SuspensionStartDateTime as datetime)
		,SuspensionEndDateTime = cast(SuspensionEndDateTime as datetime)
		,SuspensionReasonID = cast(SuspensionReasonID as int)
		,RequestUrgencyForAuthorisationID = cast(RequestUrgencyForAuthorisationID as int)
		,ECRRequestStatusID = cast(ECRRequestStatusID as int)
		,ECRRequestStatusDateTime = cast(ECRRequestStatusDateTime as datetime)
		,LastGPConsultationDateTime = cast(LastGPConsultationDateTime as datetime)
		,CodingStatusID = cast(CodingStatusID as int)
		,AdministrativeCategoryID = cast(AdministrativeCategoryID as int)
		,ActivityTypeID = cast(ActivityTypeID as int)
		,ClinicalPriority = cast(ClinicalPriority as int)
		,AccidentRelatedStatusID = cast(AccidentRelatedStatusID as int)
		,AccidentTypeID = cast(AccidentTypeID as int)
		,AccidentNumber = cast(nullif(AccidentNumber, '') as varchar(50))
		,AccidentApprovalStatusID = cast(AccidentApprovalStatusID as int)
		,AccidentClaimNumber = cast(nullif(AccidentClaimNumber, '') as varchar(50))
		,ExpirtDateTime = cast(ExpirtDateTime as datetime)
		,ReferredToWardID = cast(ReferredToWardID as int)
		,ReferredToClinicID = cast(ReferredToClinicID as int)
		,Referral = cast(nullif(Referral, '') as varchar(80))
		,ClinicalManagementStatusID = cast(ClinicalManagementStatusID as int)
		,CloseReasonID = cast(CloseReasonID as int)
		,AccessLevelID = cast(AccessLevelID as int)
		,LastNonAppServerModifiedDateTime = cast(LastNonAppServerModifiedDateTime as datetime)
		,UserID = cast(UserID as int)
		,ReferralMediumID = cast(ReferralMediumID as int)
		,ParentReferralID = cast(ParentReferralID as int)
		,FirstSeenDelayReasonID = cast(FirstSeenDelayReasonID as int)
		,ReferralToTreatmentDelayReasonID = cast(ReferralToTreatmentDelayReasonID as int)
		,DescisionToTreatmentDelayReasonID = cast(DescisionToTreatmentDelayReasonID as int)
		,SuspectedCancerTypeID = cast(SuspectedCancerTypeID as int)
		,PrimaryClientTypeID = cast(PrimaryClientTypeID as int)
		,PrimaryClientSubTypeID = cast(PrimaryClientSubTypeID as int)
		,IsServiceInitiated = cast(nullif(IsServiceInitiated, '') as char(1))
		,ServiceInitiatedDateTime = cast(ServiceInitiatedDateTime as datetime)
		,SeenDateTime = cast(SeenDateTime as datetime)
		,AssessmentDateTime = cast(AssessmentDateTime as datetime)
		,CareAllocationDateTime = cast(CareAllocationDateTime as datetime)
		,InactiveDateTime = cast(InactiveDateTime as datetime)
		,IsEnhancedAudit = cast(nullif(IsEnhancedAudit, '') as char(1))
		,StatusDateTime = cast(StatusDateTime as datetime)
		,PatientPathwayID = cast(nullif(PatientPathwayID, '') as varchar(300))
		,RTTStartDateTime = cast(RTTStartDateTime as datetime)
		,RTTStarted = cast(nullif(RTTStarted, '') as char(1))
		,RTTStatusID = cast(RTTStatusID as int)
		,PatientPathwayIdentifierOriginalOrganisationCode = cast(nullif(PatientPathwayIdentifierOriginalOrganisationCode, '') as varchar(50))
		,IsPatientPathwayIdentifierAvailable = cast(nullif(IsPatientPathwayIdentifierAvailable, '') as char(1))
		,SpecifiedGPReferenceNumber = cast(SpecifiedGPReferenceNumber as int)
		,IsDirectAccess = cast(nullif(IsDirectAccess, '') as char(1))
		,SuperClusterID = cast(SuperClusterID as int)
		,HasLongTermCondition = cast(nullif(HasLongTermCondition, '') as varchar(50))
		,BritishArmedForcesID = cast(BritishArmedForcesID as int)
		,PreviousSymptomID = cast(PreviousSymptomID as int)
		,Disability = cast(nullif(Disability, '') as varchar(255))
		,PreviousSymptom = cast(nullif(PreviousSymptom, '') as varchar(50))
		,IsCommunityActivity = cast(nullif(IsCommunityActivity, '') as char(1))
		,ServiceTypeID = cast(ServiceTypeID as int)
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as int)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			ReferralID = REFRL_REFNO
			,ReferredBySpecialtyID = REFBY_SPECT_REFNO
			,ReferredToSpecialtyID = REFTO_SPECT_REFNO
			,ProviderID = PROVD_REFNO
			,ContractID = CONTR_REFNO
			,PurchaserID = PURCH_REFNO
			,PatientID = PATNT_REFNO
			,PriorProfessionalCareEpisodeID = PRIOR_REFNO
			,SourceOfReferralID = SORRF_REFNO
			,ReceivedDateTime = RECVD_DTTM
			,ReasonForReferral = REASN_REFNO
			,CancelledDateTime = CANCD_DTTM
			,CancelReasonID = CANRS_REFNO
			,Comments = COMMENTS
			,SentDateTime = SENT_DTTM
			,ReferredByReference = REFBY_REFERENCE
			,UrgencyID = URGNC_REFNO
			,PriorityID = PRITY_REFNO
			,AuthorisationID = AUTHR_REFNO
			,AuthorisationDateTime = AUTHR_DTTM
			,LetterSentFlag = LETTER_SENT_FLAG
			,OriginalHospitalReceivedDateTime = OTHER_HORSC_DTTM
			,ReferralEndDateTime = WAIT_END_DTTM
			,AuthorisationFlag = AUTHR_FLAG
			,CreatedDateTime = CREATE_DTTM
			,ModifiedDateTime = MODIF_DTTM
			,UserCreate = USER_CREATE
			,UserModify = USER_MODIF
			,ReferralClosureDateTime = CLOSR_DATE
			,OriginalDescisionToAdmitDateTime = ORDTA_DTTM
			,ReferredByProfessionalCarerID = REFBY_PROCA_REFNO
			,ReferredByHealthOrganisationID = REFBY_HEORG_REFNO
			,ReferredToProfessionalCarerID = REFTO_PROCA_REFNO
			,ReferredToHealthOrganisationID = REFTO_HEORG_REFNO
			,ReferredToStaffTeamID = REFTO_STEAM_REFNO
			,ReferredByStaffTeamID = REFBY_STEAM_REFNO
			,ReferredToReference = REFTO_REFERENCE
			,ReferralOutcomeID = RFOCM_REFNO
			,CampusID = CAMPUS_REFNO
			,SessionTransactionID = STRAN_REFNO
			,PriorPointerID = PRIOR_POINTER
			,ExternalKey = EXTERNAL_KEY
			,ReferralStatusID = RSTAT_REFNO
			,ReferralRejectStatusID = RJECT_REFNO
			,AuthorisationProfessionalCarerID = AUTHR_PROCA_REFNO
			,AuthorisationHealthOrganisationID = AUTHR_HEORG_REFNO
			,AuthorisationStaffTeamID = AUTHR_STEAM_REFNO
			,AuthorisationSpecialtyID = AUTHR_SPECT_REFNO
			,ResponsibleProfessionalCarerID = RESP_PROCA_REFNO
			,ResponsibleHealthOrganisationID = RESP_HEORG_REFNO
			,ResponsibleStaffTeamID = RESP_STEAM_REFNO
			,ResponsibleSpecialtyID = RESP_SPECT_REFNO
			,MentalHealthCareEpisodeID = MHCEP_REFNO
			,ContractServiceID = CTRSV_REFNO
			,PeriodOfCareID = POCAR_REFNO
			,ReferralTypeID = RETYP_REFNO
			,ParentID = PARNT_REFNO
			,LastContactDateTime = LAST_CONTACT_DTTM
			,NextContactDateTime = NEXT_CONTACT_DTTM
			,SuspensionStartDateTime = SUSP_START_DTTM
			,SuspensionEndDateTime = SUSP_END_DTTM
			,SuspensionReasonID = SUSRS_REFNO
			,RequestUrgencyForAuthorisationID = RQURG_REFNO
			,ECRRequestStatusID = AUSTS_REFNO
			,ECRRequestStatusDateTime = AURQS_DTTM
			,LastGPConsultationDateTime = CNSLT_DTTM
			,CodingStatusID = CSTAT_REFNO
			,AdministrativeCategoryID = ADCAT_REFNO
			,ActivityTypeID = ACTYP_REFNO
			,ClinicalPriority = CLINICAL_PRIORITY
			,AccidentRelatedStatusID = ACCRL_REFNO
			,AccidentTypeID = ACCTY_REFNO
			,AccidentNumber = ACC_NUMBER
			,AccidentApprovalStatusID = ACCAS_REFNO
			,AccidentClaimNumber = ACC_CLAIM_NUMBER
			,ExpirtDateTime = EXPIRY_DTTM
			,ReferredToWardID = REFTO_WARD_SPONT_REFNO
			,ReferredToClinicID = REFTO_CLINIC_SPONT_REFNO
			,Referral = DESCRIPTION
			,ClinicalManagementStatusID = CLMAS_REFNO
			,CloseReasonID = CLORS_REFNO
			,AccessLevelID = ACLEV_REFNO
			,LastNonAppServerModifiedDateTime = NON_APPSVR_MODIF_DTTM
			,UserID = USER_REFNO
			,ReferralMediumID = RFMED_REFNO
			,ParentReferralID = PARNT_REFRL_REFNO
			,FirstSeenDelayReasonID = FSDRS_REFNO
			,ReferralToTreatmentDelayReasonID = RTDRS_REFNO
			,DescisionToTreatmentDelayReasonID = DTDRS_REFNO
			,SuspectedCancerTypeID = UCTYP_REFNO
			,PrimaryClientTypeID = PCLTY_REFNO
			,PrimaryClientSubTypeID = PCLSB_REFNO
			,IsServiceInitiated = SERV_INIT_FLAG
			,ServiceInitiatedDateTime = SERV_INIT_DTTM
			,SeenDateTime = SEEN_DTTM
			,AssessmentDateTime = ASST_DTTM
			,CareAllocationDateTime = CATGET_DTTM
			,InactiveDateTime = INACTIVE_DTTM
			,IsEnhancedAudit = EFLAG
			,StatusDateTime = STATUS_DTTM
			,PatientPathwayID = PATNT_PATHWAY_ID
			,RTTStartDateTime = RTT_START_DATE
			,RTTStarted = RTT_STARTED
			,RTTStatusID = RTTST_REFNO
			,PatientPathwayIdentifierOriginalOrganisationCode = PPID_ORIGIN_ORG_CODE
			,IsPatientPathwayIdentifierAvailable = PPID_DATA_UNAVAILABLE
			,SpecifiedGPReferenceNumber = SPECIFIED_GP_REFNO
			,IsDirectAccess = DIRECT_ACCESS
			,SuperClusterID = SUCLU_REFNO
			,HasLongTermCondition = LONG_TERM_CONDN
			,BritishArmedForcesID = BAFIN_REFNO
			,PreviousSymptomID = PRESI_REFNO
			,Disability = DISABLITY
			,PreviousSymptom = YEAR_MONTH_SYMPTOM
			,IsCommunityActivity = COMMUNITY_FLAG
			,ServiceTypeID = CSVTP_REFNO
			,HealthOrganisationOwnerID = OWNER_HEORG_REFNO
			,LorenzoCreated = Created


			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.Referral
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASReferral on #TLoadPASReferral
	(
	ReferralID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.Referral target
using
	(
	select
		*
	from
		#TLoadPASReferral
	
	) source
	on	source.ReferralID = target.ReferralID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		ReferralID
		,ReferredBySpecialtyID
		,ReferredToSpecialtyID
		,ProviderID
		,ContractID
		,PurchaserID
		,PatientID
		,PriorProfessionalCareEpisodeID
		,SourceOfReferralID
		,ReceivedDateTime
		,ReasonForReferral
		,CancelledDateTime
		,CancelReasonID
		,Comments
		,SentDateTime
		,ReferredByReference
		,UrgencyID
		,PriorityID
		,AuthorisationID
		,AuthorisationDateTime
		,LetterSentFlag
		,OriginalHospitalReceivedDateTime
		,ReferralEndDateTime
		,AuthorisationFlag
		,CreatedDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,ReferralClosureDateTime
		,OriginalDescisionToAdmitDateTime
		,ReferredByProfessionalCarerID
		,ReferredByHealthOrganisationID
		,ReferredToProfessionalCarerID
		,ReferredToHealthOrganisationID
		,ReferredToStaffTeamID
		,ReferredByStaffTeamID
		,ReferredToReference
		,ReferralOutcomeID
		,CampusID
		,SessionTransactionID
		,PriorPointerID
		,ExternalKey
		,ReferralStatusID
		,ReferralRejectStatusID
		,AuthorisationProfessionalCarerID
		,AuthorisationHealthOrganisationID
		,AuthorisationStaffTeamID
		,AuthorisationSpecialtyID
		,ResponsibleProfessionalCarerID
		,ResponsibleHealthOrganisationID
		,ResponsibleStaffTeamID
		,ResponsibleSpecialtyID
		,MentalHealthCareEpisodeID
		,ContractServiceID
		,PeriodOfCareID
		,ReferralTypeID
		,ParentID
		,LastContactDateTime
		,NextContactDateTime
		,SuspensionStartDateTime
		,SuspensionEndDateTime
		,SuspensionReasonID
		,RequestUrgencyForAuthorisationID
		,ECRRequestStatusID
		,ECRRequestStatusDateTime
		,LastGPConsultationDateTime
		,CodingStatusID
		,AdministrativeCategoryID
		,ActivityTypeID
		,ClinicalPriority
		,AccidentRelatedStatusID
		,AccidentTypeID
		,AccidentNumber
		,AccidentApprovalStatusID
		,AccidentClaimNumber
		,ExpirtDateTime
		,ReferredToWardID
		,ReferredToClinicID
		,Referral
		,ClinicalManagementStatusID
		,CloseReasonID
		,AccessLevelID
		,LastNonAppServerModifiedDateTime
		,UserID
		,ReferralMediumID
		,ParentReferralID
		,FirstSeenDelayReasonID
		,ReferralToTreatmentDelayReasonID
		,DescisionToTreatmentDelayReasonID
		,SuspectedCancerTypeID
		,PrimaryClientTypeID
		,PrimaryClientSubTypeID
		,IsServiceInitiated
		,ServiceInitiatedDateTime
		,SeenDateTime
		,AssessmentDateTime
		,CareAllocationDateTime
		,InactiveDateTime
		,IsEnhancedAudit
		,StatusDateTime
		,PatientPathwayID
		,RTTStartDateTime
		,RTTStarted
		,RTTStatusID
		,PatientPathwayIdentifierOriginalOrganisationCode
		,IsPatientPathwayIdentifierAvailable
		,SpecifiedGPReferenceNumber
		,IsDirectAccess
		,SuperClusterID
		,HasLongTermCondition
		,BritishArmedForcesID
		,PreviousSymptomID
		,Disability
		,PreviousSymptom
		,IsCommunityActivity
		,ServiceTypeID
		,HealthOrganisationOwnerID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		source.ReferralID
		,source.ReferredBySpecialtyID
		,source.ReferredToSpecialtyID
		,source.ProviderID
		,source.ContractID
		,source.PurchaserID
		,source.PatientID
		,source.PriorProfessionalCareEpisodeID
		,source.SourceOfReferralID
		,source.ReceivedDateTime
		,source.ReasonForReferral
		,source.CancelledDateTime
		,source.CancelReasonID
		,source.Comments
		,source.SentDateTime
		,source.ReferredByReference
		,source.UrgencyID
		,source.PriorityID
		,source.AuthorisationID
		,source.AuthorisationDateTime
		,source.LetterSentFlag
		,source.OriginalHospitalReceivedDateTime
		,source.ReferralEndDateTime
		,source.AuthorisationFlag
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.UserCreate
		,source.UserModify
		,source.ReferralClosureDateTime
		,source.OriginalDescisionToAdmitDateTime
		,source.ReferredByProfessionalCarerID
		,source.ReferredByHealthOrganisationID
		,source.ReferredToProfessionalCarerID
		,source.ReferredToHealthOrganisationID
		,source.ReferredToStaffTeamID
		,source.ReferredByStaffTeamID
		,source.ReferredToReference
		,source.ReferralOutcomeID
		,source.CampusID
		,source.SessionTransactionID
		,source.PriorPointerID
		,source.ExternalKey
		,source.ReferralStatusID
		,source.ReferralRejectStatusID
		,source.AuthorisationProfessionalCarerID
		,source.AuthorisationHealthOrganisationID
		,source.AuthorisationStaffTeamID
		,source.AuthorisationSpecialtyID
		,source.ResponsibleProfessionalCarerID
		,source.ResponsibleHealthOrganisationID
		,source.ResponsibleStaffTeamID
		,source.ResponsibleSpecialtyID
		,source.MentalHealthCareEpisodeID
		,source.ContractServiceID
		,source.PeriodOfCareID
		,source.ReferralTypeID
		,source.ParentID
		,source.LastContactDateTime
		,source.NextContactDateTime
		,source.SuspensionStartDateTime
		,source.SuspensionEndDateTime
		,source.SuspensionReasonID
		,source.RequestUrgencyForAuthorisationID
		,source.ECRRequestStatusID
		,source.ECRRequestStatusDateTime
		,source.LastGPConsultationDateTime
		,source.CodingStatusID
		,source.AdministrativeCategoryID
		,source.ActivityTypeID
		,source.ClinicalPriority
		,source.AccidentRelatedStatusID
		,source.AccidentTypeID
		,source.AccidentNumber
		,source.AccidentApprovalStatusID
		,source.AccidentClaimNumber
		,source.ExpirtDateTime
		,source.ReferredToWardID
		,source.ReferredToClinicID
		,source.Referral
		,source.ClinicalManagementStatusID
		,source.CloseReasonID
		,source.AccessLevelID
		,source.LastNonAppServerModifiedDateTime
		,source.UserID
		,source.ReferralMediumID
		,source.ParentReferralID
		,source.FirstSeenDelayReasonID
		,source.ReferralToTreatmentDelayReasonID
		,source.DescisionToTreatmentDelayReasonID
		,source.SuspectedCancerTypeID
		,source.PrimaryClientTypeID
		,source.PrimaryClientSubTypeID
		,source.IsServiceInitiated
		,source.ServiceInitiatedDateTime
		,source.SeenDateTime
		,source.AssessmentDateTime
		,source.CareAllocationDateTime
		,source.InactiveDateTime
		,source.IsEnhancedAudit
		,source.StatusDateTime
		,source.PatientPathwayID
		,source.RTTStartDateTime
		,source.RTTStarted
		,source.RTTStatusID
		,source.PatientPathwayIdentifierOriginalOrganisationCode
		,source.IsPatientPathwayIdentifierAvailable
		,source.SpecifiedGPReferenceNumber
		,source.IsDirectAccess
		,source.SuperClusterID
		,source.HasLongTermCondition
		,source.BritishArmedForcesID
		,source.PreviousSymptomID
		,source.Disability
		,source.PreviousSymptom
		,source.IsCommunityActivity
		,source.ServiceTypeID
		,source.HealthOrganisationOwnerID
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.ReferredBySpecialtyID
		,target.ReferredToSpecialtyID
		,target.ProviderID
		,target.ContractID
		,target.PurchaserID
		,target.PatientID
		,target.PriorProfessionalCareEpisodeID
		,target.SourceOfReferralID
		,target.ReceivedDateTime
		,target.ReasonForReferral
		,target.CancelledDateTime
		,target.CancelReasonID
		,target.Comments
		,target.SentDateTime
		,target.ReferredByReference
		,target.UrgencyID
		,target.PriorityID
		,target.AuthorisationID
		,target.AuthorisationDateTime
		,target.LetterSentFlag
		,target.OriginalHospitalReceivedDateTime
		,target.ReferralEndDateTime
		,target.AuthorisationFlag
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.ReferralClosureDateTime
		,target.OriginalDescisionToAdmitDateTime
		,target.ReferredByProfessionalCarerID
		,target.ReferredByHealthOrganisationID
		,target.ReferredToProfessionalCarerID
		,target.ReferredToHealthOrganisationID
		,target.ReferredToStaffTeamID
		,target.ReferredByStaffTeamID
		,target.ReferredToReference
		,target.ReferralOutcomeID
		,target.CampusID
		,target.SessionTransactionID
		,target.PriorPointerID
		,target.ExternalKey
		,target.ReferralStatusID
		,target.ReferralRejectStatusID
		,target.AuthorisationProfessionalCarerID
		,target.AuthorisationHealthOrganisationID
		,target.AuthorisationStaffTeamID
		,target.AuthorisationSpecialtyID
		,target.ResponsibleProfessionalCarerID
		,target.ResponsibleHealthOrganisationID
		,target.ResponsibleStaffTeamID
		,target.ResponsibleSpecialtyID
		,target.MentalHealthCareEpisodeID
		,target.ContractServiceID
		,target.PeriodOfCareID
		,target.ReferralTypeID
		,target.ParentID
		,target.LastContactDateTime
		,target.NextContactDateTime
		,target.SuspensionStartDateTime
		,target.SuspensionEndDateTime
		,target.SuspensionReasonID
		,target.RequestUrgencyForAuthorisationID
		,target.ECRRequestStatusID
		,target.ECRRequestStatusDateTime
		,target.LastGPConsultationDateTime
		,target.CodingStatusID
		,target.AdministrativeCategoryID
		,target.ActivityTypeID
		,target.ClinicalPriority
		,target.AccidentRelatedStatusID
		,target.AccidentTypeID
		,target.AccidentNumber
		,target.AccidentApprovalStatusID
		,target.AccidentClaimNumber
		,target.ExpirtDateTime
		,target.ReferredToWardID
		,target.ReferredToClinicID
		,target.Referral
		,target.ClinicalManagementStatusID
		,target.CloseReasonID
		,target.AccessLevelID
		,target.LastNonAppServerModifiedDateTime
		,target.UserID
		,target.ReferralMediumID
		,target.ParentReferralID
		,target.FirstSeenDelayReasonID
		,target.ReferralToTreatmentDelayReasonID
		,target.DescisionToTreatmentDelayReasonID
		,target.SuspectedCancerTypeID
		,target.PrimaryClientTypeID
		,target.PrimaryClientSubTypeID
		,target.IsServiceInitiated
		,target.ServiceInitiatedDateTime
		,target.SeenDateTime
		,target.AssessmentDateTime
		,target.CareAllocationDateTime
		,target.InactiveDateTime
		,target.IsEnhancedAudit
		,target.StatusDateTime
		,target.PatientPathwayID
		,target.RTTStartDateTime
		,target.RTTStarted
		,target.RTTStatusID
		,target.PatientPathwayIdentifierOriginalOrganisationCode
		,target.IsPatientPathwayIdentifierAvailable
		,target.SpecifiedGPReferenceNumber
		,target.IsDirectAccess
		,target.SuperClusterID
		,target.HasLongTermCondition
		,target.BritishArmedForcesID
		,target.PreviousSymptomID
		,target.Disability
		,target.PreviousSymptom
		,target.IsCommunityActivity
		,target.ServiceTypeID
		,target.HealthOrganisationOwnerID
		,target.LorenzoCreated
		)

then
	update
	set
		target.ReferredBySpecialtyID = source.ReferredBySpecialtyID
		,target.ReferredToSpecialtyID = source.ReferredToSpecialtyID
		,target.ProviderID = source.ProviderID
		,target.ContractID = source.ContractID
		,target.PurchaserID = source.PurchaserID
		,target.PatientID = source.PatientID
		,target.PriorProfessionalCareEpisodeID = source.PriorProfessionalCareEpisodeID
		,target.SourceOfReferralID = source.SourceOfReferralID
		,target.ReceivedDateTime = source.ReceivedDateTime
		,target.ReasonForReferral = source.ReasonForReferral
		,target.CancelledDateTime = source.CancelledDateTime
		,target.CancelReasonID = source.CancelReasonID
		,target.Comments = source.Comments
		,target.SentDateTime = source.SentDateTime
		,target.ReferredByReference = source.ReferredByReference
		,target.UrgencyID = source.UrgencyID
		,target.PriorityID = source.PriorityID
		,target.AuthorisationID = source.AuthorisationID
		,target.AuthorisationDateTime = source.AuthorisationDateTime
		,target.LetterSentFlag = source.LetterSentFlag
		,target.OriginalHospitalReceivedDateTime = source.OriginalHospitalReceivedDateTime
		,target.ReferralEndDateTime = source.ReferralEndDateTime
		,target.AuthorisationFlag = source.AuthorisationFlag
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.ReferralClosureDateTime = source.ReferralClosureDateTime
		,target.OriginalDescisionToAdmitDateTime = source.OriginalDescisionToAdmitDateTime
		,target.ReferredByProfessionalCarerID = source.ReferredByProfessionalCarerID
		,target.ReferredByHealthOrganisationID = source.ReferredByHealthOrganisationID
		,target.ReferredToProfessionalCarerID = source.ReferredToProfessionalCarerID
		,target.ReferredToHealthOrganisationID = source.ReferredToHealthOrganisationID
		,target.ReferredToStaffTeamID = source.ReferredToStaffTeamID
		,target.ReferredByStaffTeamID = source.ReferredByStaffTeamID
		,target.ReferredToReference = source.ReferredToReference
		,target.ReferralOutcomeID = source.ReferralOutcomeID
		,target.CampusID = source.CampusID
		,target.SessionTransactionID = source.SessionTransactionID
		,target.PriorPointerID = source.PriorPointerID
		,target.ExternalKey = source.ExternalKey
		,target.ReferralStatusID = source.ReferralStatusID
		,target.ReferralRejectStatusID = source.ReferralRejectStatusID
		,target.AuthorisationProfessionalCarerID = source.AuthorisationProfessionalCarerID
		,target.AuthorisationHealthOrganisationID = source.AuthorisationHealthOrganisationID
		,target.AuthorisationStaffTeamID = source.AuthorisationStaffTeamID
		,target.AuthorisationSpecialtyID = source.AuthorisationSpecialtyID
		,target.ResponsibleProfessionalCarerID = source.ResponsibleProfessionalCarerID
		,target.ResponsibleHealthOrganisationID = source.ResponsibleHealthOrganisationID
		,target.ResponsibleStaffTeamID = source.ResponsibleStaffTeamID
		,target.ResponsibleSpecialtyID = source.ResponsibleSpecialtyID
		,target.MentalHealthCareEpisodeID = source.MentalHealthCareEpisodeID
		,target.ContractServiceID = source.ContractServiceID
		,target.PeriodOfCareID = source.PeriodOfCareID
		,target.ReferralTypeID = source.ReferralTypeID
		,target.ParentID = source.ParentID
		,target.LastContactDateTime = source.LastContactDateTime
		,target.NextContactDateTime = source.NextContactDateTime
		,target.SuspensionStartDateTime = source.SuspensionStartDateTime
		,target.SuspensionEndDateTime = source.SuspensionEndDateTime
		,target.SuspensionReasonID = source.SuspensionReasonID
		,target.RequestUrgencyForAuthorisationID = source.RequestUrgencyForAuthorisationID
		,target.ECRRequestStatusID = source.ECRRequestStatusID
		,target.ECRRequestStatusDateTime = source.ECRRequestStatusDateTime
		,target.LastGPConsultationDateTime = source.LastGPConsultationDateTime
		,target.CodingStatusID = source.CodingStatusID
		,target.AdministrativeCategoryID = source.AdministrativeCategoryID
		,target.ActivityTypeID = source.ActivityTypeID
		,target.ClinicalPriority = source.ClinicalPriority
		,target.AccidentRelatedStatusID = source.AccidentRelatedStatusID
		,target.AccidentTypeID = source.AccidentTypeID
		,target.AccidentNumber = source.AccidentNumber
		,target.AccidentApprovalStatusID = source.AccidentApprovalStatusID
		,target.AccidentClaimNumber = source.AccidentClaimNumber
		,target.ExpirtDateTime = source.ExpirtDateTime
		,target.ReferredToWardID = source.ReferredToWardID
		,target.ReferredToClinicID = source.ReferredToClinicID
		,target.Referral = source.Referral
		,target.ClinicalManagementStatusID = source.ClinicalManagementStatusID
		,target.CloseReasonID = source.CloseReasonID
		,target.AccessLevelID = source.AccessLevelID
		,target.LastNonAppServerModifiedDateTime = source.LastNonAppServerModifiedDateTime
		,target.UserID = source.UserID
		,target.ReferralMediumID = source.ReferralMediumID
		,target.ParentReferralID = source.ParentReferralID
		,target.FirstSeenDelayReasonID = source.FirstSeenDelayReasonID
		,target.ReferralToTreatmentDelayReasonID = source.ReferralToTreatmentDelayReasonID
		,target.DescisionToTreatmentDelayReasonID = source.DescisionToTreatmentDelayReasonID
		,target.SuspectedCancerTypeID = source.SuspectedCancerTypeID
		,target.PrimaryClientTypeID = source.PrimaryClientTypeID
		,target.PrimaryClientSubTypeID = source.PrimaryClientSubTypeID
		,target.IsServiceInitiated = source.IsServiceInitiated
		,target.ServiceInitiatedDateTime = source.ServiceInitiatedDateTime
		,target.SeenDateTime = source.SeenDateTime
		,target.AssessmentDateTime = source.AssessmentDateTime
		,target.CareAllocationDateTime = source.CareAllocationDateTime
		,target.InactiveDateTime = source.InactiveDateTime
		,target.IsEnhancedAudit = source.IsEnhancedAudit
		,target.StatusDateTime = source.StatusDateTime
		,target.PatientPathwayID = source.PatientPathwayID
		,target.RTTStartDateTime = source.RTTStartDateTime
		,target.RTTStarted = source.RTTStarted
		,target.RTTStatusID = source.RTTStatusID
		,target.PatientPathwayIdentifierOriginalOrganisationCode = source.PatientPathwayIdentifierOriginalOrganisationCode
		,target.IsPatientPathwayIdentifierAvailable = source.IsPatientPathwayIdentifierAvailable
		,target.SpecifiedGPReferenceNumber = source.SpecifiedGPReferenceNumber
		,target.IsDirectAccess = source.IsDirectAccess
		,target.SuperClusterID = source.SuperClusterID
		,target.HasLongTermCondition = source.HasLongTermCondition
		,target.BritishArmedForcesID = source.BritishArmedForcesID
		,target.PreviousSymptomID = source.PreviousSymptomID
		,target.Disability = source.Disability
		,target.PreviousSymptom = source.PreviousSymptom
		,target.IsCommunityActivity = source.IsCommunityActivity
		,target.ServiceTypeID = source.ServiceTypeID
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime