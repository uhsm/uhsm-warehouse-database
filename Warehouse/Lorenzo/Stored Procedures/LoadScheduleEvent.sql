﻿CREATE PROCEDURE [Lorenzo].[LoadScheduleEvent]

AS


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(ScheduleEvent.LorenzoCreated)
		from
			Lorenzo.ScheduleEvent
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 ScheduleEventID
			,SpecialtyID
			,SessionTransactionID
			,ProviderID
			,ContractID
			,PurchaserID
			,ProfessionalCarerID
			,ReferralID
			,ProfessionalCarerEpisodeID
			,PatientID
			,AccessLevelID
			,RequestDateTime
			,StartDateTime
			,EndDateTime
			,VisitTypeID
			,ScheduleTypeID
			,ReasonID
			,DataLinkageConsentID
			,ReasonComments
			,UrgencyID
			,PriorityID
			,TransportID
			,ScheduleCancellationReasonID
			,CancelledDateTime
			,MovementReasonID
			,MovemenDateTime
			,ArrivedDateTime
			,SeenDateTime
			,DepartedDateTime
			,AttendanceID
			,DidNotAttendReasonID
			,OutcomeID
			,PreviousScheduleOutcomeID
			,Comments
			,PriorID
			,CalledDateTime
			,ServicePointID
			,Duration
			,AdministrativeCategoryID
			,CancellationByID
			,ServicePointSessionID
			,MoveCount
			,StaffTeamID
			,LinkID
			,LocationTypeID
			,Location
			,ActualDuration
			,OutwardJourneyDuration
			,ReturnJourneyDuration
			,GroupPlannedNumber
			,GroupActualNumber
			,ContactPurposeID
			,ContactTypeID
			,ResourceUnits
			,ResourceUnitTypeID
			,StaffActivityTypeID
			,OutwardJourneyStartDateTime
			,OutwardJourneyEndDateTime
			,ReturnJourneyStartDateTime
			,ReturnJourneyEndDateTime
			,IsApproximate
			,ScheduleStatusID
			,PreviousScheduleStatusID
			,PACACID
			,PatientAssessmentID
			,ServicePointStayID
			,ServicePointStayServicePointID
			,ContractedServicesID
			,AllocationOverrideFlag
			,WaitingListID
			,ParentScheduleEventID
			,AdmissionOfferID
			,MoveRequestedByID
			,CodingStatusID
			,ClinicalCostCentresID
			,PatientConfirmationStatusID
			,ScheduleIdentifier
			,OperationDuration
			,AnaestheticDuration
			,AnaesthetistID
			,IntoTheatreDateTime
			,IntoPostOpDateTime
			,IsUnplannedReturn
			,AEAttendanceID
			,PregnancyStatusID
			,LMPDate
			,BedStayID
			,InvoiceTag
			,DNANextScheduleEventID
			,ScheduleRecurrenceID
			,LocationTypeHealthOrganisationID
			,SessionLateStartReasonID
			,SeenByProfessionalCarerID
			,AnaestheticTypeID
			,LinkedServicePointID
			,LinkedBedID
			,NewAppointmentsDescription
			,CreatedDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,PriorPointerID
			,ExternalKey
			,CompensableDetailsID
			,PatientClassificationTypeID
			,DaycareEpisodeID
			,DietaryRequirementsID
			,TransportReference
			,ParentFacilityScheduleID
			,PlannedAttendees
			,ActualAttendees
			,FollowUpCount
			,NonAppServerLastModifyDateTime
			,UserID
			,AnaestheticDateTime
			,BookingTypeID
			,IsOverbooked
			,IsEBSBooking
			,UniqueBookingReferenceNo
			,BedCategoryID
			,ReferralReceivedFlag
			,SendSMS
			,LastModifiedDetails
			,TreatInitFlag
			,ProviderSpellID
			,ContactScheduleID
			,PatientChosenBy
			,ConsultationMediaID
			,OfferDateTime
			,OtherDateTime
			,IsShortNotice
			,IsUnplanned
			,IsHoldingArea
			,FollowUp
			,RTTStatusID
			,EarliestReasonableOfferDateTime
			,PatientConfirmationDateTime
			,IsVerbal
			,IsFirstDefinitivetreatment
			,AppointmentPurposeID
			,PsychotropicMedicationID
			,CareProfessionalRoleID
			,IsCommunityActivity
			,HealthOrganisationOwnerID
			,LorenzoCreated
			)
into
	#TLoadPASScheduleEvent
from
	(
	select
		ScheduleEventID
		,SpecialtyID = cast(SpecialtyID as int)
		,SessionTransactionID = cast(SessionTransactionID as int)
		,ProviderID = cast(ProviderID as int)
		,ContractID = cast(ContractID as int)
		,PurchaserID = cast(PurchaserID as int)
		,ProfessionalCarerID = cast(ProfessionalCarerID as int)
		,ReferralID = cast(ReferralID as int)
		,ProfessionalCarerEpisodeID = cast(ProfessionalCarerEpisodeID as int)
		,PatientID = cast(PatientID as int)
		,AccessLevelID = cast(AccessLevelID as int)
		,RequestDateTime = cast(RequestDateTime as datetime)
		,StartDateTime = cast(StartDateTime as datetime)
		,EndDateTime = cast(EndDateTime as datetime)
		,VisitTypeID = cast(VisitTypeID as int)
		,ScheduleTypeID = cast(ScheduleTypeID as int)
		,ReasonID = cast(ReasonID as int)
		,DataLinkageConsentID = cast(DataLinkageConsentID as int)
		,ReasonComments = cast(nullif(ReasonComments, '') as varchar(255))
		,UrgencyID = cast(UrgencyID as int)
		,PriorityID = cast(PriorityID as int)
		,TransportID = cast(TransportID as int)
		,ScheduleCancellationReasonID = cast(ScheduleCancellationReasonID as int)
		,CancelledDateTime = cast(CancelledDateTime as datetime)
		,MovementReasonID = cast(MovementReasonID as int)
		,MovemenDateTime = cast(MovemenDateTime as datetime)
		,ArrivedDateTime = cast(ArrivedDateTime as datetime)
		,SeenDateTime = cast(SeenDateTime as datetime)
		,DepartedDateTime = cast(DepartedDateTime as datetime)
		,AttendanceID = cast(AttendanceID as int)
		,DidNotAttendReasonID = cast(DidNotAttendReasonID as int)
		,OutcomeID = cast(OutcomeID as int)
		,PreviousScheduleOutcomeID = cast(PreviousScheduleOutcomeID as int)
		,Comments = cast(nullif(Comments, '') as varchar(255))
		,PriorID = cast(PriorID as int)
		,CalledDateTime = cast(CalledDateTime as datetime)
		,ServicePointID = cast(ServicePointID as int)
		,Duration = cast(Duration as int)
		,AdministrativeCategoryID = cast(AdministrativeCategoryID as int)
		,CancellationByID = cast(CancellationByID as int)
		,ServicePointSessionID = cast(ServicePointSessionID as int)
		,MoveCount = cast(MoveCount as int)
		,StaffTeamID = cast(StaffTeamID as int)
		,LinkID = cast(nullif(LinkID, '') as varchar(50))
		,LocationTypeID = cast(LocationTypeID as int)
		,Location = cast(nullif(Location, '') as varchar(255))
		,ActualDuration = cast(ActualDuration as int)
		,OutwardJourneyDuration = cast(OutwardJourneyDuration as int)
		,ReturnJourneyDuration = cast(ReturnJourneyDuration as int)
		,GroupPlannedNumber = cast(GroupPlannedNumber as int)
		,GroupActualNumber = cast(GroupActualNumber as int)
		,ContactPurposeID = cast(ContactPurposeID as int)
		,ContactTypeID = cast(ContactTypeID as int)
		,ResourceUnits = cast(ResourceUnits as int)
		,ResourceUnitTypeID = cast(ResourceUnitTypeID as int)
		,StaffActivityTypeID = cast(StaffActivityTypeID as int)
		,OutwardJourneyStartDateTime = cast(OutwardJourneyStartDateTime as datetime)
		,OutwardJourneyEndDateTime = cast(OutwardJourneyEndDateTime as datetime)
		,ReturnJourneyStartDateTime = cast(ReturnJourneyStartDateTime as datetime)
		,ReturnJourneyEndDateTime = cast(ReturnJourneyEndDateTime as datetime)
		,IsApproximate = cast(nullif(IsApproximate, '') as char(1))
		,ScheduleStatusID = cast(ScheduleStatusID as int)
		,PreviousScheduleStatusID = cast(PreviousScheduleStatusID as int)
		,PACACID = cast(PACACID as int)
		,PatientAssessmentID = cast(PatientAssessmentID as int)
		,ServicePointStayID = cast(ServicePointStayID as int)
		,ServicePointStayServicePointID = cast(ServicePointStayServicePointID as int)
		,ContractedServicesID = cast(ContractedServicesID as int)
		,AllocationOverrideFlag = cast(nullif(AllocationOverrideFlag, '') as char(1))
		,WaitingListID = cast(WaitingListID as int)
		,ParentScheduleEventID = cast(ParentScheduleEventID as int)
		,AdmissionOfferID = cast(AdmissionOfferID as int)
		,MoveRequestedByID = cast(MoveRequestedByID as int)
		,CodingStatusID = cast(CodingStatusID as int)
		,ClinicalCostCentresID = cast(ClinicalCostCentresID as int)
		,PatientConfirmationStatusID = cast(PatientConfirmationStatusID as int)
		,ScheduleIdentifier = cast(nullif(ScheduleIdentifier, '') as varchar(50))
		,OperationDuration = cast(OperationDuration as int)
		,AnaestheticDuration = cast(AnaestheticDuration as int)
		,AnaesthetistID = cast(AnaesthetistID as int)
		,IntoTheatreDateTime = cast(IntoTheatreDateTime as datetime)
		,IntoPostOpDateTime = cast(IntoPostOpDateTime as datetime)
		,IsUnplannedReturn = cast(nullif(IsUnplannedReturn, '') as char(1))
		,AEAttendanceID = cast(AEAttendanceID as int)
		,PregnancyStatusID = cast(PregnancyStatusID as int)
		,LMPDate = cast(LMPDate as datetime)
		,BedStayID = cast(BedStayID as int)
		,InvoiceTag = cast(nullif(InvoiceTag, '') as varchar(50))
		,DNANextScheduleEventID = cast(DNANextScheduleEventID as int)
		,ScheduleRecurrenceID = cast(ScheduleRecurrenceID as int)
		,LocationTypeHealthOrganisationID = cast(LocationTypeHealthOrganisationID as int)
		,SessionLateStartReasonID = cast(SessionLateStartReasonID as int)
		,SeenByProfessionalCarerID = cast(SeenByProfessionalCarerID as int)
		,AnaestheticTypeID = cast(AnaestheticTypeID as int)
		,LinkedServicePointID = cast(LinkedServicePointID as int)
		,LinkedBedID = cast(LinkedBedID as int)
		,NewAppointmentsDescription = cast(nullif(NewAppointmentsDescription, '') as varchar(500))
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(50))
		,UserModify = cast(nullif(UserModify, '') as varchar(50))
		,PriorPointerID = cast(PriorPointerID as int)
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(50))
		,CompensableDetailsID = cast(CompensableDetailsID as int)
		,PatientClassificationTypeID = cast(PatientClassificationTypeID as int)
		,DaycareEpisodeID = cast(DaycareEpisodeID as int)
		,DietaryRequirementsID = cast(DietaryRequirementsID as int)
		,TransportReference = cast(nullif(TransportReference, '') as varchar(50))
		,ParentFacilityScheduleID = cast(ParentFacilityScheduleID as int)
		,PlannedAttendees = cast(PlannedAttendees as int)
		,ActualAttendees = cast(ActualAttendees as int)
		,FollowUpCount = cast(FollowUpCount as int)
		,NonAppServerLastModifyDateTime = cast(NonAppServerLastModifyDateTime as datetime)
		,UserID = cast(UserID as int)
		,AnaestheticDateTime = cast(AnaestheticDateTime as datetime)
		,BookingTypeID = cast(BookingTypeID as int)
		,IsOverbooked = cast(nullif(IsOverbooked, '') as char(1))
		,IsEBSBooking = cast(nullif(IsEBSBooking, '') as char(1))
		,UniqueBookingReferenceNo = cast(nullif(UniqueBookingReferenceNo, '') as varchar(255))
		,BedCategoryID = cast(BedCategoryID as int)
		,ReferralReceivedFlag = cast(nullif(ReferralReceivedFlag, '') as char(1))
		,SendSMS = cast(nullif(SendSMS, '') as char(1))
		,LastModifiedDetails = cast(nullif(LastModifiedDetails, '') as varchar(100))
		,TreatInitFlag = cast(nullif(TreatInitFlag, '') as char(1))
		,ProviderSpellID = cast(ProviderSpellID as int)
		,ContactScheduleID = cast(ContactScheduleID as int)
		,PatientChosenBy = cast(PatientChosenBy as int)
		,ConsultationMediaID = cast(ConsultationMediaID as int)
		,OfferDateTime = cast(OfferDateTime as datetime)
		,OtherDateTime = cast(OtherDateTime as datetime)
		,IsShortNotice = cast(nullif(IsShortNotice, '') as char(1))
		,IsUnplanned = cast(nullif(IsUnplanned, '') as char(1))
		,IsHoldingArea = cast(nullif(IsHoldingArea, '') as char(1))
		,FollowUp = cast(nullif(FollowUp, '') as char(1))
		,RTTStatusID = cast(RTTStatusID as int)
		,EarliestReasonableOfferDateTime = cast(EarliestReasonableOfferDateTime as datetime)
		,PatientConfirmationDateTime = cast(PatientConfirmationDateTime as datetime)
		,IsVerbal = cast(nullif(IsVerbal, '') as char(1))
		,IsFirstDefinitivetreatment = cast(nullif(IsFirstDefinitivetreatment, '') as char(1))
		,AppointmentPurposeID = cast(AppointmentPurposeID as int)
		,PsychotropicMedicationID = cast(PsychotropicMedicationID as int)
		,CareProfessionalRoleID = cast(CareProfessionalRoleID as int)
		,IsCommunityActivity = cast(nullif(IsCommunityActivity, '') as char(1))
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as int)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			ScheduleEventID =  [SCHDL_REFNO]
			  ,SpecialtyID = [SPECT_REFNO]
			  ,SessionTransactionID = [STRAN_REFNO]
			  ,ProviderID = [PROVD_REFNO]
			  ,ContractID = [CONTR_REFNO]
			  ,PurchaserID = [PURCH_REFNO]
			  ,ProfessionalCarerID = [PROCA_REFNO]
			  ,ReferralID = [REFRL_REFNO]
			  ,ProfessionalCarerEpisodeID = [PRCAE_REFNO]
			  ,PatientID = [PATNT_REFNO]
			  ,AccessLevelID  = [ACLEV_REFNO] 
			  ,RequestDateTime = [REQST_DTTM]
			  ,StartDateTime = [START_DTTM]
			  ,EndDateTime = [END_DTTM]
			  ,VisitTypeID = [VISIT_REFNO]
			  ,ScheduleTypeID = [SCTYP_REFNO]
			  ,ReasonID = [REASN_REFNO]
			  ,DataLinkageConsentID = [DLNKC_REFNO]
			  ,ReasonComments = [REASN_COMMENTS]
			  ,UrgencyID = [URGNC_REFNO]
			  ,PriorityID = [PRITY_REFNO]
			  ,TransportID = [TRANS_REFNO]
			  ,ScheduleCancellationReasonID = [CANCR_REFNO]
			  ,CancelledDateTime = [CANCR_DTTM]
			  ,MovementReasonID = [MOVRN_REFNO]
			  ,MovemenDateTime = [MOVE_DTTM]
			  ,ArrivedDateTime = [ARRIVED_DTTM]
			  ,SeenDateTime = [SEEN_DTTM]
			  ,DepartedDateTime = [DEPARTED_DTTM]
			  ,AttendanceID = [ATTND_REFNO]
			  ,DidNotAttendReasonID = [DNARS_REFNO]
			  ,OutcomeID = [SCOCM_REFNO]
			  ,PreviousScheduleOutcomeID = [OLD_SCOCM_REFNO]
			  ,Comments = [COMMENTS]
			  ,PriorID = [PRIOR_REFNO] 
			  ,CalledDateTime = [CALLED_DTTM]
			  ,ServicePointID = [SPONT_REFNO]
			  ,Duration = [DURATION]
			  ,AdministrativeCategoryID = [ADCAT_REFNO]
			  ,CancellationByID = [CANCB_REFNO]
			  ,ServicePointSessionID = [SPSSN_REFNO]
			  ,MoveCount = [MOVE_COUNT]  
			  ,StaffTeamID = [STEAM_REFNO]
			  ,LinkID = [LINK_REFNO]
			  ,LocationTypeID = [LOTYP_REFNO]
			  ,Location = [LOCATION]
			  ,ActualDuration = [ACTUAL_DURATION]
			  ,OutwardJourneyDuration = [OUTWD_DURATION]
			  ,ReturnJourneyDuration = [RETRN_DURATION]
			  ,GroupPlannedNumber = [PLANNED_NUMBER] 
			  ,GroupActualNumber = [ACTUAL_NUMBER]
			  ,ContactPurposeID = [CONTP_REFNO]
			  ,ContactTypeID = [CONTY_REFNO]
			  ,ResourceUnits = [RESOURCE_UNITS]
			  ,ResourceUnitTypeID = [RESUT_REFNO]
			  ,StaffActivityTypeID = [SATYP_REFNO]
			  ,OutwardJourneyStartDateTime = [OUTWD_JRNEY_START_DTTM]
			  ,OutwardJourneyEndDateTime = [OUTWD_JRNEY_END_DTTM] 
			  ,ReturnJourneyStartDateTime = [RETRN_JRNEY_START_DTTM]
			  ,ReturnJourneyEndDateTime = [RETRN_JRNEY_END_DTTM]
			  ,IsApproximate = [APPROX_FLAG]
			  ,ScheduleStatusID = [SCSDA_REFNO]
			  ,PreviousScheduleStatusID = [PREV_SCSDA_REFNO]
			  ,PACACID = [PACAC_REFNO]
			  ,PatientAssessmentID = [PAASS_REFNO]
			  ,ServicePointStayID = [SSTAY_REFNO]
			  ,ServicePointStayServicePointID = [SSTAY_SPONT_REFNO]
			  ,ContractedServicesID = [CTRSV_REFNO]
			  ,AllocationOverrideFlag = [ALLCN_OVERRIDE_FLAG]
			  ,WaitingListID = [WLIST_REFNO]
			  ,ParentScheduleEventID = [PARNT_REFNO]
			  ,AdmissionOfferID = [ADMOF_REFNO]
			  ,MoveRequestedByID = [MURQB_REFNO]
			  ,CodingStatusID = [CSTAT_REFNO]
			  ,ClinicalCostCentresID = [CCCCC_REFNO]
			  ,PatientConfirmationStatusID = [PCONF_REFNO]
			  ,ScheduleIdentifier = [IDENTIFIER] 
			  ,OperationDuration = [OPER_DURATION]
			  ,AnaestheticDuration = [ANAES_DURATION]
			  ,AnaesthetistID = [ANAES_PROCA_REFNO] 
			  ,IntoTheatreDateTime = [INTO_THEATRE_DTTM]
			  ,IntoPostOpDateTime = [INTO_POSTOP_DTTM]
			  ,IsUnplannedReturn = [UNPLANNED_RET]
			  ,AEAttendanceID = [AEATT_REFNO]
			  ,PregnancyStatusID = [PRGST_REFNO]
			  ,LMPDate = [LMP_DATE]
			  ,BedStayID = [BEDST_REFNO]
			  ,InvoiceTag = [INVOICE_TAG]
			  ,DNANextScheduleEventID = [DNA_SCHDL_REFNO]
			  ,ScheduleRecurrenceID = [SCRCR_REFNO]
			  ,LocationTypeHealthOrganisationID = [LOTYP_HEORG_REFNO]
			  ,SessionLateStartReasonID = [SLATE_REFNO]
			  ,SeenByProfessionalCarerID = [SEENBY_PROCA_REFNO]
			  ,AnaestheticTypeID = [ANTYP_REFNO]
			  ,LinkedServicePointID = [TO_SPONT_REFNO]
			  ,LinkedBedID = [TO_BED_REFNO]
			  ,NewAppointmentsDescription = [NEW_APPTS_DESC]
			  ,CreatedDateTime = [CREATE_DTTM]
			  ,ModifiedDateTime = [MODIF_DTTM]
			  ,UserCreate = [USER_CREATE]
			  ,UserModify = [USER_MODIF]
			  ,PriorPointerID = [PRIOR_POINTER]
			  ,ExternalKey = [EXTERNAL_KEY]
			  ,CompensableDetailsID = [COMPS_REFNO]
			  ,PatientClassificationTypeID = [PTCLS_REFNO]
			  ,DaycareEpisodeID = [DYCEP_REFNO]
			  ,DietaryRequirementsID = [DIETY_REFNO]
			  ,TransportReference = [TRANS_REF]
			  ,ParentFacilityScheduleID = [FAC_SCHDL_REFNO]
			  ,PlannedAttendees = [PLANNED_ATTENDEES]
			  ,ActualAttendees = [ACTUAL_ATTENDEES]
			  ,FollowUpCount = [FOLLOWUP_COUNT]
			  ,NonAppServerLastModifyDateTime = [NON_APPSVR_MODIF_DTTM]
			  ,UserID = [USER_REFNO]
			  ,AnaestheticDateTime = [ANAES_DTTM]
			  ,BookingTypeID = [BKTYP_REFNO]
			  ,IsOverbooked = [OVERBOOK_FLAG]
			  ,IsEBSBooking = [EBS_BOOK_FLAG]
			  ,UniqueBookingReferenceNo = [UBRN] 
			  ,BedCategoryID = [TO_BDCAT_REFNO] 
			  ,ReferralReceivedFlag = [REFRL_RECIV_FLAG]
			  ,SendSMS = [send_sms]
			  ,LastModifiedDetails = [LSTMODIF_DETAILS]
			  ,TreatInitFlag = [TREAT_INIT_FLAG]
			  ,ProviderSpellID = [PRVSP_REFNO]
			  ,ContactScheduleID = [FAC_CNTCT_REFNO]  
			  ,PatientChosenBy = [PATNT_CHOSEN_BY]
			  ,ConsultationMediaID = [CMDIA_REFNO]
			  ,OfferDateTime = [OFFER_DTTM]
			  ,OtherDateTime = [OTHER_DTTM]
			  ,IsShortNotice = [SHORT_NOTICE_FLAG]
			  ,IsUnplanned = [UNPLANNED_FLAG]
			  ,IsHoldingArea = [HOLD_AREA_FLAG] 
			  ,FollowUp = [FOLLOW_UP]
			  ,RTTStatusID = [RTTST_REFNO]
			  ,EarliestReasonableOfferDateTime   = [ERO_DTTM]
			  ,PatientConfirmationDateTime = [PCONF_DTTM]
			  ,IsVerbal = [VERBAL_FLAG] 
			  ,IsFirstDefinitivetreatment = [FIRST_DEF_TRMNT]
			  ,AppointmentPurposeID = [APTYP_REFNO]
			  ,PsychotropicMedicationID = [USMED_REFNO]
			  ,CareProfessionalRoleID = [CAROL_REFNO]
			  ,IsCommunityActivity = [COMMUNITY_FLAG]
			  ,HealthOrganisationOwnerID = [OWNER_HEORG_REFNO]
			  ,LorenzoCreated = [Created]


			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.ScheduleEvent
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASScheduleEvent on #TLoadPASScheduleEvent
	(
	ScheduleEventID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.ScheduleEvent target
using
	(
	select
		*
	from
		#TLoadPASScheduleEvent
	
	) source
	on	source.ScheduleEventID = target.ScheduleEventID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		  ScheduleEventID
		,SpecialtyID
		,SessionTransactionID
		,ProviderID
		,ContractID
		,PurchaserID
		,ProfessionalCarerID
		,ReferralID
		,ProfessionalCarerEpisodeID
		,PatientID
		,AccessLevelID
		,RequestDateTime
		,StartDateTime
		,EndDateTime
		,VisitTypeID
		,ScheduleTypeID
		,ReasonID
		,DataLinkageConsentID
		,ReasonComments
		,UrgencyID
		,PriorityID
		,TransportID
		,ScheduleCancellationReasonID
		,CancelledDateTime
		,MovementReasonID
		,MovemenDateTime
		,ArrivedDateTime
		,SeenDateTime
		,DepartedDateTime
		,AttendanceID
		,DidNotAttendReasonID
		,OutcomeID
		,PreviousScheduleOutcomeID
		,Comments
		,PriorID
		,CalledDateTime
		,ServicePointID
		,Duration
		,AdministrativeCategoryID
		,CancellationByID
		,ServicePointSessionID
		,MoveCount
		,StaffTeamID
		,LinkID
		,LocationTypeID
		,Location
		,ActualDuration
		,OutwardJourneyDuration
		,ReturnJourneyDuration
		,GroupPlannedNumber
		,GroupActualNumber
		,ContactPurposeID
		,ContactTypeID
		,ResourceUnits
		,ResourceUnitTypeID
		,StaffActivityTypeID
		,OutwardJourneyStartDateTime
		,OutwardJourneyEndDateTime
		,ReturnJourneyStartDateTime
		,ReturnJourneyEndDateTime
		,IsApproximate
		,ScheduleStatusID
		,PreviousScheduleStatusID
		,PACACID
		,PatientAssessmentID
		,ServicePointStayID
		,ServicePointStayServicePointID
		,ContractedServicesID
		,AllocationOverrideFlag
		,WaitingListID
		,ParentScheduleEventID
		,AdmissionOfferID
		,MoveRequestedByID
		,CodingStatusID
		,ClinicalCostCentresID
		,PatientConfirmationStatusID
		,ScheduleIdentifier
		,OperationDuration
		,AnaestheticDuration
		,AnaesthetistID
		,IntoTheatreDateTime
		,IntoPostOpDateTime
		,IsUnplannedReturn
		,AEAttendanceID
		,PregnancyStatusID
		,LMPDate
		,BedStayID
		,InvoiceTag
		,DNANextScheduleEventID
		,ScheduleRecurrenceID
		,LocationTypeHealthOrganisationID
		,SessionLateStartReasonID
		,SeenByProfessionalCarerID
		,AnaestheticTypeID
		,LinkedServicePointID
		,LinkedBedID
		,NewAppointmentsDescription
		,CreatedDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,PriorPointerID
		,ExternalKey
		,CompensableDetailsID
		,PatientClassificationTypeID
		,DaycareEpisodeID
		,DietaryRequirementsID
		,TransportReference
		,ParentFacilityScheduleID
		,PlannedAttendees
		,ActualAttendees
		,FollowUpCount
		,NonAppServerLastModifyDateTime
		,UserID
		,AnaestheticDateTime
		,BookingTypeID
		,IsOverbooked
		,IsEBSBooking
		,UniqueBookingReferenceNo
		,BedCategoryID
		,ReferralReceivedFlag
		,SendSMS
		,LastModifiedDetails
		,TreatInitFlag
		,ProviderSpellID
		,ContactScheduleID
		,PatientChosenBy
		,ConsultationMediaID
		,OfferDateTime
		,OtherDateTime
		,IsShortNotice
		,IsUnplanned
		,IsHoldingArea
		,FollowUp
		,RTTStatusID
		,EarliestReasonableOfferDateTime
		,PatientConfirmationDateTime
		,IsVerbal
		,IsFirstDefinitivetreatment
		,AppointmentPurposeID
		,PsychotropicMedicationID
		,CareProfessionalRoleID
		,IsCommunityActivity
		,HealthOrganisationOwnerID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		  source.ScheduleEventID
		,source.SpecialtyID
		,source.SessionTransactionID
		,source.ProviderID
		,source.ContractID
		,source.PurchaserID
		,source.ProfessionalCarerID
		,source.ReferralID
		,source.ProfessionalCarerEpisodeID
		,source.PatientID
		,source.AccessLevelID
		,source.RequestDateTime
		,source.StartDateTime
		,source.EndDateTime
		,source.VisitTypeID
		,source.ScheduleTypeID
		,source.ReasonID
		,source.DataLinkageConsentID
		,source.ReasonComments
		,source.UrgencyID
		,source.PriorityID
		,source.TransportID
		,source.ScheduleCancellationReasonID
		,source.CancelledDateTime
		,source.MovementReasonID
		,source.MovemenDateTime
		,source.ArrivedDateTime
		,source.SeenDateTime
		,source.DepartedDateTime
		,source.AttendanceID
		,source.DidNotAttendReasonID
		,source.OutcomeID
		,source.PreviousScheduleOutcomeID
		,source.Comments
		,source.PriorID
		,source.CalledDateTime
		,source.ServicePointID
		,source.Duration
		,source.AdministrativeCategoryID
		,source.CancellationByID
		,source.ServicePointSessionID
		,source.MoveCount
		,source.StaffTeamID
		,source.LinkID
		,source.LocationTypeID
		,source.Location
		,source.ActualDuration
		,source.OutwardJourneyDuration
		,source.ReturnJourneyDuration
		,source.GroupPlannedNumber
		,source.GroupActualNumber
		,source.ContactPurposeID
		,source.ContactTypeID
		,source.ResourceUnits
		,source.ResourceUnitTypeID
		,source.StaffActivityTypeID
		,source.OutwardJourneyStartDateTime
		,source.OutwardJourneyEndDateTime
		,source.ReturnJourneyStartDateTime
		,source.ReturnJourneyEndDateTime
		,source.IsApproximate
		,source.ScheduleStatusID
		,source.PreviousScheduleStatusID
		,source.PACACID
		,source.PatientAssessmentID
		,source.ServicePointStayID
		,source.ServicePointStayServicePointID
		,source.ContractedServicesID
		,source.AllocationOverrideFlag
		,source.WaitingListID
		,source.ParentScheduleEventID
		,source.AdmissionOfferID
		,source.MoveRequestedByID
		,source.CodingStatusID
		,source.ClinicalCostCentresID
		,source.PatientConfirmationStatusID
		,source.ScheduleIdentifier
		,source.OperationDuration
		,source.AnaestheticDuration
		,source.AnaesthetistID
		,source.IntoTheatreDateTime
		,source.IntoPostOpDateTime
		,source.IsUnplannedReturn
		,source.AEAttendanceID
		,source.PregnancyStatusID
		,source.LMPDate
		,source.BedStayID
		,source.InvoiceTag
		,source.DNANextScheduleEventID
		,source.ScheduleRecurrenceID
		,source.LocationTypeHealthOrganisationID
		,source.SessionLateStartReasonID
		,source.SeenByProfessionalCarerID
		,source.AnaestheticTypeID
		,source.LinkedServicePointID
		,source.LinkedBedID
		,source.NewAppointmentsDescription
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.UserCreate
		,source.UserModify
		,source.PriorPointerID
		,source.ExternalKey
		,source.CompensableDetailsID
		,source.PatientClassificationTypeID
		,source.DaycareEpisodeID
		,source.DietaryRequirementsID
		,source.TransportReference
		,source.ParentFacilityScheduleID
		,source.PlannedAttendees
		,source.ActualAttendees
		,source.FollowUpCount
		,source.NonAppServerLastModifyDateTime
		,source.UserID
		,source.AnaestheticDateTime
		,source.BookingTypeID
		,source.IsOverbooked
		,source.IsEBSBooking
		,source.UniqueBookingReferenceNo
		,source.BedCategoryID
		,source.ReferralReceivedFlag
		,source.SendSMS
		,source.LastModifiedDetails
		,source.TreatInitFlag
		,source.ProviderSpellID
		,source.ContactScheduleID
		,source.PatientChosenBy
		,source.ConsultationMediaID
		,source.OfferDateTime
		,source.OtherDateTime
		,source.IsShortNotice
		,source.IsUnplanned
		,source.IsHoldingArea
		,source.FollowUp
		,source.RTTStatusID
		,source.EarliestReasonableOfferDateTime
		,source.PatientConfirmationDateTime
		,source.IsVerbal
		,source.IsFirstDefinitivetreatment
		,source.AppointmentPurposeID
		,source.PsychotropicMedicationID
		,source.CareProfessionalRoleID
		,source.IsCommunityActivity
		,source.HealthOrganisationOwnerID
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		 target.ScheduleEventID
		,target.SpecialtyID
		,target.SessionTransactionID
		,target.ProviderID
		,target.ContractID
		,target.PurchaserID
		,target.ProfessionalCarerID
		,target.ReferralID
		,target.ProfessionalCarerEpisodeID
		,target.PatientID
		,target.AccessLevelID
		,target.RequestDateTime
		,target.StartDateTime
		,target.EndDateTime
		,target.VisitTypeID
		,target.ScheduleTypeID
		,target.ReasonID
		,target.DataLinkageConsentID
		,target.ReasonComments
		,target.UrgencyID
		,target.PriorityID
		,target.TransportID
		,target.ScheduleCancellationReasonID
		,target.CancelledDateTime
		,target.MovementReasonID
		,target.MovemenDateTime
		,target.ArrivedDateTime
		,target.SeenDateTime
		,target.DepartedDateTime
		,target.AttendanceID
		,target.DidNotAttendReasonID
		,target.OutcomeID
		,target.PreviousScheduleOutcomeID
		,target.Comments
		,target.PriorID
		,target.CalledDateTime
		,target.ServicePointID
		,target.Duration
		,target.AdministrativeCategoryID
		,target.CancellationByID
		,target.ServicePointSessionID
		,target.MoveCount
		,target.StaffTeamID
		,target.LinkID
		,target.LocationTypeID
		,target.Location
		,target.ActualDuration
		,target.OutwardJourneyDuration
		,target.ReturnJourneyDuration
		,target.GroupPlannedNumber
		,target.GroupActualNumber
		,target.ContactPurposeID
		,target.ContactTypeID
		,target.ResourceUnits
		,target.ResourceUnitTypeID
		,target.StaffActivityTypeID
		,target.OutwardJourneyStartDateTime
		,target.OutwardJourneyEndDateTime
		,target.ReturnJourneyStartDateTime
		,target.ReturnJourneyEndDateTime
		,target.IsApproximate
		,target.ScheduleStatusID
		,target.PreviousScheduleStatusID
		,target.PACACID
		,target.PatientAssessmentID
		,target.ServicePointStayID
		,target.ServicePointStayServicePointID
		,target.ContractedServicesID
		,target.AllocationOverrideFlag
		,target.WaitingListID
		,target.ParentScheduleEventID
		,target.AdmissionOfferID
		,target.MoveRequestedByID
		,target.CodingStatusID
		,target.ClinicalCostCentresID
		,target.PatientConfirmationStatusID
		,target.ScheduleIdentifier
		,target.OperationDuration
		,target.AnaestheticDuration
		,target.AnaesthetistID
		,target.IntoTheatreDateTime
		,target.IntoPostOpDateTime
		,target.IsUnplannedReturn
		,target.AEAttendanceID
		,target.PregnancyStatusID
		,target.LMPDate
		,target.BedStayID
		,target.InvoiceTag
		,target.DNANextScheduleEventID
		,target.ScheduleRecurrenceID
		,target.LocationTypeHealthOrganisationID
		,target.SessionLateStartReasonID
		,target.SeenByProfessionalCarerID
		,target.AnaestheticTypeID
		,target.LinkedServicePointID
		,target.LinkedBedID
		,target.NewAppointmentsDescription
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.PriorPointerID
		,target.ExternalKey
		,target.CompensableDetailsID
		,target.PatientClassificationTypeID
		,target.DaycareEpisodeID
		,target.DietaryRequirementsID
		,target.TransportReference
		,target.ParentFacilityScheduleID
		,target.PlannedAttendees
		,target.ActualAttendees
		,target.FollowUpCount
		,target.NonAppServerLastModifyDateTime
		,target.UserID
		,target.AnaestheticDateTime
		,target.BookingTypeID
		,target.IsOverbooked
		,target.IsEBSBooking
		,target.UniqueBookingReferenceNo
		,target.BedCategoryID
		,target.ReferralReceivedFlag
		,target.SendSMS
		,target.LastModifiedDetails
		,target.TreatInitFlag
		,target.ProviderSpellID
		,target.ContactScheduleID
		,target.PatientChosenBy
		,target.ConsultationMediaID
		,target.OfferDateTime
		,target.OtherDateTime
		,target.IsShortNotice
		,target.IsUnplanned
		,target.IsHoldingArea
		,target.FollowUp
		,target.RTTStatusID
		,target.EarliestReasonableOfferDateTime
		,target.PatientConfirmationDateTime
		,target.IsVerbal
		,target.IsFirstDefinitivetreatment
		,target.AppointmentPurposeID
		,target.PsychotropicMedicationID
		,target.CareProfessionalRoleID
		,target.IsCommunityActivity
		,target.HealthOrganisationOwnerID
		,target.LorenzoCreated
		)

then
	update
	set
		 target.ScheduleEventID = source.ScheduleEventID
		,target.SpecialtyID = source.SpecialtyID
		,target.SessionTransactionID = source.SessionTransactionID
		,target.ProviderID = source.ProviderID
		,target.ContractID = source.ContractID
		,target.PurchaserID = source.PurchaserID
		,target.ProfessionalCarerID = source.ProfessionalCarerID
		,target.ReferralID = source.ReferralID
		,target.ProfessionalCarerEpisodeID = source.ProfessionalCarerEpisodeID
		,target.PatientID = source.PatientID
		,target.AccessLevelID = source.AccessLevelID
		,target.RequestDateTime = source.RequestDateTime
		,target.StartDateTime = source.StartDateTime
		,target.EndDateTime = source.EndDateTime
		,target.VisitTypeID = source.VisitTypeID
		,target.ScheduleTypeID = source.ScheduleTypeID
		,target.ReasonID = source.ReasonID
		,target.DataLinkageConsentID = source.DataLinkageConsentID
		,target.ReasonComments = source.ReasonComments
		,target.UrgencyID = source.UrgencyID
		,target.PriorityID = source.PriorityID
		,target.TransportID = source.TransportID
		,target.ScheduleCancellationReasonID = source.ScheduleCancellationReasonID
		,target.CancelledDateTime = source.CancelledDateTime
		,target.MovementReasonID = source.MovementReasonID
		,target.MovemenDateTime = source.MovemenDateTime
		,target.ArrivedDateTime = source.ArrivedDateTime
		,target.SeenDateTime = source.SeenDateTime
		,target.DepartedDateTime = source.DepartedDateTime
		,target.AttendanceID = source.AttendanceID
		,target.DidNotAttendReasonID = source.DidNotAttendReasonID
		,target.OutcomeID = source.OutcomeID
		,target.PreviousScheduleOutcomeID = source.PreviousScheduleOutcomeID
		,target.Comments = source.Comments
		,target.PriorID = source.PriorID
		,target.CalledDateTime = source.CalledDateTime
		,target.ServicePointID = source.ServicePointID
		,target.Duration = source.Duration
		,target.AdministrativeCategoryID = source.AdministrativeCategoryID
		,target.CancellationByID = source.CancellationByID
		,target.ServicePointSessionID = source.ServicePointSessionID
		,target.MoveCount = source.MoveCount
		,target.StaffTeamID = source.StaffTeamID
		,target.LinkID = source.LinkID
		,target.LocationTypeID = source.LocationTypeID
		,target.Location = source.Location
		,target.ActualDuration = source.ActualDuration
		,target.OutwardJourneyDuration = source.OutwardJourneyDuration
		,target.ReturnJourneyDuration = source.ReturnJourneyDuration
		,target.GroupPlannedNumber = source.GroupPlannedNumber
		,target.GroupActualNumber = source.GroupActualNumber
		,target.ContactPurposeID = source.ContactPurposeID
		,target.ContactTypeID = source.ContactTypeID
		,target.ResourceUnits = source.ResourceUnits
		,target.ResourceUnitTypeID = source.ResourceUnitTypeID
		,target.StaffActivityTypeID = source.StaffActivityTypeID
		,target.OutwardJourneyStartDateTime = source.OutwardJourneyStartDateTime
		,target.OutwardJourneyEndDateTime = source.OutwardJourneyEndDateTime
		,target.ReturnJourneyStartDateTime = source.ReturnJourneyStartDateTime
		,target.ReturnJourneyEndDateTime = source.ReturnJourneyEndDateTime
		,target.IsApproximate = source.IsApproximate
		,target.ScheduleStatusID = source.ScheduleStatusID
		,target.PreviousScheduleStatusID = source.PreviousScheduleStatusID
		,target.PACACID = source.PACACID
		,target.PatientAssessmentID = source.PatientAssessmentID
		,target.ServicePointStayID = source.ServicePointStayID
		,target.ServicePointStayServicePointID = source.ServicePointStayServicePointID
		,target.ContractedServicesID = source.ContractedServicesID
		,target.AllocationOverrideFlag = source.AllocationOverrideFlag
		,target.WaitingListID = source.WaitingListID
		,target.ParentScheduleEventID = source.ParentScheduleEventID
		,target.AdmissionOfferID = source.AdmissionOfferID
		,target.MoveRequestedByID = source.MoveRequestedByID
		,target.CodingStatusID = source.CodingStatusID
		,target.ClinicalCostCentresID = source.ClinicalCostCentresID
		,target.PatientConfirmationStatusID = source.PatientConfirmationStatusID
		,target.ScheduleIdentifier = source.ScheduleIdentifier
		,target.OperationDuration = source.OperationDuration
		,target.AnaestheticDuration = source.AnaestheticDuration
		,target.AnaesthetistID = source.AnaesthetistID
		,target.IntoTheatreDateTime = source.IntoTheatreDateTime
		,target.IntoPostOpDateTime = source.IntoPostOpDateTime
		,target.IsUnplannedReturn = source.IsUnplannedReturn
		,target.AEAttendanceID = source.AEAttendanceID
		,target.PregnancyStatusID = source.PregnancyStatusID
		,target.LMPDate = source.LMPDate
		,target.BedStayID = source.BedStayID
		,target.InvoiceTag = source.InvoiceTag
		,target.DNANextScheduleEventID = source.DNANextScheduleEventID
		,target.ScheduleRecurrenceID = source.ScheduleRecurrenceID
		,target.LocationTypeHealthOrganisationID = source.LocationTypeHealthOrganisationID
		,target.SessionLateStartReasonID = source.SessionLateStartReasonID
		,target.SeenByProfessionalCarerID = source.SeenByProfessionalCarerID
		,target.AnaestheticTypeID = source.AnaestheticTypeID
		,target.LinkedServicePointID = source.LinkedServicePointID
		,target.LinkedBedID = source.LinkedBedID
		,target.NewAppointmentsDescription = source.NewAppointmentsDescription
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.PriorPointerID = source.PriorPointerID
		,target.ExternalKey = source.ExternalKey
		,target.CompensableDetailsID = source.CompensableDetailsID
		,target.PatientClassificationTypeID = source.PatientClassificationTypeID
		,target.DaycareEpisodeID = source.DaycareEpisodeID
		,target.DietaryRequirementsID = source.DietaryRequirementsID
		,target.TransportReference = source.TransportReference
		,target.ParentFacilityScheduleID = source.ParentFacilityScheduleID
		,target.PlannedAttendees = source.PlannedAttendees
		,target.ActualAttendees = source.ActualAttendees
		,target.FollowUpCount = source.FollowUpCount
		,target.NonAppServerLastModifyDateTime = source.NonAppServerLastModifyDateTime
		,target.UserID = source.UserID
		,target.AnaestheticDateTime = source.AnaestheticDateTime
		,target.BookingTypeID = source.BookingTypeID
		,target.IsOverbooked = source.IsOverbooked
		,target.IsEBSBooking = source.IsEBSBooking
		,target.UniqueBookingReferenceNo = source.UniqueBookingReferenceNo
		,target.BedCategoryID = source.BedCategoryID
		,target.ReferralReceivedFlag = source.ReferralReceivedFlag
		,target.SendSMS = source.SendSMS
		,target.LastModifiedDetails = source.LastModifiedDetails
		,target.TreatInitFlag = source.TreatInitFlag
		,target.ProviderSpellID = source.ProviderSpellID
		,target.ContactScheduleID = source.ContactScheduleID
		,target.PatientChosenBy = source.PatientChosenBy
		,target.ConsultationMediaID = source.ConsultationMediaID
		,target.OfferDateTime = source.OfferDateTime
		,target.OtherDateTime = source.OtherDateTime
		,target.IsShortNotice = source.IsShortNotice
		,target.IsUnplanned = source.IsUnplanned
		,target.IsHoldingArea = source.IsHoldingArea
		,target.FollowUp = source.FollowUp
		,target.RTTStatusID = source.RTTStatusID
		,target.EarliestReasonableOfferDateTime = source.EarliestReasonableOfferDateTime
		,target.PatientConfirmationDateTime = source.PatientConfirmationDateTime
		,target.IsVerbal = source.IsVerbal
		,target.IsFirstDefinitivetreatment = source.IsFirstDefinitivetreatment
		,target.AppointmentPurposeID = source.AppointmentPurposeID
		,target.PsychotropicMedicationID = source.PsychotropicMedicationID
		,target.CareProfessionalRoleID = source.CareProfessionalRoleID
		,target.IsCommunityActivity = source.IsCommunityActivity
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.LorenzoCreated = source.LorenzoCreated

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime
