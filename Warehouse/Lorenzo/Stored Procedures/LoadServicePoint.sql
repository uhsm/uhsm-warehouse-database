﻿CREATE procedure [Lorenzo].[LoadServicePoint] as 


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(ServicePoint.LorenzoCreated)
		from
			Lorenzo.ServicePoint
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			SessionTransactionID
			,SpecialtyID
			,StaffTeamID
			,ProfessionalCarerID
			,HealthOrganisationID
			,ServicePointCode
			,NominalDepartmentCode
			,ServicePointDescription
			,ServicePoint
			,WarningLevel
			,MaximumLevel
			,MaximumCapacity
			,FacilityPurposeID
			,IsHolidayExcluded
			,StartDateTime
			,EndDateTime
			,ServicePointPurposeID
			,ServicePointTypeID
			,IsCurrent
			,IsAELocation
			,IsOutpatientLocation
			,PatientDocumentTypeID
			,LocalCategoryID
			,DiagnosisFlag
			,Instructions
			,MaxRescheduleDaysDifference
			,AgeRestrictionType
			,AgeRestrictionQualifier
			,CreatedDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,PriorPointerID
			,ExternalKey
			,UseBedManagement
			,BookClinicSlotInAdvanceDays
			,TheatreTypeID
			,HasSeparateAnaestheticRoom
			,HasSeparateRecoveryRoom
			,IsGroupClinic
			,AutoDefaultDepartureDetails
			,AutoChargeAccount
			,AllowsPartialBookings
			,PartialBookingLeadTime
			,PartialBookingLeadTimeUnits
			,ClinicianBasedLeadTime
			,ClinicalDocumentTemplateID
			,ExcludeInCDS
			,AdministrationTypeID
			,IsCommunity
			,CommunityServiceTypeID
			,HealthOrganisationOwnerID
			,LorenzoCreated
			)
into
	#TLoadPASServicePoint
from
	(
	select
		ServicePointID
		,SessionTransactionID = cast(SessionTransactionID as int)
		,SpecialtyID = cast(SpecialtyID as int)
		,StaffTeamID = cast(StaffTeamID as int)
		,ProfessionalCarerID = cast(ProfessionalCarerID as int)
		,HealthOrganisationID = cast(HealthOrganisationID as int)
		,ServicePointCode = cast(nullif(ServicePointCode, '') as varchar(50))
		,NominalDepartmentCode = cast(nullif(NominalDepartmentCode, '') as varchar(50))
		,ServicePointDescription = cast(nullif(ServicePointDescription, '') as varchar(255))
		,ServicePoint = cast(nullif(ServicePoint, '') as varchar(100))
		,WarningLevel = cast(WarningLevel as int)
		,MaximumLevel = cast(MaximumLevel as int)
		,MaximumCapacity = cast(MaximumCapacity as int)
		,FacilityPurposeID = cast(FacilityPurposeID as int)
		,IsHolidayExcluded = cast(nullif(IsHolidayExcluded, '') as char(1))
		,StartDateTime = cast(StartDateTime as datetime)
		,EndDateTime = cast(EndDateTime as datetime)
		,ServicePointPurposeID = cast(ServicePointPurposeID as int)
		,ServicePointTypeID = cast(ServicePointTypeID as int)
		,IsCurrent = cast(nullif(IsCurrent, '') as char(1))
		,IsAELocation = cast(nullif(IsAELocation, '') as char(1))
		,IsOutpatientLocation = cast(nullif(IsOutpatientLocation, '') as char(1))
		,PatientDocumentTypeID = cast(PatientDocumentTypeID as int)
		,LocalCategoryID = cast(LocalCategoryID as int)
		,DiagnosisFlag = cast(nullif(DiagnosisFlag, '') as char(1))
		,Instructions = cast(nullif(Instructions, '') as varchar(255))
		,MaxRescheduleDaysDifference = cast(MaxRescheduleDaysDifference as int)
		,AgeRestrictionType = cast(nullif(AgeRestrictionType, '') as varchar(50))
		,AgeRestrictionQualifier = cast(AgeRestrictionQualifier as int)
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(50))
		,UserModify = cast(nullif(UserModify, '') as varchar(50))
		,PriorPointerID = cast(PriorPointerID as int)
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(50))
		,UseBedManagement = cast(nullif(UseBedManagement, '') as char(1))
		,BookClinicSlotInAdvanceDays = cast(BookClinicSlotInAdvanceDays as int)
		,TheatreTypeID = cast(TheatreTypeID as int)
		,HasSeparateAnaestheticRoom = cast(nullif(HasSeparateAnaestheticRoom, '') as char(1))
		,HasSeparateRecoveryRoom = cast(nullif(HasSeparateRecoveryRoom, '') as char(1))
		,IsGroupClinic = cast(nullif(IsGroupClinic, '') as char(1))
		,AutoDefaultDepartureDetails = cast(nullif(AutoDefaultDepartureDetails, '') as char(1))
		,AutoChargeAccount = cast(nullif(AutoChargeAccount, '') as char(1))
		,AllowsPartialBookings = cast(nullif(AllowsPartialBookings, '') as char(1))
		,PartialBookingLeadTime = cast(PartialBookingLeadTime as int)
		,PartialBookingLeadTimeUnits = cast(nullif(PartialBookingLeadTimeUnits, '') as char(1))
		,ClinicianBasedLeadTime = cast(nullif(ClinicianBasedLeadTime, '') as char(1))
		,ClinicalDocumentTemplateID = cast(ClinicalDocumentTemplateID as int)
		,ExcludeInCDS = cast(nullif(ExcludeInCDS, '') as char(1))
		,AdministrationTypeID = cast(AdministrationTypeID as int)
		,IsCommunity = cast(nullif(IsCommunity, '') as char(1))
		,CommunityServiceTypeID = cast(CommunityServiceTypeID as int)
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as int)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			ServicePointID = SPONT_REFNO
			,SessionTransactionID = STRAN_REFNO
			,SpecialtyID = SPECT_REFNO
			,StaffTeamID = STEAM_REFNO
			,ProfessionalCarerID = PROCA_REFNO
			,HealthOrganisationID = HEORG_REFNO
			,ServicePointCode = CODE
			,NominalDepartmentCode = NOMINAL_DEPT_CODE
			,ServicePointDescription = DESCRIPTION
			,ServicePoint = NAME
			,WarningLevel = WARN_LEVEL
			,MaximumLevel = MAX_LEVEL
			,MaximumCapacity = MAX_CAPACITY
			,FacilityPurposeID = FCPUR_REFNO
			,IsHolidayExcluded = EXCLUDE_HOLS
			,StartDateTime = START_DTTM
			,EndDateTime = END_DTTM
			,ServicePointPurposeID = PURPS_REFNO
			,ServicePointTypeID = SPTYP_REFNO
			,IsCurrent = CURNT_FLAG
			,IsAELocation = AE_FLAG
			,IsOutpatientLocation = CONTACT_FLAG
			,PatientDocumentTypeID = PDTYP_REFNO
			,LocalCategoryID = LOCAT_REFNO
			,DiagnosisFlag = DIAG_FLAG
			,Instructions = INSTRUCTIONS
			,MaxRescheduleDaysDifference = RESCH_MAX_DIFF
			,AgeRestrictionType = AGE_TYPE
			,AgeRestrictionQualifier = AGE_QUALIFIER
			,CreatedDateTime = CREATE_DTTM
			,ModifiedDateTime = MODIF_DTTM
			,UserCreate = USER_CREATE
			,UserModify = USER_MODIF
			,PriorPointerID = PRIOR_POINTER
			,ExternalKey = EXTERNAL_KEY
			,UseBedManagement = USE_BED_MANAGEMENT
			,BookClinicSlotInAdvanceDays = HORIZ_VALUE
			,TheatreTypeID = THTYP_REFNO
			,HasSeparateAnaestheticRoom = SEP_ANAES_ROOM
			,HasSeparateRecoveryRoom = SEP_RECOV_ROOM
			,IsGroupClinic = GROUP_CLINIC_FLAG
			,AutoDefaultDepartureDetails = AUTO_DEPART_FLAG
			,AutoChargeAccount = CHG_AC_AUTO
			,AllowsPartialBookings = PBK_FLAG
			,PartialBookingLeadTime = PBK_LEAD_TIME
			,PartialBookingLeadTimeUnits = PBK_LEAD_TIME_UNITS
			,ClinicianBasedLeadTime = LEAD_TIME_CLIN_FLAG
			,ClinicalDocumentTemplateID = CLN_DOCTMPLT_REFNO
			,ExcludeInCDS = EXCLUDE_IN_CDS
			,AdministrationTypeID = CFADT_REFNO
			,IsCommunity = COMMUNITY_FLAG
			,CommunityServiceTypeID = CSVTP_REFNO
			,HealthOrganisationOwnerID = OWNER_HEORG_REFNO
			,LorenzoCreated = Created

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.ServicePoint
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASServicePoint on #TLoadPASServicePoint
	(
	ServicePointID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.ServicePoint target
using
	(
	select
		*
	from
		#TLoadPASServicePoint
	
	) source
	on	source.ServicePointID = target.ServicePointID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		 ServicePointID
		,SessionTransactionID
		,SpecialtyID
		,StaffTeamID
		,ProfessionalCarerID
		,HealthOrganisationID
		,ServicePointCode
		,NominalDepartmentCode
		,ServicePointDescription
		,ServicePoint
		,WarningLevel
		,MaximumLevel
		,MaximumCapacity
		,FacilityPurposeID
		,IsHolidayExcluded
		,StartDateTime
		,EndDateTime
		,ServicePointPurposeID
		,ServicePointTypeID
		,IsCurrent
		,IsAELocation
		,IsOutpatientLocation
		,PatientDocumentTypeID
		,LocalCategoryID
		,DiagnosisFlag
		,Instructions
		,MaxRescheduleDaysDifference
		,AgeRestrictionType
		,AgeRestrictionQualifier
		,CreatedDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,PriorPointerID
		,ExternalKey
		,UseBedManagement
		,BookClinicSlotInAdvanceDays
		,TheatreTypeID
		,HasSeparateAnaestheticRoom
		,HasSeparateRecoveryRoom
		,IsGroupClinic
		,AutoDefaultDepartureDetails
		,AutoChargeAccount
		,AllowsPartialBookings
		,PartialBookingLeadTime
		,PartialBookingLeadTimeUnits
		,ClinicianBasedLeadTime
		,ClinicalDocumentTemplateID
		,ExcludeInCDS
		,AdministrationTypeID
		,IsCommunity
		,CommunityServiceTypeID
		,HealthOrganisationOwnerID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		 source.ServicePointID
		,source.SessionTransactionID
		,source.SpecialtyID
		,source.StaffTeamID
		,source.ProfessionalCarerID
		,source.HealthOrganisationID
		,source.ServicePointCode
		,source.NominalDepartmentCode
		,source.ServicePointDescription
		,source.ServicePoint
		,source.WarningLevel
		,source.MaximumLevel
		,source.MaximumCapacity
		,source.FacilityPurposeID
		,source.IsHolidayExcluded
		,source.StartDateTime
		,source.EndDateTime
		,source.ServicePointPurposeID
		,source.ServicePointTypeID
		,source.IsCurrent
		,source.IsAELocation
		,source.IsOutpatientLocation
		,source.PatientDocumentTypeID
		,source.LocalCategoryID
		,source.DiagnosisFlag
		,source.Instructions
		,source.MaxRescheduleDaysDifference
		,source.AgeRestrictionType
		,source.AgeRestrictionQualifier
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.UserCreate
		,source.UserModify
		,source.PriorPointerID
		,source.ExternalKey
		,source.UseBedManagement
		,source.BookClinicSlotInAdvanceDays
		,source.TheatreTypeID
		,source.HasSeparateAnaestheticRoom
		,source.HasSeparateRecoveryRoom
		,source.IsGroupClinic
		,source.AutoDefaultDepartureDetails
		,source.AutoChargeAccount
		,source.AllowsPartialBookings
		,source.PartialBookingLeadTime
		,source.PartialBookingLeadTimeUnits
		,source.ClinicianBasedLeadTime
		,source.ClinicalDocumentTemplateID
		,source.ExcludeInCDS
		,source.AdministrationTypeID
		,source.IsCommunity
		,source.CommunityServiceTypeID
		,source.HealthOrganisationOwnerID
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.SessionTransactionID
		,target.SpecialtyID
		,target.StaffTeamID
		,target.ProfessionalCarerID
		,target.HealthOrganisationID
		,target.ServicePointCode
		,target.NominalDepartmentCode
		,target.ServicePointDescription
		,target.ServicePoint
		,target.WarningLevel
		,target.MaximumLevel
		,target.MaximumCapacity
		,target.FacilityPurposeID
		,target.IsHolidayExcluded
		,target.StartDateTime
		,target.EndDateTime
		,target.ServicePointPurposeID
		,target.ServicePointTypeID
		,target.IsCurrent
		,target.IsAELocation
		,target.IsOutpatientLocation
		,target.PatientDocumentTypeID
		,target.LocalCategoryID
		,target.DiagnosisFlag
		,target.Instructions
		,target.MaxRescheduleDaysDifference
		,target.AgeRestrictionType
		,target.AgeRestrictionQualifier
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.PriorPointerID
		,target.ExternalKey
		,target.UseBedManagement
		,target.BookClinicSlotInAdvanceDays
		,target.TheatreTypeID
		,target.HasSeparateAnaestheticRoom
		,target.HasSeparateRecoveryRoom
		,target.IsGroupClinic
		,target.AutoDefaultDepartureDetails
		,target.AutoChargeAccount
		,target.AllowsPartialBookings
		,target.PartialBookingLeadTime
		,target.PartialBookingLeadTimeUnits
		,target.ClinicianBasedLeadTime
		,target.ClinicalDocumentTemplateID
		,target.ExcludeInCDS
		,target.AdministrationTypeID
		,target.IsCommunity
		,target.CommunityServiceTypeID
		,target.HealthOrganisationOwnerID
		,target.LorenzoCreated
		)

then
	update
	set
		 target.ServicePointID = source.ServicePointID
		,target.SessionTransactionID = source.SessionTransactionID
		,target.SpecialtyID = source.SpecialtyID
		,target.StaffTeamID = source.StaffTeamID
		,target.ProfessionalCarerID = source.ProfessionalCarerID
		,target.HealthOrganisationID = source.HealthOrganisationID
		,target.ServicePointCode = source.ServicePointCode
		,target.NominalDepartmentCode = source.NominalDepartmentCode
		,target.ServicePointDescription = source.ServicePointDescription
		,target.ServicePoint = source.ServicePoint
		,target.WarningLevel = source.WarningLevel
		,target.MaximumLevel = source.MaximumLevel
		,target.MaximumCapacity = source.MaximumCapacity
		,target.FacilityPurposeID = source.FacilityPurposeID
		,target.IsHolidayExcluded = source.IsHolidayExcluded
		,target.StartDateTime = source.StartDateTime
		,target.EndDateTime = source.EndDateTime
		,target.ServicePointPurposeID = source.ServicePointPurposeID
		,target.ServicePointTypeID = source.ServicePointTypeID
		,target.IsCurrent = source.IsCurrent
		,target.IsAELocation = source.IsAELocation
		,target.IsOutpatientLocation = source.IsOutpatientLocation
		,target.PatientDocumentTypeID = source.PatientDocumentTypeID
		,target.LocalCategoryID = source.LocalCategoryID
		,target.DiagnosisFlag = source.DiagnosisFlag
		,target.Instructions = source.Instructions
		,target.MaxRescheduleDaysDifference = source.MaxRescheduleDaysDifference
		,target.AgeRestrictionType = source.AgeRestrictionType
		,target.AgeRestrictionQualifier = source.AgeRestrictionQualifier
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.PriorPointerID = source.PriorPointerID
		,target.ExternalKey = source.ExternalKey
		,target.UseBedManagement = source.UseBedManagement
		,target.BookClinicSlotInAdvanceDays = source.BookClinicSlotInAdvanceDays
		,target.TheatreTypeID = source.TheatreTypeID
		,target.HasSeparateAnaestheticRoom = source.HasSeparateAnaestheticRoom
		,target.HasSeparateRecoveryRoom = source.HasSeparateRecoveryRoom
		,target.IsGroupClinic = source.IsGroupClinic
		,target.AutoDefaultDepartureDetails = source.AutoDefaultDepartureDetails
		,target.AutoChargeAccount = source.AutoChargeAccount
		,target.AllowsPartialBookings = source.AllowsPartialBookings
		,target.PartialBookingLeadTime = source.PartialBookingLeadTime
		,target.PartialBookingLeadTimeUnits = source.PartialBookingLeadTimeUnits
		,target.ClinicianBasedLeadTime = source.ClinicianBasedLeadTime
		,target.ClinicalDocumentTemplateID = source.ClinicalDocumentTemplateID
		,target.ExcludeInCDS = source.ExcludeInCDS
		,target.AdministrationTypeID = source.AdministrationTypeID
		,target.IsCommunity = source.IsCommunity
		,target.CommunityServiceTypeID = source.CommunityServiceTypeID
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime