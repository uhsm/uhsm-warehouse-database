﻿CREATE PROCEDURE [Lorenzo].[LoadServicePointSession]

AS



declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(ServicePointSession.LorenzoCreated)
		from
			Lorenzo.ServicePointSession
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 ServicePointSessionID
			,SpecialtyID
			,SessionTransactionID
			,StaffTeamID
			,HealthOrganisationID
			,ProfessionalCarerID
			,ServicePointID
			,StartDateTime
			,EndDateTime
			,SlotDuration
			,DayOfWeekID
			,SessionStatusID
			,Instructions
			,CreatedDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,SessionDuration
			,PriorPointerID
			,IsDirty
			,IsTemplate
			,ExternalKey
			,ServicePointSessionCode
			,ServicePointSessionDescription
			,SessionCancelDateTime
			,FrequencyID
			,FrequencyOffsetID
			,FrequencyOffset2ID
			,FrequencyOffset3ID
			,FrequencyOffset4ID
			,ServicePointSessionTemplateID
			,ServicePointSessionPurposeID
			,Comments
			,ActualProfessionalCarerID
			,ProfessionalCarerArrivalDateTime
			,ProfessionalCarerDepartureDateTime
			,ServicePointSessionChangeReasonID
			,DaysAhead
			,DaysBehind
			,ServicePointSessionStartStatusID
			,ServicePointSessionEndStatusID
			,ServicePointSessionStartStatusReasonID
			,ServicePointSessionEndStatusReasonID
			,StatusChangeRequestedByID
			,SessionTypeID
			,OverbookTol
			,ClosingDateTime
			,SessionLateStartReasonID
			,MaxPatients
			,IsGroupFlag
			,SessionStartDelayReasonID
			,DelayInSessionStart
			,WarningLevel
			,FacilityPurposeID
			,IsFinal
			,HasVariableSlots
			,SessionFrequencyID
			,SessionOffsetID
			,OnWeeks
			,OnDays
			,DaysInWeek1
			,DaysInWeek2
			,DaysInWeek3
			,DaysInWeek4
			,DaysInWeek5
			,DaysInWeekL
			,ExcludeHolidays
			,RescheduleFromDateTime
			,CopyOverbookSlotsFlag
			,CanRescheduleEarlierFlag
			,UseOverbookSlotsFlag
			,MaximumDifferenceRescheduleDays
			,RestructureFromDateTime
			,IsPartialBooking
			,PartialBookingLeadTime
			,PartialBookingLeadTimeMeasure
			,IsEBS
			,EBSServiceID
			,NewVisits
			,PublishAllSlotsFlag
			,LeadTimeFromClinicianFlag
			,NumberOfSessions
			,AnaesthetisProfessionalCarerID
			,ServicePointMaximumAppointments
			,ServicePointAppointmentsWarningLevel
			,ServicePointTotalBookedAppointments
			,ServicePointSessionTotalBookedAppointments
			,HealthOrganisationOwnerID
			,LorenzoCreated
			)
into
	#TLoadPASServicePointSession
from
	(
	select
		ServicePointSessionID
		,SpecialtyID = cast(SpecialtyID as numeric)
		,SessionTransactionID = cast(SessionTransactionID as numeric)
		,StaffTeamID = cast(StaffTeamID as numeric)
		,HealthOrganisationID = cast(HealthOrganisationID as numeric)
		,ProfessionalCarerID = cast(ProfessionalCarerID as numeric)
		,ServicePointID = cast(ServicePointID as numeric)
		,StartDateTime = cast(StartDateTime as datetime)
		,EndDateTime = cast(EndDateTime as datetime)
		,SlotDuration = cast(SlotDuration as numeric)
		,DayOfWeekID = cast(DayOfWeekID as numeric)
		,SessionStatusID = cast(SessionStatusID as numeric)
		,Instructions = cast(nullif(Instructions, '') as varchar(255))
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(30))
		,UserModify = cast(nullif(UserModify, '') as varchar(30))
		,SessionDuration = cast(SessionDuration as numeric)
		,PriorPointerID = cast(PriorPointerID as numeric)
		,IsDirty = cast(nullif(IsDirty, '') as char(1))
		,IsTemplate = cast(nullif(IsTemplate, '') as char(1))
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(20))
		,ServicePointSessionCode = cast(nullif(ServicePointSessionCode, '') as varchar(20))
		,ServicePointSessionDescription = cast(nullif(ServicePointSessionDescription, '') as varchar(255))
		,SessionCancelDateTime = cast(SessionCancelDateTime as datetime)
		,FrequencyID = cast(FrequencyID as numeric)
		,FrequencyOffsetID = cast(FrequencyOffsetID as numeric)
		,FrequencyOffset2ID = cast(FrequencyOffset2ID as numeric)
		,FrequencyOffset3ID = cast(FrequencyOffset3ID as numeric)
		,FrequencyOffset4ID = cast(FrequencyOffset4ID as numeric)
		,ServicePointSessionTemplateID = cast(ServicePointSessionTemplateID as numeric)
		,ServicePointSessionPurposeID = cast(ServicePointSessionPurposeID as numeric)
		,Comments = cast(nullif(Comments, '') as varchar(255))
		,ActualProfessionalCarerID = cast(ActualProfessionalCarerID as numeric)
		,ProfessionalCarerArrivalDateTime = cast(ProfessionalCarerArrivalDateTime as datetime)
		,ProfessionalCarerDepartureDateTime = cast(ProfessionalCarerDepartureDateTime as datetime)
		,ServicePointSessionChangeReasonID = cast(ServicePointSessionChangeReasonID as numeric)
		,DaysAhead = cast(DaysAhead as numeric)
		,DaysBehind = cast(DaysBehind as numeric)
		,ServicePointSessionStartStatusID = cast(ServicePointSessionStartStatusID as numeric)
		,ServicePointSessionEndStatusID = cast(ServicePointSessionEndStatusID as numeric)
		,ServicePointSessionStartStatusReasonID = cast(ServicePointSessionStartStatusReasonID as numeric)
		,ServicePointSessionEndStatusReasonID = cast(ServicePointSessionEndStatusReasonID as numeric)
		,StatusChangeRequestedByID = cast(StatusChangeRequestedByID as numeric)
		,SessionTypeID = cast(SessionTypeID as numeric)
		,OverbookTol = cast(OverbookTol as numeric)
		,ClosingDateTime = cast(ClosingDateTime as datetime)
		,SessionLateStartReasonID = cast(SessionLateStartReasonID as numeric)
		,MaxPatients = cast(MaxPatients as numeric)
		,IsGroupFlag = cast(nullif(IsGroupFlag, '') as char(1))
		,SessionStartDelayReasonID = cast(SessionStartDelayReasonID as numeric)
		,DelayInSessionStart = cast(DelayInSessionStart as numeric)
		,WarningLevel = cast(WarningLevel as numeric)
		,FacilityPurposeID = cast(FacilityPurposeID as numeric)
		,IsFinal = cast(nullif(IsFinal, '') as char(1))
		,HasVariableSlots = cast(nullif(HasVariableSlots, '') as char(1))
		,SessionFrequencyID = cast(SessionFrequencyID as numeric)
		,SessionOffsetID = cast(SessionOffsetID as numeric)
		,OnWeeks = cast(nullif(OnWeeks, '') as varchar(50))
		,OnDays = cast(nullif(OnDays, '') as varchar(30))
		,DaysInWeek1 = cast(nullif(DaysInWeek1, '') as varchar(30))
		,DaysInWeek2 = cast(nullif(DaysInWeek2, '') as varchar(30))
		,DaysInWeek3 = cast(nullif(DaysInWeek3, '') as varchar(30))
		,DaysInWeek4 = cast(nullif(DaysInWeek4, '') as varchar(30))
		,DaysInWeek5 = cast(nullif(DaysInWeek5, '') as varchar(30))
		,DaysInWeekL = cast(nullif(DaysInWeekL, '') as varchar(30))
		,ExcludeHolidays = cast(nullif(ExcludeHolidays, '') as char(1))
		,RescheduleFromDateTime = cast(RescheduleFromDateTime as datetime)
		,CopyOverbookSlotsFlag = cast(nullif(CopyOverbookSlotsFlag, '') as char(1))
		,CanRescheduleEarlierFlag = cast(nullif(CanRescheduleEarlierFlag, '') as char(1))
		,UseOverbookSlotsFlag = cast(nullif(UseOverbookSlotsFlag, '') as char(1))
		,MaximumDifferenceRescheduleDays = cast(MaximumDifferenceRescheduleDays as numeric)
		,RestructureFromDateTime = cast(RestructureFromDateTime as datetime)
		,IsPartialBooking = cast(nullif(IsPartialBooking, '') as char(1))
		,PartialBookingLeadTime = cast(PartialBookingLeadTime as numeric)
		,PartialBookingLeadTimeMeasure = cast(nullif(PartialBookingLeadTimeMeasure, '') as char(1))
		,IsEBS = cast(nullif(IsEBS, '') as char(1))
		,EBSServiceID = cast(nullif(EBSServiceID, '') as varchar(255))
		,NewVisits = cast(NewVisits as numeric)
		,PublishAllSlotsFlag = cast(nullif(PublishAllSlotsFlag, '') as char(1))
		,LeadTimeFromClinicianFlag = cast(nullif(LeadTimeFromClinicianFlag, '') as char(1))
		,NumberOfSessions = cast(NumberOfSessions as numeric)
		,AnaesthetisProfessionalCarerID = cast(AnaesthetisProfessionalCarerID as numeric)
		,ServicePointMaximumAppointments = cast(ServicePointMaximumAppointments as numeric)
		,ServicePointAppointmentsWarningLevel = cast(ServicePointAppointmentsWarningLevel as numeric)
		,ServicePointTotalBookedAppointments = cast(ServicePointTotalBookedAppointments as numeric)
		,ServicePointSessionTotalBookedAppointments = cast(ServicePointSessionTotalBookedAppointments as numeric)
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as numeric)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			ServicePointSessionID =  [SPSSN_REFNO]
			  ,SpecialtyID = [SPECT_REFNO]
			  ,SessionTransactionID = [STRAN_REFNO]
			  ,StaffTeamID = [STEAM_REFNO]
			  ,HealthOrganisationID = [HEORG_REFNO]
			  ,ProfessionalCarerID = [PROCA_REFNO]
			  ,ServicePointID = [SPONT_REFNO]
			  ,StartDateTime = [START_DTTM]
			  ,EndDateTime = [END_DTTM]
			  ,SlotDuration = [SLOT_DURATION]
			  ,DayOfWeekID = [DAYOW_REFNO]
			  ,SessionStatusID = [SSTAT_REFNO]
			  ,Instructions = [INSTRUCTIONS]
			  ,CreatedDateTime = [CREATE_DTTM]
			  ,ModifiedDateTime = [MODIF_DTTM]
			  ,UserCreate = [USER_CREATE]
			  ,UserModify = [USER_MODIF]
			  ,SessionDuration = [SESSION_DURATION]
			  ,PriorPointerID = [PRIOR_POINTER]
			  ,IsDirty = [DIRTY_FLAG]
			  ,IsTemplate = [TEMPLATE_FLAG]
			  ,ExternalKey = [EXTERNAL_KEY]
			  ,ServicePointSessionCode = [CODE]
			  ,ServicePointSessionDescription = [DESCRIPTION]
			  ,SessionCancelDateTime = [CANCEL_DTTM]
			  ,FrequencyID = [FREQN_REFNO]
			  ,FrequencyOffsetID = [FREOF_REFNO]
			  ,FrequencyOffset2ID = [FREOF2_REFNO]
			  ,FrequencyOffset3ID = [FREOF3_REFNO]
			  ,FrequencyOffset4ID = [FREOF4_REFNO]
			  ,ServicePointSessionTemplateID = [TMPLT_REFNO]
			  ,ServicePointSessionPurposeID = [PURPS_REFNO]
			  ,Comments = [COMMENTS]
			  ,ActualProfessionalCarerID = [ACTUAL_PROCA_REFNO]
			  ,ProfessionalCarerArrivalDateTime = [PROCA_ARRIVED_DTTM]
			  ,ProfessionalCarerDepartureDateTime = [PROCA_DEPARTED_DTTM]
			  ,ServicePointSessionChangeReasonID = [SSCHR_REFNO]
			  ,DaysAhead = [DAYS_AHEAD]
			  ,DaysBehind = [DAYS_BEHIND]
			  ,ServicePointSessionStartStatusID = [SSSTS_REFNO]
			  ,ServicePointSessionEndStatusID = [SSENS_REFNO]
			  ,ServicePointSessionStartStatusReasonID = [SSSRS_REFNO]
			  ,ServicePointSessionEndStatusReasonID =[SSERS_REFNO]
			  ,StatusChangeRequestedByID = [STCRB_REFNO]
			  ,SessionTypeID = [SESTY_REFNO]
			  ,OverbookTol = [OVERBOOK_TOL]
			  ,ClosingDateTime = [CLOSING_DTTM]
			  ,SessionLateStartReasonID = [SLATE_REFNO]
			  ,MaxPatients = [MAX_PATIENTS]
			  ,IsGroupFlag = [GROUP_CLINIC_FLAG]
			  ,SessionStartDelayReasonID = [SDLRS_REFNO]
			  ,DelayInSessionStart = [START_DELAY]
			  ,WarningLevel = [WARN_LEVEL]
			  ,FacilityPurposeID = [FCPUR_REFNO]
			  ,IsFinal = [FINAL_FLAG]
			  ,HasVariableSlots = [VAR_SLOTS_FLAG]
			  ,SessionFrequencyID = [SFRQN_REFNO]
			  ,SessionOffsetID = [SFROF_REFNO]
			  ,OnWeeks = [ON_WEEKS]
			  ,OnDays = [ON_DAYS]
			  ,DaysInWeek1 = [DAYS_IN_WEEK1]
			  ,DaysInWeek2 = [DAYS_IN_WEEK2]
			  ,DaysInWeek3 = [DAYS_IN_WEEK3]
			  ,DaysInWeek4 = [DAYS_IN_WEEK4]
			  ,DaysInWeek5 = [DAYS_IN_WEEK5]
			  ,DaysInWeekL = [DAYS_IN_WEEKL]
			  ,ExcludeHolidays = [EXCLUDE_HOLS]
			  ,RescheduleFromDateTime = [RESCH_FROM_DTTM]
			  ,CopyOverbookSlotsFlag = [COPY_OVERBOOK_FLAG]
			  ,CanRescheduleEarlierFlag = [RESCH_EARLIER]
			  ,UseOverbookSlotsFlag = [USE_OVERBOOK_FLAG]
			  ,MaximumDifferenceRescheduleDays = [RESCH_MAX_DIFF] 
			  ,RestructureFromDateTime = [RESTU_FROM_DTTM]
			  ,IsPartialBooking = [PBK_FLAG]
			  ,PartialBookingLeadTime = [PBK_LEAD_TIME]
			  ,PartialBookingLeadTimeMeasure = [PBK_LEAD_TIME_UNITS]
			  ,IsEBS = [EBS_FLAG] 
			  ,EBSServiceID = [EBS_SER_ID]
			  ,NewVisits = [NEW_VISITS]
			  ,PublishAllSlotsFlag = [PUB_ALL_SLOTS]
			  ,LeadTimeFromClinicianFlag = [LEAD_TIME_CLIN_FLAG]
			  ,NumberOfSessions = [SESSION_COUNT]
			  ,AnaesthetisProfessionalCarerID = [ANAES_PROCA_REFNO]
			  ,ServicePointMaximumAppointments = [SPONT_MAX]
			  ,ServicePointAppointmentsWarningLevel = [SPONT_WARN]
			  ,ServicePointTotalBookedAppointments = [SPONT_TOT_BOOKED]
			  ,ServicePointSessionTotalBookedAppointments = [SPSSN_TOT_BOOKED]
			  ,HealthOrganisationOwnerID = [OWNER_HEORG_REFNO]
			  ,LorenzoCreated = [Created]


			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.ServicePointSession
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASServicePointSession on #TLoadPASServicePointSession
	(
	ServicePointSessionID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.ServicePointSession target
using
	(
	select
		*
	from
		#TLoadPASServicePointSession
	
	) source
	on	source.ServicePointSessionID = target.ServicePointSessionID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		  ServicePointSessionID
		,SpecialtyID
		,SessionTransactionID
		,StaffTeamID
		,HealthOrganisationID
		,ProfessionalCarerID
		,ServicePointID
		,StartDateTime
		,EndDateTime
		,SlotDuration
		,DayOfWeekID
		,SessionStatusID
		,Instructions
		,CreatedDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,SessionDuration
		,PriorPointerID
		,IsDirty
		,IsTemplate
		,ExternalKey
		,ServicePointSessionCode
		,ServicePointSessionDescription
		,SessionCancelDateTime
		,FrequencyID
		,FrequencyOffsetID
		,FrequencyOffset2ID
		,FrequencyOffset3ID
		,FrequencyOffset4ID
		,ServicePointSessionTemplateID
		,ServicePointSessionPurposeID
		,Comments
		,ActualProfessionalCarerID
		,ProfessionalCarerArrivalDateTime
		,ProfessionalCarerDepartureDateTime
		,ServicePointSessionChangeReasonID
		,DaysAhead
		,DaysBehind
		,ServicePointSessionStartStatusID
		,ServicePointSessionEndStatusID
		,ServicePointSessionStartStatusReasonID
		,ServicePointSessionEndStatusReasonID
		,StatusChangeRequestedByID
		,SessionTypeID
		,OverbookTol
		,ClosingDateTime
		,SessionLateStartReasonID
		,MaxPatients
		,IsGroupFlag
		,SessionStartDelayReasonID
		,DelayInSessionStart
		,WarningLevel
		,FacilityPurposeID
		,IsFinal
		,HasVariableSlots
		,SessionFrequencyID
		,SessionOffsetID
		,OnWeeks
		,OnDays
		,DaysInWeek1
		,DaysInWeek2
		,DaysInWeek3
		,DaysInWeek4
		,DaysInWeek5
		,DaysInWeekL
		,ExcludeHolidays
		,RescheduleFromDateTime
		,CopyOverbookSlotsFlag
		,CanRescheduleEarlierFlag
		,UseOverbookSlotsFlag
		,MaximumDifferenceRescheduleDays
		,RestructureFromDateTime
		,IsPartialBooking
		,PartialBookingLeadTime
		,PartialBookingLeadTimeMeasure
		,IsEBS
		,EBSServiceID
		,NewVisits
		,PublishAllSlotsFlag
		,LeadTimeFromClinicianFlag
		,NumberOfSessions
		,AnaesthetisProfessionalCarerID
		,ServicePointMaximumAppointments
		,ServicePointAppointmentsWarningLevel
		,ServicePointTotalBookedAppointments
		,ServicePointSessionTotalBookedAppointments
		,HealthOrganisationOwnerID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		 source.ServicePointSessionID
		,source.SpecialtyID
		,source.SessionTransactionID
		,source.StaffTeamID
		,source.HealthOrganisationID
		,source.ProfessionalCarerID
		,source.ServicePointID
		,source.StartDateTime
		,source.EndDateTime
		,source.SlotDuration
		,source.DayOfWeekID
		,source.SessionStatusID
		,source.Instructions
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.UserCreate
		,source.UserModify
		,source.SessionDuration
		,source.PriorPointerID
		,source.IsDirty
		,source.IsTemplate
		,source.ExternalKey
		,source.ServicePointSessionCode
		,source.ServicePointSessionDescription
		,source.SessionCancelDateTime
		,source.FrequencyID
		,source.FrequencyOffsetID
		,source.FrequencyOffset2ID
		,source.FrequencyOffset3ID
		,source.FrequencyOffset4ID
		,source.ServicePointSessionTemplateID
		,source.ServicePointSessionPurposeID
		,source.Comments
		,source.ActualProfessionalCarerID
		,source.ProfessionalCarerArrivalDateTime
		,source.ProfessionalCarerDepartureDateTime
		,source.ServicePointSessionChangeReasonID
		,source.DaysAhead
		,source.DaysBehind
		,source.ServicePointSessionStartStatusID
		,source.ServicePointSessionEndStatusID
		,source.ServicePointSessionStartStatusReasonID
		,source.ServicePointSessionEndStatusReasonID
		,source.StatusChangeRequestedByID
		,source.SessionTypeID
		,source.OverbookTol
		,source.ClosingDateTime
		,source.SessionLateStartReasonID
		,source.MaxPatients
		,source.IsGroupFlag
		,source.SessionStartDelayReasonID
		,source.DelayInSessionStart
		,source.WarningLevel
		,source.FacilityPurposeID
		,source.IsFinal
		,source.HasVariableSlots
		,source.SessionFrequencyID
		,source.SessionOffsetID
		,source.OnWeeks
		,source.OnDays
		,source.DaysInWeek1
		,source.DaysInWeek2
		,source.DaysInWeek3
		,source.DaysInWeek4
		,source.DaysInWeek5
		,source.DaysInWeekL
		,source.ExcludeHolidays
		,source.RescheduleFromDateTime
		,source.CopyOverbookSlotsFlag
		,source.CanRescheduleEarlierFlag
		,source.UseOverbookSlotsFlag
		,source.MaximumDifferenceRescheduleDays
		,source.RestructureFromDateTime
		,source.IsPartialBooking
		,source.PartialBookingLeadTime
		,source.PartialBookingLeadTimeMeasure
		,source.IsEBS
		,source.EBSServiceID
		,source.NewVisits
		,source.PublishAllSlotsFlag
		,source.LeadTimeFromClinicianFlag
		,source.NumberOfSessions
		,source.AnaesthetisProfessionalCarerID
		,source.ServicePointMaximumAppointments
		,source.ServicePointAppointmentsWarningLevel
		,source.ServicePointTotalBookedAppointments
		,source.ServicePointSessionTotalBookedAppointments
		,source.HealthOrganisationOwnerID
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		 target.ServicePointSessionID
		,target.SpecialtyID
		,target.SessionTransactionID
		,target.StaffTeamID
		,target.HealthOrganisationID
		,target.ProfessionalCarerID
		,target.ServicePointID
		,target.StartDateTime
		,target.EndDateTime
		,target.SlotDuration
		,target.DayOfWeekID
		,target.SessionStatusID
		,target.Instructions
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.SessionDuration
		,target.PriorPointerID
		,target.IsDirty
		,target.IsTemplate
		,target.ExternalKey
		,target.ServicePointSessionCode
		,target.ServicePointSessionDescription
		,target.SessionCancelDateTime
		,target.FrequencyID
		,target.FrequencyOffsetID
		,target.FrequencyOffset2ID
		,target.FrequencyOffset3ID
		,target.FrequencyOffset4ID
		,target.ServicePointSessionTemplateID
		,target.ServicePointSessionPurposeID
		,target.Comments
		,target.ActualProfessionalCarerID
		,target.ProfessionalCarerArrivalDateTime
		,target.ProfessionalCarerDepartureDateTime
		,target.ServicePointSessionChangeReasonID
		,target.DaysAhead
		,target.DaysBehind
		,target.ServicePointSessionStartStatusID
		,target.ServicePointSessionEndStatusID
		,target.ServicePointSessionStartStatusReasonID
		,target.ServicePointSessionEndStatusReasonID
		,target.StatusChangeRequestedByID
		,target.SessionTypeID
		,target.OverbookTol
		,target.ClosingDateTime
		,target.SessionLateStartReasonID
		,target.MaxPatients
		,target.IsGroupFlag
		,target.SessionStartDelayReasonID
		,target.DelayInSessionStart
		,target.WarningLevel
		,target.FacilityPurposeID
		,target.IsFinal
		,target.HasVariableSlots
		,target.SessionFrequencyID
		,target.SessionOffsetID
		,target.OnWeeks
		,target.OnDays
		,target.DaysInWeek1
		,target.DaysInWeek2
		,target.DaysInWeek3
		,target.DaysInWeek4
		,target.DaysInWeek5
		,target.DaysInWeekL
		,target.ExcludeHolidays
		,target.RescheduleFromDateTime
		,target.CopyOverbookSlotsFlag
		,target.CanRescheduleEarlierFlag
		,target.UseOverbookSlotsFlag
		,target.MaximumDifferenceRescheduleDays
		,target.RestructureFromDateTime
		,target.IsPartialBooking
		,target.PartialBookingLeadTime
		,target.PartialBookingLeadTimeMeasure
		,target.IsEBS
		,target.EBSServiceID
		,target.NewVisits
		,target.PublishAllSlotsFlag
		,target.LeadTimeFromClinicianFlag
		,target.NumberOfSessions
		,target.AnaesthetisProfessionalCarerID
		,target.ServicePointMaximumAppointments
		,target.ServicePointAppointmentsWarningLevel
		,target.ServicePointTotalBookedAppointments
		,target.ServicePointSessionTotalBookedAppointments
		,target.HealthOrganisationOwnerID
		,target.LorenzoCreated
		)

then
	update
	set
		 target.ServicePointSessionID = source.ServicePointSessionID
		,target.SpecialtyID = source.SpecialtyID
		,target.SessionTransactionID = source.SessionTransactionID
		,target.StaffTeamID = source.StaffTeamID
		,target.HealthOrganisationID = source.HealthOrganisationID
		,target.ProfessionalCarerID = source.ProfessionalCarerID
		,target.ServicePointID = source.ServicePointID
		,target.StartDateTime = source.StartDateTime
		,target.EndDateTime = source.EndDateTime
		,target.SlotDuration = source.SlotDuration
		,target.DayOfWeekID = source.DayOfWeekID
		,target.SessionStatusID = source.SessionStatusID
		,target.Instructions = source.Instructions
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.SessionDuration = source.SessionDuration
		,target.PriorPointerID = source.PriorPointerID
		,target.IsDirty = source.IsDirty
		,target.IsTemplate = source.IsTemplate
		,target.ExternalKey = source.ExternalKey
		,target.ServicePointSessionCode = source.ServicePointSessionCode
		,target.ServicePointSessionDescription = source.ServicePointSessionDescription
		,target.SessionCancelDateTime = source.SessionCancelDateTime
		,target.FrequencyID = source.FrequencyID
		,target.FrequencyOffsetID = source.FrequencyOffsetID
		,target.FrequencyOffset2ID = source.FrequencyOffset2ID
		,target.FrequencyOffset3ID = source.FrequencyOffset3ID
		,target.FrequencyOffset4ID = source.FrequencyOffset4ID
		,target.ServicePointSessionTemplateID = source.ServicePointSessionTemplateID
		,target.ServicePointSessionPurposeID = source.ServicePointSessionPurposeID
		,target.Comments = source.Comments
		,target.ActualProfessionalCarerID = source.ActualProfessionalCarerID
		,target.ProfessionalCarerArrivalDateTime = source.ProfessionalCarerArrivalDateTime
		,target.ProfessionalCarerDepartureDateTime = source.ProfessionalCarerDepartureDateTime
		,target.ServicePointSessionChangeReasonID = source.ServicePointSessionChangeReasonID
		,target.DaysAhead = source.DaysAhead
		,target.DaysBehind = source.DaysBehind
		,target.ServicePointSessionStartStatusID = source.ServicePointSessionStartStatusID
		,target.ServicePointSessionEndStatusID = source.ServicePointSessionEndStatusID
		,target.ServicePointSessionStartStatusReasonID = source.ServicePointSessionStartStatusReasonID
		,target.ServicePointSessionEndStatusReasonID = source.ServicePointSessionEndStatusReasonID
		,target.StatusChangeRequestedByID = source.StatusChangeRequestedByID
		,target.SessionTypeID = source.SessionTypeID
		,target.OverbookTol = source.OverbookTol
		,target.ClosingDateTime = source.ClosingDateTime
		,target.SessionLateStartReasonID = source.SessionLateStartReasonID
		,target.MaxPatients = source.MaxPatients
		,target.IsGroupFlag = source.IsGroupFlag
		,target.SessionStartDelayReasonID = source.SessionStartDelayReasonID
		,target.DelayInSessionStart = source.DelayInSessionStart
		,target.WarningLevel = source.WarningLevel
		,target.FacilityPurposeID = source.FacilityPurposeID
		,target.IsFinal = source.IsFinal
		,target.HasVariableSlots = source.HasVariableSlots
		,target.SessionFrequencyID = source.SessionFrequencyID
		,target.SessionOffsetID = source.SessionOffsetID
		,target.OnWeeks = source.OnWeeks
		,target.OnDays = source.OnDays
		,target.DaysInWeek1 = source.DaysInWeek1
		,target.DaysInWeek2 = source.DaysInWeek2
		,target.DaysInWeek3 = source.DaysInWeek3
		,target.DaysInWeek4 = source.DaysInWeek4
		,target.DaysInWeek5 = source.DaysInWeek5
		,target.DaysInWeekL = source.DaysInWeekL
		,target.ExcludeHolidays = source.ExcludeHolidays
		,target.RescheduleFromDateTime = source.RescheduleFromDateTime
		,target.CopyOverbookSlotsFlag = source.CopyOverbookSlotsFlag
		,target.CanRescheduleEarlierFlag = source.CanRescheduleEarlierFlag
		,target.UseOverbookSlotsFlag = source.UseOverbookSlotsFlag
		,target.MaximumDifferenceRescheduleDays = source.MaximumDifferenceRescheduleDays
		,target.RestructureFromDateTime = source.RestructureFromDateTime
		,target.IsPartialBooking = source.IsPartialBooking
		,target.PartialBookingLeadTime = source.PartialBookingLeadTime
		,target.PartialBookingLeadTimeMeasure = source.PartialBookingLeadTimeMeasure
		,target.IsEBS = source.IsEBS
		,target.EBSServiceID = source.EBSServiceID
		,target.NewVisits = source.NewVisits
		,target.PublishAllSlotsFlag = source.PublishAllSlotsFlag
		,target.LeadTimeFromClinicianFlag = source.LeadTimeFromClinicianFlag
		,target.NumberOfSessions = source.NumberOfSessions
		,target.AnaesthetisProfessionalCarerID = source.AnaesthetisProfessionalCarerID
		,target.ServicePointMaximumAppointments = source.ServicePointMaximumAppointments
		,target.ServicePointAppointmentsWarningLevel = source.ServicePointAppointmentsWarningLevel
		,target.ServicePointTotalBookedAppointments = source.ServicePointTotalBookedAppointments
		,target.ServicePointSessionTotalBookedAppointments = source.ServicePointSessionTotalBookedAppointments
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime
