﻿CREATE PROCEDURE [Lorenzo].[LoadServicePointSlot]

AS

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(ServicePointSlot.LorenzoCreated)
		from
			Lorenzo.ServicePointSlot
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 ServicePointSlotID
			,SessionTransactionID
			,ServicePointSessionID
			,StartDateTime
			,EndDateTime
			,PreviousStartDateTime
			,PreviousEndDateTime
			,NoOfBookings
			,TimeSlotStatusID
			,PreviousTimeslotStatusID
			,ClinicInstructions
			,CreatedDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,Duration
			,USRN
			,PriorPointerID
			,IsTemplate
			,IsEBSPublished
			,ServicePointID
			,ExternalKey
			,ServicePointTimeslotTemplateID
			,CancellationDateTime
			,Comments
			,SlotChangeReasonID
			,MaximumBookings
			,SDSUserID
			,StatusChangeRequestedByID
			,DiaryChanged
			,HealthOrganisationOwnerID
			,LorenzoCreated
			)
into
	#TLoadPASServicePointSlot
from
	(
	select
		ServicePointSlotID
		,SessionTransactionID = cast(SessionTransactionID as int)
		,ServicePointSessionID = cast(ServicePointSessionID as int)
		,StartDateTime = cast(StartDateTime as datetime)
		,EndDateTime = cast(EndDateTime as datetime)
		,PreviousStartDateTime = cast(PreviousStartDateTime as datetime)
		,PreviousEndDateTime = cast(PreviousEndDateTime as datetime)
		,NoOfBookings = cast(NoOfBookings as int)
		,TimeSlotStatusID = cast(TimeSlotStatusID as int)
		,PreviousTimeslotStatusID = cast(PreviousTimeslotStatusID as int)
		,ClinicInstructions = cast(nullif(ClinicInstructions, '') as varchar(255))
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(50))
		,UserModify = cast(nullif(UserModify, '') as varchar(50))
		,Duration = cast(Duration as int)
		,USRN = cast(nullif(USRN, '') as varchar(255))
		,PriorPointerID = cast(PriorPointerID as int)
		,IsTemplate = cast(nullif(IsTemplate, '') as char(1))
		,IsEBSPublished = cast(nullif(IsEBSPublished, '') as char(1))
		,ServicePointID = cast(ServicePointID as int)
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(50))
		,ServicePointTimeslotTemplateID = cast(ServicePointTimeslotTemplateID as int)
		,CancellationDateTime = cast(CancellationDateTime as datetime)
		,Comments = cast(nullif(Comments, '') as varchar(255))
		,SlotChangeReasonID = cast(SlotChangeReasonID as int)
		,MaximumBookings = cast(MaximumBookings as int)
		,SDSUserID = cast(nullif(SDSUserID, '') as varchar(50))
		,StatusChangeRequestedByID = cast(StatusChangeRequestedByID as int)
		,DiaryChanged = cast(nullif(DiaryChanged, '') as varchar(255))
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as int)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			ServicePointSlotID = [SPSLT_REFNO]
		  ,SessionTransactionID = [STRAN_REFNO]
		  ,ServicePointSessionID = [SPSSN_REFNO]
		  ,StartDateTime = [START_DTTM]
		  ,EndDateTime = [END_DTTM]
		  ,PreviousStartDateTime = [PREV_START_DTTM] 
		  ,PreviousEndDateTime = [PREV_END_DTTM]
		  ,NoOfBookings = [NOOF_BOOKINGS]
		  ,TimeSlotStatusID = [TSTAT_REFNO]
		  ,PreviousTimeslotStatusID = [PREV_TSTAT_REFNO]
		  ,ClinicInstructions = [INSTRUCTIONS]
		  ,CreatedDateTime = [CREATE_DTTM]
		  ,ModifiedDateTime = [MODIF_DTTM]
		  ,UserCreate = [USER_CREATE]
		  ,UserModify = [USER_MODIF]
		  ,Duration = [DURATION]
		  ,USRN = [USRN] 
		  ,PriorPointerID = [PRIOR_POINTER]
		  ,IsTemplate = [TEMPLATE_FLAG]
		  ,IsEBSPublished = [EBS_PUB_FLAG]
		  ,ServicePointID = [SPONT_REFNO] 
		  ,ExternalKey = [EXTERNAL_KEY]
		  ,ServicePointTimeslotTemplateID = [TMPLT_REFNO]
		  ,CancellationDateTime = [CANCEL_DTTM]
		  ,Comments = [COMMENTS]
		  ,SlotChangeReasonID = [SLCHR_REFNO]
		  ,MaximumBookings = [MAX_BOOKINGS]
		  ,SDSUserID = [SDS_USER_ID]
		  ,StatusChangeRequestedByID = [STCRB_REFNO]
		  ,DiaryChanged = [DIARY_CHANGED]
		  ,HealthOrganisationOwnerID = [OWNER_HEORG_REFNO]
		  ,LorenzoCreated = [Created]

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.ServicePointSlot
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASServicePointSlot on #TLoadPASServicePointSlot
	(
	ServicePointSlotID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.ServicePointSlot target
using
	(
	select
		*
	from
		#TLoadPASServicePointSlot
	
	) source
	on	source.ServicePointSlotID = target.ServicePointSlotID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		 ServicePointSlotID
		,SessionTransactionID
		,ServicePointSessionID
		,StartDateTime
		,EndDateTime
		,PreviousStartDateTime
		,PreviousEndDateTime
		,NoOfBookings
		,TimeSlotStatusID
		,PreviousTimeslotStatusID
		,ClinicInstructions
		,CreatedDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,Duration
		,USRN
		,PriorPointerID
		,IsTemplate
		,IsEBSPublished
		,ServicePointID
		,ExternalKey
		,ServicePointTimeslotTemplateID
		,CancellationDateTime
		,Comments
		,SlotChangeReasonID
		,MaximumBookings
		,SDSUserID
		,StatusChangeRequestedByID
		,DiaryChanged
		,HealthOrganisationOwnerID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		  source.ServicePointSlotID
		,source.SessionTransactionID
		,source.ServicePointSessionID
		,source.StartDateTime
		,source.EndDateTime
		,source.PreviousStartDateTime
		,source.PreviousEndDateTime
		,source.NoOfBookings
		,source.TimeSlotStatusID
		,source.PreviousTimeslotStatusID
		,source.ClinicInstructions
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.UserCreate
		,source.UserModify
		,source.Duration
		,source.USRN
		,source.PriorPointerID
		,source.IsTemplate
		,source.IsEBSPublished
		,source.ServicePointID
		,source.ExternalKey
		,source.ServicePointTimeslotTemplateID
		,source.CancellationDateTime
		,source.Comments
		,source.SlotChangeReasonID
		,source.MaximumBookings
		,source.SDSUserID
		,source.StatusChangeRequestedByID
		,source.DiaryChanged
		,source.HealthOrganisationOwnerID
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		 target.ServicePointSlotID
		,target.SessionTransactionID
		,target.ServicePointSessionID
		,target.StartDateTime
		,target.EndDateTime
		,target.PreviousStartDateTime
		,target.PreviousEndDateTime
		,target.NoOfBookings
		,target.TimeSlotStatusID
		,target.PreviousTimeslotStatusID
		,target.ClinicInstructions
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.Duration
		,target.USRN
		,target.PriorPointerID
		,target.IsTemplate
		,target.IsEBSPublished
		,target.ServicePointID
		,target.ExternalKey
		,target.ServicePointTimeslotTemplateID
		,target.CancellationDateTime
		,target.Comments
		,target.SlotChangeReasonID
		,target.MaximumBookings
		,target.SDSUserID
		,target.StatusChangeRequestedByID
		,target.DiaryChanged
		,target.HealthOrganisationOwnerID
		,target.LorenzoCreated
		)

then
	update
	set
		  target.ServicePointSlotID = source.ServicePointSlotID
		,target.SessionTransactionID = source.SessionTransactionID
		,target.ServicePointSessionID = source.ServicePointSessionID
		,target.StartDateTime = source.StartDateTime
		,target.EndDateTime = source.EndDateTime
		,target.PreviousStartDateTime = source.PreviousStartDateTime
		,target.PreviousEndDateTime = source.PreviousEndDateTime
		,target.NoOfBookings = source.NoOfBookings
		,target.TimeSlotStatusID = source.TimeSlotStatusID
		,target.PreviousTimeslotStatusID = source.PreviousTimeslotStatusID
		,target.ClinicInstructions = source.ClinicInstructions
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.Duration = source.Duration
		,target.USRN = source.USRN
		,target.PriorPointerID = source.PriorPointerID
		,target.IsTemplate = source.IsTemplate
		,target.IsEBSPublished = source.IsEBSPublished
		,target.ServicePointID = source.ServicePointID
		,target.ExternalKey = source.ExternalKey
		,target.ServicePointTimeslotTemplateID = source.ServicePointTimeslotTemplateID
		,target.CancellationDateTime = source.CancellationDateTime
		,target.Comments = source.Comments
		,target.SlotChangeReasonID = source.SlotChangeReasonID
		,target.MaximumBookings = source.MaximumBookings
		,target.SDSUserID = source.SDSUserID
		,target.StatusChangeRequestedByID = source.StatusChangeRequestedByID
		,target.DiaryChanged = source.DiaryChanged
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime