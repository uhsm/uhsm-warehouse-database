﻿CREATE procedure [Lorenzo].[LoadServicePointStay] as 


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(ServicePointStay.LorenzoCreated)
		from
			Lorenzo.ServicePointStay
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			PatientID
			,PatientSystemIdentifier
			,NHSNumber
			,ServicePointID
			,ServicePointCode
			,ServicePoint
			,ProviderSpellID
			,StartDateTime
			,EndDateTime
			,CreatedDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,ProvisionalOnlyIndicatorFlag
			,ProfessionalCarerID
			,ProfessionalCarerMainCode
			,BedCategoryID
			,BedCategoryMainCode
			,BedCategory
			,RequestedBedCategoryID
			,RequestedBedCategoryMainCode
			,RequestedBedCategory
			,IsChaplainVisitAllowed
			,IsVisitorAllowed
			,HealthOrganisationOwnerID
			,HealthOrganisationOwnerMainCode
			,WardStartDateTime
			,LorenzoCreated
			)
into
	#TLoadPASServicePointStay
from
	(
	select
		ServicePointStayID
		,PatientID = cast(PatientID as int)
		,PatientSystemIdentifier = cast(nullif(PatientSystemIdentifier, '') as varchar(20))
		,NHSNumber = cast(nullif(NHSNumber, '') as varchar(20))
		,ServicePointID = cast(ServicePointID as int)
		,ServicePointCode = cast(nullif(ServicePointCode, '') as varchar(25))
		,ServicePoint = cast(nullif(ServicePoint, '') as varchar(80))
		,ProviderSpellID = cast(ProviderSpellID as int)
		,StartDateTime = cast(StartDateTime as datetime)
		,EndDateTime = cast(EndDateTime as datetime)
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(30))
		,UserModify = cast(nullif(UserModify, '') as varchar(30))
		,ProvisionalOnlyIndicatorFlag = cast(nullif(ProvisionalOnlyIndicatorFlag, '') as char(1))
		,ProfessionalCarerID = cast(ProfessionalCarerID as int)
		,ProfessionalCarerMainCode = cast(nullif(ProfessionalCarerMainCode, '') as varchar(25))
		,BedCategoryID = cast(BedCategoryID as int)
		,BedCategoryMainCode = cast(nullif(BedCategoryMainCode, '') as varchar(25))
		,BedCategory = cast(nullif(BedCategory, '') as varchar(80))
		,RequestedBedCategoryID = cast(RequestedBedCategoryID as int)
		,RequestedBedCategoryMainCode = cast(nullif(RequestedBedCategoryMainCode, '') as varchar(25))
		,RequestedBedCategory = cast(nullif(RequestedBedCategory, '') as varchar(80))
		,IsChaplainVisitAllowed = cast(nullif(IsChaplainVisitAllowed, '') as char(1))
		,IsVisitorAllowed = cast(nullif(IsVisitorAllowed, '') as char(1))
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as int)
		,HealthOrganisationOwnerMainCode = cast(nullif(HealthOrganisationOwnerMainCode, '') as varchar(25))
		,WardStartDateTime = cast(WardStartDateTime as datetime)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			ServicePointStayID = SSTAY_REFNO
			,PatientID = PATNT_REFNO
			,PatientSystemIdentifier = PATNT_REFNO_PASID
			,NHSNumber = PATNT_REFNO_NHS_IDENTIFIER
			,ServicePointID = SPONT_REFNO
			,ServicePointCode = SPONT_REFNO_CODE
			,ServicePoint = SPONT_REFNO_NAME
			,ProviderSpellID = PRVSP_REFNO
			,StartDateTime = START_DTTM
			,EndDateTime = END_DTTM
			,CreatedDateTime = CREATE_DTTM
			,ModifiedDateTime = MODIF_DTTM
			,UserCreate = USER_CREATE
			,UserModify = USER_MODIF
			,ProvisionalOnlyIndicatorFlag = PRVSN_FLAG
			,ProfessionalCarerID = PROCA_REFNO
			,ProfessionalCarerMainCode = PROCA_REFNO_MAIN_IDENT
			,BedCategoryID = BDCAT_REFNO
			,BedCategoryMainCode = BDCAT_REFNO_MAIN_CODE
			,BedCategory = BDCAT_REFNO_DESCRIPTION
			,RequestedBedCategoryID = REQTD_BDCAT_REFNO
			,RequestedBedCategoryMainCode = REQTD_BDCAT_REFNO_MAIN_CODE
			,RequestedBedCategory = REQTD_BDCAT_REFNO_DESCRIPTION
			,IsChaplainVisitAllowed = CHAPLAIN_VISIT_FLAG
			,IsVisitorAllowed = VISITORS_ALLOWED_FLAG
			,HealthOrganisationOwnerID = OWNER_HEORG_REFNO
			,HealthOrganisationOwnerMainCode = OWNER_HEORG_REFNO_MAIN_IDENT
			,WardStartDateTime = WARD_START_DTTM
			,LorenzoCreated = Created

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.ServicePointStay
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASServicePointStay on #TLoadPASServicePointStay
	(
	ServicePointStayID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.ServicePointStay target
using
	(
	select
		*
	from
		#TLoadPASServicePointStay
	
	) source
	on	source.ServicePointStayID = target.ServicePointStayID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		ServicePointStayID
		,PatientID
		,PatientSystemIdentifier
		,NHSNumber
		,ServicePointID
		,ServicePointCode
		,ServicePoint
		,ProviderSpellID
		,StartDateTime
		,EndDateTime
		,CreatedDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,ProvisionalOnlyIndicatorFlag
		,ProfessionalCarerID
		,ProfessionalCarerMainCode
		,BedCategoryID
		,BedCategoryMainCode
		,BedCategory
		,RequestedBedCategoryID
		,RequestedBedCategoryMainCode
		,RequestedBedCategory
		,IsChaplainVisitAllowed
		,IsVisitorAllowed
		,HealthOrganisationOwnerID
		,HealthOrganisationOwnerMainCode
		,WardStartDateTime
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		source.ServicePointStayID
		,source.PatientID
		,source.PatientSystemIdentifier
		,source.NHSNumber
		,source.ServicePointID
		,source.ServicePointCode
		,source.ServicePoint
		,source.ProviderSpellID
		,source.StartDateTime
		,source.EndDateTime
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.UserCreate
		,source.UserModify
		,source.ProvisionalOnlyIndicatorFlag
		,source.ProfessionalCarerID
		,source.ProfessionalCarerMainCode
		,source.BedCategoryID
		,source.BedCategoryMainCode
		,source.BedCategory
		,source.RequestedBedCategoryID
		,source.RequestedBedCategoryMainCode
		,source.RequestedBedCategory
		,source.IsChaplainVisitAllowed
		,source.IsVisitorAllowed
		,source.HealthOrganisationOwnerID
		,source.HealthOrganisationOwnerMainCode
		,source.WardStartDateTime
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		target.PatientID
		,target.PatientSystemIdentifier
		,target.NHSNumber
		,target.ServicePointID
		,target.ServicePointCode
		,target.ServicePoint
		,target.ProviderSpellID
		,target.StartDateTime
		,target.EndDateTime
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.ProvisionalOnlyIndicatorFlag
		,target.ProfessionalCarerID
		,target.ProfessionalCarerMainCode
		,target.BedCategoryID
		,target.BedCategoryMainCode
		,target.BedCategory
		,target.RequestedBedCategoryID
		,target.RequestedBedCategoryMainCode
		,target.RequestedBedCategory
		,target.IsChaplainVisitAllowed
		,target.IsVisitorAllowed
		,target.HealthOrganisationOwnerID
		,target.HealthOrganisationOwnerMainCode
		,target.WardStartDateTime
		,target.LorenzoCreated
		)

then
	update
	set
		target.PatientID = source.PatientID
		,target.PatientSystemIdentifier = source.PatientSystemIdentifier
		,target.NHSNumber = source.NHSNumber
		,target.ServicePointID = source.ServicePointID
		,target.ServicePointCode = source.ServicePointCode
		,target.ServicePoint = source.ServicePoint
		,target.ProviderSpellID = source.ProviderSpellID
		,target.StartDateTime = source.StartDateTime
		,target.EndDateTime = source.EndDateTime
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.ProvisionalOnlyIndicatorFlag = source.ProvisionalOnlyIndicatorFlag
		,target.ProfessionalCarerID = source.ProfessionalCarerID
		,target.ProfessionalCarerMainCode = source.ProfessionalCarerMainCode
		,target.BedCategoryID = source.BedCategoryID
		,target.BedCategoryMainCode = source.BedCategoryMainCode
		,target.BedCategory = source.BedCategory
		,target.RequestedBedCategoryID = source.RequestedBedCategoryID
		,target.RequestedBedCategoryMainCode = source.RequestedBedCategoryMainCode
		,target.RequestedBedCategory = source.RequestedBedCategory
		,target.IsChaplainVisitAllowed = source.IsChaplainVisitAllowed
		,target.IsVisitorAllowed = source.IsVisitorAllowed
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.HealthOrganisationOwnerMainCode = source.HealthOrganisationOwnerMainCode
		,target.WardStartDateTime = source.WardStartDateTime
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime