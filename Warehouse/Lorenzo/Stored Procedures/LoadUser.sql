﻿CREATE PROCEDURE [Lorenzo].[LoadUser]

AS




declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max([User].LorenzoCreated)
		from
			Lorenzo.[User]
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 UserID
			,ProfessionalCarerID
			,UserCode
			,Username
			,Department
			,Email
			,HasEvents
			,UserCreate
			,UserModify
			,CreatedDateTime
			,ModifiedDateTime
			,ExternalKey
			,SessionTransactionID
			,AlterDateTime
			,LastLogonDateTime
			,IsLoggedOn
			,IsLocked
			,LockedComments
			,ChangePasswordFlag
			,AccessLevelsID
			,IsNHSUser
			,HealthOrganisationOwnerID
			,LorenzoCreated
			)
into
	#TLoadPASUser
from
	(
	select
		UserID
		,ProfessionalCarerID = cast(ProfessionalCarerID as int)
		,UserCode = cast(nullif(UserCode, '') as varchar(20))
		,Username = cast(nullif(Username, '') as varchar(35))
		,Department = cast(nullif(Department, '') as varchar(80))
		,Email = cast(nullif(Email, '') as varchar(80))
		,HasEvents = cast(nullif(HasEvents, '') as char(1))
		,UserCreate = cast(nullif(UserCreate, '') as varchar(30))
		,UserModify = cast(nullif(UserModify, '') as varchar(30))
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(20))
		,SessionTransactionID = cast(SessionTransactionID as int)
		,AlterDateTime = cast(AlterDateTime as datetime)
		,LastLogonDateTime = cast(LastLogonDateTime as datetime)
		,IsLoggedOn = cast(IsLoggedOn as int)
		,IsLocked = cast(nullif(IsLocked, '') as char(1))
		,LockedComments = cast(nullif(LockedComments, '') as varchar(255))
		,ChangePasswordFlag = cast(nullif(ChangePasswordFlag, '') as char(1))
		,AccessLevelsID = cast(AccessLevelsID as int)
		,IsNHSUser = cast(nullif(IsNHSUser, '') as char(1))
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as int)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			UserID =  [USERS_REFNO]
		  ,ProfessionalCarerID = [PROCA_REFNO]
		  ,UserCode = [CODE] 
		  ,Username = [USER_NAME] 
		  ,Department = [DEPARTMENT] 
		  ,Email = [EMAIL]
		  ,HasEvents = [EVENTS_EXIST_FLAG]
		  ,UserCreate = [USER_CREATE]
		  ,UserModify = [USER_MODIF]
		  ,CreatedDateTime = [CREATE_DTTM] 
		  ,ModifiedDateTime = [MODIF_DTTM]
		  ,ExternalKey = [EXTERNAL_KEY]
		  ,SessionTransactionID = [STRAN_REFNO]
		  ,AlterDateTime = [ALTER_DTTM]
		  ,LastLogonDateTime = [LAST_LOGON_DTTM]
		  ,IsLoggedOn = [LOGGED_ON]
		  ,IsLocked = [LOCKED_FLAG]
		  ,LockedComments = [LOCKED_COMMENTS]
		  ,ChangePasswordFlag = [CHANGE_PASSWORD_FLAG]
		  ,AccessLevelsID = [ACLEV_REFNO]
		  ,IsNHSUser = [NHS_USER_FLAG]
		  ,HealthOrganisationOwnerID = [OWNER_HEORG_REFNO]
		  ,LorenzoCreated = [Created] 

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.[User]
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASUser on #TLoadPASUser
	(
	UserID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.[User] target
using
	(
	select
		*
	from
		#TLoadPASUser
	
	) source
	on	source.UserID = target.UserID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		 UserID
		,ProfessionalCarerID
		,UserCode
		,Username
		,Department
		,Email
		,HasEvents
		,UserCreate
		,UserModify
		,CreatedDateTime
		,ModifiedDateTime
		,ExternalKey
		,SessionTransactionID
		,AlterDateTime
		,LastLogonDateTime
		,IsLoggedOn
		,IsLocked
		,LockedComments
		,ChangePasswordFlag
		,AccessLevelsID
		,IsNHSUser
		,HealthOrganisationOwnerID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		 source.UserID
		,source.ProfessionalCarerID
		,source.UserCode
		,source.Username
		,source.Department
		,source.Email
		,source.HasEvents
		,source.UserCreate
		,source.UserModify
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.ExternalKey
		,source.SessionTransactionID
		,source.AlterDateTime
		,source.LastLogonDateTime
		,source.IsLoggedOn
		,source.IsLocked
		,source.LockedComments
		,source.ChangePasswordFlag
		,source.AccessLevelsID
		,source.IsNHSUser
		,source.HealthOrganisationOwnerID
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		 target.UserID
		,target.ProfessionalCarerID
		,target.UserCode
		,target.Username
		,target.Department
		,target.Email
		,target.HasEvents
		,target.UserCreate
		,target.UserModify
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.ExternalKey
		,target.SessionTransactionID
		,target.AlterDateTime
		,target.LastLogonDateTime
		,target.IsLoggedOn
		,target.IsLocked
		,target.LockedComments
		,target.ChangePasswordFlag
		,target.AccessLevelsID
		,target.IsNHSUser
		,target.HealthOrganisationOwnerID
		,target.LorenzoCreated
		)

then
	update
	set
		 target.UserID = source.UserID
		,target.ProfessionalCarerID = source.ProfessionalCarerID
		,target.UserCode = source.UserCode
		,target.Username = source.Username
		,target.Department = source.Department
		,target.Email = source.Email
		,target.HasEvents = source.HasEvents
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.ExternalKey = source.ExternalKey
		,target.SessionTransactionID = source.SessionTransactionID
		,target.AlterDateTime = source.AlterDateTime
		,target.LastLogonDateTime = source.LastLogonDateTime
		,target.IsLoggedOn = source.IsLoggedOn
		,target.IsLocked = source.IsLocked
		,target.LockedComments = source.LockedComments
		,target.ChangePasswordFlag = source.ChangePasswordFlag
		,target.AccessLevelsID = source.AccessLevelsID
		,target.IsNHSUser = source.IsNHSUser
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime
