﻿CREATE PROCEDURE [Lorenzo].[LoadWaitingList]

AS


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(WaitingList.LorenzoCreated)
		from
			Lorenzo.WaitingList
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 WaitingListID
			,WaitingListRuleID
			,AdmissionDecisionID
			,ReferralID
			,SpecialtyID
			,ProfessionalCarerID
			,HealthOrganisationID
			,PatientID
			,AdmissionMethodID
			,AdministrativeCategoryID
			,ManagementIntentionID
			,PriorityID
			,UrgencyID
			,WaitingListDescription
			,ShortNotice
			,OriginalDecisionToAdmitDateTime
			,WaitingListDateTime
			,RemovalDateTime
			,RemovalID
			,PriorWaitingListID
			,GuaranteedAdmissionDateTime
			,TransportID
			,IsHotelStay
			,AdmissionDateTime
			,ServiceTypeID
			,ServicePointID
			,DayOfWeekID
			,TheatreTime
			,WhoCanOperateID
			,ClinicalPriorityID
			,NoFundsFlag
			,ContractID
			,PurchaserID
			,ProviderID
			,LocalWaitingListRuleID
			,CodingStatusID
			,TransferServicePointID
			,LengthOfStay
			,transferLengthOfStay
			,AnaestheticTypeID
			,WaitingStartDateTime
			,WaitingSuspensionStartDateTime
			,TotalSuspensionTime
			,IsReadyForAdmission
			,ActiveIndicator
			,ThVisitID
			,Deposit
			,WaitingListNumber
			,CreatedDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,SessionTransactionID
			,PriorPointerID
			,ExternalKey
			,PlannedProcedure
			,ServicePointSessionID
			,DayCareEpisodeID
			,AccessLevelsID
			,StaffTeamID
			,ContactPurposeID
			,RemovalHealthOrganisationID
			,PatialBookingNumber
			,VisitID
			,BookingTypeID
			,PartialBookingStageID
			,AcknowledgementDateTime
			,AcknowledgementStatusID
			,BookingInvitationDateTime
			,BookingInvitationStatusID
			,BookingReminderDateTime
			,BookingReminderStatusID
			,CancellationDateTime
			,CancellationStatusID
			,NumberOfDaysSuspended
			,IsPatientChoice
			,NoOfOccurences
			,TreatInitFlag
			,DeletedFlag
			,RemovedByUserID
			,RemovedByDateTime
			,RemovedByID
			,RTTStatusID
			,DiagnosticRequestTypeServiceID
			,ISDiagnosticWait
			,WaitingListStaffTeamID
			,IsFirstDefinitiveTreatment
			,HealthOrganisationOwnerID
			,LorenzoCreated
			)
into
	#TLoadPASWaitingList
from
	(
	select
		WaitingListID
		,WaitingListRuleID = cast(WaitingListRuleID as int)
		,AdmissionDecisionID = cast(AdmissionDecisionID as int)
		,ReferralID = cast(ReferralID as int)
		,SpecialtyID = cast(SpecialtyID as int)
		,ProfessionalCarerID = cast(ProfessionalCarerID as int)
		,HealthOrganisationID = cast(HealthOrganisationID as int)
		,PatientID = cast(PatientID as int)
		,AdmissionMethodID = cast(AdmissionMethodID as int)
		,AdministrativeCategoryID = cast(AdministrativeCategoryID as int)
		,ManagementIntentionID = cast(ManagementIntentionID as int)
		,PriorityID = cast(PriorityID as int)
		,UrgencyID = cast(UrgencyID as int)
		,WaitingListDescription = cast(nullif(WaitingListDescription, '') as varchar(255))
		,ShortNotice = cast(nullif(ShortNotice, '') as char(1))
		,OriginalDecisionToAdmitDateTime = cast(OriginalDecisionToAdmitDateTime as datetime)
		,WaitingListDateTime = cast(WaitingListDateTime as datetime)
		,RemovalDateTime = cast(RemovalDateTime as datetime)
		,RemovalID = cast(RemovalID as int)
		,PriorWaitingListID = cast(PriorWaitingListID as int)
		,GuaranteedAdmissionDateTime = cast(GuaranteedAdmissionDateTime as datetime)
		,TransportID = cast(TransportID as int)
		,IsHotelStay = cast(nullif(IsHotelStay, '') as char(1))
		,AdmissionDateTime = cast(AdmissionDateTime as datetime)
		,ServiceTypeID = cast(ServiceTypeID as int)
		,ServicePointID = cast(ServicePointID as int)
		,DayOfWeekID = cast(DayOfWeekID as int)
		,TheatreTime = cast(TheatreTime as int)
		,WhoCanOperateID = cast(WhoCanOperateID as int)
		,ClinicalPriorityID = cast(ClinicalPriorityID as int)
		,NoFundsFlag = cast(nullif(NoFundsFlag, '') as char(1))
		,ContractID = cast(ContractID as int)
		,PurchaserID = cast(PurchaserID as int)
		,ProviderID = cast(ProviderID as int)
		,LocalWaitingListRuleID = cast(LocalWaitingListRuleID as int)
		,CodingStatusID = cast(CodingStatusID as int)
		,TransferServicePointID = cast(TransferServicePointID as int)
		,LengthOfStay = cast(LengthOfStay as int)
		,transferLengthOfStay = cast(transferLengthOfStay as int)
		,AnaestheticTypeID = cast(AnaestheticTypeID as int)
		,WaitingStartDateTime = cast(WaitingStartDateTime as datetime)
		,WaitingSuspensionStartDateTime = cast(WaitingSuspensionStartDateTime as datetime)
		,TotalSuspensionTime = cast(TotalSuspensionTime as int)
		,IsReadyForAdmission = cast(nullif(IsReadyForAdmission, '') as char(1))
		,ActiveIndicator = cast(ActiveIndicator as int)
		,ThVisitID = cast(ThVisitID as int)
		,Deposit = cast(Deposit as int)
		,WaitingListNumber = cast(nullif(WaitingListNumber, '') as varchar(50))
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(50))
		,UserModify = cast(nullif(UserModify, '') as varchar(50))
		,SessionTransactionID = cast(SessionTransactionID as int)
		,PriorPointerID = cast(PriorPointerID as int)
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(50))
		,PlannedProcedure = cast(nullif(PlannedProcedure, '') as varchar(255))
		,ServicePointSessionID = cast(ServicePointSessionID as int)
		,DayCareEpisodeID = cast(DayCareEpisodeID as int)
		,AccessLevelsID = cast(AccessLevelsID as int)
		,StaffTeamID = cast(StaffTeamID as int)
		,ContactPurposeID = cast(ContactPurposeID as int)
		,RemovalHealthOrganisationID = cast(RemovalHealthOrganisationID as int)
		,PatialBookingNumber = cast(nullif(PatialBookingNumber, '') as varchar(50))
		,VisitID = cast(VisitID as int)
		,BookingTypeID = cast(BookingTypeID as int)
		,PartialBookingStageID = cast(PartialBookingStageID as int)
		,AcknowledgementDateTime = cast(AcknowledgementDateTime as datetime)
		,AcknowledgementStatusID = cast(AcknowledgementStatusID as int)
		,BookingInvitationDateTime = cast(BookingInvitationDateTime as datetime)
		,BookingInvitationStatusID = cast(BookingInvitationStatusID as int)
		,BookingReminderDateTime = cast(BookingReminderDateTime as datetime)
		,BookingReminderStatusID = cast(BookingReminderStatusID as int)
		,CancellationDateTime = cast(CancellationDateTime as datetime)
		,CancellationStatusID = cast(CancellationStatusID as int)
		,NumberOfDaysSuspended = cast(NumberOfDaysSuspended as int)
		,IsPatientChoice = cast(nullif(IsPatientChoice, '') as char(1))
		,NoOfOccurences = cast(NoOfOccurences as int)
		,TreatInitFlag = cast(nullif(TreatInitFlag, '') as char(1))
		,DeletedFlag = cast(nullif(DeletedFlag, '') as char(1))
		,RemovedByUserID = cast(nullif(RemovedByUserID, '') as varchar(50))
		,RemovedByDateTime = cast(RemovedByDateTime as datetime)
		,RemovedByID = cast(RemovedByID as int)
		,RTTStatusID = cast(RTTStatusID as int)
		,DiagnosticRequestTypeServiceID = cast(DiagnosticRequestTypeServiceID as int)
		,ISDiagnosticWait = cast(nullif(ISDiagnosticWait, '') as char(1))
		,WaitingListStaffTeamID = cast(WaitingListStaffTeamID as int)
		,IsFirstDefinitiveTreatment = cast(nullif(IsFirstDefinitiveTreatment, '') as char(1))
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as int)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			 WaitingListID = WLIST_REFNO
			,WaitingListRuleID = WLRUL_REFNO
			,AdmissionDecisionID = ADMDC_REFNO
			,ReferralID = REFRL_REFNO
			,SpecialtyID = SPECT_REFNO
			,ProfessionalCarerID = PROCA_REFNO
			,HealthOrganisationID = HEORG_REFNO
			,PatientID = PATNT_REFNO
			,AdmissionMethodID = ADMET_REFNO
			,AdministrativeCategoryID = ADCAT_REFNO
			,ManagementIntentionID = INMGT_REFNO
			,PriorityID = PRITY_REFNO
			,UrgencyID = URGNC_REFNO
			,WaitingListDescription = [DESCRIPTION]
			,ShortNotice = SHORT_NOTICE_FLAG
			,OriginalDecisionToAdmitDateTime = ORDTA_DTTM
			,WaitingListDateTime = WLIST_DTTM
			,RemovalDateTime = REMVL_DTTM
			,RemovalID = REMVL_REFNO
			,PriorWaitingListID = PRIOR_REFNO
			,GuaranteedAdmissionDateTime = GUADM_DTTM
			,TransportID = TRANS_REFNO
			,IsHotelStay = HOTEL_STAY_FLAG
			,AdmissionDateTime = ADMIT_DTTM
			,ServiceTypeID = SVTYP_REFNO
			,ServicePointID = SPONT_REFNO
			,DayOfWeekID = DAYOW_REFNO
			,TheatreTime = THEAT_TIME
			,WhoCanOperateID = WHOCO_REFNO
			,ClinicalPriorityID = CLPTY_REFNO
			,NoFundsFlag = NO_FUNDS_FLAG
			,ContractID = CONTR_REFNO
			,PurchaserID = PURCH_REFNO
			,ProviderID = PROVD_REFNO
			,LocalWaitingListRuleID = LOCAL_WLRUL_REFNO
			,CodingStatusID = CSTAT_REFNO
			,TransferServicePointID = XFER_SPONT_REFNO
			,LengthOfStay = LENGTH_STAY
			,transferLengthOfStay = XFER_LENGTH_STAY
			,AnaestheticTypeID = ANTYP_REFNO
			,WaitingStartDateTime = WAITING_START_DTTM
			,WaitingSuspensionStartDateTime = WAITING_SUSP_START_DTTM
			,TotalSuspensionTime = WAITING_SUSP_TOTAL
			,IsReadyForAdmission = ADMIT_READY_FLAG
			,ActiveIndicator = ACTIVE_INDICATOR
			,ThVisitID = THVISIT_YNUNK_REFNO
			,Deposit = DEPOSIT
			,WaitingListNumber = WAITING_LIST_NUMBER
			,CreatedDateTime = CREATE_DTTM
			,ModifiedDateTime = MODIF_DTTM
			,UserCreate = USER_CREATE
			,UserModify = USER_MODIF
			,SessionTransactionID = STRAN_REFNO
			,PriorPointerID = PRIOR_POINTER
			,ExternalKey = EXTERNAL_KEY
			,PlannedProcedure = PLANNED_PROC
			,ServicePointSessionID = SPSSN_REFNO
			,DayCareEpisodeID = DYCEP_REFNO
			,AccessLevelsID = ACLEV_REFNO
			,StaffTeamID = STEAM_REFNO
			,ContactPurposeID = CONTP_REFNO
			,RemovalHealthOrganisationID = REMVL_HEORG_REFNO
			,PatialBookingNumber = PBK_NUMBER
			,VisitID = VISIT_REFNO
			,BookingTypeID = BKTYP_REFNO
			,PartialBookingStageID = STAGE_REFNO
			,AcknowledgementDateTime = ACKNW_DTTM
			,AcknowledgementStatusID = ACKNW_LETST_REFNO
			,BookingInvitationDateTime = INVIT_DTTM
			,BookingInvitationStatusID = INVIT_LETST_REFNO
			,BookingReminderDateTime = REMND_DTTM
			,BookingReminderStatusID = REMND_LETST_REFNO
			,CancellationDateTime = CANCL_DTTM
			,CancellationStatusID = CANCL_LETST_REFNO
			,NumberOfDaysSuspended = DAYS_SUSPENDED
			,IsPatientChoice = PATNT_CHOICE_FLG
			,NoOfOccurences = NO_OF_OCCURENCES
			,TreatInitFlag = TREAT_INIT_FLAG
			,DeletedFlag = DELETED_FLAG
			,RemovedByUserID = REMOVEDBY_USER
			,RemovedByDateTime = REMOVEDBY_DTTM
			,RemovedByID = REMOVEDBY_REFNO
			,RTTStatusID = RTTST_REFNO
			,DiagnosticRequestTypeServiceID = DSRTP_REFNO
			,ISDiagnosticWait = DIAG_WAIT_FLAG
			,WaitingListStaffTeamID = WL_STEAM_REFNO
			,IsFirstDefinitiveTreatment = FIRST_DEF_TRMNT
			,HealthOrganisationOwnerID = OWNER_HEORG_REFNO
			,LorenzoCreated = Created

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.WaitingList
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASWaitingList on #TLoadPASWaitingList
	(
	WaitingListID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.WaitingList target
using
	(
	select
		*
	from
		#TLoadPASWaitingList
	
	) source
	on	source.WaitingListID = target.WaitingListID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		 WaitingListID
		,WaitingListRuleID
		,AdmissionDecisionID
		,ReferralID
		,SpecialtyID
		,ProfessionalCarerID
		,HealthOrganisationID
		,PatientID
		,AdmissionMethodID
		,AdministrativeCategoryID
		,ManagementIntentionID
		,PriorityID
		,UrgencyID
		,WaitingListDescription
		,ShortNotice
		,OriginalDecisionToAdmitDateTime
		,WaitingListDateTime
		,RemovalDateTime
		,RemovalID
		,PriorWaitingListID
		,GuaranteedAdmissionDateTime
		,TransportID
		,IsHotelStay
		,AdmissionDateTime
		,ServiceTypeID
		,ServicePointID
		,DayOfWeekID
		,TheatreTime
		,WhoCanOperateID
		,ClinicalPriorityID
		,NoFundsFlag
		,ContractID
		,PurchaserID
		,ProviderID
		,LocalWaitingListRuleID
		,CodingStatusID
		,TransferServicePointID
		,LengthOfStay
		,transferLengthOfStay
		,AnaestheticTypeID
		,WaitingStartDateTime
		,WaitingSuspensionStartDateTime
		,TotalSuspensionTime
		,IsReadyForAdmission
		,ActiveIndicator
		,ThVisitID
		,Deposit
		,WaitingListNumber
		,CreatedDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,SessionTransactionID
		,PriorPointerID
		,ExternalKey
		,PlannedProcedure
		,ServicePointSessionID
		,DayCareEpisodeID
		,AccessLevelsID
		,StaffTeamID
		,ContactPurposeID
		,RemovalHealthOrganisationID
		,PatialBookingNumber
		,VisitID
		,BookingTypeID
		,PartialBookingStageID
		,AcknowledgementDateTime
		,AcknowledgementStatusID
		,BookingInvitationDateTime
		,BookingInvitationStatusID
		,BookingReminderDateTime
		,BookingReminderStatusID
		,CancellationDateTime
		,CancellationStatusID
		,NumberOfDaysSuspended
		,IsPatientChoice
		,NoOfOccurences
		,TreatInitFlag
		,DeletedFlag
		,RemovedByUserID
		,RemovedByDateTime
		,RemovedByID
		,RTTStatusID
		,DiagnosticRequestTypeServiceID
		,ISDiagnosticWait
		,WaitingListStaffTeamID
		,IsFirstDefinitiveTreatment
		,HealthOrganisationOwnerID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		 source.WaitingListID
		,source.WaitingListRuleID
		,source.AdmissionDecisionID
		,source.ReferralID
		,source.SpecialtyID
		,source.ProfessionalCarerID
		,source.HealthOrganisationID
		,source.PatientID
		,source.AdmissionMethodID
		,source.AdministrativeCategoryID
		,source.ManagementIntentionID
		,source.PriorityID
		,source.UrgencyID
		,source.WaitingListDescription
		,source.ShortNotice
		,source.OriginalDecisionToAdmitDateTime
		,source.WaitingListDateTime
		,source.RemovalDateTime
		,source.RemovalID
		,source.PriorWaitingListID
		,source.GuaranteedAdmissionDateTime
		,source.TransportID
		,source.IsHotelStay
		,source.AdmissionDateTime
		,source.ServiceTypeID
		,source.ServicePointID
		,source.DayOfWeekID
		,source.TheatreTime
		,source.WhoCanOperateID
		,source.ClinicalPriorityID
		,source.NoFundsFlag
		,source.ContractID
		,source.PurchaserID
		,source.ProviderID
		,source.LocalWaitingListRuleID
		,source.CodingStatusID
		,source.TransferServicePointID
		,source.LengthOfStay
		,source.transferLengthOfStay
		,source.AnaestheticTypeID
		,source.WaitingStartDateTime
		,source.WaitingSuspensionStartDateTime
		,source.TotalSuspensionTime
		,source.IsReadyForAdmission
		,source.ActiveIndicator
		,source.ThVisitID
		,source.Deposit
		,source.WaitingListNumber
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.UserCreate
		,source.UserModify
		,source.SessionTransactionID
		,source.PriorPointerID
		,source.ExternalKey
		,source.PlannedProcedure
		,source.ServicePointSessionID
		,source.DayCareEpisodeID
		,source.AccessLevelsID
		,source.StaffTeamID
		,source.ContactPurposeID
		,source.RemovalHealthOrganisationID
		,source.PatialBookingNumber
		,source.VisitID
		,source.BookingTypeID
		,source.PartialBookingStageID
		,source.AcknowledgementDateTime
		,source.AcknowledgementStatusID
		,source.BookingInvitationDateTime
		,source.BookingInvitationStatusID
		,source.BookingReminderDateTime
		,source.BookingReminderStatusID
		,source.CancellationDateTime
		,source.CancellationStatusID
		,source.NumberOfDaysSuspended
		,source.IsPatientChoice
		,source.NoOfOccurences
		,source.TreatInitFlag
		,source.DeletedFlag
		,source.RemovedByUserID
		,source.RemovedByDateTime
		,source.RemovedByID
		,source.RTTStatusID
		,source.DiagnosticRequestTypeServiceID
		,source.ISDiagnosticWait
		,source.WaitingListStaffTeamID
		,source.IsFirstDefinitiveTreatment
		,source.HealthOrganisationOwnerID
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		 target.WaitingListID
		,target.WaitingListRuleID
		,target.AdmissionDecisionID
		,target.ReferralID
		,target.SpecialtyID
		,target.ProfessionalCarerID
		,target.HealthOrganisationID
		,target.PatientID
		,target.AdmissionMethodID
		,target.AdministrativeCategoryID
		,target.ManagementIntentionID
		,target.PriorityID
		,target.UrgencyID
		,target.WaitingListDescription
		,target.ShortNotice
		,target.OriginalDecisionToAdmitDateTime
		,target.WaitingListDateTime
		,target.RemovalDateTime
		,target.RemovalID
		,target.PriorWaitingListID
		,target.GuaranteedAdmissionDateTime
		,target.TransportID
		,target.IsHotelStay
		,target.AdmissionDateTime
		,target.ServiceTypeID
		,target.ServicePointID
		,target.DayOfWeekID
		,target.TheatreTime
		,target.WhoCanOperateID
		,target.ClinicalPriorityID
		,target.NoFundsFlag
		,target.ContractID
		,target.PurchaserID
		,target.ProviderID
		,target.LocalWaitingListRuleID
		,target.CodingStatusID
		,target.TransferServicePointID
		,target.LengthOfStay
		,target.transferLengthOfStay
		,target.AnaestheticTypeID
		,target.WaitingStartDateTime
		,target.WaitingSuspensionStartDateTime
		,target.TotalSuspensionTime
		,target.IsReadyForAdmission
		,target.ActiveIndicator
		,target.ThVisitID
		,target.Deposit
		,target.WaitingListNumber
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.SessionTransactionID
		,target.PriorPointerID
		,target.ExternalKey
		,target.PlannedProcedure
		,target.ServicePointSessionID
		,target.DayCareEpisodeID
		,target.AccessLevelsID
		,target.StaffTeamID
		,target.ContactPurposeID
		,target.RemovalHealthOrganisationID
		,target.PatialBookingNumber
		,target.VisitID
		,target.BookingTypeID
		,target.PartialBookingStageID
		,target.AcknowledgementDateTime
		,target.AcknowledgementStatusID
		,target.BookingInvitationDateTime
		,target.BookingInvitationStatusID
		,target.BookingReminderDateTime
		,target.BookingReminderStatusID
		,target.CancellationDateTime
		,target.CancellationStatusID
		,target.NumberOfDaysSuspended
		,target.IsPatientChoice
		,target.NoOfOccurences
		,target.TreatInitFlag
		,target.DeletedFlag
		,target.RemovedByUserID
		,target.RemovedByDateTime
		,target.RemovedByID
		,target.RTTStatusID
		,target.DiagnosticRequestTypeServiceID
		,target.ISDiagnosticWait
		,target.WaitingListStaffTeamID
		,target.IsFirstDefinitiveTreatment
		,target.HealthOrganisationOwnerID
		,target.LorenzoCreated
		)

then
	update
	set
		 target.WaitingListID = source.WaitingListID
		,target.WaitingListRuleID = source.WaitingListRuleID
		,target.AdmissionDecisionID = source.AdmissionDecisionID
		,target.ReferralID = source.ReferralID
		,target.SpecialtyID = source.SpecialtyID
		,target.ProfessionalCarerID = source.ProfessionalCarerID
		,target.HealthOrganisationID = source.HealthOrganisationID
		,target.PatientID = source.PatientID
		,target.AdmissionMethodID = source.AdmissionMethodID
		,target.AdministrativeCategoryID = source.AdministrativeCategoryID
		,target.ManagementIntentionID = source.ManagementIntentionID
		,target.PriorityID = source.PriorityID
		,target.UrgencyID = source.UrgencyID
		,target.WaitingListDescription = source.WaitingListDescription
		,target.ShortNotice = source.ShortNotice
		,target.OriginalDecisionToAdmitDateTime = source.OriginalDecisionToAdmitDateTime
		,target.WaitingListDateTime = source.WaitingListDateTime
		,target.RemovalDateTime = source.RemovalDateTime
		,target.RemovalID = source.RemovalID
		,target.PriorWaitingListID = source.PriorWaitingListID
		,target.GuaranteedAdmissionDateTime = source.GuaranteedAdmissionDateTime
		,target.TransportID = source.TransportID
		,target.IsHotelStay = source.IsHotelStay
		,target.AdmissionDateTime = source.AdmissionDateTime
		,target.ServiceTypeID = source.ServiceTypeID
		,target.ServicePointID = source.ServicePointID
		,target.DayOfWeekID = source.DayOfWeekID
		,target.TheatreTime = source.TheatreTime
		,target.WhoCanOperateID = source.WhoCanOperateID
		,target.ClinicalPriorityID = source.ClinicalPriorityID
		,target.NoFundsFlag = source.NoFundsFlag
		,target.ContractID = source.ContractID
		,target.PurchaserID = source.PurchaserID
		,target.ProviderID = source.ProviderID
		,target.LocalWaitingListRuleID = source.LocalWaitingListRuleID
		,target.CodingStatusID = source.CodingStatusID
		,target.TransferServicePointID = source.TransferServicePointID
		,target.LengthOfStay = source.LengthOfStay
		,target.transferLengthOfStay = source.transferLengthOfStay
		,target.AnaestheticTypeID = source.AnaestheticTypeID
		,target.WaitingStartDateTime = source.WaitingStartDateTime
		,target.WaitingSuspensionStartDateTime = source.WaitingSuspensionStartDateTime
		,target.TotalSuspensionTime = source.TotalSuspensionTime
		,target.IsReadyForAdmission = source.IsReadyForAdmission
		,target.ActiveIndicator = source.ActiveIndicator
		,target.ThVisitID = source.ThVisitID
		,target.Deposit = source.Deposit
		,target.WaitingListNumber = source.WaitingListNumber
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.SessionTransactionID = source.SessionTransactionID
		,target.PriorPointerID = source.PriorPointerID
		,target.ExternalKey = source.ExternalKey
		,target.PlannedProcedure = source.PlannedProcedure
		,target.ServicePointSessionID = source.ServicePointSessionID
		,target.DayCareEpisodeID = source.DayCareEpisodeID
		,target.AccessLevelsID = source.AccessLevelsID
		,target.StaffTeamID = source.StaffTeamID
		,target.ContactPurposeID = source.ContactPurposeID
		,target.RemovalHealthOrganisationID = source.RemovalHealthOrganisationID
		,target.PatialBookingNumber = source.PatialBookingNumber
		,target.VisitID = source.VisitID
		,target.BookingTypeID = source.BookingTypeID
		,target.PartialBookingStageID = source.PartialBookingStageID
		,target.AcknowledgementDateTime = source.AcknowledgementDateTime
		,target.AcknowledgementStatusID = source.AcknowledgementStatusID
		,target.BookingInvitationDateTime = source.BookingInvitationDateTime
		,target.BookingInvitationStatusID = source.BookingInvitationStatusID
		,target.BookingReminderDateTime = source.BookingReminderDateTime
		,target.BookingReminderStatusID = source.BookingReminderStatusID
		,target.CancellationDateTime = source.CancellationDateTime
		,target.CancellationStatusID = source.CancellationStatusID
		,target.NumberOfDaysSuspended = source.NumberOfDaysSuspended
		,target.IsPatientChoice = source.IsPatientChoice
		,target.NoOfOccurences = source.NoOfOccurences
		,target.TreatInitFlag = source.TreatInitFlag
		,target.DeletedFlag = source.DeletedFlag
		,target.RemovedByUserID = source.RemovedByUserID
		,target.RemovedByDateTime = source.RemovedByDateTime
		,target.RemovedByID = source.RemovedByID
		,target.RTTStatusID = source.RTTStatusID
		,target.DiagnosticRequestTypeServiceID = source.DiagnosticRequestTypeServiceID
		,target.ISDiagnosticWait = source.ISDiagnosticWait
		,target.WaitingListStaffTeamID = source.WaitingListStaffTeamID
		,target.IsFirstDefinitiveTreatment = source.IsFirstDefinitiveTreatment
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime