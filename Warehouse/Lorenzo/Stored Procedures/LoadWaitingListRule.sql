﻿CREATE PROCEDURE [Lorenzo].[LoadWaitingListRule]

AS



declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(WaitingListRule.LorenzoCreated)
		from
			Lorenzo.WaitingListRule
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 WaitingListRuleID
			,SpecialtyID
			,ProfessionalCarerID
			,HealthOrganisationID
			,MaximumWaitDayCaseAdmission
			,MaximumWaitOrdinaryAdmission
			,CreatedDateTime
			,ModifiedDateTime
			,UserCreate
			,UserModify
			,WaitingListRuleCode
			,WaitingListRule
			,Comments
			,SessionTransactionID
			,PriorPointerID
			,ExternalKey
			,ServiceTypeID
			,ServicePointID
			,IsLocalType
			,EndDateTime
			,StartDateTime
			,IsDiagnosticWait
			,HealthOrganisationOwnerID
			,LorenzoCreated
			)
into
	#TLoadPASWaitingListRule
from
	(
	select
		WaitingListRuleID
		,SpecialtyID = cast(SpecialtyID as int)
		,ProfessionalCarerID = cast(ProfessionalCarerID as int)
		,HealthOrganisationID = cast(HealthOrganisationID as int)
		,MaximumWaitDayCaseAdmission = cast(MaximumWaitDayCaseAdmission as int)
		,MaximumWaitOrdinaryAdmission = cast(MaximumWaitOrdinaryAdmission as int)
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifiedDateTime = cast(ModifiedDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(50))
		,UserModify = cast(nullif(UserModify, '') as varchar(50))
		,WaitingListRuleCode = cast(nullif(WaitingListRuleCode, '') as varchar(50))
		,WaitingListRule = cast(nullif(WaitingListRule, '') as varchar(100))
		,Comments = cast(nullif(Comments, '') as varchar(255))
		,SessionTransactionID = cast(SessionTransactionID as int)
		,PriorPointerID = cast(PriorPointerID as int)
		,ExternalKey = cast(nullif(ExternalKey, '') as varchar(50))
		,ServiceTypeID = cast(ServiceTypeID as int)
		,ServicePointID = cast(ServicePointID as int)
		,IsLocalType = cast(nullif(IsLocalType, '') as char(1))
		,EndDateTime = cast(EndDateTime as datetime)
		,StartDateTime = cast(StartDateTime as datetime)
		,IsDiagnosticWait = cast(nullif(IsDiagnosticWait, '') as char(1))
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as int)
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			WaitingListRuleID = WLRUL_REFNO
		  ,SpecialtyID = SPECT_REFNO
		  ,ProfessionalCarerID = PROCA_REFNO
		  ,HealthOrganisationID = HEORG_REFNO
		  ,MaximumWaitDayCaseAdmission = MWAIT_DAYCASE
		  ,MaximumWaitOrdinaryAdmission = MWAIT_ORDINARY
		  ,CreatedDateTime = CREATE_DTTM
		  ,ModifiedDateTime = MODIF_DTTM
		  ,UserCreate = USER_CREATE
		  ,UserModify = USER_MODIF
		  ,WaitingListRuleCode = CODE
		  ,WaitingListRule = NAME
		  ,Comments = COMMENTS
		  ,SessionTransactionID = STRAN_REFNO
		  ,PriorPointerID = PRIOR_POINTER
		  ,ExternalKey = EXTERNAL_KEY
		  ,ServiceTypeID = SVTYP_REFNO
		  ,ServicePointID = SPONT_REFNO
		  ,IsLocalType = LOCAL_FLAG 
		  ,EndDateTime = END_DTTM
		  ,StartDateTime = START_DTTM
		  ,IsDiagnosticWait = DIAG_WAIT_FLAG
		  ,HealthOrganisationOwnerID = OWNER_HEORG_REFNO
		  ,LorenzoCreated = Created

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.WaitingListRule
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASWaitingListRule on #TLoadPASWaitingListRule
	(
	WaitingListRuleID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.WaitingListRule target
using
	(
	select
		*
	from
		#TLoadPASWaitingListRule
	
	) source
	on	source.WaitingListRuleID = target.WaitingListRuleID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		  WaitingListRuleID
		,SpecialtyID
		,ProfessionalCarerID
		,HealthOrganisationID
		,MaximumWaitDayCaseAdmission
		,MaximumWaitOrdinaryAdmission
		,CreatedDateTime
		,ModifiedDateTime
		,UserCreate
		,UserModify
		,WaitingListRuleCode
		,WaitingListRule
		,Comments
		,SessionTransactionID
		,PriorPointerID
		,ExternalKey
		,ServiceTypeID
		,ServicePointID
		,IsLocalType
		,EndDateTime
		,StartDateTime
		,IsDiagnosticWait
		,HealthOrganisationOwnerID
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		 source.WaitingListRuleID
		,source.SpecialtyID
		,source.ProfessionalCarerID
		,source.HealthOrganisationID
		,source.MaximumWaitDayCaseAdmission
		,source.MaximumWaitOrdinaryAdmission
		,source.CreatedDateTime
		,source.ModifiedDateTime
		,source.UserCreate
		,source.UserModify
		,source.WaitingListRuleCode
		,source.WaitingListRule
		,source.Comments
		,source.SessionTransactionID
		,source.PriorPointerID
		,source.ExternalKey
		,source.ServiceTypeID
		,source.ServicePointID
		,source.IsLocalType
		,source.EndDateTime
		,source.StartDateTime
		,source.IsDiagnosticWait
		,source.HealthOrganisationOwnerID
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		 target.WaitingListRuleID
		,target.SpecialtyID
		,target.ProfessionalCarerID
		,target.HealthOrganisationID
		,target.MaximumWaitDayCaseAdmission
		,target.MaximumWaitOrdinaryAdmission
		,target.CreatedDateTime
		,target.ModifiedDateTime
		,target.UserCreate
		,target.UserModify
		,target.WaitingListRuleCode
		,target.WaitingListRule
		,target.Comments
		,target.SessionTransactionID
		,target.PriorPointerID
		,target.ExternalKey
		,target.ServiceTypeID
		,target.ServicePointID
		,target.IsLocalType
		,target.EndDateTime
		,target.StartDateTime
		,target.IsDiagnosticWait
		,target.HealthOrganisationOwnerID
		,target.LorenzoCreated
		)

then
	update
	set
		  target.WaitingListRuleID = source.WaitingListRuleID
		,target.SpecialtyID = source.SpecialtyID
		,target.ProfessionalCarerID = source.ProfessionalCarerID
		,target.HealthOrganisationID = source.HealthOrganisationID
		,target.MaximumWaitDayCaseAdmission = source.MaximumWaitDayCaseAdmission
		,target.MaximumWaitOrdinaryAdmission = source.MaximumWaitOrdinaryAdmission
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifiedDateTime = source.ModifiedDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.WaitingListRuleCode = source.WaitingListRuleCode
		,target.WaitingListRule = source.WaitingListRule
		,target.Comments = source.Comments
		,target.SessionTransactionID = source.SessionTransactionID
		,target.PriorPointerID = source.PriorPointerID
		,target.ExternalKey = source.ExternalKey
		,target.ServiceTypeID = source.ServiceTypeID
		,target.ServicePointID = source.ServicePointID
		,target.IsLocalType = source.IsLocalType
		,target.EndDateTime = source.EndDateTime
		,target.StartDateTime = source.StartDateTime
		,target.IsDiagnosticWait = source.IsDiagnosticWait
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime