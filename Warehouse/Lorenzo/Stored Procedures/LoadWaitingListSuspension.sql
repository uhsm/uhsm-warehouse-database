﻿CREATE PROCEDURE [Lorenzo].[LoadWaitingListSuspension]

AS


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @from datetime =
	isnull(
		(
		select
			max(WaitingListSuspension.LorenzoCreated)
		from
			Lorenzo.WaitingListSuspension
		)
		,'1 Jan 1900'
	)

----create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 WaitingListSuspensionID
			,WaitingListID
			,SuspensionReasonID
			,SuspensionReasonMainCode
			,SuspensionReasonDescription
			,StartDateTime
			,EndDateTime
			,Comments
			,CreatedDateTime
			,ModifyDateTime
			,UserCreate
			,UserModify
			,ReviewDateTime
			,HealthOrganisationOwnerID
			,HealthOrganisationOwnerMainCode
			,IsEnd
			,IsStart
			,LorenzoCreated
			)
into
	#TLoadPASWaitingListSuspension
from
	(
	select
		WaitingListSuspensionID
		,WaitingListID = cast(WaitingListID as numeric)
		,SuspensionReasonID = cast(SuspensionReasonID as numeric)
		,SuspensionReasonMainCode = cast(nullif(SuspensionReasonMainCode, '') as varchar(25))
		,SuspensionReasonDescription = cast(nullif(SuspensionReasonDescription, '') as varchar(80))
		,StartDateTime = cast(StartDateTime as datetime)
		,EndDateTime = cast(EndDateTime as datetime)
		,Comments = cast(nullif(Comments, '') as varchar(255))
		,CreatedDateTime = cast(CreatedDateTime as datetime)
		,ModifyDateTime = cast(ModifyDateTime as datetime)
		,UserCreate = cast(nullif(UserCreate, '') as varchar(30))
		,UserModify = cast(nullif(UserModify, '') as varchar(30))
		,ReviewDateTime = cast(ReviewDateTime as datetime)
		,HealthOrganisationOwnerID = cast(HealthOrganisationOwnerID as numeric)
		,HealthOrganisationOwnerMainCode = cast(nullif(HealthOrganisationOwnerMainCode, '') as varchar(25))
		,IsEnd = cast(nullif(IsEnd, '') as char(1))
		,IsStart = cast(nullif(IsStart, '') as char(1))
		,LorenzoCreated = cast(LorenzoCreated as datetime)

		,IsDeleted
	from
		(
		select
			 WaitingListSuspensionID = WSUSP_REFNO
			  ,WaitingListID = WLIST_REFNO
			  ,SuspensionReasonID = SUSRS_REFNO
			  ,SuspensionReasonMainCode = SUSRS_REFNO_MAIN_CODE
			  ,SuspensionReasonDescription = SUSRS_REFNO_DESCRIPTION 
			  ,StartDateTime = START_DTTM
			  ,EndDateTime = END_DTTM
			  ,Comments = COMMENTS
			  ,CreatedDateTime = CREATE_DTTM
			  ,ModifyDateTime = MODIF_DTTM
			  ,UserCreate = USER_CREATE
			  ,UserModify = USER_MODIF
			  ,ReviewDateTime = REVIEW_DTTM
			  ,HealthOrganisationOwnerID = OWNER_HEORG_REFNO
			  ,HealthOrganisationOwnerMainCode = OWNER_HEORG_REFNO_MAIN_IDENT
			  ,IsEnd = END_FLAG
			  ,IsStart = START_FLAG
			  ,LorenzoCreated = Created

			,IsDeleted = cast(case when ARCHV_FLAG = 'N' then 0 else 1 end as bit)
		from
			Lorenzo.dbo.WaitingListSuspension
		where
			Created > @from

		) Encounter

	) Encounter


create unique clustered index #IX_TLoadPASWaitingListSuspension on #TLoadPASWaitingListSuspension
	(
	WaitingListSuspensionID  ASC
	)


declare @ProcessList table
	(
	Action nvarchar(10)
	)

merge
	Lorenzo.WaitingListSuspension target
using
	(
	select
		*
	from
		#TLoadPASWaitingListSuspension
	
	) source
	on	source.WaitingListSuspensionID = target.WaitingListSuspensionID

when matched
and	source.IsDeleted = 1
then delete

when not matched
then
	insert
		(
		  WaitingListSuspensionID
		,WaitingListID
		,SuspensionReasonID
		,SuspensionReasonMainCode
		,SuspensionReasonDescription
		,StartDateTime
		,EndDateTime
		,Comments
		,CreatedDateTime
		,ModifyDateTime
		,UserCreate
		,UserModify
		,ReviewDateTime
		,HealthOrganisationOwnerID
		,HealthOrganisationOwnerMainCode
		,IsEnd
		,IsStart
		,LorenzoCreated

		,Created
		,ByWhom
		)
	values
		(
		  source.WaitingListSuspensionID
		,source.WaitingListID
		,source.SuspensionReasonID
		,source.SuspensionReasonMainCode
		,source.SuspensionReasonDescription
		,source.StartDateTime
		,source.EndDateTime
		,source.Comments
		,source.CreatedDateTime
		,source.ModifyDateTime
		,source.UserCreate
		,source.UserModify
		,source.ReviewDateTime
		,source.HealthOrganisationOwnerID
		,source.HealthOrganisationOwnerMainCode
		,source.IsEnd
		,source.IsStart
		,source.LorenzoCreated

		,getdate()
		,suser_name()
		)

when matched
and source.EncounterChecksum <>
	CHECKSUM(
		 target.WaitingListSuspensionID
		,target.WaitingListID
		,target.SuspensionReasonID
		,target.SuspensionReasonMainCode
		,target.SuspensionReasonDescription
		,target.StartDateTime
		,target.EndDateTime
		,target.Comments
		,target.CreatedDateTime
		,target.ModifyDateTime
		,target.UserCreate
		,target.UserModify
		,target.ReviewDateTime
		,target.HealthOrganisationOwnerID
		,target.HealthOrganisationOwnerMainCode
		,target.IsEnd
		,target.IsStart
		,target.LorenzoCreated
		)

then
	update
	set
		 target.WaitingListSuspensionID = source.WaitingListSuspensionID
		,target.WaitingListID = source.WaitingListID
		,target.SuspensionReasonID = source.SuspensionReasonID
		,target.SuspensionReasonMainCode = source.SuspensionReasonMainCode
		,target.SuspensionReasonDescription = source.SuspensionReasonDescription
		,target.StartDateTime = source.StartDateTime
		,target.EndDateTime = source.EndDateTime
		,target.Comments = source.Comments
		,target.CreatedDateTime = source.CreatedDateTime
		,target.ModifyDateTime = source.ModifyDateTime
		,target.UserCreate = source.UserCreate
		,target.UserModify = source.UserModify
		,target.ReviewDateTime = source.ReviewDateTime
		,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
		,target.HealthOrganisationOwnerMainCode = source.HealthOrganisationOwnerMainCode
		,target.IsEnd = source.IsEnd
		,target.IsStart = source.IsStart
		,target.LorenzoCreated = source.LorenzoCreated

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime