﻿CREATE TABLE [Lorenzo].[AdmissionDecision] (
    [AdmissionDecisionID]       INT          NOT NULL,
    [ClinicalCostCentreID]      INT          NULL,
    [SpecialtyID]               INT          NULL,
    [PriorityID]                INT          NULL,
    [ReferralID]                INT          NULL,
    [ProfessionalCarerID]       INT          NULL,
    [PatientID]                 INT          NULL,
    [ScheduleID]                INT          NULL,
    [DecisionToAdmitDateTime]   DATETIME     NULL,
    [CreatedDateTime]           DATETIME     NULL,
    [ModifiedDateTime]          DATETIME     NULL,
    [UserCreate]                VARCHAR (50) NULL,
    [UserModify]                VARCHAR (50) NULL,
    [SessionTransactionID]      INT          NULL,
    [PriorPointerID]            INT          NULL,
    [ExternalKey]               VARCHAR (50) NULL,
    [NoAdmissionAfterDateTime]  DATETIME     NULL,
    [StaffTeamID]               INT          NULL,
    [HealthOrganisationOwnerID] INT          NULL,
    [LorenzoCreated]            DATETIME     NOT NULL,
    [Created]                   DATETIME     NOT NULL,
    [Updated]                   DATETIME     NULL,
    [ByWhom]                    VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Lorenzo_AdmissionDecision] PRIMARY KEY CLUSTERED ([AdmissionDecisionID] ASC)
);



GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170110-110548]
    ON [Lorenzo].[AdmissionDecision]([ReferralID] ASC, [Created] ASC, [Updated] ASC);

