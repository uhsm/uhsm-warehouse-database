﻿CREATE TABLE [Lorenzo].[AdmissionOffer] (
    [AdmissionOfferID]                INT           NOT NULL,
    [WaitingListID]                   INT           NULL,
    [DeferralReasonID]                INT           NULL,
    [OfferOutcomeID]                  INT           NULL,
    [ServicePointBedID]               INT           NULL,
    [AdmissionOfferDateTime]          DATETIME      NULL,
    [AdmissionOfferOutcomeDateTime]   DATETIME      NULL,
    [TCIDateTime]                     DATETIME      NULL,
    [OfferCancellationDateTime]       DATETIME      NULL,
    [ConfirmationDateTime]            DATETIME      NULL,
    [Comments]                        VARCHAR (255) NULL,
    [IsDeferred]                      CHAR (1)      NULL,
    [DeferralDateTime]                DATETIME      NULL,
    [ServicePointID]                  INT           NULL,
    [BedCategoryID]                   INT           NULL,
    [TransferBedCategoryID]           INT           NULL,
    [ExpectedDischargeDateTime]       DATETIME      NULL,
    [TransferServicePointID]          INT           NULL,
    [TransferDateTime]                DATETIME      NULL,
    [LatestPossibleTCIDateTime]       DATETIME      NULL,
    [ConfirmationStatusID]            INT           NULL,
    [OperationDateTime]               DATETIME      NULL,
    [NilByMouthDateTime]              DATETIME      NULL,
    [IsAccompanied]                   CHAR (1)      NULL,
    [IsLockedBed]                     CHAR (1)      NULL,
    [ScheduleCancellationReasonID]    INT           NULL,
    [CreatedDateTime]                 DATETIME      NULL,
    [ModifiedDateTime]                DATETIME      NULL,
    [UserCreate]                      VARCHAR (50)  NULL,
    [UserModify]                      VARCHAR (50)  NULL,
    [SessionTransactionID]            INT           NULL,
    [PriorPointerID]                  INT           NULL,
    [ExternalKey]                     VARCHAR (50)  NULL,
    [ServicePointBedCopyID]           INT           NULL,
    [AdmissionOfferTypeID]            INT           NULL,
    [BookingTypeID]                   INT           NULL,
    [InpatientOtherDateTime]          DATETIME      NULL,
    [ShortNoticeFlag]                 CHAR (1)      NULL,
    [RTTStatusID]                     VARCHAR (50)  NULL,
    [EarliestReasonableOfferDateTime] DATETIME      NULL,
    [VerbalFlag]                      VARCHAR (50)  NULL,
    [IsPatientUnavailable]            CHAR (1)      NULL,
    [ConfirmationDetailID]            INT           NULL,
    [OutcomeDetailID]                 INT           NULL,
    [HealthOrganisationOwnerID]       INT           NULL,
    [LorenzoCreated]                  DATETIME      NULL,
    [Created]                         DATETIME      NOT NULL,
    [Updated]                         DATETIME      NULL,
    [ByWhom]                          VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Lorenzo_AdmissionOffer] PRIMARY KEY CLUSTERED ([AdmissionOfferID] ASC)
);





GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170104-083632]
    ON [Lorenzo].[AdmissionOffer]([OfferOutcomeID] ASC, [WaitingListID] ASC)
    INCLUDE([TCIDateTime]);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170110-110438]
    ON [Lorenzo].[AdmissionOffer]([WaitingListID] ASC, [Created] ASC, [Updated] ASC);

