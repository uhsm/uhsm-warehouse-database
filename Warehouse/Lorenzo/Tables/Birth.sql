﻿CREATE TABLE [Lorenzo].[Birth] (
    [BirthID]                         INT           NOT NULL,
    [MaternitySpellID]                INT           NULL,
    [PatientID]                       INT           NULL,
    [MotherID]                        INT           NULL,
    [ProfessionalCarerID]             INT           NULL,
    [BirthOrder]                      INT           NULL,
    [DateOfBirth]                     SMALLDATETIME NULL,
    [SexID]                           INT           NULL,
    [DeliveryPersonStatusID]          INT           NULL,
    [BirthStatusID]                   INT           NULL,
    [DeliveryPlaceID]                 INT           NULL,
    [ResuscitationMethodByPressureID] INT           NULL,
    [ResuscitationMethodByDrugID]     INT           NULL,
    [BirthWeight]                     INT           NULL,
    [ApgarScoreAtOneMinute]           INT           NULL,
    [ApgarScoreAtFiveMinutes]         INT           NULL,
    [BCGAdministeredID]               INT           NULL,
    [BabyHeadCircumference]           INT           NULL,
    [BabyLength]                      INT           NULL,
    [ExaminationOfHipsID]             INT           NULL,
    [FollowUpCareID]                  INT           NULL,
    [FoetalPresentationID]            INT           NULL,
    [MetabolicScreeningID]            INT           NULL,
    [JaundiceID]                      INT           NULL,
    [FeedingTypeID]                   INT           NULL,
    [DeliveryMethodID]                INT           NULL,
    [MaternityDrugsID]                INT           NULL,
    [GestationLengthID]               INT           NULL,
    [ApgarScoreAtTenMinutes]          INT           NULL,
    [IsDeleted]                       VARCHAR (1)   NULL,
    [SessionTransactionID]            INT           NULL,
    [ReferralID]                      INT           NULL,
    [PriorPointerID]                  INT           NULL,
    [UserCreate]                      VARCHAR (30)  NULL,
    [UserModify]                      VARCHAR (30)  NULL,
    [CreatedDateTime]                 DATETIME      NULL,
    [ModifiedDateTime]                DATETIME      NULL,
    [ExternalKey]                     VARCHAR (20)  NULL,
    [NeonatalLevelOfCareID]           INT           NULL,
    [HealthOrganisationOwnerID]       INT           NULL,
    [LorenzoCreated]                  DATETIME      NULL,
    [Created]                         DATETIME      NOT NULL,
    [Updated]                         DATETIME      NULL,
    [ByWhom]                          VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Lorenzo_Birth] PRIMARY KEY CLUSTERED ([BirthID] ASC)
);



GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170110-104849]
    ON [Lorenzo].[Birth]([PatientID] ASC, [Created] ASC, [Updated] ASC)
    INCLUDE([DateOfBirth]);

