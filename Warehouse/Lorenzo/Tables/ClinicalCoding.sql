﻿CREATE TABLE [Lorenzo].[ClinicalCoding] (
    [ClinicalCodingID]               INT           NOT NULL,
    [ClinicalCodingClassificationID] INT           NULL,
    [PatientID]                      INT           NULL,
    [MedicalProblemLevelID]          INT           NULL,
    [CarriedOutOrProposedID]         INT           NULL,
    [OtherClinicalCodeID]            INT           NULL,
    [LinkClinicalCodingID]           INT           NULL,
    [Comments]                       VARCHAR (255) NULL,
    [ClinicalCoding]                 VARCHAR (255) NULL,
    SourceID                     INT           NULL,
    SourceCode                   VARCHAR (5)   NULL,
    [ClinicalCodingDateTime]         SMALLDATETIME NULL,
    [ClinicalCodingSubtypeCode]      VARCHAR (5)   NULL,
    [CreatedDateTime]                DATETIME      NULL,
    [ModifiedDateTime]               DATETIME      NULL,
    [UserCreate]                     VARCHAR (30)  NULL,
    [UserModify]                     VARCHAR (30)  NULL,
    [ClinicalCodingCode]             VARCHAR (20)  NULL,
    [ClinicalCodingInternalCode]     VARCHAR (25)  NULL,
    [SupplementaryCode]              VARCHAR (20)  NULL,
    [SupplementaryInternalCode]      VARCHAR (5)   NULL,
    [SessionTransactionID]           INT           NULL,
    [PriorPointerID]                 INT           NULL,
    [ExternalKey]                    VARCHAR (20)  NULL,
    [StartDateTime]                  SMALLDATETIME NULL,
    [EndDateTime]                    SMALLDATETIME NULL,
    [UpdatedDateTime]                SMALLDATETIME NULL,
    [StaffTeamID]                    INT           NULL,
    [ProfessionalCarerID]            INT           NULL,
    [DurationMinutes]                INT           NULL,
    [IsConfidential]                 CHAR (1)      NULL,
    [Dosage]                         INT           NULL,
    [DosageUnitID]                   INT           NULL,
    [LocationOnBodyID]               INT           NULL,
    [Frequency]                      INT           NULL,
    [FrequencyUnitID]                INT           NULL,
    [CancelDateTime]                 SMALLDATETIME NULL,
    [ParentClinicalCodingID]         INT           NULL,
    [RulesID]                        INT           NULL,
    [Value]                          VARCHAR (255) NULL,
    [IsProblem]                      CHAR (1)      NULL,
    [LateralityID]                   INT           NULL,
    [ProfessionalCareEpisodeID]      INT           NULL,
    [PeriodAdministeredID]           INT           NULL,
    [ReasonAdministeredID]           INT           NULL,
    [AnaestheticAnalgesicCategoryID] INT           NULL,
    [IsCauseOfDeath]                 CHAR (1)      NULL,
    [ClinicalCodingOrder]            INT           NULL,
    [CodingAuthorisationFlag]        CHAR (1)      NULL,
    [LatchedAuthorisationFlag]       VARCHAR (1)   NULL,
    [ActionToTake]                   VARCHAR (255) NULL,
    [AlertSeverityID]                INT           NULL,
    [IsContractedProcedure]          VARCHAR (1)   NULL,
    [SpecialtyID]                    INT           NULL,
    [IsHospitalService]              VARCHAR (1)   NULL,
    [MinimumPrice]                   INT           NULL,
    [MaximumPrice]                   INT           NULL,
    [ClinicalCodingEndDateTime]      SMALLDATETIME NULL,
    [BillODPCDID]                    INT           NULL,
    [ProcedureLocationID]            INT           NULL,
    [TheatreBodyRegionID]            INT           NULL,
    [IndividualCodingStatusID]       INT           NULL,
    [SynchronisationCode]            VARCHAR (20)  NULL,
    [AnaestheticTypeID]              INT           NULL,
    [CodingOnsetTypeID]              INT           NULL,
    [VisitPurposeID]                 INT           NULL,
    [HealthOrganisationOwnerID]      INT           NULL,
    [LorenzoCreated]                 DATETIME      NULL,
    [SequenceNumber]                 INT           NULL,
    [Created]                        DATETIME      NOT NULL,
    [Updated]                        DATETIME      NULL,
    [ByWhom]                         VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Lorenzo_ClinicalCoding] PRIMARY KEY CLUSTERED ([ClinicalCodingID] ASC)
);





GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170104-101100]
    ON [Lorenzo].[ClinicalCoding](SourceCode ASC, [ClinicalCodingSubtypeCode] ASC, [SequenceNumber] ASC, SourceID ASC)
    INCLUDE([ClinicalCodingDateTime], [ClinicalCodingCode], [ClinicalCodingEndDateTime]);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170104-084050]
    ON [Lorenzo].[ClinicalCoding]([PatientID] ASC, [ClinicalCodingSubtypeCode] ASC, [ClinicalCodingCode] ASC, [StartDateTime] ASC, [Created] ASC, [Updated] ASC);




GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170110-110900]
    ON [Lorenzo].[ClinicalCoding](SourceCode ASC)
    INCLUDE(SourceID, [StartDateTime], [Created], [Updated]);

