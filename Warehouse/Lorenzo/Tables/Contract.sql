﻿CREATE TABLE [Lorenzo].[Contract](
	[ContractID] [int] NOT NULL,
	[PurchaserID] [int] NULL,
	[SerialNumber] [varchar](20) NULL,
	[Identifier] [varchar](60) NULL,
	[Contract] [varchar](80) NULL,
	[StartDateTime] [smalldatetime] NULL,
	[EndDateTime] [smalldatetime] NULL,
	[ContractTypeID] [int] NULL,
	[ContractTypeMainCode] [varchar](25) NULL,
	[ContractType] [varchar](80) NULL,
	[ContractStatusID] [int] NULL,
	[ContractStatusMainCode] [varchar](25) NULL,
	[ContractStatus] [varchar](80) NULL,
	[UserCreate] [varchar](30) NULL,
	[UserModify] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL,

	[LorenzoCreated] [datetime] NOT NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,
    CONSTRAINT [PK_Lorenzo_Contract] PRIMARY KEY ([ContractID])
)
