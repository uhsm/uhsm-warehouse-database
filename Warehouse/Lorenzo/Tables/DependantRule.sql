﻿CREATE TABLE [Lorenzo].DependantRule
(
	[DependantRuleID] [int] NOT NULL,
	[RuleID] [int] NULL,
	[SourceID] [int] NULL,
	[SourceCode] [varchar](5) NULL,
	[IsEnforced] [char](1) NULL,
	[RuleOperator] [varchar](10) NULL,
	[RuleValue] [varchar](20) NULL,
	[CreatedDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL,
	[UserCreate] [varchar](30) NULL,
	[UserModify] [varchar](30) NULL,
	[SessionTransactionID] [int] NULL,
	[PriorPointerID] [int] NULL,
	[ExternalKey] [varchar](20) NULL,
	[Serial] [numeric](18, 0) NULL,
	[Parenthesis] [varchar](30) NULL,
	[LogicalOperator] [char](1) NULL,
	[AssociatedValue] [varchar](20) NULL,
	[HealthOrganisationOwnerID] [int] NULL,
	[LorenzoCreated] [datetime] NULL,

    [Created]                              DATETIME      NOT NULL,
    [Updated]                              DATETIME      NULL,
    [ByWhom]                               VARCHAR (50)  NOT NULL,
	CONSTRAINT [PK_Lorenzo_DependantRule] PRIMARY KEY CLUSTERED ([DependantRuleID] ASC)
)
