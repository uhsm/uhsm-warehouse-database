﻿CREATE TABLE [Lorenzo].[NoteRole]
(
	[NoteRoleID] [int] NOT NULL,
	[NotesID]  [int] NULL,
	[NotesIDKeyWords] [varchar](100) NULL,
	[NotesIDNote] [varchar](2000) NULL,
	[ActivityID]  [int] NULL,
	[ActivityCode] [varchar](5) NULL,
	[StartDateTime] [datetime] NULL,
	[EndDateTime] [datetime] NULL,
	[UserCreate] [varchar](30) NULL,
	[UserModify] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL,
	[NoteSerial]  [int] NULL,
	[NoteRoleTypeID]  [int] NULL,
	[NoteRoleTypeMainCode] [varchar](25) NULL,
	[NoteRoleTypeDescription] [varchar](80) NULL,
	[HealthOrganisationOwnerID]  [int] NULL,
	[HealthOrgnanisationOwnerMainCode] [varchar](25) NULL,
	[LorenzoCreated] [datetime] NULL,
	
	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,
    CONSTRAINT [PK_Lorenzo_NoteRole] PRIMARY KEY ([NoteRoleID])
)
