﻿CREATE TABLE [Lorenzo].[OverseasVisitorStatus] (
    [OverseasVisitorStatusID]    INT          NOT NULL,
    [OverseasStatusID]           INT          NULL,
    [OverseasStatusMainCode]     VARCHAR (25) NULL,
    [OverseasStatus]             VARCHAR (80) NULL,
    [PatientID]                  INT          NULL,
    [PatientSystemIdentifier]    VARCHAR (20) NULL,
    [NHSNumber]                  VARCHAR (20) NULL,
    [StartDateTime]              DATETIME     NULL,
    [EndDateTime]                DATETIME     NULL,
    [EnteredHostCountryDateTime] DATETIME     NULL,
    [CreatedDateTime]            DATETIME     NULL,
    [ModifiedDateTime]           DATETIME     NULL,
    [UserCreate]                 VARCHAR (30) NULL,
    [UserModify]                 VARCHAR (30) NULL,
    [LorenzoCreated]             DATETIME     NULL,
    [Created]                    DATETIME     NOT NULL,
    [Updated]                    DATETIME     NULL,
    [ByWhom]                     VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Lorenzo_OverseasVisitorStatus] PRIMARY KEY CLUSTERED ([OverseasVisitorStatusID] ASC)
);






GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170104-110000]
    ON [Lorenzo].[OverseasVisitorStatus]([PatientID] ASC, [StartDateTime] DESC, [OverseasVisitorStatusID] ASC)
    INCLUDE([OverseasStatusID]);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170110-104532]
    ON [Lorenzo].[OverseasVisitorStatus]([PatientID] ASC, [Created] ASC, [Updated] ASC);

