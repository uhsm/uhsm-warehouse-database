﻿CREATE TABLE [Lorenzo].[PatientAddress] (
    [PatientAddressID]          INT           NOT NULL,
    [AddressTypeID]             VARCHAR (5)   NULL,
    [Line1]                     VARCHAR (50)  NULL,
    [Line2]                     VARCHAR (50)  NULL,
    [Line3]                     VARCHAR (50)  NULL,
    [Line4]                     VARCHAR (50)  NULL,
    [Postcode]                  VARCHAR (25)  NULL,
    [LocalAuthorityCode]        VARCHAR (10)  NULL,
    [HealthDistrictCode]        VARCHAR (10)  NULL,
    [ElectoralWardCode]         VARCHAR (10)  NULL,
    [County]                    VARCHAR (30)  NULL,
    [CountryID]                 INT           NULL,
    [StartDateTime]             DATETIME      NULL,
    [EndDateTime]               SMALLDATETIME NULL,
    [CreatedDateTime]           DATETIME      NULL,
    [ModifiedDateTime]          DATETIME      NULL,
    [UserCreate]                VARCHAR (30)  NULL,
    [UserModify]                VARCHAR (30)  NULL,
    [SessionTransactionID]      INT           NULL,
    [PriorPointerID]            INT           NULL,
    [ExternalKey]               VARCHAR (20)  NULL,
    [CountyID]                  INT           NULL,
    [CommunityID]               INT           NULL,
    [ParishID]                  INT           NULL,
    [PurchaserAreaID]           INT           NULL,
    [PrimaryCareAreaID]         INT           NULL,
    [ResRegisteredDateTime]     SMALLDATETIME NULL,
    [Suburb]                    VARCHAR (40)  NULL,
    [StateCode]                 VARCHAR (5)   NULL,
    [PatientAccomodationID]     INT           NULL,
    [PCGCode]                   VARCHAR (10)  NULL,
    [TeamAreaID]                INT           NULL,
    [CommunityCareAreaID]       INT           NULL,
    [CommunitySectorID]         INT           NULL,
    [TownlandsID]               INT           NULL,
    [SynchronisationCode]       VARCHAR (20)  NULL,
    [DeliveryPointID]           INT           NULL,
    [QASBarcode]                VARCHAR (255) NULL,
    [PAFKey]                    INT           NULL,
    [QASVerified]               CHAR (1)      NULL,
    [PAFAddKey]                 VARCHAR (25)  NULL,
    [PAFInvalidDate]            DATETIME      NULL,
    [HealthOrganisationOwnerID] INT           NULL,
    [LorenzoCreated]            DATETIME      NULL,
    [Created]                   DATETIME      NOT NULL,
    [Updated]                   DATETIME      NULL,
    [ByWhom]                    VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Lorenzo_PatientAddress] PRIMARY KEY CLUSTERED ([PatientAddressID] ASC)
);






GO
CREATE UNIQUE NONCLUSTERED INDEX [NonClusteredIndex-20170103-161559]
    ON [Lorenzo].[PatientAddress]([AddressTypeID] ASC, [PatientAddressID] ASC);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170110-103751]
    ON [Lorenzo].[PatientAddress]([PatientAddressID] ASC, [Created] ASC, [Updated] ASC);

