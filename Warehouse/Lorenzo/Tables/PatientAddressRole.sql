﻿CREATE TABLE [Lorenzo].[PatientAddressRole] (
    [PatientAddressRoleID]           INT          NOT NULL,
    [ProfessionalCarerID]            INT          NULL,
    [PatientID]                      INT          NULL,
    [PersonalCarerID]                INT          NULL,
    [HealthOrganisationID]           INT          NULL,
    [AddressID]                      INT          NULL,
    [RollTypeCode]                   VARCHAR (25) NULL,
    [IsForCorrespondence]            CHAR (1)     NULL,
    [StartDateTime]                  DATETIME     NULL,
    [EndDateTime]                    DATETIME     NULL,
    [PatientTransportationAddressID] INT          NULL,
    [IsCurrent]                      VARCHAR (1)  NULL,
    [HomeLeaveID]                    INT          NULL,
    [PurchaserID]                    INT          NULL,
    [ProviderID]                     INT          NULL,
    [SecureFlag]                     CHAR (1)     NULL,
    [PatientPersonalCarerID]         INT          NULL,
    [PersonID]                       INT          NULL,
    [OrderID]                        INT          NULL,
    [CreatedDateTime]                DATETIME     NULL,
    [ModifiedDateTime]               DATETIME     NULL,
    [UserCreate]                     VARCHAR (50) NULL,
    [UserModify]                     VARCHAR (50) NULL,
    [SessionTransactionID]           INT          NULL,
    [PriorPointerID]                 INT          NULL,
    [ExternalKey]                    VARCHAR (50) NULL,
    [BookingFormID]                  INT          NULL,
    [SynchronisationCode]            VARCHAR (50) NULL,
    [PDSIdentifier]                  VARCHAR (50) NULL,
    [ReferralID]                     INT          NULL,
    [DecoupleFlag]                   CHAR (1)     NULL,
    [HealthOrganisationOwnerID]      INT          NULL,
    [LorenzoCreated]                 DATETIME     NULL,
    [Created]                        DATETIME     NOT NULL,
    [Updated]                        DATETIME     NULL,
    [ByWhom]                         VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Lorenzo_PatientAddressRole] PRIMARY KEY CLUSTERED ([PatientAddressRoleID] ASC)
);





GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170103-160912]
    ON [Lorenzo].[PatientAddressRole]([RollTypeCode] ASC, [PatientID] ASC)
    INCLUDE([PatientAddressRoleID], [StartDateTime], [EndDateTime], [AddressID]);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170110-103516]
    ON [Lorenzo].[PatientAddressRole]([RollTypeCode] ASC, [PatientID] ASC, [Created] ASC, [Updated] ASC);

