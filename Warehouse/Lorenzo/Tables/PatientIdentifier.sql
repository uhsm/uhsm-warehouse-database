﻿CREATE TABLE [Lorenzo].[PatientIdentifier] (
    [PatientIdentifierID]                INT          NOT NULL,
    [PatientIdentifierTypeID]            INT          NULL,
    [PatientID]                          INT          NULL,
    [PatientIdentifier]                  VARCHAR (20) NULL,
    [RDProjectID]                        INT          NULL,
    [EndDateTime]                        DATETIME     NULL,
    [StartDateTime]                      DATETIME     NULL,
    [IsCurrent]                          CHAR (1)     NULL,
    [PatientIdentifierStatusID]          INT          NULL,
    [PatientServiceRankID]               INT          NULL,
    [CreatedDateTime]                    DATETIME     NULL,
    [ModifiedDateTime]                   DATETIME     NULL,
    [UserCreate]                         VARCHAR (30) NULL,
    [UserModify]                         VARCHAR (30) NULL,
    [SessionTransactionID]               INT          NULL,
    [PriorPointerID]                     INT          NULL,
    [ExternalKey]                        VARCHAR (20) NULL,
    [PatientIdentifierSuffixID]          INT          NULL,
    [HealthOrganisationID]               INT          NULL,
    [MedicalRecordsHealthOrganisationID] INT          NULL,
    [HealthOrganisationOwnerID]          INT          NULL,
    [LorenzoCreated]                     DATETIME     NULL,
    [Created]                            DATETIME     NOT NULL,
    [Updated]                            DATETIME     NULL,
    [ByWhom]                             VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Lorenzo_PatientIdentifier] PRIMARY KEY CLUSTERED ([PatientIdentifierID] ASC)
);





GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170103-155906]
    ON [Lorenzo].[PatientIdentifier]([PatientIdentifierTypeID] ASC, [PatientID] ASC, [PatientIdentifier] ASC)
    INCLUDE([PatientIdentifierID], [StartDateTime]);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170110-102945]
    ON [Lorenzo].[PatientIdentifier]([PatientIdentifierTypeID] ASC, [PatientID] ASC, [Created] ASC, [Updated] ASC);

