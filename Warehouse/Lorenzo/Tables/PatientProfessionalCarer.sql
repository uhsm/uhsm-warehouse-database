﻿CREATE TABLE [Lorenzo].[PatientProfessionalCarer] (
    [PatientProfessionalCarerID]       INT          NOT NULL,
    [ProfessionalCarerTypeID]          INT          NULL,
    [ProfessionalCarerTypeMainCode]    VARCHAR (25) NULL,
    [ProfessionalCarerType]            VARCHAR (80) NULL,
    [ProfessionalCarerID]              INT          NULL,
    [ProfessionalCarerMainIdentifier]  VARCHAR (25) NULL,
    [PatientID]                        INT          NULL,
    [SystemPatientIdentifier]          VARCHAR (20) NULL,
    [NHSNumber]                        VARCHAR (20) NULL,
    [HealthOrganisationID]             INT          NULL,
    [HealthOrganisationMainIdentifier] VARCHAR (25) NULL,
    [StartDateTime]                    DATETIME     NULL,
    [EndDateTime]                      DATETIME     NULL,
    [CreatedDateTime]                  DATETIME     NULL,
    [ModifiedDateTime]                 DATETIME     NULL,
    [UserCreate]                       VARCHAR (30) NULL,
    [UserModify]                       VARCHAR (30) NULL,
    [LorenzoCreated]                   DATETIME     NULL,
    [Created]                          DATETIME     NOT NULL,
    [Updated]                          DATETIME     NULL,
    [ByWhom]                           VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Lorenzo_PatientProfessionalCarer] PRIMARY KEY CLUSTERED ([PatientProfessionalCarerID] ASC)
);





GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170103-165934]
    ON [Lorenzo].[PatientProfessionalCarer]([ProfessionalCarerTypeMainCode] ASC, [PatientID] ASC, [StartDateTime] ASC)
    INCLUDE([PatientProfessionalCarerID], [EndDateTime], [HealthOrganisationID], [ProfessionalCarerID]);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170110-103943]
    ON [Lorenzo].[PatientProfessionalCarer]([PatientID] ASC, [Created] ASC, [Updated] ASC);

