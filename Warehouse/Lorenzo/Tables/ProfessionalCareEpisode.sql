﻿CREATE TABLE [Lorenzo].[ProfessionalCareEpisode] (
    [ProfessionalCareEpisodeID]         INT           NOT NULL,
    [WaitingListID]                     INT           NULL,
    [ServicePointStayID]                INT           NULL,
    [ProviderSpellID]                   INT           NULL,
    [ServicePointStayServicePointID]    INT           NULL,
    [ProviderID]                        INT           NULL,
    [SpecialtyID]                       INT           NULL,
    [PurchaserID]                       INT           NULL,
    [SourceOfReferralID]                INT           NULL,
    [EpisodeID]                         INT           NULL,
    [ManagementIntentionID]             INT           NULL,
    [AdministrativeCategoryID]          INT           NULL,
    [EpisodeTypeID]                     INT           NULL,
    [MentalCategoryOnTransferID]        INT           NULL,
    [ContractID]                        INT           NULL,
    [AttendedIndicatorID]               INT           NULL,
    [PatientClassificationID]           INT           NULL,
    [EpisodeOutcomeID]                  INT           NULL,
    [IncidentLocationID]                INT           NULL,
    [PatientGroupID]                    INT           NULL,
    [ReferralID]                        INT           NULL,
    [LegalStatusCategoryID]             INT           NULL,
    [ProfessionalCarerID]               INT           NULL,
    [PatientID]                         INT           NULL,
    [PaymentRulesID]                    INT           NULL,
    [VisitTypeID]                       INT           NULL,
    [StartDateTime]                     DATETIME      NULL,
    [EndDateTime]                       DATETIME      NULL,
    [DNAReasonID]                       INT           NULL,
    [AdmissionMethodID]                 INT           NULL,
    [PriorProfessionalCareEpisodeID]    INT           NULL,
    [SelfPaymentExemptionReasonID]      INT           NULL,
    [Comments]                          VARCHAR (255) NULL,
    [CreatedDateTime]                   DATETIME      NULL,
    [ModifiedDateTime]                  DATETIME      NULL,
    [UserCreate]                        VARCHAR (50)  NULL,
    [UserModify]                        VARCHAR (50)  NULL,
    [EpisodeIdentifier]                 VARCHAR (50)  NULL,
    [SessionTransactionID]              INT           NULL,
    [PriorPointerID]                    INT           NULL,
    [ExternalKey]                       VARCHAR (50)  NULL,
    [CodingCompleteFlag]                CHAR (1)      NULL,
    [AgeBandID]                         INT           NULL,
    [MentalHealthCareEpisodeID]         INT           NULL,
    [HRGCode]                           VARCHAR (50)  NULL,
    [ContractServiceID]                 INT           NULL,
    [ServicePointID]                    INT           NULL,
    [ReallocateFlag]                    CHAR (1)      NULL,
    [DischargeSummaryCompleteFlag]      CHAR (1)      NULL,
    [ProvisionalOnlyIndicatorFlag]      CHAR (1)      NULL,
    [ClinicalCostCentreID]              INT           NULL,
    [AdmissionOfferID]                  INT           NULL,
    [ClinicalTransferReasonID]          INT           NULL,
    [CodingStatusID]                    INT           NULL,
    [CodingAuthorisationFlag]           CHAR (1)      NULL,
    [CodingAuthorisationUser]           VARCHAR (50)  NULL,
    [CodingAuthorisationDateTime]       DATETIME      NULL,
    [LatchedAuthorisationFlag]          CHAR (1)      NULL,
    [InvoiceTag]                        VARCHAR (50)  NULL,
    [ServiceCategoryID]                 INT           NULL,
    [PatientClassificationForBillingID] INT           NULL,
    [ResetDaysFlag]                     CHAR (1)      NULL,
    [PriorDays]                         INT           NULL,
    [ICUHours]                          INT           NULL,
    [CCUHours]                          INT           NULL,
    [MechanicalVentilationHours]        INT           NULL,
    [PalliativeCareStatusID]            INT           NULL,
    [ClinicalCodingID]                  INT           NULL,
    [ActivityAlias]                     CHAR (1)      NULL,
    [DDGVPCode]                         VARCHAR (50)  NULL,
    [WIESCasemixValue]                  INT           NULL,
    [ActivityCode]                      VARCHAR (50)  NULL,
    [ActivityID]                        INT           NULL,
    [HRGVersionCode]                    VARCHAR (50)  NULL,
    [RTTStatusID]                       INT           NULL,
    [HealthOrganisationOwnerID]         INT           NULL,
    [LorenzoCreated]                    DATETIME      NOT NULL,
    [Created]                           DATETIME      NOT NULL,
    [Updated]                           DATETIME      NULL,
    [ByWhom]                            VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Lorenzo_ProfessionalCareEpisode] PRIMARY KEY CLUSTERED ([ProfessionalCareEpisodeID] ASC)
);





GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170104-095259]
    ON [Lorenzo].[ProfessionalCareEpisode]([ProviderSpellID] ASC)
    INCLUDE([WaitingListID], [ProviderID], [SpecialtyID], [EpisodeID], [ProfessionalCarerID], [PatientID], [StartDateTime], [EndDateTime], [ProfessionalCareEpisodeID]);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170110-102641]
    ON [Lorenzo].[ProfessionalCareEpisode]([PatientID] ASC)
    INCLUDE([ProfessionalCareEpisodeID]);

