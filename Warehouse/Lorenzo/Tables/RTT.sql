﻿CREATE TABLE [Lorenzo].[RTT] (
    [RTTID]                     INT           NOT NULL,
    [ReferralID]                INT           NULL,
    [PatientID]                 INT           NULL,
    [SourceCode]                VARCHAR (10)  NULL,
    [SourceID]                  INT           NULL,
    [StatusDateTime]            DATETIME      NULL,
    [StatusID]                  INT           NULL,
    [Comments]                  VARCHAR (255) NULL,
    [CreatedDateTime]           DATETIME      NULL,
    [ModifiedDateTime]          DATETIME      NULL,
    [UserCreate]                VARCHAR (30)  NULL,
    [UserModify]                VARCHAR (30)  NULL,
    [SessionTransactionID]      INT           NULL,
    [ExternalKey]               VARCHAR (20)  NULL,
    [HealthOrganisationOwnerID] INT           NULL,
    [LorenzoCreated]            DATETIME      NULL,
    [IsDeleted]                 BIT           NULL,
    [Created]                   DATETIME      NOT NULL,
    [Updated]                   DATETIME      NULL,
    [ByWhom]                    VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Lorenzo_RTT] PRIMARY KEY CLUSTERED ([RTTID] ASC)
);
GO

CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170104-105034]
    ON [Lorenzo].[RTT]([SourceCode] ASC, [SourceID] ASC)
    INCLUDE([RTTID], [StatusDateTime], [StatusID]);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170104-104234]
    ON [Lorenzo].[RTT]([ReferralID] ASC)
    INCLUDE([RTTID], [StatusDateTime], [StatusID]);

