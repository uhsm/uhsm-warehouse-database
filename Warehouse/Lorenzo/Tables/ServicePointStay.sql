﻿CREATE TABLE [Lorenzo].[ServicePointStay] (
    [ServicePointStayID]              INT          NOT NULL,
    [PatientID]                       INT          NULL,
    [PatientSystemIdentifier]         VARCHAR (20) NULL,
    [NHSNumber]                       VARCHAR (20) NULL,
    [ServicePointID]                  INT          NULL,
    [ServicePointCode]                VARCHAR (25) NULL,
    [ServicePoint]                    VARCHAR (80) NULL,
    [ProviderSpellID]                 INT          NULL,
    [StartDateTime]                   DATETIME     NULL,
    [EndDateTime]                     DATETIME     NULL,
    [CreatedDateTime]                 DATETIME     NULL,
    [ModifiedDateTime]                DATETIME     NULL,
    [UserCreate]                      VARCHAR (30) NULL,
    [UserModify]                      VARCHAR (30) NULL,
    [ProvisionalOnlyIndicatorFlag]    CHAR (1)     NULL,
    [ProfessionalCarerID]             INT          NULL,
    [ProfessionalCarerMainCode]       VARCHAR (25) NULL,
    [BedCategoryID]                   INT          NULL,
    [BedCategoryMainCode]             VARCHAR (25) NULL,
    [BedCategory]                     VARCHAR (80) NULL,
    [RequestedBedCategoryID]          INT          NULL,
    [RequestedBedCategoryMainCode]    VARCHAR (25) NULL,
    [RequestedBedCategory]            VARCHAR (80) NULL,
    [IsChaplainVisitAllowed]          CHAR (1)     NULL,
    [IsVisitorAllowed]                CHAR (1)     NULL,
    [HealthOrganisationOwnerID]       INT          NULL,
    [HealthOrganisationOwnerMainCode] VARCHAR (25) NULL,
    [WardStartDateTime]               DATETIME     NULL,
    [LorenzoCreated]                  DATETIME     NULL,
    [Created]                         DATETIME     NOT NULL,
    [Updated]                         DATETIME     NULL,
    [ByWhom]                          VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Lorenzo_ServicePointStay] PRIMARY KEY CLUSTERED ([ServicePointStayID] ASC)
);





GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170104-094759]
    ON [Lorenzo].[ServicePointStay]([ServicePointStayID] ASC, [ProviderSpellID] ASC, [StartDateTime] ASC)
    INCLUDE([ServicePointID], [HealthOrganisationOwnerID]);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170110-105042]
    ON [Lorenzo].[ServicePointStay]([ProviderSpellID] ASC, [Created] ASC, [Updated] ASC);

