﻿CREATE TABLE [Lorenzo].[TestPatient]
(
	[PatientID] INT NOT NULL,
	[Created] [datetime] NOT NULL Default GetDate(),
	CONSTRAINT [PK_Lorenzo_TestPatient] PRIMARY KEY ([PatientID])
)
