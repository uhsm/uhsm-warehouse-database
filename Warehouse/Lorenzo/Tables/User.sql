﻿CREATE TABLE [Lorenzo].[User]
(
	[UserID] [int] NOT NULL,
	[ProfessionalCarerID]  [int] NULL,
	[UserCode] [varchar](20) NULL,
	[Username] [varchar](35) NULL,
	[Department] [varchar](80) NULL,
	[Email] [varchar](80) NULL,
	[HasEvents] [char](1) NULL,
	[UserCreate] [varchar](30) NULL,
	[UserModify] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL,
	[ExternalKey] [varchar](20) NULL,
	[SessionTransactionID]  [int] NULL,
	[AlterDateTime] [datetime] NULL,
	[LastLogonDateTime] [datetime] NULL,
	[IsLoggedOn] [int] NULL,
	[IsLocked] [char](1) NULL,
	[LockedComments] [varchar](255) NULL,
	[ChangePasswordFlag] [char](1) NULL,
	[AccessLevelsID]  [int] NULL,
	[IsNHSUser] [char](1) NULL,
	[HealthOrganisationOwnerID]  [int] NULL,
	[LorenzoCreated] [datetime] NULL,

	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL,
	  CONSTRAINT [PK_Lorenzo_User] PRIMARY KEY ([UserID])

)
