﻿CREATE TABLE [Lorenzo].[WaitingListRule]
(
		[WaitingListRuleID] INT NOT NULL,
	[SpecialtyID] INT NULL,
	[ProfessionalCarerID] INT NULL,
	[HealthOrganisationID] INT NULL,
	[MaximumWaitDayCaseAdmission] INT NULL,
	[MaximumWaitOrdinaryAdmission] INT NULL,
	[CreatedDateTime] DATETIME NULL,
	[ModifiedDateTime] DATETIME NULL,
	[UserCreate] VARCHAR(50) NULL,
	[UserModify] VARCHAR(50) NULL,
	[WaitingListRuleCode] VARCHAR(50) NULL,
	[WaitingListRule] VARCHAR(100) NULL,
	[Comments] VARCHAR(255) NULL,
	[SessionTransactionID] INT NULL,
	[PriorPointerID] INT NULL,
	[ExternalKey] VARCHAR(50) NULL,
	[ServiceTypeID] INT NULL,
	[ServicePointID] INT NULL,
	[IsLocalType] CHAR(1) NULL,
	[EndDateTime] DATETIME NULL,
	[StartDateTime] DATETIME NULL,
	[IsDiagnosticWait] CHAR(1) NULL,
	[HealthOrganisationOwnerID] INT NULL,
	[LorenzoCreated] DATETIME NULL,


	[Created]  DATETIME      NOT NULL,
    [Updated] DATETIME      NULL,
    [ByWhom]  VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Lorenzo_WaitingListRule] PRIMARY KEY CLUSTERED ([WaitingListRuleID] ASC)
)
