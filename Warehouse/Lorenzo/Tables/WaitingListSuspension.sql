﻿CREATE TABLE [Lorenzo].[WaitingListSuspension]
(
	[WaitingListSuspensionID] INT NOT NULL,
	[WaitingListID] INT NULL,
	[SuspensionReasonID] INT NULL,
	[SuspensionReasonMainCode] VARCHAR(25) NULL,
	[SuspensionReasonDescription] VARCHAR(80) NULL,
	[StartDateTime] DATETIME NULL,
	[EndDateTime] DATETIME NULL,
	[Comments] VARCHAR(255) NULL,
	[CreatedDateTime] DATETIME NULL,
	[ModifyDateTime] DATETIME NULL,
	[UserCreate] VARCHAR(30) NULL,
	[UserModify] VARCHAR(30) NULL,
	[ReviewDateTime] DATETIME NULL,
	[HealthOrganisationOwnerID] INT NULL,
	[HealthOrganisationOwnerMainCode] VARCHAR(25) NULL,
	[IsEnd] CHAR(1) NULL,
	[IsStart] CHAR(1) NULL,
	[LorenzoCreated] DATETIME NULL,

	[Created]  DATETIME      NOT NULL,
    [Updated] DATETIME      NULL,
    [ByWhom]  VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Lorenzo_WaitingListSuspension] PRIMARY KEY CLUSTERED ([WaitingListSuspensionID] ASC)
)
