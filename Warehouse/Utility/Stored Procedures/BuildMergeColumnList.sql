﻿create procedure Utility.BuildMergeColumnList
	 @table varchar(max)
	,@schema varchar(max) = 'dbo'
as

select
	SelectColumn =
		case
		when columns.column_id = 1
		then ' '
		else ','
		end + columns.name

	,CastColumn =
		case
		when columns.column_id = 1
		then '' + columns.name
		else ',' + columns.name + ' = cast(' + 
			case
			when columns.system_type_id in (35, 99, 167, 175, 231, 239) --string

			then 'nullif(' + columns.name + ', '''')'
			else columns.name
			end + ' as ' + systypes.name + 

			case
			when columns.collation_name is null
			then ''
			else '(' + cast(columns.max_length as varchar) + ')'
			end + ')'
		end

	,SourceColumn =
		case
		when columns.column_id = 1
		then ' source.'
		else ',source.'
		end + columns.name

	,TargetColumn =
		case
		when columns.column_id = 1
		then ' target.'
		else ',target.'
		end + columns.name

	,IsNullColumn =
		case
		when columns.column_id = 1
		then ' '
		else 'and'
		end +
		' isnull(target.' + columns.name + ', ' +
		case

		when columns.system_type_id in (40, 58, 61) --date
		then 'getdate()'

		when columns.system_type_id in (35, 99, 167, 175, 231, 239) --string
		then ''''''

		else '0'
		end +
		') = isnull(source.' + columns.name + ', ' +
		case

		when columns.system_type_id in (40, 58, 61) --date
		then 'getdate()'

		when columns.system_type_id in (35, 99, 167, 175, 231, 239) --string
		then ''''''

		else '0'
		end +
		')'
	
	,UpdateColumn =
		case
		when columns.column_id = 1
		then ' '
		else ','
		end +
		'target.' + columns.name + ' = ' +
		'source.' + columns.name

	,MaxLengthColumn =
		case
		when columns.column_id = 1
		then ' ' + columns.name
		else ',' + columns.name
		end + ' = max(len(' + columns.name + '))'


	,SpaceToNullColumn =
		case
		when columns.column_id = 1
		then ' ' + columns.name
		else ',' + columns.name 
		end + ' = nullif(' + columns.name + ', '''')'

	,*
from
	sys.columns

left join sys.tables
on	tables.object_id = columns.object_id

left join sys.views views
on	views.object_id = columns.object_id

inner join sys.systypes
on	systypes.xtype = columns.system_type_id

inner join sys.schemas
on	schemas.schema_id =
		coalesce(
			 tables.schema_id
			,views.schema_id
		)

where
	systypes.name <> 'sysname'

and	coalesce(
		tables.name
		,views.name
	) = @table

and	schemas.name = @schema

order by
	columns.column_id
