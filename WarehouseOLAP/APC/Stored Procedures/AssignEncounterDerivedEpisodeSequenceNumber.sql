﻿CREATE procedure [APC].AssignEncounterDerivedEpisodeSequenceNumber as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare @ProcessList table
	(
		 MergeRecno int
		,Action nvarchar(10)
	)

update APC.Encounter
set
	EpisodeSequenceNumber = APCEncounter.EpisodeSequenceNumber

output
	 coalesce(inserted.MergeRecno, deleted.MergeRecno)
	,'UPDATE'
into
	@ProcessList

from
	(
	select
		 Encounter.MergeRecno
		,EpisodeSequenceNumber = row_number() over (partition by SpellID order by SpellID, EpisodeStartTime)
	from
		APC.Encounter
	where
	--only check those episodes that belong to encounters for a patient that have changed
		exists
		(
		select
			1
		from
			APC.Encounter EncounterChanges

		inner join APC.EncounterProcessList ProcessList
		on	ProcessList.MergeRecno = EncounterChanges.MergeRecno

		where
			EncounterChanges.SpellID = Encounter.SpellID
		)
	) APCEncounter

inner join APC.Encounter
on	Encounter.MergeRecno = APCEncounter.MergeRecno

where
	Encounter.EpisodeSequenceNumber <> APCEncounter.EpisodeSequenceNumber
or	Encounter.EpisodeSequenceNumber is null


--Merge ProcessList
merge
	APC.EncounterProcessList target
using
	(
	select
		 MergeRecno
		,Action
	from
		@ProcessList
	) source
	on	source.MergeRecno = target.MergeRecno

when not matched
then
	insert
		(
		 MergeRecno
		,Action
		)
	values
		(
		 source.MergeRecno
		,source.Action
		)
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
	,@StartTime
