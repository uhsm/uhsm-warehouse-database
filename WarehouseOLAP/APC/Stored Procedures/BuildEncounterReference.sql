﻿CREATE procedure [APC].[BuildEncounterReference] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @ProcessList table
	(
		Action nvarchar(10)
	)

--select
--	ProcessList.MergeRecno
--	,ProcessList.Action
--	,Encounter.EncounterRecno
--	,Encounter.ContextCode
--	,AdmissionCategoryID = AdministrativeCategory.SourceID
--	,AdmissionMethodID = AdmissionMethod.SourceID
--	,AdmissionSourceID = AdmissionSource.SourceID
--	,ConsultantID = Consultant.SourceID
--	,Context.ContextID
--	,DischargeDestinationID = DischargeDestination.SourceID
--	,DischargeMethodID = DischargeMethod.SourceID
--	,EthnicCategoryID = EthnicCategory.SourceID
--	,PracticeID = RegisteredPractice.SourceID
--	,SexID = Sex.SourceID
--	,CurrentSiteID = CurrSite.SourceID
--	,StartSiteID = StartSite.SourceID
--	,EndSiteID = EndSite.SourceID
--	,TreatmentFunctionID = TreatmentFunction.SourceID
--	,ManagementIntentionID = ManagementIntention.SourceID
--	,OverseasVisitorStatusID = OverseasVisitorStatus.SourceID
--	,RTTPeriodStatusID = RTTPeriodStatus.SourceID
--	,RTTCurrentStatusID = RTTCurrentStatus.SourceID
--	,RTTCurrentProviderID = RTTCurrentProvider.SourceID
--	,StartWardID = StartWard.SourceID
--	,EndWardID = EndWard.SourceID
--into
--	#APCEncounterReference
--from
--	APC.EncounterProcessList ProcessList

--left join APC.Encounter Encounter
--on	Encounter.MergeRecno = ProcessList.MergeRecno		

--left join WH.AdministrativeCategory
--on	AdministrativeCategory.SourceCode = isnull(cast(Encounter.AdmissionCategoryID as varchar), '-1')
--and	AdministrativeCategory.SourceContextCode = Encounter.ContextCode

--left join WH.AdmissionMethod AdmissionMethod
--on	AdmissionMethod.SourceCode =  isnull(cast(Encounter.AdmissionMethodCode as varchar), '-1')
--and	AdmissionMethod.SourceContextCode = Encounter.ContextCode

--left join WH.AdmissionSource AdmissionSource
--on	AdmissionSource.SourceCode = isnull(cast(Encounter.AdmissionSourceCode as varchar), '-1')
--and	AdmissionSource.SourceContextCode = Encounter.ContextCode

--left join WH.CareProfessional Consultant
--on	Consultant.SourceCode = isnull(cast(Encounter.ConsultantCode as varchar), '-1')
--and	Consultant.SourceContextCode = Encounter.ContextCode

--left join WH.DischargeDestination DischargeDestination
--on	DischargeDestination.SourceCode = isnull(cast(Encounter.DischargeDestinationCode as varchar), '-1')
--and	DischargeDestination.SourceContextCode = Encounter.ContextCode

--left join WH.DischargeMethod DischargeMethod
--on	DischargeMethod.SourceCode = isnull(cast(Encounter.DischargeMethodCode as varchar), '-1')
--and	DischargeMethod.SourceContextCode = Encounter.ContextCode

--left join WH.Practice RegisteredPractice
--on	RegisteredPractice.SourceCode = isnull(cast(Encounter.RegisteredGpPracticeCode as varchar), '-1')
--and	RegisteredPractice.SourceContextCode = Encounter.ContextCode

--left join ReferenceMap.Map.Context
--on	Context.ContextCode = Encounter.ContextCode

--left join WH.Sex Sex
--on	Sex.SourceCode = isnull(cast(Encounter.SexID as varchar), '-1')
--and	Sex.SourceContextCode = Encounter.ContextCode

--left join WH.Site CurrSite
--on	CurrSite.SourceCode = isnull(cast(Encounter.CurrentSiteCode as varchar), '-1')
--and	CurrSite.SourceContextCode = Encounter.ContextCode

--left join WH.Site StartSite
--on	StartSite.SourceCode = isnull(cast(Encounter.StartSiteCode as varchar), '-1')
--and	StartSite.SourceContextCode = Encounter.ContextCode

--left join WH.Site EndSite
--on	EndSite.SourceCode = isnull(cast(Encounter.EndSiteCode as varchar), '-1')
--and	EndSite.SourceContextCode = Encounter.ContextCode

--left join WH.Specialty TreatmentFunction
--on	TreatmentFunction.SourceCode = isnull(cast(Encounter.TreatmentFunctionCode as varchar), '-1')
--and	TreatmentFunction.SourceContextCode = Encounter.ContextCode

--left join WH.EthnicCategory
--on	EthnicCategory.SourceCode = isnull(cast(Encounter.EthnicCategoryCode as varchar), '-1')
--and	EthnicCategory.SourceContextCode = Encounter.ContextCode

--left join WH.ManagementIntention
--on	ManagementIntention.SourceCode = isnull(cast(Encounter.ManagementIntentionID as varchar), '-1')
--and	ManagementIntention.SourceContextCode = Encounter.ContextCode

--left join WH.OverseasVisitorStatus
--on	OverseasVisitorStatus.SourceCode = isnull(cast(Encounter.OverseasVisitorStatusID as varchar), '-1')
--and	OverseasVisitorStatus.SourceContextCode = Encounter.ContextCode
	
--left join WH.RTTStatus RTTPeriodStatus
--on	RTTPeriodStatus.SourceCode = isnull(cast(Encounter.RTTPeriodStatusCode as varchar), '-1')
--and	RTTPeriodStatus.SourceContextCode = Encounter.ContextCode
	
--left join WH.RTTStatus RTTCurrentStatus
--on	RTTCurrentStatus.SourceCode = isnull(cast(Encounter.RTTCurrentStatusCode as varchar), '-1')
--and	RTTCurrentStatus.SourceContextCode = Encounter.ContextCode
	
--left join WH.Provider RTTCurrentProvider
--on	RTTCurrentProvider.SourceCode = isnull(cast(Encounter.RTTCurrentProviderCode as varchar), '-1')
--and	RTTCurrentProvider.SourceContextCode = Encounter.ContextCode

--left join WH.Ward StartWard
--on StartWard.SourceCode = isnull(Encounter.StartWardCode,'-1')
--and StartWard.SourceContextCode = Encounter.ContextCode

--left join WH.Ward EndWard
--on EndWard.SourceCode = isnull(Encounter.EndWardCode,'-1')
--and EndWard.SourceContextCode = Encounter.ContextCode

--ALTER TABLE #APCEncounterReference ADD  CONSTRAINT [PK_#APCEncounterReference] PRIMARY KEY CLUSTERED 
--(
--	MergeRecno
--)

--merge
--	APC.EncounterReference target
--using
--	(
--	select
--		*
--	from
--		#APCEncounterReference
--	) source
--on	source.MergeRecno = target.MergeRecno

--when matched
--and	source.Action = 'DELETE'
--then
--	delete

--when not matched
--then
--	insert
--		(
--		 MergeRecno
--		,EncounterRecno
--		,ContextCode
--		,AdmissionCategoryID
--		,AdmissionMethodID
--		,AdmissionSourceID
--		,ConsultantID
--		,ContextID
--		,DischargeDestinationID
--		,DischargeMethodID
--		,EthnicCategoryID
--		,PracticeID
--		,SexID
--		,CurrentSiteID
--		,StartSiteID
--		,EndSiteID
--		,TreatmentFunctionID
--		,ManagementIntentionID
--		,OverseasVisitorStatusID
--		,RTTPeriodStatusID
--		,RTTCurrentStatusID
--		,RTTCurrentProviderID
--		,StartWardID
--		,EndWardID
--		,Created
--		,ByWhom
--		)
--	values
--		(
--		 source.MergeRecno
--		,source.EncounterRecno
--		,source.ContextCode
--		,source.AdmissionCategoryID
--		,source.AdmissionMethodID
--		,source.AdmissionSourceID
--		,source.ConsultantID
--		,source.ContextID
--		,source.DischargeDestinationID
--		,source.DischargeMethodID
--		,source.EthnicCategoryID
--		,source.PracticeID
--		,source.SexID
--		,source.CurrentSiteID
--		,source.StartSiteID
--		,source.EndSiteID
--		,source.TreatmentFunctionID
--		,source.ManagementIntentionID
--		,source.OverseasVisitorStatusID
--		,source.RTTPeriodStatusID
--		,source.RTTCurrentStatusID
--		,source.RTTCurrentProviderID
--		,source.StartWardID
--		,source.EndWardID
--		,getdate()
--		,suser_name()
--		)
			
--when matched
--and	source.Action <> 'DELETE'
--then
--	update
--	set
--		target.EncounterRecno = source.EncounterRecno
--		,target.ContextCode = source.ContextCode
--		,target.AdmissionCategoryID = source.AdmissionCategoryID
--		,target.AdmissionMethodID = source.AdmissionMethodID
--		,target.AdmissionSourceID = source.AdmissionSourceID
--		,target.ConsultantID = source.ConsultantID
--		,target.ContextID = source.ContextID
--		,target.DischargeDestinationID = source.DischargeDestinationID
--		,target.DischargeMethodID = source.DischargeMethodID
--		,target.EthnicCategoryID = source.EthnicCategoryID
--		,target.PracticeID = source.PracticeID
--		,target.SexID = source.SexID
--		,target.CurrentSiteID = source.CurrentSiteID
--		,target.StartSiteID = source.StartSiteID
--		,target.EndSiteID = source.EndSiteID
--		,target.TreatmentFunctionID = source.TreatmentFunctionID
--		,target.ManagementIntentionID = source.ManagementIntentionID
--		,target.OverseasVisitorStatusID = source.OverseasVisitorStatusID
--		,target.RTTPeriodStatusID = source.RTTPeriodStatusID
--		,target.RTTCurrentStatusID = source.RTTCurrentStatusID
--		,target.RTTCurrentProviderID = source.RTTCurrentProviderID
--		,target.StartWardID = source.StartWardID
--		,target.EndWardID = source.EndWardID
--		,target.Updated = getdate()
--		,target.ByWhom = suser_name()

--output
--	$action into @ProcessList
--;


--select
--	 @inserted = sum(Inserted)
--	,@updated = sum(Updated)
--	,@deleted = sum(Deleted)
--from
--	(
--	select
--		 Inserted = case when Action = 'INSERT' then 1 else 0 end
--		,Updated = case when Action = 'UPDATE' then 1 else 0 end
--		,Deleted = case when Action = 'DELETE' then 1 else 0 end
--	from
--		@ProcessList
--	) MergeSummary


--select
--	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


--select
--	@Stats =
--		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
--		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
--		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
--		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


--exec Audit.WriteLogEvent
--	 @ProcedureName
--	,@Stats