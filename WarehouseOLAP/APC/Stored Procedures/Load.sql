﻿CREATE procedure [APC].Load as

/**********************************************************************************************************************************
Aim: Control procedure to import APC data into the OLAP model

Notes: This processes each context in turn then runs some dataset level processes.

**********************************************************************************************************************************/

declare @ReturnValue int

--Load UHSM PAS
exec @ReturnValue = UHSMLorenzo.LoadAPC

if @ReturnValue = 0
BEGIN TRY

	declare @Process varchar(255) =  OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

	declare @LastLoadTime datetime =
		Utility.GetLastLoadTime(@Process);

	if @LastLoadTime is null
		set @LastLoadTime = '1 jan 1900' --default to start of time



	--rebuild the process list with the latest changes
	truncate table APC.EncounterProcessList


	insert
	into
		APC.EncounterProcessList
		(
		MergeRecno
		,Action
		)
	select
		Activity.MergeRecno
		,Action = case when Activity.Updated is null then 'INSERT' else 'UPDATE' end
	from
		APC.Encounter Activity
	where
		isnull(Activity.Updated, Activity.Created) > @LastLoadTime

	or	not exists
		(
		select
			1
		from
			APC.EncounterReference
		where
			EncounterReference.MergeRecno = Activity.MergeRecno
		)

	union all

	select
		Activity.MergeRecno
		,Action = 'DELETE'
	from
		APC.EncounterReference Activity
	where
		not exists
		(
		select
			1
		from
			APC.Encounter
		where
			Encounter.MergeRecno = Activity.MergeRecno
		)

	--Further derivations
	exec APC.AssignEncounterDerivedEpisodeSequenceNumber


	--generate missing reference data
	declare
		@ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[APC].[Encounter]'
		,@ProcessListTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[APC].[EncounterProcessList]'
		,@NewRows int = 0
		,@AllRows int = 0


	declare	@ActivityTableOverride varchar(max) = 
		'(
		select
			Encounter.*
		from
			' + @ActivityTable + ' Encounter

		inner join ' + @ProcessListTable + ' ProcessList
		on	ProcessList.MergeRecno = Encounter.MergeRecno
		)'

	
	--generate reference data
	exec Utility.BuildReferenceDataMap @ActivityTable, @NewRows output,@Debug = 0 ,@ActivityTableOverride = @ActivityTableOverride

	select
		@AllRows = @AllRows + isnull(@NewRows, 0)


	if @AllRows > 0
		exec Utility.BuildMember


	--Build the BaseReference table
	exec APC.BuildEncounterReference

	--Allocate Business Units
	--exec Allocation.Allocation.AllocateAPC

	--Update the last load time
	set @LastLoadTime = getdate()
	exec Utility.SetLastLoadTime @Process, @LastLoadTime


END TRY

BEGIN
	CATCH
	
	set @ReturnValue = @@ERROR

	DECLARE @ErrorSeverity INT,
			@ErrorNumber   INT,
			@ErrorMessage nvarchar(4000),
			@ErrorState INT,
			@ErrorLine  INT,
			@ErrorProc nvarchar(200)
			-- Grab error information from SQL functions
	SET @ErrorSeverity = ERROR_SEVERITY()
	SET @ErrorNumber   = ERROR_NUMBER()
	SET @ErrorMessage  = ERROR_MESSAGE()
	SET @ErrorState    = ERROR_STATE()
	SET @ErrorLine     = ERROR_LINE()
	SET @ErrorProc     = ERROR_PROCEDURE()
	SET @ErrorMessage  = 'An error occurred.' + CHAR(13) + 'SQL Server Error Message is: ' + CAST(@ErrorNumber AS VARCHAR(10)) + ' in procedure: ' + @ErrorProc + ' Line: ' + CAST(@ErrorLine AS VARCHAR(10)) + ' Error text: ' + @ErrorMessage
	-- Not all errors generate an error state, to set to 1 if it's zero
	IF @ErrorState  = 0
	SET @ErrorState = 1
	-- If the error renders the transaction as uncommittable or we have open transactions, we may want to rollback
	IF @@TRANCOUNT > 0
	BEGIN
			--print 'Rollback transaction'
			ROLLBACK TRANSACTION
	END
	RAISERROR (@ErrorMessage , @ErrorSeverity, @ErrorState, @ErrorNumber)
END CATCH

RETURN @ReturnValue
