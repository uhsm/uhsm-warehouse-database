﻿CREATE TABLE [APC].[Encounter] (
	MergeRecno INT IDENTITY (1, 1) NOT NULL
	,EncounterRecno INT NOT NULL
	
	,ContextCode VARCHAR (20) NOT NULL
	,AdmittedCareID VARCHAR (60) NULL
	,SpellID VARCHAR (16) NULL
	,PatientID INT NULL
	,EpisodeNumber INT NULL
	,PatientTitle VARCHAR (10) NULL
	,PatientForename VARCHAR (50) NULL
	,PatientSurname VARCHAR (50) NULL
	,DateOfBirth DATE NULL
	,DateOfDeath DATE NULL
	,SexID VARCHAR(20) NULL
	,NHSNumber VARCHAR (10) NULL
	,NHSNumberStatusCode VARCHAR (20) NULL
	,DistrictNumber VARCHAR (20) NULL
	,CasenoteNumber VARCHAR (14) NULL
	,PatientAddressLine1 VARCHAR (80) NULL
	,PatientAddressLine2 VARCHAR (80) NULL
	,PatientAddressLine3 VARCHAR (50) NULL
	,PatientAddressLine4 VARCHAR (50) NULL
	,PatientPostcode VARCHAR (8) NULL
	,CarerSupportIndicator bit NULL		 
	,ReferredByCode VARCHAR (20) NULL
	,ReferrerCode VARCHAR (20) NULL
	,ReferrerOrganisationCode VARCHAR (20) NULL
	,RegisteredGpCode VARCHAR (20) NULL
	,RegisteredGpPracticeCode VARCHAR (20) NULL
	,ReligionCode VARCHAR (20) NULL
	,MaritalStatusCode VARCHAR (20) NULL
	,EthnicCategoryCode VARCHAR (20) NULL
	,DateOnWaitingList DATE NULL
	,ManagementIntentionID VARCHAR (20) NULL
	,PatientClassificationID VARCHAR (20) NULL
	,IsMRSA BIT NULL
	,NeonatalLevelOfCare VARCHAR (20) NULL
	,IsWellBabyFlag INT NULL
	,AdmissionMethodCode VARCHAR (20) NULL
	,AdmissionSourceCode VARCHAR (20) NULL
	,AdministrativeCategoryID VARCHAR (20) NULL
	,AdmissionTime DATETIME NULL
	,CancelledElectiveAdmissionFlag BIT NULL
	,ConsultantCode VARCHAR (20) NULL
	,TreatmentFunctionCode VARCHAR (20) NULL
	,EpisodeSequenceNumber SMALLINT NULL
	,ProviderCode VARCHAR (20) NULL
	,IsFirstEpisodeInSpell BIT NULL
	,EpisodeStartTime DATETIME NULL
	,StartSiteCode VARCHAR (20) NULL
	,StartWardCode VARCHAR (20) NULL
	,EpisodeEndTime DATETIME NULL
	,EndSiteCode VARCHAR (20) NULL
	,EndWardCode VARCHAR (20) NULL
	,IsLastEpisodeInSpell BIT NULL
	,LOE INT NULL
	,LOS INT NULL
	,ExpectedLOS SMALLINT NULL
	,DischargeDestinationCode VARCHAR (20) NULL
	,DischargeMethodCode VARCHAR (20) NULL
	,DischargeTime DATETIME NULL
	,ClinicalCodingCompleteTime DATETIME NULL
	,ClinicalCodingStatusCode VARCHAR (1) NULL
	,PASHRGCode VARCHAR (5) NULL
	,OperationStatusID TINYINT NULL
	,PrimaryDiagnosisCode VARCHAR (20) NULL
	,SubsidiaryDiagnosisCode VARCHAR (20) NULL
	,IntendedPrimaryProcedureCode VARCHAR (20) NULL
	,PrimaryOperationCode VARCHAR (20) NULL
	,PrimaryOperationDate DATE NULL
	,RTTPathwayID VARCHAR (27) NULL
	,RTTPathwayCondition VARCHAR (20) NULL
	,RTTStartTime DATETIME NULL
	,RTTEndTime DATETIME NULL
	,RTTSpecialtyCode VARCHAR (4) NULL
	,RTTCurrentProviderCode VARCHAR (20) NULL
	,RTTCurrentStatusCode VARCHAR (20) NULL
	,RTTCurrentStatusTime DATETIME NULL
	,RTTCurrentPrivatePatientFlag bit NULL
	,RTTOverseasStatusFlag bit NULL
	,RTTPeriodStatusCode VARCHAR (20) NULL
	,TransferFrom VARCHAR (14) NULL
	,IsDiagnosticProcedure BIT NULL
	,OverseasVisitorStatusID VARCHAR (20) NULL 
	,IsFirstRegularAdmission bit NULL
	,EpisodeOutcomeCode VARCHAR (20) NULL
	,LegalStatusClassificationOnAdmissionCode VARCHAR (20) NULL 
	,LegalStatusClassificationOnDischargeCode VARCHAR (20) NULL 
	,DecisionToAdmitDateTime datetime
	,OriginalDecisionToAdmitDateTime datetime
	,DurationOfElectiveWait int

	,Created datetime NOT NULL DEFAULT getdate()
	,Updated datetime NULL
	,ByWhom varchar(50) NOT NULL DEFAULT suser_name()
CONSTRAINT [PK_APCBase] PRIMARY KEY CLUSTERED ([ContextCode] ASC, [EncounterRecno] ASC)
);



GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_APC_Encounter_01]
    ON [APC].[Encounter]([MergeRecno] ASC);


GO


CREATE NONCLUSTERED INDEX [IX_APC_Encounter_02]
ON [APC].[Encounter] ([ContextCode],[SpellID],[EpisodeSequenceNumber],[EpisodeStartTime],[EpisodeEndTime])

GO

CREATE NONCLUSTERED INDEX [IX_APC_Encounter_03]
ON [APC].[Encounter] ([ContextCode],[PatientID],[EpisodeNumber],[IsFirstRegularAdmission])
INCLUDE ([EpisodeStartTime], MergeRecno)

GO

CREATE INDEX [IX_APC_Encounter_NetChange] ON [APC].[Encounter] (Created, Updated)
GO

CREATE NONCLUSTERED INDEX [IX_APC_Encounter_04]
ON [APC].[Encounter] ([NHSNumber])
INCLUDE ([MergeRecno],[EpisodeStartTime],[EpisodeEndTime])
GO
