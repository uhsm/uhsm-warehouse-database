﻿CREATE TABLE [APC].[EncounterProcessList] (
    [MergeRecno] INT           NOT NULL,
    [Action]     NVARCHAR (10) NOT NULL,
    CONSTRAINT [PK_APCEncounterProcessList] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);
