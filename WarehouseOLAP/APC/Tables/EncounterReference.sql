﻿CREATE TABLE [APC].[EncounterReference] (
    [MergeRecno]              INT          NOT NULL,
    [EncounterRecno]                 INT          NOT NULL,
    [ContextCode]                    VARCHAR (20) NOT NULL,
    [ContextID]               INT          NOT NULL,
    [AdmissionCategoryID]     INT          NOT NULL,
    [AdmissionMethodID]       INT          NOT NULL,
    [AdmissionSourceID]       INT          NOT NULL,
    [ConsultantID]            INT          NOT NULL,
    [DischargeDestinationID]  INT          NOT NULL,
    [DischargeMethodID]       INT          NOT NULL,
    [EthnicCategoryID]        INT          NOT NULL,
    [OverseasVisitorStatusID] INT          NOT NULL,
    [PracticeID]              INT          NOT NULL,
    [SexID]                   INT          NOT NULL,
    [CurrentSiteID]           INT          NOT NULL,
    [StartSiteID]             INT          NOT NULL,
    [EndSiteID]               INT          NOT NULL,
    [TreatmentFunctionID]     INT          NOT NULL,
    [ManagementIntentionID]   INT		   NOT NULL, 
    [RTTPeriodStatusID]       INT          NOT NULL,
    [RTTCurrentStatusID]      INT          NOT NULL,
    [RTTCurrentProviderID]    INT          NOT NULL,
	[StartWardID]             INT          NOT NULL,
    [EndWardID]               INT          NOT NULL,


   	[Created] [datetime] NOT NULL DEFAULT getdate(),
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL DEFAULT suser_name(),
    CONSTRAINT [PK_APC_EncounterReference] PRIMARY KEY CLUSTERED ([EncounterRecno], [ContextCode])
);

GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_APC_EncounterReference_01]
    ON [APC].[EncounterReference]([MergeRecno] ASC);


GO

