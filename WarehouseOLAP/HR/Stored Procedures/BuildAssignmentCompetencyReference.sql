﻿CREATE procedure [HR].[BuildAssignmentCompetencyReference] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @ProcessList table
	(
		Action nvarchar(10)
	)


merge
	HR.AssignmentCompetencyReference target
using
	(
	select
		Encounter.MergeRecno
		,Encounter.EncounterRecno
		,Encounter.ContextCode
		,Context.ContextID
		,CompetencyID = Competency.SourceID
	from
		HR.AssignmentCompetency Encounter

	inner join ReferenceMap.Map.Context
	on	Context.ContextCode = Encounter.ContextCode

	left join HR.Competency
	on	Competency.SourceCode = isnull(cast(Encounter.CompetencyCode as varchar(255)), '-1')
	and	Competency.SourceContextCode = Encounter.ContextCode
	) source
on	source.MergeRecno = target.MergeRecno

when not matched by source
then
	delete

when not matched
then
	insert
		(
		 MergeRecno
		,EncounterRecno
		,ContextCode
		,ContextID
		,CompetencyID
		,Created
		,ByWhom
		)
	values
		(
		 source.MergeRecno
		,source.EncounterRecno
		,source.ContextCode
		,source.ContextID
		,source.CompetencyID
		,getdate()
		,suser_name()
		)
			
when matched
and
	checksum(
		source.EncounterRecno
		,source.ContextCode
		,source.ContextID
		,source.CompetencyID
	)
	<>
	checksum(
		target.EncounterRecno
		,target.ContextCode
		,target.ContextID
		,target.CompetencyID
	)
then
	update
	set
		target.EncounterRecno = source.EncounterRecno
		,target.ContextCode = source.ContextCode
		,target.ContextID = source.ContextID
		,target.CompetencyID = source.CompetencyID
		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action into @ProcessList
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec Audit.WriteLogEvent
	 @ProcedureName
	,@Stats