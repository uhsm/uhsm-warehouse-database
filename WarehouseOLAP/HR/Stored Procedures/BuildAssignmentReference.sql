﻿CREATE procedure [HR].[BuildAssignmentReference] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @ProcessList table
	(
		Action nvarchar(10)
	)


merge
	HR.AssignmentReference target
using
	(
	select
		Encounter.MergeRecno
		,Encounter.EncounterRecno
		,Encounter.ContextCode
		,Context.ContextID
		,DivisionID = Division.SourceID
		,DirectorateID = Directorate.SourceID
		,ServiceID = Service.SourceID
		,LocationID = Location.SourceID
		,StaffGroupID = StaffGroup.SourceID
		,RoleID = Role.SourceID
		,PositionID = Position.SourceID
		,AssignmentCategoryID = AssignmentCategory.SourceID
		,EmployeeCategoryID = EmployeeCategory.SourceID
		,AssignmentStatusID = AssignmentStatus.SourceID
		,PayrollID = Payroll.SourceID
		,BankPostsHeldID = BankPostsHeld.SourceID
		
		,SubDirectorateID =
			case
			when Service.LocalID is not null
			then Service.SourceID

			else Directorate.SourceID

			end

	from
		HR.Assignment Encounter

	inner join ReferenceMap.Map.Context
	on	Context.ContextCode = Encounter.ContextCode

	left join WH.Division
	on	Division.SourceCode = isnull(cast(Encounter.DivisionCode as varchar(255)), '-1')
	and	Division.SourceContextCode = Encounter.ContextCode

	left join WH.Directorate
	on	Directorate.SourceCode = isnull(cast(Encounter.DirectorateCode as varchar(255)), '-1')
	and	Directorate.SourceContextCode = Encounter.ContextCode

	left join WH.Service
	on	Service.SourceCode = isnull(cast(Encounter.ServiceCode as varchar(255)), '-1')
	and	Service.SourceContextCode = Encounter.ContextCode

	left join WH.Location
	on	Location.SourceCode = isnull(cast(Encounter.LocationCode as varchar(255)), '-1')
	and	Location.SourceContextCode = Encounter.ContextCode

	left join HR.StaffGroup
	on	StaffGroup.SourceCode = isnull(cast(Encounter.StaffGroupCode as varchar(255)), '-1')
	and	StaffGroup.SourceContextCode = Encounter.ContextCode

	left join HR.Role
	on	Role.SourceCode = isnull(cast(Encounter.RoleCode as varchar(255)), '-1')
	and	Role.SourceContextCode = Encounter.ContextCode

	left join HR.Position
	on	Position.SourceCode = isnull(cast(Encounter.PositionCode as varchar(255)), '-1')
	and	Position.SourceContextCode = Encounter.ContextCode

	left join HR.AssignmentCategory
	on	AssignmentCategory.SourceCode = isnull(cast(Encounter.AssignmentCategoryCode as varchar(255)), '-1')
	and	AssignmentCategory.SourceContextCode = Encounter.ContextCode

	left join HR.EmployeeCategory
	on	EmployeeCategory.SourceCode = isnull(cast(Encounter.EmployeeCategoryCode as varchar(255)), '-1')
	and	EmployeeCategory.SourceContextCode = Encounter.ContextCode

	left join HR.AssignmentStatus
	on	AssignmentStatus.SourceCode = isnull(cast(Encounter.AssignmentStatusCode as varchar(255)), '-1')
	and	Division.SourceContextCode = Encounter.ContextCode

	left join HR.Payroll
	on	Payroll.SourceCode = isnull(cast(Encounter.PayrollCode as varchar(255)), '-1')
	and	Payroll.SourceContextCode = Encounter.ContextCode

	left join HR.BankPostsHeld
	on	BankPostsHeld.SourceCode = isnull(cast(Encounter.BankPostsHeldCode as varchar(255)), '-1')
	and	BankPostsHeld.SourceContextCode = Encounter.ContextCode

	) source
on	source.MergeRecno = target.MergeRecno

when not matched by source
then
	delete

when not matched
then
	insert
		(
		MergeRecno
		,EncounterRecno
		,ContextCode
		,ContextID
		,DivisionID
		,DirectorateID
		,SubDirectorateID
		,ServiceID
		,LocationID
		,StaffGroupID
		,RoleID
		,PositionID
		,AssignmentCategoryID
		,EmployeeCategoryID
		,AssignmentStatusID
		,PayrollID
		,BankPostsHeldID
		,Created
		,ByWhom
		)
	values
		(
		source.MergeRecno
		,source.EncounterRecno
		,source.ContextCode
		,source.ContextID
		,source.DivisionID
		,source.DirectorateID
		,source.SubDirectorateID
		,source.ServiceID
		,source.LocationID
		,source.StaffGroupID
		,source.RoleID
		,source.PositionID
		,source.AssignmentCategoryID
		,source.EmployeeCategoryID
		,source.AssignmentStatusID
		,source.PayrollID
		,source.BankPostsHeldID

		,getdate()
		,suser_name()
		)
			
when matched
and
	checksum(
		source.EncounterRecno
		,source.ContextCode
		,source.ContextID
		,source.DivisionID
		,source.DirectorateID
		,source.SubDirectorateID
		,source.ServiceID
		,source.LocationID
		,source.StaffGroupID
		,source.RoleID
		,source.PositionID
		,source.AssignmentCategoryID
		,source.EmployeeCategoryID
		,source.AssignmentStatusID
		,source.PayrollID
		,source.BankPostsHeldID
	)
	<>
	checksum(
		target.EncounterRecno
		,target.ContextCode
		,target.ContextID
		,target.DivisionID
		,target.DirectorateID
		,target.SubDirectorateID
		,target.ServiceID
		,target.LocationID
		,target.StaffGroupID
		,target.RoleID
		,target.PositionID
		,target.AssignmentCategoryID
		,target.EmployeeCategoryID
		,target.AssignmentStatusID
		,target.PayrollID
		,target.BankPostsHeldID
	)
then
	update
	set
		target.EncounterRecno = source.EncounterRecno
		,target.ContextCode = source.ContextCode
		,target.ContextID = source.ContextID
		,target.DivisionID = source.DivisionID
		,target.DirectorateID = source.DirectorateID
		,target.SubDirectorateID = source.SubDirectorateID
		,target.ServiceID = source.ServiceID
		,target.LocationID = source.LocationID
		,target.StaffGroupID = source.StaffGroupID
		,target.RoleID = source.RoleID
		,target.PositionID = source.PositionID
		,target.AssignmentCategoryID = source.AssignmentCategoryID
		,target.EmployeeCategoryID = source.EmployeeCategoryID
		,target.AssignmentStatusID = source.AssignmentStatusID
		,target.PayrollID = source.PayrollID
		,target.BankPostsHeldID = source.BankPostsHeldID

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action into @ProcessList
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec Audit.WriteLogEvent
	 @ProcedureName
	,@Stats