﻿CREATE procedure [HR].[BuildFactAssignmentCompetency] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@deleted int
	,@inserted int
	,@updated int


truncate table HR.FactAssignmentCompetency


insert
into
	HR.FactAssignmentCompetency
	(
	MergeRecno
	,ContextID
	,CompetencyID
	,DivisionID
	,DirectorateID
	,SubDirectorateID
	,ServiceID
	,LocationID
	,StaffGroupID
	,RoleID
	,PositionID
	,AssignmentCategoryID
	,EmployeeCategoryID
	,PositionStartDateID
	,ExpiryDateID
	,MeetsRequirement
	,AssignmentMergeRecno
	,AssignmentStatusID
	,AssignmentStartDateID
	,AssignmentEndDateID
	,PayrollID
	,IsPrimaryAssignment
	,BankPostsHeldID
	,CensusDateID
	,Cases
	,DaysToExpiry
	)
select
	AssignmentCompetencyReference.MergeRecno
	,AssignmentCompetencyReference.ContextID
	,AssignmentCompetencyReference.CompetencyID
	,AssignmentReference.DivisionID
	,AssignmentReference.DirectorateID
	,AssignmentReference.SubDirectorateID
	,AssignmentReference.ServiceID
	,AssignmentReference.LocationID
	,AssignmentReference.StaffGroupID
	,AssignmentReference.RoleID
	,AssignmentReference.PositionID
	,AssignmentReference.AssignmentCategoryID
	,AssignmentReference.EmployeeCategoryID

	,PositionStartDateID =
		coalesce(
			PositionStartDate.DateID

			,case
			when Assignment.PositionStartDate is null
			then NullDate.DateID

			when cast(Assignment.PositionStartDate as date) < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,ExpiryDateID =
		coalesce(
			ExpiryDate.DateID

			,case
			when AssignmentCompetency.ExpiryDate is null
			then NullDate.DateID

			when cast(AssignmentCompetency.ExpiryDate as date) < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,AssignmentCompetency.MeetsRequirement

	,Assignment.MergeRecno
	,AssignmentReference.AssignmentStatusID

	,AssignmentStartDateID =
		coalesce(
			AssignmentStartDate.DateID

			,case
			when Assignment.AssignmentStartDate is null
			then NullDate.DateID

			when cast(Assignment.AssignmentStartDate as date) < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,AssignmentEndDateID =
		coalesce(
			AssignmentEndDate.DateID

			,case
			when Assignment.AssignmentEndDate is null
			then NullDate.DateID

			when cast(Assignment.AssignmentEndDate as date) < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,AssignmentReference.PayrollID
	,Assignment.IsPrimaryAssignment
	,AssignmentReference.BankPostsHeldID

	,CensusDateID =
		coalesce(
			CensusDate.DateID

			,case
			when AssignmentCompetency.CensusDate is null
			then NullDate.DateID

			when cast(AssignmentCompetency.CensusDate as date) < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,Cases = 1

	,DaysToExpiry = datediff(day, cast(getdate() as date), AssignmentCompetency.ExpiryDate)

from
	HR.AssignmentCompetencyReference

inner join HR.AssignmentCompetency
on	AssignmentCompetency.MergeRecno = AssignmentCompetencyReference.MergeRecno

inner join HR.Assignment
on	Assignment.AssignmentID = AssignmentCompetency.AssignmentID
and	Assignment.CensusDate = AssignmentCompetency.CensusDate
and	Assignment.ContextCode = AssignmentCompetency.ContextCode

inner join HR.AssignmentReference
on	AssignmentReference.MergeRecno = Assignment.MergeRecno

left join WH.Calendar PositionStartDate
on	PositionStartDate.TheDate = cast(Assignment.PositionStartDate as date)

left join WH.Calendar ExpiryDate
on	ExpiryDate.TheDate = cast(AssignmentCompetency.ExpiryDate as date)

left join WH.Calendar AssignmentStartDate
on	AssignmentStartDate.TheDate = cast(Assignment.AssignmentStartDate as date)

left join WH.Calendar AssignmentEndDate
on	AssignmentEndDate.TheDate = cast(Assignment.AssignmentEndDate as date)

left join WH.Calendar CensusDate
on	CensusDate.TheDate = cast(AssignmentCompetency.CensusDate as date)

left join WH.Calendar FutureDate
on	FutureDate.TheDate = '31 dec 9999'

left join WH.Calendar PreviousDate
on	PreviousDate.TheDate = '2 Jan 1900'

left join Utility.Parameter CalendarStartDate
on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

left join WH.Calendar NullDate
on	NullDate.TheDate = '1 jan 1900'


select @inserted = @@rowcount



select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec Audit.WriteLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime

select @Elapsed = datediff(minute,@StartTime,getdate())
