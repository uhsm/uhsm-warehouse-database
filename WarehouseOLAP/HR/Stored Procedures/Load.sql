﻿CREATE procedure [HR].Load as

/**********************************************************************************************************************************
Aim: Control procedure to import data into the OLAP model
**********************************************************************************************************************************/

declare @ReturnValue int

--Load Warehouse data
exec @ReturnValue = UHSMESR.LoadHR


if @ReturnValue = 0
BEGIN TRY

	declare @Process varchar(255) =  OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

	declare @LastLoadTime datetime =
		Utility.GetLastLoadTime(@Process);

	if @LastLoadTime is null
		set @LastLoadTime = '1 jan 1900' --default to start of time


	--generate missing reference data
	declare
		@ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[HR].[AssignmentCompetency]'
		,@ProcessListTable varchar(255) = null
		,@NewRows int = 0
		,@AllRows int = 0
		,@ActivityTableOverride varchar(max) = null


	--generate reference data
	exec Utility.BuildReferenceDataMap @ActivityTable, @NewRows output,@Debug = 0 ,@ActivityTableOverride = @ActivityTableOverride

	select
		@AllRows = @AllRows + isnull(@NewRows, 0)

	set
		@ActivityTable = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[HR].[Assignment]'


	--generate reference data
	exec Utility.BuildReferenceDataMap @ActivityTable, @NewRows output,@Debug = 0 ,@ActivityTableOverride = @ActivityTableOverride

	select
		@AllRows = @AllRows + isnull(@NewRows, 0)

	if @AllRows > 0
		exec Utility.BuildMember


	--Build the activity reference table
	exec HR.BuildAssignmentCompetencyReference
	exec HR.BuildAssignmentReference


	--Build Fact tables
	exec HR.BuildFactAssignmentCompetency


	--Update the last load time
	set @LastLoadTime = getdate()
	exec Utility.SetLastLoadTime @Process, @LastLoadTime


END TRY

BEGIN
	CATCH
	
	set @ReturnValue = @@ERROR

	DECLARE @ErrorSeverity INT,
			@ErrorNumber   INT,
			@ErrorMessage nvarchar(4000),
			@ErrorState INT,
			@ErrorLine  INT,
			@ErrorProc nvarchar(200)
			-- Grab error information from SQL functions
	SET @ErrorSeverity = ERROR_SEVERITY()
	SET @ErrorNumber   = ERROR_NUMBER()
	SET @ErrorMessage  = ERROR_MESSAGE()
	SET @ErrorState    = ERROR_STATE()
	SET @ErrorLine     = ERROR_LINE()
	SET @ErrorProc     = ERROR_PROCEDURE()
	SET @ErrorMessage  = 'An error occurred.' + CHAR(13) + 'SQL Server Error Message is: ' + CAST(@ErrorNumber AS VARCHAR(10)) + ' in procedure: ' + @ErrorProc + ' Line: ' + CAST(@ErrorLine AS VARCHAR(10)) + ' Error text: ' + @ErrorMessage
	-- Not all errors generate an error state, to set to 1 if it's zero
	IF @ErrorState  = 0
	SET @ErrorState = 1
	-- If the error renders the transaction as uncommittable or we have open transactions, we may want to rollback
	IF @@TRANCOUNT > 0
	BEGIN
			--print 'Rollback transaction'
			ROLLBACK TRANSACTION
	END
	RAISERROR (@ErrorMessage , @ErrorSeverity, @ErrorState, @ErrorNumber)
END CATCH

RETURN @ReturnValue
