﻿CREATE TABLE [HR].[Assignment]
(
	MergeRecno INT IDENTITY (1, 1) NOT NULL
	,EncounterRecno INT NOT NULL
	,ContextCode VARCHAR (20) NOT NULL
	,
	[AssignmentID] [varchar](30) NOT NULL,
	[CensusDate] date not null,
	[DivisionCode] [varchar](255) NULL,
	[DirectorateCode] [varchar](255) NULL,
	[ServiceCode] [varchar](255) NULL,
	[LocationCode] [varchar](255) NULL,
	[Title] [varchar](255) NULL,
	[Forename] [varchar](255) NULL,
	[Surname] [varchar](255) NULL,
	[PreferredName] [varchar](255) NULL,
	[EmployeeNumber] [int] NULL,
	[StaffGroupCode] [varchar](255) NULL,
	[RoleCode] [varchar](255) NULL,
	[PositionCode] [varchar](255) NULL,
	[PositionNumber] [int] NULL,
	[PositionKey] [varchar](255) NULL,
	[PositionStartDate] [date] NULL,
	[AssignmentCategoryCode] [varchar](255) NULL,
	[EmployeeCategoryCode] [varchar](255) NULL,
    [AssignmentStartDate] [date] NULL,
    [AssignmentEndDate] [date] NULL,
    [AssignmentStatusCode] VARCHAR(255) NULL,
    [PayrollCode] VARCHAR(255) NULL,
    [IsPrimaryAssignment] bit NULL,
    [BankPostsHeldCode] VARCHAR(255) NULL,
    [MaidenName] VARCHAR(255) NULL,
    [PreviousSurname] VARCHAR(255) NULL,
    [EmailAddress] VARCHAR(255) NULL,
    [WorkTelephoneNumber] VARCHAR(255) NULL,

	 Created datetime NOT NULL DEFAULT getdate()
	,Updated datetime NULL
	,ByWhom varchar(50) NOT NULL DEFAULT suser_name()

CONSTRAINT [PK_HR_Assignment] PRIMARY KEY CLUSTERED ([ContextCode] ASC, [EncounterRecno] ASC)

)
