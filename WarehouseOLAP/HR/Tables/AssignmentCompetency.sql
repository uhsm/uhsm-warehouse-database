﻿CREATE TABLE [HR].[AssignmentCompetency]
(
	MergeRecno INT IDENTITY (1, 1) NOT NULL
	,EncounterRecno INT NOT NULL
	,ContextCode VARCHAR (20) NOT NULL
	
	,[AssignmentID] [varchar](30) NOT NULL
	,[CensusDate] date not null
	,[CompetencyCode] [varchar](255) NOT NULL
	,[MeetsRequirement] [bit] NULL
	,[ExpiryDate] [date] NULL

	,Created datetime NOT NULL DEFAULT getdate()
	,Updated datetime NULL
	,ByWhom varchar(50) NOT NULL DEFAULT suser_name()
CONSTRAINT [PK_HR_AssignmentCompetency] PRIMARY KEY CLUSTERED ([ContextCode] ASC, [EncounterRecno] ASC)
)
