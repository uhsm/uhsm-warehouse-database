﻿CREATE TABLE [HR].[AssignmentCompetencyReference] (
    [MergeRecno]              INT          NOT NULL,
    [EncounterRecno]                 INT          NOT NULL,
    [ContextCode]                    VARCHAR (20) NOT NULL,
    [ContextID]               INT          NOT NULL,
    [CompetencyID]     INT          NOT NULL,

   	[Created] [datetime] NOT NULL DEFAULT getdate(),
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL DEFAULT suser_name(),
    CONSTRAINT [PK_HR_AssignmentCompetencyReference] PRIMARY KEY CLUSTERED ([EncounterRecno], [ContextCode])
);

GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_HR_AssignmentCompetencyReference_01]
    ON [HR].[AssignmentCompetencyReference]([MergeRecno] ASC);


GO

