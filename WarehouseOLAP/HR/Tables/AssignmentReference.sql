﻿CREATE TABLE [HR].[AssignmentReference] (
    [MergeRecno]              INT          NOT NULL,
    [EncounterRecno]                 INT          NOT NULL,
    [ContextCode]                    VARCHAR (20) NOT NULL,
    [ContextID]               INT          NOT NULL,
    [DivisionID]     INT          NOT NULL,
    [DirectorateID]     INT          NOT NULL,
    [SubDirectorateID]     INT          NOT NULL,
    [ServiceID]     INT          NOT NULL,
    [LocationID]     INT          NOT NULL,
    [StaffGroupID]     INT          NOT NULL,
    [RoleID]     INT          NOT NULL,
    [PositionID]     INT          NOT NULL,
    [AssignmentCategoryID]     INT          NOT NULL,
    [EmployeeCategoryID]     INT          NOT NULL,
	[AssignmentStatusID] int NOT NULL,
	[PayrollID] int NOT NULL,
	[BankPostsHeldID] int NOT NULL,

   	[Created] [datetime] NOT NULL DEFAULT getdate(),
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL DEFAULT suser_name(),
    CONSTRAINT [PK_HR_AssignmentReference] PRIMARY KEY CLUSTERED ([EncounterRecno], [ContextCode])
);

GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_HR_AssignmentReference_01]
    ON [HR].[AssignmentReference]([MergeRecno] ASC);


GO

