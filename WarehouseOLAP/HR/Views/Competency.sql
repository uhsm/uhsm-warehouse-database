﻿CREATE view [HR].[Competency] as

select
	 SourceContextCode = Member.SourceContextCode
	,SourceContextID = Member.SourceContextID
	,SourceContext = Member.SourceContext
	,SourceID = Member.SourceValueID
	,SourceCode = Member.SourceValueCode
	,SourceValue = Member.SourceValue
	,LocalID = Member.LocalValueID
	,LocalCode = Member.LocalValueCode
	,LocalValue = Member.LocalValue
	,NationalID = Member.NationalValueID
	,NationalCode = Member.NationalValueCode
	,NationalValue = Member.NationalValue

	,Competency.IsNew

	,Competency.GroupLevelID
	,GroupLevel = isnull(GroupLevel.GroupLevel, 'Not Specified')

	,Competency.GroupTypeID
	,GroupType = isnull(GroupType.GroupType, 'Not Specified')

from
	WH.Member

left join Warehouse.ESR.Competency
on Competency.CompetencyCode = Member.SourceValueCode

left join Warehouse.ESR.GroupLevel
on GroupLevel.GroupLevelID = Competency.GroupLevelID

left join Warehouse.ESR.GroupType
on GroupType.GroupTypeID = Competency.GroupTypeID

where
	Member.AttributeCode = 'HR||COMPETENCY'

GO
