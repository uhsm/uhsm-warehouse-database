﻿CREATE procedure [UHSMESR].[BuildHRAssignment] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = object_schema_name(@@procid) + '.' + object_name(@@procid)
	,@Elapsed int
	,@Stats varchar(max)
	,@deleted int
	,@inserted int
	,@updated int


declare @ProcessList table
	(
	 MergeRecno int
	,Action nvarchar(10)
	)
	

merge
	HR.Assignment target
using
	(
	select
		EncounterRecno
		,ContextCode = 'UHSM||ESR'
		
		,AssignmentID = cast(nullif(AssignmentID, '') as varchar(30))
		,CensusDate = cast(CensusDate as date)
		,DivisionCode = cast(nullif(DivisionCode, '') as varchar(255))
		,DirectorateCode = cast(nullif(DirectorateCode, '') as varchar(255))
		,ServiceCode = cast(nullif(ServiceCode, '') as varchar(255))
		,LocationCode = cast(nullif(LocationCode, '') as varchar(255))
		,Title = cast(nullif(Title, '') as varchar(255))
		,Forename = cast(nullif(Forename, '') as varchar(255))
		,Surname = cast(nullif(Surname, '') as varchar(255))
		,PreferredName = cast(nullif(PreferredName, '') as varchar(255))
		,EmployeeNumber = cast(EmployeeNumber as int)
		,StaffGroupCode = cast(nullif(StaffGroupCode, '') as varchar(255))
		,RoleCode = cast(nullif(RoleCode, '') as varchar(255))
		,PositionCode = cast(nullif(PositionCode, '') as varchar(255))
		,PositionNumber = cast(PositionNumber as int)
		,PositionKey = cast(nullif(PositionKey, '') as varchar(255))
		,PositionStartDate = cast(PositionStartDate as date)
		,AssignmentCategoryCode = cast(nullif(AssignmentCategoryCode, '') as varchar(255))
		,EmployeeCategoryCode = cast(nullif(EmployeeCategoryCode, '') as varchar(255))
		,AssignmentStartDate = cast(AssignmentStartDate as date)
		,AssignmentEndDate = cast(AssignmentEndDate as date)
		,AssignmentStatusCode = cast(nullif(AssignmentStatusCode, '') as varchar(255))
		,PayrollCode = cast(nullif(PayrollCode, '') as varchar(255))
		,IsPrimaryAssignment = cast(IsPrimaryAssignment as bit)
		,BankPostsHeldCode = cast(nullif(BankPostsHeldCode, '') as varchar(255))
		,MaidenName = cast(nullif(MaidenName, '') as varchar(255))
		,PreviousSurname = cast(nullif(PreviousSurname, '') as varchar(255))
		,EmailAddress = cast(nullif(EmailAddress, '') as varchar(255))
		,WorkTelephoneNumber = cast(nullif(WorkTelephoneNumber, '') as varchar(255))
	from
		Warehouse.ESR.Assignment
	) source
on	source.EncounterRecno = target.EncounterRecno
and source.ContextCode = target.ContextCode

when not matched by source
and	target.ContextCode = 'UHSM||ESR'
then
	delete

when not matched
then
	insert
		(
		 EncounterRecno
		,ContextCode
		,AssignmentID
		,CensusDate
		,DivisionCode
		,DirectorateCode
		,ServiceCode
		,LocationCode
		,Title
		,Forename
		,Surname
		,PreferredName
		,EmployeeNumber
		,StaffGroupCode
		,RoleCode
		,PositionCode
		,PositionNumber
		,PositionKey
		,PositionStartDate
		,AssignmentCategoryCode
		,EmployeeCategoryCode
		,AssignmentStartDate
		,AssignmentEndDate
		,AssignmentStatusCode
		,PayrollCode
		,IsPrimaryAssignment
		,BankPostsHeldCode
		,MaidenName
		,PreviousSurname
		,EmailAddress
		,WorkTelephoneNumber

		,Created
		,ByWhom
		)
	values
		(
		source.EncounterRecno
		,source.ContextCode
		,source.AssignmentID
		,source.CensusDate
		,source.DivisionCode
		,source.DirectorateCode
		,source.ServiceCode
		,source.LocationCode
		,source.Title
		,source.Forename
		,source.Surname
		,source.PreferredName
		,source.EmployeeNumber
		,source.StaffGroupCode
		,source.RoleCode
		,source.PositionCode
		,source.PositionNumber
		,source.PositionKey
		,source.PositionStartDate
		,source.AssignmentCategoryCode
		,source.EmployeeCategoryCode
		,source.AssignmentStartDate
		,source.AssignmentEndDate
		,source.AssignmentStatusCode
		,source.PayrollCode
		,source.IsPrimaryAssignment
		,source.BankPostsHeldCode
		,source.MaidenName
		,source.PreviousSurname
		,source.EmailAddress
		,source.WorkTelephoneNumber

		,getdate()
		,suser_name()
		)


when matched
and
	checksum(
		source.AssignmentID
		,source.CensusDate
		,source.DivisionCode
		,source.DirectorateCode
		,source.ServiceCode
		,source.Title
		,source.Forename
		,source.Surname
		,source.PreferredName
		,source.EmployeeNumber
		,source.StaffGroupCode
		,source.RoleCode
		,source.PositionCode
		,source.PositionNumber
		,source.PositionKey
		,source.PositionStartDate
		,source.AssignmentCategoryCode
		,source.EmployeeCategoryCode
		,source.AssignmentStartDate
		,source.AssignmentEndDate
		,source.AssignmentStatusCode
		,source.PayrollCode
		,source.IsPrimaryAssignment
		,source.BankPostsHeldCode
		,source.MaidenName
		,source.PreviousSurname
		,source.EmailAddress
		,source.WorkTelephoneNumber
		,source.LocationCode
	)
	<>
	checksum(
		target.AssignmentID
		,target.CensusDate
		,target.DivisionCode
		,target.DirectorateCode
		,target.ServiceCode
		,target.Title
		,target.Forename
		,target.Surname
		,target.PreferredName
		,target.EmployeeNumber
		,target.StaffGroupCode
		,target.RoleCode
		,target.PositionCode
		,target.PositionNumber
		,target.PositionKey
		,target.PositionStartDate
		,target.AssignmentCategoryCode
		,target.EmployeeCategoryCode
		,target.AssignmentStartDate
		,target.AssignmentEndDate
		,target.AssignmentStatusCode
		,target.PayrollCode
		,target.IsPrimaryAssignment
		,target.BankPostsHeldCode
		,target.MaidenName
		,target.PreviousSurname
		,target.EmailAddress
		,target.WorkTelephoneNumber
		,target.LocationCode
	)
then
	update
	set
		target.AssignmentID = source.AssignmentID
		,target.CensusDate = source.CensusDate
		,target.DivisionCode = source.DivisionCode
		,target.DirectorateCode = source.DirectorateCode
		,target.ServiceCode = source.ServiceCode
		,target.LocationCode = source.LocationCode
		,target.Title = source.Title
		,target.Forename = source.Forename
		,target.Surname = source.Surname
		,target.PreferredName = source.PreferredName
		,target.EmployeeNumber = source.EmployeeNumber
		,target.StaffGroupCode = source.StaffGroupCode
		,target.RoleCode = source.RoleCode
		,target.PositionCode = source.PositionCode
		,target.PositionNumber = source.PositionNumber
		,target.PositionKey = source.PositionKey
		,target.PositionStartDate = source.PositionStartDate
		,target.AssignmentCategoryCode = source.AssignmentCategoryCode
		,target.EmployeeCategoryCode = source.EmployeeCategoryCode
		,target.AssignmentStartDate = source.AssignmentStartDate
		,target.AssignmentEndDate = source.AssignmentEndDate
		,target.AssignmentStatusCode = source.AssignmentStatusCode
		,target.PayrollCode = source.PayrollCode
		,target.IsPrimaryAssignment = source.IsPrimaryAssignment
		,target.BankPostsHeldCode = source.BankPostsHeldCode
		,target.MaidenName = source.MaidenName
		,target.PreviousSurname = source.PreviousSurname
		,target.EmailAddress = source.EmailAddress
		,target.WorkTelephoneNumber = source.WorkTelephoneNumber

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	 coalesce(inserted.MergeRecno, deleted.MergeRecno)
	,$action
into
	@ProcessList

;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + cast(@Elapsed as varchar) + ' minutes'

exec Audit.WriteLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
