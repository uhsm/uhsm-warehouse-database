﻿CREATE procedure [UHSMESR].[BuildHRAssignmentCompetency] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = object_schema_name(@@procid) + '.' + object_name(@@procid)
	,@Elapsed int
	,@Stats varchar(max)
	,@deleted int
	,@inserted int
	,@updated int


declare @ProcessList table
	(
	 MergeRecno int
	,Action nvarchar(10)
	)
	

merge
	HR.AssignmentCompetency target
using
	(
	select
		EncounterRecno
		,ContextCode = 'UHSM||ESR'
		
		,AssignmentID = cast(nullif(AssignmentID, '') as varchar(30))
		,CensusDate = cast(CensusDate as date)
		,CompetencyCode = cast(nullif(CompetencyCode, '') as varchar(255))
		,MeetsRequirement = cast(MeetsRequirement as bit)
		,ExpiryDate = cast(ExpiryDate as date)
	from
		Warehouse.ESR.AssignmentCompetency
	) source
on	source.EncounterRecno = target.EncounterRecno
and source.ContextCode = target.ContextCode

when not matched by source
and	target.ContextCode = 'UHSM||ESR'
then
	delete

when not matched
then
	insert
		(
		 EncounterRecno
		,ContextCode
		,AssignmentID
		,CensusDate
		,CompetencyCode
		,MeetsRequirement
		,ExpiryDate

		,Created
		,ByWhom
		)
	values
		(
		source.EncounterRecno
		,source.ContextCode
		,source.AssignmentID
		,source.CensusDate
		,source.CompetencyCode
		,source.MeetsRequirement
		,source.ExpiryDate

		,getdate()
		,suser_name()
		)


when matched
and
	checksum(
		source.AssignmentID
		,source.CensusDate
		,source.CompetencyCode
		,source.MeetsRequirement
		,source.ExpiryDate
	)
	<>
	checksum(
		target.AssignmentID
		,target.CensusDate
		,target.CompetencyCode
		,target.MeetsRequirement
		,target.ExpiryDate
	)
then
	update
	set
		target.AssignmentID = source.AssignmentID
		,target.CensusDate = source.CensusDate
		,target.CompetencyCode = source.CompetencyCode
		,target.MeetsRequirement = source.MeetsRequirement
		,target.ExpiryDate = source.ExpiryDate

		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	 coalesce(inserted.MergeRecno, deleted.MergeRecno)
	,$action
into
	@ProcessList

;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + cast(@Elapsed as varchar) + ' minutes'

exec Audit.WriteLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
