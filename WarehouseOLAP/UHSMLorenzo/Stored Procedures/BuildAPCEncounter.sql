﻿CREATE procedure [UHSMLorenzo].[BuildAPCEncounter] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = object_schema_name(@@procid) + '.' + object_name(@@procid)
	,@Elapsed int
	,@Stats varchar(max)
	,@deleted int
	,@inserted int
	,@updated int


declare @ProcessList table
	(
	 MergeRecno int
	,Action nvarchar(10)
	)

create table #Encounter
	(
	EncounterRecno INT NOT NULL
	,ContextCode VARCHAR (20) NOT NULL
	,Action nvarchar(10) not null
	,AdmittedCareID VARCHAR (60) NULL
	,SpellID VARCHAR (16) NULL
	,PatientID INT NULL
	,EpisodeNumber INT NULL
	,PatientTitle VARCHAR (10) NULL
	,PatientForename VARCHAR (50) NULL
	,PatientSurname VARCHAR (50) NULL
	,DateOfBirth DATE NULL
	,DateOfDeath DATE NULL
	,SexID VARCHAR (20) NULL
	,NHSNumber VARCHAR (10) NULL
	,NHSNumberStatusCode VARCHAR (20) NULL
	,DistrictNumber VARCHAR (20) NULL
	,CasenoteNumber VARCHAR (14) NULL
	,PatientAddressLine1 VARCHAR (80) NULL
	,PatientAddressLine2 VARCHAR (80) NULL
	,PatientAddressLine3 VARCHAR (50) NULL
	,PatientAddressLine4 VARCHAR (50) NULL
	,PatientPostcode VARCHAR (8) NULL
	,CarerSupportIndicator bit null
	,ReferredByCode VARCHAR (20) NULL
	,ReferrerCode VARCHAR (20) NULL
	,ReferrerOrganisationCode VARCHAR (20) NULL
	,RegisteredGpCode VARCHAR (20) NULL
	,RegisteredGpPracticeCode VARCHAR (20) NULL
	,ReligionCode VARCHAR (20) NULL
	,MaritalStatusCode VARCHAR (20) NULL
	,EthnicCategoryCode VARCHAR (20) NULL
	,DateOnWaitingList DATE NULL
	,ManagementIntentionID VARCHAR (20) NULL
	,PatientClassificationID VARCHAR (20) NULL
	,IsMRSA BIT NULL
	,NeonatalLevelOfCare VARCHAR (20) NULL
	,IsWellBabyFlag INT NULL
	,AdmissionMethodCode VARCHAR (20) NULL
	,AdmissionSourceCode VARCHAR (20) NULL
	,AdministrativeCategoryID VARCHAR (20) NULL
	,AdmissionTime DATETIME NULL
	,CancelledElectiveAdmissionFlag BIT NULL
	,ConsultantCode VARCHAR (20) NULL
	,TreatmentFunctionCode VARCHAR (20) NULL
	,ProviderCode VARCHAR (20) NULL
	,IsFirstEpisodeInSpell BIT NULL
	,EpisodeStartTime DATETIME NULL
	,StartSiteCode VARCHAR (20) NULL
	,StartWardCode VARCHAR (20) NULL
	,EpisodeEndTime DATETIME NULL
	,EndSiteCode VARCHAR (20) NULL
	,EndWardCode VARCHAR (20) NULL
	,IsLastEpisodeInSpell BIT NULL
	,LOE INT NULL
	,LOS INT NULL
	,ExpectedLOS SMALLINT NULL
	,DischargeDestinationCode VARCHAR (20) NULL
	,DischargeMethodCode VARCHAR (20) NULL
	,DischargeTime DATETIME NULL
	,ClinicalCodingCompleteTime DATETIME NULL
	,ClinicalCodingStatusCode VARCHAR (1) NULL
	,PASHRGCode VARCHAR (5) NULL
	,OperationStatusID TINYINT NULL
	,PrimaryDiagnosisCode VARCHAR (20) NULL
	,SubsidiaryDiagnosisCode VARCHAR (20) NULL
	,IntendedPrimaryProcedureCode VARCHAR (20) NULL
	,PrimaryOperationCode VARCHAR (20) NULL
	,PrimaryOperationDate DATE NULL
	,RTTPathwayID VARCHAR (27) NULL
	,RTTPathwayCondition VARCHAR (20) NULL
	,RTTStartTime DATETIME NULL
	,RTTEndTime DATETIME NULL
	,RTTSpecialtyCode VARCHAR (4) NULL
	,RTTCurrentProviderCode VARCHAR (20) NULL
	,RTTCurrentStatusCode VARCHAR (20) NULL
	,RTTCurrentStatusTime DATETIME NULL
	,RTTCurrentPrivatePatientFlag bit NULL
	,RTTOverseasStatusFlag bit NULL
	,RTTPeriodStatusCode VARCHAR (20) NULL
	,TransferFrom VARCHAR (14) NULL
	,IsDiagnosticProcedure BIT NULL
	,OverseasVisitorStatusID VARCHAR (20) NULL
	,IsFirstRegularAdmission int NULL
	,EpisodeOutcomeCode VARCHAR (20) NULL
	,LegalStatusClassificationOnAdmissionCode VARCHAR (20) NULL
	,LegalStatusClassificationOnDischargeCode VARCHAR (20) NULL
	,DecisionToAdmitDateTime datetime
	,OriginalDecisionToAdmitDateTime datetime
	,DurationOfElectiveWait int
 	)

insert
into
	#Encounter
select
	ProcessList.EncounterRecno
	,ContextCode = 'UHSM||LORENZO'
	,ProcessList.Action
	,AdmittedCareID = Episode.ProfessionalCareEpisodeID
	,SpellID = Episode.ProviderSpellID
	,PatientID = Episode.PatientID
	,EpisodeNumber = null --Episode.EpisodeIdentifier
	,Title = Patient.Title
	,Forename = Patient.Forename
	,Surname = Patient.Surname
	,DateOfBirth = Patient.DateOfBirth
	,DateOfDeath = Patient.DateOfDeath
	,SexID = Patient.SexID
	,NHSNumber = Patient.NHSIdentifier
	,NHSNumberStatusCode = Patient.NHSNumberTracingServiceTypeID
	,DistrictNumber = Patient.PASID
	,CasenoteNumber = null
	,PatientAddress1 = PatientAddress.Line1
	,PatientAddress2 = PatientAddress.Line2	
	,PatientAddress3 = PatientAddress.Line3	
	,PatientAddress4 = PatientAddress.Line4
	,Postcode = left(PatientAddress.Postcode, 8)
	,CarerSupportIndicator = null
	,ReferredByCode = null
	,ReferrerCode = null
	,ReferringOrganisationCode = null

	,RegisteredGpCode =
		isnull(
			RegisteredGp.ProfessionalCarerID
			,DefaultRegisteredGp.ProfessionalCarerID
		)

	,RegisteredGpPracticeCode =
		isnull(
			RegisteredGp.HealthOrganisationID
			,DefaultRegisteredGp.HealthOrganisationID
		)

	,ReligionCode = Patient.ReligionID
	,MaritalStatusCode = Patient.MaritalStatusID
	,EthnicCategoryCode = Patient.EthnicCategoryID

	,DateOnWaitingList =
		convert(
			date
			,coalesce(
				(
				select
					max(AdmissionOffer.TCIDateTime)
				from
					Warehouse.Lorenzo.AdmissionOffer
				where
					AdmissionOffer.WaitingListID = WaitingList.WaitingListID
				and	AdmissionOffer.OfferOutcomeID in
					(
					 985	--Did Not Attend (DNA)
					,2003601	--No Contact
					,2000657	--Admission Cancelled by or on behalf of Patient
					,2003600	--DNA (Did Not Attend)
					,2003599	--Cancelled due to Patient Death
					)
				)

				,WaitingList.WaitingListDateTime
			)
		)

	,ManagementIntentionID = Spell.ManagementIntentionID
	,PatientClassificationID = Spell.PatientClassificationID

	,MRSAFlag =
		convert(
			 bit
			,case
			when exists
				(
				select
					1
				from
					Warehouse.Lorenzo.ClinicalCoding
				where
					ClinicalCoding.PatientID = Episode.PatientID
				and	ClinicalCoding.StartDateTime <= Episode.EndDateTime
				and	ClinicalCoding.ClinicalCodingCode in ('009', 'INFC006')
				and	ClinicalCoding.ClinicalCodingSubtypeCode = 'ALERT'
				)
			then 1
			else 0
			end
		)

	,NeonatalLevelOfCare = null

	,IsWellBabyFlag = null

	,AdmissionMethodCode = Spell.AdmissionMethodID
	,AdmissionSourceCode = Spell.AdmissionSourceID
	,AdministrativeCategoryID = Spell.AdministrativeCategoryID
	,AdmissionTime = Spell.AdmissionDateTime
	,CancelledElectiveAdmissionFlag = null
	,ConsultantCode = Episode.ProfessionalCarerID
	,TreatmentFunctionCode = Episode.SpecialtyID
	,ProviderCode = Episode.ProviderID
	,IsFirstEpisodeInSpell = case when Episode.StartDateTime = Spell.AdmissionDateTime then 1 else 0 end
	,EpisodeStartTime = Episode.StartDateTime
	,StartSiteCode = AdmissionWardStay.HealthOrganisationOwnerID
	,StartWardCode = AdmissionWardStay.ServicePointID
	,EpisodeEndTime = Episode.EndDateTime
	,EndSiteCode = DischargeWardStay.HealthOrganisationOwnerID
	,EndWardCode = DischargeWardStay.ServicePointID

	,IsLastEpisodeInSpell = 
		case
		when Episode.EndDateTime = 
			case --handle the provisional end indicated discharges
			when Spell.IsProvisionalEnd = 'N'
			then Spell.DischargeDateTime
			else null
			end

		then 1 
		else 0 
		end

	,LOE = datediff(day, Episode.StartDateTime, Episode.EndDateTime)
	,LOS = datediff(day, Spell.AdmissionDateTime, Spell.ExpectedDischargeDateTime)
	,ExpectedLOS = datediff(day, Spell.AdmissionDateTime, Spell.DischargeDateTime)
	,DischargeDestinationCode = Spell.DischargeDestinationID
	,DischargeMethodCode = Spell.DischargeMethodID

	,DischargeTime =
		case --handle the provisional end indicated discharges
		when Spell.IsProvisionalEnd = 'N'
		then Spell.DischargeDateTime
		else null
		end

	,ClinicalCodingCompleteTime =
		case Episode.CodingStatusID 
		when 455 -- Complete
		then PrimaryProcedure.ClinicalCodingDateTime --this previously used ClinicalCodingEndDateTime but this is never populated
		else null
		end

	,ClinicalCodingStatusCode = 
		case Episode.CodingStatusID 
		when 454 Then 'I'	-- Incomplete
		when 455 Then 'C'	-- Complete
		when 456 Then 'A'	-- Awaiting Results
		else 'N'			-- Not Specified
		end

	,PASHRGCode = null
		
	,OperationStatusID =
		case
		when PrimaryProcedure.ClinicalCodingCode is not null
		then '1'
		else '8'
		end

	,PrimaryDiagnosisCode = PrimaryDiagnosis.ClinicalCodingCode
	,SubsidiaryDiagnosisCode = null

	,IntendedPrimaryProcedureCode =
		left(
			isnull(
				WaitingList.PlannedProcedure
				,PlannedProcedure.ClinicalCodingCode
			)
			,20
		)

	,PrimaryOperationCode = PrimaryProcedure.ClinicalCodingCode
	,PrimaryOperationDate = PrimaryProcedure.ClinicalCodingDateTime
	,RTTPathwayID = rtrim(left(Referral.PatientPathwayID, 25))
	,RTTPathwayCondition = null
	,RTTStartTime = Referral.RTTStartDateTime
	,RTTEndTime = null
	,RTTSpecialtyCode = null
	,RTTCurrentProviderCode = null
	,RTTCurrentStatusCode = RTT.StatusID
	,RTTCurrentStatusTime = RTT.StatusDateTime
	,RTTCurrentPrivatePatientFlag = null
	,RTTOverseasStatusFlag = null
	
	,RTTPeriodStatusCode =
		coalesce(
			EpisodePeriodStatus.StatusID
			,SpellPeriodStatus.StatusID
			,Spell.RTTStatusID
		)

	,TransferFrom = null			
	,IsDiagnosticProcedure = null

	,OverseasVisitorStatusID =
		(
		select top 1
			OverseasVisitorStatus.OverseasStatusID
		from
			Warehouse.Lorenzo.OverseasVisitorStatus OverseasVisitorStatus
		where
			OverseasVisitorStatus.PatientID = Patient.PatientID
		and	OverseasVisitorStatus.StartDateTime <= Episode.StartDateTime

		order by
			OverseasVisitorStatus.StartDateTime desc
		)

	,IsFirstRegularAdmission =
		case
		when Spell.IsFirstRegularAdmission = 'N'
		then 0
		else 1
		end

    ,EpisodeOutcomeCode = Episode.EpisodeOutcomeID
	,LegalStatusClassificationOnAdmissionCode = Spell.LegalStatusOnAdmissionID
	,LegalStatusClassificationOnDischargeCode = Spell.LegalStatusOnDischargeID
	,DecisionToAdmitDateTime = Spell.DecisionToAdmitDateTime
	,OriginalDecisionToAdmitDateTime = AdmissionDecision.DecisionToAdmitDateTime

	,DurationOfElectiveWait =
		datediff(
			day
			,cast(AdmissionDecision.DecisionToAdmitDateTime as date)
			,cast(Spell.DecisionToAdmitDateTime as date)
		)

from
	Warehouse.Lorenzo.ProfessionalCareEpisode Episode

inner join UHSMLorenzo.APCProcessList ProcessList
on	ProcessList.Dataset = 'Encounter'
and	ProcessList.Action <> 'DELETE'
and	ProcessList.EncounterRecno = Episode.ProfessionalCareEpisodeID

inner join Warehouse.Lorenzo.Patient
on	Episode.PatientID = Patient.PatientID
	
left join Warehouse.Lorenzo.ProviderSpell Spell 
on	Episode.ProviderSpellID = Spell.ProviderSpellID

left join Warehouse.Lorenzo.AdmissionDecision
on	AdmissionDecision.AdmissionDecisionID = Spell.AdmissionDecisionID

left join Warehouse.Lorenzo.Contract
on	Contract.ContractID = Spell.ContractID

left join Warehouse.Lorenzo.PatientIdentifier DistrictNo
on	DistrictNo.PatientID = Patient.PatientID
and	DistrictNo.PatientIdentifierTypeID = 2001232 --facility
and	DistrictNo.PatientIdentifier like 'RM2%'
and	not exists
	(
	select
		1
	from
		Warehouse.Lorenzo.PatientIdentifier Previous
	where
		Previous.PatientID = DistrictNo.PatientID
	and	Previous.PatientIdentifierTypeID = DistrictNo.PatientIdentifierTypeID
	and	Previous.PatientIdentifier like 'RM2%'
	and	(
			Previous.StartDateTime > DistrictNo.StartDateTime
		or
			(
				Previous.StartDateTime = DistrictNo.StartDateTime
			and	Previous.PatientIdentifierID > DistrictNo.PatientIdentifierID
			)
		)
	)

left join Warehouse.Lorenzo.PatientAddressRole
on	PatientAddressRole.PatientID = Patient.PatientID
and	PatientAddressRole.RollTypeCode = 'HOME'
and	exists
	(
	select
		1
	from
		Warehouse.Lorenzo.PatientAddress
	where
		PatientAddress.PatientAddressID = PatientAddressRole.AddressID
	and	PatientAddress.AddressTypeID = 'POSTL'
	)

and	coalesce(Episode.EndDateTime, getdate()) between PatientAddressRole.StartDateTime and
	coalesce(
		PatientAddressRole.EndDateTime
		,Episode.EndDateTime
		,getdate()
	)
and	not exists
	(
	select
		1
	from
		Warehouse.Lorenzo.PatientAddressRole Previous
	where
		Previous.PatientID = PatientAddressRole.PatientID
	and	Previous.RollTypeCode = PatientAddressRole.RollTypeCode
	and	exists
		(
		select
			1
		from
			Warehouse.Lorenzo.PatientAddress
		where
			PatientAddress.PatientAddressID = Previous.AddressID
		and	PatientAddress.AddressTypeID = 'POSTL'
		)
	and	coalesce(Episode.EndDateTime, getdate()) between 
			Previous.StartDateTime 
		and coalesce(
				 Previous.EndDateTime
				,Episode.EndDateTime
				,getdate()
			)
	and	(
			Previous.StartDateTime > PatientAddressRole.StartDateTime
		or	(
				Previous.StartDateTime = PatientAddressRole.StartDateTime
			and	Previous.PatientAddressRoleID > PatientAddressRole.PatientAddressRoleID
			)
		)
	)

left join Warehouse.Lorenzo.PatientAddress
on	PatientAddress.PatientAddressID = PatientAddressRole.AddressID

left join Warehouse.Lorenzo.PatientProfessionalCarer DefaultRegisteredGp
on	DefaultRegisteredGp.PatientID = Patient.PatientID
and	DefaultRegisteredGp.ProfessionalCarerTypeMainCode = 'GMPRC'
and	Episode.StartDateTime < DefaultRegisteredGp.StartDateTime
and	not exists
	(
	select
		1
	from
		Warehouse.Lorenzo.PatientProfessionalCarer Previous
	where
		Previous.PatientID = DefaultRegisteredGp.PatientID
	and	Previous.ProfessionalCarerTypeMainCode = DefaultRegisteredGp.ProfessionalCarerTypeMainCode
	and	Episode.StartDateTime < Previous.StartDateTime
	and	(
			Previous.StartDateTime > DefaultRegisteredGp.StartDateTime
		or	(
				Previous.StartDateTime = DefaultRegisteredGp.StartDateTime
			and	Previous.PatientProfessionalCarerID > DefaultRegisteredGp.PatientProfessionalCarerID
			)
		)
	)

left join Warehouse.Lorenzo.PatientProfessionalCarer RegisteredGp
on	RegisteredGp.PatientID = Patient.PatientID
and	RegisteredGp.ProfessionalCarerTypeMainCode = 'GMPRC'
and	Episode.StartDateTime between 
		RegisteredGp.StartDateTime 
	and coalesce(
			RegisteredGp.EndDateTime
			,Episode.StartDateTime
		)
and	not exists
	(
	select
		1
	from
		Warehouse.Lorenzo.PatientProfessionalCarer Previous
	where
		Previous.PatientID = RegisteredGp.PatientID
	and	Previous.ProfessionalCarerTypeMainCode = RegisteredGp.ProfessionalCarerTypeMainCode
	and	Episode.StartDateTime between 
			Previous.StartDateTime 
		and coalesce(
				Previous.EndDateTime
				,Episode.StartDateTime
			)
	and	(
			Previous.StartDateTime > RegisteredGp.StartDateTime
		or	(
				Previous.StartDateTime = RegisteredGp.StartDateTime
			and	Previous.PatientProfessionalCarerID > RegisteredGp.PatientProfessionalCarerID
			)
		)
	)

left join Warehouse.Lorenzo.WaitingList
on	WaitingList.WaitingListID = Episode.WaitingListID

inner join Warehouse.Lorenzo.ServicePointStay AdmissionWardStay
on	AdmissionWardStay.ProviderSpellID = Spell.ProviderSpellID
and	not exists
	(
	select
		1
	from
		 Warehouse.Lorenzo.ServicePointStay Previous
	where
		Previous.ProviderSpellID = AdmissionWardStay.ProviderSpellID
	and	(
			Previous.StartDateTime < AdmissionWardStay.StartDateTime
		or	(
				Previous.StartDateTime = AdmissionWardStay.StartDateTime
			and	Previous.ServicePointStayID < AdmissionWardStay.ServicePointStayID
			)
		)
	)

inner join Warehouse.Lorenzo.ServicePointStay DischargeWardStay
on	DischargeWardStay.ProviderSpellID = Spell.ProviderSpellID
and	not exists
	(
	select
		1
	from
		 Warehouse.Lorenzo.ServicePointStay Previous
	where
		Previous.ProviderSpellID = DischargeWardStay.ProviderSpellID
	and	(
			Previous.StartDateTime > DischargeWardStay.StartDateTime
		or	(
				Previous.StartDateTime = DischargeWardStay.StartDateTime
			and	Previous.ServicePointStayID > DischargeWardStay.ServicePointStayID
			)
		)
	)

left join Warehouse.Lorenzo.ClinicalCoding PrimaryProcedure
on	PrimaryProcedure.SourceCode = 'PRCAE'
and	PrimaryProcedure.ClinicalCodingSubtypeCode = 'PROCE'
and	PrimaryProcedure.SourceID = Episode.ProfessionalCareEpisodeID
and	PrimaryProcedure.SequenceNumber = 1

left join Warehouse.Lorenzo.ClinicalCoding PrimaryDiagnosis
on	PrimaryDiagnosis.SourceCode = 'PRCAE'
and	PrimaryDiagnosis.ClinicalCodingSubtypeCode = 'DIAGN'
and	PrimaryDiagnosis.SourceID = Episode.ProfessionalCareEpisodeID
and	PrimaryDiagnosis.SequenceNumber = 1

left join Warehouse.Lorenzo.ClinicalCoding PlannedProcedure
on	PlannedProcedure.SourceCode = 'WLIST'
and	PlannedProcedure.ClinicalCodingSubtypeCode = 'PROCE'
and	PlannedProcedure.SourceID = Episode.WaitingListID
and	PlannedProcedure.SequenceNumber = 1

left join Warehouse.Lorenzo.Referral
on	Referral.ReferralID = Episode.ReferralID

left join Warehouse.Lorenzo.RTT
on	RTT.ReferralID = Episode.ReferralID
and	RTT.StatusDateTime is not null
and	not exists
	(
	select
		1
	from
		Warehouse.Lorenzo.RTT Previous
	where
		Previous.ReferralID = Episode.ReferralID
	and	Previous.StatusDateTime is not null
	and	(
			Previous.StatusDateTime > RTT.StatusDateTime
		or	(
				Previous.StatusDateTime = RTT.StatusDateTime
			and	Previous.RTTID > RTT.RTTID
			)
		)
	)					

left join Warehouse.Lorenzo.RTT EpisodePeriodStatus
on	EpisodePeriodStatus.SourceCode = 'PRCAE'
and	EpisodePeriodStatus.SourceID = Episode.ProfessionalCareEpisodeID
and	EpisodePeriodStatus.StatusDateTime is not null
and	not exists
	(
	select
		1
	from
		Warehouse.Lorenzo.RTT Previous
	where
		Previous.SourceCode = 'PRCAE'
	and	Previous.SourceID = Episode.ProfessionalCareEpisodeID
	and	Previous.StatusDateTime is not null
	and	(
			Previous.StatusDateTime > EpisodePeriodStatus.StatusDateTime
		or	(
				Previous.StatusDateTime = EpisodePeriodStatus.StatusDateTime
			and	Previous.RTTID > EpisodePeriodStatus.RTTID
			)
		)
	)

left join Warehouse.Lorenzo.RTT SpellPeriodStatus
on	SpellPeriodStatus.SourceCode = 'PRVSP'
and	SpellPeriodStatus.SourceID = Spell.ProviderSpellID
and	SpellPeriodStatus.StatusDateTime is not null
and	not exists
	(
	select
		1
	from
		Warehouse.Lorenzo.RTT Previous
	where
		Previous.SourceCode = 'PRVSP'
	and	Previous.SourceID = Spell.ProviderSpellID
	and	Previous.StatusDateTime is not null
	and	(
			Previous.StatusDateTime > SpellPeriodStatus.StatusDateTime
		or	(
				Previous.StatusDateTime = SpellPeriodStatus.StatusDateTime
			and	Previous.RTTID > SpellPeriodStatus.RTTID
			)
		)
	)


insert
into
	#Encounter
	(
	EncounterRecno
	,ContextCode
	,Action
	)
select
	EncounterRecno
	,ContextCode = 'UHSM||LORENZO'
	,Action
from
	UHSMLorenzo.APCProcessList ProcessList
where
	ProcessList.Dataset = 'Encounter'
and	ProcessList.Action = 'DELETE'



create unique clustered index #IX_APCEncounter on #Encounter
	(
	 EncounterRecno ASC
	,ContextCode
	)


merge
	APC.Encounter target
using
	(
	select
		*
	from
		#Encounter
	) source
on	source.EncounterRecno = target.EncounterRecno
and source.ContextCode = target.ContextCode

when matched
and	source.Action = 'DELETE'
then
	delete

when not matched
then
	insert
		(
		 EncounterRecno
		,ContextCode
		,AdmittedCareID
		,SpellID
		,PatientID
		,EpisodeNumber
		,PatientTitle
		,PatientForename
		,PatientSurname
		,DateOfBirth
		,DateOfDeath
		,SexID
		,NHSNumber
		,NHSNumberStatusCode
		,DistrictNumber
		,CasenoteNumber
		,PatientAddressLine1
		,PatientAddressLine2
		,PatientAddressLine3
		,PatientAddressLine4
		,PatientPostcode
		,CarerSupportIndicator
		,ReferredByCode
		,ReferrerCode
		,ReferrerOrganisationCode
		,RegisteredGpCode
		,RegisteredGpPracticeCode
		,ReligionCode
		,MaritalStatusCode
		,EthnicCategoryCode
		,DateOnWaitingList
		,ManagementIntentionID
		,PatientClassificationID
		,IsMRSA
		,NeonatalLevelOfCare
		,IsWellBabyFlag
		,AdmissionMethodCode
		,AdmissionSourceCode
		,AdministrativeCategoryID
		,AdmissionTime
		,CancelledElectiveAdmissionFlag
		,ConsultantCode
		,TreatmentFunctionCode
		,ProviderCode
		,IsFirstEpisodeInSpell
		,EpisodeStartTime
		,StartSiteCode
		,StartWardCode
		,EpisodeEndTime
		,EndSiteCode
		,EndWardCode
		,IsLastEpisodeInSpell
		,LOE
		,LOS
		,ExpectedLOS
		,DischargeDestinationCode
		,DischargeMethodCode
		,DischargeTime
		,ClinicalCodingCompleteTime
		,ClinicalCodingStatusCode
		,PASHRGCode
		,OperationStatusID
		,PrimaryDiagnosisCode
		,SubsidiaryDiagnosisCode
		,IntendedPrimaryProcedureCode
		,PrimaryOperationCode
		,PrimaryOperationDate
		,RTTPathwayID
		,RTTPathwayCondition
		,RTTStartTime
		,RTTEndTime
		,RTTSpecialtyCode
		,RTTCurrentProviderCode
		,RTTCurrentStatusCode
		,RTTCurrentStatusTime
		,RTTCurrentPrivatePatientFlag
		,RTTOverseasStatusFlag
		,RTTPeriodStatusCode
		,TransferFrom
		,IsDiagnosticProcedure
		,OverseasVisitorStatusID
		,IsFirstRegularAdmission
		,EpisodeOutcomeCode
		,LegalStatusClassificationOnAdmissionCode
		,LegalStatusClassificationOnDischargeCode
		,DecisionToAdmitDateTime
		,OriginalDecisionToAdmitDateTime
		,DurationOfElectiveWait

		,Created
		,ByWhom
		)
	values
		(
		 source.EncounterRecno
		,source.ContextCode
		,source.AdmittedCareID
		,source.SpellID
		,source.PatientID
		,source.EpisodeNumber
		,source.PatientTitle
		,source.PatientForename
		,source.PatientSurname
		,source.DateOfBirth
		,source.DateOfDeath
		,source.SexID
		,source.NHSNumber
		,source.NHSNumberStatusCode
		,source.DistrictNumber
		,source.CasenoteNumber
		,source.PatientAddressLine1
		,source.PatientAddressLine2
		,source.PatientAddressLine3
		,source.PatientAddressLine4
		,source.PatientPostcode
		,source.CarerSupportIndicator
		,source.ReferredByCode
		,source.ReferrerCode
		,source.ReferrerOrganisationCode
		,source.RegisteredGpCode
		,source.RegisteredGpPracticeCode
		,source.ReligionCode
		,source.MaritalStatusCode
		,source.EthnicCategoryCode
		,source.DateOnWaitingList
		,source.ManagementIntentionID
		,source.PatientClassificationID
		,source.IsMRSA
		,source.NeonatalLevelOfCare
		,source.IsWellBabyFlag
		,source.AdmissionMethodCode
		,source.AdmissionSourceCode
		,source.AdministrativeCategoryID
		,source.AdmissionTime
		,source.CancelledElectiveAdmissionFlag
		,source.ConsultantCode
		,source.TreatmentFunctionCode
		,source.ProviderCode
		,source.IsFirstEpisodeInSpell
		,source.EpisodeStartTime
		,source.StartSiteCode
		,source.StartWardCode
		,source.EpisodeEndTime
		,source.EndSiteCode
		,source.EndWardCode
		,source.IsLastEpisodeInSpell
		,source.LOE
		,source.LOS
		,source.ExpectedLOS
		,source.DischargeDestinationCode
		,source.DischargeMethodCode
		,source.DischargeTime
		,source.ClinicalCodingCompleteTime
		,source.ClinicalCodingStatusCode
		,source.PASHRGCode
		,source.OperationStatusID
		,source.PrimaryDiagnosisCode
		,source.SubsidiaryDiagnosisCode
		,source.IntendedPrimaryProcedureCode
		,source.PrimaryOperationCode
		,source.PrimaryOperationDate
		,source.RTTPathwayID
		,source.RTTPathwayCondition
		,source.RTTStartTime
		,source.RTTEndTime
		,source.RTTSpecialtyCode
		,source.RTTCurrentProviderCode
		,source.RTTCurrentStatusCode
		,source.RTTCurrentStatusTime
		,source.RTTCurrentPrivatePatientFlag
		,source.RTTOverseasStatusFlag
		,source.RTTPeriodStatusCode
		,source.TransferFrom
		,source.IsDiagnosticProcedure
		,source.OverseasVisitorStatusID
		,source.IsFirstRegularAdmission
		,source.EpisodeOutcomeCode
		,source.LegalStatusClassificationOnAdmissionCode
		,source.LegalStatusClassificationOnDischargeCode
		,source.DecisionToAdmitDateTime
		,source.OriginalDecisionToAdmitDateTime
		,source.DurationOfElectiveWait
		,getdate()
		,suser_name()
		)


when matched
and	source.Action <> 'DELETE'
then
	update
	set
		 target.AdmittedCareID = source.AdmittedCareID
		,target.SpellID = source.SpellID
		,target.PatientID = source.PatientID
		,target.EpisodeNumber = source.EpisodeNumber
		,target.PatientTitle = source.PatientTitle
		,target.PatientForename = source.PatientForename
		,target.PatientSurname = source.PatientSurname
		,target.DateOfBirth = source.DateOfBirth
		,target.DateOfDeath = source.DateOfDeath
		,target.SexID = source.SexID
		,target.NHSNumber = source.NHSNumber
		,target.NHSNumberStatusCode = source.NHSNumberStatusCode
		,target.DistrictNumber = source.DistrictNumber
		,target.CasenoteNumber = source.CasenoteNumber
		,target.PatientAddressLine1 = source.PatientAddressLine1
		,target.PatientAddressLine2 = source.PatientAddressLine2
		,target.PatientAddressLine3 = source.PatientAddressLine3
		,target.PatientAddressLine4 = source.PatientAddressLine4
		,target.PatientPostcode = source.PatientPostcode
		,target.CarerSupportIndicator = source.CarerSupportIndicator
		,target.ReferredByCode = source.ReferredByCode
		,target.ReferrerCode = source.ReferrerCode
		,target.ReferrerOrganisationCode = source.ReferrerOrganisationCode
		,target.RegisteredGpCode = source.RegisteredGpCode
		,target.RegisteredGpPracticeCode = source.RegisteredGpPracticeCode
		,target.ReligionCode = source.ReligionCode
		,target.MaritalStatusCode = source.MaritalStatusCode
		,target.EthnicCategoryCode = source.EthnicCategoryCode
		,target.DateOnWaitingList = source.DateOnWaitingList
		,target.ManagementIntentionID = source.ManagementIntentionID
		,target.PatientClassificationID = source.PatientClassificationID
		,target.IsMRSA = source.IsMRSA
		,target.NeonatalLevelOfCare = source.NeonatalLevelOfCare
		,target.IsWellBabyFlag = source.IsWellBabyFlag
		,target.AdmissionMethodCode = source.AdmissionMethodCode
		,target.AdmissionSourceCode = source.AdmissionSourceCode
		,target.AdministrativeCategoryID = source.AdministrativeCategoryID
		,target.AdmissionTime = source.AdmissionTime
		,target.CancelledElectiveAdmissionFlag = source.CancelledElectiveAdmissionFlag
		,target.ConsultantCode = source.ConsultantCode
		,target.TreatmentFunctionCode = source.TreatmentFunctionCode
		,target.ProviderCode = source.ProviderCode
		,target.IsFirstEpisodeInSpell = source.IsFirstEpisodeInSpell
		,target.EpisodeStartTime = source.EpisodeStartTime
		,target.StartSiteCode = source.StartSiteCode
		,target.StartWardCode = source.StartWardCode
		,target.EpisodeEndTime = source.EpisodeEndTime
		,target.EndSiteCode = source.EndSiteCode
		,target.EndWardCode = source.EndWardCode
		,target.IsLastEpisodeInSpell = source.IsLastEpisodeInSpell
		,target.LOE = source.LOE
		,target.LOS = source.LOS
		,target.ExpectedLOS = source.ExpectedLOS
		,target.DischargeDestinationCode = source.DischargeDestinationCode
		,target.DischargeMethodCode = source.DischargeMethodCode
		,target.DischargeTime = source.DischargeTime
		,target.ClinicalCodingCompleteTime = source.ClinicalCodingCompleteTime
		,target.ClinicalCodingStatusCode = source.ClinicalCodingStatusCode
		,target.PASHRGCode = source.PASHRGCode
		,target.OperationStatusID = source.OperationStatusID
		,target.PrimaryDiagnosisCode = source.PrimaryDiagnosisCode
		,target.SubsidiaryDiagnosisCode = source.SubsidiaryDiagnosisCode
		,target.IntendedPrimaryProcedureCode = source.IntendedPrimaryProcedureCode
		,target.PrimaryOperationCode = source.PrimaryOperationCode
		,target.PrimaryOperationDate = source.PrimaryOperationDate
		,target.RTTPathwayID = source.RTTPathwayID
		,target.RTTPathwayCondition = source.RTTPathwayCondition
		,target.RTTStartTime = source.RTTStartTime
		,target.RTTEndTime = source.RTTEndTime
		,target.RTTSpecialtyCode = source.RTTSpecialtyCode
		,target.RTTCurrentProviderCode = source.RTTCurrentProviderCode
		,target.RTTCurrentStatusCode = source.RTTCurrentStatusCode
		,target.RTTCurrentStatusTime = source.RTTCurrentStatusTime
		,target.RTTCurrentPrivatePatientFlag = source.RTTCurrentPrivatePatientFlag
		,target.RTTOverseasStatusFlag = source.RTTOverseasStatusFlag
		,target.RTTPeriodStatusCode = source.RTTPeriodStatusCode
		,target.TransferFrom = source.TransferFrom
		,target.IsDiagnosticProcedure = source.IsDiagnosticProcedure
		,target.OverseasVisitorStatusID = source.OverseasVisitorStatusID
		,target.IsFirstRegularAdmission = source.IsFirstRegularAdmission
		,target.EpisodeOutcomeCode = source.EpisodeOutcomeCode
		,target.LegalStatusClassificationOnAdmissionCode = source.LegalStatusClassificationOnAdmissionCode
		,target.LegalStatusClassificationOnDischargeCode = source.LegalStatusClassificationOnDischargeCode
		,target.DecisionToAdmitDateTime = source.DecisionToAdmitDateTime
		,target.OriginalDecisionToAdmitDateTime = source.OriginalDecisionToAdmitDateTime
		,target.DurationOfElectiveWait = source.DurationOfElectiveWait
		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	 coalesce(inserted.MergeRecno, deleted.MergeRecno)
	,$action
into
	@ProcessList

;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + cast(@Elapsed as varchar) + ' minutes'

exec Audit.WriteLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
