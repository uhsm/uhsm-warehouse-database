﻿CREATE procedure UHSMLorenzo.LoadAPC
as

declare @ReturnValue int = 0

declare @Process varchar(255) =  OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @LastLoadTime datetime =
	Utility.GetLastLoadTime(@Process);

if @LastLoadTime is null
	set @LastLoadTime = '1 Jan 1900' --default to start of time
;

BEGIN TRY

	--clear process list for this context
	truncate table UHSMLorenzo.APCProcessList


	insert into
		UHSMLorenzo.APCProcessList
		(
		EncounterRecno
		,Dataset
		,Action
		)
	select
		 EncounterRecno = Activity.ProfessionalCareEpisodeID
		,Dataset = 'Encounter'
		,Action = case when Activity.Updated is null then 'INSERT' else 'UPDATE' end
	from
		Warehouse.Lorenzo.ProfessionalCareEpisode Activity
	where
		not exists
		(
		select
			1 
		from
			Warehouse.Lorenzo.TestPatient
		where
			TestPatient.PatientID = Activity.PatientID
		)

	and	(
			isnull(Activity.Updated, Activity.Created) > @LastLoadTime

		or	not exists
			(
			select
				1
			from
				APC.Encounter
			where
				Encounter.EncounterRecno = Activity.ProfessionalCareEpisodeID
			and	Encounter.ContextCode = 'UHSM||LORENZO'
			)
		)

	union all

	select
		Activity.EncounterRecno
		,Dataset = 'Encounter'
		,Action = 'DELETE'
	from
		APC.Encounter Activity
	where
		Activity.ContextCode = 'UHSM||LORENZO'
	and	(
			not exists
			(
			select
				1
			from
				Warehouse.Lorenzo.ProfessionalCareEpisode ConsultantEpisode
			where
				ConsultantEpisode.ProfessionalCareEpisodeID = Activity.EncounterRecno
			)
		or	exists
			(
			select
				1 
			from
				Warehouse.Lorenzo.TestPatient
			where
				TestPatient.PatientID = Activity.PatientID
			)
		)


	--Add implied updates due to other dataset changes
	insert into
		UHSMLorenzo.APCProcessList
		(
		 EncounterRecno
		,Dataset
		,Action
		)
	select
		 EncounterRecno = ConsultantEpisode.ProfessionalCareEpisodeID
		,Dataset = 'Encounter'
		,Action = 'UPDATE'
	from
		Warehouse.Lorenzo.ProfessionalCareEpisode ConsultantEpisode
	where
		(
		--Add implied updates due to patient changes
			exists
			(
			select
				1
			from
				Warehouse.Lorenzo.Patient
			where
				isnull(Patient.Updated, Patient.Created) > @LastLoadTime
			and	Patient.PatientID = ConsultantEpisode.PatientID
			)

		--Add implied updates due to PatientIdentifier changes
		or	exists
			(
			select
				1
			from
				Warehouse.Lorenzo.PatientIdentifier
			where
				isnull(PatientIdentifier.Updated, PatientIdentifier.Created) > @LastLoadTime
			and	PatientIdentifier.PatientID = ConsultantEpisode.PatientID
			and	PatientIdentifier.PatientIdentifierTypeID = 2001232 --facility
			)

		--Add implied updates due to PatientAddressRole changes
		or	exists
			(
			select
				1
			from
				Warehouse.Lorenzo.PatientAddressRole
			where
				isnull(PatientAddressRole.Updated, PatientAddressRole.Created) > @LastLoadTime
			and	PatientAddressRole.PatientID = ConsultantEpisode.PatientID
			and	PatientAddressRole.RollTypeCode = 'HOME'
			)

		--Add implied updates due to PatientAddress changes
		or	exists
			(
			select
				1
			from
				Warehouse.Lorenzo.PatientAddress

			inner join Warehouse.Lorenzo.PatientAddressRole
			on	PatientAddressRole.AddressID = PatientAddress.PatientAddressID
			and	PatientAddressRole.PatientID = ConsultantEpisode.PatientID
			and	PatientAddressRole.RollTypeCode = 'HOME'

			where
				isnull(PatientAddress.Updated, PatientAddress.Created) > @LastLoadTime
			)

		--Add implied updates due to PatientProfessionalCarer changes
		or	exists
			(
			select
				1
			from
				Warehouse.Lorenzo.PatientProfessionalCarer
			where
				isnull(PatientProfessionalCarer.Updated, PatientProfessionalCarer.Created) > @LastLoadTime
			and	PatientProfessionalCarer.PatientID = ConsultantEpisode.PatientID
			)

		--Add implied updates due to Contract changes
		or	exists
			(
			select
				1
			from
				Warehouse.Lorenzo.Contract
			where
				isnull(Contract.Updated, Contract.Created) > @LastLoadTime
			and	Contract.ContractID = ConsultantEpisode.ContractID
			)

		--Add implied updates due to Referral changes
		or	exists
			(
			select
				1
			from
				Warehouse.Lorenzo.Referral
			where
				isnull(Referral.Updated, Referral.Created) > @LastLoadTime
			and	Referral.ReferralID = ConsultantEpisode.ReferralID
			)

		--Add implied updates due to ServicePoint changes
		or	exists
			(
			select
				1
			from
				Warehouse.Lorenzo.ServicePoint
			where
				isnull(ServicePoint.Updated, ServicePoint.Created) > @LastLoadTime
			and	ServicePoint.ServicePointID = ConsultantEpisode.ServicePointID
			)

		--Add implied updates due to OverseasVisitorStatus changes
		or	exists
			(
			select
				1
			from
				Warehouse.Lorenzo.OverseasVisitorStatus
			where
				isnull(OverseasVisitorStatus.Updated, OverseasVisitorStatus.Created) > @LastLoadTime
			and	OverseasVisitorStatus.PatientID = ConsultantEpisode.PatientID
			)

		--Add implied updates due to ProviderSpell changes
		or	exists
			(
			select
				1
			from
				Warehouse.Lorenzo.ProviderSpell
			where
				isnull(ProviderSpell.Updated, ProviderSpell.Created) > @LastLoadTime
			and	ProviderSpell.ProviderSpellID = ConsultantEpisode.ProviderSpellID
			)

		--Add implied updates due to Birth changes
		or	exists
			(
			select
				1
			from
				Warehouse.Lorenzo.Birth
			where
				isnull(Birth.Updated, Birth.Created) > @LastLoadTime
			and	Birth.PatientID = ConsultantEpisode.PatientID
			and	cast(Birth.DateOfBirth as date) = cast(ConsultantEpisode.[StartDateTime] as date)
			)

		--Add implied updates due to ServicePointStay changes
		or	exists
			(
			select
				1
			from
				Warehouse.Lorenzo.ServicePointStay
			where
				isnull(ServicePointStay.Updated, ServicePointStay.Created) > @LastLoadTime
			and	ServicePointStay.ProviderSpellID = ConsultantEpisode.ProviderSpellID
			)

		--Add implied updates due to ClinicalCoding changes
		or	exists
			(
			select
				1
			from
				Warehouse.Lorenzo.ClinicalCoding
			where
				isnull(ClinicalCoding.Updated, ClinicalCoding.Created) > @LastLoadTime
			and	ClinicalCoding.PatientID = ConsultantEpisode.PatientID
			and	ClinicalCoding.StartDateTime <= ConsultantEpisode.[EndDateTime]
			and	ClinicalCoding.ClinicalCodingCode in ('009', 'INFC006')
			and	ClinicalCoding.ClinicalCodingSubtypeCode = 'ALERT'
			)

		--Add implied updates due to ClinicalCoding changes
		or	exists
			(
			select
				1
			from
				Warehouse.Lorenzo.ClinicalCoding
			where
				isnull(ClinicalCoding.Updated, ClinicalCoding.Created) > @LastLoadTime
			and	ClinicalCoding.SourceID = ConsultantEpisode.ProfessionalCareEpisodeID
			and	ClinicalCoding.SourceCode = 'PRCAE'
			and	ClinicalCoding.StartDateTime <= ConsultantEpisode.[EndDateTime]
			)

		--Add implied updates due to AdmissionOffer changes
		or	exists
			(
			select
				1
			from
				Warehouse.Lorenzo.AdmissionOffer
			where
				isnull(AdmissionOffer.Updated, AdmissionOffer.Created) > @LastLoadTime
			and	AdmissionOffer.WaitingListID = ConsultantEpisode.WaitingListID
			)

		--Add implied updates due to AdmissionDecision changes
		or	exists
			(
			select
				1
			from
				Warehouse.Lorenzo.AdmissionDecision
			where
				isnull(AdmissionDecision.Updated, AdmissionDecision.Created) > @LastLoadTime
			and	AdmissionDecision.ReferralID = ConsultantEpisode.ReferralID
			)

		--Add implied updates due to WaitingList changes
		or	exists
			(
			select
				1
			from
				Warehouse.Lorenzo.WaitingList
			where
				isnull(WaitingList.Updated, WaitingList.Created) > @LastLoadTime
			and	WaitingList.WaitingListID = ConsultantEpisode.WaitingListID
			)

		--Add implied updates due to ClinicalCoding changes
		or	exists
			(
			select
				1
			from
				Warehouse.Lorenzo.ClinicalCoding
			where
				isnull(ClinicalCoding.Updated, ClinicalCoding.Created) > @LastLoadTime
			and	ClinicalCoding.SourceID = ConsultantEpisode.WaitingListID
			and	ClinicalCoding.SourceCode = 'WLIST'
			and	ClinicalCoding.StartDateTime <= ConsultantEpisode.EndDateTime
			)
		)

	and	not exists
		(
		select
			1
		from
			UHSMLorenzo.APCProcessList ProcessList
		where
			ProcessList.EncounterRecno = ConsultantEpisode.ProfessionalCareEpisodeID
		and	ProcessList.Dataset = 'Encounter'
		)

	and not exists
		(
		select
			1 
		from
			Warehouse.Lorenzo.TestPatient
		where
			TestPatient.PatientID = ConsultantEpisode.PatientID
		)



	--import the data
	exec UHSMLorenzo.BuildAPCEncounter


	set @LastLoadTime = getdate()
	exec Utility.SetLastLoadTime @Process, @LastLoadTime



END TRY

BEGIN
    CATCH
	
	set @ReturnValue = @@ERROR

    DECLARE @ErrorSeverity INT,
            @ErrorNumber   INT,
            @ErrorMessage nvarchar(4000),
            @ErrorState INT,
            @ErrorLine  INT,
            @ErrorProc nvarchar(200)
            -- Grab error information from SQL functions
    SET @ErrorSeverity = ERROR_SEVERITY()
    SET @ErrorNumber   = ERROR_NUMBER()
    SET @ErrorMessage  = ERROR_MESSAGE()
    SET @ErrorState    = ERROR_STATE()
    SET @ErrorLine     = ERROR_LINE()
    SET @ErrorProc     = ERROR_PROCEDURE()
    SET @ErrorMessage  = 'An error occurred.' + CHAR(13) + 'SQL Server Error Message is: ' + CAST(@ErrorNumber AS VARCHAR(10)) + ' in procedure: ' + @ErrorProc + ' Line: ' + CAST(@ErrorLine AS VARCHAR(10)) + ' Error text: ' + @ErrorMessage
    -- Not all errors generate an error state, to set to 1 if it's zero
    IF @ErrorState  = 0
    SET @ErrorState = 1
    -- If the error renders the transaction as uncommittable or we have open transactions, we may want to rollback
    IF @@TRANCOUNT > 0
    BEGIN
            --print 'Rollback transaction'
            ROLLBACK TRANSACTION
    END
    RAISERROR (@ErrorMessage , @ErrorSeverity, @ErrorState, @ErrorNumber)
END CATCH


RETURN @ReturnValue

