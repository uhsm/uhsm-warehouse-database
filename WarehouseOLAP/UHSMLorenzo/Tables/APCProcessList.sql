﻿CREATE TABLE [UHSMLorenzo].[APCProcessList] (
    [EncounterRecno] INT           NOT NULL,
    [Dataset]        VARCHAR (50)  NOT NULL,
    [Action]         NVARCHAR (10) NOT NULL,
    CONSTRAINT [PK_UHSMLorenzo_APCProcessList] PRIMARY KEY CLUSTERED ([Dataset] ASC, [EncounterRecno] ASC)
);

