﻿CREATE FUNCTION [Utility].GetLastLoadTime
(
	 @Process varchar(255)
)
RETURNS datetime

as

begin

return
	(
	select
		LastLoadTime = Parameter.DateValue
	from
		Utility.Parameter
	where
		Parameter.Parameter = 'Last load time: ' + @Process + ' (System Generated)'
	)

end
