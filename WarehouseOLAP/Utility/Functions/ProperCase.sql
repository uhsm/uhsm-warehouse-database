﻿
CREATE FUNCTION Utility.ProperCase(@Text as varchar(512)) RETURNS varchar(512) as
BEGIN

	if (@Text is null) return null

	DECLARE @Reset bit
	DECLARE @Ret varchar(512)
	DECLARE @i int
	DECLARE @c char(1)

	SELECT @Reset = 1, @i=1, @Ret = ''

	WHILE @i <= LEN(@Text)
		SELECT @c= SUBSTRING(@Text,@i,1),
		@Ret = @Ret + CASE WHEN @Reset=1 THEN UPPER(@c) ELSE LOWER(@c) END,
		@Reset= 
			CASE WHEN 
				CASE WHEN SUBSTRING(@Text,@i-4,5) like '_[a-z] [DOL]''' THEN 1 
				WHEN SUBSTRING(@Text,@i-4,5) like '_[a-z] [D][I]' THEN 1 
				WHEN SUBSTRING(@Text,@i-4,5) like '_[a-z] [M][C]' THEN 1 
				ELSE 0 
				END = 1 
			THEN 1 
			ELSE 
				CASE WHEN @c like '[a-zA-Z]' or @c in ('''') THEN 0 
			ELSE 1 
			END 
	END,
	
	@i = @i +1
	
	return 
		replace(
		replace(
		replace(
			@Ret
			,'Nhs'
			,'NHS'
		)
			,'Ccg'
			,'CCG'
		)
			,'Lhb'
			,'LHB'
		)

end
