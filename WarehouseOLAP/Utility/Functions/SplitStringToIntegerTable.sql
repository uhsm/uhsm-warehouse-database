﻿CREATE FUNCTION [Utility].[SplitStringToIntegerTable]
(	@StringToSplit 	varchar(8000),
	@Delimiter 	char(1)
)

RETURNS @SplitTable TABLE
(	DataValue	int,
	ValueAt		int IDENTITY(1,1)
)
AS 
BEGIN

  DECLARE @End int
  DECLARE @Start int
  DECLARE @Len int

  SET @Len=LEN(@StringToSplit)

  IF @Len = 0 RETURN

	SET @Delimiter = LTRIM(RTRIM(@Delimiter))
	SET @Start = 1
  SET @End=0

  WHILE @Start<=@Len BEGIN

	  SET @End =CHARINDEX(@Delimiter, @StringToSplit, @Start)
    IF @End=0 BEGIN
      
      
      INSERT @SplitTable
      SELECT  CONVERT(int,NULLIF(LTRIM(RTRIM(SUBSTRING(@StringToSplit,@Start,@Len))),''))
      RETURN

    END ELSE BEGIN

      INSERT @SplitTable
      SELECT CONVERT(int,NULLIF(LTRIM(RTRIM(SUBSTRING(@StringToSplit,@Start,@End-@Start))),''))
      SET @Start=@End+1

    END

  END

  RETURN

END