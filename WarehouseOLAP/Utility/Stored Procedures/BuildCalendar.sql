﻿create procedure [Utility].[BuildCalendar] as

set datefirst 1 --monday


BEGIN TRANSACTION;

delete
from 
	WH.Calendar
where
	not exists
		(
		select
		1
		from
			WH.CalendarBase
		where
			CalendarBase.TheDate = WH.Calendar.TheDate
		);

insert into WH.Calendar
	(
	DateID
	,TheDate
	,TheDay
	,DayOfWeekKey
	,LongDate
	,TheMonth
	,TheMonthKey
	,FinancialQuarter
	,FinancialYear
	,FinancialMonthKey
	,FinancialQuarterKey
	,CalendarQuarter
	,CalendarSemester
	,CalendarYear
	,LastCompleteMonth
	,WeekNoKey
	,WeekNo
	,FirstDayOfWeek
	,LastDayOfWeek
	,FirstDayOfMonth
	,LastDayOfMonth
	,MonthYear
	)
select
	 DateID = cast(convert(varchar, TheDate, 112) as int)
	,TheDate
	,TheDay = coalesce(DefaultDate.DefaultDate, Calendar.TheDay)
	,DayOfWeekKey = coalesce(DefaultDate.DefaultKey, Calendar.DayOfWeekKey)
	,LongDate = coalesce(DefaultDate.DefaultDate, Calendar.LongDate)
	,TheMonth = coalesce(DefaultDate.DefaultDate, Calendar.TheMonth)
	,TheMonthKey = coalesce(DefaultDate.DefaultKey, Calendar.TheMonthKey)
	,FinancialQuarter = coalesce(DefaultDate.DefaultDate, Calendar.FinancialQuarter)
	,FinancialYear = coalesce(DefaultDate.DefaultDate, Calendar.FinancialYear)
	,FinancialMonthKey = coalesce(DefaultDate.DefaultKey, Calendar.FinancialMonthKey)
	,FinancialQuarterKey = coalesce(DefaultDate.DefaultKey, Calendar.FinancialQuarterKey)
	,CalendarQuarter = coalesce(DefaultDate.DefaultDate, Calendar.CalendarQuarter)
	,CalendarSemester = coalesce(DefaultDate.DefaultDate, Calendar.CalendarSemester)
	,CalendarYear = coalesce(DefaultDate.DefaultDate, Calendar.CalendarYear)
	,LastCompleteMonth
	,WeekNoKey = coalesce(DefaultDate.DefaultKey, Calendar.WeekNoKey)
	,WeekNo = coalesce(DefaultDate.DefaultDate, Calendar.WeekNo)
	,FirstDayOfWeek
	,LastDayOfWeek
	,FirstDayOfMonth
	,LastDayOfMonth
	,MonthYear = coalesce(DefaultDate.DefaultDate, Calendar.MonthYear)

from
	(
	select
		 TheDate

		,TheDay =
			datename(weekday, TheDate)

		,DayOfWeekKey =
			datepart(weekday, TheDate)

		,LongDate =
			left(convert(varchar, TheDate, 113), 11)

		,TheMonth =
			left(datename(month, TheDate), 3) 

		,TheMonthKey = 
			case 
			when datepart(month, TheDate) <= 3 then datepart(month, TheDate) + 9
			else datepart(month, TheDate) - 3
			end

		,FinancialQuarter =
			'Qtr ' + 
			case datepart(quarter, TheDate)
			when 1 then '4'
			else convert(varchar, datepart(quarter, TheDate) - 1)
			end + ' ' + FinancialYear
		

		,FinancialYear
		,FinancialMonthKey
		,FinancialQuarterKey

		,CalendarQuarter
		,CalendarSemester
		,CalendarYear

	--derived as 1 if either last day of month or 
		,LastCompleteMonth =
			case
			when
				datepart(day, dateadd(day, 1, getdate())) = 1 
			and	TheDate = dateadd(day, -1, datepart(day, dateadd(day, 1, getdate())))
			then 1 --it is the last day of month
			when
				TheDate = dateadd(month, -1, dateadd(day, datepart(day, getdate()) * -1 + 1, dateadd(day, datediff(day, 0, getdate()), 0))) --mid month so go back to start of last month
			then 1
			else 0
			end

		,WeekNoKey =
			datename(year, TheDate) + right('00' + datename(week, TheDate), 2) 

		,WeekNo =
			'Week Ending ' + left(convert(varchar, dateadd(day, 6, FirstDayOfWeek), 113), 11) 

		,FirstDayOfWeek

		,LastDayOfWeek =
			dateadd(day, +6, FirstDayOfWeek)

		,FirstDayOfMonth
		,LastDayOfMonth  = dateadd(dd,-1,dateadd(month, datediff(month,0,[TheDate])+1, 0))

		,MonthYear =
			left(datename(month, TheDate), 3) + ' ' + datename(year, TheDate) 

	from
		(
		select
			TheDate

			,FinancialYear =
				case datepart(quarter, TheDate)
				when 1 then convert(char(4),datepart(year,dateadd(year,-1, convert(datetime, TheDate)))) + '/' + datename(year, TheDate)
				else datename(year, TheDate) + '/' + convert(char(4),datepart(year,dateadd(year,1, TheDate)))
				end

			,FinancialMonthKey =
				case datepart(quarter, TheDate)
				when 1 then datepart(year, TheDate) - 1
				else datepart(year, TheDate)
				end * 100 + 
				case datepart(quarter, TheDate)
				when 1 then 9 else -3 
				end + datepart(month, TheDate)

			,FinancialQuarterKey =
				case datepart(quarter, TheDate)
				when 1 then datepart(year, TheDate) - 1
				else datepart(year, TheDate)
				end * 100 + 
				case datepart(quarter, TheDate)
				when 1 then 4
				else datepart(quarter, TheDate) - 1
				end

			,FirstDayOfWeek = dateadd(day,-(datepart(weekday,convert(date,TheDate))+6)%7,convert(date,TheDate))
				------ ** APB edited was returned incorrect date at start of year date 2014-01-16
				--------(
				--------select
				--------	min(StartOfWeek.TheDate)
				--------from
				--------	WH.CalendarBase StartOfWeek
				--------where
				--------	datepart(week, StartOfWeek.TheDate) = datepart(week, CalendarBase.TheDate)
				--------and	datepart(year, StartOfWeek.TheDate) = datepart(year, CalendarBase.TheDate)
				--------)

			,FirstDayOfMonth =
				convert(smalldatetime, '01 ' + datename(month, TheDate) + ' ' + datename(year, TheDate)) 

		--calendar hierarchy
			,CalendarQuarter =
				'Qtr ' + convert(char,datepart(quarter,TheDate))
	 
			,CalendarSemester =
				case when datepart(quarter, TheDate) < 3 then '1st Semester' else '2nd Semester' end

			,CalendarYear =
				datename(year,TheDate) 

		from
			WH.CalendarBase
		where
			not exists
				(
				select
					1
				from
					WH.Calendar CurrentCalendar
				where
					CurrentCalendar.TheDate = WH.CalendarBase.TheDate
				)

		and	CalendarBase.TheDate not in
			(
			 '1 Jan 1900'
			,'2 Jan 1900'
			,'31 Dec 9999'
			)
		) Calendar
	) Calendar

left join
	(
	select
		 DefaultTheDate = '1 Jan 1900'
		,DefaultDate = 'N/A'
		,DefaultKey = -1
	) DefaultDate
on	DefaultDate.DefaultTheDate = Calendar.TheDate


insert into WH.Calendar
	(
	DateID
	,TheDate
	,TheDay
	,DayOfWeekKey
	,LongDate
	,TheMonth
	,TheMonthKey
	,FinancialQuarter
	,FinancialYear
	,FinancialMonthKey
	,FinancialQuarterKey
	,CalendarQuarter
	,CalendarSemester
	,CalendarYear
	,LastCompleteMonth
	,WeekNoKey
	,WeekNo
	,FirstDayOfWeek
	,LastDayOfWeek
	,FirstDayOfMonth
	,LastDayOfMonth
	,MonthYear
	)
select
	*
from
	(
	select
		DateID = 99991231
		,TheDate = '31 dec 9999'
		,TheDay = 'N/A'
		,DayOfWeekKey = 9
		,LongDate = 'Future Date'
		,TheMonth = 'Future Month'
		,TheMonthKey = 999912
		,FinancialQuarter = 'N/A'
		,FinancialYear = 'Future Year'
		,FinancialMonthKey = 999909
		,FinancialQuarterKey = 999903
		,CalendarQuarter = 'Future Qtr'
		,CalendarSemester = 'Future Semester'
		,CalendarYear = 'Future Year'
		,LastCompleteMonth = 0
		,WeekNoKey = '999953'
		,WeekNo = 'Future Week'
		,FirstDayOfWeek = '31 dec 9999'
		,LastDayOfWeek = '31 dec 9999'
		,FirstDayOfMonth = '1 dec 9999'
		,LastDayOfMonth = '31 dec 9999'
		,MonthYear = 'Future Month'

	union

	select
		DateID = 19000101
		,TheDate = '1 Jan 1900'
		,TheDay = 'N/A'
		,DayOfWeekKey = 9
		,LongDate = 'N/A'
		,TheMonth = 'N/A'
		,TheMonthKey = 190001
		,FinancialQuarter = 'N/A'
		,FinancialYear = 'No Date'
		,FinancialMonthKey = 190001
		,FinancialQuarterKey = 190001
		,CalendarQuarter = 'N/A'
		,CalendarSemester = 'N/A'
		,CalendarYear = 'N/A'
		,LastCompleteMonth = 0
		,WeekNoKey = '190001'
		,WeekNo = 'N/A'
		,FirstDayOfWeek = '1 Jan 1900'
		,LastDayOfWeek = '1 Jan 1900'
		,FirstDayOfMonth = '1 Jan 1900'
		,LastDayOfMonth = '1 Jan 1900'
		,MonthYear = 'N/A'

	union

	select
		DateID = 19000102
		,TheDate = '2 Jan 1900'
		,TheDay = 'N/A'
		,DayOfWeekKey = 9
		,LongDate = 'Previous Date'
		,TheMonth = 'Previous Month'
		,TheMonthKey = 190001
		,FinancialQuarter = 'N/A'
		,FinancialYear = 'Previous Year'
		,FinancialMonthKey = 190009
		,FinancialQuarterKey = 999901
		,CalendarQuarter = 'Previous Qtr'
		,CalendarSemester = 'Previous Semester'
		,CalendarYear = 'Previous Year'
		,LastCompleteMonth = 0
		,WeekNoKey = '190002'
		,WeekNo = 'Previous Week'
		,FirstDayOfWeek = '2 Jan 1900'
		,LastDayOfWeek = '2 Jan 1900'
		,FirstDayOfMonth = '2 Jan 1900'
		,LastDayOfMonth = '2 Jan 1900'
		,MonthYear = 'Previous Month'

	) SystemDate
where
	not exists
	(
	select
		1
	from
		WH.Calendar
	where
		Calendar.TheDate = SystemDate.TheDate
	)


COMMIT TRANSACTION;
