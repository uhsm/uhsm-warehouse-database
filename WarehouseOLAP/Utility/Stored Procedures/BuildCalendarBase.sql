﻿CREATE procedure [Utility].[BuildCalendarBase]
as

declare @FromDate date =
	(
	select
		Parameter.DateValue
	from
		Utility.Parameter
	where
		Parameter.Parameter = 'CALENDARSTARTDATE'
	)

declare @FutureFinancialYears int =
	(
	select
		Parameter.NumericValue
	from
		Utility.Parameter
	where
		Parameter.Parameter = 'CALENDARFUTUREFINANCIALYEARS'
	)

declare @ToDate date =
	cast(
		'31 Mar ' +
		case

		when datepart(quarter, getdate()) = 1
		then datename(year, dateadd(year, @FutureFinancialYears, getdate()))
		else datename(year, dateadd(year, @FutureFinancialYears + 1, getdate()))

		end

		as date
	)


declare @NumberOfDayIntervals int = datediff(d , @FromDate , @ToDate);

if not exists
	(
	select
		TheDate
	FROM
		WH.CalendarBase
	where
		TheDate = '1 Jan 1900'
	)

	insert into WH.CalendarBase
	values
	(
		'1 jan 1900'
	);

if not exists
	(
	select
		TheDate
	FROM
		WH.CalendarBase
	where
		TheDate = '2 Jan 1900'
	)

	insert into WH.CalendarBase
	values
	(
		'2 jan 1900'
	);

if not exists
	(
	select
		TheDate
	FROM
		WH.CalendarBase
	where
		TheDate = '31 dec 9999'
	)

	insert into WH.CalendarBase
	values
	(
		'31 dec 9999'
	);


insert WH.CalendarBase
(
	TheDate
)

select
	Result.TheDate
from
	(
	select
		dateadd(d , IntegerBase.TheInteger , @FromDate) TheDate
	FROM
		Utility.IntegerBase
	
	LEFT OUTER JOIN WH.CalendarBase 
	ON dateadd(d , IntegerBase.TheInteger , @FromDate) = CalendarBase.TheDate 

	WHERE
		IntegerBase.TheInteger <= @NumberOfDayIntervals
	AND	CalendarBase.TheDate IS NULL
	) Result;

if @@ROWCOUNT > 0
begin
	exec Utility.BuildCalendar

	update
		WH.Calendar
	set
		LongDate = 'Post ' +
		(
		select top 1
			Calendar.TheMonth + ' ' + Calendar.CalendarYear
		from
			WH.Calendar
		where
			TheDate =
			(
			select
				max(Calendar.TheDate)
			from
				WH.Calendar
			where
				TheDate < '31 dec 9999'
			)
		)
	where
		TheDate = '31 dec 9999'


end
