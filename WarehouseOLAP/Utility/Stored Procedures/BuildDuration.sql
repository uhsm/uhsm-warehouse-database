﻿CREATE procedure [Utility].[BuildDuration] as

declare @MaxDurationMinutes int = 364 * 24 * 60; -- 52 weeks

truncate table WH.Duration;

with RecurseMinutes as (
	select
		Minutes = 0
	
	union all
	
	select
		Minutes + 1 
	from
		RecurseMinutes
	where
		Minutes + 1 <= @MaxDurationMinutes

), Counts as (
	select
		 MinuteCount = Minutes
		,FiveMinuteCount = Minutes / 5
		,TenMinuteCount = Minutes / 10
		,FifteenMinuteCount = Minutes / 15
		,ThirtyMinuteCount = Minutes / 30
		,HourCount = Minutes / 60
		,DayCount = Minutes / (24 * 60)
		,WeekCount = Minutes / (24 * 60 * 7)
	from
		RecurseMinutes

), BandRanges as (
	-- calculate counts of minutes
	-- and band min / max values
	select
		 Counts.*
		,FiveMinuteBandMin = FiveMinuteCount * 5
		,FiveMinuteBandMax = (FiveMinuteCount * 5) + 5
		,TenMinuteBandMin = TenMinuteCount * 10
		,TenMinuteBandMax = (TenMinuteCount * 10) + 10
		,FifteenMinuteBandMin = FifteenMinuteCount * 15
		,FifteenMinuteBandMax = (FifteenMinuteCount * 15) + 15
		,ThirtyMinuteBandMin = ThirtyMinuteCount * 30
		,ThirtyMinuteBandMax = (ThirtyMinuteCount * 30) + 30
		,HourBandMin = HourCount * 60
		,HourBandMax = (HourCount * 60) + 60
		,DayBandMin = DayCount * (60 * 24)
		,DayBandMax = (DayCount + 1) * (60 * 24)
		,WeekBandMin = WeekCount * (60 * 24 * 7)
		,WeekBandMax = (WeekCount + 1) * (60 * 24 * 7)

	from Counts

), FormattedBandRanges as (
	-- format the start and end values of bands
	select
		 BandRanges.MinuteCount
		,MinutesFormatted = cast(MinuteCount / 60 as varchar) + ':' + right('00' + cast(MinuteCount - HourBandMin as varchar), 2)
		,FiveMinuteBandMin = cast(FiveMinuteBandMin / 60 as varchar) + ':' + right('00' + cast(FiveMinuteBandMin - HourBandMin as varchar), 2)
		,FiveMinuteBandMax = cast(FiveMinuteBandMax / 60 as varchar) + ':' + right('00' + cast(FiveMinuteBandMax % 60 as varchar), 2)
		,TenMinuteBandMin = cast(TenMinuteBandMin / 60 as varchar) + ':' + right('00' + cast(TenMinuteBandMin - HourBandMin as varchar), 2)
		,TenMinuteBandMax = cast(TenMinuteBandMax / 60 as varchar) + ':' + right('00' + cast(TenMinuteBandMax % 60 as varchar), 2)
		,FifteenMinuteBandMin = cast(FifteenMinuteBandMin / 60 as varchar) + ':' + right('00' + cast(FifteenMinuteBandMin - HourBandMin as varchar), 2)
		,FifteenMinuteBandMax = cast(FifteenMinuteBandMax / 60 as varchar) + ':' + right('00' + cast(FifteenMinuteBandMax % 60 as varchar), 2)
		,ThirtyMinuteBandMin = cast(ThirtyMinuteBandMin / 60 as varchar) + ':' + right('00' + cast(ThirtyMinuteBandMin - HourBandMin as varchar), 2)
		,ThirtyMinuteBandMax = cast(ThirtyMinuteBandMax / 60 as varchar) + ':' + right('00' + cast(ThirtyMinuteBandMax % 60 as varchar), 2)
		,HourBandMin = cast(HourBandMin / 60 as varchar) + ':00'
		,HourBandMax = cast(HourBandMax / 60 as varchar) + ':00'
		-- Format with week & day counter
		,Week_MinutesFormatted = 
			cast(MinuteCount / (60 * 24 * 7) as varchar) + 'w '
		  + cast((MinuteCount - WeekBandMin) / (60 * 24) as varchar) + 'd '
		  + right('00' + cast(((MinuteCount - DayBandMin) / 60) as varchar), 2) + ':'
		  + right('00' + cast(MinuteCount - HourBandMin as varchar), 2)

		,Week_FiveMinuteBandMin = 
			cast(FiveMinuteBandMin / (60 * 24 * 7) as varchar) + 'w ' 
		  + cast((FiveMinuteBandMin - WeekBandMin) / (60 * 24) as varchar) + 'd '
		  + right('00' + cast(((FiveMinuteBandMin - DayBandMin) / 60) as varchar), 2) + ':' 
		  + right('00' + cast(FiveMinuteBandMin - HourBandMin as varchar), 2)

		,Week_FiveMinuteBandMax =
			cast(FiveMinuteBandMax / (60 * 24 * 7) as varchar) + 'w ' 
		  + cast((FiveMinuteBandMax - WeekBandMin) / (60 * 24) as varchar) + 'd '
		  + right('00' + cast((FiveMinuteBandMax / 60) % 24 as varchar), 2) + ':' +
		  + right('00' + cast(FiveMinuteBandMax % 60 as varchar), 2)

		,Week_TenMinuteBandMin = 
			cast(TenMinuteBandMin / (60 * 24 * 7) as varchar) + 'w ' 
		  + cast((TenMinuteBandMin - WeekBandMin) / (60 * 24) as varchar) + 'd '
		  + right('00' + cast(((TenMinuteBandMin - DayBandMin) / 60) as varchar), 2) + ':' 
		  + right('00' + cast(TenMinuteBandMin - HourBandMin as varchar), 2)

		,Week_TenMinuteBandMax =
			cast(TenMinuteBandMax / (60 * 24 * 7) as varchar) + 'w ' 
		  + cast((TenMinuteBandMax - WeekBandMin) / (60 * 24) as varchar) + 'd '
		  + right('00' + cast((TenMinuteBandMax / 60) % 24 as varchar), 2) + ':' +
		  + right('00' + cast(TenMinuteBandMax % 60 as varchar), 2)

		,Week_FifteenMinuteBandMin = 
			cast(FifteenMinuteBandMin / (60 * 24 * 7) as varchar) + 'w ' 
		  + cast((FifteenMinuteBandMin - WeekBandMin) / (60 * 24) as varchar) + 'd '
		  + right('00' + cast(((FifteenMinuteBandMin - DayBandMin) / 60) as varchar), 2) + ':' 
		  + right('00' + cast(FifteenMinuteBandMin - HourBandMin as varchar), 2)

		,Week_FifteenMinuteBandMax =
			cast(FifteenMinuteBandMax / (60 * 24 * 7) as varchar) + 'w ' 
		  + cast((FifteenMinuteBandMax - WeekBandMin) / (60 * 24) as varchar) + 'd '
		  + right('00' + cast((FifteenMinuteBandMax / 60) % 24 as varchar), 2) + ':' +
		  + right('00' + cast(FifteenMinuteBandMax % 60 as varchar), 2)

		,Week_ThirtyMinuteBandMin = 
			cast(ThirtyMinuteBandMin / (60 * 24 * 7) as varchar) + 'w ' 
		  + cast((ThirtyMinuteBandMin - WeekBandMin) / (60 * 24) as varchar) + 'd '
		  + right('00' + cast(((ThirtyMinuteBandMin - DayBandMin) / 60) as varchar), 2) + ':' 
		  + right('00' + cast(ThirtyMinuteBandMin - HourBandMin as varchar), 2)

		,Week_ThirtyMinuteBandMax =
			cast(ThirtyMinuteBandMax / (60 * 24 * 7) as varchar) + 'w ' 
		  + cast((ThirtyMinuteBandMax - WeekBandMin) / (60 * 24) as varchar) + 'd '
		  + right('00' + cast((ThirtyMinuteBandMax / 60) % 24 as varchar), 2) + ':' +
		  + right('00' + cast(ThirtyMinuteBandMax % 60 as varchar), 2)

		,Week_HourBandMin = 
			cast(HourBandMin / (60 * 24 * 7) as varchar) + 'w ' 
		  + cast((HourBandMin - WeekBandMin) / (60 * 24) as varchar) + 'd '
		  + right('00' + cast(((HourBandMin - DayBandMin) / 60) as varchar), 2) + ':00'

		,Week_HourBandMax =
			cast(HourBandMax / (60 * 24 * 7) as varchar) + 'w ' 
		  + cast((HourBandMax - WeekBandMin) / (60 * 24) as varchar) + 'd '
		  + right('00' + cast((HourBandMax / 60) % 24 as varchar), 2) + ':00'

		,Week_DayBandMin =
			cast(DayBandMin / (60 * 24 * 7) as varchar) + 'w ' 
		  + cast((DayBandMin - WeekBandMin) / (60 * 24) as varchar) + 'd 00:00'

		,Week_DayBandMax =
			cast(DayBandMax / (60 * 24 * 7)  as varchar) + 'w ' 
		  + cast(isnull(nullif((DayBandMax - WeekBandMin) / (60 * 24), 7), 0) as varchar) + 'd 00:00' -- isnull(nullif(... ,7), 0) is a bit of a hack. Revisit this logic.

		,WeekBandMin = cast(WeekBandMin / (60 * 24 * 7) as varchar) + 'w 0d 00:00'
		,WeekBandMax = cast(WeekBandMax/ (60 * 24 * 7) as varchar) + 'w 0d 00:00'
	from
		BandRanges

), Durations as (
	-- generate the 'x < y' records of the final duration table
	select
		 MinuteCount = BandRanges.MinuteCount
		,RollupType = 0
		,FormattedMinutes = FormattedBandRanges.MinutesFormatted
		,FiveMinuteBand = FormattedBandRanges.FiveMinuteBandMin + ' < ' + FormattedBandRanges.FiveMinuteBandMax
		,TenMinuteBand = FormattedBandRanges.TenMinuteBandMin + ' < ' + FormattedBandRanges.TenMinuteBandMax
		,FifteenMinuteBand = FormattedBandRanges.FifteenMinuteBandMin + ' < ' + FormattedBandRanges.FifteenMinuteBandMax
		,ThirtyMinuteBand = FormattedBandRanges.ThirtyMinuteBandMin + ' < ' + FormattedBandRanges.ThirtyMinuteBandMax
		,HourBand = FormattedBandRanges.HourBandMin + ' < ' + FormattedBandRanges.HourBandMax
		,FormattedMinutesWithWeeks = FormattedBandRanges.Week_MinutesFormatted
		,FiveMinuteBandWithWeeks = FormattedBandRanges.Week_FiveMinuteBandMin + ' < ' + FormattedBandRanges.Week_FiveMinuteBandMax
		,TenMinuteBandWithWeeks = FormattedBandRanges.Week_TenMinuteBandMin + ' < ' + FormattedBandRanges.Week_TenMinuteBandMax
		,FifteenMinuteBandWithWeeks = FormattedBandRanges.Week_FifteenMinuteBandMin + ' < ' + FormattedBandRanges.Week_FifteenMinuteBandMax
		,ThirtyMinuteBandWithWeeks = FormattedBandRanges.Week_ThirtyMinuteBandMin + ' < ' + FormattedBandRanges.Week_ThirtyMinuteBandMax
		,HourBandWithWeeks = FormattedBandRanges.Week_HourBandMin + ' < ' + FormattedBandRanges.Week_HourBandMax
		,DayBandWithWeeks = FormattedBandRanges.Week_DayBandMin + ' < ' + FormattedBandRanges.Week_DayBandMax
		,WeekBand = FormattedBandRanges.WeekBandMin + ' < ' + FormattedBandRanges.WeekBandMax
		,WeekCount = BandRanges.WeekCount
		,DayCount = BandRanges.DayCount
		,HourCount = BandRanges.HourCount
		,ThirtyMinuteCount = BandRanges.ThirtyMinuteCount
		,FifteenMinuteCount = BandRanges.FifteenMinuteCount
		,TenMinuteCount = BandRanges.TenMinuteCount
		,FiveMinuteCount = BandRanges.FiveMinuteCount
	
	from FormattedBandRanges

	inner join BandRanges
	on FormattedBandRanges.MinuteCount = BandRanges.MinuteCount

	union all

	-- generate the 'less than ...' records of the final duration table
	select
		 MinuteCount = BandRanges.MinuteCount
		,RollupType = -1
		,FormattedMinutes = FormattedBandRanges.MinutesFormatted
		,FiveMinuteBand = LessThan.Hours
		,TenMinuteBand = LessThan.Hours
		,FifteenMinuteBand = LessThan.Hours
		,ThirtyMinuteBand = LessThan.Hours
		,HourBand = LessThan.Hours
		,FormattedMinutesWithWeeks = FormattedBandRanges.Week_MinutesFormatted
		,FiveMinuteBandWithWeeks = LessThan.Days
		,TenMinuteBandWithWeeks = LessThan.Days
		,FifteenMinuteBandWithWeeks = LessThan.Days
		,ThirtyMinuteBandWithWeeks = LessThan.Days
		,HourBandWithWeeks = LessThan.Days
		,DayBandWithWeeks = LessThan.Days
		,WeekBand = LessThan.Weeks

		,WeekCount = BandRanges.WeekCount
		,DayCount = BandRanges.DayCount
		,HourCount = BandRanges.HourCount
		,ThirtyMinuteCount = BandRanges.ThirtyMinuteCount
		,FifteenMinuteCount = BandRanges.FifteenMinuteCount
		,TenMinuteCount = BandRanges.TenMinuteCount
		,FiveMinuteCount = BandRanges.FiveMinuteCount

	from FormattedBandRanges

	inner join BandRanges
	on FormattedBandRanges.MinuteCount = BandRanges.MinuteCount

	cross apply (
		select 
			 Hours = '< ' + cast(HourCount + 1 as varchar) + ' hour' + case when HourCount + 1 <> 1 then 's' else '' end
			,Days = '< ' + cast(DayCount + 1 as varchar) + ' day' + case when DayCount + 1 <> 1 then 's' else '' end			
			,Weeks = '< ' + cast((WeekCount + 1) as varchar) + ' week' + case when (WeekCount + 1) <> 1 then 's' else '' end
		) as LessThan


	union all

	-- generate the 'greater than ...' records of the final duration table
	select
		 MinuteCount = BandRanges.MinuteCount
		,RollupType = 1
		,FormattedMinutes = FormattedBandRanges.MinutesFormatted
		,FiveMinuteBand = GreaterThan.Hours
		,TenMinuteBand = GreaterThan.Hours
		,FifteenMinuteBand = GreaterThan.Hours
		,ThirtyMinuteBand = GreaterThan.Hours
		,HourBand = GreaterThan.Hours
		,FormattedMinutesWithWeeks = FormattedBandRanges.Week_MinutesFormatted
		,FiveMinuteBandWithWeeks = GreaterThan.Days
		,TenMinuteBandWithWeeks = GreaterThan.Days
		,FifteenMinuteBandWithWeeks = GreaterThan.Days
		,ThirtyMinuteBandWithWeeks = GreaterThan.Days
		,HourBandWithWeeks = GreaterThan.Days
		,DayBandWithWeeks = GreaterThan.Days
		,WeekBand = GreaterThan.Weeks

		,WeekCount = BandRanges.WeekCount
		,DayCount = BandRanges.DayCount
		,HourCount = BandRanges.HourCount
		,ThirtyMinuteCount = BandRanges.ThirtyMinuteCount
		,FifteenMinuteCount = BandRanges.FifteenMinuteCount
		,TenMinuteCount = BandRanges.TenMinuteCount
		,FiveMinuteCount = BandRanges.FiveMinuteCount		

	from FormattedBandRanges

	inner join BandRanges
	on FormattedBandRanges.MinuteCount = BandRanges.MinuteCount

	cross apply (
		select 
			 Hours = '>= ' + cast(HourCount as varchar) + ' hour' + case when HourCount <> 1 then 's' else '' end
			,Days =  '>= ' + cast(DayCount as varchar) + ' day' + case when DayCount <> 1 then 's' else '' end
			,Weeks = 
				'>= ' + cast(WeekCount as varchar) + ' week' + case when WeekCount <> 1 then 's' else '' end
		) as GreaterThan
)

insert into WH.Duration (
	 DurationID
	,Minutes
	,RollupType
	,FormattedMinutes
	,FiveMinuteBand
	,TenMinuteBand
	,FifteenMinuteBand
	,ThirtyMinuteBand
	,HourBand
	,FormattedMinutesWithWeeks
	,FiveMinuteBandWithWeeks
	,TenMinuteBandWithWeeks
	,FifteenMinuteBandWithWeeks
	,ThirtyMinuteBandWithWeeks
	,HourBandWithWeeks
	,DayBandWithWeeks
	,WeekBand
	,WeekKey
	,DayKey
	,HourKey
	,ThirtyMinuteKey
	,FifteenMinuteKey
	,TenMinuteKey
	,FiveMinuteKey
	,MinuteKey
	,IsMaxDuration
)

-- generate a single record for N/A duration (DurationID = -1)
select
	 DurationID = cast(-1 as int)
	,Minutes = null
	,RollupType = 0
	,FormattedMinutes = NotApplicable.Value
	,FiveMinuteBand = NotApplicable.Value
	,TenMinuteBand = NotApplicable.Value
	,FifteenMinuteBand = NotApplicable.Value
	,ThirtyMinuteBand = NotApplicable.Value
	,HourBand = NotApplicable.Value
	,FormattedMinutesWithWeeks = NotApplicable.Value
	,FiveMinuteBandWithWeeks = NotApplicable.Value
	,TenMinuteBandWithWeeks = NotApplicable.Value
	,FifteenMinuteBandWithWeeks = NotApplicable.Value
	,ThirtyMinuteBandWithWeeks = NotApplicable.Value
	,HourBandWithWeeks = NotApplicable.Value
	,DayBandWithWeeks = NotApplicable.Value
	,WeekBand = NotApplicable.Value
	,WeekKey = -1
	,DayKey = -1
	,HourKey = -1
	,ThirtyMinuteKey = -1
	,FifteenMinuteKey = -1
	,TenMinuteKey = -1
	,FiveMinuteKey = -1
	,MinuteKey = -1
	,IsMaxDuration = 0

from (select Value = 'N/A') NotApplicable

union all
	
-- Add a duration ID to the whole table, ordered by RollupType and Minutes
select
	 DurationID = cast(row_number() over (order by RollupType, MinuteCount) as int)
	,Minutes = MinuteCount
	,RollupType
	,FormattedMinutes
	,FiveMinuteBand
	,TenMinuteBand
	,FifteenMinuteBand
	,ThirtyMinuteBand
	,HourBand
	,FormattedMinutesWithWeeks
	,FiveMinuteBandWithWeeks
	,TenMinuteBandWithWeeks
	,FifteenMinuteBandWithWeeks
	,ThirtyMinuteBandWithWeeks
	,HourBandWithWeeks
	,DayBandWithWeeks
	,WeekBand
	-- produce unique identifier per time count & rollup type
	,WeekKey = dense_rank() over (order by RollupType, WeekCount)
	,DayKey = dense_rank() over (order by RollupType, DayCount)
	,HourKey = dense_rank() over (order by RollupType, HourCount)
	,ThirtyMinuteKey = dense_rank() over (order by RollupType, ThirtyMinuteCount)
	,FifteenMinuteKey = dense_rank() over (order by RollupType, FifteenMinuteCount)
	,TenMinuteKey = dense_rank() over (order by RollupType, TenMinuteCount)
	,FiveMinuteKey = dense_rank() over (order by RollupType, FiveMinuteCount)
	,MinuteKey = dense_rank() over (order by RollupType, MinuteCount)
	
	,IsMaxDuration = case when MinuteCount = @MaxDurationMinutes then 1 else 0 end
	
from Durations

option (maxrecursion 0)