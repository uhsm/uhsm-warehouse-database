﻿create procedure [Utility].[BuildMember] as

truncate table WH.Member

;
with

HierarchyLevel as
	(
	select
		ValueID = ValueXref.ValueXrefID
		,ValueCode = Value.ValueCode
		,Value = Value.Value
		,ContextCode = Context.ContextCode
		,ValueXrefValueID = ValueXref.ValueID
	from
		ReferenceMap.Map.ValueXref

	left join ReferenceMap.Map.Value
	on	Value.ValueID = ValueXref.ValueXrefID

	left join ReferenceMap.Map.AttributeContext
	on	AttributeContext.AttributeContextID = Value.AttributeContextID

	left join ReferenceMap.Map.Context
	on	Context.ContextID = AttributeContext.ContextID

	where
		exists
		(
		select
			1
		from
			ReferenceMap.Map.Value OtherValue

		inner join ReferenceMap.Map.AttributeContext OtherAttributeContext
		on	OtherValue.ValueID = ValueXref.ValueXrefID
		and	OtherValue.AttributeContextID = OtherAttributeContext.AttributeContextID

		inner join ReferenceMap.Map.Context OtherContext
		on	OtherContext.ContextID = OtherAttributeContext.ContextID
		)

	and	not exists
		(
		select
			1
		from
			ReferenceMap.Map.ValueXref Previous
		where
			Previous.ValueID = Value.ValueID
		and	exists
			(
			select
				1
			from
				ReferenceMap.Map.Value OtherValue

			inner join ReferenceMap.Map.AttributeContext OtherAttributeContext
			on	OtherValue.ValueID = Previous.ValueXrefID
			and	OtherValue.AttributeContextID = OtherAttributeContext.AttributeContextID

			inner join ReferenceMap.Map.Context OtherContext
			on	OtherContext.ContextID = OtherAttributeContext.ContextID
			)
		and	Previous.AttributeXrefID > ValueXref.AttributeXrefID
		)

	)

INSERT INTO WH.Member
(
	 AttributeCode
	,Attribute
	,SourceContextID
	,SourceContextCode
	,SourceContext
	,SourceValueID
	,SourceValueCode
	,SourceValue
	,LocalValueID
	,LocalValueCode
	,LocalValue
	,NationalValueID
	,NationalValueCode
	,NationalValue
	,OtherValueID
	,OtherValueCode
	,OtherValue
)

select
	 AttributeCode = Attribute.AttributeCode
	,Attribute = Attribute.Attribute
	,Context.ContextID
	,SourceContextCode = Context.ContextCode
	,SourceContext = Context.Context
	,SourceValueID = Value.ValueID
	,SourceValueCode = Value.ValueCode

	,SourceValue =
		case
		when Attribute.UseProperCase = 1
		then Utility.ProperCase(Value.Value)
		else Value.Value
		end
		 +
		case
		when exists
			(
			select
				1
			from
				ReferenceMap.Map.Value SameValue
			where
				SameValue.Value = Value.Value
			and	SameValue.AttributeContextID = Value.AttributeContextID
			and	SameValue.ValueID <> Value.ValueID
			)
		then ' (' + Context.ContextCode + ':' + Value.ValueCode + ')'
		else ''
		end

	,LocalValueID = LocalValue.ValueID
	,LocalValueCode = LocalValue.ValueCode
	,LocalValue = Utility.ProperCase(LocalValue.Value)
	,NationalValueID = NationalValue.ValueID
	,NationalValueCode = NationalValue.ValueCode

	,NationalValue =
		left(
			case
			when Attribute.UseProperCase = 1
			then Utility.ProperCase(NationalValue.Value)
			else NationalValue.Value
			end
			,200
		)

	,OtherValueID = OtherValue.ValueID
	,OtherValueCode = OtherValue.ValueCode

	,OtherValue =
		left(
			case
			when Attribute.UseProperCase = 1
			then Utility.ProperCase(OtherValue.Value)
			else OtherValue.Value
			end
			,200
		)

from
	ReferenceMap.Map.Value

inner join ReferenceMap.Map.AttributeContext
on	AttributeContext.AttributeContextID = Value.AttributeContextID

inner join ReferenceMap.Map.Attribute
on	Attribute.AttributeID = AttributeContext.AttributeID

inner join ReferenceMap.Map.Context
on	Context.ContextID = AttributeContext.ContextID
and	Context.BaseContext = 'True'

left join HierarchyLevel LocalValue
on	LocalValue.ValueXrefValueID = Value.ValueID
and	LocalValue.ContextCode = 'L'

left join HierarchyLevel NationalValue
on	NationalValue.ValueXrefValueID = Value.ValueID
and	NationalValue.ContextCode = 'N'

left join HierarchyLevel OtherValue
on	OtherValue.ValueXrefValueID = Value.ValueID
and	OtherValue.ContextCode = 'O'

