﻿CREATE procedure [Utility].[BuildReferenceDataMap]
	 @Table varchar(255)
	,@NewRows int output
	,@Debug bit = 0
	,@ActivityTableOverride varchar(max) = null
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare @TableID smallint =
	(
	select
		TableBase.TableID
	from
		ReferenceMap.Map.TableBase
	where
		TableBase.[Table] = @Table
	)

exec ReferenceMap.Map.BuildMissingValues @TableID, @NewRows output, @Debug, @ActivityTableOverride
exec ReferenceMap.Map.BuildMissingXrefs @Debug
exec ReferenceMap.Map.BuildDefaultXrefs @Debug

if @Debug = 0
begin

	select
		@Elapsed = DATEDIFF(minute, @StartTime, getdate())

	select
		@Stats =
			'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes' +
			', Table: ' + @Table


	exec Audit.WriteLogEvent
		 @ProcedureName
		,@Stats
end

