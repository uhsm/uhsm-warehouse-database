﻿CREATE procedure [Utility].SetLastLoadTime
(
	 @Process varchar(255)
	 ,@LastLoadTime datetime
)
as

begin

	declare @Parameter varchar(255) = 'Last load time: ' + @Process + ' (System Generated)'

	update
		Utility.Parameter
	set
		DateValue = @LastLoadTime
		,Updated = getdate()
	where
		Parameter.Parameter = @Parameter

	if @@rowcount = 0
		insert
		into
			Utility.Parameter
			(
			Parameter
			,DateValue
			)
		values
			(
			@Parameter
			,@LastLoadTime
			)

	return 0

end
