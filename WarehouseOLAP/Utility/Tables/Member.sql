﻿CREATE TABLE [WH].[Member] (
    [AttributeCode]     VARCHAR (20)  NOT NULL,
    [Attribute]         VARCHAR (100) NOT NULL,
    [SourceContextID]   INT           NOT NULL,
    [SourceContextCode] VARCHAR (20)  NOT NULL,
    [SourceContext]     VARCHAR (100) NOT NULL,
    [SourceValueID]     INT           NOT NULL,
    [SourceValueCode]   VARCHAR (100) NOT NULL,
    [SourceValue]       VARCHAR (900) NOT NULL,
    [LocalValueID]      INT           NULL,
    [LocalValueCode]    VARCHAR (50)  NULL,
    [LocalValue]        VARCHAR (200) NULL,
    [NationalValueID]   INT           NULL,
    [NationalValueCode] VARCHAR (50)  NULL,
    [NationalValue]     VARCHAR (200) NULL,
    [OtherValueID]   INT           NULL,
    [OtherValueCode] VARCHAR (50)  NULL,
    [OtherValue]     VARCHAR (200) NULL,
    CONSTRAINT [PK_Member] PRIMARY KEY CLUSTERED ([SourceValueID] ASC)
);
GO

CREATE NONCLUSTERED INDEX [IX_WH_Member_01]
ON [WH].[Member] ([AttributeCode],[SourceContextCode])
INCLUDE ([SourceValueCode],[NationalValueCode])
GO
