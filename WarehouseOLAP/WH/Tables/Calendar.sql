﻿CREATE TABLE [WH].[Calendar] (
    [DateID]              INT          NOT NULL,
    [TheDate]             DATE         NOT NULL,
    [TheDay]              VARCHAR (20) NULL,
    [DayOfWeekKey]        INT          NULL,
    [LongDate]            VARCHAR (20) NULL,
    [TheMonth]            VARCHAR (20) NULL,
    [TheMonthKey]         INT          NULL,
    [FinancialQuarter]    VARCHAR (20) NULL,
    [FinancialYear]       VARCHAR (20) NULL,
    [FinancialMonthKey]   INT          NULL,
    [FinancialQuarterKey] INT          NULL,
    [CalendarQuarter]     VARCHAR (20) NULL,
    [CalendarSemester]    VARCHAR (20) NOT NULL,
    [CalendarYear]        VARCHAR (20) NULL,
    [LastCompleteMonth]   INT          NOT NULL,
    [WeekNoKey]           VARCHAR (6)  NULL,
    [WeekNo]              VARCHAR (30) NULL,
    [FirstDayOfWeek]      DATE         NULL,
    [LastDayOfWeek]       DATE         NULL,
    [FirstDayOfMonth]     DATE         NULL,
    [LastDayOfMonth]      DATE         NULL,
    [MonthYear]           VARCHAR (20) NULL,
	[IsBankHoliday]		  BIT NULL,
    CONSTRAINT [PK_WHCalendar] PRIMARY KEY CLUSTERED ([DateID] ASC)
);

GO

CREATE INDEX [ix_Calendar_DayOfWeekKey_TheMonthKey_includes]
ON [WH].[Calendar] ([DayOfWeekKey], [TheMonthKey])
INCLUDE ([TheDate], [CalendarYear]) 

WITH (FILLFACTOR=100);
GO
