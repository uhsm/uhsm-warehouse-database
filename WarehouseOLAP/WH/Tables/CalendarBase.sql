﻿CREATE TABLE [WH].[CalendarBase] (
    [TheDate] DATE NOT NULL,
    CONSTRAINT [PK_WHCalendarBase] PRIMARY KEY CLUSTERED ([TheDate] ASC)
);
