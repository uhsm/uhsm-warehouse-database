﻿create table WH.Duration (
	 DurationID int not null
    ,Minutes int null
    ,RollupType int not null
    ,FormattedMinutes varchar(100) null
    ,FiveMinuteBand varchar(100) null
    ,TenMinuteBand varchar(100) null
    ,FifteenMinuteBand varchar(100) null
    ,ThirtyMinuteBand varchar(100) null
    ,HourBand varchar(100) null
    ,FormattedMinutesWithWeeks varchar(100) null
    ,FiveMinuteBandWithWeeks varchar(100) null
    ,TenMinuteBandWithWeeks varchar(100) null
    ,FifteenMinuteBandWithWeeks varchar(100) null
    ,ThirtyMinuteBandWithWeeks varchar(100) null
    ,HourBandWithWeeks varchar(100) null
    ,DayBandWithWeeks varchar(100) null
	,WeekBand varchar(100) null
    ,WeekKey int null
	,DayKey int null
	,HourKey int null
    ,ThirtyMinuteKey int null
	,FifteenMinuteKey int null
	,TenMinuteKey int null
	,FiveMinuteKey int null
	,MinuteKey int null
	,IsMaxDuration bit not null
	,constraint PK_Duration primary key clustered (DurationID)
);