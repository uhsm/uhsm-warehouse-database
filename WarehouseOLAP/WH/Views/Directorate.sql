﻿CREATE view [WH].[Directorate] as

select
	 SourceContextCode = Member.SourceContextCode
	,SourceContextID = Member.SourceContextID
	,SourceContext = Member.SourceContext
	,SourceID = Member.SourceValueID
	,SourceCode = Member.SourceValueCode
	,SourceValue = Member.SourceValue
	,LocalID = Member.LocalValueID
	,LocalCode = Member.LocalValueCode
	,LocalValue = Member.LocalValue
	,NationalID = Member.NationalValueID
	,NationalCode = Member.NationalValueCode
	,NationalValue = Member.NationalValue
from
	WH.Member
where
	Member.AttributeCode = 'DIRECTORATE'

GO
