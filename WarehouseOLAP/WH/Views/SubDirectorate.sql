﻿CREATE view [WH].[SubDirectorate] as

select
	 SourceContextCode = Member.SourceContextCode
	,SourceContextID = Member.SourceContextID
	,SourceContext = Member.SourceContext
	,SourceID = Member.SourceValueID
	,SourceCode = Member.SourceValueCode
	,SourceValue = Member.SourceValue
	,LocalID = Member.LocalValueID
	,LocalCode = Member.LocalValueCode
	,LocalValue = Member.LocalValue
	,NationalID = Member.NationalValueID
	,NationalCode = Member.NationalValueCode
	,NationalValue = Member.NationalValue
from
	WH.Member
where
	Member.AttributeCode = 'SERVICE'
and	Member.LocalValueID is not null

union all

select
	 SourceContextCode = Member.SourceContextCode
	,SourceContextID = Member.SourceContextID
	,SourceContext = Member.SourceContext
	,SourceID = Member.SourceValueID
	,SourceCode = Member.SourceValueCode
	,SourceValue = Member.SourceValue
	,LocalID = Member.LocalValueID
	,LocalCode = Member.LocalValueCode
	,LocalValue = Member.LocalValue
	,NationalID = Member.NationalValueID
	,NationalCode = Member.NationalValueCode
	,NationalValue = Member.NationalValue
from
	WH.Member
where
	Member.AttributeCode = 'DIRECTORATE'
and	not exists
	(
	select
		1
	from
		WH.Member Service
	where
		Service.AttributeCode = 'SERVICE'
	and	Service.LocalValueCode = Member.SourceValueID
	)

